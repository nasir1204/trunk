#!/bin/sh
#
# Infinitely running script. Use ctrl+C to stop.
################################################

platform=`uname`

if [ $platform = "Linux" ]; then
agent=`rpm -qa | grep MFEcma`
else
if [ $platform = "Darwin" ]; then
agent=`cat /etc/ma.d/EPOAGENT3000/config.xml | grep "<Version>" | cut -d ">" -f 2 | cut -d "<" -f 1`
fi
fi

#SunOS
#agent=`pkginfo -l MFEcma | grep VERSION | awk '{print $2}'`
#else
#if [ $platform = "Darwin" ]; then
#agent=`cat /etc/ma.d/EPOAGENT3000/config.xml | grep "<Version>" | cut -d ">" -f 2 | cut -d "<" -f 1`
#else
#if [ $platform = "HP-UX" ]; then
#agent=`uname`
#fi
#fi

if [ $? != 0 ]
then
   echo "No McAfee Agent Installed..."
   echo "Exiting Performance Monitor script..."
   exit
fi

echo " "
echo "PLATFORM   : $platform"
echo "AGENT      : $agent"
echo "STARTED ON : `date`"
echo "PERFORMANCE TEST IS IN PROGRESS..."

log1="`pwd`/PerfLogFileMaSvc.csv"
log2="`pwd`/PerfLogFileMaCmnSvc.csv"
log3="`pwd`/PerfLogFileMaCompatSvc.csv"
log4="`pwd`/PerfLogFileCmdAgent.csv"
log5="`pwd`/PerfLogFileMaConfig.csv"
log6="`pwd`/PerfLogFileMueInUse.csv"

if [ "$1" = "" ]
then
    seconds="1"
else
    seconds="$1"
fi

# Measure the process specs every second unless provided by user.
while [ 1 ]
    do
        if [ $platform = "Linux" ]
          then
              ps -v | grep masvc >> $log
              date >> $log
        fi

        if [ $platform = "Darwin" ]
          then
              ps -v | grep masvc >> $log1 && date >> $log1
              ps -v | grep macmnsvc >> $log2 && date >> $log2
              ps -v | grep macompatsvc >> $log3 && date >> $log3
              ps -v | grep cmdagent >> $log4 && date >> $log4
              ps -v | grep maconfig >> $log5 && date >> $log5
              ps -v | grep Mue_InUse >> $log6 && date >> $log6
        fi

        sleep $seconds
    done
    
    

