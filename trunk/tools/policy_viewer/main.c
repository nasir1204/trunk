#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#include "ma/ma_common.h"
#include "../db_viewer/ma_db_viewer.h"
#include "ma/internal/utils/getopt/ma_getopt.h"


#define ALL_PRODUCTS "ALL"

typedef enum ma_viewer_type {
	POLICIES,
	TASKS,	
	PROPERTIES
}ma_viewer_type_t;

static char *tool_usage[] = {
	"\nUsage : madb2xml [type] -d <Database file> -o <Output Xml file name> -i[optional] <Product Id>\n",
    "where type is:\n",
    "  -t:          for tasks \n",
    "  -p:          for policies \n",
	"  -r:          for properties \n",
    "   e.g.  \n",
	"Get all Policy details : \n",
	"   madb2xml -p -d \"C:\\mapolicy.db\" -o \"C:\\policy.xml\" \n",
	"Get Policy details for a particular product : \n",
	"   madb2xml -p -d \"C:\\mapolicy.db\" -o \"C:\\policy.xml\" -i EPOAGENT3000 \n",
	NULL
};


ma_error_t ma_validate_params(const char *db_name, const char *productId, const char *xmlFileName){
	if(db_name && (strlen(db_name) > 0) && 
	   productId && (strlen(productId) > 0) && 
	   xmlFileName && (strlen(xmlFileName) > 0)){	
		return MA_OK;
	}

	return MA_ERROR_INVALIDARG;
}

static void  print_usage(char **usage) {
    char **pp = NULL;
    for(pp = usage; (*pp != NULL); pp++) {        
		fprintf(stderr, "%s \n", *pp);
    }
}

int main(int argc, char *argv[]){
    ma_error_t rc = MA_OK;    
	
	int ch;
	ma_viewer_type_t viewer_type = POLICIES;
	const char *db_path = NULL;
	const char *xml_path = NULL;	
	char *product_id = ALL_PRODUCTS;
	char *tmp_product_id = NULL;
	char *data_buff = NULL;
	int data_buff_size = 0;
	FILE *fp = NULL;
		
    if(argc < 6) {
        print_usage(tool_usage); 
        return 1;
    }   
	
    while ((ch = ma_getopt(argc, argv, "tpri:d:o:")) != EOF){
        switch (ch){
            case 't':
				viewer_type = TASKS;                
                break;

            case 'p':
				viewer_type = POLICIES;                
                break;

			case 'r':
				viewer_type = PROPERTIES;                
                break;

			case 'i':
				product_id = (char *)ma_getoptarg();                				
                break;

			case 'd':
				db_path = ma_getoptarg();                
                break;

			case 'o':
				xml_path = ma_getoptarg();                
                break;

            default:
                printf("\n Error: Inalid option supplied \n \n");
				print_usage(tool_usage); 
                return 1;
                break;
        }
    }

	/*
	viewer_type == POLICIES;
	db_path = "E:\\Project\\ma\\build\\msvc\\Release\\mapolicy.db";
	xml_path = "test.xml";*/
	if(MA_OK != (rc = ma_validate_params(db_path, xml_path, product_id))){
		fprintf(stderr, "Invalid parameters passed");
	}
	

	// Get Policy details
	if(viewer_type == POLICIES)
	{

		if(0 == strcmp(product_id, ALL_PRODUCTS)){
			if(MA_OK != (rc = get_all_product_policies(db_path, &data_buff, &data_buff_size))){
				fprintf(stderr, "Failed to retrieve policy details, err = %d \n", rc);
				return rc;
			}
		}
		else{
			if(MA_OK != (rc = get_product_policies(db_path, product_id, &data_buff, &data_buff_size))){
				fprintf(stderr, "Failed to retrieve policy details for product id %s, err = %d \n",product_id, rc);
				return rc;
			}
		}

	}
	// Get task details
	else if(viewer_type == TASKS)
	{
		if(0 == strcmp(product_id, ALL_PRODUCTS)){
			if(MA_OK != (rc = get_all_product_tasks(db_path, &data_buff, &data_buff_size))){
				fprintf(stderr, "Failed to retrieve task details, err = %d \n", rc);
				return rc;
			}
		}
		else {
			if(MA_OK != (rc = get_product_tasks(db_path, product_id, &data_buff, &data_buff_size))){
				fprintf(stderr, "Failed to retrieve task details for product id %s, err = %d \n",product_id, rc);
				return rc;
			}
		}
	}
	else if(viewer_type == PROPERTIES)
	{
		if(MA_OK != (rc = get_all_properties(db_path, &data_buff, &data_buff_size))){
			fprintf(stderr, "Failed to retrieve property details, err = %d \n", rc);
			return rc;
		}
	}

	// Save the contents to the xml file.
	fp = fopen(xml_path,"w");
	if(!fp) {
		rc = MA_ERROR_XML_FILE_NOT_FOUND;
		fprintf(stderr, "Failed to open file %s for writing", xml_path);
		return MA_ERROR_XML_FILE_NOT_FOUND;
	}
	fprintf(fp, "%s", data_buff);
	fclose(fp);

	if(data_buff_size > 0) {
		free(data_buff);
	}
	
	return rc;
}


/* 
Policy
------
<Product SoftwareID="EPOAGENT3000">
	<PolicyRoot>
		<EPOPolicySettings name="My Default" feature="EPOAGENTMETA" category="Product Improvement Program" type="Telemetry">
			<Section name="Telemetry">
				<Setting name="branch">Current</Setting>
				<Setting name="optIn">true</Setting>
				<Setting name="serverLevelInstallFlag">true</Setting>
			</Section>
		</EPOPolicySettings>
	</PolicyRoot>
</Product>

Task :
----
<Product SoftwareID="EPOAGENT3000">
	<TaskRoot>
		<Task id="200" name="EPm_dep" type="Deployment" priority="0" timestamp="2014-07-25T10:24:20.000">
			<Section name="AutoUpdateOption">
				<Setting name="AutoUpdateEnabled">0</Setting>
			</Section>
		</Task>
	</TaskRoot>
</Product>



*/