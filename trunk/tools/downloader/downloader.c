#include "ma/internal/utils/downloader/ma_repository_downloader.h"
#include "ma/ma_client.h"
#include "ma/repository/ma_repository_client.h"
#include "ma/logger/ma_outputdebugstring_logger.h"

#include <stdio.h>

#pragma comment(lib, "ws2_32.lib")

#ifdef MA_WINDOWS
#include <windows.h>
#endif
#ifdef _MSC_VER
# include <crtdbg.h> /* */
# define MA_TRACKLEAKS _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF)
#else
# define MA_TRACKLEAKS ((void *)0)
#endif


ma_downloader_t *downloader = NULL;
static ma_bool_t download_done = MA_FALSE;
ma_net_client_service_t *net_service = NULL;
ma_client_t *client = NULL;

static ma_error_t downloader_progress_cb(ma_downloader_t *self, ma_download_progress_state_t state, ma_stream_t *stream, size_t size, void *cb_data) {
    if(MA_DOWNLOADER_DOWNLOAD_PRE_START == state) {
        ma_repository_url_type_t url_type;
        const ma_repository_t *repo = ma_repository_downloader_get_current_repository(downloader);
        ma_repository_get_url_type((ma_repository_t *)repo, &url_type);
        ma_repository_downloader_set_use_next_source(self, MA_TRUE);
    }

    if(MA_DOWNLOADER_DOWNLOAD_DONE == state) {
        ma_bytebuffer_t *byte_buffer = NULL;
        if(MA_OK == ma_stream_get_buffer(stream, &byte_buffer)) {
            fprintf(stderr, "size  = %d\n", ma_bytebuffer_get_size(byte_buffer));
            fprintf(stderr, "data  = %s\n", ma_bytebuffer_get_bytes(byte_buffer));
        }        
        download_done = MA_TRUE;
    }

    if(MA_DOWNLOADER_DOWNLOAD_FAILED == state) {
        fprintf(stderr, "download failed\n");        
        download_done = MA_TRUE;
    }

    if(download_done) {
        ma_downloader_release(self);
        ma_stream_release(stream);

        ma_net_client_service_stop(net_service);
        ma_client_stop(client);
    }

    return MA_OK;
}

int main(int argc, char *argv[])
{    
    ma_error_t rc = MA_OK;
    ma_event_loop_t *event_loop = NULL;   
    ma_repository_list_t *repository_list = NULL;
    ma_proxy_config_t *proxy_config = NULL;
    ma_msgbus_t *msgbus = NULL;
    ma_logger_t *logger = NULL;
    
    MA_TRACKLEAKS;

    if(MA_OK != (rc = ma_msgbus_create("DOWNLOADER_TEST", &msgbus))) 
        return rc;

    if(!(event_loop = ma_msgbus_get_event_loop(msgbus)))
        return -1;    

    if(MA_OK != (rc = ma_net_client_service_create(event_loop, &net_service)))
        return rc;
    
    if(MA_OK != (rc = ma_net_client_service_start(net_service)))    
        return rc;

    if(MA_OK != (rc = ma_client_create("DOWNLOADER_TEST",  &client)))    
        return rc;

    ma_client_set_msgbus(client, msgbus);

    ma_msgbus_start(msgbus);

    if(MA_OK != (rc = ma_client_start(client)))    
        return rc;
    
    if(MA_OK != (rc = ma_repository_client_get_repositories(client, &repository_list)))
        return rc;

    if(MA_OK != (rc = ma_repository_client_get_proxy_config(client, &proxy_config)))
        return rc;
    
    if(MA_OK != (rc = ma_outputdebugstring_logger_create(&logger)))
        return rc;

    if(MA_OK == ma_repository_downloader_create(&downloader)) {
        ma_stream_t *stream = NULL;
        ma_repository_downloader_set_logger(downloader , logger);
        ma_repository_downloader_set_net_client(downloader, net_service);
        ma_repository_downloader_set_repository_list(downloader, repository_list);
        ma_repository_downloader_set_proxy_config(downloader, proxy_config);
        unlink("c:\\windows\\temp\\my_sitestat.xml");
            
        if(MA_OK == (rc = ma_fstream_create("c:\\windows\\temp\\catalog.z", MA_STREAM_ACCESS_FLAG_WRITE, NULL, &stream))) {
		    rc = ma_downloader_start(downloader, "/Software/catalog.z", stream, downloader_progress_cb, downloader);            
        }        
    }
      
    ma_msgbus_stop(msgbus, MA_TRUE);
    
    ma_proxy_config_release(proxy_config);
    ma_repository_list_release(repository_list);

    ma_net_client_service_release(net_service);
    ma_client_release(client);

    ma_msgbus_release(msgbus);
    ma_logger_release(logger);

	return rc;
}

