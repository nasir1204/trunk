#include "ma/internal/services/event/me_event_message_defs.h"
#include "ma/internal/services/event/ma_event_service.h"
#include "ma/event/ma_event_context.h"
#include "ma/event/ma_event_bag.h"
#include "ma/internal/core/msgbus/ma_msgbus_impl.h"
#include "ma/datastore/ma_ds_database.h"
#include "ma/logger/ma_console_logger.h"
#include "ma/logger/ma_file_logger.h"
#include "tests/utils/macrypto_test/ma_crypto_tests_setup.h"

ma_logger_t *ma_services_logger = NULL;
#include "ma_services_spipe_hook.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

void timer_event(uv_timer_t *handle, int status);


#define NO_OF_SITES 2

#define CA_FILE_NAME        "C:\\Programdata\\cabundle.cer"
#define GLOBAL_VERSION      "VlUZlJaB6wuBd75vKrG/LUUa2dLd59wdI0BOR9lUmIs="
#define LOCAL_VERSION       "Mon, 16 Jul 2012 11:52:32 UTC"
typedef struct test_data_s {
    char *name;
    char *ver;
    char *fqdn;
    char *short_name;
    char *ip;
    ma_uint32_t port;
    ma_uint32_t ssl_port;
    ma_uint32_t order;
} test_data_t;

/* test data for ordered site list */
static test_data_t data1[NO_OF_SITES] = { 
								  {"EPO_SER3", "5.0.0", "test1.corp.nai.org", "test1", "172.16.220.2", 80, 443, 3},                                  
								  {"EPO_SER3", "5.0.0", "test2.corp.nai.org", "test2", "172.16.220.3", 80, 443, 3},                                  
                                };


typedef struct ma_common_objects_s{
	ma_file_logger_t *file_logger;
	ma_msgbus_t *message_bus_instance;
	ma_crypto_t *crypto_object;	
	ma_event_loop_t *bus_loop;
	ma_net_client_service_t *net_service;
	ma_ah_client_service_t *agent_handler;	
	ma_ah_sitelist_t *sitelist;
}ma_common_objects_t;

void ma_common_objects_init(ma_common_objects_t *common_objects)
{	
	int i=0;
	MA_LOG(ma_services_logger, MA_LOG_SEV_INFO, "Setting up common objects.");		
    ma_msgbus_initialize();
	ma_msgbus_set_logger(ma_services_logger);
    common_objects->message_bus_instance = ma_msgbus_get_instance();
	common_objects->bus_loop = ma_msgbus_get_event_loop(common_objects->message_bus_instance);
    create_macrypto_object_with_keys(MA_CRYPTO_MODE_NON_FIPS, MA_CRYPTO_ROLE_USER, MA_CRYPTO_AGENT_MANAGED, &common_objects->crypto_object);

    ma_net_client_service_create(common_objects->bus_loop, &common_objects->net_service);
    ma_net_client_service_setlogger(common_objects->net_service, ma_services_logger);
    ma_net_client_service_start(common_objects->net_service);


	ma_ah_sitelist_create(&common_objects->sitelist);  
    ma_ah_sitelist_set_global_version(common_objects->sitelist, GLOBAL_VERSION);
    ma_ah_sitelist_set_local_version(common_objects->sitelist, LOCAL_VERSION);
    ma_ah_sitelist_set_ca_file(common_objects->sitelist, CA_FILE_NAME);    
    for(i = 0; i < NO_OF_SITES ; i++) {
        ma_ah_site_t *site = NULL;
        ma_ah_site_create(&site);
        ma_ah_site_set_name(site, data1[i].name);
        ma_ah_site_set_version(site, data1[i].ver);
        ma_ah_site_set_fqdn_name(site, data1[i].fqdn);
        ma_ah_site_set_short_name(site, data1[i].short_name);
        ma_ah_site_set_ip_address(site, data1[i].ip);
        ma_ah_site_set_port(site, data1[i].port);
        ma_ah_site_set_ssl_port(site, data1[i].ssl_port);
        ma_ah_site_set_order(site, data1[i].order);
        ma_ah_sitelist_add_site(common_objects->sitelist, site);        
    }
    ma_ah_sitelist_adjust_site_order(common_objects->sitelist);

    ma_ah_client_service_create(common_objects->bus_loop, common_objects->net_service, common_objects->crypto_object, &common_objects->agent_handler);
    ma_ah_client_service_set_logger(common_objects->agent_handler, ma_services_logger);
	ma_ah_client_service_set_sitelist(common_objects->agent_handler, common_objects->sitelist);
}


void ma_common_objects_deinit(ma_common_objects_t *common_objects)
{
	MA_LOG(ma_services_logger, MA_LOG_SEV_INFO, "cleanup up common objects.");		
	ma_ah_client_service_cancel_active_connection(common_objects->agent_handler);
    ma_ah_client_service_release(common_objects->agent_handler);

    ma_net_client_service_stop(common_objects->net_service);
    ma_net_client_service_release(common_objects->net_service);

    release_macrypto_object(MA_CRYPTO_MODE_NON_FIPS, MA_CRYPTO_ROLE_USER, MA_CRYPTO_AGENT_MANAGED, common_objects->crypto_object);

	ma_msgbus_deinitialize();    	
}


//service base object.
typedef struct ma_service_object_s{
	ma_common_objects_t *common_objects;
	ma_service_t *service_obj;
    ma_spipe_decorator_t *service_decorator;
    ma_spipe_handler_t *service_handler;
}ma_service_object_t;

void ma_service_object_service_start(ma_service_object_t *service)
{	
	MA_LOG(ma_services_logger, MA_LOG_SEV_INFO, "Starting service %d", ma_service_start(service->service_obj));;
	ma_ah_client_service_register_spipe_decorator(service->common_objects->agent_handler, service->service_decorator);
	ma_ah_client_service_register_spipe_handler(service->common_objects->agent_handler, service->service_handler);		
}

void ma_service_object_service_stop(ma_service_object_t *service)
{
	ma_ah_client_service_unregister_spipe_decorator(service->common_objects->agent_handler, service->service_decorator);
	ma_ah_client_service_unregister_spipe_handler(service->common_objects->agent_handler, service->service_handler);
	MA_LOG(ma_services_logger, MA_LOG_SEV_INFO, "Stopping service %d", ma_service_stop(service->service_obj));
}

//event service.
typedef struct ma_event_service_object_s{
	ma_service_object_t base;
	ma_db_t *db_handle;
	ma_ds_t *ds_handle;	
	ma_ds_database_t *ds_database_handle;
}ma_event_service_object_t;
	
ma_service_object_t *ma_event_service_object_create(ma_common_objects_t *common_objects)
{		
	//ma_ds_database_t *ds_database_handle = NULL;	
	ma_event_service_object_t *event_service  = (ma_event_service_object_t*) calloc(1, sizeof(ma_event_service_object_t) );

	MA_LOG(ma_services_logger, MA_LOG_SEV_INFO, "Creating event service object.");		

	unlink("Events.db");
    ma_db_intialize();
    ma_db_open("Events.db", &event_service->db_handle);
    ma_ds_database_open(event_service->db_handle, "MyTestEvents", &event_service->ds_database_handle);
    event_service->ds_handle = (ma_ds_t *)event_service->ds_database_handle;
	
	ma_event_service_create(common_objects->bus_loop, event_service->ds_handle, common_objects->agent_handler, (ma_event_service_t**)&event_service->base.service_obj);
	ma_event_service_set_logger((ma_event_service_t*)event_service->base.service_obj, ma_services_logger);						
	ma_event_service_get_spipe_decorator((ma_event_service_t*)event_service->base.service_obj, &event_service->base.service_decorator);
	ma_event_service_get_spipe_handler((ma_event_service_t*)event_service->base.service_obj, &event_service->base.service_handler);

	event_service->base.common_objects = common_objects;
	return (ma_service_object_t*)event_service;
}

void ma_event_service_object_release(ma_service_object_t *service)
{
	ma_event_service_object_t *event_service  = (ma_event_service_object_t*)service;
	MA_LOG(ma_services_logger, MA_LOG_SEV_INFO, "Releasing event service object.");		
	ma_event_service_release((ma_event_service_t*)event_service->base.service_obj);
	ma_ds_database_release(event_service->ds_database_handle);
    ma_db_close(event_service->db_handle);
    ma_db_deintialize();
	unlink("Events.db");
}



//all services.
struct ma_services_s{
	ma_common_objects_t common_objects;
	ma_service_object_t *event_service;
	ma_service_object_t *property_service;
	ma_service_object_t *policy_service;
	ma_services_spipe_hook_t spipe_hook;
};

static void create_services(struct ma_services_s *all_services)
{
	ma_common_objects_init(&all_services->common_objects);
	ma_services_spipe_hook_init(&all_services->spipe_hook);
	
	//create all services..
	all_services->event_service = ma_event_service_object_create(&all_services->common_objects);
}

static void start_services(struct ma_services_s *all_services)
{
	MA_LOG(ma_services_logger, MA_LOG_SEV_INFO, "Starting all services.");		
	//start all services..
	ma_service_object_service_start(all_services->event_service);

	//start hooks
	ma_ah_client_service_register_spipe_decorator(all_services->common_objects.agent_handler, &all_services->spipe_hook.hook_decorator);	
	ma_ah_client_service_register_spipe_handler(all_services->common_objects.agent_handler, &all_services->spipe_hook.hook_handler);	
}

static void stop_services(struct ma_services_s *all_services)
{
	MA_LOG(ma_services_logger, MA_LOG_SEV_INFO, "Stopping all services.");		
	//stop all services..
	ma_service_object_service_stop(all_services->event_service);


	//stops hooks
	ma_ah_client_service_unregister_spipe_decorator(all_services->common_objects.agent_handler, &all_services->spipe_hook.hook_decorator);	
	ma_ah_client_service_unregister_spipe_handler(all_services->common_objects.agent_handler, &all_services->spipe_hook.hook_handler);		
}

static void release_services(struct ma_services_s *all_services)
{
	//release all services..
	MA_LOG(ma_services_logger, MA_LOG_SEV_INFO, "Releasing all services.");		
	ma_event_service_object_release(all_services->event_service);

	ma_common_objects_deinit(&all_services->common_objects);
}

struct control{
	uv_timer_t exit_trigger;
	ma_event_loop_t *client_loop;
};
static void timer_exit(uv_timer_t *handle, int status){
	struct control *ctl = (struct control*)handle->data;
	printf("\n\n .......\nexisting......... \n\n");
	uv_timer_stop(&ctl->exit_trigger);
}

int main()
{
	struct control  ctl;	
	struct ma_services_s services;
	uv_loop_t *loop = NULL;

	ma_console_logger_create((ma_console_logger_t**)&ma_services_logger);

	MA_LOG(ma_services_logger, MA_LOG_SEV_INFO, "Starting services.");		

	ma_event_loop_create(&ctl.client_loop);
	ma_event_loop_get_loop_impl(ctl.client_loop, &loop);

	printf("\n\n maloop ref count %d \n\n", loop->active_handles);

	//start the exit timer
	uv_timer_init(loop, &ctl.exit_trigger);
	ctl.exit_trigger.data = &ctl;
	uv_timer_start(&ctl.exit_trigger, timer_exit, 2000000, 0);

	//create and start the service
	create_services(&services);
	start_services(&services);
	
	
	ma_event_loop_run(ctl.client_loop);

	MA_LOG(ma_services_logger, MA_LOG_SEV_INFO, "Stopping services.");		
	//stop & release the services..
	stop_services(&services);
	release_services(&services);
	ma_event_loop_release(ctl.client_loop);
	
	ma_logger_release(ma_services_logger);	
}





