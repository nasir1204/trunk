#include "ma/ma_common.h"
#include "ma/ma_variant.h"
#include "ma/internal/ma_macros.h"
#include "ma/logger/ma_logger.h"
#include "ma/internal/utils/macrypto/ma_crypto.h"
#include "ma/internal/utils/spipe/ma_spipe_package_defs.h"
#include "ma/internal/utils/datastructures/ma_stream.h"
#include "ma/internal/utils/datastructures/ma_bytebuffer.h"
#include "ma/ma_message.h"
#include "ma/ma_log.h"
#include "ma/internal/services/ahclient/ma_spipe_decorator.h"
#include "ma/internal/services/ahclient/ma_spipe_handler.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#define MA_TOPIC_SPIPE_PUBLISH	                  "publish.spipe.pkg"
#define MA_SPIPE_PROP_IS_SEND_PKG                 "SendPkg"
#define MA_SPIPE_PROP_RET_STATUS                  "ErrorCode"
#define MA_SPIPE_PROP_RESPONSE_CODE				  "ResponseCode"   
#define MA_SPIPE_INFO_TABLE						  "InfoTable"   
#define MA_SPIPE_DATA_TABLE					      "DataTable"   

#define MA_GUID_SIZE					   64
#define MA_MAX_COMPUTER_NAME_SIZE		   80
#define MA_MAX_SIGNATURE_SIZE			   256
#define MA_SPIPE_HEADER_SIZE			   234

struct ma_spipe_header_s{
	unsigned char sig[2];
	ma_uint32_t spipe_version;
	ma_uint32_t datablock_offset;
	ma_uint32_t datablock_data_count;
	ma_uint32_t reserved;
	ma_uint32_t datablock_size;
	unsigned char sender_guid[MA_GUID_SIZE+1];
	unsigned char package_guid[MA_GUID_SIZE+1];
	ma_uint32_t package_type;
	unsigned char computer_name[MA_MAX_COMPUTER_NAME_SIZE+1];	
	ma_bytebuffer_t *packedbuffer;	/*after packing.*/
};

struct ma_spipe_infos_s {
	ma_uint32_t element_count;
	ma_uint32_t binary_size;	
	ma_table_t *table;		
	ma_bytebuffer_t *packed_buffer;	/* after packing */	
};

struct ma_spipe_datas_s {
	struct ma_spipe_infos_s base;	
	ma_uint16_t packing_index;	 
};


struct ma_spipe_package_s {
	struct ma_spipe_header_s pkgheader;				/* Spipe pkg header. */
	struct ma_spipe_infos_s infos;					/* Holds info details. */
	struct ma_spipe_datas_s datas;					/* Holds data details. */			
	ma_crypto_t *crypto;                            /* Crypto for sign verify. */
};


/* spipe decorator*/
static ma_error_t spipe_decorator_decorate(ma_spipe_decorator_t *decorator, ma_spipe_priority_t, ma_spipe_package_t *pkg);
static ma_error_t spipe_decorator_get_spipe_priority(ma_spipe_decorator_t *decorator, ma_spipe_priority_t *priority);
static ma_error_t spipe_decorator_destroy(ma_spipe_decorator_t *decorator);

static struct ma_spipe_decorator_methods_s decorator_methods = {
	&spipe_decorator_decorate,
	&spipe_decorator_get_spipe_priority,
	&spipe_decorator_destroy
};

/* spipe handler*/
static ma_error_t spipe_handler_on_spipe_response(ma_spipe_handler_t *handler, const ma_spipe_response_t *spipe_response);
static ma_error_t spipe_handler_destroy(ma_spipe_handler_t *handler);

static struct ma_spipe_handler_methods_s handler_methods = {
	&spipe_handler_on_spipe_response,
	&spipe_handler_destroy	
};

typedef struct ma_services_spipe_hook_s{	
	/*event service decorator*/
	ma_spipe_decorator_t hook_decorator;

	/*event service handler*/
	ma_spipe_handler_t hook_handler;	
}ma_services_spipe_hook_t;

static void ma_services_spipe_hook_init(ma_services_spipe_hook_t *hook){
	ma_spipe_decorator_init(&hook->hook_decorator, &decorator_methods, hook); 
	ma_spipe_handler_init(&hook->hook_handler, &handler_methods, hook); 
}

static void ma_services_spipe_hook_deinit(ma_services_spipe_hook_t *hook){	
}

static void publish_spipe_package(ma_bool_t is_send, ma_error_t error, int res_code, ma_spipe_package_t *pkg){
	ma_error_t rc = MA_OK;
	ma_message_t *msg = NULL;
	if (MA_OK == (rc = ma_message_create(MA_MESSAGE_TYPE_PUBLISH, &msg) ) )
	{		
		char buffer[21] = {0};
		ma_message_set_property(msg, MA_SPIPE_PROP_IS_SEND_PKG, is_send ? "1" :"0");
		
		MA_MSC_SELECT(_snprintf, snprintf)(buffer, 21, "%d", error);
		ma_message_set_property(msg, MA_SPIPE_PROP_RET_STATUS, buffer);

		MA_MSC_SELECT(_snprintf, snprintf)(buffer, 21, "%d", res_code);
		ma_message_set_property(msg, MA_SPIPE_PROP_RESPONSE_CODE, buffer);
		

		if(pkg)
		{
			ma_table_t *payload_tbl = NULL;
			ma_variant_t *payload = NULL;
			MA_LOG(ma_services_logger, MA_LOG_SEV_INFO, "Valid spipe pkg to publish.");		
			if( MA_OK == ma_table_create(&payload_tbl) && 
				MA_OK == ma_variant_create_from_table(payload_tbl, &payload) ) 
			{				

				//add info
				{
					ma_variant_t *info = NULL;
					ma_variant_create_from_table(pkg->infos.table, &info);
					ma_table_add_entry(payload_tbl, MA_SPIPE_INFO_TABLE, info );			
					ma_variant_release(info);
				}
				//add datas
				{
					ma_variant_t *datas = NULL;
					ma_variant_create_from_table(pkg->datas.base.table, &datas);
					ma_table_add_entry(payload_tbl, MA_SPIPE_DATA_TABLE, datas );			
					ma_variant_release(datas);
				}
				ma_message_set_payload(msg, payload);
			}
		
			if(MA_OK != ma_msgbus_publish(MA_TOPIC_SPIPE_PUBLISH, MSGBUS_CONSUMER_REACH_OUTPROC,  msg) ){
				MA_LOG(ma_services_logger, MA_LOG_SEV_ERROR, "Failed to published spipe pkg.");		
			}
			ma_table_release(payload_tbl);
			ma_variant_release(payload);			
		}		
	}	
}
/*Implement spipe decorator*/
static  ma_error_t spipe_decorator_decorate(ma_spipe_decorator_t *decorator, ma_spipe_priority_t priority, ma_spipe_package_t *pkg){
	publish_spipe_package(MA_TRUE, MA_OK, -1, pkg);
}

static ma_error_t spipe_decorator_get_spipe_priority(ma_spipe_decorator_t *decorator, ma_spipe_priority_t *priority){
	*priority = MA_SPIPE_PRIORITY_NONE;
	return MA_OK;
}

static  ma_error_t spipe_decorator_destroy(ma_spipe_decorator_t *decorator){		
	return MA_OK;	
}

/* Implement spipe handler */
static ma_error_t spipe_handler_on_spipe_response(ma_spipe_handler_t *handler, const ma_spipe_response_t *spipe_response){
	publish_spipe_package(MA_TRUE, spipe_response->net_service_error_code, spipe_response->http_response_code, spipe_response->respond_spipe_pkg );
	return MA_OK;
}

static  ma_error_t spipe_handler_destroy(ma_spipe_handler_t *handler){		
	return MA_OK;	
}



