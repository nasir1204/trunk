#include "ma/datastore/ma_ds_ini.h"
#include "ma/internal/utils/platform/ma_dl.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/ma_common.h"
#include "ma/ma_datastore.h" 
#include "ma/internal/utils/database/ma_db.h"
#include "ma/internal/utils/database/ma_db_recordset.h"
#include "ma/internal/utils/database/ma_db_statement.h"
#include "ma/logger/ma_console_logger.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include "ma/internal/utils/text/ma_utf8.h"

#include <stdio.h>
#include <Windows.h>
#include <tchar.h>

#if !defined(MA_WINDOWS)
typedef void *HMODULE;
#endif

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "ma_res_to_ini"
#define MA_MAX_STRING_ID_COUNT  3000

static const char *program;
static ma_logger_t *logger = NULL ;
static ma_ds_t *g_ds = NULL;
const wchar_t section[] = L"STRINGTABLE";
const char resource_lib[] = "AgentRes.dll";
const char resourc_ini[] = "AgentRes.ini";
const wchar_t wresourc_ini[] = L"AgentRes.ini";

static ma_error_t load_resource_library(const char *path, ma_lib_t *lib);
static ma_error_t close_resource_library(ma_lib_t *lib);
static ma_error_t open_resource_ini(const char *path, wchar_t *out_path);
static ma_error_t close_resource_ini(ma_ds_t *ds);
static ma_error_t build_ini_from_single_resource(ma_lib_t *res, wchar_t *out_path);
static ma_error_t build_ini_from_dual_resource(ma_lib_t *res_key, ma_lib_t *res_value, wchar_t *out_path);
static void usage();
static unsigned ma_hash_string(const char *key, int len);
static void substitue_lf_to_backslash(wchar_t *str);

static wchar_t dir_path[MA_MAX_PATH_LEN] = {0};

unsigned ma_hash_string(const char *key, int len) {
    if(key) {
        unsigned char *p = (unsigned char*)key;
        unsigned h = 0;
        int i;

        for ( i = 0; i < len; i++ )
            h = 33 * h ^ p[i];

        return h;
    }
    return 0;
}

LPTSTR ma_get_res_wstring(HMODULE hInstResDLL, UINT    uiStrID) {
    static int   iCurNdx = 0;
    static TCHAR szResString[16][3000];
    iCurNdx = ((iCurNdx + 1) % 16);

    if (!LoadString(hInstResDLL, uiStrID, szResString[iCurNdx], sizeof(szResString[iCurNdx]) / sizeof(TCHAR))) {
        szResString[iCurNdx][0] = TEXT('\0');   // If no string found, make the string empty
    }
    return (LPTSTR)szResString[iCurNdx];
}

const char* ma_get_res_string(HMODULE hInstResDLL, UINT uiStrID) {
    static int  iCurNdx = 0;
    static CHAR szResString[16][3000] = {0};
    iCurNdx = ((iCurNdx + 1) % 16);

    if (!LoadStringA(hInstResDLL, uiStrID, szResString[iCurNdx], sizeof(szResString[iCurNdx]) / sizeof(CHAR))) {
        szResString[iCurNdx][0] = '\0';   // If no string found, make the string empty
    }

    return (LPSTR)szResString[iCurNdx];
}

void usage() {
    fprintf(stderr, "usage: ma_restoini dir1 [dir2]\n\
                     dir1 -- resource library directory\n\
                     dir2 -- resource library directory\n\
                     \n");
}

ma_error_t load_resource_library(const char *path, ma_lib_t *lib) {
    if(path && lib) {
        ma_error_t err = MA_OK;
        ma_temp_buffer_t tmp = MA_TEMP_BUFFER_INIT;
        size_t len = strlen(path)+strlen(resource_lib)+1;
        char *res_path = NULL;

        ma_temp_buffer_reserve(&tmp, len);
        MA_MSC_SELECT(_snprintf, snprintf)(res_path = (char*)ma_temp_buffer_get(&tmp), len, "%s%c%s", path, MA_PATH_SEPARATOR,  resource_lib);
        MA_LOG(logger, MA_LOG_SEV_DEBUG, "%s library(%s) loading...", program, res_path);
        if(MA_OK == (err = ma_dl_open(res_path, 0, lib))) {
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "%s library(%s) loaded succesfully", program, res_path);
        } else
            MA_LOG(logger, MA_LOG_SEV_ERROR, "%s failed to open resource library(%s), errorcode(%d)", program, res_path, err);
        ma_temp_buffer_uninit(&tmp);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t build_ini_from_single_resource(ma_lib_t *res, wchar_t *out_path) {
    if(res) {
        ma_error_t err = MA_OK;
        unsigned int itr = 0;

        for(itr = 1; itr < MA_MAX_STRING_ID_COUNT; ++itr) {
            const char *key = ma_get_res_string((HMODULE)res->handle, itr);
            const wchar_t *str = ma_get_res_wstring((HMODULE)res->handle, itr);
            if(key && str && wcscmp(str, L"")) {
                wchar_t tmp[256] = {0};
                unsigned int hash = ma_hash_string(key, strlen(key));

                MA_MSC_SELECT(_snwprintf, snwprintf)((wchar_t*)tmp, 256, L"%u", hash);
                if(wcsstr(str, L"\n")) {
                    wchar_t *pstr = NULL;

                    if((pstr = (wchar_t*)calloc(wcslen(str)*2, sizeof(wchar_t)))) {
                        wcscpy(pstr, str);
                        substitue_lf_to_backslash(pstr);
                        WritePrivateProfileString(section, tmp, pstr, out_path);
                        free(pstr);
                    }
                } else 
                    WritePrivateProfileString(section, tmp, str, out_path);

            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t build_ini_from_dual_resource(ma_lib_t *res_key, ma_lib_t *res_value, wchar_t *out_path) {
    if(res_key && res_value) {
        ma_error_t err = MA_OK;
        unsigned int itr = 0;

        for(itr = 1; itr < MA_MAX_STRING_ID_COUNT; ++itr) {
            const char *key = ma_get_res_string((HMODULE)res_key->handle, itr);
            const wchar_t *str = ma_get_res_wstring((HMODULE)res_value->handle, itr);

            if(key && str && wcscmp(str, L"")) {
                ma_temp_buffer_t btmp = {0};
                wchar_t tmp[256] = {0};
                unsigned int hash = ma_hash_string(key, strlen(key));

                ma_temp_buffer_init(&btmp);

                MA_MSC_SELECT(_snwprintf, snwprintf)((wchar_t*)tmp, 256, L"%u", hash);
                
                if(wcsstr(str, L"\n")) {
                    wchar_t *pstr = NULL;

                    if((pstr = (wchar_t*)calloc(wcslen(str)*2, sizeof(wchar_t)))) {
                        wcscpy(pstr, str);
                        substitue_lf_to_backslash(pstr);
                        WritePrivateProfileString(section, tmp, pstr, out_path);
                        free(pstr);
                    }
                } else 
                    WritePrivateProfileString(section, tmp, str, out_path);

                ma_temp_buffer_uninit(&btmp);
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t open_resource_ini(const char *path, wchar_t *out_path) {
    if(path) {
        ma_error_t err = MA_OK;
        ma_temp_buffer_t btmp = MA_TEMP_BUFFER_INIT;
        char *res_path = NULL;

        ma_convert_utf8_to_wide_char(path, &btmp);
        MA_MSC_SELECT(_snwprintf, snwprintf)(out_path, MAX_PATH, L"%s%c%s", (wchar_t *)ma_temp_buffer_get(&btmp), L'\\',  wresourc_ini);

        ma_temp_buffer_uninit(&btmp);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t close_resource_library(ma_lib_t *lib) {
    if(lib) {
        ma_dl_close(lib);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t close_resource_ini(ma_ds_t *ds) {
    return ds ? ma_ds_ini_release((ma_ds_ini_t*)ds) : MA_ERROR_INVALIDARG;
}

int main (int argc, char *argv[]) {
    ma_error_t err = MA_OK;
    ma_lib_t lib = {0};

    program = argv[0];
    if(MA_OK == (err = ma_console_logger_create((ma_console_logger_t**)&logger))) {
        if (argc >= 2) {
            const char *key_path = argv[1];
            const char *value_path = argv[2];
            ma_lib_t key_handle = {0};

            if(key_path && value_path) {
                if(!strcmp(key_path, value_path)) {
                    /* both path are identical */
                    if(MA_OK == (err = load_resource_library(key_path, &key_handle))) {
                        if(MA_OK == (err = open_resource_ini(key_path, dir_path))) {
                            if(MA_OK != (err = build_ini_from_single_resource(&key_handle, dir_path))) {
                                MA_LOG(logger, MA_LOG_SEV_ERROR, "%s resource ini creation failed, errorcode(%d)", program, err); 
                            }
                            (void)close_resource_ini(g_ds);
                        }
                        (void)close_resource_library(&key_handle);
                    }
                } else {
                    ma_lib_t value_handle = {0};
                    /* both are different */
                    if(MA_OK == (err = load_resource_library(key_path, &key_handle))) {
                        if(MA_OK == (err = load_resource_library(value_path, &value_handle))) {
                            if(MA_OK == (err = open_resource_ini(value_path, dir_path))) {
                                if(MA_OK != (err = build_ini_from_dual_resource(&key_handle, &value_handle, dir_path))) {
                                    MA_LOG(logger, MA_LOG_SEV_ERROR, "%s resource ini creation failed, errorcode(%d)", program, err); 
                                }
                                (void)close_resource_ini(g_ds);
                            }
                            (void)close_resource_library(&value_handle);
                        }
                        (void)close_resource_library(&key_handle);
                    }
                }
            } else if (key_path && !value_path) {
                /* both path are identical */
                if(MA_OK == (err = load_resource_library(key_path, &key_handle))) {
                    if(MA_OK == (err = open_resource_ini(key_path, dir_path))) {
                        if(MA_OK != (err = build_ini_from_single_resource(&key_handle, dir_path))) {
                            MA_LOG(logger, MA_LOG_SEV_ERROR, "%s resource ini creation failed, errorcode(%d)", program, err); 
                        }
                        (void)close_resource_ini(g_ds);
                    }
                    (void)close_resource_library(&key_handle);
                }
            } else if(!key_path && value_path) {
                usage();
            } else {
                /* control will never reach here */
            }
        } else usage();

        (void)ma_console_logger_release((ma_console_logger_t*)logger);
    }

    return 1;
}

void substitue_lf_to_backslash(wchar_t *str) {
    if(str) {
        wchar_t *t = NULL;
        wchar_t *pstr = str;

        if((t = (wchar_t*)calloc(wcslen(str) * 2, sizeof(wchar_t)))) {
            wchar_t *pt = t;

            while(pstr && (*pstr != L'\0')) {
                if(*pstr == L'\n') {
                    *t = L'\\';
                    t++;
                    *t = 'n';
                } else if(*pstr == L'\t') {
                    *t = L'\\';
                    t++;
                    *t = 't';
                } else if(*pstr == L'\r') {
                    *t = L'\\';
                    t++;
                    *t = 'r';
                }  else if(*pstr == L'\\') {
                    *t = L'\\';
                    t++;
                    *t = '\\';
                } else
                    *t = *pstr;
                t++;
                pstr++;
            }
            wcscpy(str, pt);
            free(pt);
        }
    }
}
