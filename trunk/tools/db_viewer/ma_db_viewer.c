// ma_policy_extractor.cpp : Defines the exported functions for the DLL application.
//

#include "ma_db_viewer.h"

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#include "ma/ma_common.h"
#include "ma/ma_dispatcher.h"
#include "ma/internal/utils/database/ma_db.h"
#include "ma/internal/utils/xml/ma_xml.h"
#include "ma/internal/utils/xml/ma_xml_node.h"
#include "ma/internal/services/policy/ma_policy_db.h"
#include "ma/internal/utils/datastructures/ma_bytebuffer.h"
#include "ma/internal/services/property/property_xml_generator.h"
#include "ma/internal/defs/ma_property_defs.h"
#include "ma/internal/defs/ma_general_defs.h"
#include "ma/datastore/ma_ds_database.h"


typedef struct ma_policy_viewer_s {
    ma_db_t         *db;
    ma_xml_t        *xml;	
} ma_db_viewer_t;

static ma_db_viewer_t viewer;

typedef enum ma_nodetype_e {
	SECTION_NODE,
	SETTING_NODE
}ma_nodetype_t;

typedef enum ma_viewer_type {
	POLICIES,
	TASKS,	
}ma_viewer_type_t;

typedef enum ma_operation_type{
	ALL_POLICIES,
	ALL_TASKS,
	ALL_PROPERTIES,
	PRODUCT_BASED_POLICIES,
	PRODUCT_BASED_TASKS
}ma_operation_type_t;

void convert_to_upper(char *buffer) {
	int i = 0;
	while(buffer[i] != '\0'){
		buffer[i] = toupper(buffer[i]);
		++i;
	}
}

ma_error_t ma_db_viewer_init(const char *db_file, ma_db_viewer_t *self) {
    ma_error_t rc = MA_OK;	
    if(MA_OK != (rc = ma_db_open(db_file, MA_DB_OPEN_READONLY, &self->db))){
		fprintf(stderr, "Failed to open database %s, error %d", db_file, rc);
		return rc;
	}

	if(MA_OK != (rc = ma_xml_create(&self->xml))){
		fprintf(stderr, "Failed to create xml file, internal error %d", rc);
		return rc;
	}
    
	if(MA_OK != (rc = ma_xml_construct(self->xml))){
		fprintf(stderr, "Failed to construct xml, internal error %d", rc);
		return rc;
	}
	
    return rc;
}

ma_error_t ma_policy_viewer_deinit(ma_db_viewer_t *self) {   

    ma_xml_release(self->xml);

    ma_db_close(self->db);

	return MA_OK;
}

const char *get_str_value(const ma_variant_t *value) {
    ma_buffer_t *buffer;
    const char *str = NULL; size_t size = 0;
    if(MA_OK == ma_variant_get_string_buffer((ma_variant_t *)value, &buffer)) {        
        ma_buffer_get_string(buffer, &str, &size);
        ma_buffer_release(buffer);
    }
    return str;
}

/**
 * @brief : Helper function to retrieve the Sections and Settings data from the supplied variant data
 * and then adds the corresponding xml element node to the parent node. 
 * 
 * @param [in/out] parent_node   Node to which the new node will be added.
 * @param [in]     data          Data containing the Section / Setting information.
 * @param [in]     nodeType      Denotes if we have to pull Section / Settings data from the input data.
 * 
 * @return         MA_OK on success
 *                 MA_ERROR_INVALIDARG on invalid argument passed
 */
ma_error_t add_section_setting_node(ma_xml_node_t **parent_node, ma_variant_t *data, ma_nodetype_t nodeType){
	ma_error_t rc = MA_OK;
	ma_table_t *data_table = NULL;
	ma_array_t *key_arr = NULL;
	size_t array_size = 0;
	int index = 0;
	ma_variant_t *key_var = NULL;
	const char *key = NULL;
	ma_xml_node_t *new_node = NULL;
	ma_variant_t *value_var = NULL;
	const char *node_name = NULL;

	if(!parent_node || !data){
		return MA_ERROR_INVALIDARG;
	}

	// Get table from the input variant data. 
	// Get the table keys in an array
	// Iterate through the key array and for each key, do the following
	// --If the input type is SECTION, add a Section node to the parent node and
	// add the key as the attribute to the newly added Section node. Get the value
	// for the key from the table and pull settings data
	// --If the input type is SETTING, add a Setting node to the parent node and 
	// add the key as the attribute to the newly added Setting node. Get the value 
	// for the key and assign it as the value of the newly added Setting node. 

	if(MA_OK == (rc = ma_variant_get_table(data, &data_table))) {
		if(MA_OK == (rc = ma_table_get_keys(data_table, &key_arr))){
			if(MA_OK == (rc = ma_array_size(key_arr, &array_size))){
				for(index = 1; (size_t)index <= array_size; ++index) {
					if(MA_OK == (rc = ma_array_get_element_at(key_arr, index, &key_var))) {
						key = get_str_value(key_var);

						if(nodeType == SECTION_NODE){
							node_name = "Section";
						}
						else if(nodeType == SETTING_NODE){
							node_name = "Setting";
						}

						if(MA_OK == (rc = ma_xml_node_create(*parent_node,node_name, &new_node))){
							ma_xml_node_attribute_set(new_node, "name", key);

							if((MA_OK == (rc = ma_table_get_value(data_table, key, &value_var))) && value_var) {
								if(nodeType == SECTION_NODE){
									add_section_setting_node(&new_node, value_var, SETTING_NODE);
								}
								else if(nodeType == SETTING_NODE){
									ma_xml_node_set_data(new_node, get_str_value(value_var));									
								}
								(void) ma_variant_release(value_var);
							}
						}
											
						(void) ma_variant_release(key_var);
					}					

				} // end for loop
			}
			(void) ma_array_release(key_arr);
		}		
		(void) ma_table_release(data_table);
	}
	
	return rc;
}

/**
 * @brief : Helper function to retrieve all product id's from task db.
 * 
 * @param [in] db_handle Database handle
 * @param [out] db_record Contains the list of task records for the given product id.
 * @return  MA_OK on success
 *          MA_ERROR_INVALIDARG on invalid argument passed
 */
#define GET_ALL_PRODUCT_STMT "SELECT DISTINCT META_PRODUCT FROM MA_EPO_TASK_ASSIGNMENT"
ma_error_t get_all_products_from_task_db(ma_db_t *db_handle, ma_db_recordset_t **db_record)  {
    if(db_handle && db_record) {
        ma_db_statement_t *db_stmt = NULL;
        ma_error_t rc = MA_OK;
        int tvalue= 0;     
           
        if( MA_OK == (rc = ma_db_statement_create(db_handle, GET_ALL_PRODUCT_STMT, &db_stmt)) ) 
        {
            rc = ma_db_statement_execute_query(db_stmt, db_record) ;            
            ma_db_statement_release(db_stmt) ;
        }

        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

/**
 * @brief : Helper function to retrieve all task details for a given product id. 
 * 
 * @param [in] db_handle Database handle
 * @param [in] product   Product id 
 * @param [out] db_record Contains the list of task records for the given product id.
 * @return  MA_OK on success
 *          MA_ERROR_INVALIDARG on invalid argument passed
 */
#define GET_ALL_PRODUCT_TASKS_STMT "SELECT DISTINCT META.META_PRODUCT, MET.TASK_OBJECT_ID, MET.TASK_ID, MET.TASK_NAME, MET.TASK_TYPE, MET.TASK_PRIORITY, MET.TIMESTAMP, MET.TASK_DATA FROM MA_EPO_TASK MET, MA_EPO_TASK_ASSIGNMENT META WHERE MET.TASK_OBJECT_ID=META.TASK_OBJECT_ID AND (META.META_PRODUCT =  ?)"
ma_error_t db_get_product_tasks(ma_db_t *db_handle, const char *product, ma_db_recordset_t **db_record)  {
    if(db_handle && product) {
        ma_db_statement_t *db_stmt = NULL;
        ma_error_t rc = MA_OK;
        int tvalue= 0;     
           
        if( MA_OK == (rc = ma_db_statement_create(db_handle, GET_ALL_PRODUCT_TASKS_STMT, &db_stmt)) ) 
        {
            if( MA_OK == (rc = ma_db_statement_set_string(db_stmt, 1, 1, product, (product)?strlen(product):0)))
            {
                rc = ma_db_statement_execute_query(db_stmt, db_record) ;
            }
            ma_db_statement_release(db_stmt) ;
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

/**
 * @brief : Retrieves all the policy related data from the database for the given product id, creates 
 * respective xml nodes and appends it to the parent node passed in.
 * 
 * @param [in/out] parent_node Node to which the policy data needs to be added
 * @param [in]     db          Database handle
 * @param [in]     product_id  Product id
 * @return  MA_OK on success
 *          MA_ERROR_INVALIDARG on invalid argument passed 
 */
ma_error_t extract_product_policy_details(ma_xml_node_t **parent_node, ma_db_t *db, const char *product_id){
    ma_xml_node_t *prod_node = NULL;
	ma_error_t rc = MA_OK;
	ma_db_recordset_t *record_set = NULL;

	if(!parent_node || !db || !product_id){
		return MA_ERROR_INVALIDARG;
	}
		
	ma_xml_node_create(*parent_node,"Product", &prod_node);
	ma_xml_node_attribute_set(prod_node, "SoftwareID", product_id);	
	
	// Get the record set from the database for the given Product Id
	if(MA_OK == (rc = db_get_product_policies(db, product_id, NULL, NULL, NULL, &record_set))){
					
		ma_xml_node_t *pso_node = NULL;
		
		// Iterate through each record found and convert the fields to xml 
		while((MA_OK == rc) && record_set && (MA_ERROR_NO_MORE_ITEMS != (rc = ma_db_recordset_next(record_set)))) {
			const char *feature = NULL, *category = NULL, *type = NULL, *name = NULL, *pso_name = NULL, *pso_param_int = NULL, *pso_param_str = NULL;
			ma_variant_t *sec_var = NULL;
			
			// Following query is used to fetch the data from DB
			//
			// #define GET_POLICIES_FOR_PRODUCT_STMT "SELECT DISTINCT MPO.PO_FEATURE, MPO.PO_CATEGORY, MPO.PO_TYPE, 
			// MPO.PO_NAME, MPSO.PSO_NAME, MPSO.PSO_PARAM_INT, MPSO.PSO_PARAM_STR, MPSO.PSO_DATA FROM MA_POLICY_OBJECT MPO, 
			// MA_POLICY_PRODUCT_ASSIGNMENT MPPA, MA_POLICY_SETTINGS_OBJECT MPSO WHERE MPO.PO_ID = MPSO.PO_ID AND MPO.PO_ID = MPPA.PO_ID AND MPPA.PPA_PRODUCT = ? AND
			// ( ? = 0 OR (? = 1 AND MPSO.PSO_TIMESTAMP = ?)) AND MPPA.PPA_METHOD = ? AND MPPA.PPA_PARAM = ? AND MPO.PO_CATEGORY != ? COLLATE NOCASE"
							
			if((MA_OK == (rc =	ma_db_recordset_get_string(record_set, 1, &feature))) &&
				(MA_OK == (rc = ma_db_recordset_get_string(record_set, 2, &category))) &&
                (MA_OK == (rc = ma_db_recordset_get_string(record_set, 3, &type))) &&
                (MA_OK == (rc = ma_db_recordset_get_string(record_set, 4, &name))) &&
				(MA_OK == (rc = ma_db_recordset_get_string(record_set, 5, &pso_name))) &&
				(MA_OK == (rc = ma_db_recordset_get_variant(record_set, 8, MA_VARTYPE_TABLE, &sec_var)))) {
					(void) ma_db_recordset_get_string(record_set, 6, &pso_param_int);
					(void) ma_db_recordset_get_string(record_set, 7, &pso_param_str);

					if(MA_OK == (rc = ma_xml_node_create(prod_node,"EPOPolicySettings", &pso_node))){
						ma_xml_node_attribute_set(pso_node, "type", type);
						ma_xml_node_attribute_set(pso_node, "category", category);						
						ma_xml_node_attribute_set(pso_node, "feature", feature);
						ma_xml_node_attribute_set(pso_node, "name", name);						
						
					}
								
					if(sec_var){
						if(MA_OK != (rc = add_section_setting_node(&pso_node, sec_var, SECTION_NODE))){							
							fprintf(stderr, "Failed to read section/setting data, error = %d \n", rc);
							(void)ma_variant_release(sec_var);
							break;
						}
						(void)ma_variant_release(sec_var);
					}
			}
		
		}		

		if(record_set) (void)ma_db_recordset_release(record_set);
	}

	if(rc == MA_ERROR_NO_MORE_ITEMS)
	{
		rc = MA_OK;
	}
	return rc;
}

/**
 * @brief : Retrieves all the task related data from the database for the given product id, creates 
 * respective xml nodes and appends it to the parent node passed in.
 * 
 * @param [in/out] parent_node Node to which the policy data needs to be added
 * @param [in]     db          Database handle
 * @param [in]     product_id  Product id
 * @return  MA_OK on success
 *          MA_ERROR_INVALIDARG on invalid argument passed 
 */
ma_error_t extract_product_task_details(ma_xml_node_t **parent_node, ma_db_t *db, const char *product_id){
    ma_xml_node_t *prod_node = NULL;
	ma_error_t rc = MA_OK;
	ma_db_recordset_t *record_set = NULL;

	if(!parent_node || !db || !product_id){
		return MA_ERROR_INVALIDARG;
	}
	
	ma_xml_node_create(*parent_node,"Product", &prod_node);
	ma_xml_node_attribute_set(prod_node, "SoftwareID", product_id);	
	
	
	// Get the record set from the database for the given Product Id
	if(MA_OK == (rc = db_get_product_tasks(db, product_id, &record_set))){
					
		ma_xml_node_t *task_node = NULL;
		
		// Iterate through each record found and convert the fields to xml 
		while((MA_OK == rc) && record_set && (MA_ERROR_NO_MORE_ITEMS != (rc = ma_db_recordset_next(record_set)))) {
			const char *id = NULL, *name = NULL, *type = NULL, *priority = NULL, *timestamp = NULL;			
			ma_variant_t *sec_var = NULL;

			// Following query is used to fetch the data from DB
			//
			// "SELECT DISTINCT META.META_PRODUCT, MET.TASK_OBJECT_ID, MET.TASK_ID, MET.TASK_NAME, MET.TASK_TYPE, MET.TASK_PRIORITY, 
			// MET.TIMESTAMP, MET.TASK_DATA FROM MA_EPO_TASK MET, MA_EPO_TASK_ASSIGNMENT META WHERE 
			// MET.TASK_OBJECT_ID=META.TASK_OBJECT_ID AND (META.META_PRODUCT =  ?))"
	
			if((MA_OK == (rc =	ma_db_recordset_get_string(record_set, 3, &id))) &&
				(MA_OK == (rc = ma_db_recordset_get_string(record_set, 4, &name))) &&
                (MA_OK == (rc = ma_db_recordset_get_string(record_set, 5, &type))) &&
                (MA_OK == (rc = ma_db_recordset_get_string(record_set, 6, &priority))) &&
				(MA_OK == (rc = ma_db_recordset_get_string(record_set, 7, &timestamp))) &&
				(MA_OK == (rc = ma_db_recordset_get_variant(record_set, 8, MA_VARTYPE_TABLE, &sec_var)))) {

					if(MA_OK == (rc = ma_xml_node_create(prod_node,"Task", &task_node))){
						ma_xml_node_attribute_set(task_node, "timestamp", timestamp);
						ma_xml_node_attribute_set(task_node, "priority", priority);
						ma_xml_node_attribute_set(task_node, "type", type);
						ma_xml_node_attribute_set(task_node, "name", name);
						ma_xml_node_attribute_set(task_node, "id", id);
						
					}

					if(sec_var){
						if(MA_OK != (rc = add_section_setting_node(&task_node, sec_var, SECTION_NODE))){							
							fprintf(stderr, "Failed to read section/setting data, error = %d \n", rc);
							(void)ma_variant_release(sec_var);
							break;
						}
						(void)ma_variant_release(sec_var);
					}

			}
		
		}		

		if(record_set) (void)ma_db_recordset_release(record_set);
	}

	if(rc == MA_ERROR_NO_MORE_ITEMS)
	{
		rc = MA_OK;
	}
	return rc;
}

ma_error_t extract_all_policies(ma_xml_node_t **parent_node, ma_db_t *db){
	ma_error_t rc = MA_OK;
	ma_db_recordset_t *recordset = NULL;

	if(!parent_node || !db){
		return MA_ERROR_INVALIDARG;
	}

	// Get all the Product Id's and then extract the policy details for each. 	
	if(MA_OK == db_get_all_products(db, 1, &recordset)) {
		while(MA_OK == ma_db_recordset_next(recordset)) {
			const char *product_id = NULL;
			(void)ma_db_recordset_get_string(recordset, 1, &product_id);
			if(product_id) {
				if(MA_OK != (rc = extract_product_policy_details(parent_node, db, product_id))){
					fprintf(stderr, "Failed to retrieve policy details for product id %s, err = %d \n", product_id, rc);
				}						
			}
		}

		if(recordset) (void) ma_db_recordset_release(recordset);
	}

	return rc;
}

ma_error_t extract_all_tasks(ma_xml_node_t **parent_node, ma_db_t *db){
	ma_error_t rc = MA_OK;
	ma_db_recordset_t *recordset = NULL;

	if(!parent_node || !db){
		return MA_ERROR_INVALIDARG;
	}

	// Get all the Product Id's and then extract the task details for each. 	
	if(MA_OK == get_all_products_from_task_db(db, &recordset)) {
		while(MA_OK == ma_db_recordset_next(recordset)) {
			const char *product_id = NULL;
			(void)ma_db_recordset_get_string(recordset, 1, &product_id);
			if(product_id) {
				if(MA_OK != (rc = extract_product_task_details(parent_node, db, product_id))){
					fprintf(stderr, "Failed to retrieve task details for product id %s, err = %d \n", product_id, rc);
				}						
			}
		}

		if(recordset) (void) ma_db_recordset_release(recordset);
	}

	return rc;
}


ma_error_t get_policy_task_data(const char * db_file_path, const char* product_id, char **data, int* buffLen, ma_operation_type_t operation){
	ma_error_t rc = MA_OK;    	
	ma_db_viewer_t *self = &viewer;
	ma_bytebuffer_t *byte_buff = NULL;
	memset(self, 0 , sizeof(ma_db_viewer_t));
	

	if(MA_OK == (rc = ma_db_viewer_init(db_file_path, self))) {		
			
		ma_xml_node_t *root_node = ma_xml_get_node(self->xml);
		ma_xml_node_t *node = NULL;

		switch(operation){
			case ALL_POLICIES:				
				ma_xml_node_create(root_node,"PolicyRoot", &node);
				if(MA_OK != (rc = extract_all_policies(&node, self->db))){
					fprintf(stderr, "Failed to retrieve policy details, err = %d \n", rc);
					ma_policy_viewer_deinit(self);
					return rc;
				}
				break;

			case ALL_TASKS:
				ma_xml_node_create(root_node,"TaskRoot", &node);
				if(MA_OK != (rc = extract_all_tasks(&node, self->db))){
					fprintf(stderr, "Failed to retrieve task details, err = %d \n", rc);
					ma_policy_viewer_deinit(self);
					return rc;
				}
				break;

			case PRODUCT_BASED_POLICIES:
				ma_xml_node_create(root_node,"PolicyRoot", &node);
				if(MA_OK != (rc = extract_product_policy_details(&node, self->db, product_id))){
					fprintf(stderr, "Failed to retrieve policy details for product %s, err = %d \n",product_id, rc);
					ma_policy_viewer_deinit(self);
					return rc;
				}
				break;

			case PRODUCT_BASED_TASKS:
				ma_xml_node_create(root_node,"TaskRoot", &node);
				if(MA_OK != (rc = extract_product_task_details(&node, self->db, product_id))){
					fprintf(stderr, "Failed to retrieve policy details for product %s, err = %d \n",product_id, rc);
					ma_policy_viewer_deinit(self);
					return rc;
				}
				break;

			default:
				fprintf(stderr, "Invalid operation type %d", operation);
				ma_policy_viewer_deinit(self);
				return MA_ERROR_NOT_SUPPORTED;
				break;
		}
		
		// Convert the xml contents to char buffer		
		if(MA_OK == (rc = ma_bytebuffer_create(1024, &byte_buff))){
			unsigned char *p_temp_buf = NULL;
			size_t size = 0;
			if( (MA_OK == (rc = ma_xml_save_to_buffer (self->xml, byte_buff))) && 
				(p_temp_buf = ma_bytebuffer_get_bytes(byte_buff)) && 
				(size = ma_bytebuffer_get_size(byte_buff))){					
					*data = (char *) calloc(size + 1 ,sizeof(unsigned char));
					if(*data){
						strncpy((char*) *data,(char*) p_temp_buf, size);
						(*data)[size] = '\0';
						*buffLen = size;
						rc = MA_OK;
					}
			}
			ma_bytebuffer_release(byte_buff);
		}

		rc = ma_policy_viewer_deinit(self);
	}
		
	return rc;
}

int get_all_product_policies(const char* db_file_path, char** policyData, int* buffLen){
	ma_error_t rc = MA_OK; 
	if(db_file_path && (strlen(db_file_path) > 0) && 
	   policyData && buffLen){
		   char *tmp_buf = NULL;
	       int buf_size = 0;
		   if(MA_OK == (rc = get_policy_task_data(db_file_path, NULL, &tmp_buf, &buf_size, ALL_POLICIES)) && (buf_size > 0)){			   
			   *policyData = (char *) calloc(buf_size + 1 ,sizeof(unsigned char));
			   if(*policyData){
				   strcpy((char*) *policyData,(char*) tmp_buf);
				   *buffLen = buf_size;
				   rc = MA_OK;
				}

			    //fprintf(stderr, "lenght is %d data is \n %s", *buffLen, *policyData);
				free(tmp_buf);
			}
	}
	else
	{
		rc = MA_ERROR_INVALIDARG;
	}

	return rc;
}


int get_product_policies(const char* db_file_path, char* product_id, char** policyData, int* buffLen){
	ma_error_t rc = MA_OK; 
	if(db_file_path && (strlen(db_file_path) > 0) && 
		product_id && (strlen(product_id) > 0) && 
		policyData && buffLen){
			char *tmp_buf = NULL;
			int buf_size = 0;
			convert_to_upper(product_id);
			if(MA_OK == (rc = get_policy_task_data(db_file_path, product_id, &tmp_buf, &buf_size, PRODUCT_BASED_POLICIES)) && (buf_size > 0)){
				*policyData = (char *) calloc(buf_size + 1 ,sizeof(unsigned char));
				if(*policyData){
					strcpy((char*) *policyData,(char*) tmp_buf);
					*buffLen = buf_size;
					rc = MA_OK;
				}
				free(tmp_buf);
			}
	}
	else
	{
		rc = MA_ERROR_INVALIDARG;
	}
	return rc;
}

int get_all_product_tasks(const char* db_file_path, char** taskData, int* buffLen){
	ma_error_t rc = MA_OK; 
	if(db_file_path && (strlen(db_file_path) > 0) && 
	   taskData && buffLen){	
		   char *tmp_buf = NULL;
		   int buf_size = 0;
		   if(MA_OK == (rc = get_policy_task_data(db_file_path, NULL, &tmp_buf, &buf_size, ALL_TASKS)) && (buf_size > 0)){
			   *taskData = (char *) calloc(buf_size + 1 ,sizeof(unsigned char));
			   if(*taskData){
				   strcpy((char*) *taskData,(char*) tmp_buf);
				   *buffLen = buf_size;				   
				}
				free(tmp_buf);
			}
	}
	else
	{
		rc = MA_ERROR_INVALIDARG;
	}
	return rc;
}

int get_product_tasks(const char* db_file_path, char* product_id, char** taskData, int* buffLen){
	ma_error_t rc = MA_OK; 
	if(db_file_path && (strlen(db_file_path) > 0) && 
	   product_id && (strlen(product_id) > 0) && 
	   taskData && buffLen){		   	
			char *tmp_buf = NULL;
			int buf_size = 0;
			convert_to_upper(product_id);
			if(MA_OK == (rc = get_policy_task_data(db_file_path, product_id, &tmp_buf, &buf_size, PRODUCT_BASED_TASKS)) && (buf_size > 0)){
				*taskData = (char *) calloc(buf_size + 1 ,sizeof(unsigned char));
				if(*taskData){
					strcpy((char*) *taskData,(char*) tmp_buf);
					*buffLen = buf_size;
					rc = MA_OK;
				}
				free(tmp_buf);
			}
	}
	else
	{
		rc = MA_ERROR_INVALIDARG;
	}
	return rc;
}

int get_all_properties(const char* db_file_path, char** propertyData, int* buffLen){
	ma_error_t rc = MA_OK; 	
	ma_db_viewer_t *self = &viewer;
	ma_ds_database_t *ds_db_handle = NULL;
	char *props_version = "1234";
	const char *guid = NULL, *tenantid = NULL, *computername = NULL;
	char *props_data = NULL ;
	ma_buffer_t *buffer = NULL, *guid_buffer = NULL, *tenantid_buffer = NULL, *computer_name_buf = NULL ;
	ma_property_xml_generator_t *prop_xml_genrtr = NULL ;
	size_t buf_size = 0 ;
	ma_property_xml_generator_t *prop_xml_gen = NULL ;	
	ma_ds_t* ds_handle = NULL;
	
	if(db_file_path && (strlen(db_file_path) > 0) && propertyData && buffLen){
		if(MA_OK != (rc = ma_db_viewer_init(db_file_path, self))) {
			fprintf(stderr,"Failed to initialize db viewer, err = %d \n", rc);
			return rc;
		}

		if(MA_OK != (rc = ma_ds_database_create(self->db, MA_DS_DEFAULT_INSTANCE_NAME, &ds_db_handle))) {
			fprintf(stderr, "Failed to create data store, err is %d \n", rc);
			return rc;
		}
		
		ds_handle = (ma_ds_t *)ds_db_handle ;

		if(MA_OK == (rc = ma_ds_get_str(ds_handle, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_GUID_STR, &guid_buffer)))
			ma_buffer_get_string(guid_buffer, &guid, &buf_size) ;
		//else
			//fprintf(stderr,"Could not get GUID, err = %d \n", rc ) ;
	
		if(MA_OK == (rc = ma_ds_get_str(ds_handle, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_TENANT_ID_STR, &tenantid_buffer)))
			ma_buffer_get_string(tenantid_buffer, &tenantid, &buf_size) ;
		//else
			//fprintf(stderr,"Could not get Tenant id, err = %d \n", rc);

		if(MA_OK == (rc = ma_ds_get_str(ds_handle, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_COMPUTER_NAME_STR, &computer_name_buf))) 
			ma_buffer_get_string(computer_name_buf, &computername, &buf_size) ;
		//else
			//fprintf(stderr,"Could not get computer name, err = %d \n", rc);
	
		if(MA_OK == (rc = ma_property_xml_generator_create(&prop_xml_gen, ds_handle, MA_PROPERTY_DATA_PATH_NAME_STR, computername, guid, tenantid, props_version))) {		
			if(MA_OK == (rc = ma_property_xml_generator_get_last_full_props(prop_xml_gen, (unsigned char **)&props_data, &buf_size))) {
				//fprintf(stderr,"property data is \n ---- %s \n", props_data);
				*propertyData = (char *) calloc(buf_size + 1 ,sizeof(unsigned char));
				if(*propertyData){
				   strcpy((char*) *propertyData, props_data);
				   *buffLen = buf_size;				   
				}
				free(props_data) ;	props_data = NULL ;

			}else
				fprintf(stderr,"ma_property_xml_generator_get_last_full_props failed, err = %d \n", rc);
		
			(void)ma_property_xml_generator_release(prop_xml_gen) ;	prop_xml_gen = NULL ;
		}
		else
			fprintf(stderr,"ma_property_xml_generator_create failed, err = %d \n", rc);
	
	
		if(guid_buffer) (void)ma_buffer_release(guid_buffer);
		if(tenantid_buffer) (void)ma_buffer_release(tenantid_buffer) ;	
		if(computer_name_buf) (void)ma_buffer_release(computer_name_buf);	

	}
	else	
		rc = MA_ERROR_INVALIDARG;
	
	return rc;
}




