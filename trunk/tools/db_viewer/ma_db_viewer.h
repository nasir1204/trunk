#ifndef MA_DB_VIEWER_H_INCLUDED
#define MA_DB_VIEWER_H_INCLUDED


//#include "ma/ma_common.h"

#ifdef _MSC_VER
/* Windows - set up dll import/export decorators. */
# if defined(MA_BUILDING_DB_VIEWER_SHARED)
/* Building shared library. */
#   define MA_DB_VIEWER_API __declspec(dllexport)
# elif !defined(MA_BUILDING_DB_VIEWER_STATIC)
/* Using shared library. */
#   define MA_DB_VIEWER_API __declspec(dllimport)
# else
/* Building static library or executable (unit test). */
#   define MA_DB_VIEWER_API /* nothing */
# endif
# endif

#ifdef __cplusplus
# define MA_CPP(x) x
#else
# define MA_CPP(x)
#endif

MA_CPP(extern "C" {)

/** 
 * @brief Helper function to retrieve policy data from the given DB file in xml format
 *
 * @param[in] db_file			Database file name
 * @param[out] policyDataXml		Policy data in Xml format.
 * @param[out] buffLen			Length of the data buffer.
 *
 * @return On Success 0
 *         On Failure Return code describing the failure.	
*/
MA_DB_VIEWER_API int get_all_product_policies(const char* db_file, char** policyData, int* buffLen);

/**
 * @brief Helper function to retrieve policy data for the specified product id from the given DB file in xml format 
 * 
 * @param[in] db_file			Database file name
 * @param[in] product_id		Product id 
 * @param[out] policyDataXml	Policy data in Xml format.
 * @param[out] buffLen			Length of the data buffer.
 *
 * @return On success 0
 *        On Failure Return code for the failure.	
 */
MA_DB_VIEWER_API int get_product_policies(const char* db_file, char* product_id, char** policyData, int* buffLen);

/** 
 * @brief Helper function to retrieve task data from the given DB file in xml format
 *
 * @param[in] db_file			Database file name
 * @param[out] taskData		    Task data in Xml format.
 * @param[out] buffLen			Length of the data buffer.
 *
 * @return On Success 0
 *         On Failure Return code describing the failure.	
 */
MA_DB_VIEWER_API int get_all_product_tasks(const char* db_file, char** taskData, int* buffLen);

/**
 * @brief Helper function to retrieve task data for the specified product id from the given DB file in xml format 
 * 
 * @param[in] db_file			Database file name
 * @param[in] product_id		Product id 
 * @param[out] policyDataXml	Policy data in Xml format.
 * @param[out] buffLen			Length of the data buffer.
 *
 * @return On success 0
 *        On Failure Return code for the failure.	
 */
MA_DB_VIEWER_API int get_product_tasks(const char* db_file, char* product_id, char** taskData, int* buffLen);

/** 
 * @brief Helper function to retrieve property data from the given DB file in xml format
 *
 * @param[in] db_file			Database file name
 * @param[out] propertyData		Property data in Xml format.
 * @param[out] buffLen			Length of the data buffer.
 *
 * @return On Success 0
 *         On Failure Return code describing the failure.	
 */
MA_DB_VIEWER_API int get_all_properties(const char* db_file, char** propertyData, int* buffLen);

MA_CPP(})

#endif /* MA_DB_VIEWER_H_INCLUDED */

