/*****************************************************************************
This file contains the unit tests for APIs for handling the raw data from the
Variant library.
******************************************************************************/
#include "unity.h"
#include "unity_fixture.h"

#include "ma/ma_variant.h"


TEST_GROUP_RUNNER(ma_variant_raw_test_group)
{
    do {RUN_TEST_CASE(ma_variant_raw_tests, ma_variant_create_and_get_from_raw_buffer_test_01)} while (0);
    do {RUN_TEST_CASE(ma_variant_raw_tests, ma_variant_create_and_get_from_raw_buffer_test_02)} while (0);
    do {RUN_TEST_CASE(ma_variant_raw_tests, ma_variant_is_equal_raw_test)} while (0);
    do {RUN_TEST_CASE(ma_variant_raw_tests, ma_variant_is_equal_raw_test_01)} while (0);
    do {RUN_TEST_CASE(ma_variant_raw_tests, ma_variant_is_equal_raw_test_02)} while (0);
    do {RUN_TEST_CASE(ma_variant_raw_tests, ma_variant_inc_ref_dec_ref_raw_test)} while (0);
}

TEST_SETUP(ma_variant_raw_tests)
{
}

TEST_TEAR_DOWN(ma_variant_raw_tests)
{

}


/*
Unit tests for following macros for raw data (stream of characters) -
1) ma_variant_create_from_raw
2) ma_variant_get_raw_buffer
3) ma_variant_get_type

Note - This test uses the ma_buffer_get_raw macro from the ma_buffer.h/.c for
verifying the functionality of above mentioned macros. This ma_buffer_get_raw
macro will be fully verified (with all its robustness) in the ma_buffer unit
and white box tests.
*/
TEST(ma_variant_raw_tests, ma_variant_create_and_get_from_raw_buffer_test_01)
{
    /* 'ma_variant_t' type Pointer for storing the base address of created variant */
    ma_variant_t *variant_ptr = NULL;

    /* 'ma_buffer_t' type Pointer for storing the base address of the buffer */
    ma_buffer_t *buffer_ptr = NULL;

    /* Loop iterator. */
    ma_uint8_t counter = 0;

    /* To hold the raw stream and its size, as returned by the ma_buffer_get_raw
       macro */
    const unsigned char *raw_val = 0;
    size_t raw_size = 0;

    /* To hold the size of the buffer */
    size_t buffer_size = 0;

    /* This variable should hold MA_VARTYPE_RAW after the ma_variant_get_type
       call. Initial value of this variable is given as MA_VARTYPE_NULL. */
    ma_vartype_t vartype_obj = MA_VARTYPE_NULL;

    // For raw data, size of stream is equal to the size of the buffer
    struct LookupTable
    {
        const char *test_buffer;        // Test String passed to the ma_variant_create_from_raw macro.
        size_t test_length;             // Test String length passed to the ma_variant_create_from_raw macro.
        const char *return_raw_buffer;  // Expected raw stream stored and returned by the ma_buffer_get_raw macro.
        size_t return_raw_buffer_size;  // Expected raw stream length stored and returned by the ma_buffer_get_raw macro.
    };

    static const char notNullTerminated[] = {'N','o','t'};

    static const char wantAll5bytes[] = {3,0,5,1,0};

    const struct LookupTable test_vector[] =
    {
        { "MA Security Agent",                                                                    17,   "MA Security Agent",                                                                    17 },
        { "McAfee Security 12345654569587Agent",                                                  35,   "McAfee Security 12345654569587Agent",                                                  35 },
        { "MCAFEE SECURITY AGENT",                                                                 6,   "MCAFEE",                                                                                6 },
        { "MCAFEE SECURITY AGENT 1.0",                                                           100,   "MCAFEE SECURITY AGENT 1.0",                                                           100 },
        { "12345678901234567890",                                                                 30,   "12345678901234567890",                                                                 30 },
        { "!@@#$%%^^&*&&(*(**(**)()",                                                             24,   "!@@#$%%^^&*&&(*(**(**)()",                                                             24 },
        { "ABCD1234$%@$%",                                                                        12,   "ABCD1234$%@$",                                                                         12 },
        { "~!@#$%^&*$^%^%$^#^%&^%&^%",                                                            25,   "~!@#$%^&*$^%^%$^#^%&^%&^%",                                                            25 },
        { "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghi\0jklmnopqrstuvwxyz123456789\n0!@#$%^&*()_+-=<>?:", 82,   "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghi\0jklmnopqrstuvwxyz123456789\n0!@#$%^&*()_+-=<>?:", 82 },
        { notNullTerminated,  sizeof(notNullTerminated),   notNullTerminated,  sizeof(notNullTerminated), },
        { wantAll5bytes,  sizeof(wantAll5bytes),   wantAll5bytes,  sizeof(wantAll5bytes), },
    };


    /*--------------------------------------------------------------------------*/
    /*
    Test 1 : Verify that the ma_variant_create_from_raw macro returns
    MA_ERROR_INVALIDARG    when the raw buffer passed to the macro is NULL.
    */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_raw(NULL, 10, &variant_ptr) == MA_ERROR_INVALIDARG, \
                        "ma_variant_create_from_raw doesn't return MA_ERROR_INVALIDARG when raw buffer address passed to macro is NULL");

    /*--------------------------------------------------------------------------*/
    /*
    Test 2 : Verify that the ma_variant_create_from_raw macro returns
    MA_ERROR_PRECONDITION when the length of the raw buffer passed to
    the macro is 0.
    */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_raw("MA Security Agent", 0, &variant_ptr) == MA_OK, \
                        "ma_variant_create_from_raw doesn't return MA_OK when length of raw buffer is 0");

    ma_variant_release(variant_ptr);

    /*--------------------------------------------------------------------------*/
    /*
    Test 3 : Verify that the ma_variant_create_from_raw macro returns
    MA_ERROR_INVALIDARG    when the address of variant pointer passed for storing
    created variant is NULL.
    */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_raw("MA Security Agent", 17, NULL) == MA_ERROR_INVALIDARG, \
                        "ma_variant_create_from_raw doesn't return MA_ERROR_INVALIDARG when address of variant pointer is NULL");

    /*--------------------------------------------------------------------------*/
    /*
    Test 4 : Verify that the ma_variant_create_from_raw macro returns MA_OK
    and creates the variant correctly using the passed raw buffer.
    */
    for(counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        TEST_ASSERT_MESSAGE(ma_variant_create_from_raw(test_vector[counter].test_buffer, test_vector[counter].test_length, &variant_ptr) == MA_OK, \
                            "Raw buffer variant could not be created");

        TEST_ASSERT_MESSAGE(ma_variant_get_type(variant_ptr, &vartype_obj) == MA_OK, \
                            "ma_variant_get_type doesn't return MA_OK for Raw Buffer Variant");

        TEST_ASSERT_MESSAGE(vartype_obj == MA_VARTYPE_RAW, \
                            "ma_variant_get_type doesn't return MA_VARTYPE_RAW for Raw Buffer Variant");

        TEST_ASSERT_MESSAGE(ma_variant_get_raw_buffer(variant_ptr, &buffer_ptr) == MA_OK, \
                            "Raw Buffer cannot be retrieved from the Variant");

        TEST_ASSERT_MESSAGE(ma_buffer_size(buffer_ptr, &buffer_size) == MA_OK, \
                            "ma_buffer_size doesn't return MA_OK for valid buffer");

        TEST_ASSERT_MESSAGE(test_vector[counter].test_length == buffer_size, \
                            "ma_buffer_size doesn't return correct buffer size");

        TEST_ASSERT_MESSAGE(ma_buffer_get_raw(buffer_ptr, &raw_val, &raw_size) == MA_OK, \
                            "ma_buffer_get_raw could not retrieve raw buffer and its size from the raw buffer variant");

        TEST_ASSERT_MESSAGE(test_vector[counter].return_raw_buffer_size == raw_size, \
                            "Size of retrieved raw stream is not correct");

        TEST_ASSERT_EQUAL_MEMORY_MESSAGE(test_vector[counter].return_raw_buffer, \
                                         raw_val, test_vector[counter].return_raw_buffer_size, \
                                         "Buffer could not be retrieved correctly from the Variant");

        /* ma_variant_get_raw_buffer returns MA_ERROR_INVALIDARG for NULL variant */
        TEST_ASSERT_MESSAGE(ma_variant_get_raw_buffer(NULL, &buffer_ptr) == MA_ERROR_INVALIDARG, \
                            "ma_variant_get_raw_buffer doesn't return MA_ERROR_INVALIDARG for NULL variant");

        /* ma_variant_get_raw_buffer returns MA_ERROR_INVALIDARG for NULL buffer pointer */
        TEST_ASSERT_MESSAGE(ma_variant_get_raw_buffer(variant_ptr, NULL) == MA_ERROR_INVALIDARG, \
                            "ma_variant_get_raw_buffer doesn't return MA_ERROR_INVALIDARG for NULL buffer pointer");

        /* Deallocating buffer */
        ma_buffer_release(buffer_ptr);
        ma_variant_release(variant_ptr);
    }

    /*--------------------------------------------------------------------------*/
}


/*
Unit tests for following macros for raw data (aw stream) -
1) ma_variant_create_from_raw
2) ma_variant_get_raw_buffer
3) ma_variant_get_type

Note - This test uses the ma_buffer_get_raw macro from the ma_buffer.h/.c for
verifying the functionality of above mentioned macros. This ma_buffer_get_raw
macro will be fully verified (with all its robustness) in the ma_buffer unit
and white box tests.
*/
TEST(ma_variant_raw_tests, ma_variant_create_and_get_from_raw_buffer_test_02)
{
    /* 'ma_variant_t' type Pointer for storing the base address of created variant */
    ma_variant_t *variant_ptr = NULL;

    /* 'ma_buffer_t' type Pointer for storing the base address of the buffer */
    ma_buffer_t *buffer_ptr = NULL;

    /* To hold the raw stream and its size, as returned by the ma_buffer_get_raw
       macro */
    const unsigned char *raw_val = 0;
    size_t raw_size = 0;

    /* To hold the size of the buffer */
    size_t buffer_size = 0;

    /* This variable should hold MA_VARTYPE_RAW after the ma_variant_get_type
       call. Initial value of this variable is given as MA_VARTYPE_NULL. */
    ma_vartype_t vartype_obj = MA_VARTYPE_NULL;

    /* An instance of this structure acts as the raw buffer stream */
    struct test_struct
    {
        ma_uint8_t int_data;
        char char_data[300];
        ma_float_t float_data;
        ma_bool_t boolean_data;
    };

    struct test_struct raw_buffer = { 0x12,
                                      "This is a Next-Gen McAfee Agent software - Variant Library raw buffer test."
                                      "!@@!#@#$#%#$^$%^%$&^%&*^&*^&(&*(&*(^&^$%@#$#!@#!@$%%$&%^*&%^*^&(*&(*&(%^^$"
                                      "!@#$%^&*()_+01234567890123456789",
                                      2.3f,
                                      1};


    TEST_ASSERT_MESSAGE(ma_variant_create_from_raw((const void*)&raw_buffer, sizeof(raw_buffer), &variant_ptr) == MA_OK, \
                        "Raw Buffer variant could not be created");

    TEST_ASSERT_MESSAGE(ma_variant_get_type(variant_ptr, &vartype_obj) == MA_OK, \
                        "ma_variant_get_type doesn't return MA_OK for Raw Buffer Variant");

    TEST_ASSERT_MESSAGE(vartype_obj == MA_VARTYPE_RAW, "ma_variant_get_type doesn't return MA_VARTYPE_RAW for RAW Buffer Variant");

    TEST_ASSERT_MESSAGE(ma_variant_get_raw_buffer(variant_ptr, &buffer_ptr) == MA_OK, \
                        "Raw Buffer cannot be retrieved from the Variant");

    TEST_ASSERT_MESSAGE(ma_buffer_size(buffer_ptr, &buffer_size) == MA_OK, \
                        "ma_buffer_size doesn't return MA_OK for valid buffer");

    TEST_ASSERT_MESSAGE(sizeof(raw_buffer) == buffer_size, \
                        "ma_buffer_size doesn't return correct buffer size");

    TEST_ASSERT_MESSAGE(ma_buffer_get_raw(buffer_ptr, &raw_val, &raw_size) == MA_OK, \
                        "ma_buffer_get_raw could not retrieve raw buffer and its size from the raw buffer variant");

    TEST_ASSERT_MESSAGE(sizeof(raw_buffer) == raw_size, \
                            "Size of retrieved raw stream is not correct");

    TEST_ASSERT_EQUAL_MEMORY_MESSAGE((const void*)&raw_buffer, raw_val, sizeof(raw_buffer), \
                                         "Buffer could not be retrieved correctly from the Variant");

     /* Deallocating buffer */
    ma_buffer_release(buffer_ptr);
    ma_variant_release(variant_ptr);
}


/*
Unit tests for ma_variant_is_equal macro for raw buffer variants.
*/
TEST(ma_variant_raw_tests, ma_variant_is_equal_raw_test)
{
    /* 'ma_variant_t' type Pointer for storing the base address of created variant */
    ma_variant_t *variant_ptr_1 = NULL;
    ma_variant_t *variant_ptr_2 = NULL;

    /* Loop iterator. */
    ma_uint8_t counter = 0;

    /* This variable should hold MA_VARTYPE_RAW after the ma_variant_get_type
       call. Initial value of this variable is given as MA_VARTYPE_NULL. */
    ma_vartype_t vartype_obj = MA_VARTYPE_NULL;

    /* Variable to hold the results */
    ma_bool_t comparison_result = MA_FALSE;

    struct LookupTable
    {
        const char *test_buffer_1;
        ma_uint32_t test_buffer_length_1;
        const char *test_buffer_2;
        ma_uint32_t test_buffer_length_2;
        int exp_result;
    };

    const struct LookupTable test_vector[] =
    {
        { "MA Security Agent",                                                                     17,   "MA Security Agent",                                                                    17,   MA_TRUE  },
        { "McAfee Security 12345654569587Agent",                                                   35,   "McAfee Security 12345654569587Agent",                                                  35,   MA_TRUE  },
        { "McAfee Security 12345654569587Agent",                                                  35,   "McAfee Security 12345654569587Agent",                                                  35,   MA_TRUE  },
        { "MCAFEE SECURITY AGENT",                                                                 22,   "MCAFEE SECURITY AGENT",                                                                21,   MA_FALSE },
        { "MCAFEE SECURITY AGENT 1.0",                                                            100,   "MCAFEE SECURITY AGENT 1.0",                                                            25,   MA_FALSE  },
        { "MCAFEE SECURITY AGENT 1.0",                                                             26,   "MCAFEE SECURITY AGENT 1.0 ",                                                           26,   MA_FALSE },
        { "12345678901234567890",                                                                  19,   "12345678901234567890",                                                                 20,   MA_FALSE },
        { "!@@#$%%^^&*&&(*(**(**)()",                                                              24,   "!@@#$%%^^&*&&(*(**(**)()",                                                             24,   MA_TRUE  },
        { "ABCD1234$%@$%",                                                                         13,   "ABCD1234$%@$",                                                                         13,   MA_FALSE },
        { "~!@#$%^&*$^%^%$^#^%&^%&^%",                                                             25,   "~!@#$%^&*$^%^%$^#^%&^%&^%",                                                            25,   MA_TRUE  },
        { "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghi jklmnopqrstuvwxyz1234567890!@#$%^&*()_+-=<>?:\"",   82,   "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghi jklmnopqrstuvwxyz1234567890!@#$%^&*()_+-=<>?:\"",  82,   MA_TRUE  },
        { "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghi j klmnopqrstuvwxyz1234567890!@#$%^&*()_+-=<>?:\"",  83,   "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghi jklmnopqrstuvwxyz1234567890!@#$%^&*()_+-=<>?:\"",  82,   MA_FALSE },
    };

    /*--------------------------------------------------------------------------*/

    for(counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        /* Creating Raw Buffer variant 1 and checking its type */
        TEST_ASSERT_MESSAGE(ma_variant_create_from_raw(test_vector[counter].test_buffer_1, test_vector[counter].test_buffer_length_1, &variant_ptr_1) == MA_OK, \
                            "Raw Buffer variant could not be created");

        TEST_ASSERT_MESSAGE(ma_variant_get_type(variant_ptr_1, &vartype_obj) == MA_OK, \
                            "ma_variant_get_type doesn't return MA_OK for Raw Buffer Variant");

        TEST_ASSERT_MESSAGE(vartype_obj == MA_VARTYPE_RAW, \
                            "ma_variant_get_type doesn't return MA_VARTYPE_RAW for RAW Buffer Variant");

        /* Creating Raw Buffer variant 2 and checking its type */
        TEST_ASSERT_MESSAGE(ma_variant_create_from_raw(test_vector[counter].test_buffer_2, test_vector[counter].test_buffer_length_2, &variant_ptr_2) == MA_OK, \
                            "Raw buffer variant could not be created");

        TEST_ASSERT_MESSAGE(ma_variant_get_type(variant_ptr_2, &vartype_obj) == MA_OK, \
                            "ma_variant_get_type doesn't return MA_OK for Raw Buffer Variant");

        TEST_ASSERT_MESSAGE(vartype_obj == MA_VARTYPE_RAW, \
                            "ma_variant_get_type doesn't return MA_VARTYPE_RAW for Raw Buffer Variant");

        /* Equality Checks */

        /* Comparing variant 1 with NULL results in MA_ERROR_INVALIDARG */
        TEST_ASSERT_MESSAGE(ma_variant_is_equal(variant_ptr_1, NULL, &comparison_result) == MA_ERROR_INVALIDARG, \
                            "Comparing Raw Buffer Variant with NULL doesn't return MA_ERROR_INVALIDARG");

        /* Comparing variant 2 with NULL results in MA_ERROR_INVALIDARG */
        TEST_ASSERT_MESSAGE(ma_variant_is_equal(NULL, variant_ptr_2, &comparison_result) == MA_ERROR_INVALIDARG, \
                            "Comparing Raw Buffer Variant with NULL doesn't return MA_ERROR_INVALIDARG");

        /* MA_ERROR_INVALIDARG when location to store comparison result is NULL */
        TEST_ASSERT_MESSAGE(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, NULL) == MA_ERROR_INVALIDARG, \
                            "MA_ERROR_INVALIDARG is not returned, when location to store comparison result is NULL");

        /* Comparing Raw Buffer Variant 1 with itself */
        TEST_ASSERT_MESSAGE(ma_variant_is_equal(variant_ptr_1, variant_ptr_1, &comparison_result) == MA_OK, \
                            "Raw Buffer variant could not be compared with itself");

        TEST_ASSERT_MESSAGE(comparison_result == MA_TRUE, "Raw Buffer Variant comparison with itself returns MA_FALSE, expected MA_TRUE");


        /* Comparing Raw Buffer Variant 2 with itself */
        TEST_ASSERT_MESSAGE(ma_variant_is_equal(variant_ptr_2, variant_ptr_2, &comparison_result) == MA_OK, \
                            "Raw Buffer variant could not be compared with itself");

        TEST_ASSERT_MESSAGE(comparison_result == MA_TRUE, "Raw Buffer Variant comparison with itself returns MA_FALSE, expected MA_TRUE");

        /* Comparing Valid Raw Buffer Variants (Variant 1 and Variant 2 */
        TEST_ASSERT_MESSAGE(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, &comparison_result) == MA_OK, \
                            "Valid Raw Buffer variants could not be compared");

        TEST_ASSERT_MESSAGE(comparison_result == test_vector[counter].exp_result, "Comparison result is not correct");

        ma_variant_release(variant_ptr_1);
        ma_variant_release(variant_ptr_2);
    }
    /*--------------------------------------------------------------------------*/
}

TEST(ma_variant_raw_tests, ma_variant_is_equal_raw_test_01)
{
    /* 'ma_variant_t' type Pointer for storing the base address of created variant */
    ma_variant_t *variant_ptr_1 = NULL;
    ma_variant_t *variant_ptr_2 = NULL;

    /* This variable should hold MA_VARTYPE_STRING after the ma_variant_get_type
       call. Initial value of this variable is given as MA_VARTYPE_NULL. */
    ma_vartype_t vartype_obj = MA_VARTYPE_NULL;

    /* Variable to hold the results */
    ma_bool_t comparison_result = MA_FALSE;

    ma_uint8_t raw_buffer_1[] = { 0x12, 0x23, 0x34, 0x45, 0x56, 0x67, 0x78, 0x89, 0x90 };
    ma_uint8_t raw_buffer_2[] = { 0x12, 0x23, 0x34, 0x45, 0x56, 0x67, 0x78, 0x89, 0x90 };

    /*--------------------------------------------------------------------------*/
        /* Creating Raw Buffer variant 1 and checking its type */
        TEST_ASSERT_MESSAGE(ma_variant_create_from_raw(raw_buffer_1, sizeof(raw_buffer_1)/sizeof(raw_buffer_1[0]), &variant_ptr_1) == MA_OK, \
                            "Raw Buffer variant could not be created");

        TEST_ASSERT_MESSAGE(ma_variant_get_type(variant_ptr_1, &vartype_obj) == MA_OK, \
                            "ma_variant_get_type doesn't return MA_OK for Raw Buffer Variant");

        TEST_ASSERT_MESSAGE(vartype_obj == MA_VARTYPE_RAW, \
                            "ma_variant_get_type doesn't return MA_VARTYPE_RAW for RAW Buffer Variant");

        /* Creating Raw Buffer variant 2 and checking its type */
        TEST_ASSERT_MESSAGE(ma_variant_create_from_raw(raw_buffer_2, sizeof(raw_buffer_2)/sizeof(raw_buffer_2[0]), &variant_ptr_2) == MA_OK, \
                            "Raw Buffer variant could not be created");

        TEST_ASSERT_MESSAGE(ma_variant_get_type(variant_ptr_2, &vartype_obj) == MA_OK, \
                            "ma_variant_get_type doesn't return MA_OK for Raw Buffer Variant");

        TEST_ASSERT_MESSAGE(vartype_obj == MA_VARTYPE_RAW, \
                            "ma_variant_get_type doesn't return MA_VARTYPE_RAW for Raw Buffer Variant");

        /* Equality Checks */

        /* Comparing Valid Raw Buffer Variants (Variant 1 and Variant 2) */
        TEST_ASSERT_MESSAGE(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, &comparison_result) == MA_OK, \
                            "Valid Raw Buffer variants could not be compared");

        TEST_ASSERT_MESSAGE(comparison_result == MA_TRUE, "Comparison result is not correct");

        ma_variant_release(variant_ptr_2);
        ma_variant_release(variant_ptr_1);
    /*--------------------------------------------------------------------------*/
}

/* Raw buffer equality test for complex type like structures. */
TEST(ma_variant_raw_tests, ma_variant_is_equal_raw_test_02)
{
    /* 'ma_variant_t' type Pointer for storing the base address of created variant */
    ma_variant_t *variant_ptr_1 = NULL;
    ma_variant_t *variant_ptr_2 = NULL;
    ma_variant_t *variant_ptr_3 = NULL;
    ma_variant_t *variant_ptr_4 = NULL;

    /* This variable should hold MA_VARTYPE_STRING after the ma_variant_get_type
       call. Initial value of this variable is given as MA_VARTYPE_NULL. */
    ma_vartype_t vartype_obj = MA_VARTYPE_NULL;

    /* Variable to hold the results */
    ma_bool_t comparison_result = MA_FALSE;

    struct test_struct
    {
        ma_uint8_t int_data;
        char char_data[300];
        ma_float_t float_data;
        ma_bool_t boolean_data;
    };

    struct test_struct raw_buffer_1 = { 0x12,
                                        "This is a Next-Gen McAfee Agent software - Variant Library raw buffer test."
                                        "This is a Next-Gen McAfee Agent software - Variant Library raw buffer test."
                                        "This is a Next-Gen McAfee Agent software - Variant Library raw buffer test."
                                        "!@#$%^&*()_+01234567890123456789",
                                        2.3f,
                                        1};

    // Same as raw_buffer_1
    struct test_struct raw_buffer_2 =  { 0x12,
                                        "This is a Next-Gen McAfee Agent software - Variant Library raw buffer test."
                                        "This is a Next-Gen McAfee Agent software - Variant Library raw buffer test."
                                        "This is a Next-Gen McAfee Agent software - Variant Library raw buffer test."
                                        "!@#$%^&*()_+01234567890123456789",
                                        2.3f,
                                        1};

    // String member is different from raw_buffer_1
    struct test_struct raw_buffer_3 =  { 0x12,
                                        "This is a Next-Gen McAfee Agent software - Variant Library raw buffer test."
                                        "This is a Next-Gen McAfee Agent software - Variant Library raw buffer test."
                                        "This is a Next-Gen McAfee Agent software - Variant Library raw buffer test."
                                        "!@#$%^&*()_+0123456789012345678",
                                        2.3f,
                                        1};

    // Float member is different from raw_buffer_1
    struct test_struct raw_buffer_4 =  { 0x12,
                                        "This is a Next-Gen McAfee Agent software - Variant Library raw buffer test."
                                        "This is a Next-Gen McAfee Agent software - Variant Library raw buffer test."
                                        "This is a Next-Gen McAfee Agent software - Variant Library raw buffer test."
                                        "!@#$%^&*()_+01234567890123456789",
                                        -2.3f,
                                        1};


    /*--------------------------------------------------------------------------*/
        /* Creating Raw Buffer variant 1 and checking its type */
        TEST_ASSERT_MESSAGE(ma_variant_create_from_raw((const void*)&raw_buffer_1, sizeof(raw_buffer_1), &variant_ptr_1) == MA_OK, \
                            "Raw Buffer variant could not be created");

        TEST_ASSERT_MESSAGE(ma_variant_get_type(variant_ptr_1, &vartype_obj) == MA_OK, \
                            "ma_variant_get_type doesn't return MA_OK for Raw Buffer Variant");

        TEST_ASSERT_MESSAGE(vartype_obj == MA_VARTYPE_RAW, \
                            "ma_variant_get_type doesn't return MA_VARTYPE_RAW for RAW Buffer Variant");

        /* Creating Raw Buffer variant 2 and checking its type */
        TEST_ASSERT_MESSAGE(ma_variant_create_from_raw((const void*)&raw_buffer_2, sizeof(raw_buffer_2), &variant_ptr_2) == MA_OK, \
                            "Raw Buffer variant could not be created");

        TEST_ASSERT_MESSAGE(ma_variant_get_type(variant_ptr_2, &vartype_obj) == MA_OK, \
                            "ma_variant_get_type doesn't return MA_OK for Raw Buffer Variant");

        TEST_ASSERT_MESSAGE(vartype_obj == MA_VARTYPE_RAW, \
                            "ma_variant_get_type doesn't return MA_VARTYPE_RAW for Raw Buffer Variant");

        /* Creating Raw Buffer variant 3 and checking its type */
        TEST_ASSERT_MESSAGE(ma_variant_create_from_raw((const void*)&raw_buffer_3, sizeof(raw_buffer_3), &variant_ptr_3) == MA_OK, \
                            "Raw Buffer variant could not be created");

        TEST_ASSERT_MESSAGE(ma_variant_get_type(variant_ptr_3, &vartype_obj) == MA_OK, \
                            "ma_variant_get_type doesn't return MA_OK for Raw Buffer Variant");

        TEST_ASSERT_MESSAGE(vartype_obj == MA_VARTYPE_RAW, \
                            "ma_variant_get_type doesn't return MA_VARTYPE_RAW for Raw Buffer Variant");

        /* Creating Raw Buffer variant 4 and checking its type */
        TEST_ASSERT_MESSAGE(ma_variant_create_from_raw((const void*)&raw_buffer_4, sizeof(raw_buffer_4), &variant_ptr_4) == MA_OK, \
                            "Raw Buffer variant could not be created");

        TEST_ASSERT_MESSAGE(ma_variant_get_type(variant_ptr_4, &vartype_obj) == MA_OK, \
                            "ma_variant_get_type doesn't return MA_OK for Raw Buffer Variant");

        TEST_ASSERT_MESSAGE(vartype_obj == MA_VARTYPE_RAW, \
                            "ma_variant_get_type doesn't return MA_VARTYPE_RAW for Raw Buffer Variant");

        /* Equality Checks */

        /* Comparing Valid Raw Buffer Variants (Variant 1 and Variant 2) */
        TEST_ASSERT_MESSAGE(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, &comparison_result) == MA_OK, \
                            "Valid Raw Buffer variants could not be compared");

        TEST_ASSERT_MESSAGE(comparison_result == MA_TRUE, "Comparison result is not correct");

        /* Comparing Valid Raw Buffer Variants (Variant 1 and Variant 3) */
        TEST_ASSERT_MESSAGE(ma_variant_is_equal(variant_ptr_1, variant_ptr_3, &comparison_result) == MA_OK, \
                            "Valid Raw Buffer variants could not be compared");

        TEST_ASSERT_MESSAGE(comparison_result == MA_FALSE, "Comparison result is not correct");

        /* Comparing Valid Raw Buffer Variants (Variant 1 and Variant 4) */
        TEST_ASSERT_MESSAGE(ma_variant_is_equal(variant_ptr_1, variant_ptr_4, &comparison_result) == MA_OK, \
                            "Valid Raw Buffer variants could not be compared");

        TEST_ASSERT_MESSAGE(comparison_result == MA_FALSE, "Comparison result is not correct");

        /*--------------------------------------------------------------------------*/
        ma_variant_release(variant_ptr_4);
        ma_variant_release(variant_ptr_3);
        ma_variant_release(variant_ptr_2);
        ma_variant_release(variant_ptr_1);

}

/*
Unit tests for ma_variant_inc_ref and ma_variant_dec_ref macros for raw buffer variant.
*/
TEST(ma_variant_raw_tests, ma_variant_inc_ref_dec_ref_raw_test)
{
    /* 'ma_variant_t' type Pointer for storing the base address of variant object */
    ma_variant_t *variant_ptr = NULL;

    // Loop iterator.
    ma_uint8_t counter = 0;

    /* Variable to hold the variant type */
    ma_vartype_t vartype_obj = MA_VARTYPE_NULL;

    /*Test 1 : Calling ma_variant_inc_ref macro with address of variant as NULL*/

    TEST_ASSERT_MESSAGE(ma_variant_add_ref(NULL) == MA_ERROR_INVALIDARG, \
                        "ma_variant_inc_ref doesn't return MA_ERROR_INVALIDARG when argument passed is NULL");

    /*Test 2 : Calling ma_variant_dec_ref macro with address of variant as NULL*/

    TEST_ASSERT_MESSAGE(ma_variant_release(NULL) == MA_ERROR_INVALIDARG, \
                        "ma_variant_dec_ref doesn't return MA_ERROR_INVALIDARG when argument passed is NULL");

    /*Test 3 : Verifying that the ma_variant_inc_ref/ma_variant_dec_ref macro maintains correct reference count*/

    /*Creating a raw buffer variant*/
    TEST_ASSERT_MESSAGE(ma_variant_create_from_raw(L"MA Security Agent", 17, &variant_ptr) == MA_OK, \
                        "ma_variant_create_from_raw doesn't return MA_OK for raw buffer");

    /*Incrementing the variant reference count (10 times) */
    for(counter = 0; counter < 10; counter++)
    {
        TEST_ASSERT_MESSAGE(ma_variant_add_ref(variant_ptr) == MA_OK, \
                            "ma_variant_inc_ref could not increase variant reference count");

        TEST_ASSERT_MESSAGE(ma_variant_get_type(variant_ptr, &vartype_obj) == MA_OK, \
                            "The Variant type could not be retrieved");

        TEST_ASSERT_MESSAGE(vartype_obj == MA_VARTYPE_RAW, \
                            "ma_variant_get_type doesn't hold correct variant type");
    }

    /* Decrementing the variant reference count (11 times). The variant should be destroyed
       by the end of this for-loop */
    for(counter = 0; counter < 11; counter++)
    {
        TEST_ASSERT_MESSAGE(ma_variant_get_type(variant_ptr, &vartype_obj) == MA_OK, \
                            "The Variant type could not be retrieved");

        TEST_ASSERT_MESSAGE(vartype_obj == MA_VARTYPE_RAW, \
                            "ma_variant_get_type doesn't hold correct variant type");

        TEST_ASSERT_MESSAGE(ma_variant_release(variant_ptr) == MA_OK, \
                            "ma_variant_dec_ref could not decrease variant reference count");
    }
}


