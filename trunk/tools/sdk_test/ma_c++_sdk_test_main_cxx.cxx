#include "gtest/gtest.h"
#include "ma/ma_dispatcher.h"

#ifdef MA_WINDOWS
#include <windows.h>
#endif
#ifdef _MSC_VER
# include <crtdbg.h> /* */
# define MA_TRACKLEAKS _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF)
#else
# define MA_TRACKLEAKS ((void *)0)
#endif

ma_dispatcher_t *my_handle = NULL;

GTEST_API_ int main(int argc, char **argv)
{
    MA_TRACKLEAKS;

	ma_dispatcher_initialize(NULL, &my_handle);

    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();

	ma_dispatcher_deinitialize(my_handle);
}


