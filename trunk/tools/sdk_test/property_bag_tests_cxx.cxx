/*****************************************************************************
Property Bag Unit Tests.
******************************************************************************/
#include "ma/properties/property_provider.hxx"
#include "gtest/gtest.h"

TEST(property_bag_tests, property_bag_test_data)
{
    ma_property_bag_t *bag = NULL ;

    ma_property_bag_create(&bag);

    // Property bag C++ binding
	mileaccess::ma::property_bag pb(bag);
	
	// Adding properties
	{
	    mileaccess::ma::variant v("Windows 7 Desktop");
		pb.add_property("ComputerProperties1", "PlatformID", v);
	}

	{
	    mileaccess::ma::variant v("user.beaeng.mfeeng.org");
		pb.add_property("ComputerProperties2", "IPHostName", v);
	}

	{
	    mileaccess::ma::variant v("X's Laptop");
		pb.add_property("ComputerProperties1", "ComputerName", v);
	}

    // Retrieving properties in reverse order
	{
		mileaccess::ma::variant v = pb.get_property("ComputerProperties1", "ComputerName");
        ASSERT_STREQ("X's Laptop", mileaccess::ma::get<std::string>(v).data());
	}

	{
		mileaccess::ma::variant v = pb.get_property("ComputerProperties2", "IPHostName");
        ASSERT_STREQ("user.beaeng.mfeeng.org", mileaccess::ma::get<std::string>(v).data());
	}

	{
		mileaccess::ma::variant v = pb.get_property("ComputerProperties1", "PlatformID");
        ASSERT_STREQ("Windows 7 Desktop", mileaccess::ma::get<std::string>(v).data());
	}

    ma_property_bag_release(bag);
}

