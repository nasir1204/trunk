#include "ma/message.hxx"
#include "gtest/gtest.h"

TEST(ma_message_tests, construction_test_1)
{
    mileaccess::ma::message m;

    // Setting properties
    m.set_property("first_property", "123456");
    m.set_property("@!#@#@cecfef#$@#$#@", "#423423#$5435$#534324@#$^&");
    m.set_property("third_property", "@#@!#@$@#$@#%$#%$^$^");
    m.set_property("#@$#@$DCCDGERR$#@", "DFCSDRECVSDCASEWQEWRERTRTHBFGBFGHFGHFGBGNGH");
    m.set_property("#$#$   ERWEREREV43234cdc", "eWEWwewdWERFFCcsde#@@##");

    // Setting message payload
    mileaccess::ma::variant v("variant_payload");
    m.set_payload(v);

    // Converting message to variant
    mileaccess::ma::variant m_v = m.to_variant();

    // Creating new message from the variant
    mileaccess::ma::message m_new(m_v);

    // Getting the properties from new variant and verifying
    ASSERT_EQ("123456", m_new.get_property("first_property"));
    ASSERT_EQ("#423423#$5435$#534324@#$^&", m_new.get_property("@!#@#@cecfef#$@#$#@"));
    ASSERT_EQ("@#@!#@$@#$@#%$#%$^$^", m_new.get_property("third_property"));
    ASSERT_EQ("DFCSDRECVSDCASEWQEWRERTRTHBFGBFGHFGHFGBGNGH", m_new.get_property("#@$#@$DCCDGERR$#@"));
    ASSERT_EQ("eWEWwewdWERFFCcsde#@@##", m_new.get_property("#$#$   ERWEREREV43234cdc"));

    // retrieving payload from the message
    mileaccess::ma::variant v_payload = m_new.get_payload();
    ASSERT_STREQ("variant_payload", mileaccess::ma::get<std::string>(v_payload).data());
}

// Usage as wrapper over C
TEST(ma_message_tests, construction_test_2)
{
    ma_message_t *my_message = NULL;
    ma_message_create(&my_message);
    mileaccess::ma::message m(my_message, true);

    // Setting properties
    m.set_property("first_property", "123456");
    m.set_property("@!#@#@cecfef#$@#$#@", "#423423#$5435$#534324@#$^&");
    m.set_property("third_property", "@#@!#@$@#$@#%$#%$^$^");
    m.set_property("#@$#@$DCCDGERR$#@", "DFCSDRECVSDCASEWQEWRERTRTHBFGBFGHFGHFGBGNGH");
    m.set_property("#$#$   ERWEREREV43234cdc", "eWEWwewdWERFFCcsde#@@##");

    // Setting message payload
    mileaccess::ma::variant v("variant_payload");
    m.set_payload(v);

    // Converting message to variant
    mileaccess::ma::variant m_v = m.to_variant();

    // Creating new message from the variant
    mileaccess::ma::message m_new(m_v);

	// Cloning the message
	mileaccess::ma::message m_clone = m_new.clone();

    // Getting the properties from the cloned message and verifying
    ASSERT_EQ("123456", m_clone.get_property("first_property"));
    ASSERT_EQ("#423423#$5435$#534324@#$^&", m_clone.get_property("@!#@#@cecfef#$@#$#@"));
    ASSERT_EQ("@#@!#@$@#$@#%$#%$^$^", m_clone.get_property("third_property"));
    ASSERT_EQ("DFCSDRECVSDCASEWQEWRERTRTHBFGBFGHFGHFGBGNGH", m_clone.get_property("#@$#@$DCCDGERR$#@"));
    ASSERT_EQ("eWEWwewdWERFFCcsde#@@##", m_clone.get_property("#$#$   ERWEREREV43234cdc"));

	// retrieving payload from the message
    mileaccess::ma::variant v_payload = m_clone.get_payload();
    ASSERT_STREQ("variant_payload", mileaccess::ma::get<std::string>(v_payload).data());

    ma_message_release(my_message);
}
