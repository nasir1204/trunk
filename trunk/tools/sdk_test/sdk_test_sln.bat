@echo off
call sdk_env_vars.bat
if "%SDK_TEST_BASE%"=="" goto error1
if "%MA_SDK_BASE%"=="" goto error2

start sdk_test.sln

goto end

:error1
echo Error: SDK_TEST_BASE path not defined

:error2
echo Error: MA_SDK_BASE path not defined

:end
