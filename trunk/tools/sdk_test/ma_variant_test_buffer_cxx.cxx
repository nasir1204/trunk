/*****************************************************************************
This file contains the unit tests for the character and wide character buffer
APIs from the Variant library.
******************************************************************************/
#include "ma/buffer.hxx"
#include "gtest/gtest.h"

TEST(ma_variant_buffer_tests, ma_buffer_create_set_get_string_test)
{
    /* 'ma_buffer_t' type Pointer for storing the base address of buffer */
    ma_buffer_t *buffer_ptr = NULL;

    struct LookupTable
    {
        size_t test_buffer_size;
        char *test_string;
        size_t test_string_size;
        ma_error_t ret_value;
    };

    const struct LookupTable test_vector[] =
    {
        { 20,             "MA Security Agent",                                                                    17,                  MA_OK               },
        { 40,             "McAfee Security 12345654569587Agent",                                                  35,                  MA_OK               },
        { 26,             "MCAFEE SECURITY AGENT 1.0",                                                            25,                  MA_OK               },
        { 21,             "12345678901234567890",                                                                 20,                  MA_OK               },
        { 25,             "!@@#$%%^^&*&&(*(**(**)()",                                                             24,                  MA_OK               },
        { 15,             "ABCD1234$%@$%",                                                                        13,                  MA_OK               },
        { 30,             "~!@#$%^&*$^%^%$^#^%&^%&^%",                                                            25,                  MA_OK               },
        { 0,              "",                                                                                     0,                   MA_OK               },
        { 100,            "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghi jklmnopqrstuvwxyz1234567890!@#$%^&*()_+-=<>?:\"",  82,                  MA_OK               },
    };

    /*--------------------------------------------------------------------------*/
	{
	    ma_buffer_create(&buffer_ptr, 0);
        mileaccess::ma::buffer b(buffer_ptr, false);
        ASSERT_EQ(0, b.size());
	    ASSERT_STREQ("", b.data());
	}

    /*--------------------------------------------------------------------------*/
    for(ma_uint8_t counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
		ma_buffer_create(&buffer_ptr, test_vector[counter].test_buffer_size);
        ma_buffer_set(buffer_ptr, test_vector[counter].test_string, test_vector[counter].test_string_size);
        mileaccess::ma::buffer b(buffer_ptr, true);
        ASSERT_EQ(test_vector[counter].test_string_size, b.size());
		ASSERT_STREQ(test_vector[counter].test_string, b.data());
		ma_buffer_release(buffer_ptr);
		buffer_ptr = NULL;
	}
    /*--------------------------------------------------------------------------*/
}

//TEST(ma_variant_buffer_tests, ma_buffer_create_set_get_raw_test)
//{
//    /* 'ma_buffer_t' type Pointer for storing the base address of buffer */
//    ma_buffer_t *buffer_ptr = NULL;
//
//    /* To hold the raw stream and its size, as returned by the ma_buffer_get_raw
//       macro */
//    const unsigned char *raw_val = 0;
//    size_t raw_size = 0;
//
//    /* To hold the size of the buffer */
//    size_t buffer_size = 0;
//
//    /* For storing the result of input and output raw stream comparison. */
//    ma_bool_t comp_result = 1;
//
//    /* An instance of this structure acts as the raw buffer stream */
//    struct test_struct
//    {
//        ma_uint8_t int_data;
//        char char_data[300];
//        ma_float_t float_data;
//        ma_bool_t boolean_data;
//    };
//
//    struct test_struct raw_buffer = { 0x12,
//                                      "This is a Next-Gen McAfee Agent software - Variant Library raw buffer test."
//                                      "This is a Next-Gen McAfee Agent software - Variant Library raw buffer test."
//                                      "This is a Next-Gen McAfee Agent software - Variant Library raw buffer test."
//                                      "!@#$%^&*()_+01234567890123456789",
//                                      2.3f,
//                                      1};
//
//
//    /*--------------------------------------------------------------------------*/
//    /* Creating buffer for size more than the size of raw stream */
//    TEST_ASSERT(ma_buffer_create(&buffer_ptr, sizeof(raw_buffer) + 100) == MA_OK);
//    TEST_ASSERT(ma_buffer_size(buffer_ptr, &buffer_size) == MA_OK);
//    TEST_ASSERT(sizeof(raw_buffer) + 100 == buffer_size);
//    TEST_ASSERT(ma_buffer_set(buffer_ptr, (const char *)&raw_buffer, sizeof(raw_buffer)) == MA_OK);
//
//    /* Invalid condition check */
//    TEST_ASSERT(ma_buffer_get_raw(NULL, &raw_val, &raw_size) == MA_ERROR_INVALIDARG);
//    TEST_ASSERT(ma_buffer_get_raw(buffer_ptr, NULL, &raw_size) == MA_ERROR_INVALIDARG);
//    TEST_ASSERT(ma_buffer_get_raw(buffer_ptr, (const unsigned char**)&raw_val, NULL) == MA_ERROR_INVALIDARG);
//
//    /* Valid condition check */
//    TEST_ASSERT(ma_buffer_get_raw(buffer_ptr, &raw_val, &raw_size) == MA_OK);
//
//    /* Size of raw stream should be equal to size of buffer */
//    TEST_ASSERT(buffer_size == raw_size);
//
//    for(ma_uint8_t counter = 0; counter < sizeof(raw_buffer); counter++)
//    {
//        // Typecasting structure instance to raw stream
//        if(((const unsigned char*)&raw_buffer)[counter] != raw_val[counter])
//        {
//            comp_result = 0;
//        }
//    }
//
//    TEST_ASSERT(comp_result == 1);
//
//    ma_buffer_release(buffer_ptr);
//    /*--------------------------------------------------------------------------*/
//}


///*
//Unit tests for ma_buffer_is_equal macro (for strings).
//*/
//TEST(ma_variant_buffer_tests, ma_buffer_is_equal_test)
//{
//    /* 'ma_buffer_t' type Pointer for storing the base address of buffer */
//    ma_buffer_t *buffer_ptr_1 = NULL;
//    ma_buffer_t *buffer_ptr_2 = NULL;
//
//    /* Loop iterator. */
//    ma_uint8_t counter = 0;
//
//    /* Variable to hold the results */
//    ma_bool_t comparison_result = MA_FALSE;
//
//    struct LookupTable
//    {
//        size_t test_buffer_size_1;
//        char *test_string_1;
//        size_t test_buffer_size_2;
//        char *test_string_2;
//        ma_error_t ret_value;
//        int exp_result;
//    };
//
//    const struct LookupTable test_vector[] =
//    {
//        { 50, "MA Security Agent",                              40, "MA Security Agent",                     MA_OK,                 MA_FALSE            },
//        { 30, "McAfee Security 12345654569587Agent",            30, "McAfee Security 12345654569587",        MA_OK,                 MA_TRUE             },
//        { 25, "890!@#$%^&*()_+-=<>?:\"",                        25, "890!@#$%^&*()_+-=<>?:\"",               MA_OK,                 MA_TRUE             },
//        { 50, "MCAFEE SECURITY AGENT",                          50, "MCAFEE SECURITY AGENt",                 MA_OK,                 MA_FALSE            },
//        { 25, "12345678901234567890",                           25, "12345678901234567890",                  MA_OK,                 MA_TRUE             },
//        { 50, "~!@#$%^&*$^%^%$^#^%&^%&^%",                      50, "!@#$%^&*$^%^%$^#^%&^%&^%",              MA_OK,                 MA_FALSE            },
//        { 50, "MCAFEE SECURITY AGENT 1.0",                      50, "MCAFEE SECURITY AGENT1.0",              MA_OK,                 MA_FALSE            },
//        { 50, "!@@#$%%^^&*&&(*(**(**)()",                       50, "!@@#$%%^^&*&&(*(**(**)()",              MA_OK,                 MA_TRUE             },
//        { 50, "ABCD1234$%@$%",                                  50, "ABCD1234$%@$%",                         MA_OK,                 MA_TRUE             },
//        { 10, "",                                               10, "",                                      MA_OK,                 MA_TRUE             },
//    };
//
//    /*--------------------------------------------------------------------------*/
//
//    for(counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
//    {
//        TEST_ASSERT(ma_buffer_create(&buffer_ptr_1, test_vector[counter].test_buffer_size_1) == MA_OK);
//        TEST_ASSERT(ma_buffer_create(&buffer_ptr_2, test_vector[counter].test_buffer_size_2) == MA_OK);
//        TEST_ASSERT(ma_buffer_set(buffer_ptr_1, test_vector[counter].test_string_1, test_vector[counter].test_buffer_size_1) == MA_OK);
//        TEST_ASSERT(ma_buffer_set(buffer_ptr_2, test_vector[counter].test_string_2, test_vector[counter].test_buffer_size_2) == MA_OK);
//        TEST_ASSERT(test_vector[counter].ret_value == ma_buffer_is_equal(buffer_ptr_1, buffer_ptr_2, &comparison_result));
//        TEST_ASSERT(comparison_result == test_vector[counter].exp_result);
//
//        /* Deallocating buffer */
//        ma_buffer_release(buffer_ptr_1);
//        ma_buffer_release(buffer_ptr_2);
//    }
//    /*--------------------------------------------------------------------------*/
//
//}
//
///*
//Unit tests for ma_buffer_is_equal macro (for raw stream).
//*/
//TEST(ma_variant_buffer_tests, ma_buffer_is_equal_test_raw_stream)
//{
//    /* 'ma_buffer_t' type Pointer for storing the base address of buffer */
//    ma_buffer_t *buffer_ptr_1 = NULL;
//    ma_buffer_t *buffer_ptr_2 = NULL;
//    ma_buffer_t *buffer_ptr_3 = NULL;
//    ma_buffer_t *buffer_ptr_4 = NULL;
//
//    /* Variable to hold the results */
//    ma_bool_t comparison_result = MA_FALSE;
//
//    struct test_struct
//    {
//        ma_uint8_t int_data;
//        char char_data[300];
//        ma_float_t float_data;
//        ma_bool_t boolean_data;
//    };
//
//    struct test_struct raw_buffer_1 = { 0x12,
//                                        "This is a Next-Gen McAfee Agent software - Variant Library raw buffer test."
//                                        "This is a Next-Gen McAfee Agent software - Variant Library raw buffer test."
//                                        "This is a Next-Gen McAfee Agent software - Variant Library raw buffer test."
//                                        "!@#$%^&*()_+01234567890123456789",
//                                        2.3f,
//                                        1};
//
//    // Same as raw_buffer_1
//    struct test_struct raw_buffer_2 =  { 0x12,
//                                        "This is a Next-Gen McAfee Agent software - Variant Library raw buffer test."
//                                        "This is a Next-Gen McAfee Agent software - Variant Library raw buffer test."
//                                        "This is a Next-Gen McAfee Agent software - Variant Library raw buffer test."
//                                        "!@#$%^&*()_+01234567890123456789",
//                                        2.3f,
//                                        1};
//
//    // String member is different from raw_buffer_1
//    struct test_struct raw_buffer_3 =  { 0x12,
//                                        "This is a Next-Gen McAfee Agent software - Variant Library raw buffer test."
//                                        "This is a Next-Gen McAfee Agent software - Variant Library raw buffer test."
//                                        "This is a Next-Gen McAfee Agent software - Variant Library raw buffer test."
//                                        "!@#$%^&*()_+0123456789012345678",
//                                        2.3f,
//                                        1};
//
//    // Float member is different from raw_buffer_1
//    struct test_struct raw_buffer_4 =  { 0x12,
//                                        "This is a Next-Gen McAfee Agent software - Variant Library raw buffer test."
//                                        "This is a Next-Gen McAfee Agent software - Variant Library raw buffer test."
//                                        "This is a Next-Gen McAfee Agent software - Variant Library raw buffer test."
//                                        "!@#$%^&*()_+01234567890123456789",
//                                        -2.3f,
//                                        1};
//
//    /*--------------------------------------------------------------------------*/
//
//    // Creating buffers of size more than size of the raw stream
//    TEST_ASSERT(ma_buffer_create(&buffer_ptr_1, sizeof(raw_buffer_1) + 1) == MA_OK);
//    TEST_ASSERT(ma_buffer_create(&buffer_ptr_2, sizeof(raw_buffer_2) + 1) == MA_OK);
//    TEST_ASSERT(ma_buffer_create(&buffer_ptr_3, sizeof(raw_buffer_3) + 1) == MA_OK);
//    TEST_ASSERT(ma_buffer_create(&buffer_ptr_4, sizeof(raw_buffer_4) + 1) == MA_OK);
//
//    // Setting known raw stream in buffers
//    TEST_ASSERT(ma_buffer_set(buffer_ptr_1, (const char *)&raw_buffer_1, sizeof(raw_buffer_1)) == MA_OK);
//    TEST_ASSERT(ma_buffer_set(buffer_ptr_2, (const char *)&raw_buffer_2, sizeof(raw_buffer_2)) == MA_OK);
//    TEST_ASSERT(ma_buffer_set(buffer_ptr_3, (const char *)&raw_buffer_3, sizeof(raw_buffer_3)) == MA_OK);
//    TEST_ASSERT(ma_buffer_set(buffer_ptr_4, (const char *)&raw_buffer_4, sizeof(raw_buffer_4)) == MA_OK);
//
//    /* Checking for invalid arguments */
//    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_buffer_is_equal(NULL, buffer_ptr_2, &comparison_result));
//    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_buffer_is_equal(buffer_ptr_1, NULL, &comparison_result));
//    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_buffer_is_equal(buffer_ptr_1, buffer_ptr_2, NULL));
//
//    /* Valid equality comparisons */
//    TEST_ASSERT(MA_OK == ma_buffer_is_equal(buffer_ptr_1, buffer_ptr_2, &comparison_result));
//    TEST_ASSERT(comparison_result == MA_TRUE);
//
//    TEST_ASSERT(MA_OK == ma_buffer_is_equal(buffer_ptr_1, buffer_ptr_3, &comparison_result));
//    TEST_ASSERT(comparison_result == MA_FALSE);
//
//    TEST_ASSERT(MA_OK == ma_buffer_is_equal(buffer_ptr_1, buffer_ptr_4, &comparison_result));
//    TEST_ASSERT(comparison_result == MA_FALSE);
//
//    /* Deallocating buffer */
//    ma_buffer_release(buffer_ptr_1);
//    ma_buffer_release(buffer_ptr_2);
//    ma_buffer_release(buffer_ptr_3);
//    ma_buffer_release(buffer_ptr_4);
//    /*--------------------------------------------------------------------------*/
//}
//

TEST(ma_variant_buffer_tests, ma_wbuffer_create_and_set_test)
{
    /* 'ma_wbuffer_t' type Pointer for storing the base address of buffer */
    ma_wbuffer_t *wbuffer_ptr = NULL;

    struct LookupTable
    {
        size_t test_wbuffer_size;
        wchar_t *test_wstring;
        size_t test_wstring_size;
        ma_error_t ret_value;
    };

    const struct LookupTable test_vector[] =
    {
        { 20,             L"MA Security Agent",                                                                    17,                  MA_OK               },
        { 40,             L"McAfee Security 12345654569587Agent",                                                  35,                  MA_OK               },
        { 26,             L"MCAFEE SECURITY AGENT 1.0",                                                            25,                  MA_OK               },
        { 21,             L"1234567890123456789\x0041",                                                            20,                  MA_OK               },
        { 25,             L"!@@#$%%^^&*&&(*(**(**)()",                                                             24,                  MA_OK               },
        { 15,             L"ABCD1234$%@$%",                                                                        13,                  MA_OK               },
        { 30,             L"~!@#$%^&*$^%^%$^#^%&^%&^%",                                                            25,                  MA_OK               },
        { 1,              L"",                                                                                     0,                   MA_OK               },
        { 100,            L"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghi jklmnopqrstuvwxyz1234567890!@#$%^&*()_+-=<>?:\"",  82,                  MA_OK               },
    };

    /*--------------------------------------------------------------------------*/
	{
	    ma_wbuffer_create(&wbuffer_ptr, 0);
        mileaccess::ma::wbuffer b(wbuffer_ptr, false);
        ASSERT_EQ(0, b.size());
		ASSERT_STREQ(L"", b.data());
	}

    /*--------------------------------------------------------------------------*/
    for(ma_uint8_t counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
		ma_wbuffer_create(&wbuffer_ptr, test_vector[counter].test_wbuffer_size);
        ma_wbuffer_set(wbuffer_ptr, test_vector[counter].test_wstring, test_vector[counter].test_wstring_size);
        mileaccess::ma::wbuffer b(wbuffer_ptr, true);
        ASSERT_EQ(test_vector[counter].test_wstring_size, b.size());
		ASSERT_STREQ(test_vector[counter].test_wstring, b.data());
		ma_wbuffer_release(wbuffer_ptr);
		wbuffer_ptr = NULL;
	}
    /*--------------------------------------------------------------------------*/
}

///*
//Unit tests for ma_wbuffer_is_equal macro.
//*/
//
//TEST(ma_variant_buffer_tests, ma_wbuffer_is_equal_test)
//{
//    /* 'ma_wbuffer_t' type Pointer for storing the base address of buffer */
//    ma_wbuffer_t *wbuffer_ptr_1 = NULL;
//    ma_wbuffer_t *wbuffer_ptr_2 = NULL;
//
//    /* Loop iterator. */
//    ma_uint8_t counter = 0;
//
//    /* Variable to hold the results */
//    ma_bool_t comparison_result = MA_FALSE;
//
//    struct LookupTable
//    {
//        size_t test_wbuffer_size_1;
//        wchar_t *test_wstring_1;
//        size_t test_wbuffer_size_2;
//        wchar_t *test_wstring_2;
//        ma_error_t ret_value;
//        int exp_result;
//    };
//
//    const struct LookupTable test_vector[] =
//    {
//        { 50, L"MA Security Agent",                              40, L"MA Security Agent",                     MA_OK,                 MA_FALSE            },
//        { 50, L"McAfee Security 12345654569587Agent",            50, L"McAfee Security 12345654569587",        MA_OK,                 MA_FALSE            },
//        { 50, L"MCAFEE SECURITY AGENT",                          50, L"MCAFEE SECURITY AGENt",                 MA_OK,                 MA_FALSE            },
//        { 50, L"MCAFEE SECURITY AGENT 1.0",                      50, L"MCAFEE SECURITY AGENT1.0",              MA_OK,                 MA_FALSE            },
//        { 25, L"12345678901234567890",                           25, L"12345678901234567890",                  MA_OK,                 MA_TRUE             },
//        { 50, L"!@@#$%%^^&*&&(*(**(**)()\x212B",                 50, L"!@@#$%%^^&*&&(*(**(**)()\x212B",        MA_OK,                 MA_TRUE             },
//        { 50, L"ABCD1234$%@$%",                                  50, L"ABCD1234$%@$%",                         MA_OK,                 MA_TRUE             },
//        { 50, L"~!@#$%^&*$^%^%$^#^%&^%&^%",                      50, L"!@#$%^&*$^%^%$^#^%&^%&^%",              MA_OK,                 MA_FALSE            },
//        { 10, L"",                                               10, L"",                                      MA_OK,                 MA_TRUE             },
//        { 25, L"890!@#$%^&*()_+-=<>?:\"",                        25, L"890!@#$%^&*()_+-=<>?:\"",               MA_OK,                 MA_TRUE             },
//    };
//
//    /*--------------------------------------------------------------------------*/
//
//    for(counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
//    {
//        TEST_ASSERT(ma_wbuffer_create(&wbuffer_ptr_1, test_vector[counter].test_wbuffer_size_1) == MA_OK);
//        TEST_ASSERT(ma_wbuffer_create(&wbuffer_ptr_2, test_vector[counter].test_wbuffer_size_2) == MA_OK);
//        TEST_ASSERT(ma_wbuffer_set(wbuffer_ptr_1, test_vector[counter].test_wstring_1, test_vector[counter].test_wbuffer_size_1) == MA_OK);
//        TEST_ASSERT(ma_wbuffer_set(wbuffer_ptr_2, test_vector[counter].test_wstring_2, test_vector[counter].test_wbuffer_size_2) == MA_OK);
//        TEST_ASSERT(test_vector[counter].ret_value == ma_wbuffer_is_equal(wbuffer_ptr_1, wbuffer_ptr_2, &comparison_result));
//        TEST_ASSERT(comparison_result == test_vector[counter].exp_result);
//
//        /* Deallocating buffer which was allocated in Test 4 */
//        ma_wbuffer_release(wbuffer_ptr_1);
//        ma_wbuffer_release(wbuffer_ptr_2);
//    }
//    /*--------------------------------------------------------------------------*/
//}
//
