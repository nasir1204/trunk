/*****************************************************************************
Main Test runner for all Variant library unit tests.
******************************************************************************/

#include "unity.h"
#include "unity_fixture.h"
#include "ma/ma_dispatcher.h"

#ifdef MA_WINDOWS
#include <windows.h>
#endif
#ifdef _MSC_VER
# include <crtdbg.h> /* */
# define MA_TRACKLEAKS _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF)
#else
# define MA_TRACKLEAKS ((void *)0)
#endif


static void run_all_tests(void) 
{
    // Variant Tests	
	do {RUN_TEST_GROUP(ma_variant_basic_types_test_group); } while (0);
    do {RUN_TEST_GROUP(ma_variant_raw_test_group);} while (0);
    do {RUN_TEST_GROUP(ma_variant_string_test_group);} while (0);
    do {RUN_TEST_GROUP(ma_variant_array_test_group);} while (0);
    do {RUN_TEST_GROUP(ma_variant_table_test_group);} while (0);
    do {RUN_TEST_GROUP(ma_variant_buffer_test_group); } while (0);

	// Proxy Tests
    do {RUN_TEST_GROUP(ma_proxy_tests_group);} while (0);
    do {RUN_TEST_GROUP(ma_proxy_config_tests_group);} while (0);

	// Property Client Tests
	do {RUN_TEST_GROUP(property_bag_tests_group);} while (0);
}

ma_dispatcher_t *my_handle = NULL;

int main(int argc, char *argv[]) {

	ma_dispatcher_initialize(NULL, &my_handle);

	MA_TRACKLEAKS;
    return UnityMain(argc, argv, run_all_tests);

	ma_dispatcher_deinitialize(my_handle);
}
