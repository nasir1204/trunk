/******************************************************************************
1) A generic implementation to test the message bus functionality. Please refer
   to documentation throughout this file to understand the functionality.

   For execution commands, see the 'help_string' below or execute following
   command -
   msgbus_performance_test -h

2) This implementation uses ma_msgbus_start() API which runs message bus in a
   different thread (non-blocking)

3) Use Debug mode, to enable extra logs from this tool as well as message bus.
******************************************************************************/
#include <iostream>
#include <string.h>
#include "ma/message.hxx"
#include "ma/msgbus/msgbus.hxx"
#include "ma/logger/ma_file_logger.h"
#include "uv.h"
#include <time.h>

using namespace std;

#ifdef WIN32
    #include <Windows.h>
#else
    #include <unistd.h>
#endif

// Facility Name.
// To be used for Logging and as Product ID.
#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "MESSAGEBUS_PERFORMANCE_TEST"

// Externally link the following functions from the ma_getopt.c.
// These functions are needed for parsing the command-line arguments.
extern int ma_getopt(int argc, char *argv[], char *optstring);
extern const char *ma_getoptarg();

/******************************************************************************
Defines the arguments received by the main function in this file. These parameters
decide the type of message bus being created.
******************************************************************************/
enum { SUBSCRIBER = 0, PUBLISHER = 1, SYNC_CLIENT = 2, ASYNC_CLIENT = 3, SEND_FORGET_CLIENT = 4, SERVER = 5 } endpoint_type;
static const char *endpoint_name = NULL;
static ma_msgbus_consumer_reachability_t reach = MSGBUS_CONSUMER_REACH_OUTPROC;
static ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;
static long int total_run_duration = 300;      // Default Value
static long int message_frequency_in_ms = 100; // Default Value
static long int message_size_in_bytes = 100;   // Default value
static const char *server_to_communicate = NULL;
static const char *host_name = NULL;
static const char *topic = NULL;

/******************************************************************************
Object pointers for various endpoint types
******************************************************************************/
static mileaccess::ma::msgbus msgbus_obj(LOG_FACILITY_NAME);                                          // Message Bus
static mileaccess::ma::msgbus_subscriber p_subscriber;                                                // Message Bus Subscriber
static mileaccess::ma::msgbus_server p_server(endpoint_name);                                         // Message Bus Server
static mileaccess::ma::msgbus_endpoint p_endpoint(msgbus_obj, server_to_communicate, host_name);      // Message Bus Client

/******************************************************************************
total_msg_published                - Total no. of messages published by the
                                     publisher for given topic.
total_msg_sub_rcvd                 - Total messages received by subscriber who
                                     is listening to a particular topic
total_client_req_sent              - Total number of requests (sync/async/send
                                     and forget) sent by client.
total_req_handled_server           - Total number of requests received and processed
                                     by server. Note that if the same server is
                                     handling requests from multiple clients, this
                                     number may be considerably high.
total_resp_recvd_client            - Number of responses received by client for
                                     the requests sent. Note - No responses are
                                     expected for 'send and forget' type client.
******************************************************************************/
static unsigned long long int total_msg_published = 0;
static unsigned long long int total_msg_sub_rcvd = 0;
static unsigned long long int total_client_req_sent = 0;
static unsigned long long int total_req_handled_server = 0;
static unsigned long long int total_resp_recvd_client = 0;

/******************************************************************************
Libuv Variables for managing the timing for which the endpoint will be executing.
******************************************************************************/
static uv_loop_t *wait_loop = NULL;
static uv_timer_t periodic_timer;
static uv_timer_t exit_timer;

/******************************************************************************
Utility function for generating random numbers.

Returns a random value between 0 and passed input value (excluding the input
value) on every function call.
******************************************************************************/
int generate_random_number(int max_value_exclusive)
{
    static int count = 0;

    if(count == 0)
    {
        srand((unsigned int)time(NULL));
        count++;
    }
    return(rand() % max_value_exclusive);
}

/******************************************************************************
Utility function for generating random messages. Fills the passed string of given
length with any one character from the below character set.

NOTE : The user can replace this function with its own utility function.
******************************************************************************/
const char charset[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*()+_:<>?";

void generate_message(char *str, unsigned int length)
{
    unsigned int counter = 0;

    char mychar = 'a';

    mychar = charset[generate_random_number((int)strlen(charset))];

    for(counter = 0; counter < length; counter++)
    {
        str[counter] = mychar;
    }
}

/******************************************************************************
Utility function to print help and usage information.
******************************************************************************/
void print_help()
{
    printf("\n\t !!! Welcome to Message Bus Performance Test !!!\n\n");
    printf("Usage Instructions : \n\n");
    printf("Command - msgbus_performance_test <Options>....\n\n");

    printf("-e   Endpoint Type [PUBLISHER/SUBSCRIBER/ASYNC_CLIENT/SYNC_CLIENT/SEND_FORGET_CLIENT/SERVER] Default - SUBSCRIBER \n");
    printf("-n   Endpoint Name                                                                           Mandatory \n");
    printf("-r   Reachability  [INRPOC/OUTPROC/OUTBOX]                                                   Default - OUTPROC \n");
    printf("-t   Thread option [THREAD_POOL/THREAD_IO]                                                   Default - Thread Pool \n");
    printf("-F   Total Time for which end-point will be running (in seconds)                             Default - 5 Minutes (300 Seconds) \n");
    printf("-f   Message Frequency (in milli-seconds)                                                    Default - 100 ms \n");
    printf("-m   Message Size (in Bytes)                                                                 Default - 100 Bytes \n");
    printf("-S   Server Name                                                                             Mandatory for Clients \n");
    printf("-T   Topic to be published/subscribed                                                        Mandatory for Subscribers/Publishers \n");
    /*printf("-H   Host Name, can be NULL for INPROC and OUTPROC communication. This argument              Mandatory for Server. Can be ignored for INPROC and OUTPROC \n");
    printf("     is not parsed currently. \n");*/
    printf("-h   Prints Help                                                                             Optional \n");
    return;
}

/******************************************************************************
Utility function for generating response to the received message. Converts the
upper case letters in given string to lower case letters and vice-versa. Numbers
and alphanumeric characters remain unchanged.

NOTE : This function simulates as if the server is doing some processing before
sending the response message. The user can replace this function with its own
utility function.
******************************************************************************/
void generate_response(char *str)
{
    int count = 0;

    while(str[count] != '\0')
    {
        int int_value = (int)str[count];

        if((int_value >= 65) && (int_value <= 90)) str[count] = (char)(int_value + 32);

        if((int_value >= 97) && (int_value <= 122)) str[count] = (char)(int_value - 32);

        count++;
    }
}

/******************************************************************************
Publishing the Process details of current process into a file, so that the data
can be consumed by the monitoring code.
******************************************************************************/
void dump_pid_info()
{
    FILE * pid_info_file_handle = NULL;
    char * pid_info_file_name = "pid_list.txt";

    #ifdef WIN32
       DWORD pid_number = GetCurrentProcessId();
    #else
       pid_t pid_number = getpid(void);
    #endif

    pid_info_file_handle = fopen(pid_info_file_name, "a+");
    fprintf(pid_info_file_handle, "%s \t %d\n", endpoint_name, pid_number);
    fclose(pid_info_file_handle);
}

/******************************************************************************
Publisher function which publishes a single message on the message bus.
******************************************************************************/
//void publish()
//{
//    char value[10];
//	// Increment the count of the message being published
//    total_msg_published++;
//	
//    // Allocate memory for storing the message payload (1 extra byte for NULL).
//    // Fill payload with random data.
//    // Create variant of string.
//    char *my_string = new char[message_size_in_bytes + 1];
//    generate_message(my_string, message_size_in_bytes);    
//	mileaccess::ma::variant v(my_string);  
//
//    // Creating fresh message.
//    // Set Publisher Name and Message Number in the message property.
//    // Set string variant as message payload and then release variant
//    // Publish the Message on message Bus
//    mileaccess::ma::message m;
//	m.set_property("PUBLISHER_NAME", endpoint_name);
//
//	sprintf(value, "%d", total_msg_published);
//	m.set_property("MESSAGE_NUMBER", value);
//
//	m.set_payload(v);
//
//	msgbus_obj.publish();
//
//    ma_message_set_payload(publisher_payload, variant_ptr);
//    ma_variant_release(variant_ptr);
//    ma_msgbus_publish(msgbus_obj, topic, reach, publisher_payload);
//
//
//    // Cleanup code
//    ma_message_release(publisher_payload);    publisher_payload = NULL;
//    delete []my_string; my_string = NULL;
//}


/******************************************************************************
Subscriber callback function. This callback function gets executed when the
subscriber receives a message corresponding to the topic it had subscribed for.
******************************************************************************/
struct sub_handler : public mileaccess::ma::msgbus_subscriber::handler 
{       
    ma_error_t on_pubsub_message(char const *topic, mileaccess::ma::message const &msg_payload);
};

ma_error_t sub_handler::on_pubsub_message(char const *topic, mileaccess::ma::message const &msg_payload)
{
    // Incrementing count of number of messages received by subscriber
    total_msg_sub_rcvd++;

    std::string publisher_name = msg_payload.get_property("PUBLISHER_NAME");
	std::string message_number = msg_payload.get_property("MESSAGE_NUMBER");

    mileaccess::ma::variant v = msg_payload.get_payload();
	std::string my_string = mileaccess::ma::get<std::string>(v).data();

    cout<<"Subscriber - "<<endpoint_name<<"Received Message - "<<message_number<<"from Publisher - "<<publisher_name<<endl;

    return MA_OK;
}

/******************************************************************************
Client call back function. This function gets executed -
1) as a call back function, whenever an asynchronous client receives response
   from server.
2) called by send_client_request() function in this sample code to process the
   response packet received by synchronous client.
******************************************************************************/
ma_error_t process_server_response(ma_error_t status, ma_message_t *response_payload, void *cb_data, ma_msgbus_request_t *request)
{
    cout<<"Hello"<<endl;
	//// Variable to hold current time
    //time_t current_time;

    //// Getting current time. Obtain the time as soon as possible for better accurancy.
    //// Here, the time is obtained as the first executable statement in the call-back function.
    //time(&current_time);

    //// Incrementing count of number of responses received by client
    //total_resp_recvd_client++;

    //// Handling the message payload
    //if(response_payload == NULL)
    //{
    //    printf("No payload in the response received from server\n");
    //    return MA_ERROR_UNEXPECTED;
    //}

    //{
    //    // Variant to hold the variant extracted from message
    //    ma_variant_t *variant_ptr = NULL;

    //    // To hold the property retrieved from variant
    //    const char *client_name = NULL;
    //    const char *request_number = NULL;

    //    // Buffer to hold string extracted from variant
    //    ma_buffer_t *buffer = NULL;

    //    // String Message retrieved from the buffer and its size
    //    const char *my_string = NULL;
    //    size_t size = 0;

    //    ma_message_get_property(response_payload, "CLIENT_NAME", &client_name);
    //    ma_message_get_property(response_payload, "REQUEST_NUMBER", &request_number);
    //    ma_message_get_payload(response_payload, &variant_ptr);
    //    ma_variant_get_string_buffer(variant_ptr, &buffer);
    //    ma_buffer_get_string(buffer, &my_string, &size);
    //    ma_buffer_release(buffer); buffer = NULL;

    //    printf("Client - %s received Response for Request No. - %d \n", endpoint_name, atoi(request_number));

    //    // Cleanup code
    //    ma_variant_release(variant_ptr); variant_ptr = NULL;
    //}

    return MA_OK;
}


/******************************************************************************
Client function for sending the Request to server. This function sends a single
request message (sync/async or send & forget type) of given size on the message
bus.
******************************************************************************/
void send_client_request()
{
    char value[10];

    // Allocate memory for storing the message payload (1 extra byte for NULL).
    // Fill payload with random data.
    // Create variant of string.
    char *my_string = new char[message_size_in_bytes + 1];
    generate_message(my_string, message_size_in_bytes);    
	mileaccess::ma::variant v(my_string); 

    // Increment the count of the client requests sent
    total_client_req_sent++;

    // Creating fresh message.
    // Set Client Number and Request Number in the message property.
    // Set string variant as message payload and then release variant
    // Send the request message based on the client type.
    mileaccess::ma::message m;
	m.set_property("CLIENT_NAME", endpoint_name);

	sprintf(value, "%d", total_msg_published);
	m.set_property("MESSAGE_NUMBER", value);

	m.set_payload(v);
	
    // Sending correct request based on the Client type
    if(endpoint_type == SEND_FORGET_CLIENT) p_endpoint.send_and_forget(m);
    if(endpoint_type == SYNC_CLIENT) p_endpoint.send(m);
 //   if(endpoint_type == ASYNC_CLIENT) ma_msgbus_async_send(p_endpoint, request_payload, process_server_response, NULL, &request_packet);

    cout<<"Client - "<<endpoint_name<<"sent Request No. - "<<total_client_req_sent;

    // For synchronous client, process the received response.
    //if(endpoint_type == SYNC_CLIENT)
    //{
    //    process_server_response(MA_OK, response_payload, NULL, request_packet);
    //}

    // Cleanup code
    delete []my_string; my_string = NULL;
}


/******************************************************************************
Call back function to be executed when the server receives a request from the
client. This server will handle both synchronous as well as asynchronous requests.
******************************************************************************/
struct server_handler : public mileaccess::ma::msgbus_server::handler 
{       
    ma_error_t on_request(mileaccess::ma::msgbus_server &server, mileaccess::ma::message const &request_msg, ma_msgbus_client_request_t *c_request);
};

ma_error_t on_request(mileaccess::ma::msgbus_server &server, mileaccess::ma::message const &request_msg, ma_msgbus_client_request_t *c_request)
{    
    char *my_string = NULL;
    ma_message_t *my_message = NULL;

	// Incrementing count of number of requests handled by the server
    total_req_handled_server++;	

	// Processing incoming message to retrieve information
	std::string client_name = request_msg.get_property("CLIENT_NAME");
	std::string request_number = request_msg.get_property("REQUEST_NUMBER");

	mileaccess::ma::variant v = request_msg.get_payload();
	std::string retrieved_string = mileaccess::ma::get<std::string>(v).data();

	cout<<"Server - "<<endpoint_name<<"Received Request - "<<request_number<<"from Client - "<<client_name<<"Request - "<<retrieved_string<<endl;
	
    // Sending the response to client for incoming message
    
	// Generate response string by processing the payload obtained from request
    // Convert it to variant
	generate_response(my_string);
	mileaccess::ma::variant v_new(my_string);

    // Creating the response message
    ma_message_create(&my_message);
    mileaccess::ma::message m(my_message, true);
    m.set_property("CLIENT_NAME", client_name.c_str());
	m.set_property("REQUEST_NUMBER", request_number.c_str());
	m.set_payload(v_new);

	ma_msgbus_server_client_request_post_result(c_request, MA_OK, my_message);
    ma_message_release(my_message);


    // For cloning verification. To Test, create a subscriber to listen to "hello" topic
    //{
    //    ma_message_t *response1 = NULL;
    //    ma_message_t *response2 = NULL;

    //    ma_message_clone(request_payload, &response1);
    //    ma_message_clone(request_payload, &response2);

    //    ma_msgbus_server_client_request_post_result(c_request, MA_OK, response1);

    //    ma_msgbus_publish(msgbus_obj, "hello", MSGBUS_CONSUMER_REACH_OUTPROC, response2);
    //
    //    ma_message_release(response1);
    //    ma_message_release(response2);
    //}

    return MA_OK;
}


/******************************************************************************
Timer callback functions to be executed when this endpoint is running.

periodic_timer_callback - Will be called periodically by publisher/client to
send messages.

exit_timer_callback - Will be called only once when the total run duration
timer expires. This will stop all timers.
******************************************************************************/
void periodic_timer_callback(uv_timer_t* handle, int status)
{
//    if(endpoint_type == PUBLISHER) publish();
    if((endpoint_type == SYNC_CLIENT) || (endpoint_type == ASYNC_CLIENT) || (endpoint_type == SEND_FORGET_CLIENT)) send_client_request();
}

void exit_timer_callback(uv_timer_t* handle, int status)
{
    uv_timer_stop(&periodic_timer);
    uv_timer_stop(&exit_timer);
}


/******************************************************************************
Start of Execution -

1) Parse command line arguments.
2) Create message bus.
3) Create appropriate endpoint as per input command line arguments.
******************************************************************************/
int main(int argc, char* argv[])
{
    /**************************************************************************
    Processing Command Line Arguments.
    **************************************************************************/
    {
        int c;

        int counter = 0;

        while ((c = ma_getopt(argc, argv, "e:n:r:t:F:f:m:S:T:h")) != -1)
        {
            switch (c)
            {
                case 'e':      if(strcmp(ma_getoptarg(), "PUBLISHER") == 0) endpoint_type = PUBLISHER;
                          else if(strcmp(ma_getoptarg(), "SUBSCRIBER") == 0) endpoint_type = SUBSCRIBER;
                          else if(strcmp(ma_getoptarg(), "ASYNC_CLIENT") == 0) endpoint_type = ASYNC_CLIENT;
                          else if(strcmp(ma_getoptarg(), "SYNC_CLIENT") == 0) endpoint_type = SYNC_CLIENT;
                          else if(strcmp(ma_getoptarg(), "SEND_FORGET_CLIENT") == 0) endpoint_type = SEND_FORGET_CLIENT;
                          else if(strcmp(ma_getoptarg(), "SERVER") == 0) endpoint_type = SERVER;
                          else { print_help(); return -1; }
                          break;

                case 'n': endpoint_name = ma_getoptarg(); break;

                case 'r':      if(strcmp(ma_getoptarg(), "INPROC") == 0) reach = MSGBUS_CONSUMER_REACH_INPROC;
                          else if(strcmp(ma_getoptarg(), "OUTPROC") == 0) reach = MSGBUS_CONSUMER_REACH_OUTPROC;
                          else if(strcmp(ma_getoptarg(), "OUTBOX") == 0) reach = MSGBUS_CONSUMER_REACH_OUTBOX;
                            else reach = MSGBUS_CONSUMER_REACH_OUTPROC; // Default value
                          break;

                 case 't':      if(strcmp(ma_getoptarg(), "THREAD_POOL") == 0) thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;
                          else if(strcmp(ma_getoptarg(), "THREAD_IO") == 0) thread_option = MA_MSGBUS_CALLBACK_THREAD_IO;
                          else thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL; // Default value
                          break;

                case 'F': total_run_duration = atoi(ma_getoptarg()); break;

                case 'f': message_frequency_in_ms = atoi(ma_getoptarg()); break;

                case 'm': message_size_in_bytes = atoi(ma_getoptarg()); break;

                case 'S': server_to_communicate = ma_getoptarg(); break;

                case 'T': topic = ma_getoptarg(); break;

                case 'h': print_help(); return 0; break;

                case '?': cout<<"Unknown Command Line option"<<(char)c; break;
            }
        }

        if(!endpoint_name)
        {
            cout<<"Incomplete Options : Endpoint Name is not provided"<<endl;
            print_help();
            return -1;
        }

        if(((endpoint_type == SUBSCRIBER) || (endpoint_type == PUBLISHER))  && (topic == NULL))
        {
            cout<<"Incomplete Options : 'Topic' Missing"<<endl;
            print_help();
            return -1;
        }

        if(((endpoint_type == SYNC_CLIENT) || (endpoint_type == ASYNC_CLIENT) || (endpoint_type == SEND_FORGET_CLIENT)) &&
            (server_to_communicate == NULL))
        {
            cout<<"Incomplete Options : Provide Server Name to Communicate"<<endl;
            print_help();
            return -1;
        }

        #ifdef DEBUG
            cout<<"Endpoint Type - "<<endpoint_type<<endl;
            cout<<"Endpoint Name - "<<endpoint_name<<endl;
            cout<<"Endpoint Reach - "<<reach<<endl;
            cout<<"Thread Option - "<<thread_option<<endl;
            cout<<"Total Run Duration (in seconds) - "<<total_run_duration<<endl;
            cout<<"Message Frequency (in milli-seconds) - "<<message_frequency_in_ms<<endl;
            cout<<"Message Size (in bytes) - "<<message_size_in_bytes<<endl;
            cout<<"Server to Communicate - "<<server_to_communicate<<endl;
            cout<<"Host Name - "<<host_name<<endl;
            cout<<"Topic - "<<topic<<endl;
        #endif

    }

    /**************************************************************************
    Dumping PID info of currently running process in file.
    **************************************************************************/
    dump_pid_info();

    /**************************************************************************
    Create message Bus.
    Set Logger.
    Start Message Bus.
    **************************************************************************/
    mileaccess::ma::msgbus msgbus_obj(LOG_FACILITY_NAME);
	//ma_msgbus_create(LOG_FACILITY_NAME, &msgbus_obj);

    msgbus_obj.start();

    /**************************************************************************
    If endpoint is a SUBSCRIBER,
    1) Create a subscriber.
    2) Set reachability and thread model options. Set timeout equal to the total
       duration of test run, so that no message gets discarded due to timeout.
    3) Register for 'Topic' for listening.
    **************************************************************************/
    if(endpoint_type == SUBSCRIBER)
    {
		p_subscriber.create(msgbus_obj.get());
        p_subscriber.set_option(MSGBUSOPT_REACH, reach);
        p_subscriber.set_option(MSGBUSOPT_THREADMODEL, thread_option);
        
		sub_handler handle;
		p_subscriber.start(handle, topic);
    }

    /**************************************************************************
    If endpoint is a SERVER,
    1) Create a server
    2) Set reachability and thread model options. Set timeout equal to the total
       duration of test run, so that no message gets discarded due to timeout.
    3) Start server for listening to client requests (Register callback function).
    **************************************************************************/
    if(endpoint_type == SERVER)
    {
        p_server.create(msgbus_obj.get());
        p_server.set_option(MSGBUSOPT_REACH, reach);
        p_server.set_option(MSGBUSOPT_THREADMODEL, thread_option);

		server_handler handle;		
		p_server.start(handle);
    }

    /**************************************************************************
    If endpoint is a CLIENT,
    1) Create a client.
    2) Set reachability and thread model options. Set timeout equal to the total
       duration of test run, so that no message gets discarded due to timeout.
    **************************************************************************/
    if((endpoint_type == SYNC_CLIENT) || (endpoint_type == ASYNC_CLIENT) || (endpoint_type == SEND_FORGET_CLIENT))
    {
        p_endpoint.set_option(MSGBUSOPT_REACH, reach);
        p_endpoint.set_option(MSGBUSOPT_THREADMODEL, thread_option);
    }

    /**************************************************************************
    Run logic for executing this client till "total_run_duration" seconds.
    **************************************************************************/
    wait_loop = uv_loop_new();
    uv_timer_init(wait_loop, &exit_timer);
    uv_timer_init(wait_loop, &periodic_timer);

    // For server or subscriber, only exit timer is to be initialized. For clients
    // and publishers, both exit timer and message/request sending timer is also to
    // be initialized.
    if((endpoint_type == SERVER) || (endpoint_type == SUBSCRIBER))
    {
        uv_timer_start(&exit_timer, exit_timer_callback, total_run_duration * 1000, 0);
    }
    else
    {
        uv_timer_start(&exit_timer, exit_timer_callback, total_run_duration * 1000, 0);
        uv_timer_start(&periodic_timer, periodic_timer_callback, 0, message_frequency_in_ms);
    }

    uv_run(wait_loop, UV_RUN_DEFAULT);
    uv_loop_delete(wait_loop);

    /**************************************************************************
    Log the overall statistics after the CTRL+C is hit. This information can be
    used to identify the number of messages dropped during communication.
    **************************************************************************/
    switch(endpoint_type)
    {
        case PUBLISHER          : cout<<"Publisher No. - "<<endpoint_name;
                                  cout<<"Msg. Published - "<<total_msg_published<<endl;
                                  break;
        case SUBSCRIBER         : cout<<"Subscriber No. - "<<endpoint_name;
                                  cout<<"No. of Msg. Rcvd. - "<<total_msg_sub_rcvd<<endl;
                                  break;
        case ASYNC_CLIENT       :
        case SYNC_CLIENT        : cout<<"Client No. - "<<endpoint_name;
                                  cout<<"Requests Sent - "<<total_client_req_sent;
                                  cout<<"Response Rcvd. - "<<total_resp_recvd_client<<endl;
                                  break;
        case SEND_FORGET_CLIENT : cout<<"Client No. - "<<endpoint_name;
                                  cout<<"Requests Sent - "<<total_client_req_sent<<endl;
                                  break;
        case SERVER             : cout<<"Server No. - "<<endpoint_name;
                                  cout<<"No. of Req. handled - "<<total_req_handled_server<<endl;
                                  break;
    }

    /**************************************************************************
    Unregister and release the subscriber.
    **************************************************************************/
    if(endpoint_type == SUBSCRIBER)
    {
        p_subscriber.stop();
        p_subscriber.release();
    }

    /**************************************************************************
    Stop and release server.
    **************************************************************************/
    if(endpoint_type == SERVER)
    {
        p_server.stop();
        p_server.close();
    }

    /**************************************************************************
    Release the endpoint client.
    **************************************************************************/
    if((endpoint_type == SYNC_CLIENT) || (endpoint_type == ASYNC_CLIENT) || (endpoint_type == SEND_FORGET_CLIENT))
    {
        p_endpoint.close();
    }

    /**************************************************************************
    Release message-bus
    **************************************************************************/
    msgbus_obj.stop(MA_TRUE);
    msgbus_obj.close();
}





