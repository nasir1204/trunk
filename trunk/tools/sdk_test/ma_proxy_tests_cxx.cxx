/*
Proxy and Proxy List UTs
*/

#include "ma/repository/repository_client.hxx"
#include "gtest/gtest.h"

TEST(ma_proxy_tests, proxy_create_release_test)
{
	// Lookup Table consisting of Proxy Information
	struct LookupTable
	{
		std::string proxy_address;
		ma_int32_t proxy_port;
		ma_uint32_t proxy_scopeid;
		ma_proxy_protocol_type_t proxy_type;
		ma_int32_t proxy_flag;
		ma_bool_t proxy_auth;
		std::string proxy_user_name;
		std::string proxy_password;
	};

	struct LookupTable proxies[] =
	{
		// Address          Port      Scope ID  Type                Flag   Authentication      Username            Password
		{  "199.234.34.56", 10,       10,       MA_PROXY_TYPE_FTP,  1,     MA_TRUE,            "Administrator",    "wwehewlkheklneflke" },
		{  "199.202.18.34", 1000,     15,       MA_PROXY_TYPE_BOTH, 2,     MA_TRUE,            "Normal User",      "dedjds98fudufdp#$#" },
		{  "199.234.34.34", 100000,   1000,     MA_PROXY_TYPE_HTTP, 1,     MA_FALSE,           NULL,               NULL                 },
		{  "0.0.0.0",       1,        555,      MA_PROXY_TYPE_FTP,  0,     MA_TRUE,            "Administrator",    "#@$#@$#@$#@$#@$#@"  },
		{  "199.34.34.34",  83,       0,        MA_PROXY_TYPE_HTTP, 2,     MA_TRUE,            "User 1",           "#@$#@$#@$#@$#@$#@"  },
		{  "127.0.0.1",     8443,     234,      MA_PROXY_TYPE_BOTH, 0,     MA_FALSE,           NULL,               "lefe;lfelmelfemfle" }

	};

    for(ma_uint8_t counter = 0; counter < sizeof(proxies)/sizeof(proxies[0]); counter++)
    {
//		mileaccess::ma::Proxy p(false);
		printf("%d\n", counter);

//		p.set_address(proxies[counter].proxy_address);
		//p.set_port(proxies[counter].proxy_port);
		//p.set_scopeid(proxies[counter].proxy_scopeid);
		//p.set_protocol_type(proxies[counter].proxy_type);
		//p.set_flag(proxies[counter].proxy_flag);
		//p.set_authentication(proxies[counter].proxy_auth, proxies[counter].proxy_user_name, proxies[counter].proxy_password);
	

  //      {		
		//	std::string address;
		//	p.get_address(address);		

		//}
	}
}


    //        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_get_address(NULL, &address));
    //        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_get_address(my_proxy, NULL));
    //        TEST_ASSERT(MA_OK == ma_proxy_get_address(my_proxy, &address));
    //        TEST_ASSERT(strcmp(proxies[counter].proxy_address, address) == 0);
    //    }

    //    {
    //        ma_int32_t port = 0;
    //        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_get_port(NULL, &port));
    //        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_get_port(my_proxy, NULL));
    //        TEST_ASSERT(MA_OK == ma_proxy_get_port(my_proxy, &port));
    //        TEST_ASSERT(proxies[counter].proxy_port == port);
    //    }

    //    {
    //        ma_uint32_t scopeid = 0;
    //        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_get_scopeid(NULL, &scopeid));
    //        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_get_scopeid(my_proxy, NULL));
    //        TEST_ASSERT(MA_OK == ma_proxy_get_scopeid(my_proxy, &scopeid));
    //        TEST_ASSERT(proxies[counter].proxy_scopeid == scopeid);
    //    }

    //    {
    //        ma_proxy_protocol_type_t type = MA_PROXY_TYPE_BOTH;
    //        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_get_protocol_type(NULL, &type));
    //        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_get_protocol_type(my_proxy, NULL));
    //        TEST_ASSERT(MA_OK == ma_proxy_get_protocol_type(my_proxy, &type));
    //        TEST_ASSERT(proxies[counter].proxy_type == type);
    //    }

    //    {
    //        ma_int32_t flag = 0;
    //        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_get_flag(NULL, &flag));
    //        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_get_flag(my_proxy, NULL));
    //        TEST_ASSERT(MA_OK == ma_proxy_get_flag(my_proxy, &flag));
    //        TEST_ASSERT(proxies[counter].proxy_flag == flag);
    //    }

    //    {
    //        ma_bool_t auth = MA_FALSE;
    //        const char *username = NULL;
    //        const char *password = NULL;
    //        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_get_authentication(NULL, &auth, &username, &password));
    //        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_get_authentication(my_proxy, NULL, &username, &password));
    //        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_get_authentication(my_proxy, &auth, NULL, &password));
    //        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_get_authentication(my_proxy, &auth, &username, NULL));
    //        TEST_ASSERT(MA_OK == ma_proxy_get_authentication(my_proxy, &auth, &username, &password));
    //        TEST_ASSERT(proxies[counter].proxy_auth == auth);

    //        if(proxies[counter].proxy_auth)
    //        {
    //            TEST_ASSERT(strcmp(proxies[counter].proxy_user_name, username) == 0);
    //            TEST_ASSERT(strcmp(proxies[counter].proxy_password, password) == 0);
    //        }
    //        else
    //        {
    //            TEST_ASSERT_NULL(username);
    //            TEST_ASSERT_NULL(password);
    //        }
    //    }

    //    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_release(NULL));
    //    TEST_ASSERT(MA_OK == ma_proxy_release(my_proxy));
    //}

    //// If authentication is true, verifying that both username and password as present.
    //{
    //    TEST_ASSERT(MA_OK == ma_proxy_create(&my_proxy));
    //    TEST_ASSERT(MA_OK == ma_proxy_set_address(my_proxy, "10.213.254.84"));
    //    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_set_authentication(my_proxy, MA_TRUE, NULL, "we0p80-9cikpripou34-0"));
    //    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_set_authentication(my_proxy, MA_TRUE, "Test User with Admin Priviledges", NULL));
    //    TEST_ASSERT(MA_OK == ma_proxy_release(my_proxy));
    //}
//}

//TEST(ma_proxy_tests, proxy_list_create_release_test)
//{
//    ma_proxy_t *my_proxy = NULL;
//    ma_proxy_list_t *my_proxy_list = NULL;
//    ma_int32_t counter = 0;
//
//    // Creating Proxy List
//    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_list_create(NULL));
//    TEST_ASSERT(MA_OK == ma_proxy_list_create(&my_proxy_list));
//
//    // Creating a set of proxies, adding them to list and then releasing proxy object
//    // Verifying that the size of list increases by 1 in each iteration.
//    for(counter = 0; counter < sizeof(proxies)/sizeof(proxies[0]); counter++)
//    {
//        TEST_ASSERT(MA_OK == ma_proxy_create(&my_proxy));
//        TEST_ASSERT(MA_OK == ma_proxy_set_address(my_proxy, proxies[counter].proxy_address));
//        TEST_ASSERT(MA_OK == ma_proxy_set_port(my_proxy, proxies[counter].proxy_port));
//        TEST_ASSERT(MA_OK == ma_proxy_set_protocol_type(my_proxy, proxies[counter].proxy_type));
//        TEST_ASSERT(MA_OK == ma_proxy_set_flag(my_proxy, proxies[counter].proxy_flag));
//        TEST_ASSERT(MA_OK == ma_proxy_set_authentication(my_proxy, proxies[counter].proxy_auth,
//                                                                   proxies[counter].proxy_user_name,
//                                                                   proxies[counter].proxy_password));
//
//        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_list_add_proxy(NULL, my_proxy));
//        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_list_add_proxy(my_proxy_list, NULL));
//        TEST_ASSERT(MA_OK == ma_proxy_list_add_proxy(my_proxy_list, my_proxy));
//        TEST_ASSERT(MA_OK == ma_proxy_release(my_proxy));
//
//        {
//            size_t size = 0;
//            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_list_size(NULL, &size));
//            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_list_size(my_proxy_list, NULL));
//
//            TEST_ASSERT(MA_OK == ma_proxy_list_size(my_proxy_list, &size));
//            TEST_ASSERT((counter + 1) == size);
//        }
//    }
//
//    // Trying to add an empty proxy (not configured) into the proxy list
//    TEST_ASSERT(MA_OK == ma_proxy_create(&my_proxy));
//    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_list_add_proxy(my_proxy_list, my_proxy));
//    TEST_ASSERT(MA_OK == ma_proxy_release(my_proxy));
//
//     // Verifying the information stored in the proxy list using the proxy list object retrieved
//    // from the variant
//
//    // Retrieving proxies from the list
//    for(counter = 0; counter < sizeof(proxies)/sizeof(proxies[0]); counter++)
//    {
//        ma_proxy_t *new_proxy = NULL;
//
//        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_list_get_proxy(NULL, counter, &new_proxy));
//        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_list_get_proxy(my_proxy_list, counter, NULL));
//        TEST_ASSERT(MA_OK == ma_proxy_list_get_proxy(my_proxy_list, counter, &new_proxy));
//
//        {
//            const char *address = NULL;
//            TEST_ASSERT(MA_OK == ma_proxy_get_address(new_proxy, &address));
//            TEST_ASSERT(strcmp(proxies[counter].proxy_address, address) == 0);
//        }
//
//        {
//            ma_int32_t port = 0;
//            TEST_ASSERT(MA_OK == ma_proxy_get_port(new_proxy, &port));
//            TEST_ASSERT(proxies[counter].proxy_port == port);
//        }
//
//        {
//            ma_proxy_protocol_type_t type = MA_PROXY_TYPE_BOTH;
//            TEST_ASSERT(MA_OK == ma_proxy_get_protocol_type(new_proxy, &type));
//            TEST_ASSERT(proxies[counter].proxy_type == type);
//        }
//
//        {
//            ma_int32_t flag = 0;
//            TEST_ASSERT(MA_OK == ma_proxy_get_flag(new_proxy, &flag));
//            TEST_ASSERT(proxies[counter].proxy_flag == flag);
//        }
//
//        {
//            ma_bool_t auth = MA_FALSE;
//            const char *username = NULL;
//            const char *password = NULL;
//            TEST_ASSERT(MA_OK == ma_proxy_get_authentication(new_proxy, &auth, &username, &password));
//
//            TEST_ASSERT(proxies[counter].proxy_auth == auth);
//
//            if(proxies[counter].proxy_auth)
//            {
//                TEST_ASSERT(strcmp(proxies[counter].proxy_user_name, username) == 0);
//                TEST_ASSERT(strcmp(proxies[counter].proxy_password, password) == 0);
//            }
//            else
//            {
//                TEST_ASSERT_NULL(username);
//                TEST_ASSERT_NULL(password);
//            }
//        }
//
//        TEST_ASSERT(MA_OK == ma_proxy_release(new_proxy));
//        new_proxy = NULL;
//
//        // Trying to retrieve proxy from location which is more than size of the proxy list.
//        // It should return INVALIDARG
//        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_list_get_proxy(my_proxy_list, sizeof(proxies)/sizeof(proxies[0]), &new_proxy));
//        TEST_ASSERT_NULL(new_proxy);
//    }
//
//    {
//        ma_int32_t index = -1;
//
//        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_list_set_last_proxy_index(NULL, 10));
//        TEST_ASSERT(MA_OK == ma_proxy_list_set_last_proxy_index(my_proxy_list, 10));
//        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_list_get_last_proxy_index(NULL, &index));
//        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_list_get_last_proxy_index(my_proxy_list, NULL));
//        TEST_ASSERT(MA_OK == ma_proxy_list_get_last_proxy_index(my_proxy_list, &index));
//        TEST_ASSERT(10 == index);
//
//        TEST_ASSERT(MA_OK == ma_proxy_list_set_last_proxy_index(my_proxy_list, 100));
//        TEST_ASSERT(MA_OK == ma_proxy_list_get_last_proxy_index(my_proxy_list, &index));
//        TEST_ASSERT(100 == index);
//    }
//
//    // Adding 10 reference to the proxy list and then, releasing it 11 times.
//    for(counter = 0; counter < 10; counter++)
//    {
//        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_list_add_ref(NULL));
//        TEST_ASSERT(MA_OK == ma_proxy_list_add_ref(my_proxy_list));
//    }
//
//    for(counter = 0; counter < 11; counter++)
//    {
//        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_list_release(NULL));
//        TEST_ASSERT(MA_OK == ma_proxy_list_release(my_proxy_list));
//    }
//}
//
//
//
//
