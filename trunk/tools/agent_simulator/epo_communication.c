/******************************************************************************
Tool to verify the McAfee agent communication to/from the ePO server.

Command to be used - agent_sim.exe <Options>

Options List -
------------
--FIPS (case-insensitive)                        FIPS mode enabled.
--officer (case-insensitive)                     Officer/User Role.
******************************************************************************/
#include <stdio.h>
#include "ma/logger/ma_console_logger.h"
#include "ma/internal/services/ahclient/ma_ah_client_service.h"

// State machine sequence
enum {
         AH_COMMUNICATION_START = 0,
         SEND_PROP_VERSION_PACKAGE = 1,
         HANDLE_REQUEST_PUBLIC_KEY_PACKAGE = 2,
         SEND_AGENT_PUBLIC_KEY_PACKAGE = 3,
         HANDLE_PROPS_RESPONSE_PACKAGE = 4,
         SEND_MANIFEST_REQUEST_PACKAGE = 5,
         HANDLE_MANIFEST_ESPONSE_PACKAGE = 6,
         AH_COMMUNICATION_COMPLETE = 7
     }current_mode = AH_COMMUNICATION_START;

// Declaration for the cleanup function defined towards the end of the file.
void perform_cleanup();

ma_ah_client_service_t *agent_handler = NULL;

/******************************************************************************
Console Logger pointer. Console logger is being used for network logging as well
as logging for any failures within this sample code.
******************************************************************************/
ma_console_logger_t *console_logger = NULL;

void create_logger()
{
    if(ma_console_logger_create(&console_logger) != MA_OK)
    {
        printf("Console Logger could not be created.\n");
    }
}

/******************************************************************************
Timer call-back function. This function gets executed whenever timer expires.
The timer event is stopped when the network communication is complete.
******************************************************************************/
int nw_communication_in_progress = 1;

void timer_event(uv_timer_t *handle, int status)
{
    static int counter = 0;

    // Need to raise alert only once at the start of state machine.
    if((current_mode == AH_COMMUNICATION_START) && (counter == 0))
    {
        printf("Raised S-Pipe alert\n");
        current_mode = SEND_PROP_VERSION_PACKAGE;
        ma_ah_client_service_raise_spipe_alert(agent_handler);
        counter++;
    }

    if((current_mode == SEND_MANIFEST_REQUEST_PACKAGE))
    {
        printf("Raised S-Pipe alert\n");
        current_mode = SEND_MANIFEST_REQUEST_PACKAGE;
        ma_ah_client_service_raise_spipe_alert(agent_handler);
        counter++;
    }

    // While network communication is in progress
    if(current_mode == AH_COMMUNICATION_COMPLETE)
    {
        // N/W communication is complete. No need to keep timer running.
        uv_timer_stop(handle);
    }
    else
    {
        #ifdef _DEBUG
            printf("Timer event executed\n");
        #endif
    }
}

/**************************************************************************
Parsing through argument list.
**************************************************************************/
unsigned int fips_mode = 0;
unsigned int officer_role = 0;

// SSL Certificate
char *ssl_certificate = NULL;

void parse_command_line(int argc, char* argv[])
{
    // Loop iterator
    int count = 1;

    for(count = 1; count < argc; count++)
    {
        if(stricmp(argv[count], "--FIPS") == 0)
        {
            fips_mode = 1;

            #ifdef _DEBUG
                 printf("FIPS standard enabled.\n");
            #endif

            continue;
        }

        if(stricmp(argv[count], "--officer") == 0)
        {
            officer_role = 1;

            #ifdef _DEBUG
                 printf("In Office Role.\n");
            #endif

            continue;
        }

        if(strcmp(argv[count], "--cert") == 0)
        {
            ssl_certificate = argv[count + 1];

            #ifdef _DEBUG
                 printf("SSL Certificate Path - %s\n", ssl_certificate);
            #endif

            count++;
            continue;
        }

        // If control reaches here, it means that an unknown/mistyped option has
        // been observed from command line.
        MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Unknown/Mis-typed Option - %s", argv[count]);
    }
}

/**************************************************************************
Creating a Crypto context and Crypto objects.
1) Creating crypto context -
   a) FIPS/NON-FIPS mode
   b) Officer or User role.
   c) Managed mode (Communication with ePO is possible only in managed mode)
   d) Setting logger.
   e) Setting Keystore and Crypto signature file path.
2) Creating and initializing crypto object with the above crypto context.
**************************************************************************/
ma_crypto_ctx_t *crypto_context_object = NULL;
ma_crypto_t *crypto_object = NULL;

void create_crypto_object()
{
    ma_error_t error_code = MA_OK;

    error_code = ma_crypto_ctx_create(&crypto_context_object);

    if(error_code != MA_OK)
    {
        MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Crypto context could not be created");
    }

    error_code = fips_mode ? ma_crypto_ctx_set_fips_mode(crypto_context_object, MA_CRYPTO_MODE_FIPS)
                           : ma_crypto_ctx_set_fips_mode(crypto_context_object, MA_CRYPTO_MODE_NON_FIPS);

    error_code = officer_role ? ma_crypto_ctx_set_role(crypto_context_object, MA_CRYPTO_ROLE_OFFICER)
                              : ma_crypto_ctx_set_role(crypto_context_object, MA_CRYPTO_ROLE_USER);

    // Communication with ePO is possible only in managed mode
    error_code = ma_crypto_ctx_set_agent_mode(crypto_context_object, MA_CRYPTO_AGENT_MANAGED);

    error_code = ma_crypto_ctx_set_logger(crypto_context_object, (ma_logger_t *)console_logger);

    error_code = ma_crypto_ctx_set_keystore(crypto_context_object, L"C:\\ProgramData\\McAfee\\Common Framework\\keystore");

    // Will be created using windows pre-build events
    error_code = officer_role ? ma_crypto_ctx_set_sig_file_path(crypto_context_object, L"E:\\SVN_Repository\\ma\\trunk\\tools\\agent_simulator\\Debug\\agent_sim_officer.sig")
                              : ma_crypto_ctx_set_sig_file_path(crypto_context_object, L"E:\\SVN_Repository\\ma\\trunk\\tools\\agent_simulator\\Debug\\agent_sim_user.sig");

    if(error_code != MA_OK)
    {
        MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Crypto context could not be initialized");
    }

    error_code = ma_crypto_create(&crypto_object);

    error_code = ma_crypto_initialize(crypto_context_object, crypto_object);

    if(error_code != MA_OK)
    {
        MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Crypto object could not be created/initialized");
    }

    // Importing server public key
/*    if(ma_crypto_import_server_public_key(crypto_object, "E:\\Softwares\\keystore\\sr2048WIN-D9VEOIG9UQP\\srpubkey.bin", 1024) != MA_OK)
    {
        MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Server public key could not be imported into the crypto object");
    }

    // Importing server request secret key (server signing key)
    if(ma_crypto_import_server_signing_key(crypto_object, "E:\\Softwares\\keystore\\sr2048WIN-D9VEOIG9UQP\\reqseckey.bin", 2048) != MA_OK)
    {
        MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Server request secret key could not be imported into the crypto object");
    }*/
}

/**************************************************************************
Creating an ePO sitelist. (Later on, this sitelist will be imported from
sitelist.xml when the sitelist service is ready.
**************************************************************************/
ma_ah_site_t *site_object = NULL;
ma_ah_sitelist_t *sitelist_object = NULL;

void create_site_list()
{
    // Creating sitelist
    if(ma_ah_sitelist_create(&sitelist_object) != MA_OK)
    {
        MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Sitelist could not be created");
    }

    // Sitelist configuration
    ma_ah_sitelist_set_global_version(sitelist_object, "R12WqYdfNvPtiqHZHe1BqfNwwDRpALV6748aSOYXiiQ=");
    ma_ah_sitelist_set_local_version(sitelist_object, "00000000000000");
    ma_ah_sitelist_set_ca_file(sitelist_object, "C:\\ProgramData\\McAfee\\Common Framework\\cabundle.cer");

    // Creating a site, providing the configuration.
    if(ma_ah_site_create(&site_object) != MA_OK)
    {
        MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Site could not be created");
    }

    ma_ah_site_set_name(site_object, "ePO_WIN-D9VEOIG9UQP");
    ma_ah_site_set_version(site_object, "5.0.0");
    ma_ah_site_set_fqdn_name(site_object, "WIN-D9VEOIG9UQP");
    ma_ah_site_set_short_name(site_object, "D9VEOIG9UQP");
    ma_ah_site_set_ip_address(site_object, "172.16.220.177");
    ma_ah_site_set_port(site_object, 80);
    ma_ah_site_set_ssl_port(site_object, 443);
    ma_ah_site_set_order(site_object, 1);

    // Adding site into the sitelist.
    if(ma_ah_sitelist_add_site(sitelist_object, site_object) != MA_OK)
    {
        MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Site could not be added to Sitelist");
    }
}

/**************************************************************************
1) Creating Event Loop.
2) Extracting UV loop from the Event Loop.
3) Using the event loop it to initiate a timer. The timer will start
   immediately and will repeat every 500ms.
4) Creating Network Client service, setting the logger and starting the
   service.
**************************************************************************/
ma_event_loop_t *event_loop = NULL;
uv_loop_t *loop = NULL;
uv_timer_t timer;
ma_net_client_service_t *net_service = NULL;

void initiate_net_client_service()
{
    if(ma_event_loop_create(&event_loop) != MA_OK)
    {
        MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Event Loop could not be created");
    }

    ma_event_loop_get_loop_impl(event_loop, &loop);
    uv_timer_init(loop, &timer);
    uv_timer_start(&timer, timer_event, 0, 5000);

    if(ma_net_client_service_create(event_loop, &net_service) != MA_OK)
    {
        MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Network Client service could not be instantiated");
    }

    ma_net_client_service_setlogger(net_service, (ma_logger_t *)console_logger);
    ma_net_client_service_start(net_service);
}

/**************************************************************************
Property Version Decorator functions (PropsVersion).
**************************************************************************/
ma_error_t decorate_prop_version_pkg(ma_spipe_decorator_t* decorator, ma_spipe_priority_t priority, ma_spipe_package_t* spipe_pkg)
{
    ma_error_t error_code = MA_OK;
    ma_stream_t *prop_stream = NULL;
    size_t prop_size = 0;
    size_t read_bytes = 0;
    ma_bytebuffer_t *prop_buffer = NULL;
    ma_bytebuffer_t *server_public_key_hash_buffer = NULL;

    printf("Prop Version Package decorator called \n");

    // when both decorator and S-Pipe package are not NULL
    if(decorator && spipe_pkg)
    {
        if(SEND_PROP_VERSION_PACKAGE == current_mode)
        {
            // Set Package Type
            ma_spipe_package_set_package_type(spipe_pkg, MA_SPIPE_PACKAGE_TYPE_DATA);

            // Setting Header Properties
            ma_spipe_package_set_header_property(spipe_pkg, MA_SPIPE_PROPERTY_SENDER_GUID, "{FDC3FF5D-F109-4D0A-8AC3-3C4F8C1F1234}");
            ma_spipe_package_set_header_property(spipe_pkg, MA_SPIPE_PROPERTY_PACKAGE_GUID, "{FDC3FF5D-F109-4D0A-8AC3-3C4F8C1F5678}");
            ma_spipe_package_set_header_property(spipe_pkg, MA_SPIPE_PROPERTY_COMPUTER_NAME, "ANURAG-LAPPY");

            // Setting Information fields
            ma_spipe_package_add_info(spipe_pkg, "AgentVersion", "5.0.0", strlen("5.0.0"));
            ma_spipe_package_add_info(spipe_pkg, "AssignmentList", "0", strlen("0"));
            ma_spipe_package_add_info(spipe_pkg, "ComputerName", "Anurag-Lappy", strlen("Anurag-Lappy"));
            ma_spipe_package_add_info(spipe_pkg, "EventFilterVersion", "0", strlen("0"));
            ma_spipe_package_add_info(spipe_pkg, "FQDN", "Anurag-Lappy", strlen("Anurag-Lappy"));
            ma_spipe_package_add_info(spipe_pkg, "GuidRegenerationSupported", "1", strlen("1"));
            ma_spipe_package_add_info(spipe_pkg, "IPAddress", "192.16.234.123", strlen("192.16.234.123"));
            ma_spipe_package_add_info(spipe_pkg, "NETAddress", "000000000000", strlen("000000000000"));
            ma_spipe_package_add_info(spipe_pkg, "PackageType", "PropsVersion", strlen("PropsVersion"));
            ma_spipe_package_add_info(spipe_pkg, "PolicyVersion", "1", strlen("1"));
            ma_spipe_package_add_info(spipe_pkg, "PrevPropsVersion", "0", strlen("0"));
            ma_spipe_package_add_info(spipe_pkg, "PropsVersion", "0", strlen("0"));
            ma_spipe_package_add_info(spipe_pkg, "RepoKeyHashVersion", "0", strlen("0"));
            ma_spipe_package_add_info(spipe_pkg, "SequenceNumber", "1", strlen("1"));

            // Server Key Hash
            ma_crypto_get_server_public_key_hash(crypto_object, &server_public_key_hash_buffer);
            ma_spipe_package_add_info(spipe_pkg, "ServerKeyHash", ma_bytebuffer_get_bytes(server_public_key_hash_buffer), ma_bytebuffer_get_size(server_public_key_hash_buffer));
            
            ma_spipe_package_add_info(spipe_pkg, "SupportsManifestContent", "1", strlen("1"));

            ma_spipe_package_add_info(spipe_pkg, "SiteinfoVersion", "R12WqYdfNvPtiqHZHe1BqfNwwDRpALV6748aSOYXiiQ=", strlen("R12WqYdfNvPtiqHZHe1BqfNwwDRpALV6748aSOYXiiQ="));
            ma_spipe_package_add_info(spipe_pkg, "SupportedSPIPEVersion", "6.0", strlen("6.0"));
            ma_spipe_package_add_info(spipe_pkg, "Supports2048BitRSA", "1", strlen("1"));
            ma_spipe_package_add_info(spipe_pkg, "TaskVersion", "0", strlen("0"));
            ma_spipe_package_add_info(spipe_pkg, "TransactionGUID", "{0FFA6BA8-8FEC-4EB1-8045-8A7520EB9AA6}", strlen("{0FFA6BA8-8FEC-4EB1-8045-8A7520EB9AA6}"));
            ma_spipe_package_add_info(spipe_pkg, "User1", "ANURAG-LAPPY\\admin", strlen("ANURAG-LAPPY\\admin"));
            ma_spipe_package_add_info(spipe_pkg, "UserAssignmentCount", "0", strlen("0"));
            ma_spipe_package_add_info(spipe_pkg, "Vdi", "0", strlen("0"));

            // Set Data Fields - Converting props.xml to stream -> creating a buffer out of fstream -> setting the buffer into properties.
            if(ma_fstream_create("E:\\Softwares\\props.xml", MA_STREAM_ACCESS_FLAG_READ, NULL, &prop_stream) != MA_OK)
            {
                MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "File stream handle could not be created from the Props.xml");
            }

            ma_stream_get_size(prop_stream, &prop_size);
            ma_bytebuffer_create(prop_size, &prop_buffer);
            ma_stream_read(prop_stream, ma_bytebuffer_get_bytes(prop_buffer), prop_size, &read_bytes);
            ma_stream_release(prop_stream);

            ma_bytebuffer_shrink(prop_buffer, prop_size);

            if(ma_spipe_package_add_data(spipe_pkg, "Props.xml", ma_bytebuffer_get_bytes(prop_buffer), ma_bytebuffer_get_size(prop_buffer)) != MA_OK)
            {
                MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Props.xml could not be added to the S-pipe package");
            }

            current_mode = HANDLE_REQUEST_PUBLIC_KEY_PACKAGE;
        }
        else
        {
            // Do not set anything on S-Pipe package but return OK error code.
            error_code = MA_OK;
        }
    }
    else
    {
        error_code = MA_ERROR_INVALIDARG;
    }

    return error_code;
}


ma_error_t get_prop_version_pkg_priority(ma_spipe_decorator_t* decorator, ma_spipe_priority_t* priority)
{
    printf("Prop Version Package Priority called %d \n", current_mode);

    if(decorator && priority)
    {
        if(current_mode == SEND_PROP_VERSION_PACKAGE)
        {
            *priority = MA_SPIPE_PRIORITY_IMMEDIATE;
        }
        else
        {
            *priority = MA_SPIPE_PRIORITY_NONE;
        }
        return MA_OK;
    }

    return MA_ERROR_INVALIDARG;
}

ma_error_t prop_version_pkg_destroy(ma_spipe_decorator_t* decorator)
{
    printf("Prop Version Package destroy called \n");

    if(decorator)
    {
        // Nothing to release here
        return MA_OK;
    }

    return MA_ERROR_INVALIDARG;
}

// Decorator object and methods
ma_spipe_decorator_t prop_version_decorator;
ma_spipe_decorator_methods_t prop_version_decorator_methods = { &decorate_prop_version_pkg, &get_prop_version_pkg_priority, &prop_version_pkg_destroy };


/**************************************************************************
Public Key Request Handler function (RequestPublicKey).
**************************************************************************/
ma_error_t handle_req_pub_key_pkg(ma_spipe_handler_t* handler, const ma_spipe_response_t *spipe_response)
{
    ma_error_t error_code = MA_OK;
    size_t down_stream_size = 0;
    const unsigned char *down_stream_value = NULL;
    ma_spipe_package_type_t package_type;

    printf("Request Public key handler called \n");

    printf("net_service_error_code %d \n", spipe_response->net_service_error_code);
    printf("http_response_code %d \n", spipe_response->http_response_code);

    if(current_mode == HANDLE_REQUEST_PUBLIC_KEY_PACKAGE)
    {
        ma_spipe_package_get_header_property(spipe_response->respond_spipe_pkg, MA_SPIPE_PROPERTY_SENDER_GUID, &down_stream_value, &down_stream_size);
        printf("MA_SPIPE_PROPERTY_SENDER_GUID - %s \t %d \n", down_stream_value, down_stream_size);

        ma_spipe_package_get_header_property(spipe_response->respond_spipe_pkg, MA_SPIPE_PROPERTY_PACKAGE_GUID, &down_stream_value, &down_stream_size);
        printf("MA_SPIPE_PROPERTY_PACKAGE_GUID - %s \t %d \n", down_stream_value, down_stream_size);

        ma_spipe_package_get_header_property(spipe_response->respond_spipe_pkg, MA_SPIPE_PROPERTY_COMPUTER_NAME, &down_stream_value, &down_stream_size);
        printf("MA_SPIPE_PROPERTY_COMPUTER_NAME - %s \t %d \n", down_stream_value, down_stream_size);

        ma_spipe_package_get_package_type(spipe_response->respond_spipe_pkg, &package_type);
        printf("Package Type - %d \n", package_type);

        current_mode = SEND_AGENT_PUBLIC_KEY_PACKAGE;
    }


    return error_code;
}


ma_error_t req_pub_key_pkg_destroy(ma_spipe_handler_t* handler)
{
    printf("Request Public key destroyed \n");

    if(handler)
    {
        // Nothing to release here
        return MA_OK;
    }

    return MA_ERROR_INVALIDARG;
}

ma_spipe_handler_t req_pub_key_handler;

ma_spipe_handler_methods_t req_pub_key_handler_methods = { &handle_req_pub_key_pkg, &req_pub_key_pkg_destroy };


/**************************************************************************
Agent Public Key Decorator functions (AgentPubKey).
**************************************************************************/
ma_error_t decorate_agent_pub_key_pkg(ma_spipe_decorator_t* decorator, ma_spipe_priority_t priority, ma_spipe_package_t* spipe_pkg)
{
    ma_error_t error_code = MA_OK;
    ma_stream_t *prop_stream = NULL;
    size_t prop_size = 0;
    size_t read_bytes = 0;
    ma_bytebuffer_t *prop_buffer = NULL;
    ma_bytebuffer_t *server_public_key_hash_buffer = NULL;
    ma_bytebuffer_t *agent_public_key_buffer = NULL;

    printf("Agent Public key decorator called \n");

    // when both decorator and S-Pipe package are not NULL
    if(decorator && spipe_pkg)
    {
        if(current_mode == SEND_AGENT_PUBLIC_KEY_PACKAGE)
        {
            // Set Package Type
            ma_spipe_package_set_package_type(spipe_pkg, MA_SPIPE_PACKAGE_TYPE_KEY);

            // Setting Header Properties
            ma_spipe_package_set_header_property(spipe_pkg, MA_SPIPE_PROPERTY_SENDER_GUID, "{FDC3FF5D-F109-4D0A-8AC3-3C4F8C1F1234}");
            ma_spipe_package_set_header_property(spipe_pkg, MA_SPIPE_PROPERTY_PACKAGE_GUID, "{FDC3FF5D-F109-4D0A-8AC3-3C4F8C1F5678}");
            ma_spipe_package_set_header_property(spipe_pkg, MA_SPIPE_PROPERTY_COMPUTER_NAME, "ANURAG-LAPPY");

            // Setting Information fields
            ma_spipe_package_add_info(spipe_pkg, "AgentVersion", "5.0.0", strlen("5.0.0"));
            ma_spipe_package_add_info(spipe_pkg, "AssignmentList", "0", strlen("0"));
            ma_spipe_package_add_info(spipe_pkg, "ComputerName", "Anurag-Lappy", strlen("Anurag-Lappy"));
            ma_spipe_package_add_info(spipe_pkg, "DomainName", "HelloWorld", strlen("HelloWorld"));
            ma_spipe_package_add_info(spipe_pkg, "EventFilterVersion", "0", strlen("0"));
            ma_spipe_package_add_info(spipe_pkg, "FQDN", "Anurag-Lappy", strlen("Anurag-Lappy"));
            ma_spipe_package_add_info(spipe_pkg, "GuidRegenerationSupported", "1", strlen("1"));
            ma_spipe_package_add_info(spipe_pkg, "IPAddress", "192.16.234.123", strlen("192.16.234.123"));
            ma_spipe_package_add_info(spipe_pkg, "NETAddress", "000000000000", strlen("000000000000"));
            ma_spipe_package_add_info(spipe_pkg, "PackageType", "AgentPubKey", strlen("AgentPubKey"));
            ma_spipe_package_add_info(spipe_pkg, "PlatformID", "Windows_7", strlen("Windows_7"));
            ma_spipe_package_add_info(spipe_pkg, "PolicyVersion", "1", strlen("1"));
            ma_spipe_package_add_info(spipe_pkg, "PropsVersion", "0", strlen("0"));
            ma_spipe_package_add_info(spipe_pkg, "RepoKeyHashVersion", "0", strlen("0"));
            ma_spipe_package_add_info(spipe_pkg, "SequenceNumber", "3", strlen("3"));

            // Server Key Hash
            ma_crypto_get_server_public_key_hash(crypto_object, &server_public_key_hash_buffer);
            ma_spipe_package_add_info(spipe_pkg, "ServerKeyHash", ma_bytebuffer_get_bytes(server_public_key_hash_buffer), ma_bytebuffer_get_size(server_public_key_hash_buffer));

            ma_spipe_package_add_info(spipe_pkg, "SupportsManifestContent", "1", strlen("1"));

            ma_spipe_package_add_info(spipe_pkg, "SiteinfoVersion", "R12WqYdfNvPtiqHZHe1BqfNwwDRpALV6748aSOYXiiQ=", strlen("R12WqYdfNvPtiqHZHe1BqfNwwDRpALV6748aSOYXiiQ="));
            ma_spipe_package_add_info(spipe_pkg, "SupportedSPIPEVersion", "6.0", strlen("6.0"));
            ma_spipe_package_add_info(spipe_pkg, "Supports2048BitRSA", "1", strlen("1"));
            ma_spipe_package_add_info(spipe_pkg, "TaskVersion", "0", strlen("0"));
            ma_spipe_package_add_info(spipe_pkg, "TransactionGUID", "{0FFA6BA8-8FEC-4EB1-8045-8A7520EB9AA6}", strlen("{0FFA6BA8-8FEC-4EB1-8045-8A7520EB9AA6}"));
            ma_spipe_package_add_info(spipe_pkg, "User1", "ANURAG-LAPPY\\admin", strlen("ANURAG-LAPPY\\admin"));
            ma_spipe_package_add_info(spipe_pkg, "UserAssignmentCount", "0", strlen("0"));
            ma_spipe_package_add_info(spipe_pkg, "Vdi", "0", strlen("0"));

            // Set Data Fields - Converting props.xml to stream -> creating a buffer out of fstream -> setting the buffer into properties.
            if(ma_fstream_create("E:\\Softwares\\props.xml", MA_STREAM_ACCESS_FLAG_READ, NULL, &prop_stream) != MA_OK)
            {
                MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "File stream handle could not be created from the Props.xml");
            }

            ma_stream_get_size(prop_stream, &prop_size);
            ma_bytebuffer_create(prop_size, &prop_buffer);
            ma_stream_read(prop_stream, ma_bytebuffer_get_bytes(prop_buffer), prop_size, &read_bytes);
            ma_stream_release(prop_stream);

            ma_bytebuffer_shrink(prop_buffer, prop_size);

            if(ma_spipe_package_add_data(spipe_pkg, "Props.xml", ma_bytebuffer_get_bytes(prop_buffer), ma_bytebuffer_get_size(prop_buffer)) != MA_OK)
            {
                MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Props.xml could not be added to the S-pipe package");
            }

            // Get the agent public key in the byte buffer and send it
            if(ma_crypto_get_agent_public_key(crypto_object, &agent_public_key_buffer) != MA_OK)
            {
                MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Agent Public key cannot be converted to byte buffer");
            }

            if(ma_spipe_package_add_data(spipe_pkg, "agpubkey.bin", ma_bytebuffer_get_bytes(agent_public_key_buffer), ma_bytebuffer_get_size(agent_public_key_buffer)) != MA_OK)
            {
                MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Agent Public key information could not be added to the S-pipe package");
            }

            current_mode = HANDLE_PROPS_RESPONSE_PACKAGE;
        }
        else
        {
            // Do not set anything on S-Pipe package but return OK error code.
            printf("Nothing to send\n");
            error_code = MA_OK;
        }
    }
    else
    {
        error_code = MA_ERROR_INVALIDARG;
    }

    return error_code;
}


ma_error_t get_agent_pub_key_pkg_priority(ma_spipe_decorator_t* decorator, ma_spipe_priority_t* priority)
{
    printf("Agent Public key Package priority called %d\n", current_mode);

    if(decorator && priority)
    {
        if(current_mode == SEND_AGENT_PUBLIC_KEY_PACKAGE)
        {
            *priority = MA_SPIPE_PRIORITY_IMMEDIATE;
        }
        else
        {
            *priority = MA_SPIPE_PRIORITY_NONE;
        }

        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t agent_pub_key_pkg_destroy(ma_spipe_decorator_t* decorator)
{
    printf("Agent Public key destory called \n");

    if(decorator)
    {
        // Nothing to release here
        return MA_OK;
    }

    return MA_ERROR_INVALIDARG;
}

// Decorator object and methods
ma_spipe_decorator_t agent_pub_key_decorator;
ma_spipe_decorator_methods_t agent_pub_key_decorator_methods = { &decorate_agent_pub_key_pkg, &get_agent_pub_key_pkg_priority, &agent_pub_key_pkg_destroy };


/**************************************************************************
Props response package containing policy information (PropsResponse).
**************************************************************************/
ma_error_t handle_props_response_pkg(ma_spipe_handler_t* handler, const ma_spipe_response_t *spipe_response)
{
    ma_error_t error_code = MA_OK;
    size_t down_stream_size = 0;
    const unsigned char *down_stream_value = NULL;
    ma_spipe_package_type_t package_type;
    void *down_stream_data = NULL;

    printf("Handle properties response called \n");

    printf("net_service_error_code %d \n", spipe_response->net_service_error_code);
    printf("http_response_code %d \n", spipe_response->http_response_code);

    if(current_mode == HANDLE_PROPS_RESPONSE_PACKAGE)
    {
        ma_spipe_package_get_header_property(spipe_response->respond_spipe_pkg, MA_SPIPE_PROPERTY_SENDER_GUID, &down_stream_value, &down_stream_size);
        printf("MA_SPIPE_PROPERTY_SENDER_GUID - %s \t %d \n", down_stream_value, down_stream_size);

        ma_spipe_package_get_header_property(spipe_response->respond_spipe_pkg, MA_SPIPE_PROPERTY_PACKAGE_GUID, &down_stream_value, &down_stream_size);
        printf("MA_SPIPE_PROPERTY_PACKAGE_GUID - %s \t %d \n", down_stream_value, down_stream_size);

        ma_spipe_package_get_header_property(spipe_response->respond_spipe_pkg, MA_SPIPE_PROPERTY_COMPUTER_NAME, &down_stream_value, &down_stream_size);
        printf("MA_SPIPE_PROPERTY_COMPUTER_NAME - %s \t %d \n", down_stream_value, down_stream_size);

        ma_spipe_package_get_package_type(spipe_response->respond_spipe_pkg, &package_type);
        printf("Package Type - %d \n", package_type);

        ma_spipe_package_get_info(spipe_response->respond_spipe_pkg, "TransactionGUID", &down_stream_value, &down_stream_size);
        printf("TransactionGUID - %s \t %d \n", down_stream_value, down_stream_size);

        ma_spipe_package_get_data(spipe_response->respond_spipe_pkg, "Policy\\Server.xml", &down_stream_data, &down_stream_size);

        // All communication completed. Close the timer event.
        current_mode = SEND_MANIFEST_REQUEST_PACKAGE;
    }

    return error_code;
}


ma_error_t props_response_pkg_destroy(ma_spipe_handler_t* handler)
{
    printf("Props response package destroy called \n");

    if(handler)
    {
        // Nothing to release here
        return MA_OK;
    }

    return MA_ERROR_INVALIDARG;
}

ma_spipe_handler_t props_response_handler;

ma_spipe_handler_methods_t props_response_handler_methods = { &handle_props_response_pkg, &props_response_pkg_destroy };



ma_error_t decorate_manifest_req_pkg(ma_spipe_decorator_t* decorator, ma_spipe_priority_t priority, ma_spipe_package_t* spipe_pkg)
{
    ma_error_t error_code = MA_OK;
    ma_stream_t *prop_stream = NULL;
    size_t prop_size = 0;
    size_t read_bytes = 0;
    ma_bytebuffer_t *prop_buffer = NULL;
    ma_bytebuffer_t *server_public_key_hash_buffer = NULL;

    printf("Prop Version Package decorator called \n");

    // when both decorator and S-Pipe package are not NULL
    if(decorator && spipe_pkg)
    {
        if(SEND_MANIFEST_REQUEST_PACKAGE == current_mode)
        {
            // Set Package Type
            ma_spipe_package_set_package_type(spipe_pkg, MA_SPIPE_PACKAGE_TYPE_DATA);

            // Setting Header Properties
            ma_spipe_package_set_header_property(spipe_pkg, MA_SPIPE_PROPERTY_SENDER_GUID, "{FDC3FF5D-F109-4D0A-8AC3-3C4F8C1F1234}");
            ma_spipe_package_set_header_property(spipe_pkg, MA_SPIPE_PROPERTY_PACKAGE_GUID, "{FDC3FF5D-F109-4D0A-8AC3-3C4F8C1F5678}");
            ma_spipe_package_set_header_property(spipe_pkg, MA_SPIPE_PROPERTY_COMPUTER_NAME, "Vinod-test");

            // Setting Information fields
            ma_spipe_package_add_info(spipe_pkg, "AgentVersion", "5.0.0", strlen("5.0.0"));
            ma_spipe_package_add_info(spipe_pkg, "AssignmentList", "0", strlen("0"));
            ma_spipe_package_add_info(spipe_pkg, "ComputerName", "Vinod-test", strlen("Vinod-test"));
            ma_spipe_package_add_info(spipe_pkg, "EventFilterVersion", "0", strlen("0"));
            ma_spipe_package_add_info(spipe_pkg, "FQDN", "Vinod-test", strlen("Vinod-test"));
            ma_spipe_package_add_info(spipe_pkg, "GuidRegenerationSupported", "1", strlen("1"));
            ma_spipe_package_add_info(spipe_pkg, "IPAddress", "172.16.220.100", strlen("172.16.220.100"));
            ma_spipe_package_add_info(spipe_pkg, "NETAddress", "000000000000", strlen("000000000000"));
            

            ma_spipe_package_add_info(spipe_pkg, "PackageType", "PolicyManifestRequest", strlen("PolicyManifestRequest"));


            ma_spipe_package_add_info(spipe_pkg, "PolicyVersion", "1", strlen("1"));
            ma_spipe_package_add_info(spipe_pkg, "PrevPropsVersion", "0", strlen("0"));
            ma_spipe_package_add_info(spipe_pkg, "PropsVersion", "0", strlen("0"));
            ma_spipe_package_add_info(spipe_pkg, "RepoKeyHashVersion", "0", strlen("0"));
            ma_spipe_package_add_info(spipe_pkg, "SequenceNumber", "10", strlen("10"));

            // Server Key Hash
            ma_crypto_get_server_public_key_hash(crypto_object, &server_public_key_hash_buffer);
            ma_spipe_package_add_info(spipe_pkg, "ServerKeyHash", ma_bytebuffer_get_bytes(server_public_key_hash_buffer), ma_bytebuffer_get_size(server_public_key_hash_buffer));
            
            ma_spipe_package_add_info(spipe_pkg, "SupportsManifestContent", "1", strlen("1"));

            ma_spipe_package_add_info(spipe_pkg, "SiteinfoVersion", "R12WqYdfNvPtiqHZHe1BqfNwwDRpALV6748aSOYXiiQ=", strlen("R12WqYdfNvPtiqHZHe1BqfNwwDRpALV6748aSOYXiiQ="));
            ma_spipe_package_add_info(spipe_pkg, "SupportedSPIPEVersion", "6.0", strlen("6.0"));
            ma_spipe_package_add_info(spipe_pkg, "Supports2048BitRSA", "1", strlen("1"));
            ma_spipe_package_add_info(spipe_pkg, "TaskVersion", "0", strlen("0"));
            ma_spipe_package_add_info(spipe_pkg, "TransactionGUID", "{0FFA6BA8-8FEC-4EB1-8045-8A7520EB9AA6}", strlen("{0FFA6BA8-8FEC-4EB1-8045-8A7520EB9AA6}"));
            ma_spipe_package_add_info(spipe_pkg, "User1", "Vinod-Test\\admin", strlen("Vinod-Test\\admin"));
            ma_spipe_package_add_info(spipe_pkg, "UserAssignmentCount", "0", strlen("0"));
            ma_spipe_package_add_info(spipe_pkg, "Vdi", "0", strlen("0"));

            // Set Data Fields - Converting props.xml to stream -> creating a buffer out of fstream -> setting the buffer into properties.
            if(ma_fstream_create("C:\\props.xml", MA_STREAM_ACCESS_FLAG_READ, NULL, &prop_stream) != MA_OK)
            {
                MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "File stream handle could not be created from the Props.xml");
            }

            ma_stream_get_size(prop_stream, &prop_size);
            ma_bytebuffer_create(prop_size, &prop_buffer);
            ma_stream_read(prop_stream, ma_bytebuffer_get_bytes(prop_buffer), prop_size, &read_bytes);
            ma_stream_release(prop_stream);

            ma_bytebuffer_shrink(prop_buffer, prop_size);

            if(ma_spipe_package_add_data(spipe_pkg, "Props.xml", ma_bytebuffer_get_bytes(prop_buffer), ma_bytebuffer_get_size(prop_buffer)) != MA_OK)
            {
                MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Props.xml could not be added to the S-pipe package");
            }

            current_mode = HANDLE_MANIFEST_ESPONSE_PACKAGE;
        }
        else
        {
            // Do not set anything on S-Pipe package but return OK error code.
            error_code = MA_OK;
        }
    }
    else
    {
        error_code = MA_ERROR_INVALIDARG;
    }

    return error_code;
}

ma_error_t get_manifest_req_pkg_priority(ma_spipe_decorator_t* decorator, ma_spipe_priority_t* priority)
{
    printf("Agent Public key Package priority called %d\n", current_mode);

    if(decorator && priority)
    {
        if(current_mode == SEND_MANIFEST_REQUEST_PACKAGE)
        {
            *priority = MA_SPIPE_PRIORITY_IMMEDIATE;
        }
        else
        {
            *priority = MA_SPIPE_PRIORITY_NONE;
        }

        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t manifest_pkg_destroy(ma_spipe_decorator_t* decorator)
{
    printf("manifest pkg destory called \n");

    if(decorator)
    {
        // Nothing to release here
        return MA_OK;
    }

    return MA_ERROR_INVALIDARG;
}

// Decorator object and methods
ma_spipe_decorator_t manifest_req_decorator;
ma_spipe_decorator_methods_t manifest_req_decorator_methods = { &decorate_manifest_req_pkg, &get_manifest_req_pkg_priority, &manifest_pkg_destroy };


ma_error_t handle_manifest_response_pkg(ma_spipe_handler_t* handler, const ma_spipe_response_t *spipe_response)
{
    ma_error_t error_code = MA_OK;
    size_t down_stream_size = 0;
    const unsigned char *down_stream_value = NULL;
    ma_spipe_package_type_t package_type;
    void *down_stream_data = NULL;

    printf("Handle manifest response called \n");

    printf("net_service_error_code %d \n", spipe_response->net_service_error_code);
    printf("http_response_code %d \n", spipe_response->http_response_code);

    if(current_mode == HANDLE_MANIFEST_ESPONSE_PACKAGE)
    {
        FILE *fp = fopen("C:\\manifest.xml", "w");
        ma_spipe_package_get_header_property(spipe_response->respond_spipe_pkg, MA_SPIPE_PROPERTY_SENDER_GUID, &down_stream_value, &down_stream_size);
        printf("MA_SPIPE_PROPERTY_SENDER_GUID - %s \t %d \n", down_stream_value, down_stream_size);

        ma_spipe_package_get_header_property(spipe_response->respond_spipe_pkg, MA_SPIPE_PROPERTY_PACKAGE_GUID, &down_stream_value, &down_stream_size);
        printf("MA_SPIPE_PROPERTY_PACKAGE_GUID - %s \t %d \n", down_stream_value, down_stream_size);

        ma_spipe_package_get_header_property(spipe_response->respond_spipe_pkg, MA_SPIPE_PROPERTY_COMPUTER_NAME, &down_stream_value, &down_stream_size);
        printf("MA_SPIPE_PROPERTY_COMPUTER_NAME - %s \t %d \n", down_stream_value, down_stream_size);

        ma_spipe_package_get_package_type(spipe_response->respond_spipe_pkg, &package_type);
        printf("Package Type - %d \n", package_type);

        ma_spipe_package_get_info(spipe_response->respond_spipe_pkg, "TransactionGUID", &down_stream_value, &down_stream_size);
        printf("TransactionGUID - %s \t %d \n", down_stream_value, down_stream_size);

        ma_spipe_package_get_data(spipe_response->respond_spipe_pkg, "Manifest.xml", &down_stream_data, &down_stream_size);

        fwrite(down_stream_data, sizeof(char), down_stream_size, fp);

        fclose(fp);

        // All communication completed. Close the timer event.
        current_mode = AH_COMMUNICATION_COMPLETE;
    }

    return error_code;

}

ma_error_t manifest_response_pkg_destroy(ma_spipe_handler_t* handler)
{
    printf("Props manifest package destroy called \n");

    if(handler)
    {
        // Nothing to release here
        return MA_OK;
    }

    return MA_ERROR_INVALIDARG;
}

ma_spipe_handler_t manifest_response_handler;

ma_spipe_handler_methods_t manifest_response_handler_methods = { &handle_manifest_response_pkg, &manifest_response_pkg_destroy };

/**************************************************************************
1) Parse command line arguments to obtain configuration information.
2) Create logger.
3) Create crypto object.
4) Initiate network client service.
5) Create agent handler service and set logger for it.
6) Create a site, add to sitelist and then, assign sitelist to agent handler.
7) Register decorators and handlers.
8) Raise S-Pipe alert.
9) Run the event loop.
10) Finally, perform cleanup by releasing all objects.
**************************************************************************/
int main(int argc, char* argv[])
{
    int count = 1;

    ma_error_t error_code = MA_OK;

    // Parsing the command line arguments
    parse_command_line(argc, argv);

    create_logger();

    // Creating crypto object
    create_crypto_object();

    initiate_net_client_service();

    if(ma_ah_client_service_create(event_loop, net_service, crypto_object, &agent_handler) != MA_OK)
    {
        MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Agent Handler service could not be created");
    }

    ma_ah_client_service_set_logger(agent_handler, (ma_logger_t *)console_logger);
    create_site_list();

    if(ma_ah_client_service_set_sitelist(agent_handler, sitelist_object) != MA_OK)
    {
        MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Sitelist cannot be configured for agent handler");
    }

    ma_spipe_decorator_init(&prop_version_decorator, &prop_version_decorator_methods, (void *)0);
    ma_spipe_decorator_init(&agent_pub_key_decorator, &agent_pub_key_decorator_methods, (void *)2);
    ma_spipe_handler_init(&req_pub_key_handler, &req_pub_key_handler_methods, (void *)1);
    ma_spipe_handler_init(&props_response_handler, &props_response_handler_methods, (void *)3);
    ma_spipe_decorator_init(&manifest_req_decorator, &manifest_req_decorator_methods, (void *)4);
    ma_spipe_handler_init(&manifest_response_handler, &manifest_response_handler_methods, (void *)5);


    if(ma_ah_client_service_register_spipe_decorator(agent_handler, &prop_version_decorator) != MA_OK)
    {
        MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Prop Version Decorator could not be registered");
    }

    if(ma_ah_client_service_register_spipe_handler(agent_handler, &req_pub_key_handler))
    {
        MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Request Public Key handler could not be registered");
    }

    if(ma_ah_client_service_register_spipe_decorator(agent_handler, &agent_pub_key_decorator) != MA_OK)
    {
        MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Agent Public Key Decorator could not be registered");
    }

    if(ma_ah_client_service_register_spipe_handler(agent_handler, &props_response_handler))
    {
        MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Properties Response Package handler could not be registered");
    }

    if(ma_ah_client_service_register_spipe_decorator(agent_handler, &manifest_req_decorator) != MA_OK)
    {
        MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "manifest Decorator could not be registered");
    }

    if(ma_ah_client_service_register_spipe_handler(agent_handler, &manifest_response_handler))
    {
        MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "manifest handler could not be registered");
    }

    ma_event_loop_run(event_loop);

    perform_cleanup();

    return 0;
}

/**************************************************************************
1) Unregister decorator and handler.
2) Release sitelist and site objects.
3) Release Agent handler object.
4) De-initialize the crypto object and release it. Also, release the cryto
   context.
5) Finally, release logger.
**************************************************************************/
void perform_cleanup()
{
    ma_error_t error_code = MA_OK;

    printf("Cleanup started\n");

    ma_ah_client_service_unregister_spipe_decorator(agent_handler, &prop_version_decorator);
    ma_ah_client_service_unregister_spipe_decorator(agent_handler, &agent_pub_key_decorator);
    ma_ah_client_service_unregister_spipe_handler(agent_handler, &req_pub_key_handler);
    ma_ah_client_service_unregister_spipe_handler(agent_handler, &props_response_handler);
    ma_ah_client_service_unregister_spipe_decorator(agent_handler, &manifest_req_decorator);
    ma_ah_client_service_unregister_spipe_handler(agent_handler, &manifest_response_handler);
    ma_ah_sitelist_release(sitelist_object);
    ma_ah_site_release(site_object);
    ma_ah_site_release(site_object);
    ma_ah_client_service_release(agent_handler);
    ma_crypto_deinitialize(crypto_object);
    ma_crypto_release(crypto_object);
    ma_crypto_ctx_release(crypto_context_object);
    ma_console_logger_release(console_logger);
}








