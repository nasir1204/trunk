#include "bc/bc_event.hxx"
#include "bc/bc_strdef.hxx"

extern ::ma_logger_t *beacon_logger;
namespace mileaccess  { namespace bc {

bc_event_subscriber::bc_event_subscriber(mileaccess::ma::client const &cli):event_client(cli){
	try{
		this->subscribe();
	}
	catch(mileaccess::ma::utils::exception(err)){
		MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Error in event info retrival, %d", err.error);
	}
}

bc_event_subscriber::~bc_event_subscriber(){
	try{
		this->unsubscribe();
	}
	catch(mileaccess::ma::utils::exception(err)){
		MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Error in event info retrival, %d", err.error);
	}
}

ma_error_t bc_event_subscriber::on_cef_event(mileaccess::ma::client const &cli, std::string const &product_id, mileaccess::ma::events::event_bag const &bag){
	ma_error_t rc = MA_OK;
	MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "CEF event bag received from product %s.", product_id.c_str());
	try{
		MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "product name %s", bag.get_product_name() );
		MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "product version %s", bag.get_product_version() );
		MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "product family %s", bag.get_product_family() );

		size_t no_of_events = bag.get_event_count();

		for(size_t i = 0; i< no_of_events; i++){
			mileaccess::ma::events::event evt = bag.get_event(i);
			MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "event id %d", evt.get_id() );
			MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "event severity %d", evt.get_severity() );			
		}
	}
	catch(mileaccess::ma::utils::exception(err)){
		MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Error in event info retrival");
	}
	return rc;
}

ma_error_t bc_event_subscriber::on_custom_event(mileaccess::ma::client const &cli, std::string const &product_id, mileaccess::ma::events::custom_event const &bag){
	ma_error_t rc = MA_OK;
	MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Custom event bag received.");

	try{
	}
	catch(mileaccess::ma::utils::exception(err)){
		MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Error in event info retrival");
	}
	return rc;
}

void bc_event_generate(mileaccess::ma::client const &cli, std::string event_name, ma_uint32_t event_id, ma_event_severity_t severity) {
     try {
        MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Create event bag.");
        mileaccess::ma::events::event_bag event_bag(cli, BC_PRODUCT_ID_STR);
        
        /*Set product version and family.*/
        event_bag.set_product_version(BC_PROP_VALUE_PRODUCT_VERSION_STR);
		event_bag.set_product_family("TVD");

	    /**Set product common fileds.*/
        event_bag.add_common_field(MA_EVENT_COMMON_FILED_KEY_Analyzer, mileaccess::ma::variant(BC_PRODUCT_ID_STR));
        event_bag.add_common_field(MA_EVENT_COMMON_FILED_KEY_Analyzer, mileaccess::ma::variant(BC_PRODUCT_NAME_STR));
        event_bag.add_common_field(MA_EVENT_COMMON_FILED_KEY_Analyzer, mileaccess::ma::variant(BC_PROP_VALUE_PRODUCT_VERSION_STR));


        /*Create and add event into bag*/
        mileaccess::ma::events::event event(cli, event_id, severity);

        /*add common fields.*/
        event.add_common_field(MA_EVENT_COMMON_FILED_KEY_ThreatCategory, mileaccess::ma::variant("beacon_t_category"));
        event.add_common_field(MA_EVENT_COMMON_FILED_KEY_ThreatName, mileaccess::ma::variant(event_name));
        event.add_common_field(MA_EVENT_COMMON_FILED_KEY_ThreatType, mileaccess::ma::variant("beacon_type"));

        /*add event to bag */
	    event_bag.add_event(event);

        /*sending event*/
		event_bag.send();
    }
    catch(mileaccess::ma::utils::exception(err)) {
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Event generation failed. event api error = %d.", err.error);
    }
    MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Event bag sent to agent.");
}

}}
