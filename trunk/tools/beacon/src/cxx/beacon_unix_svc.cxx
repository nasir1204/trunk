#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#if defined(MACX)
#include <sys/syslimits.h>
#elif defined(LINUX)
#include <linux/limits.h>
#else // SOLARIS || HPUX || AIX and etc..
#include <limits.h>
#endif

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

/* TBD - will change this location */
#define BEACONCXX_PID_FILE     "/var/Miracklez/beacon/beaconcxx.pid"
#define BEACON_INSTALL_DIR	"/opt/Miracklez/beacon/bin"


#ifdef  OPEN_MAX
static long openmax = OPEN_MAX;
#else
static long openmax = 0;
#endif

#include "ma/ma_common.h"
#include "bc/beacon.hxx"

using namespace mileaccess :: bc;

beacon *bc;

/*
 * If OPEN_MAX is indeterminate, we're not
 * guaranteed that this is adequate.
 */
#define OPEN_MAX_GUESS  256

long open_max(void)
{
#ifndef OPEN_MAX
    if (openmax == 0) {     /* first time through */
        errno = 0;
        if ((openmax = sysconf(_SC_OPEN_MAX)) < 0) {
            if (errno == 0)
                openmax = OPEN_MAX_GUESS;   /* it's indeterminate */
        }
    }
#endif
    return(openmax);
}

static ma_bool_t is_beacon_instance_running(const char *lock_file, pid_t *pid)
{
    int lock_file_fd = 0;
    char pid_str[20] = {0};

    lock_file_fd = open(lock_file, O_RDWR|O_CREAT, 0666);
    if(0 > lock_file_fd)
        return MA_TRUE;

    if(0 > lockf(lock_file_fd, F_TLOCK, 0)) {
    read(lock_file_fd, pid_str,sizeof(pid_str));
        close(lock_file_fd);
    *pid=atoi(pid_str);
        return MA_TRUE;
    }

    //Acquired lock so write pid into lock file
    sprintf(pid_str,"%ld\n", (long) getpid());

    //Now that we have acquired the lock, first truncate the file
    if(0 != ftruncate(lock_file_fd, 0)) {
        close(lock_file_fd);
        return MA_TRUE;
    }
    //Write pid in lock file
    write(lock_file_fd, pid_str,strlen(pid_str));
    return MA_FALSE;
}

int create_beacon_service_daemon(const char * base_dir, const char *lock_file)
{
    pid_t pid = 0;
    int open_fd =0;
 
    pid = fork();
    if(pid < 0)
      return -1;
    
    if(pid != 0)
       return pid; //We are in a parent process so return

    //Child process continues

    //First obtain a new process group
    setsid();

    //Close all open descriptors
    for(open_fd = open_max(); open_fd >= 0; --open_fd)
    	close(open_fd);

    //Take care of std lib
    open_fd = open("/dev/null", O_RDWR);
    if(-1 == open_fd)
        return -1;

    dup(open_fd);
    dup(open_fd);

    //Set process file mode creation mask
    umask(033);

    //Go to your own home dir
    chdir(base_dir);

    //Make sure that only one instance runs at a time
    if(is_beacon_instance_running(lock_file, &pid))
        return -1;

    return 0;
}

int start_beacon_service() {
    int rc =  0;
    struct sigaction action;
    sigaction (SIGPIPE, NULL, &action);
    action.sa_handler = SIG_IGN	;
    sigaction (SIGPIPE, &action, NULL);

    rc = create_beacon_service_daemon(BEACON_INSTALL_DIR, BEACONCXX_PID_FILE);

    if(-1 == rc)
        return -1;

    if(0 < rc ) { //we are in parent so exit.
        fprintf(stderr, "created beacon daemon successfully\n");
        return 0;
    }

    try {   
       bc = new beacon();
       fprintf(stderr,"BEACON created successfully\n");
	
       bc->start();
       fprintf(stderr,"BEACON started successfully\n");


        //This will block signals for a process/thread
        sigset_t sig_set;
        sigemptyset(&sig_set);

        sigaddset(&sig_set, SIGTERM);
        pthread_sigmask(SIG_SETMASK, &sig_set, NULL);

        while(1)
        {
            int  received_signal = 0, sig_return;

            sig_return = sigwait(&sig_set, &received_signal);

            if(-1 == sig_return || 0 != sig_return)
                continue;

            sig_return = sigismember(&sig_set, received_signal);
            if(1 == sig_return)
                break;
            else
                continue;
        }

        bc->stop();
        fprintf(stderr, "BEACON stopped successfully\n");
    
        delete bc;
    }
    catch(...) {
        rc = -1;
    }

    return 0;
}

int stop_beacon_service() {
    pid_t pid=0;

    if(is_beacon_instance_running(BEACONCXX_PID_FILE, &pid)) {
       kill(pid,SIGTERM);
       fprintf(stderr, "stopped beacon service\n");
    }
    else {
        fprintf(stderr, "beacon is not running\n");
    }

    return 0;
}

int status_beacon_service() {
    pid_t pid=0;

    if(is_beacon_instance_running(BEACONCXX_PID_FILE, &pid)) {
        fprintf(stderr, "beacon is running...\n");
        return 0;
    }
    else {
        fprintf(stderr, "beacon is not running\n");
    }

    return 1;
}

int main(int argc, char **argv)
{
    int rc = 0;

    if( argc != 2 ) {
        printf("Usage: %s start/stop/status \n", argv[0]);
        return 0;
    }   
    
    if(!strncmp(argv[1], "start", 5))
        rc = start_beacon_service();
    else if(!strncmp(argv[1], "stop", 4))
        rc = stop_beacon_service();
    else if(!strncmp(argv[1], "status", 5))
        rc = status_beacon_service();
   	else {
            printf("Usage: %s start/stop/status \n", argv[0]);
            rc = 0;
    	}

    return rc;
}


