#include "bc/beacon.hxx"
#include "bc/bc_strdef.hxx"

#include "ma/logger/ma_file_logger.h"

/* Hard coded this path for now, will read this from registry/config.xml */
#ifdef MA_WINDOWS
#define BC_LOG_FOLDER   "C:\\ProgramData\\McAfee\\Beacon\\logs"
#endif

/* Make sure mirror location is valid and write access */
#ifdef MA_WINDOWS
#define MIRROR_LOCATION   "C:\\ProgramData\\McAfee\\Beacon\\mirror"
#else
#define MIRROR_LOCATION   "/opt/McAfee/mirror"
#endif

::ma_logger_t *beacon_logger = NULL;
static void log_message_fun(ma_log_severity_t severity, const char *module_name, const char *log_message);

namespace mileaccess  { namespace bc {

beacon::beacon() : 
    m_client(NULL),
	m_client_logger(NULL),	
	m_info_event_handler(NULL),	
	m_property_provider(NULL),	
	m_policy_notification_handler(NULL),	
	m_task_handler(NULL),
	m_datachannel_handler(NULL),
	m_updater_handler(NULL),
	m_event_handler(NULL),
	m_repository_client(NULL),
	m_repository_mirror(NULL)

{

    try {
        this->m_client = new mileaccess::ma::client(BC_PRODUCT_ID_STR);
		this->m_client->set_logger(*this);
		/* NOTE- Purely optional and only requires mostly for interpeters langugag like JAVA/Python
            if user wants to do challenge and response authentication in addition to executable validation.
            User must add AuthorizationHash entry(which is sha256 and string representation of passphrase)
            in the /etc/ma.d/<product_id>/config.xml or HKLM\McAfee\Agent\Applications\<SOFTWARE_ID> (REG_SZ)
		*/ 
		this->m_client->set_passphrase_provider(*this);
		beacon_logger = NULL;
    }
    catch(mileaccess::ma::utils::exception &e) {
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Failed to create ma client, %d.", e.error);
        throw e;
    }
    catch(...) {
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Failed to create ma client due to unknown error");
        throw;
    }
}
void beacon::client_start() {
    try {
        m_client->start();
        MA_LOG(beacon_logger, MA_LOG_SEV_INFO, "Successfuly started ma client.");
		m_info_event_handler = new mileaccess::bc::info_event_handler(*m_client) ;
        m_property_provider = new mileaccess::bc::property_provider(*m_client);
        m_policy_notification_handler = new mileaccess::bc::policy_notification_handler(*m_client);
        m_task_handler = new mileaccess::bc::task_handler(*m_client);
        m_datachannel_handler = new mileaccess::bc::datachannel_handler(*m_client);
        m_updater_handler = new mileaccess::bc::updater_handler(*m_client);		
		m_event_handler = new bc_event_subscriber(*m_client);
		m_repository_client = new bc_repository_client(*m_client);
		std::string mirror_path(MIRROR_LOCATION);
		m_repository_mirror = new bc_repository_mirror(*m_client, mirror_path);

        MA_LOG(beacon_logger, MA_LOG_SEV_INFO, "Started beacon.");
    }
    catch(mileaccess::ma::utils::exception &e) {
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Failed to start beacon due to %d.", e.error);
        throw e;
    }
    catch(...) {
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Failed to start beacon due unknown error.");
        throw;
    }
}   

void beacon::client_stop() {
    try {
		delete m_property_provider; 
		m_property_provider = NULL;

        delete m_policy_notification_handler;
		m_policy_notification_handler = NULL;

        delete m_task_handler;
		m_task_handler = NULL;

        delete m_datachannel_handler;
		m_datachannel_handler = NULL;

        delete m_updater_handler;
		m_updater_handler = NULL;

		delete m_event_handler;
		m_event_handler = NULL;

		delete m_info_event_handler;
		m_info_event_handler = NULL;

		delete m_repository_client;
		m_repository_client = NULL;

		delete m_repository_mirror;
		m_repository_mirror = NULL;

        m_client->stop();
        MA_LOG(beacon_logger, MA_LOG_SEV_INFO, "Stopped beacon.");
    }
    catch(mileaccess::ma::utils::exception &e) {
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Failed to stop beacon due to %d.", e.error);
        throw e;
    }
    catch(...) {
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Failed to stop beacon due unknown error.");
        throw;
    }
}

void beacon::start() {
    try {
        MA_LOG(beacon_logger, MA_LOG_SEV_INFO, "registering for the ma client notification.");
		this->register_handler(*this->m_client);
        MA_LOG(beacon_logger, MA_LOG_SEV_INFO, "registered for the ma client notification.");
    }
    catch(mileaccess::ma::utils::exception &e) {
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Failed to register for ma client notification due to %d.", e.error);
        throw e;
    }
    catch(...) {
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Failed to register for ma client notification due to unknown error.");
        throw;
    }
}   

void beacon::stop() {
    try {
		 MA_LOG(beacon_logger, MA_LOG_SEV_INFO, "unregistering for the ma client notification.");
		 this->unregister_handler(*this->m_client);
         MA_LOG(beacon_logger, MA_LOG_SEV_INFO, "unregistered for the ma client notification.");
    }
    catch(mileaccess::ma::utils::exception &e) {
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "failed to unregister for the ma client notification due to %d.", e.error);
        throw e;
    }
    catch(...) {
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "failed to unregister for the ma client notification due unknown error.");
        throw;
    }
}

void beacon::on_notification(client const &cli, ::ma_client_notification_t type){
	try{		
		MA_LOG(beacon_logger, MA_LOG_SEV_INFO, "Beacon received client notification with type %d", type) ;

		switch(type) 
		{
			case MA_CLIENT_NOTIFY_STOP:
			{
				this->client_stop() ;
				if(beacon_logger) ma_logger_release(beacon_logger);				
				beacon_logger = NULL;
				break ;
			}
			case MA_CLIENT_NOTIFY_START:
			{
				ma_file_logger_t *logger = NULL;
				#ifdef MA_WINDOWS
				ma_file_logger_create(BC_LOG_FOLDER, BC_LOG_FILE_NAME_STR, NULL, &logger);
				#else
				ma_file_logger_create(NULL, BC_LOG_FILE_NAME_STR, NULL, &logger);
				#endif
				if(logger)					
					beacon_logger =  (ma_logger_t *)logger;								
				this->client_start();
				break ;
			}
			default :
				break ;
		}	
	}
	catch(...){
	}
}

void beacon::on_logmessage(::ma_log_severity_t sev, const std::string &module_name, const std::string &log_message)
{
	if(beacon_logger)
		ma_log(beacon_logger, sev, module_name.c_str(), "", 0, "", log_message.c_str());
	else
		log_message_fun(sev, module_name.c_str(), log_message.c_str());
}

ma_error_t beacon::on_provide_passphrase(unsigned char *passphrase, size_t *passphrase_size){
	static const char bc_passphrase[65] = "0123456789012345678901234567890123456789012345678901234567890123";
    /* sha256 digest of bc_passphrase in string format is c999681d5a9790c4e703e1e41dadb88a4566691f9cd58ad43c9e76f19d49a44a */
    if(passphrase && passphrase_size && *passphrase_size >= 64) {
        memcpy(passphrase, bc_passphrase, 64);
        *passphrase_size = 64;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

beacon::~beacon() {
    try {		
        delete m_client;
		delete m_client_logger;
    }
    catch(mileaccess::ma::utils::exception &e) {
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Failed to release beacon due to %d.", e.error);        
    }
    catch(...) {
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Failed to release beacon due unknown error.");        
    }

	if(beacon_logger) ma_logger_release(beacon_logger);				
	beacon_logger = NULL;
}


}}

#include <stdio.h>
#include <stdarg.h>

void log_message_fun(ma_log_severity_t severity, const char *module_name, const char *log_message)
{
	FILE *fp = NULL;
	char file_path[256] = {0};

	#ifdef MA_WINDOWS
	_snprintf(file_path, 256, "%s\\%s.log", BC_LOG_FOLDER, BC_LOG_FILE_NAME_STR);
	#else
	snprintf(file_path, 256, "%s.log", BC_LOG_FILE_NAME_STR);
	#endif

    fp = fopen (file_path,"a+");
	
    if (fp)
    {
		static const char *g_severity_names[] = {"Critical", "Error", "Warning", "Notice", "Info", "Debug", "Trace" , "Perfomance"};

		fprintf(fp," %s.%s %s\n", module_name, g_severity_names[int(severity)-1], log_message);
		fclose(fp);
    }    
}
