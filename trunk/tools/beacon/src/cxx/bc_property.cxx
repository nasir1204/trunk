#include "bc/bc_property.hxx"
#include "bc/bc_strdef.hxx"
#include "bc/bc_event.hxx"
#include "bc/bc_custom_event.hxx"

#define PROPERTY_CLIENT_EVENT_ID 10000
#define SAMPLE_THREAT_ID1 1027
#define SAMPLE_THREAT_ID2 777

extern ::ma_logger_t *beacon_logger;
namespace mileaccess  { namespace bc {

property_provider::property_provider(mileaccess::ma::client const &cli): mileaccess::ma::property_provider(cli),m_cli(cli) {
    try {
        register_provider(BC_PRODUCT_ID_STR);
    }
    catch(mileaccess::ma::utils::exception(err)) {
        bc_event_generate(cli, "BC_PROPERTY_PROVIDER_REGISTRATION_FAILED", PROPERTY_CLIENT_EVENT_ID+1, MA_EVENT_SEVERITY_CRITICAL);
		bc_custom_event_generate(cli, SAMPLE_THREAT_ID1, MA_EVENT_SEVERITY_INFORMATIONAL);
		bc_custom_event_generate(cli, SAMPLE_THREAT_ID2, MA_EVENT_SEVERITY_CRITICAL);
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Failed to register beacon as property provider with MA ,%d", err.error); 
        throw err;
    }
    MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Registered successfully beacon as property provider with MA");
    bc_event_generate(cli, "BC_PROPERTY_PROVIDER_REGISTRATION_SUCCESS", PROPERTY_CLIENT_EVENT_ID+2, MA_EVENT_SEVERITY_INFORMATIONAL);
	bc_custom_event_generate(cli, SAMPLE_THREAT_ID1, MA_EVENT_SEVERITY_INFORMATIONAL);
	bc_custom_event_generate(cli, SAMPLE_THREAT_ID2, MA_EVENT_SEVERITY_CRITICAL);
}

property_provider::~property_provider() {
    try {
        unregister_provider(BC_PRODUCT_ID_STR);
    }
    catch(mileaccess::ma::utils::exception(err)) {
        bc_event_generate(this->m_cli, "BC_PROPERTY_PROVIDER_UNREGISTRATION_FAILED", PROPERTY_CLIENT_EVENT_ID+3, MA_EVENT_SEVERITY_CRITICAL);
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Failed to unregister beacon as property provider with MA ,%d", err.error); 
        throw err;
    }
    MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Unregistered succesfully beacon as property provider with MA");
    bc_event_generate(this->m_cli, "BC_PROPERTY_PROVIDER_UNREGISTRATION_SUCCESS", PROPERTY_CLIENT_EVENT_ID+4, MA_EVENT_SEVERITY_INFORMATIONAL);
}


ma_error_t property_provider::on_collect_properties(mileaccess::ma::client const &cli, std::string const &product_id, mileaccess::ma::property_bag &properties) {
   try {

        MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Beacon prodvider callback invoked for product id %s", product_id.c_str());
        bc_event_generate(this->m_cli, "BC_PROPERTY_PROVIDER_CB", PROPERTY_CLIENT_EVENT_ID+5, MA_EVENT_SEVERITY_INFORMATIONAL);

        /*adding product version as STRING */
        properties.add_property(BC_PROP_PATH_GENERAL_STR, BC_PROP_KEY_PRODUCT_VERSION_STR, mileaccess::ma::variant(BC_PROP_VALUE_PRODUCT_VERSION_STR));
        /*adding product name as STRING */
        properties.add_property(BC_PROP_PATH_GENERAL_STR, BC_PROP_KEY_PRODUCT_NAME_STR, mileaccess::ma::variant(BC_PROP_VALUE_PRODUCT_NAME_STR));
        /*adding Showlisting Enabled STRING */
        properties.add_property(BC_PROP_PATH_SHOWLIST_STR, BC_PROP_KEY_SHOWLISTING_ENABLED_STR, mileaccess::ma::variant(BC_PROP_VALUE_SHOWLIST_ENABLED_STR));
        /*adding Showlisting list STRING */
        properties.add_property(BC_PROP_PATH_SHOWLIST_STR, BC_PROP_KEY_SHOWLISTING_LIST_STR, mileaccess::ma::variant(BC_PROP_VALUE_SHOWLISTING_LIST_STR));

        /*FUTURE- API's START adding whitelisting enabled as BOOL */
        /*properties.add_property(BC_PROP_PATH_SHOWLIST_STR, BC_PROP_KEY_SHOWLISTING_ENABLED_BOOL, mileaccess::ma::variant(MA_TRUE));*/

        /*FUTURE-adding whitelisted  list as ARRAY*/
        /*mileaccess::ma::array a;
        a.push(mileaccess::ma::variant(MA_TRUE)); //Bool - 1st element
        a.push(mileaccess::ma::variant(BC_PROP_VALUE_SHOWLISTING_LIST_STR)); // String - 2nd element
        properties.add_property(BC_PROP_PATH_SHOWLIST_STR, BC_PROP_KEY_SHOWLISTING_LIST_ARRAY, mileaccess::ma::variant(a));
        */
    }
    catch(mileaccess::ma::utils::exception(err)) {
        MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Beacon failed to add properties due to  %s", err.what());
        return MA_ERROR_UNEXPECTED;
    }
    catch(...) {
        MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Beacon failed to add properties due to  unknown exception.");
        return MA_ERROR_UNEXPECTED;
    }
    MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Beacon prodvider for product id %s provider properties finished.", product_id.c_str());
    return MA_OK;
}

    
}}
