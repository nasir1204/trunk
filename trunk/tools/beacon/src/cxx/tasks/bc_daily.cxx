#include "bc/bc_task.hxx"
#include "bc/bc_strdef.hxx"
#include "bc/bc_event.hxx"
#include "ma/scheduler/trigger.hxx"
#include "ma/scheduler/time.hxx"
#include "ma/scheduler/scheduler.hxx"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"
#include "ma/scheduler/time.hxx"

#include <stdlib.h>

extern ::ma_logger_t *beacon_logger;

using mileaccess::ma::client;
#ifdef __cplusplus
extern "C" {
#endif
ma_error_t run_daily_task(client const & cli) {
    mileaccess::ma::scheduler::scheduler s(cli);
    ma_trigger_daily_policy_t policy = {1};
    ma_task_time_t today = {0};
    mileaccess::ma::scheduler::maybe_owned_task daily = mileaccess::ma::scheduler::maybe_owned_task::new_task("daily");
    mileaccess::ma::scheduler::trigger t = mileaccess::ma::scheduler::trigger::make_daily_trigger(policy);

    get_localtime(&today);
    t.set_begin_date(today);
    daily.set_name("daily");
    daily.set_type("daily_type");
    daily.set_software_id(BC_PRODUCT_ID_STR);
    daily.add_trigger(t);
    s.add_task(daily);
    daily.drop_ownership();
}
#ifdef __cplusplus
}
#endif
