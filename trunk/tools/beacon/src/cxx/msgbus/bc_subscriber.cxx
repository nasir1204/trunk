#include "bc/msgbus/bc_subscriber.hxx"

extern ::ma_logger_t *beacon_logger;
namespace mileaccess  { namespace bc {

bc_subscriber::bc_subscriber(mileaccess::ma::client const &cli) {
    try {
		m_subscriber.create(cli.get_msgbus());
		m_subscriber.set_option(MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
		m_subscriber.set_option(MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_POOL);
		m_subscriber.start(*this, MA_LOGGER_MSG_PUB_TOPIC);
    }
    catch(mileaccess::ma::utils::exception(err)) {
		MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Failed to register subscriber for property collect,%d", err.error);
        throw err;
    }
}

bc_subscriber::~bc_subscriber() {
	try {
		m_subscriber.stop();
		m_subscriber.release();
    }
    catch(mileaccess::ma::utils::exception(err)) {
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Failed to unregister subscriber for property collect,%d", err.error);
        throw err;
    }
}

ma_error_t bc_subscriber::on_pubsub_message(char const *topic, mileaccess::ma::message const &msg) {
    try {
		MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "subscriber provided callback invoked topic %s ", topic);
		mileaccess::ma::message_auth_info auth_info = msg.get_auth_info();
		
		MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "issuer name = %s", auth_info.get_issuer_cn().c_str());
		MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "subject name = %s", auth_info.get_subject_cn().c_str());
		MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "pid  = %ld", auth_info.get_pid());
    } catch (...) {
		MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "failed to get auth info");
    }
    return MA_OK;
}

    
}}
