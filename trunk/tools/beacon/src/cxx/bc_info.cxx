#include "bc/bc_info.hxx"
#include "bc/bc_strdef.hxx"
extern ::ma_logger_t *beacon_logger;
namespace mileaccess  { namespace bc {

info_event_handler::info_event_handler(mileaccess::ma::client const &cli): mileaccess::ma::info::info_event_handler(cli), m_cli(cli) {
    try {
        //register_info_events(BC_PRODUCT_ID_STR);
    }
    catch(mileaccess::ma::utils::exception(err)) {
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Failed to register beacon for info events with MA ,%d", err.error); 
        throw err;
    }
    MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Registered successfully beacon for info events with MA");

}

info_event_handler::~info_event_handler() {
    
	try {
       // unregister_info_events(BC_PRODUCT_ID_STR);
    }
    catch(mileaccess::ma::utils::exception(err)) {
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Failed to unregister beacon for info events with MA ,%d", err.error); 
        throw err;
    }

    MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Unregistered succesfully beacon for info events with MA");
}


ma_error_t info_event_handler::on_info_event(mileaccess::ma::client const &cli, std::string const &product_id, std::string const &info_event) {
    
	try {
        MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Beacon info handler callback invoked for product id %s with info event %s", product_id.c_str(), info_event.c_str());

		MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Agent version : %s", (mileaccess::ma::get<std::string>(get_info(cli, MA_INFO_VERSION_STR))).c_str()) ;
      
    }
    catch(mileaccess::ma::utils::exception(err)) {
        MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Beacon failed to handle info event due to  %s", err.what());
        return MA_ERROR_UNEXPECTED;
    }
    catch(...) {
        MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Beacon failed to handle info event due to  unknown exception.");
        return MA_ERROR_UNEXPECTED;
    }
    MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Beacon info event handler for product id %s finished.", product_id.c_str());
    return MA_OK;
}

    
}}
