#include "bc/bc_repository_mirror.hxx"
#include "ma/mirror/mirror.hxx"
#include "bc/bc_strdef.hxx"
extern ::ma_logger_t *beacon_logger;

namespace mileaccess  { namespace bc {
	bc_repository_mirror::bc_repository_mirror(mileaccess::ma::client const &cli, std::string &destination_path):mileaccess::ma::RepositoryMirror(cli, destination_path){
		/*This API's are used on demand(unlike property collection, event generation) to create mirror, create task or get mirror state. 
		Since mirror location is mandetory and  we are not sure of location as well as use of mirroring state, so commenting this code.
		To debug/test this API's,  uncommnet the following.	As well as add remaining api's as required */
		/*
		try {
			MA_LOG(beacon_logger, MA_LOG_SEV_TRACE, "Calling Create mirror API");
			this->start();

			MA_LOG(beacon_logger, MA_LOG_SEV_TRACE, "Session Id of mirror is <%s>", this->get_sessionId().data());

			MA_LOG(beacon_logger, MA_LOG_SEV_TRACE, "mirroring state is <%d>", (int)this->get_state());

			MA_LOG(beacon_logger, MA_LOG_SEV_TRACE, "mirroring stopping");
			this->stop();

			MA_LOG(beacon_logger, MA_LOG_SEV_TRACE, "creating task");
			ma_task_t *task = this->create_task();
			if(!task)
				MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "creating task failed");
			//User need to schedule task and get session id and check state of mirroring.


			
		}
		catch(mileaccess::ma::utils::exception(err)) {
			MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "bc_repository_mirror c'tor exception with err<%d>", (int)(err.err()));
		}
		*/
		
	}
}}
