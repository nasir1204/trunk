#include "bc/bc_updater.hxx"
#include "bc/bc_strdef.hxx"

#include "ma/updater/updater.hxx"

extern ::ma_logger_t *beacon_logger;
namespace mileaccess  { namespace bc {

updater_handler::updater_handler(mileaccess::ma::client const &cli): mileaccess::ma::updater::updater_handler(cli),m_cli(cli) {
    try {
        mileaccess::ma::updater::updater u(cli);
        u.register_handler(BC_PRODUCT_ID_STR, *this);

        run_update_now();
    }
    catch(mileaccess::ma::utils::exception(err)) {
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Failed to register beacon for updater handler with MA ,%d", err.error); 
        throw err;
    }
    MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Registered succesfully beacon for updater handler with MA");
}

updater_handler::~updater_handler() {
    try {
        mileaccess::ma::updater::updater u(this->m_cli);
        u.unregister_handler(BC_PRODUCT_ID_STR);
    }
    catch(mileaccess::ma::utils::exception(err)) {
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Failed to unregister beacon for updater handler with MA ,%d", err.error); 
        throw err;
    }
    MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "UnRegistered succesfully beacon for updater handler with MA");
}

::ma_error_t updater_handler::notify_cb(client const &cli, std::string const &product_id,  std::string const &update_type, ::ma_update_state_t state, std::string const &message, std::string const& extra_info, ::ma_updater_product_return_code_t &return_code) {
    try {
         MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Beacon noitfy invoked product = %s, update_type = %s, update_state = %d.", product_id.c_str(), update_type.c_str(), state) ;
         return_code = MA_UPDATER_PRODUCT_RETURN_CODE_OK;
    }
    catch(...) {
    }
    return MA_OK;
}

::ma_error_t updater_handler::set_cb(client const &cli, std::string const &product_id, std::string const &update_type, std::string const &key, mileaccess::ma::variant const &value, ::ma_updater_product_return_code_t &return_code) {
    try {
         MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Beacon set_cb invoked product = %s, update_type = %s, key = %s.", product_id.c_str(), update_type.c_str(), key.c_str());
         return_code = MA_UPDATER_PRODUCT_RETURN_CODE_OK;
    }
    catch(...) {
    }
    return MA_OK;
}

::ma_error_t updater_handler::get_cb(client const &cli, std::string const &product_id, std::string const &update_type, std::string const &key, mileaccess::ma::variant &value, ::ma_updater_product_return_code_t &return_code) {
    try {
         MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Beacon get_cb invoked product = %s, update_type = %s, key = %s.", product_id.c_str(), update_type.c_str(), key.c_str());
         return_code = MA_UPDATER_PRODUCT_RETURN_CODE_OK;
    }
    catch(...) {
    }
    return MA_OK;
}

::ma_error_t updater_handler::updater_event_cb(client const &cli, std::string const &product_id, const updater_event &upd_event) {
    try {
         MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Beacon event_cb invoked product = %s.", product_id.c_str());
    }
    catch(...) {
    }
    return MA_OK;
}


 void updater_handler::run_update_now() {
    try {
        mileaccess::ma::updater::updater u(this->m_cli);
        mileaccess::ma::updater::update_request request(MA_UPDATER_UPDATE_TYPE_NORMAL_UPDATE);
        mileaccess::ma::buffer session_id;
        
        MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Requesting MA for on demand update.");
        u.start_update(request, session_id);
        MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Requested done for on demand update.");
    }
    catch(mileaccess::ma::utils::exception(err)) {
        MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Failed to send request for on demand update due to  %s", err.what());
    }
    catch(...) {
        MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Failed to send request for on demand update due to  unknown exception");
    }
 }

}}
