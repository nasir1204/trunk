#include "bc/bc_datachannel.hxx"
#include "bc/bc_strdef.hxx"

#include "ma/datachannel/datachannel.hxx"

extern ::ma_logger_t *beacon_logger;
namespace mileaccess  { namespace bc {

datachannel_handler::datachannel_handler(mileaccess::ma::client const &cli): mileaccess::ma::datachannel::datachannel_handler(cli),m_cli(cli) {
     try {
        mileaccess::ma::datachannel::datachannel d(cli);
        register_datachannel_message_handler(BC_PRODUCT_ID_STR);
        register_notification_handler(BC_PRODUCT_ID_STR);
        d.subscribe(BC_PRODUCT_ID_STR, BC_DC_ITEM1, MA_DC_SUBSCRIBER_LISTENER);

        send_datachannel_item();
    }
    catch(mileaccess::ma::utils::exception(err)) {
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Failed to register beacon for datachannel handler and messages with MA ,%d", err.error); 
        throw err;
    }
    MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Registered succesfully beacon for datachannel handler and messages with MA");
}

datachannel_handler::~datachannel_handler() {
    try {
        mileaccess::ma::datachannel::datachannel d(this->m_cli);
        d.unsubscribe(BC_PRODUCT_ID_STR, BC_DC_ITEM1);
        unregister_datachannel_message_handler(BC_PRODUCT_ID_STR);
        unregister_notification_handler(BC_PRODUCT_ID_STR);
    }
    catch(mileaccess::ma::utils::exception(err)) {
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Failed to unregister beacon for datachannel handler and messages with MA ,%d", err.error); 
        throw err;
    }
    MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "UnRegistered succesfully beacon for datachannel handler and messages with MA");
}

void datachannel_handler::on_datachannel_notification(mileaccess::ma::client const &c, std::string const &product_id, std::string const &message_id, ::ma_int64_t correlation_id, ::ma_datachannel_item_notification_t status, ::ma_error_t reason) {
    try {

        MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Beacon datachannel on notification callback invoked product id = %s, message id=%s, correlation id=%d, notification=%d.",
            product_id.c_str(), message_id.c_str(), correlation_id, status);
    }
    catch(...) {
    }
}

void datachannel_handler::on_datachannel_message(mileaccess::ma::client const &c, std::string const &product_id, std::string const &message_id, ::ma_int64_t correlation_id, mileaccess::ma::datachannel::item message) {
    try {

        MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Beacon datachannel on message callback invoked product id = %s, message id=%s, correlation id=%d.",
            product_id.c_str(), message_id.c_str(), correlation_id);

        if(product_id == BC_PRODUCT_ID_STR) {
            mileaccess::ma::variant payload = message.get_payload();
            MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Received message from ePO. Item ID: [%s], Correlation ID:[%d].", message_id.c_str(), message.get_correlation_id());
            /*Payload can be used here*/
            MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Processed %s", message_id.c_str());    
		}
		else 
			MA_LOG(beacon_logger, MA_LOG_SEV_CRITICAL, "Unable to process", message_id.c_str());
    }
    catch(mileaccess::ma::utils::exception(err)) {
        MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Beacon failed to parse datchannel message due to  %s", err.what());
    }
    catch(...) {
        MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Beacon failed to parse datachannel message due to  unknown exception.");
    }
}


 void datachannel_handler::send_datachannel_item() {
    try {
	mileaccess::ma::variant v(BC_DC_DATA);
        mileaccess::ma::datachannel::item message(BC_DC_ITEM1, v);

        MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Sending datachannel messages.");
        message.set_option(MA_DATACHANNEL_PERSIST, MA_TRUE);
        message.set_option(MA_DATACHANNEL_DELIVERY_NOTIFICATION, MA_TRUE);
        message.set_option(MA_DATACHANNEL_PURGED_NOTIFICATION, MA_TRUE);
        message.set_option(MA_DATACHANNEL_ENCRYPT, MA_TRUE);
        message.set_ttl(5000);
        mileaccess::ma::datachannel::datachannel d(this->m_cli);
        d.async_send(BC_PRODUCT_ID_STR, message);
        MA_LOG(beacon_logger, MA_LOG_SEV_INFO, "Sent DC item to datachannel service");
    }
    catch(mileaccess::ma::utils::exception(err)) {
        MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Failed to send datachannel message due to  %s", err.what());
    }
    catch(...) {
        MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Failed to send  datachannel message due to  unknown exception.");
    }
 }

}}
