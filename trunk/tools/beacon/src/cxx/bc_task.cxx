#include "bc/bc_task.hxx"
#include "bc/bc_strdef.hxx"
#include "bc/bc_event.hxx"

#include <stdlib.h>

extern ::ma_logger_t *beacon_logger;
#define TASK_CLIENT_EVENT_ID 12000

namespace mileaccess  { namespace bc {

task_handler::task_handler(mileaccess::ma::client const &cli): mileaccess::ma::scheduler::task_handler(cli),m_cli(cli) {
    try {
        register_task_handler(BC_PRODUCT_ID_STR, NULL);
    }
    catch(mileaccess::ma::utils::exception(err)) {
        bc_event_generate(this->m_cli, "BC_TASK_CALLBACKS_REGISTRATION_FAILED", TASK_CLIENT_EVENT_ID+1, MA_EVENT_SEVERITY_CRITICAL);
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Failed to register beacon  for task notifications with MA ,%d", err.error); 
        throw err;
    }
    MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Registered succesfully beacon for  for task notifications with MA");
    bc_event_generate(this->m_cli, "BC_TASK_CALLBACKS_REGISTRATION_SUCCESS", TASK_CLIENT_EVENT_ID+2, MA_EVENT_SEVERITY_INFORMATIONAL);
}

task_handler::~task_handler() {
    try {
        unregister_task_handler(BC_PRODUCT_ID_STR, NULL);
    }
    catch(mileaccess::ma::utils::exception(err)) {
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Failed to un-register beacon for task notification with MA ,%d", err.error); 
		bc_event_generate(this->m_cli, "BC_TASK_CALLBACKS_UNREGISTRATION_FAILED", TASK_CLIENT_EVENT_ID+3, MA_EVENT_SEVERITY_CRITICAL);
        throw err;
    }
    bc_event_generate(this->m_cli, "BC_TASK_CALLBACKS_UNREGISTRATION_SUCCESS", TASK_CLIENT_EVENT_ID+4, MA_EVENT_SEVERITY_INFORMATIONAL);
    MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Unregistered succesfully beacon for task notification with MA");
}

::ma_error_t task_handler::on_execute(mileaccess::ma::client  const &cli, std::string const &product_id, std::string const &task_type, mileaccess::ma::scheduler::task &t) {
    try {
        
        MA_LOG(beacon_logger, MA_LOG_SEV_INFO, "Beacon task execute callback invoked for task type =%s, creator id=%s, id= %s, name = %s",
                    t.get_type().c_str(), t.get_creator_id().c_str(), t.get_id().c_str(), t.get_name().c_str());

        if(task_type == BC_TASK_EVENT_TYPE_STR) {
            MA_LOG(beacon_logger, MA_LOG_SEV_INFO, "Beacon \"Event\" task has been submitted");
            handle_beacon_event_task(t);
            MA_LOG(beacon_logger, MA_LOG_SEV_INFO, "Beacon \"Event\" task succedded");
            bc_event_generate(this->m_cli, "BC_TASK_EXECUTE_DONE", TASK_CLIENT_EVENT_ID+5, MA_EVENT_SEVERITY_INFORMATIONAL);
			if(MA_OK != ma_scheduler_update_task_status(cli.get(), t.get_id().c_str(), MA_TASK_EXEC_SUCCESS)) 
                MA_LOG(beacon_logger, MA_LOG_SEV_INFO, "Beacon task id(%s) status updation failed", t.get_id().c_str());
        }
        else if(task_type == BC_TASK_LONG_RUNNING_TYPE_STR) {
            MA_LOG(beacon_logger, MA_LOG_SEV_INFO, "Beacon \"LongRunning\" task has been submitted");
            handle_beacon_long_running_task(t);
            bc_event_generate(this->m_cli, "BC_TASK_EXECUTE_DONE", TASK_CLIENT_EVENT_ID+7, MA_EVENT_SEVERITY_INFORMATIONAL);
        }
        else {
            MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Beacon received unknown task");
            bc_event_generate(this->m_cli, "BC_TASK_EXECUTE_DONE", TASK_CLIENT_EVENT_ID+8, MA_EVENT_SEVERITY_CRITICAL);
			if(MA_OK != ma_scheduler_update_task_status(cli.get(), t.get_id().c_str(), MA_TASK_EXEC_SUCCESS)) 
                MA_LOG(beacon_logger, MA_LOG_SEV_INFO, "Beacon task id(%s) status updation failed", t.get_id().c_str());
        }
    }
    catch(mileaccess::ma::utils::exception(err)) {
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Beacon failed to execute task due to  %s", err.what());
    }
    catch(...) {
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Beacon failed to execute task due to  unknown exception.");
    }
    return MA_OK;
}


::ma_error_t task_handler::on_stop(mileaccess::ma::client  const &cli, std::string const &product_id, std::string const &task_type, mileaccess::ma::scheduler::task &t) {
    try {
        
        MA_LOG(beacon_logger, MA_LOG_SEV_INFO, "Beacon task stop callback invoked for task type =%s, creator id=%s, id= %s, name = %s",
            t.get_type().c_str(), t.get_creator_id().c_str(), t.get_id().c_str(), t.get_name().c_str());

        bc_event_generate(this->m_cli, "BC_TASK_STOP_NOTIFICATION", TASK_CLIENT_EVENT_ID+10, MA_EVENT_SEVERITY_INFORMATIONAL);

    }
    catch(mileaccess::ma::utils::exception(err)) {
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Beacon on stop callback failed due to  %s", err.what());
    }
    catch(...) {
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Beacon on stop callback failed due to  unknown exception.");
    }
    return MA_OK;
}

::ma_error_t task_handler::on_remove(mileaccess::ma::client  const &cli, std::string const &product_id, std::string const &task_type, mileaccess::ma::scheduler::task &t) {
    try {
        
        MA_LOG(beacon_logger, MA_LOG_SEV_INFO, "Beacon task remove callback invoked for task type =%s, creator id=%s, id= %s, name = %s",
                t.get_type().c_str(), t.get_creator_id().c_str(), t.get_id().c_str(), t.get_name().c_str());

        bc_event_generate(this->m_cli, "BC_TASK_REMOVE_NOTIFICATION", TASK_CLIENT_EVENT_ID+11, MA_EVENT_SEVERITY_INFORMATIONAL);
    }
    catch(mileaccess::ma::utils::exception(err)) {
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Beacon on remove callback failed due to  %s", err.what());
    }
    catch(...) {
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Beacon on remove callback failed due to  unknown exception.");
    }
    return MA_OK;
}

::ma_error_t task_handler::on_update_status(mileaccess::ma::client  const &cli, std::string const &product_id, std::string const &task_type, mileaccess::ma::scheduler::task &t) {
    try {
        
        MA_LOG(beacon_logger, MA_LOG_SEV_INFO, "Beacon task remove callback invoked for task type =%s, creator id=%s, id= %s, name = %s",
                t.get_type().c_str(), t.get_creator_id().c_str(), t.get_id().c_str(), t.get_name().c_str());

        bc_event_generate(this->m_cli, "BC_TASK_REMOVE_NOTIFICATION", TASK_CLIENT_EVENT_ID+11, MA_EVENT_SEVERITY_INFORMATIONAL);


        MA_LOG(beacon_logger, MA_LOG_SEV_INFO, "Beacon task update callback invoked for task type =%s, creator id=%s, id= %s, name = %s",
            t.get_type().c_str(), t.get_creator_id().c_str(), t.get_id().c_str(), t.get_name().c_str());

        if(task_type == BC_TASK_EVENT_TYPE_STR) {
            MA_LOG(beacon_logger, MA_LOG_SEV_INFO, "Beacon \"Event\" task succedded");
            /*TODO - needs to change it when scheduler cxx binding have the same API */
            if(MA_OK != ma_scheduler_update_task_status(cli.get(), t.get_id().c_str(), MA_TASK_EXEC_SUCCESS)) 
                MA_LOG(beacon_logger, MA_LOG_SEV_INFO, "Beacon task id(%s) status updation failed", t.get_id().c_str());
        }
        else if(task_type == BC_TASK_LONG_RUNNING_TYPE_STR) {
            MA_LOG(beacon_logger, MA_LOG_SEV_INFO, "Beacon \"LongRunning\" task has been submitting updating status");
        }
        else {
            MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Beacon received unknown task");
        }
    }
    catch(mileaccess::ma::utils::exception(err)) {
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Beacon on updater status callback failed due to  %s", err.what());
    }
    catch(...) {
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Beacon on updater status callback failed due to  unknown exception.");
    }
    return MA_OK;
}


void task_handler::handle_beacon_event_task(mileaccess::ma::scheduler::task &t) {
    try {
        int num_of_events = 0, i =0;
        ma_event_severity_t severity = MA_EVENT_SEVERITY_INFORMATIONAL;
        
        severity = (ma_event_severity_t)atoi(mileaccess::ma::get<std::string>(t.get_setting("BSPP_Task", "Severity")).c_str());
        num_of_events = atoi(mileaccess::ma::get<std::string>(t.get_setting("BSPP_Task", "NumEvents")).c_str());

        for(i = 0 ;  i < num_of_events; i++)
            bc_event_generate(this->m_cli, "BC_TASK_EVENT_GENERATION", TASK_CLIENT_EVENT_ID+ 12 + i, severity);

    }
    catch(mileaccess::ma::utils::exception(err)) {
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Beacon event task failed due to  %s", err.what());
    }
    catch(...) {
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Beacon event task failed due to  unknown exception.");
    }
    return ;
}

void task_handler::handle_beacon_long_running_task(mileaccess::ma::scheduler::task &t) {
     try {
        long duration = 0;
        duration = atol(mileaccess::ma::get<std::string>(t.get_setting("BSPP_Task", "Duration")).c_str());
        MA_LOG(beacon_logger, MA_LOG_SEV_INFO, "Beacon long running task duration , %ld.", duration); 

    }
    catch(mileaccess::ma::utils::exception(err)) {
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Beacon long running task failed due to  %s", err.what());
    }
    catch(...) {
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Beacon long running task failed due to  unknown exception.");
    }
    return ;

}
    
}}
