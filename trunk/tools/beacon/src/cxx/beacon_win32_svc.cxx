#include "bc/beacon.hxx"
#include "bc/bc_strdef.hxx"

#include <windows.h>
#include <tchar.h>


#define BC_COUNTOF(x) (sizeof(x)/sizeof((x)[0]))
/*
 Register this as a service:

 sc[.exe] create masvc binPath= c:\dev\ma\src\trunk\build\msvc\Debug\bcsvc++.exe start= auto
 */
using namespace mileaccess::bc;

typedef struct service_data_s {
  SERVICE_STATUS_HANDLE     service_status_handle;

  SERVICE_STATUS            service_status;

  beacon					*beacon;
  HANDLE                    stop_event;

} service_data_t;

void set_service_state(service_data_t *sd, DWORD state) {
    sd->service_status.dwCurrentState = state;
    SetServiceStatus(sd->service_status_handle, &sd->service_status);
}

static int service_run(void);

static void WINAPI service_main(DWORD dwNumServicesArgs, LPTSTR *lpServiceArgVectors);

static DWORD WINAPI handler_ex(DWORD dwControl, DWORD dwEventType, LPVOID lpEventData, LPVOID lpContext);

/* for debug printing */

static struct service_description {
    const char *event_description;
    const char *event_details;
} service_control_code_text(DWORD dwControl, DWORD dwEventType, LPVOID lpEventData);


int WINAPI _tWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PTSTR pCmdLine, int nCmdShow) {
    /* check it is actually being started as a service (as there could be several cmd line options) */
    return service_run();
}


static int service_run(void) {
    SERVICE_TABLE_ENTRY service_start_table = {_T(""),&service_main};
    if (!StartServiceCtrlDispatcher(&service_start_table)) {/* This call blocks, see you in service_main()*/
        return GetLastError();
    }

    return NO_ERROR;
}

static void WINAPI service_main(DWORD dwNumServicesArgs, LPTSTR *lpServiceArgVectors) {
    char szPath[MAX_PATH]={0};
    service_data_t sd = {0};
	size_t i = 0;
	sd.service_status.dwServiceType = SERVICE_WIN32_OWN_PROCESS;
    sd.service_status.dwControlsAccepted = SERVICE_ACCEPT_SHUTDOWN | SERVICE_ACCEPT_STOP | SERVICE_ACCEPT_POWEREVENT | SERVICE_ACCEPT_SESSIONCHANGE | SERVICE_ACCEPT_TIMECHANGE;
    sd.service_status.dwWin32ExitCode = NO_ERROR;
    sd.service_status.dwServiceSpecificExitCode = NO_ERROR;
    sd.service_status.dwCheckPoint = 0;
    sd.service_status.dwWaitHint = 0;

    sd.stop_event = CreateEvent(NULL,TRUE,FALSE,NULL);

    sd.service_status_handle = RegisterServiceCtrlHandlerEx(_T(""), &handler_ex, &sd);
	
	

    if (NULL == sd.service_status_handle) {
        CloseHandle(sd.stop_event);
    	set_service_state(&sd, SERVICE_STOPPED);
        return;
    }
	if( !GetModuleFileNameA( NULL, szPath, MAX_PATH ) )
		return;
	for(i=strlen(szPath);i>0;i--){
		if('\\' == szPath[i]){
			szPath[i+1] = '\0';
			break;
		}
	}
	SetCurrentDirectoryA(szPath);

    set_service_state(&sd, SERVICE_START_PENDING);
	try {

        sd.beacon = new beacon();
        sd.beacon->start();
        /* process events */
        set_service_state(&sd, SERVICE_RUNNING);
        WaitForSingleObject(sd.stop_event,INFINITE);
        sd.beacon->stop();
        delete sd.beacon;         
    }
    catch(...) {
    }

    CloseHandle(sd.stop_event);
    set_service_state(&sd, SERVICE_STOPPED);
}


static DWORD WINAPI handler_ex(DWORD dwControl, DWORD dwEventType, LPVOID lpEventData, LPVOID lpContext) {
    service_data_t *sd = (service_data_t*)lpContext;
    //struct service_description service_control_description = service_control_code_text(dwControl, dwEventType, lpEventData); 
    
    switch (dwControl) {
    case SERVICE_CONTROL_INTERROGATE: return NO_ERROR;

    case SERVICE_CONTROL_SHUTDOWN: 
        /* fall through */

    case SERVICE_CONTROL_STOP: {
    
        set_service_state(sd, SERVICE_STOP_PENDING);
        SetEvent(sd->stop_event);
        return NO_ERROR;
        }
    }

    return ERROR_CALL_NOT_IMPLEMENTED;

}

#define MK_TEXT_TRIPLE(x,fn) {x,#x,fn}

static const char *session_change_details(DWORD dwEventType, LPVOID lpEventData);

static struct service_description service_control_code_text(DWORD dwControl, DWORD dwEventType, LPVOID lpEventData) {
    struct service_description result = {"Unknown Service Control Code",""};
    struct service_control_names {
        DWORD control;
        const char *name;
        const char * (*event_details_text)(DWORD, LPVOID);
    } control_names[] = {
        MK_TEXT_TRIPLE(SERVICE_CONTROL_CONTINUE, 0),
        MK_TEXT_TRIPLE(SERVICE_CONTROL_INTERROGATE, 0),
        MK_TEXT_TRIPLE(SERVICE_CONTROL_NETBINDADD, 0),
        MK_TEXT_TRIPLE(SERVICE_CONTROL_NETBINDDISABLE, 0),
        MK_TEXT_TRIPLE(SERVICE_CONTROL_NETBINDENABLE, 0),
        MK_TEXT_TRIPLE(SERVICE_CONTROL_NETBINDREMOVE, 0),
        MK_TEXT_TRIPLE(SERVICE_CONTROL_PARAMCHANGE, 0),
        MK_TEXT_TRIPLE(SERVICE_CONTROL_PAUSE, 0),
        MK_TEXT_TRIPLE(SERVICE_CONTROL_PRESHUTDOWN, 0),
        MK_TEXT_TRIPLE(SERVICE_CONTROL_SHUTDOWN, 0),
        MK_TEXT_TRIPLE(SERVICE_CONTROL_STOP, 0),
        MK_TEXT_TRIPLE(SERVICE_CONTROL_DEVICEEVENT, 0),
        MK_TEXT_TRIPLE(SERVICE_CONTROL_HARDWAREPROFILECHANGE, 0),
        MK_TEXT_TRIPLE(SERVICE_CONTROL_POWEREVENT, 0),
        MK_TEXT_TRIPLE(SERVICE_CONTROL_SESSIONCHANGE, &session_change_details),
        MK_TEXT_TRIPLE(SERVICE_CONTROL_TIMECHANGE, 0),
        MK_TEXT_TRIPLE(SERVICE_CONTROL_TRIGGEREVENT, 0),
        {0x00000040, "SERVICE_CONTROL_USERMODEREBOOT", 0}
    };

    int i;
    for (i = 0; i!=BC_COUNTOF(control_names);++i) {
        if (control_names[i].control == dwControl) {
            result.event_description = control_names[i].name;
            if (control_names[i].event_details_text) {
                result.event_details = control_names[i].event_details_text(dwEventType, lpEventData);
            }
            break;
        }
    }

    return result;
}


#define MK_TEXT_TUPLE(x) {x,#x}


static const char *session_change_details(DWORD dwEventType, LPVOID lpEventData) {
    struct session_change_names {
        DWORD type;
        const char *name;
    } session_change_names[] = {
        MK_TEXT_TUPLE(WTS_CONSOLE_CONNECT),
        MK_TEXT_TUPLE(WTS_CONSOLE_DISCONNECT),
        MK_TEXT_TUPLE(WTS_REMOTE_CONNECT),
        MK_TEXT_TUPLE(WTS_REMOTE_DISCONNECT),
        MK_TEXT_TUPLE(WTS_SESSION_LOGON),
        MK_TEXT_TUPLE(WTS_SESSION_LOGOFF),
        MK_TEXT_TUPLE(WTS_SESSION_LOCK),
        MK_TEXT_TUPLE(WTS_SESSION_UNLOCK),
        MK_TEXT_TUPLE(WTS_SESSION_REMOTE_CONTROL), /* GetSystemMetrics(SM_REMOTECONTROL, ...)  */
        {0xA, "WTS_SESSION_CREATE"},
        {0xB, "WTS_SESSION_TERMINATE"}
    };
    int i;
    for (i = 0; i!=BC_COUNTOF(session_change_names);++i) {
        if (session_change_names[i].type == dwEventType) {
            return session_change_names[i].name;
        }
    }

    return "Unknown session change event";
}
