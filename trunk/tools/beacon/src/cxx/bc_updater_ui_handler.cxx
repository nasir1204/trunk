#include "bc/bc_updater_ui_handler.hxx"
#include "bc/bc_strdef.hxx"

#include "ma/updater/updater.hxx"

extern ::ma_logger_t *beacon_logger;
namespace mileaccess  { namespace bc {

updater_ui_handler::updater_ui_handler(mileaccess::ma::client const &cli): mileaccess::ma::updater::ui_registrar(cli.get()),m_cli(cli) {
    try {
	std::string s(BC_PRODUCT_ID_STR);
        register_ui_provider(*this, s, MA_MSGBUS_CALLBACK_THREAD_POOL);
    }
    catch(mileaccess::ma::utils::exception(err)) {
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Failed to register beacon for updater ui handler with MA ,%d", err.error); 
        throw err;
    }
    MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Registered succesfully beacon for updater ui handler with MA");
}

updater_ui_handler::~updater_ui_handler() {
    try {
	std::string s(BC_PRODUCT_ID_STR);
        unregister_ui_provider(s);
    }
    catch(mileaccess::ma::utils::exception(err)) {
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Failed to unregister beacon for updater ui handler with MA ,%d", err.error); 
        throw err;
    }
    MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "UnRegistered succesfully beacon for updater ui handler with MA");
}

::ma_error_t updater_ui_handler::on_show_ui(mileaccess::ma::client const &client, std::string &session_id, ::ma_updater_ui_type_t type, ui_request const &request, ui_response &response) {
    try {
        MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Beacon updater_ui_provider_show_ui_cb invoked with ui type (%d)", type) ;
        response.set_return_code(MA_UPDATER_UI_RC_NO_UI);
        post_ui_provider_response(response);
    }
    catch(...) {
    }
    return MA_OK;
}

::ma_error_t updater_ui_handler::on_progress(mileaccess::ma::client const &client, std::string &session_id, ::ma_int32_t progress_step, ::ma_int32_t max_progress_step, const char *progress_message) {
    try {
        MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Beacon updater_ui_provider_progress_cb invoked with progress_step(%d), max_progress_step(%d), progress_message(%s)",
                progress_step, max_progress_step, progress_message ? progress_message : "") ;
    }
    catch(...) {
    }
    return MA_OK;
}

::ma_error_t updater_ui_handler::on_end_ui(mileaccess::ma::client const &client, std::string &session_id, const char *title, const char *end_message) {
    try {
        MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Beacon updater_ui_provider_end_ui_cb invoked with title(%s), end_message(%s)", title, end_message) ;
    }
    catch(...) {
    }
    return MA_OK;
}

::ma_error_t updater_ui_handler::on_event(mileaccess::ma::client const &client, std::string &session_id, updater_event const &ei) {
    try {
        
        MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Beacon updater_ui_provider_event_cb invoked" ) ;
    }
    catch(...) {
    }
    return MA_OK;
}

}}
