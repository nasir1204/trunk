#include "bc/bc_policy.hxx"
#include "bc/bc_strdef.hxx"
#include "bc/bc_event.hxx"
#include "ma/crypto/crypto.hxx"
#include "ma/policies/policy_bag.hxx"
#include "ma/policies/policy_bag_iterator.hxx"
#include <stdio.h>

#define POLICY_CLIENT_EVENT_ID 11000
extern ::ma_logger_t *beacon_logger;
// File for storing policy information
#define FNAME "policies_cxx.txt"

namespace mileaccess  { namespace bc {

policy_notification_handler::policy_notification_handler(mileaccess::ma::client const &cli) : mileaccess::ma::policies::policy_notification_handler(cli),m_cli(cli) {
    try {
        register_policy_notification_handler(BC_PRODUCT_ID_STR);
    }
    catch(mileaccess::ma::utils::exception(err)) {
        bc_event_generate(cli, "BC_POLICY_NOTIFICATION_REGISTRATION_FAILED", POLICY_CLIENT_EVENT_ID+1, MA_EVENT_SEVERITY_CRITICAL);
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Failed to register beacon  for policy notification with MA ,%d", err.error);
        throw err;
    }
    MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Registered successfully beacon for policy notification with MA");
    bc_event_generate(cli, "BC_POLICY_NOTIFICATION_REGISTRATION_SUCCESS", POLICY_CLIENT_EVENT_ID+2, MA_EVENT_SEVERITY_INFORMATIONAL);
}

policy_notification_handler::~policy_notification_handler() {
    try {
        unregister_policy_notification_handler(BC_PRODUCT_ID_STR);
    }
    catch(mileaccess::ma::utils::exception(err)) {
        bc_event_generate(this->m_cli, "BC_POLICY_NOTIFICATION_UNREGISTRATION_FAILED", POLICY_CLIENT_EVENT_ID+3, MA_EVENT_SEVERITY_CRITICAL);
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Failed to un-register beacon for policy notification with MA ,%d", err.error);
        throw err;
    }
    MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Unregistered successfully beacon for policy notification with MA");
    bc_event_generate(this->m_cli, "BC_POLICY_NOTIFICATION_UNREGISTRATION_SUCCESS", POLICY_CLIENT_EVENT_ID+4, MA_EVENT_SEVERITY_INFORMATIONAL);
}


ma_error_t policy_notification_handler::on_policy_notification(mileaccess::ma::client const &cli, std::string const &product_id, mileaccess::ma::policies::policy_uri const &info_uri) {
   /* For logging policies (contents of policy bag) into a file */
	FILE *fp = NULL;
    try {
        
        MA_LOG(beacon_logger, MA_LOG_SEV_INFO, "Beacon policy notification callback invoked for reason %d",info_uri.get_notification_type());
        bc_event_generate(this->m_cli, "BC_POLICY_NOTIFICATION", POLICY_CLIENT_EVENT_ID+5, MA_EVENT_SEVERITY_INFORMATIONAL);

        mileaccess::ma::policies::policy_bag pb(cli, info_uri);

		fp = fopen(FNAME, "a"); // Opening in append mode

		fprintf(fp, "-----------------------------Policy enforcment Start----------------------------------------- \n");
        /* Approach 1-  To get specific section and key based uri */
		mileaccess::ma::policies::policy_uri t_uri;		
		std::string f = "BeaconMETA", c = "eventgen", t = "eventgen", n = "My Default";
		t_uri.set_feature(f);
		t_uri.set_category(c);
		t_uri.set_type(t);
		t_uri.set_name(n);
		
		/* pso name, param int and param str can also be set here if already knows */
		mileaccess::ma::variant severity = pb.get_value(t_uri, "Event", "NumEvents");
		fprintf(fp, "-----------------------------Policy enforcment Approach - 1----------------------------------------- \n");
        MA_LOG(beacon_logger, MA_LOG_SEV_INFO, "Single section %s and key % s received %s.", "Event", "Severity", mileaccess::ma::get<std::string>(severity).c_str());
		fprintf(fp, "Feature -> %s \t Category -> %s \t Type -> %s \t Name -> %s section name -> %s setting name -> %s settign value -> %s\n", "BeaconMETA", "eventgen", "eventgen", "My Default", "Event", "Severity", mileaccess::ma::get<std::string>(severity).c_str());
        
        /* Approach 2 - iterate all the policies */
		fprintf(fp, "-----------------------------Policy enforcment Approach - 2----------------------------------------- \n");
        for (mileaccess::ma::policies::policy_bag_iterator it( pb ); it; ++it) {        

			mileaccess::ma::policies::const_policy_uri uri((*it).uri);
            
            std::string feature = uri.get_feature(), category = uri.get_category(), type = uri.get_type(), name = uri.get_name();
			std::string pso_name = uri.get_pso_name(), param_int = uri.get_pso_param_int(), param_str = uri.get_pso_param_str();
            /*TODO - need to fix cxx bindings to create object with const c handle */
            mileaccess::ma::variant v = mileaccess::ma::variant((ma_variant_t *)(*it).value, true);
            
            //MA_LOG(beacon_logger, MA_LOG_SEV_TRACE, "feature=%s, category=%s, type=%s, name=%s", feature.c_str(), category.c_str(), type.c_str(), name.c_str());
			fprintf(fp, "Feature -> %s \t Category -> %s \t Type -> %s \t Name -> %s pso name -> %s param int -> %s param str -> %s\n", feature.c_str(), category.c_str(), type.c_str(), name.c_str(), pso_name.c_str(), param_int.c_str(), param_str.c_str());

            std::string section = (*it).section_name, key = (*it).key_name, value = mileaccess::ma::get<std::string>(v);
            
            if(key == "szAdminOverridePassword" && !value.empty()) {                
                //MA_LOG(beacon_logger, MA_LOG_SEV_TRACE, "section=%s, key=%s, value=%s",section.c_str(), key.c_str(), value.c_str());
				fprintf(fp, "Section = %s, Key = %s, Value = %s \n", section.c_str(), key.c_str(), value.c_str());

                mileaccess::ma::buffer buffer = mileaccess::ma::crypto::decrypt(this->m_cli, MA_CRYPTO_ALGO_TYPE_POLICY_AES128_STR, (const unsigned char *)value.c_str(), value.length());

                //MA_LOG(beacon_logger, MA_LOG_SEV_TRACE, "section=%s, key=%s, value=%s",section.c_str(), key.c_str(), buffer.data());
				fprintf(fp, "Section = %s, Key = %s, Value = %s \n", section.c_str(), key.c_str(), buffer.data());
            }
            else {
                //MA_LOG(beacon_logger, MA_LOG_SEV_TRACE, "section=%s, key=%s, value=%s", section.c_str(), key.c_str(), value.c_str());
				fprintf(fp, "Section = %s, Key = %s, Value = %s \n", section.c_str(), key.c_str(), value.c_str());
			}          
		}
		
		/* Approach 3 -  To iterate  in different ways */
		fprintf(fp, "-----------------------------Policy enforcment Approach - 3----------------------------------------- \n");
		{	                     
			mileaccess::ma::policies::policy_uri full_policies_uri;
			std::string software_id(product_id);
			full_policies_uri.set_software_id(software_id);
			mileaccess::ma::policies::policy_bag fpb(cli, full_policies_uri);
			for (mileaccess::ma::policies::policy_bag_uri_iterator uri_it( fpb ); uri_it; ++uri_it) {   
				mileaccess::ma::policies::const_policy_uri curi((*uri_it).uri);	
				for (mileaccess::ma::policies::policy_bag_section_iterator section_it(fpb,curi); section_it; ++section_it) {   
					for (mileaccess::ma::policies::policy_bag_setting_iterator settings_it(fpb, curi, (*section_it).section_name); settings_it; ++settings_it) {   						
						mileaccess::ma::variant v = mileaccess::ma::variant((ma_variant_t *)(*settings_it).key_value, true);
						fprintf(fp, "Feature -> %s \t Category -> %s \t Type -> %s \t Name -> %s \t PSO Name -> %s \t param int -> %s \t param str -> %s \t section name -> %s \t setting name -> %s \t setting value -> %s \n", curi.get_feature().c_str(),
																									  curi.get_category().c_str(), 
																									  curi.get_type().c_str(),
																									  curi.get_name().c_str(),
																									  curi.get_pso_name().c_str(),
																									  curi.get_pso_param_int().c_str(),
																									  curi.get_pso_param_str().c_str(),
																									  (*section_it).section_name,
																									  (*settings_it).key_name,
																									  mileaccess::ma::get<std::string>(v).c_str());
					}
				}
			}
		}

		/* Approach 4 -  To get all the policies  irrespective of the notification*/
		{	                     
			mileaccess::ma::policies::policy_uri full_policies_uri;
			std::string software_id(product_id);
			full_policies_uri.set_software_id(software_id);
			mileaccess::ma::policies::policy_bag fpb(cli, full_policies_uri);
			for (mileaccess::ma::policies::policy_bag_iterator it( fpb ); it; ++it) {   
                     /* do something with the bag*/
			}
		}


		// Close file handle
		fprintf(fp, "-----------------------------Policy enforcment End----------------------------------------- \n");
		fclose(fp);
    }
    catch(mileaccess::ma::utils::exception(err)) {
        bc_event_generate(this->m_cli, "BC_POLICY_BAG_GET_IT_FAILED", POLICY_CLIENT_EVENT_ID+6, MA_EVENT_SEVERITY_MAJOR);
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Failed to get policies for beacon ,%d", err.error); 
        return err.error;
    }
    
    return MA_OK;
}


}}
