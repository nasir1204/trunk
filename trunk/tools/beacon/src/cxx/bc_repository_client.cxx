#include "bc/bc_repository_client.hxx"
#include "ma/repository/repository.hxx"
#include "ma/repository/proxy.hxx"
#include "bc/bc_strdef.hxx"
extern ::ma_logger_t *beacon_logger;

namespace mileaccess  { namespace bc {
	bc_repository_client::bc_repository_client(mileaccess::ma::client const &cli):mileaccess::ma::RepositoryClient(cli){
		/*This API's are used on demand(unlike property collection, event generation) to set/get agent repo/proxy. 
		Unintetionally calling this API's(specific to set) will corrupt agent DB, so commenting this code.
		To debug/test this API's,  uncommnet the following.	As well as add remaining api's as required */
		/*
		try {
			MA_LOG(beacon_logger, MA_LOG_SEV_TRACE, "Calling sync get repo list");
			mileaccess::ma::RepositoryList *repo_list = this->get_repositories();
			int repo_count = (int)repo_list->get_repo_count();
			MA_LOG(beacon_logger, MA_LOG_SEV_TRACE, "Total number of repositories <%d>", repo_count);
			for(int i=0; i< repo_count; i++){
				mileaccess::ma::Repository repo = repo_list->get_repository_by_index(i);
				std::string name = repo.get_name();
				MA_LOG(beacon_logger, MA_LOG_SEV_TRACE, "Repository %d: name : %s", i+1, name.c_str());
				ma_repository_type_t repository_type = repo.get_type();
				MA_LOG(beacon_logger, MA_LOG_SEV_TRACE, "Repository %d: type: %d", i+1, (int)repository_type);
				ma_repository_url_type_t url_type = repo.get_url_type();
				MA_LOG(beacon_logger, MA_LOG_SEV_TRACE, "Repository %d: url type: %d", i+1, (int)url_type);
				ma_repository_namespace_t repo_namespace =	repo.get_namespace();
				MA_LOG(beacon_logger, MA_LOG_SEV_TRACE, "Repository %d: namespace: %d", i+1, (int)repo_namespace);
				ma_repository_auth_t aut_type = repo.get_authentication();
				MA_LOG(beacon_logger, MA_LOG_SEV_TRACE, "Repository %d: auth type: %d", i+1, (int)aut_type);
				std::string server_fqdn = repo.get_server_fqdn();
				MA_LOG(beacon_logger, MA_LOG_SEV_TRACE, "Repository %d: fqdn: %s", i+1, server_fqdn.c_str());
				std::string server_name = repo.get_server_name();
				MA_LOG(beacon_logger, MA_LOG_SEV_TRACE, "Repository %d: name: %s", i+1, server_name.c_str());
				std::string ip = repo.get_server_ip();
				MA_LOG(beacon_logger, MA_LOG_SEV_TRACE, "Repository %d: ip: %s", i+1, ip.c_str());
				std::string server_path = repo.get_server_path();
				MA_LOG(beacon_logger, MA_LOG_SEV_TRACE, "Repository %d: path: %s", i+1, server_path.c_str());
				ma_uint32_t port = repo.get_port();
				MA_LOG(beacon_logger, MA_LOG_SEV_TRACE, "Repository %d: port: %d", i+1, port);
				ma_uint32_t ssl_port = repo.get_ssl_port();
				MA_LOG(beacon_logger, MA_LOG_SEV_TRACE, "Repository %d: ssl port: %d", i+1, ssl_port);
				ma_bool_t is_enable = repo.get_enabled();
				MA_LOG(beacon_logger, MA_LOG_SEV_TRACE, "Repository %d: is enable: %d", i+1, (int)is_enable);
				std::string username = repo.get_user_name();
				MA_LOG(beacon_logger, MA_LOG_SEV_TRACE, "Repository %d: user name: %s", i+1, username.c_str());
				std::string passwd = repo.get_password();
				MA_LOG(beacon_logger, MA_LOG_SEV_TRACE, "Repository %d: password: %s", i+1, passwd.c_str());
				std::string domain = repo.get_domain_name();
				MA_LOG(beacon_logger, MA_LOG_SEV_TRACE, "Repository %d: domain: %s", i+1, domain.c_str());
				ma_uint32_t pingtime = repo.get_pingtime();
				MA_LOG(beacon_logger, MA_LOG_SEV_TRACE, "Repository %d: ping time: %d", i+1, pingtime);
				ma_uint32_t subnetdistance = repo.get_subnetdistance();
				MA_LOG(beacon_logger, MA_LOG_SEV_TRACE, "Repository %d: subnet distance: %d", i+1, subnetdistance);
				ma_uint32_t siteorder = repo.get_siteorder();
				MA_LOG(beacon_logger, MA_LOG_SEV_TRACE, "Repository %d: site order: %d", i+1, siteorder);
				ma_repository_state_t state = repo.get_state();
				MA_LOG(beacon_logger, MA_LOG_SEV_TRACE, "Repository %d: state: %d", i+1, (int)state);
				ma_bool_t is_valid = repo.validate();
				MA_LOG(beacon_logger, MA_LOG_SEV_TRACE, "Repository %d: is valid: %d", i+1, (int)is_valid);
				//toggeling the enable state, to add it back in agent
				MA_LOG(beacon_logger, MA_LOG_SEV_TRACE, "Toggeling set from <%d> to <%d> ", is_enable, !is_enable);
				is_enable = !is_enable;
				repo.set_enabled(is_enable);
			}

			MA_LOG(beacon_logger, MA_LOG_SEV_TRACE, "calling set repository with toggeled state");
			this->set_repositories(*repo_list);

			MA_LOG(beacon_logger, MA_LOG_SEV_TRACE, "Calling sync get system proxy");
			std::string url("www.mileaccess.com");
			mileaccess::ma::ProxyList proxy_list(this->get_system_proxy(url));	
			size_t proxy_count = proxy_list.get_proxy_list_size();
			MA_LOG(beacon_logger, MA_LOG_SEV_TRACE, "size of proxy list <%d>", proxy_count);
			for(int i=0; i< (int)proxy_count; i++){
				mileaccess::ma::Proxy *proxy = proxy_list.get_proxy(i);
				std::string addr = proxy->get_address();
				MA_LOG(beacon_logger, MA_LOG_SEV_TRACE, "Proxy %d: %s", i+1, addr.c_str());
			}

			MA_LOG(beacon_logger, MA_LOG_SEV_TRACE, "Calling sync get proxy config");
			mileaccess::ma::ProxyConfig proxy_config(this->get_proxy_config());
			MA_LOG(beacon_logger, MA_LOG_SEV_TRACE, "Proxy usage type <%d>", (int)proxy_config.get_proxy_usage_type());
			delete repo_list;
		}
		catch(mileaccess::ma::utils::exception(err)) {
			MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "bc_repository_client c'tor exception with err<%d>", (int)(err.err()));
		}
		*/
		
	}
}}
