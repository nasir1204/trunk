#include "bc/bc_custom_event.hxx"
#include "bc/bc_strdef.hxx"
#include "ma/events/custom_event.hxx"

extern ::ma_logger_t *beacon_logger;
/*********************Sample Custom Event Will be look like this (ONLY EVENTID & SEVERITY WILL BE INPUT)*******************************
<VirusDetectionEvent>
	<MachineInfo>
		<MachineName>SHANU-PC</MachineName>
		<AgentGUID>{e61796e4-96cb-4961-86e5-f5e1775b7dec}</AgentGUID>
		<IPAddress>10.213.254.15</IPAddress>
		<OSName>Windows 7</OSName>
		<UserName>EPO2003\admin</UserName>
		<TimeZoneBias>-330</TimeZoneBias>
		<RawMACAddress>d4bed9457b8d</RawMACAddress>
	</MachineInfo>
	<ScannerSoftware ProductName="VirusScan Enterprise" ProductVersion="8.8" ProductFamily="TVD">
		<EngineVersion>5600.1067</EngineVersion>
		<DATVersion>7330.0000</DATVersion>
		<ScannerType>OAS</ScannerType>
		<TaskName>OAS</TaskName>
		<ProductFamily>TVD</ProductFamily>
		<ProductName>VirusScan Enterprise</ProductName>
		<ProductVersion>8.8</ProductVersion>
		<DetectionInfo>
			<EventID>1027</EventID>
			<Severity>3</Severity>
			<GMTTime>2014-01-27T15:20:56</GMTTime>
			<UTCTime>2014-01-27T09:50:56</UTCTime>
			<FileName>C:\test\01jkkjff</FileName>
			<VirusName>Elspy.worm</VirusName>
			<Source>_</Source>
			<VirusType>0</VirusType>
			<szVirusType>virus</szVirusType>
		</DetectionInfo>
	</ScannerSoftware>
</VirusDetectionEvent>
*******************************************************************************************************************************/


namespace mileaccess  { namespace bc {

void bc_custom_event_generate(mileaccess::ma::client const &cli, ma_uint32_t event_id, ma_event_severity_t severity) {
    try {
        MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Create custom event .");
		ma_xml_node_t *root_node = NULL;
        mileaccess::ma::events::custom_event custom_event(cli, "VirusDetectionEvent", &root_node);
        
		ma_xml_node_t *ScannerSoftware_node = NULL;
		custom_event.custom_event_node_create(root_node, "ScannerSoftware", &ScannerSoftware_node);

		custom_event.custom_event_node_attribute_set(ScannerSoftware_node, "ProductName", "VirusScan Enterprise");
		custom_event.custom_event_node_attribute_set(ScannerSoftware_node, "ProductVersion", "8.8");
		custom_event.custom_event_node_attribute_set(ScannerSoftware_node, "ProductFamily", "TVD");

		ma_xml_node_t *ProductName_node = NULL;
		custom_event.custom_event_node_create(ScannerSoftware_node, "ProductName",&ProductName_node);
		custom_event.custom_event_node_set_data(ProductName_node, "VirusScan Enterprise");

		ma_xml_node_t *ProductVersion_node = NULL;
		custom_event.custom_event_node_create(ScannerSoftware_node, "ProductVersion",&ProductVersion_node);
		custom_event.custom_event_node_set_data(ProductVersion_node, "8.8");

		ma_xml_node_t *ProductFamily_node = NULL;
		custom_event.custom_event_node_create(ScannerSoftware_node, "ProductFamily",&ProductFamily_node);
		custom_event.custom_event_node_set_data(ProductFamily_node, "TVD");

		ma_xml_node_t *EngineVersion_node = NULL;
		custom_event.custom_event_node_create(ScannerSoftware_node, "EngineVersion",&EngineVersion_node);
		custom_event.custom_event_node_set_data(EngineVersion_node, "5600.1067");

		ma_xml_node_t *DATVersion_node = NULL;
		custom_event.custom_event_node_create(ScannerSoftware_node, "DATVersion",&DATVersion_node);
		custom_event.custom_event_node_set_data(DATVersion_node, "7330.0000");

		ma_xml_node_t *ScannerType_node = NULL;
		custom_event.custom_event_node_create(ScannerSoftware_node, "ScannerType",&ScannerType_node);
		custom_event.custom_event_node_set_data(ScannerType_node, "OAS");

		ma_xml_node_t *TaskName_node = NULL;
		custom_event.custom_event_node_create(ScannerSoftware_node, "TaskName",&TaskName_node);
		custom_event.custom_event_node_set_data(TaskName_node, "OAS");

		ma_xml_node_t *DetectionInfo = NULL;
		custom_event.custom_event_create_event_node(ScannerSoftware_node, "DetectionInfo", event_id, severity, &DetectionInfo);

		ma_xml_node_t *VirusName = NULL;
		custom_event.custom_event_node_create(DetectionInfo, "VirusName",&VirusName);
		custom_event.custom_event_node_set_data(VirusName, "Elspy.worm");

		ma_xml_node_t *Source = NULL;
		custom_event.custom_event_node_create(DetectionInfo, "Source",&Source);
		custom_event.custom_event_node_set_data(Source, "_");

		ma_xml_node_t *VirusType = NULL;
		custom_event.custom_event_node_create(DetectionInfo, "VirusType",&VirusType);
		custom_event.custom_event_node_set_data(VirusType, "0");

		ma_xml_node_t *szVirusType = NULL;
		custom_event.custom_event_node_create(DetectionInfo, "VirusName",&szVirusType);
		custom_event.custom_event_node_set_data(szVirusType, "virus");

		custom_event.custom_event_send();
    }
    catch(mileaccess::ma::utils::exception(err)) {
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Custom Event [%u] sent failed. event api error = %d.", event_id, err.error);
    }
    MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Custom Event [%u] sent to agent.", event_id);
}

}}
