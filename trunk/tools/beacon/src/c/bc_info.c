#include "bc/bc_info.h"
#include "bc/bc_strdef.h"

#include "ma/info/ma_info.h"

#include <string.h>

extern ma_logger_t *beacon_logger;
static ma_error_t bc_info_events_cb(ma_client_t *ma_client, const char *product_id, const char *info_event, void *cb_data) ;

bc_error_t bc_info_intialize(ma_client_t *ma_client) {
    ma_error_t rc = MA_OK ;
   
    /* Registering for MA info events,
        - product id can be passed as NULL if we are trying to register for the same product id as in context.
        - can be called multiple times with different product id's if we want to register for multiple product's
    */
    if(MA_OK != (rc = ma_info_events_register_callback(ma_client, BC_PRODUCT_ID_STR, bc_info_events_cb, NULL))) {
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Failed to register beacon for info events with MA ,%d", rc) ; 
        return BC_INFO_EVENTS_REGISTRATION_FAILED;
    }
		
    MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Registered successfully beacon for info events with MA") ;

    return BC_OK ;
}

bc_error_t bc_info_deinitalize(ma_client_t *ma_client) {
    ma_error_t rc = MA_OK ;
    
    /* UnRegistering for MA info events,
        - product id can be passed as NULL if we are trying to unregister for the same product id as in context.
        - can be called multiple times with product id's if we have registered with different product id's
    */
    if(MA_OK != (rc = ma_info_events_unregister_callback(ma_client, BC_PRODUCT_ID_STR))){
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Failed to unregister beacon for info events with MA ,%d", rc) ; 
		return BC_INFO_EVENTS_UNREGISTRATION_FAILED ;
    }

    MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Unregistered succesfully beacon for info events with MA") ;

    return BC_OK;
}

static ma_error_t bc_info_events_cb(ma_client_t *ma_client, const char *product_id, const char *info_event, void *cb_data){
	ma_variant_t *var_value = NULL ;
	ma_buffer_t *buffer = NULL ;
	const char *key_value = NULL ;
	size_t size = 0 ;

    MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Beacon info events cb invoked for product id %s with info event %s", product_id, info_event) ;

	if(MA_OK == ma_info_get(ma_client, MA_INFO_AGENT_MODE_STR, &var_value)) {
		ma_variant_get_string_buffer(var_value, &buffer) ;
		ma_buffer_get_string(buffer, &key_value, &size) ;
		MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "%s = %s", MA_INFO_AGENT_MODE_STR, key_value) ;
		ma_buffer_release(buffer) ;	buffer = NULL ;
		ma_variant_release(var_value) ;	var_value = NULL ;
	}
	if(MA_OK == ma_info_get(ma_client, MA_INFO_VERSION_STR, &var_value)) {
		ma_variant_get_string_buffer(var_value, &buffer) ;
		ma_buffer_get_string(buffer, &key_value, &size) ;
		MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "%s = %s", MA_INFO_VERSION_STR, key_value) ;
		ma_buffer_release(buffer) ;	buffer = NULL ;
		ma_variant_release(var_value) ;	var_value = NULL ;
	}
	if(MA_OK == ma_info_get(ma_client, MA_INFO_GUID_STR, &var_value)) {
		ma_variant_get_string_buffer(var_value, &buffer) ;
		ma_buffer_get_string(buffer, &key_value, &size) ;
		MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "%s = %s", MA_INFO_GUID_STR, key_value) ;
		ma_buffer_release(buffer) ;	buffer = NULL ;
		ma_variant_release(var_value) ;	var_value = NULL ;
	}
	if(MA_OK == ma_info_get(ma_client, MA_INFO_LOCALE_STR, &var_value)) {
		ma_variant_get_string_buffer(var_value, &buffer) ;
		ma_buffer_get_string(buffer, &key_value, &size) ;
		MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "%s = %s", MA_INFO_LOCALE_STR, key_value) ;
		ma_buffer_release(buffer) ;	buffer = NULL ;
		ma_variant_release(var_value) ;	var_value = NULL ;
	}
	if(MA_OK == ma_info_get(ma_client, MA_INFO_LOG_LOCATION_STR, &var_value)) {
		ma_variant_get_string_buffer(var_value, &buffer) ;
		ma_buffer_get_string(buffer, &key_value, &size) ;
		MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "%s = %s", MA_INFO_LOG_LOCATION_STR, key_value) ;
		ma_buffer_release(buffer) ;	buffer = NULL ;
		ma_variant_release(var_value) ;	var_value = NULL ;
	}
	if(MA_OK == ma_info_get(ma_client, MA_INFO_INSTALL_LOCATION_STR, &var_value)) {
		ma_variant_get_string_buffer(var_value, &buffer) ;
		ma_buffer_get_string(buffer, &key_value, &size) ;
		MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "%s = %s", MA_INFO_INSTALL_LOCATION_STR, key_value) ;
		ma_buffer_release(buffer) ;	buffer = NULL ;
		ma_variant_release(var_value) ;	var_value = NULL ;
	}
	if(MA_OK == ma_info_get(ma_client, MA_INFO_CRYPTO_MODE_STR, &var_value)) {
		ma_variant_get_string_buffer(var_value, &buffer) ;
		ma_buffer_get_string(buffer, &key_value, &size) ;
		MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "%s = %s", MA_INFO_CRYPTO_MODE_STR, key_value) ;
		ma_buffer_release(buffer) ;	buffer = NULL ;
		ma_variant_release(var_value) ;	var_value = NULL ;
	}
	if(MA_OK == ma_info_get(ma_client, MA_INFO_DATA_LOCATION_STR, &var_value)) {
		ma_variant_get_string_buffer(var_value, &buffer) ;
		ma_buffer_get_string(buffer, &key_value, &size) ;
		MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "%s = %s", MA_INFO_DATA_LOCATION_STR, key_value) ;
		ma_buffer_release(buffer) ;	buffer = NULL ;
		ma_variant_release(var_value) ;	var_value = NULL ;
	}
	if(MA_OK == ma_info_get(ma_client, MA_INFO_EPO_SERVER_LIST, &var_value)) {
		ma_variant_get_string_buffer(var_value, &buffer) ;
		ma_buffer_get_string(buffer, &key_value, &size) ;
		MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "%s = %s", MA_INFO_EPO_SERVER_LIST, key_value) ;
		ma_buffer_release(buffer) ;	buffer = NULL ;
		ma_variant_release(var_value) ;	var_value = NULL ;
	}
	if(MA_OK == ma_info_get(ma_client, MA_INFO_EPO_PORT_LIST, &var_value)) {
		ma_variant_get_string_buffer(var_value, &buffer) ;
		ma_buffer_get_string(buffer, &key_value, &size) ;
		MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "%s = %s", MA_INFO_EPO_PORT_LIST, key_value) ;
		ma_buffer_release(buffer) ;	buffer = NULL ;
		ma_variant_release(var_value) ;	var_value = NULL ;
	}
	if(MA_OK == ma_info_get(ma_client, MA_INFO_EPO_SERVER_LAST_USED_STR, &var_value)) {
		ma_variant_get_string_buffer(var_value, &buffer) ;
		ma_buffer_get_string(buffer, &key_value, &size) ;
		MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "%s = %s", MA_INFO_EPO_SERVER_LAST_USED_STR, key_value) ;
		ma_buffer_release(buffer) ;	buffer = NULL ;
		ma_variant_release(var_value) ;	var_value = NULL ;
	}
	if(MA_OK == ma_info_get(ma_client, MA_INFO_EPO_PORT_LAST_USED_STR, &var_value)) {
		ma_variant_get_string_buffer(var_value, &buffer) ;
		ma_buffer_get_string(buffer, &key_value, &size) ;
		MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "%s = %s", MA_INFO_EPO_PORT_LAST_USED_STR, key_value) ;
		ma_buffer_release(buffer) ;	buffer = NULL ;
		ma_variant_release(var_value) ;	var_value = NULL ;
	}
	if(MA_OK == ma_info_get(ma_client, MA_INFO_LAST_ASC_TIME, &var_value)) {
		ma_variant_get_string_buffer(var_value, &buffer) ;
		ma_buffer_get_string(buffer, &key_value, &size) ;
		MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "%s = %s", MA_INFO_LAST_ASC_TIME, key_value) ;
		ma_buffer_release(buffer) ;	buffer = NULL ;
		ma_variant_release(var_value) ;	var_value = NULL ;
	}
	if(MA_OK == ma_info_get(ma_client, MA_INFO_LAST_POLICY_UPDATE_TIME, &var_value)) {
		ma_variant_get_string_buffer(var_value, &buffer) ;
		ma_buffer_get_string(buffer, &key_value, &size) ;
		MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "%s = %s", MA_INFO_LAST_POLICY_UPDATE_TIME, key_value) ;
		ma_buffer_release(buffer) ;	buffer = NULL ;
		ma_variant_release(var_value) ;	var_value = NULL ;
	}
	if(MA_OK == ma_info_get(ma_client, MA_INFO_TENANTID_STR, &var_value)) {
		ma_variant_get_string_buffer(var_value, &buffer) ;
		ma_buffer_get_string(buffer, &key_value, &size) ;
		MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "%s = %s", MA_INFO_TENANTID_STR, key_value) ;
		ma_buffer_release(buffer) ;	buffer = NULL ;
		ma_variant_release(var_value) ;	var_value = NULL ;
	}
	if(MA_OK == ma_info_get(ma_client, MA_INFO_ASC_INTERVAL_STR, &var_value)) {
		ma_variant_get_string_buffer(var_value, &buffer) ;
		ma_buffer_get_string(buffer, &key_value, &size) ;
		MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "%s = %s", MA_INFO_ASC_INTERVAL_STR, key_value) ;
		ma_buffer_release(buffer) ;	buffer = NULL ;
		ma_variant_release(var_value) ;	var_value = NULL ;
	}
	if(MA_OK == ma_info_get(ma_client, MA_INFO_POLICY_ENFORCE_INTERVAL_STR, &var_value)) {
		ma_variant_get_string_buffer(var_value, &buffer) ;
		ma_buffer_get_string(buffer, &key_value, &size) ;
		MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "%s = %s", MA_INFO_POLICY_ENFORCE_INTERVAL_STR, key_value) ;
		ma_buffer_release(buffer) ;	buffer = NULL ;
		ma_variant_release(var_value) ;	var_value = NULL ;
	}
	if(MA_OK == ma_info_get(ma_client, MA_INFO_EVENTS_UPLOAD_INTERVAL_STR, &var_value)) {
		ma_variant_get_string_buffer(var_value, &buffer) ;
		ma_buffer_get_string(buffer, &key_value, &size) ;
		MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "%s = %s", MA_INFO_EVENTS_UPLOAD_INTERVAL_STR, key_value) ;
		ma_buffer_release(buffer) ;	buffer = NULL ;
		ma_variant_release(var_value) ;	var_value = NULL ;
	}
	if(MA_OK == ma_info_get(ma_client, MA_INFO_IPADDRESS_STR, &var_value)) {
		ma_variant_get_string_buffer(var_value, &buffer) ;
		ma_buffer_get_string(buffer, &key_value, &size) ;
		MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "%s = %s", MA_INFO_IPADDRESS_STR, key_value) ;
		ma_buffer_release(buffer) ;	buffer = NULL ;
		ma_variant_release(var_value) ;	var_value = NULL ;
	}
	if(MA_OK == ma_info_get(ma_client, MA_INFO_HOSTNAME_STR, &var_value)) {
		ma_variant_get_string_buffer(var_value, &buffer) ;
		ma_buffer_get_string(buffer, &key_value, &size) ;
		MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "%s = %s", MA_INFO_HOSTNAME_STR, key_value) ;
		ma_buffer_release(buffer) ;	buffer = NULL ;
		ma_variant_release(var_value) ;	var_value = NULL ;
	}

    return MA_OK;
}

