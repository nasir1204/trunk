#include "bc/bc_task.h"
#include "bc/bc_strdef.h"

#include "ma/scheduler/ma_scheduler.h"
#include "bc/bc_event.h"

#include <string.h>
#include <stdlib.h>

#define TASK_CLIENT_EVENT_ID 12000

extern ma_logger_t *beacon_logger;

static ma_error_t bc_task_execute_cb(ma_client_t *ma_client, const char *product_id, const char *task_type, ma_task_t *task, void *cb_data);
static ma_error_t bc_task_stop_cb(ma_client_t *ma_client, const char *product_id, const char *task_type, ma_task_t *task, void *cb_data);
static ma_error_t bc_task_remove_cb(ma_client_t *ma_client, const char *product_id, const char *task_type, ma_task_t *task, void *cb_data);
static ma_error_t bc_task_update_cb(ma_client_t *ma_client, const char *product_id, const char *task_type, ma_task_t *task, void *cb_data);

bc_error_t bc_task_intialize(ma_client_t *ma_client) {
    ma_error_t rc = MA_OK;
     ma_scheduler_task_callbacks_t task_callbacks = {0};

    task_callbacks.on_execute = bc_task_execute_cb; task_callbacks.on_stop = bc_task_stop_cb; 
    task_callbacks.on_remove = bc_task_remove_cb; task_callbacks.on_update_status = bc_task_update_cb;


    /* Registering for tasks(it can come from server on any local shceduled tasks)
        - task_type can  be passed as NULL if we are trying to register generic callback for all task types
        - can be called multiple times with different task types and different call backs
    */
    if(MA_OK != (rc = ma_scheduler_register_task_callbacks(ma_client, NULL, NULL, &task_callbacks, NULL))) {
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Failed to register beacon  for task notifications with MA ,%d", rc);
		bc_event_generate(ma_client, "BC_TASK_CALLBACKS_REGISTRATION_FAILED", TASK_CLIENT_EVENT_ID+1, MA_EVENT_SEVERITY_CRITICAL);
        return BC_TASK_CALLBACKS_REGISTRATION_FAILED;
    }
	else
		bc_event_generate(ma_client, "BC_TASK_CALLBACKS_REGISTRATION_SUCCESS", TASK_CLIENT_EVENT_ID+2, MA_EVENT_SEVERITY_INFORMATIONAL);

    MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Registered succesfully beacon for  for task notifications with MA");

    return BC_OK;
}

bc_error_t bc_task_deinitalize(ma_client_t *ma_client) {
    ma_error_t rc = MA_OK;
    /* UnRegistering for task callbacks notification
        - task_type can  be passed as NULL if we are trying to un-register generic callback for all task types
        - can be called multiple times with product id's if we have registered with different task types
    */
    if(MA_OK != (rc = ma_scheduler_unregister_task_callbacks(ma_client, NULL, NULL))){
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Failed to un-register beacon for task notification with MA ,%d", rc); 
		bc_event_generate(ma_client, "BC_TASK_CALLBACKS_UNREGISTRATION_FAILED", TASK_CLIENT_EVENT_ID+3, MA_EVENT_SEVERITY_CRITICAL);
        return BC_TASK_CALLBACKS_UNREGISTRATION_FAILED;
    }
	else
		bc_event_generate(ma_client, "BC_TASK_CALLBACKS_UNREGISTRATION_SUCCESS", TASK_CLIENT_EVENT_ID+4, MA_EVENT_SEVERITY_INFORMATIONAL);

    MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Unregistered succesfully beacon for task notification with MA");
    return BC_OK;
}

static void schedule_event_task(ma_client_t *ma_cleint, ma_task_t *task);
static void schedule_long_running_task(ma_client_t *ma_cleint, ma_task_t *task);
static ma_error_t bc_task_execute_cb(ma_client_t *ma_client, const char *product_id, const char *task_type, ma_task_t *task, void *cb_data) {
     /*TODO - get rid of this as soon as task get API' are fixed for constness*/
     ma_task_t *tsk = (ma_task_t *)task;
     const char *creator_id = NULL;
     const char *task_name = NULL, *task_id = NULL;
	     
    ma_task_get_creator_id(tsk, &creator_id);
    ma_task_get_id(tsk, &task_id);
    ma_task_get_name(tsk, &task_name);
    MA_LOG(beacon_logger, MA_LOG_SEV_INFO, "Beacon task execute callback invoked for task type =%s, creator id=%s, id= %s, name = %s",task_type, creator_id, task_id, task_name);

    if(!strcmp("Event", task_type)) {
        MA_LOG(beacon_logger, MA_LOG_SEV_INFO, "Beacon \"Event\" task has been submitted");
        schedule_event_task(ma_client, tsk);
        MA_LOG(beacon_logger, MA_LOG_SEV_INFO, "Beacon \"Event\" task succedded");
        bc_event_generate(ma_client, "BC_TASK_EXECUTE_DONE", TASK_CLIENT_EVENT_ID+5, MA_EVENT_SEVERITY_INFORMATIONAL);
		if(MA_OK != ma_scheduler_update_task_status(ma_client, task_id, MA_TASK_EXEC_SUCCESS)) 
			MA_LOG(beacon_logger, MA_LOG_SEV_INFO, "Beacon task id(%s) status updation failed", task_id);
		
    }
    else if(!strcmp("LongRunning", task_type)) {
        MA_LOG(beacon_logger, MA_LOG_SEV_INFO, "Beacon \"LongRunning\" task has been submitted");
        schedule_long_running_task(ma_client, tsk);
        bc_event_generate(ma_client, "BC_TASK_EXECUTE_DONE", TASK_CLIENT_EVENT_ID+7, MA_EVENT_SEVERITY_INFORMATIONAL);
    }
    else {
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Beacon received unknown task");
        bc_event_generate(ma_client, "BC_TASK_EXECUTE_DONE", TASK_CLIENT_EVENT_ID+8, MA_EVENT_SEVERITY_CRITICAL);
		if(MA_OK != ma_scheduler_update_task_status(ma_client, task_id, MA_TASK_EXEC_SUCCESS)) 
			MA_LOG(beacon_logger, MA_LOG_SEV_INFO, "Beacon task id(%s) status updation failed", task_id);
    }
    return MA_OK;
}

static ma_error_t bc_task_stop_cb(ma_client_t *ma_client, const char *product_id, const char *task_type, ma_task_t *task, void *cb_data) {
     const char *creator_id = NULL;
    const char *task_name = NULL;
    const char *task_id = NULL;
    ma_task_t *tsk = (ma_task_t *)task;

    ma_task_get_creator_id(tsk, &creator_id);
    ma_task_get_id(tsk, &task_id);
    ma_task_get_name(tsk, &task_name);
    MA_LOG(beacon_logger, MA_LOG_SEV_INFO, "Beacon task stop callback invoked for task type =%s, creator id=%s, id= %s, name = %s",task_type, creator_id, task_id, task_name);

    bc_event_generate(ma_client, "BC_TASK_STOP_NOTIFICATION", TASK_CLIENT_EVENT_ID+10, MA_EVENT_SEVERITY_INFORMATIONAL);

    return MA_OK;
}

static ma_error_t bc_task_remove_cb(ma_client_t *ma_client, const char *product_id, const char *task_type, ma_task_t *task, void *cb_data) {
    const char *creator_id = NULL;
    const char *task_name = NULL;
    const char *task_id = NULL;
    ma_task_t *tsk = (ma_task_t *)task;

    ma_task_get_creator_id(tsk, &creator_id);
    ma_task_get_id(tsk, &task_id);
    ma_task_get_name(tsk, &task_name);
    MA_LOG(beacon_logger, MA_LOG_SEV_INFO, "Beacon task remove callback invoked for task type =%s, creator id=%s, id= %s, name = %s",task_type, creator_id, task_id, task_name);

    bc_event_generate(ma_client, "BC_TASK_REMOVE_NOTIFICATION", TASK_CLIENT_EVENT_ID+11, MA_EVENT_SEVERITY_INFORMATIONAL);
    return MA_OK;
}


static ma_error_t bc_task_update_cb(ma_client_t *ma_client, const char *product_id, const char *task_type, ma_task_t *task, void *cb_data) {
    const char *creator_id = NULL;
    const char *task_name = NULL;
    const char *task_id = NULL;
    ma_task_t *tsk = (ma_task_t *)task;

    ma_task_get_creator_id(tsk, &creator_id);
    ma_task_get_id(tsk, &task_id);
    ma_task_get_name(tsk, &task_name);
    MA_LOG(beacon_logger, MA_LOG_SEV_INFO, "Beacon task update callback invoked for task type =%s, creator id=%s, id= %s, name = %s",task_type, creator_id, task_id, task_name);
    if(!strcmp("Event", task_type)) {
        MA_LOG(beacon_logger, MA_LOG_SEV_INFO, "Beacon \"Event\" task has been submitted");
        if(MA_OK != ma_scheduler_update_task_status(ma_client, task_id, MA_TASK_EXEC_SUCCESS)) {
        MA_LOG(beacon_logger, MA_LOG_SEV_INFO, "Beacon task id(%s) status updation failed", task_id);
    }
        
    }
    else if(!strcmp("LongRunning", task_type)) {
        MA_LOG(beacon_logger, MA_LOG_SEV_INFO, "Beacon \"LongRunning\" task has been submitting updating status");
        
    }
    else {
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Beacon received unknown task");
    }
	bc_event_generate(ma_client, "BC_TASK_UPDATE_NOTIFICATION", TASK_CLIENT_EVENT_ID+12, MA_EVENT_SEVERITY_INFORMATIONAL);
    return MA_OK;
}

static void schedule_event_task(ma_client_t *ma_client, ma_task_t *task) {
    ma_variant_t *variant = NULL;
    ma_buffer_t *buffer = NULL;
    const char *value = NULL;
    size_t size = 0;
    int num_of_events = 0, i =0;
    ma_event_severity_t severity = MA_EVENT_SEVERITY_INFORMATIONAL;

    /* get severity */
    ma_task_get_setting(task,"BSPP_Task", "Severity", &variant);
    ma_variant_get_string_buffer(variant, &buffer);
    ma_buffer_get_string(buffer, &value, &size);
    severity = (ma_event_severity_t)atoi(value);
    ma_buffer_release(buffer);
    ma_variant_release(variant) ; //???it should be constant

    /* Get Num events */
    ma_task_get_setting(task,"BSPP_Task", "NumEvents", &variant);
    ma_variant_get_string_buffer(variant, &buffer);
    ma_buffer_get_string(buffer, &value, &size);
    num_of_events = atoi(value);
    ma_buffer_release(buffer);
    ma_variant_release(variant) ; //???it should be constant

    for(i = 0 ;  i < num_of_events; i++)
        bc_event_generate(ma_client, "BC_TASK_EVENT_GENERATION", TASK_CLIENT_EVENT_ID+ 12 + i, severity);

}

static void schedule_long_running_task(ma_client_t *ma_client, ma_task_t *task) {
    ma_variant_t *variant = NULL;
    ma_buffer_t *buffer = NULL;
    size_t size = 0;
    const char *value = NULL;
    
    ma_task_get_setting(task,"BSPP_Task", "Duration", &variant);
    ma_variant_get_string_buffer(variant, &buffer);
    ma_buffer_get_string(buffer, &value, &size);
    size = atoi(value);
    ma_buffer_release(buffer);
    ma_variant_release(variant) ; //???it should be constant

}



