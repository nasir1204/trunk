#include "bc/bc_custom_event.h"
#include "bc/bc_strdef.h"
#include "ma/ma_log.h"
extern ma_logger_t *beacon_logger;
/*********************Sample Custom Event Will be look like this (ONLY EVENTID & SEVERITY WILL BE INPUT)*******************************
<VirusDetectionEvent>
	<MachineInfo>
		<MachineName>SHANU-PC</MachineName>
		<AgentGUID>{e61796e4-96cb-4961-86e5-f5e1775b7dec}</AgentGUID>
		<IPAddress>10.213.254.15</IPAddress>
		<OSName>Windows 7</OSName>
		<UserName>EPO2003\admin</UserName>
		<TimeZoneBias>-330</TimeZoneBias>
		<RawMACAddress>d4bed9457b8d</RawMACAddress>
	</MachineInfo>
	<ScannerSoftware ProductName="VirusScan Enterprise" ProductVersion="8.8" ProductFamily="TVD">
		<EngineVersion>5600.1067</EngineVersion>
		<DATVersion>7330.0000</DATVersion>
		<ScannerType>OAS</ScannerType>
		<TaskName>OAS</TaskName>
		<ProductFamily>TVD</ProductFamily>
		<ProductName>VirusScan Enterprise</ProductName>
		<ProductVersion>8.8</ProductVersion>
		<DetectionInfo>
			<EventID>1027</EventID>
			<Severity>3</Severity>
			<GMTTime>2014-01-27T15:20:56</GMTTime>
			<UTCTime>2014-01-27T09:50:56</UTCTime>
			<FileName>C:\test\01jkkjff</FileName>
			<VirusName>Elspy.worm</VirusName>
			<Source>_</Source>
			<VirusType>0</VirusType>
			<szVirusType>virus</szVirusType>
		</DetectionInfo>
	</ScannerSoftware>
</VirusDetectionEvent>
*******************************************************************************************************************************/

static ma_error_t create_node(ma_xml_node_t *parent, const char *key, const char *val) {
	ma_xml_node_t *node = NULL;
	ma_error_t rc;
	if(MA_OK == (rc = ma_custom_event_node_create(parent, key, &node)))
		rc = ma_custom_event_node_set_data(node, val);
		
	return rc;
}

bc_error_t bc_custom_event_generate(ma_client_t *ma_client, ma_uint32_t event_id, ma_event_severity_t severity){
	ma_error_t rc = MA_OK;
	ma_custom_event_t *event = NULL;
	ma_xml_node_t *root_node = NULL;

	if(MA_OK == (rc = ma_custom_event_create(ma_client, "VirusDetectionEvent", &event, &root_node))) {
		ma_xml_node_t *ScannerSoftware_node = NULL;
		if(MA_OK == (rc = ma_custom_event_node_create(root_node, "ScannerSoftware", &ScannerSoftware_node))) {
			if(MA_OK == (rc = ma_custom_event_node_attribute_set(ScannerSoftware_node, "ProductName", "VirusScan Enterprise"))) {
				if(MA_OK == (rc = ma_custom_event_node_attribute_set(ScannerSoftware_node, "ProductVersion", "8.8"))) {
					if(MA_OK == (rc = ma_custom_event_node_attribute_set(ScannerSoftware_node, "ProductFamily", "TVD"))) {
						if(MA_OK == (rc = create_node(ScannerSoftware_node, "ProductName","VirusScan Enterprise"))) {
							if(MA_OK == (rc = create_node(ScannerSoftware_node, "ProductVersion","8.8"))) {
								if(MA_OK == (rc = create_node(ScannerSoftware_node, "ProductFamily","TVD"))) {
									if(MA_OK == (rc = create_node(ScannerSoftware_node, "EngineVersion","5600.1067"))) {
										if(MA_OK == (rc = create_node(ScannerSoftware_node, "DATVersion","7330.0000"))) {
											if(MA_OK == (rc = create_node(ScannerSoftware_node, "ScannerType","OAS"))) {
												if(MA_OK == (rc = create_node(ScannerSoftware_node, "TaskName","OAS"))) {
													ma_xml_node_t *DetectionInfo = NULL;
													if(MA_OK == (rc = ma_custom_event_create_event_node(event, ScannerSoftware_node, "DetectionInfo", event_id, severity, &DetectionInfo))) {
														if(MA_OK == (rc = create_node(DetectionInfo, "VirusName", "Elspy.worm"))) {
															if(MA_OK == (rc = create_node(DetectionInfo, "Source", "_"))) {
																if(MA_OK == (rc = create_node(DetectionInfo, "VirusType", "0"))) {
																	if(MA_OK == (rc = create_node(DetectionInfo, "szVirusType", "virus"))) {
																		rc = ma_custom_event_send(event);
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	ma_custom_event_release(event);

	if(MA_OK == rc) 
		MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "custom event [%u] sent to agent succeed.", event_id);
	else
		MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "custom event[%u] sent to agent failed, error [%d]", event_id, rc);

	return MA_OK == rc ? BC_OK : BC_EVENT_GENERATE_FIELD;
}

