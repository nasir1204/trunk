#include "bc/bc_updater_ui_handler.h"
#include "bc/bc_strdef.h"

#include "ma/updater/ma_updater.h"
#include "ma/updater/ma_updater_ui_provider.h"

#include <string.h>

extern ma_logger_t *beacon_logger;
/* updater ui provider callbacks */
static ma_error_t updater_ui_provider_show_ui_cb(ma_client_t *ma_client, void *cb_data,  const char *session_id, ma_updater_ui_type_t type, const ma_updater_show_ui_request_t *ui_request, ma_updater_show_ui_response_t *ui_response) ; 
static ma_error_t updater_ui_provider_progress_cb(ma_client_t *ma_client, void *cb_data, const char *session_id, ma_int32_t event_type, ma_int32_t progress_step , ma_int32_t max_progress_step, const char *progress_message) ;
static ma_error_t updater_ui_provider_event_cb(ma_client_t *ma_client, void *cb_data, const char *session_id, ma_updater_event_t const *event_info) ;
static ma_error_t updater_ui_provider_end_ui_cb(ma_client_t *ma_client, void *cb_data, const char *session_id, const char *title, const char *end_message, const char *count_down_message, ma_int64_t count_down_value);

static ma_updater_ui_provider_callbacks_t  updater_ui_provider_cbs;

bc_error_t bc_updater_ui_handler_intialize(ma_client_t *ma_client) {
    ma_error_t rc = MA_OK;
    
    updater_ui_provider_cbs.on_show_ui_cb = updater_ui_provider_show_ui_cb ;
    updater_ui_provider_cbs.on_progress_cb = updater_ui_provider_progress_cb ;
    updater_ui_provider_cbs.on_end_ui_cb = updater_ui_provider_end_ui_cb ;
	updater_ui_provider_cbs.on_update_event_cb = updater_ui_provider_event_cb ;


    if(MA_OK == (rc = ma_updater_register_ui_provider_callbacks(ma_client, BC_PRODUCT_ID_STR, MA_MSGBUS_CALLBACK_THREAD_POOL, &updater_ui_provider_cbs, NULL))) 
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Registered beacon updater ui provider callbacks with MA ") ; 
    else {
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "failed to Register beacon updater ui provider callbacks with MA ,%d", rc) ;
        return BC_UPDATER_UI_PROVIDER_CALLBACKS_REGISTRATION_FAILED ;
    }
    return BC_OK ;
}

bc_error_t bc_updater_ui_handler_deintialize(ma_client_t *ma_client) {
    (void)ma_updater_unregister_ui_provider_callbacks(ma_client, BC_PRODUCT_ID_STR) ;

    return BC_OK ;
}

/* updater ui provider callbacks */
static ma_error_t updater_ui_provider_show_ui_cb(ma_client_t *ma_client, void *cb_data,  const char *session_id, ma_updater_ui_type_t type, const ma_updater_show_ui_request_t *ui_request, ma_updater_show_ui_response_t *ui_response) {
     MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Beacon updater_ui_provider_show_ui_cb invoked with ui type (%d)", type) ;
    
    return MA_OK ;
}

static ma_error_t updater_ui_provider_progress_cb(ma_client_t *ma_client, void *cb_data, const char *session_id, ma_int32_t event_type, ma_int32_t progress_step , ma_int32_t max_progress_step, const char *progress_message) {
     MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Beacon updater_ui_provider_progress_cb invoked with progress_step(%d), max_progress_step(%d), progress_message(%s)", progress_step, max_progress_step, progress_message ? progress_message : "") ;

    return MA_OK ;
}

static ma_error_t updater_ui_provider_event_cb(ma_client_t *ma_client, void *cb_data, const char *session_id, ma_updater_event_t const *event_info) {
     MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Beacon updater_ui_provider_event_cb invoked" ) ;

    return MA_OK ;
}

static ma_error_t updater_ui_provider_end_ui_cb(ma_client_t *ma_client, void *cb_data, const char *session_id, const char *title, const char *end_message, const char *count_down_message, ma_int64_t count_down_value) {
     MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Beacon updater_ui_provider_end_ui_cb invoked with title(%s), end_message(%s)", title, end_message) ;

    return MA_OK ;
}
