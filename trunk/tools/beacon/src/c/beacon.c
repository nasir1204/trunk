#include "bc/beacon.h"
#include "bc/bc_strdef.h"

#include "bc/bc_property.h"
#include "bc/bc_policy.h"
#include "bc/bc_task.h"
#include "bc/bc_event.h"
#include "bc/bc_datachannel.h"
#include "bc/bc_updater.h"
#include "bc/bc_info.h"

#include "ma/ma_client.h"
#include "ma/ma_log.h"
#include "ma/logger/ma_file_logger.h"
#include "ma/info/ma_info.h"

#include <stdlib.h>

/* Hard coded this path for now, will read this from registry/config.xml */
#ifdef MA_WINDOWS
#define BC_LOG_FOLDER   "C:\\ProgramData\\McAfee\\Beacon\\logs"
#endif


struct beacon_s {
    ma_client_t					*ma_client;
	ma_logger_t					*logger;	
};
    
ma_logger_t  *beacon_logger = NULL;
static void bc_client_notification_cb(ma_client_t *client, ma_client_notification_t type, void *cb_data);
static void bc_client_logger_cb(ma_log_severity_t severity, const char *module_name, const char *log_message, void *user_data) ;
static void log_message_fun(ma_log_severity_t severity, const char *module_name, const char *log_message);

static ma_error_t beacon_passphrase_provider_callback(ma_msgbus_t *msgbus, void *cb_data, unsigned char *passphrase, size_t *passphrase_size){
    static const char bc_passphrase[65] = "0123456789012345678901234567890123456789012345678901234567890123";
    /* sha256 digest of bc_passphrase in string format is c999681d5a9790c4e703e1e41dadb88a4566691f9cd58ad43c9e76f19d49a44a */
    if(passphrase && passphrase_size && *passphrase_size >= 64) {
        memcpy(passphrase, bc_passphrase, 64);
        *passphrase_size = 64;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

bc_error_t beacon_create(beacon_t **beacon) {
    if(beacon) {
        ma_error_t rc = MA_OK;
        beacon_t *self = (beacon_t *)calloc(1, sizeof(beacon_t));
        
        if(!self) return BC_ERROR_OUTOFMEMORY;

	    if(MA_OK == (rc = ma_client_create(BC_PRODUCT_ID_STR, &self->ma_client))) {
			(void)ma_client_set_logger_callback(self->ma_client, bc_client_logger_cb, self) ;
			self->logger = NULL;
			beacon_logger =  NULL;
				
                /* NOTE- Purely optional and only requires mostly for interpeters langugag like JAVA/Python
                    if user wants to do challenge and response authentication in addition to executable validation.
                    User must add AuthorizationHash entry(which is sha256 and string representation of passphrase)
                    in the /etc/ma.d/<product_id>/config.xml or HKLM\McAfee\Agent\Applications\<SOFTWARE_ID> (REG_SZ)
                */ 
                ma_client_set_msgbus_passphrase_callback(self->ma_client, beacon_passphrase_provider_callback, self);
				
				MA_LOG(self->logger, MA_LOG_SEV_INFO, "Successfuly created ma client.");
				*beacon = self;
				return BC_OK;      
		}

        MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to create ma client,%d.",rc);
        beacon_release(self);
        return BC_CREATION_FAILED;
    }
    return BC_ERROR_INVALIDARG;
}

bc_error_t beacon_start(beacon_t *self){
	ma_error_t rc = MA_OK;
	if(MA_OK == (rc = ma_client_register_notification_callback(self->ma_client, bc_client_notification_cb, (void *)self))) {
		MA_LOG(self->logger, MA_LOG_SEV_INFO, "register for ma client notification.");		
bc_daily_task_create(self->ma_client);
		return BC_OK;                
	}
	else{
		MA_LOG(self->logger, MA_LOG_SEV_ERROR, "failed to register for ma client notification.");		
		return BC_OK;                
	}		
}

bc_error_t beacon_stop(beacon_t *self){
	if(MA_OK != ma_client_unregister_notification_callback(self->ma_client))
		MA_LOG(self->logger, MA_LOG_SEV_ERROR, "failed to unregister for ma client notification.");		
	return BC_OK;
}

bc_error_t beacon_client_start(beacon_t *self) {
    if(self) {
        bc_error_t rc = BC_START_FAILED;
        ma_error_t ma_rc = MA_OK;
		/*first start the client*/
        if(MA_OK == (ma_rc = ma_client_start(self->ma_client))) {           
            MA_LOG(self->logger, MA_LOG_SEV_INFO, "Successfuly started ma client.");
            rc = BC_OK;
        }

		/*then register for all other services.*/
        if(BC_OK == rc) {
			if(BC_OK == (rc = bc_info_intialize(self->ma_client))) {
				if(BC_OK == (rc = bc_property_intialize(self->ma_client))) {
					if(BC_OK == (rc = bc_policy_intialize(self->ma_client))) {
						if(BC_OK == (rc = bc_task_intialize(self->ma_client))) {
							if(BC_OK == (rc = bc_datachannel_intialize(self->ma_client))) {
								 if(BC_OK == (rc = bc_updater_intialize(self->ma_client))){
									 if(BC_OK == (rc = bc_event_intialize(self->ma_client))) {
										 MA_LOG(self->logger, MA_LOG_SEV_INFO, "Started beacon.");
									 }
								 }
							}
						}
					}            
				}
			}
		    if(BC_OK != rc){
			    beacon_stop(self);
			    MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to start beacon, rc = <%d>.", rc);
		    }        
        }
        else {
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to start ma client,%d.",rc);
            beacon_release(self); self = NULL;
			return rc;
        }
        return rc;
    }
    return BC_ERROR_INVALIDARG;
}

bc_error_t beacon_client_stop(beacon_t *self) {
     if(self) {
		 /*unregister for all other services.*/
        bc_property_deinitalize(self->ma_client);
        bc_policy_deinitalize(self->ma_client);		
        bc_task_deinitalize(self->ma_client);
		bc_datachannel_deinitalize(self->ma_client);
        bc_updater_deinitalize(self->ma_client);
		bc_event_deintialize(self->ma_client);
		bc_info_deinitalize(self->ma_client) ;

		/*stop ma client.*/
        ma_client_stop(self->ma_client);
        MA_LOG(self->logger, MA_LOG_SEV_INFO, "Stopped beacon.");
        return BC_OK;
    }
    return BC_ERROR_INVALIDARG;
}

bc_error_t beacon_release(beacon_t *self) {
    if(self) {		
        if(self->ma_client) ma_client_release(self->ma_client);
		if(self->logger) ma_logger_release(self->logger);
        free(self);
        return BC_OK;
    }
    return BC_ERROR_INVALIDARG;
}

static void bc_client_notification_cb(ma_client_t *client, ma_client_notification_t type, void *cb_data) {
	beacon_t *self = (beacon_t *)cb_data ;
	MA_LOG(self->logger, MA_LOG_SEV_INFO, "Beacon received client notification with type %d", type) ;

	switch(type) {
		case MA_CLIENT_NOTIFY_STOP:
		{
			beacon_client_stop(self) ;
			if(self->logger) ma_logger_release(self->logger);
			self->logger = NULL;
			beacon_logger = NULL;
			break ;
		}
		case MA_CLIENT_NOTIFY_START:
		{
			ma_file_logger_t *logger = NULL;
			#ifdef MA_WINDOWS
		    ma_file_logger_create(BC_LOG_FOLDER, BC_LOG_FILE_NAME_STR, NULL, &logger);
			#else
            ma_file_logger_create(NULL, BC_LOG_FILE_NAME_STR, NULL, &logger);
			#endif
			if(logger){
				self->logger = (ma_logger_t *)logger;
				beacon_logger =  (ma_logger_t *)logger;				
			}
			beacon_client_start(self) ;
			break ;
		}
		default :
			break ;
	}	
}

static void bc_client_logger_cb(ma_log_severity_t severity, const char *module_name, const char *log_message, void *user_data) {
	beacon_t *self = (beacon_t *)user_data ;
	if(self->logger)
		ma_log(self->logger, severity, module_name, "", 0, "", log_message);
	else
		log_message_fun(severity, module_name, log_message);
}

#include <stdio.h>
#include <stdarg.h>

void log_message_fun(ma_log_severity_t severity, const char *module_name, const char *log_message)
{
	FILE *fp = NULL;
	char file_path[256] = {0};

	#ifdef MA_WINDOWS
	_snprintf(file_path, 256, "%s\\%s.log", BC_LOG_FOLDER, BC_LOG_FILE_NAME_STR);
	#else
	snprintf(file_path, 256, "%s.log", BC_LOG_FILE_NAME_STR);
	#endif

    fp = fopen (file_path,"a+");
	
    if (fp)
    {
		static char *g_severity_names[] = {"Critical", "Error", "Warning", "Notice", "Info", "Debug", "Trace" , "Perfomance"};

		fprintf(fp, "%s.%s %s\n", module_name, g_severity_names[(int)severity-1], log_message);
		fclose(fp);
    }    
}

