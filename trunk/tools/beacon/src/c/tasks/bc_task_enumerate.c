#include "bc/bc_task.h"
#include "bc/bc_strdef.h"
#include "bc/bc_utils.h"
#include "ma/ma_variant.h"
#include "ma/scheduler/ma_scheduler.h"

#include <string.h>
#include <stdlib.h>


#ifdef MA_WINDOWS
#include <Windows.h>
#endif

static ma_logger_t *logger = NULL;

static ma_error_t on_task_execute(ma_context_t *context, const char *product_id, const char *task_type,  ma_task_t *task, void *cb_data) {
    char *task_id = NULL;
    ma_buffer_t *buffer_in = NULL;
    const char *beacon_path = "section";
    const char *beacon_key = "key";
    ma_variant_t *app_payload_in = NULL;
    char *user_data=NULL;
    unsigned int size = 0;


    ma_error_t rc = MA_OK;
    ma_task_get_id((ma_task_t *)task, &task_id);

    
    /*Get the setting set for the task*/
    if(MA_OK != (rc =ma_task_get_setting(task,beacon_path,beacon_key,&app_payload_in)) && (app_payload_in==NULL)) {
        MA_LOG(logger, MA_LOG_SEV_INFO, "Get task setting failed. error code(%d)", rc);
    }
    if(MA_OK != (rc =ma_variant_get_string_buffer(app_payload_in,&buffer_in))) {
        MA_LOG(logger, MA_LOG_SEV_INFO, "ma_variant_get_string_buffer failed. error code(%d)", rc);
    }
    if(MA_OK != (rc =ma_buffer_get_string(buffer_in,&user_data,&size))) {
        MA_LOG(logger, MA_LOG_SEV_INFO, "ma_buffer_get_string failed. error code(%d)", rc);
    }
    
    if(MA_OK != (rc = ma_scheduler_update_task_status(context, task_id, MA_TASK_STATUS_SUCCESS))) {
            MA_LOG(logger, MA_LOG_SEV_INFO, "updating task id(%s) status failed. error code(%d)", rc);
    }

    MA_LOG(logger, MA_LOG_SEV_INFO, "Executing Task id %s ", task_id);
    /*Dum the user data set for the task*/
    MA_LOG(logger, MA_LOG_SEV_INFO, "Got the user dat=%s", user_data);
    if(MA_OK != (rc =ma_buffer_release(buffer_in))) {
        MA_LOG(logger, MA_LOG_SEV_INFO, "ma_buffer_release failed. error code(%d)", rc);
    }
    if(MA_OK != (rc =ma_variant_release(app_payload_in))) {
       MA_LOG(logger, MA_LOG_SEV_INFO, "ma_variant_release failed. error code(%d)", rc);

    }
    
    return MA_OK;
}

static ma_error_t on_task_stop(ma_context_t *context, const char *task_type, const char *product_id, ma_task_t *task, void *cb_data) {
    char *task_id = NULL;
    ma_error_t rc = MA_OK;

    ma_task_get_id((ma_task_t *)task, &task_id);

    MA_LOG(logger, MA_LOG_SEV_INFO, "Stopping Task %s", task_id);
    return MA_OK;

}

static ma_error_t on_task_remove(ma_context_t *context, const char *task_type, const char *product_id, ma_task_t *task, void *cb_data) {
    char *task_id = NULL;
    ma_task_get_id((ma_task_t *)task, &task_id);
    MA_LOG(logger, MA_LOG_SEV_INFO, "Removing Task %s", task_id);
    return MA_OK;
}

static ma_error_t on_task_update_status(ma_context_t *context, const char *task_type, const char *product_id, ma_task_t *task, void *cb_data) {
    char *task_id = NULL;
    ma_error_t rc = MA_OK;

    ma_task_get_id((ma_task_t *)task, &task_id);
    if(MA_OK != (rc = ma_scheduler_update_task_status(context, task_id, MA_TASK_STATUS_SUCCESS))) {
            MA_LOG(logger, MA_LOG_SEV_INFO, "updating task id(%s) status failed. error code(%d) ",rc);
    }
    MA_LOG(logger, MA_LOG_SEV_INFO, "Updating Task %s status", task_id);
    return MA_OK;
}

/* Creates a daily task with Product ID, Creator ID and Tsak type mentioned by user*/
ma_error_t bc_create_daily_task_e(ma_context_t *ma_context,char *task_id,char *task_name,char *product_id, char *beacon_creator,char * task_type) {
    ma_error_t rc = MA_OK;
	if(ma_context&& task_id && task_name &&product_id &&beacon_creator &&task_type) {
		ma_task_t *task = NULL;
        ma_trigger_daily_policy_t policy = {0};
        ma_trigger_t *trigger = NULL;
        ma_task_time_t start_date = {0};
        ma_task_time_t end_date = {0};
        ma_variant_t *app_payload = NULL;
        ma_task_repeat_policy_t repeat_policy = {0};
        ma_task_t *task_get = NULL;
        ma_task_time_t next_time={0};
        ma_time_zone_t zone = MA_TIME_ZONE_UTC;
        const char *beacon_str = "User-data beacon_monthlydow";
        const char *beacon_path = "section";
        const char *beacon_key = "key";
        //const char *task_id = "ma.beacon.daily";
  
        policy.days_interval = 1; /* days intervals = daily */
        (void)ma_context_get_logger(ma_context, &logger);
		repeat_policy.repeat_interval.minute = 2; //Every 2min repeat
        repeat_policy.repeat_until.minute = 4; //Repeat until 24 hrs
		if(MA_OK == (rc = ma_task_create(task_id,&task))) { 
			MA_LOG(logger, MA_LOG_SEV_INFO, "Creating Task %s", task_id);
            if(MA_OK != (rc = ma_task_set_name(task,task_name))) {
				MA_LOG(logger, MA_LOG_SEV_INFO, "beacon failed to set task name. error code(%d)", rc);
            }
            if(MA_OK != (rc = ma_task_set_type(task,task_type))) {
				MA_LOG(logger, MA_LOG_SEV_INFO, "beacon failed to set task type. error code(%d)", rc);
            }
            if(MA_OK == (rc = ma_task_set_software_id(task, product_id))) {
				if(MA_OK != (rc = ma_variant_create_from_string(beacon_str, strlen(beacon_str), &app_payload))) {
					MA_LOG(logger, MA_LOG_SEV_INFO, "beacon failed to create variant. error code(%d) ", rc);
                }
                if(MA_OK != (rc = ma_task_set_setting(task, beacon_path, beacon_key, app_payload))) {
                    MA_LOG(logger, MA_LOG_SEV_INFO, "beacon failed to set task settings. error code(%d) ", rc);
                }
                if(MA_OK != (rc = ma_task_set_creator_id(task, beacon_creator))) {
                    MA_LOG(logger, MA_LOG_SEV_INFO, "beacon failed to set task creator id.  error code(%d) ", rc);
                }
                if(MA_OK != (rc = ma_task_set_repeat_policy(task, repeat_policy))) {
                    MA_LOG(logger, MA_LOG_SEV_INFO, "beacon failed to set task repeat policy. error code(%d) ", rc);
                }
                if(MA_OK != (rc = ma_task_set_max_run_time_limit(task, 1))) {
                    MA_LOG(logger, MA_LOG_SEV_INFO, "beacon failed to set max run time limit. error code(%d) ", rc);
                }

                if(MA_OK == (rc = ma_trigger_create_daily(&policy, &trigger))) {
                    advance_current_date_by_seconds(600*6, &start_date); /* start after 1min from now */
                    if(MA_OK == (rc = ma_trigger_set_begin_date(trigger, &start_date))) {
                        if(MA_OK == (rc = ma_task_add_trigger(task, trigger))) {
                            if(MA_OK != (rc = ma_scheduler_add_task(ma_context, task))){
								MA_LOG(logger, MA_LOG_SEV_INFO, "scheduler add task failed. error code(%d)", rc);
                            }else{
								MA_LOG(logger, MA_LOG_SEV_INFO, "scheduler added task id(%s)successfully", task_id);
							}
                         }
                    }else{
                         MA_LOG(logger, MA_LOG_SEV_INFO, "set begin date failed. error code(%d)", rc);
                    }
                }
            }
            (void)ma_task_release(task);
        }
        if(MA_OK != (rc =ma_variant_release(app_payload))) {
            MA_LOG(logger, MA_LOG_SEV_INFO, "ma_variant_release failed. error code(%d) ", rc);
        }
        return rc;
	}
return MA_ERROR_INVALIDARG;
}

ma_error_t remove_task(ma_context_t *ma_context,char * task_type){
	ma_task_t **task_set = NULL;
	size_t task_no = 0;
	ma_error_t rc = MA_OK;
	char * task_id = NULL;
	size_t i =0;

	if(ma_context && task_type){
		if(MA_OK != (rc =ma_scheduler_enumerate_task(ma_context,NULL,NULL,task_type,&task_set,&task_no))){
			MA_LOG(logger, MA_LOG_SEV_INFO, "task ma_scheduler_enumerate_task failed. error code(%d) ", rc);
			return rc;
		}
		if(task_set != NULL && task_no != 0){
			for(i =0;i<task_no;i++){
				ma_task_get_id(*(task_set+i),&task_id);
				MA_LOG(logger, MA_LOG_SEV_INFO, "removing task id =(%s) ",task_id);
				if(MA_OK != (rc = ma_scheduler_remove_task(ma_context,task_id))){
					MA_LOG(logger, MA_LOG_SEV_INFO, "task ma_scheduler_remove_task failed. error code(%d) ", rc);
					return rc;
				}
			}
		}
		
	 }
	 return MA_ERROR_INVALIDARG;

}

bc_error_t bc_task_enumerate(ma_context_t *ma_context){
	
	ma_error_t rc = MA_OK;
	ma_task_t **task_set = NULL;
	size_t task_no = 0;
	size_t i =0;

	if(ma_context){
	MA_LOG(logger, MA_LOG_SEV_INFO, "Test task enumerate %d", ma_context);
	
	if(MA_OK != (rc = bc_create_daily_task_e(ma_context,"Task1","Becaon Task1","BEACON_TEST1","BEACON_USER1","Type1"))){
		MA_LOG(logger, MA_LOG_SEV_INFO, "task creation failed. error code(%d) ", rc);
		return (bc_error_t)rc;
	}
	Sleep(30*1000);
	if(MA_OK != (rc = bc_create_daily_task_e(ma_context,"Task2","Becaon Task2","BEACON_TEST1","BEACON_USER1","Type1"))){
		MA_LOG(logger, MA_LOG_SEV_INFO, "task creation failed. error code(%d) ", rc);
		return (bc_error_t)rc;
	}
	Sleep(30*1000);
	if(MA_OK != (rc = bc_create_daily_task_e(ma_context,"Task3","Becaon Task3","BEACON_TEST3","BEACON_USER2","Type2"))){
		MA_LOG(logger, MA_LOG_SEV_INFO, "task creation failed. error code(%d) ", rc);
		return (bc_error_t)rc;
	}
	Sleep(30*1000);
	if(MA_OK != (rc = bc_create_daily_task_e(ma_context,"Task4","Becaon Task4","BEACON_TEST1","BEACON_USER1","Type1"))){
		MA_LOG(logger, MA_LOG_SEV_INFO, "task creation failed. error code(%d) ", rc);
		return (bc_error_t)rc;
	}
	Sleep(60*1000);
	
	if(MA_OK != (rc =ma_scheduler_enumerate_task(ma_context,NULL,NULL,NULL,&task_set,&task_no))){
		MA_LOG(logger, MA_LOG_SEV_INFO, "task ma_scheduler_enumerate_task failed. error code(%d) ", rc);
		//return (bc_error_t)rc;
	}
	if(task_set != NULL && task_no != 0){
		MA_LOG(logger, MA_LOG_SEV_INFO, "Got the task in task enumerat=(%d) ", task_no);
	}
	Sleep(10*1000);
	task_no = 0;
	
	/* Pass the product ID and get all task created by that prodcut*/
	if(MA_OK != (rc =ma_scheduler_enumerate_task(ma_context,"BEACON_TEST1",NULL,NULL,&task_set,&task_no))){
		MA_LOG(logger, MA_LOG_SEV_INFO, "task ma_scheduler_enumerate_task failed. error code(%d) ", rc);
		return (bc_error_t)rc;
	}
	if(task_set != NULL && task_no != 0){
		char * task_name = NULL;
		MA_LOG(logger, MA_LOG_SEV_INFO, "Number of task in product id BEACON_TEST1=(%d) ", task_no);
		for(i=0;i<task_no;i++){
			ma_task_get_name(*(task_set+i),&task_name);
			MA_LOG(logger, MA_LOG_SEV_INFO, "1st Task=(%s) ", task_name);
		}
	}
	
	Sleep(10*1000);
	task_no = 0;
	/* Pass the creator ID and get all task created by that creator*/
	if(MA_OK != (rc =ma_scheduler_enumerate_task(ma_context,NULL,"BEACON_USER1",NULL,&task_set,&task_no))){
		MA_LOG(logger, MA_LOG_SEV_INFO, "task ma_scheduler_enumerate_task failed. error code(%d) ", rc);
		//return (bc_error_t)rc;
	}
	if(task_set != NULL && task_no != 0){
		char * task_name = NULL;
		MA_LOG(logger, MA_LOG_SEV_INFO, "Number of task in creator id BEACON_USER1=(%d) ", task_no);
		for(i=0;i<task_no;i++){
			ma_task_get_name(*(task_set+i),&task_name);
			MA_LOG(logger, MA_LOG_SEV_INFO, "1st Task=(%s) ", task_name);
		}

	}
	
	Sleep(10*1000);
	task_no = 0;
	/* Pass the task type and get all task of that type*/
	if(MA_OK != (rc =ma_scheduler_enumerate_task(ma_context,NULL,NULL,"Type1",&task_set,&task_no))){
		MA_LOG(logger, MA_LOG_SEV_INFO, "task ma_scheduler_enumerate_task failed. error code(%d) ", rc);
		//return (bc_error_t)rc;
	}
	if(task_set != NULL && task_no != 0){
		char * task_name = NULL;
		MA_LOG(logger, MA_LOG_SEV_INFO, "Number of task in task type type1=(%d) ", task_no);
		for(i=0;i<task_no;i++){
			ma_task_get_name(*(task_set+i),&task_name);
			MA_LOG(logger, MA_LOG_SEV_INFO, "1st Task=(%s) ", task_name);
		}
	}
	
	Sleep(10*1000);
	task_no = 0;
	/* Pass the prodct ID and creator ID,get all task which created by that prodcu id and creator id*/
	if(MA_OK != (rc =ma_scheduler_enumerate_task(ma_context,"BEACON_TEST1","BEACON_USER1",NULL,&task_set,&task_no))){
		MA_LOG(logger, MA_LOG_SEV_INFO, "task ma_scheduler_enumerate_task failed. error code(%d) ", rc);
		//return (bc_error_t)rc;
	}
	if(task_set != NULL && task_no != 0){
		char * task_name = NULL;
		MA_LOG(logger, MA_LOG_SEV_INFO, "Number of task in creator id BEACON_USER1 prodcut id BEACON_TEST1=(%d) ", task_no);
		for(i=0;i<task_no;i++){
			ma_task_get_name(*(task_set+i),&task_name);
			MA_LOG(logger, MA_LOG_SEV_INFO, "1st Task=(%s) ", task_name);
		}
	}

	Sleep(10*1000);
	task_no = 0;
	/* Pass the creator ID and task type,get all task which created by that creator id and task type*/
	if(MA_OK != (rc =ma_scheduler_enumerate_task(ma_context,NULL,"BEACON_USER1","Type1",&task_set,&task_no))){
		MA_LOG(logger, MA_LOG_SEV_INFO, "task ma_scheduler_enumerate_task failed. error code(%d) ", rc);
		//return (bc_error_t)rc;
	}
	if(task_set != NULL && task_no != 0){
		char * task_name = NULL;
		MA_LOG(logger, MA_LOG_SEV_INFO, "Number of task in creator id BEACON_USER1 and task Type1=(%d) ", task_no);
		for(i=0;i<task_no;i++){
			ma_task_get_name(*(task_set+i),&task_name);
			MA_LOG(logger, MA_LOG_SEV_INFO, "1st Task=(%s) ", task_name);
		}
	}

	Sleep(10*1000);
	task_no = 0;
	/* Pass the prodct ID and task type,get all task which created by that prodcut id and task type*/
	if(MA_OK != (rc =ma_scheduler_enumerate_task(ma_context,"BEACON_TEST1",NULL,"Type1",&task_set,&task_no))){
		MA_LOG(logger, MA_LOG_SEV_INFO, "task ma_scheduler_enumerate_task failed. error code(%d) ", rc);
		//return (bc_error_t)rc;
	}
	if(task_set != NULL && task_no != 0){
		char * task_name = NULL;
		MA_LOG(logger, MA_LOG_SEV_INFO, "Number of task in prodcut id BEACON_TEST1 and task Type1=(%d) ", task_no);
		for(i=0;i<task_no;i++){
			ma_task_get_name(*(task_set+i),&task_name);
			MA_LOG(logger, MA_LOG_SEV_INFO, "1st Task=(%s) ", task_name);
		}
	}


	Sleep(10*1000);
	task_no = 0;
	/* pass the prodct id and task type and creator id,get all task which created by that prodcut id creator id and task type*/
	if(MA_OK != (rc =ma_scheduler_enumerate_task(ma_context,"BEACON_TEST1","BEACON_USER1","Type1",&task_set,&task_no))){
		MA_LOG(logger, MA_LOG_SEV_INFO, "task ma_scheduler_enumerate_task failed. error code(%d) ", rc);
		//return (bc_error_t)rc;
	}
	if(task_set != NULL && task_no != 0){
		char * task_name = NULL;
		MA_LOG(logger, MA_LOG_SEV_INFO, "Number of task in prodcut id BEACON_TEST1, creator id BEACON_USER1 and task Type1=(%d) ", task_no);
		for(i=0;i<task_no;i++){
			ma_task_get_name(*(task_set+i),&task_name);
			MA_LOG(logger, MA_LOG_SEV_INFO, "1st Task=(%s) ", task_name);
		}
	}


	Sleep(30*1000);
	/*Removing task added for test task enumerator*/
	if(MA_OK != (rc =remove_task(ma_context,"Type1"))){
		MA_LOG(logger, MA_LOG_SEV_INFO, "remove_task. error code(%d) ", rc);
		//return (bc_error_t)rc;
	}
	Sleep(10*1000);
	if(MA_OK != (rc =remove_task(ma_context,"Type2"))){
		MA_LOG(logger, MA_LOG_SEV_INFO, "remove_task. error code(%d) ", rc);
		//return (bc_error_t)rc;
	}

	

	return (bc_error_t)rc;
}else{
	return (bc_error_t)MA_ERROR_INVALIDARG;
	}
}

