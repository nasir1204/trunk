#include "bc/bc_task.h"
#include "bc/bc_strdef.h"
#include "bc/bc_utils.h"
#include "ma/ma_variant.h"
#include "ma/scheduler/ma_scheduler.h"

#include <string.h>
#include <stdlib.h>

#ifdef MA_WINDOWS
#include <Windows.h>
#endif

static ma_logger_t *logger = NULL;
static int count=0;

static ma_error_t on_task_execute(ma_context_t *context, const char *product_id, const char *task_type,  ma_task_t *task, void *cb_data) {
    char *task_id = NULL;
	char *task_name = NULL;
	char *task_type_ = NULL;
	ma_buffer_t *buffer_in = NULL;
	const char *beacon_path = "section";
	const char *beacon_key = "key";
	ma_variant_t *app_payload_in = NULL;
	char *user_data=NULL;
	unsigned int size = 0;

    ma_error_t rc = MA_OK;
	ma_task_get_id((ma_task_t *)task, &task_id);
	ma_task_get_name((ma_task_t *)task, &task_name);
	ma_task_get_type((ma_task_t *)task, &task_type_);
	
	
	/*Get the setting set for the task*/
	if(MA_OK != (rc =ma_task_get_setting(task,beacon_path,beacon_key,&app_payload_in))) {
		MA_LOG(logger, MA_LOG_SEV_INFO, "Get task setting failed. error code(%s) error reason(%s)", rc, rc);
	}
	if(MA_OK != (rc =ma_variant_get_string_buffer(app_payload_in,&buffer_in))) {
		MA_LOG(logger, MA_LOG_SEV_INFO, "ma_variant_get_string_buffer failed. error code(%s) error reason(%s)", rc, rc);
	}
	if(MA_OK != (rc =ma_buffer_get_string(buffer_in,&user_data,&size))) {
		MA_LOG(logger, MA_LOG_SEV_INFO, "ma_buffer_get_string failed. error code(%s) error reason(%s)", rc, rc);
	}
	
	/*Update the status as SUCCESS for 2 repeates */
	if(count != 2){
		if(MA_OK != (rc = ma_scheduler_update_task_status(context, task_id,MA_TASK_STATUS_SUCCESS))) {
			MA_LOG(logger, MA_LOG_SEV_INFO, "updating task id(%s) status failed. error code(%s) error reason(%s)", rc, rc);
		}
	}else{
		MA_LOG(logger, MA_LOG_SEV_INFO, "Not updating status count=%d",count);
	}
	count++;
	MA_LOG(logger, MA_LOG_SEV_INFO, "Executing Task id %s and count=%d", task_id,count);
	/*Dum the user data set for the task*/
	MA_LOG(logger, MA_LOG_SEV_INFO, "Got the user dat=%s", user_data);
	if(MA_OK != (rc =ma_buffer_release(buffer_in))) {
		MA_LOG(logger, MA_LOG_SEV_INFO, "ma_buffer_release failed. error code(%s) error reason(%s)", rc, rc);
	}
	if(MA_OK != (rc =ma_variant_release(app_payload_in))) {
       MA_LOG(logger, MA_LOG_SEV_INFO, "ma_variant_release failed. error code(%s) error reason(%s)", rc, rc);
    }
    return MA_OK;
}

static ma_error_t on_task_stop(ma_context_t *context, const char *task_type, const char *product_id, ma_task_t *task, void *cb_data) {
    char *task_id = NULL;
	ma_error_t rc = MA_OK;
	ma_task_t *task_get = NULL;
	ma_task_time_t next_time={0};
	
	ma_task_get_id((ma_task_t *)task, &task_id);
	MA_LOG(logger, MA_LOG_SEV_INFO, "Stopping Task %s", task_id);
	
	if( MA_OK != (rc = ma_scheduler_get_task(context,"ma.beacon.monthlydow",&task_get))){
		MA_LOG(logger, MA_LOG_SEV_INFO, "scheduler get task failed. error code(%s) error reason(%s)", rc, rc);
	}
	if( MA_OK != (rc = ma_task_get_next_run_time(task_get,&next_time))){
		MA_LOG(logger, MA_LOG_SEV_INFO, "scheduler ma_task_get_next_run_time failed. error code(%s) error reason(%s)", rc, rc);
	}
	MA_LOG(logger,MA_LOG_SEV_INFO,"Next run time <%d:%d:%d:%d:%d:%d>\n",next_time.day,next_time.month,next_time.year,next_time.hour,next_time.minute,next_time.secs);
	
    return MA_OK;
}

static ma_error_t on_task_remove(ma_context_t *context, const char *task_type, const char *product_id, ma_task_t *task, void *cb_data) {
    char *task_id = NULL;
    ma_task_get_id((ma_task_t *)task, &task_id);
	MA_LOG(logger, MA_LOG_SEV_INFO, "Removing Task %s", task_id);
    return MA_OK;

}


static ma_error_t on_task_update_status(ma_context_t *context, const char *task_type, const char *product_id, ma_task_t *task, void *cb_data) {
    char *task_id = NULL;
    ma_task_get_id((ma_task_t *)task, &task_id);
	MA_LOG(logger, MA_LOG_SEV_INFO, "Updating Task %s status", task_id);
    return MA_OK;

}

/*Create a Monthly day of week task which will start on the current day and will execute on 1st SUNDAY of months JULY|AUGUST|NOVEMBER|OCTOBER|FEBRUARY*/
bc_error_t bc_monthlydow_task_create(ma_context_t *ma_context) {
    if(ma_context) {
	    ma_error_t rc = MA_OK;
        ma_scheduler_task_callbacks_t cb = {&on_task_execute, &on_task_stop, &on_task_remove,&on_task_update_status};
        const char *task_type = "ma.beacon.monthlydow.add";
        const char *product_id = "BEACON__TEST";

		count=0;
		MA_LOG(logger, MA_LOG_SEV_INFO, "bc_monthlydow_task_create %d", ma_context);
        if(MA_OK == (rc = ma_scheduler_register_task_callbacks(ma_context, product_id, task_type, &cb, NULL))) {
            ma_task_t *task = NULL;
            ma_trigger_monthly_dow_policy_t policy = {FIRST_WEEK,SUNDAY,OCTOBER};
            ma_trigger_t *trigger = NULL;
            ma_task_time_t start_date = {0};
	        ma_task_time_t end_date = {0};
            ma_variant_t *app_payload = NULL;
	        ma_task_repeat_policy_t repeat_policy = {0};
			ma_task_t *task_get = NULL;
			ma_time_zone_t zone = MA_TIME_ZONE_UTC;
			ma_task_time_t next_time={0};
            const char *beacon_str = "User-data beacon_monthlydow";
			const char *beacon_path = "section";
			const char *beacon_key = "key";
            const char *beacon_creator = "beacon";
  
			policy.day_of_the_week = SUNDAY;/* day of week  = SUNDAY */
			policy.which_week = FIRST_WEEK;/* week  = FIRST week*/
			policy.months = JULY|AUGUST|NOVEMBER|OCTOBER|FEBRUARY;/* Months on which task need o execute*/

            (void)ma_context_get_logger(ma_context, &logger);

            repeat_policy.repeat_interval.minute = 2; //Every 2min repeat
            repeat_policy.repeat_until.minute = 5; //Repeat until 1 hrs

            if(MA_OK == (rc = ma_task_create("ma.beacon.monthlydow", &task))) { 
				MA_LOG(logger, MA_LOG_SEV_INFO, "Creating Task %s", task_type);
				if(MA_OK != (rc = ma_task_set_name(task, "BEACON_ADD_monthlydow"))) {
					MA_LOG(logger, MA_LOG_SEV_INFO, "beacon failed to set task name. error code(%s) error reason(%s)", rc, rc);
                }
				if(MA_OK != (rc = ma_task_set_type(task,task_type))) {
					MA_LOG(logger, MA_LOG_SEV_INFO, "beacon failed to set task type. error code(%s) error reason(%s)", rc, rc);
                }
                if(MA_OK == (rc = ma_task_set_software_id(task, product_id))) {
                    if(MA_OK != (rc = ma_variant_create_from_string(beacon_str, strlen(beacon_str), &app_payload))) {
                        MA_LOG(logger, MA_LOG_SEV_INFO, "beacon failed to create variant. error code(%s) error reason(%s)", rc, rc);
                    }
					/*seting User data to task*/
	                if(MA_OK != (rc = ma_task_set_setting(task, beacon_path, beacon_key, app_payload))) {
                        MA_LOG(logger, MA_LOG_SEV_INFO, "beacon failed to set task settings. error code(%s) error reason(%s)", rc, rc);
                    }
                    if(MA_OK != (rc = ma_task_set_creator_id(task, beacon_creator))) {
                        MA_LOG(logger, MA_LOG_SEV_INFO, "beacon failed to set task creator id.  error code(%s) error reason(%s)", rc, rc);
                    }
                   if(MA_OK != (rc = ma_task_set_repeat_policy(task, repeat_policy))) {
                        MA_LOG(logger, MA_LOG_SEV_INFO, "beacon failed to set task repeat policy. error code(%s) error reason(%s)", rc, rc);
                    }
				   #ifdef Timezone
                        if(MA_OK != (rc = ma_task_set_time_zone(task,zone))) {
                            MA_LOG(logger, MA_LOG_SEV_INFO, "beacon failed to set tiem zone. error code(%d)", rc);
                        }
                        MA_LOG(logger, MA_LOG_SEV_INFO, "Set the time zone to utc");
                    #endif
                    if(MA_OK != (rc = ma_task_set_max_run_time_limit(task, 1))) {
                        MA_LOG(logger, MA_LOG_SEV_INFO, "beacon failed to set max run time limit. error code(%s) error reason(%s)", rc, rc);
                    }
                    if(MA_OK == (rc = ma_trigger_create_monthly_dow(&policy, &trigger))) {
                        advance_current_date_by_seconds(60, &start_date); /* start after 1min from now */
						if(MA_OK == (rc = ma_trigger_set_begin_date(trigger, &start_date))) {
                            if(MA_OK == (rc = ma_task_add_trigger(task, trigger))) {
                                if(MA_OK != (rc = ma_scheduler_add_task(ma_context, task))){
		                            MA_LOG(logger, MA_LOG_SEV_INFO, "scheduler add task failed. error code(%s) error reason(%s)", rc, rc);
	                            }
								
							}
                        }
                    }
                }
                (void)ma_task_release(task);
            }
			if(MA_OK != (rc =ma_variant_release(app_payload))) {
                MA_LOG(logger, MA_LOG_SEV_INFO, "ma_variant_release failed. error code(%s) error reason(%s)", rc, rc);
            }
        }else{
			MA_LOG(logger, MA_LOG_SEV_INFO, "Creating Task %s failed", task_type);
		}
		
	    return (bc_error_t)rc;
    }
    return (bc_error_t)MA_ERROR_INVALIDARG;
}



