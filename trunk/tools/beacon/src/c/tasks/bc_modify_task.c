#include "bc/bc_task.h"
#include "bc/bc_strdef.h"
#include "bc/bc_utils.h"
#include "ma/ma_variant.h"
#include "ma/scheduler/ma_scheduler.h"

#include <string.h>
#include <stdlib.h>

#ifdef MA_WINDOWS
#include <Windows.h>
#endif

static ma_logger_t *logger = NULL;
static int count=0;

static ma_error_t on_task_execute(ma_context_t *context, const char *product_id, const char *task_type,  ma_task_t *task, void *cb_data) {
    char *task_id = NULL;
	ma_buffer_t *buffer_in = NULL;
	const char *beacon_path = "section";
	const char *beacon_key = "key";
	ma_variant_t *app_payload_in = NULL;
	char *user_data=NULL;
	unsigned int size = 0;

	ma_error_t rc = MA_OK;
	ma_task_get_id((ma_task_t *)task, &task_id);
	
	
	/*Get the setting set for the task*/
	if(MA_OK != (rc =ma_task_get_setting(task,beacon_path,beacon_key,&app_payload_in)) && (app_payload_in==NULL)) {
		MA_LOG(logger, MA_LOG_SEV_INFO, "Get task setting failed. error code(%s) error reason(%s)", rc, rc);
	}
	if(MA_OK != (rc =ma_variant_get_string_buffer(app_payload_in,&buffer_in))) {
		MA_LOG(logger, MA_LOG_SEV_INFO, "ma_variant_get_string_buffer failed. error code(%s) error reason(%s)", rc, rc);
	}
	if(MA_OK != (rc =ma_buffer_get_string(buffer_in,&user_data,&size))) {
		MA_LOG(logger, MA_LOG_SEV_INFO, "ma_buffer_get_string failed. error code(%s) error reason(%s)", rc, rc);
	}
	
	if(MA_OK != (rc = ma_scheduler_update_task_status(context, task_id, MA_TASK_STATUS_SUCCESS))) {
			MA_LOG(logger, MA_LOG_SEV_INFO, "updating task id(%s) status failed. error code(%s) error reason(%s)", rc, rc);
	}
	count++;
	MA_LOG(logger, MA_LOG_SEV_INFO, "Executing Task id %s and count=%d", task_id,count);
	/*Dum the user data set for the task*/
	MA_LOG(logger, MA_LOG_SEV_INFO, "Got the user dat=%s", user_data);
	if(MA_OK != (rc =ma_buffer_release(buffer_in))) {
		MA_LOG(logger, MA_LOG_SEV_INFO, "ma_buffer_release failed. error code(%s) error reason(%s)", rc, rc);
	}
	if(MA_OK != (rc =ma_variant_release(app_payload_in))) {
       MA_LOG(logger, MA_LOG_SEV_INFO, "ma_variant_release failed. error code(%s) error reason(%s)", rc, rc);
    }
    return MA_OK;
}

static ma_error_t on_task_stop(ma_context_t *context, const char *task_type, const char *product_id, ma_task_t *task, void *cb_data) {
    char *task_id = NULL;
	ma_error_t rc = MA_OK;

    ma_task_get_id((ma_task_t *)task, &task_id);
	MA_LOG(logger, MA_LOG_SEV_INFO, "Stopping Task %s", task_id);
	
    return MA_OK;
}

static ma_error_t on_task_remove(ma_context_t *context, const char *task_type, const char *product_id, ma_task_t *task, void *cb_data) {
    char *task_id = NULL;
    ma_task_get_id((ma_task_t *)task, &task_id);
	MA_LOG(logger, MA_LOG_SEV_INFO, "Removing Task %s", task_id);
    return MA_OK;
}

static ma_error_t on_task_update_status(ma_context_t *context, const char *task_type, const char *product_id, ma_task_t *task, void *cb_data) {
    char *task_id = NULL;
    ma_task_get_id((ma_task_t *)task, &task_id);
	MA_LOG(logger, MA_LOG_SEV_INFO, "Updating Task %s status", task_id);
    return MA_OK;

}

/*Modify the Daily task interval from 5 min to 3 min and repeat  until from 1 hour to 30 min after task repeats 2 times*/
bc_error_t bc_modify_task(ma_context_t *ma_context) {
    if(ma_context) {
	    ma_error_t rc = MA_OK;
        ma_scheduler_task_callbacks_t cb = {&on_task_execute, &on_task_stop, &on_task_remove,&on_task_update_status};
        const char *task_type = "ma.beacon.daily.add_m";
        const char *product_id = "BEACON__TEST";

		count=0;
		MA_LOG(logger, MA_LOG_SEV_INFO, "bc_daily_task_create %d", ma_context);
        if(MA_OK == (rc = ma_scheduler_register_task_callbacks(ma_context, product_id, task_type, &cb, NULL))) {
            ma_task_t *task = NULL;
            ma_trigger_daily_policy_t policy = {0};
            ma_trigger_t *trigger = NULL;
            ma_task_time_t start_date = {0};
	        ma_task_time_t end_date = {0};
            ma_variant_t *app_payload = NULL;
	        ma_task_repeat_policy_t repeat_policy = {0};
			ma_task_t *task_get = NULL;
			ma_task_time_t next_time={0};
            const char *beacon_str = "User-data beacon_monthlydow";
			const char *beacon_path = "section";
			const char *beacon_key = "key";
            const char *beacon_creator = "beacon";
  
	        policy.days_interval = 1; /* days intervals = daily */
            (void)ma_context_get_logger(ma_context, &logger);

            repeat_policy.repeat_interval.minute = 5; //Every 2min repeat
            repeat_policy.repeat_until.hour = 1; //Repeat until 24 hrs
			repeat_policy.repeat_until.minute = 0;

            if(MA_OK == (rc = ma_task_create("ma.beacon.daily_m", &task))) { 
				MA_LOG(logger, MA_LOG_SEV_INFO, "Creating Task %s", task_type);
				if(MA_OK != (rc = ma_task_set_name(task,"BEACON_ADD_daily_m"))) {
					MA_LOG(logger, MA_LOG_SEV_INFO, "beacon failed to set task name. error code(%s) error reason(%s)", rc, rc);
                }
				if(MA_OK != (rc = ma_task_set_type(task,task_type))) {
					MA_LOG(logger, MA_LOG_SEV_INFO, "beacon failed to set task type. error code(%s) error reason(%s)", rc, rc);
                }
                if(MA_OK == (rc = ma_task_set_software_id(task, product_id))) {
                    if(MA_OK != (rc = ma_variant_create_from_string(beacon_str, strlen(beacon_str), &app_payload))) {
                        MA_LOG(logger, MA_LOG_SEV_INFO, "beacon failed to create variant. error code(%s) error reason(%s)", rc, rc);
                    }
	                if(MA_OK != (rc = ma_task_set_setting(task, beacon_path, beacon_key, app_payload))) {
                        MA_LOG(logger, MA_LOG_SEV_INFO, "beacon failed to set task settings. error code(%s) error reason(%s)", rc, rc);
                    }
                    if(MA_OK != (rc = ma_task_set_creator_id(task, beacon_creator))) {
                        MA_LOG(logger, MA_LOG_SEV_INFO, "beacon failed to set task creator id.  error code(%s) error reason(%s)", rc, rc);
                    }
                    if(MA_OK != (rc = ma_task_set_repeat_policy(task, repeat_policy))) {
                        MA_LOG(logger, MA_LOG_SEV_INFO, "beacon failed to set task repeat policy. error code(%s) error reason(%s)", rc, rc);
                    }
                    if(MA_OK != (rc = ma_task_set_max_run_time_limit(task, 1))) {
                        MA_LOG(logger, MA_LOG_SEV_INFO, "beacon failed to set max run time limit. error code(%s) error reason(%s)", rc, rc);
                    }
                    if(MA_OK == (rc = ma_trigger_create_daily(&policy, &trigger))) {
                        advance_current_date_by_seconds(60, &start_date); /* start after 1min from now */
                        if(MA_OK == (rc = ma_trigger_set_begin_date(trigger, &start_date))) {
                            if(MA_OK == (rc = ma_task_add_trigger(task, trigger))) {
                                if(MA_OK != (rc = ma_scheduler_add_task(ma_context, task))){
		                            MA_LOG(logger, MA_LOG_SEV_INFO, "scheduler add task failed. error code(%s) error reason(%s)", rc, rc);
	                            }
								#ifdef MA_WINDOWS
									Sleep(630*1000);
								#else
									sleep(150);
								#endif
								if( MA_OK == (rc = ma_scheduler_get_task(ma_context,"ma.beacon.daily_m",&task_get))){
									if( MA_OK != (rc = ma_task_get_next_run_time(task_get,&next_time))){
										MA_LOG(logger, MA_LOG_SEV_INFO, "scheduler ma_task_get_next_run_time failed. error code(%s) error reason(%s)", rc, rc);
									}
									MA_LOG(logger,MA_LOG_SEV_INFO,"######Next run time <%d:%d:%d:%d:%d:%d>\n",next_time.day,next_time.month,next_time.year,next_time.hour,next_time.minute,next_time.secs);
									repeat_policy.repeat_interval.minute = 3; //change repeat from 5 min 3min repeat
									repeat_policy.repeat_until.hour = 0; 
									repeat_policy.repeat_until.minute = 30; //change Repeat until 1 hrs to 7 min
									if(MA_OK != (rc = ma_task_set_repeat_policy(task_get, repeat_policy))) {
									   MA_LOG(logger, MA_LOG_SEV_INFO, "beacon failed to set task repeat policy. error code(%s) error reason(%s)", rc, rc);
									}
									if(MA_OK != (rc = ma_scheduler_alter_task(ma_context,task_get))){
										  MA_LOG(logger, MA_LOG_SEV_INFO, "beacon failed modify task. error code(%s) error reason(%s)", rc, rc);
									}
								}else{
									MA_LOG(logger, MA_LOG_SEV_INFO, "scheduler get task failed. error code(%s) error reason(%s)", rc, rc);
								}
							}
                        }
                    }
                }
                (void)ma_task_release(task);
            }
			if(MA_OK != (rc =ma_variant_release(app_payload))) {
                MA_LOG(logger, MA_LOG_SEV_INFO, "ma_variant_release failed. error code(%s) error reason(%s)", rc, rc);
            }
        }else{
			MA_LOG(logger, MA_LOG_SEV_INFO, "Creating Task %s failed", task_type);
		}
		
	    return (bc_error_t)rc;
    }
    return (bc_error_t)MA_ERROR_INVALIDARG;
}


