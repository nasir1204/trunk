#include "bc/bc_task.h"
#include "bc/bc_strdef.h"
#include "bc/bc_utils.h"
#include "ma/ma_variant.h"
#include "ma/scheduler/ma_scheduler.h"

#include <string.h>
#include <stdlib.h>


#ifdef MA_WINDOWS
#include <Windows.h>
#endif

static ma_logger_t *logger = NULL;

static ma_error_t on_task_execute(ma_context_t *context, const char *product_id, const char *task_type,  ma_task_t *task, void *cb_data) {
    char *task_id = NULL;
	char *task_name = NULL;
	char *task_type_ = NULL;
    ma_error_t rc = MA_OK;
	ma_task_get_id((ma_task_t *)task, &task_id);
	ma_task_get_name((ma_task_t *)task, &task_name);
	ma_task_get_type((ma_task_t *)task, &task_type_);
	
	
	MA_LOG(logger, MA_LOG_SEV_INFO, "Executing Task id %s and count=%d", task_id);

    return MA_OK;
}

static ma_error_t on_task_stop(ma_context_t *context, const char *task_type, const char *product_id, ma_task_t *task, void *cb_data) {
    char *task_id = NULL;
	ma_error_t rc = MA_OK;
	ma_task_t *task_get = NULL;
	ma_task_time_t next_time={0};
	
	ma_task_get_id((ma_task_t *)task, &task_id);
	MA_LOG(logger, MA_LOG_SEV_INFO, "Stopping Task %s", task_id);

    return MA_OK;
}

static ma_error_t on_task_remove(ma_context_t *context, const char *task_type, const char *product_id, ma_task_t *task, void *cb_data) {
    char *task_id = NULL;
    ma_task_get_id((ma_task_t *)task, &task_id);
	MA_LOG(logger, MA_LOG_SEV_INFO, "Removing Task %s", task_id);
    return MA_OK;

}


static ma_error_t on_task_update_status(ma_context_t *context, const char *task_type, const char *product_id, ma_task_t *task, void *cb_data) {
    char *task_id = NULL;
    ma_task_get_id((ma_task_t *)task, &task_id);
	MA_LOG(logger, MA_LOG_SEV_INFO, "Updating Task %s status", task_id);
    return MA_OK;

}

/* Sample to show how to create run once task*/
bc_error_t bc_runnow_task_create(ma_context_t *ma_context) {
    if(ma_context) {
	    ma_error_t rc = MA_OK;
        ma_scheduler_task_callbacks_t cb = {&on_task_execute, &on_task_stop, &on_task_remove,&on_task_update_status};
        const char *task_type = "ma.beacon.runnow.add";
        const char *product_id = "BEACON__TEST";

	
		MA_LOG(logger, MA_LOG_SEV_INFO, "bc_runnow_task_create %d", ma_context);
        if(MA_OK == (rc = ma_scheduler_register_task_callbacks(ma_context, product_id, task_type, &cb, NULL))) {
            ma_task_t *task = NULL;
			ma_trigger_run_now_policy_t policy = {0};
            ma_trigger_t *trigger = NULL;
            ma_task_time_t start_date = {0};
	        ma_task_time_t end_date = {0};
            ma_variant_t *app_payload = NULL;
	        ma_task_repeat_policy_t repeat_policy = {0};
            const char *beacon_str = "beacon_runnow";
            const char *beacon_creator = "beacon";
  
	        (void)ma_context_get_logger(ma_context, &logger);

			/*Even Repeat policy is set for task since it has run now  tigger will not repeate*/
            repeat_policy.repeat_interval.minute = 2; //Every 2min repeat
            repeat_policy.repeat_until.minute = 5; //Repeat until 1 hrs

            if(MA_OK == (rc = ma_task_create("ma.beacon.runnow", &task))) { 
				MA_LOG(logger, MA_LOG_SEV_INFO, "Creating Task %s", task_type);
				if(MA_OK != (rc = ma_task_set_name(task, "BEACON_ADD_runnow"))) {
					MA_LOG(logger, MA_LOG_SEV_INFO, "beacon failed to set task name. error code(%s) error reason(%s)", rc, rc);
                }
				if(MA_OK != (rc = ma_task_set_type(task,task_type))) {
					MA_LOG(logger, MA_LOG_SEV_INFO, "beacon failed to set task type. error code(%s) error reason(%s)", rc, rc);
                }
                if(MA_OK == (rc = ma_task_set_software_id(task, product_id))) {
                    if(MA_OK != (rc = ma_variant_create_from_string(beacon_str, strlen(beacon_str), &app_payload))) {
                        MA_LOG(logger, MA_LOG_SEV_INFO, "beacon failed to create variant. error code(%s) error reason(%s)", rc, rc);
                    }
	                if(MA_OK != (rc = ma_task_set_setting(task, beacon_str, beacon_str, app_payload))) {
                        MA_LOG(logger, MA_LOG_SEV_INFO, "beacon failed to set task settings. error code(%s) error reason(%s)", rc, rc);
                    }
                    if(MA_OK != (rc = ma_task_set_creator_id(task, beacon_creator))) {
                        MA_LOG(logger, MA_LOG_SEV_INFO, "beacon failed to set task creator id.  error code(%s) error reason(%s)", rc, rc);
                    }
                    if(MA_OK != (rc = ma_task_set_repeat_policy(task, repeat_policy))) {
                        MA_LOG(logger, MA_LOG_SEV_INFO, "beacon failed to set task repeat policy. error code(%s) error reason(%s)", rc, rc);
                    }
					
					if(MA_OK != (rc = ma_task_set_max_run_time_limit(task, 1))) {
                        MA_LOG(logger, MA_LOG_SEV_INFO, "beacon failed to set max run time limit. error code(%s) error reason(%s)", rc, rc);
                    }
                    if(MA_OK == (rc = ma_trigger_create_run_now(&policy, &trigger))) {
						if(MA_OK == (rc = ma_task_add_trigger(task, trigger))) {
							if(MA_OK != (rc = ma_scheduler_add_task(ma_context, task))){
								MA_LOG(logger, MA_LOG_SEV_INFO, "scheduler add task failed. error code(%s) error reason(%s)", rc, rc);
	                        }
						}
                     }
                }
                (void)ma_task_release(task);
            }
        }else{
			MA_LOG(logger, MA_LOG_SEV_INFO, "Creating Task %s failed", task_type);
		}
		
	    return (bc_error_t)rc;
    }
    return (bc_error_t)MA_ERROR_INVALIDARG;
}



