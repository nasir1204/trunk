#include "bc/bc_datachannel.h"
#include "bc/bc_strdef.h"
#include "ma/datachannel/ma_datachannel.h"
#include "ma/ma_variant.h"

static void send_datachannel_item(ma_client_t *client, const char *product_id, const char *item_id) ;
extern ma_logger_t *beacon_logger;
void ma_datachannel_notification_cb(ma_client_t *client, const char *product_id, const char *message_id, ma_int64_t correlation_id, ma_datachannel_item_notification_t status, ma_error_t reason, void *cb_data) {
	 
    MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Beacon datachannel on notification callback invoked product id = %s, message id=%s, correlation id=%d, notification=%d.",
            product_id, message_id, correlation_id, status);
}

void ma_datachannel_subscriber_cb(ma_client_t *client, const char *product_id, const char *message_id, ma_int64_t correlation_id, ma_datachannel_item_t *dc_message, void *cb_data){
    MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Beacon datachannel on message callback invoked product id = %s, message id=%s, correlation id=%d.", product_id, message_id, correlation_id);
	if(0 == strcmp(product_id, BC_PRODUCT_ID_STR)) {
        ma_variant_t *variant = NULL;
		MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Received message from ePO. Item ID: [%s], Correlation ID:[%d].", message_id, correlation_id);
			
        if(MA_OK == ma_datachannel_item_get_payload(dc_message, &variant)) {
            /*Payload can be used here*/
            ma_variant_release(variant);
		}
        MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Processed %s", message_id);
    }
    else 
        MA_LOG(beacon_logger, MA_LOG_SEV_CRITICAL, "Unable to process", message_id);
}

static void send_datachannel_item(ma_client_t *client, const char *product_id, const char *item_id) {
	ma_datachannel_item_t *item = NULL;
	ma_variant_t *variant = NULL;
	if(MA_OK == ma_variant_create_from_raw(BC_DC_DATA, strlen(BC_DC_DATA), &variant)) {
		if(MA_OK == ma_datachannel_item_create(item_id, variant, &item)) {
			if(MA_OK == ma_datachannel_item_set_option(item, MA_DATACHANNEL_PERSIST,MA_TRUE) && 
				MA_OK == ma_datachannel_item_set_option(item, MA_DATACHANNEL_DELIVERY_NOTIFICATION,MA_TRUE) && 
				MA_OK == ma_datachannel_item_set_option(item, MA_DATACHANNEL_PURGED_NOTIFICATION,MA_TRUE) &&
				MA_OK == ma_datachannel_item_set_option(item, MA_DATACHANNEL_ENCRYPT,MA_TRUE) &&
				MA_OK == ma_datachannel_item_set_ttl(item, 5000)) {
					ma_datachannel_async_send(client, product_id, item);
					MA_LOG(beacon_logger, MA_LOG_SEV_INFO, "Sent DC item to datachannel service");
			}
			ma_datachannel_item_release(item);
		}
		ma_variant_release(variant);
	}
}
bc_error_t bc_datachannel_intialize(ma_client_t *client) {
	ma_error_t rc = MA_OK;
	if(client) {
		/*First register the subscriber callback and notification callback(if you are interested in notifications of sent DC items) and any data that you wish to receive when your callbacks are called. */
		if(MA_OK == (rc = ma_datachannel_register_notification_callback(client, BC_PRODUCT_ID_STR, ma_datachannel_notification_cb, NULL))) {
            if(MA_OK == (rc = ma_datachannel_register_message_handler_callback(client, BC_PRODUCT_ID_STR, ma_datachannel_subscriber_cb, NULL))) {
			    /*Subscribe to a DC item*/
			    if(MA_OK == (rc = ma_datachannel_subscribe(client, BC_PRODUCT_ID_STR, BC_DC_ITEM1, MA_DC_SUBSCRIBER_LISTENER))) {
				    /*Now send a DC item to DCTestMsg(BC_DC_ITEM1)*/
				    send_datachannel_item(client, BC_PRODUCT_ID_STR, BC_DC_ITEM1);
				    return BC_OK;
			    }
                ma_datachannel_unregister_message_handler_callback(client, BC_PRODUCT_ID_STR);
            }
            ma_datachannel_unregister_notification_callback(client, BC_PRODUCT_ID_STR);
		}
	}
	return BC_ERROR_INVALIDARG;
}

bc_error_t bc_datachannel_deinitalize(ma_client_t *client) {
	ma_datachannel_unsubscribe(client, BC_PRODUCT_ID_STR, BC_DC_ITEM1);
	ma_datachannel_unregister_message_handler_callback(client, BC_PRODUCT_ID_STR);
    ma_datachannel_unregister_notification_callback(client, BC_PRODUCT_ID_STR);
	return BC_OK;
}

