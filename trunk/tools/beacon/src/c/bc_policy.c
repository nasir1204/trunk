#include "bc/bc_policy.h"
#include "bc/bc_strdef.h"

#include "ma/policy/ma_policy.h"
#include "ma/crypto/ma_crypto.h"

#include "bc/bc_event.h"
#include <string.h>
#include <stdio.h>

extern ma_logger_t *beacon_logger;
#define POLICY_CLIENT_EVENT_ID 11000

// File for storing policy information
#define FNAME "policies_c.txt"

static ma_error_t bc_policy_notification_cb(ma_client_t *ma_client, const char *product_id, const ma_policy_uri_t *info_uri, void *cb_data);

bc_error_t bc_policy_intialize(ma_client_t *ma_client) {
    ma_error_t rc = MA_OK;
    ma_uint32_t notify_type = MA_POLICY_TIMEOUT | MA_POLICY_CHANGED | MA_USER_CHANGED | MA_LOCATION_CHANGED;
   
    /* Registering as policy consumer,
        - product id can be passed as NULL if we are trying to register for the same product id as in context.
        - can be called multiple times with different product id's if we want to register for multiple product's as policy consumer.
    */
    if(MA_OK != (rc = ma_policy_register_notification_callback(ma_client, BC_PRODUCT_ID_STR, notify_type, bc_policy_notification_cb, NULL))) {
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Failed to register beacon  for policy notification with MA ,%d", rc);
		bc_event_generate(ma_client, "BC_POLICY_NOTIFICATION_REGISTRATION_FAILED", POLICY_CLIENT_EVENT_ID+1, MA_EVENT_SEVERITY_CRITICAL);
        return BC_POLICY_NOTIFICATION_REGISTRATION_FAILED;
    }
	else
		bc_event_generate(ma_client, "BC_POLICY_NOTIFICATION_REGISTRATION_SUCCESS", POLICY_CLIENT_EVENT_ID+2, MA_EVENT_SEVERITY_INFORMATIONAL);
    MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Registered succesfully beacon for policy notification with MA");

    return BC_OK;
}

bc_error_t bc_policy_deinitalize(ma_client_t *ma_client) {
    ma_error_t rc = MA_OK;
    /* UnRegistering as policy consumer,
        - product id can be passed as NULL if we are trying to unregister for the same product id as in context.
        - can be called multiple times with product id's if we have registered with different product id's as policy consumer.
    */
    if(MA_OK != (rc = ma_policy_unregister_notification_callback(ma_client, BC_PRODUCT_ID_STR))){
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Failed to un-register beacon for policy notification with MA ,%d", rc); 
		bc_event_generate(ma_client, "BC_POLICY_NOTIFICATION_UNREGISTRATION_FAILED", POLICY_CLIENT_EVENT_ID+3, MA_EVENT_SEVERITY_CRITICAL);
        return BC_POLICY_NOTIFICATION_UNREGISTRATION_FAILED;
    }
	else
		bc_event_generate(ma_client, "BC_POLICY_NOTIFICATION_UNREGISTRATION_SUCCESS", POLICY_CLIENT_EVENT_ID+4, MA_EVENT_SEVERITY_INFORMATIONAL);

    MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Unregistered succesfully beacon for policy notification with MA");
    return BC_OK;
}


static ma_error_t bc_policy_notification_cb(ma_client_t *ma_client, const char *product_id, const ma_policy_uri_t *info_uri, void *cb_data) {
     
     const char *section_name = NULL;
     const char *key_name = NULL;
     ma_policy_bag_t *policy_bag = NULL;
     ma_policy_bag_iterator_t *it = NULL;
     const ma_variant_t *variant_value = NULL;
     const ma_policy_uri_t *uri = NULL;   
     ma_policy_notification_type_t notification_type;
	 FILE *fp = NULL;
	 ma_error_t rc = MA_OK;

    ma_policy_uri_get_notification_type(info_uri, &notification_type);
    MA_LOG(beacon_logger, MA_LOG_SEV_INFO, "Beacon policy notification callback invoked for reason %d",notification_type);

	bc_event_generate(ma_client, "BC_POLICY_NOTIFICATION", POLICY_CLIENT_EVENT_ID+5, MA_EVENT_SEVERITY_INFORMATIONAL);

    if(MA_OK != ma_policy_get(ma_client, info_uri, &policy_bag)) {
        MA_LOG(beacon_logger, MA_LOG_SEV_INFO, "Failed to get the policies for beacon");
		bc_event_generate(ma_client, "BC_POLICY_GET_FAILED", POLICY_CLIENT_EVENT_ID+6, MA_EVENT_SEVERITY_MINOR);
        return MA_OK;
    }
    /* To get one specific section and key */

    /* For logging policies (contents of policy bag) into a file */
	fp = fopen(FNAME, "a"); // Open in append mode, so that policy bags from multiple iterations can be stored.

	fprintf(fp, "-----------------------------Policy Enforcement Start--------------------------------------- \n");	
    /* To iterate through all of the policies*/
    if(MA_OK == ma_policy_bag_get_iterator(policy_bag, &it)) {
        while(MA_ERROR_NO_MORE_ITEMS != (rc = ma_policy_bag_iterator_get_next(it, &uri, &section_name, &key_name, &variant_value))) {
            const char *feature = NULL, *category = NULL, *type = NULL, *name = NULL;
            ma_buffer_t *buffer = NULL;
            const char *key_value = NULL;
            size_t size = 0;          
		
			if(MA_OK != rc) {
				fprintf(fp, "-----------------------------Policy Enforcement Failed--------------------------------------- \n");						
				break;
			}

			ma_policy_uri_get_feature(uri, &feature); ma_policy_uri_get_category(uri, &category);
            ma_policy_uri_get_type(uri, &type); ma_policy_uri_get_name(uri, &name);

           // MA_LOG(logger, MA_LOG_SEV_TRACE, "feature=%s, category=%s, type=%s, name=%s",feature, category, type,name);	
			fprintf(fp, "Feature -> %s \t Category -> %s \t Type -> %s \t Name -> %s \n", feature, category, type, name);

            ma_variant_get_string_buffer((ma_variant_t*)variant_value, &buffer);
            ma_buffer_get_string(buffer, &key_value, &size); 
            if(!strcmp(key_name, "szAdminOverridePassword") && 0 < strlen(key_value)) {
                ma_buffer_t *dec_buffer = NULL;
                ma_crypto_decrypt_data_by_type(ma_client, MA_CRYPTO_ALGO_TYPE_POLICY_AES128_STR, (const unsigned char *)key_value, size, &dec_buffer);
                if(dec_buffer) {
                    const char *pwd = NULL;
                    size_t pwd_len = 0;
                    if(MA_OK == ma_buffer_get_string(dec_buffer, &pwd, &pwd_len))
					{
                        //MA_LOG(logger, MA_LOG_SEV_TRACE, "section=%s, key=%s, value=%s",section_name, key_name, pwd);
					    fprintf(fp, "Section = %s, Key = %s, Value = %s\n", section_name, key_name, pwd);
					}
                    ma_buffer_release(dec_buffer);
                }
            }
            else {
               // MA_LOG(logger, MA_LOG_SEV_TRACE, "section=%s, key=%s, value=%s", section_name, key_name, key_value);
			    fprintf(fp, "Section = %s, Key = %s, Value = %s\n", section_name, key_name, key_value);
		    }

            ma_buffer_release(buffer);
        }
    }
	else
		bc_event_generate(ma_client, "BC_POLICY_BAG_GET_IT_FAILED", POLICY_CLIENT_EVENT_ID+6, MA_EVENT_SEVERITY_MAJOR);

	// Close file handle
	fprintf(fp, "-----------------------------Policy Enforcement End--------------------------------------- \n");		
	fclose(fp);

    return MA_OK;
}
