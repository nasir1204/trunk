#include "bc/bc_property.h"
#include "bc/bc_strdef.h"
#include "bc/bc_event.h"

#include "ma/property/ma_property.h"

#include <string.h>

#define PROPERTY_CLIENT_EVENT_ID 10000
extern ma_logger_t *beacon_logger;
static ma_error_t bc_property_provider_cb(ma_client_t *ma_client,const char *product_id, ma_property_bag_t *property_bag, void *cb_data);

bc_error_t bc_property_intialize(ma_client_t *ma_client) {
    ma_error_t rc = MA_OK;
   
    /* Registering as property provider,
        - product id can be passed as NULL if we are trying to register for the same product id as in context.
        - can be called multiple times with different product id's if we want to register for multiple product's as property provider.
    */
    if(MA_OK != (rc = ma_property_register_provider_callback(ma_client, BC_PRODUCT_ID_STR, bc_property_provider_cb, NULL))) {
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Failed to register beacon as property provider with MA ,%d", rc); 
		bc_event_generate(ma_client, "BC_PROPERTY_PROVIDER_REGISTRATION_FAILED", PROPERTY_CLIENT_EVENT_ID+1, MA_EVENT_SEVERITY_CRITICAL);
        return BC_PROPERTY_PROVIDER_REGISTRATION_FAILED;
    }
	else
		bc_event_generate(ma_client, "BC_PROPERTY_PROVIDER_REGISTRATION_SUCCESS", PROPERTY_CLIENT_EVENT_ID+2, MA_EVENT_SEVERITY_INFORMATIONAL);
	
    MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Registered successfully beacon as property provider with MA");

    return BC_OK;
}

bc_error_t bc_property_deinitalize(ma_client_t *ma_client) {
    ma_error_t rc = MA_OK;
    
    /* UnRegistering as property provider,        - product id can be passed as NULL if we are trying to unregister for the same product id as in context.
        - can be called multiple times with product id's if we have registered with different product id's as peroperty provider.
    */
    if(MA_OK != (rc = ma_property_unregister_provider_callback(ma_client, BC_PRODUCT_ID_STR))){
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Failed to unregister beacon as property provider with MA ,%d", rc); 
		bc_event_generate(ma_client, "BC_PROPERTY_PROVIDER_UNREGISTRATION_FAILED", PROPERTY_CLIENT_EVENT_ID+3, MA_EVENT_SEVERITY_CRITICAL);
        return BC_PROPERTY_PROVIDER_UNREGISTRATION_FAILED;
    }
	else
		bc_event_generate(ma_client, "BC_PROPERTY_PROVIDER_UNREGISTRATION_SUCCESS", PROPERTY_CLIENT_EVENT_ID+4, MA_EVENT_SEVERITY_INFORMATIONAL);

    MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Unregistered succesfully beacon as property provider with MA");
    return BC_OK;
}



static ma_error_t bc_property_provider_cb(ma_client_t *ma_client,const char *product_id, ma_property_bag_t *property_bag, void *cb_data){
     ma_variant_t *variant = NULL;
    
    MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Beacon prodvider callback invoked for product id %s", product_id);

	bc_event_generate(ma_client, "BC_PROPERTY_PROVIDER_CB", PROPERTY_CLIENT_EVENT_ID+5, MA_EVENT_SEVERITY_INFORMATIONAL);

    /*adding product version as STRING */
    ma_variant_create_from_string(BC_PROP_VALUE_PRODUCT_VERSION_STR, strlen(BC_PROP_VALUE_PRODUCT_VERSION_STR), &variant);
    ma_property_bag_add_property(property_bag, BC_PROP_PATH_GENERAL_STR, BC_PROP_KEY_PRODUCT_VERSION_STR, variant);
    ma_variant_release(variant);

    /*adding product name as STRING */
    ma_variant_create_from_string(BC_PROP_VALUE_PRODUCT_NAME_STR, strlen(BC_PROP_VALUE_PRODUCT_NAME_STR), &variant);
    ma_property_bag_add_property(property_bag, BC_PROP_PATH_GENERAL_STR, BC_PROP_KEY_PRODUCT_NAME_STR, variant);
    ma_variant_release(variant);

    /*adding Showlisting Enabled STRING */
    ma_variant_create_from_string(BC_PROP_VALUE_SHOWLIST_ENABLED_STR, strlen(BC_PROP_VALUE_SHOWLIST_ENABLED_STR), &variant);
    ma_property_bag_add_property(property_bag, BC_PROP_PATH_SHOWLIST_STR, BC_PROP_KEY_SHOWLISTING_ENABLED_STR, variant);
    ma_variant_release(variant);

    /*adding Showlisting list STRING */
    ma_variant_create_from_string(BC_PROP_VALUE_SHOWLISTING_LIST_STR, strlen(BC_PROP_VALUE_SHOWLISTING_LIST_STR), &variant);
    ma_property_bag_add_property(property_bag, BC_PROP_PATH_SHOWLIST_STR, BC_PROP_KEY_SHOWLISTING_LIST_STR, variant);
    ma_variant_release(variant);

    /*FUTURE- API's START adding whitelisting enabled as BOOL */
    /*ma_variant_create_from_bool(MA_TRUE, &variant);
    ma_property_bag_add_property(property_bag, BC_PROP_PATH_SHOWLIST_STR, BC_PROP_KEY_SHOWLISTING_ENABLED_BOOL, variant);
    ma_variant_release(variant);*/

    /*FUTURE-adding whitelisted  list as ARRAY*/
    /*create_list_of_whitelisted_applications(&array);
    ma_variant_create_from_array(array,&variant);
    ma_property_bag_add_property(property_bag, BC_PROP_PATH_SHOWLIST_STR, BC_PROP_KEY_SHOWLISTING_LIST_ARRAY, variant);
    ma_variant_release(variant);*/
    /*FUTURE- API's END adding whitelisting enabled as BOOL */

    MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Beacon prodvider for product id %s provider properties.", product_id);
    return MA_OK;
}

