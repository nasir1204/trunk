#include "bc/msgbus/bc_subscriber.h"

#include "ma/ma_msgbus.h"

extern ma_logger_t *beacon_logger;

static ma_msgbus_subscriber_t *g_logger_subscriber = NULL;

static ma_error_t logger_subscriber_cb(const char *topic, ma_message_t *msg, void *cb_data);

bc_error_t bc_subscriber_intialize(ma_client_t *ma_client) {
    ma_error_t rc = MA_OK;
	ma_msgbus_t *msgbus = NULL;
	if(MA_OK == (rc = ma_client_get_msgbus(ma_client, &msgbus))) {
		if(MA_OK == (rc = ma_msgbus_subscriber_create(msgbus, &g_logger_subscriber))) {
			(void)ma_msgbus_subscriber_setopt(g_logger_subscriber, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
			(void)ma_msgbus_subscriber_setopt(g_logger_subscriber, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_POOL);
			if(MA_OK == (rc = ma_msgbus_subscriber_register(g_logger_subscriber, MA_LOGGER_MSG_PUB_TOPIC, logger_subscriber_cb, ma_client))) {
				return BC_OK;
			}else 				
				MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Failed to register subscriber,%d", rc);
		}else
			MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Failed to create subscriber ,%d", rc);
	}else
		MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Failed to get msgbus from client,%d", rc);
	return BC_SUBSCRIBER_INIT_FAILED;
}

bc_error_t bc_subscriber_deintialize(ma_client_t *ma_client) {
    
	if(g_logger_subscriber) {
		(void)ma_msgbus_subscriber_unregister(g_logger_subscriber);
		(void)ma_msgbus_subscriber_release(g_logger_subscriber);
	}
    return BC_OK;
}

static ma_error_t logger_subscriber_cb(const char *topic, ma_message_t *msg, void *cb_data) {
	
	if(topic && msg && cb_data) {
		ma_message_auth_info_t *auth_info = NULL;
		ma_error_t rc = MA_OK;

		MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "logger subscriber callback invoked topic %s", topic);
		if(MA_OK == (rc = ma_message_get_auth_info(msg, &auth_info))) {
			const char *issuer_name = NULL, *subject_name = NULL; 
			long pid = -1;

			(void)ma_message_auth_info_get_verified_signer(auth_info, &issuer_name);
			(void)ma_message_auth_info_get_verified_publisher(auth_info, &subject_name);
			(void)ma_message_auth_info_get_pid(auth_info, &pid);

			MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "issuer name = %s", issuer_name);
			MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "subject name = %s", subject_name);
			MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "pid  = %ld", pid);

			ma_message_auth_info_release(auth_info);
		}else
			MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "failed to get auth info, %d", rc);
	}
    return MA_OK;
}

