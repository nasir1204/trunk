#include "bc/bc_updater.h"
#include "bc/bc_strdef.h"
#include "ma/updater/ma_updater.h"
#include "ma/updater/ma_updater_update_request.h"
#include <string.h>

extern ma_logger_t *beacon_logger;
/* updater callbacks */
static ma_error_t updater_notify_cb(ma_client_t *ma_client, const char *product_id, void *cb_data, const char *update_type, ma_update_state_t state, const char *message, const char *extra_info, ma_updater_product_return_code_t *return_code);
static ma_error_t updater_set_cb(ma_client_t *ma_client, const char *product_id, void *cb_data, const char *update_type, const char *key, ma_variant_t *value, ma_updater_product_return_code_t *return_code);
static ma_error_t updater_get_cb(ma_client_t *ma_client, const char *product_id, void *cb_data, const char *update_type, const char *key, ma_variant_t **value, ma_updater_product_return_code_t *return_code);
static ma_error_t updater_update_event_cb(ma_client_t *ma_client, const char *product_id, void *cb_data, const ma_updater_event_t *event_info) ;

static ma_updater_callbacks_t updater_cbs;

static void run_update_now(ma_client_t *ma_client);

bc_error_t bc_updater_intialize(ma_client_t *ma_client) {
    ma_error_t rc = MA_OK;
    
    updater_cbs.notify_cb = updater_notify_cb ;
    updater_cbs.get_cb = updater_get_cb ;
    updater_cbs.set_cb = updater_set_cb ;
	updater_cbs.update_event_cb = updater_update_event_cb;

    if(MA_OK == (rc = ma_updater_register_callbacks(ma_client, BC_PRODUCT_ID_STR, &updater_cbs, NULL))) {
        MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Registered beacon updater callbacks with MA") ; 
        run_update_now(ma_client);
    }
    else {
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Failed to register beacon updater callbacks with MA ,%d", rc) ; 
        return BC_UPDATER_CALLBACKS_REGISTRATION_FAILED ;
    }
    return BC_OK ;
}

bc_error_t bc_updater_deinitalize(ma_client_t *ma_client) {
    (void)ma_updater_unregister_callbacks(ma_client, BC_PRODUCT_ID_STR) ;

	MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Unregistered succesfully beacon  updater callbacks with MA");
    
	return BC_OK ;
}

/* updater callbacks */
static ma_error_t updater_notify_cb(ma_client_t *ma_client, const char *product_id, void *cb_data, const char *update_type, ma_update_state_t update_state, const char *message, const char *extra_info, ma_updater_product_return_code_t *return_code){
     MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Beacon noitfy invoked product = %s, update_type = %s, update_state = %d.", product_id, update_type, update_state) ;
    *return_code = MA_UPDATER_PRODUCT_RETURN_CODE_OK ;
    return MA_OK ;
}

static ma_error_t updater_set_cb(ma_client_t *ma_client, const char *product_id, void *cb_data, const char *update_type, const char *key, ma_variant_t *value, ma_updater_product_return_code_t *return_code){
     MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Beacon updater_set_cb invoked for update (%s) key(%s)", update_type, key) ;
    *return_code = MA_UPDATER_PRODUCT_RETURN_CODE_OK ;
    return MA_OK ;
}

static ma_error_t updater_get_cb(ma_client_t *ma_client, const char *product_id, void *cb_data, const char *update_type, const char *key, ma_variant_t **value, ma_updater_product_return_code_t *return_code) {
    MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Beacon updater_get_cb invoked for update (%s) key(%s)", update_type, key) ;

    *return_code = MA_UPDATER_PRODUCT_RETURN_CODE_OK ;
    return MA_OK ;
}

static ma_error_t updater_update_event_cb(ma_client_t *ma_client, const char *product_id, void *cb_data, const ma_updater_event_t *event_info) {
    MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Beacon event_cb invoked product = %s.", product_id);
	return MA_OK;
}


static void run_update_now(ma_client_t *ma_client) {
    ma_error_t rc = MA_OK;
    ma_updater_update_request_t *request = NULL;
	if(MA_OK == (rc = ma_updater_update_request_create(MA_UPDATER_UPDATE_TYPE_NORMAL_UPDATE, &request))) {
        ma_buffer_t *session_id = NULL;
        if(MA_OK == (rc = ma_updater_start_update(ma_client, request,  &session_id))) {
            MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Requested done for on demand update.");
            ma_buffer_release(session_id);
        }
        else 
            MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Failed to send request for on demand update due to  %d", rc);
        ma_updater_update_request_release(request);
    }
    else
        MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Failed to create request due to %d.",rc);
 }

