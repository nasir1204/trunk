#include "bc/bc_event.h"
#include "bc/bc_strdef.h"
#include "ma/ma_log.h"
#include "bc/bc_custom_event.h"

#define SAMPLE_THREAT_ID1 1027
#define SAMPLE_THREAT_ID2 777
extern ma_logger_t *beacon_logger;

static ma_error_t bc_cef_event_bag_cb(ma_client_t *ma_client, const char *event_product_id, ma_event_bag_t *event_bag, void *cb_data){
	
	MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "CEF event bag received from product %s", event_product_id);

	if(event_bag)
	{
		ma_error_t rc = MA_OK;
		size_t size = 0;

		const char *str = NULL;

		if(MA_OK == (rc = ma_event_bag_get_product_name(event_bag, &str)))
			MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "CEF event bag product name %s", str);
		else
			MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "CEF event bag without product name, error = %d", rc);

		str = NULL;
		if(MA_OK == (rc = ma_event_bag_get_product_version(event_bag, &str)))
			MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "CEF event bag product version %s", str);
		else
			MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "CEF event bag without product version, error = %d", rc);

		str = NULL;
		if(MA_OK == (rc = ma_event_bag_get_product_family(event_bag, &str)))
			MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "CEF event bag product family %s", str);
		else
			MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "CEF event bag without product family, error = %d", rc);


		if(MA_OK == (rc = ma_event_bag_get_event_count(event_bag, &size))){
			size_t index = 0;
			MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "CEF event bag has %d event", size);

			for(index = 0; index < size; index++){
				ma_event_t *event = NULL;

				MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "CEF event bag event at %d", index);

				if(MA_OK == ma_event_bag_get_event(event_bag, index, &event)){
					ma_uint32_t id = 0;
					ma_event_severity_t severity;

					if(MA_OK == ma_event_get_id(event, &id)){
						MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "CEF event id %d ", id);
					}
					else
						MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "CEF event without id");

					if(MA_OK == ma_event_get_severity(event, &severity)){
						MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "CEF event severity %d ", severity);
					}
					else
						MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "CEF event without severity");

					ma_event_release(event);
				}
				else
					MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "CEF event bag event at %d not found", index);
			}
		}
		else
			MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "CEF event bag received without event, err = %d ", rc);
	}

	return MA_OK;
}

static ma_error_t bc_custom_event_bag_cb(ma_client_t *ma_client, const char *event_product_id, ma_custom_event_t *event_bag, void *cb_data){

	MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "CUSTOM event received from product %s", event_product_id);

	return MA_OK;
}

bc_error_t bc_event_intialize(ma_client_t *ma_client){
	ma_error_t rc = MA_OK;
   static ma_event_callbacks_t cbs;
	cbs.cef_event_bag_cb = &bc_cef_event_bag_cb;
	cbs.custom_event_bag_cb = &bc_custom_event_bag_cb;

    
    /* Registering as policy consumer,
        - product id can be passed as NULL if we are trying to register for the same product id as in context.
        - can be called multiple times with different product id's if we want to register for multiple product's as policy consumer.
    */
    if(MA_OK != (rc = ma_event_register_callbacks(ma_client, &cbs, NULL))) {
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Failed to register beacon  for event notification with MA ,%d", rc);
		return BC_EVENT_NOTIFICATION_REGISTRATION_FAILED;
    }
	else		
		MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Registered succesfully beacon for event notification with MA");

	bc_custom_event_generate(ma_client, SAMPLE_THREAT_ID1, MA_EVENT_SEVERITY_INFORMATIONAL);
	bc_custom_event_generate(ma_client, SAMPLE_THREAT_ID2, MA_EVENT_SEVERITY_CRITICAL);
	

    return BC_OK;
}

bc_error_t bc_event_deintialize(ma_client_t *ma_client){
	ma_error_t rc = MA_OK;
    /* UnRegistering as policy consumer,
        - product id can be passed as NULL if we are trying to unregister for the same product id as in context.
        - can be called multiple times with product id's if we have registered with different product id's as policy consumer.
    */
    if(MA_OK != (rc = ma_event_unregister_callbacks(ma_client))){
        MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Failed to un-register beacon for event notification with MA ,%d", rc); 		
        return BC_EVENT_NOTIFICATION_UNREGISTRATION_FAILED;
    }
	else
		MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Unregistered succesfully beacon for event notification with MA");
    return BC_OK;
}

bc_error_t bc_event_generate(ma_client_t *ma_client, const char *event_name, ma_uint32_t event_id, ma_event_severity_t severity){
	ma_error_t rc = MA_OK;
	ma_event_bag_t *event_bag = NULL;
	  
	/*Create event bag.*/
	rc = ma_event_bag_create(ma_client, "Beacon Sample Point Product", &event_bag);

	/*Set product version and family.*/
	if (MA_OK == rc){
		rc = ma_event_bag_set_product_version(event_bag, BC_PROP_VALUE_PRODUCT_VERSION_STR);
	}
	if (MA_OK == rc){
		rc = ma_event_bag_set_product_family(event_bag, "TVD");
	}

	/**Set product common fileds.*/
	if (MA_OK == rc){
		ma_variant_t *var = NULL;
		if (MA_OK == rc) rc = ma_variant_create_from_string(BC_PRODUCT_ID_STR, strlen(BC_PRODUCT_ID_STR), &var);
		if (MA_OK == rc){
			rc = ma_event_bag_add_common_field(event_bag, MA_EVENT_COMMON_FILED_KEY_Analyzer, var);
		}
		ma_variant_release(var);
	}

	if (MA_OK == rc){
		ma_variant_t *var = NULL;
		if (MA_OK == rc) rc = ma_variant_create_from_string(BC_PRODUCT_NAME_STR, strlen(BC_PRODUCT_NAME_STR), &var);
		if (MA_OK == rc){
			rc = ma_event_bag_add_common_field(event_bag, MA_EVENT_COMMON_FILED_KEY_AnalyzerName, var);
		}
		ma_variant_release(var);
	}

	if (MA_OK == rc){
		ma_variant_t *var = NULL;
		if (MA_OK == rc) rc = ma_variant_create_from_string(BC_PROP_VALUE_PRODUCT_VERSION_STR, strlen(BC_PROP_VALUE_PRODUCT_VERSION_STR), &var);
		if (MA_OK == rc){
			rc = ma_event_bag_add_common_field(event_bag, MA_EVENT_COMMON_FILED_KEY_AnalyzerVersion, var);
		}
		ma_variant_release(var);
	}
	
	/*Create and send event.*/
	if (MA_OK == rc){
		ma_event_t *event_obj = NULL;
		if (MA_OK == rc){
			rc = ma_event_create(ma_client, event_id, severity, &event_obj);
		}
			
		/*add common fields.*/
		if (MA_OK == rc){
			ma_variant_t *var = NULL;
			if (MA_OK == rc) rc = ma_variant_create_from_string("beacon_t_catagory", strlen("avbeacon_t_catagory"), &var);
			if (MA_OK == rc){
				rc = ma_event_add_common_field(event_obj, MA_EVENT_COMMON_FILED_KEY_ThreatCategory, var);
			}
			ma_variant_release(var);
		}
		if (MA_OK == rc){
			ma_variant_t *var = NULL;
			if (MA_OK == rc) rc = ma_variant_create_from_string(event_name, strlen(event_name), &var);
			if (MA_OK == rc){
				rc = ma_event_add_common_field(event_obj, MA_EVENT_COMMON_FILED_KEY_ThreatName, var);
			}
			ma_variant_release(var);
		}

		if (MA_OK == rc){
			ma_variant_t *var = NULL;
			if (MA_OK == rc) rc = ma_variant_create_from_string("beacon_type", strlen("beacon_type"), &var);
			if (MA_OK == rc){
				rc = ma_event_add_common_field(event_obj, MA_EVENT_COMMON_FILED_KEY_ThreatType, var);
			}
			ma_variant_release(var);
		}
		if (MA_OK == rc){
			rc = ma_event_bag_add_event(event_bag, event_obj);
		}
		ma_event_release(event_obj);
    }

	/*Send event bag.*/
    if (MA_OK == rc){
		if ( MA_OK == (rc = ma_event_bag_send(event_bag)))
			MA_LOG(beacon_logger, MA_LOG_SEV_DEBUG, "Event bag sent to agent.");		
	}
	else
		MA_LOG(beacon_logger, MA_LOG_SEV_ERROR, "Event generate failed. event api error = %d.", rc);

	ma_event_bag_release(event_bag);
	/*retrun the event bag result.*/
	return MA_OK == rc ? BC_OK : BC_EVENT_GENERATE_FIELD;
}
