#ifndef BC_POLICY_HXX_INCLUDED
#define BC_POLICY_HXX_INCLUDED

#include "bc/bc_common.hxx"

#include "ma/policies/policy_notification_handler.hxx"

namespace mileaccess { namespace bc {

struct policy_notification_handler : mileaccess::ma::policies::policy_notification_handler {
    // Note throws exceptions if it fails.
    policy_notification_handler(mileaccess::ma::client const &cli);
    // Note throws exceptions if it fails.
    virtual ~policy_notification_handler();

    virtual ma_error_t on_policy_notification(mileaccess::ma::client const &cli, std::string const &product_id, mileaccess::ma::policies::policy_uri const &info_uri);

    mileaccess::ma::client const &m_cli;

};

}} /* namespace mileaccess : namespace bc*/


#endif /* BC_POLICY_HXX_INCLUDED */
