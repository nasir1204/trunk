#ifndef BC_TASK_HANDLER_HXX_INCLUDED
#define BC_TASK_HANDLER_HXX_INCLUDED

#include "bc/bc_common.hxx"

#include "ma/scheduler/task_handler.hxx"

namespace mileaccess { namespace bc {

struct task_handler : mileaccess::ma::scheduler::task_handler {

    // Note throws exceptions if it fails.
    task_handler(mileaccess::ma::client const &cli);
    // Note throws exceptions if it fails.
    virtual ~task_handler();

    virtual ::ma_error_t on_execute(mileaccess::ma::client  const &cli, std::string const &product_id, std::string const &task_type, mileaccess::ma::scheduler::task &t);

    virtual ::ma_error_t on_stop(mileaccess::ma::client  const &cli, std::string const &product_id, std::string const &task_type, mileaccess::ma::scheduler::task &t);

    virtual ::ma_error_t on_remove(mileaccess::ma::client  const &cli, std::string const &product_id, std::string const &task_type, mileaccess::ma::scheduler::task &t);

    virtual ::ma_error_t on_update_status(mileaccess::ma::client  const &cli, std::string const &product_id, std::string const &task_type, mileaccess::ma::scheduler::task &t);

    void    handle_beacon_event_task(mileaccess::ma::scheduler::task &t);

    void    handle_beacon_long_running_task(mileaccess::ma::scheduler::task &t);

    mileaccess::ma::client const &m_cli;

};

}} /* namespace mileaccess : namespace bc*/


#endif /* BC_TASK_HANDLER_HXX_INCLUDED */
