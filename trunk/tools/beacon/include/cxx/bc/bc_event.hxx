#ifndef BC_EVENT_HXX_INCLUDED
#define BC_EVENT_HXX_INCLUDED

#include "bc/bc_common.hxx"

#include "ma/events/event_client.hxx"

namespace mileaccess { namespace bc {

struct bc_event_subscriber : mileaccess::ma::events::event_client {
    // Note throws exceptions if it fails.
    bc_event_subscriber(mileaccess::ma::client const &cli);
    // Note throws exceptions if it fails.
    virtual ~bc_event_subscriber();

    ma_error_t on_cef_event(mileaccess::ma::client const &cli, std::string const &product_id, mileaccess::ma::events::event_bag const &bag);

	ma_error_t on_custom_event(mileaccess::ma::client const &cli, std::string const &product_id, mileaccess::ma::events::custom_event const &bag);
};

void bc_event_generate(mileaccess::ma::client const &cli, std::string event_name, ma_uint32_t event_id, ma_event_severity_t severity);

}} /* namespace mileaccess : namespace bc*/


#endif /* BC_EVENT_HXX_INCLUDED */
