#ifndef BC_ERRORS_HXX_INCLUDED
#define BC_ERRORS_HXX_INCLUDED

typedef enum bc_error_e {
    BC_ERROR_BASE                   = 0 ,
    BC_COMMON_BASE                  = BC_ERROR_BASE + 0,          /* All common error codes */
    BC_OK                           = BC_COMMON_BASE + 0,         /* 0 */
    BC_NOERROR                      = BC_OK,                      /* 0 */
    BC_ERROR_INVALIDARG             =1,
    BC_ERROR_OUTOFMEMORY,
    BC_CREATION_FAILED,
    BC_START_FAILED,
    BC_PROPERTY_PROVIDER_REGISTRATION_FAILED,
    BC_PROPERTY_PROVIDER_UNREGISTRATION_FAILED,
    BC_POLICY_NOTIFICATION_REGISTRATION_FAILED,
    BC_POLICY_NOTIFICATION_UNREGISTRATION_FAILED,
    BC_POLICY_FETCH_FAILED,
    BC_TASK_CALLBACKS_REGISTRATION_FAILED,
    BC_TASK_CALLBACKS_UNREGISTRATION_FAILED,
    BC_EVENT_GENERATE_FIELD
}bc_error_t;	


#endif /* BC_ERRORS_HXX_INCLUDED */
