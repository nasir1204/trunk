#ifndef BC_INFO_HXX_INCLUDED
#define BC_INFO_HXX_INCLUDED

#include "bc/bc_common.hxx"

#include "ma/info/info.hxx"

namespace mileaccess { namespace bc {

struct  info_event_handler : mileaccess::ma::info::info_event_handler {
    // Note throws exceptions if it fails.
    info_event_handler(mileaccess::ma::client const &c) ;
    // Note throws exceptions if it fails.
    virtual ~info_event_handler() ;

    virtual ma_error_t on_info_event(mileaccess::ma::client const &cli, std::string const &product_id, std::string const & info_event) ;
    
    mileaccess::ma::client const &m_cli ;

};

}} /* namespace mileaccess : namespace bc*/


#endif /* BC_INFO_HXX_INCLUDED */

