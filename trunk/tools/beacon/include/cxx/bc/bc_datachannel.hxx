#ifndef BC_DATACHANNEL_HXX_INCLUDED
#define BC_DATACHANNEL_HXX_INCLUDED

#include "bc/bc_common.hxx"
#include "ma/datachannel/datachannel_handler.hxx"
#include "ma/datachannel/item.hxx"

namespace mileaccess { namespace bc {

struct  datachannel_handler : mileaccess::ma::datachannel::datachannel_handler {
    // Note throws exceptions if it fails.
    datachannel_handler(mileaccess::ma::client const &c);
    // Note throws exceptions if it fails.
    virtual ~datachannel_handler();

    void on_datachannel_notification(mileaccess::ma::client const &c, std::string const &product_id, std::string const &message_id, ::ma_int64_t correlation_id, ::ma_datachannel_item_notification_t status, ::ma_error_t reasons);

    void on_datachannel_message(mileaccess::ma::client const &c, std::string const &product_id, std::string const &message_id, ::ma_int64_t correlation_id, mileaccess::ma::datachannel::item message);
    
private:
    void send_datachannel_item();
    
    mileaccess::ma::client const &m_cli;

};

}} /* namespace mileaccess : namespace bc*/


#endif /* BC_DATACHANNEL_HXX_INCLUDED */
