#ifndef BC_UPDATER_UI_HANDLER_HXX_INCLUDED
#define BC_UPDATER_UI_HANDLER_HXX_INCLUDED

#include "bc/bc_common.hxx"

#include "ma/client.hxx"
#include "ma/updater/ui_provider.hxx"

using namespace mileaccess::ma;
using namespace mileaccess::ma::updater;

namespace mileaccess { namespace bc {

struct  updater_ui_handler : mileaccess::ma::updater::ui_provider, mileaccess::ma::updater::ui_registrar {
    // Note throws exceptions if it fails.
    updater_ui_handler(mileaccess::ma::client const &c);
    // Note throws exceptions if it fails.
    virtual ~updater_ui_handler();

    virtual ::ma_error_t on_show_ui(mileaccess::ma::client const &client, std::string &session_id, ::ma_updater_ui_type_t type, ui_request const &request, ui_response &response);
    
    virtual ::ma_error_t on_progress(mileaccess::ma::client const &client, std::string &session_id, ::ma_int32_t progress_step, ::ma_int32_t max_progress_step, const char *progress_message);
	    
    virtual ::ma_error_t on_end_ui(mileaccess::ma::client const &client, std::string &session_id, const char *title, const char *end_message);

	virtual ::ma_error_t on_event(mileaccess::ma::client const &client, std::string &session_id, updater_event const &ei);
    
private:
    mileaccess::ma::client const &m_cli;
};

}} /* namespace mileaccess : namespace bc*/


#endif /* BC_UPDATER_UI_HANDLER_HXX_INCLUDED */
