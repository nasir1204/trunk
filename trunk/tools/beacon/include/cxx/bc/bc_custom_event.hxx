#ifndef BC_CUSTOM_EVENT_HXX_INCLUDED
#define BC_CUSTOM_EVENT_HXX_INCLUDED

#include "bc/bc_common.hxx"

#include "ma/utils/auto_handle.hxx"
#include "ma/utils/exception.hxx"
#include "ma/client.hxx"
#include "ma/event/ma_event_client.h"
#include "ma/event/ma_custom_event.h"

namespace mileaccess { namespace bc {

void bc_custom_event_generate(mileaccess::ma::client const &cli, ma_uint32_t event_id, ma_event_severity_t severity);

}} /* namespace mileaccess : namespace bc*/


#endif /* BC_CUSTOM_EVENT_HXX_INCLUDED */
