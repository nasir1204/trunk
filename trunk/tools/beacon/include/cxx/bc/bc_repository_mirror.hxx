#ifndef BC_REPOSITORY_MIRROR_HXX_INCLUDED
#define BC_REPOSITORY_MIRROR_HXX_INCLUDED

#include "bc/bc_common.hxx"

#include "ma/utils/auto_handle.hxx"
#include "ma/utils/exception.hxx"
#include "ma/client.hxx"
#include "ma/mirror/mirror.hxx"

namespace mileaccess { namespace bc {
	struct bc_repository_mirror: mileaccess::ma::RepositoryMirror {
		bc_repository_mirror(mileaccess::ma::client const &cli, std::string &destination_path);
	};
}} /* namespace mileaccess : namespace bc*/


#endif /* BC_REPOSITORY_MIRROR_HXX_INCLUDED */
