#ifndef MA_BEACON_HXX_INCLUDED
#define MA_BEACON_HXX_INCLUDED

#include "ma/client.hxx"
#include "ma/logger.hxx"

#include "bc/bc_common.hxx"
#include "bc/bc_info.hxx"
#include "bc/bc_property.hxx"
#include "bc/bc_policy.hxx"
#include "bc/bc_task.hxx"
#include "bc/bc_datachannel.hxx"
#include "bc/bc_updater.hxx"
#include "bc/bc_event.hxx"
#include "bc/bc_client_notification_handler.hxx"
#include "bc/bc_repository_client.hxx"
#include "bc/bc_repository_mirror.hxx"

namespace mileaccess { namespace bc {

struct beacon : public mileaccess::ma::notification_handler, public mileaccess::ma::client_logger,public mileaccess::ma::passphrase_provider {

public:
    // Note throws exceptions if it fails.
    beacon();
    // Note throws exceptions if it fails.
    ~beacon();
    // Note throws exceptions if it fails.
    void start();
    // Note throws exceptions if it fails.
    void stop();

protected:
	// Note throws exceptions if it fails.
    void client_start();	
    void client_stop();

	/*implement the client nofifier*/
	void on_notification(client const &cli, ::ma_client_notification_t type);

	/*implement the client logger*/
	void on_logmessage(::ma_log_severity_t sev, const std::string &module_name, const std::string &log_message);

	/*implement the passphrase provider - optional if required */
	ma_error_t on_provide_passphrase(unsigned char *passphrase, size_t *passphrase_size);
private:
    /* prevent accidental copying and assignment */
    beacon(beacon const&);

    beacon const &operator=(beacon const &);

    mileaccess::ma::client                          *m_client;

	mileaccess::ma::client_logger					*m_client_logger;

	mileaccess::bc::info_event_handler				*m_info_event_handler ;

    mileaccess::bc::property_provider               *m_property_provider;

    mileaccess::bc::policy_notification_handler     *m_policy_notification_handler;

    mileaccess::bc::task_handler                    *m_task_handler;

    mileaccess::bc::datachannel_handler             *m_datachannel_handler;

    mileaccess::bc::updater_handler                 *m_updater_handler;

	mileaccess::bc::bc_event_subscriber             *m_event_handler;

	mileaccess::bc::bc_repository_client            *m_repository_client;

	mileaccess::bc::bc_repository_mirror            *m_repository_mirror;
};


}} //namespace mileaccess :: bc

#endif /* MA_BEACON_HXX_INCLUDED */

