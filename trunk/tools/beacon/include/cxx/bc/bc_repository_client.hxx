#ifndef BC_REPOSITORY_CLIENT_HXX_INCLUDED
#define BC_REPOSITORY_CLIENT_HXX_INCLUDED

#include "bc/bc_common.hxx"

#include "ma/utils/auto_handle.hxx"
#include "ma/utils/exception.hxx"
#include "ma/client.hxx"
#include "ma/repository/repository_client.hxx"

namespace mileaccess { namespace bc {
	struct bc_repository_client: mileaccess::ma::RepositoryClient {
		bc_repository_client(mileaccess::ma::client const &cli);
	};
}} /* namespace mileaccess : namespace bc*/


#endif /* BC_REPOSITORY_CLIENT_HXX_INCLUDED */
