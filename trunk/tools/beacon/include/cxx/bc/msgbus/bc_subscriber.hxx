#ifndef BC_SUBSCRIBER_HXX_INCLUDED
#define BC_SUBSCRIBER_HXX_INCLUDED

#include "bc/bc_common.hxx"

#include "ma/client.hxx"
#include "ma/msgbus/msgbus.hxx"

namespace mileaccess { namespace bc {

struct  bc_subscriber : mileaccess::ma::msgbus_subscriber::handler {
    // Note throws exceptions if it fails.
    bc_subscriber(mileaccess::ma::client const &c);
    // Note throws exceptions if it fails.
    virtual ~bc_subscriber();

	ma_error_t on_pubsub_message(char const *, mileaccess::ma::message const &);

	mileaccess::ma::msgbus_subscriber m_subscriber;


};

}} /* namespace mileaccess : namespace bc*/


#endif /* BC_SUBSCRIBER_HXX_INCLUDED */
