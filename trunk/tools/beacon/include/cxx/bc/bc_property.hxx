#ifndef BC_PROPERTY_HXX_INCLUDED
#define BC_PROPERTY_HXX_INCLUDED

#include "bc/bc_common.hxx"

#include "ma/client.hxx"
#include "ma/properties/property_provider.hxx"

namespace mileaccess { namespace bc {

struct  property_provider : mileaccess::ma::property_provider {
    // Note throws exceptions if it fails.
    property_provider(mileaccess::ma::client const &c);
    // Note throws exceptions if it fails.
    virtual ~property_provider();

    virtual ma_error_t on_collect_properties(mileaccess::ma::client const &cli, std::string const &product_id, mileaccess::ma::property_bag &properties);
    
    mileaccess::ma::client const &m_cli;

};

}} /* namespace mileaccess : namespace bc*/


#endif /* BC_PROPERTY_HXX_INCLUDED */
