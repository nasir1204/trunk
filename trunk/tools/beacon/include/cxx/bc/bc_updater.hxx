#ifndef BC_UPDATER_HXX_INCLUDED
#define BC_UPDATER_HXX_INCLUDED

#include "bc/bc_common.hxx"

#include "ma/client.hxx"
#include "ma/updater/updater_handler.hxx"

using namespace mileaccess::ma;
using namespace mileaccess::ma::updater;

namespace mileaccess { namespace bc {

struct  updater_handler : mileaccess::ma::updater::updater_handler {
    // Note throws exceptions if it fails.
    updater_handler(mileaccess::ma::client const &c);
    // Note throws exceptions if it fails.
    virtual ~updater_handler();

    virtual ::ma_error_t notify_cb(client const &cli, std::string const &product_id,  std::string const &update_type, ::ma_update_state_t state, std::string const &message, std::string const& extra_info, ::ma_updater_product_return_code_t &return_code);

    virtual ::ma_error_t set_cb(client const &cli, std::string const &product_id, std::string const &update_type, std::string const &key, mileaccess::ma::variant const &value, ::ma_updater_product_return_code_t &return_code);

    virtual ::ma_error_t get_cb(client const &cli, std::string const &product_id, std::string const &update_type, std::string const &key, mileaccess::ma::variant &value, ::ma_updater_product_return_code_t &return_code);

	virtual ::ma_error_t updater_event_cb(client const &cli, std::string const &product_id, const updater_event &upd_event);
    
private:
    void run_update_now();
    
    mileaccess::ma::client const &m_cli;

};

}} /* namespace mileaccess : namespace bc*/


#endif /* BC_UPDATER_HXX_INCLUDED */
