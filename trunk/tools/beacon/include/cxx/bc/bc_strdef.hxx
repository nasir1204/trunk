#ifndef BC_STRDEF_HXX_INCLUDED
#define BC_STRDEF_HXX_INCLUDED

#include "bc/bc_common.hxx"

namespace mileaccess { namespace bc {

#define BC_PRODUCT_ID_STR                               "BEACON__1000"
#define BC_LOG_FILE_NAME_STR                            "beacon++"
#define BC_PRODUCT_NAME_STR                             "Beacon++ Software"

/* Beacon properties - General section */
#define BC_PROP_PATH_GENERAL_STR                        "General"
#define BC_PROP_KEY_PRODUCT_VERSION_STR                 "ProductVersion"
#define BC_PROP_KEY_PRODUCT_NAME_STR                    "ProductName"

#define BC_PROP_VALUE_PRODUCT_VERSION_STR               "5.0.0.1234"
#define BC_PROP_VALUE_PRODUCT_NAME_STR                  "Beacon++ - A sample product with cxx binding"

/* Beacon properties - Showlist section */
#define BC_PROP_PATH_SHOWLIST_STR                       "Showlist"

#define BC_PROP_KEY_SHOWLISTING_ENABLED_STR             "ShowlistingEnabled"
#define BC_PROP_KEY_SHOWLISTING_LIST_STR                "ShowlistingList"

#define BC_PROP_VALUE_SHOWLIST_ENABLED_STR              "Enabled"
#define BC_PROP_VALUE_SHOWLIST_DISABLED_STR             "Disabled"
#define BC_PROP_VALUE_SHOWLISTING_LIST_STR              "Properties;Polices;Event;Task"

#define BC_PROP_KEY_SHOWLISTING_ENABLED_BOOL            "ShowlistingEnabled"
#define BC_PROP_KEY_SHOWLISTING_LIST_ARRAY              "ShowlistingList"


#define BC_TASK_EVENT_TYPE_STR                          "Event"
#define BC_TASK_LONG_RUNNING_TYPE_STR                   "LongRunning"

#define BC_DC_ITEM1										"DcTestEcho"
#define BC_DC_ITEM2										"MA_RunNow"
#define BC_DC_ITEM3										"MA_UpdateNow"
#define BC_DC_DATA										"This is a sample datachannel message that is sent from beacon++"
}}

#endif /* BC_STRDEF_HXX_INCLUDED */
