#ifndef BC_CLIENT_NOTIFICATION_HANDLER_HXX_INCLUDED
#define BC_CLIENT_NOTIFICATION_HANDLER_HXX_INCLUDED

#include "bc/bc_common.hxx"

#include "ma/client.hxx"

using namespace mileaccess::ma;

namespace mileaccess { namespace bc {

struct  notification_handler : mileaccess::ma::notification_handler {
    // Note throws exceptions if it fails.
    notification_handler(mileaccess::ma::client const &c, void *cb_data) ;
    // Note throws exceptions if it fails.
    virtual ~notification_handler() ;

    //virtual void on_notification(client const &cli, ::ma_client_notification_t type, void *cb_data) ;
    virtual void on_notification(client const &cli, ::ma_client_notification_t type) ;
    
private:
    mileaccess::ma::client const &m_cli ;
	void *cb_data ;
};

}} /* namespace mileaccess : namespace bc*/


#endif /* BC_CLIENT_NOTIFICATION_HANDLER_HXX_INCLUDED */

