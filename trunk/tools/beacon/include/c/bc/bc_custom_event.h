#ifndef BC_CUSTOM_EVENT_H_INCLUDED
#define BC_CUSTOM_EVENT_H_INCLUDED

#include "bc/bc_common.h"
#include "ma/ma_client.h"
#include "ma/event/ma_event_client.h"
#include "ma/event/ma_custom_event.h"

BC_CPP(extern "C" {)

bc_error_t bc_custom_event_generate(ma_client_t *ma_client, ma_uint32_t event_id, ma_event_severity_t severity);

BC_CPP(})

#endif  /* BC_CUSTOM_EVENT_H_INCLUDED */
