#ifndef BC_CONTROLLER_H_INCLUDED
#define BC_CONTROLLER_H_INCLUDED

#include "bc/bc_common.h"

#define BC_OBJECT_MA_CONTEXT_NAME_STR	"bc.object.ma.context"

BC_CPP(extern "C" {)

typedef struct beacon_s beacon_t, *beacon_h;

bc_error_t beacon_create(beacon_t **beacon);

bc_error_t beacon_start(beacon_t *beacon);

bc_error_t beacon_stop(beacon_t *beacon);

bc_error_t beacon_release(beacon_t *beacon);

BC_CPP(})

#endif  /* BC_CONTROLLER_H_INCLUDED */

