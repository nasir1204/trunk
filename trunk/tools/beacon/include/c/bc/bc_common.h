#ifndef BC_COMMON_DEFS_H_INCLUDED
#define BC_COMMON_DEFS_H_INCLUDED

#define LOG_FACILITY_NAME "beacon"

#include "bc/bc_errors.h"

#ifdef __cplusplus
# define BC_CPP(x) x
extern "C" {
#else
# define BC_CPP(x)
#endif


#ifdef __cplusplus
}
#endif
#endif /* BC_COMMON_DEFS_H_INCLUDED */
