#ifndef BC_TASK_H_INCLUDED
#define BC_TASK_H_INCLUDED

/* demonstrates the integration with tasks coming down from ePO server, if any */

#include "bc/bc_common.h"
#include "ma/ma_client.h"
//#define Timezone

BC_CPP(extern "C" {)

bc_error_t bc_task_intialize(ma_client_t *ma_client);

bc_error_t bc_task_deinitalize(ma_client_t *ma_client);

bc_error_t bc_daily_task_create(ma_client_t *ma_client);

/*Functions still not added in the sln file*/
/*
bc_error_t bc_weekly_task_create(ma_client_t *ma_client);

bc_error_t bc_runonce_task_create(ma_client_t *ma_client);

bc_error_t bc_runnow_task_create(ma_client_t *ma_client);

bc_error_t bc_monthlyday_task_create(ma_client_t *ma_client);

bc_error_t bc_monthlydow_task_create(ma_client_t *ma_client);



bc_error_t bc_modify_trigger(ma_client_t *ma_client);*/

bc_error_t bc_task_enumerate(ma_client_t *ma_client);
bc_error_t bc_modify_task(ma_client_t *ma_client);

BC_CPP(})

#endif  /* BC_TASK_H_INCLUDED */
