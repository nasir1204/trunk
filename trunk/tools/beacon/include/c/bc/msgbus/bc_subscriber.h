#ifndef BC_SUBSCRIBER_H_INCLUDED
#define BC_SUBSCRIBER_H_INCLUDED

#include "bc/bc_common.h"

#include "ma/ma_client.h"

BC_CPP(extern "C" {)

bc_error_t bc_subscriber_intialize(ma_client_t *ma_client);

bc_error_t bc_subscriber_deintialize(ma_client_t *ma_client);

BC_CPP(})

#endif  /* BC_SUBSCRIBER_H_INCLUDED */

