#ifndef BC_UPDATER_UI_HANDLER_H_INCLUDED
#define BC_UPDATER_UI_HANDLER_H_INCLUDED

#include "bc/bc_common.h"

#include "ma/ma_client.h"

BC_CPP(extern "C" {)

bc_error_t bc_updater_ui_handler_intialize(ma_client_t *ma_client);

bc_error_t bc_updater_ui_handler_deinitalize(ma_client_t *ma_client);

BC_CPP(})

#endif  /* BC_UPDATER_UI_HANDLER_H_INCLUDED */

