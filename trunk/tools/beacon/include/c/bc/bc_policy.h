#ifndef BC_POLICY_H_INCLUDED
#define BC_POLICY_H_INCLUDED

#include "bc/bc_common.h"

#include "ma/ma_client.h"

BC_CPP(extern "C" {)

bc_error_t bc_policy_intialize(ma_client_t *ma_client);

bc_error_t bc_policy_deinitalize(ma_client_t *ma_client);

BC_CPP(})

#endif  /* BC_POLICY_H_INCLUDED */

