#ifndef BC_UTILS_H_INCLUDED
#define BC_UTILS_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/scheduler/ma_scheduler.h"
#include "ma/scheduler/ma_task.h"

#ifdef MA_WINDOWS
#include <Windows.h>
#endif

static void get_localtime(ma_task_time_t *time) {
    if(time) {
#ifdef MA_WINDOWS
    SYSTEMTIME systime;

    GetLocalTime(&systime);
    time->year = systime.wYear;
    time->month = systime.wMonth;
    time->day   = systime.wDay;

    time->hour  = systime.wHour;
    time->minute = systime.wMinute;
    time->secs = systime.wSecond;

    //time->milliseconds = systime.wMilliseconds;
#else
    struct tm *tm_time;
    struct timeval tv;

    if ((0==gettimeofday(&tv, 0)) && NULL != (tm_time = localtime(&tv.tv_sec))) {
        time->year          = 1900 + tm_time->tm_year;
        time->month         = 1 + tm_time->tm_mon;
        time->day           = tm_time->tm_mday;

        time->hour          = tm_time->tm_hour;
        time->minute        = tm_time->tm_min;
        time->secs        = tm_time->tm_sec;

        //time->milliseconds  = tv.tv_usec / 1000;
    }
#endif
    }
}

static void advance_current_date_by_seconds(ma_int64_t seconds, ma_task_time_t *new_date) {
    if(new_date) {
        if(seconds) {
            ma_task_time_t tdate = {0};
            struct tm task_time = {0};
            struct tm *next_date = NULL;
            time_t tt = 0;

            get_localtime(&tdate);
            task_time.tm_hour = tdate.hour;
            task_time.tm_year = tdate.year-1900;
            task_time.tm_mon = tdate.month-1;
            task_time.tm_mday = tdate.day;
            task_time.tm_min = tdate.minute;
            task_time.tm_sec = tdate.secs;
            task_time.tm_isdst = -1;
            tt = mktime(&task_time) + seconds;

            if(tt && (next_date = localtime(&tt))) {
                new_date->year = next_date->tm_year+1900;
                new_date->month = next_date->tm_mon + 1;
                new_date->day = next_date->tm_mday;
                new_date->minute = next_date->tm_min;
                new_date->hour = next_date->tm_hour;
                new_date->secs = next_date->tm_sec;
            }
        }
    }
}

#endif /* BC_UTILS_H_INCLUDED */
