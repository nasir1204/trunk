#ifndef BC_EVENT_H_INCLUDED
#define BC_EVENT_H_INCLUDED

#include "bc/bc_common.h"
#include "ma/ma_context.h"
#include "ma/event/ma_event_bag.h"

BC_CPP(extern "C" {)

bc_error_t bc_event_generate(ma_context_t *ma_context, const char *event_name, ma_uint32_t event_id, ma_event_severity_t severity);

BC_CPP(})

#endif  /* BC_EVENT_H_INCLUDED */
