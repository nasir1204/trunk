#!/bin/bash

add_path()
{
	echo "Adding $1 to \$PATH"
	INDATA=$1
	data=`echo $PATH | grep "\(\(^$INDATA\)\|\(:$INDATA\)\)" | wc -l`
	if (( !$data )); then
	    if [ "$PATH" == "" ]; then 
		PATH=$1
	    else
		PATH=$1:$PATH
	    fi
	fi
}

printParams()
{
	echo "version=" $version
	echo "buildType=" $buildType
	echo "configType=" $configType
	echo "platform=" $platform
	echo "arch=" $arch
	echo "archBit=" $archBit
	echo "toolChainPath=" $toolChainPath
}

ValidateParams()
{
	MAJVER=`echo $version | cut -d. -f1`
	MINVER=`echo $version | cut -d. -f2`
	SUBVER=`echo $version | cut -d. -f3`
	BUILDVER=`echo $version | cut -d. -f4`

	if [ "$MAJVER" == "" ] || [ "$MINVER" == "" ] || [ "$SUBVER" == "" ] || [ "$BUILDVER" == "" ] 
        then
		echo "Version Details are not correct"
        	Usage 
        fi

	if [ "$platform" != "Linux" ] && [ "$platform" != "Darwin" ] && [ "$platform" != "HP-UX" ] && [ "$platform" != "AIX" ] && [ "$platform" != "SunOS" ] && [ "$platform" != "FreeBSD" ] 
	then
    		echo "Error: " $platform "Platform is not supported"
    		exit 3
	fi

	if [ "$buildType" != "compile" ] && [ "$buildType" != "clean" ]
	then
    		Usage
	fi

	if [ "$configType" != "release" ] && [ "$configType" != "debug" ] && [ "$configType" != "all" ]
	then
    		Usage
	fi

	if [ "$arch" != "intel" ] && [ "$arch" != "ppc" ] && [ "$arch" != "sparc" ] && [ "$arch" != "itanium" ] && [ "$arch" != "pa_risc" ] && [ "$arch" != "aix" ] && [ "$arch" != "amd" ]
	then
    		Usage
	fi

	if [ "$archBit" != "all" ] && [ "$archBit" != "32" ] && [ "$archBit" != "64" ] 
	then
    		Usage
	fi
}

# Usage 
Usage()
{
    echo "Usage: bash <complete path>/build/build.sh -v <version> -t <build type> -c <config type> -p <platform> -a <arch> -b <arch-bit> -T <tool chain path>"
    echo "   -v version i.e. 5.0.0.215 (Required)"
    echo "   -t build type i.e compile/clean default is compile (optional)"
    echo "   -c configuration type  i.e release/debug/all default is release (optional)"
    echo "   -p unix platform type, output of 'uname -s' i.e. Linux/SunOS/HP-UX/AIX/Darwin/FreeBSD (Required)"
    echo "   -a Architecture type i.e. intel/ppc/sparc/itanium/pa-risc/aix/amd default is intel (optional)"
    echo "   -b bit type i.e. 32/64/all default is 32 (optional)"
    echo "   -T tool chain path default is taken from PATH env variable (optional)"
    exit 1
}

# Enable debugging option if SH_DEBUG=true
if [ "x$SH_DEBUG" == "xtrue" ]
then
	set -xv
fi


scriptFullPath=$0
firstchar=`echo $scriptFullPath | grep "^\/"`

if [ "$firstchar" == "" ]; then
    echo "The build script path is not absolute path"
    Usage
fi

if [ "x$BC_BASE" == "x" ]
then
BC_BASE=`echo $scriptFullPath | grep ".build.sh" | sed -e  's/\/build\/build.sh//g'`
fi

echo "BEACON  base =" $BC_BASE

# Parsing build script arguments 
while getopts v:t:c:p:a:b:T: opt
  do
  case $opt in
      v)
	  version=$OPTARG;;
      t)
	  buildType=$OPTARG;;
      c)
	  configType=$OPTARG;;
      p)
          platform=$OPTARG;;
      a)
          arch=$OPTARG;;
      b)  
	  archBit=$OPTARG;;
      T)  
	  toolChainPath=$OPTARG;;
      h)
	  Usage;;
      \?) 
          Usage;;
  esac
done
shift `expr $OPTIND - 1` 

# Assigning default values for optional parameters if not specified
[ -z $version ] && Usage
[ -z $buildType ] && buildType=compile
[ -z $configType ] && configType=release
[ -z $platform ] && Usage 
[ -z $arch ] && arch=intel
[ -z $archBit ] && archBit=32

# Printing all parameter values before validation
printParams

# Validating the path and parameters specified
ValidateParams

if [ "$platform" == "Linux" ] && [ "$archBit" == "64" ] && [ -f "/etc/redhat-release" ]
then
DIST=`cat /etc/redhat-release | cut -d" " -f1`
makeCmdArgs=" DIST=$DIST "$makeCmdArgs
fi

if [ "$platform" == "Linux" ] && [ "$archBit" == "64" ] &&  [ x"mileaccess" != x"$DIST" ]
then
echo "Beacon not building in linux 64 bit"
#exit 0
fi

# Adding TOOLCHAIN Path to PATH
if [ ! -z $toolChainPath ]
then
add_path $toolChainPath
makeCmdArgs=" TOOLCHAIN=$toolChainPath "
fi

# cheicking build environment
echo ""
echo "Build Environment is..."
echo ""
echo `uname -a`
echo Using make `which make`
echo Using gcc  `which gcc`
echo Using g++  `which g++`
echo Using ld   `which ld`
echo Using nm   `which nm`
echo ""

echo "Displaying gcc version:"
echo `gcc -v`
echo `uname -a`
if [ "$platform" == "Darwin" ]
then
	#echo `/Developer/usr/bin/xcodebuild  -version`
	echo `/usr/bin/xcodebuild  -version`
fi
echo ""

# setting target setting for make

# Adding configuration, Platform, arch, version, type

makeCmdArgs=" MAJ_VER=$MAJVER MIN_VER=$MINVER SUB_VER=$SUBVER BUILD_VER=$BUILDVER "$makeCmdArgs
makeCmdArgs=" OS_NAME=$platform ARCH=$arch "$makeCmdArgs

if [ "$configType" == "all" ]
then
configuration="release debug"
else
configuration=$configType
fi

if [ "$archBit" == "all" ]
then
bit_type="32 64"
else
bit_type=$archBit
fi

# Build beacon 
makeCmd="make "$buildType

# saving current directory and changin directory to $BC_BASE/build.makefiles to execute make command
curdir=`pwd`
cd $BC_BASE/build/makefiles

echo "Changing directory to " $BC_BASE/build/makefiles

MASIGN=$BC_BASE/../ma_sdk/masign/masign

for con in $configuration
do
for bit in $bit_type
do
echo "********** " $bit bit $con "Build Starting **********"
Cmd=$makeCmd" CONFIG=$con ARCH_BIT=$bit "$makeCmdArgs
echo $Cmd
$Cmd
	if [ $? -ne 0 ] 
	then
	echo "********** " $bit bit $con " Beacon Build Failed.... " 
	cd $curdir
	exit 1
	else  
	echo "********** " $bit bit $con " Beacon Build Ending **********" 
	fi 

    export BINARY_DIR=$BC_BASE/build/makefiles/$con/	

    if [ $bit == "64" ]; then
        export BINARY_DIR=$BC_BASE/build/makefiles/$con/$bit/
    fi

    #******************IF NEEDED APPLE BINARY SIGNING PUT IT ABOVE THAT 
    $MASIGN -prep -dir $BINARY_DIR -name beacon -out $BC_BASE/build/packager/data/ -hints "Technology=C AuthRule=AuthType==ExecutableValidation,ChallengeResponse and PlatformID==MLOS,LINUX,MAC and User==root and Group==root"
    $MASIGN -prep -dir $BINARY_DIR -name beacon++ -out $BC_BASE/build/packager/data/

done
done

if [ ! -f $BC_BASE/build/packager/data/ma_msgbus_auth.xml ]
then
        echo WARNING !!!!  ma_msgbus_auth.xml is not found
else
        echo ma_msgbus_auth.xml is present at the expected location for signing
        cat $BC_BASE/build/packager/data/ma_msgbus_auth.xml
fi


cd $curdir
echo "Changing directory back to " $curdir
exit 0
