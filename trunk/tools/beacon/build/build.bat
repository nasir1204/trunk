@echo off
cls
echo executing %0 %*
set BC_BASE=%~dp0..

if /i "%1"=="help" goto help
if /i "%1"=="--help" goto help
if /i "%1"=="-help" goto help
if /i "%1"=="/help" goto help
if /i "%1"=="?" goto help
if /i "%1"=="-?" goto help
if /i "%1"=="--?" goto help
if /i "%1"=="/?" goto help

if exist %BC_BASE%\build\msvc\ma_tools_outdir (
	@rem delete all files 
	del /q /s %BC_BASE%\build\msvc\ma_tools_outdir
	@rem all the folders
	rmdir /q /s %BC_BASE%\build\msvc\ma_tools_outdir
)

@rem Process arguments.
set config=Release
set target=Rebuild
set bit=32
set arch=intel
set vs_toolset=x86
set platform=Win32
set library=static_library
set builgLogFile=.\build.log
set allconfig=
set allbit=

:next-arg
set str1=%1
if "%1"=="" 						goto args-done
if /i "%1"=="debug"        			set config=Debug & goto arg-ok
if /i "%1"=="release"      			set config=Release & goto arg-ok
if /i "%1"=="allconfig"      		set allconfig=1 & goto arg-ok

if /i "%1"=="clean"        			set target=Clean & goto arg-ok
if /i "%1"=="build"        			set target=Build & goto arg-ok
if /i "%1"=="rebuild"        		set target=Rebuild & goto arg-ok

if /i "%1"=="32"          			set bit=32 & goto arg-ok
if /i "%1"=="64"         			set bit=64 & goto arg-ok
if /i "%1"=="all"         			set allbit=1 & goto arg-ok

if /i "%1"=="intel"          		set arch=intel & goto arg-ok
if /i "%1"=="itanium"          		set arch=itanium & goto arg-ok
if /i "%1"=="amd"          			set arch=amd & goto arg-ok

if /i "%1"=="test"          		set test=1 & goto arg-ok

if /i "%1"=="msi"          			set msi=1 & goto arg-ok

if not x%str1::\=%==x%str1%  (
	if not x%str1:.log=%==x%str1% (
		set builgLogFile=%1 & goto arg-ok
	) else (
		set outFolder=%1 & goto arg-ok
	)
)

if /i "%1"=="shared"       			set library=shared_library & goto arg-ok
if /i "%1"=="static"       			set library=static_library & goto arg-ok
:arg-ok
shift
goto next-arg
:args-done


if not defined MA_VERSION_MAJOR set MA_VERSION_MAJOR=5
if not defined MA_VERSION_MINOR set MA_VERSION_MINOR=0
if not defined MA_VERSION_SUBMINOR set MA_VERSION_SUBMINOR=0
if not defined MA_VERSION_BUILDNUMBER set MA_VERSION_BUILDNUMBER=999

set CMD_LINE=MA_VERSION_MAJOR=%MA_VERSION_MAJOR%;MA_VERSION_MINOR=%MA_VERSION_MINOR%;MA_VERSION_SUBMINOR=%MA_VERSION_SUBMINOR%;MA_VERSION_BUILDNUMBER=%MA_VERSION_BUILDNUMBER%

echo BC base=%BC_BASE%
echo.
echo config=%config%
echo bit=%bit%
echo arch=%arch%

if /i %bit%==32 (
	if %arch%==intel set vs_toolset=x86 & set platform=Win32
	if %arch%==itanium set vs_toolset=x86 & set platform=Win32
	if %arch%==amd set vs_toolset=x86 & set platform=Win32
)
if /i %bit%==64 (
	if %arch%==intel set vs_toolset=x86_amd64 & set platform=x64
	if %arch%==itanium set vs_toolset=x86_ia64 & set platform=x64
	if %arch%==amd set vs_toolset=x86_amd64 & set platform=x64
)

rem if /i %vs_toolset%==x86 (
rem	set platform=Win32
rem	) else (
rem		set platform=%vs_toolset%
rem	)

set allbits=(%platform%)
if defined allbit set allbits=(Win32, x64)


set allconfigs=(%config%)
if defined allconfig  set allconfigs=(Release, Debug)

echo vs_toolset=%vs_toolset%
echo pltaform=%platform%
echo.
echo outFolder=%outFolder%
echo builgLogFile=%builgLogFile%

if defined msi goto buildmsi

if "%outFolder%"=="" goto skip-outfolder
if not exist %outFolder% (
	echo creating folder %outFolder%
	mkdir %outFolder%
) else (
	echo %outFolder% already exists. Provide another out folder.
	goto exit
)

:skip-outfolder


@rem Look for Visual Studio 2010
if not defined VS100COMNTOOLS goto vc-set-notfound
if not exist "%VS100COMNTOOLS%\..\..\vc\vcvarsall.bat" goto vc-set-notfound
goto msbuild


:vc-set-notfound
echo *************************************************
echo Error: Visual Studio 2010 not found
echo *************************************************
goto exit

:msbuild
set MSBuildEmitSolution=1
echo allbits=%allbits%
echo allconfigs=%allconfigs%
echo %BC_BASE%
echo.
for %%A in %allbits% Do (
	if %%A==Win32 (
		 call "%VS100COMNTOOLS%\..\..\vc\vcvarsall.bat" x86
	) else (
		if %arch%==itanium ( 
			call "%VS100COMNTOOLS%\..\..\vc\vcvarsall.bat" x86_ia64
			) else (
				call "%VS100COMNTOOLS%\..\..\vc\vcvarsall.bat" x86_amd64
			)
	)
	for %%B in %allconfigs% Do (
		msbuild %BC_BASE%\build\msvc\bc.sln /t:%target% /p:Configuration=%%B /p:Platform=%%A >> %builgLogFile%				
		if errorlevel 1 goto exit-error
	)
)

goto exit


:help
echo fullpath\build.bat [debug/release/allconfig] [test] [32/64/all] [intel/itanium/amd] [static/shared] [outfolderFullPath] [buildLogFilenameFullPath]
echo Examples:
echo   w:\code\build.bat               										: builds release build, build.log would be generated at .\ where the bat file is invoked from. 
echo   w:\code\build.bat test          										: builds release build, build.log would be generated at .\ where the bat file is invoked from and also builds the test solution. 
echo   w:\code\build.bat Release all intel w:\code\test\ w:\build.log		: builds release on all platforms (Win32, x64) and the binaries are copied to "w:\code\test\" and the compile logs are generated in w:\build.log
echo   w:\code\build.bat allconfig Win32 intel w:\code\test\ w:\build.log	: builds release on all configuraions (Release, Debug) on intel and the binaries are copied to "w:\code\test\" and the compile logs are generated in w:\build.log
goto exit

:exit-error
echo ************************************************************************
echo Error occured while compilation, please check the logs (%builgLogFile%). error=%errorlevel%
exit /B 1
echo ************************************************************************
goto exit

:buildmsi
msbuild %BC_BASE%\build\packager\win\McAfeeAgent.sln /t:Rebuild /p:Configuration=Release /p:Platform=x86 >> %builgLogFile%

:exit
echo ************************************************************************
echo Build is Success.
echo ************************************************************************
exit /B 0
