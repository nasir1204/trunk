@echo off
call bc_env_vars.bat
if "%BC_BASE%"=="" goto error1
if "%MA_SDK_BASE%"=="" goto error2

start msvc\bc.sln

goto end

:error1
echo Error: BC_BASE path not defined

:error2
echo Error: MA_SDK_BASE path not defined

:end
