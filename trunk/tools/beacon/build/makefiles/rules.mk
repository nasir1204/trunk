###################################################
#Copyright mileaccess Inc 2013, All rights reserved
#Date           August 2013
###################################################

# Common rules for all components

ifeq ($(BIT),32)
OUTDIR = $(CONFIGURATION)
else
OUTDIR = $(CONFIGURATION)/$(BIT)
endif

DEPDIR = $(OUTDIR)/.deps

.PHONY: all init

all: init $(DEPLIBS) $(EXE_TARGET) $(POST_BUILD_TARGET)

init:
	@if [ ! -e $(OUTDIR) ];then mkdir -p $(OUTDIR);fi
	@if [ ! -e $(OUTPUT_DIR) ];then mkdir -p $(OUTPUT_DIR);fi
	@if [ ! -e $(DEPDIR) ];then mkdir $(DEPDIR);fi

VPATH = $(SOURCES):$(OUTDIR)
ASMCOMPILE = $(CC) $(CFLAGS) $(DEFINES) $(INCLUDES) -o $(OUTDIR)/${@F} $<


CPPCOMPILE = $(CXX) $(CXXFLAGS) $(ARCH_FLAGS) $(DEFINES) $(INCLUDES) -o $(OUTDIR)/${@F} $<
CCOMPILE = $(CC) $(CFLAGS) $(ARCH_FLAGS) $(DEFINES) $(INCLUDES) -o $(OUTDIR)/${@F} $<

.s.o:
	@echo Compiling  $<
	$(ASMCOMPILE) 
.c.o:
	@echo creating $< dependencies
	$(CC) $(CFLAGS) -MM $(ARCH_FLAGS) $(INCLUDES) $< > $(DEPDIR)/$*.d
	@mv -f $(DEPDIR)/$*.d $(DEPDIR)/$*.d.tmp
	@sed -e 's|.*:|$*.o:|' < $(DEPDIR)/$*.d.tmp > $(DEPDIR)/$*.d
	@sed -e 's/.*://' -e 's/\\$$//' < $(DEPDIR)/$*.d.tmp | fmt -1 | sed -e 's/^ *//' -e 's/$$/:/' >> $(DEPDIR)/$*.d
	@rm -f $(DEPDIR)/$*.d.tmp
	@echo Compiling $< 
	$(CCOMPILE)
.cpp.o .cxx.o:
	@echo creating $< dependencies
	$(CXX) $(CXXFLAGS) -MM $(ARCH_FLAGS) $(INCLUDES) $< > $(DEPDIR)/$*.d
	@mv -f $(DEPDIR)/$*.d $(DEPDIR)/$*.d.tmp
	@sed -e 's|.*:|$*.o:|' < $(DEPDIR)/$*.d.tmp > $(DEPDIR)/$*.d
	@sed -e 's/.*://' -e 's/\\$$//' < $(DEPDIR)/$*.d.tmp | fmt -1 | sed -e 's/^ *//' -e 's/$$/:/' >> $(DEPDIR)/$*.d
	@rm -f $(DEPDIR)/$*.d.tmp
	@echo Compiling $<
	$(CPPCOMPILE)

-include $(OBJS:%.o=$(DEPDIR)/%.d)

.SUFFIXES: .c .cxx .cpp .s .o .d 	

ifdef DEPLIBS 
$(DEPLIBS):
	@echo [$(MAKELEVEL)] building $@ for $(TARGET)
	cd $(BUILD_DIR)/makefiles; $(MAKE) $@ OS_NAME=$(OS_NAME) ARCH_BIT=$(ARCH_BIT) ARCH=$(ARCH) CONFIG=$(CONFIGURATION) 
	@echo [$(MAKELEVEL)] built $@ for $(TARGET)
endif


# Rule for build exe
ifdef EXE_TARGET
TARGET := $(EXE_TARGET)
$(EXE_TARGET) : $(OBJS)
	cd $(OUTDIR); \
	$(LINKER) $(LFLAGS) $(ARCH_FLAGS) $(OBJS) $(LIBPATH) $(LIBS) -o $@; \
	cp $(TARGET) $(OUTPUT_DIR);
endif

ifdef POST_BUILD_TARGET
$(POST_BUILD_TARGET):
	echo "Executing post build command $(POST_BUILD_COMMAND) "
	$(POST_BUILD_COMMAND)
endif


clean:
	if [ -e $(OUTDIR) ];then rm -fr $(OUTDIR);fi
	@echo cleaned $(TARGET)
