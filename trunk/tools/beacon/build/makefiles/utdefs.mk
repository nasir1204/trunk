# This file should be included in all UT makefiles
# It contains all common settings for UTs and also inherits defs from defs.mk

include $(MA_BASE)/build/makefiles/defs.mk

DEFINES := $(DEFINES) -DUNITY_SUPPORT_64
