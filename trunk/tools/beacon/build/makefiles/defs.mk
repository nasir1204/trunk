###################################################
#Copyright mileaccess Inc 2013, All rights reserved 
#Date 		August 2013
###################################################

# MA Version information
ifndef MAJ_VER
MAJ_VER :=5
endif
ifndef MIN_VER
MIN_VER :=0
endif
ifndef SUBMIN_VER
SUBMIN_VER :=0
endif
ifndef BUILD_VER
BUILD_VER :=0
endif
ifndef HOTFIX_VER
HOTFIX_VER :=0
endif

MAJVER :=$(MAJ_VER)
MINVER :=$(MIN_VER)
SUBMINVER :=$(SUBMIN_VER)
BUILDNUM :=$(BUILD_VER)
HOTFIXNUM :=$(HOTFIX_VER)

# Setting CONFIGURATION based on value of CONFIG
ifeq ($(CONFIG),debug)
export CONFIGURATION := debug
else
export CONFIGURATION := release
endif

# Setting BC_BASE if not set already
ifndef BC_BASE 
export BC_BASE := $(PWD)/../../
endif

# Setting MA_SDK_BASE if not set already
ifndef MA_SDK_BASE 
export MA_SDK_BASE := $(BC_BASE)/../ma_sdk/
endif

export MASIGN := $(MA_SDK_BASE)/masign/masign
export MASIGN_PREP_OUT := $(BC_BASE)/build/packager/data


# Default settings and compilaiton flages

ifndef ARCH_BIT
export ARCH_BIT := 32
endif

export BIT := $(ARCH_BIT)

ifndef ARCH
export ARCH := intel
endif

CC := gcc
CXX := g++
# default linker is gcc, this can be overwritten in local makefiles for c++ modules 
LINKER := $(CC)
AR := ar

LIB_EXTN := so
SLIB_EXTN := a
CFLAGS :=  -Wall -c -fPIC
CXXFLAGS :=  -Wall -Werror -c -fPIC
SO_LFLAGS := -fPIC -shared
LFLAGS := 
ARFLAGS := -rcs
LIBS := -lpthread
ARCH_FLAGS :=

# adding -m64 option for 64bit build
ifeq ($(ARCH_BIT),64)
BIT := 64
#CFLAGS := $(CFLAGS) -m64 -mtune=generic
#CXXFLAGS := $(CXXFLAGS) -m64 -mtune=generic
#SO_LFLAGS := $(SO_LFLAGS) -m64
#LFLAGS := $(LFLAGS) -m64 
endif

#if TOOLCHAIN path is set then overwrite the compiler macros with TOOLCHAIN paths 
ifdef TOOLCHAIN
CC := $(TOOLCHAIN)/gcc
CXX := $(TOOLCHAIN)/g++
LINKER := $(CC)
endif 

# Setting macros for all the source paths
SOURCE_DIR := $(BC_BASE)/src
INCLUDE_DIR := $(BC_BASE)/include
BUILD_DIR := $(BC_BASE)/build

ifeq ($(BIT),32) 
LIB_DIR_SUFFIX=lib/$(CONFIGURATION)
BIN_DIR_SUFFIX=bin/$(CONFIGURATION)
OUTPUT_DIR := $(BUILD_DIR)/makefiles/$(CONFIGURATION)
else
OUTPUT_DIR := $(BUILD_DIR)/makefiles/$(CONFIGURATION)/$(BIT)
LIB_DIR_SUFFIX=lib/$(CONFIGURATION)/$(BIT)
BIN_DIR_SUFFIX=bin/$(CONFIGURATION)/$(BIT)
endif

# common runtime library path , over write it for any platforms
RUNTIME_LIBRARY_PATH=export LD_LIBRARY_PATH=$(OUTPUT_DIR):/usr/local/lib:./

ifndef BC_INSTALL_PATH
BC_INSTALL_PATH := /opt/mileaccess/beacon
endif

# Checking OS name and overwrite/add the specific flags to existing macros  
uname_S := $(shell sh -c 'uname -s 2>/dev/null || echo not')

ifndef OS_NAME  
OS_NAME = $(uname_S)
endif

ifeq ($(OS_NAME),Linux)    # Linux specific settings
   DEFINES := $(DEFINES) -DLINUX -D_GNU_SOURCE 
   LIBS := $(LIBS) -ldl -lc -lm -lrt
   LIB_EXTN := $(LIB_EXTN)
   SLIB_EXTN := $(SLIB_EXTN)
   ARFLAGS := -rcs
   SO_LFLAGS := $(SO_LFLAGS) -fPIC -Wl,-soname,$(LIBLINK_MAJ) -Wl,-rpath,$(BC_INSTALL_PATH)/lib
   # -march=i686 is adding to CFLAGS to resolve the symbols __sync_sub_and_fetch and __sync_add_and_fetch whihc are using for atomic operations
   CFLAGS := $(CFLAGS)
ifeq ($(CONFIG),release)
   CFLAGS := $(CFLAGS) -O3
endif
ifeq ($(ARCH_BIT),32)
   CFLAGS := $(CFLAGS) -march=i686
endif
   LFLAGS := $(LFLAGS) -Wl,-rpath,$(BC_INSTALL_PATH)/lib:$(BC_INSTALL_PATH)/bin 
else
ifeq ($(OS_NAME),SunOS)   # Solaris specific settings
   SHELL := /usr/bin/bash
   DEFINES := $(DEFINES) -DSOLARIS -D_POSIX_PTHREAD_SEMANTICS
   SO_LFLAGS := $(SO_LFLAGS) -G -R$(BC_INSTALL_PATH)/lib -R$(BC_INSTALL_PATH)/bin 
   LFLAGS := $(LFLAGS) -R$(BC_INSTALL_PATH)/lib -R$(BC_INSTALL_PATH)/bin 
   LIBS := $(LIBS) -lrt -lkstat -lsocket -lm -lnsl -lsendfile -lstdc++
   AR=/usr/ccs/bin/ar
else
ifeq ($(OS_NAME),HP-UX)     # HP UX specific ssettings
   DEFINES := $(DEFINES) -DHPUX -D_XOPEN_SOURCE_EXTENDED -DREENTRANT -D_REENTRANT
   LIB_EXTN := sl
   CFLAGS := $(CFLAGS) -Wno-missing-braces
   CXXFLAGS := $(CXXFLAGS) -Wno-missing-braces
   ifeq ($(ARCH),pa_risc)
   CFLAGS := $(CFLAGS) -D__BIG_ENDIAN__
   CXXFLAGS := $(CXXFLAGS) -D__BIG_ENDIAN__
   endif
   SO_LFLAGS := $(SO_LFLAGS)  -Wl,+s,+nodefaultrpath,+b$(BC_INSTALL_PATH)/lib:$(BC_INSTALL_PATH)/bin:/usr/lib 
   LFLAGS := $(LFLAGS) -Wl,+s,+nodefaultrpath,+b$(BC_INSTALL_PATH)/lib:$(BC_INSTALL_PATH)/bin:/usr/lib -D_PSTAT64 
   RUNTIME_LIBRARY_PATH:=export SHLIB_PATH=./:$(OUTPUT_DIR):/usr/local/lib:/usr/local/lib/hpux32:/opt/hp-gcc/4.0.1/ilp32/lib/:/usr/lib/
   LIBGCC := -L/usr/local/lib/hpux32 -lgcc_s
   ifeq ($(ARCH),itanium)
        LIBGCC := -L/usr/local/lib/gcc/ia64-hp-hpux11.23/4.2.3 -lgcc
   endif
   LFLAGS := $(LFLAGS) $(LIBGCC)
   LIBS := $(LIBS)  -lunwind
   SO_LFLAGS := $(SO_LFLAGS) $(LIBGCC)
else
ifeq ($(OS_NAME),AIX)       # AIX specific settings
   DEFINES := $(DEFINES) -DAIX
   SO_LFLAGS := $(SO_LFLAGS) -Wl,-brtl,-G,-berok,-bM:SRE -Wl,-blibpath:$(BC_INSTALL_PATH)/lib:$(BC_INSTALL_PATH)/bin:/usr/lib 
   LFLAGS := $(LFLAGS) -fPIC -Wl,-brtl,-bmaxdata:0x80000000,-bmaxstack:0x10000000 -Wl,-blibpath:$(BC_INSTALL_PATH)/lib:$(BC_INSTALL_PATH)/bin:/usr/lib 
   RUNTIME_LIBRARY_PATH:=export LIBPATH=$(OUTPUT_DIR):.:/opt/freeware/lib:/opt/freeware/lib/gcc/powerpc-ibm-aix5.3.0.0/4.2.4/:/usr/lib
else
ifeq ($(OS_NAME),Darwin)       # MAC specific settings  # TBD check actual uname -s output on MAC machine
   MACX := 1
   export MACOSX_DEPLOYMENT_TARGET=10.6
   DEFINES := $(DEFINES) -DMACX
   LIB_EXTN := dylib
   #SO_LFLAGS := $(SO_LFLAGS) -single_module -undefined warning -flat_namespace -dynamiclib -multiply_defined warning -isysroot /Applications/Xcode-Beta.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.9.sdk -framework CoreServices -framework CoreFoundation
   SO_LFLAGS := $(SO_LFLAGS) -single_module -undefined warning -flat_namespace -dynamiclib -multiply_defined warning -isysroot /Developer/SDKs/MacOSX10.9.sdk -framework CoreServices -framework CoreFoundation
   #LFLAGS := $(LFLAGS)  -Xlinker -multiply_defined -Xlinker suppress -isysroot /Developer/SDKs/MacOSX10.6.sdk
   LFLAGS := $(LFLAGS)  -Xlinker -multiply_defined -Xlinker suppress -isysroot /Applications/Xcode-Beta.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.9.sdk
   RUNTIME_LIBRARY_PATH=export DYLD_LIBRARY_PATH=$(OUTPUT_DIR):/usr/local/lib
   AR=libtool
   ARFLAGS=-static
   ifeq ($(ARCH),intel)
     ifeq ($(BIT),64)
        ARCH_FLAGS := -arch x86_64
        #ARCH_FLAGS := -arch x86_64
     else
        ARCH_FLAGS := -arch i386
     endif
   else
   ARCH_FLAGS := -arch ppc 
   endif
else
ifeq ($(OS_NAME),FreeBSD)
   LIBS := $(LIBS) -lkvm -lm 
endif
endif
endif
endif
endif
endif

#### TBD -- Add additional platforms if any ########

# adding debugging options for debug builds
ifeq ($(CONFIGURATION),debug)
CFLAGS := $(CFLAGS) -g -DDEBUG -D_DEBUG
CXXFLAGS := $(CXXFLAGS) -g -DDEBUG -D_DEBUG
endif


INCLUDES := -I. -I$(INCLUDE_DIR)

DEFINES := -DUNIX \
           $(DEFINES) \
           -D$(ARCH) \
	   -DMAJVER=$(MAJ_VER)\
	   -DMINVER=$(MIN_VER) \
	   -DSUBMINVER=$(SUBMIN_VER) \
	   -DBUILDNUM=$(BUILD_NUM) \
	   -DHOTFIXNUM=$(HOTFIX_NUM)

LIBPATH := -L$(OUTPUT_DIR)

SOURCES := $(SOURCE_DIR)
