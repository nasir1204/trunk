# Copyright (C) 2013  mileaccess Inc; All rights reserved
###################################################################
# mileaccess Beacon 
#
###################################################################
Name            : __BC_EXECUTABLE_NAME__
Version         : __BC_VERSION__
Release         : __PACKAGE_NUMBER__
Summary         : Beacon 
License         : commercial. See the COPYRIGHT file for details.
Group           : Utilities
Vendor          : mileaccess Inc.
Packager        : mileaccess Inc.
Conflicts       : __BC_CONFLICT__ 
BuildRoot       : /tmp/__BC_EXECUTABLE_NAME__-%{version}-%{release}-buildroot
Requires        : __DEPS__
Autoreq         : 0
Autoprov        : 0

#################################
# Here begins the beefy bits....#
#################################
%undefine __check_files
%define _source_filedigest_algorithm 0
%define _binary_filedigest_algorithm 0
%define _binary_payload w9.gzdio
%define __os_install_post %{nil}


%description 
Beacon

%prep
__REPLACE_PREPINSTALL_SCRIPT__

%install
__REPLACE_INSTALL_SCRIPT__

%pre
__REPLACE_PREINSTALL_SCRIPT__

%post
__REPLACE_POSINSTALL_SCRIPT__

%clean

%preun
__REPLACE_PREUNINSTALL_SCRIPT__

%postun
__REPLACE_POSTUNINSTALL_SCRIPT__


%files
