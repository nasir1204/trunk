#!/bin/bash

#########################################
# Don't invoke this script directly, it must be invoked via bc_packager.sh
#########################################

HasCRLFHenceExit()
{
    error=0
    for i in `find $1`
    do
		file $i | cut -d: -f2 | grep -e "\([Ss]cript\)\|\(ASCII..*text\)" >/dev/null
      	if (( $? == 0 )); then
	  	str1=`strings -1 $i | head -1`
	  	str2=`cat $i | head -1`
	  	if [ "$str1" != "$str2" ]; then
	    	echo $i 
	    	error=1
	  	fi
      	fi
    done
    if (( $error != 0 )); then
	echo "Have CRLF in Text file $i" 
	file $i
	#Sdas to fix
	#exit 4;
    fi
}
parseFile()
{
	#parseFile "/build/packager/unix" "ma_rpm_spec1.spec" "/ma50/out.tmp" 
	FILE_NAME="$2"
	SCRIPT_PATH="$1/scripts"
	OUT_FILE_NAME=$3
	rm -rf $OUT_FILE_NAME
	touch $OUT_FILE_NAME	
	cat $FILE_NAME | while read LINE
	do
		if [ "$(echo $LINE | grep "^__REPLACE_PREPINSTALL_SCRIPT__")"  ]; then
			cat $SCRIPT_PATH/bc_prepinstall  | sed 's/\\\\/\\/g;s/\\&/\&/g' >>$OUT_FILE_NAME
		elif [ "$(echo $LINE | grep "^__REPLACE_INSTALL_SCRIPT__")"  ]; then
			cat $SCRIPT_PATH/bc_install  | sed 's/\\\\/\\/g;s/\\&/\&/g' >>$OUT_FILE_NAME
		elif [ "$(echo $LINE | grep "^__REPLACE_PREINSTALL_SCRIPT__")"  ]; then
			cat $SCRIPT_PATH/bc_preinstall  | sed 's/\\\\/\\/g;s/\\&/\&/g' >>$OUT_FILE_NAME
		elif [ "$(echo $LINE | grep "^__REPLACE_POSINSTALL_SCRIPT__")"  ]; then
			cat $SCRIPT_PATH/bc_postinstall  | sed 's/\\\\/\\/g;s/\\&/\&/g' >>$OUT_FILE_NAME
		elif [ "$(echo $LINE | grep "^__REPLACE_PREUNINSTALL_SCRIPT__")"  ]; then
			cat $SCRIPT_PATH/bc_preuninstall  | sed 's/\\\\/\\/g;s/\\&/\&/g' >>$OUT_FILE_NAME
		elif [ "$(echo $LINE | grep "^__REPLACE_POSTUNINSTALL_SCRIPT__")"  ]; then
			cat $SCRIPT_PATH/bc_postuninstall  | sed 's/\\\\/\\/g;s/\\&/\&/g' >>$OUT_FILE_NAME
		else
			echo $LINE >>$OUT_FILE_NAME
		fi
	done
}
setDebianVariableIfRequired()
{
	if [ "$PLATFORM" == "Linux" ]; then
		DEBIAN_BUILD_ROOT=/tmp/$BC_EXECUTABLE_NAME-$BC_VERSION-$PACKAGE_NUM-buildroot
		echo "The current debian buildroot is $DEBIAN_BUILD_ROOT"

		if [ -d $DEBIAN_BUILD_ROOT ]; then
       		rm -rf $DEBIAN_BUILD_ROOT
		fi

		echo "Creating the debian buildroot at $DEBIAN_BUILD_ROOT"
		mkdir -p $DEBIAN_BUILD_ROOT
   else
		echo "Platform is not Linux so setting DEBIAN_BUILD_ROOT to blank"
        DEBIAN_BUILD_ROOT=""
   fi

}
createDebianPackageIfRequired()
{
   if [ "$PLATFORM" == "Linux" ]; then
	echo "Creating debian package"
	echo "Editing the debian control file for package Sizes"
    CONTROLNAME=$RPM_CURRENT_DIR/dpkg_install_control
	mkdir -p $DEBIAN_BUILD_ROOT/DEBIAN
	cp -rf $RPM_BUILD_ROOT/* $DEBIAN_BUILD_ROOT
	
	dfout=`du -sk $RPM_BUILD_ROOT`
	pkgsize=`echo $dfout | cut -d' ' -f1`
	reqsize=$(( (($pkgsize + 1023 )/1024)*1024 ))
	echo "Package Size of rpm package is $reqsize KB"

	str="s:^REQUIRED_INSTALLATION_SIZE.*:REQUIRED_INSTALLATION_SIZE=$reqsize:;s:__DEPS__:$BC_DEPS:g;s:__BC_VERSION__:$BC_VERSION:g;s:__PACKAGE_NUMBER__:$PACKAGE_NUM:g;s:__BC_SOFTWARE_ID__:$BC_SOFTWAREID:g;s:__BC_VERSION__:$BC_VERSION:g;s:__PACKAGE_NUMBER__:$PACKAGE_NUM:g;s:__BC_DATA_PATH__:$BC_DATA_PATH:g;s:__BC_INSTALL_PATH__:$BC_INSTALL_PATH:g;s:__BC_EXECUTABLE_NAME__:$BC_EXECUTABLE_NAME:g;s:__BC_CONFLICT__:$BC_CONFLICT):g"
	
	echo "using debian control file $CONTROLNAME"
	echo $str
	sed -e "$str" $CONTROLNAME > $DEBIAN_BUILD_ROOT/DEBIAN/control
	
	SCRIPT_PATH=$RPM_CURRENT_DIR/../scripts
	awk -v var="`cat $SCRIPT_PATH/bc_preinstall`" '{ gsub(/__REPLACE_DEBIAN_SCRIPT__/,var,$0); print  }' $SCRIPT_PATH/bc_debian_script > /tmp/bc_debian_script
	sed -e "$str" /tmp/bc_debian_script > $DEBIAN_BUILD_ROOT/DEBIAN/preinst
	rm -rf /tmp/bc_debian_script
	
	awk -v var="`cat $SCRIPT_PATH/bc_postinstall`" '{ gsub(/__REPLACE_DEBIAN_SCRIPT__/,var,$0); print  }' $SCRIPT_PATH/bc_debian_script > /tmp/bc_debian_script
	sed -e "$str" /tmp/bc_debian_script > $DEBIAN_BUILD_ROOT/DEBIAN/postinst	
	rm -rf /tmp/bc_debian_script
	
	awk -v var="`cat $SCRIPT_PATH/bc_preuninstall`" '{ gsub(/__REPLACE_DEBIAN_SCRIPT__/,var,$0); print  }' $SCRIPT_PATH/bc_debian_script > /tmp/bc_debian_script
	sed -e "$str" /tmp/bc_debian_script > $DEBIAN_BUILD_ROOT/DEBIAN/prerm
	rm -rf /tmp/bc_debian_script
	
	awk -v var="`cat $SCRIPT_PATH/bc_postuninstall`" '{ gsub(/__REPLACE_DEBIAN_SCRIPT__/,var,$0); print  }' $SCRIPT_PATH/bc_debian_script > /tmp/bc_debian_script
	sed -e "$str" /tmp/bc_debian_script > $DEBIAN_BUILD_ROOT/DEBIAN/postrm
	rm -rf /tmp/bc_debian_script

	chmod 755 $DEBIAN_BUILD_ROOT/DEBIAN/preinst
	chmod 755 $DEBIAN_BUILD_ROOT/DEBIAN/postinst
	chmod 755 $DEBIAN_BUILD_ROOT/DEBIAN/prerm
	chmod 755 $DEBIAN_BUILD_ROOT/DEBIAN/postrm
	chmod 755 $DEBIAN_BUILD_ROOT/DEBIAN/control
	
	echo "End editing the debian control file for package sizes"
	echo "Building the debian package..."
	#cp $DEBIAN_BUILD_ROOT/DEBIAN/control /tmp/control.bak

	if [ -f /usr/bin/dpkg-deb ] ; then
	        DEBCMD="/usr/bin/dpkg-deb -b"
	else
	        DEBCMD="/bin/dpkg-deb -b"
	fi
	echo $DEBCMD $DEBIAN_BUILD_ROOT
	$DEBCMD $DEBIAN_BUILD_ROOT
	echo "Finished debian package creation with status $?"
   else
       echo "Platform is not Linux so ignoring debian package creation"
   fi
return 0
}

RPM_CURRENT_DIR=`echo $0 | grep ".rpm_packager.sh" | sed -e 's/.rpm_packager.sh//g'`
echo "RPM_CURRENT_DIR =" $RPM_CURRENT_DIR

echo Output dir is $OUTPUT_DIR

if [ ! -d $OUTPUT_DIR ];
then
    echo $OUTPUT_DIR directory doesnt exist, creating
    mkdir -p $OUTPUT_DIR
fi

#### constants to be used in rpm spec #####
RPM_DEFAULT_PERMISSIONS="%defattr(755,root,root)"
RPM_FILE_PERMISSIONS="(755, root, root)"
RPM_SIG_FILE_PERMISSIONS="(400, root, root)"
RPM_DIR_CONST="%dir"
RPM_FILE_CONST="%attr"
###########################################

RPM_SPEC_NAME=$RPM_CURRENT_DIR/rpm_install.spec
echo "Using rpm spec file $RPM_SPEC_NAME"
cp $RPM_SPEC_NAME /tmp/bc_rpm_spec1

BC_DEPS=MFEcma

if [ x"$ARCH_BIT" == x"32" ]; then
SUFFIX_DIR=$CONFIG_TYPE
if [ x"$PLATFORM" == x"Linux" ] ;then
	TARGET=i686
elif [  x"$PLATFORM" == x"AIX" ]; then
	TARGET=ppc
fi

else
SUFFIX_DIR=$CONFIG_TYPE/$ARCH_BIT
TARGET=x86_64
fi

# BC binaries Path
BC_TARGETS_DIR=$BC_BASE/build/makefiles/$SUFFIX_DIR/
echo BC binaries Path $BC_TARGETS_DIR
if [ ! -e $BC_TARGETS_DIR ]; then
	echo BC targets dir $BC_TARGETS_DIR not found
	exit 1
fi


BC_SIG_FILE=$BC_BASE/build/packager/data/ma_msgbus_auth.sig
echo Sig file path is $BC_SIG_FILE
if [ -f $BC_SIG_FILE ]; then
        echo ma_msgbus_auth.sig file found
else
        echo $BC_SIG_FILE not found, Warning continuing without it by touching it
        touch $BC_SIG_FILE
fi

echo copying $BC_SIG_FILE to $BC_TARGETS_DIR for packaging
cp $BC_SIG_FILE $BC_TARGETS_DIR


if [ x"$PLATFORM" == x"Linux" ] && [ -f "/etc/redhat-release" ]
then
# it is mileaccess for MLOS...
LINUX_DIST=`cat /etc/redhat-release | cut -d" " -f1`
fi

# RPM build root path
RPM_BUILD_ROOT=/tmp/$BC_EXECUTABLE_NAME-$BC_VERSION-$PACKAGE_NUM-buildroot
echo The current rpm buildroot is $RPM_BUILD_ROOT
if [ -d $RPM_BUILD_ROOT ]; then
	rm -rf $RPM_BUILD_ROOT
fi
    
echo Creating the rpm build root at $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT

#This will set debian variables if platform is linux 
setDebianVariableIfRequired

# The below env file contains all the required environment variables.
source $RPM_CURRENT_DIR/../bc_packager.env

echo $RPM_DEFAULT_PERMISSIONS >> /tmp/bc_rpm_spec1

str="s:__BC_SOFTWARE_ID__:$BC_SOFTWAREID:g;s:__BC_EXECUTABLE_NAME__:$BC_EXECUTABLE_NAME:g;"
sed -e "$str" $RPM_CURRENT_DIR/../bc_install_files > /tmp/bc_install_files.tmp

# Creating install, data and config dirs in build root
DIRS_LIST=`cat /tmp/bc_install_files.tmp | grep dir: | cut -d: -f2`
for dir in `eval echo $DIRS_LIST`
do
	echo Creating $RPM_BUILD_ROOT/$dir
	mkdir -p $RPM_BUILD_ROOT/$dir
	echo $RPM_DIR_CONST $dir >> /tmp/bc_rpm_spec1
done


# Copying config files into build root
FILES_LIST=`cat /tmp/bc_install_files.tmp | grep config: | cut -d: -f2`
for file in `eval echo $FILES_LIST` 
do
	echo Copying $RPM_CURRENT_DIR/../data/$file $RPM_BUILD_ROOT/$BC_CONFIG_PATH/$BC_SOFTWAREID/$file
	cp $RPM_CURRENT_DIR/../data/$file $RPM_BUILD_ROOT/$BC_CONFIG_PATH/$BC_SOFTWAREID/$file
	echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS $BC_CONFIG_PATH/$BC_SOFTWAREID/$file >> /tmp/bc_rpm_spec1
done

# Copying scripts into build root
SCRIPT_LIST=`cat /tmp/bc_install_files.tmp | grep script: | cut -d: -f2`
for file in `eval echo $SCRIPT_LIST` 
do
	echo Copying $RPM_CURRENT_DIR/../scripts/$PLATFORM/$file $RPM_BUILD_ROOT/$BC_INSTALL_PATH/scripts/$file
	cp $RPM_CURRENT_DIR/../scripts/$PLATFORM/$file $RPM_BUILD_ROOT/$BC_INSTALL_PATH/scripts/$file
	echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS $BC_INSTALL_PATH/scripts/$file >> /tmp/bc_rpm_spec1
done

# Copying all bc install bin files into build root
BINS_LIST=`cat /tmp/bc_install_files.tmp | grep bin: | cut -d: -f2`
for file in `eval echo $BINS_LIST` 
do
	if [ -e $BC_TARGETS_DIR/$file ]; then
		echo Copying file $BC_TARGETS_DIR/$file to $RPM_BUILD_ROOT/$BC_INSTALL_PATH/bin/$file
		cp $BC_TARGETS_DIR/$file $RPM_BUILD_ROOT/$BC_INSTALL_PATH/bin/$file
		echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS $BC_INSTALL_PATH/bin/$file >> /tmp/bc_rpm_spec1
	fi
done

# Update placeholders in config.xml
BC_START_CMD="$BC_INIT_PATH/beacon start"
BC_STOP_CMD="$BC_INIT_PATH/beacon stop"	

str="s:__BC_INIT_PATH__:$BC_INIT_PATH:g;s:__BC_SOFTWARE_ID__:$BC_SOFTWAREID:g;s:__BC_VERSION__:$BC_VERSION:g;s:__PACKAGE_NUMBER__:$PACKAGE_NUM:g;s:__BC_DATA_PATH__:$BC_DATA_PATH:g;s:__BC_INSTALL_PATH__:$BC_INSTALL_PATH:g;s:__BC_START_CMD__:$BC_START_CMD:g;s:__BC_STOP_CMD__:$BC_STOP_CMD:g"
sed -e "$str" $RPM_CURRENT_DIR/../data/config.xml > $RPM_BUILD_ROOT/$BC_CONFIG_PATH/$BC_SOFTWAREID/config.xml

str="s:__BC_INIT_PATH__:$BC_INIT_PATH:g;s:__BC_SOFTWARE_ID__:$BC_SOFTWAREID:g;s:__BC_EXECUTABLE_NAME__:$BC_EXECUTABLE_NAME:g;s:__BC_VERSION__:$BC_VERSION:g;s:__PACKAGE_NUMBER__:$PACKAGE_NUM:g;s:__BC_DATA_PATH__:$BC_DATA_PATH:g;s:__BC_INSTALL_PATH__:$BC_INSTALL_PATH:g"
sed -e "$str" $RPM_CURRENT_DIR/../scripts/$PLATFORM/beacon > $RPM_BUILD_ROOT/$BC_INSTALL_PATH/scripts/beacon
HasCRLFHenceExit $RPM_BUILD_ROOT

##### Adding pre/post install/uninstall scripts ######
SCRIPT_PATH=$RPM_CURRENT_DIR/../scripts
if [ x"$PLATFORM" == x"AIX" ] ;then
	parseFile "$RPM_CURRENT_DIR/.." "/tmp/bc_rpm_spec1" "/tmp/bc_rpm_spec7" 
else
	awk -v var="`cat $SCRIPT_PATH/bc_prepinstall`" '{ gsub(/__REPLACE_PREPINSTALL_SCRIPT__/,var,$0); print  }' /tmp/bc_rpm_spec1 > /tmp/bc_rpm_spec2
	awk -v var="`cat $SCRIPT_PATH/bc_install`" '{ gsub(/__REPLACE_INSTALL_SCRIPT__/,var,$0); print  }' /tmp/bc_rpm_spec2 > /tmp/bc_rpm_spec3
	awk -v var="`cat $SCRIPT_PATH/bc_preinstall`" '{ gsub(/__REPLACE_PREINSTALL_SCRIPT__/,var,$0); print  }' /tmp/bc_rpm_spec3 > /tmp/bc_rpm_spec4
	awk -v var="`cat $SCRIPT_PATH/bc_postinstall`" '{ gsub(/__REPLACE_POSINSTALL_SCRIPT__/,var,$0); print  }' /tmp/bc_rpm_spec4 > /tmp/bc_rpm_spec5
	awk -v var="`cat $SCRIPT_PATH/bc_preuninstall`" '{ gsub(/__REPLACE_PREUNINSTALL_SCRIPT__/,var,$0); print  }' /tmp/bc_rpm_spec5 > /tmp/bc_rpm_spec6
	awk -v var="`cat $SCRIPT_PATH/bc_postuninstall`" '{ gsub(/__REPLACE_POSTUNINSTALL_SCRIPT__/,var,$0); print  }' /tmp/bc_rpm_spec6 > /tmp/bc_rpm_spec7
fi
#####################################################


dfout=`du -sk $RPM_BUILD_ROOT`
pkgsize=`echo $dfout | cut -d' ' -f1`
reqsize=$(( (($pkgsize + 1023 )/1024)*1024 ))
echo "Package Size of rpm package is $reqsize KB"

str="s:__PLATFORM__:$PLATFORM:g;s:__BC_INIT_PATH__:$BC_INIT_PATH:g;s:^REQUIRED_INSTALLATION_SIZE.*:REQUIRED_INSTALLATION_SIZE=$reqsize:;s:__DEPS__:$BC_DEPS:g;s:__BC_VERSION__:$BC_VERSION:g;s:__PACKAGE_NUMBER__:$PACKAGE_NUM:g;s:__BC_SOFTWARE_ID__:$BC_SOFTWAREID:g;s:__BC_VERSION__:$BC_VERSION:g;s:__PACKAGE_NUMBER__:$PACKAGE_NUM:g;s:__BC_DATA_PATH__:$BC_DATA_PATH:g;s:__BC_INSTALL_PATH__:$BC_INSTALL_PATH:g;s:__BC_EXECUTABLE_NAME__:$BC_EXECUTABLE_NAME:g;s:__BC_CONFLICT__:$BC_CONFLICT):g"
sed -e "$str" /tmp/bc_rpm_spec7 > /tmp/bc_rpm_spec8
echo "End editing the rpm spec file for package sizes"
echo "Building the rpm package..."
cp /tmp/bc_rpm_spec8 /tmp/bc_rpm_spec.bak
if [ -f /usr/bin/rpmbuild ] ; then 
	RPMCMD="/usr/bin/rpmbuild -bb"
else
	RPMCMD="/bin/rpm -bb"
fi

RPMDIR="packages"

if [ x"mileaccess" == x"$LINUX_DIST" ]
then
        echo  Creating tar of file for MLOS rpmbuld
        if [ -f /tmp/$BC_EXECUTABLE_NAME-$BC_VERSION-$PACKAGE_NUM.tar ]; then
          rm /tmp/$BC_EXECUTABLE_NAME-$BC_VERSION-$PACKAGE_NUM.tar
        fi

        cd $RPM_BUILD_ROOT
        tar cf /tmp/$BC_EXECUTABLE_NAME-$BC_VERSION-$PACKAGE_NUM.tar *
        cd $RPM_CURRENT_DIR
fi
mkdir ${RPM_BUILD_ROOT}_tmp
cp -rf ${RPM_BUILD_ROOT}/* ${RPM_BUILD_ROOT}_tmp
echo $RPMCMD --buildroot "$RPM_BUILD_ROOT" --target $TARGET /tmp/bc_rpm_spec8
cd
$RPMCMD --buildroot "$RPM_BUILD_ROOT" --target $TARGET /tmp/bc_rpm_spec8
echo "Finished rpm package creation with status $?"

createDebianPackageIfRequired

echo "Copying Beacon rpm files to output directory, $OUTPUT_DIR"


if [ x"mileaccess" == x"$LINUX_DIST" ];then
	# For MLOS
	mv ~/rpmbuild/RPMS/$TARGET/$BC_EXECUTABLE_NAME-$BC_VERSION-$PACKAGE_NUM.$TARGET.rpm $OUTPUT_DIR/$BC_EXECUTABLE_NAME-$BC_VERSION-$PACKAGE_NUM.$TARGET.mlos.rpm
elif [ "$PLATFORM" == "Linux" ]; then
		# For non MLOS
		if [ -f /usr/src/$RPMDIR/RPMS/$TARGET/$BC_EXECUTABLE_NAME-$BC_VERSION-$PACKAGE_NUM.$TARGET.rpm ]; then
			mv /usr/src/$RPMDIR/RPMS/$TARGET/$BC_EXECUTABLE_NAME-$BC_VERSION-$PACKAGE_NUM.$TARGET.rpm $OUTPUT_DIR/$BC_EXECUTABLE_NAME-$BC_VERSION-$PACKAGE_NUM.$TARGET.$PLATFORM.rpm
		else
			mv ~/rpmbuild/RPMS/$TARGET/$BC_EXECUTABLE_NAME-$BC_VERSION-$PACKAGE_NUM.$TARGET.rpm $OUTPUT_DIR/$BC_EXECUTABLE_NAME-$BC_VERSION-$PACKAGE_NUM.$TARGET.$PLATFORM.rpm
		fi
		#Copying debian package
		mv /tmp/$BC_EXECUTABLE_NAME-$BC_VERSION-$PACKAGE_NUM-buildroot.deb $OUTPUT_DIR/$BC_EXECUTABLE_NAME-$BC_VERSION-$PACKAGE_NUM.$TARGET.$PLATFORM.deb
		if (( $? )); then
				echo "Unable to copy ma deb to $OUTPUT_DIR "
		else
			echo "Successfully copied debian package to output directory $outdir"
		fi
elif [ "$PLATFORM" == "AIX" ]; then
	AIX_VER="aix5.3"
	if [ -f /opt/freeware/src/packages/RPMS/$TARGET/$BC_EXECUTABLE_NAME-$BC_VERSION-$PACKAGE_NUM.$AIX_VER.$TARGET.rpm ]; then
		mv /opt/freeware/src/packages/RPMS/$TARGET/$BC_EXECUTABLE_NAME-$BC_VERSION-$PACKAGE_NUM.$AIX_VER.$TARGET.rpm $OUTPUT_DIR/$BC_EXECUTABLE_NAME-$BC_VERSION-$PACKAGE_NUM.$AIX_VER.$TARGET.$PLATFORM.rpm
	fi
	if (( $? )); then
		echo "[Error]: copy failed of file \"/opt/freeware/src/packages/RPMS/$TARGET/$BC_EXECUTABLE_NAME-$BC_VERSION-$PACKAGE_NUM.$AIX_VER.$TARGET.rpm\" to \"$OUTPUT_DIR/$BC_EXECUTABLE_NAME-$BC_VERSION-$PACKAGE_NUM.$AIX_VER.$TARGET.$PLATFORM.rpm\"";
	else
			echo "Successfully copied package to output directory $outdir"
	fi
fi
if [ -f /tmp/$BC_EXECUTABLE_NAME-$BC_VERSION-$PACKAGE_NUM.tar ]; then
		rm /tmp/$BC_EXECUTABLE_NAME-$BC_VERSION-$PACKAGE_NUM.tar
fi
if (( $? )); then
	echo "Unable to move Beacon rpm to $OUTPUT_DIR "
fi

if [ -d $RPM_BUILD_ROOT ]; then
	rm -rf $RPM_BUILD_ROOT
fi
if [ -d $RPM_BUILD_ROOT ]; then
	rm -rf $RPM_BUILD_ROOT
fi
if [ -d ${RPM_BUILD_ROOT}_tmp ]; then
	rm -rf ${RPM_BUILD_ROOT}_tmp
fi
if [ -d $DEBIAN_BUILD_ROOT ]; then
	rm -rf $DEBIAN_BUILD_ROOT
fi
rm -fr /tmp/bc_install_files.tmp
rm -rf /tmp/bc_rpm_spec1
rm -rf /tmp/bc_rpm_spec2
rm -rf /tmp/bc_rpm_spec3
rm -rf /tmp/bc_rpm_spec4
rm -rf /tmp/bc_rpm_spec5
rm -rf /tmp/bc_rpm_spec6
rm -rf /tmp/bc_rpm_spec7
rm -rf /tmp/bc_rpm_spec8
cp  /tmp/bc_rpm_spec.bak $OUTPUT_DIR
rm -rf /tmp/bc_rpm_spec.bak
