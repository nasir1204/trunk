#!/bin/bash

printParams()
{
	echo "version=" $version
	echo "package_num=" $package_num
	echo "config_type=" $config_type
	echo "platform=" $platform
	echo "arch_bit=" $arch_bit
	echo "output_dir=" $output_dir
	echo "date_and_time=" $date_and_time
}

ValidateParams()
{
	MAJVER=`echo $version | cut -d. -f1`
	MINVER=`echo $version | cut -d. -f2`
	SUBVER=`echo $version | cut -d. -f3`

	if [ "$MAJVER" == "" ] || [ "$MINVER" == "" ] || [ "$SUBVER" == "" ]
      then
		echo "Version Details are not correct"
        	Usage 
        fi

	if [ "$platform" != "Linux" ] && [ "$platform" != "Darwin" ] && [ "$platform" != "HP-UX" ] && [ "$platform" != "AIX" ] && [ "$platform" != "SunOS" ] && [ "$platform" != "FreeBSD" ] 
	then
    		echo "Error: " $platform "Platform is not supported"
    		exit 3
	fi

	if [ "$config_type" != "release" ] && [ "$config_type" != "debug" ] && [ "$config_type" != "all" ]
	then
    		Usage
	fi

	if [ "$arch_bit" != "32" ] && [ "$arch_bit" != "64" ] && [ "$arch_bit" != "all" ]
	then
    		Usage
	fi
}

# Usage 
Usage()
{
    echo "Usage: bash <Absolute path>/build/packager/unix/bc_packager.sh -v <version> -n <build number> -c <config type> -p <platform> -b <arch-bit> -o <output directory>"
    echo "   -v version i.e. 5.0.0 (Required)"
    echo "   -n build number i.e. 143 (Required)"
    echo "   -c configuration type i.e release/debug/all default is release (optional)"
    echo "   -p unix platform type, output of 'uname -s' i.e. Linux/SunOS/HP-UX/AIX/Darwin/FreeBSD (Required)"
    echo "   -b bit type i.e. 32/64/all default is 32 (optional)"
    echo "   -s date/time stamp - YYYYMMDDHHMMSS"
    echo "   -o output directory i.e. <Absolute path> e.g. /abc/xyz"
    exit 1
}

# Enable debugging option if SH_DEBUG=true
if [ "x$SH_DEBUG" == "xtrue" ]
then
	set -xv
fi

scriptFullPath=$0
firstchar=`echo $scriptFullPath | grep "^\/"`

if [ "$firstchar" == "" ]; then
    echo "The build script path is not an absolute path"
    Usage
fi

CURRENT_DIR=`echo $scriptFullPath | grep ".bc_packager.sh" | sed -e 's/.bc_packager.sh//g'`

echo "CURRENT_DIR =" $CURRENT_DIR

if [ "x$BC_BASE" == "x" ]
then
BC_BASE=`echo $CURRENT_DIR | sed -e 's/.unix//g' | sed -e 's/.packager//g' | sed -e 's/.build//g'`
fi

echo "BC_BASE =" $BC_BASE

if [ "x$MA_SDK_PATH" == "x" ]
then
MA_SDK_PATH=$BC_BASE/../ma_sdk
fi

echo "MA_SDK_PATH=" $MA_SDK_PATH

# Parsing build script arguments 
while getopts v:n:c:p:b:o:s: opt
  do
  case $opt in
      v)
	  	version=$OPTARG;;
      n)
	  	package_num=$OPTARG;;
      c)
	  	config_type=$OPTARG;;
      p)
        platform=$OPTARG;;
      b)  
	  	arch_bit=$OPTARG;;
      o)  
	  	output_dir=$OPTARG;;
      s) 
        date_and_time=$OPTARG;;
      h)
	  	Usage;;
      \?) 
        Usage;;
  esac
done
shift `expr $OPTIND - 1` 

# Assigning default values for optional parameters if not specified
[ -z $version ] && Usage
[ -z $package_num ] && Usage 
[ -z $config_type ] && config_type=release
[ -z $platform ] && Usage 
[ -z $arch_bit ] && arch_bit=32
[ -z $output_dir ] && Usage
[ -z $date_and_time ] && date_and_time=00000000000000 

# Printing all parameter values before validation
printParams

# Validating the path and parameters specified
ValidateParams

export BC_BASE 
export MA_SDK_PATH 
export BC_VERSION=$version
export PACKAGE_NUM=$package_num 
export PLATFORM=$platform
export OUTPUT_DIR=$output_dir 
export DATE_AND_TIME=$date_and_time

if [ "$config_type" == "all" ]
then
configuration="release debug"
else
configuration=$config_type
fi

if [ "$arch_bit" == "all" ]
then
bit_type="32 64"
else
bit_type=$arch_bit
fi


export BC_EXECUTABLE_NAME=beacon
export BC_CONFLICT=beacon++
echo "Creating Beacon Packaging for $platform Platform"
for bit in $bit_type
do
for config in $configuration
do
echo "********** " $bit bit $config "Packaging Start **********"
if [ "$platform" == "Linux" ] || [ "$platform" == "AIX" ]
then
	export ARCH_BIT=$bit
	export CONFIG_TYPE=$config 
	bash $CURRENT_DIR/rpm/rpm_packager.sh
	if [ $? -ne 0 ]
	then
		echo "********** " $bit bit $config "Packaging Failed.... "
		exit 1
	else
		echo "********** " $bit bit $config "Packaging End **********"
	fi
fi
if [ "$platform" == "Darwin" ]
then
	export ARCH_BIT=$bit
	export CONFIG_TYPE=$config 
	bash $CURRENT_DIR/dmg/dmg_packager.sh
	if [ $? -ne 0 ]
	then
		echo "********** " $bit bit $config "Packaging Failed.... "
		exit 1
	else
		echo "********** " $bit bit $config "Packaging End **********"
	fi
fi
done
done

export BC_EXECUTABLE_NAME=beacon++
export BC_CONFLICT=beacon
echo "Creating Beacon CXX Packaging for $platform Platform"
for bit in $bit_type
do
for config in $configuration
do
echo "********** " $bit bit $config "Packaging Start **********"
if [ "$platform" == "Linux" ] || [ "$platform" == "AIX" ]
then
	export ARCH_BIT=$bit
	export CONFIG_TYPE=$config 
	bash $CURRENT_DIR/rpm/rpm_packager.sh
	if [ $? -ne 0 ]
	then
		echo "********** " $bit bit $config "Packaging Failed.... "
		exit 1
	else
		echo "********** " $bit bit $config "Packaging End **********"
	fi
fi
if [ "$platform" == "Darwin" ]
then
	export ARCH_BIT=$bit
	export CONFIG_TYPE=$config 
	bash $CURRENT_DIR/dmg/dmg_packager.sh
	if [ $? -ne 0 ]
	then
		echo "********** " $bit bit $config "Packaging Failed.... "
		exit 1
	else
		echo "********** " $bit bit $config "Packaging End **********"
	fi
fi
done
done

exit 0
