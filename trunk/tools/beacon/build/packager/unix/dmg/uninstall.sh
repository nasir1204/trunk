#!/bin/sh

SERVERIP=
PORTNUMBER=

Error()
{
    echo $* > /dev/stderr
}

Echo()
{
    echo $* 
}

Usage()
{
    Echo "Usage: uninstall.sh" 
    Echo " -h  The usage message" 
    Echo "You will need the +++root privileges+++ to uninstall this software"
    Echo "Either +++su to root+++ or use +++sudo cmduninst+++ to uninstall the software"
    exit 2
}

uid=`id -u`
if (( $uid != 0 )); then
    Usage
    exit -1
fi

if (( $# > 0 )); then
    Usage
    exit -1
fi

#if [ -d /etc/cma.d ]; then
#        for i in /etc/cma.d/*
#        do
#                if [ ! -d $i ]; then
#                        continue
#                fi
#                configname=$i/config.xml
#                if [ -f $configname ]; then
#                        productname=`grep "\<ProductName\>" $configname | sed 's/.*<ProductName>\(.*\)<\/ProductName>.*/\1/'`
#                       	isProductCMA=""
#			isProductCMA=`echo $i | grep 'EPOAGENT\|CMNUPD\|CMDAGENT' `
#			UnloadCmaPluginCmd=`grep "\<UnloadCmaPluginCommand\>" $configname | sed 's/.*<UnloadCmaPluginCommand>\(.*\)<\/UnloadCmaPluginCommand>.*/\1/'`
#			if [ -z "$isProductCMA" ]; then
#				
#				if [ "$productname" != "" ]; then
#					echo "For Product Name : $productname"
#				fi
#
#				if [ -n "$UnloadCmaPluginCmd" ]; then
#					$UnloadCmaPluginCmd
#					RC=$?
#					if [ "$RC" == "0" ] ; then
#						echo "Successfully notified $i to unload cma plugins"
#						echo ""
#					fi
#				fi
#			fi
#               fi
#        done
#fi

echo "Stopping beacon service"
SystemStarter stop beacon 
echo "Done!!!"
echo "Removing beacon configuration information"
rm -rf /Library/mileaccess/beacon
rm -rf /var/mileaccess/beacon
rm -rf /etc/ma.d/BEACON__1000
rm -rf /Library/StartupItems/beacon
#rm -rf /Library/mileaccess/shared
sudo /usr/sbin/pkgutil --forget comp.nai.beaconmac > /dev/null 2>&1
echo "Beacon uninstalled"

exit 0
