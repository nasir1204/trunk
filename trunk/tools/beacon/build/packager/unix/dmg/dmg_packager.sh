#!/bin/bash

#########################################
# Don't invoke this script directly, it must be invoked via bc_packager.sh
#########################################
set -x
HasCRLFHenceExit()
{
    error=0
    for i in `find $1`
    do
		file $i | cut -d: -f2 | grep -e "\([Ss]cript\)\|\(ASCII..*text\)" >/dev/null
      	if (( $? == 0 )); then
	  	str1=`strings -1 $i | head -1`
	  	str2=`cat $i | head -1`
	  	if [ "$str1" != "$str2" ]; then
	    	echo $i 
	    	error=1
	  	fi
      	fi
    done
    if (( $error != 0 )); then
	echo "Have CRLF in Text file $i" 
	file $i
	#Sdas to fix
	#exit 4;
    fi
}


DMG_CURRENT_DIR=`echo $0 | grep ".dmg_packager.sh" | sed -e 's/.dmg_packager.sh//g'`
echo "DMG_CURRENT_DIR =" $DMG_CURRENT_DIR

echo Output dir is $OUTPUT_DIR

if [ ! -d $OUTPUT_DIR ];
then
    echo $OUTPUT_DIR directory doesnt exist, creating
    mkdir -p $OUTPUT_DIR
fi

BC_DEPS=MFEma

if [ x"$ARCH_BIT" == x"32" ]
then
SUFFIX_DIR=$CONFIG_TYPE
TARGET=i686
else
SUFFIX_DIR=$CONFIG_TYPE/$ARCH_BIT
TARGET=x86_64
fi

# BC binaries Path
BC_TARGETS_DIR=$BC_BASE/build/makefiles/$SUFFIX_DIR/
echo BC binaries Path $BC_TARGETS_DIR
if [ ! -e $BC_TARGETS_DIR ]; then
	echo BC targets dir $BC_TARGETS_DIR not found
	exit 1
fi

BC_SIG_FILE=$BC_BASE/build/packager/data/ma_msgbus_auth.sig
echo Sig file path is $BC_SIG_FILE
if [ -f $BC_SIG_FILE ]; then
        echo ma_msgbus_auth.sig file found
else
        echo $BC_SIG_FILE not found, Warning continuing without it by touching it
        touch $BC_SIG_FILE
fi

echo copying $BC_SIG_FILE to $BC_TARGETS_DIR for packaging
cp $BC_SIG_FILE $BC_TARGETS_DIR


DSTROOT=$OUTPUT_DIR/DSTROOT

if [ -d $DSTROOT ]; then
	rm -rf $DSTROOT
fi
    
echo "Creating DSTROOT directory at $DSTROOT"
mkdir -p $DSTROOT

#This will set debian variables if platform is linux 
# setDebianVariableIfRequired

# The below env file contains all the required environment variables.
source $DMG_CURRENT_DIR/../bc_packager.env

str="s:__BC_SOFTWARE_ID__:$BC_SOFTWAREID:g;s:__BC_EXECUTABLE_NAME__:$BC_EXECUTABLE_NAME:g;"
sed -e "{$str}" $DMG_CURRENT_DIR/../bc_install_files > /tmp/bc_install_files.tmp

# Creating install, data and config dirs in build root
DIRS_LIST=`cat /tmp/bc_install_files.tmp | grep dir: | cut -d: -f2`
for dir in `eval echo $DIRS_LIST`
do
	echo Creating $DSTROOT/$dir
	mkdir -p $DSTROOT/$dir
done


# Copying config files into build root
FILES_LIST=`cat /tmp/bc_install_files.tmp | grep config: | cut -d: -f2`
for file in `eval echo $FILES_LIST` 
do
	echo Copying $DMG_CURRENT_DIR/../data/$file $DSTROOT/$BC_CONFIG_PATH/$BC_SOFTWAREID/$file
	cp $DMG_CURRENT_DIR/../data/$file $DSTROOT/$BC_CONFIG_PATH/$BC_SOFTWAREID/$file
done

# Copying scripts into build root
SCRIPT_LIST=`cat /tmp/bc_install_files.tmp | grep script: | cut -d: -f2`
for file in `eval echo $SCRIPT_LIST` 
do
	echo Copying $DMG_CURRENT_DIR/../scripts/$PLATFORM/$file $DSTROOT/$BC_INSTALL_PATH/scripts/$file
	cp $DMG_CURRENT_DIR/../scripts/$PLATFORM/$file $DSTROOT/$BC_INSTALL_PATH/scripts/$file
done

# Copying all bc install bin files into build root
BINS_LIST=`cat /tmp/bc_install_files.tmp | grep bin: | cut -d: -f2`
for file in `eval echo $BINS_LIST` 
do
	if [ -e $BC_TARGETS_DIR/$file ]; then
		echo Copying file $BC_TARGETS_DIR/$file to $DSTROOT/$BC_INSTALL_PATH/bin/$file
		cp $BC_TARGETS_DIR/$file $DSTROOT/$BC_INSTALL_PATH/bin/$file
	fi
done

# Update placeholders in config.xml
BC_START_CMD="SystemStarter start beacon"
BC_STOP_CMD="SystemStarter stop beacon"

source $DMG_CURRENT_DIR/../bc_packager.env
str="s:__BC_SOFTWARE_ID__:$BC_SOFTWAREID:g;s:__BC_VERSION__:$BC_VERSION:g;s:__PACKAGE_NUMBER__:$PACKAGE_NUM:g;s:__BC_DATA_PATH__:$BC_DATA_PATH:g;s:__BC_INSTALL_PATH__:$BC_INSTALL_PATH:g;s:__BC_START_CMD__:$BC_START_CMD:g;s:__BC_STOP_CMD__:$BC_STOP_CMD:g;"
sed -e "{$str}" $DMG_CURRENT_DIR/../data/config.xml > $DSTROOT/$BC_CONFIG_PATH/$BC_SOFTWAREID/config.xml

str="s:__BC_SOFTWARE_ID__:$BC_SOFTWAREID:g;s:__BC_EXECUTABLE_NAME__:$BC_EXECUTABLE_NAME:g;s:__BC_VERSION__:$BC_VERSION:g;s:__PACKAGE_NUMBER__:$PACKAGE_NUM:g;s:__BC_DATA_PATH__:$BC_DATA_PATH:g;s:__BC_INSTALL_PATH__:$BC_INSTALL_PATH:g;"
sed -e "{$str}" $DMG_CURRENT_DIR/../scripts/$PLATFORM/beacon > $DSTROOT/$BC_INSTALL_PATH/scripts/beacon

mkdir -p $DSTROOT/Library/StartupItems/
mkdir -p $signing_dir/sign_in
mkdir -p $signing_dir/sign_out
rm -f $signing_dir/sign_in/*
rm -f $signing_dir/sign_out/*


echo "Copying additional items"
cp $DMG_CURRENT_DIR/Info.plist $OUTPUT_DIR
cp -pRf $DMG_CURRENT_DIR/Resources $OUTPUT_DIR
cp $DMG_CURRENT_DIR/beacon.info $OUTPUT_DIR
cp $DMG_CURRENT_DIR/beacon_raw.pkgproj $OUTPUT_DIR
cp $DMG_CURRENT_DIR/beacon.pkgproj $OUTPUT_DIR
cp $DMG_CURRENT_DIR/uninstall.sh $DSTROOT/Library/mileaccess/beacon/bin/
cp $DMG_CURRENT_DIR/beacon.plist $DSTROOT/Library/LaunchDaemons/beacon.plist
cp -r $DMG_CURRENT_DIR/StartupItems/beacon $DSTROOT/Library/StartupItems/beacon
echo creating package

echo Checking CRLF
HasCRLFHenceExit $DSTROOT

cd  $DSTROOT
echo changing permissions
for i in `find . -type f`
do
  chmod 755 $i
done
cd  -

echo Listing out dstroot
ls -al $DSTROOT


########### Signing binaries ##########

## TODO : libmfelpc bundle, Enable signing in buildroom

#bash $CURRENT_DIR/binary_sign.sh $DSTROOT/$MA_INSTALL_PATH/bin $SIGN_DIR "bin"

if [ $? != 0 ]; then
    echo "Binaries signing failed"
#    exit 1;
fi

cp -f $SIGN_DIR/sign_out/* $DSTROOT/$MA_INSTALL_PATH/bin
rm -f $DSTROOT/$MA_INSTALL_PATH/bin/sign_*

########### create packages ###########

echo change dir to $OUTPUT_DIR
cd $OUTPUT_DIR
mkdir -p $OUTPUT_DIR/build 
rm -f $OUTPUT_DIR/build/*

# TODO : Add Startup items in beacon_raw.pkgproj

echo now build raw packages.
/usr/local/bin/packagesbuild  -v beacon_raw.pkgproj

if [ ! -f $OUTPUT_DIR/build/beacon.pkg ]; then
   echo "failed to create agent raw pkg"
   exit 1
fi
rm -f $OUTPUT_DIR/beacon_raw.pkg
cp $OUTPUT_DIR/build/beacon.pkg $OUTPUT_DIR/beacon_raw.pkg

########## create dist packages #########
echo change dir to $OUTPUT_DIR
cd $OUTPUT_DIR
mkdir -p $OUTPUT_DIR/dist
rm -f $OUTPUT_DIR/dist/*

echo now build the dist package.
/usr/local/bin/packagesbuild  -v beacon.pkgproj
if [ ! -f $OUTPUT_DIR/dist/beacon.pkg ]; then
    echo "failed to create agent distribution pkg"
    exit 1
fi

############# Signing packages ###########################

#bash $CURRENT_DIR/binary_sign.sh $OUTPUT_DIR/dist/ $SIGN_DIR "pkg"
if [ $? != 0 ]; then
    echo "Package signing failed"
#    exit 1
fi

rm -f $OUTPUT_DIR/beacon.pkg
cp $SIGN_DIR/sign_out/beacon.pkg $OUTPUT_DIR/beacon.pkg

############  temp overriding signing 

cp $OUTPUT_DIR/dist/beacon.pkg $OUTPUT_DIR/beacon.pkg

###########################################################
#create disk image
cd  $OUTPUT_DIR
if (( $? != 0 )); then
     echo "packaging Failed. Exiting..."
     exit 1
fi

#check for the disk size required
disksize=`du -sk $OUTPUT_DIR/beacon.pkg | cut -f1`
echo "Calculated required disk size using du command is, $disksize"

disksize=$(( (disksize + 1123)/1024 ))
if (( $disksize < 7 )); then
    disksize=10
else
    disksize=`expr $disksize + 2`
fi
echo "Used disk size is: $disksize"

# create a 10MB disk image

/usr/bin/hdiutil create -size "$disksize"m beacon10mb.dmg -layout NONE
if (( $? != 0 )); then
    echo "Unable to Create beacon10mb.dmg exiting..."
    exit 1
fi
# associate a device with this but don't mount it
MYDEV=$(/usr/bin/hdid -nomount beacon10mb.dmg)
# create a file system
/sbin/newfs_hfs -v MFEBEACON $MYDEV
if (( $? != 0 )); then
    echo "Unable to Create filesystem on beacon10mb.dmg exiting..."
    exit 1
fi
# diassociate device
/usr/bin/hdiutil eject $MYDEV
# mount it
MNTNAME=`/usr/bin/hdid beacon10mb.dmg`
MNTF2=`echo $MNTNAME | cut -d' ' -f2`
MNTF3=`echo $MNTNAME | cut -d' ' -f3`
echo F2 $MNTF2
echo F3 $MNTF3
echo Mount name $MNTNAME

if [ ! -z "$MNTF3" ]; then
    MNTDIR="$MNTF2\ $MNTF3"
else
    MNTDIR="$MNTF2"
fi
echo "Created Mount $MNTDIR" 
if (( $? != 0 )); then
    echo "Mounting beacon10mb.dmg failed. exiting..."
    exit 1
fi
# copy stuff to it
/bin/cp -R beacon.pkg $MNTDIR
echo /bin/cp -R beacon.pkg "$MNTDIR"
if (( $? != 0 )); then
    /usr/bin/hdiutil eject $MYDEV
    echo "Copying package failed. exiting..."
    exit 1
fi

# eject
/usr/bin/hdiutil eject $MYDEV
if (( $? != 0 )); then
    echo "Unable to eject device beacon10mb.dmg. exiting..."
    exit 1
fi
rm $OUTPUT_DIR/beacon.dmg
# compress it and make it read only
/usr/bin/hdiutil convert -format UDZO beacon10mb.dmg -o beacon.dmg
if (( $? != 0 )); then
    echo "Compressing beacon10mb.dmg to beacon.dmg failed. exiting..."
    exit 1
fi

rm -rf $OUTPUT_DIR/beacon10mb.dmg $OUTPUT_DIR/Resources $OUTPUT_DIR/Info.plist $OUTPUT_DIR/CMA.Info
