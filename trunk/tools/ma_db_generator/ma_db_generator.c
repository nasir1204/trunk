#include "ma_db_generator.h"

static ma_console_logger_t *logger = NULL ;

void main() 
{
    ma_console_logger_create(&logger) ;
    ma_db_intialize() ;
    create_agent_default_db() ;
    //create_agent_policy_db() ;
    //create_agent_events_db() ;
    //create_agent_schedular_db() ;
    ma_db_deintialize() ;
}