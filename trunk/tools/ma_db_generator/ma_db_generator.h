#ifndef AGENT_DB_H
#define AGENT_DB_H

#include "ma/ma_common.h"
#include "ma/ma_datastore.h" 
#include "ma/internal/utils/database/ma_db.h"
#include "ma/internal/utils/database/ma_db_recordset.h"
#include "ma/internal/utils/database/ma_db_statement.h"
#include "ma/logger/ma_console_logger.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"

#include <stdio.h>

#define MA_POLICY_DB_FILENAME  "./ma_policy.db"
#define MA_DB_FILENAME  "./ma.db"
#define MA_EVENTS_DB_FILENAME  "./ma_events.db"
#define MA_SCHEDULAR_DB_FILENAME  "./ma_scheduler.db"

ma_error_t create_agent_default_db() ;
ma_error_t create_agent_policy_db() ;
ma_error_t create_agent_events_db() ;
ma_error_t create_agent_scheduler_db() ;

#endif  //AGENT_DB_H