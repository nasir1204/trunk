#include "ma_db_generator.h"

#include <string.h>
#include <stdlib.h>

extern ma_console_logger_t *logger ;

#define CREATE_PARENT_TABLE_SQL_STATEMENT "CREATE TABLE IF NOT EXISTS AGENT_PARENT (ID INTEGER NOT NULL UNIQUE, PATH TEXT NOT NULL UNIQUE, PRIMARY KEY (ID) ON CONFLICT REPLACE )"
#define CREATE_CHILD_TABLE_SQL_STATEMENT  "CREATE TABLE IF NOT EXISTS AGENT_CHILD (ID INTEGER NOT NULL, NAME TEXT NOT NULL, VALUE BLOB NOT NULL, PRIMARY KEY (ID, NAME) ON CONFLICT REPLACE , FOREIGN KEY (ID) REFERENCES AGENT_PARENT(ID)  ON UPDATE CASCADE ON DELETE CASCADE )"

static ma_error_t add_tables(ma_db_t *db_handle) {
    ma_db_statement_t *db_stmt = NULL ;

    ma_db_statement_create(db_handle, CREATE_PARENT_TABLE_SQL_STATEMENT, &db_stmt) ;
    ma_db_statement_execute(db_stmt) ;
    ma_db_statement_release(db_stmt) ; db_stmt = NULL ;

    ma_db_statement_create(db_handle, CREATE_CHILD_TABLE_SQL_STATEMENT, &db_stmt) ;
    ma_db_statement_execute(db_stmt) ;
    ma_db_statement_release(db_stmt) ; db_stmt = NULL ;

    return MA_OK ;
}

static ma_error_t add_parent_root(ma_db_t *db_handle) {
    ma_db_statement_t *db_stmt = NULL ;

    ma_db_statement_create(db_handle, "INSERT INTO AGENT_PARENT VALUES (0 , 'AGENT')", &db_stmt) ;
    ma_db_statement_execute(db_stmt) ;
    ma_db_statement_release(db_stmt) ; db_stmt = NULL ;

    return MA_OK ;
}

#define VERSION_KEY         "AGENT_VERSION"
#define SOFTWARE_KEY        "SOFTWARE_ID"
#define LANG_KEY            "LANGAUAGE"
#define INSTALLPATH_KEY     "INSTALL_PATH"
#define DATAPATH_KEY        "DATA_PATH"
#define LIBLOCATION_KEY     "LIB_LOCATION"
#define RSDK_LOCATION_KEY   "RSDK_LOCATION"
#define FIPSMODE_KEY        "FIPS_MODE"

#define VERSION_VALUE   "5.0.0"
#define SOFTWARE_VALUE  "EPOAGENT3000"
#define LANG_VALUE      "0x409"
#ifdef MA_WINDOWS
    #define INSTALLPATH_VALUE   "C:\Program Files (x86)\McAfee"
    #define DATAPATH_VALUE      "C:\ProgramData\McAfee"
    #define LIBLOCATION_VALUE   "C:\Program Files (x86)\McAfee\Common Framework"
    #define RSDK_LOCATION_VALUE "C:\Program Files (x86)\McAfee\Common Framework"
#else
    #define INSTALLPATH_VALUE   "/opt/McAfee"
    #define DATAPATH_VALUE      "/opt/McAfee"
    #define LIBLOCATION_VALUE   "/opt/McAfee/lib"
    #define RSDK_LOCATION_VALUE "/opt/McAfee/lib"
#endif
#define FIPSMODE_VALUE   "0"

static ma_error_t add_configuration(ma_ds_t *ds_handle) {
    ma_db_t *db_handle = (ma_db_t *)ds_handle ;
    ma_db_statement_t *db_stmt = NULL ;

    ma_db_statement_create(db_handle, "INSERT INTO AGENT_PARENT VALUES (10 , 'AGENT\\CONFIGURATION')", &db_stmt) ;
    ma_db_statement_execute(db_stmt) ;
    ma_db_statement_release(db_stmt) ; db_stmt = NULL ;

    ma_ds_set_str(ds_handle, "AGENT\\CONFIGURATION", VERSION_KEY, VERSION_VALUE, strlen(VERSION_VALUE)) ;
    ma_ds_set_str(ds_handle, "AGENT\\CONFIGURATION", SOFTWARE_KEY, SOFTWARE_VALUE, strlen(SOFTWARE_VALUE)) ;
    ma_ds_set_str(ds_handle, "AGENT\\CONFIGURATION", SOFTWARE_KEY, SOFTWARE_VALUE, strlen(SOFTWARE_VALUE)) ;
    ma_ds_set_str(ds_handle, "AGENT\\CONFIGURATION", LANG_KEY, LANG_VALUE, strlen(LANG_VALUE)) ;
    ma_ds_set_str(ds_handle, "AGENT\\CONFIGURATION", INSTALLPATH_KEY, INSTALLPATH_VALUE, strlen(INSTALLPATH_VALUE)) ;
    ma_ds_set_str(ds_handle, "AGENT\\CONFIGURATION", DATAPATH_KEY, DATAPATH_VALUE, strlen(DATAPATH_VALUE)) ;
    ma_ds_set_str(ds_handle, "AGENT\\CONFIGURATION", LIBLOCATION_KEY, LIBLOCATION_VALUE, strlen(LIBLOCATION_VALUE)) ;
    ma_ds_set_str(ds_handle, "AGENT\\CONFIGURATION", RSDK_LOCATION_KEY, RSDK_LOCATION_VALUE, strlen(RSDK_LOCATION_VALUE)) ;
    ma_ds_set_int(ds_handle, "AGENT\\CONFIGURATION", FIPSMODE_KEY, (ma_int64_t)(MA_MSC_SELECT(_strtoi64,strtoll)(FIPSMODE_VALUE, NULL, 0))) ;

    return MA_OK ;
}


static ma_error_t add_services(ma_ds_t *ds_handle) {
    ma_db_t *db_handle = (ma_db_t *)ds_handle ;
    ma_db_statement_t *db_stmt = NULL ;

   ma_db_statement_create(db_handle, "INSERT INTO AGENT_PARENT VALUES (20 , 'AGENT\SERVICES')", &db_stmt) ;
    ma_db_statement_execute(db_stmt) ;
    ma_db_statement_release(db_stmt) ; db_stmt = NULL ;

    //add services path
    ma_db_statement_create(db_handle, "INSERT INTO AGENT_PARENT VALUES (200 , 'AGENT\SERVICES\AH')", &db_stmt) ;
    ma_db_statement_execute(db_stmt) ;
    ma_db_statement_release(db_stmt) ; db_stmt = NULL ;

    ma_db_statement_create(db_handle, "INSERT INTO AGENT_PARENT VALUES (210 , 'AGENT\SERVICES\DATACHANNEL')", &db_stmt) ;
    ma_db_statement_execute(db_stmt) ;
    ma_db_statement_release(db_stmt) ; db_stmt = NULL ;


    ma_db_statement_create(db_handle, "INSERT INTO AGENT_PARENT VALUES (220 , 'AGENT\SERVICES\EVENT')", &db_stmt) ;
    ma_db_statement_execute(db_stmt) ;
    ma_db_statement_release(db_stmt) ; db_stmt = NULL ;

    ma_db_statement_create(db_handle, "INSERT INTO AGENT_PARENT VALUES (230 , 'AGENT\SERVICES\GENERAL')", &db_stmt) ;
    ma_db_statement_execute(db_stmt) ;
    ma_db_statement_release(db_stmt) ; db_stmt = NULL ;

    ma_db_statement_create(db_handle, "INSERT INTO AGENT_PARENT VALUES (240 , 'AGENT\SERVICES\IO')", &db_stmt) ;
    ma_db_statement_execute(db_stmt) ;
    ma_db_statement_release(db_stmt) ; db_stmt = NULL ;

    ma_db_statement_create(db_handle, "INSERT INTO AGENT_PARENT VALUES (250 , 'AGENT\SERVICES\LOGGER')", &db_stmt) ;
    ma_db_statement_execute(db_stmt) ;
    ma_db_statement_release(db_stmt) ; db_stmt = NULL ;

    ma_db_statement_create(db_handle, "INSERT INTO AGENT_PARENT VALUES (260 , 'AGENT\SERVICES\POLICY')", &db_stmt) ;
    ma_db_statement_execute(db_stmt) ;
    ma_db_statement_release(db_stmt) ; db_stmt = NULL ;

    ma_db_statement_create(db_handle, "INSERT INTO AGENT_PARENT VALUES (270 , 'AGENT\SERVICES\PROPERTY')", &db_stmt) ;
    ma_db_statement_execute(db_stmt) ;
    ma_db_statement_release(db_stmt) ; db_stmt = NULL ;

    ma_db_statement_create(db_handle, "INSERT INTO AGENT_PARENT VALUES (280 , 'AGENT\SERVICES\SCHEDULER')", &db_stmt) ;
    ma_db_statement_execute(db_stmt) ;
    ma_db_statement_release(db_stmt) ; db_stmt = NULL ;

    ma_db_statement_create(db_handle, "INSERT INTO AGENT_PARENT VALUES (290 , 'AGENT\SERVICES\UPDATER' )", &db_stmt) ;
    ma_db_statement_execute(db_stmt) ;
    ma_db_statement_release(db_stmt) ; db_stmt = NULL ;

    return MA_OK ;
}

static ma_error_t add_default_sub_path_for_services(ma_ds_t *ds_handle) {
    ma_db_t *db_handle = (ma_db_t *)ds_handle ;
    ma_db_statement_t *db_stmt = NULL ;
    ma_ds_iterator_t *path_iter = NULL ;
    ma_buffer_t *buffer = NULL ;
    
    ma_ds_iterator_create(ds_handle, 1, "AGENT\SERVICES", &path_iter) ;
    while(MA_ERROR_DS_NO_MORE_ITEMS != ma_ds_iterator_get_next(path_iter, &buffer)) {

    }

    return MA_OK ;
}

ma_error_t create_agent_default_db()
{
    ma_ds_t *ds_handle = NULL ;
    ma_db_t *db_handle = NULL ;
    ma_db_statement_t *db_stmt = NULL ;

    unlink(MA_DB_FILENAME) ;

    ma_db_open(MA_DB_FILENAME, &db_handle) ;
    ds_handle = (ma_ds_t *)db_handle ;

    add_tables(db_handle) ;
    add_parent_root(db_handle) ;
    add_configuration(ds_handle) ;
    add_services(ds_handle) ;
    add_default_sub_path_for_services(ds_handle) ;

 
    return MA_OK ;
}
