#include "ma_db_generator.h"

extern ma_console_logger_t *logger ;

#define CREATE_MPO_TABLE_STMT "CREATE TABLE IF NOT EXISTS MA_POLICY_OBJECT(PO_ID TEXT NOT NULL UNIQUE,PO_NAME TEXT NOT NULL,PO_FEATURE TEXT NOT NULL, PO_CATEGORY	TEXT NOT NULL, PO_TYPE TEXT	NOT NULL, PO_DIGEST	TEXT NOT NULL, PRIMARY KEY (PO_ID) ON CONFLICT REPLACE)"
#define CREATE_MPSO_TABLE_STMT "CREATE TABLE IF NOT EXISTS MA_POLICY_SETTINGS_OBJECT (PO_ID TEXT NOT NULL ,PSO_ID TEXT NOT NULL, PSO_NAME	TEXT	NOT NULL ,PSO_DIGEST	TEXT	NOT NULL ,PSO_DATA	BLOB ,PSO_PARAM_INT	INTEGER ,PSO_PARAM_STR	TEXT ,PSO_TIMESTAMP	TEXT NOT NULL,PRIMARY KEY (PSO_ID) ON CONFLICT REPLACE, FOREIGN KEY (PO_ID) REFERENCES MA_POLICY_OBJECT(PO_ID) ON UPDATE CASCADE ON DELETE CASCADE )"
#define CREATE_MPPA_TABLE_STMT "CREATE TABLE IF NOT EXISTS MA_POLICY_PRODUCT_ASSIGNMENT (PO_ID TEXT NOT NULL ,PPA_PRODUCT TEXT NOT NULL, PPA_METHOD	TEXT,PPA_PARAM	TEXT, UNIQUE(PO_ID, PPA_PRODUCT, PPA_METHOD, PPA_PARAM) ON CONFLICT IGNORE, FOREIGN KEY (PO_ID) REFERENCES MA_POLICY_OBJECT(PO_ID) ON UPDATE CASCADE ON DELETE CASCADE )"

ma_error_t agent_policy_db_instance_create(ma_db_t *db_handle)
{
    if(db_handle){
        ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;

        if(MA_OK == (rc = ma_db_transaction_begin(db_handle))) 
        {
            if( MA_OK == (rc = ma_db_statement_create(db_handle, CREATE_MPO_TABLE_STMT, &db_stmt)))
            {
                rc = ma_db_statement_execute(db_stmt) ;
                ma_db_statement_release(db_stmt); db_stmt = NULL ;
                if( MA_OK == rc && MA_OK == (rc = ma_db_statement_create(db_handle, CREATE_MPSO_TABLE_STMT, &db_stmt)) )
                {
                    rc = ma_db_statement_execute(db_stmt) ;
                    ma_db_statement_release(db_stmt); db_stmt = NULL ;
                    if( MA_OK == rc && MA_OK == (rc = ma_db_statement_create(db_handle, CREATE_MPPA_TABLE_STMT, &db_stmt)))
                    {
                        if(MA_OK  == (rc = ma_db_statement_execute(db_stmt)))
                            rc = ma_db_transaction_end(db_handle) ;
                        ma_db_statement_release(db_stmt); db_stmt = NULL ;
                    }
                }
            }
            if(MA_OK != rc)
               ma_db_transaction_cancel(db_handle) ;
        }
         return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}


ma_error_t create_agent_policy_db() {
    ma_db_t *db_handle = NULL ;
    //ma_db_statement_t *db_stmt = NULL ;

    unlink(MA_POLICY_DB_FILENAME) ;
    ma_db_open(MA_POLICY_DB_FILENAME, &db_handle) ;

    agent_policy_db_instance_create(db_handle) ;

    return MA_OK ;
}