#include "ma_db_generator.h"

extern ma_console_logger_t *logger ;

#define CREATE_SCHEDULER_PARENT_TABLE_SQL_STATEMENT "CREATE TABLE IF NOT EXISTS SCHEDULER_PARENT (ID INTEGER NOT NULL UNIQUE, PATH TEXT NOT NULL UNIQUE, PRIMARY KEY (ID) ON CONFLICT REPLACE )"
#define CREATE_SCHEDULER_CHILD_TABLE_SQL_STATEMENT  "CREATE TABLE IF NOT EXISTS SCHEDULER_CHILD (ID INTEGER NOT NULL, NAME TEXT NOT NULL, VALUE BLOB NOT NULL, PRIMARY KEY (ID, NAME) ON CONFLICT REPLACE , FOREIGN KEY (ID) REFERENCES AGENT_PARENT(ID)  ON UPDATE CASCADE ON DELETE CASCADE )"

ma_error_t create_agent_scheduler_db()
{
    ma_db_t *db_handle = NULL ;
    ma_db_statement_t *db_stmt = NULL ;

    unlink(MA_DB_FILENAME) ;

    ma_db_open(MA_DB_FILENAME, &db_handle) ;
    
    ma_db_statement_create(db_handle, CREATE_SCHEDULER_PARENT_TABLE_SQL_STATEMENT, &db_stmt) ;
    ma_db_statement_execute(db_stmt) ;
    ma_db_statement_release(db_stmt) ; db_stmt = NULL ;

    ma_db_statement_create(db_handle, CREATE_SCHEDULER_CHILD_TABLE_SQL_STATEMENT, &db_stmt) ;
    ma_db_statement_execute(db_stmt) ;
    ma_db_statement_release(db_stmt) ; db_stmt = NULL ;

    //add data

    return MA_OK ;
}

