//#include "unity.h"
//#include "unity_fixture.h"
//#include "ma/logger/ma_console_logger.h"
//
//#include "ma/internal/clients/udp/ma_udp_request.h"
//#include "ma/internal/services/udp_server/ma_udp_server.h"
//
//#include "ma/internal/utils/event_loop/ma_event_loop.h"
//#include "ma/internal/utils/event_loop/ma_loop_worker.h"
//
//#undef LOG_FACILITY_NAME
//#define LOG_FACILITY_NAME "udp_test"
//
//
//#define PORT			8089
//#define ADDRESS			"ff08::9"
//
//static ma_loop_worker_t		*ma_loop_worker = NULL;
//static ma_event_loop_t		*ma_ev_loop = NULL;
//static ma_logger_t			*logger = NULL;
//static ma_udp_msg_handler_t *handler = NULL;
//static ma_udp_server_t		*server = NULL;
//
//static ma_udp_client_t		*udp_client = NULL;
//static ma_udp_request_t		*udp_req = NULL;
//
//static int what = 0;
//static int send_come = 0;
//static int response_come = 0;
//static int final_come = 0;
//
//extern ma_error_t ma_udp_handler_create(ma_udp_msg_handler_t **self, ma_logger_t *logger);
//
//TEST_GROUP_RUNNER(ma_udp_server_test_group)
//{
//    do {RUN_TEST_CASE(ma_udp_server_test_ut, udp_server_test);} while(0);
//}
//
//ma_error_t udp_request_send_cb(ma_udp_request_t *request, void *user_data, ma_bool_t *stop){
//	MA_LOG(logger, MA_LOG_SEV_DEBUG, "udp_request_send_cb");
//	send_come = 1;
//}
//
//ma_error_t udp_response_cb(ma_udp_request_t *request, ma_udp_response_t *response, void *user_data, ma_bool_t *stop){
//	MA_LOG(logger, MA_LOG_SEV_DEBUG, "udp_response_cb");
//	response_come = 1;
//}
//
//void udp_request_finalize_cb(ma_error_t rc, ma_udp_request_t *request, void *user_data){
//	MA_LOG(logger, MA_LOG_SEV_DEBUG, "udp_request_finalize_cb");
//	final_come = 1;
//}
//
//TEST_SETUP(ma_udp_server_test_ut)
//{   
//	static ma_udp_server_policy_t policy;
//	policy.is_enabled = MA_TRUE;
//	policy.port = PORT;
//	policy.multicast_addr = ADDRESS;
//
//    ma_event_loop_create(&ma_ev_loop);
//	ma_loop_worker_create(ma_ev_loop, &ma_loop_worker);
//	ma_loop_worker_start(ma_loop_worker);
//	ma_console_logger_create((ma_console_logger_t**)&logger);
//
//	/*udp server*/
//	ma_udp_server_create(&server);
//	ma_udp_server_set_logger(server, logger);
//	ma_udp_server_set_event_loop(server, ma_ev_loop);
//
//	/*handler*/
//	ma_udp_handler_create(&handler, logger);
//	ma_udp_server_register_msg_handler(server, handler);
//	ma_udp_server_set_policy(server, &policy);
//
//	/*client*/
//	ma_udp_client_create(&udp_client);
//	ma_udp_client_set_logger(udp_client, logger);
//	ma_udp_client_set_loop_worker(udp_client, ma_loop_worker);
//	ma_udp_client_set_event_loop(udp_client, ma_ev_loop);
//
//	/*udp request*/
//	ma_udp_request_create(udp_client, &udp_req);
//	ma_udp_request_set_port(udp_req, PORT);
//	ma_udp_request_set_multicast_addr(udp_req, ADDRESS);
//	ma_udp_request_set_timeout(udp_req, 5000);
//	ma_udp_request_set_callbacks(udp_req, udp_request_send_cb, udp_response_cb, NULL);
//}
//
//TEST_TEAR_DOWN(ma_udp_server_test_ut)
//{
//	ma_logger_release(logger);	
//	ma_loop_worker_release(ma_loop_worker);
//	ma_udp_msg_handler_release(handler);
//	ma_udp_server_release(server);
//	ma_udp_request_release(udp_req);
//	ma_event_loop_release(ma_ev_loop);	
//}
//
//static uv_buf_t alloc_cb(uv_handle_t* handle, size_t suggested_size) {
//	if(handle) {
//		return uv_buf_init((char*)calloc(suggested_size, 1), suggested_size);
//	}
//	return uv_buf_init(NULL, 0);
//}
//
//static void recv_cb(uv_udp_t* handle, ssize_t nread, uv_buf_t buf, struct sockaddr* addr, unsigned flags) {	
//}
//
//static void udp_handle_close_cb(uv_handle_t* handle){
//	MA_LOG(logger, MA_LOG_SEV_DEBUG, "**************");
//}
//
//void timer_exit(uv_timer_t *handle, int status)
//{
//	static uv_udp_t test;
//	if(what == 0){
//		ma_udp_msg_t *request[1];
//		ma_message_t *message = NULL;
//
//		uv_loop_t *loop = NULL;
//		
//		struct sockaddr_in sa = uv_ip4_addr("0.0.0.0", 12345);
//
//		loop = ma_event_loop_get_uv_loop(ma_ev_loop);
//		//start the exit timer
//		uv_udp_init(loop, &test);    
//		//uv_udp_bind(&test, sa, 0);
//		//uv_udp_recv_start(&test, alloc_cb, recv_cb);
//
//		uv_timer_stop(handle); 
//		what++;
//		/*send*/
//
//		ma_udp_msg_create(&request[0]);
//		ma_message_create(&message);
//		ma_message_set_property(message, "type", "relay");
//		ma_udp_msg_set_ma_message(request[0], message);
//		ma_udp_request_set_message(udp_req, request, 1);
//		ma_udp_msg_release(request[0]);
//		ma_message_release(message);
//		
//		TEST_ASSERT(MA_OK == ma_udp_server_start(server));
//		TEST_ASSERT(MA_OK == ma_udp_request_async_send(udp_req, udp_request_finalize_cb));
//		uv_timer_start(handle, timer_exit, 50000, 0);
//	}
//	else if(what == 1){
//		uv_timer_stop(handle); 
//		what++;
//		//Sleep(20000);
//		/*check response*/
//		TEST_ASSERT(send_come == 1);
//		TEST_ASSERT(response_come == 1);
//		TEST_ASSERT(final_come == 1);
//
//		/*relase server and client*/
//		ma_udp_server_stop(server);
//		ma_udp_server_unregister_msg_handler(server, handler);			
//		ma_loop_worker_stop(ma_loop_worker);
//		uv_close((uv_handle_t*) &test, udp_handle_close_cb);
//	}
//}
//
//
//
//
//TEST(ma_udp_server_test_ut, udp_server_test)
//{
//	uv_timer_t timer;	
//    uv_loop_t *loop = NULL;
//	
//	loop = ma_event_loop_get_uv_loop(ma_ev_loop);
//    //start the exit timer
//    uv_timer_init(loop, &timer);
//    uv_timer_start(&timer, timer_exit, 2000, 0);
//	ma_event_loop_run(ma_ev_loop);    	
//}