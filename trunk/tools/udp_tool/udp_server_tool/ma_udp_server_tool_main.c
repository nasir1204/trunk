#include "ma/logger/ma_console_logger.h"
#include <assert.h>

#include "ma/internal/services/udp_server/ma_udp_server.h"

#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/utils/event_loop/ma_loop_worker.h"

#include "ma/internal/services/p2p/ma_p2p_discovery_hanlder.h"
//#include "src/services/p2p/ma_p2p_discovery_handler.c"
#include "ma/internal/services/p2p/ma_p2p_db_manager.h"
#include "p2p_header.h"


#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "udp_server_test_tool"


#define PORT			8089
#define ADDRESS			"ff08::9"

static ma_loop_worker_t		*ma_loop_worker = NULL;
static ma_event_loop_t		*ma_ev_loop = NULL;
static ma_logger_t			*logger = NULL;
static ma_udp_msg_handler_t *handler = NULL;
static ma_p2p_discovery_handler_t *p2p_handler = NULL;
static ma_udp_server_t		*server = NULL;


static int what = 0;
static int send_come = 0;
static int response_come = 0;
static int final_come = 0;

extern ma_error_t ma_udp_handler_create(ma_udp_msg_handler_t **self, ma_logger_t *logger);
extern ma_error_t ma_p2p_discovery_handler_create(ma_udp_msg_handler_t **self);
extern ma_error_t ma_p2p_discovery_handler_release(ma_udp_msg_handler_t **self);
extern ma_error_t ma_p2p_db_is_content_exists(ma_db_t *db_handle, const char *content_hash, ma_bool_t *is_exists) ;
static uv_buf_t alloc_cb(uv_handle_t* handle, size_t suggested_size) {
	if(handle) {
		return uv_buf_init((char*)calloc(suggested_size, 1), suggested_size);
	}
	return uv_buf_init(NULL, 0);
}

static void recv_cb(uv_udp_t* handle, ssize_t nread, uv_buf_t buf, struct sockaddr* addr, unsigned flags) {	
}

static void udp_handle_close_cb(uv_handle_t* handle){
	MA_LOG(logger, MA_LOG_SEV_DEBUG, "**************");
}

void timer_exit(uv_timer_t *handle, int status)
{
	static uv_udp_t test;
	if(what == 0){
		ma_udp_msg_t *request[1];
		ma_message_t *message = NULL;

		uv_loop_t *loop = NULL;
		
		struct sockaddr_in sa = uv_ip4_addr("0.0.0.0", 12345);

		loop = ma_event_loop_get_uv_loop(ma_ev_loop);
		//start the exit timer
		uv_udp_init(loop, &test);    
		
		uv_timer_stop(handle); 
		what++;
		
		assert(MA_OK == ma_udp_server_start(server));
		uv_timer_start(handle, timer_exit, 50000, 0);
	}
	else if(what == 1){
		uv_timer_stop(handle); 
		what++;
		
		/*relase server and client*/
		ma_udp_server_stop(server);
		//ma_udp_server_unregister_msg_handler(server, handler);			
		ma_loop_worker_stop(ma_loop_worker);
		uv_close((uv_handle_t*) &test, udp_handle_close_cb);
	}
}

int main() {

	static ma_udp_server_policy_t policy;
	
	uv_timer_t timer;	
    uv_loop_t *loop = NULL;
	
	policy.is_enabled = MA_TRUE;
	policy.port = PORT;
	policy.multicast_addr = ADDRESS;

    ma_event_loop_create(&ma_ev_loop);
	ma_loop_worker_create(ma_ev_loop, &ma_loop_worker);
	ma_loop_worker_start(ma_loop_worker);
	ma_console_logger_create((ma_console_logger_t**)&logger);

	/*udp server*/
	ma_udp_server_create(&server);
	ma_udp_server_set_logger(server, logger);
	ma_udp_server_set_event_loop(server, ma_ev_loop);

	/*handler*/
	ma_udp_handler_create(&handler, logger);
	ma_udp_server_register_msg_handler(server, handler);
	ma_udp_server_set_policy(server, &policy);

	//Create a p2p handler
	local_p2p_discovery_handler_create(NULL,(ma_udp_msg_handler_t**)&p2p_handler);
	ma_udp_server_register_msg_handler(server, (ma_udp_msg_handler_t*)p2p_handler);
	ma_udp_server_set_policy(server, &policy);
	
//Between tearup and teardown
	
	
	loop = ma_event_loop_get_uv_loop(ma_ev_loop);
    //start the exit timer
    uv_timer_init(loop, &timer);
    uv_timer_start(&timer, timer_exit, 2000, 0);
	ma_event_loop_run(ma_ev_loop);    	

	//Teardown
	
	ma_logger_release(logger);	
	ma_loop_worker_release(ma_loop_worker);
	ma_udp_msg_handler_release(handler);
	local_p2p_discovery_handler_release(p2p_handler);
	ma_udp_server_release(server);
	ma_event_loop_release(ma_ev_loop);	
}


