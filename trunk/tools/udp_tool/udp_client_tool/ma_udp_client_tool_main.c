#include "ma/logger/ma_console_logger.h"
#include <assert.h>

#include "ma/internal/clients/udp/ma_udp_request.h"
#include "ma/internal/defs/ma_p2p_defs.h"

//#include "ma/p2p/ma_p2p_client.h"
#include "local_p2p_client.h"

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "udp_client_test_tool"


#define PORT			8089
#define ADDRESS			"ff08::9"


static ma_loop_worker_t		*ma_loop_worker = NULL;
static ma_event_loop_t		*ma_ev_loop = NULL;
static ma_logger_t			*logger = NULL;

static ma_udp_client_t		*udp_client = NULL;
static ma_p2p_client_t      *p2p_client = NULL;
static ma_udp_request_t		*udp_req = NULL, *udp_req1=NULL;

static int what = 0;
static int what_p2p = 0;
static int send_come = 0;
static int response_come = 0;
static int final_come = 0;

static uv_udp_t test, test1;

ma_error_t udp_request_send_cb(ma_udp_request_t *request, void *user_data, ma_bool_t *stop){
	MA_LOG(logger, MA_LOG_SEV_DEBUG, "udp_request_send_cb");
	send_come = 1;
}

ma_error_t udp_response_cb(ma_udp_request_t *request, ma_udp_response_t *response, void *user_data, ma_bool_t *stop){
	MA_LOG(logger, MA_LOG_SEV_DEBUG, "udp_response_cb");
	MA_LOG(logger,MA_LOG_SEV_DEBUG, "Response: %s",(char*) user_data);
	//Need to print the response message payload if it is present
	if(response)
	{
		ma_message_t *mamsg;
		ma_variant_t *payload;
		// Buffer to hold string extracted from variant
	    ma_buffer_t *buffer = NULL;
		char *response_string = NULL;
		
		size_t size;

		ma_udp_msg_t *udpmsg = response->response;
		ma_udp_msg_get_ma_message(udpmsg, &mamsg);
		if(mamsg) {
			char *response_http_port = NULL;
			//Check for P2P property being set
			ma_message_get_property(mamsg,"prop.key.content_url.port",&response_http_port);
			if(response_http_port)
			MA_LOG(logger,MA_LOG_SEV_DEBUG, "P2P Http Port: %s",response_http_port);

			ma_message_get_payload(mamsg, &payload);
			ma_variant_get_string_buffer(payload, &buffer);
			ma_buffer_get_string(buffer, &response_string, &size);
			
			MA_LOG(logger,MA_LOG_SEV_DEBUG, "Response Payload: %s",response_string);

		}
	}
	response_come = 1;
	 
}

void udp_request_finalize_cb(ma_error_t rc, ma_udp_request_t *request, void *user_data){
	MA_LOG(logger, MA_LOG_SEV_DEBUG, "udp_request_finalize_cb");
	final_come = 1;
}

static uv_buf_t alloc_cb(uv_handle_t* handle, size_t suggested_size) {
	if(handle) {
		return uv_buf_init((char*)calloc(suggested_size, 1), suggested_size);
	}
	return uv_buf_init(NULL, 0);
}

static void recv_cb(uv_udp_t* handle, ssize_t nread, uv_buf_t buf, struct sockaddr* addr, unsigned flags) {	
}

static void udp_handle_close_cb(uv_handle_t* handle){
	MA_LOG(logger, MA_LOG_SEV_DEBUG, "**************");
}

void timer_exit(uv_timer_t *handle, int status)
{
	//static uv_udp_t test;
	if(what == 0){
		ma_udp_msg_t *request[1],*request1[1];
		ma_message_t *message = NULL,*message1=NULL;

		uv_loop_t *loop = NULL;
		ma_variant_t *vart=NULL,*vart1=NULL;
		ma_error_t err=MA_OK;
		
		struct sockaddr_in sa = uv_ip4_addr("0.0.0.0", 12345);

		loop = ma_event_loop_get_uv_loop(ma_ev_loop);
		//start the exit timer
		uv_udp_init(loop, &test);    
		what++;
		/*send*/
		printf("\nhere1");

		ma_udp_msg_create(&request[0]);
		ma_message_create(&message);
		ma_message_set_property(message, "type", "relay");
		//Set payload to the message
		assert( MA_OK == ( err = ma_variant_create_from_string("hello2hi",strlen("hello2hi"),&vart))  );
		ma_message_set_payload(message, vart);
		ma_udp_msg_set_ma_message(request[0], message);
		ma_udp_request_set_message(udp_req, request, 1);
		ma_udp_msg_release(request[0]);
		ma_message_release(message);
		printf("\nhere2");
		assert(MA_OK == ma_udp_request_async_send(udp_req, udp_request_finalize_cb));
		printf("\nhere3");
		uv_timer_start(handle, timer_exit, 50000, 0);
	}
	else if(what == 1){
		uv_timer_stop(handle); 
		what++;
		
		//release server and client
		ma_loop_worker_stop(ma_loop_worker);
		uv_close((uv_handle_t*) &test, udp_handle_close_cb);
	}
}

void timer_exit_p2p(uv_timer_t *handle, int status)
{
	
	if(what_p2p== 0){
		ma_udp_msg_t *request[1],*request1[1];
		ma_message_t *message = NULL,*message1=NULL;

		uv_loop_t *loop = NULL;
		ma_variant_t *vart=NULL,*vart1=NULL;
		ma_error_t err=MA_OK;
		char *url_port=NULL;

		struct sockaddr_in sa = uv_ip4_addr("0.0.0.0", 12345);

		loop = ma_event_loop_get_uv_loop(ma_ev_loop);
		
		uv_udp_init(loop, &test1);    
		what_p2p++;
		/*send*/
			
		//Send a p2p request message
		ma_udp_msg_create(&request1[0]);
		ma_message_create(&message1);
		ma_message_set_property(message1,"prop.key.msg_type","prop.val.rqst.p2p.content_discovery");
		ma_message_set_property(message1,MA_P2P_MSG_KEY_CONTENT_HASH_STR,"123");
		
		ma_udp_msg_set_ma_message(request1[0],message1);
		ma_udp_request_set_message(udp_req1,request1,1);
		//ma_udp_msg_release(request1[0]);
		//ma_message_release(message1);

		printf("\nhere2");
		assert(MA_OK == ma_udp_request_async_send(udp_req1, udp_request_finalize_cb));

		uv_timer_start(handle, timer_exit_p2p, 50000, 0);
	}
	
	else if(what_p2p == 1){
		uv_timer_stop(handle); 
		what_p2p++;
		
		//relase server and client
		ma_loop_worker_stop(ma_loop_worker);
		uv_close((uv_handle_t*) &test1, udp_handle_close_cb);
		
	}
}

int main() {

	uv_timer_t timer,timer1;	
    uv_loop_t *loop = NULL;
	
	ma_event_loop_create(&ma_ev_loop);
	ma_loop_worker_create(ma_ev_loop, &ma_loop_worker);
	ma_loop_worker_start(ma_loop_worker);
	ma_console_logger_create((ma_console_logger_t**)&logger);

	//client
	ma_udp_client_create(&udp_client);
	ma_udp_client_set_logger(udp_client, logger);
	ma_udp_client_set_loop_worker(udp_client, ma_loop_worker);
	ma_udp_client_set_event_loop(udp_client, ma_ev_loop);

	//udp request
	ma_udp_request_create(udp_client, &udp_req);
	ma_udp_request_set_port(udp_req, PORT);
	ma_udp_request_set_multicast_addr(udp_req, ADDRESS);
	ma_udp_request_set_timeout(udp_req, 5000);
	ma_udp_request_set_callbacks(udp_req, udp_request_send_cb, udp_response_cb, "hello1");
	

	//udp request1
	ma_udp_request_create(udp_client, &udp_req1);
	ma_udp_request_set_port(udp_req1, PORT);
	ma_udp_request_set_multicast_addr(udp_req1, ADDRESS);
	ma_udp_request_set_timeout(udp_req1, 5000);
	ma_udp_request_set_callbacks(udp_req1, NULL,&udp_response_cb, "hello2");

	
	//Between tearup and teardown
	loop = ma_event_loop_get_uv_loop(ma_ev_loop);
    //start the exit timer
    uv_timer_init(loop, &timer);
    uv_timer_start(&timer, timer_exit, 2000, 0);
	//ma_event_loop_run(ma_ev_loop);  
	uv_timer_init(loop, &timer1);
	uv_timer_start(&timer1, timer_exit_p2p, 3000, 0);
	ma_event_loop_run(ma_ev_loop);    	

	
	//Teardown
	//ma_loop_worker_stop(ma_loop_worker);
	//ma_event_loop_stop(ma_ev_loop);
	ma_logger_release(logger);	
	ma_loop_worker_release(ma_loop_worker);
	ma_udp_request_release(udp_req);
	ma_udp_request_release(udp_req1);
	ma_udp_client_release(udp_client);
	ma_event_loop_release(ma_ev_loop);	

	return 0;

}



