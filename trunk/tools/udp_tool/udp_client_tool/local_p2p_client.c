#include "ma/p2p/ma_p2p_client.h"
#include "ma/internal/clients/p2p/ma_p2p_content_request_internal.h"
#include "ma/internal/defs/ma_p2p_defs.h"
#include "ma/internal/services/p2p/ma_p2p_db_manager.h"

#include "ma/internal/clients/udp/ma_udp_request.h"
#include "ma/internal/defs/ma_udp_defs.h"
#include "ma/internal/defs/ma_http_server_defs.h"

#include "ma/ma_log.h"
#include "ma/ma_message.h"
#include "ma/internal/services/ma_agent_policy_settings_bag.h" 
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/defs/ma_object_defs.h"

#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include "ma/logger/ma_logger.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define LOG_FACILITY_NAME "p2p_client"

struct ma_p2p_client_s {    
	ma_msgbus_t   *msgbus ;
	ma_logger_t   *logger ;
	ma_msgbus_callback_thread_options_t thread_option ;
} ;

ma_error_t local_p2p_client_create(ma_p2p_client_t **p2p_client) {
    if(p2p_client) {
        ma_error_t rc = MA_ERROR_OUTOFMEMORY ;
        ma_p2p_client_t *tmp = (ma_p2p_client_t *)calloc(1, sizeof(ma_p2p_client_t)) ;
		
        if(tmp){
			*p2p_client = tmp ;
			return MA_OK ;
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t local_p2p_client_set_logger(ma_p2p_client_t *self, ma_logger_t *logger) {
	if(self && logger){
		ma_logger_add_ref(self->logger = logger) ;
		return MA_OK ;
	}
	return MA_ERROR_INVALIDARG ;
}


ma_error_t ma_p2p_client_set_msgbus(ma_p2p_client_t *self, ma_msgbus_t *msgbus) {
	if(self && msgbus){
		self->msgbus = msgbus ;
		return MA_OK ;
	}
	return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_p2p_client_set_thread_option(ma_p2p_client_t *self, ma_msgbus_callback_thread_options_t thread_option) {
	if(self){
		self->thread_option = thread_option ;
		return MA_OK ;
	}
	return MA_ERROR_INVALIDARG ;
}

/*
static ma_error_t ma_p2p_client_add_remove_content(ma_p2p_client_t *self, ma_p2p_content_request_t *request, ma_bool_t is_add) {
    ma_error_t rc = MA_OK ;
    ma_message_t *msg = NULL;
    ma_variant_t *payload = NULL ;
		
	if(!self->logger || !self->msgbus)
		return MA_ERROR_PRECONDITION ;

    ma_p2p_content_request_add_ref(request) ;
        
    MA_LOG(self->logger, MA_LOG_SEV_INFO, "Sending content \"%s\" request", is_add ? "add" : "remove") ;
        
    if(MA_OK == (rc = ma_p2p_content_request_as_variant(request, &payload))) {
		ma_msgbus_endpoint_t *p2p_ep = NULL;

		if(MA_OK == (rc = ma_msgbus_endpoint_create(self->msgbus, MA_P2P_SERVICE_NAME_STR, "localhost", &p2p_ep))) 
		{
			ma_msgbus_endpoint_setopt(p2p_ep, MSGBUSOPT_THREADMODEL, self->thread_option) ;
			ma_msgbus_endpoint_setopt(p2p_ep, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC) ;
				
			if(MA_OK == (rc = ma_message_create(&msg))) {
				(void)ma_message_set_property(msg, MA_P2P_MSG_KEY_TYPE_STR, is_add ? MA_P2P_MSG_PROP_VAL_RQST_ADD_CONTENT_STR : MA_P2P_MSG_PROP_VAL_RQST_REMOVE_CONTENT_STR) ;
				(void)ma_message_set_payload(msg, payload) ;

				if(self->thread_option == MA_MSGBUS_CALLBACK_THREAD_IO) {
					rc = ma_msgbus_send_and_forget(p2p_ep, msg) ;
				}
				else {
					ma_message_t *response = NULL ;
					rc = ma_msgbus_send(p2p_ep, msg, &response) ;
					if(response)	(void)ma_message_release(response) ;
				}
				ma_message_release(msg) ; msg = NULL ;
			}
			else {
				MA_LOG(self->logger, MA_LOG_SEV_INFO, "Content \"%s\" request message creation failed(%d).", (is_add ? "add" : "remove"), rc ) ;
			}
			ma_msgbus_endpoint_release(p2p_ep);
		}
		else {
			MA_LOG(self->logger, MA_LOG_SEV_INFO, "Msgbus endpoint creation failed error (%d).", rc ) ;
		}
		ma_variant_release(payload) ;  payload = NULL ;
    }
    else {
        MA_LOG(self->logger, MA_LOG_SEV_INFO, "Content \"%s\" request payload create failed(%d).",(is_add ? "add" : "remove"), rc ) ;
    }

	if(MA_OK == rc ){
		MA_LOG(self->logger, MA_LOG_SEV_INFO, "Content \"%s\" request succeded.", (is_add ? "add" : "remove") );
	}
	else {
		MA_LOG(self->logger, MA_LOG_SEV_INFO, "Content \"%s\" request failed(%d).", (is_add ? "add" : "remove"), rc ) ;
	}

    ma_p2p_content_request_release(request) ;
    return rc ;
}

ma_error_t ma_p2p_client_add_content(ma_p2p_client_t *self, ma_p2p_content_request_t *request) {
    if(self && request) {        
        return ma_p2p_client_add_remove_content(self, request, MA_TRUE);        
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_p2p_client_remove_content(ma_p2p_client_t *self, ma_p2p_content_request_t *request) {
    if(self && request) {
        return ma_p2p_client_add_remove_content(self, request, MA_FALSE);        
    }
    return MA_ERROR_INVALIDARG ;
}
*/
ma_error_t ma_p2p_client_get_content_filepath(ma_p2p_client_t *self, const char *hash, char **file_path) {
	if(self && hash && file_path) {
		ma_error_t rc = MA_OK ;

		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

typedef struct p2p_discovery_callback_data_s {
	char *hash ;
	char *url ;
}p2p_discovery_callback_data_t ;

static ma_error_t p2p_discovery_callback_data_create(p2p_discovery_callback_data_t **cb_data) ;
static void p2p_discovery_callback_data_release(p2p_discovery_callback_data_t *cb_data) ;

static ma_error_t p2p_discovery_cb(ma_udp_request_t *request, ma_udp_response_t *response, void *user_data, ma_bool_t *stop) ;

ma_error_t local_p2p_client_discover_content(ma_p2p_client_t *self, const char *multicast_addr, ma_uint32_t discovery_port, const char *hash, char **url){
	if(self && multicast_addr && discovery_port && hash && url) {
		ma_error_t rc = MA_OK ;
        ma_message_t *msg = NULL ;
		ma_udp_client_t  *udp_client = NULL;
		ma_udp_request_t *udp_request = NULL;
		ma_udp_msg_t *udp_msg = NULL ;

		MA_LOG(self->logger, MA_LOG_SEV_INFO, "Checking content via discovery...") ;

		if(MA_OK == (rc = ma_udp_client_create(&udp_client))) 
		{			
			(void)ma_udp_client_set_logger(udp_client, self->logger) ;
			(void)ma_udp_client_set_event_loop(udp_client, ma_msgbus_get_event_loop(self->msgbus)) ;
			(void)ma_udp_client_set_loop_worker(udp_client, ma_msgbus_get_loop_worker(self->msgbus)) ;

			if(MA_OK == (rc = ma_udp_request_create(udp_client, &udp_request))) 
			{
				p2p_discovery_callback_data_t *cb_data = NULL ;

				(void)p2p_discovery_callback_data_create(&cb_data) ;
				(void)ma_udp_request_set_multicast_addr(udp_request, multicast_addr) ;
				(void)ma_udp_request_set_port(udp_request, discovery_port) ;
				cb_data->hash = strdup(hash) ;
				(void)ma_udp_request_set_callbacks(udp_request, NULL, &p2p_discovery_cb, (void *)cb_data) ;
				(void)ma_udp_request_set_timeout(udp_request, 4*1000) ;

				if(MA_OK == (rc = ma_udp_msg_create(&udp_msg))) 
				{
					if(MA_OK == (rc = ma_message_create(&msg))) {
						(void)ma_message_set_property(msg, MA_P2P_MSG_KEY_TYPE_STR, MA_P2P_MSG_PROP_VAL_RQST_CONTENT_DISCOVERY_STR) ;
						(void)ma_message_set_property(msg, MA_P2P_MSG_KEY_CONTENT_HASH_STR, hash) ;
						
						(void)ma_udp_msg_set_ma_message(udp_msg, msg) ;						
						(void)ma_udp_request_set_message(udp_request, &udp_msg, 1) ;
						
						if(MA_OK == (rc = ma_udp_request_send(udp_request))) {
							if(cb_data->url) {
								*url = strdup(cb_data->url) ;
							}
							else {
								rc = MA_ERROR_P2P_CONTENT_NOT_DISCOVERED ;
							}
						}
						(void)ma_message_release(msg);
					}
					(void)ma_udp_msg_release(udp_msg);
				}
				(void)p2p_discovery_callback_data_release(cb_data) ;
				(void)ma_udp_request_release(udp_request);
			}
			(void)ma_udp_client_release(udp_client);
		}

		if(MA_OK == rc) {
			MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "p2p content url(%s) for hash(%s).", *url, hash) ;
		}
		else {
			MA_LOG(self->logger, MA_LOG_SEV_INFO, "Couldn't discover content for hash(%s), (%d).", hash, rc) ;
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_p2p_client_release(ma_p2p_client_t *self) {
    if(self) {
		if(self->logger) ma_logger_release(self->logger);
        free(self) ;
    }
    return MA_OK ;
}

ma_error_t p2p_discovery_cb(ma_udp_request_t *request, ma_udp_response_t *response, void *user_data, ma_bool_t *stop) {
	if(request && response && user_data) {
		p2p_discovery_callback_data_t *cb_data = (p2p_discovery_callback_data_t *)user_data ;
		char *msg_type = NULL, *reply_hash = NULL ;
		ma_message_t *msg = NULL ;
		
		*stop = MA_FALSE ;
		
		ma_udp_msg_get_ma_message(response->response, &msg) ;
		ma_message_get_property(msg, MA_P2P_MSG_KEY_TYPE_STR, &msg_type) ;

		if(msg_type && 0 == strcmp(msg_type, MA_P2P_MSG_PROP_VAL_RPLY_CONTENT_DISCOVERY_STR)) {
			ma_message_get_property(msg, MA_P2P_MSG_KEY_CONTENT_HASH_STR, &reply_hash) ;
			if(reply_hash && 0 == strcmp(reply_hash, cb_data->hash)) {
				char *str_port = NULL, *p = NULL ;
				ma_message_get_property(msg, MA_P2P_MSG_KEY_CONTENT_URL_PORT_STR, &str_port) ;
				if(str_port) {
					ma_temp_buffer_t temp_url = MA_TEMP_BUFFER_INIT ;

					
					if(!strncmp(response->peer_addr, "fe80:", strlen("fe80:")) || !strncmp(response->peer_addr, "FE80:", strlen("FE80:"))){
						ma_temp_buffer_reserve(&temp_url, strlen(MA_P2P_CONTENT_URL_FORMAT_WITH_SCOPE_ID_STR) + strlen(response->peer_addr) + strlen("%") + sizeof(ma_uint32_t)+ strlen(str_port) + sizeof(ma_uint32_t) + strlen(reply_hash) + 1) ;
						MA_MSC_SELECT(_snprintf, snprintf)(p = (char *)ma_temp_buffer_get(&temp_url), ma_temp_buffer_capacity(&temp_url) ,MA_P2P_CONTENT_URL_FORMAT_WITH_SCOPE_ID_STR, response->peer_addr, "%", response->receiving_local_nic, atoi(str_port), reply_hash) ;
					}
					else {
						ma_temp_buffer_reserve(&temp_url, strlen(MA_P2P_CONTENT_URL_FORMAT_STR) + strlen(response->peer_addr) + sizeof(ma_uint32_t)+ strlen(str_port) + sizeof(ma_uint32_t) + strlen(reply_hash) + 1) ;
						MA_MSC_SELECT(_snprintf, snprintf)(p = (char *)ma_temp_buffer_get(&temp_url), ma_temp_buffer_capacity(&temp_url) ,MA_P2P_CONTENT_URL_FORMAT_STR, response->peer_addr, atoi(str_port), reply_hash) ;
					}
					cb_data->url = strdup(p = (char *)ma_temp_buffer_get(&temp_url)) ;
					*stop = MA_TRUE ;
					ma_temp_buffer_uninit(&temp_url) ;
				}
			}
		}
		return MA_OK ;
	}
	return MA_ERROR_INVALIDARG ;
}

static ma_error_t p2p_discovery_callback_data_create(p2p_discovery_callback_data_t **cb_data) {
	if(cb_data) {
		*cb_data = (p2p_discovery_callback_data_t *)calloc(1, sizeof(p2p_discovery_callback_data_t)) ;
		return MA_OK ;
	}
	return MA_ERROR_INVALIDARG ;
}

static void p2p_discovery_callback_data_release(p2p_discovery_callback_data_t *cb_data) {
	if(cb_data) {
		if(cb_data->hash)	free(cb_data->hash) ;
		cb_data->hash = NULL ;

		if(cb_data->url)	free(cb_data->url) ;
		cb_data->url = NULL ;

		free(cb_data) ;
	}
	return ;
}
