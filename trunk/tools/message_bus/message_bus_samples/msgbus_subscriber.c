#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "ma/ma_message.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_dispatcher.h"

#define PRODUCT_ID "MESSAGEBUS_SUBSCRIBER"

static int endpoint_number = 999;
static const char *topic = "My_Pet_Topic";

/******************************************************************************
Object pointers for various endpoint types
******************************************************************************/
ma_msgbus_t *msgbus_obj = NULL; // Message Bus
ma_msgbus_subscriber_t *p_subscriber = NULL; // Message Bus Subscriber
ma_dispatcher_t *my_handle = NULL;

/******************************************************************************
Subscriber callback function. This callback function gets executed when the
subscriber recieves a message corresponding to the topic it had subscribed for.
******************************************************************************/
ma_error_t subscriber_callback_func(const char *topic, ma_message_t *subscriber_payload, void *cb_data)
{
    // Variant to hold the variant extracted from message
    ma_variant_t *variant_ptr = NULL;

    // To hold the property retrieved from variant
    const char *publisher_number = NULL;

	// Buffer to hold string extracted from variant
    ma_buffer_t *buffer = NULL;

    // String Message retrieved from the buffer and its size
    char *my_string = NULL;
    size_t size;

    ma_message_get_property(subscriber_payload, "PUBLISHER_NUMBER", &publisher_number);
    ma_message_get_payload(subscriber_payload, &variant_ptr);
    ma_variant_get_string_buffer(variant_ptr, &buffer);
    ma_buffer_get_string(buffer, &my_string, &size);
    ma_buffer_release(buffer);

    printf("Subscriber No. - %d Received Msg from Publisher No. - %d \n", endpoint_number, atoi(publisher_number));
    printf("Topic - %s \n", topic);
    printf("Message Payload - %s \n", my_string);

	// Cleanup code
    ma_variant_release(variant_ptr);

	return MA_OK;
}

/******************************************************************************
Start of Execution.
******************************************************************************/
int main(int argc, char* argv[])
{
	ma_dispatcher_initialize(NULL, &my_handle);    
	
	/**************************************************************************
    Create and start message bus
    **************************************************************************/
    ma_msgbus_create(PRODUCT_ID, &msgbus_obj);
	ma_msgbus_start(msgbus_obj);

    /**************************************************************************
    If endpoint is a SUBSCRIBER,
    1) Create a subscriber.
    2) Set reachability, thread model and timeout options.
    3) Register for 'Topic' for listening.
    **************************************************************************/
    ma_msgbus_subscriber_create(msgbus_obj, &p_subscriber);
    ma_msgbus_subscriber_setopt(p_subscriber, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
    ma_msgbus_subscriber_setopt(p_subscriber, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_POOL);
    ma_msgbus_subscriber_setopt(p_subscriber, MSGBUSOPT_TIMEOUT, 100000);
    ma_msgbus_subscriber_register(p_subscriber, topic, subscriber_callback_func, (void *)&endpoint_number);

    // Waiting for messages corresponding to registered topic
    while(1) {};

    /**************************************************************************
    Unregister and release the subscriber.
    **************************************************************************/
    ma_msgbus_subscriber_unregister(p_subscriber);
    ma_msgbus_subscriber_release(p_subscriber);

    /**************************************************************************
    Release message-bus
    **************************************************************************/
    ma_msgbus_stop(msgbus_obj, 1);
	ma_msgbus_release(msgbus_obj);

	ma_dispatcher_deinitialize(my_handle);
}




