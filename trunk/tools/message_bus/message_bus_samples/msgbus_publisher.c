#include <stdio.h>
#include <string.h>
#include "ma/ma_message.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_dispatcher.h"
#include <Windows.h>

#define PRODUCT_ID "MESSAGEBUS_PUBLISHER"

static int endpoint_number = 110;
static long int message_size_in_bytes = 100;
static const char *topic = "My_Pet_Topic";


/******************************************************************************
Object pointers for various endpoint types
******************************************************************************/
static ma_msgbus_t *msgbus_obj = NULL; // Message Bus
static ma_dispatcher_t *my_handle = NULL;


/******************************************************************************
Utility function for generating random numbers.

Returns a random value between 0 and passed input value (excluding the input
value) on every function call.
******************************************************************************/
int generate_random_number(int max_value_exclusive)
{
    static int count = 0;

    if(count == 0)
    {
        srand((unsigned int)time(NULL));
        count++;
    }
    return(rand() % max_value_exclusive);
}

/******************************************************************************
Utility function for generating random messages. Fills the passed string of given
length with any one character from the below character set.

NOTE : The user can replace this function with its own utility function.
******************************************************************************/
const char charset[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*()+_:<>?";

void generate_message(char *str, unsigned int length)
{
    unsigned int counter = 0;

    char mychar = 'a';

    mychar = charset[generate_random_number((int)strlen(charset))];

    for(counter = 0; counter < length; counter++)
    {
        str[counter] = mychar;
    }
}

/******************************************************************************
Publisher function which publishes a single message on the message bus.
******************************************************************************/
void publish()
{
    // Pointer pointing to the 'message_size_in_bytes' message.
    char *my_string = NULL;

    // Variant Pointer for holding the variant created from message string.
    ma_variant_t *variant_ptr = NULL;

    // Message to be published
    ma_message_t* publisher_payload = NULL;

    // Allocate memory for storing the message payload (1 extra byte for NULL).
    // Fill payload with random data.
    // Create variant of string.
    my_string = (char *)calloc(message_size_in_bytes + 1, 1);
    generate_message(my_string, message_size_in_bytes);
    ma_variant_create_from_string(my_string, strlen(my_string), &variant_ptr);

    // Creating fresh message.
    // Set Publisher Number and Message Number in the message property.
    // Set string variant as message payload and then release variant
    // Publish the Message on message Bus
    ma_message_create(&publisher_payload);

    {
        char value[10];
        sprintf(value, "%d", endpoint_number);
        ma_message_set_property(publisher_payload, "PUBLISHER_NUMBER", value);
    }

    ma_message_set_payload(publisher_payload, variant_ptr);
    ma_variant_release(variant_ptr);
	ma_msgbus_publish(msgbus_obj, topic, MSGBUS_CONSUMER_REACH_OUTPROC, publisher_payload);

    printf("Publisher No. - %d \n", endpoint_number);
    printf("Topic - %s \n", topic);
    printf("Message Payload - %s \n\n", my_string);

    // Cleanup code
    ma_message_release(publisher_payload);
    free(my_string);
}


int main(int argc, char* argv[])
{
	int counter = 0;

	ma_dispatcher_initialize(NULL, &my_handle);

	/**************************************************************************
    Create and start message bus
    **************************************************************************/
    ma_msgbus_create(PRODUCT_ID, &msgbus_obj);
	ma_msgbus_start(msgbus_obj);

	// Start publishing periodically
	for(counter = 0; counter < 50; counter++)
	{
		publish();
		Sleep(2000);
	}

    /**************************************************************************
    Release message-bus
    **************************************************************************/
    ma_msgbus_stop(msgbus_obj, 1);
	ma_msgbus_release(msgbus_obj);

	ma_dispatcher_deinitialize(my_handle);
}



