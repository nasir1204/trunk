#include "ma/ma_message.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_dispatcher.h"

#include <stdio.h>
#include <string.h>
#include <time.h>     // Seed for srand
#include <stdlib.h>   // For malloc and random function

#define PRODUCT_ID "MESSAGEBUS_SERVER"

static int endpoint_number = 101;
static const char *server_name = "MY_SERVER";

static ma_dispatcher_t *my_handle = NULL;

/******************************************************************************
Object pointers for various endpoint types
******************************************************************************/
static ma_msgbus_t *msgbus_obj = NULL; // Message Bus
static ma_msgbus_server_t *p_server = NULL; // Message Bus Server

/******************************************************************************
Utility function for generating response to the received message. Converts the
upper case letters in given string to lower case letters and vice-versa. Numbers
and alphanumeric characters remain unchanged.

NOTE : This function simulates as if the server is doing some processing before
sending the response message. The user can replace this function with its own
utility function.
******************************************************************************/
void generate_response(char *str)
{
    int count = 0;

    while(str[count] != '\0')
    {
        int int_value = (int)str[count];

        if((int_value >= 65) && (int_value <= 90)) str[count] = (char)(int_value + 32);

        if((int_value >= 97) && (int_value <= 122)) str[count] = (char)(int_value - 32);

        count++;
    }
}

/******************************************************************************
Call back function to be executed when the server receives a request from the
client. This server will handle both synchronous as well as asynchronous requests.
******************************************************************************/
ma_error_t respond_to_client_requests(ma_msgbus_server_t *server, ma_message_t *request_payload, void *cb_data, ma_msgbus_client_request_t *c_request)
{
    // Will hold the response message created by this server for incoming request
    ma_message_t* response_payload = NULL;

    // Variant to hold the variant extracted from message
    ma_variant_t *variant_ptr = NULL;

    // Buffer to hold string extracted from variant
    ma_buffer_t *buffer = NULL;

    // String Message retrieved from the buffer and its size
    char *my_string = NULL;
    size_t size;

    // To hold the property retrieved from variant
    const char *client_number = NULL;

    // Processing the incoming message to the server
    ma_message_get_property(request_payload, "CLIENT_NUMBER", &client_number);
    ma_message_get_payload(request_payload, &variant_ptr);
    ma_variant_get_string_buffer(variant_ptr, &buffer);
    ma_buffer_get_string(buffer, &my_string, &size);
    ma_buffer_release(buffer);

    printf("Server No. - %d recieved Request from Client No. - %d \n", endpoint_number, atoi(client_number));
    printf("Request Payload - %s\n\n", my_string);

    // Sending the response to client for incoming message

    // Changing the payload in the request payload to make the response string
    generate_response(my_string);
    ma_variant_create_from_string(my_string, strlen(my_string), &variant_ptr);
    ma_message_set_payload(request_payload, variant_ptr);
    ma_msgbus_server_client_request_post_result(c_request, MA_OK, request_payload);
    ma_variant_release(variant_ptr);

    return MA_OK;
}


/******************************************************************************
Start of Execution.
******************************************************************************/
int main(int argc, char* argv[])
{
	ma_dispatcher_initialize(NULL, &my_handle);

    /**************************************************************************
    Create and start message bus
    **************************************************************************/
    ma_msgbus_create(PRODUCT_ID, &msgbus_obj);
    ma_msgbus_start(msgbus_obj);

    /**************************************************************************
    Creating a message bus server
    1) Create a server
    2) Set reachability, thread model and timeout options.
    3) Start server for listening to client requests (Register callback function).
    **************************************************************************/
    ma_msgbus_server_create(msgbus_obj, "MY_SERVER", &p_server);
    ma_msgbus_server_setopt(p_server, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
    ma_msgbus_server_setopt(p_server, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_POOL);
    ma_msgbus_server_setopt(p_server, MSGBUSOPT_TIMEOUT, 100000);
    ma_msgbus_server_start(p_server, respond_to_client_requests, (void *)&endpoint_number);

    // Waiting for requests
    while(1) {};

    ma_msgbus_server_stop(p_server);
    ma_msgbus_server_release(p_server);

    /**************************************************************************
    Release message-bus
    **************************************************************************/
    ma_msgbus_stop(msgbus_obj, 1);
    ma_msgbus_release(msgbus_obj);

	ma_dispatcher_deinitialize(my_handle);
}



