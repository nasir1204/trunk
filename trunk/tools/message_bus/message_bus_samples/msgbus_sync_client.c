#include "ma/ma_message.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_dispatcher.h"

#include <stdio.h>
#include <string.h>
#include <time.h>     // Seed for srand
#include <stdlib.h>   // For malloc and random function
#include <Windows.h>  // For Sleep


#define PRODUCT_ID "MESSAGEBUS_SYNC_CLIENT"

static int endpoint_number = 201;
static long int message_size_in_bytes = 100;
static const char *server_name = "MY_SERVER";

static ma_dispatcher_t *my_handle = NULL;

/******************************************************************************
Object pointers for various endpoint types
******************************************************************************/
static ma_msgbus_t *msgbus_obj = NULL; // Message Bus
static ma_msgbus_endpoint_t *p_endpoint = NULL; // Message Bus Client

/******************************************************************************
Utility function for generating random numbers.

Returns a random value between 0 and passed input value (excluding the input
value) on every function call.
******************************************************************************/
int generate_random_number(int max_value_exclusive)
{
    static int count = 0;

    if(count == 0)
    {
        srand((unsigned int)time(NULL));
        count++;
    }
    return(rand() % max_value_exclusive);
}

/******************************************************************************
Utility function for generating random messages. Fills the passed string of given
length with any one character from the below character set.

NOTE : The user can replace this function with its own utility function.
******************************************************************************/
const char charset[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*()+_:<>?";

void generate_message(char *str, unsigned int length)
{
    unsigned int counter = 0;

    char mychar = 'a';

    mychar = charset[generate_random_number((int)strlen(charset))];

    for(counter = 0; counter < length; counter++)
    {
        str[counter] = mychar;
    }
}

/******************************************************************************
Client call back function. This function is called by send_client_request()
function in this sample code to process the response packet received by synchronous
client.
******************************************************************************/
ma_error_t process_server_response(ma_error_t status, ma_message_t *response_payload, void *cb_data, ma_msgbus_request_t *request)
{
    // Variant to hold the variant extracted from message
    ma_variant_t *variant_ptr = NULL;

    // To hold the property retrieved from variant
    const char *client_number = NULL;

    // Buffer to hold string extracted from variant
    ma_buffer_t *buffer = NULL;

    // String Message retrieved from the buffer and its size
    char *my_string = NULL;
    size_t size;

    ma_message_get_property(response_payload, "CLIENT_NUMBER", &client_number);
    ma_message_get_payload(response_payload, &variant_ptr);
    ma_variant_get_string_buffer(variant_ptr, &buffer);
    ma_buffer_get_string(buffer, &my_string, &size);
    ma_buffer_release(buffer);

    printf("Client No. - %d Recieved Response. Response Payload -%s \n\n", endpoint_number, my_string);

    // Cleanup code
    ma_variant_release(variant_ptr);

    return MA_OK;
}


/******************************************************************************
Client function for sending the Request to server. This function sends a single
request message (synchronous) of given size on the message bus.
******************************************************************************/
void send_client_request()
{
    // Pointer pointing to the 'message_size_in_bytes' message.
    char *my_string = NULL;

    // Variant Pointer for holding the variant created from message string.
    ma_variant_t *variant_ptr = NULL;

    // Request Message to be sent to server
    ma_message_t* request_payload = NULL;

    // Response message received from server for a synchronous request
    ma_message_t* response_payload = NULL;

    // Request Packet
    ma_msgbus_request_t *request_packet = NULL;

    // Allocate memory for storing the message payload (1 extra byte for NULL).
    // Fill payload with random data.
    // Create variant of string.
    my_string = (char *)calloc(message_size_in_bytes + 1, 1);
    generate_message(my_string, message_size_in_bytes);
    ma_variant_create_from_string(my_string, strlen(my_string), &variant_ptr);

    // Creating fresh message.
    // Set Client Number in the message property. Set string variant as
    // message payload and then release variant.
    // Send the synchronous request.
    ma_message_create(&request_payload);

    {
        char buffer[10];
        sprintf(buffer, "%d", endpoint_number);
        ma_message_set_property(request_payload, "CLIENT_NUMBER", buffer);
    }

    ma_message_set_payload(request_payload, variant_ptr);
    ma_variant_release(variant_ptr);

    // Sending synchronous request to the server
    ma_msgbus_send(p_endpoint, request_payload, &response_payload);

    printf("Client No. - %d sent Request. Request Payload - %s \n", endpoint_number, my_string);

    // Process the response receieved from server
    process_server_response(MA_OK, response_payload, NULL, request_packet);

    // Cleanup code
    ma_message_release(request_payload);
    free(my_string);
}

/******************************************************************************
Start of Execution.
******************************************************************************/
int main(int argc, char* argv[])
{
    int counter = 0;

	ma_dispatcher_initialize(NULL, &my_handle);

    /**************************************************************************
    Create and start message bus
    **************************************************************************/
    ma_msgbus_create(PRODUCT_ID, &msgbus_obj);
    ma_msgbus_start(msgbus_obj);

    /**************************************************************************
    If endpoint is a CLIENT,
    1) Create a client.
    2) Set reachability and thread model options. Set timeout equal to the total
       duration of test run, so that no message gets discarded due to timeout.
    **************************************************************************/
    ma_msgbus_endpoint_create(msgbus_obj, "MY_SERVER", NULL, &p_endpoint);
    ma_msgbus_endpoint_setopt(p_endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
    ma_msgbus_endpoint_setopt(p_endpoint, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_POOL);
    ma_msgbus_endpoint_setopt(p_endpoint, MSGBUSOPT_TIMEOUT, 100000);

    for(counter = 0; counter < 50; counter++)
    {
        send_client_request();
        Sleep(2000);
    }

    ma_msgbus_endpoint_release(p_endpoint);


    /**************************************************************************
    Release message-bus
    **************************************************************************/
    ma_msgbus_stop(msgbus_obj, 1);
    ma_msgbus_release(msgbus_obj);

	ma_dispatcher_deinitialize(my_handle);
}



