###############################################################################
# Main Test Runner for Message Bus Performance Benchmark tests.
# 
# Use following command for help information - 
# python msgbus_performance_test_monitor.py -h/--help 
#
# Note - This code depends on following modules - argparse, psutil, os, time, datetime
#
###############################################################################

# Importing required python modules
import argparse   # For parsing command line arguments
import os         # For process spawning
import psutil     # For process info collection
import time       # for sleeping
from datetime import datetime  # For getting the current time

##############################################
# Code for parsing the command line arguments
##############################################
# Parsing the command-line arguments
parser = argparse.ArgumentParser(prog='Message Bus Performance Test', 
                                 description='Main Test Runner for Message Bus performance Test Suite.', 
                                 argument_default=argparse.SUPPRESS)
parser.add_argument('-e', '--executable', action='store', required=True, type=str, help='Executable File Name')
parser.add_argument('-f', '--test_file', action='store', required=True, type=str, help='Test Case Configuration File')
parser.add_argument('-t', '--time_interval', action='store', required=False, default=60, type=int, help='Interval (in seconds) b/w data collection')

# Obtaining the argument dictionary from the command-line
# and retrieve arguments from dictionary
arg_dict = vars(parser.parse_args())
executable_file_name = arg_dict['executable']
test_case_file_name = arg_dict['test_file']
time_interval = arg_dict['time_interval']

##############################################
# Code for parsing the input test case file
# containing the list of end-points and their
# configurations.
##############################################
print '!!!! Parsing test case configuration file !!!!'

test_case_file_handle = open(test_case_file_name, 'r')

list_of_endpoints = []
while True : 
	line = test_case_file_handle.readline()
	if not line : break
	list_of_endpoints.append(line)
test_case_file_handle.close()

##############################################
# 1) For each item in the endpoint list, spawn a
#    new process. 
# 2) Wait for few seconds to write its PIDs in
#     the "pid_list.txt".
##############################################
print '!!!! Creating End-points !!!!'

# Sleep for few seconds, allowing processes to
# write their PIDs
for x in list_of_endpoints:
	my_list = executable_file_name + ' ' + x
        os.spawnv(os.P_NOWAIT, executable_file_name, my_list.split())
        time.sleep(1)

print '!!!! End-points created !!!!'

##############################################
# Parse the pid_list.txt and extract PIDs into
# a list.
##############################################
pid_list = []

pid_file_handle = open('pid_list.txt', 'r')

# 'pid_list.txt' contains the data in '<Endpoint-name> \t <PID>'
# format. Therefore, PID is the second element after splitting.
while True : 
	line = pid_file_handle.readline()
	if not line : break      
        elements = line.split()
        pid_list.append(int(elements[1]))
pid_file_handle.close()

# No need of PID file now. Removing it.
os.unlink('pid_list.txt')

##############################################
# Monitor periodically until at least one of the
# process in the 'pid_list.txt' is running.
#
# It is expected that the benchmarking tests 
# will be executed for hours and the time 
# interval between two consecutive info collection
# will be in minutes, therefore, this script
# opens the file handle, writes and closes the
# handle, instead of keeping the file handle
# open throughout the test run.
##############################################
print 'Start monitoring PIDs - ', pid_list

output_file_name = 'performance_test_statistics.txt'
while len(pid_list) != 0:
	output_file_handle = open(output_file_name, 'a', 2)
	current_time_info = 'Current Time - ' + str(datetime.now()) + '\n'
	cpu_info = ('CPU Time - ' + str(psutil.cpu_times()) +
	'\tPhysical Memory (Available - ' + str(psutil.avail_phymem()) + ' Bytes\tUsed - ' + str(psutil.used_phymem()) + ' Bytes)' +
	'\tVirtual Memory (Total - ' + str(psutil.total_virtmem()) + ' Bytes\tAvailable - ' + str(psutil.avail_virtmem()) + ' Bytes\tUsed - ' + str(psutil.used_virtmem()) + ' Bytes)' +
        '\n')
        
        output_file_handle.write(current_time_info)
        output_file_handle.write(cpu_info)
        
	for x in pid_list:	
		if psutil.pid_exists(x):
			my_process = psutil.Process(x)	
		
		        # If process is running, monitor it,
			# else, remove the process from the
			# list of processes being monitored
	
			process_info = ('PID - ' + str(x) +
			'\tStatus - ' + str(my_process.status) +
			'\tThreads - ' + str(my_process.get_num_threads()) +
			'\tCPU Times - ' + str(my_process.get_cpu_times()) +
			'\tIO Counters - ' + str(my_process.get_io_counters()) +
			'\tMemory Info - ' + str(my_process.get_memory_info()) +
			'\tProcess Mem. Utilization - ' + str(my_process.get_memory_percent()) + '%' +
			'\tOpened Files - ' + str(my_process.get_open_files()) +
			'\tOpened Connections - ' + str(my_process.get_connections()) +
			'\n')
			
			output_file_handle.write(process_info)
		else:
			pid_list.remove(x)			      
        output_file_handle.write('\n\n')
        output_file_handle.close()
        time.sleep(time_interval)

print "Hurray!!!! Performance Test Complete !!!!"



