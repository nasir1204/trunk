/******************************************************************************
1) A generic implementation to test the message bus functionality. Please refer
   to documentation throughout this file to understand the functionality.

   For execution commands, see the 'help_string' below or execute following
   command -
   msgbus_performance_test -h

2) This implementation uses ma_msgbus_start() API which runs message bus in a
   different thread (non-blocking)

3) Use Debug mode, to enable extra logs from this tool as well as message bus.
******************************************************************************/
#include <stdio.h>
#include <string.h>
#include "ma/ma_message.h"
#include "ma/ma_msgbus.h"
#include "ma/logger/ma_file_logger.h"
#include "uv.h"
#include <time.h>

#ifdef WIN32
    #include <Windows.h>
#else
    #include <unistd.h>
#endif

#ifdef _MSC_VER
    # include <crtdbg.h> /* */
    # define MA_TRACKLEAKS _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF)
#else
    # define MA_TRACKLEAKS ((void *)0)
#endif

// Facility Name.
// To be used for Logging and as Product ID.
#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "MESSAGEBUS_PERFORMANCE_TEST"

// Externally link the following functions from the ma_getopt.c.
// These functions are needed for parsing the command-line arguments.
extern int ma_getopt(int argc, char *argv[], char *optstring);
extern const char *ma_getoptarg();

/******************************************************************************
Defines the arguments received by the main function in this file. These parameters
decide the type of message bus being created.
******************************************************************************/
enum { SUBSCRIBER = 0, PUBLISHER = 1, SYNC_CLIENT = 2, ASYNC_CLIENT = 3, SEND_FORGET_CLIENT = 4, SERVER = 5 } endpoint_type;
static const char *endpoint_name = NULL;
static ma_msgbus_consumer_reachability_t reach = MSGBUS_CONSUMER_REACH_OUTPROC;
static ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;
static long int total_run_duration = 300;      // Default Value
static long int message_frequency_in_ms = 100; // Default Value
static long int message_size_in_bytes = 100;   // Default value
static const char *server_to_communicate = NULL;
static const char *host_name = NULL;
static const char *topic = NULL;

/******************************************************************************
File Logger pointer. File Logger is being used for logging additional information
for debugging purposes. This flag is enabled automatically when the execution
is in debug mode.
******************************************************************************/
#ifdef DEBUG
    static ma_file_logger_t *file_logger = NULL;
#endif

/******************************************************************************
Object pointers for various endpoint types
******************************************************************************/
static ma_msgbus_t *msgbus_obj = NULL;              // Message Bus
static ma_msgbus_server_t *p_server = NULL;         // Message Bus Server
static ma_msgbus_endpoint_t *p_endpoint = NULL;     // Message Bus Client
static ma_msgbus_subscriber_t *p_subscriber = NULL; // Message Bus Subscriber

/******************************************************************************
1) Define file handle and file name for logging statistics in file. Statistics
   will be displayed on console and logged into the file.
2) Modify the file name here, if you wish to use a different file name.
******************************************************************************/
static FILE * performance_test_report_file_handle = NULL;
static char * performance_test_report_file_name = "performance_test_report.txt";

/******************************************************************************
total_msg_published                - Total no. of messages published by the
                                     publisher for given topic.
total_msg_sub_rcvd                 - Total messages received by subscriber who
                                     is listening to a particular topic
total_client_req_sent              - Total number of requests (sync/async/send
                                     and forget) sent by client.
total_req_handled_server           - Total number of requests received and processed
                                     by server. Note that if the same server is
                                     handling requests from multiple clients, this
                                     number may be considerably high.
total_resp_recvd_client            - Number of responses received by client for
                                     the requests sent. Note - No responses are
                                     expected for 'send and forget' type client.
******************************************************************************/
static unsigned long long int total_msg_published = 0;
static unsigned long long int total_msg_sub_rcvd = 0;
static unsigned long long int total_client_req_sent = 0;
static unsigned long long int total_req_handled_server = 0;
static unsigned long long int total_resp_recvd_client = 0;

/******************************************************************************
Libuv Variables for managing the timing for which the endpoint will be executing.
******************************************************************************/
static uv_loop_t *wait_loop = NULL;
static uv_timer_t periodic_timer;
static uv_timer_t exit_timer;

/******************************************************************************
Utility function for generating random numbers.

Returns a random value between 0 and passed input value (excluding the input
value) on every function call.
******************************************************************************/
int generate_random_number(int max_value_exclusive)
{
    static int count = 0;

    if(count == 0)
    {
        srand((unsigned int)time(NULL));
        count++;
    }
    return(rand() % max_value_exclusive);
}

/******************************************************************************
Utility function for generating random messages. Fills the passed string of given
length with any one character from the below character set.

NOTE : The user can replace this function with its own utility function.
******************************************************************************/
const char charset[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*()+_:<>?";

void generate_message(char *str, unsigned int length)
{
    unsigned int counter = 0;

    char mychar = 'a';

    mychar = charset[generate_random_number((int)strlen(charset))];

    for(counter = 0; counter < length; counter++)
    {
        str[counter] = mychar;
    }
}

/******************************************************************************
Utility function to print help and usage information.
******************************************************************************/
void print_help()
{
    printf("\n\t !!! Welcome to Message Bus Performance Test !!!\n\n");
    printf("Usage Instructions : \n\n");
    printf("Command - msgbus_performance_test <Options>....\n\n");

    printf("-e   Endpoint Type [PUBLISHER/SUBSCRIBER/ASYNC_CLIENT/SYNC_CLIENT/SEND_FORGET_CLIENT/SERVER] Default - SUBSCRIBER \n");
    printf("-n   Endpoint Name                                                                           Mandatory \n");
    printf("-r   Reachability  [INRPOC/OUTPROC/OUTBOX]                                                   Default - OUTPROC \n");
    printf("-t   Thread option [THREAD_POOL/THREAD_IO]                                                   Default - Thread Pool \n");
    printf("-F   Total Time for which end-point will be running (in seconds)                             Default - 5 Minutes (300 Seconds) \n");
    printf("-f   Message Frequency (in milli-seconds)                                                    Default - 100 ms \n");
    printf("-m   Message Size (in Bytes)                                                                 Default - 100 Bytes \n");
    printf("-S   Server Name                                                                             Mandatory for Clients \n");
    printf("-T   Topic to be published/subscribed                                                        Mandatory for Subscribers/Publishers \n");
    /*printf("-H   Host Name, can be NULL for INPROC and OUTPROC communication. This argument              Mandatory for Server. Can be ignored for INPROC and OUTPROC \n");
    printf("     is not parsed currently. \n");*/
    printf("-h   Prints Help                                                                             Optional \n");
    return;
}

/******************************************************************************
Utility function for generating response to the received message. Converts the
upper case letters in given string to lower case letters and vice-versa. Numbers
and alphanumeric characters remain unchanged.

NOTE : This function simulates as if the server is doing some processing before
sending the response message. The user can replace this function with its own
utility function.
******************************************************************************/
void generate_response(char *str)
{
    int count = 0;

    while(str[count] != '\0')
    {
        int int_value = (int)str[count];

        if((int_value >= 65) && (int_value <= 90)) str[count] = (char)(int_value + 32);

        if((int_value >= 97) && (int_value <= 122)) str[count] = (char)(int_value - 32);

        count++;
    }
}

/******************************************************************************
Publishing the Process details of current process into a file, so that the data
can be consumed by the monitoring code.
******************************************************************************/
void dump_pid_info()
{
    FILE * pid_info_file_handle = NULL;
    char * pid_info_file_name = "pid_list.txt";

    #ifdef WIN32
       DWORD pid_number = GetCurrentProcessId();
    #else
       pid_t pid_number = getpid(void);
    #endif

    pid_info_file_handle = fopen(pid_info_file_name, "a+");
    fprintf(pid_info_file_handle, "%s \t %d\n", endpoint_name, pid_number);
    fclose(pid_info_file_handle);
}

/******************************************************************************
Publisher function which publishes a single message on the message bus.
******************************************************************************/
void publish()
{
    // Variable to hold current time
    time_t current_time;

    // Pointer pointing to the 'message_size_in_bytes' message.
    char *my_string = NULL;

    // Variant Pointer for holding the variant created from message string.
    ma_variant_t *variant_ptr = NULL;

    // Message to be published
    ma_message_t* publisher_payload = NULL;

    // Allocate memory for storing the message payload (1 extra byte for NULL).
    // Fill payload with random data.
    // Create variant of string.
    my_string = (char *)calloc(message_size_in_bytes + 1, 1);
    generate_message(my_string, message_size_in_bytes);
    ma_variant_create_from_string(my_string, strlen(my_string), &variant_ptr);

    // Increment the count of the message being published
    total_msg_published++;

    // Creating fresh message.
    // Set Publisher Name and Message Number in the message property.
    // Set string variant as message payload and then release variant
    // Publish the Message on message Bus
    ma_message_create(&publisher_payload);

    {
        char value[10];
        ma_message_set_property(publisher_payload, "PUBLISHER_NAME", endpoint_name);

        sprintf(value, "%d", total_msg_published);
        ma_message_set_property(publisher_payload, "MESSAGE_NUMBER", value);
    }

    ma_message_set_payload(publisher_payload, variant_ptr);
    ma_variant_release(variant_ptr);
    ma_msgbus_publish(msgbus_obj, topic, reach, publisher_payload);

    // Getting the time at which message is published
    time(&current_time);

    printf("Publisher Name - %s \t Msg No. - %d \n", endpoint_name, total_msg_published);

    #ifdef DEBUG
        MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_INFO, "Publisher - %s published Message ( %d ) Topic - %s, Message Payload - %s, Message Received at - %s", endpoint_name, total_msg_published, topic, my_string, ctime(&current_time));
    #endif

    // Cleanup code
    ma_message_release(publisher_payload);    publisher_payload = NULL;
    free(my_string); my_string = NULL;
}


/******************************************************************************
Subscriber callback function. This callback function gets executed when the
subscriber receives a message corresponding to the topic it had subscribed for.
******************************************************************************/
ma_error_t subscriber_callback_func(const char *topic, ma_message_t *subscriber_payload, void *cb_data)
{
    // Variable to hold current time
    time_t current_time;

    // Getting current time. Obtain the time as soon as possible for better accurancy.
    // Here, the time is obtained as the first executable statement in the call-back function.
    time(&current_time);

    // Incrementing count of number of messages received by subscriber
    total_msg_sub_rcvd++;

    // Handling the message payload
    if(subscriber_payload == NULL)
    {
        printf("No payload in the message received from publisher\n");
        return MA_ERROR_UNEXPECTED;
    }

    {
        // Variant to hold the variant extracted from message
        ma_variant_t *variant_ptr = NULL;

        // To hold the property retrieved from variant
        const char *publisher_name = NULL;
        const char *message_number = NULL;

        // Buffer to hold string extracted from variant
        ma_buffer_t *buffer = NULL;

        // String Message retrieved from the buffer and its size
        const char *my_string = NULL;
        size_t size = 0;

        ma_message_get_property(subscriber_payload, "PUBLISHER_NAME", &publisher_name);
        ma_message_get_property(subscriber_payload, "MESSAGE_NUMBER", &message_number);
        ma_message_get_payload(subscriber_payload, &variant_ptr);
        ma_variant_get_string_buffer(variant_ptr, &buffer);
        ma_buffer_get_string(buffer, &my_string, &size);
        ma_buffer_release(buffer); buffer = NULL;

        printf("Subscriber - %s Received Message (%d) from Publisher - %s \n", endpoint_name, atoi(message_number), publisher_name);

        #ifdef DEBUG
            MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_INFO, "Subscriber - %s received Message ( %d ) from Publisher - %s \t Topic - %s, Message Payload - %s, Message Received at - %s", endpoint_name, atoi(message_number), publisher_name, topic, my_string, ctime(&current_time));
        #endif

        // Cleanup code
        ma_variant_release(variant_ptr); variant_ptr = NULL;
    }

    return MA_OK;
}

/******************************************************************************
Client call back function. This function gets executed -
1) as a call back function, whenever an asynchronous client receives response
   from server.
2) called by send_client_request() function in this sample code to process the
   response packet received by synchronous client.
******************************************************************************/
ma_error_t process_server_response(ma_error_t status, ma_message_t *response_payload, void *cb_data, ma_msgbus_request_t *request)
{
    // Variable to hold current time
    time_t current_time;

    // Getting current time. Obtain the time as soon as possible for better accurancy.
    // Here, the time is obtained as the first executable statement in the call-back function.
    time(&current_time);

    // Incrementing count of number of responses received by client
    total_resp_recvd_client++;

    // Handling the message payload
    if(response_payload == NULL)
    {
        printf("No payload in the response received from server\n");
        return MA_ERROR_UNEXPECTED;
    }

    {
        // Variant to hold the variant extracted from message
        ma_variant_t *variant_ptr = NULL;

        // To hold the property retrieved from variant
        const char *client_name = NULL;
        const char *request_number = NULL;

        // Buffer to hold string extracted from variant
        ma_buffer_t *buffer = NULL;

        // String Message retrieved from the buffer and its size
        const char *my_string = NULL;
        size_t size = 0;

        ma_message_get_property(response_payload, "CLIENT_NAME", &client_name);
        ma_message_get_property(response_payload, "REQUEST_NUMBER", &request_number);
        ma_message_get_payload(response_payload, &variant_ptr);
        ma_variant_get_string_buffer(variant_ptr, &buffer);
        ma_buffer_get_string(buffer, &my_string, &size);
        ma_buffer_release(buffer); buffer = NULL;

        printf("Client - %s received Response for Request No. - %d \n", endpoint_name, atoi(request_number));

        #ifdef DEBUG
            MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_INFO, "Client - %s received response for Request No. - %d \t Response Payload - %s, Response Received at - %s", endpoint_name, atoi(request_number), my_string, ctime(&current_time));
        #endif

        // Cleanup code
        ma_variant_release(variant_ptr); variant_ptr = NULL;
    }

    return MA_OK;
}


/******************************************************************************
Client function for sending the Request to server. This function sends a single
request message (sync/async or send & forget type) of given size on the message
bus.
******************************************************************************/
void send_client_request()
{
    // Variable to hold current time
    time_t current_time;

    // Pointer pointing to the 'message_size_in_bytes' message.
    char *my_string = NULL;

    // Variant Pointer for holding the variant created from message string.
    ma_variant_t *variant_ptr = NULL;

    // Request Message to be sent to server
    ma_message_t *request_payload = NULL;

    // Response message received from server for a synchronous request
    ma_message_t *response_payload = NULL;

    // Request Packet
    ma_msgbus_request_t *request_packet = NULL;

    // Allocate memory for storing the message payload (1 extra byte for NULL).
    // Fill payload with random data.
    // Create variant of string.
    my_string = (char *)calloc(message_size_in_bytes + 1, 1);
    generate_message(my_string, message_size_in_bytes);
    ma_variant_create_from_string(my_string, strlen(my_string), &variant_ptr);

    // Increment the count of the client requests sent
    total_client_req_sent++;

    // Creating fresh message.
    // Set Client Number and Request Number in the message property.
    // Set string variant as message payload and then release variant
    // Send the request message based on the client type.
    ma_message_create(&request_payload);

    {
        char value[10];
        ma_message_set_property(request_payload, "CLIENT_NAME", endpoint_name);

        sprintf(value, "%d", total_client_req_sent);
        ma_message_set_property(request_payload, "REQUEST_NUMBER", value);
    }

    ma_message_set_payload(request_payload, variant_ptr);
    ma_variant_release(variant_ptr); variant_ptr = NULL;

    // Sending correct request based on the Client type
    if(endpoint_type == SEND_FORGET_CLIENT) ma_msgbus_send_and_forget(p_endpoint, request_payload);
    if(endpoint_type == SYNC_CLIENT) ma_msgbus_send(p_endpoint, request_payload, &response_payload);
    if(endpoint_type == ASYNC_CLIENT) ma_msgbus_async_send(p_endpoint, request_payload, process_server_response, NULL, &request_packet);

    // Getting the time at which the request is sent
    time(&current_time);

    printf("Client - %s sent Request No. - %d \n", endpoint_name, total_client_req_sent);

    #ifdef DEBUG
        MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_INFO, "Client - %s sent request No. - %d \t Message Payload - %s, Request Sent at - %s", endpoint_name, total_client_req_sent, my_string, ctime(&current_time));
    #endif

    // For synchronous client, process the received response.
    if(endpoint_type == SYNC_CLIENT)
    {
        process_server_response(MA_OK, response_payload, NULL, request_packet);
    }

    // Cleanup code
    ma_message_release(request_payload); request_payload = NULL;
    free(my_string); my_string = NULL;
}


/******************************************************************************
Call back function to be executed when the server receives a request from the
client. This server will handle both synchronous as well as asynchronous requests.
******************************************************************************/
ma_error_t respond_to_client_requests(ma_msgbus_server_t *server, ma_message_t *request_payload, void *cb_data, ma_msgbus_client_request_t *c_request)
{
    // Variable to hold current time
    time_t current_time;

    // Will hold the response message created by this server for incoming request
    ma_message_t* response_payload = NULL;

    // String Message retrieved from the buffer and its size
    char *my_string = NULL;
    size_t size = 0;

    // To hold the property retrieved from variant
    const char *client_name = NULL;
    const char *request_number = NULL;

    // Getting current time. Obtain the time as soon as possible for better accurancy.
    // Here, the time is obtained as the first executable statement in the call-back function.
    time(&current_time);

    // Incrementing count of number of requests handled by the server
    total_req_handled_server++;

    // Handling the message payload
    if(request_payload == NULL)
    {
        printf("No payload in the request received from client\n");
        return MA_ERROR_UNEXPECTED;
    }

    // Processing the incoming message to the server
    {
        // Variant to hold the variant extracted from message
        ma_variant_t *variant_ptr = NULL;

        // Buffer to hold string extracted from variant
        ma_buffer_t *buffer = NULL;

        ma_message_get_property(request_payload, "CLIENT_NAME", &client_name);
        ma_message_get_property(request_payload, "REQUEST_NUMBER", &request_number);
        ma_message_get_payload(request_payload, &variant_ptr);
        ma_variant_get_string_buffer(variant_ptr, &buffer);
        ma_buffer_get_string(buffer, &my_string, &size);
        ma_buffer_release(buffer); buffer = NULL;

        printf("Server - %s received Request No. - %d from Client - %s \n", endpoint_name, atoi(request_number), client_name);

        #ifdef DEBUG
            MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_INFO, "Server - %s received Request No. - %d from Client - %s \t Request Payload - %s, Request Receive Time - %s", endpoint_name, atoi(request_number), client_name, my_string, ctime(&current_time));
        #endif

        // Cleanup code
        ma_variant_release(variant_ptr); variant_ptr = NULL;
    }

    // Sending the response to client for incoming message
    {
        // Variant to hold the variant of response string
        ma_variant_t *variant_ptr = NULL;

        // Generate response string by processing the payload obtained from request
        // Convert it to variant
        generate_response(my_string);
        ma_variant_create_from_string(my_string, strlen(my_string), &variant_ptr);

        // Creating the response message
        ma_message_create(&response_payload);
        ma_message_set_property(response_payload, "CLIENT_NAME", client_name);
        ma_message_set_property(response_payload, "REQUEST_NUMBER", request_number);

        ma_message_set_payload(response_payload, variant_ptr);
        ma_variant_release(variant_ptr); variant_ptr = NULL;

        ma_msgbus_server_client_request_post_result(c_request, MA_OK, response_payload);

        ma_message_release(response_payload);
    }

    // For cloning verification. To Test, create a subscriber to listen to "hello" topic
    //{
    //    ma_message_t *response1 = NULL;
    //    ma_message_t *response2 = NULL;

    //    ma_message_clone(request_payload, &response1);
    //    ma_message_clone(request_payload, &response2);

    //    ma_msgbus_server_client_request_post_result(c_request, MA_OK, response1);

    //    ma_msgbus_publish(msgbus_obj, "hello", MSGBUS_CONSUMER_REACH_OUTPROC, response2);
    //
    //    ma_message_release(response1);
    //    ma_message_release(response2);
    //}

    return MA_OK;
}


/******************************************************************************
Timer callback functions to be executed when this endpoint is running.

periodic_timer_callback - Will be called periodically by publisher/client to
send messages.

exit_timer_callback - Will be called only once when the total run duration
timer expires. This will stop all timers.
******************************************************************************/
void periodic_timer_callback(uv_timer_t* handle, int status)
{
    if(endpoint_type == PUBLISHER) publish();
    if((endpoint_type == SYNC_CLIENT) || (endpoint_type == ASYNC_CLIENT) || (endpoint_type == SEND_FORGET_CLIENT)) send_client_request();
}

void exit_timer_callback(uv_timer_t* handle, int status)
{
    uv_timer_stop(&periodic_timer);
    uv_timer_stop(&exit_timer);
}


/******************************************************************************
Start of Execution -

1) Parse command line arguments.
2) Create message bus.
3) Create appropriate endpoint as per input command line arguments.
******************************************************************************/
int main(int argc, char* argv[])
{
    MA_TRACKLEAKS;

    /**************************************************************************
    Processing Command Line Arguments.
    **************************************************************************/
    {
        int c;

        int counter = 0;

        while ((c = ma_getopt(argc, argv, "e:n:r:t:F:f:m:S:T:h")) != -1)
        {
            switch (c)
            {
                case 'e':      if(strcmp(ma_getoptarg(), "PUBLISHER") == 0) endpoint_type = PUBLISHER;
                          else if(strcmp(ma_getoptarg(), "SUBSCRIBER") == 0) endpoint_type = SUBSCRIBER;
                          else if(strcmp(ma_getoptarg(), "ASYNC_CLIENT") == 0) endpoint_type = ASYNC_CLIENT;
                          else if(strcmp(ma_getoptarg(), "SYNC_CLIENT") == 0) endpoint_type = SYNC_CLIENT;
                          else if(strcmp(ma_getoptarg(), "SEND_FORGET_CLIENT") == 0) endpoint_type = SEND_FORGET_CLIENT;
                          else if(strcmp(ma_getoptarg(), "SERVER") == 0) endpoint_type = SERVER;
                          else { print_help(); return -1; }
                          break;

                case 'n': endpoint_name = ma_getoptarg(); break;

                case 'r':      if(strcmp(ma_getoptarg(), "INPROC") == 0) reach = MSGBUS_CONSUMER_REACH_INPROC;
                          else if(strcmp(ma_getoptarg(), "OUTPROC") == 0) reach = MSGBUS_CONSUMER_REACH_OUTPROC;
                          else if(strcmp(ma_getoptarg(), "OUTBOX") == 0) reach = MSGBUS_CONSUMER_REACH_OUTBOX;
                            else reach = MSGBUS_CONSUMER_REACH_OUTPROC; // Default value
                          break;

                 case 't':      if(strcmp(ma_getoptarg(), "THREAD_POOL") == 0) thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;
                          else if(strcmp(ma_getoptarg(), "THREAD_IO") == 0) thread_option = MA_MSGBUS_CALLBACK_THREAD_IO;
                          else thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL; // Default value
                          break;

                case 'F': total_run_duration = atoi(ma_getoptarg()); break;

                case 'f': message_frequency_in_ms = atoi(ma_getoptarg()); break;

                case 'm': message_size_in_bytes = atoi(ma_getoptarg()); break;

                case 'S': server_to_communicate = ma_getoptarg(); break;

                case 'T': topic = ma_getoptarg(); break;

                case 'h': print_help(); return 0; break;

                case '?': printf("Unknown Command Line option %c", (char)c); break;
            }
        }

        if(!endpoint_name)
        {
            printf("Incomplete Options : Endpoint Name is not provided\n");
            print_help();
            return -1;
        }

        if(((endpoint_type == SUBSCRIBER) || (endpoint_type == PUBLISHER))  && (topic == NULL))
        {
            printf("Incomplete Options : 'Topic' Missing\n");
            print_help();
            return -1;
        }

        if(((endpoint_type == SYNC_CLIENT) || (endpoint_type == ASYNC_CLIENT) || (endpoint_type == SEND_FORGET_CLIENT)) &&
            (server_to_communicate == NULL))
        {
            printf("Incomplete Options : Provide Server Name to Communicate \n");
            print_help();
            return -1;
        }

        #ifdef DEBUG
            printf("Endpoint Type - %d\n", endpoint_type);
            printf("Endpoint Name - %s\n", endpoint_name);
            printf("Endpoint Reach - %d\n", reach);
            printf("Thread Option - %d\n", thread_option);
            printf("Total Run Duration (in seconds) - %ld\n", total_run_duration);
            printf("Message Frequency (in milli-seconds) - %ld\n", message_frequency_in_ms);
            printf("Message Size (in bytes) - %ld\n", message_size_in_bytes);
            printf("Server to Communicate - %s\n", server_to_communicate);
            printf("Host Name - %s\n", host_name);
            printf("Topic - %s\n", topic);
        #endif

    }

    /**************************************************************************
    Dumping PID info of currently running process in file.
    **************************************************************************/
    dump_pid_info();

    /**************************************************************************
    Create message Bus.
    Set Logger.
    Start Message Bus.
    **************************************************************************/
    ma_msgbus_create(LOG_FACILITY_NAME, &msgbus_obj);

    #ifdef DEBUG
        ma_file_logger_create(NULL, "MessageBus", ".log", &file_logger);
        ma_msgbus_set_logger(msgbus_obj, (ma_logger_t *)file_logger);
    #endif

    ma_msgbus_start(msgbus_obj);

    /**************************************************************************
    If endpoint is a SUBSCRIBER,
    1) Create a subscriber.
    2) Set reachability and thread model options. Set timeout equal to the total
       duration of test run, so that no message gets discarded due to timeout.
    3) Register for 'Topic' for listening.
    **************************************************************************/
    if(endpoint_type == SUBSCRIBER)
    {
        ma_msgbus_subscriber_create(msgbus_obj, &p_subscriber);
        ma_msgbus_subscriber_setopt(p_subscriber, MSGBUSOPT_REACH, reach);
        ma_msgbus_subscriber_setopt(p_subscriber, MSGBUSOPT_THREADMODEL, thread_option);
        ma_msgbus_subscriber_register(p_subscriber, topic, subscriber_callback_func, NULL);

        #ifdef DEBUG
        printf("Started Subscriber - %s\n", endpoint_name);
        #endif
    }

    /**************************************************************************
    If endpoint is a SERVER,
    1) Create a server
    2) Set reachability and thread model options. Set timeout equal to the total
       duration of test run, so that no message gets discarded due to timeout.
    3) Start server for listening to client requests (Register callback function).
    **************************************************************************/
    if(endpoint_type == SERVER)
    {
        ma_msgbus_server_create(msgbus_obj, endpoint_name, &p_server);
        ma_msgbus_server_setopt(p_server, MSGBUSOPT_REACH, reach);
        ma_msgbus_server_setopt(p_server, MSGBUSOPT_THREADMODEL, thread_option);
        ma_msgbus_server_start(p_server, respond_to_client_requests, NULL);

        #ifdef DEBUG
            printf("Started Server - %s\n", endpoint_name);
        #endif
    }

    /**************************************************************************
    If endpoint is a CLIENT,
    1) Create a client.
    2) Set reachability and thread model options. Set timeout equal to the total
       duration of test run, so that no message gets discarded due to timeout.
    **************************************************************************/
    if((endpoint_type == SYNC_CLIENT) || (endpoint_type == ASYNC_CLIENT) || (endpoint_type == SEND_FORGET_CLIENT))
    {
        ma_msgbus_endpoint_create(msgbus_obj, server_to_communicate, host_name, &p_endpoint);
        ma_msgbus_endpoint_setopt(p_endpoint, MSGBUSOPT_REACH, reach);
        ma_msgbus_endpoint_setopt(p_endpoint, MSGBUSOPT_THREADMODEL, thread_option);

        #ifdef DEBUG
            printf("Started Client - %s\n", endpoint_name);
        #endif
    }

    /**************************************************************************
    Run logic for executing this client till "total_run_duration" seconds.
    **************************************************************************/
    wait_loop = uv_loop_new();
    uv_timer_init(wait_loop, &exit_timer);
    uv_timer_init(wait_loop, &periodic_timer);

    // For server or subscriber, only exit timer is to be initialized. For clients
    // and publishers, both exit timer and message/request sending timer is also to
    // be initialized.
    if((endpoint_type == SERVER) || (endpoint_type == SUBSCRIBER))
    {
        uv_timer_start(&exit_timer, exit_timer_callback, total_run_duration * 1000, 0);
    }
    else
    {
        uv_timer_start(&exit_timer, exit_timer_callback, total_run_duration * 1000, 0);
        uv_timer_start(&periodic_timer, periodic_timer_callback, 0, message_frequency_in_ms);
    }

    uv_run(wait_loop, UV_RUN_DEFAULT);
    uv_loop_delete(wait_loop);

    /**************************************************************************
    Log the overall statistics after the CTRL+C is hit. This information can be
    used to identify the number of messages dropped during communication.
    **************************************************************************/
    switch(endpoint_type)
    {
        case PUBLISHER          : printf("Publisher No. - %s\t", endpoint_name);
                                  printf("Msg. Published - %d\n", total_msg_published);
                                  break;
        case SUBSCRIBER         : printf("Subscriber No. - %s\t", endpoint_name);
                                  printf("No. of Msg. Rcvd. - %d\n", total_msg_sub_rcvd);
                                  break;
        case ASYNC_CLIENT       :
        case SYNC_CLIENT        : printf("Client No. - %s\t", endpoint_name);
                                  printf("Requests Sent - %d\t", total_client_req_sent);
                                  printf("Response Rcvd. - %d\n", total_resp_recvd_client);
                                  break;
        case SEND_FORGET_CLIENT : printf("Client No. - %s\t", endpoint_name);
                                  printf("Requests Sent - %d\n", total_client_req_sent);
                                  break;
        case SERVER             : printf("Server No. - %s\t", endpoint_name);
                                  printf("No. of Req. handled - %d\n", total_req_handled_server);
                                  break;
    }


    performance_test_report_file_handle = fopen(performance_test_report_file_name, "a+");

    switch(endpoint_type)
    {
        case PUBLISHER          : fprintf(performance_test_report_file_handle, "Publisher No. - %s\t", endpoint_name);
                                  fprintf(performance_test_report_file_handle, "Msg. Published - %d\n", total_msg_published);
                                  break;
        case SUBSCRIBER         : fprintf(performance_test_report_file_handle, "Subscriber No. - %s\t", endpoint_name);
                                  fprintf(performance_test_report_file_handle, "No. of Msg. Rcvd. - %d\n", total_msg_sub_rcvd);
                                  break;
        case ASYNC_CLIENT       :
        case SYNC_CLIENT        : fprintf(performance_test_report_file_handle, "Client No. - %s\t", endpoint_name);
                                  fprintf(performance_test_report_file_handle, "Requests Sent - %d\t", total_client_req_sent);
                                  fprintf(performance_test_report_file_handle, "Response Rcvd. - %d\n", total_resp_recvd_client);
                                  break;
        case SEND_FORGET_CLIENT : fprintf(performance_test_report_file_handle, "Client No. - %s\t", endpoint_name);
                                  fprintf(performance_test_report_file_handle, "Requests Sent - %d\n", total_client_req_sent);
                                  break;
        case SERVER             : fprintf(performance_test_report_file_handle, "Server No. - %s\t", endpoint_name);
                                  fprintf(performance_test_report_file_handle, "No. of Req. handled - %d\n", total_req_handled_server);
                                  break;
    }

    fclose(performance_test_report_file_handle);

    /**************************************************************************
    Unregister and release the subscriber.
    **************************************************************************/
    if(endpoint_type == SUBSCRIBER)
    {
        ma_msgbus_subscriber_unregister(p_subscriber);
        ma_msgbus_subscriber_release(p_subscriber);

        #ifdef DEBUG
            printf("Stopped Subscriber - %s\n", endpoint_name);
        #endif
    }

    /**************************************************************************
    Stop and release server.
    **************************************************************************/
    if(endpoint_type == SERVER)
    {
        ma_msgbus_server_stop(p_server);
        ma_msgbus_server_release(p_server);

        #ifdef DEBUG
            printf("Stopped Server - %s\n", endpoint_name);
        #endif
    }

    /**************************************************************************
    Release the endpoint client.
    **************************************************************************/
    if((endpoint_type == SYNC_CLIENT) || (endpoint_type == ASYNC_CLIENT) || (endpoint_type == SEND_FORGET_CLIENT))
    {
        ma_msgbus_endpoint_release(p_endpoint);

        #ifdef DEBUG
            printf("Stopped Client - %s\n", endpoint_name);
        #endif
    }

    /**************************************************************************
    Release message-bus
    **************************************************************************/
    ma_msgbus_stop(msgbus_obj, MA_TRUE);
    ma_msgbus_release(msgbus_obj);

    #ifdef DEBUG
        printf("Stopped Message Bus - %d\n", endpoint_name);
    #endif

    /**************************************************************************
    Cleanup loggers.
    **************************************************************************/
    #ifdef DEBUG
        ma_file_logger_release(file_logger);
    #endif
}





