/******************************************************************************
Sample test for verifying the functionality of network library.

Command to be used - network_sample.exe <Options>

Options List -
------------
-T, -u, --upload_file <FILE>                     It is an upload request. The
                                                 next argument is considered as
                                                 file to be uploaded.
-o, -d, --output <FILE>                          It is a download request. The
                                                 next argument is considered as
                                                 file to store the downloaded
                                                 content.
-a, --append <FILE>                              It is a download request. The
                                                 next argument is considered as
                                                 file to store the downloaded
                                                 content. The downloaded content
                                                 is appended to existing file.
--url <URL>                                      The next argument is the URL
                                                 to connect to.
-v, --verbose                                    Display verbose information
                                                 useful for debugging.
--connect-timeout <seconds>                      Maximum time in seconds that
                                                 you allow the connection to
                                                 the server to take.
-m, --max-time, --transfer-timeout <seconds>     Maximum time in seconds that
                                                 you allow the whole operation
                                                 to take.
-#, --progress-bar                               Display progress information.
--auth <username:password>                       Use username and password while
                                                 connecting to server.
--user <username>                                Username. Will not override the
                                                 username provided with --auth
--password <Password>                            Password. Will not override the
                                                 password provided with --auth
-x, --proxy <IP Address:Port:User:Password>      Use this proxy server. Username
                                                 and Password are optional.
                                                 Multiple proxy servers can be
                                                 provided using this syntax.
--ssl                                            Use SSL secure connection.
--cert                                           SSL certificate path.
-r, --range <start-end>                          Specify range for partial download.
******************************************************************************/
#include <stdio.h>
#include <uv.h>
#include "ma/logger/ma_console_logger.h"
#include "ma/internal/utils/network/ma_net_client_service.h"
#include "ma/internal/utils/network/ma_url_request.h"
#include "ma/internal/utils/datastructures/ma_stream.h"
#include "ma/proxy/ma_proxy.h"

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "network"
/******************************************************************************
Console Logger pointer. Console logger is being used for network logging as well
as logging for any failures within this sample code.
******************************************************************************/
ma_console_logger_t *console_logger = NULL;

/******************************************************************************
Timer call-back function. This function gets executed whenever timer expires.
The timer event is stopped when the network communication is complete.
******************************************************************************/
int nw_communication_in_progress = 1;

void timer_event(uv_timer_t *handle, int status)
{
    if(nw_communication_in_progress)
    {
        #ifdef _DEBUG
            printf("Timer event executed\n");
        #endif
    }
    else
    {
        // N/W communication is complete. No need to keep timer running.
        uv_timer_stop(handle);
    }
}

/******************************************************************************
Connection callback. Returns MA_OK.
******************************************************************************/
ma_error_t request_connect_callback(void *userdata, ma_url_request_t *net_request)
{
    #ifdef _DEBUG
        printf("Connection Established.\n");
    #endif

    return MA_OK;
}

/******************************************************************************
Request Complete callback. Sets the 'nw_communication_in_progress' flag to 0 so
that Idle watcher can exit.
******************************************************************************/
void request_complete_callback(ma_error_t status, long response_code, void *userdata, ma_url_request_t *net_request)
{
    printf("Network IO request is compelete. Response Code - %d \n", response_code);
    nw_communication_in_progress = 0;
}

/******************************************************************************
Progress Callback. This function displays the %age of upload/download completed
till now.
******************************************************************************/
int progress_callback(double dltotal, double dlnow, double ultotal, double ulnow,void *userdata)
{
    float download_percentage = 0.0f;
    float upload_percentage = 0.0f;

    if(dltotal != 0)
    {
        download_percentage = (float)(dlnow/dltotal) * 100.0f;
    }

    if(ultotal != 0)
    {
        upload_percentage = (float)(ulnow/ultotal) * 100.0f;
    }

    printf("Downloaded - %f% \t Uploaded - %f% \n", download_percentage, upload_percentage);
    return 0;
}

int main(int argc, char* argv[])
{
    // Counter
    int count = 1;

    // URL
    const char *url = NULL;

    // Request type (upload/download/download append).
    int request_type_download = 1; // Download by default
    const char *file_to_be_uploaded = NULL;
    const char *write_output_to_file = NULL;

    // Verbose enabled/disabled
    int verbose_enabled = 0;

    // Connection and Transfer timeout values
    unsigned long int connect_timeout = 60;
    unsigned long int transfer_timeout = 60 * 60;

    // To show the progress information or not
    int show_progress_bar = 0;

    // User authentication. Username and Password.
    int use_authentication = 0;
    const char *user = NULL;
	const char *domainname = NULL;
    const char *password = NULL;

    // SSL authentication. SSL Ceritificate.
    int ssl_enabled = 0;
    const char *certificate = NULL;

    // Range download.
    int range_enabled = 0;
    unsigned long int start_range = 0;
    unsigned long int end_range = 0;

    // Proxy information
    int use_proxy = 0;
    char *proxy_ip = NULL;
    unsigned int proxy_port = 0;
    char *proxy_user = NULL;
    char *proxy_password = NULL;

    // Handlers
    ma_event_loop_t *event_loop = NULL;
    uv_timer_t timer;
    uv_loop_t *loop = NULL;
    ma_net_client_service_t *net_service = NULL;
    ma_url_request_t *url_request = NULL;
    ma_stream_t *io_stream = NULL;
    ma_url_request_io_t *url_request_io = NULL;
    ma_url_request_progress_t *url_request_progress = NULL;
    ma_url_request_auth_t * url_authentication_info = NULL;
    ma_url_request_ssl_info_t *ssl_info = NULL;
    ma_url_request_range_t *range_info = NULL;
    ma_proxy_list_t *proxy_list = NULL;
    ma_proxy_t *proxy = NULL;

    ma_error_t error_code = MA_OK;

    /**************************************************************************
    Creating Console Logger. Creating console logger as soon as possible, so that
    error/information logging can be started
    **************************************************************************/
    if((error_code = ma_console_logger_create(&console_logger)) != MA_OK)
    {
        printf("Console Logger could not be created.\n");
        return error_code;
    }

    /**************************************************************************
    Parsing through argument list.
    **************************************************************************/
    for(count = 1; count < argc; count++)
    {
        if((strcmp(argv[count], "-T") == 0) || (strcmp(argv[count], "--upload_file") == 0) || (strcmp(argv[count], "-u") == 0))
        {
            request_type_download = 0;
            file_to_be_uploaded = argv[count + 1];

            #ifdef _DEBUG
                printf("Request type is upload. %s file is to be uploaded.\n", file_to_be_uploaded);
            #endif

            // Next argument is already parsed, so skip it.
            count++;
            continue;
        }

        if((strcmp(argv[count], "-o") == 0) || (strcmp(argv[count], "--output") == 0) || (strcmp(argv[count], "-d") == 0))
        {
            request_type_download = 1;
            write_output_to_file = argv[count + 1];

            #ifdef _DEBUG
                printf("Request type is download. The downloaded content will be written to %s file.\n", write_output_to_file);
            #endif

            count++;
            continue;
        }

        if((strcmp(argv[count], "-a") == 0) || (strcmp(argv[count], "--append") == 0))
        {
            request_type_download = 2;
            write_output_to_file = argv[count + 1];

            #ifdef _DEBUG
                printf("Request type is download with append. The downloaded content will be written to %s file.\n", write_output_to_file);
            #endif

            count++;
            continue;
        }

        if(strcmp(argv[count], "--url") == 0)
        {
            url = argv[count + 1];

            #ifdef _DEBUG
                printf("URL - %s\n", url);
            #endif

            count++;
            continue;
        }

        if((strcmp(argv[count], "-v") == 0) || (strcmp(argv[count], "--verbose") == 0))
        {
            verbose_enabled = 1;

            #ifdef _DEBUG
                 printf("Verbose Enabled\n");
            #endif

            continue;
        }

        if(strcmp(argv[count], "--connect-timeout") == 0)
        {
            connect_timeout = atoi(argv[count + 1]);

            #ifdef _DEBUG
                 printf("Connection Timeout - %d\n", connect_timeout);
            #endif

            count++;
            continue;
        }

        if((strcmp(argv[count], "-m") == 0) || (strcmp(argv[count], "--max-time") == 0) || (strcmp(argv[count], "--transfer-timeout") == 0))
        {
            transfer_timeout = atoi(argv[count + 1]);

            #ifdef _DEBUG
                 printf("Transfer Timeout - %d\n", transfer_timeout);
            #endif

            count++;
            continue;
        }

        if((strcmp(argv[count], "--progress-bar") == 0) || (strcmp(argv[count], "-#") == 0))
        {
            show_progress_bar = 1;

            #ifdef _DEBUG
                 printf("Progress Information Enabled\n");
            #endif

            continue;
        }

        if(strcmp(argv[count], "--auth") == 0)
        {
            // Obtaining username and password by seperating the [username:password]
            // format. The argv[count + 1] command-line argument will hold this
            // authentication information.
            char *seperator = ":";

            use_authentication = 1;
            user = strtok(argv[count + 1], seperator);
            password = strtok(NULL, seperator);

            #ifdef _DEBUG
                 printf("User Authentication Enabled. Username - %s, Password - %s\n", user, password);
            #endif

            count++;
            continue;
        }

        // Override the password provided with --auth username:password
        if(strcmp(argv[count], "--user") == 0)
        {
			use_authentication = 1;
            if(user == NULL)
            {
                user = argv[count + 1];
            }

            #ifdef _DEBUG
                 printf("Username for authentication - %s\n", user);
            #endif

            count++;
            continue;
        }

		 // Override the password provided with --auth username:password
        if(strcmp(argv[count], "--domainname") == 0)
        {
            if(domainname == NULL)
            {
                domainname = argv[count + 1];
            }

            #ifdef _DEBUG
                 printf("domainname for authentication - %s\n", domainname);
            #endif

            count++;
            continue;
        }

        if(strcmp(argv[count], "--password") == 0)
        {
            if(password == NULL)
            {
                password = argv[count + 1];
            }

            #ifdef _DEBUG
                 printf("Password for authentication - %s\n", password);
            #endif

            count++;
            continue;
        }

        if(strcmp(argv[count], "--ssl") == 0)
        {
            ssl_enabled = 1;

            #ifdef _DEBUG
                 printf("SSL Enabled\n");
            #endif

            continue;
        }

        if(strcmp(argv[count], "--cert") == 0)
        {
            ssl_enabled = 1;
            certificate = argv[count + 1];

            #ifdef _DEBUG
                 printf("SSL Certificate Path - %s\n", certificate);
            #endif

            count++;
            continue;
        }

        if((strcmp(argv[count], "-r") == 0) || (strcmp(argv[count], "--range") == 0))
        {
            // Obtaining start and end range by seperating the [start-end] format
            // the argv[count + 1] the command-line argument will hold the range
            char *seperator = "-";

            range_enabled = 1;
            start_range = atoi(strtok(argv[count + 1], seperator));
            end_range = atoi(strtok(NULL, seperator));

            #ifdef _DEBUG
                 printf("Performing partial download from %d to %d bytes\n", start_range, end_range);
            #endif

            count++;
            continue;
        }

        if((strcmp(argv[count], "-x") == 0) || (strcmp(argv[count], "--proxy") == 0))
        {
            // Obtaining Proxy IP Address, Port, username and password by seperating
            // the [IP Address:port:username:password] format. The argv[count + 1]
            // command-line argument will hold this authentication information.
            char *seperator = ":";

            // Create proxy list when the --proxy is encountered on command-line for
            // the first time.
            if(use_proxy == 0)
            {
                if(ma_proxy_list_create(&proxy_list) != MA_OK)
                {
                    MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "List of valid proxy servers could not be created.");
                }
            }

            // Set use_proxy to identify that proxy servers are enabled.
            use_proxy = 1;

            proxy_ip = strtok(argv[count + 1], seperator);
            proxy_port = atoi(strtok(NULL, seperator));
            proxy_user = strtok(NULL, seperator);
            proxy_password = strtok(NULL, seperator);
            count++;

            // Creating a proxy object
            ma_proxy_create(&proxy);
			ma_proxy_set_address(proxy, proxy_ip);
			ma_proxy_set_port(proxy, proxy_port);
			ma_proxy_set_protocol_type(proxy, MA_PROXY_TYPE_BOTH);
			ma_proxy_set_authentication(proxy, MA_TRUE, proxy_user, proxy_password);

            #ifdef _DEBUG
                 printf("User Proxy Server : [%s : %d : %s : %s]\n", proxy_ip, proxy_port, proxy_user, proxy_password);
            #endif

            // Adding proxy object to list
            if(ma_proxy_list_add_proxy(proxy_list, proxy) != MA_OK)
            {
                MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Proxy Server %s could not be added to list.", proxy_ip);
            }

            // De-allocate memory allocated to proxy object
            ma_proxy_release(proxy);
            continue;
        }

        // If control reaches here, it means that an unknown/mistyped option has
        // been observed from command line.
        MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Unknown/Mis-typed Option - %s", argv[count]);
    }

    /**************************************************************************
    1) Creating Event Loop.
    2) Extracting UV loop from the Event Loop.
    3) Using the event loop it to initiate a timer. The timer will start immediately
       and will repeat every 500ms.
    **************************************************************************/
    if((error_code = ma_event_loop_create(&event_loop)) != MA_OK)
    {
        MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Event Loop could not be created");
        return error_code;
    }

    ma_event_loop_get_loop_impl(event_loop, &loop);
    uv_timer_init(loop, &timer);
    uv_timer_start(&timer, timer_event, 0, 500);

    /**************************************************************************
    Creating and starting Network Client service.
    **************************************************************************/
    if((error_code = ma_net_client_service_create(event_loop, &net_service)) != MA_OK)
    {
        MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Network Client service could not be instantiated");
        return error_code;
    }

    ma_net_client_service_setlogger(net_service, (ma_logger_t *)console_logger);
    ma_net_client_service_start(net_service);

    /**************************************************************************
    Creating the URL request and setting the options.
    **************************************************************************/
    if((url == NULL) || ((error_code = (ma_url_request_create(net_service, url, &url_request))) != MA_OK))
    {
        MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "URL Request could not be created");
        return MA_ERROR_NETWORK_INVALID_URL;
    }

    // Setting request type option (POST/GET)
    error_code = request_type_download ?
                 ma_url_request_set_option(url_request, MA_URL_REQUEST_OPTION_REQUEST_TYPE, (unsigned long) MA_URL_REQUEST_TYPE_GET) :
                 ma_url_request_set_option(url_request, MA_URL_REQUEST_OPTION_REQUEST_TYPE, (unsigned long) MA_URL_REQUEST_TYPE_POST);

    if(error_code != MA_OK)
    {
        MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "URL Request Type could not be set");
    }

    // Enable Verbose or default logging.
    error_code = verbose_enabled ?
                 ma_url_request_set_option(url_request, MA_URL_REQUEST_OPTION_VERBOSE_LOG, (unsigned long) 1) :
                 ma_url_request_set_option(url_request, MA_URL_REQUEST_OPTION_VERBOSE_LOG, (unsigned long) 0);

    if(error_code != MA_OK)
    {
        MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "URL Request Verbose settings could not be done");
    }

    // Default value of connection timeout is set to 60 seconds. Default value of transfer timeout is set to
    // 1 hour, big enough, not to cause the timeout error, if the option is not set.
    error_code = ma_url_request_set_option(url_request, MA_URL_REQUEST_OPTION_CONNECT_TIMEOUT, connect_timeout);
    error_code = ma_url_request_set_option(url_request, MA_URL_REQUEST_OPTION_TRANSFER_TIMEOUT, transfer_timeout);

    if(error_code != MA_OK)
    {
        MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "URL Request Connection or Transfer Time-out could not be set");
    }

    /**************************************************************************
    Creating appropriate type of stream based on the request type. Setting IO
    callbacks accordingly.
    **************************************************************************/
    url_request_io = (ma_url_request_io_t *)calloc(1, sizeof(ma_url_request_io_t));
    MA_URL_REQUEST_IO_INIT(url_request_io);

    if(request_type_download == 0)
    {
        error_code = ma_fstream_create(file_to_be_uploaded, MA_STREAM_ACCESS_FLAG_READ, NULL, &io_stream);
        MA_URL_REQUEST_IO_SET_READ_STREAM(url_request_io, io_stream);
    }
    else if(request_type_download == 1)
    {
        error_code = ma_fstream_create(write_output_to_file, MA_STREAM_ACCESS_FLAG_WRITE, NULL, &io_stream);
        MA_URL_REQUEST_IO_SET_WRITE_STREAM(url_request_io, io_stream);
    }
    else
    {
        error_code = ma_fstream_create(write_output_to_file, MA_STREAM_ACCESS_FLAG_APPEND, NULL, &io_stream);
        MA_URL_REQUEST_IO_SET_WRITE_STREAM(url_request_io, io_stream);
    }

    if(error_code != MA_OK)
    {
        MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "File Stream could not be created for file upload/download");
    }

    if(ma_url_request_set_io_callback(url_request, url_request_io) != MA_OK)
    {
        MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "IO Callbacks could not be set for the URL");
    }

    /**************************************************************************
    Display progress information or not
    **************************************************************************/
    if(show_progress_bar)
    {
        url_request_progress = (ma_url_request_progress_t *)calloc(1, sizeof(ma_url_request_progress_t));
        MA_URL_REQUEST_PROGRESS_INIT(url_request_progress);
        MA_URL_REQUEST_PROGRESS_SET_CB(url_request_progress, progress_callback);

        if(ma_url_request_set_progress_callback(url_request, url_request_progress) != MA_OK)
        {
            MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Progress Callbacks could not be set for the URL");
        }
    }

    /**************************************************************************
    Use username/password authentication or not.
    **************************************************************************/
    if(use_authentication)
    {
		ma_bool_t use_loggedon = MA_FALSE;

        url_authentication_info = (ma_url_request_auth_t *)calloc(1, sizeof(ma_url_request_auth_t));
        MA_URL_AUTH_INIT(url_authentication_info);
		
		MA_URL_AUTH_SET_LOCAL_AUTH_INFO(url_authentication_info, domainname, user, password, use_loggedon);

        if(ma_url_request_set_url_auth_info(url_request, url_authentication_info) != MA_OK)
        {
            MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Authentication information could not be set for the URL");
        }
    }

    /**************************************************************************
    Use SSL for secure communication
    **************************************************************************/
    if(ssl_enabled)
    {
        ssl_info = (ma_url_request_ssl_info_t *)calloc(1, sizeof(ma_url_request_ssl_info_t));
        MA_URL_SSL_INIT(ssl_info);
        MA_URL_AUTH_SET_SSL_INFO(ssl_info, certificate);

        if(ma_url_request_set_ssl_info(url_request, ssl_info) != MA_OK)
        {
            MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "SSL information could not be set for the URL");
        }
    }

    /**************************************************************************
    Download Partial data defined by the range information
    **************************************************************************/
    if(range_enabled)
    {
        range_info = (ma_url_request_range_t *)calloc(1, sizeof(ma_url_request_range_t));
        MA_URL_RANGE_INIT(range_info);
        MA_URL_RANGE_SET_RANGE_INFO(range_info, start_range, end_range);

        if(ma_url_request_set_range_info(url_request, range_info) != MA_OK)
        {
            MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Range Information could not be set");
        }
    }

    /**************************************************************************
    Configure Proxy servers by associating List of proxy servers to the URL request
    **************************************************************************/
    if(use_proxy && (ma_url_request_set_proxy_info(url_request, proxy_list) != MA_OK))
    {
        MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Proxy information could not be set.");
    }

    /**************************************************************************
    Send URL request.
    **************************************************************************/
    if(ma_url_request_set_connect_callback(url_request, request_connect_callback) != MA_OK)
    {
        MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Connection callback could not be associated with URL");
    }

    if(ma_url_request_send(url_request, request_complete_callback, NULL) != MA_OK)
    {
        MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Download request could not be sent");
    }
		

    /**************************************************************************
    Start the event loop.
    **************************************************************************/
    ma_event_loop_run(event_loop);

    /**************************************************************************
    Cleanup Code
    **************************************************************************/
    free(url_request_io);

    if(show_progress_bar)
    {
        free(url_request_progress);
    }

    if(use_authentication)
    {
        MA_URL_AUTH_DEINIT(url_authentication_info);
    }

    if(ssl_enabled)
    {
        MA_URL_SSL_DEINIT(ssl_info);
    }

    if(range_enabled)
    {
        free(range_info);
    }

    if(use_proxy && (ma_proxy_list_release(proxy_list) != MA_OK))
    {
        MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Proxy List could not be destroyed");
    }

    if(ma_url_request_release(url_request) != MA_OK)
    {
        MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "URL Request could not be released");
    }

    if(ma_stream_release(io_stream) != MA_OK)
    {
        MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "File IO Stream could not be released");
    }

    if(ma_net_client_service_stop(net_service) != MA_OK)
    {
        MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Network Client service could not be stopped");
    }

    if(ma_net_client_service_release(net_service) != MA_OK)
    {
        MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Network Client service could not be released");
    }

    /*
    if(ma_event_loop_stop(event_loop) != MA_OK)
    {
        MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Event Loop could not be stopped");
    }
    */
    if(ma_event_loop_release(event_loop) != MA_OK)
    {
       MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Event Loop could not be released");
    }

    if(ma_console_logger_release(console_logger) != MA_OK)
    {
        MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Console Logger could not be released");
    }

    return error_code;
}




