@echo off
cd ..\..\..\build\
call ma_env_vars.bat

if "%MA_BASE%"=="" goto error1
if "%MA_TOOLS%"=="" goto error2

echo %MA_BASE%
echo %MA_TOOLS%

cd ..\tools\network\network_library_test\
start network_lib_test.sln
goto end

:error1
echo Error: MA_BASE path not defined

:error2
echo Error: MA_TOOLS path not defined

:end
