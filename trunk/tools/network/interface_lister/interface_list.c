#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifdef WIN32
#include <Windows.h>
#include <WinSock.h>
#endif
//This is a test tool
#include <ma/internal/utils/network/ma_net_interface.h>
#include "ma/internal/utils/platform/ma_net_interface_list_internal.h"

void usage(char *exe_name) {
	printf("Usage:\t%s [-l [1/0]]\n",exe_name);
}

ma_bool_t process_args(int argc,char*argv[],ma_bool_t *include_loopback) {
	if(argc == 1) {
		usage(argv[0]);
		*include_loopback = MA_TRUE;
		return MA_TRUE;
	}
	if(argc > 3 || !include_loopback) {
		usage(argv[0]);
		return MA_FALSE;
	}
	else {
		if((strcmp(argv[1],"-l")!= 0 && strcmp(argv[1],"-L") !=0 ) &&
			(strcmp(argv[1],"-h") == 0 || strcmp(argv[1],"-H") == 0 ||
			strcmp(argv[1],"--help")==0 || strcmp(argv[1],"-?") == 0 )) {
			usage(argv[0]);
			return MA_FALSE;
		}
		if(argc == 2)
			*include_loopback= MA_TRUE;
		else
			*include_loopback = atoi(argv[2]);
		return MA_TRUE;
	}
}

int main(int argc, char * argv[]) {

	ma_error_t error = MA_OK;
	ma_net_interface_list_h list = NULL;
	ma_bool_t b_include_loopback = MA_FALSE;
#ifdef WIN32
	WSADATA wsadata;
	if(0 == WSAStartup(MAKEWORD(2,2), &wsadata)) 
#endif
	{
		ma_bool_t ret = process_args(argc,argv,&b_include_loopback);
		if(ret == MA_TRUE) {
			error = ma_net_interface_list_create(&list);
			if(error == MA_OK) {
				printf("Netinterface list created\n");
				error = ma_net_interface_list_scan(list,b_include_loopback);
				if(error == MA_OK) {
					ma_net_interface_h interf = NULL;
					ma_net_address_info_h address = NULL;
					printf("Scan successful.\nNumber of interfaces = %d\n",list->interfaces_count);
				
					MA_SLIST_FOREACH(list,interfaces,interf) {
						unsigned char mac_addr[MA_FORMATTED_MAC_ADDRESS_SIZE] = {0};
						printf("Interface index = %d\n",interf->index);
						printf("Interface name = %s\n",interf->interface_name);
						ma_format_mac_address(interf->mac_address, mac_addr);
						printf("Physical MAC address = %s\n", mac_addr);
						printf("Addresses present in the nic: %d\n",interf->MA_SLIST_COUNT(addresses));
						MA_SLIST_FOREACH(interf,addresses,address){
							printf("\n\tFAMILY: %d\n\tIP Address\t:\t%s",address->family,address->ip_addr);
							printf("\n\tNET_MASK\t:\t%s",address->subnet_mask);
							printf("\n\tNET_ADDR\t:\t%s",address->broadcast_addr);
							if(address->family==MA_IPFAMILY_IPV6)
								printf("\n\tScope ID : %d",address->ipv6_scope_id);
							printf("\n");
						
						}
						printf("_________________________\n");
					}
				}
				else
					printf("Scan unsuccessful %d\n",error);
			}
		}
		ma_net_interface_list_release(list);
#ifdef WIN32
		WSACleanup();
#endif
	}
	return 0;
}
