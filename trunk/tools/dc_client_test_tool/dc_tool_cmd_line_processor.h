#include "ma/ma_common.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/clients/datachannel/ma_datachannel_internal.h"
#ifdef __cplusplus
extern "C" {
#endif

#define DCTOOL_PARAM_UPLOAD							"-upload"
#define DCTOOL_PARAM_UPLOAD_ID						"-id"
#define DCTOOL_PARAM_UPLOAD_DATA					"-data"
#define DCTOOL_PARAM_UPLOAD_DATA_TEXT				"-text"
#define DCTOOL_PARAM_UPLOAD_DATA_FILE				"-file"
#define DCTOOL_PARAM_UPLOAD_DATA_CREATE				"-create"
#define DCTOOL_PARAM_UPLOAD_DATA_CREATE_SIZE		"-size"
#define DCTOOL_PARAM_UPLOAD_OPT_PERSIST				"-persist"
#define DCTOOL_PARAM_UPLOAD_OPT_ENCRYPT				"-encrypt"
#define DCTOOL_PARAM_UPLOAD_OPT_DELIVERY			"-notifydelivery"
#define DCTOOL_PARAM_UPLOAD_OPT_PURGE				"-notifypurge"
#define DCTOOL_PARAM_UPLOAD_OPT_CORRELATION			"-correlation"
#define DCTOOL_PARAM_UPLOAD_TTL						"-ttl"
#define DCTOOL_PARAM_UPLOAD_COUNT					"-count"
#define DCTOOL_PARAM_LOG							"-log"
#define DCTOOL_PARAM_LOG_VERBOSE					"-verbose"
#define DCTOOL_PARAM_LOG_FILE						"-filepath"
#define DCTOOL_PARAM_LISTEN							"-listen"
#define DCTOOL_PARAM_LISTEN_IDS						"-ids"

typedef  enum dc_tool_error_e {
	DCTOOL_ERROR_UNIDENTIFIED_ARGUMENT = -2,
	DCTOOL_ERROR_INVALIDARGUMENT = -1,
	DCTOOL_ERROR_OK = 0,
	DCTOOL_ERROR_UPLOAD_ID_INVALID,
	DCTOOL_ERROR_UPLOAD_COUNT_INVALID,
	DCTOOL_ERROR_UPLOAD_DATA_FILE_PATH_NOT_FOUND,
	DCTOOL_ERROR_UPLOAD_DATA_CREATE_FILESIZE_INCORRECT,
	DCTOOL_ERROR_UPLOAD_DATA_EMPTY,
	DCTOOL_ERROR_LOG_PATH_INVALID,
	DCTOOL_ERROR_LISTEN_IDS_MISSING,
	DCTOOL_ERROR_INVALID_UPLOAD_CORRELATIONID,
	DCTOOL_ERROR_INVALID_UPLOAD_COUNT,
	DCTOOL_ERROR_INVALID_UPLOAD_TTL,
	DCTOOL_ERROR_LOG_MISSING_VALUE
}dc_tool_error_t;

	
typedef struct dc_tool_data_context_s {
	ma_bool_t	b_use_path;
	ma_bool_t	b_create_file;
	char 		filepath[MA_MAX_BUFFER_LEN];
	size_t		file_size;
}dc_tool_data_context_t;

typedef struct dc_tool_upload_context_s {
	char 		upload_id[MA_DATACHANNEL_MAX_ID_LEN];
	ma_int32_t	upload_count;
	
	ma_bool_t	b_persist;
	ma_bool_t	b_encrypt;
	ma_bool_t	b_delivery;
	ma_bool_t	b_purge;

	ma_int32_t	correlation_id;
	ma_int32_t	ttl;
	dc_tool_data_context_t data;
}dc_tool_upload_context_t;

typedef struct dc_tool_log_context_s {
	ma_bool_t	b_log;
	ma_bool_t	b_verbose;
	char 		log_file[MA_MAX_BUFFER_LEN];

}dc_tool_log_context_t;

typedef struct dc_tool_listen_context_s {
	ma_bool_t	b_listen;
	char **		listen_ids;
	size_t		listen_ids_count;
}dc_tool_listen_context_t;


typedef struct dc_tool_context_s {
	ma_bool_t					b_upload;
	ma_bool_t					b_listen;
	ma_bool_t					b_log;
	dc_tool_upload_context_t	upload_context;
	char *						data;

	dc_tool_log_context_t		log_context;
	dc_tool_listen_context_t	listen_context;
	struct tm *timeinfo;
}dc_tool_cmd_processor_t;

ma_error_t dc_tool_context_create(dc_tool_cmd_processor_t **context);

ma_error_t dc_tool_context_release(dc_tool_cmd_processor_t *context);

dc_tool_error_t dc_tool_context_initialize(dc_tool_cmd_processor_t *context, int argc, char **argv);

ma_error_t dc_tool_context_print_usage(dc_tool_error_t error);

int ma_stricmp(const char *str1, const char *str2);

#ifdef __cplusplus
};
#endif
