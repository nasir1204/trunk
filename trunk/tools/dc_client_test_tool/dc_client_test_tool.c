// dc_client_test_tool.cpp : Defines the entry point for the console application.
//

#include "ma/ma_client.h"
#include "ma/logger/ma_file_logger.h"
#include "ma/logger/ma_console_logger.h"
#include "ma/ma_log.h"
#include "ma/logger/ma_log_filter.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/platform/ma_sys_apis.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define MA_DC_TEST_TOOL					"DCCLIENTTESTTOOL"
#define LOG_FACILITY_NAME				MA_DC_TEST_TOOL
#define MA_DC_TOOL_MAX_CMD_LINE_PARAMS	12
#define MA_LOG_FILTER					"%d:%+t %p %f.%s: %m"
#define DC_PRODUCT_ID					"MADCTESTTOOL"

#include "dc_tool_cmd_line_processor.h"
#include "ma/ma_dispatcher.h"
#include "ma/ma_client.h"

ma_bool_t g_bStop = MA_FALSE;


#ifdef WIN32
#include <Windows.h>
#include <direct.h>
BOOL WINAPI CtrlHandler(DWORD param) {
	switch(param)
	{
	case CTRL_C_EVENT:
	case CTRL_BREAK_EVENT:
	case CTRL_CLOSE_EVENT:
	case CTRL_LOGOFF_EVENT:
	case CTRL_SHUTDOWN_EVENT:
		{
			g_bStop = MA_TRUE;
			return TRUE;
		}
		break;
	default:
		break;
	}
	return FALSE;
}
#else
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#endif

ma_error_t sys_usleep(long usec) {
#if defined(MA_WINDOWS)
    Sleep(usec / 1000);
#elif defined(HAVE_NANOSLEEP)
    /* POSIX newer nanosleep(3) variant */
    struct timespec ts;
    ts.tv_sec  = 0;
    ts.tv_nsec = 1000 * usec;
    nanosleep(&ts, NULL);
#else
    /* POSIX older select(2) variant */
    struct timeval tv;
    tv.tv_sec  = 0;
    tv.tv_usec = usec;
    select(0, NULL, NULL, NULL, &tv);
#endif
    return MA_OK;
}

ma_logger_t  *console_logger = NULL;
ma_logger_t  *	file_logger = NULL;

void log_cb(ma_log_severity_t severity, const char *module_name, const char *log_message, void *user_data) {
	ma_log(console_logger, severity, module_name, __FILE__, __LINE__, __FUNCTION__, log_message); 
	ma_log(file_logger, severity, module_name, __FILE__, __LINE__, __FUNCTION__, log_message); 
}

#include "ma/datachannel/ma_datachannel.h"

void on_notify(ma_client_t *ma_client, const char *product_id, const char *message_id, ma_int64_t correlation_id, ma_datachannel_item_notification_t status, ma_error_t reason, void *cb_data) {
	switch(status) {
	case MA_DC_NOTIFICATION_DELIVERED:
		MA_LOG(console_logger, MA_LOG_SEV_INFO, "Received notification for successful delivery of item with correlation ID %lld for MessageId %s", correlation_id,message_id);
		MA_LOG(file_logger, MA_LOG_SEV_INFO, "Received notification for successful delivery of item with correlation ID %lld for MessageId %s", correlation_id,message_id);
		break;
	case MA_DC_NOTIFICATION_PURGED:
		MA_LOG(console_logger, MA_LOG_SEV_INFO, "Received notification for purge of item with correlation ID %lld to MessageId %s Reason : %d", correlation_id,message_id,reason);
		MA_LOG(file_logger, MA_LOG_SEV_INFO, "Received notification for purge of item with correlation ID %lld to MessageId %s Reason : %d", correlation_id,message_id,reason);
		break;
	case MA_DC_NOTIFICATION_NOSTATUS:
	default:
		MA_LOG(console_logger, MA_LOG_SEV_INFO, "NO STATUS %lld %s", correlation_id,message_id);
	}
}

void on_msg(ma_client_t *ma_client, const char *product_id, const char *message_id, ma_int64_t correlation_id, ma_datachannel_item_t *dc_message, void *cb_data)  {
	ma_variant_t *variant = NULL;
	ma_buffer_t *buffer = NULL;
	static int counter = 0;
	dc_tool_cmd_processor_t *context = (dc_tool_cmd_processor_t *) cb_data;
	MA_LOG(console_logger, MA_LOG_SEV_INFO, "Received Message from ePO - PRODUCT_ID: %s\tMESSAGE ID: %s\tCORRELATIONID %lld", product_id, message_id, correlation_id);
	MA_LOG(file_logger, MA_LOG_SEV_INFO, "Received Message from ePO - PRODUCT_ID: %s\tMESSAGE ID: %s\tCORRELATIONID %lld", product_id, message_id, correlation_id);
	if(MA_OK == ma_datachannel_item_get_payload(dc_message, &variant)) {
		if(MA_OK == ma_variant_get_raw_buffer(variant, &buffer)) {
			unsigned char *p = NULL;
			size_t size = 0;
			if(MA_OK == ma_buffer_get_raw(buffer, &p, &size)) {
				if(console_logger) {
					printf("\n\n%s\n\n", (char*) p);
				}
				if(file_logger) {
					char file[MA_MAX_BUFFER_LEN] = {0};
					char temp[1024] = {0};
					struct tm *timeinfo = context->timeinfo;
					MA_MSC_SELECT(_snprintf, snprintf)(temp, 1024, "%0.4d%0.2d%0.2d_%0.2d%0.2d%0.2d", 1900+timeinfo->tm_year, timeinfo->tm_mon +1 , timeinfo->tm_mday, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec);
					strcpy(file, context->log_context.log_file);
					strcat(file, MA_PATH_SEPARATOR_STR);
					strcat(file, temp);
					if(counter == 0) {
						MA_MSC_SELECT(_mkdir,mkdir)(file);
					}
					strcat(file, MA_PATH_SEPARATOR_STR);
					MA_MSC_SELECT(_snprintf, snprintf)(temp, 1024, "_%d_%lld.dump",counter++,correlation_id);
					strcat(file, temp);

					{
						FILE *fp = fopen(file, "w+");
						if(fp) {
							fwrite(p, sizeof(char), size, fp);
							fclose(fp);
						}
					}
				}
			}
		}
	}
}

int main(int argc, char* argv[]) {

	dc_tool_cmd_processor_t *context = NULL;
#ifdef MA_WINDOWS
	SetConsoleCtrlHandler(CtrlHandler, TRUE);
#endif
	if(MA_OK == dc_tool_context_create(&context)) {
		dc_tool_error_t rc = DCTOOL_ERROR_OK;
		if(DCTOOL_ERROR_OK == (rc = dc_tool_context_initialize(context, argc, argv))) {
			ma_client_t *client = NULL;
			if(MA_OK == ma_client_create(DC_PRODUCT_ID, &client)) {
				ma_client_set_logger_callback(client, log_cb, context);
				if(context->log_context.b_verbose) {
					ma_console_logger_create((ma_console_logger_t **) &console_logger);
				}
				if(context->log_context.log_file[0]) {
					ma_file_logger_create(context->log_context.log_file, "DataChannelTestTool", ".log", (ma_file_logger_t **) &file_logger);
				}


				if(MA_OK == ma_client_start(client)) { 
					ma_datachannel_register_message_handler_callback(client, DC_PRODUCT_ID, on_msg, context);
					ma_datachannel_register_notification_callback(client, DC_PRODUCT_ID, on_notify, context);


					if(context->b_listen) {
						size_t i = 0;
						for( ; i < context->listen_context.listen_ids_count; i++) {
							ma_datachannel_subscribe(client, DC_PRODUCT_ID, context->listen_context.listen_ids[i], MA_DC_SUBSCRIBER_LISTENER);
						}
					}

					
					if(context->b_upload) {
						ma_variant_t *var = NULL;
						if(MA_OK == ma_variant_create_from_raw(context->data, context->upload_context.data.file_size, &var)) {
							int count = context->upload_context.upload_count;
							int i = 0;
							for(i = 0 ; i < count ; i++ ) {
								ma_datachannel_item_t *item = NULL;
								if(MA_OK == ma_datachannel_item_create(context->upload_context.upload_id, var, &item)) {
									ma_datachannel_item_set_correlation_id(item, context->upload_context.correlation_id++);
									ma_datachannel_item_set_ttl(item, context->upload_context.ttl);
									ma_datachannel_item_set_option(item, MA_DATACHANNEL_PERSIST, context->upload_context.b_persist);
									ma_datachannel_item_set_option(item, MA_DATACHANNEL_ENCRYPT, context->upload_context.b_encrypt);
									ma_datachannel_item_set_option(item, MA_DATACHANNEL_DELIVERY_NOTIFICATION, context->upload_context.b_delivery);
									ma_datachannel_item_set_option(item, MA_DATACHANNEL_PURGED_NOTIFICATION, context->upload_context.b_purge);
									if(MA_OK == ma_datachannel_async_send(client, DC_PRODUCT_ID, item)) {
										MA_LOG(console_logger, MA_LOG_SEV_INFO,"Sent DC item successfully");
										MA_LOG(file_logger, MA_LOG_SEV_INFO,"Sent DC item successfully");
									}
									ma_datachannel_item_release(item);
								}
							}
						}
						else
							g_bStop = MA_TRUE;
					}

					printf("Press CTRL + C to exit\n");
					while(!g_bStop) {
						sys_usleep(1 * 1000);
					}

					
					if(context->b_listen) {
						size_t i = 0;
						for( ; i < context->listen_context.listen_ids_count; i++) {
							ma_datachannel_unsubscribe(client, DC_PRODUCT_ID, context->listen_context.listen_ids[i]);
						}
					}

					ma_datachannel_unregister_message_handler_callback(client , DC_PRODUCT_ID);
					ma_datachannel_unregister_notification_callback(client , DC_PRODUCT_ID);
					ma_client_stop(client);
				}
				if(console_logger)
					ma_logger_release(console_logger);
				if(file_logger)
					ma_logger_release(file_logger);
				ma_client_release(client);
			}

		}
		else {
			dc_tool_context_print_usage(rc);
		}
		dc_tool_context_release(context);
	}
	return 0;
}
