#include "ma/internal/utils/datastructures/ma_map.h"
#include "dc_tool_cmd_line_processor.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static dc_tool_error_t dc_tool_process_arguments(dc_tool_cmd_processor_t *context, int argc, char **argv);
static dc_tool_error_t dc_tool_process_listen_arguments(dc_tool_cmd_processor_t *context, int *i, int argc, char **argv);
static dc_tool_error_t dc_tool_process_log_arguments(dc_tool_cmd_processor_t *context, int *i, int argc, char **argv);
static dc_tool_error_t dc_tool_process_upload_arguments(dc_tool_cmd_processor_t *context, int *i, int argc, char **argv);



ma_error_t dc_tool_context_create(dc_tool_cmd_processor_t **context) {
	if(context) {
		dc_tool_cmd_processor_t *self = (dc_tool_cmd_processor_t *) calloc(1, sizeof(dc_tool_cmd_processor_t ));
		if(self) {
			time_t rawtime = time(&rawtime);
			self->timeinfo= localtime(&rawtime);
			self->upload_context.upload_count = -1;
			*context = self;
			return MA_OK;
		}
		else
			return MA_ERROR_OUTOFMEMORY;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t dc_tool_context_release(dc_tool_cmd_processor_t *self) {
	if(self) {
		int count = self->listen_context.listen_ids_count;
		if(self->data) {
			free(self->data);
			self->data = NULL;
		}
		
		if(count > 0) {
			while(count >=0) {
				free(self->listen_context.listen_ids[count]);
				count--;
			}
			free(self->listen_context.listen_ids);
		}
		free(self);
	}
	return MA_ERROR_INVALIDARG;
}


dc_tool_error_t dc_tool_context_initialize(dc_tool_cmd_processor_t *context, int argc, char **argv) {
	if(context && argc && argv) {
		dc_tool_error_t rc = dc_tool_process_arguments(context, argc, argv);
		if(DCTOOL_ERROR_OK == rc) {
			if(!context->upload_context.upload_id[0])
				return DCTOOL_ERROR_UPLOAD_ID_INVALID;
			if(!context->data)
				return DCTOOL_ERROR_UPLOAD_DATA_EMPTY;
			if(0 == context->upload_context.upload_count) 
				return DCTOOL_ERROR_UPLOAD_COUNT_INVALID;
			else if (-1 == context->upload_context.upload_count)
				context->upload_context.upload_count = 1;
			if(!context->log_context.log_file[0] && context->log_context.b_log)
				return DCTOOL_ERROR_LOG_PATH_INVALID;
			if(context->listen_context.b_listen && !context->listen_context.listen_ids_count)
				return DCTOOL_ERROR_LISTEN_IDS_MISSING;
			if(context->b_log && !(context->log_context.b_verbose || context->log_context.b_log)) 
				return DCTOOL_ERROR_LOG_MISSING_VALUE;
		}
		return rc;
	}
	return DCTOOL_ERROR_INVALIDARGUMENT;
}


dc_tool_error_t dc_tool_process_arguments(dc_tool_cmd_processor_t *context, int argc, char **argv) {
	dc_tool_error_t rc = DCTOOL_ERROR_OK;
	if(argc <= 1) 
		return DCTOOL_ERROR_INVALIDARGUMENT;

	{
		int i = 1;
		while(rc == DCTOOL_ERROR_OK && (i < argc)) {
			if( 0 == ma_stricmp(argv[i], DCTOOL_PARAM_UPLOAD)) {
				i++;
				context->b_upload = MA_TRUE;
				rc = dc_tool_process_upload_arguments(context, &i, argc, argv);
			}
			else if( 0 == ma_stricmp(argv[i], DCTOOL_PARAM_LISTEN)) {
				i++;
				context->b_listen = MA_TRUE;
				rc = dc_tool_process_listen_arguments(context, &i, argc, argv);
			}
			else if( 0 == ma_stricmp(argv[i], DCTOOL_PARAM_LOG)) {
				i++;
				context->b_log = MA_TRUE;
				rc = dc_tool_process_log_arguments(context, &i, argc, argv);
			}
			else {
				return DCTOOL_ERROR_INVALIDARGUMENT;
			}
			if(rc == DCTOOL_ERROR_UNIDENTIFIED_ARGUMENT) {
					rc = DCTOOL_ERROR_OK;
					i--;
			}
			i++;
		}
	}
	return rc;
}

dc_tool_error_t dc_tool_process_listen_arguments(dc_tool_cmd_processor_t *self, int *i, int argc, char **argv) {
	dc_tool_error_t rc = DCTOOL_ERROR_OK;
	while(rc == DCTOOL_ERROR_OK && (*i < argc)) {
		if( 0 == ma_stricmp(argv[*i], DCTOOL_PARAM_LISTEN_IDS)) {
			(*i)++;
			if(*i < argc) {
				char *ids = MA_MSC_SELECT(_strdup, strdup)(argv[*i]);
				char *token = strtok(ids, ",");
				size_t ids_count = 0;
				while(token) {
					ids_count++;
					token = strtok(NULL, ",");
				}
				free(ids);

				self->listen_context.listen_ids_count = ids_count;
				self->listen_context.listen_ids = (char **) calloc(ids_count, sizeof(char*));
				if(self->listen_context.listen_ids){
					int j = 0;
					token = strtok(argv[*i], ",");
					while(token) {
						self->listen_context.listen_ids[j] = MA_MSC_SELECT(_strdup, strdup)(token);
						j++;
						token = strtok(NULL, ",");
					}
				}
				(*i)++;
			}
			else
				return DCTOOL_ERROR_LISTEN_IDS_MISSING;
		}
		else
			return DCTOOL_ERROR_UNIDENTIFIED_ARGUMENT;
	}
	return rc;
}

dc_tool_error_t dc_tool_process_log_arguments(dc_tool_cmd_processor_t *self, int *i, int argc, char **argv) {
	dc_tool_error_t rc = DCTOOL_ERROR_OK;
	while(rc == DCTOOL_ERROR_OK && (*i < argc)) {
		if( 0 == ma_stricmp(argv[*i], DCTOOL_PARAM_LOG)) {
			(*i)++;
			self->log_context.b_log = MA_TRUE;
		}
		else if( 0 == ma_stricmp(argv[*i], DCTOOL_PARAM_LOG_VERBOSE)) {
			(*i)++;
			self->log_context.b_verbose = MA_TRUE;
		}
		else if( 0 == ma_stricmp(argv[*i], DCTOOL_PARAM_LOG_FILE)) {
			(*i)++;
			if(*i < argc)  {
				strcpy(self->log_context.log_file, argv[*i]);
				(*i)++;
			}
			else
				return DCTOOL_ERROR_LOG_PATH_INVALID;
		}
		else
			return DCTOOL_ERROR_UNIDENTIFIED_ARGUMENT;
	}
	return rc;
}

dc_tool_error_t dc_tool_process_upload_arguments(dc_tool_cmd_processor_t *context, int *i, int argc, char **argv) {
	dc_tool_error_t rc = DCTOOL_ERROR_OK;
	while(rc == DCTOOL_ERROR_OK && (*i < argc)) {
		if( 0 == ma_stricmp(argv[*i], DCTOOL_PARAM_UPLOAD_ID)) {
			(*i)++;
			if(*i < argc && argv[*i][0] == '-') {
				return DCTOOL_ERROR_UPLOAD_ID_INVALID;
			}
			else {
				strcpy( context->upload_context.upload_id, (argv[*i]) );
				(*i)++;
			}
		}
		else if( 0 == ma_stricmp(argv[*i], DCTOOL_PARAM_UPLOAD_DATA)) {
			(*i)++;
			if( *i < argc  && 0 == ma_stricmp(argv[*i], DCTOOL_PARAM_UPLOAD_DATA_TEXT)) {
				size_t len = 0;
				(*i)++;

				if( *i < argc && ((len = strlen(argv[*i])) > 0) ) {
					context->data = MA_MSC_SELECT(_strdup,strdup)(argv[*i]);
					context->upload_context.data.file_size = len;
					(*i)++;
				}
				else {
					return DCTOOL_ERROR_UPLOAD_DATA_EMPTY;
				}
			}
			else if( *i < argc  && 0 == ma_stricmp(argv[*i], DCTOOL_PARAM_UPLOAD_DATA_FILE)) {
				size_t len = 0;
				(*i)++;
				context->upload_context.data.b_use_path = MA_TRUE;
				if( *i < argc ) {
					if(((len = strlen(argv[*i])) > 0) ) {
						strcpy(context->upload_context.data.filepath, argv[*i]);
						(*i)++;
						if( *i < argc &&  0 == ma_stricmp(argv[*i], DCTOOL_PARAM_UPLOAD_DATA_CREATE)) {
							(*i)++;
							context->upload_context.data.b_create_file = MA_TRUE;
							if( *i < argc) { 
								if(0 == ma_stricmp(argv[*i], DCTOOL_PARAM_UPLOAD_DATA_CREATE_SIZE)) {
									(*i)++;
									if( *i < argc) { 
										context->upload_context.data.file_size = atol(argv[*i]);
										if(context->upload_context.data.file_size <= 0)
											return DCTOOL_ERROR_UPLOAD_DATA_CREATE_FILESIZE_INCORRECT;
										else
											(*i)++;
									}
									else
										return DCTOOL_ERROR_UPLOAD_DATA_CREATE_FILESIZE_INCORRECT;
								}
								else {
									return DCTOOL_ERROR_UPLOAD_DATA_CREATE_FILESIZE_INCORRECT;
								}
							}
							else
								return DCTOOL_ERROR_UPLOAD_DATA_CREATE_FILESIZE_INCORRECT;
						}
					}
				}
				else 
					return DCTOOL_ERROR_UPLOAD_DATA_FILE_PATH_NOT_FOUND;

				{
					FILE *fp = fopen(context->upload_context.data.filepath, context->upload_context.data.b_create_file? "w+":"r");

					if(context->upload_context.data.b_create_file) {
						context->data = (char *) calloc(context->upload_context.data.file_size, sizeof(char));
						memset(context->data, 49, context->upload_context.data.file_size);
						fwrite(context->data, sizeof(char), context->upload_context.data.file_size, fp);
					}
					else {
						size_t size = 0;
						fseek(fp, 0L, SEEK_END);
						size = ftell(fp);
						fseek(fp, 0L, SEEK_SET);
						context->data = (char *) calloc(size, sizeof(char));
						fread(context->data, sizeof(char), size, fp);
						if(size == 0) {
							fclose(fp);
							return DCTOOL_ERROR_UPLOAD_DATA_EMPTY;
						}
					}

					fclose(fp);

				}
			}
			else {
				return DCTOOL_ERROR_UPLOAD_DATA_EMPTY;
			}
		}
		else if( 0 == ma_stricmp(argv[*i], DCTOOL_PARAM_UPLOAD_OPT_PERSIST)) {
			(*i)++;
			context->upload_context.b_persist = MA_TRUE;
		}
		else if( 0 == ma_stricmp(argv[*i], DCTOOL_PARAM_UPLOAD_OPT_ENCRYPT)) {
			(*i)++;
			context->upload_context.b_encrypt = MA_TRUE;
		}
		else if( 0 == ma_stricmp(argv[*i], DCTOOL_PARAM_UPLOAD_OPT_DELIVERY)) {
			(*i)++;
			context->upload_context.b_delivery = MA_TRUE;
		}
		else if( 0 == ma_stricmp(argv[*i], DCTOOL_PARAM_UPLOAD_OPT_PURGE)) {
			(*i)++;
			context->upload_context.b_purge = MA_TRUE;
		}
		else if( 0 == ma_stricmp(argv[*i], DCTOOL_PARAM_UPLOAD_OPT_CORRELATION)) {
			(*i)++;
			if( *i < argc ) {
				context->upload_context.correlation_id = atoi(argv[*i]);
				(*i)++;
			}
			else {
				return DCTOOL_ERROR_INVALID_UPLOAD_CORRELATIONID;
			}
		}
		else if( 0 == ma_stricmp(argv[*i], DCTOOL_PARAM_UPLOAD_COUNT)) {
			(*i)++;
			if( *i < argc) {
				context->upload_context.upload_count = atoi(argv[*i]);
				(*i)++;
			}
			else 
				return DCTOOL_ERROR_INVALID_UPLOAD_COUNT;
		}
		else if( 0 == ma_stricmp(argv[*i], DCTOOL_PARAM_UPLOAD_TTL)) {
			(*i)++;
			if( *i < argc) {
				context->upload_context.ttl = atoi(argv[*i]);
				(*i)++;
			}
			else 
				return DCTOOL_ERROR_INVALID_UPLOAD_TTL;
		}
		else {
			return DCTOOL_ERROR_UNIDENTIFIED_ARGUMENT;
		}
	}
	return rc;
}

int ma_stricmp(const char *str1, const char *str2){
	return MA_MSC_SELECT(_stricmp, strcasecmp)(str1,str2);
}
//
//typedef  enum dc_tool_error_e {
//	DCTOOL_ERROR_UNIDENTIFIED_ARGUMENT = -2,
//	DCTOOL_ERROR_INVALIDARGUMENT = -1,
//	DCTOOL_ERROR_OK = 0,
//	DCTOOL_ERROR_UPLOAD_ID_INVALID,
//	DCTOOL_ERROR_UPLOAD_COUNT_INVALID,
//	DCTOOL_ERROR_UPLOAD_DATA_FILE_PATH_NOT_FOUND,
//	DCTOOL_ERROR_UPLOAD_DATA_CREATE_FILESIZE_INCORRECT,
//	DCTOOL_ERROR_UPLOAD_DATA_EMPTY,
//	DCTOOL_ERROR_LOG_PATH_INVALID,
//	DCTOOL_ERROR_LISTEN_IDS_MISSING,
//	DCTOOL_ERROR_INVALID_UPLOAD_CORRELATIONID,
//}dc_tool_error_t;
ma_error_t dc_tool_context_print_usage(dc_tool_error_t error) {
	printf("Error : %d\n", error);
	switch(error) {
	case DCTOOL_ERROR_UNIDENTIFIED_ARGUMENT:
	case DCTOOL_ERROR_INVALIDARGUMENT:
		{
			printf("Unidentified argument or invalid number of arguments\n");
		}
		break;
	case DCTOOL_ERROR_UPLOAD_ID_INVALID:
		{
			printf("Invalid upload id given to %s\n",DCTOOL_PARAM_UPLOAD_ID);
		}
		break;
	case DCTOOL_ERROR_UPLOAD_COUNT_INVALID:
		{
			printf("Invalid upload count given or missing for %s\n",DCTOOL_PARAM_UPLOAD_COUNT);
		}
		break;
	case DCTOOL_ERROR_UPLOAD_DATA_FILE_PATH_NOT_FOUND:
		{
			printf("invalid file path given for %s %s\n",DCTOOL_PARAM_UPLOAD, DCTOOL_PARAM_UPLOAD_DATA_FILE);
		}
		break;
	case DCTOOL_ERROR_UPLOAD_DATA_CREATE_FILESIZE_INCORRECT:
		{
			printf("Invalid create size given for %s %s %s %s %s\n", DCTOOL_PARAM_UPLOAD, DCTOOL_PARAM_UPLOAD_DATA, DCTOOL_PARAM_UPLOAD_DATA_FILE, DCTOOL_PARAM_UPLOAD_DATA_CREATE, DCTOOL_PARAM_UPLOAD_DATA_CREATE_SIZE);
		}
		break;
	case DCTOOL_ERROR_UPLOAD_DATA_EMPTY:
		{
			printf("No data was supplied or file not found or data read is empty\n");
		}
		break;
	case DCTOOL_ERROR_LOG_PATH_INVALID:
		{
			printf("The log path could not be found\n");
		}
		break;
	case DCTOOL_ERROR_LISTEN_IDS_MISSING:
		{
			printf("List of comma-seperated ids not supplied for %s %s\n",DCTOOL_PARAM_LISTEN, DCTOOL_PARAM_LISTEN_IDS);
		}
		break;
	case DCTOOL_ERROR_INVALID_UPLOAD_CORRELATIONID:
		{
			printf("Missing correlation id\n");
		}
		break;
	case DCTOOL_ERROR_INVALID_UPLOAD_TTL:
		{
			printf("Missing correlation TTL\n");
		}
		break;
	case DCTOOL_ERROR_LOG_MISSING_VALUE:
		{
			printf("Incorrect log arguments\n");
		}
		break;
	case DCTOOL_ERROR_OK:
		{
			printf("All arguments OK\n");
		}
		break;
	default:
		{
			printf("Unknown error code\n");
		}
		break;
	}
	if(error == DCTOOL_ERROR_OK) {
	}
	else {
		printf("DataChannelMsgbusTool \n"
					" -upload "
						" -id <DC_ID> \n"
						" -data [-text][-file]\n"
							"  -text <string>\n"
							"  -file <filepath>\n"
								"   -create\n"
								"    -size <size in bytes>\n"
						"  -persist\n"
						"  -encrypt\n"
						"  -notifydelivery\n"
						"  -notifypurge\n"
						"  -correlation <correlation id>\n"
						"  -ttl <time to live in seconds>\n"
						"  -count <number of times item is to be uploaded\n"
					" -log\n"
						"  -verbose\n"
						"  -filepath <path of location where dumps and logs should be generated\n"
					" -listen\n"
						"  -ids <list of ids to subscribe seperated by ,>\n");

		printf("\nUploading text: \ne.g.\n -upload -id \"DcTestEcho\" -data -text \"Hello, World\" -persist -encrypt -notifydelivery -correlation 12345 -ttl 120 -count 10 -log -verbose -filepath \"c:\\test\\data\" -listen -ids \"DcTestEcho,DcSimpleTest\"\n");
		printf("\nUploading file: \ne.g.\n -upload -id \"DcTestEcho\" -data -file \"c:\\test\\data\\data\" -create -size 1048576 -persist -encrypt -notifydelivery -correlation 12345 -ttl 120 -count 10 -log -verbose -filepath \"c:\\test\\data\" -listen -ids \"DcTestEcho,DcSimpleTest\"\n");
	}
	return MA_OK;
}