/** @file ma_log.h
 *  @brief Logger front end api's
 */

#ifndef MA_LOG_H_INCLUDED
#define MA_LOG_H_INCLUDED

#include "ma/ma_common.h"

MA_CPP(extern "C" {)

typedef struct ma_logger_s ma_logger_t, *ma_logger_h;

typedef enum ma_log_severity_e {
    MA_LOG_SEV_CRITICAL = 1,

    MA_LOG_SEV_ERROR,

    MA_LOG_SEV_WARNING,

    MA_LOG_SEV_NOTICE,

    MA_LOG_SEV_INFO,

    MA_LOG_SEV_DEBUG,

    MA_LOG_SEV_TRACE,

    MA_LOG_SEV_PERFORMANCE = MA_LOG_SEV_TRACE, /* alias */

    MA_LOG_SEV_MASK = 0xf,

    MA_LOG_FLAG_LOCALIZE = 0x10

} ma_log_severity_t;




/** @brief Logs message in the given logger
 *  @param [in] logger        logger handle
 *  @param [in] severity      severity level, possibly OR'ed with additional bit flags that tags the log message for various purposes
 *  @param [in] module        module name
 *  @param [in] file          source file name
 *  @param [in] func          function name
 *  @param [in] line          line number
 *  @param [in] fmt           format string
 */
MA_LOGGER_API ma_error_t ma_log(ma_logger_t    *logger,
                          int    severity,
                          const char           *module,
                          const char           *file,
                          int                  line,
                          const char           *func,
                          const char           *fmt, ...);

MA_LOGGER_API ma_error_t ma_wlog(ma_logger_t   *logger,
                          int    severity,
                          const char           *module,
                          const char           *file,
                          int                  line,
                          const char           *func,
                          const ma_wchar_t     *fmt, ...);



/** @brief macro to inject log message into given logger
 *  requires the client to define/declare a LOG_FACILITY_NAME identifier that evaluates to a narrow string - typically via a #define macro
 *
 */
#define MA_LOG(logger, severity, fmt, ...) \
    (void)(logger ? ma_log((logger), (severity), (LOG_FACILITY_NAME), __FILE__, __LINE__, __FUNCTION__, (fmt) , ## __VA_ARGS__) : MA_OK)

#define MA_WLOG(logger, severity, fmt, ...) \
    (void)(logger ? ma_wlog((logger), (severity), (LOG_FACILITY_NAME), __FILE__, __LINE__, __FUNCTION__, (fmt) , ## __VA_ARGS__) : MA_OK)

#include "ma/dispatcher/ma_logger_dispatcher.h"

MA_CPP(})

#endif /* MA_LOG_H_INCLUDED */
