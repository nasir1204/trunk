#ifndef MA_P2P_CONTENT_REQUEST_H_INCLUDED
#define MA_P2P_CONTENT_REQUEST_H_INCLUDED

#include "ma/ma_common.h"

MA_CPP(extern "C" {)

typedef struct ma_p2p_content_request_s ma_p2p_content_request_t, *ma_p2p_content_request_h ;

typedef enum ma_p2p_file_op_e {
	MA_P2P_FILE_OP_NONE = 0,	/* no operation, p2p will just hold the reference to the content */
    MA_P2P_FILE_OP_COPY,
    MA_P2P_FILE_OP_MOVE,
} ma_p2p_file_op_t ;


MA_P2P_API ma_error_t ma_p2p_content_file_request_create(const char *contributor, ma_p2p_file_op_t file_op, ma_p2p_content_request_t **request) ;

MA_P2P_API ma_error_t ma_p2p_content_buffer_request_create(const char *contributor, ma_p2p_content_request_t **request) ;

MA_P2P_API ma_error_t ma_p2p_content_request_add_file(ma_p2p_content_request_t *self, const char *hash, const char *base_path, const char *urn, char *content_type) ;

MA_P2P_API ma_error_t ma_p2p_content_request_add_buffer(ma_p2p_content_request_t *self, const char *hash, unsigned char *buffer, size_t buffer_size, const char *urn, char *content_type) ;

MA_P2P_API ma_error_t ma_p2p_content_request_release(ma_p2p_content_request_t *self) ;

MA_CPP(})

#endif /* MA_P2P_CONTENT_REQUEST_H_INCLUDED */