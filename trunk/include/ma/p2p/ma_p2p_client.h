#ifndef MA_P2P_CLIENT_H_INCLUDED
#define MA_P2P_CLIENT_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/p2p/ma_p2p_content_request.h"
#include "ma/internal/clients/udp/ma_udp_client.h"

#include "ma/ma_msgbus.h"
#include "ma/ma_log.h"

MA_CPP(extern "C" {)

typedef struct ma_p2p_client_s  ma_p2p_client_t, *ma_p2p_client_h ;

/* TBD : Do we need ma_client_t ??*/
MA_P2P_API ma_error_t ma_p2p_client_create(ma_udp_client_t *udp_client, ma_p2p_client_t **p2p_client) ;

MA_P2P_API ma_error_t ma_p2p_client_set_logger(ma_p2p_client_t *p2p_client, ma_logger_t *logger) ;

MA_P2P_API ma_error_t ma_p2p_client_set_msgbus(ma_p2p_client_t *p2p_client, ma_msgbus_t *msgbus) ;

MA_P2P_API ma_error_t ma_p2p_client_set_thread_option(ma_p2p_client_t *self, ma_msgbus_callback_thread_options_t thread_option) ;

MA_P2P_API ma_error_t ma_p2p_client_add_content(ma_p2p_client_t *self, ma_p2p_content_request_t *request) ;

MA_P2P_API ma_error_t ma_p2p_client_remove_content(ma_p2p_client_t *self, ma_p2p_content_request_t *request) ;

MA_P2P_API ma_error_t ma_p2p_client_get_content_filepath(ma_p2p_client_t *self, const char *hash, char **file_path) ;

MA_P2P_API ma_error_t ma_p2p_client_discover_content(ma_p2p_client_t *self, const char *multicast_addr, ma_uint32_t discovery_port, const char *hash, char **url) ;

MA_P2P_API ma_error_t ma_p2p_client_discover_service(ma_p2p_client_t *self, const char *multicast_addr, ma_uint32_t discovery_port, ma_bool_t *found) ;

MA_P2P_API ma_error_t ma_p2p_client_release(ma_p2p_client_t *self) ;

MA_CPP(})

#endif /* MA_P2P_CLIENT_H_INCLUDED */