#ifndef MA_SERVICE_H_INCLUDED
#define MA_SERVICE_H_INCLUDED

#include "ma/ma_common.h"

MA_CPP(extern "C" {)

typedef struct ma_service_s ma_service_t, *ma_service_h;

typedef struct ma_service_methods_s ma_service_methods_t;

struct ma_context_s;

/*!
 * current service specification version
 * This number is incremented when there is an ABI change between releases
 */
#define MA_SERVICE_VERSION 2

typedef enum ma_service_config_hints_e {
    /*! first time configuration */ 
    MA_SERVICE_CONFIG_NEW = 0x100,

    /*! one or more configuration settings have changed */
    MA_SERVICE_CONFIG_MODIFIED = 0x200,
} ma_service_config_hints_t;


/*!
 * Service states, free for implementors to use for state management, not currently used
 * at the API level
 */
typedef enum ma_service_state_e {

    MA_SERVICE_STOPPED,

    MA_SERVICE_STARTING,

    MA_SERVICE_STARTED,

    MA_SERVICE_STOPPING

} ma_service_state_t;

/*!
 * EXPERIMENTAL
 * candidate prototype for a general service creator function
 * @param[in] service_name the name of the service requested
 * @param[out] service address where the new object will be stored
 * @return MA_OK on successful object creation, otherwise an error (doh!)
 */
typedef ma_error_t (*ma_service_create_fn)(char const *service_name, ma_service_t **service);

struct ma_service_methods_s {

    /*!
     * configure service using interfaces from the supplied context
     * @param[in] service for which settings are to be applied
     * @param[in] supplied context
     * @param[in] hint a flag
     * This method can be NULL
     */
    ma_error_t (*configure)(ma_service_t *service, struct ma_context_s *context, unsigned hint);

    /*!
     * start service, engage with message bus or event loop or whatever
     */
    ma_error_t (*start)(ma_service_t *service);

    /*!
     * stop service, disengage from message bus and/or loop
     */
    ma_error_t (*stop)(ma_service_t *service);

    /*!
     * destructor, please release all resources
     */
    void (*destroy)(ma_service_t *service);
};

struct ma_service_s {
    /*!
     * vtable methods
     */
    ma_service_methods_t            const *methods;

    /*!
     * version (for backwards compatibility)
     */
    ma_uint16_t                     version;

    /*!
     * some data available for derived classes to use. Opaque to the owning service controller 
     */
    void                            *data;
};

/*!
 * configures the common members of a service. Typically derived/implementation classes use this to set up the service structure
 */
MA_STATIC_INLINE void ma_service_init(ma_service_t *service, ma_service_methods_t const *methods, char const *service_name, void *data) {
    if (service) {
        service->methods = methods;
        service->version = MA_SERVICE_VERSION;
        //(service_name);
        service->data = data;
    }
}

/*!
 * configures the service using the information from the supplied context
 * the service in turn grabs whatever information is relevant for its running. The service can only hope that whatever it needs has already been added to the context!
 */
MA_STATIC_INLINE ma_error_t ma_service_configure(ma_service_t *service, struct ma_context_s *context, unsigned hint) {
    return (service && service->methods)
        ? (service->methods->configure) ? service->methods->configure(service, context, hint) : MA_OK
        : MA_ERROR_INVALIDARG;
}

/*!
 * starts that service
 */
MA_STATIC_INLINE ma_error_t ma_service_start(ma_service_t *service) {
    return (service && service->methods)
        ? service->methods->start(service)
        : MA_ERROR_INVALIDARG;
}

/*!
 * stops that service
 */
MA_STATIC_INLINE ma_error_t ma_service_stop(ma_service_t *service) {
    return (service && service->methods)
        ? service->methods->stop(service)
        : MA_ERROR_INVALIDARG;
}

/*!
 * releases the memory and other resources associated with the given service
 * Note, the service must be in a non started state
 */
MA_STATIC_INLINE void ma_service_release(ma_service_t *service) {
    if (service && service->methods && service->methods->destroy){
        service->methods->destroy(service);
    }
}


MA_CPP(})

#endif /* MA_SERVICE_H_INCLUDED */
