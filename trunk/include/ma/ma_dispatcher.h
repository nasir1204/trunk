/** @file ma_dispatcher.h
 *  @brief Dispatcher prototypes 
 *  
 */

#ifndef MA_DISPATCHER_H_INCLUDED
#define MA_DISPATCHER_H_INCLUDED

#include "ma/ma_common.h"

MA_CPP(extern "C" {)
    
/** @brief dispatcher handle
 */
typedef struct ma_dispatcher_s ma_dispatcher_t, *ma_dispatcher_h;

/** @brief Initializes dispatcher.  
 *  @param [in]  default_rsdk_path		default rsdk path (NULLABLE)	
 *  @param [out] handle					dispatcher handle	
 *  @return								MA_OK on successful initialization
 *										MA_ERROR_OUTOFMEMORY on memory allocation failure
 *										MA_ERROR_DISPATCHER_INIT_FAILED on initialization failure
 */
MA_DISPATCHER_API ma_error_t ma_dispatcher_initialize(const char *default_rsdk_path, ma_dispatcher_t **dispatcher_handle);

/** @brief Deinitializes dispatcher
 *  @param [in] handle		dispatcher handle	
 *  @return					MA_OK on successful deinitialization
 *                          MA_ERROR_INVALIDARG on invalid arguments passed
 *                          MA_ERROR_DISPATCHER_DEINIT_FAILED on deinitialization failure
 */
MA_DISPATCHER_API ma_error_t ma_dispatcher_deinitialize(ma_dispatcher_t *dispatcher_handle);

MA_CPP(})
#endif // MA_DISPATCHER_H_INCLUDED


