#ifndef MA_SELFPROTECT_H_INCLUDED
#define MA_SELFPROTECT_H_INCLUDED

#include "ma/ma_common.h"

MA_C_SCOPE_BEGIN

#define MA_SELFPROTECT_ENABLE MA_TRUE

#define MA_SELFPROTECT_DISABLE MA_FALSE

#define MA_SELFPROTECT_PRODUCT_MA 0x1

#define MA_SELFPROTECT_PRODUCT_SYSCORE 0x2



/**
 * Enables or disables self protection for 0 or more products
 *   The following products are currently supported
 *   MA, Syscore
 
 * @param products Combination (OR'ed together) of MA_SELFPROTECT_PRODUCT_xxx that should be enabled or disabled
 * @param enable Specifies whether specified products should be enabled or disabled
 
 * @return MA_OK on success, otherwise a failure code
 */ 
MA_SELFPROTECT_API ma_error_t ma_selfprotect_enable(ma_uint32_t products, ma_bool_t enable);

/**
 * Retrieves the products for which self protection is enabled.
 *
 * @param products Pointer to variable that receives the list of product where self protection is enabled. The corresponding bits will be lit
 *
 * @return MA_OK on success, otherwise a failure code
 */
MA_SELFPROTECT_API ma_error_t ma_selfprotect_query_status(ma_uint32_t *products);
	
MA_C_SCOPE_END

#include "ma/dispatcher/ma_selfprotect_dispatcher.h"

#endif /* MA_SELFPROTECT_H_INCLUDED */
