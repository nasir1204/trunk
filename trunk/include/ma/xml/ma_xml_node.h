/** @file ma_xml_node.h
 *  @brief XML node interfaces
 *
*/
#ifndef MA_XML_NODE_H_INCLUDED
#define MA_XML_NODE_H_INCLUDED

#include "ma/ma_common.h"

/**
 * Represents a xml node(tag), which MUST have name , optional attributes and data
 */

MA_CPP(extern "C" {)

typedef struct ma_xml_node_s ma_xml_node_t, *ma_xml_node_h;

/**
 * create a node with the mentioned name and parent
 * @parent parent node must
 * @name  name of the node
 * @node node pointer to hold newly created node object
 * 
 */
MA_XML_API ma_error_t ma_xml_node_create(ma_xml_node_t *parent, const char *name, ma_xml_node_t **node);

/**
 * release node object menory
 * @node node object
 * 
 */
MA_XML_API ma_error_t ma_xml_node_release(ma_xml_node_t *node);

/**
 * delete's the node and subtree from the xml tree
 * @node node object
 * NOTE: It doesnt releases the memory, call ma_xml_node_release to release the memory
 */
MA_XML_API ma_error_t ma_xml_node_delete(ma_xml_node_t *node);

/**
 * search the node with passed node name
 * @node node object
 * @name name of the node to be searched
 * 
 */
MA_XML_API ma_xml_node_t *ma_xml_node_search(ma_xml_node_t *node, const char *name);

/**
 * search the node with path(e.g. SiteList/SpipeSite)
 * @node node object
 * @path path of the node to be searched
 * 
 */
MA_XML_API ma_xml_node_t *ma_xml_node_search_by_path(ma_xml_node_t *node, const char *path);

/**
 * sets the node name, replaces the old one
 * @node node object
 * @name name to be set
 * 
 */
MA_XML_API ma_error_t ma_xml_node_set_name(ma_xml_node_t *node, const char *name);

/**
 * sets the node data(makes it a leaf node in the tree)
 * @node node object
 * @data data to be set
 * 
 */
MA_XML_API ma_error_t ma_xml_node_set_data(ma_xml_node_t *node, const char *data);

/**
 * returns node name
 * @node node object
 * 
 */
MA_XML_API const char *ma_xml_node_get_name(ma_xml_node_t *node);

/**
 * returns node has data or not(MA_TRUE or MA_FALSE)
 * @node node object
 * 
 */
MA_XML_API ma_bool_t   ma_xml_node_has_data(ma_xml_node_t *node);

/**
 * returns node data assocaited with it if it have one
 * @node node object
 * 
 */
MA_XML_API const char *ma_xml_node_get_data(ma_xml_node_t *node);

/**
 * add attribute with name and value
 * @node node object
 * @name attribute name
 * @value attribute value
 * 
 */
MA_XML_API ma_error_t ma_xml_node_attribute_set(ma_xml_node_t *node, const char *name, const char *value);

/**
 * delete's attribute with provide name
 * @node node object
 * @name attribute name
 * 
 */
MA_XML_API ma_error_t ma_xml_node_attribute_del(ma_xml_node_t *node, const char *name);

/**
 * returns node have attribute with the name(MA_TRUE or MA_FALSE)
 * @node node object
 * @name attribute name
 * 
 */
MA_XML_API ma_bool_t  ma_xml_node_attribute_has(ma_xml_node_t *node, const char *name);

/**
 * returns attribute value with name provided if it have one
 * @node node object
 * @name attribute name
 * 
 */
MA_XML_API const char *ma_xml_node_attribute_get(ma_xml_node_t *node, const char *name);

/**
 * returns parent node
 * @node node object
 * 
 */
MA_XML_API ma_xml_node_t *ma_xml_node_get_parent(ma_xml_node_t *node);

/**
 * returns child node if it has one
 * @node node object
 * 
 */
MA_XML_API ma_xml_node_t *ma_xml_node_get_child(ma_xml_node_t *node);

/**
 * returns next logical node or NULL
 * @node node object
 * 
 */
MA_XML_API ma_xml_node_t *ma_xml_node_get_next(ma_xml_node_t *node);

/**
 * returns previous logical node or NULL
 * @node node object
 * 
 */
MA_XML_API ma_xml_node_t *ma_xml_node_get_prev(ma_xml_node_t *node);

/**
 * returns next sibling node or NULL
 * @node node object
 * 
 */
MA_XML_API ma_xml_node_t *ma_xml_node_get_next_sibling(ma_xml_node_t *self);

/**
 * returns previous sibling node or NULL
 * @node node object
 * 
 */
MA_XML_API ma_xml_node_t *ma_xml_node_get_prev_sibling(ma_xml_node_t *self);

/**
 * returns first child of the node if it has one
 * @node node object
 * 
 */
MA_XML_API ma_xml_node_t *ma_xml_node_get_first_child(ma_xml_node_t *node);

/**
 * returns last child of the node if it has one
 * @node node object
 * 
 */
MA_XML_API ma_xml_node_t *ma_xml_node_get_last_child(ma_xml_node_t *node);

/**
 * returns the number of child elements present just below this node. nested xml nodes to the children will be ignored
 * @node node object
 *
 */
MA_XML_API ma_int32_t ma_xml_node_get_child_count(ma_xml_node_t *node);


/**
 * returns a child node with the specified input name
 * @node node object
 * @name name of the node need to be found
 */
MA_XML_API ma_xml_node_t *ma_xml_node_get_child_by_name(ma_xml_node_t *node, const char *name) ;

MA_CPP(})

#endif //MA_XML_NODE_H_INCLUDED

