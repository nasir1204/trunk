#ifndef MA_SERVICE_FLAG_H_INCLUDED
#define MA_SERVICE_FLAG_H_INCLUDED

#include "ma/ma_common.h"

#define MA_SERVICE_NONE					((ma_uint64_t)0)
#define MA_SERVICE_BASE					((ma_uint64_t)1)

#define MA_SERVICE_MSGBUS				(MA_SERVICE_BASE<<0)
#define MA_SERVICE_PROPERTY				(MA_SERVICE_BASE<<1)
#define MA_SERVICE_POLICY				(MA_SERVICE_BASE<<2)
#define MA_SERVICE_EVENT				(MA_SERVICE_BASE<<3)
#define MA_SERVICE_DC					(MA_SERVICE_BASE<<4)
#define MA_SERVICE_UPDATER				(MA_SERVICE_BASE<<5)
#define MA_SERVICE_SCHEDULER			(MA_SERVICE_BASE<<6)
#define MA_SERVICE_REPOSITORY			(MA_SERVICE_BASE<<7)
#define MA_SERVICE_CRYPTO				(MA_SERVICE_BASE<<8)
#define MA_SERVICE_INFO					(MA_SERVICE_BASE<<9)
#define MA_SERVICE_ALL					( ~(MA_SERVICE_NONE) )

/*Point Product writes the "ServiceRequired" in their plugin area.*/
#define MA_SERVICE_REQUIRED_FLAGS_INT	"ServiceRequired"

#endif /*MA_SERVICE_FLAG_H_INCLUDED*/