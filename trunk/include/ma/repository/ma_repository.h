/** @file ma_repository.h
 *  @brief Repository interfaces
 *
*/
#ifndef MA_REPOSITORY_H_INCLUDED
#define MA_REPOSITORY_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/proxy/ma_proxy_config.h"

MA_CPP(extern "C" {)


typedef struct ma_repository_s ma_repository_t, *ma_repository_h ;

/**
*  repository url type
*/
typedef enum ma_repository_url_type_e {
    MA_REPOSITORY_URL_TYPE_HTTP = 0,
    MA_REPOSITORY_URL_TYPE_FTP,
    MA_REPOSITORY_URL_TYPE_UNC,
    MA_REPOSITORY_URL_TYPE_LOCAL,
	MA_REPOSITORY_URL_TYPE_SPIPE,
	MA_REPOSITORY_URL_TYPE_SA,
	MA_REPOSITORY_URL_TYPE_END
} ma_repository_url_type_t ;

/**
*  repository type
*/
typedef enum ma_repository_type_e {
    MA_REPOSITORY_TYPE_REPO = 0,
    MA_REPOSITORY_TYPE_MASTER,
    MA_REPOSITORY_TYPE_FALLBACK,
	MA_REPOSITORY_TYPE_END
} ma_repository_type_t ;

/**
*  repository namespace
*/
typedef enum ma_repository_namespace_e {
    MA_REPOSITORY_NAMESPACE_GLOBAL = 0,
    MA_REPOSITORY_NAMESPACE_LOCAL,
	MA_REPOSITORY_NAMESPACE_END
} ma_repository_namespace_t ;

/**
*  repository authentication type
*/
typedef enum ma_repository_auth_e {
    MA_REPOSITORY_AUTH_NONE = 0,
    MA_REPOSITORY_AUTH_ANONYMOUS,
	MA_REPOSITORY_AUTH_LOGGEDON_USER,
	MA_REPOSITORY_AUTH_CREDENTIALS,
	MA_REPOSITORY_AUTH_END
} ma_repository_auth_t;

/**
*  repository state
*/
typedef enum ma_repository_state_e {
	MA_REPOSITORY_STATE_DEFAULT = 0,
    MA_REPOSITORY_STATE_ADDED,
    MA_REPOSITORY_STATE_REMOVED,
	MA_REPOSITORY_STATE_ALTERED,
	MA_REPOSITORY_STATE_END
} ma_repository_state_t;

/** 
 * @brief creates repository object
 * @param repository               [out]     pointer containing repository object.
 * @result                                   MA_OK on success.
 *                                           MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_REPOSITORY_API ma_error_t   ma_repository_create(ma_repository_t **repository);

/** 
 * @brief increment repository object reference
 * @param repository                [in]	repository object whose reference to be incremented.
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_REPOSITORY_API ma_error_t   ma_repository_add_ref(ma_repository_t *repository);

/** 
 * @brief release repository object
 * @param repository                [in]	repository object to be release.
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_REPOSITORY_API ma_error_t   ma_repository_release(ma_repository_t *repository);

/** 
 * @brief set type in repository object
 * @param repository               [in]		repository object pointer in which type to be set.
 * @param type				       [in]		repository type .
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_REPOSITORY_API ma_error_t   ma_repository_set_type(ma_repository_t *repository, ma_repository_type_t type);

/** 
 * @brief set state in repository object
 * @param repository               [in]		repository object pointer in which state to be set.
 * @param state				       [in]		repository state .
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_REPOSITORY_API ma_error_t   ma_repository_set_state(ma_repository_t *repository, ma_repository_state_t state);

/** 
 * @brief set url type in repository object
 * @param repository               [in]		repository object pointer in which urltype to be set.
 * @param url_type			       [in]		repository url type .
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_REPOSITORY_API ma_error_t   ma_repository_set_url_type(ma_repository_t *repository, ma_repository_url_type_t url_type);

/** 
 * @brief set repository name
 * @param repository               [in]		repository object pointer in which name to be set.
 * @param name				       [in]		repository name .
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_REPOSITORY_API ma_error_t   ma_repository_set_name(ma_repository_t *repository, const char *name);

/** 
 * @brief set repository fqdn
 * @param repository               [in]		repository object pointer in which server fqdn to be set.
 * @param server_fqdn		       [in]		server fqdn .
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_REPOSITORY_API ma_error_t   ma_repository_set_server_fqdn(ma_repository_t *repository, const char *server_fqdn);

/** 
 * @brief set repository server name
 * @param repository               [in]		repository object pointer in which server name to be set.
 * @param server_name		       [in]		server name .
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_REPOSITORY_API ma_error_t   ma_repository_set_server_name(ma_repository_t *repository, const char *server_name);

/** 
 * @brief set repository server ip
 * @param repository               [in]		repository object pointer in which server ip to be set.
 * @param server_ip			       [in]		server  ip .
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark									This api is appilcable to spipe and sa only
 *
 */
MA_REPOSITORY_API ma_error_t   ma_repository_set_server_ip(ma_repository_t *repository, const char *server_ip); 

/** 
 * @brief set repository server path
 * @param repository               [in]		repository object pointer in which server repo path to be set.
 * @param server_path		       [in]		server repo path .
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_REPOSITORY_API ma_error_t   ma_repository_set_server_path(ma_repository_t *repository, const char *server_path);

/** 
 * @brief enable/disable repository 
 * @param repository               [in]		repository object pointer which is enable/disable.
 * @param is_enabled		       [in]		1:enable/0:disable .
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_REPOSITORY_API ma_error_t   ma_repository_set_enabled(ma_repository_t *repository, ma_bool_t is_enabled);

/** 
 * @brief set repository authentication type.
 * @param repository               [in]		repository object pointer in which authentication type to be set.
 * @param type				       [in]		type .
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_REPOSITORY_API ma_error_t   ma_repository_set_authentication(ma_repository_t *repository, ma_repository_auth_t type);

/** 
 * @brief set repository namespace
 * @param repository               [in]		repository object pointer in which namespace to be set.
 * @param repo_namespace	       [in]		namespace .
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_REPOSITORY_API ma_error_t   ma_repository_set_namespace(ma_repository_t *repository, ma_repository_namespace_t repo_namespace);

/** 
 * @brief set proxy usage type in repository.
 * @param repository               [in]		repository object pointer in which proxy usage type to be set.
 * @param proxy_usage		       [in]		namespace proxy usage type.
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_REPOSITORY_API ma_error_t   ma_repository_set_proxy_usage_type(ma_repository_t *repository, ma_proxy_usage_type_t proxy_usage);

/** 
 * @brief set port number in repository.
 * @param repository               [in]		repository object pointer in which port number to be set.
 * @param port				       [in]		port number.
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark									used only for http and ftp repository       
 *
 */
MA_REPOSITORY_API ma_error_t   ma_repository_set_port(ma_repository_t *repository, ma_uint32_t port);

/** 
 * @brief set ssl port number in repository.
 * @param repository               [in]		repository object pointer in which ssl port number to be set.
 * @param port				       [in]		ssl port number.
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark									used only for spipe and sa repository       
 *
 */
MA_REPOSITORY_API ma_error_t   ma_repository_set_ssl_port(ma_repository_t *repository, ma_uint32_t port);

/** 
 * @brief set user name for repository.
 * @param repository               [in]		repository object pointer in which user name to be set.
 * @param user_name			       [in]		user name.
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark									
 *
 */
MA_REPOSITORY_API ma_error_t   ma_repository_set_user_name(ma_repository_t *repository, const char *user_name);

/** 
 * @brief set domain name for repository.
 * @param repository               [in]		repository object pointer in which domain name to be set.
 * @param domain_name		       [in]		domain name.
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark									applicable to unc and local type only
 *
 */
MA_REPOSITORY_API ma_error_t   ma_repository_set_domain_name(ma_repository_t *repository, const char *domain_name);

/** 
 * @brief set password for repository.
 * @param repository               [in]		repository object pointer in which domain name to be set.
 * @param password			       [in]		password.
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark									
 *
 */
MA_REPOSITORY_API ma_error_t   ma_repository_set_password(ma_repository_t *repository, const char *password);

/** 
 * @brief set ping time for repository.
 * @param repository               [in]		repository object pointer in which pingtime to be set.
 * @param pingtime			       [in]		ping time.
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark									
 *
 */
MA_REPOSITORY_API ma_error_t   ma_repository_set_pingtime(ma_repository_t *repository, ma_uint32_t pingtime);

/** 
 * @brief set subnet distance for repository.
 * @param repository               [in]		repository object pointer in which subnet distance to be set.
 * @param subnetdistance	       [in]		subnet distance.
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark									
 *
 */
MA_REPOSITORY_API ma_error_t   ma_repository_set_subnetdistance(ma_repository_t *repository, ma_uint32_t subnetdistance);

/** 
 * @brief set site order for repository.
 * @param repository               [in]		repository object pointer in which repository order to be set.
 * @param siteorder			       [in]		repository order.
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark									
 *
 */
MA_REPOSITORY_API ma_error_t   ma_repository_set_siteorder(ma_repository_t *repository, ma_uint32_t siteorder);

/** 
 * @brief get name of repository.
 * @param repository               [in]		repository object pointer from which name to be get.
 * @param name				       [out]	repository name.
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark									
 *
 */
MA_REPOSITORY_API ma_error_t   ma_repository_get_name(ma_repository_t *repository, const char **name);

/** 
 * @brief get type of repository.
 * @param repository               [in]		repository object pointer from which type to be get.
 * @param type				       [out]	repository type.
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark									
 *
 */
MA_REPOSITORY_API ma_error_t   ma_repository_get_type(ma_repository_t *repository, ma_repository_type_t *type);

/** 
 * @brief get url type of repository.
 * @param repository               [in]		repository object pointer from which url type to be get.
 * @param url type			       [out]	repository url type.
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark									
 *
 */
MA_REPOSITORY_API ma_error_t   ma_repository_get_url_type(ma_repository_t *repository, ma_repository_url_type_t *url_type);

/** 
 * @brief get namespace of repository.
 * @param repository               [in]		repository object pointer from which namespace to be get.
 * @param repo_namespace			       [out]	repository namespace.
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark									
 *
 */
MA_REPOSITORY_API ma_error_t   ma_repository_get_namespace(ma_repository_t *repository, ma_repository_namespace_t *repo_namespace);

/** 
 * @brief get authentication type of repository.
 * @param repository               [in]		repository object pointer from which authentication type to be get.
 * @param type				       [out]	repository authentication type.
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark									
 *
 */
MA_REPOSITORY_API ma_error_t   ma_repository_get_authentication(ma_repository_t *repository, ma_repository_auth_t *type);

/** 
 * @brief get proxy usage type of repository.
 * @param repository               [in]		repository object pointer from which proxy usage type to be get.
 * @param proxy_usage		       [out]	proxy usage type of repository.
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark									
 *
 */
MA_REPOSITORY_API ma_error_t   ma_repository_get_proxy_usage_type(ma_repository_t *repository, ma_proxy_usage_type_t *proxy_usage);

/** 
 * @brief get server fqdn of repository.
 * @param repository               [in]		repository object pointer from which server fqdn to be get.
 * @param server_fqdn		       [out]	server fqdn.
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark									
 *
 */
MA_REPOSITORY_API ma_error_t   ma_repository_get_server_fqdn(ma_repository_t *repository, const char **server_fqdn);

/** 
 * @brief get server name from repository.
 * @param repository               [in]		repository object pointer from which server name to be get.
 * @param server_name		       [out]	server name.
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark									
 *
 */
MA_REPOSITORY_API ma_error_t   ma_repository_get_server_name(ma_repository_t *repository, const char **server_name);

/** 
 * @brief get server path from repository.
 * @param repository               [in]		repository object pointer from which server path to be get.
 * @param server_path		       [out]	server path.
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark									
 *
 */
MA_REPOSITORY_API ma_error_t   ma_repository_get_server_path(ma_repository_t *repository, const char **server_path);

/** 
 * @brief get port from repository.
 * @param repository               [in]		repository object pointer from which port to be get.
 * @param port				       [out]	port number.
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark									
 *
 */
MA_REPOSITORY_API ma_error_t   ma_repository_get_port(ma_repository_t *repository, ma_uint32_t *port);

/** 
 * @brief get ssl port from repository.
 * @param repository               [in]		repository object pointer from which ssl port to be get.
 * @param port				       [out]	ssl port number.
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark									
 *
 */
MA_REPOSITORY_API ma_error_t   ma_repository_get_ssl_port(ma_repository_t *repository, ma_uint32_t *port);

/** 
 * @brief get repository state(enable/disable).
 * @param repository               [in]		repository object pointer from which state to be get.
 * @param is_enabled		       [out]	state.
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark									
 *
 */
MA_REPOSITORY_API ma_error_t   ma_repository_get_enabled(ma_repository_t *repository, ma_bool_t *is_enabled);

/** 
 * @brief get user name of repository.
 * @param repository               [in]		repository object pointer from which user name to be get.
 * @param user_name				   [out]	user name.
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark									
 *
 */
MA_REPOSITORY_API ma_error_t   ma_repository_get_user_name(ma_repository_t *repository, const char **user_name);

/** 
 * @brief get domain name of repository.
 * @param repository               [in]		repository object pointer from which domain name to be get.
 * @param domain_name			   [out]	domain name.
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark									
 *
 */
MA_REPOSITORY_API ma_error_t   ma_repository_get_domain_name(ma_repository_t *repository, const char **domain_name);

/** 
 * @brief get password of repository.
 * @param repository               [in]		repository object pointer from which domain to be get.
 * @param password				   [out]	password.
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark									
 *
 */
MA_REPOSITORY_API ma_error_t   ma_repository_get_password(ma_repository_t *repository, const char **password);

/** 
 * @brief get pingtime of repository.
 * @param repository               [in]		repository object pointer from which pingtime to be get.
 * @param pingtime				   [out]	pingtime.
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark									
 *
 */
MA_REPOSITORY_API ma_error_t   ma_repository_get_pingtime(ma_repository_t *repository, ma_uint32_t *pingtime);

/** 
 * @brief get subnetdistance of repository.
 * @param repository               [in]		repository object pointer from which subnetdistance to be get.
 * @param subnetdistance		   [out]	subnetdistance.
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark									
 *
 */
MA_REPOSITORY_API ma_error_t   ma_repository_get_subnetdistance(ma_repository_t *repository, ma_uint32_t *subnetdistance);

/** 
 * @brief get siteorder of repository.
 * @param repository               [in]		repository object pointer from which siteorder to be get.
 * @param siteorder				   [out]	siteorder.
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark									
 *
 */
MA_REPOSITORY_API ma_error_t   ma_repository_get_siteorder(ma_repository_t *repository, ma_uint32_t *siteorder);

/** 
 * @brief get exclusion check of proxy from repository.
 * @param repository               [in]		repository object pointer from which exclusion check of proxy to be get.
 * @param configuration            [in]		proxy config object pointer for which exclusion check to be get.
 * @param is_excuded			   [out]	exclusion check.
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark									
 *
 */
MA_REPOSITORY_API ma_error_t   ma_repository_excusion_check(char *repo_name, ma_proxy_config_t *configuration, ma_bool_t *is_excuded);

/** 
 * @brief get server ip of repository.
 * @param repository               [in]		repository object pointer from which server ip to be get.
 * @param server_ip				   [out]	server ip.
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark									
 *
 */
MA_REPOSITORY_API ma_error_t   ma_repository_get_server_ip(ma_repository_t *repository, const char **server_ip);

/** 
 * @brief get state of repository.
 * @param repository               [in]		repository object pointer from which state to be get.
 * @param state					   [out]	state.
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark									
 *
 */
MA_REPOSITORY_API ma_error_t   ma_repository_get_state(ma_repository_t *repository, ma_repository_state_t *state);

/** 
 * @brief validate repository.
 * @param repository               [in]		repository object which need to validate.
 * @result									MA_OK on valid.
 *											MA_ERROR_INVALIDARG on invalid argument or invalid repository passed.
 * @remark									
 *
 */
MA_REPOSITORY_API ma_error_t   ma_repository_validate(ma_repository_t *repository);

MA_CPP(})

#endif  /* MA_REPOSITORY_H_INCLUDED */

