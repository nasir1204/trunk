/** @file ma_repository_mirror.h
 *  @brief Repository mirror
 *
*/
#ifndef MA_REPOSITORY_MIRROR_H_INCLUDED
#define MA_REPOSITORY_MIRROR_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_client.h"
#include "ma/scheduler/ma_task.h"
#include "ma/repository/ma_repository_client_defs.h"

MA_CPP(extern "C" {)


/**
 * Callback to be implemented by applications to get the mirror state info
 * @param ma_client    			   [in]         MA client.
 * @param session_id			   [in]	        session_id with which mirror in progress 
 * @param stat                     [in]         state of mirror, ma_repository_mirror_state_t
 * @param cb_data                  [in,optional] callback data as it was specified in 'ma_repository_mirror_register_state_callback', use NULL if do not want any data to be passed.
 */
typedef ma_error_t (*ma_repository_mirror_state_cb_t)(ma_client_t *ma_client, const char *session_id, ma_repository_mirror_state_t state, void *cb_data);

MA_REPOSITORY_API ma_error_t ma_repository_mirror_register_state_callback(ma_client_t *ma_client, ma_repository_mirror_state_cb_t cb, void *cb_data);


/**
    * replicates the repository (choosen by MA) to the destination
    * @param ma_client          [in] ma client object
    * @param destination_path   [in] destination full path where the repository is to be mirrored
    * @param session_id         [in,optional] session id to get the state and stop, if user want to.
*/
MA_REPOSITORY_API ma_error_t ma_repository_mirror_start(ma_client_t *ma_client, const char *destination_path, ma_buffer_t **session_id);

/**
    * stop the replication/mirror session in progress.
    * @param ma_client          [in] ma client object
    * @param session_id         [in] session id associated with mirror.
*/
MA_REPOSITORY_API ma_error_t ma_repository_mirror_get_state(ma_client_t *ma_client, ma_buffer_t *session_id, ma_repository_mirror_state_t *state);

/**
    * stop the replication/mirror session in progress.
    * @param ma_client          [in] ma client object
    * @param session_id         [in] session id associated with mirror.
*/
MA_REPOSITORY_API ma_error_t ma_repository_mirror_stop(ma_client_t *ma_client, ma_buffer_t *session_id);

/**
    * Utility for creating task type of repository mirror
    * @param ma_client          [in] ma client object
    * @param task_id            [in,optional] id of the task associated with task
    * @param destination_path   [in] destination full path where the repository is to be mirrored
    * @param task               [in] creates a task object of type mirror which is ready for scheduling.
    * @remark  This just creates scheduler task of type mirror, which will be handled by MA for replication.
               Scheduling and adding to scheduler and all user have to do.
*/
MA_REPOSITORY_API ma_error_t ma_repository_mirror_task_create(ma_client_t *ma_client, const char *task_id, const char *destination_path, ma_task_t **task);

/**
    * get session id associated with mirror task
    * @param task               [in] task object of type mirror
    * @param session_id         [in] session id to get the state and stop, if user want to.
*/
MA_REPOSITORY_API ma_error_t ma_repository_mirror_task_get_session_id(ma_task_t *task, ma_buffer_t **session_id);


MA_CPP(})

#include "ma/dispatcher/ma_repository_mirror_dispatcher.h"

#endif  /* MA_REPOSITORY_MIRROR_H_INCLUDED */
