/** @file ma_repository_client.h
 *  @brief Repository Client Interfaces
 *
*/
#ifndef MA_REPOSITORY_CLIENT_H_INCLUDED
#define MA_REPOSITORY_CLIENT_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_client.h"
#include "ma/proxy/ma_proxy_config.h"
#include "ma/repository/ma_repository_list.h"

MA_CPP(extern "C" {)

/** fetch repositories from MA
 *
 * @param ma_client    			[in]  MA client object
 * @param repository_list		[out]  repository list returns on success
 * @result                            MA_OK on success.
 *                                    MA_ERROR_INVALIDARG on invalid argument passed.
 *                                    MA_ERROR_REPOSITORY_INVALID_SITELIST if invalid sitelist
 * @remark
 *
 */
MA_REPOSITORY_API ma_error_t ma_repository_client_get_repositories(ma_client_t *ma_client, ma_repository_list_t **repository_list);

/** Save repositories to MA
 * 
 * @param ma_client    			[in]  MA client object
 * @param repository_list		[in]  repository list to be saved
 * @result                            MA_OK on success.
 *                                    MA_ERROR_INVALIDARG on invalid argument passed.
 *                                    
 * @remark Any modified repositories will be overridden based on the policy on enforcement
 *
 */
MA_REPOSITORY_API ma_error_t ma_repository_client_set_repositories(ma_client_t *ma_client, ma_repository_list_t *repository_list);

/**
 * imports a repoisitories through buffer in the ePO sitelist format
 * @param ma_client    			[in]  MA client object
 * @param repo_buffer			[in]  buffer in xml format of ePO sitelist format
 * @result                            MA_OK on success.
 *                                    MA_ERROR_INVALIDARG on invalid argument passed.
 *                                    MA_ERROR_REPOSITORY_INVALID_SITELIST if invalid sitelist
 * @remark if MA is in managed mode and trying to import sitelist with no spipe entry will be considered invalid.
 *
 */
MA_REPOSITORY_API ma_error_t ma_repository_client_import_repositories(ma_client_t *ma_client, ma_buffer_t *repo_buffer);

/** fetch proxy configuration from MA
 *
 * @param ma_client    			[in]  MA client object
 * @param proxy_config			[out]  proxy configuration, returns on success
 * @result                            MA_OK on success.
 *                                    MA_ERROR_INVALIDARG on invalid argument passed.
 *                                    MA_ERROR_REPOSITORY_INVALID_SITELIST if invalid sitelist
 * @remark
 *
 */
MA_REPOSITORY_API ma_error_t ma_repository_client_get_proxy_config(ma_client_t *ma_client, ma_proxy_config_t **proxy_config);


/** Save proxy configuration to MA
 * 
 * @param ma_client    			[in]  MA client object
 * @param proxy_config			[in]  proxy configuration to be saved
 * @result                            MA_OK on success.
 *                                    MA_ERROR_INVALIDARG on invalid argument passed.
 *                                    
 * @remark Any modified proxy configuration will be overridden based on the policy on enforcement
 *
 */
MA_REPOSITORY_API ma_error_t ma_repository_client_set_proxy_config(ma_client_t *ma_client, ma_proxy_config_t *proxy_config);


/** Fetch system configured proxy for a specific url
 * 
 * @param ma_client    			[in]    MA client object
 * @param url    			    [in]    url
 * @param sys_proxy_list		[out]   proxy list to be used, returns on success
 * @result                              MA_OK on success.
 *                                      MA_ERROR_INVALIDARG on invalid argument passed.
 *                                    
 * @remark  
 *
 */
MA_REPOSITORY_API ma_error_t ma_repository_client_get_system_proxy(ma_client_t *ma_client, const char *url, ma_proxy_list_t **sys_proxy_list);


/* Asynchronous get APIs */

/**
* Callback for fetching the repositories from MA using asynchronous api
* @param status                 [in]            status, MA_OK on success
* @param ma_client              [in]            ma client object
* @param repository_list        [in]            repository list
* @param cb_data                [in,optional]   callback data as it was specified in 'ma_repository_client_async_get_repositories'
*
*/
typedef ma_error_t (*ma_repositories_get_cb_t)(ma_error_t status, ma_client_t *ma_client, const ma_repository_list_t *repository_list, void *cb_data);

/** fetch repositories from MA asynchronously
 *
 * @param ma_client    			    [in]  MA client object
 * @param ma_repositories_get_cb_t	[in]  user's callback to be invoked
 * @param cb_data                   [in]   user's data, this will be provided in the callback. This is opaque to MA
 * @result                            MA_OK on success.
 *                                    MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark
 *
 */
MA_REPOSITORY_API ma_error_t ma_repository_client_async_get_repositories(ma_client_t *ma_client, ma_repositories_get_cb_t cb, void *cb_data); 


/**
* Callback for fetching the proxy configuration from MA using asynchronous api
* @param status                 [in]            status, MA_OK on success
* @param ma_client              [in]            ma client object
* @param proxy_config           [in]            proxy configuration
* @param cb_data                [in,optional]   callback data as it was specified in 'ma_repository_client_async_get_proxy_config'
*
*/
typedef ma_error_t (*ma_proxy_config_get_cb_t)(ma_error_t status, ma_client_t *ma_client, const ma_proxy_config_t *proxy_config, void *cb_data);

/** fetch proxy configuration from MA asynchronously
 *
 * @param ma_client    			    [in]  MA client object
 * @param ma_proxy_config_get_cb_t	[in]  user's callback to be invoked
 * @param cb_data                   [in]  user's data, this will be provided in the callback. This is opaque to MA
 * @result                            MA_OK on success.
 *                                    MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark  
 *
 */
MA_REPOSITORY_API ma_error_t ma_repository_client_async_get_proxy_config(ma_client_t *ma_client, ma_proxy_config_get_cb_t cb, void *cb_data); 


/**
* Callback for fetching the system configured proxy for a specific url using asynchronous api
* @param status                 [in]            status, MA_OK on success
* @param ma_client              [in]            ma client object
* @param repository_list        [in]            repository list
* @param cb_data                [in,optional]   callback data as it was specified in 'ma_repository_client_async_get_repositories'
*
*/
typedef ma_error_t (*ma_system_proxy_get_cb_t)(ma_error_t status, ma_client_t *ma_client, ma_proxy_list_t *system_proxy, void *cb_data);

/** fetch system configured proxy for a specific url from MA asynchronously
 *
 * @param ma_client    			    [in]  MA client object
 * @param url    			        [in]  url
 * @param ma_system_proxy_get_cb_t	[in]  user's callback to be invoked
 * @param cb_data                   [in]   user's data, this will be provided in the callback. This is opaque to MA
 * @result                            MA_OK on success.
 *                                    MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark
 *
 */
MA_REPOSITORY_API ma_error_t ma_repository_client_async_get_system_proxy(ma_client_t *ma_client, const char *url, ma_system_proxy_get_cb_t cb, void *cb_data);

MA_CPP(})

#include "ma/dispatcher/ma_repository_dispatcher.h"

#endif  /* MA_REPOSITORY_CLIENT_H_INCLUDED */
