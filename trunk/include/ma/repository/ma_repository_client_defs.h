/** @file ma_repository_client_defs.h
 *  @brief Macros and Declarations
 *
*/
#ifndef MA_REPOSITORY_CLIENT_DEFS_H_INCLUDED
#define MA_REPOSITORY_CLIENT_DEFS_H_INCLUDED

#include "ma/ma_common.h"

MA_CPP(extern "C" {)

/* get repositories filter url_type.state */
#define MA_REPOSITORY_GET_REPOSITORIES_FILTER_NONE								"*"
#define MA_REPOSITORY_GET_REPOSITORIES_FILTER_ALL_ENABLED						"*.enabled"
#define MA_REPOSITORY_GET_REPOSITORIES_FILTER_SPIPE_ENABLED						"spipe.enabled"
#define MA_REPOSITORY_GET_REPOSITORIES_FILTER_SPIPE								"spipe"

/* Task types owned by MA Mirror */
#define MA_MIRROR_TASK_TYPE_MIRROR_STR									"ma.mirror.task.type.mirror"


/*! \brief Current repository mirror initiator type identification values
*/
typedef enum ma_repository_mirror_initiator_type_e {
    MA_REPOSITORY_MIRROR_INITIATOR_TYPE_UNKNOWN                         = -1,
    MA_REPOSITORY_MIRROR_INITIATOR_TYPE_BASE                            = 0,
    
    MA_REPOSITORY_MIRROR_INITIATOR_TYPE_ONDEMAND_MIRROR                 = MA_REPOSITORY_MIRROR_INITIATOR_TYPE_BASE,
    MA_REPOSITORY_MIRROR_INITIATOR_TYPE_TASK_MIRROR                     = MA_REPOSITORY_MIRROR_INITIATOR_TYPE_BASE + 1,    
} ma_repository_mirror_initiator_type_t;

/*! \brief Current repository mirror session state identification values
*/

typedef enum ma_repository_mirror_state_e {
	MA_REPOSITORY_MIRROR_STATE_UNKNOWN                                  = -1,
    MA_REPOSITORY_MIRROR_STATE_BASE                                     = 0, 

    MA_REPOSITORY_MIRROR_STATE_FINISHED_SUCCEEDED                       = MA_REPOSITORY_MIRROR_STATE_BASE,
    MA_REPOSITORY_MIRROR_STATE_FINISHED_FAILED                          = MA_REPOSITORY_MIRROR_STATE_BASE + 1,
	MA_REPOSITORY_MIRROR_STATE_IN_PROGRESS                              = MA_REPOSITORY_MIRROR_STATE_BASE + 2,
    MA_REPOSITORY_MIRROR_STATE_CANCELED                                 = MA_REPOSITORY_MIRROR_STATE_BASE + 3,
    MA_REPOSITORY_MIRROR_STATE_TIMEOUT                                  = MA_REPOSITORY_MIRROR_STATE_BASE + 4,
} ma_repository_mirror_state_t;


MA_CPP(})

#endif /* MA_REPOSITORY_CLIENT_DEFS_H_INCLUDED */
