/** @file ma_repository_list.h
 *  @brief Repository List
 *
*/
#ifndef MA_REPOSITORY_LIST_H_INCLUDED
#define MA_REPOSITORY_LIST_H_INCLUDED
#include "ma/ma_variant.h"
#include "ma/repository/ma_repository.h"

MA_CPP(extern "C" {)

typedef struct ma_repository_list_s ma_repository_list_t, *ma_repository_list_h ;

/**
    * @brief create repository list
    * @param repository_list                [out]			repository list
	* @result												MA_OK on success
	*														MA_ERROR_INVALIDARG on invalid argument passed
	*														MA_ERROR_OUTOFMEMORY on memory allocation failuer
    *
    * @remark   
    */
MA_REPOSITORY_API ma_error_t   ma_repository_list_create(ma_repository_list_t **repository_list);

/**
    * @brief release repository list
    * @param repository_list                [in]			repository list
	* @result												MA_OK on success
	*														MA_ERROR_INVALIDARG on invalid argument passed
    *
    * @remark   
    */
MA_REPOSITORY_API ma_error_t   ma_repository_list_release(ma_repository_list_t *repository_list);

/**
    * @brief add repository in list
    * @param repository_list                [in]			repository list
	* @param repository		                [in]			repository to be added in list
	* @result												MA_OK on success
	*														MA_ERROR_INVALIDARG on invalid argument passed
	*														MA_ERROR_OUTOFMEMORY on memory allocation failuer
    *
    * @remark   
    */
MA_REPOSITORY_API ma_error_t   ma_repository_list_add_repository(ma_repository_list_t *repository_list, ma_repository_t *repository);

/**
    * @brief remove repository from list
    * @param repository_list                [in]			repository list
	* @param repository		                [in]			repository to be remove from list
	* @result												MA_OK on success
	*														MA_ERROR_INVALIDARG on invalid argument passed
	*														MA_ERROR_OUTOFMEMORY on memory allocation failuer
    *
    * @remark   
    */
MA_REPOSITORY_API ma_error_t   ma_repository_list_remove_repository(ma_repository_list_t *repository_list, ma_repository_t *repository);

/**
    * @brief get repository list size
    * @param repository_list                [in]			repository list
	* @param repositories_count             [out]			repository count
	* @result												MA_OK on success
	*														MA_ERROR_INVALIDARG on invalid argument passed
    *
    * @remark   
    */
MA_REPOSITORY_API ma_error_t   ma_repository_list_get_repositories_count(ma_repository_list_t *repository_list, size_t *repositories_count);

/**
    * @brief get repository from list at indexed location 
    * @param repository_list                [in]			repository list
	* @param index				            [in]			location
	* @param repository			            [out]			repository
	* @result												MA_OK on success
	*														MA_ERROR_INVALIDARG on invalid argument passed
	*														MA_ERROR_OUTOFMEMORY on memory allocation failuer
    *
    * @remark   
    */
MA_REPOSITORY_API ma_error_t   ma_repository_list_get_repository_by_index(ma_repository_list_t *repository_list, ma_int32_t index, ma_repository_t **repository);

/**
    * @brief get repository by name 
    * @param repository_list                [in]			repository list
	* @param name				            [in]			repository name
	* @param repository			            [out]			repository
	* @result												MA_OK on success
	*														MA_ERROR_INVALIDARG on invalid argument passed
	*														MA_ERROR_OUTOFMEMORY on memory allocation failuer
    *
    * @remark   
    */
MA_REPOSITORY_API ma_error_t   ma_repository_list_get_repository_by_name(ma_repository_list_t *repository_list, const char *name, ma_repository_t **repository);

MA_CPP(})

#endif  /* MA_REPOSITORY_LIST_H_INCLUDED */
