/** @file ma_property_bag.h
 *  @brief Property bag to provider properties.
 *  
*/

#ifndef MA_PROPERTY_BAG_H_INCLUDED
#define MA_PROPERTY_BAG_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_variant.h"

MA_CPP(extern "C" {)

typedef struct ma_property_bag_s ma_property_bag_t, *ma_property_bag_h;

typedef enum ma_property_bag_type_e {
	MA_PROPERTY_BAG_TYPE_FULL_PROPS = 0,
	MA_PROPERTY_BAG_TYPE_MIN_PROPS = 1,
}ma_property_bag_type_t;

/**
* Adds properties to the bag.
* @param bag        [in]    an opaque property bag.
* @param path       [in]    path of the property,similar to section name.
* @param name       [in]    name of property,similar to setting name.
* @param value      [in]    value which can be any variant.
* @result                   MA_OK on success.
*                           MA_ERROR_INVALIDARG on invalid argument passed.
*                           any error codervariant error codes if it fails.
*
* @remark As of now it can accept only string variant due to limitation of ePO Properties format as XML. Future we will add support for handling all variants.
*/

MA_PROPERTY_API ma_error_t ma_property_bag_add_property(ma_property_bag_t *bag, const char *path, const char *name, ma_variant_t *value);

/**
* set property bag type.
* @param bag	      [in]  property bag.
* @param bag_type	  [in]  bag type.
* @result                   MA_OK on success.
*                           MA_ERROR_INVALIDARG on invalid argument passed.
*                           any error codervariant error codes if it fails.
*
* @remark 
*/
MA_PROPERTY_API ma_error_t ma_property_bag_get_type(ma_property_bag_t *bag, ma_property_bag_type_t *bag_type);


MA_CPP(})

#endif /* MA_PROPERTY_BAG_H_INCLUDED */


