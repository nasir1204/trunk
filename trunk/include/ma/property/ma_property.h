/** @file ma_property.h
 *  @brief Property collection interfaces for products 
 *  
 */
#ifndef MA_PROPERTY_H_INCLUDED
#define MA_PROPERTY_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_client.h"
#include "ma/property/ma_property_bag.h"

MA_CPP(extern "C" {)

/**
* Callback for providing the properties
* @param ma_client              [in]            ma client object
* @param product_id             [in]            product_id for which property provider callback has been called
* @param property_bag           [in]            a bag in which the provider providers should fill its properties(ma_property_bag_add)
* @param cb_data                [in,optional]   callback data as it was specified in 'ma_property_register_provider_callback'
*
*/
typedef ma_error_t (*ma_property_provider_cb_t)(ma_client_t *ma_client, const char *product_id, ma_property_bag_t *property_bag, void *cb_data);

/**
* Registers a property provider with ma, the provided callback will be called at ma's discretion at periodic intervals to collect properties
* @param ma_client              [in]   ma client object
* @param product_id             [in]   product id of the provider for which properties are reported to server
* @param property_provider_cb   [in]   user's callback to be invoked at property collection time
* @param cb_data                [in]   user's data, this will be provided in the callback. This is opaque to MA
* @result                              MA_OK on success
*                                      MA_ERROR_INVALIDARG on invalid argument passed
*                                      MA_ERROR_PROPERTY_PROVIDER_ALREADY_REGISTERED if the provider already registered with the product id
* @remark product_id can be NULL, if we want to provide the properties for product id same as client
* @remark can be called multiple times if user want's to register for different property provider with different product ID's
* @remark This is mandatory to have properties registered if product is integrating with ePO
*/

MA_PROPERTY_API ma_error_t ma_property_register_provider_callback(ma_client_t *ma_client, const char *product_id, ma_property_provider_cb_t property_provider_cb, void *cb_data);

/**
* Un-registers a property provider with ma for the product id
* @param ma_client              [in]   ma client object
* @param product_id             [in]   product id of the provider for which properties are reported to server
* @result                              MA_OK on success
*                                      MA_ERROR_INVALIDARG on invalid argument passed
*                                      MA_ERROR_PROPERTY_PROVIDER_NOT_REGISTERED if the provider is not registered with the product id
* @remark product_id can be NULL, if product id same as client
* @remark should be called exact times the number for which registier is called
*/
MA_PROPERTY_API ma_error_t ma_property_unregister_provider_callback(ma_client_t *ma_client, const char *product_id);

/**
* Send request to MA to initiate the collect and send properties
* @param ma_client              [in]   ma client object
* @param product_id             [in]   product id  for which properties are reported to server
* @result                              MA_OK on success
*                                      MA_ERROR_INVALIDARG on invalid argument passed
*                                      MA_ERROR_PROPERTY_PROVIDER_NOT_REGISTERED if the provider is not registered with the product id
* @remark Call ONLY on need basis as it effects the network traffic and every other product for collection.
*/
MA_PROPERTY_API ma_error_t ma_property_send(ma_client_t *ma_client, const char *product_id);

MA_CPP(})

#include "ma/dispatcher/ma_property_dispatcher.h"

#endif /* MA_PROPERTY_H_INCLUDED */


