#ifndef MA_SCHEDULER_H_INCLUDED
#define MA_SCHEDULER_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_client.h"
#include "ma/scheduler/ma_task.h"

MA_CPP(extern "C" {)

typedef struct ma_booker_task_callbacks_s ma_booker_task_callbacks_t, *ma_booker_task_callbacks_h;

struct ma_booker_task_callbacks_s {
    /**
    * Callback for receiving the task and executing the same
    * @param ma_client              [in]            ma client object.
    * @param product_id             [in]            product_id for which booker task callback is been called. 
    * @param task_type				[in]            type of the task for which booker task callback is been called.
    * @param task					[in]			task object. Do not release this task.
    * @param cb_data                [in,optional]   callback data as it was specified in 'ma_booker_register_task_callbacks', use NULL if do not want any data to be passed.
    *
    */
    ma_error_t (*on_execute)(ma_client_t *ma_client, const char *product_id, const char *task_type, ma_task_t *task, void *cb_data);

    /**
    * Callback for receiving the task and stopping the same
    * @param ma_client              [in]            ma client object.
    * @param product_id             [in]            product_id for which booker task callback is been called. 
    * @param task_type				[in]            type of the task for which booker task callback is been called.
    * @param task					[in]			task object. Do not release this task.
    * @param cb_data                [in,optional]   callback data as it was specified in 'ma_booker_register_task_callbacks', use NULL if do not want any data to be passed.
    *
    */
    ma_error_t (*on_stop)(ma_client_t *ma_client, const char *product_id, const char *task_type, ma_task_t *task, void *cb_data);
    /**
    * Callback for receiving the task and removing the same
    * @param ma_client              [in]            ma client object.
    * @param product_id             [in]            product_id for which booker task callback is been called. 
    * @param task_type				[in]            type of the task for which booker task callback is been called.
    * @param task					[in]			task object. Do not release this task.
    * @param cb_data                [in,optional]   callback data as it was specified in 'ma_booker_register_task_callbacks', use NULL if do not want any data to be passed.
    *
    */
    ma_error_t (*on_remove)(ma_client_t *ma_client, const char *product_id, const char *task_type, ma_task_t *task, void *cb_data);
    /**
    * Callback for receiving the task and updating the task execution status
    * @param ma_client              [in]            ma client object.
    * @param product_id             [in]            product_id for which booker task callback is been called. 
    * @param task_type				[in]            type of the task for which booker task callback is been called.
    * @param task					[in]			task object. Do not release this task.
    * @param cb_data                [in,optional]   callback data as it was specified in 'ma_booker_register_task_callbacks', use NULL if do not want any data to be passed.
    *
    */
    ma_error_t (*on_update_status)(ma_client_t *ma_client, const char *product_id, const char *task_type, ma_task_t *task, void *cb_data);
};

/**
* Registers a task user with ma, the provided callback will be called based on the task schedule.
* @param ma_client              [in]   ma client object.
* @param product_id             [in]   product id of the user for which tasks are received.
* @param task_type				[in]   type of the task for which a user is interested in.
* @param task_callbacks			[in]   set of user's callbacks to be invoked at execute/stop/remove time of a task.
* @param cb_data                [in]   user's data, this will be provided in the callback.This is opaque to MA.
* @result                              MA_OK on success.
*                                      MA_ERROR_INVALIDARG on invalid argument passed.
*                                      MA_ERROR_SCHEDULER_CALLBACKS_ALREADY_REGISTERED if the task user is already registered with the product id and task type.
* @remark product_id can be NULL, if we want to provide the product id same as client.
* @remark can be called multiple times if user wants to register for different product id and task type combination.
* @remark This is mandatory to have task callbacks registered if product is integrating with ePO and wish to receive the ePO tasks.
*/
MA_SCHEDULER_API ma_error_t ma_booker_register_task_callbacks(ma_client_t *ma_client, const char *product_id, const char *task_type, ma_booker_task_callbacks_t const *task_callbacks, void *cb_data);

/**
* Un-registers a task user with ma for the product id and task type
* @param ma_client              [in]   ma client object.
* @param product_id             [in]   product id of the user for which tasks are received.
* @param task_type				[in]   type of the task for which a user is interested in.
* @result                              MA_OK on success.
*                                      MA_ERROR_INVALIDARG on invalid argument passed.
*                                      MA_ERROR_SCHEDULER_CALLBACKS_NOT_REGISTERED if the task user is not registered with the product id and task type
* @remark product_id can be NULL, if product id is same as client.
* @remark should be called exact number of times for which register is called.
*/
MA_SCHEDULER_API ma_error_t ma_booker_unregister_task_callbacks(ma_client_t *ma_client, const char *product_id, const char *task_type);

/**
* Gets the numeric representation of booker client library that you're currently linked against
* The format uses one byte each for the major, minor, and patchlevel parts of the version number. The low-order byte is unused. 
* For example, 5.2.1 version has a numeric representation of 0x05020100
* @param version    [out]   version number is written into this address
*/
MA_SCHEDULER_API ma_error_t ma_booker_get_version(ma_uint32_t *version);

/**
* Adds a task into booker.
* @param ma_client  [in]   ma client object.
* @param task		[in]   task object.
* @result                  MA_OK on success.
*                          MA_ERROR_INVALIDARG on invalid argument passed.
*                          MA_ERROR_SCHEDULER_INVALID_TASK_ID if such a task id pre-exist.
*/
MA_SCHEDULER_API ma_error_t ma_booker_add_task(ma_client_t *ma_client, ma_task_t *task);

/**
* Gets a task from booker.
* @param ma_client  [in]   ma client object.
* @param task_id	[in]   id of the task
* @param task_name	[in]   name of the task
* @param task_type	[in]   type of the task
* @param task		[out]  Address of pointer that receives the task object. Don't forget to call ma_task_release() when you are done
* @result                  MA_OK on success.
*                          MA_ERROR_INVALIDARG on invalid argument passed.
*                          MA_ERROR_SCHEDULER_INVALID_TASK_ID if there is no such task id.
*/
MA_SCHEDULER_API ma_error_t ma_booker_get_task(ma_client_t *ma_client, const char *task_id, ma_task_t **task);

/**
* Removes a task from booker.
* @param ma_client  [in]   ma client object.
* @param task_id	[in]   id of the task
* @result                  MA_OK on success.
*                          MA_ERROR_INVALIDARG on invalid argument passed.
*                          MA_ERROR_SCHEDULER_INVALID_TASK_ID if there is no such task id.
*/
MA_SCHEDULER_API ma_error_t ma_booker_remove_task(ma_client_t *ma_client, const char *task_id);

/**
* Alters a task in booker.
* @param ma_client  [in]   ma client object.
* @param task		[in]   altered task
* @result                  MA_OK on success.
*                          MA_ERROR_INVALIDARG on invalid argument passed.
*/
MA_SCHEDULER_API ma_error_t ma_booker_alter_task(ma_client_t *ma_client, ma_task_t *task);

/**
* Updates the status of running task in the booker. It is usually called at the end of ma_booker_task_callbacks_t
* @param ma_client  [in]   ma client object.
* @param task_id	[in]   id of the task
* @param task_name	[in]   name of the task
* @param task_type	[in]   type of the task
* @param status		[in]   status of the task 
* @result                  MA_OK on success.
*                          MA_ERROR_INVALIDARG on invalid argument passed.
*                          MA_ERROR_SCHEDULER_INVALID_TASK_ID if there is no such task id.
*/
MA_SCHEDULER_API ma_error_t ma_booker_update_task_status(ma_client_t *ma_client, const char *task_id, ma_task_exec_status_t status);

/**
* Enumerate schedule task(s)
* @param ma_client  [in]   ma client object.
* @param product_id [in]   product id
* @param creator_id [in]   creator id of the task
* @param task_type  [in]   type of the task
* @param task       [out]  Address of pointer that receives the enumerated task objects. Don't forget to call free() when you are done
* @param no_of_tasks[out]  Pointer to memory location that holds the number of enumerated task count
* @result                  MA_OK on success.
*                          MA_ERROR_INVALIDARG on invalid argument passed.
*                          MA_ERROR_SCHEDULER_INVALID_TASK_ID if there is no such task id.
*/
MA_SCHEDULER_API ma_error_t ma_booker_enumerate_task(ma_client_t *ma_client, const char *product_id, const char *creator_id, const char *task_type, ma_task_t ***task_set, size_t *no_of_tasks);


/**
* Enumerate schedule task(s) release
* @param task_set   [in]  Address of pointer that holds enumerated task objects set
* @param no_of_tasks[in]  Pointer to memory location that holds the number of enumerated task count
* @result                 MA_OK on success.
*                         MA_ERROR_INVALIDARG on invalid argument passed.
*/
MA_SCHEDULER_API ma_error_t ma_booker_enumerate_task_release(ma_task_t **task_set, size_t *no_of_tasks);


/**
* Postpone scheduled task execution 
* @param ma_client		[in]	ma client object.
* @param task_id		[in]	task id
* @param intervals		[in]	postpone delay in minutes
* @result						MA_OK on success.
*								MA_ERROR_INVALIDARG on invalid argument passed.
*/
MA_SCHEDULER_API ma_error_t ma_booker_postpone_task(ma_client_t *ma_client, const char *task_id, size_t intervals);

MA_CPP(})

#include "ma/dispatcher/ma_booker_dispatcher.h"

#endif /* MA_SCHEDULER_H_INCLUDED */





