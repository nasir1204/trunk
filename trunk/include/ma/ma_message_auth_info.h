#ifndef MA_MESSAGE_AUTH_INFO_H_INCLUDED
#define MA_MESSAGE_AUTH_INFO_H_INCLUDED

#include "ma/ma_common.h"

MA_CPP(extern "C" {)
typedef struct ma_message_auth_info_s ma_message_auth_info_t, *ma_message_auth_info_h;

typedef enum ma_message_auth_info_attribute_e {
    /* Issuer's CN. Type: address of const char* */
    MA_MSGBUS_AUTH_INFO_ATTRIBUTE_VERIFIED_SIGNER = 0,

    /* Subject's CN. Type: address of const char* */
    MA_MSGBUS_AUTH_INFO_ATTRIBUTE_VERIFIED_PUBLISHER,

    /* Process' user (s)id. Type: address of PSID */
    MA_MSGBUS_AUTH_INFO_ATTRIBUTE_USER_SID,

    /* Process' id. Type: address of long */
    MA_MSGBUS_AUTH_INFO_ATTRIBUTE_PID,

    /* Process' effective uid. Type: address of long */
    MA_MSGBUS_AUTH_INFO_ATTRIBUTE_UID,

    /* Process' effective gid. Type: address of long */
    MA_MSGBUS_AUTH_INFO_ATTRIBUTE_GID,

    /* Process is admin/root. Type: address of ma_bool_t. Vista+: must be elevated to achieve this */
    MA_MSGBUS_AUTH_INFO_ATTRIBUTE_USER_IS_PRIVILEGED,

} ma_message_auth_info_attribute_t;

#include "ma/ma_msgbus.h"


#define ma_message_auth_info_get_verified_signer(x,y) \
    ma_message_auth_info_get_attribute((x), MA_MSGBUS_AUTH_INFO_ATTRIBUTE_VERIFIED_SIGNER, (const char **)(y))

#define ma_message_auth_info_get_verified_publisher(x,y) \
    ma_message_auth_info_get_attribute((x), MA_MSGBUS_AUTH_INFO_ATTRIBUTE_VERIFIED_PUBLISHER, (const char **)(y))

#define ma_message_auth_info_get_user_sid(x,y) \
    ma_message_auth_info_get_attribute((x), MA_MSGBUS_AUTH_INFO_ATTRIBUTE_USER_SID, (const void **)(y))

#define ma_message_auth_info_get_pid(x,y) \
    ma_message_auth_info_get_attribute((x), MA_MSGBUS_AUTH_INFO_ATTRIBUTE_PID, (long *)(y))

#define ma_message_auth_info_get_uid(x,y) \
    ma_message_auth_info_get_attribute((x), MA_MSGBUS_AUTH_INFO_ATTRIBUTE_UID, (long *)(y))

#define ma_message_auth_info_get_gid(x,y) \
    ma_message_auth_info_get_attribute((x), MA_MSGBUS_AUTH_INFO_ATTRIBUTE_GID, (long *)(y))

#define ma_message_auth_info_get_is_privileged(x,y) \
    ma_message_auth_info_get_attribute((x), MA_MSGBUS_AUTH_INFO_ATTRIBUTE_USER_IS_PRIVILEGED, (ma_bool_t *)(y)) 


MA_MSGBUS_API ma_error_t ma_message_auth_info_add_ref(ma_message_auth_info_t *auth_info);

MA_MSGBUS_API ma_error_t ma_message_auth_info_release(ma_message_auth_info_t *auth_info);

MA_MSGBUS_API ma_error_t ma_message_auth_info_get_attribute(const ma_message_auth_info_t *auth_info, ma_message_auth_info_attribute_t ident_attribute, ...);

MA_CPP(})

#endif /* MA_MESSAGE_AUTH_INFO_H_INCLUDED */
