#ifndef MA_OUTPUTDEBUGSTRING_LOGGER_H_INCLUDED
#define MA_OUTPUTDEBUGSTRING_LOGGER_H_INCLUDED
/*
 Implementation of logger that simply outputs log messages via OutputDebugString (on Windows. Does nothing on Unix at this time)
*/

#include "ma/logger/ma_logger.h"


MA_CPP(extern "C" {)

/**
 * creates an instance
 */
MA_LOGGER_API ma_error_t ma_outputdebugstring_logger_create(ma_logger_t **logger);

MA_CPP(})

#endif /* MA_OUTPUTDEBUGSTRING_LOGGER_H_INCLUDED */
