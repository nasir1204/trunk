#ifndef MA_LOG_MSG_H_INCLUDED
#define MA_LOG_MSG_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_log.h"
#include "ma/ma_buffer.h"

MA_CPP(extern "C" {)

typedef struct ma_log_msg_s ma_log_msg_t, *ma_log_msg_h;

typedef struct ma_log_time_s ma_log_time_t, *ma_log_time_h;

/**
* get log message severity.
* @param log_msg	        [in]			log message
* @param severity	        [out]			log message severity
* @result                   MA_OK on success.
*                           MA_ERROR_INVALIDARG on invalid argument passed.
*
* @remark 
*/
MA_LOGGER_API ma_error_t ma_log_msg_get_severity(ma_log_msg_t *log_msg, ma_log_severity_t *severity);

/**
* get log message severity string.
* @param log_msg	        [in]			log message
* @param severity_label     [out]			severity string.
* @result                   MA_OK on success.
*                           MA_ERROR_INVALIDARG on invalid argument passed.
*
* @remark 
*/
MA_LOGGER_API ma_error_t ma_log_msg_get_severity_str(ma_log_msg_t *log_msg, ma_buffer_t **severity_label);

/**
* get log message facility.
* @param log_msg	        [in]			log message
* @param facility		    [out]			facility
* @result                   MA_OK on success.
*                           MA_ERROR_INVALIDARG on invalid argument passed.
*
* @remark 
*/
MA_LOGGER_API ma_error_t ma_log_msg_get_facility(ma_log_msg_t *log_msg, ma_buffer_t **facility);

/**
* check whether log message is wide charater string.
* @param log_msg	        [in]			log message
* @param is_wide_char	    [out]			is wide character string
* @result                   MA_OK on success.
*                           MA_ERROR_INVALIDARG on invalid argument passed.
*
* @remark 
*/
MA_LOGGER_API ma_error_t ma_log_msg_is_wchar_message(ma_log_msg_t *log_msg, ma_bool_t *is_wide_char);
   
/**
* get wchar message from log
* @param log_msg	        [in]			log message
* @param message		    [out]			wide character message
* @result                   MA_OK on success.
*                           MA_ERROR_INVALIDARG on invalid argument passed.
*
* @remark 
*/
MA_LOGGER_API ma_error_t ma_log_msg_get_wchar_message(ma_log_msg_t *log_msg, ma_wbuffer_t **message);

/**
* get message from log
* @param log_msg	        [in]			log message
* @param message		    [out]			message
* @result                   MA_OK on success.
*                           MA_ERROR_INVALIDARG on invalid argument passed.
*
* @remark 
*/
MA_LOGGER_API ma_error_t ma_log_msg_get_message(ma_log_msg_t *log_msg, ma_buffer_t **message);

#include "ma/dispatcher/ma_log_msg_dispatcher.h"

MA_CPP(})

#endif //#ifndef MA_LOG_MSG_H_INCLUDED

