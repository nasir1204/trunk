
#ifndef MA_CONSOLE_LOGGER_H_INCLUDED
#define MA_CONSOLE_LOGGER_H_INCLUDED

#include "ma/logger/ma_logger.h"
/* Declaration of ma_console_logger  - a logger implementation that logs the messages on the console  */

MA_CPP(extern "C" {)

typedef struct ma_console_logger_s ma_console_logger_t ;

#define DEFAULT_CONSOLE_LOG_FORMAT_PATTERN     \
                "%d %+t %p(%P.%T) %f.%s: %m"


/** @brief console logger constructor
 *  @param  [out] logger            pointer to memory location that hold newly constructed console logger object
 *  @result                         MA_OK on success
 *                                  MA_ERROR_INVALIDARG on invalid argument passed
 *                                  MA_ERROR_OUTOFMEMORY on memory allocation failure
 */
MA_LOGGER_API ma_error_t ma_console_logger_create(ma_console_logger_t **logger);


/** @brief console logger destructor
 *  @param  [in] logger     console logger handle
 *  @result                 MA_OK on success
 *                          MA_ERROR_INVALIDARG on invalid argument passed
 */
MA_LOGGER_API ma_error_t ma_console_logger_release(ma_console_logger_t *logger);

#include "ma/dispatcher/ma_console_logger_dispatcher.h"
MA_CPP(})

#endif //#ifndef MA_CONSOLE_LOGGER_H_INCLUDED
