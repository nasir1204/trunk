/** @file ma_logger_collector.h
 *  @brief Logger Collector Declarations
 *
 *  This file contains prototypes of logger collector api's
 *  Logger collector is polymorphic interface to add 1 to many logger's interface
 *  
 */
#ifndef MA_LOGGER_COLLECTOR_H_INCLUDED
#define MA_LOGGER_COLLECTOR_H_INCLUDED

#include "ma/logger/ma_logger.h"

MA_CPP(extern "C" {)

typedef struct ma_logger_collector_s ma_logger_collector_t, *ma_logger_collector_h;

/** @brief Create's logger collector
 *  @param  [out] self Pointer to newly constructed logger collector handle
 *  @result            MA_OK on successful
 *                     MA_ERROR_INVALIDARG on invalid argument passed
 *                     MA_ERROR_OUTOFMEMORY on memory allocation failure
 */
MA_LOGGER_API ma_error_t ma_logger_collector_create(ma_logger_collector_t **self);

/** @brief Release's logger collector
 *  @param  [in] self logger collector handle
 *  @result           MA_OK on successful
 *                    MA_ERROR_INVALIDARG on invalid argument passed
 */
MA_LOGGER_API ma_error_t ma_logger_collector_release(ma_logger_collector_t *self);

/** @brief Add's logger into logger collector
 *  @param  [in] self   logger collector handle
 *  @param  [in] logger logger handle
 *  @result             MA_OK on successful
 *                      MA_ERROR_INVALIDARG on invalid argument passed
 */
MA_LOGGER_API ma_error_t ma_logger_collector_add_logger(ma_logger_collector_t *self, ma_logger_t *logger);

/** @brief Remove's logger from logger collector
 *  @param  [in] self    logger collector handle
 *  @param  [in] logger  logger handle
 *  @result              MA_OK on successful
 *                       MA_ERROR_INVALIDARG on invalid argument passed
 */
MA_LOGGER_API ma_error_t ma_logger_collector_remove_logger(ma_logger_collector_t *self, ma_logger_t *logger);

/** @brief Get's logger collector implementation
 *  @param  [in]  self   logger collector handle
 *  @param  [out] logger Pointer to memory location that holds logger collector interface
 *  @result              MA_OK on successful
 *                       MA_ERROR_INVALIDARG on invalid argument passed
 */
MA_LOGGER_API ma_error_t ma_logger_collector_get_impl(ma_logger_collector_t *self, ma_logger_t **logger);

#include "ma/dispatcher/ma_logger_collector_dispatcher.h"
MA_CPP(})

#endif //#ifndef MA_LOGGER_COLLECTOR_H_INCLUDED
