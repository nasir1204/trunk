#ifndef MA_SYS_LOGGER_H_INCLUDED
#define MA_SYS_LOGGER_H_INCLUDED

#include "ma/logger/ma_logger.h"
/* Declaration of ma_sys_logger  - a logger implementation that logs the messages on the file  */
#include <sys/syslog.h>

MA_CPP(extern "C" {)

#define DEFAULT_SYS_LOG_FORMAT_PATTERN     \
                "%s:tid(%T):session-id(%S):uid(%u):facility(%f):%F:function(%U):line(%l):%m"
				



typedef struct ma_sys_logger_s ma_sys_logger_t ;

typedef enum ma_sys_log_severity_e {
	MA_SYS_LOG_EMERG  = 1,
	MA_SYS_LOG_ALERT ,
	MA_SYS_LOG_CRIT ,
	MA_SYS_LOG_ERR ,
	MA_SYS_LOG_WARNING ,
	MA_SYS_LOG_NOTICE ,
	MA_SYS_LOG_INFO ,
	MA_SYS_LOG_DEBUG 

}ma_sys_log_severity_t;


typedef enum ma_syslog_options_e
{
	MA_SYS_LOG_PID = 1,
	MA_SYS_LOG_CONS,
	MA_SYS_LOG_NDELAY, 
	MA_SYS_LOG_NOWAIT,
	MA_SYS_LOG_ODELAY,
	MA_SYS_LOG_PERROR 

}ma_syslog_options_t;

typedef enum ma_sys_facility_e {
	MA_SYS_LOG_USER = 1,
	MA_SYS_LOG_AUTH,
	MA_SYS_LOG_AUTHPRIV,
	MA_SYS_LOG_CRON,
    MA_SYS_LOG_DAEMON ,
    MA_SYS_LOG_FTP ,
    MA_SYS_LOG_KERN ,
    MA_SYS_LOG_LPR,
	MA_SYS_LOG_MAIL ,
	MA_SYS_LOG_NEWS ,
	MA_SYS_LOG_SYSLOG, 
	MA_SYS_LOG_UUCP 
} ma_sys_facility_t;

typedef struct ma_sys_logger_policy_s {    
    char    *format_pattern ;    
    int    options; 		 
    int    facility; 		 
	int  severity;
}ma_sys_logger_policy_t; 


MA_LOG_API ma_error_t ma_sys_logger_create(ma_sys_logger_t **logger);

MA_LOG_API void ma_sys_logger_release(ma_sys_logger_t *logger);


MA_CPP(})


#endif //#ifndef MA_SYS_LOGGER_H_INCLUDED
