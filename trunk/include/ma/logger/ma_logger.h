#ifndef MA_LOGGER_H_INCLUDED
#define MA_LOGGER_H_INCLUDED

#include "ma/ma_log.h"
#include "ma/logger/ma_log_msg.h"
#include "ma/logger/ma_log_filter.h"
#include "ma/ma_variant.h"

#include <stdarg.h>

#define MA_LOGGER_MSG_PUB_TOPIC "ma.logger.msg.pub.topic"

MA_CPP(extern "C" {)

typedef void (*ma_logger_on_visit_cb_t)(const ma_log_msg_t *log_msg, void *cb_data);

typedef ma_error_t (*ma_logger_on_localize_cb_t)(const char *msg, ma_buffer_t **localized_msg, void *cb_data);

/* 'Base' class all loggers must derive from */


/* Defines the C equivalent of a 'v-table' for the ma_logger interface implementor */
typedef struct ma_logger_methods_s {
    ma_error_t (*on_log_message)(ma_logger_t *self, ma_log_msg_t *msg);
    void (*release)(ma_logger_t *self); /*< virtual destructor */
} ma_logger_methods_t;


struct ma_logger_s {
    ma_logger_methods_t                 const *methods;   /*< v-table */
    ma_log_filter_t                     *filter; /*filter*/    
    int                                 ref_count; /*Ref count*/
    ma_logger_on_visit_cb_t             visitor_cb;
    ma_logger_on_localize_cb_t          localizer_cb;
    void                                *vistor_cb_data;
    void                                *localizer_cb_data;
    void                                *data; /* logger extension data */
};

/* define macros so we can use conforming names in our code */
#define ma_logger_add_ref(x) ma_logger_inc_ref(x)

#define ma_logger_release(x) ma_logger_dec_ref(x)


/*!
 * increments the reference count
 */
MA_LOGGER_API ma_error_t ma_logger_inc_ref(ma_logger_t *logger);

/*!
 * decrements ref count. If count reaches 0, will call derived class' release method which must clean up and free any memory 
 */
MA_LOGGER_API ma_error_t ma_logger_dec_ref(ma_logger_t *logger);

/*!
 * attaches the filter in the logger, refer ma_log_filter.h for definition.
 */
MA_LOGGER_API ma_error_t ma_logger_add_filter(ma_logger_t *logger, ma_log_filter_t *filter); 

/*!
 * de-attaches the filter in the logger  , NOTE: It will just remove the attachment; not free the filter
 */
MA_LOGGER_API ma_error_t ma_logger_remove_filter(ma_logger_t *self) ;

/*!
 * gets  the filter attached to the logger  , refer ma_log_filter.h for definition.
 */
MA_LOGGER_API ma_error_t ma_logger_get_filter(ma_logger_t *logger,  ma_log_filter_t **filter); 



MA_LOGGER_API ma_error_t ma_logger_register_visitor(ma_logger_t *logger,  ma_logger_on_visit_cb_t cb, void *cb_data);

MA_LOGGER_API ma_error_t ma_logger_unregister_visitor(ma_logger_t *logger);


MA_LOGGER_API ma_error_t ma_logger_register_localizer(ma_logger_t *logger,  ma_logger_on_localize_cb_t cb, void *cb_data);

MA_LOGGER_API ma_error_t ma_logger_unregister_localizer(ma_logger_t *logger);


MA_LOGGER_API ma_error_t ma_log_v(ma_logger_t  *logger, int severity, const char *module, const char *file, int line, const char *func, const char *fmt,va_list ap);

MA_LOGGER_API ma_error_t ma_wlog_v(ma_logger_t *logger, int severity, const char *module, const char *file, int  line, const char *func, const ma_wchar_t *fmt, va_list ap);


/*!
 * Helper function for derived classes to use for initialization
 */
static MA_INLINE void ma_logger_init(ma_logger_t *logger, ma_logger_methods_t  const *methods, int ref_count ,  void *data) {
    logger->methods = methods;
    logger->ref_count = ref_count;
    logger->data = data;    
} 

static MA_INLINE ma_error_t ma_logger_log_message(ma_logger_t *logger, ma_log_msg_t *msg) {
    if(!logger) return MA_ERROR_INVALIDARG;

    if (logger->filter && !ma_log_filter_accept(logger->filter, msg)) return MA_ERROR_LOG_FILTERED;

    return logger->methods->on_log_message(logger, msg);
}


MA_CPP(})

#endif /* MA_LOGGER_H_INCLUDED */

