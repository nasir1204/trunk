#ifndef MA_LOG_FILTER_H_INCLUDED
#define MA_LOG_FILTER_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/logger/ma_log_msg.h"

MA_CPP(extern "C" {)

/* 'Base' class for all log filters */
typedef struct ma_log_filter_s ma_log_filter_t;

/* Defines the C equivalent of a 'v-table' for the ma_log_filter interface implementor */
typedef struct ma_log_filter_methods_s {
    /*!
     * determines if a log message should be forwarded or not
     */
    ma_bool_t (*accept)(ma_log_filter_t *self, ma_log_msg_t *msg);
    
    /*!
     * virtual destructor
     */
    void (*destroy)(ma_log_filter_t *self);

} ma_log_filter_methods_t;

struct ma_log_filter_s {
    ma_log_filter_methods_t  const  *methods ; /*< v-table */
    void                            *data;     /*< log filter extension data */
};


MA_INLINE static ma_bool_t ma_log_filter_accept(ma_log_filter_t *filter, ma_log_msg_t *msg) { 
    return (filter)
        ? filter->methods->accept(filter, msg)
        : MA_FALSE;
}    

MA_INLINE static void ma_log_filter_release(ma_log_filter_t *filter) {
    /* not reference counted, so destroy right away */
    if(filter && filter->methods->destroy)
        filter->methods->destroy(filter);
}



MA_LOGGER_API ma_error_t ma_generic_log_filter_create(const char *filter_pattern , ma_log_filter_t **log_filter);

#include "ma/dispatcher/ma_logger_dispatcher.h"

MA_CPP(})

#endif /* MA_LOG_FILTER_H_INCLUDED */
