
#ifndef MA_FILE_LOGGER_H_INCLUDED
#define MA_FILE_LOGGER_H_INCLUDED

#include "ma/logger/ma_logger.h"
/* Declaration of ma_file_logger  - a logger implementation that logs the messages on the file  */

MA_CPP(extern "C" {)

typedef struct ma_file_logger_s ma_file_logger_t ;

#define DEFAULT_LOG_FILE_EXTENSION                  ".log"
#define DEFAULT_LOG_BASE_DIR                        "." 
#define MAX_FILE_EXT_LENGTH                         32  //Including NULL teminator

/* Default logging policies */
#define DEFAULT_LOG_SIZE_LIMIT                      1 /* 1 MB*/
#define DEFAULT_FILE_LOG_FORMAT_PATTERN             \
                "%d %+t %p(%P.%T) %f.%s: %m"

/*Default rollover policies */
#define DEFAULT_IS_ROLLOVER_ENABLED                 MA_FALSE
#define DEFAULT_NUMBER_OF_ROLLOVERS                 1
#define MAX_ROLLOVER_FILE_SUFFIX_LEN                32  //Including NULL terminator
#define DEFAULT_ROLLOVER_FILE_SUFFIX                "backup"   
/* Rollover file name format , <LOG_FILE_NAME>_<ROLLOVER_FILE_SUFFIX>_<(MAX_NUMBER_OF_ROLLOVER--)><LOG_FILE_EXTENSION> */

/* TODO - need to think about the pointer and the static data about policies , while applying policies to many malloc , but can be fine as it is only when policies changes */

typedef struct ma_file_logger_rollover_policy_s {        
    char                *rollover_file_suffix;
    unsigned short int  max_number_of_rollovers ;    
}ma_file_logger_rollover_policy_t;

typedef struct ma_file_logger_policy_s {    
	ma_bool_t                           is_logging_enabled ;
    char                                *format_pattern ;   //Refer ma_log_utils.h for pattern  types supported
    size_t                              log_size_limit ;     //in MB
    ma_bool_t                           is_rollover_enabled ;
    ma_file_logger_rollover_policy_t    rollover_policy ;
}ma_file_logger_policy_t;


/** @brief file logger constructor
 *  @param  [in] log_base_dir       base directory
 *  @param  [in] log_file_name      log file name
 *  @param  [in] log_file_ext       log file extension
 *  @param  [out] logger            pointer to memory location that hold newly constructed file logger object
 *  @result                         MA_OK on success
 *                                  MA_ERROR_INVALIDARG on invalid argument passed
 *                                  MA_ERROR_OUTOFMEMORY on memory allocation failure
 */
MA_LOGGER_API ma_error_t ma_file_logger_create( const char *log_base_dir , const char *log_file_name  , const char *log_file_ext ,  ma_file_logger_t **logger);

/** @brief file logger destructor
 *  @param  [in] logger     file logger handle
 *  @result                 MA_OK on success
 *                          MA_ERROR_INVALIDARG on invalid argument passed
 */
MA_LOGGER_API ma_error_t ma_file_logger_release(ma_file_logger_t *logger);

/** @brief set's file logger policy
 *  @param  [in]    logger  file logger handle
 *  @param  [in]    policy  pointer to memory location that holds file logger policy
 *  @result         MA_OK on success
 *                  MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_LOGGER_API ma_error_t ma_file_logger_set_policy(ma_file_logger_t *logger , ma_file_logger_policy_t *policy);

/** @brief retrieves file logger policy
 *  @param  [in]    logger  file logger handle
 *  @param  [out]   policy  pointer to memory location that holds file logger policy
 *  @result         MA_OK on success
 *                  MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_LOGGER_API ma_error_t ma_file_logger_get_policy(ma_file_logger_t *logger , const ma_file_logger_policy_t **policy);

#include "ma/dispatcher/ma_file_logger_dispatcher.h"

MA_CPP(})

#endif /* MA_FILE_LOGGER_H_INCLUDED */

