#ifndef MA__MAILSLOT_LOGGER_H_INCLUDED
#define MA__MAILSLOT_LOGGER_H_INCLUDED

#include "ma/logger/ma_logger.h"

#define DEFAULT_MAILSLOT_LOG_FORMAT_PATTERN             \
                "%d %+t %p(%P.%T) %f.%s: %m"

MA_CPP(extern "C" {)

typedef struct ma_mail_slot_logger_s ma_mail_slot_logger_t;

typedef struct ma_mail_slot_logger_policy_s {    
    char    *format_pattern ;
}ma_mail_slot_logger_policy_t;


/** @brief mail slot logger constructor
 *  @param  [in] slot       slot location
 *  @param  [out] logger    pointer to memory location that hold newly constructed mail slot logger object
 *  @result                 MA_OK on success
 *                          MA_ERROR_INVALIDARG on invalid argument passed
 *                          MA_ERROR_OUTOFMEMORY on memory allocation failure
 */
MA_LOGGER_API ma_error_t ma_mail_slot_logger_create(const char *slot, ma_mail_slot_logger_t **logger);


/** @brief mail slot logger destructor
 *  @param  [in] logger     handle
 *  @result                 MA_OK on success
 *                          MA_ERROR_INVALIDARG on invalid argument passed
 */
MA_LOGGER_API ma_error_t ma_mail_slot_logger_release(ma_mail_slot_logger_t *logger);


/** @brief sets mail slot logger policy
 *  @param  [in]    logger  handle
 *  @param  [in]    policy  pointer to memory location that holds mail slot logger policy
 *  @result         MA_OK on success
 *                  MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_LOGGER_API ma_error_t ma_mail_slot_logger_set_policy(ma_mail_slot_logger_t *logger , ma_mail_slot_logger_policy_t *policy);


/** @brief retrieves mail slot logger policy
 *  @param  [in]    logger  handle
 *  @param  [out]   policy  pointer to memory location that holds file logger policy
 *  @result         MA_OK on success
 *                  MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_LOGGER_API ma_error_t ma_mail_slot_logger_get_policy(ma_mail_slot_logger_t *logger , const ma_mail_slot_logger_policy_t **policy);

#include "ma/dispatcher/ma_mail_slot_logger_dispatcher.h"
MA_CPP(})

#endif /* MA__MAILSLOT_LOGGER_H_INCLUDED */
