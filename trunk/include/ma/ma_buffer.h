/** @file ma_buffer.h
 *  @brief Buffer Declarations
 *
 * This file contains prototypes for the buffer prototypes
 * Buffer represents structure for manipulating char strings
 * and wide character support manipulations.
 * The deletion and memory management will be done using the
 * reference counting technique with the count value in the buffer structure.
*/

#ifndef MA_BUFFER_H_INCLUDED
#define MA_BUFFER_H_INCLUDED

#include "ma/ma_common.h"

MA_CPP(extern "C" {)
/** @brief Opaque buffer structure
 */
typedef struct ma_buffer_s ma_buffer_t, *ma_buffer_h;

/** @brief creates a buffer object
 *  @param  [out]  self  buffer handle
 *  @param  [in]   size   buffer length
 *  @result              MA_OK on success
 *                       MA_ERROR_INVALIDARG on invalid argument passed
 *                       MA_ERROR_OUTOFMEMORY on memory allocation failure
 */
MA_VARIANT_API ma_error_t ma_buffer_create(ma_buffer_t **self, size_t size);

/** @brief Increments reference count of the given buffer object
 *  @param  [in]  self   buffer handler
 *  @result              MA_OK on success
 *                       MA_ERROR_INVALIDARG on invalid argument passed
 */
MA_VARIANT_API ma_error_t ma_buffer_add_ref(ma_buffer_t *self);

/** @brief Decrement reference count, if reference count reaches to
 *         0, it will deallocate buffer
 *  @param  [in]  self  buffer handle
 *  @result             MA_OK on success
 *                      MA_ERROR_INVALIDARG on invalid argument passed
 */
MA_VARIANT_API ma_error_t ma_buffer_release(ma_buffer_t	*self);


/** @brief Returns string from the input buffer
 *  @param [in]  self  buffer handle
 *  @param [out] value pointer to memory location that holds original string address
 *  @param [out] size  Pointer that holds size of string length
 *  @result            MA_OK on success
 *                     MA_ERROR_INVALIDARG on invalid argument passed
 */
MA_VARIANT_API ma_error_t ma_buffer_get_string(ma_buffer_t *self, const char **value, size_t *size);

/** @brief Returns raw buffer from the input buffer
 *  @param [in]  self  buffer handle
 *  @param [out] value Pointer to memory location that receives the raw buffer address
 *  @param [out] size  Pointer that holds size of raw buffer
 *  @result            MA_OK on success
 *                     MA_ERROR_INVALIDARG on invalid argument passed
 */
MA_VARIANT_API ma_error_t ma_buffer_get_raw(ma_buffer_t *self, const unsigned char **value, size_t *size);

/** @brief Sets the value of the buffer object
 *  @param  [in]  self           buffer handle
 *  @param  [in]  value          characetrs to store in the buffer
 *  @param  [in]  size			 number of bytes in the buffer
 *  @result                      MA_OK on success
 *                               MA_ERROR_INVALIDARG on invalid argument passed
 */
MA_VARIANT_API ma_error_t ma_buffer_set(ma_buffer_t *self, const char *value, size_t size);

/** @brief returns the size in bytes of the underlying buffer
 *  @param  [in]  self            buffer handle
 *  @param  [out] size			  size of the buffer
 *  @result                       MA_OK on success
 *                                MA_ERROR_INVALIDARG on invalid argument passed
 */
MA_VARIANT_API ma_error_t ma_buffer_size(ma_buffer_t *self, size_t *size);

/** @brief compares equality of input buffers
 *  @param  [in]  first   left buffer handle
 *  @param  [in]  second  right buffer handle
 *  @param  [out] result  pointer to the memory location that stores
 *                        comparision result
 *  @result               MA_OK on success
 *                        MA_ERROR_INVALIDARG on invalid argument passed
 */
MA_VARIANT_API ma_error_t ma_buffer_is_equal(const ma_buffer_t *first, const ma_buffer_t *second, ma_bool_t *result);

#ifdef MA_HAS_WCHAR_T
/** @brief wide char Buffer container
 */  
typedef struct ma_wbuffer_s ma_wbuffer_t, *ma_wbuffer_h;

/** @brief creates a wide char buffer object
 *  @param  [out]  self  wide char buffer handle
 *  @param  [in]   size   wide char string length
 *  @result              MA_OK on success
 *                       MA_ERROR_INVALIDARG on invalid argument passed
 *                       MA_ERROR_OUTOFMEMORY on memory allocation failure
 */
MA_VARIANT_API ma_error_t ma_wbuffer_create(ma_wbuffer_t **self, size_t size);

/** @brief Increments reference count of the given buffer object
 *  @param  [in]  self  buffer handle
 *  @result             MA_OK on success
 *                      MA_ERROR_INVALIDARG on invalid argument passed
 */
MA_VARIANT_API ma_error_t ma_wbuffer_add_ref(ma_wbuffer_t *self);

/** @brief Decrement reference count, if reference count reaches to
 *         0, it will deallocate the buffer
 *  @param  [in]  self  buffer handle
 *  @result             MA_OK on success otherwise an error code
 *                      MA_ERROR_INVALIDARG on invalid argument passed
 */
MA_VARIANT_API ma_error_t ma_wbuffer_release(ma_wbuffer_t *self);

/** @brief Sets the value of the wide charstring to buffer object
 *  @param  [in]  self            wide char buffer handle
 *  @param  [in]  value			  characetrs to store in the buffer
 *  @param  [in]  size            number of characters to copy
 *  @result                       MA_OK on success
 *                                MA_ERROR_INVALIDARG on invalid argument passed
 */
MA_VARIANT_API ma_error_t ma_wbuffer_set(ma_wbuffer_t* self, const wchar_t *value, size_t size);

/** @brief returns the size in bytes of the underlying wide char buffer
 *  @param  [in]  self             wide char buffer handle
 *  @param  [out] size			   pointer to hold the size of the buffer
 *  @result                        MA_OK on success
 *                                 MA_ERROR_INVALIDARG on invalid argument passed
 */
MA_VARIANT_API ma_error_t ma_wbuffer_size(ma_wbuffer_t *self, size_t *size);

/** @brief Returns wide char string from the input buffer
 *  @param [in]  self  buffer handle
 *  @param [out] value Pointer to memory location that receives the
 *                     wide char string
 *  @param [out] size  Pointer that holds size of string length
 *  @result            MA_OK on success
 *                     MA_ERROR_INVALIDARG on invalid argument passed
 */
MA_VARIANT_API ma_error_t ma_wbuffer_get_string(ma_wbuffer_t *self, const wchar_t **value, size_t *size);


/** @brief compares equality of input wide char buffers
 *  @param  [in]  first   left buffer handle
 *  @param  [in]  second  right buffer handle
 *  @param  [out] result  pointer to the memory location that stores
 *                        comparision result
 *  @result               MA_OK on success
 *                        MA_ERROR_INVALIDARG on invalid argument passed
 */
MA_VARIANT_API ma_error_t ma_wbuffer_is_equal(const ma_wbuffer_t *first, const ma_wbuffer_t *second, ma_bool_t *result);
#endif /* MA_HAS_WCHAR_T */


MA_CPP(})

#include "ma/dispatcher/ma_buffer_dispatcher.h"

#endif /* MA_BUFFER_H_INCLUDED */


