/** @file ma_info.h
 *  @brief Query information about Agent
 *
*/
#ifndef MA_INFO_H_INCLUDED
#define MA_INFO_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_variant.h"
#include "ma/ma_client.h"

MA_CPP(extern "C" {)

/* MA info event types, will be publised from MA */
#define MA_INFO_EVENT_PROPERTY_SENT_STR				"PropertySent"
#define MA_INFO_EVENT_POLICY_DOWNLOADED_STR			"PolicyDownloaded"
#define MA_INFO_EVENT_POLICY_ENFORCED_STR			"PolicyEnforced"
#define MA_INFO_EVENT_EVENTS_UPLOADED_STR			"EventsUploaded"
#define MA_INFO_EVENT_DC_MSG_AVAILABLE_STR			"DCMsgAvailable"
#define MA_INFO_EVENT_DC_MSG_UPLOADED_STR			"DCMsgUploaded"
#define MA_INFO_EVENT_AGENT_STARTED_STR				"AgentStarted"
#define MA_INFO_EVENT_AGENT_STOPED_STR				"AgentStoped"


/* Info type available from Agent */
#define MA_INFO_AGENT_MODE_STR							"AgentMode"  /* 0 - Unmanaged , 1 - Managed, 2 - Cloud managed */
#define MA_INFO_VERSION_STR								"Version"
#define MA_INFO_GUID_STR								"GUID"
#define MA_INFO_LOCALE_STR								"Locale"
#define MA_INFO_LOG_LOCATION_STR						"LogLocation"
#define MA_INFO_INSTALL_LOCATION_STR					"InstallLocation"
#define MA_INFO_CRYPTO_MODE_STR							"CryptoMode"
#define MA_INFO_DATA_LOCATION_STR						"DataLocation"
#define MA_INFO_EPO_SERVER_LIST							"EpoServerList"
#define MA_INFO_EPO_PORT_LIST							"EpoPortList"
#define MA_INFO_EPO_SERVER_LAST_USED_STR				"EpoServerLastUsed"
#define MA_INFO_EPO_PORT_LAST_USED_STR					"EpoPortLastUsed"
#define MA_INFO_LAST_ASC_TIME							"LastASCTime"
#define MA_INFO_LAST_POLICY_UPDATE_TIME					"LastPolicyUpdateTime"
#define MA_INFO_TENANTID_STR							"TenantId"
#define MA_INFO_EPO_VERSION_STR							"EpoVersion"
#define MA_INFO_ASC_INTERVAL_STR						"ASCInterval" 
#define MA_INFO_POLICY_ENFORCE_INTERVAL_STR				"PolicyEnforceInterval"
#define MA_INFO_EVENTS_UPLOAD_INTERVAL_STR				"EventsUploadInterval"
#define MA_INFO_IPADDRESS_STR							"IPAddress"
#define MA_INFO_HOSTNAME_STR							"HostName"


/**
* Callback for providing the properties
* @param ma_client              [in]            ma client object
* @param product_id             [in]            product_id
* @param info_event             [in]            info event type
* @param cb_data                [in,optional]   callback data as it was specified in 'ma_info_events_register_callback'
*
*/
typedef ma_error_t (*ma_info_events_cb_t)(ma_client_t *ma_client, const char *product_id, const char *info_event, void *cb_data) ;

/**
* Registers a info event susbcriber with ma, the provided callback will be called whenever an info event type is occured in MA 
* @param ma_client              [in]   ma client object
* @param product_id             [in]   product id
* @param info_events_cb         [in]   user's callback to be invoked at info event occurence
* @param cb_data                [in]   user's data, this will be provided in the callback. This is opaque to MA
* @result                              MA_OK on success
*                                      MA_ERROR_INVALIDARG on invalid argument passed
*                                      MA_ERROR_ALREADY_REGISTERED if already registered with the product id
* @remark product_id can be NULL, if we want to register for product id same as client
*/
MA_CLIENT_API ma_error_t ma_info_events_register_callback(ma_client_t *ma_client, const char *product_id, ma_info_events_cb_t info_events_cb, void *cb_data) ;


/**
* Un-registers a info event susbcriber with ma for the product id
* @param ma_client              [in]   ma client object
* @param product_id             [in]   product id of the provider for which properties are reported to server
* @result                              MA_OK on success
*                                      MA_ERROR_INVALIDARG on invalid argument passed
* @remark product_id can be NULL, if product id same as client
* 
*/
MA_CLIENT_API ma_error_t ma_info_events_unregister_callback(ma_client_t *ma_client, const char *product_id) ;


/**
 * Fetch a specific agent information
 * @param ma_client     [in]    MA client.
 * @param key	        [in]    info type
 * @param value        	[out]   value, returns on success
 * @result                      MA_OK on success.
 *                              MA_ERROR_INVALIDARG on invalid argument passed.
 * 
 * @remark  user can call this on demand to get the info, if this is called after the ma_info_events_cb user will get updated info.
 */
MA_CLIENT_API ma_error_t ma_info_get(ma_client_t *client, const char *key, ma_variant_t **value);


MA_CPP(})

#include "ma/dispatcher/ma_info_dispatcher.h"

#endif

