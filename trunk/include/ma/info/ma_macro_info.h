/** @file ma_macro_info.h
 *  @brief Getting the System information (Windows only)
 *
*/
#ifndef MA_MACRO_UTILS_H_INCLUDED
#define MA_MACRO_UTILS_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_variant.h"

#define MA_AGENT_MACRO_COMPUTERNAME                    "COMPUTER_NAME"
#define MA_AGENT_MACRO_USERNAME                        "USER_NAME"
#define MA_AGENT_MACRO_DOMAINNAME                      "DOMAIN_NAME"
#define MA_AGENT_MACRO_SYS_DRIVE                       "SYSTEM_DRIVE"                                                    /* e.g. C: */
#define MA_AGENT_MACRO_SYS_ROOT_DRIVE                  "SYSTEM_ROOT"                                                     /* e.g. C:\Winnt */
#define MA_AGENT_MACRO_SYS_DIR                         "SYSTEM_DIR"                                                      /* e.g. C:\Winnt\system32 */
#define MA_AGENT_MACRO_TEMP_DIR                        "TEMP_DIR"
#define MA_AGENT_MACRO_PROG_FILES_DIR                  "PROGRAM_FILES_DIR"                                               /* e.g. C:\Program Files */
#define MA_AGENT_MACRO_PROG_FILES_COMMON_DIR           "PROGRAM_FILES_COMMON_DIR"                                        /* e.g. C:\Program Files\Common */
#define MA_AGENT_MACRO_MY_DOCUMENTS                    "MY_DOCUMENTS"

MA_CPP(extern "C" {)

/**
* get agent info, where key as macro
* @param macro_name	        [in]			ma info macro
* @param macro_value	    [out]			macro value
* @result                   MA_OK on success.
*                           MA_ERROR_INVALIDARG on invalid argument passed.
*                           MA_ERROR_APIFAILED on registry operation failuer.
*
* @remark 
*/
MA_CLIENT_API ma_error_t ma_macro_info_get(const char *macro_name, ma_buffer_t **macro_value);

MA_CPP(})

#include "ma/dispatcher/ma_macro_info_dispatcher.h"

#endif /*MA_CLIENT_UTILS_H_INCLUDED*/