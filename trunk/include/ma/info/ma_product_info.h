/** @file ma_product_info.h
 *  @brief Getting the license information
 *
*/
#ifndef MA_PRODUCT_INFO_H_INCLUDED
#define MA_PRODUCT_INFO_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_buffer.h"
#include "ma/ma_client.h"

MA_CPP(extern "C" {)

/**
 * Gets product license information from MA.
 * @param client        [in]    MA client.
 * @param software_id   [in]    Software ID.
 * @param license_state	[out]   ma_buffer_t type license information to be returned to the caller on success. it must be released by ma_buffer_release API.
 * @result                      MA_OK on success.
 *                              MA_ERROR_LICENSE_STATE_NOT_FOUND if state information is not available.
 *                              MA_ERROR_INVALIDARG on invalid argument passed.
 * 
 * @remark  Clients can call this API to get the updated license information when client receives policy notification. 
 *          
 *
 */
MA_CLIENT_API ma_error_t ma_product_info_get_license_state(ma_client_t *client, const char *software_id, ma_buffer_t **license_state);

MA_CPP(})
#include "ma/dispatcher/ma_info_dispatcher.h"
#endif /* MA_PRODUCT_INFO_H_INCLUDED */


