#ifndef MA_DATASTORE_H_INCLUDED
#define MA_DATASTORE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_variant.h"

MA_CPP(extern "C" {)

typedef struct ma_ds_s  ma_ds_t, *ma_ds_h;

typedef struct ma_ds_iterator_s ma_ds_iterator_t, *ma_ds_iterator_h;


#define MA_DATASTORE_PATH_SEPARATOR        "\\"
#define MA_DATASTORE_PATH_C_SEPARATOR		'\\'

/* set and get int API's */
MA_DATASTORE_API ma_error_t ma_ds_set_int(ma_ds_t *datastore, const char *path, const char *key, ma_int64_t value);

MA_DATASTORE_API ma_error_t ma_ds_get_int16(ma_ds_t *datastore, const char *path, const char *key, ma_int16_t *value);

MA_DATASTORE_API ma_error_t ma_ds_get_int32(ma_ds_t *datastore, const char *path, const char *key, ma_int32_t *value);

MA_DATASTORE_API ma_error_t ma_ds_get_int64(ma_ds_t *datastore, const char *path, const char *key, ma_int64_t *value);

/* set and get string API's */

MA_DATASTORE_API ma_error_t ma_ds_set_str(ma_ds_t *datastore, const char *path, const char *key, const char *value, size_t size);

MA_DATASTORE_API ma_error_t ma_ds_get_str(ma_ds_t *datastore, const char *path, const char *key, ma_buffer_t **value);

#ifdef MA_HAS_WCHAR_T
MA_DATASTORE_API ma_error_t ma_ds_set_wstr(ma_ds_t *datastore, const char *path, const char *key, const ma_wchar_t *value, size_t size);

MA_DATASTORE_API ma_error_t ma_ds_get_wstr(ma_ds_t *datastore, const char *path, const char *key, ma_wbuffer_t **value);
#endif /* MA_HAS_WCHAR_T */

/* set and get blob API's */
MA_DATASTORE_API ma_error_t ma_ds_set_blob(ma_ds_t *datastore, const char *path, const char *key, const void *value, size_t size);

MA_DATASTORE_API ma_error_t ma_ds_get_blob(ma_ds_t *datastore, const char *path, const char *key, ma_buffer_t **value);

/* set and get variant API's */
MA_DATASTORE_API ma_error_t ma_ds_set_variant(ma_ds_t *datastore, const char *path, const char *key, ma_variant_t *value);

MA_DATASTORE_API ma_error_t ma_ds_get_variant(ma_ds_t *datastore, const char *path, const char *key, ma_vartype_t var_type, ma_variant_t **value);

/*Remove API's */
MA_DATASTORE_API ma_error_t ma_ds_rem(ma_ds_t *datastore, const char *path, const char *key, unsigned short need_recursive_remove);

/*Check Path exists */
MA_DATASTORE_API ma_error_t ma_ds_is_path_exists(ma_ds_t *datastore, const char *path , ma_bool_t *is_exists);

MA_DATASTORE_API ma_error_t ma_ds_is_key_exists(ma_ds_t *datastore, const char *path , const char *key, ma_bool_t *is_exists);


/*Iterator API's */
MA_DATASTORE_API ma_error_t ma_ds_iterator_create(ma_ds_t *datastore, ma_uint8_t is_path_iterator, const char *path, ma_ds_iterator_t **iterator);

MA_DATASTORE_API ma_error_t ma_ds_iterator_get_next(ma_ds_iterator_t *iterator, ma_buffer_t **buffer);

MA_DATASTORE_API ma_error_t ma_ds_iterator_release(ma_ds_iterator_t *iterator);


MA_CPP(})

#endif /* MA_DATASTORE_H_INCLUDED */
