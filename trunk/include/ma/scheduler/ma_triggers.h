/** @file ma_triggers.h
 *  @brief Triggers
 *
*/
#ifndef MA_TRIGGERS_H_INCLUDED
#define MA_TRIGGERS_H_INCLUDED

#include "ma/ma_common.h"

#define MA_SCHEDULER_INLINE_API MA_STATIC_INLINE
#define MA_NO_OF_DAYS_IN_WEEK       7
#define MA_NO_OF_HOURS_IN_DAY       24
#define MA_NO_OF_MINUTES_IN_HOUR    60
#define MA_NO_OF_SECONDS_IN_MINUTE  60
#define MA_MAX_SECONDS_IN_DAY       (24 * 60 * 60)
#define MA_MAX_SECONDS_IN_HOUR      (60 * 60)

MA_CPP(extern "C" {)

typedef struct ma_trigger_s ma_trigger_t, *ma_trigger_h;
typedef struct ma_task_time_s ma_task_time_t;

/* trigger life cycle state(s) */
typedef enum ma_trigger_state_e {
    MA_TRIGGER_STATE_NOT_STARTED = 1,
    MA_TRIGGER_STATE_RUNNING,
    MA_TRIGGER_STATE_BLOCKED,
    MA_TRIGGER_STATE_COMPLETE,
    MA_TRIGGER_STATE_EXPIRED,
}ma_trigger_state_t;

/* time defination */
struct ma_task_time_s {
    ma_uint16_t   year; /* year = YYYY format */
    ma_uint16_t   month; /* month 1 to 12 format */
    ma_uint16_t   day; /* days 1 to 31 format */
    ma_uint16_t   hour; /* hour 1 to 60 format */
    ma_uint16_t   minute;/* minutes */
    ma_uint16_t   secs; /* seconds */
};

/* week days */
typedef enum ma_day_of_week_e {
    SUNDAY = 0x01, /* week begin day - sunday */
    MONDAY = 0x02,
    TUESDAY = 0x04,
    WEDNESDAY = 0x08,
    THURSDAY = 0x10,
    FRIDAY = 0x20,
    SATURDAY = 0x40, /* week end day - saturday */
}ma_day_of_week_t;


/* weeks in month */
typedef enum ma_which_week_e {
    FIRST_WEEK = 1, /* first week */
    SECOND_WEEK, /* second week */
    THIRD_WEEK, /* third week */
    FOURTH_WEEK, /* fourth week */
    LAST_WEEK, /* last week */
}ma_which_week_t;


/* months of year */
typedef enum ma_month_of_year_e {
    JANUARY = 0x1, /* first month of the year */
    FEBRUARY = 0x2,
    MARCH = 0x4,
    APRIL = 0x8,
    MAY = 0x10,
    JUNE = 0x20,
    JULY = 0x40,
    AUGUST = 0x80,
    SEPTEMBER = 0x100,
    OCTOBER = 0x200,
    NOVEMBER = 0x400,
    DECEMBER = 0x800, /* last month of the year */
}ma_month_of_year_t;


/* trigger types */
typedef enum ma_trigger_type_e {
    MA_TRIGGER_DAILY = 0,
    MA_TRIGGER_WEEKLY,
    MA_TRIGGER_MONTHLY_DATE,
    MA_TRIGGER_ONCE,
    MA_TRIGGER_AT_SYSTEM_START,
    MA_TRIGGER_AT_LOGON,
    MA_TRIGGER_ON_SYSTEM_IDLE,
    MA_TRIGGER_NOW,
    MA_TRIGGER_ON_DIALUP,
    MA_TRIGGER_MONTHLY_DOW,
}ma_trigger_type_t;


/* run once trigger policy */
typedef struct ma_trigger_run_once_policy_s {
    ma_bool_t once; /* just once */
}ma_trigger_run_once_policy_t;


/* run immediately trigger policy */
typedef struct ma_trigger_run_now_policy_s {
    ma_bool_t now; /* immediate */
}ma_trigger_run_now_policy_t;


/* daily trigger policy */
typedef struct ma_trigger_daily_policy_s {
    ma_uint16_t days_interval; /* days intervals (1- 31) */
}ma_trigger_daily_policy_t;


/* weekly trigger policy */
typedef struct ma_trigger_weekly_policy_s {
    ma_uint16_t weeks_interval; /* weeks interval */
    ma_uint16_t days_of_the_week; /* week days (MONDAY | TUESDAY | WEDNESDAY | THURSDAY | FRIDAY | SATURDAY)*/
}ma_trigger_weekly_policy_t;


/* monthly date trigger policy */
typedef struct ma_trigger_monthly_date_policy_s {
    ma_uint16_t date; /* date (1 - 31) */
    ma_uint16_t months;/* months of the year (JANUARY | FEBRUARY | MARCH) */
}ma_trigger_monthly_date_policy_t;


/* monthly day of the week trigger policy */
typedef struct ma_trigger_monthly_dow_policy_s {
    ma_which_week_t which_week; /* week = FIRST/SECOND/THIRD/FOURTH/LAST */
    ma_day_of_week_t day_of_the_week; /* day of the week (MONDAY, TUESDAY, WEDNESDAY) */
    ma_uint16_t months; /* months of the year (JANUARY | FEBRUARY | MARCH) */
}ma_trigger_monthly_dow_policy_t;


/* system idle trigger policy */
typedef struct ma_trigger_system_idle_policy_s {
    ma_uint16_t idle_wait_minutes; /* idle wait minutes before trigger start */
}ma_trigger_system_idle_policy_t;


/* power save trigger policy */
typedef struct ma_trigger_powersave_policy_s {
    ma_uint16_t powersave_minutes;
    ma_uint16_t powersave_deadline_minutes;
}ma_trigger_powersave_policy_t;


/* dailup trigger policy */
typedef struct ma_trigger_dialup_policy_s {
    ma_bool_t daily_once; /* run once in a day, if event occurs */
}ma_trigger_dialup_policy_t;


/* system startup trigger policy */
typedef struct ma_trigger_systemstart_policy_s {
    ma_bool_t daily_once; /* run once in a day, if event occurs */
    ma_bool_t run_just_once; /* run once on system startup, if event occurs */
}ma_trigger_systemstart_policy_t;


/* logon trigger policy */
typedef struct ma_trigger_logon_policy_s {
    ma_bool_t daily_once; /* run once in a day, if event occurs */
}ma_trigger_logon_policy_t;




/* @brief creates run now(immediate) trigger 
 * @param policy        [in]    run now policy
 * @param trigger       [out]   pointer to memory location that stores constructed trigger
 * @result                      MA_ERROR_INVALIDARG on invalid argument passed
 *                              MA_OK on successful the reference count on the 
 *                              returned trigger will be incremented,
 *                              so the caller should release it when done
 *                              MA_ERROR_OUTOFMEMORY on task memory allocation failure
 */
MA_SCHEDULER_API ma_error_t ma_trigger_create_run_now(ma_trigger_run_now_policy_t const *policy, ma_trigger_t **trigger);



/* @brief creates run once(immediate) trigger 
 * @param policy        [in]    run once policy
 * @param trigger       [out]   pointer to memory location that stores constructed trigger
 * @result                      MA_ERROR_INVALIDARG on invalid argument passed
 *                              MA_OK on successful the reference count on the 
 *                              returned trigger will be incremented,
 *                              so the caller should release it when done
 *                              MA_ERROR_OUTOFMEMORY on task memory allocation failure
 */
MA_SCHEDULER_API ma_error_t ma_trigger_create_run_once(ma_trigger_run_once_policy_t *policy, ma_trigger_t **trigger);



/* @brief creates daily trigger 
 * @param policy        [in]    daily policy
 * @param trigger       [out]   pointer to memory location that stores constructed trigger
 * @result                      MA_ERROR_INVALIDARG on invalid argument passed
 *                              MA_OK on successful the reference count on the 
 *                              returned trigger will be incremented,
 *                              so the caller should release it when done
 *                              MA_ERROR_OUTOFMEMORY on task memory allocation failure
 */
MA_SCHEDULER_API ma_error_t ma_trigger_create_daily(ma_trigger_daily_policy_t *policy, ma_trigger_t **trigger);



/* @brief creates weekly trigger 
 * @param policy        [in]    weekly policy
 * @param trigger       [out]   pointer to memory location that stores constructed trigger
 * @result                      MA_ERROR_INVALIDARG on invalid argument passed
 *                              MA_OK on successful the reference count on the 
 *                              returned trigger will be incremented,
 *                              so the caller should release it when done
 *                              MA_ERROR_OUTOFMEMORY on task memory allocation failure
 */
MA_SCHEDULER_API ma_error_t ma_trigger_create_weekly(ma_trigger_weekly_policy_t *policy, ma_trigger_t **trigger);



/* @brief creates monthly date trigger 
 * @param policy        [in]    monthly date policy
 * @param trigger       [out]   pointer to memory location that stores constructed trigger
 * @result                      MA_ERROR_INVALIDARG on invalid argument passed
 *                              MA_OK on successful the reference count on the 
 *                              returned trigger will be incremented,
 *                              so the caller should release it when done
 *                              MA_ERROR_OUTOFMEMORY on task memory allocation failure
 */
MA_SCHEDULER_API ma_error_t ma_trigger_create_monthly_date(ma_trigger_monthly_date_policy_t *policy, ma_trigger_t **trigger);



/* @brief creates monthly day of week trigger 
 * @param policy        [in]    monthly day of week policy
 * @param trigger       [out]   pointer to memory location that stores constructed trigger
 * @result                      MA_ERROR_INVALIDARG on invalid argument passed
 *                              MA_OK on successful the reference count on the 
 *                              returned trigger will be incremented,
 *                              so the caller should release it when done
 *                              MA_ERROR_OUTOFMEMORY on task memory allocation failure
 */
MA_SCHEDULER_API ma_error_t ma_trigger_create_monthly_dow(ma_trigger_monthly_dow_policy_t *policy, ma_trigger_t **trigger);



/* @brief creates system idle trigger 
 * @param policy        [in]    system idle policy
 * @param trigger       [out]   pointer to memory location that stores constructed trigger
 * @result                      MA_ERROR_INVALIDARG on invalid argument passed
 *                              MA_OK on successful the reference count on the 
 *                              returned trigger will be incremented,
 *                              so the caller should release it when done
 *                              MA_ERROR_OUTOFMEMORY on task memory allocation failure
 */
MA_SCHEDULER_API ma_error_t ma_trigger_create_system_idle(ma_trigger_system_idle_policy_t *policy, ma_trigger_t **trigger);



/* @brief creates dialup trigger 
 * @param policy        [in]    dailup policy
 * @param trigger       [out]   pointer to memory location that stores constructed trigger
 * @result                      MA_ERROR_INVALIDARG on invalid argument passed
 *                              MA_OK on successful the reference count on the 
 *                              returned trigger will be incremented,
 *                              so the caller should release it when done
 *                              MA_ERROR_OUTOFMEMORY on task memory allocation failure
 */
MA_SCHEDULER_API ma_error_t ma_trigger_create_dialup(ma_trigger_dialup_policy_t *policy, ma_trigger_t **trigger);



/* @brief creates system startup trigger 
 * @param policy        [in]    system startup policy
 * @param trigger       [out]   pointer to memory location that stores constructed trigger
 * @result                      MA_ERROR_INVALIDARG on invalid argument passed
 *                              MA_OK on successful the reference count on the 
 *                              returned trigger will be incremented,
 *                              so the caller should release it when done
 *                              MA_ERROR_OUTOFMEMORY on task memory allocation failure
 */
MA_SCHEDULER_API ma_error_t ma_trigger_create_system_start(ma_trigger_systemstart_policy_t *policy, ma_trigger_t **trigger);



/* @brief creates logon trigger 
 * @param policy        [in]    logon policy
 * @param trigger       [out]   pointer to memory location that stores constructed trigger
 * @result                      MA_ERROR_INVALIDARG on invalid argument passed
 *                              MA_OK on successful the reference count on the 
 *                              returned trigger will be incremented,
 *                              so the caller should release it when done
 *                              MA_ERROR_OUTOFMEMORY on task memory allocation failure
 */
MA_SCHEDULER_API ma_error_t ma_trigger_create_logon(ma_trigger_logon_policy_t *policy, ma_trigger_t **trigger);



/* @brief releases trigger
 * @param trigger       [in]    trigger handle
 * @result                      MA_ERROR_INVALIDARG on invalid argument passed
 *                              MA_OK on successful the reference count on the 
 *                              released trigger will be decremented on reaching to 0 it will frees trigger memory
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_trigger_release(ma_trigger_t *trigger);




/* @brief sets trigger begin date
 * @param trigger       [in]    trigger handle
 * @param date          [in]    date
 * @result                      MA_ERROR_INVALIDARG on invalid argument passed
 *                              MA_OK on successful
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_trigger_set_begin_date(ma_trigger_t *trigger, ma_task_time_t *date);



/* @brief sets trigger end date
 * @param trigger       [in]    trigger handle
 * @param date          [in]    date
 * @result                      MA_ERROR_INVALIDARG on invalid argument passed
 *                              MA_OK on successful
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_trigger_set_end_date(ma_trigger_t *trigger, ma_task_time_t *date);



/* @brief retrieves trigger type
 * @param trigger       [in]    trigger handle
 * @param type          [out]   pointer to memory location that store trigger type
 * @result                      MA_ERROR_INVALIDARG on invalid argument passed
 *                              MA_OK on successful
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_trigger_get_type(ma_trigger_t *trigger, ma_trigger_type_t *type);



/* @brief retrieves trigger state
 * @param trigger       [in]    trigger handle
 * @param state         [out]   pointer to memory location that store trigger state
 * @result                      MA_ERROR_INVALIDARG on invalid argument passed
 *                              MA_OK on successful
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_trigger_get_state(ma_trigger_t *trigger, ma_trigger_state_t *state);



/* @brief retrieves trigger id
 * @param trigger       [in]    trigger handle
 * @param id            [out]   pointer to memory location that store trigger id
 * @result                      MA_ERROR_INVALIDARG on invalid argument passed
 *                              MA_OK on successful
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_trigger_get_id(ma_trigger_t *trigger, const char **id);



/* @brief retrieves trigger begin date
 * @param trigger       [in]    trigger handle
 * @param date          [out]   pointer to memory location that store trigger begin date
 * @result                      MA_ERROR_INVALIDARG on invalid argument passed
 *                              MA_OK on successful
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_trigger_get_begin_date(ma_trigger_t *trigger, ma_task_time_t *date);



/* @brief retrieves trigger end date
 * @param trigger       [in]    trigger handle
 * @param date          [out]   pointer to memory location that store trigger end date
 * @result                      MA_ERROR_INVALIDARG on invalid argument passed
 *                              MA_OK on successful
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_trigger_get_end_date(ma_trigger_t *trigger, ma_task_time_t *end_date);



/* @brief copies trigger policy based on trigger type
 * @param trigger       [in]    trigger handle
 * @param date          [out]   pointer to memory location that copies trigger policy
 * @result                      MA_ERROR_INVALIDARG on invalid argument passed
 *                              MA_OK on successful copy of trigger policy into policy variable passed
 *                              user is expected to pass exact trigger type policy variable for safe copy
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_trigger_get_policy(ma_trigger_t *trigger, void *policy);

MA_CPP(})

#include "ma/scheduler/ma_itriggers.h"

#include "ma/dispatcher/ma_trigger_dispatcher.h"
#endif // MA_TRIGGERS_H_INCLUDED


