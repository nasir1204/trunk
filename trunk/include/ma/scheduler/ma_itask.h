#ifndef MA_ITASK_H_INCLUDED
#define MA_ITASK_H_INCLUDED

#include "ma/ma_common.h"

#define MA_LOCAL_ZONE_STR "LOCAL"
#define MA_UTC_ZONE_STR "UTC"

MA_CPP(extern "C"{)
typedef struct ma_task_methods_s ma_task_methods_t;

#undef MA_TASK_ACTUAL_ARG_EXPANDER
#define MA_TASK_ACTUAL_ARG_EXPANDER\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_release,(ma_task_t *task),(task))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_set_setting,(ma_task_t *task, const char *path , const char *name, ma_variant_t *value),(task, path, name, value))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_set_retry_policy,(ma_task_t *task, ma_task_retry_policy_t policy),(task, policy))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_set_app_payload,(ma_task_t *task, ma_variant_t *app_payload),(task, app_payload))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_set_repeat_policy,(ma_task_t *task, ma_task_repeat_policy_t policy),(task, policy))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_set_max_run_time,(ma_task_t *task, ma_uint16_t max_run_time),(task, max_run_time))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_set_max_run_time_limit,(ma_task_t *task, ma_uint16_t max_run_time_limit),(task, max_run_time_limit))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_set_randomize_policy,(ma_task_t *task, ma_task_randomize_policy_t policy),(task, policy))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_set_priority,(ma_task_t *task, ma_task_priority_t priority),(task, priority))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_set_conditions,(ma_task_t *task, ma_uint32_t conds),(task, conds))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_set_execution_status,(ma_task_t *task, ma_task_exec_status_t status),(task, status))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_set_delay_policy,(ma_task_t *task, ma_task_delay_policy_t policy),(task, policy))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_set_missed_policy,(ma_task_t *task, ma_task_missed_policy_t policy),(task, policy))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_set_creator_id,(ma_task_t *task, const char *id),(task, id))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_get_state,(ma_task_t *task, ma_task_state_t *state),(task, state))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_get_last_run_time,(ma_task_t *task, ma_task_time_t *last_run_time),(task, last_run_time))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_get_next_run_time,(ma_task_t *task, ma_task_time_t *next_run_time),(task, next_run_time))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_get_app_payload,(ma_task_t *task, ma_variant_t **app_payload),(task, app_payload))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_get_missed_policy,(ma_task_t *task, ma_task_missed_policy_t *policy),(task, policy))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_get_repeat_policy,(ma_task_t *task, ma_task_repeat_policy_t *policy),(task, policy))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_get_priority,(ma_task_t *task, ma_task_priority_t *priority),(task, priority))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_get_conditions,(ma_task_t *task, ma_uint32_t *conds),(task, conds))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_get_id,(ma_task_t *task, const char **task_id),(task, task_id))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_get_name,(ma_task_t *task, const char **task_name),(task, task_name))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_get_type,(ma_task_t *task, const char **task_type),(task, task_type))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_get_creator_id,(ma_task_t *task, const char **creator_id),(task, creator_id))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_get_retry_policy,(ma_task_t *task, ma_task_retry_policy_t *policy),(task, policy))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_get_max_run_time,(ma_task_t *task, ma_uint16_t *max_run_time),(task, max_run_time))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_get_max_run_time_limit,(ma_task_t *task, ma_uint16_t *max_run_time_limit),(task, max_run_time_limit))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_get_execution_status,(ma_task_t *task, ma_task_exec_status_t *status),(task, status))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_add_trigger,(ma_task_t *task, ma_trigger_t *trigger),(task, trigger))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_remove_trigger,(ma_task_t *task, const char *trigger_id),(task, trigger_id))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_find_trigger,(ma_task_t *task, const char *trigger_id, ma_trigger_t **trigger),(task, trigger_id, trigger))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_set_name,(ma_task_t *task, const char *name),(task, name))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_set_type,(ma_task_t *task, const char *type),(task, type))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_set_software_id,(ma_task_t *task, const char *id),(task, id))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_get_software_id,(ma_task_t *task, const char **id),(task, id))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_get_setting,(ma_task_t *task, const char *path , const char *name, ma_variant_t **value),(task,path,name,value))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_set_state,(ma_task_t *task, ma_task_state_t state),(task, state))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_clear_setting,(ma_task_t *task),(task))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_set_next_run_time,(ma_task_t *task, ma_task_time_t time),(task, time))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_set_last_run_time,(ma_task_t *task, ma_task_time_t time),(task, time))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_set_time_zone,(ma_task_t *task, ma_time_zone_t zone),(task, zone))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_get_time_zone,(ma_task_t *task, ma_time_zone_t *zone),(task, zone))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_copy,(const ma_task_t *task, ma_task_t **copy),(task, copy))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_set_id,(ma_task_t *task, const char *task_id),(task, task_id))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_get_trigger_list,(ma_task_t *task, ma_trigger_list_t **tl),(task, tl))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_get_randomize_policy,(ma_task_t *task, ma_task_randomize_policy_t *policy), (task, policy))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_get_delay_policy,(ma_task_t *task, ma_task_delay_policy_t *policy),(task, policy))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_dispose_trigger,(ma_task_t *task, ma_trigger_t *trigger),(task, trigger))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_size,(ma_task_t *task, size_t *no_of_triggers),(task, no_of_triggers))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_add_ref,(ma_task_t *task),(task))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_unset_condition,(ma_task_t *task, ma_task_cond_t cond), (task, cond))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_set_condition,(ma_task_t *task, ma_task_cond_t cond),(task, cond))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_set_power_policy,(ma_task_t *task, ma_task_power_policy_t policy),(task, policy))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_get_power_policy,(ma_task_t *task, ma_task_power_policy_t *policy),(task, policy))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_set_missed,(ma_task_t *task, ma_bool_t missed),(task, missed))\
    MA_TASK_ARG_EXPANDER(ma_error_t,ma_task_get_missed,(ma_task_t *task, ma_bool_t *missed),(task, missed))

struct ma_task_s {
    const ma_task_methods_t *methods;
    void *data;
};

static MA_INLINE void ma_task_init(ma_task_t *task, const ma_task_methods_t *methods, void *data) {
    if(task) {
        task->methods = methods;
        task->data = data;
    }
}

struct ma_task_methods_s {
#define MA_TASK_ARG_EXPANDER(ret_type, function, signature, arg) ret_type (*function##_fn) signature;
    MA_TASK_ACTUAL_ARG_EXPANDER
#undef MA_TASK_ARG_EXPANDER
};

#define MA_TASK_ARG_EXPANDER(ret_type, function, signature, arg)\
    MA_SCHEDULER_INLINE_API ret_type function signature {\
        if(task && task->methods) {\
            return task->methods->function##_fn?task->methods->function##_fn arg:MA_ERROR_PRECONDITION;\
        }\
        return MA_ERROR_INVALIDARG;\
    }
    MA_TASK_ACTUAL_ARG_EXPANDER
#undef MA_TASK_ARG_EXPANDER

MA_CPP(})

#endif // MA_ITASK_H_INCLUDED


