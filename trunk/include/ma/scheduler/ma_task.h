/** @file ma_task.h
 *  @brief Task
 *
*/
#ifndef MA_TASK_H_INCLUDED
#define MA_TASK_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/scheduler/ma_triggers.h"
#include "ma/scheduler/ma_triggerlist.h"
#include "ma/ma_variant.h"

#define MA_SCHEDULER_INLINE_API MA_STATIC_INLINE

/* task execution status topic */
#define MA_TASK_EXEC_NOT_STARTED_TOPIC "ma.task.exec.not.started"
#define MA_TASK_EXEC_RUNNING_TOPIC "ma.task.exec.running"
#define MA_TASK_EXEC_FAILED_TOPIC "ma.task.exec.failed"
#define MA_TASK_EXEC_SUCCESS_TOPIC "ma.task.exec.success"
#define MA_TASK_EXEC_STOP_INITIATED_BY_SCHEDULER_TOPIC "ma.task.exec.stop.initiated.by.scheduler"
#define MA_TASK_EXEC_STOPPED_TOPIC "ma.task.exec.stopped"

MA_CPP(extern "C" {)
typedef struct ma_task_s ma_task_t, *ma_task_h;

/* task conditions */
typedef enum ma_task_cond_e {
    MA_TASK_COND_INTERACTIVE = 0x01, /* default interactive task */
    MA_TASK_COND_DELETE_WHEN_DONE = 0x02, /* deletes the task on completion(no more runs pending) */
    MA_TASK_COND_DISABLED = 0x04, /* disables task */
    MA_TASK_COND_START_ONLY_IF_IDLE = 0x08,
    MA_TASK_COND_KILL_ON_IDLE_END = 0x10,
    MA_TASK_COND_DONT_START_IF_ON_BATTERIES = 0x20,
    MA_TASK_COND_STOP_IF_GOING_ON_BATTERIES = 0x40,
    MA_TASK_COND_RUN_ONLY_IF_DOCKED = 0x80,
    MA_TASK_COND_HIDDEN = 0x100, /* hiddens task for enumeration */
    MA_TASK_COND_RUN_IF_CONNECTED_TO_INTERNET = 0x200,
    MA_TASK_COND_RESTART_ON_IDLE_RESUME = 0x400,
    MA_TASK_COND_SYSTEM_REQUIRED = 0x800, /* run task on local m/c  */
    MA_TASK_COND_ENFORCE_POWER_POLICY = 0x1000,
    MA_TASK_COND_NOTIFY_ON_EXEC_STATUS_CHANGE = 0x2000, /* publish on task execution status change */
    MA_TASK_COND_RUN_AT_SYSTEM_STARTUP = 0x4000, /* run task at system startup */
    MA_TASK_COND_RUN_ON_LOGON = 0x8000, /* run task on any user logon */
    MA_TASK_COND_ADJUST_BEGIN_DATE_ON_SYSTEM_TIME_CHANGE = 0x10000, 
}ma_task_cond_t;


/* task life cycle state(s) */
typedef enum ma_task_state_e {
    MA_TASK_STATE_NOT_SCHEDULED = 1,
    MA_TASK_STATE_RUNNING,
    MA_TASK_STATE_NO_TRIGGER,
    MA_TASK_STATE_NO_TRIGGER_TIME,
    MA_TASK_STATE_NO_MORE_RUN,
    MA_TASK_STATE_NEVER_RUN,
    MA_TASK_STATE_EXPIRED,
}ma_task_state_t;


/* time zones */
typedef enum ma_time_zone_e {
    MA_TIME_ZONE_LOCAL,
    MA_TIME_ZONE_UTC,
}ma_time_zone_t;

#undef MA_TASK_EXEC_STATUS_EXPANDER
#define MA_TASK_EXEC_STATUS_EXPANDER\
    MA_TASK_EXEC_EXPANDER(MA_TASK_EXEC_NOT_STARTED, 1, MA_TASK_EXEC_NOT_STARTED_TOPIC)\
    MA_TASK_EXEC_EXPANDER(MA_TASK_EXEC_RUNNING, 2, MA_TASK_EXEC_RUNNING_TOPIC)\
    MA_TASK_EXEC_EXPANDER(MA_TASK_EXEC_FAILED, 3, MA_TASK_EXEC_FAILED_TOPIC)\
    MA_TASK_EXEC_EXPANDER(MA_TASK_EXEC_SUCCESS,4, MA_TASK_EXEC_SUCCESS_TOPIC)\
    MA_TASK_EXEC_EXPANDER(MA_TASK_EXEC_STOP_INITIATED_BY_SCHEDULER, 5, MA_TASK_EXEC_STOP_INITIATED_BY_SCHEDULER_TOPIC)\
    MA_TASK_EXEC_EXPANDER(MA_TASK_EXEC_STOPPED, 6, MA_TASK_EXEC_STOPPED_TOPIC)


/* task execution life cycle state(s) */
typedef enum ma_task_exec_status_e {
#define MA_TASK_EXEC_EXPANDER(status, index, status_str) status = index,
    MA_TASK_EXEC_STATUS_EXPANDER
#undef MA_TASK_EXEC_EXPANDER
}ma_task_exec_status_t;


/* intervals setting */
typedef struct ma_task_interval_s {
    ma_uint16_t hour; /* 24 hour format */
    ma_uint16_t minute;
}ma_task_interval_t;


/* task priority */
typedef enum ma_task_priority_e {
    MA_TASK_PRIORITY_LOW = 1,
    MA_TASK_PRIORITY_NORMAL,
    MA_TASK_PRIORITY_HIGH,
}ma_task_priority_t;



/* missed policy */
typedef struct ma_task_missed_policy_s {
    ma_task_interval_t missed_duration; /* missed duration */
}ma_task_missed_policy_t;

/* repeat policy */
typedef struct ma_task_repeat_policy_s {
    ma_task_interval_t repeat_interval; /* repeat interval */
    ma_task_interval_t repeat_until; /* until duration */
}ma_task_repeat_policy_t;

/* delay policy */
typedef struct ma_task_delay_policy_s {
    ma_task_interval_t delay_time; /* delay duration */
}ma_task_delay_policy_t;

/* retry policy */
typedef struct ma_task_retry_policy_s {
    int retry_count; /* error retry count */
    ma_task_interval_t retry_interval; /* error retry intervals */
}ma_task_retry_policy_t;

/* randomize policy */
typedef struct ma_task_randomize_policy_s {
    ma_task_interval_t random_interval; /* randomize intervals */
}ma_task_randomize_policy_t;

/* power supply policy */
typedef struct ma_task_power_policy_s {
    ma_bool_t on_battery; /* ON = battery, OFF = AC */
    size_t battery_life_time; /* battery life time in minutes */
}ma_task_power_policy_t;


/*
 * @brief creates new task with given task id, if task id is NULL then it will generate unique ID.
 * @param task_id       [in]    supplied task id
 * @param task          [out]   pointer to newly allocated task memory
 * @result                      MA_ERROR_INVALIDARG on invalid argument passed
 *                              MA_OK on successful the reference count on the 
 *                              returned task will be incremented,
 *                              so the caller should release it when done
 *                              MA_ERROR_OUTOFMEMORY on task memory allocation failure
 */
MA_SCHEDULER_API ma_error_t ma_task_create(const char *task_id, ma_task_t **task);


/* @brief decrements the reference count of task, if task reference count reaches to 0 then it will frees task memory
 * @param task  [in]    task handle
 * @result              MA_OK on successful
 *                      MA_ERROR_INVALIDARG on invalid argument passed
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_task_release(ma_task_t *task);



/* @brief sets applications settings into task payload
 * @param task      [in]    task handle
 * @param path      [in]    section
 * @param name      [in]    key
 * @param value     [in]    variant
 * @result                  MA_OK on successful
 *                          MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_task_set_setting(ma_task_t *task, const char *path , const char *name, ma_variant_t *value);


/* @brief sets task retry policy, it will override default task retry policy(default retry interval is 5mins and retry count 3)
 * retries applied on task execution failure it should be treated as "error or execution failure retries"
 * @param task      [in]        task handle
 * @param policy    [in]        retry policy
 * @result                      MA_OK on successful
 *                              MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_task_set_retry_policy(ma_task_t *task, ma_task_retry_policy_t policy);


/* @brief sets task payload and increments variant reference count
 * @param task      [in]        task handle
 * @param payload   [in]        variant as payload
 * @result                      MA_OK on successful
 *                              MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_task_set_app_payload(ma_task_t *task, ma_variant_t *payload);


/* @brief sets task repeat policy this will allow task to run for the until duration and then repeat for the number of intervals
 * @limitation until duration should never be more than 24hrs and intervals should <= duration and for run once task this policy is not applicable
 * @param task      [in]        task handle
 * @param policy    [in]        repeat policy
 * @result                      MA_OK on successful
 *                              MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_task_set_repeat_policy(ma_task_t *task, ma_task_repeat_policy_t policy);


/* @brief sets maximum duration task should run in minutes, it will override default task maximum run time of 24hrs
 * @param task          [in]    task handle
 * @param max_run_time  [in]    max task run duration(> 0 minutes)
 * @result                      MA_OK on successful
 *                              MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_task_set_max_run_time(ma_task_t *task, ma_uint16_t max_run_time);


/* @brief sets limit on maximum task run duration
 * @param task                  [in]    task handle
 * @param max_run_time_limit    [in]    max task run duration limit(> 0 minutes)
 * @result                      MA_OK on successful
 *                              MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_task_set_max_run_time_limit(ma_task_t *task, ma_uint16_t max_run_time_limit);



/* @brief randomize task execution begin for the given limit
 * @param task          [in]    task handle
 * @param policy        [in]    randomize policy
 * @result                      MA_OK on successful
 *                              MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_task_set_randomize_policy(ma_task_t *task, ma_task_randomize_policy_t policy);



/* @brief sets task priority(low, medium and high)
 * @param task          [in]    task handle
 * @param priority      [in]    priority
 * @result                      MA_OK on successful
 *                              MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_task_set_priority(ma_task_t *task, ma_task_priority_t priority);



/* @brief sets task conditions and these conditions are used as "actions" to determines whether task should run or not and other purposes
 * @param task          [in]    task handle
 * @param conds         [in]    conditions
 * @result                      MA_OK on successful
 *                              MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_task_set_conditions(ma_task_t *task, ma_uint32_t conds);



/* @brief task executor can update the task executions status
 * @param task          [in]    task handle
 * @param status        [in]    executed status(failed or successful)
 * @result                      MA_OK on successful
 *                              MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_task_set_execution_status(ma_task_t *task, ma_task_exec_status_t status);



/* @brief delays task execution
 * @param task          [in]    task handle
 * @param policy        [in]    delay policy
 * @result                      MA_OK on successful
 *                              MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_task_set_delay_policy(ma_task_t *task, ma_task_delay_policy_t policy);




/* @brief sets task missed policy overrides default missed policy, this is applicable and used to schedule task which got missed before agent went down
 * @param task          [in]    task handle
 * @param policy        [in]    missed policy
 * @result                      MA_OK on successful
 *                              MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_task_set_missed_policy(ma_task_t *task, ma_task_missed_policy_t policy);




/* @brief sets task creator
 * @param task          [in]    task handle
 * @param id            [in]    creator
 * @result                      MA_OK on successful
 *                              MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_task_set_creator_id(ma_task_t *task, const char *id);



/* @brief sets product to which task belongs
 * @param task          [in]    task handle
 * @param id            [in]    product id
 * @result                      MA_OK on successful
 *                              MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_task_set_software_id(ma_task_t *task, const char *id);



/* @brief sets task name
 * @param task          [in]    task handle
 * @param name          [in]    name
 * @result                      MA_OK on successful
 *                              MA_ERROR_INVALIDARG on invalid arguments passed
 */ 
MA_SCHEDULER_INLINE_API ma_error_t ma_task_set_name(ma_task_t *task, const char *name);



/* @brief sets task type
 * @param task          [in]    task handle
 * @param type          [in]    type
 * @result                      MA_OK on successful
 *                              MA_ERROR_INVALIDARG on invalid arguments passed
 */ 
MA_SCHEDULER_INLINE_API ma_error_t ma_task_set_type(ma_task_t *task, const char *type);




/* "get only" task properties */
/* @brief retrieves task current state
 * @param task          [in]    task handle
 * @param state         [out]   pointer to memory that holds current task state
 * @result                      MA_OK on successful
 *                              MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_task_get_state(ma_task_t *task, ma_task_state_t *state);



/* @brief retrieves task last run time
 * @param task          [in]    task handle
 * @param last_run_time [in]    pointer to memory location that holds task last run time
 * @result                      MA_OK on successful
 *                              MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_task_get_last_run_time(ma_task_t *task, ma_task_time_t *last_run_time);



/* @brief retrieves task next run time
 * @param task          [in]    task handle
 * @param last_run_time [in]    pointer to memory location that holds task next run time
 * @result                      MA_OK on successful
 *                              MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_task_get_next_run_time(ma_task_t *task, ma_task_time_t *next_run_time);



/* @brief retrieves task payload
 * @param task          [in]    task handle
 * @param payload       [out]   pointer to memory location that holds the task payload
 * @result                      MA_ERROR_INVALIDARG on invalid arguments passed 
 *                              MA_OK on successful the reference count on the 
 *                              returned payload will be incremented,
 *                              so the caller should release it when done
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_task_get_app_payload(ma_task_t *task, ma_variant_t **payload);



/* @brief retrieves task missed policy
 * @param task          [in]    task handle
 * @param policy        [out]   pointer to memory location that stores task missed policy details
 * @result                      MA_OK on successful
 *                              MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_task_get_missed_policy(ma_task_t *task, ma_task_missed_policy_t *policy);



/* @brief retrieves task repeat policy
 * @param task          [in]    task handle
 * @param policy        [out]   pointer to memory location that stores task repeat policy details
 * @result                      MA_OK on successful
 *                              MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_task_get_repeat_policy(ma_task_t *task, ma_task_repeat_policy_t *policy);



/* @brief retrieves task priority
 * @param task          [in]    task handle
 * @param priority      [out]   pointer to memory location that holds task priority
 * @result                      MA_OK on successful
 *                              MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_task_get_priority(ma_task_t *task, ma_task_priority_t *priority);



/* @brief retrieves task conditions
 * @param task          [in]    task handle
 * @param conds         [out]   pointer to memory location that holds task conditions
 * @result                      MA_OK on successful
 *                              MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_task_get_conditions(ma_task_t *task, ma_uint32_t *conds);



/* @brief retrieves task id
 * @param task          [in]    task handle
 * @param task_id       [out]   pointer to memory location that holds task id
 * @result                      MA_OK on successful
 *                              MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_task_get_id(ma_task_t *task, const char **task_id);



/* @brief retrieves task name
 * @param task          [in]    task handle
 * @param name          [out]   pointer to memory location that hold task name
 * @result                      MA_OK on successful
 *                              MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_task_get_name(ma_task_t *task, const char **name);



/* @brief retrieves task type
 * @param task          [in]    task handle
 * @param type          [out]   pointer to memory location that hold task type
 * @result                      MA_OK on successful
 *                              MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_task_get_type(ma_task_t *task, const char **type);



/* @brief retrieves task creator id
 * @param task          [in]    task handle
 * @param id            [out]   pointer to memory location that holds task creator id
 * @result                      MA_OK on successful
 *                              MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_task_get_creator_id(ma_task_t *task, const char **id);



/* @brief retrieves task retry policy
 * @param task          [in]    task handle
 * @param policy        [out]   pointer to memory location that stores task retry policy details
 * @result                      MA_OK on successful
 *                              MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_task_get_retry_policy(ma_task_t *task, ma_task_retry_policy_t *policy);



/* @brief retrieves task max run time
 * @param task          [in]    task handle
 * @param max_run_time  [out]   pointer to memory location that holds task max run time
 * @result                      MA_OK on successful
 *                              MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_task_get_max_run_time(ma_task_t *task, ma_uint16_t *max_run_time);



/* @brief retrieves task max run time limit
 * @param task                  [in]    task handle
 * @param max_run_time_limit    [out]   pointer to memory location that holds task max run time limit
 * @result                              MA_OK on successful
 *                                      MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_task_get_max_run_time_limit(ma_task_t *task, ma_uint16_t *max_run_time_limit);



/* @brief retrieves task executions status
 * @param task          [in]    task handle
 * @param status        [out]   pointer to memory location that holds task execution status
 * @result                      MA_OK on successful
 *                              MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_task_get_execution_status(ma_task_t *task, ma_task_exec_status_t *status);



/* @brief retrieves task trigger list handle
 * @param task          [in]    task handle
 * @param tl            [out]   pointer to memory location that holds task trigger list handle
 * @result                      MA_OK on successful
 *                              MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_task_get_trigger_list(ma_task_t *task, ma_trigger_list_t **tl);



/* @brief retrieves task randomize policy
 * @param task          [in]    task handle
 * @param policy        [out]   pointer to memory location that stores task randomize policy details
 * @result                      MA_OK on successful
 *                              MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_task_get_randomize_policy(ma_task_t *task, ma_task_randomize_policy_t *policy);




/* @brief retrieves task delay policy
 * @param task          [in]    task handle
 * @param policy        [out]   pointer to memory location that stores task delay policy details
 * @result                      MA_OK on successful
 *                              MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_task_get_delay_policy(ma_task_t *task, ma_task_delay_policy_t *policy);



/* @brief adds trigger into task and task will issue unique id to trigger
 * @param task          [in]    task handle
 * @param trigger       [in]    trigger handle
 * @result                      MA_OK on successful and increments the reference count of the added trigger
 *                              MA_ERROR_INVALIDARG on invalid arguments passed
 *                              MA_ERROR_SCHEDULER_INVALID_TRIGGER_ID on invalid trigger
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_task_add_trigger(ma_task_t *task, ma_trigger_t *trigger);



/* @brief removes trigger by id from the task
 * @param task          [in]    task handle
 * @param id            [in]    trigger id
 * @result                      MA_OK on successful and decrements the reference count of the removed trigger
 *                              MA_ERROR_INVALIDARG on invalid arguments passed
 *                              MA_ERROR_SCHEDULER_INVALID_TRIGGER_ID on invalid trigger
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_task_remove_trigger(ma_task_t *task, const char *id);



/* @brief finds trigger by id from the task
 * @param task          [in]    task handle
 * @param id            [in]    trigger id
 * @param trigger       [out]   pointer to memory location that holds trigger
 * @result                      MA_OK on successful and increments the reference count of the found trigger
 *                              MA_ERROR_INVALIDARG on invalid arguments passed
 *                              MA_ERROR_SCHEDULER_INVALID_TRIGGER_ID on invalid trigger
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_task_find_trigger(ma_task_t *task, const char *id, ma_trigger_t **trigger);



/* @brief retrieves product id from the task
 * @param task          [in]    task handle
 * @param id            [in]    product id
 * @result                      MA_OK on successful
 *                              MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_task_get_software_id(ma_task_t *task, const char **id);



/* @brief retrieves specific task settings
 * @param task          [in]    task handle
 * @param path          [in]    section
 * @param name          [in]    key
 * @param value         [out]   pointer to memory location that holds variant
 * @result                      MA_OK on successful and increments reference count of returned variant
 *                              MA_ERROR_INVALIDARG on invalid arguments passed
 *                              MA_ERROR_OBJECTNOTFOUND on invalid key
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_task_get_setting(ma_task_t *task, const char *path , const char *name, ma_variant_t **value);



/* @brief erase task settings
 * @param task          [in]    task handle
 * @result                      MA_OK on successful
 *                              MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_task_clear_setting(ma_task_t *task);


/* @brief sets task time zone
 * @param task          [in]    task handle
 * @param zone          [in]    time zone
 * @result                      MA_OK on successful
 *                              MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_task_set_time_zone(ma_task_t *task, ma_time_zone_t zone);



/* @brief retrieves task time zone
 * @param task          [in]    task handle
 * @param zone          [out]   pointer to memory location that store task time zone
 * @result                      MA_OK on successful
 *                              MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_task_get_time_zone(ma_task_t *task, ma_time_zone_t *zone);



/* @brief clones task copy
 * @param task          [in]    task handle
 * @param copy          [out]   pointer to memory location that store newly constructed task
 * @result                      MA_OK on successful the reference count on the 
 *                              returned task will be incremented,
 *                              so the caller should release it when done
 *                              MA_ERROR_INVALIDARG on invalid arguments passed
 *                              MA_ERROR_OUTOFMEMORY on allocation failure
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_task_copy(const ma_task_t *task, ma_task_t **copy);


/* @brief retrieves number of trigger count exist in task
 * @param task          [in]    task handle
 * @param no_of_triggers[out]   pointer to memory location that store number of triggers count in the task
 * @result                      MA_OK on successful
 *                              MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_task_size(ma_task_t *task, size_t *no_of_triggers);

/* @brief adds condition to task
 * @param task          [in]    task handle
 * @param cond          [in]    cond
 * @result                      MA_OK on successful
 *                              MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_task_set_condition(ma_task_t *task, ma_task_cond_t cond);

/* @brief removes condition bit from condition
 * @param task          [in]    task handle
 * @param cond          [in]    cond
 * @result                      MA_OK on successful
 *                              MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_task_unset_condition(ma_task_t *task, ma_task_cond_t cond);


/* @brief sets power policy, if task condition is set MA_TASK_COND_ENFORCE_POWER_POLICY then power policy is evaluated before task is executed
 * @param task          [in]        task handle
 * @param cond          [in]    power policy
 * @result                          MA_OK on successful
 *                                  MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_task_set_power_policy(ma_task_t *task, ma_task_power_policy_t policy);


/* @brief extracts task power policy
 * @param task          [in]        task handle
 * @param cond          [out]    pointer to memory that stores task power policy settings
 * @result                          MA_OK on successful
 *                                  MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_task_get_power_policy(ma_task_t *task, ma_task_power_policy_t *policy);


/* @brief sets task as missed
 * @param task          [in]        task handle
 * @param missed        [in]        missed status
 * @result                          MA_OK on successful
 *                                  MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_task_set_missed(ma_task_t *task, ma_bool_t missed);


/* @brief extracts task missed status
 * @param task          [in]        task handle
 * @param missed        [out]       pointer to memory that stores task missed status
 * @result                          MA_OK on successful
 *                                  MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_SCHEDULER_INLINE_API ma_error_t ma_task_get_missed(ma_task_t *task, ma_bool_t *missed);

MA_CPP(})
    
#include "ma/scheduler/ma_itask.h"

#include "ma/dispatcher/ma_task_dispatcher.h"

#endif /* MA_TASK_H_INCLUDED */


