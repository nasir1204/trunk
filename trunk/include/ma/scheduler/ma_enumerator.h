/** @file ma_enumerator.h
 *  @brief Enumerator
 *
*/
#ifndef MA_ENUMERATOR_H_INCLUDED
#define MA_ENUMERATOR_H_INCLUDED

#include "ma/ma_common.h"

MA_CPP(extern "C" {)

typedef struct ma_enumerator_s ma_enumerator_t;
typedef struct ma_enumerator_methods_s ma_enumerator_methods_t;

struct ma_enumerator_methods_s {
    ma_error_t (*cur)(ma_enumerator_t *enumerator, void *cur);
    ma_error_t (*next)(ma_enumerator_t *enumerator, void *next);
    void (*reset)(ma_enumerator_t *enumerator);
};

struct ma_enumerator_s {
    const ma_enumerator_methods_t *methods;
    void *data;
};

static MA_INLINE ma_error_t ma_enumerator_current(ma_enumerator_t *enumerator, void *cur) {
    if(enumerator && enumerator->methods) {
        return enumerator->methods->cur?enumerator->methods->cur(enumerator, cur):MA_ERROR_PRECONDITION;
    }
    return MA_ERROR_INVALIDARG;
}

static MA_INLINE ma_error_t ma_enumerator_next(ma_enumerator_t *enumerator, void *next) {
    if(enumerator && enumerator->methods) {
        return enumerator->methods->next?enumerator->methods->next(enumerator, next):MA_ERROR_PRECONDITION;
    }
    return MA_ERROR_INVALIDARG;
}

static MA_INLINE void ma_enumerator_reset(ma_enumerator_t *enumerator) {
    if(enumerator && enumerator->methods) {
        enumerator->methods->reset(enumerator);
    }
}

static MA_INLINE void ma_enumerator_init(ma_enumerator_t *enumerator, const ma_enumerator_methods_t *methods, void *data) {
    if(enumerator) {
        enumerator->methods = methods;
        enumerator->data = data;
    }
}

MA_CPP(})
#endif // MA_ENUMERATOR_H_INCLUDED


