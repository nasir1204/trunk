/** @file ma_triggerlist.h
 *  @brief Trigger List
 *
*/
#ifndef MA_TRIGGERLIST_H_INCLUDED
#define MA_TRIGGERLIST_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/scheduler/ma_triggers.h"

MA_CPP(extern "C" {)
typedef struct ma_trigger_list_s ma_trigger_list_t, *ma_trigger_list_h;


/* @brief adds trigger into task trigger list
 * @param tl        [in]    trigger list handle
 * @param trigger   [in]    trigger handle
 * @result                  MA_OK on successful and increments the reference count of the added trigger, user is responsible to release reference when done.
 *                          MA_ERROR_OUTOFMEMORY on allocation failure
 *                          MA_ERROR_INVALIDARG on invalid arguments
 */
MA_SCHEDULER_API ma_error_t ma_trigger_list_add(ma_trigger_list_t *tl, ma_trigger_t *trigger);



/* @brief removes trigger from task trigger list
 * @param tl        [in]    trigger list handle
 * @param id        [in]    trigger id
 * @result                  MA_OK on successful and decrements the reference count of the removed trigger from the list
 *                          MA_ERROR_INVALIDARG on invalid arguments
 */
MA_SCHEDULER_API ma_error_t ma_trigger_list_remove(ma_trigger_list_t *tl, const char *id);



/* @brief finds trigger from task trigger list
 * @param tl        [in]    trigger list handle
 * @param id        [in]    trigger id
 * @param trigger   [out]   pointer to memory location that holds the address of trigger
 * @result                  MA_OK on successful and increments the reference count of the found trigger in the list, user is responsible to release reference when done.
 *                          MA_ERROR_INVALIDARG on invalid arguments
 *                          MA_ERROR_SCHEDULER_INVALID_TRIGGER_ID on invalid trigger
 */
MA_SCHEDULER_API ma_error_t ma_trigger_list_find(ma_trigger_list_t *tl, const char *id, ma_trigger_t **trigger);



/* @brief returns number of triggers exist in the trigger list
 * @param tl        [in]    trigger list handle
 * @param size      [out]   pointer to memory location that stores number of triggers exist in the trigger list
 * @result                  MA_OK on successful
 *                          MA_ERROR_INVALIDARG on invalid arguments
 */
MA_SCHEDULER_API ma_error_t ma_trigger_list_size(ma_trigger_list_t *tl, size_t *size);



/* @brief retrieves trigger for the given index from the task trigger list
 * @param tl        [in]    trigger list handle
 * @param trigger   [in]    trigger handle
 * @result                  MA_OK on successful and increments the reference count of the added trigger, user is responsible to release reference when done.
 *                          MA_ERROR_INVALIDARG on invalid arguments
 */
MA_SCHEDULER_API ma_error_t ma_trigger_list_at(ma_trigger_list_t *tl, size_t index, ma_trigger_t **trigger);

MA_CPP(})

#include "ma/dispatcher/ma_trigger_list_dispatcher.h"

#endif /* MA_TRIGGERLIST_H_INCLUDED */


