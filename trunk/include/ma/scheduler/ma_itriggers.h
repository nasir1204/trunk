#ifndef MA_ITRIGGERS_H_INCLUDED
#define MA_ITRIGGERS_H_INCLUDED

#include "ma/ma_common.h"

MA_CPP(extern "C"{)
typedef struct ma_trigger_methods_s ma_trigger_methods_t;

#undef MA_TRIGGER_ACTUAL_ARG_EXPANDER
#define MA_TRIGGER_ACTUAL_ARG_EXPANDER\
    MA_TRIGGER_ARG_EXPANDER(ma_error_t,ma_trigger_set_begin_date,(ma_trigger_t *trigger, ma_task_time_t *start_date),(trigger, start_date))\
    MA_TRIGGER_ARG_EXPANDER(ma_error_t,ma_trigger_set_end_date,(ma_trigger_t *trigger, ma_task_time_t *end_date),(trigger, end_date))\
    MA_TRIGGER_ARG_EXPANDER(ma_error_t,ma_trigger_get_type,(ma_trigger_t *trigger, ma_trigger_type_t *type),(trigger, type))\
    MA_TRIGGER_ARG_EXPANDER(ma_error_t,ma_trigger_get_state,(ma_trigger_t *trigger, ma_trigger_state_t *state),(trigger, state))\
    MA_TRIGGER_ARG_EXPANDER(ma_error_t,ma_trigger_get_id,(ma_trigger_t *trigger, const char **id),(trigger, id))\
    MA_TRIGGER_ARG_EXPANDER(ma_error_t,ma_trigger_get_begin_date,(ma_trigger_t *trigger, ma_task_time_t *begin_date),(trigger, begin_date))\
    MA_TRIGGER_ARG_EXPANDER(ma_error_t,ma_trigger_get_end_date,(ma_trigger_t *trigger, ma_task_time_t *end_date),(trigger, end_date))\
    MA_TRIGGER_ARG_EXPANDER(ma_error_t,ma_trigger_get_policy,(ma_trigger_t *trigger, void *policy),(trigger, policy))\
    MA_TRIGGER_ARG_EXPANDER(ma_error_t,ma_trigger_set_state,(ma_trigger_t *trigger, ma_trigger_state_t state),(trigger, state))\
    MA_TRIGGER_ARG_EXPANDER(ma_error_t,ma_trigger_release,(ma_trigger_t *trigger),(trigger))\
    MA_TRIGGER_ARG_EXPANDER(ma_error_t,ma_trigger_add_ref,(ma_trigger_t *trigger),(trigger))\
    MA_TRIGGER_ARG_EXPANDER(ma_error_t,ma_trigger_set_next_fire_date,(ma_trigger_t *trigger, ma_task_time_t next_date),(trigger, next_date))\
    MA_TRIGGER_ARG_EXPANDER(ma_error_t,ma_trigger_get_next_fire_date,(ma_trigger_t *trigger, ma_task_time_t *next_date),(trigger, next_date))\
    MA_TRIGGER_ARG_EXPANDER(ma_error_t,ma_trigger_set_last_fire_date,(ma_trigger_t *trigger, ma_task_time_t last_date),(trigger, last_date))\
    MA_TRIGGER_ARG_EXPANDER(ma_error_t,ma_trigger_get_last_fire_date,(ma_trigger_t *trigger, ma_task_time_t *last_date),(trigger, last_date))\
    MA_TRIGGER_ARG_EXPANDER(ma_error_t,ma_trigger_validate_policy,(ma_trigger_t *trigger),(trigger))


struct ma_trigger_methods_s {
#define MA_TRIGGER_ARG_EXPANDER(ret_type, function, signature, arg) ret_type (*function##_fn) signature;
    MA_TRIGGER_ACTUAL_ARG_EXPANDER
#undef MA_TRIGGER_ARG_EXPANDER
};

struct ma_trigger_s {
    const ma_trigger_methods_t *methods;
    void *data;
};

MA_STATIC_INLINE void ma_trigger_init(ma_trigger_t *trigger, const ma_trigger_methods_t *methods, void *data) {
    if(trigger) {
        trigger->methods = methods;
        trigger->data = data;
    }
}

#define MA_TRIGGER_ARG_EXPANDER(ret_type, function, signature, arg)\
    MA_SCHEDULER_INLINE_API ret_type function signature {\
        if(trigger && trigger->methods) {\
            return trigger->methods->function##_fn?trigger->methods->function##_fn arg:MA_ERROR_PRECONDITION;\
        }\
        return MA_ERROR_INVALIDARG;\
    }
    MA_TRIGGER_ACTUAL_ARG_EXPANDER
#undef MA_TRIGGER_ARG_EXPANDER


MA_CPP(})
#endif // MA_ITRIGGERS_H_INCLUDED


