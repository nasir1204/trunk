/** @file ma_proxy_config.h
 *  @brief Getting the proxy configuration information
 *
*/
#ifndef MA_PROXY_CONFIG_H_INCLUDED
#define MA_PROXY_CONFIG_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/proxy/ma_proxy.h"

MA_CPP(extern "C" {)

typedef struct ma_proxy_config_s ma_proxy_config_t, *ma_proxy_config_h;

/**
*  proxy usage Type 
*/
typedef enum ma_proxy_usage_type_e { 
	MA_PROXY_USAGE_TYPE_NONE = 0,           /* No proxy to be used */
	MA_PROXY_USAGE_TYPE_SYSTEM_CONFIGURED,  /* use proxies configured in IE settings or PAC file OR user configured if allowed*/
	MA_PROXY_USAGE_TYPE_SERVER_CONFIGURED,  /* use proxy configured from epo server */
    MA_PROXY_USAGE_TYPE_END
}ma_proxy_usage_type_t ;

/** 
 * @brief creates proxy config object
 * @param configuration               [out]     pointer containing proxy config object.
 * @result										MA_OK on success.
 *												MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_PROXY_API ma_error_t ma_proxy_config_create(ma_proxy_config_t **configuration);

/** 
 * @brief release proxy config object
 * @param configuration               [in]		proxy config object to be relase.
 * @result										MA_OK on success.
 *												MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_PROXY_API ma_error_t ma_proxy_config_release(ma_proxy_config_t *configuration);

/** 
 * @brief increment reference of proxy config object
 * @param configuration               [in]		proxy config object, whose reference to be increment.
 * @result										MA_OK on success.
 *												MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_PROXY_API ma_error_t ma_proxy_config_add_ref(ma_proxy_config_t *configuration) ;

/** 
 * @brief set proxy config usage type
 * @param configuration               [in]     proxy config object, whose usage type to be set.
 * @param type						  [in]     usage type to be set.
 * @result									   MA_OK on success.
 *											   MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_PROXY_API ma_error_t ma_proxy_config_set_proxy_usage_type(ma_proxy_config_t *configuration, ma_proxy_usage_type_t type);

/** 
 * @brief get proxy config usage type
 * @param configuration               [in]     proxy config object, whose usage type to be get.
 * @param type						  [out]    usage type.
 * @result									   MA_OK on success.
 *											   MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_PROXY_API ma_error_t ma_proxy_config_get_proxy_usage_type(ma_proxy_config_t *configuration, ma_proxy_usage_type_t *type);

/** 
 * @brief set system proxy authentication
 * @param configuration               [in]     proxy config object, whose authentication to be set.
 * @param protocol_type				  [in]     system proxy type(http/ftp).
 * @param auth_req					  [in]     system proxy authentication required(1) or not(0).
 * @param user_name					  [in]     system proxy user name.
 * @param password					  [in]     system proxy password.
 * @result									   MA_OK on success.
 *											   MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_PROXY_API ma_error_t ma_proxy_config_set_system_proxy_authentication(ma_proxy_config_t *configuration, ma_proxy_protocol_type_t protocol_type, ma_bool_t auth_req, const char *user_name, const char *password);

/** 
 * @brief get system proxy authentication
 * @param configuration               [in]     proxy config object, from which authentication to be get.
 * @param protocol_type				  [out]    system proxy type(http/ftp).
 * @param auth_req					  [out]    system proxy authentication required..
 * @param user_name					  [out]    system proxy user name.
 * @param password					  [out]    system proxy password.
 * @result									   MA_OK on success.
 *											   MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_PROXY_API ma_error_t ma_proxy_config_get_system_proxy_authentication(ma_proxy_config_t *configuration, ma_proxy_protocol_type_t protocol_type, ma_bool_t *auth_req, const char **user_name, const char **password);

/** 
 * @brief get system proxy list
 * @param configuration               [in]     proxy config object, from which system proxy list to be get.
 * @param url						  [out]    system proxy type(http/ftp).
 * @param auth_req					  [out]    system proxy authentication required..
 * @param user_name					  [out]    system proxy user name.
 * @param password					  [out]    system proxy password.
 * @result									   MA_OK on success.
 *											   MA_ERROR_INVALIDARG on invalid argument passed.
 *											   MA_ERROR_NOT_SUPPORTED on not supported platform/placeholder api.
 * @remark               
 *
 */
MA_PROXY_API ma_error_t ma_proxy_config_get_system_proxy(ma_proxy_config_t *configuration, const char *url, ma_proxy_list_t **list);

/** 
 * @brief set allow local proxy configuration.
 * @param configuration               [in]     proxy config object, whose allow local proxy configuration to be set.
 * @param allow_or_not				  [in]     allow local proxy configuration to be set.
 * @result									   MA_OK on success.
 *											   MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_PROXY_API ma_error_t ma_proxy_config_set_allow_local_proxy_configuration(ma_proxy_config_t *configuration, ma_bool_t allow_or_not);

/** 
 * @brief get allow local proxy configuration.
 * @param configuration               [in]     proxy config object, whose allow local proxy configuration to be get.
 * @param allow_or_not				  [out]    usage type.
 * @result									   MA_OK on success.
 *											   MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_PROXY_API ma_error_t ma_proxy_config_get_allow_local_proxy_configuration(ma_proxy_config_t *configuration, ma_bool_t *allow_or_not);

/** 
 * @brief set proxy list in proxy config object.
 * @param configuration               [in]     proxy config object, in which proxy list to be set.
 * @param list						  [in]     proxy list to be set.
 * @result									   MA_OK on success.
 *											   MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_PROXY_API ma_error_t ma_proxy_config_set_proxy_list(ma_proxy_config_t *configuration, ma_proxy_list_t *list);

/** 
 * @brief get proxy list from proxy config object.
 * @param configuration               [in]     proxy config object, from which proxy list to be get.
 * @param list				  [out]    proxy list to be get.
 * @result									   MA_OK on success.
 *											   MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_PROXY_API ma_error_t ma_proxy_config_get_proxy_list(ma_proxy_config_t *configuration, ma_proxy_list_t **list);

/** 
 * @brief set proxy config bypass local value.
 * @param configuration               [in]     proxy config object, whose bypass local value to be set.
 * @param bypass_local				  [in]     bypass local value to be set.
 * @result									   MA_OK on success.
 *											   MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_PROXY_API ma_error_t ma_proxy_config_set_bypass_local(ma_proxy_config_t *configuration, ma_bool_t bypass_local);

/** 
 * @brief get proxy config bypass local value.
 * @param configuration               [in]     proxy config object, whose bypass local value to be get.
 * @param bypass_local				  [out]    bypass local value to be get.
 * @result									   MA_OK on success.
 *											   MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_PROXY_API ma_error_t ma_proxy_config_get_bypass_local(ma_proxy_config_t *configuration, ma_bool_t *bypass_local);

/** 
 * @brief set proxy config exclusion addresses list.
 * @param configuration               [in]     proxy config object, whose exclusion addresses list to be set.
 * @param bypass_exclusion_str		  [in]     comma seperated exclusion addresses string.
 * @result									   MA_OK on success.
 *											   MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_PROXY_API ma_error_t ma_proxy_config_set_exclusion_urls(ma_proxy_config_t *configuration, const char *bypass_exclusion_str);

/** 
 * @brief get proxy config exclusion addresses list.
 * @param configuration               [in]     proxy config object, whose exclusion addresses list to be get.
 * @param bypass_exclusion_str		  [out]    comma seperated exclusion addresses string.
 * @result									   MA_OK on success.
 *											   MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_PROXY_API ma_error_t ma_proxy_config_get_exclusion_urls(ma_proxy_config_t *configuration, const char **bypass_exclusion_str);

MA_CPP(})

#endif  /* MA_PROXY_H_INCLUDED */
