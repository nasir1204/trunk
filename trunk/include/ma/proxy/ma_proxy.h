/** @file ma_proxy.h
 *  @brief Proxy interfaces
 *
*/
#ifndef MA_PROXY_H_INCLUDED
#define MA_PROXY_H_INCLUDED

#include "ma/ma_common.h"

#define MA_PROXY_FLAG_SERVER_CONFIGURED         0x01
#define MA_PROXY_FLAG_LOCAL_CONFIGURED          0x02


MA_CPP(extern "C" {)

typedef struct ma_proxy_s				    ma_proxy_t, *ma_proxy_h ;
typedef struct ma_system_proxy_auth_info_s	ma_system_proxy_auth_info_t, *ma_system_proxy_auth_info_h ;
typedef struct ma_proxy_list_s			    ma_proxy_list_t, *ma_proxy_list_h ;

/**
*  proxy protocol type
*/
typedef enum ma_proxy_protocol_type_e {
    MA_PROXY_TYPE_HTTP = 0,
    MA_PROXY_TYPE_FTP,
	MA_PROXY_TYPE_BOTH,
	MA_PROXY_TYPE_END
}ma_proxy_protocol_type_t;


/** 
 * @brief creates proxy object
 * @param proxy                    [out]     pointer containing proxy object.
 * @result                                   MA_OK on success.
 *                                           MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_PROXY_API ma_error_t ma_proxy_create(ma_proxy_t **proxy);

/** 
 * @brief release proxy object
 * @param proxy                    [in]		proxy object to be release.
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_PROXY_API ma_error_t ma_proxy_release(ma_proxy_t *proxy);

/** 
 * @brief set address in proxy object
 * @param proxy                    [in]		proxy object pointer in which address to be set.
 * @param address                  [in]		address string.
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_PROXY_API ma_error_t ma_proxy_set_address(ma_proxy_t *proxy, const char *address);

/** 
 * @brief get address from proxy object
 * @param proxy                    [in]		proxy object pointer from which address to be get.
 * @param address                  [out]	address string of proxy.
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_PROXY_API ma_error_t ma_proxy_get_address(ma_proxy_t *proxy, const char **address);

/** 
 * @brief set port in proxy object
 * @param proxy                    [in]		proxy object pointer in which port to be set.
 * @param port		               [in]		proxy port to be set.
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_PROXY_API ma_error_t ma_proxy_set_port(ma_proxy_t *proxy, ma_int32_t port);

/** 
 * @brief get port from proxy object
 * @param proxy                    [in]		proxy object pointer from which port to be get.
 * @param port	                   [out]	port of proxy.
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_PROXY_API ma_error_t ma_proxy_get_port(ma_proxy_t *proxy, ma_int32_t *port);

/** 
 * @brief set scope id in proxy object
 * @param proxy                    [in]		proxy object pointer in which scope id to be set.
 * @param scopeid	               [in]		proxy scope id to be set.
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_PROXY_API ma_error_t ma_proxy_set_scopeid(ma_proxy_t *proxy, ma_uint32_t scopeid);

/** 
 * @brief get scope id from proxy object
 * @param proxy                    [in]		proxy object pointer from which scope id to be get.
 * @param scopeid                  [out]	scope id of proxy.
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_PROXY_API ma_error_t ma_proxy_get_scopeid(ma_proxy_t *proxy, ma_uint32_t *scopeid);

/** 
 * @brief set protocol type in proxy object
 * @param proxy                    [in]		proxy object pointer in which protocol type to be set.
 * @param type					   [in]		proxy protocol type to be set.
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_PROXY_API ma_error_t ma_proxy_set_protocol_type(ma_proxy_t *proxy, ma_proxy_protocol_type_t type);

/** 
 * @brief get protocol type from proxy object
 * @param proxy                    [in]		proxy object pointer from which protocol type to be get.
 * @param type	                   [out]	protocol type of proxy.
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_PROXY_API ma_error_t ma_proxy_get_protocol_type(ma_proxy_t *proxy, ma_proxy_protocol_type_t *type);

/** 
 * @brief set authentication in proxy object
 * @param proxy                    [in]		proxy object pointer in which authentication to be set.
 * @param auth_req				   [in]		whether proxy authentication required(0:not required, 1:required).
 * @param user_name				   [in]		proxy user name to be set.
 * @param password				   [in]		proxy password to be set.
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_PROXY_API ma_error_t ma_proxy_set_authentication(ma_proxy_t *proxy, ma_bool_t auth_req, const char *user_name, const char *password);

/** 
 * @brief get authentication details from proxy object
 * @param proxy                    [in]		proxy object pointer from which authentication details to be get.
 * @param auth_req				   [out]	proxy authentication type (0:not required, 1:required).
 * @param user_name				   [out]	user name of proxy.
 * @param password				   [out]	password of proxy.
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_PROXY_API ma_error_t ma_proxy_get_authentication(ma_proxy_t *proxy, ma_bool_t *auth_req, const char **user_name, const char **password);

/** 
 * @brief set flag(server/local confiured) in proxy object
 * @param proxy                    [in]		proxy object pointer in which flag to be set.
 * @param flag					   [in]		proxy flag to be set(1:server, 2:local).
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_PROXY_API ma_error_t ma_proxy_set_flag(ma_proxy_t *proxy, ma_int32_t flag);

/** 
 * @brief get flag(server/local confiured) from proxy object
 * @param proxy                    [in]		proxy object pointer from which flag to be get.
 * @param flag	                   [out]	flag of proxy (1:server, 2:local).
 * @result									MA_OK on success.
 *											MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_PROXY_API ma_error_t ma_proxy_get_flag(ma_proxy_t *proxy, ma_int32_t *flag);


/** 
 * @brief creates proxy list object
 * @param list                    [out]     pointer containing proxy list object.
 * @result                                  MA_OK on success.
 *                                          MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_PROXY_API ma_error_t ma_proxy_list_create(ma_proxy_list_t **list);

/** 
 * @brief increment reference in proxy list object
 * @param list                    [in]     proxy list object whose reference to be increament.
 * @result                                 MA_OK on success.
 *                                         MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_PROXY_API ma_error_t ma_proxy_list_add_ref(ma_proxy_list_t *list);

/** 
 * @brief add proxy in proxy list object
 * @param list                    [in]     proxy list object in which proxy to be added.
 * @param proxy                   [in]     proxy object, which need to added in proxy list.
 * @result                                 MA_OK on success.
 *                                         MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_PROXY_API ma_error_t ma_proxy_list_add_proxy(ma_proxy_list_t *list, ma_proxy_t *proxy);

/** 
 * @brief get number of proxies in proxy list object
 * @param list                    [in]     proxy list object for which size to be get.
 * @param size                    [out]    number of proxies.
 * @result                                 MA_OK on success.
 *                                         MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_PROXY_API ma_error_t ma_proxy_list_size(ma_proxy_list_t *list, size_t *size);

/** 
 * @brief get ith proxy from proxy list object
 * @param list                    [in]     proxy list object.
 * @param index                   [in]     index of proxy in proxy list.
 * @param proxy                   [out]    proxy object at index location in proxy list.
 * @result                                 MA_OK on success.
 *                                         MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_PROXY_API ma_error_t ma_proxy_list_get_proxy(ma_proxy_list_t *list, ma_int32_t index, ma_proxy_t **proxy);

/** 
 * @brief set index of last used proxy, in proxy list object
 * @param list                    [in]     proxy list object.
 * @param index                   [in]     index of last used proxy to be set.
 * @result                                MA_OK on success.
 *                                         MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_PROXY_API ma_error_t ma_proxy_list_set_last_proxy_index(ma_proxy_list_t *list, ma_int32_t index);

/** 
 * @brief get index of last used proxy, from proxy list object
 * @param list                    [in]     proxy list object.
 * @param index                   [out]    index of last used proxy.
 * @result                                MA_OK on success.
 *                                         MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_PROXY_API ma_error_t ma_proxy_list_get_last_proxy_index(ma_proxy_list_t *list, ma_int32_t *index);

/** 
 * @brief release proxy list object
 * @param list                    [in]     proxy list object to be release.
 * @result                                 MA_OK on success.
 *                                         MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *
 */
MA_PROXY_API ma_error_t ma_proxy_list_release(ma_proxy_list_t *list);

MA_CPP(})

#endif  /* MA_PROXY_H_INCLUDED */

