#ifndef MA_DS_INI_H_INCLUDED
#define MA_DS_INI_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/datastore/ma_ds.h"

MA_CPP(extern "C" {)

typedef struct ma_ds_ini_s ma_ds_ini_t, *ma_ds_ini_h;

#define MA_INI_FILE_EXT         ".ini"

/* flag : 0 ds ini will take care of file opening mode, if files exists, open in append mode or else write mode
		  1 ds ini will open the file always in write mode.
*/
MA_DATASTORE_API ma_error_t ma_ds_ini_open(const char *ini_file, int flag, ma_ds_ini_t **datastore);

MA_DATASTORE_API ma_error_t ma_ds_ini_release(ma_ds_ini_t *datastore);

MA_CPP(})

#endif	/* MA_DS_INI_H_INCLUDED */

