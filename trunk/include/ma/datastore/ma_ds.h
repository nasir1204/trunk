#ifndef MA_DS_H_INCLUDED
#define MA_DS_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_datastore.h"

MA_CPP(extern "C" {)


/* Defines the C equivalent of a 'v-table' for the datastore interface implementor */
typedef struct ma_ds_methods_s {    
	ma_error_t (*set)(ma_ds_t *self, const char *path, const char *key, ma_variant_t *value);
	ma_error_t (*get)(ma_ds_t *self, const char *path, const char *key, ma_vartype_t type, ma_variant_t **value);
    ma_error_t (*is_exists)(ma_ds_t *self, const char *path, const char *key, ma_bool_t *is_exists);
    ma_error_t (*remove)(ma_ds_t *self, const char *path, const char *key);
	ma_error_t (*release)(ma_ds_t *self);      /*< virtual destructor */


	ma_error_t (*get_iterator)(ma_ds_t *self, ma_uint8_t is_path_iterator, const char *path, ma_ds_iterator_t **iterator);
} ma_ds_methods_t;

/* 'Base' class for all the datastore should derive from */
struct ma_ds_s {
    ma_ds_methods_t					const *methods;   /*< v-table */
    void                           *data; /* datastore extension data */
};


/*!
 * Helper function for derived classes to use for initialization
 */
static MA_INLINE void ma_ds_init(ma_ds_t *datastore, ma_ds_methods_t  const *methods, void *data) {
    datastore->methods = methods;    
    datastore->data = data;    
} 


static MA_INLINE ma_error_t ma_ds_set(ma_ds_t *datastore, const char *path, const char *key, ma_variant_t *value) {    
	if(!datastore || !path || !key || !value) return MA_ERROR_PRECONDITION;
    return datastore->methods->set ? datastore->methods->set(datastore, path, key, value) : MA_ERROR_NOT_SUPPORTED;
}

static MA_INLINE ma_error_t ma_ds_get(ma_ds_t *datastore, const char *path, const char *key, ma_vartype_t type, ma_variant_t **value) {    
    if(!datastore || !path || !key || !value) return MA_ERROR_PRECONDITION;
    return datastore->methods->get ? datastore->methods->get(datastore, path, key, type, value) : MA_ERROR_NOT_SUPPORTED;
}


static MA_INLINE ma_error_t ma_ds_remove(ma_ds_t *datastore, const char *path, const char *key) {    
    if(!datastore || !path ) return MA_ERROR_PRECONDITION;
	return datastore->methods->remove ? datastore->methods->remove(datastore, path, key) : MA_ERROR_NOT_SUPPORTED;
}

static MA_INLINE ma_error_t ma_ds_entry_exists(ma_ds_t *datastore, const char *path, const char *key , ma_bool_t *is_exists) {    
    if(!datastore || !path || !is_exists ) return MA_ERROR_PRECONDITION;
	return datastore->methods->is_exists ? datastore->methods->is_exists(datastore, path, key, is_exists) : MA_ERROR_NOT_SUPPORTED;
}

static MA_INLINE ma_error_t ma_ds_release(ma_ds_t *datastore) {
    if(!datastore) return MA_ERROR_PRECONDITION;
    return datastore->methods->release ?  datastore->methods->release(datastore) : MA_ERROR_NOT_SUPPORTED;
}

static MA_INLINE ma_error_t ma_ds_get_iterator(ma_ds_t *datastore, ma_uint8_t is_path_iterator, const char *path, ma_ds_iterator_t **iterator) {
	if(!datastore || !path || !iterator) return MA_ERROR_PRECONDITION;
	return datastore->methods->get_iterator ?  datastore->methods->get_iterator(datastore,is_path_iterator,path,iterator) : MA_ERROR_NOT_SUPPORTED;
}

MA_CPP(})

#endif /*MA_DS_H_INCLUDED*/
