#ifndef MA_DS_REGISTRY_H_INCLUDED
#define MA_DS_REGISTRY_H_INCLUDED

#include "ma/ma_common.h"
/* Windows only */
#ifdef  MA_WINDOWS 
#include "ma/datastore/ma_ds.h"

#include <Windows.h>


# define MA_DS_REGISTRY_DEFAULT_VIEW MA_TRUE
# define MA_DS_REGISTRY_ALTERNATE_VIEW MA_FALSE

/* Use these flags as 'b_default_view' argument to ma_ds_registry_open() for selecting an absolute registry view regardless of 64 or 32bit compilation */
#ifdef _WIN64
# define MA_DS_REGISTRY_32BIT_VIEW MA_FALSE
# define MA_DS_REGISTRY_64BIT_VIEW MA_TRUE
#else
# define MA_DS_REGISTRY_32BIT_VIEW MA_TRUE
# define MA_DS_REGISTRY_64BIT_VIEW MA_FALSE
#endif


MA_CPP(extern "C" {)

typedef struct ma_ds_registry_s ma_ds_registry_t, *ma_ds_registry_h;

/* It will never create the key, it's just opens the path to be used for next set of datastore API's
    Refer RegOpenKeyEx
	@param b_default_view pass true if you just want to access the default view of registry based on your platform
    x86 and x64.
    pass false if you want to access cross-platform registry view. Will have no impact when running on x86 machines.
*/
MA_DATASTORE_API ma_error_t ma_ds_registry_open(HKEY base, const char *root_path, ma_bool_t b_default_view, ma_bool_t create_if_not_exist, REGSAM access, ma_ds_registry_t **datastore) ;

MA_DATASTORE_API ma_error_t ma_ds_registry_release(ma_ds_registry_t *datastore);

MA_CPP(})

#endif  /* MA_WINDOWS */

#endif	/* MA_DS_REGISTRY_H_INCLUDED */

