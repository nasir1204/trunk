#ifndef MA_DS_ITERATOR_H_INCLUDED
#define MA_DS_ITERATOR_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_datastore.h"


MA_CPP(extern "C" {)


/* Defines the C equivalent of a 'v-table' for the datastore iterator interface implementor */
typedef struct ma_ds_iterator_methods_s {    
	ma_error_t (*get_next)(ma_ds_iterator_t *self, ma_buffer_t **buffer);
	ma_error_t (*release)(ma_ds_iterator_t *self);      /*< virtual destructor */
} ma_ds_iterator_methods_t;

/* 'Base' class for all the datastore iterators should derive from */
struct ma_ds_iterator_s {
	ma_ds_iterator_methods_t		const *methods;/*< v-table */
    void								 *data; /* datastore iterator extension data */
};


static MA_INLINE void ma_ds_iterator_init(ma_ds_iterator_t *iterator, ma_ds_iterator_methods_t const *methods, void *data) {
	iterator->methods = methods;
	iterator->data = data;
}



MA_CPP(})

#endif /*MA_DS_ITERATOR_H_INCLUDED*/
