#ifndef MA_DS_DATABASE_H_INCLUDED
#define MA_DS_DATABASE_H_INCLUDED

#include "ma/ma_common.h"

#include "ma/internal/utils/database/ma_db.h"

#include "ma/datastore/ma_ds.h"

MA_CPP(extern "C" {)

typedef struct ma_ds_database_s ma_ds_database_t, *ma_ds_database_h;

MA_DATASTORE_API ma_error_t ma_ds_database_create(ma_db_t *database, const char *db_instance_name , ma_ds_database_t **datastore);

MA_DATASTORE_API ma_error_t ma_ds_database_release(ma_ds_database_t *datastore);

MA_DATASTORE_API ma_error_t ma_ds_database_delete_instance(ma_ds_database_t *datastore);

MA_DATASTORE_API ma_error_t ma_ds_database_transaction_begin(ma_ds_database_t *datastore);

MA_DATASTORE_API ma_error_t ma_ds_database_transaction_cancel(ma_ds_database_t *datastore);

MA_DATASTORE_API ma_error_t ma_ds_database_transaction_end(ma_ds_database_t *datastore);


MA_CPP(})

#endif	/* MA_DS_DATABASE_H_INCLUDED */

