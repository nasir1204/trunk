#ifndef MA_DS_MAPPER_H_INCLUDED
#define MA_DS_MAPPER_H_INCLUDED

#include "ma/datastore/ma_ds.h"
/* ma_ds_mapper is a implementation of ma_ds, which is a collection of datastore and it maps to specific implementation on the basis of MAPPPER_PREFIX */

MA_CPP(extern "C" {)

typedef struct ma_ds_mapper_s ma_ds_mapper_t, *ma_ds_mapper_h;


#define MA_DS_MAPPER_PREFIX_REG            "MREG\\"
#define MA_DS_MAPPER_PREFIX_INI            "MINI\\"
#define MA_DS_MAPPER_PREFIX_DBH            "MDBH\\"

#define MA_DS_MAPPER_PREFIX_LEN             5

typedef enum ma_ds_type_e  {
    MA_DS_TYPE_INI = 0, 
    MA_DS_TYPE_REG    ,
    MA_DS_TYPE_DBH
}ma_ds_type_t;


MA_DATASTORE_API ma_error_t ma_ds_mapper_create(ma_ds_mapper_t **mapper);

/* You can pass filter as NULL if you are not adding more filters on the key other than the generic prefix for each data types */
//TODO - implement the filter as of nothing but null
MA_DATASTORE_API ma_error_t ma_ds_mapper_ds_add(ma_ds_mapper_t *mapper, ma_ds_type_t type, ma_ds_t *datastore, const char *filter );

MA_DATASTORE_API ma_error_t ma_ds_mapper_ds_remove(ma_ds_mapper_t *mapper, ma_ds_t *datastore);

MA_DATASTORE_API ma_error_t ma_ds_mapper_release(ma_ds_mapper_t *mapper);



MA_CPP(})


#endif	/* MA_DS_MAPPER_H_INCLUDED */

