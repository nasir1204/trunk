#ifndef MA_DS_XML_H_INCLUDED
#define MA_DS_XML_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/datastore/ma_ds.h"

MA_CPP(extern "C" {)

/***
The XML datastore doesn't support the attributes in xml
<Properties>
	<MachineName>IMToolComputer</MachineName>
	<MachineID>{01234567-89AB-CDEF-0123-456789ABCDEF}</MachineID>
	<PropsVersion>20051208224530</PropsVersion>
	<ComputerProperties>
		<PlatformID>WNTS:5:0:4</PlatformID>
		<ComputerName>Anurag's Computer</ComputerName>
		<CPUinfo>
			<CPUType>Intel Pentium III or newer</CPUType>
			<NumOfCPU>2</NumOfCPU>
			<CPUSpeed>1993</CPUSpeed>
			<CPUSerialNumber>N/A</CPUSerialNumber>
		</CPUinfo>
		<OSInfo>
			<OSType>Windows 7</OSType>
			<OSPlatform>Server</OSPlatform>
			<OSVersion>5.0</OSVersion>
			<OSBuildNum>2195</OSBuildNum>
			<OSCsdVersion>Service Pack 4</OSCsdVersion>
		</OSInfo>
			<TotalPhysicalMemory>804270080</TotalPhysicalMemory><FreeMemory>268947456</FreeMemory>
			<TimeZone>Pacific Standard Time</TimeZone>
			<DefaultLangID>0409</DefaultLangID>
			<EmailAddress>WNTS</EmailAddress><OSOEMId>51876-335-4615354-05779</OSOEMId><LastUpdate>12/08/2005 14:45:30</LastUpdate>
		<USERInfo>
			<UserName>imtool_user</UserName><DomainName>IMToolDomain</DomainName>
		</USERInfo>
	</ComputerProperties>
</Properties>

***/
typedef struct ma_ds_xml_s ma_ds_xml_t, *ma_ds_xml_h ;

#define MA_XML_FILE_EXT ".xml"

MA_DATASTORE_API ma_error_t ma_ds_xml_open(const char *xml_file, ma_ds_xml_t **datastore) ;

MA_CPP(})

#endif	/* MA_DS_XML_H_INCLUDED */

