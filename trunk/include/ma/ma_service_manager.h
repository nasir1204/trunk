#ifndef MA_SERVICE_MANAGER_H_INCLUDED
#define MA_SERVICE_MANAGER_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_msgbus.h"

MA_CPP(extern "C" {)

typedef struct ma_service_manager_s ma_service_manager_t, *ma_service_manager_h;

typedef struct ma_service_manager_subscriber_list_s ma_service_manager_subscriber_list_t, *ma_service_manager_subscriber_list_h;

typedef struct ma_service_manager_service_list_s ma_service_manager_service_list_t, *ma_service_manager_service_list_h;

/**
* Callback on subscriber Registration/Unregistration
* @param service_manager    [in]            Handle of service manager client
* @param status				[in]            status, MA_TRUE for subscriber registration, MA_FALSE for subscriber unregistration
* @param topic				[in]			Subscribed topic 
* @param product_id			[in]			Product Id of subscriber
* @param cb_data			[in]            callers data
*/
typedef ma_error_t (*ma_service_manager_on_subscribe_cb_t)(ma_service_manager_t *service_manager, ma_bool_t status, const char *topic, const char *product_id, void *cb_data);

/*
 * Creates a Service manager client Handle
 * @param msgbus					   [in]         MSGBUS Handle.
 * @param service_manager			   [out]	    Handle of service manager client. Don't forget to call ma_service_manager_release() when you are done.
 *
 */					
MA_SERVICE_MANAGER_API ma_error_t ma_service_manager_create(ma_msgbus_t *msgbus, ma_service_manager_t **service_manager);

/*
 * Sets logger
 * @param service_manager			  [in]         Service Manager Handle.
 * @param logger					  [in]         Logger Instance
 *
 */
MA_SERVICE_MANAGER_API ma_error_t ma_service_manager_set_logger(ma_service_manager_t *service_manager, ma_logger_t *logger);

/*
 * Registers for a subscriber change Notifications 
 * @param service_manager			   [in]         Service Manager Handle.
 * @param topic_filter				   [in]         Topic filter, e.g. ma_poilicy_timeout, ma.*, ma.policy.* etc.
 * @param cb						   [in]			Callback to be invoked on subscriber Registration/Unregistration.
 * @param cb_data					   [in]			Caller's data.
 *
 */
MA_SERVICE_MANAGER_API ma_error_t ma_service_manager_register_subscriber_notification(ma_service_manager_t *service_manager, const char *topic_filter, ma_service_manager_on_subscribe_cb_t cb, void *cb_data);

/*
 * Unregisters for a subscriber change Notifications 
 * @param service_manager			   [in]         Service Manager Handle.
 * @param topic_filter				   [in]         Topic filter 
 *
 */
MA_SERVICE_MANAGER_API ma_error_t ma_service_manager_unregister_subscriber_notification(ma_service_manager_t *service_manager, const char *topic_filter);

/*
 * Gets Subscribers list for specified topic. 
 * @param service_manager			   [in]         Service Manager Handle.
 * @param topic_filter				   [in]         Topic Name, e.g. ma_poilicy_timeout, ma.*, ma.policy.* etc.
 * @param subscribers_list			   [out]		Subscribers list. Don't forget to call ma_service_manager_subscriber_list_release().
 * @param count						   [out]		Number of subscribers registered for specified topic.
 *
 */
MA_SERVICE_MANAGER_API ma_error_t ma_service_manager_get_subscribers(ma_service_manager_t *service_manager, const char *topic_filter, ma_service_manager_subscriber_list_t **subscribers_list, size_t *count);


/**
* Callback to get the subscriber list 
* @param status             [in]            Status of operation
* @param service_manager    [in]            Handle of service manager client
* @param topic_filter    	[in]			Subscribed topic 
* @param subscribers_list	[in]		    Subscribers list.
* @param count				[in]		    Number of subscribers registered for specified topic.
* @param cb_data			[in]            callers data
*/
typedef ma_error_t (*ma_service_manager_get_subscribes_cb_t)(ma_error_t status, ma_service_manager_t *service_manager, const char *topic_filter, const ma_service_manager_subscriber_list_t *subscribers_list, size_t count, void *cb_data);

/*
 * Gets Subscribers list for specified topic in async. 
 * @param service_manager			   [in]         Service Manager Handle.
 * @param topic_filter				   [in]         Topic Name, e.g. ma_poilicy_timeout, ma.*, ma.policy.* etc.
 * @param subscribers_list			   [out]		Subscribers list. Don't forget to call ma_service_manager_subscriber_list_release().
 * @param count						   [out]		Number of subscribers registered for specified topic.
 *
 */
MA_SERVICE_MANAGER_API ma_error_t ma_service_manager_get_subscribers_async(ma_service_manager_t *service_manager, const char *topic_filter, ma_service_manager_get_subscribes_cb_t cb, void *cb_data);

/*
 * Releases Service Manager Handle 
 * @param service_manager			   [in]         Service Manager Handle.
 *
 */
MA_SERVICE_MANAGER_API ma_error_t ma_service_manager_release(ma_service_manager_t *service_manager);

/*
 * Releases Subscribers list Handle
 * @param subscriber_list			   [in]         Service Manager subscriber list Handle.
 *
 */
MA_SERVICE_MANAGER_API ma_error_t ma_service_manager_subscriber_list_release(ma_service_manager_subscriber_list_t *subscriber_list);

/*
 * Gets subscriber topic filter 
 * @param subscriber_list			   [in]         Service Manager subscriber list Handle.
 * @param index						   [in]			index of subscribers list 
 * @param topic						   [out]		Subscriber's Topic filter name.
 *
 */
MA_SERVICE_MANAGER_API ma_error_t ma_service_manager_subscriber_list_get_topic_name(ma_service_manager_subscriber_list_t *subscriber_list, ma_uint32_t index, const char **topic);

/*
 * Gets subscriber Product ID 
 * @param subscriber_list			   [in]         Service Manager subscriber list Handle.
 * @param index						   [in]			index of subscribers list 
 * @param product_id				   [out]		Product ID of the Subscriber.
 *
 */
MA_SERVICE_MANAGER_API ma_error_t ma_service_manager_subscriber_list_get_product_id(ma_service_manager_subscriber_list_t *subscriber_list, ma_uint32_t index, const char **product_id);

/*
 * Gets Service list 
 * @param service_manager			   [in]         Service Manager Handle.
 * @param service_name_filter		   [in]         Service name filter e.g. ma.policy.service, ma.policy.*, ma.* etc.
 * @param service_list			       [out]		Service list. Don't forget to call ma_service_manager_service_list_release().
 * @param count						   [out]		Number of services registered for specified service filter.
 *
 */
MA_SERVICE_MANAGER_API ma_error_t ma_service_manager_get_services(ma_service_manager_t *service_manager, const char *service_name_filter, ma_service_manager_service_list_t **service_list, size_t *count);

/*
 * Releases Services list Handle
 * @param service_list			   [in]         Service Manager service list Handle.
 *
 */
MA_SERVICE_MANAGER_API ma_error_t ma_service_manager_service_list_release(ma_service_manager_service_list_t *service_list);

/* 
 * Gets service name
 * @param service_list			       [in]         Service Manager service list Handle.
 * @param index						   [in]			index of service list 
 * @param service_name			       [out]		Service name.
 *
 */
MA_SERVICE_MANAGER_API ma_error_t ma_service_manager_service_list_get_service_name(ma_service_manager_service_list_t *service_list, ma_uint32_t index, const char **service_name);

/*
 * Gets service product ID 
 * @param service_list			       [in]         Service Manager service list Handle.
 * @param index						   [in]			index of service list 
 * @param product_id				   [out]		Product ID of the service.
 *
 */
MA_SERVICE_MANAGER_API ma_error_t ma_service_manager_service_list_get_product_id(ma_service_manager_service_list_t *service_list, ma_uint32_t index, const char **product_id);

MA_CPP(})    

#include "ma/dispatcher/ma_service_manager_dispatcher.h"

#endif /* MA_SERVICE_MANAGER_H_INCLUDED */

