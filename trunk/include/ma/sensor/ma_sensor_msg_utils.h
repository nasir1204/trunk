#ifndef MA_SENSOR_MSG_UTILS_H_INCLUDED
#define MA_SENSOR_MSG_UTILS_H_INCLUDED

#include "ma/sensor/ma_sensor_msg.h"

MA_CPP(extern "C" {)

ma_error_t ma_sensor_get_power_sensor_msg(ma_power_sensor_msg_t **msg);

ma_error_t ma_sensor_get_network_sensor_msg(ma_network_sensor_msg_t **msg);

MA_CPP(})

#endif /* MA_SENSOR_MSG_UTILS_H_INCLUDED */
