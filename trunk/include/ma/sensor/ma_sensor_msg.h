#ifndef MA_SENSOR_MSG_H_INCLUDED
#define MA_SENSOR_MSG_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/sensor/ma_sensor.h"
#include "ma/ma_buffer.h"
#include "ma/internal/utils/threading/ma_atomic.h"


MA_CPP(extern "C" {)
typedef struct ma_network_sensor_msg_s ma_network_sensor_msg_t;
typedef struct ma_idle_sensor_msg_s ma_idle_sensor_msg_t;
typedef struct ma_power_sensor_msg_s ma_power_sensor_msg_t;
typedef struct ma_system_sensor_msg_s ma_system_sensor_msg_t;
typedef struct ma_user_sensor_msg_s ma_user_sensor_msg_t;
typedef struct ma_user_info_s ma_user_info_t;
typedef struct ma_power_info_s ma_power_info_t;
typedef struct ma_idle_info_s ma_idle_info_t;
typedef struct ma_network_info_s ma_network_info_t;
typedef struct ma_system_info_s ma_system_info_t;

/*
typedef enum ma_user_logged_state_e ma_user_logged_state_t;
typedef enum ma_power_status_e ma_power_status_t;
typedef enum ma_battery_charge_status_e ma_battery_charge_status_t;
typedef enum ma_network_connectivity_status_e ma_network_connectivity_status_t;
*/

typedef enum ma_user_logged_state_e {
    MA_USER_LOGGED_STATE_OFF,
    MA_USER_LOGGED_STATE_ON,
}ma_user_logged_state_t;

typedef enum ma_power_status_e {
    MA_POWER_STATUS_OFFLINE = 0x00,
    MA_POWER_STATUS_ONLINE = 0x01,
    MA_POWER_STATUS_UNKNOWN = 0xff,
}ma_power_status_t;

typedef enum ma_battery_charge_status_e {
    MA_BATTERY_CHARGE_STATUS_HIGH = 0x01,
    MA_BATTERY_CHARGE_STATUS_LOW = 0x02,
    MA_BATTERY_CHARGE_STATUS_CRITICAL = 0x04,
    MA_BATTERY_CHARGE_STATUS_CHARGING = 0x08,
    MA_BATTERY_CHARGE_STATUS_NO_BATTERY = 0x80,
    MA_BATTERY_CHARGE_STATUS_UNKNOWN = 0xff
}ma_battery_charge_status_t;

typedef enum ma_network_connectivity_status_e {
    MA_NETWORK_CONNECTIVITY_STATUS_CONNECTED,
    MA_NETWORK_CONNECTIVITY_STATUS_DISCONNECTED,
}ma_network_connectivity_status_t;

ma_error_t ma_sensor_msg_set_logger(ma_logger_t *logger);
/* user sensor msg api(s) */
ma_error_t ma_user_sensor_msg_create(ma_user_sensor_msg_t **msg, size_t no_of_users);
ma_error_t ma_user_sensor_msg_get_size(ma_user_sensor_msg_t *msg, size_t *no_of_users);
ma_error_t ma_user_sensor_msg_set_size(ma_user_sensor_msg_t *msg, size_t no_of_msgs);
ma_error_t ma_user_sensor_msg_add_ref(ma_user_sensor_msg_t *msg);
ma_error_t ma_user_sensor_msg_release(ma_user_sensor_msg_t *msg);

ma_error_t ma_user_sensor_msg_set_event_type(ma_user_sensor_msg_t *msg, ma_sensor_event_type_t type);
ma_error_t ma_user_sensor_msg_get_event_type(ma_user_sensor_msg_t *msg, ma_sensor_event_type_t *type);

ma_error_t ma_user_sensor_msg_set_user(ma_user_sensor_msg_t *msg, size_t index, const char *user);
ma_error_t ma_user_sensor_msg_get_user(ma_user_sensor_msg_t *msg, size_t index, ma_buffer_t **user);
ma_error_t ma_user_sensor_msg_set_domain(ma_user_sensor_msg_t *msg, size_t index, const char *domain);
ma_error_t ma_user_sensor_msg_get_domain(ma_user_sensor_msg_t *msg, size_t index, ma_buffer_t **domain);
ma_error_t ma_user_sensor_msg_set_session_id(ma_user_sensor_msg_t *msg, size_t index, ma_uint32_t sid);
ma_error_t ma_user_sensor_msg_get_session_id(ma_user_sensor_msg_t *msg, size_t index, ma_uint32_t *sid);
ma_error_t ma_user_sensor_msg_set_logon_state(ma_user_sensor_msg_t *msg, ma_user_logged_state_t state);
ma_error_t ma_user_sensor_msg_get_logon_state(ma_user_sensor_msg_t *msg, ma_user_logged_state_t *state);
ma_error_t ma_user_sensor_msg_set_startup_flag(ma_user_sensor_msg_t *msg, ma_bool_t flag);
ma_error_t ma_user_sensor_msg_get_startup_flag(ma_user_sensor_msg_t *msg, ma_bool_t *flag);

ma_error_t ma_user_sensor_msg_is_user_wide_char(ma_user_sensor_msg_t *msg, size_t index, ma_uint32_t *is_wide_char);
ma_error_t ma_user_sensor_msg_is_domain_wide_char(ma_user_sensor_msg_t *msg, size_t index, ma_uint32_t *is_wide_char);

#if MA_HAS_WCHAR_T
ma_error_t ma_user_sensor_msg_set_wuser(ma_user_sensor_msg_t *msg, size_t index, const wchar_t *user);
ma_error_t ma_user_sensor_msg_get_wuser(ma_user_sensor_msg_t *msg, size_t index, ma_wbuffer_t **user);
ma_error_t ma_user_sensor_msg_set_wdomain(ma_user_sensor_msg_t *msg, size_t index, const wchar_t *domain);
ma_error_t ma_user_sensor_msg_get_wdomain(ma_user_sensor_msg_t *msg, size_t index, ma_wbuffer_t **domain);
#endif

/* system sensor msg api(s) */
ma_error_t ma_system_sensor_msg_create(ma_system_sensor_msg_t **msg, size_t no_of_msgs);
ma_error_t ma_system_sensor_msg_get_size(ma_system_sensor_msg_t *msg, size_t *no_of_msgs);
ma_error_t ma_system_sensor_msg_set_size(ma_system_sensor_msg_t *msg, size_t no_of_msgs);
ma_error_t ma_system_sensor_msg_add_ref(ma_system_sensor_msg_t *msg);
ma_error_t ma_system_sensor_msg_release(ma_system_sensor_msg_t *msg);

ma_error_t ma_system_sensor_msg_set_event_type(ma_system_sensor_msg_t *msg, ma_sensor_event_type_t type);
ma_error_t ma_system_sensor_msg_get_event_type(ma_system_sensor_msg_t *msg, ma_sensor_event_type_t *type);

/* idle sensor msg api(s) */
ma_error_t ma_idle_sensor_msg_create(ma_idle_sensor_msg_t **msg, size_t no_of_msgs);
ma_error_t ma_idle_sensor_msg_get_size(ma_idle_sensor_msg_t *msg, size_t *no_of_msgs);
ma_error_t ma_idle_sensor_msg_set_size(ma_idle_sensor_msg_t *msg, size_t no_of_msgs);
ma_error_t ma_idle_sensor_msg_add_ref(ma_idle_sensor_msg_t *msg);
ma_error_t ma_idle_sensor_msg_release(ma_idle_sensor_msg_t *msg);

ma_error_t ma_idle_sensor_msg_set_idle_wait(ma_idle_sensor_msg_t *msg, size_t index, int idle_wait);
ma_error_t ma_idle_sensor_msg_get_idle_wait(ma_idle_sensor_msg_t *msg, size_t index, int *idle_wait);

ma_error_t ma_idle_sensor_msg_set_event_type(ma_idle_sensor_msg_t *msg, ma_sensor_event_type_t type);
ma_error_t ma_idle_sensor_msg_get_event_type(ma_idle_sensor_msg_t *msg, ma_sensor_event_type_t *type);

/* network message api's */
ma_error_t ma_network_sensor_msg_create(ma_network_sensor_msg_t **msg, size_t no_of_msgs);
ma_error_t ma_network_sensor_msg_get_size(ma_network_sensor_msg_t *msg, size_t *no_of_msgs);
ma_error_t ma_network_sensor_msg_set_size(ma_network_sensor_msg_t *msg, size_t no_of_msgs);
ma_error_t ma_network_sensor_msg_add_ref(ma_network_sensor_msg_t *msg);
ma_error_t ma_network_sensor_msg_release(ma_network_sensor_msg_t *msg);

ma_error_t ma_network_sensor_msg_set_event_type(ma_network_sensor_msg_t *msg, ma_sensor_event_type_t type);
ma_error_t ma_network_sensor_msg_get_event_type(ma_network_sensor_msg_t *msg, ma_sensor_event_type_t *type);
ma_error_t ma_network_sensor_msg_set_connectivity_status(ma_network_sensor_msg_t *msg, ma_network_connectivity_status_t status);
ma_error_t ma_network_sensor_msg_get_connectivity_status(ma_network_sensor_msg_t *msg, ma_network_connectivity_status_t *status);

/* power sensor msg api(s) */
ma_error_t ma_power_sensor_msg_create(ma_power_sensor_msg_t **msg, size_t no_of_msgs);
ma_error_t ma_power_sensor_msg_get_size(ma_power_sensor_msg_t *msg, size_t *no_of_msgs);
ma_error_t ma_power_sensor_msg_set_size(ma_power_sensor_msg_t *msg, size_t no_of_msgs);
ma_error_t ma_power_sensor_msg_add_ref(ma_power_sensor_msg_t *msg);
ma_error_t ma_power_sensor_msg_release(ma_power_sensor_msg_t *msg);

ma_error_t ma_power_sensor_msg_set_event_type(ma_power_sensor_msg_t *msg, ma_sensor_event_type_t type);
ma_error_t ma_power_sensor_msg_get_event_type(ma_power_sensor_msg_t *msg, ma_sensor_event_type_t *type);

ma_error_t ma_power_sensor_msg_set_power_status(ma_power_sensor_msg_t *msg, size_t index, ma_power_status_t status);
ma_error_t ma_power_sensor_msg_get_power_status(ma_power_sensor_msg_t *msg, size_t index, ma_power_status_t *status);

ma_error_t ma_power_sensor_msg_set_battery_charge_status(ma_power_sensor_msg_t *msg, size_t index, ma_battery_charge_status_t status);
ma_error_t ma_power_sensor_msg_get_batter_charge_status(ma_power_sensor_msg_t *msg, size_t index, ma_battery_charge_status_t *status);

ma_error_t ma_power_sensor_msg_set_battery_lifetime(ma_power_sensor_msg_t *msg, size_t index, unsigned long secs);
ma_error_t ma_power_sensor_msg_get_battery_lifetime(ma_power_sensor_msg_t *msg, size_t index, unsigned long *secs);

ma_error_t ma_power_sensor_msg_set_battery_percent(ma_power_sensor_msg_t *msg, size_t index, int percentage);
ma_error_t ma_power_sensor_msg_get_battery_percent(ma_power_sensor_msg_t *msg, size_t index, int *percentage);

/* sensor msg api(s) */
ma_error_t ma_sensor_msg_create_from_network_sensor_msg(ma_network_sensor_msg_t *network_msg, ma_sensor_msg_t **sensor_msg);
ma_error_t ma_sensor_msg_get_network_sensor_msg(ma_sensor_msg_t *sensor_msg, ma_network_sensor_msg_t **network_msg);

ma_error_t ma_sensor_msg_create_from_user_sensor_msg(ma_user_sensor_msg_t *user_msg, ma_sensor_msg_t **sensor_msg);
ma_error_t ma_sensor_msg_get_user_sensor_msg(ma_sensor_msg_t *sensor_msg, ma_user_sensor_msg_t **user_msg);

ma_error_t ma_sensor_msg_create_from_idle_sensor_msg(ma_idle_sensor_msg_t *idle_msg, ma_sensor_msg_t **sensor_msg);
ma_error_t ma_sensor_msg_get_idle_sensor_msg(ma_sensor_msg_t *sensor_msg, ma_idle_sensor_msg_t **user_msg);

ma_error_t ma_sensor_msg_create_from_system_sensor_msg(ma_system_sensor_msg_t *system_msg, ma_sensor_msg_t **sensor_msg);
ma_error_t ma_sensor_msg_get_system_sensor_msg(ma_sensor_msg_t *sensor_msg, ma_system_sensor_msg_t **user_msg);

ma_error_t ma_sensor_msg_create_from_power_sensor_msg(ma_power_sensor_msg_t *power_msg, ma_sensor_msg_t **sensor_msg);
ma_error_t ma_sensor_msg_get_power_sensor_msg(ma_sensor_msg_t *sensor_msg, ma_power_sensor_msg_t **user_msg);

ma_error_t ma_sensor_msg_create(ma_sensor_msg_t **msg);
ma_error_t ma_sensor_msg_add_ref(ma_sensor_msg_t *msg);
ma_error_t ma_sensor_msg_release(ma_sensor_msg_t *msg);
ma_error_t ma_sensor_msg_get_type(ma_sensor_msg_t *msg, ma_sensor_type_t *type);
ma_error_t ma_sensor_msg_set_type(ma_sensor_msg_t *msg, ma_sensor_type_t type);

MA_CPP(})

#endif /* MA_SENSOR_MSG_H_INCLUDED */


