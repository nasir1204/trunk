#ifndef MA_USER_SENSOR_H_INCLUDED
#define MA_USER_SENSOR_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_msgbus.h"
#include "ma/sensor/ma_sensor_msg.h"

MA_CPP(extern "C" {)

typedef struct ma_user_sensor_s ma_user_sensor_t;

MA_SENSOR_API ma_error_t ma_user_sensor_create(ma_msgbus_t *msgbus, ma_user_sensor_t **user);

MA_SENSOR_API ma_error_t ma_user_sensor_release(ma_user_sensor_t *user);

MA_SENSOR_API ma_error_t ma_user_sensor_get_all_logged_on_users(ma_user_sensor_t *user, ma_user_sensor_msg_t **msg);

MA_CPP(})

#endif /* MA_USER_SENSOR_H_INCLUDED */
