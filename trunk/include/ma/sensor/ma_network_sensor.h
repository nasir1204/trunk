#ifndef MA_NETWORK_SENSOR_H_INCLUDED
#define MA_NETWORK_SENSOR_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_msgbus.h"

MA_CPP(extern "C" {)

typedef struct ma_network_sensor_s ma_network_sensor_t;

MA_SENSOR_API ma_error_t ma_network_sensor_create(ma_msgbus_t *msgbus, ma_network_sensor_t **power);

MA_SENSOR_API ma_error_t ma_network_sensor_release(ma_network_sensor_t *power);

MA_CPP(})

#endif /* MA_NETWORK_SENSOR_H_INCLUDED */

