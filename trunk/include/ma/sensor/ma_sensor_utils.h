#ifndef MA_SENSOR_UTILS_H_INCLUDED
#define MA_SENSOR_UTILS_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_variant.h"
#include "ma/ma_message.h"
#include "ma/sensor/ma_sensor.h"
#include "ma/sensor/ma_sensor_msg.h"

MA_CPP(extern "C" {)

ma_error_t ma_sensor_publish_msg(ma_msgbus_t *msgbus, const char *sensor, const char *topic, ma_message_t *msg);
ma_error_t convert_variant_to_user_sensor_msg(ma_variant_t *variant, ma_user_sensor_msg_t **msg);
ma_error_t convert_user_sensor_msg_to_variant(ma_user_sensor_msg_t *msg, ma_variant_t **msg_var);
ma_error_t convert_variant_to_power_sensor_msg(ma_variant_t *variant, ma_power_sensor_msg_t **msg);
ma_error_t convert_power_sensor_msg_to_variant(ma_power_sensor_msg_t *msg, ma_variant_t **msg_var);
ma_error_t convert_variant_to_network_sensor_msg(ma_variant_t *variant, ma_network_sensor_msg_t **msg);
ma_error_t convert_network_sensor_msg_to_variant(ma_network_sensor_msg_t *msg, ma_variant_t **msg_var);
ma_error_t convert_system_sensor_msg_to_variant(ma_system_sensor_msg_t *msg, ma_variant_t **msg_var);
ma_error_t convert_variant_to_system_sensor_msg(ma_variant_t *variant, ma_system_sensor_msg_t **msg);
ma_error_t convert_idle_sensor_msg_to_variant(ma_idle_sensor_msg_t *msg, ma_variant_t **msg_var);
ma_error_t convert_variant_to_idle_sensor_msg(ma_variant_t *variant, ma_idle_sensor_msg_t **msg);


MA_CPP(})

#endif /* MA_SENSOR_UTILS_H_INCLUDED */

