#ifndef MA_SYSTEM_SENSOR_H_INCLUDED
#define MA_SYSTEM_SENSOR_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_msgbus.h"

MA_CPP(extern "C" {)

typedef struct ma_system_sensor_s ma_system_sensor_t;

MA_SENSOR_API ma_error_t ma_system_sensor_create(ma_msgbus_t *msgbus, ma_system_sensor_t **system);

MA_SENSOR_API ma_error_t ma_system_sensor_release(ma_system_sensor_t *system);

MA_CPP(})

#endif /* MA_SYSTEM_SENSOR_H_INCLUDED */
