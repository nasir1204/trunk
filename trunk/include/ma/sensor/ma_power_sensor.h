#ifndef MA_POWER_SENSOR_H_INCLUDED
#define MA_POWER_SENSOR_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_msgbus.h"

MA_CPP(extern "C" {)

typedef struct ma_power_sensor_s ma_power_sensor_t;

MA_SENSOR_API ma_error_t ma_power_sensor_create(ma_msgbus_t *msgbus, ma_power_sensor_t **power);

MA_SENSOR_API ma_error_t ma_power_sensor_release(ma_power_sensor_t *power);

MA_CPP(})

#endif /* MA_POWER_SENSOR_H_INCLUDED */

