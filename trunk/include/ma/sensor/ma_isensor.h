#ifndef MA_ISENSOR_H_INCLUDED
#define MA_ISENSOR_H_INCLUDED

#include "ma/ma_common.h"

MA_CPP(extern "C"{)

typedef struct ma_sensor_methods_s ma_sensor_methods_t;

#undef MA_SENSOR_ACTUAL_ARG_EXPANDER
#define MA_SENSOR_ACTUAL_ARG_EXPANDER\
    MA_SENSOR_ARG_EXPANDER(ma_error_t,ma_sensor_on_msg,(ma_sensor_t *sensor, ma_sensor_msg_t *msg),(sensor, msg))\
    MA_SENSOR_ARG_EXPANDER(ma_error_t,ma_sensor_get_type,(ma_sensor_t *sensor, ma_sensor_type_t *type),(sensor, type))\
    MA_SENSOR_ARG_EXPANDER(ma_error_t,ma_sensor_get_id,(ma_sensor_t *sensor, const char **id),(sensor, id))\
    MA_SENSOR_ARG_EXPANDER(ma_error_t,ma_sensor_start,(ma_sensor_t *sensor),(sensor))\
    MA_SENSOR_ARG_EXPANDER(ma_error_t,ma_sensor_stop,(ma_sensor_t *sensor),(sensor))\
    MA_SENSOR_ARG_EXPANDER(ma_error_t,ma_sensor_release,(ma_sensor_t *sensor),(sensor))\
    MA_SENSOR_ARG_EXPANDER(ma_error_t,ma_sensor_detect,(ma_sensor_t *sensor),(sensor))


struct ma_sensor_s {
    const ma_sensor_methods_t *methods;
    int ref_count; /*Ref count*/
    void *data;
};

struct ma_sensor_methods_s {
#define MA_SENSOR_ARG_EXPANDER(ret_type, function, signature, arg) ret_type (*function##_fn) signature;
    MA_SENSOR_ACTUAL_ARG_EXPANDER
#undef MA_SENSOR_ARG_EXPANDER
};

static MA_INLINE void ma_sensor_init(ma_sensor_t *sensor, const ma_sensor_methods_t *methods, void *data) {
    if(sensor) {
        sensor->methods = methods;
        sensor->data = data;
    }
}

#define MA_SENSOR_ARG_EXPANDER(ret_type, function, signature, arg)\
    MA_STATIC_INLINE ret_type function signature {\
        if(sensor && sensor->methods) {\
            return sensor->methods->function##_fn?sensor->methods->function##_fn arg:MA_ERROR_PRECONDITION;\
        }\
        return MA_ERROR_INVALIDARG;\
    }
    MA_SENSOR_ACTUAL_ARG_EXPANDER
#undef MA_SENSOR_ARG_EXPANDER



MA_SENSOR_API ma_error_t ma_sensor_inc_ref(ma_sensor_t *self);

MA_SENSOR_API ma_error_t ma_sensor_dec_ref(ma_sensor_t *self);

MA_SENSOR_API const char* ma_sensor_type_to_str(ma_sensor_type_t type);

MA_SENSOR_API const char* ma_sensor_event_type_to_str(ma_sensor_event_type_t type);

MA_SENSOR_API ma_error_t ma_sensor_set_logger(ma_logger_t *logger);

MA_CPP(})

#endif /* MA_ISENSOR_H_INCLUDED */


