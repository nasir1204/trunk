#ifndef MA_SENSOR_TRAY_H_INCLUDED
#define MA_SENSOR_TRAY_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_log.h"
#include "ma/ma_msgbus.h"
#include "ma/sensor/ma_sensor.h"

#define MA_DETECTOR_DEFAULT_REPEAT_INTERVAL     60 /* 1 minute */

MA_CPP(extern "C" {)
typedef struct ma_sensor_tray_s ma_sensor_tray_t;

MA_SENSOR_API ma_error_t ma_sensor_tray_create(ma_msgbus_t *msgbus, ma_sensor_tray_t **tray);

MA_SENSOR_API ma_error_t ma_sensor_tray_add(ma_sensor_tray_t *tray, ma_sensor_t *sensor);

MA_SENSOR_API ma_error_t ma_sensor_tray_remove(ma_sensor_tray_t *tray, ma_sensor_t *sensor);

MA_SENSOR_API ma_error_t ma_sensor_tray_find(ma_sensor_tray_t *tray, ma_sensor_t *senor, ma_bool_t *exist);

MA_SENSOR_API ma_error_t ma_sensor_tray_find_by_type(ma_sensor_tray_t *tray, ma_sensor_type_t type, ma_sensor_t **sensor);

MA_SENSOR_API ma_error_t ma_sensor_tray_release(ma_sensor_tray_t *tray);

MA_CPP(})

#endif /* MA_SENSOR_TRAY_H_INCLUDED */
