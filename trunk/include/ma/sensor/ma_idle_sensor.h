#ifndef MA_IDLE_SENSOR_H_INCLUDED
#define MA_IDLE_SENSOR_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_msgbus.h"

MA_CPP(extern "C" {)

typedef struct ma_idle_sensor_s ma_idle_sensor_t;

MA_SENSOR_API ma_error_t ma_idle_sensor_create(ma_msgbus_t *msgbus, ma_idle_sensor_t **idle);

MA_SENSOR_API ma_error_t ma_idle_sensor_release(ma_idle_sensor_t *idle);

MA_CPP(})

#endif /* MA_IDLE_SENSOR_H_INCLUDED */

