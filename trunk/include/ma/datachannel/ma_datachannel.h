/** @file ma_datachannel.h
 *  @brief Data Channel Declarations
 *
*/
#ifndef MA_DATACHANNEL_H_INCLUDED
#define MA_DATACHANNEL_H_INCLUDED

#include "ma/datachannel/ma_datachannel_item.h"
#include "ma/ma_variant.h"
#include "ma/ma_client.h"

MA_CPP(extern "C" {)

/**
* Notification Type 
* Represents the type of notification when a DataChannel item is sent to the server.
*/
typedef enum ma_datachannel_item_notification_e {
	MA_DC_NOTIFICATION_NOSTATUS,
	MA_DC_NOTIFICATION_DELIVERED,
	MA_DC_NOTIFICATION_PURGED,
} ma_datachannel_item_notification_t;

/**
*  Subscription Type 
*  Represents the type of subscription a client wants to listen for DataChannel messages from ePO.
*   
*/
typedef enum ma_datachannel_subscription_type_e {
/**
 *  LISTENER SUBSCRIBER
 *  Client subscribed as a Listener Subscriber receives only the datachannel messages between the start and end of a subscription.
 */
	MA_DC_SUBSCRIBER_LISTENER, 
/**
 *  PRIMARY SUBSCRIBER
 *  Client subscribed as a Primary is a Listener Subscriber but also receives any DataChannel message that was received prior to subscription. The message 
 *  received from ePO should be persistable(if not, it would be discarded if no subscribers are present).
 */
  MA_DC_SUBSCRIBER_PRIMARY,
}ma_datachannel_subscription_type_t;

/**
* Callback that receives any notification of the delivery of DataChannel messages to ePO.
* @param ma_client			[in] 	the ma_client object 
* @param product_id 		[in]	the productID that is used as the origin of datachannel messages
* @param message_id		[in]	the name representing the type of DataChannel message that is registered on the server extension
* @param correlation_id	[in]	the id representing the message that was sent to the ePO using ma_datachannel_async_send.
* @param status			[in]	the type of notification. Either it was successfully delivered or purged
* @param reason			[in]	error code associated if the item was purged.
* @param cb_data			[in]	the callback data that was provided as part of register notification callback.
* @remark		
* Notifications will be received only when a callback of this signature is registered using ma_datachannel_register_notification_callback.
* Notification will be stopped when unregistering with ma_datachannel_unregister_notification_callback
*   
*/
typedef void (*ma_datachannel_notification_cb_t)(ma_client_t *ma_client, const char *product_id, const char *message_id, ma_int64_t correlation_id, ma_datachannel_item_notification_t status, ma_error_t reason, void *cb_data);

/**
*   Callback that receives DataChannel messages from the ePO if the client is subscribed to message_id.
*   @param ma_client		[in] 	the ma_client object 
*   @param product_id 		[in]	the productID that is used as the origin of datachannel messages
*   @param message_id		[in]	the name representing the type of DataChannel message that is registered on the server extension
*   @param correlation_id	[in]	the id representing the message that was sent to the ePO using ma_datachannel_async_send.
*   @param dc_message		[in]	the datachannel item received from the server (Contains the data)
*   @param cb_data			[in]	the callback data that will be supplied which was registered when registering this callback.
*   @remark	
*	When the client is subscribed to message_id using ma_datachannel_subscribe, messages will be given to the client using this callback.
*	Do not release the datachannel item given here, once the callback returns it is already taken care.
*/
typedef void (*ma_datachannel_on_message_cb_t )(ma_client_t *ma_client, const char *product_id, const char *message_id, ma_int64_t correlation_id, ma_datachannel_item_t *dc_message, void *cb_data);

/**
* This registers the notification callback which receives notification of datachannel items which are marked for notification.
* @param ma_client		[in]	the ma_client object
* @param product_id		[in]	Your products SOFTWAREID
* @param notify_cb		[in]	user's callback 
* @param cb_data			[in]	user's data, this will be provided in the callback. This is opaque to MA
* @result					MA_OK on success
* 							MA_ERROR_ALREADY_REGISTERED if the product id already has a callback registered 
*							MA_ERROR_INVALID_CLIENT_OBJECT if client is not configured properly
*							MA_ERROR_CLIENT_NOT_FOUND if the callback was not registered for the provided software id.
*/
MA_DATACHANNEL_API ma_error_t ma_datachannel_register_notification_callback(ma_client_t *ma_client, const char *product_id, ma_datachannel_notification_cb_t notify_cb, void *cb_data);

/**
* This unregisters the notification callback for the specified product id
* @param ma_client		[in]	the ma_client object
* @param product_id		[in]	Your products SOFTWAREID
* @result					MA_OK on success
*							MA_ERROR_OUTOFMEMORY if there is less memory
*/

MA_DATACHANNEL_API ma_error_t ma_datachannel_unregister_notification_callback(ma_client_t *ma_client, const char *product_id);

/**
* This registers the message handler callback which receives messages from the ePO. 
* @param ma_client		[in]	the ma_client object
* @param product_id		[in]	Your products SOFTWAREID
* @param notify_cb		[in]	user's callback 
* @param cb_data			[in]	user's data, this will be provided in the callback. This is opaque to MA
* @result					MA_OK on success
*							MA_ERROR_ALREADY_REGISTERED if the product id already has a callback registered 
*							MA_ERROR_INVALID_CLIENT_OBJECT if client is not configured properly
*							MA_ERROR_CLIENT_NOT_FOUND if the callback was not registered for the provided software id.
* @remark					This callback is called whenever the user subscribes for a message id that is expected to reach down from the ePO.
*/
MA_DATACHANNEL_API ma_error_t ma_datachannel_register_message_handler_callback(ma_client_t *ma_client, const char *product_id, ma_datachannel_on_message_cb_t on_message_cb,void *cb_data);

/**
* This unregisters the message handler callback that was previously registered
* @param	ma_client		[in]	the ma_client object
* @param	product_id		[in]	Your product SOFTWAREID
* @result					MA_OK on success
*							MA_ERROR_INVALID_CLIENT_OBJECT if client is not configured properly
*							MA_ERROR_CLIENT_NOT_FOUND if the callback was not registered for the provided software id.
*	
*/
MA_DATACHANNEL_API ma_error_t ma_datachannel_unregister_message_handler_callback(ma_client_t *ma_client, const char *product_id);

/**
* This API is used to start listening for DataChannel messages based on the dc_message_id
* @param	ma_client			[in]	the ma_client object
* @param	product_id			[in]	Your product's SOFTWAREID
* @param	dc_message_id		[in]	A Name must be between 1 to 255 Unicode characters long. The data channel does not maintain private namespaces for each participating product.
*										There is one global namespace.To preserve private namespaces and/or prevent naming collisions with other participants, every item must use the following format:
*										* <Software_ID> � The assigned, unique software ID for your product.
*										* '_' � an underscore character.
*										* <user-defined name> � a name/identifier meaningful for your purpose.
* @param	subscription_type	[in]	Specify the type of subscription (Primary or Listener).
*/
MA_DATACHANNEL_API ma_error_t ma_datachannel_subscribe(ma_client_t *ma_client, const char *product_id, const char *dc_message_id, ma_datachannel_subscription_type_t subscription_type);

/**
* This API is used to stop listening for DataChannel messages for specified dc_message_id
* @param	ma_client			[in]	the ma_client object
* @param	product_id			[in]	Your product's SOFTWAREID
* @param	dc_message_id		[in]	The name for the DataChannel message.
* @return						MA_OK on success
*								MA_ERROR_INVALID_CLIENT_OBJECT when invalid client is passed.
*								MA_ERROR_OBJECTNOTFOUND if the message id is not subscribed earlier.
*/
MA_DATACHANNEL_API ma_error_t ma_datachannel_unsubscribe(ma_client_t *ma_client, const char *product_id, const char *dc_message_id);

/**
* @param ma_client			[in]	the ma_client object
* @param product_id			[in]	Your product's SOFTWAREID. This will be viewed as the origin on the ePO. Notifications and Message handlers are seperate for each product id.
* @param dc_message			[in]	The datachannel item with configured options and data that should be uploaded to the server.
* @return						MA_OK on success
*								MA_ERROR_INVALID_CLIENT_OBJECT when invalid client is provided
* @remark	
*	The async send does not have a return callback for success or failure of send. If the item that was attempted to be sent has been configured for purge notifications
*   the notification callback is invoked (if registered) along with the reason for the failure for which the item was purged. The dc_message must be freed by the user.
*	Async send will make a copy of its own and use that. Items will be attempted to be uploaded by the server for atleast once, and will be reattempted if configured to be
*	be persisted until a specified time to live.
*/
MA_DATACHANNEL_API ma_error_t ma_datachannel_async_send(ma_client_t *ma_client, const char *product_id, ma_datachannel_item_t *dc_message);

#include "ma/dispatcher/ma_datachannel_dispatcher.h"
MA_CPP(})

#endif
