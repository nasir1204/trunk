/** @file ma_datachannel_item.h
 *  @brief Data Channel Item Declarations
 *
*/
#ifndef MA_DATACHANNEL_MESSAGE_H_INCLUDED
#define MA_DATACHANNEL_MESSAGE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_buffer.h"
#include "ma/ma_variant.h"
#include "ma/ma_message.h"

MA_CPP( extern "C" {)

typedef struct ma_datachannel_item_s ma_datachannel_item_t, *ma_datachannel_item_h;

#define MA_DATACHANNEL_DEFAULT					0x0
#define MA_DATACHANNEL_PERSIST					0x1
#define MA_DATACHANNEL_PURGED_NOTIFICATION		0x2
#define MA_DATACHANNEL_DELIVERY_NOTIFICATION	0x4
#define MA_DATACHANNEL_ENCRYPT					0x8

/**
*	Creates a datachannel item(message) that must be uploaded to the server.
*	@param message_type			[in]	the name of the datachannel item that must be uploaded
*										A Name must be between 1 to 255 Unicode characters long. The data channel does not maintain private namespaces for each participating product.
*										There is one global namespace.To preserve private namespaces and/or prevent naming collisions with other participants, every item must use the following format:
*										* <Software_ID> � The assigned, unique software ID for your product.
*										* '_' � an underscore character.
*										* <user-defined name> � a name/identifier meaningful for your purpose.
*	@param payload				[in]	The data that is created as a variant that must be uploaded to the ePO. Currently String and Raw variant formats are supported.
*	@param datachannel_message	[out]	The object representing the datachannel message that must be uploaded to the ePO.
*	@result						MA_OK on success
*								MA_ERROR_INVALID_OPTION if invalid message_type is supplied.
*/
MA_DATACHANNEL_API ma_error_t ma_datachannel_item_create(const char *message_type, ma_variant_t *payload, ma_datachannel_item_t **datachannel_message);

/**
*	Frees up the memory allocated for the datachannel item
*	@param dc_item		[in]	the datachannel item
*	@return				MA_OK on success
*/
MA_DATACHANNEL_API ma_error_t ma_datachannel_item_release(ma_datachannel_item_t *dc_item);

/**
*	Retreives the message type from the datachannel items
*	@param dc_item		[in]	the datachannel item
*	@param buffer		[in]	string buffer which contains the message type as string buffer.
*	@return				MA_OK on success
*						MA_ERROR_INVALIDARG if invalid args are supplied
*/
MA_DATACHANNEL_API ma_error_t ma_datachannel_item_get_message_type(ma_datachannel_item_t *dc_item, ma_buffer_t **buffer);

/**
*	Retreives the message type from the datachannel items
*	@param dc_item		[in]	the datachannel item
*	@param buffer		[out]	string buffer which contains the message type as string buffer.
*	@return				MA_OK on success
*						MA_ERROR_INVALIDARG if invalid args are supplied
*/
MA_DATACHANNEL_API ma_error_t ma_datachannel_item_get_origin(ma_datachannel_item_t *dc_item, ma_buffer_t **buffer);

/**
*	Retreives the message type from the datachannel items
*	@param dc_item		[in]	the datachannel item
*	@param payload		[out]	variant that contains the data
*	@return				MA_OK on success
*						MA_ERROR_INVALIDARG if invalid args are supplied
*/
MA_DATACHANNEL_API ma_error_t ma_datachannel_item_get_payload(ma_datachannel_item_t *dc_item, ma_variant_t **payload);

/**
*	Retreives the message type from the datachannel items
*	@param dc_item				[in]	the datachannel item
*	@param ttl_in_seconds		[out]	variable to get the time to live in seconds.
*	@return				MA_OK on success
*						MA_ERROR_INVALIDARG if invalid args are supplied
*/
MA_DATACHANNEL_API ma_error_t ma_datachannel_item_get_ttl(ma_datachannel_item_t *dc_item, ma_int64_t *ttl_in_seconds);

/**
*	Retreives the correlation id from the datachannel items
*	@param dc_item				[in]	the datachannel item
*	@param correlation_id		[out]	variable to retreive the correlation id.
*	@return				MA_OK on success
*						MA_ERROR_INVALIDARG if invalid args are supplied
*/
MA_DATACHANNEL_API ma_error_t ma_datachannel_item_get_correlation_id(ma_datachannel_item_t *dc_item, ma_int64_t *correlation_id);

/**
*	Sets the origin for the datachannel item.
*	@param dc_item		[in]	the datachannel item
*	@param origin		[in]	1-255 length string that represents the origin from where the datachannel item is sent.
*	@return				MA_OK on success
*						MA_ERROR_INVALIDARG if invalid args are supplied
*	@remark		
*	This is completely optional, if not set for datachannel item, at time of doing a ma_datachannel_async_send, the product id will be used as origin.
*/
MA_DATACHANNEL_API ma_error_t ma_datachannel_item_set_origin(ma_datachannel_item_t *dc_item, const char *origin);

/**
*	Set the Payload for the datachannel item
*	@param dc_item		[in]	the datachannel item
*	@param payload		[in]	The String/Raw variant
*	@return				MA_OK on success
*						MA_ERROR_DATACHANNEL_BAD_REQUEST if its not string or raw data.
*						MA_ERROR_DATACHANNEL_MESSAGE_TOO_LARGE if data size is greater than ~9.8MB
*						MA_ERROR_INVALIDARG if invalid args are supplied
*	@remark	Due to limitations in size that can be uploaded through the SPIPE package no data can be uploaded more than approximately 9.8 MB per datachannel item.
*	If you wish to upload more than the size, you can break the data into chunks and use correlation id to sequence the chunks at the ePO side.
*/
MA_DATACHANNEL_API ma_error_t ma_datachannel_item_set_payload(ma_datachannel_item_t *dc_item, ma_variant_t *payload);

/**
*	Sets the time to live for the datachannel item
*	@param dc_item				[in]	the datachannel item
*	@param ttl_in_seconds		[in]	the time to live in seconds
*	@return				MA_OK on success
*						MA_ERROR_INVALIDARG if invalid args are supplied
*	@remark			
*	This information is used by server to keep the item alive before purging it. This is used on items that have the MA_DATACHANNEL_PERSIST flag set.
*	for non-persistant messages, this is ignored and item is purged immediately on failure to upload. 
*	If ttl is set to 0 and item is persistable the item will be kept alive for a maximum of 1 week after which the item will be purged and notification will be sent.
*/
MA_DATACHANNEL_API ma_error_t ma_datachannel_item_set_ttl(ma_datachannel_item_t *dc_item, ma_int64_t ttl_in_seconds);

/**
*	Sets the correlation id for the datachannel item
*	@param dc_item			[in]	the datachannel item
*	@param correlation_id	[in]	a integer that helps in identifying the message at the ePO end for ensuring reliablity of messages
*	@return				MA_OK on success
*						MA_ERROR_INVALIDARG if invalid args are supplied
*	@remark	currently for ePO 5.1.1, the correlation id is restricted to 32-bit number. Any number larger than this will be truncated. 
*	The 64-bit is reserved for future use incase DataChannel SPIPE protocol allows 64 bit numbers to be transmitted.
*/
MA_DATACHANNEL_API ma_error_t ma_datachannel_item_set_correlation_id(ma_datachannel_item_t *dc_item, ma_int64_t correlation_id);

/**
*	Sets the option for the datachannel items
*	@param dc_item		[in]	the datachannel item
*	@param option		[in]	MA_DATACHANNEL_PERSIST makes the datachannel item to be persisted
*								MA_DATACHANNEL_ENCRYPT will allow the datachannel item to be encrypted when persisted on to disk.
*								MA_DATACHANNEL_PURGED_NOTIFICATION will allow the client to receive notification when item is purged.
*								MA_DATACHANNEL_DELIVERY_NOTIFICATION will allow the client to receive notification when item is delivered to ePO successfully.
*	@param b_enable		[in]	MA_TRUE or MA_FALSE to enable the flags
*	@return				MA_OK on success
*						MA_ERROR_INVALIDARG if invalid args are supplied
*	@remark
*	MA_DATACHANNEL_PERSIST will make the item to be persisted on to the MA filesystem if there is some issue where a reattempt has to be made(ranging from network disconnection to server downtime etc.)
*	MA_DATACHANNEL_ENCRYPT will encrypt the item when stored by MA into its filesystem. If any sensitive/confidential data is being transmitted this flag MUST be enabled. Enabling this for non-sensitive information will lead to degradation in performance due to constant encryption and decrpytion of data.
*	MA_DATACHANNEL_PURGED_NOTIFICATION if no notification is registered then this flag will be cleared.
*/
MA_DATACHANNEL_API ma_error_t ma_datachannel_item_set_option(ma_datachannel_item_t *dc_item, ma_uint32_t option, ma_bool_t b_enable);

/**
*	Retreives the origin from the datachannel item
*	@param dc_item		[in]	the datachannel item
*	@param option		[out]	the options that are set for the DataChannel item.
*	@return				MA_OK on success
*						MA_ERROR_INVALIDARG if invalid args are supplied
*/
MA_DATACHANNEL_API ma_error_t ma_datachannel_item_get_option(ma_datachannel_item_t *dc_item, ma_uint32_t *option);

MA_CPP(})
#endif
