/** @file ma_variant.h
 *  @brief Variant Declarations
 *
 * This file contains prototypes for the variant library
 * Variant Library represents the datatypes - ma_bool_t, int, float, double, raw, string, table & array
 * Each respective type have the creator and getter.
 * The deletion and memory management will be done using the
 * reference counting technique with the count value in the variant structure.
*/

#ifndef MA_VARIANT_H_INCLUDED
#define MA_VARIANT_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_buffer.h"

MA_CPP(extern "C" {)

#define VARIANTLIBRARYVER 0x05020100

/**
 * @brief ma_vartype_t - The various variant types available
 */
typedef enum ma_vartype_e {
    MA_VARTYPE_NULL   = 0x00,
    MA_VARTYPE_UINT64 = 0x01,
    MA_VARTYPE_INT64  = 0x02,
    MA_VARTYPE_DOUBLE = 0x03,
    MA_VARTYPE_UINT32 = 0x04,
    MA_VARTYPE_INT32  = 0x05,
    MA_VARTYPE_FLOAT  = 0x06,
    MA_VARTYPE_UINT16 = 0x07,
    MA_VARTYPE_INT16  = 0x08,
    MA_VARTYPE_UINT8  = 0x09,
    MA_VARTYPE_INT8   = 0x0A,
    MA_VARTYPE_BOOL   = 0x0B,
    MA_VARTYPE_TABLE  = 0x0C,
    MA_VARTYPE_ARRAY  = 0x0D,
    MA_VARTYPE_RAW    = 0x0E,
    MA_VARTYPE_STRING = 0x0F,
} ma_vartype_t;

/**
 * @brief variant structure that represents the ma_vartype_t type
 */
typedef struct ma_variant_s ma_variant_t, *ma_variant_h;

/**
 * @brief variant table handle
 */
typedef struct ma_table_s ma_table_t, *ma_table_h;

/**
 * @brief variant array handle
 */
typedef struct ma_array_s ma_array_t, *ma_array_h;

/** @brief Returns ma_variant library version
 *  @param [out] version The address to which the current Variant
 *                       library version will be stored
 *  @return              MA_OK on success
 *                       MA_ERROR_INVALIDARG on invalid argument passed
 */
MA_VARIANT_API ma_error_t ma_variant_get_version(unsigned int *version);

/** @brief Creates variant object
 *  @param [out] variant Pointer to memory location that receives the newly constructed variant
 *  @return              MA_OK on success
 *                       MA_ERROR_INVALIDARG on invalid argument passed
 *                       MA_ERROR_OUTOFMEMORY on memory allocation failure
 */
MA_VARIANT_API ma_error_t ma_variant_create(ma_variant_t **variant);

/** @brief Increments reference count of the given variant
 *  @param  [in] variant variant object
 *  @return           MA_OK on success
 *                    MA_ERROR_INVALIDARG on invalid argument passed
 */
MA_VARIANT_API ma_error_t ma_variant_add_ref(ma_variant_t *variant);

/** @brief Decrements reference count of the given variant,
 *         if count reaches to 0 to release variant allocated memory
 *  @param  [in] variant variant object
 *  @return           MA_OK on success
 *                    MA_ERROR_INVALIDARG on invalid argument passed
 */
MA_VARIANT_API ma_error_t ma_variant_release(ma_variant_t *variant);

/** @brief compares the equality of given two variants by type
 *         size and content
 *  @param  [in]  first  variant
 *  @param  [in]  second variant
 *  @param  [out] result The address to which comparison results will be stored
                         MA_TRUE on match otherwise MA_FALSE
 *  @return              MA_OK on success
 *                       MA_ERROR_INVALIDARG on invalid argument passed
 */
MA_VARIANT_API ma_error_t ma_variant_is_equal(const ma_variant_t *first, const ma_variant_t *second, ma_bool_t *result);

/** @brief Returns type of the variant
 *  @param  [in]  variant   variant object
 *  @param  [out] var_type  The address of vartype variable to which variant type will be stored
 *  @return                 MA_OK on success 
 *                          MA_ERROR_INVALIDARG on invalid argument passed
 *  @NOTE					ma_variant_get_type is DEPRECATED API to check its type, use ma_variant_is_type instead.
 */
MA_VARIANT_API ma_error_t ma_variant_get_type(const ma_variant_t *variant, ma_vartype_t *var_type);

/** @brief check variant type
 *  @param  [in]  variant   variant object
 *  @param  [in] var_type   type to check.
 *  @return                 MA_OK on match, if variant value can be retrieved as var_type.
 *                          MA_ERROR_INVALIDARG on invalid argument passed
 *                          MA_ERROR_TYPE_MISMATCH on type mismatch.
 */
MA_VARIANT_API ma_error_t ma_variant_is_type(const ma_variant_t *variant, ma_vartype_t var_type);

/** @brief Creates unsigned 64-bit integer variant
 *  @param  [in]  value     64-bit unsigned integer value
 *  @param  [out] variant   Pointer to memory location that receives the
 *                          newly constructed variant
 *  @return                 MA_OK on success
 *                          MA_ERROR_INVALIDARG on invalid argument passed
 *                          MA_ERROR_OUTOFMEMORY on memory allocation failure
 */
MA_VARIANT_API ma_error_t ma_variant_create_from_uint64(ma_uint64_t value, ma_variant_t **variant);

/** @brief Fetches 64-bit unsigned integer value from given variant by checking
 *         type
 *  @param  [in]  variant      variant
 *  @param  [out] value     The address to which value will be stored
 *  @return                 MA_OK on success
 *                          MA_ERROR_INVALIDARG on invalid argument passed
 *                          MA_ERROR_PRECONDITION on variant type mismatch
 */
MA_VARIANT_API ma_error_t ma_variant_get_uint64(const ma_variant_t *variant, ma_uint64_t *value);

/** @brief Creates signed 64-bit integer variant
 *  @param  [in]  value     64-bit signed integer value
 *  @param  [out] variant   Pointer to memory location that receives the
 *                          newly constructed variant
 *  @return                 MA_OK on success
 *                          MA_ERROR_INVALIDARG on invalid argument passed
 *                          MA_ERROR_OUTOFMEMORY on memory allocation failure
 */
MA_VARIANT_API ma_error_t ma_variant_create_from_int64(ma_int64_t value, ma_variant_t **variant);

/** @brief Fetches 64-bit signed integer value from given variant by checking
 *         type
 *  @param  [in]  variant      variant
 *  @param  [out] value     The address to which value will be stored
 *  @return                 MA_OK on success
 *                          MA_ERROR_INVALIDARG on invalid argument passed
 *                          MA_ERROR_PRECONDITION on variant type mismatch
 */
MA_VARIANT_API ma_error_t ma_variant_get_int64(const ma_variant_t *variant, ma_int64_t *value);

/** @brief Creates variant for type double
 *  @param  [in]  value     Holds double value
 *  @param  [out] variant   Pointer to memory location that receives the
 *                          newly constructed variant
 *  @return                 MA_OK on success
 *                          MA_ERROR_INVALIDARG on invalid argument passed
 *                          MA_ERROR_OUTOFMEMORY on memory allocation failure
 */
MA_VARIANT_API ma_error_t ma_variant_create_from_double(double value, ma_variant_t **variant);

/** @brief Fetches double value from given variant by checking
 *         type
 *  @param  [in]  variant      variant
 *  @param  [out] value     The address to which value will be stored
 *  @return                 MA_OK on success
 *                          MA_ERROR_INVALIDARG on invalid argument passed
 *                          MA_ERROR_PRECONDITION on variant type mismatch
 */
MA_VARIANT_API ma_error_t ma_variant_get_double(const ma_variant_t *variant, double *value);

/** @brief Creates unsigned 32-bit integer variant
 *  @param  [in]  value     32-bit unsigned integer value
 *  @param  [out] variant   Pointer to memory location that receives the
 *                          newly constructed variant
 *  @return                 MA_OK on success
 *                          MA_ERROR_INVALIDARG on invalid argument passed
 *                          MA_ERROR_OUTOFMEMORY on memory allocation failure
 */
MA_VARIANT_API ma_error_t ma_variant_create_from_uint32(ma_uint32_t value, ma_variant_t **variant);

/** @brief Fetches 32-bit unsigned integer value from given variant by checking
 *         type
 *  @param  [in]  variant      variant
 *  @param  [out] value     The address to which value will be stored
 *  @return                 MA_OK on success
 *                          MA_ERROR_INVALIDARG on invalid argument passed
 *                          MA_ERROR_PRECONDITION on variant type mismatch
 */
MA_VARIANT_API ma_error_t ma_variant_get_uint32(const ma_variant_t *variant, ma_uint32_t *value);


/** @brief Creates signed 32-bit integer variant
 *  @param  [in]  value     32-bit signed integer value
 *  @param  [out] variant   Pointer to memory location that receives the
 *                          newly constructed variant
 *  @return                 MA_OK on success
 *                          MA_ERROR_INVALIDARG on invalid argument passed
 *                          MA_ERROR_OUTOFMEMORY on memory allocation failure
 */
MA_VARIANT_API ma_error_t ma_variant_create_from_int32(ma_int32_t value, ma_variant_t **variant);

/** @brief Fetches 32-bit signed integer value from given variant by checking
 *         type
 *  @param  [in]  variant      variant
 *  @param  [out] value     The address to which value will be stored
 *  @return                 MA_OK on success
 *                          MA_ERROR_INVALIDARG on invalid argument passed
 *                          MA_ERROR_PRECONDITION on variant type mismatch
 */
MA_VARIANT_API ma_error_t ma_variant_get_int32(const ma_variant_t *variant, ma_int32_t *value);


/** @brief Creates variant for type float
 *  @param  [in]  value     Holds float value
 *  @param  [out] variant   Pointer to memory location that receives the
 *                          newly constructed variant
 *  @return                 MA_OK on success
 *                          MA_ERROR_INVALIDARG on invalid argument passed
 *                          MA_ERROR_OUTOFMEMORY on memory allocation failure
 */
MA_VARIANT_API ma_error_t ma_variant_create_from_float(float value, ma_variant_t **variant);

/** @brief Fetches float value from given variant by checking
 *         type
 *  @param  [in]  variant      variant
 *  @param  [out] value     The address to which value will be stored
 *  @return                 MA_OK on success
 *                          MA_ERROR_INVALIDARG on invalid argument passed
 *                          MA_ERROR_PRECONDITION on variant type mismatch
 */
MA_VARIANT_API ma_error_t ma_variant_get_float(const ma_variant_t *variant, float *value);

/** @brief Creates unsigned 16-bit integer variant
 *  @param  [in]  value     16-bit unsigned integer value
 *  @param  [out] variant   Pointer to memory location that receives the
 *                          newly constructed variant
 *  @return                 MA_OK on success
 *                          MA_ERROR_INVALIDARG on invalid argument passed
 *                          MA_ERROR_OUTOFMEMORY on memory allocation failure
 */
MA_VARIANT_API ma_error_t ma_variant_create_from_uint16(ma_uint16_t value, ma_variant_t **variant);

/** @brief Fetches 16-bit unsigned integer value from given variant by checking
 *         type
 *  @param  [in]  variant      variant
 *  @param  [out] value     The address to which value will be stored
 *  @return                 MA_OK on success
 *                          MA_ERROR_INVALIDARG on invalid argument passed
 *                          MA_ERROR_PRECONDITION on variant type mismatch
 */
MA_VARIANT_API ma_error_t ma_variant_get_uint16(const ma_variant_t *variant, ma_uint16_t *value);

/** @brief Creates signed 16-bit integer variant
 *  @param  [in]  value     16-bit signed integer value
 *  @param  [out] variant   Pointer to memory location that receives the
 *                          newly constructed variant
 *  @return                 MA_OK on success
 *                          MA_ERROR_INVALIDARG on invalid argument passed
 *                          MA_ERROR_OUTOFMEMORY on memory allocation failure
 */
MA_VARIANT_API ma_error_t ma_variant_create_from_int16(ma_int16_t value, ma_variant_t **variant);

/** @brief Fetches 16-bit signed integer value from given variant by checking
 *         type
 *  @param  [in]  variant      variant
 *  @param  [out] value     The address to which value will be stored
 *  @return                 MA_OK on success
 *                          MA_ERROR_INVALIDARG on invalid argument passed
 *                          MA_ERROR_PRECONDITION on variant type mismatch
 */
MA_VARIANT_API ma_error_t ma_variant_get_int16(const ma_variant_t *variant, ma_int16_t *value);

/** @brief Creates unsigned 8-bit integer variant
 *  @param  [in]  value     8-bit signed integer value
 *  @param  [out] variant   Pointer to memory location that receives the
 *                          newly constructed variant
 *  @return                 MA_OK on success
 *                          MA_ERROR_INVALIDARG on invalid argument passed
 *                          MA_ERROR_OUTOFMEMORY on memory allocation failure
 */
MA_VARIANT_API ma_error_t ma_variant_create_from_uint8(ma_uint8_t value, ma_variant_t **variant);

/** @brief Fetches 8-bit unsigned integer value from given variant by checking
 *         type
 *  @param  [in]  variant      variant
 *  @param  [out] value     The address to which value will be stored
 *  @return                 MA_OK on success
 *                          MA_ERROR_INVALIDARG on invalid argument passed
 *                          MA_ERROR_PRECONDITION on variant type mismatch
 */
MA_VARIANT_API ma_error_t ma_variant_get_uint8(const ma_variant_t *variant, ma_uint8_t *value);


/** @brief Creates signed 8-bit integer variant
 *  @param  [in]  value     8-bit signed integer value
 *  @param  [out] variant   Pointer to memory location that receives the
 *                          newly constructed variant
 *  @return                 MA_OK on success
 *                          MA_ERROR_INVALIDARG on invalid argument passed
 *                          MA_ERROR_OUTOFMEMORY on memory allocation failure
 */
MA_VARIANT_API ma_error_t ma_variant_create_from_int8(ma_int8_t value, ma_variant_t **variant);

/** @brief Fetches 8-bit signed integer value from given variant by checking
 *         type
 *  @param  [in]  variant      variant
 *  @param  [out] value     The address to which value will be stored
 *  @return                 MA_OK on success
 *                          MA_ERROR_INVALIDARG on invalid argument passed
 *                          MA_ERROR_PRECONDITION on variant type mismatch
 */
MA_VARIANT_API ma_error_t ma_variant_get_int8(const ma_variant_t *variant, ma_int8_t *value);

/** @brief Creates variant for type bool
 *  @param  [in]  value     Holds float value
 *  @param  [out] variant   Pointer to memory location that receives the
 *                          newly constructed variant
 *  @return                 MA_OK on success
 *                          MA_ERROR_INVALIDARG on invalid argument passed
 *                          MA_ERROR_OUTOFMEMORY on memory allocation failure
 */
MA_VARIANT_API ma_error_t ma_variant_create_from_bool(ma_bool_t value, ma_variant_t **variant);

/** @brief Fetches bool value from given variant by checking
 *         type
 *  @param  [in]  variant      variant
 *  @param  [out] value     The address to which value will be stored
 *  @return                 MA_OK on success
 *                          MA_ERROR_INVALIDARG on invalid argument passed
 *                          MA_ERROR_PRECONDITION on variant type mismatch
 */
MA_VARIANT_API ma_error_t ma_variant_get_bool(const ma_variant_t *variant, ma_bool_t *value);

/** @brief Creates new variant from string
 *  @param  [in]  value     input string
 *  @param  [in]  len       If len > i/p string length then it
 *                          will compute i/p string length or
 *                          else if len < i/p string length then it
 *                          consider actual i/p len
 *  @param  [out] variant   Pointer to memory location that receives the
 *                          newly constructed variant
 *  @return                 MA_OK on success otherwise an error code
 *                          MA_ERROR_INVALIDARG on invalid argument passed
 *                          MA_ERROR_OUTOFMEMORY on variant creation failure
 *                          MA_ERROR_PRECONDITION on input len = 0
 */
MA_VARIANT_API ma_error_t ma_variant_create_from_string(const char *value, size_t size, ma_variant_t **variant);

/** @brief Fetches string buffer from the variant
 *  @param  [in]  variant  variant
 *  @param  [out] buffer Pointer to memory location to the newly
 *                       constructed buffer that holds string
 *  @return              MA_OK on success otherwise an error code
 *                       MA_ERROR_INVALIDARG on invalid argument passed
 *                       MA_ERROR_PRECONDITION when the variant handle id not of string type
 *  @remarks     Caller must release the returned buffer object when done
 */
MA_VARIANT_API ma_error_t ma_variant_get_string_buffer(ma_variant_t *variant, ma_buffer_t **buffer);

#ifdef MA_HAS_WCHAR_T

/** @brief Creates a new variant from a wide character string
 *  @param  [in]  value   The string to be copied
 *  @param  [in]  len     If len > i/p string length then it
 *                        will compute i/p string length or
 *                        else if len < i/p string length then it
 *                        consider actual i/p len
 *  @param  [out] variant Pointer to memory location that
 *                        receives the newly constructed variant
 *  @return               MA_OK on success otherwise an error code
 *                        MA_ERROR_INVALIDARG on invalid argument passed
 *                        MA_ERROR_OUTOFMEMORY on variant creation failure
 *                        MA_ERROR_PRECONDITION on input len = 0
*/
MA_VARIANT_API ma_error_t ma_variant_create_from_wstring(const wchar_t *value, size_t size, ma_variant_t **variant);

/** @brief Fetches buffer from the given variant
 *  @param  [in]  variant    variant
 *  @param  [out] wbuffer Pointer to memory location that receive the newly
 *                        constructed buffer
 *  @return               MA_OK on success otherwise an error code
 *                        MA_ERROR_INVALIDARG on invalid argument passed
 *                        MA_ERROR_PRECONDITION when the variant handle id not of string type
 *  @remarks     Caller must release the returned buffer object when done
 */
MA_VARIANT_API ma_error_t ma_variant_get_wstring_buffer(ma_variant_t *variant, ma_wbuffer_t **wbuffer);

#endif /* MA_HAS_WCHAR_T */

/** @brief Creates a new variant for table
 *  @param  [in]  value    table handle
 *  @param  [out] variant  Pointer to memory location that receives
 *                         the newly constructed variant
 *  @return                MA_OK on success,reference count on the 
 *                         returned table will be incremented,
 *                         so the caller should release it when done
 *                         MA_ERROR_INVALIDARG on invalid argument passed
 *                         MA_ERROR_OUTOFMEMORY on memory allocation failure
 */
MA_VARIANT_API ma_error_t ma_variant_create_from_table(ma_table_t *value, ma_variant_t **variant);

/** @brief Fetches table entry based on given key
 *  @param  [in]  variant  variant
 *  @param  [out] value Pointer to memory location that receives
 *                      the table handle
 *  @return             MA_OK on success,reference count on the 
 *                      returned table will be incremented,
 *                      so the caller should release it when done
 *                      MA_ERROR_INVALIDARG on invalid argument passed
 *                      MA_ERROR_PRECONDITION on variant type mismatch
 *  @remarks     This operation will bump the reference count on the returned value, 
 *               caller must release the returned object when done
 */
MA_VARIANT_API ma_error_t ma_variant_get_table(ma_variant_t *variant, ma_table_t **value);

/** @brief Creates a new variant for array
 *  @param  [in]  value   array element
 *  @param  [out] variant Pointer to memory location that receives
 *                        the variant
 *  @return               MA_OK on success
 *                        MA_ERROR_INVALIDARG on invalid argument passed
 *                        MA_ERROR_OUTOFMEMORY on memory allocation failurea
 *  @remarks      This operation takes a reference to the specified array, the caller is still responsible
 *                for releasing her own reference count!
 */
MA_VARIANT_API ma_error_t ma_variant_create_from_array(ma_array_t *value, ma_variant_t **variant);

/** @brief Gets array handle
 *  @param  [in]  variant   variant
 *  @param  [out] value  Pointer to memory location that receives
 *                       then newly constructed array type variant
 *  @return              MA_OK on success
 *                       MA_ERROR_INVALIDARG on invalid argument passed
 *                       MA_ERROR_PRECONDITION on variant type mismatch
 */
MA_VARIANT_API ma_error_t ma_variant_get_array(ma_variant_t *variant, ma_array_t **value);

/** @brief Creates a new variant from raw bytes
 *  @param  [in]  value   raw data
 *  @param  [in]  size    raw buffer size in bytes
 *  @param  [out] variant Pointer to memory location that
 *                        receives the newly constructed variant
 *  @return               MA_OK on success otherwise an error code
 *                        MA_ERROR_INVALIDARG on (len == size_t(-1)) value
 *                        and invalid argument passed
 *                        MA_ERROR_OUTOFMEMORY on variant creation failure
 *                        MA_ERROR_PRECONDITION on input len = 0
 *  @remarks      The content of the supplied buffer is copied into a separate buffer
 */
MA_VARIANT_API ma_error_t ma_variant_create_from_raw(const void *value, size_t size, ma_variant_t **variant);

/** @brief Fetches raw data from the given variant
 *  @param  [in]  variant  variant
 *  @param  [out] buffer Pointer to memory location that receives
 *                      the ma_buffer_t object.
 *  @return             MA_OK on success otherwise an error code
 *                      MA_ERROR_INVALIDARG on invalid argument passed
 *                      MA_ERROR_PRECONDITION on variant type is not raw type
 *                      MA_ERROR_UNEXPECTED on empty variant value
 */
MA_VARIANT_API ma_error_t ma_variant_get_raw_buffer(ma_variant_t *variant, ma_buffer_t **buffer);

/** @brief Fetches raw data from the given variant
 *  @param  [in]  variant   variant
 *  @param  [out] raw		Pointer to memory location that receives
 *  @param  [out] size		of the buffer.
 *  @return					MA_OK on success otherwise an error code
 *							MA_ERROR_INVALIDARG on invalid argument passed
 *							MA_ERROR_PRECONDITION on variant type is not raw/string type *							
 */
MA_VARIANT_API ma_error_t ma_variant_get_raw(ma_variant_t *variant, const unsigned char **raw, size_t *size);

/** @brief Fetches null terminated string of given variant.
 *  @param  [in]  variant   variant
 *  @param  [out] str		Pointer to the string, this is null terminated.
 *  @return					MA_OK on success otherwise an error code
 *							MA_ERROR_INVALIDARG on invalid argument passed
 *							MA_ERROR_PRECONDITION on variant type is not string type *							
 */
MA_VARIANT_API ma_error_t ma_variant_get_string(ma_variant_t *variant, const char **str);

/** @brief Creates an empty table
 *  @param  [out]  table table handle to the newly constructed table
 *  @return        MA_OK on success
 *                 MA_ERROR_INVALIDARG on invalid argument passed
 *                 MA_ERROR_OUTOFMEMORY on memory allocation failure
 */
MA_VARIANT_API ma_error_t ma_table_create(ma_table_t **table);

/** @brief Increments the table reference count
 *  @param  [in]  table table handle
 *  @return            MA_OK on success
 *                     MA_ERROR_INVALIDARG on invalid argument passed
 */
MA_VARIANT_API ma_error_t ma_table_add_ref(ma_table_t *table);

/** @brief decrement the table reference count and calls
 *         destroy method, if count reaches to 0 to release table
 *         allocated memory
 *  @param  [in]  table table handle
 *  @return            MA_OK on success
 *                     MA_ERROR_INVALIDARG on invalid argument passed
 */
MA_VARIANT_API ma_error_t ma_table_release(ma_table_t *table);

/** @brief Returns the number of elements stored in the table
 *  @param  [in]  table  table handle
 *  @param  [out] size  Pointer to memory locations that holds
 *                      the number of key-value pairs in the table
 *  @return             MA_OK on success
 *                      MA_ERROR_INVALIDARG on invalid argument passed
 */
MA_VARIANT_API ma_error_t ma_table_size(const ma_table_t *table, size_t *size);

/** @brief checks for equality of 2 tables both in size and value
 *  @param  [in] first       the first table to compare
 *  @param  [in] second       the second table to compare
 *  @param  [out] result memory address that receives the result of the comparison.
 *                       This will be set to 1 if the tables are equal, otherwise 0
 *                       MA_ERROR_INVALIDARG on invalid argument passed
 */
MA_VARIANT_API ma_error_t ma_table_is_equal(const ma_table_t *first, const ma_table_t *second, ma_bool_t *result);

/** @brief Add node to the table with char key and variant value
 *  @param  [in]  variant      table handle
 *  @param  [in]  key		   key 
 *  @param  [in]  value		   variant handle value
 *  @return                 MA_OK on success
 *                          MA_ERROR_INVALIDARG on invalid argument passed
 *                          MA_ERROR_OUTOFMEMORY on memory allocation failure
 */
MA_VARIANT_API ma_error_t ma_table_add_entry(ma_table_t *variant, const char *key, ma_variant_t *value);

/** @brief removes single char key node from the table
 *  @param  [in]  variant      table handle
 *  @param  [in]  key		   key
 *  @return                 MA_OK on success
 *                          MA_ERROR_INVALIDARG on invalid argument passed
 *                          MA_ERROR_OBJECTNOTFOUND on key not found
 *                          MA_ERROR_OUTOFMEMORY on memory allocation failure
 */
MA_VARIANT_API ma_error_t ma_table_remove_entry(ma_table_t *variant, const char *key);

#ifdef MA_HAS_WCHAR_T
/** @brief Add node to the table with wide char key and variant value
 *  @param  [in]  variant      table handle
 *  @param  [in]  key wide char string key
 *  @param  [in]  value     variant handle value
 *  @result                 MA_OK on success
 *                          MA_ERROR_INVALIDARG on invalid argument passed
 *                          MA_ERROR_OUTOFMEMORY on memory allocation failure
 */
MA_VARIANT_API ma_error_t ma_table_add_wentry(ma_table_t *variant, const wchar_t *key, ma_variant_t *value);

/** @brief removes single wide char key node from the table
 *  @param  [in]  variant      table handle
 *  @param  [in]  key		   wide char string key
 *  @return                 MA_OK on success
 *                          MA_ERROR_INVALIDARG on invalid argument passed
 *                          MA_ERROR_OBJECTNOTFOUND on key not found
 *                          MA_ERROR_OUTOFMEMORY on memory allocation failure
 */
MA_VARIANT_API ma_error_t ma_table_remove_wentry(ma_table_t *variant, const wchar_t *key);
#endif /* MA_HAS_WHCAR_T */

/** @brief Get the list of keys from the table.
 *  @param  [in]  variant  table handle
 *  @param  [out] keys  array handle that holds the key list
 *  @result             MA_OK on success
 *                      MA_ERROR_INVALIDARG on invalid argument passed
 *                      MA_ERROR_OUTOFMEMORY on memory allocation failure
 */
MA_VARIANT_API ma_error_t ma_table_get_keys(const ma_table_t *variant, ma_array_t **keys);

/** @brief Retrieve the table value for a given char key
 *  @param  [in]  variant        table handle
 *  @param  [in]  key   char string key
 *  @param  [out] value variant handle memory location that
 *                            stores variant value of the respective char key
 *  @return                   MA_OK on success,reference count on the 
 *                            returned table will be incremented,
 *                            so the caller should release it when done
 *                            MA_ERROR_INVALIDARG on invalid argument passed
 *                            MA_ERROR_OBJECTNOTFOUND on key not found
 */
MA_VARIANT_API ma_error_t ma_table_get_value(ma_table_t *variant, const char *key, ma_variant_t **value);

#ifdef MA_HAS_WCHAR_T
/** @brief Retrieve the table value for a given wide char key
 *  @param  [in]  variant          table handle
 *  @param  [in]  key key wide char string key
 *  @param  [out] value   variant handle memory location that
 *                              stores variant value of the respective wide char key 
 *  @return                     MA_OK on success
 *                              MA_ERROR_INVALIDARG on invalid argument passed
 *                              MA_ERROR_OBJECTNOTFOUND on key not found
 */
MA_VARIANT_API ma_error_t ma_table_get_wvalue(ma_table_t *variant, const wchar_t *key, ma_variant_t **value);
#endif /* MA_HAS_WHCAR_T */

/** @brief Call back to Enumerates char-key/variant-value pair of table
 *  @param  [out]  key   char key
 *  @param  [out]  value variant value for a key
 *  @param  [out]  cb_data      user passed data
 *  @param  [out]  stop_loop   controls whether the loop should call the callback with next table or key
 *  @result        void
 */
typedef void (*MA_TABLE_FOREACH_CALLBACK)(const char *key, ma_variant_t *value, void *cb_data, ma_bool_t *stop_loop);

/** @brief Iterates for the table char-key/value pairs
 *  @param   [in]  variant                      variant handler
 *  @param   [in]  MA_TABLE_FOREACH_CALLBACK call back pointer for char key
 *  @param   [in]  cb_data                 optional
 *  @result                                  MA_OK on success
 *                                           MA_ERROR_INVALIDARG on invalid argument passed 
 *                                           MA_ERROR_PRECONDITION on passing
 *                                           NULL call back pointer value
 */
MA_VARIANT_API ma_error_t ma_table_foreach(const ma_table_t *variant, MA_TABLE_FOREACH_CALLBACK cb, void *cb_data);


#ifdef MA_HAS_WCHAR_T
/* @brief  Call back to Enumerates wide char-key/variant-value pair of table
 * @param   [out]  key   wide char key
 * @param   [out]  value variant value
 * @param   [out]  cb_data    user passed data
 * @param   [out]  stop_loop   controls whether the loop should call the callback with next table or key
 * @result                     MA_OK on success otherwise an error code
 */
typedef void (*MA_TABLE_WFOREACH_CALLBACK)(const wchar_t *key, ma_variant_t *value, void *cb_data, ma_bool_t *stop_loop);

/** @brief Iterates for the table wide-key/value pairs
 *  @param  [in]  variant                       variant handler
 *  @param  [in]  MA_TABLE_WFOREACH_CALLBACK call back function for wide char key
 *  @param  [in]  cb_data                  user supplied data
 *  @result                                  MA_OK on success
 *                                           MA_ERROR_INVALIDARG on invalid argument passed
 */
MA_VARIANT_API ma_error_t ma_table_wforeach(const ma_table_t *variant, MA_TABLE_WFOREACH_CALLBACK cb, void *cb_data);
#endif /* MA_HAS_WHCAR_T */ 


/** @brief Enumeration call back for array variant
 *  @param  [out]  index   array index
 *  @param  [out]  value    variant
 *  @param  [out]  cb_data user passed data
 */
typedef void (*MA_ARRAY_FOREACH_CALLBACK)(size_t index, ma_variant_t *value, void *cb_data, ma_bool_t *stop_loop);

/** @brief Creates a new array objext
 *  @param  [out]  array address of array handle to receive the
 *                 newly constructed array
 *  @result        MA_OK on success
 *                 MA_ERROR_INVALIDARG on invalid argument passed
 *                 MA_ERROR_OUTOFMEMORY on memory allocation failure
 */
MA_VARIANT_API ma_error_t ma_array_create(ma_array_t **array);

/** @brief Increments array reference count
 *  @param  [in]  array array handler
 *  @result            MA_OK on success
 *                     MA_ERROR_INVALIDARG on passing invalid arguments
 */
MA_VARIANT_API ma_error_t ma_array_add_ref(ma_array_t *array);

/** @brief Decrements array reference count, on reaching reference
 *         count to zero, it will call destroy to release the array object
 *  @param [in] array  array handler
 *  @result           MA_OK on success
 *                    MA_ERROR_INVALIDARG on invalid argument passed
 *                    MA_ERROR_UNEXPECTED on array malfunction
 */
MA_VARIANT_API ma_error_t ma_array_release(ma_array_t *array);

/** @brief Returns the number of elements
 *  @param  [in] array  array handler
 *  @param  [out] size Pointer to memory that holds the size of array
 *  @result            MA_OK on success
 *                     MA_ERROR_INVALIDARG on invalid argument passed
 *                     MA_ERROR_UNEXPECTED on array malfunction
 */
MA_VARIANT_API ma_error_t ma_array_size(const ma_array_t *array, size_t *size);


/** @brief checks for equality of 2 arrays both in size and value
 *  @param  [in] first       the first array to compare
 *  @param  [in] second       the second array to compare
 *  @param  [out] result address that receives the result of the comparison.
 *                       This will be set to 1 if the arrays are equal, otherwise 0
 *  @result              MA_OK on success
 *                       MA_ERROR_INVALIDARG on invalid argument passed
 */
MA_VARIANT_API ma_error_t ma_array_is_equal(const ma_array_t *first, const ma_array_t *second, ma_bool_t *result);

/** @brief Add element to end of the array
 *  @param  [in]  array  array handle
 *  @param  [in]  value value to be added
 *  @result             MA_OK on success
 *                      MA_ERROR_INVALIDARG on invalid argument passed
 *                      MA_ERROR_OUTOFMEMORY on memory allocation failure
 *                      MA_ERROR_UNEXPECTED on array malfunction
 */
MA_VARIANT_API ma_error_t ma_array_push(ma_array_t *array, ma_variant_t *value);

/** @brief Retrieves the element at the specified index.
 *  @param  [in]  variant      array handle
 *  @param  [in]  index     array subscript
 *  @param  [out] value Pointer to memory where the
 *                                     variant element will be stored
 *  @result                            MA_OK on success
 *                                     reference count on the returned variant will be incremented,
 *                                     so the caller should release it when done
 *                                     MA_ERROR_INVALIDARG on invalid argument passed
 *                                     MA_ERROR_UNEXPECTED on array malfunction
 */
MA_VARIANT_API ma_error_t ma_array_get_element_at(ma_array_t *variant, size_t index, ma_variant_t **value);

/** @brief Sets the element at the specified index.
 *  @param  [in] variant      array handle
 *  @param  [in] index     array subscript
 *  @param  [in] value value to be stored
 *  @result                            MA_OK on success                                     
 *                                     MA_ERROR_INVALIDARG on invalid argument passed
 *                                     MA_ERROR_UNEXPECTED on array malfunction
 */
MA_VARIANT_API ma_error_t ma_array_set_element_at(ma_array_t *variant, size_t index, ma_variant_t *value);

/** @brief Iterate through the elements in the array
 *  @param  [in]  variant                      array handle
 *  @param  [in]  MA_ARRAY_FOREACH_CALLBACK call back function
 *  @param  [in]  cb_data						user supplied data
 *  @result                                 MA_OK on success
 *                                          MA_ERROR_INVALIDARG on invalid argument passed
 *                                          MA_ERROR_UNEXPECTED on array malfunction
 *                                          MA_ERROR_PRECONDITION on passing
 *                                          NULL call back pointer value
 */
MA_VARIANT_API ma_error_t ma_array_foreach(const ma_array_t *variant, MA_ARRAY_FOREACH_CALLBACK cb, void *cb_data);

MA_CPP(})

#include "ma/dispatcher/ma_variant_dispatcher.h"

#endif /* MA_VARIANT_H_INCLUDED */
