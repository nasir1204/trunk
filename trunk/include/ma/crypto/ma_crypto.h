/** @file ma_crypto.h
 *  @brief Cryptographic functions
 *
*/
#ifndef MA_CRYPTO_H_INCLUDED
#define MA_CRYPTO_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_client.h"

MA_CPP(extern "C" {)

/* Tripple DES algorithm */
#define MA_CRYPTO_ALGO_TYPE_3DES_STR                                        "3DES"
/* AES 128 bit algorithm  */
#define MA_CRYPTO_ALGO_TYPE_AES128_STR                                      "AES128"
/* POLICY AES 128 bit, only decryption */
#define MA_CRYPTO_ALGO_TYPE_POLICY_AES128_STR                               "POLICY_AES128"

/* SHA-1 hash/digest */
#define MA_CRYPTO_DIGEST_TYPE_SHA1_STR                                       "SHA1"
/* SHA-256 hash/digest */
#define MA_CRYPTO_DIGEST_TYPE_SHA256_STR                                     "SHA256"


/** 
 * @brief encrypt's data by alogrithm ID
 * @param client                    [in]        ma client object
 * @param type                      [in]        alogrithm type
 * @param data                      [in]        pointer to input buffer
 * @param data_len				    [in]        input buffer length
 * @param buffer                    [in/out]    pointer to hold buffer which will contain the result in raw format.
 * @result                                      MA_OK on success.
 *                                              MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *      call ma_buffer_release to free the memory   
 *      "POLICY_AES128" encryption is not supported.
 */
MA_CRYPTO_API ma_error_t ma_crypto_encrypt_data_by_type(ma_client_t *client, const char *algo_type, const unsigned char *data, size_t data_len, ma_buffer_t **buffer);


/** 
 * @brief decrypt's data by alogrithm ID
 * @param client                    [in]        ma client object
 * @param type                      [in]        alogrithm type
 * @param data                      [in]        pointer to input buffer
 * @param data_len				    [in]        input buffer length
 * @param buffer                    [in/out]    pointer to hold buffer which will contain the result in raw format.
 * @result                                      MA_OK on success.
 *                                              MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *      call ma_buffer_release to free the memory   
 */
MA_CRYPTO_API ma_error_t ma_crypto_decrypt_data_by_type(ma_client_t *client, const char *algo_type, const unsigned char *data, size_t data_len, ma_buffer_t **buffer);


/**
 * @brief calculates digest/hash(sha1/sha256) of a data
 * @param client                    [in]        ma client object
 * @param type                      [in]        alogrithm type
 * @param data                      [in]        pointer to input buffer
 * @param data_len				    [in]        input buffer length
 * @param buffer                    [in/out]    pointer to hold buffer which will contain the result in raw format. 
 * @result                                      MA_OK on success.
 *                                              MA_ERROR_INVALIDARG on invalid argument passed.
 * @remark               
 *      call ma_buffer_release to free the memory    
 */  
MA_CRYPTO_API ma_error_t ma_crypto_hash_data_by_type(ma_client_t *client,  const char *digest_type, const unsigned char *data, size_t data_len, ma_buffer_t **buffer);

MA_CPP(})

#include "ma/dispatcher/ma_crypto_dispatcher.h"

#endif /* MA_CRYPTO_H_INCLUDED */

