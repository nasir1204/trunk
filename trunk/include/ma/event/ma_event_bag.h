/** @file ma_event_bag.h
 *  @brief Event Bag Interfaces
 *
*/
#ifndef MA_EVENT_BAG_H_INCLUDED
#define MA_EVENT_BAG_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/event/ma_event.h"
#include "ma/event/ma_event_client_defs.h"

MA_CPP(extern "C" {)

/**
Event bag to hold product info and its events.
 */
typedef struct ma_event_bag_s ma_event_bag_t, *ma_event_bag_h;


/** 
 * Create an event bag to hold the software and events info
 * @param ma_client             [in]    ma client object
 * @param product_name          [in]    product name which creates event
 * @param event_bag             [out]   ma event bag object
 * @return                              MA_OK on success
 *                                      MA_ERROR_OUTOFMEMORY on memory allocation failure
 *                                      MA_ERROR_INVALIDARG on invalid arguments
 */
MA_EVENT_API ma_error_t ma_event_bag_create(ma_client_t *ma_client, const char *product_name, ma_event_bag_t **event_bag);

/** 
 * Set the product version (Optional)
 * @param self                  [in]    ma event bag object
 * @param product_version       [in]    product version
 * @return                              MA_OK on successful
 *                                      MA_ERROR_INVALIDARG on invalid arguments
 */
MA_EVENT_API ma_error_t ma_event_bag_set_product_version(ma_event_bag_t *self, const char *product_version);

/** 
 * Set the product family (Optional)
 * @param self                  [in]    ma event bag object
 * @param product_family        [in]    product family (possible values are "TVD" & "Secure")
 * @return                              MA_OK on successful
 *                                      MA_ERROR_INVALIDARG on invalid arguments
 */
MA_EVENT_API ma_error_t ma_event_bag_set_product_family(ma_event_bag_t *self, const char *product_family);

/** 
 * Add common field (Optional), common fields added to event bag are inherited by all the events in the event bag
 * @param self                  [in]    ma event bag object
 * @param key                   [in]    common field key (Permitted keys are in ma_event_defs.h, starts with MA_EVENT_COMMON_FILED_KEY_*)
 * @param value                 [in]    common field value (variant MA_VARTYPE_STRING Type only.)
 * @return                              MA_OK on successful
 *                                      MA_ERROR_EVENT_INVALID_VALUE_TYPE, if variant is not MA_VARTYPE_STRING type
 *                                      MA_ERROR_INVALIDARG on invalid arguments
 */
MA_EVENT_API ma_error_t ma_event_bag_add_common_field(ma_event_bag_t *self, const char *key, ma_variant_t *value);

/** 
 * Set custom field table name, (Must if user wants to add custom field)
 * @param self                  [in]    ma event bag object
 * @param target_table          [in]    custom field table name
 * @return                              MA_OK on successful
 *                                      MA_ERROR_INVALIDARG on invalid arguments
 */
MA_EVENT_API ma_error_t ma_event_bag_set_custom_fields_table(ma_event_bag_t *self, const char *target_table);

/** 
 * Add custom field (optional)
 * @param self                  [in]    MA event bag object
 * @param key                   [in]    custom field key
 * @param value                 [in]    custom field value (variant MA_VARTYPE_STRING Type only)
 * @return                              MA_OK on successful
 *                                      MA_ERROR_EVENT_INVALID_VALUE_TYPE if variant is not MA_VARTYPE_STRING type
 *                                      MA_ERROR_INVALIDARG on invalid arguments
 */
MA_EVENT_API ma_error_t ma_event_bag_add_custom_field(ma_event_bag_t *self, const char *key, ma_variant_t *value);

/** 
 * Add product event to event bag
 * @param self                  [in]    ma event bag object
 * @param ma_event              [in]    event info object
 * @return                              MA_OK on success
 *                                      MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_EVENT_API ma_error_t ma_event_bag_add_event(ma_event_bag_t *self, ma_event_t *ma_event);

/** 
 * Send event bag to event service
 * @param self                  [in]    ma event bag object
 * @return                              MA_OK on successful
 *                                      MA_ERROR_INVALIDARG on invalid arguments
 *                                      Others from event service, in case of failure
 */
MA_EVENT_API ma_error_t ma_event_bag_send(ma_event_bag_t *self);

/** 
 * Add event bag reference.
 * @param self                  [in]    ma event bag object
 * @return                              MA_OK on Success
 *                                      MA_ERROR_INVALIDARG on invalid arguments
 */
MA_EVENT_API ma_error_t ma_event_bag_add_ref(ma_event_bag_t *self);

/** 
 * Release event bag.
 * @param self                  [in]    ma event bag object
 * @return                              MA_OK on Success
 *                                      MA_ERROR_INVALIDARG on invalid arguments
 */
MA_EVENT_API ma_error_t ma_event_bag_release(ma_event_bag_t *self);


/** 
 * Get the product name
 * @param self                  [in]    ma event bag object
 * @param product_name	        [out]   product name
 * @return                              MA_OK on successful
 *                                      MA_ERROR_INVALIDARG on invalid arguments
 */
MA_EVENT_API ma_error_t ma_event_bag_get_product_name(ma_event_bag_t *self, const char **product_name);

/** 
 * Get the product version
 * @param self                  [in]    ma event bag object
 * @param product_version       [out]   product version
 * @return                              MA_OK on successful
 *                                      MA_ERROR_EVENT_SETTING_NOT_FOUND, if version not set
 *                                      MA_ERROR_INVALIDARG on invalid arguments
 */
MA_EVENT_API ma_error_t ma_event_bag_get_product_version(ma_event_bag_t *self, const char **product_version);

/** 
 * Get the product family
 * @param self                  [in]    ma event bag object
 * @param product_family        [out]   product family (possible values are "TVD" & "Secure")
 * @return                              MA_OK on successful
 *                                      MA_ERROR_EVENT_SETTING_NOT_FOUND, if product_family not set
 *                                      MA_ERROR_INVALIDARG on invalid arguments
 */
MA_EVENT_API ma_error_t ma_event_bag_get_product_family(ma_event_bag_t *self, const char **product_family);

/** 
 * Get common field
 * @param self                  [in]    ma event bag object
 * @param key                   [in]    common field key (Permitted keys are in ma_event_defs.h, starts with MA_EVENT_COMMON_FILED_KEY_*)
 * @param value                 [out]   common field value.
 * @return                              MA_OK on successful
 *                                      MA_ERROR_EVENT_SETTING_NOT_FOUND, if common fields are not set
 *                                      MA_ERROR_INVALIDARG on invalid arguments
 */
MA_EVENT_API ma_error_t ma_event_bag_get_common_field(ma_event_bag_t *self, const char *key, ma_variant_t **value);

/** 
 * Get custom field table name
 * @param self                  [in]    ma event bag object
 * @param target_table          [out]   custom field table name
 * @return                              MA_OK on successful
 *                                      MA_ERROR_EVENT_SETTING_NOT_FOUND, if custom_fields_table not set
 *                                      MA_ERROR_INVALIDARG on invalid arguments
 */
MA_EVENT_API ma_error_t ma_event_bag_get_custom_fields_table(ma_event_bag_t *self, const char **target_table);

/** 
 * Get custom field
 * @param self                  [in]    MA event bag object
 * @param key                   [in]    custom field key
 * @param value                 [out]   custom field value 
 * @return                              MA_OK on successful
 *                                      MA_ERROR_EVENT_SETTING_NOT_FOUND, if custom fields are not set
 *                                      MA_ERROR_INVALIDARG on invalid arguments
 */
MA_EVENT_API ma_error_t ma_event_bag_get_custom_field(ma_event_bag_t *self, const char *key, ma_variant_t **value);

/** 
 * Get product event count in event bag
 * @param self                  [in]    ma event bag object
 * @param count	                [out]   event count
 * @return                              MA_OK on success
 *                                      MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_EVENT_API ma_error_t ma_event_bag_get_event_count(ma_event_bag_t *self, size_t *count);

/** 
 * Get product event from event bag at index.
 * @param self                  [in]    ma event bag object
 * @param index	                [in]	event index (0 to count-1)
 * @param ma_event              [out]   event info object
 * @return                              MA_OK on success
 *                                      MA_ERROR_OUTOFBOUNDS if out of bounds.
 *                                      MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_EVENT_API ma_error_t ma_event_bag_get_event(ma_event_bag_t *self, size_t index, ma_event_t **ma_event);

MA_CPP(})

#endif /* MA_EVENT_BAG_H_INCLUDED */
