/** @file ma_event.h
 *  @brief Event Interfaces
 *
*/
#ifndef MA_EVENT_H_INCLUDED
#define MA_EVENT_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_variant.h"
#include "ma/ma_client.h"

MA_CPP(extern "C" {)

/** 
Event object
 */
typedef struct ma_event_s ma_event_t, *ma_event_h;

/** 
Event Severity
 */
typedef enum ma_event_severity_e{
    MA_EVENT_SEVERITY_INFORMATIONAL = 0,
    MA_EVENT_SEVERITY_WARNING,
    MA_EVENT_SEVERITY_MINOR,
    MA_EVENT_SEVERITY_MAJOR,
    MA_EVENT_SEVERITY_CRITICAL
}ma_event_severity_t;

/** 
 * Create an event object
 * @param ma_client         [in]    ma client object
 * @param eventid           [in]    event id
 * @param severity          [in]    event severity
 * @param ma_event          [out]   ma event object
 * @return                          MA_OK on success
 *                                  MA_ERROR_EVENT_DISABLED, If event id is disabled by policy
 *                                  MA_ERROR_OUTOFMEMORY on memory allocation failure
 *                                  MA_ERROR_INVALIDARG on invalid arguments
 */
MA_EVENT_API ma_error_t ma_event_create(ma_client_t *ma_client, ma_uint32_t eventid, ma_event_severity_t severity, ma_event_t **ma_event);

/** 
 * Set common field
 * @param self              [in]    ma event object
 * @param key               [in]    common field key (Permitted Keys are in ma_event_defs.h, starts with MA_EVENT_COMMON_FILED_KEY_*)
 * @param value             [in]    common field value (Variant MA_VARTYPE_STRING Type only)
 * @return                          MA_OK on success
 *                                  MA_ERROR_EVENT_INVALID_VALUE_TYPE, If value variant is not MA_VARTYPE_STRING type
 *                                  MA_ERROR_INVALIDARG on invalid arguments
 */
MA_EVENT_API ma_error_t ma_event_add_common_field(ma_event_t *self, const char *key, ma_variant_t *value);

/** 
 * Set custom field table name (Must if user wants to add custom fields)
 * @param self              [in]    ma event object
 * @param target_table      [in]    custom field table name
 * @return                          MA_OK on successful
 *                                  MA_ERROR_INVALIDARG on invalid arguments
 */
MA_EVENT_API ma_error_t ma_event_set_custom_fields_table(ma_event_t *self, const char *target_table);


/** 
 * Add custom field (Optional)
 * @param self              [in]    ma event object
 * @param key               [in]    custom field key
 * @return                          MA_OK on success
 *                                  MA_ERROR_EVENT_INVALID_VALUE_TYPE, If variant is not MA_VARTYPE_STRING type
 *                                  MA_ERROR_INVALIDARG on invalid arguments
 */
MA_EVENT_API ma_error_t ma_event_add_custom_field(ma_event_t *self, const char *key, ma_variant_t *value);


/** 
 * Add event object reference.
 * @param self              [in]    ma event object
 * @return                          MA_OK on successful
 *                                  MA_ERROR_INVALIDARG on invalid arguments
 */
MA_EVENT_API ma_error_t ma_event_add_ref(ma_event_t *self);

/** 
 * Release event object.
 * @param self              [in]    ma event object
 * @return                          MA_OK on successful
 *                                  MA_ERROR_INVALIDARG on invalid arguments
 * @remark User can release the event object after adding it to event bag
 */
MA_EVENT_API ma_error_t ma_event_release(ma_event_t *self);


/** 
 * Get event id
 * @param self              [in]    ma event object
 * @param key               [out]   eventid
 * @return                          MA_OK on success
 *                                  MA_ERROR_INVALIDARG on invalid arguments
 */
MA_EVENT_API ma_error_t ma_event_get_id(ma_event_t *self, ma_uint32_t *eventid);

/** 
 * Get event id
 * @param self              [in]    ma event object
 * @param severity          [out]   event severity
 * @return                          MA_OK on success
 *                                  MA_ERROR_INVALIDARG on invalid arguments
 */
MA_EVENT_API ma_error_t ma_event_get_severity(ma_event_t *self, ma_event_severity_t *severity);

/** 
 * Get common field
 * @param self              [in]    ma event object
 * @param key               [in]    common field key (Permitted Keys are in ma_event_defs.h, starts with MA_EVENT_COMMON_FILED_KEY_*)
 * @param value             [out]   common field value 
 * @return                          MA_OK on success
 *                                  MA_ERROR_EVENT_SETTING_NOT_FOUND, if key not found.
 *                                  MA_ERROR_INVALIDARG on invalid arguments
 */
MA_EVENT_API ma_error_t ma_event_get_common_field(ma_event_t *self, const char *key, ma_variant_t **value);

/** 
 * Get custom field table name 
 * @param self              [in]    ma event object
 * @param target_table      [out]   custom field table name
 * @return                          MA_OK on successful
 *                                  MA_ERROR_EVENT_SETTING_NOT_FOUND, if key not found.
 *                                  MA_ERROR_INVALIDARG on invalid arguments
 */
MA_EVENT_API ma_error_t ma_event_get_custom_fields_table(ma_event_t *self, const char **target_table);


/** 
 * Get custom field
 * @param self              [in]    ma event object
 * @param key               [in]    custom field key
 * @param value             [out]   custom field value 
 * @return                          MA_OK on success
 *                                  MA_ERROR_EVENT_SETTING_NOT_FOUND, if key not found.
 *                                  MA_ERROR_INVALIDARG on invalid arguments
 */
MA_EVENT_API ma_error_t ma_event_get_custom_field(ma_event_t *self, const char *key, ma_variant_t **value);

MA_CPP(})


#endif /*MA_EVENT_H_INCLUDED*/
