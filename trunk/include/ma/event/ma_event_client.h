/** @file ma_event_client.h
 *  @brief Event Client Interfaces
 *
*/
#ifndef MA_EVENT_CLIENT_H_INCLUDED
#define MA_EVENT_CLIENT_H_INCLUDED

#include "ma/event/ma_event.h"
#include "ma/event/ma_event_bag.h"
#include "ma/event/ma_custom_event.h"

MA_CPP(extern "C" {)

typedef struct ma_event_callbacks_s ma_event_callbacks_t, *ma_event_callbacks_h ;


struct ma_event_callbacks_s {
	/**
	* Callback for getting the CEF published events.
	* @param ma_client              [in]            ma client object
	* @param producer_id		    [in]            event producer id 
	* @param event_bag	            [in]            cef event bag.
	* @param cb_data                [in,optional]   callback data as it was specified in 'ma_event_client_register_callbacks'
	*
	*/
    ma_error_t (*cef_event_bag_cb)(ma_client_t *ma_client, const char *event_product_id, ma_event_bag_t *event_bag, void *cb_data);

	/**
	* Callback for getting the CUSTOM published events.
	* @param ma_client              [in]            ma client object
	* @param producer_id	        [in]            event producer id  
	* @param event_bag	            [in]            custom event bag.
	* @param cb_data                [in,optional]   callback data as it was specified in 'ma_event_client_register_callbacks'
	*
	*/
    ma_error_t (*custom_event_bag_cb)(ma_client_t *ma_client, const char *event_product_id, ma_custom_event_t *event_bag, void *cb_data);    
};

/**
* Registers for event.
* @param ma_client              [in]   ma client object
* @param cb					    [in]   event callbacks, User can register for CEF/CUSTOM/both, Set NULL if not interested.
* @param cb_data                [in]   user's data, this will be provided in the callback. This is opaque to MA
* @result                              MA_OK on success
*                                      MA_ERROR_INVALIDARG on invalid argument passed
*                                      MA_ERROR_CLIENT_ALREADY_REGISTERED if the provider already registered with the product id
*/

MA_EVENT_API ma_error_t ma_event_register_callbacks(ma_client_t *ma_client, ma_event_callbacks_t *cb, void *cb_data);

/**
* Un-registers for event
* @param ma_client              [in]   ma client object
* @param product_id             [in]   event consumer product id
* @result                              MA_OK on success
*                                      MA_ERROR_INVALIDARG on invalid argument passed
*/

MA_EVENT_API ma_error_t ma_event_unregister_callbacks(ma_client_t *ma_client);

/**
* Forwards event to server
* @param ma_client              [in]   ma client object
* @param product_id             [in]   event consumer product id
* @result                              MA_OK on success
*                                      MA_ERROR_INVALIDARG on invalid argument passed
* @remark Call ONLY on need basis as it effects the network traffic, as of now product id is ignored.
*/
MA_EVENT_API ma_error_t ma_event_upload(ma_client_t *ma_client, const char *product_id);

MA_CPP(})

#include "ma/dispatcher/ma_event_dispatcher.h"

#endif
