/** @file ma_event_payload_defs.h
 *  @brief Event Payload Definitions
 *
*/
#ifndef MA_EVENT_PAYLOAD_DEFS_H_INCLUDED
#define MA_EVENT_PAYLOAD_DEFS_H_INCLUDED

/**
ma common event format event keys (inside event message payload)
*/
#define MA_EVENT_EVENT_NAME_KEY										"EventName"
#define MA_EVENT_EVENT_ID_KEY										"EventID"
#define MA_EVENT_EVENT_SEVERITY_KEY									"Severity"
#define MA_EVENT_EVENT_GMTTIME_KEY									"GMTTime"
#define MA_EVENT_EVENT_LOCALTIME_KEY								"LocalTime"
#define MA_EVENT_COMMON_FIELDS_KEY									"CommonFields"
#define MA_EVENT_CUSTOM_TARGET_KEY									"target"
#define MA_EVENT_CUSTOM_FIELDS_KEY									"CustomFields"

/**
ma common event format message payload keys.
*/
#define MA_EVENT_BAG_PRODUCT_NAME_KEY								 "ProductName"
#define MA_EVENT_BAG_PRODUCT_VERSION_KEY							 "ProductVersion"
#define MA_EVENT_BAG_PRODUCT_FAMILY_KEY							     "ProductFamily"
#define MA_EVENT_BAG_EVENTS_KEY										 "Events"
#define MA_EVENT_BAG_COMMON_FIELDS_KEY								  MA_EVENT_COMMON_FIELDS_KEY
#define MA_EVENT_BAG_CUSTOM_TARGET_KEY							      MA_EVENT_CUSTOM_TARGET_KEY
#define MA_EVENT_BAG_CUSTOM_FIELDS_KEY							      MA_EVENT_CUSTOM_FIELDS_KEY


#endif /*MA_EVENT_PAYLOAD_DEFS_H_INCLUDED*/
