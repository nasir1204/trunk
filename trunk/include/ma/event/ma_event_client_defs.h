/** @file ma_event_client_defs.h
 *  @brief Event Client Definition to extract information out of the XML
 *
*/
#ifndef MA_EVENT_CLIENT_DEFS_H_INCLUDED
#define MA_EVENT_CLIENT_DEFS_H_INCLUDED


/*  Event service constants */
/* Event bag/event common filed keys.*/
#define MA_EVENT_COMMON_FILED_KEY_DetectedUTC								    "DetectedUTC"
#define MA_EVENT_COMMON_FILED_KEY_AgentGUID                                      "AgentGUID"
#define MA_EVENT_COMMON_FILED_KEY_Analyzer                                       "Analyzer"
#define MA_EVENT_COMMON_FILED_KEY_AnalyzerName                                   "AnalyzerName"
#define MA_EVENT_COMMON_FILED_KEY_AnalyzerVersion                                "AnalyzerVersion"
#define MA_EVENT_COMMON_FILED_KEY_AnalyzerHostName                               "AnalyzerHostName"
#define MA_EVENT_COMMON_FILED_KEY_AnalyzerIPV4                                   "AnalyzerIPV4"
#define MA_EVENT_COMMON_FILED_KEY_AnalyzerIPV6                                   "AnalyzerIPV6"
#define MA_EVENT_COMMON_FILED_KEY_AnalyzerMAC                                    "AnalyzerMAC"
#define MA_EVENT_COMMON_FILED_KEY_AnalyzerDATVersion                             "AnalyzerDATVersion"
#define MA_EVENT_COMMON_FILED_KEY_AnalyzerEngineVersion                          "AnalyzerEngineVersion"
#define MA_EVENT_COMMON_FILED_KEY_AnalyzerDetectionMethod                        "AnalyzerDetectionMethod"
#define MA_EVENT_COMMON_FILED_KEY_SourceHostName                                 "SourceHostName"
#define MA_EVENT_COMMON_FILED_KEY_SourceIPV4                                     "SourceIPV4"
#define MA_EVENT_COMMON_FILED_KEY_SourceIPV6                                     "SourceIPV6"
#define MA_EVENT_COMMON_FILED_KEY_SourceMAC                                      "SourceMAC"
#define MA_EVENT_COMMON_FILED_KEY_SourceUserName                                 "SourceUserName"
#define MA_EVENT_COMMON_FILED_KEY_SourceProcessName                              "SourceProcessName"
#define MA_EVENT_COMMON_FILED_KEY_SourceURL                                      "SourceURL"
#define MA_EVENT_COMMON_FILED_KEY_TargetHostName                                 "TargetHostName"
#define MA_EVENT_COMMON_FILED_KEY_TargetIPV4                                     "TargetIPV4"
#define MA_EVENT_COMMON_FILED_KEY_TargetIPV6                                     "TargetIPV6"
#define MA_EVENT_COMMON_FILED_KEY_TargetMAC                                      "TargetMAC"
#define MA_EVENT_COMMON_FILED_KEY_TargetUserName                                 "TargetUserName"
#define MA_EVENT_COMMON_FILED_KEY_TargetPort                                     "TargetPort"
#define MA_EVENT_COMMON_FILED_KEY_TargetProtocol                                 "TargetProtocol"
#define MA_EVENT_COMMON_FILED_KEY_TargetProcessName                              "TargetProcessName"
#define MA_EVENT_COMMON_FILED_KEY_TargetFileName                                 "TargetFileName"
#define MA_EVENT_COMMON_FILED_KEY_ThreatCategory                                 "ThreatCategory"
#define MA_EVENT_COMMON_FILED_KEY_ThreatEventID                                  "ThreatEventID"
#define MA_EVENT_COMMON_FILED_KEY_ThreatSeverity                                 "ThreatSeverity"
#define MA_EVENT_COMMON_FILED_KEY_ThreatName                                     "ThreatName"
#define MA_EVENT_COMMON_FILED_KEY_ThreatType                                     "ThreatType"
#define MA_EVENT_COMMON_FILED_KEY_ThreatActionTaken                              "ThreatActionTaken"
#define MA_EVENT_COMMON_FILED_KEY_ThreatHandled                                  "ThreatHandled"

#endif /* MA_EVENT_CLIENT_DEFS_H_INCLUDED */

