/** @file ma_custom_event.h
 *  @brief Custom Event Interfaces
 *
*/
#ifndef MA_CUSTOM_EVENT_H_INCLUDED
#define MA_CUSTOM_EVENT_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_variant.h"
#include "ma/ma_client.h"
#include "ma/xml/ma_xml_node.h"
#include "ma/event/ma_event.h"
MA_CPP(extern "C" {)

/*
custom event object
*/
typedef struct ma_custom_event_s ma_custom_event_t, *ma_custom_event_h;

 /* Create an custom event object
 * @param ma_client			      [in]		client object
 * @param event_root_name         [in]		root name of event
 * @param event			          [out]		custom event object
 * @param root_node			      [out]		custom event object **********[IMP NOTE]:node return by api is own by event, so can not be deleted by client. It will get release in event release api.
 * @return									MA_OK on success
 *											MA_ERROR_APIFAILED, If api failed
 *											MA_ERROR_OUTOFMEMORY on memory allocation failure
 *											MA_ERROR_INVALIDARG on invalid arguments
 */

/*
	This API implicitly adds MachineInfo tag with all required elements as example:
	<MachineInfo>
		<MachineName>SHANU-PC</MachineName>
		<AgentGUID>{e61796e4-96cb-4961-86e5-f5e1775b7dec}</AgentGUID>
		<IPAddress>10.213.254.15</IPAddress>
		<OSName>Windows 7</OSName>
		<UserName>EPO2003\admin</UserName>
		<TimeZoneBias>-330</TimeZoneBias>
		<RawMACAddress>d4bed9457b8d</RawMACAddress>
	</MachineInfo>
*/

MA_EVENT_API ma_error_t ma_custom_event_create(ma_client_t *ma_client, const char *event_root_name, ma_custom_event_t **event, ma_xml_node_t **root_node);


/** 
 * create custom event from buffer
 * @param ma_client			    [in]	client object
 * @param buffer				[in]    raw buffer which contain custom event xml buffer
 * @param event					[out]   even object
 * @return								MA_OK on success
 *										MA_ERROR_APIFAILED, If api failed
 *										MA_ERROR_OUTOFMEMORY on memory allocation failure
 *										MA_ERROR_INVALIDARG on invalid arguments
 */
			
MA_EVENT_API ma_error_t ma_custom_event_create_from_buffer(ma_client_t *ma_client, const char *buffer, ma_custom_event_t **event);

/** 
 * Create node
 * @param event_node_name		[in]    node name
 * @param event_id				[in]    event node Id
 * @param severity				[in]    event node severity 
 * @param node					[out]   custom event node **********[IMP NOTE]:node return by api is own by event, so can not be deleted by client. It will get release in event release api.
 * @return								MA_OK on success
 *										MA_ERROR_APIFAILED, If api failed
 *										MA_ERROR_OUTOFMEMORY, if memory allocation failure
 *										MA_ERROR_INVALIDARG, if invalid arguments
 *										MA_ERROR_EVENT_DISABLED, if event is filtered.
 */

/*This api internally adds following four elements under newly created node <event_node_name>  :
<event_node_name>
	<EventID>1027</EventID>
	<Severity>3</Severity>
	<GMTTime>2014-01-27T15:20:56</GMTTime>
	<UTCTime>2014-01-27T09:50:56</UTCTime>
</event_node_name>
*/

MA_EVENT_API ma_error_t ma_custom_event_create_event_node(ma_custom_event_t *event, ma_xml_node_t *parent, const char *event_node_name, ma_uint32_t event_id, ma_event_severity_t severity, ma_xml_node_t **node);

/** 
 * create custom event message and send
 * @param event					[in]    event object
 * @return								MA_OK on success
 *		                                MA_ERROR_APIFAILED, If api failed
 *				                        MA_ERROR_OUTOFMEMORY on memory allocation failure
 *						                MA_ERROR_INVALIDARG on invalid arguments
 */
MA_EVENT_API ma_error_t ma_custom_event_send(ma_custom_event_t *event);


/** 
 * release custom event and all associated nodes
 * @param event					[in]    event object
 * @return							    MA_OK on success
 *										MA_ERROR_APIFAILED, If api failed
 *										MA_ERROR_OUTOFMEMORY on memory allocation failure
 *										MA_ERROR_INVALIDARG on invalid arguments
 */
MA_EVENT_API ma_error_t ma_custom_event_release(ma_custom_event_t *event);

/**
 * create a node with the mentioned name and become child of given parent
 * @param parent				[in]    parent node to which new node be associated
 * @param name					[in]	name of the node
 * @param node					[out]   child node **********[IMP NOTE]:node return by api is own by parent, so can not be deleted by client. It will get release in event release api.
 * @return							    MA_OK on success
 *										MA_ERROR_APIFAILED, If api failed
 *										MA_ERROR_OUTOFMEMORY on memory allocation failure
 *										MA_ERROR_INVALIDARG on invalid arguments
 * 
 */
MA_EVENT_API ma_error_t ma_custom_event_node_create(ma_xml_node_t *parent, const char *name, ma_xml_node_t **node);


/**
 * add attribute with name and value to given node
 * @param node					[in]    node to which attribute to be set
 * @param name					[in]	name of attribute
 * @param value					[in]	value of attribute
  * @return							    MA_OK on success
 *										MA_ERROR_APIFAILED, If api failed
 *										MA_ERROR_OUTOFMEMORY on memory allocation failure
 *										MA_ERROR_INVALIDARG on invalid arguments
 * 
 */

MA_EVENT_API ma_error_t ma_custom_event_node_attribute_set(ma_xml_node_t *node, const char *name, const char *value);

/**
 * set data value to given node
 * @param node					[in]    node to which data to be set
 * @param data					[in]	value
  * @return							    MA_OK on success
 *										MA_ERROR_APIFAILED, If api failed
 *										MA_ERROR_OUTOFMEMORY on memory allocation failure
 *										MA_ERROR_INVALIDARG on invalid arguments
 * 
 */
MA_EVENT_API ma_error_t ma_custom_event_node_set_data(ma_xml_node_t *node, const char *data);

/**
 * returns root node of event, this root node can be used for further processing
 * @param event					[in]    custom event
 * @param root_node				[out]	root node of custom event...[IMP NOTE]:node return by api is own by event, so can not be deleted by client. It will get release in event release api.
  * @return							    MA_OK on success
 *										MA_ERROR_APIFAILED, If api failed
 *										MA_ERROR_OBJECTNOTFOUND on memory allocation failure
 *										MA_ERROR_INVALIDARG on invalid arguments
 * 
 */
MA_EVENT_API ma_error_t ma_custom_event_get_root_node(ma_custom_event_t *event, ma_xml_node_t **root_node);

/**
 * returns field name string associated with given node
 * @param node					[in]    node
 * @param name					[out]	field name associated with node
  * @return							    MA_OK on success
 *										MA_ERROR_APIFAILED, If api failed
 *										MA_ERROR_OBJECTNOTFOUND on memory allocation failure
 *										MA_ERROR_INVALIDARG on invalid arguments
 * 
 */
MA_EVENT_API ma_error_t ma_custom_event_node_name_get(ma_xml_node_t *node, const char **name);

/**
 * returns data string associated with given node
 * @param node					[in]    node
 * @param data					[out]	data associated with node ......[IMP NOTE]:data return by api is own by node, so can not be deleted by client. It will get release in event release api.
  * @return							    MA_OK on success
 *										MA_ERROR_APIFAILED, If api failed
 *										MA_ERROR_OBJECTNOTFOUND on memory allocation failure
 *										MA_ERROR_INVALIDARG on invalid arguments
 * 
 */
MA_EVENT_API ma_error_t ma_custom_event_node_data_get(ma_xml_node_t *node, const char **data);

/**
 * returns data string associated with attribute of given node
 * @param node					[in]    node
 * @param attribute				[in]    attribute
 * @param data					[out]	data associated with attribute......[IMP NOTE]:data return by api is own by node, so can not be deleted by client. It will get release in event release api.
  * @return							    MA_OK on success
 *										MA_ERROR_APIFAILED, If api failed
 *										MA_ERROR_OBJECTNOTFOUND on memory allocation failure
 *										MA_ERROR_INVALIDARG on invalid arguments
 * 
 */
MA_EVENT_API ma_error_t ma_custom_event_node_attribute_get(ma_xml_node_t *node, const char *attribute, const char **data);

/**
 * returns node matching to given name
 * @param node					[in]    root node
 * @param name					[in]    name of node to be search
 * @param data					[out]	node associated with name......[IMP NOTE]:data return by api is own by node, so can not be deleted by client. It will get release in event release api.
 * @return							    MA_OK on success
 *										MA_ERROR_APIFAILED, If api failed
 *										MA_ERROR_OBJECTNOTFOUND on memory allocation failure
 *										MA_ERROR_INVALIDARG on invalid arguments
 * 
 */
MA_EVENT_API ma_error_t ma_custom_event_node_search(ma_xml_node_t *node, const char *name, ma_xml_node_t **data);

/**
 * returns child node given input parent
 * @param node					[in]    parent node
 * @param child_node			[out]	child node......[IMP NOTE]:data return by api is own by event, so can not be deleted by client. It will get release in event release api.
  * @return							    MA_OK on success
 *										MA_ERROR_APIFAILED, If api failed
 *										MA_ERROR_OBJECTNOTFOUND on memory allocation failure
 *										MA_ERROR_INVALIDARG on invalid arguments
 * 
 */
MA_EVENT_API ma_error_t ma_custom_event_get_child_node(ma_xml_node_t *node, ma_xml_node_t **child_node);

/**
 * returns next node given input parent
 * @param node					[in]    parent node
 * @param next_node				[out]	next node......[IMP NOTE]:data return by api is own by event, so can not be deleted by client. It will get release in event release api.
 * @return							    MA_OK on success
 *										MA_ERROR_APIFAILED, If api failed
 *										MA_ERROR_OBJECTNOTFOUND on memory allocation failure
 *										MA_ERROR_INVALIDARG on invalid arguments
 * 
 */
MA_EVENT_API ma_error_t ma_custom_event_get_next_node(ma_xml_node_t *node, ma_xml_node_t **next_node);

/**
 * returns next sibling node of given input node
 * @param node					[in]    node
 * @param next_node				[out]	sibling node......[IMP NOTE]:data return by api is own by event, so can not be deleted by client. It will get release in event release api.
 * @return							    MA_OK on success
 *										MA_ERROR_APIFAILED, If api failed
 *										MA_ERROR_OBJECTNOTFOUND on memory allocation failure
 *										MA_ERROR_INVALIDARG on invalid arguments
 * 
 */
MA_EVENT_API ma_error_t ma_custom_event_get_next_sibling(ma_xml_node_t *node, ma_xml_node_t **sibling_node);

/** 
 * Add custom event object reference.
 * @param self              [in]    ma custom event object
 * @return                          MA_OK on successful
 *                                  MA_ERROR_INVALIDARG on invalid arguments
 */
MA_EVENT_API ma_error_t ma_custom_event_add_ref(ma_custom_event_t *self);

MA_CPP(})

#include "ma/dispatcher/ma_custom_event_dispatcher.h"

#endif /*MA_CUSTOM_EVENT_H_INCLUDED*/

