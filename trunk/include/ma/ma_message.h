/** @file ma_message.h
 *  @brief Declarations Message for Message Bus
 *
*/
#ifndef MA_MESSAGE_H_INCLUDED
#define MA_MESSAGE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_variant.h"

/* message type value should be within the range 0 - 15 */
typedef enum ma_message_type_e {

    MA_MESSAGE_TYPE_UNSPECIFIED = 0,

    MA_MESSAGE_TYPE_REQUEST,

    MA_MESSAGE_TYPE_REPLY,

    MA_MESSAGE_TYPE_PUBLISH,

    MA_MESSAGE_TYPE_SIGNAL,
} ma_message_type_t;

/* message sub type value should be within the range 0 - 15 */
typedef enum ma_message_sub_type_e {
    MA_MESSAGE_SUB_TYPE_ONEWAY = 1,
} ma_message_sub_type_t;

#include "ma/ma_message_auth_info.h"
#include "ma/ma_msgbus.h"

#include <time.h>

MA_CPP(extern "C" {)

/*!
 * Creates a fresh message
 */
MA_MSGBUS_API ma_error_t ma_message_create(ma_message_t **message);

/*!
 * Creates a messages from an appropriately structured variant (e.g. variant that is an array with 2 or 3 elements, where the first and second elements are of type table)
 */
MA_MSGBUS_API ma_error_t ma_message_from_variant(ma_variant_t *variant, ma_message_t **message);

/*!
 * Increments the reference count
 */
MA_MSGBUS_API ma_error_t ma_message_add_ref(ma_message_t *message);

/*!
 * Decrements the ref count, possibly destroying the message
 */
MA_MSGBUS_API ma_error_t ma_message_release(ma_message_t *message);


/*!
 * Retrieves a timestamp 
 *
 * @param message Message for which timestamp to be retrieved
 * @param field time_t pointer
 *
 * @return MA_OK on success, in which case the time_t pointer will be holding the value
 * @note It's not supported as of now
 */
MA_MSGBUS_API ma_error_t ma_message_get_timestamp(const ma_message_t *message, time_t *timestamp);

/*!
 * Retrieves a type of the message
 *
 * @param message Message for which type needs to be known
 * @param field ma_message_type_t pointer 
 *
 * @return MA_OK on success, in which case the msg_type will be holding the type of the message  
 */
MA_MSGBUS_API ma_error_t ma_message_get_message_type(ma_message_t *message, ma_message_type_t *msg_type);

/*!
 * Sets a property value
 * 
 * @param message The message for which to set the property
 * @param key Name of the property. This should be an ascii or utf-8 encoded string
 * @param value Property value to set
 * 
 * @note Any existing property value will be overridden
 */
MA_MSGBUS_API ma_error_t ma_message_set_property(ma_message_t *message, const char *key, const char *value);


/*!
 * Retrieves a property value
 * 
 * @param message The message from where the property value should be retrieved
 * @param key The name of the desired property
 * @param value A pointer to memory that receives the string pointer. Do not modify this
 * 
 * @note The returned pointer is valid until the message is released or that property value is modified. The caller should not free the memory
 *
 */
MA_MSGBUS_API ma_error_t ma_message_get_property(ma_message_t *message, const char *key, const char **value);


/*!
 * Sets the payload of the message. Any variant structure can be specified
 *
 * @param message The message for which to set the payload
 * @param payload The payload to set
 * @Note that this increments the reference count on the specified payload, don't forget to perform your own memory management on the variant
 * Any existing payload will be overridden (but will of course have its reference properly decremented)
 * 
 */
MA_MSGBUS_API ma_error_t ma_message_set_payload(ma_message_t *message, ma_variant_t *payload);

/*!
 * Retrieves the payload of the specified message
 *
 * @param message
 * @param payload A pointer to memory that receives the address of the specified payload. The reference count of the returned payload is incremented, it is your repsponsibility to call ma_variant_release() on it when no longer needed
 */
MA_MSGBUS_API ma_error_t ma_message_get_payload(ma_message_t *message, ma_variant_t **payload);

/*!
 * Retrieves the message authentication info object for the specified message
 * @param [in] message The message for which to get the auth information
 * @param [out] auth_info Address of pointer to an ma_message_auth_info_s that will receive the authtentication information
 *   Note, the caller must release the returned object by calling ma_message_auth_info_release() when done
 * 
 */
MA_MSGBUS_API ma_error_t ma_message_get_auth_info(ma_message_t *message, ma_message_auth_info_t **auth_info);

/*!
 * Returns a ma_variant representation of the entire message
 * @note The variant will be an array with 3 elements, first element is the header section , next element is the properties , and the third element is the payload, which is a variant of whatever type
 */
MA_MSGBUS_API ma_error_t ma_message_as_variant(ma_message_t *message, ma_variant_t **variant);

/*!
 * Gets a clone of message.
 * @note This will copy only properties and payload. Caller gets the ownership of new message.
 */
MA_MSGBUS_API ma_error_t ma_message_clone(ma_message_t *source, ma_message_t **dest);


MA_CPP(})


#endif /* MA_MESSAGE_H_INCLUDED */
