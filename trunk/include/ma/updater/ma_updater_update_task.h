/** @file ma_updater_update_task.h
 *  @brief Update Task API's
 *
*/
#ifndef MA_UPDATER_UPDATE_TASK_H_INCLUDED
#define MA_UPDATER_UPDATE_TASK_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_client.h"
#include "ma/scheduler/ma_task.h"

MA_CPP(extern "C" {)
/**
    * Utility for creating task type update
    * @param ma_client         [in] ma client object
    * @param task_id           [in,optional] id of the task associated with task
    * @param task              [in] creates a task object of type update which is ready for scheduling.
    * @remark  This just creates scheduler task of type update, which will be handled by MA updater service.
               Scheduling and adding to scheduler and all user have to do.
*/
MA_UPDATER_API ma_error_t ma_updater_update_task_create(ma_client_t *ma_client, const char *task_id, ma_task_t **task);

/**
    * sets the product list to be updated, used by updater
    * @param task              [in] task object of update task type as update
    * @param product_list      [in]  array of product list to be updated.
*/

MA_UPDATER_API ma_error_t ma_updater_update_task_set_product_list(ma_task_t *task, ma_array_t *product_list);

/**
    * sets the locale to to be used, used by updater
    * @param task              [in] task object of update task type as update
    * @param locale            [in]  locale to be used
*/

MA_UPDATER_API ma_error_t ma_updater_update_task_set_locale(ma_task_t *task, ma_uint32_t locale);

MA_CPP(})

#endif  /* MA_UPDATER_UPDATE_TASK_H_INCLUDED */

