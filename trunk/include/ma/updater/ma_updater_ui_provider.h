/** @file ma_updater_ui_provider.h
 *  @brief Updater UI provider interfaces
 *
*/
#ifndef MA_UPDATER_UI_PROVIDER_INCLUDED
#define MA_UPDATER_UI_PROVIDER_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_client.h"

#include "ma/updater/ma_updater_event.h"

MA_CPP(extern "C" {)

/* Updater UI type */
typedef enum ma_updater_ui_type_e {
    MA_UPDATER_UI_TYPE_UNKNOWN                = -1,
    MA_UPDATER_UI_TYPE_REBOOT                 = 0,
    MA_UPDATER_UI_TYPE_PROGRESS               = 1,
    MA_UPDATER_UI_TYPE_POSTPONE               = 2,
}ma_updater_ui_type_t;

/* Return codes from the updater UI */
typedef enum ma_updater_ui_return_codes_e {
    MA_UPDATER_UI_RC_REBOOT_UNKNOWN           = -1,
    MA_UPDATER_UI_RC_REBOOT_POSTPONED         = 0,
    MA_UPDATER_UI_RC_REBOOT_NOW               = 1,
    MA_UPDATER_UI_RC_REBOOT_TIMEOUT           = 2,
    MA_UPDATER_UI_RC_NO_UI                    = 3,
    MA_UPDATER_UI_RC_POSTPONE_DECLINED        = 4,
    MA_UPDATER_UI_RC_POSTPONE_NOW             = 5,
    MA_UPDATER_UI_RC_POSTPONE_TIMEOUT         = 6
}ma_updater_ui_return_code_t;

typedef struct ma_updater_show_ui_request_s ma_updater_show_ui_request_t, *ma_updater_show_ui_request_h ;
typedef struct ma_updater_show_ui_response_s ma_updater_show_ui_response_t, *ma_updater_show_ui_response_h ;


/* updater UI call backs */

typedef struct ma_updater_ui_provider_callbacks_s {
    /**
    * @brief Callback to show the user's ui
    * @param ma_client              [in]            ma client object
    * @param cb_data                [in,optional]   callback data as it was specified in 'ma_updater_register_ui_provider_callbacks', use NULL if do not want any data to be passed
    * @param session_id             [in]            updater session identifier 
    * @param type   				[in]            updater ui type 'ma_updater_ui_type_e'
    * @param request				[in]			show ui request, has info like initiator type, title, message...
    * @param response				[in]			show ui response, user will use to post the repose of the call back with return code
    *
    * @remark   UTF8 encoded strings will be passed
    */
    ma_error_t (*on_show_ui_cb)(ma_client_t *ma_client, void *cb_data,  const char *session_id, ma_updater_ui_type_t type, const ma_updater_show_ui_request_t *request, ma_updater_show_ui_response_t *response) ; 

    /**
    * @brief Callback to show progress info
    * @param ma_client              [in]            ma client object
    * @param cb_data                [in,optional]   callback data as it was specified in 'ma_updater_register_ui_provider_callbacks', use NULL if do not want any data to be passed
    * @param session_id             [in]            updater session identifier 
    * @param event_type				[in]            updater event type 
    * @param progress_step			[in]			progress_step
    * @param max_progress_step		[in]			max_progress_step
    * @param progress_message		[in]			progress message to be displayed
    *
    * @remark   UTF8 encoded strings will be passed
    */
    ma_error_t (*on_progress_cb)(ma_client_t *ma_client, void *cb_data, const char *session_id, ma_int32_t event_type, ma_int32_t progress_step , ma_int32_t max_progress_step, const char *progress_message);
	    
    /**
    * @brief Callback to end/close user's ui
    * @param ma_client              [in]            ma client object
    * @param cb_data                [in,optional]   callback data as it was specified in 'ma_updater_register_ui_provider_callbacks', use NULL if do not want any data to be passed
    * @param session_id             [in]            updater session identifier 
    * @param title   				[in]            title for end dialog
    * @param end_message			[in]			end dialog message
    * @param count_down_message		[in]			auto close/end dialog message
    * @param count_down_value		[in]			auto close count down value
    * @remark   UTF8 encoded strings will be passed
    */
    ma_error_t (*on_end_ui_cb)(ma_client_t *ma_client, void *cb_data, const char *session_id, const char *title, const char *end_message, const char *count_down_message, ma_int64_t count_down_value);

	/**
    * Callback to get the updater event information
    * @param ma_client              [in]            ma client object
    * @param cb_data                [in,optional]   callback data as it was specified in 'ma_updater_register_ui_provider_callbacks', use NULL if do not want any data to be passed
    * @param session_id             [in]            updater session identifier 
    * @param event_info   			[in]            updater event info, holds eventid, type, update state, update error ...
    *
    * @remark   UTF8 encoded strings will be passed
    */
	ma_error_t (*on_update_event_cb)(ma_client_t *ma_client, void *cb_data, const char *session_id, const ma_updater_event_t *event_info) ;
}ma_updater_ui_provider_callbacks_t;


/**
* @brief Registers updater ui provider
* @param ma_client              [in]   ma client object
* @param software_id            [in]   product id of the user for which tasks are received
* @param cb_thread_option		[in]   msgbus callback thread option
* @param callbacks			    [in]   set of user's callbacks to be invoked
* @param cb_data                [in]   user's data, this will be provided in the callback.This is opaque to MA
* @result                              MA_OK on success
*                                      MA_ERROR_INVALIDARG on invalid argument passed
*                                      MA_ERROR_UPDATER_UI_PROVIDER_ALREADY_REGISTERED if the user is already registered with the product id
* @remark product_id can be NULL, if we want to provide the product id same as client
* @remark can be called multiple times if user wants to register for different product id
* @remark This is not mandatory to have callbacks registered if product doesn't have their own updater ui
*/
MA_UPDATER_API ma_error_t ma_updater_register_ui_provider_callbacks(ma_client_t *ma_client, const char *software_id, ma_msgbus_callback_thread_options_t cb_thread_option, ma_updater_ui_provider_callbacks_t const *callbacks, void *cb_data) ;

/**
* @brief Get the provider name registered for the software/product id
* @param ma_client              [in]   ma client object
* @param software_id            [in]   product id of the user for which provider is registered
* @param name   				[out]  provider name
* @result                              MA_OK on success.
*                                      MA_ERROR_INVALIDARG on invalid argument passed.
*                                      MA_ERROR_UPDATER_UI_PROVIDER_ALREADY_REGISTERED if the user is not registered with the product id
* @remark product_id can be NULL, if product id is same as client.
*/
MA_UPDATER_API ma_error_t ma_updater_get_ui_provider_name(ma_client_t *ma_client, const char *software_id, const char **name);

/**
* @brief Un-registers updater ui provider
* @param ma_client              [in]   ma client object
* @param software_id            [in]   product id of the user
* @result                              MA_OK on success.
*                                      MA_ERROR_INVALIDARG on invalid argument passed.
*                                      MA_ERROR_UPDATER_UI_PROVIDER_ALREADY_REGISTERED if the user has not registered with the product id
* @remark product_id can be NULL, if product id is same as client.
*/
MA_UPDATER_API ma_error_t ma_updater_unregister_ui_provider_callbacks(ma_client_t *ma_client, const char *software_id) ;

/** 
 * @brief Retrieves initiator type from show ui request
 * @param request             [in]  show ui request object
 * @param initiated_type      [out]  initiator type
 * @result                          MA_OK on successful
 *                                  MA_ERROR_INVALIDARG on invalid arguments passed
*/
MA_UPDATER_API ma_error_t ma_updater_show_ui_request_get_initiator_type(const ma_updater_show_ui_request_t *request, ma_uint32_t *initiated_type);

/** 
 * @brief Retrieves title from show ui request
 * @param request             [in]  show ui request object
 * @param initiated_type      [out]  title
 * @result                          MA_OK on successful
 *                                  MA_ERROR_INVALIDARG on invalid arguments passed
*/
MA_UPDATER_API ma_error_t ma_updater_show_ui_request_get_title(const ma_updater_show_ui_request_t *request, const char **title);

/** 
 * @brief Retrieves message from show ui request
 * @param request             [in]  show ui request object
 * @param message             [out]  message
 * @result                          MA_OK on successful
 *                                  MA_ERROR_INVALIDARG on invalid arguments passed
*/
MA_UPDATER_API ma_error_t ma_updater_show_ui_request_get_message(const ma_updater_show_ui_request_t *request, const char **message);

/** 
 * @brief Retrieves count down message from show ui request
 * @param request             [in]  show ui request object
 * @param count_down_message  [out]  count down message
 * @result                          MA_OK on successful
 *                                  MA_ERROR_INVALIDARG on invalid arguments passed
*/
MA_UPDATER_API ma_error_t ma_updater_show_ui_request_get_count_down_message(const ma_updater_show_ui_request_t *request, const char **count_down_message);

/** 
 * @brief Retrieves count down value from show ui request
 * @param request             [in]  show ui request object
 * @param count_down_value    [out]  count down value
 * @result                          MA_OK on successful
 *                                  MA_ERROR_INVALIDARG on invalid arguments passed
*/
MA_UPDATER_API ma_error_t ma_updater_show_ui_request_get_count_down_value(const ma_updater_show_ui_request_t *request, ma_uint32_t *count_down_value);

/** 
 * @brief post a response for the show ui request
 * @param ma_client             [in]   ma client object
 * @param show_ui_response      [in]   show ui reponse object
 * @result                          MA_OK on successful
 *                                  MA_ERROR_INVALIDARG on invalid arguments passed
*/
MA_UPDATER_API ma_error_t ma_updater_show_ui_response_post(ma_client_t *ma_client, ma_updater_show_ui_response_t *show_ui_response ) ;

/** 
 * @brief sets return code to show ui response
 * @param response            [in]  show ui reponse object
 * @param return_code         [in]  return code of type ma_updater_ui_return_code_t
 * @result                          MA_OK on successful
 *                                  MA_ERROR_INVALIDARG on invalid arguments passed
*/
MA_UPDATER_API ma_error_t ma_updater_show_ui_response_set_return_code(ma_updater_show_ui_response_t *response, ma_updater_ui_return_code_t return_code) ; 

/** 
 * @brief sets postpone time to show ui response
 * @param response            [in]  show ui reponse object
 * @param postpone_timeout    [in]  postpone time in minutes
 * @result                          MA_OK on successful
 *                                  MA_ERROR_INVALIDARG on invalid arguments passed
*/
MA_UPDATER_API ma_error_t ma_updater_show_ui_response_set_postpone_timeout(ma_updater_show_ui_response_t *response, ma_uint32_t postpone_timeout) ;

/** 
 * @brief sets reboot time to show ui response
 * @param response            [in]  show ui reponse object
 * @param reboot_timeout      [in]  reboot time in minutes
 * @result                          MA_OK on successful
 *                                  MA_ERROR_INVALIDARG on invalid arguments passed
*/
MA_UPDATER_API ma_error_t ma_updater_show_ui_response_set_reboot_timeout(ma_updater_show_ui_response_t *response, ma_uint32_t reboot_timeout) ;



MA_CPP(})

#include "ma/dispatcher/ma_updater_ui_provider_dispatcher.h"
#endif  /* MA_UPDATER_UI_PROVIDER_INCLUDED */




