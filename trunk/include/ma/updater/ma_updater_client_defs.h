/** @file ma_updater_client_defs.h
 *  @brief Updater client definitions
 *
*/
#ifndef MA_UPDATER_CLIENT_DEFS_H_INCLUDED
#define MA_UPDATER_CLIENT_DEFS_H_INCLUDED

#include "ma/ma_common.h"

MA_CPP(extern "C" {)

/* Task types owned by MA updater */
#define MA_UPDATER_TASK_TYPE_UPDATE_STR                                 "ma.updater.task.type.update"

#define MA_UPDATER_TASK_TYPE_DEPLOYMENT_STR                             "ma.updater.task.type.deployment"


/*! \brief Current updater initiator type identification values
*/
typedef enum ma_updater_initiator_type_e {
    MA_UPDATER_INITIATOR_TYPE_UNKNOWN                                   = -1,
    MA_UPDATER_INITIATOR_TYPE_BASE                                      = 0,
    
    MA_UPDATER_INITIATOR_TYPE_ONDEMAND_UPDATE                           = MA_UPDATER_INITIATOR_TYPE_BASE,
    MA_UPDATER_INITIATOR_TYPE_TASK_UPDATE                               = MA_UPDATER_INITIATOR_TYPE_BASE + 1,
    MA_UPDATER_INITIATOR_TYPE_GLOBAL_UPDATE                             = MA_UPDATER_INITIATOR_TYPE_BASE + 2,
    /*this update type is no longer supported from MA 4.0 onwards */
    MA_UPDATER_INITIATOR_TYPE_REMEDIATION_UPDATE                        = MA_UPDATER_INITIATOR_TYPE_BASE + 3, 
    MA_UPDATER_INITIATOR_TYPE_TASK_DEPLOYMENT                           = MA_UPDATER_INITIATOR_TYPE_BASE + 4,
    MA_UPDATER_INITIATOR_TYPE_TASK_MIRROR                               = MA_UPDATER_INITIATOR_TYPE_BASE + 5,
    MA_UPDATER_INITIATOR_TYPE_ONDEMAND_ROLLBACK                         = MA_UPDATER_INITIATOR_TYPE_BASE + 6
}ma_updater_initiator_type_t;

/*! \brief Current updater session state identification values
*/

typedef enum ma_update_state_e {
	MA_UPDATE_STATE_UNKNOWN                                    = -1,
    MA_UPDATE_STATE_BASE                                       = 0, 
	MA_UPDATE_STATE_UPDATE_READY                               = MA_UPDATE_STATE_BASE, 
	//!< finished updating all products
    MA_UPDATE_STATE_FINISHED_OK                                = MA_UPDATE_STATE_BASE + 1,
    //!< failed to update any product
	MA_UPDATE_STATE_FINISHED_FAIL                              = MA_UPDATE_STATE_BASE + 2,
    //!< no qualifying product found
	MA_UPDATE_STATE_FINISHED_NOTFOUND                          = MA_UPDATE_STATE_BASE + 3,
    //!< failed to update any product
	MA_UPDATE_STATE_FINISHED_CANCEL                            = MA_UPDATE_STATE_BASE + 4,
    //!< finished updating all products but requires reboot
	MA_UPDATE_STATE_FINISHED_WAITING_REBOOT                    = MA_UPDATE_STATE_BASE + 5,
    //!< finished updating some products but requires    reboot and restart update
	MA_UPDATE_STATE_FINISHED_WAITING_REBOOT_RESTART            = MA_UPDATE_STATE_BASE + 6,  
    //!< some product updated successfully some are not
	MA_UPDATE_STATE_FINISHED_PARTIAL                           = MA_UPDATE_STATE_BASE + 7,      
    //!< component update succeeded
	MA_UPDATE_STATE_SUCCEEDED                                  = MA_UPDATE_STATE_BASE + 8,
    //!< component update failed
	MA_UPDATE_STATE_FAILED                                     = MA_UPDATE_STATE_BASE + 9,			        
	MA_UPDATE_STATE_INIT                                       = MA_UPDATE_STATE_BASE + 10,
	MA_UPDATE_STATE_LOADCONFIG                                 = MA_UPDATE_STATE_BASE + 11,
    //!< prenotify, if update required
	MA_UPDATE_STATE_PRENOTIFY                                  = MA_UPDATE_STATE_BASE + 12,
	MA_UPDATE_STATE_EOL_CHECK                                  = MA_UPDATE_STATE_BASE + 13,
	MA_UPDATE_STATE_ROLLBACK_CHECK                             = MA_UPDATE_STATE_BASE + 14,
	MA_UPDATE_STATE_RUNNING                                    = MA_UPDATE_STATE_BASE + 15,
	MA_UPDATE_STATE_CRITICAL                                   = MA_UPDATE_STATE_BASE + 16,
	MA_UPDATE_STATE_NONCRITICAL                                = MA_UPDATE_STATE_BASE + 17,
	MA_UPDATE_STATE_SUSPEND                                    = MA_UPDATE_STATE_BASE + 18,
	MA_UPDATE_STATE_RESUME                                     = MA_UPDATE_STATE_BASE + 19,
	MA_UPDATE_STATE_DOWNLOAD                                   = MA_UPDATE_STATE_BASE + 20,
	MA_UPDATE_STATE_STOP_SERVICE                               = MA_UPDATE_STATE_BASE + 21,
	MA_UPDATE_STATE_START_SERVICE                              = MA_UPDATE_STATE_BASE + 22,
	MA_UPDATE_STATE_BACKUP                                     = MA_UPDATE_STATE_BASE + 23,
	MA_UPDATE_STATE_COPY                                       = MA_UPDATE_STATE_BASE + 24,
	MA_UPDATE_STATE_SET_CONFIG                                 = MA_UPDATE_STATE_BASE + 25,
    //!< postnotify, if updated
	MA_UPDATE_STATE_POST_NOTIFY                                = MA_UPDATE_STATE_BASE + 26,
	MA_UPDATE_STATE_CHECK_SITE_STATUS                          = MA_UPDATE_STATE_BASE + 27,
	MA_UPDATE_STATE_COMPARE_SITES                              = MA_UPDATE_STATE_BASE + 28,
	MA_UPDATE_STATE_PRENOTIFY_FORCE                            = MA_UPDATE_STATE_BASE + 29,
	MA_UPDATE_STATE_STOP_INITIATED                             = MA_UPDATE_STATE_BASE + 30,
    MA_UPDATE_STATE_PRE_UPDATE_CHECKS                          = MA_UPDATE_STATE_BASE + 31
}ma_update_state_t;


/*! \brief Current update product return value types
*/
typedef enum ma_updater_product_return_code_e {
    MA_UPDATER_PRODUCT_RETURN_CODE_UNKNOWN = -1, //!< point product returns this for unknown errors
	MA_UPDATER_PRODUCT_RETURN_CODE_OK = 0, //!< if point product returns this then update proceeds
	MA_UPDATER_PRODUCT_RETURN_CODE_ACCEPT = 0, //!< if point product returns this then update is applied
	MA_UPDATER_PRODUCT_RETURN_CODE_FAIL,    //!< Generic error. update fails for this product, unless other alternate in script provided
	MA_UPDATER_PRODUCT_RETURN_CODE_REJECT, //!< update is not applied for this product, if rejected
	MA_UPDATER_PRODUCT_RETURN_CODE_ROLLBACK, //!< if Point product returns this then only rollback done
	MA_UPDATER_PRODUCT_RETURN_CODE_LICENSE_EXPIRED //!< if Point product returns this when license gets expired
} ma_updater_product_return_code_t;


/*Supported updater types*/
#define MA_UPDATE_TYPE_RENU_STR                                        "RENU" 
#define MA_UPDATE_TYPE_DAT_STR                                         "DAT"
#define MA_UPDATE_TYPE_ENGINE_STR                                      "Engine"
#define MA_UPDATE_TYPE_EXTRA_STR                                       "ExtraDAT"
#define MA_UPDATE_TYPE_SPAM_RULES_STR                                  "SpamRules"
#define MA_UPDATE_TYPE_SPAM_ENGINE_STR                                 "SpamEngine"
#define MA_UPDATE_TYPE_SERIVCE_PACK_STR                                "ServicePack"
#define MA_UPDATE_TYPE_HOTFIX_STR                                      "HotFix"
#define MA_UPDATE_TYPE_LICENSE_STR                                     "License"
#define MA_UPDATE_TYPE_LEGACY_STR			                           "Legacy"
#define MA_UPDATE_TYPE_PLUGIN_STR                                      "Plugin"
#define MA_UPDATE_TYPE_LANGPACK_STR                                    "LangPack"
#define MA_UPDATE_TYPE_PRODUCT_STR                                     "Product" 
#define MA_UPDATE_TYPE_PRODUCT_INSTALL_STR                             "Install" 
#define MA_UPDATE_TYPE_PRODUCT_UNINSTALL_STR                           "Uninstall" 


/* supported Updater engine  info */
#define MA_UPDATER_INFO_VERSION_STR                                    "Version"		//!< Version number of the component
#define MA_UPDATER_INFO_BUILDNUM_STR			                       "BuildNumber"	//!< Release build number of the component
#define MA_UPDATER_INFO_DATE_TIME_STR			                       "DateTime"	//!< Release date of the component
#define MA_UPDATER_INFO_LOCALE_STR 				                       "Locale"		//!< Locale of the component
#define MA_UPDATER_INFO_ERROR_STR						               "UpdateError"	//!< Used to set update error for the component
#define MA_UPDATER_INFO_INSTALL_DATE_STR				               "InstallDate"	//!< Used to set last update time for the component
#define MA_UPDATER_INFO_LOCATION_STR					               "Location"	//!< Used to get the location for the component


MA_CPP(})

#endif /* MA_UPDATER_CLIENT_DEFS_H_INCLUDED */
