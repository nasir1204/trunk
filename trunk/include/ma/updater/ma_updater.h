/** @file ma_updater.h
 *  @brief Updater interfaces
 *
*/
#ifndef MA_UPDATER_H_INCLUDED
#define MA_UPDATER_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_client.h"
#include "ma/updater/ma_updater_event.h"
#include "ma/updater/ma_updater_update_request.h"

MA_CPP(extern "C" {)

typedef struct ma_updater_callbacks_s ma_updater_callbacks_t, *ma_updater_callbacks_h ;

/* updater call back structure */
struct ma_updater_callbacks_s {

	/**
    * @brief notify callback to perticular product
    * @param ma_client              [in]            ma client object
    * @param product_id             [in]			product id for which notification will be sent
    * @param cb_data	            [in]            callback data
    * @param update_type			[in]            update type
    * @param state					[in]			state of update
    * @param message				[in]			message to be send
	* @param extra_info				[in]			any extra info to send
	* @param return_code			[out]			return code
	* @result										MA_OK on success
	*												MA_ERROR_INVALIDARG on invalid argument passed
    *
    * @remark  
    */
    ma_error_t (*notify_cb)(ma_client_t *ma_client, const char *product_id, void *cb_data, const char *update_type, ma_update_state_t state, const char *message, const char *extra_info, ma_updater_product_return_code_t *return_code);

	/**
    * @brief set info callback to set info for perticular product
    * @param ma_client              [in]            ma client object
    * @param product_id             [in]			product id for which notification will be sent
    * @param cb_data	            [in]            callback data
    * @param update_type			[in]            update type
    * @param key					[in]			key 
    * @param value					[in]			value for given key
	* @param return_code			[out]			return code
	* @result										MA_OK on success
	*												MA_ERROR_INVALIDARG on invalid argument passed
    *
    * @remark  
    */
    ma_error_t (*set_cb)(ma_client_t *ma_client, const char *product_id, void *cb_data, const char *update_type, const char *key, ma_variant_t *value, ma_updater_product_return_code_t *return_code);

	/**
    * @brief get  info callback to get info from perticular product
    * @param ma_client              [in]            ma client object
    * @param product_id             [in]			product id for which notification will be recieved
    * @param cb_data	            [in]            callback data
    * @param update_type			[in]            update type
    * @param key					[in]			key 
    * @param value					[out]			value for given key
	* @param return_code			[out]			return code
	* @result										MA_OK on success
	*												MA_ERROR_INVALIDARG on invalid argument passed
    *
    * @remark  
    */
    ma_error_t (*get_cb)(ma_client_t *ma_client, const char *product_id, void *cb_data, const char *update_type, const char *key, ma_variant_t **value, ma_updater_product_return_code_t *return_code);

	/**
    * @brief update event callback 
    * @param ma_client              [in]            ma client object
    * @param product_id             [in]			product id for which event will be sent
    * @param cb_data	            [in]            callback data
    * @param event_info				[in]            event info
	* @result										MA_OK on success
	*												MA_ERROR_INVALIDARG on invalid argument passed
    *
    * @remark  
    */
    ma_error_t (*update_event_cb)(ma_client_t *ma_client, const char *product_id, void *cb_data, const ma_updater_event_t *event_info) ;
};

/**
    * @brief callback registation of given product id
    * @param ma_client              [in]            ma client object
    * @param product_id             [in]			product id
    * @param callbacks	            [in]            updater call back 
    * @param cb_data   				[in]            callback data
	* @result										MA_OK on success
	*												MA_ERROR_INVALIDARG on invalid argument passed
	*												appropriate MA error code will be return in all other failure cases.
    *
    * @remark   
    */
MA_UPDATER_API ma_error_t ma_updater_register_callbacks(ma_client_t *ma_client, const char *product_id, ma_updater_callbacks_t const *callbacks, void *cb_data);

/**
    * @brief callback un-registartion of rperticular product
    * @param ma_client              [in]            ma client object
    * @param product_id             [in]			product id
	* @result										MA_OK on success
	*												MA_ERROR_INVALIDARG on invalid argument passed
	*												appropriate MA error code will be return in all other failure cases.
    *
    * @remark   
    */
MA_UPDATER_API ma_error_t ma_updater_unregister_callbacks(ma_client_t *ma_client, const char *product_id);

/**
    * @brief start update
    * @param ma_client              [in]            ma client object
    * @param request	            [in]			update request
	* @param session_id	            [out]			session id of update
	* @result										MA_OK on success
	*												MA_ERROR_INVALIDARG on invalid argument passed
	*												MA_ERROR_INVALID_CLIENT_OBJECT on invalid client
	*												appropriate MA error code will be return in all other failure cases.
    *
    * @remark   
    */
MA_UPDATER_API ma_error_t ma_updater_start_update(ma_client_t *ma_client, ma_updater_update_request_t *request, ma_buffer_t **session_id);

/**
    * @brief get update state of given session
    * @param ma_client              [in]            ma client object
    * @param session_id	            [in]			session id of update
	* @param state		            [out]			update state
	* @result										MA_OK on success
	*												MA_ERROR_INVALIDARG on invalid argument passed
	*												MA_ERROR_INVALID_CLIENT_OBJECT on invalid client
	*												appropriate MA error code will be return in all other failure cases.
    *
    * @remark   
    */
MA_UPDATER_API ma_error_t ma_updater_get_update_state(ma_client_t *ma_client, ma_buffer_t *session_id, ma_update_state_t *state);

/**
    * @brief stop update
    * @param ma_client              [in]            ma client object
    * @param session_id	            [in]			session id of update to be stop
	* @result										MA_OK on success
	*												MA_ERROR_INVALIDARG on invalid argument passed
	*												MA_ERROR_INVALID_CLIENT_OBJECT on invalid client
	*												appropriate MA error code will be return in all other failure cases.
    *
    * @remark   
    */
MA_UPDATER_API ma_error_t ma_updater_stop_update(ma_client_t *ma_client, ma_buffer_t *session_id);


MA_CPP(})

#include "ma/dispatcher/ma_updater_dispatcher.h"

#endif  /* MA_UPDATER_H_INCLUDED */

