/** @file ma_updater_event.h
 *  @brief  Updater Event Interfaces
 *
*/
#ifndef MA_UPDATER_EVENT_H_INCLUDED
#define MA_UPDATER_EVENT_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_variant.h"
#include "ma/updater/ma_updater_client_defs.h"

MA_CPP(extern "C" {)

typedef struct ma_updater_event_s ma_updater_event_t, *ma_updater_event_h;

MA_UPDATER_API ma_error_t ma_updater_event_get_event_id(const ma_updater_event_t *updater_event, long *event_id);

MA_UPDATER_API ma_error_t ma_updater_event_get_severity(const ma_updater_event_t *updater_event, ma_int32_t *severity);

MA_UPDATER_API ma_error_t ma_updater_event_get_session_id(const ma_updater_event_t *update_event, const char **session_id) ;

MA_UPDATER_API ma_error_t ma_updater_event_get_product_id(const ma_updater_event_t *update_event, const char **product_id) ;

MA_UPDATER_API ma_error_t ma_updater_event_get_locale(const ma_updater_event_t *update_event, const char **locale) ;

MA_UPDATER_API ma_error_t ma_updater_event_get_update_type(const ma_updater_event_t *update_event, const char **update_type) ;

MA_UPDATER_API ma_error_t ma_updater_event_get_update_state(const ma_updater_event_t *update_event, ma_update_state_t *update_state) ;

MA_UPDATER_API ma_error_t ma_updater_event_get_update_error(const ma_updater_event_t *update_event, ma_int32_t *update_error) ;

MA_UPDATER_API ma_error_t ma_updater_event_get_new_version(const ma_updater_event_t *update_event, const char **new_version) ;

MA_UPDATER_API ma_error_t ma_updater_event_get_date_time(const ma_updater_event_t *update_event, const char **date_time) ;


ma_error_t ma_updater_event_from_variant(ma_variant_t *var_updater_event, const char *session_id, ma_updater_event_t **updater_event);

ma_error_t ma_updater_event_release(ma_updater_event_t *update_event);

MA_CPP(})

#endif  /* MA_UPDATER_EVENT_H_INCLUDED */

