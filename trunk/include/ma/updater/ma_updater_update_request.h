/** @file ma_updater_update_request.h
 *  @brief Update Request interfaces
 *
*/
#ifndef MA_UPDATER_UPDATE_REQUEST_H_INCLUDED
#define MA_UPDATER_UPDATE_REQUEST_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_variant.h"
#include "ma/updater/ma_updater_update_task.h"
#include "ma/updater/ma_updater_client_defs.h"

MA_CPP(extern "C" {)

typedef struct ma_updater_update_request_s ma_updater_update_request_t, *ma_updater_update_request_h;

/**
*  update type
*/
typedef enum ma_updater_update_type_e {
    MA_UPDATER_UPDATE_TYPE_NORMAL_UPDATE=0,
    MA_UPDATER_UPDATE_TYPE_ROLLBACK_UPDATE=1
}ma_updater_update_type_t;

/**
    * @brief updater request create
    * @param update_type            [in]            update type
    * @param request                [out]			updater request
	* @result										MA_OK on success
	*												MA_ERROR_INVALIDARG on invalid argument passed
	*												MA_ERROR_OUTOFMEMORY on memory allocation failed
    *
    * @remark   
    */
MA_UPDATER_API ma_error_t ma_updater_update_request_create(ma_updater_update_type_t update_type, ma_updater_update_request_t **request);

/**
    * @brief updater request release
    * @param request                [in]			updater update request
	* @result										MA_OK on success
	*												MA_ERROR_INVALIDARG on invalid argument passed
    *
    * @remark   
    */
MA_UPDATER_API ma_error_t ma_updater_update_request_release(ma_updater_update_request_t *request);

/**
    * @brief set product list in update request
    * @param request                [in]			updater request
	* @param product_list           [in]			product list
	* @result										MA_OK on success
	*												MA_ERROR_INVALIDARG on invalid argument passed
    *
    * @remark   
    */
MA_UPDATER_API ma_error_t ma_updater_update_request_set_product_list(ma_updater_update_request_t *request, ma_array_t *product_list);

/**
    * @brief get product list from update request
    * @param request                [in]			updater request
	* @param product_list           [out]			product list
	* @result										MA_OK on success
	*												MA_ERROR_INVALIDARG on invalid argument passed
    *
    * @remark   
    */
MA_UPDATER_API ma_error_t ma_updater_update_request_get_product_list(ma_updater_update_request_t *request, ma_array_t **product_list);

/**
    * @brief set locale
    * @param request                [in]			updater request
	* @param locale		            [in]			locale id
	* @result										MA_OK on success
	*												MA_ERROR_INVALIDARG on invalid argument passed
    *
    * @remark   
    */
MA_UPDATER_API ma_error_t ma_updater_update_request_set_locale(ma_updater_update_request_t *request, ma_uint32_t locale);

/**
    * @brief get locale
    * @param request                [in]			updater request
	* @param locale		            [out]			locale id
	* @result										MA_OK on success
	*												MA_ERROR_INVALIDARG on invalid argument passed
    *
    * @remark   
    */
MA_UPDATER_API ma_error_t ma_updater_update_request_get_locale(ma_updater_update_request_t *request, ma_uint32_t *locale);

/**
    * @brief set ui option (show/dont show)
    * @param request                [in]			updater request
	* @param is_showui	            [in]			1:show, 0:dont show ui
	* @result										MA_OK on success
	*												MA_ERROR_INVALIDARG on invalid argument passed
    *
    * @remark   
    */
MA_UPDATER_API ma_error_t ma_updater_update_request_set_uioption(ma_updater_update_request_t *request, ma_bool_t is_showui);

/**
    * @brief get ui option 
	* @param request                [in]			updater request
	* @param is_showui	            [out]			show ui setting
	* @result										MA_OK on success
	*												MA_ERROR_INVALIDARG on invalid argument passed
    *
    * @remark   
    */
MA_UPDATER_API ma_error_t ma_updater_update_request_get_uioption(ma_updater_update_request_t *request, ma_bool_t *is_showui);

/**
    * @brief get update request type 
	* @param request                [in]			updater request
	* @param update_type            [out]			update type
	* @result										MA_OK on success
	*												MA_ERROR_INVALIDARG on invalid argument passed
    *
    * @remark   
    */
MA_UPDATER_API ma_error_t ma_updater_update_request_get_type(ma_updater_update_request_t *request, ma_updater_update_type_t *update_type);

/**
    * @brief get update request initiater type 
	* @param request                [in]			updater request
	* @param initiator_type         [out]			initiator type
	* @result										MA_OK on success
	*												MA_ERROR_INVALIDARG on invalid argument passed
    *
    * @remark   
    */
MA_UPDATER_API ma_error_t ma_updater_update_request_get_initiator_type(ma_updater_update_request_t *request, ma_updater_initiator_type_t *initiator_type) ;

/**
    * @brief set update request initiater type 
	* @param request                [in]			updater request
	* @param initiator_type         [in]			initiator type
	* @result										MA_OK on success
	*												MA_ERROR_INVALIDARG on invalid argument passed
    *
    * @remark   
    */
MA_UPDATER_API ma_error_t ma_updater_update_request_set_initiator_type(ma_updater_update_request_t *request, ma_updater_initiator_type_t initiator_type) ;

/**
    * @brief set updater UI provider name
	* @param request                [in]			updater request
	* @param name			        [in]			ui provider name
	* @result										MA_OK on success
	*												MA_ERROR_INVALIDARG on invalid argument passed
    *
    * @remark   
    */
MA_UPDATER_API ma_error_t ma_updater_update_request_set_ui_provider_name(ma_updater_update_request_t *request, const char *name);

/**
    * @brief get updater UI provider name
	* @param request                [in]			updater request
	* @param name			        [out]			ui provider name
	* @result										MA_OK on success
	*												MA_ERROR_INVALIDARG on invalid argument passed
    *
    * @remark   
    */
MA_UPDATER_API ma_error_t ma_updater_update_request_get_ui_provider_name(ma_updater_update_request_t *request, const char **name);

MA_CPP(})

#endif  /* MA_UPDATER_UPDATE_REQUEST_H_INCLUDED */

