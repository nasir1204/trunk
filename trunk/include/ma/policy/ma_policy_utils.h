/** @file ma_policy_utils.h
 *  @brief Search Product details in table
 *
*/
#ifndef MA_POLICY_UTILS_H_INCLUDED
#define MA_POLICY_UTILS_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_variant.h"

MA_CPP(extern "C" {)

/**
* @brief search product id in table
* @param product_id	        [in]			product id to be search
* @param table				[in]			table, in which product id to be search
* @param product_found	    [out]			does product found
* @result									MA_OK on all cases.
*
* @remark 
*/
ma_error_t search_product_id(const char *product_id, ma_table_t *table, ma_bool_t *product_found);

MA_CPP(})    

#endif /* MA_POLICY_UTILS_H_INCLUDED */
