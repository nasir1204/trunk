/** @file ma_policy.h
 *  @brief Access Policies of your product
 *
*/
#ifndef MA_POLICY_H_INCLUDED
#define MA_POLICY_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/policy/ma_policy_uri.h"
#include "ma/policy/ma_policy_bag.h"
#include "ma/ma_client.h"

MA_CPP(extern "C" {)

/**
 * Callback to be implemented by applications to get the policy notification info.
 * @param ma_client    			   [in]         MA client.
 * @param product_id			   [in]	        Product id which register to get the policy notifications.
 * @param info_uri                 [in]         Instance of ma_policy_uri_t. The notification info can be retrieved from the URI using URI get APIs.
 * @param cb_data                  [in,optinal] callback data as it was specified in 'ma_policy_register_notification_callback', use NULL if do not want any data to be passed.
 */
typedef ma_error_t (*ma_policy_notification_cb_t)(ma_client_t *ma_client, const char *product_id, const ma_policy_uri_t *info_uri, void *cb_data);

/**
 * Registers a policy notification with MA, the provided callback will be called to notify the policy notification with information.
 * @param ma_client    			[in]  MA client.
 * @param product_id			[in]  Product id which register to get the policy notifications.
 * @notify_type                 [in]  type of notification to be registered for policy notifications e.g. MA_POLICY_TIMEOUT | MA_POLICY_CHANGED | MA_USER_CHANGED or MA_LOCATION_CHANGED
 * @param cb                    [in]  callback to be invoked when policy notification occurs.
 * @param cb_data               [in]  callback data, this will be provided in the callback. It is opaque to MA.
 * @result                            MA_OK on success.
 *                                    MA_ERROR_INVALIDARG on invalid argument passed.
 *                                    MA_ERROR_POLICY_CALLBACK_ALREADY_REGISTERED if already registered with the product id.
 * @remark product_id can be NULL, if we want to get notification for product id same as client.
 * @remark can be called multiple times if user wants to register for policy notification with different product ID's.
 * @remark Notification with MA_POLICY_CHANGED will be called only if the policies are changed for the product id mentioned. Other product policy changes are not going to affect this.
 * @remark Notification with MA_LOCATION_CHANGED is futuristic and not supported in this release.
 * @remark Incremental/Changed policies can be fetched using "get" with POLICY_VERSION(MA_POLICY_GET_POLICY_VERSION) passed in URI object.
 *
 */
MA_POLICY_API ma_error_t ma_policy_register_notification_callback(ma_client_t *ma_client, const char *product_id, ma_uint32_t notify_type, ma_policy_notification_cb_t cb, void *cb_data);

/**
 * Unregister a policy notification from MA.
 * @param ma_client    		    [in]  MA client.
 * @param product_id			[in]  Product id which register to get the policy notifications.
 * @result                            MA_OK on success.
 *                                    MA_ERROR_INVALIDARG on invalid argument passed.
 *                                    MA_ERROR_PROPERTY_PROVIDER_ALREADY_REGISTERED if the provider already registered with the product id.
 * @remark product_id can be NULL, if product id same as client.
 * @remark should be called exact times the number for which register is called.
 */
MA_POLICY_API ma_error_t ma_policy_unregister_notification_callback(ma_client_t *ma_client, const char *product_id);

/**
 * Fetch policies from MA based on Policies URI.
 * @param ma_client     [in]    MA client.
 * @param uri	        [in]    Instance of ma_policy_uri_t and fields can be set by URI interfaces.
 * @param policy_bag   	[out]   Policy bag object returns on success.
 * @result                      MA_OK on success.
 *                              MA_ERROR_INVALIDARG on invalid argument passed.
 * 
 * @remark  if user is passing the same URI object passed with notification it will impact that way. 
            for e.g. session id used in uri with "MA_POLICY_CHANGED" will fetch you only incremental policies.
 */
MA_POLICY_API ma_error_t ma_policy_get(ma_client_t *ma_client, const ma_policy_uri_t *uri, ma_policy_bag_t **policy_bag); 


/**
 * Fetch policies from MA based on Policies URI.
 * @param ma_client     [in]    MA client.
 * @param uri	        [in]    Instance of ma_policy_uri_t and fields can be set by URI interfaces.
 * @param policy_bag   	[out]   Policy bag object.
 * @result                      MA_OK on success.
 *                              MA_ERROR_INVALIDARG on invalid argument passed.
 * 
 */
MA_POLICY_API ma_error_t ma_policy_set(ma_client_t *ma_client, const ma_policy_uri_t *uri, const ma_policy_bag_t *policy_bag);


/**
 * Refresh policies.
 * @param ma_client    [in] MA client.
 * @param uri	       [in] Instance of ma_policy_uri_t and fields can be set by URI interfaces.
 * @result                  MA_OK on success
 *                          MA_ERROR_INVALIDARG on invalid argument passed
 *
 */
MA_POLICY_API ma_error_t ma_policy_refresh(ma_client_t *ma_client, const ma_policy_uri_t *uri);


/**
 * Enforce policies.
 * @param ma_client    [in] MA client.
 * @param product_id   [in] product id for which policy enforcement needs to be done.
 * @result                  MA_OK on success
 *                          MA_ERROR_INVALIDARG on invalid argument passed
 * @remark  Call only on very need basis, as it impacts every PP's as of now. 
 *
 */
MA_POLICY_API ma_error_t ma_policy_enforce(ma_client_t *ma_client, const char *product_id);

MA_CPP(})    

#include "ma/dispatcher/ma_policy_dispatcher.h"

#endif /* MA_POLICY_H_INCLUDED */
