/** @file ma_policy_bag.h
 *  @brief Interfaces to get policy data from policy bag  
 *  
 */

#ifndef MA_POLICY_BAG_H_INCLUDED
#define MA_POLICY_BAG_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_variant.h"

#include "ma/policy/ma_policy_uri.h"

MA_CPP(extern "C" {)

typedef struct ma_policy_bag_s ma_policy_bag_t, *ma_policy_bag_h;

typedef struct ma_policy_bag_iterator_s ma_policy_bag_iterator_t, *ma_policy_bag_iterator_h;


/**
 * Create Policy bag.
 * @param bag	       [out]    Policy bag instance
 * @result                      MA_OK on success
 *                              MA_ERROR_INVALIDARG on invalid argument passed
 *
 */
MA_POLICY_API ma_error_t ma_policy_bag_create(ma_policy_bag_t **bag);

/**
 * Add reference on Policy bag.
 * @param bag	       [out]    Policy bag instance
 * @result                      MA_OK on success
 *                              MA_ERROR_INVALIDARG on invalid argument passed
 *
 */
MA_POLICY_API ma_error_t ma_policy_bag_add_ref(ma_policy_bag_t *bag);

/**
 * Release Policy bag.
 * @param bag	       [in]    Policy bag instance to be released
 * @result                     MA_OK on success
 *                             MA_ERROR_INVALIDARG on invalid argument passed
 *
 */
MA_POLICY_API ma_error_t ma_policy_bag_release(ma_policy_bag_t *bag);

/**
 * Get Policy setting value from policy bag. 
 * @param bag	        [in]    Policy bag instance which contains policy data
 * @param uri        	[in]    Instance of ma_policy_uri_t. It can be constructed using ma_policy_uri_t APIs and URI set macros.
 * @param section_name 	[in]    Name of the policy section 
 * @param key_name     	[in]    Name of the policy key name
 * @param value   	    [out]   ma_variant_t type value to be returned to the caller
 * @result                      MA_OK on success
 *                              MA_ERROR_INVALIDARG on invalid argument passed
 *  
 */
MA_POLICY_API ma_error_t ma_policy_bag_get_value(const ma_policy_bag_t *bag, const ma_policy_uri_t *uri, const char *section_name, const char *key_name, ma_variant_t **value);

/**
 * Set Policy setting value from policy bag.
 * @param bag	        [in]    Policy bag instance which contains policy data
 * @param uri        	[in]    Instance of ma_policy_uri_t. It can be constructed using ma_policy_uri_t APIs and URI set macros.
 * @param section_name 	[in]    Name of the policy section 
 * @param key_name     	[in]    Name of the policy key name
 * @param value   	    [in]    ma_variant_t type value to be set in the bag
 * @result                      MA_OK on success
 *                              MA_ERROR_INVALIDARG on invalid argument passed
 * 
 */
MA_POLICY_API ma_error_t ma_policy_bag_set_value(ma_policy_bag_t *bag, const ma_policy_uri_t *uri, const char *section_name, const char *key_name, const ma_variant_t *value);


/**
 * Creates Iterator on policy bag. This iterator can be used to iterate the policy bag to retrieve the policy policy data from policy bag
 * @param bag	       [in]    Policy bag instance which contains the policy data
 * @param iterator     [out]   Iterator instance that returns to the caller
 * @result                     MA_OK on success
 *                             MA_ERROR_INVALIDARG on invalid argument passed
 */
MA_POLICY_API ma_error_t ma_policy_bag_get_iterator(ma_policy_bag_t *bag,  ma_policy_bag_iterator_t **iterator);

/**
 * Creates uri Iterator on policy bag . This iterator can be used to iterate the policy bag uri's to retrive uri's
 * @param bag	       [in]    Policy bag instance which contains the policy data
 * @param iterator     [out]   Iterator instance that returns to the caller
 * @result                     MA_OK on success
 *                             MA_ERROR_INVALIDARG on invalid argument passed
 */
MA_POLICY_API ma_error_t ma_policy_bag_get_uri_iterator(ma_policy_bag_t *bag,  ma_policy_bag_iterator_t **iterator);

/**
 * Creates section Iterator on policy bag uri's . This iterator can be used to iterate the policy bag uri's  sections to retrive sections for the corresponding uri
 * @param bag	       [in]    Policy bag instance which contains the policy data
 * @param uri	       [in]    uri for which sections needs to be iterated
 * @param iterator     [out]   Iterator instance that returns to the caller
 * @result                     MA_OK on success
 *                             MA_ERROR_INVALIDARG on invalid argument passed
 */
MA_POLICY_API ma_error_t ma_policy_bag_get_section_iterator(ma_policy_bag_t *bag, const ma_policy_uri_t *uri, ma_policy_bag_iterator_t **iterator);

/**
 * Creates setting Iterator on policy bag uri-section. This iterator can be used to iterate the policy bag uri section to retrive secttings for the corresponding uri-section
 * @param bag	       [in]    Policy bag instance which contains the policy data
 * @param uri	       [in]    uri which represents pso
 * @param uri	       [in]    section name for which settings needs to be retrieved
 * @param iterator     [out]   Iterator instance that returns to the caller
 * @result                     MA_OK on success
 *                             MA_ERROR_INVALIDARG on invalid argument passed
 */

MA_POLICY_API ma_error_t ma_policy_bag_get_setting_iterator(ma_policy_bag_t *bag, const ma_policy_uri_t *uri, const char *section, ma_policy_bag_iterator_t **iterator);

/**
 * Release Policy bag iterator.
 * @param iterator	   [in]    Policy Iterator instance to be released
 * @result                     MA_OK on success
 *                             MA_ERROR_INVALIDARG on invalid argument passed
 *
 */
MA_POLICY_API ma_error_t ma_policy_bag_iterator_release(ma_policy_bag_iterator_t *iterator);

/**
 * Get next policy data from iterator. 
 * @param bag	        [in]    Policy iterator instance.
 * @param uri        	[out]   Instance of ma_policy_uri_t. feature, category, type and name can be get using URI get macros.
 * @param section_name 	[out]   Name of the policy section 
 * @param key_name     	[out]   Name of the policy key name
 * @param value   	    [out]   ma_variant_t type value
 * @result                      MA_OK on success
 *                              MA_ERROR_NO_MORE_ITEMS on end of the iteration.
 *                              MA_ERROR_INVALIDARG on invalid argument passed
 *                              MA_ERROR_INVALID_POLICY_BAG_ITERATOR on invalid iterator.
 * 
 */
MA_POLICY_API ma_error_t ma_policy_bag_iterator_get_next(ma_policy_bag_iterator_t *iterator, const ma_policy_uri_t **uri, const char **section_name, const char **key_name, const ma_variant_t **value);

/**
 * Get next policy uri from iterator. 
 * @param bag	        [in]    Policy iterator instance.
 * @param uri        	[out]   Instance of ma_policy_uri_t
 * @result                      MA_OK on success
 *                              MA_ERROR_NO_MORE_ITEMS on end of the iteration.
 *                              MA_ERROR_INVALIDARG on invalid argument passed
 *                              MA_ERROR_INVALID_POLICY_BAG_ITERATOR on invalid iterator.
 * 
 */
MA_POLICY_API ma_error_t ma_policy_bag_uri_iterator_get_next(ma_policy_bag_iterator_t *iterator, const ma_policy_uri_t **uri);

/**
 * Get next policy section from iterator. 
 * @param bag	        [in]    Policy iterator instance.
 * @param section       [out]   Name of the policy section
 * @result                      MA_OK on success
 *                              MA_ERROR_NO_MORE_ITEMS on end of the iteration.
 *                              MA_ERROR_INVALIDARG on invalid argument passed
 *                              MA_ERROR_INVALID_POLICY_BAG_ITERATOR on invalid iterator.
 * 
 */
MA_POLICY_API ma_error_t ma_policy_bag_section_iterator_get_next(ma_policy_bag_iterator_t *iterator, const char **section_name);

/**
 * Get next policy section from iterator. 
 * @param bag	        [in]    Policy iterator instance.
 * @param key_name      [out]   Name of the policy key name
 * @param key_value     [out]   ma_variant_t type key value to be returned to the caller
 * @result                      MA_OK on success
 *                              MA_ERROR_NO_MORE_ITEMS on end of the iteration.
 *                              MA_ERROR_INVALIDARG on invalid argument passed
 *                              MA_ERROR_INVALID_POLICY_BAG_ITERATOR on invalid iterator.
 * 
 */
MA_POLICY_API ma_error_t ma_policy_bag_setting_iterator_get_next(ma_policy_bag_iterator_t *iterator, const char **key_name, const ma_variant_t **key_value);



MA_CPP(})

#endif /* MA_POLICY_BAG_H_INCLUDED */
