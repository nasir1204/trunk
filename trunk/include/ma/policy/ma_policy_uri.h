/** @file ma_policy_uri.h
 *  @brief Policy Uri interfaces
 *
*/
#ifndef MA_POLICY_URI_H_INCLUDED
#define MA_POLICY_URI_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_variant.h"

MA_CPP(extern "C" {)

typedef struct ma_policy_uri_s ma_policy_uri_t, *ma_policy_uri_h;

/**
 * User log on states
 *
 */
typedef enum ma_user_logon_state_e {   
    MA_USER_NOT_LOGGED_ON = 0,
    MA_USER_LOG_ON = 1, 
    MA_USER_LOGGED_ON = 2, 
    MA_USER_LOG_OFF = 3
} ma_user_logon_state_t;

/**
 * Policy notification types to be registered for policy notifications
 *
 */
typedef enum ma_policy_notification_type_e {
    MA_POLICY_TIMEOUT               = 0x01,
    MA_POLICY_CHANGED               = 0x02,
    MA_USER_CHANGED                 = 0x04,
    MA_LOCATION_CHANGED             = 0x08,
	MA_SERVICE_STARTUP	            = 0x10,
    MA_POLICY_FORCED_ENFORCEMENT    = 0x20,
    MA_POLICY_NOTIFICATION_ALL      = 0x3F
} ma_policy_notification_type_t;

/**
* @brief create policy uri
* @param uri		        [out]				policy uri
* @result										MA_OK on success.
*												MA_ERROR_INVALIDARG on invalid argument passed.
*												MA_ERROR_OUTOFMEMORY on memory allocation failure.
*
* @remark 
*/
MA_POLICY_API ma_error_t ma_policy_uri_create(ma_policy_uri_t **uri);

/**
* @brief copy policy uri to destination
* @param uri_src		        [in]			source uri
* @param uri_dest		        [out]			destination uri
* @result										MA_OK on success.
*												MA_ERROR_INVALIDARG on invalid argument passed.
*												MA_ERROR_PRECONDITION on passing NULL call back pointer value
*
* @remark 
*/
MA_POLICY_API ma_error_t ma_policy_uri_copy(const ma_policy_uri_t *uri_src, ma_policy_uri_t **uri_dest);

/**
* @brief add reference in policy uri
* @param uri			        [in]			policy uri
* @result										MA_OK on success.
*												MA_ERROR_INVALIDARG on invalid argument passed.
*
* @remark 
*/
MA_POLICY_API ma_error_t ma_policy_uri_add_ref(ma_policy_uri_t *uri);

/**
* @brief release policy uri
* @param uri			        [in]			policy uri
* @result										MA_OK on success.
*												MA_ERROR_INVALIDARG on invalid argument passed.
*
* @remark 
*/
MA_POLICY_API ma_error_t ma_policy_uri_release(ma_policy_uri_t *uri);

/* URI get Interfaces */
/**
* @brief get notification type of policy uri.
* @param uri			        [in]			policy uri
* @param type			        [out]			uri type
* @result										MA_OK on success.
*												MA_ERROR_INVALIDARG on invalid argument passed.
*
* @remark 
*/
MA_POLICY_API ma_error_t ma_policy_uri_get_notification_type(const ma_policy_uri_t *uri, ma_policy_notification_type_t *type);


/**
* @brief get software id of policy uri.
* @param uri			        [in]			policy uri
* @param software_id	        [out]			software id
* @result										MA_OK on success.
*												MA_ERROR_INVALIDARG on invalid argument passed.
*
* @remark 
*/
MA_POLICY_API ma_error_t ma_policy_uri_get_software_id(const ma_policy_uri_t *uri, const char **software_id);

/**
* @brief get version of policy uri.
* @param uri			        [in]			policy uri
* @param version		        [out]			version
* @result										MA_OK on success.
*												MA_ERROR_INVALIDARG on invalid argument passed.
*
* @remark 
*/
MA_POLICY_API ma_error_t ma_policy_uri_get_version(const ma_policy_uri_t *uri, const char **version);

/**
* @brief get user of policy uri.
* @param uri			        [in]			policy uri
* @param user			        [out]			user
* @result										MA_OK on success.
*												MA_ERROR_INVALIDARG on invalid argument passed.
*
* @remark 
*/
MA_POLICY_API ma_error_t ma_policy_uri_get_user(const ma_policy_uri_t *uri, const char **user);

/**
* @brief get user session id.
* @param uri			        [in]			policy uri
* @param session_id		        [out]			session id
* @result										MA_OK on success.
*												MA_ERROR_INVALIDARG on invalid argument passed.
*
* @remark 
*/
MA_POLICY_API ma_error_t ma_policy_uri_get_user_session_id(const ma_policy_uri_t *uri, ma_uint32_t *session_id);

/**
* @brief get user logon state
* @param uri			        [in]			policy uri
* @param logon_state	        [out]			logon state
* @result										MA_OK on success.
*												MA_ERROR_INVALIDARG on invalid argument passed.
*
* @remark 
*/
MA_POLICY_API ma_error_t ma_policy_uri_get_user_logon_state(const ma_policy_uri_t *uri, ma_user_logon_state_t *logon_state);

/**
* @brief get location id
* @param uri			        [in]			policy uri
* @param location_id	        [out]			location id
* @result										MA_OK on success.
*												MA_ERROR_INVALIDARG on invalid argument passed.
*
* @remark 
*/
MA_POLICY_API ma_error_t ma_policy_uri_get_location_id(const ma_policy_uri_t *uri, const char **location_id);

/* Get policy information */
/**
* @brief get policy feature
* @param uri			        [in]			policy uri
* @param feature		        [out]			feature
* @result										MA_OK on success.
*												MA_ERROR_INVALIDARG on invalid argument passed.
*
* @remark 
*/
MA_POLICY_API ma_error_t ma_policy_uri_get_feature(const ma_policy_uri_t *uri, const char **feature);

/**
* @brief get policy category
* @param uri			        [in]			policy uri
* @param category		        [out]			category
* @result										MA_OK on success.
*												MA_ERROR_INVALIDARG on invalid argument passed.
*
* @remark 
*/
MA_POLICY_API ma_error_t ma_policy_uri_get_category(const ma_policy_uri_t *uri, const char **category);

/**
* @brief get uri type
* @param uri			        [in]			policy uri
* @param type			        [out]			type
* @result										MA_OK on success.
*												MA_ERROR_INVALIDARG on invalid argument passed.
*
* @remark 
*/
MA_POLICY_API ma_error_t ma_policy_uri_get_type(const ma_policy_uri_t *uri, const char **type);

/**
* @brief get uri name
* @param uri			        [in]			policy uri
* @param name			        [out]			name
* @result										MA_OK on success.
*												MA_ERROR_INVALIDARG on invalid argument passed.
*
* @remark 
*/
MA_POLICY_API ma_error_t ma_policy_uri_get_name(const ma_policy_uri_t *uri, const char **name);

/**
* @brief get uri pso name
* @param uri			        [in]			policy uri
* @param pso_name		        [out]			pso name
* @result										MA_OK on success.
*												MA_ERROR_INVALIDARG on invalid argument passed.
*
* @remark 
*/
MA_POLICY_API ma_error_t ma_policy_uri_get_pso_name(const ma_policy_uri_t *uri, const char **pso_name);

/**
* @brief get uri pso parameter in integer
* @param uri			        [in]			policy uri
* @param pso_param_int	        [out]			pso parameter in integer
* @result										MA_OK on success.
*												MA_ERROR_INVALIDARG on invalid argument passed.
*
* @remark 
*/
MA_POLICY_API ma_error_t ma_policy_uri_get_pso_param_int(const ma_policy_uri_t *uri, const char **pso_param_int);

/**
* @brief get uri pso parameter in string
* @param uri			        [in]			policy uri
* @param pso_param_str	        [out]			pso parameter in string
* @result										MA_OK on success.
*												MA_ERROR_INVALIDARG on invalid argument passed.
*
* @remark 
*/
MA_POLICY_API ma_error_t ma_policy_uri_get_pso_param_str(const ma_policy_uri_t *uri, const char **pso_param_str);

/* URI set Interfaces */
/**
* @brief set software id in policy uri
* @param uri			        [in]			policy uri
* @param software_id	        [in]			software id
* @result										MA_OK on success.
*												MA_ERROR_INVALIDARG on invalid argument passed.
*
* @remark 
*/
MA_POLICY_API ma_error_t ma_policy_uri_set_software_id(ma_policy_uri_t *uri, const char *software_id);

/**
* @brief set version in policy uri
* @param uri			        [in]			policy uri
* @param version		        [in]			version
* @result										MA_OK on success.
*												MA_ERROR_INVALIDARG on invalid argument passed.
*
* @remark 
*/
MA_POLICY_API ma_error_t ma_policy_uri_set_version(ma_policy_uri_t *uri, const char *version);

/**
* @brief set user in policy uri
* @param uri			        [in]			policy uri
* @param user			        [in]			user
* @result										MA_OK on success.
*												MA_ERROR_INVALIDARG on invalid argument passed.
*
* @remark 
*/
MA_POLICY_API ma_error_t ma_policy_uri_set_user(ma_policy_uri_t *uri, const char *user);

/**
* @brief set user list in policy uri
* @param uri			        [in]			policy uri
* @param user_list		        [in]			user list
* @result										MA_OK on success.
*												MA_ERROR_INVALIDARG on invalid argument passed.
*
* @remark 
*/
MA_POLICY_API ma_error_t ma_policy_uri_set_user_list(ma_policy_uri_t *uri, ma_array_t *user_list);

/**
* @brief set location id in policy uri
* @param uri			        [in]			policy uri
* @param location_id	        [in]			location_id
* @result										MA_OK on success.
*												MA_ERROR_INVALIDARG on invalid argument passed.
*
* @remark 
*/
MA_POLICY_API ma_error_t ma_policy_uri_set_location_id(ma_policy_uri_t *uri, const char *location_id);

/* Set policy information */
/**
* @brief set feature of policy uri
* @param uri			        [in]			policy uri
* @param feature		        [in]			feature
* @result										MA_OK on success.
*												MA_ERROR_INVALIDARG on invalid argument passed.
*
* @remark 
*/
MA_POLICY_API ma_error_t ma_policy_uri_set_feature(ma_policy_uri_t *uri, const char *feature);

/**
* @brief set category of policy uri
* @param uri			        [in]			policy uri
* @param category		        [in]			category
* @result										MA_OK on success.
*												MA_ERROR_INVALIDARG on invalid argument passed.
*
* @remark 
*/
MA_POLICY_API ma_error_t ma_policy_uri_set_category(ma_policy_uri_t *uri, const char *category);

/**
* @brief set type of policy uri
* @param uri			        [in]			policy uri
* @param type			        [in]			type
* @result										MA_OK on success.
*												MA_ERROR_INVALIDARG on invalid argument passed.
*
* @remark 
*/
MA_POLICY_API ma_error_t ma_policy_uri_set_type(ma_policy_uri_t *uri, const char *type);

/**
* @brief set name of policy uri
* @param uri			        [in]			policy uri
* @param name			        [in]			name
* @result										MA_OK on success.
*												MA_ERROR_INVALIDARG on invalid argument passed.
*
* @remark 
*/
MA_POLICY_API ma_error_t ma_policy_uri_set_name(ma_policy_uri_t *uri, const char *name);

/**
* @brief set pso name in policy uri
* @param uri			        [in]			policy uri
* @param pso_name		        [in]			pso name
* @result										MA_OK on success.
*												MA_ERROR_INVALIDARG on invalid argument passed.
*
* @remark 
*/
MA_POLICY_API ma_error_t ma_policy_uri_set_pso_name(ma_policy_uri_t *uri, const char *pso_name);

/**
* @brief set integer pso parameter in policy uri
* @param uri			        [in]			policy uri
* @param pso_param_int	        [in]			integer pso parameter
* @result										MA_OK on success.
*												MA_ERROR_INVALIDARG on invalid argument passed.
*
* @remark 
*/
MA_POLICY_API ma_error_t ma_policy_uri_set_pso_param_int(ma_policy_uri_t *uri, const char *pso_param_int);

/**
* @brief set string pso parameter in policy uri
* @param uri			        [in]			policy uri
* @param pso_param_str	        [in]			string pso parameter
* @result										MA_OK on success.
*												MA_ERROR_INVALIDARG on invalid argument passed.
*
* @remark 
*/
MA_POLICY_API ma_error_t ma_policy_uri_set_pso_param_str(ma_policy_uri_t *uri, const char *pso_param_str);

MA_CPP(})

#endif /* MA_POLICY_URI_H_INCLUDED */
