#ifndef MA_COMMON_H_INCLUDED
#define MA_COMMON_H_INCLUDED

#ifdef __cplusplus
# define MA_CPP(x) x
#else
# define MA_CPP(x)
#endif

#define MA_C_SCOPE_BEGIN MA_CPP(extern "C" {)
#define MA_C_SCOPE_END MA_CPP(})


#include <stddef.h>

MA_C_SCOPE_BEGIN

#define MA_TRUE  1
#define MA_FALSE 0

#ifdef _MSC_VER
# define MA_INLINE __inline /* use __inline (VC++ specific) */
#define MA_STATIC_INLINE static __inline
#else
# define MA_INLINE inline        /* use standard inline */
#define MA_STATIC_INLINE static inline
#endif

#ifdef _MSC_VER
/* Windows - set up  dll import/export decorators. */
# if defined(MA_BUILDING_DISPATCHER_SHARED)
/* Building shared library. */
#   define MA_DISPATCHER_API __declspec(dllexport)
# elif !defined(MA_USE_DISPATCHER_STATIC)
/* Using shared library. */
#   define MA_DISPATCHER_API __declspec(dllimport)
# else
/* Building static library or executable (unit test). */
#   define MA_DISPATCHER_API /* nothing */
# endif
#elif __GNUC__ >= 4
# define MA_DISPATCHER_API __attribute__((visibility("default")))
#else
# define MA_DISPATCHER_API /* nothing */
#endif

#ifdef _MSC_VER
/* Windows - set up dll import/export decorators. */
# if defined(MA_BUILDING_DATASTORE_SHARED)
/* Building shared library. */
#   define MA_DATASTORE_API __declspec(dllexport)
# elif !defined(MA_USE_DATASTORE_STATIC)
/* Using shared library. */
#   define MA_DATASTORE_API __declspec(dllimport)
# else
/* Building static library or executable (unit test). */
#   define MA_DATASTORE_API /* nothing */
# endif
#elif __GNUC__ >= 4
# define MA_DATASTORE_API __attribute__((visibility("default")))
#else
# define MA_DATASTORE_API /* nothing */
#endif

#if !defined(MA_NO_DISPATCHER) && !defined(MA_NO_VARIANT_DISPATCHER)
/* Using dispatcher */
#   define MA_VARIANT_API MA_STATIC_INLINE
#else
#ifdef _MSC_VER
/* Windows - set up dll import/export decorators. */
#if defined(MA_BUILDING_VARIANT_SHARED)
/* Building shared library. */
#   define MA_VARIANT_API __declspec(dllexport)
# elif !defined(MA_USE_VARIANT_STATIC)
/* Using shared library. */
#   define MA_VARIANT_API __declspec(dllimport)
# else
/* Building static library or executable (unit test). */
#   define MA_VARIANT_API /* nothing */
# endif
#elif __GNUC__ >= 4
# define MA_VARIANT_API __attribute__((visibility("default")))
#else
# define MA_VARIANT_API /* nothing */
#endif
#endif

#if !defined(MA_NO_DISPATCHER) && !defined(MA_NO_SCHEDULER_DISPATCHER)
/* Using dispatcher */
#   define MA_SCHEDULER_API MA_STATIC_INLINE
#else
#ifdef _MSC_VER
/* Windows - set up dll import/export decorators. */
# if defined(MA_BUILDING_SCHEDULER_SHARED)
/* Building shared library. */
#   define MA_SCHEDULER_API __declspec(dllexport)
# elif !defined(MA_USE_SCHEDULER_STATIC)
/* Using shared library. */
#   define MA_SCHEDULER_API __declspec(dllimport)
# else
/* Building static library or executable (unit test). */
#   define MA_SCHEDULER_API /* nothing */
# endif
#elif __GNUC__ >= 4
# define MA_SCHEDULER_API __attribute__((visibility("default")))
#else
# define MA_SCHEDULER_API /* nothing */
#endif
#endif

#if !defined(MA_NO_DISPATCHER) && !defined(MA_NO_BOOKING_DISPATCHER)
/* Using dispatcher */
#   define MA_BOOKING_API MA_STATIC_INLINE
#else
#ifdef _MSC_VER
/* Windows - set up dll import/export decorators. */
# if defined(MA_BUILDING_BOOKING_SHARED)
/* Building shared library. */
#   define MA_BOOKING_API __declspec(dllexport)
# elif !defined(MA_USE_BOOKING_STATIC)
/* Using shared library. */
#   define MA_BOOKING_API __declspec(dllimport)
# else
/* Building static library or executable (unit test). */
#   define MA_BOOKING_API /* nothing */
# endif
#elif __GNUC__ >= 4
# define MA_BOOKING_API __attribute__((visibility("default")))
#else
# define MA_BOOKING_API /* nothing */
#endif
#endif

#if !defined(MA_NO_DISPATCHER) && !defined(MA_NO_COMMENT_DISPATCHER)
/* Using dispatcher */
#   define MA_COMMENT_API MA_STATIC_INLINE
#else
#ifdef _MSC_VER
/* Windows - set up dll import/export decorators. */
# if defined(MA_BUILDING_COMMENT_SHARED)
/* Building shared library. */
#   define MA_BOOKING_API __declspec(dllexport)
# elif !defined(MA_USE_COMMENT_STATIC)
/* Using shared library. */
#   define MA_COMMENT_API __declspec(dllimport)
# else
/* Building static library or executable (unit test). */
#   define MA_COMMENT_API /* nothing */
# endif
#elif __GNUC__ >= 4
# define MA_COMMENT_API __attribute__((visibility("default")))
#else
# define MA_COMMENT_API /* nothing */
#endif
#endif

#if !defined(MA_NO_DISPATCHER) && !defined(MA_NO_BOARD_DISPATCHER)
/* Using dispatcher */
#   define MA_BOARD_API MA_STATIC_INLINE
#else
#ifdef _MSC_VER
/* Windows - set up dll import/export decorators. */
# if defined(MA_BUILDING_BOARD_SHARED)
/* Building shared library. */
#   define MA_BOARD_API __declspec(dllexport)
# elif !defined(MA_USE_BOARD_STATIC)
/* Using shared library. */
#   define MA_BOARD_API __declspec(dllimport)
# else
/* Building static library or executable (unit test). */
#   define MA_BOARD_API /* nothing */
# endif
#elif __GNUC__ >= 4
# define MA_BOARD_API __attribute__((visibility("default")))
#else
# define MA_BOARD_API /* nothing */
#endif
#endif

#if !defined(MA_NO_DISPATCHER) && !defined(MA_NO_SEARCH_DISPATCHER)
/* Using dispatcher */
#   define MA_SEARCH_API MA_STATIC_INLINE
#else
#ifdef _MSC_VER
/* Windows - set up dll import/export decorators. */
# if defined(MA_BUILDING_SEARCH_SHARED)
/* Building shared library. */
#   define MA_SEARCH_API __declspec(dllexport)
# elif !defined(MA_USE_SEARCH_STATIC)
/* Using shared library. */
#   define MA_SEARCH_API __declspec(dllimport)
# else
/* Building static library or executable (unit test). */
#   define MA_SEARCH_API /* nothing */
# endif
#elif __GNUC__ >= 4
# define MA_SEARCH_API __attribute__((visibility("default")))
#else
# define MA_SEARCH_API /* nothing */
#endif
#endif

#if !defined(MA_NO_DISPATCHER) && !defined(MA_NO_SEASON_DISPATCHER)
/* Using dispatcher */
#   define MA_SEASON_API MA_STATIC_INLINE
#else
#ifdef _MSC_VER
/* Windows - set up dll import/export decorators. */
# if defined(MA_BUILDING_SEASON_SHARED)
/* Building shared library. */
#   define MA_SEASON_API __declspec(dllexport)
# elif !defined(MA_USE_SEASON_STATIC)
/* Using shared library. */
#   define MA_SEASON_API __declspec(dllimport)
# else
/* Building static library or executable (unit test). */
#   define MA_SEASON_API /* nothing */
# endif
#elif __GNUC__ >= 4
# define MA_SEASON_API __attribute__((visibility("default")))
#else
# define MA_SEASON_API /* nothing */
#endif
#endif

#if !defined(MA_NO_DISPATCHER) && !defined(MA_NO_RECORDER_DISPATCHER)
/* Using dispatcher */
#   define MA_RECORDER_API MA_STATIC_INLINE
#else
#ifdef _MSC_VER
/* Windows - set up dll import/export decorators. */
# if defined(MA_BUILDING_RECORDER_SHARED)
/* Building shared library. */
#   define MA_RECORDER_API __declspec(dllexport)
# elif !defined(MA_USE_RECORDER_STATIC)
/* Using shared library. */
#   define MA_RECORDER_API __declspec(dllimport)
# else
/* Building static library or executable (unit test). */
#   define MA_RECORDER_API /* nothing */
# endif
#elif __GNUC__ >= 4
# define MA_RECORDER_API __attribute__((visibility("default")))
#else
# define MA_RECORDER_API /* nothing */
#endif
#endif

#if !defined(MA_NO_DISPATCHER) && !defined(MA_NO_INVENTORY_DISPATCHER)
/* Using dispatcher */
#   define MA_INVENTORY_API MA_STATIC_INLINE
#else
#ifdef _MSC_VER
/* Windows - set up dll import/export decorators. */
# if defined(MA_BUILDING_INVENTORY_SHARED)
/* Building shared library. */
#   define MA_INVENTORY_API __declspec(dllexport)
# elif !defined(MA_USE_INVENTORY_STATIC)
/* Using shared library. */
#   define MA_INVENTORY_API __declspec(dllimport)
# else
/* Building static library or executable (unit test). */
#   define MA_INVENTORY_API /* nothing */
# endif
#elif __GNUC__ >= 4
# define MA_INVENTORY_API __attribute__((visibility("default")))
#else
# define MA_INVENTORY_API /* nothing */
#endif
#endif


#if !defined(MA_NO_DISPATCHER) && !defined(MA_NO_LOCATION_DISPATCHER)
/* Using dispatcher */
#   define MA_LOCATION_API MA_STATIC_INLINE
#else
#ifdef _MSC_VER
/* Windows - set up dll import/export decorators. */
# if defined(MA_BUILDING_LOCATION_SHARED)
/* Building shared library. */
#   define MA_LOCATION_API __declspec(dllexport)
# elif !defined(MA_USE_LOCATION_STATIC)
/* Using shared library. */
#   define MA_LOCATION_API __declspec(dllimport)
# else
/* Building static library or executable (unit test). */
#   define MA_LOCATION_API /* nothing */
# endif
#elif __GNUC__ >= 4
# define MA_LOCATION_API __attribute__((visibility("default")))
#else
# define MA_LOCATION_API /* nothing */
#endif
#endif

#if !defined(MA_NO_DISPATCHER) && !defined(MA_NO_PROFILE_DISPATCHER)
/* Using dispatcher */
#   define MA_PROFILE_API MA_STATIC_INLINE
#else
#ifdef _MSC_VER
/* Windows - set up dll import/export decorators. */
# if defined(MA_BUILDING_PROFILE_SHARED)
/* Building shared library. */
#   define MA_PROFILE_API __declspec(dllexport)
# elif !defined(MA_USE_PROFILE_STATIC)
/* Using shared library. */
#   define MA_PROFILE_API __declspec(dllimport)
# else
/* Building static library or executable (unit test). */
#   define MA_PROFILE_API /* nothing */
# endif
#elif __GNUC__ >= 4
# define MA_PROFILE_API __attribute__((visibility("default")))
#else
# define MA_PROFILE_API /* nothing */
#endif
#endif

#if !defined(MA_NO_DISPATCHER) && !defined(MA_NO_CONSIGN_DISPATCHER)
/* Using dispatcher */
#   define MA_CONSIGN_API MA_STATIC_INLINE
#else
#ifdef _MSC_VER
/* Windows - set up dll import/export decorators. */
# if defined(MA_BUILDING_CONSIGN_SHARED)
/* Building shared library. */
#   define MA_CONSIGN_API __declspec(dllexport)
# elif !defined(MA_USE_CONSIGN_STATIC)
/* Using shared library. */
#   define MA_CONSIGN_API __declspec(dllimport)
# else
/* Building static library or executable (unit test). */
#   define MA_CONSIGN_API /* nothing */
# endif
#elif __GNUC__ >= 4
# define MA_CONSIGN_API __attribute__((visibility("default")))
#else
# define MA_CONSIGN_API /* nothing */
#endif
#endif


#if !defined(MA_NO_DISPATCHER) && !defined(MA_NO_MAIL_DISPATCHER)
/* Using dispatcher */
#   define MA_MAIL_API MA_STATIC_INLINE
#else
#ifdef _MSC_VER
/* Windows - set up dll import/export decorators. */
# if defined(MA_BUILDING_MAIL_SHARED)
/* Building shared library. */
#   define MA_MAIL_API __declspec(dllexport)
# elif !defined(MA_USE_MAIL_STATIC)
/* Using shared library. */
#   define MA_MAIL_API __declspec(dllimport)
# else
/* Building static library or executable (unit test). */
#   define MA_MAIL_API /* nothing */
# endif
#elif __GNUC__ >= 4
# define MA_MAIL_API __attribute__((visibility("default")))
#else
# define MA_MAIL_API /* nothing */
#endif
#endif

#if !defined(MA_NO_DISPATCHER) && !defined(MA_NO_SENSOR_DISPATCHER)
/* Using dispatcher */
#   define MA_SENSOR_API MA_STATIC_INLINE
#else
#ifdef _MSC_VER
/* Windows - set up dll import/export decorators. */
# if defined(MA_BUILDING_SENSOR_SHARED)
/* Building shared library. */
#   define MA_SENSOR_API __declspec(dllexport)
# elif !defined(MA_USE_SENSOR_STATIC)
/* Using shared library. */
#   define MA_SENSOR_API __declspec(dllimport)
# else
/* Building static library or executable (unit test). */
#   define MA_SENSOR_API /* nothing */
# endif
#elif __GNUC__ >= 4
# define MA_SENSOR_API __attribute__((visibility("default")))
#else
# define MA_SENSOR_API /* nothing */
#endif
#endif

#if !defined(MA_NO_DISPATCHER) && !defined(MA_NO_PROPERTY_DISPATCHER)
/* Using dispatcher */
#   define MA_PROPERTY_API MA_STATIC_INLINE
#else
#ifdef _MSC_VER
/* Windows - set up dll import/export decorators. */
# if defined(MA_BUILDING_PROPERTY_SHARED)
/* Building shared library. */
#   define MA_PROPERTY_API __declspec(dllexport)
# elif !defined(MA_USE_PROPERTY_STATIC)
/* Using shared library. */
#   define MA_PROPERTY_API __declspec(dllimport)
# else
/* Building static library or executable (unit test). */
#   define MA_PROPERTY_API /* nothing */
# endif
#elif __GNUC__ >= 4
# define MA_PROPERTY_API __attribute__((visibility("default")))
#else
# define MA_PROPERTY_API /* nothing */
#endif
#endif


#if !defined(MA_NO_DISPATCHER) && !defined(MA_NO_POLICY_DISPATCHER)
/* Using dispatcher */
#   define MA_POLICY_API MA_STATIC_INLINE
#else
#ifdef _MSC_VER
/* Windows - set up dll import/export decorators. */
# if defined(MA_BUILDING_POLICY_SHARED)
/* Building shared library. */
#   define MA_POLICY_API __declspec(dllexport)
# elif !defined(MA_USE_POLICY_STATIC)
/* Using shared library. */
#   define MA_POLICY_API __declspec(dllimport)
# else
/* Building static library or executable (unit test). */
#   define MA_POLICY_API /* nothing */
# endif
#elif __GNUC__ >= 4
# define MA_POLICY_API __attribute__((visibility("default")))
#else
# define MA_POLICY_API /* nothing */
#endif
#endif


#if !defined(MA_NO_DISPATCHER) && !defined(MA_NO_EVENT_DISPATCHER)
/* Using dispatcher */
#   define MA_EVENT_API MA_STATIC_INLINE
#else
#ifdef _MSC_VER
/* Windows - set up dll import/export decorators. */
# if defined(MA_BUILDING_EVENT_SHARED)
/* Building shared library. */
#   define MA_EVENT_API __declspec(dllexport)
# elif !defined(MA_USE_EVENT_STATIC)
/* Using shared library. */
#   define MA_EVENT_API __declspec(dllimport)
# else
/* Building static library or executable (unit test). */
#   define MA_EVENT_API /* nothing */
# endif
#elif __GNUC__ >= 4
# define MA_EVENT_API __attribute__((visibility("default")))
#else
# define MA_EVENT_API /* nothing */
#endif
#endif


#if !defined(MA_NO_DISPATCHER) && !defined(MA_NO_CLIENT_CONTROLLER_DISPATCHER)
/* Using dispatcher */
#   define MA_CLIENT_CONTROLLER_API MA_STATIC_INLINE
#else
#ifdef _MSC_VER
/* Windows - set up dll import/export decorators. */
# if defined(MA_BUILDING_CLIENT_CONTROLLER_SHARED)
/* Building shared library. */
#   define MA_CLIENT_CONTROLLER_API __declspec(dllexport)
# elif !defined(MA_USE_CLIENT_CONTROLLER_STATIC)
/* Using shared library. */
#   define MA_CLIENT_CONTROLLER_API __declspec(dllimport)
# else
/* Building static library or executable (unit test). */
#   define MA_CLIENT_CONTROLLER_API /* nothing */
# endif
#elif __GNUC__ >= 4
# define MA_CLIENT_CONTROLLER_API __attribute__((visibility("default")))
#else
# define MA_CLIENT_CONTROLLER_API /* nothing */
#endif
#endif

#if !defined(MA_NO_DISPATCHER) && !defined(MA_NO_LOGGER_DISPATCHER)
/* Using dispatcher */
#   define MA_LOGGER_API MA_STATIC_INLINE
#else
#ifdef _MSC_VER
/* Windows - set up dll import/export decorators. */
# if defined(MA_BUILDING_LOGGER_SHARED)
/* Building shared library. */
#   define MA_LOGGER_API __declspec(dllexport)
# elif !defined(MA_USE_LOGGER_STATIC)
/* Using shared library. */
#   define MA_LOGGER_API __declspec(dllimport)
# else
/* Building static library or executable (unit test). */
#   define MA_LOGGER_API /* nothing */
# endif
#elif __GNUC__ >= 4
# define MA_LOGGER_API __attribute__((visibility("default")))
#else
# define MA_LOGGER_API /* nothing */
#endif
#endif


#if !defined(MA_NO_DISPATCHER) && !defined(MA_NO_COMPAT_SERVICE_DISPATCHER)
/* Using dispatcher */
#   define MA_COMPAT_SERVICE_API MA_STATIC_INLINE
#else
#ifdef _MSC_VER
/* Windows - set up dll import/export decorators. */
# if defined(MA_BUILDING_COMPAT_SERVICE_SHARED)
/* Building shared library. */
#   define MA_COMPAT_SERVICE_API __declspec(dllexport)
# elif !defined(MA_USE_COMPAT_SERVICE_STATIC)
/* Using shared library. */
#   define MA_COMPAT_SERVICE_API __declspec(dllimport)
# else
/* Building static library or executable (unit test). */
#   define MA_COMPAT_SERVICE_API /* nothing */
# endif
#elif __GNUC__ >= 4
# define MA_COMPAT_SERVICE_API __attribute__((visibility("default")))
#else
# define MA_COMPAT_SERVICE_API /* nothing */
#endif
#endif


#if !defined(MA_NO_DISPATCHER) && !defined(MA_NO_DATACHANNEL_DISPATCHER)
/* Using dispatcher */
#   define MA_DATACHANNEL_API MA_STATIC_INLINE
#else
#ifdef _MSC_VER
/* Windows - set up dll import/export decorators. */
# if defined(MA_BUILDING_DATACHANNEL_SHARED)
/* Building shared library. */
#   define MA_DATACHANNEL_API __declspec(dllexport)
# elif !defined(MA_USE_DATACHANNEL_STATIC)
/* Using shared library. */
#   define MA_DATACHANNEL_API __declspec(dllimport)
# else
/* Building static library or executable (unit test). */
#   define MA_DATACHANNEL_API /* nothing */
# endif
#elif __GNUC__ >= 4
# define MA_DATACHANNEL_API __attribute__((visibility("default")))
#else
# define MA_DATACHANNEL_API /* nothing */
#endif
#endif


#if !defined(MA_NO_DISPATCHER) && !defined(MA_NO_UPDATER_DISPATCHER)
/* Using dispatcher */
#   define MA_UPDATER_API MA_STATIC_INLINE
#else
#ifdef _MSC_VER
/* Windows - set up dll import/export decorators. */
# if defined(MA_BUILDING_UPDATER_SHARED)
/* Building shared library. */
#   define MA_UPDATER_API __declspec(dllexport)
# elif !defined(MA_USE_UPDATER_STATIC)
/* Using shared library. */
#   define MA_UPDATER_API __declspec(dllimport)
# else
/* Building static library or executable (unit test). */
#   define MA_UPDATER_API /* nothing */
# endif
#elif __GNUC__ >= 4
# define MA_UPDATER_API __attribute__((visibility("default")))
#else
# define MA_UPDATER_API /* nothing */
#endif
#endif

#if !defined(MA_NO_DISPATCHER) && !defined(MA_NO_REPOSITORY_DISPATCHER)
/* Using dispatcher */
#   define MA_REPOSITORY_API MA_STATIC_INLINE
#else
#ifdef _MSC_VER
/* Windows - set up dll import/export decorators. */
# if defined(MA_BUILDING_REPOSITORY_SHARED)
/* Building shared library. */
#   define MA_REPOSITORY_API __declspec(dllexport)
# elif !defined(MA_USE_REPOSITORY_STATIC)
/* Using shared library. */
#   define MA_REPOSITORY_API __declspec(dllimport)
# else
/* Building static library or executable (unit test). */
#   define MA_REPOSITORY_API /* nothing */
# endif
#elif __GNUC__ >= 4
# define MA_REPOSITORY_API __attribute__((visibility("default")))
#else
# define MA_REPOSITORY_API /* nothing */
#endif
#endif

#if !defined(MA_NO_DISPATCHER) && !defined(MA_NO_PROXY_DISPATCHER)
/* Using dispatcher */
#   define MA_PROXY_API MA_STATIC_INLINE
#else
#ifdef _MSC_VER
/* Windows - set up dll import/export decorators. */
# if defined(MA_BUILDING_PROXY_SHARED)
/* Building shared library. */
#   define MA_PROXY_API __declspec(dllexport)
# elif !defined(MA_USE_PROXY_STATIC)
/* Using shared library. */
#   define MA_PROXY_API __declspec(dllimport)
# else
/* Building static library or executable (unit test). */
#   define MA_PROXY_API /* nothing */
# endif
#elif __GNUC__ >= 4
# define MA_PROXY_API __attribute__((visibility("default")))
#else
# define MA_PROXY_API /* nothing */
#endif
#endif

#if !defined(MA_NO_DISPATCHER) && !defined(MA_NO_CRYPTO_DISPATCHER)
/* Using dispatcher */
#   define MA_CRYPTO_API MA_STATIC_INLINE
#else
#ifdef _MSC_VER
/* Windows - set up dll import/export decorators. */
# if defined(MA_BUILDING_CRYPTO_SHARED)
/* Building shared library. */
#   define MA_CRYPTO_API __declspec(dllexport)
# elif !defined(MA_USE_CRYPTO_STATIC)
/* Using shared library. */
#   define MA_CRYPTO_API __declspec(dllimport)
# else
/* Building static library or executable (unit test). */
#   define MA_CRYPTO_API /* nothing */
# endif
#elif __GNUC__ >= 4
# define MA_CRYPTO_API __attribute__((visibility("default")))
#else
# define MA_CRYPTO_API /* nothing */
#endif
#endif

#if !defined(MA_NO_DISPATCHER) && !defined(MA_NO_CLIENT_DISPATCHER)
/* Using dispatcher */
#   define MA_CLIENT_API MA_STATIC_INLINE
#else
#ifdef _MSC_VER
/* Windows - set up dll import/export decorators. */
# if defined(MA_BUILDING_CLIENT_SHARED)
/* Building shared library. */
#   define MA_CLIENT_API __declspec(dllexport)
# elif !defined(MA_USE_CLIENT_STATIC)
/* Using shared library. */
#   define MA_CLIENT_API __declspec(dllimport)
# else
/* Building static library or executable (unit test). */
#   define MA_CLIENT_API /* nothing */
# endif
#elif __GNUC__ >= 4
# define MA_CLIENT_API __attribute__((visibility("default")))
#else
# define MA_CLIENT_API /* nothing */
#endif
#endif

#if !defined(MA_NO_DISPATCHER) && !defined(MA_NO_SERVICE_MANAGER_DISPATCHER)
/* Using dispatcher */
#   define MA_SERVICE_MANAGER_API MA_STATIC_INLINE
#else
#ifdef _MSC_VER
/* Windows - set up dll import/export decorators. */
# if defined(MA_BUILDING_SERVICE_MANAGER_SHARED)
/* Building shared library. */
#   define MA_SERVICE_MANAGER_API __declspec(dllexport)
# elif !defined(MA_USE_SERVICE_MANAGER_STATIC)
/* Using shared library. */
#   define MA_SERVICE_MANAGER_API __declspec(dllimport)
# else
/* Building static library or executable (unit test). */
#   define MA_SERVICE_MANAGER_API /* nothing */
# endif
#elif __GNUC__ >= 4
# define MA_SERVICE_MANAGER_API __attribute__((visibility("default")))
#else
# define MA_SERVICE_MANAGER_API /* nothing */
#endif
#endif

#if !defined(MA_NO_DISPATCHER) && !defined(MA_NO_SELFPROTECT_DISPATCHER)
/* Using dispatcher */
#   define MA_SELFPROTECT_API MA_STATIC_INLINE
#else
#ifdef _MSC_VER
/* Windows - set up dll import/export decorators. */
# if defined(MA_BUILDING_SELFPROTECT_SHARED)
/* Building shared library. */
#   define MA_SELFPROTECT_API __declspec(dllexport)
# elif !defined(MA_USE_SELFPROTECT_STATIC)
/* Using shared library. */
#   define MA_SELFPROTECT_API __declspec(dllimport)
# else
/* Building static library or executable (unit test). */
#   define MA_SELFPROTECT_API /* nothing */
# endif
#elif __GNUC__ >= 4
# define MA_SELFPROTECT_API __attribute__((visibility("default")))
#else
# define MA_SELFPROTECT_API /* nothing */
#endif
#endif

#ifdef _MSC_VER 
	#if defined(MA_SERIALIZE_BUILDING_SHARED) 
		#define MA_SERIALIZE_API	__declspec(dllexport) 
	#elif !defined(MA_USE_SERIALIZE_STATIC) 
		#define MA_SERIALIZE_API	__declspec(dllimport) 
	#else 
		#define MA_SERIALIZE_API 
	#endif 
#elif __GNUC__  >=4 
#define MA_SERIALIZE_API	__attribute__((visibility("default"))) 
#else
#define MA_SERIALIZE_API /*Nothing*/
#endif


#ifdef _MSC_VER 
	#if defined(MA_DESERIALIZE_BUILDING_SHARED) 
		#define MA_DESERIALIZE_API	__declspec(dllexport) 
	#elif !defined(MA_USE_DESERIALIZE_STATIC) 
		#define MA_DESERIALIZE_API	__declspec(dllimport) 
	#else 
		#define MA_DESERIALIZE_API 
	#endif 
#elif __GNUC__  >=4 
#define MA_DESERIALIZE_API	__attribute__((visibility("default"))) 
#else
#define MA_DESERIALIZE_API /*Nothing*/
#endif

#ifdef _MSC_VER 
	#if defined(MA_UTILS_BUILDING_SHARED) 
		#define MA_UTILS_API	__declspec(dllexport) 
	#elif !defined(MA_USE_UTILS_STATIC) 
		#define MA_UTILS_API	__declspec(dllimport) 
	#else 
		#define MA_UTILS_API 
	#endif 
#elif __GNUC__  >=4 
#define MA_UTILS_API	__attribute__((visibility("default"))) 
#else
#define MA_UTILS_API /*Nothing*/
#endif

#ifdef _MSC_VER 
	#if defined(MA_DATABASE_BUILDING_SHARED) 
		#define MA_DATABASE_API	__declspec(dllexport) 
	#elif !defined(MA_USE_DATABASE_STATIC) 
		#define MA_DATABASE_API	__declspec(dllimport) 
	#else 
		#define MA_DATABASE_API 
	#endif 
#elif __GNUC__  >=4 
#define MA_DATABASE_API	__attribute__((visibility("default"))) 
#else
#define MA_DATABASE_API /*Nothing*/
#endif


#ifdef _MSC_VER 
	#if defined(MA_NETWORK_BUILDING_SHARED) 
		#define MA_NETWORK_API	__declspec(dllexport) 
	#elif !defined(MA_USE_NETWORK_STATIC) 
		#define MA_NETWORK_API	__declspec(dllimport) 
	#else 
		#define MA_NETWORK_API 
	#endif 
#elif __GNUC__  >=4 
#define MA_NETWORK_API	__attribute__((visibility("default"))) 
#else
#define MA_NETWORK_API /*Nothing*/
#endif


#ifdef _MSC_VER 
	#if defined(MA_XML_BUILDING_SHARED) 
		#define MA_XML_API	__declspec(dllexport) 
	#elif !defined(MA_USE_XML_STATIC) 
		#define MA_XML_API	__declspec(dllimport) 
	#else 
		#define MA_XML_API 
	#endif 
#elif __GNUC__  >=4 
#define MA_XML_API	__attribute__((visibility("default"))) 
#else
#define MA_XML_API /*Nothing*/
#endif

#ifdef _MSC_VER 
	#if defined(MA_AH_CLIENT_BUILDING_SHARED) 
		#define MA_AH_CLIENT_API	__declspec(dllexport) 
	#elif !defined(MA_USE_AH_CLIENT_STATIC) 
		#define MA_AH_CLIENT_API	__declspec(dllimport) 
	#else 
		#define MA_AH_CLIENT_API 
	#endif 
#elif __GNUC__  >=4 
#define MA_AH_CLIENT_API	__attribute__((visibility("default"))) 
#else
#define MA_AH_CLIENT_API /*Nothing*/
#endif

#ifdef _MSC_VER 
	#if defined(MA_IO_SERVICE_BUILDING_SHARED) 
		#define MA_IO_SERVICE_API	__declspec(dllexport) 
	#elif !defined(MA_USE_IO_SERVICE_STATIC) 
		#define MA_IO_SERVICE_API	__declspec(dllimport) 
	#else 
		#define MA_IO_SERVICE_API 
	#endif 
#elif __GNUC__  >=4 
#define MA_IO_SERVICE_API	__attribute__((visibility("default"))) 
#else
#define MA_IO_SERVICE_API /*Nothing*/
#endif

#ifdef _MSC_VER 
	#if defined(MA_DISCOVERY_BUILDING_SHARED) 
		#define MA_DISCOVERY_API	__declspec(dllexport) 
	#elif !defined(MA_USE_DISCOVERY_STATIC) 
		#define MA_DISCOVERY_API	__declspec(dllimport) 
	#else 
		#define MA_DISCOVERY_API 
	#endif 
#elif __GNUC__  >=4 
#define MA_DISCOVERY_API	__attribute__((visibility("default"))) 
#else
#define MA_DISCOVERY_API /*Nothing*/
#endif

#ifdef _MSC_VER 
	#if defined(MA_UDP_BUILDING_SHARED) 
		#define MA_UDP_API	__declspec(dllexport) 
	#elif !defined(MA_USE_UDP_STATIC) 
		#define MA_UDP_API	__declspec(dllimport) 
	#else 
		#define MA_UDP_API 
	#endif 
#elif __GNUC__  >=4 
#define MA_UDP_API	__attribute__((visibility("default"))) 
#else
#define MA_UDP_API /*Nothing*/
#endif

#ifdef _MSC_VER 
	#if defined(MA_P2P_BUILDING_SHARED) 
		#define MA_P2P_API	__declspec(dllexport) 
	#elif !defined(MA_USE_P2P_STATIC) 
		#define MA_P2P_API	__declspec(dllimport) 
	#else 
		#define MA_P2P_API 
	#endif 
#elif __GNUC__  >=4 
#define MA_P2P_API	__attribute__((visibility("default"))) 
#else
#define MA_P2P_API /*Nothing*/
#endif

#ifdef _MSC_VER 
	#if defined(MA_CONFIGURATOR_BUILDING_SHARED) 
		#define MA_CONFIGURATOR_API	__declspec(dllexport) 
	#elif !defined(MA_USE_CONFIGURATOR_STATIC) 
		#define MA_CONFIGURATOR_API	__declspec(dllimport) 
	#else 
		#define MA_CONFIGURATOR_API 
	#endif 
#elif __GNUC__  >=4 
#define MA_CONFIGURATOR_API	__attribute__((visibility("default"))) 
#else
#define MA_CONFIGURATOR_API /*Nothing*/
#endif

/* Misc. type aliases */
typedef double            ma_double_t;
typedef float             ma_float_t;
typedef unsigned char     ma_raw_t;
typedef wchar_t           ma_wchar_t;

#if !defined(_MSC_VER) || 1600 <= _MSC_VER
#ifndef HPUX
    #include <stdint.h>
#else
    #include <inttypes.h>
#endif
    typedef int8_t                  ma_int8_t;
    typedef uint8_t                 ma_uint8_t;
    typedef int16_t                 ma_int16_t;
    typedef uint16_t                ma_uint16_t;
    typedef int32_t                 ma_int32_t;
    typedef uint32_t                ma_uint32_t;
    typedef int64_t                 ma_int64_t;
    typedef uint64_t                ma_uint64_t;
#else
    typedef __int8                  ma_int8_t;
    typedef unsigned __int8         ma_uint8_t;
    typedef __int16                 ma_int16_t;
    typedef unsigned __int16        ma_uint16_t;
    typedef __int32                 ma_int32_t;
    typedef unsigned __int32        ma_uint32_t;
    typedef __int64                 ma_int64_t;
    typedef unsigned __int64        ma_uint64_t;
#endif

/* only C99 supports bool */
#if !defined(__STDC_VERSION__) || (__STDC_VERSION__ < 199901L)
    typedef ma_uint8_t              ma_bool_t;
#else
#include <stdbool.h>
    typedef bool                    ma_bool_t;
#endif

#if defined(_WIN32) || defined(__MINGW32__)
# define MA_WINDOWS 1
#endif

#if defined(__linux__)
# define MA_LINUX __linux__
#endif

#if defined(_MSC_VER) && defined(_WCHAR_T_DEFINED)
# define MA_HAS_WCHAR_T 1
# ifdef _NATIVE_WCHAR_T_DEFINED
#  define MA_HAS_NATIVE_WCHAR_T 1
# endif
# elif(1)
# define MA_HAS_WCHAR_T 1
# endif

#include "ma/ma_errors.h"

MA_CPP(})

#endif /* MA_COMMON_H_INCLUDED */
