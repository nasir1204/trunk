#if  !defined(MA_NO_DISPATCHER) && !defined(MA_NO_INFO_DISPATCHER)
#ifndef MA_INFO_DISPATCHER_H_INCLUDED
#define MA_INFO_DISPATCHER_H_INCLUDED

#include "ma/dispatcher/ma_dispatcher_macros.h"

#undef MA_DISPATCHER_PRODUCT_NAME
#undef MA_DISPATCHER_LIBRARY_NAME
#undef MA_DISPATCHER_MODULE
#undef MA_DISPATCHER_LIBRARY_VERSION

#define MA_DISPATCHER_PRODUCT_NAME		 "EPOAGENT3000"
#define MA_DISPATCHER_LIBRARY_NAME		 MA_DEFINE_DISPATCHER_LIB(ma_client)
#define MA_DISPATCHER_MODULE			 "ma_info"
#define MA_DISPATCHER_LIBRARY_VERSION	 0x05020100

static unsigned int ma_info_handler_lib_version;

/*
MA_CLIENT_API ma_error_t ma_info_events_register_callback(ma_client_t *ma_client, const char *product_id, ma_info_events_cb_t info_events_cb, void *cb_data) ;
MA_CLIENT_API ma_error_t ma_info_events_unregister_callback(ma_client_t *ma_client, const char *product_id) ;
MA_CLIENT_API ma_error_t ma_product_info_get_license_state(ma_client_t *client, const char *software_id, ma_buffer_t **license_state);
MA_CLIENT_API ma_error_t ma_info_get(ma_client_t *client, char *key, ma_variant_t **value);
*/

MA_CPP(extern "C" {)
#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#define MA_DISPATCHER_ACTUAL_ARG_EXPANDER\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_info_events_register_callback, (ma_client_t *ma_client, const char *product_id, ma_info_events_cb_t info_events_cb, void *cb_data), (ma_client, product_id, info_events_cb,  cb_data)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_info_events_unregister_callback, (ma_client_t *ma_client, const char *product_id), (ma_client, product_id)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_info_get,(ma_client_t *client, const char *key, ma_variant_t **value), (client, key, value))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_product_info_get_license_state, (ma_client_t *client, const char *software_id, ma_buffer_t **license_state), (client, software_id,license_state ))

static const char *ma_info_handler_function_names[] = {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) #function,
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
};

typedef struct ma_info_handler_functions_s {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) ret_type (*function##_fn) signature;
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
}ma_info_handler_functions_t;
static ma_info_handler_functions_t* ma_info_handler_functions = NULL;

#define ma_info_handler_functions_size (sizeof(ma_info_handler_function_names)/sizeof(ma_info_handler_function_names[0]))

#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) \
    MA_DISTPATCHER_ARG_STUB_CREATOR(&ma_info_handler_lib_version, MA_DISPATCHER_PRODUCT_NAME, MA_DISPATCHER_LIBRARY_NAME, MA_DISPATCHER_LIBRARY_VERSION,MA_DISPATCHER_MODULE,ret_type, function, signature, arg, ma_info_handler_function_names, &ma_info_handler_functions, ma_info_handler_functions_size)
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER 
#undef MA_DISPATCHER_ARG_EXPANDER

#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER 
MA_CPP(})
#endif /*MA_INFO_DISPATCHER_H_INCLUDED*/
#endif /*!defined(MA_NO_DISPATCHER) && !defined(MA_NO_INFO_DISPATCHER) */

