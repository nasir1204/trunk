#if  !defined(MA_NO_DISPATCHER) && !defined(MA_NO_CRYPTO_DISPATCHER)
#ifndef MA_CRYPTO_DISPATCHER_H_INCLUDED
#define MA_CRYPTO_DISPATCHER_H_INCLUDED

#include "ma/dispatcher/ma_dispatcher_macros.h"

#undef MA_DISPATCHER_PRODUCT_NAME
#undef MA_DISPATCHER_LIBRARY_NAME
#undef MA_DISPATCHER_MODULE
#undef MA_DISPATCHER_LIBRARY_VERSION

#define MA_DISPATCHER_PRODUCT_NAME		 "EPOAGENT3000"
#define MA_DISPATCHER_LIBRARY_NAME		 MA_DEFINE_DISPATCHER_LIB(ma_client)
#define MA_DISPATCHER_MODULE			 "crypto"
#define MA_DISPATCHER_LIBRARY_VERSION	 0x05020100

static unsigned int ma_crypto_lib_version;

MA_CPP(extern "C" {)
#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#define MA_DISPATCHER_ACTUAL_ARG_EXPANDER\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_encrypt_data_by_type,(ma_client_t *client, const char *algo_type, const unsigned char *data, size_t data_len, ma_buffer_t **buffer), (client, algo_type, data, data_len, buffer))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_decrypt_data_by_type,(ma_client_t *client, const char *algo_type, const unsigned char *data, size_t data_len, ma_buffer_t **buffer), (client, algo_type, data, data_len, buffer))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_hash_data_by_type,(ma_client_t *client, const char *digest_type, const unsigned char *data, size_t data_len, ma_buffer_t **buffer), (client, digest_type, data, data_len, buffer))



    static const char *ma_crypto_function_names[] = {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) #function,
        MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
};

typedef struct ma_crypto_functions_s {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) ret_type (*function##_fn) signature;
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
}ma_crypto_functions_t;
static ma_crypto_functions_t* ma_crypto_functions = NULL;

#define ma_crypto_functions_size (sizeof(ma_crypto_function_names)/sizeof(ma_crypto_function_names[0]))

#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) \
    MA_DISTPATCHER_ARG_STUB_CREATOR(&ma_crypto_lib_version, MA_DISPATCHER_PRODUCT_NAME, MA_DISPATCHER_LIBRARY_NAME, MA_DISPATCHER_LIBRARY_VERSION,MA_DISPATCHER_MODULE,ret_type, function, signature, arg, ma_crypto_function_names, &ma_crypto_functions, ma_crypto_functions_size)
MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER

#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER
    MA_CPP(})

#endif /* MA_CRYPTO_DISPATCHER_H_INCLUDED */
#endif /*MA_USE_DISPATCHER*/

