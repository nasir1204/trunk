#if  !defined(MA_NO_DISPATCHER) && !defined(MA_NO_SCHEDULER_DISPATCHER)
#ifndef MA_SCHEDULER_DISPATCHER_H_INCLUDED
#define MA_SCHEDULER_DISPATCHER_H_INCLUDED

#include "ma/dispatcher/ma_dispatcher_macros.h"

#undef MA_DISPATCHER_PRODUCT_NAME
#undef MA_DISPATCHER_LIBRARY_NAME
#undef MA_DISPATCHER_MODULE
#undef MA_DISPATCHER_LIBRARY_VERSION

#define MA_DISPATCHER_PRODUCT_NAME		 "EPOAGENT3000"
#define MA_DISPATCHER_LIBRARY_NAME		 MA_DEFINE_DISPATCHER_LIB(ma_client)
#define MA_DISPATCHER_MODULE			 "ma_scheduler"
#define MA_DISPATCHER_LIBRARY_VERSION	 0x05020100

static unsigned int ma_scheduler_lib_version;

MA_CPP(extern "C" {)
#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#define MA_DISPATCHER_ACTUAL_ARG_EXPANDER\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_scheduler_get_version,(ma_uint32_t *version),(version))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_scheduler_register_task_callbacks,(ma_client_t *ma_client, const char *product_id, const char *task_type, ma_scheduler_task_callbacks_t const *task_callbacks, void *cb_data),(ma_client,product_id,task_type,task_callbacks,cb_data))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_scheduler_unregister_task_callbacks,(ma_client_t *ma_client, const char *product_id, const char *task_type), (ma_client, product_id,task_type))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_scheduler_add_task,(ma_client_t *ma_client, ma_task_t *task),(ma_client, task))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_scheduler_get_task,(ma_client_t *ma_client, const char *task_id, ma_task_t **task), (ma_client, task_id, task))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_scheduler_remove_task,(ma_client_t *ma_client, const char *task_id),(ma_client, task_id))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_scheduler_alter_task,(ma_client_t *ma_client, ma_task_t *task),(ma_client, task))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_scheduler_update_task_status,(ma_client_t *ma_client, const char *task_id, ma_task_exec_status_t status),(ma_client, task_id, status))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_scheduler_enumerate_task,(ma_client_t *ma_client, const char *product_id, const char *creator_id, const char *task_type, ma_task_t ***task_set, size_t *no_of_tasks),(ma_client, product_id, creator_id, task_type, task_set, no_of_tasks))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_scheduler_enumerate_task_release,(ma_task_t **task_set, size_t *no_of_tasks),(task_set, no_of_tasks))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_scheduler_postpone_task,(ma_client_t *ma_client, const char *task_id, size_t intervals),(ma_client, task_id, intervals))


static const char *ma_scheduler_function_names[] = {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) #function,
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
};

typedef struct ma_scheduler_functions_s {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) ret_type (*function##_fn) signature;
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
}ma_scheduler_functions_t;
static ma_scheduler_functions_t *ma_scheduler_functions;

#define ma_scheduler_function_table_size (sizeof(ma_scheduler_function_names)/sizeof(ma_scheduler_function_names[0]))

#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) \
    MA_DISTPATCHER_ARG_STUB_CREATOR(&ma_scheduler_lib_version, MA_DISPATCHER_PRODUCT_NAME, MA_DISPATCHER_LIBRARY_NAME, MA_DISPATCHER_LIBRARY_VERSION,MA_DISPATCHER_MODULE,ret_type, function, signature, arg, ma_scheduler_function_names, &ma_scheduler_functions, ma_scheduler_function_table_size)
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER 
#undef MA_DISPATCHER_ARG_EXPANDER

#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER 
MA_CPP(})
#endif
#endif //MA_USE_DISPATCHER
