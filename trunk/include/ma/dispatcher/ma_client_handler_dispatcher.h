#if  !defined(MA_NO_DISPATCHER) && !defined(MA_NO_CLIENT_DISPATCHER)
#ifndef MA_CLIENT_DISPATCHER_H_INCLUDED
#define MA_CLIENT_DISPATCHER_H_INCLUDED

#include "ma/dispatcher/ma_dispatcher_macros.h"

#undef MA_DISPATCHER_PRODUCT_NAME
#undef MA_DISPATCHER_LIBRARY_NAME
#undef MA_DISPATCHER_MODULE
#undef MA_DISPATCHER_LIBRARY_VERSION

#define MA_DISPATCHER_PRODUCT_NAME		 "EPOAGENT3000"
#define MA_DISPATCHER_LIBRARY_NAME		 MA_DEFINE_DISPATCHER_LIB(ma_client)
#define MA_DISPATCHER_MODULE			 "ma_client_handler"
#define MA_DISPATCHER_LIBRARY_VERSION	 0x05020100

static unsigned int ma_client_handler_lib_version;

/*
ma_error_t ma_client_handler_create(const char *product_id,  ma_client_handler_t **handler);
ma_error_t ma_client_handler_start(ma_client_handler_t *handler);
ma_error_t ma_client_handler_stop(ma_client_handler_t *handler);
ma_error_t ma_client_handler_release(ma_client_handler_t *handler);
ma_error_t ma_client_handler_get_context(ma_client_handler_t *handler, const ma_context_t **context);
ma_error_t ma_client_handler_set_logger(ma_client_handler_t *handler, ma_logger_t *logger);
ma_error_t ma_client_handler_get_logger(ma_client_handler_t *handler, ma_logger_t **logger);
ma_error_t ma_client_handler_get_msgbus(ma_client_handler_t *handler, ma_msgbus_t **msgbus);
ma_error_t ma_client_handler_get_thread_option(ma_client_handler_t *handler, ma_msgbus_callback_thread_options_t *thread_option)
*/

MA_CPP(extern "C" {)
#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#define MA_DISPATCHER_ACTUAL_ARG_EXPANDER\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_client_handler_create,(const char *product_id, ma_client_handler_t **client_handler), (product_id, client_handler))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_client_handler_start, (ma_client_handler_t *handler), (handler))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_client_handler_stop, (ma_client_handler_t *handler), (handler))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_client_handler_release,(ma_client_handler_t *handler), (handler))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_client_handler_get_context,(ma_client_handler_t *handler, const ma_context_t **context),(handler, context))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_client_handler_set_thread_option,(ma_client_handler_t *handler, ma_msgbus_callback_thread_options_t thread_option),(handler, thread_option))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_client_handler_get_thread_option,(ma_client_handler_t *handler, ma_msgbus_callback_thread_options_t *thread_option),(handler, thread_option))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_client_handler_set_consumer_reachability,(ma_client_handler_t *handler, ma_msgbus_consumer_reachability_t consumer_reach),(handler, consumer_reach))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_client_handler_get_consumer_reachability,(ma_client_handler_t *handler, ma_msgbus_consumer_reachability_t *consumer_reach),(handler, consumer_reach))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_client_handler_set_logger,(ma_client_handler_t *handler, ma_logger_t *logger),(handler, logger))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_client_handler_set_logger_callback,(ma_client_handler_t *handler, ma_client_log_cb_t log_cb, void *user_data),(handler, log_cb, user_data))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_client_handler_set_msgbus_passphrase_callback,(ma_client_handler_t *handler, ma_msgbus_passphrase_provider_callback_t cb, void *cb_data),(handler,cb,cb_data))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_client_handler_get_msgbus,(ma_client_handler_t *handler, ma_msgbus_t **msgbus),(handler, msgbus))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_client_handler_set_msgbus,(ma_client_handler_t *handler, ma_msgbus_t *msgbus),(handler, msgbus))

static const char *ma_client_handler_function_names[] = {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) #function,
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
};

typedef struct ma_client_handler_functions_s {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) ret_type (*function##_fn) signature;
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
}ma_client_handler_functions_t;
static ma_client_handler_functions_t* ma_client_handler_functions = NULL;

#define ma_client_handler_functions_size (sizeof(ma_client_handler_function_names)/sizeof(ma_client_handler_function_names[0]))

#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) \
    MA_DISTPATCHER_ARG_STUB_CREATOR(&ma_client_handler_lib_version, MA_DISPATCHER_PRODUCT_NAME, MA_DISPATCHER_LIBRARY_NAME, MA_DISPATCHER_LIBRARY_VERSION,MA_DISPATCHER_MODULE,ret_type, function, signature, arg, ma_client_handler_function_names, &ma_client_handler_functions, ma_client_handler_functions_size)
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER 
#undef MA_DISPATCHER_ARG_EXPANDER

#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER 
MA_CPP(})
#endif
#endif //MA_USE_DISPATCHER

