#if  !defined(MA_NO_DISPATCHER) && !defined(MA_NO_CUSTOM_EVENT_DISPATCHER)
#ifndef MA_CUSTOM_EVENT_DISPATCHER_H_INCLUDED
#define MA_CUSTOM_EVENT_DISPATCHER_H_INCLUDED

#include "ma/dispatcher/ma_dispatcher_macros.h"

#undef MA_DISPATCHER_PRODUCT_NAME
#undef MA_DISPATCHER_LIBRARY_NAME
#undef MA_DISPATCHER_MODULE
#undef MA_DISPATCHER_LIBRARY_VERSION

#define MA_DISPATCHER_PRODUCT_NAME		 "EPOAGENT3000"
#define MA_DISPATCHER_LIBRARY_NAME		  MA_DEFINE_DISPATCHER_LIB(ma_client)
#define MA_DISPATCHER_MODULE			 "ma_custom_event"
#define MA_DISPATCHER_LIBRARY_VERSION	 0x05020100

static unsigned int ma_custom_event_lib_version;

MA_CPP(extern "C" {)
#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#define MA_DISPATCHER_ACTUAL_ARG_EXPANDER\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_custom_event_create,(ma_client_t *ma_client, const char *event_root_name, ma_custom_event_t **event, ma_xml_node_t **root_node), (ma_client, event_root_name, event, root_node))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_custom_event_create_from_buffer,(ma_client_t *ma_client, const char *buffer, ma_custom_event_t **event), (ma_client, buffer, event))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_custom_event_create_event_node,(ma_custom_event_t *event, ma_xml_node_t *parent, const char *event_node_name, ma_uint32_t event_id, ma_event_severity_t severity, ma_xml_node_t **node), (event, parent, event_node_name, event_id, severity, node))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_custom_event_send,(ma_custom_event_t *event), (event))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_custom_event_release,(ma_custom_event_t *event), (event))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_custom_event_node_create,(ma_xml_node_t *parent, const char *name, ma_xml_node_t **node), (parent, name, node))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_custom_event_node_attribute_set,(ma_xml_node_t *node, const char *name, const char *value), (node, name, value))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_custom_event_node_set_data,(ma_xml_node_t *node, const char *data), (node, data))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_custom_event_get_root_node,(ma_custom_event_t *event, ma_xml_node_t **root_node), (event, root_node))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_custom_event_node_data_get,(ma_xml_node_t *node, const char **data), (node, data))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_custom_event_node_name_get,(ma_xml_node_t *node, const char **name), (node, name))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_custom_event_node_attribute_get,(ma_xml_node_t *node, const char *attribute, const char **data), (node, attribute, data))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_custom_event_node_search,(ma_xml_node_t *node, const char *name, ma_xml_node_t **data), (node, name, data))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_custom_event_get_child_node,(ma_xml_node_t *node, ma_xml_node_t **child_node), (node, child_node))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_custom_event_get_next_node,(ma_xml_node_t *node, ma_xml_node_t **next_node), (node, next_node))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_custom_event_get_next_sibling,(ma_xml_node_t *node, ma_xml_node_t **sibling_node), (node, sibling_node))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_custom_event_add_ref,(ma_custom_event_t *self), (self))

static const char *ma_custom_event_function_names[] = {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) #function,
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
};

typedef struct ma_custom_event_functions_s {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) ret_type (*function##_fn) signature;
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
}ma_custom_event_functions_t;
static ma_custom_event_functions_t* ma_custom_event_functions = NULL;

#define ma_custom_event_functions_size (sizeof(ma_custom_event_function_names)/sizeof(ma_custom_event_function_names[0]))

#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) \
    MA_DISTPATCHER_ARG_STUB_CREATOR(&ma_custom_event_lib_version, MA_DISPATCHER_PRODUCT_NAME, MA_DISPATCHER_LIBRARY_NAME, MA_DISPATCHER_LIBRARY_VERSION,MA_DISPATCHER_MODULE,ret_type, function, signature, arg, ma_custom_event_function_names, &ma_custom_event_functions, ma_custom_event_functions_size)
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER

#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER
MA_CPP(})
#endif
#endif

