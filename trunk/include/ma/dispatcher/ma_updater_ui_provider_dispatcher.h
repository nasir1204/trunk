#if  !defined(MA_NO_DISPATCHER) && !defined(MA_NO_UPDATER_DISPATCHER)
#ifndef MA_UPDATER_UI_PROVIDER_DISPATCHER_H_INCLUDED
#define MA_UPDATER_UI_PROVIDER_DISPATCHER_H_INCLUDED

#include "ma/dispatcher/ma_dispatcher_macros.h"

#undef MA_DISPATCHER_PRODUCT_NAME
#undef MA_DISPATCHER_LIBRARY_NAME
#undef MA_DISPATCHER_MODULE
#undef MA_DISPATCHER_LIBRARY_VERSION

#define MA_DISPATCHER_PRODUCT_NAME		 "EPOAGENT3000"
#define MA_DISPATCHER_LIBRARY_NAME		 MA_DEFINE_DISPATCHER_LIB(ma_client)
#define MA_DISPATCHER_MODULE			 "updater"
#define MA_DISPATCHER_LIBRARY_VERSION	 0x05020100

static unsigned int ma_updater_ui_provider_version;

MA_CPP(extern "C" {)
#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#define MA_DISPATCHER_ACTUAL_ARG_EXPANDER\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_updater_register_ui_provider_callbacks,(ma_client_t *ma_client, const char *software_id, ma_msgbus_callback_thread_options_t cb_thread_option, ma_updater_ui_provider_callbacks_t const *callbacks, void *cb_data), (ma_client, software_id, cb_thread_option, callbacks, cb_data)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_updater_get_ui_provider_name,(ma_client_t *ma_client, const char *software_id, const char **name), (ma_client, software_id, name)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_updater_unregister_ui_provider_callbacks,(ma_client_t *ma_client, const char *software_id), (ma_client, software_id)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_updater_show_ui_response_post,(ma_client_t *ma_client, ma_updater_show_ui_response_t *show_ui_response), (ma_client, show_ui_response)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_updater_show_ui_request_get_initiator_type, (const ma_updater_show_ui_request_t *request, ma_uint32_t *initiated_type),(request, initiated_type)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_updater_show_ui_request_get_title,(const ma_updater_show_ui_request_t *request, const char **title), (request, title)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_updater_show_ui_request_get_message,(const ma_updater_show_ui_request_t *request, const char **message), (request, message)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_updater_show_ui_request_get_count_down_message,(const ma_updater_show_ui_request_t *request, const char **count_down_message),(request, count_down_message)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_updater_show_ui_request_get_count_down_value,(const ma_updater_show_ui_request_t *request, ma_uint32_t *count_down_value),(request, count_down_value)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_updater_show_ui_response_set_return_code,(ma_updater_show_ui_response_t *response, ma_updater_ui_return_code_t return_code),(response, return_code)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_updater_show_ui_response_set_postpone_timeout,(ma_updater_show_ui_response_t *response, ma_uint32_t postpone_timeout),(response, postpone_timeout)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_updater_show_ui_response_set_reboot_timeout,(ma_updater_show_ui_response_t *response, ma_uint32_t reboot_timeout),(response, reboot_timeout))     

static const char *ma_updater_ui_provider_function_names[] = {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) #function,
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
};

typedef struct ma_updater_ui_provider_functions_s {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) ret_type (*function##_fn) signature;
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
}ma_updater_ui_provider_functions_t ;

static ma_updater_ui_provider_functions_t* ma_updater_ui_provider_functions = NULL;

#define ma_updater_ui_provider_functions_size (sizeof(ma_updater_ui_provider_function_names)/sizeof(ma_updater_ui_provider_function_names[0]))

#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) \
    MA_DISTPATCHER_ARG_STUB_CREATOR(&ma_updater_ui_provider_version, MA_DISPATCHER_PRODUCT_NAME, MA_DISPATCHER_LIBRARY_NAME, MA_DISPATCHER_LIBRARY_VERSION,MA_DISPATCHER_MODULE,ret_type, function, signature, arg, ma_updater_ui_provider_function_names, &ma_updater_ui_provider_functions, ma_updater_ui_provider_functions_size)
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER 
#undef MA_DISPATCHER_ARG_EXPANDER

#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER
MA_CPP(})

#endif /* MA_UPDATER_UI_PROVIDER_DISPATCHER_H_INCLUDED */
#endif /*MA_USE_DISPATCHER*/
