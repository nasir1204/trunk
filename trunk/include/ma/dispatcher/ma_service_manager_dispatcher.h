#if  !defined(MA_NO_DISPATCHER) && !defined(MA_NO_SERVICE_MANAGER_DISPATCHER)
#ifndef MA_SERVICE_MANAGER_DISPATCHER_H_INCLUDED
#define MA_SERVICE_MANAGER_DISPATCHER_H_INCLUDED

#include <time.h>
#include "ma/dispatcher/ma_dispatcher_macros.h"

#undef MA_DISPATCHER_PRODUCT_NAME
#undef MA_DISPATCHER_LIBRARY_NAME
#undef MA_DISPATCHER_MODULE
#undef MA_DISPATCHER_LIBRARY_VERSION

#define MA_DISPATCHER_PRODUCT_NAME      "EPOAGENT3000"
#define MA_DISPATCHER_LIBRARY_NAME      MA_DEFINE_DISPATCHER_LIB(ma_service_manager_client)
#define MA_DISPATCHER_MODULE            "ma_service_manager_client"
#define MA_DISPATCHER_LIBRARY_VERSION   0x05020100

static unsigned int ma_service_manager_client_lib_version;

MA_CPP(extern "C" {)
#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#define MA_DISPATCHER_ACTUAL_ARG_EXPANDER\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_service_manager_create, (ma_msgbus_t *msgbus, ma_service_manager_t **service_manager), (msgbus, service_manager))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_service_manager_set_logger, (ma_service_manager_t *service_manager, ma_logger_t *logger), (service_manager, logger))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_service_manager_register_subscriber_notification, (ma_service_manager_t *service_manager, const char *topic_filter, ma_service_manager_on_subscribe_cb_t cb, void *cb_data), (service_manager, topic_filter, cb, cb_data))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_service_manager_unregister_subscriber_notification, (ma_service_manager_t *service_manager, const char *topic_filter), (service_manager, topic_filter))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_service_manager_get_subscribers, (ma_service_manager_t *service_manager, const char *topic_filter, ma_service_manager_subscriber_list_t **subscribers_list, size_t *count), (service_manager, topic_filter, subscribers_list, count))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_service_manager_get_subscribers_async, (ma_service_manager_t *service_manager, const char *topic_filter, ma_service_manager_get_subscribes_cb_t cb, void *cb_data), (service_manager, topic_filter, cb, cb_data))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_service_manager_release, (ma_service_manager_t *service_manager), (service_manager))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_service_manager_subscriber_list_release, (ma_service_manager_subscriber_list_t *subscriber_list), (subscriber_list))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_service_manager_subscriber_list_get_topic_name, (ma_service_manager_subscriber_list_t *subscriber_list, ma_uint32_t index, const char **topic), (subscriber_list,  index, topic))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_service_manager_subscriber_list_get_product_id, (ma_service_manager_subscriber_list_t *subscriber_list, ma_uint32_t index, const char **product_id), (subscriber_list, index, product_id))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_service_manager_get_services, (ma_service_manager_t *service_manager, const char *service_name_filter, ma_service_manager_service_list_t **service_list, size_t *count), (service_manager, service_name_filter, service_list, count))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_service_manager_service_list_release, (ma_service_manager_service_list_t *service_list), (service_list))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_service_manager_service_list_get_service_name, (ma_service_manager_service_list_t *service_list, ma_uint32_t index, const char **service_name), (service_list,  index, service_name))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_service_manager_service_list_get_product_id, (ma_service_manager_service_list_t *service_list, ma_uint32_t index, const char **product_id), (service_list, index, product_id))

static const char *ma_service_manager_client_function_names[] = {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) #function,
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
};

typedef struct ma_service_manager_client_functions_s {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) ret_type (*function##_fn) signature;
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
}ma_service_manager_client_functions_t;
static ma_service_manager_client_functions_t *ma_service_manager_client_functions;

#define ma_service_manager_client_functions_size (sizeof(ma_service_manager_client_function_names)/sizeof(ma_service_manager_client_function_names[0]))

#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) \
    MA_DISTPATCHER_ARG_STUB_CREATOR(&ma_service_manager_client_lib_version, MA_DISPATCHER_PRODUCT_NAME, MA_DISPATCHER_LIBRARY_NAME, MA_DISPATCHER_LIBRARY_VERSION,MA_DISPATCHER_MODULE,ret_type, function, signature, arg, ma_service_manager_client_function_names, &ma_service_manager_client_functions, ma_service_manager_client_functions_size)
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER 
#undef MA_DISPATCHER_ARG_EXPANDER

#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER
MA_CPP(})
#endif /* MA_SERVICE_MANAGER_DISPATCHER_H_INCLUDED */
#endif /* MA_USE_DISPATCHER */

