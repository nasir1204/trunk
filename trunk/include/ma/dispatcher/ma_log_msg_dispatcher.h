#if  !defined(MA_NO_DISPATCHER) && !defined(MA_NO_LOG_MSG_DISPATCHER)
#ifndef MA_NO_LOG_MSG_DISPATCHER_H_INCLUDED
#define MA_NO_LOG_MSG_DISPATCHER_H_INCLUDED

#include "ma/dispatcher/ma_dispatcher_macros.h"
#include "ma/logger/ma_log_msg.h"

#undef MA_DISPATCHER_PRODUCT_NAME
#undef MA_DISPATCHER_LIBRARY_NAME
#undef MA_DISPATCHER_MODULE
#undef MA_DISPATCHER_LIBRARY_VERSION

#define MA_DISPATCHER_PRODUCT_NAME      "EPOAGENT3000"
#define MA_DISPATCHER_LIBRARY_NAME      MA_DEFINE_DISPATCHER_LIB(ma_logger)
#define MA_DISPATCHER_MODULE            "ma_logger"
#define MA_DISPATCHER_LIBRARY_VERSION   0x05020100 

static unsigned int ma_log_msg_lib_version;

/*
MA_LOGGER_API ma_error_t ma_log_msg_get_severity(ma_log_msg_t *log_msg, ma_log_severity_t *severity);

MA_LOGGER_API ma_error_t ma_log_msg_get_severity_str(ma_log_msg_t *log_msg, ma_buffer_t **severity_label);

MA_LOGGER_API ma_error_t ma_log_msg_get_facility(ma_log_msg_t *log_msg, ma_buffer_t **facility);

MA_LOGGER_API ma_error_t ma_log_msg_is_wchar_message(ma_log_msg_t *log_msg, ma_bool_t *is_wide_char);
    
MA_LOGGER_API ma_error_t ma_log_msg_get_wchar_message(ma_log_msg_t *log_msg, ma_wbuffer_t **message);

MA_LOGGER_API ma_error_t ma_log_msg_get_message(ma_log_msg_t *log_msg, ma_buffer_t **message);
*/

MA_CPP(extern "C" {)
#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#define MA_DISPATCHER_ACTUAL_ARG_EXPANDER\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_log_msg_get_severity,(ma_log_msg_t *log_msg,ma_log_severity_t *severity),(log_msg,severity))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_log_msg_get_severity_str,(ma_log_msg_t *log_msg,ma_buffer_t **severity_label),(log_msg,severity_label))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_log_msg_get_facility,(ma_log_msg_t *log_msg,ma_buffer_t **facility),(log_msg,facility))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_log_msg_is_wchar_message,(ma_log_msg_t *log_msg,ma_bool_t *is_wide_char),(log_msg,is_wide_char))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_log_msg_get_wchar_message,(ma_log_msg_t *log_msg,ma_wbuffer_t **message),(log_msg,message))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_log_msg_get_message,(ma_log_msg_t *log_msg,ma_buffer_t **message),(log_msg,message))


static const char *ma_log_msg_function_names[] = {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) #function,
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
};

typedef struct ma_log_msg_functions_s {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) ret_type (*function##_fn) signature;
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
}ma_log_msg_functions_t;
static ma_log_msg_functions_t *ma_log_msg_functions;

#define ma_log_msg_functions_size (sizeof(ma_log_msg_function_names)/sizeof(ma_log_msg_function_names[0]))

#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) \
    MA_DISTPATCHER_ARG_STUB_CREATOR(&ma_log_msg_lib_version, MA_DISPATCHER_PRODUCT_NAME, MA_DISPATCHER_LIBRARY_NAME, MA_DISPATCHER_LIBRARY_VERSION,MA_DISPATCHER_MODULE,ret_type, function, signature, arg, ma_log_msg_function_names, &ma_log_msg_functions, ma_log_msg_functions_size)
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER 
#undef MA_DISPATCHER_ARG_EXPANDER

#undef MA_DISPATCHER_ARG_EXPANDER
MA_CPP(})
#endif /* MA_NO_LOG_MSG_DISPATCHER_INCLUDED */
#endif /* #if  !defined(MA_NO_DISPATCHER) && !defined(MA_NO_LOGGER_DISPATCHER)*/

