#if  !defined(MA_NO_DISPATCHER) && !defined(MA_NO_SCHEDULER_DISPATCHER)
#ifndef MA_TRIGGER_LIST_DISPATCHER_H_INCLUDED
#define MA_TRIGGER_LIST_DISPATCHER_H_INCLUDED

#include "ma/dispatcher/ma_dispatcher_macros.h"

#undef MA_DISPATCHER_PRODUCT_NAME
#undef MA_DISPATCHER_LIBRARY_NAME
#undef MA_DISPATCHER_MODULE
#undef MA_DISPATCHER_LIBRARY_VERSION

#define MA_DISPATCHER_PRODUCT_NAME		"EPOAGENT3000"
#define MA_DISPATCHER_LIBRARY_NAME		 MA_DEFINE_DISPATCHER_LIB(ma_client)
#define MA_DISPATCHER_MODULE			 "ma_scheduler"
#define MA_DISPATCHER_LIBRARY_VERSION	 0x05020100

static unsigned int scheduler_trigger_list_version;

MA_CPP(extern "C" {)
#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#define MA_DISPATCHER_ACTUAL_ARG_EXPANDER\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_trigger_list_create,(ma_trigger_list_t **tl),(tl))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_trigger_list_add,(ma_trigger_list_t *tl, ma_trigger_t *trigger),(tl, trigger))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_trigger_list_remove,(ma_trigger_list_t *tl, const char *id),(tl, id))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_trigger_list_find,(ma_trigger_list_t *tl, const char *id, ma_trigger_t **trigger),(tl, id, trigger))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_trigger_list_release,(ma_trigger_list_t *tl),(tl))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_trigger_list_size,(ma_trigger_list_t *tl, size_t *size),(tl, size))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_trigger_list_at,(ma_trigger_list_t *tl, size_t index, ma_trigger_t **trigger),(tl, index, trigger))



static const char *ma_trigger_list_function_names[] = {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) #function,
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
};

typedef struct ma_trigger_list_functions_s {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) ret_type (*function##_fn) signature;
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
}ma_trigger_list_functions_t;
static ma_trigger_list_functions_t *ma_trigger_list_functions;

#define ma_trigger_list_function_table_size (sizeof(ma_trigger_list_function_names)/sizeof(ma_trigger_list_function_names[0]))

#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) \
    MA_DISTPATCHER_ARG_STUB_CREATOR(&scheduler_trigger_list_version, MA_DISPATCHER_PRODUCT_NAME, MA_DISPATCHER_LIBRARY_NAME, MA_DISPATCHER_LIBRARY_VERSION,MA_DISPATCHER_MODULE,ret_type, function, signature, arg, ma_trigger_list_function_names, &ma_trigger_list_functions, ma_trigger_list_function_table_size)
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER 
#undef MA_DISPATCHER_ARG_EXPANDER

#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER 
MA_CPP(})
#endif
#endif //MA_USE_DISPATCHER
