#if  !defined(MA_NO_DISPATCHER) && !defined(MA_NO_LOGGER_DISPATCHER)
#ifndef MA_FILE_LOGGER_DISPATCHER_H_INCLUDED
#define MA_FILE_LOGGER_DISPATCHER_H_INCLUDED

#include "ma/dispatcher/ma_dispatcher_macros.h"

#undef MA_DISPATCHER_PRODUCT_NAME
#undef MA_DISPATCHER_LIBRARY_NAME
#undef MA_DISPATCHER_MODULE
#undef MA_DISPATCHER_LIBRARY_VERSION

#define MA_DISPATCHER_PRODUCT_NAME      "EPOAGENT3000"
#define MA_DISPATCHER_LIBRARY_NAME      MA_DEFINE_DISPATCHER_LIB(ma_logger)
#define MA_DISPATCHER_MODULE            "ma_logger"
#define MA_DISPATCHER_LIBRARY_VERSION   0x05020100 

static unsigned int ma_file_logger_lib_version;

MA_CPP(extern "C" {)
#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#define MA_DISPATCHER_ACTUAL_ARG_EXPANDER\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_file_logger_create,(const char *log_base_dir , const char *log_file_name  , const char *log_file_ext ,  ma_file_logger_t **logger),(log_base_dir,log_file_name,log_file_ext,logger))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_file_logger_release,(ma_file_logger_t *logger),(logger))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_file_logger_set_policy,(ma_file_logger_t *logger, ma_file_logger_policy_t *policy),(logger, policy))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_file_logger_get_policy,(ma_file_logger_t *logger, const ma_file_logger_policy_t **policy),(logger, policy))
    
static const char *ma_file_logger_function_names[] = {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) #function,
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
};

typedef struct ma_file_logger_functions_s {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) ret_type (*function##_fn) signature;
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
}ma_file_logger_functions_t;
static ma_file_logger_functions_t *ma_file_logger_functions;

#define ma_file_logger_functions_size (sizeof(ma_file_logger_function_names)/sizeof(ma_file_logger_function_names[0]))

#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) \
    MA_DISTPATCHER_ARG_STUB_CREATOR(&ma_file_logger_lib_version, MA_DISPATCHER_PRODUCT_NAME, MA_DISPATCHER_LIBRARY_NAME, MA_DISPATCHER_LIBRARY_VERSION,MA_DISPATCHER_MODULE,ret_type, function, signature, arg, ma_file_logger_function_names, &ma_file_logger_functions, ma_file_logger_functions_size)
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER 
#undef MA_DISPATCHER_ARG_EXPANDER

#undef MA_DISPATCHER_ARG_EXPANDER
MA_CPP(})
#endif /* MA_FILE_LOGGER_DISPATCHER_H_INCLUDED */
#endif /* #if  !defined(MA_NO_DISPATCHER) && !defined(MA_NO_LOGGER_DISPATCHER)*/

