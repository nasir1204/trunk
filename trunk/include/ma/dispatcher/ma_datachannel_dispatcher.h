#if !defined (MA_NO_DISPATCHER) && !defined (MA_NO_DC_DISPATCHER)
#ifndef MA_DATACHANNEL_DISPATCHER_H_INCLUDED
#define MA_DATACHANNEL_DISPATCHER_H_INCLUDED

#include "ma/dispatcher/ma_dispatcher_macros.h"

#undef MA_DISPATCHER_PRODUCT_NAME
#undef MA_DISPATCHER_LIBRARY_NAME
#undef MA_DISPATCHER_MODULE
#undef MA_DISPATCHER_LIBRARY_VERSION

#define MA_DISPATCHER_PRODUCT_NAME		 "EPOAGENT3000"
#define MA_DISPATCHER_LIBRARY_NAME		 MA_DEFINE_DISPATCHER_LIB(ma_client)
#define MA_DISPATCHER_MODULE			 "ma_datachannel_client"
#define MA_DISPATCHER_LIBRARY_VERSION	 0x05020100

static unsigned int ma_datachannel_lib_version;

/*
ma_error_t ma_datachannel_register_notification_callback(ma_client_t *ma_client, const char *product_id, ma_datachannel_notification_cb_t notify_cb, void *cb_data);
ma_error_t ma_datachannel_unregister_notification_callback(ma_client_t *ma_client, const char *product_id);
ma_error_t ma_datachannel_register_message_handler_callback(ma_client_t *ma_client, const char *product_id, ma_datachannel_on_message_cb_t on_message_cb,void *cb_data);
ma_error_t ma_datachannel_unregister_message_handler_,callback(ma_client_t *ma_client, const char *product_id);

ma_error_t ma_datachannel_subscribe(ma_client_t *ma_client, const char *product_id, const char *dc_message_id);
ma_error_t ma_datachannel_unsubscribe(ma_client_t *ma_client, const char *product_id, const char *dc_message_id);
ma_error_t ma_datachannel_async_send(ma_client_t *ma_client, const char *product_id, ma_datachannel_item_t *dc_message);

ma_error_t ma_datachannel_item_create(const char *message_type, ma_variant_t *payload, ma_datachannel_item_t **datachannel_message);
ma_error_t ma_datachannel_item_release(ma_datachannel_item_t *dc_item);
ma_error_t ma_datachannel_item_get_message_type(ma_datachannel_item_t *dc_item, ma_buffer_t **buffer);
ma_error_t ma_datachannel_item_get_origin(ma_datachannel_item_t *dc_item, ma_buffer_t **buffer);
ma_error_t ma_datachannel_item_get_payload(ma_datachannel_item_t *dc_item, ma_variant_t **payload);
ma_error_t ma_datachannel_item_get_ttl(ma_datachannel_item_t *dc_item, ma_int64_t *ttl_in_seconds);
ma_error_t ma_datachannel_item_get_correlation_id(ma_datachannel_item_t *dc_item, ma_int64_t *correlation_id);
ma_error_t ma_datachannel_item_set_origin(ma_datachannel_item_t *dc_item, const char *origin);
ma_error_t ma_datachannel_item_set_payload(ma_datachannel_item_t *dc_item, ma_variant_t *payload);
ma_error_t ma_datachannel_item_set_ttl(ma_datachannel_item_t *dc_item, ma_int64_t ttl_in_seconds);
ma_error_t ma_datachannel_item_set_correlation_id(ma_datachannel_item_t *dc_item, ma_int64_t correlation_id);
ma_error_t ma_datachannel_item_set_option(ma_datachannel_item_t *dc_item, ma_uint32_t option, ma_bool_t b_enable);
ma_error_t ma_datachannel_item_get_option(ma_datachannel_item_t *dc_item, ma_uint32_t *option);
*/


MA_CPP(extern "C" {)
#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#define MA_DISPATCHER_ACTUAL_ARG_EXPANDER\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_datachannel_register_notification_callback,(ma_client_t *ma_client, const char *product_id, ma_datachannel_notification_cb_t notify_cb, void *cb_data),(ma_client, product_id, notify_cb, cb_data))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_datachannel_unregister_notification_callback,(ma_client_t *ma_client, const char *product_id),(ma_client, product_id))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_datachannel_register_message_handler_callback,(ma_client_t *ma_client, const char *product_id, ma_datachannel_on_message_cb_t on_message_cb,void *cb_data),(ma_client, product_id, on_message_cb, cb_data))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_datachannel_unregister_message_handler_callback,(ma_client_t *ma_client, const char *product_id),(ma_client, product_id))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_datachannel_subscribe,(ma_client_t *ma_client, const char *product_id, const char *dc_message_id,ma_datachannel_subscription_type_t subcription_type),(ma_client, product_id, dc_message_id, subcription_type))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_datachannel_unsubscribe,(ma_client_t *ma_client, const char *product_id, const char *dc_message_id), (ma_client, product_id, dc_message_id))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_datachannel_async_send,(ma_client_t *ma_client, const char *product_id, ma_datachannel_item_t *dc_message), (ma_client, product_id, dc_message))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_datachannel_item_create,(const char *message_type, ma_variant_t *payload, ma_datachannel_item_t **datachannel_message),(message_type,payload,datachannel_message))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_datachannel_item_release,(ma_datachannel_item_t *dc_item),(dc_item))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_datachannel_item_get_message_type,(ma_datachannel_item_t *dc_item, ma_buffer_t **buffer),(dc_item,buffer))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_datachannel_item_get_origin,(ma_datachannel_item_t *dc_item, ma_buffer_t **buffer),(dc_item,buffer))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_datachannel_item_get_payload,(ma_datachannel_item_t *dc_item, ma_variant_t **payload),(dc_item,payload))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_datachannel_item_get_ttl,(ma_datachannel_item_t *dc_item, ma_int64_t *ttl_in_seconds),(dc_item,ttl_in_seconds))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_datachannel_item_get_correlation_id,(ma_datachannel_item_t *dc_item, ma_int64_t *correlation_id),(dc_item,correlation_id))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_datachannel_item_set_origin,(ma_datachannel_item_t *dc_item, const char *origin),(dc_item,origin))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_datachannel_item_set_payload,(ma_datachannel_item_t *dc_item, ma_variant_t *payload),(dc_item,payload))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_datachannel_item_set_ttl,(ma_datachannel_item_t *dc_item, ma_int64_t ttl_in_seconds),(dc_item,ttl_in_seconds))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_datachannel_item_set_correlation_id,(ma_datachannel_item_t *dc_item, ma_int64_t correlation_id),(dc_item,correlation_id))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_datachannel_item_set_option,(ma_datachannel_item_t *dc_item, ma_uint32_t option, ma_bool_t b_enable), (dc_item, option, b_enable))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_datachannel_item_get_option,(ma_datachannel_item_t *dc_item, ma_uint32_t *option),(dc_item, option))

static const char *ma_datachannel_function_names[] = {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) #function,
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
};


typedef struct ma_datachannel_functions_s {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) ret_type (*function##_fn) signature;
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
}ma_datachannel_functions_t;
static ma_datachannel_functions_t* ma_datachannel_functions = NULL;


#define ma_datachannel_function_size (sizeof(ma_datachannel_function_names)/sizeof(ma_datachannel_function_names[0]))

#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) \
    MA_DISTPATCHER_ARG_STUB_CREATOR(&ma_datachannel_lib_version, MA_DISPATCHER_PRODUCT_NAME, MA_DISPATCHER_LIBRARY_NAME, MA_DISPATCHER_LIBRARY_VERSION,MA_DISPATCHER_MODULE,ret_type, function, signature, arg, ma_datachannel_function_names, &ma_datachannel_functions, ma_datachannel_function_size)
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER

#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER
MA_CPP(})
#endif
#endif
