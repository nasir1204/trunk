#if  !defined(MA_NO_DISPATCHER) && !defined(MA_NO_REPOSITORY_DISPATCHER)
#ifndef MA_REPOSITORY_DISPATCHER_H_INCLUDED
#define MA_REPOSITORY_DISPATCHER_H_INCLUDED

#include "ma/dispatcher/ma_dispatcher_macros.h"

#undef MA_DISPATCHER_PRODUCT_NAME
#undef MA_DISPATCHER_LIBRARY_NAME
#undef MA_DISPATCHER_MODULE
#undef MA_DISPATCHER_LIBRARY_VERSION

#define MA_DISPATCHER_PRODUCT_NAME		 "EPOAGENT3000"
#define MA_DISPATCHER_LIBRARY_NAME		 MA_DEFINE_DISPATCHER_LIB(ma_client)
#define MA_DISPATCHER_MODULE			 "repository"
#define MA_DISPATCHER_LIBRARY_VERSION	 0x05020100

static unsigned int ma_repository_lib_version;

MA_CPP(extern "C" {)
#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#define MA_DISPATCHER_ACTUAL_ARG_EXPANDER\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_client_get_repositories,(ma_client_t *ma_client, ma_repository_list_t **repository_list), (ma_client, repository_list)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_client_set_repositories,(ma_client_t *ma_client, ma_repository_list_t *repository_list), (ma_client, repository_list)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_client_get_proxy_config,(ma_client_t *ma_client, ma_proxy_config_t **proxy_config), (ma_client, proxy_config)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_client_set_proxy_config,(ma_client_t *ma_client, ma_proxy_config_t *proxy_config), (ma_client, proxy_config)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_client_async_get_repositories,(ma_client_t *ma_client, ma_repositories_get_cb_t cb, void *cb_data), (ma_client, cb, cb_data)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_client_async_get_proxy_config,(ma_client_t *ma_client, ma_proxy_config_get_cb_t cb, void *cb_data), (ma_client, cb, cb_data)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_client_async_get_system_proxy,(ma_client_t *ma_client, const char *url, ma_system_proxy_get_cb_t cb, void *cb_data), (ma_client, url, cb, cb_data)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_client_get_system_proxy,(ma_client_t *ma_client, const char *url, ma_proxy_list_t **sys_proxy_list), (ma_client, url, sys_proxy_list)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_client_import_repositories, (ma_client_t *ma_client, ma_buffer_t *repo_buffer), (ma_client, repo_buffer)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_create,(ma_repository_t **repository), (repository)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_release,(ma_repository_t *repository), (repository)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_validate,(ma_repository_t *repository), (repository)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_add_ref,(ma_repository_t *repository), (repository)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_set_type,(ma_repository_t *repository, ma_repository_type_t repository_type), (repository, repository_type)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_set_state, (ma_repository_t *repository, ma_repository_state_t repository_type), (repository, repository_type)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_set_url_type, (ma_repository_t *repository, ma_repository_url_type_t repository_type), (repository, repository_type)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_set_name, (ma_repository_t *repository, const char *repository_name), (repository, repository_name)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_set_server_fqdn, (ma_repository_t *repository, const char *server_fqdn), (repository, server_fqdn)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_set_server_name, (ma_repository_t *repository, const char *server_name), (repository, server_name)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_set_server_ip, (ma_repository_t *repository, const char *server_ip), (repository, server_ip)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_set_server_path, (ma_repository_t *repository, const char *repo_path), (repository, repo_path)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_set_enabled, (ma_repository_t *repository, ma_bool_t is_enabled), (repository, is_enabled)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_set_authentication, (ma_repository_t *repository, ma_repository_auth_t type), (repository, type)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_set_namespace, (ma_repository_t *repository, ma_repository_namespace_t repo_namespace), (repository, repo_namespace)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_set_proxy_usage_type, (ma_repository_t *repository, ma_proxy_usage_type_t proxy_usage), (repository, proxy_usage)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_set_port, (ma_repository_t *repository, ma_uint32_t port), (repository, port)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_set_ssl_port, (ma_repository_t *repository, ma_uint32_t port), (repository, port)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_set_user_name, (ma_repository_t *repository, const char *user_name), (repository, user_name)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_set_domain_name, (ma_repository_t *repository, const char *domain_name), (repository, domain_name)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_set_password, (ma_repository_t *repository, const char *password), (repository, password)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_set_pingtime, (ma_repository_t *repository, ma_uint32_t pingtime), (repository, pingtime)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_set_subnetdistance, (ma_repository_t *repository, ma_uint32_t subnetdistance), (repository, subnetdistance)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_set_siteorder, (ma_repository_t *repository, ma_uint32_t siteorder), (repository, siteorder)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_get_name, (ma_repository_t *repository, const char **repository_name), (repository, repository_name)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_get_type, (ma_repository_t *repository, ma_repository_type_t *repository_type), (repository, repository_type)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_get_url_type, (ma_repository_t *repository, ma_repository_url_type_t *url_type), (repository, url_type)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_get_namespace, (ma_repository_t *repository, ma_repository_namespace_t *repo_namespace), (repository, repo_namespace)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_get_authentication, (ma_repository_t *repository, ma_repository_auth_t *type), (repository, type)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_get_proxy_usage_type, (ma_repository_t *repository, ma_proxy_usage_type_t *proxy_usage), (repository, proxy_usage)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_get_server_fqdn, (ma_repository_t *repository, const char **server_fqdn), (repository, server_fqdn)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_get_server_name, (ma_repository_t *repository, const char **server_name), (repository, server_name)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_get_server_path, (ma_repository_t *repository, const char **server_path), (repository, server_path)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_get_port, (ma_repository_t *repository, ma_uint32_t *port), (repository, port)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_get_ssl_port, (ma_repository_t *repository, ma_uint32_t *port), (repository, port)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_get_enabled, (ma_repository_t *repository, ma_bool_t *is_enabled), (repository, is_enabled)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_get_user_name, (ma_repository_t *repository, const char **user_name), (repository, user_name)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_get_domain_name, (ma_repository_t *repository, const char **domain_name), (repository, domain_name)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_get_password, (ma_repository_t *repository, const char **password), (repository, password)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_get_pingtime, (ma_repository_t *repository, ma_uint32_t *pingtime), (repository, pingtime)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_get_subnetdistance, (ma_repository_t *repository, ma_uint32_t *subnetdistance), (repository, subnetdistance)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_get_siteorder, (ma_repository_t *repository, ma_uint32_t *siteorder), (repository, siteorder)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_excusion_check, (char *repo_name, ma_proxy_config_t *configuration, ma_bool_t *is_excuded), (repo_name, configuration, is_excuded)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_get_server_ip, (ma_repository_t *repository, const char **server_ip), (repository, server_ip)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_get_state, (ma_repository_t *repository, ma_repository_state_t *state), (repository, state)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_list_from_variant, (ma_variant_t *data, ma_repository_list_t **repository_list), (data, repository_list)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_list_as_variant, (ma_repository_list_t *repository_list, ma_variant_t **variant), (repository_list, variant)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_as_variant, (ma_repository_t *repository, ma_variant_t **repo_variant), (repository, repo_variant)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_from_variant, (ma_variant_t *repo_variant, ma_repository_t **repository), (repo_variant, repository)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_list_create, (ma_repository_list_t **repository_list), (repository_list)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_list_release, (ma_repository_list_t *repository_list), (repository_list)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_list_add_repository, (ma_repository_list_t *repository_list, ma_repository_t *repository), (repository_list, repository)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_list_remove_repository, (ma_repository_list_t *repository_list, ma_repository_t *repository), (repository_list, repository)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_list_get_repositories_count, (ma_repository_list_t *repository_list, size_t *repositories_count), (repository_list, repositories_count)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_list_get_repository_by_index, (ma_repository_list_t *repository_list, ma_int32_t index, ma_repository_t **repository), (repository_list, index, repository)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_repository_list_get_repository_by_name, (ma_repository_list_t *repository_list, const char *name, ma_repository_t **repository), (repository_list, name, repository)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_proxy_create, (ma_proxy_t **proxy), (proxy)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_proxy_release, (ma_proxy_t *proxy), (proxy)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_proxy_set_address, (ma_proxy_t *proxy, const char *address), (proxy, address)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_proxy_get_address, (ma_proxy_t *proxy, const char **address), (proxy, address)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_proxy_set_port, (ma_proxy_t *proxy, ma_int32_t port), (proxy, port)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_proxy_get_port, (ma_proxy_t *proxy, ma_int32_t *port), (proxy, port)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_proxy_set_scopeid, (ma_proxy_t *proxy, ma_uint32_t scopeid), (proxy, scopeid)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_proxy_get_scopeid, (ma_proxy_t *proxy, ma_uint32_t *scopeid), (proxy, scopeid)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_proxy_set_protocol_type, (ma_proxy_t *proxy, ma_proxy_protocol_type_t type), (proxy, type)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_proxy_get_protocol_type, (ma_proxy_t *proxy, ma_proxy_protocol_type_t *type), (proxy, type)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_proxy_set_authentication, (ma_proxy_t *proxy, ma_bool_t auth_req, const char *user_name, const char *password), (proxy, auth_req, user_name, password)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_proxy_get_authentication, (ma_proxy_t *proxy, ma_bool_t *auth_req, const char **user_name, const char **password), (proxy, auth_req, user_name, password)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_proxy_set_flag, (ma_proxy_t *proxy, ma_int32_t flag), (proxy, flag)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_proxy_get_flag, (ma_proxy_t *proxy, ma_int32_t *flag), (proxy, flag)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_proxy_list_create, (ma_proxy_list_t **list), (list)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_proxy_list_add_proxy, (ma_proxy_list_t *list, ma_proxy_t *proxy), (list, proxy)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_proxy_list_size, (ma_proxy_list_t *list, size_t *size), (list, size)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_proxy_list_get_proxy, (ma_proxy_list_t *list, ma_int32_t index, ma_proxy_t **proxy), (list, index, proxy)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_proxy_list_release, (ma_proxy_list_t *list), (list)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_proxy_config_create, (ma_proxy_config_t **configuration), (configuration)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_proxy_config_release, (ma_proxy_config_t *configuration), (configuration)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_proxy_config_add_ref, (ma_proxy_config_t *configuration), (configuration)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_proxy_config_set_proxy_usage_type, (ma_proxy_config_t *configuration, ma_proxy_usage_type_t type), (configuration, type)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_proxy_config_get_proxy_usage_type, (ma_proxy_config_t *configuration, ma_proxy_usage_type_t *type), (configuration, type)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_proxy_config_set_system_proxy_authentication, (ma_proxy_config_t *configuration, ma_proxy_protocol_type_t protocol_type, ma_bool_t auth_req, const char *user_name, const char *password), (configuration, protocol_type, auth_req, user_name, password)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_proxy_config_get_system_proxy_authentication, (ma_proxy_config_t *configuration, ma_proxy_protocol_type_t protocol_type, ma_bool_t *auth_req, const char **user_name, const char **password), (configuration, protocol_type, auth_req, user_name, password)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_proxy_config_get_system_proxy, (ma_proxy_config_t *configuration, const char *url, ma_proxy_list_t **list), (configuration, url, list)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_proxy_config_set_allow_local_proxy_configuration, (ma_proxy_config_t *configuration, ma_bool_t allow_or_not), (configuration, allow_or_not)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_proxy_config_get_allow_local_proxy_configuration, (ma_proxy_config_t *configuration, ma_bool_t *allow_or_not), (configuration, allow_or_not)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_proxy_config_set_proxy_list, (ma_proxy_config_t *configuration, ma_proxy_list_t *list), (configuration, list)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_proxy_config_get_proxy_list, (ma_proxy_config_t *configuration, ma_proxy_list_t **list), (configuration, list)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_proxy_config_set_bypass_local, (ma_proxy_config_t *configuration, ma_bool_t bypass_local), (configuration, bypass_local)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_proxy_config_get_bypass_local, (ma_proxy_config_t *configuration, ma_bool_t *bypass_local), (configuration, bypass_local)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_proxy_config_set_exclusion_urls, (ma_proxy_config_t *configuration, const char *bypass_exclusion_str), (configuration, bypass_exclusion_str)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_proxy_list_add_ref, (ma_proxy_list_t *list), (list)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_proxy_list_set_last_proxy_index, (ma_proxy_list_t *list, ma_int32_t index), (list, index)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_proxy_list_get_last_proxy_index, (ma_proxy_list_t *list, ma_int32_t *index), (list, index)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_proxy_config_get_exclusion_urls, (ma_proxy_config_t *configuration, const char **bypass_exclusion_str), (configuration, bypass_exclusion_str))

static const char *ma_repository_function_names[] = {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) #function,
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
};

typedef struct ma_repository_functions_s {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) ret_type (*function##_fn) signature;
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
}ma_repository_functions_t;
static ma_repository_functions_t* ma_repository_functions = NULL;

#define ma_repository_functions_size (sizeof(ma_repository_function_names)/sizeof(ma_repository_function_names[0]))

#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) \
    MA_DISTPATCHER_ARG_STUB_CREATOR(&ma_repository_lib_version, MA_DISPATCHER_PRODUCT_NAME, MA_DISPATCHER_LIBRARY_NAME, MA_DISPATCHER_LIBRARY_VERSION,MA_DISPATCHER_MODULE,ret_type, function, signature, arg, ma_repository_function_names, &ma_repository_functions, ma_repository_functions_size)
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER

#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER
MA_CPP(})

#endif /* MA_REPOSITORY_DISPATCHER_H_INCLUDED */
#endif /*MA_USE_DISPATCHER*/
