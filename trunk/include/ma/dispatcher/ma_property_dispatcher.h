#if  !defined(MA_NO_DISPATCHER) && !defined(MA_NO_PROPERTY_DISPATCHER)
#ifndef MA_PROPERTY_DISPATCHER_H_INCLUDED
#define MA_PROPERTY_DISPATCHER_H_INCLUDED

#include "ma/dispatcher/ma_dispatcher_macros.h"

#undef MA_DISPATCHER_PRODUCT_NAME
#undef MA_DISPATCHER_LIBRARY_NAME
#undef MA_DISPATCHER_MODULE
#undef MA_DISPATCHER_LIBRARY_VERSION

#define MA_DISPATCHER_PRODUCT_NAME		 "EPOAGENT3000"
#define MA_DISPATCHER_LIBRARY_NAME		 MA_DEFINE_DISPATCHER_LIB(ma_client)
#define MA_DISPATCHER_MODULE			 "property"
#define MA_DISPATCHER_LIBRARY_VERSION	 0x05020100

static unsigned int ma_property_lib_version;


MA_CPP(extern "C" {)
#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#define MA_DISPATCHER_ACTUAL_ARG_EXPANDER\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_property_register_provider_callback,(ma_client_t *ma_client, const char *product_id, ma_property_provider_cb_t property_provider_cb, void *cb_data), (ma_client, product_id, property_provider_cb, cb_data))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_property_unregister_provider_callback,(ma_client_t *ma_client, const char *product_id), (ma_client, product_id)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_property_send,(ma_client_t *ma_client, const char *product_id), (ma_client, product_id)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_property_bag_get_type,(ma_property_bag_t *bag, ma_property_bag_type_t *bag_type), (bag, bag_type)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_property_bag_add_property,(ma_property_bag_t *bag, const char *path, const char *name, ma_variant_t *value), (bag, path, name, value))


static const char *ma_property_function_names[] = {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) #function,
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
};

typedef struct ma_property_functions_s {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) ret_type (*function##_fn) signature;
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
}ma_property_functions_t;
static ma_property_functions_t* ma_property_functions = NULL;

#define ma_property_functions_size (sizeof(ma_property_function_names)/sizeof(ma_property_function_names[0]))

#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) \
    MA_DISTPATCHER_ARG_STUB_CREATOR(&ma_property_lib_version, MA_DISPATCHER_PRODUCT_NAME, MA_DISPATCHER_LIBRARY_NAME, MA_DISPATCHER_LIBRARY_VERSION,MA_DISPATCHER_MODULE,ret_type, function, signature, arg, ma_property_function_names, &ma_property_functions, ma_property_functions_size)
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER

#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER
MA_CPP(})

#endif /* MA_PROPERTY_DISPATCHER_H_INCLUDED */
#endif /* MA_USE_DISPATCHER */

