#if  !defined(MA_NO_DISPATCHER) && !defined(MA_NO_MSGBUS_DISPATCHER)
#ifndef MA_MSGBUS_DISPATCHER_H_INCLUDED
#define MA_MSGBUS_DISPATCHER_H_INCLUDED

#include <time.h>
#include "ma/dispatcher/ma_dispatcher_macros.h"

#undef MA_DISPATCHER_PRODUCT_NAME
#undef MA_DISPATCHER_LIBRARY_NAME
#undef MA_DISPATCHER_MODULE
#undef MA_DISPATCHER_LIBRARY_VERSION

#define MA_DISPATCHER_PRODUCT_NAME      "EPOAGENT3000"
#define MA_DISPATCHER_LIBRARY_NAME      MA_DEFINE_DISPATCHER_LIB(ma_msgbus)
#define MA_DISPATCHER_MODULE            "ma_msgbus"
#define MA_DISPATCHER_LIBRARY_VERSION 0x05020100 

static unsigned int ma_msgbus_lib_version;

MA_CPP(extern "C" {)
#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#define MA_DISPATCHER_ACTUAL_ARG_EXPANDER\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_message_create,(ma_message_t **message),(message))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_message_get_message_type,(ma_message_t *message, ma_message_type_t* msg_type),(message,msg_type))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_msgbus_set_logger,(ma_msgbus_t *msgbus, ma_logger_t *logger),(msgbus, logger))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_msgbus_set_passphrase_provider_callback,(ma_msgbus_t *msgbus, ma_msgbus_passphrase_provider_callback_t cb, void *cb_data),(msgbus,cb,cb_data))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_msgbus_passphrase_digest,(ma_msgbus_t *msgbus, const void *passphrase, size_t passphrase_size, unsigned char passphrase_digest[32]),(msgbus,passphrase,passphrase_size,passphrase_digest))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_msgbus_get_version,(unsigned int *version),(version))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_msgbus_endpoint_create,(ma_msgbus_t *msgbus, const char *server_name, const void *reserved, ma_msgbus_endpoint_t **endpoint),(msgbus, server_name, reserved, endpoint))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_msgbus_endpoint_release,(ma_msgbus_endpoint_t *endpoint),(endpoint))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_msgbus_async_send,(ma_msgbus_endpoint_t *endpoint, ma_message_t *payload, ma_msgbus_request_callback_t cb, void *cb_data, ma_msgbus_request_t **request), (endpoint, payload, cb, cb_data, request))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_msgbus_send,(ma_msgbus_endpoint_t *endpoint, ma_message_t *request, ma_message_t **response),(endpoint, request, response))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_msgbus_send_and_forget,(ma_msgbus_endpoint_t *endpoint, ma_message_t *payload),(endpoint, payload))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_msgbus_request_get_endpoint,(ma_msgbus_request_t *request, ma_msgbus_endpoint_t **endpoint),(request, endpoint))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_msgbus_request_cancel,(ma_msgbus_request_t *request, int flags),(request, flags))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_msgbus_request_release,(ma_msgbus_request_t *request),(request))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_msgbus_server_create,(ma_msgbus_t *msgbus, const char *server_name, ma_msgbus_server_t **server),(msgbus, server_name, server))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_msgbus_server_release,(ma_msgbus_server_t *server),(server))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_msgbus_server_start,(ma_msgbus_server_t *server, ma_msgbus_server_handler handler, void *cb_data),(server, handler, cb_data))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_msgbus_server_stop,(ma_msgbus_server_t *server),(server))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_msgbus_server_client_request_post_result,(ma_msgbus_client_request_t *c_request, ma_error_t status, ma_message_t *response),(c_request, status, response))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_msgbus_client_request_add_ref,(ma_msgbus_client_request_t *c_request),(c_request))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_msgbus_client_request_release,(ma_msgbus_client_request_t *c_request),(c_request))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_msgbus_subscriber_create,(ma_msgbus_t *msgbus, ma_msgbus_subscriber_t **subscriber),(msgbus, subscriber))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_msgbus_subscriber_release,(ma_msgbus_subscriber_t *subscriber),(subscriber))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_msgbus_subscriber_register,(ma_msgbus_subscriber_t *subscriber,  const char *topic_filter, ma_msgbus_subscription_callback_t cb, void *cb_data),(subscriber, topic_filter, cb, cb_data))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_msgbus_subscriber_unregister,(ma_msgbus_subscriber_t *subscriber),(subscriber))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_msgbus_publish,(ma_msgbus_t *msgbus, const char *topic, ma_msgbus_consumer_reachability_t pub_reachability,  ma_message_t *payload),(msgbus, topic, pub_reachability, payload))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_msgbus_endpoint_vsetopt,(ma_msgbus_endpoint_t *endpoint,  ma_msgbus_option_t option, va_list ap), (endpoint, option, ap))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_msgbus_server_vsetopt,(ma_msgbus_server_t *server, ma_msgbus_option_t server_option, va_list ap),(server, server_option, ap))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_msgbus_subscriber_vsetopt,(ma_msgbus_subscriber_t *subscriber, ma_msgbus_option_t sub_options, va_list ap),(subscriber, sub_options, ap))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_message_from_variant,(ma_variant_t* variant, ma_message_t **message),(variant, message))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_message_add_ref,(ma_message_t *message),(message))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_message_release,(ma_message_t *message),(message))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_message_get_timestamp,(const ma_message_t *message, time_t* timestamp),(message,timestamp))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_message_set_property,(ma_message_t *message, const char *key, const char *value),(message,key,value))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_message_get_property,(ma_message_t *message, const char *key, const char **value),(message,key,value))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_message_set_payload,(ma_message_t *message, ma_variant_t *payload),(message,payload))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_message_get_payload,(ma_message_t *message, ma_variant_t **payload),(message,payload))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_message_clone,(ma_message_t *source, ma_message_t **dest),(source,dest))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_message_as_variant,(ma_message_t *message, ma_variant_t **variant),(message,variant))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_message_get_auth_info,(ma_message_t *message, ma_message_auth_info_t **auth_info),(message, auth_info))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_message_auth_info_add_ref,(ma_message_auth_info_t *auth_info),(auth_info))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_message_auth_info_release,(ma_message_auth_info_t *auth_info),(auth_info))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_message_auth_info_vgetopt,(const ma_message_auth_info_t *auth_info, ma_message_auth_info_attribute_t option, va_list ap),(auth_info, option, ap))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_msgbus_release,(ma_msgbus_t *msgbus),(msgbus))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_msgbus_stop,(ma_msgbus_t *msgbus, ma_bool_t wait),(msgbus,wait))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_msgbus_start,(ma_msgbus_t *msgbus),(msgbus))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_msgbus_disable_watcher,(ma_msgbus_t *msgbus),(msgbus))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_msgbus_run,(ma_msgbus_t *msgbus),(msgbus))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_msgbus_create,(const char *product_id, ma_msgbus_t **msgbus), (product_id, msgbus))

static const char *ma_msgbus_function_names[] = {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) #function,
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
};

typedef struct ma_msgbus_functions_s {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) ret_type (*function##_fn) signature;
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
}ma_msgbus_functions_t;
static ma_msgbus_functions_t *ma_msgbus_functions;

#define ma_msgbus_functions_size (sizeof(ma_msgbus_function_names)/sizeof(ma_msgbus_function_names[0]))

#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) \
    MA_DISTPATCHER_ARG_STUB_CREATOR(&ma_msgbus_lib_version, MA_DISPATCHER_PRODUCT_NAME, MA_DISPATCHER_LIBRARY_NAME, MA_DISPATCHER_LIBRARY_VERSION,MA_DISPATCHER_MODULE,ret_type, function, signature, arg, ma_msgbus_function_names, &ma_msgbus_functions, ma_msgbus_functions_size)
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER 
#undef MA_DISPATCHER_ARG_EXPANDER

MA_DISTPATCHER_VARG_STUB_CREATOR(&ma_msgbus_lib_version, MA_DISPATCHER_PRODUCT_NAME, MA_DISPATCHER_LIBRARY_NAME, MA_DISPATCHER_LIBRARY_VERSION,MA_DISPATCHER_MODULE, ma_error_t, ma_msgbus_endpoint_setopt,(ma_msgbus_endpoint_t *endpoint,  ma_msgbus_option_t option, ...), ma_msgbus_endpoint_vsetopt,(ma_msgbus_endpoint_t *endpoint,  ma_msgbus_option_t option, va_list ap),(endpoint, option, ap), option, ma_msgbus_function_names, &ma_msgbus_functions, ma_msgbus_functions_size)
MA_DISTPATCHER_VARG_STUB_CREATOR(&ma_msgbus_lib_version, MA_DISPATCHER_PRODUCT_NAME, MA_DISPATCHER_LIBRARY_NAME, MA_DISPATCHER_LIBRARY_VERSION,MA_DISPATCHER_MODULE, ma_error_t,ma_msgbus_server_setopt,(ma_msgbus_server_t *server,  ma_msgbus_option_t server_option, ...),ma_msgbus_server_vsetopt,(ma_msgbus_server_t *server, ma_msgbus_option_t server_option, va_list ap),(server, server_option, ap), server_option, ma_msgbus_function_names, &ma_msgbus_functions, ma_msgbus_functions_size)
MA_DISTPATCHER_VARG_STUB_CREATOR(&ma_msgbus_lib_version, MA_DISPATCHER_PRODUCT_NAME, MA_DISPATCHER_LIBRARY_NAME, MA_DISPATCHER_LIBRARY_VERSION,MA_DISPATCHER_MODULE, ma_error_t,ma_msgbus_subscriber_setopt,(ma_msgbus_subscriber_t *subscriber,  ma_msgbus_option_t sub_options, ...),ma_msgbus_subscriber_vsetopt,(ma_msgbus_subscriber_t *subscriber, ma_msgbus_option_t sub_options, va_list ap),(subscriber, sub_options, ap), sub_options, ma_msgbus_function_names, &ma_msgbus_functions, ma_msgbus_functions_size)
MA_DISTPATCHER_VARG_STUB_CREATOR(&ma_msgbus_lib_version, MA_DISPATCHER_PRODUCT_NAME, MA_DISPATCHER_LIBRARY_NAME, MA_DISPATCHER_LIBRARY_VERSION,MA_DISPATCHER_MODULE, ma_error_t,ma_message_auth_info_get_attribute,(const ma_message_auth_info_t *auth_info, ma_message_auth_info_attribute_t option, ...),ma_message_auth_info_vgetopt,(ma_message_auth_info_t *auth_info, ma_message_auth_info_attribute_t option, va_list ap),(auth_info, option, ap), option, ma_msgbus_function_names, &ma_msgbus_functions, ma_msgbus_functions_size)


#undef MA_DISPATCHER_ARG_EXPANDER
MA_CPP(})
#endif /* MA_MSGBUS_DISPATCHER_H_INCLUDED */
#endif /* MA_USE_DISPATCHER */

