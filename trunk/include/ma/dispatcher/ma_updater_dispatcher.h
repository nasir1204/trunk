#if  !defined(MA_NO_DISPATCHER) && !defined(MA_NO_UPDATER_DISPATCHER)
#ifndef MA_UPDATER_DISPATCHER_H_INCLUDED
#define MA_UPDATER_DISPATCHER_H_INCLUDED

#include "ma/dispatcher/ma_dispatcher_macros.h"

#undef MA_DISPATCHER_PRODUCT_NAME
#undef MA_DISPATCHER_LIBRARY_NAME
#undef MA_DISPATCHER_MODULE
#undef MA_DISPATCHER_LIBRARY_VERSION

#define MA_DISPATCHER_PRODUCT_NAME		 "EPOAGENT3000"
#define MA_DISPATCHER_LIBRARY_NAME		 MA_DEFINE_DISPATCHER_LIB(ma_client)
#define MA_DISPATCHER_MODULE			 "updater"
#define MA_DISPATCHER_LIBRARY_VERSION	 0x05020100

static unsigned int ma_updater_lib_version;


MA_CPP(extern "C" {)
#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#define MA_DISPATCHER_ACTUAL_ARG_EXPANDER\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_updater_register_callbacks,(ma_client_t *ma_client, const char *product_id, ma_updater_callbacks_t const *callbacks, void *cb_data), (ma_client, product_id, callbacks, cb_data)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_updater_unregister_callbacks,(ma_client_t *ma_client, const char *product_id), (ma_client, product_id)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_updater_start_update,(ma_client_t *ma_client, ma_updater_update_request_t *request, ma_buffer_t **session_id), (ma_client, request, session_id)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_updater_get_update_state,(ma_client_t *ma_client, ma_buffer_t *session_id, ma_update_state_t *state), (ma_client, session_id, state)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_updater_stop_update,(ma_client_t *ma_client, ma_buffer_t *session_id), (ma_client, session_id)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_updater_update_request_create,(ma_updater_update_type_t update_type, ma_updater_update_request_t **request), (update_type, request)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_updater_update_request_release,(ma_updater_update_request_t *request), (request)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_updater_update_request_set_product_list,(ma_updater_update_request_t *request, ma_array_t *product_list), (request, product_list)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_updater_update_request_get_product_list,(ma_updater_update_request_t *request, ma_array_t **product_list), (request, product_list)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_updater_update_request_set_locale,(ma_updater_update_request_t *request, ma_uint32_t locale), (request, locale)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_updater_update_request_get_locale,(ma_updater_update_request_t *request, ma_uint32_t *locale), (request, locale)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_updater_update_request_get_type,(ma_updater_update_request_t *request, ma_updater_update_type_t *update_type), (request, update_type)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_updater_update_request_set_initiator_type,(ma_updater_update_request_t *request, ma_updater_initiator_type_t initiator_type), (request, initiator_type)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_updater_update_request_set_uioption,(ma_updater_update_request_t *request, ma_bool_t ui), (request, ui)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_updater_update_request_get_uioption,(ma_updater_update_request_t *request, ma_bool_t *ui), (request, ui)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_updater_update_request_set_ui_provider_name,(ma_updater_update_request_t *request, const char *name), (request, name)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_updater_update_request_get_ui_provider_name,(ma_updater_update_request_t *request, const char **name), (request, name)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_updater_update_request_get_initiator_type,(ma_updater_update_request_t *request, ma_updater_initiator_type_t *initiator_type), (request, initiator_type)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_updater_update_task_create,(ma_client_t *ma_client, const char *task_id, ma_task_t **task), (ma_client, task_id, task)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_updater_update_task_set_product_list,(ma_task_t *task, ma_array_t *product_list), (task, product_list)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_updater_update_task_set_locale,(ma_task_t *task, ma_uint32_t locale), (task, locale)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_updater_event_get_event_id,(const ma_updater_event_t *updater_event, long *event_id),(updater_event, event_id)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_updater_event_get_severity,(const ma_updater_event_t *updater_event, ma_int32_t *severity),(updater_event, severity)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_updater_event_get_session_id,(const ma_updater_event_t *updater_event, const char **session_id),(updater_event, session_id)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_updater_event_get_product_id,(const ma_updater_event_t *updater_event, const char **product_id),(updater_event, product_id)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_updater_event_get_locale,(const ma_updater_event_t *updater_event, const char **locale),(updater_event, locale)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_updater_event_get_update_type,(const ma_updater_event_t *updater_event, const char **update_type),(updater_event, update_type)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_updater_event_get_update_state,(const ma_updater_event_t *updater_event, ma_update_state_t *update_state),(updater_event, update_state)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_updater_event_get_update_error,(const ma_updater_event_t *updater_event, ma_int32_t *update_error),(updater_event, update_error)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_updater_event_get_new_version,(const ma_updater_event_t *updater_event, const char **new_version),(updater_event, new_version)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_updater_event_get_date_time,(const ma_updater_event_t *updater_event, const char **date_time),(updater_event, date_time)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_updater_event_get_script_id,(const ma_updater_event_t *updater_event, long *script_id),(updater_event, script_id))



static const char *ma_updater_function_names[] = {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) #function,
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
};

typedef struct ma_updater_functions_s {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) ret_type (*function##_fn) signature;
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
}ma_updater_functions_t;
static ma_updater_functions_t* ma_updater_functions = NULL;

#define ma_updater_functions_size (sizeof(ma_updater_function_names)/sizeof(ma_updater_function_names[0]))

#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) \
    MA_DISTPATCHER_ARG_STUB_CREATOR(&ma_updater_lib_version, MA_DISPATCHER_PRODUCT_NAME, MA_DISPATCHER_LIBRARY_NAME, MA_DISPATCHER_LIBRARY_VERSION,MA_DISPATCHER_MODULE,ret_type, function, signature, arg, ma_updater_function_names, &ma_updater_functions, ma_updater_functions_size)
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER 
#undef MA_DISPATCHER_ARG_EXPANDER

#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER 
MA_CPP(})

#endif /* MA_UPDATER_DISPATCHER_H_INCLUDED */
#endif /*MA_USE_DISPATCHER*/
