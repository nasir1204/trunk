#if  !defined(MA_NO_DISPATCHER) && !defined(MA_NO_LOGGER_DISPATCHER)
#ifndef MA_LOGGER_DISPATCHER_H_INCLUDED
#define MA_LOGGER_DISPATCHER_H_INCLUDED

#include "ma/dispatcher/ma_dispatcher_macros.h"
#include "ma/logger/ma_logger.h"

#undef MA_DISPATCHER_PRODUCT_NAME
#undef MA_DISPATCHER_LIBRARY_NAME
#undef MA_DISPATCHER_MODULE
#undef MA_DISPATCHER_LIBRARY_VERSION

#define MA_DISPATCHER_PRODUCT_NAME      "EPOAGENT3000"
#define MA_DISPATCHER_LIBRARY_NAME      MA_DEFINE_DISPATCHER_LIB(ma_logger)
#define MA_DISPATCHER_MODULE            "ma_logger"
#define MA_DISPATCHER_LIBRARY_VERSION   0x05020100 

static unsigned int ma_logger_lib_version;

MA_CPP(extern "C" {)
#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#define MA_DISPATCHER_ACTUAL_ARG_EXPANDER\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_log_v,(ma_logger_t *logger, int severity, const char *module, const char *file, int line, const char *func, const char *fmt, va_list ap),(logger,severity,module,file,line,func,fmt,ap))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_wlog_v,(ma_logger_t *logger, int severity, const char *module, const char *file, int line, const char *func, const ma_wchar_t *fmt, va_list ap),(logger,severity,module,file,line,func,fmt,ap))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_logger_inc_ref,(ma_logger_t *logger),(logger))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_logger_dec_ref,(ma_logger_t *logger),(logger))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_logger_add_filter,(ma_logger_t *logger,ma_log_filter_t *filter),(logger,filter))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_logger_remove_filter,(ma_logger_t *logger),(logger))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_logger_get_filter,(ma_logger_t *logger,ma_log_filter_t **filter),(logger,filter))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_generic_log_filter_create,(const char *filter_pattern , ma_log_filter_t **filter),(filter_pattern, filter))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_logger_register_visitor,(ma_logger_t *logger, ma_logger_on_visit_cb_t cb, void *cb_data),(logger, cb, cb_data))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_logger_register_localizer,(ma_logger_t *logger, ma_logger_on_localize_cb_t cb, void *cb_data),(logger, cb, cb_data))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_logger_unregister_visitor,(ma_logger_t *logger),(logger))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_logger_unregister_localizer,(ma_logger_t *logger),(logger))
    


static const char *ma_logger_function_names[] = {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) #function,
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
};

typedef struct ma_logger_functions_s {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) ret_type (*function##_fn) signature;
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
}ma_logger_functions_t;
static ma_logger_functions_t *ma_logger_functions;

#define ma_logger_functions_size (sizeof(ma_logger_function_names)/sizeof(ma_logger_function_names[0]))

#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) \
    MA_DISTPATCHER_ARG_STUB_CREATOR(&ma_logger_lib_version, MA_DISPATCHER_PRODUCT_NAME, MA_DISPATCHER_LIBRARY_NAME, MA_DISPATCHER_LIBRARY_VERSION,MA_DISPATCHER_MODULE,ret_type, function, signature, arg, ma_logger_function_names, &ma_logger_functions, ma_logger_functions_size)
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER 
#undef MA_DISPATCHER_ARG_EXPANDER

MA_DISTPATCHER_VARG_STUB_CREATOR(&ma_logger_lib_version, MA_DISPATCHER_PRODUCT_NAME, MA_DISPATCHER_LIBRARY_NAME, MA_DISPATCHER_LIBRARY_VERSION,MA_DISPATCHER_MODULE, ma_error_t, ma_log,(ma_logger_t *logger, int severity, const char *module, const char *file, int line, const char *func, const char *fmt, ...), ma_log_v,(ma_logger_t *logger, int severity, const char *module, const char *file, int line, const char *func, const char *fmt, va_list ap),(logger,severity,module,file,line,func,fmt,ap), fmt, ma_logger_function_names, &ma_logger_functions, ma_logger_functions_size)
MA_DISTPATCHER_VARG_STUB_CREATOR(&ma_logger_lib_version, MA_DISPATCHER_PRODUCT_NAME, MA_DISPATCHER_LIBRARY_NAME, MA_DISPATCHER_LIBRARY_VERSION,MA_DISPATCHER_MODULE, ma_error_t, ma_wlog,(ma_logger_t *logger, int severity, const char *module, const char *file, int line, const char *func, const ma_wchar_t *fmt, ...), ma_wlog_v,(ma_logger_t *logger, int severity, const char *module, const char *file, int line, const char *func, const ma_wchar_t *fmt, va_list ap),(logger,severity,module,file,line,func,fmt,ap), fmt, ma_logger_function_names, &ma_logger_functions, ma_logger_functions_size)

#undef MA_DISPATCHER_ARG_EXPANDER
MA_CPP(})
#endif /* MA_LOGGER_DISPATCHER_H_INCLUDED */
#endif /* #if  !defined(MA_NO_DISPATCHER) && !defined(MA_NO_LOGGER_DISPATCHER)*/

