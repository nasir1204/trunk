#if  !defined(MA_NO_DISPATCHER) && !defined(MA_NO_VARIANT_DISPATCHER)
#ifndef MA_BUFFER_DISPATCHER_H_INCLUDED
#define MA_BUFFER_DISPATCHER_H_INCLUDED

#include "ma/dispatcher/ma_dispatcher_macros.h"
#undef MA_DISPATCHER_PRODUCT_NAME
#undef MA_DISPATCHER_LIBRARY_NAME
#undef MA_DISPATCHER_MODULE
#undef MA_DISPATCHER_LIBRARY_VERSION

#define MA_DISPATCHER_PRODUCT_NAME      "EPOAGENT3000"
#define MA_DISPATCHER_LIBRARY_NAME      MA_DEFINE_DISPATCHER_LIB(ma_variant)
#define MA_DISPATCHER_MODULE            "ma_variant"
#define MA_DISPATCHER_LIBRARY_VERSION   0x05020100

static unsigned int ma_buffer_lib_version;


MA_CPP(extern "C" {)
#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#define MA_DISPATCHER_ACTUAL_ARG_EXPANDER\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_buffer_get_string,(ma_buffer_t *self, const char **value, size_t *size),(self, value, size))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_buffer_get_raw,(ma_buffer_t *self, const unsigned char **value, size_t *size),(self, value, size))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_buffer_create,(ma_buffer_t **self, size_t len),(self, len))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_buffer_add_ref,(ma_buffer_t *self),(self))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_buffer_release,(ma_buffer_t* self),(self))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_buffer_set,(ma_buffer_t *self, const char* src, size_t buffer_length),(self, src, buffer_length))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_buffer_size,(ma_buffer_t *self, size_t *buffer_length),(self, buffer_length))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_buffer_is_equal,(const ma_buffer_t *first, const ma_buffer_t *second, ma_bool_t *result),(first, second, result))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_wbuffer_create,(ma_wbuffer_t **self, size_t len),(self, len))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_wbuffer_add_ref,(ma_wbuffer_t *self), (self))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_wbuffer_release,(ma_wbuffer_t* self),(self))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_wbuffer_set,(ma_wbuffer_t* self, const wchar_t *src, size_t wbuffer_length),(self, src, wbuffer_length))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_wbuffer_size,(ma_wbuffer_t *self, size_t *wbuffer_length),(self, wbuffer_length))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_wbuffer_get_string,(ma_wbuffer_t *self, const wchar_t **value, size_t *size),(self, value, size))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_wbuffer_is_equal,(const ma_wbuffer_t *first, const ma_wbuffer_t *second, ma_bool_t *result),(first, second, result))

static const char *ma_buffer_function_names[] = {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) #function,
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
};

typedef struct ma_buffer_functions_s {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) ret_type (*function##_fn) signature;
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
}ma_buffer_functions_t;
static ma_buffer_functions_t* ma_buffer_functions;

#define ma_buffer_functions_size (sizeof(ma_buffer_function_names)/sizeof(ma_buffer_function_names[0]))

#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) \
    MA_DISTPATCHER_ARG_STUB_CREATOR(&ma_buffer_lib_version, MA_DISPATCHER_PRODUCT_NAME, MA_DISPATCHER_LIBRARY_NAME, MA_DISPATCHER_LIBRARY_VERSION,MA_DISPATCHER_MODULE,ret_type, function, signature, arg, ma_buffer_function_names, &ma_buffer_functions, ma_buffer_functions_size)
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER 
#undef MA_DISPATCHER_ARG_EXPANDER

#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER 
MA_CPP(})
#endif
#endif //MA_USE_DISPATCHER

