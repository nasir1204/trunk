#ifndef  MA_DISPATCHER_MACROS_H_INCLUDED
#define  MA_DISPATCHER_MACROS_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_errors.h"

#include <string.h>
#include <stdarg.h>

#if defined(HPUX)
#define MA_LIB_EXTN				".sl"
#elif defined(MACX)
#define MA_LIB_EXTN				".dylib"
#elif defined(_WIN32)
#define MA_LIB_EXTN				".dll"
#else
#define MA_LIB_EXTN				".so"
#endif

#ifdef MA_WINDOWS
#define MA_DEFINE_DISPATCHER_LIB(x) #x MA_LIB_EXTN
#else
#define MA_DEFINE_DISPATCHER_LIB(x) "lib"#x MA_LIB_EXTN
#endif


#define MA_INIT_FUNCTION_PATTERN "_init"
#define MA_DEINIT_FUNCTION_PATTERN "_deinit"
#define MA_DISPATCHER_MATCH_INIT_FUNCTION(func) ((strstr(#func,MA_INIT_FUNCTION_PATTERN))?MA_TRUE:MA_FALSE)
#define MA_DISPATCHER_MATCH_DEINIT_FUNCTION(func) ((strstr(#func,MA_DEINIT_FUNCTION_PATTERN))?MA_TRUE:MA_FALSE)

MA_CPP(extern "C" {)

extern ma_error_t ma_dispatcher_load_library(const char *module, const char *product_id, const char *library_id, unsigned int *version, const char **fnt, void *fat, size_t size);
extern ma_error_t ma_dispatcher_unload_library(const char *product_id, const char *library_id);

/* @MA_DISTPATCHER_ARG_STUB_CREATOR
 * @brief macro generates stub by iterating funtion name defined in function name table(fnt)
 *        and loads all the functions address in function address table(fat) once by calling 
 *        ma_dispatcher load_library() function.
 *        on executing library specific _init & _deinit method maintains reference count it will unloads all the library
 *        symbols and release library on reference count reaches to 0
 */
#define MA_DISTPATCHER_ARG_STUB_CREATOR(version, product, library, library_version, d_module, ret_type, function, signature, arg, fnt, fat, fnt_size)\
    MA_STATIC_INLINE ret_type function signature {\
        ret_type err = MA_OK;\
        ma_bool_t match_result = MA_FALSE;\
        /*if(*version && (library_version > *version))return MA_ERROR_DISPATCHER_VERSION_NOT_SUPPORTED;\*/\
        if((match_result = MA_DISPATCHER_MATCH_INIT_FUNCTION(function)) == MA_TRUE)\
            err = ma_dispatcher_load_library(d_module, product, library, version, (const char**)fnt, fat, fnt_size);\
        if(*fat) {\
            err = (*fat)->function##_fn?(*fat)->function##_fn arg:MA_ERROR_DISPATCHER_INVALID_MODULE;\
        } else {\
             err = ma_dispatcher_load_library(d_module, product, library, version, (const char**)fnt, fat, fnt_size);\
             err = ((*fat) &&(*fat)->function##_fn)?(*fat)->function##_fn arg:MA_ERROR_DISPATCHER_INVALID_MODULE;\
        }\
        if((match_result = MA_DISPATCHER_MATCH_DEINIT_FUNCTION(function)) == MA_TRUE)\
            (void)ma_dispatcher_unload_library(product, library);\
        return err;\
    }

/* @MA_DISTPATCHER_VARG_STUB_CREATOR
 * @brief macro generates stub by parsing variable arguments and builds va_list and calls substitute function which takes va_list as argument
 *        on executing library specific _init & _deinit method maintains reference count it will unloads all the library
 *        symbols and release library on reference count reaches to 0
 */
#define MA_DISTPATCHER_VARG_STUB_CREATOR(version, product, library, library_version, d_module, ret_type, function, signature, vfunction, vsignature, vparam, lastarg,fnt, fat, fnt_size)\
    MA_STATIC_INLINE ret_type function signature {\
        ret_type err = MA_OK;\
        ma_bool_t match_result = MA_FALSE;\
        va_list ap;\
        /*if(*version && (library_version > *version))return MA_ERROR_DISPATCHER_VERSION_NOT_SUPPORTED;\*/\
        if((match_result = MA_DISPATCHER_MATCH_INIT_FUNCTION(function)) == MA_TRUE)\
            err = ma_dispatcher_load_library(d_module, product, library, version, (const char**)fnt, fat, fnt_size);\
        va_start(ap, lastarg);\
        if(*fat) {\
            err = (*fat)->vfunction##_fn?(*fat)->vfunction##_fn vparam:MA_ERROR_DISPATCHER_INVALID_MODULE;\
        } else {\
            err = ma_dispatcher_load_library(d_module, product, library, version, (const char**)fnt, fat, fnt_size);\
            err = ((*fat) && (*fat)->vfunction##_fn)?(*fat)->vfunction##_fn vparam:MA_ERROR_DISPATCHER_INVALID_MODULE;\
        }\
        va_end(ap);\
        if((match_result = MA_DISPATCHER_MATCH_DEINIT_FUNCTION(function)) == MA_TRUE)\
            (void)ma_dispatcher_unload_library(product, library);\
        return err;\
    }

MA_CPP(})

#endif /* MA_DISPATCHER_MACROS_H_INCLUDED */
