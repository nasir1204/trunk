#if  !defined(MA_NO_DISPATCHER) && !defined(MA_NO_SCHEDULER_DISPATCHER)
#ifndef MA_TRIGGER_DISPATCHER_H_INCLUDED
#define MA_TRIGGER_DISPATCHER_H_INCLUDED

#include "ma/dispatcher/ma_dispatcher_macros.h"

#undef MA_DISPATCHER_PRODUCT_NAME
#undef MA_DISPATCHER_LIBRARY_NAME
#undef MA_DISPATCHER_MODULE
#undef MA_DISPATCHER_LIBRARY_VERSION

#define MA_DISPATCHER_PRODUCT_NAME		 "EPOAGENT3000"
#define MA_DISPATCHER_LIBRARY_NAME		 MA_DEFINE_DISPATCHER_LIB(ma_client)
#define MA_DISPATCHER_MODULE			 "ma_scheduler"
#define MA_DISPATCHER_LIBRARY_VERSION	 0x05020100

static unsigned int scheduler_trigger_version;

MA_CPP(extern "C" {)
#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#define MA_DISPATCHER_ACTUAL_ARG_EXPANDER\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_trigger_create_run_once,(ma_trigger_run_once_policy_t *policy, ma_trigger_t **trigger),(policy, trigger))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_trigger_create_daily,(ma_trigger_daily_policy_t *policy, ma_trigger_t **trigger),(policy, trigger))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_trigger_create_weekly,(ma_trigger_weekly_policy_t *policy, ma_trigger_t **trigger),(policy,trigger))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_trigger_create_monthly_date,(ma_trigger_monthly_date_policy_t *policy, ma_trigger_t **trigger),(policy, trigger))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_trigger_create_monthly_dow,(ma_trigger_monthly_dow_policy_t *policy, ma_trigger_t **trigger),(policy, trigger))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_trigger_create_system_idle,(ma_trigger_system_idle_policy_t *policy, ma_trigger_t **trigger),(policy, trigger))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_trigger_create_dialup,(ma_trigger_dialup_policy_t *policy, ma_trigger_t **trigger),(policy, trigger))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_trigger_create_system_start,(ma_trigger_systemstart_policy_t *policy, ma_trigger_t **trigger),(policy, trigger))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_trigger_create_logon,(ma_trigger_logon_policy_t *policy, ma_trigger_t **trigger),(policy, trigger))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_trigger_create_run_now,(ma_trigger_run_now_policy_t const *policy, ma_trigger_t **trigger),(policy, trigger))



static const char *ma_trigger_function_names[] = {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) #function,
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
};

typedef struct ma_trigger_functions_s {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) ret_type (*function##_fn) signature;
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
}ma_trigger_functions_t;
static ma_trigger_functions_t *ma_trigger_functions;

#define ma_trigger_function_table_size (sizeof(ma_trigger_function_names)/sizeof(ma_trigger_function_names[0]))

#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) \
    MA_DISTPATCHER_ARG_STUB_CREATOR(&scheduler_trigger_version, MA_DISPATCHER_PRODUCT_NAME, MA_DISPATCHER_LIBRARY_NAME, MA_DISPATCHER_LIBRARY_VERSION,MA_DISPATCHER_MODULE,ret_type, function, signature, arg, ma_trigger_function_names, &ma_trigger_functions, ma_trigger_function_table_size)
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER 
#undef MA_DISPATCHER_ARG_EXPANDER

#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER 
MA_CPP(})
#endif
#endif //MA_USE_DISPATCHER
