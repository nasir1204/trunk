#if  !defined(MA_NO_DISPATCHER) && !defined(MA_NO_SCHEDULER_DISPATCHER)
#ifndef MA_SCHEDULER_SERVICE_DISPATCHER_H_INCLUDED
#define MA_SCHEDULER_SERVICE_DISPATCHER_H_INCLUDED

#include "ma/dispatcher/ma_dispatcher_macros.h"

#undef MA_DISPATCHER_PRODUCT_NAME
#undef MA_DISPATCHER_LIBRARY_NAME
#undef MA_DISPATCHER_MODULE
#undef MA_DISPATCHER_LIBRARY_VERSION

#define MA_DISPATCHER_PRODUCT_NAME		 "EPOAGENT3000"
#define MA_DISPATCHER_LIBRARY_NAME		 MA_DEFINE_DISPATCHER_LIB(ma_scheduler)
#define MA_DISPATCHER_MODULE			 "ma_scheduler_service"
#define MA_DISPATCHER_LIBRARY_VERSION	 	 0x05020100

static unsigned int scheduler_svc_lib_ver;

MA_CPP(extern "C" {)
#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#define MA_DISPATCHER_ACTUAL_ARG_EXPANDER\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_scheduler_service_create,(const char *service_name, ma_service_t **service),(service_name,service))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_scheduler_service_release,(ma_scheduler_service_t *service), (service))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_scheduler_service_start,(ma_scheduler_service_t *service),(service))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_scheduler_service_stop,(ma_scheduler_service_t *service), (service))


static const char *ma_scheduler_service_function_names[] = {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) #function,
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
};

typedef struct ma_scheduler_service_functions_s {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) ret_type (*function##_fn) signature;
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
}ma_scheduler_service_functions_t;
static ma_scheduler_service_functions_t *scheduler_service_functions;

#define scheduler_service_ft_size (sizeof(ma_scheduler_service_function_names)/sizeof(ma_scheduler_service_function_names[0]))

#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) \
    MA_DISTPATCHER_ARG_STUB_CREATOR(&scheduler_svc_lib_ver, MA_DISPATCHER_PRODUCT_NAME, MA_DISPATCHER_LIBRARY_NAME, MA_DISPATCHER_LIBRARY_VERSION,MA_DISPATCHER_MODULE,ret_type, function, signature, arg, ma_scheduler_service_function_names, &scheduler_service_functions, scheduler_service_ft_size)
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER 
#undef MA_DISPATCHER_ARG_EXPANDER

#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER 
MA_CPP(})
#endif
#endif //MA_USE_DISPATCHER
