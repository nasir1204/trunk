#if  !defined(MA_NO_DISPATCHER) && !defined(MA_NO_REPOSITORY_MIRROR_DISPATCHER)
#ifndef MA_REPOSITORY_MIRROR_DISPATCHER_H_INCLUDED
#define MA_REPOSITORY_MIRROR_DISPATCHER_H_INCLUDED

#include "ma/dispatcher/ma_dispatcher_macros.h"

#undef MA_DISPATCHER_PRODUCT_NAME
#undef MA_DISPATCHER_LIBRARY_NAME
#undef MA_DISPATCHER_MODULE
#undef MA_DISPATCHER_LIBRARY_VERSION

#define MA_DISPATCHER_PRODUCT_NAME		 "EPOAGENT3000"
#define MA_DISPATCHER_LIBRARY_NAME		 MA_DEFINE_DISPATCHER_LIB(ma_client)
#define MA_DISPATCHER_MODULE			 "repository"
#define MA_DISPATCHER_LIBRARY_VERSION	 0x05020100

static unsigned int ma_repository_mirror_lib_version;

MA_CPP(extern "C" {)
#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#define MA_DISPATCHER_ACTUAL_ARG_EXPANDER\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_repository_mirror_register_state_callback, (ma_client_t *ma_client, ma_repository_mirror_state_cb_t cb, void *cb_data), (ma_client, cb, cb_data)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_repository_mirror_start, (ma_client_t *ma_client, const char *destination_path, ma_buffer_t **session_id), (ma_client, destination_path, session_id)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_repository_mirror_get_state, (ma_client_t *ma_client, ma_buffer_t *session_id, ma_repository_mirror_state_t *state), (ma_client, session_id, state)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_repository_mirror_stop, (ma_client_t *ma_client, ma_buffer_t *session_id), (ma_client, session_id)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_repository_mirror_task_create, (ma_client_t *ma_client, const char *task_id, const char *destination_path, ma_task_t **task), (ma_client, task_id, destination_path, task )) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_repository_mirror_task_get_session_id, (ma_task_t *task, ma_buffer_t **session_id), (task,session_id ))

static const char *ma_repository_mirror_function_names[] = {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) #function,
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
};

typedef struct ma_repository_mirror_functions_s {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) ret_type (*function##_fn) signature;
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
}ma_repository_mirror_functions_t;
static ma_repository_mirror_functions_t* ma_repository_mirror_functions = NULL;

#define ma_repository_mirror_functions_size (sizeof(ma_repository_mirror_function_names)/sizeof(ma_repository_mirror_function_names[0]))

#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) \
    MA_DISTPATCHER_ARG_STUB_CREATOR(&ma_repository_mirror_lib_version, MA_DISPATCHER_PRODUCT_NAME, MA_DISPATCHER_LIBRARY_NAME, MA_DISPATCHER_LIBRARY_VERSION,MA_DISPATCHER_MODULE,ret_type, function, signature, arg, ma_repository_mirror_function_names, &ma_repository_mirror_functions, ma_repository_mirror_functions_size)
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER

#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER
MA_CPP(})

#endif /* MA_REPOSITORY_MIRROR_DISPATCHER_H_INCLUDED */
#endif /*MA_USE_DISPATCHER*/
