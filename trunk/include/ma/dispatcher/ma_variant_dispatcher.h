#if  !defined(MA_NO_DISPATCHER) && !defined(MA_NO_VARIANT_DISPATCHER)
#ifndef MA_VARIANT_DISPATCHER_H_INCLUDED
#define MA_VARIANT_DISPATCHER_H_INCLUDED

#include "ma/dispatcher/ma_dispatcher_macros.h"
#undef MA_DISPATCHER_PRODUCT_NAME
#undef MA_DISPATCHER_LIBRARY_NAME
#undef MA_DISPATCHER_MODULE
#undef MA_DISPATCHER_LIBRARY_VERSION

#define MA_DISPATCHER_PRODUCT_NAME      "EPOAGENT3000"
#define MA_DISPATCHER_LIBRARY_NAME      MA_DEFINE_DISPATCHER_LIB(ma_variant)
#define MA_DISPATCHER_MODULE            "ma_variant"
#define MA_DISPATCHER_LIBRARY_VERSION   0x05020100

static unsigned int ma_variant_lib_version;


MA_CPP(extern "C" {)
#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#define MA_DISPATCHER_ACTUAL_ARG_EXPANDER\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t, ma_variant_init, (), ())\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_variant_deinit,(), ())\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_variant_create,(ma_variant_t** p1),(p1))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_variant_get_version,(unsigned int *p1), (p1))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_variant_is_equal,(const ma_variant_t* p1, const ma_variant_t* p2, ma_bool_t* p3), (p1, p2, p3))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_variant_add_ref,(ma_variant_t *self), (self))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_variant_release,(ma_variant_t *self), (self))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_variant_get_type,(const ma_variant_t *self, ma_vartype_t *var_type), (self, var_type))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_variant_create_from_uint64,(ma_uint64_t value, ma_variant_t **var_obj), (value, var_obj))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_variant_get_uint64,(const ma_variant_t *self, ma_uint64_t *value), (self, value))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_variant_create_from_int64,(ma_int64_t value, ma_variant_t **var_obj), (value, var_obj))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_variant_get_int64,(const ma_variant_t *self, ma_int64_t *value), (self, value))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_variant_create_from_double,(double value, ma_variant_t **var_obj), (value, var_obj))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_variant_get_double,(const ma_variant_t *self, double *value), (self, value))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_variant_create_from_uint32,(ma_uint32_t value, ma_variant_t **var_obj), (value, var_obj))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_variant_get_uint32,(const ma_variant_t *self, ma_uint32_t *value), (self, value))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_variant_create_from_int32,(ma_int32_t value, ma_variant_t** var_obj), (value, var_obj))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_variant_get_int32,(const ma_variant_t *self, ma_int32_t *value),(self, value))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_variant_create_from_float,(float value, ma_variant_t** var_obj),(value, var_obj))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_variant_get_float,(const ma_variant_t *self, float *value),(self, value))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_variant_create_from_uint16,(ma_uint16_t value, ma_variant_t** var_obj),(value, var_obj))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_variant_get_uint16,(const ma_variant_t *self, ma_uint16_t *value), (self, value))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_variant_create_from_int16,(ma_int16_t value, ma_variant_t** var_obj),(value, var_obj))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_variant_get_int16,(const ma_variant_t *self, ma_int16_t *value),(self, value))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_variant_create_from_uint8,(ma_uint8_t value, ma_variant_t** var_obj),(value, var_obj))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_variant_get_uint8,(const ma_variant_t *self, ma_uint8_t *value),(self, value))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_variant_create_from_int8,(ma_int8_t value, ma_variant_t** var_obj),(value, var_obj))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_variant_get_int8,(const ma_variant_t *self, ma_int8_t *value),(self, value))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_variant_create_from_bool,(ma_bool_t value, ma_variant_t** var_obj),(value, var_obj))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_variant_get_bool,(const ma_variant_t *self, ma_bool_t *value),(self, value))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_variant_create_from_string,(const char *value, size_t  len, ma_variant_t **var_obj),(value, len, var_obj))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_variant_get_string_buffer,(ma_variant_t *self, ma_buffer_t **buffer), (self, buffer))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_variant_create_from_wstring,(const wchar_t *value, size_t len, ma_variant_t **var_obj), (value, len, var_obj))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_variant_get_wstring_buffer,(ma_variant_t *self, ma_wbuffer_t **wbuffer),(self, wbuffer))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_variant_create_from_table,(ma_table_t *value, ma_variant_t** var_obj),(value, var_obj))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_variant_get_table,(ma_variant_t *self, ma_table_t **value),(self, value))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_variant_create_from_array,(ma_array_t *value, ma_variant_t** var_obj),(value, var_obj))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_variant_get_array,(ma_variant_t *self, ma_array_t **value),(self, value))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_variant_create_from_raw,(const void *value, size_t len, ma_variant_t** var_obj),(value, len, var_obj))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_variant_get_raw_buffer,(ma_variant_t *self, ma_buffer_t **buffer), (self, buffer))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_table_create,(ma_table_t **table),(table))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_table_add_ref,(ma_table_t *self),(self))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_table_release,(ma_table_t *self),(self))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_table_size,(const ma_table_t *self, size_t *size),(self, size))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_table_is_equal,(ma_table_t const *l, ma_table_t const *r, ma_bool_t *result),(l,r,result))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_table_add_entry,(ma_table_t *self, const char *table_key, ma_variant_t *value),(self, table_key, value))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_table_add_wentry,(ma_table_t *self, const wchar_t *table_key, ma_variant_t *value),(self, table_key, value))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_table_remove_entry,(ma_table_t *self, const char *table_key),(self, table_key))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_table_remove_wentry,(ma_table_t *self, const wchar_t *table_key),(self, table_key))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_table_foreach,(const ma_table_t *self,MA_TABLE_FOREACH_CALLBACK cb,void *cb_args),(self,cb,cb_args))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_table_wforeach,(const ma_table_t *self, MA_TABLE_WFOREACH_CALLBACK cb, void *cb_args),(self, cb, cb_args))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_table_get_wvalue,(ma_table_t *self,const wchar_t *table_key,ma_variant_t **table_value),(self, table_key, table_value))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_table_get_value,(ma_table_t *self,const char *table_key,ma_variant_t **table_value),(self,table_key,table_value))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_table_get_keys,(const ma_table_t *self,ma_array_t **keys),(self,keys))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_array_create,(ma_array_t **new_list),(new_list))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_array_add_ref,(ma_array_t *self),(self))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_array_release,(ma_array_t *self),(self))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_array_size,(const ma_array_t *self, size_t *size),(self, size))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_array_is_equal,(ma_array_t const *l, ma_array_t const *r, ma_bool_t *result),(l, r, result))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_array_push,(ma_array_t *self, ma_variant_t *value),(self, value))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_array_get_element_at,(ma_array_t *self, size_t array_list_index, ma_variant_t **element_return_value),(self, array_list_index, element_return_value))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_array_set_element_at,(ma_array_t *self, size_t array_list_index, ma_variant_t *element_value),(self, array_list_index, element_value))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_array_foreach,(const ma_array_t *self, MA_ARRAY_FOREACH_CALLBACK cb, void *user_data),(self, cb, user_data))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_variant_is_type,(const ma_variant_t *self, ma_vartype_t var_type), (self, var_type))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_variant_get_raw,(ma_variant_t *variant, const unsigned char **raw, size_t *size),(variant, raw, size)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_variant_get_string,(ma_variant_t *variant, const char **str),(variant, str))

static const char *ma_variant_function_names[] = {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) #function,
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
};

typedef struct ma_variant_functions_s {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) ret_type (*function##_fn) signature;
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
}ma_variant_functions_t;
static ma_variant_functions_t* ma_variant_functions;

#define ma_variant_functions_size (sizeof(ma_variant_function_names)/sizeof(ma_variant_function_names[0]))

#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) \
    MA_DISTPATCHER_ARG_STUB_CREATOR(&ma_variant_lib_version, MA_DISPATCHER_PRODUCT_NAME, MA_DISPATCHER_LIBRARY_NAME, MA_DISPATCHER_LIBRARY_VERSION,MA_DISPATCHER_MODULE,ret_type, function, signature, arg, ma_variant_function_names, &ma_variant_functions, ma_variant_functions_size)
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER 
#undef MA_DISPATCHER_ARG_EXPANDER

#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER 
MA_CPP(})
#endif
#endif //MA_USE_DISPATCHER

