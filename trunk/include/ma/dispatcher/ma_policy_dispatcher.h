#if  !defined(MA_NO_DISPATCHER) && !defined(MA_NO_POLICY_DISPATCHER)
#ifndef MA_POLICY_DISPATCHER_H_INCLUDED
#define MA_POLICY_DISPATCHER_H_INCLUDED

#include "ma/dispatcher/ma_dispatcher_macros.h"

#undef MA_DISPATCHER_PRODUCT_NAME
#undef MA_DISPATCHER_LIBRARY_NAME
#undef MA_DISPATCHER_MODULE
#undef MA_DISPATCHER_LIBRARY_VERSION

#define MA_DISPATCHER_PRODUCT_NAME		 "EPOAGENT3000"
#define MA_DISPATCHER_LIBRARY_NAME		 MA_DEFINE_DISPATCHER_LIB(ma_client)
#define MA_DISPATCHER_MODULE			 "policy"
#define MA_DISPATCHER_LIBRARY_VERSION	 0x05020100

static unsigned int ma_policy_lib_version;



MA_CPP(extern "C" {)
#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#define MA_DISPATCHER_ACTUAL_ARG_EXPANDER\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_uri_create,(ma_policy_uri_t **uri), (uri))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_uri_release,(ma_policy_uri_t *uri), (uri)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_uri_add_ref,(ma_policy_uri_t *uri), (uri))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_uri_copy,(const ma_policy_uri_t *uri_src, ma_policy_uri_t **uri_dest), (uri_src, uri_dest))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_uri_set_software_id,(ma_policy_uri_t *uri, const char *software_id), (uri, software_id)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_uri_set_version,(ma_policy_uri_t *uri, const char *version), (uri, version)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_uri_set_user,(ma_policy_uri_t *uri, const char *user), (uri, user)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_uri_set_user_list,(ma_policy_uri_t *uri, ma_array_t *user_list), (uri, user_list)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_uri_set_location_id,(ma_policy_uri_t *uri, const char *location_id), (uri, location_id)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_uri_set_feature,(ma_policy_uri_t *uri, const char *feature), (uri, feature)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_uri_set_category,(ma_policy_uri_t *uri, const char *category), (uri, category)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_uri_set_type,(ma_policy_uri_t *uri, const char *type), (uri, type)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_uri_set_name,(ma_policy_uri_t *uri, const char *name), (uri, name)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_uri_set_pso_name,(ma_policy_uri_t *uri, const char *pso_name), (uri, pso_name)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_uri_set_pso_param_int,(ma_policy_uri_t *uri, const char *pso_param_int), (uri, pso_param_int)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_uri_set_pso_param_str,(ma_policy_uri_t *uri, const char *pso_param_str), (uri, pso_param_str)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_uri_get_notification_type,(const ma_policy_uri_t *uri, enum ma_policy_notification_type_e *type), (uri, type)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_uri_get_software_id,(const ma_policy_uri_t *uri, const char **software_id), (uri, software_id)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_uri_get_version,(const ma_policy_uri_t *uri, const char **version), (uri, version)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_uri_get_user,(const ma_policy_uri_t *uri, const char **user), (uri, user)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_uri_get_user_session_id,(const ma_policy_uri_t *uri, ma_uint32_t *session_id), (uri, session_id)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_uri_get_user_logon_state,(const ma_policy_uri_t *uri, enum ma_user_logon_state_e *logon_state), (uri, logon_state)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_uri_get_location_id,(const ma_policy_uri_t *uri, const char **location_id), (uri, location_id)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_uri_get_feature,(const ma_policy_uri_t *uri, const char **feature), (uri, feature)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_uri_get_category,(const ma_policy_uri_t *uri, const char **category), (uri, category)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_uri_get_type,(const ma_policy_uri_t *uri, const char **type), (uri, type)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_uri_get_name,(const ma_policy_uri_t *uri, const char **name), (uri, name)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_uri_get_pso_name,(const ma_policy_uri_t *uri, const char **pso_name), (uri, pso_name)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_uri_get_pso_param_int,(const ma_policy_uri_t *uri, const char **pso_param_int), (uri, pso_param_int)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_uri_get_pso_param_str,(const ma_policy_uri_t *uri, const char **pso_param_str), (uri, pso_param_str)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_bag_get_value,(const ma_policy_bag_t *bag, const ma_policy_uri_t *uri, const char *section_name, const char *key_name, ma_variant_t **value), (bag, uri, section_name, key_name, value)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_bag_set_value,(ma_policy_bag_t *bag, const ma_policy_uri_t *uri, const char *section_name, const char *key_name, const ma_variant_t *value), (bag, uri, section_name, key_name, value)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_bag_create,(ma_policy_bag_t **bag), (bag)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_bag_add_ref,(ma_policy_bag_t *bag), (bag)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_bag_release,(ma_policy_bag_t *bag), (bag)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_bag_get_iterator,(ma_policy_bag_t *bag,  ma_policy_bag_iterator_t **iterator), (bag,  iterator)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_bag_get_uri_iterator,(ma_policy_bag_t *bag,  ma_policy_bag_iterator_t **iterator), (bag,  iterator)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_bag_get_section_iterator,(ma_policy_bag_t *bag,  const ma_policy_uri_t *uri, ma_policy_bag_iterator_t **iterator), (bag, uri, iterator)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_bag_get_setting_iterator,(ma_policy_bag_t *bag,  const ma_policy_uri_t *uri, const char *section, ma_policy_bag_iterator_t **iterator), (bag, uri, section, iterator)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_bag_iterator_get_next,(ma_policy_bag_iterator_t *iterator, const ma_policy_uri_t **uri, const char **section_name, const char **key_name, const ma_variant_t **value), (iterator, uri, section_name, key_name, value)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_bag_uri_iterator_get_next,(ma_policy_bag_iterator_t *iterator, const ma_policy_uri_t **uri), (iterator, uri)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_bag_section_iterator_get_next,(ma_policy_bag_iterator_t *iterator, const char **section_name), (iterator, section_name)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_bag_setting_iterator_get_next,(ma_policy_bag_iterator_t *iterator, const char **key_name, const ma_variant_t **key_value), (iterator, key_name, key_value)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_bag_iterator_release,(ma_policy_bag_iterator_t *iterator), (iterator)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_register_notification_callback,(ma_client_t *ma_client, const char *product_id, ma_uint32_t notify_type, ma_policy_notification_cb_t cb, void *cb_data), (ma_client, product_id, notify_type, cb, cb_data)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_unregister_notification_callback,(ma_client_t *ma_client, const char *product_id), (ma_client, product_id)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_get,(ma_client_t *ma_client, const ma_policy_uri_t *uri, ma_policy_bag_t **policy_bag), (ma_client, uri, policy_bag)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_set,(ma_client_t *ma_client, const ma_policy_uri_t *uri, const ma_policy_bag_t *policy_bag), (ma_client, uri, policy_bag)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_enforce,(ma_client_t *ma_client, const char *product_id), (ma_client, product_id)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_refresh,(ma_client_t *ma_client, const ma_policy_uri_t *uri), (ma_client, uri))



static const char *ma_policy_function_names[] = {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) #function,
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
};

typedef struct ma_policy_functions_s {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) ret_type (*function##_fn) signature;
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
}ma_policy_functions_t;
static ma_policy_functions_t* ma_policy_functions = NULL;

#define ma_policy_functions_size (sizeof(ma_policy_function_names)/sizeof(ma_policy_function_names[0]))

#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) \
    MA_DISTPATCHER_ARG_STUB_CREATOR(&ma_policy_lib_version, MA_DISPATCHER_PRODUCT_NAME, MA_DISPATCHER_LIBRARY_NAME, MA_DISPATCHER_LIBRARY_VERSION,MA_DISPATCHER_MODULE,ret_type, function, signature, arg, ma_policy_function_names, &ma_policy_functions, ma_policy_functions_size)
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER

#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER
MA_CPP(})

#endif /* MA_POLICY_DISPATCHER_H_INCLUDED */
#endif /*MA_USE_DISPATCHER*/

