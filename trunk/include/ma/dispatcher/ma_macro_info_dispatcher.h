#if  !defined(MA_NO_DISPATCHER) && !defined(MA_NO_INFO_DISPATCHER)
#ifndef MA_MACRO_INFO_DISPATCHER_H_INCLUDED
#define MA_MACRO_INFO_DISPATCHER_H_INCLUDED

#include "ma/dispatcher/ma_dispatcher_macros.h"

#undef MA_DISPATCHER_PRODUCT_NAME
#undef MA_DISPATCHER_LIBRARY_NAME
#undef MA_DISPATCHER_MODULE
#undef MA_DISPATCHER_LIBRARY_VERSION

#define MA_DISPATCHER_PRODUCT_NAME		 "EPOAGENT3000"
#define MA_DISPATCHER_LIBRARY_NAME		 MA_DEFINE_DISPATCHER_LIB(ma_client)
#define MA_DISPATCHER_MODULE			 "ma_macro_info"
#define MA_DISPATCHER_LIBRARY_VERSION	 0x05020100

static unsigned int ma_macro_info_lib_version;

MA_CPP(extern "C" {)
#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#define MA_DISPATCHER_ACTUAL_ARG_EXPANDER\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_macro_info_get,(const char *macro_name, ma_buffer_t **macro_value), (macro_name, macro_value))	

static const char *ma_info_handler_function_names[] = {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) #function,
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
};

typedef struct ma_info_handler_functions_s {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) ret_type (*function##_fn) signature;
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
}ma_info_handler_functions_t;
static ma_info_handler_functions_t* ma_info_handler_functions = NULL;

#define ma_info_handler_functions_size (sizeof(ma_info_handler_function_names)/sizeof(ma_info_handler_function_names[0]))

#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) \
    MA_DISTPATCHER_ARG_STUB_CREATOR(&ma_info_handler_lib_version, MA_DISPATCHER_PRODUCT_NAME, MA_DISPATCHER_LIBRARY_NAME, MA_DISPATCHER_LIBRARY_VERSION,MA_DISPATCHER_MODULE,ret_type, function, signature, arg, ma_info_handler_function_names, &ma_info_handler_functions, ma_info_handler_functions_size)
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER 
#undef MA_DISPATCHER_ARG_EXPANDER

#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER 
MA_CPP(})
#endif /*MA_MACRO_INFO_DISPATCHER_H_INCLUDED*/
#endif /*!defined(MA_NO_DISPATCHER) && !defined(MA_NO_INFO_DISPATCHER) */

