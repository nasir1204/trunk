#if  !defined(MA_NO_DISPATCHER) && !defined(MA_NO_EVENT_DISPATCHER)
#ifndef MA_EVENT_DISPATCHER_H_INCLUDED
#define MA_EVENT_DISPATCHER_H_INCLUDED

#include "ma/dispatcher/ma_dispatcher_macros.h"

#undef MA_DISPATCHER_PRODUCT_NAME
#undef MA_DISPATCHER_LIBRARY_NAME
#undef MA_DISPATCHER_MODULE
#undef MA_DISPATCHER_LIBRARY_VERSION

#define MA_DISPATCHER_PRODUCT_NAME		 "EPOAGENT3000"
#define MA_DISPATCHER_LIBRARY_NAME		  MA_DEFINE_DISPATCHER_LIB(ma_client)
#define MA_DISPATCHER_MODULE			 "ma_event"
#define MA_DISPATCHER_LIBRARY_VERSION	 0x05020100

static unsigned int ma_event_lib_version;

/*
MA_EVENT_API ma_error_t ma_event_bag_create(ma_client_t *ma_client, const char *product_name, ma_event_bag_t **event_bag);
MA_EVENT_API ma_error_t ma_event_bag_set_product_version(ma_event_bag_t *self, const char *product_version);
MA_EVENT_API ma_error_t ma_event_bag_set_product_family(ma_event_bag_t *self, const char *product_family);
MA_EVENT_API ma_error_t ma_event_bag_add_common_field(ma_event_bag_t *self, const char *key, ma_variant_t *value);
MA_EVENT_API ma_error_t ma_event_bag_set_custom_fields_table(ma_event_bag_t *self, const char *target_table);
MA_EVENT_API ma_error_t ma_event_bag_add_custom_field(ma_event_bag_t *self, const char *key, ma_variant_t *value);
MA_EVENT_API ma_error_t ma_event_bag_add_event(ma_event_bag_t *self, ma_event_t *ma_event);
MA_EVENT_API ma_error_t ma_event_bag_send(ma_event_bag_t *self);
MA_EVENT_API ma_error_t ma_event_bag_release(ma_event_bag_t *self);
*/

MA_CPP(extern "C" {)
#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#define MA_DISPATCHER_ACTUAL_ARG_EXPANDER\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_event_create,(ma_client_t *ma_client, ma_uint32_t eventid, ma_event_severity_t severity, ma_event_t **ma_event), (ma_client, eventid, severity, ma_event))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_event_add_common_field,(ma_event_t *self, const char *key, ma_variant_t *value), (self, key, value))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_event_set_custom_fields_table,(ma_event_t *self, const char *target_table),(self, target_table))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_event_add_custom_field,(ma_event_t *self, const char *key, ma_variant_t *value), (self, key, value))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_event_add_ref,(ma_event_t *self), (self))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_event_release,(ma_event_t *self), (self))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_event_get_id,(ma_event_t *self, ma_uint32_t *eventid), (self, eventid))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_event_get_severity,(ma_event_t *self, ma_event_severity_t *severity), (self, severity))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_event_get_common_field,(ma_event_t *self, const char *key, ma_variant_t **value), (self, key, value))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_event_get_custom_fields_table,(ma_event_t *self, const char **target_table),(self, target_table))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_event_get_custom_field,(ma_event_t *self, const char *key, ma_variant_t **value), (self, key, value))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_event_bag_create,(ma_client_t *ma_client, const char *product_name, ma_event_bag_t **event_bag), (ma_client, product_name, event_bag))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_event_bag_set_product_version,(ma_event_bag_t *self, const char *product_version), (self, product_version))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_event_bag_set_product_family,(ma_event_bag_t *self, const char *product_family),(self, product_family))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_event_bag_add_common_field,(ma_event_bag_t *self, const char *key, ma_variant_t *value), (self, key, value))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_event_bag_set_custom_fields_table,(ma_event_bag_t *self, const char *target_table),(self, target_table))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_event_bag_add_custom_field,(ma_event_bag_t *self, const char *key, ma_variant_t *value), (self, key, value))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_event_bag_add_event,(ma_event_bag_t *self, ma_event_t *ma_event), (self, ma_event))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_event_bag_send,(ma_event_bag_t *self), (self))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_event_bag_add_ref,(ma_event_bag_t *self), (self))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_event_bag_release,(ma_event_bag_t *self), (self))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_event_bag_get_product_name,(ma_event_bag_t *self, const char **product_name), (self, product_name))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_event_bag_get_product_version,(ma_event_bag_t *self, const char **product_version), (self,product_version))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_event_bag_get_product_family,(ma_event_bag_t *self, const char **product_family), (self, product_family))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_event_bag_get_common_field,(ma_event_bag_t *self, const char *key, ma_variant_t **value), (self, key,value))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_event_bag_get_custom_fields_table,(ma_event_bag_t *self, const char **target_table),(self, target_table))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_event_bag_get_custom_field,(ma_event_bag_t *self, const char *key, ma_variant_t **value),(self, key, value))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_event_bag_get_event_count,(ma_event_bag_t *self, size_t *count),(self, count))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_event_bag_get_event,(ma_event_bag_t *self, size_t index, ma_event_t **ma_event),(self, index, ma_event))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_event_upload,(ma_client_t *ma_client, const char *product_id),(ma_client, product_id))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_event_register_callbacks,(ma_client_t *ma_client, ma_event_callbacks_t *cb, void *cb_data),(ma_client, cb, cb_data))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_event_unregister_callbacks,(ma_client_t *ma_client),(ma_client))

static const char *ma_event_function_names[] = {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) #function,
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
};

typedef struct ma_event_functions_s {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) ret_type (*function##_fn) signature;
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
}ma_event_functions_t;
static ma_event_functions_t* ma_event_functions = NULL;

#define ma_event_functions_size (sizeof(ma_event_function_names)/sizeof(ma_event_function_names[0]))

#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) \
    MA_DISTPATCHER_ARG_STUB_CREATOR(&ma_event_lib_version, MA_DISPATCHER_PRODUCT_NAME, MA_DISPATCHER_LIBRARY_NAME, MA_DISPATCHER_LIBRARY_VERSION,MA_DISPATCHER_MODULE,ret_type, function, signature, arg, ma_event_function_names, &ma_event_functions, ma_event_functions_size)
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER

#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER
MA_CPP(})
#endif
#endif //MA_USE_DISPATCHER
