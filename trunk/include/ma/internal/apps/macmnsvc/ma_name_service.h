#ifndef MA_NAME_SERVICE_H_INCLUDED
#define MA_NAME_SERVICE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/services/ma_service.h"

MA_CPP(extern "C" {)

typedef struct ma_name_service_s ma_name_service_t, *ma_name_service_h;

ma_error_t ma_name_service_create(char const *service_name, ma_service_t **name_service);

MA_CPP(})

#endif /* MA_NAME_SERVICE_H_INCLUDED */
