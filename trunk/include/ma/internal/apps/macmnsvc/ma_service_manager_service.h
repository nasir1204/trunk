#ifndef MA_SERVICES_MANAGER_SERVICE_H_INCLUDED
#define MA_SERVICES_MANAGER_SERVICE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/services/ma_service.h"

MA_CPP(extern "C" {)

typedef struct ma_services_manager_service_s ma_services_manager_service_t, *ma_services_manager_service_h;

ma_error_t ma_services_manager_service_create(char const *service_name, ma_service_t **services_manager_service);

MA_CPP(})

#endif /* MA_SERVICES_MANAGER_SERVICE_H_INCLUDED */
