#ifndef MA_PUBSUB_SERVICE_H_INCLUDED
#define MA_PUBSUB_SERVICE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/services/ma_service.h"

MA_CPP(extern "C" {)

typedef struct ma_pubsub_service_s ma_pubsub_service_t, *ma_pubsub_service_h;

ma_error_t ma_pubsub_service_create(char const *service_name, ma_service_t **pubsub_service);

MA_CPP(})

#endif /* MA_PUBSUB_SERVICE_H_INCLUDED */
