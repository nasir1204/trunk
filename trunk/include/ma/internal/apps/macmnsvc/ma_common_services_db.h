#ifndef MA_COMMON_SERVICES_DB_H_INCLUDED
#define MA_COMMON_SERVICES_DB_H_INCLUDED
#include "ma/internal/utils/database/ma_db.h"
#include "ma/internal/utils/database/ma_db_recordset.h"
#include "ma/internal/utils/database/ma_db_statement.h"

MA_CPP(extern "C" {)

/* DDL'S */
#define CREATE_MSGBUS_SERVICES_TABLE_STMT "CREATE TABLE IF NOT EXISTS MA_MSGBUS_SERVICES(SERVICE_NAME TEXT NOT NULL UNIQUE, SERVICE_ID TEXT NOT NULL, PRODUCT_ID TEXT NOT NULL, PROCESS_ID NUMBER, HOST_NAME TEXT NOT NULL, PRIMARY KEY (SERVICE_NAME) ON CONFLICT REPLACE)"
#define CREATE_MSGBUS_SUBSCRIBERS_TABLE_STMT "CREATE TABLE IF NOT EXISTS MA_MSGBUS_SUBSCRIBERS(TOPIC_NAME TEXT NOT NULL, SERVICE_ID TEXT NOT NULL, PRODUCT_ID TEXT NOT NULL, PROCESS_ID NUMBER, HOST_NAME TEXT NOT NULL, PRIMARY KEY (TOPIC_NAME, SERVICE_ID, PRODUCT_ID, PROCESS_ID) ON CONFLICT REPLACE)"

/* creates tables if not exists */
ma_error_t db_add_broker_instance(ma_db_t *db_handle);

ma_error_t db_add_service(ma_db_t *db_handle, const char *service_name, const char *service_id, const char *product_id, ma_uint32_t process_id, const char *host_name);
ma_error_t db_add_subscriber(ma_db_t *db_handle, const char *topic_name, const char *service_id, const char *product_id, ma_uint32_t process_id, const char *host_name);

ma_error_t db_remove_service(ma_db_t *db_handle, const char *service_name) ;
ma_error_t db_remove_subscriber(ma_db_t *db_handle, const char *topic_name, const char *product_id, ma_uint32_t process_id);
ma_error_t db_remove_all_subscribers(ma_db_t *db_handle);
ma_error_t db_remove_all_services(ma_db_t *db_handle);
ma_error_t db_remove_subscribers_by_process_id(ma_db_t *db_handle, const char *process_id);
ma_error_t db_remove_services_by_process_id(ma_db_t *db_handle, const char *process_id);

ma_error_t db_get_all_services(ma_db_t *db_handle, ma_db_recordset_t **db_record);
ma_error_t db_get_all_subscribers(ma_db_t *db_handle, ma_db_recordset_t **db_record);

ma_error_t db_get_subscribers_list(ma_db_t *db_handle, const char *topic_name, ma_db_recordset_t **db_record); 
ma_error_t db_get_services_list(ma_db_t *db_handle, const char *service_name, ma_db_recordset_t **db_record);

MA_CPP(})
#endif  /* MA_COMMON_SERVICES_DB_H_INCLUDED */
