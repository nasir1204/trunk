/*
1. macmnsvc is started as a message bus server.
2. A server can register with ma_broker if it is willing to allow connections from clients (either from same machine or remote)
3. A client can get the name of the named pipe, on which a particular server is running.
*/

#ifndef MA_COMMON_SERVICES_H_INCLUDED
#define MA_COMMON_SERVICES_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_log.h"

MA_CPP(extern "C" {)

typedef struct ma_common_services_s ma_common_services_t, *ma_common_services_h;

struct ma_event_loop_s;

/**
 *  @brief Create common services 
 *  
 */
ma_error_t ma_common_services_create(char const *service_name, ma_logger_t *logger, ma_common_services_t **service) ;

/**
 *  @brief Start common services
 *  
 */
ma_error_t ma_common_services_run(ma_common_services_t *service) ;

/**
 *  @brief Stop common services 
 *  
 */
ma_error_t ma_common_services_stop(ma_common_services_t *service) ; 

/**
 *  @brief Release common services
 *  
 */
ma_error_t ma_common_services_release(ma_common_services_t *service) ;


ma_error_t  ma_common_services_get_event_loop(ma_common_services_t *service, struct ma_event_loop_s **event_loop);

MA_CPP(})
   
#endif /* MA_COMMON_SERVICES_H_INCLUDED */
