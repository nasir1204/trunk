#ifndef MA_ADD_USER_CONTROLLER_H_INCLUDED
#define MA_ADD_USER_CONTROLLER_H_INCLUDED

#include <tnt/component.h>
#include <tnt/componentfactory.h>
#include <tnt/httprequest.h>
#include <tnt/httpreply.h>

#include "ma/ma_common.h"
#include "ma/ma_log.h"
#include "ma/internal/utils/profiler/ma_profiler.h"
#include "ma/internal/utils/profiler/ma_profile_info.h"

#include <iostream>
#include <string>
#include <stdlib.h>
#include <curl/curl.h>

#define MA_AUTUMNIS_ENABLESERVICE_ENDPOINT "/autmunIS/users/enableservice"
using namespace std;

class add_user_controller : public tnt::Component {
  public:
    add_user_controller(){}
    add_user_controller(const tnt::Compident& ci,
        const tnt::Urlmapper& um, tnt::Comploader& cl) {
    }
    unsigned operator() (tnt::HttpRequest& request, tnt::HttpReply& reply, tnt::QueryParams& qparam);
    ma_error_t add(ma_profile_info_t *info, ma_bool_t *res);
};



#endif

