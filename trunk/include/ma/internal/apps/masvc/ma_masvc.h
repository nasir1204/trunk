#ifndef MA_MASVC_H_INCLUDED
#define MA_MASVC_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/logger/ma_file_logger.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/conf/ma_conf_log.h"
#include "ma/internal/services/io/ma_io_service.h"
#include "ma/internal/services/sensor/ma_sensor_service.h"
#include "ma/internal/services/profile/ma_profile_service.h"
#include "ma/internal/services/booking/ma_booking_service.h"
#include "ma/internal/services/mail/ma_mail_service.h"
#include "ma/internal/services/inventory/ma_inventory_service.h"
#include "ma/internal/services/board/ma_board_service.h"
#include "ma/internal/services/seasons/ma_season_service.h"
#include "ma/internal/services/search/ma_search_service.h"

typedef struct service_data_s {
#ifdef MA_WINDOWS
  SERVICE_STATUS_HANDLE     service_status_handle;
  SERVICE_STATUS            service_status;
#endif

  ma_log_filter_t           *filter;
  
  ma_logger_t               *logger;

  ma_io_service_t           *io_service;

  ma_sensor_service_t       *sensor_service;

  ma_profile_service_t      *profile_service;

  ma_booking_service_t      *booking_service;

  ma_mail_service_t         *mail_service;

  ma_inventory_service_t    *inventory_service;

  ma_board_service_t        *board_service;

  ma_search_service_t       *search_service;

  ma_season_service_t       *season_service;


} service_data_t;



#endif

