#ifndef MA_APP_DRIVER_H_INCLUDED
#define MA_APP_DRIVER_H_INCLUDED

#include "ma/ma_log.h"
#include "ma/logger/ma_file_logger.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/utils/threading/ma_thread.h"
#include "ma/internal/services/io/ma_io_service.h"
#include "ma/internal/services/profile/ma_profile_service.h"
#include "ma/internal/services/booking/ma_booking_service.h"
#include "ma/internal/services/sensor/ma_sensor_service.h"
#include "ma/internal/services/mail/ma_mail_service.h"
#include "ma/internal/services/inventory/ma_inventory_service.h"
#include "ma/internal/services/board/ma_board_service.h"
#include "ma/internal/services/seasons/ma_season_service.h"
#include "ma/internal/services/search/ma_search_service.h"
#include "ma/internal/services/board/ma_board_service.h"

class app_driver {
    private:
       ma_io_service_t *io_service;
       ma_profile_service_t *profile_service;
       ma_booking_service_t *booking_service;
       ma_sensor_service_t *sensor_service;
       ma_mail_service_t *mail_service;
       ma_inventory_service_t *inventory_service;
       ma_search_service_t *search_service;
       ma_season_service_t *season_service;
       ma_board_service_t *board_service;

       app_driver();
       app_driver(const app_driver&);
       void *operator new(unsigned long size)throw();
       app_driver& operator=(const app_driver&);
       void operator delete(void *p);
    public:
       static app_driver& get_instance();
       virtual ~app_driver();
       void set_io_service(ma_io_service_t *service);
       void set_booking_service(ma_booking_service_t *service);
       void set_profile_service(ma_profile_service_t *service);
       void set_sensor_service(ma_sensor_service_t *service);
       void set_mail_service(ma_mail_service_t *service);
       void set_inventory_service(ma_inventory_service_t *service);
       void set_board_service(ma_board_service_t *service);
       void set_search_service(ma_search_service_t *service);
       void set_season_service(ma_season_service_t *service);
       ma_io_service_t *get_io_service();
       ma_booking_service_t *get_booking_service();
       ma_profile_service_t *get_profile_service();
       ma_sensor_service_t *get_sensor_service();
       ma_mail_service_t *get_mail_service();
       ma_inventory_service_t *get_inventory_service();
       ma_season_service_t *get_season_service();
       ma_search_service_t *get_search_service();
       ma_board_service_t *get_board_service();
};

#endif //MA_APP_DRIVER_H_INCLUDED
