#ifndef MA_DETAIL_ORDER_CONTROLLER_H_INCLUDED
#define MA_DETAIL_ORDER_CONTROLLER_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_variant.h"
#include "ma/ma_log.h"
#include "ma/internal/utils/consignor/ma_consignor.h"
#include "ma/internal/utils/consignor/ma_consign_info.h"
#include <tnt/component.h>
#include <tnt/componentfactory.h>
#include <tnt/httprequest.h>
#include <tnt/httpreply.h>

#include <iostream>
#include <string>
#include <stdlib.h>

using namespace std;

class detail_order_controller : public tnt::Component {
  public:
    detail_order_controller() {}
    detail_order_controller(const tnt::Compident& ci,
        const tnt::Urlmapper& um, tnt::Comploader& cl) {
    }
    unsigned operator() (tnt::HttpRequest& request, tnt::HttpReply& reply, tnt::QueryParams& qparam);
    ma_error_t get(const char *id, ma_variant_t **payload);
};



#endif

