#ifndef MA_SEASON_PIN_CONTROLLER_H_INCLUDED
#define MA_SEASON_PIN_CONTROLLER_H_INCLUDED

#include <tnt/component.h>
#include <tnt/componentfactory.h>
#include <tnt/httprequest.h>
#include <tnt/httpreply.h>

#include <iostream>
#include <string>
#include <stdlib.h>

#define MA_SEASON_PIN_OPERATION_ADD_STR                    "/add"
#define MA_SEASON_PIN_OPERATION_GET_STR                    "/get"
#define MA_SEASON_PIN_OPERATION_DELETE_STR                 "/delete"
#define MA_SEASON_PIN_OPERATION_DELETE_USER_STR            "/delete_user"
#define MA_SEASON_PIN_OPERATION_REGISTER_STR               "/register"
#define MA_SEASON_PIN_OPERATION_SUBSCRIBE_BOARDS_STR       "/subscribe_boards"
#define MA_SEASON_PIN_OPERATION_BOARD_SELECTION_STR        "/board_selections"
#define MA_SEASON_PIN_OPERATION_QUERY_STR                  "/query"
#define MA_SEASON_PIN_OPERATION_UPDATE_PROFILE_STR         "/update_profile"
#define MA_SEASON_PIN_OPERATION_GET_PROFILE_STR            "/get_profile"
#define MA_SEASON_PIN_OPERATION_ADD_BOARD_STR              "/add_board"
#define MA_SEASON_PIN_OPERATION_AUTHENTICATE_STR           "/authenticate"
#define MA_SEASON_PIN_OPERATION_GET_BY_DAY_STR            "/get_by_day"
#define MA_BOOKING_OPERATION_CREATE_ORDER_STR             "/create_order"
#define MA_BOOKING_OPERATION_DELETE_ORDER_STR             "/delete_order"
#define MA_SEASON_PIN_OPERATION_UNKNOWN_STR                   "NA"

#undef MA_SEASON_PIN_OPERATION_EXPANDER
#define MA_SEASON_PIN_OPERATION_EXPANDER\
    MA_SEASON_PIN_OPERATION_EXPAND(MA_SEASON_PIN_OPERATION_ADD, 1, MA_SEASON_PIN_OPERATION_ADD_STR)\
    MA_SEASON_PIN_OPERATION_EXPAND(MA_SEASON_PIN_OPERATION_GET, 2, MA_SEASON_PIN_OPERATION_GET_STR)\
    MA_SEASON_PIN_OPERATION_EXPAND(MA_SEASON_PIN_OPERATION_DELETE, 3, MA_SEASON_PIN_OPERATION_DELETE_STR)\
    MA_SEASON_PIN_OPERATION_EXPAND(MA_SEASON_PIN_OPERATION_REGISTER, 4, MA_SEASON_PIN_OPERATION_REGISTER_STR)\
    MA_SEASON_PIN_OPERATION_EXPAND(MA_SEASON_PIN_OPERATION_SUBSCRIBE_BOARDS, 5, MA_SEASON_PIN_OPERATION_SUBSCRIBE_BOARDS_STR)\
    MA_SEASON_PIN_OPERATION_EXPAND(MA_SEASON_PIN_OPERATION_QUERY, 6, MA_SEASON_PIN_OPERATION_QUERY_STR)\
    MA_SEASON_PIN_OPERATION_EXPAND(MA_SEASON_PIN_OPERATION_BOARD_SELECTION, 7, MA_SEASON_PIN_OPERATION_BOARD_SELECTION_STR)\
    MA_SEASON_PIN_OPERATION_EXPAND(MA_SEASON_PIN_OPERATION_UPDATE_PROFILE, 8, MA_SEASON_PIN_OPERATION_UPDATE_PROFILE_STR)\
    MA_SEASON_PIN_OPERATION_EXPAND(MA_SEASON_PIN_OPERATION_GET_PROFILE, 9, MA_SEASON_PIN_OPERATION_GET_PROFILE_STR)\
    MA_SEASON_PIN_OPERATION_EXPAND(MA_SEASON_PIN_OPERATION_ADD_BOARD, 10, MA_SEASON_PIN_OPERATION_ADD_BOARD_STR)\
    MA_SEASON_PIN_OPERATION_EXPAND(MA_SEASON_PIN_OPERATION_AUTHENTICATE, 11, MA_SEASON_PIN_OPERATION_AUTHENTICATE_STR)\
    MA_SEASON_PIN_OPERATION_EXPAND(MA_SEASON_PIN_OPERATION_DELETE_USER, 12, MA_SEASON_PIN_OPERATION_DELETE_USER_STR)\
    MA_SEASON_PIN_OPERATION_EXPAND(MA_SEASON_PIN_OPERATION_GET_BY_DAY, 13, MA_SEASON_PIN_OPERATION_GET_BY_DAY_STR)\
    MA_SEASON_PIN_OPERATION_EXPAND(MA_BOOKING_OPERATION_CREATE_ORDER, 14, MA_BOOKING_OPERATION_CREATE_ORDER_STR)\
    MA_SEASON_PIN_OPERATION_EXPAND(MA_BOOKING_OPERATION_DELETE_ORDER, 15, MA_BOOKING_OPERATION_DELETE_ORDER_STR)\
    MA_SEASON_PIN_OPERATION_EXPAND(MA_SEASON_PIN_OPERATION_UNKNOWN, 16, MA_SEASON_PIN_OPERATION_UNKNOWN_STR)

using namespace std;


typedef enum ma_season_pin_operation_type_e {
#define MA_SEASON_PIN_OPERATION_EXPAND(status, index, status_str) status = index,
    MA_SEASON_PIN_OPERATION_EXPANDER
#undef MA_SEASON_PIN_OPERATION_EXPAND
}ma_season_pin_operation_type_t;

class season_pin_controller : public tnt::Component {
    public:
        season_pin_controller() {
        }
        season_pin_controller(const tnt::Compident& ci,
            const tnt::Urlmapper& um, tnt::Comploader& cl) {
        }
        unsigned operator() (tnt::HttpRequest& request, tnt::HttpReply& reply, tnt::QueryParams& qparam);
    private:
        ma_season_pin_operation_type_t get_op_type(const char *op);
        const char* get_op_str(ma_season_pin_operation_type_t type);
       
};



#endif

