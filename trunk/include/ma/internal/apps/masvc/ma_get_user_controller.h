#ifndef MA_GET_USER_CONTROLLER_H_INCLUDED
#define MA_GET_USER_CONTROLLER_H_INCLUDED

#include <tnt/component.h>
#include <tnt/componentfactory.h>
#include <tnt/httprequest.h>
#include <tnt/httpreply.h>

#include "ma/ma_common.h"
#include "ma/ma_log.h"
#include "ma/internal/utils/profiler/ma_profiler.h"
#include "ma/internal/utils/profiler/ma_profile_info.h"

#include <iostream>
#include <string>
#include <stdlib.h>

using namespace std;

class get_user_controller : public tnt::Component {
  public:
    get_user_controller(){}
    get_user_controller(const tnt::Compident& ci,
        const tnt::Urlmapper& um, tnt::Comploader& cl) {
    }
    unsigned operator() (tnt::HttpRequest& request, tnt::HttpReply& reply, tnt::QueryParams& qparam);
    ma_error_t get(const char *id, ma_variant_t **payload);
};



#endif

