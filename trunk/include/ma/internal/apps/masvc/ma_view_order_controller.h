#ifndef MA_VIEW_ORDER_CONTROLLER_H_INCLUDED
#define MA_VIEW_ORDER_CONTROLLER_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_variant.h"
#include "ma/internal/utils/booker/ma_booking_info.h"
#include <tnt/component.h>
#include <tnt/componentfactory.h>
#include <tnt/httprequest.h>
#include <tnt/httpreply.h>

#include <iostream>
#include <string>
#include <stdlib.h>

using namespace std;

class view_order_controller : public tnt::Component {
  public:
    view_order_controller() {}
    view_order_controller(const tnt::Compident& ci,
        const tnt::Urlmapper& um, tnt::Comploader& cl) {
    }
    unsigned operator() (tnt::HttpRequest& request, tnt::HttpReply& reply, tnt::QueryParams& qparam);
    ma_error_t process(const char *crn, ma_variant_t **payload);
};



#endif

