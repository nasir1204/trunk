#ifndef MA_UPDATE_LOCATION_CONTROLLER_H_INCLUDED
#define MA_UPDATE_LOCATION_CONTROLLER_H_INCLUDED

#include <tnt/component.h>
#include <tnt/componentfactory.h>
#include <tnt/httprequest.h>
#include <tnt/httpreply.h>

#include <iostream>
#include <string>
#include <stdlib.h>

using namespace std;

class update_location_controller : public tnt::Component {
  public:
    update_order_controller() {}
    update_location_controller(const tnt::Compident& ci,
        const tnt::Urlmapper& um, tnt::Comploader& cl) {
    }
    static ma_error_t process(ma_booking_info_t *info);
    unsigned operator() (tnt::HttpRequest& request, tnt::HttpReply& reply, tnt::QueryParams& qparam);
};



#endif

