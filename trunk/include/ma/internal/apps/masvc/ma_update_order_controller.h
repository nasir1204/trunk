#ifndef MA_UPDATE_ORDER_CONTROLLER_H_INCLUDED
#define MA_UPDATE_ORDER_CONTROLLER_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/booker/ma_booking_info.h"
#include <tnt/component.h>
#include <tnt/componentfactory.h>
#include <tnt/httprequest.h>
#include <tnt/httpreply.h>

#include <iostream>
#include <string>
#include <stdlib.h>

using namespace std;

class update_order_controller : public tnt::Component {
  public:
    update_order_controller() {}
    update_order_controller(const tnt::Compident& ci,
        const tnt::Urlmapper& um, tnt::Comploader& cl) {
    }
    unsigned operator() (tnt::HttpRequest& request, tnt::HttpReply& reply, tnt::QueryParams& qparam);
private:
    ma_error_t process(ma_booking_info_t *info);
};



#endif

