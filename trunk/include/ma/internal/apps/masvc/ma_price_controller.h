#ifndef MA_PRICE_CONTROLLER_H_INCLUDED
#define MA_PRICE_CONTROLLER_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/booker/ma_booking_info.h"
#include "ma/internal/utils/json/ma_json_utils.h"
#include <tnt/component.h>
#include <tnt/componentfactory.h>
#include <tnt/httprequest.h>
#include <tnt/httpreply.h>

#include <iostream>
#include <string>
#include <stdlib.h>

using namespace std;

#define MA_PARCEL_STR "Parcel"
#define MA_COVER_STR  "Cover"

typedef struct ma_price_info_s ma_price_info_t, *ma_price_info_h;

class price_controller : public tnt::Component {
  public:
    price_controller() {
    }
    price_controller(const tnt::Compident& ci,
        const tnt::Urlmapper& um, tnt::Comploader& cl) {
    }
    unsigned operator() (tnt::HttpRequest& request, tnt::HttpReply& reply, tnt::QueryParams& qparam);
  private:
    ma_error_t process(ma_price_info_t *info, ma_float_t *price);
};



#endif

