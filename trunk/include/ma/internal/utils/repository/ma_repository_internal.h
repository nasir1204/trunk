#ifndef MA_REPOSITORY_INTERNAL_H_INCLUDED
#define MA_REPOSITORY_INTERNAL_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/proxy/ma_proxy.h"
#include "ma/proxy/ma_proxy_config.h"
#include "ma/repository/ma_repository.h"
#include "ma/repository/ma_repository_list.h"
#include "ma/internal/utils/threading/ma_atomic.h"

MA_CPP(extern "C" {)
struct ma_context_s ;

struct ma_http_repository_params_s{
	/*repo server*/
    char *server_fqdn ;
	char *path ;
    ma_int32_t port;
    
	/*auth*/       
    char *auth_user ;
    char *auth_passwd ;	
};

typedef enum ma_repository_rank_type_e {
    MA_REPOSITORY_RANK_BY_PING_TIME = 0,
    MA_REPOSITORY_RANK_BY_SUBNET_DISTANCE,
    MA_REPOSITORY_RANK_BY_SITELIST_ORDER,
	MA_REPOSITORIES_FOR_RANKING_STMT,
    MA_REPOSITORY_RANK_BY_END
} ma_repository_rank_type_t ;

struct ma_ftp_repository_params_s{
	/*repo server*/
    char *server_fqdn ;
	char *path ;
    ma_int32_t port;
    
	/*auth*/       
    char *auth_user ;
    char *auth_passwd ;	
};

struct ma_spipe_repository_params_s{
	/*repo server*/
    char *server_fqdn ;
    char *server_ip ;
	char *server_name ;
	char *path ;
    ma_int32_t port;
    ma_int32_t ssl_port;
	/*auth*/       
    char *auth_user ;
    char *auth_passwd ;	
};

struct ma_sa_repository_params_s{
	/*repo server*/
	char *server_fqdn ;
	char *server_ip ;
	char *server_name ;
	char *path ;
	ma_int32_t port;
	ma_int32_t ssl_port;
	/*auth*/       
	char *auth_user ;
	char *auth_passwd ;	
};

struct ma_unc_repository_params_s{
	/*repo server*/
    char *server_fqdn ;
	char *path ;
    
	/*auth*/    
    char *domain ;
    char *auth_user ;
    char *auth_passwd ;	
};

struct ma_local_repository_params_s{
	/*repo server*/    
	char *path ;    
    
	/*auth*/    
    char *domain ;
    char *auth_user ;
    char *auth_passwd ;	
};

typedef union ma_repository_params_s{
	struct ma_http_repository_params_s	http_repository;
	struct ma_ftp_repository_params_s	ftp_repository;
	struct ma_unc_repository_params_s	ucn_repository;
	struct ma_local_repository_params_s local_repository;
	struct ma_spipe_repository_params_s spipe_repository;
	struct ma_sa_repository_params_s	sa_repository;
}ma_repository_params_t;


struct ma_repository_s {
    char						*name ;
    ma_repository_type_t		repo_type ;
    ma_repository_url_type_t	url_type ;
	ma_repository_auth_t		auth_type;
	ma_repository_namespace_t	repo_namespace ;
	ma_proxy_usage_type_t		proxy_usage;
	ma_repository_state_t		state;

	/*repo settings*/
    ma_bool_t					enabled  ;
    
	/*sort param*/
    ma_int32_t					ping_time ;
    ma_int32_t					subnet_distance ;
    ma_int32_t					sitelist_order ;

	ma_repository_params_t		params;	
	
	ma_atomic_counter_t			ref_count ;
} ;

MA_REPOSITORY_API ma_error_t   ma_repository_list_from_variant(ma_variant_t *data, ma_repository_list_t **repository_list);

MA_REPOSITORY_API ma_error_t   ma_repository_list_as_variant(ma_repository_list_t *repository_list, ma_variant_t **variant);

MA_REPOSITORY_API ma_error_t   ma_repository_list_copy(const ma_repository_list_t *repository_list_from, ma_repository_list_t **repository_list_to);

MA_REPOSITORY_API ma_error_t   ma_repository_as_variant(ma_repository_t *repository, ma_variant_t **repo_variant);

MA_REPOSITORY_API ma_error_t   ma_repository_from_variant(ma_variant_t *repo_variant, ma_repository_t **repository);

struct ma_repository_list_s {
	ma_array_t *repo;
};


MA_CPP(})

#endif  /* MA_REPOSITORY_INTERNAL_H_INCLUDED */

