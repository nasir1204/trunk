#ifndef MA_SITELIST_H_INCLUDED
#define MA_SITELIST_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_log.h"
#include "ma/ma_datastore.h"
#include "ma/internal/utils/database/ma_db.h"
#include "ma/internal/utils/macrypto/ma_crypto.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#define MA_SITELISTS_ROOT                           "ns:SiteLists"
#define MA_SITELISTS_CERTIFICATE_PATH               "ns:SiteLists/CaBundle/CaCertificate"
#define MA_SITELISTS_SPIPE_SITE_PATH                "ns:SiteLists/SiteList/SpipeSite"

#define MA_SITELISTS_GLOBAL_VERSION                 "GlobalVersion"
#define MA_SITELISTS_LOCAL_VERSION                  "LocalVersion"
#define MA_SITELISTS_CERTIFICATE                    "CaCertificate"
#define MA_SITELISTS_SPIPE                          "SpipeSite"

/* Sites Attributes */
#define MA_SITE_TYPE	                             "Type"
#define MA_SITE_NAME                                "Name"
/* ePO server version stored in SPIPE site */
#define MA_SITE_SERVER_VERSION                      "Version"
#define MA_SITE_SERVER_FQDN                         "Server"
#define MA_SITE_SERVER_SHORT						 "ServerName"
#define MA_SITE_SERVER_IP                           "ServerIP"
#define MA_SITE_SERVER_SSL_PORT						"SecurePort"
#define MA_SITE_SERVER_PORT							"Port"
#define MA_SITE_ORDER                               "Order"

#define MA_EPO_SERVER_LIST_OR_PORT_SEPARATOR_STR				"|"
#define MA_EPO_SERVER_LIST_OR_PORT_SEPARATOR_C					'|'


MA_CPP(extern "C" {)

typedef struct ma_sitelist_handler_s{
	ma_crypto_t		*crypto;
	ma_ds_t			*ma_ds;
	ma_db_t			*ma_db;
	ma_logger_t		*logger;
	const char      *data_path;
}ma_sitelist_handler_t;

MA_REPOSITORY_API ma_error_t ma_sitelist_handler_init(ma_sitelist_handler_t *handler, ma_crypto_t *crypto, ma_ds_t *ds, ma_db_t *db, ma_logger_t *logger, const char *datapath);

MA_REPOSITORY_API ma_error_t ma_sitelist_handler_import_xml(ma_sitelist_handler_t *handler, char *sitelist_buffer, size_t buffer_size, ma_bool_t is_spipe_request);

typedef enum ma_server_list_filter_e {
	MA_SERVER_LIST_BY_IP = 0x01,
	MA_SERVER_LIST_BY_SERVER = 0x02,
	MA_SERVER_LIST_BY_FQDN = 0x04,
	MA_SERVER_LIST_ALL = 0x07
} ma_server_list_filter_t;

MA_REPOSITORY_API ma_error_t ma_sitelist_get_server_list(ma_db_t *ma_db, ma_server_list_filter_t filter, ma_temp_buffer_t *list);

MA_REPOSITORY_API ma_error_t ma_sitelist_get_server_port_list_get(ma_db_t *ma_db, ma_temp_buffer_t *list);


MA_CPP(})

#endif  /* MA_SITELIST_HANDLER_H_INCLUDED */

