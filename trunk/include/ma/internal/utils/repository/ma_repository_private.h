#ifndef MA_REPOSITORY_PRIVATE_H_INCLUDED
#define MA_REPOSITORY_PRIVATE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/repository/ma_repository_internal.h"

MA_CPP(extern "C" {)

struct ma_context_s ;
struct ma_repository_list_s ;

MA_REPOSITORY_API ma_error_t   ma_repository_get_repositories(struct ma_context_s *context, const char *filter, struct ma_repository_list_s  **repository_list) ;

MA_CPP(})

#endif	/* MA_REPOSITORY_PRIVATE_H_INCLUDED */