#ifndef MA_REPOSITORY_DB_MANAGER_H_INCLUDED
#define MA_REPOSITORY_DB_MANAGER_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/datastore/ma_ds.h"
#include "ma/internal/utils/database/ma_db.h"
#include "ma/repository/ma_repository.h"
#include "ma/internal/utils/database/ma_db_recordset.h"
#include "ma/internal/utils/repository/ma_repository_internal.h"
MA_CPP(extern "C" {)

struct ma_crypto_s ;

/* get repositories filter url_type.state  additional defs for internal purpose*/
#define MA_REPOSITORY_GET_REPOSITORIES_FILTER_FOR_RANKING						"*.*.rank"

/* AGENT_REPOSITORIES - columns

1 NAME TEXT NOT NULL UNIQUE, 
2 REPO_TYPE INTEGER NOT NULL, 
3 URL_TYPE INTEGER NOT NULL,
4 NAMESPACE INTEGER NOT NULL, 
5 PROXY_USAGE INTEGER NOT NULL,
6 AUTH_TYPE INTEGER NOT NULL,
7 ENABLED INTEGER NOT NULL,
8 SERVER_FQDN TEXT, 
9 SERVER_IP TEXT,
10 SERVER_NAME TEXT,
11 PORT INTEGER,
12 SSL_PORT INTEGER,
13 PATH TEXT NOT NULL, 
14 DOMAIN TEXT, 
15 AUTH_USER TEXT, 
16 AUTH_PASSWD TEXT, 
17 IS_PASSWD_ENCRYPTED INTEGER NOT NULL, 
18 PING_TIME INTEGER NOT NULL, 
19 SUBNET_DISTANCE INTEGER NOT NULL, 
20 SITELIST_ORDER INTEGER NOT NULL,
21 STATE INTEGER NOT NULL,

*/

#define MA_REPO_TABLE_FIELD_BASE					  0
#define MA_REPO_TABLE_FILED_NAME					  MA_REPO_TABLE_FIELD_BASE + 1
#define MA_REPO_TABLE_FILED_REPO_TYPE				  MA_REPO_TABLE_FIELD_BASE + 2
#define MA_REPO_TABLE_FILED_URL_TYPE				  MA_REPO_TABLE_FIELD_BASE + 3
#define MA_REPO_TABLE_FILED_NAMESPACE				  MA_REPO_TABLE_FIELD_BASE + 4
#define MA_REPO_TABLE_FILED_PROXY_USAGE				  MA_REPO_TABLE_FIELD_BASE + 5
#define MA_REPO_TABLE_FILED_AUTH_TYPE				  MA_REPO_TABLE_FIELD_BASE + 6
#define MA_REPO_TABLE_FILED_ENABLED					  MA_REPO_TABLE_FIELD_BASE + 7
#define MA_REPO_TABLE_FILED_SERVER_FQDN				  MA_REPO_TABLE_FIELD_BASE + 8
#define MA_REPO_TABLE_FILED_SERVER_IP				  MA_REPO_TABLE_FIELD_BASE + 9
#define MA_REPO_TABLE_FILED_SERVER_NAME				  MA_REPO_TABLE_FIELD_BASE + 10
#define MA_REPO_TABLE_FILED_PORT					  MA_REPO_TABLE_FIELD_BASE + 11
#define MA_REPO_TABLE_FILED_SSL_PORT				  MA_REPO_TABLE_FIELD_BASE + 12
#define MA_REPO_TABLE_FILED_PATH					  MA_REPO_TABLE_FIELD_BASE + 13
#define MA_REPO_TABLE_FILED_DOMAIN					  MA_REPO_TABLE_FIELD_BASE + 14
#define MA_REPO_TABLE_FILED_AUTH_USER				  MA_REPO_TABLE_FIELD_BASE + 15
#define MA_REPO_TABLE_FILED_AUTH_PASSWD				  MA_REPO_TABLE_FIELD_BASE + 16
#define MA_REPO_TABLE_FILED_IS_PASSWD_ENCRYPTED		  MA_REPO_TABLE_FIELD_BASE + 17
#define MA_REPO_TABLE_FILED_PING_TIME				  MA_REPO_TABLE_FIELD_BASE + 18
#define MA_REPO_TABLE_FILED_SUBNET_DISTANCE			  MA_REPO_TABLE_FIELD_BASE + 19
#define MA_REPO_TABLE_FILED_SITELIST_ORDER			  MA_REPO_TABLE_FIELD_BASE + 20
#define MA_REPO_TABLE_FILED_STATE					  MA_REPO_TABLE_FIELD_BASE + 21




/* Repository DB API's*/
MA_REPOSITORY_API ma_error_t ma_repository_db_create_schema(ma_db_t *db_handle) ;

MA_REPOSITORY_API ma_error_t ma_repository_db_add_repository(ma_db_t *, ma_repository_t *) ;

MA_REPOSITORY_API ma_error_t ma_repository_db_remove_all_repositories(ma_db_t *) ;

MA_REPOSITORY_API ma_error_t ma_repository_db_remove_repository_by_name(ma_db_t *, const char *name) ;

MA_REPOSITORY_API ma_error_t ma_repository_db_set_repository_ping_count(ma_db_t *, const char *name, ma_int32_t ping_count) ;

MA_REPOSITORY_API ma_error_t ma_repository_db_set_repository_subnet_distance(ma_db_t *, const char *name, ma_int32_t subnet_distance) ;

MA_REPOSITORY_API ma_error_t ma_repository_db_get_repository_ping_count(ma_db_t *, const char *name, ma_int32_t *ping_count) ;

MA_REPOSITORY_API ma_error_t ma_repository_db_get_repository_subnet_distance(ma_db_t *, const char *name, ma_int32_t *subnet_distance) ;

MA_REPOSITORY_API ma_error_t ma_repository_db_set_all_repositories_enabled(ma_db_t *, ma_bool_t enabled) ;

MA_REPOSITORY_API ma_error_t ma_repository_db_set_repository_enabled(ma_db_t *, const char *name, ma_bool_t enabled) ;

MA_REPOSITORY_API ma_error_t ma_repository_db_set_repository_sitelistorder(ma_db_t *, const char *name, ma_int32_t sitelist_order) ;

MA_REPOSITORY_API ma_error_t ma_repository_db_get_repository_fqdn(ma_db_t *db_handle, const char *repo_name, char **server) ;

MA_REPOSITORY_API ma_error_t ma_repository_db_get_repository_ip(ma_db_t *db_handle, const char *repo_name, char **server) ;

MA_REPOSITORY_API ma_error_t ma_repository_db_get_repository_server(ma_db_t *db_handle, const char *repo_name, char **server) ;

MA_REPOSITORY_API ma_error_t ma_repository_db_get_repository_path(ma_db_t *db_handle, const char *repo_name, char **path) ;

MA_REPOSITORY_API ma_error_t ma_repository_db_get_repositories(ma_db_t *db_handle, struct ma_crypto_s *crypto,const char *filter, ma_repository_rank_type_t repository_rank, ma_repository_list_t **repository_list) ;

MA_REPOSITORY_API ma_error_t ma_repository_db_get_spipe_repositories(ma_db_t *db_handle, ma_db_recordset_t **db_record) ;

MA_REPOSITORY_API ma_error_t ma_repository_db_set_local_repositories(ma_db_t *) ;

MA_REPOSITORY_API ma_error_t ma_repository_db_remove_repositories_by_state(ma_db_t *db_handle);

MA_REPOSITORY_API ma_error_t ma_repository_db_remove_all_global_repositories(ma_db_t *db_handle);

MA_REPOSITORY_API ma_error_t ma_repository_db_remove_all_local_repositories(ma_db_t *db_handle) ;

MA_REPOSITORY_API ma_error_t ma_repository_db_set_all_repository_state(ma_db_t *db_handle, ma_repository_state_t state);

MA_REPOSITORY_API ma_error_t ma_repository_db_get_url_type_by_name(ma_db_t *db_handle, const char *repo_name, ma_repository_url_type_t *url_type);
        
MA_REPOSITORY_API ma_error_t ma_repository_db_get_epo_site_enable_state(ma_db_t *db_handle, ma_bool_t *enable_state) ;

MA_REPOSITORY_API ma_error_t ma_repository_db_set_ah_sites_enable_state(ma_db_t *db_handle, ma_bool_t enable_state) ;

MA_REPOSITORY_API ma_error_t ma_repository_db_is_site_exists(ma_db_t *db_handle, const char *name, ma_repository_url_type_t url_type, ma_bool_t *exists) ;

/*Get the repository policies.*/
MA_REPOSITORY_API ma_error_t ma_repository_db_get_policy(ma_db_t *db_handle, const char *product, const char *timestamp, ma_db_recordset_t **db_record);

/* It returns the count of sties for specified type and server nIP/Name/FQDN */
MA_REPOSITORY_API ma_error_t ma_repository_db_get_url_type_by_server(ma_db_t *db_handle, const char *server, ma_db_recordset_t **db_record);

/*Sets ping time, subnet distance and state */
MA_REPOSITORY_API ma_error_t   ma_repository_set_icmp_attributes(ma_db_t *db, ma_repository_rank_type_t rank_type, ma_uint32_t max_pingtime, ma_uint32_t subnetdistance, ma_repository_t *repository);

MA_REPOSITORY_API ma_error_t ma_repository_db_get_repository_state(ma_db_t *db_handle, const char *repo_name, ma_repository_state_t *state);

MA_REPOSITORY_API ma_error_t ma_repository_db_mark_remove_all_global_repositories(ma_db_t *db_handle);

MA_REPOSITORY_API ma_error_t ma_repository_db_remove_all_global_repositories_by_state(ma_db_t *db_handle);

MA_REPOSITORY_API ma_error_t ma_repository_get_non_sitelist_xml_attr(ma_db_t *db_handle, const char *repo_name, int *enabled, int *ping_time, int *subnet, int *state);

MA_REPOSITORY_API ma_error_t ma_repository_db_reset_all_repository_icmp_attr(ma_db_t *db_handle, int ping_time, int subnet);

MA_REPOSITORY_API ma_error_t ma_repository_db_reset_all_repository_ping_attr(ma_db_t *db_handle, int ping_time);

MA_REPOSITORY_API ma_error_t ma_repository_db_reset_all_repository_subnet_dist_attr(ma_db_t *db_handle, int subnet);

MA_CPP(})
#endif  /* MA_REPOSITORY_DB_MANAGER_H_INCLUDED */

