#ifndef MA_ICMP_H_INCLUDED
#define MA_ICMP_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_log.h"
#include "uv.h"
#include "ma/internal/utils/datastructures/ma_queue.h"
#include "ma/internal/utils/repository/ma_repository_internal.h"
MA_CPP(extern "C" {)

typedef struct ma_icmp_context_s ma_icmp_context_t;
typedef struct ma_icmp_callback_s ma_icmp_callback_t;

typedef struct ma_icmp_policy_s {
    ma_repository_rank_type_t	rank_type ;
    ma_uint16_t                 max_ping_time ;
    ma_uint16_t                 max_subnet_distance ;
}ma_icmp_policy_t ;

typedef struct ma_icmp_repo_node_list_s {
	char								*name;
	char								*url;
	ma_bool_t							is_numeric_url; /*Since DB stores ip in fqdn column instead of ip column (this is ePO bug), 
														icmp module handle it locally. So time being this attribute is not in use. */
	ma_queue_t							qlink;
}ma_icmp_repo_node_list_t;

typedef struct ma_icmp_methods_s {
    ma_error_t (*get)(void *icmp_context, ma_icmp_repo_node_list_t **icmp_url_list); /*Get input: list of urls */
	ma_error_t (*update)(void *icmp_callback); /*Update output to corresponding url/name */
    ma_error_t (*stop)(void *icmp_callback); /*inform caller: I am done, call icmp context & your won memory*/
}ma_icmp_methods_t;

struct ma_icmp_callback_s {
    char						*name ;
	ma_uint16_t					ping_time;
	ma_uint16_t					subnet_distance;
	ma_bool_t					computation_finishied;
	ma_icmp_context_t			*icmp_context;
};

ma_error_t ma_icmp_repo_node_list_create(ma_icmp_repo_node_list_t **icmp_url_list);
ma_error_t ma_icmp_repo_node_list_init(ma_icmp_repo_node_list_t *icmp_url_list, char *name, char *url, ma_bool_t is_numeric_url);
ma_error_t ma_icmp_repo_node_list_release(ma_icmp_repo_node_list_t *icmp_url_list);

ma_error_t ma_icmp_policy_create(ma_icmp_policy_t *icmp_policy, ma_uint16_t max_ping_time, ma_uint16_t max_ttl_value, ma_repository_rank_type_t rank_type);

ma_error_t ma_icmp_context_create(ma_icmp_context_t **icmp_context, void *data);
ma_error_t ma_icmp_context_set_method(ma_icmp_context_t *icmp_context, ma_icmp_methods_t *methods);

ma_error_t ma_icmp_context_set_logger(ma_icmp_context_t *icmp_context, ma_logger_t	*logger);
ma_error_t ma_icmp_context_set_uv_loop(ma_icmp_context_t *icmp_context, uv_loop_t *uv_loop);
ma_error_t ma_icmp_context_set_policy(ma_icmp_context_t *icmp_context, ma_icmp_policy_t icmp_policy);
ma_error_t ma_icmp_context_set_data(ma_icmp_context_t *icmp_context, void *data);

ma_error_t ma_icmp_context_get_policy(ma_icmp_context_t *icmp_context, ma_icmp_policy_t	*icmp_policy);
ma_error_t ma_icmp_context_get_methods(ma_icmp_context_t *icmp_context, ma_icmp_methods_t *methods);
void* ma_icmp_context_get_data(ma_icmp_context_t *icmp_context);

ma_error_t ma_icmp_computation_start(ma_icmp_context_t *icmp_context);
void ma_icmp_computation_stop(ma_icmp_context_t **icmp_context);

MA_CPP(})
#endif /*MA_ICMP_H_INCLUDED*/

