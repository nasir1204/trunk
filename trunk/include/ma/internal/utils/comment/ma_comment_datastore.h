#ifndef MA_COMMENT_DATASTORE_H_INCLUDED
#define MA_COMMENT_DATASTORE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/comment/ma_comment.h"
#include "ma/internal/utils/comment/ma_comment_info.h"
#include "ma/ma_datastore.h"
#include "ma/datastore/ma_ds_database.h"

MA_CPP(extern "C" {)
MA_COMMENT_API ma_error_t ma_comment_datastore_read(ma_comment_t *comment, ma_ds_t *datastore, const char *ds_path);

MA_COMMENT_API ma_error_t ma_comment_datastore_get(ma_comment_t *comment, ma_ds_t *datastore, const char *ds_path, const char *comment_id, ma_comment_info_t **info);

MA_COMMENT_API ma_error_t ma_comment_datastore_write(ma_comment_t *comment, ma_comment_info_t *info, ma_ds_t *datastore, const char *ds_path);

MA_COMMENT_API ma_error_t ma_comment_datastore_write_variant(ma_comment_t *comment, const char *comment_id, ma_ds_t *datastore, const char *ds_path, ma_variant_t *var);

MA_COMMENT_API ma_error_t ma_comment_datastore_remove_comment(const char *comment, ma_ds_t *datastore, const char *ds_path);

MA_COMMENT_API ma_error_t ma_comment_datastore_clear(ma_ds_t *datastore, const char *ds_path);

MA_CPP(})


#endif /* MA_COMMENT_DATASTORE_H_INCLUDED */

