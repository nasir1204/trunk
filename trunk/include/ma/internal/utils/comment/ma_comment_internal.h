#ifndef MA_COMMENT_INTERNAL_H_INCLUDED
#define MA_COMMENT_INTERNAL_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_variant.h"
#include "ma/scheduler/ma_triggers.h"
#include "ma/internal/utils/json/ma_json_utils.h"

MA_CPP(extern "C" {)

typedef struct ma_comment_internal_s ma_comment_internal_t, *ma_comment_internal_h;

MA_COMMENT_API ma_error_t ma_comment_internal_create(ma_comment_internal_t **self);
MA_COMMENT_API ma_error_t ma_comment_internal_release(ma_comment_internal_t *self);

/* setter */
MA_COMMENT_API ma_error_t ma_comment_internal_set_date(ma_comment_internal_t *self, const char *date);
MA_COMMENT_API ma_error_t ma_comment_internal_set_comment(ma_comment_internal_t *self, const char *comment);
MA_COMMENT_API ma_error_t ma_comment_internal_set_id(ma_comment_internal_t *self, const char *id);
MA_COMMENT_API ma_error_t ma_comment_internal_set_email(ma_comment_internal_t *self, const char *user);

MA_COMMENT_API ma_error_t ma_comment_internal_add(ma_comment_internal_t *info);

/* getter */
MA_COMMENT_API ma_error_t ma_comment_internal_get_date(ma_comment_internal_t *self, const char **date);
MA_COMMENT_API ma_error_t ma_comment_internal_get_comment(ma_comment_internal_t *self, const char **comment);
MA_COMMENT_API ma_error_t ma_comment_internal_get_id(ma_comment_internal_t *self, const char **id);
MA_COMMENT_API ma_error_t ma_comment_internal_get_email(ma_comment_internal_t *self, const char **user);
MA_COMMENT_API ma_error_t ma_comment_internal_get_json(ma_comment_internal_t *self, ma_json_t **json);

MA_COMMENT_API ma_error_t ma_comment_internal_convert_to_variant(ma_comment_internal_t *self, ma_variant_t **variant);
MA_COMMENT_API ma_error_t ma_comment_internal_convert_from_variant(ma_variant_t *variant, ma_comment_internal_t **self);

MA_CPP(})

#endif /* MA_COMMENT_INTERNAL_H_INCLUDED */
