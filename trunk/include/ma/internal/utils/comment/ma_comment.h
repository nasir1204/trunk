#ifndef MA_COMMENT_H_INCLUDED
#define MA_COMMENT_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/comment/ma_comment_info.h"
#include "ma/ma_datastore.h"
#include "ma/datastore/ma_ds_database.h"
#include "ma/ma_log.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_variant.h"


MA_CPP(extern "C" {)

typedef struct ma_comment_s ma_comment_t, *ma_comment_h;


MA_COMMENT_API ma_error_t ma_comment_create(ma_msgbus_t *msgbus, ma_ds_t *datastore, const char *base_path, ma_comment_t **comment);

MA_COMMENT_API ma_error_t ma_comment_start(ma_comment_t *comment);

MA_COMMENT_API ma_error_t ma_comment_stop(ma_comment_t *comment);

MA_COMMENT_API ma_error_t ma_comment_add(ma_comment_t *comment, ma_comment_info_t *info);

MA_COMMENT_API ma_error_t ma_comment_add_variant(ma_comment_t *comment, const char *comment_id, ma_variant_t *var) ;

MA_COMMENT_API ma_error_t ma_comment_delete(ma_comment_t *comment, const char *comment_id);

MA_COMMENT_API ma_error_t ma_comment_delete_all(ma_comment_t *comment);

MA_COMMENT_API ma_error_t ma_comment_get(ma_comment_t *comment, const char *comment_id, ma_comment_info_t **info);

MA_COMMENT_API ma_error_t ma_comment_set_logger(ma_logger_t *logger);

MA_COMMENT_API ma_error_t ma_comment_get_msgbus(ma_comment_t *comment, ma_msgbus_t **msgbus);

MA_COMMENT_API ma_error_t ma_comment_release(ma_comment_t *comment);


MA_CPP(})

#endif /* MA_COMMENT_H_INCLUDED */
