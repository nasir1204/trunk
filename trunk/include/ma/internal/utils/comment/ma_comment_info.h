#ifndef MA_COMMENT_INFO_H_INCLUDED
#define MA_COMMENT_INFO_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_variant.h"
#include "ma/scheduler/ma_triggers.h"
#include "ma/internal/utils/json/ma_json_utils.h"
#include "ma/internal/utils/comment/ma_comment_internal.h"

MA_CPP(extern "C" {)

typedef struct ma_comment_info_s ma_comment_info_t, *ma_comment_info_h;

/* comment info ctor */
MA_COMMENT_API ma_error_t ma_comment_info_create(ma_comment_info_t **info);

/* comment info dtor */
MA_COMMENT_API ma_error_t ma_comment_info_release(ma_comment_info_t *info);

/* setter */
MA_COMMENT_API ma_error_t ma_comment_info_set_id(ma_comment_info_t *info, const char *id);
MA_COMMENT_API ma_error_t ma_comment_info_set_comment(ma_comment_info_t *info, const char *id, ma_comment_internal_t *comment);

MA_COMMENT_API ma_error_t ma_comment_info_add(ma_comment_info_t *info);
MA_COMMENT_API ma_error_t ma_comment_info_remove(ma_comment_info_t *info, const char *id);

/* getter */
MA_COMMENT_API ma_error_t ma_comment_info_get_id(ma_comment_info_t *info, const char **id);
MA_COMMENT_API ma_error_t ma_comment_info_get_json(ma_comment_info_t *info, ma_json_t **json);
MA_COMMENT_API ma_error_t ma_comment_info_get_comment(ma_comment_info_t *info, const char *id, ma_comment_internal_t **comment);
MA_COMMENT_API ma_error_t ma_comment_info_get_comments(ma_comment_info_t *info, const ma_comment_internal_t ***comments, size_t *size);

/* convert comment info to variant  and vice versa */
MA_COMMENT_API ma_error_t ma_comment_info_convert_to_variant(ma_comment_info_t *info, ma_variant_t **variant);
MA_COMMENT_API ma_error_t ma_comment_info_convert_from_variant(ma_variant_t *variant, ma_comment_info_t **info);

/* convert comment info to json and vice versa */
MA_COMMENT_API ma_error_t ma_comment_info_convert_to_json(ma_comment_info_t *info, ma_json_t **json);


MA_CPP(})

#endif /* MA_COMMENT_INFO_H_INCLUDED */

