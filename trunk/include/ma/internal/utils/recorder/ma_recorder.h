#ifndef MA_RECORDER_H_INCLUDED
#define MA_RECORDER_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/recorder/ma_recorder_info.h"
#include "ma/ma_datastore.h"
#include "ma/datastore/ma_ds_database.h"
#include "ma/ma_log.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_variant.h"


MA_CPP(extern "C" {)

typedef struct ma_recorder_s ma_recorder_t, *ma_recorder_h;


MA_RECORDER_API ma_error_t ma_recorder_create(ma_msgbus_t *msgbus, ma_ds_t *datastore, const char *base_path, ma_recorder_t **recorder);

MA_RECORDER_API ma_error_t ma_recorder_start(ma_recorder_t *recorder);

MA_RECORDER_API ma_error_t ma_recorder_stop(ma_recorder_t *recorder);

MA_RECORDER_API ma_error_t ma_recorder_add(ma_recorder_t *recorder, ma_recorder_info_t *info);

MA_RECORDER_API ma_error_t ma_recorder_add_variant(ma_recorder_t *recorder, const char *recorder_id, ma_variant_t *var) ;

MA_RECORDER_API ma_error_t ma_recorder_enumerate_order_until_date(ma_recorder_t *recorder, const char *from, const char *to, ma_variant_t **var);

MA_RECORDER_API ma_error_t ma_recorder_delete_all(ma_recorder_t *recorder);

MA_RECORDER_API ma_error_t ma_recorder_delete(ma_recorder_t *recorder, const char *recorder_id);

MA_RECORDER_API ma_error_t ma_recorder_get(ma_recorder_t *recorder, const char *recorder_id, ma_recorder_info_t **info);

MA_RECORDER_API ma_error_t ma_recorder_get_all(ma_recorder_t *recorder, const char *recorder_id, ma_variant_t **var);

MA_RECORDER_API ma_error_t ma_recorder_set_logger(ma_logger_t *logger);

MA_RECORDER_API ma_error_t ma_recorder_get_msgbus(ma_recorder_t *recorder, ma_msgbus_t **msgbus);

MA_RECORDER_API ma_error_t ma_recorder_release(ma_recorder_t *recorder);


MA_CPP(})

#endif /* MA_RECORDER_H_INCLUDED */

