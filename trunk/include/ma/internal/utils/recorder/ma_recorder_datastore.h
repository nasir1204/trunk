#ifndef MA_RECORDER_DATASTORE_H_INCLUDED
#define MA_RECORDER_DATASTORE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/recorder/ma_recorder.h"
#include "ma/internal/utils/recorder/ma_recorder_info.h"
#include "ma/ma_datastore.h"
#include "ma/datastore/ma_ds_database.h"
#include "ma/ma_variant.h"

MA_CPP(extern "C" {)
MA_RECORDER_API ma_error_t ma_recorder_datastore_read(ma_recorder_t *recorder, ma_ds_t *datastore, const char *ds_path);

MA_RECORDER_API ma_error_t ma_recorder_datastore_get(ma_recorder_t *recorder, ma_ds_t *datastore, const char *ds_path, const char *recorder_id, ma_recorder_info_t **info);

MA_RECORDER_API ma_error_t ma_recorder_datastore_get_all(ma_recorder_t *recorder, ma_ds_t *datastore, const char *ds_path, const char *recorder_id, ma_variant_t **var);

MA_RECORDER_API ma_error_t ma_recorder_datastore_write(ma_recorder_t *recorder, ma_recorder_info_t *info, ma_ds_t *datastore, const char *ds_path);

MA_RECORDER_API ma_error_t ma_recorder_datastore_write_variant(ma_recorder_t *recorder, const char *recorder_id, ma_ds_t *datastore, const char *ds_path, ma_variant_t *var);

MA_RECORDER_API ma_error_t ma_recorder_datastore_remove_recorder(const char *recorder, ma_ds_t *datastore, const char *ds_path);

MA_RECORDER_API ma_error_t ma_recorder_datastore_clear(ma_ds_t *datastore, const char *ds_path);

MA_CPP(})


#endif /* MA_RECORDER_DATASTORE_H_INCLUDED */

