#ifndef MA_RECORDER_INFO_H_INCLUDED
#define MA_RECORDER_INFO_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_variant.h"
#include "ma/internal/utils/profiler/ma_profile_info.h"
#include "ma/scheduler/ma_task.h"


MA_CPP(extern "C" {)

typedef struct ma_recorder_info_s ma_recorder_info_t, *ma_recorder_info_h;

/* ctor */
MA_RECORDER_API ma_error_t ma_recorder_info_create(ma_recorder_info_t **info);

/* dtor */
MA_RECORDER_API ma_error_t ma_recorder_info_release(ma_recorder_info_t *info);

MA_RECORDER_API ma_error_t ma_recorder_info_set_pending_orders_map(ma_recorder_info_t *info, ma_table_t *table);
MA_RECORDER_API ma_error_t ma_recorder_info_set_completed_orders_map(ma_recorder_info_t *info, ma_table_t *table);

MA_RECORDER_API ma_error_t ma_recorder_info_add_pending_order(ma_recorder_info_t *info, const char *contact, const char *order);
MA_RECORDER_API ma_error_t ma_recorder_info_remove_pending_order(ma_recorder_info_t *info, const char *contact, const char *order);
MA_RECORDER_API ma_error_t ma_recorder_info_pending_order_list_size(ma_recorder_info_t *info, const char *contact, size_t *size);

MA_RECORDER_API ma_error_t ma_recorder_info_add_completed_order(ma_recorder_info_t *info, const char *contact, const char *order);
MA_RECORDER_API ma_error_t ma_recorder_info_remove_completed_order(ma_recorder_info_t *info, const char *contact, const char *order);
MA_RECORDER_API ma_error_t ma_recorder_info_completed_order_list_size(ma_recorder_info_t *info, const char *contact, size_t *size);

/* setter */
MA_RECORDER_API ma_error_t ma_recorder_info_set_date(ma_recorder_info_t *info, const char *date);
MA_RECORDER_API ma_error_t ma_recorder_info_set_pending_orders_map(ma_recorder_info_t *info, ma_table_t *table);
MA_RECORDER_API ma_error_t ma_recorder_info_set_completed_orders_map(ma_recorder_info_t *info, ma_table_t *table);

/* getters */
MA_RECORDER_API ma_error_t ma_recorder_info_get_date(ma_recorder_info_t *info, const char **date);
MA_RECORDER_API ma_error_t ma_recorder_info_get_pending_orders_map(ma_recorder_info_t *info, ma_table_t **table);
MA_RECORDER_API ma_error_t ma_recorder_info_get_completed_orders_map(ma_recorder_info_t *info, ma_table_t **table);


/* convert recorder info to variant  and vice versa */
MA_RECORDER_API ma_error_t ma_recorder_info_convert_to_variant(ma_recorder_info_t *info, ma_variant_t **variant);
MA_RECORDER_API ma_error_t ma_recorder_info_convert_from_variant(ma_variant_t *variant, ma_recorder_info_t **info);
MA_RECORDER_API ma_error_t ma_recorder_info_convert_to_json(ma_recorder_info_t *info, ma_json_t **json);


MA_CPP(})

#endif /* MA_RECORDER_INFO_H_INCLUDED */

