#ifndef MA_REGEX_H_INCLUDED
#define MA_REGEX_H_INCLUDED
#include "ma/ma_common.h"

MA_CPP(extern "C" {)

typedef struct ma_regex_s ma_regex_t, *ma_regex_h;

MA_UTILS_API ma_error_t ma_regex_create(const char *pattern , ma_regex_t **regex);

MA_UTILS_API ma_error_t ma_regex_release(ma_regex_t *regex);

/*!
 * checks the text passed matches the regex pattern suppplied , return true on success else false
 */
MA_UTILS_API ma_bool_t ma_regex_match(ma_regex_t *regex , const char *text);

MA_CPP(})

#endif /* MA_REGEX_H_INCLUDED */

