#ifndef MA_CONFIGURATOR_UTILS_H_INCLUDED
#define MA_CONFIGURATOR_UTILS_H_INCLUDED

#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/datastore/ma_ds_database.h"
#include "ma/policy/ma_policy.h"
#include "ma/internal/clients/policy/ma_policy_bag_internal.h"
#include "ma/internal/clients/policy/ma_policy_uri_internal.h"
MA_CPP( extern "C" { )

#define MA_POLICY_FEATURE_ID            "EPOAGENTMETA"
#define MA_POLICY_CATEGORY_ID           "General"
#define MA_POLICY_TYPE_ID               "General"
#define MA_POLICY_NAME_ID               "My Default"
#define MA_REPO_POLICY_CATEGORY_ID      "Repository"
#define MA_REPO_POLICY_TYPE_ID          "Repository"

typedef struct ma_datastore_configuration_request_s ma_datastore_configuration_request_t;
typedef struct ma_db_request_s {
    ma_bool_t   is_required;
    int         db_flags;
}ma_db_request_t;

struct ma_datastore_configuration_request_s{
	ma_db_request_t   ma_default;
	ma_db_request_t   ma_policy;
	ma_db_request_t   ma_task;
	ma_db_request_t   ma_scheduler;
	ma_db_request_t   ma_msgbus;
        ma_db_request_t   ma_booking;
        ma_db_request_t   ma_profile;
        ma_db_request_t   ma_inventory;
        ma_db_request_t   ma_recorder;
        ma_db_request_t   ma_location;
        ma_db_request_t   ma_season;
        ma_db_request_t   ma_search;
        ma_db_request_t   ma_board;
        ma_db_request_t   ma_comment;
        ma_db_request_t   ma_consignment;
};

MA_CONFIGURATOR_API ma_error_t ma_configurator_intialize_datastores(ma_configurator_t *self, ma_datastore_configuration_request_t *request);

MA_CONFIGURATOR_API ma_error_t ma_configurator_configure_agent_policies(ma_configurator_t *configurator);

MA_CONFIGURATOR_API ma_error_t ma_configurator_configure_repository_policies(ma_configurator_t *configurator) ;

MA_CONFIGURATOR_API ma_error_t ma_configurator_set_guid(ma_configurator_t *configurator, char guid[UUID_LEN_TXT]);

MA_CONFIGURATOR_API ma_error_t ma_import_policies(ma_db_t *policy_db, ma_policy_uri_t *info_uri, ma_policy_bag_t *policy_bag);

MA_CONFIGURATOR_API ma_error_t ma_configurator_reconfigure_datastores_permissions(ma_configurator_t *configurator);

MA_CPP(})

#endif /* MA_CONFIGURATOR_H_INCLUDED */

