#ifndef MA_CONFIGURATOR_H_INCLUDED
#define MA_CONFIGURATOR_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_datastore.h"
#include "ma/datastore/ma_ds_database.h"
#include "ma/datastore/ma_ds.h"
#include "ma/internal/utils/database/ma_db.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/uuid/ma_uuid.h"
#include "ma/internal/utils/app_info/ma_registry_store.h"
#include "ma/internal/defs/ma_general_defs.h"

MA_CPP( extern "C" { )

//struct ma_ds_database_s;
typedef struct ma_configurator_s ma_configurator_t, *ma_configurator_h;
typedef struct ma_configurator_methods_s ma_configurator_methods_t;
typedef struct ma_agent_configuration_s  ma_agent_configuration_t;

struct ma_agent_configuration_s {
    unsigned short int					agent_mode;
    unsigned short int					agent_crypto_mode;
    unsigned short int					agent_crypto_role;
	unsigned short int					vdi_mode;
	int									seq_num;
    char								agent_guid[UUID_LEN_TXT];
	char								tenant_id[MA_MAX_KEY_LEN];
    ma_bool_t                           mode_changed;
};

MA_CONFIGURATOR_API ma_error_t ma_configurator_create(ma_agent_configuration_t *agent_configuration, ma_configurator_t **configurator);
MA_CONFIGURATOR_API ma_error_t ma_service_configurator_create(ma_configurator_t **configurator);


/*configurator base class */
 
#define MA_DS_INFO_DEFAULT					0
#define MA_DS_INFO_POLICY					MA_DS_INFO_DEFAULT + 1
#define MA_DS_INFO_TASK						MA_DS_INFO_DEFAULT + 2
#define MA_DS_INFO_SCHEDULER				MA_DS_INFO_DEFAULT + 3
#define MA_DS_INFO_MSGBUS					MA_DS_INFO_DEFAULT + 4
#define MA_DS_INFO_BOOKING                                      MA_DS_INFO_DEFAULT + 5
#define MA_DS_INFO_PROFILE                                      MA_DS_INFO_DEFAULT + 6
#define MA_DS_INFO_INVENTORY                                    MA_DS_INFO_DEFAULT + 7
#define MA_DS_INFO_RECORDER                                    MA_DS_INFO_DEFAULT + 8
#define MA_DS_INFO_LOCATION                                    MA_DS_INFO_DEFAULT + 9
#define MA_DS_INFO_SEASON                                      MA_DS_INFO_DEFAULT + 10
#define MA_DS_INFO_SEARCH                                      MA_DS_INFO_DEFAULT + 11
#define MA_DS_INFO_BOARD                                      MA_DS_INFO_DEFAULT + 12
#define MA_DS_INFO_COMMENT                                      MA_DS_INFO_DEFAULT + 13
#define MA_DS_INFO_CONSIGNMENT                                      MA_DS_INFO_DEFAULT + 14

struct ma_configuration_s{
	ma_agent_configuration_t				*agent_configuration;
    const char								*agent_install_path;
    const char								*agent_data_path;
	const char								*agent_lib_path;
    const char								*agent_tools_lib_path;    
};

struct ma_datastore_info_s{
	ma_ds_t   *ds;
	ma_db_t   *db;
};

struct ma_configurator_methods_s{
	ma_error_t (*get_ds_info)(ma_configurator_t *configurator, int ds_type, struct ma_datastore_info_s *ds_info);
	ma_error_t (*populate_agent_configuration)(ma_configurator_t *configurator, struct ma_configuration_s *configuration);
	void (*configurator_destroy)(ma_configurator_t *configurator);
};

struct ma_configurator_s {
	const ma_configurator_methods_t     *methods;
    void								*data;
};

MA_STATIC_INLINE ma_error_t ma_configurator_init(ma_configurator_t *self, const ma_configurator_methods_t *methods, void *userdata){
	if(self && methods && userdata){
		self->methods = methods;
		self->data = userdata;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

MA_STATIC_INLINE ma_error_t ma_configurator_release(ma_configurator_t *self){
	if(self){
		self->methods->configurator_destroy(self);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

MA_STATIC_INLINE const char *ma_configurator_get_agent_data_path(ma_configurator_t *self) {	
	if(self){		
		static struct ma_configuration_s config = {0};
		if(!config.agent_data_path)
			self->methods->populate_agent_configuration(self, &config);
		return config.agent_data_path;
	}
    return  NULL;
}

MA_STATIC_INLINE const char *ma_configurator_get_agent_install_path(ma_configurator_t *self) {
    if(self){		
		static struct ma_configuration_s config = {0};
		if(!config.agent_install_path)
			self->methods->populate_agent_configuration(self, &config);
		return config.agent_install_path;
	}
    return  NULL;
}

MA_STATIC_INLINE const char *ma_configurator_get_agent_lib_path(ma_configurator_t *self, ma_bool_t is_ma_lib_path) {
	if(self){		
		static struct ma_configuration_s config = {0};
		
		#ifdef MA_WINDOWS
			/* For windows, ma libs and ma tools lib are same as ma install path - TODO - will change windows installation structure also*/
			if(!config.agent_install_path)
				self->methods->populate_agent_configuration(self, &config);
			return config.agent_install_path;
		#else
			if(!config.agent_lib_path)
				self->methods->populate_agent_configuration(self, &config);
			return (is_ma_lib_path) ? config.agent_lib_path : config.agent_tools_lib_path;
		#endif
	}
    return  NULL;
}

MA_STATIC_INLINE const char *ma_configurator_get_agent_software_id(ma_configurator_t *self) {
    return MA_SOFTWAREID_GENERAL_STR;
}

MA_STATIC_INLINE const char *ma_configurator_get_agent_meta_software_id(ma_configurator_t *self) {
    return MA_SOFTWAREID_META_STR;
}

MA_STATIC_INLINE const char *ma_configurator_get_agent_guid(ma_configurator_t *self) {
	if(self){		
		struct ma_configuration_s config = {0};
		self->methods->populate_agent_configuration(self, &config);
		return config.agent_configuration  ? config.agent_configuration->agent_guid : NULL;
	}
    return  NULL;
}

MA_STATIC_INLINE const char *ma_configurator_get_tenantid(ma_configurator_t *self) {
	if(self){		
		static struct ma_configuration_s config = {0};
		if(!config.agent_configuration)
			self->methods->populate_agent_configuration(self, &config);
		return config.agent_configuration  ? config.agent_configuration->tenant_id : NULL;
	}
    return  NULL;
}

MA_STATIC_INLINE unsigned short int ma_configurator_get_vdimode(ma_configurator_t *self) {
	#ifdef MA_WINDOWS
		if(self){		
			static struct ma_configuration_s config = {0};
			if(!config.agent_configuration)
				self->methods->populate_agent_configuration(self, &config);
			return config.agent_configuration  ? config.agent_configuration->vdi_mode : 0;
		}
		return  0;
	#else
		return  0;
	#endif
}

MA_STATIC_INLINE unsigned short int ma_configurator_get_agent_mode(ma_configurator_t *self) {
	if(self){		
		static struct ma_configuration_s config = {0};
		if(!config.agent_configuration)
			self->methods->populate_agent_configuration(self, &config);
        return config.agent_configuration->agent_mode;
	}
    return  0;

}
MA_STATIC_INLINE ma_ds_t *ma_configurator_get_datastore(ma_configurator_t *self){
    if(self){		
		static struct ma_datastore_info_s ds_info = {0};
		if(!ds_info.ds)
			self->methods->get_ds_info(self, MA_DS_INFO_DEFAULT, &ds_info);
		return ds_info.ds;
	}
    return  NULL;
}

MA_STATIC_INLINE ma_db_t *ma_configurator_get_database(ma_configurator_t *self) {
    if(self){		
		static struct ma_datastore_info_s ds_info = {0};
		if(!ds_info.db)
			self->methods->get_ds_info(self, MA_DS_INFO_DEFAULT, &ds_info);
		return ds_info.db;
	}
    return  NULL;
}

MA_STATIC_INLINE ma_db_t *ma_configurator_get_policies_database(ma_configurator_t *self) {
     if(self){		
		static struct ma_datastore_info_s ds_info = {0};
		if(!ds_info.db)
			self->methods->get_ds_info(self, MA_DS_INFO_POLICY, &ds_info);
		return ds_info.db;
	}
    return  NULL;
}

MA_STATIC_INLINE ma_db_t *ma_configurator_get_task_database(ma_configurator_t *self) {
     if(self){		
		static struct ma_datastore_info_s ds_info = {0};
		if(!ds_info.db)
			self->methods->get_ds_info(self, MA_DS_INFO_TASK, &ds_info);
		return ds_info.db;
	}
    return  NULL;
}

MA_STATIC_INLINE ma_ds_t *ma_configurator_get_scheduler_datastore(ma_configurator_t *self) {
     if(self){		
		static struct ma_datastore_info_s ds_info = {0};
		if(!ds_info.ds)
			self->methods->get_ds_info(self, MA_DS_INFO_SCHEDULER, &ds_info);
		return ds_info.ds;
	}
    return  NULL;
}

MA_STATIC_INLINE ma_db_t *ma_configurator_get_scheduler_database(ma_configurator_t *self) {
     if(self){		
		static struct ma_datastore_info_s ds_info = {0} ;
		if(!ds_info.db)
			self->methods->get_ds_info(self, MA_DS_INFO_SCHEDULER, &ds_info) ;
		return ds_info.db ;
	}
    return  NULL ;
}

MA_STATIC_INLINE ma_db_t *ma_configurator_get_msgbus_database(ma_configurator_t *self) {
     if(self){		
		static struct ma_datastore_info_s ds_info = {0};
		if(!ds_info.db)
			self->methods->get_ds_info(self, MA_DS_INFO_MSGBUS, &ds_info);
		return ds_info.db;
	}
    return  NULL;
}

MA_STATIC_INLINE ma_db_t *ma_configurator_get_booking_database(ma_configurator_t *self) {
     if(self){
                static struct ma_datastore_info_s ds_info = {0};
                if(!ds_info.db)
                        self->methods->get_ds_info(self, MA_DS_INFO_BOOKING, &ds_info);
                return ds_info.db;
        }
    return  NULL;
}

MA_STATIC_INLINE ma_ds_t *ma_configurator_get_booking_datastore(ma_configurator_t *self) {
     if(self){		
		static struct ma_datastore_info_s ds_info = {0};
		if(!ds_info.ds)
			self->methods->get_ds_info(self, MA_DS_INFO_BOOKING, &ds_info);
		return ds_info.ds;
	}
    return  NULL;
}
MA_STATIC_INLINE ma_db_t *ma_configurator_get_inventory_database(ma_configurator_t *self) {
     if(self){
                static struct ma_datastore_info_s ds_info = {0};
                if(!ds_info.db)
                        self->methods->get_ds_info(self, MA_DS_INFO_INVENTORY, &ds_info);
                return ds_info.db;
        }
    return  NULL;
}

MA_STATIC_INLINE ma_ds_t *ma_configurator_get_inventory_datastore(ma_configurator_t *self) {
     if(self){		
		static struct ma_datastore_info_s ds_info = {0};
		if(!ds_info.ds)
			self->methods->get_ds_info(self, MA_DS_INFO_INVENTORY, &ds_info);
		return ds_info.ds;
	}
    return  NULL;
}

MA_STATIC_INLINE ma_db_t *ma_configurator_get_recoder_database(ma_configurator_t *self) {
     if(self){
                static struct ma_datastore_info_s ds_info = {0};
                if(!ds_info.db)
                        self->methods->get_ds_info(self, MA_DS_INFO_RECORDER, &ds_info);
                return ds_info.db;
        }
    return  NULL;
}

MA_STATIC_INLINE ma_ds_t *ma_configurator_get_recorder_datastore(ma_configurator_t *self) {
     if(self){		
		static struct ma_datastore_info_s ds_info = {0};
		if(!ds_info.ds)
			self->methods->get_ds_info(self, MA_DS_INFO_RECORDER, &ds_info);
		return ds_info.ds;
	}
    return  NULL;
}

MA_STATIC_INLINE ma_db_t *ma_configurator_get_location_database(ma_configurator_t *self) {
     if(self){
                static struct ma_datastore_info_s ds_info = {0};
                if(!ds_info.db)
                        self->methods->get_ds_info(self, MA_DS_INFO_LOCATION, &ds_info);
                return ds_info.db;
        }
    return  NULL;
}

MA_STATIC_INLINE ma_ds_t *ma_configurator_get_location_datastore(ma_configurator_t *self) {
     if(self){		
		static struct ma_datastore_info_s ds_info = {0};
		if(!ds_info.ds)
			self->methods->get_ds_info(self, MA_DS_INFO_LOCATION, &ds_info);
		return ds_info.ds;
	}
    return  NULL;
}

MA_STATIC_INLINE ma_db_t *ma_configurator_get_profile_database(ma_configurator_t *self) {
     if(self){
                static struct ma_datastore_info_s ds_info = {0};
                if(!ds_info.db)
                        self->methods->get_ds_info(self, MA_DS_INFO_PROFILE, &ds_info);
                return ds_info.db;
        }
    return  NULL;
}

MA_STATIC_INLINE ma_ds_t *ma_configurator_get_profile_datastore(ma_configurator_t *self) {
     if(self){		
		static struct ma_datastore_info_s ds_info = {0};
		if(!ds_info.ds)
			self->methods->get_ds_info(self, MA_DS_INFO_PROFILE, &ds_info);
		return ds_info.ds;
	}
    return  NULL;
}

MA_STATIC_INLINE ma_db_t *ma_configurator_get_consignment_database(ma_configurator_t *self) {
     if(self){
                static struct ma_datastore_info_s ds_info = {0};
                if(!ds_info.db)
                        self->methods->get_ds_info(self, MA_DS_INFO_CONSIGNMENT, &ds_info);
                return ds_info.db;
        }
    return  NULL;
}

MA_STATIC_INLINE ma_ds_t *ma_configurator_get_consignment_datastore(ma_configurator_t *self) {
     if(self){		
		static struct ma_datastore_info_s ds_info = {0};
		if(!ds_info.ds)
			self->methods->get_ds_info(self, MA_DS_INFO_CONSIGNMENT, &ds_info);
		return ds_info.ds;
	}
    return  NULL;
}

MA_STATIC_INLINE ma_db_t *ma_configurator_get_season_database(ma_configurator_t *self) {
     if(self){
                static struct ma_datastore_info_s ds_info = {0};
                if(!ds_info.db)
                        self->methods->get_ds_info(self, MA_DS_INFO_SEASON, &ds_info);
                return ds_info.db;
        }
    return  NULL;
}

MA_STATIC_INLINE ma_ds_t *ma_configurator_get_season_datastore(ma_configurator_t *self) {
     if(self){		
		static struct ma_datastore_info_s ds_info = {0};
		if(!ds_info.ds)
			self->methods->get_ds_info(self, MA_DS_INFO_SEASON, &ds_info);
		return ds_info.ds;
	}
    return  NULL;
}
MA_STATIC_INLINE ma_ds_t *ma_configurator_get_search_datastore(ma_configurator_t *self) {
     if(self){		
		static struct ma_datastore_info_s ds_info = {0};
		if(!ds_info.ds)
			self->methods->get_ds_info(self, MA_DS_INFO_SEARCH, &ds_info);
		return ds_info.ds;
	}
    return  NULL;
}
MA_STATIC_INLINE ma_ds_t *ma_configurator_get_board_datastore(ma_configurator_t *self) {
     if(self){		
		static struct ma_datastore_info_s ds_info = {0};
		if(!ds_info.ds)
			self->methods->get_ds_info(self, MA_DS_INFO_BOARD, &ds_info);
		return ds_info.ds;
	}
    return  NULL;
}
MA_STATIC_INLINE ma_ds_t *ma_configurator_get_comment_datastore(ma_configurator_t *self) {
     if(self){		
		static struct ma_datastore_info_s ds_info = {0};
		if(!ds_info.ds)
			self->methods->get_ds_info(self, MA_DS_INFO_COMMENT, &ds_info);
		return ds_info.ds;
	}
    return  NULL;
}
MA_CPP(})

#endif /* MA_CONFIGURATOR_H_INCLUDED */
