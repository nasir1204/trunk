#if  !defined(MA_NO_DISPATCHER) && !defined(MA_NO_CRYPTO_DISPATCHER)
#ifndef MA_CRYPTO_EX_DISPATCHER_H_INCLUDED
#define MA_CRYPTO_EX_DISPATCHER_H_INCLUDED

#include "ma/dispatcher/ma_dispatcher_macros.h"

#undef MA_DISPATCHER_PRODUCT_NAME
#undef MA_DISPATCHER_LIBRARY_NAME
#undef MA_DISPATCHER_MODULE
#undef MA_DISPATCHER_LIBRARY_VERSION

#define MA_DISPATCHER_PRODUCT_NAME		 "EPOAGENT3000"
#define MA_DISPATCHER_LIBRARY_NAME		 MA_DEFINE_DISPATCHER_LIB(ma_crypto)
#define MA_DISPATCHER_MODULE			 "macrypto"
#define MA_DISPATCHER_LIBRARY_VERSION	 0x05020100

static unsigned int ma_crypto_ex_lib_version;

MA_CPP(extern "C" {)
#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#define MA_DISPATCHER_ACTUAL_ARG_EXPANDER\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_ctx_create,(ma_crypto_ctx_t **ctx), (ctx))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_ctx_set_fips_mode,(ma_crypto_ctx_t *ctx, ma_crypto_mode_t mode), (ctx,mode))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_ctx_set_role,(ma_crypto_ctx_t *ctx, ma_crypto_role_t role), (ctx,role))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_ctx_set_agent_mode,(ma_crypto_ctx_t *ctx, ma_crypto_agent_mode_t agent_mode), (ctx,agent_mode))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_ctx_set_logger,(ma_crypto_ctx_t *ctx, ma_logger_t *logger), (ctx,logger))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_ctx_set_keystore,(ma_crypto_ctx_t *ctx, const char *keystore_path), (ctx,keystore_path))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_ctx_set_certstore,(ma_crypto_ctx_t *ctx, const char *certstore), (ctx,certstore))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_ctx_set_crypto_lib_path,(ma_crypto_ctx_t *ctx, const char *crypto_lib_path), (ctx,crypto_lib_path))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_ctx_release,(ma_crypto_ctx_t *ctx), (ctx))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_create,(ma_crypto_t **crypto), (crypto))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_release,(ma_crypto_t *crypto), (crypto))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_initialize,(ma_crypto_ctx_t *context, ma_crypto_t *crypto), (context, crypto))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_deinitialize,(ma_crypto_t *crypto), (crypto))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_add_cert,(ma_crypto_t *crypto, const char *cert_name, const unsigned char *data, size_t data_len), (crypto,cert_name,data,data_len))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_remove_cert,(ma_crypto_t *crypto, const char *cert_name), (crypto,cert_name))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_import_server_public_key,(ma_crypto_t *crypto, const unsigned char *key, unsigned int key_size), (crypto,key,key_size))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_import_server_signing_key,(ma_crypto_t *crypto, const unsigned char *key, unsigned int key_size), (crypto,key,key_size))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_get_agent_public_key,(ma_crypto_t *crypto, const ma_bytebuffer_t **key), (crypto,key))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_get_server_public_key,(ma_crypto_t *crypto, const ma_bytebuffer_t **key), (crypto,key))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_get_agent_public_key_hash,(ma_crypto_t *crypto, const ma_bytebuffer_t **key_hash), (crypto,key_hash))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_get_server_public_key_hash,(ma_crypto_t *crypto, const ma_bytebuffer_t **key_hash), (crypto,key_hash))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_sign_create,(ma_crypto_t *macrypto, ma_crypto_sign_key_type_t type, ma_crypto_sign_t **sign), (macrypto,type,sign))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_sign_data_update,(ma_crypto_sign_t *sign, const unsigned char *data, unsigned int data_size), (sign,data,data_size))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_sign_data_final,(ma_crypto_sign_t *sign, ma_bytebuffer_t *signature), (sign,signature))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_sign_buffer,(ma_crypto_sign_t *sign, const unsigned char *data, unsigned int data_size, ma_bytebuffer_t *signature), (sign,data,data_size,signature))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_sign_release,(ma_crypto_sign_t *sign), (sign))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_verify_create,(ma_crypto_t *macrypto, ma_crypto_verify_key_type_t type, ma_crypto_verify_t **verify), (macrypto,type,verify))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_verify_create_with_key,(ma_crypto_t *macrypto, const unsigned char *key, unsigned int key_size, unsigned int rsa_key_size, ma_crypto_verify_t **verify), (macrypto,key,key_size,rsa_key_size,verify))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_verify_data_update,(ma_crypto_verify_t *verify, const unsigned char *data, unsigned int data_size), (verify,data,data_size))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_verify_data_final,(ma_crypto_verify_t *verify, const unsigned char *signature, unsigned int signature_size, ma_bool_t *is_success), (verify,signature,signature_size,is_success))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_verify_buffer,(ma_crypto_verify_t *verify, const unsigned char *data, unsigned int data_size, const unsigned char *signature, unsigned int signature_size, ma_bool_t *is_success), (verify,data,data_size,signature,signature_size,is_success))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_verify_release,(ma_crypto_verify_t *verify), (verify))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_verify_legacy_cab_file,(ma_crypto_t *macrypto, const unsigned char *key, size_t key_len, const unsigned char *data, size_t data_len, unsigned char *signature, size_t sig_len, ma_bool_t *result), (macrypto,key,key_len,data,data_len,signature,sig_len,result))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_hash_create,(ma_crypto_t *macrypto, ma_crypto_hash_type_t type, ma_crypto_hash_t **hash), (macrypto,type,hash))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_hash_data_update,(ma_crypto_hash_t *hash, const unsigned char *data, unsigned int data_size), (hash,data,data_size))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_hash_data_final,(ma_crypto_hash_t *hash, ma_bytebuffer_t *hash_buffer), (hash,hash_buffer))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_hash_buffer,(ma_crypto_hash_t *hash, const unsigned char *data, unsigned int data_size, ma_bytebuffer_t *hash_buffer), (hash,data,data_size,hash_buffer))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_hash_and_encode,(ma_crypto_hash_t *hash, const unsigned char *data, unsigned int data_size, ma_bytebuffer_t *hash_buffer), (hash,data,data_size,hash_buffer))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_hash_release,(ma_crypto_hash_t *hash), (hash))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_encrypt_create,(ma_crypto_t *macrypto, ma_crypto_encryption_type_t type, ma_crypto_encryption_t **encrypt), (macrypto,type,encrypt))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_encrypt,(ma_crypto_encryption_t *encrypt, const unsigned char *data, unsigned int data_size, ma_bytebuffer_t *encrypted_data), (encrypt,data,data_size,encrypted_data))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_encrypt_and_encode,(ma_crypto_encryption_t *encrypt, const unsigned char *data, unsigned int data_size, ma_bytebuffer_t *encoded_data), (encrypt,data,data_size,encoded_data))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_encrypt_release,(ma_crypto_encryption_t *encrypt), (encrypt))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_decrypt_create,(ma_crypto_t *macrypto, ma_crypto_decryption_type_t type, ma_crypto_decryption_t **decrypt), (macrypto,type,decrypt))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_decrypt,(ma_crypto_decryption_t *decrypt, const unsigned char *data, unsigned int data_size, ma_bytebuffer_t *decrypted_data), (decrypt,data,data_size,decrypted_data))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_decode_and_decrypt,(ma_crypto_decryption_t *decrypt, const unsigned char *data, unsigned int data_size, ma_bytebuffer_t *decrypted_data), (decrypt,data,data_size,decrypted_data))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_decrypt_release,(ma_crypto_decryption_t *decrypt), (decrypt))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_encode,(const unsigned char *data, unsigned int data_size, ma_bytebuffer_t *encoded_data, ma_bool_t ignore_obfuscation), (data,data_size,encoded_data,ignore_obfuscation))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_decode,(const unsigned char *data, unsigned int data_size, ma_bytebuffer_t *decoded_data, ma_bool_t ignore_obfuscation), (data,data_size,decoded_data,ignore_obfuscation))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_hex_encode,(const unsigned char *raw,const unsigned int size,unsigned char *hex,unsigned int max_size), (raw,size,hex,max_size))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_hex_decode,(const unsigned char *hex,const unsigned int size,unsigned char *raw,unsigned int max_size), (hex,size,raw,max_size))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_cms_create,(ma_crypto_t *crypto, ma_crypto_cms_t **cms), (crypto,cms))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_cms_release,(ma_crypto_cms_t *cms), (cms))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_cms_verify,(ma_crypto_cms_t *cms, int cms_fd, int *is_verified), (cms,cms_fd,is_verified))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_cms_get_auth_manifest,(ma_crypto_cms_t *cms, const char **auth_manifest), (cms,auth_manifest))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_cms_get_issuer_name,(ma_crypto_cms_t *cms, const char **issuer_name), (cms,issuer_name))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_crypto_cms_get_subject_name,(ma_crypto_cms_t *cms, const char **subject_name), (cms,subject_name))


    static const char *ma_crypto_ex_function_names[] = {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) #function,
        MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
};

typedef struct ma_crypto_ex_functions_s {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) ret_type (*function##_fn) signature;
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
}ma_crypto_ex_functions_t;
static ma_crypto_ex_functions_t* ma_crypto_ex_functions = NULL;

#define ma_crypto_ex_functions_size (sizeof(ma_crypto_ex_function_names)/sizeof(ma_crypto_ex_function_names[0]))

#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) \
    MA_DISTPATCHER_ARG_STUB_CREATOR(&ma_crypto_ex_lib_version, MA_DISPATCHER_PRODUCT_NAME, MA_DISPATCHER_LIBRARY_NAME, MA_DISPATCHER_LIBRARY_VERSION,MA_DISPATCHER_MODULE,ret_type, function, signature, arg, ma_crypto_ex_function_names, &ma_crypto_ex_functions, ma_crypto_ex_functions_size)
MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER

#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER
    MA_CPP(})

#endif /* MA_CRYPTO_EX_DISPATCHER_H_INCLUDED */
#endif /*MA_USE_DISPATCHER*/

