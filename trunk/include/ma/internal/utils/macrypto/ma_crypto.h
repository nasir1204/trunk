#ifndef MA_CRYPTO_INTERNAL_H_INCLUDED
#define MA_CRYPTO_INTERNAL_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/datastructures/ma_bytebuffer.h"
#include "ma/ma_log.h"

MA_CPP(extern "C" {)

typedef struct ma_crypto_s ma_crypto_t, *ma_crypto_h ;
typedef struct ma_crypto_ctx_s ma_crypto_ctx_t, *ma_crypto_ctx_h;


typedef enum ma_crypto_mode_e {
    MA_CRYPTO_MODE_NON_FIPS = 0,
    MA_CRYPTO_MODE_FIPS
}ma_crypto_mode_t;

typedef enum ma_crypto_role_e {
    MA_CRYPTO_ROLE_USER = 0,
    MA_CRYPTO_ROLE_OFFICER
}ma_crypto_role_t;

typedef enum ma_crypto_agent_mode_e {
    MA_CRYPTO_AGENT_UNMANAGED = 0,
    MA_CRYPTO_AGENT_MANAGED
}ma_crypto_agent_mode_t;


/* macrypto context API's */
MA_CRYPTO_API ma_error_t ma_crypto_ctx_create(ma_crypto_ctx_t **ctx);

MA_CRYPTO_API ma_error_t ma_crypto_ctx_set_fips_mode(ma_crypto_ctx_t *ctx, ma_crypto_mode_t mode);

MA_CRYPTO_API ma_error_t ma_crypto_ctx_set_role(ma_crypto_ctx_t *ctx, ma_crypto_role_t role);

MA_CRYPTO_API ma_error_t ma_crypto_ctx_set_agent_mode(ma_crypto_ctx_t *ctx, ma_crypto_agent_mode_t agent_mode);

MA_CRYPTO_API ma_error_t ma_crypto_ctx_set_logger(ma_crypto_ctx_t *ctx, ma_logger_t *logger);

MA_CRYPTO_API ma_error_t ma_crypto_ctx_set_keystore(ma_crypto_ctx_t *ctx, const char *keystore_path);

MA_CRYPTO_API ma_error_t ma_crypto_ctx_set_certstore(ma_crypto_ctx_t *ctx, const char *certstore);

MA_CRYPTO_API ma_error_t ma_crypto_ctx_set_crypto_lib_path(ma_crypto_ctx_t *ctx, const char *crypto_lib_path);

MA_CRYPTO_API ma_error_t ma_crypto_ctx_release(ma_crypto_ctx_t *ctx);

#if  defined(MA_NO_DISPATCHER) || defined(MA_NO_CRYPTO_DISPATCHER)
MA_CRYPTO_API ma_crypto_ctx_t *ma_crypto_ctx_copy(const ma_crypto_ctx_t *ctx);

/* macrypto context get API's */
MA_CRYPTO_API ma_crypto_mode_t ma_crypto_ctx_get_fips_mode(const ma_crypto_ctx_t *ctx);

MA_CRYPTO_API ma_crypto_agent_mode_t ma_crypto_ctx_get_agent_mode(const ma_crypto_ctx_t *ctx);

MA_CRYPTO_API ma_crypto_role_t ma_crypto_ctx_get_role(const ma_crypto_ctx_t *ctx);
#endif /*defined(MA_NO_DISPATCHER) || defined(MA_NO_CRYPTO_DISPATCHER)*/

/* macrypto creation/release - Creates and releases crypto object */

MA_CRYPTO_API ma_error_t ma_crypto_create(ma_crypto_t **crypto);

#if  defined(MA_NO_DISPATCHER) || defined(MA_NO_CRYPTO_DISPATCHER)
MA_CRYPTO_API const ma_crypto_ctx_t *ma_crypto_get_context(ma_crypto_t *crypto);
#endif /*defined(MA_NO_DISPATCHER) || defined(MA_NO_CRYPTO_DISPATCHER)*/

MA_CRYPTO_API ma_error_t ma_crypto_release(ma_crypto_t *crypto);

/* macrypto Initialize/Deinitialize - Loads and Unloads underlying crypto library*/
MA_CRYPTO_API ma_error_t ma_crypto_initialize(ma_crypto_ctx_t *context, ma_crypto_t *crypto);

MA_CRYPTO_API ma_error_t ma_crypto_deinitialize(ma_crypto_t *crypto);

/* cert management API's */
MA_CRYPTO_API ma_error_t ma_crypto_add_cert(ma_crypto_t *crypto, const char *cert_name, const unsigned char *data, size_t data_len);

MA_CRYPTO_API ma_error_t ma_crypto_remove_cert(ma_crypto_t *crypto, const char *cert_name);

/* Key management API's */
MA_CRYPTO_API ma_error_t ma_crypto_import_server_public_key(ma_crypto_t *crypto, const unsigned char *key, unsigned int key_size);

MA_CRYPTO_API ma_error_t ma_crypto_import_server_signing_key(ma_crypto_t *crypto, const unsigned char *key, unsigned int key_size);

MA_CRYPTO_API ma_error_t ma_crypto_get_agent_public_key(ma_crypto_t *crypto, const ma_bytebuffer_t **key);

MA_CRYPTO_API ma_error_t ma_crypto_get_server_public_key(ma_crypto_t *crypto, const ma_bytebuffer_t **key);

MA_CRYPTO_API ma_error_t ma_crypto_get_agent_public_key_hash(ma_crypto_t *crypto, const ma_bytebuffer_t **key_hash);

MA_CRYPTO_API ma_error_t ma_crypto_get_server_public_key_hash(ma_crypto_t *crypto, const ma_bytebuffer_t **key_hash);


/* Sign/Verify API's */
typedef struct ma_crypto_sign_s ma_crypto_sign_t, *ma_crypto_sign_h ;

typedef enum ma_crypto_sign_key_type_s {    
    MA_CRYPTO_SIGN_AGENT_PRIVATE_KEY = 0,    
    MA_CRYPTO_SIGN_SERVER_SIGNING_KEY       
}ma_crypto_sign_key_type_t;

MA_CRYPTO_API ma_error_t ma_crypto_sign_create(ma_crypto_t *macrypto, ma_crypto_sign_key_type_t type, ma_crypto_sign_t **sign);

MA_CRYPTO_API ma_error_t ma_crypto_sign_data_update(ma_crypto_sign_t *sign, const unsigned char *data, unsigned int data_size);

MA_CRYPTO_API ma_error_t ma_crypto_sign_data_final(ma_crypto_sign_t *sign, ma_bytebuffer_t *signature);

MA_CRYPTO_API ma_error_t ma_crypto_sign_buffer(ma_crypto_sign_t *sign, const unsigned char *data, unsigned int data_size, ma_bytebuffer_t *signature);

MA_CRYPTO_API ma_error_t ma_crypto_sign_release(ma_crypto_sign_t *sign);


typedef struct ma_crypto_verify_s ma_crypto_verify_t, *ma_crypto_verify_h ;

typedef enum ma_crypto_verify_key_type_s {    
    MA_CRYPTO_VERIFY_AGENT_PUBLIC_KEY = 0,    
    MA_CRYPTO_VERIFY_SERVER_PUBLIC_KEY       
}ma_crypto_verify_key_type_t;

MA_CRYPTO_API ma_error_t ma_crypto_verify_create(ma_crypto_t *macrypto, ma_crypto_verify_key_type_t type, ma_crypto_verify_t **verify);

MA_CRYPTO_API ma_error_t ma_crypto_verify_create_with_key(ma_crypto_t *macrypto, const unsigned char *key, unsigned int key_size, unsigned int rsa_key_size, ma_crypto_verify_t **verify);

MA_CRYPTO_API ma_error_t ma_crypto_verify_data_update(ma_crypto_verify_t *verify, const unsigned char *data, unsigned int data_size);

MA_CRYPTO_API ma_error_t ma_crypto_verify_data_final(ma_crypto_verify_t *verify, const unsigned char *signature, unsigned int signature_size, ma_bool_t *is_success);

MA_CRYPTO_API ma_error_t ma_crypto_verify_buffer(ma_crypto_verify_t *verify, const unsigned char *data, unsigned int data_size, const unsigned char *signature, unsigned int signature_size, ma_bool_t *is_success);

MA_CRYPTO_API ma_error_t ma_crypto_verify_release(ma_crypto_verify_t *verify);

MA_CRYPTO_API ma_error_t ma_crypto_verify_legacy_cab_file(ma_crypto_t *macrypto, const unsigned char *key, size_t key_len, const unsigned char *data, size_t data_len, unsigned char *signature, size_t sig_len, ma_bool_t *result);

/* Hash API's */
typedef struct ma_crypto_hash_s ma_crypto_hash_t, *ma_crypto_hash_h ;

typedef enum ma_crypto_hash_type_e {
    MA_CRYPTO_HASH_SHA1      = 0,
    MA_CRYPTO_HASH_SHA256,
    MA_CRYPTO_HASH_DEFAULT
}ma_crypto_hash_type_t;

MA_CRYPTO_API ma_error_t ma_crypto_hash_create(ma_crypto_t *macrypto, ma_crypto_hash_type_t type, ma_crypto_hash_t **hash);

MA_CRYPTO_API ma_error_t ma_crypto_hash_data_update(ma_crypto_hash_t *hash, const unsigned char *data, unsigned int data_size);

MA_CRYPTO_API ma_error_t ma_crypto_hash_data_final(ma_crypto_hash_t *hash, ma_bytebuffer_t *hash_buffer);

MA_CRYPTO_API ma_error_t ma_crypto_hash_buffer(ma_crypto_hash_t *hash, const unsigned char *data, unsigned int data_size, ma_bytebuffer_t *hash_buffer);

MA_CRYPTO_API ma_error_t ma_crypto_hash_and_encode(ma_crypto_hash_t *hash, const unsigned char *data, unsigned int data_size, ma_bytebuffer_t *hash_buffer);

MA_CRYPTO_API ma_error_t ma_crypto_hash_release(ma_crypto_hash_t *hash);


/* Encryption and Decryption API's */
typedef struct ma_crypto_encryption_s ma_crypto_encryption_t, *ma_crypto_encryption_h ;

typedef enum ma_crypto_encryption_type_e {
    MA_CRYPTO_ENCRYPTION_3DES = 0,
    MA_CRYPTO_ENCRYPTION_AES128
}ma_crypto_encryption_type_t;

MA_CRYPTO_API ma_error_t ma_crypto_encrypt_create(ma_crypto_t *macrypto, ma_crypto_encryption_type_t type, ma_crypto_encryption_t **encrypt);

MA_CRYPTO_API ma_error_t ma_crypto_encrypt(ma_crypto_encryption_t *encrypt, const unsigned char *data, unsigned int data_size, ma_bytebuffer_t *encrypted_data);

MA_CRYPTO_API ma_error_t ma_crypto_encrypt_and_encode(ma_crypto_encryption_t *encrypt, const unsigned char *data, unsigned int data_size, ma_bytebuffer_t *encoded_data);

MA_CRYPTO_API ma_error_t ma_crypto_encrypt_release(ma_crypto_encryption_t *encrypt);


typedef struct ma_crypto_decryption_s ma_crypto_decryption_t, *ma_crypto_decryption_h ;

typedef enum ma_crypto_decryption_type_e {
    MA_CRYPTO_DECRYPTION_3DES = 0,
    MA_CRYPTO_DECRYPTION_AES128,
    MA_CRYPTO_DECRYPTION_POLICY_SYM,            /*EPOAES128: or decode It is using MC_ALGID_SYMMETRIC_AES_128_ECB algorithm */
    MA_CRYPTO_DECRYPTION_PASSWORD_3DES,         /* skip conversion in fips mode */
    MA_CRYPTO_DECRYPTION_PASSWORD_ASYM,         /*Server signing key , skip conversion in fips mode */  
}ma_crypto_decryption_type_t;


MA_CRYPTO_API ma_error_t ma_crypto_decrypt_create(ma_crypto_t *macrypto, ma_crypto_decryption_type_t type, ma_crypto_decryption_t **decrypt);

MA_CRYPTO_API ma_error_t ma_crypto_decrypt(ma_crypto_decryption_t *decrypt, const unsigned char *data, unsigned int data_size, ma_bytebuffer_t *decrypted_data);

MA_CRYPTO_API ma_error_t ma_crypto_decode_and_decrypt(ma_crypto_decryption_t *decrypt, const unsigned char *data, unsigned int data_size, ma_bytebuffer_t *decrypted_data);

MA_CRYPTO_API ma_error_t ma_crypto_decrypt_release(ma_crypto_decryption_t *decrypt);


/* Encode and Decode API's - Base64 encode/decode*/
MA_CRYPTO_API ma_error_t ma_crypto_encode(const unsigned char *data, unsigned int data_size, ma_bytebuffer_t *encoded_data, ma_bool_t ignore_obfuscation);

MA_CRYPTO_API ma_error_t ma_crypto_decode(const unsigned char *data, unsigned int data_size, ma_bytebuffer_t *decoded_data, ma_bool_t ignore_obfuscation);

/* Encode and Decode API's - hex encode/decode, ecoding size should be atleast double, decode size should be atleast half*/
MA_CRYPTO_API ma_error_t ma_crypto_hex_encode(const unsigned char *raw,const unsigned int size,unsigned char *hex,unsigned int max_size);

MA_CRYPTO_API ma_error_t ma_crypto_hex_decode(const unsigned char *hex,const unsigned int size,unsigned char *raw,unsigned int max_size);


/* Binary/msgbus authentication */
typedef struct ma_crypto_cms_s ma_crypto_cms_t, *ma_crypto_cms_h;

MA_CRYPTO_API ma_error_t ma_crypto_cms_create(ma_crypto_t *crypto, ma_crypto_cms_t **cms);

MA_CRYPTO_API ma_error_t ma_crypto_cms_release(ma_crypto_cms_t *cms);

MA_CRYPTO_API ma_error_t ma_crypto_cms_verify(ma_crypto_cms_t *cms, int cms_fd, int *is_verified);

MA_CRYPTO_API ma_error_t ma_crypto_cms_get_auth_manifest(ma_crypto_cms_t *cms, const char **auth_manifest);

MA_CRYPTO_API ma_error_t ma_crypto_cms_get_issuer_name(ma_crypto_cms_t *cms, const char **issuer_name);

MA_CRYPTO_API ma_error_t ma_crypto_cms_get_subject_name(ma_crypto_cms_t *self, const char **subject_name);

MA_CPP(})

#include "ma/internal/utils/macrypto/ma_crypto_ex_dispatcher.h"

#endif /* MA_CRYPTO_H_INCLUDED */

