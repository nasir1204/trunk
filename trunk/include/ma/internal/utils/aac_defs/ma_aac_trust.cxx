#include "ma/internal/utils/aac_defs/ma_aac_trust.h"

#include <windows.h>
#include <atlbase.h>
#include <atlconv.h>
#include "accesslib.h"

#pragma comment(lib, "accesslib")
#pragma comment(lib, "rpcrt4")

ma_aac_trust_handle ma_aac_trust_handle_create() {
    return static_cast<ma_aac_trust_handle>(::OpenAccessHandle(ACCESS_VSCORE_14_2|ACCESS_AAC)); 
} 

void ma_aac_trust_handle_release(ma_aac_trust_handle h) {
    ::CloseAccessHandle(static_cast<ACCESSLIB_HANDLE>(h));
}

void ma_aac_trust_acquire_file(ma_aac_trust_handle h, const char *path) {
    ma_aac_trust_acquire_file_w(h, ATL::CA2WEX<MAX_PATH>(path,CP_UTF8));
}

void ma_aac_trust_acquire_file_w(ma_aac_trust_handle h, const wchar_t *path) {
    ::GetAccess(h, AAC_OBJECT_FILE, path, AAC_ACCESS_CREATE | AAC_ACCESS_WRITE | AAC_ACCESS_DELETE);
}