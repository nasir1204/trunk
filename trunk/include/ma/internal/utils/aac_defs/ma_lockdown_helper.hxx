#pragma once

#include <memory>
#include <atlbase.h>

#define MA_SERVICE_LOCKDOWN_REGPATH_TEMPLATE _T("SYSTEM\\CurrentControlSet\\seRvices\\%hs")

#define MA_SERVICE_LOCKDOWN_ENABLED_VALUE _T("LockdownEnabled")


struct ma_lockdown_module {

    ma_lockdown_module(const char *service_name) {
        _sntprintf(service_regpath, _countof(service_regpath), MA_SERVICE_LOCKDOWN_REGPATH_TEMPLATE, service_name); 
    }

    HKEY get_monitor_key() const {
        HKEY hkey;
        if (ERROR_SUCCESS == ::RegOpenKeyEx(HKEY_LOCAL_MACHINE, service_regpath, 0, KEY_READ, &hkey)) {
            return hkey;
        }
        return 0;
    }

    bool is_service_locked_down() const {
        ATL::CRegKey key;
        if (ERROR_SUCCESS == key.Open(HKEY_LOCAL_MACHINE, service_regpath, KEY_READ)) {
            DWORD value;
            if (ERROR_SUCCESS == key.QueryDWORDValue(MA_SERVICE_LOCKDOWN_ENABLED_VALUE, value) &&  0 == value) {
                return false;
            }
        }
        return true;
    }

    bool set_lockdown(bool lockdown) {
        ATL::CRegKey key;
        // TODO Should request AAC bypass
        if (ERROR_SUCCESS == key.Open(HKEY_LOCAL_MACHINE, service_regpath, KEY_WRITE)) {
            return ERROR_SUCCESS == key.SetDWORDValue(MA_SERVICE_LOCKDOWN_ENABLED_VALUE, lockdown ? 1 : 0);
        }
        return false;
    }

    TCHAR service_regpath[MAX_PATH];
};
