#include "ma/internal/utils/aac_defs/ma_aac_defs.h"

const GUID MA_AAC_PRODUCT_GUID = {0xD7687DD1, 0x979A, 0x4052, {0xA6, 0xE5, 0xED, 0xB9, 0x67, 0x90, 0x1C, 0x48}}; /* D7687DD1-979A-4052-A6E5-EDB967901C48 */

const GUID MA_AAC_MSGBUS_AUTH_POLICY_GUID = { 0xd0ed4888, 0x89d2, 0x41ac, { 0x98, 0xd0, 0xd5, 0x7a, 0x1d, 0x4e, 0x90, 0x4d } }; /* D0ED4888-89D2-41AC-98D0-D57A1D4E904D */

const GUID MA_AAC_SELF_PROTECTION_POLICY_GUID = {0xC7080E42, 0x1D78, 0x4B64, {0x8E, 0x32, 0x6D, 0x9E, 0x03, 0x21, 0x23, 0xFE}}; /* C7080E42-1D78-4B64-8E32-6D9E032123FE */

const GUID MA_AAC_LEGACY_ALLOW_GUID = { 0x4ab5e975, 0x278e, 0x481d, { 0xb5, 0x46, 0xfd, 0x5d, 0xba, 0x4c, 0xa, 0xae } }; /* 4AB5E975-278E-481D-B546-FD5DBA4C0AAE */

const GUID MA_AAC_MSGBUS_RULE_GUID = {0xB09A20F2, 0x1A10, 0x4F7B, {0x92, 0x3A, 0x73, 0x6D, 0x42, 0x81, 0x9D, 0x29}}; /* B09A20F2-1A10-4F7B-923A-736D42819D29 */

const GUID MA_AAC_PROCESS_RULE_GUID = { 0xd43246f4, 0xddf4, 0x4c6f, { 0xa3, 0x4d, 0x52, 0x84, 0x2, 0xa, 0x2f, 0xa9} }; /* D43246F4-DDF4-4C6F-A34D-5284020A2FA9 */

const GUID MA_AAC_SANITIZE_RULE_GUID = {0xc8edad6, 0x94e6, 0x424d, {0xbb, 0xcd, 0x79, 0x8c, 0xfd, 0x3d, 0xb7, 0x19} }; /* {0C8EDAD6-94E6-424D-BBCD-798CFD3DB719} */

const GUID MA_AAC_REGISTRY_RULE_GUID = {0xA5132714, 0xFE02, 0x4C0F, {0x96, 0x90, 0x15, 0x07, 0x6F, 0x70, 0x74, 0x35} }; /* A5132714-FE02-4C0F-9690-15076F707435 */

const GUID MA_AAC_ALLOW_PPINSTALLERS_RULE_GUID = { 0x2a4eb724, 0xe40f, 0x4c99, { 0xa8, 0xbb, 0x74, 0x69, 0x3f, 0x28, 0xb3, 0x2f} }; /* 2A4EB724-E40F-4C99-A8BB-74693F28B32F */

const GUID MA_AAC_FILE_RULE_GUID = {0xA24388FC, 0x5418, 0x4F26, {0x93, 0x9A, 0x18, 0x85, 0x0B, 0xAA, 0x62, 0x0A}}; /* A24388FC-5418-4F26-939A-18850BAA620A */

const GUID MA_SYSCORE_POLICY_GUID = {0x2aa87236, 0x7e59, 0x449b, {0xbd, 0xe1, 0xd2, 0xa6, 0x04, 0x5e , 0xd1, 0x5b}}; /* 2aa87236-7e59-449b-bde1-d2a6045ed15b */

const GUID MA_SYSCORE_AAC_POLICY_GUID = {0x65cf3d67, 0xb283, 0x4477, {0x83, 0xc6, 0x18, 0x1c, 0xef, 0xac, 0x74, 0x43}}; /* 65cf3d67-b283-4477-83c6-181cefac7443 */


const GUID *MA_AAC_ALL_RULES[] = {
    &MA_AAC_PROCESS_RULE_GUID,
    &MA_AAC_SANITIZE_RULE_GUID,
    &MA_AAC_MSGBUS_RULE_GUID,
    &MA_AAC_REGISTRY_RULE_GUID,
    &MA_AAC_FILE_RULE_GUID,
    NULL
};
