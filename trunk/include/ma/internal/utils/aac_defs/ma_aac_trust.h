#ifndef MA_AAC_TRUST_H_INCLUDED
#define MA_AAC_TRUST_H_INCLUDED

#include "ma/ma_common.h"

/* C wrapper API similar to VSCore's accesslib.h */

#ifdef MA_WINDOWS

MA_CPP(extern "C" {)

typedef struct ma_aac_trust_handle_s *ma_aac_trust_handle;

ma_aac_trust_handle ma_aac_trust_handle_create(); 

void ma_aac_trust_handle_release(ma_aac_trust_handle h);

void ma_aac_trust_acquire_file(ma_aac_trust_handle h, const char *path);

void ma_aac_trust_acquire_file_w(ma_aac_trust_handle h, const wchar_t *path);

#define MA_AAC_TRUST_HANDLE_INIT(h) ma_aac_trust_handle h = ma_aac_trust_handle_create();
#define MA_AAC_TRUST_ACQUIRE_TRUST_FOR_FILE(h, path) ma_aac_trust_acquire_file(h, path);
#define MA_AAC_TRUST_ACQUIRE_TRUST_FOR_FILE_W(h, path) ma_aac_trust_acquire_file_w(h, path);
#define MA_AAC_TRUST_HANDLE_DEINIT(h) ma_aac_trust_handle_release(h);

MA_CPP(})

#else

#define MA_AAC_TRUST_HANDLE_INIT(h)
#define MA_AAC_TRUST_ACQUIRE_TRUST_FOR_FILE(h, path)
#define MA_AAC_TRUST_ACQUIRE_TRUST_FOR_FILE_W(h, path)
#define MA_AAC_TRUST_HANDLE_DEINIT(h)

#endif

#endif /* MA_AAC_TRUST_H_INCLUDED */
