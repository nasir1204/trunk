#include "ma/ma_selfprotect.h"
#include "ma/internal/utils/aac_defs/ma_aac_defs.h"
#include "ma/internal/utils/aac_defs/ma_lockdown_helper.hxx"

#include "aacapi.h"

#include <atlsecurity.h>

ma_error_t ma_selfprotect_enable(ma_uint32_t products, ma_bool_t enable) {

    if (true) {
        bool bIsAdmin;
        if (!ATL::CAccessToken().CheckTokenMembership(Sids::Admins(), &bIsAdmin) || !bIsAdmin) {
            return MA_ERROR_ACCESS_DENIED;
        }
    }

    // set lockdown bit

    //ma_lockdown_module lockdown_module;
    //if (lockdown_module) {
    //    // note, this is a bit troubled because if MA SP is disabled, this will disable lockdown for everyone!
    //    lockdown_module.enable(MA_FALSE != enable);
    //}

    AacControl *aac_ctrl = nullptr;
    int rc = ::AacControlCreate(&aac_ctrl, MA_AAC_PRODUCT_GUID);
    if (0 == rc) {
        
        if (products & MA_SELFPROTECT_PRODUCT_MA) {
            rc = aac_ctrl->EnableDisableGroup(MA_AAC_SELF_PROTECTION_POLICY_GUID, _CRT_WIDE(MA_AAC_SELF_PROTECTION_GROUP), MA_FALSE != enable);
        }

        if (products & MA_SELFPROTECT_PRODUCT_SYSCORE) {
            aac_ctrl->EnableDisableGroup(MA_SYSCORE_POLICY_GUID, _CRT_WIDE(MA_SYSCORE_GROUP_NAME), MA_FALSE != enable);
            aac_ctrl->EnableDisableGroup(MA_SYSCORE_AAC_POLICY_GUID, _CRT_WIDE(MA_SYSCORE_GROUP_NAME), MA_FALSE != enable);
        }

        aac_ctrl->Release();
        ::AacDllUnload();
    }


    return ma_error_t(rc);
}


static bool is_some_blocking_rule_for_tag_enabled(AacActivePolicyList *policy_list, const UUID &policy_guid, LPCWSTR rule_tag) {
    bool enabled = false;
    AacPolicy *policy = nullptr;
    if (0 == policy_list->GetPolicy(&policy, policy_guid)) {
        for (AacRule *rule = nullptr; nullptr != (rule = policy->GetNextRule(rule)); ) {
            if (AacIsReactionBlock(rule->GetReaction()) && 0 == wcscmp(rule_tag, rule->GetGroupTag())) {
                enabled = rule->IsEnabled();
                break;
            }
        }
        policy->Release();
    }
    return enabled;
}

ma_error_t ma_selfprotect_query_status(ma_uint32_t *products) {
    if (!products) return MA_ERROR_INVALIDARG;

    *products = 0;

    AacControl *aac_ctrl = nullptr;
    int rc = ::AacControlCreate(&aac_ctrl, MA_AAC_PRODUCT_GUID);
    if (0 == rc) {
        AacActivePolicyList *policy_list = nullptr;
        rc = aac_ctrl->GetActivePolicyList(&policy_list);
        if (0 == rc) {

            if (is_some_blocking_rule_for_tag_enabled(policy_list, MA_AAC_SELF_PROTECTION_POLICY_GUID, _CRT_WIDE(MA_AAC_SELF_PROTECTION_GROUP))) {
                *products |= MA_SELFPROTECT_PRODUCT_MA;
            }

            if (is_some_blocking_rule_for_tag_enabled(policy_list, MA_SYSCORE_POLICY_GUID, _CRT_WIDE(MA_SYSCORE_GROUP_NAME))) {
                *products |= MA_SELFPROTECT_PRODUCT_SYSCORE;
            }

            policy_list->Release();
        }

        aac_ctrl->Release();
        ::AacDllUnload();
    }

    return ma_error_t(rc);
}