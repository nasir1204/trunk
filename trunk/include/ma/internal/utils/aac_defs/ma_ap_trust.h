#ifndef MA_AP_TRUST_H_INCLUDED
#define MA_AP_TRUST_H_INCLUDED

#include "ma/ma_common.h"

#ifdef MA_WINDOWS

# include <windows.h>
# define inline MA_INLINE /* Mfetrust incorrectly uses 'inline' keyword which is not valid C89 */
# include "mfetrust.h" /* to bypass VSE AP */
# undef inline

# define START_AP_EXCLUSION(x) \
    HANDLE x = MfeTrustOpen(); \
    BOOL dummy_ap_worked = (MfeApTrustProcessStart(x), MA_TRUE);
# define STOP_AP_EXCLUSION(x)     do {MfeApTrustProcessEnd(x), MfeTrustClose(x), x=0;} while(0);


#else
# define START_AP_EXCLUSION(x) 
# define STOP_AP_EXCLUSION(x)

#endif /* MA_WINDOWS */

#endif /* MA_AP_TRUST_H_INCLUDED */
