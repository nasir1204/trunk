#ifndef MA_LOG_MSG_INTERNAL_H_INCLUDED
#define MA_LOG_MSG_INTERNAL_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_log.h"
#include "ma/logger/ma_log_msg.h"

#include "ma/internal/utils/threading/ma_thread.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"

#include <time.h>
#include <string.h>

#ifdef MA_WINDOWS
#include <process.h> /* _getpid() */
#include <Windows.h>
#else
#include <errno.h>
#include <unistd.h>
#include <sys/time.h>
#endif



MA_CPP(extern "C" {)

/* time structure with milliseconds */
struct ma_log_time_s {
    ma_uint16_t   year;   /* 2012 */
    ma_uint16_t   month; /* 1-12 */
    ma_uint16_t   day;   /* 1-31 */

    ma_uint16_t   hour;   /* 0-23 */
    ma_uint16_t   minute; /* 0-59 */
    ma_uint16_t   second; /* 0-59 */

    ma_uint16_t  milliseconds; /* 0-999 */
};

struct ma_log_msg_s {
    ma_log_severity_t    severity;
    const char           *facility;

    ma_uint8_t           msg_is_wide_char; /* if set use wchar_message otherwise use utf8_message */   
    union {
        ma_wchar_t const *wchar_message;
        char const       *utf8_message;
    }                    message;

    ma_uint32_t          pid;
#ifdef MACX
    ma_thread_id_t       thread_id;
#else
    ma_uint32_t          thread_id;
#endif
    ma_log_time_t        time;
#ifdef MA_WINDOWS
    void                 *window_stn;
    DWORD                 session_id;
#else
    size_t               uid;
#endif
    char                 *machine_name;
    const char           *file_name;
    const char           *func_name;
    ma_uint32_t          line_num;
    ma_bool_t		     is_localized:1;
};


static void ma_log_time_get_localtime(ma_log_time_t *time) {
#ifdef MA_WINDOWS
    SYSTEMTIME systime;

    GetLocalTime(&systime);
    time->year = systime.wYear;
    time->month = systime.wMonth;
    time->day   = systime.wDay;

    time->hour  = systime.wHour;
    time->minute = systime.wMinute;
    time->second = systime.wSecond;

    time->milliseconds = systime.wMilliseconds;
#else
    struct tm *tm_time;
    struct timeval tv;

    if ((0==gettimeofday(&tv, 0)) && NULL != (tm_time = localtime(&tv.tv_sec))) {
        time->year          = 1900 + tm_time->tm_year;
        time->month         = 1 + tm_time->tm_mon;
        time->day           = tm_time->tm_mday;

        time->hour          = tm_time->tm_hour;
        time->minute        = tm_time->tm_min;
        time->second        = tm_time->tm_sec;

        time->milliseconds  = tv.tv_usec / 1000;
    }
#endif
}

/* initializes a log message by filling out every member except for the log message */
static void ma_log_msg_init(ma_log_msg_t *msg,
    ma_log_severity_t severity,
    const char           *module,
    const char           *file,
    int                  line,
    const char           *func) {

        ma_log_time_get_localtime(&msg->time);
        msg->facility = module;
        msg->file_name = file;
        msg->func_name = func;
        msg->line_num = line;
        msg->severity = severity;
        msg->pid = MA_MSC_SELECT(_getpid(), getpid());

        msg->thread_id = ma_gettid();

#ifdef MA_WINDOWS
        msg->window_stn = GetProcessWindowStation();
        //GetUserObjectInformation
        ProcessIdToSessionId(msg->pid, &msg->session_id);
#else
        msg->uid = geteuid();
#endif
}



/*!
Format Pattern 

; %d            date in local time in YY-MM-DD format or YYYY-MM-DD when %+d is used
; %t            time in local time , specify '+' to add ms
; %s            severity level
: %p            process name
; %P            process id , specify '+' to print in hexadecimal
; %T            thread-id , specify '+' to print in hexadecimal
; %w            window station(Only Windows)
; %S            session id(Only Windows) , specify '+' to print in hexadecimal
; %u            uid(Only *Nix)
; %f            facility or subsystem name
; %F            source filename
; %U            function name
; %l            source line number
; %m            the message

format modifiers: + to get more, i.e "%+t" for getting time in milliseconds

e.g. a format string like 
�%+d %+t %P %f.%s : %m�

would generate a log message like this
�2012-12-20 17:33:20.341 2520 msgbus.info : message sent successfully

 * formats a log message according to the specification of 'format' 
 * returns the number of characters that would have been copied into buffer if enough room was available. * 
 * can also be used to calculate how many bytes are required to store the full message by passing buffer=NULL,
 * @param buffer Buffer to receive the formatted string. If null, this function will instead calculate how many characters are needed to store the string, not including the terminating null
 * @param count Maximum number of characters to copy
 * @param format A null terminated format string, see below for the syntax
 * @param msg The message where values are extracted from 
 */
int ma_log_utils_format_msg(char *buffer, size_t count, const char *format, struct ma_log_msg_s const *msg);


/*!
Filter Pattern 
 Each rule(separated by '|' ),
 'aRule' is in the form [~]x.y where x is * or facility, y is severity string and ~means negate.    

 e.g. 
 1. MsgBus.Info 
    will allow only message from msgbus facility and severity level is greater than or equal to Info 
 2. *.Info 
    will allow all the messages whose severity level is greater than or equal to Info 
 3. ~Msgbus.*
    will allow all the messages where facility is not msgbus
 4. msgbus.info|network.debug
    will allow only messages from msgbus facility where msgbus severity level should >= Info
        OR
    will allow onlymessage from the network facility where network facility level should >=Info

 * creates a filter object according to the 'filter'.
 * ma_log_filter_accept will work according to the filter only.
 * @param filter_pattern a pattern as per specification. 
 * @param filter , a filter pointer where filter object created will be passed.
 @NOTE:It must be released using the ma_log_filter_release(); 

*/

static const char *g_severity_names[] = {"Critical", "Error", "Warning", "Notice", "Info", "Debug", "Trace" , "Perfomance"};

/**
 * returns a printable name for the specified severity level
 */
static MA_INLINE const char *ma_log_get_severity_label(ma_log_severity_t s) {
    int i = s-1;
    return g_severity_names[i<0 ? 0 : i<MA_COUNTOF(g_severity_names) ? i : MA_COUNTOF(g_severity_names)-1 ];
}

/**
 * gets the severity label from the specified text
 */
static MA_INLINE ma_log_severity_t ma_log_get_severity_from_label(const char *name) {
    int i = 0 ; 
    for(; i < MA_COUNTOF(g_severity_names)-1 ; ++i) {
        if(0 == MA_MSC_SELECT(strnicmp, strncasecmp)(name,g_severity_names[i],strlen(name))) // prefix comparison, ie "Warn" matches "Warning"
            break;
    }
    return (ma_log_severity_t) (1 + i);
}

void ma_logger_pass_message_to_visitor(ma_logger_t *logger, ma_log_msg_t *msg);

MA_CPP(})

#endif //#ifndef MA_LOG_MSG_INTERNAL_H_INCLUDED

