#ifndef MA_JSON_UTILS_H_INCLUDED
#define MA_JSON_UTILS_H_INCLUDED


#include "ma/ma_common.h"
#include "ma/ma_variant.h"
#include "ma/internal/utils/datastructures/ma_bytebuffer.h"

MA_CPP(extern "C" {)

typedef struct ma_json_s ma_json_t, *ma_json_h;

ma_error_t ma_json_create(ma_json_t **json);

ma_error_t ma_json_add_element(ma_json_t *json, const char *key, const char *value);

ma_error_t ma_json_add_object(ma_json_t *json, const char *key, const unsigned char *value, size_t size);

ma_error_t ma_json_set_table(ma_json_t *self, ma_table_t *table);

ma_error_t ma_json_get_data(ma_json_t *json, ma_bytebuffer_t *data);

ma_error_t ma_json_get_table(ma_json_t *self, ma_table_t **table);

ma_error_t ma_json_release(ma_json_t *json);


/* JSON array */
typedef struct ma_json_array_s ma_json_array_t, *ma_json_array_h;

ma_error_t ma_json_array_create(ma_json_array_t **json_array);

ma_error_t ma_json_array_add_object(ma_json_array_t *json_array, ma_json_t *json);

ma_error_t ma_json_array_set_array(ma_json_array_t *self, ma_array_t *array);

ma_error_t ma_json_array_get_data(ma_json_array_t *json_array, ma_bytebuffer_t *data);

ma_error_t ma_json_array_get_array(ma_json_array_t *self, ma_array_t **array);

ma_error_t ma_json_array_release(ma_json_array_t *json_array);

ma_bool_t ma_json_array_is_empty(ma_json_array_t *json_array);

size_t ma_json_array_size(ma_json_array_t *self);

/* JSON table  */
typedef struct ma_json_table_s ma_json_table_t, *ma_json_table_h;

ma_error_t ma_json_table_create(ma_json_table_t **json_table);

ma_error_t ma_json_table_add_object(ma_json_table_t *json_table, const char *key, ma_json_array_t *json);

ma_error_t ma_json_table_add_string(ma_json_table_t *self, const char *key, const char *value);

ma_error_t ma_json_table_get_data(ma_json_table_t *json_table, ma_bytebuffer_t *data);

ma_error_t ma_json_table_release(ma_json_table_t *json_table);

ma_bool_t ma_json_table_is_empty(ma_json_table_t *json_table);

size_t ma_json_table_size(ma_json_table_t *self);

/* JSON multi table  */
typedef struct ma_json_multi_table_s ma_json_multi_table_t, *ma_json_multi_table_h;

ma_error_t ma_json_multi_table_create(ma_json_multi_table_t **self);

ma_error_t ma_json_multi_table_add_object(ma_json_multi_table_t *self, const char *key, ma_json_table_t *json);

ma_error_t ma_json_multi_table_get_data(ma_json_multi_table_t *self, ma_bytebuffer_t *data);

ma_error_t ma_json_multi_table_release(ma_json_multi_table_t *self);

ma_bool_t ma_json_multi_table_is_empty(ma_json_multi_table_t *self);

size_t ma_json_multi_table_size(ma_json_multi_table_t *self);


void ma_trim_string_by_char(char *str, char c);

MA_CPP(})

#endif /* MA_JSON_UTILS_H_INCLUDED */

