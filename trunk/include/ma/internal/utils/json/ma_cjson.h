#ifndef MA_CJSON_H_INCLUDED
#define MA_CJSON_H_INCLUDED

#include "ma/ma_common.h"

MA_CPP(extern "C" {)

/* ma_cjson Types: */
#define ma_cjson_False  (1 << 0)
#define ma_cjson_True   (1 << 1)
#define ma_cjson_NULL   (1 << 2)
#define ma_cjson_Number (1 << 3)
#define ma_cjson_String (1 << 4)
#define ma_cjson_Array  (1 << 5)
#define ma_cjson_Object (1 << 6)
	
#define ma_cjson_IsReference 256
#define ma_cjson_StringIsConst 512

/* The ma_cjson structure: */
typedef struct ma_cjson {
	struct ma_cjson *next,*prev;	/* next/prev allow you to walk array/object chains. Alternatively, use GetArraySize/GetArrayItem/GetObjectItem */
	struct ma_cjson *child;		/* An array or object item will have a child pointer pointing to a chain of the items in the array/object. */

	int type;					/* The type of the item, as above. */

	char *valuestring;			/* The item's string, if type==ma_cjson_String */
	int valueint;				/* The item's number, if type==ma_cjson_Number */
	double valuedouble;			/* The item's number, if type==ma_cjson_Number */

	char *string;				/* The item's name string, if this item is the child of, or is in the list of subitems of an object. */
} ma_cjson;

typedef struct ma_cjson_Hooks {
      void *(*malloc_fn)(size_t sz);
      void (*free_fn)(void *ptr);
} ma_cjson_Hooks;

/* Supply malloc, realloc and free functions to ma_cjson */
extern void ma_cjson_InitHooks(ma_cjson_Hooks* hooks);


/* Supply a block of JSON, and this returns a ma_cjson object you can interrogate. Call ma_cjson_Delete when finished. */
extern ma_cjson *ma_cjson_Parse(const char *value);
/* Render a ma_cjson entity to text for transfer/storage. Free the char* when finished. */
extern char  *ma_cjson_Print(ma_cjson *item);
/* Render a ma_cjson entity to text for transfer/storage without any formatting. Free the char* when finished. */
extern char  *ma_cjson_PrintUnformatted(ma_cjson *item);
/* Render a ma_cjson entity to text using a buffered strategy. prebuffer is a guess at the final size. guessing well reduces reallocation. fmt=0 gives unformatted, =1 gives formatted */
extern char *ma_cjson_PrintBuffered(ma_cjson *item,int prebuffer,int fmt);
/* Delete a ma_cjson entity and all subentities. */
extern void   ma_cjson_Delete(ma_cjson *c);

/* Returns the number of items in an array (or object). */
extern int	  ma_cjson_GetArraySize(ma_cjson *array);
/* Retrieve item number "item" from array "array". Returns NULL if unsuccessful. */
extern ma_cjson *ma_cjson_GetArrayItem(ma_cjson *array,int item);
/* Get item "string" from object. Case insensitive. */
extern ma_cjson *ma_cjson_GetObjectItem(ma_cjson *object,const char *string);
extern int ma_cjson_HasObjectItem(ma_cjson *object,const char *string);
/* For analysing failed parses. This returns a pointer to the parse error. You'll probably need to look a few chars back to make sense of it. Defined when ma_cjson_Parse() returns 0. 0 when ma_cjson_Parse() succeeds. */
extern const char *ma_cjson_GetErrorPtr(void);
	
/* These calls create a ma_cjson item of the appropriate type. */
extern ma_cjson *ma_cjson_CreateNull(void);
extern ma_cjson *ma_cjson_CreateTrue(void);
extern ma_cjson *ma_cjson_CreateFalse(void);
extern ma_cjson *ma_cjson_CreateBool(int b);
extern ma_cjson *ma_cjson_CreateNumber(double num);
extern ma_cjson *ma_cjson_CreateString(const char *string);
extern ma_cjson *ma_cjson_CreateArray(void);
extern ma_cjson *ma_cjson_CreateObject(void);

/* These utilities create an Array of count items. */
extern ma_cjson *ma_cjson_CreateIntArray(const int *numbers,int count);
extern ma_cjson *ma_cjson_CreateFloatArray(const float *numbers,int count);
extern ma_cjson *ma_cjson_CreateDoubleArray(const double *numbers,int count);
extern ma_cjson *ma_cjson_CreateStringArray(const char **strings,int count);

/* Append item to the specified array/object. */
extern void ma_cjson_AddItemToArray(ma_cjson *array, ma_cjson *item);
extern void	ma_cjson_AddItemToObject(ma_cjson *object,const char *string,ma_cjson *item);
extern void	ma_cjson_AddItemToObjectCS(ma_cjson *object,const char *string,ma_cjson *item);	/* Use this when string is definitely const (i.e. a literal, or as good as), and will definitely survive the ma_cjson object */
/* Append reference to item to the specified array/object. Use this when you want to add an existing ma_cjson to a new ma_cjson, but don't want to corrupt your existing ma_cjson. */
extern void ma_cjson_AddItemReferenceToArray(ma_cjson *array, ma_cjson *item);
extern void	ma_cjson_AddItemReferenceToObject(ma_cjson *object,const char *string,ma_cjson *item);

/* Remove/Detatch items from Arrays/Objects. */
extern ma_cjson *ma_cjson_DetachItemFromArray(ma_cjson *array,int which);
extern void   ma_cjson_DeleteItemFromArray(ma_cjson *array,int which);
extern ma_cjson *ma_cjson_DetachItemFromObject(ma_cjson *object,const char *string);
extern void   ma_cjson_DeleteItemFromObject(ma_cjson *object,const char *string);
	
/* Update array items. */
extern void ma_cjson_InsertItemInArray(ma_cjson *array,int which,ma_cjson *newitem);	/* Shifts pre-existing items to the right. */
extern void ma_cjson_ReplaceItemInArray(ma_cjson *array,int which,ma_cjson *newitem);
extern void ma_cjson_ReplaceItemInObject(ma_cjson *object,const char *string,ma_cjson *newitem);

/* Duplicate a ma_cjson item */
extern ma_cjson *ma_cjson_Duplicate(ma_cjson *item,int recurse);
/* Duplicate will create a new, identical ma_cjson item to the one you pass, in new memory that will
need to be released. With recurse!=0, it will duplicate any children connected to the item.
The item->next and ->prev pointers are always zero on return from Duplicate. */

/* ParseWithOpts allows you to require (and check) that the JSON is null terminated, and to retrieve the pointer to the final byte parsed. */
/* If you supply a ptr in return_parse_end and parsing fails, then return_parse_end will contain a pointer to the error. If not, then ma_cjson_GetErrorPtr() does the job. */
extern ma_cjson *ma_cjson_ParseWithOpts(const char *value,const char **return_parse_end,int require_null_terminated);

extern void ma_cjson_Minify(char *json);

/* Macros for creating things quickly. */
#define ma_cjson_AddNullToObject(object,name)		ma_cjson_AddItemToObject(object, name, ma_cjson_CreateNull())
#define ma_cjson_AddTrueToObject(object,name)		ma_cjson_AddItemToObject(object, name, ma_cjson_CreateTrue())
#define ma_cjson_AddFalseToObject(object,name)		ma_cjson_AddItemToObject(object, name, ma_cjson_CreateFalse())
#define ma_cjson_AddBoolToObject(object,name,b)	ma_cjson_AddItemToObject(object, name, ma_cjson_CreateBool(b))
#define ma_cjson_AddNumberToObject(object,name,n)	ma_cjson_AddItemToObject(object, name, ma_cjson_CreateNumber(n))
#define ma_cjson_AddStringToObject(object,name,s)	ma_cjson_AddItemToObject(object, name, ma_cjson_CreateString(s))

/* When assigning an integer value, it needs to be propagated to valuedouble too. */
#define ma_cjson_SetIntValue(object,val)			((object)?(object)->valueint=(object)->valuedouble=(val):(val))
#define ma_cjson_SetNumberValue(object,val)		((object)?(object)->valueint=(object)->valuedouble=(val):(val))

/* Macro for iterating over an array */
#define ma_cjson_ArrayForEach(pos, head)			for(pos = (head)->child; pos != NULL; pos = pos->next)

MA_CPP(})

#endif
