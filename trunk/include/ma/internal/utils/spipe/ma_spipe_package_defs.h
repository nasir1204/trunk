#ifndef MA_SPIPE_PACKAGE_DEFS_H_INCLUDED
#define MA_SPIPE_PACKAGE_DEFS_H_INCLUDED

/* TBD - definitions will be reviewed and modified if necessary */

/* Package Type and Info */
#define STR_PACKAGE_TYPE                                "PackageType"
#define STR_PKGTYPE_AGENT_PUB_KEY                       "AgentPubKey"
#define STR_PKGTYPE_PROPS_VERSION                       "PropsVersion"
#define STR_PKGTYPE_REQUEST_PROPS                       "RequestProps"
#define STR_PKGTYPE_REQUEST_PUB_KEY                     "RequestPublicKey"
#define STR_PKGTYPE_PROPS                               "Props"
#define STR_PKGTYPE_FULL_PROPS                          "FullProps"
#define STR_PKGTYPE_INC_PROPS                           "IncProps"
#define STR_PKGTYPE_UPDATE_PROPS                        "UpdateProps"
#define STR_PKGTYPE_PROPS_RESPONSE                      "PropsResponse"
#define STR_PKGTYPE_EVENT                               "Event"
#define STR_PKGTYPE_MSG_AVAILABLE                       "MsgAvailable"
#define STR_PKGTYPE_MSG_REQUEST                         "MsgRequest"
#define STR_PKGTYPE_MSG_RESPONSE                        "MsgResponse"
#define STR_PKGTYPE_MSG_UPLOAD                          "MsgUpload"
#define STR_PKGTYPE_PACKAGE_REQUEST                     "PackageRequest"
#define STR_PKGTYPE_PLUGIN_DOWNLOAD_REQUEST             "PluginDownloadRequest"
#define STR_PKGTYPE_PLUGIN_DOWNLOAD_RESPONSE            "PluginDownloadResponse"
#define STR_PKGTYPE_PACKAGE_RESPONSE                    "PackageResponse"
#define STR_PKGTYPE_LEGACY_PREINSTALL_REQUEST           "LegacyPreinstallRequest"
#define STR_PKGTYPE_LEGACY_PREINSTALL_RESPONSE          "LegacyPreinstallResponse"
#define STR_PKGTYPE_AGENT_UNINSTALL                     "AgentUninstall"
#define STR_PKGTYPE_AGENT_UNINSTALL_RESPONSE            "AgentUninstResponse"
#define STR_PKGTYPE_AGENT_PING                          "AgentPing"
#define STR_PKGTYPE_SUPER_AGENT_PING                    "SuperAgentPing"
#define STR_PKGTYPE_ACTION                              "Action"
#define STR_PKGTYPE_KEY_PACKAGE                         "KeyPackage"
#define STR_PKGTYPE_SERVER_KEY_HASH                     "ServerKeyHash"

#define STR_PKGINFO_TRANSACTION_GUID                    "TransactionGUID"
#define STR_PKGINFO_PACKAGE                             "Package"
#define STR_PKGINFO_SCRIPT_FILE                         "ScriptFile"
#define STR_PKGINFO_PROPS_VERSION                       "PropsVersion"
#define STR_PKGINFO_PREV_PROPS_VERSION                  "PrevPropsVersion"
#define STR_PKGINFO_POLICY_VERSION                      "PolicyVersion"
#define STR_PKGINFO_TASK_VERSION                        "TaskVersion"


#define STR_PKGINFO_LEGACY_PREINSTALL                   "LegacyPreinstall"
#define STR_PKGINFO_SOFTWARE_ID                         "SoftwareID"
#define STR_PKGINFO_AGENT_COMPUTER_NAME                 "AgentComputerName"
#define STR_PKGINFO_PLUGIN_NAME                         "PluginName"
//#define STR_PKGTYPE_SUPER_AGENT_PING                  "SuperAgentPing"
#define STR_PKGINFO_SERVER_NAME                         "ServerName"
#define STR_PKGINFO_SERVER_TIME_STAMP                   "ServerTimeStamp"
#define STR_PKGINFO_MSG_UPLOAD_NAMES                    "Types"
#define STR_PKGINFO_USER_LIST                           "userlist"
#define STR_USER_ASSIGNMENT_COUNT						"UserAssignmentCount"
#define STR_PKGINFO_USER								"User"
#define MAX_LOGGED_ON_USERS								"9999"

#define STR_SERVER_PUB_KEY_FILE                         "srpubkey.bin"
#define STR_SERVER_REQ_SEC_KEY_FILE						"reqseckey.bin"
#define STR_SERVER_2048_PUB_KEY_FILE                    "sr2048pubkey.bin"
#define STR_SERVER_REQ_2048_SEC_KEY_FILE				"req2048seckey.bin"
#define STR_AGENT_PUB_KEY_FILE                          "agpubkey.bin"
#define STR_AGENT_SEC_KEY_FILE                          "agseckey.bin"
#define STR_AGENT_FIPS_MODE_FLAG_FILE					"agentfipsmode"
#define STR_MANAGEMENT_PROPS_SEND						"Props.xml"

#define STR_SITEINFO_VERSION                            "SiteinfoVersion"
#define STR_SITELIST_NAME                               "SiteList.xml"
#define STR_SITELIST_NAME_LOWERCASE                     "sitelist.xml"

/* Manifest based policies definitions */
#define STR_PKGINFO_SUPPORTS_MANIFEST_CONTENT           "SupportsManifestContent"
#define STR_PKGTYPE_POLICY_MANIFEST_REQUEST		        "PolicyManifestRequest"
#define STR_PKGTYPE_POLICY_MANIFEST                     "PolicyManifest"                 
#define STR_MANAGEMENT_POLICY_MANIFEST                  "Manifest.xml"

#define MA_STR_SPIPEPKG_INFO_EVENT_FILTER_VERSION		"EventFilterVersion"
#define MA_STR_SPIPEPKG_DATA_EVENT_FILTER_INI			"EvtFiltr.ini"

#define MA_STR_SPIPEPKG_DATA_REPOKEYS_INI				"RepoKeys.ini"

/* Spipe package common properties and infos */
#define STR_COMPUTER_NAME								 "ComputerName"
#define STR_DOMAIN_NAME									 "DomainName"
#define STR_AGENT_VERSION								 "AgentVersion"
#define STR_FQDN_NAME									 "FQDN"
#define STR_IP_ADDRESS									 "IPAddress"
#define STR_NET_ADDRESS									 "NETAddress"
#define STR_PLATFORM_ID									 "PlatformID"
#define STR_GUID_GEN_SUPPORTED							 "GuidRegenerationSupported"
#define STR_SUPPORTED_SPIPE_VERSION						 "SupportedSPIPEVersion"
#define STR_SUPPORT_HIGHER_KEYS 						 "Supports2048BitRSA"
#define STR_VDI											 "Vdi"
#define STR_REPO_KEY_HASH_VERSION						 "RepoKeyHashVersion"
#define STR_SEQUENCE_NO									 "SequenceNumber"
#define STR_TRANSACTION_GUID							 "TransactionGUID"
#define STR_TENANT_ID									 "TenantId"
#define STR_PKGINFO_AGENT_GUID                           "AgentGUID"

/* Agent wake-up infos */
#define STR_AGENT_PING_SEND_FULL_PROPS					 "bSendFullProps"
#define STR_AGENT_PING_RAND_INTERVAL					 "AgentPingRandInterval"
#define STR_AGENT_PING_CATALOG_VERSION                   "CatalogVersion"
#define STR_AGENT_PING_GLOBAL_UPDATE_PRODUCT_LIST        "GlobalUpdateProductID"

#endif //MA_SPIPE_PACKAGE_DEFS_H_INCLUDED

