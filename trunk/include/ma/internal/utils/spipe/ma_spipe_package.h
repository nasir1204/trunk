#ifndef MA_SPIPE_PACKAGE_H_INCLUDED
#define MA_SPIPE_PACKAGE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_log.h"
#include "ma/internal/utils/macrypto/ma_crypto.h"
#include "ma/internal/utils/datastructures/ma_stream.h"
#include "ma/internal/utils/spipe/ma_spipe_package_defs.h"

MA_CPP(extern "C" {)

/* Spipe package. */
typedef struct ma_spipe_package_s ma_spipe_package_t, *ma_spipe_package_h;

typedef enum ma_spipeversion_e {
    MA_SPIPE_VERSION1 = 0x10000001,
    MA_SPIPE_VERSION2 = 0x20000001,
    MA_SPIPE_VERSION3 = 0x30000001,
    MA_SPIPE_VERSION4 = 0x40000001,
	MA_SPIPE_VERSION5 = 0x50000001,
	MA_SPIPE_VERSION6 = 0x60000001
} ma_spipeversion_t;

typedef enum ma_spipe_package_type_e{
	MA_SPIPE_PACKAGE_TYPE_KEY=1,
	MA_SPIPE_PACKAGE_TYPE_DATA
} ma_spipe_package_type_t;

typedef enum ma_spipe_package_property_e{	
	MA_SPIPE_PROPERTY_SENDER_GUID,
	MA_SPIPE_PROPERTY_PACKAGE_GUID,
	MA_SPIPE_PROPERTY_COMPUTER_NAME,	
} ma_spipe_package_property_t;

MA_AH_CLIENT_API ma_error_t ma_spipe_package_create(ma_crypto_t *crypto, ma_spipe_package_t **spipepkg);
MA_AH_CLIENT_API ma_error_t ma_spipe_package_add_ref(ma_spipe_package_t *self);
MA_AH_CLIENT_API ma_error_t ma_spipe_package_release(ma_spipe_package_t *self);
MA_AH_CLIENT_API ma_error_t ma_spipe_package_set_logger(ma_spipe_package_t *self, ma_logger_t *logger);

/* To make the spipe package from info. Used while upload.*/
MA_AH_CLIENT_API ma_error_t ma_spipe_package_set_package_type(ma_spipe_package_t *self, ma_spipe_package_type_t);
MA_AH_CLIENT_API ma_error_t ma_spipe_package_set_header_property(ma_spipe_package_t *self, ma_spipe_package_property_t key, char *value);
MA_AH_CLIENT_API ma_error_t ma_spipe_package_add_info(ma_spipe_package_t *self, const char *key, const unsigned char *value, size_t length);
MA_AH_CLIENT_API ma_error_t ma_spipe_package_add_data(ma_spipe_package_t *self, const char *key, void *value, size_t length);
MA_AH_CLIENT_API ma_error_t ma_spipe_package_serialize(ma_spipe_package_t *self, ma_stream_t *spipestream);

/* To get the information from the spipe package. Used after download. */
MA_AH_CLIENT_API ma_error_t ma_spipe_package_deserialize(ma_spipe_package_t *self, ma_stream_t *spipestream);
MA_AH_CLIENT_API ma_error_t ma_spipe_package_get_package_type(ma_spipe_package_t *self, ma_spipe_package_type_t *package_type);
MA_AH_CLIENT_API ma_error_t ma_spipe_package_get_header_property(ma_spipe_package_t *self, ma_spipe_package_property_t key, const unsigned char **buffer, size_t *buffersize);
MA_AH_CLIENT_API ma_error_t ma_spipe_package_get_info(ma_spipe_package_t *self, const char *key, const unsigned char **value, size_t *size);
MA_AH_CLIENT_API ma_error_t ma_spipe_package_get_data(ma_spipe_package_t *self, const char *key, void **value, size_t *size);
MA_AH_CLIENT_API ma_error_t ma_spipe_package_get_data_element_count(ma_spipe_package_t *self, size_t *size);

MA_CPP(})

#endif /* MA_SPIPE_PACKAGE_H_INCLUDED */

