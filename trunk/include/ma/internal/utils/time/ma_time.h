#ifndef MA_TIME_H_INCLUDED
#define MA_TIME_H_INCLUDED

#include "ma/ma_common.h"

#ifdef MA_WINDOWS
#include <Windows.h>
#else
#include <errno.h>
#include <unistd.h>
#include <sys/time.h>
#include <unistd.h>
#endif
#include <stdio.h>
#include <time.h>

MA_CPP(extern "C" {)

typedef struct ma_time_s {
    ma_uint16_t   year;   /* 2012 */
    ma_uint16_t   month; /* 1-12 */
    ma_uint16_t   day;   /* 1-31 */

    ma_uint16_t   hour;   /* 0-23 */
    ma_uint16_t   minute; /* 0-59 */
    ma_uint16_t   second; /* 0-59 */

    ma_uint16_t  milliseconds; /* 0-999 */
} ma_time_t;

static void ma_time_get_localtime(ma_time_t *time){
	#ifdef MA_WINDOWS
    SYSTEMTIME systime;

    GetLocalTime(&systime);
    time->year = systime.wYear;
    time->month = systime.wMonth;
    time->day   = systime.wDay;

    time->hour  = systime.wHour;
    time->minute = systime.wMinute;
    time->second = systime.wSecond;

    time->milliseconds = systime.wMilliseconds;
	#else
    struct tm *tm_time;
    struct timeval tv;

    if ((0==gettimeofday(&tv, 0)) && NULL != (tm_time = localtime(&tv.tv_sec))) {
        time->year          = 1900 + tm_time->tm_year;
        time->month         = 1 + tm_time->tm_mon;
        time->day           = tm_time->tm_mday;

        time->hour          = tm_time->tm_hour;
        time->minute        = tm_time->tm_min;
        time->second        = tm_time->tm_sec;

        time->milliseconds  = tv.tv_usec / 1000;
    }
	#endif
}

static void ma_time_get_gmttime(ma_time_t *ma_time){
    time_t rawtime;
    struct tm gmt = {0};
    struct tm *tmp = NULL;
    time(&rawtime);

    #ifdef MA_WINDOWS
    gmtime_s(&gmt, &rawtime);
    #else
    if(NULL != (tmp = gmtime(&rawtime)))
        gmt = *tmp;
    #endif

	ma_time->year          = 1900 + gmt.tm_year;
    ma_time->month         = 1 + gmt.tm_mon;
    ma_time->day           = gmt.tm_mday;

    ma_time->hour          = gmt.tm_hour;
    ma_time->minute        = gmt.tm_min;
    ma_time->second        = gmt.tm_sec;
    ma_time->milliseconds  = 0;
}

static void ma_time_get_utctime(ma_time_t *ma_time){
	/* add here if needed*/
}

static ma_uint64_t ma_time_to_time_t(ma_time_t time){
    struct tm tm = {0};

    tm.tm_hour = time.hour;
    tm.tm_year = time.year-1900;
    tm.tm_mon = time.month-1;
    tm.tm_mday = time.day;
    tm.tm_min = time.minute;
    tm.tm_sec = time.second;
    tm.tm_isdst = -1;
    return mktime(&tm);
}


MA_CPP(})

#endif
