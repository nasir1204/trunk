#ifndef MA_THREAD_UNIX_H_INCLUDED
#define MA_THREAD_UNIX_H_INCLUDED

#include "ma/ma_common.h"

#include <sys/types.h>
#include <unistd.h>
#include <pthread.h>
#include <errno.h>
#include <stdlib.h>

#ifdef MA_LINUX
#include <sys/syscall.h>
#endif

#define MA_ONCE_INIT PTHREAD_ONCE_INIT

typedef pthread_mutex_t ma_mutex_t, *ma_mutex_h;
typedef pthread_once_t ma_once_t;

#ifdef MACX
typedef pthread_t ma_thread_id_t;
#else 
typedef pid_t ma_thread_id_t;
#endif

MA_CPP(extern "C" {)

/*!
* gets the current thread id
*/   
static MA_INLINE ma_thread_id_t ma_gettid() {
#if defined(MA_LINUX)
    return syscall(SYS_gettid);
#else
    //This is very linux specific and as of now it's failing ..may be we should use pthread_get_id something
    //return gettid(); /* unless Linux <  2.4.11. */
    //return getpid();
	
	//In Mac OSX pthread_self() returns pthread_t so we are explicitly casting -- Real fix might be changing the typedef of  ma_thread_id_t to pthread_t
    return (ma_thread_id_t) pthread_self();	
#endif
}

static MA_INLINE int  ma_mutex_init(ma_mutex_t* mutex) {
#ifdef MA_DEBUG
    if(pthread_mutex_init(mutex, NULL))
        return -1;
    else
        return 0;
#else
    pthread_mutexattr_t attr;
    int r;

    if(pthread_mutexattr_init(&attr))
        return -1;

    if(pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE))
        return -1;

    r = pthread_mutex_init(mutex, &attr);

    if(pthread_mutexattr_destroy(&attr))
        return -1;

    return r ? -1 : 0;
#endif    
}


static MA_INLINE int ma_mutex_destroy(ma_mutex_t* mutex) {
    if(pthread_mutex_destroy(mutex))
        return -1;
	return 0;
}


static MA_INLINE int ma_mutex_lock(ma_mutex_t* mutex) {
    if(pthread_mutex_lock(mutex))
        return -1;
	return 0;
}


static MA_INLINE int ma_mutex_trylock(ma_mutex_t* mutex) {
    int r;
    r = pthread_mutex_trylock(mutex);
    if(r && r != EBUSY && r != EAGAIN)
        return -1;
    return r ? -1 : 0 ;
}


static MA_INLINE int ma_mutex_unlock(ma_mutex_t* mutex) {
    if(pthread_mutex_unlock(mutex))
        return -1;
	return 0;
}

static MA_INLINE int ma_once(ma_once_t *guard, void (*callback)(void)) {
	if (pthread_once(guard, callback))
		return -1;
	return 0;
}

MA_CPP(})


#endif /* MA_THREAD_UNIX_H_INCLUDED */
