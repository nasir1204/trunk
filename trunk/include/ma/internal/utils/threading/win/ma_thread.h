#ifndef MA_THREAD_WIN_H_INCLUDED
#define MA_THREAD_WIN_H_INCLUDED

#include "ma/ma_common.h"

#include <Windows.h>
#include <assert.h>

#define MA_ONCE_INIT { 0, NULL }

typedef CRITICAL_SECTION ma_mutex_t , *ma_mutex_h;
typedef unsigned long ma_thread_id_t;
__declspec(dllimport) ma_thread_id_t __stdcall GetCurrentThreadId();

typedef struct ma_once_s {
  unsigned char ran;
  HANDLE event;
} ma_once_t;


MA_CPP(extern "C" {)

/*!
 * gets the current thread id
 */   
MA_INLINE ma_thread_id_t ma_gettid() {
    return GetCurrentThreadId();
}

MA_INLINE int  ma_mutex_init(ma_mutex_t* mutex) {
  InitializeCriticalSection(mutex);
  return 0;
}


MA_INLINE int ma_mutex_destroy(ma_mutex_t* mutex) {
  DeleteCriticalSection(mutex);
  return 0;
}


MA_INLINE int ma_mutex_lock(ma_mutex_t* mutex) {
  EnterCriticalSection(mutex);
  return 0;
}


MA_INLINE int ma_mutex_trylock(ma_mutex_t* mutex) {
  if (TryEnterCriticalSection(mutex))
    return 0;
  else
    return -1;
}


MA_INLINE int ma_mutex_unlock(ma_mutex_t* mutex) {
  LeaveCriticalSection(mutex);
  return 0;
}

MA_INLINE int ma_once_inner(ma_once_t* guard, void (*callback)(void)) {
  DWORD result;
  HANDLE existing_event, created_event;

  created_event = CreateEvent(NULL, 1, 0, NULL);
  if (created_event == 0) {
    /* Could fail in a low-memory situation? */
      //assert(0);
	  return -1;
  }

  existing_event = InterlockedCompareExchangePointer(&guard->event,
                                                     created_event,
                                                     NULL);

  if (existing_event == NULL) {
    /* We won the race */
    callback();

    result = SetEvent(created_event);
    if(!result) return -1;
    guard->ran = 1;

  } else {
    /* We lost the race. Destroy the event we created and wait for the */
    /* existing one todv become signaled. */
    CloseHandle(created_event);
    result = WaitForSingleObject(existing_event, INFINITE);
    if(result != WAIT_OBJECT_0) return -1;
  }
  return 0;
}

MA_INLINE int ma_once(ma_once_t *guard, void (*callback)(void)) {
  /* Fast case - avoid WaitForSingleObject. */
  if (guard->ran) {
	return 0;
  }
  return ma_once_inner(guard, callback);
}

MA_CPP(})
#endif /* MA_THREAD_WIN_H_INCLUDED */
