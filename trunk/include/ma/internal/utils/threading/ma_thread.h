#ifndef MA_THREAD_H_INCLUDED
#define MA_THREAD_H_INCLUDED

#include "ma/ma_common.h"

#if defined(MA_WINDOWS)
#include "win/ma_thread.h"
#else  // For all non-windows
#include "unix/ma_thread.h"
#endif

#endif /* MA_THREAD_H_INCLUDED */
