#ifndef MA_ATOMIC_H_INCLUDED
#define MA_ATOMIC_H_INCLUDED

#include "ma/ma_common.h"

#if defined(MA_WINDOWS)             /* Windows */
#       ifndef _INC_WINDOWS
#           include <WinSock2.h> /* include <winsock2.h> as a courtesy unless windows.h was already included   */
#       endif
#       include <windows.h>
#       define MA_ATOMIC_INCREMENT(X) InterlockedIncrement((volatile long*)&(X))
#       define MA_ATOMIC_DECREMENT(X) InterlockedDecrement((volatile long*)&(X))
typedef long ma_atomic_counter_t;
#else

typedef ma_int32_t ma_atomic_counter_t;
#if defined(__GNUC__) && ((__GNUC__*10 + __GNUC_MINOR__) < 41)

#if (defined(__SPARC__) || defined(__sparc__))

static inline ma_int32_t atomic_cas32(volatile ma_int32_t *mem, ma_int32_t with, ma_int32_t cmp)
{
   __asm__ __volatile__ ( "cas [%1],%2,%0"
                          : "+r" ( with )
                          : "r" ( mem ), "r" ( cmp ) );
   return with;
}

static inline ma_int32_t atomic_read32(volatile ma_int32_t * mem)
{
   __asm__ __volatile__ ( "membar #StoreLoad | #LoadLoad" : : : "memory" );
   return *(volatile ma_int32_t *)mem;
}

static inline ma_int32_t atomic_add32(volatile ma_int32_t * mem, ma_int32_t val)
{
   ma_int32_t nrv;
   do {
      nrv = atomic_read32(mem);
   } while (atomic_cas32(mem,nrv+val,nrv) != nrv);
   return nrv+val;
}

#       define MA_ATOMIC_INCREMENT(X) (atomic_add32 (&(X), 1))
#       define MA_ATOMIC_DECREMENT(X) (atomic_add32 (&(X), -1))

#elif AIX
#include <sys/atomic_op.h>
static inline ma_int32_t atomic_add32(volatile ma_int32_t * mem, ma_int32_t val)
{
   return fetch_and_add(mem,val) + val;   
}

#       define MA_ATOMIC_INCREMENT(X) (atomic_add32 (&(X), 1))
#       define MA_ATOMIC_DECREMENT(X) (atomic_add32 (&(X), -1))

#else
static inline ma_int32_t fetch_and_add( ma_int32_t *variable, ma_int32_t value)
{
     ma_int32_t rs;
     __asm__ __volatile__("lock; xaddl %%eax, %2;"
                  :"=a" (rs)
                  :"a" (value), "m" (*variable)
                                  :"memory");
     return (rs + value);
}

#       define MA_ATOMIC_INCREMENT(X) (fetch_and_add (&(X), 1))
#       define MA_ATOMIC_DECREMENT(X) (fetch_and_add (&(X), -1))
#endif

#elif defined(__GNUC__) && ((__GNUC__*10 + __GNUC_MINOR__) >= 41)
#       define MA_ATOMIC_INCREMENT(X) (__sync_add_and_fetch (&(X), 1))
#       define MA_ATOMIC_DECREMENT(X) (__sync_sub_and_fetch (&(X), 1))

#else
#warning Defined for atomic incr/decr is required
#   define MA_ATOMIC_INCREMENT(X) (++(X))
#   define MA_ATOMIC_DECREMENT(X) (--(X))
#endif

#endif /* defined (MA_WINDOWS) */

#endif /* MA_ATOMIC_H_INCLUDED */

