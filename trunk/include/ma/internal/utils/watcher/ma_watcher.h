#ifndef MA_WATCHER_H_INCLUDED
#define MA_WATCHER_H_INCLUDED

#include "ma/ma_common.h"

#include "ma/internal/utils/event_loop/ma_event_loop.h"

MA_CPP(extern "C" {)

/* 'Base' class for all the watcher should dervie from */

typedef struct ma_watcher_s  ma_watcher_t, *ma_watcher_h;

/* TBD - can we provide the event type either file rename or change */
typedef ma_error_t (*ma_watcher_callback_t)(ma_watcher_t *watcher, const char *name, void *cb_data);

/* Defines the C equivalent of a 'v-table' for the ma_watcher interface implementor */
typedef struct ma_watcher_methods_s {
    ma_error_t (*add_watcher)(ma_watcher_t *self, const char *name, ma_watcher_callback_t cb, ma_bool_t option, void *cb_data);
	ma_error_t (*delete_watcher)(ma_watcher_t *self, const char *name);
    ma_error_t (*release_watcher)(ma_watcher_t *self); /*< virtual destructor */
} ma_watcher_methods_t;

struct ma_watcher_s {
    ma_watcher_methods_t        const *methods;   /*< v-table */   
	ma_event_loop_t				*ma_loop;   
    void                        *data; /* watcher extension data */
};

/*!
 * Helper function for derived classes to use for initialization
 */
static MA_INLINE void ma_watcher_init(ma_watcher_t *watcher, ma_watcher_methods_t  const *methods, ma_event_loop_t *ma_loop, void *data) {
    watcher->methods = methods;
	watcher->ma_loop = ma_loop;
    watcher->data = data;
} 

/*  option values 
 *	For Registry Watcher - MA_TRUE - for default view, MA_FALSE - for Cross view
 *  For File Watcher	 - MA_TRUE - directory recursive, MA_FALSE - For non-recursive. this option can be ignored if watcher on regular file.
 *  For Process watcher  - This value can be ignored
 */
static MA_INLINE ma_error_t ma_watcher_add(ma_watcher_t *watcher, const char *name, ma_watcher_callback_t cb, ma_bool_t option, void *cb_data) {    
	if(!watcher || !name || !cb) 
		return MA_ERROR_PRECONDITION;
    return (watcher->methods) && (watcher->methods->add_watcher) ? watcher->methods->add_watcher(watcher, name, cb, option, cb_data) : MA_ERROR_NOT_SUPPORTED;
}

static MA_INLINE ma_error_t ma_watcher_delete(ma_watcher_t *watcher, const char *name) {
	if(!watcher || !name) 
		return MA_ERROR_PRECONDITION;
    return (watcher->methods) && (watcher->methods->delete_watcher) ? watcher->methods->delete_watcher(watcher, name) : MA_ERROR_NOT_SUPPORTED;
}

static MA_INLINE ma_error_t ma_watcher_release(ma_watcher_t *watcher) {
    if(!watcher) 
		return MA_ERROR_PRECONDITION;
    return (watcher->methods) && (watcher->methods->release_watcher)?  watcher->methods->release_watcher(watcher) : MA_ERROR_NOT_SUPPORTED;
}

MA_CPP(})

#endif /* MA_WATCHER_H_INCLUDED */
