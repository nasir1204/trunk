#ifndef MA_FILE_WATCHER_H_INCLUDED
#define MA_FILE_WATCHER_H_INCLUDED

#include "ma/ma_common.h"

#include "ma/internal/utils/watcher/ma_watcher.h"

MA_CPP(extern "C" {)

ma_error_t ma_file_watcher_create(ma_event_loop_t *ma_loop, ma_watcher_t **watcher);

MA_CPP(})

#endif /* MA_FILE_WATCHER_H_INCLUDED */
