#ifndef MA_REGISTRY_WATCHER_H_INCLUDED
#define MA_REGISTRY_WATCHER_H_INCLUDED

#include "ma/ma_common.h"

#include "ma/internal/utils/watcher/ma_watcher.h"
#include "ma/internal/utils/event_loop/ma_loop_worker.h"

MA_CPP(extern "C" {)

ma_error_t ma_registry_watcher_create(ma_event_loop_t *ma_loop, ma_loop_worker_t *loop_worker, ma_watcher_t **reg_watcher);

/* Available flags : REG_NOTIFY_CHANGE_NAME | REG_NOTIFY_CHANGE_ATTRIBUTES | REG_NOTIFY_CHANGE_LAST_SET | REG_NOTIFY_CHANGE_SECURITY */
ma_error_t ma_regitry_watcher_set_filter(ma_watcher_t *reg_watcher, DWORD reg_filter);

MA_CPP(})

#endif /* MA_REGISTRY_WATCHER_H_INCLUDED */
