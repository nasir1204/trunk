#ifndef MA_PROFILER_DATASTORE_H_INCLUDED
#define MA_PROFILER_DATASTORE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/profiler/ma_profiler.h"
#include "ma/internal/utils/profiler/ma_profile_info.h"
#include "ma/ma_datastore.h"
#include "ma/datastore/ma_ds_database.h"

MA_CPP(extern "C" {)
MA_PROFILE_API ma_error_t ma_profiler_datastore_read(ma_profiler_t *profiler, ma_ds_t *datastore, const char *ds_path);

MA_PROFILE_API ma_error_t ma_profiler_datastore_get(ma_profiler_t *profiler, ma_ds_t *datastore, const char *ds_path, const char *profile_id, ma_profile_info_t **info);

MA_PROFILE_API ma_error_t ma_profiler_datastore_write(ma_profiler_t *profiler, ma_profile_info_t *info, ma_ds_t *datastore, const char *ds_path);

MA_PROFILE_API ma_error_t ma_profiler_datastore_write_variant(ma_profiler_t *profiler, const char *profile_id, ma_ds_t *datastore, const char *ds_path, ma_variant_t *var);

MA_PROFILE_API ma_error_t ma_profiler_datastore_enumerate_city_pincode(ma_profiler_t *profiler, ma_ds_t *datastore, const char *ds_path, const char *pincode, const char *city, ma_variant_t **ret_var);

MA_PROFILE_API ma_error_t ma_profiler_datastore_enumerate_city_user_pincode(ma_profiler_t *profiler, ma_ds_t *datastore, const char *ds_path, const char *pincode, const char *city, ma_profile_info_user_type_t type, ma_variant_t **ret_var);

MA_PROFILE_API ma_error_t ma_profiler_datastore_remove_profile(const char *profile, ma_ds_t *datastore, const char *ds_path);

MA_PROFILE_API ma_error_t ma_profiler_datastore_clear(ma_ds_t *datastore, const char *ds_path);

MA_CPP(})


#endif /* MA_PROFILER_DATASTORE_H_INCLUDED */

