#ifndef MA_PROFILE_INFO_H_INCLUDED
#define MA_PROFILE_INFO_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_variant.h"
#include "ma/scheduler/ma_triggers.h"
#include "ma/internal/utils/board/ma_board.h"
#include "ma/internal/utils/season/ma_season_info.h"
#include "ma/internal/utils/season/ma_season.h"
#include "ma/internal/utils/board/ma_board_pins.h"
#include "ma/internal/utils/board/ma_board_topics.h"
#include "ma/internal/utils/json/ma_json_utils.h"

#define MA_PROFILE_INFO_USER_TYPE_NORMAL_STR                    "Consumer"
#define MA_PROFILE_INFO_USER_TYPE_CUSTOMER_EXECUTIVE_STR        "CustomerExecutive"
#define MA_PROFILE_INFO_USER_TYPE_OPERATION_STR                 "OperationExecutive"
#define MA_PROFILE_INFO_USER_TYPE_ADMIN_STR                     "Admin"

#define MA_PROFILE_INFO_STATUS_DELETED_STR          "deleted"
#define MA_PROFILE_INFO_STATUS_NOT_ACTIVE_STR       "inactive"
#define MA_PROFILE_INFO_STATUS_ACTIVE_STR           "active"

MA_CPP(extern "C" {)

typedef struct ma_profile_info_s ma_profile_info_t, *ma_profile_info_h;

typedef enum ma_profile_info_user_type_e {
    MA_PROFILE_INFO_USER_TYPE_NORMAL,
    MA_PROFILE_INFO_USER_TYPE_CUSTOMER_EXECUTIVE,
    MA_PROFILE_INFO_USER_TYPE_OPERATION,
    MA_PROFILE_INFO_USER_TYPE_ADMIN,
}ma_profile_info_user_type_t;

typedef enum ma_profile_info_privilege_e {
    MA_PROFILE_INFO_PRIVILEGE_RDONLY,
    MA_PROFILE_INFO_PRIVILEGE_RDWR,
}ma_profile_info_privilege_t;

#undef MA_PROFILE_INFO_STATUS_EXPANDER
#define MA_PROFILE_INFO_STATUS_EXPANDER\
    MA_PROFILE_INFO_STATUS_EXPAND(MA_PROFILE_INFO_STATUS_NOT_ACTIVE, 0, MA_PROFILE_INFO_STATUS_NOT_ACTIVE_STR)\
    MA_PROFILE_INFO_STATUS_EXPAND(MA_PROFILE_INFO_STATUS_ACTIVE, 1, MA_PROFILE_INFO_STATUS_NOT_ACTIVE_STR)\
    MA_PROFILE_INFO_STATUS_EXPAND(MA_PROFILE_INFO_STATUS_DELETED, 2, MA_PROFILE_INFO_STATUS_DELETED_STR)

typedef enum ma_profile_info_status_e {
#define MA_PROFILE_INFO_STATUS_EXPAND(status, index, status_str) status = index,
    MA_PROFILE_INFO_STATUS_EXPANDER
#undef MA_PROFILE_INFO_STATUS_EXPAND
}ma_profile_info_status_t;


/* profile info ctor */
MA_PROFILE_API ma_error_t ma_profile_info_create(ma_profile_info_t **info, ma_profile_info_user_type_t user_type);

/* profile info dtor */
MA_PROFILE_API ma_error_t ma_profile_info_release(ma_profile_info_t *info);


/* setter */
MA_PROFILE_API ma_error_t ma_profile_info_set_status_from_string(ma_profile_info_t *info, const char *str);
MA_PROFILE_API ma_error_t ma_profile_info_set_boards(ma_profile_info_t *info, const char *boards[], size_t no_of_boards);
MA_PROFILE_API ma_error_t ma_profile_info_set_email_id(ma_profile_info_t *info, const char *id);
MA_PROFILE_API ma_error_t ma_profile_info_set_user(ma_profile_info_t *info, const char *user);
MA_PROFILE_API ma_error_t ma_profile_info_set_contact(ma_profile_info_t *info, const char *contact); 
MA_PROFILE_API ma_error_t ma_profile_info_set_from_address(ma_profile_info_t *info, const char *addr);
MA_PROFILE_API ma_error_t ma_profile_info_set_from_city(ma_profile_info_t *info, const char *city);
MA_PROFILE_API ma_error_t ma_profile_info_set_to_city(ma_profile_info_t *info, const char *city);
MA_PROFILE_API ma_error_t ma_profile_info_set_to_address(ma_profile_info_t *info, const char *addr);
MA_PROFILE_API ma_error_t ma_profile_info_set_password(ma_profile_info_t *info, const char *passwd);
MA_PROFILE_API ma_error_t ma_profile_info_set_last_login(ma_profile_info_t *info, ma_uint32_t time);
MA_PROFILE_API ma_error_t ma_profile_info_set_status(ma_profile_info_t *info, ma_profile_info_status_t status);
MA_PROFILE_API ma_error_t ma_profile_info_set_source_pincode(ma_profile_info_t *info, const char *pincode);
MA_PROFILE_API ma_error_t ma_profile_info_set_dest_pincode(ma_profile_info_t *info, const char *pincode);
MA_PROFILE_API ma_error_t ma_profile_info_set_user_type(ma_profile_info_t *info, ma_profile_info_user_type_t type);
MA_PROFILE_API ma_error_t ma_profile_info_set_privilege(ma_profile_info_t *info, ma_profile_info_privilege_t priv);
MA_PROFILE_API ma_error_t ma_profile_info_set_order_count(ma_profile_info_t *info, size_t count);
MA_PROFILE_API ma_error_t ma_profile_info_set_last_seen(ma_profile_info_t *info, ma_task_time_t time);
MA_PROFILE_API ma_error_t ma_profile_info_set_boards(ma_profile_info_t *info, const char *boards[], size_t no_of_boards);
MA_PROFILE_API ma_error_t ma_profile_info_add_board(ma_profile_info_t *info, const char *board);
MA_PROFILE_API ma_error_t ma_profile_info_set_profile_url(ma_profile_info_t *info, const char *url);
MA_PROFILE_API ma_error_t ma_profile_info_set_subscribed(ma_profile_info_t *info, ma_bool_t yes);
MA_PROFILE_API ma_error_t ma_profile_info_set_gender(ma_profile_info_t *info, const char *gender);
MA_PROFILE_API ma_error_t ma_profile_info_set_dob(ma_profile_info_t *info, const char *dob);
MA_PROFILE_API ma_error_t ma_profile_info_set_gstin(ma_profile_info_t *info, const char *gst);
MA_PROFILE_API ma_error_t ma_profile_info_set_ssn(ma_profile_info_t *info, const char *ssn);
MA_PROFILE_API ma_error_t ma_profile_info_set_channel(ma_profile_info_t *info, const char *channel);

MA_PROFILE_API ma_error_t ma_profile_info_order_increment(ma_profile_info_t *info);
MA_PROFILE_API ma_error_t ma_profile_info_order_decrement(ma_profile_info_t *info);

/* getter */
MA_PROFILE_API ma_error_t ma_profile_info_get_status_str(ma_profile_info_t *info, char *str);
MA_PROFILE_API ma_error_t ma_profile_info_get_boards(ma_profile_info_t *info, const char ***boards, size_t *no_of_boards);
MA_PROFILE_API ma_error_t ma_profile_info_get_user_type_str(ma_profile_info_t *info, char *str);
MA_PROFILE_API ma_error_t ma_profile_info_get_from_city(ma_profile_info_t *info, char *city);
MA_PROFILE_API ma_error_t ma_profile_info_get_to_city(ma_profile_info_t *info, char *city);
MA_PROFILE_API ma_error_t ma_profile_info_get_privilege(ma_profile_info_t *info, ma_profile_info_privilege_t *priv);
MA_PROFILE_API ma_error_t ma_profile_info_get_user_type(ma_profile_info_t *info, ma_profile_info_user_type_t *type);
MA_PROFILE_API ma_error_t ma_profile_info_get_email_id(ma_profile_info_t *info, char *id);
MA_PROFILE_API ma_error_t ma_profile_info_get_user(ma_profile_info_t *info, char *user);
MA_PROFILE_API ma_error_t ma_profile_info_get_contact(ma_profile_info_t *info, char *contact); 
MA_PROFILE_API ma_error_t ma_profile_info_get_from_address(ma_profile_info_t *info, char *addr);
MA_PROFILE_API ma_error_t ma_profile_info_get_to_address(ma_profile_info_t *info, char *addr);
MA_PROFILE_API ma_error_t ma_profile_info_get_password(ma_profile_info_t *info, char *passwd);
MA_PROFILE_API ma_error_t ma_profile_info_get_last_login(ma_profile_info_t *info, ma_uint32_t *time);
MA_PROFILE_API ma_error_t ma_profile_info_get_status(ma_profile_info_t *info, ma_profile_info_status_t *status);
MA_PROFILE_API ma_error_t ma_profile_info_get_source_pincode(ma_profile_info_t *info, char *pincode);
MA_PROFILE_API ma_error_t ma_profile_info_get_dest_pincode(ma_profile_info_t *info, char *pincode);
MA_PROFILE_API ma_error_t ma_profile_info_get_order_count(ma_profile_info_t *info, size_t *count);
MA_PROFILE_API ma_error_t ma_profile_info_get_last_seen(ma_profile_info_t *info, ma_task_time_t *time);
MA_PROFILE_API ma_error_t ma_profile_info_get_profile_url(ma_profile_info_t *info, const char **url);
MA_PROFILE_API ma_error_t ma_profile_info_board_at(ma_profile_info_t *info, size_t index, const char **o);
MA_PROFILE_API ma_error_t ma_profile_info_board_size(ma_profile_info_t *info, size_t *size);
MA_PROFILE_API ma_error_t ma_profile_info_get_subscribed(ma_profile_info_t *info, ma_bool_t *yes);
MA_PROFILE_API ma_error_t ma_profile_info_get_gender(ma_profile_info_t *info, const char **gender);
MA_PROFILE_API ma_error_t ma_profile_info_get_dob(ma_profile_info_t *info, const char **dob);
MA_PROFILE_API ma_error_t ma_profile_info_get_gstin(ma_profile_info_t *info, const char **gst);
MA_PROFILE_API ma_error_t ma_profile_info_get_ssn(ma_profile_info_t *info, const char **ssn);
MA_PROFILE_API ma_error_t ma_profile_info_get_channel(ma_profile_info_t *info, const char **channel);

/* convert profile info to variant  and vice versa */
MA_PROFILE_API ma_error_t ma_profile_info_convert_to_variant(ma_profile_info_t *info, ma_variant_t **variant);
MA_PROFILE_API ma_error_t ma_profile_info_convert_from_variant(ma_variant_t *variant, ma_profile_info_t **info);

MA_PROFILE_API ma_error_t ma_profile_info_convert_to_json(ma_profile_info_t *info, ma_json_t **json);
MA_PROFILE_API ma_error_t ma_profile_info_convert_from_json(const char *json_str, ma_profile_info_t **pinfo);

MA_CPP(})

#endif /* MA_PROFILE_INFO_H_INCLUDED */

