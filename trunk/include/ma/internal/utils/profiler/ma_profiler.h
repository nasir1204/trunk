#ifndef MA_PROFILER_H_INCLUDED
#define MA_PROFILER_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/profiler/ma_profile_info.h"
#include "ma/ma_datastore.h"
#include "ma/datastore/ma_ds_database.h"
#include "ma/ma_log.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_variant.h"


MA_CPP(extern "C" {)

typedef struct ma_profiler_s ma_profiler_t, *ma_profiler_h;


MA_PROFILE_API ma_error_t ma_profiler_create(ma_msgbus_t *msgbus, ma_ds_t *datastore, const char *base_path, ma_profiler_t **profiler);

MA_PROFILE_API ma_error_t ma_profiler_start(ma_profiler_t *profiler);

MA_PROFILE_API ma_error_t ma_profiler_stop(ma_profiler_t *profiler);

MA_PROFILE_API ma_error_t ma_profiler_add(ma_profiler_t *profiler, ma_profile_info_t *info);

MA_PROFILE_API ma_error_t ma_profiler_add_variant(ma_profiler_t *profiler, const char *profile_id, ma_variant_t *var) ;

MA_PROFILE_API ma_error_t ma_profiler_delete(ma_profiler_t *profiler, const char *profile);

MA_PROFILE_API ma_error_t ma_profiler_delete_all(ma_profiler_t *profiler);

MA_PROFILE_API ma_error_t ma_profiler_get(ma_profiler_t *profiler, const char *profile, ma_profile_info_t **info);

MA_PROFILE_API ma_error_t ma_profiler_set_logger(ma_logger_t *logger);

MA_PROFILE_API ma_error_t ma_profiler_get_msgbus(ma_profiler_t *profiler, ma_msgbus_t **msgbus);

MA_PROFILE_API ma_error_t ma_profiler_release(ma_profiler_t *profiler);


MA_CPP(})

#endif /* MA_PROFILER_H_INCLUDED */
