#ifndef MA_ACL_H_INCLUDED
#define MA_ACL_H_INCLUDED

#include "ma/ma_common.h"

#ifdef MA_WINDOWS
#include <AccCtrl.h>
#define MA_ACL_NO_INHERITANCE  NO_INHERITANCE
#else
#define MA_ACL_NO_INHERITANCE  0
#endif



MA_CPP(extern "C" {)

typedef struct ma_acl_s ma_acl_t;

typedef enum ma_sid_s {
    MA_SID_SYSTEM = 1,
    MA_SID_LOCAL = 2,
    MA_SID_ADMIN = 3,
    MA_SID_WORLD = 4,
    MA_SID_UNKNOWN = 5,
}ma_sid_t;

typedef enum ma_acl_object_type_e {
    MA_ACL_OBJECT_FILE = 1,
    MA_ACL_OBJECT_UNKNOWN = 2,
}ma_acl_object_type_t;

MA_UTILS_API ma_error_t  ma_acl_create(ma_sid_t sid, const char *sid_name, ma_acl_t **acl);

MA_UTILS_API ma_error_t  ma_acl_add(ma_acl_t *acl, ma_bool_t allow, ma_int64_t permissions, ma_int64_t inheritance);
    
MA_UTILS_API ma_error_t  ma_acl_attach_object(ma_acl_t *acl, const char *object_name, ma_acl_object_type_t object_type, ma_bool_t allow_inheritance);

MA_UTILS_API ma_error_t  ma_acl_release(ma_acl_t *acl);

MA_CPP(})

#endif /* MA_ACL_H_INCLUDED */
