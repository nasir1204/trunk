#ifndef MA_NET_COMMON_H_INCLUDED
#define MA_NET_COMMON_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/network/ma_url_request_auth.h"
#include "ma/proxy/ma_proxy.h"
#include "ma/internal/utils/network/ma_url_request_context.h"
#include "ma/internal/utils/network/ma_url_request_range.h"
#include "ma/internal/utils/network/ma_url_request_resolve.h"

MA_CPP(extern "C" {)

#define MA_HTTP_POST_USER_AGENT_STR				"Mozilla/4.0 (compatible; SPIPE/3.0; Windows) McAfee Agent"
#define MA_HTTP_POST_USER_AGENT_WSTR			L"Mozilla/4.0 (compatible; SPIPE/3.0; Windows) McAfee Agent"
#define MA_HTTP_GET_USER_AGENT_STR				"McAfee Agent"

typedef enum ma_network_transfer_protocol_e {
    MA_URL_TRANSER_PROTOCOL_HTTP = 0,
    MA_URL_TRANSER_PROTOCOL_FTP,
	MA_URL_TRANSER_PROTOCOL_FILE,
    MA_URL_TRANSER_PROTOCOL_UNC    
}ma_network_transfer_protocol_t;

typedef enum ma_url_request_type_e {
    MA_URL_REQUEST_TYPE_POST = 0,    
    MA_URL_REQUEST_TYPE_GET,
    MA_URL_REQUEST_TYPE_RAW_CONNECT
}ma_url_request_type_t;

typedef enum ma_url_request_state_e {        
    MA_URL_REQUEST_STATE_IDLE = 0,        // Not yet started         
    MA_URL_REQUEST_STATE_INITIALIZING,    // Started, but not much more         
    MA_URL_REQUEST_STATE_RESOLVING_DNS,   // Resolving DNS          
    MA_URL_REQUEST_STATE_CONNECTING,      // Connecting to server or proxy          
    MA_URL_REQUEST_STATE_HANDSHAKING,	  // Authentication, ssl etc.          
    MA_URL_REQUEST_STATE_REQUESTING,      // Sending request         
    MA_URL_REQUEST_STATE_RECEIVING,       // Receiving response         
    MA_URL_REQUEST_STATE_COMPLETED,		  // Completed    
    MA_URL_REQUEST_STATE_CANCELLED        // Cancelled
}ma_url_request_state_t ;

typedef enum ma_url_request_option_e{
    MA_URL_REQUEST_OPTION_REQUEST_TYPE,				    // POST/GET/RAW CONNECT
    MA_URL_REQUEST_OPTION_VERBOSE_LOG,				    // (1) verbose/(0) default logging
    MA_URL_REQUEST_OPTION_TRANSFER_TIMEOUT,				// Full request transfer timeout.
	MA_URL_REQUEST_OPTION_CONNECT_TIMEOUT,				// Socket connect timeout, mandatory in case of RAW connect.
	MA_URL_REQUEST_OPTION_USE_FRESH_CONNECTION			// Will enable the curl options CURLOPT_FRESH_CONNECT and CURLOPT_FORBID_REUSE, applicable only for HTTP GET
}ma_url_request_option_t ;

struct ma_url_request_connection_info_s{
	ma_uint16_t		port;
	ma_bool_t		is_secure_port;
    char			*address;	
	ma_proxy_list_t *proxy_list;
};

typedef struct ma_url_request_s ma_url_request_t, *ma_url_request_h;
typedef struct ma_url_request_connection_info_s ma_url_request_connection_info_t;
typedef void (*ma_url_request_finalize_callback_t)(ma_error_t status, long response_code , void *userdata , ma_url_request_t *net_request ) ;
typedef ma_error_t (*ma_url_request_connect_callback_t)(void *userdata , ma_url_request_t *net_request ) ;

MA_CPP(})


#endif
