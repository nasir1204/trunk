#ifndef MA_URL_REQUEST_RANGE_H_INCLUDED
#define MA_URL_REQUEST_RANGE_H_INCLUDED

#include "ma/ma_common.h"

MA_CPP(extern "C" {)

typedef struct ma_url_request_range_s ma_url_request_range_t, *ma_url_request_range_h ;

struct ma_url_request_range_s {
    unsigned long start_position ;
    unsigned long end_position ;
} ;

#define MA_URL_RANGE_INIT(ma_url_request_range ) \
            ma_url_request_range->start_position = 0, ma_url_request_range->end_position = 0;

#define MA_URL_RANGE_SET_START_RANGE_INFO(ma_url_request_range, start) \
            ma_url_request_range->start_position = start, ma_url_request_range->end_position = 0 ;

#define MA_URL_RANGE_SET_RANGE_INFO(ma_url_request_range, start, end) \
            ma_url_request_range->start_position = start, ma_url_request_range->end_position = end ;



MA_CPP(})
#endif //MA_URL_REQUEST_RANGE_H_INCLUDED

