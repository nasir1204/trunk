#ifndef MA_URL_REQUEST_AUTH_H_INCLUDED
#define MA_URL_REQUEST_AUTH_H_INCLUDED

#include "ma/ma_common.h"

MA_CPP(extern "C" {)

typedef struct ma_url_request_auth_s ma_url_request_auth_t, *ma_url_request_auth_h ;
typedef struct ma_url_request_ssl_info_s ma_url_request_ssl_info_t, *ma_url_request_ssl_info_h ;

struct ma_url_request_ssl_info_s{
    char *certificate_path ;
};
struct ma_url_request_auth_s{
	char *domainname;
    char *user;	
    char *password;
	ma_bool_t use_loggedOn_user_credentials;
} ;

#define MA_URL_AUTH_INIT(ma_url_request_auth ) \
			ma_url_request_auth->user = NULL, ma_url_request_auth->domainname = NULL, ma_url_request_auth->password = NULL, ma_url_request_auth->use_loggedOn_user_credentials = MA_FALSE;  

#define MA_URL_AUTH_SET_AUTH_INFO(ma_url_request_auth, user_name, passwd) \
            ma_url_request_auth->user = strdup(user_name), ma_url_request_auth->password = strdup(passwd);

#define MA_URL_AUTH_SET_UNC_AUTH_INFO(ma_url_request_auth, domain_name, user_name, passwd, use_loggedOn_credentials ) \
            ma_url_request_auth->user = strdup(user_name), ma_url_request_auth->domainname = strdup(domain_name), ma_url_request_auth->password = strdup(passwd), ma_url_request_auth->use_loggedOn_user_credentials = use_loggedOn_credentials ;

#define MA_URL_AUTH_SET_LOCAL_AUTH_INFO(ma_url_request_auth, domain_name, user_name, passwd, use_loggedOn_credentials ) MA_URL_AUTH_SET_UNC_AUTH_INFO(ma_url_request_auth, domain_name, user_name, passwd, use_loggedOn_credentials)            

#define MA_URL_AUTH_DEINIT(ma_url_request_auth ) \
            if(ma_url_request_auth->user) free(ma_url_request_auth->user) ; if(ma_url_request_auth->domainname) free(ma_url_request_auth->domainname) ;  if(ma_url_request_auth->password) free(ma_url_request_auth->password); ma_url_request_auth->use_loggedOn_user_credentials = MA_FALSE; 

#define MA_URL_SSL_INIT(ma_url_request_ssl ) \
            ma_url_request_ssl->certificate_path = NULL;

#define MA_URL_AUTH_SET_SSL_INFO(ma_url_request_ssl, cert) \
            ma_url_request_ssl->certificate_path = strdup(cert) ;

#define MA_URL_SSL_DEINIT(ma_url_request_ssl ) \
            if(ma_url_request_ssl->certificate_path) free(ma_url_request_ssl->certificate_path) ;

MA_CPP(})
#endif //MA_URL_REQUEST_AUTH_H_INCLUDED

