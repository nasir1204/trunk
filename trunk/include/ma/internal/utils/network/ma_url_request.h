#ifndef MA_URL_REQUEST_H_INCLUDED
#define MA_URL_REQUEST_H_INCLUDED

#include "ma/internal/utils/network/ma_net_common.h"
#include "ma/internal/utils/network/ma_net_client_service.h"

MA_CPP(extern "C" {)

/**
 * creates a new url request
 * @param net_service[IN] network service is used to perform the URL request, if user pass it NULL, then the default implemention in the network libraries used. 
 * @param url[IN] URL of the URL request.
 * @param url_request[out] URL request object.
 */

MA_NETWORK_API  ma_error_t ma_url_request_create(ma_net_client_service_t *net_service, const char *url, ma_url_request_t **url_request) ;


/**
 * increases the reference
 * @param url_request[IN] URL request object.
 **/
MA_NETWORK_API ma_error_t ma_url_request_add_ref(ma_url_request_t *url_request);


/**
 * cleans up any and all resources associated with the specified request. If a request is in progress it will be canceled
 * @param url_request[IN] URL request object.
 **/
MA_NETWORK_API ma_error_t ma_url_request_release(ma_url_request_t *url_request);

/**
 * Set option for the request as per ma_url_request_option_t
 * @param url_request[IN] URL request object.
 * @param option[IN] set the URL request option.
 * @param option_value[IN] option value.
**/
MA_NETWORK_API ma_error_t ma_url_request_set_option(ma_url_request_t *url_request, ma_url_request_option_t option, unsigned long option_value) ;  


/**
 * Set URL for the request
 * @param url_request[IN] URL request object.
 * @param url[IN] URL.
**/
MA_NETWORK_API ma_error_t ma_url_request_set_url(ma_url_request_t *url_request, const char *url) ;  

/**
 * Set post fields for POST request
 * @param url_request[IN] URL request object.
 * @param post_fileds[IN] post_fileds.
**/
MA_NETWORK_API ma_error_t ma_url_request_set_post_fields(ma_url_request_t *url_request, const char *post_fileds) ;  

/**
 * Set custom header in key value pair
 * @param url_request[IN] URL request object.
 * @param key		 [IN] key 
 * @param key		 [IN] value
 @NOTE - In this version it will override the exitsting custom header, we can extend it to be list in future

**/
MA_NETWORK_API ma_error_t ma_url_request_set_custom_header(ma_url_request_t *url_request, const char *key, const char *value) ;  

/**
 * Set URL credentials if any.
 * @param url_request[IN] URL request object.
 * @param ma_url_request_auth_t[IN] URL credentials. 
**/

MA_NETWORK_API ma_error_t ma_url_request_set_url_auth_info(ma_url_request_t *url_request, ma_url_request_auth_t *authenticate) ;

/**
 * Set URL SSL info, needed in case if HTTPs request.
 * @param url_request[IN] URL request object.
 * @param ssl_info[IN] SSL info (certificate...). 
**/
MA_NETWORK_API ma_error_t ma_url_request_set_ssl_info(ma_url_request_t *url_request, ma_url_request_ssl_info_t *ssl_info) ;

/**
 * Set RANGE info, in case of RANGED download.
 * @param url_request[IN] URL request object.
 * @param range_info[IN] Download range. 
**/
MA_NETWORK_API ma_error_t ma_url_request_set_range_info(ma_url_request_t *url_request, ma_url_request_range_t *range_info) ;


/**
 * Set URL proxy info.
 * @param url_request[IN] URL request object.
 * @param proxy[IN] proxy info object. 
**/
MA_NETWORK_API ma_error_t ma_url_request_set_proxy_info(ma_url_request_t *url_request, ma_proxy_list_t *proxy_list) ;

/**
 * Set URL resolve info.
 * @param url_request[IN] URL request object.
 * @param resolve[IN] resolve info object. 
**/
MA_NETWORK_API ma_error_t ma_url_request_set_resolve_info(ma_url_request_t *url_request, ma_url_request_resolve_t *resolve_info) ;

/**
 * Set URL request io callback.
 * @param url_request[IN] URL request object.
 * @param ma_url_request_io_t[IN] data io apis(read_cb,write_cb,header_read_cb). 
**/
MA_NETWORK_API ma_error_t ma_url_request_set_io_callback(ma_url_request_t *url_request, ma_url_request_io_t *rw_cbs) ;

/**
 * Set URL request progress callback.
 * @param url_request[IN] URL request object.
 * @param progress_cb[IN] request progress callback. 
**/
MA_NETWORK_API ma_error_t ma_url_request_set_progress_callback(ma_url_request_t *url_request, ma_url_request_progress_t *progress_cb) ;

/**
 * Set URL request connection callback.
 * @param url_request[IN] URL request object.
 * @param connect_cb[IN] request connect callback. 
**/
MA_NETWORK_API ma_error_t ma_url_request_set_connect_callback(ma_url_request_t *self, ma_url_request_connect_callback_t connect_cb) ;

/**
 * Send the request in asynchronous way.
 * @param url_request[IN] URL request object.
 * @param completion_cb[IN] request final callback, called once the request finishes.
 * @param userdata[IN] user data pointer.
 */
MA_NETWORK_API ma_error_t ma_url_request_send(ma_url_request_t *url_request, ma_url_request_finalize_callback_t completion_cb, void *userdata) ;

/**
 * Send the request in synchronous way.
 * @param url_request[IN] URL request object.
 */
MA_NETWORK_API ma_error_t ma_url_request_sync_send(ma_url_request_t *url_request, void *userdata) ;

/**
 * Query the state of the request.
 * @param url_request[IN] URL request object.
 * @param ma_url_request_state_t[OUT] request state. 
 */
MA_NETWORK_API ma_error_t ma_url_request_query_state(ma_url_request_t *url_request, ma_url_request_state_t *state);

/**
 * Cancel the request.
 * @param url_request[IN] URL request object.
**/
MA_NETWORK_API ma_error_t ma_url_request_cancel(ma_url_request_t *url_request);

/**
 * Restart the request
 * @param url_request[IN] URL request object.
**/
MA_NETWORK_API ma_error_t ma_url_request_restart(ma_url_request_t *url_request) ;

/**
 * Get the response message.
 * @param response_code[IN] response code received in the final callback.
 * @param message[OUT] message to print.
**/
MA_NETWORK_API ma_error_t ma_url_response_message(long response_code, const char **message);

MA_NETWORK_API ma_error_t ma_url_request_get_connection_info(ma_url_request_t *url_request, ma_url_request_connection_info_t **connection) ;

MA_NETWORK_API ma_error_t ma_url_request_release_connection_info(ma_url_request_connection_info_t *connection) ;

MA_NETWORK_API ma_error_t ma_url_request_get_source_ip_info(ma_url_request_t *url_request, char **source_ip) ;

MA_NETWORK_API ma_bool_t ma_url_request_get_result(ma_url_request_t *self, long response_code);

MA_CPP(})

#endif  //MA_URL_REQUEST_H_INCLUDED
