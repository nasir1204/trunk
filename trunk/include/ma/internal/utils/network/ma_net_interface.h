#ifndef MA_NET_INTERFACE_H
#define MA_NET_INTERFACE_H

#include "ma/ma_common.h"
#include "ma/internal/utils/datastructures/ma_slist.h"

#ifndef MA_WINDOWS
#include <sys/types.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <errno.h>
#else
#include <ws2tcpip.h>
#endif

#define MA_MAC_ADDRESS_SIZE				6
#define MA_FORMATTED_MAC_ADDRESS_SIZE	13
#define MA_IPV6_ADDR_LEN				128
#define MA_INTERFACE_NAME_SIZ			256

MA_CPP( extern "C"{ )

typedef enum ma_ip_family_e {
	MA_IPFAMILY_UNKNOWN,
	MA_IPFAMILY_IPV4,
	MA_IPFAMILY_IPV6,
	MA_IPFAMILY_DUAL
}ma_ip_family_t;


typedef struct ma_net_address_info_s ma_net_address_info_t, *ma_net_address_info_h;
typedef struct ma_net_interface_s ma_net_interface_t, *ma_net_interface_h;
typedef struct ma_net_interface_list_s ma_net_interface_list_t, *ma_net_interface_list_h; 

struct ma_net_address_info_s {
	MA_SLIST_NODE_DEFINE(struct ma_net_address_info_s);
	// holds the ip address
	char ip_addr[MA_IPV6_ADDR_LEN];
	// holds the subnet mask for the concerned ipv4 address for ipv6, a dummy mask is entered
	char subnet_mask[MA_IPV6_ADDR_LEN];	
	// the subnet address for the ipv4
	char subnet_addr[MA_IPV6_ADDR_LEN];
	// the broadcast address for the ipv4
	char broadcast_addr[MA_IPV6_ADDR_LEN];
	// The scope id information for ipv6 otherwise 0
	ma_uint32_t ipv6_scope_id;
	// the family that the IP belongs to IPv4 or IPv6
	ma_ip_family_t family;
};

struct ma_net_interface_s {
	MA_SLIST_NODE_DEFINE(struct ma_net_interface_s);
	/*
		List of address. 
		Every Nic will have a maximum of 
			1) one IPv4 address 
			2) two IPv6 address (Link-Local & Global IP[if assigned])
	*/
	MA_SLIST_DEFINE(addresses,ma_net_address_info_t);
	
	unsigned char interface_name[MA_INTERFACE_NAME_SIZ];		// the name of the interface

	unsigned char mac_address[MA_MAC_ADDRESS_SIZE];		// The mac address associated with an interface
	
	ma_uint16_t index;			// the index id of the interface
	
	ma_ip_family_t family;		// the network family, DUAL represents a dual-stack
};

struct ma_net_interface_list_s {

	MA_SLIST_DEFINE(interfaces,ma_net_interface_t);

	ma_bool_t	b_loopback;		//Whether to include the loopback interface or not. default is false
	
	ma_ip_family_t family;		// the network family of the system
								// IPv4 represents a system with only ipv4 nic(s)
								// IPv6 represents a system with only ipv6 nic(s)
								// DUAL represents a system which has both IPv4 or IPv6 address in one nic or across multiple nics.
};

/**
 * Helper functions
 */

MA_NETWORK_API ma_error_t ma_format_mac_address(unsigned char mac_address[MA_MAC_ADDRESS_SIZE], unsigned char formatted_mac_address[MA_FORMATTED_MAC_ADDRESS_SIZE]);

MA_NETWORK_API ma_error_t expand_ipv6_address(char retAddr[MA_IPV6_ADDR_LEN], const char ip[MA_IPV6_ADDR_LEN]);

MA_NETWORK_API ma_bool_t ma_compare_with_peer_address(const char *addr, const char *peer_addr);

MA_NETWORK_API ma_bool_t ma_format_v6_addr_with_scope(ma_net_interface_list_t *list, char *str_url, char *str_out_url, ma_bool_t b_for_curl);

MA_NETWORK_API int ma_create_addrinfo_struct(ma_net_interface_list_t *list, char *addr, char *port, struct addrinfo **res, ma_bool_t b_ipv4);

MA_NETWORK_API void ma_format_v6_addr(char *str_url, char *str_out_url, ma_bool_t b_for_curl);

MA_CPP(})

#endif //MA_NET_INTERFACE_H 
