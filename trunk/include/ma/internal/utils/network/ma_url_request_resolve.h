#ifndef MA_URL_REQUEST_RESOLVE_H_INCLUDED
#define MA_URL_REQUEST_RESOLVE_H_INCLUDED

#include "ma/ma_common.h"

MA_CPP(extern "C" {)

typedef struct ma_url_request_resolve_s ma_url_request_resolve_t, *ma_url_request_resolve_h ;

struct ma_url_request_resolve_s {
    char *host_name ;
    ma_uint32_t port;
    char *ip_address;
};

#define MA_URL_RESOLVE_INIT(ma_url_request_resolve) \
            ma_url_request_resolve->host_name = NULL, ma_url_request_resolve->port = 0, ma_url_request_resolve->ip_address = NULL;

#define MA_URL_RESOLVE_SET_INFO(ma_url_request_resolve, host, port_id, ip) \
            ma_url_request_resolve->host_name = strdup(host), ma_url_request_resolve->port = port_id, ma_url_request_resolve->ip_address = strdup(ip); 

#define MA_URL_RESOLVE_DEINIT(ma_url_request_resolve) \
            if(ma_url_request_resolve->host_name) free(ma_url_request_resolve->host_name) ; ma_url_request_resolve->port = 0; if(ma_url_request_resolve->ip_address) free(ma_url_request_resolve->ip_address) ;

MA_CPP(})

#endif /* MA_URL_REQUEST_RESOLVE_H_INCLUDED */

