#ifndef MA_URL_REQUEST_BASE_H_INCLUDED
#define MA_URL_REQUEST_BASE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/network/ma_url_request.h"
#include "ma/internal/utils/threading/ma_atomic.h"

#include <string.h>

MA_CPP(extern "C" {)

typedef struct ma_url_request_methods_s{    

	ma_error_t (*release)(ma_url_request_t *url_request);

	ma_error_t (*set_url_auth_info)(ma_url_request_t *network_request, ma_url_request_auth_t *authenticate) ;

	ma_error_t (*set_ssl_info)(ma_url_request_t *network_request, ma_url_request_ssl_info_t *ssl_info);

	ma_error_t (*set_range_info)(ma_url_request_t *network_request, ma_url_request_range_t *range_info);

	ma_error_t (*set_proxy_info)(ma_url_request_t *network_request, ma_proxy_list_t *proxy) ;

    ma_error_t (*set_resolve_info)(ma_url_request_t *network_request, ma_url_request_resolve_t *resolve_info) ;

}ma_url_request_methods_t;


typedef struct ma_url_custom_header_s {
	char		*key;
	char		*value;
}ma_url_custom_header_t;

struct ma_url_request_s{    

	ma_url_request_methods_t const *methods;

    int verbose_log ;

    unsigned long transfer_timeout ;

	unsigned long connect_timeout ;

	unsigned long use_fresh_connection;

    ma_network_transfer_protocol_t  protocol ;  /* HTTP/FTP/UNC/LOCAL */

    ma_url_request_type_t   request_type ;      /* POST/PUT/GET...   */

	ma_url_request_progress_t *progress_cb ;

    ma_url_request_io_t *io_cbs ;

    ma_url_request_connect_callback_t connect_cb ;

    ma_url_request_finalize_callback_t final_cb ;

    ma_url_request_state_t request_state ;

	char *url ;                                 

	char *post_fields ;                         

	ma_url_custom_header_t	custom_header;

    void *userdata ;

	ma_url_request_handler_t *url_handler;

    const void *cryptc_context;

    ma_atomic_counter_t ref_count;
};

/*!
 * Helper function for derived classes to use for initialization
 */
static MA_INLINE void ma_url_request_init(ma_url_request_t *url_request, const char *url, ma_url_request_methods_t  const *methods, int ref_count) {
    url_request->methods = methods;
    url_request->ref_count = ref_count;
    url_request->url = strdup(url);
} 
MA_CPP(})

#endif  /*MA_URL_REQUEST_BASE_H_INCLUDED*/

