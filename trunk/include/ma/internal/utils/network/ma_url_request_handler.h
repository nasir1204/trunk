#ifndef MA_URL_REQUEST_HANDLER_INCLUDED_H
#define MA_URL_REQUEST_HANDLER_INCLUDED_H

#include "ma/internal/utils/network/ma_net_common.h"
#include "ma/internal/utils/threading/ma_atomic.h"


MA_CPP(extern "C" {)

#if defined (MA_NETWORK_USE_CURL) || defined(MA_NETWORK_USE_ALL)
#define MA_CURL_REQUEST_HANDLER_CREATE(A,B) \
       MA_OK == ( err = ma_curl_request_handler_create(A->ev_loop, &B)) &&\
       MA_OK == ( err = ma_url_request_handlers_add_rh(A->rh_container, MA_URL_TRANSER_PROTOCOL_HTTP, B) ) &&\
       MA_OK == ( err = ma_url_request_handlers_add_rh(A->rh_container, MA_URL_TRANSER_PROTOCOL_FTP, B) ) 
#else
    #define MA_CURL_REQUEST_HANDLER_CREATE(A,B)   !MA_OK
#endif

#if defined (MA_NETWORK_USE_UNC) || defined(MA_NETWORK_USE_ALL)
#define MA_UNC_REQUEST_HANDLER_CREATE(A, B) \
        MA_OK == ( err = ma_unc_request_handler_create(A->ev_loop, &B)) &&\
        MA_OK == ( err = ma_url_request_handlers_add_rh(A->rh_container, MA_URL_TRANSER_PROTOCOL_UNC , B) )
#else
    #define MA_UNC_REQUEST_HANDLER_CREATE(A,B)    !MA_OK
#endif 

#if defined (MA_NETWORK_USE_LOCAL) || defined(MA_NETWORK_USE_ALL)
#define MA_LOCAL_REQUEST_HANDLER_CREATE(A, B) \
        MA_OK == ( err = ma_local_request_handler_create(A->ev_loop, &B)) &&\
		MA_OK == ( err = ma_url_request_handlers_add_rh(A->rh_container, MA_URL_TRANSER_PROTOCOL_FILE , B) )
#else
    #define MA_LOCAL_REQUEST_HANDLER_CREATE(A,B)    !MA_OK
#endif 


#if defined (MA_NETWORK_USE_MOCK) 
#define MA_MOCK_REQUEST_HANDLER_CREATE(A,B) \
       MA_OK == ( err = ma_mock_request_handler_create(A->ev_loop, &B)) &&\
       MA_OK == ( err = ma_url_request_handlers_add_rh(A->rh_container, MA_URL_TRANSER_PROTOCOL_HTTP, B) ) &&\
       MA_OK == ( err = ma_url_request_handlers_add_rh(A->rh_container, MA_URL_TRANSER_PROTOCOL_FTP, B) ) &&\
       MA_OK == ( err = ma_url_request_handlers_add_rh(A->rh_container, MA_URL_TRANSER_PROTOCOL_FILE, B) )
#else
    #define MA_MOCK_REQUEST_HANDLER_CREATE(A,B)   !MA_OK
#endif

typedef struct ma_url_request_handler_s ma_url_request_handler_t, *ma_url_request_handler_h;

struct ma_url_request_handler_methods_s {	
	ma_error_t (*start)(ma_url_request_handler_t *self);
	ma_error_t (*stop)(ma_url_request_handler_t *self);
	ma_error_t (*is_running)(ma_url_request_handler_t *self, ma_bool_t *is_runnsing);
	ma_error_t (*release)(ma_url_request_handler_t *self);		
	ma_error_t (*create_request)(ma_url_request_handler_t *self, const char *url, ma_url_request_t **url_request ) ;
	ma_error_t (*submit_request)(ma_url_request_handler_t *self, ma_url_request_t *url_request , ma_url_request_finalize_callback_t completion_cb, void *userdata) ;
	ma_error_t (*remove_request)(ma_url_request_handler_t *self, ma_url_request_t *url_request ) ;
	ma_error_t (*query_request_state)(ma_url_request_handler_t *self, ma_url_request_t *url_request, ma_url_request_state_t *state) ;	
};

struct ma_url_request_handler_s {
	struct ma_url_request_handler_methods_s const *vtable;
    ma_atomic_counter_t ref_count ;
};

ma_error_t ma_url_request_handler_start(ma_url_request_handler_t *self);
ma_error_t ma_url_request_handler_stop(ma_url_request_handler_t *self);
ma_error_t ma_url_request_handler_is_running(ma_url_request_handler_t *self, ma_bool_t *is_runnsing);
ma_error_t ma_url_request_handler_release(ma_url_request_handler_t *self);		
ma_error_t ma_url_request_handler_create_request(ma_url_request_handler_t *self, const char *url, ma_url_request_t **url_request ) ;
ma_error_t ma_url_request_handler_submit_request(ma_url_request_handler_t *self, ma_url_request_t *url_request , ma_url_request_finalize_callback_t completion_cb, void *userdata) ;
ma_error_t ma_url_request_handler_remove_request(ma_url_request_handler_t *self, ma_url_request_t *url_request ) ;
ma_error_t ma_url_request_handler_query_request_state(ma_url_request_handler_t *self, ma_url_request_t *url_request, ma_url_request_state_t *state) ;	

typedef struct ma_url_request_handlers_s ma_url_request_handlers_t, *ma_url_request_handlers_h;
ma_error_t ma_url_request_handlers_create(ma_url_request_handlers_t**);
ma_error_t ma_url_request_handlers_release(ma_url_request_handlers_t*);

ma_error_t ma_url_request_handlers_start_all(ma_url_request_handlers_t*);
ma_error_t ma_url_request_handlers_stop_all(ma_url_request_handlers_t*);

ma_error_t ma_url_request_handlers_add_rh(ma_url_request_handlers_t*,ma_network_transfer_protocol_t protocol,ma_url_request_handler_t *handler);
ma_error_t ma_url_request_handlers_remove_rh(ma_url_request_handlers_t*,ma_network_transfer_protocol_t protocol,ma_url_request_handler_t **handler);
ma_error_t ma_url_request_handlers_get_rh(ma_url_request_handlers_t*,ma_network_transfer_protocol_t protocol,ma_url_request_handler_t **handler);

MA_CPP(})

#endif  //MA_NET_CLIENT_SERVICE_INCLUDED_H
