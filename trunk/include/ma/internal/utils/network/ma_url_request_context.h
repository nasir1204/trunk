#ifndef MA_URL_REQUEST_CONTEXT_H_INCLUDED
#define MA_URL_REQUEST_CONTEXT_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/datastructures/ma_stream.h"

MA_CPP(extern "C" {)

typedef struct ma_url_request_io_s ma_url_request_io_t, *ma_url_request_io_h;
typedef struct ma_url_request_progress_s ma_url_request_progress_t, *ma_url_request_progress_h;

struct ma_url_request_io_s{        
    ma_stream_t *read_stream;
	ma_stream_t *write_stream;
};

typedef int (*progress_callback_t)(double dltotal, double dlnow, double ultotal, double ulnow,void *userdata);
struct ma_url_request_progress_s{    
    progress_callback_t progress_callback;
};

#define MA_URL_REQUEST_IO_INIT(ma_url_request_io) \
        ma_url_request_io->read_stream = NULL, ma_url_request_io->write_stream = NULL ;
        
#define MA_URL_REQUEST_IO_SET_STREAMS(ma_url_request_io, read_cb, write_cb) \
        ma_url_request_io->write_stream = write_cb, ma_url_request_io->read_stream = read_cb;

#define MA_URL_REQUEST_IO_SET_READ_STREAM(ma_url_request_io, read_cb) \
        ma_url_request_io->read_stream = read_cb ;

#define MA_URL_REQUEST_IO_SET_WRITE_STREAM(ma_url_request_io, write_cb) \
        ma_url_request_io->write_stream = write_cb ;

#define MA_URL_REQUEST_PROGRESS_INIT(ma_url_request_progress) \
        ma_url_request_progress->progress_callback = NULL ;
        
#define MA_URL_REQUEST_PROGRESS_SET_CB(ma_url_request_progress, progress_cb) \
        ma_url_request_progress->progress_callback = progress_cb ;

MA_CPP(})

#endif //MA_URL_REQUEST_H_INCLUDED

