#ifndef MA_NET_CLIENT_SERVICE_H_INCLUDED
#define MA_NET_CLIENT_SERVICE_H_INCLUDED

#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/utils/network/ma_net_common.h"
#include "ma/internal/utils/network/ma_url_request_handler.h"
#include "ma/ma_log.h"

MA_CPP(extern "C" {)



typedef struct ma_net_client_service_s ma_net_client_service_t, *ma_net_client_service_h;

MA_NETWORK_API ma_error_t ma_net_client_service_create(ma_event_loop_t*, ma_net_client_service_t**);
MA_NETWORK_API ma_error_t ma_net_client_service_release(ma_net_client_service_t*);
MA_NETWORK_API ma_error_t ma_net_client_service_start(ma_net_client_service_t*);
MA_NETWORK_API ma_error_t ma_net_client_service_stop(ma_net_client_service_t*);
MA_NETWORK_API ma_error_t ma_net_client_service_setlogger(ma_net_client_service_t*, ma_logger_t *) ;

MA_CPP(})

#endif

