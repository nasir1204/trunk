#ifndef MA_CONF_LOG_H_INCLUDED
#define MA_CONF_LOG_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"

MA_CPP(extern "C" {)

MA_UTILS_API ma_error_t ma_conf_get_agent_log_file_name(ma_temp_buffer_t *agent_log_file_name);

MA_UTILS_API ma_error_t ma_conf_get_common_services_log_file_name(ma_temp_buffer_t *cmn_svc_log_file_name);

MA_UTILS_API ma_error_t ma_conf_get_compat_service_log_file_name(ma_temp_buffer_t *compat_svc_log_file_name);

MA_CPP(})

#endif /* MA_CONF_LOG_H_INCLUDED */
