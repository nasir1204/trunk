#ifndef MA_CONF_H_INCLUDED
#define MA_CONF_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"

MA_CPP(extern "C" {)

MA_UTILS_API ma_error_t ma_conf_get_install_path(ma_temp_buffer_t *install_path);

MA_UTILS_API ma_error_t ma_conf_get_data_path(ma_temp_buffer_t *data_path);

MA_UTILS_API ma_error_t ma_conf_get_logs_path(ma_temp_buffer_t *logs_path);

MA_UTILS_API ma_error_t ma_conf_get_keystore_path(ma_temp_buffer_t *keystore_path);

MA_UTILS_API ma_error_t ma_conf_get_certstore_path(ma_temp_buffer_t *certstore_path);

MA_UTILS_API ma_error_t ma_conf_get_db_path(ma_temp_buffer_t *db_path);

MA_UTILS_API ma_error_t ma_conf_get_lib_path(ma_temp_buffer_t *lib_path);

MA_UTILS_API ma_error_t ma_conf_get_crypto_lib_path(ma_temp_buffer_t *crypto_lib_path);

MA_UTILS_API ma_error_t ma_conf_get_tools_lib_path(ma_temp_buffer_t *tools_lib_path);

MA_UTILS_API ma_error_t ma_conf_get_rsdk_lib_path(ma_temp_buffer_t *rsdk_lib_path);
MA_CPP(})

#endif /* MA_CONF_H_INCLUDED */
