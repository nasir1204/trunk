#ifndef MA_CONF_APPLICATION_H_INCLUDED
#define MA_CONF_APPLICATION_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"

#define MA_MAX_APPLICATIONS_INT		10

MA_CPP(extern "C" {)

typedef struct ma_conf_application_list_s ma_conf_application_list_t, *ma_conf_application_list_h;

MA_UTILS_API ma_error_t ma_conf_applications_create(ma_conf_application_list_t **list);

MA_UTILS_API ma_error_t ma_conf_applications_scan(ma_conf_application_list_t *list);

MA_UTILS_API ma_error_t ma_conf_applications_get_size(ma_conf_application_list_t *list, size_t *list_size);

MA_UTILS_API ma_error_t ma_conf_applications_release(ma_conf_application_list_t *list);

MA_UTILS_API ma_error_t ma_conf_applications_get_product_id(ma_conf_application_list_t *list, size_t index, const char **product_id);

MA_UTILS_API ma_error_t	ma_conf_application_get_property_str(const char *product_id, const char *key, ma_temp_buffer_t *value);

MA_UTILS_API ma_error_t	ma_conf_application_get_property_int(const char *product_id, const char *key, unsigned int *value);

MA_CPP(})

#endif /* MA_CONF_APPLICATION_H_INCLUDED */
