#ifndef MA_INVENTORY_H_INCLUDED
#define MA_INVENTORY_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/inventory/ma_inventory_info.h"
#include "ma/ma_datastore.h"
#include "ma/datastore/ma_ds_database.h"
#include "ma/ma_log.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_variant.h"


MA_CPP(extern "C" {)

typedef struct ma_inventory_s ma_inventory_t, *ma_inventory_h;


MA_INVENTORY_API ma_error_t ma_inventory_create(ma_msgbus_t *msgbus, ma_ds_t *datastore, const char *base_path, ma_inventory_t **inventory);

MA_INVENTORY_API ma_error_t ma_inventory_start(ma_inventory_t *inventory);

MA_INVENTORY_API ma_error_t ma_inventory_stop(ma_inventory_t *inventory);

MA_INVENTORY_API ma_error_t ma_inventory_add(ma_inventory_t *inventory, ma_inventory_info_t *info);

MA_INVENTORY_API ma_error_t ma_inventory_add_variant(ma_inventory_t *inventory, const char *inventory_id, ma_variant_t *var) ;

MA_INVENTORY_API ma_error_t ma_inventory_enumerate_city_pincode(ma_inventory_t *inventory, const char *pincode, const char *city, ma_variant_t **var);

MA_INVENTORY_API ma_error_t ma_inventory_delete_crn(ma_inventory_t *inventory, const char *inventory_id, const char *crn);

MA_INVENTORY_API ma_error_t ma_inventory_delete_all(ma_inventory_t *inventory);

MA_INVENTORY_API ma_error_t ma_inventory_delete(ma_inventory_t *inventory, const char *inventory_id);

MA_INVENTORY_API ma_error_t ma_inventory_get(ma_inventory_t *inventory, const char *inventory_id, const char *crn, ma_inventory_info_t **info);

MA_INVENTORY_API ma_error_t ma_inventory_get_all(ma_inventory_t *inventory, const char *inventory_id, ma_variant_t **var);

MA_INVENTORY_API ma_error_t ma_inventory_enumerate(ma_inventory_t *inventory, ma_variant_t **var);

MA_INVENTORY_API ma_error_t ma_inventory_set_logger(ma_logger_t *logger);

MA_INVENTORY_API ma_error_t ma_inventory_get_msgbus(ma_inventory_t *inventory, ma_msgbus_t **msgbus);

MA_INVENTORY_API ma_error_t ma_inventory_release(ma_inventory_t *inventory);


MA_CPP(})

#endif /* MA_INVENTORY_H_INCLUDED */

