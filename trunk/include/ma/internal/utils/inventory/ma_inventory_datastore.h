#ifndef MA_INVENTORY_DATASTORE_H_INCLUDED
#define MA_INVENTORY_DATASTORE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/inventory/ma_inventory.h"
#include "ma/internal/utils/inventory/ma_inventory_info.h"
#include "ma/ma_datastore.h"
#include "ma/datastore/ma_ds_database.h"
#include "ma/ma_variant.h"

MA_CPP(extern "C" {)
MA_INVENTORY_API ma_error_t ma_inventory_datastore_read(ma_inventory_t *inventory, ma_ds_t *datastore, const char *ds_path);

MA_INVENTORY_API ma_error_t ma_inventory_datastore_get(ma_inventory_t *inventory, ma_ds_t *datastore, const char *ds_path, const char *inventory_id, const char *crn, ma_inventory_info_t **info);

MA_INVENTORY_API ma_error_t ma_inventory_datastore_get_all(ma_inventory_t *inventory, ma_ds_t *datastore, const char *ds_path, const char *inventory_id, ma_variant_t **var);

MA_INVENTORY_API ma_error_t ma_inventory_datastore_write(ma_inventory_t *inventory, ma_inventory_info_t *info, ma_ds_t *datastore, const char *ds_path);

MA_INVENTORY_API ma_error_t ma_inventory_datastore_remove_inventory_crn(ma_inventory_t *inventory, const char *inventory_id, const char *crn, ma_ds_t *datastore, const char *ds_path);

MA_INVENTORY_API ma_error_t ma_inventory_datastore_enumerate(ma_inventory_t *inventory, ma_ds_t *datastore, const char *ds_path, ma_variant_t **var);

MA_INVENTORY_API ma_error_t ma_inventory_datastore_enumerate_city_pincode(ma_inventory_t *inventory, ma_ds_t *datastore, const char *ds_path, const char *city, const char *pincode, ma_variant_t **ret_var);

MA_INVENTORY_API ma_error_t ma_inventory_datastore_write_variant(ma_inventory_t *inventory, const char *inventory_id, ma_ds_t *datastore, const char *ds_path, ma_variant_t *var);

MA_INVENTORY_API ma_error_t ma_inventory_datastore_remove_inventory(const char *inventory, ma_ds_t *datastore, const char *ds_path);

MA_INVENTORY_API ma_error_t ma_inventory_datastore_clear(ma_ds_t *datastore, const char *ds_path);

MA_CPP(})


#endif /* MA_INVENTORY_DATASTORE_H_INCLUDED */

