#ifndef MA_INVENTORY_INFO_H_INCLUDED
#define MA_INVENTORY_INFO_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_variant.h"
#include "ma/internal/utils/profiler/ma_profile_info.h"
#include "ma/scheduler/ma_task.h"

#define MA_INVENTORY_INFO_VEHICLE_TYPE_TWO_WHEELER_STR  "TwoWheeler"
#define MA_INVENTORY_INFO_VEHICLE_TYPE_THREE_WHEELER_STR "ThreeWheeler"
#define MA_INVENTORY_INFO_VEHICLE_TYPE_FOUR_WHEELER_STR "FourWheeler"

#define MA_INVENTORY_INFO_WORK_STATUS_PRESENT_STR "Present"
#define MA_INVENTORY_INFO_WORK_STATUS_ABSENT_STR "Absent"

MA_CPP(extern "C" {)

typedef struct ma_inventory_info_s ma_inventory_info_t, *ma_inventory_info_h;


#undef MA_INVENTORY_INFO_VEHICLE_EXPANDER
#define MA_INVENTORY_INFO_VEHICLE_EXPANDER\
    MA_INVENTORY_INFO_VEHICLE_EXPAND(MA_INVENTORY_INFO_VEHICLE_TYPE_TWO_WHEELER, 1, MA_INVENTORY_INFO_VEHICLE_TYPE_TWO_WHEELER_STR)\
    MA_INVENTORY_INFO_VEHICLE_EXPAND(MA_INVENTORY_INFO_VEHICLE_TYPE_THREE_WHEELER, 2, MA_INVENTORY_INFO_VEHICLE_TYPE_THREE_WHEELER_STR ) \
    MA_INVENTORY_INFO_VEHICLE_EXPAND(MA_INVENTORY_INFO_VEHICLE_TYPE_FOUR_WHEELER, 3, MA_INVENTORY_INFO_VEHICLE_TYPE_FOUR_WHEELER_STR)

typedef enum ma_inventory_info_vehicle_type_e {
#define MA_INVENTORY_INFO_VEHICLE_EXPAND(status, index, status_str) status = index,
    MA_INVENTORY_INFO_VEHICLE_EXPANDER
#undef MA_INVENTORY_INFO_VEHICLE_EXPAND
}ma_inventory_info_vehicle_type_t;

#undef MA_INVENTORY_INFO_WORK_STATUS_EXPANDER
#define MA_INVENTORY_INFO_WORK_STATUS_EXPANDER\
    MA_INVENTORY_INFO_WORK_STATUS_EXPAND(MA_INVENTORY_INFO_WORK_STATUS_PRESENT, 1, MA_INVENTORY_INFO_WORK_STATUS_PRESENT_STR)\
    MA_INVENTORY_INFO_WORK_STATUS_EXPAND(MA_INVENTORY_INFO_WORK_STATUS_ABSENT, 2, MA_INVENTORY_INFO_WORK_STATUS_ABSENT_STR)


typedef enum ma_inventory_info_work_status_e {
#define MA_INVENTORY_INFO_WORK_STATUS_EXPAND(status, index, status_str) status = index,
    MA_INVENTORY_INFO_WORK_STATUS_EXPANDER
#undef MA_INVENTORY_INFO_WORK_STATUS_EXPAND
}ma_inventory_info_work_status_t;

/* ctor */
MA_INVENTORY_API ma_error_t ma_inventory_info_create(ma_inventory_info_t **info);

/* dtor */
MA_INVENTORY_API ma_error_t ma_inventory_info_release(ma_inventory_info_t *info);

/* setter */
MA_INVENTORY_API ma_error_t ma_inventory_info_set_vehicle_type(ma_inventory_info_t *info, ma_inventory_info_vehicle_type_t type);
MA_INVENTORY_API ma_error_t ma_inventory_info_set_vehicle_type_from_str(ma_inventory_info_t *info, const char *str);
MA_INVENTORY_API ma_error_t ma_inventory_info_set_work_status_from_str(ma_inventory_info_t *info, const char *str);
MA_INVENTORY_API ma_error_t ma_inventory_info_set_email_id(ma_inventory_info_t *info, const char *id);
MA_INVENTORY_API ma_error_t ma_inventory_info_set_date(ma_inventory_info_t *info, ma_task_time_t date);
MA_INVENTORY_API ma_error_t ma_inventory_info_set_pincode(ma_inventory_info_t *info, const char *pin);
MA_INVENTORY_API ma_error_t ma_inventory_info_set_order_completed_count(ma_inventory_info_t *info, ma_uint32_t count);
MA_INVENTORY_API ma_error_t ma_inventory_info_set_order_pending_count(ma_inventory_info_t *info, ma_uint32_t count);
MA_INVENTORY_API ma_error_t ma_inventory_info_set_order_cancelled_count(ma_inventory_info_t *info, ma_uint32_t count);
MA_INVENTORY_API ma_error_t ma_inventory_info_set_order_not_delivered_count(ma_inventory_info_t *info, ma_uint32_t count);
MA_INVENTORY_API ma_error_t ma_inventory_info_set_start_kilometers(ma_inventory_info_t *info, ma_uint32_t km);
MA_INVENTORY_API ma_error_t ma_inventory_info_set_end_kilometers(ma_inventory_info_t *info, ma_uint32_t km);
MA_INVENTORY_API ma_error_t ma_inventory_info_set_start_time(ma_inventory_info_t *info, ma_task_time_t start);
MA_INVENTORY_API ma_error_t ma_inventory_info_set_end_time(ma_inventory_info_t *info, ma_task_time_t end);
MA_INVENTORY_API ma_error_t ma_inventory_info_set_work_status(ma_inventory_info_t *info, ma_inventory_info_work_status_t status);
MA_INVENTORY_API ma_error_t ma_inventory_info_set_contact(ma_inventory_info_t *info, const char *contact);

/* getters */
MA_INVENTORY_API ma_error_t ma_inventory_info_get_order_not_delivered_count(ma_inventory_info_t *info, ma_uint32_t *count);
MA_INVENTORY_API ma_error_t ma_inventory_info_get_vehicle_type_str(ma_inventory_info_t *info, char *str);
MA_INVENTORY_API ma_error_t ma_inventory_info_get_work_status_str(ma_inventory_info_t *info, char *str);
MA_INVENTORY_API ma_error_t ma_inventory_info_get_vehicle_type(ma_inventory_info_t *info, ma_inventory_info_vehicle_type_t *type);
MA_INVENTORY_API ma_error_t ma_inventory_info_get_email_id(ma_inventory_info_t *info, char *id);
MA_INVENTORY_API ma_error_t ma_inventory_info_get_contact(ma_inventory_info_t *info, char *id);
MA_INVENTORY_API ma_error_t ma_inventory_info_get_date(ma_inventory_info_t *info, ma_task_time_t *date);
MA_INVENTORY_API ma_error_t ma_inventory_info_get_pincode(ma_inventory_info_t *info, char *pin);
MA_INVENTORY_API ma_error_t ma_inventory_info_get_order_completed_count(ma_inventory_info_t *info, ma_uint32_t *count);
MA_INVENTORY_API ma_error_t ma_inventory_info_get_order_pending_count(ma_inventory_info_t *info, ma_uint32_t *count);
MA_INVENTORY_API ma_error_t ma_inventory_info_get_order_cancelled_count(ma_inventory_info_t *info, ma_uint32_t *count);
MA_INVENTORY_API ma_error_t ma_inventory_info_get_start_kilometers(ma_inventory_info_t *info, ma_uint32_t *km);
MA_INVENTORY_API ma_error_t ma_inventory_info_get_end_kilometers(ma_inventory_info_t *info, ma_uint32_t *km);
MA_INVENTORY_API ma_error_t ma_inventory_info_get_start_time(ma_inventory_info_t *info, ma_task_time_t *start);
MA_INVENTORY_API ma_error_t ma_inventory_info_get_end_time(ma_inventory_info_t *info, ma_task_time_t *end);
MA_INVENTORY_API ma_error_t ma_inventory_info_get_work_status(ma_inventory_info_t *info, ma_inventory_info_work_status_t *status);



MA_INVENTORY_API ma_error_t ma_inventory_info_order_add(ma_inventory_info_t *info, const char *order);
MA_INVENTORY_API ma_error_t ma_inventory_info_order_remove(ma_inventory_info_t *info, const char *order);

/* convert inventory info to variant  and vice versa */
MA_INVENTORY_API ma_error_t ma_inventory_info_convert_to_variant(ma_inventory_info_t *info, ma_variant_t **variant);
MA_INVENTORY_API ma_error_t ma_inventory_info_convert_from_variant(ma_variant_t *variant, ma_inventory_info_t **info);


MA_CPP(})

#endif /* MA_INVENTORY_INFO_H_INCLUDED */

