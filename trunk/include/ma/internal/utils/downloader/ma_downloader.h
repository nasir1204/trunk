#ifndef MA_DOWNLOADER_H_INCLUDED
#define MA_DOWNLOADER_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/threading/ma_atomic.h"
#include "ma/internal/utils/datastructures/ma_stream.h"


MA_CPP(extern "C" {)

typedef enum ma_downloader_progress_state_e {
    MA_DOWNLOADER_DOWNLOAD_IN_PROGRESS,
    MA_DOWNLOADER_DOWNLOAD_CANCLED,
    MA_DOWNLOADER_DOWNLOAD_DONE,
    MA_DOWNLOADER_CONNECT_FAILED,
    MA_DOWNLOADER_DOWNLOAD_FAILED,
} ma_download_progress_state_t;

typedef struct ma_downloader_s ma_downloader_t, *ma_downloader_h;

typedef struct ma_downloader_methods_s ma_downloader_methods_t, *ma_downloader_methods_h;

typedef ma_error_t (*ma_on_download_progress_cb)(ma_downloader_t *self, ma_download_progress_state_t state, ma_stream_t *stream, size_t download_total, void *cb_data);

struct ma_downloader_methods_s {
    ma_error_t (*sync_start)(ma_downloader_t *self, const char *url, ma_stream_t *stream); /* Synchronous download */
    ma_error_t (*start)(ma_downloader_t *self, const char *url, ma_stream_t *stream, ma_on_download_progress_cb cb, void *cb_data); /* Asynchronous download */
    ma_error_t (*stop)(ma_downloader_t *self);  /* For Asynchronous download */
    ma_error_t (*destroy)(ma_downloader_t *self);
} ;


struct ma_downloader_s {
    ma_downloader_methods_t   const    *methods;
    ma_atomic_counter_t                ref_count;
    void                               *data;
};


MA_STATIC_INLINE void ma_downloader_init(ma_downloader_t *downloader, ma_downloader_methods_t  const *methods, void *data) {
    if(downloader) {
        downloader->methods = methods;
        downloader->ref_count = 1;
        downloader->data = data;
    }
}

MA_STATIC_INLINE ma_error_t ma_downloader_add_ref(ma_downloader_t *downloader) {
    if (downloader && (0 <= downloader->ref_count)) {
        MA_ATOMIC_INCREMENT(downloader->ref_count);
    }
    return MA_OK;
}

MA_STATIC_INLINE ma_error_t ma_downloader_sync_start(ma_downloader_t *downloader, const char *url, ma_stream_t *stream) {
    return (downloader && downloader->methods)
            ? downloader->methods->sync_start(downloader, url, stream) : MA_ERROR_INVALIDARG;
}

MA_STATIC_INLINE ma_error_t ma_downloader_start(ma_downloader_t *downloader, const char *url, ma_stream_t *stream, ma_on_download_progress_cb cb, void *cb_data) {
    return (downloader && downloader->methods)
            ? downloader->methods->start(downloader, url, stream, cb, cb_data) : MA_ERROR_INVALIDARG;
}

MA_STATIC_INLINE ma_error_t ma_downloader_stop(ma_downloader_t *downloader) {
    return (downloader && downloader->methods)
            ? downloader->methods->stop(downloader) : MA_ERROR_INVALIDARG;
}

MA_STATIC_INLINE ma_error_t ma_downloader_release(ma_downloader_t *downloader) {
    if (downloader && (0 <= downloader->ref_count)) {
        if (0 == MA_ATOMIC_DECREMENT(downloader->ref_count)) {
            downloader->methods->destroy(downloader);
        }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

MA_CPP(})

#endif /* MA_DOWNLOADER_H_INCLUDED */

