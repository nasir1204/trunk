#ifndef MA_REPOSITORY_DOWNLOADER_H_INCLUDED
#define MA_REPOSITORY_DOWNLOADER_H_INCLUDED

#include "ma/internal/utils/downloader/ma_downloader.h"
#include "ma/repository/ma_repository.h"
#include "ma/internal/utils/network/ma_net_client_service.h"
#include "ma/proxy/ma_proxy_config.h"
#include "ma/repository/ma_repository_list.h"
#include "ma/datastore/ma_ds.h"
#include "ma/internal/utils/context/ma_context.h"

MA_CPP(extern "C" {)

typedef struct ma_repository_downloader_s ma_repository_downloader_t, *ma_repository_downloader_h;

ma_error_t ma_repository_downloader_create(ma_downloader_t **downloader);

ma_error_t ma_repository_downloader_add_header(ma_downloader_t *downloader, const char *key, const char *value);

ma_error_t ma_repository_downloader_set_net_client(ma_downloader_t *downloader, ma_net_client_service_t *net_client);

ma_error_t ma_repository_downloader_set_repository(ma_downloader_t *downloader, ma_repository_t *repository);

ma_error_t ma_repository_downloader_set_proxy_config(ma_downloader_t *downloader, ma_proxy_config_t *proxy_config);

ma_error_t ma_repository_downloader_set_logger(ma_downloader_t *downloader, ma_logger_t *logger);

ma_error_t ma_repository_downloader_set_ds(ma_downloader_t *downloader, ma_ds_t *ds);

ma_error_t ma_repository_downloader_use_relay(ma_downloader_t *downloader, ma_bool_t use_relay);

ma_error_t ma_repository_downloader_set_context(ma_downloader_t *downloader, ma_context_t *context);

MA_CPP(})

#endif /* MA_REPOSITORY_DOWNLOADER_H_INCLUDED */
