#ifndef MA_PATH_UTILS_H_INCLUDED
#define MA_PATH_UTILS_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include <sys/stat.h>

MA_CPP(extern "C" {)

/* pid 2 path*/
MA_UTILS_API ma_error_t ma_pid_to_path(long pid, ma_temp_buffer_t *buffer);

/* pid 2 stat buffer*/
MA_UTILS_API ma_error_t ma_pid_to_stat(long pid, struct stat *stat_buf);

MA_CPP(})

#endif /* MA_PATH_UTILS_H_INCLUDED*/
