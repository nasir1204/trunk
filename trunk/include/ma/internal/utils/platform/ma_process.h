#ifndef MA_PROCESS_H_INCLUDED
#define MA_PROCESS_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"

MA_CPP(extern "C" {)

MA_UTILS_API ma_int32_t ma_process_start();

MA_UTILS_API ma_int32_t ma_process_stop();

MA_UTILS_API ma_int32_t ma_process_status();

typedef void (*ma_process_stop_handler_cb_t)(void *data);

MA_UTILS_API ma_int32_t ma_process_register_stop_handler(ma_event_loop_t *event_loop, ma_process_stop_handler_cb_t cb, void *cb_data);

MA_CPP(})

#endif /* MA_PROCESS_H_INCLUDED */

