#ifndef MA_NET_INTERFACE_LIST_INTERNAL_H_
#define MA_NET_INTERFACE_LIST_INTERNAL_H_

#include "ma/ma_common.h"
#include "ma/internal/utils/network/ma_net_interface.h"

MA_NETWORK_API ma_error_t ma_net_interface_list_create(ma_net_interface_list_t ** list);
MA_NETWORK_API ma_error_t ma_net_interface_list_release(ma_net_interface_list_t *list);

ma_error_t ma_net_interface_list_add_interface(ma_net_interface_list_t *list, ma_net_interface_t **inf);
ma_error_t ma_net_interface_list_set_family(ma_net_interface_list_t *list, ma_ip_family_t family);

MA_NETWORK_API ma_error_t ma_net_interface_list_scan	(ma_net_interface_list_t	*list, ma_bool_t loopback);
MA_NETWORK_API ma_error_t ma_net_interface_list_reset	(ma_net_interface_list_t	*list);
MA_NETWORK_API ma_error_t ma_net_interface_list_rescan	(ma_net_interface_list_t	*list, ma_bool_t loopback);

ma_error_t ma_get_system_network_family(ma_net_interface_list_t *list, ma_ip_family_t *family);


ma_error_t ma_net_interface_create(ma_net_interface_t **itf);
ma_error_t ma_net_interface_release(ma_net_interface_t *itf);
ma_error_t ma_net_interface_set_ipfamily(ma_net_interface_t *itf, ma_ip_family_t family);
ma_error_t ma_net_interface_add_address(ma_net_interface_t *itf, ma_net_address_info_t **address);


ma_error_t ma_address_info_create(ma_net_address_info_t **address);
ma_error_t ma_address_info_release(ma_net_address_info_t *address);



MA_NETWORK_API ma_error_t ma_get_mac_from_ip(unsigned char *mac, ma_uint32_t *msize,const unsigned char *ip,ma_uint32_t isize, ma_net_interface_list_t *list);

MA_NETWORK_API ma_error_t ma_get_net_interface_from_ip(ma_net_interface_t **interfaces, const unsigned char *ip, ma_uint32_t size, ma_net_interface_list_t *list);

MA_NETWORK_API ma_error_t convert_v6_address_to_literal(unsigned char *retAddr, ma_uint32_t *rsize, const unsigned char *ip, ma_uint32_t isize);

MA_NETWORK_API ma_error_t ma_net_interface_list_get_ipv6_scope_ids(ma_net_interface_list_t *list, ma_uint32_t **scope_ids, size_t *size);

#endif 

