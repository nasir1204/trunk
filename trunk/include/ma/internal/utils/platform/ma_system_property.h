/** @file ma_system_property_provider.h
 *  @brief System Property collection interfaces for products 
 *  
 */

#ifndef MA_SYSTEM_PROPRTY_H_INCLUDED
#define MA_SYSTEM_PROPRTY_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/ma_macros.h"
#include "ma/ma_log.h"
#include "ma/internal/utils/network/ma_net_interface.h"

MA_CPP(extern "C" {)

#define MA_MAX_USER_LIST_LEN 128

typedef struct ma_system_property_s ma_system_property_t ;
typedef struct ma_cluster_system_property_s	{
	char		state[4] ;  /* 0(ClusterStateNotInstalled)/ (1)ClusterStateRunning/ (2)ClusterStateNotRunning/ (3)ClusterStateNotConfigured/ (4)StateUnknown */
	/* Following field/memory will not allocated in all state except "ClusterStateRunning" */
	/* It make sense to populate following fields in state "ClusterStateNotRunning", but ePOo is not interested. So we don't populate it */
	/* Unlike other data structure following fields allocated on heap, because as per our observation most computer run on non-clustered. So most of time this will be NULL */
	char		*name ;
	char		*ipaddr;
	char		*host;
	char		*quorum_resource_path;
	char		*nodes;
	ma_bool_t	is_available;
}ma_cluster_system_property_t;

typedef struct ma_cpu_system_property_s	{
	char		type[MA_MAX_PATH_LEN] ;
	char		speed[16] ;
	char		numbers[8];
	char		serialnumber[32];
	ma_bool_t	is_available;
}ma_cpu_system_property_t;

typedef struct ma_memory_system_property_s{
	char		total_memory[32];
	char		free_memory[32];
	ma_bool_t	is_available;
}ma_memory_system_property_t;

typedef struct ma_os_system_property_s{
	char		type[64];			/*version like "Windows 7" */
	char		platform[32];		/* Workstation, Server etc */
	char		version[32];	
	char		servicepack[MA_MAX_LEN];
	char		buildnumber[64];
	char		oemid[64]; 
	char		platformid[32];		/*combination of minor major pack ... e.g. "Windows 7:6:1:1 " */
	char		is64bit[4];
	char		smbios_uuid[64];
	ma_bool_t	is_available;
}ma_os_system_property_t;

typedef struct ma_misc_system_property_s{
	char		username[MA_MAX_USER_LIST_LEN];
	char		timezone[128];
	char		timezonebios[128];
	char		language[32];
    char		language_code[32];
	char		computername[MA_MAX_PATH_LEN];
	char		computerdesc[MA_MAX_PATH_LEN];
	char		isportable[4];
	char		systemtime[64];
	char		domainname[MA_MAX_PATH_LEN];
	char		ipx_addr[4];    /* This always set to N/A */
	char		hostname[MA_MAX_PATH_LEN];
	ma_bool_t	is_available;
}ma_misc_system_property_t;

typedef struct ma_file_system_property_s	{
	char		drive_name[64];
	char		drive_totalspace[32];
	char		drive_freespace[32];
	ma_bool_t	is_available;
}ma_file_system_property_t;

typedef struct ma_disk_system_property_s{
	char		total_diskspace[32];
	char		free_diskspace[32];
	char		total_filesystems[4];
	ma_file_system_property_t *filesysteminfo;
	ma_bool_t	is_available;
}ma_disk_system_property_t;

typedef struct ma_network_system_property_s{	
	char		ip_address[MA_IPV6_ADDR_LEN]; /* One of multiple (if any) local ip */
	char		ip_address_formated[MA_IPV6_ADDR_LEN]; /*above ip, ipv6 addr need to be formated as per epo requirement*/
	char		spipe_src_ip_address_formatted[MA_IPV6_ADDR_LEN]; /*Agent ip address which communicating to ePO */
	char		mac_address[MA_MAC_ADDRESS_SIZE];
	char		mac_address_formated[MA_FORMATTED_MAC_ADDRESS_SIZE];
	char		subnet_address[MA_IPV6_ADDR_LEN];
	char		subnet_mask[MA_IPV6_ADDR_LEN]; 
	char		fqdn[MA_MAX_PATH_LEN];
	ma_bool_t	is_available;
}ma_network_system_property_t;


MA_CONFIGURATOR_API ma_error_t ma_system_property_create(ma_system_property_t **self, ma_logger_t *logger);
MA_CONFIGURATOR_API ma_error_t ma_system_property_scan(ma_system_property_t *self);
MA_CONFIGURATOR_API void ma_system_property_release(ma_system_property_t *self);

/*
forced_scan , if MA_TRUE: It forcefully scans non transient system properties e.g. memory/disk/time etc properties.
			,if MA_FALSE: It returns all availabe properties without re-calculation. 
			 In case of any system property unavailable, then it recalculate (irrespective of any option)
*/
MA_CONFIGURATOR_API const ma_cpu_system_property_t *ma_system_property_cpu_get(ma_system_property_t *self, ma_bool_t forced_scan);
MA_CONFIGURATOR_API const ma_memory_system_property_t *ma_system_property_memory_get(ma_system_property_t *self, ma_bool_t forced_scan);
MA_CONFIGURATOR_API const ma_os_system_property_t *ma_system_property_os_get(ma_system_property_t *self, ma_bool_t forced_scan);
MA_CONFIGURATOR_API const ma_misc_system_property_t *ma_system_property_misc_get(ma_system_property_t *self, ma_bool_t forced_scan);
MA_CONFIGURATOR_API const ma_disk_system_property_t *ma_system_property_disk_get(ma_system_property_t *self, ma_bool_t forced_scan);
MA_CONFIGURATOR_API const ma_network_system_property_t *ma_system_property_network_get(ma_system_property_t *self, ma_bool_t forced_scan);
MA_CONFIGURATOR_API const ma_cluster_system_property_t *ma_system_property_cluster_get(ma_system_property_t *self, ma_bool_t forced_scan);


MA_CPP(})
#endif // MA_SYSTEM_PROPRTY_H_INCLUDED
