#ifndef MA_SYS_APIS_H_INCLUDED
#define MA_SYS_APIS_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"

MA_CPP(extern "C" {)

/* env value should be const string in the form of "<name>=<value>" */
MA_UTILS_API ma_error_t ma_sys_putenv(const char *env);

MA_UTILS_API size_t ma_strnlen(const char *s, size_t maxlen);

MA_UTILS_API wchar_t *ma_wcsdup(const wchar_t* string);

MA_UTILS_API ma_uint32_t ma_sys_rand();

MA_UTILS_API ma_int64_t ma_atoint64(const char *string);
/* Time APIs */
MA_UTILS_API ma_error_t ma_sys_gettimeofday(struct timeval *time_val);

MA_UTILS_API ma_error_t ma_sys_usleep(long usec);

/* random number gneration */
MA_UTILS_API ma_error_t ma_prng_gen_random_number(void *data_ptr, size_t data_len);

MA_UTILS_API ma_bool_t ma_get_computer_name(char computer_name[], size_t length);

MA_UTILS_API uint64_t ma_get_disk_free_space(const char *dir_name);

MA_UTILS_API unsigned long ma_get_tick_count();

/*
 * Creates a filesystem iterator to iterate the folders, filenames won't be given
 * Maybe if needed can be extended.
 */
typedef enum ma_filesystem_iterator_mode_e {
	MA_FILESYSTEM_ITERATE_DIRECTORY = 0,
	MA_FILESYSTEM_ITERATE_FILES,
	MA_FILESYSTEM_ITERATE_ALL
} ma_filesystem_iterator_mode_t;

typedef struct ma_filesystem_iterator_s ma_filesystem_iterator_t, *ma_filesystem_iterator_h;

MA_UTILS_API ma_error_t ma_filesystem_iterator_create(const char *absolute_path, ma_filesystem_iterator_t **iterator);

MA_UTILS_API ma_error_t ma_filesystem_iterator_release(ma_filesystem_iterator_t *iterator);

MA_UTILS_API ma_error_t ma_filesystem_iterator_get_next(ma_filesystem_iterator_t *iterator, ma_temp_buffer_t *buffer);

MA_UTILS_API ma_error_t ma_filesystem_iterator_set_mode(ma_filesystem_iterator_t *iterator, ma_filesystem_iterator_mode_t mode);

MA_UTILS_API ma_error_t ma_filesystem_iterator_get_mode(ma_filesystem_iterator_t *iterator, ma_filesystem_iterator_mode_t *mode);

MA_UTILS_API ma_error_t ma_filesystem_iterator_count(const char *path, ma_filesystem_iterator_mode_t mode, size_t *size);

/* Process status */
/* Returns -1 on Failure, 0 on Process not running, 1 on process running */
MA_UTILS_API ma_int32_t ma_sys_get_process_status(const char *process_name);

/* Change the process ownership*/
/* 0 on success else failure*/
MA_UTILS_API ma_int32_t ma_sys_chprocessown(const char *username);

/* Change the file ownership*/
/* 0 on success else failure*/
MA_UTILS_API ma_int32_t ma_sys_chfileown(const char *file, const char *username);

MA_CPP(})

#endif /* MA_SYS_APIS_H_INCLUDED*/
