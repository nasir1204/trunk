#ifndef MA_DL_H_INCLUDED
#define MA_DL_H_INCLUDED
#include "ma/ma_common.h"

MA_CPP(extern "C" {)

typedef struct ma_lib_s {
  void  *handle;  
} ma_lib_t;

MA_UTILS_API ma_error_t ma_dl_open(const char *library_name, unsigned long flags , ma_lib_t *lib);

MA_UTILS_API void ma_dl_close(ma_lib_t *lib);

MA_UTILS_API void *ma_dl_sym(ma_lib_t *lib , const char *symbol_name);

MA_CPP(})

#endif /* MA_DL_H_INCLUDED*/
