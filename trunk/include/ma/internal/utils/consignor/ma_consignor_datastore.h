#ifndef MA_CONSIGNER_DATASTORE_H_INCLUDED
#define MA_CONSIGNER_DATASTORE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/consignor/ma_consignor.h"
#include "ma/internal/utils/consignor/ma_consign_info.h"
#include "ma/ma_datastore.h"
#include "ma/datastore/ma_ds_database.h"

MA_CPP(extern "C" {)
MA_CONSIGN_API ma_error_t ma_consignor_datastore_read(ma_consignor_t *consignor, ma_ds_t *datastore, const char *ds_path);

MA_CONSIGN_API ma_error_t ma_consignor_datastore_get(ma_consignor_t *consignor, ma_ds_t *datastore, const char *ds_path, const char *id, ma_consign_info_t **info);

MA_CONSIGN_API ma_error_t ma_consignor_datastore_write(ma_consignor_t *consignor, ma_consign_info_t *info, ma_ds_t *datastore, const char *ds_path);

MA_CONSIGN_API ma_error_t ma_consignor_datastore_write_variant(ma_consignor_t *consignor, const char *id, ma_ds_t *datastore, const char *ds_path, ma_variant_t *var);

MA_CONSIGN_API ma_error_t ma_consignor_datastore_remove_profile(const char *profile, ma_ds_t *datastore, const char *ds_path);

MA_CONSIGN_API ma_error_t ma_consignor_datastore_clear(ma_ds_t *datastore, const char *ds_path);

MA_CPP(})


#endif /* MA_CONSIGNER_DATASTORE_H_INCLUDED */

