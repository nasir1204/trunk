#ifndef MA_CONSIGN_INFO_H_INCLUDED
#define MA_CONSIGN_INFO_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_variant.h"
#include "ma/scheduler/ma_triggers.h"
#include "ma/internal/utils/json/ma_json_utils.h"

MA_CPP(extern "C" {)

typedef struct ma_consign_info_s ma_consign_info_t, *ma_consign_info_h;

/* profile info ctor */
MA_CONSIGN_API ma_error_t ma_consign_info_create(ma_consign_info_t **info);

/* profile info dtor */
MA_CONSIGN_API ma_error_t ma_consign_info_release(ma_consign_info_t *info);


/* setter */
MA_CONSIGN_API ma_error_t ma_consign_info_set_contact(ma_consign_info_t *info, const char *contact); 
MA_CONSIGN_API ma_error_t ma_consign_info_set_date(ma_consign_info_t *info, const char *date);
MA_CONSIGN_API ma_error_t ma_consign_info_set_pending_orders_map(ma_consign_info_t *info, ma_table_t *table);
MA_CONSIGN_API ma_error_t ma_consign_info_set_completed_orders_map(ma_consign_info_t *info, ma_table_t *table);

MA_CONSIGN_API ma_error_t ma_consign_info_add_pending_order(ma_consign_info_t *info, const char *contact, const char *order);
MA_CONSIGN_API ma_error_t ma_consign_info_remove_pending_order(ma_consign_info_t *info, const char *contact, const char *order);
MA_CONSIGN_API ma_error_t ma_consign_info_pending_order_list_size(ma_consign_info_t *info, const char *contact, size_t *size);

MA_CONSIGN_API ma_error_t ma_consign_info_add_completed_order(ma_consign_info_t *info, const char *contact, const char *order);
MA_CONSIGN_API ma_error_t ma_consign_info_remove_completed_order(ma_consign_info_t *info, const char *contact, const char *order);
MA_CONSIGN_API ma_error_t ma_consign_info_completed_order_list_size(ma_consign_info_t *info, const char *contact, size_t *size);

/* getter */
MA_CONSIGN_API ma_error_t ma_consign_info_get_contact(ma_consign_info_t *info, const char **contact); 
MA_CONSIGN_API ma_error_t ma_consign_info_get_date(ma_consign_info_t *info, const char **date);
MA_CONSIGN_API ma_error_t ma_consign_info_get_pending_orders_map(ma_consign_info_t *info, ma_table_t **table);
MA_CONSIGN_API ma_error_t ma_consign_info_get_completed_orders_map(ma_consign_info_t *info, ma_table_t **table);

/* convert profile info to variant  and vice versa */
MA_CONSIGN_API ma_error_t ma_consign_info_convert_to_variant(ma_consign_info_t *info, ma_variant_t **variant);
MA_CONSIGN_API ma_error_t ma_consign_info_convert_from_variant(ma_variant_t *variant, ma_consign_info_t **info);
MA_CONSIGN_API ma_error_t ma_consign_info_convert_to_json(ma_consign_info_t *info, ma_json_t **json);


MA_CPP(})

#endif /* MA_CONSIGN_INFO_H_INCLUDED */

