#ifndef MA_CONSIGNER_H_INCLUDED
#define MA_CONSIGNER_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/consignor/ma_consign_info.h"
#include "ma/ma_datastore.h"
#include "ma/datastore/ma_ds_database.h"
#include "ma/ma_log.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_variant.h"


MA_CPP(extern "C" {)

typedef struct ma_consignor_s ma_consignor_t, *ma_consignor_h;


MA_CONSIGN_API ma_error_t ma_consignor_create(ma_msgbus_t *msgbus, ma_ds_t *datastore, const char *base_path, ma_consignor_t **consignor);

MA_CONSIGN_API ma_error_t ma_consignor_start(ma_consignor_t *consignor);

MA_CONSIGN_API ma_error_t ma_consignor_stop(ma_consignor_t *consignor);

MA_CONSIGN_API ma_error_t ma_consignor_add(ma_consignor_t *consignor, ma_consign_info_t *info);

MA_CONSIGN_API ma_error_t ma_consignor_add_variant(ma_consignor_t *consignor, const char *id, ma_variant_t *var) ;

MA_CONSIGN_API ma_error_t ma_consignor_delete(ma_consignor_t *consignor, const char *profile);

MA_CONSIGN_API ma_error_t ma_consignor_delete_all(ma_consignor_t *consignor);

MA_CONSIGN_API ma_error_t ma_consignor_get(ma_consignor_t *consignor, const char *profile, ma_consign_info_t **info);

MA_CONSIGN_API ma_error_t ma_consignor_set_logger(ma_logger_t *logger);

MA_CONSIGN_API ma_error_t ma_consignor_get_msgbus(ma_consignor_t *consignor, ma_msgbus_t **msgbus);

MA_CONSIGN_API ma_error_t ma_consignor_release(ma_consignor_t *consignor);


MA_CPP(})

#endif /* MA_CONSIGNER_H_INCLUDED */
