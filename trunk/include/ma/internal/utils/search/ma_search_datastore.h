#ifndef MA_SEARCH_DATASTORE_H_INCLUDED
#define MA_SEARCH_DATASTORE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/search/ma_search.h"
#include "ma/internal/utils/search/ma_search_info.h"
#include "ma/ma_datastore.h"
#include "ma/datastore/ma_ds_database.h"

MA_CPP(extern "C" {)
MA_SEARCH_API ma_error_t ma_search_datastore_read(ma_search_t *search, ma_ds_t *datastore, const char *ds_path);

MA_SEARCH_API ma_error_t ma_search_datastore_get(ma_search_t *search, ma_ds_t *datastore, const char *ds_path, const char *search_id, ma_search_info_t **info);

MA_SEARCH_API ma_error_t ma_search_datastore_write(ma_search_t *search, ma_search_info_t *info, ma_ds_t *datastore, const char *ds_path);

MA_SEARCH_API ma_error_t ma_search_datastore_write_variant(ma_search_t *search, const char *search_id, ma_ds_t *datastore, const char *ds_path, ma_variant_t *var);

MA_SEARCH_API ma_error_t ma_search_datastore_remove_search(const char *search, ma_ds_t *datastore, const char *ds_path);

MA_SEARCH_API ma_error_t ma_search_datastore_clear(ma_ds_t *datastore, const char *ds_path);

MA_CPP(})


#endif /* MA_SEARCH_DATASTORE_H_INCLUDED */

