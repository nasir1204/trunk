#ifndef MA_SEARCH_INFO_H_INCLUDED
#define MA_SEARCH_INFO_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_variant.h"
#include "ma/scheduler/ma_triggers.h"
#include "ma/internal/utils/board/ma_board_pins.h"

MA_CPP(extern "C" {)

typedef struct ma_search_info_s ma_search_info_t, *ma_search_info_h;

/* search info ctor */
MA_SEARCH_API ma_error_t ma_search_info_create(ma_search_info_t **self);

/* search info dtor */
MA_SEARCH_API ma_error_t ma_search_info_release(ma_search_info_t *self);

/* setter */
MA_SEARCH_API ma_error_t ma_search_info_set_id(ma_search_info_t *self, const char *id);
MA_SEARCH_API ma_error_t ma_search_info_set_pin(ma_search_info_t *self, const char *id, ma_board_pin_t *pin);
MA_SEARCH_API ma_error_t ma_search_info_set_size(ma_search_info_t *self, size_t no_of_pins);
MA_SEARCH_API ma_error_t ma_search_info_copy(ma_search_info_t *dest, const ma_search_info_t *src);

/* getter */
MA_SEARCH_API ma_error_t ma_search_info_get_id(ma_search_info_t *self, const char **id);
MA_SEARCH_API ma_error_t ma_search_info_get_pin(ma_search_info_t *self, const char *id, ma_board_pin_t **pin);
MA_SEARCH_API ma_error_t ma_search_info_get_pins(ma_search_info_t *self, ma_board_pin_t ***pin, int no_of_pins);
MA_SEARCH_API ma_error_t ma_search_info_get_size(ma_search_info_t *self, size_t *no_of_pins);

MA_SEARCH_API ma_error_t ma_search_info_convert_to_variant(ma_search_info_t *self, ma_variant_t **v);
MA_SEARCH_API ma_error_t ma_search_info_convert_from_variant(ma_variant_t *v, ma_search_info_t **self);



MA_CPP(})

#endif /* MA_SEARCH_INFO_H_INCLUDED */

