#ifndef MA_SEARCH_H_INCLUDED
#define MA_SEARCH_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/search/ma_search_info.h"
#include "ma/ma_datastore.h"
#include "ma/datastore/ma_ds_database.h"
#include "ma/ma_log.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_variant.h"


MA_CPP(extern "C" {)

typedef struct ma_search_s ma_search_t, *ma_search_h;


MA_SEARCH_API ma_error_t ma_search_create(ma_msgbus_t *msgbus, ma_ds_t *datastore, const char *base_path, ma_search_t **search);

MA_SEARCH_API ma_error_t ma_search_start(ma_search_t *search);

MA_SEARCH_API ma_error_t ma_search_stop(ma_search_t *search);

MA_SEARCH_API ma_error_t ma_search_add(ma_search_t *search, ma_search_info_t *info);

MA_SEARCH_API ma_error_t ma_search_add_variant(ma_search_t *search, const char *search_id, ma_variant_t *var) ;

MA_SEARCH_API ma_error_t ma_search_delete(ma_search_t *search, const char *search_id);

MA_SEARCH_API ma_error_t ma_search_delete_all(ma_search_t *search);

MA_SEARCH_API ma_error_t ma_search_get(ma_search_t *search, const char *search_id, ma_search_info_t **info);

MA_SEARCH_API ma_error_t ma_search_set_logger(ma_logger_t *logger);

MA_SEARCH_API ma_error_t ma_search_get_msgbus(ma_search_t *search, ma_msgbus_t **msgbus);

MA_SEARCH_API ma_error_t ma_search_release(ma_search_t *search);


MA_CPP(})

#endif /* MA_SEARCH_H_INCLUDED */
