#ifndef MA_WINDOWS_EVENT_H_INCLUDED
#define MA_WINDOWS_EVENT_H_INCLUDED

#include "ma/ma_common.h"

#if defined (MA_WINDOWS)
#include <Windows.h>

MA_CPP(extern "C" {)

HRESULT ma_windows_event_create(const wchar_t *event_name, unsigned short is_manual_reset, unsigned short is_initial_state, ma_bool_t is_restricted_access, HANDLE *event_handle);

HRESULT ma_windows_event_open(const wchar_t *event_name, HANDLE *event_handle) ;

MA_CPP(})
#endif

#endif /* MA_WINDOWS_EVENT_H_INCLUDED */
