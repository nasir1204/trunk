#ifndef MA_SMS_H_INCLUDED
#define MA_SMS_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_log.h"

#define MA_SMS_SEND_PATH "/autmunIS/smsengine/send/"
#define MA_MAX_SMS_MESSAGE_SIZE 10000

typedef enum boolean {
    FALSE = 0, TRUE = 1, }boolean;

MA_CPP(extern "C" {)
/* Default - 10K max memory for our param strings */

/* Send message function, detailed in twilio.c */
int     ma_sms_send_message     (char *account_sid,
                                 char *auth_token,
                                 char *message,
                                 char *from_number,
                                 char *to_number,
                                 char *picture_url,
                                 boolean verbose);
void ma_sms_send(const char *protocol, const char *server, const char *port, const char *path, const char *contact, const char *msg);
void ma_sms_set_logger(ma_logger_t *masvc_logger);
MA_CPP(})
#endif
