#ifndef MA_BOOKER_H_INCLUDED
#define MA_BOOKER_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/booker/ma_booking_info.h"
#include "ma/ma_datastore.h"
#include "ma/datastore/ma_ds_database.h"
#include "ma/ma_log.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_variant.h"


MA_CPP(extern "C" {)

typedef struct ma_booker_s ma_booker_t, *ma_booker_h;


MA_BOOKING_API ma_error_t ma_booker_create(ma_msgbus_t *msgbus, ma_ds_t *datastore, const char *base_path, ma_booker_t **booker);

MA_BOOKING_API ma_error_t ma_booker_start(ma_booker_t *booker);

MA_BOOKING_API ma_error_t ma_booker_stop(ma_booker_t *booker);

MA_BOOKING_API ma_error_t ma_booker_add(ma_booker_t *booker, ma_booking_info_t *info);

MA_BOOKING_API ma_error_t ma_booker_add_variant(ma_booker_t *booker, const char *booking_id, ma_variant_t *var) ;

MA_BOOKING_API ma_error_t ma_booker_delete_all(ma_booker_t *booker);

MA_BOOKING_API ma_error_t ma_booker_delete(ma_booker_t *booker, const char *booking);

MA_BOOKING_API ma_error_t ma_booker_get(ma_booker_t *booker, const char *booking, ma_booking_info_t **info);

MA_BOOKING_API ma_error_t ma_booker_get_all(ma_booker_t *booker, const char *booking, ma_variant_t **var);

MA_BOOKING_API ma_error_t ma_booker_set_logger(ma_logger_t *logger);

MA_BOOKING_API ma_error_t ma_booker_get_msgbus(ma_booker_t *booker, ma_msgbus_t **msgbus);

MA_BOOKING_API ma_error_t ma_booker_release(ma_booker_t *booker);


MA_CPP(})

#endif /* MA_BOOKER_H_INCLUDED */

