#ifndef MA_BOOKING_INFO_H_INCLUDED
#define MA_BOOKING_INFO_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_variant.h"
#include "ma/internal/utils/json/ma_json_utils.h"

#define MA_BOOKING_INFO_STATUS_NOT_ASSIGNED_STR                  "Not Assigned"
#define MA_BOOKING_INFO_STATUS_ASSIGNED_STR                      "Assigned"
#define MA_BOOKING_INFO_STATUS_CANCELLED_STR                     "Cancelled"
#define MA_BOOKING_INFO_STATUS_DELIVERED_STR                     "Delivered"
#define MA_BOOKING_INFO_STATUS_NOT_DELIVERED_STR                 "NotDelivered"
#define MA_BOOKING_INFO_STATUS_PICKED_STR                        "Picked"
#define MA_BOOKING_INFO_STATUS_TRANSIT_STR                       "Transit"

#define MA_BOOKING_INFO_DELIVERY_TYPE_ONE_WAY_STR                "One-Way"
#define MA_BOOKING_INFO_DELIVERY_TYPE_TWO_WAY_STR                "Two-Way"

#define MA_BOOKING_INFO_SERVICE_TYPE_PARCEL_STR                  "Parcel"
#define MA_BOOKING_INFO_SERVICE_TYPE_DOCUMENT_STR                "Document"

#define MA_BOOKING_INFO_PAYMENT_TYPE_CREDIT_CARD_STR             "CreditCard"
#define MA_BOOKING_INFO_PAYMENT_TYPE_DEBIT_CARD_STR              "DebitCard"
#define MA_BOOKING_INFO_PAYMENT_TYPE_ONLINE_STR                  "Online"
#define MA_BOOKING_INFO_PAYMENT_TYPE_CASH_STR                    "Cash"

#define MA_BOOKING_INFO_PAYMENT_STATUS_PAID_STR                  "paid"
#define MA_BOOKING_INFO_PAYMENT_STATUS_UNPAID_STR                "unpaid"

MA_CPP(extern "C" {)

typedef struct ma_booking_info_s ma_booking_info_t, *ma_booking_info_h;

#undef MA_BOOKING_INFO_PAYMENT_EXPANDER
#define MA_BOOKING_INFO_PAYMENT_EXPANDER\
    MA_BOOKING_INFO_PAYMENT_EXPAND(MA_BOOKING_INFO_PAYMENT_TYPE_CREDIT_CARD, 1, MA_BOOKING_INFO_PAYMENT_TYPE_CREDIT_CARD_STR)\
    MA_BOOKING_INFO_PAYMENT_EXPAND(MA_BOOKING_INFO_PAYMENT_TYPE_DEBIT_CARD, 2, MA_BOOKING_INFO_PAYMENT_TYPE_DEBIT_CARD_STR ) \
    MA_BOOKING_INFO_PAYMENT_EXPAND(MA_BOOKING_INFO_PAYMENT_TYPE_ONLINE, 3, MA_BOOKING_INFO_PAYMENT_TYPE_ONLINE_STR) \
    MA_BOOKING_INFO_PAYMENT_EXPAND(MA_BOOKING_INFO_PAYMENT_TYPE_CASH, 4, MA_BOOKING_INFO_PAYMENT_TYPE_CASH_STR)

#undef MA_BOOKING_INFO_PAYMENT_STATUS_EXPANDER
#define MA_BOOKING_INFO_PAYMENT_STATUS_EXPANDER\
    MA_BOOKING_INFO_PAYMENT_STATUS_EXPAND(MA_BOOKING_INFO_PAYMENT_STATUS_UNPAID, 0, MA_BOOKING_INFO_PAYMENT_STATUS_UNPAID_STR)\
    MA_BOOKING_INFO_PAYMENT_STATUS_EXPAND(MA_BOOKING_INFO_PAYMENT_STATUS_PAID, 1, MA_BOOKING_INFO_PAYMENT_STATUS_PAID_STR)

typedef enum ma_booking_info_payment_status_e {
#define MA_BOOKING_INFO_PAYMENT_STATUS_EXPAND(status, index, status_str) status = index,
    MA_BOOKING_INFO_PAYMENT_STATUS_EXPANDER
#undef MA_BOOKING_INFO_PAYMENT_STATUS_EXPAND
}ma_booking_info_payment_status_t;

typedef enum ma_booking_info_payment_type_e {
#define MA_BOOKING_INFO_PAYMENT_EXPAND(status, index, status_str) status = index,
    MA_BOOKING_INFO_PAYMENT_EXPANDER
#undef MA_BOOKING_INFO_PAYMENT_EXPAND
}ma_booking_info_payment_type_t;

#define MA_BOOKING_INFO_LOCATION_TYPE_SAME_AREA_STR "SameArea"
#define MA_BOOKING_INFO_LOCATION_TYPE_DIFF_AREA_STR "DiffArea"
#define MA_BOOKING_INFO_LOCATION_TYPE_ACROSS_CITY_STR "AcrossCity"

#undef MA_BOOKING_INFO_LOCATION_EXPANDER
#define MA_BOOKING_INFO_LOCATION_EXPANDER\
    MA_BOOKING_INFO_LOCATION_EXPAND(MA_BOOKING_INFO_LOCATION_TYPE_SAME_AREA, 1, MA_BOOKING_INFO_LOCATION_TYPE_SAME_AREA_STR)\
    MA_BOOKING_INFO_LOCATION_EXPAND(MA_BOOKING_INFO_LOCATION_TYPE_DIFF_AREA, 2, MA_BOOKING_INFO_LOCATION_TYPE_DIFF_AREA_STR) \
    MA_BOOKING_INFO_LOCATION_EXPAND(MA_BOOKING_INFO_LOCATION_TYPE_ACROSS_CITY, 3, MA_BOOKING_INFO_LOCATION_TYPE_ACROSS_CITY_STR)

typedef enum ma_booking_info_location_type_e {
#define MA_BOOKING_INFO_LOCATION_EXPAND(status, index, status_str) status = index,
    MA_BOOKING_INFO_LOCATION_EXPANDER
#undef MA_BOOKING_INFO_LOCATION_EXPAND
}ma_booking_info_location_type_t;

typedef enum ma_booking_info_user_type_e {
    MA_BOOKING_INFO_USER_TYPE_NORMAL,
    MA_BOOKING_INFO_USER_TYPE_CUSTOMER_EXECUTIVE,
    MA_BOOKING_INFO_USER_TYPE_OPERATION,
    MA_BOOKING_INFO_USER_TYPE_ADMIN,
    MA_BOOKING_INFO_USER_TYPE_SUPERADMIN,
}ma_booking_info_user_type_t;

typedef enum ma_booking_info_privilege_e {
    MA_BOOKING_INFO_PRIVILEGE_LOW,
    MA_BOOKING_INFO_PRIVILEGE_HIGH,
}ma_booking_info_privilege_t;

#undef MA_BOOKING_INFO_STATUS_EXPANDER
#define MA_BOOKING_INFO_STATUS_EXPANDER\
    MA_BOOKING_INFO_STATUS_EXPAND(MA_BOOKING_INFO_STATUS_NOT_ASSIGNED, 1, MA_BOOKING_INFO_STATUS_NOT_ASSIGNED_STR)\
    MA_BOOKING_INFO_STATUS_EXPAND(MA_BOOKING_INFO_STATUS_ASSIGNED, 2, MA_BOOKING_INFO_STATUS_ASSIGNED_STR) \
    MA_BOOKING_INFO_STATUS_EXPAND(MA_BOOKING_INFO_STATUS_CANCELLED, 3, MA_BOOKING_INFO_STATUS_CANCELLED_STR) \
    MA_BOOKING_INFO_STATUS_EXPAND(MA_BOOKING_INFO_STATUS_DELIVERED, 4, MA_BOOKING_INFO_STATUS_DELIVERED_STR) \
    MA_BOOKING_INFO_STATUS_EXPAND(MA_BOOKING_INFO_STATUS_NOT_DELIVERED, 5, MA_BOOKING_INFO_STATUS_NOT_DELIVERED_STR) \
    MA_BOOKING_INFO_STATUS_EXPAND(MA_BOOKING_INFO_STATUS_PICKED, 6, MA_BOOKING_INFO_STATUS_PICKED_STR) \
    MA_BOOKING_INFO_STATUS_EXPAND(MA_BOOKING_INFO_STATUS_TRANSIT, 7, MA_BOOKING_INFO_STATUS_TRANSIT_STR) \


/* booking life cycle state(s) */
typedef enum ma_booking_info_status_e {
#define MA_BOOKING_INFO_STATUS_EXPAND(status, index, status_str) status = index,
    MA_BOOKING_INFO_STATUS_EXPANDER
#undef MA_BOOKING_INFO_STATUS_EXPAND
}ma_booking_info_status_t;

#undef MA_BOOKING_INFO_DELIVERY_TYPE_EXPANDER
#define MA_BOOKING_INFO_DELIVERY_TYPE_EXPANDER\
    MA_BOOKING_INFO_DELIVERY_TYPE_EXPAND(MA_BOOKING_INFO_DELIVERY_TYPE_ONE_WAY, 1, MA_BOOKING_INFO_DELIVERY_TYPE_ONE_WAY_STR)\
    MA_BOOKING_INFO_DELIVERY_TYPE_EXPAND(MA_BOOKING_INFO_DELIVERY_TYPE_TWO_WAY, 2, MA_BOOKING_INFO_DELIVERY_TYPE_TWO_WAY_STR)


/* task execution life cycle state(s) */
typedef enum ma_booking_info_delivery_type_e {
#define MA_BOOKING_INFO_DELIVERY_TYPE_EXPAND(status, index, status_str) status = index,
    MA_BOOKING_INFO_DELIVERY_TYPE_EXPANDER
#undef MA_BOOKING_INFO_DELIVERY_TYPE_EXPAND
}ma_booking_info_delivery_type_t;

#undef MA_BOOKING_INFO_SERVICE_TYPE_EXPANDER
#define MA_BOOKING_INFO_SERVICE_TYPE_EXPANDER\
    MA_BOOKING_INFO_SERVICE_TYPE_EXPAND(MA_BOOKING_INFO_SERVICE_TYPE_PARCEL, 1, MA_BOOKING_INFO_SERVICE_TYPE_PARCEL_STR)\
    MA_BOOKING_INFO_SERVICE_TYPE_EXPAND(MA_BOOKING_INFO_SERVICE_TYPE_DOCUMENT, 2, MA_BOOKING_INFO_SERVICE_TYPE_DOCUMENT_STR)


/* Booking service types */
typedef enum ma_booking_info_service_type_e {
#define MA_BOOKING_INFO_SERVICE_TYPE_EXPAND(status, index, status_str) status = index,
    MA_BOOKING_INFO_SERVICE_TYPE_EXPANDER
#undef MA_BOOKING_INFO_SERVICE_TYPE_EXPAND
}ma_booking_info_service_type_t;


/* booking info ctor */
MA_BOOKING_API ma_error_t ma_booking_info_create(ma_booking_info_t **info, ma_booking_info_user_type_t user_type);

/* booking info dtor */
MA_BOOKING_API ma_error_t ma_booking_info_release(ma_booking_info_t *info);

/* setter */
MA_BOOKING_API ma_error_t ma_booking_info_set_delivery_note(ma_booking_info_t *info, const char *note);
MA_BOOKING_API ma_error_t ma_booking_info_set_payment_status_from_string(ma_booking_info_t *info, char *status);
MA_BOOKING_API ma_error_t ma_booking_info_set_payment_status(ma_booking_info_t *info, ma_booking_info_payment_status_t status);
MA_BOOKING_API ma_error_t ma_booking_info_set_package_image_url(ma_booking_info_t *info, const char *url);
MA_BOOKING_API ma_error_t ma_booking_info_set_active(ma_booking_info_t *info, ma_bool_t active);
MA_BOOKING_API ma_error_t ma_booking_info_set_transit(ma_booking_info_t *info, const char *str);
MA_BOOKING_API ma_error_t ma_booking_info_set_status_from_string(ma_booking_info_t *info, const char *str);
MA_BOOKING_API ma_error_t ma_booking_info_set_email_id(ma_booking_info_t *info, const char *id);
MA_BOOKING_API ma_error_t ma_booking_info_set_user(ma_booking_info_t *info, const char *user);
MA_BOOKING_API ma_error_t ma_booking_info_set_contact(ma_booking_info_t *info, const char *contact); 
MA_BOOKING_API ma_error_t ma_booking_info_set_from_address(ma_booking_info_t *info, const char *addr);
MA_BOOKING_API ma_error_t ma_booking_info_set_to_address(ma_booking_info_t *info, const char *addr);
MA_BOOKING_API ma_error_t ma_booking_info_set_picker_contact(ma_booking_info_t *info, const char *picker_contact);
MA_BOOKING_API ma_error_t ma_booking_info_set_last_login(ma_booking_info_t *info, ma_uint32_t time);
MA_BOOKING_API ma_error_t ma_booking_info_set_status(ma_booking_info_t *info, ma_booking_info_status_t status);
MA_BOOKING_API ma_error_t ma_booking_info_set_source_pincode(ma_booking_info_t *info, const char *pincode);
MA_BOOKING_API ma_error_t ma_booking_info_set_dest_pincode(ma_booking_info_t *info, const char *pincode);
MA_BOOKING_API ma_error_t ma_booking_info_set_user_type(ma_booking_info_t *info, ma_booking_info_user_type_t type);
MA_BOOKING_API ma_error_t ma_booking_info_set_privilege(ma_booking_info_t *info, ma_booking_info_privilege_t priv);
MA_BOOKING_API ma_error_t ma_booking_info_set_recipient(ma_booking_info_t *info, const char *recipient);
MA_BOOKING_API ma_error_t ma_booking_info_set_recipient_contact(ma_booking_info_t *info, const char *contact);
MA_BOOKING_API ma_error_t ma_booking_info_set_delivery_type(ma_booking_info_t *info, ma_booking_info_delivery_type_t type);
MA_BOOKING_API ma_error_t ma_booking_info_set_service_type(ma_booking_info_t *info, ma_booking_info_service_type_t type);
MA_BOOKING_API ma_error_t ma_booking_info_set_id(ma_booking_info_t *info, const char *id);
MA_BOOKING_API ma_error_t ma_booking_info_set_pickup_date(ma_booking_info_t *info, const char *date);
MA_BOOKING_API ma_error_t ma_booking_info_set_deliver_date(ma_booking_info_t *info, const char *str);
MA_BOOKING_API ma_error_t ma_booking_info_set_price(ma_booking_info_t *info, ma_uint32_t price);
MA_BOOKING_API ma_error_t ma_booking_info_set_discount(ma_booking_info_t *info, ma_uint32_t discount);
MA_BOOKING_API ma_error_t ma_booking_info_set_picker_email_id(ma_booking_info_t *info, const char *id);
MA_BOOKING_API ma_error_t ma_booking_info_set_deliver_email_id(ma_booking_info_t *info, const char *id);
MA_BOOKING_API ma_error_t ma_booking_info_set_details(ma_booking_info_t *info, const char *details);
MA_BOOKING_API ma_error_t ma_booking_info_set_payment_type(ma_booking_info_t *info, ma_booking_info_payment_type_t type);
MA_BOOKING_API ma_error_t ma_booking_info_set_distance(ma_booking_info_t *info, ma_uint32_t distance);
MA_BOOKING_API ma_error_t ma_booking_info_set_pick_time(ma_booking_info_t *info, long time);
MA_BOOKING_API ma_error_t ma_booking_info_set_deliver_time(ma_booking_info_t *info, long time);
MA_BOOKING_API ma_error_t ma_booking_info_set_weight(ma_booking_info_t *info, ma_uint32_t weight);
MA_BOOKING_API ma_error_t ma_booking_info_set_length(ma_booking_info_t *info, ma_uint32_t length);
MA_BOOKING_API ma_error_t ma_booking_info_set_estimate_delivery(ma_booking_info_t *info, ma_uint32_t days);
MA_BOOKING_API ma_error_t ma_booking_info_set_location_type(ma_booking_info_t *info, ma_booking_info_location_type_t loc_type);
MA_BOOKING_API ma_error_t ma_booking_info_set_from_city(ma_booking_info_t *info, const char *city);
MA_BOOKING_API ma_error_t ma_booking_info_set_from_state(ma_booking_info_t *info, const char *state);
MA_BOOKING_API ma_error_t ma_booking_info_set_from_country(ma_booking_info_t *info, const char *country);
MA_BOOKING_API ma_error_t ma_booking_info_set_to_city(ma_booking_info_t *info, const char *city);
MA_BOOKING_API ma_error_t ma_booking_info_set_to_state(ma_booking_info_t *info, const char *state);
MA_BOOKING_API ma_error_t ma_booking_info_set_to_country(ma_booking_info_t *info, const char *country);
MA_BOOKING_API ma_error_t ma_booking_info_set_payment_from_string(ma_booking_info_t *info, const char *str);
MA_BOOKING_API ma_error_t ma_booking_info_set_location_from_string(ma_booking_info_t *info, const char *str);
MA_BOOKING_API ma_error_t ma_booking_info_set_transitions(ma_booking_info_t *info, ma_table_t *transitions);

/* getter */
MA_BOOKING_API ma_error_t ma_booking_info_get_delivery_note(ma_booking_info_t *info, char *note);
MA_BOOKING_API ma_error_t ma_booking_info_get_payment_status(ma_booking_info_t *info, ma_booking_info_payment_status_t *status);
MA_BOOKING_API ma_error_t ma_booking_info_get_payment_status_str(ma_booking_info_t *info, char *status);
MA_BOOKING_API ma_error_t ma_booking_info_get_package_image_url(ma_booking_info_t *info, char *url);
MA_BOOKING_API ma_error_t ma_booking_info_get_active(ma_booking_info_t *info, ma_bool_t *active);
MA_BOOKING_API ma_error_t ma_booking_info_get_transitions(ma_booking_info_t *info, ma_table_t **transitions);
MA_BOOKING_API ma_error_t ma_booking_info_get_transit(ma_booking_info_t *info, char *str);
MA_BOOKING_API ma_error_t ma_booking_info_get_from_city(ma_booking_info_t *info, char *city);
MA_BOOKING_API ma_error_t ma_booking_info_get_to_city(ma_booking_info_t *info, char *city);
MA_BOOKING_API ma_error_t ma_booking_info_get_price(ma_booking_info_t *info, ma_uint32_t *price);
MA_BOOKING_API ma_error_t ma_booking_info_get_discount(ma_booking_info_t *info, ma_uint32_t *discount);
MA_BOOKING_API ma_error_t ma_booking_info_get_picker_email_id(ma_booking_info_t *info, char *id);
MA_BOOKING_API ma_error_t ma_booking_info_get_deliver_email_id(ma_booking_info_t *info, char *id);
MA_BOOKING_API ma_error_t ma_booking_info_get_details(ma_booking_info_t *info, char *details);
MA_BOOKING_API ma_error_t ma_booking_info_get_payment_type(ma_booking_info_t *info, ma_booking_info_payment_type_t *type);
MA_BOOKING_API ma_error_t ma_booking_info_get_distance(ma_booking_info_t *info, ma_uint32_t *distance);
MA_BOOKING_API ma_error_t ma_booking_info_get_pick_time(ma_booking_info_t *info, long *time);
MA_BOOKING_API ma_error_t ma_booking_info_get_deliver_time(ma_booking_info_t *info, long *time);
MA_BOOKING_API ma_error_t ma_booking_info_get_weight(ma_booking_info_t *info, ma_uint32_t *weight);
MA_BOOKING_API ma_error_t ma_booking_info_get_length(ma_booking_info_t *info, ma_uint32_t *length);
MA_BOOKING_API ma_error_t ma_booking_info_get_estimate_delivery(ma_booking_info_t *info, ma_uint32_t *days);
MA_BOOKING_API ma_error_t ma_booking_info_get_location_type(ma_booking_info_t *info, ma_booking_info_location_type_t *loc_type);
MA_BOOKING_API ma_error_t ma_booking_info_get_pickup_date(ma_booking_info_t *info, char *date);
MA_BOOKING_API ma_error_t ma_booking_info_get_recipient_contact(ma_booking_info_t *info, char *contact);
MA_BOOKING_API ma_error_t ma_booking_info_get_id(ma_booking_info_t *info, char *id);
MA_BOOKING_API ma_error_t ma_booking_info_get_service_type(ma_booking_info_t *info, ma_booking_info_service_type_t *type);
MA_BOOKING_API ma_error_t ma_booking_info_get_delivery_type(ma_booking_info_t *info, ma_booking_info_delivery_type_t *type);
MA_BOOKING_API ma_error_t ma_booking_info_get_recipient(ma_booking_info_t *info, char *recipient); 
MA_BOOKING_API ma_error_t ma_booking_info_get_privilege(ma_booking_info_t *info, ma_booking_info_privilege_t *priv);
MA_BOOKING_API ma_error_t ma_booking_info_get_user_type(ma_booking_info_t *info, ma_booking_info_user_type_t *type);
MA_BOOKING_API ma_error_t ma_booking_info_get_email_id(ma_booking_info_t *info, char *id);
MA_BOOKING_API ma_error_t ma_booking_info_get_user(ma_booking_info_t *info, char *user);
MA_BOOKING_API ma_error_t ma_booking_info_get_contact(ma_booking_info_t *info, char *contact); 
MA_BOOKING_API ma_error_t ma_booking_info_get_from_address(ma_booking_info_t *info, char *addr);
MA_BOOKING_API ma_error_t ma_booking_info_get_to_address(ma_booking_info_t *info, char *addr);
MA_BOOKING_API ma_error_t ma_booking_info_get_picker_contact(ma_booking_info_t *info, char *picker_contact);
MA_BOOKING_API ma_error_t ma_booking_info_get_last_login(ma_booking_info_t *info, ma_uint32_t *time);
MA_BOOKING_API ma_error_t ma_booking_info_get_status(ma_booking_info_t *info, ma_booking_info_status_t *status);
MA_BOOKING_API ma_error_t ma_booking_info_get_source_pincode(ma_booking_info_t *info, char *pincode);
MA_BOOKING_API ma_error_t ma_booking_info_get_dest_pincode(ma_booking_info_t *info, char *pincode);
MA_BOOKING_API ma_error_t ma_booking_info_get_status_str(ma_booking_info_t *info, char *str);
MA_BOOKING_API ma_error_t ma_booking_info_get_delivery_type_str(ma_booking_info_t *info, char *str);
MA_BOOKING_API ma_error_t ma_booking_info_get_service_type_str(ma_booking_info_t *info, char *str);
MA_BOOKING_API ma_error_t ma_booking_info_get_deliver_date(ma_booking_info_t *info, char *str);
MA_BOOKING_API ma_error_t ma_booking_info_add_transits(ma_booking_info_t *info, const char *transits);

/* convert booking info to variant  and vice versa */
MA_BOOKING_API ma_error_t ma_booking_info_convert_to_variant(ma_booking_info_t *info, ma_variant_t **variant);
MA_BOOKING_API ma_error_t ma_booking_info_convert_from_variant(ma_variant_t *variant, ma_booking_info_t **info);
MA_BOOKING_API ma_error_t ma_booking_info_convert_from_json(const char *json_str, ma_booking_info_t **binfo);
MA_BOOKING_API ma_error_t ma_booking_info_convert_to_json(ma_booking_info_t *info, ma_json_t **json);



MA_CPP(})

#endif /* MA_BOOKING_INFO_H_INCLUDED */

