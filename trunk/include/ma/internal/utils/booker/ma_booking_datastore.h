#ifndef MA_BOOKING_DATASTORE_H_INCLUDED
#define MA_BOOKING_DATASTORE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/booker/ma_booker.h"
#include "ma/internal/utils/booker/ma_booking_info.h"
#include "ma/ma_datastore.h"
#include "ma/datastore/ma_ds_database.h"
#include "ma/ma_variant.h"

MA_CPP(extern "C" {)
MA_BOOKING_API ma_error_t ma_booking_datastore_read(ma_booker_t *booker, ma_ds_t *datastore, const char *ds_path);

MA_BOOKING_API ma_error_t ma_booking_datastore_get(ma_booker_t *booker, ma_ds_t *datastore, const char *ds_path, const char *booking_id, ma_booking_info_t **info);

MA_BOOKING_API ma_error_t ma_booking_datastore_get_all(ma_booker_t *booker, ma_ds_t *datastore, const char *ds_path, const char *booking_id, ma_variant_t **var);

MA_BOOKING_API ma_error_t ma_booking_datastore_write(ma_booker_t *booker, ma_booking_info_t *info, ma_ds_t *datastore, const char *ds_path);

MA_BOOKING_API ma_error_t ma_booking_datastore_write_variant(ma_booker_t *booker, const char *booking_id, ma_ds_t *datastore, const char *ds_path, ma_variant_t *var);

MA_BOOKING_API ma_error_t ma_booking_datastore_remove_booking(const char *booking, ma_ds_t *datastore, const char *ds_path);

MA_BOOKING_API ma_error_t ma_booking_datastore_clear(ma_ds_t *datastore, const char *ds_path);

MA_CPP(})


#endif /* MA_BOOKING_DATASTORE_H_INCLUDED */

