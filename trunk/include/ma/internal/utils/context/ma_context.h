#ifndef MA_CONTEXT_H_INCLUDED
#define MA_CONTEXT_H_INCLUDED

#include "ma/ma_common.h"

MA_CPP(extern "C" {)

typedef struct ma_context_s ma_context_t, *ma_context_h;

MA_UTILS_API ma_error_t ma_context_create(ma_context_t **context);

MA_UTILS_API ma_error_t ma_context_release(ma_context_t *context);

/*!
 * adds a mapping (string -> void**)
 * Note this implementation assumes that 'name' remains in memory
 * note this is taking the address of the object pointer
 */
MA_UTILS_API ma_error_t ma_context_add_object_info(ma_context_t *context, char const *object_name, void * const * address);


MA_UTILS_API void *ma_context_get_object_info(ma_context_t *context, char const *object_name);


MA_CPP(})

#endif /* MA_CONTEXT_H_INCLUDED */

