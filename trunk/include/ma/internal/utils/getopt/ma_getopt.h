#ifndef MA_GETOPT_H_INCLUDED
#define MA_GETOPT_H_INCLUDED

#include "ma/ma_common.h"

MA_CPP(extern "C" {)
  
MA_UTILS_API int ma_getopt(int argc, char *argv[], char *optstring);

MA_UTILS_API const char *ma_getoptarg();

MA_UTILS_API const char *ma_getopt_str();

MA_CPP(})

#endif /*MA_GETOPT_H_INCLUDED*/
 
