#ifndef MA_HASH_H_INCLUDED
#define MA_HASH_H_INCLUDED

#include "ma/ma_common.h"

MA_CPP(extern "C" {)

MA_UTILS_API ma_uint32_t ma_hash_compute(const char *key, int len);

MA_CPP(})

#endif /* MA_HASH_H_INCLUDED */

