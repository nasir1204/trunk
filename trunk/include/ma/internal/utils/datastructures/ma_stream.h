#ifndef MA_STREAM_H_INCLUDED
#define MA_STREAM_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/datastructures/ma_bytebuffer.h"
#include "ma/internal/utils/threading/ma_atomic.h"

#define	MA_STREAM_ACCESS_FLAG_READ   0x01
#define MA_STREAM_ACCESS_FLAG_WRITE  0x02
#define MA_STREAM_ACCESS_FLAG_APPEND 0x04

MA_CPP(extern "C" {)

typedef struct ma_stream_s ma_stream_t;
typedef struct ma_stream_methods_s {
	ma_error_t (*write)(ma_stream_t *self,const unsigned char *ptr, size_t size, size_t *written_bytes) ;
    ma_error_t (*read)(ma_stream_t *self,unsigned char *ptr, size_t size, size_t *read_bytes) ;    	
	ma_error_t (*seek)(ma_stream_t *self,size_t offset) ;    	
	ma_error_t (*get_size)(ma_stream_t *self,size_t *size) ;    
	ma_error_t (*get_buffer)(ma_stream_t *self,ma_bytebuffer_t **);			
	ma_error_t (*release)(ma_stream_t *self) ;
}ma_stream_methods_t;

struct ma_stream_s {
	ma_stream_methods_t *vtable;
    ma_atomic_counter_t ref_count;
	void *userdata;	
};

MA_UTILS_API ma_error_t ma_fstream_create(const char *filename , int falgs, void *userdata,ma_stream_t **stream);
MA_UTILS_API ma_error_t ma_mstream_create(size_t initialsize ,void *userdata,ma_stream_t **stream);

typedef size_t (*ma_netstream_cb_t)(const unsigned char *data, size_t size, void *userdata);
MA_UTILS_API ma_error_t ma_netstream_create(ma_netstream_cb_t cb, void *userdata, ma_stream_t **stream);

MA_UTILS_API ma_error_t ma_stream_write(ma_stream_t *self,const unsigned char *ptr, size_t size,size_t *written_bytes);
MA_UTILS_API ma_error_t ma_stream_read(ma_stream_t *self,unsigned char *ptr, size_t size,size_t *read_bytes);
MA_UTILS_API ma_error_t ma_stream_seek(ma_stream_t *self,size_t offset) ;
MA_UTILS_API ma_error_t ma_stream_get_size(ma_stream_t *self,size_t *size);
MA_UTILS_API ma_error_t ma_stream_get_buffer(ma_stream_t *self,ma_bytebuffer_t **buffer);
MA_UTILS_API ma_error_t ma_stream_release(ma_stream_t *self);
MA_UTILS_API ma_error_t ma_stream_add_ref(ma_stream_t *self);

MA_CPP(})

#endif  //MA_STREAM_H_INCLUDED


