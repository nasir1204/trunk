#ifndef MA_BYTEBUFFER_H_INCLUDED
#define MA_BYTEBUFFER_H_INCLUDED

#include "ma/ma_common.h"

MA_CPP(extern "C" {)

typedef struct ma_bytebuffer_s ma_bytebuffer_t, *ma_bytebuffer_h;

MA_UTILS_API ma_error_t ma_bytebuffer_create(size_t capacity, ma_bytebuffer_t **byte_buffer);

MA_UTILS_API ma_error_t ma_bytebuffer_add_ref(ma_bytebuffer_t *self);

MA_UTILS_API ma_error_t ma_bytebuffer_reserve(ma_bytebuffer_t *self, size_t capacity);

MA_UTILS_API ma_error_t ma_bytebuffer_shrink(ma_bytebuffer_t *self, size_t size);

MA_UTILS_API ma_error_t ma_bytebuffer_set_bytes(ma_bytebuffer_t *self, size_t offset, const unsigned char *buffer, size_t len);

MA_UTILS_API ma_error_t ma_bytebuffer_append_bytes(ma_bytebuffer_t *self, const unsigned char *buffer, size_t len);

MA_UTILS_API unsigned char *ma_bytebuffer_get_bytes(ma_bytebuffer_t *self);

MA_UTILS_API size_t ma_bytebuffer_get_capacity(ma_bytebuffer_t *self);

MA_UTILS_API size_t ma_bytebuffer_get_size(ma_bytebuffer_t *self);

MA_UTILS_API ma_error_t ma_bytebuffer_release(ma_bytebuffer_t *self);

MA_CPP(})


#endif //MA_BYTEBUFFER_H_INCLUDED

