#ifndef MA_MAP_H_INCLUDED
#define MA_MAP_H_INCLUDED

#include "ma/ma_common.h"
#include "uv-private/tree.h"

MA_CPP(extern "C" {)

/*
 * TBD - Should we allow dllexport of the below functions so that the user can use the map anywhere
 * This macro defines the prototype of the map that you wish to create
 * KEY_TYPE and VALUE_TYPE can take in anything from int to structure pointers
 */
#define MA_MAP_DECLARE(NAME, KEY_TYPE, VALUE_TYPE)\
		typedef struct NAME##_iterator_t {\
			void *node;\
			KEY_TYPE first;\
			VALUE_TYPE second;\
		}NAME##_iterator_t;\
		typedef struct NAME##_t NAME##_t;\
		ma_error_t NAME##_create(NAME##_t **map);\
		ma_error_t NAME##_release(NAME##_t *map);\
		ma_error_t NAME##_add_entry(NAME##_t *map, const KEY_TYPE key, VALUE_TYPE value);\
		ma_error_t NAME##_remove_entry(NAME##_t *map, const KEY_TYPE key);\
		ma_error_t NAME##_size(NAME##_t *map, size_t *size);\
		ma_error_t NAME##_get_value(NAME##_t *self, const KEY_TYPE key, VALUE_TYPE *value);\
		ma_error_t NAME##_get_next(NAME##_t *self, NAME##_iterator_t *iter);\
		ma_error_t NAME##_clear(NAME##_t *self);\
/*
 * Provides the map implementation
 * This map is generic, if the user of this wishes that the map must not own the responsiblity of owning the key or value,
 * or should contain a copy of the key/value, he can supply the KEY_COPY_CONSTRUCTOR/VALUE_COPY_CONSTRUCTOR. If the map is
 * supposed to destroy objects during remove_entry or release, then the destructor can be supplied. If the map is just a container
 * of the actual objects, then the copy constructor and destructor can be left empty.
 * eg. if create a map of string matched to integers
 * MA_MAP_DECLARE(map_string_to_int, char *, int); in a .h file
 * For map owning the contents(copies any string passed)
 * MA_MAP_DEFINE(map_string_to_int, char *, strdup, free, int, , ,strcmp);
 * For map that stores the string directly will be defined like this
 * MA_MAP_DEFINE(map_string_to_int, char *, , , int , , ,strcmp);
 * KEY_COMPARE_FN is required and cannot be empty.
 */
#define MA_MAP_DEFINE(NAME, KEY_TYPE, KEY_COPY_CONSTRUCTOR, KEY_DESTRUCTOR, VALUE_TYPE, VALUE_COPY_CONSTRUCTOR, VALUE_DESTRUCTOR, KEY_COMPARE_FN )\
		typedef struct NAME##_tree_s NAME##_tree_t;\
		typedef struct NAME##_node_t NAME##_node_t;\
		struct NAME##_node_t {\
			RB_ENTRY(NAME##_node_t) map_entry;\
			KEY_TYPE key;\
			VALUE_TYPE value;\
		};\
		RB_HEAD(NAME##_tree_s, NAME##_node_t);\
		struct NAME##_t{\
			NAME##_tree_t rb_tree;\
		};\
		static int NAME##_node_compare(NAME##_node_t *node1, NAME##_node_t *node2);\
		RB_GENERATE_STATIC(NAME##_tree_s, NAME##_node_t, map_entry, NAME##_node_compare);\
		int NAME##_node_compare(NAME##_node_t *node1, NAME##_node_t *node2){\
			return KEY_COMPARE_FN(node1->key, node2->key);\
		}\
		ma_error_t NAME##_node_create(NAME##_node_t **node){\
			if(node){\
				*node = (NAME##_node_t *) calloc(1, sizeof(NAME##_node_t));\
				return MA_OK;\
			}\
			return MA_ERROR_INVALIDARG;\
		}\
		ma_error_t NAME##_node_destroy(NAME##_node_t *node) {\
			if(node){\
				KEY_DESTRUCTOR(node->key);\
				VALUE_DESTRUCTOR(node->value);\
				free(node);\
				return MA_OK;\
			}\
			return MA_ERROR_INVALIDARG;\
		}\
		ma_error_t NAME##_insert_into_rb_tree(NAME##_tree_t *tree, NAME##_node_t *node) {\
			if(tree && node) {\
				NAME##_node_t *found_node = NULL;\
				if(NULL != (found_node = RB_INSERT(NAME##_tree_s, tree, node))) {\
					VALUE_DESTRUCTOR(found_node->value);\
					found_node->value = VALUE_COPY_CONSTRUCTOR(node->value);\
					NAME##_node_destroy(node);\
				}\
				return MA_OK;\
			}\
			return MA_ERROR_INVALIDARG;\
		}\
		ma_error_t NAME##_erase_tree(NAME##_node_t *node) {\
			if(node){\
				NAME##_erase_tree(node->map_entry.rbe_left);\
				NAME##_erase_tree(node->map_entry.rbe_right);\
				return NAME##_node_destroy(node);\
			}\
			return MA_OK;\
		}\
		NAME##_node_t *NAME##_find_entry_in_tree(NAME##_tree_t *root, const KEY_TYPE key) {\
			if(root){\
				NAME##_node_t node = {0};\
				NAME##_node_t *entry = NULL;\
				node.key = (KEY_TYPE) key;\
				entry = RB_FIND(NAME##_tree_s,root, &node);\
				return entry;\
			}\
			return NULL;\
		}\
		ma_error_t NAME##_remove_entry_in_tree(NAME##_tree_t *root, KEY_TYPE key) {\
			if(root) {\
				ma_error_t err = MA_ERROR_OBJECTNOTFOUND;\
				NAME##_node_t *entry = NULL;\
				if(entry = NAME##_find_entry_in_tree(root, key)){\
					RB_REMOVE(NAME##_tree_s, root, entry);\
					err = NAME##_node_destroy(entry);\
				}\
				return err;\
			}\
			return MA_ERROR_INVALIDARG;\
		}\
		ma_error_t NAME##_get_node_count(NAME##_node_t *node, size_t *size) {\
			if(size) {\
				if(node) {\
					*size = (*size) + 1;\
					NAME##_get_node_count(node->map_entry.rbe_left, size);\
					NAME##_get_node_count(node->map_entry.rbe_right, size);\
				}\
				return MA_OK;\
			}\
			return MA_ERROR_INVALIDARG;\
		}\
		ma_error_t NAME##_size(NAME##_t *self,size_t *size){\
			if(self && size){\
				*size = 0;\
				return NAME##_get_node_count(self->rb_tree.rbh_root, size);\
			}\
			return MA_ERROR_INVALIDARG;\
		}\
		ma_error_t NAME##_create(NAME##_t **map) {\
			if(map){\
				NAME##_t *self = NULL;\
				self = (NAME##_t *) calloc(1,sizeof(NAME##_t));\
				if(self){\
					*map = self;\
					return MA_OK;\
				}\
				return MA_ERROR_OUTOFMEMORY;\
			}\
			return MA_ERROR_INVALIDARG;\
		}\
		ma_error_t NAME##_release(NAME##_t *self) {\
			if(self){\
				if(self->rb_tree.rbh_root){\
					NAME##_erase_tree(self->rb_tree.rbh_root->map_entry.rbe_left);\
					NAME##_erase_tree(self->rb_tree.rbh_root->map_entry.rbe_right);\
				}\
				NAME##_node_destroy(self->rb_tree.rbh_root);\
				free(self);\
				return MA_OK;\
			}\
			return MA_ERROR_INVALIDARG;\
		}\
		ma_error_t NAME##_clear(NAME##_t *self) {\
			if(self) {\
				if(self->rb_tree.rbh_root){\
					NAME##_erase_tree(self->rb_tree.rbh_root->map_entry.rbe_left);\
					NAME##_erase_tree(self->rb_tree.rbh_root->map_entry.rbe_right);\
				}\
				NAME##_node_destroy(self->rb_tree.rbh_root);\
				self->rb_tree.rbh_root = NULL;\
				return MA_OK;\
			}\
			return MA_ERROR_INVALIDARG;\
		}\
		ma_error_t NAME##_add_entry(NAME##_t *self, const KEY_TYPE key, VALUE_TYPE value) {\
			if(self){\
				NAME##_node_t *node = NULL;\
				if(MA_OK == NAME##_node_create(&node)) {\
					node->key = KEY_COPY_CONSTRUCTOR((KEY_TYPE)key);\
					node->value = VALUE_COPY_CONSTRUCTOR(value);\
					if(!self->rb_tree.rbh_root) {\
						self->rb_tree.rbh_root = node;\
						return MA_OK;\
					}\
					else {\
						if(MA_OK == NAME##_insert_into_rb_tree(&self->rb_tree, node)) \
							return MA_OK;\
					}\
					NAME##_node_destroy(node);\
				}\
				return MA_ERROR_APIFAILED;\
			}\
			return MA_ERROR_INVALIDARG;\
		}\
		ma_error_t NAME##_remove_entry(NAME##_t *self, const KEY_TYPE key) {\
			if(self){\
				return NAME##_remove_entry_in_tree(&self->rb_tree,(KEY_TYPE) key);\
			}\
			return MA_ERROR_INVALIDARG;\
		}\
		ma_error_t NAME##_get_value(NAME##_t *self, const KEY_TYPE key, VALUE_TYPE *value) {\
			if(self && value) {\
				NAME##_node_t *entry = NULL;\
				if(entry = NAME##_find_entry_in_tree(&self->rb_tree, (KEY_TYPE)key)) {\
					*value = entry->value;\
					return MA_OK;\
				}\
				return MA_ERROR_OBJECTNOTFOUND;\
			}\
			return MA_ERROR_INVALIDARG;\
		}\
		ma_error_t NAME##_get_next(NAME##_t *self, NAME##_iterator_t *iter) {\
			if(self && iter){\
				NAME##_node_t *node = (NAME##_node_t *) (iter->node);\
				if(!node){\
					node = RB_MIN(NAME##_tree_s,&self->rb_tree);\
					if(node){\
						iter->node = node;\
						iter->first = node->key;\
						iter->second = node->value;\
						return MA_OK;\
					}\
				}\
				else {\
					node = NAME##_tree_s##_RB_NEXT(node);\
					if(node) {\
						iter->node = node;\
						iter->first = node->key;\
						iter->second = node->value;\
						return MA_OK;\
					}\
					return MA_ERROR_OBJECTNOTFOUND;\
				}\
				return MA_ERROR_OBJECTNOTFOUND;\
			}\
			return MA_ERROR_INVALIDARG;\
		}\


/*This declares a variable*/
#define MA_MAP_T(NAME) NAME##_t

/*Create the map*/
#define MA_MAP_CREATE(NAME,VAR_NAME)				NAME##_create(&VAR_NAME)
#define MA_MAP_ADD_ENTRY(NAME, VAR_NAME,KEY,VALUE)	NAME##_add_entry(VAR_NAME, KEY, VALUE)
#define MA_MAP_REMOVE_ENTRY(NAME, VAR_NAME, KEY)	NAME##_remove_entry(VAR_NAME, KEY)
#define MA_MAP_GET_VALUE(NAME,VAR_NAME,KEY,VALUE)	NAME##_get_value(VAR_NAME, KEY, &VALUE)
#define MA_MAP_SIZE(NAME,VAR_NAME,SIZE)				NAME##_size(VAR_NAME, &SIZE)
#define MA_MAP_RELEASE(NAME, VAR_NAME)				NAME##_release(VAR_NAME)
#define MA_MAP_CLEAR(NAME, VAR_NAME)				NAME##_clear(VAR_NAME)

/*Needs to be put inside a curly brace, since a variable is being declared.
* iter is the variable name for iter, can be dot-dereferenced to get the key and value
* eg.
* int main(){
*   MA_MAP_T(ma_sample_map) map = NULL;
*   . . .
*   {
*		MA_MAP_FOREACH(ma_sample_map,map, iter) {
*		}
*   }
*   {//second iteration
*		MA_MAP_FOREACH(ma_sample_map,map, iter) {
*		}
*   }
* }
*/
#define MA_MAP_FOREACH(NAME,VAR_NAME,iter)			\
	NAME##_iterator_t iter = {0};\
	ma_error_t NAME##loop_error = MA_OK;\
	for(NAME##loop_error = NAME##_get_next(VAR_NAME, &iter);NAME##loop_error == MA_OK;NAME##loop_error = NAME##_get_next(VAR_NAME, &iter))

MA_CPP(})

#endif /* MA_MAP_H_INCLUDED */

