#ifndef MA_BYTE_STRAM_H_INCLUDED
#define MA_BYTE_STRAM_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/datastructures/ma_bytebuffer.h"
#include "ma/internal/utils/datastructures/ma_stream.h"

MA_CPP(extern "C" {)

typedef enum ma_bytestream_endianness_e {
	MA_BIG_ENDIAN=0,
	MA_LITTLE_ENDIAN,
}ma_bytestream_endianness_t;

typedef struct ma_bytestream_s ma_bytestream_t, *ma_bytestream_h;

MA_UTILS_API ma_error_t ma_bytestream_create(ma_bytestream_endianness_t byteorder,size_t size,ma_bytestream_t **bstream);
MA_UTILS_API ma_error_t ma_bytestream_set_stream(ma_bytestream_t *self,ma_stream_t *streamtouse);
MA_UTILS_API ma_error_t ma_bytestream_release(ma_bytestream_t *self);
MA_UTILS_API ma_error_t ma_bytestream_write_uint16(ma_bytestream_t *self,ma_uint16_t data);
MA_UTILS_API ma_error_t ma_bytestream_write_uint32(ma_bytestream_t *self,ma_uint32_t data);
MA_UTILS_API ma_error_t ma_bytestream_write_uint64(ma_bytestream_t *self,ma_uint64_t data);
MA_UTILS_API ma_error_t ma_bytestream_write_buffer(ma_bytestream_t *self,const unsigned char *data,size_t len,size_t *written);
MA_UTILS_API ma_error_t ma_bytestream_read_uint16(ma_bytestream_t *self,ma_uint16_t *data);
MA_UTILS_API ma_error_t ma_bytestream_read_uint32(ma_bytestream_t *self,ma_uint32_t *data);
MA_UTILS_API ma_error_t ma_bytestream_read_uint64(ma_bytestream_t *self,ma_uint64_t *data);
MA_UTILS_API ma_error_t ma_bytestream_read_buffer(ma_bytestream_t *self,unsigned char *data,size_t len,size_t *read);
MA_UTILS_API ma_error_t ma_bytestream_seek(ma_bytestream_t *self,size_t offset);

MA_UTILS_API ma_bytebuffer_t *ma_bytestream_get_buffer(ma_bytestream_t *self);
MA_UTILS_API size_t ma_bytestream_get_size(ma_bytestream_t *self);	


MA_CPP(})

#endif //MA_BYTE_STRAM_H_INCLUDED

