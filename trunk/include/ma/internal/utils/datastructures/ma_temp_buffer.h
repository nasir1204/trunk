#ifndef MA_TEMP_BUFFER_H_INCLUDED
#define MA_TEMP_BUFFER_H_INCLUDED

#include "ma/ma_common.h"

#include <string.h>
#include <stdlib.h>

MA_CPP(extern "C" {)

#define MA_TEMP_BUFFER_STATIC_CAPACITY 256

typedef struct ma_temp_buffer_s {
    /* Please don't access these members or make any assumptions on the logic on how these are use, use the API below instead */
    size_t capacity;
    void   *extra;
    union {
        ma_wchar_t dummy[1]; /* To enforce the alignment */
        char static_buffer[MA_TEMP_BUFFER_STATIC_CAPACITY];
        void (*free_fn)(void*); /* de-allocator for dynamically allocated memory */
    } b;
} ma_temp_buffer_t;

/* For quick initialization */
#define MA_TEMP_BUFFER_INIT {MA_TEMP_BUFFER_STATIC_CAPACITY,0}

MA_UTILS_API void ma_temp_buffer_init(ma_temp_buffer_t *b);

MA_UTILS_API void ma_temp_buffer_uninit(ma_temp_buffer_t *b);

MA_UTILS_API size_t ma_temp_buffer_capacity(ma_temp_buffer_t *b);

MA_UTILS_API void *ma_temp_buffer_get(ma_temp_buffer_t *b) ;

/*!
 * returns the address of the buffer. This is typically used with functions that allocate memory, but you then want that memory to be managed by ma_temp_buffer
 */
MA_UTILS_API void **ma_temp_buffer_address_of(ma_temp_buffer_t *b, void (*free_fn)(void*)) ;

MA_UTILS_API void ma_temp_buffer_reserve(ma_temp_buffer_t *b, size_t bytes_requested);

MA_UTILS_API void ma_temp_buffer_copy(ma_temp_buffer_t *dest , const ma_temp_buffer_t *source);

MA_CPP(})

#endif /* MA_TEMP_BUFFER_H_INCLUDED */

