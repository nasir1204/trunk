#ifndef MA_VECTOR_H_INCLUDED
#define MA_VECTOR_H_INCLUDED

#define ma_vector_define(prefix, type)		\
	ma_uint32_t prefix ## _size;			\
	ma_uint32_t prefix ## _capacity;	    \
	type *prefix ## _ptr

#define ma_vector_size(prefix)		 prefix ## _size 
#define ma_vector_capacity(prefix)   prefix ## _capacity 
#define ma_vector_ptr(prefix)		 prefix ## _ptr 

#define ma_vector_get_size(v,prefix)	    ((v)->ma_vector_size(prefix))
#define ma_vector_get_capacity(v,prefix)	((v)->ma_vector_capacity(prefix))
#define ma_vector_get_ptr(v,prefix)	        ((v)->ma_vector_ptr(prefix))

#define v_exit() exit(-1)                                

#define ma_vector_init(v, n, prefix, type)                                                                  \
  do {                                                                                                      \
    ma_vector_get_size(v,prefix)        = 0;                                                                \
    ma_vector_get_capacity(v,prefix)    = n;                                                                \
    if(NULL == (ma_vector_get_ptr(v,prefix) = (type*) calloc(n, sizeof(type))))  v_exit();                  \
  }                                                                                                         \
  while (0)

#define ma_vector_empty(v, prefix)                                                                          \
    (0 == ma_vector_get_size(v,prefix))

#define ma_vector_full(v, prefix)                                                                           \
    (ma_vector_get_capacity(v,prefix) == ma_vector_get_size(v,prefix))

#define ma_vector_reserve(v, n, prefix, type)                                                                               \
  do {                                                                                                                      \
    type *tmp_buf = NULL;                                                                                                   \
    ma_vector_get_capacity(v,prefix) += n;                                                                                  \
    if(NULL == (tmp_buf = (type*)realloc(ma_vector_get_ptr(v,prefix) , ma_vector_get_capacity(v,prefix)*sizeof(type))))  {  \
        free(ma_vector_get_ptr(v,prefix));                                                                                  \
        v_exit();                                                                                                           \
    }                                                                                                                       \
    ma_vector_get_ptr(v,prefix) = tmp_buf;                                                                                  \
  }                                                                                                                         \
  while (0)

#define ma_vector_at(v, index, type, prefix)                                         \
    ((ma_vector_empty(v, prefix) || (index >= ma_vector_get_size(v,prefix))) ? 0 : ma_vector_get_ptr(v,prefix)[index])

#define ma_vector_push_back(v, x, type, prefix)                                    \
  do {                                                                             \
    if(ma_vector_full(v, prefix))                                                  \
        ma_vector_reserve(v, ma_vector_get_capacity(v,prefix), prefix, type);      \
    ma_vector_get_ptr(v,prefix)[ma_vector_get_size(v,prefix)] = x;                 \
    ma_vector_get_size(v,prefix)++;                                                \
  }                                                                                \
  while(0)

#define ma_vector_pop_back(v, type, prefix, release_func)                          \
  do {                                                                             \
    if(!ma_vector_empty(v, prefix) && release_func)  {                             \
        release_func(ma_vector_get_ptr(v,prefix)[ma_vector_get_size(v,prefix)-1]); \
        ma_vector_get_size(v,prefix)--;                                            \
    }                                                                              \
  }                                                                                \
  while(0)

#define ma_vector_remove(v, x, type, prefix, release_func)                                                      \
  do {                                                                                                          \
        ma_uint32_t index = 0;                                                                                  \
        for(index=0; index<ma_vector_get_size(v, prefix); index++) {                                            \
            if(x == ma_vector_get_ptr(v,prefix)[index])                                                         \
                break;                                                                                          \
        };                                                                                                      \
        if(index < ma_vector_get_size(v, prefix)) {                                                             \
            ma_vector_get_ptr(v,prefix)[index] = ma_vector_get_ptr(v,prefix)[ma_vector_get_size(v,prefix)-1];   \
            ma_vector_get_ptr(v,prefix)[ma_vector_get_size(v,prefix)-1] = x ;                                   \
            ma_vector_pop_back(v, type, prefix, release_func);                                                  \
        }                                                                                                       \
  }                                                                                                             \
  while(0)

#define ma_vector_clear(v, type, prefix, release_func)                        \
  do {                                                                        \
    if(!ma_vector_empty(v, prefix) && release_func) {                         \
      ma_uint32_t i = 0;                                                      \
      ma_uint32_t size = ma_vector_get_size(v,prefix);                        \
      for(i=0; i < size; i++)                                                 \
        ma_vector_pop_back(v, type, prefix, release_func);                    \
      free(ma_vector_get_ptr(v,prefix));                                      \
      ma_vector_get_ptr(v,prefix) = NULL;                                     \
    }                                                                         \
  }                                                                           \
  while(0)

#define ma_vector_sort(v, type, prefix, compare_func)                                                   \
  do {                                                                                                  \
    qsort(ma_vector_get_ptr(v,prefix), ma_vector_get_size(v,prefix), sizeof(type), compare_func);       \
  }                                                                                                     \
  while(0)

#endif /* MA_VECTOR_H_INCLUDED */
