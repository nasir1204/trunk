#ifndef MA_SLIST_H_INCLUDED
#define MA_SLIST_H_INCLUDED

#include "ma/ma_common.h"

MA_CPP( extern "C" { )

/**
	This list will not facilitate a direct basictype(of int,float, char etc. ) list.
	If any list is wanted, 
	define the node that represents your data,
	e.g.	struct int_list_node_s
			{
				int a;
				MA_SLIST_NODE_DEFINE(struct int_list_node_s);
			}; 
	then define the list as 
	
	struct int_list
	{
		MA_SLIST_DEFINE(data,struct int_list_node_s); 
		//add any other data members you want to have along with the list
	};

	and use any of the below macros to add, delete or get the count of items in the list.

	Idea is basically implement how normally people implement lists of each data type(create node, then the list structure to hold the head/tail).
	
*/
#define MA_SLIST_NODE_DEFINE(type)	\
	type *p_link

#define MA_SLIST_DEFINE(prefix, type)		\
	type *prefix ## _head;					\
	type *prefix ## _tail;					\
	ma_uint32_t prefix ## _count
	
#define MA_SLIST_HEAD(prefix)		 prefix ## _head 
#define MA_SLIST_TAIL(prefix)		 prefix ## _tail 
#define MA_SLIST_COUNT(prefix)		 prefix ## _count 

#define MA_SLIST_GET_HEAD(l_list,prefix)	(l_list)->MA_SLIST_HEAD(prefix)
#define MA_SLIST_GET_TAIL(l_list,prefix)	(l_list)->MA_SLIST_TAIL(prefix)
#define MA_SLIST_GET_COUNT(l_list,prefix)	(l_list)->MA_SLIST_COUNT(prefix)

#define MA_SLIST_FOREACH(l_list,prefix, node)	for(node = (l_list)->MA_SLIST_HEAD(prefix); node != NULL; node = (node)->p_link)

#define MA_SLIST_NODE_INIT(l_node)		(l_node)->p_link = NULL;
#define MA_SLIST_NODE_DEINIT(l_node)	(l_node)->p_link = NULL;

#define MA_SLIST_INIT(l_list,prefix)											\
	(l_list)->MA_SLIST_HEAD(prefix) = (l_list)->MA_SLIST_TAIL(prefix) = NULL;	\
	(l_list)->MA_SLIST_COUNT(prefix) = 0;

#define MA_SLIST_EMPTY(l_list, prefix)                                          \
    (0 == MA_SLIST_GET_COUNT(l_list,prefix))

#define MA_SLIST_GET_NEXT_NODE(l_current)	        	                        \
    (l_current ? l_current->p_link : NULL)

#define MA_SLIST_GET_PREV_NODE(l_list, l_current, l_node, prefix)				\
	MA_SLIST_FOREACH(l_list,prefix,l_node) {									\
		if(l_node->p_link == l_current)											\
			break;																\
	}																			\

#define MA_SLIST_PUSH_BACK(l_list,prefix,l_node)											\
	do	{																					\
		if(l_node){																			\
			if((l_list)->MA_SLIST_TAIL(prefix) != NULL) {									\
				(l_list)->MA_SLIST_TAIL(prefix)->p_link = l_node;							\
				(l_node)->p_link = NULL;													\
				(l_list)->MA_SLIST_TAIL(prefix) = l_node;									\
			}																				\
			else {																			\
				(l_list)->MA_SLIST_HEAD(prefix) = (l_list)->MA_SLIST_TAIL(prefix) = l_node;	\
			}																				\
			((l_list)->MA_SLIST_COUNT(prefix))++;											\
		}																					\
	}while(0)

#define MA_SLIST_PUSH_FRONT(l_list,prefix,l_node)											\
	do	{																					\
		if(l_node) {																		\
			if((l_list)->MA_SLIST_HEAD(prefix) != NULL)	{									\
				(l_node)->p_link = (l_list)->MA_SLIST_HEAD(prefix);							\
				(l_list)->MA_SLIST_HEAD(prefix) = l_node;									\
			}																				\
			else {																			\
				(l_list)->MA_SLIST_HEAD(prefix) = (l_list)->MA_SLIST_TAIL(prefix) = l_node;	\
			}																				\
			((l_list)->MA_SLIST_COUNT(prefix))++;											\
		}																					\
	}while(0)

#define MA_SLIST_POP_BACK(l_list,prefix,l_node)											\
do	{																					\
	if((l_list)->MA_SLIST_TAIL(prefix)==NULL) {											\
		l_node = NULL;																	\
		(l_list)->MA_SLIST_COUNT(prefix) --;											\
	}																					\
	else if((l_list)->MA_SLIST_TAIL(prefix) == (l_list)->MA_SLIST_HEAD(prefix)){		\
		l_node = (l_list)->MA_SLIST_TAIL(prefix);										\
		(l_list)->MA_SLIST_HEAD(prefix) = (l_list)->MA_SLIST_TAIL(prefix) = NULL;		\
		(l_list)->MA_SLIST_COUNT(prefix) --;											\
	}																					\
	else {																				\
		MA_SLIST_GET_PREV_NODE(l_list,(l_list)->MA_SLIST_TAIL(prefix),l_node,prefix);	\
		if(l_node) {                                            						\
			(l_list)->MA_SLIST_TAIL(prefix) = l_node;									\
			l_node = (l_list)->MA_SLIST_TAIL(prefix)->p_link;							\
			(l_list)->MA_SLIST_TAIL(prefix)->p_link = NULL;     						\
			(l_list)->MA_SLIST_COUNT(prefix)--;											\
		}                                                       						\
	}																					\
}while(0)

#define MA_SLIST_POP_FRONT(l_list, prefix, l_node)									\
do	{																				\
	if((l_list)->MA_SLIST_HEAD(prefix)==NULL){										\
		l_node = NULL;																\
		(l_list)->MA_SLIST_COUNT(prefix) --;										\
	}																				\
	else if((l_list)->MA_SLIST_HEAD(prefix) == (l_list)->MA_SLIST_TAIL(prefix)){	\
		l_node = (l_list)->MA_SLIST_HEAD(prefix);									\
		(l_list)->MA_SLIST_HEAD(prefix) = (l_list)->MA_SLIST_TAIL(prefix) = NULL;	\
		(l_list)->MA_SLIST_COUNT(prefix) --;										\
	}																				\
	else {																			\
		l_node = (l_list)->MA_SLIST_HEAD(prefix);									\
		(l_list)->MA_SLIST_HEAD(prefix) = l_node->p_link;							\
		(l_list)->MA_SLIST_COUNT(prefix) --;											\
	}																				\
}while(0)


#define MA_SLIST_REMOVE_NODE(l_list, prefix, l_rnode, type)						\
do {																				\
		type *l_tnode = NULL;														\
		if((l_list)->MA_SLIST_HEAD(prefix) == l_rnode)								\
			MA_SLIST_POP_FRONT(l_list,prefix,l_rnode);								\
		else if((l_list)->MA_SLIST_TAIL(prefix) == l_rnode)							\
			MA_SLIST_POP_BACK(l_list,prefix,l_rnode);								\
		else {																		\
			MA_SLIST_FOREACH(l_list,prefix, l_tnode) {								\
				if(l_tnode == l_rnode) {											\
					MA_SLIST_GET_PREV_NODE(l_list,l_rnode,l_tnode,prefix);			\
					if(l_tnode)	{													\
						l_tnode->p_link = l_rnode->p_link;							\
						l_rnode->p_link = NULL;										\
						(l_list)->MA_SLIST_COUNT(prefix) --;						\
					}																\
				}																	\
			}																		\
		}																			\
}while(0)

#define MA_SLIST_CLEAR(l_list, prefix,type,release_func)						\
	do{																			\
		type *l_node = NULL;															\
		while(MA_SLIST_GET_COUNT(l_list,prefix) > 0) {							\
			MA_SLIST_POP_FRONT(l_list,prefix,l_node);							\
			release_func(l_node);												\
			l_node = NULL;														\
		}																		\
	}while(0)


MA_CPP( } )

#endif //
