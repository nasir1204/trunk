#ifndef MA_CSV_PARSER_INCLUDED
#define MA_CSV_PARSER_INCLUDED

#include "ma/ma_common.h"

#include <stddef.h>

/* sample input: A=B, AB="DEF" ABC = Home, ABCD = "My address is: ""929 109th Ave SE, Bellevue, WA, 98004, USA"""  */

/* Ok: A="" (Ok, value is empty string). A="""" (Ok, value is "), A=""" (Error, unbalanced) */

MA_BEGIN_C_SCOPE

enum MA_CSV_PARSER_STATE {

    MA_CSV_PARSER_STATE_ERROR,

    MA_CSV_PARSER_STATE_START,

    MA_CSV_PARSER_STATE_PARSING_KEY,

    MA_CSV_PARSER_STATE_AWAITING_ASSIGN, /* ws after key */

    MA_CSV_PARSER_STATE_AWAITING_VALUE,
    
    MA_CSV_PARSER_STATE_PARSING_VALUE,
    
    MA_CSV_PARSER_STATE_PARSING_ESCAPED_VALUE,
    
    MA_CSV_PARSER_STATE_AWAITING_ESCAPED_CHAR,
    
    MA_CSV_PARSER_STATE_AWAITING_SEPARATOR,

    MA_CSV_PARSER_STATE_AWAITING_KEY,
};

typedef struct ma_csv_parser_s {
    enum MA_CSV_PARSER_STATE state;

    wchar_t const *start_capture;

} ma_csv_parser_t;

void MA_STATIC_INLINE ma_csv_parser_init(ma_csv_parser_t *csv_parser) {
    csv_parser->state = MA_CSV_PARSER_STATE_START;
}

ma_error_t MA_UTILS_API ma_csv_parser_parse(ma_csv_parser_t *csv_parser, wchar_t const *input, size_t max_chars);

MA_END_C_SCOPE

#endif /* MA_CSV_PARSER_INCLUDED */
