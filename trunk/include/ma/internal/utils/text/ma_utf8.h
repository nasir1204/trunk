#ifndef MA_UTF8_H_INCLUDED
#define MA_UTF8_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"

MA_CPP(extern "C" {)

/* History/Reasons of the below logic
    1. wcstombs/mbstowcs family will work only with the current locale set and will fail if an invalid characters other than locale.
    2. We can use wctomb(iteration will be needed ) family and whenever any invalid characters encounters , 
       can change it to some default but what's the use of it.
Best Bet -
    1. Use WideCharToMultiByte  on windows
    2. Use iconv on *nix.
    3. if no iconv(how to determine iconv is there or not TODO) . then use wcstombs family but that too reentrant versions on unix.
*/


/*! 
 * converts as wide char to UTF-8 
 * returns the number of characters that been converted and been copied into buffer 
 * if enough room was available , else -1.
 * 
 * can also be used to calculate how many bytes are required to store the conversion  by passing buffer=NULL,
 * @param dest destination buffer to recieve the converted string , if null it will return size of buffer required.
 * @param dest_length length of the buffer or max to use
 * @param src wide char string to be converted.
 * @param src_length length of source string , if passed zero process the whole source string or exactly the number of characters

 *NOTE: Size returned by the API will calculate the  NULL termination too 
 */

MA_UTILS_API int ma_wide_char_to_utf8(char *dest , size_t dest_length, const wchar_t *src , size_t src_length);


/*! 
 * converts as UTF-8 to wide char 
 * returns the number of characters that been converted and been copied into buffer 
 * if enough room was available , else -1.
 * 
 * can also be used to calculate how many bytes are required to store the conversion  by passing buffer=NULL,
 * @param dest destination buffer to recieve the converted string , if null it will return size of buffer required.
 * @param dest_length length of the buffer or max to use
 * @param src char string to be converted.
 * @param src_length length of source string ,if passed zero process the whole source string or exactly the number of characters

 *NOTE: Size returned by the API will calculate the  NULL termination too 
 */

MA_UTILS_API int ma_utf8_to_wide_char(wchar_t *dest , size_t dest_length, const char *src , size_t src_length);

/*! 
 * converts as wide char to utf-8
 * if enough room was available , else MA_ERROR_CONVERSION_FAILED.
 *  
 * @param wstr  , source wide string to be converted
 * @param buffer , temp_buffer already initialized(ma_temp_buffer_init) keeps the buffer bytes in utf8

 *NOTE: User is responsible for calling ma_temp_buffer_uninit
 */
MA_UTILS_API ma_error_t ma_convert_wide_char_to_utf8(const wchar_t *wstr , ma_temp_buffer_t *buffer) ;

/*!
* converts as UTF-8 to wide char  
 * if enough room was available , else MA_ERROR_CONVERSION_FAILED.
 *  
 * @param str  , source string to be converted
 * @param buffer , temp_buffer already initialized(ma_temp_buffer_init) keeps the buffer bytes in wide char format

 *NOTE: User is responsible for calling ma_temp_buffer_uninit
 */
MA_UTILS_API ma_error_t ma_convert_utf8_to_wide_char(const char *str , ma_temp_buffer_t *buffer) ;

MA_CPP(})

#endif /* MA_UTF8_H_INCLUDED*/
