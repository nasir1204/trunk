#ifndef MA_INI_PARSER_H_INCLUDED
#define MA_INI_PARSER_H_INCLUDED

#include "ma/ma_common.h"

MA_CPP(extern "C" {)

/*
ini_handler returns 0 to continue.
ini_handler returns !0 to break the parsing.
*/
typedef int (*ini_handler)(const char *section, const char *key, const char *value, void *userdata);

/*
it will call the ini_handler on every occurrence of the key/value in a section.
*/
MA_UTILS_API ma_error_t ma_ini_parser_start(char *buffer, size_t size, ini_handler handler, void *userdata);


MA_CPP(})

#endif /*MA_INI_PARSER_H_INCLUDED*/

