#ifndef MA_BYTEORDER_H_INCLUDED
#define MA_BYTEORDER_H_INCLUDED

#include "ma/ma_common.h"

MA_CPP(extern "C" {)

MA_UTILS_API ma_bool_t ma_byteorder_is_host_le();

MA_UTILS_API ma_uint16_t ma_byteorder_host_to_le_uint16( ma_uint16_t data );

MA_UTILS_API ma_uint16_t ma_byteorder_host_to_be_uint16( ma_uint16_t data );

MA_UTILS_API ma_uint32_t ma_byteorder_host_to_le_uint32( ma_uint32_t data );

MA_UTILS_API ma_uint32_t ma_byteorder_host_to_be_uint32( ma_uint32_t data );

MA_UTILS_API ma_uint64_t ma_byteorder_host_to_le_uint64( ma_uint64_t data );

MA_UTILS_API ma_uint64_t ma_byteorder_host_to_be_uint64( ma_uint64_t data );

MA_UTILS_API ma_uint16_t ma_byteorder_le_to_host_uint16( ma_uint16_t data );

MA_UTILS_API ma_uint16_t ma_byteorder_be_to_host_uint16( ma_uint16_t data );

MA_UTILS_API ma_uint32_t ma_byteorder_le_to_host_uint32( ma_uint32_t data );

MA_UTILS_API ma_uint32_t ma_byteorder_be_to_host_uint32( ma_uint32_t data );

MA_UTILS_API ma_uint64_t ma_byteorder_le_to_host_uint64( ma_uint64_t data );

MA_UTILS_API ma_uint64_t ma_byteorder_be_to_host_uint64( ma_uint64_t data );

MA_CPP(})

#endif //MA_BYTEORDER_H_INCLUDED
