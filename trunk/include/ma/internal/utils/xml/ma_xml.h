#ifndef MA_XML_H_INCLUDED
#define MA_XML_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/xml/ma_xml_node.h"
#include "ma/internal/utils/datastructures/ma_bytebuffer.h"

MA_CPP(extern "C" {)

typedef struct ma_xml_s ma_xml_t, *ma_xml_h;

/**
 * create a xml object which can be used to create/load/save xml
 * @xml pointer to hold new xml object
 * 
 */
MA_XML_API ma_error_t ma_xml_create(ma_xml_t **xml);

/**
 * releases memory for xml object
 * @xml xml object
 * 
 */
MA_XML_API ma_error_t ma_xml_release(ma_xml_t *xml);

/**
 * constructs the new xml object, use get_node to manipulate it further
 * @xml xml object
 * 
 */
MA_XML_API ma_error_t ma_xml_construct(ma_xml_t *xml);

/**
 * loads the xml from the file
 * @xml xml object
 * @path path of the xml file to be loaded
 * 
 */
MA_XML_API ma_error_t ma_xml_load_from_file(ma_xml_t *xml, const char *path);

/**
 * loads the xml from the provided buffer
 * @xml xml object
 * @buffer buffered xml which needs to loaded
 * @len length of the passed buffer
 * 
 */
MA_XML_API ma_error_t ma_xml_load_from_buffer(ma_xml_t *xml, const char *buffer, size_t len);

/**
 * saves the xml into the file
 * @xml xml object
 * @path path of the file where xml to be save
 * 
 */
MA_XML_API ma_error_t ma_xml_save_to_file(ma_xml_t *xml, const char *path);

/**
 * saves the xml into the bytebuffer
 * @xml xml object
 * @buffer bytebuffer where buffered xml will be saved
 * 
 */
MA_XML_API ma_error_t ma_xml_save_to_buffer(ma_xml_t *xml, ma_bytebuffer_t *buffer);
/**
 * saves the xml into the bytebuffer
 * @xml xml object
 * @returns the root node of the xml document
 * 
 */
MA_XML_API ma_xml_node_t *ma_xml_get_node(ma_xml_t *xml);

MA_CPP(})

#endif //MA_XML_H_INCLUDED
