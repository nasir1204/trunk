#ifndef MA_ZIP_UTILS_H_INCLUDED
#define MA_ZIP_UTILS_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_errors.h"

MA_CPP(extern "C" {)

ma_error_t ma_unzip(const char *zipfilename , const char *destdirname);

MA_CPP(})

#endif //MA_ZIP_UTILS_H_INCLUDED