#ifndef MA_UUID_H_INCLUDED
#define MA_UUID_H_INCLUDED

#include "ma/ma_common.h"

MA_CPP(extern "C" {)

/* MAC address octet length */
#define MAC_OCTETS 6

/* UUID Text Length */
#define UUID_LEN_TXT  (1 /*{*/ + (128 /*bit*/ / 4 /*nibbles*/) + 4 /*hyphens*/ + 1 /*}*/ + 1 /*last \0*/)

/* Generation of uuid  - uuid string must be passed with size UUID_LEN_TXT */
MA_UTILS_API ma_error_t ma_uuid_generate(unsigned short int use_mac_address , unsigned char mac_address[MAC_OCTETS], char uuid[UUID_LEN_TXT]);

MA_CPP(})

#endif /* MA_UUID_H_INCLUDED*/
