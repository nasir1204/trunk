#ifndef MA_FIREWALL_EXCEPTIONS_H_INCLUDED
#define MA_FIREWALL_EXCEPTIONS_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/configurator/ma_configurator.h"

MA_CPP(extern "C" {)

MA_CONFIGURATOR_API ma_error_t ma_firewall_exceptions_add_constant_rules(ma_configurator_t *configurator);

MA_CONFIGURATOR_API ma_error_t ma_firewall_exceptions_add_dynamic_rules(ma_configurator_t *configurator, const char *tcp_port, const char *udp_port);

MA_CONFIGURATOR_API ma_error_t ma_firewall_exceptions_remove_constant_rules(ma_configurator_t *configurator);

MA_CONFIGURATOR_API ma_error_t ma_firewall_exceptions_remove_dynamic_rules(ma_configurator_t *configurator);

MA_CPP(})
#endif  /* MA_FIREWALL_EXCEPTIONS_H_INCLUDED */

