#ifndef MA_DB_H_INCLUDED
#define MA_DB_H_INCLUDED

#include "ma/ma_common.h"

#define MA_DB_OPEN_READONLY					1
#define MA_DB_OPEN_READWRITE				2
#define MA_DB_OPEN_READ_WITH_REPAIR			3

MA_CPP(extern "C" {)

typedef struct  ma_db_s  ma_db_t, *ma_db_h;


MA_DATABASE_API ma_error_t ma_db_open(const char *db_name, int flags, ma_db_t **database);

MA_DATABASE_API ma_error_t ma_db_close(ma_db_t *database);

MA_DATABASE_API ma_error_t ma_db_transaction_begin(ma_db_t *database);

MA_DATABASE_API ma_error_t ma_db_transaction_cancel(ma_db_t *database);

MA_DATABASE_API ma_error_t ma_db_transaction_end(ma_db_t *database);

MA_DATABASE_API ma_error_t ma_db_provision(ma_db_t *database);




MA_CPP(})


#endif /* MA_DB_H_INCLUDED */

