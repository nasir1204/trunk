#ifndef MA_DB_RECORDSET_H_INCLUDED
#define MA_DB_RECORDSET_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_variant.h"

MA_CPP(extern "C" {)

typedef struct ma_db_recordset_s ma_db_recordset_t, *ma_db_recordset_h;

MA_DATABASE_API ma_error_t ma_db_recordset_release(ma_db_recordset_t *recordset);

MA_DATABASE_API ma_error_t ma_db_recordset_get_column_count(ma_db_recordset_t *recordset, int *count);

MA_DATABASE_API ma_error_t ma_db_recordset_get_column_name(ma_db_recordset_t *recordset, int column_index, const char **column_name);

MA_DATABASE_API ma_error_t ma_db_recordset_get_string(ma_db_recordset_t *recordset, int column_index, const char **value);

MA_DATABASE_API ma_error_t ma_db_recordset_get_int(ma_db_recordset_t *recordset, int column_index, int *value);

MA_DATABASE_API ma_error_t ma_db_recordset_get_long(ma_db_recordset_t *recordset, int column_index, ma_int64_t *value);

MA_DATABASE_API ma_error_t ma_db_recordset_get_blob(ma_db_recordset_t *recordset, int column_index, const void **value, size_t *size);

MA_DATABASE_API ma_error_t ma_db_recordset_get_variant(ma_db_recordset_t *recordset, int column_index, ma_vartype_t var_type, ma_variant_t **value);

/**
 * Moves the cursor down one row from its current position. A
 * RecoderSet cursor is initially positioned before the first row; the
 * first call to this method makes the first row the current row; the
 * second call makes the second row the current row, and so on. When
 * there are not more available rows MA_ERROR_NO_MORE_ITEMS is returned. An empty
 * RecordSet will return MA_ERROR_NO_MORE_ITEMS on the first call to ma_db_recordset_next().
 */

MA_DATABASE_API ma_error_t ma_db_recordset_next(ma_db_recordset_t *recordset);

MA_CPP(})


#endif /* MA_DB_RECORDSET_H_INCLUDED */

