#ifndef MA_DB_STATEMENT_H_INCLUDED
#define MA_DB_STATEMENT_H_INCLUDED

#include "ma/ma_common.h"
#include "ma_db.h"
#include "ma_db_recordset.h"

MA_CPP(extern "C" {)

typedef struct ma_db_statement_s ma_db_statement_t, *ma_db_statement_h;

/**
 * db statement represent a single SQL statement pre-compiled into byte code for later execution.
 * Note that string and blob parameter values are set by reference and 
 * MUST not "disappear" before either ma_db_statement_execute() or ma_db_statement_execute_query() is called.
 * Example:
 * INSERT INTO employee(name, salary) VALUES(?, ?)
 * ma_db_statement_t *db_stmt = NULL;
 * ma_db_statement_create(db, "INSERT INTO employee(name, salary) VALUES(?, ?)" &db_stmt);
 * ma_db_statement_set_string(p, 1, "Kamiya Kaoru");
 * ma_db_statement_set_int(p, 2, 100000);
 * ma_db_statement_execute(p);
 
 * Reuse
 * A db statement can be reused.
 * ma_db_statement_t *db_stmt = NULL;
 * ma_db_statement_create(db, "INSERT INTO employee(name, salary) VALUES(?, ?)" &db_stmt);
 * for (int i = 0; employees[i].name; i++) {
 *      ma_db_statement_set_string(p, 1, "Kamiya Kaoru");
 *      ma_db_statement_set_int(p, 2, 100000);
 *      ma_db_statement_execute(p)
 * }
 * 
 * Rercordset
 * Here is another example where we use a db statement to execute a query
 * which returns a recordset
 * Example
 * "SELECT id FROM employee WHERE name LIKE ?"
 * ma_db_statement_t *db_stmt p = NULL;
 * ma_db_statement_create(db, "SELECT id FROM employee WHERE name LIKE ?" , &db_stmt); 
 * ma_db_statement_execute_query(db_stmt, &recordset);
 * while (MA_ERROR_NO_MORE_ITEMS != ma_ds_recordset_next(recordset));
 * ma_ds_recordset_release
 */

MA_DATABASE_API  ma_error_t ma_db_statement_create(ma_db_t *handle, const char *stmt, ma_db_statement_t **db_stmt);

MA_DATABASE_API ma_error_t ma_db_statement_release(ma_db_statement_t *db_stmt);

MA_DATABASE_API ma_error_t ma_db_statement_set_int(ma_db_statement_t *db_stmt, int parameter_index, int value);

MA_DATABASE_API ma_error_t ma_db_statement_set_long(ma_db_statement_t *db_stmt, int parameter_index, ma_int64_t value);

MA_DATABASE_API ma_error_t ma_db_statement_set_string(ma_db_statement_t *db_stmt, int parameter_index, unsigned short is_data_transient, const char *value, size_t size);

MA_DATABASE_API ma_error_t ma_db_statement_set_blob(ma_db_statement_t *db_stmt, int parameter_index, unsigned short is_data_transient, const void *value, size_t size);

MA_DATABASE_API ma_error_t ma_db_statement_set_variant(ma_db_statement_t *db_stmt, int parameter_index, ma_variant_t *value);

MA_DATABASE_API ma_error_t ma_db_statement_execute(ma_db_statement_t *db_stmt);

MA_DATABASE_API ma_error_t ma_db_statement_execute_query(ma_db_statement_t *db_stmt, ma_db_recordset_t **recordset);

MA_CPP(})

#endif /* MA_DB_STATEMENT_H_INCLUDED */


