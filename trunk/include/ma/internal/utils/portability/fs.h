/* ex: ts=8 sw=4 sts=4 et
** -*- coding: utf-8 -*-
*/
#ifndef MA_INTERNAL_UTILS_PORTABILITY_FS_H
#define MA_INTERNAL_UTILS_PORTABILITY_FS_H

#include <stddef.h> /* for size_t */
#include "ma/ma_common.h"

/*
   Return the directory (dirname) component of the given path
   in the buffer described by buf_out and size_out.

   All strings are expected to be UTF-8 encoded.

   Returns 0 if all is well, -errno otherwise.
*/
MA_UTILS_API int ma_dirname(const char* path_in,
                            char* const buf_out,
                            const size_t size_out);

/*
   create the specified directory path, creating parent directories
   as needed.

   All strings are expected to be UTF-8 encoded.

   return 0 for success, -errno for failure.
*/
MA_UTILS_API int ma_mkpath(const char* path_in);

#endif /* MA_INTERNAL_UTILS_PORTABILITY_FS_H */
