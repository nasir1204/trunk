/* ex: ts=8 sw=4 sts=4 et
** -*- coding: utf-8 -*-
*/
#ifndef MA_INTERNAL_UTILS_PORTABILITY_POSIX_STDARG_H
#define MA_INTERNAL_UTILS_PORTABILITY_POSIX_STDARG_H
#include <stdarg.h>
#endif /* MA_INTERNAL_UTILS_PORTABILITY_POSIX_STDARG_H */
