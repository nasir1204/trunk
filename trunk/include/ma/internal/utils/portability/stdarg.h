/* ex: ts=8 sw=4 sts=4 et
** -*- coding: utf-8 -*-
*/
#ifndef MA_INTERNAL_UTILS_PORTABILITY_STDARG_H
#define MA_INTERNAL_UTILS_PORTABILITY_STDARG_H

#ifdef _WIN32
#include "ma/internal/utils/portability/win32/stdarg.h"
#else
#include "ma/internal/utils/portability/posix/stdarg.h"
#endif

#endif /* MA_INTERNAL_UTILS_PORTABILITY_STDARG_H */
