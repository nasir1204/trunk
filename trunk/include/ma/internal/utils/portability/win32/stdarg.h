/* ex: ts=8 sw=4 sts=4 et
** -*- coding: utf-8 -*-
*/
#ifndef MA_INTERNAL_UTILS_PORTABILITY_WIN32_STDARG_H
#define MA_INTERNAL_UTILS_PORTABILITY_WIN32_STDARG_H
#include <stdarg.h>
/*
   va_copy is available beginning with Visual Studio 12.0.  If we are using
   an older compiler, simply copy the pointer as that's safe in win32.
*/
#ifndef va_copy
#define va_copy(dest, src) ((dest) = (src))
#endif /* va_copy? */
#endif /* MA_INTERNAL_UTILS_PORTABILITY_WIN32_STDARG_H */
