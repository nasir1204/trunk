#ifndef MA_PROXY_CONFIG_INTERNAL_H_INCLUDED
#define MA_PROXY_CONFIG_INTERNAL_H_INCLUDED

#include "ma/ma_common.h"

MA_CPP(extern "C" {)

struct ma_proxy_config_s ;
struct ma_context_s ;

MA_PROXY_API ma_error_t  ma_proxy_get_config(struct ma_context_s *context, struct ma_proxy_config_s **config) ;

MA_CPP(})

#endif /* MA_PROXY_CONFIG_INTERNAL_H_INCLUDED */