#include "ma/ma_common.h"
#include "ma/internal/utils/database/ma_db.h"
#include "ma/internal/utils/proxy/ma_proxy_internal.h"
#include "ma/internal/utils/database/ma_db_recordset.h"

/*
1. SERVER TEXT, 
2. PORT INTEGER, 
3. PROTOCOL_TYPE INTEGER NOT NULL, 
4. FLAG INTEGER NOT NULL, 
5. USE_AUTH INTEGER NOT NULL, 
6. USER_NAME TEXT, 
7. PASSWORD TEXT, 
*/

#define MA_PROXY_TABLE_FIELD_BASE					  0
#define MA_PROXY_TABLE_FIELD_NAME					  MA_PROXY_TABLE_FIELD_BASE + 1
#define MA_PROXY_TABLE_FIELD_PORT					  MA_PROXY_TABLE_FIELD_BASE + 2
#define MA_PROXY_TABLE_FIELD_PROTOCOL_TYPE			  MA_PROXY_TABLE_FIELD_BASE + 3
#define MA_PROXY_TABLE_FIELD_FLAG					  MA_PROXY_TABLE_FIELD_BASE + 4
#define MA_PROXY_TABLE_FIELD_USE_AUTH				  MA_PROXY_TABLE_FIELD_BASE + 5
#define MA_PROXY_TABLE_FIELD_USER_NAME				  MA_PROXY_TABLE_FIELD_BASE + 6
#define MA_PROXY_TABLE_FIELD_PASSWORD				  MA_PROXY_TABLE_FIELD_BASE + 7

/* Proxy DB API's*/

MA_PROXY_API ma_error_t ma_proxy_db_create_schema(ma_db_t *db_handle) ;

MA_PROXY_API ma_error_t ma_proxy_db_add_proxy(ma_db_t *db_handle, ma_proxy_t *proxy) ;

MA_PROXY_API ma_error_t ma_proxy_db_remove_all_proxies(ma_db_t *db_handle) ;

MA_PROXY_API ma_error_t ma_proxy_db_remove_all_global_proxies(ma_db_t *db_handle) ;

MA_PROXY_API ma_error_t ma_proxy_db_remove_proxy(ma_db_t *db_handle, ma_proxy_t *proxy) ;

MA_PROXY_API ma_error_t ma_proxy_db_get_all_proxies(ma_db_t *db_handle, int flag, ma_proxy_list_t **proxy_list) ;

MA_PROXY_API ma_error_t ma_proxy_db_add_proxy_config(ma_db_t *db_handle, ma_proxy_config_t *self) ;

MA_PROXY_API ma_error_t ma_proxy_db_get_proxy_config(ma_db_t *db_handle, ma_proxy_config_t **proxy_config) ;

MA_PROXY_API ma_error_t ma_proxy_db_remove_proxy_config(ma_db_t *db_handle) ;
