#ifndef MA_PROXY_INTERNAL_H_INCLUDED
#define MA_PROXY_INTERNAL_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_variant.h"
#include "ma/proxy/ma_proxy.h"
#include "ma/proxy/ma_proxy_config.h"
#include "ma/internal/utils/threading/ma_atomic.h"
#include "ma/ma_log.h"

MA_CPP(extern "C" {)

struct ma_proxy_s{
	char						*server;
	ma_int32_t					server_port;
	ma_bool_t					is_auth_rqued;
	char						*user_name;
	char						*user_passwd;
	ma_proxy_protocol_type_t	type;
	ma_int32_t					flag;
	ma_uint32_t					nic_scope_id;
};

struct ma_system_proxy_auth_info_s{
	char						*http_user_name;
	char						*http_user_passwd;
	char						*ftp_user_name;
	char						*ftp_user_passwd;
	ma_bool_t					ftp_auth_rqrd ;
    ma_bool_t					http_auth_rqrd ;
};

struct ma_proxy_list_s{
	ma_array_t					*list;
	ma_int32_t					last_used_proxy_index;
	ma_atomic_counter_t			ref_count;
};

struct ma_proxy_config_s{
	ma_bool_t					    bypass_local_address;
	char						    *exclusion_list;	/* semi colon separated url list */
	ma_proxy_usage_type_t		    proxy_usage_type;
	ma_proxy_list_t				    *proxy_list;	
	ma_system_proxy_auth_info_t	    *system_proxy_auth ;
    ma_bool_t                       allow_user_config ;
	ma_atomic_counter_t				ref_count	;
};

ma_error_t   ma_proxy_as_variant(ma_proxy_t *proxy, ma_variant_t **proxy_variant);
ma_error_t   ma_proxy_from_variant(ma_variant_t *proxy_variant, ma_proxy_t **proxy);

MA_PROXY_API ma_error_t   ma_proxy_config_as_variant(ma_proxy_config_t *proxy_config, ma_variant_t **proxy_config_variant);
MA_PROXY_API ma_error_t   ma_proxy_config_from_variant(ma_variant_t *proxy_config_variant, ma_proxy_config_t **proxy_config) ;

MA_PROXY_API ma_error_t   ma_proxy_config_copy(ma_proxy_config_t *proxy_config_from, ma_proxy_config_t **proxy_config_to);

MA_PROXY_API ma_error_t   ma_proxy_config_detect_system_proxy(ma_proxy_config_t *proxy_config_from, ma_logger_t *logger, const char *url, ma_proxy_list_t **proxy_list);

MA_PROXY_API ma_error_t   ma_proxy_list_to_variant(ma_proxy_list_t *proxy_list, ma_variant_t **variant);
MA_PROXY_API ma_error_t   ma_proxy_list_from_variant(ma_variant_t *variant, ma_proxy_list_t **proxy_list);

MA_CPP(})
#endif  /* MA_PROXY_INTERNAL_H_INCLUDED */

