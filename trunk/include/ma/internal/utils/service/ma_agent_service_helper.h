#ifndef MA_AGENT_SERVICE_HELPER_H_INCLUDED
#define MA_AGENT_SERVICE_HELPER_H_INCLUDED

#include "ma/ma_common.h"

MA_CPP(extern "C" {)

ma_error_t ma_agent_service_start(const char *service_name, ma_bool_t wait_till_starts);

ma_error_t ma_agent_service_stop(const char *service_name, ma_bool_t wait_till_stops);

ma_error_t ma_agent_service_is_running(const char *service_name, ma_bool_t *is_running);

MA_CPP(})

#endif /* MA_AGENT_SERVICE_HELPER_H_INCLUDED */
