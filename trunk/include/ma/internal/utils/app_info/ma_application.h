#ifndef MA_APPLICATION_INFO_H_INCLUDED
#define MA_APPLICATION_INFO_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_service_flag.h"

MA_CPP( extern "C" { )

typedef enum ma_application_integration_method_e {
	MA_INTEGRATION_METHOD_UNKNOWN = 0,
	MA_INTEGRATION_METHOD_LEGACY_PLUGIN_BASED = 1,
	MA_INTEGRATION_METHOD_LEGACY_LPC_BASED = 2,
	MA_INTEGRATION_METHOD_MESSAGE_BASED = 3,
}ma_application_integration_method_t;

typedef enum ma_application_registered_services_e {
    MA_REGISTERED_NONE = 0,
    MA_REGISTERED_PROPERTY_SERVICE = 1,
    MA_REGISTERED_POLICY_SERVICE = 2,
    MA_REGISTERED_EVENT_SERVICE = 4,
	MA_REGISTERED_SCHEDULER_SERVICE = 8,
    MA_REGISTERED_DATACHANNEL_SERVICE = 16,
    MA_REGISTERED_UPDATER_SERVICE = 32,
    MA_REGISTERED_ALL = 63
}ma_application_registered_services_t;



typedef struct ma_application_s ma_application_t, *ma_application_h;

ma_application_integration_method_t ma_application_get_integration_method(const ma_application_t *application);

MA_CONFIGURATOR_API ma_uint64_t ma_application_get_required_services_flag(const ma_application_t *application);

MA_CONFIGURATOR_API const char *ma_application_get_software_id(const ma_application_t *application);

MA_CPP(})

#endif	/* MA_APPLICATION_INFO_H_INCLUDED */

