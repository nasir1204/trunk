#ifndef MA_REGISTRY_STORE_H_INCLUDED
#define MA_REGISTRY_STORE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/app_info/ma_application.h"

MA_CPP( extern "C" { )

typedef struct ma_registry_store_s ma_registry_store_t, *ma_registry_store_h;

#define MA_REGISTRY_STORE_FOR_EACH_APPLICATION(registry_store, application) \
				application = NULL;	\
				while( NULL != (application = ma_registry_store_get_next_application(registry_store, application) ))


MA_CONFIGURATOR_API ma_error_t ma_registry_store_create(ma_registry_store_t **registry_store);

MA_CONFIGURATOR_API ma_error_t ma_registry_store_release(ma_registry_store_t *registry_store);

MA_CONFIGURATOR_API ma_error_t ma_registry_store_scan(ma_registry_store_t *registry_store);

MA_CONFIGURATOR_API const char *ma_registry_store_get_agent_install_path(ma_registry_store_t *registry_store);

MA_CONFIGURATOR_API const char *ma_registry_store_get_agent_data_path(ma_registry_store_t *registry_store);

/*
 * @param registry_store		-	the pointer to an ma_registry_store_t object
 * @param software_id		-	The id you are interested to obtain
 * @param bitness_affinity	-	specify 0 to get the first available hit on software id,
 *								specify 1 to get the software id that has x86 affinity(if it was scanned)
 *								specify 2 to get the software id that has x64 affinity(if it was scanned)
 */
MA_CONFIGURATOR_API const ma_application_t *ma_registry_store_get_application(ma_registry_store_t *registry_store, const char *software_id);

MA_CONFIGURATOR_API const ma_application_t *ma_registry_store_get_next_application(ma_registry_store_t *registry_store, const ma_application_t *application);

MA_CPP(})

#endif /* MA_REGISTRY_STORE_H_INCLUDED */

