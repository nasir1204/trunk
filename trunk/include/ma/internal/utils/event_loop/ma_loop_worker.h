#ifndef MA_LOOP_WORKER_H_INCLUDED
#define MA_LOOP_WORKER_H_INCLUDED

#include "ma/ma_msgbus.h"

#include "ma/internal/utils/event_loop/ma_event_loop.h"

/*
 * provides functionality to send execution jobs from any thread to be executed on i/o thread
 * 
 */

MA_CPP(extern "C" {)

typedef struct ma_loop_worker_s ma_loop_worker_t, *ma_loop_worker_h;

/*! 
 * Creates the loop_worker object
 * Must be called only from i/o thread
 */
ma_error_t MA_MSGBUS_API ma_loop_worker_create(ma_event_loop_t *loop, ma_loop_worker_t **loop_worker);

/*!
 * Starts listening and processing worker request
 * Must be called from i/o thread
 */
ma_error_t MA_MSGBUS_API ma_loop_worker_start(ma_loop_worker_t *loop_worker);

/*!
 * stops receiving, unregisters from the event loop
 * Must be called from i/o thread
 */ 
ma_error_t MA_MSGBUS_API ma_loop_worker_stop(ma_loop_worker_t *loop_worker);

/*!
 * releases memory and external references
 */
ma_error_t MA_MSGBUS_API ma_loop_worker_release(ma_loop_worker_t *loop_worker);

/*!
 * sets logger
 */
ma_error_t MA_MSGBUS_API ma_loop_worker_set_logger(ma_loop_worker_t *loop_worker, ma_logger_t *logger);


/*!
 * function to be called from i/o thread
 * you may also want to free/release/dec_ref or whatever any memory/resources allocated for the work request itself
 */
typedef void (*ma_work_function)(void *work_data);

/*!
 * Submits some work to be executed on the i/o thread
 * safe to call from any thread as long as the loop is active and not blocked
 * @param loop_worker Handle to the loop_worker
 * @param work_fun Function to be executed on the i/o thread
 * @param work_data Piece of data that will be passed back to the work_function. This is opaque to message bus framework
 * @param wait Set to MA_TRUE to tell ma_loop_worker_submit_work should wait till the work function has completed. When set to 0 (MA_FALSE), this function returns immediately, independent from work_function
 * @return MA_OK or an error code. In case of error the callback will not be invoked.
 */
ma_error_t MA_MSGBUS_API ma_loop_worker_submit_work(ma_loop_worker_t *loop_worker, ma_work_function work_fun, void *work_data, ma_bool_t wait);

MA_CPP(})

#endif /* MA_LOOP_WORKER_H_INCLUDED */
