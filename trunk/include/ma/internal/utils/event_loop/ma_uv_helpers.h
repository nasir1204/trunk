#ifndef MA_UV_HELPERS_H_INCLUDED
#define MA_UV_HELPERS_H_INCLUDED

#define MA_ERROR_FROM_UV_LAST_ERROR(loop) ((ma_error_t) uv_last_error(loop).code)

#define UV_ERROR_STR_FROM_LAST_UV_ERROR(loop) (uv_strerror(uv_last_error(loop)))

#endif

