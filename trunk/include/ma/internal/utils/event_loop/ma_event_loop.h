/*
 *
 */
#ifndef MA_EVENT_LOOP_H_INCLUDED
#define MA_EVENT_LOOP_H_INCLUDED


#include "ma/ma_common.h"

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable: 4201) /* nonstandard extension used : nameless struct/union */
#include <uv.h>
#pragma warning(pop)
#else
#include <uv.h>
#endif 

MA_CPP(extern "C" {)

typedef struct ma_event_loop_s ma_event_loop_t, *ma_event_loop_h;

ma_error_t ma_event_loop_create(ma_event_loop_t **loop);

ma_error_t ma_event_loop_release(ma_event_loop_t *loop);

ma_error_t ma_event_loop_start(ma_event_loop_t *loop);

/*!
 * starts event loop processing until explicitly stopped. This is a blocking call
 */
ma_error_t ma_event_loop_run(ma_event_loop_t *loop);

/*!
 * causes the event loop to stop eventually. This is a very asynchronous call!
 */
ma_error_t ma_event_loop_stop(ma_event_loop_t *loop);

/*! 
 * returns the inner uv_loop
 */
ma_error_t ma_event_loop_get_loop_impl(ma_event_loop_t *loop, struct uv_loop_s **uv_loop);

MA_STATIC_INLINE struct uv_loop_s *ma_event_loop_get_uv_loop(ma_event_loop_t *loop) {
    return (struct uv_loop_s *)(*(void**)loop);
}

/*!
 * signature of callback function to be registered with ma_event_loop_register_stop_handler()
 */
typedef void (*ma_event_loop_stop_handler_cb)(ma_event_loop_t *loop, int status, void *cb_data);

/*!
 * registers a stop handler. The provided callback is implemented by the client (e.g. you) and will be invoked on the message loop thread
 * Your implementation should remove all uv watchers to allow the loop to terminate.
 */
ma_error_t ma_event_loop_register_stop_handler(ma_event_loop_t *loop, ma_event_loop_stop_handler_cb cb, void *cb_data);

/*! 
 * returns the thread id of the event processing thread
 */
ma_error_t ma_event_loop_get_thread_id(const ma_event_loop_t *loop, unsigned int *tid);

/*!
 * Tests if the calling thread is the same thread the loop is running on
 */

ma_bool_t ma_event_loop_is_loop_thread(const ma_event_loop_t *loop);

MA_CPP(})

#endif /* MA_EVENT_LOOP_H_INCLUDED */
