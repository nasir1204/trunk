#ifndef MA_LOCATION_INFO_H_INCLUDED
#define MA_LOCATION_INFO_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_variant.h"
#include "ma/internal/utils/profiler/ma_profile_info.h"
#include "ma/scheduler/ma_task.h"


MA_CPP(extern "C" {)

typedef struct ma_location_info_s ma_location_info_t, *ma_location_info_h;

typedef enum ma_location_info_type_e {
   MA_LOCATION_INFO_TYPE_ORDER = 1,
   MA_LOCATION_INFO_TYPE_USER,
   MA_LOCATION_INFO_TYPE_APP,
   MA_LOCATION_INFO_TYPE_NONE,
}ma_location_info_type_t;


/* ctor */
MA_LOCATION_API ma_error_t ma_location_info_create(ma_location_info_t **info);

/* dtor */
MA_LOCATION_API ma_error_t ma_location_info_release(ma_location_info_t *info);

/* setter */
MA_LOCATION_API ma_error_t ma_location_info_set_email_id(ma_location_info_t *info, const char *id);
MA_LOCATION_API ma_error_t ma_location_info_set_pincode(ma_location_info_t *info, const char *id);
MA_LOCATION_API ma_error_t ma_location_info_set_date(ma_location_info_t *info, ma_task_time_t date);
MA_LOCATION_API ma_error_t ma_location_info_set_type(ma_location_info_t *info, ma_location_info_type_t type);
MA_LOCATION_API ma_error_t ma_location_info_set_entry_date(ma_location_info_t *info, ma_task_time_t date);
MA_LOCATION_API ma_error_t ma_location_info_set_entry(ma_location_info_t *info, const char *entry);
MA_LOCATION_API ma_error_t ma_location_info_set_details(ma_location_info_t *info, const char *details);
MA_LOCATION_API ma_error_t ma_location_info_set_contact_list(ma_location_info_t *info, ma_table_t *table);
MA_LOCATION_API ma_error_t ma_location_info_add_contact(ma_location_info_t *info, const char *pincode, const char *contact);
MA_LOCATION_API ma_error_t ma_location_info_remove_contact(ma_location_info_t *info, const char *pincode, const char *contact);
MA_LOCATION_API ma_error_t ma_location_info_get_contact(ma_location_info_t *info, const char *pincode, ma_variant_t **v);

/* getters */
MA_LOCATION_API ma_error_t ma_location_info_get_details(ma_location_info_t *info, char *details);
MA_LOCATION_API ma_error_t ma_location_info_get_entry(ma_location_info_t *info, char *entry);
MA_LOCATION_API ma_error_t ma_location_info_get_entry_date(ma_location_info_t *info, ma_task_time_t *date);
MA_LOCATION_API ma_error_t ma_location_info_get_email_id(ma_location_info_t *info, char *id);
MA_LOCATION_API ma_error_t ma_location_info_get_pincode(ma_location_info_t *info, char *id);
MA_LOCATION_API ma_error_t ma_location_info_get_date(ma_location_info_t *info, ma_task_time_t *date);
MA_LOCATION_API ma_error_t ma_location_info_get_type(ma_location_info_t *info, ma_location_info_type_t *type);
MA_LOCATION_API ma_error_t ma_location_info_get_contact_list(ma_location_info_t *info, ma_table_t **table);


/* convert location info to variant  and vice versa */
MA_LOCATION_API ma_error_t ma_location_info_convert_to_variant(ma_location_info_t *info, ma_variant_t **variant);
MA_LOCATION_API ma_error_t ma_location_info_convert_from_variant(ma_variant_t *variant, ma_location_info_t **info);


MA_CPP(})

#endif /* MA_LOCATION_INFO_H_INCLUDED */

