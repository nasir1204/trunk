#ifndef MA_LOCATION_H_INCLUDED
#define MA_LOCATION_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/location/ma_location_info.h"
#include "ma/ma_datastore.h"
#include "ma/datastore/ma_ds_database.h"
#include "ma/ma_log.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_variant.h"


MA_CPP(extern "C" {)

typedef struct ma_location_s ma_location_t, *ma_location_h;


MA_LOCATION_API ma_error_t ma_location_create(ma_msgbus_t *msgbus, ma_ds_t *datastore, const char *base_path, ma_location_t **location);

MA_LOCATION_API ma_error_t ma_location_start(ma_location_t *location);

MA_LOCATION_API ma_error_t ma_location_stop(ma_location_t *location);

MA_LOCATION_API ma_error_t ma_location_add(ma_location_t *location, ma_location_info_t *info);

MA_LOCATION_API ma_error_t ma_location_add_variant(ma_location_t *location, const char *location_id, ma_variant_t *var) ;

MA_LOCATION_API ma_error_t ma_location_enumerate_order_until_date(ma_location_t *location, const char *from, const char *to, ma_variant_t **var);

MA_LOCATION_API ma_error_t ma_location_delete_crn(ma_location_t *location, const char *location_id, const char *crn);

MA_LOCATION_API ma_error_t ma_location_delete_all(ma_location_t *location);

MA_LOCATION_API ma_error_t ma_location_delete(ma_location_t *location, const char *location_id);

MA_LOCATION_API ma_error_t ma_location_get(ma_location_t *location, const char *location_id, ma_location_info_t **info);

MA_LOCATION_API ma_error_t ma_location_get_all(ma_location_t *location, const char *location_id, ma_variant_t **var);

MA_LOCATION_API ma_error_t ma_location_set_logger(ma_logger_t *logger);

MA_LOCATION_API ma_error_t ma_location_get_msgbus(ma_location_t *location, ma_msgbus_t **msgbus);

MA_LOCATION_API ma_error_t ma_location_release(ma_location_t *location);


MA_CPP(})

#endif /* MA_LOCATION_H_INCLUDED */

