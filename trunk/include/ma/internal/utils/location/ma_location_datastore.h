#ifndef MA_LOCATION_DATASTORE_H_INCLUDED
#define MA_LOCATION_DATASTORE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/location/ma_location.h"
#include "ma/internal/utils/location/ma_location_info.h"
#include "ma/ma_datastore.h"
#include "ma/datastore/ma_ds_database.h"
#include "ma/ma_variant.h"

MA_CPP(extern "C" {)
MA_LOCATION_API ma_error_t ma_location_datastore_read(ma_location_t *location, ma_ds_t *datastore, const char *ds_path);

MA_LOCATION_API ma_error_t ma_location_datastore_get(ma_location_t *location, ma_ds_t *datastore, const char *ds_path, const char *location_id, ma_location_info_t **info);

MA_LOCATION_API ma_error_t ma_location_datastore_get_all(ma_location_t *location, ma_ds_t *datastore, const char *ds_path, const char *location_id, ma_variant_t **var);

MA_LOCATION_API ma_error_t ma_location_datastore_write(ma_location_t *location, ma_location_info_t *info, ma_ds_t *datastore, const char *ds_path);

MA_LOCATION_API ma_error_t ma_location_datastore_remove_location_crn(ma_location_t *location, const char *location_id, const char *crn, ma_ds_t *datastore, const char *ds_path);

MA_LOCATION_API ma_error_t ma_location_datastore_enumerate_order_until_date(ma_location_t *location, ma_ds_t *datastore, const char *ds_path, const char *from, const char *to, ma_variant_t **ret_var);

MA_LOCATION_API ma_error_t ma_location_datastore_write_variant(ma_location_t *location, const char *location_id, ma_ds_t *datastore, const char *ds_path, ma_variant_t *var);

MA_LOCATION_API ma_error_t ma_location_datastore_remove_location(const char *location, ma_ds_t *datastore, const char *ds_path);

MA_LOCATION_API ma_error_t ma_location_datastore_clear(ma_ds_t *datastore, const char *ds_path);

MA_CPP(})


#endif /* MA_LOCATION_DATASTORE_H_INCLUDED */

