#ifndef MA_SEASON_INFO_H_INCLUDED
#define MA_SEASON_INFO_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_variant.h"
#include "ma/scheduler/ma_triggers.h"
#include "ma/internal/utils/json/ma_json_utils.h"

MA_CPP(extern "C" {)

typedef struct ma_season_info_s ma_season_info_t, *ma_season_info_h;

/* season info ctor */
MA_SEASON_API ma_error_t ma_season_info_create(ma_season_info_t **info);

/* season info dtor */
MA_SEASON_API ma_error_t ma_season_info_release(ma_season_info_t *info);

/* setter */
MA_SEASON_API ma_error_t ma_season_info_set_email_id(ma_season_info_t *info, const char *id);
MA_SEASON_API ma_error_t ma_season_info_set_contact(ma_season_info_t *info, const char *id);
MA_SEASON_API ma_error_t ma_season_info_set_domain(ma_season_info_t *info, const char *domain);
MA_SEASON_API ma_error_t ma_season_info_set_attribution(ma_season_info_t *info, const char *attri); 
MA_SEASON_API ma_error_t ma_season_info_set_description(ma_season_info_t *info, const char *descr);
MA_SEASON_API ma_error_t ma_season_info_set_about(ma_season_info_t *info, const char *about);
MA_SEASON_API ma_error_t ma_season_info_set_location(ma_season_info_t *info, const char *loc);
MA_SEASON_API ma_error_t ma_season_info_set_full_name(ma_season_info_t *info, const char *name);
MA_SEASON_API ma_error_t ma_season_info_set_follower_count(ma_season_info_t *info, int count);
MA_SEASON_API ma_error_t ma_season_info_set_image_small_url(ma_season_info_t *info, const char *url);
MA_SEASON_API ma_error_t ma_season_info_set_pin_count(ma_season_info_t *info, int count);
MA_SEASON_API ma_error_t ma_season_info_set_pin_id(ma_season_info_t *info, const char *id);
MA_SEASON_API ma_error_t ma_season_info_set_profile_url(ma_season_info_t *info, const char *url);
MA_SEASON_API ma_error_t ma_season_info_set_repin_count(ma_season_info_t *info, int count);
MA_SEASON_API ma_error_t ma_season_info_set_dominant_color(ma_season_info_t *info, const char *color);
MA_SEASON_API ma_error_t ma_season_info_set_like_count(ma_season_info_t *info, int count);
MA_SEASON_API ma_error_t ma_season_info_set_link(ma_season_info_t *info, const char *link);
MA_SEASON_API ma_error_t ma_season_info_set_url(ma_season_info_t *info, const char *url);
MA_SEASON_API ma_error_t ma_season_info_set_width(ma_season_info_t *info, int width);
MA_SEASON_API ma_error_t ma_season_info_set_height(ma_season_info_t *info, int height);
MA_SEASON_API ma_error_t ma_season_info_set_embed(ma_season_info_t *info, const char *embed);
MA_SEASON_API ma_error_t ma_season_info_set_is_video(ma_season_info_t *info, ma_bool_t is_video);
MA_SEASON_API ma_error_t ma_season_info_set_id(ma_season_info_t *info, const char *id);
MA_SEASON_API ma_error_t ma_season_info_set_topic(ma_season_info_t *info, const char *topic);
MA_SEASON_API ma_error_t ma_season_info_set_board(ma_season_info_t *info, const char *board);
MA_SEASON_API ma_error_t ma_season_info_set_saved_by(ma_season_info_t *info, const char *saved_by);
MA_SEASON_API ma_error_t ma_season_info_set_board_image_url(ma_season_info_t *info, const char *image_url);

MA_SEASON_API ma_error_t ma_season_info_add(ma_season_info_t *info);

/* getter */
MA_SEASON_API ma_error_t ma_season_info_get_email_id(ma_season_info_t *info, const char **id);
MA_SEASON_API ma_error_t ma_season_info_get_contact(ma_season_info_t *info, const char **id);
MA_SEASON_API ma_error_t ma_season_info_get_domain(ma_season_info_t *info, const char **domain);
MA_SEASON_API ma_error_t ma_season_info_get_attribution(ma_season_info_t *info, const char **attri); 
MA_SEASON_API ma_error_t ma_season_info_get_description(ma_season_info_t *info, const char **descr);
MA_SEASON_API ma_error_t ma_season_info_get_about(ma_season_info_t *info, const char **about);
MA_SEASON_API ma_error_t ma_season_info_get_location(ma_season_info_t *info, const char **loc);
MA_SEASON_API ma_error_t ma_season_info_get_full_name(ma_season_info_t *info, const char **name);
MA_SEASON_API ma_error_t ma_season_info_get_follower_count(ma_season_info_t *info, int *count);
MA_SEASON_API ma_error_t ma_season_info_get_image_small_url(ma_season_info_t *info, const char **url);
MA_SEASON_API ma_error_t ma_season_info_get_pin_count(ma_season_info_t *info, int *count);
MA_SEASON_API ma_error_t ma_season_info_get_pin_id(ma_season_info_t *info, const char **id);
MA_SEASON_API ma_error_t ma_season_info_get_profile_url(ma_season_info_t *info, const char **url);
MA_SEASON_API ma_error_t ma_season_info_get_repin_count(ma_season_info_t *info, int *count);
MA_SEASON_API ma_error_t ma_season_info_get_dominant_color(ma_season_info_t *info, const char **color);
MA_SEASON_API ma_error_t ma_season_info_get_like_count(ma_season_info_t *info, int *count);
MA_SEASON_API ma_error_t ma_season_info_get_link(ma_season_info_t *info, const char **link);
MA_SEASON_API ma_error_t ma_season_info_get_url(ma_season_info_t *info, const char **url);
MA_SEASON_API ma_error_t ma_season_info_get_width(ma_season_info_t *info, int *width);
MA_SEASON_API ma_error_t ma_season_info_get_height(ma_season_info_t *info, int *height);
MA_SEASON_API ma_error_t ma_season_info_get_embed(ma_season_info_t *info, const char **embed);
MA_SEASON_API ma_error_t ma_season_info_get_is_video(ma_season_info_t *info, ma_bool_t *is_video);
MA_SEASON_API ma_error_t ma_season_info_get_id(ma_season_info_t *info, const char **id);
MA_SEASON_API ma_error_t ma_season_info_get_json(ma_season_info_t *info, ma_json_t **json);
MA_SEASON_API ma_error_t ma_season_info_get_topic(ma_season_info_t *info, const char **topic);
MA_SEASON_API ma_error_t ma_season_info_get_board(ma_season_info_t *info, const char **board);
MA_SEASON_API ma_error_t ma_season_info_get_saved_by(ma_season_info_t *info, const char **saved_by);
MA_SEASON_API ma_error_t ma_season_info_get_board_image_url(ma_season_info_t *info, const char **image_url);

/* convert season info to variant  and vice versa */
MA_SEASON_API ma_error_t ma_season_info_convert_to_variant(ma_season_info_t *info, ma_variant_t **variant);
MA_SEASON_API ma_error_t ma_season_info_convert_from_variant(ma_variant_t *variant, ma_season_info_t **info);

/* convert season info to json and vice versa user is responsible to release json */
MA_SEASON_API ma_error_t ma_season_info_convert_to_json(ma_season_info_t *info, ma_json_t **json);
MA_SEASON_API ma_error_t ma_season_info_convert_from_json(const char *json_str, ma_season_info_t **info);


MA_CPP(})

#endif /* MA_SEASON_INFO_H_INCLUDED */

