#ifndef MA_SEASON_H_INCLUDED
#define MA_SEASON_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/season/ma_season_info.h"
#include "ma/ma_datastore.h"
#include "ma/datastore/ma_ds_database.h"
#include "ma/ma_log.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_variant.h"


MA_CPP(extern "C" {)

typedef struct ma_season_s ma_season_t, *ma_season_h;


MA_SEASON_API ma_error_t ma_season_create(ma_msgbus_t *msgbus, ma_ds_t *datastore, const char *base_path, ma_season_t **season);

MA_SEASON_API ma_error_t ma_season_start(ma_season_t *season);

MA_SEASON_API ma_error_t ma_season_stop(ma_season_t *season);

MA_SEASON_API ma_error_t ma_season_add(ma_season_t *season, ma_season_info_t *info);

MA_SEASON_API ma_error_t ma_season_add_variant(ma_season_t *season, const char *season_id, ma_variant_t *var) ;

MA_SEASON_API ma_error_t ma_season_delete(ma_season_t *season, const char *season_id);

MA_SEASON_API ma_error_t ma_season_delete_all(ma_season_t *season);

MA_SEASON_API ma_error_t ma_season_get(ma_season_t *season, const char *season_id, ma_season_info_t **info);

MA_SEASON_API ma_error_t ma_season_set_logger(ma_logger_t *logger);

MA_SEASON_API ma_error_t ma_season_get_msgbus(ma_season_t *season, ma_msgbus_t **msgbus);

MA_SEASON_API ma_error_t ma_season_release(ma_season_t *season);


MA_CPP(})

#endif /* MA_SEASON_H_INCLUDED */
