#ifndef MA_SEASON_DATASTORE_H_INCLUDED
#define MA_SEASON_DATASTORE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/season/ma_season.h"
#include "ma/internal/utils/season/ma_season_info.h"
#include "ma/ma_datastore.h"
#include "ma/datastore/ma_ds_database.h"

MA_CPP(extern "C" {)
MA_SEASON_API ma_error_t ma_season_datastore_read(ma_season_t *season, ma_ds_t *datastore, const char *ds_path);

MA_SEASON_API ma_error_t ma_season_datastore_get(ma_season_t *season, ma_ds_t *datastore, const char *ds_path, const char *season_id, ma_season_info_t **info);

MA_SEASON_API ma_error_t ma_season_datastore_write(ma_season_t *season, ma_season_info_t *info, ma_ds_t *datastore, const char *ds_path);

MA_SEASON_API ma_error_t ma_season_datastore_write_variant(ma_season_t *season, const char *season_id, ma_ds_t *datastore, const char *ds_path, ma_variant_t *var);

MA_SEASON_API ma_error_t ma_season_datastore_remove_season(const char *season, ma_ds_t *datastore, const char *ds_path);

MA_SEASON_API ma_error_t ma_season_datastore_clear(ma_ds_t *datastore, const char *ds_path);

MA_CPP(})


#endif /* MA_SEASON_DATASTORE_H_INCLUDED */

