/** @file ma_deserialize.h
 *  @brief Deserialization API Declarations
 *
 * This file contains API's for the Deserialization stream and
 * construct's variant after unpacking using messagepack unpacking
 * mechanism
 */

#ifndef MA_DESERIALIZE_H_INCLUDED
#define MA_DESERIALIZE_H_INCLUDED

#ifdef _MSC_VER
#pragma once
#endif // MSC_VER

#include "ma/ma_common.h"
#include "ma/ma_variant.h"

MA_CPP(extern "C" {)

typedef struct ma_deserializer_s ma_deserializer_t, *ma_deserializer_h;

/** @brief Creates deserializer
 *  @param    [in]   self  Pointer to memory location holds newly constructed
 *                         deserializer
 *  @result                MA_OK on successful
 *                         MA_ERROR_OUTOFMEMORY on allocation failure
 *                         MA_ERROR_INVALIDARG  on invalid argument
 */
MA_DESERIALIZE_API ma_error_t ma_deserializer_create(ma_deserializer_t **self);

/** @brief Destroys deserializer
 *  @param    [in]  self deserializer handle
 *  @result              MA_OK on successful
 *                       MA_ERROR_INVALIDARG  on invalid argument
 */
MA_DESERIALIZE_API ma_error_t ma_deserializer_destroy(ma_deserializer_t *self);

/** @brief Set's Deserialize buffer
 *  @param   [in]  self              deserializer handle
 *  @param   [in]  serialized_buffer serialized buffer
 *  @param   [in]  len               serialized buffer length
 *  @result                          MA_OK on successful
 *                                   MA_ERROR_INVALIDARG on invalid arguments
 */
MA_DESERIALIZE_API ma_error_t ma_deserializer_put(ma_deserializer_t   *self,
                                                  const char          *serialized_buffer,
                                                  size_t              len);

/** @brief Deserialize and constructs variant
 *  @param    [in]  self    deserializer handle
 *  @param    [out] var_obj Pointer to newly constructed variant
 *  @result                 MA_OK on successful
 *                          MA_ERROR_INVALIDARG  on invalid arguments
 */
MA_DESERIALIZE_API ma_error_t ma_deserializer_get_next(ma_deserializer_t *self,
                                                       ma_variant_t      **var_obj);

/** @brief Reset deserializer buffer handles
 *  @param  [in]  deserializer     deserializer handle
 *  @param  [in]  forced		   forcefull reset [0: do not reset if unparsed data is present (e.g. incomplete data) 1: blindly reset ]
 *  @result                MA_OK on successful
 *                         MA_ERROR_OUTOFMEMORY on allocation failure
 *                         MA_ERROR_INVALIDARG on invalid argument
 */ 
MA_SERIALIZE_API ma_error_t ma_deserializer_reset(ma_deserializer_t *deserializer, ma_bool_t forced) ;

MA_CPP(})

#endif //#ifndef MA_DESERIALIZE_H_INCLUDED

