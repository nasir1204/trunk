/** @file ma_serialize.h
 *  @brief serialization API Declarations
 *
 * This file contains API's for the serializing data
 * by using messagepack packing mechanism
 */

#ifndef MA_SERIALIZE_H_INCLUDED
#define MA_SERIALIZE_H_INCLUDED

#ifdef _MSC_VER
#pragma once
#endif // MSC_VER

#include "ma/ma_common.h"
#include "ma/ma_variant.h"

MA_CPP(extern "C" {)

typedef struct ma_serializer_s ma_serializer_t, *ma_serializer_h;

/** @brief Creates serializer
 *  @param    [in]   serializer  Pointer to memory location holds newly constructed
 *                         serializer
 *  @result                MA_OK on successful
 *                         MA_ERROR_OUTOFMEMORY on allocation failure
 *                         MA_ERROR_INVALIDARG  on invalid argument
 */
MA_SERIALIZE_API ma_error_t ma_serializer_create(ma_serializer_t **serializer);

/** @brief Destroys serializer
 *  @param    [in]  serializer serializer handle
 *  @result              MA_OK on successful
 *                       MA_ERROR_INVALIDARG  on invalid argument
 */
MA_SERIALIZE_API ma_error_t ma_serializer_destroy(ma_serializer_t *serializer);

/** @brief  Serialize variant
 *  @param    [in]  serializer    serializer handle
 *  @param    [in]  var_obj variant to be serialized
 *  @result                 MA_OK on successful
 *                          MA_ERROR_INVALIDARG  on invalid arguments
 */
MA_SERIALIZE_API ma_error_t ma_serializer_add_variant(ma_serializer_t       *serializer,
                                              const ma_variant_t    *var_obj);


/** @brief Returns serializer's buffer and length
 *  @param   [in]  serializer               serializer handle
 *  @param   [out] serialized_buf_ptr Pointer holds address of serializer buffer
 *  @param   [out] serialized_buf_len serialized buffer length
 *  @result                           MA_OK on successful
 *                                    MA_ERROR_INVALIDARG on invalid arguments
 */
MA_SERIALIZE_API ma_error_t ma_serializer_get(ma_serializer_t *serializer,
                                              char            **serialized_buf_ptr,
                                              size_t          *serialized_buf_len);

/** @brief Extracts serializer's buffer length
 *  @param  [in]  serializer     serializer handle
 *  @param  [out] len      Pointer to memory location that stores length of the buffer
 *  @result                MA_OK on successful
 *                         MA_ERROR_INVALIDARG on invalid argument
 */
MA_SERIALIZE_API ma_error_t ma_serializer_length(ma_serializer_t *serializer, size_t *len);

/** @brief Reset serializer buffer handles
 *  @param  [in]  serializer     serializer handle
 *  @result                MA_OK on successful
 *                         MA_ERROR_OUTOFMEMORY on allocation failure
 *                         MA_ERROR_INVALIDARG on invalid argument
 */ 
MA_SERIALIZE_API ma_error_t ma_serializer_reset(ma_serializer_t *serializer);


MA_CPP(})

#endif //#ifndef MA_SERIALIZE_H_INCLUDED

