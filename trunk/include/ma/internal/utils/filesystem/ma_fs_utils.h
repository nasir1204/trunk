#ifndef MA_FS_UTILS_H_INCLUDED
#define MA_FS_UTILS_H_INCLUDED

#include "ma/ma_common.h"
#include "uv.h"

MA_CPP(extern "C" {)

/* TBD - Can we merge the the below two function, i.e, is_file_exists returns size if file exists */
ma_bool_t ma_fs_is_file_exists(uv_loop_t *uv_loop, const char *file_path);

size_t ma_fs_get_file_size(uv_loop_t *uv_loop, const char *file_path);

ma_bool_t	ma_fs_is_dir(uv_loop_t *uv_loop, const char *file_path) ;

/* if base_dir is NULL, dir_name must be a absolute path */
void ma_fs_make_recursive_dir(uv_loop_t *uv_loop, char *base_dir, char *dir_name);

void ma_fs_make_recursive_dir_as_user(uv_loop_t *uv_loop, char *base_dir, char *dir_name, const char *usernam);

void ma_fs_create_file(uv_loop_t *uv_loop, const char *file_name);

ma_bool_t ma_fs_remove_dir(uv_loop_t *uv_loop, char *dir_name) ;

ma_bool_t ma_fs_remove_recursive_dir(uv_loop_t *uv_loop, char *dir_name) ;

ma_bool_t ma_fs_move_file(uv_loop_t *uv_loop, const char *src, const char *dst);

ma_bool_t ma_fs_move_file_as_user(uv_loop_t *uv_loop, char *src, char *dst, const char *username);

ma_bool_t ma_fs_copy_file(uv_loop_t *uv_loop, char *src, char *dst) ;

ma_bool_t ma_fs_copy_file_with_timestamp(uv_loop_t *uv_loop, char *src, char *dst) ;

ma_bool_t ma_fs_copy_file_as_user(uv_loop_t *uv_loop, char *src, char *dst, const char *username) ;

ma_bool_t ma_fs_remove_file(uv_loop_t *uv_loop, const char *file) ;

ma_bool_t ma_fs_write_data(uv_loop_t *uv_loop, char *file_name, unsigned char *data, size_t size) ;

/* If data is NULL, size will return the lenght of bufffer needed to load file data */
ma_bool_t ma_fs_get_data(uv_loop_t *uv_loop, const char *file_name, unsigned char *data, size_t *size) ;

MA_CPP(})

#endif /* MA_FS_UTILS_H_INCLUDED */

