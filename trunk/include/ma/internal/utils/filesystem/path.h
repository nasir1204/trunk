#ifndef MA_FILESYSTEM_PATH_H_INCLUDED
#define MA_FILESYSTEM_PATH_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"

#ifdef MA_WINDOWS
#define MA_PATH_SEPARATOR '\\'
#define MA_PATH_SEPARATOR_STR "\\"
#else
#define MA_PATH_SEPARATOR '/'
#define MA_PATH_SEPARATOR_STR "/"
#endif

#define MA_DATASTORE_PATH_C	'\\'

//Is it right ??
#define MAX_PATH_LEN        1024 

MA_CPP(extern "C" {)

ma_error_t MA_UTILS_API form_path(ma_temp_buffer_t *destination, const char *root_path, const char *sub_path, ma_bool_t b_datastore_path);

MA_CPP(})

#endif /* MA_FILESYSTEM_PATH_H_INCLUDED */
