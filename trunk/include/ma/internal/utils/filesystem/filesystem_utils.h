#ifndef MA_FILESYSTEM_UTILS_H_INCLUDED
#define MA_FILESYSTEM_UTILS_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/filesystem/path.h"

ma_error_t ma_filesystem_directory_tree_create(char *base, char *dir_path) ;

ma_error_t ma_filesystem_directory_create(char *path) ;

/*Non-recursive directory deletion */
ma_error_t ma_filesystem_directory_delete(char *path) ;

ma_error_t ma_filesystem_copy_file(char *source,  char *dest) ;

ma_error_t ma_filesystem_move_file(char *source,  char *dest) ;

ma_error_t ma_filesystem_write_data_to_file(char *file, unsigned char *data, size_t data_size) ;

ma_error_t ma_filesystem_delete_file(char *file) ;

#endif /* MA_FILESYSTEM_UTILS_H_INCLUDED */
