#ifndef MA_BOARD_PINS_H_INCLUDED
#define MA_BOARD_PINS_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_variant.h"
#include "ma/internal/utils/json/ma_json_utils.h"

MA_CPP(extern "C" {)

typedef struct ma_board_pin_s ma_board_pin_t, *ma_board_pin_h;

MA_BOARD_API ma_error_t ma_board_pin_create(ma_board_pin_t **pins);
MA_BOARD_API ma_error_t ma_board_pin_release(ma_board_pin_t *pins);
MA_BOARD_API ma_error_t ma_board_pin_add(ma_board_pin_t *pin);
MA_BOARD_API ma_error_t ma_board_pin_copy(ma_board_pin_t *dest, const ma_board_pin_t *src);

/* setter */
MA_BOARD_API ma_error_t ma_board_pin_set_id(ma_board_pin_t *pins, const char *id);
MA_BOARD_API ma_error_t ma_board_pin_set_topic(ma_board_pin_t *pins, const char *parent);

/* getter */
MA_BOARD_API ma_error_t ma_board_pin_get_id(ma_board_pin_t *pins, const char **id);
MA_BOARD_API ma_error_t ma_board_pin_get_topic(ma_board_pin_t *pins, const char **parent);

MA_BOARD_API ma_error_t ma_board_pin_convert_to_variant(ma_board_pin_t *pins, ma_variant_t **v);
MA_BOARD_API ma_error_t ma_board_pin_convert_from_variant(ma_variant_t *v, ma_board_pin_t **pins);

MA_BOARD_API ma_error_t ma_board_pin_convert_to_json(ma_board_pin_t *pin, ma_json_t **json);


MA_CPP(})

#endif
