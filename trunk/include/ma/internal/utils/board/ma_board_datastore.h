#ifndef MA_BOARD_DATASTORE_H_INCLUDED
#define MA_BOARD_DATASTORE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/board/ma_board.h"
#include "ma/internal/utils/board/ma_board_info.h"
#include "ma/ma_datastore.h"
#include "ma/datastore/ma_ds_database.h"

MA_CPP(extern "C" {)
MA_BOARD_API ma_error_t ma_board_datastore_read(ma_board_t *board, ma_ds_t *datastore, const char *ds_path, ma_variant_t **var);

MA_BOARD_API ma_error_t ma_board_datastore_get(ma_board_t *board, ma_ds_t *datastore, const char *ds_path, const char *board_id, ma_board_info_t **info);

MA_BOARD_API ma_error_t ma_board_datastore_write(ma_board_t *board, ma_board_info_t *info, ma_ds_t *datastore, const char *ds_path);

MA_BOARD_API ma_error_t ma_board_datastore_write_variant(ma_board_t *board, const char *board_id, ma_ds_t *datastore, const char *ds_path, ma_variant_t *var);

MA_BOARD_API ma_error_t ma_board_datastore_remove_board(const char *board, ma_ds_t *datastore, const char *ds_path);

MA_BOARD_API ma_error_t ma_board_datastore_clear(ma_ds_t *datastore, const char *ds_path);

MA_CPP(})


#endif /* MA_BOARD_DATASTORE_H_INCLUDED */

