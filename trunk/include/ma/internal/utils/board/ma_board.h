#ifndef MA_BOARD_H_INCLUDED
#define MA_BOARD_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/board/ma_board_info.h"
#include "ma/ma_datastore.h"
#include "ma/datastore/ma_ds_database.h"
#include "ma/ma_log.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_variant.h"
#include "ma/internal/defs/ma_dashboard_defs.h"
#include "ma/internal/utils/context/ma_context.h"


MA_CPP(extern "C" {)

typedef struct ma_board_s ma_board_t, *ma_board_h;


MA_BOARD_API ma_error_t ma_board_create(ma_msgbus_t *msgbus, ma_ds_t *datastore, const char *base_path, ma_board_t **board);

MA_BOARD_API ma_error_t ma_board_start(ma_board_t *board);

MA_BOARD_API ma_error_t ma_board_stop(ma_board_t *board);

MA_BOARD_API ma_error_t ma_board_add(ma_board_t *board, ma_board_info_t *info);

MA_BOARD_API ma_error_t ma_board_add_variant(ma_board_t *board, const char *board_id, ma_variant_t *var) ;

MA_BOARD_API ma_error_t ma_board_delete(ma_board_t *board, const char *board_id);

MA_BOARD_API ma_error_t ma_board_delete_all(ma_board_t *board);

MA_BOARD_API ma_error_t ma_board_get(ma_board_t *board, const char *board_id, ma_board_info_t **info);

MA_BOARD_API ma_error_t ma_board_get_all(ma_board_t *board, ma_variant_t **var);

MA_BOARD_API ma_error_t ma_board_set_logger(ma_logger_t *logger);

MA_BOARD_API ma_error_t ma_board_set_context(ma_context_t *context);

MA_BOARD_API ma_error_t ma_board_get_msgbus(ma_board_t *board, ma_msgbus_t **msgbus);

MA_BOARD_API ma_error_t ma_board_release(ma_board_t *board);


MA_CPP(})

#endif /* MA_BOARD_H_INCLUDED */
