#ifndef MA_BOARD_INFO_H_INCLUDED
#define MA_BOARD_INFO_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_variant.h"
#include "ma/scheduler/ma_triggers.h"
#include "ma/internal/utils/board/ma_board_topics.h"
#include "ma/internal/utils/json/ma_json_utils.h"

MA_CPP(extern "C" {)

typedef struct ma_board_info_s ma_board_info_t, *ma_board_info_h;

/* board info ctor */
MA_BOARD_API ma_error_t ma_board_info_create(ma_board_info_t **self);

/* board info dtor */
MA_BOARD_API ma_error_t ma_board_info_release(ma_board_info_t *self);

MA_BOARD_API ma_error_t ma_board_info_copy(ma_board_info_t *dest, const ma_board_info_t *src);

/* setter */
MA_BOARD_API ma_error_t ma_board_info_set_id(ma_board_info_t *self, const char *id);
MA_BOARD_API ma_error_t ma_board_info_set_topic(ma_board_info_t *self, const char *id, ma_board_topic_t *topic);
MA_BOARD_API ma_error_t ma_board_info_set_size(ma_board_info_t *self, size_t no_of_topics);
MA_BOARD_API ma_error_t ma_board_info_set_image_url(ma_board_info_t *self, const char *url);

/* getter */
MA_BOARD_API ma_error_t ma_board_info_get_id(ma_board_info_t *self, const char **id);
MA_BOARD_API ma_error_t ma_board_info_get_topic(ma_board_info_t *self, const char *id, ma_board_topic_t **topic);
MA_BOARD_API ma_error_t ma_board_info_get_topics(ma_board_info_t *self, const ma_board_topic_t ***topic, int *no_of_topics);
MA_BOARD_API ma_error_t ma_board_info_get_size(ma_board_info_t *self, size_t *no_of_topics);
MA_BOARD_API ma_error_t ma_board_info_get_image_url(ma_board_info_t *self, const char **url);

MA_BOARD_API ma_error_t ma_board_info_convert_to_variant(ma_board_info_t *self, ma_variant_t **v);
MA_BOARD_API ma_error_t ma_board_info_convert_from_variant(ma_variant_t *v, ma_board_info_t **self);

/* user is responsible to release json array */
MA_BOARD_API ma_error_t ma_board_info_convert_to_json(ma_board_info_t *self, ma_json_table_t **json);


MA_CPP(})

#endif /* MA_BOARD_INFO_H_INCLUDED */

