#ifndef MA_BOARD_TOPICS_H_INCLUDED
#define MA_BOARD_TOPICS_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_variant.h"
#include "ma/internal/utils/board/ma_board_pins.h"
#include "ma/internal/utils/json/ma_json_utils.h"

MA_CPP(extern "C" {)

typedef struct ma_board_topic_s ma_board_topic_t, *ma_board_topic_h;

/* ctor & dtor */
MA_BOARD_API ma_error_t ma_board_topic_create(ma_board_topic_t **self);
MA_BOARD_API ma_error_t ma_board_topic_release(ma_board_topic_t *self);
MA_BOARD_API ma_error_t ma_board_topic_add(ma_board_topic_t *topic);

MA_BOARD_API ma_error_t ma_board_topic_remove_pin(ma_board_topic_t *self, const char *id);

/* setter */
MA_BOARD_API ma_error_t ma_board_topic_set_id(ma_board_topic_t *self, const char *id);
MA_BOARD_API ma_error_t ma_board_topic_set_pin(ma_board_topic_t *self, const char *id, ma_board_pin_t *pin);
MA_BOARD_API ma_error_t ma_board_topic_set_board(ma_board_topic_t *self, const char *id);
MA_BOARD_API ma_error_t ma_board_topic_set_size(ma_board_topic_t *self, size_t no_of_pins);
MA_BOARD_API ma_error_t ma_board_topic_copy(ma_board_topic_t *dest, const ma_board_topic_t *src);

/* getter */
MA_BOARD_API ma_error_t ma_board_topic_get_id(ma_board_topic_t *self, const char **id);
MA_BOARD_API ma_error_t ma_board_topic_get_pin(ma_board_topic_t *self, const char *id, ma_board_pin_t **pin);
MA_BOARD_API ma_error_t ma_board_topic_get_board(ma_board_topic_t *self, const char **id);
MA_BOARD_API ma_error_t ma_board_topic_get_pins(ma_board_topic_t *self, ma_board_pin_t ***pin, int no_of_pins);
MA_BOARD_API ma_error_t ma_board_topic_get_size(ma_board_topic_t *self, size_t *no_of_pins);

MA_BOARD_API ma_error_t ma_board_topic_convert_to_variant(ma_board_topic_t *self, ma_variant_t **v);
MA_BOARD_API ma_error_t ma_board_topic_convert_from_variant(ma_variant_t *v, ma_board_topic_t **self);

MA_BOARD_API ma_error_t ma_board_topic_convert_to_json(ma_board_topic_t *self, ma_json_array_t **array);

MA_CPP(})

#endif
