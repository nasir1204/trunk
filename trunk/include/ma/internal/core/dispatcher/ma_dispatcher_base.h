#ifndef MA_DISPATCHER_BASE_H_INCLUDED
#define MA_DISPATCHER_BASE_H_INCLUDED

#include "ma/ma_common.h"

#ifdef MA_THREADING_ENABLED
    #include "ma/internal/utils/threading/ma_thread.h"
    #define MA_MUTEX_INIT(x) ma_mutex_init(&x->mutex)
    #define MA_MUTEX_LOCK(x)   ma_mutex_lock(&x->mutex)
    #define MA_MUTEX_UNLOCK(x) ma_mutex_unlock(&x->mutex)
    #define MA_MUTEX_DESTROY(x) ma_mutex_destroy(&x->mutex)
#else
    #define MA_MUTEX_INIT(x) /* NOP */
    #define MA_MUTEX_LOCK(x)   /* NOP */
    #define MA_MUTEX_UNLOCK(x) /* NOP */
    #define MA_MUTEX_DESTROY(x)/* NOP */
#endif

MA_CPP(extern "C" {)

typedef struct ma_dispatcher_base_s ma_dispatcher_base_t;

typedef struct ma_dispatcher_base_methods_s {
    ma_error_t (*initialize)(ma_dispatcher_base_t *self, const char *default_rsdk_path);
    ma_error_t (*load)(ma_dispatcher_base_t *self, const char *module, const char *product_name, const char *library_name, unsigned int *version, const char **sym_name_table, void *sym_addr_table, size_t size);
    ma_error_t (*unload)(ma_dispatcher_base_t *self, const char *product_name, const char *library_name);
    void (*release)(ma_dispatcher_base_t *self);
} ma_dispatcher_base_methods_t;


struct ma_dispatcher_base_s {
    const ma_dispatcher_base_methods_t  *methods;
    void                                *data ;
#ifdef MA_THREADING_ENABLED
    ma_mutex_t                         mutex;
#endif
};

static MA_INLINE void ma_dispatcher_base_init(ma_dispatcher_base_t *base,
                                              const ma_dispatcher_base_methods_t  *methods,
                                              void *data) {
    base->methods = methods;
    base->data = data;
    MA_MUTEX_INIT(base);
}

static MA_INLINE ma_error_t ma_dispatcher_base_initialize(ma_dispatcher_base_t *base, const char *default_rsdk_path) {
    if(base && base->methods) {
        ma_error_t err = MA_OK;
        MA_MUTEX_LOCK(base);
        err = (base->methods->initialize)?base->methods->initialize(base, default_rsdk_path):MA_ERROR_PRECONDITION;
        MA_MUTEX_UNLOCK(base);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static MA_INLINE ma_error_t ma_dispatcher_base_load(ma_dispatcher_base_t *base, const char *module, const char *product_name, const char *library_name,  unsigned int *version, const char **sym_name_table, void *sym_addr_table, size_t sym_table_size) {
    if(base && base->methods) {
        ma_error_t err = MA_OK;
        MA_MUTEX_LOCK(base);
        err = (base->methods->load)?base->methods->load(base, module, product_name,library_name, version, sym_name_table, sym_addr_table, sym_table_size):MA_ERROR_PRECONDITION;
        MA_MUTEX_UNLOCK(base);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static MA_INLINE ma_error_t ma_dispatcher_base_unload(ma_dispatcher_base_t *base, const char *product_name, const char *library_name) {
    if(base && base->methods) {
        ma_error_t err = MA_OK;
        MA_MUTEX_LOCK(base);
        err = (base->methods->unload)?base->methods->unload(base, product_name, library_name):MA_ERROR_PRECONDITION;
        MA_MUTEX_UNLOCK(base);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static MA_INLINE void ma_dispatcher_base_release(ma_dispatcher_base_t *base) {
    if(base && base->methods) {
        MA_MUTEX_LOCK(base);
        if(base->methods->release)base->methods->release(base);
        MA_MUTEX_UNLOCK(base);
        MA_MUTEX_DESTROY(base);
    }
}

MA_CPP(})
#endif //#ifndef MA_DISPATCHER_BASE_H_INCLUDED
