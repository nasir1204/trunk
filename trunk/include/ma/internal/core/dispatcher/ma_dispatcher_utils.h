#ifndef MA_DISPATCH_UTILS_H_INCLUDED
#define MA_DISPATCH_UTILS_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/platform/ma_dl.h"

#if  !defined(MA_WINDOWS)
typedef void *HMODULE;
#endif

/* RTLD_DEEPBIND flag is introduced for Linux as dispatcher load the library and resolve the symbols from dependent libs only. 
   Otherise symbol collison may be happened when diffrent versions of 3rd party libs used by PPs and MA or openssl and RSA used by PPs and MA respectiviely.
   e.g. openssl is consumed by PP and RSA is consumed by MA but both crypto components have the common names like BIO_new_fd.

--> From - dlopen man page 
RTLD_DEEPBIND (since glibc 2.3.4)
Place the lookup scope of the symbols in this library ahead of the global scope. This means that a self-contained library will use its own symbols in preference to global symbols with the same name contained in libraries that have already been loaded. This flag is not specified in POSIX.1-2001.

	This behaviour is default on MAC, so it would work without any other flags.
	TBD - Will tryo to fond solutions for other UNIX
*/

#ifdef MA_LINUX
#include <dlfcn.h>
#define MA_DL_OPEN(x,y) ma_dl_open(x, RTLD_LAZY | RTLD_DEEPBIND, y)
#else
#define MA_DL_OPEN(x,y) ma_dl_open(x, 0, y)
#endif

#define MA_DL_SYM(x, y) ma_dl_sym(x,y)
#define MA_DL_CLOSE(x) ma_dl_close(x)

MA_CPP(extern "C" {)
ma_error_t ma_get_product_rsdk_path(const char *product_id, char **path);
MA_CPP(})
#endif //#ifndef MA_DISPATCH_UTILS_H_INCLUDED
