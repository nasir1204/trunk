#ifndef MA_DISPATCHER_GROUPER_H_INCLUDED
#define MA_DISPATCHER_GROUPER_H_INCLUDED

#include "ma/ma_common.h"


MA_CPP(extern "C" {)

typedef struct ma_grouper_s ma_grouper_t;

typedef struct ma_grouper_methods_s {
    ma_error_t (*add_member)(ma_grouper_t *grouper, const char *member);
    ma_error_t (*remove_member)(ma_grouper_t *grouper, const char *member);
    ma_error_t (*find_member)(ma_grouper_t *grouper, const char *member_name, void *member);
    ma_error_t (*load_sym_table)(ma_grouper_t *grouper, const char *member_name, const char **sym_name_table, void *sym_addr_table, size_t sym_table_size);
    void (*destroy_group)(ma_grouper_t *grouper);
} ma_grouper_methods_t;


struct ma_grouper_s {
    const ma_grouper_methods_t  *methods;
    void                        *data ;
};

static MA_INLINE void ma_grouper_init(ma_grouper_t *grouper,
                                      const ma_grouper_methods_t  *methods,
                                      void *data) {
    grouper->methods = methods;
    grouper->data = data;
}

static MA_INLINE ma_error_t ma_grouper_add_member(ma_grouper_t *grouper, const char *member) {
    if(grouper && grouper->methods) {
        return (grouper->methods->add_member)?grouper->methods->add_member(grouper, member):MA_ERROR_UNEXPECTED;
    }
    return MA_ERROR_INVALIDARG;
}

static MA_INLINE ma_error_t ma_grouper_remove_member(ma_grouper_t *grouper, const char *member) {
    if(grouper && grouper->methods) {
        return (grouper->methods->remove_member)?grouper->methods->remove_member(grouper, member):MA_ERROR_UNEXPECTED;
    }
    return MA_ERROR_INVALIDARG;
}

static MA_INLINE ma_error_t ma_grouper_find_member(ma_grouper_t *grouper, const char *member_name, void *member) {
    if(grouper && grouper->methods) {
        return (grouper->methods->find_member)?grouper->methods->find_member(grouper, member_name, member):MA_ERROR_UNEXPECTED;
    }
    return MA_ERROR_INVALIDARG;
}

static MA_INLINE ma_error_t ma_grouper_load_sym_table(ma_grouper_t *grouper, const char *member, const char **sym_name_table, void *sym_addr_table, size_t sym_table_size) {
    if(grouper && grouper->methods) {
        return (grouper->methods->load_sym_table)?grouper->methods->load_sym_table(grouper, member, sym_name_table, sym_addr_table, sym_table_size):MA_ERROR_UNEXPECTED;
    }
    return MA_ERROR_INVALIDARG;
}

static MA_INLINE void ma_grouper_destroy_group(ma_grouper_t *grouper) {
    if(grouper && grouper->methods) {
        if(grouper->methods->destroy_group)
            grouper->methods->destroy_group(grouper);
    }
    return ;
}

MA_CPP(})
#endif //#ifndef MA_DISPATCHER_GROUPER_H_INCLUDED
