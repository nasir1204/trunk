#ifndef MA_PRODUCT_GROUPER_H_INCLUDED
#define MA_PRODUCT_GROUPER_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/core/dispatcher/ma_dispatcher_grouper.h"
#include "ma/internal/core/dispatcher/ma_library_grouper.h"

typedef struct ma_product_grouper_s ma_product_grouper_t;

struct ma_product_grouper_s {
    ma_grouper_t             grouper_impl;
    ma_library_grouper_t     *group; /* product array */
    size_t                   size;
    size_t                   capacity;
	char                     *default_rsdk_path;
    void                     *data ;
};

MA_CPP(extern "C" {)
void ma_product_grouper_init(ma_product_grouper_t *product_grouper, const char *default_rsdk_path, void *data);
MA_CPP(})
#endif //#ifndef MA_PRODUCT_GROUPER_H_INCLUDED
