#ifndef MA_LIBRARY_GROUPER_H_INCLUDED
#define MA_LIBRARY_GROUPER_H_INCLUDED

#include "ma/internal/utils/threading/ma_atomic.h"
#include "ma/internal/core/dispatcher/ma_dispatcher_grouper.h"
#include "ma/internal/utils/platform/ma_dl.h"
#include "ma/internal/core/dispatcher/ma_dispatcher_utils.h"

MA_CPP(extern "C" {)
typedef struct ma_library_s {
    ma_lib_t            lib;
    ma_atomic_counter_t ref_count;
    unsigned int        version;
    ma_bool_t           is_loaded;
    void                *plugin_ver;/* plugin version address place holder*/
    char                *name;
    void                **sym_addr_table_set;
    size_t              sym_table_set_size;
}ma_library_t, *ma_library_h;

typedef struct ma_library_grouper_s {
    ma_grouper_t     grouper_impl;
    ma_library_t     *group;/* library array */
    char             *name; /* product name */
    char             *path;
    size_t           size;
    size_t           capacity;
    ma_atomic_counter_t ref_count;
    void             *data ;
}ma_library_grouper_t;

void ma_library_grouper_init(ma_library_grouper_t *library_grouper, void *data);

MA_CPP(})
#endif //#ifndef MA_LIBRARY_GROUPER_H_INCLUDED
