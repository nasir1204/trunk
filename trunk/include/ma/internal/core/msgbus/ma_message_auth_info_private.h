#ifndef MA_MESSAGE_AUTH_INFO_PRIVATE_H_INLCUDED
#define MA_MESSAGE_AUTH_INFO_PRIVATE_H_INLCUDED

#include "ma/ma_message_auth_info.h"

#include "ma/internal/core/msgbus/ma_named_pipe_credentials.h"

/* Contains internal methods to manipulate ma_message_auth_info_t object objects */

/* Creates a new instance with an initial ref count of 1 */
ma_error_t ma_message_auth_info_create(ma_message_auth_info_t **auth_info);

ma_error_t ma_message_auth_info_set_attribute(ma_message_auth_info_t *auth_info, ma_message_auth_info_attribute_t attrib, ...);



typedef struct ma_message_auth_info_provider_s ma_message_auth_info_provider_t, *ma_message_auth_info_provider_h;

typedef struct ma_message_auth_info_provider_methods_s {
    
    /*!
     * virtual destructor
     */
    void (*destroy)(ma_message_auth_info_provider_h);
    
    /*!
     * one and only method that provides auth info 
     */
    ma_error_t (*decorate_auth_info)(ma_message_auth_info_provider_h, ma_pid_t pid, ma_message_auth_info_t *auth_info);

} ma_message_auth_info_provider_functions_t;

struct ma_message_auth_info_provider_s {
    /*!
     * v-table
     */
    ma_message_auth_info_provider_functions_t const *methods;

    /*!
     * some opaque data for derived classes to use
     */
    void *data;
};

/*!
 * initializes (the base of) a auth_provider implementation 
 * this is a convenience function to be used by implemntors 
 */
ma_error_t MA_STATIC_INLINE ma_message_auth_info_provider_init(ma_message_auth_info_provider_h provider, ma_message_auth_info_provider_functions_t const *methods, void *data) {
    if (provider) {
        provider->methods = methods;
        provider->data = data;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

void MA_STATIC_INLINE ma_message_auth_info_provider_release(ma_message_auth_info_provider_h provider) {
    if (provider)
        provider->methods->destroy(provider);

}


/*! 
 * finds auth information for the given pid and populates it into the specified auth_info structure
 */
ma_error_t MA_STATIC_INLINE ma_message_auth_info_provider_decorate_auth_info(ma_message_auth_info_provider_h provider, ma_pid_t pid, ma_message_auth_info_t *auth_info) {
    return provider 
        ? provider->methods->decorate_auth_info(provider, pid, auth_info)
        : MA_ERROR_INVALIDARG; 
}

#endif /* MA_MESSAGE_AUTH_INFO_PRIVATE_H_INLCUDED */
