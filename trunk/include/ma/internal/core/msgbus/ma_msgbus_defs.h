#ifndef MA_MSGBUS_DEFS_H_INCLUED
#define MA_MSGBUS_DEFS_H_INCLUED
/*
* Macros needed by MsgBus */
#define MA_MSGBUS_PATH_NAME_STR                     "MsgBus"
#define MA_MSGBUS_KEY_BROKER_ID_STR                 "msgbus_broker_id"
#define MA_MSGBUS_KEY_BROKER_PID_INT                "msgbus_broker_pid"
#define MA_MSGBUS_KEY_BROKER_HOST_NAME_STR          "msgbus_broker_host_name"
#define MA_MSGBUS_KEY_BROKER_STATUS_INT             "msgbus_broker_status"

#define MA_MSGBUS_CONF_BASE_DIR_STR                 "/var/tmp/"     /* For *nix */

#define MA_MSGBUS_AUTH_SIG_NAME_STR	                "ma_msgbus_auth.sig"

#define MA_MSGBUS_AUTH_EXE_SHA256_DIGEST_LENGTH		32

#define MA_MSGBUS_PASSPHRASE_HASH_STR				"AuthorizationHash"

/*
<?xml version="1.0" encoding="utf-8"?>
<MsgbusAuthManifest Version = "5.0.0">
       <MsgbusAuth Name="masvc.exe" Aliases=""  SHA256="" Technology="">
              <AuthRule Version="5.0.0"> </AuthRule>
       </MsgbusAuth>
       <MsgbusAuth Name="macompatsvc.exe" Aliases=""  SHA256= "" Technology="">
              <AuthRule Version="5.0.0"> </AuthRule>
       </MsgbusAuth>
</MsgbusAuthManifest>
*/
 
 /* ASN1 representation, STACK OF ATTRIBUTES
       ASN1_STRING - Manifest Version
	   ASN1_OCTET_STRING - Manifest xml file
*/
 
/* 
Sample Auth rules

"AuthType==ExecutableValidation,ChallengeResponse and User==root,user1,user2 and Group==root,system and PlatformID==MLOS,ANDROID"

- No space in (key==value) pair
- A single space before and after "and"

"AuthType==ExecutableValidation and AuthType==ChallengeResponse and User==root and User==user1 and Group==root and PlatformID==MLOS and PlatformID==ANDROID"

"AuthType==ExecutableValidation and AuthType==ChallengeResponse and User==root,user1 and Group==root,system and PlatformID==MLOS,LINUX"
*/



#define MA_MSGBUS_KEY_AUTH_MANIFEST_STR						"MsgbusAuthManifest"

#define MA_MSGBUS_KEY_AUTH_MANIFEST_VERSION_STR				"Version"

#define MA_MSGBUS_VALUE_AUTH_MANIFEST_VERSION_STR			"5.0.0"

#define MA_MSGBUS_KEY_AUTH_NODE_STR							"MsgbusAuth"

#define MA_MSGBUS_KEY_AUTH_SELF_NAME_STR					"Name"

#define MA_MSGBUS_KEY_AUTH_SELF_ALIASES_STR					"Aliases"

#define MA_MSGBUS_KEY_AUTH_SELF_HASH_STR					"SHA256"

#define MA_MSGBUS_KEY_AUTH_RULE_STR							"AuthRule"

#define MA_MSGBUS_VALUE_DEFAULT_AUTH_RULE_STR				"AuthType==ExecutableValidation"

#define MA_MSGBUS_KEY_AUTH_RULE_VERSION_STR					"Version"

#define MA_MSGBUS_VALUE_AUTH_RULE_VERSION_STR				"5.0.0"

#define MA_MSGBUS_KEY_AUTH_TYPE_STR							"AuthType"

#define MA_MSGBUS_VALUE_AUTH_TYPE_AUTHENTICODE_STR			"Authenticode"

#define MA_MSGBUS_VALUE_AUTH_TYPE_EXECUTABLE_VALIDATION_STR "ExecutableValidation"

#define MA_MSGBUS_VALUE_AUTH_TYPE_CHALLENGE_RESPONSE_STR    "ChallengeResponse"

#define MA_MSGBUS_KEY_AUTH_USER_STR							"User"

#define MA_MSGBUS_KEY_AUTH_GROUP_STR						"Group"

#define MA_MSGBUS_KEY_AUTH_PLATFORM_ID_STR					"PlatformID"

#define MA_MSGBUS_VALUE_AUTH_PLATFORMS_STR					"WNTS|WNTW|WXPW|WXPS|WXPHE|WXPE|W2KS|W2KW|WVST|WVSTS|WNT7W|WIN8W|WIN8S|LINUX|MLOS|WRLNX|MAC|SCM|SLR|HPUX|HPIA|AIX|LIFELINE" 

#define MA_MSGBUS_KEY_AUTH_OS_VERSION_STR					"OSVersion"

#define MA_MSGBUS_KEY_AUTH_TECHNOLOGY_STR					"Technology"

#define MA_MSGBUS_VALUE_AUTH_TECH_C_STR						"C"

#define MA_MSGBUS_VALUE_AUTH_TECH_PYTHON_STR				"PYTHON"

#define MA_MSGBUS_VALUE_AUTH_TECH_LUA_STR					"LUA"

#define MA_MSGBUS_VALUE_AUTH_TECH_JAVA_STR					"JAVA"

#define MA_MSGBUS_AUTH_NODE_XML_PATH_STR					"MsgbusAuthManifest/MsgbusAuth"

#define MA_MSGBUS_AUTH_HINTS_ASSIGNMENT_STR					"="


#define MA_MSGBUS_AUTH_RULE_COND_SEPERATOR_STR				"and"

#define MA_MSGBUS_AUTH_RULE_KEY_VALUE_SEPERATOR_STR			"=="

#define MA_MSGBUS_AUTH_RULE_VALUE_SEPERATOR_STR				","








#endif /* MA_MSGBUS_DEFS_H_INCLUED */

