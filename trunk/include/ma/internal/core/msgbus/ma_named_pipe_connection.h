#ifndef MA_NAMED_PIPE_CONNECTION_H_INCLUDED
#define MA_NAMED_PIPE_CONNECTION_H_INCLUDED

#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/core/msgbus/ma_message_consumer.h"


MA_CPP(extern "C" {)

typedef struct ma_named_pipe_connection_s ma_named_pipe_connection_t, *ma_named_pipe_connection_h;

struct ma_message_auth_info_provider_s;
struct ma_msgbus_passphrase_handler_s;
struct ma_msgbus_crypto_provider_s;

/*!
 * Creates a new named pipe connection resource. 
 */
ma_error_t ma_named_pipe_connection_create(ma_event_loop_t *loop, ma_named_pipe_connection_t **connection);

/*!
 * Increments the reference count on a connection
 */
ma_error_t ma_named_pipe_connection_add_ref(ma_named_pipe_connection_t *connection);

/*!
 * Decrements the reference count
 */
ma_error_t ma_named_pipe_connection_release(ma_named_pipe_connection_t *connection);

typedef void (*ma_named_pipe_accept_cb)(ma_named_pipe_connection_t *connection, ma_error_t status, void *cb_data);
/*!
 * Accepts a connection. This connection becomes a server connection
 */
ma_error_t ma_named_pipe_connection_accept(ma_named_pipe_connection_t *connection, struct ma_msgbus_crypto_provider_s *crypto_provider, uv_stream_t *server, ma_named_pipe_accept_cb cb,  void *cb_data);


/*!
 * register a message consumer that will receive messages
 */
ma_error_t ma_named_pipe_connection_set_consumer(ma_named_pipe_connection_t *connection, ma_message_consumer_t *consumer);


/*!
 * Sets logger
 */
ma_error_t ma_named_pipe_connection_set_logger(ma_named_pipe_connection_t *connection, ma_logger_t *logger);

/*!
 * Sets optional auth info provider
 */
ma_error_t ma_named_pipe_connection_set_auth_info_provider(ma_named_pipe_connection_t *connection, struct ma_message_auth_info_provider_s *provider);


/*!
 * Sets pipe name, it's mainly for server accepted connections
 */
ma_error_t ma_named_pipe_connection_set_pipe_name(ma_named_pipe_connection_t *connection, const char *pipe_name);

typedef void (*ma_named_pipe_connect_cb)(ma_named_pipe_connection_t *connection, ma_error_t status, void *cb_data);

/*!
 * Connects to a named pipe server using the specified server name
 */
ma_error_t ma_named_pipe_connection_connect(ma_named_pipe_connection_t *connection, struct ma_msgbus_crypto_provider_s *crypto_provider, const char *name, ma_named_pipe_connect_cb cb, void *cb_data);

/*!
 * sets the passphrase handler
 */
ma_error_t ma_named_pipe_connection_set_passphrase_handler(ma_named_pipe_connection_t *connection, struct ma_msgbus_passphrase_handler_s *passphrase_handler);

/*!
 * starts receiving, will publish messages to the configured message sink
 */

ma_error_t ma_named_pipe_connection_start_receiving(ma_named_pipe_connection_t *connection);

/*!
 * Stops receiving, does not close the connection
 */
ma_error_t ma_named_pipe_connection_stop_receiving(ma_named_pipe_connection_t *connection);

/*!
 * stops all communication and releases the underlying OS resource
 */
ma_error_t ma_named_pipe_connection_close(ma_named_pipe_connection_t *connection, int options);


MA_CPP(})

#endif /* MA_NAMED_PIPE_CONNECTION_H_INCLUDED */
