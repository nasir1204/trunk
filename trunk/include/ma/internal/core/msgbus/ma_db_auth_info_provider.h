#ifndef MA_DB_AUTH_INFO_PROVIDER_H_INCLUDED
#define MA_DB_AUTH_INFO_PROVIDER_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/core/msgbus/ma_message_auth_info_private.h"

typedef struct ma_db_auth_info_provider_s ma_db_auth_info_provider_t, *ma_db_auth_info_provider_h;

struct ma_db_s;

ma_error_t ma_db_auth_info_provider_create(const char *db_path, ma_db_auth_info_provider_h *provider);

ma_error_t ma_db_auth_info_provider_set_logger(ma_db_auth_info_provider_h provider, ma_logger_t *logger);

ma_error_t ma_db_auth_info_provider_set_msgbus_db(ma_db_auth_info_provider_h provider, struct ma_db_s *msgbus_db);

#endif /* MA_DB_AUTH_INFO_PROVIDER_H_INCLUDED */
