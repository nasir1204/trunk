#ifndef MA_MSGBUS_CRYPTO_PROVIDER_H_INLCUDED
#define MA_MSGBUS_CRYPTO_PROVIDER_H_INLCUDED

#include "ma/ma_common.h"

typedef struct ma_msgbus_crypto_provider_s ma_msgbus_crypto_provider_t, *ma_msgbus_crypto_provider_h;
struct ma_crypto_s;
struct ma_temp_buffer_s;


typedef struct ma_msgbus_crypto_provider_functions_s {
    /*!
     * virtual destructor
     */
    void (*destroy)(ma_msgbus_crypto_provider_t *crypto_provider);

	/*!
     * provides hash 256
     */
    ma_error_t (*get_hash256)(ma_msgbus_crypto_provider_t *provider, const unsigned char *buffer, size_t buffer_size, unsigned char digest[32]);

	/*!
     * provides hash 256 from file
     */
    ma_error_t (*get_hash256_file)(ma_msgbus_crypto_provider_t *provider, int fd, unsigned char digest[32]);

	/*!
     * provides cms verification
     */
    ma_error_t (*cms_verify)(ma_msgbus_crypto_provider_t *provider, int cms_fd, struct ma_temp_buffer_s *manifest, struct ma_temp_buffer_s *issuer, struct ma_temp_buffer_s *subject, int *is_verified);

	/*!
     * provides hex decoding
     */
    ma_error_t (*hex_decode)(ma_msgbus_crypto_provider_t *provider, const unsigned char *hex,const unsigned int size,unsigned char *raw,unsigned int max_size);
    
	/*!
     * to get underlying crypto object
     */
    ma_error_t (*get)(ma_msgbus_crypto_provider_t *crypto_provider, struct ma_crypto_s **crypto);

}ma_msgbus_crypto_provider_functions_t;

struct ma_msgbus_crypto_provider_s {
    /*!
     * v-table
     */
    ma_msgbus_crypto_provider_functions_t const *methods;

    /*!
     * some opaque data for derived classes to use
     */
    void *data;
};

/*!
 * initializes (the base of) a crypto provider implementation 
 * this is a convenience function to be used by implemntors 
 */
ma_error_t MA_STATIC_INLINE ma_msgbus_crypto_provider_init(ma_msgbus_crypto_provider_t *crypto_provider, ma_msgbus_crypto_provider_functions_t const *methods, void *data) {
    if (crypto_provider) {
		crypto_provider->methods = methods;
        crypto_provider->data = data;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

void MA_STATIC_INLINE ma_msgbus_crypto_provider_release(ma_msgbus_crypto_provider_t *crypto_provider) {
    if (crypto_provider)
        crypto_provider->methods->destroy(crypto_provider);
}

/*!
 * to get underlying crypto object */
ma_error_t MA_STATIC_INLINE ma_msgbus_crypto_provider_get(ma_msgbus_crypto_provider_t *crypto_provider, struct ma_crypto_s **crypto) {
	return crypto_provider 
		? crypto_provider->methods->get(crypto_provider, crypto)
        : MA_ERROR_INVALIDARG; 
}

ma_error_t MA_STATIC_INLINE ma_msgbus_crypto_provider_get_hash256(ma_msgbus_crypto_provider_t *crypto_provider, const unsigned char *buffer, size_t buffer_size, unsigned char digest[32]) {
	return crypto_provider 
		? crypto_provider->methods->get_hash256(crypto_provider, buffer, buffer_size,digest)
        : MA_ERROR_INVALIDARG; 
}

ma_error_t MA_STATIC_INLINE ma_msgbus_crypto_provider_get_hash256_file(ma_msgbus_crypto_provider_t *crypto_provider, int fd, unsigned char digest[32]) {
	return crypto_provider 
		? crypto_provider->methods->get_hash256_file(crypto_provider, fd, digest)
        : MA_ERROR_INVALIDARG; 
}

ma_error_t MA_STATIC_INLINE ma_msgbus_crypto_provider_cms_verify(ma_msgbus_crypto_provider_t *crypto_provider, int cms_fd, struct ma_temp_buffer_s *manifest, struct ma_temp_buffer_s *issuer, struct ma_temp_buffer_s *subject, int *is_verified) {
	return crypto_provider 
		? crypto_provider->methods->cms_verify(crypto_provider, cms_fd, manifest, issuer, subject, is_verified)
        : MA_ERROR_INVALIDARG; 
}

ma_error_t MA_STATIC_INLINE ma_msgbus_crypto_provider_hex_decode(ma_msgbus_crypto_provider_t *crypto_provider, const unsigned char *hex,const unsigned int size,unsigned char *raw,unsigned int max_size) {
	return crypto_provider 
		? crypto_provider->methods->hex_decode(crypto_provider, hex, size, raw, max_size)
        : MA_ERROR_INVALIDARG; 
}

#endif /* MA_MSGBUS_CRYPTO_PROVIDER_H_INLCUDED */
