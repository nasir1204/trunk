#ifndef MA_MSGBUS_PASSPHRASE_HANDLER_H_INCLUDED
#define MA_MSGBUS_PASSPHRASE_HANDLER_H_INCLUDED

#include "ma/ma_common.h"

typedef struct ma_msgbus_passphrase_handler_s ma_msgbus_passphrase_handler_t, *ma_msgbus_passphrase_handler_h;

typedef struct ma_msgbus_passphrase_handler_functions_s {
    
    /*!
     * virtual destructor
     */
    void (*destroy)(ma_msgbus_passphrase_handler_t *handler);
    
    /*!
     * to acquire passphrase
     */
    ma_error_t (*on_acquire_passphrase)(ma_msgbus_passphrase_handler_t *handler, char product_id[64], unsigned char *passphrase, size_t *passphrase_size);

	/*!
     * to validate passphrase
     */
    ma_error_t (*on_validate_passphrase)(ma_msgbus_passphrase_handler_t *handler, char product_id[64], unsigned char *passphrase, size_t passphrase_size, int *is_validated);

} ma_msgbus_passphrase_handler_functions_t;

struct ma_msgbus_passphrase_handler_s {
    /*!
     * v-table
     */
    ma_msgbus_passphrase_handler_functions_t const *methods;

    /*!
     * some opaque data for derived classes to use
     */
    void *data;
};

/*!
 * initializes (the base of) a pass phrase handler implementation 
 * this is a convenience function to be used by implemntors 
 */
ma_error_t MA_STATIC_INLINE ma_msgbus_passphrase_handler_init(ma_msgbus_passphrase_handler_t *handler, ma_msgbus_passphrase_handler_functions_t const *methods, void *data) {
    if (handler) {
        handler->methods = methods;
        handler->data = data;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

void MA_STATIC_INLINE ma_msgbus_passphrase_handler_release(ma_msgbus_passphrase_handler_t *handler) {
    if (handler)
        handler->methods->destroy(handler);

}

/*! 
 * acquires the pass phrase from the passphrase handler
 */
ma_error_t MA_STATIC_INLINE ma_msgbus_passphrase_handler_acquire_passphrase(ma_msgbus_passphrase_handler_t *handler, char product_id[64], unsigned char *passphrase, size_t *passphrase_size) {
    return handler 
		? handler->methods->on_acquire_passphrase(handler, product_id, passphrase, passphrase_size)
        : MA_ERROR_INVALIDARG; 
}

/*! 
 * validates the pass phrase from the passphrase handler
*/
ma_error_t MA_STATIC_INLINE ma_msgbus_passphrase_handler_validate_passphrase(ma_msgbus_passphrase_handler_t *handler, char product_id[64], unsigned char *passphrase, size_t passphrase_size, int *is_validated) {
    return handler 
		? handler->methods->on_validate_passphrase(handler, product_id, passphrase, passphrase_size, is_validated)
        : MA_ERROR_INVALIDARG; 
}

#endif /* MA_MSGBUS_PASSPHRASE_HANDLER_H_INCLUDED */
