#ifndef MA_EP_CONNECT_REQUEST_H_INCLUDED
#define MA_EP_CONNECT_REQUEST_H_INCLUDED

#include "ma/ma_common.h"

MA_CPP(extern "C" {)
/* It is a consumer so feel free to use generic API's for incrementing ref and decrementing ref */

typedef struct ma_ep_connect_request_s ma_ep_connect_request_t , *ma_ep_connect_request_h;


/* This endpoint name can be service name or pipe name */
ma_error_t ma_ep_connect_request_create(const char *endpoint_name ,  ma_ep_connect_request_t **request);

/*Just decrement the ref count */
ma_error_t ma_ep_connect_request_dec_ref(ma_ep_connect_request_t *self) ;

ma_error_t ma_ep_connect_request_inc_ref(ma_ep_connect_request_t *self) ;

ma_error_t ma_ep_connect_request_start(ma_ep_connect_request_t *request);

MA_CPP(})

#endif /* MA_EP_CONNECT_REQUEST_H_INCLUDED */

