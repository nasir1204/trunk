#ifndef MA_NAMED_PIPE_CREDENTIALS_H_INCLUDED
#define MA_NAMED_PIPE_CREDENTIALS_H_INCLUDED

#include "ma/ma_common.h"

#define LOG_FACILITY_NAME "msgbus"

MA_CPP(extern "C" {)

#if defined(MA_WINDOWS)
typedef unsigned long       ma_pid_t;
typedef unsigned long       ma_uid_t;
typedef unsigned long       ma_gid_t;
typedef void                *ma_pipe_t;
#else
#include <sys/types.h>
#include <unistd.h>
typedef pid_t               ma_pid_t;
typedef uid_t               ma_uid_t;
typedef gid_t               ma_gid_t;
typedef int                 ma_pipe_t;
#endif /* MA_WINDOWS */

ma_error_t ma_named_pipe_credentials_get(ma_pipe_t peer_handle, ma_pid_t *pid, ma_uid_t *uid, ma_gid_t *gid);

MA_CPP(})

#endif /* MA_NAMED_PIPE_CREDENTIALS_H_INCLUDED */ 
