#ifndef MA_MSGBUS_PRIVATE_H_INCLUDED
#define MA_MSGBUS_PRIVATE_H_INCLUDED

#include "ma/ma_msgbus.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/utils/event_loop/ma_loop_worker.h"
#include "ma/internal/core/msgbus/ma_message_router.h"
#include "ma/internal/core/msgbus/ma_named_pipe_connection_manager.h"
#include "ma/internal/core/msgbus/ma_local_broker.h"
#include "ma/internal/utils/database/ma_db.h"

#ifndef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "msgbus"
#endif

#if (9 < UV_VERSION_MINOR)
# define UV_AFTER_WORK_PARAMS , int status
#else
# define UV_AFTER_WORK_PARAMS
#endif

#define MSGBUSOPTTYPE_MASK								0x0ffff
#define MSGBUSOPTTYPE_TYPEMASK							0xf0000



MA_CPP(extern "C" {)

struct ma_message_auth_info_provider_s;

struct ma_msgbus_crypto_provider_s ;

struct ma_crypto_s ;
 /*!
 * gets the product id.
 */
const char *ma_msgbus_get_product_id(ma_msgbus_t *self);

/*!
 * gets the router
 */
ma_message_router_t *ma_msgbus_get_message_router(ma_msgbus_t *msgbus);

/*!
 * gets the local broker
 */
ma_local_broker_t *ma_msgbus_get_local_broker(ma_msgbus_t *msgbus);

/*!
 * gets the named pipe connection manager
 */
ma_named_pipe_connection_manager_t *ma_msgbus_get_named_pipe_connection_manager(ma_msgbus_t *msgbus);

/*!
 * sets the crypto object
 */
MA_MSGBUS_API ma_error_t ma_msgbus_set_crypto(ma_msgbus_t *msgbus, struct ma_crypto_s *crypto);

/*!
 * sets the msgbus db
 */
MA_MSGBUS_API ma_error_t ma_msgbus_set_msgbus_db(ma_msgbus_t *msgbus, ma_db_t *msgbus_db);


struct ma_event_loop_s;
/*!
 * gets the contained event loop. It is exported for MA internal usage.
 */
MA_MSGBUS_API struct ma_event_loop_s *ma_msgbus_get_event_loop(ma_msgbus_t *msgbus);

struct ma_loop_worker_s;
/*!
 * gets the contained loop worker. It is exported for MA internal usage.
 */
MA_MSGBUS_API struct ma_loop_worker_s *ma_msgbus_get_loop_worker(ma_msgbus_t *msgbus);

/*!
 * gets msgbus conf ini file.
 */
MA_MSGBUS_API const char *ma_msgbus_get_conf_file(ma_msgbus_t *self);

/*!
 * gets the logger
 */
ma_logger_t *ma_msgbus_get_logger(ma_msgbus_t *msgbus);

/*!
 * gets the optional auth info provider
 */
struct ma_message_auth_info_provider_s *ma_msgbus_get_auth_info_provider(ma_msgbus_t *msgbus);

/*!
 * gets the optional passphrase provider
 */
struct ma_msgbus_passphrase_handler_s  *ma_msgbus_get_passphrase_handler(ma_msgbus_t *msgbus);

struct ma_msgbus_crypto_provider_s  *ma_msgbus_get_crypto_provider(ma_msgbus_t *msgbus);

/* Return char*  should NOT be freed by the user */
/*char *ma_msgbus_get_broker_service_name(ma_msgbus_t *msgbus);
char *ma_msgbus_get_name_service_name(ma_msgbus_t *msgbus);
char *ma_msgbus_get_pubsub_service_name(ma_msgbus_t *msgbus);
*/

/* Return long*/
ma_int64_t  ma_msgbus_get_broker_pid(ma_msgbus_t *msgbus);

/* Return char*  should NOT be freed by the user */
const char *ma_msgbus_get_broker_pipe_name(ma_msgbus_t *msgbus);

/* check msgbus is started in single thread or not */
ma_bool_t	ma_msgbus_has_single_thread(ma_msgbus_t *msgbus);

/* This key is used to send the pipe name in signal message on conneciton termination */
#define MA_MSGBUS_PEER_END_TERMINATION_PUB_SUB_TOPIC    "ma.msgbus.peer_end_termination"
#define MA_PROP_KEY_PIPE_NAME_STR	                    "prop.key.pipe_name"
#define MA_PROP_KEY_PEER_PID_STR	                    "prop.key.peer_pid"

MA_CPP(})

#endif /* MA_MSGBUS_PRIVATE_H_INCLUDED */ 
