#ifndef MA_EP_LOOKUP_H_INCLUDED
#define MA_EP_LOOKUP_H_INCLUDED

#include "ma/ma_msgbus.h"
#include "ma/internal/core/msgbus/ma_local_broker.h"

MA_CPP(extern "C" {)

typedef struct ma_ep_lookup_s ma_ep_lookup_t, *ma_ep_lookup_h;

typedef void (*ep_lookup_cb)(ma_error_t status, ma_message_consumer_t *consumer, void *ep_cookie, void *cb_data);

ma_error_t ma_ep_lookup_start(ma_msgbus_t *mbus, const char *ep_name, ma_msgbus_consumer_reachability_t lookup_reach, ep_lookup_cb cb, void *cb_data);

ma_error_t ma_ep_lookup_stop(ma_ep_lookup_t *ep_lookup, void *ep_cookie);

#endif /* MA_EP_LOOKUP_H_INCLUDED */

