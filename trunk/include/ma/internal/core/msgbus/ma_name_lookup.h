#ifndef MA_NAME_LOOKUP_H_INCLUDED
#define MA_NAME_LOOKUP_H_INCLUDED

#include "ma/internal/core/msgbus/ma_msgbus_impl.h"
#include "ma/internal/core/msgbus/ma_message_consumer.h"

MA_CPP(extern "C" {)
	
typedef struct ma_name_lookup_s ma_name_lookup_t, *ma_name_lookup_h;

typedef struct ma_name_lookup_info_s {
    char        *endpoint_name;
	long		pid;
	char		*pipe_name ;
	char		*host_name ;
}ma_name_lookup_info_t ;

/*!
 * Creates a new name lookup object
 */
ma_error_t ma_name_lookup_create(ma_msgbus_t *bus , ma_name_lookup_t **lookup);

/*!
 * Releases the name lookup object
 */
ma_error_t ma_name_lookup_release(ma_name_lookup_t *lookup);



/*Incremented ref counted consumer if already found , else NULL */
typedef void (*ma_name_lookup_start_cb)(ma_name_lookup_t *lookup, ma_error_t status, ma_message_consumer_t *consumer ,void *cb_data);
/*!
 * Starts the lookup and replies with lookup info which contains the information about what it got
 */
ma_error_t ma_name_lookup_start(ma_name_lookup_t *lookup , ma_msgbus_consumer_reachability_t lookup_reach ,  const char *endpoint_name, ma_name_lookup_start_cb cb, void *cb_data);

/*!
 * Retrieves the looked up information
 */
ma_name_lookup_info_t *ma_name_lookup_get_info(ma_name_lookup_t *lookup);

/*!
 * Stops looking up for the particular endpoint
 */
ma_error_t ma_name_lookup_stop(ma_name_lookup_t *lookup);




MA_CPP(})

#endif /* MA_NAME_LOOKUP_H_INCLUDED */
