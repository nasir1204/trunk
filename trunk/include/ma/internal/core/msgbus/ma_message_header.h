/** @file ma_message_header.h
 *  @brief Message headers for message
 *
*/
#ifndef MA_MESSAGE_HEADER_H_INCLUDED
#define MA_MESSAGE_HEADER_H_INCLUDED

#include "ma/ma_common.h"

MA_CPP(extern "C" {)

/* TBD - Will check the communication between applications on little endian and big endian.
       - Will check the communication between 16bit(if any embedded devices ??) and 32bit/64bit applications
*/

/* TBD - In future, the message structure will be changed from C structure to msgpack object,
         so that �message� can be used in any language and between different CPU architectures using msgpack
*/

typedef struct ma_message_address_name_s {
    ma_uint32_t     text_length; // changing the type from size_t to ma_uint64_t to achieve successful communication between 32 bit and 64 bit applications
    ma_uint32_t     dummy;       // It is added for padding.
    char            *text;
} ma_message_address_name_t;

typedef struct ma_message_address_s {
    /*! service name */
    ma_message_address_name_t   service_name;

    /*! service process id, or 0 for in-process  */
    ma_uint64_t                 pid;   // changing the type from long to ma_uint64_t to achieve successful communication between 32 bit and 64 bit applications

    /*! host name (for remote message passing) */
    ma_message_address_name_t   host_name;
} ma_message_address_t;

/*TODO- Optimize it later , padding etc */
typedef struct ma_message_header_s {
    ma_uint8_t	version ;
    ma_uint8_t	header_length ;
    ma_uint8_t	message_flags ; //lower 4 bits are message type and upper 4 bits for message sub type
    ma_uint8_t	reserved1 ; //May be this can be used for subject
    ma_uint32_t	correlation_id ;
    ma_uint32_t	timestamp ;
    ma_uint16_t	status ;
    ma_uint16_t	reserved2 ;

    ma_message_address_t source;

    ma_message_address_t destination;

} ma_message_header_t;

#define TYPECAST(addr) ((ma_message_header_t *)addr)

/*!  Get header macros */
#define GET_MESSAGE_VERSION(addr)					TYPECAST(addr)->version
#define GET_MESSAGE_HEADER_LEN(addr)				sizeof(ma_message_header_t)

#define GET_MESSAGE_TYPE(addr)						(TYPECAST(addr)->message_flags & 0x0F)
#define GET_MESSAGE_SUB_TYPE(addr)					(TYPECAST(addr)->message_flags >> 4)

#define GET_MESSAGE_CORRELATION_ID(addr)			TYPECAST(addr)->correlation_id
#define GET_MESSAGE_TIME(addr)						TYPECAST(addr)->timestamp
#define GET_MESSAGE_STATUS(addr)					TYPECAST(addr)->status

// Source get macros
#define GET_MESSAGE_SOURCE_NAME(addr)				TYPECAST(addr)->source.service_name.text
#define GET_MESSAGE_SOURCE_NAME_LEN(addr)			TYPECAST(addr)->source.service_name.text_length
#define GET_MESSAGE_SOURCE_PID(addr)				TYPECAST(addr)->source.pid
#define GET_MESSAGE_SOURCE_HOST_NAME(addr)			TYPECAST(addr)->source.host_name.text
#define GET_MESSAGE_SOURCE_HOST_NAME_LEN(addr)		TYPECAST(addr)->source.host_name.text_length

// Destination get macros
#define GET_MESSAGE_DESTINATION_NAME(addr)			TYPECAST(addr)->destination.service_name.text
#define GET_MESSAGE_DESTINATION_NAME_LEN(addr)		TYPECAST(addr)->destination.service_name.text_length
#define GET_MESSAGE_DESTINATION_PID(addr)			TYPECAST(addr)->destination.pid
#define GET_MESSAGE_DESTINATION_HOST_NAME(addr)		TYPECAST(addr)->destination.host_name.text
#define GET_MESSAGE_DESTINATION_HOST_NAME_LEN(addr)	TYPECAST(addr)->destination.host_name.text_length


/*!  Set header macros */
#define SET_MESSAGE_VERSION(addr,x)					(TYPECAST(addr)->version		= x)
#define SET_MESSAGE_HEADER_LEN(addr)				(TYPECAST(addr)->header_length	= sizeof(ma_message_header_t) )

#define SET_MESSAGE_TYPE(addr, x)					(TYPECAST(addr)->message_flags = ((TYPECAST(addr)->message_flags) & 0xF0) | x)
#define SET_MESSAGE_SUB_TYPE(addr, x)				(TYPECAST(addr)->message_flags = ((TYPECAST(addr)->message_flags) & 0x0F) | x << 4)

#define SET_MESSAGE_CORRELATION_ID(addr, x)			(TYPECAST(addr)->correlation_id	= x)
#define SET_MESSAGE_TIME(addr, x)					(TYPECAST(addr)->timestamp		= x)
#define SET_MESSAGE_STATUS(addr, x)					(TYPECAST(addr)->status			= x)

// Source set macros
#define SET_MESSAGE_SOURCE_NAME(addr, x)			(TYPECAST(addr)->source.service_name.text	= (x)?strdup(x):0, TYPECAST(addr)->source.service_name.text_length = (ma_uint32_t)((x) ? 1+strlen(x) : 0))
#define SET_MESSAGE_SOURCE_PID(addr, x)				(TYPECAST(addr)->source.pid					= x)
#define SET_MESSAGE_SOURCE_HOST_NAME(addr, x)		(TYPECAST(addr)->source.host_name.text		= (x)?strdup(x):0, TYPECAST(addr)->source.host_name.text_length = (ma_uint32_t)((x) ? 1+strlen(x) : 0))


// Destination set macros
#define SET_MESSAGE_DESTINATION_NAME(addr, x)       (TYPECAST(addr)->destination.service_name.text	= (x)?strdup(x):0, TYPECAST(addr)->destination.service_name.text_length = (ma_uint32_t)((x) ? 1+strlen(x) : 0))
#define SET_MESSAGE_DESTINATION_PID(addr, x)		(TYPECAST(addr)->destination.pid				= x)
#define SET_MESSAGE_DESTINATION_HOST_NAME(addr, x)	(TYPECAST(addr)->destination.host_name.text		= (x)?strdup(x):0, TYPECAST(addr)->destination.host_name.text_length = (ma_uint32_t)((x) ? 1+strlen(x) : 0))

/* Property for publish/subscribe  , anwyays we need to think better about properties*/
#define MA_MSG_PUBLISH_TOPIC				"MA_MSG_PUBLISH_TOPIC"

MA_CPP(})


#endif /* MA_MESSAGE_HEADER_H_INCLUDED */
