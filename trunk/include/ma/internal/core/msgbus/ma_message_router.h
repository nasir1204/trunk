#ifndef MA_MESSAGE_ROUTER_H_INCLUDED
#define MA_MESSAGE_ROUTER_H_INCLUDED

#include "ma/internal/core/msgbus/ma_message_consumer.h"

/* Declaration of ma_message_router - a message consumer implementation that forwards messages to a set of registered message consumers (fan-out pattern)  */

MA_CPP(extern "C" {)

typedef struct ma_message_router_s ma_message_router_t, *ma_message_router_h;

/* Consumer type */
typedef enum ma_message_consumer_type_e {
    MA_MSGBUS_SERVER    =   0,   
    MA_MSGBUS_CLIENT,
    MA_MSGBUS_SUBSCRIBER,
    MA_MSGBUS_NAMED_PIPE_CONNECTION,
    MA_MSGBUS_PROXY_SERVER,         /* TBD - This type is proxy of the outproc server named pipe connection. this will be changed in future.. */
    MA_MSGBUS_LOCAL_BROKER,         /* New type added for specific to local broker. is this ok??? */
    MA_MSGBUS_UNKNOWN,
} ma_message_consumer_type_t;

ma_error_t ma_message_router_create(ma_message_router_t **router);

ma_error_t ma_message_router_release(ma_message_router_t *router);

ma_error_t ma_message_router_set_logger(ma_message_router_t *router, ma_logger_t *logger);

//ma_error_t ma_message_router_<>(ma_message_router_t *router);


/*! 
 * Adds a message consumer to the router. The same consumer can be added multiple times (not sure who would do that)
 * @param router        Handle to an instance of the message router
 * @param consumer      Handle to the consumer to be registered
 * @param consumer_name Name of the consumer, it may be NULL for consumer type MA_MSGBUS_CLIENT
 * @param consumer_type Type of the consumer, types are classified in ma_message_consumer_type_t
 * @return MA_OK if the consumer was successfully added, 
 *         MA_INVALIDARG if any of the input arguments are invalid (e.g. NULL) 
 *         MA_ERROR_OUTOFMEMORY if there is not enough memory
 */
ma_error_t ma_message_router_add_consumer(ma_message_router_t *router, ma_message_consumer_t *consumer, const char *consumer_name, ma_message_consumer_type_t consumer_type);

/*!
 * Removes a previously registered consumer. If a consumer has been added multiple times, this function will only remove the first registered consumer
 * @param router    Handle to the message router
 * @param consumer  Handle to the consumer to be removed
 * @return MA_OK if the requested consumer was successfully removed, otherwise MA_ERROR_INVALIDARG
 */
ma_error_t ma_message_router_remove_consumer(ma_message_router_t *router, ma_message_consumer_t *consumer);

/*!
 * Search consumer handle based on consumer name. If a consumer has been added multiple times with same name, this function will only returns the first registered consumer
 * @param router            Handle to the message router
 * @param consumer_name     Name of the consumer
 * @param lookup_reach      reachability of server
 * @param consumer_name     Output type of consumer
 * @param endpoint_consumer Output parameter to pass the consumer object if it is avaiable.
 * @return MA_OK if the requested consumer is available, otherwise MA_ERROR_MSGBUS_SERVICE_NOT_FOUND.
 */
ma_error_t ma_message_router_search_consumer(ma_message_router_t *router, const char *consumer_name, ma_msgbus_consumer_reachability_t lookup_reach, ma_message_consumer_type_t *consumer_type, ma_message_consumer_t **endpoint_consumer);


void ma_message_router_dump_consumers(ma_message_router_t *router);

MA_CPP(})


#endif /* MA_MESSAGE_ROUTER_H_INCLUDED */

