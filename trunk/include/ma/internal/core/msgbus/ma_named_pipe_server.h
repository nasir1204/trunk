#ifndef MA_NAMED_PIPE_SERVER_H_INCLUDED
#define MA_NAMED_PIPE_SERVER_H_INCLUDED

#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/core/msgbus/ma_named_pipe_connection.h"


MA_CPP(extern "C" {)

typedef struct ma_named_pipe_server_s ma_named_pipe_server_t, *ma_named_pipe_server_h;
struct ma_message_auth_info_provider_s;
struct ma_msgbus_passphrase_handler_s;
struct ma_msgbus_crypto_provider_s;


/*!
 * Creates a new named pipe server.
 */
ma_error_t ma_named_pipe_server_create(ma_event_loop_t *loop, ma_named_pipe_server_t **pipe_server);

/*!
 * Releases the named pipe server and any associated resources
 */
ma_error_t ma_named_pipe_server_release(ma_named_pipe_server_t *pipe_server);

/*!
 * Callback signature for new incoming connections
 */
typedef void (*ma_named_pipe_server_connection_cb)(ma_named_pipe_connection_t *connection, void *cb_data);

ma_error_t ma_named_pipe_server_start(ma_named_pipe_server_t *pipe_server, struct ma_msgbus_crypto_provider_s *crypto_provider, const char *pipe_name, ma_named_pipe_server_connection_cb connection_cb, void *cb_data);

/*!
 * Stops the named pipe server and pulls it out of the event loop
 */
ma_error_t ma_named_pipe_server_stop(ma_named_pipe_server_t *pipe_server);


/*!
 * Retrieves the full pipe name. Only valid after started
 * @param pipe_server A handle to the pipe server 
 # @param pipe_name address of a string that receives the pipe_name. Do not free this memory, it is owned by the pipe_server
 */

ma_error_t ma_named_pipe_server_get_pipe_name(ma_named_pipe_server_t *pipe_server, const char **pipe_name);

ma_error_t ma_named_pipe_server_set_logger(ma_named_pipe_server_t *pipe_server, ma_logger_t *logger);

ma_error_t ma_named_pipe_server_set_auth_info_provider(ma_named_pipe_server_t *pipe_server, struct ma_message_auth_info_provider_s *provider);

/*!
 * sets the passphrase handler
 */
ma_error_t ma_named_pipe_server_set_passphrase_handler(ma_named_pipe_server_t *pipe_server, struct ma_msgbus_passphrase_handler_s *passphrase_handler);
MA_CPP(})

#endif /* MA_NAMED_PIPE_SERVER_H_INCLUDED */
