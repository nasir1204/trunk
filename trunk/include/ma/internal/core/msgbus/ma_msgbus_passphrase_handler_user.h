#ifndef MA_MSGBUS_PASSPHRASE_HANDLER_USER_H_INCLUDED
#define MA_MSGBUS_PASSPHRASE_HANDLER_USER_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_log.h"
#include "ma/internal/core/msgbus/ma_msgbus_passphrase_handler.h"

typedef struct ma_msgbus_passphrase_handler_user_s ma_msgbus_passphrase_handler_user_t, *ma_msgbus_passphrase_handler_user_h;

ma_error_t ma_msgbus_passphrase_handler_user_create(ma_msgbus_t *msgbus, ma_msgbus_passphrase_handler_user_t **passphrase_handler);

ma_error_t ma_msgbus_passphrase_handler_user_set_passphrase_callback(ma_msgbus_passphrase_handler_user_t *passphrase_handler, ma_msgbus_passphrase_provider_callback_t cb, void *cb_data);

ma_error_t ma_msgbus_passphrase_handler_user_set_logger(ma_msgbus_passphrase_handler_user_t *passphrase_handler, ma_logger_t *logger);

#endif /* MA_MSGBUS_PASSPHRASE_HANDLER_USER_H_INCLUDED */
