#ifndef MA_NAMED_PIPE_CONNECTION_MANAGER_H_INCLUDED
#define MA_NAMED_PIPE_CONNECTION_MANAGER_H_INCLUDED

#include "ma/ma_common.h"

#include "ma/internal/core/msgbus/ma_named_pipe_connection.h"


MA_CPP(extern "C" {)

typedef struct ma_named_pipe_connection_manager_s ma_named_pipe_connection_manager_t, *ma_named_pipe_connection_manager_h;

ma_error_t ma_named_pipe_connection_manager_create(ma_named_pipe_connection_manager_t **conn_mgr);

ma_error_t ma_named_pipe_connection_manager_release(ma_named_pipe_connection_manager_t *conn_mgr);

ma_error_t ma_named_pipe_connection_manager_add(ma_named_pipe_connection_manager_t *conn_mgr, const char *pipe_name , ma_named_pipe_connection_t *connection);

ma_error_t ma_named_pipe_connection_manager_search(ma_named_pipe_connection_manager_t *conn_mgr, const char *pipe_name ,  ma_message_consumer_t **consumer);

ma_error_t ma_named_pipe_connection_manager_remove(ma_named_pipe_connection_manager_t *conn_mgr, ma_named_pipe_connection_t *connection);

ma_error_t ma_named_pipe_connection_manager_close_all(ma_named_pipe_connection_manager_t *conn_mgr);

ma_error_t ma_named_pipe_connection_manager_set_logger(ma_named_pipe_connection_manager_t *conn_mgr, ma_logger_t *logger);


MA_CPP(})

#endif

