#ifndef MA_MESSAGE_PRIVATE_H_INLCUDED
#define MA_MESSAGE_PRIVATE_H_INLCUDED

#include "ma/ma_message.h"

MA_CPP(extern "C" {)

ma_error_t ma_message_set_auth_info(ma_message_t *message, ma_message_auth_info_t *auth_info);

MA_MSGBUS_API ma_error_t ma_message_as_network_variant(ma_message_t *self, ma_variant_t **variant_value);
MA_MSGBUS_API ma_error_t ma_message_from_network_variant(ma_variant_t *var_obj , ma_message_t **message);

MA_CPP(})

#endif /* MA_MESSAGE_PRIVATE_H_INLCUDED */
