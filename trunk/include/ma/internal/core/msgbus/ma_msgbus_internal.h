#ifndef MA_MSGBUS_INTERNAL_H_INCLUDED
#define MA_MSGBUS_INTERNAL_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_msgbus.h"

MA_CPP(extern "C" {)

MA_MSGBUS_API ma_error_t ma_msgbus_disable_watcher(ma_msgbus_t *msgbus);

MA_CPP(})

#endif /* MA_MSGBUS_INTERNAL_H_INCLUDED */ 
