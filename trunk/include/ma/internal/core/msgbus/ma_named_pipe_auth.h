#ifndef MA_NAMED_PIPE_AUTH_H_INCLUDED
#define MA_NAMED_PIPE_AUTH_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_log.h"
#include "ma/ma_message_auth_info.h"
#include <uv.h>

MA_CPP(extern "C" {)
typedef struct ma_named_pipe_auth_s ma_named_pipe_auth_t, *ma_named_pipe_auth_h;
struct ma_msgbus_passphrase_handler_s;
struct ma_msgbus_crypto_provider_s;

typedef enum trust_level_e {
    TRUST_LEVEL_UNDETERMINED = 0, /* not tried yet */
    TRUST_LEVEL_UNKNOWN, /* tried, but couldn't figure out */
    TRUST_LEVEL_UNTRUSTED,
    TRUST_LEVEL_TRUSTED,
}trust_level_t;

ma_error_t ma_named_pipe_auth_create(uv_loop_t *uv_loop, struct ma_msgbus_crypto_provider_s *crypto_provider,  uv_pipe_t *pipe, ma_named_pipe_auth_t **pipe_auth);

ma_error_t ma_named_pipe_auth_add_ref(ma_named_pipe_auth_t *pipe_auth);

ma_error_t ma_named_pipe_auth_release(ma_named_pipe_auth_t *pipe_auth);

ma_error_t ma_named_pipe_auth_set_logger(ma_named_pipe_auth_t *pipe_auth, ma_logger_t *logger);

ma_error_t ma_named_pipe_auth_set_server(ma_named_pipe_auth_t *self) ;

ma_error_t ma_named_pipe_auth_set_passphrase_handler(ma_named_pipe_auth_t *pipe_auth, struct ma_msgbus_passphrase_handler_s *passphrase_handler);

typedef void (*ma_named_pipe_on_authenticate_cb)(ma_named_pipe_auth_t *pipe_auth, ma_error_t status, void *cb_data, ma_message_auth_info_t *peer_auth_info);
/*!
 * Performs the authentication and returns the result, this objects can be used to get more information about the other end
 */
ma_error_t ma_named_pipe_auth_start(ma_named_pipe_auth_t *pipe_auth, long timeout_in_milliseconds, ma_named_pipe_on_authenticate_cb cb, void *cb_data);

ma_error_t ma_named_pipe_auth_stop(ma_named_pipe_auth_t *pipe_auth);

MA_CPP(})

#endif /* MA_NAMED_PIPE_AUTH_H_INCLUDED */
