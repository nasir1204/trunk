#ifndef MA_MESSAGE_CONSUMER_H_INCLUDED
#define MA_MESSAGE_CONSUMER_H_INCLUDED

#include "ma/ma_message.h"
#include "ma/internal/utils/threading/ma_atomic.h"

MA_CPP(extern "C" {)

typedef struct ma_message_consumer_s ma_message_consumer_t, *ma_message_consumer_h;

static ma_error_t ma_message_consumer_send(ma_message_consumer_t *consumer, ma_message_t *msg);

static void ma_message_consumer_release(ma_message_consumer_t *consumer);

/* TODO: Should this be reference counted instead?  */

/* Defines the C equivalent of a 'v-table' for the ma_message_consumer interface implementor */
typedef struct ma_message_consumer_methods_s {
    ma_error_t (*on_message)(ma_message_consumer_t *self, ma_message_t *msg);
    void (*destroy)(ma_message_consumer_t *self); /*< virtual destructor */
} ma_message_consumer_methods_t;

/* 'Base' class all message consumers should derive from
 * Alternatively this structure can be used directly and the data field can be used to hold custom data 
 */
struct ma_message_consumer_s {
    ma_message_consumer_methods_t   const *methods;   /*< v-table */
    ma_atomic_counter_t             ref_count;
    void                            *data;      /*< custom data, available for implementors */
};

/*!
 * Helper function for derived classes to use for initialization
 */
static MA_INLINE void ma_message_consumer_init(ma_message_consumer_t *consumer, ma_message_consumer_methods_t  const *methods, int ref_count, void *data) {
    consumer->methods = methods;
    consumer->ref_count = ref_count;
    consumer->data = data;    
} 

/*!
 * increments the reference count
 */
ma_error_t ma_message_consumer_inc_ref(ma_message_consumer_t *consumer);

/*!
 * decrements ref count. If count reaches 0, will call derived class' destroy method which must clean up and free any memory 
 */
ma_error_t ma_message_consumer_dec_ref(ma_message_consumer_t *consumer);




static MA_INLINE ma_error_t ma_message_consumer_send(ma_message_consumer_t *consumer, ma_message_t *msg) {
    return (consumer)
        ? consumer->methods->on_message(consumer, msg)
        : MA_ERROR_PRECONDITION;
}

static MA_INLINE void ma_message_consumer_release(ma_message_consumer_t *consumer) {
    ma_message_consumer_dec_ref(consumer);
}


MA_CPP(})

#endif /* MA_MESSAGE_CONSUMER_H_INCLUDED  */

