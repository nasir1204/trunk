#ifndef MA_MSGBUS_AUTH_RULE_H_INCLUDED
#define MA_MSGBUS_AUTH_RULE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_variant.h"
#include "ma/ma_log.h"

/* 
Sample Auth rules

"AuthType==ExecutableValidation,ChallengeResponse and User==root,user1,user2 and Group==root,system and PlatformID==MLOS,ANDROID"

- No space in (key==value) pair
- A single space before and after "and"

"AuthType==ExecutableValidation and AuthType==ChallengeResponse and User==root and User==user1 and Group==root and Group == system and PlatformID==MLOS and PlatformID==ANDROID"

"AuthType==ExecutableValidation and AuthType==ChallengeResponse and User==root,user1 and Group==root,system and PlatformID==MLOS and PlatformID==ANDROID"
*/

typedef struct ma_msgbus_auth_rule_s ma_msgbus_auth_rule_t, *ma_msgbus_auth_rule_h;

typedef enum ma_msgbus_auth_conditions_e {
    MA_AUTH_COND_NONE = 0x0 ,
    MA_AUTH_COND_EXECUTABLE_VALIDATION = 0x2,
    MA_AUTH_COND_CHALLENGE_AND_RESPONSE = 0x4,
	MA_AUTH_COND_PLATFORM_VALIDATION = 0x8,
	MA_AUTH_COND_USER_VALIDATION = 0x10,
	MA_AUTH_COND_GROUP_VALIDATION = 0x20,
} ma_msgbus_auth_conditions_t ;

ma_error_t ma_msgbus_auth_rule_create(const char *rule, ma_msgbus_auth_rule_t **auth_rule);

ma_error_t ma_msgbus_auth_rule_release(ma_msgbus_auth_rule_t *auth_rule);

ma_error_t ma_msgbus_auth_rule_set_logger(ma_msgbus_auth_rule_t *auth_rule, ma_logger_t *logger);

ma_error_t ma_msgbus_auth_rule_get_auth_conditions(ma_msgbus_auth_rule_t *auth_rule, ma_uint16_t *auth_conditions);

ma_error_t ma_msgbus_auth_rule_get_platforms(ma_msgbus_auth_rule_t *auth_rule, ma_array_t **platforms_list);

ma_error_t ma_msgbus_auth_rule_get_users(ma_msgbus_auth_rule_t *auth_rule, ma_array_t **users_list);

ma_error_t ma_msgbus_auth_rule_get_groups(ma_msgbus_auth_rule_t *auth_rule, ma_array_t **groups_list);

#endif /* MA_MSGBUS_AUTH_RULE_H_INCLUDED */
