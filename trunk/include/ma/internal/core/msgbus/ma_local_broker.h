#ifndef MA_LOCAL_BROKER_H_INCLUDED
#define MA_LOCAL_BROKER_H_INCLUDED

#include "ma/ma_msgbus.h"
#include "ma/internal/core/msgbus/ma_message_consumer.h"
#include "ma/internal/core/msgbus/ma_message_router.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"

MA_CPP(extern "C" {)

typedef struct ma_local_broker_s ma_local_broker_t, *ma_local_broker_h;

ma_error_t ma_local_broker_create(ma_event_loop_t *ma_loop, ma_message_router_t *router, ma_local_broker_t **broker);

ma_error_t ma_local_broker_start(ma_msgbus_t *mbus, ma_local_broker_t *broker);

ma_error_t ma_local_broker_stop(ma_local_broker_t *broker);

ma_error_t ma_local_broker_release(ma_local_broker_t *broker);

ma_error_t ma_local_broker_set_logger(ma_local_broker_t *broker, ma_logger_t *logger);

ma_error_t ma_local_broker_check_registrations(ma_local_broker_t *broker);

/*  ep_name -> service name for Serivces and topic filter for Subscribers 
	is_service -> True for services and False for Subscribers    
 */
ma_error_t ma_local_broker_consumer_register(ma_local_broker_t *broker, ma_message_consumer_t *ep_consumer, const char *ep_name , ma_msgbus_consumer_reachability_t reachability, ma_bool_t is_service);

ma_error_t ma_local_broker_consumer_unregister(ma_local_broker_t *broker , ma_message_consumer_t *ep_consumer, const char *ep_name, ma_msgbus_consumer_reachability_t reachability, ma_bool_t is_service);

MA_CPP(})

#endif /* MA_LOCAL_BROKER_H_INCLUDED */

