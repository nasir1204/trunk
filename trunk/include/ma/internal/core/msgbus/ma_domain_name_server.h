#ifndef MA_DOMAIN_NAME_SERVER_H_INCLUDED
#define MA_DOMAIN_NAME_SERVER_H_INCLUDED

#include "ma/internal/core/msgbus/ma_msgbus_impl.h"


MA_CPP(extern "C" {)
	
typedef struct ma_dns_s ma_dns_t , *ma_dns_h;

typedef struct ma_lookup_entry_s ma_lookup_entry_t , *ma_lookup_entry_h;

 //reference counted
typedef struct ma_resolve_entry_s ma_resolve_entry_t , *ma_resolve_entry_h; 

ma_error_t ma_dns_create(ma_msgbus_t *bus , ma_dns_t **dns);

ma_error_t ma_dns_release(ma_dns_t *dns);

typedef void (*ma_dns_lookup_cb)(ma_dns_t *dns, int status, ma_lookup_entry_t *lookup_entry  ,  void *cb_data);
ma_error_t ma_dns_lookup(ma_dns_t *dns ,  ma_msgbus_consumer_reachability_t lookup_reach ,  const char *endpoint_name, ma_dns_lookup_cb cb , void *cb_data);
ma_error_t ma_dns_lookup_stop(ma_dns_t *dns , const char *endpoint_name);	


typedef void (*ma_dns_resolve_cb)(ma_dns_t *dns, int status , ma_lookup_entry_t *lookup_entry , ma_resolve_entry_t *resolve_entry ,  void *cb_data);
ma_error_t ma_dns_resolve(ma_dns_t *dns ,  ma_lookup_entry_t *lookup_entry, ma_dns_resolve_cb cb , void *cb_data);
ma_error_t ma_dns_resolve_stop(ma_dns_t *dns , ma_lookup_entry_t *lookup_entry);


/* Will provide more get function if needed */
long ma_lookup_entry_get_pid(ma_lookup_entry_t *lookup_entry);
ma_error_t ma_lookup_entry_release(ma_lookup_entry_t *lookup_entry);


ma_error_t ma_resolve_entry_release(ma_resolve_entry_t *resolve_entry);

MA_CPP(})

#endif /* MA_DOMAIN_NAME_SERVER_H_INCLUDED */
