#ifndef MA_MSGBUS_AGENT_CRYPTO_PROVIDER_H_INCLUDED
#define MA_MSGBUS_AGENT_CRYPTO_PROVIDER_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/core/msgbus/ma_msgbus_crypto_provider.h"
#include "ma/ma_log.h"

typedef struct ma_msgbus_agent_crypto_provider_s  ma_msgbus_agent_crypto_provider_t, *ma_msgbus_agent_crypto_provider_h;
struct ma_crypto_s;

ma_error_t ma_msgbus_agent_crypto_provider_create(ma_msgbus_agent_crypto_provider_t **provider);

ma_error_t ma_msgbus_agent_crypto_provider_set_crypto(ma_msgbus_agent_crypto_provider_t *provider, struct ma_crypto_s *crypto);

ma_error_t ma_msgbus_agent_crypto_provider_set_logger(ma_msgbus_agent_crypto_provider_t *provider, ma_logger_t *logger);


#endif /* MA_MSGBUS_AGENT_CRYPTO_PROVIDER_H_INCLUDED */
