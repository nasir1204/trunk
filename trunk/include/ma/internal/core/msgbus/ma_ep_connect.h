#ifndef MA_EP_CONNECT_H_INCLUDED
#define MA_EP_CONNECT_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/core/msgbus/ma_msgbus_impl.h"
#include "ma/internal/core/msgbus/ma_message_consumer.h"

/*It is a consumer by itself */
MA_CPP(extern "C" {)

typedef struct ma_ep_connect_s ma_ep_connect_t , *ma_ep_connect_h;


/* Pipe name to connect to , not sure we need host name or not , lets keep it for now*/
ma_error_t ma_ep_connect_create(ma_msgbus_t *bus  , const char *pipe_name , const char *host_name , ma_ep_connect_t **connect);

/* or call ma_message_consumer_dec_ref */
ma_error_t ma_ep_connect_release(ma_ep_connect_t *connect);

/* Incremented ref count of consumer if found */
typedef void (*ma_ep_connect_connect_cb)(ma_ep_connect_t *connect, ma_error_t status ,  void *cb_data);

/* connect callback needs to be provide if user wants to know when the connection establishes , else we can pass it NULL */
ma_error_t ma_ep_connect_start(ma_ep_connect_t *connect , ma_ep_connect_connect_cb cb, void *cb_data);

ma_error_t ma_ep_connect_stop(ma_ep_connect_t *connect);


MA_CPP(})

#endif /* MA_EP_CONNECT_H_INCLUDED */

