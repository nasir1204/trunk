#ifndef MA_MSGBUS_REQUEST_H_INCLUDED
#define MA_MSGBUS_REQUEST_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_msgbus.h"
#include "ma/internal/core/msgbus/ma_message_consumer.h"

#include "uv.h"

MA_CPP(extern "C" {)

#define ephemeral_name_template "ma.internal.request.%08X.%lu"


struct ma_msgbus_request_s {  
    /*
     * MessageBus and filtering data
     */

    /*! It is a message consumer -- Please keep as first field in this struct */
    ma_message_consumer_t			consumer;

    /*! this objects' ephemeral name */
    char                            ephemeral_name_buffer[8+8+sizeof(ephemeral_name_template)]; /* */

    /*! CORRELATION ID of this request */
    ma_uint32_t						correlation_id;


    /*
     * Client data 
     */

    /*! Request cancel flags */
    int								cancel_flags;

    /*! timeout for message request */
    long							timeout_ms;

    /*! thread model (ma_msgbus_callback_thread_options_t) */
    unsigned int                    thread_model; 

    /*! callback for completion notification */
    ma_msgbus_request_callback_t	callback;

    /*! Callback data */
    void							*callback_data;

    /*! message to be sent */
    ma_message_t					*request_message;

    /*
     * Internal data 
     *
     */

    /*! Endpoint handle corresponding to request*/
    ma_msgbus_endpoint_t			*endpoint;

    /*! message bus instance to access all message bus objects */
    struct ma_msgbus_s				*mbus;
   

    // endpoint name
    char                            *ep_name;

    /*! uv timer handle */
    uv_timer_t						uv_timer;

	ma_bool_t                       is_request_send;

    /*! work object for callback invocation from thread pool */
    uv_work_t                       uv_work;

    /*
     * RESULT DATA 
     */
    ma_message_t		            *response_message;

    ma_error_t		                status;	
     

    /*
     * Unknown stuff
     */
    /*! Endpoint consumer and cookie to play with stop on endpoint*/
    ma_message_consumer_t			*ep_consumer ; 
    void                            *ep_cookie;
};


// Creates a handle to the message request.
ma_error_t ma_msgbus_request_create(ma_msgbus_t *mbus, ma_msgbus_request_t **request);

/* starts the message sending process. It will take care of itself as it progresses...  */
ma_error_t ma_msgbus_request_start(ma_msgbus_request_t *request, const char *service_name, ma_msgbus_consumer_reachability_t reachability);

// Increments request-message reference count.
ma_error_t ma_msgbus_request_add_ref(ma_msgbus_request_t *request);

// This is already decleared in ma_msgbus.h file.
// Decrements request-message reference count. release request-message object when the ref count is zero.
//ma_error_t ma_msgbus_request_release(ma_msgbus_request_t *request);

MA_CPP(})

#endif /* MA_MSGBUS_REQUEST_H_INCLUDED */
