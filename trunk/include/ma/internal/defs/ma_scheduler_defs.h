#ifndef MA_SCHEDULER_DEFS_H_INCLUDED
#define MA_SCHEDULER_DEFS_H_INCLUDED

/* Services, names , constants , datastore configurations */
#define MA_SCHEDULER_SERVICE_NAME_STR                                       "ma.service.scheduler"
#define MA_SCHEDULER_CLIENT_NAME_STR                                        "ma.client.scheduler"
#define MA_SCHEDULER_SERVICE_HOSTNAME_STR                                   "localhost"


/*  Scheduler service ePO/server policies */
#define MA_SCHEDULER_SERVICE_SECTION_NAME_STR                               "SchedulerService"
#define MA_SCHEDULER_KEY_IS_INSTALLED_INT                                   "IsInstalled"
#define MA_SCHEDULER_KEY_IS_ENABLED_INT                                     "IsEnabled"


/*  Scheduler service datastore settings*/
#define MA_SCHEDULER_PATH_NAME_STR                                          "AGENT\\SERVICES\\SCHEDULER"
/*  Scheduler service properties */
#define MA_SCHEDULER_PROPERTIES_PATH_NAME_STR                               "AGENT\\SERVICES\\SCHEDULER\\PROPERTIES"

/*  Scheduler service local Policies */
#define MA_SCHEDULER_POLICIES_PATH_NAME_STR                                 "AGENT\\SERVICES\\SCHEDULER\\POLICIES"

/*  Scheduler service Data */
#define MA_SCHEDULER_DATA_PATH_NAME_STR                                     "AGENT\\SERVICES\\SCHEDULER\\DATA"


/* scheduler service messages */
#define MA_SCHEDULER_SERVICE_MSG_TYPE       "TASK_OP_TYPE"
// task operation message type(s)
#define MA_TASK_MSG_TYPE_ADD                "TASK_ADD"
#define MA_TASK_MSG_TYPE_GET                "TASK_GET"
#define MA_TASK_MSG_TYPE_DELETE             "TASK_DELETE"
#define MA_TASK_MSG_TYPE_QUERY_STATUS       "TASK_QUERY_STATUS"
#define MA_TASK_MSG_TYPE_MODIFY             "TASK_MODIFY"
#define MA_TASK_MSG_TYPE_UPDATE             "TASK_UPDATE"
#define MA_TASK_MSG_TYPE_ON_DEMAND_RUN      "TASK_ON_DEMAND_RUN"
#define MA_TASK_ENUMERATE                   "TASK_ENUMERATE"
#define MA_TASK_MSG_TYPE_POSTPONE           "TASK_POSTPONE"

/* task pubsub topics */
#define MA_TASK_EXECUTE_PUBSUB_TOPIC		"ma.task.execute"
#define MA_TASK_STOP_PUBSUB_TOPIC		    "ma.task.stop"
#define MA_TASK_REMOVE_PUBSUB_TOPIC		    "ma.task.remove"
#define MA_TASK_UPDATE_STATUS_PUBSUB_TOPIC  "ma.task.update.status"

/* request task status updation message type(s) */
#define MA_TASK_STATUS_SET_FAILED           "TASK_STATUS_SET_FAILED"
#define MA_TASK_STATUS_SET_SUCCESS          "TASK_STATUS_SET_SUCCESS"

#define MA_TASK_ID                          "TASK_ID"
#define MA_TASK_CREATOR_ID                  "TASK_CREATOR_ID"
#define MA_TASK_NAME                        "TASK_NAME"
#define MA_TASK_TYPE                        "TASK_TYPE"
#define	MA_TASK_PRODUCT_ID                  "TASK_PRODUCT_ID"
#define MA_TASK_STATUS                      "TASK_STATUS"
#define MA_TASK_POSTPONE_INTERVALS          "TASK_POSTPONE_INTERVALS"
#define MA_EMPTY                            ""
#define MA_ERROR_CODE                       "ERROR_CODE"

/* error message type */
#define MA_TASK_INVALID_ID                  "TASK_ID_INVALID"

/* reply status message type(s) */
#define MA_TASK_REPLY_STATUS_SUCCESS        "TASK_REPLY_STATUS_SUCCESS"
#define MA_TASK_REPLY_STATUS_FAILED         "TASK_REPLY_STATUS_FAILED"

#define MA_TASK_DEFAULT_TASK_TYPE_STR       "MA_DEFAULT_TASK_TYPE"



/*  Scheduler service constants */

#define MA_SCHEDULER_BASE_PATH_STR              "scheduler"
#define MA_MAX_TASKID_LEN 128
#define MA_MAX_TRIGGERID_LEN 3
/*TODO - move this to more upper layer ma_strdef.h*/
#define MA_MAX_SOFTWARE_ID_LEN_INT  64 /* Software ID*/
#define MA_MAX_CREATOR_ID_LEN_INT   64 /* It should be software id only*/

// TBD: get the default values from 4.8 scheduler
#define MA_DEFAULT_MAX_RUN_TIME (24 * 60) /* 24 hrs * 60 mins */
#define MA_DEFAULT_IDLE_WAIT_DEAD_LINE 5 /* in mins */
#define MA_DEFAULT_MAX_RETRIES 5
#define MA_DEFAULT_MAX_RUN_TIME_LIMIT (24*60)
#define MA_DEFAULT_RETRY_INTERVAL 5
#define MA_DEFAULT_RETRY_COUNT 3
#define MA_DEFAULT_MISSED_INTERVAL 2 /* in mins */

/* task ini defs */
/* section [Task] */
#define MA_TASK_INI_SECTION_TASK        "Task"
#ifdef MA_WINDOWS
#define MA_TASK_INI_KEY_NAME            "TaskName"
#define MA_TASK_INI_KEY_SOFTWARE_NAME   "SoftwareName"
#define MA_TASK_INI_KEY_CREATOR_ID      "CreatorSoftwareID"
#else
#define MA_TASK_INI_KEY_NAME            "DisplayName"
#define MA_TASK_INI_KEY_SOFTWARE_NAME   "SoftwareID"
#define MA_TASK_INI_KEY_CREATOR_ID      "CreatorID"
#endif
#define MA_TASK_INI_KEY_PLATFORM        "Platforms"
#define MA_TASK_INI_KEY_PRIORITY        "TaskPriority"
#define MA_TASK_INI_KEY_ID              "TaskID"
#define MA_TASK_INI_KEY_TASK_TYPE            "TaskType"
#define MA_TASK_INI_KEY_VERSION         "TaskVersion"
#define MA_TASK_INI_KEY_USER            "UserName"
#define MA_TASK_INI_KEY_DOMAIN          "DomainName"
#define MA_TASK_INI_KEY_PASSWORD        "Password"

/* section [Settings] */
#define MA_TASK_INI_SECTION_SETTINGS        "Settings"
#define MA_TASK_INI_KEY_STOP_AFTER_MINUTES  "StopAfterMinutes"
#define MA_TASK_INI_KEY_ENABLED             "Enabled"
#define MA_TASK_INI_KEY_DUPLICATED          "Duplicated"

/* section [UpdateOptions] */
#define MA_TASK_INI_SECTION_UPDATE_OPTIONS  "UpdateOptions"
#define MA_TASK_INI_KEY_ALLOW_POSTPONE      "bAllowPostpone"
#define MA_TASK_INI_KEY_SHOW_UI             "bShowUI"
#define MA_TASK_INI_KEY_MAX_POSTPONES       "nMaxPostpones"
#define MA_TASK_INI_KEY_POSTPONE_TIMEOUT    "nPostponeTimeout"
#define MA_TASK_INI_KEY_POSTPONE_TEXT       "szPostponeText"

/* section [Schedule] */
#define MA_TASK_INI_SECTION_SCHEDULE        "Schedule"
#define MA_TASK_INI_KEY_TYPE                "Type"
#define MA_TASK_INI_KEY_GMTTIME             "GMTTime"
#define MA_TASK_INI_KEY_RUN_IF_MISSED       "RunIfMissed"
#define MA_TASK_INI_KEY_RUN_IF_MISSED_DELAY "RunIfMissedDelayMins"
#define MA_TASK_INI_KEY_START_DATE          "StartDateTime"
#define MA_TASK_INI_KEY_STOP_DATE_VALID     "StopDateValid"
#define MA_TASK_INI_KEY_STOP_DATE_TIME      "StopDateTime"
#define MA_TASK_INI_KEY_TASK_REPEATABLE     "TaskRepeatable"
#define MA_TASK_INI_KEY_REPEAT_OPTION       "RepeatOption"
#define MA_TASK_INI_KEY_REPEAT_INTERVAL     "RepeatInterval"
#define MA_TASK_INI_KEY_UNTIL_OPTION        "UntilOption"
#define MA_TASK_INI_KEY_UNTIL_TIME          "UntilTime"
#define MA_TASK_INI_KEY_UNTIL_DURATION      "UntilDuration"
#define MA_TASK_INI_KEY_RANDOMIZATION_ENABLED   "RandomizationEnabled"
#define MA_TASK_INI_KEY_RANDOMIZATION_WND_MINS  "RandomizationWndMins"
#define MA_TASK_INI_KEY_ENABLE_RUN_TASK_ONCEADAY    "EnableRunTaskOnceADay"
#define MA_TASK_INI_KEY_AT_STARTUP_DELAY_MINUTES    "AtStartupDelayMinutes"
#define MA_TASK_INI_KEY_REPEAT_DAYS             "RepeatDays"
#define MA_TASK_INI_KEY_REPEAT_WEEKS            "RepeatWeeks"
#define MA_TASK_INI_KEY_MASK_DAYSOFWEEK         "MaskDaysOfWeek"
#define MA_TASK_INI_KEY_MONTH_OPTION            "MonthOption"
#define MA_TASK_INI_KEY_DAY_NUM_OF_MONTH        "DayNumOfMonth"
#define MA_TASK_INI_KEY_WEEK_NUM_OF_MONTH       "WeekNumOfMonth"
#define MA_TASK_INI_KEY_DAY_OF_WEEK             "DayOfWeek"
#define MA_TASK_INI_KEY_MASK_MONTHS_OF_YEAR     "MaskMonthsOfYear"
#define MA_TASK_INI_KEY_IDLE_MINS               "IdleMinutes"
#define MA_TASK_INI_KEY_LAST_RUN_TIME           "LastRunTime"
#define MA_TASK_INI_KEY_FINISHED                "TaskFinished"

#define MA_TASK_INI_SECTION_GENERAL             "General"

#define MA_TASK_INI_SECTION_INET_MANAGER        "InetManager"
#define MA_TASK_INI_KEY_ENABLE_SITES            "EnabledSites"
#define MA_TASK_INI_KEY_ENABLED_SITES_NUM       "EnabledSitesNum"
#define MA_TASK_INI_KEY_REGENERATE_CACHE_ENABLED    "RegenerateCacheEnabled"
#define MA_TASK_INI_KEY_REGENERATE_CACHE_TIMEOUT_MINS   "szRegenerateCacheTimoutMins"

#define MA_TASK_INI_SECTION_SELECTED_UPDATES    "SelectedUpdates"
#define MA_TASK_INI_KEY_NUM_OF_UPDATES          "NumOfUpdates"
#define MA_TASK_INI_KEY_UPDATE_ALL              "UpdateAll"
#define MA_TASK_INI_KEY_UPDATE_1                "Update_1"
#define MA_TASK_INI_KEY_UPDATE_2                "Update_2"
#define MA_TASK_INI_KEY_UPDATE_3                "Update_3"

#define MA_TASK_INI_SECTION_SELECTED_UPDATES_AMCORDAT   "SelectedUpdates\\AMCORDAT1000"
#define MA_TASK_INI_KEY_UPDATE                  "Update"

#define MA_TASK_INI_SECTION_SELECTED_UPDATES_VSSCANDAT  "SelectedUpdates\\VSCANDAT1000"
#define MA_TASK_INI_SECTION_SELECTED_UPDATES_VSSCANENG  "SelectedUpdates\\VSCANENG1000"

#define MA_TASK_INI_SECTION_INSTALL             "Install"
#define MA_TASK_INI_KEY_INSTALL_1               "Install_1"
#define MA_TASK_INI_NUM_INSTALLS                "NumInstalls"

#define MA_TASK_INI_SECTION_INSTALL_TELEMETRY   "Install\\TELEMTRY1000"
#define MA_TASK_INI_KEY_BUILD_VERSION           "BuildVersion"
#define MA_TASK_INI_KEY_INSTALL_COMMAND_LINE    "InstallCommandLine"
#define MA_TASK_INI_KEY_LANGUAGE                "Language"
#define MA_TASK_INI_KEY_PACKAGE_PATH_TYPE       "PackagePathType"

#endif /* MA_SCHEDULER_DEFS_H_INCLUDED*/

