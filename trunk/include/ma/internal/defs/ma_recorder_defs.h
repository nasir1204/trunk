#ifndef MA_RECORDER_SERVICE_DEFS_H_INCLUDED
#define MA_RECORDER_SERVICE_DEFS_H_INCLUDED

/* Services, names , constants , datastore configurations */
#define MA_RECORDER_SERVICE_NAME_STR                                       "ma.service.recorder"
#define MA_RECORDER_CLIENT_NAME_STR                                        "ma.client.recorder"
#define MA_RECORDER_SERVICE_HOSTNAME_STR                                   "localhost"
#define MA_RECORDER_BASE_PATH_STR                                           "recorder"


/*  Recording service ePO/server policies */
#define MA_RECORDER_SERVICE_SECTION_NAME_STR                               "RecordingService"
#define MA_RECORDER_KEY_IS_INSTALLED_INT                                   "IsInstalled"
#define MA_RECORDER_KEY_IS_ENABLED_INT                                     "IsEnabled"


/*  Recording service datastore settings*/
#define MA_RECORDER_PATH_NAME_STR                                          "AGENT\\SERVICES\\RECORDER"
/*  Recording service properties */
#define MA_RECORDER_PROPERTIES_PATH_NAME_STR                               "AGENT\\SERVICES\\RECORDER\\PROPERTIES"

/*  Recording service local Policies */
#define MA_RECORDER_POLICIES_PATH_NAME_STR                                 "AGENT\\SERVICES\\RECORDER\\POLICIES"

/*  Recording service Data */
#define MA_RECORDER_DATA_PATH_NAME_STR                                     "AGENT\\SERVICES\\RECORDER\\DATA"


/* recorder service messages */
#define MA_RECORDER_SERVICE_MSG_TYPE                                       "ma.recorder.op.msg.type"
#define MA_RECORDER_SERVICE_MSG_REGISTER                                   "ma.recorder.op.msg.register"
#define MA_RECORDER_SERVICE_MSG_DEREGISTER                                 "ma.recorder.op.msg.deregister"
#define MA_RECORDER_SERVICE_MSG_ORDER_CREATE_TYPE                          "ma.recorder.op.msg.order.create.type"
#define MA_RECORDER_SERVICE_MSG_ORDER_MODIFY_TYPE                          "ma.recorder.op.msg.order.modify.type"
#define MA_RECORDER_SERVICE_MSG_ORDER_DELETE_TYPE                          "ma.recorder.op.msg.order.delete.type"
#define MA_RECORDER_SERVICE_MSG_ORDER_UPDATE_STATUS_TYPE                   "ma.recorder.op.msg.order.udpate.status.type"
#define MA_RECORDER_SERVICE_MSG_GET_TYPE                                   "ma.recorder.op.msg.get.type"
#define MA_RECORDER_SERVICE_MSG_RECORDER_DELETE_TYPE                        "ma.recorder.op.msg.delete.type"
#define MA_RECORDER_SERVICE_MSG_GET_JSON_TYPE                              "ma.recorder.op.msg.get.json"
#define MA_RECORDER_ID                                                     "ma.recorder.id"
#define MA_RECORDER_CRN                                                    "ma.recorder.crn"
#define MA_RECORDER_ID_EXIST                                               "ma.recorder.id.exist"
#define MA_RECORDER_ID_NOT_EXIST                                           "ma.recorder.id.not.exist"
#define MA_RECORDER_STATUS                                                 "ma.recorder.status"
#define MA_RECORDER_ID_WRITTEN                                             "ma.recorder.id.written"
#define MA_RECORDER_ID_DELETED                                             "ma.recorder.id.deleted"

/* recorder attributes */
#define MA_RECORDER_INFO_ATTR_DATE                                        "date"
#define MA_RECORDER_INFO_ATTR_PENDING_ORDERS                               "pendingorders"
#define MA_RECORDER_INFO_ATTR_COMPLETED_ORDERS                             "completedorders"


#endif /* MA_RECORDER_SERVICE_DEFS_H_INCLUDED */

