#ifndef MA_INFO_DEFS_H_INCLUDED
#define MA_INFO_DEFS_H_INCLUDED

#define MA_INFO_EVENTS_CLIENT_NAME_STR			"ma.client.ma_info"
#define MA_INFO_EVENTS_PUBSUB_TOPIC_STR			"ma_info.events.publish"
#define MA_INFO_MSG_KEY_EVENT_TYPE_STR			"ma_info.msg.porp.event_type"

#define MA_AGENT_INFO_MSG_PROP_KEY_MSG_TYPE_STR                 "prop.key.msg_type"
#define MA_AGENT_INFO_MSG_PROP_VAL_RPLY_GET_AGENT_INFO_STR		"prop.val.rply.agent_info.get"


#endif /* MA_INFO_DEFS_H_INCLUDED*/