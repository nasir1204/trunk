/** @file ma_repository_defs.h
 *  @brief repository common defs
 *
 */
#include "ma/repository/ma_repository_client_defs.h"

#ifndef MA_REPOSITORY_DEFS_H_INCLUDED
#define MA_REPOSITORY_DEFS_H_INCLUDED

#define MA_REPOSITORY_SERVICE_NAME_STR				                                    "ma.service.repository"
#define MA_REPOSITORY_CLIENT_NAME_STR                                                   "ma.client.repository"
#define MA_REPOSITORY_SERVICE_HOSTNAME_STR                                              "localhost"
#define MA_REPOSITORY_MIRROR_CLIENT_NAME_STR                                            "ma.client.repository.mirror"


/*  Repository service ePO/server policies */
#define MA_REPOSITORY_SERVICE_SECTION_NAME_STR                                          "RepositoryService"
#define MA_REPOSITORY_KEY_IS_INSTALLED_INT                                              "IsInstalled"
#define MA_REPOSITORY_KEY_IS_ENABLED_INT                                                "IsEnabled"

#define MA_REPOSITORY_ADVANCED_SECTION_NAME_STR                                       "Advanced"
#define MA_REPOSITORY_KEY_OVERWRITE_CLIENT_REPOS_INT                                    "OverwriteClientSites"
#define MA_REPOSITORY_KEY_RANK_TYPE_INT                                                 "uiFindNearestMethod"
#define MA_REPOSITORY_KEY_HOP_LIMIT_INT                                                 "nMaxHopLimit"
#define MA_REPOSITORY_KEY_PING_TIMEOUT_INT                                              "nMaxPingTimeout"

#define MA_REPOSITORY_LICENSE_SECTION_NAME_STR                                       "License"
#define MA_REPOSITORY_KEY_LICENSE_STR				                                    "LicenseKey"

#define MA_REPOSITORY_PROXY_SECTION_NAME_STR                                          "ProxySettings"
#define MA_REPOSITORY_PROXY_KEY_PROXY_USAGE_INT                                         "uiUseProxyType"
#define MA_REPOSITORY_PROXY_KEY_ENABLE_BYPASS_LOCAL_INT                                 "bBypassLocalAddress"
#define MA_REPOSITORY_PROXY_KEY_ENABLE_USER_CONFIG_INT                                  "bAllowUserToConfigureProxy"
#define MA_REPOSITORY_PROXY_KEY_NUM_OF_EXCEPTIONS_INT                                   "uiNumExceptions"
#define MA_REPOSITORY_PROXY_KEY_EXCEPTION_LIST_STR                                      "Exception_"
#define MA_REPOSITORY_PROXY_KEY_USE_SINGLE_PROXY_INT                                    "bUseSingleProxySettings"
#define MA_REPOSITORY_PROXY_KEY_HTTP_PROXY_SERVER_URL_STR                               "szHttpProxyServer"
#define MA_REPOSITORY_PROXY_KEY_HTTP_PROXY_PORT_INT                                     "uiHttpProxyPort"
#define MA_REPOSITORY_PROXY_KEY_USE_HTTP_AUTH_INT                                       "bUseHttpAuthentication"
#define MA_REPOSITORY_PROXY_KEY_HTTP_PROXY_USER_STR                                     "szHttpProxyUser"
#define MA_REPOSITORY_PROXY_KEY_HTTP_PROXY_PASSWD_STR                                   "szHttpProxyPassword"
#define MA_REPOSITORY_PROXY_KEY_HTTP_PROXY_FIPS_PASSWD_STR                              "256_szHttpProxyPassword"
#define MA_REPOSITORY_PROXY_KEY_FTP_PROXY_SERVER_URL_STR                                "szFtpProxyServer"
#define MA_REPOSITORY_PROXY_KEY_FTP_PROXY_PORT_INT                                      "uiFtpProxyPort"
#define MA_REPOSITORY_PROXY_KEY_USE_FTP_AUTH_INT                                        "bUseFtpAuthentication"
#define MA_REPOSITORY_PROXY_KEY_FTP_PROXY_USER_STR                                      "szFtpProxyUser"
#define MA_REPOSITORY_PROXY_KEY_FTP_PROXY_PASSWD_STR                                    "szFtpProxyPassword"
#define MA_REPOSITORY_PROXY_KEY_FTP_PROXY_FIPS_PASSWD_STR                               "256_szFtpProxyPassword"

#define MA_REPOSITORY_SITELIST_SECTION_NAME_STR                                         "SiteList"
#define MA_REPOSITORY_SITELIST_KEY_NUM_OF_SITES_INT                                     "uiNumberOfSites"
#define MA_REPOSITORY_SITELIST_KEY_NAME_STR                                             "szName_"
#define MA_REPOSITORY_SITELIST_KEY_SITE_TYPE_INT                                        "uiSiteType_"
#define MA_REPOSITORY_SITELIST_KEY_NAMESPACE_INT                                        "bLocal_"
#define MA_REPOSITORY_SITELIST_KEY_USE_AUTH_INT                                         "bUseAuth_"
#define MA_REPOSITORY_SITELIST_KEY_IS_ENABLED_INT                                       "bEnabled_"
#define MA_REPOSITORY_SITELIST_KEY_SERVER_URL_STR                                       "szServer_"
#define MA_REPOSITORY_SITELIST_KEY_SERVER_PORT_INT                                      "uiPort_"
#define MA_REPOSITORY_SITELIST_KEY_RELATIVE_PATH_STR                                    "szRelativePath_"
#define MA_REPOSITORY_SITELIST_KEY_SHARE_NAME_STR                                       "szShareName_"
#define MA_REPOSITORY_SITELIST_KEY_DOMAIN_NAME_STR                                      "szDomainName_"
#define MA_REPOSITORY_SITELIST_KEY_USER_NAME_STR                                        "szUserName_"
#define MA_REPOSITORY_SITELIST_KEY_PASSWD_STR                                           "szPassword_"
#define MA_REPOSITORY_SITELIST_KEY_FIPS_PASSWD_STR                                      "256_szPassword_"
#define MA_REPOSITORY_SITELIST_KEY_IS_PASSWD_ENCRYPTED_INT                              "bEncrypted_"
#define MA_REPOSITORY_SITELIST_KEY_SITE_ORDER_INT                                       "uiOrder_"


#define MA_REPOSITORY_INETMANAGER_SECTION_NAME_STR                                      "InetManager"
#define MA_REPOSITORY_INETMANAGER_KEY_DISABLED_SITES_NUM_INT                            "DisabledSiteNum"
#define MA_REPOSITORY_INETMANAGER_KEY_DISABLED_SITES_STR                                "DisabledSites_"
#define MA_REPOSITORY_INETMANAGER_KEY_SITELIST_ORDER_NUM_INT                            "SitelistOrderNum"
#define MA_REPOSITORY_INETMANAGER_KEY_SITELIST_ORDER_STR                                "SitelistOrder_"
#define MA_REPOSITORY_INETMANAGER_KEY_INCLUDE_REPOS_BY_DEFAULT_INT                      "includeReposByDefault"



/*  Repository service datastore settings*/
/*  Repository services policies */
#define MA_RESOSITORY_PROPERTIES_PATH_NAME_STR                                          "AGENT\\SERVICES\\REPOSITORY\\POLICIES"
///*  Repository services policies */
#define MA_REPOSITORY_POLICIES_PROXY_PATH_NAME_STR                                      MA_RESOSITORY_PROPERTIES_PATH_NAME_STR
#define MA_REPOSITORY_POLICIES_PROXY_KEY_PROXY_USAGE_INT                                MA_REPOSITORY_PROXY_KEY_PROXY_USAGE_INT
#define MA_REPOSITORY_POLICIES_PROXY_KEY_BYPASS_LOCAL_INT                               MA_REPOSITORY_PROXY_KEY_ENABLE_BYPASS_LOCAL_INT
#define MA_REPOSITORY_POLICIES_PROXY_KEY_ALLOW_USER_CONFIG_INT                          MA_REPOSITORY_PROXY_KEY_ENABLE_USER_CONFIG_INT
#define MA_REPOSITORY_POLICIES_PROXY_KEY_EXCLUSION_LIST_STR                             MA_REPOSITORY_PROXY_KEY_EXCEPTION_LIST_STR

#define MA_REPOSITORY_POLICIES_SYSTEM_PROXY_AUTH_PATH_STR                               "AGENT\\SERVICES\\REPOSITORY\\POLICIES\\PROXY_CONFIG\\SYSTEM_PROXY_AUTH"
#define MA_REPOSITORY_POLICIES_SYSTEM_PROXY_AUTH_KEY_HTTP_AUTH_INT                      "HTTP_AUTH"
#define MA_REPOSITORY_POLICIES_SYSTEM_PROXY_AUTH_KEY_FTP_AUTH_INT                       "FTP_AUTH"
#define MA_REPOSITORY_POLICIES_SYSTEM_PROXY_AUTH_KEY_HTTP_USER_STR                      "HTTP_USER"
#define MA_REPOSITORY_POLICIES_SYSTEM_PROXY_AUTH_KEY_FTP_USER_STR                       "FTP_USER"
#define MA_REPOSITORY_POLICIES_SYSTEM_PROXY_AUTH_KEY_HTTP_PASSWD_STR                    "HTTP_PASSWD"
#define MA_REPOSITORY_POLICIES_SYSTEM_PROXY_AUTH_KEY_FTP_PASSWD_STR                     "FTP_PASSWD"
/*  Repository service data */
#define MA_REPOSITORY_DATA_PATH_NAME_STR									            "AGENT\\SERVICES\\REPOSITORY\\DATA"
#define MA_REPOSITORY_DATA_KEY_REPO_KEY_STR											    "RepositoryKey"
#define MA_REPOSITORY_DATA_KEY_REPO_VERSION_STR										    "RepositoryKeyHashVersion"

/*  Repository service properties */
#define MA_REPOSITORY_PROPERTIES_PATH_NAME_STR								            "AGENT\\SERVICES\\REPOSITORY\\PROPERTIES"



/* Repository service messages */
#define MA_REPOSITORY_MSG_KEY_TYPE_STR                                                  "prop.key.msg_type"
#define MA_REPOSITORY_MSG_PROP_KEY_PRODUCT_ID_STR                                       "prop.key.product_id"
#define MA_REPOSITORY_PUBSUB_TOPIC_MIRROR_STATE_STR    		                            "ma.repository.mirror.state"
#define MA_REPOSITORY_MSG_PROP_KEY_MIRROR_SESSION_ID_STR    		                    "prop.key.mirror.session_id"
#define MA_REPOSITORY_MSG_PROP_KEY_FILTER_STR				   		                    "prop.key.filter"

/* request message types */
#define MA_REPOSITORY_MSG_PROP_VAL_RQST_GET_REPOSITORIES_STR                            "prop.val.rqst.repositories.get"
#define MA_REPOSITORY_MSG_PROP_VAL_RQST_UPDATE_REPOSITORIES_STR                         "prop.val.rqst.repositories.update"
#define MA_REPOSITORY_MSG_PROP_VAL_RQST_IMPORT_REPOSITORIES_STR                         "prop.val.rqst.repositories.import"
#define MA_REPOSITORY_MSG_PROP_VAL_RQST_RANK_REPOSITORIES_STR                           "prop.val.rqst.repositories.rank"
#define MA_REPOSITORY_MSG_PROP_VAL_RQST_ENFORCE_REPOSITORY_POLICY_STR                   "prop.val.rqst.repositories.policy.enforce"
#define MA_REPOSITORY_MSG_PROP_VAL_RQST_GET_PROXIES_STR                                 "prop.val.rqst.proxies.get"
#define MA_REPOSITORY_MSG_PROP_VAL_RQST_GET_SYS_PROXIES_STR                             "prop.val.rqst.system.proxies.get"
#define MA_REPOSITORY_MSG_PROP_VAL_RQST_UPDATE_PROXIES_STR                              "prop.val.rqst.proxies.update"
#define MA_REPOSITORY_MSG_PROP_VAL_RQST_GET_REPOKEYS_STR					            "prop.val.rqst.repokeys.get"
#define MA_REPOSITORY_MSG_PROP_VAL_RQST_MIRROR_TASK_STR									"prop.val.rqst.mirror.task"
#define MA_REPOSITORY_MSG_PROP_VAL_SYS_PROXY_URL_STR									"prop.val.system.proxy.url.get"
/* response message types */
#define MA_REPOSITORY_MSG_PROP_VAL_RPLY_GET_REPOSITORIES_STR                            "prop.val.rply.repositories.get"
#define MA_REPOSITORY_MSG_PROP_VAL_RPLY_UPDATE_REPOSITORIES_STR                         "prop.val.rply.repositories.update"
#define MA_REPOSITORY_MSG_PROP_VAL_RPLY_IMPORT_REPOSITORIES_STR                         "prop.val.rply.repositories.import"
#define MA_REPOSITORY_MSG_PROP_VAL_RPLY_RANK_REPOSITORIES_STR                           "prop.val.rply.repositories.rank"
#define MA_REPOSITORY_MSG_PROP_VAL_RPLY_ENFORCE_REPOSITORY_POLICY_STR                   "prop.val.r.repositories.policy.enforce"
#define MA_REPOSITORY_MSG_PROP_VAL_RPLY_GET_PROXIES_STR                                 "prop.val.rply.proxies.get"
#define MA_REPOSITORY_MSG_PROP_VAL_RPLY_GET_SYS_PROXIES_STR                             "prop.val.rply.sys.proxies.get"
#define MA_REPOSITORY_MSG_PROP_VAL_RPLY_UPDATE_PROXIES_STR                              "prop.val.rply.proxies.update"
#define MA_REPOSITORY_MSG_PROP_VAL_RPLY_GET_REPOKEYS_STR					            "prop.val.rply.repokeys.get"
#define MA_REPOSITORY_MSG_PROP_VAL_RPLY_MIRROR_TASK_STR									"prop.val.rply.mirror.task"


/*  repository service constants */

#define MA_KEY_CATALOG_VERSION_STR                                                      "CatalogVersion"
#define MA_KEY_CATALOG_VERSION_FROM_POLICY_STR                                          "CatalogVersionFromPolicy"
/*RepoKeys section .*/
#define MA_SECTION_REPOKEYHASHLIST_STR				                                    "RepoKeyHashList"
#define MA_SECTION_REPOKEYS_DATA_STR				                                    "KeyData"

/*RepoKeys keys.*/
#define MA_KEY_REPOKEY_VERSION_STR					                                    "Version"
#define MA_KEY_REPOKEY_HASHLIST_STR					                                    "List"
#define MA_KEY_REPOKEY_KEYCOUNT_STR					                                    "KeyCount"
#define MA_KEY_REPOKEYHASH_STR						                                    "Key%dHash"
#define MA_KEY_REPOKEYDATA_STR						                                    "Key%dData"

/* Back compat task types for mirror */
#define MA_MIRROR_TASK_TYPE_COMPAT_MIRROR_STR											"Mirror"
/* Task types owned by MA Mirror */
#define MA_MIRROR_TASK_TYPE_MIRROR_STR													"ma.mirror.task.type.mirror"

#define MA_MIRROR_TASK_OPTIONS_SECTION_STR												"MirrorOptions"
#define MA_MIRROR_TASK_PATH_KEY_STR														"szPath"

#endif /* MA_REPOSITORY_DEFS_H_INCLUDED */
