#ifndef MA_OBJECT_DEFS_H_INCLUDED
#define MA_OBJECT_DEFS_H_INCLUDED

#include "ma/internal/utils/context/ma_context.h"

#define MA_OBJECT_MSGBUS_NAME_STR                       "ma.object.msgbus"
#define MA_OBJECT_PRODUCT_ID_STR                        "ma.object.product_id"
#define MA_OBJECT_LOGGER_NAME_STR                       "ma.object.logger"
#define MA_OBJECT_AH_CLIENT_NAME_STR                    "ma.object.ah.client"
#define MA_OBJECT_CLIENT_NAME_STR						"ma.object.client"
#define MA_OBJECT_CRYPTO_NAME_STR                       "ma.object.crypto"
#define MA_OBJECT_NET_CLIENT_NAME_STR                   "ma.object.net.client"
#define MA_OBJECT_SCHEDULER_NAME_STR                    "ma.object.scheduler"
#define MA_OBJECT_BOOKER_NAME_STR                       "ma.object.booker"
#define MA_OBJECT_PROFILER_NAME_STR                     "ma.object.profiler"
#define MA_OBJECT_CONSIGNOR_NAME_STR                    "ma.object.consignor"
#define MA_OBJECT_SEASON_NAME_STR                       "ma.object.season"
#define MA_OBJECT_SEARCH_NAME_STR                       "ma.object.search"
#define MA_OBJECT_BOARD_NAME_STR                        "ma.object.board"
#define MA_OBJECT_COMMENT_NAME_STR                      "ma.object.comment"
#define MA_OBJECT_INVENTORY_NAME_STR                    "ma.object.inventory"
#define MA_OBJECT_LOCATION_NAME_STR                     "ma.object.location"
#define MA_OBJECT_RECORDER_NAME_STR                      "ma.object.recorder"
#define MA_OBJECT_SENSOR_NAME_STR                       "ma.object.sensor"
#define MA_OBJECT_SYSTEM_PROPERTY_STR                   "ma.object.system.property"
#define MA_OBJECT_MA_CONFIGURATOR_NAME_STR              "ma.object.ma.configurator"

#define MA_OBJECT_POLICY_BAG_STR                        "ma.object.policy.bag"
#define MA_OBJECT_POLICY_SETTINGS_BAG_STR               "ma.object.policy.settings.bag"

#define MA_OBJECT_CLIENT_MANAGER_STR                    "ma.object.client.manager"

#define MA_CONTEXT_GET_MSGBUS(context)                  (ma_msgbus_t*)ma_context_get_object_info(context, MA_OBJECT_MSGBUS_NAME_STR)
#define MA_CONTEXT_GET_PRODUCT_ID(context)              (const char*)ma_context_get_object_info(context, MA_OBJECT_PRODUCT_ID_STR)

#define MA_CONTEXT_GET_LOGGER(context)                  (ma_logger_t*)ma_context_get_object_info(context, MA_OBJECT_LOGGER_NAME_STR)
#define MA_CONTEXT_GET_AH_CLIENT(context)               (ma_ah_client_service_t*)ma_context_get_object_info(context, MA_OBJECT_AH_CLIENT_NAME_STR)
#define MA_CONTEXT_GET_CLIENT(context)	                (ma_client_t*)ma_context_get_object_info(context, MA_OBJECT_CLIENT_NAME_STR)
#define MA_CONTEXT_GET_CRYPTO(context)                  (ma_crypto_t*)ma_context_get_object_info(context, MA_OBJECT_CRYPTO_NAME_STR)
#define MA_CONTEXT_GET_NET_CLIENT(context)              (ma_net_client_service_t *)ma_context_get_object_info(context, MA_OBJECT_NET_CLIENT_NAME_STR)
#define MA_CONTEXT_GET_SENSOR(context)                  (ma_sensor_t *)ma_context_get_object_info(context, MA_OBJECT_SENSOR_NAME_STR)
#define MA_CONTEXT_GET_SCHEDULER(context)               (ma_scheduler_t *)ma_context_get_object_info(context, MA_OBJECT_SCHEDULER_NAME_STR)
#define MA_CONTEXT_GET_SYSTEM_PROPERTY(context)         (ma_system_property_t *)ma_context_get_object_info(context, MA_OBJECT_SYSTEM_PROPERTY_STR)
#define MA_CONTEXT_GET_CONFIGURATOR(context)            (ma_configurator_t *)ma_context_get_object_info(context, MA_OBJECT_MA_CONFIGURATOR_NAME_STR)
#define MA_CONTEXT_GET_PROFILER(context)                (ma_profiler_t*)ma_context_get_object_info(context, MA_OBJECT_PROFILER_NAME_STR)
#define MA_CONTEXT_GET_BOOKER(context)                  (ma_booker_t*)ma_context_get_object_info(context, MA_OBJECT_BOOKER_NAME_STR)
#define MA_CONTEXT_GET_INVENTORY(context)               (ma_inventory_t*)ma_context_get_object_info(context, MA_OBJECT_INVENTORY_NAME_STR)
#define MA_CONTEXT_GET_RECORDER(context)                (ma_recorder_t*)ma_context_get_object_info(context, MA_OBJECT_RECORDER_NAME_STR)
#define MA_CONTEXT_GET_LOCATION(context)                (ma_location_t*)ma_context_get_object_info(context, MA_OBJECT_LOCATION_NAME_STR)
#define MA_CONTEXT_GET_SEASON(context)                  (ma_season_t*)ma_context_get_object_info(context, MA_OBJECT_SEASON_NAME_STR)
#define MA_CONTEXT_GET_SEARCH(context)                  (ma_search_t*)ma_context_get_object_info(context, MA_OBJECT_SEARCH_NAME_STR)
#define MA_CONTEXT_GET_BOARD(context)                   (ma_board_t*)ma_context_get_object_info(context, MA_OBJECT_BOARD_NAME_STR)
#define MA_CONTEXT_GET_COMMENT(context)                 (ma_comment_t*)ma_context_get_object_info(context, MA_OBJECT_COMMENT_NAME_STR)
#define MA_CONTEXT_GET_CONSIGNOR(context)                 (ma_consignor_t*)ma_context_get_object_info(context, MA_OBJECT_CONSIGNOR_NAME_STR)

#define MA_CONTEXT_GET_POLICY_BAG(context)              (ma_policy_bag_t*)ma_context_get_object_info(context, MA_OBJECT_POLICY_BAG_STR)
#define MA_CONTEXT_GET_POLICY_SETTINGS_BAG(context)     (ma_policy_settings_bag_t*)ma_context_get_object_info(context, MA_OBJECT_POLICY_SETTINGS_BAG_STR)

#define MA_CONTEXT_GET_CLIENT_MANAGER(context)          (ma_client_manager_t*)ma_context_get_object_info(context, MA_OBJECT_CLIENT_MANAGER_STR)

#endif /* MA_OBJECT_DEFS_H_INCLUDED*/

