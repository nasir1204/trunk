#ifndef MA_GENERAL_DEFS_H_INCLUDED
#define MA_GENERAL_DEFS_H_INCLUDED

/* Constants */
#define MA_SOFTWAREID_META_STR                                                  "EPOAGENTMETA"
#define MA_SOFTWAREID_GENERAL_STR                                               "EPOAGENT3000"
#define MA_UPDATER_SOFTWAREID_STR                                               "CMNUPD__3000"
#define MA_SYSPROPS_SOFTWAREID_STR                                              "SYSPROPS1000"
#define MA_SOFTWAREID_COMPAT_STR	                                        "CMAAGENT3000"

#define MA_MCAFEE_AGENT_SOFTWARE_STR						"McAfee Agent"
#define MA_MCAFEE_AGENT_VERSION_STR						"5.0.0"

#define MA_AGENT_MAIN_SERVICE_NAME_STR                                          "masvc"
#define MA_AGENT_COMMON_SERVICE_NAME_STR                                        "macmnsvc"
#define MA_AGENT_COMPAT_SERVICE_NAME_STR                                        "McAfeeFramework"

#define MA_AGENT_MAIN_SERVICE_NAME_WSTR                                         L"masvc"
#define MA_AGENT_COMMON_SERVICE_NAME_WSTR                                       L"macmnsvc"
#define MA_AGENT_COMPAT_SERVICE_NAME_WSTR                                       L"McAfeeFramework"

#define MA_MFEAGENT_USERNAME_STR						"mfe"
#define MA_MFEAGENT_USERGROUP_STR						"mfe"

#define MA_AGENT_MODE_UNMANAGED_INT                                             0
#define MA_AGENT_MODE_MANAGED_INT                                               1


#if defined( WIN64 )
#define REG_6432_BASE_PATH_A                                                    "SOFTWARE\\Wow6432Node\\"
#define MA_REG_CROSS_ACCESS	                                                KEY_WOW64_32KEY
#else // !WIN64
#define REG_6432_BASE_PATH_A                                                    "SOFTWARE\\"
#define MA_REG_CROSS_ACCESS	                                                KEY_WOW64_64KEY
#endif // WIN64


/*configuration paths */
#if defined(MA_WINDOWS)
#define MA_CONFIGURATION_REGISTRY_BASE_PATH_STR				        "Software"
#define MA_CONFIGURATION_REGISTRY_AGENT_PATH_STR			        "McAfee\\Agent\\"
#define MA_CONFIGURATION_REGISTRY_APPLICATIONS_PATH_STR                         "Software\\McAfee\\Agent\\Applications\\"
#define MA_CONFIGURATION_REGISTRY_AGENT_FULL_PATH_STR				"Software\\McAfee\\Agent\\"
#else
#define MA_CONFIGURATION_REGISTRY_AGENT_PATH_STR			        "/etc/ma.d/config.xml"
#define MA_CONFIGURATION_REGISTRY_APPLICATIONS_PATH_STR                         "/etc/ma.d/"
#define MA_CONFIGURATION_REGISTRY_CONFIG_FILE_NAME_STR                          "config.xml"
#define MA_CONFIGURATION_REGISTRY_CONFIG_SECTION_NAME_STR                       "Configuration"
#define MA_CONFIGURATION_AGENT_INFO_FILE_PATH_STR							"/etc/ma.d/mainfo.ini"
#endif
#define MA_CONFIGURATION_AGENT_KEY_DATA_PATH_STR			            "DataPath"

#if defined(MA_WINDOWS) && defined(_WIN64)
#define MA_CONFIGURATION_AGENT_KEY_INSTALL_PATH_STR			            "InstallPath64"
#else
#define MA_CONFIGURATION_AGENT_KEY_INSTALL_PATH_STR			            "InstallPath"
#endif

#define MA_CONFIGURATION_AGENT_KEY_INSTALL_PATH32_STR			        "InstallPath"

#if defined(MA_WINDOWS) && defined(_WIN64)
#define MA_CONFIGURATION_APPLICATIONS_KEY_RSDK_PATH_STR			        "RuntimeSdkPath64"
#else
#define MA_CONFIGURATION_APPLICATIONS_KEY_RSDK_PATH_STR			        "RuntimeSdkPath"
#endif

#define MA_CONFIGURATION_APPLICATIONS_KEY_TOOLS_PATH_STR		        "ToolsPath"

#define MA_CONFIGURATION_DB_DIR_NAME_STR				            "db"
#define MA_CONFIGURATION_CERTSTORE_DIR_NAME_STR				        "certstore"
#define MA_CONFIGURATION_KEYSTORE_DIR_NAME_STR				        "keystore"
#define MA_CONFIGURATION_LOGS_DIR_NAME_STR				            "logs"
#define MA_CONFIGURATION_UDDATE_DIR_NAME_STR				        "update"
#define MA_CONFIGURATION_LIB_DIR_NAME_STR				            "lib"
#define MA_CONFIGURATION_TOOLS_DIR_NAME_STR				            "tools"
#define MA_CONFIGURATION_RSDK_DIR_NAME_STR				            "rsdk"


/* database /datastore constants */
#define MA_DS_DEFAULT_INSTANCE_NAME                                             "AGENT"
#define MA_DS_BOOKING_INSTANCE_NAME                                             "BOOKING"
#define MA_DS_PROFILE_INSTANCE_NAME                                             "PROFILE"
#define MA_DS_INVENTORY_INSTANCE_NAME                                           "INVENTORY"
#define MA_DS_RECORDER_INSTANCE_NAME                                            "RECORDER"
#define MA_DS_LOCATION_INSTANCE_NAME                                            "LOCATION"
#define MA_DS_SEASON_INSTANCE_NAME                                              "SEASON"
#define MA_DS_SEARCH_INSTANCE_NAME                                              "SEARCH"
#define MA_DS_BOARD_INSTANCE_NAME                                               "BOARD"
#define MA_DS_COMMENT_INSTANCE_NAME                                             "COMMENT"
#define MA_DS_CONSIGNMENT_INSTANCE_NAME                                             "CONSIGNMENT"
#define MA_DS_EVENT_INSTANCE_NAME                                               "EVENTS"
#define MA_DS_SCHEDULER_INSTANCE_NAME                                           "SCHEDULER"
#define MA_DS_BROKER_INSTANCE_NAME		                                        "BROKER"


#define MA_DB_DEFAULT_FILENAME_STR                                              "ma.db"
#define MA_DB_POLICY_FILENAME_STR                                               "mapolicy.db"
#define MA_DB_TASK_FILENAME_STR                                                 "matask.db"
#define MA_DB_EVENT_FILENAME_STR                                                "maevent.db"
#define MA_DB_SCHEDULER_FILENAME_STR                                            "mascheduler.db"
#define MA_DB_COMMON_SERVICES_FILENAME_STR				        "macmnsvc.db"
#define MA_DB_BOOKING_SERVICES_FILENAME_STR                                     "mabooking.db"
#define MA_DB_SEASON_SERVICES_FILENAME_STR                                      "maseason.db"
#define MA_DB_SEARCH_SERVICES_FILENAME_STR                                      "masearch.db"
#define MA_DB_BOARD_SERVICES_FILENAME_STR                                       "maboard.db"
#define MA_DB_COMMENT_SERVICES_FILENAME_STR                                     "macomment.db"
#define MA_DB_CONSIGNMENT_SERVICES_FILENAME_STR                                 "maconsignment.db"
#define MA_DB_PROFILE_SERVICES_FILENAME_STR                                     "maprofile.db"
#define MA_DB_INVENTORY_SERVICES_FILENAME_STR                                   "mainventory.db"
#define MA_DB_RECORDER_SERVICES_FILENAME_STR                                    "marecorder.db"
#define MA_DB_LOCATION_SERVICES_FILENAME_STR                                    "malocation.db"

#define MA_GENERAL_SUBSCRIBER_TOPIC_STR                                         "ma.*"

/* Configuration datastore settings */
#define MA_CONFIGURATION_PATH_NAME_STR                                          "AGENT\\CONFIGURATION"
#define MA_CONFIGURATION_KEY_AGENT_INSTALL_PATH_STR                             "AgentInstallPath"
#define MA_CONFIGURATION_KEY_AGENT_DATA_PATH_STR                                "AgentDataPath"
#define MA_CONFIGURATION_KEY_AGENT_LOGS_PATH_STR                                "AgentLogsPath"
#define MA_CONFIGURATION_KEY_RSDK_PATH_STR				                        "RuntimeSdkPath"
#define MA_CONFIGURATION_KEY_AGENT_LANGUAGE_STR                                 "AgentLanguage"
#define MA_CONFIGURATION_KEY_USER_LANGUAGE_STR									"UserLanguage"
#define MA_CONFIGURATION_KEY_SOFTWARE_ID_STR                                    "AgentSoftwareId"
#define MA_CONFIGURATION_KEY_AGENT_MODE_INT                                     "AgentMode"
#define MA_CONFIGURATION_KEY_AGENT_VERSION_STR                                  "AgentVersion"
#define MA_CONFIGURATION_KEY_MSGBUS_VERSION_STR									"MsgbusVersion"
#define MA_CONFIGURATION_KEY_AGENT_GUID_STR                                     "AgentGuid"
#define MA_CONFIGURATION_KEY_SMBIOS_UUID_STR                                    "SMBiosUUID"
#define MA_CONFIGURATION_KEY_AGENT_COMPUTER_NAME_STR                            "AgentComputerName"
#define MA_CONFIGURATION_KEY_TENANT_ID_STR					                    "TenantId"
#define MA_CONFIGURATION_KEY_AGENT_DB_PATH_STR                                  "AgentDbPath"
#define MA_CONFIGURATION_KEY_AGENT_KEYSTORE_PATH_STR                            "AgentKeystorePath"
#define MA_CONFIGURATION_KEY_AGENT_CERTSTORE_PATH_STR                            "AgentCertstorePath"
#define MA_CONFIGURATION_KEY_SERVER_KEY_HASH_STR                                "ServerKeyHash"
#define MA_CONFIGURATION_KEY_CRYPTO_ROLE_INT                                    "CryptoRole"
#define MA_CONFIGURATION_KEY_CRYPTO_MODE_INT                                    "CryptoMode"
#define MA_CONFIGURATION_KEY_SPIPE_SEQUENCE_NUMBER_STR				            "SpipeSeqNumber"
#define MA_CONFIGURATION_KEY_VDI_MODE_STR					                    "VDIMode"
#define MA_CONFIGURAITON_KEY_LANG_OVERRIDE_INT					                "AgentLanguageOverride"
#define MA_CONFIGURATION_KEY_SEQ_NUM_INT					                    "SpipeSeqNumber"
#define MA_CONFIGURATION_KEY_LAST_COMMUNICATED_EPO_SERVER_STR                   "LastCommunicatedEpoServer"
#define MA_CONFIGURATION_KEY_LAST_COMMUNICATED_EPO_PORT_INT					    "LastCommunicatedEpoPort"
#define MA_CONFIGURATION_KEY_EPO_VERSION_STR                                    "ePOVersion"

/* Server settings keys and publish topic */
#define MA_SERVER_SETTINGS_CHANGE_PUB_TOPIC_STR                                  "ma.server_settings_change"

#define MA_SERVER_SETTING_AGENT_BROADCAST_PORT_STR                               "AgentBroadcastPingPort"
#define MA_SERVER_SETTING_AGENT_PING_PORT_STR                                    "AgentPingPort"
#define MA_SERVER_SETTING_CATALOG_VERSION_STR                                    "CatalogVersion"
#define MA_SERVER_SETTING_ENABLE_AUTO_UPDATE_STR                                 "EnableAutoUpdate"
#define MA_SERVER_SETTING_ENFORCE_POLICY_STR                                     "EnforcePolicy"
#define MA_SERVER_SETTING_LICENSE_KEY_STR                                        "LicenseKey"
#define MA_SERVER_SETTING_SERVER_NAME_STR                                        "ServerName"

/*  Agent general ePO/server policies */
#define MA_AGENT_GENERAL_SECTION_NAME_STR                                       "General"
#define MA_AGENT_KEY_SHOW_AGENT_UI_INT                                          "ShowAgentUI"
#define MA_AGENT_KEY_SHOW_REBOOT_UI_INT                                         "ShowRebootUI"
#define MA_AGENT_KEY_REBOOT_TIMEOUT_INT                                         "RebootTimeOut"
#define MA_AGENT_KEY_ALLOW_UPDATE_SECURITY_INT                                  "bAllowUpdateSecurity"
#define MA_AGENT_EVENTS_PATH_STR                                                "AgentEvents"
#define MA_AGENT_EVENTS_UPLOAD_PATH_STR                                         "Upload"
#define MA_AGENT_DATA_DIR_STR                                                   "data"
#define MA_AGENT_LOGGING_DATA_DIR_STR                                           "logging"
#define MA_AGENT_KEY_IS_CLOUD_INT						                        "IsCloud"
#define MA_AGENT_KEY_SERVER_NAME_STR                                            "ServerName"
#define MA_AGENT_EXENSION_VERSION_STR                                           "ExtensionVersion"
#define MA_AGENT_SELF_PROTECTION_ENABLED_STR                                    "IsSelfProtectionEnabled"
#define MA_AGENT_FORCE_INSTALL_STR                                              "ForceInstall"
#define MA_AGENT_REDUCE_PROCESS_PRIORITY_STR                                    "ReduceProcessPriority"

#define MA_AGENT_LANGUAGE_OPTIONS_SECTION_NAME_STR                              "LanguageOptions"
#define MA_AGENT_ENABLE_LANG_SELECTION_STR                                      "bEnableAgentLangSelection"
#define MA_AGENT_LANGUAGE_SELECTION_STR                                         "AgentLanguage"
#define MA_AGENT_DEFAULT_LANG_ID                                                "0000"

#define MA_AGENT_REPO_RANK_IP_STR												"RepoRankAgentIP"
#define MA_AGENT_REPO_RANK_MAX_PING_TIME_STR									"RepoRankMaxPingTime"
#define MA_AGENT_REPO_RANK_MAX_SUBNET_DIST_STR									"RepoRankMaxSubnetDist"

/* platform ID's */
#if defined(MA_WINDOWS) || defined(WIN32)
#define MA_PLATFORM_ID                                                          "WIN"
#elif defined(MA_PLATFORM_LINUX)
#define MA_PLATFORM_ID                                                          "LINUX"
#elif defined(MA_PLATFORM_MLOS)
#define MA_PLATFORM_ID                                                          "MLOS"
#elif defined(MA_PLATFORM_SOLARIS)
#define MA_PLATFORM_ID                                                          "SLR"
#elif defined(MA_PLATFORM_HPUX)
#define MA_PLATFORM_ID                                                          "HPUX"
#elif defined(MA_PLATFORM_HPIA)
#define MA_PLATFORM_ID                                                          "HPIA"
#elif defined(MA_PLATFORM_AIX)
#define MA_PLATFORM_ID                                                          "AIX"
#elif defined(MA_PLATFORM_MAC)
#define MA_PLATFORM_ID                                                          "MAC"
#else
#define MA_PLATFORM_ID															"UNKNOWN"
#endif

/* Firewall rules for MA 
	Rule_1=<RuleName>,<AppPath>,<Protocol>,<RemoteServer>,<RemotePort>,<LocalPort>,<Direction>
*/

#define MA_FIREWALL_RULE_CONSTANT_STR								"Rule_%s"
#define MA_FIREWALL_RULE_REGISTRY_STR								"FirewallRules"
#define MA_FIREWALL_RULE_INI_STR									"ma_firewall_rules.ini"
#define MA_FIREWALL_RULE_IDENTIFIER_STR								"Identifier"
#define MA_FIREWALL_RULE_GUID_STR									"GUID"

/*macmnnsvc outgoing rules */
/*Rule_1 */
#define MA_FIREWALL_RULE_MACMNSVC_TCP_OUT_ALLOW_ALL_STR				"Allow Agent HTTP Server,%s,0,,,,,1"
/*Rule_2 */
#define MA_FIREWALL_RULE_MACMNSVC_UDP_OUT_ALLOW_ALL_STR				"Allow Agent UDP Client,%s,1,,,,,1"

/*macmnnsvc incoming rules */
/*Rule_3 - Dynamic */
#define MA_FIREWALL_RULE_MACMNSVC_TCP_IN_ALLOW_SERVER				"Allow Agent Wakeup,%s,0,,,,%s,0"
/*Rule_4 -  Dynamic*/
#define MA_FIREWALL_RULE_MACMNSVC_UDP_IN_ALLOW_ALL_STR				"Allow Agent UDP Discovery,%s,1,,,,%s,0"
/*Rule_5 */
#define MA_FIREWALL_RULE_MACMNSVC_UDP_COMPAT_IN_ALLOW_ALL_STR		"Allow Agent Compat Relay Discovery,%s,1,,,,8083,0"

/*masvc outgoing rules */
/*Rule_6 */
#define MA_FIREWALL_RULE_MASVC_TCP_OUT_ALLOW_ALL_STR				"Allow Agent TCP Traffic,%s,0,,,,,1"
/*Rule_7 */
#define MA_FIREWALL_RULE_MASVC_UDP_OUT_ALLOW_ALL_STR				"Allow Agent UDP Traffic,%s,1,,,,,1"

/*maconfig outgoing rules */
/*Rule_8 */
#define MA_FIREWALL_RULE_MACONFIG_TCP_OUT_ALLOW_ALL_STR				"Allow Agent Provisioning,%s,0,,,,,1"

/*updater outgoing rules */
/*Rule_9 */
#define MA_FIREWALL_RULE_MUE_TCP_OUT_ALLOW_ALL_STR					"Allow Agent Updater Engine TCP Traffic,%s,0,,,,,1"
/*Rule_10 */
#define MA_FIREWALL_RULE_MUE_UDP_OUT_ALLOW_ALL_STR					"Allow Agent Updater Engine UDP Traffic,%s,1,,,,,1"

/*repository mirror outgoing rules */
/*Rule_11*/
#define MA_FIREWALL_RULE_MAREPOMIRROR_TCP_OUT_ALLOW_ALL_STR			"Allow Agent Repository Mirror,%s,0,,,,,1"


//#define MA_EMAIL_ID      "noreply@mileaccess.com"
//#define MA_EMAIL_ID      "noreplymileaccess@gmail.com"
#define MA_COMMA   ','
#define MA_SPACE   ' '
#define MA_EMAIL_ID      "support@mileaccess.com"


#endif /* MA_GENERAL_DEFS_H_INCLUDED*/

