#ifndef MA_RELAY_DEFS_H_INCLUDED
#define MA_RELAY_DEFS_H_INCLUDED

/* Services, names , constants , datastore configurations */
#define MA_RELAY_SERVICE_NAME_STR                                     "ma.service.relay"
#define MA_RELAY_CLIENT_NAME_STR                                      "ma.client.relay"


/*  Relay service ePO/server policies */
#define MA_RELAY_SERVICE_SECTION_NAME_STR                             "RelayService"
#define MA_RELAY_KEY_IS_INSTALLED_INT                                 "IsInstalled"
#define MA_RELAY_KEY_IS_ENABLED_INT                                   "IsEnabled"
#define MA_RELAY_KEY_SERVER_PORT_INT                                  "RelayServerPort"
#define MA_RELAY_KEY_MULTICAST_ADDRESS_STR                            "RelayMulticastAddress"
#define	MA_RELAY_KEY_CONCURRENT_CONNECTIONS_LIMIT_INT	  			  "ConcurrentConnectionsLimit"
#define MA_RELAY_KEY_ENABLE_CLIENT_INT								  "EnableClient"

/*  Relay  service datastore settings*/
#define MA_RELAY_PATH_NAME_STR                                       "AGENT\\SERVICES\\RELAY"
/*  Relay service properties */
#define MA_RELAY_PROPERTIES_PATH_NAME_STR                            "AGENT\\SERVICES\\RELAY\\PROPERTIES"

/*  Relay service local Policies */
#define MA_RELAY_POLICIES_PATH_NAME_STR                              "AGENT\\SERVICES\\RELAY\\POLICIES"

/*  Relay service Data */
#define MA_RELAY_DATA_PATH_NAME_STR                                  "AGENT\\SERVICES\\RELAY\\DATA"

/*relay discovery message request/response*/
#define MA_RELAY_MSG_PROP_KEY_TYPE_STR		                         "prop.key.msg_type"
#define MA_RELAY_PORT_STR										     "prop.key.relay_port"

/* request message types */
#define MA_RELAY_MSG_PROP_VAL_REQUEST_RELAY_DISCOVERY_STR		     "prop.val.rqst.relay_discovery"

/* response message types */
#define MA_RELAY_MSG_PROP_VAL_RESPONSE_RELAY_DISCOVERY_STR			 "prop.val.resp.relay_discovery"


/* relay compat defs*/
#define MA_RELAY_COMPAT_PORT										 8083

#define MA_COMPAT_RELAY_SERVICE_ID									 "RELA"
#define MA_COMPAT_RELAY_DISCOVERY_MESSAGE							 "RELAY_RESPOND"


#endif /* MA_RELAY_DEFS_H_INCLUDED */

