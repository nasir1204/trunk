#ifndef MA_SERVICES_MANAGER_SERVICE_DEFS_H_INCLUDED
#define MA_SERVICES_MANAGER_SERVICE_DEFS_H_INCLUDED


/* Broker - Services Manager service properties field */
#define MA_MSGBUS_SERVICE_MANAGER_SERVICE_NAME_STR				"ma.msgbus.services_manager_service.service"
#define MA_MSGBUS_SERVICE_MANAGER_HOST_NAME_STR					"localhost"

/* Services Registration/Unregistration Notificaitons */
#define MA_MSGBUS_SERVICE_CHANGE_NOTIFICATION_TOPIC_STR			"ma.msgbus.service_change_notification"

#define MA_PROP_KEY_SERVICE_CHANGE_NOTIFICATION_STR             "prop.key.service_change_notification"                /* Property key */
#define MA_PROP_VALUE_NOTIFICATION_SERVICE_REGISTERED_STR       "prop.value.notification_service_registered"
#define MA_PROP_VALUE_NOTIFICATION_SERVICE_UNREGISTERED_STR     "prop.value.notification_service_unregistered"

#define MA_PROP_KEY_SERVICE_NAME_STR							"prop.key.service_name"                /* Property key */


/* Subscribers Registration/Unregistration Notificaitons */
#define MA_MSGBUS_SUBSCRIBER_CHANGE_NOTIFICATION_TOPIC_STR		   "ma.msgbus.subscriber_change_notification"

#define MA_PROP_KEY_SUBSCRIBER_CHANGE_NOTIFICATION_STR             "prop.key.subscriber_change_notification"                /* Property key */
#define MA_PROP_VALUE_NOTIFICATION_SUBSCRIBER_REGISTERED_STR       "prop.value.notification_subscriber_registered"
#define MA_PROP_VALUE_NOTIFICATION_SUBSCRIBER_UNREGISTERED_STR     "prop.value.notification_subscriber_unregistered"

#define MA_PROP_KEY_SUBSCRIBER_TOPIC_NAME_STR				        "prop.key.subscriber_topic_name"             /* Property key */
#define MA_PROP_KEY_SUBSCRIBER_PRODUCT_ID_STR				        "prop.key.subscriber_product_id"             /* Property key */


/* Requests handled by Service Manager Service */
#define MA_PROP_KEY_SERVICE_MANAGER_REQUEST_TYPE_STR		        "prop.key.service_manager_request_type"
#define MA_PROP_KEY_SERVICE_MANAGER_REPLY_TYPE_STR			        "prop.key.service_manager_reply_type"

#define MA_PROP_VALUE_GET_SUBSCRIBERS_LIST_REQUEST_STR		        "prop.value.get_subscribers_list_request"
#define MA_PROP_VALUE_GET_SERVICES_LIST_REQUEST_STR		            "prop.value.get_services_list_request"

#define MA_PROP_KEY_SUBSCRIBER_TOPIC_FILTER_STR						"prop.key.subscriber_topic_filter"     /* Value is subscriber topic filter */
#define MA_PROP_KEY_SERVICE_NAME_FILTER_STR						    "prop.key.service_name_filter"     /* Value is service name filter */

#define MA_SUBSCRIBER_TOPIC_NAME_STR			"subscriber.topic_name"
#define MA_SUBSCRIBER_PRODUCT_ID_STR			"subscriber.product_id"

#define MA_SERVICE_NAME_STR			            "service.name"
#define MA_SERVICE_PRODUCT_ID_STR			    "service.product_id"

/* The below macros to be moved in common header file */
#define MAX_PID_LEN                 16

#endif /* MA_SERVICES_MANAGER_SERVICE_DEFS_H_INCLUDED */

