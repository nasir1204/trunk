#ifndef MA_SEARCH_SERVICE_DEFS_H_INCLUDED
#define MA_SEARCH_SERVICE_DEFS_H_INCLUDED

/* Services, names , constants , datastore configurations */
#define MA_SEARCH_SERVICE_NAME_STR                                       "ma.service.search"
#define MA_SEARCH_CLIENT_NAME_STR                                        "ma.client.search"
#define MA_SEARCH_SERVICE_HOSTNAME_STR                                   "localhost"
#define MA_SEARCH_BASE_PATH_STR                                          "search"


/*  search service server policies */
#define MA_SEARCH_SERVICE_SECTION_NAME_STR                               "searchService"
#define MA_SEARCH_KEY_IS_INSTALLED_INT                                   "IsInstalled"
#define MA_SEARCH_KEY_IS_ENABLED_INT                                     "IsEnabled"


/*  search service datastore settings*/
#define MA_SEARCH_PATH_NAME_STR                                          "AGENT\\SERVICES\\SEARCH"
/*  search service properties */
#define MA_SEARCH_PROPERTIES_PATH_NAME_STR                               "AGENT\\SERVICES\\SEARCH\\PROPERTIES"

/*  search service local Policies */
#define MA_SEARCH_POLICIES_PATH_NAME_STR                                 "AGENT\\SERVICES\\SEARCH\\POLICIES"

/*  search service Data */
#define MA_SEARCH_DATA_PATH_NAME_STR                                     "AGENT\\SERVICES\\SEARCH\\DATA"


/* search service messages */
#define MA_SEARCH_SERVICE_MSG_TYPE                                       "ma.search.op.msg.type"
#define MA_SEARCH_SERVICE_MSG_REGISTER_TYPE                              "ma.search.op.msg.register.type"
#define MA_SEARCH_SERVICE_MSG_UNREGISTER_TYPE                            "ma.search.op.msg.unregister.type"
#define MA_SEARCH_SERVICE_MSG_GET_TYPE                                   "ma.search.op.msg.get.type"
#define MA_SEARCH_ID                                                     "ma.search.id"
#define MA_SEARCH_ID_EXIST                                               "ma.search.id.exist"
#define MA_SEARCH_ID_NOT_EXIST                                           "ma.search.id.not.exist"
#define MA_SEARCH_STATUS                                                 "ma.search.status"
#define MA_SEARCH_ID_WRITTEN                                             "ma.search.id.written"
#define MA_SEARCH_ID_DELETED                                             "ma.search.id.deleted"
#define MA_SEARCH_OP_SUCCESS                                             "ma.search.op.success"
#define MA_SEARCH_OP_FAILED                                              "ma.search.op.failed"
#define MA_SEARCH_OP_QUERY                                               "query"

/* search attributes */
#define MA_SEARCH_INFO_ATTR_ID                                         "ma.search.info.id"
#define MA_SEARCH_INFO_ATTR_SIZE                                       "ma.search.info.size"
#define MA_SEARCH_INFO_ATTR_TOPICS                                     "ma.search.info.topics"
#define MA_SEARCH_INFO_ATTR_BOARD                                      "ma.search.info.board"

#endif /* MA_SEARCH_SERVICE_DEFS_H_INCLUDED */
