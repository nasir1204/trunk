#ifndef MA_SEASON_SERVICE_DEFS_H_INCLUDED
#define MA_SEASON_SERVICE_DEFS_H_INCLUDED

/* Services, names , constants , datastore configurations */
#define MA_SEASON_SERVICE_NAME_STR                                       "ma.service.season"
#define MA_SEASON_CLIENT_NAME_STR                                        "ma.client.season"
#define MA_SEASON_SERVICE_HOSTNAME_STR                                   "localhost"
#define MA_SEASON_BASE_PATH_STR                                          "season"


/*  season service server policies */
#define MA_SEASON_SERVICE_SECTION_NAME_STR                               "SeasonService"
#define MA_SEASON_KEY_IS_INSTALLED_INT                                   "IsInstalled"
#define MA_SEASON_KEY_IS_ENABLED_INT                                     "IsEnabled"
#define MA_SEASON_TASK_TYPE                                              "Season"


/*  season service datastore settings*/
#define MA_SEASON_PATH_NAME_STR                                          "AGENT\\SERVICES\\SEASON"
/*  season service properties */
#define MA_SEASON_PROPERTIES_PATH_NAME_STR                               "AGENT\\SERVICES\\SEASON\\PROPERTIES"

/*  season service local Policies */
#define MA_SEASON_POLICIES_PATH_NAME_STR                                 "AGENT\\SERVICES\\SEASON\\POLICIES"

/*  season service Data */
#define MA_SEASON_DATA_PATH_NAME_STR                                     "AGENT\\SERVICES\\SEASON\\DATA"


/* season service messages */
#define MA_SEASON_SERVICE_MSG_TYPE                                       "ma.season.op.msg.type"
#define MA_SEASON_SERVICE_MSG_REGISTER_TYPE                              "ma.season.op.msg.register.type"
#define MA_SEASON_SERVICE_MSG_UNREGISTER_TYPE                            "ma.season.op.msg.unregister.type"
#define MA_SEASON_SERVICE_MSG_UPDATE_TYPE                                "ma.season.op.msg.update.type"
#define MA_SEASON_SERVICE_MSG_GET_TYPE                                   "ma.season.op.msg.get.type"
#define MA_SEASON_SERVICE_MSG_GET_BY_DAY_TYPE                           "ma.season.op.msg.get.by.day.type"
#define MA_SEASON_ID                                                     "ma.season.id"
#define MA_SEASON_ID_EXIST                                               "ma.season.id.exist"
#define MA_SEASON_ID_NOT_EXIST                                           "ma.season.id.not.exist"
#define MA_SEASON_STATUS                                                 "ma.season.status"
#define MA_SEASON_ID_WRITTEN                                             "ma.season.id.written"
#define MA_SEASON_ID_DELETED                                             "ma.season.id.deleted"
#define MA_SEASON_OP_SUCCESS                                             "ma.season.op.success"
#define MA_SEASON_OP_FAILED                                              "ma.season.op.failed"
#define MA_SEASON_WHICH_DAY                                               "which_day"

/* season attributes */
#define MA_SEASON_INFO_ATTR_EMAIL_ID                                     "email.id"
#define MA_SEASON_INFO_ATTR_CONTACT                                      "contact"
#define MA_SEASON_INFO_ATTR_DOMAIN                                       "domain"
#define MA_SEASON_INFO_ATTR_ATTRIBUTION                                  "attribution"
#define MA_SEASON_INFO_ATTR_DESCRIPTION                                  "description"
#define MA_SEASON_INFO_ATTR_ABOUT                                        "about"
#define MA_SEASON_INFO_ATTR_LOCATION                                     "location"
#define MA_SEASON_INFO_ATTR_FULL_NAME                                    "full_name"
#define MA_SEASON_INFO_ATTR_FOLLOWER_COUNT                               "follower_count"
#define MA_SEASON_INFO_ATTR_IMAGE_SMALL_URL                              "image_small_url"
#define MA_SEASON_INFO_ATTR_PIN_COUNT                                    "pin_count"
#define MA_SEASON_INFO_ATTR_PINNER_ID                                    "pinner_id"
#define MA_SEASON_INFO_ATTR_PROFILE_URL                                  "profile_url"
#define MA_SEASON_INFO_ATTR_REPIN_COUNT                                  "repin_count"
#define MA_SEASON_INFO_ATTR_DOMINANT_COLOR                               "dominant_count"
#define MA_SEASON_INFO_ATTR_LIKE_COUNT                                   "like_count"
#define MA_SEASON_INFO_ATTR_LINK                                         "link"
#define MA_SEASON_INFO_ATTR_URL                                          "url"
#define MA_SEASON_INFO_ATTR_WIDTH                                        "width"
#define MA_SEASON_INFO_ATTR_HEIGHT                                       "height"
#define MA_SEASON_INFO_ATTR_EMBED                                        "embed"
#define MA_SEASON_INFO_ATTR_IS_VIDEO                                     "is_video"
#define MA_SEASON_INFO_ATTR_ID                                           "id"
#define MA_SEASON_INFO_ATTR_TOPIC                                        "topic"
#define MA_SEASON_INFO_ATTR_BOARD                                        "board"
#define MA_SEASON_INFO_ATTR_SAVED_BY                                     "saved_by"
#define MA_SEASON_INFO_ATTR_BOARD_IMAGE_URL                                  "board_image_url"


#define MA_SEASON_SMALL_IMAGE_URL                                        "http://www.mileaccess.com:8080/autmunIS/media/compact/"
#define MA_SEASON_LARGE_IMAGE_URL                                        "http://www.mileaccess.com:8080/autmunIS/media/large/"

#endif /* MA_SEASON_SERVICE_DEFS_H_INCLUDED */
