#ifndef MA_PROPERTY_DEFS_H_INCLUDED
#define MA_PROPERTY_DEFS_H_INCLUDED

/* Services, names , constants , datastore configurations */
#define MA_PROPERTY_SERVICE_NAME_STR                                    "ma.service.property"
#define MA_PROPERTY_CLIENT_NAME_STR                                     "ma.client.property"
#define MA_PROPERTY_SERVICE_HOSTNAME_STR                                "localhost"


/*  Property service ePO/server policies */
#define MA_PROPERTY_SERVICE_SECTION_NAME_STR                            "PropertyService"
#define MA_PROPERTY_KEY_COLLECTION_TIMEOUT_INT                          "PropertyCollectionTimeout"
#define MA_PROPERTY_KEY_IS_INSTALLED_INT                                "IsInstalled"
#define MA_PROPERTY_KEY_IS_ENABLED_INT                                  "IsEnabled"
#define MA_PROPERTY_KEY_COLLECT_FULL_PROPS_INT                          "PropertyCollectFullProps"
#define MA_PROPERTY_KEY_COLLECT_SESSION_TIMEOUT_INT                     "PropertyCollectSessionTimeout"
#define MA_PROPERTY_KEY_COLLECT_IF_DELAYS_BY_DAYS_INT                   "PropertyCollectionIfDelayByDays"
						


/*  Property service datastore settings*/
#define MA_PROPERTY_PATH_NAME_STR                                       "AGENT\\SERVICES\\PROPERTY"
/*  Property service properties */
#define MA_PROPERTY_PROPERTIES_PATH_NAME_STR                            "AGENT\\SERVICES\\PROPERTY\\PROPERTIES"
#define MA_PROPERTY_PROPERTIES_KEY_LAST_COLLECTION_STATUS			    "PropertyLastCollectionStatus"
#define MA_PROPERTY_PROPERTIES_KEY_LAST_POLICY_ENFORCEMENT_STATUS		"PolicyLastEnforcementStatus"
#define MA_PROPERTY_PROPERTIES_CUSTOM_STR								"UserProperty"
#define MA_PROPERTY_PROPERTIES_CUSTOM1_STR								"UserProperty1"
#define MA_PROPERTY_PROPERTIES_CUSTOM2_STR								"UserProperty2"
#define MA_PROPERTY_PROPERTIES_CUSTOM3_STR								"UserProperty3"
#define MA_PROPERTY_PROPERTIES_CUSTOM4_STR								"UserProperty4"

/*  Property service local Policies */
#define MA_PROPERTY_POLICIES_PATH_NAME_STR                              "AGENT\\SERVICES\\PROPERTY\\POLICIES"

/*  Property service Data */
#define MA_PROPERTY_DATA_PATH_NAME_STR                                  "AGENT\\SERVICES\\PROPERTY\\DATA"
#define MA_PROPERTY_DATA_KEY_PROPERTY_VERSION_STR                       "PropertyVersion"
#define MA_PROPERTY_DATA_KEY_ASC_TIME_STR                               "LastASCTime"

#define MA_PROPERTY_DATA_PRODUCT_PROPERTIES_PATH_STR					"AGENT\\SERVICES\\PROPERTY\\DATA\\PRODUCT_PROPERTIES"

/* Property service messages */
#define MA_PROPERTY_MSG_KEY_TYPE_STR					                "prop.key.msg_type"
#define MA_PROPERTY_MSG_KEY_INITIATOR_STR                               "prop.key.initiator_id"
#define MA_PROPERTY_MSG_KEY_PROPS_TYPE_STR                              "prop.key.props_type"
#define MA_PROPERTY_MSG_KEY_RANDOM_INTERVALS_STR                        "prop.key.random_intervals"
#define MA_PROPERTY_MSG_KEY_ENFORCE_POLICY_STR                          "prop.key.enforce_policy"
#define MA_PROPERTY_MSG_TYPE_COLLECT_STR         			            "prop.key.property_collect"
#define MA_PROPERTY_MSG_TYPE_COLLECTED_STR			                    "prop.key.property_collected"
#define MA_PROPERTY_MSG_TYPE_COLLECT_FAILED_STR		                    "prop.key.property_collect_failed"
#define MA_PROPERTY_MSG_TYPE_COLLECT_SEND_STATUS_STR		            "prop.key.property_collect_send_status"

#define MA_PROPERTY_MSG_VALUE_PROPS_TYPE_INCR_STR						"0"

#define MA_PROPERTY_MSG_VALUE_PROPS_TYPE_FULL_STR						"1"

#define MA_PROPERTY_MSG_VALUE_PROPS_TYPE_VERSION_STR					"2"


#define MA_PROPERTY_MSG_KEY_SESSION_ID_INT      	                    "ma.property.session_id"
#define MA_PROPERTY_MSG_KEY_PROVIDER_ID_STR    		                    "ma.property.provider_id"
#define MA_PROPERTY_MSG_KEY_COLLECT_STATUS_INT                          "ma.property.collect_status"
#define	MA_PROPERTY_MSG_KEY_MINIMAL_PROPS_INT							"ma.property.minimal_props"

#define MA_PROPERTY_PUBSUB_TOPIC_COLLECT_STR    		                "ma.property.collect"
#define MA_PROPERTY_PUBSUB_TOPIC_COLLECT_STATUS_STR                     "ma.property.collect_send_status"

/*  Property service constants */
#define MA_PROPERTY_MSG_KEY_COLLECT_STATUS_KEY_NAME_STR					"ma.property.collect_status"

#define MA_PROPERTY_MSG_KEY_COLLLECT_SESSION_STR						"ma.property.collect_session_id"
#define MA_PROPERTY_MSG_KEY_PROVIDER_STR   								"ma.property.provider_id"

#define	MA_PROPERTY_GENERAL_SECTION_STR									"General"
#define	MA_PROPERTY_SYSTEMINFO_SECTION_STR								"SystemInfo"
#define	MA_PROPERTY_COMPUTERPROPERTIES_SECTION_STR						"ComputerProperties"

/*event on property collection success and failure*/
#define	MA_PROPERTY_COLLECTION_FAILURE_MESSAGE_STR						"Property collection failed for software %s"
#define	MA_PROPERTY_COLLECTION_TIMEOUT_MESSAGE_STR						"Property collection timeout for software %s"
#define	MA_PROPERTY_EVENT_THREAT_CATEGORY_STR							"ops"
#define	MA_PROPERTY_EVENT_THREAT_TYPE_STR								"Property Collection"
#define	MA_PROPERTY_TIMEOUT_EVENT_THREAT_TYPE_STR						"Property Collection Failure"
#define MA_PROPERTY_COLLECT_SUCCESS_EVENT_ID							2426
#define MA_PROPERTY_COLLECT_FAILURE_EVENT_ID							2427

#define MA_PROPERTY_COLLECT_FAILURE_ERROR_ID							83

#endif /* MA_PROPERTY_DEFS_H_INCLUDED*/

