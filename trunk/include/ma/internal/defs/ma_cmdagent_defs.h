#ifndef MA_CMDAGENT_DEFS_H_INCLUDED
#define MA_CMDAGENT_DEFS_H_INCLUDED

#include "ma/internal/defs/ma_io_defs.h"

#define MA_PROPERTY_COLLECT_SCHEDULER_TASK_ID           "ma.property.collect.scheduler_task.task_id"
#define MA_PROPERTY_COLLECT_SCHEDULER_TASK_NAME         "ma.property.collect.scheduler_task.task_name"

#define MA_POLICY_ENFORCE_TIMEOUT_TASK_ID				"ma.policy.enforce.timeout.task.id"
#define MA_POLICY_ENFORCE_TIMEOUT_TASK_NAME				"ma.policy.enforce.timeout.task.name"

#define MA_EVENT_TRIGGER_TASK_ID						"ma.event.service.priority.event.send.task"

#endif

