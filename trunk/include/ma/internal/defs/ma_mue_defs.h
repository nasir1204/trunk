#ifndef MA_MUE_DEFS_H_INCLUDED
#define MA_MUE_DEFS_H_INCLUDED

#include "ma/internal/defs/ma_updater_defs.h"

#define MA_MUEEVENT_PUBLISH_TOPIC				"ma.mue_event.publish"
#define MA_MUEEVENT_PROP_OPERATION_TYPE			"ma.mue_event.prop.operation_type"
#define MA_MUEEVENT_PROP_SESSION_ID				"ma.mue_event.prop.session_id"


#define MA_UPDATER_EVENT_THREAT_CATEGORY_STR	"ops"


typedef enum ma_update_error_e
{
	eUPDATE_ERROR_UNKNOWN = -1,
	eUPDATE_ERROR_START_OK = 0,	//!< update started OK, Happy
	eUPDATE_ERROR_OK =	0,	//!< update OK, Happy
	eUPDATE_ERROR_LATEST,		//!< Update OK, but just to tell running latest version
	eUPDATE_ERROR_FAILED_TO_START, //!< update not started
	eUPDATE_ERROR_CALLBACKINIT,	//!< Point product callback component can not be loaded
	eUPDATE_ERROR_REJECT,	//!< point product rejected the update
	eUPDATE_ERROR_CANCEL,	//!< user cancelled the update
	eUPDATE_ERROR_EOL,		//!< End of life for product/scanengine
	eUPDATE_ERROR_LOADCONFIG,		//!< Load config error
	eUPDATE_ERROR_DOWNLOAD,
	eUPDATE_ERROR_POSTPONE,
	eUPDATE_ERROR_REPLICATE,
	eUPDATE_ERROR_SERVICE_STOP,
	eUPDATE_ERROR_SERVICE_START,
	eUPDATE_ERROR_INVALID, //!< invalid files/invalid site
	eUPDATE_ERROR_INVALID_DATS, //!< create different code for invalid dats
	eUPDATE_ERROR_BACKUP,
	eUPDATE_ERROR_ROLLBACK,
	eUPDATE_ERROR_COPY,
	eUPDATE_ERROR_DISKSPACE,
	eUPDATE_ERROR_SETCONFIG,
	eUPDATE_ERROR_LICENSE_EXPIRED,
	eUPDATE_ERROR_REBOOT_PENDING, //!< update/deployment failed because reboot pending
	eUPDATE_ERROR_UPDATEOPTIONS,   //!< Something wrong with the format of UpdateOptions.ini
    eUPDATE_ERROR_SCRIP_ENGINE_TERMINATED_UNEXPECTEDLY, //!<Script engine terminated unexpectedly
    eUPDATE_ERROR_INVALID_PARAM,    //!< Invalid parameters are passed to the updater APIs
    eUPDATE_ERROR_UNZIP,    //!< unable to de-compress file
    eUPDATE_ERROR_NO_REPO,    //!< Unable to find valid repository
    eUPDATE_ERROR_UPDPLUGIN, //!<update callback plugin error
    eUPDATE_ERROR_NO_PRODUCTS,    //!< No qualifying product         
	eUPDATE_ERROR_STOP_INITIATED,	//!< stopUpdate() couldn't complete but just managed to initiate stop process.
	eUPDATE_ERROR_MEMORY, 		//Memory allocation problem
    eUPDATE_ERROR_ANOTHER_SESSION_RUNNING,
	eUPDATE_ERROR_CREATE_DIR,
	eUPDATE_ERROR_NETWORK
} ma_update_error_t;


enum MueUecOperation
{
	eOpUnknown,
	eOpProgress,
	eOpEvent,
	eOpShowUpdateDialog,
	eOpEndUpdateDialog,
};

enum muecError
{
	muecSuccess,
	muecBadCommandParams,
	muecBadScriptPath,
	muecFileDoesntExist,
	muecBadLocale,
	muecBadInitiator,
	muecUnInitialized,
	muecIpcNotInitialized,
	muecIpcFailed,
	muecRebootPending,
	muecFailed
};
			
/**
*These data were previousy strings.Strings are an inefficient and heavyweight
*way of doing this.Use enums instead
*/

enum muecInitiator
{
	i_Task,
	i_DeploymentTask,
	i_UpdateTask,
	i_OnDemand,
	i_OnDemandRollback,
	i_Automatic,
	i_Remediation,
	i_RemediationRollback,
	i_unknown
};

enum muecProgressEvent
{
	pe_Start		=	0,
	pe_Error		=	1,
	pe_Resuming	=	2,
	pe_Progress	=	3,
	pe_Finished	=	4,
	pe_Unknown	=	5
};

enum muecComponent
{
	c_Download	=	0,
	c_Upload		=	1,
	c_Script		=	2,	
	c_Signing	=	3,		
	c_Decompress	=	4,
	c_Site_lookup=	5,
};
			
enum muecUpdateDialogType
{
	dlg_reboot = 0,
	dlg_prgress = 1,
	dlg_postpone = 2,
	dlg_unknown = 4
};

enum muecUpdateDialogReturnType
{
	rt_reboot_postponed =	0,
	rt_reboot_now =	1,
	rt_reboot_timeout =	2,
	rt_reboot_no_ui =3,
	rt_postpone_declined = 4,
	rt_postpone_now = 5,
	rt_postpone_timeout = 6,
	rt_unknown = 7
};


#define EVENT_COMMON_UPDATE_SUCCESS		2401
#define EVENT_COMMON_UPDATE_FAIL		2402

#define EVENT_DEPLOYMENT_SUCCESS		2411
#define EVENT_DEPLOYMENT_FAIL			2412

#define MA_MUE_SHUTDOWN_GLOBAL_EVENT_NAME_STR						  L"Global\\MueShutdownEvent"

#define EVENT_PRODUCT_LICENSE_CHECK_FAILED							16029	


//I don't know whether these are valid states and what combinations will pass through as well
//Once we get the final values and combinations, let us do a proper fix
 
#define MA_PRODUCT_LICENSE_STATE_PAID								"paid" 
#define MA_PRODUCT_LICENSE_STATE_TRIAL								"trial" 
#define MA_PRODUCT_LICENSE_STATE_OVERUSE						    "overused"  

//For beta_resresh branch  "licensed"   <->    "LicensedOrTrialNotOveruse"
#define MA_PRODUCT_LICENSE_STATE_LICENSEDORTRIALNOTOVERUSE			"LicensedOrTrialNotOveruse" 

 


#define MA_PRODUCT_LICENSE_CHECK_TYPE								"License Check"
#define MA_PRODUCT_LICENSE_INVALID_THREAT_NAME						"Software %s can't install, license state \"%s\"."

#endif /* MA_MUE_DEFS_H_INCLUDED*/

