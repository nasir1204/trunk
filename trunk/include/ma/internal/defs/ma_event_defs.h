#ifndef MA_EVENT_DEFS_H_INCLUDED
#define MA_EVENT_DEFS_H_INCLUDED

/* Services, names , constants , datastore configurations */
#define MA_EVENT_SERVICE_NAME_STR                                       "ma.service.event"
#define MA_EVENT_CLIENT_NAME_STR                                        "ma.client.event"
#define MA_EVENT_SERVICE_HOSTNAME_STR                                   "localhost"


/*  Event service ePO/server policies */
#define MA_EVENT_SERVICE_SECTION_NAME_STR                               "EventService"
#define MA_EVENT_KEY_IS_INSTALLED_INT                                   "IsInstalled"
#define MA_EVENT_KEY_IS_ENABLED_INT                                     "IsEnabled"
#define MA_EVENT_KEY_FILTER_VERSION_STR                                 "EventFilterVersion"
/*TODO - can we make an array , yes we get string from epo ";" separated but why not ?*/
#define MA_EVENT_KEY_FILTERED_LIST_STR                                  "EventFilteredList"
#define MA_EVENT_KEY_IS_ENABLED_PRIORITY_FORWARD_INT			        "EventIsEnabledPriorityForward"
#define MA_EVENT_KEY_UPLOAD_TIMEOUT_INT                                 "EventUploadTimeout"
#define MA_EVENT_KEY_UPLOAD_THRESHOLD_INT				                "EventUploadThreshold"
#define MA_EVENT_KEY_PRIORITY_LEVEL_INT             			        "EventPriorityLevel"

#define MA_EVENT_DISK_SPACE_THRESHOLD_PERCENTAGE_INT			        "EventFreeDiskSpaceThresholdPercentage"
#define MA_EVENT_DISK_SPACE_THRESHOLD_MIN_INT						    "EventFreeDiskSpaceThreshold"

/*  Event service datastore settings*/
#define MA_EVENT_PATH_NAME_STR                                          "AGENT\\SERVICES\\EVENT"
/*  Event service properties */
#define MA_EVENT_PROPERTIES_PATH_NAME_STR                               "AGENT\\SERVICES\\EVENT\\PROPERTIES"
/*  Event service local Policies */
#define MA_EVENT_POLICIES_PATH_NAME_STR                                 "AGENT\\SERVICES\\EVENT\\POLICIES"
/*  Event service Data */
#define MA_EVENT_DATA_PATH_NAME_STR                                     "AGENT\\SERVICES\\EVENT\\DATA"


/* Event service messages */
/* ma event service incoming request messages. */
#define MA_EVENT_MSG_PROP_KEY_MSG_TYPE                                   "prop.key.msg_type"
#define MA_EVENT_MSG_PROP_KEY_EVENT_PRIORITY                             "prop.key.event_priority"
#define MA_EVENT_MSG_PROP_KEY_INITIATOR_STR                              "prop.key.initiator_id"
#define MA_EVENT_MSG_PROP_KEY_EVENT_FILTER_VERSION                       "prop.key.event_filter_version"
#define MA_EVENT_MSG_PROP_KEY_PRODUCT_ID		                         "prop.key.software_id"

#define MA_EVENT_MSG_PROP_VALUE_REQ_GET_EVENT_FILTER                      "prop.value.req.get_filter"
#define MA_EVENT_MSG_PROP_VALUE_REQ_POST_EVENT                            "prop.value.req.post_event"
#define MA_EVENT_MSG_PROP_VALUE_REQ_POST_CUSTOM_EVENT                     "prop.value.req.post_custom_event"
#define MA_EVENT_MSG_PROP_VALUE_REQ_UPLOAD_EVENT                          "prop.value.req.upload_event"
#define MA_CUSTOM_EVENT_MACHINEINFO_NODE								  "MachineInfo"

/* ma event service publish topic.*/
#define MA_EVENT_MSG_TOPIC_PUBSUB_EVENTS                                  "ma.topic.pubsub.events"
#define MA_EVENT_MSG_TOPIC_PUBSUB_CUSTOM_EVENTS                           "ma.topic.pubsub.custom_events"
#define MA_EVENT_MSG_TOPIC_PUBSUB_EVENT_FILTER                            "ma.topic.pubsub.event_filter"

/*ma event filter message payload keys */
#define MA_EVENT_MSG_PAYLOAD_KEY_DISABLED_EVENTS                          "payload.key.disabled_event"

/*compat event policy*/
#define key_event_policy_ini											 "eventpolicies.ini"
#define key_str_event_section											 "AgentEvents"
#define key_str_events_agentMode										 "AgPlcyAgentMode"
#define key_str_events_EnableEventTrigger 								 "AgPlcyEnableEventTrigger" 
#define key_str_events_EventTriggerThreshold 							 "AgPlcyEventTriggerThreshold"
#define key_str_events_EventDirectory         							 "AgPlcyEventDirectory"
#define key_str_events_EventFilterString  								 "AgPlcyEventFilterString"
#define key_str_events_EventAgentGuid 									 "AgPlcyEventAgentGUID" 
#define key_str_events_EventIPAddress 									 "AgPlcyEventAgentIPAddress" 
#define key_str_events_EventComputerName 								 "AgPlcyEventAgentComputerName" 
#define key_str_events_EventUser 										 "AgPlcyEventAgentUser"
#define key_str_events_EventOS 											 "AgPlcyEventAgentOS"
#define key_str_events_EventMacAddress 									 "AgPlcyEventAgentMacAddress" 

/*  Event service constants */
#include "ma/event/ma_event_client_defs.h"


#endif /* MA_EVENT_DEFS_H_INCLUDED*/

