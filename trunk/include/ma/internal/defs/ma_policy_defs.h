#ifndef MA_POLICY_DEFS_H_INCLUDED
#define MA_POLICY_DEFS_H_INCLUDED

/* Services, names , constants , datastore configurations */
#define MA_POLICY_SERVICE_NAME_STR                                          "ma.service.policy"
#define MA_POLICY_CLIENT_NAME_STR                                           "ma.client.policy"
#define MA_POLICY_SERVICE_HOSTNAME_STR                                      "localhost"


/*  Policy service ePO/server policies */
#define MA_POLICY_SERVICE_SECTION_NAME_STR                                  "PolicyService"
#define MA_POLICY_KEY_IS_INSTALLED_INT                                      "IsInstalled"
#define MA_POLICY_KEY_IS_ENABLED_INT                                        "IsEnabled"
#define MA_POLICY_KEY_ENFORCEMENT_TIMEOUT_INT                               "PolicyEnforcementTimeout"
#define MA_POLICY_KEY_ENFORCE_POLICY_STR                                    "EnforcePolicy"


/*  Policy service datastore settings*/
#define MA_POLICY_PATH_NAME_STR                                             "AGENT\\SERVICES\\POLICY"
/*  Policy service properties */
#define MA_POLICY_PROPERTIES_PATH_NAME_STR                                  "AGENT\\SERVICES\\POLICY\\PROPERTIES"
/*  Policy service local Policies */
#define MA_POLICY_POLICIES_PATH_NAME_STR                                    "AGENT\\SERVICES\\POLICY\\POLICIES"

/*  Policy service Data */
#define MA_POLICY_DATA_PATH_NAME_STR                                        "AGENT\\SERVICES\\POLICY\\DATA"


/* Policy service messages */
/* MA Policy Service Pub-Sub topics */
#define MA_POLICY_NOTIFICATION_TOPIC_STR        	        "ma.policy.notification" 
#define MA_TASK_CHANGED_TOPIC_STR                           "ma.policy.task_changed"

/* publish topics on success */
#define MA_POLICY_REFRESH_SUCCESS_TOPIC_STR                 "ma.policy.policy_refresh_success"
#define MA_POLICY_UPDATE_SUCCESS_TOPIC_STR                  "ma.policy.policy_update_success"
#define MA_TASK_UPDATE_SUCCESS_TOPIC_STR                    "ma.policy.task_update_success"

/* published topics on failures */
#define MA_POLICY_REFRESH_FAILURE_TOPIC_STR                 "ma.policy.policy_refresh_failure"
#define MA_POLICY_UPDATE_FAILURE_TOPIC_STR                  "ma.policy.policy_update_failure"
#define MA_TASK_UPDATE_FAILURE_TOPIC_STR                    "ma.policy.task_update_failure"

/* Internally policy service subscribed for policy service status */
#define MA_POLICY_STATUS_SUBSCRIBE_TOPIC_STR                "ma.policy.*"

/* MA_TASK_CHANGED_TOPIC_STR publish topic has the timestamp property */
#define MA_PROP_KEY_TASK_CHANGE_VERSION_STR                 "prop.key.task_change_timestamp"
/* Value is timestamp in string form */

/* MA Policy Service Request Response Messages */
#define MA_PROP_KEY_POLICY_REQUEST_TYPE_STR                 "prop.key.policy_request_type"
#define MA_PROP_KEY_POLICY_RESPONSE_TYPE_STR                "prop.key.policy_response_type"

#define MA_PROP_KEY_FORCE_POLICY_ENFORCEMENT_STR            "prop.key.force_policy_enforcement"       

/* REQUEST - Check New policies */
#define MA_PROP_VALUE_REFRESH_POLICIES_REQUEST_STR          "prop.value.refresh_policies_request"

/* REQUEST - Policy enforcement */
#define MA_PROP_VALUE_ENFORCE_POLICIES_REQUEST_STR          "prop.value.enforce_policies_request"

/* REQUEST - Get All Policies */
#define MA_PROP_VALUE_GET_POLICIES_REQUEST_STR              "prop.value.get_policies_request"
#define MA_PROP_VALUE_GET_POLICY_URI_LIST_REQUEST_STR       "prop.value.get_policy_uri_list_request"
#define MA_PROP_VALUE_GET_POLICIES_BY_URI_REQUEST_STR       "prop.value.get_policies_by_uri_request"

#define MA_PROP_VALUE_GET_SECTIONS_BY_URI_REQUEST_STR       "prop.value.get_sections_by_uri_request"
#define MA_PROP_VALUE_GET_SECTTINGS_BY_SECITON_REQUEST_STR  "prop.value.get_settings_by_section_request"

#define MA_PROP_KEY_SECTION_NAME_STR						"prop.key.section_name"		/* Propert key , value is section name */
#define MA_PROP_KEY_PRODUCT_ID_STR							"prop.key.product_id"		/* Propert key , value is product id */

/* REQUEST - Set Policies */
#define MA_PROP_VALUE_SET_POLICIES_REQUEST_STR              "prop.value.set_policies_request"

/* REQUEST - Policy enforcement confirmation */
#define MA_PROP_VALUE_POLICY_ENFORCEMENT_CONFIRMATION		"prop.value.policy_enforcement_confirmation"

#define MA_PROP_KEY_POLICY_ENFORCE_REASON_STR               "prop.key.policy_enforce_reason"         /* Property Key */
#define MA_PROP_KEY_POLICY_INITIATOR_STR                    "prop.key.initiator_id"
#define MA_PROP_VALUE_POLICY_TIMEOUT_STR                    "prop.value.policy_timeout"              /* Property Values */
#define MA_PROP_VALUE_POLICY_CHANGED_STR                    "prop.value.policy_changed"
#define MA_PROP_VALUE_SERVICE_STARTUP_STR                   "prop.value.service_startup"		/* on agent service start up */
#define MA_PROP_VALUE_POLICY_FORCED_ENFORCEMENT_STR         "prop.value.policy_forced_enforcement"

#define MA_PROP_KEY_POLICY_VERSION                          "prop.key.policy_version"
/* Property value contains policy version string */

#define MA_PROP_KEY_POLICY_REQUEST_URI_STR                   "prop.key.policy_request_uri"   /* Property Key */
/* Property value contains Software ID and user or location details */

#define MA_PROP_KEY_POLICY_NOTIFICATION_STR                 "prop.key.policy_notification"                /* Property key */
#define MA_PROP_VALUE_NOTIFICATION_POLICY_TIMEOUT_STR       "prop.value.notification_policy_timeout"      /* Property values */ 
#define MA_PROP_VALUE_NOTIFICATION_POLICY_CHANGED_STR       "prop.value.notification_policy_changed"        
#define MA_PROP_VALUE_NOTIFICATION_USER_CHANGED_STR         "prop.value.notification_user_changed"          
#define MA_PROP_VALUE_NOTIFICATION_LOCATION_CHANGED_STR     "prop.value.notification_location_changed"
#define MA_PROP_VALUE_NOTIFICATION_SERVICE_STARTUP_STR      "prop.value.notification_service_startup"
#define MA_PROP_VALUE_NOTIFICATION_FORCED_ENFORCEMENT_STR   "prop.value.notification_forced_enforcement"

/* property value is uri string */
#define MA_PROP_KEY_NOTIFICATION_URI_STR                    "prop.key.notification_uri"        /* Property Key */


#define MA_PROP_KEY_SOFTWARE_ID										"prop.key.software_id"
#define MA_PROP_KEY_ENFORCEMENT_STATUS								"prop.key.enforcement_status"

/*  Policy service constants */
/* Below macros are used for policy notification and consumer */
#define NOTIFY_TYPE_STR     "notification_type"
#define SOFTWARE_ID_STR     "software_id"
#define VERSION_STR         "version"
#define USER_ID_STR         "user_id"
#define SESSION_ID_STR      "session_id"
#define LOGON_STATE_STR     "logon_state"
#define LOCATION_ID_STR     "location_id"
#define PRODUCT_LIST_STR    "product_list"
#define USER_LIST_STR       "user_list"

/* Need this property in user change uri for back compat */
#define IS_USER_LOGON_FIRST_TIME	"is_user_log_on_first_time"

/* Below macros are used for policies */
#define PO_FEATURE_STR         "po_feature"
#define PO_CATEGORY_STR        "po_category"
#define PO_TYPE_STR            "po_type"
#define PO_NAME_STR            "po_name"
#define PSO_NAME_STR            "pso_name"
#define PSO_PARAM_INT_STR       "pso_param_int"
#define PSO_PARAM_STR_STR       "pso_param_Str"

#define NOT_LOGGED_ON_STR   "not_logged_on"
#define LOG_ON_STR          "log_on"
#define LOGGED_ON_STR       "logged_on"
#define LOG_OFF_STR         "log_off"

#define MA_POLICY_UPDATE_TIME_STR				            "LastPolicyUpdateTime"


/*event on policy enforcement success and failure*/
#define	MA_POLICY_ENFORCEMENT_FAILURE_MESSAGE_STR						"Policy enforcement failed for software %s"
#define	MA_POLICY_EVENT_THREAT_CATEGORY_STR								"ops"
#define	MA_POLICY_EVENT_THREAT_TYPE_STR									"Policy Enforcement"
#define MA_POLICY_ENFORCEMENT_SUCCESS_EVENT_ID							2421
#define MA_POLICY_ENFORCEMENT_FAILURE_EVENT_ID							2422
#define MA_POLICY_ENFORCMENT_ERROR										59

#endif /* MA_POLICY_DEFS_H_INCLUDED*/

