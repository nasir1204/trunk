#ifndef MA_LOGGER_DEFS_H_INCLUDED
#define MA_LOGGER_DEFS_H_INCLUDED

/* Services, names , constants , datastore configurations */
#define MA_LOGGER_SERVICE_NAME_STR                                          "ma.service.logger"
#define MA_LOGGER_CLIENT_NAME_STR                                           "ma.client.logger"
#define MA_LOGGER_SERVICE_HOSTNAME_STR                                      "localhost"


/* Logger service ePO/server policies */
#define MA_LOGGER_SERVICE_SECTION_NAME_STR                                  "LoggerService"
#define MA_LOGGER_KEY_IS_INSTALLED_INT                                      "IsInstalled"
#define MA_LOGGER_KEY_IS_ENABLED_INT                                        "IsEnabled"
#define MA_LOGGER_KEY_FILTER_PATTERN_STR                                    "LogFilterPattern"
#define MA_LOGGER_KEY_FORMAT_PATTERN_STR                                    "LogFormatPattern"
#define MA_LOGGER_KEY_LOG_SIZE_LIMIT_INT                                    "LogSizeLimit"
#define MA_LOGGER_KEY_LOG_ENABLE_ROLLOVER_INT                               "LogEnableRollover"
#define MA_LOGGER_KEY_LOG_MAX_ROLLOVER_INT                                  "LogMaxRollover"
#define MA_LOGGER_KEY_LOG_APP_LOGGING_ENABLED_INT							"IsApplicationLogEnabled"

/* Remote logging policies */
#define MA_LOGGER_KEY_IS_REMOTE_LOG_ENABLED_INT                             "IsRemoteLogEnabled"
#define MA_LOGGER_KEY_IS_LOG_RECORDING_ENABLED_INT                          "IsLogRecordingEnabled"
#define MA_LOGGER_KEY_LOG_RECORDS_SIZE_INT                                  "LogRecordsSize"
   
/*  Logger service datastore settings*/
#define MA_LOGGER_PATH_NAME_STR                                             "AGENT\\SERVICES\\LOGGER"
/*  Logger service properties */
#define MA_LOGGER_PROPERTIES_PATH_NAME_STR                                  "AGENT\\SERVICES\\LOGGER\\PROPERTIES"
/*  Logger service Policies */
#define MA_LOGGER_POLICIES_PATH_NAME_STR                                    "AGENT\\SERVICES\\LOGGER\\POLICIES"
/*  Logger service Data */
#define MA_LOGGER_DATA_PATH_NAME_STR                                        "AGENT\\SERVICES\\LOGGER\\DATA"

/* Logger service messages */
#define MA_PROP_KEY_LOGGER_REQUEST_TYPE_STR                                 "prop.key.logger_request"

#define MA_PROP_VALUE_LOG_LOCALIZE_MESSAGE_STR 	                            "prop.value.log_localize_message"
#define MA_PROP_VALUE_GET_PRODUCTS_STR 										"prop.value.get_products"
#define MA_PROP_VALUE_GET_PRODUCT_LOG_FILES_STR 							"prop.value.get_product_log_files"
#define MA_PROP_VALUE_GET_PRODUCT_LOG_PATH_STR 								"prop.value.get_product_log_path"

#define MA_PROP_KEY_LOGGER_RESPONSE_TYPE_STR                                 "prop.key.logger_response"

/*  Logger service constants */
#define MA_LOG_AGENT_FILE_NAME_STR                                          "masvc"
#define MA_LOG_COMMON_SVCS_FILE_NAME_STR                                    "macmnsvc"
#define MA_LOG_COMPAT_FILE_NAME_STR                                         "macompatsvc"
#define MA_LOG_DEFAULT_FILTER                                               "*.Info"
#define MA_LOG_DEBUG_FILTER                                                 "*.Debug"
#define MA_LOG_TRACE_FILTER                                                 "*.Trace"
#define MA_LOG_ERROR_FILTER                                                 "*.Error"

/* Publish log message keys */
#define MA_LOG_MSG_KEY_TIME_STR         									"Time"
#define MA_LOG_MSG_KEY_SEVERITY_STR     									"Severity"
#define MA_LOG_MSG_KEY_FACILITY_STR     									"Facility"
#define MA_LOG_MSG_KEY_MESSAGE_STR      									"Message"

#define MA_PRODUCT_ID_STR         											"product_id"
#define MA_PRODUCT_NAME_STR         										"product_name"
#define MA_LOG_FILE_NAME_STR         										"log_file_name"
#define MA_LOG_FILE_FULL_PATH_STR         									"log_file_full_path"

#define REG_SOFTWARE_ROOT_STR												"Software"
#define REG_APPLICATIONS_PATH_STR											"McAfee\\Agent\\Applications"
#define REG_PRODUCT_DISPLAY_NAME_STR										"ProductName"
#define REG_PRODUCT_LOG_FILE_PATH											"LogFilesPath"
#define REG_PRODUCT_LOG_FILE_PATTERN_PATH									"LogFilePattern"

#endif /* MA_LOGGER_DEFS_H_INCLUDED*/

