#ifndef MA_DATACHANNEL_DEFS_H_INCLUDED
#define MA_DATACHANNEL_DEFS_H_INCLUDED

/* Services, names , constants , datastore configurations */
#define MA_DATACHANNEL_SERVICE_NAME_STR                                     "epo.service.datachannel"
#define MA_DATACHANNEL_CLIENT_NAME_STR                                      "epo.client.datachannel"


/*  DataChannel service ePO/server policies */
#define MA_DATACHANNEL_SERVICE_SECTION_NAME_STR                            "DatachannelService"
#define MA_DATACHANNEL_KEY_IS_INSTALLED_INT                                "IsInstalled"
#define MA_DATACHANNEL_KEY_IS_ENABLED_INT                                  "IsEnabled"
#define MA_DATACHANNEL_KEY_MAX_ITEMS									   "MaxItems"
/*  DataChannel service datastore settings*/
#define MA_DATACHANNEL_PATH_NAME_STR                                       "AGENT\\SERVICES\\DATACHANNEL"
/*  DataChannel service properties */
#define MA_DATCHANNEL_PROPERTIES_PATH_NAME_STR                             "AGENT\\SERVICES\\DATACHANNEL\\PROPERTIES"

/*  DataChannel service local Policies */
#define MA_DATACHANNEL_POLICIES_PATH_NAME_STR                              "AGENT\\SERVICES\\DATACHANNEL\\POLICIES"

/*  DataChannel service Data */
#define MA_DATACHANNEL_DATA_PATH_NAME_STR                                  "AGENT\\SERVICES\\DATACHANNEL\\DATA"

/* MA Data channel Service Request Response Messages */

/* REQUEST - Message available Request */


/*  DataChannel service constants */
#define EPO_DATACHANNEL_STARTUP_UPLOAD_TIMEOUT						(10 * 1000)
#define EPO_DATACHANNEL_IMMEDIATE_ROUTING_TIMEOUT					(1)
#define EPO_DATACHANNEL_MAINTANANCE_TIMEOUT							(60 * 1000)
#define EPO_DATACHANNEL_SPIPE_RETRY_TIMEOUT							(10 * 1000)

#define EPO_DATACHANNEL_MESSAGE_BUS_PROP_STATUS						"epo.datachannel.msgbus.response.prop.status"
#define EPO_DATACHANNEL_MESSAGE_BUS_PROP_REASON						"epo.datachannel.msgbus.response.prop.reason"

#define EPO_DATACHANNEL_NOTIFICATION_PUBLISHER_TOPIC_STR			"epo.datachannel.delivery.notification"


#define EPO_DATACHANNEL_ITEM_PROP_MESSAGE_TYPE_STR					"epo.datachannel.message.prop.messagetype"
#define EPO_DATACHANNEL_ITEM_PROP_TTL_STR							"epo.datachannel.message.prop.ttl"
#define EPO_DATACHANNEL_ITEM_PROP_FLAGS_STR							"epo.datachannel.message.prop.flags"
#define EPO_DATACHANNEL_ITEM_PROP_ORIGIN_STR						"epo.datachannel.message.prop.origin"
#define EPO_DATACHANNEL_ITEM_PROP_CORRELATION_ID_STR				"epo.datachannel.message.prop.correlation_id"
#define EPO_DATACHANNEL_ITEM_PROP_STATUS_STR						"epo.datachannel.message.prop.status"
#define EPO_DATACHANNEL_ITEM_PROP_REASON_STR						"epo.datachannel.message.prop.reason"
#define EPO_DATACHANNEL_ITEM_PROP_TIMECREATED_STR					"epo.datachannel.message.prop.timecreated"


#define EPO_DATACHANNEL_MESSAGES_TYPE_STR							"epo.datachannel.msgbus.request.type"

/*This is used by DC service to identify messages which are given by Compat service if it fails to deliver the items to LPC products*/
#define EPO_DATACHANNEL_MESSAGES_TYPE_COMPAT_STR					"epo.datachannel.msgbus.request.type.compat_requeue"		
/*This is used by DC service to identify messages which are requesting any undelivered messages for a particular topic */	
#define EPO_DATACHANNEL_MESSAGES_TYPE_SUBSCRIPTION_STR				"epo.datachannel.msgbus.request.type.subscription"	
/*This is used by DC service to identify the messages to be uploaded to ePO.*/
#define EPO_DATACHANNEL_MESSAGES_TYPE_MSGUPLOAD_STR					"epo.datachannel.msgbus.request.type.msgupload"			
/*This is used by DC service to identify any message available request received in http_server*/
#define EPO_DATACHANNEL_MESSAGES_TYPE_MSGAVAILABLE_STR				"epo.datachannel.msgbus.request.type.msgavailable"		

/*This is set in messages sent to client for responses got from ePO which are distributed to subscribers in the client.*/
#define EPO_DATACHANNEL_MESSAGES_TYPE_MSGRESPONSE_STR				"epo.datachannel.msgbus.request.type.msgresponse"			
/*This is set in messages sent to client for notifying them of the status of the message whether it was sent to ePO or it was purged*/
#define EPO_DATACHANNEL_MESSAGES_TYPE_NOTIFICATION_STR				"epo.datachannel.msgbus.request.type.notification"		


#define MA_DATACHANNEL_MAX_ID_LEN									256
#define MA_DATACHANNEL_MAX_TTL										(60*60*24*7)
#define MA_DATACHANNEL_MAX_ITEMS									64
#define MA_DATACHANNEL_MAX_RETRY_TIMEOUT							320 * 1000
#endif /* MA_DATACHANNEL_DEFS_H_INCLUDED*/

