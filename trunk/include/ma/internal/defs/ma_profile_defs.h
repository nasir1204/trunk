#ifndef MA_PROFILE_SERVICE_DEFS_H_INCLUDED
#define MA_PROFILE_SERVICE_DEFS_H_INCLUDED

/* Services, names , constants , datastore configurations */
#define MA_PROFILE_SERVICE_NAME_STR                                       "ma.service.profile"
#define MA_PROFILE_CLIENT_NAME_STR                                        "ma.client.profile"
#define MA_PROFILE_SERVICE_HOSTNAME_STR                                   "localhost"
#define MA_PROFILER_BASE_PATH_STR                                          "profiler"


/*  Profile service server policies */
#define MA_PROFILE_SERVICE_SECTION_NAME_STR                               "ProfileService"
#define MA_PROFILE_KEY_IS_INSTALLED_INT                                   "IsInstalled"
#define MA_PROFILE_KEY_IS_ENABLED_INT                                     "IsEnabled"


/*  Profile service datastore settings*/
#define MA_PROFILE_PATH_NAME_STR                                          "AGENT\\SERVICES\\PROFILE"
/*  Profile service properties */
#define MA_PROFILE_PROPERTIES_PATH_NAME_STR                               "AGENT\\SERVICES\\PROFILE\\PROPERTIES"

/*  Profile service local Policies */
#define MA_PROFILE_POLICIES_PATH_NAME_STR                                 "AGENT\\SERVICES\\PROFILE\\POLICIES"

/*  Profile service Data */
#define MA_PROFILE_DATA_PATH_NAME_STR                                     "AGENT\\SERVICES\\PROFILE\\DATA"


/* profile service messages */
#define MA_PROFILE_SERVICE_MSG_TYPE                                       "ma.profile.op.msg.type"
#define MA_PROFILE_SERVICE_MSG_REGISTER_TYPE                              "ma.profile.op.msg.register.type"
#define MA_PROFILE_SERVICE_MSG_UNREGISTER_TYPE                            "ma.profile.op.msg.unregister.type"
#define MA_PROFILE_SERVICE_MSG_UPDATE_TYPE                                "ma.profile.op.msg.update.type"
#define MA_PROFILE_SERVICE_MSG_GET_TYPE                                   "ma.profile.op.msg.get.type"
#define MA_PROFILE_SERVICE_MSG_GET_JSON_TYPE                              "ma.profile.op.msg.get.json.type"
#define MA_PROFILE_ID                                                     "ma.profile.id"
#define MA_PROFILE_ID_EXIST                                               "ma.profile.id.exist"
#define MA_PROFILE_ID_NOT_EXIST                                           "ma.profile.id.not.exist"
#define MA_PROFILE_STATUS                                                 "ma.profile.status"
#define MA_PROFILE_ID_WRITTEN                                             "ma.profile.id.written"
#define MA_PROFILE_ID_DELETED                                             "ma.profile.id.deleted"

/* profile attributes */
#define MA_PROFILE_INFO_ATTR_EMAIL_ID                                     "email"
#define MA_PROFILE_INFO_ATTR_NAME                                         "name"
#define MA_PROFILE_INFO_ATTR_CONTACT                                      "contact"
#define MA_PROFILE_INFO_ATTR_FROM_ADDRESS                                 "from.address"
#define MA_PROFILE_INFO_ATTR_FROM_PINCODE                                 "from.pincode"
#define MA_PROFILE_INFO_ATTR_TO_ADDRESS                                   "to.address"
#define MA_PROFILE_INFO_ATTR_TO_PINCODE                                   "to.pincode"
#define MA_PROFILE_INFO_ATTR_USER_TYPE                                    "user.type"
#define MA_PROFILE_INFO_ATTR_PRIVILEGE_TYPE                               "privilege.type"
#define MA_PROFILE_INFO_ATTR_PASSWORD                                     "password"
#define MA_PROFILE_INFO_ATTR_STATUS                                       "status"
#define MA_PROFILE_INFO_ATTR_LAST_LOGIN                                   "last.login" 
#define MA_PROFILE_INFO_ATTR_FROM_CITY                                    "from.city"
#define MA_PROFILE_INFO_ATTR_TO_CITY                                      "to.city"
#define MA_PROFILE_INFO_ATTR_ORDER_COUNT                                  "order.count"
#define MA_PROFILE_INFO_ATTR_LAST_SEEN                                    "last.seen"
#define MA_PROFILE_INFO_ATTR_BOARDS                                       "boards"
#define MA_PROFILE_INFO_ATTR_REFERRED_BY                                  "referred.by"
#define MA_PROFILE_INFO_ATTR_PROFILE_URL                                  "url"
#define MA_PROFILE_INFO_ATTR_SUBSCRIBED                                   "subscribed"
#define MA_PROFILE_INFO_ATTR_GENDER                                       "gender"
#define MA_PROFILE_INFO_ATTR_DOB                                          "dob"
#define MA_PROFILE_INFO_ATTR_GSTIN                                        "gstin"
#define MA_PROFILE_INFO_ATTR_SSN                                        "ssn"
#define MA_PROFILE_INFO_ATTR_CHANNEL                                    "channel"


/* admin profile */
#define MA_PROFILE_INFO_ADMIN_MAIL_ID                                     "admin@mileaccess.com"
#define MA_PROFILE_INFO_ADMIN_PASSWORD                                    "admin@mileaccess"
#define MA_PROFILE_INFO_ADMIN                                             "admin"

#endif /* MA_PROFILE_SERVICE_DEFS_H_INCLUDED */
