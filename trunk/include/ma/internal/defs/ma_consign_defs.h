#ifndef MA_CONSIGN_SERVICE_DEFS_H_INCLUDED
#define MA_CONSIGN_SERVICE_DEFS_H_INCLUDED

/* Services, names , constants , datastore configurations */
#define MA_CONSIGN_SERVICE_NAME_STR                                       "ma.service.consign"
#define MA_CONSIGN_CLIENT_NAME_STR                                        "ma.client.consign"
#define MA_CONSIGN_SERVICE_HOSTNAME_STR                                   "localhost"
#define MA_CONSIGNOR_BASE_PATH_STR                                          "consignor"


/*  Profile service server policies */
#define MA_CONSIGN_SERVICE_SECTION_NAME_STR                               "ProfileService"
#define MA_CONSIGN_KEY_IS_INSTALLED_INT                                   "IsInstalled"
#define MA_CONSIGN_KEY_IS_ENABLED_INT                                     "IsEnabled"


/*  Profile service datastore settings*/
#define MA_CONSIGN_PATH_NAME_STR                                          "AGENT\\SERVICES\\CONSIGN"
/*  Profile service properties */
#define MA_CONSIGN_PROPERTIES_PATH_NAME_STR                               "AGENT\\SERVICES\\CONSIGN\\PROPERTIES"

/*  Profile service local Policies */
#define MA_CONSIGN_POLICIES_PATH_NAME_STR                                 "AGENT\\SERVICES\\CONSIGN\\POLICIES"

/*  Profile service Data */
#define MA_CONSIGN_DATA_PATH_NAME_STR                                     "AGENT\\SERVICES\\CONSIGN\\DATA"


/* consign service messages */
#define MA_CONSIGN_SERVICE_MSG_TYPE                                       "ma.consign.op.msg.type"
#define MA_CONSIGN_SERVICE_MSG_REGISTER_TYPE                              "ma.consign.op.msg.register.type"
#define MA_CONSIGN_SERVICE_MSG_UNREGISTER_TYPE                            "ma.consign.op.msg.unregister.type"
#define MA_CONSIGN_SERVICE_MSG_UPDATE_TYPE                                "ma.consign.op.msg.update.type"
#define MA_CONSIGN_SERVICE_MSG_GET_TYPE                                   "ma.consign.op.msg.get.type"
#define MA_CONSIGN_SERVICE_MSG_GET_JSON_TYPE                              "ma.consign.op.msg.get.json.type"
#define MA_CONSIGN_SERVICE_MSG_GET_RAN_DISTANCE_TYPE                          "ma.consign.op.msg.get.ran.distance.type"
#define MA_CONSIGN_ID                                                     "ma.consign.id"
#define MA_CONSIGN_ID_EXIST                                               "ma.consign.id.exist"
#define MA_CONSIGN_ID_NOT_EXIST                                           "ma.consign.id.not.exist"
#define MA_CONSIGN_STATUS                                                 "status"
#define MA_CONSIGN_ID_WRITTEN                                             "ma.consign.id.written"
#define MA_CONSIGN_ID_DELETED                                             "ma.consign.id.deleted"
#define MA_CONSIGN_RAN_DISTANCE                                           "ran.distance"

/* consign attributes */
#define MA_CONSIGN_INFO_ATTR_CONTACT                                      "contact"
#define MA_CONSIGN_INFO_ATTR_DATE                                         "date"
#define MA_CONSIGN_INFO_ATTR_PENDING_ORDERS                               "pendingorders"
#define MA_CONSIGN_INFO_ATTR_COMPLETED_ORDERS                             "completedorders"



#endif /* MA_CONSIGN_SERVICE_DEFS_H_INCLUDED */
