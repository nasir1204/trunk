#ifndef MA_BOOKING_SERVICE_DEFS_H_INCLUDED
#define MA_BOOKING_SERVICE_DEFS_H_INCLUDED

/* Services, names , constants , datastore configurations */
#define MA_BOOKING_SERVICE_NAME_STR                                       "ma.service.booking"
#define MA_BOOKING_CLIENT_NAME_STR                                        "ma.client.booking"
#define MA_BOOKING_SERVICE_HOSTNAME_STR                                   "localhost"
#define MA_BOOKING_BASE_PATH_STR                                          "booker"


/*  Booking service ePO/server policies */
#define MA_BOOKING_SERVICE_SECTION_NAME_STR                               "BookingService"
#define MA_BOOKING_KEY_IS_INSTALLED_INT                                   "IsInstalled"
#define MA_BOOKING_KEY_IS_ENABLED_INT                                     "IsEnabled"
#define MA_BOOKING_TASK_TYPE                                              "Booking"


/*  Booking service datastore settings*/
#define MA_BOOKING_PATH_NAME_STR                                          "AGENT\\SERVICES\\BOOKING"
/*  Booking service properties */
#define MA_BOOKING_PROPERTIES_PATH_NAME_STR                               "AGENT\\SERVICES\\BOOKING\\PROPERTIES"

/*  Booking service local Policies */
#define MA_BOOKING_POLICIES_PATH_NAME_STR                                 "AGENT\\SERVICES\\BOOKING\\POLICIES"

/*  Booking service Data */
#define MA_BOOKING_DATA_PATH_NAME_STR                                     "AGENT\\SERVICES\\BOOKING\\DATA"


/* booking service messages */
#define MA_BOOKING_SERVICE_MSG_TYPE                                       "ma.booking.op.msg.type"
#define MA_BOOKING_SERVICE_MSG_ORDER_CREATE_TYPE                          "ma.booking.op.msg.order.create.type"
#define MA_BOOKING_SERVICE_MSG_ORDER_MODIFY_TYPE                          "ma.booking.op.msg.order.modify.type"
#define MA_BOOKING_SERVICE_MSG_ORDER_DELETE_TYPE                          "ma.booking.op.msg.order.delete.type"
#define MA_BOOKING_SERVICE_MSG_ORDER_UPDATE_STATUS_TYPE                   "ma.booking.op.msg.order.udpate.status.type"
#define MA_BOOKING_SERVICE_MSG_GET_TYPE                                   "ma.booking.op.msg.get.type"
#define MA_BOOKING_SERVICE_MSG_GET_JSON                                   "ma.booking.op.msg.get.json"
#define MA_BOOKING_SERVICE_MSG_BOOKING_DELETE_TYPE                        "ma.booking.op.msg.delete.type"
#define MA_BOOKING_ID                                                     "ma.booking.id"
#define MA_BOOKING_CRN                                                    "ma.booking.crn"
#define MA_BOOKING_ID_EXIST                                               "ma.booking.id.exist"
#define MA_BOOKING_ID_NOT_EXIST                                           "ma.booking.id.not.exist"
#define MA_BOOKING_STATUS                                                 "ma.booking.status"
#define MA_BOOKING_ID_WRITTEN                                             "ma.booking.id.written"
#define MA_BOOKING_ID_UPDATED                                             "ma.booking.id.updated"
#define MA_BOOKING_ID_DELETED                                             "ma.booking.id.deleted"

/* booking attributes */
#define MA_BOOKING_INFO_ATTR_EMAIL_ID                                     "email.id"
#define MA_BOOKING_INFO_ATTR_NAME                                         "name"
#define MA_BOOKING_INFO_ATTR_CONTACT                                      "contact"
#define MA_BOOKING_INFO_ATTR_FROM_ADDRESS                                 "from.address"
#define MA_BOOKING_INFO_ATTR_FROM_PINCODE                                 "from.pincode"
#define MA_BOOKING_INFO_ATTR_TO_ADDRESS                                   "to.address"
#define MA_BOOKING_INFO_ATTR_TO_PINCODE                                   "to.pincode"
#define MA_BOOKING_INFO_ATTR_USER_TYPE                                    "user.type"
#define MA_BOOKING_INFO_ATTR_PRIVILEGE_TYPE                               "privilege.type"
#define MA_BOOKING_INFO_ATTR_PICKER_CONTACT                                     "picker.contact"
#define MA_BOOKING_INFO_ATTR_STATUS                                       "status"
#define MA_BOOKING_INFO_ATTR_LAST_LOGIN                                   "last.login" 
#define MA_BOOKING_INFO_ATTR_ID                                           "id"
#define MA_BOOKING_INFO_ATTR_SERVICE_TYPE                                 "service.type"
#define MA_BOOKING_INFO_ATTR_DELIVERY_TYPE                                "delivery.type"
#define MA_BOOKING_INFO_ATTR_RECIPIENT                                    "recipient"
#define MA_BOOKING_INFO_ATTR_RECIPIENT_CONTACT                            "recipient.contact"
#define MA_BOOKING_INFO_ATTR_PICKUP_DATE                                  "pickup.date"
#define MA_BOOKING_INFO_ATTR_DELIVER_DATE                                 "deliver.date"
#define MA_BOOKING_INFO_ATTR_FROM_CITY                                    "from.city"
#define MA_BOOKING_INFO_ATTR_FROM_STATE                                   "from.state"
#define MA_BOOKING_INFO_ATTR_FROM_COUNTRY                                 "from.country"
#define MA_BOOKING_INFO_ATTR_TO_CITY                                      "to.city"
#define MA_BOOKING_INFO_ATTR_TO_STATE                                     "to.state"
#define MA_BOOKING_INFO_ATTR_TO_COUNTRY                                   "to.country"
#define MA_BOOKING_INFO_ATTR_PRICE                                        "price"
#define MA_BOOKING_INFO_ATTR_DISCOUNT                                     "discount"
#define MA_BOOKING_INFO_ATTR_PICKER_EMAIL_ID                              "picker.emailid"
#define MA_BOOKING_INFO_ATTR_DELIVER_EMAIL_ID                             "deliver.emailid"
#define MA_BOOKING_INFO_ATTR_DETAILS                                      "details"
#define MA_BOOKING_INFO_ATTR_PAID_TYPE                                    "paid.type"
#define MA_BOOKING_INFO_ATTR_DISTANCE                                     "distance"
#define MA_BOOKING_INFO_ATTR_PICK_TIME                                    "pick.time"
#define MA_BOOKING_INFO_ATTR_DELIVER_TIME                                 "deliver.time"
#define MA_BOOKING_INFO_ATTR_WEIGHT                                       "weight"
#define MA_BOOKING_INFO_ATTR_LENGTH                                       "length"
#define MA_BOOKING_INFO_ATTR_ESTIMATE_DELIVERY                            "estimate.delivery"
#define MA_BOOKING_INFO_ATTR_LOCATION_TYPE                                "location.type"
#define MA_BOOKING_INFO_ATTR_TRANSIT                                      "transit"
#define MA_BOOKING_INFO_ATTR_TRANSITIONS                                  "transitions"
#define MA_BOOKING_INFO_ATTR_TRANSITS                                     "transits"
#define MA_BOOKING_INFO_ATTR_ACTIVE                                       "active"
#define MA_BOOKING_INFO_ATTR_PACKAGE_URL                                  "package.url"
#define MA_BOOKING_INFO_ATTR_PAYMENT_STATUS                               "payment.status"
#define MA_BOOKING_INFO_ATTR_DELIVERY_NOTE                                "delivery.note"

#endif /* MA_BOOKING_SERVICE_DEFS_H_INCLUDED */
