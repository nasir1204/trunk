#ifndef MA_SENSOR_DEFS_H_INCLUDED
#define MA_SENSOR_DEFS_H_INCLUDED

/* Services, names , constants , datastore configurations */
#define MA_SENSOR_SERVICE_NAME_STR                                      "ma.service.sensor"
#define MA_SENSOR_CLIENT_NAME_STR                                       "ma.client.sensor"


/*  Sensor service ePO/server policies */
#define MA_SENSOR_SERVICE_SECTION_NAME_STR                              "SensorService"
#define MA_SENSOR_KEY_IS_INSTALLED_INT                                  "IsInstalled"
#define MA_SENSOR_KEY_IS_ENABLED_INT                                    "IsEnabled"


/*  Sensor service datastore settings*/
#define MA_SENSOR_PATH_NAME_STR                                       "AGENT\\SERVICES\\SENSOR"
/*  Sensor service properties */
#define MA_SENSOR_PROPERTIES_PATH_NAME_STR                              "AGENT\\SERVICES\\SENSOR\\PROPERTIES"

/*  Sensor service local Policies */
#define MA_SENSOR_POLICIES_PATH_NAME_STR                                "AGENT\\SERVICES\\SENSOR\\POLICIES"

/*  Sensor service Data */
#define MA_SENSOR_DATA_PATH_NAME_STR                                    "AGENT\\SERVICES\\SENSOR\\DATA"


/* Sensor service messages */
#define MA_SENSOR_SERVICE_MSG_TYPE                                  "ma.sensor.msg.type"
#define MA_SENSOR_SERVICE_MSG_SUB_TYPE                              "ma.sensor.msg.sub.type"
#define MA_SENSOR_REPLY_STATUS_STR                                  "ma.sensor.reply"

/* reply status message type(s) */
#define MA_SENSOR_REPLY_STATUS_SUCCESS                              "ma.sensor.reply.success"
#define MA_SENSOR_REPLY_STATUS_FAILED                               "ma.sensor.reply.failed"

#define MA_SENSOR_TRAY_STR                                          "ma.sensor.tray"
#define MA_SENSOR_TYPE_USER_STR                                     "ma.sensor.user"
#define MA_SENSOR_TYPE_POWER_STR                                    "ma.sensor.power"
#define MA_SENSOR_TYPE_IDLE_STR                                     "ma.sensor.idle"
#define MA_SENSOR_TYPE_NETWORK_STR                                  "ma.sensor.network"
#define MA_SENSOR_TYPE_SYSTEM_STR                                   "ma.sensor.system"
#define MA_SENSOR_TYPE_TRAY_STR                                     "ma.sensor.tray"

/* system event sub type(s) */
#define MA_SYSTEM_SENSOR_SHUTDOWN_PUB_TOPIC_STR                     "ma.sensor.system.shutdown"
#define MA_SYSTEM_SENSOR_STOP_PUB_TOPIC_STR                         "ma.sensor.system.stop"
#define MA_SYSTEM_SENSOR_STARTUP_PUB_TOPIC_STR                      "ma.sensor.system.agent.startup"
#define MA_SYSTEM_SENSOR_SYSTEM_STARTUP_PUB_TOPIC_STR               "ma.sensor.system.startup"
#define MA_SYSTEM_SENSOR_TIME_CHANGE_EVENT_PUB_TOPIC_STR            "ma.sensor.system.time.change"

/* user event sub type(s) */
#define MA_USER_SENSOR_CONSOLE_DISCONNECT_PUB_TOPIC_STR             "ma.sensor.user.console.disconnect"
#define MA_USER_SENSOR_REMOTE_DISCONNECT_PUB_TOPIC_STR              "ma.sensor.user.remote.disconnect"
#define MA_USER_SENSOR_CONSOLE_CONNECT_PUB_TOPIC_STR                "ma.sensor.user.console.connect"
#define MA_USER_SENSOR_REMOTE_CONNECT_PUB_TOPIC_STR                 "ma.sensor.user.remote.connect"
/* power event sub type(s) */
#define MA_POWER_SENSOR_STATUS_CHANGE_PUB_TOPIC_STR                 "ma.sensor.power.status.change"
#define MA_POWER_SENSOR_BATTERY_LOW_PUB_TOPIC_STR                   "ma.sensor.power.battery.low"
#define MA_POWER_SENSOR_SETTING_CHANGE_PUB_TOPIC_STR                "ma.sensor.power.setting.change"
/* network event sub type(s) */
#define MA_NETWORK_SENSOR_CONNECTIVITY_STATUS_CHANGE_PUB_TOPIC_STR  "ma.sensor.network.connectivity.status"
/* idle event sub type(s) */
#define MA_IDLE_SENSOR_IDLE_TIME_CHANGE_PUB_TOPIC_STR               "ma.sensor.idle.time.change"


/* MA Sensor Service Published topics */
#define MA_SENSOR_USER_CHANGED_PUB_TOPIC_STR        	            "ma.sensor.user_changed"

/* MA Sensor clients subscribed topics */
#define MA_SENSOR_USER_CHANGED_SUB_TOPIC_STR        	            "ma.sensor.user_changed" 

#define MA_SENSOR_NETWORK_CHANGED_SUB_TOPIC_STR        	            "ma.sensor.network_changed" 

#define MA_SENSOR_PROP_KEY_DOMAIN_ID_STR                           "prop.key.domain_id"               /* Property Key */
/* property value is the domain name */

#define MA_SENSOR_PROP_KEY_USER_ID_STR                             "prop.key.user_id"                 /* Property Key */  
/* property value is the user name */

#define MA_SENSOR_PROP_KEY_SESSION_ID_STR                          "prop.key.session_id"               /* Property Key */
/* property value is the session id string */

#define MA_SENSOR_PROP_KEY_USER_LOGON_STATE_STR                    "prop.key.user_logon_state"       /* Property key */
#define MA_SENSOR_PROP_VALUE_NOT_LOGGED_ON_STR                     "prop.value.not_logged_on"        /* Property values - These values should be matched with states in policy std defs file */
#define MA_SENSOR_PROP_VALUE_LOG_ON_STR                            "prop.value.log_on" 
#define MA_SENSOR_PROP_VALUE_LOGGED_ON_STR                         "prop.value.logged_on"
#define MA_SENSOR_PROP_VALUE_LOG_OFF_STR                           "prop.value.log_off"

/* Sensor service constants */
#define MA_MAX_SYSTEM_STARTUP_TIME                                  5 /* wait for about 5 minutes to make sure system startup completely */

#endif /* MA_SENSOR_DEFS_H_INCLUDED*/

