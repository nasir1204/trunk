#ifndef MA_UPDATER_DEFS_H_INCLUDED
#define MA_UPDATER_DEFS_H_INCLUDED
#include "ma/updater/ma_updater_client_defs.h"

/* Services, names , constants , datastore configurations */
#define MA_UPDATER_SERVICE_NAME_STR                                    "ma.service.updater"
#define MA_UPDATER_CLIENT_NAME_STR                                     "ma.client.updater"
#define MA_UPDATER_CLIENT_HANDLER_STR                                  "ma.client.updater.handler"
#define MA_UPDATER_SERVICE_UI_PROVIDER_STR                             "ma.service.updater.uiprovider"
#define MA_UPDATER_CLIENT_UI_PROVIDER_STR                              "ma.client.updater.uiprovider"
#define MA_UPDATER_SERVICE_HOSTNAME_STR                                "localhost"

/*  Updater service ePO/server policies */
#define MA_UPDATER_SERVICE_SECTION_NAME_STR                            "UpdaterService"
#define MA_UPDATER_KEY_IS_INSTALLED_INT                                "IsInstalled"
#define MA_UPDATER_KEY_IS_ENABLED_INT                                  "IsEnabled"
#define MA_UPDATER_KEY_ENABLE_AGENT_UI_INT                             "EnableAgentUI"
#define MA_UPDATER_KEY_ENABLE_REBOOT_UI_INT                            "EnableRebootUI"
#define MA_UPDATER_KEY_REBOOT_TIMEOUT_INT                              "RebootTimeout"
#define MA_UPDATER_KEY_ENABLE_DAT_DOWNGRADE_INT                        "EnableDatDowngrade"
#define MA_UPDATER_KEY_ENABLE_RUN_EXE_AFTER_UPDATE_INT                 "EnableExeAfterUpdate"
#define MA_UPDATER_KEY_EXE_NAME_TO_RUN_AFTER_UPDATE_STR                "ExeNameToRunAfterUpdate"
#define MA_UPDATER_KEY_UPFATE_LOG_FILE_NAME_STR						   "UpdateLogFileName"
#define MA_UPDATER_KEY_CATALOG_VERSION_STR                             "CatalogVersion"

#define MA_UPDATER_BRANCH_SELECTION_NAME_STR                           "BranchSelection"
#define MA_UPDATER_KEY_BRANCH_TYPE_STR                                 "BranchType"
#define MA_UPDATER_KEY_UPDATE_SOFTWARE_ID_STR                          "SoftwareID"
#define MA_UPDATER_KEY_NUMBER_OF_ITEMS_STR                             "NumberOfItems"


/*  Updater service datastore settings*/
#define MA_UPDATER_PATH_NAME_STR                                        "AGENT\\SERVICES\\UPDATER"
/*  Updater service properties */
#define MA_UPDATER_PROPERTIES_PATH_NAME_STR                             "AGENT\\SERVICES\\UPDATER\\PROPERTIES"

/*  Updater service local Policies */
#define MA_UPDATER_POLICIES_PATH_NAME_STR                               "AGENT\\SERVICES\\UPDATER\\POLICIES"

/*  Updater service Data */
#define MA_UPDATER_DATA_PATH_NAME_STR                                   "AGENT\\SERVICES\\UPDATER\\DATA"


/* Updater service messages */
#define MA_UPDATER_MSG_PROP_KEY_REQUEST_TYPE_STR                        "prop.key.request_type"
#define MA_UPDATER_MSG_PROP_KEY_SOFTWARE_ID_STR                         "prop.key.software_id"
#define MA_UPDATER_MSG_PROP_KEY_SERVER_NAME_STR                         "prop.key.server_name"

/* request message types from mileaccess update engine */
#define MA_UPDATER_MSG_PROP_VAL_REQUEST_GET_INFO_STR                    "prop.val.rqst.get_info"
#define MA_UPDATER_MSG_PROP_VAL_REQUEST_SET_INFO_STR                    "prop.val.rqst.set_info"
#define MA_UPDATER_MSG_PROP_VAL_REQUEST_PROGRESS_STR                     "prop.val.rqst.progress"

/* response message types to mileaccess update engine */
#define MA_UPDATER_MSG_PROP_VAL_REPLY_GET_INFO_STR                      "prop.val.rply.get_info"
#define MA_UPDATER_MSG_PROP_VAL_REPLY_SET_INFO_STR                      "prop.val.rply.set_info"
#define MA_UPDATER_MSG_PROP_VAL_REPLY_PROGRESS_STR                      "prop.val.rply.progress"

/*request message types from updater ui provider to updater service */
#define MA_UPDATER_UIPROVIDER_MSG_PROP_VAL_REQUEST_REGISTER_STR        "prop.val.rqst.register_uiprovider"
#define MA_UPDATER_UIPROVIDER_MSG_PROP_VAL_REQUEST_UNREGISTER_STR      "prop.val.rqst.unregister_uiprovider"
#define MA_UPDATER_UIPROVIDER_MSG_PROP_VAL_REQUEST_SHOWUI_STR          "prop.val.rqst.showui"

/*response message types from updater service to updater ui provider */
#define MA_UPDATER_UIPROVIDER_MSG_PROP_VAL_REPLY_REGISTER_STR          "prop.val.rply.register_uiprovider"
#define MA_UPDATER_UIPROVIDER_MSG_PROP_VAL_REPLY_UNREGISTER_STR        "prop.val.rply.unregister_uiprovider"
#define MA_UPDATER_UIPROVIDER_MSG_PROP_VAL_REPLY_SHOWUI_STR            "prop.val.rply.showui"



/*  Updater service constants */

/* Back compat task types for updating */
#define MA_UPDATER_TASK_TYPE_COMPAT_UPDATE_STR                      "Update"
#define MA_UPDATER_TASK_TYPE_COMPAT_DEPLOYMENT_STR                  "Deployment"


#define MA_SECTION_GENERAL_STR		                                    "General"
#define MA_SECTION_UPDATE_OPTIONS_STR				                    "UpdateOptions"
#define MA_SECTION_ROLLBACK_OPTIONS_STR				                    "RollbackOptions"
#define MA_SECTION_BRANCH_SELECTION_STR				                    "BranchSelection"
#define MA_SECTION_AGENT_STR						                    "Agent"

/*Agent policy Keys.*/
#define MA_KEY_SHOWREBOOT_UI_STR					                    "ShowRebootUI"
#define MA_KEY_SHOWREBOOT_TIMOUT_STR				                    "RebootTimeOut"

/*
updater policy keys
*/
#define MA_KEY_ALLOW_DATA_DOWNGRADE_STR				                    "bAllowDATDowngrade"
#define MA_KEY_RUN_EXE_IF_SUCCESS_STR				                    "bRunIfUpdateSuccess"
#define MA_KEY_LOG_FILE_NAME_STR					                    "szLogFileName"
#define MA_KEY_RUN_EXE_AFTER_UPDATE_STR				                    "szRunAfterUpdateEXE"
#define MA_REPOSITORY_LICENSE_SECTION_NAME_STR                          "License"
#define MA_REPOSITORY_KEY_LICENSE_STR				                    "LicenseKey"
/*
branch selection keys
*/
#define MA_KEY_BS_NO_OF_ITEMS_STR	                                    "NumberOfItems"
#define MA_KEY_BS_BRANCH_TYPE_STR	                                    "BranchType_%d"
#define MA_KEY_BS_SOFTWARE_ID_STR					                    "SoftwareID_%d"

/*
 Updater Task update options 
*/
#define MA_KEY_SHOW_UI_STR                                              "bShowUI"
#define MA_KEY_ALLOW_POSTPONE_STR                                       "bAllowPostpone"
#define MA_KEY_MAX_POSTPONES_STR                                        "nMaxPostpones"
#define MA_KEY_ORG_MAX_POSTPONES_STR                                    "nOrgMaxPostpones"
#define MA_KEY_POSTONE_TIMEOUT_STR                                      "nPostponeTimeout"
#define MA_KEY_POSTPONE_TEXT_STR                                        "szPostponeText"
#define MA_KEY_DEP_POSTPONE_DONE_NUM_STR								"NumDepPostponesDone"
#define MA_KEY_UPD_POSTPONE_DONE_NUM_STR								"NumPostponesDone"
#define MA_SECTION_SELECTED_UPDATES_STR                                 "SelectedUpdates"
#define MA_SECTION_SELECTIVE_UPDATE_OPTIONS_STR                         "SelectiveUpdateOptions"

#define MA_KEY_UPDATE_ALL_STR                                           "UpdateAll"
#define MA_KEY_NUM_OF_UPDATES_STR                                       "NumOfUpdates"
#define MA_KEY_UPDATE_STR                                               "Update"
#define MA_KEY_SELECTIVE_UPDATE_SOFTWARE_LIST_STR                       "SelectiveClientUpdateList"

#define MA_KEY_LOCALE_INT                                                "Locale"

/*
Rollback task settings 
*/
#define MA_KEY_ENABLE_ROLLBACK_UPDATE_STR                                "bRollback"
#define MA_KEY_ROLLBACK_UPDATE_PRODUCT_ID_STR                            "ProductID"


/*
Deployment task related strings
*/
#define MA_SECTION_SETTINGS_STR                                         "Settings"
#define MA_SECTION_INSTALL_STR                                          "Install"
#define MA_SECTION_UNINSTALL_STR                                        "Uninstall"
#define MA_KEY_RUN_AT_ENFORCEMENT_ENABLED_STR                           "RunAtEnforcementEnabled"
#define MA_KEY_NUM_OF_INSTALLS_STR                                      "NumInstalls"
#define MA_KEY_NUM_OF_UNINSTALLS_STR                                    "NumUninstalls"
#define MA_KEY_INSTALL_LANGUAGE_STR                                     "Language"
#define MA_KEY_INSTALL_PATH_STR                                         "InstallPath"
#define MA_KEY_INSTALL_COMMANDLINE_STR                                  "InstallCommandLine"
#define MA_KEY_INSTALL_PLATFORMS_STR                                    "Platforms"
#define MA_KEY_INSTALL_BRANCH_STR                                       "PackagePathType"
#define MA_KEY_BUILD_VERSION_STR										"BuildVersion"

#define MA_SECTION_DEPLOYMENT_OPTIONS_STR                               "DeploymentOptions"
#define MA_KEY_UPDATE_AFTER_DEPLOYMENT_STR                              "UpdateAfterDeployment"

/* updater ui provider constants*/

#define MA_UPDATER_SHOW_UI_SETTINGS_UPDATER_UI_TYPE_INT                 "UpdaterUIType"
#define MA_UPDATER_SHOW_UI_SETTINGS_TITLE_STR                           "Title"
#define MA_UPDATER_SHOW_UI_SETTINGS_MESSAGE_STR                         "Message"
#define MA_UPDATER_SHOW_UI_SETTINGS_COUNTDOWN_MESSAGE_STR               "CountDownMessage"
#define MA_UPDATER_SHOW_UI_SETTINGS_COUNTDOWN_VALUE_STR                 "CountDownValue"

#define MA_UPDATER_UI_PROGRESS_STEP_INT                                 "ProgressStep"
#define MA_UPDATER_UI_PROGRESS_MAXSTEP_INT                              "MaxProgressStep"
#define MA_UPDATER_UI_PROGRESS_MESSAGE_STR                              "ProgressMessage"

#define MA_UPDATER_END_UI_TITLE_STR                                     "Title"
#define MA_UPDATER_END_UI_MESSAGE_STR                                   "EndMessage"
    
/* updater ui provider task setting*/
#define MA_SECTION_UI_PROVIDER_NAME_STR		                            "UIProviderSection"
#define MA_KEY_UI_PROVIDER_NAME_STR					                    "UIProviderName"


/*
Update task session settings 
*/
#define MA_SECTION_UPDATER_SESSION_NAME_STR                             "UpdaterSession"
#define MA_KEY_SESSION_SETTINGS_VARIANT                                 "SessionSettings"

#define REBOOT_PENDING_REGKEY_PATH										"Network Associates\\ePolicy Orchestrator\\Application Plugins\\CMNUPD__3000"
#define REBOOT_PENDING_REGKEY											"RebootPending"
#define REBOOT_MARKED_FILE_PATH											"RebootPath"
#define MA_AGENT_CONFIGURATION_LAST_UPDATE_STR							"LastUpdate"


/*
global update task section & settings. 
*/
#define MA_GLOBAL_UPDATE_TASK_SECTION_STR				                "GlobalUpdate"
#define MA_GLOBAL_UPDATE_CATALOG_VERSION_SETTINGS_STR				        "GlobalUpdateCatalogVersion"
#define MA_UPDATER_KEY_LAST_GLOBAL_UPDATE_CATALOG_VERSION_STR                           "LastGlobalUpdateCatalogVersion"

#define MA_GLOBAL_UPDATING_TASK_NAME_STR						"ma.task.global.update.EPOAGENT3000"
#define MA_GLOBAL_UPDATING_TASK_ID_STR							"ma.task.id.global.update.EPOAGENT3000"

#endif /* MA_UPDATER_DEFS_H_INCLUDED*/

