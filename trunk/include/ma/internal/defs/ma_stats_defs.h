#ifndef MA_STATS_DEFS_H_INCLUDED
#define MA_STATS_DEFS_H_INCLUDED

/* Services, names , constants , datastore configurations */
#define MA_STATS_SERVICE_NAME_STR                                          "ma.service.stats"
#define MA_STATS_CLIENT_NAME_STR                                           "ma.client.stats"
#define MA_STATS_SERVICE_HOSTNAME_STR                                      "localhost"


/* stats service ePO/server policies */
#define MA_STATS_SERVICE_SECTION_NAME_STR                            "StatsService"
#define MA_STATS_KEY_IS_INSTALLED_INT                                  "IsInstalled"
#define MA_STATS_KEY_IS_ENABLED_INT                                    "IsEnabled"


/*  stats service datastore settings*/
#define MA_STATS_PATH_NAME_STR                                             "AGENT\\SERVICES\\STATS"
/*  stats service properties */
#define MA_STATS_PROPERTIES_PATH_NAME_STR                                  "AGENT\\SERVICES\\STATS\\PROPERTIES"
/*  stats service Policies */
#define MA_STATS_POLICIES_PATH_NAME_STR                                  "AGENT\\SERVICES\\STATS\\POLICIES"
/*  stats service Data */
#define MA_STATS_DATA_PATH_NAME_STR                                      "AGENT\\SERVICES\\STATS\\DATA"


/* STAT EVENTS */
#define	MA_STAT_P2P							"P2P"
#define	MA_STAT_SAHU						"SAHU"
#define	MA_STAT_RELAY_SERVER				"RELAY"

/*  STAT ATTRIBUTES */
#define MA_STAT_ATTRIBUTE_FILES_SERVED						"TotalFilesServed"
#define MA_STAT_ATTRIBUTE_HITS								"TotalHits"
#define MA_STAT_ATTRIBUTE_BANDWITH_SAVED					"Bandwidthsaved"
#define MA_STAT_ATTRIBUTE_FAILED_CONNECTIONS				"FailedConnections"
#define MA_STAT_ATTRIBUTE_SUCCEEDED_CONNECTIONS				"SucceededConnections"
#define MA_STAT_ATTRIBUTE_SURGED_CONNECTIONS				"SurgedConnections"

/* stats service messages */
#define MA_STATS_MSG_KEY_TYPE_STR                               "prop.key.msg_type"

/* request message types */
#define MA_STATS_MSG_PROP_VAL_RQST_COLLECT_AND_SEND_STR           "prop.val.rqst.stats.collect_and_send"

/* response message types */
#define MA_STATS_MSG_PROP_VAL_RPLY_COLLECT_AND_SEND_STR           "prop.val.rply.stats.collect_and_send"


/* stats xml nodes */
#define MA_STATS_XML_NODE_ROOT					"StatisticsReport"

#define MA_STATS_XML_NODE_MACHINE_INFO			"MachineInfo"
#define MA_STATS_XML_TAG_MACHINE_NAME			"MachineName"
#define MA_STATS_XML_TAG_AGENT_GUID				"AgentGUID"
#define MA_STATS_XML_TAG_IP_ADDRESS				"IPAddress"
#define MA_STATS_XML_TAG_MAC_ADDRESS			"RawMACAddress"
#define MA_STATS_XML_TAG_OS_NAME				"OSName"
#define MA_STATS_XML_TAG_USER_NAME				"UserName"
#define MA_STATS_XML_TAG_TIMEZONE_BIAS			"TimeZoneBias"
#define MA_STATS_XML_TAG_GMT_TIME				"GMTTime"
#define MA_STATS_XML_TAG_PRODUCT_ID				"ProductID"

#define MA_STATS_XML_NODE_RELAY_STATS			"RelayServerStats"
#define MA_STATS_XML_TAG_SUCCEEDED_CONNECTIONS	"SucceededConnnections"
#define MA_STATS_XML_TAG_FAILED_CONNECRIONS		"FailedConnections"
#define MA_STATS_XML_TAG_SURGED_CONNECTIONS		"NumberOfTimesMaxed"

#define MA_STATS_XML_NODE_SAHU_STATS			"SAHUStats"
#define MA_STATS_XML_TAG_SAHU_FILES_SERVED		"SAHUFilesServed"
#define MA_STATS_XML_TAG_SAHU_HITS				"SAHUTotalHits"
#define MA_STATS_XML_TAG_SAHU_BANDWIDTH_SAVED	"SAHUBandwidthsaved"

#define MA_STATS_XML_NODE_P2P_STATS				"P2PStats"
#define MA_STATS_XML_TAG_P2P_FILES_SERVED		"P2PFilesServed"
#define MA_STATS_XML_TAG_P2P_HITS				"P2PTotalHits"
#define MA_STATS_XML_TAG_P2P_BANDWIDTH_SAVED	"P2PBandwidthsaved"

#endif /* MA_STATS_DEFS_H_INCLUDED*/

