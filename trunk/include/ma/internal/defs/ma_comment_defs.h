#ifndef MA_COMMENT_SERVICE_DEFS_H_INCLUDED
#define MA_COMMENT_SERVICE_DEFS_H_INCLUDED

/* Services, names , constants , datastore configurations */
#define MA_COMMENT_SERVICE_NAME_STR                                       "ma.service.comment"
#define MA_COMMENT_CLIENT_NAME_STR                                        "ma.client.comment"
#define MA_COMMENT_SERVICE_HOSTNAME_STR                                   "localhost"
#define MA_COMMENT_BASE_PATH_STR                                          "comment"


/*  comment service server policies */
#define MA_COMMENT_SERVICE_SECTION_NAME_STR                               "CommentService"
#define MA_COMMENT_KEY_IS_INSTALLED_INT                                   "IsInstalled"
#define MA_COMMENT_KEY_IS_ENABLED_INT                                     "IsEnabled"
#define MA_COMMENT_TASK_TYPE                                              "Comment"


/*  comment service datastore settings*/
#define MA_COMMENT_PATH_NAME_STR                                          "AGENT\\SERVICES\\COMMENT"
/*  comment service properties */
#define MA_COMMENT_PROPERTIES_PATH_NAME_STR                               "AGENT\\SERVICES\\COMMENT\\PROPERTIES"

/*  comment service local Policies */
#define MA_COMMENT_POLICIES_PATH_NAME_STR                                 "AGENT\\SERVICES\\COMMENT\\POLICIES"

/*  comment service Data */
#define MA_COMMENT_DATA_PATH_NAME_STR                                     "AGENT\\SERVICES\\COMMENT\\DATA"


/* comment service messages */
#define MA_COMMENT_SERVICE_MSG_TYPE                                       "ma.comment.op.msg.type"
#define MA_COMMENT_SERVICE_MSG_REGISTER_TYPE                              "ma.comment.op.msg.register.type"
#define MA_COMMENT_SERVICE_MSG_UNREGISTER_TYPE                            "ma.comment.op.msg.unregister.type"
#define MA_COMMENT_SERVICE_MSG_UPDATE_TYPE                                "ma.comment.op.msg.update.type"
#define MA_COMMENT_SERVICE_MSG_GET_TYPE                                   "ma.comment.op.msg.get.type"
#define MA_COMMENT_ID                                                     "ma.comment.id"
#define MA_COMMENT_ID_EXIST                                               "ma.comment.id.exist"
#define MA_COMMENT_ID_NOT_EXIST                                           "ma.comment.id.not.exist"
#define MA_COMMENT_STATUS                                                 "ma.comment.status"
#define MA_COMMENT_ID_WRITTEN                                             "ma.comment.id.written"
#define MA_COMMENT_ID_DELETED                                             "ma.comment.id.deleted"

/* comment attributes */
#define MA_COMMENT_INFO_ATTR_ID                                           "ma.comment.info.id"
#define MA_COMMENT_INFO_ATTR_COMMENTS                                     "ma.comment.info.comments"
#define MA_COMMENT_INFO_ATTR_SIZE                                         "ma.comment.info.size"

/* comment internal attributes */
#define MA_COMMENT_INTERNAL_ATTR_DATE                                     "ma.comment.internal.date"
#define MA_COMMENT_INTERNAL_ATTR_ID                                       "ma.comment.internal.id"
#define MA_COMMENT_INTERNAL_ATTR_EMAIL                                    "ma.comment.internal.email"
#define MA_COMMENT_INTERNAL_ATTR_COMMENT                                  "ma.comment.internal.comment"


#endif /* MA_COMMENT_SERVICE_DEFS_H_INCLUDED */
