#ifndef MA_COMPAT_DEFS_H_INCLUDED
#define MA_COMPAT_DEFS_H_INCLUDED

/* Services, names , constants , datastore configurations */
#define MA_COMPAT_SERVICE_NAME_STR                                      "ma.service.compat"
#define MA_COMPAT_CLIENT_NAME_STR                                       "ma.client.compat"


/*  Compat service ePO/server policies */
#define MA_COMPAT_SERVICE_SECTION_NAME_STR                              "CompatService"
#define MA_COMPAT_KEY_IS_INSTALLED_INT                                  "IsInstalled"
#define MA_COMPAT_KEY_IS_ENABLED_INT                                    "IsEnabled"

/*  Compat service datastore settings*/
/*  compat service properties */
#define MA_COMPAT_PROPERTIES_PATH_NAME_STR                              "AGENT\\SERVICES\\COMPAT\\PROPERTIES"
/*  copat service local Policies */
#define MA_COMPAT_POLICIES_PATH_NAME_STR                                "AGENT\\SERVICES\\COMPAT\\POLICIES"
/*  Compat service Data */
#define MA_COMPAT_DATA_PATH_NAME_STR                                    "AGENT\\SERVICES\\COMPAT\\DATA"


/* Compat service constants */
#define MA_COMPAT_MSG_STOP                                              "ma.compat.service.stop"
#define MA_COMPAT_SERVICE_STOP_TOPIC		                            "ma.compat.service.stop"

/* Task section names */
#define MA_COMPAT_TASK_SECTION_STR                                      "Task"
#define MA_COMPAT_TASK_DOMAIN_NAME_STR                                  "DomainName"
#define MA_COMPAT_TASK_USER_NAME_STR                                    "UserName"
#define MA_COMPAT_TASK_PASSWORD_STR                                     "Password"



#define MA_CONFIGURATION_LOCAL_MACHINE_SOFTWARE					            "Software"

#define SZ_COMPAT_TVD_FRAMEWORK_REG_PATH						           "Software\\Network Associates\\TVD\\Shared Components\\Framework"
#define SZ_COMPAT_TVD_FRAMEWORK_REG_KEY						               "Network Associates\\TVD\\Shared Components\\Framework"
#define SZ_COMPAT_AGENT_REG_KEY								                "Network Associates\\ePolicy Orchestrator\\Agent"
#define SZ_COMPAT_ePO_AGENT_REG_KEY							                "Network Associates\\ePolicy Orchestrator\\Agent"
#define SZ_COMPAT_ePO_AGENT_REG_PATH							            "Software\\Network Associates\\ePolicy Orchestrator\\Agent"
#define SZ_COMPAT_AGENT_GUID_A                                              "AgentGUID"
#define SZ_COMPAT_IPADDRESS_A                                               "IPAddress"
#define SZ_COMPAT_AGENT_MODE_A                                              "AgentMode"
#define SZ_COMPAT_SERVER_KEY_HASH_A                                         "binsphash"
#define SZ_COMPAT_AGENT_ENABLE_EVENT_TRIGGER                                "AgPlcyEnableEventTrigger"
#define SZ_COMPAT_AGENT_ENABLE_EVENT_TRIGGER_THRESHOLD                      "AgPlcyEventTriggerThreshold"
#define SZ_COMPAT_AGENT_EPOAGENT3000						                SZ_COMPAT_AGENT_APPLICATION_PLUGINS "\\EPOAGENT3000"
#define SZ_COMPAT_AGENT										                "Agent"
#define SZ_COMPAT_AGENT_UNINSTALL_CMD_REG_KEY				                "SOFTWARE\\Network Associates\\ePolicy Orchestrator\\Application Plugins" 
/* compat end */

#define MA_AGENT_ABOUT_VERSION									            "Version"
#define MA_AGENT_ABOUT_LAST_UPDATE_CHECK						            "LastUpdateCheck"
#define MA_AGENT_ABOUT_EPO_SERVER_LIST							            "ePOServerList"
#define MA_AGENT_ABOUT_POLICY_SUPER_AGENT						            "bEnableSuperAgent"
#define MA_AGENT_ABOUT_POLICY_RELAY_SERVICE						            "bEnableRelayService"
#define MA_AGENT_ABOUT_POLICY_P2P_SERVER						            "bEnableP2PServer"
#define MA_AGENT_ABOUT_LAST_ASCI								            "LastASCI"
#define MA_AGENT_ABOUT_POLICY_ENFORCEMENT_INTERVAL				            "PolicyEnforcementInterval"
#define MA_AGENT_ABOUT_POLICY_CHECK_NETWORK_MESSAGE_INTERVAL	            "CheckNetworkMessageInterval"
#define MA_AGENT_ABOUT_COMPUTER_NAME							            "ComputerName"

#define MA_AGENT_POLICY_USE_LANGUAGE							            "UseLanguage"
#define MA_AGENT_POLICY_USE_LANGUAGE_OVERRIDE					            "bOverrideLanguage"
#define MA_MCTRAY_POLICY_LANGUAGE								            "LANGID"
#define MA_MCTRAY_POLICY_ALLOW_UPDATE							            "McTrayAllowUpdate"
#define MA_AGENT_SERVICE_STATE									            "ServiceState"

#define MA_AGENT_LAST_ASCI_TIME_DWORD                                       "LastASCTime"
#define MA_AGENT_LAST_POLICY_UPDATE_TIME_DWORD                              "LastPolicyUpdateTime"


#define MA_COMPAT_UPDATERUI_LAUNCHER_TOPIC				"ma.compat.launcher.updaterui"
#define MA_COMPAT_UPDATERUI_LAUNCHER_PROP_KEY_SESSION_ID "ma.compat.launcher.updaterui.prop.key.session_id"	

#endif /* MA_COMPAT_DEFS_H_INCLUDED*/

