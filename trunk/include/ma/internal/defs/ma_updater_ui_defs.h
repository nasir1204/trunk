#ifndef MA_UPDATER_UI_DEFS_H_INCLUDED
#define MA_UPDATER_UI_DEFS_H_INCLUDED

#define MA_LOCALMACHINE_REGKEY_ROOT								"SOFTWARE\\"
#define MA_UPDATER_UI_LOCALIZATION_PICKUP_PATH					"Network Associates\\TVD\\Shared Components\\Framework"
#define MA_UPDATER_UI_LOCALIZATION_MCTRAY_PICKUP_PATH			"McAfee\\Win32_GUI_Support_DLL"
#define MA_UPDATER_UI_USE_LANGUAGE								"UseLanguage"
#define MA_UPDATER_UI_MCTRAY_LANG								"LANGID"

/*Updater UI policies*/
#define MA_UPDATER_UI_PO_TYPE_GENERAL							"General"
#define MA_UPDATER_UI_PO_SECTION_GENERAL						"General"
#define MA_UPDATER_UI_PO_SETTING_SHOW_AGENT_UI					"ShowAgentUI"
#define MA_UPDATER_UI_PO_SETTING_ALLOW_UPDATE_SECURITY			"bAllowUpdateSecurity"

#define MA_UPDATER_UI_PO_TYPE_TROUBLESHOOTING					"Troubleshooting"
#define MA_UPDATER_UI_PO_SECTION_LANGUAGE_OPTIONS				"LanguageOptions"
#define MA_UPDATER_UI_PO_SETTING_AGENT_LANGUAGE					"AgentLanguage"
#define MA_UPDATER_UI_PO_SETTING_ENABLE_AGENT_LANG_SELECTION	"bEnableAgentLangSelection"
#define MA_UPDATER_UI_LANGUAGE_OVERRIDE							"bLangOverride"
#endif /* MA_UPDATER_DEFS_H_INCLUDED*/

