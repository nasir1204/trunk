#ifndef MA_UDP_DEFS_H_INCLUDED
#define MA_UDP_DEFS_H_INCLUDED

/* Services, names , constants , datastore configurations */

/* */
#define MA_UDP_SERVICE_NAME_STR                                     "ma.service.udp"
#define MA_UDP_CLIENT_NAME_STR                                      "ma.client.udp"


/*  UDP service ePO/server policies */
#define MA_UDP_SERVICE_SECTION_NAME_STR                             "UdpService"
#define MA_UDP_KEY_IS_INSTALLED_INT                                 "IsInstalled"
#define MA_UDP_KEY_IS_ENABLED_INT                                   "IsEnabled"
#define MA_UDP_KEY_MULTICAST_ADDRESS_STR                            "UdpMulticastAddress"
#define MA_UDP_KEY_BROADCAST_PORT_INT                               "UdpBroadcastPort"
#define MA_UDP_KEY_ENABLE_BROADCAST_PING_INT						"IsBroadcastPingEnabled"
#define MA_UDP_ENABLE_AUTO_UPDATE_STR                               "EnableAutoUpdate"

#define MA_UDP_DEFAULT_PORT_INT                                      8082
#define MA_UDP_DEFAULT_PORT_STR                                      "8082"
#define MA_UDP_MULTICAST_ADDR_STR							        "ff08::2"
/*  UDP service datastore settings*/
#define MA_UDP_PATH_NAME_STR                                       "AGENT\\SERVICES\\UDP"

/*  UDP service properties */
#define MA_UDP_PROPERTIES_PATH_NAME_STR                            "AGENT\\SERVICES\\UDP\\PROPERTIES"

/*  UDP service local Policies */
#define MA_UDP_POLICIES_PATH_NAME_STR                              "AGENT\\SERVICES\\UDP\\POLICIES"

/*  UDP service Data */
#define MA_UDP_DATA_PATH_NAME_STR                                  "AGENT\\SERVICES\\UDP\\DATA"
/* UDP message constants */
#define MA_MSG_AGENT_GLOBAL_UPDATE_TOPIC_STR                       "ma.msg.global.update.execute"

/* UDP message constants */
#define MA_UDP_MSG_KEY_TYPE_STR										"prop.key.msg_type"
#define MA_UDP_MSG_KEY_GLOBAL_UPDATE_PRODUCT_LIST_STR				"prop.key.global_update.product_list"
#define MA_UDP_MSG_KEY_GLOBAL_UPDATE_CATALOG_VERSION_STR			"prop.key.global_update.catalog_version"
#define MA_UDP_MSG_KEY_GLOBAL_UPDATE_SERVER_NAME_STR				"prop.key.global_update.server_name"
#define MA_UDP_MSG_KEY_GLOBAL_UPDATE_SERVER_TIMESTAMP_STR			"prop.key.global_update.server_timestamp"

#define MA_UDP_MSG_KEY_AGENT_WAKEUP_NEED_FULL_PROPS_STR				"prop.key.agent_wakeup.need_full_props"
#define MA_UDP_MSG_KEY_AGENT_WAKEUP_SERVER_NAME_STR					"prop.key.agent_wakeup.server_name"
#define MA_UDP_MSG_KEY_AGENT_WAKEUP_SERVER_TIMESTAMP_STR	        "prop.key.agent_wakeup.server_timestamp"
#define MA_UDP_MSG_KEY_AGENT_WAKEUP_RANDOM_INTERVAL_STR	            "prop.key.agent_wakeup.random_interval"

#define MA_UDP_MSG_PROP_VAL_AGENT_WAKEUP_STR					   "prop.val.agent_wakeup"
#define MA_UDP_MSG_PROP_VAL_GLOBAL_UPDATE_STR					   "prop.val.global_update"
#define MA_UDP_MSG_PROP_VAL_P2P_CONTENT_DISCOVERY_STR			   "prop.val.p2p_content_discovery"

/*compat message */
#define MA_COMPAT_RELAY_HEADER_FOOTER_STR							"RELA"
#define MA_COMPAT_SA_WAKEUP_MESSAGE_HEADER_STR						"<Type=\"AgentWakeup\" Version=\"1.0\""

#define MA_UDP_MSGBUS_MSG_HEADER_STR								"<type=\"msgbus\" version=\"1.0\">"

#endif /*   MA_UDP_DEFS_H_INCLUDED */
