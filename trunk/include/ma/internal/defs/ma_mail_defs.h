#ifndef MA_MAIL_SERVICE_DEFS_H_INCLUDED
#define MA_MAIL_SERVICE_DEFS_H_INCLUDED

/* Services, names , constants , datastore configurations */
#define MA_MAIL_SERVICE_NAME_STR                                       "ma.service.mail"
#define MA_MAIL_CLIENT_NAME_STR                                        "ma.client.mail"
#define MA_MAIL_SERVICE_HOSTNAME_STR                                   "localhost"


/*  Mail service server policies */
#define MA_MAIL_SERVICE_SECTION_NAME_STR                               "MailService"
#define MA_MAIL_KEY_IS_INSTALLED_INT                                   "IsInstalled"
#define MA_MAIL_KEY_IS_ENABLED_INT                                     "IsEnabled"


/*  Mail service datastore settings*/
#define MA_MAIL_PATH_NAME_STR                                          "AGENT\\SERVICES\\MAIL"
/*  Mail service properties */
#define MA_MAIL_PROPERTIES_PATH_NAME_STR                               "AGENT\\SERVICES\\MAIL\\PROPERTIES"

/*  Mail service local Policies */
#define MA_MAIL_POLICIES_PATH_NAME_STR                                 "AGENT\\SERVICES\\MAIL\\POLICIES"

/*  Mail service Data */
#define MA_MAIL_DATA_PATH_NAME_STR                                     "AGENT\\SERVICES\\MAIL\\DATA"


/* mail service messages */
#define MA_MAIL_SERVICE_MSG_TYPE                                       "ma.mail.op.msg.type"
#define MA_MAIL_SERVICE_MSG_SEND_TYPE                                  "ma.mail.op.msg.send.type"
#define MA_MAIL_ID_FROM                                                "ma.mail.id.from"
#define MA_MAIL_ID_TO                                                  "ma.mail.id.to"
#define MA_MAIL_ID_CC                                                  "ma.mail.id.cc"
#define MA_MAIL_ID_BCC                                                 "ma.mail.id.bcc"
#define MA_MAIL_SUBJECT                                                "ma.mail.subject"
#define MA_MAIL_ATTACHMENT                                             "ma.mail.attachment"
#define MA_MAIL_ENVELOPE                                               "ma.mail.envelope"
#define MA_MAIL_STATUS                                                 "ma.mail.status"
#define MA_MAIL_SEND_SUCCESS                                           "ma.mail.send.success"
#define MA_MAIL_SEND_FAILED                                            "ma.mail.send.failed"


#endif /* MA_MAIL_SERVICE_DEFS_H_INCLUDED */

