#ifndef MA_NETWORK_DEFS_H_INCLUDED
#define MA_NETWORK_DEFS_H_INCLUDED

/* Services, names , constants , datastore configurations */
#define MA_NETWORK_SERVICE_NAME_STR                                          "ma.service.network"
#define MA_NETWORK_CLIENT_NAME_STR                                           "ma.client.network"
#define MA_NETWORK_SERVICE_HOSTNAME_STR                                      "localhost"


/* Network service ePO/server policies */



/*  Network service datastore settings*/
#define MA_NETWORK_PATH_NAME_STR                                             "AGENT\\SERVICES\\NETWORK"
/*  Network service properties */
#define MA_NETWORK_PROPERTIES_PATH_NAME_STR                                  "AGENT\\SERVICES\\NETWORK\\PROPERTIES"
/*  Network service Policies */
#define MA_NETWORK_POLICIES_PATH_NAME_STR                                  "AGENT\\SERVICES\\NETWORK\\POLICIES"
/*  Network service Data */
#define MA_NETWORK_DATA_PATH_NAME_STR                                      "AGENT\\SERVICES\\NETWORK\\DATA"


/*  Network service constants */
#define MA_NETWORK_SECTION_NAME_STR											"Network"
#define MA_CHECK_NETWORK_MESSAGE_INTERVAL_KEY_STR							"CheckNetworkMessageInterval"



#endif /* MA_NETWORK_DEFS_H_INCLUDED*/

