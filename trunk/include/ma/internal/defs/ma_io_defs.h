#ifndef MA_IO_DEFS_H_INCLUDED
#define MA_IO_DEFS_H_INCLUDED

/* Services, names , constants , datastore configurations */
#define MA_IO_SERVICE_NAME_STR                                   "ma.service.io"
#define MA_IO_CLIENT_NAME_STR                                    "ma.client.io"

/*  I/O service ePO/server policies */
#define MA_IO_SERVICE_SECTION_NAME_STR                            "IOService"

/*  IO service datastore settings*/
#define MA_IO_PATH_NAME_STR                                       "AGENT\\SERVICES\\IO"
/*  IO service properties */
#define MA_IO_PROPERTIES_PATH_NAME_STR                            "AGENT\\SERVICES\\IO\\PROPERTIES"
/*  IO service local Policies */
#define MA_IO_POLICIES_PATH_NAME_STR                              "AGENT\\SERVICES\\IO\\POLICIES"
/*  IO service Data */
#define MA_IO_DATA_PATH_NAME_STR                                  "AGENT\\SERVICES\\IO\\DATA"

/* io service messages */
#define MA_IO_SERVICE_MSG_TYPE                                  "ma.io.msg.type"

/* service which suppose to take over from io service to process it */
#define MA_SERVICE_NAME_MSG										"ma.service.name.msg"
/* reply status message type(s) */
#define MA_IO_REPLY_STATUS                                      "MA_IO_REPLY_STATUS"
#define MA_IO_REPLY_STATUS_SUCCESS                              "MA_IO_REPLY_STATUS_SUCCESS"
#define MA_IO_REPLY_STATUS_FAILED                               "MA_IO_REPLY_STATUS_FAILED"

#define MA_IO_MSG_TYPE_LOGLEVEL_SETTING_CHANGED_STR             "ma.io.loglevel.setting.changed"
#define MA_IO_MSG_TYPE_LANG_SETTING_CHANGED_STR                 "ma.io.lang.setting.changed"
#define MA_IO_MSG_TYPE_LANG_SETTING_RESET_STR                   "ma.io.lang.setting.reset"
#define MA_IO_MSG_TYPE_STATUS_CHECK			                    "ma.io.service.status"
#define MA_IO_MSG_TYPE_UPDATE_REG_INFO							"ma.io.service.update.registry"
#define MA_IO_CONFIGURED_LANGUAGE_STR                           "ma.io.configured.language"
#define MA_IO_MSG_TYPE_RECONFIGURE_SERVICES_STR                 "ma.io.service.reconfigure.services"
#define MA_IO_CONFIGURED_LOGLEVEL_STR                           "ma.io.configured.loglevel"
/* Firewall rules updte message */
#define MA_IO_MSG_TYPE_FIREWALL_RULE_CHANGED_STR                 "ma.io.service.firewall.rule.changed"


#define MA_IO_MSG_TYPE_GET_RELAY_INFO		                    "ma.io.msg.relay.get"
#define MA_IO_MSG_TYPE_REFRESH_RELAY_INFO		                "ma.io.msg.relay.refresh"

#define MA_CONFIG_MSG_TYPE_CUSTOM_PROPS_STR						"ma.config.custom.props.set"
#define MA_CONFIG_CUSTOM_PROP1_STR								"ma.config.custom.prop1.set"
#define MA_CONFIG_CUSTOM_PROP2_STR								"ma.config.custom.prop2.set"
#define MA_CONFIG_CUSTOM_PROP3_STR								"ma.config.custom.prop3.set"
#define MA_CONFIG_CUSTOM_PROP4_STR								"ma.config.custom.prop4.set"

#define MA_CONFIG_MSG_TYPE_LICENSE_KEY_STR						"ma.config.license.key.set"
#define MA_PROP_VALUE_LICENSE_KEY_STR							"prop.value.license.key"
/*IO Service publish message*/
#define MA_AGENT_SERVICE_STATE_PUB_SUB_TOPIC					"ma.msg.topic.ma_service_state"

/* I/O service messages */
#define MA_PROP_KEY_REQUEST_TYPE_STR                            "prop.key.io_request"

#define MA_PROP_VALUE_KEY_UPDATE	                            "prop.value.key_update"
#define MA_PROP_VALUE_CMD_COLLECT_PROPS							"prop.value.cmd_collect_send_props"
#define MA_PROP_VALUE_CMD_ENFORCE_POLICY						"prop.value.cmd_enforce_policy"
#define MA_PROP_VALUE_CMD_REFRESH_POLICY						"prop.value.cmd_refresh_policy"
#define MA_PROP_VALUE_CMD_UPLOAD_EVENTS							"prop.value.cmd_upload_events"
#define MA_PROP_VALUE_CMD_AGENT_STATE							"prop.value.cmd_get_state"
#define MA_PROP_VALUE_AGENT_INFO_COLLECT						"prop.value.get_agent_info"
#define MA_PROP_VALUE_GET_PRODUCT_LICENSE_STATE_STR             "prop.value.get_product_license_state"
#define MA_PROP_VALUE_ACL_REQUEST_STR                           "prop.value.acl_request"
#define MA_PROP_VALUE_FILE_MOVE_REQUEST_STR                     "prop.value.file_move_request"

#define MA_PROP_KEY_SPIPE_SOURCE_IP_STR							"prop.key.spipe_source_ip"

/*IO service send message for with key prop.key.ma_service_state*/
#define MA_PROP_KEY_SERVICE_STATE								"prop.key.ma_service_state"

#define MA_PROP_VALUE_SERVICE_STATE_STARTING					"prop.value.starting"
#define MA_PROP_VALUE_SERVICE_STATE_STARTED						"prop.value.started"
#define MA_PROP_VALUE_SERVICE_STATE_STOPPING					"prop.value.stopping"
#define MA_PROP_VALUE_SERVICE_STATE_STOPPED						"prop.value.stopped"

/* This keys are used to provide the license state information */
#define MA_PROP_KEY_PRODUCT_ID_STR                              "prop.key.product_id"


/*  IO service constants */
#define MA_PP_LICENSE_POLICY_SECTION_NAME                       "PPLicense"

#define MA_INFO_REQUEST_KEY_STR                                 "prop.key.agent_info_key.request"

/* Info needed for acl request */
#define MA_ACL_SID_INT                                          "prop.key.acl_sid"
#define MA_ACL_SID_NAME_STR                                     "prop.key.acl_sid_name"
#define MA_ACL_SID_ALLOW_BOOL                                   "prop.key.acl_sid_allow"
#define MA_ACL_OBJECT_NAME_STR                                  "prop.key.acl_object_name"
#define MA_ACL_OBJECT_PERMISSIONS_INT                           "prop.key.acl_object_permissions"
#define MA_ACL_OBJECT_INHERITANCE_INT                           "prop.key.acl_object_inheritance"
#define MA_ACL_OBJECT_INHERITANCE_BOOL                          "prop.key.acl_object_allow_inheritance"
#define MA_ACL_OBJECT_IS_DIRCTORY_BOOL                          "prop.key.acl_object_is_directory"

#define MA_DESTINATION_FOLDER_NAME_STR                           "prop.key.destination_folder_name"

/* Info needed for file move request */
#define MA_FILE_MOVE_SOURCE_STR                                 "prop.key.file_move_source"
#define MA_FILE_MOVE_DESTINATION_STR                            "prop.key.file_move_destination"
#define MA_FILE_MOVE_BASE_DIR_STR                               "prop.key.file_move_base_dir"

#endif /* MA_IO_DEFS_H_INCLUDED*/

