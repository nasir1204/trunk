#ifndef MA_P2P_DEFS_H_INCLUDED
#define MA_P2P_DEFS_H_INCLUDED

/* Services, names , constants , datastore configurations */

/* */
#define MA_P2P_SERVICE_NAME_STR                                     "ma.service.p2p"
#define MA_P2P_CLIENT_NAME_STR                                      "ma.client.p2p"


/*  P2P service ePO/server policies */
#define MA_P2P_SERVICE_SECTION_NAME_STR                              "P2pService"
#define MA_P2P_KEY_IS_INSTALLED_INT                                  "IsInstalled"
#define MA_P2P_KEY_IS_ENABLED_INT                                    "IsEnabled"
#define MA_P2P_KEY_PURGE_METHOD_INT									 "PurgeMethod"
#define MA_P2P_KEY_HIT_COUNT_INT									 "HitCount"
#define MA_P2P_KEY_DISK_QUOTA_INT									 "DiskQuota"
#define MA_P2P_KEY_LONGEVITY_INT									 "ContentLongevity"
#define MA_P2P_KEY_ENABLE_CLIENT_INT								 "EnableClient"
#define MA_P2P_KEY_ENABLE_SERVING_INT								 "EnableServing"
#define	MA_P2P_KEY_CONCURRENT_CONNECTIONS_LIMIT_INT					 "ConcurrentConnectionsLimit"
#ifdef MA_WINDOWS
#define MA_P2P_KEY_REPO_PATH_STR									 "P2pRepoPath"
#else
#define MA_P2P_KEY_REPO_PATH_STR									 "P2pRepoPathUnix"
#endif
/*  P2P service datastore settings*/
#define MA_P2P_PATH_NAME_STR                                       "AGENT\\SERVICES\\P2P"

/*  P2P service properties */
#define MA_P2P_PROPERTIES_PATH_NAME_STR                            "AGENT\\SERVICES\\P2P\\PROPERTIES"

/*  P2P service local Policies */
#define MA_P2P_POLICIES_PATH_NAME_STR                              "AGENT\\SERVICES\\P2P\\POLICIES"

/*  P2P service Data */
#define MA_P2P_DATA_PATH_NAME_STR                                  "AGENT\\SERVICES\\P2P\\DATA"


/* P2P service messages */
#define MA_P2P_MSG_KEY_TYPE_STR                                    "prop.key.msg_type"
#define MA_P2P_MSG_KEY_CONTENT_URL_STR							   "prop.key.content_url"
#define MA_P2P_MSG_KEY_CONTENT_HASH_STR							   "prop.key.content_hash"
#define MA_P2P_MSG_KEY_CONTENT_URL_PORT_STR						   "prop.key.content_url.port"
#define MA_P2P_MSG_KEY_CONTENT_DIR_STR							   "prop.key.content_dir"
#define MA_P2P_MSG_KEY_CONTENT_EXISTS_STR						   "prop.key.check_content_exists"
#define MA_P2P_MSG_KEY_CHECK_SERVICE_EXISTS_STR					   "prop.key.check_service_exists"

/* request message types */
#define MA_P2P_MSG_PROP_VAL_RQST_ADD_CONTENT_STR                   "prop.val.rqst.p2p.add_content"
#define MA_P2P_MSG_PROP_VAL_RQST_REMOVE_CONTENT_STR                "prop.val.rqst.p2p.remove_content"
#define	MA_P2P_MSG_PROP_VAL_RQST_SERVICE_DISCOVERY_STR			   "prop.val.rqst.p2p.service_discovery"
#define	MA_P2P_MSG_PROP_VAL_RQST_CONTENT_DISCOVERY_STR			   "prop.val.rqst.p2p.content_discovery"
#define	MA_P2P_MSG_PROP_VAL_RQST_GET_CONTENT_DIR_STR			   "prop.val.rqst.p2p.get_content_dir"		
#define	MA_P2P_MSG_PROP_VAL_RQST_CHECK_CONTENT_EXISTS_STR		   "prop.val.rqst.p2p.check_content_exists"
#define	MA_P2P_MSG_PROP_VAL_RQST_RUN_PURGE_TASK_STR				   "prop.val.rqst.p2p.run_purge_task"

/* response message types */
#define MA_P2P_MSG_PROP_VAL_RPLY_ADD_CONTENT_STR                   "prop.val.rply.p2p.add_content"
#define MA_P2P_MSG_PROP_VAL_RPLY_REMOVE_CONTENT_STR                "prop.val.rply.p2p.remove_content"
#define	MA_P2P_MSG_PROP_VAL_RPLY_SERVICE_DISCOVERY_STR			   "prop.val.rply.p2p.service_discovery"
#define	MA_P2P_MSG_PROP_VAL_RPLY_CONTENT_DISCOVERY_STR			   "prop.val.rply.p2p.content_discovery"
#define	MA_P2P_MSG_PROP_VAL_RPLY_GET_CONTENT_DIR_STR			   "prop.val.rply.p2p.get_content_dir"
#define	MA_P2P_MSG_PROP_VAL_RPLY_CHECK_CONTENT_EXISTS_STR		   "prop.val.rply.p2p.check_content_exists"
#define	MA_P2P_MSG_PROP_VAL_RPLY_RUN_PURGE_TASK_STR				   "prop.val.rply.p2p.run_purge_task"

/*  p2p service constants */
#define MA_P2P_CONTENT_BASE_DIR_STR									"McAfeeP2P"
#define MA_P2P_CONTENT_URL_FORMAT_STR								"http://%s:%d/p2p/%s"
#define MA_P2P_CONTENT_IPV6_URL_FORMAT_STR							"http://[%s]:%d/p2p/%s"
#define MA_P2P_CONTENT_IPV6_URL_FORMAT_WITH_SCOPE_ID_STR			"http://[%s%c%d]:%d/p2p/%s"

#endif /*   MA_P2P_DEFS_H_INCLUDED */
