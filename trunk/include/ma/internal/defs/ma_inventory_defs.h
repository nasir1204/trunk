#ifndef MA_INVENTORY_SERVICE_DEFS_H_INCLUDED
#define MA_INVENTORY_SERVICE_DEFS_H_INCLUDED

/* Services, names , constants , datastore configurations */
#define MA_INVENTORY_SERVICE_NAME_STR                                       "ma.service.inventory"
#define MA_INVENTORY_CLIENT_NAME_STR                                        "ma.client.inventory"
#define MA_INVENTORY_SERVICE_HOSTNAME_STR                                   "localhost"
#define MA_INVENTORY_BASE_PATH_STR                                           "inventory"


/*  Inventory service ePO/server policies */
#define MA_INVENTORY_SERVICE_SECTION_NAME_STR                               "InventoryService"
#define MA_INVENTORY_KEY_IS_INSTALLED_INT                                   "IsInstalled"
#define MA_INVENTORY_KEY_IS_ENABLED_INT                                     "IsEnabled"
#define MA_INVENTORY_TASK_ID                                                "ma.inventory.task.id"
#define MA_INVENTORY_TASK_TYPE                                              "ma.inventory.task.inventory.type"
#define MA_INVENTORY_TASK_MAX_RUNTIME                                       2 /* minutes */


/*  Inventory service datastore settings*/
#define MA_INVENTORY_PATH_NAME_STR                                          "AGENT\\SERVICES\\INVENTORY"
/*  Inventory service properties */
#define MA_INVENTORY_PROPERTIES_PATH_NAME_STR                               "AGENT\\SERVICES\\INVENTORY\\PROPERTIES"

/*  Inventory service local Policies */
#define MA_INVENTORY_POLICIES_PATH_NAME_STR                                 "AGENT\\SERVICES\\INVENTORY\\POLICIES"

/*  Inventory service Data */
#define MA_INVENTORY_DATA_PATH_NAME_STR                                     "AGENT\\SERVICES\\INVENTORY\\DATA"


/* inventory service messages */
#define MA_INVENTORY_SERVICE_MSG_TYPE                                       "ma.inventory.op.msg.type"
#define MA_INVENTORY_SERVICE_MSG_REGISTER                                   "ma.inventory.op.msg.register"
#define MA_INVENTORY_SERVICE_MSG_DEREGISTER                                 "ma.inventory.op.msg.deregister"
#define MA_INVENTORY_SERVICE_MSG_ORDER_CREATE_TYPE                          "ma.inventory.op.msg.order.create.type"
#define MA_INVENTORY_SERVICE_MSG_ORDER_MODIFY_TYPE                          "ma.inventory.op.msg.order.modify.type"
#define MA_INVENTORY_SERVICE_MSG_ORDER_DELETE_TYPE                          "ma.inventory.op.msg.order.delete.type"
#define MA_INVENTORY_SERVICE_MSG_ORDER_UPDATE_STATUS_TYPE                   "ma.inventory.op.msg.order.udpate.status.type"
#define MA_INVENTORY_SERVICE_MSG_GET_TYPE                                   "ma.inventory.op.msg.get.type"
#define MA_INVENTORY_SERVICE_MSG_INVENTORY_DELETE_TYPE                        "ma.inventory.op.msg.delete.type"
#define MA_INVENTORY_ID                                                     "ma.inventory.id"
#define MA_INVENTORY_CRN                                                    "ma.inventory.crn"
#define MA_INVENTORY_ID_EXIST                                               "ma.inventory.id.exist"
#define MA_INVENTORY_ID_NOT_EXIST                                           "ma.inventory.id.not.exist"
#define MA_INVENTORY_STATUS                                                 "ma.inventory.status"
#define MA_INVENTORY_ID_WRITTEN                                             "ma.inventory.id.written"
#define MA_INVENTORY_ID_DELETED                                             "ma.inventory.id.deleted"

/* inventory attributes */
#define MA_INVENTORY_INFO_ATTR_EMAIL_ID                                     "ma.inventory.info.email.id"
#define MA_INVENTORY_INFO_ATTR_CONTACT                                      "ma.inventory.info.contact"
#define MA_INVENTORY_INFO_ATTR_DATE                                         "ma.inventory.info.date"
#define MA_INVENTORY_INFO_ATTR_PINCODE                                      "ma.inventory.info.pincode"
#define MA_INVENTORY_INFO_ATTR_ORDER_COMPLETE_COUNT                         "ma.inventory.info.order.complete.count"
#define MA_INVENTORY_INFO_ATTR_ORDER_PENDING_COUNT                          "ma.inventory.info.order.pending.count"
#define MA_INVENTORY_INFO_ATTR_ORDER_CANCELLED_COUNT                        "ma.inventory.info.order.cancelled.count"
#define MA_INVENTORY_INFO_ATTR_ORDER_NOT_DELIVERED_COUNT                    "ma.inventory.info.order.not.delivered.count"
#define MA_INVENTORY_INFO_ATTR_START_KILOMETER                              "ma.inventory.info.start.kilometer"
#define MA_INVENTORY_INFO_ATTR_END_KILOMETER                                "ma.inventory.info.end.kilometer"
#define MA_INVENTORY_INFO_ATTR_START_TIME                                   "ma.inventory.info.start.time"
#define MA_INVENTORY_INFO_ATTR_END_TIME                                     "ma.inventory.info.end.time"
#define MA_INVENTORY_INFO_ATTR_WORK_STATUS                                  "ma.inventory.info.work.status"
#define MA_INVENTORY_INFO_ATTR_VEHICLE_TYPE                                 "ma.inventory.info.vehicle.type"

#endif /* MA_INVENTORY_SERVICE_DEFS_H_INCLUDED */
