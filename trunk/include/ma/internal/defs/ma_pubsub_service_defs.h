#ifndef MA_PUBSUB_SERVICE_DEFS_H_INCLUDED
#define MA_PUBSUB_SERVICE_DEFS_H_INCLUDED


/* Broker - pub sub service properties field */
#define MA_MSGBUS_PUBSUB_SERVICE_NAME_STR				"ma.msgbus.pubsub_service.service"
#define MA_MSGBUS_PUBSUB_SERVICE_HOST_NAME_STR	        "localhost"

#define MA_PROP_KEY_PUBSUB_SERVICE_REQUEST_TYPE_STR		"prop.key.pubsub_service_request_type"
#define MA_PROP_KEY_PUBSUB_SERVICE_REPLY_TYPE_STR		"prop.key.pubsub_service_reply_type"

#define MA_PROP_VALUE_REGISTER_SUBSCRIBER_REQUEST_STR		"prop.value.register_subscriber_request"
#define MA_PROP_VALUE_UNREGISTER_SUBSCRIBER_REQUEST_STR     "prop.value.unregister_subscriber_request"
#define MA_PROP_VALUE_DELETE_SUBSCRIBERS_REQUEST_STR		"prop.value.delete_subscribers_request"

#define MA_PROP_KEY_SUBSCRIBER_TOPIC_STR		"prop.key.subscriber_topic"
#define MA_PROP_KEY_PRODUCT_ID_STR				"prop.key.product_id"
#define MA_PROP_KEY_SERVICE_ID_STR				"prop.key.service_id"   /* Pipe Name */
#define MA_PROP_KEY_HOST_NAME_STR				"prop.key.host_name"
#define MA_PROP_KEY_PID_STR						"prop.key.pid"
#define MA_PROP_KEY_SKIP_RESPONSE_STR		    "prop.key.skip_response" /* This is internal for msgbus */

/* The below macros to be moved in common header file */
#define MAX_PIPENAME_LEN            256
#define MAX_HOST_NAME_LEN           256
#define MAX_PID_LEN                 16

#endif /* MA_PUBSUB_SERVICE_DEFS_H_INCLUDED */
