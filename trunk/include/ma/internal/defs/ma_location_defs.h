#ifndef MA_LOCATION_SERVICE_DEFS_H_INCLUDED
#define MA_LOCATION_SERVICE_DEFS_H_INCLUDED

/* Services, names , constants , datastore configurations */
#define MA_LOCATION_SERVICE_NAME_STR                                       "ma.service.location"
#define MA_LOCATION_CLIENT_NAME_STR                                        "ma.client.location"
#define MA_LOCATION_SERVICE_HOSTNAME_STR                                   "localhost"
#define MA_LOCATION_BASE_PATH_STR                                           "location"


/*  Recording service ePO/server policies */
#define MA_LOCATION_SERVICE_SECTION_NAME_STR                               "RecordingService"
#define MA_LOCATION_KEY_IS_INSTALLED_INT                                   "IsInstalled"
#define MA_LOCATION_KEY_IS_ENABLED_INT                                     "IsEnabled"


/*  Recording service datastore settings*/
#define MA_LOCATION_PATH_NAME_STR                                          "AGENT\\SERVICES\\LOCATION"
/*  Recording service properties */
#define MA_LOCATION_PROPERTIES_PATH_NAME_STR                               "AGENT\\SERVICES\\LOCATION\\PROPERTIES"

/*  Recording service local Policies */
#define MA_LOCATION_POLICIES_PATH_NAME_STR                                 "AGENT\\SERVICES\\LOCATION\\POLICIES"

/*  Recording service Data */
#define MA_LOCATION_DATA_PATH_NAME_STR                                     "AGENT\\SERVICES\\LOCATION\\DATA"


/* location service messages */
#define MA_LOCATION_SERVICE_MSG_TYPE                                       "ma.location.op.msg.type"
#define MA_LOCATION_SERVICE_MSG_REGISTER                                   "ma.location.op.msg.register"
#define MA_LOCATION_SERVICE_MSG_DEREGISTER                                 "ma.location.op.msg.deregister"
#define MA_LOCATION_SERVICE_MSG_UPDATE                                     "ma.location.op.msg.update"
#define MA_LOCATION_SERVICE_MSG_ORDER_CREATE_TYPE                          "ma.location.op.msg.order.create.type"
#define MA_LOCATION_SERVICE_MSG_ORDER_MODIFY_TYPE                          "ma.location.op.msg.order.modify.type"
#define MA_LOCATION_SERVICE_MSG_ORDER_DELETE_TYPE                          "ma.location.op.msg.order.delete.type"
#define MA_LOCATION_SERVICE_MSG_ORDER_UPDATE_STATUS_TYPE                   "ma.location.op.msg.order.udpate.status.type"
#define MA_LOCATION_SERVICE_MSG_GET_TYPE                                   "ma.location.op.msg.get.type"
#define MA_LOCATION_SERVICE_MSG_VALIDATE_PINCODE_TYPE                      "ma.location.op.validate.pincode.type"
#define MA_LOCATION_SERVICE_MSG_LOCATION_DELETE_CONTACT               "ma.location.op.msg.delete.contact"
#define MA_LOCATION_ID                                                     "ma.location.id"
#define MA_LOCATION_CRN                                                    "ma.location.crn"
#define MA_LOCATION_ID_EXIST                                               "ma.location.id.exist"
#define MA_LOCATION_ID_NOT_EXIST                                           "ma.location.id.not.exist"
#define MA_LOCATION_STATUS                                                 "status"
#define MA_LOCATION_ID_WRITTEN                                             "ma.location.id.written"
#define MA_LOCATION_ID_DELETED                                             "ma.location.id.deleted"

/* location attributes */
#define MA_LOCATION_INFO_ATTR_EMAIL_ID                                     "ma.location.info.email.id"
#define MA_LOCATION_INFO_ATTR_PINCODE                                      "ma.location.info.pincode"
#define MA_LOCATION_INFO_ATTR_DATE                                         "ma.location.info.date"
#define MA_LOCATION_INFO_ATTR_TYPE                                         "ma.location.info.type"
#define MA_LOCATION_INFO_ATTR_ENTRY                                        "ma.location.info.entry"
#define MA_LOCATION_INFO_ATTR_ENTRY_DETAILS                                "ma.location.info.entry.details"
#define MA_LOCATION_INFO_ATTR_ENTRY_DATE                                   "ma.location.info.entry.date"
#define MA_LOCATION_INFO_ATTR_CONTACTS_LIST                                 "ma.location.info.contacts.list"

#endif /* MA_LOCATION_SERVICE_DEFS_H_INCLUDED */

