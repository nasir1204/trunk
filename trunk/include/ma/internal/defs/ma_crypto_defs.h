#ifndef MA_CRYPTO_DEFS_H_INCLUDED
#define MA_CRYPTO_DEFS_H_INCLUDED

/* Services, names , constants , datastore configurations */
#define MA_CRYPTO_SERVICE_NAME_STR                                      "ma.service.crypto"
#define MA_CRYPTO_CLIENT_NAME_STR                                       "ma.client.crypto"

/*  crypto service ePO/server policies */
#define MA_CRYPTO_SERVICE_SECTION_NAME_STR                              "CryptoService"
#define MA_CRYPTO_KEY_IS_INSTALLED_INT                                  "IsInstalled"
#define MA_CRYPTO_KEY_IS_ENABLED_INT                                    "IsEnabled"


/*  Crypto service datastore settings*/
#define MA_CRYPTO_PATH_NAME_STR                                         "AGENT\\SERVICES\\CRYPTO"
/*  Crypto service properties */
#define MA_CRYPTO_PROPERTIES_PATH_NAME_STR                              "AGENT\\SERVICES\\CRYPTO\\PROPERTIES"

/*  Crypto service local Policies */
#define MA_CRYPTO_POLICIES_PATH_NAME_STR                                "AGENT\\SERVICES\\CRYPTO\\POLICIES"

/*  Crypto service Data */
#define MA_CRYPOT_DATA_PATH_NAME_STR                                    "AGENT\\SERVICES\\CRYPTO\\DATA"



/* MA crypto Service Request Response Messages */
#define MA_PROP_KEY_CRYPTO_REQUEST_TYPE_STR                             "prop.key.crypto_request_type"
#define MA_PROP_KEY_CRYPTO_RESPONSE_TYPE_STR                            "prop.key.crypto_response_type"


/*request types */
#define MA_PROP_VALUE_ENCRYPT_REQUEST_STR                               "prop.value.encrypt_request"
#define MA_PROP_VALUE_DECRYPT_REQUEST_STR                               "prop.value.decrypt_request"
#define MA_PROP_VALUE_ENCODE_REQUEST_STR                                "prop.value.encode_request"
#define MA_PROP_VALUE_DECODE_REQUEST_STR                                "prop.value.decode_request"
#define MA_PROP_VALUE_ENCRYPT_ENCODE_REQUEST_STR                        "prop.value.encrypt_encode_request"
#define MA_PROP_VALUE_DECODE_DECRYPT_REQUEST_STR                        "prop.value.decode_decrypt_request"
#define MA_PROP_VALUE_HASH_REQUEST_STR                                  "prop.value.hash_request"

/* sub key type */
#define MA_PROP_KEY_ATTR_ALGO_ID_STR                                    "prop.key.attr_algo_id"


#endif /* MA_CRYPTO_DEFS_H_INCLUDED*/

