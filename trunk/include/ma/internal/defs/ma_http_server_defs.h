#ifndef MA_HTTP_SERVER_DEFS_H_INCLUDED
#define MA_HTTP_SERVER_DEFS_H_INCLUDED

/* Services, names , constants , datastore configurations */
#define MA_HTTP_SERVER_SERVICE_NAME_STR                                     "ma.service.http_server"
#define MA_HTTP_SERVER_CLIENT_NAME_STR                                      "ma.client.http_server"


/*  Http Server service ePO/server policies */
#define MA_HTTP_SERVER_SERVICE_SECTION_NAME_STR                             "HttpServerService"
#define MA_HTTP_SERVER_KEY_IS_INSTALLED_INT                                 "IsInstalled"
#define MA_HTTP_SERVER_KEY_IS_ENABLED_INT                                   "IsEnabled"
#define MA_HTTP_SERVER_KEY_PORT_INT                                         "HttpServerPort"
#define MA_HTTP_SERVER_KEY_BROADCAST_PORT_INT                               "HttpServerBroadcastPort"
#define MA_HTTP_SERVER_KEY_ENABLE_AGENT_PING_INT                            "IsAgentPingEnabled"
#define MA_HTTP_SERVER_KEY_ENABLE_LAZY_CACHING_INT                          "IsLazyCachingEnabled"
#define MA_HTTP_SERVER_KEY_ENABLE_SUPER_AGENT_INT                           "IsSuperAgentEnabled"
#define MA_HTTP_SERVER_KEY_ENABLE_SUPER_AGENT_REPOSITORY_INT                "IsSuperAgentRepositoryEnabled"
#define MA_HTTP_SERVER_KEY_ENABLE_DATACHANNEL_PING_INT                      "IsDataChannelPingEnabled"
#define MA_HTTP_SERVER_KEY_LISTEN_TO_EPO_SERVER_ONLY_INT                    "IsListenToEPOServerOnly"
#define MA_HTTP_SERVER_KEY_REPOSITORY_SYNC_INTERVAL_INT                     "RepositorySyncInterval"
#define MA_HTTP_SERVER_KEY_REPOSITORY_PURGE_INTERVAL_INT                    "ContentLongevity"
#define MA_HTTP_SERVER_KEY_REPOSITORY_DISK_QUOTA_INT						"DiskQuota"
#define	MA_HTTP_SERVER_KEY_SAREPO_CONCURRENT_CONNECTIONS_LIMIT_INT			"SAConcurrentConnectionsLimit"
#ifdef MA_WINDOWS
#define MA_HTTP_SERVER_KEY_VIRTUAL_DIRECTORY_STR							"VirtualDirectory"
#else
#define MA_HTTP_SERVER_KEY_VIRTUAL_DIRECTORY_STR							"VirtualDirectoryUnix"
#endif


#define MA_HTTP_SERVER_DEFAULT_TCP_PORT_INT                                  8081
#define MA_HTTP_SERVER_DEFAULT_TCP_PORT_STR                                 "8081"

/*  Http Server service datastore settings*/
#define MA_HTTP_SERVER_PATH_NAME_STR										"AGENT\\SERVICES\\HTTPSERVER"
/*  Http Server service properties */
#define MA_HTTP_SERVER_PROPERTIES_PATH_NAME_STR								"AGENT\\SERVICES\\HTTPSERVER\\PROPERTIES"

/*  Http server service local Policies */
#define MA_HTTP_SERVER_POLICIES_PATH_NAME_STR								"AGENT\\SERVICES\\HTTPSERVER\\POLICIES"

/*  Http Server service Data */
#define MA_HTTP_SERVER_DATA_PATH_NAME_STR									"AGENT\\SERVICES\\HTTPSERVER\\DATA"

/* Http server header name and value constants */
#define MA_HTTP_SERVER_FILE_HASH_HEADER_NAME_STR							"FileHash"
#define MA_HTTP_SERVER_FILE_HASH256_HEADER_NAME_STR							"FileHash256"
#define MA_HTTP_SERVER_USER_AGENT_HEADER_NAME_STR							"User-Agent"
#define MA_HTTP_SERVER_CONTENT_LENGTH_HEADER_NAME_STR						"Content-Length"
#define MA_HTTP_SERVER_CONTENT_TYPE_HEADER_NAME_STR							"Content-Type"
#define MA_HTTP_SERVER_SIGNATURE_HEADER_NAME_STR							"Signature"

#define MA_HTTP_SERVER_USER_AGENT_HEADER_VALUE_STR							"McAfee Agent"


/* msgbus servers in http factory handlers */
#define MA_HTTP_SERVER_MSG_KEY_TYPE_STR										"prop.key.msg_type"

#define	MA_HTTP_SERVER_MSG_PROP_VAL_RQST_RUN_PURGE_TASK_STR					"prop.val.rqst.run_purge_task"

#define	MA_HTTP_SERVER_MSG_PROP_VAL_RPLY_RUN_PURGE_TASK_STR					"prop.val.rply.run_purge_task"

#define	MA_HTTP_SERVER_MSG_PROP_VAL_RQST_SUSPEND_P2P_SERVING_STR			"prop.val.rqst.suspend_p2p_serving"
#define	MA_HTTP_SERVER_MSG_PROP_VAL_RPLY_SUSPEND_P2P_SERVING_STR			"prop.val.rply.suspend_p2p_serving"

#define	MA_HTTP_SERVER_MSG_PROP_VAL_RQST_RESUME_P2P_SERVING_STR				"prop.val.rqst.resume_p2p_serving"
#define	MA_HTTP_SERVER_MSG_PROP_VAL_RPLY_RESUME_P2P_SERVING_STR				"prop.val.rply.resume_p2p_serving"

/*  HTTP server service constants */
#ifdef MA_USE_HTTP_SERVER_STATIC
# define MA_USE_xxx_STATIC
#endif 

#ifdef MA_BUILDING_HTTP_SERVER_SHARED
# define MA_BUILDING_xxx_SHARED
#endif 


/* input: MA_BUILDING_xxx_SHARED, MA_USE_xxx_STATIC, output: MA_xxx_API  */

/* Windows - set up dll import/export decorators. */
#if defined(_MSC_VER) && defined(MA_BUILDING_xxx_SHARED)
/* Building shared library. */
# define MA_xxx_API __declspec(dllexport)
#elif defined(_MSC_VER) && !defined(MA_USE_xxx_STATIC)
/* Using shared library. */
#   define MA_xxx_API __declspec(dllimport)
#elif defined(__GNUC__) && __GNUC__ >= 4
# define MA_xxx_API __attribute__((visibility("default")))
#else
# define MA_xxx_API /* nothing */
#endif

#undef MA_USE_xxx_STATIC
#undef MA_BUILDING_xxx_SHARED

#define MA_HTTP_SERVER_API MA_xxx_API

#endif /* MA_HTTP_SERVER_DEFS_H_INCLUDED */

