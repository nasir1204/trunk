#ifndef MA_BOARD_SERVICE_DEFS_H_INCLUDED
#define MA_BOARD_SERVICE_DEFS_H_INCLUDED

/* Services, names , constants , datastore configurations */
#define MA_BOARD_SERVICE_NAME_STR                                       "ma.service.board"
#define MA_BOARD_CLIENT_NAME_STR                                        "ma.client.board"
#define MA_BOARD_SERVICE_HOSTNAME_STR                                   "localhost"
#define MA_BOARD_BASE_PATH_STR                                          "board"


/*  board service server policies */
#define MA_BOARD_SERVICE_SECTION_NAME_STR                               "BoardService"
#define MA_BOARD_KEY_IS_INSTALLED_INT                                   "IsInstalled"
#define MA_BOARD_KEY_IS_ENABLED_INT                                     "IsEnabled"


/*  board service datastore settings*/
#define MA_BOARD_PATH_NAME_STR                                          "AGENT\\SERVICES\\BOARD"
/*  board service properties */
#define MA_BOARD_PROPERTIES_PATH_NAME_STR                               "AGENT\\SERVICES\\BOARD\\PROPERTIES"

/*  board service local Policies */
#define MA_BOARD_POLICIES_PATH_NAME_STR                                 "AGENT\\SERVICES\\BOARD\\POLICIES"

/*  board service Data */
#define MA_BOARD_DATA_PATH_NAME_STR                                     "AGENT\\SERVICES\\BOARD\\DATA"


/* board service messages */
#define MA_BOARD_SERVICE_MSG_TYPE                                       "ma.board.op.msg.type"
#define MA_BOARD_SERVICE_MSG_REGISTER_TYPE                              "ma.board.op.msg.register.type"
#define MA_BOARD_SERVICE_MSG_UNREGISTER_TYPE                            "ma.board.op.msg.unregister.type"
#define MA_BOARD_SERVICE_MSG_GET_TYPE                                   "ma.board.op.msg.get.type"
#define MA_BOARD_SERVICE_MSG_GET_BOARD_SELECTION                        "ma.board.op.msg.get.board.selection"
#define MA_BOARD_SERVICE_MSG_ADD_BOARD                                  "ma.board.op.msg.add.board"
#define MA_BOARD_ID                                                     "ma.board.id"
#define MA_BOARD_ID_EXIST                                               "ma.board.id.exist"
#define MA_BOARD_ID_NOT_EXIST                                           "ma.board.id.not.exist"
#define MA_BOARD_STATUS                                                 "ma.board.status"
#define MA_BOARD_ID_WRITTEN                                             "ma.board.id.written"
#define MA_BOARD_ID_DELETED                                             "ma.board.id.deleted"

/* admin board */
#define MA_BOARD_INFO_ADMIN_MAIL_ID                                     "admin@mileaccess.com"
#define MA_BOARD_INFO_ADMIN_PASSWORD                                    "admin@mileaccess"
#define MA_BOARD_INFO_ADMIN                                             "admin"

/* pin details */
#define MA_BOARD_PINS_ATTR_ID                                           "ma.board.pins.id"
#define MA_BOARD_PINS_ATTR_TOPIC                                       "ma.board.pins.topic"

/* topic details */
#define MA_BOARD_TOPICS_ATTR_ID                                         "ma.board.topic.id"
#define MA_BOARD_TOPICS_ATTR_SIZE                                       "ma.board.topic.size"
#define MA_BOARD_TOPICS_ATTR_PINS                                       "ma.board.topic.pins"
#define MA_BOARD_TOPICS_ATTR_BOARD                                      "ma.board.topic.board"

/* info details */
#define MA_BOARD_INFO_ATTR_ID                                         "ma.board.info.id"
#define MA_BOARD_INFO_ATTR_SIZE                                       "ma.board.info.size"
#define MA_BOARD_INFO_ATTR_TOPICS                                     "ma.board.info.topics"
#define MA_BOARD_INFO_ATTR_BOARD                                      "ma.board.info.board"
#define MA_BOARD_INFO_ATTR_IMAGE_URL                                  "board_image_url"
#define MA_BOARD_INFO_ATTR_BOARDS                                     "board"

#endif /* MA_BOARD_SERVICE_DEFS_H_INCLUDED */
