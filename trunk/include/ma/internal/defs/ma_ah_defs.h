#ifndef MA_AH_DEFS_H_INCLUDED
#define MA_AH_DEFS_H_INCLUDED

/* Services, names , constants , datastore configurations */
#define MA_AH_SERVICE_NAME_STR                                          "ma.service.ahclient"
#define MA_AH_SERVICE_HOSTNAME_STR                                      "localhost"


/*  AH service ePO/server policies */
#define MA_AH_SERVICE_SECTION_NAME_STR                                  "AHService"
#define MA_AH_KEY_IS_INSTALLED_INT                                      "IsInstalled"
#define MA_AH_KEY_IS_ENABLED_INT                                        "IsEnabled"

/*  AH service datastore settings*/
#define MA_AH_PATH_NAME_STR                                             "AGENT\\SERVICES\\AH"
/*  AH service properties */
#define MA_AH_PROPERTIES_PATH_NAME_STR                                  "AGENT\\SERVICES\\AH\\PROPERTIES"
/*  AH service local Policies */
#define MA_AH_POLICIES_PATH_NAME_STR                                    "AGENT\\SERVICES\\PROPERTY\\POLICIES"
/* AH sitelist policies */
#define MA_AH_POLICIES_SITELIST_PATH_NAME_STR                           "AGENT\\SERVICES\\AH\\POLICIES\\SITELIST"
#define MA_AH_KEY_SITELIST_GLOBAL_VERSION_STR                           "AH_SITELIST_GLOBAL_VERSION"
#define MA_AH_KEY_SITELIST_LOCAL_VERSION_STR                            "AH_SITELIST_LOCAL_VERSION"
#define MA_AH_KEY_SITELIST_CA_FILE_STR                                  "AH_SITELIST_CA_FILE"

#define MA_AH_KEY_SITELIST_SITE_PATH_NAME_STR                           "AGENT\\SERVICES\\AH\\POLICIES\\SITELIST\\%s"
#define MA_AH_KEY_SITELIST_SERVER_FQDN_STR                              "AH_SITELIST_SERVER_FQDN"
#define MA_AH_KEY_SITELIST_SERVER_NAME_STR                              "AH_SITELIST_SERVER_NAME"
#define MA_AH_KEY_SITELIST_SERVER_IP_ADDRESS_STR                        "AH_SITELIST_SERVER_IP_ADDRESS"
#define MA_AH_KEY_SITELIST_SERVER_VERSION_NUMBER_STR                    "AH_SITELIST_SERVER_VERSION_NUMBER"
#define MA_AH_KEY_SITELIST_SERVER_PORT_INT                              "AH_SITELIST_SERVER_PORT"
#define MA_AH_KEY_SITELIST_SERVER_SSL_PORT_INT                          "AH_SITELIST_SERVER_SSL_PORT"
#define MA_AH_KEY_SITELIST_SITE_ORDER_INT                               "AH_SITELIST_SITE_ORDER"

/*  AH service Data */
#define MA_AH_DATA_PATH_NAME_STR                                        "AGENT\\SERVICES\\AH\\DATA"

/*  AH service constants */
#define MA_AH_CA_BUNDLE_FILE_NAME_STR                                   "cabundle.cer"
#define STR_DATACHANNEL_SPIPE_INFO_BLOCK_COUNT							".COUNT"
#define MA_MAX_SPIPE_SIZE												((9*1024*1024) + (900000))

#endif /* MA_AH_DEFS_H_INCLUDED*/

