/* Abstraction over [Msg]WaitForMultipleObjects[Ex] 
 * User can add or remove watchers whose callbacks will be invoked when the associated event is signaled
 */
#ifndef MA_WIN_EVENT_LOOP_H
#define MA_WIN_EVENT_LOOP_H

#include "ma/ma_common.h"

#ifdef MA_WINDOWS
#include <stddef.h>

MA_CPP(extern "C" {)

typedef struct ma_win_event_loop_s ma_win_event_loop_t, *ma_win_event_loop_h; /* opaque type representing the event loop */
typedef struct ma_win_event_watcher_s ma_win_event_watcher_t, *ma_win_event_watcher_h; /* watcher type, defined below */

typedef void *ma_win_handle_t; /* e.g. HANDLE */

/* signature of watcher callback function */
typedef void (*ma_win_event_watcher_cb) (ma_win_event_watcher_t *w, int result);

struct ma_win_event_watcher_s {
    ma_win_handle_t h;
    ma_win_event_watcher_cb cb;
    void *data;
    };


/** Options that can be specified when creating a ma_win_event_loop_t */
enum ma_win_event_loop_options_e {

    /* also check for Windows messages (consider making this another watcher type...) */
    MA_WIN_EVENT_LOOP_OPT_MESSAGES = 0x1, 
    };

/**
 * creates a new instance of the event loop
 * @param flags a combination zero or more of MA_WIN_EVENT_LOOP_OPT_xxx options
 * 
 * @return MA_OK on success or an error code
 */
ma_error_t ma_win_event_loop_create(unsigned flags, ma_win_event_loop_t **loop);

/**
 * destroys an event loop and any associated resources
 */
ma_error_t ma_win_event_loop_destroy(ma_win_event_loop_t *loop);

/**
 * Adds a new watcher
 */
ma_error_t ma_win_event_loop_add_watcher(ma_win_event_loop_t *loop, ma_win_event_watcher_t *w);

/**
 * Removes a watcher
 */
ma_error_t ma_win_event_loop_remove_watcher(ma_win_event_loop_t *loop, ma_win_event_watcher_t *w);

/* 
 * returns the number of registered watchers
 */
ma_error_t ma_win_event_loop_get_watcher_count(const ma_win_event_loop_t *loop, size_t *count);


/*
 * ma_win_event_loop_run_once results
 */
#define MA_WIN_EVENT_LOOP_BREAK_NO_EVENTS 0x1 /* no events */
#define MA_WIN_EVENT_LOOP_BREAK_PENDING_WIN_MESSAGES 0x2 /* One or more Windows messages pending */
#define MA_WIN_EVENT_LOOP_BREAK_EVENTS_PROCESSED 0x4 /* processed at least 1 event */

/**
 * process pending events, then return
 * 
 * @param self
 * @param wait set to TRUE to wait for at least 1 one event to be processed. This may block
 * @break_reason a pointer to a variable that receives the reason for the function to return, PENDING
 */
ma_error_t ma_win_event_loop_run_once(ma_win_event_loop_t *loop, int wait, unsigned *break_reason);

/**
 * process events until explicitly stopped
 */
ma_error_t ma_win_event_loop_run(ma_win_event_loop_t *loop);

/**
 * stops the processor
 */
ma_error_t ma_win_event_loop_stop(ma_win_event_loop_t *loop);


void ma_win_event_watcher_init(ma_win_event_watcher_t *w, ma_win_handle_t h, ma_win_event_watcher_cb cb, void *data);

MA_CPP(})

#endif /* MA_WINDOWS */

#endif