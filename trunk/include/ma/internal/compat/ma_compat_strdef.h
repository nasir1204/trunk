#ifndef MA_COMPAT_STRDEF_HPP_INCLUDED
#define MA_COMPAT_STRDEF_HPP_INCLUDED

#define MA_COMPAT_SERVICE_LOG_FILE_NAME_STR		"macompatsvc"

#ifdef MA_WINDOWS
#define REG_ROOT_PATH							"Software\\"
#define REG_AGENT_PATH							"McAfee\\Agent\\"
#define REG_AGENT_DATAPATH_KEY					"DataPath"
#define REG_AGENT_INSTALLPATH_KEY				"InstallPath"
#endif


// Framework (-1600)
#define ERROR_SUBSYS_DEINITIALIZED			-1600	// the requested subsystem in not intialized
#define ERROR_SUBSYS_NOT_FOUND				-1601	// the requested subsystem was not found
#define ERROR_SUBSYS_STOPPED				-1602	// the subsystem is not running
#define ERROR_SUBSYS_SUSPENDED				-1603	// the subsystem is suspended
#define ERROR_SUBSYS_DISABLED				-1604	// the subsystem is not registered for use (see framework manifest)
#define ERROR_SUBSYS_STOPPING				-1605	// the subsystem is stopping, can't process any more

// Framework Factory (-1700)
#define ERROR_CALLBACK_NOT_REGISTERED		-1700	// caller must register callback before using factory
#define ERROR_FRAMEWORK_UNAVAILABLE			-1701	// the framework service is not available
#define ERROR_FRAMEWORK_AVAILABLE			-1702	// the framework is currently available

#define ERROR_NAI_CREATE_EVENT				-18		// could not open/create a required event
#define ERROR_MANAGER_CREATE_THREAD			-1116	// the epo thread could not be created

#define ERROR_NAI_COM_IFACE_UNSUPPORTED		 -33		// the object does not support the required interface
#define ERROR_INSUFFICIENT_MEMORY			-3		// failed to allocate required memory

#define MA_COMPAT_STOP_EVENT_GLOBAL_OBJECT_NAME_STR     L"Global\\COMPAT_FRAMEWORK_SERVICE_EVENT_STOPPED"

#define MA_COMPAT_SERVICE_SINGLE_INSTANCE_MUTEX					L"MCAFEE_B100C17B-DF56-465A-87BD-7FB310377FB6_COMPAT_MUTEX"
#define MA_COMPAT_LPC_HEARTBEAT_EVENT_GLOBAL_OBJECT_NAME_STR     L"Global\\McAfeeAgentTimerEvent2"
#define MA_COMPAT_SERVICE_SHUTDOWN_EVENT_GLOBAL_OBJECT_NAME_STR     L"Global\\McAfeeCompatServiceAgentShutdownTimerEvent"

#define MA_COMPAT_SCHEDULER_SUBSYSTEM_STR               L"Scheduler"
#define MA_COMPAT_SCHEDULER_SUBSYSTEM_CLSID_STR         L"{4EF17F94-3975-4ACF-B228-29485BDE5860}"

#define MA_COMPAT_UPDATE_SUBSYSTEM_STR                  L"Updater"
#define MA_COMPAT_UPDATE_SUBSYSTEM_CLSID_STR            L"{6E389062-C540-4145-96B9-B8745CF7D856}"

#define MA_COMPAT_LOGGING_SUBSYSTEM_STR                 L"Logging"
#define MA_COMPAT_LOGGING_SUBSYSTEM_CLSID_STR           L"{83D7FBE7-E507-4486-8785-8D1776D498F4}"

#define MA_COMPAT_AGENT_SUBSYSTEM_STR                 L"Agent"
#define MA_COMPAT_AGENT_SUBSYSTEM_CLSID_STR           L"{3153E8D7-C724-47CC-A7FE-5C90EB4093A0}"

#define MA_COMPAT_FRAMEWORK_CLSID_STR                   L"{896511A7-6957-459C-9041-13DA4BA2D42F}"      

#endif /* MA_COMPAT_STRDEF_HPP_INCLUDED */

