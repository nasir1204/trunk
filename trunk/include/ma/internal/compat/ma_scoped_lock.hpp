#ifndef MA_SCOPED_LOCK_H_INCLUDED
#define MA_SCOPED_LOCK_H_INCLUDED

#include "ma/internal/utils/threading/ma_thread.h"

namespace mileaccess{
namespace ma{
namespace compat{

class ScopedLock
{
	ma_mutex_t *mutex;
	public:
		ScopedLock(ma_mutex_t *mtx):mutex(mtx){
			ma_mutex_lock(mutex);
		}
		~ScopedLock(){
			ma_mutex_unlock(mutex);
		}
};

};};};

#endif
