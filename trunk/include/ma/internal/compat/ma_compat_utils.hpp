#ifndef MA_COMPAT_UTILS_H_INCLUDED
#define MA_COMPAT_UTILS_H_INCLUDED

#include "ma/internal/utils/text/ma_utf8.h"
#include "ma/variant.hxx"
#include <string>

namespace mileaccess{
namespace ma{
namespace compat{

class Utils
{
	Utils(){};
	~Utils(){};
	public:
		static inline std::string convertString(const std::wstring &wstr)throw(){
			if(wstr.length()){
				try{
					mileaccess::ma::variant v(wstr.c_str());
					return mileaccess::ma::get<std::string>(v);			
				}
				catch(...){}
			}
			return std::string("");			
		}

		static inline std::string convertString(const wchar_t *wstr)throw(){
			std::wstring tmp(wstr? wstr : L"");			
			return convertString(tmp);					
		}
		
		static inline std::wstring convertString(const std::string &str)throw(){
			if(str.length()){
				try{
					mileaccess::ma::variant v(str.c_str());
					return mileaccess::ma::get<std::wstring>(v);			
				}
				catch(...){}
			}
			return std::wstring(L"");			
		}

		static inline std::wstring convertString(const char *str)throw(){
			std::string tmp(str ? str : "");			
			return convertString(tmp);		
		}

		static inline std::wstring convertString(ma_variant_t *var)throw(){	
			try{
				mileaccess::ma::variant v(var);
				return mileaccess::ma::get<std::wstring>(v);
			}
			catch(...){}
			return std::wstring(L"");			
		}
};

};};};

#endif
