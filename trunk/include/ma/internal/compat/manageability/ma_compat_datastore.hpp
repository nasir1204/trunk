#ifndef MA_COMPAT_DATASTORE_H_INCLUDED
#define MA_COMPAT_DATASTORE_H_INCLUDED

#include "ma/internal/compat/ma_compat_common.hpp"
#include "lpc/manageability/moipccommon.hpp"
#include "ma/internal/compat/manageability/ma_compat_poplugin.h"
#include "lpc/manageability/moipccommon.hpp"

#define GET_COMPAT_DATA_STORE() ICompatDataStore::getInstance()

namespace mileaccess{
namespace ma{
namespace compat{

class MA_COMPAT_API ICompatDataStore
{
    static ICompatDataStore *datastore;
    public:
        static ICompatDataStore *getInstance()throw();        
		static void releaseInstance()throw();

        virtual ma_error_t getPolicyObjectContainer(const std::wstring &softwareid, mileaccess_com::MA::manageability::Lpc_PolicyObjectContainer &container) = 0 ;
        virtual ma_error_t getPolicyObjectContainer(const std::wstring &softwareid,  const std::wstring &domain, const std::wstring &user, mileaccess_com::MA::manageability::Lpc_PolicyObjectContainer &container) = 0 ;
        virtual ma_error_t getOnePolicySection(const std::wstring &feature, const std::wstring &category, const std::wstring &type, const std::wstring &name, const std::wstring &int_param, const std::wstring &str_param, const std::wstring &section_name, mileaccess_com::MA::manageability::Lpc_OneSectionHandle &section) = 0 ;
        virtual ma_error_t getAllPolicySections(const std::wstring &feature, const std::wstring &category, const std::wstring &type, const std::wstring &name, const std::wstring &int_param, const std::wstring &str_param, mileaccess_com::MA::manageability::Lpc_MultipleSectionsHandle &sections) = 0 ;
		virtual ma_error_t getAllUserPolicySections(const std::wstring &softwareid, const std::wstring &feature, const std::wstring &category, const std::wstring &type, const std::wstring &int_param, const std::wstring &str_param, mileaccess_com::MA::manageability::Lpc_MultipleSectionsHandle &sections) = 0 ;

        virtual ma_bool_t isLegacyPolicy(const std::wstring &softwareid) = 0 ;
        virtual ma_bool_t isNamedPolicy(const std::wstring &softwareid) = 0 ;
        virtual ma_bool_t isPolicyExists(const std::wstring &softwareid) = 0 ;

        virtual ma_error_t getPolicyObjectContainer(const std::wstring &softwareid, ICMAPolicyObjectList **policyobjectlist) = 0 ;
        virtual ma_error_t getPolicyObjectContainer(const std::wstring &softwareid, ICMAPolicySettingList **setting_list) = 0 ;

		virtual ma_error_t getAllUsersWithPolicy(std::string &software_id, std::vector<std::string> &users) = 0 ;
} ;

};};};



#endif  /* MA_COMPAT_DATASTORE_H_INCLUDED */
