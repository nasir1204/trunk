#ifndef MA_COMPAT_LPC_H_INCLUDED
#define MA_COMPAT_LPC_H_INCLUDED

#include "ma/internal/compat/ma_compat_common.hpp"

namespace mileaccess{
namespace ma{
namespace compat{

class MA_COMPAT_API ILpcService
{	
	static ILpcService *lpc_service;	
	protected:
		ILpcService() throw(){};
		virtual ~ILpcService() throw(){};
	public:		
		static ILpcService *getInstance() throw();	
		static void releaseInstance() throw();
		virtual ma_error_t start() throw() = 0;
		virtual ma_error_t stop() throw() = 0;
};

};};};

#endif
