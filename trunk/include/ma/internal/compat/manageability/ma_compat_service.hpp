#ifndef MA_COMPAT_SERVICE_H_INCLUDED
#define MA_COMPAT_SERVICE_H_INCLUDED

#include "ma/internal/compat/ma_compat_common.hpp"
#include "ma/internal/compat/manageability/ma_compat_watcher.hpp"
#include "ma/internal/compat/manageability/ma_compat_client_manager.hpp"
#include "ma/internal/compat/manageability/ma_compat_service_context.hpp"
#include "ma/internal/compat/manageability/ma_compat_lpc.hpp"

namespace mileaccess{
namespace ma{
namespace compat{


/*
Compat service 
1) Run a registry/cma.d config watcher.
2) Add a compat client for each point product at start
3) Remove the compat client for each point product at stop.
4) Add a compat client when it is installed.
5) Remove a compat client when it is uninstalled.
6) Handle the service stop request from agent service.
.*/

class ICompatService : public ICompatWatcherCallabck
{
	protected:
	ICompatService() throw(){};
	virtual ~ICompatService() throw(){};		
	virtual bool initialize(::ma_logger_t *logger) {return true;};
	virtual bool deinitialize() {return true;};
	static ICompatService *compat_service;	
    static ::ma_logger_t *m_plogger;
    public:		
        static void setLogger(::ma_logger_t *logger);
		static ICompatService *getInstance()throw();
		static void releaseInstance()throw();        
		virtual void applicationChangeNotify() = 0;		
    	virtual void reloadPlugin(const std::string &software_id) = 0;		
    	virtual void unloadPlugin(const std::string &software_id) = 0;		
    	virtual void run()throw() = 0;
        virtual void stop() = 0;
};


}}}

#endif
