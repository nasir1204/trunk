#ifndef MA_COMPAT_DC_SUBSCRIPTION_MANAGER_H_INCLUDED
#define MA_COMPAT_DC_SUBSCRIPTION_MANAGER_H_INCLUDED

#include "ma/internal/compat/ma_compat_common.hpp"
#include "ma/internal/utils/threading/ma_thread.h"
#include <map>
#include <list>
#include <string>


enum SubscriptionType{
	eInvalid = 0,
	ePrimary = 1,
	eListener = 2,
	eTupple = 3
};
struct SubscriptionInfo 
{
	std::string sName;
	SubscriptionType sType;
};

typedef std::list<SubscriptionInfo> DcSubsciptionList;
typedef std::map<std::string, DcSubsciptionList> DcSubscriptionMap;

class MA_COMPAT_API CompatDcSubscriptionManager
{
private:
	static CompatDcSubscriptionManager *s_instance;
private:
	DcSubscriptionMap m_SubscriptionMap;
	ma_mutex_t m_mutex;
	std::string  m_PersistenceDirectory;
private:
	CompatDcSubscriptionManager();
	CompatDcSubscriptionManager(const CompatDcSubscriptionManager&){}
	~CompatDcSubscriptionManager();

	bool isSubscribed(const std::string &product_id, const std::string &dc_message_id);
public:
	static CompatDcSubscriptionManager * GetInstance();
	static void ReleaseInstance();
public:
	void AddSubscription(const std::string &product_id, const std::string &dc_message_id, SubscriptionType sType);
	void RemoveSubscription(const std::string &product_id, const std::string &dc_message_id);
	DcSubscriptionMap GetSubscriptionInformation();
public:
	bool WriteToFile();
	bool LoadFromFile();
};

#endif
