#ifndef MA_COMPAT_APPLICATION_INFO_H_INCLUDED
#define MA_COMPAT_APPLICATION_INFO_H_INCLUDED

#include "ma/internal/compat/ma_compat_common.hpp"

#include <map>
#include <string>
#include <vector>
#include <set>

namespace mileaccess{
namespace ma{
namespace compat{

class MA_COMPAT_API ApplicationInfo
{	
	public:
		ApplicationInfo():SoftwareId(""), AuthorizationHash(""), InstallPath(""), Version(""), ProductName(""), Language(""), PluginPath(""), EnforceFlag(0), PolicyFlag(0), PluginFlag(0), UseIpc(0){};
		~ApplicationInfo(){};

		std::string SoftwareId;
		std::string AuthorizationHash;
		std::string InstallPath;
		std::string Version;
		std::string ProductName;
		std::string Language;
		std::string PluginPath;
		int EnforceFlag;
		int PolicyFlag;
		int PluginFlag;
		int UseIpc;	
};


class MA_COMPAT_API ApplicationStore
{
	ApplicationStore();
	~ApplicationStore();
	public:
    	static ApplicationInfo getApplicationInfo(const std::string &software_id);
        static std::set<std::string> getAllApplicationID() ;
};

}}}
#endif
