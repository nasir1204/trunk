/* Copyright (C) 2005 by McAfee, Inc. All Rights Reserved. */
/* Copyright (C) 2005 by McAfee Inc;All rights reserved */
#ifndef TASK_STATUS_H_INCLUDED
#define TASK_STATUS_H_INCLUDED

enum schedulerTaskStatus
  {
    sched_task_running = 0,//Will invoke stop task and free cookie
    sched_task_finished = 1,//Will invoke free cookie
    sched_task_failed = -3, //Will invoke free cookie
    sched_task_stop_initiated = -2, //No stop and no free cookie
    sched_task_query_timed_out = -1, //No Stop and no free cookie
    sched_task_stop_failed = -4,  //No stop and no free cookie
    sched_task_unknown = -5,      //No stop and no free cookie
    sched_task_unknown_error = -6 
  };

#endif
