/* Copyright (C) 2005 by McAfee, Inc. All Rights Reserved. */
#ifndef POPLUGIN_H
#define POPLUGIN_H

//=====================================
// For McAfee Agent Version 1.0
//=====================================
//#ifndef TASK_STATUS_H_INCLUDED
//#error include taskstatus.h first
//#endif  //ifdef TASK_STATUS_H_INCLUDED

#define MAX_APP_DATA            3072
#include <stdio.h>

//=====================================
// C++ Policy Object Interface
//=====================================

template <class TYPE>
class CMAPtr
{
	TYPE *ptr_;
	public:
	inline CMAPtr()
	{
		ptr_ = NULL;
	}

	inline void deletePtr(void)
	{
		if (ptr_ != NULL)
		{
			/* Don't use the calling program's 'delete' function -- it may be from
			a different run-time library and have a different heap from the one
			CMA allocated ptr_ from. */
			ptr_->deleteThis ();
		}
	}

	inline ~CMAPtr()
	{
		deletePtr();
	}

	inline void operator=(TYPE *ptr)
	{
		deletePtr ();
		ptr_ = ptr;
	}
	inline operator TYPE *&(){ 	return ptr_; 	}

	inline TYPE *operator ->() const {	return ptr_;	}
};

typedef CMAPtr<class ICMAString> CMAStringPtr;
typedef CMAPtr<class ICMAPolicyObjectList> CMAPolicyObjectListPtr;
typedef CMAPtr<class ICMAPolicySettingObjectList> CMAPolicySettingObjectListPtr;
typedef CMAPtr<class ICMAPolicySettingList> CMAPolicySettingListPtr;

class ICMAString
{
	public:
		virtual const wchar_t *get (void) = 0 ;

		virtual ~ICMAString() 
		{
		}
		virtual void deleteThis (void) = 0 ;
};

class ICMAPolicySettingList
{
	public:
		virtual bool get (
				wchar_t *sectionName,
				wchar_t *settingName,
				CMAStringPtr &value) = 0;

		virtual bool getNext (
				CMAStringPtr &sectionName,
				CMAStringPtr &settingName,
				CMAStringPtr &value) = 0;

		virtual bool reset (void) = 0 ;

		virtual void deleteThis (void) = 0 ;
};

class ICMAPolicySettingObjectList
{
	public:
		virtual bool getNext (
				long &paramInt,
				CMAStringPtr &paramStr,
				CMAStringPtr &name,
				CMAPolicySettingListPtr &policySettingList) = 0;

		virtual bool reset (void) = 0 ;

		virtual void deleteThis (void) = 0 ;
};


class ICMAPolicyObjectList
{
	public:
		virtual bool getNext (
				CMAStringPtr &feature,
				CMAStringPtr &category,
				CMAStringPtr &type,
				CMAStringPtr &name,
				CMAPolicySettingObjectListPtr &policySettingObjectList) = 0;

		virtual void getSoftwareId (
				CMAStringPtr &softwareId) = 0;

		virtual bool reset (void) = 0 ;

		virtual void deleteThis (void) = 0 ;
};




#ifdef __cplusplus
extern "C" {
#endif  //ifdef __cplusplus


/**
 *\enum Enumeration describing the types of data that can be interchanged between NWA and the plugin
*/
enum EXCHANGE_DATA_TYPE
{
	///The data type represents a  wide  string
	VAL_WSTRING     = 0x00,
	///The data type represents a dword
	VAL_DWORD      = 0x01,
	///The data type represents a 64 bit integer
	VAL_INT64      = 0x02,
	///data is binary daya
	VAL_BINARY     = 0x03
};
  

#define POPLUGIN_GETPROPERTIES		"POPLUGIN_GetProperties"
#define POPLUGIN_ENFORCEPOLICY		"POPLUGIN_EnforcePolicy"
#define POPLUGIN_ENFORCETASK		"POPLUGIN_EnforceTask"
#define POPLUGIN_GETTASKSTATUS		"POPLUGIN_GetTaskStatus"
#define POPLUGIN_STOPTASK		"POPLUGIN_StopTask"
#define POPLUGIN_FREECOOKIE		"POPLUGIN_FreeCookie"
#define POPLUGIN_ISSOFTWAREINSTALLED	"POPLUGIN_IsSoftwareInstalled"
#define POPLUGIN_ISINTERFACEUNICODE	"POPLUGIN_IsInterfaceUnicode"
#define POPLUGIN_ENFORCEPOLICYOBJECT	"POPLUGIN_EnforcePolicyObject"  // EPO 3.6
#define POPLUGIN_NOTIFYTASKDELETE	"POPLUGIN_NotifyTaskDelete"




// Callback Signatures
typedef int ( *PO_SETPROP_PROC)(
				const wchar_t *lpszSoftwareID, 
				const wchar_t *lpszSectionName, 
				const wchar_t *lpszKeyName, 
				const unsigned char * lpValue, 
				int iValType, 
				unsigned long dwSize);
  
typedef int ( *PO_GETPOLICY_PROC)(
				const wchar_t * lpszSoftwareID, 
				const wchar_t * lpszSectionName, 
				const wchar_t * lpszKeyName, 
				unsigned char * lpValue, 
				int iValType, 
				unsigned long * lpdwSize);
  
typedef int ( *PO_GETTASKOPTION_PROC)(
				const wchar_t * lpszSoftwareID, 
				const wchar_t * lpszTaskID, 
				const wchar_t * lpszSectionName, 
				const wchar_t * lpszKeyName, 
				unsigned char * lpValue, 
				int iValType, 
				unsigned long * lpdwSize);

typedef bool (*POPLUGIN_ENFORCEPOLICYOBJECT_PROC)( ICMAPolicyObjectList *policyObjectList ); // EPO 3.6

//Plugin API signatures
  
typedef int (*POPLUGIN_GETPROPERTIES_PROC)(
				const wchar_t * lpszSoftwareID, 
				PO_SETPROP_PROC fnSetPropProcW );
  
typedef int (*POPLUGIN_ENFORCEPOLICY_PROC)( 
				const wchar_t * lpszSoftwareID, 
				PO_GETPOLICY_PROC fnGetPolicyProcW );
  
typedef int (*POPLUGIN_ENFORCETASK_PROC)( 
				const wchar_t * lpszSoftwareID, 
				const wchar_t * lpszTaskID, 
				PO_GETTASKOPTION_PROC fnGetTaskOptionProcW, 
				void **lplpCookie );
  
  
  
  
  
typedef schedulerTaskStatus  (*POPLUGIN_GETTASKSTATUS_PROC)(
				const wchar_t *,
				void * lpCookie );

typedef schedulerTaskStatus  (*POPLUGIN_STOPTASK_PROC)(
				const wchar_t *,
				void * lpCookie );

typedef int (*POPLUGIN_FREECOOKIE_PROC)(
				const wchar_t *,
				void * lpCookie ); 

typedef int (*POPLUGIN_ISSOFTWAREINSTALLED_PROC)(
				const wchar_t *);
  
typedef int (*POPLUGIN_NOTIFY_TASK_DELETE_PROC)(
				const wchar_t *lpszSoftwareId, 
				const wchar_t *taskId);

int POPLUGIN_GetProperties(
			const wchar_t *lpszSoftwareID, 
			PO_SETPROP_PROC fnSetPropProc);
  
  
int POPLUGIN_EnforcePolicy(
			const wchar_t *lpszSoftwareID, 
			PO_GETPOLICY_PROC fnGetPolicyProc);


int POPLUGIN_EnforceTask(
			const wchar_t *lpszSoftwareID, 
			const wchar_t * lpszTaskID, 
			PO_GETTASKOPTION_PROC fnGetTaskOptionProc, 
			void **lplpCookie);
  
schedulerTaskStatus POPLUGIN_GetTaskStatus(
			const wchar_t *lpszSoftwareId,
			void * lpCookie);
  
schedulerTaskStatus POPLUGIN_StopTask(
			const wchar_t * lpszSoftwareId, 
			void * lpCookie);

int POPLUGIN_FreeCookie(
			const wchar_t * lpszSoftwareId,
			void * lpCookie);

int POPLUGIN_IsSoftwareInstalled(
			const wchar_t * lpszSoftwareId);

bool POPLUGIN_EnforcePolicyObject (ICMAPolicyObjectList *policyObjectList); // EPO 3.6

int POPLUGIN_NotifyTaskDelete(const wchar_t *lpszSoftwareId,const wchar_t *lpszTaskId);

#ifdef __cplusplus
}
#endif //ifdef __cplusplus


#endif //ifdef POPLUGIN_H
