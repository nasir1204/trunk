#ifndef MA_COMPAT_WATCHER_H_INCLUDED
#define MA_COMPAT_WATCHER_H_INCLUDED

#include "ma/internal/compat/ma_compat_common.hpp"
#include "ma/internal/utils/watcher/ma_watcher.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/utils/event_loop/ma_loop_worker.h"

namespace mileaccess{
namespace ma{
namespace compat{

class ICompatWatcherCallabck
{
	public:
		ICompatWatcherCallabck(){};
		~ICompatWatcherCallabck(){};	
		virtual void applicationChangeNotify() = 0;
};

class CompatWatcher 
{
    ma_watcher_t *watcher;    
	ICompatWatcherCallabck *watcher_cb;
    ma_event_loop_t *loop;
    ma_loop_worker_t *loop_worker;

    CompatWatcher(const CompatWatcher&)throw(){};
    CompatWatcher& operator=(const CompatWatcher&)throw(){};  

    public :
        CompatWatcher(ma_event_loop_t *maloop, ma_loop_worker_t *loop_worker, ICompatWatcherCallabck *watcher) throw();
        ~CompatWatcher() throw();

        ma_error_t initialize();
        ma_error_t deinitialize();

        static ma_error_t compat_watcher_path_cb(ma_watcher_t *watcher, const char *name, void *cb_data);        
};


}}}

#endif  /*  MA_COMPAT_WATCHER_H_INCLUDED    */
