#ifndef MA_COMPAT_POPLUGIN_H_INCLUDED
#define MA_COMPAT_POPLUGIN_H_INCLUDED

#ifdef MA_WINDOWS
#include <Windows.h>
#include "ma/internal/compat/manageability/win/poplugin.h"
#else
#include "ma/internal/compat/manageability/unix/taskstatus.h"
#include "ma/internal/compat/manageability/unix/poplugin.h"
#endif

#endif 
