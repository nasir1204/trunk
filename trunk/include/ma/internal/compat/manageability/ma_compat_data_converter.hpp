#ifndef MA_COMPAT_DATA_CONVERTER_H_INCLUDED
#define MA_COMPAT_DATA_CONVERTER_H_INCLUDED

/*
compat headers
*/
#include "ma/internal/compat/ma_compat_common.hpp"

/*
MA 5.0 headers.
*/
#include "ma/property/ma_property.h"
#include "ma/policy/ma_policy.h"
#include "ma/scheduler/ma_scheduler.h"
#include "ma/repository/ma_repository_list.h"
#include "ma/proxy/ma_proxy_config.h"
/*
MA4.8- headers.
*/
#include "lpc/manageability/sitelistinfo.hpp"
#include "lpc/manageability/moipccommon.hpp"
using namespace mileaccess_com::MA::manageability;
using namespace mileaccess_com::MA::services ;
#include "ma/internal/compat/manageability/ma_compat_poplugin.h"

/*
system headers.
*/

namespace mileaccess{
namespace ma{
namespace compat{

class MA_COMPAT_API CompatDataConverter
{
    CompatDataConverter()throw(){} ;
    ~CompatDataConverter()throw(){} ;
    CompatDataConverter(const CompatDataConverter&)throw(){} ;
	
    static CompatDataConverter *compat_data_converter ;

	public:
		static CompatDataConverter *getInstance()throw() ;

		static void releaseInstance()throw() ;

		ma_error_t convertProperty(Lpc_MultipleSectionsHandle *lpc_props, ma_property_bag_t *bag)throw() ;

		ma_error_t convertPolicy(ma_policy_bag_t *policy_bag, Lpc_PolicyObjectContainer *lpc_policy_container)throw() ;

		ma_error_t convertPolicy(ma_policy_bag_t *policy_bag, ICMAPolicyObjectList *lpc_policy_container)throw();

		ma_error_t convertTask(ma_task_t *task, Lpc_MultipleSectionsHandle &lpcSectionHandle)throw();
     
        ma_error_t convertRepository(SiteListInfo *sitelist, ma_repository_list_t *list, ma_proxy_config_t *proxy_config)throw() ;

        ma_error_t convertRepository(ma_repository_list_t *list, ma_proxy_config_t *proxy_config, SiteListInfo &sitelist)throw() ;
};

};};};


#endif
