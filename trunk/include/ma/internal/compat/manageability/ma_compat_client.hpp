#ifndef MA_COMPAT_CLIENT_H_INCLUDED
#define MA_COMPAT_CLIENT_H_INCLUDED

#include "ma/internal/compat/ma_compat_common.hpp"
#include "ma/internal/compat/manageability/ma_compat_application_info.hpp"

#include "ma/internal/utils/threading/ma_atomic.h"
#include "ma/internal/utils/threading/ma_thread.h"
#include "ma/property/ma_property.h"
#include "ma/policy/ma_policy.h"
#include "ma/scheduler/ma_scheduler.h"
#include "ma/datachannel/ma_datachannel.h"

namespace mileaccess{
namespace ma{
namespace compat{

typedef struct ma_timer_s ma_timer_t;

class MA_COMPAT_API CompatClient
{		
	ma_mutex_t data_lock;		
	ma_mutex_t task_lock;
	ma_timer_t *task_status_poller;
	bool pc_active;
	bool pe_active;			
	bool tc_active;			
	bool is_started;		
	bool is_initialized;	

	bool doesNeedToCollectProps();
	bool doesNeedToEnforcePolicy();
	bool doesNeedToRunTask();		
	bool startPCSession();	
	void stopPCSession();
	bool startTCSession();	
	void stopTCSession();
	bool startPESession();
	void stopPESession();	
	protected:
		std::string software_id;
		std::wstring software_idw;
		ApplicationInfo app_info;

		bool isStarted();	
		bool isInitialize();	
		static void taskStatusPollerCb(ma_timer_t* handle, int status /*UNUSED*/);
	public:
		CompatClient(const std::string &software_id, const ApplicationInfo &app) throw();
		virtual ~CompatClient() throw();			
		const std::string &getSoftwareId() throw();
		const std::wstring &getSoftwareIdW() throw();

		void setApplicationInfo(ApplicationInfo &app, bool &is_changed) throw();
		const ApplicationInfo &getApplicationInfo() throw();

		virtual ma_error_t start() throw();
		virtual ma_error_t stop() throw();			

		virtual ma_error_t initialize()throw();
		virtual ma_error_t deinitialize()throw();
		
		ma_error_t safeCollectProperties(ma_property_bag_t *property_bag) throw();
		ma_error_t safeEnforcePolicy(const ma_policy_uri_t *info_uri) throw();
		ma_error_t safeEnforceTask(std::string &task_id,  std::string &task_type, ma_task_t *task) throw();
		ma_error_t safeUpdateTaskStatus(std::string &task_id, ma_task_t *task) throw();
		ma_error_t safeUpdateAllTaskStatus() throw();
		ma_error_t safeStopTask(std::string &task_id, ma_task_t *task) throw();
		ma_error_t safeRemoveTask(std::string &task_id, ma_task_t *task) throw();

		ma_error_t safeDcDataReceived(std::string product_id, std::string message_id, ma_int64_t correlation_id, ma_datachannel_item_t *dc_message, void *cb_data) throw();
		ma_error_t safeDcNotification(std::string product_id, std::string message_id, ma_int64_t correlation_id, ma_datachannel_item_notification_t status, void *cb_data) throw();

		virtual ma_error_t collectProperties(ma_property_bag_t *property_bag) throw() {return MA_OK;};
		virtual ma_error_t enforcePolicy(const ma_policy_uri_t *info_uri) throw() {return MA_OK;};
		virtual ma_error_t enforceTask(std::string &task_id, std::string &task_type,  ma_task_t *task) throw() {return MA_OK;};
		virtual ma_error_t updateTaskStatus(std::string &task_id, ma_task_t *task) throw(){return MA_OK;};
		virtual bool isAnyTaskRunning() throw(){return false;};
		virtual ma_error_t updateAllTaskStatus() throw(){return MA_OK;};
		virtual ma_error_t stopTask(std::string &task_id, ma_task_t *task) throw() {return MA_OK;};		
		virtual ma_error_t removeTask(std::string &task_id, ma_task_t *task) throw() {return MA_OK;};
		virtual void DcDataReceived(std::string product_id, std::string message_id, ma_int64_t correlation_id, ma_datachannel_item_t *dc_message, void *cb_data) throw() {};
		virtual void DcNotification(std::string product_id, std::string message_id, ma_int64_t correlation_id, ma_datachannel_item_notification_t status, void *cb_data) throw() {};
};

};};};


#endif
