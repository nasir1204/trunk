#ifndef MA_COMPAT_CLIENT_MANAGER_H_INCLUDED
#define MA_COMPAT_CLIENT_MANAGER_H_INCLUDED

#include "ma/internal/compat/ma_compat_common.hpp"
#include "ma/internal/compat/manageability/ma_compat_application_info.hpp"
#include "ma/internal/compat/manageability/ma_compat_client.hpp"
#include "ma/internal/compat/manageability/ma_compat_client_table.hpp"
#include "ma/internal/utils/threading/ma_thread.h"

namespace mileaccess{
namespace ma{
namespace compat{

class CompatClientManager
{	
	std::set<std::string> current_applications;
	
	CompatClientManager(CompatClientManager&) throw();
	CompatClientManager &operator=(CompatClientManager&) throw();	
	public:		
		bool addCompatClient(const std::string &software_id, ApplicationInfo &app_info)throw();		
		bool removeCompatClient(const std::string &software_id)throw();			
		CompatClientManager() throw();
		~CompatClientManager() throw();
		bool initialize() throw();
		void deinitialize() throw();		
		void updateApplications();
		void start()throw();
		void stop()throw();
};

}}}

#endif
