#ifndef MA_COMPAT_SERVICE_CONTEXT_H_INCLUDED
#define MA_COMPAT_SERVICE_CONTEXT_H_INCLUDED

#include "ma/internal/compat/ma_compat_common.hpp"
#include "ma/ma_msgbus.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/utils/macrypto/ma_crypto.h"
#include "ma/ma_log.h"
#include "ma/ma_client.h"

namespace mileaccess{
namespace ma{
namespace compat{

class ServiceContextNotificationReceiver
{
public:
	virtual bool onStart() = 0;
	virtual bool onStop() = 0;	
};

class MA_COMPAT_API ServiceContext{
    
	protected:
	ServiceContext();	
	virtual ~ServiceContext();
	static ServiceContext  *service_context;
    static ::ma_logger_t *logger;
    public:
        static void setLogger(::ma_logger_t *logger);				
		static ::ma_logger_t *getLogger();
		static ServiceContext *getInstance();				
		static void releaseInstance();
		virtual bool run(ServiceContextNotificationReceiver *ops) = 0;
		virtual bool stop() = 0;
		virtual ::ma_client_t *getClient() = 0;		
		virtual ::ma_msgbus_t *getMsgbus() = 0;
		virtual ::ma_crypto_t *getCrypto() = 0;		
        virtual void *getLoopWorker() = 0;
		virtual void *getEventLoop() = 0;
};

#define GET_MA_CLIENT()				                    ServiceContext::getInstance()->getClient()
#define GET_COMPAT_LOGGER()			                    ServiceContext::getLogger()
#define GET_COMPAT_MSGBUS()			                    ServiceContext::getInstance()->getMsgbus()
#define GET_COMPAT_CRYPTO()			                    ServiceContext::getInstance()->getCrypto()
#define GET_COMPAT_LOOP_WORKER()	                    (ma_loop_worker_t*)ServiceContext::getInstance()->getLoopWorker()
#define GET_COMPAT_EVENT_LOOP()		                    (ma_event_loop_t*)ServiceContext::getInstance()->getEventLoop()

};};};

#endif
