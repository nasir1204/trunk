#ifndef MA_COMPAT_CLIENT_TABLE_H_INCLUDED
#define MA_COMPAT_CLIENT_TABLE_H_INCLUDED

#include "ma/internal/compat/manageability/ma_compat_client.hpp"

namespace mileaccess{
namespace ma{
namespace compat{

class MA_COMPAT_API RefCountedCompatClient
{	
	ma_atomic_counter_t ref_count; 
	ma_mutex_t data_mutex;
	CompatClient *client;

	RefCountedCompatClient(CompatClient*);						
	RefCountedCompatClient(const RefCountedCompatClient&);
	RefCountedCompatClient& operator=(const RefCountedCompatClient&);
	~RefCountedCompatClient();
	void ref();		
	public:		
		void unref();			
		CompatClient *get();			
	friend class CompatClientTable;		
};

class MA_COMPAT_API CompatClientTable
{	
	std::map<std::string, RefCountedCompatClient*> compat_clients;
	ma_mutex_t data_mutex;
	CompatClientTable();
	~CompatClientTable();
	static CompatClientTable *compatClientTable;
	public:		
		static CompatClientTable *getInstance();
		static void releaseInstance();
		bool add(const std::string &software_id, CompatClient *client);
		bool isRegistered(const std::string &software_id);
		bool remove(const std::string &software_id);		
		void startAll();
		void stopAll();
		void cleanup();
		RefCountedCompatClient *get(const std::string &software_id);		
};

}}}
#endif
