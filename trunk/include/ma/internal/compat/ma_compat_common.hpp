#ifndef MA_COMPAT_COMMON_H_INCLUDED
#define MA_COMPAT_COMMON_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_errors.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/compat/ma_compat_strdef.h"

#ifdef _MSC_VER
    #ifdef MALIB_STATIC
        #define MA_COMPAT_API
     #else
        #ifdef MALIB_EXPORTS
         /* The malib project sets this when the malib DLL is built. This causes
            the malib.dll file to export its functions. */
            #define MA_COMPAT_API __declspec (dllexport)
      #else
         /* This is the normal default for applications that use malib. This
            makes applications import the malib functions from the malib DLL. */
            #define MA_COMPAT_API __declspec (dllimport)            
        #endif
    #endif
#else
    #define MA_COMPAT_API    
#endif

#ifdef MA_WINDOWS
#define MA_PATH_SEPARATOR '\\'
#else
#include<string>
using namespace std;

#define MA_PATH_SEPARATOR '/'
typedef long long __int64;

#define CONST		const
#define WINAPI 		__stdcall
#define __stdcall
typedef  char CHAR;
typedef  CONST CHAR *LPCSTR, *PCSTR;
typedef  wchar_t WCHAR;
typedef  CONST WCHAR *LPCWSTR, *PCWSTR;
typedef LPCWSTR PCTSTR, LPCTSTR;
typedef unsigned long	DWORD, *PDWORD, *LPDWORD;
typedef int			BOOL;
typedef unsigned char	BYTE, *PBYTE, *LPBYTE;
typedef long HRESULT;
#define SUCCEEDED(hr) (((HRESULT)(hr)) >= 0)
#define S_OK                                   ((HRESULT)0L)
#define S_FALSE                                ((HRESULT)1L)
#ifndef TRUE
#define TRUE		1
#endif
#ifndef FALSE
#define FALSE		0
#endif

#endif

#define CAPI __cdecl

#endif