#ifndef MA_ACCESS_EXCLUSION_HPP_INCLUDED
#define MA_ACCESS_EXCLUSION_HPP_INCLUDED

#include <memory>
#include <string>

#ifdef _WIN32
#include <windows.h>
#include <accesslib.h>

#pragma comment(lib, "accesslib")
#pragma comment(lib, "rpcrt4")
#pragma comment(lib, "Shlwapi") // uses wnsprintf


namespace mileaccess { namespace ma {

struct access_handle /*:: boost::noncopyable */ {

    access_handle(ACCESSLIB_HANDLE h) : h(h) {

    }

    access_handle() : h(NULL) {

    }

    void release_handle() {
        if (h) {
            ::CloseAccessHandle(h);
            h = NULL;
        }
    }

    bool check_init() {
        if (!h) {
            h = ::OpenAccessHandle(ACCESS_VSCORE_14_2|ACCESS_AAC);
        }
        return !!h;
    }
    
    bool acquire_access(AacObjectType type, wchar_t const *path, AacBitmaskType access = AAC_ACCESS_CREATE | AAC_ACCESS_WRITE | AAC_ACCESS_DELETE) {
        return check_init() ? ::GetAccess(h, type, path, access) : false;
    }

    void release_access(AacObjectType at, wchar_t const *path) {
        if (h) ::DoneAccess(h, at, path);
    }
    
    ~access_handle() {
        release_handle();
    }
    
    ACCESSLIB_HANDLE h;

private:
    // prevent copying
    access_handle(access_handle const &);
    access_handle &operator=(access_handle const &);
};

struct access_exclusion {
    access_exclusion(access_handle &ah, ::AacObjectType type, wchar_t const *path, ::AacBitmaskType access = AAC_ACCESS_CREATE | AAC_ACCESS_WRITE | AAC_ACCESS_DELETE) : handle(ah), type(type), path(path)  {
        handle.acquire_access(type, path, access);
    }

    ~access_exclusion() {
        handle.release_access(type, path.c_str());
    }

    access_handle &handle;

    ::AacObjectType type;

    std::basic_string<wchar_t> path;

private:
    // prevent copying
    access_exclusion(access_exclusion const &);
    access_exclusion &operator=(access_exclusion const &);
};

}}

#endif





#endif // MA_ACCESS_EXCLUSION_HPP_INCLUDED
