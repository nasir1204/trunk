#ifndef MA_RELAY_DISCOVERY_REQUEST_H_INCLUDED
#define MA_RELAY_DISCOVERY_REQUEST_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_log.h"

#include "ma/internal/clients/udp/ma_udp_client.h"
#include "ma/proxy/ma_proxy_config.h"

MA_CPP(extern "C" {)

typedef void (*relay_discovery_final_callback_t)(void *user_data);
typedef struct ma_relay_discovery_request_s ma_relay_discovery_request_t, *ma_relay_discovery_request_h;
typedef struct ma_relay_discovery_policy_s ma_relay_discovery_policy_t;

struct ma_relay_discovery_policy_s{
	char			*discovery_multicast_addr;

	ma_uint32_t		discovery_port;		

	ma_uint16_t		max_no_of_relays_to_discover;

	ma_uint16_t		discovery_timeout_ms;

	ma_bool_t		sync_discovery;
};

MA_UDP_API ma_error_t ma_relay_discovery_request_create(ma_udp_client_t *udp_client, ma_relay_discovery_request_t **request);

MA_UDP_API ma_error_t ma_relay_discovery_request_set_policy(ma_relay_discovery_request_t *self, ma_relay_discovery_policy_t *policy);

MA_UDP_API ma_error_t ma_relay_discovery_request_set_final_cb(ma_relay_discovery_request_t *self, relay_discovery_final_callback_t cb, void *user_data);

MA_UDP_API ma_error_t ma_relay_discovery_request_send(ma_relay_discovery_request_t *self);

MA_UDP_API ma_error_t ma_relay_discovery_request_cancel(ma_relay_discovery_request_t *self);

MA_UDP_API ma_error_t ma_relay_discovery_request_reset(ma_relay_discovery_request_t *self);

MA_UDP_API ma_error_t ma_relay_discovery_request_get_relays(ma_relay_discovery_request_t *self, const ma_proxy_list_t **relay_list);

MA_UDP_API ma_error_t ma_relay_discovery_request_release(ma_relay_discovery_request_t *self);

MA_CPP(})
#endif

