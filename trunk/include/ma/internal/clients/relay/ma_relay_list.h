#ifndef MA_RELAY_LIST_H_INCLUDED
#define MA_RELAY_LIST_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_errors.h"

MA_CPP(extern "C" {)

typedef struct ma_relay_s  ma_relay_t;
typedef struct ma_relay_list_s	ma_relay_list_t ,*ma_relay_list_h;

struct ma_relay_s{		
	ma_uint32_t		relay_port;

	ma_uint32_t		nic_scope_id;

	char			relay_addr[65];
};

MA_UDP_API ma_error_t ma_relay_list_create(ma_uint16_t capacity, ma_relay_list_t **relay_list);

MA_UDP_API ma_error_t ma_relay_list_add_relay(ma_relay_list_t *self, ma_relay_t *relay);

MA_UDP_API ma_error_t ma_relay_list_add_ref(ma_relay_list_t *self);

MA_UDP_API ma_error_t ma_relay_list_release(ma_relay_list_t *self);

MA_UDP_API ma_error_t ma_relay_list_get_count(ma_relay_list_t *self, ma_uint16_t *count);

MA_UDP_API ma_error_t ma_relay_list_get_relay(ma_relay_list_t *self, ma_uint16_t index, ma_relay_t *relay);


MA_CPP(})

#endif