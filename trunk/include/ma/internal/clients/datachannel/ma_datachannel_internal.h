#ifndef MA_DATACHANNEL_INTERNAL_H_INCLUDED
#define MA_DATACHANNEL_INTERNAL_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/defs/ma_datachannel_defs.h"
#include "ma/datachannel/ma_datachannel_item.h"

MA_CPP(extern "C" {)

/*Internal*/

#define MA_DATACHANNEL_MAX_ASYNC_SEND_TIMEOUT		(180 * 1000)
ma_error_t ma_datachannel_item_prepare_message(ma_datachannel_item_t *dc_item, const char *product_id, ma_message_t **message);

struct ma_datachannel_item_s {
	char origin[MA_DATACHANNEL_MAX_ID_LEN];
	char item_id[MA_DATACHANNEL_MAX_ID_LEN];
	ma_int32_t flags;
	ma_int64_t ttl;
	ma_int64_t time_created; /*For back compat purposes*/
	ma_int64_t correlation_id;
	ma_variant_t *payload;
};

MA_CPP(})
#endif

