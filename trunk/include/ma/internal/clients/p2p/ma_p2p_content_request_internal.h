#ifndef MA_P2P_CONTENT_REQUEST_INTERNAL_H_INCLUDED
#define MA_P2P_CONTENT_REQUEST_INTERNAL_H_INCLUDED

#include "ma/p2p/ma_p2p_content_request.h"
#include "ma/ma_variant.h"
#include "ma/internal/utils/threading/ma_atomic.h"

MA_CPP(extern "C" {)

struct ma_p2p_content_request_s {
    ma_p2p_file_op_t    file_op ;
    ma_table_t          *content_table ;
	ma_bool_t			format ;	//0 - file, 1 - buffer 
	char				*contributor ;
    ma_atomic_counter_t ref_count ;
} ;

ma_error_t ma_p2p_content_request_as_variant(ma_p2p_content_request_t *request, ma_variant_t **var_request) ;

ma_error_t ma_p2p_content_request_from_variant(ma_variant_t *var_request, ma_p2p_content_request_t **request) ;

ma_error_t ma_p2p_content_request_add_ref(ma_p2p_content_request_t *self) ;

ma_error_t ma_p2p_content_request_dec_ref(ma_p2p_content_request_t *self) ;

MA_CPP(})

#endif /* MA_P2P_CONTENT_REQUEST_INTERNAL_H_INCLUDED */