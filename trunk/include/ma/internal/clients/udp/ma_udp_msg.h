#ifndef MA_UDP_MSG_H_INCLUDED
#define MA_UDP_MSG_H_INCLUDED

#include "ma/ma_common.h"

#include "ma/ma_message.h"
#include "ma/ma_variant.h"

#include "ma/internal/utils/datastructures/ma_bytebuffer.h"

MA_CPP(extern "C" {)

typedef struct ma_udp_msg_s ma_udp_msg_t, *ma_udp_msg_h;
typedef enum ma_udp_msg_type_e {	
	MA_UDP_MESSAGE_TYPE_MSGBUS	= 0,
	MA_UDP_MESSAGE_TYPE_RAW		    		
}ma_udp_msg_type_t;

MA_UDP_API ma_error_t ma_udp_msg_create(ma_udp_msg_t **msg);

MA_UDP_API ma_error_t ma_udp_msg_add_ref(ma_udp_msg_t *msg);

MA_UDP_API ma_error_t ma_udp_msg_release(ma_udp_msg_t *msg);

MA_UDP_API ma_error_t ma_udp_msg_get_type(ma_udp_msg_t *msg, ma_udp_msg_type_t *type);

MA_UDP_API ma_error_t ma_udp_msg_set_raw_message(ma_udp_msg_t *msg, ma_buffer_t *message);

MA_UDP_API ma_error_t ma_udp_msg_get_raw_message(ma_udp_msg_t *msg, ma_buffer_t **message);

MA_UDP_API ma_error_t ma_udp_msg_set_ma_message(ma_udp_msg_t *msg, ma_message_t *message);

MA_UDP_API ma_error_t ma_udp_msg_get_ma_message(ma_udp_msg_t *msg, ma_message_t **message);

MA_UDP_API ma_error_t ma_udp_msg_deserialize(const unsigned char *buffer, size_t size, ma_udp_msg_t **msg);

MA_UDP_API ma_error_t ma_udp_msg_serialize(ma_udp_msg_t *msg, ma_bytebuffer_t **buffer);

MA_CPP(})

#endif

