#ifndef MA_UDP_CLIENT_H_INCLUDED
#define MA_UDP_CLIENT_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/utils/event_loop/ma_loop_worker.h"
#include "ma/ma_log.h"

MA_CPP(extern "C" {)

typedef struct ma_udp_client_s ma_udp_client_t, *ma_udp_client_h;

MA_UDP_API ma_error_t ma_udp_client_create(ma_udp_client_t **udp_client);

MA_UDP_API ma_error_t ma_udp_client_set_logger(ma_udp_client_t *self, ma_logger_t *logger);

MA_UDP_API ma_error_t ma_udp_client_get_logger(ma_udp_client_t *self, ma_logger_t **logger);

MA_UDP_API ma_error_t ma_udp_client_set_event_loop(ma_udp_client_t *self, ma_event_loop_t *loop);

MA_UDP_API ma_error_t ma_udp_client_get_event_loop(ma_udp_client_t *self, ma_event_loop_t **loop);

MA_UDP_API ma_error_t ma_udp_client_set_loop_worker(ma_udp_client_t *self, ma_loop_worker_t *loop_worker);

MA_UDP_API ma_error_t ma_udp_client_release(ma_udp_client_t *self);

MA_CPP(})

#endif /*MA_UDP_CLIENT_H_INCLUDED */

