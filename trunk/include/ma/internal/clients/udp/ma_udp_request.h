#ifndef MA_UDP_REQUEST_H_INCLUDED
#define MA_UDP_REQUEST_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_log.h"
#include "ma/internal/clients/udp/ma_udp_msg.h"
#include "ma/internal/clients/udp/ma_udp_client.h"

#define MA_UDP_AUTO_SCOPE_ID	-1

MA_CPP(extern "C" {)

struct ma_udp_response_s{
	ma_udp_msg_t	*response;

	char const		*peer_addr;

	size_t			 receiving_local_nic;
};

typedef struct ma_udp_request_s ma_udp_request_t, *ma_udp_request_h;
typedef struct ma_udp_response_s ma_udp_response_t, *ma_udp_response_h;

typedef ma_error_t (*udp_request_send_cb_t)(ma_udp_request_t *request, void *user_data, ma_bool_t *stop);
typedef ma_error_t (*udp_response_cb_t)(ma_udp_request_t *request, ma_udp_response_t *response, void *user_data, ma_bool_t *stop);
typedef void (*udp_request_finalize_cb_t)(ma_error_t rc, ma_udp_request_t *request, void *user_data);

MA_UDP_API ma_error_t ma_udp_request_create(ma_udp_client_t *client, ma_udp_request_t **request);

MA_UDP_API ma_error_t ma_udp_request_set_port(ma_udp_request_t *self, ma_uint16_t udp_port);

MA_UDP_API ma_error_t ma_udp_request_set_multicast_addr(ma_udp_request_t *self, char const *multicast);

MA_UDP_API ma_error_t ma_udp_request_set_timeout(ma_udp_request_t *self, ma_uint64_t timeout_ms);

MA_UDP_API ma_error_t ma_udp_request_set_callbacks(ma_udp_request_t *self, udp_request_send_cb_t send_cb, udp_response_cb_t cb, void *user_data);

MA_UDP_API ma_error_t ma_udp_request_set_message(ma_udp_request_t *self, ma_udp_msg_t *msg[], ma_uint16_t no_of_message);

MA_UDP_API ma_error_t ma_udp_request_async_send(ma_udp_request_t *self, udp_request_finalize_cb_t final_cb);

MA_UDP_API ma_error_t ma_udp_request_get_client(ma_udp_request_t *self,  ma_udp_client_t **udp_client);

MA_UDP_API ma_error_t ma_udp_request_send(ma_udp_request_t *self);

MA_UDP_API ma_error_t ma_udp_request_release(ma_udp_request_t *self);


MA_CPP(})

#endif /*MA_UDP_REQUEST_H_INCLUDED */

