#ifndef MA_UPDATE_INFO_SYNC_H_INCLUDED
#define MA_UPDATE_INFO_SYNC_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_variant.h"
#include "ma/updater/ma_updater_client_defs.h"

MA_CPP(extern "C" {)

typedef struct ma_update_info_sync_s ma_update_info_sync_t, *ma_update_info_sync_h;

typedef enum ma_update_info_sync_type_e {
	MA_UPDATE_INFO_SYNC_TYPE_CHECK_EXISTANCE = 0, /*Internal purpose*/
    MA_UPDATE_INFO_SYNC_TYPE_NOTIFY			    ,
    MA_UPDATE_INFO_SYNC_TYPE_GET				,
    MA_UPDATE_INFO_SYNC_TYPE_SET				,	
	MA_UPDATE_INFO_SYNC_TYPE_RESPONSE			
}ma_update_info_sync_type_t;


ma_error_t ma_update_info_sync_create(ma_update_info_sync_t **sync_request);

ma_error_t ma_update_info_sync_release(ma_update_info_sync_t *self);

ma_error_t ma_update_info_sync_get_type(ma_update_info_sync_t *self, ma_update_info_sync_type_t *type);

ma_error_t ma_update_info_sync_set_type(ma_update_info_sync_t *self, ma_update_info_sync_type_t type);

ma_error_t ma_update_info_sync_get_update_state(ma_update_info_sync_t *self, ma_update_state_t *state);

ma_error_t ma_update_info_sync_set_update_state(ma_update_info_sync_t *self, ma_update_state_t state);

ma_error_t ma_update_info_sync_set_software_id(ma_update_info_sync_t *self, const char *software_id);

ma_error_t ma_update_info_sync_get_software_id(ma_update_info_sync_t *self, const char **software_id);

ma_error_t ma_update_info_sync_set_info_key(ma_update_info_sync_t *self, const char *key);

ma_error_t ma_update_info_sync_get_info_key(ma_update_info_sync_t *self, const char **key);

ma_error_t ma_update_info_sync_set_info_value(ma_update_info_sync_t *self, ma_variant_t *value);

ma_error_t ma_update_info_sync_get_info_value(ma_update_info_sync_t *self, ma_variant_t **value);

ma_error_t ma_update_info_sync_set_product_response_code(ma_update_info_sync_t *self, ma_updater_product_return_code_t return_code);

ma_error_t ma_update_info_sync_get_product_response_code(ma_update_info_sync_t *self, ma_updater_product_return_code_t *return_code);

ma_error_t ma_update_info_sync_set_update_type(ma_update_info_sync_t *self, const char *update_type);

ma_error_t ma_update_info_sync_get_update_type(ma_update_info_sync_t *self, const char **update_type);

ma_error_t ma_update_info_sync_set_message(ma_update_info_sync_t *self, const char *message);

ma_error_t ma_update_info_sync_get_message(ma_update_info_sync_t *self, const char **message);

ma_error_t ma_update_info_sync_set_extra_info(ma_update_info_sync_t *self, const char *extra_info);

ma_error_t ma_update_info_sync_get_extra_info(ma_update_info_sync_t *self, const char **extra_info);


ma_error_t ma_update_info_sync_from_variant(ma_variant_t *variant, ma_update_info_sync_t **self);

ma_error_t ma_update_info_sync_to_variant(ma_update_info_sync_t *sync, ma_variant_t **variant);


MA_CPP(})

#endif  /* MA_UPDATE_INFO_SYNC_H_INCLUDED */

