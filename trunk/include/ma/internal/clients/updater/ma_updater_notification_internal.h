#ifndef MA_UPDATER_NOTIFICATION_INTERNAL_H_INCLUDED
#define MA_UPDATER_NOTIFICATION_INTERNAL_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/updater/ma_updater_notification.h"

MA_CPP(extern "C" {)
/* Helper functions */
MA_UPDATER_API ma_error_t ma_updater_notification_to_variant(ma_updater_notification_t *notification, ma_variant_t **variant);

MA_UPDATER_API ma_error_t ma_updater_notification_from_variant(ma_variant_t *variant, ma_updater_notification_t **notification);

MA_CPP(})

#endif  /* MA_UPDATER_NOTIFICATION_INTERNAL_H_INCLUDED */
