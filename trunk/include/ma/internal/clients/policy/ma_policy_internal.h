#ifndef MA_POLICY_INTERNAL_H_INCLUDED
#define MA_POLICY_INTERNAL_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/policy/ma_policy_uri.h"
#include "ma/policy/ma_policy_bag.h"
#include "ma/ma_client.h"

MA_CPP( extern "C" { )

MA_POLICY_API ma_error_t ma_policy_get_internal(ma_client_t *ma_client, const ma_policy_uri_t *uri, ma_policy_bag_t **policy_bag);

MA_CPP(})

#endif /* MA_POLICY_BAG_INTERNAL_H_INCLUDED */
