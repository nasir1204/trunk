#ifndef MA_POLICY_URI_INTERNAL_H_INCLUDED
#define MA_POLICY_URI_INTERNAL_H_INCLUDED

#include "ma/policy/ma_policy_uri.h"

MA_CPP( extern "C" { )

MA_POLICY_API ma_error_t ma_policy_uri_create_from_table(ma_table_t *prop_table, ma_policy_uri_t **uri);

MA_POLICY_API ma_error_t ma_policy_uri_get_table(const ma_policy_uri_t *uri, ma_table_t **prop_table);

MA_CPP(})

#endif  /* MA_POLICY_URI_INTERNAL_H_INCLUDED  */
