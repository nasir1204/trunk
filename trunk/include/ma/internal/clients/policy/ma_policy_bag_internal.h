#ifndef MA_POLICY_BAG_INTERNAL_H_INCLUDED
#define MA_POLICY_BAG_INTERNAL_H_INCLUDED

#include "ma/policy/ma_policy_bag.h"

MA_CPP( extern "C" { )

struct ma_client_s;

MA_POLICY_API ma_error_t  ma_policy_bag_create_from_variant(ma_variant_t *data, ma_policy_bag_t **bag);

MA_POLICY_API ma_error_t  ma_policy_bag_get_variant(ma_policy_bag_t *bag, ma_variant_t **data);

MA_POLICY_API size_t ma_policy_bag_size(ma_policy_bag_t *bag);

/**
 *  It returns the first available section name, key and value set if uri value is NULL and bag contains duplicate section name & key name.
 *  If key name is NULL, settings table to be returned for sepcified section name.
 *  
 */
MA_POLICY_API ma_error_t ma_policy_bag_get_value_internal(const ma_policy_bag_t *bag, const ma_policy_uri_t *uri, const char *section_name, const char *key_name, ma_variant_t **value);

MA_POLICY_API ma_error_t ma_policy_bag_set_client(ma_policy_bag_t *bag, struct ma_client_s *ma_client);

MA_POLICY_API ma_error_t ma_policy_bag_set_product_id(ma_policy_bag_t *bag, const char *product_id);


MA_CPP(})

#endif /* MA_POLICY_BAG_INTERNAL_H_INCLUDED */
