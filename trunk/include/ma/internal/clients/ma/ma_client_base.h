/** @file ma_client_base.h
 *  @brief ma client base declarations
 */

#ifndef MA_CLIENT_BASE_H_INCLUDED
#define MA_CLIENT_BASE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_message.h"

MA_CPP(extern "C" {)

typedef struct ma_client_base_s ma_client_base_t, *ma_client_base_h;
typedef struct ma_client_base_methods_s ma_client_base_methods_t, *ma_client_base_methods_h;

struct ma_client_base_methods_s {
    ma_error_t (*on_message)(ma_client_base_t *client_base, const char *topic, ma_message_t *message);
    ma_error_t (*release)(ma_client_base_t *client_base);
};

struct ma_client_base_s {
    const ma_client_base_methods_t  *methods;
	void                            *data;
};

static MA_INLINE void ma_client_base_init(ma_client_base_t *client_base, const ma_client_base_methods_t *methods, void *data) {
    if(client_base) {
        client_base->methods = methods;
        client_base->data = data;
    }
}

static MA_INLINE ma_error_t ma_client_base_on_message(ma_client_base_t *client_base, const char *topic, ma_message_t *message) {
    if(client_base && client_base->methods && client_base->methods->on_message) {
        return client_base->methods->on_message(client_base, topic, message);
    }
    return MA_ERROR_PRECONDITION;
}

static MA_INLINE ma_error_t ma_client_base_release(ma_client_base_t *client_base) {
    if(client_base && client_base->methods && client_base->methods->release) {
        return client_base->methods->release(client_base);
    }
    return MA_ERROR_PRECONDITION;
}

MA_CPP(})

#endif /* MA_CLIENT_BASE_H_INCLUDED */
