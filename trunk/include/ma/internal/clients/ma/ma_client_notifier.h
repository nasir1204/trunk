#ifndef MA_CLIENT_NOTIFIER_H_INCLUDED
#define MA_CLIENT_NOTIFIER_H_INCLUDED


#include "ma/internal/clients/ma/ma_client_internal.h"

#define MA_CLIENT_CONNECTION_CHECK_TIMEOUT			(15*1000)
#define MA_CLIENT_CONNECTION_VERIFY_TIMEOUT			(30*1000)

#define MA_CLIENT_NOTIFIER_STOP_TIMEOUT				(15*1000)
#define MA_MSGBUS_VERSION_LENGTH					(20)

MA_CPP(extern "C" {)

typedef struct ma_client_notifier_s	ma_client_notifier_t ;

ma_error_t ma_client_notifier_create(struct ma_client_s *self, ma_client_notifier_t **notifier);

ma_error_t ma_client_notifier_release(ma_client_notifier_t *self);

ma_error_t ma_client_notifier_start(ma_client_notifier_t *self);

ma_error_t ma_client_notifier_stop(ma_client_notifier_t *self);


MA_CPP(})

#endif /* MA_CLIENT_NOTIFIER_H_INCLUDED */
