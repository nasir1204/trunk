#ifndef MA_CLIENT_EVENT_H_INCLUDED
#define MA_CLIENT_EVENT_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/event/ma_event_client.h"

MA_CPP(extern "C" {)

	/*<EventID>2402</EventID>
	<Severity>4</Severity>
	<GMTTime>2014-04-10T05:34:42</GMTTime>
	<ProductID>EPOAGENT3000</ProductID>
	<Locale>0409</Locale>
	<Type>N/A</Type>
	<Error>-1</Error>
	<Version>N/A</Version>	
	<InitiatorID>VIRUSCAN8800</InitiatorID>
	InitiatorType>OnDemand</InitiatorType>
	<SiteName/>*/

struct ma_event_parameter_s{
	ma_uint32_t eventid;
	ma_event_severity_t severity;
	char *source_productid;
	char locale[10];
	char *type;
	char error[10];
	char *new_version;	
	char *initiator_id;
	char *initiator_type;
	char site_name[1024];
};


ma_error_t ma_client_event_generate(ma_client_t *client, struct ma_event_parameter_s *param);

ma_error_t ma_event_parameter_init(struct ma_event_parameter_s *param);

MA_CPP(})

#endif
