#ifndef MA_CLIENT_INTERNAL_H_INCLUDED
#define MA_CLIENT_INTERNAL_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_client.h"
#include "ma/internal/utils/context/ma_context.h"
#include "ma/ma_dispatcher.h"
#include "ma/internal/clients/ma/client/ma_client_handler.h"

#include <stdio.h>
#include <stdarg.h>

#define MA_CLIENT_LOG(ma_client, severity, fmt, ...) ma_client_log_v(ma_client, (severity), (LOG_FACILITY_NAME), (fmt), ## __VA_ARGS__)
#define LOG_MAX_BUFFER_SIZE							(1024*2)

MA_CPP(extern "C" {)


struct ma_client_notifier_s;

struct ma_client_s {    
    ma_dispatcher_t             *dispatcher;

    ma_client_handler_t         *client_handler;

	ma_msgbus_t					*msgbus;

	ma_logger_t					*logger;

	ma_msgbus_callback_thread_options_t	thread_option ;

	char						*product_id ;

	ma_client_notification_cb_t	client_cb;

	int							client_state;

	int							client_cb_state;

	/*user client notification callback.*/
	ma_client_notification_cb_t	notify_cb;
	
	void						*notify_cb_data ;

	/*user log message callback.*/
	ma_client_logmessage_cb_t	log_cb;

	void						*log_user_data;

	struct ma_client_notifier_s	*notifier ;
	
	ma_msgbus_passphrase_provider_callback_t passphrase_cb;

	void									 *passphrase_cb_data;
};

static MA_INLINE ma_error_t ma_client_get_context(ma_client_t *self, const ma_context_t **context) {
    return ma_client_handler_get_context(self->client_handler, context);
}

static MA_INLINE ma_error_t ma_client_set_consumer_reachability(ma_client_t *self, ma_msgbus_consumer_reachability_t consumer_reach) {
    return ma_client_handler_set_consumer_reachability(self->client_handler, consumer_reach);
}

static MA_INLINE ma_error_t ma_client_set_logger(ma_client_t *self, ma_logger_t *logger) {
	self->logger = logger;	
	return MA_OK;
}

static MA_INLINE ma_error_t ma_client_get_consumer_reachability(ma_client_t *self, ma_msgbus_consumer_reachability_t *consumer_reach) {
    return ma_client_handler_get_consumer_reachability(self->client_handler, consumer_reach);
}

static MA_INLINE void ma_client_log(ma_client_t *self, ma_log_severity_t severity, const char *module_name, const char *log_message){
	if(self->log_cb)
		self->log_cb(severity, module_name, log_message, self->log_user_data);			
	else if(self->logger)
		ma_log(self->logger, severity, module_name, "", 0, "", log_message);	
}



static MA_INLINE void ma_client_log_v(ma_client_t *self, ma_log_severity_t severity, const char *module_name, const char *fmt, ...){
	char fmt_buf[LOG_MAX_BUFFER_SIZE] = {0};
	va_list arg;
    va_start(arg, fmt);
	vsnprintf(fmt_buf, LOG_MAX_BUFFER_SIZE, fmt, arg);
	va_end(arg);

    ma_client_log(self, severity, module_name, fmt_buf);        
}

MA_CPP(})

#endif /* MA_CLIENT_INTERNAL_H_INCLUDED */




