#ifndef MA_CLIENT_HANDLER_H_INCLUDED
#define MA_CLIENT_HANDLER_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_log.h"
#include "ma/internal/defs/ma_object_defs.h"

MA_CPP(extern "C" {)

typedef struct ma_client_handler_s ma_client_handler_t, *ma_client_handler_h;

typedef void (*ma_client_log_cb_t)(ma_log_severity_t severity, const char *module_name, const char *log_message, void *user_data);

MA_CLIENT_API ma_error_t ma_client_handler_create(const char *product_id,  ma_client_handler_t **handler);

MA_CLIENT_API ma_error_t ma_client_handler_start(ma_client_handler_t *handler);

MA_CLIENT_API ma_error_t ma_client_handler_stop(ma_client_handler_t *handler);

MA_CLIENT_API ma_error_t ma_client_handler_release(ma_client_handler_t *handler);

MA_CLIENT_API ma_error_t ma_client_handler_get_context(ma_client_handler_t *self, const ma_context_t **context);

MA_CLIENT_API ma_error_t ma_client_handler_set_logger(ma_client_handler_t *handler, ma_logger_t *logger);

MA_CLIENT_API ma_error_t ma_client_handler_set_logger_callback(ma_client_handler_t *handler, ma_client_log_cb_t log_cb, void *user_data);

MA_CLIENT_API ma_error_t ma_client_handler_set_msgbus_passphrase_callback(ma_client_handler_t *handler, ma_msgbus_passphrase_provider_callback_t cb, void *cb_data);

MA_CLIENT_API ma_error_t ma_client_handler_set_msgbus(ma_client_handler_t *handler, ma_msgbus_t *msgbus);

MA_CLIENT_API ma_error_t ma_client_handler_get_msgbus(ma_client_handler_t *handler, ma_msgbus_t **msgbus);

MA_CLIENT_API ma_error_t ma_client_handler_get_thread_option(ma_client_handler_t *handler, ma_msgbus_callback_thread_options_t *thread_option);

MA_CLIENT_API ma_error_t ma_client_handler_set_thread_option(ma_client_handler_t *handler, ma_msgbus_callback_thread_options_t thread_option);

MA_CLIENT_API ma_error_t ma_client_handler_set_consumer_reachability(ma_client_handler_t *self, ma_msgbus_consumer_reachability_t consumer_reach);

MA_CLIENT_API ma_error_t ma_client_handler_get_consumer_reachability(ma_client_handler_t *self, ma_msgbus_consumer_reachability_t *consumer_reach);

MA_CPP(})

#include "ma/dispatcher/ma_client_handler_dispatcher.h"

#endif /* MA_CLIENT_HANDLER_H_INCLUDED */


