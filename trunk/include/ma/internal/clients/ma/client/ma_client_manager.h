#ifndef MA_CLIENT_MANAGER_H_INCLUDED
#define MA_CLIENT_MANAGER_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_log.h"
#include "ma/ma_msgbus.h"
#include "ma/internal/clients/ma/ma_client_base.h"

#define MAX_CLIENT_NAME_LENGTH      256

MA_CPP(extern "C" {)

typedef struct ma_client_manager_s ma_client_manager_t, *ma_client_manager_h;

MA_CLIENT_API ma_error_t ma_client_manager_create(ma_msgbus_t *msgbus, ma_client_manager_t **client_manager);

MA_CLIENT_API ma_error_t ma_client_manager_start(ma_client_manager_t *self);

MA_CLIENT_API ma_error_t ma_client_manager_stop(ma_client_manager_t *self);

MA_CLIENT_API ma_error_t ma_client_manager_release(ma_client_manager_t *client_manager);

MA_CLIENT_API ma_error_t ma_client_manager_set_thread_option(ma_client_manager_t *client_manager, ma_msgbus_callback_thread_options_t thread_option);

MA_CLIENT_API ma_error_t ma_client_manager_set_consumer_reachability(ma_client_manager_t *client_manager, ma_msgbus_consumer_reachability_t consumer_reach);

MA_CLIENT_API ma_error_t ma_client_manager_get_thread_option(ma_client_manager_t *client_manager, ma_msgbus_callback_thread_options_t *thread_option);

MA_CLIENT_API ma_error_t ma_client_manager_get_consumer_reachability(ma_client_manager_t *client_manager, ma_msgbus_consumer_reachability_t *consumer_reach);

MA_CLIENT_API ma_error_t ma_client_manager_set_logger(ma_client_manager_t *client_manager, ma_logger_t *logger);

MA_CLIENT_API ma_error_t ma_client_manager_add_client(ma_client_manager_t *client_manager, const char *client_name, ma_client_base_t *client);

MA_CLIENT_API ma_error_t ma_client_manager_get_client(ma_client_manager_t *client_manager, const char *client_name, ma_client_base_t **client);

MA_CLIENT_API ma_bool_t ma_client_manager_has_client(ma_client_manager_t *client_manager, const char *client_name);

MA_CLIENT_API ma_error_t ma_client_manager_remove_client(ma_client_manager_t *client_manager, ma_client_base_t *client);

MA_CPP(})

#endif /* MA_CLIENT_MANAGER_H_INCLUDED */


