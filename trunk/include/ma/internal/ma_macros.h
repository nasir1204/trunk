#ifndef MA_MACROS_H_INCLUDED
#define MA_MACROS_H_INCLUDED

#include <ma/ma_common.h>

#ifdef MA_WINDOWS
#define MA_MAX_PATH_LEN   		260
#define MA_PATH_SEPARATOR 		'\\'
#define MA_PATH_SEPARATOR_STR 		"\\"
#else
#ifdef __APPLE__
#include <sys/syslimits.h>
#elif defined(__linux__)
#include <linux/limits.h>
#else
#include <limits.h>
#endif
#define MA_MAX_PATH_LEN   		PATH_MAX
#define MA_PATH_SEPARATOR 		'/'
#define MA_PATH_SEPARATOR_STR 		"/"
#endif

#define MA_MAX_LEN          256
#define MA_MAX_BUFFER_LEN   2048
#define MA_MAX_NAME         256
#define MA_MAX_TIME_LEN     32
#define MA_MAX_KEY_LEN      260
#define MA_MAX_HASH_LEN     32
#define MA_MILEACCESS_HOST  "mileaccess.com"
#define MA_MILEACCESS_PINCODE_MAPPER_PORT "8080"
#define MA_HTTP_NON_SECURE_PROTOCOL  "http"
#define MA_HTTP_SECURE_PROTOCOL "https"

#define MA_COUNTOF(x) (sizeof(x)/sizeof((x)[0]))

#ifdef _MSC_VER
#define MA_MSC_CONCAT(x,y) x ## y
#define MA_MSC_SELECT(x,y) (x)
#else
#define MA_MSC_CONCAT(x,y) (y)
#define MA_MSC_SELECT(x,y) (y)
#endif

#ifdef _WIN32
#define MA_WIN_CONCAT(x,y) x ## y
#define MA_WIN_SELECT(x,y) (x)
#else
#define MA_WIN_CONCAT(x,y) (y)
#define MA_WIN_SELECT(x,y) (y)
#endif

#ifdef NDEBUG
# define MA_DEBUG_SELECT(x,y) (y)
#else
# define MA_DEBUG_SELECT(x,y) (x)
#endif


#define MA_ASCII_STRING 0
#define MA_WCHAR_STRING 1
#define MA_RAW          2

#define MA_CHAR_CODE(x) ((x) + '0')
#define MA_CHAR_CODE_LEN 2
#define MA_CHAR_SET_NULL(x, i) (x[i] = '\0')
#define MA_CHAR_NUMERIC_VALUE(x, i) (x[i] - '0')

#define MA_TOKEN_CONSTRUCTOR(t1, t2) t1##t2
#define MA_TOKEN_STR(x) (#x)

#define MA_BYTE_SET_BIT(X,Y) ((X) |= (Y))
#define MA_BYTE_CLEAR_BIT(X,Y) ((X) &= ~(Y))
#define MA_BYTE_TOGGLE_BIT(X,Y) ((X) ^= (Y))
#define MA_BYTE_CHECK_BIT(X,Y) ((X) & (Y))

#endif

