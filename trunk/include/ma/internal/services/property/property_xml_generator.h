#ifndef PROPERTY_XML_GENERATOR_H_INCLUDED
#define PROPERTY_XML_GENERATOR_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_variant.h"
#include "ma/ma_datastore.h"
#include "ma/internal/utils/xml/ma_xml.h"
#include "ma/internal/utils/xml/ma_xml_node.h"
#include "ma/ma_log.h"

#define MA_SYSTEM_PROP_PROVIDER_ID					"SYSPROPS1000"
#define MA_PROPS_ROOT_NODE_NAME						"ns:naiProperties"
#define MA_PROPS_XML_ATTRIBUTE_MACHINE_NAME			"MachineName"
#define MA_PROPS_XML_ATTRIBUTE_MACHINE_ID			"MachineID"
#define MA_PROPS_XML_ATTRIBUTE_TENANT_ID			"TenantId"
#define MA_PROPS_XML_ATTRIBUTE_PROPSVERSION			"PropsVersion"
#define MA_PROPS_XML_ATTRIBUTE_PROPS_TYPE			"FullProps"
#define MA_PROPS_XML_ATTRIBUTE_XML_NAMESPACE		"xmlns:ns"
#define MA_PROPS_XML_ATTRIBUTE_XML_NAMESPACE_VAL	"naiProps"

#define MA_STR_TRUE									"true"
#define MA_STR_FALSE								"false"
#define MA_STR_SYSTEM_PROPERTY						"ComputerProperties"
#define MA_STR_PROD_PROPERTY_SETTING				"Setting"
#define MA_STR_PROD_PROPERTY_SECTION				"Section"
#define MA_STR_PROD_PROPERTY						"ProductProperties"

#define MA_STR_PROD_PROPERTY_ATTR_SOFTWARE_ID		"SoftwareID"
#define MA_STR_PROD_PROPERTY_ATTR_NAME				"name"

#define MA_STR_PROPERTY_ATTR_DELETE					"delete"
MA_CPP(extern "C" {) 
typedef struct ma_property_xml_generator_s ma_property_xml_generator_t, *ma_property_xml_generator_h; 

ma_error_t ma_property_xml_generator_create(
		ma_property_xml_generator_t **xml_generator,
		ma_ds_t *datastore,
		const char *props_ds_root_path,
		const char *machine_name,
		const char *machine_guid, const char *tenantid, const char *prop_version);

ma_error_t ma_property_xml_generator_release(
		ma_property_xml_generator_t *xml_generator);

ma_error_t ma_property_xml_generator_append_prop(
		ma_property_xml_generator_t *xml_generator,
		const char *product_id,
		ma_table_t *product_properties);

ma_error_t ma_property_xml_generator_get_incremental_props( ma_property_xml_generator_t *xml_generator, unsigned char **incr_buffer, size_t *size);

ma_error_t ma_property_xml_generator_get_full_props( ma_property_xml_generator_t *xml_generator, unsigned char **full_buffer, size_t *size);

ma_error_t ma_property_xml_generator_set_product_not_running(ma_property_xml_generator_t *xml_generator, const char *product_id);

ma_bool_t ma_property_xml_generator_is_product_not_running(ma_property_xml_generator_t *xml_generator, const char *product_id);

ma_error_t ma_property_xml_generator_save_props_to_datastore(ma_property_xml_generator_t *xml_generator);

ma_error_t ma_property_xml_generator_get_last_full_props( ma_property_xml_generator_t *xml_generator, unsigned char **full_buffer, size_t *size);


ma_error_t ma_property_xml_generator_set_logger(ma_property_xml_generator_t *xml_generator, ma_logger_t *logger);
MA_CPP(})
#endif //PROPERTY_XML_GENERATOR_H_INCLUDED

