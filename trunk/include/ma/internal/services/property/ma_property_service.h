#ifndef MA_PROPERTY_SERVICE_H_INCLUDED
#define MA_PROPERTY_SERVICE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_message.h"
#include "ma/internal/services/ma_service.h"

/* Property Service - Incoming messages*/
/*
  MESSAGE_TYPE=PROPERTY_COLLECT
  PROPS_TYPE = FULL OR INCR
*/

typedef enum ma_props_type_e {
    PROPS_TYPE_INCR = 0,    /* Incremental props */
    PROPS_TYPE_FULL,         /* FULL props */
    PROPS_TYPE_VERSION
} ma_props_type_t ;

/*
  MESSAGE_TYPE=PROPERTY_COLLECTED
  SESSION_ID=<integer identifier>
  PROVIDER_ID=<string>
  <payload>(Refer documentation for payload)
*/

MA_CPP(extern "C" {)

typedef struct ma_property_service_s ma_property_service_t, *ma_property_service_h;

/**
 *  @brief Create Property service 
 *  
 */
MA_PROPERTY_API ma_error_t ma_property_service_create(const char *service_name, ma_service_t **service) ;

/**
 *  @brief Create Property collect message 
 *  TBD controller was using this , will it be used by task service if so need to move to the utils... 
 *  
 */
MA_PROPERTY_API ma_error_t ma_property_collect_message_create(ma_props_type_t props_type, ma_message_t **msg) ;

MA_CPP(})

#include "ma/internal/services/dispatcher/ma_property_service_dispatcher.h"

#endif // MA_PROPERTY_SERVICE_H_INCLUDED

