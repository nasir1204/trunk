#ifndef MA_BOOKING_SERVICE_H_INCLUDED
#define MA_BOOKING_SERVICE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/services/ma_service.h"
#include "ma/internal/utils/booker/ma_booker.h"

MA_CPP(extern "C" {)
/* booking service class */
typedef struct ma_booking_service_s ma_booking_service_t, *ma_booking_service_h;

// booking service ctor
MA_BOOKING_API ma_error_t ma_booking_service_create(const char *service_name, ma_service_t **service);

MA_BOOKING_API ma_error_t ma_booking_service_get_booker(ma_booking_service_t *booking, ma_booker_t **booker);

MA_BOOKING_API ma_error_t ma_booking_service_get_context(ma_booking_service_t *service, ma_context_t **context);
MA_CPP(})

#include "ma/dispatcher/ma_booking_service_dispatcher.h"
#endif /* MA_BOOKING_SERVICE_H_INCLUDED */

