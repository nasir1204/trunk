#ifndef MA_SERVICES_MANAGER_H_INCLUDED
#define MA_SERVICES_MANAGER_H_INCLUDED

/************************************************************************/
/* maintains a collection of services                                   */
/************************************************************************/

#include "ma/internal/services/ma_service.h"
#include "ma/internal/utils/datastructures/ma_queue.h"
#include "ma/ma_log.h"

typedef struct ma_service_manager_s ma_service_manager_t, *ma_service_manager_h;

struct ma_service_manager_s {
    ma_queue_t  qhead;
    ma_logger_t *logger;
};

typedef enum ma_service_manager_service_option_e {
      /* take the default options: enabled, required */
    MA_SERVICE_OPTION_DEFAULT = 0x0,
      /* service failure constitutes startup failure */
    MA_SERVICE_OPTION_REQUIRED = 0x0,
      
      /* service is enabled */
    MA_SERVICE_OPTION_ENABLED = 0x0,

      /* service is disabled, don't start it */
    MA_SERVICE_OPTION_DISABLED = 0x100,

      /* service failure does not cause complete failure */
    MA_SERVICE_OPTION_OPTIONAL = 0x200,

      /* service is in shared object, load it please */
    MA_SERVICE_OPTION_DYNLOAD = 0x400
} ma_service_manager_service_option_t;

MA_STATIC_INLINE unsigned MA_SERVICE_ENABLE_IF(ma_bool_t flag) {return flag?MA_SERVICE_OPTION_ENABLED:MA_SERVICE_OPTION_DISABLED;}  


typedef struct ma_service_manager_service_entry {
    unsigned                flags;
    const char              *name;
    ma_service_create_fn    creator_fn;

    /* name of dll/dylib/so */
    const char              *module_name;

    /* name of entry point, defaults to 'service_create' */
    const char              *entry_point;
} ma_service_manager_service_entry_t;


void ma_service_manager_init(ma_service_manager_t *manager);

void ma_service_manager_set_logger(ma_service_manager_t *manager, ma_logger_t *logger);

/*!
 * One stop initialization from a list of service entries. The list must be terminated by a 0 entry.
 */
ma_error_t ma_service_manager_create_services(ma_service_manager_t *manager, ma_service_manager_service_entry_t const *entries);

ma_error_t ma_service_manager_add_service(ma_service_manager_t *manager, ma_service_t *service, const char *service_name);

ma_service_t *ma_service_manager_find_service(ma_service_manager_t *manager, char const *service_name);

ma_error_t ma_service_manager_configure_all(ma_service_manager_t *manager, ma_context_t *context, unsigned hint);

ma_error_t ma_service_manager_start_all(ma_service_manager_t *manager);

ma_error_t ma_service_manager_stop_all(ma_service_manager_t *manager);

void ma_service_manager_release_all(ma_service_manager_t *manager);

#endif /* MA_SERVICES_MANAGER_H_INCLUDED */


