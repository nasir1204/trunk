#ifndef MA_SENSOR_SERVICE_H_INCLUDED
#define MA_SENSOR_SERVICE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/services/ma_service.h"

MA_CPP(extern "C" {)
/* sensor service class */
typedef struct ma_sensor_service_s ma_sensor_service_t, *ma_sensor_service_h;

struct ma_sensor_msg_s;

/* sensor service ctor */
MA_SENSOR_API ma_error_t ma_sensor_service_create(const char *service_name, ma_service_t **service);

/*TODO- NEEDS to be streamlined */
MA_SENSOR_API ma_error_t  ma_sensor_service_post_sensor_msg(ma_sensor_service_t *service, struct ma_sensor_msg_s *msg);

MA_CPP(})

#endif /* MA_SENSOR_SERVICE_H_INCLUDED */

