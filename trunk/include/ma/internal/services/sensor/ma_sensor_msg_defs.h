#ifndef MA_SENSOR_MSG_DEFS_H_INCLUDED
#define MA_SENSOR_MSG_DEFS_H_INCLUDED

#include "ma/ma_common.h"

/* user sensor message defs */
#define MA_USER_SENSOR_MSG_ATTR_SESSION_ID              "ma.user.sensor.attr.session.id"
#define MA_USER_SENSOR_MSG_ATTR_USER                    "ma.user.sensor.attr.user"
#define MA_USER_SENSOR_MSG_ATTR_DOMAIN                  "ma.user.sensor.attr.domain"
#define MA_USER_SENSOR_MSG_ATTR_IS_USER_WCHAR           "ma.user.sensor.attr.is.user.wchar"
#define MA_USER_SENSOR_MSG_ATTR_IS_DOMAIN_WCHAR         "ma.user.sensor.attr.is.domain.wchar"
#define MA_USER_SENSOR_MSG_ATTR_EVENT_TYPE              "ma.user.sensor.attr.event.type"
#define MA_USER_SENSOR_MSG_ATTR_LOG_ON_STATE            "ma.user.sensor.attr.log.on.state"
#define MA_USER_SENSOR_MSG_ATTR_SIZE                    "ma.user.sensor.attr.size"
#define MA_USER_SENSOR_MSG_ATTR_INFO_LIST               "ma.user.sensor.attr.info.list"
#define MA_USER_SENSOR_MSG_ATTR_STARTUP_FLAG			"ma.user.sensor.attr.startup.flag"

/* power sensor message defs */
#define MA_POWER_SENSOR_MSG_ATTR_SIZE                   "ma.power.sensor.attr.size"
#define MA_POWER_SENSOR_MSG_ATTR_EVENT_TYPE             "ma.power.sensor.attr.event.type"
#define MA_POWER_SENSOR_MSG_ATTR_SETTING_LIST           "ma.power.sensor.attr.setting.list"
#define MA_POWER_SENSOR_MSG_ATTR_POWER_STATUS           "ma.power.sensor.attr.power.status"
#define MA_POWER_SENSOR_MSG_ATTR_BATTERY_CHARGE_STATUS  "ma.power.sensor.attr.battery.charge.status"
#define MA_POWER_SENSOR_MSG_ATTR_BATTERY_PERCENTAGE     "ma.power.sensor.attr.battery.percentage"
#define MA_POWER_SENSOR_MSG_ATTR_BATTERY_LIFE           "ma.power.sensor.attr.battery.life"

/* network sensor message defs */
#define MA_NETWORK_SENSOR_MSG_ATTR_SIZE                   "ma.network.sensor.attr.size"
#define MA_NETWORK_SENSOR_MSG_ATTR_EVENT_TYPE             "ma.network.sensor.attr.event.type"
#define MA_NETWORK_SENSOR_MSG_ATTR_SETTING_LIST           "ma.network.sensor.attr.setting.list"
#define MA_NETWORK_SENSOR_MSG_ATTR_CONNECTIVITY_STATUS    "ma.network.sensor.attr.connectivity.status"
#define MA_NETWORK_SENSOR_MSG_ATTR_IPADDRESS              "ma.network.sensor.attr.ipaddress"

/* system sensor message defs */
#define MA_SYSTEM_SENSOR_MSG_ATTR_SIZE                      "ma.system.sensor.attr.size"
#define MA_SYSTEM_SENSOR_MSG_ATTR_EVENT_TYPE                "ma.system.sensor.attr.event.type"
#define MA_SYSTEM_SENSOR_MSG_ATTR_SETTING_LIST              "ma.system.sensor.attr.setting.list"

/* idle sensor message defs */
#define MA_IDLE_SENSOR_MSG_ATTR_SIZE                        "ma.idle.sensor.attr.size"
#define MA_IDLE_SENSOR_MSG_ATTR_EVENT_TYPE                  "ma.idle.sensor.attr.event.type"
#define MA_IDLE_SENSOR_MSG_ATTR_SETTING_LIST                "ma.idle.sensor.attr.setting.list"
#define MA_IDLE_SENSOR_MSG_ATTR_IDLE_WAIT                   "ma.idle.sensor.attr.idle.wait"


#endif /* MA_SENSOR_MSG_DEFS_H_INCLUDED */

