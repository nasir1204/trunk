#ifndef MA_SERVICE_UTILS_H_INCLUDED
#define MA_SERVICE_UTILS_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/policy/ma_policy_bag.h"
#include "ma/internal/utils/database/ma_db.h"

MA_CPP(extern "C" {)
  

ma_error_t get_agent_policies(const ma_policy_uri_t *agent_uri, ma_db_t *policy_db, ma_policy_bag_t **policy_bag);

MA_CPP(})

#endif /* MA_SERVICE_UTILS_H_INCLUDED */
