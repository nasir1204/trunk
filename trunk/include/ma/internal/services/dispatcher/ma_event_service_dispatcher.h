#ifdef MA_USE_DISPATCHER
#ifndef MA_EVENT_SERVICE_DISPATCHER_H_INCLUDED
#define MA_EVENT_SERVICE_DISPATCHER_H_INCLUDED

#include "ma/dispatcher/ma_dispatcher_macros.h"

#undef MA_DISPATCHER_PRODUCT_NAME
#undef MA_DISPATCHER_LIBRARY_NAME
#undef MA_DISPATCHER_MODULE
#undef MA_DISPATCHER_LIBRARY_VERSION

#define MA_DISPATCHER_PRODUCT_NAME		 "ma" //TODO. Update with proper name
#define MA_DISPATCHER_LIBRARY_NAME		 MA_DEFINE_DISPATCHER_LIB(event_service)
#define MA_DISPATCHER_MODULE			 "event_service"
#define MA_DISPATCHER_LIBRARY_VERSION	 0x05020100

static unsigned int ma_event_service_lib_version;

/*

typedef struct ma_event_service_s ma_event_service_t, *ma_event_service_h;
MA_EVENT_API ma_error_t ma_event_service_create(ma_msgbus_t *msgbus, ma_ds_t *ds, ma_ah_client_service_t *ah_client, ma_event_service_t **event_service);
MA_EVENT_API ma_error_t ma_event_service_start(ma_event_service_t *self);
MA_EVENT_API ma_error_t ma_event_service_stop(ma_event_service_t *self);
MA_EVENT_API ma_error_t ma_event_service_get_spipe_decorator(ma_event_service_t *service, ma_spipe_decorator_t **decorator);
MA_EVENT_API ma_error_t ma_event_service_get_spipe_handler(ma_event_service_t *service, ma_spipe_handler_t **handler);
MA_EVENT_API ma_error_t ma_event_service_set_logger(ma_event_service_t *self, ma_logger_t *logger);
MA_EVENT_API ma_error_t ma_event_service_set_priority_event_forwarding_policy(ma_event_service_t *self, ma_bool_t is_enabled, const ma_priority_event_forwarding_policy_t *ev_policy);
MA_EVENT_API ma_error_t ma_event_service_release(ma_event_service_t *self);
*/


MA_CPP(extern "C" {)
#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#define MA_DISPATCHER_ACTUAL_ARG_EXPANDER\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_event_service_create,(ma_msgbus_t *msgbus, ma_ds_t *ds, ma_ah_client_service_t *ah_client, ma_event_service_t **event_service), (msgbus, ds,ah_client, event_service))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_event_service_start,(ma_event_service_t *self), (self)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_event_service_stop,(ma_event_service_t *self), (self))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_event_service_get_spipe_decorator,(ma_event_service_t *service, ma_spipe_decorator_t **decorator), (service, decorator)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_event_service_get_spipe_handler,(ma_event_service_t *service, ma_spipe_handler_t **handler), (service, handler)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_event_service_set_logger,(ma_event_service_t *self, ma_logger_t *logger), (self, logger))  \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_event_service_set_priority_event_forwarding_policy,(ma_event_service_t *self, ma_bool_t is_enabled, const ma_priority_event_forwarding_policy_t *ev_policy), (self, is_enabled, ev_policy)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_event_service_release,(ma_event_service_t *self), (self))


char *ma_event_service_function_names[] = {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) #function,
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
};

typedef struct ma_event_service_functions_s {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) ret_type (*function##_fn) signature;
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
}ma_event_service_functions_t;
ma_event_service_functions_t* ma_event_service_functions = NULL;

#define ma_event_service_functions_size (sizeof(ma_event_service_function_names)/sizeof(ma_event_service_function_names[0]))

#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) \
    MA_DISTPATCHER_ARG_STUB_CREATOR(&ma_event_service_lib_version, MA_DISPATCHER_PRODUCT_NAME, MA_DISPATCHER_LIBRARY_NAME, MA_DISPATCHER_LIBRARY_VERSION,MA_DISPATCHER_MODULE,ret_type, function, signature, arg, ma_event_service_function_names, &ma_event_service_functions, ma_event_service_functions_size)
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER

#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER
MA_CPP(})

#endif
#endif //MA_USE_DISPATCHER
