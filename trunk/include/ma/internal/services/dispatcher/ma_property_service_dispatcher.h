#ifdef MA_USE_DISPATCHER
#ifndef MA_PROPERTY_SERVICE_DISPATCHER_H_INCLUDED
#define MA_PROPERTY_SERVICE_DISPATCHER_H_INCLUDED

#include "ma/dispatcher/ma_dispatcher_macros.h"

#undef MA_DISPATCHER_PRODUCT_NAME
#undef MA_DISPATCHER_LIBRARY_NAME
#undef MA_DISPATCHER_MODULE
#undef MA_DISPATCHER_LIBRARY_VERSION

#define MA_DISPATCHER_PRODUCT_NAME		 "ma" //TODO. Update with proper name
#define MA_DISPATCHER_LIBRARY_NAME		 MA_DEFINE_DISPATCHER_LIB(property_service)
#define MA_DISPATCHER_MODULE			 "property_service"
#define MA_DISPATCHER_LIBRARY_VERSION	 0x05020100

static unsigned int ma_property_service_lib_version;

/*
MA_PROPERTY_API ma_error_t ma_property_service_create(ma_msgbus_t *msgbus, ma_ah_client_service_t *ah, ma_logger_t *logger, ma_ds_t *ds, ma_property_service_t **service) ;
MA_PROPERTY_API ma_error_t ma_property_service_start(ma_property_service_t *self) ;
MA_PROPERTY_API ma_error_t ma_property_service_stop(ma_property_service_t *self) ;
MA_PROPERTY_API ma_error_t ma_property_service_release(ma_property_service_t *self) ;
MA_PROPERTY_API ma_spipe_decorator_t * ma_property_get_decorator(ma_property_service_t *pps) ;
MA_PROPERTY_API ma_spipe_handler_t * ma_property_get_handler(ma_property_service_t *pps) ;
MA_PROPERTY_API ma_error_t ma_property_collect_message_create(ma_props_type_t props_type, ma_message_t **msg) ;
*/


MA_CPP(extern "C" {)
#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#define MA_DISPATCHER_ACTUAL_ARG_EXPANDER\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_property_service_create,(ma_msgbus_t *msgbus, ma_ah_client_service_t *ah, ma_logger_t *logger, ma_ds_t *ds, ma_property_service_t **service), (msgbus, ah, logger, ds, service))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_property_service_start,(ma_property_service_t *self), (self)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_property_service_stop,(ma_property_service_t *self), (self))\
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_property_service_release,(ma_property_service_t *self), (self)) \
    MA_DISPATCHER_ARG_EXPANDER(ma_spipe_decorator_t*,ma_property_service_get_spipe_decorator,(ma_property_service_t *service), (service)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_spipe_handler_t*,ma_property_service_get_spipe_handler,(ma_property_service_t *service), (service)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_property_collect_message_create,(ma_props_type_t props_type, ma_message_t **msg), (props_type, msg))



char *ma_property_service_function_names[] = {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) #function,
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
};

typedef struct ma_property_service_functions_s {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) ret_type (*function##_fn) signature;
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
}ma_property_service_functions_t;
ma_property_service_functions_t* ma_property_service_functions = NULL;

#define ma_property_service_functions_size (sizeof(ma_property_service_function_names)/sizeof(ma_property_service_function_names[0]))

#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) \
    MA_DISTPATCHER_ARG_STUB_CREATOR(&ma_property_service_lib_version, MA_DISPATCHER_PRODUCT_NAME, MA_DISPATCHER_LIBRARY_NAME, MA_DISPATCHER_LIBRARY_VERSION,MA_DISPATCHER_MODULE,ret_type, function, signature, arg, ma_property_service_function_names, &ma_property_service_functions, ma_property_service_functions_size)
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER

#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER
MA_CPP(})

#endif
#endif //MA_USE_DISPATCHER
