#ifdef MA_USE_DISPATCHER
#ifndef MA_POLICY_SERVICE_DISPATCHER_H_INCLUDED
#define MA_POLICY_SERVICE_DISPATCHER_H_INCLUDED

#include "ma/dispatcher/ma_dispatcher_macros.h"

#undef MA_DISPATCHER_PRODUCT_NAME
#undef MA_DISPATCHER_LIBRARY_NAME
#undef MA_DISPATCHER_MODULE
#undef MA_DISPATCHER_LIBRARY_VERSION

#define MA_DISPATCHER_PRODUCT_NAME		 "ma" //TODO. Update with proper name
#define MA_DISPATCHER_LIBRARY_NAME		 MA_DEFINE_DISPATCHER_LIB(policy_service)
#define MA_DISPATCHER_MODULE			 "policy_service"
#define MA_DISPATCHER_LIBRARY_VERSION	 0x05020100

static unsigned int ma_policy_service_lib_version;

/*

MA_POLICY_API ma_error_t ma_policy_service_create(ma_msgbus_t *msgbus, ma_db_t *db, ma_net_client_service_t *net_service, ma_ah_client_service_t *ah_client_service, ma_ah_sitelist_t *sitelist, ma_policy_service_t **policy_service);
MA_POLICY_API ma_error_t ma_policy_service_set_logger(ma_policy_service_t *policy_service, ma_logger_t *logger);
MA_POLICY_API ma_error_t ma_policy_service_start(ma_policy_service_t *policy_service);
MA_POLICY_API ma_error_t ma_policy_service_stop(ma_policy_service_t *policy_service);
MA_POLICY_API ma_error_t ma_policy_service_release(ma_policy_service_t *policy_service);
MA_POLICY_API ma_spipe_decorator_t *ma_policy_service_get_decorator(ma_policy_service_t *policy_service);
MA_POLICY_API ma_spipe_handler_t *ma_policy_service_get_handler(ma_policy_service_t *policy_service);
*/


MA_CPP(extern "C" {)
#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#define MA_DISPATCHER_ACTUAL_ARG_EXPANDER\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_service_create,(ma_msgbus_t *msgbus, ma_db_t *db, ma_net_client_service_t *net_service, ma_ah_client_service_t *ah_client_service, ma_ah_sitelist_t *sitelist, ma_policy_service_t **policy_service), (msgbus, db, net_service, ah_client_service, sitelist, policy_service))\
    MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_service_start,(ma_policy_service_t *self), (self)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_service_stop,(ma_policy_service_t *self), (self))\
    MA_DISPATCHER_ARG_EXPANDER(ma_spipe_decorator_t*,ma_policy_service_get_spipe_decorator,(ma_policy_service_t *service), (service)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_spipe_handler_t*,ma_policy_service_get_spipe_handler,(ma_policy_service_t *service), (service)) \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_service_set_logger,(ma_policy_service_t *self, ma_logger_t *logger), (self, logger))  \
	MA_DISPATCHER_ARG_EXPANDER(ma_error_t,ma_policy_service_release,(ma_policy_service_t *self), (self))


char *ma_policy_service_function_names[] = {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) #function,
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
};

typedef struct ma_policy_service_functions_s {
#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) ret_type (*function##_fn) signature;
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER
}ma_policy_service_functions_t;
ma_policy_service_functions_t* ma_policy_service_functions = NULL;

#define ma_policy_service_functions_size (sizeof(ma_policy_service_function_names)/sizeof(ma_policy_service_function_names[0]))

#define MA_DISPATCHER_ARG_EXPANDER(ret_type, function, signature, arg) \
    MA_DISTPATCHER_ARG_STUB_CREATOR(&ma_policy_service_lib_version, MA_DISPATCHER_PRODUCT_NAME, MA_DISPATCHER_LIBRARY_NAME, MA_DISPATCHER_LIBRARY_VERSION,MA_DISPATCHER_MODULE,ret_type, function, signature, arg, ma_policy_service_function_names, &ma_policy_service_functions, ma_policy_service_functions_size)
    MA_DISPATCHER_ACTUAL_ARG_EXPANDER
#undef MA_DISPATCHER_ARG_EXPANDER

#undef MA_DISPATCHER_ACTUAL_ARG_EXPANDER
MA_CPP(})

#endif
#endif //MA_USE_DISPATCHER
