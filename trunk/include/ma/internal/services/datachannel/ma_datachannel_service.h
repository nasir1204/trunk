#ifndef MA_DATACHANNEL_SERVICE_H_INCLUDED
#define MA_DATACHANNEL_SERVICE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/services/ma_service.h"

MA_CPP(extern "C" {)
	
MA_DATACHANNEL_API ma_error_t ma_datachannel_service_create(char const *service_name, ma_service_t **datachannel_service);

#include "ma/internal/services/dispatcher/ma_datachannel_service_dispatcher.h"

MA_CPP(})

#endif
