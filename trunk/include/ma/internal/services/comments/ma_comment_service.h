#ifndef MA_COMMENT_SERVICE_H_INCLUDED
#define MA_COMMENT_SERVICE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/services/ma_service.h"
#include "ma/internal/utils/comment/ma_comment.h"

MA_CPP(extern "C" {)
/* comment service class */
typedef struct ma_comment_service_s ma_comment_service_t, *ma_comment_service_h;

// comment service ctor
MA_COMMENT_API ma_error_t ma_comment_service_create(const char *service_name, ma_service_t **service);

MA_COMMENT_API ma_error_t ma_comment_service_get_comment(ma_comment_service_t *service, ma_comment_t **comment);

MA_COMMENT_API ma_error_t ma_comment_service_get_msgbus(ma_comment_service_t *service, ma_msgbus_t **msgbus);

MA_CPP(})

#include "ma/dispatcher/ma_comment_service_dispatcher.h"
#endif /* MA_COMMENT_SERVICE_H_INCLUDED */

