#ifndef MA_SPIPE_HANDLER_H
#define MA_SPIPE_HANDLER_H

#include "ma/ma_common.h"
#include "ma/internal/utils/spipe/ma_spipe_package.h"
#include "ma/internal/utils/threading/ma_atomic.h"

#define IS_SPIPE_SEND_SUCCEEDED(response_code)  (200 == response_code || 202 == response_code)

MA_CPP(extern "C" {)

typedef struct ma_spipe_response_s{
	ma_error_t net_service_error_code;
	int http_response_code;
	ma_spipe_package_t *respond_spipe_pkg;
}ma_spipe_response_t;

typedef struct 	ma_spipe_handler_s ma_spipe_handler_t,*ma_spipe_handler_h;
typedef struct ma_spipe_handler_methods_s{	
	ma_error_t (*on_spipe_response)(ma_spipe_handler_t*, const ma_spipe_response_t *spipe_response);
	ma_error_t (*destroy)(ma_spipe_handler_t*);
}ma_spipe_handler_methods_t;

struct 	ma_spipe_handler_s{
	struct ma_spipe_handler_methods_s *vtable;
	ma_atomic_counter_t ref_count;
	void *data;
};

static MA_INLINE void ma_spipe_handler_init(ma_spipe_handler_t *self, struct ma_spipe_handler_methods_s *vtable, void *data){	
	self->vtable=vtable;
    self->data = data;
	MA_ATOMIC_INCREMENT(self->ref_count);
}

static MA_INLINE ma_error_t ma_spipe_handler_on_spipe_response(ma_spipe_handler_t *self, const ma_spipe_response_t *spipe_response){
	if(self && spipe_response){
		return self->vtable->on_spipe_response(self,spipe_response);
	}
	return MA_ERROR_INVALIDARG;
}

static MA_INLINE ma_error_t ma_spipe_handler_add_ref(ma_spipe_handler_t *self){
	if(self){
		MA_ATOMIC_INCREMENT(self->ref_count);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

static MA_INLINE ma_error_t ma_spipe_handler_release(ma_spipe_handler_t *self){
	if (self){
		if( 0 == MA_ATOMIC_DECREMENT(self->ref_count) )
			return self->vtable->destroy(self);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

MA_CPP(})
#endif
