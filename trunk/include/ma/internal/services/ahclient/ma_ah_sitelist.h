#ifndef MA_AH_SITELIST_H_INCLUDED
#define MA_AH_SITELIST_H_INCLUDED

#include "ma/ma_common.h"

#include "ma/internal/services/ahclient/ma_ah_site.h"

typedef struct ma_ah_sitelist_s ma_ah_sitelist_t, *ma_ah_sitelist_h;


MA_AH_CLIENT_API ma_error_t ma_ah_sitelist_create(ma_ah_sitelist_t **ah_sitelist);

MA_AH_CLIENT_API ma_error_t ma_ah_sitelist_assign(ma_ah_sitelist_t *self, ma_ah_sitelist_t *source);

MA_AH_CLIENT_API ma_error_t ma_ah_sitelist_release(ma_ah_sitelist_t *ah_sitelist);


/* ma_ah_sitelist Get APIs */
MA_AH_CLIENT_API const char *ma_ah_sitelist_get_ca_file(ma_ah_sitelist_t *ah_sitelist);

MA_AH_CLIENT_API const char *ma_ah_sitelist_get_local_version(ma_ah_sitelist_t *ah_sitelist);

MA_AH_CLIENT_API const char *ma_ah_sitelist_get_global_version(ma_ah_sitelist_t *ah_sitelist);

/* ma_ah_sitelist Set APIs */
MA_AH_CLIENT_API ma_error_t ma_ah_sitelist_set_ca_file(ma_ah_sitelist_t *ah_sitelist, const char *ca_file);

MA_AH_CLIENT_API ma_error_t ma_ah_sitelist_set_local_version(ma_ah_sitelist_t *ah_sitelist, const char *local_version);

MA_AH_CLIENT_API ma_error_t ma_ah_sitelist_set_global_version(ma_ah_sitelist_t *ah_sitelist, const char *global_version);


/* APIs to add/get ma_ah_site objects to/from ma_ah_sitelist*/

MA_AH_CLIENT_API ma_error_t ma_ah_sitelist_add_site(ma_ah_sitelist_t *ah_sitelist, ma_ah_site_t *ah_site);

MA_AH_CLIENT_API ma_error_t ma_ah_sitelist_adjust_site_order(ma_ah_sitelist_t *ah_sitelist);

MA_AH_CLIENT_API ma_error_t ma_ah_sitelist_remove_site(ma_ah_sitelist_t *ah_sitelist, ma_ah_site_t *ah_site);

/** @brief  gets number of sites from the sitelist.
 *  @param [in] ah_sitelist	sitelist object.
 *  @return		number of sites on successful.
 *              -1 on failure. 
 */
MA_AH_CLIENT_API ma_uint32_t  ma_ah_sitelist_get_count(ma_ah_sitelist_t *ah_sitelist);

/** @brief  gets site at specified index from the sitelist.
 *  @param [in] index index 
 *  @param [in] ah_sitelist	sitelist object 
 *  @return		site object on Successful.
 *              NULL on Failure. 
 */
MA_AH_CLIENT_API ma_ah_site_t *ma_ah_sitelist_get_site(ma_uint32_t index, ma_ah_sitelist_t *ah_sitelist);


/** @brief  callback which is executed in ma_ah_sitelist_for_each API.
 *  @param [in] ah_site	site object 
 *  @param [in] cb_data	user callback data.
 */
typedef ma_error_t (*for_each_callback)(ma_ah_site_t *ah_site, void *cb_data);

/** @brief  executes user callback on each site object in sitelist.
 *  @param [in] ah_sitelist	sitelist object 
 *  @param [in] cb	        user callback.
 *  @param [in] cb_data     user callbback data.
 *  @return		MA_OK on successful.
 */
MA_AH_CLIENT_API ma_error_t ma_ah_sitelist_for_each(ma_ah_sitelist_t *ah_sitelist, for_each_callback cb, void *cb_data);


#endif /* MA_AH_SITELIST_H_INCLUDED */

