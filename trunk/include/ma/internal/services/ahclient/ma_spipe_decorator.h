#ifndef MA_SPIPE_DECORATOR_H_INCLUDED
#define MA_SPIPE_DECORATOR_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/spipe/ma_spipe_package.h"
#include "ma/internal/utils/threading/ma_atomic.h"

MA_CPP(extern "C" {)

typedef enum ma_spipe_priority_e {	
	MA_SPIPE_PRIORITY_NONE		= 0x0,	
	MA_SPIPE_PRIORITY_NORMAL	= 0x1,
	MA_SPIPE_PRIORITY_IMMEDIATE	= 0x3,
}ma_spipe_priority_t;

typedef struct 	ma_spipe_decorator_s ma_spipe_decorator_t,*ma_spipe_decorator_h;
typedef struct ma_spipe_decorator_methods_s{
	ma_error_t (*decorate)(ma_spipe_decorator_t*, ma_spipe_priority_t, ma_spipe_package_t*);
	ma_error_t (*get_spipe_priority)(ma_spipe_decorator_t*, ma_spipe_priority_t*);
	ma_error_t (*destroy)(ma_spipe_decorator_t*);
}ma_spipe_decorator_methods_t;

struct 	ma_spipe_decorator_s{
	struct ma_spipe_decorator_methods_s *vtable;
	ma_atomic_counter_t ref_count;
	void *data;
};

static MA_INLINE void ma_spipe_decorator_init(ma_spipe_decorator_t *self, struct ma_spipe_decorator_methods_s *vtable, void *data){	
	self->vtable=vtable;
    self->data = data;
	MA_ATOMIC_INCREMENT(self->ref_count);
}

static MA_INLINE ma_error_t ma_spipe_decorator_add_ref(ma_spipe_decorator_t *self){
	if(self){
		MA_ATOMIC_INCREMENT(self->ref_count);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

static MA_INLINE ma_error_t ma_spipe_decorator_get_spipe_priority(ma_spipe_decorator_t *self, ma_spipe_priority_t *priority){
	if(self && priority){
		return self->vtable->get_spipe_priority(self,priority);
	}
	return MA_ERROR_INVALIDARG;
}

static MA_INLINE ma_error_t ma_spipe_decorator_decorate(ma_spipe_decorator_t *self, ma_spipe_priority_t priority, ma_spipe_package_t *spipepackage){
	if(self && spipepackage){
		return self->vtable->decorate(self,priority,spipepackage);
	}
	return MA_ERROR_INVALIDARG;
}

static MA_INLINE ma_error_t ma_spipe_decorator_release(ma_spipe_decorator_t *self){
	if(self){
		if( 0 == MA_ATOMIC_DECREMENT(self->ref_count) )
			return self->vtable->destroy(self);		
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

MA_CPP(})

#endif

