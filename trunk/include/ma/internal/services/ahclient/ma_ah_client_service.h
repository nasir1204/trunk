#ifndef MA_AH_CLIENT_SERVICE_H_INCLUDED
#define MA_AH_CLIENT_SERVICE_H_INCLUDED

#include "ma/internal/services/ahclient/ma_ah_sitelist.h"
#include "ma/internal/services/ahclient/ma_spipe_decorator.h"
#include "ma/internal/services/ahclient/ma_spipe_handler.h"
#include "ma/internal/utils/network/ma_net_common.h"
#include "ma/internal/services/ma_service.h"

MA_CPP(extern "C" {)

/** @brief  Agent handler client service instance.
 */
typedef struct ma_ah_client_service_s ma_ah_client_service_t,*ma_ah_client_service_h;

/** @brief  Create agent handler service.
 *  @param [out] ah_service AH client service. 	
 *  @return					MA_OK on successful initialization.
 *                          MA_ERROR_INVALIDARG on invalid arguments passed.
 */
MA_AH_CLIENT_API ma_error_t ma_ah_client_service_create(const char *service_name, ma_service_t **ah_service);

/** @brief  reload sitelist.
 */
MA_AH_CLIENT_API ma_error_t ma_ah_client_service_reload_sitelist(ma_ah_client_service_t *self);

/** @brief  reload proxy.
 */
MA_AH_CLIENT_API ma_error_t ma_ah_client_service_reload_proxy(ma_ah_client_service_t *self);

/** @brief  get new sitelist.
 */
MA_AH_CLIENT_API ma_error_t ma_ah_client_service_get_sitelist(ma_ah_client_service_t *self, const ma_ah_sitelist_t **sitelist);

/** @brief  Register Spipe decorator.
 *  @param [in] ahservice  AH client service. 	
 *  @param [in] sdecorator	Spipe decorator handle 	
 *  @return					MA_OK on successful register
 *                          MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_AH_CLIENT_API ma_error_t ma_ah_client_service_register_spipe_decorator(ma_ah_client_service_t *self, ma_spipe_decorator_t *sdecorator);

/** @brief  Unregister Spipe decorator.
 *  @param [in] ahservice   AH client service. 	
 *  @param [in] sdecorator	Spipe decorator handle 	
 *  @return					MA_OK on successful unregister
 *                          MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_AH_CLIENT_API ma_error_t ma_ah_client_service_unregister_spipe_decorator(ma_ah_client_service_t *self, ma_spipe_decorator_t *sdecorator);

/** @brief  Register Spipe handler.
 *  @param [in] ah_service  AH client service. 	
 *  @param [in] shandler	Spipe response handler handle 	
 *  @return					MA_OK on successful register
 *                          MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_AH_CLIENT_API ma_error_t ma_ah_client_service_register_spipe_handler(ma_ah_client_service_t *self,ma_spipe_handler_t *shandler);

/** @brief  Unregister Spipe handler.
 *  @param [in] ah_service   AH client service. 	
 *  @param [in] shandler	Spipe response handler handle 	
 *  @return					MA_OK on successful unregister
 *                          MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_AH_CLIENT_API ma_error_t ma_ah_client_service_unregister_spipe_handler(ma_ah_client_service_t *self,ma_spipe_handler_t *shandler);

/** @brief  Raise spipe alert, spipe decorator calls raise_spipe_alert whenever they have spipe decoration pending.
 *  @param [in] ah_service     AH client service. 	 
 *  @param [in] package_type   Package Type.
 *  @return					MA_OK on successful alert.
 *                          MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_AH_CLIENT_API ma_error_t ma_ah_client_service_raise_spipe_alert(ma_ah_client_service_t *self, char const *package_type);

/** @brief  Get the last connection info, so that policy can use the same AH.
 *  @param [in] ah_service     AH client service. 	 
 *  @param [in] url_request    last url request.
 *  @return					MA_OK on successful.
 *                          MA_ERROR_INVALIDARG on invalid arguments passed
 */
MA_AH_CLIENT_API ma_error_t ma_ah_client_service_get_last_connection(ma_ah_client_service_t *self,const ma_url_request_t **url_request);

/** @brief  Send the uninstall package to epo.
 *  @param [in] ah_service     AH client service. 	 
 *  @return					MA_OK on successful.
 *                          MA_ERROR_INVALIDARG on invalid arguments passed
 */MA_AH_CLIENT_API ma_error_t ma_ah_client_service_send_uninstall_package(ma_ah_client_service_t *self);


MA_CPP(})

#endif //end MA_AH_CLIENT_SERVICE_H_INCLUDED
