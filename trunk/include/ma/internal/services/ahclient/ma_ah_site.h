#ifndef MA_AH_SITE_H_INCLUDED
#define MA_AH_SITE_H_INCLUDED

#include "ma/ma_common.h"

typedef struct ma_ah_site_s ma_ah_site_t, *ma_ah_site_h;

MA_AH_CLIENT_API ma_error_t ma_ah_site_create(ma_ah_site_t **ah_site);

MA_AH_CLIENT_API ma_error_t ma_ah_site_release(ma_ah_site_t *ah_site);

/* ma_ah_site Get APIs */
MA_AH_CLIENT_API const char *ma_ah_site_get_name(ma_ah_site_t *ah_site);

MA_AH_CLIENT_API const char *ma_ah_site_get_version(ma_ah_site_t *ah_site);

MA_AH_CLIENT_API const char *ma_ah_site_get_fqdn_name(ma_ah_site_t *ah_site);

MA_AH_CLIENT_API const char *ma_ah_site_get_short_name(ma_ah_site_t *ah_site);

MA_AH_CLIENT_API const char *ma_ah_site_get_ip_address(ma_ah_site_t *ah_site);

MA_AH_CLIENT_API ma_uint32_t ma_ah_site_get_port(ma_ah_site_t *ah_site);

MA_AH_CLIENT_API ma_uint32_t ma_ah_site_get_ssl_port(ma_ah_site_t *ah_site);

/* ma_ah_site Set APIs */
MA_AH_CLIENT_API ma_error_t ma_ah_site_set_name(ma_ah_site_t *ah_site, const char *name);

MA_AH_CLIENT_API ma_error_t ma_ah_site_set_version(ma_ah_site_t *ah_site, const char *version);

MA_AH_CLIENT_API ma_error_t ma_ah_site_set_fqdn_name(ma_ah_site_t *ah_site, const char *fqdn_name);

MA_AH_CLIENT_API ma_error_t ma_ah_site_set_short_name(ma_ah_site_t *ah_site, const char *short_name);

MA_AH_CLIENT_API ma_error_t ma_ah_site_set_ip_address(ma_ah_site_t *ah_site, const char *ip_address);

MA_AH_CLIENT_API ma_error_t ma_ah_site_set_port(ma_ah_site_t *ah_site, ma_uint32_t port);

MA_AH_CLIENT_API ma_error_t ma_ah_site_set_ssl_port(ma_ah_site_t *ah_site, ma_uint32_t ssl_port);

MA_AH_CLIENT_API ma_error_t ma_ah_site_set_order(ma_ah_site_t *ah_site, ma_uint32_t order);

#endif /* MA_AH_SITE_H_INCLUDED */

