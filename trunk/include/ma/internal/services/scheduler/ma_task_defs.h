#ifndef MA_TASK_DEFS_H_INCLUDED
#define MA_TASK_DEFS_H_INCLUDED

#define MA_TOTAL_NO_OF_HOURS_IN_DAY 24
#define MA_TOTAL_NO_OF_MINUTES_IN_HOUR 60 
// task def(s)
#define MA_TASK_ATTR_ID "task_id"
#define MA_TASK_ATTR_CONDITIONS "task_conds"
#define MA_TASK_ATTR_RETRY_POLICY "task_retry_policy"
#define MA_TASK_ATTR_REPEAT_POLICY "task_repeat_policy"
#define MA_TASK_ATTR_RANDOMIZE_POLICY "task_randomize_policy"
#define MA_TASK_ATTR_DELAY_POLICY "task_delay_policy"
#define MA_TASK_ATTR_APP_PAYLOAD "task_app_payload"
#define MA_TASK_ATTR_EXEC_STATUS "task_execution_status"
#define MA_TASK_ATTR_MAX_RUN_TIME "task_max_run_time"
#define MA_TASK_ATTR_MAX_RUN_TIME_LIMIT "task_max_run_time_limit"
#define MA_TASK_ATTR_MISSED_POLICY "task_missed_policy"
#define MA_TASK_ATTR_PRIORITY "task_priority"
#define MA_TASK_ATTR_CREATOR "task_creator"
#define MA_TASK_ATTR_TRIGGER_LIST "task_trigger_list"
#define MA_TASK_ATTR_NAME "task_name"
#define MA_TASK_ATTR_TYPE "task_type"
#define MA_TASK_ATTR_STATE "task_state"
#define MA_TASK_ATTR_EXECUTION_STATUS "task_execution_status"
#define MA_TASK_ATTR_SOFTWARE_ID "task_software_id"
#define MA_TASK_ATTR_LAST_RUN_TIME "task_last_run_time"
#define MA_TASK_ATTR_NEXT_RUN_TIME "task_next_run_time"
#define MA_TASK_ATTR_TIME_ZONE "task_time_zone"
#define MA_TASK_ATTR_POWER_POLICY "task_power_policy"
#define MA_TASK_ATTR_MISSED "task_missed"

// trigger def(s)
#define MA_TRIGGER_ATTR_ID "trigger_id"
#define MA_TRIGGER_ATTR_POLICY "trigger_policy"
#define MA_TRIGGER_ATTR_BEGIN_DATE "trigger_begin_date"
#define MA_TRIGGER_ATTR_STATE "trigger_state"
#define MA_TRIGGER_ATTR_END_DATE "trigger_end_date"
#define MA_TRIGGER_ATTR_TYPE  "trigger_type"
#define MA_TRIGGER_ATTR_NEXT_FIRE_DATE "trigger_next_fire_date"
#define MA_TRIGGER_ATTR_LAST_FIRE_DATE "trigger_last_fire_date"

#endif // MA_TASK_DEFS_H_INCLUDED


