#ifndef MA_TRIGGER_CORE_H_INCLUDED
#define MA_TRIGGER_CORE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/defs/ma_scheduler_defs.h"
#include "ma/scheduler/ma_task.h"
#include "ma/internal/utils/threading/ma_atomic.h"
#include "uv.h"

MA_CPP(extern "C" {)
typedef struct ma_trigger_core_s ma_trigger_core_t;
typedef struct ma_trigger_core_methods_s ma_trigger_core_methods_t;

/* trigger flags */
typedef enum ma_trigger_flags_e {
    MA_TRIGGER_HAS_END_DATE = 0x01,
    MA_TRIGGER_DISABLED = 0x02,
}ma_trigger_flag_t;

typedef enum ma_watcher_type_e {
    MA_WATCHER_IDLE,
    MA_WATCHER_TIMER,
}ma_watcher_type_t;


#undef MA_TRIGGER_CORE_ARG_EXPANDER
#define MA_TRIGGER_CORE_ARG_EXPANDER\
    MA_TRIGGER_CORE_ARG_EXPAND(ma_error_t,ma_trigger_core_fire,(ma_trigger_core_t *core),(core))\
    MA_TRIGGER_CORE_ARG_EXPAND(ma_error_t,ma_trigger_core_next_fire,(ma_trigger_core_t *core),(core))\
    MA_TRIGGER_CORE_ARG_EXPAND(ma_error_t,ma_trigger_core_next_fire_date,(ma_trigger_core_t *core),(core))\
    MA_TRIGGER_CORE_ARG_EXPAND(ma_error_t,ma_trigger_core_stop_fire,(ma_trigger_core_t *core),(core))\
    MA_TRIGGER_CORE_ARG_EXPAND(ma_error_t,ma_trigger_core_reset,(ma_trigger_core_t *core, ma_int64_t timeout),(core, timeout))\
    MA_TRIGGER_CORE_ARG_EXPAND(ma_error_t,ma_trigger_core_force_stop,(ma_trigger_core_t *core),(core))\
    MA_TRIGGER_CORE_ARG_EXPAND(ma_error_t,ma_trigger_core_close,(ma_trigger_core_t *core),(core))


struct ma_trigger_core_methods_s {
#define MA_TRIGGER_CORE_ARG_EXPAND(ret_type, function, signature, arg) ret_type (*function##_fn) signature;
    MA_TRIGGER_CORE_ARG_EXPANDER
#undef MA_TRIGGER_CORE_ARG_EXPAND
};


/* trigger structure */
struct ma_trigger_core_s {
    const ma_trigger_core_methods_t *core_methods;
    union {
        uv_idle_t idler;
        uv_timer_t timer;
    }watcher;
    ma_watcher_type_t watcher_type;
    ma_trigger_state_t state;
    char id[MA_MAX_TRIGGERID_LEN + 1];
    ma_task_time_t begin_date;
    ma_task_time_t last_fire_date;
    ma_task_time_t next_fire_date;
    ma_task_time_t last_next_fire_date;
    ma_task_time_t until_date;
    ma_task_time_t end_date;
    ma_uint16_t random_interval;
    size_t no_of_intervals;
    ma_uint32_t duration;
    ma_uint32_t interval;
    ma_bool_t is_run_time_limited;
    ma_bool_t signal_stop;
    int curr_retry_count;
    ma_trigger_type_t type;
    unsigned int flags;
    uv_loop_t *loop;
    ma_task_t *task;
    ma_uint8_t start_lock:1;
    ma_atomic_counter_t ref_count;
    ma_bool_t randomize;
    void *core_data;
};
// trigger polymorphic method(s)
void ma_trigger_core_init(ma_trigger_core_t *core, const ma_trigger_core_methods_t *methods, ma_trigger_type_t type, ma_watcher_type_t watcher_type, void *data);


#define MA_TRIGGER_CORE_ARG_EXPAND(ret_type, function, signature, arg)\
    static MA_INLINE ret_type function signature {\
        if(core && core->core_methods) {\
            return core && core->core_methods->function##_fn?core && core->core_methods->function##_fn arg:MA_ERROR_PRECONDITION;\
        }\
        return MA_ERROR_INVALIDARG;\
    }
    MA_TRIGGER_CORE_ARG_EXPANDER
#undef MA_TRIGGER_CORE_ARG_EXPAND

void core_timer_state_machine_cb(uv_timer_t *timer, int status);
void core_timer_close_cb(uv_handle_t *timer);
ma_error_t core_set_next_until_date(ma_trigger_core_t *core);
ma_error_t core_add_ref(ma_trigger_core_t *core);

void core_compute_trigger_interval_count(ma_trigger_core_t *trigger);
ma_error_t core_calculate_and_notify_timer_fire(ma_trigger_core_t *core);
ma_error_t core_calculate_and_notify_timer_misfires(ma_trigger_core_t *core);
ma_error_t core_notify_timer_fire(ma_trigger_core_t *core);
ma_error_t core_notify_timer_next_fire(ma_trigger_core_t *core);
ma_error_t core_notify_timer_fire_close(ma_trigger_core_t *core);
ma_error_t core_notify_system_startup_event_timer_next_fire(ma_trigger_core_t *core);
ma_error_t core_notify_force_stop(ma_trigger_core_t *core);

ma_error_t core_notify_run_once_fire(ma_trigger_core_t *core);
ma_error_t core_notify_run_now_fire(ma_trigger_core_t *core);
ma_error_t core_run_once_next_fire(ma_trigger_core_t *core);
ma_error_t core_run_once_stop_fire(ma_trigger_core_t *core);


ma_error_t daily_next_fire_date(ma_trigger_core_t *core);
ma_error_t core_notify_timer_fire_intervals(ma_trigger_core_t *core);
ma_error_t core_notify_timer_next_fire(ma_trigger_core_t *core);
ma_error_t core_daily_stop_fire(ma_trigger_core_t *core);


ma_error_t core_weekly_stop_fire(ma_trigger_core_t *core);


ma_error_t core_monthly_date_stop_fire(ma_trigger_core_t *core);
ma_error_t core_monthly_dow_stop_fire(ma_trigger_core_t *core);

ma_error_t core_notify_force_reset(ma_trigger_core_t *core, ma_int64_t timeout);

ma_error_t core_notify_event_fire(ma_trigger_core_t *core);
ma_error_t core_notify_event_timer_next_fire(ma_trigger_core_t *core);
ma_error_t core_notify_event_force_reset(ma_trigger_core_t *core, ma_int64_t timeout);

ma_error_t core_get_start_lock_status(ma_trigger_core_t *core, ma_bool_t *status);
ma_error_t core_reset_start_lock(ma_trigger_core_t *core);

ma_error_t core_set_begin_date(ma_trigger_core_t *core, ma_task_time_t *date);
ma_error_t core_set_end_date(ma_trigger_core_t *core, ma_task_time_t *date);
ma_error_t core_set_state(ma_trigger_core_t *core, ma_trigger_state_t state);
ma_error_t core_set_until_date(ma_trigger_core_t *core, ma_task_time_t date);
ma_error_t core_set_task_handle(ma_trigger_core_t *core, ma_task_t *task);
ma_error_t core_set_duration(ma_trigger_core_t *core, ma_uint32_t duration);
ma_error_t core_set_interval(ma_trigger_core_t *core, ma_uint32_t interval);
ma_error_t core_set_random_interval(ma_trigger_core_t *core, ma_uint16_t random_interval);
ma_error_t core_set_disable(ma_trigger_core_t *core, ma_bool_t disable);
ma_error_t core_set_id(ma_trigger_core_t *core, const char *id);
ma_error_t core_set_loop(ma_trigger_core_t *core, uv_loop_t *loop);
ma_error_t core_set_curr_retry_count(ma_trigger_core_t *core, int count);
ma_error_t core_set_next_fire_date(ma_trigger_core_t *core, ma_task_time_t next_fire_date);
ma_error_t core_set_last_fire_date(ma_trigger_core_t *core, ma_task_time_t next_fire_date);
ma_error_t core_set_task(ma_trigger_core_t *core, ma_task_t *task);

ma_error_t core_get_type(ma_trigger_core_t *core, ma_trigger_type_t *type);
ma_error_t core_get_state(ma_trigger_core_t *trigger, ma_trigger_state_t *state);
ma_error_t core_get_id(ma_trigger_core_t *core, const char **id);
ma_error_t core_get_begin_date(ma_trigger_core_t *core, ma_task_time_t *begin_date);
ma_error_t core_get_end_date(ma_trigger_core_t *core, ma_task_time_t *end_date);
ma_error_t core_get_next_run_time(ma_trigger_core_t *core, ma_task_time_t *next_run_time);
ma_error_t core_get_task_handle(ma_trigger_core_t *core, ma_task_t **task);
ma_error_t core_get_loop(ma_trigger_core_t *core, uv_loop_t **loop);
ma_error_t core_get_duration(ma_trigger_core_t *core, ma_uint32_t *duration);
ma_error_t core_get_interval(ma_trigger_core_t *core, ma_uint32_t *intervals);
ma_error_t core_get_random_interval(ma_trigger_core_t *core, ma_uint16_t *random_interval);
ma_error_t core_get_curr_retry_count(ma_trigger_core_t *core, int *curr_retry_count);
ma_error_t core_get_next_fire_date(ma_trigger_core_t *core, ma_task_time_t *date);
ma_error_t core_get_last_fire_date(ma_trigger_core_t *core, ma_task_time_t *date);


ma_error_t core_release(ma_trigger_core_t *core);


/* helper(s) method decleration(s) */
void core_reset_task_exec_not_started_status(ma_task_t *task);
ma_bool_t core_check_task_runnable(ma_task_t *task);
ma_error_t core_calculate_and_notify_timer_intervals(ma_trigger_core_t *core);
ma_error_t core_get_next_missed_run_time(ma_trigger_core_t *core, ma_task_time_t *missed);
ma_bool_t core_check_missed_runs(ma_trigger_core_t *core);
void core_timer_destroy(ma_trigger_core_t *core);

MA_CPP(})
#endif // MA_TRIGGER_CORE_H_INCLUDED


