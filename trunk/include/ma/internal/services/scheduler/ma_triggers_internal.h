#ifndef MA_TRIGGER_INTERNAL_H_INCLUDED
#define MA_TRIGGER_INTERNAL_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/services/scheduler/ma_scheduler_core.h"
#include "ma/scheduler/ma_triggerlist.h"
#include "ma/ma_variant.h"
#include "uv.h"

MA_CPP(extern "C" {)
typedef struct ma_trigger_run_once_s ma_trigger_run_once_t, *ma_trigger_runonce_h;
typedef struct ma_trigger_daily_s ma_trigger_daily_t, *ma_trigger_daily_h;
typedef struct ma_trigger_weekly_s ma_trigger_weekly_t, *ma_trigger_weekly_h;
typedef struct ma_trigger_monthly_date_s ma_trigger_monthly_date_t, *ma_trigger_monthly_date_h;
typedef struct ma_trigger_monthly_dow_s ma_trigger_monthly_dow_t, *ma_trigger_monthly_dow_h;
typedef struct ma_trigger_missed_s ma_trigger_missed_t, *ma_trigger_missed_h;
typedef struct ma_trigger_system_idle_s ma_trigger_system_idle_t, *ma_trigger_system_idle_h;
typedef struct ma_trigger_app_idle_s ma_trigger_app_idle_t, *ma_trigger_app_idle_h;
typedef struct ma_trigger_powersave_s ma_trigger_powersave_t, *ma_trigger_powersave_h;
typedef struct ma_trigger_dialup_s ma_trigger_dialup_t, *ma_trigger_dialup_h;
typedef struct ma_trigger_system_start_s ma_trigger_system_start_t, *ma_trigger_system_start_h;
typedef struct ma_trigger_logon_s ma_trigger_logon_t, *ma_trigger_logon_h;
typedef struct ma_trigger_run_now_s ma_trigger_run_now_t, *ma_trigger_now_h;

// setter(s)
MA_SCHEDULER_API ma_error_t ma_trigger_set_duration(ma_trigger_t *trigger, ma_uint32_t duration);
MA_SCHEDULER_API ma_error_t ma_trigger_set_interval(ma_trigger_t *trigger, ma_uint32_t interval);
MA_SCHEDULER_API ma_error_t ma_trigger_set_random_interval(ma_trigger_t *trigger, ma_uint16_t random_interval);
MA_SCHEDULER_API ma_error_t ma_trigger_set_disable(ma_trigger_t *trigger, ma_bool_t disable);
MA_SCHEDULER_API ma_error_t ma_trigger_set_loop(ma_trigger_t *trigger, uv_loop_t *loop);
MA_SCHEDULER_API ma_error_t ma_trigger_set_curr_retry_count(ma_trigger_t *trigger, int curr_retry_count);
MA_SCHEDULER_API ma_error_t ma_trigger_get_curr_retry_count(ma_trigger_t *trigger, int *curr_retry_count);

// getter(s)
MA_SCHEDULER_API ma_error_t ma_trigger_get_duration(ma_trigger_t *trigger, ma_uint32_t *duration);
MA_SCHEDULER_API ma_error_t ma_trigger_get_interval(ma_trigger_t *trigger, ma_uint32_t *intervals);
MA_SCHEDULER_API ma_error_t ma_trigger_get_random_interval(ma_trigger_t *trigger, ma_uint16_t *random_interval);
MA_SCHEDULER_API ma_error_t ma_trigger_get_loop(ma_trigger_t *trigger, uv_loop_t **loop);
MA_SCHEDULER_API ma_error_t ma_trigger_get_curr_retry_count(ma_trigger_t *trigger, int *curr_retry_count);
MA_SCHEDULER_API ma_error_t ma_trigger_get_next_run_time(ma_trigger_t *trigger, ma_task_time_t *next_run_time);
MA_SCHEDULER_API ma_error_t ma_trigger_stop(ma_trigger_t *trigger);
MA_SCHEDULER_API ma_error_t ma_trigger_set_task(ma_trigger_t *trigger, ma_task_t *task);
MA_SCHEDULER_API ma_error_t ma_trigger_close(ma_trigger_t *trigger);

ma_error_t trigger_set_begin_date(ma_trigger_t *trigger, ma_task_time_t *start_date);
ma_error_t trigger_set_end_date(ma_trigger_t *trigger, ma_task_time_t *end_date);
ma_error_t trigger_get_type(ma_trigger_t *trigger, ma_trigger_type_t *type);
ma_error_t trigger_get_state(ma_trigger_t *trigger, ma_trigger_state_t *state);
ma_error_t trigger_get_id(ma_trigger_t *trigger, const char **trigger_id);
ma_error_t trigger_get_begin_date(ma_trigger_t *trigger, ma_task_time_t *begin_date);
ma_error_t trigger_get_end_date(ma_trigger_t *trigger, ma_task_time_t *end_date);
ma_error_t trigger_set_state(ma_trigger_t *trigger, ma_trigger_state_t state);
ma_error_t trigger_get_policy(ma_trigger_t *trigger, void *policy);
ma_error_t trigger_release(ma_trigger_t *trigger);
ma_error_t trigger_add_ref(ma_trigger_t *trigger);
ma_error_t trigger_set_next_fire_date(ma_trigger_t *trigger, ma_task_time_t next_fire_date);
ma_error_t trigger_get_next_fire_date(ma_trigger_t *trigger, ma_task_time_t *next_fire_date);
ma_error_t trigger_set_last_fire_date(ma_trigger_t *trigger, ma_task_time_t date);
ma_error_t trigger_get_last_fire_date(ma_trigger_t *trigger, ma_task_time_t *date);
ma_error_t calender_trigger_validate_policy(ma_trigger_t *trigger);


#undef MA_TRIGGER_CORE_ACTUAL_ARG_EXPANDER
#define MA_TRIGGER_CORE_ACTUAL_ARG_EXPANDER\
    MA_TRIGGER_CORE_ARG_EXPAND(ma_error_t,set_begin_date,(ma_trigger_t *trigger, ma_task_time_t *start_date),(trigger, start_date),((ma_trigger_core_t*)trigger->data, start_date))\
    MA_TRIGGER_CORE_ARG_EXPAND(ma_error_t,set_end_date,(ma_trigger_t *trigger, ma_task_time_t *end_date),(trigger, end_date),((ma_trigger_core_t*)trigger->data, end_date))\
    MA_TRIGGER_CORE_ARG_EXPAND(ma_error_t,set_state,(ma_trigger_t *trigger, ma_trigger_state_t state),(trigger, state),((ma_trigger_core_t*)trigger->data, state))\
    MA_TRIGGER_CORE_ARG_EXPAND(ma_error_t,set_next_fire_date,(ma_trigger_t *trigger, ma_task_time_t next_fire_date),(trigger, next_fire_date), ((ma_trigger_core_t*)trigger->data, next_fire_date))\
    MA_TRIGGER_CORE_ARG_EXPAND(ma_error_t,set_last_fire_date,(ma_trigger_t *trigger, ma_task_time_t last_fire_date),(trigger, last_fire_date), ((ma_trigger_core_t*)trigger->data, last_fire_date))\
    MA_TRIGGER_CORE_ARG_EXPAND(ma_error_t,get_type,(ma_trigger_t *trigger, ma_trigger_type_t *type),(trigger, type), ((ma_trigger_core_t*)trigger->data, type))\
    MA_TRIGGER_CORE_ARG_EXPAND(ma_error_t,get_state,(ma_trigger_t *trigger, ma_trigger_state_t *state),(trigger, state), ((ma_trigger_core_t*)trigger->data, state))\
    MA_TRIGGER_CORE_ARG_EXPAND(ma_error_t,get_id,(ma_trigger_t *trigger, const char **id),(trigger, id), ((ma_trigger_core_t*)trigger->data, id))\
    MA_TRIGGER_CORE_ARG_EXPAND(ma_error_t,get_begin_date,(ma_trigger_t *trigger, ma_task_time_t *date) ,(trigger, date), ((ma_trigger_core_t*)trigger->data, date))\
    MA_TRIGGER_CORE_ARG_EXPAND(ma_error_t,get_end_date,(ma_trigger_t *trigger, ma_task_time_t *date),(trigger, date), ((ma_trigger_core_t*)trigger->data, date))\
    MA_TRIGGER_CORE_ARG_EXPAND(ma_error_t,release,(ma_trigger_t *trigger),(trigger), ((ma_trigger_core_t*)trigger->data))\
    MA_TRIGGER_CORE_ARG_EXPAND(ma_error_t,get_next_fire_date,(ma_trigger_t *trigger, ma_task_time_t *next_fire_date),(trigger, next_fire_date),((ma_trigger_core_t*)trigger->data, next_fire_date))\
    MA_TRIGGER_CORE_ARG_EXPAND(ma_error_t,get_last_fire_date,(ma_trigger_t *trigger, ma_task_time_t *last_fire_date),(trigger, last_fire_date),((ma_trigger_core_t*)trigger->data, last_fire_date))\
    MA_TRIGGER_CORE_ARG_EXPAND(ma_error_t,add_ref,(ma_trigger_t *trigger),(trigger),((ma_trigger_core_t*)trigger->data))



#undef MA_TRIGGER_EXTN_CORE_ACTUAL_ARG_EXPANDER
#define MA_TRIGGER_EXTN_CORE_ACTUAL_ARG_EXPANDER\
    MA_TRIGGER_EXTN_CORE_ARG_EXPAND(ma_error_t,set_until_date,(ma_trigger_t *trigger, ma_task_time_t date),(trigger, date), ((ma_trigger_core_t*)trigger->data, date))\
    MA_TRIGGER_EXTN_CORE_ARG_EXPAND(ma_error_t,set_task_handle,(ma_trigger_t *trigger, ma_task_t *task),(trigger, task), ((ma_trigger_core_t*)trigger->data, task))\
    MA_TRIGGER_EXTN_CORE_ARG_EXPAND(ma_error_t,set_duration,(ma_trigger_t *trigger, ma_uint32_t duration),(trigger, duration),((ma_trigger_core_t*)trigger->data, duration))\
    MA_TRIGGER_EXTN_CORE_ARG_EXPAND(ma_error_t,set_interval,(ma_trigger_t *trigger, ma_uint32_t interval),(trigger, interval),((ma_trigger_core_t*)trigger->data, interval))\
    MA_TRIGGER_EXTN_CORE_ARG_EXPAND(ma_error_t,set_random_interval,(ma_trigger_t *trigger, ma_uint16_t interval),(trigger, interval),((ma_trigger_core_t*)trigger->data, interval))\
    MA_TRIGGER_EXTN_CORE_ARG_EXPAND(ma_error_t,set_disable,(ma_trigger_t *trigger, ma_bool_t disable),(trigger, disable), ((ma_trigger_core_t*)trigger->data, disable))\
    MA_TRIGGER_EXTN_CORE_ARG_EXPAND(ma_error_t,set_id,(ma_trigger_t *trigger, const char *id),(trigger, id), ((ma_trigger_core_t*)trigger->data, id))\
    MA_TRIGGER_EXTN_CORE_ARG_EXPAND(ma_error_t,set_loop,(ma_trigger_t *trigger, uv_loop_t *loop),(trigger, loop), ((ma_trigger_core_t*)trigger->data, loop))\
    MA_TRIGGER_EXTN_CORE_ARG_EXPAND(ma_error_t,set_curr_retry_count,(ma_trigger_t *trigger, int count) ,(trigger, count), ((ma_trigger_core_t*)trigger->data, count))\
    MA_TRIGGER_EXTN_CORE_ARG_EXPAND(ma_error_t,get_next_run_time,(ma_trigger_t *trigger, ma_task_time_t *next_run_time),(trigger, next_run_time), ((ma_trigger_core_t*)trigger->data, next_run_time))\
    MA_TRIGGER_EXTN_CORE_ARG_EXPAND(ma_error_t,get_task_handle,(ma_trigger_t *trigger, ma_task_t **task),(trigger, task), ((ma_trigger_core_t*)trigger->data, task))\
    MA_TRIGGER_EXTN_CORE_ARG_EXPAND(ma_error_t,get_loop,(ma_trigger_t *trigger, uv_loop_t **loop) ,(trigger, loop), ((ma_trigger_core_t*)trigger->data, loop))\
    MA_TRIGGER_EXTN_CORE_ARG_EXPAND(ma_error_t,get_duration,(ma_trigger_t *trigger, ma_uint32_t *duration),(trigger, duration), ((ma_trigger_core_t*)trigger->data, duration))\
    MA_TRIGGER_EXTN_CORE_ARG_EXPAND(ma_error_t,get_interval,(ma_trigger_t *trigger, ma_uint32_t *intervals),(trigger, intervals), ((ma_trigger_core_t*)trigger->data, intervals))\
    MA_TRIGGER_EXTN_CORE_ARG_EXPAND(ma_error_t,get_random_interval,(ma_trigger_t *trigger, ma_uint16_t *random_interval),(trigger, random_interval), ((ma_trigger_core_t*)trigger->data, random_interval))\
    MA_TRIGGER_EXTN_CORE_ARG_EXPAND(ma_error_t,get_curr_retry_count,(ma_trigger_t *trigger, int *curr_retry_count),(trigger, curr_retry_count), ((ma_trigger_core_t*)trigger->data, curr_retry_count))\
    MA_TRIGGER_EXTN_CORE_ARG_EXPAND(ma_error_t,set_task,(ma_trigger_t *trigger, ma_task_t *task),(trigger, task), ((ma_trigger_core_t*)trigger->data, task))



MA_CPP(})
#endif /* MA_TRIGGER_INTERNAL_H_INCLUDED */

