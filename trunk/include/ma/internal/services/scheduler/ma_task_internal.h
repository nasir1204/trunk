#ifndef MA_TASK_INTERNAL_H_INCLUDED
#define MA_TASK_INTERNAL_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/services/scheduler/ma_scheduler_core.h"
#include "ma/internal/defs/ma_scheduler_defs.h"
#include "ma/scheduler/ma_triggerlist.h"
#include "ma/ma_variant.h"
#include "uv.h"

typedef enum ma_task_event_type_e {
    MA_TASK_EVENT_TYPE_NONE = 0x00,
    MA_TASK_EVENT_TYPE_SYSTEM_IDLE = 0x01,
    MA_TASK_EVENT_TYPE_SYSTEM_STARTUP = 0x02,
    MA_TASK_EVENT_TYPE_LOGON = 0x04,
    MA_TASK_EVENT_TYPE_DIALUP = 0x08,
    MA_TASK_EVENT_TYPE_SYSTEM_TIME_CHANGE = 0x100,
}ma_task_event_type_t;

MA_CPP(extern "C" {)

// setter(s)
MA_SCHEDULER_API ma_error_t ma_task_set_loop(ma_task_t *task, uv_loop_t *loop);
MA_SCHEDULER_API ma_error_t ma_task_set_scheduler(ma_task_t *task, ma_scheduler_t *scheduler);

// getter(s)
MA_SCHEDULER_API ma_error_t ma_task_get_loop(ma_task_t *task, uv_loop_t **loop);

// control task execution
MA_SCHEDULER_API ma_error_t ma_task_start(ma_task_t *task);
MA_SCHEDULER_API ma_error_t ma_task_stop(ma_task_t *task);
MA_SCHEDULER_API ma_error_t ma_task_set_event(ma_task_t *task, ma_task_event_type_t type);
MA_SCHEDULER_API ma_error_t ma_task_unset_event(ma_task_t *task, ma_task_event_type_t type);
MA_SCHEDULER_API ma_error_t ma_task_get_events(ma_task_t *task, ma_uint32_t *events);

MA_SCHEDULER_API ma_error_t ma_check_task_stopped(ma_task_t *task, ma_bool_t *stopped);
MA_SCHEDULER_API ma_error_t ma_task_set_execution_status_internal(ma_task_t *task, ma_task_exec_status_t status);
MA_SCHEDULER_API const char* ma_task_get_execution_status_str(ma_task_t *task, ma_task_exec_status_t status);
MA_SCHEDULER_API ma_error_t ma_task_run_after(ma_task_t *task, ma_task_interval_t intervals);
MA_SCHEDULER_API ma_error_t ma_task_publish_topics(ma_task_t *task, const char *topic);
MA_SCHEDULER_API ma_error_t ma_task_attributes_modified(ma_task_t *task1, ma_task_t *task2, ma_bool_t *result);
MA_SCHEDULER_API ma_error_t ma_task_attributes_copy_only(ma_task_t *src, ma_task_t *dest);

MA_CPP(})
#endif //MA_TASK_INTERNAL_H_INCLUDED

