#ifndef MA_SCHEDULER_DATASTORE_H_INCLUDED
#define MA_SCHEDULER_DATASTORE_H_INCLUDED

#include "ma/ma_common.h"

#include "ma/internal/services/scheduler/ma_scheduler_core.h"
#include "ma/internal/services/scheduler/ma_taskfolder.h"
#include "ma/scheduler/ma_task.h"

// read all tasks from ds_path and add into folder
ma_error_t ma_scheduler_datastore_read(ma_scheduler_t *scheduler, ma_taskfolder_t * folder, ma_ds_t *datastore, const char *ds_path);

// write task into ds under ds_path
ma_error_t ma_scheduler_datastore_write(ma_scheduler_t *scheduler, ma_task_t *task, ma_ds_t *datastore, const char *ds_path);


ma_error_t ma_scheduler_datastore_remove_task(ma_task_t *task, ma_ds_t *datastore, const char *ds_path);

ma_error_t ma_scheduler_datastore_clear(ma_ds_t *datastore, const char *ds_path);

MA_CPP(extern "C" {)


#endif // MA_SCHEDULER_DATASTORE_H_INCLUDED

