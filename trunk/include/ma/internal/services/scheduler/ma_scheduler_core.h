/** @file ma_scheduler.h
 *  @brief Scheduler prototypes 
 *  
 */

#ifndef MA_SCHEDULER_H_INCLUDED
#define MA_SCHEDULER_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/scheduler/ma_task.h"
#include "ma/ma_datastore.h"
#include "ma/datastore/ma_ds_database.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_log.h"

MA_CPP(extern "C" {)

typedef struct ma_scheduler_s ma_scheduler_t, *ma_scheduler_h;

typedef enum ma_scheduler_state_e {
    MA_SCHEDULER_STATE_NOTSTARTED = 1,
    MA_SCHEDULER_STATE_RUNNING,
    MA_SCHEDULER_STATE_STOPPED
}ma_scheduler_state_t;

/* @brief create scheduler instance
 */
MA_SCHEDULER_API ma_error_t ma_scheduler_create(ma_msgbus_t *msgbus, ma_ds_t *scheduler_datastore, const char *base_path, ma_scheduler_t **scheduler);

/* @brief starts scheduler 
 */
MA_SCHEDULER_API ma_error_t ma_scheduler_start(ma_scheduler_t *scheduler);

/* @brief retrieves current scheduler state
 */
MA_SCHEDULER_API ma_error_t ma_scheduler_get_state(ma_scheduler_t *scheduler, ma_scheduler_state_t *state);

/* @brief sets logger
 */
MA_SCHEDULER_API ma_error_t ma_scheduler_set_logger(ma_logger_t *ma_logger);


/* @brief stops scheduler 
 */
MA_SCHEDULER_API ma_error_t ma_scheduler_stop(ma_scheduler_t *scheduler);


/* @brief releases scheduler 
 */
MA_SCHEDULER_API ma_error_t ma_scheduler_release(ma_scheduler_t *scheduler);


/* @brief modifies scheduled task 
 */
MA_SCHEDULER_API ma_error_t ma_scheduler_modify_task(ma_scheduler_t *scheduler, ma_task_t *task);

/* @brief collects all the active task exist in scheduler folder
 */
MA_SCHEDULER_API ma_error_t ma_scheduler_get_runnning_task_set(ma_scheduler_t *scheduler, ma_task_t ***running_task_set, size_t *no_of_tasks);


/* @brief releases task handle set
 */
MA_SCHEDULER_API ma_error_t ma_scheduler_release_runnning_task_set(ma_task_t **running_task_set);


/* @brief collects all the task exist in scheduler
*/
MA_SCHEDULER_API ma_error_t ma_scheduler_get_task_set(ma_scheduler_t *scheduler, ma_task_t ***task_set, size_t *no_of_tasks);

/* @brief releases task handle set
 */
MA_SCHEDULER_API ma_error_t ma_scheduler_release_task_set(ma_task_t **task_set);

/* @brief creates task and added into scheduler
*/
MA_SCHEDULER_API ma_error_t ma_scheduler_create_task(ma_scheduler_t *scheduler, const char *creator, const char *task_id, const char *task_name, const char *task_type, ma_task_t **task);

/* @brief retrieves task handle for the given ID. User is responsible to call ma_task_release() on successful creation of task
*/
MA_SCHEDULER_API ma_error_t ma_scheduler_get_task_handle(ma_scheduler_t *scheduler, const char *task_id, ma_task_t **task);

/* @brief scheduler removes task
*/
MA_SCHEDULER_API ma_error_t ma_scheduler_release_task(ma_scheduler_t *scheduler, ma_task_t *task);

/* @brief enumerates task
*/
MA_SCHEDULER_API ma_error_t ma_scheduler_enumerate_task_server(ma_scheduler_t *scheduler, const char *product_id, const char *creator_id, const char *task_type, ma_task_t ***task_set, size_t *no_of_tasks);

/* @brief notifies event task exist in scheduler
*/
MA_SCHEDULER_API ma_error_t ma_scheduler_notify_event(ma_scheduler_t *scheduler, int event);

/* @brief restarts scheduler
 */
MA_SCHEDULER_API ma_error_t ma_scheduler_restart(ma_scheduler_t *scheduler);

/* @brief adds task into scheduler, caller is responsible to release task on successful
*/
MA_SCHEDULER_API ma_error_t ma_scheduler_add(ma_scheduler_t *scheduler, ma_task_t *task);

/* @brief removes task from scheduler
 */
MA_SCHEDULER_API ma_error_t ma_scheduler_remove(ma_scheduler_t *scheduler, const char *task_id);

#include "ma_ischeduler_core.h"

MA_CPP(})
#endif // MA_SCHEDULER_H_INCLUDED


