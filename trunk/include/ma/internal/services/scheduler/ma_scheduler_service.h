#ifndef MA_SCHEDULER_SERVICE_H_INCLUDED
#define MA_SCHEDULER_SERVICE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/services/ma_service.h"

MA_CPP(extern "C" {)
//scheduler service class
typedef struct ma_scheduler_service_s ma_scheduler_service_t, *ma_scheduler_service_h;

// scheduler service ctor
MA_SCHEDULER_API ma_error_t ma_scheduler_service_create(const char *service_name, ma_service_t **service);

MA_CPP(})

#include "ma/dispatcher/ma_scheduler_service_dispatcher.h"
#endif // MA_SCHEDULER_SERVICE_H_INCLUDED

