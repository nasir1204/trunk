/** @file ma_scheduler_analyser.h
 *  @brief Scheduler analyser prototypes 
 *  
 */

#ifndef MA_SCHEDULER_ANALYSER_H_INCLUDED
#define MA_SCHEDULER_ANALYSER_H_INCLUDED

#include "ma/ma_common.h"
#include "uv.h"
#include "ma/internal/services/scheduler/ma_scheduler_task.h"

typedef struct ma_scheduler_analyser_s ma_scheduler_analyser_t, *ma_scheduler_analyser_h;

struct ma_scheduler_analyser_s {
    ma_scheduler_base_t base;
    uv_loop_t *loop;
    ma_scheduler_task_t **slots;
    size_t capacity;
    size_t size;
};

MA_CPP(extern "C" {)
    
ma_error_t ma_scheduler_get_slotid(ma_scheduler_analyser_t *a, size_t *slotid);
ma_error_t ma_scheduler_task_slots_init(ma_scheduler_analyser_t *a);
ma_error_t ma_scheduler_task_slots_reserve(ma_scheduler_analyser_t *a, size_t size);
void ma_scheduler_task_slots_deinit(ma_scheduler_analyser_t *a);
void ma_scheduler_analyser_init(ma_scheduler_analyser_t *e, uv_loop_t *loop, void *data);

MA_CPP(})
#endif // MA_SCHEDULER_ANALYSER_H_INCLUDED


