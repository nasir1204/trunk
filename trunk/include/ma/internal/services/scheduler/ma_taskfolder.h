/** @file ma_scheduler_taskfloder.h
 *  @brief Scheduler task list prototypes 
 *  
 */

#ifndef MA_TASKFOLDER_H_INCLUDED
#define MA_TASKFOLDER_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/ma_macros.h"
#include "ma/scheduler/ma_task.h"
#include "ma/internal/services/scheduler/ma_scheduler_core.h"
#include "uv.h"

#include <stdio.h>

MA_CPP(extern "C" {)
typedef struct ma_taskfolder_s ma_taskfolder_t;
typedef enum ma_taskfolder_status_e ma_taskfolder_status_t;

enum ma_taskfolder_status_e {
    MA_TASKFOLDER_STATUS_NOT_STARTED = 0,
    MA_TASKFOLDER_STATUS_STARTING,
    MA_TASKFOLDER_STATUS_STARTED,
    MA_TASKFOLDER_STATUS_STOPPING,
    MA_TASKFOLDER_STATUS_STOPPED,
    MA_TASKFOLDER_STATUS_RESTARTING,
};

ma_error_t ma_taskfolder_create(ma_scheduler_t *scheduler, uv_loop_t *loop, ma_ds_t *scheduler_datastore, const char *base_path, ma_taskfolder_t **folder); 
ma_error_t ma_taskfolder_start(ma_taskfolder_t *folder);
ma_error_t ma_taskfolder_stop(ma_taskfolder_t *folder);
ma_error_t ma_taskfolder_release(ma_taskfolder_t *folder);
ma_error_t ma_taskfolder_get_size(ma_taskfolder_t *folder, int *size);
ma_error_t ma_taskfolder_add_task(ma_taskfolder_t *folder, ma_task_t *task);
ma_error_t ma_taskfolder_destroy_task(ma_taskfolder_t *folder, ma_task_t *task);
ma_error_t ma_taskfolder_get_task_handle(ma_taskfolder_t *folder, const char *task_id, ma_task_t **task);
ma_error_t ma_taskfolder_get_all_task_handle(ma_taskfolder_t *folder, ma_task_t ***task_set, size_t *no_of_tasks);
ma_error_t ma_taskfolder_rename_task(ma_taskfolder_t *folder, ma_task_t *new_task);
ma_error_t ma_taskfolder_enumerate_task(ma_taskfolder_t *folder, const char *product_id, const char *creator_id, const char *task_type, ma_task_t ***task_set, size_t *no_of_tasks);
ma_error_t ma_taskfolder_notify_event(ma_taskfolder_t *folder, int event);
ma_error_t ma_taskfolder_restart(ma_taskfolder_t *folder);
ma_error_t ma_taskfolder_get_status(ma_taskfolder_t *folder, int *status);

MA_CPP(})
#endif // MA_TASKFOLDER_H_INCLUDED


