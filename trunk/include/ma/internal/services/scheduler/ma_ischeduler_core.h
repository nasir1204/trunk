#ifndef MA_ISCHEDULER_H_INCLUDED
#define MA_ISCHEDULER_H_INCLUDED


#undef MA_SCHEDULER_CORE_ACTUAL_ARG_EXPANDER
#define MA_SCHEDULER_CORE_ACTUAL_ARG_EXPANDER\
    MA_SCHEDULER_CORE_ARG_EXPANDER(ma_error_t,modify_task,(ma_scheduler_t *scheduler, ma_task_t *task),(scheduler, task), (scheduler->folder, task))\
    MA_SCHEDULER_CORE_ARG_EXPANDER(ma_error_t,get_task_handle,(ma_scheduler_t *scheduler, const char *task_id, ma_task_t **task),(scheduler, task_id, task), (scheduler->folder, task_id, task))\
    MA_SCHEDULER_CORE_ARG_EXPANDER(ma_error_t,notify_event,(ma_scheduler_t *scheduler, int event),(scheduler, event), (scheduler->folder, event))\
    MA_SCHEDULER_CORE_ARG_EXPANDER(ma_error_t,restart,(ma_scheduler_t *scheduler),(scheduler), (scheduler->folder))\
    MA_SCHEDULER_CORE_ARG_EXPANDER(ma_error_t,run_task_on_demand,(ma_scheduler_t *scheduler, ma_task_t *task),(scheduler, task), (scheduler->folder, task))


#endif /* MA_ISCHEDULER_H_INCLUDED */


