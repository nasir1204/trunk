#ifndef MA_RUNNINGTASK_COLLECTOR_H_INCLUDED
#define MA_RUNNINGTASK_COLLECTOR_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/scheduler/ma_task.h"

MA_CPP(extern "C" {)
typedef struct ma_running_task_collector_s ma_running_task_collector_t;

MA_SCHEDULER_API ma_error_t ma_running_task_collector_create(ma_running_task_collector_t **collector);
MA_SCHEDULER_API ma_error_t ma_running_task_collector_add(ma_running_task_collector_t *collector, ma_task_t *td);
MA_SCHEDULER_API ma_error_t ma_running_task_collector_find(ma_running_task_collector_t *collector, ma_task_t *td, ma_bool_t *found);
MA_SCHEDULER_API ma_error_t ma_running_task_collector_remove(ma_running_task_collector_t *collector, ma_task_t *td);
MA_SCHEDULER_API ma_error_t ma_running_task_collector_start(ma_running_task_collector_t *collector, const char *task_id);
MA_SCHEDULER_API ma_error_t ma_running_task_collector_stop(ma_running_task_collector_t *collector, const char *task_id);
MA_SCHEDULER_API ma_error_t ma_running_task_collector_size(ma_running_task_collector_t *collector, size_t *task_count);
MA_SCHEDULER_API ma_error_t ma_running_task_collector_release(ma_running_task_collector_t *collector);
MA_CPP(})
#endif // MA_RUNNINGTASK_COLLECTOR_H_INCLUDED


