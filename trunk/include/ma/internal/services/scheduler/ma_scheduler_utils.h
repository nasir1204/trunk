#ifndef MA_SCHEDULER_UTILS_H_INCLUDED
#define MA_SCHEDULER_UTILS_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/scheduler/ma_task.h"
#include "ma/scheduler/ma_triggers.h"

#include <stdio.h>
#include <string.h>

#ifdef MA_WINDOWS
#include <time.h>
#else
#include <sys/time.h>
#endif

#define MA_SET_BIT(X,Y) ((X) |= (Y))
#define MA_CLEAR_BIT(X,Y) ((X) &= (~Y))
#define MA_TOGGLE_BIT(X,Y) ((X) ^= (Y))
#define MA_CHECK_BIT(X,Y) ((X) & (Y)) 

MA_CPP(extern "C" {)
ma_error_t convert_seconds_to_hour_and_minutes(ma_uint64_t seconds, ma_uint16_t *hour, ma_uint16_t *minutes);
ma_bool_t validate_date(ma_task_time_t date);
void get_localtime(ma_task_time_t *time);
void get_next_date(ma_task_time_t *next_day);
void compute_timeout(ma_task_time_t *tdate, ma_int64_t *timeout);
void get_week_day(ma_task_time_t *tdate, ma_day_of_week_t *week_day);
void get_next_week_date_after(ma_task_time_t start_date, ma_uint32_t days_of_week, ma_uint16_t weeks_interval, ma_task_time_t *new_date); 
void get_next_date_after(ma_task_time_t start_date, ma_task_time_t *new_date, int no_of_days);
ma_bool_t check_today_last_day(ma_task_time_t today, ma_task_time_t end_date);
ma_bool_t is_last_day_later(ma_task_time_t end_date, ma_task_time_t next_date);
void get_current_month_date(ma_task_time_t start_date,  ma_uint32_t months, ma_uint16_t day, ma_task_time_t *new_date);
void get_next_month_date_after(ma_task_time_t start_date,  ma_uint32_t months, ma_uint16_t day, ma_task_time_t *new_date);
void get_current_month_dow(ma_task_time_t start_date, ma_uint32_t months, ma_uint16_t which_week, ma_uint32_t days_of_week, ma_task_time_t *new_date);
void get_next_month_dow_date_after(ma_task_time_t start_date, ma_uint32_t months, ma_uint16_t which_week, ma_uint32_t days_of_week, ma_task_time_t *new_date);
ma_error_t convert_task_to_variant(ma_task_t *task, ma_variant_t **task_variant);
ma_error_t convert_variant_to_task(ma_variant_t *task_details, ma_task_t *task);
void advance_date_by_seconds(ma_int64_t seconds, ma_task_time_t tdate, ma_task_time_t *new_date);
void advance_current_date_by_seconds(ma_int64_t seconds, ma_task_time_t *new_date);
ma_error_t set_settings(const char *section, const char *name, ma_variant_t *value, ma_variant_t **ret_variant);
ma_error_t get_settings(ma_variant_t *collection, const char *section, const char *name, ma_variant_t **value);
void convert_local_to_utc(ma_task_time_t local, ma_task_time_t *gm);
void convert_utc_to_local(ma_task_time_t gm, ma_task_time_t *local);
void adjust_date_to_utc_zone(ma_task_time_t *date);
ma_bool_t validate_end_date(ma_task_time_t date);
void compute_date_diff_seconds(ma_task_time_t end_date, ma_task_time_t begin_date, ma_int64_t *diff);
void ma_compute_until_duration(ma_task_time_t start_date, ma_task_interval_t until_time, ma_task_interval_t *duration);
ma_bool_t ma_date_validator(int day, int month, int year);
ma_bool_t ma_time_validator(int hour, int minute, int seconds);
int ma_month_of_year(int month);
ma_bool_t ma_check_leap_year(int year);
void ma_mktime(ma_task_time_t tdate, time_t *tt);
void ma_convert_time_t_to_task_time(time_t tt, ma_task_time_t *new_date);
void ma_strtime(ma_task_time_t date, char *pdate);
void ma_strtime_to_task_time(const char *pdate, ma_task_time_t *date);
MA_CPP(})
#endif // MA_SCHEDULER_UTILS_H_INCLUDED


