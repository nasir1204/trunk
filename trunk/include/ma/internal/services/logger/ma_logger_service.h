#ifndef MA_LOGGER_SERVICE_H_INCLUDED
#define MA_LOGGER_SERVICE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_context.h"

MA_CPP(extern "C" {)
/* logger service class */
typedef struct ma_logger_service_s ma_logger_service_t, *ma_logger_service_h;

/* logger service ctor */
MA_LOGGER_API ma_error_t ma_logger_service_create(ma_context_t *context, ma_logger_service_t **service);

/* logger service get version */
MA_LOGGER_API ma_error_t ma_logger_service_get_version(ma_uint32_t *version);

/* logger service dtor */
MA_LOGGER_API ma_error_t ma_logger_service_release(ma_logger_service_t *service);

/* logger service start */
MA_LOGGER_API ma_error_t ma_logger_service_start(ma_logger_service_t *service);

/* logger service stop */
MA_LOGGER_API ma_error_t ma_logger_service_stop(ma_logger_service_t *service);

MA_CPP(})

//#include "ma/dispatcher/ma_logger_service_dispatcher.h"
#endif /* MA_LOGGER_SERVICE_H_INCLUDED */

