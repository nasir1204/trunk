#ifndef MA_AAC_SERVICE_H_INCLUDED
#define MA_AAC_SERVICE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/services/ma_service.h"

MA_CPP(extern "C" {)

#define MA_AAC_SERVICE_API /* declspec blah blah */

    
    /* Service dependencies: [logger, msgbus] */
MA_AAC_SERVICE_API ma_error_t ma_aac_service_create(char const *service_name, ma_service_t **service);

/* This service's bus address */
#define MA_AAC_SERVICE_NAME "ma.aac_service"

#define MA_AAC_DRIVER_RELOAD "schedule_aac_reload"

MA_CPP(})

#endif /* MA_AAC_SERVICE_H_INCLUDED */
