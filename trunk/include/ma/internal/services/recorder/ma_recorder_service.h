#ifndef MA_RECORDER_SERVICE_H_INCLUDED
#define MA_RECORDER_SERVICE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/services/ma_service.h"
#include "ma/internal/utils/recorder/ma_recorder.h"

MA_CPP(extern "C" {)
/* recorder service class */
typedef struct ma_recorder_service_s ma_recorder_service_t, *ma_recorder_service_h;

// recorder service ctor
MA_RECORDER_API ma_error_t ma_recorder_service_create(const char *service_name, ma_service_t **service);

MA_RECORDER_API ma_error_t ma_recorder_service_get_recorder(ma_recorder_service_t *recorder, ma_recorder_t **precorder);
MA_CPP(})

#include "ma/dispatcher/ma_recorder_service_dispatcher.h"
#endif /* MA_RECORDER_SERVICE_H_INCLUDED */

