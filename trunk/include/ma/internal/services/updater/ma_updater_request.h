#ifndef MA_UPDATER_REQUEST_H_INCLUDED
#define MA_UPDATER_REQUEST_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_variant.h"

#include "ma/internal/defs/ma_updater_defs.h"
MA_CPP(extern "C" {)

typedef struct ma_updater_request_s ma_updater_request_t, *ma_updater_request_h;

typedef enum ma_updater_request_type_e  {
    MA_UPDATER_REQUEST_TYPE_START = 0,
    MA_UPDATER_REQUEST_TYPE_STOP = 1,
}ma_updater_request_type_t;

MA_UPDATER_API ma_error_t ma_updater_request_create(ma_updater_request_type_t request_type, ma_updater_initiator_type_t initiator_type, ma_updater_request_t **request);

MA_UPDATER_API ma_error_t ma_updater_request_release(ma_updater_request_t *request);

MA_UPDATER_API ma_error_t ma_updater_request_get_request_type(ma_updater_request_t *request, ma_updater_request_type_t *request_type);

MA_UPDATER_API ma_error_t ma_updater_request_get_initiator_type(ma_updater_request_t *request, ma_updater_initiator_type_t *initiator_type);

/* Used for task updates and deployment */
MA_UPDATER_API ma_error_t ma_updater_request_set_task_id(ma_updater_request_t *request, const char *task_id);

MA_UPDATER_API ma_error_t ma_updater_request_get_task_id(ma_updater_request_t *request, const char **task_id);

/*Helper functions */
MA_UPDATER_API ma_error_t ma_updater_request_from_variant(ma_variant_t *variant, ma_updater_request_t **request);

MA_UPDATER_API ma_error_t ma_updater_request_to_variant(ma_updater_request_t *request, ma_variant_t **variant);

MA_CPP(})

#endif  /* MA_UPDATER_REQUEST_H_INCLUDED */
