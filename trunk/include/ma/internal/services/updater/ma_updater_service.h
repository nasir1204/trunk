#ifndef MA_UPDATER_SERVICE_H_INCLUDED
#define MA_UPDATER_SERVICE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/services/ma_service.h"

MA_CPP(extern "C" {)

typedef struct ma_updater_service_s ma_updater_service_t, *ma_updater_service_h;

struct ma_table_s;

/**
 *  @brief Create updater service
 *
 */
MA_UPDATER_API ma_error_t ma_updater_service_create(const char *service_name, ma_service_t **updater_service);

MA_CPP(})

#endif  /* MA_UPDATER_SERVICE_H_INCLUDED */


