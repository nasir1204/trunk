#ifndef MA_UPDATER_REQUEST_PARAM_H_INCLUDED
#define MA_UPDATER_REQUEST_PARAM_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_variant.h"

#include "ma/internal/ma_strdef.h"

MA_CPP(extern "C" {)

typedef struct ma_updater_request_param_s ma_updater_request_param_t, *ma_updater_request_param_h;

typedef enum ma_updater_request_type_e  {
    MA_UPDATER_REQUEST_TYPE_START = 0,
    MA_UPDATER_REQUEST_TYPE_STOP     ,
	MA_UPDATER_REQUEST_TYPE_STATUS_CHECK
}ma_updater_request_type_t;

struct ma_updater_request_param_s {
    ma_updater_request_type_t         request_type;
    ma_updater_initiator_type_t       initiator_type;
    char                              task_id[MA_MAX_TASKID_LEN+1];
};

/*Helper functions */
ma_error_t ma_updater_request_param_from_variant(ma_variant_t *variant, ma_updater_request_param_t **request);

ma_error_t ma_updater_request_param_to_variant(ma_updater_request_param_t *request, ma_variant_t **variant);

MA_CPP(})

#endif  /* MA_UPDATER_REQUEST_PARAM_H_INCLUDED */



