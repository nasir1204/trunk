#ifndef MA_UPDATER_UTILS_H_INCLUDED
#define MA_UPDATER_UTILS_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_variant.h"
#include "ma/internal/services/updater/ma_updater_request_param.h"

MA_CPP(extern "C" {)

/*Helper functions */
ma_error_t ma_updater_request_param_from_variant(ma_variant_t *variant, ma_updater_request_param_t **request);

ma_error_t ma_updater_request_param_to_variant(ma_updater_request_param_t *request, ma_variant_t **variant);

void ma_updater_request_param_release(ma_updater_request_param_t *self);

MA_CPP(})

#endif  /* MA_UPDATER_UTILS_H_INCLUDED */
