#ifndef MA_LOGGER_DB_H_INCLUDED
#define MA_LOGGER_DB_H_INCLUDED

#include "ma/internal/utils/database/ma_db.h"
#include "ma/internal/utils/database/ma_db_recordset.h"
#include "ma/internal/utils/database/ma_db_statement.h"

MA_CPP(extern "C" {)

ma_error_t ma_logger_db_schema_create(ma_db_t *db_handle);

ma_error_t ma_logger_db_add_log(ma_db_t *db_handle, const char *datetime, const char *severity, const char *facility, const char *message);

ma_error_t ma_logger_db_get_logs(ma_db_t *db_handle, ma_db_recordset_t **db_record);

ma_error_t ma_logger_db_adjust_logs(ma_db_t *db_handle, ma_int32_t records_size);

MA_CPP(})
#endif  /* MA_LOGGER_DB_H_INCLUDED */

