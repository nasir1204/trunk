#ifndef MA_IO_SERVICE_H_INCLUDED
#define MA_IO_SERVICE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/services/ma_service.h"

MA_CPP(extern "C" {)

typedef struct ma_io_service_s ma_io_service_t, *ma_io_service_h;


struct ma_event_loop_s;

MA_IO_SERVICE_API ma_error_t ma_io_service_create(char const *service_name, ma_logger_t *logger, ma_io_service_t **io_service);

MA_IO_SERVICE_API ma_error_t ma_io_service_run(ma_io_service_t *io_service) ;

MA_IO_SERVICE_API ma_error_t ma_io_service_stop(ma_io_service_t *io_service) ; 

MA_IO_SERVICE_API ma_error_t ma_io_service_shutdown(ma_io_service_t *io_service) ; 

MA_IO_SERVICE_API ma_error_t ma_io_service_release(ma_io_service_t *io_service) ;

MA_IO_SERVICE_API ma_error_t  ma_io_service_get_event_loop(ma_io_service_t *io_service, struct ma_event_loop_s **event_loop);

MA_IO_SERVICE_API ma_error_t  ma_io_service_get_service(ma_io_service_t *io_service, const char *service_name, ma_service_t **service);

MA_CPP(})

#endif /* MA_IO_SERVICE_H_INCLUDED */
