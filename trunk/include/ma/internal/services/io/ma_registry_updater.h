#ifndef MA_REGISTRY_UPDATER_H_INCLUDED
#define MA_REGISTRY_UPDATER_H_INCLUDED

#include "ma/ma_msgbus.h"
#include "ma/ma_message.h"

static ma_error_t ma_update_registry_publisher(ma_msgbus_t *msgbus) {
	if(msgbus) { 
		ma_message_t *message = NULL;
		if(MA_OK == ma_message_create(&message)) {
			ma_msgbus_publish(msgbus, MA_IO_MSG_TYPE_UPDATE_REG_INFO, MSGBUS_CONSUMER_REACH_OUTPROC, message);
			ma_message_release(message);
		}
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

#endif /*MA_REGISTRY_UPDATER_H_INCLUDED*/

