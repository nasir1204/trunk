#ifndef MA_IO_SERVICE_INTERNAL_H_INCLUDED
#define MA_IO_SERVICE_INTERNAL_H_INCLUDED

#include "ma/ma_common.h"

#include "ma/internal/services/ma_service.h"
#include "ma/internal/services/ma_service_manager.h"

#include "ma/internal/ma_strdef.h"

/* Objects */
#include "ma/internal/utils/context/ma_context.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/utils/configurator/ma_configurator_utils.h"
#include "ma/logger/ma_file_logger.h"
#include "ma/internal/services/ma_policy_settings_bag.h"
#include "ma/internal/utils/platform/ma_system_property.h"
#include "ma/policy/ma_policy_uri.h"
#include "ma/internal/utils/network/ma_net_client_service.h"

/* clients */
#include "ma/property/ma_property.h"
#include "ma/policy/ma_policy.h"
#include "ma/internal/clients/ma/ma_client_internal.h"

#include "ma/ma_msgbus.h"
#include "ma/ma_message.h"

#include "ma_spipe_common_property_manager.h"
#include "ma_spipe_misc_handler.h"
#include "ma_relay_handler.h"

MA_CPP(extern "C" {)

struct ma_io_service_s {
    ma_service_t                        base_service;

    /* main context */
    ma_context_t                        *context;

    /* logger , copy*/
    ma_logger_t                         *logger;

    /* configurator */
    ma_configurator_t                   *configurator;

    /* policy settings bag */
    ma_policy_settings_bag_t            *policy_settings_bag;

    /* system property */
    ma_system_property_t                *system_property;

    /* msgbus */
    ma_msgbus_t                         *msgbus;

    /* services manager */
    ma_service_manager_t                service_manager;

    /* spipe property common manager */
    ma_spipe_common_property_manager_t  *spipe_common_mgr;

	/* spipe misc handler */
    ma_spipe_misc_handler_t				*spipe_misc_handler;

	/* spipe misc handler */
    ma_relay_handler_t					*relay_handler;

    /* MA client */
    ma_client_t                         *client;

    /* server running */
    ma_msgbus_server_t                  *io_server;

    /* subscriber */
    ma_msgbus_subscriber_t              *io_subscriber;

    /* agent mode */
    ma_int32_t                          agent_mode;

    uv_prepare_t                        prepare_event;

    ma_table_t                          *server_settings_tb;
};


/* I/O server handler */
ma_error_t ma_io_service_server_handler(ma_msgbus_server_t *server, ma_message_t *msg, void *cb_data, ma_msgbus_client_request_t *c_request);

/* I/O subscriber handler */
ma_error_t ma_io_service_subscriber_handler(const char *topic, ma_message_t *msg, void *cb_data);

/* MA properties handling */
ma_error_t agent_property_provider_cb(ma_client_t *client, const char *software_id, ma_property_bag_t *property_bag, void *cb_data);
ma_error_t system_property_provider_cb(ma_client_t *client, const char *software_id, ma_property_bag_t *property_bag, void *cb_data);


MA_CPP(})

#endif /* MA_IO_SERVICE_INTERNAL_H_INCLUDED */
