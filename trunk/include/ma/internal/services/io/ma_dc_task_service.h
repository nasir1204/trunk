#ifndef MA_DC_TASK_SERVICE_H_INCLUDED
#define MA_DC_TASK_SERVICE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/services/ma_service.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/defs/ma_dc_task_service_defs.h"

MA_CPP(extern "C" {)

ma_error_t ma_dc_task_service_create(const char *service_name, ma_service_t **service);

MA_CPP(})

#endif /* MA_TASK_SERVICE_H_INCLUDED */
