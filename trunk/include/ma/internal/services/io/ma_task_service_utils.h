#ifndef MA_TASK_SERVICE_UTILS_H_INCLUDED
#define MA_TASK_SERVICE_UTILS_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/services/scheduler/ma_task_internal.h"

MA_CPP(extern "C" {)

struct ma_context_s;

ma_error_t ma_task_service_task_create(struct ma_context_s *context, const char *task_id, const char *task_name, const char *task_type, const char *creator, ma_int16_t interval, ma_int64_t begin_after_secs, ma_bool_t enabled);

ma_error_t ma_task_service_task_update(struct ma_context_s *context, const char *task_id, const char *task_name, const char *task_type, ma_int16_t interval, ma_bool_t enabled);

ma_error_t ma_task_service_task_set_status(struct ma_context_s *context, const char *task_id, ma_task_exec_status_t status);

ma_error_t ma_task_service_task_start(struct ma_context_s *context, const char *task_id, const char *task_name, const char *task_type);

ma_error_t ma_task_service_task_stop(struct ma_context_s *context, const char *task_id, const char *task_name, const char *task_type);

void ma_telemtry_deployment_task_run(struct ma_context_s *context);

ma_error_t ma_task_schedule_global_update(ma_message_t *msg, struct ma_context_s *context);

MA_CPP(})

#endif /* MA_TASK_SERVICE_UTILS_H_INCLUDED */


