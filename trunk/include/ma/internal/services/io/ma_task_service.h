#ifndef MA_TASK_SERVICE_H_INCLUDED
#define MA_TASK_SERVICE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_message.h"
#include "ma/internal/services/ma_service.h"
#include "ma/internal/defs/ma_object_defs.h"

MA_CPP(extern "C" {)

typedef struct ma_task_service_s {
    ma_service_t            base;

    ma_msgbus_subscriber_t  *subscriber;

    ma_context_t            *context;

    /* property collection timeout */
    ma_int32_t             property_collection_timeout ;

    /* Policy enforce timeout */
    ma_int32_t             policy_enforce_timeout;
    
    /* Event trigger interval */
    ma_int32_t             event_trigger_interval;

	/* p2p purge task interval */
	ma_bool_t				is_p2p_serving_enabled ;
	ma_int32_t				p2p_purge_interval ;

	ma_bool_t				is_sa_enabled ;

	/* SA repo purge task interval */
	ma_bool_t				is_sa_repo_task_enabled ;
	ma_int32_t				sa_repo_purge_interval ;

    ma_bool_t             is_agent_managed;

	ma_bool_t             is_telemtry_task_enabled;
} ma_task_service_t;

ma_error_t ma_task_service_create(char const *service_name, ma_service_t **service);

/* Internal functions */
ma_error_t handle_epo_tasks(ma_task_service_t *service, const char *task_version);

#define MA_TASK_SERVICE_CREATOR_NAME_STR			    "ma.io.task.service"

/* Propert collection task info */
#define MA_PROPERTY_COLLECT_TASK_ID_STR                     "ma.property.collect.timeout.task.id"
#define MA_PROPERTY_COLLECT_TASK_NAME_STR                   "ma.property.collect.timeout.task.name"
#define MA_PROPERTY_COLLECT_TASK_TYPE_STR                   "ma.property.collect.timeout.task.type"

/* Policy Enforment task info */
#define MA_POLICY_ENFORCE_TASK_ID_STR                   "ma.policy.enforce.timeout.task.id"
#define MA_POLICY_ENFORCE_TASK_NAME_STR                 "ma.policy.enforce.timeout.task.name"
#define MA_POLICY_ENFORCE_TASK_TYPE_STR                 "ma.policy.enforce.timeout.task.type"

/* Event send task info */
#define MA_PRIORITY_EVENT_TRIGGER_TASK_ID_STR           "ma.event.priority.event.send.task.id"
#define MA_PRIORITY_EVENT_TRIGGER_TASK_NAME_STR         "ma.event.priority.event.send.task.name"
#define MA_PRIORITY_EVENT_TRIGGER_TASK_TYPE_STR         "ma.event.priority.event.send.task.type"

/* P2p content purge task info */
#define MA_P2P_CONTENT_PURGE_TASK_ID_STR						   "ma.p2p.content.purge.task.id"
#define MA_P2P_CONTENT_PURGE_TASK_NAME_STR						   "ma.p2p.content.purge.task.name"
#define MA_P2P_CONTENT_PURGE_TASK_TYPE_STR						   "ma.p2p.content.purge.task.type"

/* SuperAgent Repo purge task info */
#define MA_HTTP_SERVER_SUPERAGENT_REPO_PURGE_TASK_ID_STR					"ma.superagent.repo.purge.task.id"
#define MA_HTTP_SERVER_SUPERAGENT_REPO_PURGE_TASK_NAME_STR					"ma.superagent.repo.purge.task.name"
#define MA_HTTP_SERVER_SUPERAGENT_REPO_PURGE_TASK_TYPE_STR					"ma.superagent.repo.purge.task.type"

/* SuperAgent lazycache flush task info */
#define MA_HTTP_SERVER_SUPERAGENT_LAZYCACHE_FLUSH_TASK_ID_STR				"ma.superagent.lazycache.flush.task.id"


#define MA_AGENT_STATS_COLLECT_TASK_TYPE_STR					"Statistics"
#define MA_AGENT_STATS_COLLECT_TASK_NAME_STR					"Collect All"

/* Agent wakeup task which is coming from ePO */
#define MA_AGENT_WAKEUP_TASK_TYPE_STR                   "Wakeup"

#define MA_AGENT_WAKEUP_TASK_SETTINGS_SECTION_STR           "General"
#define MA_AGENT_WAKEUP_TASK_USE_CUSTOM_ASCI_SETTING_STR    "bCustomASCISettings"
#define MA_AGENT_WAKEUP_TASK_ASCI_FLAG_STR                  "ASCIFlags"

MA_CPP(})

#endif /* MA_TASK_SERVICE_H_INCLUDED */
