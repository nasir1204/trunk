#ifndef MA_P2P_SERVICE_H_INCLUDED
#define MA_P2P_SERVICE_H_INCLUDED

#include "ma/ma_common.h"

struct ma_service_s ;

MA_CPP(extern "C" {)

typedef struct ma_p2p_service_s ma_p2p_service_t, *ma_p2p_service_h ;

/*!
 * creates an instance of the p2p service
 */
MA_P2P_API ma_error_t ma_p2p_service_create(char const *service_name, struct ma_service_s **service) ;

MA_CPP(})

#endif  /* MA_P2P_SERVICE_H_INCLUDED */
