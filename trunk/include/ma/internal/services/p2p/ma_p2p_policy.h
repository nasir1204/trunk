#ifndef MA_P2P_POLICY_H_INCLUDED
#define MA_P2P_POLICY_H_INCLUDED

#include "ma/internal/services/p2p/ma_p2p_service.h"

MA_CPP(extern "C" {)

typedef struct ma_p2p_policy_s ma_p2p_policy_t, *ma_p2p_policy_h ;

ma_error_t ma_p2p_policy_create(ma_p2p_service_t *service, ma_p2p_policy_t **p2p_policy) ;

ma_error_t ma_p2p_policy_update(ma_p2p_policy_t *self) ;

ma_error_t ma_p2p_policy_release(ma_p2p_policy_t *self) ;

MA_CPP(})

#endif  /* MA_P2P_POLICY_H_INCLUDED */

