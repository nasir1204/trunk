#ifndef MA_P2P_DICOVERY_HANDLER_H_INCLUDED
#define MA_P2P_DICOVERY_HANDLER_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/services/udp_server/ma_udp_msg_handler.h"
#include "ma/internal/defs/ma_object_defs.h"

MA_CPP(extern "C" {)

ma_error_t ma_p2p_discovery_handler_create(ma_context_t *context, ma_udp_msg_handler_t **discovery_handler) ;

ma_error_t ma_p2p_discovery_handler_configure(ma_udp_msg_handler_t *self, ma_context_t *context) ;

MA_CPP(})

#endif /* MA_P2P_DICOVERY_HANDLER_H_INCLUDED */
