#ifndef MA_P2P_CONTENT_H_INCLUDED
#define MA_P2P_CONTENT_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_buffer.h"

MA_CPP(extern "C" {)

typedef struct ma_p2p_content_s ma_p2p_content_t, *ma_p2p_content_h ;

typedef enum ma_p2p_content_format_e {
    MA_P2P_CONTENT_FORMAT_FILE = 0,
    MA_P2P_CONTENT_FORMAT_BUFFER
} ma_p2p_content_format_t ;


typedef enum ma_p2p_content_add_type_e {
	MA_P2P_CONTENT_NONE = 0,
    MA_P2P_CONTENT_COPY,
    MA_P2P_CONTENT_MOVE,
	MA_P2P_CONTENT_END
} ma_p2p_content_add_type_t ;


ma_error_t ma_p2p_content_create(ma_p2p_content_t **p2p_content) ;

ma_error_t ma_p2p_content_release(ma_p2p_content_t *self) ;

ma_error_t ma_p2p_content_set_hash(ma_p2p_content_t *self, const char *hash) ;

ma_error_t ma_p2p_content_set_content_type(ma_p2p_content_t *self, const char *type) ;

ma_error_t ma_p2p_content_set_base_dir(ma_p2p_content_t *self, const char *base_dir) ;

ma_error_t ma_p2p_content_set_urn(ma_p2p_content_t *self, const char *urn) ;

ma_error_t ma_p2p_content_set_content_size(ma_p2p_content_t *self, size_t size) ;

ma_error_t ma_p2p_content_set_contributor(ma_p2p_content_t *self, const char *contributor) ;

ma_error_t ma_p2p_content_set_owned(ma_p2p_content_t *self, ma_bool_t owned) ;

ma_error_t ma_p2p_content_set_content_format(ma_p2p_content_t *self, ma_p2p_content_format_t format) ;

ma_error_t ma_p2p_content_set_buffer(ma_p2p_content_t *self, ma_buffer_t *buffer) ;

ma_error_t ma_p2p_content_set_hit_count(ma_p2p_content_t *self, ma_uint32_t hit_count) ;

ma_error_t ma_p2p_content_set_time_last_access(ma_p2p_content_t *self, const char *time_last_access) ;

ma_error_t ma_p2p_content_set_time_created(ma_p2p_content_t *self, const char *time_created) ;

ma_error_t ma_p2p_content_set_file_op(ma_p2p_content_t *self, ma_p2p_content_add_type_t file_op) ;


ma_error_t ma_p2p_content_get_hash(ma_p2p_content_t *self, const char **hash) ;

ma_error_t ma_p2p_content_get_content_type(ma_p2p_content_t *self, const char **type) ;

ma_error_t ma_p2p_content_get_base_dir(ma_p2p_content_t *self, const char **base_dir) ;

ma_error_t ma_p2p_content_get_urn(ma_p2p_content_t *self, const char **urn) ;

ma_error_t ma_p2p_content_get_content_size(ma_p2p_content_t *self, size_t *size) ;

ma_error_t ma_p2p_content_get_contributor(ma_p2p_content_t *self, const char **contributor) ;

ma_error_t ma_p2p_content_get_owned(ma_p2p_content_t *self, ma_bool_t *owned) ;

ma_error_t ma_p2p_content_get_content_format(ma_p2p_content_t *self, ma_p2p_content_format_t *format) ;

ma_error_t ma_p2p_content_get_buffer(ma_p2p_content_t *self, ma_buffer_t **buffer) ;

ma_error_t ma_p2p_content_get_hit_count(ma_p2p_content_t *self, ma_uint32_t *hit_count) ;

ma_error_t ma_p2p_content_get_time_last_access(ma_p2p_content_t *self, const char **time_last_access) ;

ma_error_t ma_p2p_content_get_time_created(ma_p2p_content_t *self, const char **time_created) ;

ma_error_t ma_p2p_content_get_file_op(ma_p2p_content_t *self, ma_p2p_content_add_type_t *file_op) ;


MA_CPP(})

#endif /* MA_P2P_CONTENT_H_INCLUDED */
