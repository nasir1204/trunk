#ifndef MA_P2P_DB_MANAGER_H_INCLUDED
#define MA_P2P_DB_MANAGER_H_INCLUDED

#include "ma/internal/utils/database/ma_db.h"
#include "ma/internal/utils/database/ma_db_recordset.h"
#include "ma/internal/utils/database/ma_db_statement.h"

#include "ma/internal/services/p2p/ma_p2p_content.h"

MA_CPP(extern "C" {)

ma_error_t ma_p2p_db_create_schema(ma_db_t *db) ;

ma_error_t ma_p2p_db_add_content(ma_db_t *db, ma_p2p_content_t *p2p_content) ;

ma_error_t ma_p2p_db_reset_state(ma_db_t *db_handle) ;

ma_error_t ma_p2p_db_remove_content(ma_db_t *db_handle, ma_p2p_content_t *p2p_content) ;

ma_error_t ma_p2p_db_erase_content(ma_db_t *db_handle) ;

ma_error_t ma_p2p_db_erase_content_by_contributor(ma_db_t *db_handle, const char *contributor) ;

ma_error_t ma_p2p_db_remove_content_by_hash(ma_db_t *db_handle, const char *content_hash) ;

ma_error_t ma_p2p_db_remove_content_by_hit_count(ma_db_t *db_handle, ma_uint32_t hit_count) ;

ma_error_t ma_p2p_db_remove_content_by_time_created(ma_db_t *db_handle, const char *time) ;

ma_error_t ma_p2p_db_remove_content_by_time_lat_access(ma_db_t *db_handle, const char *time) ;

ma_error_t ma_p2p_db_is_content_exists(ma_db_t *db_handle, const char *content_hash, ma_bool_t *is_exists) ;

ma_error_t ma_p2p_db_get_urn_for_hash(ma_db_t *db_handle, const char *content_hash, char **urn) ;

ma_error_t ma_p2p_db_get_content_by_hash(ma_db_t *db_handle, const char *content_hash, ma_p2p_content_t **p2p_content) ;

ma_error_t ma_p2p_db_update_hit_count_and_access_time(ma_db_t *db_handle, const char *content_hash) ;

ma_error_t ma_p2p_db_get_filepath_for_hash(ma_db_t *db_handle, const char *content_hash, ma_int32_t *state, char **file_path) ;

ma_error_t ma_p2p_db_set_content_to_purge(ma_db_t *db_handle, ma_int64_t purge_size) ;

ma_error_t ma_p2p_db_unset_content_to_purge(ma_db_t *db_handle) ;

ma_error_t ma_p2p_db_get_content_to_purge(ma_db_t *db_handle, ma_db_recordset_t **content_set) ;

ma_error_t ma_p2p_db_set_content_to_purge_by_contributor(ma_db_t *db_handle, ma_int64_t purge_size, const char *contributor) ;

ma_error_t ma_p2p_db_unset_content_to_purge_by_contributor(ma_db_t *db_handle, const char *contributor) ;

ma_error_t ma_p2p_db_get_content_to_purge_by_contributor(ma_db_t *db_handle, const char *contributor, ma_db_recordset_t **content_set) ;

ma_error_t ma_p2p_db_get_aggregated_size(ma_db_t *db_handle, ma_int64_t *size) ;

ma_error_t ma_p2p_db_get_aggregated_size_by_contributor(ma_db_t *db_handle, const char *contributor, ma_int64_t *size) ;

ma_error_t ma_p2p_db_update_base_dir(ma_db_t *db_handle, const char *base_dir) ;

ma_error_t ma_p2p_db_update_base_dir_by_contributor(ma_db_t *db_handle, const char *contributor, const char *base_dir) ;

ma_error_t ma_p2p_db_remove_content_by_urn(ma_db_t *db_handle, const char *contributor, const char *urn, ma_bool_t is_dir) ;

ma_error_t ma_p2p_db_get_content_state(ma_db_t *db_handle, const char *hash, ma_int32_t *state) ;

ma_error_t ma_p2p_db_set_content_state_serving(ma_db_t *db_handle, const char *hash) ;

ma_error_t ma_p2p_db_unset_content_state_serving(ma_db_t *db_handle, const char *hash) ;

ma_error_t ma_p2p_db_is_content_movable(ma_db_t *db_handle, ma_bool_t *is_movable) ;

MA_CPP(})

#endif /* MA_P2P_CONTENT_MANAGER_H_INCLUDED */

