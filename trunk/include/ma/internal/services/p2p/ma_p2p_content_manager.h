#ifndef MA_P2P_CONTENT_MANAGER_H_INCLUDED
#define MA_P2P_CONTENT_MANAGER_H_INCLUDED

#include "ma/internal/services/p2p/ma_p2p_service.h"
#include "ma/internal//services/p2p/ma_p2p_content.h" 

MA_CPP(extern "C" {)

typedef struct ma_p2p_content_manager_s     ma_p2p_content_manager_t,   *ma_p2p_content_manager_h ;

ma_error_t ma_p2p_content_manager_create(ma_p2p_service_t *p2p_service, ma_p2p_content_manager_t **content_manager) ;

ma_error_t ma_p2p_content_manager_update(ma_p2p_content_manager_t *self) ;

ma_error_t ma_p2p_content_manager_add_content(ma_p2p_content_manager_t *self, ma_uint16_t file_op, ma_p2p_content_t *content) ;

ma_error_t ma_p2p_content_manager_remove_content(ma_p2p_content_manager_t *self, ma_p2p_content_t *content ) ;

ma_error_t ma_p2p_content_manager_get_content(ma_p2p_content_manager_t *self, const char *hash, ma_p2p_content_t **content ) ;

ma_error_t ma_p2p_content_manager_purge_content(ma_p2p_content_manager_t *self, ma_bool_t purge_task) ;

ma_error_t ma_p2p_content_manager_release(ma_p2p_content_manager_t *self) ;

MA_CPP(})

#endif /* MA_P2P_CONTENT_MANAGER_H_INCLUDED */
