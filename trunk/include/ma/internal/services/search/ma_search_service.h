#ifndef MA_SEARCH_SERVICE_H_INCLUDED
#define MA_SEARCH_SERVICE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/services/ma_service.h"
#include "ma/internal/utils/search/ma_search.h"

MA_CPP(extern "C" {)
/* search service class */
typedef struct ma_search_service_s ma_search_service_t, *ma_search_service_h;

// search service ctor
MA_SEARCH_API ma_error_t ma_search_service_create(const char *service_name, ma_service_t **service);

MA_SEARCH_API ma_error_t ma_search_service_get_search(ma_search_service_t *service, ma_search_t **search);

MA_SEARCH_API ma_error_t ma_search_service_get_msgbus(ma_search_service_t *service, ma_msgbus_t **msgbus);

MA_CPP(})

#include "ma/dispatcher/ma_search_service_dispatcher.h"
#endif /* MA_SEARCH_SERVICE_H_INCLUDED */

