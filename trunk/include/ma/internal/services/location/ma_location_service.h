#ifndef MA_LOCATION_SERVICE_H_INCLUDED
#define MA_LOCATION_SERVICE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/services/ma_service.h"
#include "ma/internal/utils/location/ma_location.h"

MA_CPP(extern "C" {)
/* location service class */
typedef struct ma_location_service_s ma_location_service_t, *ma_location_service_h;

// location service ctor
MA_LOCATION_API ma_error_t ma_location_service_create(const char *service_name, ma_service_t **service);

MA_LOCATION_API ma_error_t ma_location_service_get_location(ma_location_service_t *location, ma_location_t **plocation);
MA_CPP(})

#include "ma/dispatcher/ma_location_service_dispatcher.h"
#endif /* MA_LOCATION_SERVICE_H_INCLUDED */

