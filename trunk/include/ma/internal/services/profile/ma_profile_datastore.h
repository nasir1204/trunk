#ifndef MA_PROFILE_DATASTORE_H_INCLUDED
#define MA_PROFILE_DATASTORE_H_INCLUDED

#include "ma/ma_common.h"


MA_CPP(extern "C" {)
#if 0
// read all tasks from ds_path and add into folder
ma_error_t ma_profile_datastore_read(ma_profile_t *profile, ma_taskfolder_t * folder, ma_ds_t *datastore, const char *ds_path);

// write task into ds under ds_path
ma_error_t ma_profile_datastore_write(ma_profile_t *profile, ma_task_t *task, ma_ds_t *datastore, const char *ds_path);


ma_error_t ma_profile_datastore_remove_task(ma_task_t *task, ma_ds_t *datastore, const char *ds_path);

ma_error_t ma_profile_datastore_clear(ma_ds_t *datastore, const char *ds_path);
#endif

MA_CPP(})


#endif /* MA_PROFILE_DATASTORE_H_INCLUDED */

