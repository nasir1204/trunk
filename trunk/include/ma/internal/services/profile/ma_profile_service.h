#ifndef MA_PROFILE_SERVICE_H_INCLUDED
#define MA_PROFILE_SERVICE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/services/ma_service.h"
#include "ma/internal/utils/profiler/ma_profiler.h"

MA_CPP(extern "C" {)
/* profile service class */
typedef struct ma_profile_service_s ma_profile_service_t, *ma_profile_service_h;

// profile service ctor
MA_PROFILE_API ma_error_t ma_profile_service_create(const char *service_name, ma_service_t **service);

MA_PROFILE_API ma_error_t ma_profile_service_get_profiler(ma_profile_service_t *service, ma_profiler_t **profiler);

MA_PROFILE_API ma_error_t ma_profile_service_get_msgbus(ma_profile_service_t *service, ma_msgbus_t **msgbus);

MA_CPP(})

#include "ma/dispatcher/ma_profile_service_dispatcher.h"
#endif /* MA_PROFILE_SERVICE_H_INCLUDED */

