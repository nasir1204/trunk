#ifndef MA_AGENT_POLICY_SETTINGS_BAG_H_INCLUDED
#define MA_AGENT_POLICY_SETTINGS_BAG_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/services/ma_policy_settings_bag.h"
#include "ma/internal/utils/database/ma_db.h"
#include "ma/policy/ma_policy_bag.h"
MA_CPP(extern "C" {)

/* MA policies settings bag , ease of use */
ma_error_t ma_agent_policy_settings_bag_create(ma_db_t *policy_db, ma_policy_settings_bag_t **bag) ;

ma_error_t ma_agent_policy_settings_bag_refresh(ma_policy_settings_bag_t *bag);

ma_policy_bag_t** ma_agent_policy_settings_bag_get_policy_bag(ma_policy_settings_bag_t *bag);

ma_error_t ma_agent_policy_settings_bag_destroy(ma_policy_settings_bag_t *bag) ;

MA_CPP(})

#endif /* MA_AGENT_POLICY_SETTINGS_BAG_H_INCLUDED */
