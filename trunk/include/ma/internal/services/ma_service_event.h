#ifndef MA_SERVICE_EVENT_H_INCLUDED
#define MA_SERVICE_EVENT_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/event/ma_event_client.h"

MA_CPP(extern "C" {)

struct ma_event_parameter_s{
	ma_uint32_t eventid;
	ma_event_severity_t severity;
	char *product_version;
	char *SourceUserName;
	char *ThreatCategory;
	char *ThreatType;
	char *ThreatName;
};

ma_error_t ma_service_event_generate(ma_client_t *client, struct ma_event_parameter_s *param);

MA_CPP(})

#endif
