#ifndef MA_CONSIGNMENT_SERVICE_H_INCLUDED
#define MA_CONSIGNMENT_SERVICE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/services/ma_service.h"
#include "ma/internal/utils/consignor/ma_consignor.h"

MA_CPP(extern "C" {)
/* consignment service class */
typedef struct ma_consignment_service_s ma_consignment_service_t, *ma_consignment_service_h;

// consignment service ctor
MA_PROFILE_API ma_error_t ma_consignment_service_create(const char *service_name, ma_service_t **service);

MA_PROFILE_API ma_error_t ma_consignment_service_get_consignor(ma_consignment_service_t *service, ma_consignor_t **consignor);

MA_PROFILE_API ma_error_t ma_consignment_service_get_msgbus(ma_consignment_service_t *service, ma_msgbus_t **msgbus);

MA_CPP(})

#include "ma/dispatcher/ma_consignment_service_dispatcher.h"
#endif /* MA_CONSIGNMENT_SERVICE_H_INCLUDED */

