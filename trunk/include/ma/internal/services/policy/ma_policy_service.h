#ifndef MA_POLICY_SERVICE_H_INCLUDED
#define MA_POLICY_SERVICE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/services/ma_service.h"

MA_CPP(extern "C" {)

typedef struct ma_policy_service_s ma_policy_service_t, *ma_policy_service_h;

MA_POLICY_API ma_error_t ma_policy_service_create(const char *service_name, ma_service_t **policy_service);

MA_CPP(})

#include "ma/internal/services/dispatcher/ma_policy_service_dispatcher.h"

#endif /* MA_POLICY_SERVICE_H_INCLUDED */


