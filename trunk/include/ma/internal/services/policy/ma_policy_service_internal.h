#ifndef MA_POLICY_SERVICE_INTERNAL_H_INCLUDED
#define MA_POLICY_SERVICE_INTERNAL_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_variant.h"
#include "ma/internal/utils/database/ma_db.h"

MA_CPP(extern "C" {)

/* need_full_policies -> MA_TRUE -> return URI list and policy data
					  -> MA_FALSE -> return only Policy URI list
*/
ma_error_t ma_policy_service_get_policies(ma_table_t *uri_table, ma_db_t *policy_db, size_t *policies_count, ma_variant_t **policy_data, ma_bool_t need_full_policies);

ma_error_t ma_policy_service_set_policies(ma_table_t *uri_table, ma_db_t *policy_db, ma_variant_t *policy_data);

MA_CPP(})
#endif  /* MA_POLICY_SERVICE_INTERNAL_H_INCLUDED */

