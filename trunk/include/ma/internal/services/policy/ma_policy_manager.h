#ifndef MA_POLICY_MANAGER_H_INCLUDED
#define MA_POLICY_MANAGER_H_INCLUDED

#include "ma/ma_msgbus.h"
#include "ma/internal/utils/context/ma_context.h"
#include "ma/internal/services/ahclient/ma_ah_client_service.h"
#include "ma/internal/services/policy/ma_policy_user_info.h"

MA_CPP(extern "C" {)

typedef struct ma_policy_manager_s ma_policy_manager_t, *ma_policy_manager_h ;

ma_error_t ma_policy_manager_create(ma_context_t *context, ma_policy_manager_t **policy_manager);

ma_spipe_decorator_t *ma_policy_manager_get_decorator(ma_policy_manager_t *policy_manager);

ma_spipe_handler_t *ma_policy_manager_get_handler(ma_policy_manager_t *policy_manager);

ma_error_t ma_policy_manager_raise_alert(ma_policy_manager_t *policy_manager, ma_bool_t force_policy_enforcement);

ma_error_t ma_policy_manager_abort(ma_policy_manager_t *policy_manager);

ma_error_t ma_policy_manager_release(ma_policy_manager_t *policy_manager);

ma_error_t ma_policy_manager_set_user_info(ma_policy_manager_t *policy_manager, ma_policy_user_info_t *user_info);

ma_bool_t ma_policy_manager_raise_alert_request_done(ma_policy_manager_t *policy_manager);

MA_CPP(})

#endif  /* MA_POLICY_MANAGER_H_INCLUDED */

