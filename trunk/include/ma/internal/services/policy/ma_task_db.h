#ifndef MA_TASK_DB_H_INCLUDED
#define MA_TASK_DB_H_INCLUDED
#include "ma/internal/utils/database/ma_db.h"
#include "ma/internal/utils/database/ma_db_recordset.h"
#include "ma/internal/utils/database/ma_db_statement.h"

MA_CPP(extern "C" {)

//DDL'S
#define CREATE_MET_TABLE_STMT "CREATE TABLE IF NOT EXISTS MA_EPO_TASK(TASK_OBJECT_ID TEXT NOT NULL, TASK_ID TEXT NOT NULL,TASK_NAME TEXT NOT NULL, TASK_DIGEST TEXT NOT NULL, TASK_TYPE TEXT NOT NULL, TASK_PRIORITY TEXT NOT NULL, TASK_DATA BLOB, TIMESTAMP TEXT NOT NULL, PRIMARY KEY (TASK_OBJECT_ID) ON CONFLICT REPLACE)"
#define CREATE_META_TABLE_STMT "CREATE TABLE IF NOT EXISTS MA_EPO_TASK_ASSIGNMENT (TASK_OBJECT_ID TEXT NOT NULL UNIQUE, META_PRODUCT TEXT NOT NULL, META_METHOD	TEXT, META_PARAM TEXT, PRIMARY KEY (TASK_OBJECT_ID) ON CONFLICT REPLACE, FOREIGN KEY (TASK_OBJECT_ID) REFERENCES MA_EPO_TASK(TASK_OBJECT_ID) ON UPDATE CASCADE ON DELETE CASCADE )"

//creates tables
ma_error_t db_add_task_instance(ma_db_t *db_handle) ;

//update
ma_error_t db_update_epo_task(ma_db_t *db_handle, const char *task_object_id, const char *time_stamp);

//insert 
ma_error_t db_add_epo_task(ma_db_t *db_handle, const char *task_object_id, const char *task_id, const char *task_name, const char *task_digest, 
                            const char *task_type, const char *task_priority, ma_variant_t *task_data, const char *time_stamp);
ma_error_t db_add_epo_task_assignment(ma_db_t *db_handle, const char *task_obj_id, const char *product, const char *method, const char *param ) ;

//delete
ma_error_t db_remove_epo_task (ma_db_t *db_handle, const char *task_object_id) ;
ma_error_t db_remove_epo_task_assignment (ma_db_t *db_handle, const char *task_object_id, const char *product, const char *method, const char *param ) ;

//project/fetch/get
ma_error_t db_get_epo_task(ma_db_t *db_handle, const char *task_object_id, ma_db_recordset_t **db_record) ;
ma_error_t db_get_epo_task_assignments(ma_db_t *db_handle, const char *task_object_id, const char *product, const char *method, const char *param, ma_db_recordset_t **db_record) ; 

/* Get all tasks based on time stamp */
ma_error_t db_get_all_epo_tasks(ma_db_t *db_handle, const char *timestamp, ma_db_recordset_t **db_record) ;

ma_error_t db_get_epo_task_data(ma_db_t *db_handle, const char *task_object_id, ma_db_recordset_t **db_record);





MA_CPP(})
#endif  //MA_TASK_DB_H_INCLUDED

