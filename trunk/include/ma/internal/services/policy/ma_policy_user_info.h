#ifndef MA_POLICY_USER_INFO_H_INCLUDED
#define MA_POLICY_USER_INFO_H_INCLUDED

#include "ma/ma_msgbus.h"
#include "ma/internal/utils/context/ma_context.h"

MA_CPP(extern "C" {)

typedef struct ma_policy_user_info_s ma_policy_user_info_t, *ma_policy_user_info_h ;

ma_error_t ma_policy_user_info_create(ma_context_t *context, ma_policy_user_info_t **user_info);

/* is_refresh_from_sensor:
   MA_TRUE -  get users list from sensor and merge it into existing list
   MA_FALSE - get users list from policy DB 
*/
ma_error_t ma_policy_user_info_refresh(ma_policy_user_info_t *user_info, ma_bool_t is_refresh_from_sensor);

ma_bool_t ma_policy_user_info_search(ma_policy_user_info_t *user_info, const char *user);

ma_bool_t ma_policy_user_info_add(ma_policy_user_info_t *user_info, const char *user);

ma_bool_t ma_policy_user_info_remove(ma_policy_user_info_t *user_info, const char *user);

ma_error_t ma_policy_user_info_release(ma_policy_user_info_t *user_info);

typedef ma_error_t (*ma_policy_user_info_foreach_cb_t)(const char *user, void *cb_data);

ma_error_t ma_policy_user_info_foreach(ma_policy_user_info_t *user_info, ma_policy_user_info_foreach_cb_t cb, void *cb_data);

MA_CPP(})

#endif  /* MA_POLICY_USER_INFO_H_INCLUDED */

