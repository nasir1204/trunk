#ifndef MA_POLICY_MANIFEST_H_INCLUDED
#define MA_POLICY_MANIFEST_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_variant.h"

MA_CPP(extern "C" {)

typedef struct ma_policy_manifest_s ma_policy_manifest_t, *ma_policy_manifest_h;

ma_error_t ma_policy_manifest_create(ma_policy_manifest_t **policy_manifest);

ma_error_t ma_policy_manifest_load(ma_policy_manifest_t *policy_manifest, unsigned char *manifest, size_t manifest_size, ma_bool_t is_policy_data);

ma_error_t ma_policy_manifest_release(ma_policy_manifest_t *policy_manifest);

/* Manifest get APIs */
const char *ma_policy_manifest_get_handler(ma_policy_manifest_t *policy_manifest);

const char *ma_policy_manifest_get_key_hash(ma_policy_manifest_t *policy_manifest);

const char *ma_policy_manifest_get_created_time(ma_policy_manifest_t *policy_manifest);

ma_error_t ma_policy_manifest_get_server_settings(ma_policy_manifest_t *policy_manifest, ma_table_t **server_settings_table);

/* Assignments get APIs */
typedef struct assignment_object_s {
    char    *obj_id;
    char    *product;
    char    *method;
    char    *param;
} assignments_object_t;

ma_bool_t ma_policy_manifest_find_assignment(ma_policy_manifest_t *policy_manifest, const char *assign_key);

ma_error_t ma_policy_manifest_get_assignment(ma_policy_manifest_t *policy_manifest, const char *assign_key, const assignments_object_t **assign_data);

ma_error_t ma_policy_manifest_delete_assignment(ma_policy_manifest_t *pol_manifest, const char *assign_key);

ma_error_t ma_policy_manifest_get_assigment_keys(ma_policy_manifest_t *policy_manifest, ma_array_t **assign_key_array);

/* Policy Object get APIs */
typedef struct policy_object_s {
    char            *po_id;
    char            *digest;
    char            *type;    
    char            *category;
    char            *feature;
    char            *name; 
	char            *legacy; /* "1" if policies are legacy, "0" if policies are in object-format */
    ma_bool_t       is_hooked_policy; /* 1 for hooked policies */
    size_t          pso_total; /* This variable has total number of PSOs from manifest */
    ma_table_t      *pso_table; /* table of key - pso_id and value is policy_setting_object_t object */
} policy_object_t;

ma_error_t ma_policy_manifest_get_po(ma_policy_manifest_t *policy_manifest, const char *po_id, const policy_object_t **po);

ma_error_t ma_policy_manifest_delete_po(ma_policy_manifest_t *pol_manifest, const char *po_id);

ma_error_t ma_policy_manifest_get_po_ids(ma_policy_manifest_t *policy_manifest, ma_array_t **id_array);

/* PSO get APIs */
typedef struct policy_setting_object_s {
    char            *pso_id;
    char            *po_id;
    char            *digest;    
} policy_setting_object_t;

ma_error_t ma_policy_manifest_get_pso(ma_policy_manifest_t *policy_manifest, const char *po_id, const char *pso_id, const policy_setting_object_t **pso);

ma_error_t ma_policy_manifest_delete_pso(ma_policy_manifest_t *policy_manifest, const char *po_id, const char *pso_id);

ma_error_t ma_policy_manifest_get_pso_ids(ma_policy_manifest_t *policy_manifest, const char *po_id, ma_array_t **pso_id_array);

/* Task Object get APIs */
typedef struct task_object_s {
    char            *task_obj_id;
    char            *name;    
    char            *digest;
	ma_bool_t       is_hooked_task; /* 1 for hooked task */
} task_object_t;

ma_error_t ma_policy_manifest_get_task_object(ma_policy_manifest_t *policy_manifest, const char *task_obj_id, const task_object_t **task_object);

ma_error_t ma_policy_manifest_delete_task_object(ma_policy_manifest_t *policy_manifest, const char *task_obj_id);

ma_error_t ma_policy_manifest_get_task_ids(ma_policy_manifest_t *policy_manifest, ma_array_t **id_array);

MA_CPP(})

#endif /* MA_POLICY_MANIFEST_H_INCLUDED */
