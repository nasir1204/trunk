#ifndef MA_POLICY_URL_REQUEST_H_INCLUDED
#define MA_POLICY_URL_REQUEST_H_INCLUDED

#include "ma/ma_common.h" 

MA_CPP(extern "C" {)

struct ma_policy_manager_s;
struct ma_proxy_list_s;

typedef struct ma_policy_url_request_s ma_policy_url_request_t, *ma_policy_url_request_h; 

#define MA_POLICY_TASK_MAX_URL_LENGTH_INT       512

ma_error_t ma_policy_url_request_create(struct ma_policy_manager_s *manager, ma_policy_url_request_t **policy_url_request);

ma_error_t ma_policy_url_request_set_proxy(ma_policy_url_request_t *policy_url_request, ma_proxy_list_t *proxy_list);

ma_error_t ma_policy_url_request_send(ma_policy_url_request_t *policy_url_request, const char *url, ma_url_request_connect_callback_t connect_cb, ma_url_request_finalize_callback_t final_cb, void *cb_data);

ma_error_t ma_policy_url_request_reset_stream(ma_policy_url_request_t *policy_url_request);

ma_error_t ma_policy_url_request_cancel(ma_policy_url_request_t *policy_url_request);

ma_error_t ma_policy_url_request_release(ma_policy_url_request_t *policy_url_request);

MA_CPP(})

#endif  /* MA_POLICY_URL_REQUEST_H_INCLUDED */

