#ifndef MA_TASK_HANDLER_H_INCLUDED
#define MA_TASK_HANDLER_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/services/policy/ma_policy_manager.h"
#include "ma/internal/utils/spipe/ma_spipe_package.h"

MA_CPP(extern "C" {)

typedef struct ma_task_handler_s ma_task_handler_t, *ma_task_handler_h ;

ma_error_t ma_task_handler_create(ma_policy_manager_t *policy_manager, ma_task_handler_t **task_handler);

ma_error_t ma_task_handler_set_spipe_package(ma_task_handler_t *task_handler, ma_spipe_package_t *spipe_pkg);

ma_error_t ma_task_handler_add_ref(ma_task_handler_t *task_handler);

ma_error_t ma_task_handler_start(ma_task_handler_t *task_handler, unsigned char *manifest, size_t manifest_size);

void ma_task_handler_stop(ma_task_handler_t *task_handler, ma_error_t status);

ma_error_t ma_task_handler_release(ma_task_handler_t *task_handler);

MA_CPP(})

#endif  /* MA_TASK_HANDLER_H_INCLUDED */

