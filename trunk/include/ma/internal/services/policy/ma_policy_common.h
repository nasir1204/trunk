#ifndef MA_POLICY_COMMON_H_INCLUDED
#define MA_POLICY_COMMON_H_INCLUDED

#include "ma/ma_common.h" 
#include "ma/internal/services/policy/ma_policy_manager.h"
#include "ma/internal/services/policy/ma_policy_url_request.h"
#include "ma/internal/ma_macros.h"

#include "ma/internal/utils/network/ma_net_common.h"
#include "ma/internal/services/policy/ma_policy_handler.h"
#include "ma/internal/services/policy/ma_task_handler.h"
#include "ma/internal/utils/network/ma_url_request.h"

MA_CPP(extern "C" {)

typedef enum policy_manifest_request_status_e {	
	MA_POLICY_MANIFEST_REQUEST_RAISED,
	MA_POLICY_MANIFEST_REQUEST_DECORATED,
	MA_POLICY_MANIFEST_REQUEST_COMPLETED
} policy_manifest_request_status_t; 

struct ma_policy_manager_s {
    ma_spipe_decorator_t				decorator_impl;
    ma_spipe_handler_t					handler_impl;   
    ma_context_t						*context;
    ma_spipe_priority_t					priority;    
    policy_manifest_request_status_t    policy_request_status;
    policy_manifest_request_status_t    task_request_status;
    ma_policy_user_info_t               *user_info;
	ma_url_request_connection_info_t	*last_connection_from_ah;
	ma_policy_handler_t					*policy_handler;
	ma_task_handler_t					*task_handler;
    ma_bool_t                           force_policy_enforcement ;
};

#define XML_ID_STR          "id"
#define XML_NAME_STR        "name"
#define XML_FEATURE_STR     "feature"
#define XML_CATEGORY_STR    "category"
#define XML_TYPE_STR        "type"
#define XML_PARM_INT_STR    "param_int"
#define XML_PARAM_STR_STR   "param_str"
#define XML_PRIORITY_STR    "priority"

#define XML_POLICY_SETTING_OBJECT_ROOT_STR  "EPOPolicySettings"
#define XML_TASK_OBJECT_ROOT_STR            "EPOTask"


#define MAX_MSTREAM_SIZE    1024

struct ma_policy_url_request_s {  	
    ma_url_request_t            *url_request;    
	ma_url_request_io_t         io_streams;	
	ma_url_request_ssl_info_t   ssl_info;    
    char                        id[MA_MAX_LEN];  /* po id or task id */  
    ma_bool_t                   is_po_download;
};

MA_CPP(})

#endif  /* MA_POLICY_COMMON_H_INCLUDED */

