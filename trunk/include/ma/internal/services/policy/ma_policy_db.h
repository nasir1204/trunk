#ifndef MA_POLICY_DB_H_INCLUDED
#define MA_POLICY_DB_H_INCLUDED
#include "ma/internal/utils/database/ma_db.h"
#include "ma/internal/utils/database/ma_db_recordset.h"
#include "ma/internal/utils/database/ma_db_statement.h"

MA_CPP(extern "C" {)

//DDL'S
#define CREATE_MPO_TABLE_STMT "CREATE TABLE IF NOT EXISTS MA_POLICY_OBJECT(PO_ID TEXT NOT NULL UNIQUE,PO_NAME TEXT NOT NULL,PO_FEATURE TEXT NOT NULL, PO_CATEGORY	TEXT NOT NULL, PO_TYPE TEXT	NOT NULL, PO_DIGEST	TEXT NOT NULL, PO_LEGACY TEXT NOT NULL, PRIMARY KEY (PO_ID) ON CONFLICT REPLACE)"
#define CREATE_MPSO_TABLE_STMT "CREATE TABLE IF NOT EXISTS MA_POLICY_SETTINGS_OBJECT (PO_ID TEXT NOT NULL ,PSO_ID TEXT NOT NULL, PSO_NAME	TEXT	NOT NULL ,PSO_DIGEST	TEXT	NOT NULL ,PSO_DATA	BLOB ,PSO_PARAM_INT	TEXT ,PSO_PARAM_STR	TEXT ,PSO_TIMESTAMP	TEXT NOT NULL,PRIMARY KEY (PSO_ID) ON CONFLICT REPLACE, FOREIGN KEY (PO_ID) REFERENCES MA_POLICY_OBJECT(PO_ID) ON UPDATE CASCADE ON DELETE CASCADE )"
#define CREATE_MPPA_TABLE_STMT "CREATE TABLE IF NOT EXISTS MA_POLICY_PRODUCT_ASSIGNMENT (PO_ID TEXT NOT NULL ,PPA_PRODUCT TEXT NOT NULL, PPA_METHOD	TEXT,PPA_PARAM	TEXT, UNIQUE(PO_ID, PPA_PRODUCT, PPA_METHOD, PPA_PARAM) ON CONFLICT REPLACE, FOREIGN KEY (PO_ID) REFERENCES MA_POLICY_OBJECT(PO_ID) ON UPDATE CASCADE ON DELETE CASCADE )"

//creates table if not exists 
ma_error_t db_add_policy_instance(ma_db_t *db_handle) ;

//insert 
ma_error_t db_add_policy_object(ma_db_t *db_handle, const char *po_id, const char *po_name, const char *po_feature, const char *po_category, const char *po_type, const char *po_digest, const char *po_legacy) ;
ma_error_t db_add_policy_settings_object(ma_db_t *db_handle, const char *po_id, const char *pso_id, const char *pso_name, const char *pso_digest, ma_variant_t *pso_data, const char *pso_param_int, const char *pso_param_str, const char *time_stamp) ;
ma_error_t db_add_policy_product_assignment(ma_db_t *db_handle, const char *po_id, const char *product, const char *method, const char *param ) ;

//delete
ma_error_t db_remove_policy_object(ma_db_t *db_handle, const char *po_id) ;
ma_error_t db_remove_policy_settings_object(ma_db_t *db_handle, const char *po_id, const char *pso_id) ;
ma_error_t db_remove_policy_product_assignment(ma_db_t *db_handle, const char *po_id, const char *product, const char *method, const char *param ) ;

//project
ma_error_t db_get_policy_objects(ma_db_t *db_handle, const char *po_id, ma_db_recordset_t **db_record) ;
ma_error_t db_get_policy_settings_objects(ma_db_t *db_handle, const char *po_id, const char *pso_id, ma_db_recordset_t **db_record) ; 
ma_error_t db_get_policy_product_assignments(ma_db_t *db_handle, const char *po_id, const char *product, const char *method, const char *param, ma_db_recordset_t **db_record) ;
ma_error_t db_get_assignment_params(ma_db_t *db_handle, const char *method, ma_db_recordset_t **db_record);

ma_error_t db_get_all_products(ma_db_t *db_handle, ma_int32_t agent_mode, ma_db_recordset_t **db_record) ; 
ma_error_t db_get_product_policies(ma_db_t *db_handle, const char *product, const char *timestamp, const char *method, const char *param, ma_db_recordset_t **db_record) ;  //set method and param for appropriate policies to be retrieved 
ma_error_t db_get_notified_product_list(ma_db_t *db_handle, const char *timestamp, ma_db_recordset_t **db_record);
ma_error_t db_get_product_policies_by_uri(ma_db_t *db_handle, const char *product_id, const char *po_feature, const char *po_category, const char *po_type, const char *po_name, const char *pso_name, const char *pso_param_int, const char *pso_param_str, ma_db_recordset_t **db_record);

/* This is using to find if any existing polices based on feature, category and type */
ma_error_t db_get_policy_setting_object_id(ma_db_t *db_handle, const char *product, const char *po_feature, const char *po_category, const char *po_type, const char *po_name, const char *pso_name, const char *pso_param_int, const char *pso_param_str, ma_db_recordset_t **db_record);

ma_error_t db_update_policy_setting_object_data(ma_db_t *db_handle, ma_variant_t *pso_data, const char *pso_id);
ma_error_t db_update_po_digest(ma_db_t *db_handle, const char *po_id, const char *digest);

MA_CPP(})
#endif  //MA_POLICY_DB_H_INCLUDED

