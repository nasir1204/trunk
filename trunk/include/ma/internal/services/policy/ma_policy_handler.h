#ifndef MA_POLICY_HANDLER_H_INCLUDED
#define MA_POLICY_HANDLER_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/services/policy/ma_policy_manager.h"
#include "ma/internal/utils/spipe/ma_spipe_package.h"

MA_CPP(extern "C" {)

typedef struct ma_policy_handler_s ma_policy_handler_t, *ma_policy_handler_h;

ma_error_t ma_policy_handler_create(ma_policy_manager_t *policy_manager, ma_policy_handler_t **policy_handler);

ma_error_t ma_policy_handler_set_spipe_package(ma_policy_handler_t *policy_handler, ma_spipe_package_t *spipe_pkg);

ma_error_t ma_policy_handler_add_ref(ma_policy_handler_t *policy_handler);

ma_error_t ma_policy_handler_start(ma_policy_handler_t *policy_handler, unsigned char *manifest, size_t manifest_size);

void ma_policy_handler_stop(ma_policy_handler_t *policy_handler, ma_error_t status);

ma_error_t ma_policy_handler_release(ma_policy_handler_t *policy_handler);

MA_CPP(})

#endif  /* MA_POLICY_HANDLER_H_INCLUDED */

