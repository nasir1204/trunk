#ifndef MA_EVENT_SERVICE_H_INCLUDED
#define MA_EVENT_SERVICE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/services/ma_service.h"

MA_CPP(extern "C" {)

MA_EVENT_API ma_error_t ma_event_service_create(const char *service_name, ma_service_t **event_service);

MA_CPP(})

//#include "ma/internal/services/dispatcher/ma_event_service_dispatcher.h"

#endif /*MA_EVENT_SERVICE_H_INCLUDED*/
