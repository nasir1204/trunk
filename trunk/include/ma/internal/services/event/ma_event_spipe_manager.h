#ifndef MA_EVENT_SPIPE_MANAGER_H_INCLUDED
#define MA_EVENT_SPIPE_MANAGER_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/event/ma_event.h"
#include "ma/internal/services/ahclient/ma_spipe_decorator.h"
#include "ma/internal/services/ahclient/ma_spipe_handler.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/services/event/ma_event_persist.h"

MA_CPP(extern "C" {)

typedef struct ma_priority_event_forwarding_policy_s ma_priority_event_forwarding_policy_t, *ma_priority_event_forwarding_policy_h;

struct ma_priority_event_forwarding_policy_s{
	ma_bool_t is_enabled;
	size_t event_batch_size;
	size_t trigger_interval;
	ma_event_severity_t event_trigger_threshold;
};

typedef struct ma_event_spipe_manager_s ma_event_spipe_manager_t, *ma_event_spipe_manager_h;

ma_error_t ma_event_spipe_manager_create(ma_context_t *context, ma_event_persist_t *evt_persist, ma_event_spipe_manager_t **evt_spipe_manager);

ma_spipe_decorator_t *ma_event_spipe_manager_get_spipe_decorator(ma_event_spipe_manager_t *self);

ma_spipe_handler_t *ma_event_spipe_manager_get_spipe_handler(ma_event_spipe_manager_t *self);

void ma_event_spipe_manager_set_priority_event_forwarding_policy(ma_event_spipe_manager_t *self, const ma_priority_event_forwarding_policy_t *ev_policy);

const ma_priority_event_forwarding_policy_t *ma_event_spipe_manager_get_priority_event_forwarding_policy(ma_event_spipe_manager_t *self);

ma_bool_t ma_event_spipe_manager_raise_alert(ma_event_spipe_manager_t *self, ma_event_priority_t priority);

void ma_event_spipe_manager_release(ma_event_spipe_manager_t *self);

MA_CPP(})

#endif


