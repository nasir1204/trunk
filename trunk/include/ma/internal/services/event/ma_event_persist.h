#ifndef MA_EVENT_PERSIST_H_INCLUDED
#define MA_EVENT_PERSIST_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_variant.h"
#include "ma/internal/defs/ma_object_defs.h"



MA_CPP(extern "C" {)

/**
event message  priority.
*/
typedef enum ma_event_priority_e {
    MA_EVENT_PRIORITY_UNDECIDED = 0,
    MA_EVENT_PRIORITY_NORMAL,
    MA_EVENT_PRIORITY_IMMEDIATE 
}ma_event_priority_t;

typedef struct ma_event_xml_info_node_s ma_event_xml_info_node_t;

typedef struct ma_event_xml_list_s ma_event_xml_list_t;

typedef struct ma_event_persist_s ma_event_persist_t, *ma_event_persist_h;

ma_error_t ma_event_persist_create(ma_context_t *context, ma_event_persist_t **event_ds);
	
ma_error_t ma_event_persist_add_event(ma_event_persist_t *self, ma_event_priority_t priority, const unsigned char *event_xml_buffer, size_t size);

ma_error_t ma_event_persist_remove_event(ma_event_persist_t *self, ma_event_priority_t priority, char const *event_xmlname);

ma_error_t ma_event_persist_set_event_filter_version(ma_event_persist_t *self, const char *evt_filter_version);

ma_error_t ma_event_persist_refresh_event_filter_version(ma_event_persist_t *self);

ma_error_t ma_event_persist_get_event_filter_version(ma_event_persist_t *self, char **evt_filter_version, size_t *size);

ma_error_t ma_event_persist_set_disabled_event_list(ma_event_persist_t *self, const char *disabled_events);

ma_error_t ma_event_persist_get_disabled_event_list(ma_event_persist_t *self, ma_variant_t **event_filter_list);

ma_error_t ma_event_persist_is_event_exist(ma_event_persist_t *self, ma_event_priority_t priority, ma_bool_t *is_exist);

ma_error_t ma_event_persist_get_event_xml_list(ma_event_persist_t *self, ma_event_priority_t priority, size_t batch_size, ma_event_xml_list_t *xml_list);

ma_error_t ma_event_persist_remove_event_xml(ma_event_persist_t *self, ma_event_xml_list_t *xml_list);

ma_error_t ma_event_persist_release(ma_event_persist_t *self);

ma_error_t ma_event_persist_add_event_by_name(ma_event_persist_t *self, const char *event_xml_name, ma_event_priority_t priority, const char *event_xml_buffer, size_t size);

MA_CPP(})

#endif

