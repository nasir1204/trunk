#ifndef MA_COMPAT_RELAY_DICOVERY_HANDLER_H_INCLUDED
#define MA_COMPAT_RELAY_DICOVERY_HANDLER_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_log.h"
#include "ma/internal/services/udp_server/ma_udp_msg_handler.h"

MA_CPP(extern "C" {)

ma_error_t ma_compat_relay_discovery_handler_create(ma_udp_msg_handler_t **relay_discovery_handler);

ma_error_t ma_compat_relay_discovery_handler_configure(ma_udp_msg_handler_t *self, ma_context_t *context);

MA_CPP(})

#endif /*MA_COMPAT_RELAY_DICOVERY_HANDLER_H_INCLUDED*/