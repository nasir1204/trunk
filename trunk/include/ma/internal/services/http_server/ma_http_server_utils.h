#ifndef MA_HHTP_SERVER_UTILS_H_INCLUDED
#define MA_HHTP_SERVER_UTILS_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/macrypto/ma_crypto.h"

#include "uv.h"

#define MA_HTTP_MAX_URL_PATH_LEN                    1024

#define MA_HTTP_REPO_SOFTWARE_FOLDER_STR             "Software"

#define MA_HTTP_LOGGING_FOLDER                      "Logging"
//#define MA_HTTP_AGENT_FOLDER                      "Agent"
#define MA_HTTP_AGENT_LOG_HTML_FILE_STR              "AgentLog.html"
#define MA_HTTP_AGENT_LOG_JS_FILE_STR                "AgentLog.js"
//#define MA_HTTP_AGENT_LOG_CSS_FILE_STR            "AgentLog.css"
#define MA_HTTP_AGENT_LOG_JSON_FILE_STR              "AgentLog.json"
#define MA_HTTP_LOAD_PRODUCT_NAMES_STR				 "LoadProductNames"
#define MA_HTTP_LOAD_LOG_FILE_NAMES_STR				 "LoadLogFileNames"
#define MA_HTTP_DISPLAY_LOG_FILE_STR				 "DisplayLogFile"
#define MA_HTTP_DOWNLOAD_LOG_FILE_STR			     "DownLoadLogFile"

MA_CPP(extern "C" {)

struct ma_context_s ;

/* Http server internal utils */

/* URL parse Interfaces, caller should pass the pointer with sufficient memory 
   is_connect is 1 for CONNECT method or else 0.
   return MA_FALSE on failure 
*/
ma_bool_t get_url_path_from_url(const uv_buf_t *url, char *url_path, size_t size, int is_connect);

ma_bool_t get_query_from_url(const uv_buf_t *url, char *query, size_t size, int is_connect);

ma_bool_t get_query_value_from_url(const uv_buf_t *url, const char *name, char *query_value, size_t size, int is_connect);

ma_bool_t is_request_for_repository(const char *file_name);

ma_bool_t is_request_for_replica_log(const char *file_name);

ma_bool_t is_request_for_sitestat_xml(const char *file_name);

ma_bool_t is_request_for_log_file(const char *file_name);

ma_bool_t get_file_from_url_path(char *url_path, char *file, size_t size);

ma_bool_t get_replica_log_from_file(const char *file_name, char *replica_log, size_t size);

ma_bool_t get_local_file_path_for_file(const char *file, const char *root_dir, char *local_file_path, size_t size);

/* Validates peer ip with spipe sites */
ma_bool_t is_request_from_valid_client(struct ma_context_s *context, const char *peer_ip);

ma_bool_t verify_file_hash(uv_loop_t *uv_loop, ma_crypto_t *crypto, const char *local_file_path, ma_crypto_hash_type_t hash_type, uv_buf_t hash_buf);

const char *get_mime_type(char *file_name);

MA_CPP(})

#endif /* MA_HHTP_SERVER_UTILS_H_INCLUDED */
