#ifndef MA_LOG_FILE_HANDLER_H_INCLUDED
#define MA_LOG_FILE_HANDLER_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/services/http_server/ma_http_request_handler_factory.h"

MA_CPP(extern "C" {)

struct ma_http_server_s;

/**
 * Creates the log file handler factory.
 */
MA_HTTP_SERVER_API ma_error_t ma_log_file_handler_factory_create(struct ma_http_server_s *server, ma_http_request_handler_factory_t **log_file_handler_factory);

MA_CPP(})

#endif /* MA_LOG_FILE_HANDLER_H_INCLUDED */
    