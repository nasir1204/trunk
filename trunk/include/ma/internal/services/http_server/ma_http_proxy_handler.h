#ifndef MA_HTTP_PROXY_HANDLER_H_INCLUDED
#define MA_HTTP_PROXY_HANDLER_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/defs/ma_http_server_defs.h"
#include "ma/internal/services/http_server/ma_http_request_handler_factory.h"

MA_CPP(extern "C" {)

struct ma_logger_s;
struct ma_db_s ;
struct ma_event_loop_s;
struct ma_http_server_s;

/*!
 * Creates the http_proxy handler factory
 */
MA_HTTP_SERVER_API ma_error_t ma_http_proxy_handler_factory_create(struct ma_http_server_s *server, ma_http_request_handler_factory_t **http_proxy_factory);

MA_HTTP_SERVER_API ma_error_t ma_http_proxy_handler_factory_set_event_loop(ma_http_request_handler_factory_t *http_proxy_factory, struct ma_event_loop_s *event_loop);

MA_HTTP_SERVER_API ma_error_t  ma_http_proxy_handler_factory_logger(ma_http_request_handler_factory_t *self, struct ma_logger_s *logger);

MA_HTTP_SERVER_API ma_error_t ma_http_proxy_handler_factory_set_msgbus_db(ma_http_request_handler_factory_t *self, struct ma_db_s *db) ;

MA_CPP(})

#endif /* MA_HTTP_PROXY_HANDLER_H_INCLUDED */
    