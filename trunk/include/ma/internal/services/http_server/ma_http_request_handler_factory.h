#ifndef MA_HTTP_REQUEST_HANDLER_FACTORY_H_INCLUDED
#define MA_HTTP_REQUEST_HANDLER_FACTORY_H_INCLUDED

#include "ma/internal/services/http_server/ma_http_request_handler.h"
#include "ma/internal/utils/datastructures/ma_queue.h"

MA_CPP(extern "C" {)

struct ma_policy_settings_bag_s;

struct ma_http_request_s;

typedef struct ma_http_request_handler_factory_s ma_http_request_handler_factory_t, *ma_http_request_handler_factory_h; 

typedef struct ma_http_request_handler_factory_methods_s {
    /**
     * determines if this factory's instance can handle the specified http request
     */
    ma_bool_t (*matches)(ma_http_request_handler_factory_t *self, struct ma_http_request_s const *request);

    /**
     * determines if the specified request is authorized. If not, the connection will return a 403
     */
    ma_bool_t (*authorize)(ma_http_request_handler_factory_t *self, struct ma_http_request_s const *request);

    /**
     * apply policies
     */
    ma_error_t (*configure)(ma_http_request_handler_factory_t *self);

    /**
     * creates a new handler instance
     */
    ma_error_t (*create_instance)(ma_http_request_handler_factory_t *self, ma_http_request_handler_t **handler);

	/**
     * start
     */
    ma_error_t (*start)(ma_http_request_handler_factory_t *self) ;

	/**
     * stop
     */
    ma_error_t (*stop)(ma_http_request_handler_factory_t *self) ;

    /**
     * virtual destructor
     */
    void (*destroy)(ma_http_request_handler_factory_t *self);
} ma_http_request_handler_factory_methods_t;

/* base class / interface for any request_handler factory */
struct ma_http_request_handler_factory_s {
    /* vtable */
    ma_http_request_handler_factory_methods_t const *methods;

    /* collection */
    ma_queue_t qlink;

    void *data;
};

MA_STATIC_INLINE ma_bool_t ma_http_request_handler_factory_matches(ma_http_request_handler_factory_t *factory, struct ma_http_request_s const *request) {
    return factory ? factory->methods->matches(factory, request) : MA_FALSE;
}

MA_STATIC_INLINE ma_bool_t ma_http_request_handler_factory_authorize(ma_http_request_handler_factory_t *factory, struct ma_http_request_s const *request) {
    return factory ? factory->methods->authorize(factory, request) : MA_TRUE;
}

MA_STATIC_INLINE ma_error_t ma_http_request_handler_factory_configure(ma_http_request_handler_factory_t *factory) {
    return factory ? factory->methods->configure(factory) : MA_ERROR_INVALIDARG;

}

MA_STATIC_INLINE ma_error_t ma_http_request_handler_factory_start(ma_http_request_handler_factory_t *factory) {
    return factory ? factory->methods->start(factory) : MA_ERROR_INVALIDARG;
}

MA_STATIC_INLINE ma_error_t ma_http_request_handler_factory_stop(ma_http_request_handler_factory_t *factory) {
    return factory ? factory->methods->stop(factory) : MA_ERROR_INVALIDARG;
}

MA_STATIC_INLINE ma_error_t ma_http_request_handler_factory_create_instance(ma_http_request_handler_factory_t *factory, ma_http_request_handler_t **handler) {
    return factory ? factory->methods->create_instance(factory, handler) : MA_ERROR_INVALIDARG;
}

MA_STATIC_INLINE void ma_http_request_handler_factory_release(ma_http_request_handler_factory_t *factory) {
    if (factory) factory->methods->destroy(factory);
}

/* for implementors */

MA_STATIC_INLINE void ma_http_request_handler_factory_init(ma_http_request_handler_factory_t *self, ma_http_request_handler_factory_methods_t const *methods, void *data) {
    if (self) {
        self->methods = methods;
        self->data = data;
    }
}

MA_CPP(})

#endif /* MA_HTTP_REQUEST_HANDLER_FACTORY_H_INCLUDED */
