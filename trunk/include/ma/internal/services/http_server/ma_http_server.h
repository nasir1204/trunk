/**
 * semi private include file for ma_http_server.
 */
#ifndef MA_HTTP_SERVER_H_INCLUDED
#define MA_HTTP_SERVER_H_INCLUDED

#include "ma/internal/defs/ma_http_server_defs.h"
#include "ma/ma_common.h"


MA_CPP(extern "C" {)

/**
 Honors the following settings
 port = tcp port to listen on, default is 8081
 */

typedef struct ma_http_server_s ma_http_server_t, *ma_http_server_h;
struct ma_http_request_handler_factories_s;
struct ma_http_request_handler_factory_s; 

struct ma_event_loop_s;
struct ma_ds_s;
struct ma_db_s;
struct ma_logger_s;
struct ma_policy_settings_bag_s;

struct ma_service_s;
struct ma_msgbus_s;
struct ma_context_s;

/*!
 * creates an instance of the http server in the form of a ma_service_ex
 * API conforms to the ma_service_ex specification
 */
MA_HTTP_SERVER_API ma_error_t ma_http_server_service_create(char const *service_name, struct ma_service_s **service);

/*!
 * creates an instance of http_server
 * The created instance will not initially have any request handlers associated with it, those should be added using the ma_http_server_add_request_handler_factory()
 *   method below
 */
MA_HTTP_SERVER_API ma_error_t ma_http_server_create(ma_http_server_t **http_server);

/*!
 * frees the memory
 */
MA_HTTP_SERVER_API ma_error_t ma_http_server_release(ma_http_server_t *http_server);

MA_HTTP_SERVER_API ma_error_t ma_http_server_start(ma_http_server_t *http_server);

MA_HTTP_SERVER_API ma_error_t ma_http_server_stop(ma_http_server_t *http_server);

MA_HTTP_SERVER_API ma_error_t ma_http_server_restart(ma_http_server_t *http_server);

MA_HTTP_SERVER_API ma_error_t ma_http_server_set_event_loop(ma_http_server_t *http_server, struct ma_event_loop_s *loop);

MA_HTTP_SERVER_API ma_error_t ma_http_server_set_datastore(ma_http_server_t *http_server, struct ma_ds_s *datastore);

MA_HTTP_SERVER_API ma_error_t ma_http_server_set_database(ma_http_server_t *http_server, struct ma_db_s *database);

MA_HTTP_SERVER_API ma_error_t ma_http_server_set_msgbus_database(ma_http_server_t *self, struct ma_db_s *database) ;

MA_HTTP_SERVER_API ma_error_t ma_http_server_set_logger(ma_http_server_t *http_server, struct ma_logger_s *logger);

MA_HTTP_SERVER_API ma_error_t ma_http_server_set_msgbus(ma_http_server_t *http_server, struct ma_msgbus_s *msgbus);

MA_HTTP_SERVER_API ma_error_t ma_http_server_set_context(ma_http_server_t *http_server, struct ma_context_s *context);

MA_HTTP_SERVER_API ma_error_t ma_http_server_config(ma_http_server_t *http_server, const struct ma_policy_settings_bag_s *policy_bag, unsigned hint);

/*!
 * adds a request handler. You should add at least one handler to set up the functionality of the http_server or else will always return 404 Not Found! 
 */
MA_HTTP_SERVER_API ma_error_t ma_http_server_add_request_handler_factory(ma_http_server_t *http_server, struct ma_http_request_handler_factory_s *factory);

MA_CPP(})

#endif /* MA_HTTP_SERVER_H_INCLUDED */
