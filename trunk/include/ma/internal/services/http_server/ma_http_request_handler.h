#ifndef MA_HTTP_REQUEST_HANDLER_H_INCLUDED
#define MA_HTTP_REQUEST_HANDLER_H_INCLUDED

#include "ma/ma_common.h"

MA_CPP(extern "C" {)

struct ma_http_connection_s;


typedef struct ma_http_request_handler_s ma_http_request_handler_t, *ma_http_request_handler_h;

typedef struct ma_http_request_handler_methods_s {

    ma_error_t (*start)(ma_http_request_handler_t *handler, struct ma_http_connection_s *connection);

    /**
     * async stop method 
     */
    ma_error_t (*stop)(ma_http_request_handler_t *handler);

    /**
     * virtual destructor
     */
    void (*destroy)(ma_http_request_handler_t *handler);

} ma_http_request_handler_methods_t;

/* base class for any http request handler */ 
struct ma_http_request_handler_s {

    ma_http_request_handler_methods_t const *methods;

    void *data; /* some data */
};

/* API for ma_http_request_handler_t users */
MA_STATIC_INLINE ma_error_t ma_http_request_handler_start(ma_http_request_handler_t *handler, struct ma_http_connection_s *connection) {
    return handler ? handler->methods->start(handler, connection) : MA_ERROR_INVALIDARG;
}

MA_STATIC_INLINE ma_error_t ma_http_request_handler_stop(ma_http_request_handler_t *handler) {
    return handler ? handler->methods->stop(handler) : MA_ERROR_INVALIDARG;
}

MA_STATIC_INLINE void ma_http_request_handler_release(ma_http_request_handler_t *handler) {
    if (handler) {handler->methods->destroy(handler);}
}

/* for implementors */
MA_STATIC_INLINE void ma_http_request_handler_init(ma_http_request_handler_t *handler, ma_http_request_handler_methods_t const *methods, void *data) {
    if (handler) {
        handler->methods = methods;
        handler->data = data;
    }
}



MA_CPP(})

#endif /* MA_HTTP_REQUEST_HANDLER_H_INCLUDED */
