#ifndef MA_URL_CACHE_MANAGER_H_INCLUDED
#define MA_URL_CACHE_MANAGER_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/services/http_server/ma_url_cache.h"


MA_CPP(extern "C" {)

typedef struct ma_url_cache_manager_s ma_url_cache_manager_t, *ma_url_cache_manager_h;

ma_error_t  ma_url_cache_manager_create(ma_url_cache_manager_t **cache_manager);

ma_error_t  ma_url_cache_manager_add(ma_url_cache_manager_t *cache_manager, const char *url_name, ma_url_cache_t *cache_obj);

ma_error_t  ma_url_cache_manager_search(ma_url_cache_manager_t *cache_manager, const char *url_name, ma_url_cache_t **cache_obj);

ma_error_t  ma_url_cache_manager_remove(ma_url_cache_manager_t *cache_manager, const char *url_name, ma_url_cache_t *cache_obj);

ma_error_t  ma_url_cache_manager_clear(ma_url_cache_manager_t *cache_manager);

ma_error_t  ma_url_cache_manager_release(ma_url_cache_manager_t *cache_manager);

MA_CPP(})

#endif /* MA_URL_CACHE_MANAGER_H_INCLUDED */

