#ifndef MA_REPOSITORY_HANDLER_H_INCLUDED
#define MA_REPOSITORY_HANDLER_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/services/http_server/ma_http_request_handler_factory.h"

MA_CPP(extern "C" {)

struct ma_net_client_service_s;
struct ma_crypto_s; 
struct ma_client_s;
struct ma_db_s ;
struct ma_http_server_s;
struct ma_context_s;

/**
 * Creates the repository handler factory.
 */
MA_HTTP_SERVER_API ma_error_t ma_http_repository_handler_factory_create(struct ma_http_server_s *server, ma_http_request_handler_factory_t **repository_handler_factory);

MA_HTTP_SERVER_API ma_error_t ma_http_repository_handler_factory_set_context(ma_http_request_handler_factory_t *repository_handler_factory, struct ma_context_s *context);

MA_HTTP_SERVER_API ma_error_t ma_http_repository_handler_factory_set_net_client(ma_http_request_handler_factory_t *repository_handler_factory, struct ma_net_client_service_s *net_client);

MA_HTTP_SERVER_API ma_error_t ma_http_repository_handler_factory_set_crypto(ma_http_request_handler_factory_t *factory, struct ma_crypto_s *crypto);

MA_HTTP_SERVER_API ma_error_t ma_http_repository_handler_factory_set_client(ma_http_request_handler_factory_t *factory, struct ma_client_s *client);

MA_HTTP_SERVER_API ma_error_t ma_http_repository_handler_facotry_set_msgbus_db(ma_http_request_handler_factory_t *factory, struct ma_db_s *db) ;

MA_CPP(})

#endif /* MA_REPOSITORY_HANDLER_H_INCLUDED */
    