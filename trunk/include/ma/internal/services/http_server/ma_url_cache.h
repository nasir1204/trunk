#ifndef MA_URL_CACHE_H_INCLUDED
#define MA_URL_CACHE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/threading/ma_atomic.h"

#include "uv.h"

MA_CPP(extern "C" {)

/* Forwarddeclerations */
struct ma_http_repository_handler_factory_s;
struct ma_logger_s;
struct ma_ds_s;
struct ma_repository_s;

typedef enum ma_url_cache_state_e {
    MA_URL_CACHE_INITIALIZED,
    MA_URL_CACHE_VALIDATION_START,
    MA_URL_CACHE_VALIDATION_FAILED,
    MA_URL_CACHE_DOWNLOAD_START,
    MA_URL_CACHE_DOWNLOAD_FAILED,
    MA_URL_CACHE_DOWNLOADING,
    MA_URL_CACHE_READY,
    MA_URL_CACHE_DEINITIALIZED,
    MA_URL_CACHE_ERROR,
	MA_URL_CACHE_ABORTED,
    MA_URL_CACHE_UNKNOWN
} ma_url_cache_state_t;

typedef struct ma_url_cache_s ma_url_cache_t, *ma_url_cache_h;

typedef ma_error_t (*ma_on_cache_ready_cb_t)(ma_url_cache_t *self, ma_url_cache_state_t state, size_t file_size, void *cookie, void *cb_data);

ma_error_t ma_url_cache_create(uv_loop_t *uv_loop, const char *file, ma_url_cache_t **url_cache);

ma_error_t ma_url_cache_set_repository_factory(ma_url_cache_t *url_cache, struct ma_http_repository_handler_factory_s *factory);

ma_error_t ma_url_cache_set_ds(ma_url_cache_t *url_cache, struct ma_ds_s *ds);

ma_error_t ma_url_cache_set_logger(ma_url_cache_t *url_cache, struct ma_logger_s *logger);

ma_error_t ma_url_cache_set_repository(ma_url_cache_t *url_cache, struct ma_repository_s *repository);

ma_error_t ma_url_cache_add_ref(ma_url_cache_t *url_cache);

ma_error_t ma_url_cache_read_start(ma_url_cache_t *url_cache, ma_bool_t honor_lazy_cache, ma_on_cache_ready_cb_t cb, void *cb_data);

size_t ma_url_cache_get_data(void *cookie, unsigned char *buffer, size_t max_size);

ma_error_t ma_url_cache_read_stop(ma_url_cache_t *url_cache, void *cookie);

ma_url_cache_state_t ma_url_cache_get_state(ma_url_cache_t *url_cache);

ma_error_t ma_url_cache_set_state(ma_url_cache_t *url_cache, ma_url_cache_state_t state);

char *ma_url_cache_get_file_hash(ma_url_cache_t *url_cache) ;

ma_error_t ma_url_cache_release(ma_url_cache_t *url_cache);

ma_error_t ma_url_cache_abort(ma_url_cache_t *url_cache) ;

ma_error_t ma_url_cache_set_sahu_header(ma_url_cache_t *url_cache, const char *sahu_hdr_value);

MA_CPP(})

#endif /* MA_URL_CACHE_H_INCLUDED */


