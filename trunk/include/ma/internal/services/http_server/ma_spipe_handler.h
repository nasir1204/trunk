#ifndef MA_SPIPE_HANDLER_H_INCLUDED
#define MA_SPIPE_HANDLER_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/defs/ma_http_server_defs.h"
#include "ma/internal/services/http_server/ma_http_request_handler_factory.h"

MA_CPP(extern "C" {)

struct ma_crypto_s;
struct ma_context_s ;
struct ma_http_server_s;

/**
 * Creates the spipe handler factory.
 */
MA_HTTP_SERVER_API ma_error_t ma_spipe_handler_factory_create(struct ma_http_server_s *server, ma_http_request_handler_factory_t **spipe_handler_factory);

MA_HTTP_SERVER_API ma_error_t ma_spipe_handler_factory_set_crypto(ma_http_request_handler_factory_t *spipe_handler_factory, struct ma_crypto_s *crypto);

MA_HTTP_SERVER_API ma_error_t ma_spipe_handler_factory_set_context(ma_http_request_handler_factory_t *spipe_handler_factory, struct ma_context_s *context) ;

MA_CPP(})

#endif /* MA_SPIPE_HANDLER_H_INCLUDED */
    
