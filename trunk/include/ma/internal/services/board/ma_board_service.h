#ifndef MA_BOARD_SERVICE_H_INCLUDED
#define MA_BOARD_SERVICE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/services/ma_service.h"
#include "ma/internal/utils/board/ma_board.h"

MA_CPP(extern "C" {)
/* board service class */
typedef struct ma_board_service_s ma_board_service_t, *ma_board_service_h;

// board service ctor
MA_BOARD_API ma_error_t ma_board_service_create(const char *service_name, ma_service_t **service);

MA_BOARD_API ma_error_t ma_board_service_get_board(ma_board_service_t *service, ma_board_t **board);

MA_BOARD_API ma_error_t ma_board_service_get_msgbus(ma_board_service_t *service, ma_msgbus_t **msgbus);

MA_CPP(})

#include "ma/dispatcher/ma_board_service_dispatcher.h"
#endif /* MA_BOARD_SERVICE_H_INCLUDED */

