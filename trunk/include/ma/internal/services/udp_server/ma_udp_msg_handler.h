#ifndef MA_UDP_MSG_HANDLER_H_INCLUDED
#define MA_UDP_MSG_HANDLER_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/clients/udp/ma_udp_msg.h"

MA_CPP(extern "C" {)

struct ma_udp_connection_s;

typedef struct ma_udp_msg_handler_s ma_udp_msg_handler_t, *ma_udp_msg_handler_h;

typedef struct ma_udp_msg_handler_methods_s ma_udp_msg_handler_methods_t;

struct ma_udp_msg_handler_methods_s{
	ma_bool_t (*on_match_cb)(ma_udp_msg_handler_t *handler, ma_udp_msg_t *c_msg);

	ma_error_t (*on_msg_cb)(ma_udp_msg_handler_t *handler, struct ma_udp_connection_s *c_request, ma_udp_msg_t *c_msg);

	ma_error_t (*add_ref)(ma_udp_msg_handler_t *handler);

	ma_error_t (*on_release)(ma_udp_msg_handler_t *handler);
};

struct ma_udp_msg_handler_s{

	ma_udp_msg_handler_methods_t const *methods;

	void *user_data;
};


MA_STATIC_INLINE ma_error_t ma_udp_msg_handler_init(ma_udp_msg_handler_t *handler, ma_udp_msg_handler_methods_t const *methods, void *user_data){
	if(handler && methods && methods->add_ref && methods->on_match_cb && methods->on_msg_cb && methods->on_release){
		handler->methods = methods;
		handler->user_data = user_data;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

MA_STATIC_INLINE ma_bool_t ma_udp_msg_handler_on_match(ma_udp_msg_handler_t *handler, ma_udp_msg_t *c_msg){
	if(handler && c_msg){
		return handler->methods->on_match_cb(handler, c_msg);
	}
	return MA_FALSE;
}

MA_STATIC_INLINE ma_error_t ma_udp_msg_handler_on_msg(ma_udp_msg_handler_t *handler, struct ma_udp_connection_s *c_request, ma_udp_msg_t *c_msg){
	if(handler && c_request && c_msg){
		return handler->methods->on_msg_cb(handler, c_request, c_msg);
	}
	return MA_ERROR_INVALIDARG;
}

MA_STATIC_INLINE ma_error_t ma_udp_msg_handler_add_ref(ma_udp_msg_handler_t *handler){
	if(handler){
		return handler->methods->add_ref(handler);
	}
	return MA_ERROR_INVALIDARG;
}

MA_STATIC_INLINE ma_error_t ma_udp_msg_handler_release(ma_udp_msg_handler_t *handler){
	if(handler){
		return handler->methods->on_release(handler);
	}
	return MA_ERROR_INVALIDARG;
}

MA_CPP(})

#endif

