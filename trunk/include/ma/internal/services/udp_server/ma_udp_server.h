#ifndef MA_UDP_SERVER_H_INCLUDED
#define MA_UDP_SERVER_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/services/udp_server/ma_udp_msg_handler.h"

MA_CPP(extern "C" {)

struct ma_event_loop_s;
struct ma_logger_s;

typedef struct ma_udp_server_policy_s{
	ma_bool_t  is_enabled;

	char	   *multicast_addr;

	ma_uint16_t port;

}ma_udp_server_policy_t;

typedef struct ma_udp_server_s ma_udp_server_t, *ma_udp_server_h;

MA_UDP_API ma_error_t ma_udp_server_create(ma_udp_server_t **server);

MA_UDP_API ma_error_t ma_udp_server_set_policy(ma_udp_server_t *self, ma_udp_server_policy_t *policy);

MA_UDP_API ma_error_t ma_udp_server_get_policy(ma_udp_server_t *self, const ma_udp_server_policy_t **policy);

MA_UDP_API ma_error_t ma_udp_server_set_event_loop(ma_udp_server_t *self, struct ma_event_loop_s *loop);

MA_UDP_API ma_error_t ma_udp_server_set_logger(ma_udp_server_t *self, struct ma_logger_s *logger);

MA_UDP_API ma_error_t ma_udp_server_start(ma_udp_server_t *self);

MA_UDP_API ma_error_t ma_udp_server_stop(ma_udp_server_t *self);

MA_UDP_API ma_error_t ma_udp_server_register_msg_handler(ma_udp_server_t *self, ma_udp_msg_handler_t *handler);

MA_UDP_API ma_error_t ma_udp_server_unregister_msg_handler(ma_udp_server_t *self, ma_udp_msg_handler_t *handler);

MA_UDP_API ma_error_t ma_udp_server_release(ma_udp_server_t *self);

MA_CPP(})

#endif