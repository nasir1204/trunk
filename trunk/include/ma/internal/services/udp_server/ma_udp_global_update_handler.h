#ifndef MA_UDP_GLOBAL_UPDATE_HANDLER_H_INCLUDED
#define MA_UDP_GLOBAL_UPDATE_HANDLER_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/services/udp_server/ma_udp_msg_handler.h"
#include "ma/internal/defs/ma_object_defs.h"

MA_CPP(extern "C" {)

ma_error_t ma_udp_global_update_handler_create(ma_context_t *context, ma_udp_msg_handler_t **global_update_handler) ;

MA_CPP(})

#endif /*MA_UDP_GLOBAL_UPDATE_HANDLER_H_INCLUDED */
