#ifndef MA_UDP_CONNECTION_H_INCLUDED
#define MA_UDP_CONNECTION_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/clients/udp/ma_udp_msg.h"

MA_CPP(extern "C" {)

typedef struct ma_udp_connection_s ma_udp_connection_t;

MA_UDP_API ma_error_t ma_udp_connection_get_request_msg(ma_udp_connection_t *self, ma_udp_msg_t **request_msg);

MA_UDP_API ma_error_t ma_udp_connection_set_delayed_response(ma_udp_connection_t *self, ma_bool_t delayed_response);

MA_UDP_API ma_error_t ma_udp_connection_set_response_delay_timeout(ma_udp_connection_t *self, ma_uint64_t delay_time_ms);

MA_UDP_API ma_error_t ma_udp_connection_post_response(ma_udp_connection_t *self, ma_udp_msg_t *response);

MA_CPP(})

#endif