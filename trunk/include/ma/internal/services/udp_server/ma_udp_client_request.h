#ifndef MA_UDP_CLIENT_REQUEST_H_INCLUDED
#define MA_UDP_CLIENT_REQUEST_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/clients/udp_client/ma_udp_msg.h"

MA_CPP(extern "C" {)

typedef struct ma_udp_client_request_s ma_udp_client_request_t;

ma_error_t ma_upd_client_request_post_response(ma_udp_client_request_t *request_c, ma_udp_msg_t *reponse_msg, ma_bool_t is_delayed_response, ma_uint64_t delay_time);

MA_CPP(})

#endif