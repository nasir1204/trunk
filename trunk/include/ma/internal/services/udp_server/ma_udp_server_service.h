#ifndef MA_UDP_SERVER_SERVICE_H_INCLUDED
#define MA_UDP_SERVER_SERVICE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/services/ma_service.h"

MA_CPP(extern "C" {)

MA_UDP_API ma_error_t ma_udp_server_service_create(const char *service_name, ma_service_t **udp_service);

MA_UDP_API ma_error_t ma_compat_relay_server_service_create(const char *service_name, ma_service_t **udp_service);

MA_CPP(})

#endif