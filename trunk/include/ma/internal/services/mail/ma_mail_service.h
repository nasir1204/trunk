#ifndef MA_MAIL_SERVICE_H_INCLUDED
#define MA_MAIL_SERVICE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_msgbus.h"
#include "ma/internal/services/ma_service.h"

MA_CPP(extern "C" {)
/* mail service class */
typedef struct ma_mail_service_s ma_mail_service_t, *ma_mail_service_h;

/* mail service ctor */
MA_MAIL_API ma_error_t ma_mail_service_create(const char *service_name, ma_service_t **service);

MA_MAIL_API ma_error_t ma_mail_service_get_msgbus(ma_mail_service_t *service, ma_msgbus_t **msgbus);


MA_CPP(})

#include "ma/dispatcher/ma_mail_service_dispatcher.h"
#endif /* MA_MAIL_SERVICE_H_INCLUDED */

