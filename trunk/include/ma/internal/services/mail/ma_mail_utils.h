#ifndef MA_MAIL_UTILS_H_INCLUDED
#define MA_MAIL_UTILS_H_INCLUDED

#include "ma/ma_common.h"

MA_MAIL_API ma_error_t ma_mail_send(const char *from, const char *to, const char *sub, const char *body);

#endif

