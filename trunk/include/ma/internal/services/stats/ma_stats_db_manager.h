#ifndef MA_STATS_DB_H_INCLUDED
#define MA_STATS_DB_H_INCLUDED

#include "ma/internal/utils/database/ma_db.h"
#include "ma/internal/utils/database/ma_db_recordset.h"
#include "ma/internal/utils/database/ma_db_statement.h"

#define MA_AGENT_STATS_FIELD_BASE							0
#define MA_AGENT_STATS_FIELD_OWNER							MA_AGENT_STATS_FIELD_BASE + 1
#define MA_AGENT_STATS_FIELD_ATTRIBUTE						MA_AGENT_STATS_FIELD_BASE + 2
#define MA_AGENT_STATS_FIELD_LAST_SENT					    MA_AGENT_STATS_FIELD_BASE + 3
#define MA_AGENT_STATS_FIELD_CURRENT						MA_AGENT_STATS_FIELD_BASE + 4

MA_CPP(extern "C" {)

ma_error_t ma_stats_db_schema_create(ma_db_t *db_handle) ;

ma_error_t ma_stats_db_check_default(ma_db_t *db_handle) ;

ma_error_t ma_stats_db_add_stat(ma_db_t *db_handle, const char *owner, const char *attribute, ma_double_t last_sent, ma_double_t current) ;

ma_error_t ma_stats_db_update_last_sent(ma_db_t *db_handle, const char*owner, const char *attribute, ma_double_t value) ;

ma_error_t ma_stats_db_get_stats_by_owner(ma_db_t *db_handle, const char*owner, ma_db_recordset_t **db_record) ;

ma_error_t ma_stats_db_update_current(ma_db_t *db_handle, const char *owner, const char *attribute, ma_double_t value) ;

MA_CPP(})

#endif /* MA_STATS_DB_H_INCLUDED */