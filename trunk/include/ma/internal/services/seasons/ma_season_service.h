#ifndef MA_SEASON_SERVICE_H_INCLUDED
#define MA_SEASON_SERVICE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/services/ma_service.h"
#include "ma/internal/utils/season/ma_season.h"

MA_CPP(extern "C" {)
/* season service class */
typedef struct ma_season_service_s ma_season_service_t, *ma_season_service_h;

// season service ctor
MA_SEASON_API ma_error_t ma_season_service_create(const char *service_name, ma_service_t **service);

MA_SEASON_API ma_error_t ma_season_service_get_season(ma_season_service_t *service, ma_season_t **season);

MA_SEASON_API ma_error_t ma_season_service_get_msgbus(ma_season_service_t *service, ma_msgbus_t **msgbus);

MA_CPP(})

#include "ma/dispatcher/ma_season_service_dispatcher.h"
#endif /* MA_SEASON_SERVICE_H_INCLUDED */

