#ifndef MA_INVENTORY_SERVICE_H_INCLUDED
#define MA_INVENTORY_SERVICE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/services/ma_service.h"
#include "ma/internal/utils/inventory/ma_inventory.h"

MA_CPP(extern "C" {)
/* inventory service class */
typedef struct ma_inventory_service_s ma_inventory_service_t, *ma_inventory_service_h;

// inventory service ctor
MA_INVENTORY_API ma_error_t ma_inventory_service_create(const char *service_name, ma_service_t **service);

MA_INVENTORY_API ma_error_t ma_inventory_service_get_inventory(ma_inventory_service_t *inventory, ma_inventory_t **pinventory);
MA_CPP(})

#include "ma/dispatcher/ma_inventory_service_dispatcher.h"
#endif /* MA_INVENTORY_SERVICE_H_INCLUDED */

