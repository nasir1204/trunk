#ifndef MA_MIRROR_REQUEST_H_INCLUDED
#define MA_MIRROR_REQUEST_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/services/repository/ma_mirror_request_param.h"

MA_CPP(extern "C" {)

typedef struct ma_mirror_request_s ma_mirror_request_t, *ma_mirror_request_h;

typedef ma_error_t (*ma_mirror_request_complete_cb_t)(ma_mirror_request_t *request, ma_repository_mirror_state_t state, void *cb_data);

ma_error_t ma_mirror_request_create(ma_context_t *context, ma_mirror_request_param_t *param, ma_mirror_request_t **request);

ma_error_t ma_mirror_request_start(ma_mirror_request_t *request, ma_mirror_request_complete_cb_t cb, void *cb_data);

ma_error_t ma_mirror_request_stop(ma_mirror_request_t *request);

ma_error_t ma_mirror_request_release(ma_mirror_request_t *request);

ma_error_t ma_mirror_request_get_status(ma_mirror_request_t *request, ma_repository_mirror_state_t *status);

MA_CPP(})

#endif  /* MA_MIRROR_REQUEST_H_INCLUDED */

