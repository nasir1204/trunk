#ifndef MA_REPOSITORY_RANK_H_INCLUDED
#define MA_REPOSITORY_RANK_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_variant.h"
#include "ma/internal/utils/repository/ma_repository_internal.h"
#include "ma/internal/services/repository/ma_repository_service.h"
#include "ma/internal/services/repository/ma_repository_policy.h"
#include "ma/internal/utils/icmp/ma_icmp.h"

MA_CPP(extern "C" {)

typedef struct ma_repository_rank_s ma_repository_rank_t, *ma_repository_rank_h ;

ma_error_t  ma_repository_rank_create(ma_repository_service_t *repository_service, ma_repository_rank_t **respository_rank) ;

ma_error_t  ma_repository_rank_start(ma_repository_rank_t *self, ma_repository_rank_type_t rank_type) ;

ma_bool_t ma_repository_rank_get_state(ma_repository_rank_t *self) ;

ma_error_t  ma_repository_rank_stop(void *data) ;

ma_error_t ma_repository_rank_sitelist_update(void *data);

ma_error_t ma_repository_rank_sitelist_get(void *data, ma_icmp_repo_node_list_t **icmp_url_head);

ma_error_t  ma_repository_rank_release(ma_repository_rank_t *self) ;

MA_CPP(})
#endif  /* MA_REPOSITORY_RANK_H_INCLUDED */

