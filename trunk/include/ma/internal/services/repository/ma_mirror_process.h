#ifndef MA_MIRROR_PROCESS_H_INCLUDED
#define MA_MIRROR_PROCESS_H_INCLUDED


#include "ma/ma_common.h"
#include "ma/internal/utils/context/ma_context.h"

MA_CPP(extern "C" {)

typedef struct ma_mirror_process_s ma_mirror_process_t, *ma_mirror_process_h;

typedef void (*ma_mirror_exit_cb_t)(ma_mirror_process_t *mirror_process, int exit_status, int term_signal, void *cb_data);

ma_error_t ma_mirror_process_create(ma_context_t *context, ma_mirror_process_t **mirror_process);

ma_error_t ma_mirror_process_status(ma_mirror_process_t *mirror_process, ma_bool_t *is_running);

ma_error_t ma_mirror_process_start(ma_mirror_process_t *mirror_process, const char *exe_path, const char **args, ma_mirror_exit_cb_t exit_cb, void *cb_data);

ma_error_t ma_mirror_process_stop(ma_mirror_process_t *mirror_process);

ma_error_t ma_mirror_process_terminate(ma_mirror_process_t *mirror_process);

ma_error_t ma_mirror_process_release(ma_mirror_process_t *mirror_process);

MA_CPP(})

#endif /* MA_MIRROR_PROCESS_H_INCLUDED */

