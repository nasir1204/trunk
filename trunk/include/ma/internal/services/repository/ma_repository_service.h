#ifndef MA_REPOSITORY_SERVICE_H_INCLUDED
#define MA_REPOSITORY_SERVICE_H_INCLUDED


#include "ma/ma_common.h"
#include "ma/internal/services/ma_service.h"

MA_CPP(extern "C" {)


typedef struct ma_repository_service_s ma_repository_service_t;
/**
 *  @brief Create repository service 
 *  
 */
MA_REPOSITORY_API ma_error_t ma_repository_service_create(const char *service_name, ma_service_t **service) ;


MA_CPP(})

//#include "ma/internal/services/dispatcher/ma_repository_service_dispatcher.h"

#endif  /* MA_REPOSITORY_SERVICE_H_INCLUDED */

