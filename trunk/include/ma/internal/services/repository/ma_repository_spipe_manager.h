#ifndef MA_REPOSITORY_SPIPE_MANAGER_H_INCLUDED
#define MA_REPOSITORY_SPIPE_MANAGER_H_INCLUDED

#include "ma/internal/services/ahclient/ma_ah_client_service.h"
#include "ma/internal/services/repository/ma_repository_service.h"

MA_CPP(extern "C" {)

typedef struct ma_repository_spipe_manager_s ma_repository_spipe_manager_t, *ma_repository_spipe_manager_h ;

ma_error_t ma_repository_spipe_manager_create(ma_repository_service_t *service, ma_repository_spipe_manager_t **repo_spipe_manager) ;

ma_error_t ma_repository_spipe_manager_release(ma_repository_spipe_manager_t *self) ;

ma_spipe_decorator_t * ma_repository_spipe_manager_get_decorator(ma_repository_spipe_manager_t *self) ;

ma_spipe_handler_t * ma_repository_spipe_manager_get_handler(ma_repository_spipe_manager_t *self) ;

ma_error_t ma_repository_spipe_manager_raise_spipe_alert(ma_repository_spipe_manager_t *self) ;

MA_CPP(})

#endif  /* MA_REPOSITORY_SPIPE_MANAGER_H_INCLUDED */

