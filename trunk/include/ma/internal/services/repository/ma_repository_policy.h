#ifndef MA_REPOSITORY_POLICY_H_INCLUDED
#define MA_REPOSITORY_POLICY_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_variant.h"
#include "ma/internal/services/repository/ma_repository_service.h"
#include "ma/internal/services/ma_policy_settings_bag.h"

MA_CPP(extern "C" {)

struct ma_proxy_config_s ;

typedef struct ma_repository_policy_s ma_repository_policy_t, *ma_repository_policy_h ;

ma_error_t ma_repository_policy_create(ma_repository_service_t *service, ma_repository_policy_t **repository_policy, int hints) ;

ma_error_t ma_repository_policy_set_proxy_config(ma_repository_policy_t *self, struct ma_proxy_config_s *proxy_config) ;

ma_error_t ma_repository_policy_release(ma_repository_policy_t *self) ;

ma_error_t ma_repository_policy_update(ma_repository_policy_t *self, int hints) ;

MA_CPP(})
#endif  /* MA_REPOSITORY_POLICY_H_INCLUDED */

