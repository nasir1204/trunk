#ifndef MA_POLICY_SETTINGS_BAG_H_INCLUDED
#define MA_POLICY_SETTINGS_BAG_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_buffer.h"
#include "ma/ma_variant.h"
#include "ma/ma_log.h"
#include "ma/logger/ma_logger.h"

#include <string.h>

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "policybag"


MA_CPP(extern "C" {)

typedef struct ma_policy_settings_bag_methods_s ma_policy_settings_bag_methods_t, *ma_policy_settings_bag_methods_h;
typedef struct ma_policy_settings_bag_s ma_policy_settings_bag_t, *ma_policy_settings_bag_h;

/* Defines the C equivalent of a 'v-table' for the interface implementor */
struct ma_policy_settings_bag_methods_s {
    
    ma_error_t (*get_bool)(ma_policy_settings_bag_t *self, const char *section, const char *key, ma_bool_t *value);

    ma_error_t (*get_int)(ma_policy_settings_bag_t *self, const char *section, const char *key, ma_int32_t *value);

    ma_error_t (*get_str)(ma_policy_settings_bag_t *self, const char *section, const char *key, const char **value);

    ma_error_t (*get_buffer)(ma_policy_settings_bag_t *self, const char *section, const char *key, ma_buffer_t **value);

    ma_error_t (*get_variant)(ma_policy_settings_bag_t *self, const char *section, const char *key, ma_variant_t **value);

    void       (*destroy)(ma_policy_settings_bag_t *self); /*< virtual destructor */

};

struct ma_policy_settings_bag_s {
    ma_policy_settings_bag_methods_t           const    *methods;   /*< v-table */
    ma_logger_t                                 *logger ; /*extra copy */
    void                                        *data;
};


/*!
 * Helper function for derived classes to use for initialization
 */
MA_STATIC_INLINE void ma_policy_settings_bag_init(ma_policy_settings_bag_t *bag, ma_policy_settings_bag_methods_t  const *methods, void *data) {
    bag->methods = methods;
    bag->data = data;    
} 


MA_STATIC_INLINE ma_error_t ma_policy_settings_bag_set_logger(ma_policy_settings_bag_t *bag, ma_logger_t *logger) {
    if(bag && logger)    ma_logger_add_ref(bag->logger = logger);
    return MA_OK;
} 


MA_STATIC_INLINE ma_error_t ma_policy_settings_bag_get_bool(ma_policy_settings_bag_t *bag, const char *section, const char *key, ma_bool_t strict_check, ma_bool_t *value, ma_bool_t default_value) {
    if(bag && bag->methods && bag->methods->get_bool) {
        ma_error_t rc = bag->methods->get_bool(bag, section, key, value);
        
        if((MA_OK != rc) && (MA_FALSE == strict_check)) {
            rc = MA_OK;
            *value = default_value ; 
            MA_LOG(bag->logger, MA_LOG_SEV_WARNING, "returning default policy for section=%s , key=%s , value=%d", section, key, default_value);
            
        }
        return rc;
    }
    return MA_ERROR_PRECONDITION;
}

MA_STATIC_INLINE ma_error_t ma_policy_settings_bag_get_int(ma_policy_settings_bag_t *bag, const char *section, const char *key, ma_bool_t strict_check, ma_int32_t *value, ma_int32_t default_value) {
    if(bag && bag->methods && bag->methods->get_int) {
        ma_error_t rc = bag->methods->get_int(bag, section, key, value);
        
        if((MA_OK != rc) && (MA_FALSE == strict_check)) {
            rc = MA_OK;
            *value = default_value ; 
            MA_LOG(bag->logger, MA_LOG_SEV_WARNING, "returning default policy for section=%s , key=%s , value=%d", section, key, default_value);
            
        }
        return rc;
    }
    return MA_ERROR_PRECONDITION;
}

/* 
Warning - The returned string can be used to access/copy but  memory is transient and it will go.
*/
MA_STATIC_INLINE ma_error_t ma_policy_settings_bag_get_str(ma_policy_settings_bag_t *bag, const char *section, const char *key,ma_bool_t strict_check,  const char **value, const char *default_value) {
    if(bag && bag->methods && bag->methods->get_str) {
        ma_error_t rc = bag->methods->get_str(bag, section, key, value);
        
        if((MA_OK != rc) && (MA_FALSE == strict_check) ) {
            rc = MA_OK;
            *value = default_value ; 
            MA_LOG(bag->logger, MA_LOG_SEV_WARNING, "returning default policy for section=%s , key=%s , value=%s", section, key, default_value);
        }
        return rc;
    }
    return MA_ERROR_PRECONDITION;
}


MA_STATIC_INLINE ma_error_t ma_policy_settings_bag_get_buffer(ma_policy_settings_bag_t *bag, const char *section, const char *key, ma_bool_t strict_check, ma_buffer_t **value, const char *default_value) {
    if(bag && bag->methods && bag->methods->get_buffer) {
        ma_error_t rc = bag->methods->get_buffer(bag, section, key, value);
        
        if((MA_OK != rc) && (MA_FALSE == strict_check)) {
            rc = MA_OK;
            if(default_value && (MA_OK == (rc = ma_buffer_create(value, strlen(default_value))))) {
                rc = ma_buffer_set(*value, default_value, strlen(default_value));
            }
            MA_LOG(bag->logger, MA_LOG_SEV_WARNING, "returning default policy for section=%s , key=%s , value=%s", section, key, default_value);
        }
        return rc;
    }
    return MA_ERROR_PRECONDITION;
}

MA_STATIC_INLINE ma_error_t ma_policy_settings_bag_get_variant(ma_policy_settings_bag_t *bag, const char *section, const char *key, ma_bool_t strict_check, ma_variant_t **value, ma_variant_t *default_value) {
    if(bag && bag->methods && bag->methods->get_variant) {
        ma_error_t rc = bag->methods->get_variant(bag, section, key, value);
        
        if((MA_OK != rc) && (MA_FALSE == strict_check)) {
            rc = MA_OK;
            if(default_value) ma_variant_add_ref(*value = default_value);
            MA_LOG(bag->logger, MA_LOG_SEV_WARNING, "returning default policy for section=%s , key=%s , value=%p", section, key, default_value);
        }
        return rc;
    }
    return MA_ERROR_PRECONDITION;
}


MA_STATIC_INLINE void ma_policy_settings_bag_release(ma_policy_settings_bag_t *bag) {
    if (bag && bag->methods && bag->methods->destroy){
        ma_logger_release(bag->logger);
        bag->methods->destroy(bag);
    }
}


MA_CPP(})

#endif /* MA_POLICY_SETTINGS_BAG_H_INCLUDED */
