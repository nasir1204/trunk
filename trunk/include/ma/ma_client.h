#ifndef MA_CLIENT_H_INCLUDED
#define MA_CLIENT_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_message.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_log.h"

MA_CPP(extern "C" {)

/*
ma client object, ma client object enables point product to access agent manageability services.
*/
typedef struct ma_client_s ma_client_t, *ma_client_h;

/*
ma_client_notification_type_t is notification type.
*/
typedef enum ma_client_notification_e {
	MA_CLIENT_NOTIFY_STOP  = 0,
	MA_CLIENT_NOTIFY_START	
} ma_client_notification_t ;

/**
* ma client notification callback.
* @param client	                [in]            ma client instance
* @param type				    [in]            client notification type 
* @param cb_data	            [in]            user data.
*/
typedef void (*ma_client_notification_cb_t)(ma_client_t *client, ma_client_notification_t type, void *cb_data) ;

/*
* ma client log message callback. 
* @param severity	            [in]            log severity
* @param module_name    	    [in]            logging module name 
* @param log_message            [in]            log message.
* @param cb_data	            [in]            user data.
*/
typedef void (*ma_client_logmessage_cb_t)(ma_log_severity_t severity, const char *module_name, const char *log_message, void *user_data);

/*
* ma_client_create allows point product to create ma client object. 
* @param product_id	            [in]            product id of user.
* @param client		    	    [out]           ma client instance
* @result			                            MA_OK on success
*							                    others on failure.
*/
ma_error_t ma_client_create(const char *product_id,  ma_client_t **client);

/*
* ma_client_set_msgbus allows point product to set the msgbus from outside. (Optional)
* Client creates its own msgbus, if not provided by point product.
* If user creates and set the msgbus then user must stop and release it in client stop notification.
* @param client	                [in]            ma client instance
* @param msgbus	                [in]            msgbus instance.
* @result			                            MA_OK on success
*							                    others on failure.
*/
ma_error_t ma_client_set_msgbus(ma_client_t *client, ma_msgbus_t *msgbus);

/*
* ma_client_get_msgbus allows point product to get the msgbus instance which client has created.
* @param client	                [in]            ma client instance
* @param msgbus	                [out]           msgbus instance.
* @result			                            MA_OK on success
*							                    others on failure.
*/
ma_error_t ma_client_get_msgbus(ma_client_t *client, ma_msgbus_t **msgbus);

/*
* ma_client_set_thread_option allows point product to set the msgbus thread option.
* @param client	                [in]            ma client instance
* @param thread_option          [in]            msgbus thread option.
* @result			                            MA_OK on success
*							                    others on failure.
*/
ma_error_t ma_client_set_thread_option(ma_client_t *client, ma_msgbus_callback_thread_options_t thread_option);

/*
* ma_client_get_thread_option allows point product to get the msgbus thread option.
* @param client	                [in]            ma client instance
* @param thread_option          [out]           msgbus thread option.
* @result			                            MA_OK on success
*							                    others on failure.
*/
ma_error_t ma_client_get_thread_option(ma_client_t *client, ma_msgbus_callback_thread_options_t *thread_option);

/*
* ma_client_set_logger_callback allows point product to set the log message callback.
* @param client	                [in]            ma client instance
* @param log_cb			        [in]            ma client log message callback.
* @result			                            MA_OK on success
*							                    others on failure.
*/
ma_error_t ma_client_set_logger_callback(ma_client_t *client, ma_client_logmessage_cb_t log_cb, void *user_data);

/*
* ma_client_set_msgbus_passphrase_callback allows point product to set the passphrase callback for msgbus if required
* @param client	                [in]            ma client instance
* @param cb						[in]            msgbus passphrase callback
* @param cb_data				[in,optiona]    callback data if required
* @result			                            MA_OK on success
*							                    others on failure.
*/
ma_error_t ma_client_set_msgbus_passphrase_callback(ma_client_t *client, ma_msgbus_passphrase_provider_callback_t cb, void *cb_data);


/**
* ma_client_register_notification_callback allows user to register for ma client start/stop notification in case of agent/libraries upgrades.
* user must register for client notification after creation of ma client. If agent is installed and ready to use then pp get the MA_CLIENT_NOTIFY_START notification too as part of registration call.
* At MA_CLIENT_NOTIFY_START, user should (1) optional: create/start and set msgbus object to ma client (if user want to use msgbus from outside) (2) start ma client (3) register for all agent services in this order.
* @param client	                [in]            ma client instance
* @param cb					    [in]            ma client notification callback. 
* @param cb_data	            [in]            user data.
* @result			                            MA_OK on success
*							                    others on failure.
*/
ma_error_t ma_client_register_notification_callback(ma_client_t *client, ma_client_notification_cb_t cb, void *cb_data) ;

/**
* ma_client_unregister_notification_callback allows point product to unregister for ma client notification.
* user gets the MA_CLIENT_NOTIFY_STOP notification too as part of unregistration, if ma client is in START state.
* At MA_CLIENT_NOTIFY_STOP, user must (1) unregister for all agent services (2) stop ma client (3) optional: stop and release msgbus object (if user set the msgbus from outside) in this order.
* @param client	                [in]            ma client instance
* @result			                            MA_OK on success
*							                    others on failure.
*/
ma_error_t ma_client_unregister_notification_callback(ma_client_t *client) ;

/**
* ma_client_start allows point product to start ma client.
* @param client	                [in]            ma client instance
* @result			                            MA_OK on success
*							                    others on failure.
*/
ma_error_t ma_client_start(ma_client_t *client);

/**
* ma_client_stop allows point product to stop ma client.
* @param client	                [in]            ma client instance
* @result			                            MA_OK on success
*							                    others on failure.
*/
ma_error_t ma_client_stop(ma_client_t *client);

/**
* ma_client_release allows point product release ma client instance.
* @param client	                [in]            ma client instance
* @result			                            MA_OK on success
*							                    others on failure.
*/
ma_error_t ma_client_release(ma_client_t *client);

MA_CPP(})

#endif /* MA_CLIENT_H_INCLUDED */


