/** @file ma_msgbus.h
*  @brief Message Bus Declarations
*
*/

#ifndef MA_MSGBUS_H_INCLUDED
#define MA_MSGBUS_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_log.h"

#if !defined(MA_NO_DISPATCHER) && !defined(MA_NO_MSGBUS_DISPATCHER)
/* Using dispatcher */
#   define MA_MSGBUS_API MA_STATIC_INLINE
#else
#ifdef _MSC_VER
/* Windows - set up dll import/export decorators. */
# if defined(MA_BUILDING_MSGBUS_SHARED)
/* Building shared library. */
#   define MA_MSGBUS_API __declspec(dllexport)
# elif !defined(MA_USE_MSGBUS_STATIC)
/* Using shared library. */
#   define MA_MSGBUS_API __declspec(dllimport)
# else
/* Building static library or executable (unit test). */
#   define MA_MSGBUS_API /* nothing */
# endif
#elif __GNUC__ >= 4
# define MA_MSGBUS_API __attribute__((visibility("default")))
#else
# define MA_MSGBUS_API /* nothing */
#endif
#endif


MA_CPP(extern "C" {)

typedef struct ma_message_s ma_message_t, *ma_message_h;

typedef struct ma_msgbus_s  ma_msgbus_t, *ma_msgbus_h;
/**
* All the APIs return MA_OK on success otherwise a failure code
*/

/**
* Reachability of a message bus consumer's  , Consumers can be a client/server/publisher/subscriber and meaning of reachability changes as per their context.
* Client - Client request messages are reachable to servers in the in-proc/out-proc/out-box , default - MSGBUS_CONSUMER_REACH_INPROC .
* Server - Server registration or services exposed to the clients in in-proc/out-proc/out-box , default - MSGBUS_CONSUMER_REACH_INPROC.
* Subscriber- Subscriber can subscribe for messages  from in-proc/out-proc/out-box , default -MSGBUS_CONSUMER_REACH_INPROC .
* publisher - Published messages can reach in-proc/out-proc/out-box, default- MSGBUS_CONSUMER_REACH_INPROC.
*/
typedef enum ma_msgbus_consumer_reachability_e {
    MSGBUS_CONSUMER_REACH_INPROC = 0,   /** reachable in the same process */
    MSGBUS_CONSUMER_REACH_OUTPROC,      /** reachable in the a) same process and b) same box */
    MSGBUS_CONSUMER_REACH_OUTBOX        /** reachable  in the a) same process b) same box and c) outside the box */
} ma_msgbus_consumer_reachability_t;

typedef enum ma_msgbus_callback_thread_options_e {

    /*! DEFAULT
    * callback from a thread pool thread 
    */
    MA_MSGBUS_CALLBACK_THREAD_POOL = 0,

    /*! EXPERIMENTAL, WINDOWS ONLY
    * callback queued as an APC via QueueUserAPC(), your thread must be in an alertable wait state to receive the callback. Must OR in the thread handle ... 
    */
    MA_MSGBUS_CALLBACK_THREAD_APC = 0x10000000,

    /*!
    * callback invoked directly from i/o thread. Please don't make any blocking calls
    */
    MA_MSGBUS_CALLBACK_THREAD_IO = 0x20000000L,

    MA_MSGBUS_CALLBACK_THREAD_MASK = 0xf0000000
} ma_msgbus_callback_thread_options_t;

#define MSGBUSOPTTYPE_LONG								0x10000

#define  MSGBUSINIT(name, type, number) MSGBUSOPT_ ## name = MSGBUSOPTTYPE_ ## type + number
/**
* Option available for a message bus user
* This macro below setups the MSGBUSOPT_[what] enum, to be used with ma_msgbus_{endpoint, server , subscriber}_setopt(). 
* The first argument in the MSGBUSINIT() macro is the [what] word.
*/
typedef enum ma_msgbus_option_e {
    /* Timeout value in milliseconds, if other than default.
    * Client - sending request message timeout
    * Server - registration timeout
    * Subscriber - subscription timeout 
    * Publisher - publish message timeout */    
    MSGBUSINIT(TIMEOUT, LONG, 1),

    /* Indicates the reachability of a consumer accepted values are MSGBUS_CONSUMER_REACH_INPROC (default),
    * MSGBUS_CONSUMER_REACH_OUTPROC and MSGBUS_CONSUMER_REACH_OUTBOX.
    * Refer ma_msgbus_consumer_reachability_t documentation. */        
    MSGBUSINIT(REACH, LONG, 2),

    /* Specify the order in which your callback functions has to be called.
    * Used for clients/server and only for making asynchronous calls.  */
    MSGBUSINIT(PRIORITY, LONG, 3),

    /* Thread preferences, for example want persistent thread, number of threads to allocate. */
    MSGBUSINIT(THREADMODEL, LONG, 4)
} ma_msgbus_option_t;


/**
* Creates a handle to the message bus
* @param product_id     [in]        an unique id (product id). This will be presented to servers and will typically be used for authentication/authorization 
* @param msgbus         [in]        Address of pointer that receives the new msgbus. Don't forget to call ma_msgbus_release() when you are done
*/
MA_MSGBUS_API ma_error_t ma_msgbus_create(const char *product_id, ma_msgbus_t **msgbus);

/**
* Releases the msgbus
* @param msgbus         [in]    pointer to the msgbus
*/
MA_MSGBUS_API ma_error_t ma_msgbus_release(ma_msgbus_t *msgbus);

/**
* Set logger instance  
* @param msgbus         [in]    pointer to the msgbus
* @param logger         [in]    logger instance to enable the logs in message bus
*/
MA_MSGBUS_API ma_error_t ma_msgbus_set_logger(ma_msgbus_t *msgbus, ma_logger_t *logger);

/**
* Set passphrase provider callback, set only if you want to particpate in challenge and response authorization too.
* @param msgbus          [in]			 pointer to the msgbus
* @param cb_data		 [in, optional]  The callback data as it was specified in 'ma_msgbus_set_passphrase_provider_callback', use NULL if do not want any data to be passed
* @param passphrase      [in,out]		 pre allocated memory for passphrase, user should fill the passphrase. maximum length of passphrase is 1024.
* @param passphrase_size [in,out]		 size of the buffer input, user should set the size  consumed
* @remark  passphrase provided will be used for Challenge and Response protocol, we will calculate the sha256 hash of passphrase provided and compare it with string representation of
"AuthorizationHash" provided in "McAfee\Agent\Application\<SOFTWARE_ID>" on windows or "</etc/ma.d/<SOFTWARE_ID>/config.xml" in *nix.
*/
typedef ma_error_t (*ma_msgbus_passphrase_provider_callback_t)(ma_msgbus_t *msgbus, void *cb_data, unsigned char *passphrase, size_t *passphrase_size);

MA_MSGBUS_API ma_error_t ma_msgbus_set_passphrase_provider_callback(ma_msgbus_t *msgbus, ma_msgbus_passphrase_provider_callback_t cb, void *cb_data);
/**
* Gets the numeric representation of message bus library that you're currently linked against
* The format uses one byte each for the major, minor, and patchlevel parts of the version number. The low-order byte is unused. 
* For example, 5.2.1 version has a numeric representation of 0x05020100
* @param version    [out]   version number is written into this address
*/
MA_MSGBUS_API ma_error_t ma_msgbus_get_version(unsigned int *version);

/**
* Starts the message bus running in a different thread. This function returns immediately
* @param msgbus         [in]        pointer to a msgbus
*/
MA_MSGBUS_API ma_error_t ma_msgbus_start(ma_msgbus_t *msgbus);

/**
* Starts the message bus running on the calling thread. This function blocks until ma_msgbus_stop() is invoked
* @param msgbus         [in]        pointer to a msgbus.
*/
MA_MSGBUS_API ma_error_t ma_msgbus_run(ma_msgbus_t *msgbus);

/**
* Stops the message bus thread. This will cause ma_msgbus_run to return (if the bus was started that way). If the bus was started using a worker thread (e.g. via ma_msgbus_start)
* ma_msgbus_stop will attempt to stop that thread, optionally wait for the thread to finish, then return
* @param msgbus         [in]        pointer to a msgbus.
* @param wait           [in]        specifies whether this function should wait for worker thread to terminate. This flag is honored only when msgbus is running a separate thread via ma_msgbus_start()   
*/
MA_MSGBUS_API ma_error_t ma_msgbus_stop(ma_msgbus_t *msgbus, ma_bool_t wait);

/***************************************************************************** REQUEST/RESPONSE CLIENT API *****************************************************************************/

typedef struct ma_msgbus_endpoint_s ma_msgbus_endpoint_t, *ma_msgbus_endpoint_h;
typedef struct ma_msgbus_request_s ma_msgbus_request_t, *ma_msgbus_request_h; /* ref counted */

/**
* Creates a handle to the specified endpoint service
* @param msgbus         [in]        msgbus instance
* @param server_name    [in]        server/service to which this client wants to send a request
* @param reserved       [in]        reserved for future use, please pass NULL for now.
* @param endpoint       [out]       Address of pointer that receives the new endpoint. Don't forget to call ma_msgbus_endpoint_release() when you are done
*/
MA_MSGBUS_API ma_error_t ma_msgbus_endpoint_create(ma_msgbus_t *msgbus, const char *server_name, const void *reserved, ma_msgbus_endpoint_t **endpoint);

/**
* Releases the endpoint
* @param endpoint   [in]    pointer to the endpoint
*/
MA_MSGBUS_API ma_error_t ma_msgbus_endpoint_release(ma_msgbus_endpoint_t *endpoint);

/**
* Sets a desired option on an endpoint
* @param endpoint   [in]            The endpoint to which the option has to be set
* @param option     [in]            any valid ma_msgbus_option_t 
* @param param      [in]            All options are set with the option followed by a parameter. That parameter can be a long, char* , depending on what the specific option expects.
*/
MA_MSGBUS_API ma_error_t  ma_msgbus_endpoint_setopt(ma_msgbus_endpoint_t *endpoint,  ma_msgbus_option_t option, ...);

/**
* Callback for async request/response completion
* @param status     [in]            Indicates the status of the operation. MA_OK indicates success and the associated response is valid
* @param response   [in]            The result of the operation as calculated by the server
* @param cb_data    [in, optional]  The callback data as it was specified in 'ma_msgbus_async_send', use NULL if do not want any data to be passed
* @param request    [in]            The associated request object
*/
typedef ma_error_t (*ma_msgbus_request_callback_t)(ma_error_t status, ma_message_t *response, void *cb_data, ma_msgbus_request_t *request);

/**
* Initiates an asynchronous request
* @param endpoint   [in]            The endpoint to which the message has to be sent
* @param payload    [in]            The message to be sent
* @param cb         [in]            function to be called on receiving a response
* @param cb_data    [in]            Some data that will be passed in the callback function. This is opaque to the msgbus framework
* @param request    [out]           Address of pointer that receives the new request object
*/
MA_MSGBUS_API ma_error_t ma_msgbus_async_send(ma_msgbus_endpoint_t *endpoint, ma_message_t *payload, ma_msgbus_request_callback_t cb, void *cb_data, ma_msgbus_request_t **request);

/**
* Blocking send request. Blocks the thread until a reply is received or timeout or error!
* @param endpoint   [in]            The endpoint to which the message has to be sent
* @param payload    [in]            The message to be sent
* @param response   [out]           Address of pointer that receives the new response message
*/
MA_MSGBUS_API ma_error_t ma_msgbus_send(ma_msgbus_endpoint_t *endpoint, ma_message_t *request, ma_message_t **response);

/**
* Send request without a response
* @param endpoint   [in]            The endpoint to which the message has to be sent
* @param payload    [in]            The message to be sent
*/
MA_MSGBUS_API ma_error_t ma_msgbus_send_and_forget(ma_msgbus_endpoint_t *endpoint, ma_message_t *payload); /** send without a response */

/**
* Retrieves the endpoint associated with the specified request
* @param request    [in]            Request object, whose associated endpoint is needed.
* @param endpoint   [out]           Address of pointer that receives the associated endpoint. You must call ma_msgbus_endpoint_release when done using it.
*/
MA_MSGBUS_API ma_error_t ma_msgbus_request_get_endpoint(ma_msgbus_request_t *request, ma_msgbus_endpoint_t **endpoint);

typedef enum ma_msgbus_request_cancel_flags_e {
    /** Do not call the registered callback function */
    MA_MSGBUS_NO_NOTIFY = 0x1       
} ma_msgbus_request_cancel_flags_t;

/**
* Cancels a request
* @param request    [in]            The request to cancel
* @param flags      [in]            a combination of ma_msgbus_request_cancel_flags_t flags
* @remark Calling this function does not guarantee that the request will not be, or was not already, received by the target
*/
MA_MSGBUS_API ma_error_t ma_msgbus_request_cancel(ma_msgbus_request_t *request, int flags);

/**
* Frees resources associated with a request
* @param request    [in]            The request to release
*/
MA_MSGBUS_API ma_error_t ma_msgbus_request_release(ma_msgbus_request_t *request);


/***************************************************************************** REQUEST/RESPONSE SERVER API *****************************************************************************/

typedef struct ma_msgbus_server_s ma_msgbus_server_t, *ma_msgbus_server_h;
typedef struct ma_msgbus_client_request_s ma_msgbus_client_request_t, *ma_msgbus_client_request_h;

/**
* Creates a server/service
* @param msgbus             [in]            msgbus instance
* @param server_name        [in]            server/service which has to be started on the message bus
* @param server             [out]           Address of pointer that receives the new server. Don't forget to call ma_msgbus_server_release() when you are done
*/
MA_MSGBUS_API ma_error_t ma_msgbus_server_create(ma_msgbus_t *msgbus, const char *server_name, ma_msgbus_server_t **server);

/**
* Releases a server handle
* @param server             [in]            pointer to server
*/
MA_MSGBUS_API ma_error_t ma_msgbus_server_release(ma_msgbus_server_t *server);


/**
* Sets a desired option on an server
* @param server             [in]            The server to which the option has to be set
* @param option      [in]            any valid ma_msgbus_option_t 
* @param param              [in]            All options are set with the option followed by a parameter. That parameter can be a long, char* , depending on what the specific option expects.
*/
MA_MSGBUS_API ma_error_t ma_msgbus_server_setopt(ma_msgbus_server_t *server,  ma_msgbus_option_t option, ...);

/**
* Callback function to be implemented by the service
* @param server             [in]            The server for which a request has arrived
* @param request_payload    [in]            The request message
* @param cb_data            [in, optional]  The callback data as it was specified in 'ma_msgbus_server_start'
* @param c_request          [in]            client request object, which holds the requester info
*/
typedef ma_error_t (*ma_msgbus_server_handler)(ma_msgbus_server_t *server, ma_message_t *request_payload, void *cb_data, ma_msgbus_client_request_t *c_request);


/**
* Starts a server
* @param server         [in]            pointer to server created by ma_msgbus_server_create
* @param handler        [in]            The callback function that handles incoming requests for the server
* @param cb_data        [in]            Some data that will be passed in the callback function. This is opaque to the msgbus framework
*/
MA_MSGBUS_API ma_error_t ma_msgbus_server_start(ma_msgbus_server_t *server, ma_msgbus_server_handler handler, void *cb_data);

/**
* Stops a server
* @param                [in]            pointer to server created by ma_msgbus_server_create
* @remark after stopping the server, the registered callback function will no longer be invoked.
*/
MA_MSGBUS_API ma_error_t ma_msgbus_server_stop(ma_msgbus_server_t *server);

/**
* Allows the server to post a result for an incoming request. You must always use this method to provide the response to an incoming request whether you process the request asynchronously or not
* @param c_request      [in]            Specifies the client request for which this response applies. This is/was passed in the server_handler callback function
* @param status         [in]            Specifies the result of the operation
* @param response       [in]            Specifies the result of the request. It's ok to pass in NULL here in case of failure
*/
MA_MSGBUS_API ma_error_t ma_msgbus_server_client_request_post_result(ma_msgbus_client_request_t *c_request, ma_error_t status, ma_message_t *response);

/**
* Increments the reference count for a client request. Use this to hold on to a client request beyond the scope of the server_handler callback function in situations where you want to process the request asynchronously
* @param c_request      [in]            Specifies the client request for which this response applies. This is/was passed in the server_handler callback function
*/
MA_MSGBUS_API ma_error_t ma_msgbus_client_request_add_ref(ma_msgbus_client_request_t *c_request);


/**
* Releases a client request, use this if you previously used ma_msgbus_client_request_add_ref to hold on to the handle 
* @param c_request      [in]            Specifies the client request for which this response applies. This is/was passed in the server_handler callback function
*/
MA_MSGBUS_API ma_error_t ma_msgbus_client_request_release(ma_msgbus_client_request_t *c_request);


/*****************************************************************************  PUB/SUB SUBSCRIBER *****************************************************************************/
typedef struct ma_msgbus_subscriber_s ma_msgbus_subscriber_t, *ma_msgbus_subscriber_h;

/*  
* Creates a handle for subscriber
* @param msgbus         [in]        msgbus instance
* @param subscriber     [out]       Address of pointer that receives the new subscriber. Don't forget to call ma_msgbus_subscriber_release() when you are done
*/
MA_MSGBUS_API ma_error_t ma_msgbus_subscriber_create(ma_msgbus_t *msgbus, ma_msgbus_subscriber_t **subscriber);

/* 
* Releases a subscriber
* @param subscriber     [in]        pointer to subscriber
*/
MA_MSGBUS_API ma_error_t ma_msgbus_subscriber_release(ma_msgbus_subscriber_t *subscriber);

/**
* Sets a desired option on a subscriber
* @param subscriber     [in]        pointer to subscriber
* @param option         [in]        any valid ma_msgbus_option_t 
* @param param          [in]        All options are set with the option followed by a parameter. That parameter can be a long, char* , depending on what the specific option expects.
*/
MA_MSGBUS_API ma_error_t ma_msgbus_subscriber_setopt(ma_msgbus_subscriber_t *subscriber,  ma_msgbus_option_t option, ...);

/**
* Callback for receiving PUB/SUB type messages
* @param topic          [in]            The topic that matched your topic_filter
* @param payload        [in]            The published message
* @param cb_data        [in, optional]  The callback data as it was specified in 'ma_msgbus_subscriber_register'
*/
typedef ma_error_t (*ma_msgbus_subscription_callback_t)(const char *topic, ma_message_t *payload, void *cb_data);

/* 
* Registers the subscription
* @param subscriber     [in]            pointer to subscriber
* @param topic_filter   [in]            subscription filter you are interested in
* @param cb             [in]            function to be called on receiving a published message that matched your topic_filter
* @param cb_data        [in,optional]   Some data that will be passed in the callback function. This is opaque to the msgbus framework
*/
MA_MSGBUS_API ma_error_t ma_msgbus_subscriber_register(ma_msgbus_subscriber_t *subscriber, const char *topic_filter, ma_msgbus_subscription_callback_t cb, void *cb_data);

/*
* Unregisters the subscription
* @param subscriber     [in]        pointer to subscriber
*/
MA_MSGBUS_API ma_error_t ma_msgbus_subscriber_unregister(ma_msgbus_subscriber_t *subscriber);


/***************************************************************************** PUB/SUB PUBLISHER *****************************************************************************/

/*
* Publish the message
* @param msgbus             [in]            msgbus instance
* @param topic              [in]            topic of a message
* @param pub_reachability   [in]            publish the message to this far
* @param payload            [in]            The published message
*/

MA_MSGBUS_API ma_error_t ma_msgbus_publish(ma_msgbus_t *msgbus, const char *topic, ma_msgbus_consumer_reachability_t pub_reachability,  ma_message_t *payload);

/***************************************************************************** MsgBus utils *****************************************************************************/

/*
* generates passphrase digest
* @param msgbus             [in]            msgbus instance
* @param passphrase         [in]            passphrase whose digest to be calculated
* @param passphrase_size    [in]            passphrase_size 
* @param passphrase_digest  [out]           passphrase_digest
*/

MA_MSGBUS_API ma_error_t ma_msgbus_passphrase_digest(ma_msgbus_t *msgbus, const void *passphrase, size_t passphrase_size, unsigned char passphrase_digest[32]);


MA_CPP(})

#include "ma/dispatcher/ma_msgbus_dispatcher.h"

#endif /* MA_MSGBUS_H_INCLUDED */
