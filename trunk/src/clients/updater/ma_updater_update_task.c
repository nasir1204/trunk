#include "ma/updater/ma_updater_update_task.h"
#include "ma/internal/defs/ma_updater_defs.h"
#include "ma/internal/defs/ma_general_defs.h"

#include "ma/internal/clients/ma/ma_client_internal.h"
#include "ma/internal/clients/ma/ma_client_base.h"
#include "ma/internal/clients/ma/client/ma_client_manager.h"

#include "ma/internal/ma_macros.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>


ma_error_t ma_updater_update_task_create(ma_client_t *ma_client, const char *task_id, ma_task_t **task) {
    if(task) {
        ma_task_t *self = NULL;
        ma_error_t rc = MA_OK;
        ma_context_t *ma_context = NULL;

        if((MA_OK == (rc = ma_client_get_context(ma_client, &ma_context))) && (ma_context)) {
            const char *creator_id = MA_CONTEXT_GET_PRODUCT_ID(ma_context);
        
            if(MA_OK == (rc = ma_task_create(task_id, &self))) {
                if(MA_OK == (rc = ma_task_set_type(self, MA_UPDATER_TASK_TYPE_UPDATE_STR))) {
                    (void)ma_task_set_creator_id(self, creator_id);
                    if(MA_OK == (rc = ma_task_set_software_id(self, MA_SOFTWAREID_GENERAL_STR))) {
                        const char *update_all = "1";
                        ma_variant_t *variant = NULL;
                        if(MA_OK == (rc = ma_variant_create_from_string(update_all, strlen(update_all), &variant))) {
                            (void)ma_task_set_setting(self, MA_SECTION_SELECTED_UPDATES_STR, MA_KEY_UPDATE_ALL_STR, variant);
                            ma_variant_release(variant);

                            *task = self;
                            return MA_OK;
                        }
                    }
                }
                (void)ma_task_release(self);
            }
            return rc;
        }
        return MA_ERROR_INVALID_CLIENT_OBJECT;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_updater_update_task_set_product_list(ma_task_t *self, ma_array_t *product_list) {
    if(self && product_list) {
        size_t l_number_of_updates = 0;
        ma_error_t rc = MA_OK;
        if(MA_OK == (rc = ma_array_size(product_list, &l_number_of_updates))) {
            /* if number of updates we have to make task setting similar to ePO server task 
                <Section name="SelectedUpdates">
                    <Setting name="NumOfUpdates" value="2"/>
                    <Setting name="UpdateAll" value="1"/>
                    <Setting name="Update_1" value="VSCANENG1000"/>
                    <Setting name="Update_2" value="VSCANDAT1000"/>
                </Section>
                <Section name="SelectedUpdates\VSCANDAT1000">
                    <Setting name="Update" value="1"/>
                </Section>
                <Section name="SelectedUpdates\VSCANENG1000">
                    <Setting name="Update" value="1"/>
                </Section>
            */
            if(l_number_of_updates > 0) {
                char s_number_of_updates[16] = {0};
                ma_variant_t *variant = NULL;

                MA_MSC_SELECT(_snprintf, snprintf)(s_number_of_updates, 16, "%d", l_number_of_updates);
                if(MA_OK == (rc = ma_variant_create_from_string(s_number_of_updates, strlen(s_number_of_updates), &variant))) {
                    int i = 0;
                    const char *update_all = "0";

                    (void)ma_task_set_setting(self, MA_SECTION_SELECTED_UPDATES_STR, MA_KEY_NUM_OF_UPDATES_STR, variant);
                    ma_variant_release(variant), variant = NULL;

                    for(i = l_number_of_updates; i > 0 ; i--) {
                        char selected_update_index[MA_MAX_LEN] = {0};
                        MA_MSC_SELECT(_snprintf, snprintf)(selected_update_index, MA_MAX_LEN -1, "%s_%d", MA_KEY_UPDATE_STR, i);

                        if(MA_OK == ma_array_get_element_at(product_list, i, &variant)) {
                            ma_buffer_t *buffer = NULL;
                            ma_task_set_setting(self, MA_SECTION_SELECTED_UPDATES_STR, selected_update_index, variant);
                            if(MA_OK == ma_variant_get_string_buffer(variant, &buffer)) {
                                const char *value = NULL;
                                size_t size = 0;
                                if(MA_OK == ma_buffer_get_string(buffer, &value, &size)) {
                                    char selected_update_section[MA_MAX_LEN] = {0};
                                    const char *update_enable = "1";
                                    ma_variant_t *update_enable_variant = NULL;
                                    if(MA_OK == ma_variant_create_from_string(update_enable, strlen(update_enable), &update_enable_variant)) {
                                        MA_MSC_SELECT(_snprintf, snprintf)(selected_update_section, MA_MAX_LEN -1, "%s\\%s", MA_SECTION_SELECTED_UPDATES_STR, value);
                                        ma_task_set_setting(self, selected_update_section, MA_KEY_UPDATE_STR, update_enable_variant);
                                        ma_variant_release(update_enable_variant);
                                    }
                                }
                                ma_buffer_release(buffer);
                            }
                            ma_variant_release(variant);
                        }
                    }
                    /* now set the update all to 0 */
                    if(MA_OK == (rc = ma_variant_create_from_string(update_all, strlen(update_all), &variant))) {
                        (void)ma_task_set_setting(self, MA_SECTION_SELECTED_UPDATES_STR, MA_KEY_UPDATE_ALL_STR, variant);
                        ma_variant_release(variant), variant = NULL;
                    }
                }
            }
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_updater_update_task_set_locale(ma_task_t *self, ma_uint32_t locale) {
    if(self) {
        ma_error_t rc = MA_OK;
        ma_variant_t *variant  = NULL;
        if(MA_OK == (rc = ma_variant_create_from_int32(locale, &variant))) {
            (void)ma_task_set_setting(self, MA_SECTION_GENERAL_STR, MA_KEY_LOCALE_INT, variant);
            ma_variant_release(variant);
        }

        return rc;
    }
    return MA_ERROR_INVALIDARG;
}
