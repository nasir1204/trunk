#include "ma/updater/ma_updater_ui_provider.h"


#include "ma/internal/clients/ma/ma_client_internal.h"
#include "ma/internal/clients/ma/ma_client_base.h"
#include "ma/internal/clients/ma/client/ma_client_manager.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/defs/ma_updater_defs.h"
#include "ma/internal/defs/ma_mue_defs.h"
#include "ma/internal/defs/ma_object_defs.h"

#ifdef MA_WINDOWS
#include <Windows.h>
#else
#include <unistd.h>
#endif

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

typedef struct ma_updater_ui_provider_client_s ma_updater_ui_provider_client_t, *ma_updater_ui_provider_client_h;

struct ma_updater_ui_provider_client_s {
    ma_client_base_t                                base_client ;

    ma_client_t                                     *ma_client;

    const ma_updater_ui_provider_callbacks_t        *provider_callbacks;

    void                                            *cb_data ;

    ma_msgbus_server_t                              *server;

	char											name[MAX_CLIENT_NAME_LENGTH];

	char											*software_id ;
};

struct ma_updater_show_ui_request_s {
	ma_uint32_t      initiatedType;
    ma_updater_ui_type_t    ui_type ;
    char            *title ;
    char            *message ;
    char            *count_down_message ;
    ma_uint32_t     count_down_value ;
} ;

struct ma_updater_show_ui_response_s {
    ma_msgbus_client_request_t *c_request ;
    ma_updater_show_ui_request_t *ui_request ;
    ma_updater_ui_return_code_t return_code ;
    ma_uint32_t     postpone_timeout ;
    ma_uint32_t     reboot_timeout ;
} ;

static ma_error_t ma_updater_ui_provider_client_create(ma_client_t *ma_client, const char *product_id, ma_msgbus_callback_thread_options_t cb_thread_option, ma_updater_ui_provider_callbacks_t const *callbacks, void *cb_data, ma_client_base_t **client);
static ma_error_t ma_updater_ui_provider_client_on_message(ma_client_base_t *client, const char *topic, ma_message_t *message) ;
static ma_error_t ma_updater_ui_provider_client_release(ma_client_base_t *client) ;

static ma_error_t ma_updater_show_ui_request_create(ma_updater_show_ui_request_t **ui_request) ;
static ma_error_t ma_updater_show_ui_request_release(ma_updater_show_ui_request_t *ui_request) ;
static ma_error_t ma_updater_show_ui_request_from_variant(ma_variant_t *var_show_ui_request, ma_updater_show_ui_request_t **ui_request) ;

static ma_error_t ma_updater_show_ui_response_create(ma_updater_show_ui_response_t **ui_response) ;
static ma_error_t ma_updater_show_ui_response_release(ma_updater_show_ui_response_t *ui_response) ;
static ma_error_t ma_updater_show_ui_response_as_variant(ma_updater_show_ui_response_t *ui_response, ma_variant_t **var_response) ;


/* utils functions */
static unsigned long get_session_id() ;
static unsigned long get_process_id() ;
static char *array_get_str_value(ma_array_t *tab, size_t index) ;
static ma_int64_t array_get_int_value(ma_array_t *tab, size_t index) ;

static void get_progress_params_from_variant(ma_variant_t *payload, ma_int32_t *event_type, ma_int32_t *progress_step, ma_int32_t *max_progress_step, char **progress_message) ;
static void get_end_ui_params_from_variant(ma_variant_t *payload, char **title, char ** end_message, char **count_down_message, ma_uint64_t *count_down_value) ;

static const ma_client_base_methods_t updater_client_methods = {
    &ma_updater_ui_provider_client_on_message,
    &ma_updater_ui_provider_client_release
};

ma_error_t ma_updater_register_ui_provider_callbacks(ma_client_t *ma_client, const char *software_id, ma_msgbus_callback_thread_options_t cb_thread_option, ma_updater_ui_provider_callbacks_t const *callbacks, void *cb_data) {
    if(ma_client && callbacks) {
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;
        const char *product_id = NULL;

		if(software_id && strlen(software_id) > 0) {
			product_id = software_id ;
		}
		else {
			product_id = MA_CONTEXT_GET_PRODUCT_ID(ma_context);
		}

		ma_client_get_context(ma_client, &ma_context);
		client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);	

        if(ma_context && client_manager && product_id) {       
            ma_error_t rc = MA_OK;
            char client_name[MAX_CLIENT_NAME_LENGTH] = {0} ;
            ma_client_base_t *client = NULL ;
		    
            MA_MSC_SELECT(_snprintf, snprintf)(client_name, MAX_CLIENT_NAME_LENGTH, "%s.%s.%lu.%lu", MA_UPDATER_SERVICE_UI_PROVIDER_STR, product_id, get_session_id(), get_process_id()) ;
            if(MA_OK == (rc = ma_client_manager_get_client(client_manager, client_name, &client)))
                return MA_ERROR_UPDATER_UI_PROVIDER_ALREADY_REGISTERED ;

            if(MA_OK == (rc =  ma_updater_ui_provider_client_create(ma_client, product_id, cb_thread_option, callbacks , cb_data, &client))) {
                if(MA_OK != (rc = ma_client_manager_add_client(client_manager, client_name, client)))
                    (void)ma_updater_ui_provider_client_release(client);
            }

            return rc ;
        }
        return MA_ERROR_INVALID_CLIENT_OBJECT;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_updater_get_ui_provider_name(ma_client_t *ma_client, const char *software_id, const char **ui_name){
	if(ma_client && ui_name) {
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;
        const char *product_id = NULL;

        if(software_id && strlen(software_id) > 0) {
			product_id = software_id ;
		}
		else {
				
			product_id = MA_CONTEXT_GET_PRODUCT_ID(ma_context);
		}
        
		ma_client_get_context(ma_client, &ma_context);
		client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);	

        if(ma_context && client_manager && product_id) {
            ma_error_t rc = MA_OK ;
            char client_name[MAX_CLIENT_NAME_LENGTH] = {0} ;
			ma_client_base_t *client = NULL ;
		
            MA_MSC_SELECT(_snprintf, snprintf)(client_name, MAX_CLIENT_NAME_LENGTH,"%s.%s.%lu.%lu",MA_UPDATER_SERVICE_UI_PROVIDER_STR, product_id, get_session_id(), get_process_id()) ;
            if(MA_OK != (rc = ma_client_manager_get_client(client_manager, client_name, &client)))
                return MA_ERROR_UPDATER_UI_PROVIDER_NOT_REGISTERED;

			*ui_name = (((ma_updater_ui_provider_client_t*)client)->name);
            return rc ;
        }
        return MA_ERROR_INVALID_CLIENT_OBJECT;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_updater_unregister_ui_provider_callbacks(ma_client_t *ma_client, const char *software_id) {
    if(ma_client) {
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;
        const char *product_id = NULL;
        
		if(software_id && strlen(software_id) > 0) {
			product_id = software_id ;
		}
		else {
				
			product_id = MA_CONTEXT_GET_PRODUCT_ID(ma_context);
		}
        
		ma_client_get_context(ma_client, &ma_context);
		client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);	

        if(ma_context && client_manager && product_id) {
            ma_error_t rc = MA_OK ;
            char client_name[MAX_CLIENT_NAME_LENGTH] = {0} ;
            ma_client_base_t *client = NULL ;
		
            MA_MSC_SELECT(_snprintf, snprintf)(client_name, MAX_CLIENT_NAME_LENGTH,"%s.%s.%lu.%lu",MA_UPDATER_SERVICE_UI_PROVIDER_STR, product_id, get_session_id(), get_process_id()) ;
            if(MA_OK != (rc = ma_client_manager_get_client(client_manager, client_name, &client)))
                return MA_ERROR_UPDATER_UI_PROVIDER_NOT_REGISTERED;

            /*(void)ma_updater_ui_provider_client_release(client) ;*/

            (void)ma_client_manager_remove_client(client_manager, client) ;

            (void)ma_client_base_release(client) ;

            return rc ;
        }
        return MA_ERROR_INVALID_CLIENT_OBJECT;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_updater_show_ui_response_post(ma_client_t *ma_client, ma_updater_show_ui_response_t *show_ui_response ) {
    if(ma_client && show_ui_response) {
        ma_error_t rc = MA_OK ;
        ma_message_t *response = NULL ;

        if(MA_OK == (rc = ma_message_create(&response))) {
            ma_variant_t *payload = NULL ;

            (void)ma_message_set_property(response, MA_UPDATER_MSG_PROP_KEY_REQUEST_TYPE_STR, MA_UPDATER_UIPROVIDER_MSG_PROP_VAL_REPLY_SHOWUI_STR) ;
            ma_updater_show_ui_response_as_variant(show_ui_response, &payload) ;
            (void)ma_message_set_payload(response, payload) ;
			ma_variant_release(payload);
            rc = ma_msgbus_server_client_request_post_result(show_ui_response->c_request, rc, response) ;
		    (void)ma_message_release(response);
		}
		ma_msgbus_client_request_release(show_ui_response->c_request) ;
        ma_updater_show_ui_request_release(show_ui_response->ui_request) ;
        ma_updater_show_ui_response_release(show_ui_response) ;

        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}


static ma_error_t ma_updater_ui_provider_handler(ma_msgbus_server_t *server, ma_message_t *request, void *cb_data, ma_msgbus_client_request_t *c_request);

static ma_error_t ma_updater_ui_provider_client_create(ma_client_t *ma_client, const char *product_id, ma_msgbus_callback_thread_options_t cb_thread_option, ma_updater_ui_provider_callbacks_t const *callbacks, void *cb_data, ma_client_base_t **client) {
    ma_context_t *ma_context = NULL;
    ma_client_manager_t *client_manager = NULL;
    ma_msgbus_t *msgbus = NULL;

    ma_client_get_context(ma_client, &ma_context);
    client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);		
    msgbus = MA_CONTEXT_GET_MSGBUS(ma_context);

    if(ma_context && client_manager && msgbus) {
        ma_error_t rc = MA_OK ;
        ma_updater_ui_provider_client_t *self = NULL ;
        ma_msgbus_consumer_reachability_t consumer_reach = MSGBUS_CONSUMER_REACH_OUTPROC;

        (void) ma_client_manager_get_consumer_reachability(client_manager, &consumer_reach);
                
        
        self = (ma_updater_ui_provider_client_t *)calloc(1, sizeof(ma_updater_ui_provider_client_t)) ;
        if(!self) return MA_ERROR_OUTOFMEMORY ;

		MA_MSC_SELECT(_snprintf, snprintf)(self->name, MAX_CLIENT_NAME_LENGTH, "%s.%s.%lu.%lu", MA_UPDATER_SERVICE_UI_PROVIDER_STR, product_id, get_session_id(), get_process_id()) ;
        if(MA_OK == (rc = ma_msgbus_server_create(msgbus, self->name, &self->server))) {
            ma_msgbus_server_setopt(self->server, MSGBUSOPT_THREADMODEL, cb_thread_option) ;
            ma_msgbus_server_setopt(self->server, MSGBUSOPT_REACH, consumer_reach) ;

            if(MA_OK == (rc = ma_msgbus_server_start(self->server, ma_updater_ui_provider_handler, (void *)self))) {
                self->provider_callbacks = callbacks;
                self->cb_data = cb_data ;
                self->ma_client = ma_client ;
				self->software_id = strdup(product_id) ;
                ma_client_base_init((ma_client_base_t *)self, &updater_client_methods, self) ;
                *client = (ma_client_base_t *)self ;
                return MA_OK ;
            }   
            (void)ma_msgbus_server_release(self->server);
        }
        free(self);
        return rc;
    }   
    return MA_ERROR_INVALID_CLIENT_OBJECT;
}

static ma_error_t ma_updater_ui_provider_client_release(ma_client_base_t *client) {
    if(client) {
        ma_updater_ui_provider_client_t *self = (ma_updater_ui_provider_client_t *)client ;
        
		if(self->software_id)
			free(self->software_id) ;

        if(self->server) {
            (void)ma_msgbus_server_stop(self->server) ;
            (void)ma_msgbus_server_release(self->server) ;
        }
        
        free(self) ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

static ma_error_t ma_updater_ui_provider_client_on_message(ma_client_base_t *client, const char *topic, ma_message_t *message) {
	ma_updater_ui_provider_client_t *ui_provider = (ma_updater_ui_provider_client_t *)client ;
    if(ui_provider->provider_callbacks && !strcmp(topic, MA_MUEEVENT_PUBLISH_TOPIC )) {
        const char *op_type = NULL ;
        const char *session_id = NULL ;
        ma_variant_t *payload = NULL ;        

        (void)ma_message_get_property(message, MA_MUEEVENT_PROP_OPERATION_TYPE, &op_type) ;
        (void)ma_message_get_property(message, MA_MUEEVENT_PROP_SESSION_ID, &session_id) ;
        (void)ma_message_get_payload(message, &payload) ;

		if(ui_provider->provider_callbacks){
			switch(atoi(op_type)) {
				case eOpProgress :
				{
					if(ui_provider->provider_callbacks->on_progress_cb){
						ma_int32_t event_type = 0, progress_step = 0 , max_progress_step = 0 ;
						char *progress_message = NULL ;

						get_progress_params_from_variant(payload, &event_type, &progress_step, &max_progress_step, &progress_message) ;
						ui_provider->provider_callbacks->on_progress_cb(ui_provider->ma_client, ui_provider->cb_data, session_id, event_type, progress_step, max_progress_step, progress_message) ;						
					}
					break ;
				}
				case eOpEvent :
				{
					if(ui_provider->provider_callbacks->on_update_event_cb){
						ma_updater_event_t *update_event = NULL ;
                    
						if(MA_OK == ma_updater_event_from_variant(payload, session_id, &update_event) && update_event){
							ui_provider->provider_callbacks->on_update_event_cb(ui_provider->ma_client, ui_provider->cb_data, session_id, update_event) ;
							ma_updater_event_release(update_event) ;
						}						
					}
					break ;
				}
				case eOpEndUpdateDialog :
				{
					if(ui_provider->provider_callbacks->on_end_ui_cb){
						char *title = NULL, *end_message = NULL , *count_down_message = NULL;
						ma_uint64_t count_down_value = 0;
                               
						get_end_ui_params_from_variant(payload, &title, &end_message, &count_down_message, &count_down_value);
						ui_provider->provider_callbacks->on_end_ui_cb(ui_provider->ma_client, ui_provider->cb_data, session_id, title, end_message, count_down_message, count_down_value) ;						
					}
					break;
				}
				default :
					break ;
			}
		}
		(void)ma_variant_release(payload);
    }
    return MA_OK ;
}

static ma_error_t ma_updater_ui_provider_handler(ma_msgbus_server_t *server, ma_message_t *request, void *cb_data, ma_msgbus_client_request_t *c_request) {
    if(server && request && c_request) {
        ma_error_t rc = MA_OK ;
        const char *rqst_type = NULL, *session_id = NULL ;        
        ma_updater_ui_provider_client_t *ui_provider = (ma_updater_ui_provider_client_t *)cb_data ;
        ma_variant_t *payload = NULL;

        (void)ma_message_get_property(request, MA_UPDATER_MSG_PROP_KEY_REQUEST_TYPE_STR, &rqst_type) ;
        (void)ma_message_get_property(request, MA_MUEEVENT_PROP_SESSION_ID, &session_id) ;
                
        if(!strcmp(rqst_type, MA_UPDATER_UIPROVIDER_MSG_PROP_VAL_REQUEST_SHOWUI_STR) && ui_provider->provider_callbacks && ui_provider->provider_callbacks->on_show_ui_cb) {
			
            ma_updater_show_ui_request_t *show_ui_request = NULL ;
            ma_updater_show_ui_response_t *show_ui_response = NULL ;
            
            ma_message_get_payload(request, &payload) ;                      
            ma_updater_show_ui_request_from_variant(payload, &show_ui_request) ;
            
            ma_updater_show_ui_response_create(&show_ui_response) ;
            ma_msgbus_client_request_add_ref(show_ui_response->c_request = c_request);

            show_ui_response->ui_request = show_ui_request ;
			
            rc = ui_provider->provider_callbacks->on_show_ui_cb(ui_provider->ma_client, ui_provider->cb_data, session_id, show_ui_request->ui_type, show_ui_request, show_ui_response) ;
        }
        else {
            rc = MA_ERROR_UNEXPECTED ;
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

static ma_error_t ma_updater_show_ui_request_create(ma_updater_show_ui_request_t **ui_request) {
    if(ui_request) {
        ma_updater_show_ui_request_t *tmp = (ma_updater_show_ui_request_t *)calloc(1, sizeof(ma_updater_show_ui_request_t)) ;
        if(tmp) {
            tmp->title = NULL ;
            tmp->message = NULL ;
            tmp->count_down_message = NULL ;
            tmp->count_down_value = 0 ;
            *ui_request = tmp ;
            return MA_OK ;
        }
        return MA_ERROR_OUTOFMEMORY ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_updater_show_ui_request_get_initiator_type(const ma_updater_show_ui_request_t *ui_request, ma_uint32_t *initiated_type) {
	if(ui_request && initiated_type) {
		*initiated_type = ui_request->initiatedType;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_updater_show_ui_request_get_title(const ma_updater_show_ui_request_t *ui_request, const char **title) {
    if(ui_request && title) {
        *title = ui_request->title ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_updater_show_ui_request_get_message(const ma_updater_show_ui_request_t *ui_request, const char **message) {
    if(ui_request && message) {
        *message = ui_request->message ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_updater_show_ui_request_get_count_down_message(const ma_updater_show_ui_request_t *ui_request, const char **count_down_message) {
    if(ui_request && count_down_message) {
        *count_down_message = ui_request->count_down_message ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_updater_show_ui_request_get_count_down_value(const ma_updater_show_ui_request_t *ui_request, ma_uint32_t *count_down_value) {
    if(ui_request && count_down_value) {
        *count_down_value = ui_request->count_down_value ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

static ma_error_t ma_updater_show_ui_request_from_variant(ma_variant_t *var_show_ui_request, ma_updater_show_ui_request_t **ui_request) {
    if(var_show_ui_request && ui_request) {
        ma_error_t rc = MA_OK ;
        ma_array_t *arr_show_ui_request = NULL ;

        if(MA_OK == (rc = ma_variant_get_array(var_show_ui_request, &arr_show_ui_request))) {
            ma_updater_show_ui_request_t *tmp = NULL ;
            if(MA_OK == ma_updater_show_ui_request_create(&tmp)) {
				tmp->initiatedType = (ma_uint32_t) array_get_int_value(arr_show_ui_request, 1) ;
                tmp->ui_type = (ma_updater_ui_type_t)array_get_int_value(arr_show_ui_request, 2) ;
                tmp->title = strdup(array_get_str_value(arr_show_ui_request, 3)) ;
                tmp->message = strdup(array_get_str_value(arr_show_ui_request, 4)) ;
                tmp->count_down_message = strdup(array_get_str_value(arr_show_ui_request, 5)) ;
                tmp->count_down_value = (ma_uint32_t)array_get_int_value(arr_show_ui_request, 6) ;
                *ui_request = tmp ;
            }
            (void)ma_array_release(arr_show_ui_request) ;
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;  
}

static ma_error_t ma_updater_show_ui_request_release(ma_updater_show_ui_request_t *ui_request) {
    if(ui_request) {
        if(ui_request->title)  free(ui_request->title) ;
        if(ui_request->message)  free(ui_request->message) ;
        if(ui_request->count_down_message)  free(ui_request->count_down_message) ;
        free(ui_request) ; ui_request = NULL ;
    }
    return MA_ERROR_INVALIDARG ;
}

static ma_error_t ma_updater_show_ui_response_create(ma_updater_show_ui_response_t **ui_response) {
    if(ui_response){
        ma_updater_show_ui_response_t *tmp = (ma_updater_show_ui_response_t *)calloc(1, sizeof(ma_updater_show_ui_response_t)) ;
        if(tmp) {
            tmp->c_request = NULL ;
            tmp->postpone_timeout = 0 ;
            tmp->reboot_timeout = 0 ;
            tmp->return_code = MA_UPDATER_UI_RC_REBOOT_UNKNOWN ;
            tmp->ui_request = NULL ;
            
            *ui_response = tmp ;

            return MA_OK ;
        }
        return MA_ERROR_OUTOFMEMORY ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_updater_show_ui_response_set_return_code(ma_updater_show_ui_response_t *response, ma_updater_ui_return_code_t return_code) {
    if(response) {
        response->return_code = return_code ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_updater_show_ui_response_set_postpone_timeout(ma_updater_show_ui_response_t *response, ma_uint32_t postpone_timeout) {
    if(response) {
        response->postpone_timeout = postpone_timeout ;
		return MA_OK;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_updater_show_ui_response_set_reboot_timeout(ma_updater_show_ui_response_t *response, ma_uint32_t reboot_timeout) {
    if(response) {
        response->reboot_timeout = reboot_timeout ;
		return MA_OK;
    }
    return MA_ERROR_INVALIDARG ;

}

static ma_error_t ma_updater_show_ui_response_release(ma_updater_show_ui_response_t *ui_response) {
    if(ui_response) {
        free(ui_response) ;
    }
    return MA_OK ;
}

static ma_error_t ma_updater_show_ui_response_as_variant(ma_updater_show_ui_response_t *ui_response, ma_variant_t **var_response) {
    if(ui_response && var_response) {
        ma_error_t rc = MA_OK ;
        ma_array_t *arr = NULL ;

        if(MA_OK == (rc = ma_array_create(&arr))) {
            ma_variant_t *value = NULL ;

            (void)ma_variant_create_from_int64((ma_int32_t)ui_response->return_code, &value) ;
            (void)ma_array_push(arr, value) ;
            (void)ma_variant_release(value) ; value = NULL ;

            if(MA_UPDATER_UI_TYPE_REBOOT == ui_response->ui_request->ui_type) {
                (void)ma_variant_create_from_int64(ui_response->reboot_timeout, &value) ;
                (void)ma_array_push(arr, value) ;
                (void)ma_variant_release(value) ; value = NULL ;
            }

            if(MA_UPDATER_UI_TYPE_POSTPONE == ui_response->ui_request->ui_type) {
                (void)ma_variant_create_from_int64(ui_response->postpone_timeout, &value) ;
                (void)ma_array_push(arr, value) ;
                (void)ma_variant_release(value) ; value = NULL ;
            }
            rc = ma_variant_create_from_array(arr, var_response) ;
            (void)ma_array_release(arr) ; arr = NULL ;
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}


static ma_int64_t array_get_int_value(ma_array_t *arr, size_t index) {
    ma_int64_t int_value = 0 ;
    ma_variant_t *var_value = NULL ;
    if(MA_OK == ma_array_get_element_at(arr, index, &var_value)) {
        (void)ma_variant_get_int64(var_value, &int_value) ;
        (void)ma_variant_release(var_value) ; var_value = NULL ;
    }
    return int_value ;
}

static char *array_get_str_value(ma_array_t *arr, size_t index) {
    char *str_value = NULL ;
    size_t str_len = 0 ;
    ma_variant_t *var_value = NULL ;
    if(MA_OK == ma_array_get_element_at(arr, index, &var_value)) {
        ma_buffer_t *buf = NULL ;
        (void)ma_variant_get_string_buffer(var_value, &buf) ;
        (void)ma_buffer_get_string(buf, &str_value, &str_len) ;
        (void)ma_buffer_release(buf) ;
        (void)ma_variant_release(var_value) ; var_value = NULL ;
    }
    return str_value ;
}

static void get_progress_params_from_variant(ma_variant_t *payload, ma_int32_t *event_type, ma_int32_t *progress_step, ma_int32_t *max_progress_step, char **progress_message) {
    ma_array_t *arr_progress_params = NULL ;

    if(MA_OK == ma_variant_get_array(payload, &arr_progress_params)) {
		*event_type = (ma_int32_t) array_get_int_value(arr_progress_params, 1);
        *progress_step = (ma_int32_t)array_get_int_value(arr_progress_params, 2) ;
        *max_progress_step = (ma_int32_t)array_get_int_value(arr_progress_params, 3) ;
        *progress_message = array_get_str_value(arr_progress_params, 4) ;

        (void)ma_array_release(arr_progress_params) ;
    }
    return ;
}

static void get_end_ui_params_from_variant(ma_variant_t *payload, char **title, char ** end_message, char **count_down_message, ma_uint64_t *count_down_value) {
    ma_array_t *arr_end_ui_params = NULL ;

    if(MA_OK == ma_variant_get_array(payload, &arr_end_ui_params)) {
        *title = array_get_str_value(arr_end_ui_params, 1) ;
        *end_message = array_get_str_value(arr_end_ui_params, 2) ;
		*count_down_message = array_get_str_value(arr_end_ui_params, 3) ;
		*count_down_value = array_get_int_value(arr_end_ui_params, 4) ;

        (void)ma_array_release(arr_end_ui_params) ;
    }
    return ;
}

static unsigned long get_session_id() {
#ifdef MA_WINDOWS
    DWORD session_id = 0 ;
    ProcessIdToSessionId(GetCurrentProcessId(), &session_id) ;
    return (unsigned long)session_id ;
#else
    return (unsigned long)getsid(getpid()) ;
#endif
}

static unsigned long get_process_id() {
	/*TBD: this needs to be removed*/
	return 0;
}


