#include "ma/internal/clients/updater/ma_update_info_sync.h"

#include <stdlib.h>
#include <string.h>

struct ma_update_info_sync_s {
    /* type ma_uint16_t */
    ma_variant_t                    *request_type;
	/*update state */
    ma_variant_t                    *update_state;	
	/*update type */
    ma_variant_t                    *update_type;	
    /*type string */
    ma_variant_t                    *software_id;
    /*type ma_int16_t */
    ma_variant_t                    *product_return_code;
    /*type string */
    ma_variant_t                    *key;
    /*type string */
    ma_variant_t                    *value;
	/*type string */
    ma_variant_t                    *message;
	/*type string */
    ma_variant_t                    *extra_info;

};

ma_error_t ma_update_info_sync_create(ma_update_info_sync_t **sync) {
    if(sync) {       
        ma_update_info_sync_t *self = (ma_update_info_sync_t *)calloc(1, sizeof(ma_update_info_sync_t));
        if(!self)       return MA_ERROR_OUTOFMEMORY;

        *sync = self;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_update_info_sync_release(ma_update_info_sync_t *self) {
    if(self) {
        (void)ma_variant_release(self->request_type);
		(void)ma_variant_release(self->update_state);
		(void)ma_variant_release(self->update_type);
        (void)ma_variant_release(self->software_id);
        (void)ma_variant_release(self->product_return_code);
        (void)ma_variant_release(self->key);
        (void)ma_variant_release(self->value);
		(void)ma_variant_release(self->message);
		(void)ma_variant_release(self->extra_info);		
        free(self);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


static ma_error_t get_string(ma_variant_t *var, const char **out){
    if(var && out) {
        ma_error_t rc = MA_OK;
        ma_buffer_t *buffer = NULL;
		*out = NULL;
        if(MA_OK == (rc = ma_variant_get_string_buffer(var, &buffer))) {
            size_t size = 0;
            rc = ma_buffer_get_string(buffer, out, &size);
            ma_buffer_release(buffer);
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_update_info_sync_set_type(ma_update_info_sync_t *self, ma_update_info_sync_type_t request_type) {
     return (self) ? ma_variant_create_from_int16(request_type, &self->request_type) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_update_info_sync_get_type(ma_update_info_sync_t *self, ma_update_info_sync_type_t *request_type) {
	if(self && request_type){
		ma_int16_t int16_type = 0;
		ma_error_t rc = MA_OK;
		if(MA_OK == (rc = ma_variant_get_int16(self->request_type, &int16_type))){
			*request_type = (ma_update_info_sync_type_t)int16_type;
		}
		return rc;
	}
	return MA_ERROR_INVALIDARG;
    
}

ma_error_t ma_update_info_sync_get_update_state(ma_update_info_sync_t *self, ma_update_state_t *state){
	if(self && state){
		ma_int16_t int16_state = 0;
		ma_error_t rc = MA_OK;
		if(MA_OK == (rc = ma_variant_get_int16(self->update_state, &int16_state))){
			*state = (ma_update_state_t)int16_state;
		}
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_update_info_sync_set_update_state(ma_update_info_sync_t *self, ma_update_state_t state){
	return (self) ? ma_variant_create_from_int16(state, &self->update_state) : MA_ERROR_INVALIDARG;
}


ma_error_t ma_update_info_sync_set_software_id(ma_update_info_sync_t *self, const char *software_id) {
    return (self && software_id) ? ma_variant_create_from_string(software_id, strlen(software_id), &self->software_id) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_update_info_sync_get_software_id(ma_update_info_sync_t *self, const char **software_id) {
    if(self && software_id) {
		return get_string(self->software_id, software_id);        
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_update_info_sync_set_message(ma_update_info_sync_t *self, const char *message){
	return (self && message) ? ma_variant_create_from_string(message, strlen(message), &self->message) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_update_info_sync_get_message(ma_update_info_sync_t *self, const char **message){
	if(self && message) {
		return get_string(self->message, message);                
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_update_info_sync_set_update_type(ma_update_info_sync_t *self, const char *update_type){
	return (self && update_type) ? ma_variant_create_from_string(update_type, strlen(update_type), &self->update_type) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_update_info_sync_get_update_type(ma_update_info_sync_t *self, const char **update_type){
	if(self && update_type) {
		return get_string(self->update_type, update_type);                
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_update_info_sync_set_extra_info(ma_update_info_sync_t *self, const char *extra_info){
	  return (self && extra_info) ? ma_variant_create_from_string(extra_info, strlen(extra_info), &self->extra_info) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_update_info_sync_get_extra_info(ma_update_info_sync_t *self, const char **extra_info){
	if(self && extra_info) {
        return get_string(self->extra_info, extra_info);                        
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_update_info_sync_set_product_response_code(ma_update_info_sync_t *self, ma_updater_product_return_code_t product_return_code)  {
     return (self) ? ma_variant_create_from_int16(product_return_code, &self->product_return_code) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_update_info_sync_get_product_response_code(ma_update_info_sync_t *self, ma_updater_product_return_code_t *product_return_code)  {
    if(self && product_return_code){
		ma_int16_t int16 = 0;
		ma_error_t rc = MA_OK;
		if(MA_OK == (rc = ma_variant_get_int16(self->product_return_code, &int16))){
			*product_return_code = (ma_updater_product_return_code_t)int16;
		}
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_update_info_sync_set_info_key(ma_update_info_sync_t *self, const char *key)  {
    return (self && key) ? ma_variant_create_from_string(key, strlen(key), &self->key) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_update_info_sync_get_info_key(ma_update_info_sync_t *self, const char **key) {
    if(self && key) {
		return get_string(self->key, key);                                
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_update_info_sync_set_info_value(ma_update_info_sync_t *self, ma_variant_t *value) {
    return (self && value) ? ma_variant_add_ref(self->value = value) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_update_info_sync_get_info_value(ma_update_info_sync_t *self, ma_variant_t **value) {
    return (self && value && self->value) ? ma_variant_add_ref(*value = self->value) :  MA_ERROR_INVALIDARG;
}

/* Helper functions */
static ma_error_t pop_members(ma_array_t *array, size_t index, ma_variant_t **member) {
    ma_error_t rc = MA_OK;
    ma_variant_t *variant = NULL;

    if(MA_OK == (rc = ma_array_get_element_at(array, index, &variant))) {
        ma_vartype_t var_type = MA_VARTYPE_NULL;
        if(MA_OK == (rc = ma_variant_get_type(variant, &var_type))) {
            if(MA_VARTYPE_NULL != var_type) {
                *member = variant;
                return MA_OK;
            }
        }
        ma_variant_release(variant);
    }
    return rc;

}

ma_error_t ma_update_info_sync_from_variant(ma_variant_t *variant, ma_update_info_sync_t **sync_request) {
    if(variant && sync_request) {
        ma_error_t rc = MA_OK ;
        ma_array_t *array = NULL;
        ma_update_info_sync_t *self = (ma_update_info_sync_t *)calloc(1, sizeof(ma_update_info_sync_t));
        if(!self)       return MA_ERROR_OUTOFMEMORY;

        if(MA_OK == (rc = ma_variant_get_array(variant, &array))) {
            size_t index = 0;
            rc = (
                    (MA_OK == (rc = pop_members(array, ++index, &self->request_type))) && 
					(MA_OK == (rc = pop_members(array, ++index, &self->update_state))) && 
					(MA_OK == (rc = pop_members(array, ++index, &self->update_type))) && 
                    (MA_OK == (rc = pop_members(array, ++index, &self->software_id))) && 
                    (MA_OK == (rc = pop_members(array, ++index, &self->product_return_code))) && 
                    (MA_OK == (rc = pop_members(array, ++index, &self->key))) && 
                    (MA_OK == (rc = pop_members(array, ++index, &self->value))) &&
					(MA_OK == (rc = pop_members(array, ++index, &self->message))) &&
					(MA_OK == (rc = pop_members(array, ++index, &self->extra_info))) 
                ) ? MA_OK : rc;
            
            ma_array_release(array);
        }
        if(MA_OK == rc) 
            *sync_request = self;
        else
            free(self);
        return rc;
    }
    return MA_ERROR_INVALIDARG;



}

static ma_error_t push_members(ma_array_t *array, ma_variant_t *member) {
    ma_error_t rc = MA_OK ;
    if(member) 
        rc = ma_array_push(array, member);
    else {
        ma_variant_t *variant = NULL;
        if(MA_OK == (rc = ma_variant_create(&variant))) {
            rc = ma_array_push(array, variant);
            ma_variant_release(variant);
        }
    }
    return rc;
}

ma_error_t ma_update_info_sync_to_variant(ma_update_info_sync_t *self, ma_variant_t **variant) {
    if(self && variant) {
        ma_error_t rc = MA_OK;
        ma_array_t *array = NULL;
        if(MA_OK == (rc = ma_array_create(&array))) {
            ma_bool_t b_ret = MA_FALSE;

            b_ret = (MA_OK == (rc = push_members(array, self->request_type))) && 
					(MA_OK == (rc = push_members(array, self->update_state))) && 
					(MA_OK == (rc = push_members(array, self->update_type))) && 
                    (MA_OK == (rc = push_members(array, self->software_id))) && 
                    (MA_OK == (rc = push_members(array, self->product_return_code))) &&
                    (MA_OK == (rc = push_members(array, self->key))) &&
                    (MA_OK == (rc = push_members(array, self->value))) &&
					(MA_OK == (rc = push_members(array, self->message))) &&
					(MA_OK == (rc = push_members(array, self->extra_info)))
					;
            if(b_ret)
                rc = ma_variant_create_from_array(array, variant);
            ma_array_release(array);
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}


