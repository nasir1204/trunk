#include "ma/updater/ma_updater_update_request.h"

#include <stdlib.h>
#include <string.h>

struct ma_updater_update_request_s {
    ma_array_t                          *products_list;
    ma_uint32_t                         locale;
	ma_bool_t							ui_enabled;
    ma_updater_update_type_t            update_type;
	ma_updater_initiator_type_t			initiator_type ;
	char								*ui_name;
};

ma_error_t ma_updater_update_request_create(ma_updater_update_type_t update_type, ma_updater_update_request_t **request) {
    if(request) {
        ma_updater_update_request_t *self = NULL;
        if(!((MA_UPDATER_UPDATE_TYPE_NORMAL_UPDATE == update_type) || (MA_UPDATER_UPDATE_TYPE_ROLLBACK_UPDATE == update_type)))
            return MA_ERROR_INVALIDARG;

        self = (ma_updater_update_request_t*)calloc(1, sizeof(ma_updater_update_request_t));
        if(!self)   return MA_ERROR_OUTOFMEMORY;

        self->update_type = update_type;
		if(MA_UPDATER_UPDATE_TYPE_ROLLBACK_UPDATE == update_type)
			self->initiator_type = MA_UPDATER_INITIATOR_TYPE_ONDEMAND_ROLLBACK ;
		else
			self->initiator_type = MA_UPDATER_INITIATOR_TYPE_ONDEMAND_UPDATE ;
		self->ui_enabled = MA_TRUE;
        *request = self;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_updater_update_request_release(ma_updater_update_request_t *self) {
    if(self) {
        (void)ma_array_release(self->products_list);
		if(self->ui_name)free(self->ui_name);
        free(self);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_updater_update_request_set_product_list(ma_updater_update_request_t *self, ma_array_t *product_list) {
    return (self && product_list) ? ma_array_add_ref(self->products_list = product_list) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_updater_update_request_set_initiator_type(ma_updater_update_request_t *self, ma_updater_initiator_type_t initiator_type) {
    if(self) {
        self->initiator_type = initiator_type ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_updater_update_request_set_locale(ma_updater_update_request_t *self, ma_uint32_t locale) {
    if(self) {
        self->locale = locale;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_updater_update_request_set_uioption(ma_updater_update_request_t *self, ma_bool_t enabled){
	if(self) {
        self->ui_enabled = enabled;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_updater_update_request_get_uioption(ma_updater_update_request_t *self, ma_bool_t *enabled){
	if(self) {
		*enabled = self->ui_enabled;        
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_updater_update_request_get_product_list(ma_updater_update_request_t *self, ma_array_t **product_list) {
    return (self && product_list)  ? ma_array_add_ref((*product_list = self->products_list)) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_updater_update_request_get_locale(ma_updater_update_request_t *self, ma_uint32_t *locale) {
    if(self && locale && self->locale) {
        *locale = self->locale;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_updater_update_request_get_type(ma_updater_update_request_t *self, ma_updater_update_type_t *update_type) {
   if(self && update_type) {
        *update_type = self->update_type;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_updater_update_request_get_initiator_type(ma_updater_update_request_t *self, ma_updater_initiator_type_t *initiator_type) {
   if(self && initiator_type) {
        *initiator_type = self->initiator_type ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}


ma_error_t ma_updater_update_request_set_ui_provider_name(ma_updater_update_request_t *self, const char *name){
	if(self && name){
		self->ui_name = strdup(name);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_updater_update_request_get_ui_provider_name(ma_updater_update_request_t *self, const char **name){
	if(self && name){
		if(self->ui_name){
			*name = self->ui_name;
			return MA_OK;
		}
		return MA_ERROR_OBJECTNOTFOUND;
	}
	return MA_ERROR_INVALIDARG;
}

