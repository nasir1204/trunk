#include "ma/updater/ma_updater_event.h"

#include <stdlib.h>
#include <string.h>

static char *null_string = "";

struct ma_updater_event_s {
	char            *session_id ;
	char            *product_id ;
    char            *locale ;
    char            *update_type ;
	char            *new_version ;
    char            *date_time ;    
    ma_uint32_t     severity ;    
    ma_int32_t      update_state ;
    ma_int32_t      update_error ;    
	long            event_id ;
    long            script_id ;
} ;

static ma_int64_t array_get_int_value(ma_array_t *arr, size_t index) {
    ma_int64_t int_value = 0 ;
    ma_variant_t *var_value = NULL ;
    if(MA_OK == ma_array_get_element_at(arr, index, &var_value)) {
        if(MA_OK == ma_variant_get_int64(var_value, &int_value))
			(void)ma_variant_release(var_value) ; 		
    }
    return int_value ;
}

static char *array_get_str_value(ma_array_t *arr, size_t index) {
    char *str_value = NULL ;
    size_t str_len = 0 ;
    ma_variant_t *var_value = NULL ;
    if(MA_OK == ma_array_get_element_at(arr, index, &var_value)) {
        ma_buffer_t *buf = NULL ;
        if(MA_OK == ma_variant_get_string_buffer(var_value, &buf)){
			ma_buffer_get_string(buf, &str_value, &str_len) ;
			(void)ma_buffer_release(buf) ;
		}
        (void)ma_variant_release(var_value) ; var_value = NULL ;
    }
    return (str_value ? str_value : null_string);
}

static ma_error_t ma_updater_event_create(ma_updater_event_t **updater_event) {
    if(updater_event) {
        ma_updater_event_t *tmp = (ma_updater_event_t *)calloc(1, sizeof(ma_updater_event_t)) ;
        if(tmp) {
			tmp->session_id = NULL;
            tmp->event_id = -1 ;
            tmp->severity = 0 ;
            tmp->product_id = NULL ;
            tmp->locale = NULL ;
            tmp->update_type = NULL ;
            tmp->update_state = 0 ;
            tmp->update_error = 0 ;
            tmp->new_version = NULL ;
            tmp->date_time = NULL ;
            tmp->script_id = 0 ;
            *updater_event = tmp ;
            return MA_OK ;
        }
        return MA_ERROR_OUTOFMEMORY ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_updater_event_release(ma_updater_event_t *updater_event) {
    if(updater_event) {
		if(updater_event->session_id)   free(updater_event->session_id) ;
        if(updater_event->product_id)   free(updater_event->product_id) ;
        if(updater_event->locale)		free(updater_event->locale) ;
        if(updater_event->update_type)  free(updater_event->update_type) ;
        if(updater_event->new_version)  free(updater_event->new_version) ;
        if(updater_event->date_time)    free(updater_event->date_time) ;        
        free(updater_event) ;   
		updater_event = NULL ;
		
		return MA_OK ;
    }
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_updater_event_from_variant(ma_variant_t *var_updater_event, const char *session_id, ma_updater_event_t **updater_event){
    if(var_updater_event && session_id && updater_event) {
        ma_updater_event_t *tmp = NULL ;
		ma_error_t rc = MA_OK;
        if(MA_OK == (rc = ma_updater_event_create(&tmp))) {
            ma_array_t *arr_updater_event = NULL ;

            if(MA_OK == (rc = ma_variant_get_array(var_updater_event, &arr_updater_event))) {
				tmp->session_id		= strdup(session_id) ;
                tmp->event_id		= (long)array_get_int_value(arr_updater_event, 1) ;
                tmp->severity		= (ma_uint32_t)array_get_int_value(arr_updater_event, 2) ;
                tmp->product_id		= strdup(array_get_str_value(arr_updater_event, 3)) ;
                tmp->locale			= strdup(array_get_str_value(arr_updater_event, 4)) ;
                tmp->update_type	= strdup(array_get_str_value(arr_updater_event, 5)) ;
                tmp->update_state	= (ma_int32_t)array_get_int_value(arr_updater_event, 6) ;
                tmp->update_error	= (ma_int32_t)array_get_int_value(arr_updater_event, 7) ;
                tmp->new_version	= strdup(array_get_str_value(arr_updater_event, 8)) ;
                tmp->date_time		= strdup(array_get_str_value(arr_updater_event, 9)) ;
                tmp->script_id		= (long)array_get_int_value(arr_updater_event, 10) ;

				*updater_event = tmp ;
                ma_array_release(arr_updater_event) ;
				return MA_OK;
            }                
			ma_updater_event_release(tmp);
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_updater_event_get_event_id(const ma_updater_event_t *updater_event, long *event_id) {
    if(updater_event && event_id) {
        *event_id = updater_event->event_id ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_updater_event_get_severity(const ma_updater_event_t *updater_event, ma_int32_t *severity) {
    if(updater_event && severity) {
        *severity = updater_event->severity ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_updater_event_get_product_id(const ma_updater_event_t *updater_event, const char **product_id) {
    if(updater_event && product_id) {
        *product_id = updater_event->product_id ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_updater_event_get_locale(const ma_updater_event_t *updater_event, const char **locale) {
    if(updater_event && locale) {
        *locale = updater_event->locale ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_updater_event_get_update_type(const ma_updater_event_t *updater_event, const char **update_type) {
    if(updater_event && update_type) {
        *update_type = updater_event->update_type ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_updater_event_get_update_state(const ma_updater_event_t *updater_event, ma_update_state_t *update_state) {
    if(updater_event && update_state) {		
        *update_state = (ma_update_state_t)updater_event->update_state ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_updater_event_get_update_error(const ma_updater_event_t *updater_event, ma_int32_t *update_error) {
    if(updater_event && update_error) {
        *update_error = updater_event->update_error ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_updater_event_get_new_version(const ma_updater_event_t *updater_event, const char **new_version) {
    if(updater_event && new_version) {
        *new_version = updater_event->new_version ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_updater_event_get_date_time(const ma_updater_event_t *updater_event, const char **date_time) {
    if(updater_event && date_time) {
        *date_time = updater_event->date_time ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_updater_event_get_script_id(const ma_updater_event_t *updater_event, long *script_id) {
    if(updater_event && script_id) {
        *script_id = updater_event->script_id ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_updater_event_get_session_id(const ma_updater_event_t *updater_event, const char **session_id) {
    if(updater_event && session_id) {
        *session_id = updater_event->session_id ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}



