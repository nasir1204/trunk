#include "ma/updater/ma_updater.h"
#include "ma/updater/ma_updater_update_task.h"
#include "ma/scheduler/ma_scheduler.h"

#include "ma/internal/services/updater/ma_updater_request_param.h"
#include "ma/internal/services/updater/ma_updater_utils.h"

#include "ma/internal/clients/updater/ma_update_info_sync.h"

#include "ma/ma_message.h"
#include "ma/internal/clients/ma/ma_client_internal.h"
#include "ma/internal/clients/ma/ma_client_base.h"
#include "ma/internal/clients/ma/client/ma_client_manager.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/defs/ma_updater_defs.h"
#include "ma/internal/defs/ma_mue_defs.h"
#include "ma/internal/defs/ma_object_defs.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

typedef struct ma_updater_client_s ma_updater_client_t, *ma_updater_client_h;

struct ma_updater_client_s {
    ma_client_base_t            base_client ;

    ma_client_t                 *ma_client;

    char                        *product_id ;

    ma_updater_callbacks_t      *cbs ;

    void                        *cb_data ;

    ma_msgbus_server_t          *server;
};

static ma_error_t ma_updater_client_create(ma_client_t *ma_client, const char *product_id, const ma_updater_callbacks_t *cb, void *cb_data, ma_client_base_t **updater_client) ;
static ma_error_t ma_updater_client_on_message(ma_client_base_t *client, const char *topic, ma_message_t *message) ;
static ma_error_t ma_updater_client_release(ma_client_base_t *client) ;

static const ma_client_base_methods_t updater_client_methods = {
    &ma_updater_client_on_message,
    &ma_updater_client_release
};

ma_error_t ma_updater_register_callbacks(ma_client_t *ma_client, const char *product_id, const ma_updater_callbacks_t *cbs, void *cb_data) {
    if(ma_client && cbs) {
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;
        ma_client_get_context(ma_client, &ma_context);
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);		
        product_id = (product_id) ? product_id: MA_CONTEXT_GET_PRODUCT_ID(ma_context);

        if(ma_context && client_manager && product_id) {       
            ma_error_t rc = MA_OK;
            char client_name[MAX_CLIENT_NAME_LENGTH] = {0} ;
            ma_client_base_t *client = NULL ;		
            
            MA_MSC_SELECT(_snprintf, snprintf)(client_name, MAX_CLIENT_NAME_LENGTH, "%s.%s", MA_UPDATER_CLIENT_NAME_STR, product_id) ;
            if(MA_OK == (rc = ma_client_manager_get_client(client_manager, client_name, &client)))
                return MA_ERROR_UPDATER_CLIENT_ALREADY_REGISTERED ;

            if(MA_OK == (rc =  ma_updater_client_create(ma_client, product_id, cbs , cb_data, &client))) {
                if(MA_OK != (rc = ma_client_manager_add_client(client_manager, client_name, client)))
                    (void)ma_updater_client_release(client);
            }

            return rc ;
        }
        return MA_ERROR_INVALID_CLIENT_OBJECT;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_updater_unregister_callbacks(ma_client_t *ma_client, const char *product_id) {
    if(ma_client) {
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;
        ma_client_get_context(ma_client, &ma_context);
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);		
        product_id = (product_id) ? product_id: MA_CONTEXT_GET_PRODUCT_ID(ma_context);
        
        if(ma_context && client_manager && product_id) {
            ma_error_t rc = MA_OK ;
            char client_name[MAX_CLIENT_NAME_LENGTH] = {0} ;
            ma_client_base_t *client = NULL ;
        
            MA_MSC_SELECT(_snprintf, snprintf)(client_name, MAX_CLIENT_NAME_LENGTH,"%s.%s",MA_UPDATER_CLIENT_NAME_STR, product_id) ;
            if(MA_OK != (rc = ma_client_manager_get_client(client_manager, client_name, &client)))
                return MA_ERROR_UPDATER_CLIENT_NOT_REGISTERED ;
            
            (void)ma_client_manager_remove_client(client_manager, client) ;

            (void)ma_client_base_release(client) ;

            return rc ;
        }
        return MA_ERROR_INVALID_CLIENT_OBJECT;
    }
    return MA_ERROR_INVALIDARG ;
}

    


ma_error_t ma_updater_start_update(ma_client_t *ma_client, ma_updater_update_request_t *request, ma_buffer_t **session_id) {
    if(ma_client && request) {
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;

        ma_client_get_context(ma_client, &ma_context);
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);		

        if(ma_context && client_manager) {
            ma_error_t rc = MA_OK;
            ma_task_t *task = NULL;
            
            if(MA_OK == (rc = ma_updater_update_task_create(ma_client, NULL, &task))) {
                ma_array_t *product_list = NULL;
                ma_updater_initiator_type_t initiator_type = MA_UPDATER_INITIATOR_TYPE_ONDEMAND_UPDATE;
                ma_updater_update_type_t update_type = MA_UPDATER_UPDATE_TYPE_NORMAL_UPDATE;
                ma_uint32_t locale = 0;
            
                (void)ma_task_set_condition(task, MA_TASK_COND_DELETE_WHEN_DONE);
                (void)ma_task_set_condition(task, MA_TASK_COND_DISABLED);
				(void)ma_task_set_condition(task, MA_TASK_COND_HIDDEN);

                /* if product list is there add it in task */
                if((MA_OK == rc) && (MA_OK == ma_updater_update_request_get_product_list(request, &product_list))) {
                    rc =  ma_updater_update_task_set_product_list(task, product_list);
                    ma_array_release(product_list);
                }
                
                /* if ui provider is there then it it in task */
                {
                    const char *name = NULL;
                    if(MA_OK == ma_updater_update_request_get_ui_provider_name(request, &name) && name){
                        ma_variant_t *var = NULL;
                        if(MA_OK == ma_variant_create_from_string(name, strlen(name), &var)){
                            ma_task_set_setting(task, MA_SECTION_UI_PROVIDER_NAME_STR, MA_KEY_UI_PROVIDER_NAME_STR, var);
                            (void)ma_variant_release(var);
                        }
                    }
                }

				 /* ui option set in the task. */
                {
                    ma_bool_t ui_option;
                    if(MA_OK == ma_updater_update_request_get_uioption(request, &ui_option)){
                        ma_variant_t *var = NULL;
                        if(MA_OK == ma_variant_create_from_string(ui_option ? "1" : "0", strlen(ui_option ? "1" : "0"), &var)){
                            ma_task_set_setting(task, MA_SECTION_UPDATE_OPTIONS_STR, MA_KEY_SHOW_UI_STR, var);
                            (void)ma_variant_release(var);
                        }
                    }
                }

                /*if locale is set */
                if((MA_OK == rc) && (MA_OK == ma_updater_update_request_get_locale(request, &locale)) && (0 != locale)) {
                    rc =  ma_updater_update_task_set_locale(task, locale);
                }

                if(MA_OK == rc) rc = ma_updater_update_request_get_type(request, &update_type) ;
                if(MA_OK == rc) rc = ma_updater_update_request_get_initiator_type(request, &initiator_type) ;

                if(MA_OK == rc) {
                    ma_updater_request_param_t params;
                    const char *task_id = NULL;
                    params.initiator_type = initiator_type;
                    params.request_type = MA_UPDATER_REQUEST_TYPE_START;
                    if(MA_OK == (rc = ma_task_get_id(task, &task_id))) {
                        ma_variant_t *variant;
                        strncpy(params.task_id, task_id, MA_MAX_TASKID_LEN -1);
                        if(MA_OK == (rc = ma_updater_request_param_to_variant(&params, &variant))) {
                            ma_msgbus_endpoint_t *endpoint = NULL;
                            if(MA_OK == (rc = ma_msgbus_endpoint_create(MA_CONTEXT_GET_MSGBUS(ma_context), MA_UPDATER_SERVICE_NAME_STR, MA_UPDATER_SERVICE_HOSTNAME_STR,&endpoint))) {
                                ma_message_t *request_msg = NULL;
                                ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;
                                (void) ma_client_manager_get_thread_option(client_manager, &thread_option);
                                (void) ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);
                                (void) ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                                if(MA_OK == (rc = ma_message_create(&request_msg))) {
                                    if(MA_OK == (rc = ma_message_set_payload(request_msg, variant))) {
                                        ma_message_t *response_msg = NULL;
                                        if(MA_OK == (rc = ma_scheduler_add_task(ma_client, task))) {
                                            if(MA_OK == (rc = ma_msgbus_send(endpoint, request_msg, &response_msg))) {
                                                (void)ma_message_release(response_msg);
                                                if(session_id) {
                                                    if(MA_OK == (rc = ma_buffer_create(session_id, strlen(task_id))))
                                                        (void)ma_buffer_set(*session_id, task_id, strlen(task_id));
                                                }
                                            }
                                            else
                                                ma_scheduler_remove_task(ma_client, task_id);
                                        }
                                    }
                                    ma_message_release(request_msg);
                                }
                                ma_msgbus_endpoint_release(endpoint);
                            }
                            ma_variant_release(variant);
                        }
                    }
                }
                (void)ma_task_release(task);
            }
            return rc;
        }
        return MA_ERROR_INVALID_CLIENT_OBJECT;
    }
    return MA_ERROR_INVALIDARG;
}
 

ma_error_t ma_updater_stop_update(ma_client_t *ma_client, ma_buffer_t *session_id) {
    if(ma_client && session_id) {
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;

        ma_client_get_context(ma_client, &ma_context);
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);		

        if(ma_context && client_manager) {
            ma_error_t rc = MA_OK;
            const char *task_id = NULL;
            size_t task_id_len = 0;

            if(MA_OK == (rc = ma_buffer_get_string(session_id, &task_id, &task_id_len))) {
                ma_updater_request_param_t params;
                ma_variant_t *variant = NULL;
                params.initiator_type = MA_UPDATER_INITIATOR_TYPE_ONDEMAND_UPDATE;
                params.request_type = MA_UPDATER_REQUEST_TYPE_STOP;
                strncpy(params.task_id, task_id, MA_MAX_TASKID_LEN -1);
                if(MA_OK == (rc = ma_updater_request_param_to_variant(&params, &variant))) {
                    ma_msgbus_endpoint_t *endpoint = NULL;
                    if(MA_OK == (rc = ma_msgbus_endpoint_create(MA_CONTEXT_GET_MSGBUS(ma_context), MA_UPDATER_SERVICE_NAME_STR, MA_UPDATER_SERVICE_HOSTNAME_STR,&endpoint))) {
                        ma_message_t *request_msg = NULL;
                        ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;
                        (void) ma_client_manager_get_thread_option(client_manager, &thread_option);
                        (void) ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);
                        (void) ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);

                        if(MA_OK == (rc = ma_message_create(&request_msg))) {
                            if(MA_OK == (rc = ma_message_set_payload(request_msg, variant))) {
                                ma_message_t *response_msg = NULL;
                                if(MA_OK == (rc = ma_msgbus_send(endpoint, request_msg, &response_msg))) {
                                    (void)ma_message_release(response_msg);
                                }
                            }
                            ma_message_release(request_msg);
                        }
                        ma_msgbus_endpoint_release(endpoint);
                    }
                    ma_variant_release(variant);
                }
            }
            return rc;
        }
        return MA_ERROR_INVALID_CLIENT_OBJECT;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_updater_get_update_state(ma_client_t *ma_client, ma_buffer_t *session_id, ma_update_state_t *state){
    if(ma_client && session_id && state) {
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;

        ma_client_get_context(ma_client, &ma_context);
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);		

        if(ma_context && client_manager) {
            ma_error_t rc = MA_OK;
            const char *task_id = NULL;
            size_t task_id_len = 0;

            if(MA_OK == (rc = ma_buffer_get_string(session_id, &task_id, &task_id_len))) {
                ma_updater_request_param_t params;
                ma_variant_t *variant = NULL;
                params.initiator_type = MA_UPDATER_INITIATOR_TYPE_ONDEMAND_UPDATE;
                params.request_type = MA_UPDATER_REQUEST_TYPE_STATUS_CHECK;
                strncpy(params.task_id, task_id, MA_MAX_TASKID_LEN -1);
                if(MA_OK == (rc = ma_updater_request_param_to_variant(&params, &variant))) {
                    ma_msgbus_endpoint_t *endpoint = NULL;
                    if(MA_OK == (rc = ma_msgbus_endpoint_create(MA_CONTEXT_GET_MSGBUS(ma_context), MA_UPDATER_SERVICE_NAME_STR, MA_UPDATER_SERVICE_HOSTNAME_STR,&endpoint))) {
                        ma_message_t *request_msg = NULL;
                        ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;
                        (void) ma_client_manager_get_thread_option(client_manager, &thread_option);
                        (void) ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);
                        (void) ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);

                        if(MA_OK == (rc = ma_message_create(&request_msg))) {
                            if(MA_OK == (rc = ma_message_set_payload(request_msg, variant))) {
                                ma_message_t *response_msg = NULL;
                                if(MA_OK == (rc = ma_msgbus_send(endpoint, request_msg, &response_msg))) {
                                    ma_variant_t *payload = NULL;
                                    if(MA_OK == (rc = ma_message_get_payload(response_msg, &payload))){
                                        ma_int16_t udpate_state = 0;
                                        if(MA_OK == (rc = ma_variant_get_int16(payload, &udpate_state))){
                                            *state = (ma_update_state_t)udpate_state;											
                                        }
                                        ma_variant_release(payload);
                                    }
                                    (void)ma_message_release(response_msg);
                                }
                            }
                            ma_message_release(request_msg);
                        }
                        ma_msgbus_endpoint_release(endpoint);
                    }
                    ma_variant_release(variant);
                }
            }
            return rc;
        }
        return MA_ERROR_INVALID_CLIENT_OBJECT;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_updater_client_request_handler(ma_msgbus_server_t *server, ma_message_t *request, void *cb_data, ma_msgbus_client_request_t *c_request);

static ma_error_t ma_updater_client_create(ma_client_t *ma_client, const char *product_id, const ma_updater_callbacks_t *cbs, void *cb_data, ma_client_base_t **updater_client) {
    if(updater_client) {        
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;
        ma_msgbus_t *msgbus = NULL;
        ma_client_get_context(ma_client, &ma_context);
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);		
        msgbus = MA_CONTEXT_GET_MSGBUS(ma_context);
        if(ma_context && client_manager && msgbus) {
            ma_error_t rc = MA_OK ;
            ma_updater_client_t *self = NULL ;
            char server_name[MAX_CLIENT_NAME_LENGTH] = {0} ;

            self = (ma_updater_client_t *)calloc(1, sizeof(ma_updater_client_t)) ;
            if(!self)     return MA_ERROR_OUTOFMEMORY ;

            MA_MSC_SELECT(_snprintf, snprintf)(server_name, MAX_CLIENT_NAME_LENGTH, "%s.%s", MA_UPDATER_CLIENT_HANDLER_STR, product_id) ;
            if(MA_OK == (rc = ma_msgbus_server_create(msgbus, server_name, &self->server))) {
                ma_msgbus_server_setopt(self->server, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO) ;
                ma_msgbus_server_setopt(self->server, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                if(MA_OK == (rc = ma_msgbus_server_start(self->server, ma_updater_client_request_handler, ma_client))) {
                    self->product_id = strdup(product_id) ;
                    self->cbs = (ma_updater_callbacks_t *)cbs;
                    self->cb_data = cb_data ;
                    self->ma_client = ma_client ;
                    ma_client_base_init((ma_client_base_t *)self, &updater_client_methods, self) ;
                    *updater_client = (ma_client_base_t *)self ;
                    return MA_OK ;
                }
                ma_msgbus_server_release(self->server);
            }
        
            free(self);
            return rc ;
        }
        return MA_ERROR_INVALID_CLIENT_OBJECT;
    }
    return MA_ERROR_INVALIDARG ;
}

static ma_error_t ma_updater_client_release(ma_client_base_t *client) {
    if(client) {
        ma_updater_client_t *self = (ma_updater_client_t *)client ;
        if(self->server) {
            ma_msgbus_server_stop(self->server) ;
            ma_msgbus_server_release(self->server) ;
        }
        
        if(self->product_id) free(self->product_id) ;
        free(self) ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

static ma_error_t create_update_info_sync_response_payload(const char *info_key, ma_variant_t *info_value, ma_updater_product_return_code_t product_return_code, ma_variant_t **out){
    ma_variant_t *response_payload = NULL;
    ma_update_info_sync_t *sync = NULL;
    ma_error_t rc = MA_OK;

    if(MA_OK == (rc = ma_update_info_sync_create(&sync))){

        ma_update_info_sync_set_type(sync, MA_UPDATE_INFO_SYNC_TYPE_RESPONSE);

        /*if user want to set the response key/value.*/
        if(info_key && info_value){
            ma_update_info_sync_set_info_key(sync, info_key);
            ma_update_info_sync_set_info_value(sync, info_value);
        }
        ma_update_info_sync_set_product_response_code(sync, product_return_code);			

        if(MA_OK == (rc = ma_update_info_sync_to_variant(sync, &response_payload)))
            *out = response_payload;
        ma_update_info_sync_release(sync);
    }
    return rc;
}


static ma_error_t handle_get_request(ma_client_t *ma_client,  ma_updater_client_t *self, const char *software_id, ma_update_info_sync_t *sync, ma_message_t *response) {
    if(self->cbs && self->cbs->get_cb) {
        ma_error_t rc = MA_OK;
        ma_updater_product_return_code_t product_return_code ;
        const char *update_type = NULL;
        const char *key = NULL;
        if(MA_OK == (rc = ma_update_info_sync_get_update_type(sync, &update_type))) {
            if(MA_OK == (rc = ma_update_info_sync_get_info_key(sync, &key))) {
                ma_variant_t *value = NULL;
                if(MA_OK == (rc = self->cbs->get_cb(ma_client, software_id, self->cb_data, update_type, key, &value, &product_return_code))) {
                    ma_variant_t *response_payload = NULL;					
                    if(MA_OK == (rc = create_update_info_sync_response_payload(key, value, product_return_code, &response_payload))){
                        (void)ma_message_set_payload(response, response_payload);
                        ma_variant_release(response_payload);
                    }
                    ma_variant_release(value);
                }
            }
        }
        return rc;
    }
    return MA_OK;
}

static ma_error_t handle_set_request(ma_client_t *ma_client,  ma_updater_client_t *self, const char *software_id, ma_update_info_sync_t *sync_request, ma_message_t *response) {
    if(self->cbs && self->cbs->set_cb) {
        ma_error_t rc = MA_OK;
        ma_updater_product_return_code_t product_return_code ;
        const char *update_type = NULL;
        const char *key = NULL;
        if(MA_OK == (rc = ma_update_info_sync_get_update_type(sync_request, &update_type))) {
            if(MA_OK == (rc = ma_update_info_sync_get_info_key(sync_request, &key))) {
                ma_variant_t *value = NULL;
                if(MA_OK == (rc = ma_update_info_sync_get_info_value(sync_request, &value))) {
                    if(MA_OK == (rc = self->cbs->set_cb(ma_client, software_id, self->cb_data, update_type, key, value, &product_return_code))) {
                        ma_variant_t *response_payload = NULL;					
                        if(MA_OK == (rc = create_update_info_sync_response_payload(NULL, NULL, product_return_code, &response_payload))){
                            (void)ma_message_set_payload(response, response_payload);
                            ma_variant_release(response_payload);
                        }
                    }
                    ma_variant_release(value);
                }
            }
        }
        return rc;
    }
    return MA_OK;
}

static ma_error_t handle_progress_request(ma_client_t *ma_client,  ma_updater_client_t *self, const char *software_id, ma_update_info_sync_t *sync_request, ma_message_t *response) {
    if(self->cbs && self->cbs->notify_cb) {
        ma_error_t rc = MA_OK;
        ma_updater_product_return_code_t product_return_code ;
        const char *update_type = NULL;
        const char *message = NULL;
        const char *extra_info = NULL;
        ma_update_state_t  state = MA_UPDATE_STATE_UNKNOWN;
        if(MA_OK == (rc = ma_update_info_sync_get_update_type(sync_request, &update_type))){
            if(MA_OK == (rc = ma_update_info_sync_get_message(sync_request, &message))){

                if(MA_OK == (rc = ma_update_info_sync_get_extra_info(sync_request, &extra_info))){

                    if(MA_OK == (rc = ma_update_info_sync_get_update_state(sync_request, &state))){
                        if(MA_OK == (rc = self->cbs->notify_cb(ma_client, software_id, self->cb_data, update_type, state, message, extra_info, &product_return_code))) {
                            ma_variant_t *response_payload = NULL;					

                            if(MA_OK == (rc = create_update_info_sync_response_payload(NULL, NULL, product_return_code, &response_payload))){
                                (void)ma_message_set_payload(response, response_payload);
                                ma_variant_release(response_payload);
                            }
                        }
                    }
                }			
            }
        }
        return rc;
    }
    return MA_OK;
}


static ma_error_t ma_updater_client_request_handler(ma_msgbus_server_t *server, ma_message_t *request, void *cb_data, ma_msgbus_client_request_t *c_request) {
    if(server && request && c_request) {
        ma_error_t rc = MA_OK ;
        ma_client_t *ma_client = (ma_client_t *)cb_data ;
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;
        ma_message_t *response = NULL;

        if(!ma_client) return MA_ERROR_INVALID_CLIENT_OBJECT;

        ma_client_get_context(ma_client, &ma_context);
        if(!ma_context) return MA_ERROR_INVALID_CLIENT_OBJECT;

        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);		
        if(!client_manager) return MA_ERROR_INVALID_CLIENT_OBJECT;
         
        if(MA_OK == (rc = ma_message_create(&response))) {
            ma_variant_t *payload = NULL;
            if(MA_OK == (rc = ma_message_get_payload(request, &payload))) 
            {
                ma_update_info_sync_t *sync = NULL;
                if(MA_OK == (rc = ma_update_info_sync_from_variant(payload, &sync))){					
                    const char *software_id;
                    if(MA_OK == (rc = ma_update_info_sync_get_software_id(sync, &software_id))) {
                        ma_update_info_sync_type_t sync_type ;
                        if(MA_OK == (rc = ma_update_info_sync_get_type(sync, &sync_type))) {
                            if(MA_UPDATE_INFO_SYNC_TYPE_CHECK_EXISTANCE != sync_type){
                                char client_name[MAX_CLIENT_NAME_LENGTH] = {0} ;
                                ma_updater_client_t *self = NULL ;		
            
                                MA_MSC_SELECT(_snprintf, snprintf)(client_name, MAX_CLIENT_NAME_LENGTH, "%s.%s", MA_UPDATER_CLIENT_NAME_STR, software_id) ;
                                if(MA_OK == (rc = ma_client_manager_get_client(client_manager, client_name, (ma_client_base_t **)&self))) {
                                    switch(sync_type) {
                                        case MA_UPDATE_INFO_SYNC_TYPE_NOTIFY :
                                            rc = handle_progress_request(ma_client, self, software_id, sync, response);
                                            break;
                                        case MA_UPDATE_INFO_SYNC_TYPE_GET :
                                            rc = handle_get_request(ma_client, self, software_id, sync, response);
                                            break;
                                        case MA_UPDATE_INFO_SYNC_TYPE_SET :
                                            rc = handle_set_request(ma_client, self, software_id, sync, response);
                                            break;
                                        default: 
                                            rc = MA_ERROR_INVALID_OPERATION;
                                    }
                                }
                            }
                        }						
                    }
                    ma_update_info_sync_release(sync);                    
                }
                ma_variant_release(payload);
            }
            rc = ma_msgbus_server_client_request_post_result(c_request, rc, response) ;
            ma_message_release(response);
            return rc;
        }
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_updater_client_on_message(ma_client_base_t *client, const char *topic, ma_message_t *message) {
     if(!strcmp(topic, MA_MUEEVENT_PUBLISH_TOPIC )) {
        const char *op_type = NULL ;
        const char *session_id = NULL ;
        ma_variant_t *payload = NULL ;
        ma_updater_client_t *self = (ma_updater_client_t*) client ;

        (void)ma_message_get_property(message, MA_MUEEVENT_PROP_OPERATION_TYPE, &op_type) ;
        (void)ma_message_get_property(message, MA_MUEEVENT_PROP_SESSION_ID, &session_id) ;
        ma_message_get_payload(message, &payload) ;
        if(op_type  && self->cbs && self->cbs->update_event_cb){
            switch(atoi(op_type)) {
                case eOpEvent :
                    {
                        ma_updater_event_t *updater_event = NULL ;
                    
                        if(MA_OK == ma_updater_event_from_variant(payload, session_id, &updater_event) && updater_event){                
                            self->cbs->update_event_cb(self->ma_client, self->product_id, self->cb_data, updater_event) ;
                            (void)ma_updater_event_release(updater_event) ;
                        }
                        break ;
                    }
                default :
                    break ;
            }
        }
		ma_variant_release(payload);
     }    
     return MA_OK ;
}

