#include "ma/property/ma_property_bag.h"
#include "ma/internal/defs/ma_property_defs.h"

#include <stdlib.h>
#include <string.h>


/*
props->table of sections->table of settings
*/
struct ma_property_bag_s {
	ma_table_t *props ;
	ma_bool_t	minimal_props ;
};

ma_error_t ma_property_bag_create(ma_property_bag_t **bag) {
	if(bag) {
		ma_error_t rc = MA_OK ;
		ma_property_bag_t *self = (ma_property_bag_t *)calloc(1,sizeof(ma_property_bag_t));
		if(!self)	return MA_ERROR_OUTOFMEMORY;

		if(MA_OK != (rc = ma_table_create(&self->props))) {
			free(self);
			return rc;
		}
		self->minimal_props = MA_FALSE ;
		*bag = self;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}
ma_error_t ma_property_bag_add_property(ma_property_bag_t *self, const char *path, const char *name, ma_variant_t *value) {
	if(self && path && name && value) {
        ma_vartype_t var_type = MA_VARTYPE_NULL ;
        ma_error_t    rc = MA_OK ;

		if(self->minimal_props == MA_FALSE || (self->minimal_props == MA_TRUE && (0 == strcmp(path, MA_PROPERTY_GENERAL_SECTION_STR) || 0 ==strcmp(path, MA_PROPERTY_SYSTEMINFO_SECTION_STR) || 0 ==strcmp(path, MA_PROPERTY_COMPUTERPROPERTIES_SECTION_STR)))) 
		{
			if(MA_OK == (rc = ma_variant_get_type(value, &var_type))) {
				if(MA_VARTYPE_STRING == var_type) {
					ma_variant_t *path_variant = NULL;
					ma_table_t	 *path_table = NULL;

					if(MA_ERROR_OBJECTNOTFOUND == (rc = ma_table_get_value(self->props, path, &path_variant))) {
						rc = (( MA_OK == (rc = ma_table_create(&path_table))) &&
									(MA_OK == (rc = ma_variant_create_from_table(path_table, &path_variant))) &&
									(MA_OK == (rc = ma_table_add_entry(self->props, path, path_variant))))
									? MA_OK :rc ;
					}
					else
						rc = ma_variant_get_table(path_variant, &path_table) ;

					if(MA_OK == rc) {
						ma_table_release(path_table) ;
						ma_table_add_entry(path_table, name, value) ;
					}
					ma_variant_release(path_variant) ;
				}
				else
					rc = MA_ERROR_NOT_SUPPORTED ;
			}
		}
        return rc;
	}
	return MA_ERROR_INVALIDARG;
}


ma_error_t ma_property_bag_get_type(ma_property_bag_t *self, ma_property_bag_type_t *bag_type) {
	if(self && bag_type) {
		*bag_type = (MA_TRUE == self->minimal_props) ? MA_PROPERTY_BAG_TYPE_MIN_PROPS : MA_PROPERTY_BAG_TYPE_FULL_PROPS;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_property_bag_get_table(ma_property_bag_t *bag, ma_table_t **table) {
    if(bag && table) {
        ma_table_t *table_tmp = bag->props ;
        ma_table_add_ref(table_tmp) ;
        *table = table_tmp ;
        return MA_OK ;
    }
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_property_bag_release(ma_property_bag_t *self) {
	if(self) {
        ma_table_release(self->props) ;
        free(self) ;
        self = NULL ;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_property_bag_set_minimal_props(ma_property_bag_t *self, ma_bool_t minimal_props) {
	if(self) {
		self->minimal_props = minimal_props ;
		return MA_OK ;
	}
	return MA_ERROR_INVALIDARG ;
}

