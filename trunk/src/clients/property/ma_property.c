#include "ma/property/ma_property.h"
#include "ma/property/ma_property_bag.h"
#include "ma/internal/defs/ma_property_defs.h"

#include "ma/ma_message.h"
#include "ma/internal/clients/ma/ma_client_internal.h"
#include "ma/internal/clients/ma/ma_client_base.h"
#include "ma/internal/clients/ma/client/ma_client_manager.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/defs/ma_general_defs.h"

#include "ma/internal/clients/ma/ma_client_event.h"


#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "property"

extern ma_error_t ma_property_bag_get_table(ma_property_bag_t *bag, ma_table_t **table) ;
extern ma_error_t ma_property_bag_set_minimal_props(ma_property_bag_t *bag, ma_bool_t minimal_props) ;
extern ma_error_t ma_property_bag_create(ma_property_bag_t **bag) ;
extern ma_error_t ma_property_bag_release(ma_property_bag_t *bag) ;


typedef struct ma_property_client_s ma_property_client_t, *ma_property_client_h;
struct ma_property_client_s {
    ma_client_base_t            base_client;
    ma_client_t                *ma_client; /*pointer to the client */
    char                        *product_id ;
    ma_property_provider_cb_t   cb ;
    ma_msgbus_subscriber_t		*property_msg_subscriber ;
    void                        *cb_data ;
};


static ma_error_t ma_property_client_create(ma_client_t *ma_client, const char *product_id, ma_property_provider_cb_t cb, void *cb_data, ma_client_base_t **property_client);
static ma_error_t ma_property_client_on_message(ma_client_base_t *client, const char *topic, ma_message_t *message);
static ma_error_t ma_property_client_release(ma_client_base_t *property_client);

static const ma_client_base_methods_t property_client_methods = {
    &ma_property_client_on_message,
    &ma_property_client_release
};

static ma_error_t ma_property_subscriber_cb(const char *topic, ma_message_t *msg, void *cb_data) ;

ma_error_t ma_property_register_provider_callback(ma_client_t *ma_client, const char *product_id, ma_property_provider_cb_t cb, void *cb_data) {
    if(ma_client && cb) {
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;        
        ma_client_get_context(ma_client, &ma_context);
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);		
        product_id = (product_id) ? product_id: MA_CONTEXT_GET_PRODUCT_ID(ma_context);

        if(ma_context && client_manager && product_id) {
            ma_error_t rc = MA_OK;
            char client_name[MAX_CLIENT_NAME_LENGTH] = {0};
            ma_client_base_t *client = NULL;				    

            MA_MSC_SELECT(_snprintf, snprintf)(client_name, MAX_CLIENT_NAME_LENGTH,"%s.%s",MA_PROPERTY_CLIENT_NAME_STR, product_id);

            if(ma_client_manager_has_client(client_manager, client_name))
                return MA_ERROR_PROPERTY_PROVIDER_ALREADY_REGISTERED;

            if(MA_OK == (rc =  ma_property_client_create(ma_client, product_id, cb , cb_data, &client))) {
                if(MA_OK == (rc = ma_msgbus_subscriber_register(((ma_property_client_t *)client)->property_msg_subscriber, MA_PROPERTY_PUBSUB_TOPIC_COLLECT_STR, ma_property_subscriber_cb,(void *)client))) {
                    if(MA_OK != (rc = ma_client_manager_add_client(client_manager, client_name, client))) {
                        (void)ma_msgbus_subscriber_unregister(((ma_property_client_t *)client)->property_msg_subscriber) ;
                        (void)ma_property_client_release(client) ;
                    }
                }
            }
            return rc;
        }
        return MA_ERROR_INVALID_CLIENT_OBJECT;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_property_unregister_provider_callback(ma_client_t *ma_client, const char *product_id) {
    if(ma_client) {
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;        
        ma_client_get_context(ma_client, &ma_context);
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);		
        product_id = (product_id) ? product_id: MA_CONTEXT_GET_PRODUCT_ID(ma_context);

        if(client_manager && product_id) {
            ma_error_t rc = MA_OK;
            char client_name[MAX_CLIENT_NAME_LENGTH] = {0};
            ma_client_base_t *client = NULL;		   

            MA_MSC_SELECT(_snprintf, snprintf)(client_name, MAX_CLIENT_NAME_LENGTH,"%s.%s",MA_PROPERTY_CLIENT_NAME_STR, product_id);
            if(MA_OK != (rc = ma_client_manager_get_client(client_manager, client_name, &client)))
                return MA_ERROR_PROPERTY_PROVIDER_NOT_REGISTERED;

            (void)ma_client_manager_remove_client(client_manager, client);
            (void)ma_msgbus_subscriber_unregister(((ma_property_client_t *)client)->property_msg_subscriber) ;

            return ma_client_base_release(client);
        }
        return MA_ERROR_INVALID_CLIENT_OBJECT;
    }
    return MA_ERROR_INVALIDARG ;
}


ma_error_t ma_property_send(ma_client_t *ma_client, const char *product_id) {
    if(ma_client) {
        ma_error_t rc = MA_OK;
        ma_context_t *ma_context = NULL;
        ma_logger_t *logger = NULL;
        ma_message_t *request = NULL;	  
        ma_msgbus_t *msgbus = NULL;

        ma_client_get_context(ma_client, &ma_context);
        logger = MA_CONTEXT_GET_LOGGER(ma_context);		
        msgbus = MA_CONTEXT_GET_MSGBUS(ma_context);        

        if(!ma_context || !msgbus)
            return MA_ERROR_INVALID_CONTEXT;
        
        MA_LOG(logger, MA_LOG_SEV_TRACE, "sending collect props command.");
        if(MA_OK == (rc = ma_message_create(&request))) {
            ma_msgbus_endpoint_t *endpoint = NULL;
            (void)ma_message_set_property(request, MA_PROPERTY_MSG_KEY_TYPE_STR, MA_PROPERTY_MSG_TYPE_COLLECT_STR);
            (void)ma_message_set_property(request, MA_PROPERTY_MSG_KEY_INITIATOR_STR, product_id);
            (void)ma_message_set_property(request, MA_PROPERTY_MSG_KEY_PROPS_TYPE_STR, MA_PROPERTY_MSG_VALUE_PROPS_TYPE_VERSION_STR);

            if(MA_OK == (rc = ma_msgbus_endpoint_create(msgbus, MA_PROPERTY_SERVICE_NAME_STR, NULL, &endpoint))) {
                (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC) ;
                if(MA_OK == (rc = ma_msgbus_send_and_forget(endpoint, request))) 
                    MA_LOG(logger, MA_LOG_SEV_INFO, "Properties collect and send command initiated by cmdagent.");							
                else
                    MA_LOG(logger, MA_LOG_SEV_ERROR, "Properties collect and send command initiated by cmdagent failed, %d.", rc);
                (void)ma_msgbus_endpoint_release(endpoint);
            }
            (void)ma_message_release(request);
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG ;
}


static ma_error_t ma_property_client_create(ma_client_t *ma_client, const char *product_id, ma_property_provider_cb_t cb, void *cb_data, ma_client_base_t **property_client) {
    ma_context_t *ma_context = NULL ;
    ma_client_manager_t *client_manager = NULL ;
    ma_error_t rc = MA_OK ;

    ma_property_client_t *client = (ma_property_client_t *)calloc(1,sizeof(ma_property_client_t)) ;
    if(!client)     return MA_ERROR_OUTOFMEMORY ;

    ma_client_get_context(ma_client, &ma_context) ;   
    client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context) ;

    if(MA_OK == (rc = ma_msgbus_subscriber_create(MA_CONTEXT_GET_MSGBUS(ma_context), &client->property_msg_subscriber))) {
        ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL ;

        client->product_id = strdup(product_id) ;
        client->cb = cb ;
        client->cb_data = cb_data ;
        client->ma_client = ma_client ;
        ma_client_base_init((ma_client_base_t *)client, &property_client_methods, client) ;

        (void) ma_client_manager_get_thread_option(client_manager, &thread_option);
        (void)ma_msgbus_subscriber_setopt(client->property_msg_subscriber, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC) ;
        (void)ma_msgbus_subscriber_setopt(client->property_msg_subscriber, MSGBUSOPT_THREADMODEL, thread_option) ;

        *property_client = (ma_client_base_t *)client ;

        return MA_OK ;
    }

    // Failure case
    free(client);

    return rc ;
}

static ma_error_t ma_property_client_on_message(ma_client_base_t *client, const char *topic, ma_message_t *recv_msg) {
    return MA_OK ;
}

static ma_error_t ma_property_client_release(ma_client_base_t *property_client) {
    if(property_client) {
        ma_property_client_t *self = (ma_property_client_t *)property_client ;

        if(self->product_id) free(self->product_id) ;
        self->product_id = NULL ;

        if(self->property_msg_subscriber)	ma_msgbus_subscriber_release(self->property_msg_subscriber) ;
        self->property_msg_subscriber = NULL ;

        free(self);

        return MA_OK;
    }
    return MA_ERROR_INVALIDARG ;
}

static ma_error_t ma_property_subscriber_cb(const char *topic, ma_message_t *recv_msg, void *cb_data) {
    if(0 == strcmp(MA_PROPERTY_PUBSUB_TOPIC_COLLECT_STR , topic)) {
        ma_property_client_t *self = (ma_property_client_t *)cb_data ;
        ma_error_t rc = MA_OK ;
        ma_property_bag_t *property_bag = NULL ;
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;  
        ma_msgbus_t *msgbus = NULL;

        ma_client_get_context(self->ma_client, &ma_context);
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);
        msgbus = MA_CONTEXT_GET_MSGBUS(ma_context);

        if(!ma_context || !client_manager || !msgbus)
            return MA_ERROR_INVALID_CONTEXT;

        //create the property bag and call the provider callback which fills the bag.
        if(MA_OK == (rc = ma_property_bag_create(&property_bag))) {
            const char *str_minimal_props = NULL ;

            (void)ma_message_get_property(recv_msg, MA_PROPERTY_MSG_KEY_MINIMAL_PROPS_INT, &str_minimal_props) ;
            if(str_minimal_props) {
                (void)ma_property_bag_set_minimal_props(property_bag, (ma_bool_t)atoi(str_minimal_props)) ;
            }

            if(MA_OK == (rc = self->cb(self->ma_client, self->product_id, property_bag, self->cb_data))) {
                //create message, set options and payload
                ma_message_t *send_msg = NULL ;
                ma_table_t *p_table = NULL ;
                ma_variant_t *payload= NULL ;
                ma_message_t* response = NULL ;
                char const *session = NULL ;
                ma_msgbus_endpoint_t *ep = NULL ;

                if(MA_OK == (rc = ma_message_create(&send_msg))) {
                    (void)ma_message_set_property(send_msg, MA_PROPERTY_MSG_KEY_TYPE_STR, MA_PROPERTY_MSG_TYPE_COLLECTED_STR) ;  
                    (void)ma_message_get_property(recv_msg, MA_PROPERTY_MSG_KEY_SESSION_ID_INT, &session) ;
                    (void)ma_message_set_property(send_msg, MA_PROPERTY_MSG_KEY_SESSION_ID_INT, session) ;
                    (void)ma_message_set_property(send_msg, MA_PROPERTY_MSG_KEY_PROVIDER_ID_STR, self->product_id) ;

                    (void)ma_property_bag_get_table(property_bag, &p_table) ;
                    (void)ma_variant_create_from_table(p_table, &payload) ;

                    (void)ma_message_set_payload(send_msg, payload) ;


                    if(MA_OK == (rc = ma_msgbus_endpoint_create(msgbus,MA_PROPERTY_SERVICE_NAME_STR, NULL, &ep))) {
                        ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;

                        (void) ma_client_manager_get_thread_option(client_manager, &thread_option);
                        (void)ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC) ;                        

                        /* This is to handle if client manager is running in the same I/O thread and */
                        if(MA_MSGBUS_CALLBACK_THREAD_IO == thread_option)   
                            rc = ma_msgbus_send_and_forget(ep, send_msg);
                        else {
                            (void)ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_THREADMODEL, thread_option);
                            rc = ma_msgbus_send(ep, send_msg, &response) ;
                        }
                        (void) ma_msgbus_endpoint_release(ep) ;
                    }
                    //TODO :process response - based on the response should we resend the message ???
                    (void)ma_table_release(p_table) ;
                    (void)ma_variant_release(payload) ;
                    (void)ma_message_release(send_msg) ;
                    (void)ma_message_release(response) ;
                }
            }
            else{
                ma_message_t* response = NULL ;
				ma_message_t* response_rep = NULL ;
                char const *session = NULL ;
                ma_msgbus_endpoint_t *ep = NULL ;
                (void)ma_message_get_property(recv_msg, MA_PROPERTY_MSG_KEY_SESSION_ID_INT, &session) ;

                if(MA_OK == (rc = ma_message_create(&response))) {
                    (void)ma_message_set_property(response, MA_PROPERTY_MSG_KEY_TYPE_STR, MA_PROPERTY_MSG_TYPE_COLLECT_FAILED_STR) ;                      
                    (void)ma_message_set_property(response, MA_PROPERTY_MSG_KEY_SESSION_ID_INT, session) ;
                    (void)ma_message_set_property(response, MA_PROPERTY_MSG_KEY_PROVIDER_ID_STR, self->product_id) ;

                    if(MA_OK == (rc = ma_msgbus_endpoint_create(msgbus,MA_PROPERTY_SERVICE_NAME_STR, NULL, &ep))) {
                        ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;

                        (void) ma_client_manager_get_thread_option(client_manager, &thread_option);
                        (void)ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC) ;                        

                        /* This is to handle if client manager is running in the same I/O thread and */
                        if(MA_MSGBUS_CALLBACK_THREAD_IO == thread_option)   
                            rc = ma_msgbus_send_and_forget(ep, response);
                        else {
                            (void)ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_THREADMODEL, thread_option);
                            rc = ma_msgbus_send(ep, response, &response_rep) ;
                        }
                        (void) ma_msgbus_endpoint_release(ep) ;
                    }
                    (void)ma_message_release(response) ;
					(void)ma_message_release(response_rep) ;
                }
            }
            (void)ma_property_bag_release(property_bag) ;
        }
        return rc;
    }
    return MA_OK ; /*Message not for us */
}


