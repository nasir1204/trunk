#include "ma/ma_common.h"
#include "ma/ma_errors.h"

#include <stdio.h>
#ifndef MA_WINDOWS
#include <sys/types.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <errno.h>
#else
#include <ws2tcpip.h>
#endif

#ifdef MA_WINDOWS

//include headers
#include <winsock2.h>
#include <windows.h>
#include <ws2tcpip.h>
#include <string.h>

//inetaddress length
#define INET_ADDRSTRLEN                       22
#define INET6_ADDRSTRLEN                      65

#else
#include <netinet/in.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <stdio.h>
#define INVALID_SOCKET -1
#define SOCKET_ERROR   -1
#ifndef NULL
#define NULL 0
#endif
#endif


#include "ma/internal/ma_macros.h"
#include "ma/logger/ma_logger.h"
#include "ma/ma_log.h"
#include "uv.h"


#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "udp_client"


static ma_error_t setsocketoption(uv_udp_t *handle, int level, int optname, const char* optval, int optlen, ma_logger_t *logger){
	int ret=0;
	ret = setsockopt(MA_WIN_SELECT(handle->socket,handle->io_watcher.fd), level, optname, optval, optlen);
	if(0 != ret){
		#ifdef WIN32
		MA_LOG(logger, MA_LOG_SEV_DEBUG, "Socket API returns=%d WSAGetLastError=%d.", ret, WSAGetLastError());;
		#else
		MA_LOG(logger, MA_LOG_SEV_DEBUG, "Socket API returns=%d ", errno);;
		#endif		
		return MA_ERROR_APIFAILED;
	}
	return MA_OK;	
}

ma_error_t set_v6_client_option(uv_udp_t *handle, size_t if_scopeid, int loopback, int ttl, ma_logger_t *logger){

	ma_error_t rc = MA_OK;		
	if(INVALID_SOCKET == MA_WIN_SELECT(handle->socket,handle->io_watcher.fd)) return MA_ERROR_INVALIDARG;

	rc = setsocketoption(handle, IPPROTO_IPV6, IPV6_MULTICAST_IF, (char*) &if_scopeid, sizeof(if_scopeid), logger);
	if(MA_OK == rc)
		rc = setsocketoption(handle, IPPROTO_IPV6, IPV6_MULTICAST_LOOP, (char*) &loopback, sizeof(loopback), logger);
	if(MA_OK == rc)
		rc = setsocketoption(handle, IPPROTO_IPV6, IPV6_MULTICAST_HOPS, (char*) &ttl, sizeof(ttl), logger);
		
	return rc;
}


