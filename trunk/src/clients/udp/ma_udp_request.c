#include "ma_udp_internal.h"

#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/utils/context/ma_context.h"
#include "ma/internal/utils/platform/ma_system_property.h"
#include "ma/internal/clients/ma/ma_client_internal.h"

#include <stdio.h>

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif

#define LOG_FACILITY_NAME "udp_client"

static ma_net_interface_list_t *get_net_interface_list(ma_udp_request_t *self);
static ma_error_t ma_udp_request_stop(ma_udp_request_t *self);

ma_error_t ma_udp_request_create(ma_udp_client_t *client, ma_udp_request_t **request){
	if(client && request) {
		if(client->ma_loop && client->loop_worker){
			ma_udp_request_t *self = (ma_udp_request_t *) calloc(1, sizeof(ma_udp_request_t));
			if(self) {
				if(self->udp_request_timer = (uv_timer_t*)calloc(1, sizeof(uv_timer_t))){
					if(NULL != (self->respondents = ma_udp_respondent_list_create())){
						self->udp_client = client;			
						self->timeout_ms = MA_UDP_DEFAULT_TIMEOUT;
						self->state      = MA_UDP_REQUEST_STATE_INIT;
						uv_timer_init(ma_event_loop_get_uv_loop(client->ma_loop), self->udp_request_timer);	
						self->udp_request_timer->data = self;
						MA_LOG(self->udp_client->logger, MA_LOG_SEV_DEBUG, "uv timer data(%p)\n", self);
						*request = self;
						return MA_OK;
					}
					free(self->udp_request_timer);
				}
				free(self);
			}
			return MA_ERROR_OUTOFMEMORY;
		}
		return MA_ERROR_PRECONDITION;
	}
	return MA_ERROR_INVALIDARG;
}

static void timer_close_cb(uv_handle_t* handle){
	free(handle);	
}

ma_error_t ma_udp_request_release(ma_udp_request_t *self) {
	if(self) {		
		if(MA_UDP_REQUEST_STATE_STARTED != self->state){
			MA_LOG(self->udp_client->logger, MA_LOG_SEV_DEBUG, "Releasing the udp request..");	
			ma_udp_respondent_list_release(self->respondents);
			if(self->udp_publishers) free(self->udp_publishers);
			if(self->multicast_addr) free(self->multicast_addr);
			if(self->message_buffers){
				int i = 0;
				for(; i< self->no_of_messages; i++){
					if(self->message_buffers[i]) ma_bytebuffer_release(self->message_buffers[i]);		
				}		
				free(self->message_buffers);
			}
			if(self->sync_data)free(self->sync_data);		
			uv_close((uv_handle_t*)self->udp_request_timer, timer_close_cb);
			free(self);
			return MA_OK;
		}
		else{
			MA_LOG(self->udp_client->logger, MA_LOG_SEV_ERROR, "Failed to destroy the udp request, still active.");	
			return MA_ERROR_UDP_REQUEST_ACTIVE;
		}
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_udp_request_set_timeout(ma_udp_request_t *self, ma_uint64_t timeout_ms){
	if(self && timeout_ms) {
		self->timeout_ms = timeout_ms;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_udp_request_set_message(ma_udp_request_t *self, ma_udp_msg_t *msg[], ma_uint16_t no_of_message){
	if(self && msg && no_of_message && no_of_message <= 2) {
		self->message_buffers = (ma_bytebuffer_t**) calloc(no_of_message, sizeof(ma_bytebuffer_t*));
		if(self->message_buffers){
			int i = 0;
			for(; i< no_of_message; i++){
				ma_bytebuffer_t *serialized_msg = NULL;
				ma_error_t rc = MA_OK;		
				if(MA_OK == (rc = ma_udp_msg_serialize(msg[i], &serialized_msg))){			
					self->message_buffers[i] = serialized_msg;
					self->no_of_messages++;
					serialized_msg = NULL;
				}				
			}
			return self->no_of_messages ? MA_OK : MA_ERROR_UNEXPECTED;
		}
		return MA_ERROR_OUTOFMEMORY;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_udp_request_set_port(ma_udp_request_t *self, ma_uint16_t udp_port){
	if(self && udp_port){
		self->routing_port = udp_port;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_udp_request_set_multicast_addr(ma_udp_request_t *self, char const *multicast){
	if(self && multicast){
		self->multicast_addr = strdup(multicast);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}


MA_UDP_API ma_error_t ma_udp_request_set_callbacks(ma_udp_request_t *self, udp_request_send_cb_t send_cb, udp_response_cb_t cb, void *user_data){
	if(self){
		self->send_cb = send_cb;
		self->response_cb = cb;
		self->cb_data = user_data ;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

static ma_error_t create_udp_publishers(ma_udp_request_t *self){
	ma_net_interface_list_t *list = get_net_interface_list(self);
	ma_error_t rc = MA_ERROR_NO_NETWORK;
	
	if(list && list->interfaces_count) {
		
		rc = MA_ERROR_OUTOFMEMORY;				
		self->udp_publishers = (ma_udp_publisher_t**) calloc((list->interfaces_count+1)*self->no_of_messages, sizeof(ma_udp_publisher_t*));
				
		if(self->udp_publishers){
			int i = 0;
			self->no_of_publishers = 0;

			for(; i <self->no_of_messages; i++){
				ma_udp_publisher_t *pub = NULL;				

				MA_LOG(self->udp_client->logger, MA_LOG_SEV_DEBUG, "Creating publisher for the message no (%d).", i+1);

				/*create a broadcaster*/
				MA_LOG(self->udp_client->logger, MA_LOG_SEV_DEBUG, "Creating udp global broadcasting publisher.");
		
				if(MA_OK == ma_udp_publisher_create(self, &pub)){
					MA_LOG(self->udp_client->logger, MA_LOG_SEV_DEBUG, "Created udp global broadcasting publisher.");					
					self->udp_publishers[self->no_of_publishers++] = pub;
					ma_udp_publisher_set_msg(pub, self->message_buffers[i]);
					pub = NULL;
				}
				else
					MA_LOG(self->udp_client->logger, MA_LOG_SEV_WARNING, "Failed to create udp global broadcasting publisher.");
			
				/*create a multicaster for every nic*/			
				{
					ma_net_interface_t *it_if = NULL;

					MA_SLIST_FOREACH(list, interfaces, it_if){
						/*create a multicaster only for ipv6 and dual nic*/			
						if(it_if->family == MA_IPFAMILY_IPV6 || it_if->family == MA_IPFAMILY_DUAL){
							ma_net_address_info_t  *addr = NULL;

							MA_SLIST_FOREACH(it_if, addresses, addr){
								/*look for the link local address*/
								if(!strncmp(addr->ip_addr, "fe80:", strlen("fe80:")) || !strncmp(addr->ip_addr, "FE80:", strlen("FE80:"))){

									MA_LOG(self->udp_client->logger, MA_LOG_SEV_DEBUG, "Creating udp multicasting publisher at nic %s with scope id %u.", it_if->interface_name, addr->ipv6_scope_id);
									if(MA_OK == ma_udp6_publisher_create(self, addr->ipv6_scope_id, &pub)){
										MA_LOG(self->udp_client->logger, MA_LOG_SEV_DEBUG, "Created udp multicasting publisher at nic %s with scope id %u.", it_if->interface_name, addr->ipv6_scope_id);								
										self->udp_publishers[self->no_of_publishers++] = pub;
										ma_udp_publisher_set_msg(pub, self->message_buffers[i]);
										pub = NULL;
									}
									else
										MA_LOG(self->udp_client->logger, MA_LOG_SEV_WARNING, "Failed to create udp multicasting publisher at nic %s with scope id %u.", it_if->interface_name, addr->ipv6_scope_id);								
									break;
								}
							}
						}
					}
				}
			}
		
			if(!self->no_of_publishers){
				rc = MA_ERROR_UNEXPECTED;
				free(self->udp_publishers);
				self->udp_publishers = NULL;
				MA_LOG(self->udp_client->logger, MA_LOG_SEV_ERROR, "Failed to create udp publishers.");								
			}
			else
				rc = MA_OK;
		}
	}
	ma_net_interface_list_release(list);
	return rc;
}

static ma_error_t ma_udp_request_stop(ma_udp_request_t *self){
	if(self){
		unsigned int i = 0;
		MA_LOG(self->udp_client->logger, MA_LOG_SEV_DEBUG, "Stopping the udp request publishers.");	
		uv_timer_stop(self->udp_request_timer);
		for(; i< self->no_of_publishers; i++){
			if(self->udp_publishers[i]){
				/*no need to relase the publisher it will be freed by itself in the handle close callback.*/
				ma_udp_publisher_stop(self->udp_publishers[i]);						
				self->udp_publishers[i] = NULL;
			}
		}
		MA_LOG(self->udp_client->logger, MA_LOG_SEV_DEBUG, "Closing udp request timer.");			
		self->state = MA_UDP_REQUEST_STATE_STOPPED;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}


static void async_timer_cb(uv_timer_t* handle, int status /*UNUSED*/) {
	ma_udp_request_t *self = (ma_udp_request_t *) handle->data;
	
	MA_LOG(self->udp_client->logger, MA_LOG_SEV_DEBUG, (self->is_user_cancelled ? "Stopping the async udp request." : "async timer callback is invoked"));
	ma_udp_request_stop(self);
	if(self->final_cb)	self->final_cb( (self->is_user_cancelled ? MA_ERROR_UDP_REQUEST_STOPPED : MA_ERROR_UDP_TIMEOUT), self, self->cb_data);			
}

static void invoke_async_user_callback(void *data) {
	ma_udp_request_t *self = (ma_udp_request_t*)data;	
	ma_error_t rc = MA_OK;

	if(MA_OK == (rc = create_udp_publishers(self))){
		unsigned int i = 0;
		for(; i< self->no_of_publishers; i++)
			ma_udp_publisher_start(self->udp_publishers[i]);			
			
		MA_LOG(self->udp_client->logger, MA_LOG_SEV_DEBUG, "No of items to publish from all nics %d", self->send_pending);				
		uv_timer_start(self->udp_request_timer, async_timer_cb, self->timeout_ms, 0);						
	}
	else{
		MA_LOG(self->udp_client->logger, MA_LOG_SEV_DEBUG, "Failed to create udp message publisher.");		
		uv_timer_start(self->udp_request_timer, async_timer_cb, 0, 0);						
	}
}

ma_error_t ma_udp_request_async_send(ma_udp_request_t *self, udp_request_finalize_cb_t final_cb){
	if(self && final_cb && self->no_of_messages && MA_UDP_REQUEST_STATE_INIT == self->state){
		ma_error_t rc = MA_OK;
		self->final_cb = final_cb;
		self->state = MA_UDP_REQUEST_STATE_STARTED;
		if(MA_OK != (rc = ma_loop_worker_submit_work(self->udp_client->loop_worker, invoke_async_user_callback, self, MA_FALSE)))
			self->state = MA_UDP_REQUEST_STATE_STOPPED;
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

static void sync_timer_cb(uv_timer_t* handle, int status /*UNUSED*/) {
	ma_udp_request_t *self = (ma_udp_request_t *) handle->data;		
	MA_LOG(self->udp_client->logger, MA_LOG_SEV_DEBUG, (self->is_user_cancelled ? "Stopping the sync udp request." : "sync timer callback is invoked"));
	
	ma_udp_request_stop(self);	
	if(self->sync_data){
		uv_mutex_lock(&self->sync_data->mutex);    
		uv_cond_signal(&self->sync_data->condition);
		uv_mutex_unlock(&self->sync_data->mutex); 	
	}
}

static void invoke_sync_user_callback(void *data) {
	ma_udp_request_t *self = (ma_udp_request_t*)data;	
	ma_error_t rc = MA_OK;

	if(MA_OK == (rc = create_udp_publishers(self))){
		unsigned int i = 0;
		for(; i< self->no_of_publishers; i++)
			ma_udp_publisher_start(self->udp_publishers[i]);			
			
		MA_LOG(self->udp_client->logger, MA_LOG_SEV_DEBUG, "No of items to publish from all nics %d", self->send_pending);				
		uv_timer_start(self->udp_request_timer, sync_timer_cb, self->timeout_ms, 0);						
	}
	else{
		MA_LOG(self->udp_client->logger, MA_LOG_SEV_DEBUG, "Failed to create udp message publisher.");		
		uv_timer_start(self->udp_request_timer, sync_timer_cb, 0, 0);						
	}
}

ma_error_t ma_udp_request_send(ma_udp_request_t *self){
	if(self && self->no_of_messages && MA_UDP_REQUEST_STATE_INIT == self->state){
		ma_error_t rc = MA_ERROR_OUTOFMEMORY;
		self->sync_data = (sync_request_data_t*) calloc(1, sizeof(sync_request_data_t));			
		if(self->sync_data){
			rc = MA_ERROR_UNEXPECTED;
			if(0 == uv_mutex_init( &self->sync_data->mutex )) {
				if(0 == uv_cond_init(&self->sync_data->condition)) {
					uv_mutex_lock(&self->sync_data->mutex);
					self->is_sync_request = MA_TRUE;						
					self->state = MA_UDP_REQUEST_STATE_STARTED;
					if(MA_OK == (rc = ma_loop_worker_submit_work(self->udp_client->loop_worker, invoke_sync_user_callback, self, MA_FALSE))){						
						uv_cond_wait(&self->sync_data->condition, &self->sync_data->mutex);								
					}
					self->state = MA_UDP_REQUEST_STATE_STOPPED;
					uv_mutex_unlock(&self->sync_data->mutex);
					uv_cond_destroy(&self->sync_data->condition);
				}
				uv_mutex_destroy(&self->sync_data->mutex);
			}
				
		}		
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_udp_request_cancel(ma_udp_request_t *self){
	if(self){		
		self->is_user_cancelled = MA_TRUE;
		if(self->is_sync_request)
			sync_timer_cb(self->udp_request_timer, -1);					
		else
			async_timer_cb(self->udp_request_timer, -1);		
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}


static ma_net_interface_list_t *get_net_interface_list(ma_udp_request_t *self) {
	if(self) {
		ma_net_interface_list_t *list = NULL;
		if(MA_OK == ma_net_interface_list_create(&list)) {
			if(MA_OK == ma_net_interface_list_scan(list, MA_FALSE)) {
				return list;
			}
			ma_net_interface_list_release(list);
			list = NULL;
		}
	}
	return NULL;
}


struct ma_udp_respondent_list_s{
	ma_table_t *respondents;
};

ma_udp_respondent_list_t *ma_udp_respondent_list_create(){
	ma_udp_respondent_list_t *r = (ma_udp_respondent_list_t*) calloc(1, sizeof(ma_udp_respondent_list_t));
	if(r){
		if(MA_OK != ma_table_create(&r->respondents)){
			free(r);
			r = NULL;
		}
	}
	return r;
}

void ma_udp_respondent_list_release(ma_udp_respondent_list_t *respondents){
	if(respondents){
		ma_table_release(respondents->respondents);
		free(respondents);
	}
}

ma_bool_t ma_udp_respondent_list_is_exist(ma_udp_respondent_list_t *respondents, const char *peer){
	if(respondents && peer){
		ma_variant_t *variant = NULL;
		if(MA_OK == ma_table_get_value(respondents->respondents, peer, &variant)){
			ma_variant_release(variant);
			return MA_TRUE;
		}		
	}
	return MA_FALSE;
}

void ma_udp_respondent_list_add(ma_udp_respondent_list_t *respondents, const char *peer){
	if(respondents && peer){
		ma_variant_t *variant = NULL;
		if(MA_OK == ma_variant_create(&variant)){
			ma_table_add_entry(respondents->respondents, peer, variant);
			ma_variant_release(variant);
		}		
	}	
}

ma_error_t ma_udp_request_get_client(ma_udp_request_t *self,  ma_udp_client_t **udp_client) {
    if(self && udp_client){
        *udp_client = self->udp_client;
        return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}