#include "ma_udp_internal.h"
#include "ma/internal/clients/udp/ma_udp_client.h"
#include "ma/ma_msgbus.h"
#include "ma/internal/ma_strdef.h"

#include "ma/logger/ma_logger.h"
#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "udp_client"


ma_error_t ma_udp_client_create(ma_udp_client_t **udp_client) {
	if(udp_client) {
		ma_udp_client_t *self = (ma_udp_client_t *) calloc(1,sizeof(ma_udp_client_t));
		if(self) {
			*udp_client = self;
			return MA_OK;
		}
		return MA_ERROR_OUTOFMEMORY;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_udp_client_set_logger(ma_udp_client_t *self, ma_logger_t *logger){
	if(self && logger){
		ma_logger_add_ref(self->logger = logger);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_udp_client_get_logger(ma_udp_client_t *self, ma_logger_t **logger){
	if(self && logger){
		if(self->logger){
			*logger = self->logger;
			return MA_OK;
		}
		return MA_ERROR_OBJECTNOTFOUND;
	}
	return MA_ERROR_INVALIDARG;
}


ma_error_t ma_udp_client_get_event_loop(ma_udp_client_t *self, ma_event_loop_t **loop){
	if(self && loop){
		if(self->ma_loop){
			*loop = self->ma_loop;
			return MA_OK;
		}
		return MA_ERROR_OBJECTNOTFOUND;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_udp_client_set_event_loop(ma_udp_client_t *self, ma_event_loop_t *loop){
	if(self && loop){
		self->ma_loop = loop;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_udp_client_set_loop_worker(ma_udp_client_t *self, ma_loop_worker_t *loop_worker){
	if(self && loop_worker){
		self->loop_worker = loop_worker;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;

}

ma_error_t ma_udp_client_release(ma_udp_client_t *self) {
	if(self){
		if(self->logger) ma_logger_release(self->logger);
		free(self);		
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

