#include "ma/internal/clients/udp/ma_udp_msg.h"
#include "ma/internal/utils/threading/ma_atomic.h"

#include "ma/internal/utils/serialization/ma_serialize.h"
#include "ma/internal/utils/serialization/ma_deserialize.h"

#include "ma/internal/defs/ma_udp_defs.h"
#include "ma/internal/core/msgbus/ma_message_private.h"

struct ma_udp_msg_s{
	ma_udp_msg_type_t			type;

	ma_atomic_counter_t			ref_cnt;  

	union{
		ma_message_t			*ma;
		ma_buffer_t             *raw;
	}message;
};

ma_error_t ma_udp_msg_create(ma_udp_msg_t **msg){
	if(msg) {
		ma_udp_msg_t *tmp = (ma_udp_msg_t *)calloc(1, sizeof(ma_udp_msg_t)) ;

		if(!tmp)	return MA_ERROR_OUTOFMEMORY ;

		MA_ATOMIC_INCREMENT(tmp->ref_cnt) ;
		tmp->type	 = MA_UDP_MESSAGE_TYPE_RAW;
		*msg = tmp ;
		return MA_OK ;
	}
	return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_udp_msg_add_ref(ma_udp_msg_t *msg){
	if(msg) {
		MA_ATOMIC_INCREMENT(msg->ref_cnt) ;		
		return MA_OK ;
	}
	return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_udp_msg_get_type(ma_udp_msg_t *msg, ma_udp_msg_type_t *type){
	if(msg && type){
		*type = msg->type;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

static void udp_payload_destroy(ma_udp_msg_t *msg){
	if(msg->type == MA_UDP_MESSAGE_TYPE_RAW){
		if(msg->message.raw)
			ma_buffer_release(msg->message.raw);
	}
	else{
		if(msg->message.ma)
			ma_message_release(msg->message.ma) ;		
	}

	msg->message.raw = NULL;
	msg->message.ma  = NULL;
}


ma_error_t ma_udp_msg_release(ma_udp_msg_t *msg){
	if(msg) {		
		if(0 == MA_ATOMIC_DECREMENT(msg->ref_cnt)) {
			udp_payload_destroy(msg);
			free(msg) ;
		}
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_udp_msg_set_ma_message(ma_udp_msg_t *msg, ma_message_t *message){
	if(msg && message) {
		udp_payload_destroy(msg);
		msg->type = MA_UDP_MESSAGE_TYPE_MSGBUS;
		ma_message_add_ref(msg->message.ma = message);
		return MA_OK ;
	}
	return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_udp_msg_get_ma_message(ma_udp_msg_t *msg, ma_message_t **message){
	if(msg && message) {
		if(msg->type == MA_UDP_MESSAGE_TYPE_MSGBUS && msg->message.ma){
			ma_message_add_ref(*message = msg->message.ma);		
			return MA_OK ;
		}
		return MA_ERROR_UNEXPECTED;
	}
	return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_udp_msg_set_raw_message(ma_udp_msg_t *msg, ma_buffer_t *message){
	if(msg && message) {
		udp_payload_destroy(msg);
		msg->type = MA_UDP_MESSAGE_TYPE_RAW;
		ma_buffer_add_ref(msg->message.raw = message);
		return MA_OK ;
	}
	return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_udp_msg_get_raw_message(ma_udp_msg_t *msg, ma_buffer_t **message){
	if(msg && message) {
		if(msg->type == MA_UDP_MESSAGE_TYPE_RAW && msg->message.raw){
			ma_buffer_add_ref(*message = msg->message.raw);		
			return MA_OK ;
		}
		return MA_ERROR_UNEXPECTED;
	}
	return MA_ERROR_INVALIDARG ;
}

static ma_bool_t is_relay_raw_message(const unsigned char *buffer, size_t size){
	if(size > 2 * strlen(MA_COMPAT_RELAY_HEADER_FOOTER_STR)){
		/*Check header*/
		if(!strncmp((char*)buffer, MA_COMPAT_RELAY_HEADER_FOOTER_STR, strlen(MA_COMPAT_RELAY_HEADER_FOOTER_STR))){

			/*Check footer*/
			if(!strncmp((char*)buffer+size - strlen(MA_COMPAT_RELAY_HEADER_FOOTER_STR), MA_COMPAT_RELAY_HEADER_FOOTER_STR, strlen(MA_COMPAT_RELAY_HEADER_FOOTER_STR))){
				return MA_TRUE;
			}
		}
		return MA_FALSE;
	}
	return MA_FALSE;
}

static ma_bool_t is_super_agent_wakeup(const unsigned char *buffer, size_t size){
	if(size > strlen(MA_COMPAT_SA_WAKEUP_MESSAGE_HEADER_STR)){
		/*Check header*/
		if(!strncmp((char*)buffer, MA_COMPAT_SA_WAKEUP_MESSAGE_HEADER_STR, strlen(MA_COMPAT_SA_WAKEUP_MESSAGE_HEADER_STR))){
			return MA_TRUE;			
		}
		return MA_FALSE;
	}
	return MA_FALSE;
}

static ma_error_t form_msgbus_message(const unsigned char *buffer, size_t size, ma_udp_msg_t **udp_msg){
	ma_error_t rc = MA_OK ;
	ma_deserializer_t  *deserializer = NULL;
	ma_variant_t *var_msg			 = NULL;
	ma_message_t *msg				 = NULL;
	ma_udp_msg_t *udp_message        = NULL;

	if(MA_OK == (rc = ma_deserializer_create(&deserializer))) {
		if(MA_OK == (rc = ma_deserializer_put(deserializer, (const char *)buffer, size))) {
			if(MA_OK == (rc = ma_deserializer_get_next(deserializer, &var_msg))){
				if(MA_OK == (rc = ma_message_from_network_variant(var_msg, &msg))){
					if(MA_OK == (rc = ma_udp_msg_create(&udp_message))) {
						ma_udp_msg_set_ma_message(udp_message, msg) ;	
						*udp_msg = udp_message;
					}
					(void)ma_message_release(msg) ;						
				}
				(void)ma_variant_release(var_msg) ;  var_msg = NULL ;					
			}
		}
		(void)ma_deserializer_destroy(deserializer) ; deserializer = NULL ;
	}
	return rc ;
}

static ma_error_t form_raw_message(const unsigned char *buffer, size_t size, ma_udp_msg_t **udp_msg){
	ma_error_t rc				= MA_OK;
	ma_buffer_t *mabuffer		= NULL;
	ma_udp_msg_t *udp_message   = NULL;
	if(MA_OK == (rc = ma_buffer_create(&mabuffer, size))){
		if(MA_OK == (rc = ma_buffer_set(mabuffer, (char*)buffer, size))){
			if(MA_OK == (rc = ma_udp_msg_create(&udp_message))) {
				(void)ma_udp_msg_set_raw_message(udp_message, mabuffer) ;	
				*udp_msg = udp_message;
			}					
		}
		(void)ma_buffer_release(mabuffer);
	}
	return rc;
}

ma_error_t ma_udp_msg_deserialize(const unsigned char *buffer, size_t size, ma_udp_msg_t **udp_msg){
	if(buffer && size && udp_msg) {
		if(is_relay_raw_message(buffer, size) || is_super_agent_wakeup(buffer, size))
			return form_raw_message(buffer, size, udp_msg);
		else if(MA_OK == form_msgbus_message(buffer, size, udp_msg))
			return MA_OK;		
		/*else
			return form_raw_message(buffer, size, udp_msg);		*/
	}
	return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_udp_msg_serialize(ma_udp_msg_t *msg, ma_bytebuffer_t **bstream)  {
	if(bstream && msg) {
		ma_error_t rc = MA_ERROR_APIFAILED;
		ma_bytebuffer_t *serialized_buff = NULL;

		if(MA_UDP_MESSAGE_TYPE_RAW  == msg->type && msg->message.raw){
			const unsigned char *buffer = NULL;
			size_t size = 0;
			if(MA_OK == (rc = ma_buffer_get_raw(msg->message.raw, &buffer, &size)) && size){
				if(MA_OK == (rc = ma_bytebuffer_create(size, &serialized_buff))){
					(void)ma_bytebuffer_set_bytes(serialized_buff, 0, buffer, size);
					*bstream = serialized_buff;					
				}			
			}			
		}
		else if(MA_UDP_MESSAGE_TYPE_MSGBUS == msg->type && msg->message.ma){

			ma_variant_t *msg_var = NULL;
			if(MA_OK == (rc = ma_message_as_network_variant(msg->message.ma, &msg_var))){
				ma_serializer_t *serializer = NULL ;
				if(MA_OK == (rc = ma_serializer_create(&serializer))){
					if(MA_OK == (rc = ma_serializer_add_variant(serializer, msg_var))) {
						char *buffer = NULL;
						size_t size = 0;
						if(MA_OK == (rc = ma_serializer_get(serializer, &buffer, &size))) {
							if(MA_OK == (rc = ma_bytebuffer_create(size, &serialized_buff))){
								ma_bytebuffer_set_bytes(serialized_buff, 0, (unsigned char *)buffer, size);
								*bstream = serialized_buff;
							}
						}
					}
					(void)ma_serializer_destroy(serializer); 
				}
				(void)ma_variant_release(msg_var);
			}			
		}
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

