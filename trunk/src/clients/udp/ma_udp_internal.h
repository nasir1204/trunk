#ifndef MA_UDP_INTERNAL_H_INCLUDED
#define MA_UDP_INTERNAL_H_INCLUDED


#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/utils/event_loop/ma_uv_helpers.h"
#include "ma/internal/clients/udp/ma_udp_client.h"
#include "ma/internal/clients/udp/ma_udp_request.h"
#include "ma/internal/clients/udp/ma_udp_msg.h"
#include "ma/internal/utils/threading/ma_atomic.h"
#include "ma/internal/utils/datastructures/ma_slist.h"
#include "ma/internal/utils/network/ma_net_interface.h"
#include "ma/internal/utils/platform/ma_net_interface_list_internal.h"
#include "ma/ma_log.h"

#include <uv.h>

MA_CPP(extern "C" {)

#define MA_UDP_DEFAULT_TIMEOUT				(2 * 1000)
#define MA_UDP_BUF_SIZE						2048
#define MA_UDP_TTL							128
#define MA_GLOBAL_BROADCAST_ADDR			"255.255.255.255"
#define MA_DISCOVERY_TIMEOUT				(2*1000)

struct ma_udp_client_s {
	ma_logger_t			*logger;
	ma_event_loop_t		*ma_loop;	
	ma_loop_worker_t	*loop_worker;
};

typedef struct ma_udp_publisher_s  ma_udp_publisher_t;
typedef struct ma_udp_respondent_list_s  ma_udp_respondent_list_t;

/* It is used in sync_send call to wait for status and response*/
typedef struct sync_request_data_s{	   
    uv_mutex_t		mutex;

    uv_cond_t		condition;

} sync_request_data_t;

typedef enum ma_udp_request_state_e{
	MA_UDP_REQUEST_STATE_INIT    = 0,
	MA_UDP_REQUEST_STATE_STARTED,
	MA_UDP_REQUEST_STATE_STOPPED
}ma_udp_request_state_t;

struct ma_udp_request_s {
	ma_udp_client_t				*udp_client;
		
	ma_udp_respondent_list_t    *respondents;

	ma_bool_t					is_user_cancelled;

	/*request setting.*/	
	char						*multicast_addr;

	ma_uint16_t					routing_port;

	ma_uint64_t					timeout_ms;
	
	udp_request_send_cb_t		send_cb;

	udp_response_cb_t			response_cb;

	udp_request_finalize_cb_t	final_cb;

	void						*cb_data;

	ma_bytebuffer_t             **message_buffers;

	ma_uint16_t                 no_of_messages;

	ma_uint16_t                 send_pending;
	/*request handle*/
	ma_bool_t					is_sync_request;

	ma_udp_request_state_t		state;

	uv_timer_t					*udp_request_timer;

	ma_udp_publisher_t			**udp_publishers; 

	size_t						no_of_publishers; 

	sync_request_data_t         *sync_data;
		
};



struct ma_udp_publisher_s{

	uv_udp_send_t				send_req;

	uv_udp_t					udp_handle;	

	ma_uint32_t					scope_id;

	ma_bool_t                   is_ipv6_handle;

	ma_bool_t                   is_receiving;

	ma_bool_t                   is_active;

	ma_logger_t                 *logger;

	char						alloc_buffer[MA_UDP_BUF_SIZE];	

	uv_buf_t					send_buf;

	ma_udp_request_t			*udp_request;
		
};

ma_error_t ma_udp_request_cancel(ma_udp_request_t *self);

ma_error_t ma_udp_publisher_create(ma_udp_request_t *request, ma_udp_publisher_t **handle);
	
ma_error_t ma_udp6_publisher_create(ma_udp_request_t *request, ma_uint32_t scope_id, ma_udp_publisher_t **handle);
	
void ma_udp_publisher_set_msg(ma_udp_publisher_t *handle, ma_bytebuffer_t *buffer);

void ma_udp_publisher_release(ma_udp_publisher_t *handle);
	
ma_error_t ma_udp_publisher_start(ma_udp_publisher_t *handle);
	
ma_error_t ma_udp_publisher_stop(ma_udp_publisher_t *handle);
	

ma_udp_respondent_list_t *ma_udp_respondent_list_create();

void ma_udp_respondent_list_release(ma_udp_respondent_list_t *respondents);

ma_bool_t ma_udp_respondent_list_is_exist(ma_udp_respondent_list_t *respondents, const char *peer);

void ma_udp_respondent_list_add(ma_udp_respondent_list_t *respondents, const char *peer);

MA_CPP(})

#endif /*MA_UDP_INTERNAL_H_INCLUDED */


