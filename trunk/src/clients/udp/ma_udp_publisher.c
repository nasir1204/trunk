#include <stdio.h>
#ifndef WIN32
#include <sys/types.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <errno.h>
#else
#include <ws2tcpip.h>
#endif

#include "ma_udp_internal.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/ma_macros.h"
#include "ma/ma_log.h"

#ifndef MA_WINDOWS
#include <errno.h>
#endif

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "udp_client"

static void udp_handle_close_cb(uv_handle_t* handle);
static void print_uv_error(ma_udp_publisher_t *self, const char *operation);
static void print_last_socket_error(ma_udp_publisher_t *self, int ret);
extern ma_error_t set_v6_client_option(uv_udp_t *handle, size_t if_scopeid, int loopback, int ttl, ma_logger_t *logger);

static int ma_udp_bind_ipv4(ma_udp_publisher_t *self) {
	struct sockaddr_in sa = uv_ip4_addr("0.0.0.0", 0);
	return uv_udp_bind(&self->udp_handle, sa, 0);
}

static int ma_udp_bind_ipv6(ma_udp_publisher_t *self) {
	struct sockaddr_in6 sa = uv_ip6_addr("::", 0);
	return uv_udp_bind6(&self->udp_handle, sa, 0);
}

ma_error_t ma_udp_publisher_create(ma_udp_request_t *request, ma_udp_publisher_t **handle){
	if(request && handle){
		ma_udp_publisher_t *self = (ma_udp_publisher_t*) calloc(1, sizeof(ma_udp_publisher_t));
		if(self){
			self->logger = request->udp_client->logger;
			self->udp_request = request;

			if(UV_OK == uv_udp_init(ma_event_loop_get_uv_loop(request->udp_client->ma_loop), &self->udp_handle)){			   
			   if(UV_OK == ma_udp_bind_ipv4(self)){ /*udp ipv4*/
				   if(UV_OK == uv_udp_set_broadcast(&self->udp_handle, MA_TRUE)){	
						self->udp_handle.data = self;
						self->send_req.data = self;
						self->send_buf.base = NULL;
						self->send_buf.len  = 0;
						self->is_active = MA_TRUE;
						*handle = self;
						return MA_OK;
				   }
				   else 
					   print_uv_error(self, "udp broadcast");
			   }
			   else 
				   print_uv_error(self, "udp bind ipv4");
			   uv_close((uv_handle_t *)&self->udp_handle, udp_handle_close_cb);
			   return MA_ERROR_UNEXPECTED;
			}
			else 
				print_uv_error(self, "udp handle init");
			free(self);
			return MA_ERROR_UNEXPECTED;
		}
		return MA_ERROR_OUTOFMEMORY;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_udp6_publisher_create(ma_udp_request_t *request, ma_uint32_t scope_id, ma_udp_publisher_t **handle){
	if(request && handle){
		ma_udp_publisher_t *self = (ma_udp_publisher_t*) calloc(1, sizeof(ma_udp_publisher_t));
		if(self){
			self->logger = request->udp_client->logger;
			self->udp_request = request;

			if(UV_OK == uv_udp_init(ma_event_loop_get_uv_loop(request->udp_client->ma_loop), &self->udp_handle)){
				if(UV_OK == ma_udp_bind_ipv6(self)){					
					size_t scope_id_v = scope_id;
					if(MA_OK == set_v6_client_option(&self->udp_handle, scope_id_v, 0, MA_UDP_TTL, self->logger)){										
						self->scope_id = scope_id;
						self->is_ipv6_handle = MA_TRUE;
						self->udp_handle.data = self;
						self->send_req.data = self;
						self->send_buf.base = NULL;
						self->send_buf.len  = 0;
						self->is_active = MA_TRUE;
						*handle = self;
						return MA_OK;
					}
				}
				else
					print_uv_error(self, "udp bind ipv6");

			   uv_close((uv_handle_t *)&self->udp_handle, udp_handle_close_cb);
			   return MA_ERROR_UNEXPECTED;
			}
			else
				print_uv_error(self, "udp6 handle init");
			free(self);
			return MA_ERROR_UNEXPECTED;
		}
		return MA_ERROR_OUTOFMEMORY;
	}
	return MA_ERROR_INVALIDARG;
}

void ma_udp_publisher_set_msg(ma_udp_publisher_t *self, ma_bytebuffer_t *buffer){
	if(self && buffer){		
		self->send_buf = uv_buf_init((char*) ma_bytebuffer_get_bytes(buffer), ma_bytebuffer_get_size(buffer));
	}
}

void ma_udp_publisher_release(ma_udp_publisher_t *self){
	if(self)free(self);
}

static uv_buf_t alloc_cb(uv_handle_t* handle, size_t suggested_size) {
	ma_udp_publisher_t *self = (ma_udp_publisher_t *)  handle->data;
	return uv_buf_init(self->alloc_buffer, MA_UDP_BUF_SIZE);
}

static void recv_cb(uv_udp_t* handle, ssize_t nread, uv_buf_t buf, struct sockaddr* addr, unsigned flags) {
	
	if(nread < 0){
		return; /* "unexpected error" */
	} 
	else if (nread == 0){
		return;
	}
	else if (0 == flags){
		ma_udp_publisher_t *self = (ma_udp_publisher_t *)  handle->data;
		struct sockaddr_storage sa;
		int namelen = sizeof(sa);
		char peer_addr[INET6_ADDRSTRLEN] = {0};			
		ma_uint16_t port_n = 0;
		MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Data received at udp handle <%p>.", handle);

		if(self->is_active){

			if(AF_INET6 == addr->sa_family){ 
				struct sockaddr_in6 *sa6 = (struct sockaddr_in6 *) addr;
				uv_ip6_name(sa6, peer_addr, MA_COUNTOF(peer_addr));
				port_n = ntohs(sa6->sin6_port);
			} 
			else{
				struct sockaddr_in *sa4 = (struct sockaddr_in *) addr;
				uv_ip4_name(sa4, peer_addr, MA_COUNTOF(peer_addr));
				port_n = ntohs(sa4->sin_port);
			}
			MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "received reply from %s:%u", peer_addr, (unsigned) port_n);

			if(ma_udp_respondent_list_is_exist(self->udp_request->respondents, peer_addr)){
				MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Duplicate response from peer %s, so ignoring it", peer_addr);
				return;
			}			
		
			if(buf.base && nread && self->udp_request->response_cb){
				ma_udp_response_t response;						

				if(MA_OK == ma_udp_msg_deserialize((const unsigned char *)buf.base, nread, &response.response)){
					ma_bool_t is_stopped_requested = MA_FALSE;				
					MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "received response from %s at nic %u", peer_addr, self->scope_id);
				
					response.receiving_local_nic = self->scope_id;
					response.peer_addr = peer_addr;

					if(MA_OK == self->udp_request->response_cb(self->udp_request, &response, self->udp_request->cb_data, &is_stopped_requested))
						ma_udp_respondent_list_add(self->udp_request->respondents, peer_addr);			

					if(is_stopped_requested){
						(void)ma_udp_request_cancel(self->udp_request);
						return;
					}
				}
				else
					MA_LOG(self->logger, MA_LOG_SEV_WARNING, "Failed to form the udp message.");
			}
		}
	}
}

static void udp_handle_close_cb(uv_handle_t* handle) {
	ma_udp_publisher_t *self = (ma_udp_publisher_t *)  handle->data;	
	MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "udp handle <%p> closed, releasing it.", handle);
	ma_udp_publisher_release(self);
}

static void send_cb(uv_udp_send_t *req, int status) {
	ma_udp_publisher_t *self = (ma_udp_publisher_t *) req->data;
	if(self->is_active && status == 0){		
		MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Data is sent from udp handle <%p>, pending sending items %d.", req->handle, --self->udp_request->send_pending);

		if(0 == self->udp_request->send_pending){ 
			/*all sent so call the send call back if set.*/
			if(self->udp_request->send_cb){
				ma_bool_t stop_requested = MA_FALSE;
				self->udp_request->send_cb(self->udp_request, self->udp_request->cb_data, &stop_requested);

				if(stop_requested){
					(void)ma_udp_request_cancel(self->udp_request);
					return;
				}
			}
			/*user not interested in the response to close the connection.*/
			if(NULL == self->udp_request->response_cb){
				(void)ma_udp_request_cancel(self->udp_request);
				return;
			}			
		}			
		/*if user is interested in the response then only start receiving.*/
		if(self->udp_request->response_cb){
			if(UV_OK == uv_udp_recv_start(req->handle, alloc_cb, recv_cb))
				self->is_receiving = MA_TRUE;
			else
				print_uv_error(self, "uv_udp_recv_start");					
		}
	}
}

ma_error_t ma_udp_publisher_start(ma_udp_publisher_t *self){
	if(self && self->udp_request  && self->send_buf.len){							
		if(self->is_ipv6_handle) {
			if( UV_OK == uv_udp_send6(&self->send_req, &self->udp_handle, &self->send_buf, 1, uv_ip6_addr(self->udp_request->multicast_addr, self->udp_request->routing_port), send_cb)) {				
				++self->udp_request->send_pending;
				MA_LOG(self->logger, MA_LOG_SEV_TRACE, "Successfully multicast udp message to %s on %u port", self->udp_request->multicast_addr, self->udp_request->routing_port);
			}
			else
				print_uv_error(self, "uv_udp_send6");									
		}
		else{
			if(UV_OK == uv_udp_send(&self->send_req, &self->udp_handle, &self->send_buf, 1, uv_ip4_addr(MA_GLOBAL_BROADCAST_ADDR, self->udp_request->routing_port), send_cb)){				
				++self->udp_request->send_pending;
				MA_LOG(self->logger, MA_LOG_SEV_TRACE, "Successfully broadcast udp message to %s on %u port", MA_GLOBAL_BROADCAST_ADDR, self->udp_request->routing_port);
			}
			else 
				print_uv_error(self, "uv_udp_send");
		}
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_udp_publisher_stop(ma_udp_publisher_t *self){
	if(self){
		if(self->is_receiving) uv_udp_recv_stop(&self->udp_handle);
		uv_close((uv_handle_t *)&self->udp_handle, udp_handle_close_cb);
		self->is_active = MA_FALSE;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}


static void print_uv_error(ma_udp_publisher_t *self, const char *operation){
	uv_err_t uv_err = uv_last_error(ma_event_loop_get_uv_loop(self->udp_request->udp_client->ma_loop));
	MA_LOG(self->logger, MA_LOG_SEV_WARNING, "%s - libuv err: (\'%s\' %s)", operation, uv_strerror(uv_err), uv_err_name(uv_err));
}

static void print_last_socket_error(ma_udp_publisher_t *self, int ret){
	#ifdef WIN32
	MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Socket API returns=%d WSAGetLastError=%d.", ret, WSAGetLastError());;
	#else
	MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Socket API returns=%d ", errno);;
	#endif	
}
