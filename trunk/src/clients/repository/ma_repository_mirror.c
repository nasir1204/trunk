#include "ma/repository/ma_repository_mirrror.h"
#include "ma/internal/defs/ma_repository_defs.h"
#include "ma/internal/defs/ma_general_defs.h"

#include "ma/scheduler/ma_scheduler.h"
#include "ma/internal/services/repository/ma_mirror_request_param.h"
#include "ma/internal/clients/ma/ma_client_internal.h"
#include "ma/internal/clients/ma/ma_client_base.h"
#include "ma/internal/clients/ma/client/ma_client_manager.h"
#include "ma/internal/ma_macros.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

static ma_error_t ma_repository_mirror_client_on_message(ma_client_base_t *client, const char *topic, ma_message_t *message);
static ma_error_t ma_repository_mirror_client_release(ma_client_base_t *client);

static struct ma_client_base_methods_s methods =  {
    &ma_repository_mirror_client_on_message,
    &ma_repository_mirror_client_release
};

typedef struct ma_repository_mirror_client_s{	
    ma_client_base_t                        base;
    char                                    *product_id;
    ma_repository_mirror_state_cb_t         user_cb;   
    void                                    *user_data;
}ma_repository_mirror_client_t;


static ma_error_t ma_repository_mirror_client_create(ma_client_t *ma_client,const char *product_id, ma_repository_mirror_client_t **mirror_client){
    if(ma_client && product_id && mirror_client) {
        ma_repository_mirror_client_t *self = (ma_repository_mirror_client_t *)calloc(1, sizeof(ma_repository_mirror_client_t));
        if(self){
            ma_client_base_init( (ma_client_base_t*)self, &methods, ma_client);
            self->product_id = strdup(product_id);
            *mirror_client = self;
            return MA_OK;
        }
        return MA_ERROR_OUTOFMEMORY;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_repository_mirror_client_release(ma_client_base_t *ma_client){
    if(ma_client){	
        ma_repository_mirror_client_t *self = (ma_repository_mirror_client_t *)ma_client;
        if(self->product_id) free(self->product_id);
        free(self);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}



ma_error_t ma_repository_mirror_task_create(ma_client_t *ma_client, const char *task_id, const char *destination_path, ma_task_t **task) {
    if(ma_client && task && destination_path) {
        ma_task_t *self = NULL;
        ma_error_t rc = MA_OK;
        ma_context_t *ma_context = NULL;

        if((MA_OK == (rc = ma_client_get_context(ma_client, &ma_context))) && ( ma_context)) {
            const char *creator_id = MA_CONTEXT_GET_PRODUCT_ID(ma_context);
            if(MA_OK == (rc = ma_task_create(task_id, &self))) {
                if(MA_OK == (rc = ma_task_set_type(self, MA_MIRROR_TASK_TYPE_MIRROR_STR))) {
                    (void)ma_task_set_creator_id(self, creator_id);
                    if(MA_OK == (rc = ma_task_set_software_id(self, MA_SOFTWAREID_GENERAL_STR))) {
                        ma_variant_t *variant = NULL;
                        if(MA_OK == (rc = ma_variant_create_from_string(destination_path, strlen(destination_path), &variant))) {
                            (void)ma_task_set_setting(self, MA_MIRROR_TASK_OPTIONS_SECTION_STR, MA_MIRROR_TASK_PATH_KEY_STR, variant);
                            ma_variant_release(variant);
                            *task = self;
                            return MA_OK;
                        }
                    }
                }
                (void)ma_task_release(self);
            }
            return rc;
        }
        return MA_ERROR_INVALID_CLIENT_OBJECT;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_repository_mirror_task_get_session_id(ma_task_t *task, ma_buffer_t **session_id) {
    if(task && session_id) {
        const char *task_id = NULL;
        ma_error_t rc = MA_OK;
        if(MA_OK == (rc = ma_task_get_id(task,&task_id))) {
            size_t size = strlen(task_id);
            ma_buffer_t *buffer = NULL;
            if(MA_OK == (rc = ma_buffer_create(&buffer, size))) {
                if(MA_OK == (rc = ma_buffer_set(buffer,task_id, size))) {
                    *session_id = buffer;
                    return MA_OK;
                }
                (void)ma_buffer_release(buffer);
            }
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_repository_mirror_start(ma_client_t *ma_client, const char *destination_path, ma_buffer_t **session_id) {
    if(ma_client && destination_path) {
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;

        ma_client_get_context(ma_client, &ma_context);
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);		

        if(ma_context && client_manager) {
            ma_error_t rc = MA_OK;
            ma_task_t *task = NULL;
            
            if(MA_OK == (rc = ma_repository_mirror_task_create(ma_client, NULL, destination_path, &task))) {                
                ma_mirror_request_param_t params ;
                const char *task_id = NULL;
                
                (void)ma_task_set_condition(task, MA_TASK_COND_DELETE_WHEN_DONE);
                (void)ma_task_set_condition(task, MA_TASK_COND_DISABLED);
                params.initiator_type = MA_REPOSITORY_MIRROR_INITIATOR_TYPE_ONDEMAND_MIRROR;
                params.request_type = MA_MIRROR_REQUEST_TYPE_START;
                if(MA_OK == (rc = ma_task_get_id(task, &task_id))) {
                    ma_variant_t *variant;
                    strncpy(params.task_id, task_id, MA_MAX_TASKID_LEN -1);
                    if(MA_OK == (rc = ma_mirror_request_param_to_variant(params, &variant))) {
                        ma_msgbus_endpoint_t *endpoint = NULL;
                        if(MA_OK == (rc = ma_msgbus_endpoint_create(MA_CONTEXT_GET_MSGBUS(ma_context), MA_REPOSITORY_SERVICE_NAME_STR, MA_REPOSITORY_SERVICE_HOSTNAME_STR,&endpoint))) {
                            ma_message_t *request_msg = NULL;
                            ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;
                            (void) ma_client_manager_get_thread_option(client_manager, &thread_option);
                            (void) ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);
                            (void) ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                            if(MA_OK == (rc = ma_message_create(&request_msg))) {
                                if(MA_OK == (rc = ma_message_set_property(request_msg,MA_REPOSITORY_MSG_KEY_TYPE_STR, MA_REPOSITORY_MSG_PROP_VAL_RQST_MIRROR_TASK_STR))) {
                                    if(MA_OK == (rc = ma_message_set_payload(request_msg, variant))) {
                                        ma_message_t *response_msg = NULL;
                                        if(MA_OK == (rc = ma_scheduler_add_task(ma_client, task))) {
                                            if(MA_OK == (rc = ma_msgbus_send(endpoint, request_msg, &response_msg))) {
                                                (void)ma_message_release(response_msg);
                                                if(session_id) {
                                                    if(MA_OK == (rc = ma_buffer_create(session_id, strlen(task_id))))
                                                        (void)ma_buffer_set(*session_id, task_id, strlen(task_id));
                                                }
                                            }
                                            else
                                                ma_scheduler_remove_task(ma_client, task_id);
                                        }
                                    }
                                }
                                ma_message_release(request_msg);
                            }
                            ma_msgbus_endpoint_release(endpoint);
                        }
                        ma_variant_release(variant);
                    }
                }

                (void)ma_task_release(task);
            }
            return rc;
        }
        return MA_ERROR_INVALID_CLIENT_OBJECT;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_repository_mirror_stop(ma_client_t *ma_client, ma_buffer_t *session_id) {
    if(ma_client && session_id) {
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;

        ma_client_get_context(ma_client, &ma_context);
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);		

        if(ma_context && client_manager) {
            ma_error_t rc = MA_OK;
            const char *task_id = NULL;
            size_t task_id_len = 0;

            if(MA_OK == (rc = ma_buffer_get_string(session_id, &task_id, &task_id_len))) {
                ma_mirror_request_param_t params;
                ma_variant_t *variant = NULL;
                params.initiator_type = MA_REPOSITORY_MIRROR_INITIATOR_TYPE_ONDEMAND_MIRROR;
                params.request_type = MA_MIRROR_REQUEST_TYPE_STOP;
                strncpy(params.task_id, task_id, MA_MAX_TASKID_LEN -1);
                if(MA_OK == (rc = ma_mirror_request_param_to_variant(params, &variant))) {
                    ma_msgbus_endpoint_t *endpoint = NULL;
                    if(MA_OK == (rc = ma_msgbus_endpoint_create(MA_CONTEXT_GET_MSGBUS(ma_context), MA_REPOSITORY_SERVICE_NAME_STR, MA_REPOSITORY_SERVICE_HOSTNAME_STR,&endpoint))) {
                        ma_message_t *request_msg = NULL;
                        ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;
                        (void) ma_client_manager_get_thread_option(client_manager, &thread_option);
                        (void) ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);
                        (void) ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);

                        if(MA_OK == (rc = ma_message_create(&request_msg))) {
                            if(MA_OK == (rc = ma_message_set_property(request_msg,MA_REPOSITORY_MSG_KEY_TYPE_STR, MA_REPOSITORY_MSG_PROP_VAL_RQST_MIRROR_TASK_STR))) {
                                if(MA_OK == (rc = ma_message_set_payload(request_msg, variant))) {
                                    ma_message_t *response_msg = NULL;
                                    if(MA_OK == (rc = ma_msgbus_send(endpoint, request_msg, &response_msg))) {
                                        (void)ma_message_release(response_msg);
                                    }
                                }
                            }
                            ma_message_release(request_msg);
                        }
                        ma_msgbus_endpoint_release(endpoint);
                    }
                    ma_variant_release(variant);
                }
            }
            return rc;
        }
        return MA_ERROR_INVALID_CLIENT_OBJECT;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_repository_mirror_get_state(ma_client_t *ma_client, ma_buffer_t *session_id, ma_repository_mirror_state_t *state) {
    if(ma_client && session_id && state) {
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;

        ma_client_get_context(ma_client, &ma_context);
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);		

        if(ma_context && client_manager) {
            ma_error_t rc = MA_OK;
            const char *task_id = NULL;
            size_t task_id_len = 0;

            if(MA_OK == (rc = ma_buffer_get_string(session_id, &task_id, &task_id_len))) {
                ma_mirror_request_param_t params ;
                
                ma_variant_t *variant = NULL;
                params.initiator_type = MA_REPOSITORY_MIRROR_INITIATOR_TYPE_ONDEMAND_MIRROR;
                params.request_type = MA_MIRROR_REQUEST_TYPE_STATUS_CHECK;
                strncpy(params.task_id, task_id, MA_MAX_TASKID_LEN -1);
                if(MA_OK == (rc = ma_mirror_request_param_to_variant(params, &variant))) {
                    ma_msgbus_endpoint_t *endpoint = NULL;
                    if(MA_OK == (rc = ma_msgbus_endpoint_create(MA_CONTEXT_GET_MSGBUS(ma_context), MA_REPOSITORY_SERVICE_NAME_STR, MA_REPOSITORY_SERVICE_HOSTNAME_STR,&endpoint))) {
                        ma_message_t *request_msg = NULL;
                        ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;
                        (void) ma_client_manager_get_thread_option(client_manager, &thread_option);
                        (void) ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);
                        (void) ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);

                        if(MA_OK == (rc = ma_message_create(&request_msg))) {
                            if(MA_OK == (rc = ma_message_set_property(request_msg,MA_REPOSITORY_MSG_KEY_TYPE_STR, MA_REPOSITORY_MSG_PROP_VAL_RQST_MIRROR_TASK_STR))) {
                                if(MA_OK == (rc = ma_message_set_payload(request_msg, variant))) {
                                    ma_message_t *response_msg = NULL;
                                    if(MA_OK == (rc = ma_msgbus_send(endpoint, request_msg, &response_msg))) {
                                        ma_variant_t *payload = NULL;
                                        if(MA_OK == (rc = ma_message_get_payload(response_msg, &payload))){
                                            ma_int16_t mirror_state = 0;
                                            if(MA_OK == (rc = ma_variant_get_int16(payload, &mirror_state))){
                                                *state = (ma_repository_mirror_state_t)mirror_state;											
                                            }
                                            ma_variant_release(payload);
                                        }
                                        (void)ma_message_release(response_msg);
                                    }
                                }
                            }
                            ma_message_release(request_msg);
                        }
                        ma_msgbus_endpoint_release(endpoint);
                    }
                    ma_variant_release(variant);
                }
            }
            return rc;
        }
        return MA_ERROR_INVALID_CLIENT_OBJECT;
    }
    return MA_ERROR_INVALIDARG;

}



ma_error_t ma_repository_mirror_register_state_callback(ma_client_t *ma_client, ma_repository_mirror_state_cb_t cb, void *cb_data) {
    if(ma_client && cb){		
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;
        const char *product_id = NULL;

        ma_client_get_context(ma_client, &ma_context);
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);		
        product_id = MA_CONTEXT_GET_PRODUCT_ID(ma_context);

        if(ma_context && client_manager && product_id) {
            ma_error_t rc = MA_OK;
            ma_repository_mirror_client_t *mirror_client = NULL;
            char client_name[MAX_CLIENT_NAME_LENGTH] = {0};
            
            /* Make the policy consumer name*/
            MA_MSC_SELECT(_snprintf, snprintf)(client_name, MAX_CLIENT_NAME_LENGTH,"%s.%s",MA_REPOSITORY_MIRROR_CLIENT_NAME_STR, product_id);           

            if(ma_client_manager_has_client(client_manager, client_name)) {		
                return MA_ERROR_ALREADY_REGISTERED;			
            }
            else {
                if(MA_OK == (rc = ma_repository_mirror_client_create(ma_client, product_id, &mirror_client) ) ){
                    mirror_client->user_cb = cb;
                    mirror_client->user_data = cb_data;
                    if(MA_OK != (rc = ma_client_manager_add_client(client_manager, client_name, (ma_client_base_t*)mirror_client) ) ) {
                        ma_repository_mirror_client_release((ma_client_base_t*)mirror_client);					
                    }								
                }		
            }		
            return rc;
        }
        return MA_ERROR_INVALID_CLIENT_OBJECT;
    }
    return MA_ERROR_INVALIDARG;
}


static ma_error_t ma_repository_mirror_client_on_message(ma_client_base_t *ma_client, const char *topic, ma_message_t *message){    
    ma_error_t rc = MA_OK;
    ma_repository_mirror_client_t *self = (ma_repository_mirror_client_t *)ma_client;
    if(self && topic && message && self->user_cb) {
        if(!strncmp(topic, MA_REPOSITORY_PUBSUB_TOPIC_MIRROR_STATE_STR, strlen(MA_REPOSITORY_PUBSUB_TOPIC_MIRROR_STATE_STR))) {
            ma_variant_t *payload = NULL;
            if(MA_OK == (rc = ma_message_get_payload(message, &payload))) {
                ma_int16_t state = -1;
                if(MA_OK == (rc = ma_variant_get_int16(payload, &state))) {
                    const char *session_id = NULL;
                    if(MA_OK == (rc = ma_message_get_property(message, MA_REPOSITORY_MSG_PROP_KEY_MIRROR_SESSION_ID_STR, &session_id))) {
                        rc = self->user_cb((ma_client_t *)self->base.data, session_id, (ma_repository_mirror_state_t)state, self->user_data);
                    }
                }
                (void)ma_variant_release(payload);
            }
        }
    }
    return rc;
}
