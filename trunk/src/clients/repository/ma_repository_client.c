#include "ma/repository/ma_repository_client.h"
#include "ma/internal/defs/ma_repository_defs.h"
#include "ma/ma_message.h"
#include "ma/ma_msgbus.h"
#include "ma/internal/clients/ma/ma_client_base.h"
#include "ma/internal/utils/proxy/ma_proxy_internal.h"
#include "ma/internal/clients/ma/ma_client_internal.h"
#include "ma/internal/utils/repository/ma_repository_internal.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/clients/ma/client/ma_client_manager.h"
#include "ma/internal/defs/ma_object_defs.h"

#include <stdlib.h>
#include <stdio.h>

ma_error_t ma_repository_client_get_repositories(ma_client_t *ma_client, ma_repository_list_t **repository_list){
	if(ma_client && repository_list) {
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;  
        const char *product_id = NULL;
        ma_msgbus_t *msgbus = NULL;            
        ma_client_get_context(ma_client, &ma_context);
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);
        product_id = MA_CONTEXT_GET_PRODUCT_ID(ma_context) ;
        msgbus = MA_CONTEXT_GET_MSGBUS(ma_context);

        if(product_id && msgbus && client_manager) {
            ma_error_t rc = MA_OK ;
            ma_message_t *get_repositories_msg = NULL, *response = NULL ;
            if(MA_OK == (rc = ma_message_create(&get_repositories_msg))) {
                ma_msgbus_endpoint_t *endpoint = NULL ;
                ma_message_set_property(get_repositories_msg, MA_REPOSITORY_MSG_KEY_TYPE_STR, MA_REPOSITORY_MSG_PROP_VAL_RQST_GET_REPOSITORIES_STR) ;
                ma_message_set_property(get_repositories_msg, MA_REPOSITORY_MSG_PROP_KEY_PRODUCT_ID_STR, product_id) ;
            
                if(MA_OK == (rc = ma_msgbus_endpoint_create(msgbus, MA_REPOSITORY_SERVICE_NAME_STR, NULL, &endpoint))) {
                    ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;                
                    (void) ma_client_manager_get_thread_option(client_manager, &thread_option);
                    ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);
				    ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);

                    if(MA_OK == (rc = ma_msgbus_send(endpoint, get_repositories_msg, &response))) {
					    ma_variant_t *res_payload = NULL;
                        if(MA_OK == (rc = ma_message_get_payload(response, &res_payload))) {
						    rc = ma_repository_list_from_variant(res_payload, repository_list);
                            (void)ma_variant_release(res_payload);
                        }
                        ma_message_release(response) ;
                    }
                    ma_msgbus_endpoint_release(endpoint) ;
                }
                ma_message_release(get_repositories_msg) ;
            }
            return rc ;
        }
        return MA_ERROR_INVALID_CLIENT_OBJECT;
    }
    return MA_ERROR_INVALIDARG ;
}
		   
ma_error_t ma_repository_client_import_repositories(ma_client_t *ma_client, ma_buffer_t *repo_buffer){
	if(ma_client && repo_buffer) {
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;  
        const char *product_id = NULL;
        ma_msgbus_t *msgbus = NULL;            
        ma_client_get_context(ma_client, &ma_context);
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);
        product_id = MA_CONTEXT_GET_PRODUCT_ID(ma_context) ;
        msgbus = MA_CONTEXT_GET_MSGBUS(ma_context);

        if(product_id && msgbus && client_manager) {
            ma_error_t rc = MA_OK ;
            ma_message_t *get_repositories_msg = NULL, *response = NULL ;
		    
            if(MA_OK == (rc = ma_message_create(&get_repositories_msg))) {
				ma_variant_t *payload= NULL;
				ma_msgbus_endpoint_t *endpoint = NULL ;
				const char *str = NULL;
				size_t  len;
				if(MA_OK == (rc = ma_buffer_get_string(repo_buffer, &str, &len))){
					if(MA_OK == (rc = ma_variant_create_from_string(str, len, &payload))) {
						(void)ma_message_set_property(get_repositories_msg, MA_REPOSITORY_MSG_KEY_TYPE_STR, MA_REPOSITORY_MSG_PROP_VAL_RQST_IMPORT_REPOSITORIES_STR) ;
						(void)ma_message_set_property(get_repositories_msg, MA_REPOSITORY_MSG_PROP_KEY_PRODUCT_ID_STR, product_id) ;
						(void)ma_message_set_payload(get_repositories_msg, payload);
            
						if(MA_OK == (rc = ma_msgbus_endpoint_create(msgbus, MA_REPOSITORY_SERVICE_NAME_STR, NULL, &endpoint))) {
							ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;                
							(void) ma_client_manager_get_thread_option(client_manager, &thread_option);
							(void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);
							(void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
							if(MA_OK == (rc = ma_msgbus_send(endpoint, get_repositories_msg, &response))) {
								(void)ma_message_release(response) ;
							}
							(void)ma_msgbus_endpoint_release(endpoint) ;
						}
					}
				}
				(void)ma_message_release(get_repositories_msg) ;
            }
            return rc ;
        }
        return MA_ERROR_INVALID_CLIENT_OBJECT;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_repository_client_set_repositories(ma_client_t *ma_client, ma_repository_list_t *repository_list){
	if(ma_client && repository_list) {
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;  
        const char *product_id = NULL;
        ma_msgbus_t *msgbus = NULL;            
        ma_client_get_context(ma_client, &ma_context);
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);
        product_id = MA_CONTEXT_GET_PRODUCT_ID(ma_context) ;
        msgbus = MA_CONTEXT_GET_MSGBUS(ma_context);

        if(product_id && msgbus && client_manager) {
            ma_error_t rc = MA_OK ;
            ma_message_t *get_repositories_msg = NULL, *response = NULL ;
		    
            if(MA_OK == (rc = ma_message_create(&get_repositories_msg))) {
				ma_variant_t *payload= NULL;
				ma_msgbus_endpoint_t *endpoint = NULL ;
				if(MA_OK == (rc = ma_repository_list_as_variant(repository_list, &payload))) {
					ma_message_set_property(get_repositories_msg, MA_REPOSITORY_MSG_KEY_TYPE_STR, MA_REPOSITORY_MSG_PROP_VAL_RQST_UPDATE_REPOSITORIES_STR) ;
					ma_message_set_property(get_repositories_msg, MA_REPOSITORY_MSG_PROP_KEY_PRODUCT_ID_STR, product_id) ;
					ma_message_set_payload(get_repositories_msg, payload);
            
					if(MA_OK == (rc = ma_msgbus_endpoint_create(msgbus, MA_REPOSITORY_SERVICE_NAME_STR, NULL, &endpoint))) {
						ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;                
                        (void) ma_client_manager_get_thread_option(client_manager, &thread_option);
                        ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);
				        ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
						if(MA_OK == (rc = ma_msgbus_send(endpoint, get_repositories_msg, &response))) {
							ma_message_release(response) ;
						}
						ma_msgbus_endpoint_release(endpoint) ;
					}
					(void)ma_variant_release(payload);
				}
                (void)ma_message_release(get_repositories_msg) ;
            }
            return rc ;
        }
        return MA_ERROR_INVALID_CLIENT_OBJECT;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_repository_client_get_proxy_config(ma_client_t *ma_client, ma_proxy_config_t **proxy_config){
	if(ma_client && proxy_config) {
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;  
        const char *product_id = NULL;
        ma_msgbus_t *msgbus = NULL;            
        ma_client_get_context(ma_client, &ma_context);
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);
        product_id = MA_CONTEXT_GET_PRODUCT_ID(ma_context) ;
        msgbus = MA_CONTEXT_GET_MSGBUS(ma_context);

        if(product_id && msgbus && client_manager) {
            ma_error_t rc = MA_OK ;
            ma_message_t *get_repositories_msg = NULL, *response = NULL ;
		
		    if(MA_OK == (rc = ma_message_create(&get_repositories_msg))) {
                ma_msgbus_endpoint_t *endpoint = NULL ;
                ma_message_set_property(get_repositories_msg, MA_REPOSITORY_MSG_KEY_TYPE_STR, MA_REPOSITORY_MSG_PROP_VAL_RQST_GET_PROXIES_STR) ;
                ma_message_set_property(get_repositories_msg, MA_REPOSITORY_MSG_PROP_KEY_PRODUCT_ID_STR, product_id) ;
            
                if(MA_OK == (rc = ma_msgbus_endpoint_create(msgbus, MA_REPOSITORY_SERVICE_NAME_STR, NULL, &endpoint))) {
                    ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;                
                    (void) ma_client_manager_get_thread_option(client_manager, &thread_option);
                    ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);
				    ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);

                    if(MA_OK == (rc = ma_msgbus_send(endpoint, get_repositories_msg, &response))) {
					    ma_variant_t *res_payload = NULL;
                        if(MA_OK == (rc = ma_message_get_payload(response, &res_payload))) {
						    rc = ma_proxy_config_from_variant(res_payload, proxy_config);
                            (void)ma_variant_release(res_payload);
                        }
                        (void)ma_message_release(response) ;
                    }
                    ma_msgbus_endpoint_release(endpoint) ;
                }
                (void)ma_message_release(get_repositories_msg) ;
            }
            return rc ;
        }
        return MA_ERROR_INVALID_CLIENT_OBJECT;
    }
    return MA_ERROR_INVALIDARG ;
}


ma_error_t ma_repository_client_set_proxy_config(ma_client_t *ma_client, ma_proxy_config_t *proxy_config){
	if(ma_client && proxy_config) {
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;  
        const char *product_id = NULL;
        ma_msgbus_t *msgbus = NULL;            
        ma_client_get_context(ma_client, &ma_context);
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);
        product_id = MA_CONTEXT_GET_PRODUCT_ID(ma_context) ;
        msgbus = MA_CONTEXT_GET_MSGBUS(ma_context);

        if(product_id && msgbus && client_manager) {
            ma_error_t rc = MA_OK ;
            ma_message_t *get_repositories_msg = NULL, *response = NULL ;
		
            if(MA_OK == (rc = ma_message_create(&get_repositories_msg))) {
				ma_variant_t *payload= NULL;
				ma_msgbus_endpoint_t *endpoint = NULL ;
				if(MA_OK == (rc = ma_proxy_config_as_variant(proxy_config, &payload))) {
					ma_message_set_property(get_repositories_msg, MA_REPOSITORY_MSG_KEY_TYPE_STR, MA_REPOSITORY_MSG_PROP_VAL_RQST_UPDATE_PROXIES_STR) ;
					ma_message_set_property(get_repositories_msg, MA_REPOSITORY_MSG_PROP_KEY_PRODUCT_ID_STR, product_id) ;
					ma_message_set_payload(get_repositories_msg, payload);
            
					if(MA_OK == (rc = ma_msgbus_endpoint_create(msgbus, MA_REPOSITORY_SERVICE_NAME_STR, NULL, &endpoint))) {
						    ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;                
                        (void) ma_client_manager_get_thread_option(client_manager, &thread_option);
                        ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);
				        ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
						
                        if(MA_OK == (rc = ma_msgbus_send(endpoint, get_repositories_msg, &response))) {
							ma_message_release(response);
						}
						ma_msgbus_endpoint_release(endpoint) ;
					}
					(void)ma_variant_release(payload);
				}
                (void)ma_message_release(get_repositories_msg) ;
            }
            return rc ;
        }
        return MA_ERROR_INVALID_CLIENT_OBJECT;
    }
    return MA_ERROR_INVALIDARG ;
}

typedef struct async_data_s {
    ma_client_t                 *ma_client;
    ma_uint8_t                  request_type; /* 1 for repository request, 2 for proxy config request , 3 for system proxy request*/
    union {
        ma_repositories_get_cb_t    repo_cb;
        ma_proxy_config_get_cb_t    proxy_cb;
		ma_system_proxy_get_cb_t    system_proxy_cb;
    } request_cb;
    void                        *cb_data;
} async_data_t;

static ma_error_t async_request_cb(ma_error_t status, ma_message_t *response, void *cb_data, ma_msgbus_request_t *request) {
    async_data_t *async_data = (async_data_t *) cb_data;
    ma_error_t rc = MA_OK;

    if(async_data) {
        ma_variant_t *res_payload = NULL;
        if(1 == async_data->request_type)  {
            ma_repository_list_t *repository_list = NULL;            
            if(MA_OK == (rc = ma_message_get_payload(response, &res_payload))) {
			    rc = ma_repository_list_from_variant(res_payload, &repository_list);            
                (void)ma_variant_release(res_payload);
            }
        
            async_data->request_cb.repo_cb(status, async_data->ma_client, repository_list, async_data->cb_data);
            if(repository_list) ma_repository_list_release(repository_list);
        }
        else if(2 == async_data->request_type){
            ma_proxy_config_t *proxy_config = NULL;            
            if(MA_OK == (rc = ma_message_get_payload(response, &res_payload))) {
			    rc = ma_proxy_config_from_variant(res_payload, &proxy_config);            
                (void)ma_variant_release(res_payload);
            }
        
            async_data->request_cb.proxy_cb(status, async_data->ma_client, proxy_config, async_data->cb_data);
            if(proxy_config) ma_proxy_config_release(proxy_config);
        }
		else if(3 == async_data->request_type){
            ma_proxy_list_t *sys_proxy_list = NULL;            
            if(MA_OK == (rc = ma_message_get_payload(response, &res_payload))) {
			    rc = ma_proxy_list_from_variant(res_payload, &sys_proxy_list);            
                (void)ma_variant_release(res_payload);
            }
        
            async_data->request_cb.system_proxy_cb(status, async_data->ma_client, sys_proxy_list, async_data->cb_data);
            if(sys_proxy_list) ma_proxy_list_release(sys_proxy_list);
        }
        free(async_data); async_data = NULL;
    }

	if((status == MA_OK) && response) ma_message_release(response);
	if(request) {
		ma_msgbus_endpoint_t * ep = NULL;
		(void) ma_msgbus_request_get_endpoint(request, &ep);         
        (void) ma_msgbus_request_release(request);          
        if(ep) (void) ma_msgbus_endpoint_release(ep);
	}
	return MA_OK;
}

ma_error_t ma_repository_client_async_get_repositories(ma_client_t *ma_client, ma_repositories_get_cb_t cb, void *cb_data) {
    if(ma_client && cb) {
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;  
        const char *product_id = NULL;
        ma_msgbus_t *msgbus = NULL;            
        ma_client_get_context(ma_client, &ma_context);
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);
        product_id = MA_CONTEXT_GET_PRODUCT_ID(ma_context) ;
        msgbus = MA_CONTEXT_GET_MSGBUS(ma_context);

        if(product_id && msgbus && client_manager) {
            ma_error_t rc = MA_OK ;
            ma_message_t *get_repositories_msg = NULL;
            if(MA_OK == (rc = ma_message_create(&get_repositories_msg))) {
                ma_msgbus_endpoint_t *endpoint = NULL ;
                ma_message_set_property(get_repositories_msg, MA_REPOSITORY_MSG_KEY_TYPE_STR, MA_REPOSITORY_MSG_PROP_VAL_RQST_GET_REPOSITORIES_STR) ;
                ma_message_set_property(get_repositories_msg, MA_REPOSITORY_MSG_PROP_KEY_PRODUCT_ID_STR, product_id) ;
            
                if(MA_OK == (rc = ma_msgbus_endpoint_create(msgbus, MA_REPOSITORY_SERVICE_NAME_STR, NULL, &endpoint))) {
                    ma_msgbus_request_t *request = NULL;
                    async_data_t * async_data = (async_data_t *)calloc(1, sizeof(async_data_t));
                    if(async_data) {
                        ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;                
                        (void) ma_client_manager_get_thread_option(client_manager, &thread_option);
                        ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);
				        ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                        async_data->request_type = 1;
                        async_data->request_cb.repo_cb = cb;
                        async_data->cb_data = cb_data;
                        async_data->ma_client = ma_client;
                        if(MA_OK == (rc = ma_msgbus_async_send(endpoint, get_repositories_msg, async_request_cb, async_data, &request))) {					       
                            ma_message_release(get_repositories_msg) ;
                            return MA_OK;
                        } 
                        free(async_data);
                    }
                    else
                        rc = MA_ERROR_OUTOFMEMORY;

                    (void)ma_msgbus_endpoint_release(endpoint);
                }
                (void)ma_message_release(get_repositories_msg) ;
            }
            return rc ;
        }
        return MA_ERROR_INVALID_CLIENT_OBJECT;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_repository_client_async_get_proxy_config(ma_client_t *ma_client, ma_proxy_config_get_cb_t cb, void *cb_data) {
    if(ma_client && cb) {
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;  
        const char *product_id = NULL;
        ma_msgbus_t *msgbus = NULL;            
        ma_client_get_context(ma_client, &ma_context);
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);
        product_id = MA_CONTEXT_GET_PRODUCT_ID(ma_context) ;
        msgbus = MA_CONTEXT_GET_MSGBUS(ma_context);

        if(product_id && msgbus && client_manager) {
            ma_error_t rc = MA_OK ;
            ma_message_t *get_repositories_msg = NULL;
            if(MA_OK == (rc = ma_message_create(&get_repositories_msg))) {
                ma_msgbus_endpoint_t *endpoint = NULL ;
                ma_message_set_property(get_repositories_msg, MA_REPOSITORY_MSG_KEY_TYPE_STR, MA_REPOSITORY_MSG_PROP_VAL_RQST_GET_PROXIES_STR) ;
                ma_message_set_property(get_repositories_msg, MA_REPOSITORY_MSG_PROP_KEY_PRODUCT_ID_STR, product_id) ;
            
                if(MA_OK == (rc = ma_msgbus_endpoint_create(msgbus, MA_REPOSITORY_SERVICE_NAME_STR, NULL, &endpoint))) {
                    ma_msgbus_request_t *request = NULL;
                    async_data_t * async_data = (async_data_t *)calloc(1, sizeof(async_data_t));
                    if(async_data) {
                        ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;                
                        (void) ma_client_manager_get_thread_option(client_manager, &thread_option);
                        ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);
				        ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                        async_data->request_type = 2;
                        async_data->request_cb.proxy_cb = cb;
                        async_data->cb_data = cb_data;
                        async_data->ma_client = ma_client;
                        if(MA_OK == (rc = ma_msgbus_async_send(endpoint, get_repositories_msg, async_request_cb, async_data, &request))) {					       
                            ma_message_release(get_repositories_msg) ;
                            return MA_OK;
                        } 
                        free(async_data);
                    }
                    else
                        rc = MA_ERROR_OUTOFMEMORY;

                    (void)ma_msgbus_endpoint_release(endpoint);
                }
                (void)ma_message_release(get_repositories_msg) ;
            }
            return rc ;
        }
        return MA_ERROR_INVALID_CLIENT_OBJECT;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_repository_client_async_get_system_proxy(ma_client_t *ma_client, const char *url, ma_system_proxy_get_cb_t cb, void *cb_data) {
    if(ma_client && cb && url) {
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;  
        const char *product_id = NULL;
        ma_msgbus_t *msgbus = NULL;            
        ma_client_get_context(ma_client, &ma_context);
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);
        product_id = MA_CONTEXT_GET_PRODUCT_ID(ma_context) ;
        msgbus = MA_CONTEXT_GET_MSGBUS(ma_context);

        if(product_id && msgbus && client_manager) {
            ma_error_t rc = MA_OK ;
            ma_message_t *sys_proxy_msg = NULL;
            if(MA_OK == (rc = ma_message_create(&sys_proxy_msg))) {
                ma_msgbus_endpoint_t *endpoint = NULL ;
                ma_message_set_property(sys_proxy_msg, MA_REPOSITORY_MSG_KEY_TYPE_STR, MA_REPOSITORY_MSG_PROP_VAL_RQST_GET_SYS_PROXIES_STR) ;
                ma_message_set_property(sys_proxy_msg, MA_REPOSITORY_MSG_PROP_VAL_SYS_PROXY_URL_STR, url) ;
				ma_message_set_property(sys_proxy_msg, MA_REPOSITORY_MSG_PROP_KEY_PRODUCT_ID_STR, product_id) ;
            
                if(MA_OK == (rc = ma_msgbus_endpoint_create(msgbus, MA_REPOSITORY_SERVICE_NAME_STR, NULL, &endpoint))) {
                    ma_msgbus_request_t *request = NULL;
                    async_data_t * async_data = (async_data_t *)calloc(1, sizeof(async_data_t));
                    if(async_data) {
                        ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;                
                        (void) ma_client_manager_get_thread_option(client_manager, &thread_option);
                        ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);
				        ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                        async_data->request_type = 3;
                        async_data->request_cb.system_proxy_cb = cb;
                        async_data->cb_data = cb_data;
                        async_data->ma_client = ma_client;
                        if(MA_OK == (rc = ma_msgbus_async_send(endpoint, sys_proxy_msg, async_request_cb, async_data, &request))) {					       
                            ma_message_release(sys_proxy_msg) ;
                            return MA_OK;
                        } 
                        free(async_data);
                    }
                    else
                        rc = MA_ERROR_OUTOFMEMORY;

                    (void)ma_msgbus_endpoint_release(endpoint);
                }
                (void)ma_message_release(sys_proxy_msg) ;
            }
            return rc ;
        }
        return MA_ERROR_INVALID_CLIENT_OBJECT;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_repository_client_get_system_proxy(ma_client_t *ma_client, const char *url, ma_proxy_list_t **sys_proxy_list) {
	ma_error_t rc = MA_ERROR_INVALIDARG ;
    if(ma_client && sys_proxy_list && url) {
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;  
        const char *product_id = NULL;
        ma_msgbus_t *msgbus = NULL;    
		*sys_proxy_list = NULL;
        ma_client_get_context(ma_client, &ma_context);
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);
        product_id = MA_CONTEXT_GET_PRODUCT_ID(ma_context) ;
        msgbus = MA_CONTEXT_GET_MSGBUS(ma_context);
        if(product_id && msgbus && client_manager) {
            ma_message_t *sys_proxy_msg = NULL;
            if(MA_OK == (rc = ma_message_create(&sys_proxy_msg))) {
                ma_msgbus_endpoint_t *endpoint = NULL ;
                ma_message_set_property(sys_proxy_msg, MA_REPOSITORY_MSG_KEY_TYPE_STR, MA_REPOSITORY_MSG_PROP_VAL_RQST_GET_SYS_PROXIES_STR) ;
                ma_message_set_property(sys_proxy_msg, MA_REPOSITORY_MSG_PROP_VAL_SYS_PROXY_URL_STR, url) ;
				ma_message_set_property(sys_proxy_msg, MA_REPOSITORY_MSG_PROP_KEY_PRODUCT_ID_STR, product_id) ;
            
                if(MA_OK == (rc = ma_msgbus_endpoint_create(msgbus, MA_REPOSITORY_SERVICE_NAME_STR, NULL, &endpoint))) {
					ma_message_t *response = NULL;
                    ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;                
                    (void) ma_client_manager_get_thread_option(client_manager, &thread_option);
                    ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);
				    ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                    if(MA_OK == (rc = ma_msgbus_send(endpoint, sys_proxy_msg, &response))) {
						ma_variant_t *res_payload = NULL;            
						if(MA_OK == (rc = ma_message_get_payload(response, &res_payload))) {
							rc = ma_proxy_list_from_variant(res_payload, sys_proxy_list);            
							(void)ma_variant_release(res_payload);
						}
                        ma_message_release(response);
					}
					(void)ma_msgbus_endpoint_release(endpoint);
				}
				(void)ma_message_release(sys_proxy_msg) ;
			}
		}
	}
	return rc;
}


