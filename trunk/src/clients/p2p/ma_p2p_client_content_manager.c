#include "ma_p2p_client_internal.h"

#include "ma/internal/services/p2p/ma_p2p_db_manager.h"
#include "ma/internal/core/msgbus/ma_msgbus_private.h"


#include "ma/ma_message.h"
#include "ma/internal/services/ma_agent_policy_settings_bag.h" 
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/defs/ma_common_services_defs.h"

#include "ma/logger/ma_logger.h"

#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal/utils/filesystem/ma_fs_utils.h"



#include <stdlib.h>
#include <string.h>
#include <stdio.h>

static char *get_str_value_from_array(ma_array_t *arr, size_t index) {
	ma_variant_t *var_value = NULL ;
	ma_buffer_t *buffer = NULL ;
	char *str_value = NULL ; size_t len = 0 ;

	(void)ma_array_get_element_at(arr, index, &var_value) ;
	(void)ma_variant_get_string_buffer(var_value, &buffer) ;
	(void)ma_buffer_get_string(buffer, &str_value, &len) ;

	(void)ma_buffer_release(buffer) ; buffer = NULL ;
	(void)ma_variant_release(var_value) ; var_value = NULL ;

	return str_value ;
}

typedef struct file_content_table_cb_data_s {
	ma_p2p_client_t *p2p_client ;
	ma_p2p_content_request_t *request ;
	char *base_path ;
	ma_msgbus_endpoint_t *p2p_ep ;
} file_content_table_cb_data_t ;

static void file_content_table_cb(char const *table_key, ma_variant_t *table_value, void *cb_args, ma_bool_t *stop_loop) {
	ma_error_t rc = MA_OK ;
	ma_array_t *arr_content = NULL ;
	file_content_table_cb_data_t *cb_data = (file_content_table_cb_data_t *)cb_args ;
	uv_loop_t *loop = ma_event_loop_get_uv_loop(ma_msgbus_get_event_loop(cb_data->p2p_client->msgbus)) ;

	if(MA_OK == (rc = ma_variant_get_array(table_value, &arr_content))) {
		char *source_base_path = NULL, *source_urn = NULL ;
		char sub_directory[MA_MAX_PATH_LEN] = {0} ;
		char file_name[MA_MAX_PATH_LEN] = {0} ;
		char target_file[MA_MAX_PATH_LEN] = {0} ;
		char source_file[MA_MAX_PATH_LEN] = {0} ;
		char *pch = NULL ;
		ma_message_t *msg = NULL ;
		char *is_file_exists = NULL ;

		source_base_path = get_str_value_from_array(arr_content, 1) ;
		source_urn = get_str_value_from_array(arr_content, 2) ;

		pch = strrchr(source_urn, MA_PATH_SEPARATOR) ;
		if(pch) {
			MA_MSC_SELECT(_snprintf, snprintf)(sub_directory, pch-source_urn, "%s", source_urn) ;
			MA_MSC_SELECT(_snprintf, snprintf)(file_name, strlen(pch+1), "%s", pch+1) ;
			ma_fs_make_recursive_dir_as_user(loop, cb_data->base_path, sub_directory, MA_CMNSVC_SID_NAME) ;
		}
		else {
			MA_MSC_SELECT(_snprintf, snprintf)(file_name, strlen(source_urn), "%s",source_urn) ;
		}

		MA_MSC_SELECT(_snprintf, snprintf)(target_file, MA_MAX_PATH_LEN, "%s%c%s%c%s_%s", cb_data->base_path, MA_PATH_SEPARATOR, sub_directory, MA_PATH_SEPARATOR, table_key, file_name) ;
		MA_MSC_SELECT(_snprintf, snprintf)(source_file, MA_MAX_PATH_LEN, "%s%c%s", source_base_path, MA_PATH_SEPARATOR, source_urn) ;



		if(MA_OK == (rc = ma_message_create(&msg))) {
			ma_message_t *response = NULL ;
			(void)ma_message_set_property(msg, MA_P2P_MSG_KEY_TYPE_STR, MA_P2P_MSG_PROP_VAL_RQST_CHECK_CONTENT_EXISTS_STR) ;
			(void)ma_message_set_property(msg, MA_P2P_MSG_KEY_CONTENT_HASH_STR, table_key) ;
			if(MA_OK == (rc = ma_msgbus_send(cb_data->p2p_ep, msg, &response))) {
				(void)ma_message_get_property(response, MA_P2P_MSG_KEY_CONTENT_EXISTS_STR, &is_file_exists) ;

				if(is_file_exists && !strcmp(is_file_exists, "0")) {
					if(cb_data->request->file_op == MA_P2P_FILE_OP_COPY) {
						ma_fs_copy_file_as_user(loop, source_file, target_file, MA_CMNSVC_SID_NAME) ;
					}
					if(cb_data->request->file_op == MA_P2P_FILE_OP_MOVE) {
						ma_fs_move_file_as_user(loop, source_file, target_file, MA_CMNSVC_SID_NAME) ;
					}
				}
				else {
					MA_LOG(cb_data->p2p_client->logger, MA_LOG_SEV_DEBUG, "\"%s\" file with hash(%s) already exists in p2p repo/storage", source_urn, table_key) ;
				}

				(void)ma_message_release(response) ; response = NULL ;
			}
			ma_message_release(msg);
		}
		(void)ma_array_release(arr_content) ;  arr_content = NULL ;
	}
	*stop_loop = MA_FALSE ;
}


static ma_error_t do_file_operation(ma_p2p_client_t *self, ma_p2p_content_request_t *request) {
	ma_error_t rc = MA_OK ;

	/* content format is file */
	if(!request->format) {
		ma_message_t *msg = NULL ;
		ma_msgbus_endpoint_t *p2p_ep = NULL ;

		if(MA_OK == (rc = ma_msgbus_endpoint_create(self->msgbus, MA_P2P_SERVICE_NAME_STR, NULL, &p2p_ep))) 
		{
			(void)ma_msgbus_endpoint_setopt(p2p_ep, MSGBUSOPT_THREADMODEL, self->thread_option) ;
			(void)ma_msgbus_endpoint_setopt(p2p_ep, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC) ;

			if(MA_OK == (rc = ma_message_create(&msg))) {
				ma_message_t *response = NULL ;
				/* TODO should check for the same process ?? */
				(void)ma_message_set_property(msg, MA_P2P_MSG_KEY_TYPE_STR, MA_P2P_MSG_PROP_VAL_RQST_GET_CONTENT_DIR_STR) ;
				if(MA_OK == (rc = ma_msgbus_send(p2p_ep, msg, &response))) {
					char *base_dir = NULL ;
					
					(void)ma_message_get_property(response, MA_P2P_MSG_KEY_CONTENT_DIR_STR, &base_dir) ;
					if(base_dir) {
						file_content_table_cb_data_t *cb_data = (file_content_table_cb_data_t *)calloc(1, sizeof(file_content_table_cb_data_t)) ;

						if(cb_data) {
							cb_data->base_path = base_dir ;
							cb_data->request = request ;
							cb_data->p2p_client = self ;
							cb_data->p2p_ep = p2p_ep ;

							rc = ma_table_foreach(request->content_table, file_content_table_cb, (void *)cb_data) ;
							free(cb_data) ; cb_data = NULL ;
						}
					}
					(void)ma_message_release(response) ; response = NULL ;
				}
				(void)ma_message_release(msg) ; msg = NULL ;
			}
			(void)ma_msgbus_endpoint_release(p2p_ep) ;	p2p_ep = NULL ;
		}
	}

	return rc ;
}

static ma_error_t ma_p2p_client_add_remove_content(ma_p2p_client_t *self, ma_p2p_content_request_t *request, ma_bool_t is_add) {
    ma_error_t rc = MA_OK ;
    ma_message_t *msg = NULL ;
    ma_variant_t *payload = NULL ;
		
	if(!self->logger || !self->msgbus)
		return MA_ERROR_PRECONDITION ;

    ma_p2p_content_request_add_ref(request) ;
        
    MA_LOG(self->logger, MA_LOG_SEV_INFO, "Sending content \"%s\" request", is_add ? "add" : "remove") ;
        
	if(MA_TRUE == is_add) {
		if(MA_OK == (rc = do_file_operation(self, request)))
			request->file_op = (ma_p2p_file_op_t)3 ;
	}

    if(MA_OK == (rc = ma_p2p_content_request_as_variant(request, &payload))) {
		ma_msgbus_endpoint_t *p2p_ep = NULL ;

		if(MA_OK == (rc = ma_msgbus_endpoint_create(self->msgbus, MA_P2P_SERVICE_NAME_STR, NULL, &p2p_ep))) 
		{
			(void)ma_msgbus_endpoint_setopt(p2p_ep, MSGBUSOPT_THREADMODEL, self->thread_option) ;
			(void)ma_msgbus_endpoint_setopt(p2p_ep, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC) ;
			
			if(MA_OK == (rc = ma_message_create(&msg))) {
				(void)ma_message_set_property(msg, MA_P2P_MSG_KEY_TYPE_STR, is_add ? MA_P2P_MSG_PROP_VAL_RQST_ADD_CONTENT_STR : MA_P2P_MSG_PROP_VAL_RQST_REMOVE_CONTENT_STR) ;
				(void)ma_message_set_payload(msg, payload) ;

				if(self->thread_option == MA_MSGBUS_CALLBACK_THREAD_IO) {
					rc = ma_msgbus_send_and_forget(p2p_ep, msg) ;
				}
				else {
					ma_message_t *response = NULL ;
					rc = ma_msgbus_send(p2p_ep, msg, &response) ;
					if(response)	(void)ma_message_release(response) ;
				}
				(void)ma_message_release(msg) ; msg = NULL ;
			}
			else {
				MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Content \"%s\" request message creation failed(%d).", (is_add ? "add" : "remove"), rc ) ;
			}
			(void)ma_msgbus_endpoint_release(p2p_ep);
		}
		else {
			MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Msgbus endpoint creation failed error (%d).", rc ) ;
		}
		(void)ma_variant_release(payload) ;  payload = NULL ;
    }
    else {
        MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Content \"%s\" request payload create failed(%d).",(is_add ? "add" : "remove"), rc ) ;
    }

	if(MA_OK == rc ){
		MA_LOG(self->logger, MA_LOG_SEV_INFO, "Content \"%s\" request succeded.", (is_add ? "add" : "remove") );
	}
	else {
		MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Content \"%s\" request failed(%d).", (is_add ? "add" : "remove"), rc ) ;
	}

    (void)ma_p2p_content_request_release(request) ;
    return rc ;
}

ma_error_t ma_p2p_client_add_content(ma_p2p_client_t *self, ma_p2p_content_request_t *request) {
    if(self && request) {        
        return ma_p2p_client_add_remove_content(self, request, MA_TRUE);        
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_p2p_client_remove_content(ma_p2p_client_t *self, ma_p2p_content_request_t *request) {
    if(self && request) {
        return ma_p2p_client_add_remove_content(self, request, MA_FALSE);        
    }
    return MA_ERROR_INVALIDARG ;
}