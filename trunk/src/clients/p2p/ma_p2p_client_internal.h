#ifndef MA_P2P_CLIENT_INTERNAL_H_INCLUDED
#define MA_P2P_CLIENT_INTERNAL_H_INCLUDED

#include "ma/p2p/ma_p2p_client.h"
#include "ma/internal/clients/p2p/ma_p2p_content_request_internal.h"
#include "ma/internal/defs/ma_p2p_defs.h"
#include "ma/internal/clients/udp/ma_udp_client.h"


#define LOG_FACILITY_NAME "p2p_client"

struct ma_p2p_client_s {
	ma_udp_client_t		*udp_client ;
	ma_msgbus_t			*msgbus ;
	ma_logger_t			*logger ;
	ma_msgbus_callback_thread_options_t thread_option ;
} ;

#endif	/* MA_P2P_CLIENT_INTERNAL_H_INCLUDED */