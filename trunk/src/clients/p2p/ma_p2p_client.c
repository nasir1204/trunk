#include "ma_p2p_client_internal.h"

#include "ma/logger/ma_logger.h"

#include <stdlib.h>

ma_error_t ma_p2p_client_create(ma_udp_client_t *udp_client, ma_p2p_client_t **p2p_client) {
    if(p2p_client && udp_client) {
        ma_error_t rc = MA_ERROR_OUTOFMEMORY ;
        ma_p2p_client_t *self = (ma_p2p_client_t *)calloc(1, sizeof(ma_p2p_client_t)) ;
		
        if(self) {
			self->udp_client = udp_client ;
			(void)ma_udp_client_get_logger(udp_client, &self->logger) ;
			*p2p_client = self ;
			return MA_OK ;
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_p2p_client_set_logger(ma_p2p_client_t *self, ma_logger_t *logger) {
	if(self && logger){
		ma_logger_add_ref(self->logger = logger) ;
		return MA_OK ;
	}
	return MA_ERROR_INVALIDARG ;
}


ma_error_t ma_p2p_client_set_msgbus(ma_p2p_client_t *self, ma_msgbus_t *msgbus) {
	if(self && msgbus){
		self->msgbus = msgbus ;
		return MA_OK ;
	}
	return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_p2p_client_set_thread_option(ma_p2p_client_t *self, ma_msgbus_callback_thread_options_t thread_option) {
	if(self){
		self->thread_option = thread_option ;
		return MA_OK ;
	}
	return MA_ERROR_INVALIDARG ;
}



ma_error_t ma_p2p_client_get_content_filepath(ma_p2p_client_t *self, const char *hash, char **file_path) {
	if(self && hash && file_path) {
		ma_error_t rc = MA_OK ;

		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_p2p_client_release(ma_p2p_client_t *self) {
    if(self) {
		if(self->logger) ma_logger_release(self->logger) ;
		self->logger = NULL ;

		self->udp_client = NULL ;

        free(self) ;
    }
    return MA_OK ;
}