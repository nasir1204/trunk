#include "ma_p2p_client_internal.h"
#include "ma/internal/clients/udp/ma_udp_request.h"
#include "ma/internal/defs/ma_udp_defs.h"
#include "ma/internal/defs/ma_http_server_defs.h"

#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include "ma/internal/ma_macros.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

typedef struct p2p_discovery_callback_data_s {
	char *hash ;
	char *url ;
	void *data ;
}p2p_discovery_callback_data_t ;

typedef struct p2p_service_discovery_callback_data_s {
	ma_bool_t found;
	void	  *data ;	
}p2p_service_discovery_callback_data_t ;

static ma_error_t p2p_discovery_callback_data_create(p2p_discovery_callback_data_t **cb_data) ;
static void p2p_discovery_callback_data_release(p2p_discovery_callback_data_t *cb_data) ;

static ma_error_t p2p_service_discovery_cb(ma_udp_request_t *request, ma_udp_response_t *response, void *user_data, ma_bool_t *stop);
static ma_error_t p2p_discovery_cb(ma_udp_request_t *request, ma_udp_response_t *response, void *user_data, ma_bool_t *stop) ;

ma_error_t ma_p2p_client_discover_service(ma_p2p_client_t *self, const char *multicast_addr, ma_uint32_t discovery_port, ma_bool_t *found) {
	if(self && multicast_addr && discovery_port && found) {
		ma_error_t rc = MA_OK ;
        ma_message_t *msg = NULL ;
		ma_udp_request_t *udp_request = NULL;
		ma_udp_msg_t *udp_msg = NULL ;
		*found = MA_FALSE;

		MA_LOG(self->logger, MA_LOG_SEV_INFO, "Checking P2P services via discovery...") ;

		if(MA_OK == (rc = ma_udp_request_create(self->udp_client, &udp_request))) 
		{
			p2p_service_discovery_callback_data_t *cb_data = (p2p_service_discovery_callback_data_t *)calloc(1, sizeof(p2p_service_discovery_callback_data_t)) ;
			
			if(cb_data) {
				(void)ma_udp_request_set_multicast_addr(udp_request, multicast_addr) ;
				(void)ma_udp_request_set_port(udp_request, discovery_port) ;
				cb_data->data = (void *)self ;
				cb_data->found = MA_FALSE;				
				(void)ma_udp_request_set_callbacks(udp_request, NULL, &p2p_service_discovery_cb, (void *)cb_data) ;
				(void)ma_udp_request_set_timeout(udp_request, 2*1000) ;

				if(MA_OK == (rc = ma_udp_msg_create(&udp_msg))) 
				{
					if(MA_OK == (rc = ma_message_create(&msg))) {
						(void)ma_message_set_property(msg, MA_P2P_MSG_KEY_TYPE_STR, MA_P2P_MSG_PROP_VAL_RQST_SERVICE_DISCOVERY_STR) ;					
						
						(void)ma_udp_msg_set_ma_message(udp_msg, msg) ;						
						(void)ma_udp_request_set_message(udp_request, &udp_msg, 1) ;
						
						if(MA_OK == (rc = ma_udp_request_send(udp_request))) {
							*found = cb_data->found;
						}
						else {
							MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to send udp request, <rc = %d>.", rc) ;
						}

						(void)ma_message_release(msg);
					}
					else {
						MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to send udp request message palyload, <rc = %d>.", rc) ;
					}
				
					(void)ma_udp_msg_release(udp_msg);
				}
				free(cb_data);
			}
			else {
				MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to create cb data");
				rc = MA_ERROR_OUTOFMEMORY;
			}

			(void)ma_udp_request_release(udp_request);
		}
		else {
			MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to create udp request message, <rc = %d>.", rc) ;
		}

		if(MA_OK == rc) {
			MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "p2p service discovery succeeded") ;
		}
		else {
			MA_LOG(self->logger, MA_LOG_SEV_ERROR, "p2p service discovery failed, <rc = %d>", rc) ;			
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_p2p_client_discover_content(ma_p2p_client_t *self, const char *multicast_addr, ma_uint32_t discovery_port, const char *hash, char **url){
	if(self && multicast_addr && discovery_port && hash && url) {
		ma_error_t rc = MA_OK ;
        ma_message_t *msg = NULL ;
		ma_udp_request_t *udp_request = NULL;
		ma_udp_msg_t *udp_msg = NULL ;

		MA_LOG(self->logger, MA_LOG_SEV_INFO, "Checking content via discovery... for hash(%s)", hash) ;

		if(MA_OK == (rc = ma_udp_request_create(self->udp_client, &udp_request))) 
		{
			p2p_discovery_callback_data_t *cb_data = NULL ;

			(void)p2p_discovery_callback_data_create(&cb_data) ;
			(void)ma_udp_request_set_multicast_addr(udp_request, multicast_addr) ;
			(void)ma_udp_request_set_port(udp_request, discovery_port) ;
			cb_data->hash = strdup(hash) ;
			cb_data->data = (void *)self ;
			(void)ma_udp_request_set_callbacks(udp_request, NULL, &p2p_discovery_cb, (void *)cb_data) ;
			(void)ma_udp_request_set_timeout(udp_request, 2*1000) ;

			if(MA_OK == (rc = ma_udp_msg_create(&udp_msg))) 
			{
				if(MA_OK == (rc = ma_message_create(&msg))) {
					(void)ma_message_set_property(msg, MA_P2P_MSG_KEY_TYPE_STR, MA_P2P_MSG_PROP_VAL_RQST_CONTENT_DISCOVERY_STR) ;
					(void)ma_message_set_property(msg, MA_P2P_MSG_KEY_CONTENT_HASH_STR, hash) ;
						
					(void)ma_udp_msg_set_ma_message(udp_msg, msg) ;						
					(void)ma_udp_request_set_message(udp_request, &udp_msg, 1) ;
						
					if(MA_OK == (rc = ma_udp_request_send(udp_request))) {
						if(cb_data->url) {
							*url = strdup(cb_data->url) ;
						}
						else {
							rc = MA_ERROR_P2P_CONTENT_NOT_DISCOVERED ;
						}
					}
					else {
						MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to send udp request, <rc = %d>.", rc) ;
					}

					(void)ma_message_release(msg);
				}
				else {
					MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to send udp request message palyload, <rc = %d>.", rc) ;
				}
				
				(void)ma_udp_msg_release(udp_msg);
			}
			(void)p2p_discovery_callback_data_release(cb_data) ;
			(void)ma_udp_request_release(udp_request);
		}
		else {
			MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to create udp request message, <rc = %d>.", rc) ;
		}

		if(MA_OK == rc) {
			MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "p2p content url(%s) for hash(%s).", *url, hash) ;
		}
		else {
			MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Couldn't discover content for hash(%s), (%d).", hash, rc) ;
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

static ma_bool_t is_ipv6(const char *ip) {
	if(strchr(ip, ':'))	return MA_TRUE ;
	return MA_FALSE ;
}

static ma_bool_t is_ipv6_linklocal(const char *ip) {
	if(!strncmp(ip, "fe80:", strlen("fe80:")) || !strncmp(ip, "FE80:", strlen("FE80:")))	return MA_TRUE ;
	return MA_FALSE ;
}

ma_error_t p2p_service_discovery_cb(ma_udp_request_t *request, ma_udp_response_t *response, void *user_data, ma_bool_t *stop) {
	if(request && response && user_data) {
		p2p_service_discovery_callback_data_t *cb_data = (p2p_service_discovery_callback_data_t *)user_data ;
		ma_p2p_client_t *self = (ma_p2p_client_t *)cb_data->data ;
		ma_error_t rc = MA_OK ;

		char *msg_type = NULL, *is_service_exist = NULL ;
		ma_message_t *msg = NULL ;
		
		*stop = MA_FALSE ;
		
		if(MA_OK == (rc = ma_udp_msg_get_ma_message(response->response, &msg))) {
			(void)ma_message_get_property(msg, MA_P2P_MSG_KEY_TYPE_STR, &msg_type) ;

			if(msg_type && 0 == strcmp(msg_type, MA_P2P_MSG_PROP_VAL_RPLY_SERVICE_DISCOVERY_STR)) {
			
				(void)ma_message_get_property(msg, MA_P2P_MSG_KEY_CHECK_SERVICE_EXISTS_STR, &is_service_exist) ;
				
				/* if there is no property, we can go head with MA_FALSE */
				cb_data->found = is_service_exist ? atoi(is_service_exist) : MA_FALSE;				
				*stop = MA_TRUE ;
			}
			else {
				MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Not a valid response(%s).", msg_type) ;
			}
		}
		else {
			MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Failed to get the message from response, <rc = %d>.") ;
		}

		return MA_OK ;
	}
	return MA_ERROR_INVALIDARG ;
}

ma_error_t p2p_discovery_cb(ma_udp_request_t *request, ma_udp_response_t *response, void *user_data, ma_bool_t *stop) {
	if(request && response && user_data) {
		p2p_discovery_callback_data_t *cb_data = (p2p_discovery_callback_data_t *)user_data ;
		ma_p2p_client_t *self = (ma_p2p_client_t *)cb_data->data ;
		ma_error_t rc = MA_OK ;

		char *msg_type = NULL, *reply_hash = NULL ;
		ma_message_t *msg = NULL ;
		
		*stop = MA_FALSE ;
		
		if(MA_OK == (rc = ma_udp_msg_get_ma_message(response->response, &msg))) {
			(void)ma_message_get_property(msg, MA_P2P_MSG_KEY_TYPE_STR, &msg_type) ;

			if(msg_type && 0 == strcmp(msg_type, MA_P2P_MSG_PROP_VAL_RPLY_CONTENT_DISCOVERY_STR)) {
			
				(void)ma_message_get_property(msg, MA_P2P_MSG_KEY_CONTENT_HASH_STR, &reply_hash) ;

				if(reply_hash && 0 == strcmp(reply_hash, cb_data->hash)) {
					char *str_port = NULL, *p = NULL ;

					(void)ma_message_get_property(msg, MA_P2P_MSG_KEY_CONTENT_URL_PORT_STR, &str_port) ;
				
					if(str_port) {
						ma_temp_buffer_t temp_url = MA_TEMP_BUFFER_INIT ;

						if(is_ipv6(response->peer_addr)) {
							if(is_ipv6_linklocal(response->peer_addr)) {
								ma_temp_buffer_reserve(&temp_url, strlen(MA_P2P_CONTENT_IPV6_URL_FORMAT_WITH_SCOPE_ID_STR) + strlen(response->peer_addr) + strlen("%") + sizeof(ma_uint32_t)+ strlen(str_port) + sizeof(ma_uint32_t) + strlen(reply_hash) + 1) ;
								MA_MSC_SELECT(_snprintf, snprintf)(p = (char *)ma_temp_buffer_get(&temp_url), ma_temp_buffer_capacity(&temp_url), MA_P2P_CONTENT_IPV6_URL_FORMAT_WITH_SCOPE_ID_STR, response->peer_addr, '%', response->receiving_local_nic, atoi(str_port), reply_hash) ;
							}
							else {
								ma_temp_buffer_reserve(&temp_url, strlen(MA_P2P_CONTENT_IPV6_URL_FORMAT_STR) + strlen(response->peer_addr) + sizeof(ma_uint32_t)+ strlen(str_port) + sizeof(ma_uint32_t) + strlen(reply_hash) + 1) ;
								MA_MSC_SELECT(_snprintf, snprintf)(p = (char *)ma_temp_buffer_get(&temp_url), ma_temp_buffer_capacity(&temp_url), MA_P2P_CONTENT_IPV6_URL_FORMAT_STR, response->peer_addr, atoi(str_port), reply_hash) ;
							}
						}
						else {
							ma_temp_buffer_reserve(&temp_url, strlen(MA_P2P_CONTENT_URL_FORMAT_STR) + strlen(response->peer_addr) + sizeof(ma_uint32_t)+ strlen(str_port) + sizeof(ma_uint32_t) + strlen(reply_hash) + 1) ;
							MA_MSC_SELECT(_snprintf, snprintf)(p = (char *)ma_temp_buffer_get(&temp_url), ma_temp_buffer_capacity(&temp_url), MA_P2P_CONTENT_URL_FORMAT_STR, response->peer_addr, atoi(str_port), reply_hash) ;
						}
					
						cb_data->url = strdup(p = (char *)ma_temp_buffer_get(&temp_url)) ;
						*stop = MA_TRUE ;

						ma_temp_buffer_uninit(&temp_url) ;
					}
				}
				else {
					MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Failed to match received hash(%s).", reply_hash) ;
				}
			}
			else {
				MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Not a valid response(%s).", msg_type) ;
			}
		}
		else {
			MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Failed to get the message from response, <rc = %d>.") ;
		}

		return MA_OK ;
	}
	return MA_ERROR_INVALIDARG ;
}

static ma_error_t p2p_discovery_callback_data_create(p2p_discovery_callback_data_t **cb_data) {
	if(cb_data) {
		*cb_data = (p2p_discovery_callback_data_t *)calloc(1, sizeof(p2p_discovery_callback_data_t)) ;
		return MA_OK ;
	}
	return MA_ERROR_INVALIDARG ;
}

static void p2p_discovery_callback_data_release(p2p_discovery_callback_data_t *cb_data) {
	if(cb_data) {
		if(cb_data->hash)	free(cb_data->hash) ;
		cb_data->hash = NULL ;

		if(cb_data->url)	free(cb_data->url) ;
		cb_data->url = NULL ;

		cb_data->data = NULL ;

		free(cb_data) ;
	}
	return ;
}

