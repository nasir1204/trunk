#include "ma/internal/clients/p2p/ma_p2p_content_request_internal.h"
#include "ma/internal/services/p2p/ma_p2p_content.h"

#include <stdlib.h>
#include <string.h>

MA_P2P_API ma_error_t ma_p2p_content_file_request_create(const char *contributor, ma_p2p_file_op_t file_op, ma_p2p_content_request_t **request) {
    if(request && contributor) {
        ma_error_t rc = MA_OK ;
        
        if(MA_OK == (rc = ma_p2p_content_buffer_request_create(contributor, request))) {
            (*request)->file_op = file_op ;
			(*request)->format = MA_FALSE ;
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

MA_P2P_API ma_error_t ma_p2p_content_buffer_request_create(const char *contributor, ma_p2p_content_request_t **request) {
    if(request && contributor) {
        ma_error_t rc = MA_ERROR_OUTOFMEMORY ;
        ma_p2p_content_request_t *tmp = (ma_p2p_content_request_t *)calloc(1, sizeof(ma_p2p_content_request_t)) ;

        if(tmp) {
            tmp->file_op = MA_P2P_FILE_OP_NONE ;
            tmp->content_table = NULL ;
            tmp->ref_count = 1 ;
			tmp->format = MA_TRUE ;
			tmp->contributor = strdup(contributor) ;

            *request = tmp ;
			rc = MA_OK ;
        }
        if(MA_OK != rc) (void)ma_p2p_content_request_release(tmp) ;
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

MA_P2P_API ma_error_t ma_p2p_content_request_add_file(ma_p2p_content_request_t *self, const char *hash, const char *base_path, const char *urn, char *content_type) {
    if(self && hash && base_path && urn && !self->format) {
        ma_variant_t *data = NULL ;
        ma_error_t rc = MA_OK ;
		ma_array_t *arr_content = NULL ;

        if(!self->content_table)
            rc = ma_table_create(&self->content_table) ;

		if(MA_OK == rc && MA_OK == (rc = ma_array_create(&arr_content))) {
			if(MA_OK == (rc = ma_variant_create_from_string(base_path, strlen(base_path), &data))) {
				rc = ma_array_push(arr_content, data) ;
				ma_variant_release(data) ; data = NULL ;
			}
			if(MA_OK == rc && MA_OK == (rc = ma_variant_create_from_string(urn, strlen(urn), &data))) {
				rc = ma_array_push(arr_content, data) ;
				ma_variant_release(data) ; data = NULL ;
			}
			if(MA_OK == rc && MA_OK == (rc = ma_variant_create_from_string(content_type?content_type:"Default", strlen(content_type?content_type:"Default"), &data))) {
				rc = ma_array_push(arr_content, data) ;
				ma_variant_release(data) ; data = NULL ;
			}

			if(MA_OK == rc && MA_OK == (rc = ma_variant_create_from_array(arr_content, &data))) {
				rc = ma_table_add_entry(self->content_table, hash, data) ;
				ma_variant_release(data) ;
			}
			if(MA_OK == (rc = ma_variant_create_from_int32(MA_P2P_CONTENT_FORMAT_FILE, &data))) {
				rc = ma_array_push(arr_content, data) ;
				ma_variant_release(data) ; data = NULL ;
			}			
			ma_array_release(arr_content) ; arr_content = NULL ;
		}
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

MA_P2P_API ma_error_t ma_p2p_content_request_add_buffer(ma_p2p_content_request_t *self, const char *hash, unsigned char *buffer, size_t buffer_size, const char *urn, char *content_type) {
	if(self && hash && buffer && 0 < buffer_size && urn && self->format) {
        ma_variant_t *data = NULL ;
        ma_error_t rc = MA_OK ;
		ma_array_t *arr_content = NULL ;

        if(!self->content_table)
            rc = ma_table_create(&self->content_table) ;

		if(MA_OK == rc && MA_OK == (rc = ma_array_create(&arr_content))) {
			if(MA_OK == (rc = ma_variant_create_from_raw(buffer, buffer_size, &data))) {
				rc = ma_array_push(arr_content, data) ;
				(void)ma_variant_release(data) ; data = NULL ;
			}
			if(MA_OK == rc && MA_OK == (rc = ma_variant_create_from_string(urn, strlen(urn), &data))) {
				rc = ma_array_push(arr_content, data) ;
				(void)ma_variant_release(data) ; data = NULL ;
			}
			if(MA_OK == rc && MA_OK == (rc = ma_variant_create_from_string(content_type?content_type:"Default", strlen(content_type?content_type:"Default"), &data))) {
				rc = ma_array_push(arr_content, data) ;
				(void)ma_variant_release(data) ; data = NULL ;
			}

			if(MA_OK == rc && MA_OK == (rc = ma_variant_create_from_array(arr_content, &data))) {
				rc = ma_table_add_entry(self->content_table, hash, data) ;
				(void)ma_variant_release(data) ;
			}
			if(MA_OK == (rc = ma_variant_create_from_int32(MA_P2P_CONTENT_FORMAT_BUFFER, &data))) {
				rc = ma_array_push(arr_content, data) ;
				ma_variant_release(data) ; data = NULL ;
			}			
			(void)ma_array_release(arr_content) ; arr_content = NULL ;
		}
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

MA_P2P_API ma_error_t ma_p2p_content_request_release(ma_p2p_content_request_t *self) {
    if(self) {
        (void)ma_p2p_content_request_dec_ref(self) ;
        
		if(0 == self->ref_count) {
            if(self->content_table)   (void)ma_table_release(self->content_table) ;
			self->content_table = NULL ;

			if(self->contributor) free(self->contributor) ;
			self->contributor = NULL ;

            free(self) ;
        }
    }
    return MA_OK ;
}

/* TODO need to check the leaks on variant operations */
ma_error_t ma_p2p_content_request_as_variant(ma_p2p_content_request_t *request, ma_variant_t **var_request) {
    if(request && var_request) {
        ma_error_t rc = MA_OK ;
        ma_variant_t *data = NULL ;
        ma_array_t *arr_request = NULL ;

        if(MA_OK == (rc = ma_array_create(&arr_request))) {
            if(MA_OK == (rc = ma_variant_create_from_uint16(request->file_op, &data))) {
                rc = ma_array_push(arr_request, data) ;
                (void)ma_variant_release(data) ; data = NULL ;
            }
            
			if(MA_OK == (rc = ma_variant_create_from_bool(request->format, &data))) {
                rc = ma_array_push(arr_request, data) ;
                (void)ma_variant_release(data) ; data = NULL ;
            }

			if(MA_OK == (rc = ma_variant_create_from_string(request->contributor, strlen(request->contributor), &data))) {
                rc = ma_array_push(arr_request, data) ;
                (void)ma_variant_release(data) ; data = NULL ;
            }

            if(MA_OK == rc && MA_OK == (rc = ma_variant_create_from_table(request->content_table, &data))) {
                rc = ma_array_push(arr_request, data) ;
                (void)ma_variant_release(data) ; data = NULL ;
            }

            if(MA_OK == rc) {
                rc = ma_variant_create_from_array(arr_request, var_request) ;
            }
            (void)ma_array_release(arr_request) ;	arr_request = NULL ;
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

/* TODO need to check the leaks on variant operations */
ma_error_t ma_p2p_content_request_from_variant(ma_variant_t *var_request, ma_p2p_content_request_t **request) {
    if(var_request && request) {
        ma_error_t rc = MA_OK ;
        ma_variant_t *data = NULL ;
        ma_array_t *arr_request = NULL ;

        if(MA_OK == (rc = ma_variant_get_array(var_request, &arr_request))) {
            ma_uint16_t file_op = 0 ;
			ma_bool_t format = MA_FALSE ;
			char *contributor = NULL ;
            
            if(MA_OK == (rc = ma_array_get_element_at(arr_request, 1, &data))) {
                (void)ma_variant_get_uint16(data, &file_op) ;
                (void)ma_variant_release(data) ; data = NULL ;
			}

			if(MA_OK == rc && MA_OK == (rc = ma_array_get_element_at(arr_request, 2, &data))) {
				(void)ma_variant_get_bool(data, &format) ;
				(void)ma_variant_release(data) ; data = NULL ;
			}
			
			if(MA_OK == rc && MA_OK == (rc = ma_array_get_element_at(arr_request, 3, &data))) {
				ma_buffer_t *buffer = NULL ; 
				size_t len = 0 ;
				
				(void)ma_variant_get_string_buffer(data, &buffer) ;
				(void)ma_buffer_get_string(buffer, &contributor, &len) ;

				(void)ma_buffer_release(buffer) ; buffer = NULL ;
				(void)ma_variant_release(data) ; data = NULL ;
			}

			if(format)
                rc = ma_p2p_content_buffer_request_create(contributor, request) ;
            else
                rc = ma_p2p_content_file_request_create(contributor, (ma_p2p_file_op_t)file_op, request) ;

            if(MA_OK == rc) {
                if(MA_OK == (rc = ma_array_get_element_at(arr_request, 4, &data))) {
                    (void)ma_variant_get_table(data, &((*request)->content_table)) ;
                    (void)ma_table_add_ref((*request)->content_table) ;
                    (void)ma_variant_release(data) ; data = NULL;
                }
            }
            (void)ma_array_release(arr_request) ; arr_request = NULL;
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_p2p_content_request_add_ref(ma_p2p_content_request_t *self) {
    if(self) {
        MA_ATOMIC_INCREMENT(self->ref_count) ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_p2p_content_request_dec_ref(ma_p2p_content_request_t *self) {
    if(self) {
        MA_ATOMIC_DECREMENT(self->ref_count) ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}
