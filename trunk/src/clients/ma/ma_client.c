#include "ma/ma_client.h"
#include "ma/internal/clients/ma/ma_client_internal.h"
#include "ma/internal/clients/ma/ma_client_notifier.h"
#include "ma/ma_dispatcher.h"
#include "ma/logger/ma_logger.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/context/ma_context.h"
#include "ma/internal/clients/ma/client/ma_client_handler.h"
#include "ma/internal/core/msgbus/ma_msgbus_internal.h"

#include <stdlib.h>
#include <string.h>

#define LOG_FACILITY_NAME	"ma_client"

static void ma_client_log_cb(ma_log_severity_t severity, const char *module_name, const char *log_message, void *user_data){
	ma_client_t *self = (ma_client_t*)user_data;
	ma_client_log_v(self, severity, module_name, log_message);
};

/*ma client implementation*/
ma_error_t ma_client_create(const char *product_id,  ma_client_t **client) {
    if(product_id && client) {
        ma_error_t rc = MA_OK;
        ma_client_t *self = (ma_client_t *)calloc(1, sizeof(ma_client_t));
        if(self) {			

			if(MA_OK == (rc = ma_dispatcher_initialize(NULL, &self->dispatcher))) {
				self->product_id = MA_MSC_SELECT(_strdup,strdup)(product_id) ;
				self->thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL ;
				self->notify_cb = NULL ;
				self->client_state = MA_CLIENT_NOTIFY_STOP;
				self->client_cb_state = MA_CLIENT_NOTIFY_STOP;
				*client = self;
				return MA_OK;			
			}
			free(self);
            return rc;
        }
        return MA_ERROR_OUTOFMEMORY;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_client_start(ma_client_t *self) {
	if(self) {
		ma_error_t rc = MA_OK;

		if(MA_CLIENT_NOTIFY_STOP == self->client_state){
			rc = MA_ERROR_INVALID_CLIENT_OBJECT;

			if(!self->dispatcher) 
				rc = ma_dispatcher_initialize(NULL, &self->dispatcher);

			if(self->dispatcher) {		
				if(MA_OK == (rc = ma_client_handler_create(self->product_id, &self->client_handler))) {
					/* set the objects which are explicit */
					if(self->msgbus)	(void)ma_client_handler_set_msgbus(self->client_handler, self->msgbus) ;
					(void)ma_client_handler_set_msgbus_passphrase_callback(self->client_handler, self->passphrase_cb,self->passphrase_cb_data);
					(void)ma_client_handler_set_thread_option(self->client_handler, self->thread_option) ;

					if(self->logger)
						(void)ma_client_handler_set_logger(self->client_handler, self->logger) ;
					else
						(void)ma_client_handler_set_logger_callback(self->client_handler, &ma_client_log_cb, self) ;

					if(MA_OK == (rc = ma_client_handler_start(self->client_handler))) {
						if(!self->msgbus)	(void)ma_client_handler_get_msgbus(self->client_handler, &self->msgbus);
						self->client_state = MA_CLIENT_NOTIFY_START;
						MA_CLIENT_LOG(self, MA_LOG_SEV_INFO, "ma client is started.") ;
						return MA_OK ;
					}
					(void)ma_client_handler_release(self->client_handler) ;
				}
				MA_CLIENT_LOG(self, MA_LOG_SEV_ERROR, "falied to start ma client.");					
			}			
		}
		else
			MA_CLIENT_LOG(self, MA_LOG_SEV_INFO, "ma client already started.");					
		return rc;
	}
	return MA_ERROR_INVALIDARG ;
}

void ma_client_dispatcher_deinitialize(ma_client_t *self) {
	if(self){
		if(self->client_handler) 
			(void)ma_client_handler_release(self->client_handler), self->client_handler = NULL;		
		self->msgbus = NULL;
		self->logger = NULL;
		if(self->dispatcher) 
			ma_dispatcher_deinitialize(self->dispatcher), self->dispatcher = NULL;
	}
}

ma_error_t ma_client_stop(ma_client_t *self) {
	if(self) {
		if(MA_CLIENT_NOTIFY_START == self->client_state){
			MA_CLIENT_LOG(self, MA_LOG_SEV_INFO, "stopping ma client.");			
			self->client_state = MA_CLIENT_NOTIFY_STOP;
			return ma_client_handler_stop(self->client_handler);			
		}
		else
			MA_CLIENT_LOG(self, MA_LOG_SEV_ERROR, "ma client already stopped.");			
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_client_release(ma_client_t *self) {
    if(self) {        
		if(self->product_id)	
			free(self->product_id), self->product_id = NULL;
		ma_client_dispatcher_deinitialize(self);

		if(self->notifier) { 
			ma_client_notifier_stop(self->notifier);
			ma_client_notifier_release(self->notifier);
			self->notifier = NULL;
		}
		free(self);
		return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_client_set_logger_callback(ma_client_t *self, ma_client_logmessage_cb_t log_cb, void *user_data){
	if(self) {
		self->log_cb = log_cb;
		self->log_user_data = user_data;
		return MA_OK ;
	}
	return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_client_set_msgbus_passphrase_callback(ma_client_t *self, ma_msgbus_passphrase_provider_callback_t cb, void *cb_data) {
	if(self) {
		self->passphrase_cb = cb;
		self->passphrase_cb_data = cb_data;
		return MA_OK ;
	}
	return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_client_get_msgbus(ma_client_t *self, ma_msgbus_t **msgbus) {
	if(self) {
		*msgbus = self->msgbus;
		return MA_OK ;
	}
	return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_client_set_msgbus(ma_client_t *self, ma_msgbus_t *msgbus) {
	if(self && msgbus) {
		self->msgbus = msgbus;
		return MA_OK ;
	}
	return MA_ERROR_INVALIDARG ;
}


ma_error_t ma_client_set_thread_option(ma_client_t *self, ma_msgbus_callback_thread_options_t thread_option) {
    if(self) {
		self->thread_option = thread_option ;
		return MA_OK ;
	}
	return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_client_get_thread_option(ma_client_t *self, ma_msgbus_callback_thread_options_t *thread_option) {
	if(self) {
		*thread_option = self->thread_option ;
		return MA_OK ;
	}
	return MA_ERROR_INVALIDARG ;
}

static void agent_notification_cb(ma_client_t *self, ma_client_notification_t type, void *cb_data) {
	if(type == self->client_cb_state) return;

	if(MA_CLIENT_NOTIFY_STOP == type){
		MA_CLIENT_LOG(self, MA_LOG_SEV_INFO, "notifying point product to stop client.");
		
		if(self->msgbus)
			(void)ma_msgbus_disable_watcher(self->msgbus);

		self->notify_cb(self, type, cb_data);
		ma_client_dispatcher_deinitialize(self);		
		self->client_cb_state = MA_CLIENT_NOTIFY_STOP;
	}
	else{
		if(!self->dispatcher) 
			(void)ma_dispatcher_initialize(NULL, &self->dispatcher);

		if(self->dispatcher){
			MA_CLIENT_LOG(self, MA_LOG_SEV_INFO, "notifying point product to start client.");
			self->notify_cb(self, type, cb_data);					
			self->client_cb_state = MA_CLIENT_NOTIFY_START;
		}		
		else
			MA_CLIENT_LOG(self, MA_LOG_SEV_ERROR, "failed to initialize dispatcher.");
	}	
}

ma_error_t ma_client_register_notification_callback(ma_client_t *self, ma_client_notification_cb_t callback, void *cb_data) {
	if(self && callback) {
		ma_error_t rc = MA_OK;
		if(!self->notifier){
			if(MA_OK == (rc = ma_client_notifier_create(self, &self->notifier))){
				MA_CLIENT_LOG(self, MA_LOG_SEV_INFO, "registering for ma client notifications.") ;		
				self->client_cb = &agent_notification_cb;
				self->notify_cb = callback;
				self->notify_cb_data = cb_data;		
				ma_client_notifier_start(self->notifier);			
			}
		}
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_client_unregister_notification_callback(ma_client_t *self) {
	if(self){
		if(self->notifier){
			MA_CLIENT_LOG(self, MA_LOG_SEV_INFO, "unregistering for ma client notifications.") ;		
			ma_client_notifier_stop(self->notifier);
			ma_client_notifier_release(self->notifier);
			self->notifier = NULL;
			if(self->notify_cb) self->client_cb(self, MA_CLIENT_NOTIFY_STOP, self->notify_cb_data);
			self->notify_cb = NULL;
			self->notify_cb_data = NULL;
			self->client_cb_state = MA_CLIENT_NOTIFY_STOP;
		}
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

