#include "ma/internal/clients/ma/client/ma_client_handler.h"
#include "ma/internal/utils/context/ma_context.h"
#include "ma/internal/clients/ma/client/ma_client_manager.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/logger/ma_logger.h"

#include <stdlib.h>
#include <string.h>

#define MA_CLIENT_LOGGER_FILTER_STR "*.Trace"

struct ma_client_handler_s {
    ma_context_t                            *context;
    ma_logger_t                             *logger;	
	ma_log_filter_t							*log_filter;
    ma_msgbus_t                             *msgbus;
    ma_client_manager_t                     *client_manager;
    char                                    *product_id;    
    ma_bool_t                               is_msgbus_set;
    ma_msgbus_callback_thread_options_t     thread_option;
    ma_msgbus_consumer_reachability_t       consumer_reach;

	/*ma client logger*/	
	ma_client_log_cb_t						log_cb;
	void									*user_data;

	ma_msgbus_passphrase_provider_callback_t passphrase_cb;
	void									 *passphrase_cb_data;
};



static ma_error_t ma_client_logger_on_log_message(ma_logger_t *logger, ma_log_msg_t *msg){
	ma_client_handler_t *self  = (ma_client_handler_t*)logger->data;
	
	if(self->log_cb) {
		ma_log_severity_t severity = MA_LOG_SEV_INFO;
		ma_buffer_t *module = NULL;
		ma_buffer_t *message = NULL;
		if(MA_OK == ma_log_msg_get_severity(msg, &severity) &&
		   MA_OK == ma_log_msg_get_facility(msg, &module) &&
		   MA_OK == ma_log_msg_get_message(msg, &message)){
				const char *c_module = NULL;
				const char *c_msg = NULL;
				size_t size = 0;

				ma_buffer_get_string(module, &c_module, &size);
				ma_buffer_get_string(message, &c_msg, &size);
				if(c_module && c_msg) self->log_cb(severity, c_module, c_msg, self->user_data);
		}
		if(module) (void)ma_buffer_release(module);
		if(message) (void)ma_buffer_release(message);
	}	
	return MA_OK;
}

static void ma_client_logger_destroy(ma_logger_t *logger){	
	if(logger){
		ma_client_handler_t *self  = (ma_client_handler_t*)logger->data;		
		if(self->logger) free(self->logger), self->logger = NULL;
		if(self->log_filter) ma_log_filter_release(self->log_filter), self->log_filter = NULL;	
	}
}

static ma_logger_methods_t log_methods = {&ma_client_logger_on_log_message, &ma_client_logger_destroy};

static void ma_client_logger_create(ma_client_handler_t *self){
	if(self){
		if(self->logger = (ma_logger_t*)calloc(1, sizeof(ma_logger_t))){
			ma_logger_init(self->logger, (const ma_logger_methods_t *) &log_methods, 1, self);
			(void)ma_generic_log_filter_create(MA_CLIENT_LOGGER_FILTER_STR, &self->log_filter);
			(void)ma_logger_add_filter(self->logger, self->log_filter);		
		}
	}
}


ma_error_t ma_client_handler_create(const char *product_id,  ma_client_handler_t **handler) {
    if(product_id && handler) {
        ma_error_t rc = MA_OK;
        ma_client_handler_t *self = (ma_client_handler_t *)calloc(1,sizeof(ma_client_handler_t));        
        if(self) {  
            self->product_id = MA_MSC_SELECT(_strdup,strdup)(product_id);
            self->thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;
            self->consumer_reach = MSGBUS_CONSUMER_REACH_OUTPROC;			
            *handler = self;
            return rc;            
        }
        return MA_ERROR_OUTOFMEMORY;		
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_client_handler_start(ma_client_handler_t *self) {
    if(self) {
        ma_error_t rc = MA_OK;

        if(MA_OK == (rc =  ma_context_create(&self->context))) {            
            if(!self->is_msgbus_set) {
                if(MA_OK == (rc = ma_msgbus_create(self->product_id, &self->msgbus))) { 
					(void)ma_msgbus_set_passphrase_provider_callback(self->msgbus, self->passphrase_cb, self->passphrase_cb_data);
					(void)ma_msgbus_set_logger(self->msgbus, self->logger);
                    rc = ma_msgbus_start(self->msgbus);                        
                }
            }

            if(MA_OK == rc) {
                if(MA_OK == (rc = ma_client_manager_create(self->msgbus, &self->client_manager))) { 
                    (void) ma_client_manager_set_thread_option(self->client_manager, self->thread_option);
                    (void) ma_client_manager_set_consumer_reachability(self->client_manager, self->consumer_reach);
					(void) ma_client_manager_set_logger(self->client_manager, self->logger);
                    if(MA_OK == (rc = ma_client_manager_start(self->client_manager))) {                        
                        ma_context_add_object_info(self->context, MA_OBJECT_CLIENT_MANAGER_STR, (void *const *)&self->client_manager);
                        ma_context_add_object_info(self->context, MA_OBJECT_MSGBUS_NAME_STR, (void *const *)&self->msgbus);
                        ma_context_add_object_info(self->context, MA_OBJECT_PRODUCT_ID_STR, (void *const *)&self->product_id);
						ma_context_add_object_info(self->context, MA_OBJECT_LOGGER_NAME_STR, (void *const *)&self->logger);
                        return MA_OK;    
                    }
                    (void)ma_client_manager_release(self->client_manager);
                }
                if(!self->is_msgbus_set) (void)ma_msgbus_stop(self->msgbus, MA_TRUE);
            }
            if(!self->is_msgbus_set) (void)ma_msgbus_release(self->msgbus);                
            (void)ma_client_manager_stop(self->client_manager);
            
            (void)ma_context_release(self->context);            
        } 
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_client_handler_stop(ma_client_handler_t *self) {
    if(self) {
        ma_error_t rc = MA_OK;

        rc = ma_client_manager_stop(self->client_manager);
        if(!self->is_msgbus_set && MA_OK == rc) {
			rc = ma_msgbus_stop(self->msgbus, MA_TRUE);
		}
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_client_handler_release(ma_client_handler_t *self) {
    if(self) {
        ma_error_t rc = MA_OK;

        if(self->client_manager) {
		    rc = ma_client_manager_release(self->client_manager);
		}

        if(!self->is_msgbus_set && MA_OK == rc && self->msgbus) {
			rc = ma_msgbus_release(self->msgbus);
		}       
         
        if(self->product_id) free(self->product_id);
        
		if(MA_OK == rc && self->context) {
			rc = ma_context_release(self->context);
		}

		if(self->logger)
			(void)ma_logger_release(self->logger);

		free(self);
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_client_handler_get_context(ma_client_handler_t *self, const ma_context_t **context) {
    if(self && context) {
        *context = self->context;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_client_handler_set_logger_callback(ma_client_handler_t *self, ma_client_log_cb_t log_cb, void *user_data){
    if(self) {
		ma_client_logger_create(self);
		self->log_cb = log_cb;
		self->user_data = user_data;        
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_client_handler_set_msgbus_passphrase_callback(ma_client_handler_t *self, ma_msgbus_passphrase_provider_callback_t cb, void *cb_data) {
	if(self) {
		self->passphrase_cb = cb;
		self->passphrase_cb_data = cb_data;
		(void)ma_msgbus_set_passphrase_provider_callback(self->msgbus, self->passphrase_cb, self->passphrase_cb_data);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}
ma_error_t ma_client_handler_set_logger(ma_client_handler_t *self, ma_logger_t *logger){
    if(self) {
		ma_logger_inc_ref(self->logger = logger);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_client_handler_set_msgbus(ma_client_handler_t *self, ma_msgbus_t *msgbus) {
    if(self && msgbus) {
        if(!self->msgbus) {
            self->is_msgbus_set = MA_TRUE;
            self->msgbus = msgbus;
            return MA_OK;
        }
        return MA_ERROR_INVALID_OPERATION;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_client_handler_get_msgbus(ma_client_handler_t *self, ma_msgbus_t **msgbus) {
    if(self && msgbus) {        
        *msgbus = self->msgbus;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_client_handler_set_thread_option(ma_client_handler_t *self, ma_msgbus_callback_thread_options_t thread_option) {
    if(self) {
        self->thread_option = thread_option;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_client_handler_set_consumer_reachability(ma_client_handler_t *self, ma_msgbus_consumer_reachability_t consumer_reach) {
    if(self) {
        self->consumer_reach = consumer_reach;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}
ma_error_t ma_client_handler_get_thread_option(ma_client_handler_t *self, ma_msgbus_callback_thread_options_t *thread_option) {
    if(self && thread_option) {
        *thread_option = self->thread_option;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_client_handler_get_consumer_reachability(ma_client_handler_t *self, ma_msgbus_consumer_reachability_t *consumer_reach) {
    if(self && consumer_reach) {
        *consumer_reach = self->consumer_reach;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}
