#include "ma/internal/clients/ma/ma_client_event.h"
#include "ma/internal/defs/ma_general_defs.h"
#include "ma/internal/utils/time/ma_time.h"
#include "ma/internal/ma_macros.h"
#include <stdio.h>
#include <string.h>

/*
<?xml version="1.0" encoding="UTF-8"?>
-<UpdateEvents>
	-<MachineInfo>
		<MachineName>TNUWAL-PC</MachineName>
		<AgentGUID>{2E7DD0D2-72E7-420E-8AF8-E83F8830AE17}</AgentGUID>
		<IPAddress>10.213.254.18</IPAddress>
		<OSName>WNT7W</OSName>
		<UserName>tnuwal</UserName>
		<TimeZoneBias>-330</TimeZoneBias>
		<RawMACAddress>d4bed920aa21</RawMACAddress>
	</MachineInfo>

	-<McAfeeCommonUpdater ProductFamily="TVD" ProductVersion="4.8.0" ProductName="McAfee AutoUpdate">
		-<UpdateEvent>
			<EventID>2402</EventID>
			<Severity>4</Severity>
			<GMTTime>2014-04-10T05:34:42</GMTTime>
			<ProductID>EPOAGENT3000</ProductID>
			<Locale>0409</Locale>
			<Type>N/A</Type>
			<Error>-1</Error>
			<Version>N/A</Version>
			<DateTime>N/A</DateTime>
			<InitiatorID>VIRUSCAN8800</InitiatorID>
			<InitiatorType>OnDemand</InitiatorType>
			<SiteName/>
		</UpdateEvent>
	</McAfeeCommonUpdater>
</UpdateEvents>
*/

static ma_error_t create_node(ma_xml_node_t *parent, const char *key, const char *val) {
	ma_xml_node_t *node = NULL;
	ma_error_t rc;
	if(MA_OK == (rc = ma_custom_event_node_create(parent, key, &node)))
		rc = ma_custom_event_node_set_data(node, val);		
	return rc;
}

#define MA_AGENT_EVENT_ROOT_NODE_NAME_STR		"UpdateEvents"
#define MA_AGENT_EVENT_SOFTWARE_NODE_NAME_STR	"McAfeeCommonUpdater"

#define MA_AGENT_EVENT_SOFTWARE_NAME_STR		"ProductName"
#define MA_AGENT_EVENT_SOFTWARE_VERSION_STR		"ProductVersion"
#define MA_AGENT_EVENT_SOFTWARE_FAMILY_STR		"ProductFamily"

#define MA_AGENT_EVENT_INFO_NODE_NAME_STR		"UpdateEvent"
#define MA_AGENT_EVENT_INFO_ID_STR				"EventID"
#define MA_AGENT_EVENT_INFO_SEVERITY_STR		"Severity"

#define MA_AGENT_EVENT_INFO_GMTTIME_STR			"GMTTime"
#define MA_AGENT_EVENT_INFO_PRODUCTID_STR		"ProductID"
#define MA_AGENT_EVENT_INFO_LOCALE_STR			"Locale"
#define MA_AGENT_EVENT_INFO_TYPE_STR			"Type"
#define MA_AGENT_EVENT_INFO_ERROR_STR			"Error"
#define MA_AGENT_EVENT_INFO_VERSION_STR			"Version"
#define MA_AGENT_EVENT_INFO_INITTIATORID_STR	"InitiatorID"
#define MA_AGENT_EVENT_INFO_INITTIATORTYPE_STR	"InitiatorType"
#define MA_AGENT_EVENT_INFO_SITENAME_STR		"SiteName"

ma_error_t ma_client_event_generate(ma_client_t *client, struct ma_event_parameter_s *param){
	if(client && param){
		ma_error_t rc = MA_OK;
		ma_custom_event_t *event = NULL;
		ma_xml_node_t *root_node = NULL;
		
		do{
			ma_xml_node_t *software_node = NULL;
			ma_xml_node_t *update_event = NULL;

			if(MA_OK != (rc = ma_custom_event_create(client, MA_AGENT_EVENT_ROOT_NODE_NAME_STR, &event, &root_node)))
				break;

			if(MA_OK != (rc = ma_custom_event_node_create(root_node, MA_AGENT_EVENT_SOFTWARE_NODE_NAME_STR, &software_node))) 
				break;

			if(MA_OK != (rc = ma_custom_event_node_attribute_set(software_node, MA_AGENT_EVENT_SOFTWARE_NAME_STR, MA_MCAFEE_AGENT_SOFTWARE_STR)))
				break;

			if(MA_OK != (rc = ma_custom_event_node_attribute_set(software_node, MA_AGENT_EVENT_SOFTWARE_VERSION_STR, MA_MCAFEE_AGENT_VERSION_STR)))
				break;

			if(MA_OK != (rc = ma_custom_event_node_attribute_set(software_node, MA_AGENT_EVENT_SOFTWARE_FAMILY_STR, "TVD")))
				break;
													
			if(MA_OK != (rc = ma_custom_event_create_event_node(event, software_node, MA_AGENT_EVENT_INFO_NODE_NAME_STR, param->eventid, param->severity, &update_event)))
				break;

			if(MA_OK != (rc = create_node(update_event, MA_AGENT_EVENT_INFO_PRODUCTID_STR, (param->source_productid ? param->source_productid : MA_SOFTWAREID_GENERAL_STR) ))) 
				break;

			if(MA_OK != (rc = create_node(update_event, MA_AGENT_EVENT_INFO_LOCALE_STR, param->locale))) 
				break;

			if(MA_OK != (rc = create_node(update_event, MA_AGENT_EVENT_INFO_ERROR_STR, param->error))) 
				break;

			if(MA_OK != (rc = create_node(update_event, MA_AGENT_EVENT_INFO_TYPE_STR, (param->type ? param->type :"Unknown") ))) 
				break;
						
			if(MA_OK != (rc = create_node(update_event, MA_AGENT_EVENT_INFO_VERSION_STR, (param->new_version ? param->new_version : "N/A") ))) 
				break;

			if(MA_OK != (rc = create_node(update_event, MA_AGENT_EVENT_INFO_INITTIATORID_STR, (param->initiator_id ? param->initiator_id : "N/A") ))) 
				break;

			if(MA_OK != (rc = create_node(update_event, MA_AGENT_EVENT_INFO_INITTIATORTYPE_STR, (param->initiator_type ? param->initiator_type : "N/A") ))) 
				break;

			if(MA_OK != (rc = create_node(update_event, MA_AGENT_EVENT_INFO_SITENAME_STR, (strlen(param->site_name) ? param->site_name : "N/A") ))) 
				break;

		} while(0);

		if(event){
			if(MA_OK == rc)
				rc = ma_custom_event_send(event);
			(void)ma_custom_event_release(event);
		}
		
		return (MA_ERROR_EVENT_DISABLED == rc ? MA_OK : rc);
	}
	return MA_ERROR_INVALIDARG;
}


ma_error_t ma_event_parameter_init(struct ma_event_parameter_s *param){
	if(param){
		param->eventid = 0;
		param->severity = MA_EVENT_SEVERITY_INFORMATIONAL;
		param->initiator_id = NULL;
		param->initiator_type = NULL;
		param->new_version = NULL;
		param->source_productid = NULL;
		param->type = NULL;
		param->site_name[0] = '\0';
		strcpy(param->locale, "0409");
		strcpy(param->error, "0");
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}