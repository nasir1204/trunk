#include "ma/internal/clients/ma/client/ma_client_manager.h"
#include "ma/internal/utils/datastructures/ma_slist.h"

#include "ma/internal/utils/threading/ma_thread.h"
#include "ma/internal/utils/threading/ma_atomic.h"

#include <stdlib.h>
#include <string.h>

/*TODO - locking - thread safe for adding removing getting */

#define MA_CONTROLLER_SUBSCRIBER_TOPIC_STR "ma.*"

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "ma_client"

typedef struct ma_client_node_s ma_client_node_t;
struct ma_client_node_s {
    /*next link*/
    MA_SLIST_NODE_DEFINE(ma_client_node_t);

    /*name of the client*/
    char		            *client_name ;

    /* client */
    ma_client_base_t        *client;
    
    /*client*/
    ma_mutex_t				client_lock;
};

static ma_client_node_t *ma_client_node_create(const char *name);
static ma_client_base_t *ma_client_node_get_client(ma_client_node_t *node);
static void ma_client_node_set_client(ma_client_node_t *node, ma_client_base_t *client);
static void ma_client_node_release(ma_client_node_t *node);

struct ma_client_manager_s {
    /* list of the clients registered for this session */
    MA_SLIST_DEFINE(clients,ma_client_node_t);

    ma_msgbus_t                             *msgbus;

    ma_logger_t                             *logger;

    ma_msgbus_subscriber_t                  *subscriber;

    ma_msgbus_callback_thread_options_t     thread_option;

    ma_msgbus_consumer_reachability_t       consumer_reach;
        
    ma_mutex_t								client_mgr_lock;

    int										list_reference_count;

    ma_bool_t								list_refresh_needed;
};

static ma_error_t ma_client_manager_start_subscriber(ma_client_manager_t *self, ma_msgbus_callback_thread_options_t thread_option, ma_msgbus_consumer_reachability_t consumer_reach);

ma_error_t ma_client_manager_create(ma_msgbus_t *msgbus, ma_client_manager_t **client_manager) {
    if(msgbus && client_manager) {
        ma_client_manager_t *self = (ma_client_manager_t *)calloc(1,sizeof(ma_client_manager_t));
        if(!self)   return MA_ERROR_OUTOFMEMORY;
        self->msgbus = msgbus;  
        /* Default options */
        self->consumer_reach = MSGBUS_CONSUMER_REACH_OUTPROC;
        self->thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;
        MA_SLIST_INIT(self, clients);
        
        if(ma_mutex_init(&self->client_mgr_lock)) {
            free(self);
            return MA_ERROR_UNEXPECTED;
        }

        *client_manager = self;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_client_manager_start(ma_client_manager_t *self){
    if(self){
        return ma_client_manager_start_subscriber(self, self->thread_option, self->consumer_reach);			
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_client_manager_stop(ma_client_manager_t *self){
    if(self){		
        return ma_msgbus_subscriber_unregister(self->subscriber);
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_client_manager_release(ma_client_manager_t *self) {
    if(self) {
        MA_SLIST_CLEAR(self, clients, ma_client_node_t, ma_client_node_release) ;		
        ma_msgbus_subscriber_release(self->subscriber);
        (void) ma_mutex_destroy(&self->client_mgr_lock);		
        free(self);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_client_manager_set_logger(ma_client_manager_t *self, ma_logger_t *logger) {
    if(self && logger) {
        self->logger = logger;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_client_manager_set_thread_option(ma_client_manager_t *self, ma_msgbus_callback_thread_options_t thread_option) {
    if(self) {
        self->thread_option = thread_option;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_client_manager_set_consumer_reachability(ma_client_manager_t *self, ma_msgbus_consumer_reachability_t consumer_reach) {
    if(self) {
        self->consumer_reach = consumer_reach;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_client_manager_get_thread_option(ma_client_manager_t *self, ma_msgbus_callback_thread_options_t *thread_option) {
    if(self && thread_option) {
        *thread_option = self->thread_option;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_client_manager_get_consumer_reachability(ma_client_manager_t *self, ma_msgbus_consumer_reachability_t *consumer_reach) {
    if(self && consumer_reach) {
        *consumer_reach = self->consumer_reach;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_client_manager_add_client(ma_client_manager_t *self, const char *client_name, ma_client_base_t *client) {
    if(self && client_name && client) {
        ma_client_node_t *client_node = NULL;
        ma_client_base_t *temp_client = NULL;
        
        if(MA_ERROR_CLIENT_NOT_FOUND != ma_client_manager_get_client(self, client_name, &temp_client))
            return MA_ERROR_CLIENT_ALREADY_REGISTERED;

        client_node = ma_client_node_create(client_name);
        if(!client_node) return MA_ERROR_OUTOFMEMORY;
        ma_client_node_set_client(client_node, client);

        if(ma_mutex_lock(&self->client_mgr_lock)) {
            ma_client_node_release(client_node);			
            return MA_ERROR_UNEXPECTED;
        }
        
        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Adding %s to client manager.", client_name);
        MA_SLIST_PUSH_BACK(self, clients, client_node) ;

        (void) ma_mutex_unlock(&self->client_mgr_lock);

        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_client_manager_remove_client(ma_client_manager_t *self, ma_client_base_t *client) {
    if(self && client) {
        ma_client_node_t *client_node = NULL ;

        if(ma_mutex_lock(&self->client_mgr_lock))
            return MA_ERROR_UNEXPECTED;

        MA_SLIST_FOREACH(self, clients, client_node) {
            if(client == client_node->client) {   
                self->list_refresh_needed = MA_TRUE;
                MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Removing %s to client manager.", client_node->client_name);
                ma_client_node_set_client(client_node, NULL);						
                break ;
            }
        }
        (void) ma_mutex_unlock(&self->client_mgr_lock);
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_client_manager_get_client(ma_client_manager_t *self, const char *client_name, ma_client_base_t **client) {
    if(self && client_name && client) {
        ma_client_node_t *client_node = NULL ;
        
        if(ma_mutex_lock(&self->client_mgr_lock))
            return MA_ERROR_UNEXPECTED;

        MA_SLIST_FOREACH(self, clients, client_node) {
            if(client_node->client && 0 == strcmp(client_node->client_name, client_name)) {
                *client = client_node->client;
                (void) ma_mutex_unlock(&self->client_mgr_lock);
                return MA_OK;
            }
        }
        (void) ma_mutex_unlock(&self->client_mgr_lock);
        return MA_ERROR_CLIENT_NOT_FOUND ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_bool_t ma_client_manager_has_client(ma_client_manager_t *self, const char *client_name) {
    if(self && client_name) {
        ma_client_node_t *client_node = NULL ;
        if(ma_mutex_lock(&self->client_mgr_lock))
            return MA_ERROR_UNEXPECTED;

        MA_SLIST_FOREACH(self, clients, client_node) {
            if(client_node->client && 0 == strcmp(client_node->client_name, client_name)){
                (void) ma_mutex_unlock(&self->client_mgr_lock);
                return MA_TRUE;
            }
        }
        (void) ma_mutex_unlock(&self->client_mgr_lock);
    }
    return MA_FALSE ;
}

static ma_bool_t ma_client_manager_list_lock(ma_client_manager_t *self){
    if(self) {
        ma_mutex_lock(&self->client_mgr_lock);
        ++self->list_reference_count;
        (void) ma_mutex_unlock(&self->client_mgr_lock);	
        return MA_TRUE;
    }
    return MA_FALSE;
}

static void ma_client_manager_list_unlock(ma_client_manager_t *self){
    if(self) {
        ma_mutex_lock(&self->client_mgr_lock);		
        --self->list_reference_count;
        (void) ma_mutex_unlock(&self->client_mgr_lock);
    }
}

static void ma_client_manager_list_refresh(ma_client_manager_t *self){
    ma_mutex_lock(&self->client_mgr_lock);		
    if(0 == self->list_reference_count && self->list_refresh_needed){
        ma_bool_t refresh_on = MA_TRUE;

        while(refresh_on){
            ma_client_node_t *client_node = NULL ;
            refresh_on = MA_FALSE;

            MA_SLIST_FOREACH(self, clients, client_node) {
                if(NULL == client_node->client) {
                    MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Removed %s to client manager.", client_node->client_name);
                    MA_SLIST_REMOVE_NODE(self, clients, client_node, ma_client_node_t) ;
                    ma_client_node_release(client_node);
                    refresh_on = MA_TRUE;
                    break;
                }
            }	
        }

        self->list_refresh_needed = MA_FALSE;
    }	
    (void) ma_mutex_unlock(&self->client_mgr_lock);
}

static ma_error_t ma_client_manager_subscriber_cb(const char *topic, ma_message_t *payload, void *cb_data) {
    ma_client_manager_t *self = (ma_client_manager_t *)cb_data;
    ma_client_node_t *client_node = NULL ;

    ma_client_manager_list_refresh(self);
    ma_client_manager_list_lock(self);

    MA_SLIST_FOREACH(self, clients, client_node) {
        ma_client_base_t *client = ma_client_node_get_client(client_node);
        if(client)
            ma_client_base_on_message(client,topic, payload);
    }

    ma_client_manager_list_unlock(self);	
    return MA_OK;
}

static ma_error_t ma_client_manager_start_subscriber(ma_client_manager_t *self, ma_msgbus_callback_thread_options_t thread_option, ma_msgbus_consumer_reachability_t consumer_reach) {
    ma_error_t rc = MA_OK;    
    if(MA_OK == (rc = ma_msgbus_subscriber_create(self->msgbus, &self->subscriber))) {
        if(MA_OK == (rc = ma_msgbus_subscriber_setopt(self->subscriber,MSGBUSOPT_THREADMODEL, thread_option))) {
            if(MA_OK == (rc = ma_msgbus_subscriber_setopt(self->subscriber,MSGBUSOPT_REACH, consumer_reach))) {
                if(MA_OK == (rc = ma_msgbus_subscriber_register(self->subscriber, MA_CONTROLLER_SUBSCRIBER_TOPIC_STR, &ma_client_manager_subscriber_cb, self))) {
                    return MA_OK;
                }
            }
        }
        ma_msgbus_subscriber_release(self->subscriber), self->subscriber = NULL;
    }
    return rc;
}


static ma_client_node_t *ma_client_node_create(const char *name){
    if(name){
        ma_client_node_t *client_node = (ma_client_node_t *)calloc(1,sizeof(ma_client_node_t));
        if(client_node) {
            if(0 == ma_mutex_init(&client_node->client_lock)){
                client_node->client_name = strdup(name);
                return client_node;
            }
            free(client_node);
        }
    }
    return NULL;
}

static ma_client_base_t *ma_client_node_get_client(ma_client_node_t *node){
    ma_client_base_t *client = NULL;
    if(node){	
        ma_mutex_lock(&node->client_lock);
        client = node->client;
        ma_mutex_unlock(&node->client_lock);		
    }
    return client;
}

static void ma_client_node_set_client(ma_client_node_t *node, ma_client_base_t *client){
    if(node){
        ma_mutex_lock(&node->client_lock);
        node->client = client;
        ma_mutex_unlock(&node->client_lock);	
    }
}

static void ma_client_node_release(ma_client_node_t *node){
    if(node){
        if(node->client_name) free(node->client_name);
        ma_mutex_destroy(&node->client_lock);			
        free(node);		
    }
}
