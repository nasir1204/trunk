#include "ma_config_notifier.h"
#include "ma/internal/defs/ma_general_defs.h"

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <unistd.h>

#define LOG_FACILITY_NAME                                       "ma_client"

#ifdef MACX
#include <notify.h>
#include <fcntl.h>
#include <sys/fcntl.h>
#include <sys/event.h>
#endif

#ifdef LINUX

#include <unistd.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <errno.h>


#ifndef __NR_inotify_init
# if defined(__x86_64__)
#  define __NR_inotify_init 253
# elif defined(__i386__)
#  define __NR_inotify_init 291
# elif defined(__arm__)
#  define __NR_inotify_init (UV_SYSCALL_BASE + 316)
# endif
#endif 

#ifndef __NR_inotify_init1
# if defined(__x86_64__)
#  define __NR_inotify_init1 294
# elif defined(__i386__)
#  define __NR_inotify_init1 332
# elif defined(__arm__)
#  define __NR_inotify_init1 (UV_SYSCALL_BASE + 360)
# endif
#endif 

#ifndef __NR_inotify_add_watch
# if defined(__x86_64__)
#  define __NR_inotify_add_watch 254
# elif defined(__i386__)
#  define __NR_inotify_add_watch 292
# elif defined(__arm__)
#  define __NR_inotify_add_watch (UV_SYSCALL_BASE + 317)
# endif
#endif 

#ifndef __NR_inotify_rm_watch
# if defined(__x86_64__)
#  define __NR_inotify_rm_watch 255
# elif defined(__i386__)
#  define __NR_inotify_rm_watch 293
# elif defined(__arm__)
#  define __NR_inotify_rm_watch (UV_SYSCALL_BASE + 318)
# endif
#endif 

int ma_inotify_init(void) {
#if defined(__NR_inotify_init)
  return syscall(__NR_inotify_init);
#else
  return errno = ENOSYS, -1;
#endif
}

int ma_inotify_add_watch(int fd, const char* path, uint32_t mask) {
#if defined(__NR_inotify_add_watch)
  return syscall(__NR_inotify_add_watch, fd, path, mask);
#else
  return errno = ENOSYS, -1;
#endif
}

int ma_inotify_rm_watch(int fd, int32_t wd) {
#if defined(__NR_inotify_rm_watch)
  return syscall(__NR_inotify_rm_watch, fd, wd);
#else
  return errno = ENOSYS, -1;
#endif
}

#include <sys/inotify.h>
#define EVENT_SIZE  ( sizeof (struct inotify_event) )
#define BUF_LEN     ( 1024 * ( EVENT_SIZE + 16 ) )
#endif

struct ma_config_notifier_s{
	ma_client_t					*client;
	int							shutdownfds[2];	
	unsigned long		 		poll_timeout;
	ma_config_notifier_cb_t		cb;
	void						*user_data;
};

ma_error_t ma_config_notifier_create(ma_client_t *client, ma_config_notifier_t **self){
	if(client && self){
		ma_config_notifier_t *config_n = (ma_config_notifier_t*) calloc(1, sizeof(ma_config_notifier_t));
		if(config_n){
			config_n->client = client;
			config_n->shutdownfds[0] = -1;
			config_n->shutdownfds[1] = -1;
			*self = config_n;
			return MA_OK;
		}
		return MA_ERROR_INVALIDARG;
	}
	return MA_ERROR_INVALIDARG;
}

void ma_config_notifier_set_poll_timeout(ma_config_notifier_t *self, unsigned long pto){
	if(self) self->poll_timeout = pto/1000;	
}

#ifdef MACX
ma_error_t ma_config_notifier_start(ma_config_notifier_t *self, ma_config_notifier_cb_t cb, void *cb_data){
	if(self && cb && cb_data){		
		int kqfd = -1;
		int inotify_fd;
		ma_error_t rc = MA_ERROR_UNEXPECTED;

		self->cb = cb;
		self->user_data = cb_data;

		if ((kqfd = kqueue()) > 0){

			if ((inotify_fd = open(MA_CONFIGURATION_REGISTRY_APPLICATIONS_PATH_STR, O_EVTONLY)) > 0)	{

				if (0 == pipe(self->shutdownfds)){
					
					ma_bool_t keep_running = MA_TRUE;

					struct kevent changelist[2];
					struct kevent evlist[2];

					int vnode_events = NOTE_DELETE |  NOTE_WRITE | NOTE_EXTEND | NOTE_ATTRIB | NOTE_LINK | NOTE_RENAME | NOTE_REVOKE;
					EV_SET(&changelist[0], inotify_fd, EVFILT_VNODE, EV_ADD | EV_CLEAR  | EV_ENABLE, vnode_events, 0,0);
										
					EV_SET(&changelist[1], self->shutdownfds[0], EVFILT_READ, EV_ADD | EV_CLEAR | EV_ENABLE, 0, 0, 0);
					rc = MA_OK;
				        	
					while(keep_running){
						int ret = -1;

						if ((ret = kevent(kqfd, changelist, 2, evlist, 2, NULL)) == -1){	
							MA_CLIENT_LOG(self->client, MA_LOG_SEV_ERROR, "kevent failures[1].");		
							keep_running = MA_FALSE;
						}
						MA_CLIENT_LOG(self->client, MA_LOG_SEV_DEBUG, "client detect ma configuration change.");

						if(ret != 0){

							if(evlist[0].ident == self->shutdownfds[0] || evlist[1].ident == self->shutdownfds[0] ){
								MA_CLIENT_LOG(self->client, MA_LOG_SEV_INFO, "ma config monitor stop received.");
								keep_running = MA_FALSE;
							}
							else{
								MA_CLIENT_LOG(self->client, MA_LOG_SEV_INFO, "ma config monitor detect ma configuration modification, rescan the configuration.");								
								self->cb(self->user_data);
							}                
						}
						else{
							MA_CLIENT_LOG(self->client, MA_LOG_SEV_ERROR, "Kevent failures [2].");
							keep_running = MA_FALSE;
						}
					}
										
				}
				else
					MA_CLIENT_LOG(self->client, MA_LOG_SEV_ERROR, "failed to create ma config monitor shutdown channel.");						

				close(inotify_fd);					
			}
			else				
				MA_CLIENT_LOG(self->client, MA_LOG_SEV_ERROR, "failed to setup ma config monitor.");				
			close(kqfd);					
		}
		else
			MA_CLIENT_LOG(self->client, MA_LOG_SEV_ERROR, "kqueue failed, failed to setup ma config monitor.");										
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

#else

#ifdef LINUX
ma_error_t ma_config_notifier_start(ma_config_notifier_t *self, ma_config_notifier_cb_t cb, void *cb_data){
	if(self && cb && cb_data){
		int inotify_fd;
		int inotify_wd;
		ma_bool_t keep_running = MA_TRUE;
		self->cb = cb;
		self->user_data = cb_data;

		inotify_fd = ma_inotify_init();

		if(inotify_fd > 0) {
			inotify_wd = ma_inotify_add_watch(inotify_fd, MA_CONFIGURATION_REGISTRY_APPLICATIONS_PATH_STR, IN_MODIFY | IN_CREATE | IN_DELETE );
			if(inotify_wd > 0){
				MA_CLIENT_LOG(self->client, MA_LOG_SEV_INFO, "inotify is supported on this platform.");
			}
			else {
				MA_CLIENT_LOG(self->client, MA_LOG_SEV_ERROR, "failed to setup ma config monitor.");
				close(inotify_fd);
				return MA_ERROR_UNEXPECTED;
			}
		}
		else{
		   MA_CLIENT_LOG(self->client, MA_LOG_SEV_INFO, "inotify is not supported on this platform, switch to scheduled ma configuration check.");
		   inotify_fd = 0;
		}

		if(0 == pipe(self->shutdownfds)){
			keep_running = MA_TRUE;
			while(keep_running){

				fd_set fdset;
				struct timeval tv = {3600,0};
				int max_fds = 0;
				int result = 0;
				FD_ZERO(&fdset);

				if(inotify_fd){
					max_fds = inotify_fd;
					FD_SET(inotify_fd, &fdset);
				}
				else{
					/*no inotify so default polling*/
					tv.tv_sec = self->poll_timeout;
				}

				FD_SET(self->shutdownfds[0], &fdset);
				if(self->shutdownfds[0] > max_fds)
					max_fds = self->shutdownfds[0];        

				result = select( max_fds + 1, &fdset, NULL, NULL, &tv);

				if(-1 != result){
					if(0 != result && FD_ISSET(self->shutdownfds[0], &fdset)){
						MA_CLIENT_LOG(self->client, MA_LOG_SEV_INFO, "ma config monitor stop received.");
						keep_running = MA_FALSE;                
					}
					else if(inotify_fd && 0 != result && FD_ISSET(inotify_fd, &fdset)){
						int length = 1;
						char buffer[BUF_LEN];
						MA_CLIENT_LOG(self->client, MA_LOG_SEV_INFO, "ma config monitor detect ma configuration modification, rescan the configuration.");				
						length = read( inotify_fd, buffer, BUF_LEN );					
						self->cb(self->user_data);
					}
					else{
						MA_CLIENT_LOG(self->client, MA_LOG_SEV_INFO, "ma config monitor scheduled ma configuration check.");									
						self->cb(self->user_data);
					}
				}
				else{
					keep_running = MA_FALSE;
					MA_CLIENT_LOG(self->client, MA_LOG_SEV_ERROR, "ma config monitor unexpected.");							
				}
				FD_ZERO(&fdset);
			}
		}

		if(inotify_fd){
			(void)ma_inotify_rm_watch(inotify_fd, inotify_wd);
			(void)close(inotify_fd);
		}
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}
#else
ma_error_t ma_config_notifier_start(ma_config_notifier_t *self, ma_config_notifier_cb_t cb, void *cb_data){
	if(self && cb && cb_data){
		ma_bool_t keep_running = MA_TRUE;
		self->cb = cb;
		self->user_data = cb_data;

		if (0 == pipe(self->shutdownfds)){
			keep_running = MA_TRUE;
			while(keep_running){
			
				fd_set fdset;
				struct timeval tv = {self->poll_timeout,0};
				int result = 0;
				FD_ZERO(&fdset);

				FD_SET(self->shutdownfds[0], &fdset);
				result = select( self->shutdownfds[0] + 1, &fdset, NULL, NULL, &tv);

				if(-1 != result){
					if(0 != result && FD_ISSET(self->shutdownfds[0], &fdset)){
						MA_CLIENT_LOG(self->client, MA_LOG_SEV_INFO, "ma config monitor stop received.");
						keep_running = MA_FALSE;                
					}
					else{
						MA_CLIENT_LOG(self->client, MA_LOG_SEV_INFO, "ma config monitor scheduled ma configuration check.");									
						self->cb(self->user_data);
					}
				}
				else{
					keep_running = MA_FALSE;
					MA_CLIENT_LOG(self->client, MA_LOG_SEV_ERROR, "ma config monitor unexpected.");							
				}
				FD_ZERO(&fdset);
			}
		}
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}
#endif
#endif

ma_error_t ma_config_notifier_stop(ma_config_notifier_t *self){
	if(self){
		if(-1 != self->shutdownfds[1]){
			write(self->shutdownfds[1], "s", 1);
		}
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_config_notifier_release(ma_config_notifier_t *self){
	if(self){
		if(-1 != self->shutdownfds[0]) close(self->shutdownfds[0]), self->shutdownfds[0] = -1;
        if(-1 != self->shutdownfds[1]) close(self->shutdownfds[1]), self->shutdownfds[1] = -1;
		free(self);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

