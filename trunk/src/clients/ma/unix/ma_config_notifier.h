#ifndef MA_CONFIG_NOTIFIER_H_INCLUDED
#define MA_CONFIG_NOTIFIER_H_INCLUDED

#include "ma/internal/clients/ma/ma_client_notifier.h"

MA_CPP(extern "C" {)

typedef struct ma_config_notifier_s ma_config_notifier_t;

typedef void (*ma_config_notifier_cb_t) (void *cb_data);

ma_error_t ma_config_notifier_create(ma_client_t *client, ma_config_notifier_t **self);

ma_error_t ma_config_notifier_start(ma_config_notifier_t *self, ma_config_notifier_cb_t cb, void *cb_data);

ma_bool_t ma_config_notifier_is_running(ma_config_notifier_t *self);

void ma_config_notifier_set_poll_timeout(ma_config_notifier_t *self, unsigned long pto);

ma_error_t ma_config_notifier_stop(ma_config_notifier_t *self);

ma_error_t ma_config_notifier_release(ma_config_notifier_t *self);

MA_CPP(})

#endif
