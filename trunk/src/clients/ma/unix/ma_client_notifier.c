#include "ma/internal/clients/ma/ma_client_notifier.h"
#include "ma_config_notifier.h"

#include "ma/internal/defs/ma_general_defs.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/time.h>
#include <pthread.h>


#define LOG_FACILITY_NAME					"ma_client"
#define WAIT_TIME_SECONDS					5

struct ma_client_notifier_s {
	ma_client_t			 *client;
	ma_bool_t			 is_running;	
	char				 msgbus_version[MA_MSGBUS_VERSION_LENGTH];
	pthread_t			 poll_thread;
	pthread_mutex_t		 poll_thread_mutex;
	pthread_cond_t		 poll_thread_cond;
	ma_config_notifier_t *config_notifier;
} ; 

ma_error_t ma_client_notifier_create(ma_client_t *client, ma_client_notifier_t **notifier) {
	if(client && notifier) {
		ma_error_t rc = MA_ERROR_OUTOFMEMORY;

		ma_client_notifier_t *self = (ma_client_notifier_t *)calloc(1, sizeof(ma_client_notifier_t)) ;
		if(self){
			rc = MA_OK;
			if(MA_OK == (rc = ma_config_notifier_create(client, &self->config_notifier))){
				pthread_mutex_init(&self->poll_thread_mutex,NULL);
				pthread_cond_init(&self->poll_thread_cond, NULL);

				self->is_running = MA_FALSE;
				self->client = client;
				ma_config_notifier_set_poll_timeout(self->config_notifier, MA_CLIENT_CONNECTION_CHECK_TIMEOUT);
				*notifier = self ;
				return MA_OK;
			}
			free(self);
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_client_notifier_release(ma_client_notifier_t *self) {
	if(self) {
		if(self->config_notifier) ma_config_notifier_release(self->config_notifier);
		pthread_mutex_destroy(&self->poll_thread_mutex);
		pthread_cond_destroy(&self->poll_thread_cond);
		free(self) ;
	}
	return MA_OK ;
}

static char *r_strip(char *start){
    char *end = start + strlen(start);
    while (end > start && isspace((unsigned char)(*--end)))
        *end = '\0';
    return start;
}

static char *l_skip(const char *start){
    while (*start && isspace((unsigned char)(*start)))
        start++;
    return (char*)start;
}

static char *find_char(const char *start, char c){    
    while (*start && *start != c && *start != '\n')
        start++;    
    return (char*)start;
}

static char *find_line_end(const char *start){    
    while (*start && *start != '\n')
        start++;    
    return (char*)start;
}

static char *get_key(char *buffer, size_t bsize, const char *key){	
	char *value = NULL;
	FILE *file = NULL;
	memset(buffer, '\0', bsize);

	if(NULL != (file = fopen(MA_CONFIGURATION_AGENT_INFO_FILE_PATH_STR, "r"))){
		fread(buffer, 1, bsize-1, file);
		char *key_start = NULL;	
		if(strstr(buffer, MA_CONFIGURATION_KEY_AGENT_MODE_INT)){
			if(NULL != (key_start = strstr(buffer, key))){		
				char *value_start = find_char(key_start, '=');
				if(*value_start == '='){
					char *value_end = find_line_end(key_start);
					value_start++;
				
					if(value_start && value_end && (value_end - value_start) > 0){					
						*value_end = '\0';
						value_start = l_skip(value_start);
						value_start = r_strip(value_start);
						value = value_start;
					}		
				}
			}
		}
		fclose(file);
	}
	return value;
}

static void notify_client(ma_client_notifier_t *self){
	 if(self){
		char file_buffer[2048] = {0};
		char *msgbus_version = NULL;
		
		MA_CLIENT_LOG(self->client, MA_LOG_SEV_DEBUG, "check agent installation status.");

		msgbus_version = get_key(file_buffer, sizeof(file_buffer), MA_CONFIGURATION_KEY_MSGBUS_VERSION_STR);
		
		if(msgbus_version){

			if(strcmp(self->msgbus_version, msgbus_version)){			

				if(self->msgbus_version[0])
					MA_CLIENT_LOG(self->client, MA_LOG_SEV_INFO, "agent is upgraded, msgbus version %s", msgbus_version);
				else
					MA_CLIENT_LOG(self->client, MA_LOG_SEV_INFO, "agent is installed, msgbus version %s", msgbus_version);

				memset(self->msgbus_version, 0, sizeof(self->msgbus_version));
				strncpy(self->msgbus_version, msgbus_version, sizeof(self->msgbus_version)-1);
				self->client->client_cb(self->client, MA_CLIENT_NOTIFY_STOP, self->client->notify_cb_data);											
				self->client->client_cb(self->client, MA_CLIENT_NOTIFY_START, self->client->notify_cb_data);	
				ma_config_notifier_set_poll_timeout(self->config_notifier, MA_CLIENT_CONNECTION_VERIFY_TIMEOUT);
			}	
		}
		else{
			self->msgbus_version[0] = '\0';
			MA_CLIENT_LOG(self->client, MA_LOG_SEV_INFO, "no msgbus version, msgbus is not ready");
			self->client->client_cb(self->client, MA_CLIENT_NOTIFY_STOP, self->client->notify_cb_data);					
			ma_config_notifier_set_poll_timeout(self->config_notifier, MA_CLIENT_CONNECTION_CHECK_TIMEOUT);
		}		
	 }
 }

 static void notify_client_at_start(ma_client_notifier_t *self){
	 if(self){
		char file_buffer[2048] = {0};
		char *msgbus_version = NULL;
		
		MA_CLIENT_LOG(self->client, MA_LOG_SEV_DEBUG, "check agent installation status.");
		
		msgbus_version = get_key(file_buffer, sizeof(file_buffer), MA_CONFIGURATION_KEY_MSGBUS_VERSION_STR);
		
		if(msgbus_version){
			MA_CLIENT_LOG(self->client, MA_LOG_SEV_INFO, "agent is installed, msgbus version %s", msgbus_version);
			memset(self->msgbus_version, 0, sizeof(self->msgbus_version));
			strncpy(self->msgbus_version, msgbus_version, sizeof(self->msgbus_version)-1);
			self->client->client_cb(self->client, MA_CLIENT_NOTIFY_START, self->client->notify_cb_data);		
			ma_config_notifier_set_poll_timeout(self->config_notifier, MA_CLIENT_CONNECTION_VERIFY_TIMEOUT);
		}
		else{
			self->msgbus_version[0] = '\0';
			MA_CLIENT_LOG(self->client, MA_LOG_SEV_INFO, "no msgbus version, msgbus is not ready");			
		}		
	 }
 }
 
static MA_INLINE void  ma_client_notifier_timed_wait(ma_client_notifier_t *self, unsigned long delay){
	struct timespec   ts;
	struct timeval    tp;
	gettimeofday(&tp, NULL);
    
    /* Convert from timeval to timespec */
    ts.tv_sec  = tp.tv_sec;
    ts.tv_nsec = tp.tv_usec * 1000;
    ts.tv_sec += delay;
	pthread_cond_timedwait(&self->poll_thread_cond, &self->poll_thread_mutex, &ts);				
}

static MA_INLINE void  ma_client_notifier_signal(ma_client_notifier_t *self){
	pthread_mutex_lock(&self->poll_thread_mutex);
	pthread_cond_signal(&self->poll_thread_cond);	
	pthread_mutex_unlock(&self->poll_thread_mutex);
}

static void *ma_client_notifier_thread( void *p){
	ma_client_notifier_t *self = (ma_client_notifier_t*)p;
	if(self){	      
		MA_CLIENT_LOG(self->client, MA_LOG_SEV_INFO, "starting ma client notifier thread.");
		/*start signal*/
		self->is_running = MA_TRUE;		
		ma_client_notifier_signal(self);
		
		ma_config_notifier_start(self->config_notifier, &notify_client, self);	
	
		/*stopped signal*/
		self->is_running = MA_FALSE;
		ma_client_notifier_signal(self);
		MA_CLIENT_LOG(self->client, MA_LOG_SEV_INFO, "ma client notifier thread existing...");
	}
	return NULL;
}

ma_error_t ma_client_notifier_start(ma_client_notifier_t *self) {
	if(self){
		ma_error_t rc = MA_OK;
		if(!self->is_running){
			notify_client_at_start(self);
			MA_CLIENT_LOG(self->client, MA_LOG_SEV_INFO, "creating ma client notifier thread.");

			pthread_mutex_lock(&self->poll_thread_mutex);
			pthread_create(&self->poll_thread, NULL, ma_client_notifier_thread, self);				
			ma_client_notifier_timed_wait(self, WAIT_TIME_SECONDS);
			pthread_mutex_unlock(&self->poll_thread_mutex);

			rc = (self->is_running ? MA_OK : MA_ERROR_APIFAILED);
		}
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_client_notifier_stop(ma_client_notifier_t *self) {
	if(self){
		ma_error_t rc = MA_OK;
		if(self->is_running){		
			MA_CLIENT_LOG(self->client, MA_LOG_SEV_INFO, "stopping ma client notifier thread.");
			pthread_mutex_lock(&self->poll_thread_mutex);
			ma_config_notifier_stop(self->config_notifier);			
			ma_client_notifier_timed_wait(self, (MA_CLIENT_CONNECTION_VERIFY_TIMEOUT/1000 + WAIT_TIME_SECONDS) );
			pthread_mutex_unlock(&self->poll_thread_mutex);
			rc = (self->is_running ? MA_ERROR_APIFAILED : MA_OK);
		}
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}


