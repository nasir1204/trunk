#include "ma/internal/clients/ma/ma_client_notifier.h"
#include "ma/internal/defs/ma_general_defs.h"

#include <windows.h>
#include <stdlib.h>
#include <stdio.h>

#define LOG_FACILITY_NAME	"ma_client"

static ma_error_t agent_install_notification_at_start(ma_client_notifier_t *self);

struct ma_client_notifier_s {
	struct ma_client_s *client;
	HANDLE				stop_event;
	HANDLE				reg_change_event;
	HANDLE				thread_handle;
	ma_bool_t			is_running;	
	ma_bool_t			stop_notifier;	
	DWORD				poll_timeout;
	char				msgbus_version[MA_MSGBUS_VERSION_LENGTH];
}; 

void ma_client_notifier_deinit(ma_client_notifier_t *self) {
	if(self->reg_change_event) CloseHandle(self->reg_change_event); self->reg_change_event = NULL;	
	if(self->stop_event) CloseHandle(self->stop_event); self->stop_event = NULL; 	
	if(self->thread_handle) CloseHandle(self->thread_handle); self->thread_handle = NULL;
}

ma_error_t ma_client_notifier_create(ma_client_t *client, ma_client_notifier_t **notifier) {
	if(client && notifier) {
		ma_client_notifier_t *self = (ma_client_notifier_t *)calloc(1, sizeof(ma_client_notifier_t)) ;

		if(!self)	return MA_ERROR_OUTOFMEMORY ;
		
		if(self->stop_event = CreateEvent(NULL, TRUE, FALSE, NULL)) {
			if(self->reg_change_event = CreateEvent(NULL, TRUE, FALSE, NULL)){
				self->poll_timeout = MA_CLIENT_CONNECTION_CHECK_TIMEOUT;
				self->client = client;
				*notifier = self;
				return MA_OK;
			}
		}
		(void)ma_client_notifier_deinit(self);
		free(self); 
		self = NULL;		
		return MA_ERROR_UNEXPECTED;
	}
	return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_client_notifier_set_poll_timeout(ma_client_notifier_t *self, unsigned long timeout){
	if(self){
		self->poll_timeout = timeout;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_client_notifier_release(ma_client_notifier_t *self) {
	if(self) {
		(void)ma_client_notifier_deinit(self);
		free(self) ;
	}
	return MA_OK ;
}

static DWORD WINAPI notifier_thread(void* arg) {
	ma_client_notifier_t *self = (ma_client_notifier_t *)arg ;
	ma_bool_t pause = MA_FALSE;
	HANDLE handles[2] = { self->reg_change_event, self->stop_event };
	HKEY hKey;
	LSTATUS err = ERROR_SUCCESS;	
	self->is_running = MA_TRUE;

	self->stop_notifier = MA_FALSE;
	while(!self->stop_notifier) 
	{	
		pause = MA_FALSE;

		if(ERROR_SUCCESS == (err = RegOpenKeyExA(HKEY_LOCAL_MACHINE, MA_CONFIGURATION_REGISTRY_AGENT_FULL_PATH_STR, 0L, KEY_READ|KEY_WOW64_32KEY , &hKey))) 
		{
			DWORD notify_filter = REG_NOTIFY_CHANGE_LAST_SET | REG_NOTIFY_CHANGE_NAME; 
			ResetEvent(self->reg_change_event);

			if(ERROR_SUCCESS == (err = RegNotifyChangeKeyValue(hKey, TRUE, notify_filter, self->reg_change_event, TRUE)))
			{
				DWORD result  = 0;				
				result = WaitForMultipleObjects(2, handles, FALSE, self->poll_timeout);
				
				switch(result) 
				{
					case WAIT_TIMEOUT:
						if(self->poll_timeout == INFINITE) self->stop_notifier = MA_TRUE;						
						else self->poll_timeout = INFINITE;

					case WAIT_OBJECT_0:
						{
							char version[MA_MSGBUS_VERSION_LENGTH]  = {0};
							DWORD version_size  = sizeof(version)-1;
							DWORD version_type = REG_SZ;

							if(ERROR_SUCCESS == (err = RegQueryValueExA(hKey, MA_CONFIGURATION_KEY_MSGBUS_VERSION_STR, 0, &version_type, (LPBYTE)&version[0], &version_size))){

								if(strcmp(self->msgbus_version, version)){									

									if(self->msgbus_version[0])
										MA_CLIENT_LOG(self->client, MA_LOG_SEV_INFO, "agent is upgraded, msgbus version %s", version);
									else
										MA_CLIENT_LOG(self->client, MA_LOG_SEV_INFO, "agent is installed, msgbus version %s", version);
											
									memset(self->msgbus_version, 0, sizeof(self->msgbus_version));
									strncpy(self->msgbus_version, version, sizeof(self->msgbus_version)-1);

									self->client->client_cb(self->client, (ma_client_notification_t)MA_CLIENT_NOTIFY_STOP, self->client->notify_cb_data);																				
									self->client->client_cb(self->client, (ma_client_notification_t)MA_CLIENT_NOTIFY_START, self->client->notify_cb_data);																				
								}
							}
							else{
								self->msgbus_version[0] = '\0';
								MA_CLIENT_LOG(self->client, MA_LOG_SEV_INFO, "no msgbus version, agent is not installed, rc(%ld)", err);								
								self->client->client_cb(self->client, MA_CLIENT_NOTIFY_STOP, self->client->notify_cb_data);										
							}
						}
						break;
					case WAIT_OBJECT_0 + 1:						
						self->stop_notifier = MA_TRUE;
						break;
					default:				
						break;
				}
			}
			else{
				MA_CLIENT_LOG(self->client, MA_LOG_SEV_ERROR, "failed to setup registry %s monitor, error(%ld)", MA_CONFIGURATION_REGISTRY_AGENT_PATH_STR, err);				
				self->stop_notifier = MA_TRUE;
			}
			RegCloseKey(hKey);									
		}
		else{
			MA_CLIENT_LOG(self->client, MA_LOG_SEV_ERROR, "failed to open registry %s, error(%ld), waiting for registry key to be re-established", MA_CONFIGURATION_REGISTRY_AGENT_PATH_STR, err);			
			pause = MA_TRUE;
		}		

		if(pause)
		{
			MA_CLIENT_LOG(self->client, MA_LOG_SEV_DEBUG, "pause for registry key to be re-established");
			self->poll_timeout = MA_CLIENT_CONNECTION_CHECK_TIMEOUT;
			WaitForSingleObject( self->stop_event, self->poll_timeout);
			agent_install_notification_at_start(self);
		}
	}
	self->is_running = MA_FALSE;
	MA_CLIENT_LOG(self->client, MA_LOG_SEV_INFO, "ma client notifier thread exiting...");
	return 0;
}

static ma_error_t agent_install_notification_at_start(ma_client_notifier_t *self){
	HKEY hKey;
	LSTATUS err = ERROR_SUCCESS;		

	if(ERROR_SUCCESS == (err = RegOpenKeyExA(HKEY_LOCAL_MACHINE, MA_CONFIGURATION_REGISTRY_AGENT_FULL_PATH_STR, 0L, KEY_READ|KEY_WOW64_32KEY , &hKey))) {
		char version[MA_MSGBUS_VERSION_LENGTH]  = {0};
		DWORD version_size  = sizeof(version)-1;
		DWORD version_type = REG_SZ;

		if(ERROR_SUCCESS == (err = RegQueryValueExA(hKey, MA_CONFIGURATION_KEY_MSGBUS_VERSION_STR, 0, &version_type, (LPBYTE)&version[0], &version_size))){

			if(version[0]){		
				MA_CLIENT_LOG(self->client, MA_LOG_SEV_INFO, "agent is installed, msgbus version %s", version);

				memset(self->msgbus_version, 0, sizeof(self->msgbus_version));
				strncpy(self->msgbus_version, version, sizeof(self->msgbus_version)-1);
				self->client->client_cb(self->client, (ma_client_notification_t)MA_CLIENT_NOTIFY_START, self->client->notify_cb_data);																				
			}
		}
		else{
			self->msgbus_version[0] = '\0';
			MA_CLIENT_LOG(self->client, MA_LOG_SEV_INFO, "no msgbus version, agent is not installed, rc(%ld)", err);
		}
	}
	else
		MA_CLIENT_LOG(self->client, MA_LOG_SEV_ERROR, "failed to open registry %s, error <%ld>", MA_CONFIGURATION_REGISTRY_AGENT_PATH_STR, err);					
	return MA_OK;
}

ma_error_t ma_client_notifier_start(ma_client_notifier_t *self) {
	if(self) {
		ma_error_t rc = MA_OK;
		if(!self->is_running){			
			DWORD thread_id = 0;
			self->thread_handle = INVALID_HANDLE_VALUE;

			if(MA_OK == (rc = agent_install_notification_at_start(self))){

				self->thread_handle = CreateThread(NULL, 0, notifier_thread, (void *)self, 0, &thread_id); 						
				if(INVALID_HANDLE_VALUE == self->thread_handle)
					MA_CLIENT_LOG(self->client, MA_LOG_SEV_ERROR, "failed to start ma client notifier.");					
				else
					MA_CLIENT_LOG(self->client, MA_LOG_SEV_INFO, "ma client notifier thread started.");	
			}
		}
		else
			MA_CLIENT_LOG(self->client, MA_LOG_SEV_INFO, "ma client notifier thread is already running.");				
		return rc;
	}
	return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_client_notifier_stop(ma_client_notifier_t *self) {
	if(self) {
		if(self->is_running){			
			SetEvent(self->stop_event);

			if(INVALID_HANDLE_VALUE != self->thread_handle) {					
				DWORD wait_rs = 0;
				self->stop_notifier = MA_TRUE;
				wait_rs = WaitForSingleObject(self->thread_handle, MA_CLIENT_NOTIFIER_STOP_TIMEOUT);				
				switch(wait_rs) {
					case WAIT_OBJECT_0:
							MA_CLIENT_LOG(self->client, MA_LOG_SEV_INFO, "ma client notifier thread stopped successfully.");																
							CloseHandle(self->thread_handle); self->thread_handle = NULL;
							break;											
					case WAIT_TIMEOUT:
							MA_CLIENT_LOG(self->client, MA_LOG_SEV_ERROR, "failed to stop ma client notifier thread.");															
							break;							
					default:
							MA_CLIENT_LOG(self->client, MA_LOG_SEV_ERROR, "unknown error to stop ma client notifier thread.");															
							break;						
				}
			}
		}
		return (self->is_running ? MA_ERROR_UNEXPECTED : MA_OK);
	}
	return MA_ERROR_INVALIDARG ;
}

