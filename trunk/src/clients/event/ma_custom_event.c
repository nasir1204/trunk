#include "ma/ma_message.h"
#include "ma_event_common.h"
#include "ma_event_client_internal.h"
#include "ma/internal/clients/ma/ma_client_internal.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ma/event/ma_custom_event.h"
#include "ma/internal/utils/xml/ma_xml.h"

#define MA_CUSTOM_EVENT_ID			"EventID"
#define MA_CUSTOM_EVENT_SEVERITY	"Severity"
#define MA_CUSTOM_EVENT_GMTTIME		"GMTTime"
#define MA_CUSTOM_EVENT_UTCTIME		"UTCTime"

struct ma_custom_event_s{
	ma_client_base_t		*cevt_client;
	ma_xml_t				*custom_event_xml;
	ma_atomic_counter_t		ref_count;
};

static ma_error_t ma_custom_event_set_machineinfo_node(ma_xml_node_t *parent_node) {
	ma_error_t rc = MA_ERROR_INVALIDARG;
	if(parent_node){
		ma_xml_node_t *node = NULL;
		rc = ma_xml_node_create(parent_node, MA_CUSTOM_EVENT_MACHINEINFO_NODE, &node);
	}
	return rc;
}

static ma_error_t ma_custom_event_set_client_context(ma_client_t *ma_client, ma_custom_event_t *event) {
	if(ma_client && event){
        const ma_context_t *ma_context = NULL;      
        ma_client_get_context(ma_client, &ma_context);
        if(ma_context) {
		    ma_error_t rc = MA_OK;
		    ma_client_base_t *event_client = NULL;
		    if(MA_OK == (rc = ma_event_client_get_client(ma_client, &event_client)) ){		
			    event->cevt_client = event_client;
		    }
		    return rc;
        }                
        return MA_ERROR_INVALID_CLIENT_OBJECT;
	}
	return MA_ERROR_INVALIDARG;
}


ma_error_t ma_custom_event_create(ma_client_t *ma_client, const char *event_root_name, ma_custom_event_t **event, ma_xml_node_t **root_node) {
	ma_error_t rc = MA_ERROR_INVALIDARG;
	if(ma_client && event_root_name && event && root_node){
		ma_custom_event_t *self = (ma_custom_event_t*) calloc (1, sizeof(ma_custom_event_t) );
		rc = MA_ERROR_OUTOFMEMORY;
		if(self) {
			if(MA_OK == (rc = ma_xml_create(&self->custom_event_xml)) ){
				if(MA_OK == (rc = ma_xml_construct(self->custom_event_xml)) ){
					if(MA_OK == (rc = ma_custom_event_set_client_context(ma_client , self))) {
						ma_xml_node_t *node = NULL;
						if(MA_OK == (rc = ma_xml_node_create(ma_xml_get_node(self->custom_event_xml), event_root_name, &node))) {
							/*Set "MachineInfo" child node */
							if(MA_OK == (rc = ma_custom_event_set_machineinfo_node(node))) {
								*root_node = node;
								MA_ATOMIC_INCREMENT(self->ref_count);
								*event = self;
								return rc;
							}
						}
					}
				}
				ma_xml_release(self->custom_event_xml);				
			}
			free(self);
		}
	}
	return rc;
}

ma_error_t ma_custom_event_add_ref(ma_custom_event_t *self){
	if(self){
		MA_ATOMIC_INCREMENT(self->ref_count);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_custom_event_release(ma_custom_event_t *event) {
	ma_error_t rc = MA_ERROR_INVALIDARG;
	if(event) {
		if(!MA_ATOMIC_DECREMENT(event->ref_count)){
			ma_xml_release(event->custom_event_xml);
			free(event);
		}
		rc = MA_OK;
	}
	return rc;
}

ma_error_t ma_custom_event_create_from_buffer(ma_client_t *ma_client, const char *buffer, ma_custom_event_t **event){
	ma_error_t rc = MA_ERROR_INVALIDARG;
	if(ma_client && buffer && event){
		ma_custom_event_t *self = (ma_custom_event_t*) calloc (1, sizeof(ma_custom_event_t) );
		rc = MA_ERROR_OUTOFMEMORY;
		if(self) {
			if(MA_OK == (rc = ma_xml_create(&self->custom_event_xml)) ){
				if(MA_OK == (rc = ma_custom_event_set_client_context(ma_client , self))) {
					if(MA_OK == (rc = ma_xml_load_from_buffer(self->custom_event_xml, buffer, strlen(buffer)))){
						*event = self;
						return rc;
					}
				}
				ma_xml_release(self->custom_event_xml);
			}
			free(self);
		}
	}
	return rc;
}

static ma_error_t ma_custom_event_time_node_set(ma_xml_node_t *parent){
	ma_error_t rc = MA_ERROR_INVALIDARG;
	ma_xml_node_t *gmt_node = NULL;
	if(MA_OK == (rc = ma_xml_node_create(parent, MA_CUSTOM_EVENT_GMTTIME, &gmt_node))){
		ma_time_t gmttime;
		char time_buffer[128] = {0};
		ma_time_get_gmttime(&gmttime);
		MA_MSC_SELECT(_snprintf, snprintf)(time_buffer, 127, "%04u-%02u-%02uT%02u:%02u:%02u", gmttime.year, gmttime.month, gmttime.day, gmttime.hour, gmttime.minute, gmttime.second);
		if(MA_OK == (rc = ma_xml_node_set_data(gmt_node, time_buffer))){			
		}
	}
	return rc;
}

ma_error_t ma_custom_event_create_event_node(ma_custom_event_t *event, ma_xml_node_t *parent, const char *event_node_name, ma_uint32_t event_id, ma_event_severity_t severity, ma_xml_node_t **node){
	ma_error_t rc = MA_ERROR_INVALIDARG;
	if(event && parent && event_node_name && node){
		if(ma_event_client_is_event_disabled(event->cevt_client, event_id))
			    return MA_ERROR_EVENT_DISABLED;	
		if(MA_OK == (rc = ma_xml_node_create(parent, event_node_name, node))){
			ma_xml_node_t *eventid_node = NULL;
			if(MA_OK == (rc = ma_xml_node_create(*node, MA_CUSTOM_EVENT_ID, &eventid_node))){
				char buff[32] = {0};
				MA_MSC_SELECT(_snprintf, snprintf)(buff, 31, "%u", event_id);
				if(MA_OK == (rc = ma_xml_node_set_data(eventid_node, buff))){
					ma_xml_node_t *severity_node = NULL;
					if(MA_OK == (rc = ma_xml_node_create(*node, MA_CUSTOM_EVENT_SEVERITY, &severity_node))){
						memset(buff, 0, 32);
						MA_MSC_SELECT(_snprintf, snprintf)(buff, 31, "%u", severity);
						if(MA_OK == (rc = ma_xml_node_set_data(severity_node, buff))){
							rc = ma_custom_event_time_node_set(*node);
						}
					}
				}
			}
		}
	}
	return rc;
}

static ma_error_t get_buffer_from_custom_event(ma_custom_event_t *event, char **buffer){
	ma_error_t rc = MA_ERROR_INVALIDARG;
	if(event && buffer){
		ma_bytebuffer_t *byte_buff = NULL;
		if(MA_OK == (rc = ma_bytebuffer_create(1024,&byte_buff))) {
			unsigned char *p_temp_buf = NULL;
			size_t size = 0;
			if( (MA_OK == (rc = ma_xml_save_to_buffer (event->custom_event_xml, byte_buff))) && (p_temp_buf = ma_bytebuffer_get_bytes(byte_buff)) && (size = ma_bytebuffer_get_size(byte_buff))){
				*buffer = (char *) calloc(size + 1 ,sizeof(unsigned char));
				if(*buffer){
					strncpy((char*) *buffer,(char*) p_temp_buf, size);
					(*buffer)[size] = '\0';
					rc = MA_OK;
				}
			}
			ma_bytebuffer_release(byte_buff);
		}
	}
	return rc;
}

ma_error_t ma_custom_event_send(ma_custom_event_t *event){
	if(event){
		ma_error_t rc = MA_OK;		
		char *buffer = NULL;
		if ( MA_OK == (rc = get_buffer_from_custom_event(event, &buffer) ) ){
			ma_message_t *message = NULL;
			ma_variant_t *payload = NULL;
			if( (MA_OK == (rc = ma_message_create(&message)))) {
				if(MA_OK == (rc = ma_variant_create_from_string(buffer, strlen(buffer), &payload))) {
					ma_msgbus_endpoint_t *endpoint = NULL;		
					ma_message_set_property(message, MA_EVENT_MSG_PROP_KEY_MSG_TYPE, MA_EVENT_MSG_PROP_VALUE_REQ_POST_CUSTOM_EVENT );
					ma_message_set_property(message, MA_EVENT_MSG_PROP_KEY_PRODUCT_ID, ma_event_client_get_software_id(event->cevt_client));
					ma_message_set_property(message, MA_EVENT_MSG_PROP_KEY_EVENT_FILTER_VERSION, ma_event_client_get_event_filter_version(event->cevt_client));			

					ma_message_set_payload(message, payload);		
					if( MA_OK == ( rc = ma_msgbus_endpoint_create(ma_event_client_get_bus(event->cevt_client), MA_EVENT_SERVICE_NAME_STR, NULL, &endpoint) ) ){
						ma_message_t *response = NULL;
						ma_message_t *pub_message = NULL;
						ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;
						ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);									

						if(MA_OK == ma_event_client_get_msgbus_thread_option(event->cevt_client, &thread_option) && MA_MSGBUS_CALLBACK_THREAD_POOL == thread_option) {
							ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);
							rc = ma_msgbus_send(endpoint, message, &response);
						}
						else {
							ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);
							rc = ma_msgbus_send_and_forget(endpoint, message);
						}
                    
						ma_msgbus_endpoint_release(endpoint);		
						if(response) 
							ma_message_release(response);
						if( (MA_OK == rc) && (MA_OK == ma_message_clone(message, &pub_message)) ) {
							(void)ma_msgbus_publish(ma_event_client_get_bus(event->cevt_client), MA_EVENT_MSG_TOPIC_PUBSUB_CUSTOM_EVENTS, MSGBUS_CONSUMER_REACH_OUTPROC, pub_message);
							ma_message_release(pub_message);
						}
					}
					ma_variant_release(payload);
				}
				ma_message_release(message);
			}
			free(buffer);
		}			
		return rc;
	}	
	return MA_ERROR_INVALIDARG;
}


ma_error_t ma_custom_event_node_create(ma_xml_node_t *parent, const char *name, ma_xml_node_t **node){
	return ma_xml_node_create(parent, name, node);
}

ma_error_t ma_custom_event_node_attribute_set(ma_xml_node_t *node, const char *name, const char *value){
	return ma_xml_node_attribute_set(node, name, value);
}

ma_error_t ma_custom_event_node_set_data(ma_xml_node_t *node, const char *data) {
	return ma_xml_node_set_data(node, data);
}

ma_error_t ma_custom_event_get_root_node(ma_custom_event_t *event, ma_xml_node_t **root_node){
	if(event && event->custom_event_xml && root_node) {
		if(!(*root_node = ma_xml_get_node(event->custom_event_xml)))
			return MA_ERROR_OBJECTNOTFOUND;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_custom_event_node_name_get(ma_xml_node_t *node, const char **name) {
	if(node && name) {
		if(!(*name = ma_xml_node_get_name(node)))
			return MA_ERROR_OBJECTNOTFOUND;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_custom_event_node_data_get(ma_xml_node_t *node, const char **data) {
	if(node && data) {
		if(!(*data = ma_xml_node_get_data(node)))
			return MA_ERROR_OBJECTNOTFOUND;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_custom_event_node_attribute_get(ma_xml_node_t *node, const char *attribute, const char **data) {
	if(node && attribute && data) {
		if(!(*data = ma_xml_node_attribute_get(node, attribute)))
			return MA_ERROR_OBJECTNOTFOUND;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_custom_event_node_search(ma_xml_node_t *node, const char *name, ma_xml_node_t **data) {
	if(node && name && data) {
		if(!(*data = ma_xml_node_search(node, name)))
			return MA_ERROR_OBJECTNOTFOUND;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_custom_event_get_child_node(ma_xml_node_t *node, ma_xml_node_t **child_node) {
	if(node && child_node) {
		if(!(*child_node = ma_xml_node_get_first_child(node)))
			return MA_ERROR_OBJECTNOTFOUND;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_custom_event_get_next_node(ma_xml_node_t *node, ma_xml_node_t **next_node) {
	if(node && next_node) {
		if(!(*next_node = ma_xml_node_get_next(node)))
			return MA_ERROR_OBJECTNOTFOUND;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_custom_event_get_next_sibling(ma_xml_node_t *node, ma_xml_node_t **sibling_node) {
	if(node && sibling_node) {
		if(!(*sibling_node = ma_xml_node_get_next_sibling(node)))
			return MA_ERROR_OBJECTNOTFOUND;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}


