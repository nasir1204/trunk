#include "ma/ma_message.h"
#include "ma_event_common.h"
#include "ma_event_client_internal.h"
#include "ma/internal/clients/ma/ma_client_internal.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

ma_error_t ma_event_bag_create(ma_client_t *ma_client, const char *product_name, ma_event_bag_t **event_bag){
	if(ma_client && product_name && event_bag){
		ma_error_t rc = MA_OK;
		ma_event_bag_t *ev_bag = NULL;
		ma_client_base_t *event_client = NULL;

		if(MA_OK == (rc = ma_event_client_get_client(ma_client, &event_client)) ){		
			rc = MA_ERROR_OUTOFMEMORY;
			ev_bag = (ma_event_bag_t*) calloc (1, sizeof(ma_event_bag_t) );
			if(ev_bag){
				if( MA_OK == (rc = ma_array_create(&ev_bag->product_events) ) ){
					ev_bag->evt_client = event_client;
					ev_bag->product_name = strdup(product_name);
					MA_ATOMIC_INCREMENT(ev_bag->ref_count);
					*event_bag = ev_bag;
				}
				else
					free(ev_bag);			
			}
		}
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_event_bag_set_product_version(ma_event_bag_t *self, const char *product_version){
	if(self && product_version){
		self->product_version = strdup(product_version);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_event_bag_set_product_family(ma_event_bag_t *self, const char *product_family){
	if(self && product_family){
		self->product_family = strdup(product_family);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

extern ma_error_t ma_event_info_table_add_entry(ma_table_t **table, const char *key, ma_variant_t *value);
ma_error_t ma_event_bag_add_common_field(ma_event_bag_t *self, const char *key, ma_variant_t *value){
	if(self && key && value)
		return ma_event_info_table_add_entry(&self->common_fileds, key, value);			
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_event_bag_set_custom_fields_table(ma_event_bag_t *self, const char *custum_target_table){
	if(self && custum_target_table){
		self->custom_targetname = strdup(custum_target_table);			
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_event_bag_add_custom_field(ma_event_bag_t *self, const char *key, ma_variant_t *value){
	if(self && key && value)
		return ma_event_info_table_add_entry(&self->custom_fileds, key, value);			
	return MA_ERROR_INVALIDARG;
}

/*
ThreatCategory
ThreatName
ThreatType
ThreatActionTaken
ThreatHandled
"ThreatActionTaken", "ThreatHandled"
*/
static ma_error_t validate_event(ma_event_t *maevent){
	if( maevent->common_fileds ){
		int index = 0;
		const char *minimal_common_fileds[] = {"ThreatCategory", "ThreatName", "ThreatType"};
		for(; index < (sizeof(minimal_common_fileds)/sizeof(const char*)); index++){
			ma_variant_t *value = NULL;
			if(MA_OK == ma_table_get_value(maevent->common_fileds, minimal_common_fileds[index], &value)){
				ma_variant_release(value);							
			}
			else{
				return MA_ERROR_EVENT_MINIMAL_COMMON_FIELDS_NOT_EXIST;
			}
		}
		return MA_OK;
	}
	return MA_ERROR_EVENT_MINIMAL_COMMON_FIELDS_NOT_EXIST;
}

ma_error_t ma_event_bag_add_event(ma_event_bag_t *self, ma_event_t *maevent){
	if(self && maevent){
		ma_error_t rc = MA_OK;
		ma_variant_t *evt_variant = NULL;		
		if( MA_OK == (rc = event_to_variant(maevent,&evt_variant) ) )
			rc = ma_array_push(self->product_events,evt_variant);
		if(evt_variant)
			ma_variant_release(evt_variant);
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_event_bag_send(ma_event_bag_t *self){
	if(self){
		ma_error_t rc = MA_OK;		
		ma_variant_t *payload = NULL;				
		if ( MA_OK == (rc = event_bag_to_variant(self,&payload) ) ){
			ma_message_t *message = NULL;
			if( MA_OK == (rc = ma_message_create(&message) ) ){
				ma_msgbus_endpoint_t *endpoint = NULL;		
				ma_msgbus_t *bus = ma_event_client_get_bus(self->evt_client);
				ma_message_set_property(message, MA_EVENT_MSG_PROP_KEY_MSG_TYPE, MA_EVENT_MSG_PROP_VALUE_REQ_POST_EVENT );
				ma_message_set_property(message, MA_EVENT_MSG_PROP_KEY_PRODUCT_ID, ma_event_client_get_software_id(self->evt_client));			
				ma_message_set_property(message, MA_EVENT_MSG_PROP_KEY_EVENT_FILTER_VERSION, ma_event_client_get_event_filter_version(self->evt_client));			
				ma_message_set_payload(message, payload);	

				if( MA_OK == ( rc = ma_msgbus_endpoint_create(bus, MA_EVENT_SERVICE_NAME_STR, NULL, &endpoint) ) ){
					ma_message_t *response = NULL;
					ma_message_t *pub_message = NULL;
					ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;
					ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);															

					if(MA_OK == ma_event_client_get_msgbus_thread_option(self->evt_client, &thread_option) && MA_MSGBUS_CALLBACK_THREAD_POOL == thread_option) {
                        ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);
						rc = ma_msgbus_send(endpoint, message, &response);
                    }
					else {
                        ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);
						rc = ma_msgbus_send_and_forget(endpoint, message);
                    }
                    
					ma_msgbus_endpoint_release(endpoint);		
					if(response) 
						ma_message_release(response);

					if( (MA_OK == rc) && (MA_OK == ma_message_clone(message, &pub_message)) ) {
						(void)ma_msgbus_publish(bus, MA_EVENT_MSG_TOPIC_PUBSUB_EVENTS, MSGBUS_CONSUMER_REACH_OUTPROC, pub_message);
						ma_message_release(pub_message);
					}
				}
				ma_message_release(message);
			}			
			ma_variant_release(payload);			
		}			
		return rc;
	}	
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_event_bag_add_ref(ma_event_bag_t *self){
	if(self){
		MA_ATOMIC_INCREMENT(self->ref_count);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_event_bag_release(ma_event_bag_t *self){
	if(self){
		if(!MA_ATOMIC_DECREMENT(self->ref_count)){
			if(self->common_fileds) ma_table_release(self->common_fileds);
			if(self->custom_fileds) ma_table_release(self->custom_fileds);		
			if(self->custom_targetname) free( (char*)self->custom_targetname);
			if(self->product_version) free( (char*)self->product_version);		
			if(self->product_family) free( (char*)self->product_family);
			free( (char*)self->product_name);		
			ma_array_release(self->product_events);
			free(self);		
		}
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}


ma_error_t ma_event_bag_get_product_name(ma_event_bag_t *self, const char **product_name){
	if(self && product_name){
		*product_name = self->product_name;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_event_bag_get_product_version(ma_event_bag_t *self, const char **product_version){
	if(self && product_version){
		if(self->product_version){
			*product_version = self->product_version;
			return MA_OK;
		}
		return MA_ERROR_EVENT_SETTING_NOT_FOUND;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_event_bag_get_product_family(ma_event_bag_t *self, const char **product_family){
	if(self && product_family){
		if(self->product_family){
			*product_family = self->product_family;
			return MA_OK;
		}
		return MA_ERROR_EVENT_SETTING_NOT_FOUND;
	}
	return MA_ERROR_INVALIDARG;
}


ma_error_t ma_event_bag_get_common_field(ma_event_bag_t *self, const char *key, ma_variant_t **value){
	if(self && key && value){
		if(MA_OK == ma_table_get_value(self->common_fileds, key, value))
			return MA_OK;
		return MA_ERROR_EVENT_SETTING_NOT_FOUND;
	}
	return MA_ERROR_INVALIDARG;
}


ma_error_t ma_event_bag_get_custom_fields_table(ma_event_bag_t *self, const char **target_table){
	if(self && target_table){
		if(self->custom_targetname){
			*target_table = self->custom_targetname;			
			return MA_OK;
		}
		return MA_ERROR_EVENT_SETTING_NOT_FOUND;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_event_bag_get_custom_field(ma_event_bag_t *self, const char *key, ma_variant_t **value){
	if(self && key && value){
		if(MA_OK == ma_table_get_value(self->custom_fileds, key, value))
			return MA_OK;
		return MA_ERROR_EVENT_SETTING_NOT_FOUND;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_event_bag_get_event_count(ma_event_bag_t *self, size_t *count){
	if(self && count){
		if(self->product_events && MA_OK == ma_array_size(self->product_events, count))
			return MA_OK;
		return MA_ERROR_EVENT_INVALID_EVENT_FORMAT;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_event_bag_get_event(ma_event_bag_t *self, size_t index, ma_event_t **ma_event){
	if(self && ma_event){
		if(self->product_events){
			ma_error_t rc = MA_OK;
			ma_variant_t *evt_payload = NULL;
			if(MA_OK == (rc = ma_array_get_element_at(self->product_events, index+1, &evt_payload))){
				rc = variant_to_event(evt_payload, ma_event);
				ma_variant_release(evt_payload);
				return rc;
			}
			return MA_ERROR_OUTOFBOUNDS;
		}
		return MA_ERROR_EVENT_INVALID_EVENT_FORMAT;
	}
	return MA_ERROR_INVALIDARG;
}
