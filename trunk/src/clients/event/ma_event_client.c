#include "ma/internal/utils/context/ma_context.h"
#include "ma/internal/clients/ma/client/ma_client_manager.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/clients/ma/ma_client_internal.h"

#include "ma_event_client_internal.h"
#include "ma_event_common.h"
#include "ma/internal/utils/threading/ma_thread.h"
#include "ma/info/ma_info.h"

#include "ma/ma_log.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MA_EVENT_CLIENT_INTERNAL_CLIENT  "EVENT_BASE"

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "event"

typedef struct ma_event_client_s ma_event_client_t;

static ma_error_t ma_event_client_on_message(ma_event_client_t *client, const char *topic, ma_message_t *message);
static ma_error_t ma_event_client_release(ma_event_client_t *client);

static struct ma_client_base_methods_s methods =  {
	&ma_event_client_on_message,
	&ma_event_client_release
};

struct ma_event_client_s{	
	ma_client_base_t		base;
	ma_client_t				*ma_client;
	ma_logger_t				*logger;
	char					*event_filter_version;
	char					*software_id;
	ma_array_t				*filtered_event;	
	ma_mutex_t				guard_eventfilter;
	ma_event_callbacks_t	cb;	
};

const char *ma_event_client_get_event_filter_version(ma_client_base_t *event_client){
	ma_event_client_t *self = (ma_event_client_t*)event_client;
	return self && self->event_filter_version ? self->event_filter_version : "0";	
}

static void event_filter_update(ma_event_client_t *self, char const *version, ma_variant_t *filter_events){
	ma_array_t *filtered_event_array = NULL;
		
	if(self->event_filter_version && version && 0 == strcmp(self->event_filter_version, version))
		return;
	/*get the events id list*/
	(void)ma_variant_get_array(filter_events, &filtered_event_array);
	if(filtered_event_array){
		ma_mutex_lock(&self->guard_eventfilter);
		if(self->event_filter_version)
			free(self->event_filter_version);
		if(self->filtered_event)
			ma_array_release(self->filtered_event);		
		self->event_filter_version = strdup(version);
		self->filtered_event = filtered_event_array;
		ma_mutex_unlock(&self->guard_eventfilter);

		MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Updating event filter to version=%s.", self->event_filter_version);
	}	
	else
		MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Disbaled event list not found in the payload.");
}

ma_error_t ma_event_upload(ma_client_t *ma_client, const char *product_id) {
    if(ma_client) {
        ma_context_t *ma_context = NULL;
        ma_msgbus_t *msgbus = NULL;

        ma_client_get_context(ma_client, &ma_context);
        msgbus = MA_CONTEXT_GET_MSGBUS(ma_context);        

        if(ma_context && msgbus) {
            ma_message_t *request = NULL;
            ma_error_t rc = MA_OK;
            ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(ma_context);            
            MA_LOG(logger, MA_LOG_SEV_TRACE, "sending upload events command.");
            if(MA_OK == (rc = ma_message_create(&request))) {
                ma_msgbus_endpoint_t *endpoint = NULL;
                (void)ma_message_set_property(request, MA_EVENT_MSG_PROP_KEY_MSG_TYPE, MA_EVENT_MSG_PROP_VALUE_REQ_UPLOAD_EVENT);
                (void)ma_message_set_property(request, MA_EVENT_MSG_PROP_KEY_EVENT_PRIORITY, "1");
                (void)ma_message_set_property(request, MA_EVENT_MSG_PROP_KEY_INITIATOR_STR, product_id);
                if(MA_OK == (rc = ma_msgbus_endpoint_create(msgbus, MA_EVENT_SERVICE_NAME_STR, MA_EVENT_SERVICE_HOSTNAME_STR, &endpoint))) {
			        (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                    if(MA_OK == (rc = ma_msgbus_send_and_forget(endpoint, request))) 
                        MA_LOG(logger, MA_LOG_SEV_INFO, "Events send to epo, command initiated by cmdagent");
					else
                        MA_LOG(logger, MA_LOG_SEV_ERROR, "Events send to epo, command initiated by cmdagent failed, %d.", rc);                    
                    (void)ma_msgbus_endpoint_release(endpoint);
                }
                (void)ma_message_release(request);
            }
            return rc;
        }
        return MA_ERROR_INVALID_CLIENT_OBJECT;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_event_client_on_message(ma_event_client_t *client, const char *topic, ma_message_t *message){
	if(message && topic){
		if(!strcmp(topic, MA_EVENT_MSG_TOPIC_PUBSUB_EVENT_FILTER) ){
			if(!strcmp(client->software_id, MA_EVENT_CLIENT_INTERNAL_CLIENT)){
				ma_variant_t *variant = NULL;		
				char const *filter_version = NULL;
				(void)ma_message_get_property(message, MA_EVENT_MSG_PROP_KEY_EVENT_FILTER_VERSION, &filter_version);			
				(void)ma_message_get_payload(message,&variant);

				MA_LOG(client->logger, MA_LOG_SEV_DEBUG, "Event filter message received.");

				if(filter_version && variant){
					ma_table_t *table = NULL;
					(void)ma_variant_get_table(variant,&table);
					(void)ma_variant_release(variant);
					if(table){
						ma_variant_t *filter_events = NULL;				
						(void)ma_table_get_value(table, MA_EVENT_MSG_PAYLOAD_KEY_DISABLED_EVENTS, &filter_events);
						(void)ma_table_release(table);
						if(filter_events){
							event_filter_update(client, filter_version, filter_events);												
							(void)ma_variant_release(filter_events);
						}
					}
				}
			}
		}
		else if(client->cb.cef_event_bag_cb && !strcmp(topic, MA_EVENT_MSG_TOPIC_PUBSUB_EVENTS)){
			ma_variant_t *variant = NULL;		
			char const *software_id = NULL;
			(void)ma_message_get_property(message, MA_EVENT_MSG_PROP_KEY_PRODUCT_ID, &software_id);			
			(void)ma_message_get_payload(message, &variant);
			MA_LOG(client->logger, MA_LOG_SEV_DEBUG, "CEF event received.");

			if(software_id && variant){
				ma_event_bag_t *event_bag = NULL;
				if(MA_OK == variant_to_event_bag(variant, &event_bag)){
					MA_LOG(client->logger, MA_LOG_SEV_DEBUG, "Sending CEF event to registered software %s.", client->software_id);
					client->cb.cef_event_bag_cb(client->ma_client, software_id, event_bag, client->base.data);
					(void)ma_event_bag_release(event_bag);
				}				
				(void)ma_variant_release(variant);
			}
			else
				MA_LOG(client->logger, MA_LOG_SEV_DEBUG, "CEF event malformed.");
		}
		else if(client->cb.custom_event_bag_cb && !strcmp(topic, MA_EVENT_MSG_TOPIC_PUBSUB_CUSTOM_EVENTS)){
			ma_variant_t *variant = NULL;		
			char const *software_id = NULL;
			ma_buffer_t *buffer = NULL;
			(void)ma_message_get_property(message, MA_EVENT_MSG_PROP_KEY_PRODUCT_ID, &software_id);			
			(void)ma_message_get_payload(message, &variant);
			MA_LOG(client->logger, MA_LOG_SEV_DEBUG, "custom event received.");
			if(software_id){
				if(variant) {
					if(MA_OK == ma_variant_get_string_buffer(variant, &buffer)) {
						ma_custom_event_t *custom_event_bag = NULL;
						const char *event_xml = NULL;
						size_t size;
						if(MA_OK == ma_buffer_get_string(buffer, &event_xml, &size)) {
							if(MA_OK == ma_custom_event_create_from_buffer(client->ma_client, event_xml, &custom_event_bag)){
								MA_LOG(client->logger, MA_LOG_SEV_DEBUG, "Sending custom event to registered software %s.", client->software_id);
								client->cb.custom_event_bag_cb(client->ma_client, software_id, custom_event_bag, client->base.data);
								(void)ma_custom_event_release(custom_event_bag);
							}else
								MA_LOG(client->logger, MA_LOG_SEV_DEBUG, "custom event create failed.");		
						}else
							MA_LOG(client->logger, MA_LOG_SEV_DEBUG, "get string from buffer failed.");
						(void)ma_buffer_release(buffer);
					}else
						MA_LOG(client->logger, MA_LOG_SEV_DEBUG, "get buffer from variant failed.");
					(void)ma_variant_release(variant);
				}else
					MA_LOG(client->logger, MA_LOG_SEV_DEBUG, "custom event payload missing.");
			}else
				MA_LOG(client->logger, MA_LOG_SEV_DEBUG, "custom event product id missing.");
		}
	}
	return MA_OK;
}

static void get_disabled_event_list(ma_event_client_t *event_client){	
	ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;
	if(MA_OK == ma_event_client_get_msgbus_thread_option( (ma_client_base_t*)event_client, &thread_option) && MA_MSGBUS_CALLBACK_THREAD_POOL == thread_option){
		ma_message_t *message = NULL;
		ma_context_t *ma_context = NULL;      
        ma_client_get_context(event_client->ma_client, &ma_context);

		MA_LOG(event_client->logger, MA_LOG_SEV_DEBUG, "Requesting the list of disabled events.");

		if( MA_OK == ma_message_create(&message) ){
			ma_msgbus_endpoint_t *endpoint = NULL;	
			(void)ma_message_set_property(message, MA_EVENT_MSG_PROP_KEY_MSG_TYPE, MA_EVENT_MSG_PROP_VALUE_REQ_GET_EVENT_FILTER );
			if( MA_OK == (ma_msgbus_endpoint_create(MA_CONTEXT_GET_MSGBUS(ma_context), MA_EVENT_SERVICE_NAME_STR, NULL, &endpoint) ) ){
				ma_message_t *response = NULL;
                ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;
                (void) ma_client_manager_get_thread_option(MA_CONTEXT_GET_CLIENT_MANAGER(ma_context), &thread_option);
                (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);
				(void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);													
				(void)ma_msgbus_send(endpoint, message, &response);
				(void)ma_msgbus_endpoint_release(endpoint);					
				if(response) {
					MA_LOG(event_client->logger, MA_LOG_SEV_DEBUG, "Disabled events request processed, response received.");
					ma_event_client_on_message(event_client, MA_EVENT_MSG_TOPIC_PUBSUB_EVENT_FILTER, response);			
					(void)ma_message_release(response);
				}
			}
			(void)ma_message_release(message);
		}								
	}
}

static char *get_client_name(char client_name[], size_t size, const char *product_id){
	MA_MSC_SELECT(_snprintf, snprintf)(client_name, size, "%s.%s", MA_EVENT_CLIENT_NAME, product_id);
	return client_name;
}
           
static ma_error_t ma_event_client_create(ma_client_t *ma_client, const char *product_id, ma_event_callbacks_t *cb, void *user_data, ma_event_client_t **event_client){
	if(ma_client && product_id && event_client){
		ma_error_t rc = MA_ERROR_EVENT_CLIENT_CREATE_FAILED;		
		ma_event_client_t *client = NULL ;
		ma_context_t *ma_context = NULL;      
        ma_client_get_context(ma_client, &ma_context);
        
        if(MA_CONTEXT_GET_MSGBUS(ma_context)){
			client = (ma_event_client_t*)calloc(1, sizeof(ma_event_client_t)) ;
			if(client){
				rc = MA_OK;
				ma_client_base_init( (ma_client_base_t*)client, &methods, user_data);
				client->ma_client = ma_client;
				client->logger = MA_CONTEXT_GET_LOGGER(ma_context);
				ma_mutex_init(&client->guard_eventfilter);									
				client->software_id = strdup(product_id);
				client->cb.cef_event_bag_cb = cb->cef_event_bag_cb;
				client->cb.custom_event_bag_cb = cb->custom_event_bag_cb;				
				get_disabled_event_list(client);			
				*event_client = client;							
			}	
		}
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_event_client_release(ma_event_client_t *self){
	if(self){		
		
		if(self->filtered_event)
			(void)ma_array_release(self->filtered_event);

		if(self->event_filter_version)
			free(self->event_filter_version);

		if(self->software_id)
			free(self->software_id);

		(void) ma_mutex_destroy(&self->guard_eventfilter);
		free(self);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

static ma_error_t event_client_register_callbacks(ma_client_t *ma_client, const char *product_id, ma_event_callbacks_t *cb, void *cb_data){
	if(ma_client && product_id && cb){		
		ma_context_t *ma_context = NULL;      
		ma_client_manager_t *client_manager = NULL;
        ma_client_get_context(ma_client, &ma_context);
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);		
        if(client_manager) {
            ma_error_t rc = MA_OK;		
		    ma_event_client_t *client = NULL;
			char client_name[MAX_CLIENT_NAME_LENGTH] = {0};

            if(MA_OK == (rc = ma_client_manager_get_client(client_manager, get_client_name(client_name, MAX_CLIENT_NAME_LENGTH, product_id) , (ma_client_base_t**)&client))){
				return MA_ERROR_CLIENT_ALREADY_REGISTERED;			
		    }
			else{
				if(MA_OK == (rc = ma_event_client_create(ma_client, product_id, cb, cb_data, &client))){
					if(MA_OK == (rc = ma_client_manager_add_client(client_manager, get_client_name(client_name, MAX_CLIENT_NAME_LENGTH, product_id) , (ma_client_base_t*)client))){						
						if(strcmp(product_id, MA_EVENT_CLIENT_INTERNAL_CLIENT))
							MA_LOG(client->logger, MA_LOG_SEV_DEBUG, "Event client registered for software id %s.", product_id);
						else
							MA_LOG(client->logger, MA_LOG_SEV_DEBUG, "Event base client created");
						return MA_OK;			
					}		
					(void)ma_event_client_release(client);
				}
			}
			return rc;
		}		
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_event_register_callbacks(ma_client_t *ma_client, ma_event_callbacks_t *cb, void *cb_data){
	if(ma_client && cb && (cb->cef_event_bag_cb || cb->custom_event_bag_cb)){
		ma_context_t *ma_context = NULL;  		
        ma_client_get_context(ma_client, &ma_context);		
		return event_client_register_callbacks(ma_client, MA_CONTEXT_GET_PRODUCT_ID(ma_context), cb , cb_data);
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_event_unregister_callbacks(ma_client_t *ma_client){
    if(ma_client) {
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;        
        ma_client_get_context(ma_client, &ma_context);
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);		
        
        if(client_manager) {
            ma_error_t rc = MA_OK;
            char client_name[MAX_CLIENT_NAME_LENGTH] = {0};
            ma_client_base_t *client = NULL;		   
						
            if(MA_OK != (rc = ma_client_manager_get_client(client_manager, get_client_name(client_name, MAX_CLIENT_NAME_LENGTH, MA_CONTEXT_GET_PRODUCT_ID(ma_context)), &client)))
                return ((MA_ERROR_CLIENT_NOT_FOUND == rc) ? MA_OK : rc);
            
            (void)ma_client_manager_remove_client(client_manager, client);			
            return ma_client_base_release(client);
        }
        return MA_ERROR_INVALID_CLIENT_OBJECT;
    }
    return MA_ERROR_INVALIDARG ;
}

/*internal client used for getting the disabled event and use them.*/
ma_error_t ma_event_client_get_client(ma_client_t *ma_client, ma_client_base_t **event_client){
	if(ma_client && event_client){
		ma_context_t *ma_context = NULL;      
		ma_client_manager_t *client_manager = NULL;
        ma_client_get_context(ma_client, &ma_context);
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);		
        if(client_manager) {
            ma_error_t rc = MA_OK;		
		    ma_event_client_t *client = NULL;
			char client_name[MAX_CLIENT_NAME_LENGTH] = {0};

            if(MA_OK == (rc = ma_client_manager_get_client(client_manager, get_client_name(client_name, MAX_CLIENT_NAME_LENGTH, MA_EVENT_CLIENT_INTERNAL_CLIENT) , (ma_client_base_t**)&client) ) ){
			    *event_client = (ma_client_base_t*)(client);			
		    }
			else {
				ma_event_callbacks_t cb = {NULL, NULL};
				if(MA_OK == (rc = event_client_register_callbacks(ma_client, MA_EVENT_CLIENT_INTERNAL_CLIENT, &cb, NULL))){
					if(MA_OK == (rc = ma_client_manager_get_client(client_manager, get_client_name(client_name, MAX_CLIENT_NAME_LENGTH, MA_EVENT_CLIENT_INTERNAL_CLIENT) , (ma_client_base_t**)&client) ) ){
						*event_client = (ma_client_base_t*)(client);			
					}
				}
			}
			return rc;
        }
        return MA_ERROR_INVALID_CONTEXT;
	}
	return MA_ERROR_INVALIDARG;
}

ma_bool_t ma_event_client_is_event_disabled(ma_client_base_t *client, ma_uint32_t eventid){
	ma_bool_t filtered = MA_FALSE;
	ma_event_client_t *event_client = (ma_event_client_t*)client;
	if( NULL == event_client ||  0 == eventid)
		return MA_TRUE;

	if(!event_client->filtered_event)
		get_disabled_event_list(event_client);

	if(event_client->filtered_event){				
		size_t index = 1;
		size_t size = 0;
		ma_mutex_lock(&event_client->guard_eventfilter);
		(void)ma_array_size(event_client->filtered_event, &size);
		for( ; index <= size ; index++){
			ma_variant_t *var = NULL;
			ma_array_get_element_at(event_client->filtered_event, index, &var );
			if(var){
				ma_uint32_t event_id = 0;
				(void)ma_variant_get_uint32(var, &event_id);
				(void)ma_variant_release(var);
				if(eventid == event_id){
					MA_LOG(event_client->logger, MA_LOG_SEV_DEBUG, "Event id %d is disabled", eventid);
					filtered = MA_TRUE;	
					break;
				}
			}
		}
		ma_mutex_unlock(&event_client->guard_eventfilter);
	}
	return filtered;
}

ma_msgbus_t *ma_event_client_get_bus(ma_client_base_t *self){
	if(self){
		ma_event_client_t *event_client = (ma_event_client_t*)self;
		ma_context_t *ma_context = NULL;      
		ma_client_get_context(event_client->ma_client, &ma_context);        
        return MA_CONTEXT_GET_MSGBUS(ma_context);
	}
	return NULL;
}

const char *ma_event_client_get_software_id(ma_client_base_t *self){
	if(self){
		ma_event_client_t *event_client = (ma_event_client_t*)self;
		ma_context_t *ma_context = NULL;      
		ma_client_get_context(event_client->ma_client, &ma_context);        
		return MA_CONTEXT_GET_PRODUCT_ID(ma_context);
	}
	return NULL;
}


ma_error_t ma_event_client_get_msgbus_thread_option(ma_client_base_t *self, ma_msgbus_callback_thread_options_t *thread_option){
	if(self && thread_option){
		ma_event_client_t *event_client = (ma_event_client_t*)self;		
		ma_context_t *ma_context = NULL;      
		ma_client_get_context(event_client->ma_client, &ma_context);        

		*thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;
		return ma_client_manager_get_thread_option(MA_CONTEXT_GET_CLIENT_MANAGER(ma_context), thread_option);
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_event_client_get_agent_mode(ma_client_t *ma_client, int *mode){
	if(ma_client && mode){
		ma_variant_t *var = NULL;
		*mode = 1;
		
		if(MA_OK == ma_info_get(ma_client, MA_INFO_AGENT_MODE_STR, &var) && var){
			ma_buffer_t *buff = NULL;
			if(MA_OK == ma_variant_get_string_buffer(var, &buff)){
				const char *buffer = NULL;
				size_t size = 0;

				if(MA_OK == ma_buffer_get_string(buff, &buffer, &size) && buffer)
					*mode = atoi(buffer);				
				(void)ma_buffer_release(buff);
			}
			(void)ma_variant_release(var);
		}		
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}


