#ifndef MA_EVENT_CLIENT_H_INCLUDED
#define MA_EVENT_CLIENT_H_INCLUDED

#include "ma/ma_message.h"
#include "ma/ma_msgbus.h"
#include "ma/internal/utils/context/ma_context.h"
#include "ma/internal/clients/ma/ma_client_base.h"

#define MA_EVENT_CLIENT_NAME	 "ma.client.event"

MA_CPP(extern "C" {)

ma_error_t ma_event_client_create(ma_context_t *context, ma_client_base_t **event_client);

ma_error_t ma_event_client_get_client(ma_context_t *context, ma_client_base_t **event_client);

ma_bool_t ma_event_client_is_event_disabled(ma_client_base_t *event_client, ma_uint32_t event_id);

ma_msgbus_t *ma_event_client_get_bus(ma_client_base_t *event_client);

ma_error_t ma_event_client_get_msgbus_thread_option(ma_client_base_t *self, ma_msgbus_callback_thread_options_t *thread_option);

MA_CPP(})    

#endif