#include "ma/ma_message.h"
#include "ma_event_common.h"
#include "ma_event_client_internal.h"
#include "ma/internal/clients/ma/ma_client_internal.h"

#include <string.h>
#include <stdio.h>

ma_error_t ma_event_create(ma_client_t *ma_client, ma_uint32_t eventid, ma_event_severity_t severity, ma_event_t **maevent){
	if(ma_client && maevent && eventid){
		ma_error_t rc = MA_OK;
		ma_event_t *evt = NULL;
		ma_client_base_t *event_client = NULL;
		int agent_mode = 1;

		if(MA_OK != (rc = ma_event_client_get_client(ma_client, &event_client) ) ) 
			return rc;		

		if(ma_event_client_is_event_disabled(event_client, eventid))
			return MA_ERROR_EVENT_DISABLED;		

		if(MA_OK == ma_event_client_get_agent_mode(ma_client, &agent_mode) && 0 == agent_mode)
			return MA_ERROR_AGENT_UNMANAGED;		

		rc = MA_ERROR_OUTOFMEMORY;
		evt = (ma_event_t*)calloc(1,sizeof(ma_event_t) );
		if(evt){
			rc = MA_OK;			
			evt->eventid = eventid;
			evt->severity = severity;
			ma_time_get_gmttime(&evt->gmttime);
			ma_time_get_localtime(&evt->localtime);
			MA_ATOMIC_INCREMENT(evt->ref_count);
			*maevent = evt;			
		}
		return rc;        
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_event_info_table_add_entry(ma_table_t **table, const char *key, ma_variant_t *value){
	ma_error_t rc = MA_OK;
	ma_vartype_t var_type = MA_VARTYPE_NULL; 
	ma_variant_get_type(value, &var_type);
	if(MA_VARTYPE_STRING == var_type){
		if(!(*table))
			rc = ma_table_create(table);
		if(*table)
			rc = ma_table_add_entry(*table, key, value);
		return rc;
	}
	return MA_ERROR_EVENT_INVALID_VALUE_TYPE;
}

ma_error_t ma_event_add_common_field(ma_event_t *self, const char *key, ma_variant_t *value){
	if(self && key && value)
		return ma_event_info_table_add_entry(&self->common_fileds, key, value);			
	return MA_ERROR_INVALIDARG;
}


ma_error_t ma_event_set_custom_fields_table(ma_event_t *self, const char *target_table){
	if(self && target_table){
		self->custom_targetname = strdup(target_table);			
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}


ma_error_t ma_event_add_custom_field(ma_event_t *self, const char *key, ma_variant_t *value){
	if(self && key && value)
		return ma_event_info_table_add_entry(&self->custom_fileds, key, value);			
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_event_add_ref(ma_event_t *self){
	if(self){
		MA_ATOMIC_INCREMENT(self->ref_count);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_event_release(ma_event_t *self){
	if(self){
		if(!MA_ATOMIC_DECREMENT(self->ref_count)){
			if(self->common_fileds) 
				ma_table_release(self->common_fileds);
			if(self->custom_fileds) 
				ma_table_release(self->custom_fileds);		
			if(self->custom_targetname) 
				free(self->custom_targetname);		
			free(self);			
		}
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}


ma_error_t ma_event_get_id(ma_event_t *self, ma_uint32_t *eventid){
	if(self && eventid){
		*eventid = self->eventid;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_event_get_severity(ma_event_t *self, ma_event_severity_t *severity){
	if(self && severity){
		*severity = self->severity;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_event_get_common_field(ma_event_t *self, const char *key, ma_variant_t **value){
	if(self && key && value){
		if(MA_OK == ma_table_get_value(self->common_fileds, key, value))
			return MA_OK;
		return MA_ERROR_EVENT_SETTING_NOT_FOUND;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_event_get_custom_fields_table(ma_event_t *self, const char **target_table){
	if(self && target_table){
		if(self->custom_targetname){
			*target_table = self->custom_targetname;			
			return MA_OK;
		}
		return MA_ERROR_EVENT_SETTING_NOT_FOUND;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_event_get_custom_field(ma_event_t *self, const char *key, ma_variant_t **value){
	if(self && key && value){
		if(MA_OK == ma_table_get_value(self->custom_fileds, key, value))
			return MA_OK;
		return MA_ERROR_EVENT_SETTING_NOT_FOUND;
	}
	return MA_ERROR_INVALIDARG;
}

