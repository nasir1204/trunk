#include "ma_event_common.h"
#include <string.h>

static ma_error_t table_add_string_value(ma_table_t *table, const char *key, const char	*value){
	ma_error_t rc = MA_OK;
	ma_variant_t *var = NULL;
	if( MA_OK == ( rc = ma_variant_create_from_string(value, strlen(value), &var) ) ){
		rc = ma_table_add_entry(table, key, var);
		ma_variant_release(var);
	}	
	return rc;	
}

static ma_error_t table_get_string_value(ma_table_t *table, const char *key, const char	**value){
	ma_error_t rc = MA_OK;
	ma_variant_t *var = NULL;
	if( MA_OK == ( rc = ma_table_get_value(table, key, &var) ) ){
		ma_buffer_t *buffer = NULL;
		if(MA_OK == (rc = ma_variant_get_string_buffer(var, &buffer))){
			const char *buffer_value = NULL;
			size_t size = 0;
			if(MA_OK == (rc = ma_buffer_get_string(buffer, &buffer_value, &size))){
				*value = buffer_value;
			}
			ma_buffer_release(buffer);
		}
		ma_variant_release(var);
	}	
	return rc;	
}

static ma_error_t table_add_table_value(ma_table_t *table, const char *key, ma_table_t *value){
	ma_error_t rc = MA_OK;
	ma_variant_t *var = NULL;
	if( MA_OK == ( rc = ma_variant_create_from_table(value, &var) ) ){
		rc = ma_table_add_entry(table, key, var);
		ma_variant_release(var);
	}	
	return rc;
}

static ma_error_t table_get_table_value(ma_table_t *table, const char *key, ma_table_t **value){
	ma_error_t rc = MA_OK;
	ma_variant_t *var = NULL;
	if( MA_OK == ( rc = ma_table_get_value(table, key, &var) ) ){
		rc = ma_variant_get_table(var, value);		
		ma_variant_release(var);
	}	
	return rc;
}

static ma_error_t table_add_array_value(ma_table_t *table, const char *key, ma_array_t *value){
	ma_error_t rc = MA_OK;
	ma_variant_t *var = NULL;
	if( MA_OK == ( rc = ma_variant_create_from_array(value, &var) ) ){
		rc = ma_table_add_entry(table, key, var);
		ma_variant_release(var);
	}	
	return rc;
}

static ma_error_t table_get_array_value(ma_table_t *table, const char *key, ma_array_t **value){
	ma_error_t rc = MA_OK;
	ma_variant_t *var = NULL;
	if( MA_OK == ( rc = ma_table_get_value(table, key, &var) ) ){
		rc = ma_variant_get_array(var, value);		
		ma_variant_release(var);
	}	
	return rc;
}

static ma_error_t table_add_uint32_value(ma_table_t *table, const char *key, ma_uint32_t value){
	ma_error_t rc = MA_OK;
	ma_variant_t *var = NULL;
	if( MA_OK == ( rc = ma_variant_create_from_uint32(value, &var) ) ){
		rc = ma_table_add_entry(table, key, var);
		ma_variant_release(var);
	}	
	return rc;
}

static ma_error_t table_get_uint32_value(ma_table_t *table, const char *key, ma_uint32_t *value){
	ma_error_t rc = MA_OK;
	ma_variant_t *var = NULL;
	if( MA_OK == ( rc = ma_table_get_value(table, key, &var) ) ){
		rc = ma_variant_get_uint32(var, value);
		ma_variant_release(var);
	}	
	return rc;
}

ma_error_t event_to_variant(ma_event_t *maevent, ma_variant_t **event_variant){
	if(maevent && event_variant){
		ma_error_t rc = MA_OK;
		ma_table_t *evt_data_table = NULL;
		ma_variant_t *evt_variant = NULL;
		
		/*create an event data table*/
		do{
			char time_str[24] = {0};

			if( MA_OK != ( rc = ma_table_create(&evt_data_table) ) )
				break;
				
			if( MA_OK != ( rc = ma_variant_create_from_table(evt_data_table, &evt_variant) ) )
				break;

			/*add gmt time*/
			MA_MSC_SELECT(_snprintf, snprintf)(time_str, 24, "%04d-%02d-%02dT%02d:%02d:%02d", 
				maevent->gmttime.year, maevent->gmttime.month, maevent->gmttime.day, 
				maevent->gmttime.hour, maevent->gmttime.minute, maevent->gmttime.second);
		
			if( MA_OK != (rc = table_add_string_value(evt_data_table, MA_EVENT_EVENT_GMTTIME_KEY, time_str) ) )
				break;

			/*add local time*/
			MA_MSC_SELECT(_snprintf, snprintf)(time_str, 24, "%04d-%02d-%02dT%02d:%02d:%02d", 
				maevent->localtime.year, maevent->localtime.month, maevent->localtime.day, 
				maevent->localtime.hour, maevent->localtime.minute, maevent->localtime.second);
		
			if( MA_OK != (rc = table_add_string_value(evt_data_table, MA_EVENT_EVENT_LOCALTIME_KEY, time_str) ) )
				break;

			/*add event id*/
			if( MA_OK != (rc = table_add_uint32_value(evt_data_table, MA_EVENT_EVENT_ID_KEY, maevent->eventid) ) )
				break;
			/*add event severity*/
			if( MA_OK != (rc = table_add_uint32_value(evt_data_table, MA_EVENT_EVENT_SEVERITY_KEY, maevent->severity) ) )
				break;		
	
			/*add common_fileds if any*/
			if( maevent->common_fileds ){
				if(MA_OK != (rc = table_add_table_value(evt_data_table, MA_EVENT_COMMON_FIELDS_KEY, maevent->common_fileds) ) )
					break;
			}		
			/*add custom_fileds if any*/
			if(maevent->custom_targetname && maevent->custom_fileds){
				if( MA_OK != (rc = table_add_string_value(evt_data_table, MA_EVENT_CUSTOM_TARGET_KEY, maevent->custom_targetname) ) )
					break;
				if( MA_OK != (rc = table_add_table_value(evt_data_table, MA_EVENT_CUSTOM_FIELDS_KEY, maevent->custom_fileds) ) ) 
					break;
			}
		}while(0);

		if(MA_OK == rc){
			*event_variant = evt_variant;
		}
		else{
			ma_variant_release(evt_variant);	
			rc = MA_ERROR_EVENT_PAYLOAD_FORMATION_FAILED;	
		}
		ma_table_release(evt_data_table);
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t variant_to_event(ma_variant_t *event_variant, ma_event_t **maevent){	
	if(event_variant && maevent){
		ma_error_t rc = MA_ERROR_OUTOFMEMORY;
		ma_event_t *ma_event = NULL;
		ma_table_t *evt_data_table = NULL;

		/*create an event*/
		do{
			ma_uint32_t value = 0;
			const char  *str_value = 0;
			ma_event = (ma_event_t*) calloc(1, sizeof(ma_event_t));			
			if(!ma_event)
				break;
			ma_event_add_ref(ma_event);

			if( MA_OK != ( rc = ma_variant_get_table(event_variant, &evt_data_table) ) )
				break;
		
			/*get event id*/
			if( MA_OK != (rc = table_get_uint32_value(evt_data_table, MA_EVENT_EVENT_ID_KEY, &ma_event->eventid) ) )
				break;

			/*get event severity*/
			if( MA_OK != (rc = table_get_uint32_value(evt_data_table, MA_EVENT_EVENT_SEVERITY_KEY, &value) ) )
				break;		

			ma_event->severity = (ma_event_severity_t)value;

			/*get common_fileds if any*/
			(void)table_get_table_value(evt_data_table, MA_EVENT_COMMON_FIELDS_KEY, &ma_event->common_fileds);
				
			/*get custom_fileds if any*/
			if(MA_OK == table_get_string_value(evt_data_table, MA_EVENT_CUSTOM_TARGET_KEY, &str_value) && str_value)
				ma_event->custom_targetname = strdup(str_value);
						
			if(ma_event->custom_targetname)
				(void)table_get_table_value(evt_data_table, MA_EVENT_CUSTOM_FIELDS_KEY, &ma_event->custom_fileds);
		
		}while(0);

		if(MA_OK == rc){
			*maevent = ma_event;
		}
		else{
			ma_event_release(ma_event);
			rc = MA_ERROR_EVENT_INVALID_EVENT_FORMAT;	
		}
		ma_table_release(evt_data_table);
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t event_bag_to_variant(ma_event_bag_t *self, ma_variant_t **payload){
	if(self && payload){
		ma_error_t rc = MA_OK;
		ma_table_t *evt_bag_table = NULL;
		ma_variant_t *evt_bag_variant = NULL;
		/*create an event bag data table*/
		do{
			if( MA_OK != ( rc = ma_table_create(&evt_bag_table) ) )	
				break;	
			if (MA_OK != ( rc = ma_variant_create_from_table(evt_bag_table, &evt_bag_variant) ) )
				break;	
			/*add product name*/
			if ( MA_OK != (rc = table_add_string_value(evt_bag_table, MA_EVENT_BAG_PRODUCT_NAME_KEY, self->product_name) ) ) 
				break;
			/*add events*/
			if ( MA_OK != (rc = table_add_array_value(evt_bag_table, MA_EVENT_BAG_EVENTS_KEY, self->product_events) ) ) 
				break;
			/*add product version if any*/
			if(self->product_version){
				if ( MA_OK != (rc = table_add_string_value(evt_bag_table, MA_EVENT_BAG_PRODUCT_VERSION_KEY, self->product_version) ) )
					break;
			}
			/*add product family if any*/
			if(self->product_family){
				if ( MA_OK != (rc = table_add_string_value(evt_bag_table, MA_EVENT_BAG_PRODUCT_FAMILY_KEY, self->product_family) ) )
					break;
			}		
			/*add common_fileds if any*/
			if(self->common_fileds){
				if ( MA_OK != (rc = table_add_table_value(evt_bag_table, MA_EVENT_BAG_COMMON_FIELDS_KEY, self->common_fileds) ) )
					break;
			}		
			/*add self->custom_fileds if any*/
			if(self->custom_fileds && self->custom_targetname){
				if ( MA_OK != (rc = table_add_string_value(evt_bag_table, MA_EVENT_BAG_CUSTOM_TARGET_KEY, self->custom_targetname) ) )
					break;
				if ( MA_OK != (rc = table_add_table_value(evt_bag_table, MA_EVENT_BAG_CUSTOM_FIELDS_KEY, self->custom_fileds) ) )
					break;
			}						
		}while(0);

		if(MA_OK == rc){
			*payload = evt_bag_variant;
		}
		else if(evt_bag_variant){
			ma_variant_release(evt_bag_variant);	
			rc = MA_ERROR_EVENT_MSG_PAYLOAD_FORMATION_FAILED;
		}
		if(evt_bag_table)
			ma_table_release(evt_bag_table);
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t variant_to_event_bag(ma_variant_t *payload, ma_event_bag_t **bag){
	if(payload && bag){
		ma_error_t rc = MA_ERROR_OUTOFMEMORY;
		ma_table_t *evt_bag_table = NULL;
		ma_event_bag_t *evt_bag = NULL;
		/*create an event bag*/
		do{
			ma_uint32_t value = 0;
			const char  *str_value = 0;

			evt_bag = (ma_event_bag_t*) calloc(1, sizeof(ma_event_bag_t));

			if(!evt_bag)	
				break;	

			ma_event_bag_add_ref(evt_bag);

			if (MA_OK != ( rc = ma_variant_get_table(payload, &evt_bag_table) ) )
				break;	

			/*get product name*/
			if ( MA_OK != (rc = table_get_string_value(evt_bag_table, MA_EVENT_BAG_PRODUCT_NAME_KEY, &str_value) ) && !str_value) 
				break;
			else{
				evt_bag->product_name = strdup(str_value);
				str_value = NULL;
			}

			/*get events*/
			if ( MA_OK != (rc = table_get_array_value(evt_bag_table, MA_EVENT_BAG_EVENTS_KEY, &evt_bag->product_events) ) ) 
				break;

			/*get product version if any*/
			if ( MA_OK == table_get_string_value(evt_bag_table, MA_EVENT_BAG_PRODUCT_VERSION_KEY, &str_value) && str_value) {
				evt_bag->product_version = strdup(str_value);
				str_value = NULL;
			}
		
			/*add product family if any*/		
			if ( MA_OK == table_get_string_value(evt_bag_table, MA_EVENT_BAG_PRODUCT_FAMILY_KEY, &str_value) && str_value) {
				evt_bag->product_family = strdup(str_value);
				str_value = NULL;
			}
				
			/*get common_fileds if any*/
			(void)table_get_table_value(evt_bag_table, MA_EVENT_BAG_COMMON_FIELDS_KEY, &evt_bag->common_fileds);
				
			/*get custom_fileds if any*/
			if(MA_OK == table_get_string_value(evt_bag_table, MA_EVENT_BAG_CUSTOM_TARGET_KEY, &str_value) && str_value)
				evt_bag->custom_targetname = strdup(str_value);
						
			if(evt_bag->custom_targetname)
				(void)table_get_table_value(evt_bag_table, MA_EVENT_BAG_CUSTOM_FIELDS_KEY, &evt_bag->custom_fileds);
										
		}while(0);

		if(MA_OK == rc){
			*bag = evt_bag;
		}
		else{
			if(evt_bag) (void)ma_event_bag_release(evt_bag);	
			rc = MA_ERROR_EVENT_INVALID_EVENT_FORMAT;
		}
		if(evt_bag_table)
			(void)ma_table_release(evt_bag_table);
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

