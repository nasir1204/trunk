#ifndef MA_EVENT_COMMON_H_INCLUDED
#define MA_EVENT_COMMON_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_variant.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/clients/ma/ma_client_base.h"
#include "ma/internal/defs/ma_event_defs.h"
#include "ma/event/ma_event_payload_defs.h"
#include "ma/event/ma_event_bag.h"
#include "ma/internal/utils/time/ma_time.h"
#include "ma/internal/utils/threading/ma_thread.h"
#include "ma/internal/utils/threading/ma_atomic.h"

MA_CPP(extern "C" {)

/* 
When an event is added event_message, all infos are polulated into table as 
mentioned below keywords and then added to event message payload.

key = MA_EVENT_KEY_EVENT_ID			value = int
key = MA_EVENT_KEY_EVENT_SEVERITY   value = int
key = MA_EVENT_EVENT_GMTTIME_KEY    value = string
key = MA_EVENT_EVENT_LOCALTIME_KEY  value = string
key = MA_EVENT_KEY_COMMON_FIELDS	value = table
key = MA_EVENT_KEY_CUSTOM_TARGET	value = string
key = MA_EVENT_KEY_CUSTOM_FIELDS	value = table  
*/

struct ma_event_s{		
	ma_uint32_t				eventid;            
	ma_event_severity_t		severity;            
	ma_time_t				gmttime;
	ma_time_t				localtime;
	ma_table_t				*common_fileds;      
	char					*custom_targetname;  
	ma_table_t				*custom_fileds;	  	

	ma_atomic_counter_t		ref_count;
};

/* 
When event_message is sent, event_message will populate all the elements in the table as below 
and set in the message payload.

key = MA_EVENT_BAG_PRODUCT_NAME_KEY			value = string
key = MA_EVENT_BAG_PRODUCT_VERSION_KEY		value = string
key = MA_EVENT_BAG_PRODUCT_FAMILY_KEY		value = string
key = MA_EVENT_BAG_COMMON_FIELDS_KEY		value = table
key = MA_EVENT_BAG_CUSTOM_TARGET_KEY		value = string
key = MA_EVENT_BAG_CUSTOM_FIELDS_KEY		value = table
key = MA_EVENT_BAG_PRODUCT_EVENTS_KEY		value = array	
*/

struct ma_event_bag_s{
	char				*product_name;        
	char				*product_version;     
	char				*product_family;      
	ma_table_t			*common_fileds;       
	char				*custom_targetname;   
	ma_table_t			*custom_fileds;	     
	ma_array_t			*product_events;   
	ma_client_base_t	*evt_client;
	ma_atomic_counter_t	 ref_count;
};

ma_error_t event_to_variant(ma_event_t *maevent, ma_variant_t **event_variant);
ma_error_t variant_to_event(ma_variant_t *event_variant, ma_event_t **maevent);
ma_error_t event_bag_to_variant(ma_event_bag_t *self, ma_variant_t **payload);
ma_error_t variant_to_event_bag(ma_variant_t *payload, ma_event_bag_t **bag);

MA_CPP(})


#endif /*MA_EVENT_COMMON_H_INCLUDED*/

