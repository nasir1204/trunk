#include "ma/scheduler/ma_scheduler.h"
#include "ma/internal/defs/ma_scheduler_defs.h"
#include "ma/internal/clients/ma/ma_client_base.h"
#include "ma/internal/clients/ma/ma_client_internal.h"
#include "ma/internal/clients/ma/client/ma_client_manager.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"
#include "ma/internal/ma_macros.h"
#include "ma/ma_message.h"
#include "ma/ma_log.h"
#include "ma/scheduler/ma_enumerator.h"
#include "ma_scheduler_client_private.h"
#include "ma/internal/defs/ma_object_defs.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

ma_logger_t *client_logger = NULL;
extern const char *gexec_status_str[];

typedef struct ma_scheduler_client_s ma_scheduler_client_t, *ma_scheduler_client_h;
typedef struct ma_product_s ma_product_t;

struct ma_product_s {
    char id[MA_MAX_NAME];
    char task_type[MA_MAX_NAME];
    ma_scheduler_task_callbacks_t cb;
    void *cb_data;
};

struct ma_scheduler_client_s {
    ma_client_base_t        base_client;
    ma_enumerator_t         enumerator;
    ma_client_t             *ma_client;
    ma_product_t            *group;
    size_t                  no_of_products;
    size_t                  no_of_products_visited;
    size_t                  capacity;
};

static ma_error_t ma_scheduler_client_create(ma_client_t *ma_client, ma_client_base_t **client);
static ma_error_t ma_scheduler_client_release(ma_client_base_t *client);

static ma_error_t current(ma_enumerator_t *enumerator, void *cur);
static ma_error_t move_next(ma_enumerator_t *enumerator, void *next);
static void reset(ma_enumerator_t *enumerator);

static ma_error_t add_product(ma_scheduler_client_t *scheduler, const char *id, const char *type, ma_scheduler_task_callbacks_t const *cb, void *cb_data);
static ma_error_t find_product(ma_scheduler_client_t *scheduler, const char *id, const char *type, ma_product_t **product);
static ma_error_t remove_product(ma_scheduler_client_t *scheduler, const char *id, const char *type);

static ma_error_t ma_scheduler_client_create(ma_client_t *ma_client, ma_client_base_t **scheduler_client);
static ma_error_t ma_scheduler_client_on_message(ma_client_base_t *client, const char *topic, ma_message_t *message);
static ma_error_t ma_scheduler_client_release(ma_client_base_t *client);

static const ma_client_base_methods_t scheduler_client_methods = {
    &ma_scheduler_client_on_message,
    &ma_scheduler_client_release
};


ma_error_t add_product(ma_scheduler_client_t *scheduler, const char *id, const char *type, ma_scheduler_task_callbacks_t const *cb, void *cb_data) {
    if(scheduler && id && type && cb) {
        ma_error_t err = MA_ERROR_SCHEDULER_CALLBACKS_ALREADY_REGISTERED;
        ma_product_t *product = NULL;

        if(MA_OK == find_product(scheduler, id, type, &product)) {
            if(!product) {
                if(scheduler->capacity) {
                    --scheduler->capacity;
                } else {
                    if(!(scheduler->group = (ma_product_t*)realloc(scheduler->group, sizeof(ma_product_t) * (scheduler->no_of_products + 1)))) {
                        scheduler->no_of_products = 0; scheduler->no_of_products_visited = 0;
                        MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "scheduler product group allocation failed. last error(%d)", err);
                        return MA_ERROR_OUTOFMEMORY;
                    }
                }
                if(strlen(id) < MA_MAX_LEN) {
                    memset(&scheduler->group[scheduler->no_of_products].id, 0, MA_MAX_LEN);
                    strncpy(scheduler->group[scheduler->no_of_products].id, id, strlen(id));
                }
                if(strlen(type) < MA_MAX_LEN) {
                    memset(&scheduler->group[scheduler->no_of_products].task_type, 0, MA_MAX_LEN);
                    strncpy(scheduler->group[scheduler->no_of_products].task_type, type, strlen(type));
                }
                scheduler->group[scheduler->no_of_products].cb.on_execute = cb->on_execute ? cb->on_execute : NULL;
                scheduler->group[scheduler->no_of_products].cb.on_remove = cb->on_remove ? cb->on_remove : NULL;
                scheduler->group[scheduler->no_of_products].cb.on_stop = cb->on_stop ? cb->on_stop : NULL;
                scheduler->group[scheduler->no_of_products].cb.on_update_status = cb->on_update_status ? cb->on_update_status : NULL;
                scheduler->group[scheduler->no_of_products].cb_data = cb_data; 
                scheduler->no_of_products++; err = MA_OK;
                MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "product id(%s) added successfully", id);
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t find_product(ma_scheduler_client_t *scheduler, const char *id, const char *type, ma_product_t **product) {
    if(scheduler && id && type && product) {
        ma_error_t err = MA_OK;
        ma_enumerator_t *enumerator = (ma_enumerator_t*)&scheduler->enumerator;

        *product = NULL;
        ma_enumerator_reset(enumerator);
        while(MA_OK == ma_enumerator_next(enumerator, product)) {
            if(!strncmp((*product)->id, id, strlen(id)) && !strncmp((*product)->task_type, type, strlen(type))) {
                MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "product id(%s) exist", (*product)->id); break;
            }
            *product = NULL;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t remove_product(ma_scheduler_client_t *scheduler, const char *id, const char *type) {
    if(scheduler && id && type) {
        ma_error_t err = MA_ERROR_OBJECTNOTFOUND;
        size_t itr = 0;

        for(itr = 0; itr < scheduler->no_of_products; ++itr) {
            if(!strncmp(scheduler->group[itr].id, id, strlen(id)) && !strncmp(scheduler->group[itr].task_type, type, strlen(type))) {
                MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "product id(%s) removed successfully", id);
                if(scheduler->no_of_products != (itr+1)) {
                    memcpy(scheduler->group+itr, scheduler->group+(itr+1), (sizeof(ma_product_t)*(scheduler->no_of_products - (itr+1))));
                }
                --scheduler->no_of_products;
                ++scheduler->capacity; err = MA_OK;
                break;
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_scheduler_register_task_callbacks(ma_client_t *ma_client, const char *id, const char *type, ma_scheduler_task_callbacks_t const *cb, void *cb_data) {
    if(ma_client && cb) {
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;
        ma_client_get_context(ma_client, &ma_context);
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);		
        client_logger = MA_CONTEXT_GET_LOGGER(ma_context);
        if(ma_context && client_manager) {
            ma_error_t err = MA_OK;
            ma_client_base_t *client = NULL;

            if(MA_OK != (err = ma_client_manager_get_client(client_manager, MA_SCHEDULER_CLIENT_NAME_STR, &client))) {
                MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "scheduler client does not exist..creating");
                if(MA_OK == (err = ma_scheduler_client_create(ma_client, &client))) {
                    MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "scheduler client create successfully");
                    if(MA_OK != (err = ma_client_manager_add_client(client_manager,MA_SCHEDULER_CLIENT_NAME_STR,client))) {
                        MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "scheduler client add to client controller failed. last error(%d)", err);
                        (void)ma_scheduler_client_release(client);
                    }
                }
            }
            if((MA_OK == err) && client) {
                err = MA_ERROR_SCHEDULER_NO_CALLBACK;
                if(cb->on_execute || cb->on_stop || cb->on_remove) {
                    if(MA_OK != (err = add_product((ma_scheduler_client_t*)client, id = id? id :MA_CONTEXT_GET_PRODUCT_ID(ma_context), type = type? type : MA_TASK_DEFAULT_TASK_TYPE_STR, cb, cb_data))) {
                        MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "product id(%s) task type(%s). last error(%d)", id, type, err);
                    }
                } else {
                    MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "callback does not exist. last error(%d)", err);
                }
            }

            return err;
        }
        return MA_ERROR_INVALID_CLIENT_OBJECT;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_scheduler_unregister_task_callbacks(ma_client_t *ma_client, const char *product_id, const char *task_type) {
    if(ma_client) {
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;
        ma_client_get_context(ma_client, &ma_context);
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);		
        client_logger = MA_CONTEXT_GET_LOGGER(ma_context);
        if(ma_context && client_manager) {
            ma_error_t err = MA_ERROR_SCHEDULER_CALLBACKS_NOT_REGISTERED;
            ma_client_base_t *client = NULL;

            if(MA_OK == (err = ma_client_manager_get_client(client_manager, MA_SCHEDULER_CLIENT_NAME_STR, &client))) {
                if(MA_OK == (err = remove_product((ma_scheduler_client_t*)client, product_id ? product_id:MA_CONTEXT_GET_PRODUCT_ID(ma_context), task_type ? task_type : MA_TASK_DEFAULT_TASK_TYPE_STR))) {
                    if(!((ma_scheduler_client_t*)client)->no_of_products) {
                        MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "releasing scheduler client");
                        if(MA_OK != ma_client_manager_remove_client(client_manager,client)) {
                            MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "removing scheduler client from client controller failed");
                        }
                        if(MA_OK != ma_client_base_release(client)) {
                            MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "scheduler client release failed");
                        }
                    }
                }
            }

            return err;
        }
        return MA_ERROR_INVALID_CLIENT_OBJECT;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_scheduler_get_version(ma_uint32_t *version) {
    if(version) {
        *version = 0x05020100; //TODO fetch version 
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_scheduler_add_task(ma_client_t *ma_client, ma_task_t *task) {
    if(ma_client && task) {
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;
        ma_msgbus_t *msgbus = NULL;
        ma_client_get_context(ma_client, &ma_context);
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);
        msgbus = MA_CONTEXT_GET_MSGBUS(ma_context);
        client_logger = MA_CONTEXT_GET_LOGGER(ma_context);
        if(ma_context && client_manager && msgbus) {
            ma_error_t err = MA_OK;
            ma_msgbus_endpoint_t *endpoint = NULL;
            ma_message_t *request = NULL;
            ma_message_t *response = NULL;
            const char *task_id = NULL;
            const char *task_name = NULL;
            const char *task_type = NULL;
            ma_variant_t *task_variant = NULL;
            const char *reply_status = NULL;

            if((MA_OK == (err = ma_task_get_id(task, &task_id))) && (MA_OK == (err = ma_task_get_name(task, &task_name))) && (MA_OK == (err = ma_task_get_type(task, &task_type)))) {
                MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "scheduler adding task_id(%s) task_name(%s) task_type(%s)", task_id, task_name, task_type);
                if(MA_OK == (err = convert_task_to_variant(task, &task_variant))) {
                    if(MA_OK == (err = ma_message_create(&request))) {
                        if(MA_OK == (err = ma_msgbus_endpoint_create(msgbus, MA_SCHEDULER_SERVICE_NAME_STR, NULL, &endpoint))) {
                            ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;

                            (void) ma_client_manager_get_thread_option(client_manager, &thread_option);
                            ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);                        
                            if(MA_OK == (err = ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC))) {
                                if((MA_OK == (err = ma_message_set_property(request, MA_SCHEDULER_SERVICE_MSG_TYPE, MA_TASK_MSG_TYPE_ADD))) &&
                                    (MA_OK == (err = ma_message_set_property(request, MA_TASK_ID, task_id))) &&
                                    (MA_OK == (err = ma_message_set_property(request, MA_TASK_NAME, task_name))) &&
                                    (MA_OK == (err = ma_message_set_property(request, MA_TASK_TYPE, task_type))) &&
                                    (MA_OK == (err = ma_message_set_payload(request, task_variant))) ) {
                                        MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "scheduler add task id(%s) request sent to scheduler service", task_id);
                                        if(MA_OK == (err = ma_msgbus_send(endpoint, request, &response))) {
                                            if(MA_OK == (err = ma_message_get_property(response, MA_TASK_STATUS, &reply_status))) {
                                                if(!strcmp(reply_status, MA_TASK_REPLY_STATUS_SUCCESS)) {
                                                    MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "scheduler add task id(%s) reply status(%s)", task_id, reply_status);
                                                } else {
                                                    const char *error_code = NULL;

                                                    err = (MA_OK == (err = ma_message_get_property(response, MA_ERROR_CODE, &error_code)))
                                                            ? (ma_error_t)atoi(error_code)
                                                            : MA_ERROR_APIFAILED;
                                                    MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "scheduler add task id(%s) reply status(%s) last error(%d)", task_id, reply_status, err);
                                                }
                                            }
                                        } else {
                                            MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "scheduler add task id(%s) request sent to scheduler service failed. last error(%d)", task_id, err);
                                        }
                                }
                            }
                            (void)ma_msgbus_endpoint_release(endpoint);
                        }
                        (void)ma_message_release(request);
                    }
                    (void)ma_variant_release(task_variant);
                } else {
                    MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "scheduler add task id(%s) message conversion failed, last error(%d)", task_id, err);
                }
            }

            return err;
        }
        return MA_ERROR_INVALID_CLIENT_OBJECT;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_scheduler_remove_task(ma_client_t *ma_client, const char *task_id) {
    if(ma_client && task_id && 0 < strlen(task_id)) {
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;
        ma_msgbus_t *msgbus = NULL;
        ma_client_get_context(ma_client, &ma_context);
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);
        msgbus = MA_CONTEXT_GET_MSGBUS(ma_context);
        client_logger = MA_CONTEXT_GET_LOGGER(ma_context);

        if(ma_context && client_manager && msgbus) {
            ma_error_t err = MA_OK;
            ma_msgbus_endpoint_t *endpoint = NULL;
            ma_message_t *request = NULL;
            ma_message_t *response = NULL;
            const char *reply_status = NULL;

            if(MA_OK == (err = ma_message_create(&request))) {
                if(MA_OK == (err = ma_msgbus_endpoint_create(msgbus, MA_SCHEDULER_SERVICE_NAME_STR, NULL, &endpoint))) {
                    ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;

                    (void) ma_client_manager_get_thread_option(client_manager, &thread_option);
                    ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);            
                    if(MA_OK == (err = ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC))) {
                        if(MA_OK == (err = ma_message_set_property(request, MA_SCHEDULER_SERVICE_MSG_TYPE, MA_TASK_MSG_TYPE_DELETE))) {
                            if(MA_OK == (err = ma_message_set_property(request, MA_TASK_ID, task_id))) {
                                MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "scheduler remove task id(%s) request sent to scheduler service", task_id);
                                if(MA_OK == (err = ma_msgbus_send(endpoint, request, &response))) {
                                    if(MA_OK == (err = ma_message_get_property(response, MA_TASK_STATUS, &reply_status))) {
                                        if(!strcmp(reply_status, MA_TASK_REPLY_STATUS_SUCCESS)) {
                                            MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "scheduler remove task id(%s) reply status(%s), last error(%d)", task_id, reply_status, err);
                                        } else {
                                            const char *error_code = NULL;

                                            err = (MA_OK == (err = ma_message_get_property(response, MA_ERROR_CODE, &error_code)))
                                                    ? (ma_error_t)atoi(error_code)
                                                    : MA_ERROR_APIFAILED;
                                            MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "scheduler remove task id(%s) reply status(%s) last error(%d)", task_id, reply_status, err);
                                        }
                                    }
                                } else {
                                    MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "scheduler remove task id(%s) request sent to scheduler service failed. last error(%d)", task_id, err);
                                }
                            }
                        }
                    }
                    (void)ma_msgbus_endpoint_release(endpoint);
                }
                (void)ma_message_release(request);
            }

            return err;
        }
        return MA_ERROR_INVALID_CLIENT_OBJECT;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_scheduler_alter_task(ma_client_t *ma_client, ma_task_t *task) {
    if(ma_client && task) {
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;
        ma_msgbus_t *msgbus = NULL;
        ma_client_get_context(ma_client, &ma_context);
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);
        msgbus = MA_CONTEXT_GET_MSGBUS(ma_context);
        client_logger = MA_CONTEXT_GET_LOGGER(ma_context);

        if(ma_context && client_manager && msgbus) {
            ma_error_t err = MA_OK;
            ma_msgbus_endpoint_t *endpoint = NULL;
            ma_message_t *request = NULL;
            ma_message_t *response = NULL;
            const char *task_id = NULL;
            const char *task_name = NULL;
            const char *task_type = NULL;
            ma_variant_t *task_variant = NULL;
            const char *reply_status = NULL;

            if((MA_OK == (err = ma_task_get_id(task, &task_id))) && (MA_OK == (err = ma_task_get_name(task, &task_name))) && (MA_OK == (err = ma_task_get_type(task, &task_type)))) {
                MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "modifying task_id(%s) task_name(%s) task_type(%s)", task_id, task_name, task_type);
                if(MA_OK == (err = convert_task_to_variant(task, &task_variant))) {
                    if(MA_OK == (err = ma_message_create(&request))) {
                        if(MA_OK == (err = ma_msgbus_endpoint_create(msgbus, MA_SCHEDULER_SERVICE_NAME_STR, NULL, &endpoint))) {
                            ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;

                            (void) ma_client_manager_get_thread_option(client_manager, &thread_option);
                            ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);            
                            if(MA_OK == (err = ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC)))  {
                                if((MA_OK == (err = ma_message_set_property(request, MA_SCHEDULER_SERVICE_MSG_TYPE, MA_TASK_MSG_TYPE_MODIFY))) &&
                                    (MA_OK == (err = ma_message_set_property(request, MA_TASK_ID, task_id))) &&
                                    (MA_OK == (err = ma_message_set_property(request, MA_TASK_NAME, task_name))) &&
                                    (MA_OK == (err = ma_message_set_property(request, MA_TASK_TYPE, task_type))) &&
                                    (MA_OK == (err = ma_message_set_payload(request, task_variant))) ) {
                                        MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "alter task id(%s) request sent to scheduler service", task_id);
                                        if(MA_OK == (err = ma_msgbus_send(endpoint, request, &response))) {
                                            if(MA_OK == (err = ma_message_get_property(response, MA_TASK_STATUS, &reply_status))) {
                                                if(!strcmp(reply_status, MA_TASK_REPLY_STATUS_SUCCESS)) {
                                                    MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "alter task id(%s) reply status(%s)", task_id, reply_status);
                                                } else {
                                                    const char *error_code = NULL;

                                                    err = (MA_OK == (err = ma_message_get_property(response, MA_ERROR_CODE, &error_code)))
                                                          ? (ma_error_t)atoi(error_code)
                                                          : MA_ERROR_APIFAILED;
                                                    MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "alter task id(%s) reply status(%s) last error(%d)", task_id, reply_status, err);
                                                }
                                            }
                                        } else {
                                            MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "alter task id(%s) request sent to scheduler service failed. last error(%d)", task_id, err);
                                        }
                                }
                            }
                            (void)ma_msgbus_endpoint_release(endpoint);
                        }
                        (void)ma_message_release(request);
                    }
                    (void)ma_variant_release(task_variant);
                } else {
                    MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "convert task id(%s) to variant failed. last error(%d)", task_id, err);
                }
            }

            return err;
        }
        return MA_ERROR_INVALID_CLIENT_OBJECT;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_scheduler_get_task(ma_client_t *ma_client, const char *task_id, ma_task_t **task) {    
    if(ma_client && task_id && 0 < strlen(task_id) && task) {
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;
        ma_msgbus_t *msgbus = NULL;
        ma_client_get_context(ma_client, &ma_context);
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);
        msgbus = MA_CONTEXT_GET_MSGBUS(ma_context);
        //client_logger = MA_CONTEXT_GET_LOGGER(ma_context);

        if(ma_context && client_manager && msgbus) {
            ma_error_t err = MA_OK;
            ma_msgbus_endpoint_t *endpoint = NULL;
            ma_message_t *request = NULL;
            ma_message_t *response = NULL;
            const char *reply_status = NULL;
            ma_variant_t *task_variant = NULL;
            ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;

            err = ma_message_create(&request);
            if(MA_OK == err) err = ma_msgbus_endpoint_create(msgbus, MA_SCHEDULER_SERVICE_NAME_STR, NULL, &endpoint);        
            if(MA_OK == err) err = ma_client_manager_get_thread_option(client_manager, &thread_option);
            if(MA_OK == err) err = ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);            
            if(MA_OK == err) err = ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
            if(MA_OK == err) err = ma_message_set_property(request, MA_SCHEDULER_SERVICE_MSG_TYPE, MA_TASK_MSG_TYPE_GET);
            if(MA_OK == err) err = ma_message_set_property(request, MA_TASK_ID, task_id);
            if(MA_OK == err) err = ma_msgbus_send(endpoint, request, &response);
            if(MA_OK == err) err = ma_message_get_property(response, MA_TASK_STATUS, &reply_status);
            if(MA_OK == err) {
                if(!strcmp(reply_status, MA_TASK_REPLY_STATUS_SUCCESS)) {
                    if(MA_OK == (err = ma_message_get_payload(response, &task_variant))) {
                        if(MA_OK == (err = ma_task_create(task_id, task))) {
                            (void)convert_variant_to_task(task_variant, *task);
                        }
                        (void)ma_variant_release(task_variant);
                    }
                } else {
                    const char *error_code = NULL;

                    err = (MA_OK == (err = ma_message_get_property(response, MA_ERROR_CODE, &error_code)))
                          ? (ma_error_t)atoi(error_code)
                          : MA_ERROR_APIFAILED;
                    MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "scheduler get task id(%s) reply status(%s) last error(%d)", task_id, reply_status, err);
                }
            }
            // clean-up
            if(request) ma_message_release(request);
            if(endpoint) ma_msgbus_endpoint_release(endpoint);
            
            return MA_OK;
        }
        return MA_ERROR_INVALID_CLIENT_OBJECT;
    }
    return MA_ERROR_INVALIDARG;
}
ma_error_t ma_scheduler_enumerate_task(ma_client_t *ma_client, const char *product_id, const char *creator_id, const char *task_type, ma_task_t ***task_set, size_t *no_of_tasks) {
    if(ma_client && task_set && no_of_tasks) {
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;
        ma_msgbus_t *msgbus = NULL;
        ma_client_get_context(ma_client, &ma_context);
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);
        msgbus = MA_CONTEXT_GET_MSGBUS(ma_context);
        client_logger = MA_CONTEXT_GET_LOGGER(ma_context);
        if(ma_context && client_manager && msgbus) {
            ma_error_t err = MA_OK;
            ma_msgbus_endpoint_t *endpoint = NULL;
            ma_message_t *request = NULL;
            ma_message_t *response = NULL;
            const char *reply_status = NULL;
            ma_variant_t *enum_var = NULL;

            if(MA_OK == (err = ma_message_create(&request))) {
                if(MA_OK == (err = ma_msgbus_endpoint_create(msgbus, MA_SCHEDULER_SERVICE_NAME_STR, NULL, &endpoint))) {
                    ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;

                    (void) ma_client_manager_get_thread_option(client_manager, &thread_option);
                    ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);           
                    if(MA_OK == (err = ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC))) {
                        if((MA_OK == (err = ma_message_set_property(request, MA_SCHEDULER_SERVICE_MSG_TYPE, MA_TASK_ENUMERATE))) &&
                           (MA_OK == (err = ma_message_set_property(request, MA_TASK_CREATOR_ID, creator_id ? creator_id : MA_EMPTY))) &&
                           (MA_OK == (err = ma_message_set_property(request, MA_TASK_PRODUCT_ID, product_id ? product_id : MA_EMPTY))) &&
                           (MA_OK == (err = ma_message_set_property(request, MA_TASK_TYPE, task_type ? task_type : MA_EMPTY)))
                          ) {
                              if(MA_OK == (err = ma_msgbus_send(endpoint, request, &response))) {
                                  if(MA_OK == (err = ma_message_get_property(response, MA_TASK_STATUS, &reply_status))) {
                                      if(!strcmp(reply_status, MA_TASK_REPLY_STATUS_SUCCESS)) {
                                        if(MA_OK == (err = ma_message_get_payload(response, &enum_var))) {
                                            ma_array_t *task_array = NULL;
                                            if(MA_OK == (err = ma_variant_get_array(enum_var, &task_array))) {
                                                if((MA_OK == (err = ma_array_size(task_array, no_of_tasks))) && *no_of_tasks) {
                                                    err = MA_ERROR_OUTOFMEMORY;
                                                    if(((*task_set) = (ma_task_t**)calloc(*no_of_tasks, sizeof(ma_task_t*)))) {
                                                        size_t itr = 0;

                                                        for(itr = 1; itr <= *no_of_tasks; ++itr) {
                                                            ma_variant_t *task_var = NULL;

                                                            if(MA_OK == (err = ma_array_get_element_at(task_array, itr, &task_var))) {
                                                                if(MA_OK == (err = ma_task_create(MA_EMPTY, &(*task_set)[itr-1]))) {
                                                                    if(MA_OK == (err = convert_variant_to_task(task_var, (*task_set)[itr-1]))) {
                                                                        const char *enum_task_id = NULL;

                                                                        (void)ma_task_get_id((*task_set)[itr-1], &enum_task_id);
                                                                        MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "scheduler client enumerated task id(%s) count(%d)", enum_task_id, itr);
                                                                    }
                                                                }
                                                                (void)ma_variant_release(task_var);
                                                            }
                                                        }
                                                    }
                                                }
                                                (void)ma_array_release(task_array);
                                            }
                                            (void)ma_variant_release(enum_var);
                                        }
                                      } else {
                                        const char *error_code = NULL;

                                        err = (MA_OK == (err = ma_message_get_property(response, MA_ERROR_CODE, &error_code)))
                                              ? (ma_error_t)atoi(error_code)
                                              : MA_ERROR_APIFAILED;
                                          MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "scheduler enumerate task reply failed, last error(%d)", err);
                                      }
                                  }
                              }
                        }
                    }
                    (void)ma_msgbus_endpoint_release(endpoint);
                }
                (void)ma_message_release(request);
            }

            return err;
        }
        return MA_ERROR_INVALID_CLIENT_OBJECT;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_scheduler_update_task_status(ma_client_t *ma_client, const char *task_id, ma_task_exec_status_t task_status) {
    if(ma_client && task_id && 0 < strlen(task_id)) {
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;
        ma_msgbus_t *msgbus = NULL;
        ma_client_get_context(ma_client, &ma_context);
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);
        msgbus = MA_CONTEXT_GET_MSGBUS(ma_context);
        client_logger = MA_CONTEXT_GET_LOGGER(ma_context);
        if(ma_context && client_manager && msgbus) {
            ma_error_t err = MA_OK;
            ma_msgbus_endpoint_t *endpoint = NULL;
            ma_message_t *request = NULL;
            ma_message_t *response = NULL;
            const char *reply_status = NULL;
            ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;
        
            err = ma_message_create(&request);
            if(MA_OK == err) err = ma_msgbus_endpoint_create(msgbus, MA_SCHEDULER_SERVICE_NAME_STR, NULL, &endpoint);
            if(MA_OK == err) err = ma_client_manager_get_thread_option(client_manager, &thread_option);
            if(MA_OK == err) err = ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);           
            if(MA_OK == err) err = ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
            if(MA_OK == err) err = ma_message_set_property(request, MA_SCHEDULER_SERVICE_MSG_TYPE, MA_TASK_MSG_TYPE_UPDATE);
            if(MA_OK == err) err = ma_message_set_property(request, MA_TASK_ID, task_id);
            if(MA_OK == err) err = ma_message_set_property(request, MA_TASK_STATUS, gexec_status_str[task_status - 1]);

            if(MA_OK == err){
                MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "task id(%s) sending status (%s) update message", task_id,  gexec_status_str[task_status - 1]);
                if(MA_OK == (err = ma_msgbus_send(endpoint, request, &response))) {
                    if(MA_OK == (err = ma_message_get_property(response, MA_TASK_STATUS, &reply_status))) {
                        if(!strcmp(reply_status, MA_TASK_REPLY_STATUS_SUCCESS)) {
                            MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "task id(%s) status updation successful", task_id);
                        } else {
                            const char *error_code = NULL;

                            err = (MA_OK == (err = ma_message_get_property(response, MA_ERROR_CODE, &error_code)))
                                  ? (ma_error_t)atoi(error_code)
                                  : MA_ERROR_APIFAILED;
                            MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "task id(%s) updation status failed, last error(%d)", task_id, err); 
                        }
                    }
                }
            }
            // clean-up
            if(request) ma_message_release(request);
            if(endpoint) ma_msgbus_endpoint_release(endpoint);

            return err;
        }
        return MA_ERROR_INVALID_CLIENT_OBJECT;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_scheduler_postpone_task(ma_client_t *ma_client, const char *task_id, size_t intervals) {
    if(ma_client && task_id && 0 < strlen(task_id)) {
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;
        ma_msgbus_t *msgbus = NULL;
        ma_client_get_context(ma_client, &ma_context);
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);
        msgbus = MA_CONTEXT_GET_MSGBUS(ma_context);
        //client_logger = MA_CONTEXT_GET_LOGGER(ma_context);

        if(ma_context && client_manager && msgbus) {
            ma_error_t err = MA_OK;
            ma_msgbus_endpoint_t *endpoint = NULL;
            ma_message_t *request = NULL;
            ma_message_t *response = NULL;
            const char *reply_status = NULL;
            ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;
            char post_pone_buf[MA_MAX_LEN] = {0};
        
            MA_MSC_SELECT(_snprintf, snprintf)(post_pone_buf, MA_MAX_LEN, "%u", intervals);
            err = ma_message_create(&request);
            if(MA_OK == err) err = ma_msgbus_endpoint_create(msgbus, MA_SCHEDULER_SERVICE_NAME_STR, NULL, &endpoint);
            if(MA_OK == err) err = ma_client_manager_get_thread_option(client_manager, &thread_option);
            if(MA_OK == err) err = ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);           
            if(MA_OK == err) err = ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
            if(MA_OK == err) err = ma_message_set_property(request, MA_SCHEDULER_SERVICE_MSG_TYPE, MA_TASK_MSG_TYPE_POSTPONE);
            if(MA_OK == err) err = ma_message_set_property(request, MA_TASK_ID, task_id);
            if(MA_OK == err) err = ma_message_set_property(request, MA_TASK_POSTPONE_INTERVALS, post_pone_buf);

            if(MA_OK == err){
                if(MA_OK == (err = ma_msgbus_send(endpoint, request, &response))) {
                    if(MA_OK == (err = ma_message_get_property(response, MA_TASK_STATUS, &reply_status))) {
                        if(!strcmp(reply_status, MA_TASK_REPLY_STATUS_SUCCESS)) {
                            MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "scheduler task id(%s) updating postpone intervals successful", task_id);
                        } else {
                            const char *error_code = NULL;

                            err = (MA_OK == (err = ma_message_get_property(response, MA_ERROR_CODE, &error_code)))
                                  ? (ma_error_t)atoi(error_code)
                                  : MA_ERROR_APIFAILED;
                            MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "scheduler task id(%s) updating postpone intervals response, last error(%d)", task_id, err); 
                        }
                    }
                }
            }
            // clean-up
            if(request) ma_message_release(request);
            if(endpoint) ma_msgbus_endpoint_release(endpoint);

            return err;
        }
        return MA_ERROR_INVALID_CLIENT_OBJECT;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_scheduler_client_on_message(ma_client_base_t *client, const char *topic, ma_message_t *recv_msg) {
    if(client && topic && recv_msg) {
        ma_error_t err = MA_OK;
        ma_bool_t is_task_execute = MA_FALSE, is_task_stop = MA_FALSE, is_task_remove = MA_FALSE, is_task_update_status = MA_FALSE;

        if((is_task_execute = (0 == strcmp(MA_TASK_EXECUTE_PUBSUB_TOPIC , topic))) 
            || (is_task_stop = (0 == strcmp(MA_TASK_STOP_PUBSUB_TOPIC, topic)))
            || (is_task_remove = (0 == strcmp(MA_TASK_REMOVE_PUBSUB_TOPIC , topic)))
            || (is_task_update_status = (0 == strcmp(MA_TASK_UPDATE_STATUS_PUBSUB_TOPIC, topic))) ) {             
            ma_scheduler_client_t *scheduler = (ma_scheduler_client_t*)client;
            const char *id = NULL;
            const char *type = NULL;
            
            if(MA_OK == (err = ma_message_get_property(recv_msg, MA_TASK_TYPE, &type))) {
                if(MA_OK == (err = ma_message_get_property(recv_msg, MA_TASK_PRODUCT_ID, &id))) {
                    ma_product_t *product = NULL;
                    ma_enumerator_t *enumerator = (ma_enumerator_t*)&scheduler->enumerator;
                    ma_enumerator_reset(enumerator);
                    while(MA_OK == ma_enumerator_next(enumerator, &product)) {
                        if(!strcmp(id, product->id) && (!strcmp(MA_TASK_DEFAULT_TASK_TYPE_STR, product->task_type) || !strcmp(type, product->task_type))) {
                            ma_task_t *task = NULL;
                            ma_variant_t *task_variant = NULL;
                            const char *task_id = NULL;
                            const char *task_name = NULL;

                            if(MA_OK == (err = ma_message_get_property(recv_msg, MA_TASK_ID, &task_id))) {
                                MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "product id(%s) received task id(%s)", id, task_id);
                                if(MA_OK == (err = ma_message_get_property(recv_msg, MA_TASK_NAME, &task_name))) {
                                    MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "product id(%s) received task id(%s) task name(%s)", id, task_id, task_name);
                                    if(MA_OK == (err =  ma_message_get_payload(recv_msg, &task_variant))) {
                                        if((MA_OK == (err = ma_task_create(task_id, &task))) && task) {
                                            if(MA_OK == (err = convert_variant_to_task(task_variant, task))) {
                                                if(is_task_execute && product->cb.on_execute) {
                                                    MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "calling task id(%s) execute call back for product id(%s)", task_id, id);
                                                    if(MA_OK != (err = product->cb.on_execute(scheduler->ma_client, id, type, task, product->cb_data))) {
                                                        MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "product id(%s) task id(%s) execute callback failed. last error(%d)", id, task_id, err);
                                                    }
                                                } else if(is_task_stop && product->cb.on_stop) {
                                                    MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "calling task id(%s) stop call back for product id(%s)", task_id, id);
                                                    if(MA_OK != (err = product->cb.on_stop(scheduler->ma_client, id, type, task, product->cb_data))) {
                                                        MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "product id(%s) task id(%s) stop callback failed. last error(%d)", id, task_id, err);
                                                    }
                                                } else if(is_task_remove && product->cb.on_remove) {
                                                    MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "calling task id(%s) remove call back for product id(%s)", task_id, id);
                                                    if(MA_OK != (err = product->cb.on_remove(scheduler->ma_client, id, type, task, product->cb_data))) {
                                                        MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "product id(%s) task id(%s) remove callback failed. last error(%d)", id, task_id, err);
                                                    }
                                                } else if(is_task_update_status && product->cb.on_update_status) {
                                                    MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "calling task id(%s) update status call back for product id(%s)", task_id, id);
                                                    if(MA_OK != (err = product->cb.on_update_status(scheduler->ma_client, id, type, task, product->cb_data))) {
                                                        MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "product id(%s) task id(%s) update status callback failed. last error(%d)", id, task_id, err);
                                                    }
                                                } else {
                                                    /*It should not come here */
                                                    err = MA_ERROR_UNEXPECTED;
                                                }
                                            } else {
                                                MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "product id(%s) task id(%s) conversion variant to task failed. last error(%d)", id, task_id, err);
                                            }
                                            (void)ma_task_release(task);
                                        } else {
                                            MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "product id(%s) task id(%s) client task creation failed. last error(%d)", id, task_id, err);
                                        }
                                        (void)ma_variant_release(task_variant);
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static const ma_enumerator_methods_t methods = {
    &current,
    &move_next,
    &reset
};

ma_error_t ma_scheduler_client_create(ma_client_t *ma_client, ma_client_base_t **client) {
    if(ma_client && client) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;
        ma_scheduler_client_t *scheduler = NULL;

        if((scheduler = (ma_scheduler_client_t*)calloc(1, sizeof(ma_scheduler_client_t)))) {  
            ma_context_t *ma_context = NULL;       
            ma_client_get_context(ma_client, &ma_context);
            ma_client_base_init(*client = (ma_client_base_t*)scheduler, &scheduler_client_methods, scheduler);
            ma_enumerator_init((ma_enumerator_t*)&scheduler->enumerator, &methods, scheduler); err = MA_OK;
            client_logger = MA_CONTEXT_GET_LOGGER(ma_context);
            scheduler->ma_client = ma_client;
            MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "scheduler client created successfully");
        } else {
            MA_LOG(client_logger, MA_LOG_SEV_ERROR, "scheduler client creation failed, last error(%d)", err);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_scheduler_client_release(ma_client_base_t *client) {
    if(client) {
        ma_scheduler_client_t *scheduler = (ma_scheduler_client_t *)client;
        
        free(scheduler->group);
        free(scheduler);
        MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "scheduler client released");

        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_scheduler_enumerate_task_release(ma_task_t **set, size_t *count) {
    if(set && count) {
        if(count) {
            size_t itr = 0;
            for(itr = 0; itr < *count; ++itr) {
                (void)ma_task_release(set[itr]);
            }
        }
        free(set);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

// enumerator methods
ma_error_t current(ma_enumerator_t *enumerator, void *cur) {
    if(enumerator && cur) {
        ma_error_t err = MA_ERROR_OUTOFBOUNDS;
        ma_scheduler_client_t *scheduler = (ma_scheduler_client_t*)enumerator->data;
        ma_product_t **product = (ma_product_t**)cur;

        *product = NULL;
        if((scheduler->no_of_products > 0) && (scheduler->no_of_products_visited < scheduler->no_of_products)) {
            *product = scheduler->group + scheduler->no_of_products_visited; err = MA_OK;
            MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "current product id(%s)", (*product)->id);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t move_next(ma_enumerator_t *enumerator, void *next) {
    if(enumerator && next) {
        ma_error_t err = MA_OK;
        ma_scheduler_client_t *scheduler = (ma_scheduler_client_t*)enumerator->data;
        ma_product_t **product = (ma_product_t**)next;
        
        *product = NULL;
        if((0 == scheduler->no_of_products) || (scheduler->no_of_products_visited == scheduler->no_of_products)) {
            err = MA_ERROR_NO_MORE_ITEMS;
            MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "products exhaust, last error(%d)", err);
        } else if(scheduler->no_of_products_visited > scheduler->no_of_products) {
            err = MA_ERROR_OUTOFBOUNDS;
            MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "products out of bounds, last error(%d)", err);
        } else {
            *product = scheduler->group + scheduler->no_of_products_visited;
            MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "next product id(%s)", (*product)->id);
            ++scheduler->no_of_products_visited;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

void reset(ma_enumerator_t *enumerator) {
    if(enumerator) {
        ((ma_scheduler_client_t*)(enumerator->data))->no_of_products_visited = 0; //reset
        MA_LOG(client_logger, MA_LOG_SEV_DEBUG, "product iterator reset");
    }
}

