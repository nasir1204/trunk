#include "ma/scheduler/ma_task.h"
#include "ma/internal/defs/ma_scheduler_defs.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"
#include "ma/scheduler/ma_enumerator.h"
#include "ma/scheduler/ma_triggerlist.h"
#include "ma_scheduler_client_private.h"
#include "ma/internal/utils/threading/ma_atomic.h"
#include "ma/internal/utils/uuid/ma_uuid.h"

#include <string.h>
#include <stdlib.h>

extern ma_logger_t *client_logger;

const char *gexec_status_str[] =  {
#define MA_TASK_EXEC_EXPANDER(status, index, status_str) status_str,
    MA_TASK_EXEC_STATUS_EXPANDER
#undef MA_TASK_EXEC_EXPANDER
};

MA_CPP(extern "C" {)
typedef struct ma_task_client_s ma_task_client_t;

ma_error_t ma_trigger_list_create(ma_trigger_list_t **tl);
ma_error_t ma_trigger_list_release(ma_trigger_list_t *tl);

static ma_error_t client_task_release(ma_task_t *task);
static ma_error_t client_task_set_settings(ma_task_t *task, const char *path , const char *name, ma_variant_t *value);
static ma_error_t client_task_set_retry_policy(ma_task_t *task, ma_task_retry_policy_t policy);
static ma_error_t client_task_set_app_payload(ma_task_t *task, ma_variant_t *app_payload);
static ma_error_t client_task_set_repeat_policy(ma_task_t *task, ma_task_repeat_policy_t policy);
static ma_error_t client_task_set_max_run_time(ma_task_t *task, ma_uint16_t max_run_time);
static ma_error_t client_task_set_max_run_time_limit(ma_task_t *task, ma_uint16_t max_run_time_limit);
static ma_error_t client_task_set_randomize_policy(ma_task_t *task, ma_task_randomize_policy_t policy);
static ma_error_t client_task_set_priority(ma_task_t *task, ma_task_priority_t priority);
static ma_error_t client_task_set_conditions(ma_task_t *task, ma_uint32_t conds);
static ma_error_t client_task_set_execution_status(ma_task_t *task, ma_task_exec_status_t status);
static ma_error_t client_task_set_delay_policy(ma_task_t *task, ma_task_delay_policy_t policy);
static ma_error_t client_task_set_missed_policy(ma_task_t *task, ma_task_missed_policy_t policy);
static ma_error_t client_task_set_creator_id(ma_task_t *task, const char *id);
static ma_error_t client_task_get_state(ma_task_t *task, ma_task_state_t *state);
static ma_error_t client_task_get_last_run_time(ma_task_t *task, ma_task_time_t *last_run_time);
static ma_error_t client_task_get_next_run_time(ma_task_t *task, ma_task_time_t *next_run_time);
static ma_error_t client_task_get_app_payload(ma_task_t *task, ma_variant_t **app_payload);
static ma_error_t client_task_get_missed_policy(ma_task_t *task, ma_task_missed_policy_t *policy);
static ma_error_t client_task_get_repeat_policy(ma_task_t *task, ma_task_repeat_policy_t *policy);
static ma_error_t client_task_get_priority(ma_task_t *task, ma_task_priority_t *priority);
static ma_error_t client_task_get_conditions(ma_task_t *task, ma_uint32_t *conds);
static ma_error_t client_task_get_id(ma_task_t *task, const char **task_id);
static ma_error_t client_task_get_name(ma_task_t *task, const char **task_name);
static ma_error_t client_task_get_type(ma_task_t *task, const char **task_type);
static ma_error_t client_task_get_creator_id(ma_task_t *task, const char **creator_id);
static ma_error_t client_task_get_retry_policy(ma_task_t *task, ma_task_retry_policy_t *retry_policy);
static ma_error_t client_task_get_max_run_time(ma_task_t *task, ma_uint16_t *max_run_time);
static ma_error_t client_task_get_max_run_time_limit(ma_task_t *task, ma_uint16_t *max_run_time_limit);
static ma_error_t client_task_get_execution_status(ma_task_t *task, ma_task_exec_status_t *status);
static ma_error_t client_task_add_trigger(ma_task_t *task, ma_trigger_t *trigger);
static ma_error_t client_task_remove_trigger(ma_task_t *task, const char *trigger_id);
static ma_error_t client_task_find_trigger(ma_task_t *task, const char *trigger_id, ma_trigger_t **trigger);
static ma_error_t client_task_set_name(ma_task_t *task, const char *name);
static ma_error_t client_task_set_type(ma_task_t *task, const char *type);
static ma_error_t client_task_set_software_id(ma_task_t *task, const char *id);
static ma_error_t client_task_get_software_id(ma_task_t *task, const char **id);
static ma_error_t client_task_get_settings(ma_task_t *task, const char *path , const char *name, ma_variant_t **value);
static ma_error_t client_task_set_state(ma_task_t *task, ma_task_state_t state);
static ma_error_t client_task_clear_setting(ma_task_t *task);
static ma_error_t client_task_set_next_run_time(ma_task_t *task, ma_task_time_t next_run_time);
static ma_error_t client_task_set_last_run_time(ma_task_t *task, ma_task_time_t last_run_time);
static ma_error_t client_task_set_time_zone(ma_task_t *task, ma_time_zone_t zone);
static ma_error_t client_task_get_time_zone(ma_task_t *task, ma_time_zone_t *zone);
static ma_error_t client_task_copy(const ma_task_t *task, ma_task_t **copy);
static ma_error_t client_task_set_id(ma_task_t *task, const char *id);
static ma_error_t client_task_get_trigger_list(ma_task_t *task, ma_trigger_list_t **tl);
static ma_error_t client_task_get_randomize_policy(ma_task_t *task, ma_task_randomize_policy_t *policy);
static ma_error_t client_task_get_delay_policy(ma_task_t *task, ma_task_delay_policy_t *policy);
static ma_error_t client_task_size(ma_task_t *task, size_t *no_of_triggers);
static ma_error_t client_task_add_ref(ma_task_t *task);
static ma_error_t client_task_unset_condition(ma_task_t *task, ma_task_cond_t cond);
static ma_error_t client_task_set_condition(ma_task_t *task, ma_task_cond_t cond);
static ma_error_t client_task_set_power_policy(ma_task_t *task, ma_task_power_policy_t policy);
static ma_error_t client_task_get_power_policy(ma_task_t *task, ma_task_power_policy_t *policy);
static ma_error_t client_task_set_missed(ma_task_t *task, ma_bool_t missed);
static ma_error_t client_task_get_missed(ma_task_t *task, ma_bool_t *missed);

const static ma_task_methods_t methods = {
    &client_task_release,
    &client_task_set_settings,
    &client_task_set_retry_policy,
    &client_task_set_app_payload,
    &client_task_set_repeat_policy,
    &client_task_set_max_run_time,
    &client_task_set_max_run_time_limit,
    &client_task_set_randomize_policy,
    &client_task_set_priority,
    &client_task_set_conditions,
    &client_task_set_execution_status,
    &client_task_set_delay_policy,
    &client_task_set_missed_policy,
    &client_task_set_creator_id,
    &client_task_get_state,
    &client_task_get_last_run_time,
    &client_task_get_next_run_time,
    &client_task_get_app_payload,
    &client_task_get_missed_policy,
    &client_task_get_repeat_policy,
    &client_task_get_priority,
    &client_task_get_conditions,
    &client_task_get_id,
    &client_task_get_name,
    &client_task_get_type,
    &client_task_get_creator_id,
    &client_task_get_retry_policy,
    &client_task_get_max_run_time,
    &client_task_get_max_run_time_limit,
    &client_task_get_execution_status,
    &client_task_add_trigger,
    &client_task_remove_trigger,
    &client_task_find_trigger,
    &client_task_set_name,
    &client_task_set_type,
    &client_task_set_software_id,
    &client_task_get_software_id,
    &client_task_get_settings,
    &client_task_set_state,
    &client_task_clear_setting,
    &client_task_set_next_run_time,
    &client_task_set_last_run_time,
    &client_task_set_time_zone,
    &client_task_get_time_zone,
    &client_task_copy,
    &client_task_set_id,
    &client_task_get_trigger_list,
    &client_task_get_randomize_policy,
    &client_task_get_delay_policy,
    0,
    &client_task_size,
    &client_task_add_ref,
    &client_task_unset_condition,
    &client_task_set_condition,
    &client_task_set_power_policy,
    &client_task_get_power_policy,
    &client_task_set_missed,
    &client_task_get_missed
};

struct ma_task_client_s {
    ma_task_t task_impl;
    char id[MA_MAX_TASKID_LEN + 1];
    char name[MA_MAX_TASKID_LEN + 1];
    char type[MA_MAX_TASKID_LEN + 1];
    char creator_id[MA_MAX_CREATOR_ID_LEN_INT + 1];
    char software_id[MA_MAX_SOFTWARE_ID_LEN_INT + 1];
    ma_task_missed_policy_t missed_policy;
    ma_task_retry_policy_t retry_policy;
    ma_task_randomize_policy_t random_policy;
    ma_task_delay_policy_t delay_policy;
    ma_task_exec_status_t exec_status; //task executor will update this state
    ma_task_repeat_policy_t repeat;
    ma_uint32_t cond;
    ma_trigger_list_t *trigger_list;
    ma_uint16_t max_run_time;//minutes
    ma_uint16_t max_run_time_limit;//minutes
    ma_task_time_t last_run_time;
    ma_task_time_t next_run_time;
    ma_task_priority_t priority;
    ma_variant_t *app_payload;
    ma_task_state_t state;
    ma_time_zone_t zone;
    ma_atomic_counter_t ref_count;
    ma_task_power_policy_t power;
    ma_uint32_t missed:1;
    void *reserved;
};

ma_error_t ma_task_create(const char *id, ma_task_t **task) {
    if(task) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;
        ma_task_client_t *client_task = NULL;
        
        if(id && (strlen(id) > MA_MAX_TASKID_LEN -1))
            return MA_ERROR_INVALIDARG;
        
        
        *task = NULL;
        if((client_task = (ma_task_client_t*)calloc(1, sizeof(ma_task_client_t)))) {
            char task_id[UUID_LEN_TXT] = {0};

            MA_SET_BIT(client_task->cond, MA_TASK_COND_INTERACTIVE);
            MA_SET_BIT(client_task->cond, MA_TASK_COND_DELETE_WHEN_DONE);
            client_task->priority = MA_TASK_PRIORITY_LOW;
            client_task->state = MA_TASK_STATE_NOT_SCHEDULED;
            client_task->max_run_time = MA_DEFAULT_MAX_RUN_TIME;
            client_task->max_run_time_limit = MA_DEFAULT_MAX_RUN_TIME_LIMIT;
            client_task->exec_status = MA_TASK_EXEC_NOT_STARTED;
            client_task->zone = MA_TIME_ZONE_LOCAL;
            if(!id) ma_uuid_generate(0, NULL, task_id);
            strncpy(client_task->id, id ? id : task_id,  MA_MAX_TASKID_LEN -1);
            strncpy(client_task->name, MA_EMPTY, strlen(MA_EMPTY));
            strncpy(client_task->type, MA_EMPTY, strlen(MA_EMPTY));
            if(MA_OK == (err = ma_trigger_list_create(&client_task->trigger_list))) {
                ma_task_init(*task = (ma_task_t*)client_task, &methods, task);
                (void)ma_task_add_ref(*task);
            } else {
                MA_LOG(client_logger, MA_LOG_SEV_DEBUG,"task creation failed"); 
                free(client_task);
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

// task dtor
ma_error_t client_task_release(ma_task_t *task) {
    if(task) {
        ma_error_t err = MA_OK;

        if(!MA_ATOMIC_DECREMENT(((ma_task_client_t*)task)->ref_count)) {
            if(MA_OK == (err = ma_trigger_list_release(((ma_task_client_t*)task)->trigger_list))) {
                (void)client_task_clear_setting(task);
                free(task);
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t client_task_set_settings(ma_task_t *task, const char *path , const char *name, ma_variant_t *value) {
    return(task && path && name && value)?set_settings(path, name, value, &((ma_task_client_t*)task)->app_payload):MA_ERROR_INVALIDARG;
}

ma_error_t client_task_set_retry_policy(ma_task_t *task, ma_task_retry_policy_t policy) {
    return(task && memcpy(&((ma_task_client_t*)task)->retry_policy, &policy, sizeof(ma_task_retry_policy_t)))?MA_OK:MA_ERROR_INVALIDARG;
}

ma_error_t client_task_set_app_payload(ma_task_t *task, ma_variant_t *app_payload) {
    if(task && app_payload) {
        if(MA_OK == ma_variant_add_ref(app_payload)) {
            ((ma_task_client_t *)task)->app_payload = app_payload;
            return MA_OK;
        }
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t client_task_set_repeat_policy(ma_task_t *task, ma_task_repeat_policy_t policy) {
    return (task && memcpy(&((ma_task_client_t*)task)->repeat, &policy, sizeof(ma_task_repeat_policy_t)))?MA_OK:MA_ERROR_INVALIDARG;
}

ma_error_t client_task_set_max_run_time(ma_task_t *task, ma_uint16_t max_run_time) {
    return (task && max_run_time && (((ma_task_client_t*)task)->max_run_time = max_run_time))?MA_OK:MA_ERROR_INVALIDARG;
}

ma_error_t client_task_set_max_run_time_limit(ma_task_t *task, ma_uint16_t max_run_time_limit) {
    return(task && max_run_time_limit && (((ma_task_client_t*)task)->max_run_time_limit = max_run_time_limit))?MA_OK:MA_ERROR_INVALIDARG;
}

ma_error_t client_task_set_randomize_policy(ma_task_t *task, ma_task_randomize_policy_t policy) {
    return (task && memcpy(&((ma_task_client_t*)task)->random_policy, &policy, sizeof(ma_task_randomize_policy_t)))?MA_OK:MA_ERROR_INVALIDARG;
}

ma_error_t client_task_set_priority(ma_task_t *task, ma_task_priority_t priority) {
    return (task && ((priority >= MA_TASK_PRIORITY_LOW) && (priority <= MA_TASK_PRIORITY_HIGH)) && (((ma_task_client_t*)task)->priority = priority))?MA_OK:MA_ERROR_INVALIDARG;
}

ma_error_t client_task_set_conditions(ma_task_t *task, ma_uint32_t conds) {
    if(task) {
        ((ma_task_client_t*)task)->cond = conds;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t client_task_set_execution_status(ma_task_t *task, ma_task_exec_status_t status) {
    return (task && ((status >= MA_TASK_EXEC_NOT_STARTED) && (status <= MA_TASK_EXEC_STOPPED)) && (((ma_task_client_t*)task)->exec_status = status))?MA_OK:MA_ERROR_INVALIDARG;
}

ma_error_t client_task_set_delay_policy(ma_task_t *task, ma_task_delay_policy_t policy) {
    return (task && memcpy(&((ma_task_client_t*)task)->delay_policy, &policy, sizeof(ma_task_delay_policy_t)))?MA_OK:MA_ERROR_INVALIDARG;    
}

ma_error_t client_task_set_missed_policy(ma_task_t *task, ma_task_missed_policy_t policy) {
    return(task && memcpy(&((ma_task_client_t*)task)->missed_policy, &policy, sizeof(ma_task_missed_policy_t)))?MA_OK:MA_ERROR_INVALIDARG;
}

ma_error_t client_task_set_creator_id(ma_task_t *task, const char *id) {
    memset(((ma_task_client_t*)task)->creator_id, 0, MA_MAX_CREATOR_ID_LEN_INT);
    return (task && id && (strlen(id) < MA_MAX_CREATOR_ID_LEN_INT) && strcpy(((ma_task_client_t*)task)->creator_id, id))?MA_OK:MA_ERROR_INVALIDARG;
}

ma_error_t client_task_get_state(ma_task_t *task, ma_task_state_t *state) {
    return(task && state && (*state = ((ma_task_client_t*)task)->state))?MA_OK:MA_ERROR_INVALIDARG;
}

ma_error_t client_task_get_last_run_time(ma_task_t *task, ma_task_time_t *last_run_time) {
    return(task && last_run_time && memcpy(last_run_time, &((ma_task_client_t*)task)->last_run_time, sizeof(ma_task_time_t)))?MA_OK:MA_ERROR_INVALIDARG;
}

ma_error_t client_task_get_next_run_time(ma_task_t *task, ma_task_time_t *next_run_time) {
    return(task && next_run_time && memcpy(next_run_time, &((ma_task_client_t*)task)->next_run_time, sizeof(ma_task_time_t)))?MA_OK:MA_ERROR_INVALIDARG;
}

ma_error_t client_task_get_app_payload(ma_task_t *task, ma_variant_t **app_payload) {
    if(task && app_payload) {
        if(MA_OK == ma_variant_add_ref( ((ma_task_client_t *)task)->app_payload)) {
            *app_payload = ((ma_task_client_t *)task)->app_payload;
            return MA_OK;
        }
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t client_task_get_missed_policy(ma_task_t *task, ma_task_missed_policy_t *policy) {
    return(task && policy && memcpy(policy, &((ma_task_client_t*)task)->missed_policy, sizeof(ma_task_missed_policy_t)))?MA_OK:MA_ERROR_INVALIDARG;
}

ma_error_t client_task_get_repeat_policy(ma_task_t *task, ma_task_repeat_policy_t *policy) {
    return(task && policy && memcpy(policy, &((ma_task_client_t*)task)->repeat, sizeof(ma_task_repeat_policy_t)))?MA_OK:MA_ERROR_INVALIDARG;
}

ma_error_t client_task_get_priority(ma_task_t *task, ma_task_priority_t *priority) {
    return(task && priority && (*priority = ((ma_task_client_t*)task)->priority))?MA_OK:MA_ERROR_INVALIDARG;
}

ma_error_t client_task_get_conditions(ma_task_t *task, ma_uint32_t *conds) {
    return(task && conds && (*conds = ((ma_task_client_t*)task)->cond))?MA_OK:MA_ERROR_INVALIDARG;
}

ma_error_t client_task_get_id(ma_task_t *task, const char **task_id) {
    return(task && task_id && (*task_id = ((ma_task_client_t*)task)->id))?MA_OK:MA_ERROR_INVALIDARG;
}

ma_error_t client_task_get_name(ma_task_t *task, const char **task_name) {
    return(task && task_name && (*task_name = ((ma_task_client_t*)task)->name))?MA_OK:MA_ERROR_INVALIDARG;
}

ma_error_t client_task_get_type(ma_task_t *task, const char **task_type) {
    return(task && task_type && (*task_type = ((ma_task_client_t*)task)->type))?MA_OK:MA_ERROR_INVALIDARG;
}

ma_error_t client_task_get_creator_id(ma_task_t *task, const char **creator_id) {
    return(task && creator_id && (*creator_id = ((ma_task_client_t*)task)->creator_id))?MA_OK:MA_ERROR_INVALIDARG;
}

ma_error_t client_task_get_retry_policy(ma_task_t *task, ma_task_retry_policy_t *retry_policy) {
    return(task && retry_policy && memcpy(retry_policy, &((ma_task_client_t*)task)->retry_policy, sizeof(ma_task_retry_policy_t)))?MA_OK:MA_ERROR_INVALIDARG;    
}

ma_error_t client_task_get_max_run_time(ma_task_t *task, ma_uint16_t *max_run_time) {
    return(task && max_run_time && (*max_run_time = ((ma_task_client_t*)task)->max_run_time))?MA_OK:MA_ERROR_INVALIDARG;
}

ma_error_t client_task_get_max_run_time_limit(ma_task_t *task, ma_uint16_t *max_run_time_limit) {
    return(task && max_run_time_limit && (*max_run_time_limit = ((ma_task_client_t*)task)->max_run_time_limit))?MA_OK:MA_ERROR_INVALIDARG;    
}

ma_error_t client_task_get_execution_status(ma_task_t *task, ma_task_exec_status_t *status) {
    return (task && status && (*status = ((ma_task_client_t*)task)->exec_status))?MA_OK:MA_ERROR_INVALIDARG;    
}

ma_error_t client_task_add_trigger(ma_task_t *task, ma_trigger_t *trigger) {
    return(task && trigger)? ma_trigger_list_add(((ma_task_client_t*)task)->trigger_list, trigger):MA_ERROR_INVALIDARG; 
}

ma_error_t client_task_remove_trigger(ma_task_t *task, const char *trigger_id) {
    return(task && trigger_id)?ma_trigger_list_remove(((ma_task_client_t*)task)->trigger_list, trigger_id):MA_ERROR_INVALIDARG; 
}

ma_error_t client_task_find_trigger(ma_task_t *task, const char *trigger_id, ma_trigger_t **trigger) {
    return (task && trigger_id && trigger)?ma_trigger_list_find(((ma_task_client_t*)task)->trigger_list, trigger_id, trigger):MA_ERROR_INVALIDARG;
}

ma_error_t client_task_set_name(ma_task_t *task, const char *name) {
    return (task && name && (strlen(name) < MA_MAX_TASKID_LEN) && strcpy(((ma_task_client_t*)task)->name, name))?MA_OK:MA_ERROR_INVALIDARG;
}

ma_error_t client_task_set_type(ma_task_t *task, const char *type) {
    return (task && type && (strlen(type) < MA_MAX_TASKID_LEN) && strcpy(((ma_task_client_t*)task)->type, type))?MA_OK:MA_ERROR_INVALIDARG;
}

ma_error_t client_task_set_software_id(ma_task_t *task, const char *id) {
    return (task && id && (strlen(id) < MA_MAX_SOFTWARE_ID_LEN_INT) && strcpy(((ma_task_client_t*)task)->software_id, id))?MA_OK:MA_ERROR_INVALIDARG;
}

ma_error_t client_task_get_software_id(ma_task_t *task, const char **id) {
    if(task && id) {
        *id = ((ma_task_client_t*)task)->software_id;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t client_task_get_settings(ma_task_t *task, const char *path , const char *name, ma_variant_t **value) {
    return(task && path && name && value)?get_settings(((ma_task_client_t*)task)->app_payload, path, name, value):MA_ERROR_INVALIDARG;
}

ma_error_t client_task_set_state(ma_task_t *task, ma_task_state_t state) {
    if(task) {
        ((ma_task_client_t*)task)->state = state;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t client_task_clear_setting(ma_task_t *task) {
    if(task) {
        return(((ma_task_client_t*)task)->app_payload)?ma_variant_release(((ma_task_client_t*)task)->app_payload):MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t client_task_set_next_run_time(ma_task_t *task, ma_task_time_t next_run_time) {
    return (task && memcpy(&((ma_task_client_t*)task)->next_run_time, &next_run_time, sizeof(ma_task_time_t)))?MA_OK:MA_ERROR_INVALIDARG;
}

ma_error_t client_task_set_last_run_time(ma_task_t *task, ma_task_time_t last_run_time) {
    return (task && memcpy(&((ma_task_client_t*)task)->last_run_time, &last_run_time, sizeof(ma_task_time_t)))?MA_OK:MA_ERROR_INVALIDARG;
}

ma_error_t client_task_set_time_zone(ma_task_t *task, ma_time_zone_t zone) {
    if(task) {
        ((ma_task_client_t*)task)->zone = zone;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t client_task_get_time_zone(ma_task_t *task, ma_time_zone_t *zone) {
    if(task && zone) {
        *zone = ((ma_task_client_t*)task)->zone;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t client_task_copy(const ma_task_t *task, ma_task_t **copy) {
    if(task && copy) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;

        if((*copy = (ma_task_t*)calloc(1, sizeof(ma_task_client_t)))) {
            ma_task_init((ma_task_t*)*copy, task->methods, copy);
            strncpy(((ma_task_client_t*)*copy)->name, ((ma_task_client_t*)task)->name, strlen(((ma_task_client_t*)task)->name));
            strncpy(((ma_task_client_t*)*copy)->type, ((ma_task_client_t*)task)->type, strlen(((ma_task_client_t*)task)->type));
            strncpy(((ma_task_client_t*)*copy)->id, ((ma_task_client_t*)task)->id, strlen(((ma_task_client_t*)task)->id));
            strncpy(((ma_task_client_t*)*copy)->software_id, ((ma_task_client_t*)task)->software_id, MA_MAX_SOFTWARE_ID_LEN_INT );
            strncpy(((ma_task_client_t*)*copy)->creator_id, ((ma_task_client_t*)task)->creator_id, MA_MAX_CREATOR_ID_LEN_INT);
            ((ma_task_client_t*)*copy)->zone = ((ma_task_client_t*)task)->zone;
            ((ma_task_client_t*)*copy)->state = MA_TASK_STATE_NO_TRIGGER;
            ((ma_task_client_t*)*copy)->cond = ((ma_task_client_t*)task)->cond;
            if(MA_OK == (err = ma_variant_add_ref(((ma_task_client_t*)copy)->app_payload = ((ma_task_client_t*)task)->app_payload))) {

                {
                    ma_task_retry_policy_t policy = {0};
                    if(MA_OK == (err = ma_task_get_retry_policy((ma_task_t*)task, &policy))) {
                        err = ma_task_set_retry_policy((ma_task_t*)*copy, policy);
                    }
                }

                {
                    ma_task_repeat_policy_t policy = {{0}};
                    if((MA_OK == err) && (MA_OK == (err = ma_task_get_repeat_policy((ma_task_t*)task, &policy)))) {
                        err = ma_task_set_repeat_policy((ma_task_t*)*copy, policy);
                    }
                }

                {
                    ma_task_missed_policy_t policy = {{0}};
                    if((MA_OK == err) && (MA_OK == (err = ma_task_get_missed_policy((ma_task_t*)task, &policy)))) {
                        err = ma_task_set_missed_policy((ma_task_t*)*copy, policy);
                    }
                }

                {
                    ma_task_delay_policy_t policy = {0};
                    if(MA_OK == ma_task_get_delay_policy((ma_task_t*)task, &policy)) {
                        (void)ma_task_set_delay_policy((ma_task_t*)*copy, policy);
                    }
                }

                {
                    ma_uint16_t max_run_time = 0;
                    if((MA_OK == err) && (MA_OK == (err = ma_task_get_max_run_time((ma_task_t*)task, &max_run_time)))) {
                        err = ma_task_set_max_run_time((ma_task_t*)*copy, max_run_time);
                    }
                }

                {
                    ma_uint16_t max_run_time_limit = 0;
                    if((MA_OK == err) && (MA_OK == (err = ma_task_get_max_run_time_limit((ma_task_t*)task, &max_run_time_limit)))) {
                        err = ma_task_set_max_run_time_limit((ma_task_t*)*copy, max_run_time_limit);
                    }
                }

                {
                    ma_task_priority_t priority = MA_TASK_PRIORITY_LOW;
                    if((MA_OK == err) && (MA_OK == (err = ma_task_get_priority((ma_task_t*)task, &priority)))) {
                        err = ma_task_set_priority((ma_task_t*)*copy, priority);
                    }
                }

                {
                    ma_task_exec_status_t exec_status = MA_TASK_EXEC_NOT_STARTED;
                    if((MA_OK == err) && (MA_OK == (err = ma_task_get_execution_status((ma_task_t*)task, &exec_status)))) {
                        err = ma_task_set_execution_status((ma_task_t*)*copy, exec_status);
                    }
                }

                if(MA_OK == err)
                    err =  ma_task_add_ref((ma_task_t*)*copy);

                {
                    if(MA_OK == (err = ma_trigger_list_create(&((ma_task_client_t*)*copy)->trigger_list))) {
                        ma_trigger_t *trigger = NULL;

                        ma_enumerator_reset((ma_enumerator_t*)((ma_task_client_t*)task));
                        while(MA_OK == (err = ma_enumerator_next((ma_enumerator_t*)((ma_task_client_t*)task)->trigger_list, &trigger))) {
                            ma_trigger_type_t type = MA_TRIGGER_ONCE;
                            if(MA_OK == (err = ma_trigger_get_type(trigger, &type))) {
                                ma_trigger_t *t = NULL;
                                switch(type) {
                                    case MA_TRIGGER_DAILY: {
                                        ma_trigger_daily_policy_t policy = {0};
                                        if(MA_OK == (err = ma_trigger_get_policy(trigger, &policy))) {
                                            err = ma_trigger_create_daily(&policy, &t);
                                        }
                                        break;
                                    }
                                    case MA_TRIGGER_WEEKLY:  {
                                        ma_trigger_weekly_policy_t policy = {0};
                                        if(MA_OK == (err = ma_trigger_get_policy(trigger, &policy))) {
                                            err = ma_trigger_create_weekly(&policy, &t);
                                        }
                                        break;
                                    }
                                    case MA_TRIGGER_MONTHLY_DATE:  {
                                        ma_trigger_monthly_date_policy_t policy = {0};
                                        if(MA_OK == (err = ma_trigger_get_policy(trigger, &policy))) {
                                            err = ma_trigger_create_monthly_date(&policy, &t);
                                        }
                                        break;
                                    }
                                    case MA_TRIGGER_ONCE:  {
                                        ma_trigger_run_once_policy_t policy = {0};
                                        if(MA_OK == (err = ma_trigger_get_policy(trigger, &policy))) {
                                            err = ma_trigger_create_run_once(&policy, &t);
                                        }
                                        break;
                                    }
                                    case MA_TRIGGER_AT_SYSTEM_START:  {
                                        ma_trigger_systemstart_policy_t policy = {0};
                                        if(MA_OK == (err = ma_trigger_get_policy(trigger, &policy))) {
                                            err = ma_trigger_create_system_start(&policy, &t);
                                        }
                                        break;
                                    }
                                    case MA_TRIGGER_AT_LOGON:  {
                                        ma_trigger_logon_policy_t policy = {0};
                                        if(MA_OK == (err =  ma_trigger_get_policy(trigger, &policy))) {
                                            err = ma_trigger_create_logon(&policy, &t);
                                        }
                                        break;
                                    }
                                    case MA_TRIGGER_ON_SYSTEM_IDLE:  {
                                        ma_trigger_system_idle_policy_t policy = {0};
                                        if(MA_OK == (err = ma_trigger_get_policy(trigger, &policy))) {
                                            err = ma_trigger_create_system_idle(&policy, &t);
                                        }
                                        break;
                                    }
                                    case MA_TRIGGER_NOW:  {
                                        ma_trigger_run_now_policy_t policy = {0};
                                        if(MA_OK == (err = ma_trigger_get_policy(trigger, &policy))) {
                                            err = ma_trigger_create_run_now(&policy, &t);
                                        }
                                        break;
                                    }
                                    case MA_TRIGGER_ON_DIALUP:  {
                                        ma_trigger_dialup_policy_t policy = {0};
                                        if(MA_OK == (err = ma_trigger_get_policy(trigger, &policy))) {
                                            err = ma_trigger_create_dialup(&policy, &t);
                                        }
                                        break;
                                    }
                                    case MA_TRIGGER_MONTHLY_DOW:  {
                                        ma_trigger_monthly_dow_policy_t policy = {0};
                                        if(MA_OK == (err = ma_trigger_get_policy(trigger, &policy))) {
                                            err = ma_trigger_create_monthly_dow(&policy, &t);
                                        }
                                        break;
                                    }
                                }

                                {
                                    ma_task_time_t begin_date = {0};
                                    if((MA_OK == err) && (MA_OK == (err = ma_trigger_get_begin_date(trigger, &begin_date)))) {
                                        (void)ma_trigger_set_begin_date(t, &begin_date);
                                    }
                                }

                                {
                                    ma_task_time_t end_date = {0};
                                    if((MA_OK == err) && (MA_OK == (err = ma_trigger_get_end_date(trigger, &end_date)))) {
                                        (void)ma_trigger_set_end_date(t, &end_date);
                                    }
                                }

                                if((MA_OK == err) && (MA_OK == (err = ma_task_add_trigger((ma_task_t*)*copy, t)))) {
                                    t = NULL;
                                }

                            }
                        }
                    }
                }
            }
        }

        return err == MA_ERROR_NO_MORE_ITEMS ? MA_OK : err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t client_task_set_id(ma_task_t *task, const char *task_id) {
    return (task && task_id && (strlen(task_id) < MA_MAX_TASKID_LEN) && strcpy(((ma_task_client_t*)task)->id, task_id))?MA_OK:MA_ERROR_INVALIDARG;
}

ma_error_t client_task_set_power_policy(ma_task_t *task, ma_task_power_policy_t policy) {
    return(task && memcpy(&((ma_task_client_t*)task)->power, &policy, sizeof(ma_task_power_policy_t)))?MA_OK:MA_ERROR_INVALIDARG;
}

ma_error_t client_task_get_power_policy(ma_task_t *task, ma_task_power_policy_t *policy) {
    return(task && memcpy(policy, &((ma_task_client_t*)task)->power, sizeof(ma_task_power_policy_t)))?MA_OK:MA_ERROR_INVALIDARG;
}

ma_error_t client_task_get_trigger_list(ma_task_t *task, ma_trigger_list_t **tl) {
    if(task && tl) {
        *tl = ((ma_task_client_t*)task)->trigger_list;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t client_task_get_randomize_policy(ma_task_t *task, ma_task_randomize_policy_t *policy) {
    return(task && policy && memcpy(policy, &((ma_task_client_t*)task)->random_policy, sizeof(ma_task_randomize_policy_t)))?MA_OK:MA_ERROR_INVALIDARG;
}

ma_error_t client_task_get_delay_policy(ma_task_t *task, ma_task_delay_policy_t *policy) {
    return (task && memcpy(policy, &((ma_task_client_t*)task)->delay_policy, sizeof(ma_task_delay_policy_t)))?MA_OK:MA_ERROR_INVALIDARG;
}

ma_error_t client_task_size(ma_task_t *task, size_t *no_of_triggers) {
    return(task && no_of_triggers)?ma_trigger_list_size(((ma_task_client_t*)task)->trigger_list, no_of_triggers):MA_ERROR_INVALIDARG;
}

ma_error_t client_task_add_ref(ma_task_t *task) {
    if(task) {
        MA_ATOMIC_INCREMENT(((ma_task_client_t*)task)->ref_count);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t client_task_unset_condition(ma_task_t *task, ma_task_cond_t cond) {
    if(task) {
        MA_CLEAR_BIT(((ma_task_client_t*)task)->cond, cond);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t client_task_set_condition(ma_task_t *task, ma_task_cond_t cond) {
    if(task) {
        MA_SET_BIT(((ma_task_client_t*)task)->cond, cond);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t client_task_set_missed(ma_task_t *task, ma_bool_t missed) {
    if(task) {
        ((ma_task_client_t*)task)->missed = missed;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t client_task_get_missed(ma_task_t *task, ma_bool_t *missed) {
    if(task) {
        *missed = ((ma_task_client_t*)task)->missed;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

MA_CPP(})
