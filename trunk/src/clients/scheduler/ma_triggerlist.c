#include "ma/scheduler/ma_enumerator.h"
#include "ma/scheduler/ma_triggerlist.h"
#include "ma/internal/defs/ma_scheduler_defs.h"
#include "ma/internal/ma_macros.h"
#include "ma/scheduler/ma_triggers.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct ma_trigger_list_s {
    ma_enumerator_t enumerator;
    ma_trigger_t **group;
	size_t triggers_visited;
    size_t no_of_triggers;
};

MA_CPP(extern "C" {)    
ma_error_t ma_trigger_set_id(ma_trigger_t *trigger, const char *id);
MA_CPP(})
    

static ma_error_t current(ma_enumerator_t *enumerator, void *cur);
static ma_error_t move_next(ma_enumerator_t *enumerator, void *next);
static void reset(ma_enumerator_t *enumerator);

static const ma_enumerator_methods_t methods = {
    &current,
    &move_next,
    &reset
};

MA_SCHEDULER_API ma_error_t ma_trigger_list_create(ma_trigger_list_t **tl) {
    if(tl) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;

        if((*tl = (ma_trigger_list_t*)calloc(1, sizeof(ma_trigger_list_t)))) {
            ma_enumerator_init((ma_enumerator_t*)*tl, &methods, tl); err = MA_OK;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_trigger_list_find(ma_trigger_list_t *tl, const char *id, ma_trigger_t **trigger) {
    if(tl && id && trigger) {
        ma_error_t err = MA_ERROR_SCHEDULER_INVALID_TRIGGER_ID;
        size_t itr = 0;

        *trigger = NULL;
        for(itr = 0; itr < tl->no_of_triggers; ++itr) {
            const char *tid = NULL;

            if(MA_OK ==  ma_trigger_get_id(tl->group[itr], &tid)) {
                if(!strcmp(tid, id)) {
                    err = ma_trigger_add_ref(*trigger = tl->group[itr]);
                    break;
                }
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_trigger_list_add(ma_trigger_list_t *tl, ma_trigger_t *trigger) {
    if(tl && trigger) {
        ma_error_t err = MA_OK;
        const char *id = NULL;

        if((MA_OK == (err = ma_trigger_get_id(trigger, &id))) && !strcmp(id, "")) {
            char id_buf[MA_MAX_TRIGGERID_LEN + 1] = {0};

            if(!(tl->group = (ma_trigger_t**)realloc(tl->group, (sizeof(ma_trigger_t*) * (tl->no_of_triggers + 1))))) {
                tl->no_of_triggers = 0;
                return MA_ERROR_OUTOFMEMORY;
            }
            MA_MSC_SELECT(_snprintf, snprintf)(id_buf, MA_MAX_TRIGGERID_LEN, "%u", tl->no_of_triggers);
            (void)ma_trigger_set_id(trigger, id_buf);
            (void)ma_trigger_add_ref(tl->group[tl->no_of_triggers] = trigger);
            ++tl->no_of_triggers;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_trigger_list_remove(ma_trigger_list_t *tl, const char *id) {
    if(tl && id) {
        ma_error_t err = MA_ERROR_SCHEDULER_INVALID_TRIGGER_ID;
        size_t pos = 0;

        if(tl->no_of_triggers && strcmp(id, "")) {
            for(pos = 0; pos < tl->no_of_triggers; ++pos) {
                const char *tid = NULL;

                (void)ma_trigger_get_id(tl->group[pos], &tid);
                if(!strcmp(tid, id)) {
                    if(MA_OK == (err = ma_trigger_release(tl->group[pos]))) {
                        if(tl->no_of_triggers != (pos+1)) {
                            memcpy(tl->group+pos, tl->group+(pos+1), (sizeof(ma_trigger_t*)*(tl->no_of_triggers  - (pos+1))));
                        }
                        --tl->no_of_triggers;
                    }
                }
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

MA_SCHEDULER_API ma_error_t ma_trigger_list_release(ma_trigger_list_t *tl) {
    if(tl) {
        size_t itr = 0;

        if(tl->group) {
            for(itr = 0; itr < tl->no_of_triggers; ++itr) {
                (void)ma_trigger_release(tl->group[itr]);
            }
            free(tl->group);
        }
        free(tl);

        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_trigger_list_size(ma_trigger_list_t *tl, size_t *size) {
    if(tl && size) {
        *size = tl->no_of_triggers;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_trigger_list_at(ma_trigger_list_t *tl, size_t index, ma_trigger_t **trigger) {
    return(tl && (index < tl->no_of_triggers) && trigger)?ma_trigger_add_ref(*trigger = tl->group[index]):MA_ERROR_INVALIDARG;
}

ma_error_t current(ma_enumerator_t *enumerator, void *cur) {
    if(enumerator && cur) {
        ma_error_t err = MA_ERROR_OUTOFBOUNDS;
        ma_trigger_list_t *tl = (ma_trigger_list_t*)enumerator;
        ma_trigger_t **ret_trigger = (ma_trigger_t**)cur;

        *ret_trigger = NULL;
        if((tl->no_of_triggers > 0) && (tl->triggers_visited < tl->no_of_triggers)) {
            *ret_trigger = tl->group[tl->triggers_visited]; err = MA_OK;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t move_next(ma_enumerator_t *enumerator, void *next) {
    if(enumerator && next) {
	    ma_error_t err = MA_OK;
        ma_trigger_list_t *tl = (ma_trigger_list_t*)enumerator;
        ma_trigger_t **ret_trigger = (ma_trigger_t**)next;

        *ret_trigger = NULL;		
		if((0 == tl->no_of_triggers) || (tl->triggers_visited == tl->no_of_triggers)) {
			err = MA_ERROR_NO_MORE_ITEMS;
        } else if(tl->triggers_visited > tl->no_of_triggers) {
			err = MA_ERROR_OUTOFBOUNDS;
        } else {
            *ret_trigger = tl->group[tl->triggers_visited];
            ++tl->triggers_visited; 
			err = MA_OK;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

void reset(ma_enumerator_t *enumerator) {
    if(enumerator) {
        ((ma_trigger_list_t*)enumerator)->triggers_visited = 0; //reset
    }
}

