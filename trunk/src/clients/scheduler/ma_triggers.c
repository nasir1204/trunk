#include "ma/scheduler/ma_triggers.h"
#include "ma/scheduler/ma_task.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"
#include "ma/internal/ma_macros.h"
#include "ma/logger/ma_file_logger.h"
#include "ma/internal/defs/ma_scheduler_defs.h"
#include "ma_scheduler_client_private.h"
#include "ma/internal/utils/threading/ma_atomic.h"

#include <string.h>
#include <stdlib.h>
#include <time.h>

#ifdef MA_WINDOWS
#include <Windows.h>
#endif

extern ma_logger_t *client_logger;
MA_CPP(extern "C" {)

typedef struct ma_trigger_client_s ma_trigger_client_t;
/* trigger structure */
struct ma_trigger_client_s {
    ma_trigger_state_t state;
    char id[MA_MAX_TRIGGERID_LEN + 1];
    ma_task_time_t begin_date;
    ma_task_time_t end_date;
    ma_task_time_t last_fire_date;
    ma_task_time_t next_fire_date;
    ma_trigger_type_t type;
    ma_atomic_counter_t ref_count;
};

// trigger(s)
typedef struct ma_trigger_run_once_s {
    ma_trigger_t impl;
    ma_trigger_client_t client;
    ma_bool_t once;
}ma_trigger_run_once_t;

typedef struct ma_trigger_run_now_s {
    ma_trigger_t trigger_impl;
    ma_trigger_client_t client;
    ma_bool_t now;
}ma_trigger_run_now_t;

typedef struct ma_trigger_daily_s {
    ma_trigger_t impl;
    ma_trigger_client_t client;
    ma_uint16_t days_interval;
}ma_trigger_daily_t;

typedef struct ma_trigger_weekly_s {
    ma_trigger_t impl;
    ma_trigger_client_t client;
    ma_uint16_t weeks_interval;
    ma_uint16_t days_of_the_week;
}ma_trigger_weekly_t;

typedef struct ma_trigger_monthly_date_s {
    ma_trigger_t impl;
    ma_trigger_client_t client;
    ma_uint16_t date;
    ma_uint16_t months;
}ma_trigger_monthly_date_t;

typedef struct ma_trigger_monthly_dow_s {
    ma_trigger_t impl;
    ma_trigger_client_t client; 
    ma_which_week_t which_week;
    ma_day_of_week_t day_of_the_week;
    ma_uint16_t months;
}ma_trigger_monthly_dow_t;

typedef struct ma_trigger_missed_s {
    ma_trigger_t impl;
    ma_trigger_client_t client;
    ma_uint16_t hour;
    ma_uint16_t minute;
}ma_trigger_missed_t;

typedef struct ma_trigger_app_idle_s {
    ma_trigger_t impl;
    ma_trigger_client_t client; 
    ma_bool_t recurr;
}ma_trigger_app_idle_t;

typedef struct ma_trigger_system_idle_s {
    ma_trigger_t impl;
    ma_trigger_client_t client;
    ma_uint16_t idle_wait_minutes;
}ma_trigger_system_idle_t;

typedef struct ma_trigger_powersave_s {
    ma_trigger_t impl;
    ma_trigger_client_t client;  
    ma_uint16_t powersave_minutes;
    ma_uint16_t powersave_deadline_minutes;
}ma_trigger_powersave_t;

typedef struct ma_trigger_dialup_s {
    ma_trigger_t impl;
    ma_trigger_client_t client;
    ma_bool_t daily_once;
}ma_trigger_dialup_t;

typedef struct ma_trigger_system_start_s {
    ma_trigger_t impl;
    ma_trigger_client_t client;
    ma_bool_t daily_once;
    ma_bool_t run_once;
}ma_trigger_system_start_t;

typedef struct ma_trigger_logon_s {
    ma_trigger_t impl;
    ma_trigger_client_t client;
    ma_bool_t daily_once;
}ma_trigger_logon_t;



static ma_error_t client_trigger_set_begin_date(ma_trigger_t *trigger, ma_task_time_t *start_date);
static ma_error_t client_trigger_set_end_date(ma_trigger_t *trigger, ma_task_time_t *end_date);
static ma_error_t client_trigger_get_type(ma_trigger_t *trigger, ma_trigger_type_t *type);
static ma_error_t client_trigger_get_state(ma_trigger_t *trigger, ma_trigger_state_t *state);
static ma_error_t client_trigger_get_id(ma_trigger_t *trigger, const char **trigger_id);
static ma_error_t client_trigger_get_begin_date(ma_trigger_t *trigger, ma_task_time_t *begin_date);
static ma_error_t client_trigger_get_end_date(ma_trigger_t *trigger, ma_task_time_t *end_date);
static ma_error_t client_trigger_get_policy(ma_trigger_t *trigger, void *policy);
static ma_error_t client_trigger_release(ma_trigger_t *trigger);
static ma_error_t client_trigger_set_state(ma_trigger_t *trigger, ma_trigger_state_t state);
static ma_error_t client_trigger_add_ref(ma_trigger_t *trigger);
static ma_error_t client_trigger_set_next_fire_date(ma_trigger_t *trigger, ma_task_time_t date);
static ma_error_t client_trigger_get_next_fire_date(ma_trigger_t *trigger, ma_task_time_t *date);
static ma_error_t client_trigger_set_last_fire_date(ma_trigger_t *trigger, ma_task_time_t date);
static ma_error_t client_trigger_get_last_fire_date(ma_trigger_t *trigger, ma_task_time_t *date);
static ma_error_t client_trigger_validate_policy(ma_trigger_t *trigger);

static const ma_trigger_methods_t trigger_methods = {
    &client_trigger_set_begin_date,
    &client_trigger_set_end_date,
    &client_trigger_get_type,
    &client_trigger_get_state,
    &client_trigger_get_id,
    &client_trigger_get_begin_date,
    &client_trigger_get_end_date,
    &client_trigger_get_policy,
    &client_trigger_set_state,
    &client_trigger_release,
    &client_trigger_add_ref,
    &client_trigger_set_next_fire_date,
    &client_trigger_get_next_fire_date,
    &client_trigger_set_last_fire_date,
    &client_trigger_get_last_fire_date,
    &client_trigger_validate_policy
};

ma_error_t client_trigger_set_begin_date(ma_trigger_t *trigger, ma_task_time_t *date) {
    if(trigger && trigger->data) {
        ma_error_t err = MA_ERROR_SCHEDULER_INVALID_DATE;
        ma_task_time_t dummy = {0};
        
        if(date) {
            if(memcmp(&date, &dummy, sizeof(ma_task_time_t))) {
                if(ma_date_validator(date->day, date->month, date->year)) {
                    if(ma_time_validator(date->hour, date->minute, date->secs)) {
                        memcpy(&((ma_trigger_client_t*)trigger->data)->begin_date, date, sizeof(ma_task_time_t)); 
                        err = MA_OK;
                    }
                }
            }
        } else {
            get_localtime((ma_task_time_t*)&(((ma_trigger_client_t*)trigger->data)->begin_date)); 
            err = MA_OK;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t client_trigger_set_end_date(ma_trigger_t *trigger, ma_task_time_t *date) {
    if(trigger && trigger->data && date) {
        ma_error_t err = MA_OK;        
        ma_task_time_t today = {0};

        memcpy(&((ma_trigger_client_t*)trigger->data)->end_date, date, sizeof(ma_task_time_t));
        if(memcmp(date, &today, sizeof(ma_task_time_t))) {
            err = MA_ERROR_SCHEDULER_INVALID_DATE;
            if(ma_date_validator(date->day, date->month, date->year)) {
                if(ma_time_validator(date->hour, date->minute, date->secs)) {
                    if(validate_date(*date)) {
                        err = MA_OK;
                    }
                }
            }
        }
        //if(MA_OK != err)
        //    memset(&((ma_trigger_client_t*)trigger->data)->end_date, 0, sizeof(ma_task_time_t));

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t client_trigger_get_type(ma_trigger_t *trigger, ma_trigger_type_t *type) {
    if(trigger && trigger->data && type) {
        *type = ((ma_trigger_client_t*)trigger->data)->type;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t client_trigger_get_state(ma_trigger_t *trigger, ma_trigger_state_t *state) {
    if(trigger && trigger->data && state) {
        *state = ((ma_trigger_client_t*)trigger->data)->state;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t client_trigger_get_id(ma_trigger_t *trigger, const char **id) {
    if(trigger && trigger->data && id) {
        *id = ((ma_trigger_client_t*)trigger->data)->id;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t client_trigger_get_begin_date(ma_trigger_t *trigger, ma_task_time_t *date) {
    return(trigger && trigger->data && date && memcpy(date, &((ma_trigger_client_t*)trigger->data)->begin_date, sizeof(ma_task_time_t))) ? MA_OK  : MA_ERROR_INVALIDARG;
}

ma_error_t client_trigger_get_end_date(ma_trigger_t *trigger, ma_task_time_t *end_date) {
    return(trigger && trigger->data && end_date && memcpy(end_date, &((ma_trigger_client_t*)trigger->data)->end_date, sizeof(ma_task_time_t))) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t client_trigger_set_state(ma_trigger_t *trigger, ma_trigger_state_t state) {
    if(trigger && trigger->data)  {
        ((ma_trigger_client_t*)trigger->data)->state = state;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t client_trigger_get_policy(ma_trigger_t *trigger, void *policy) {
     if(trigger && policy) {
         ma_error_t err = MA_OK;

         switch(((ma_trigger_client_t*)trigger->data)->type) {
            case MA_TRIGGER_NOW: {
                ((ma_trigger_run_now_policy_t*)policy)->now = ((ma_trigger_run_now_t*)trigger)->now;
                break;
            }
            case MA_TRIGGER_ONCE: {
                ((ma_trigger_run_once_policy_t*)policy)->once = ((ma_trigger_run_once_t*)trigger)->once;
                break;
            }
            case MA_TRIGGER_DAILY: {
                ((ma_trigger_daily_policy_t*)policy)->days_interval = ((ma_trigger_daily_t*)trigger)->days_interval;
                break;
            }
            case MA_TRIGGER_WEEKLY: {
                ((ma_trigger_weekly_policy_t*)policy)->days_of_the_week = ((ma_trigger_weekly_t*)trigger)->days_of_the_week;
                ((ma_trigger_weekly_policy_t*)policy)->weeks_interval = ((ma_trigger_weekly_t*)trigger)->weeks_interval;
                break;
            }
            case MA_TRIGGER_MONTHLY_DATE: {
                ((ma_trigger_monthly_date_policy_t*)policy)->date = ((ma_trigger_monthly_date_t*)trigger)->date;
                ((ma_trigger_monthly_date_policy_t*)policy)->months = ((ma_trigger_monthly_date_t*)trigger)->months;
                break;
            }
            case MA_TRIGGER_MONTHLY_DOW: {
                ((ma_trigger_monthly_dow_policy_t*)policy)->day_of_the_week = ((ma_trigger_monthly_dow_t*)trigger)->day_of_the_week;
                ((ma_trigger_monthly_dow_policy_t*)policy)->months = ((ma_trigger_monthly_dow_t*)trigger)->months;
                ((ma_trigger_monthly_dow_policy_t*)policy)->which_week = ((ma_trigger_monthly_dow_t*)trigger)->which_week;
                break;
            }
            case MA_TRIGGER_ON_SYSTEM_IDLE: {
                ((ma_trigger_system_idle_policy_t*)policy)->idle_wait_minutes = ((ma_trigger_system_idle_t*)trigger)->idle_wait_minutes;
                break;
            }
            case MA_TRIGGER_AT_SYSTEM_START: {
                ((ma_trigger_systemstart_policy_t*)policy)->daily_once = ((ma_trigger_system_start_t*)trigger)->daily_once;
                ((ma_trigger_systemstart_policy_t*)policy)->run_just_once = ((ma_trigger_system_start_t*)trigger)->run_once;
                break;
            }
            case MA_TRIGGER_AT_LOGON: {
                ((ma_trigger_logon_policy_t*)policy)->daily_once = ((ma_trigger_logon_t*)trigger)->daily_once;
                break;
            }
            default: {
                err = MA_ERROR_NOT_SUPPORTED;
                break;
            }
         }

         return err;
     }
     return MA_ERROR_INVALIDARG;
}

ma_error_t client_trigger_validate_policy(ma_trigger_t *trigger) {
    if(trigger) {
        ma_error_t err = MA_OK; 
        
        switch(((ma_trigger_client_t*)trigger->data)->type) {
            case MA_TRIGGER_NOW: 
            case MA_TRIGGER_ONCE:
            case MA_TRIGGER_DAILY:
            case MA_TRIGGER_WEEKLY:
            case MA_TRIGGER_MONTHLY_DOW:
            case MA_TRIGGER_ON_SYSTEM_IDLE:
            case MA_TRIGGER_AT_SYSTEM_START:
            case MA_TRIGGER_AT_LOGON: {
                break;
            }
            case MA_TRIGGER_MONTHLY_DATE: {
                ma_bool_t result = MA_FALSE;

                if(((ma_trigger_monthly_date_t*)trigger)->date > 0 && ((ma_trigger_monthly_date_t*)trigger)->date <= 31) {
                    ma_uint16_t months = ((ma_trigger_monthly_date_t*)trigger)->months;
                    ma_uint16_t date = ((ma_trigger_monthly_date_t*)trigger)->date;
                    int month_of_year = -1;
                    ma_task_time_t today = {0};

                    get_localtime(&today);
                    if(-1 == (month_of_year = ma_month_of_year(months))) {
                        if(MA_CHECK_BIT(months, FEBRUARY) || MA_CHECK_BIT(months, APRIL) || MA_CHECK_BIT(months, JUNE) || 
                           MA_CHECK_BIT(months, SEPTEMBER) || MA_CHECK_BIT(months, NOVEMBER)) {
                            result = date < 31 ? MA_TRUE : MA_FALSE;
                        }
                        if(MA_CHECK_BIT(months, JANUARY) || MA_CHECK_BIT(months, MARCH) || MA_CHECK_BIT(months, MAY) || MA_CHECK_BIT(months, JULY) ||
                            MA_CHECK_BIT(months, AUGUST) || MA_CHECK_BIT(months, OCTOBER) || MA_CHECK_BIT(months, DECEMBER)) {
                            result = date <= 31 ? MA_TRUE : MA_FALSE;
                        }
                    } else
                        result = FEBRUARY == month_of_year ? date < 31 ? MA_TRUE : MA_FALSE : ma_date_validator(date, month_of_year, today.year);

                    err = result ? MA_OK : MA_ERROR_SCHEDULER_INVALID_TRIGGER_POLICY;
                }
                break;
            }
            default: {
                err = MA_ERROR_NOT_SUPPORTED;
                break;
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t client_trigger_set_next_fire_date(ma_trigger_t *trigger, ma_task_time_t date) {
    return(trigger && memcpy(&((ma_trigger_client_t*)trigger->data)->next_fire_date, &date, sizeof(ma_task_time_t)))?MA_OK:MA_ERROR_INVALIDARG;
}

ma_error_t client_trigger_set_last_fire_date(ma_trigger_t *trigger, ma_task_time_t date) {
    return(trigger && memcpy(&((ma_trigger_client_t*)trigger->data)->last_fire_date, &date, sizeof(ma_task_time_t)))?MA_OK:MA_ERROR_INVALIDARG;
}

ma_error_t client_trigger_get_next_fire_date(ma_trigger_t *trigger, ma_task_time_t *date) {
    return(trigger && memcpy(date, &((ma_trigger_client_t*)trigger->data)->next_fire_date, sizeof(ma_task_time_t)))?MA_OK:MA_ERROR_INVALIDARG;
}

ma_error_t client_trigger_get_last_fire_date(ma_trigger_t *trigger, ma_task_time_t *date) {
    return(trigger && memcpy(date, &((ma_trigger_client_t*)trigger->data)->last_fire_date, sizeof(ma_task_time_t)))?MA_OK:MA_ERROR_INVALIDARG;
}

/* dtor(s) */
ma_error_t client_trigger_release(ma_trigger_t *trigger) {
     if(trigger) {
         if(!MA_ATOMIC_DECREMENT(((ma_trigger_client_t*)trigger->data)->ref_count)) {
            free(trigger);
         }
         return MA_OK;
     };
     return MA_ERROR_INVALIDARG;
}

ma_error_t client_trigger_add_ref(ma_trigger_t *trigger) {
    if(trigger) {
        MA_ATOMIC_INCREMENT(((ma_trigger_client_t*)trigger->data)->ref_count);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

/* ctor' */
ma_error_t ma_trigger_create_run_now(ma_trigger_run_now_policy_t const *policy, ma_trigger_t **trigger) {
    if(policy && trigger) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;
        ma_trigger_run_now_t *run_now = NULL;

        if((run_now = (ma_trigger_run_now_t*)calloc(1, sizeof(ma_trigger_run_once_t)))) {
            run_now->now = policy->now;
            run_now->client.state = MA_TRIGGER_STATE_NOT_STARTED;
            run_now->client.type = MA_TRIGGER_NOW;
            ma_trigger_init(*trigger = (ma_trigger_t*)run_now, &trigger_methods, &run_now->client);
            advance_current_date_by_seconds(3, &run_now->client.begin_date); //trigger after 3secs
            if(MA_OK == (err = ma_trigger_set_begin_date(*trigger, &run_now->client.begin_date))) {
                (void)ma_trigger_add_ref(*trigger);
            }
        }
        if(MA_OK != err) {
            free(*trigger);
            *trigger = NULL;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_trigger_create_run_once(ma_trigger_run_once_policy_t *policy, ma_trigger_t **trigger) {
    if(policy && trigger) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;
        ma_trigger_run_once_t *run_once = NULL;

        if((run_once = (ma_trigger_run_once_t*)calloc(1, sizeof(ma_trigger_run_once_t)))) {
            run_once->once = policy->once;
            run_once->client.state = MA_TRIGGER_STATE_NOT_STARTED;
            run_once->client.type = MA_TRIGGER_ONCE;
            ma_trigger_init(*trigger = (ma_trigger_t*)run_once, &trigger_methods, &run_once->client);
            if(MA_OK == (err = ma_trigger_set_begin_date(*trigger, NULL))) {
                (void)ma_trigger_add_ref(*trigger);
            }
        }
        if(MA_OK != err) {
            free(*trigger);
            *trigger = NULL;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

 ma_error_t ma_trigger_create_daily(ma_trigger_daily_policy_t *policy, ma_trigger_t **trigger) {
     if(policy && trigger) {
         ma_error_t err = MA_ERROR_OUTOFMEMORY;
         ma_trigger_daily_t *daily = NULL;

         if((daily = (ma_trigger_daily_t*)calloc(1, sizeof(ma_trigger_daily_t)))) {
            daily->days_interval = policy->days_interval;
            daily->client.type = MA_TRIGGER_DAILY;
            daily->client.state = MA_TRIGGER_STATE_NOT_STARTED;
            ma_trigger_init(*trigger = (ma_trigger_t*)daily, &trigger_methods, &daily->client);
            if(MA_OK == (err = ma_trigger_set_begin_date(*trigger, NULL))) {
                (void)ma_trigger_add_ref(*trigger);
            }
         }
        if(MA_OK != err) {
            free(*trigger);
            *trigger = NULL;
        }

         return err;
     }
     return MA_ERROR_INVALIDARG;
}

ma_error_t ma_trigger_create_weekly(ma_trigger_weekly_policy_t *policy, ma_trigger_t **trigger) {
    if(policy && trigger) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;
        ma_trigger_weekly_t *weekly = NULL;

        if((weekly = (ma_trigger_weekly_t*)calloc(1, sizeof(ma_trigger_weekly_t)))) {
            weekly->days_of_the_week = policy->days_of_the_week;
            weekly->weeks_interval = policy->weeks_interval;
            weekly->client.type = MA_TRIGGER_WEEKLY;
            weekly->client.state = MA_TRIGGER_STATE_NOT_STARTED;
            ma_trigger_init(*trigger = (ma_trigger_t*)weekly, &trigger_methods, &weekly->client);
            if(MA_OK == (err = ma_trigger_set_begin_date(*trigger, NULL))) {
                (void)ma_trigger_add_ref(*trigger);
            }
        }
        if(MA_OK != err) {
            free(*trigger);
            *trigger = NULL;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_trigger_create_monthly_date(ma_trigger_monthly_date_policy_t *policy, ma_trigger_t **trigger) {
    if(policy && trigger) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;
        ma_trigger_monthly_date_t *monthly_date = NULL;

        if((monthly_date = (ma_trigger_monthly_date_t*)calloc(1, sizeof(ma_trigger_monthly_date_t)))) {
            monthly_date->date = policy->date;
            monthly_date->months = policy->months;
            monthly_date->client.type = MA_TRIGGER_MONTHLY_DATE;
            monthly_date->client.state = MA_TRIGGER_STATE_NOT_STARTED;
            ma_trigger_init(*trigger = (ma_trigger_t*)monthly_date, &trigger_methods, &monthly_date->client);
            if(MA_OK == (err = ma_trigger_validate_policy(*trigger))) {
                if(MA_OK == (err = ma_trigger_set_begin_date(*trigger, NULL))) {
                    (void)ma_trigger_add_ref(*trigger);
                }
            }
        }
        if(MA_OK != err) {
            free(*trigger);
            *trigger = NULL;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_trigger_create_monthly_dow(ma_trigger_monthly_dow_policy_t *policy, ma_trigger_t **trigger) {
    if(policy && trigger) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;
        ma_trigger_monthly_dow_t *dow = NULL;

        if((dow = (ma_trigger_monthly_dow_t*)calloc(1, sizeof(ma_trigger_monthly_dow_t)))) {
            dow->day_of_the_week = policy->day_of_the_week;
            dow->months = policy->months;
            dow->which_week = policy->which_week;
            dow->client.type = MA_TRIGGER_MONTHLY_DOW;
            dow->client.state = MA_TRIGGER_STATE_NOT_STARTED;
            ma_trigger_init(*trigger = (ma_trigger_t*)dow, &trigger_methods, &dow->client);
            if(MA_OK == (err = ma_trigger_set_begin_date(*trigger, NULL))) {
                (void)ma_trigger_add_ref(*trigger);
            }
        }
        if(MA_OK != err) {
            free(*trigger);
            *trigger = NULL;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_trigger_create_system_idle(ma_trigger_system_idle_policy_t *policy, ma_trigger_t **trigger) {
    if(policy && trigger) {
        //ma_error_t err = MA_ERROR_OUTOFMEMORY;
        //ma_trigger_system_idle_t *system_idle = NULL;

        //if((system_idle = (ma_trigger_system_idle_t*)calloc(1, sizeof(ma_trigger_system_idle_t)))) {
        //    system_idle->idle_wait_minutes = policy->idle_wait_minutes;
        //    system_idle->client.type = MA_TRIGGER_ON_SYSTEM_IDLE;
        //    system_idle->client.state = MA_TRIGGER_STATE_NOT_STARTED;
        //    ma_trigger_init(*trigger =(ma_trigger_t*)system_idle , &trigger_methods, &system_idle->client);
        //    if(MA_OK == (err = ma_trigger_set_begin_date(*trigger, NULL))) {
        //        (void)ma_trigger_add_ref(*trigger);
        //    }
        //}
        //if(MA_OK != err) {
        //    free(*trigger);
        //    *trigger = NULL;
        //}

        //return err;
        return MA_ERROR_NOT_SUPPORTED;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_trigger_create_power_save(ma_trigger_powersave_policy_t *policy, ma_trigger_t **trigger) {
    if(policy && trigger) {
        //ma_error_t err = MA_ERROR_OUTOFMEMORY;
        //ma_trigger_powersave_t *pwrsave = NULL;

        //if((pwrsave = (ma_trigger_powersave_t*)calloc(1, sizeof(ma_trigger_powersave_t)))) {
        //    pwrsave->powersave_deadline_minutes = policy->powersave_deadline_minutes;
        //    pwrsave->powersave_minutes = policy->powersave_minutes;
        //    pwrsave->client.type = MA_TRIGGER_AT_POWER_SAVE;
        //    pwrsave->client.state = MA_TRIGGER_STATE_NOT_STARTED;
        //    ma_trigger_init(*trigger = (ma_trigger_t*)pwrsave, &trigger_methods, &pwrsave->client);
        //    if(MA_OK == (err = ma_trigger_set_begin_date(*trigger, NULL))) {
        //        (void)ma_trigger_add_ref(*trigger);
        //    }
        //}
        //if(MA_OK != err) {
        //    free(*trigger);
        //    *trigger = NULL;
        //}

        //return err;
        return MA_ERROR_NOT_SUPPORTED;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_trigger_create_dialup(ma_trigger_dialup_policy_t *policy, ma_trigger_t **trigger) {
    if(policy && trigger) {
        //ma_error_t err = MA_ERROR_OUTOFMEMORY;

        //if((*trigger = (ma_trigger_t*)calloc(1, sizeof(ma_trigger_dialup_t)))) {
        //    ((ma_trigger_dialup_t*)*trigger)->daily_once = policy->daily_once;
        //    (((ma_trigger_client_t*)*trigger))->type = MA_TRIGGER_ON_DIALUP;
        //    (((ma_trigger_client_t*)*trigger))->state = MA_TRIGGER_STATE_NOT_STARTED;
        //    ma_trigger_init(*trigger, &trigger_methods, trigger);
        //    if(MA_OK == (err = ma_trigger_set_begin_date(*trigger, NULL))) {
        //        (void)ma_trigger_add_ref(*trigger);
        //    }
        //}
        //if(MA_OK != err) {
        //    free(*trigger);
        //    *trigger = NULL;
        //}

        //return err;
        return MA_ERROR_NOT_SUPPORTED;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_trigger_create_system_start(ma_trigger_systemstart_policy_t *policy, ma_trigger_t **trigger) {
    if(policy && trigger) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;
        ma_trigger_system_start_t *sys_startup = NULL;

        if((sys_startup = (ma_trigger_system_start_t*)calloc(1, sizeof(ma_trigger_system_start_t)))) {
            sys_startup->daily_once = policy->daily_once;
            sys_startup->run_once = policy->run_just_once;
            sys_startup->client.type = MA_TRIGGER_AT_SYSTEM_START;
            sys_startup->client.state = MA_TRIGGER_STATE_NOT_STARTED;
            ma_trigger_init(*trigger = (ma_trigger_t*)sys_startup, &trigger_methods, &sys_startup->client);
            if(MA_OK == (err = ma_trigger_set_begin_date(*trigger, NULL))) {
                (void)ma_trigger_add_ref(*trigger);
            }
        }
        if(MA_OK != err) {
            free(*trigger);
            *trigger = NULL;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_trigger_create_logon(ma_trigger_logon_policy_t *policy, ma_trigger_t **trigger) {
    if(policy && trigger) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;
        ma_trigger_logon_t *logon = NULL;

        if((logon = (ma_trigger_logon_t*)calloc(1, sizeof(ma_trigger_logon_t)))) {
            logon->daily_once = policy->daily_once;
            logon->client.type = MA_TRIGGER_AT_LOGON;
            logon->client.state = MA_TRIGGER_STATE_NOT_STARTED;
            ma_trigger_init(*trigger = (ma_trigger_t*)logon, &trigger_methods, &logon->client);
            if(MA_OK == (err = ma_trigger_set_begin_date(*trigger, NULL))) {
                (void)ma_trigger_add_ref(*trigger);
            }
        }
        if(MA_OK != err) {
            free(*trigger);
            *trigger = NULL;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_trigger_set_id(ma_trigger_t *trigger, const char *id) {
    return(trigger && trigger->data && id && (strlen(id) < MA_MAX_TRIGGERID_LEN) && strncpy(((ma_trigger_client_t*)trigger->data)->id, id, strlen(id))) ? MA_OK : MA_ERROR_INVALIDARG;
}

MA_CPP(})
