#include "ma/internal/clients/relay/ma_relay_discovery_request.h"
#include "ma/internal/clients/udp/ma_udp_request.h"

#include "ma/internal/defs/ma_relay_defs.h"
#include "ma/internal/defs/ma_udp_defs.h"
#include "ma/logger/ma_logger.h"
#include "ma/internal/ma_macros.h"

#include "ma/internal/utils/threading/ma_atomic.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef WIN32
#define tokenize	strtok_s
#else
#define tokenize	strtok_r
#endif 

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME				"relay_client"

#define RELAY_ADDR_SIZE					65

typedef struct ma_relay_s{		
	ma_uint32_t		relay_port;

	ma_uint32_t		nic_scope_id;

	char			relay_addr[RELAY_ADDR_SIZE];
}ma_relay_t;

struct ma_relay_discovery_session_s{
	ma_bool_t									stop_discovery;

	ma_proxy_list_t								*relay_list;

	size_t										relay_count;

	ma_udp_request_t							*discovery_msgbus; 

	ma_udp_request_t							*discovery_compat; 

	ma_bool_t									discovery_started;

	char										relay_names[(RELAY_ADDR_SIZE+2)*10];
	size_t										relay_names_count;
};

struct ma_relay_discovery_request_s{
	
	ma_udp_client_t								*udp_client;

	ma_logger_t									*logger;

	ma_relay_discovery_policy_t					policy;
	
	ma_proxy_list_t								*proxy_list;
		
	ma_atomic_counter_t							ref_count;

	relay_discovery_final_callback_t			cb;

	void										*user_data;

	struct ma_relay_discovery_session_s			*session;
};

static ma_bool_t is_relay_discovery_in_progress(ma_relay_discovery_request_t *self){ return (NULL != self->session); }

ma_error_t ma_relay_discovery_request_create(ma_udp_client_t *udp_client, ma_relay_discovery_request_t **request){
	if(udp_client && request){
		ma_relay_discovery_request_t *req = (ma_relay_discovery_request_t*) calloc(1, sizeof(ma_relay_discovery_request_t));
		if(req){
			req->udp_client = udp_client;
			MA_ATOMIC_INCREMENT(req->ref_count);
			(void)ma_udp_client_get_logger(req->udp_client, &req->logger);
			*request = req;
			return MA_OK;			
		}
		return MA_ERROR_OUTOFMEMORY;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_relay_discovery_request_set_final_cb(ma_relay_discovery_request_t *self, relay_discovery_final_callback_t cb, void *user_data){
	if(self && cb){
		self->cb = cb;
		self->user_data = user_data;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_relay_discovery_request_set_policy(ma_relay_discovery_request_t *self, ma_relay_discovery_policy_t *policy){
	if(self && policy && policy->discovery_multicast_addr && policy->discovery_port){
		if(self->policy.discovery_multicast_addr)
			free(self->policy.discovery_multicast_addr);
		self->policy.discovery_multicast_addr = strdup(policy->discovery_multicast_addr);
		self->policy.discovery_port = policy->discovery_port;
		self->policy.discovery_timeout_ms = policy->discovery_timeout_ms;
		self->policy.max_no_of_relays_to_discover = policy->max_no_of_relays_to_discover;
		self->policy.sync_discovery = policy->sync_discovery;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

#define RELAY_FQDN_QUALIFIER		"FQDN"
#define RELAY_HOSTNAME_QUALIFIER	"HOST_NAME"
#define RELAY_PORT_QUALIFIER		"PORT"

static ma_error_t add_relay(ma_relay_discovery_request_t *self, ma_relay_t *relay){	
	ma_proxy_t *proxy = NULL;
	MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Received relay discovery response, relay info, [addr:%s port:%d scoped id:%d]", relay->relay_addr, relay->relay_port, relay->nic_scope_id);							
	
	{
		/*this is to avoid the duplicate relay discovered.*/
		char sub_str[RELAY_ADDR_SIZE+2] = {0};		
		MA_MSC_SELECT(_snprintf, snprintf)(sub_str, sizeof(sub_str), "%s;", relay->relay_addr);

		if(self->session->relay_count && strstr(self->session->relay_names, sub_str)){
			MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Relay is already discovered");		
			return MA_OK;
		}
		else{
			if(self->session->relay_names_count < 10){
				strcat(self->session->relay_names, sub_str);
				++self->session->relay_names_count;
			}
		}
	}

	if(MA_OK == ma_proxy_create(&proxy)){
		(void)ma_proxy_set_address(proxy, relay->relay_addr);
		(void)ma_proxy_set_scopeid(proxy, relay->nic_scope_id);
		(void)ma_proxy_set_port(proxy, relay->relay_port);								
		(void)ma_proxy_list_add_proxy(self->session->relay_list, proxy);
		(void)ma_proxy_release(proxy);
		self->session->relay_count++;
		return MA_OK;
	}
	return MA_ERROR_OUTOFMEMORY;
}

static ma_error_t add_compat_relay_discovery_response(ma_relay_discovery_request_t *self, ma_udp_response_t *response, ma_bool_t *stop){
	ma_buffer_t *raw_msg = NULL;
	ma_uint16_t	relay_count = 0;
	ma_error_t       rc  = MA_ERROR_RELAY_INVALID_RESPONSE;
	
	if(MA_OK == ma_udp_msg_get_raw_message(response->response, &raw_msg)){
		char *message		 = NULL;
		size_t size			 = 0;	
		if(MA_OK == ma_buffer_get_string(raw_msg, &message, &size)){
			char *field_delimiter = ",";
			char *delimiter = "=";
			char *token1, *token2;
			char *field;			
			
			for(field = tokenize(message, field_delimiter, &token1); field ; field = tokenize(NULL, field_delimiter, &token1)){
				char *key = NULL,*value = NULL;
				key = tokenize(field, delimiter, &token2);
				value = tokenize(NULL, delimiter, &token2);
				if(value && key && (strcmp(key,RELAY_PORT_QUALIFIER) == 0) ){
					char *relay_footer = strstr(value, "RELA");
					if(relay_footer){						
						ma_uint32_t relay_port = 0; 
						*relay_footer = '\0';
						relay_port = atoi(value);
						if(relay_port > 0 && relay_port <= 65535){
							ma_relay_t relay;
						
							relay.nic_scope_id = response->receiving_local_nic;
							relay.relay_port   = relay_port;
							MA_MSC_SELECT(_snprintf, snprintf)(relay.relay_addr, 65, "%s", response->peer_addr);			
							rc = add_relay(self, &relay);							
							break;
						}
					}
				}				
			}			
		}				
		(void)ma_buffer_release(raw_msg);
	}
	
	if(MA_OK != rc) MA_LOG(self->logger, MA_LOG_SEV_ERROR, "malformed compat relay discovery response.");

	if(self->session->relay_count == self->policy.max_no_of_relays_to_discover){
		MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Stopping relay discovery, as max discovery limit reached.");							
		*stop = self->session->stop_discovery = MA_TRUE;	
	}
	return rc;
}

static ma_error_t add_relay_discovery_response(ma_relay_discovery_request_t *self, ma_udp_response_t *response, ma_bool_t *stop){
	ma_message_t *msg = NULL;
	ma_error_t rc = MA_ERROR_RELAY_INVALID_RESPONSE;
	if(MA_OK == ma_udp_msg_get_ma_message(response->response, &msg)){
		const char *port = NULL;
		ma_uint16_t	relay_count = 0;
		ma_message_get_property(msg, MA_RELAY_KEY_SERVER_PORT_INT, &port);
		if(port){
			ma_uint32_t relay_port = atoi(port);
			if(relay_port > 0 && relay_port <= 65535){
				ma_relay_t relay;
						
				relay.nic_scope_id = response->receiving_local_nic;
				relay.relay_port   = relay_port;
				MA_MSC_SELECT(_snprintf, snprintf)(relay.relay_addr, 65, "%s", response->peer_addr);							

				rc = add_relay(self, &relay);

				if(self->session->relay_count == self->policy.max_no_of_relays_to_discover){
					MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Stopping relay discovery, as max discovery limit reached.");							
					*stop = self->session->stop_discovery = MA_TRUE;	
				}
			}
		}
		ma_message_release(msg);
	}

	if(MA_OK != rc) MA_LOG(self->logger, MA_LOG_SEV_ERROR, "malformed relay discovery response.");	

	return rc;
};

static ma_error_t relay_discovery_response_cb(ma_udp_request_t *request, ma_udp_response_t *response, void *user_data, ma_bool_t *stop){
	ma_relay_discovery_request_t *self = (ma_relay_discovery_request_t*)user_data;
	ma_udp_msg_type_t type;

	if(self->session->stop_discovery){
		*stop = MA_TRUE;
		return MA_OK;
	}

	(void)ma_udp_msg_get_type(response->response, &type);
	MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Relay discovery response in format \"%s\" received.", MA_UDP_MESSAGE_TYPE_RAW == type ? "compat" : "msgbus");

	if(MA_UDP_MESSAGE_TYPE_RAW == type)
		return add_compat_relay_discovery_response(self, response, stop);
	else
		return add_relay_discovery_response(self, response, stop);
}

static void session_cleanup(ma_relay_discovery_request_t *self){
	MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Relay discovery session is over.");
	if(self->proxy_list) ma_proxy_list_release(self->proxy_list);
	self->proxy_list = NULL;

	MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Relay discovered in the network %d.", self->session->relay_count);
	if(self->session->relay_count)
		self->proxy_list = self->session->relay_list;			
	else
		ma_proxy_list_release(self->session->relay_list);	
	free(self->session);
	self->session = NULL;

	if(self->cb) 
		self->cb(self->user_data);

}

static void relay_discovery_final_cb(ma_error_t rc, ma_udp_request_t *request, void *user_data){
	ma_relay_discovery_request_t *self = (ma_relay_discovery_request_t*)user_data;

	if(self->session->discovery_msgbus == request){
		MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Relay discovery final cb.");
		(void)ma_udp_request_release(self->session->discovery_msgbus);		
		self->session->discovery_msgbus = NULL;
	}

	if(self->session->discovery_compat == request){
		MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Compat relay discovery final cb.");
		(void)ma_udp_request_release(self->session->discovery_compat);		
		self->session->discovery_compat = NULL;
	}
	
	if(self->session->discovery_msgbus == self->session->discovery_compat){
		MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Relay discovery over, cleaning up session.");
		session_cleanup(self);
	}
	
	(void)ma_relay_discovery_request_release(self);
}

static ma_udp_msg_t *compat_relay_discovery_message_create(){
	ma_udp_msg_t *msg = NULL;
	if(MA_OK == ma_udp_msg_create(&msg)){
		ma_buffer_t *raw_msg = NULL;
		char formatted_msg[64] = {0};
		MA_MSC_SELECT(_snprintf, snprintf)(formatted_msg, 64, "%s%s%s",MA_COMPAT_RELAY_SERVICE_ID, MA_COMPAT_RELAY_DISCOVERY_MESSAGE, MA_COMPAT_RELAY_SERVICE_ID);
		
		if(MA_OK == ma_buffer_create(&raw_msg, strlen(formatted_msg))){
			(void)ma_buffer_set(raw_msg, formatted_msg, strlen(formatted_msg));
			(void)ma_udp_msg_set_raw_message(msg, raw_msg);
			(void)ma_buffer_release(raw_msg);
			return msg;
		}
		(void)ma_udp_msg_release(msg);
	}
	return NULL;
}

static ma_udp_msg_t *relay_discovery_message_create(){
	ma_udp_msg_t *msg = NULL;
	if(MA_OK == ma_udp_msg_create(&msg)){
		ma_message_t *ma_msg = NULL;
		if(MA_OK == ma_message_create(&ma_msg)){
			(void)ma_message_set_property(ma_msg, MA_RELAY_MSG_PROP_KEY_TYPE_STR, MA_RELAY_MSG_PROP_VAL_REQUEST_RELAY_DISCOVERY_STR);
			(void)ma_udp_msg_set_ma_message(msg, ma_msg);
			(void)ma_message_release(ma_msg);
			return msg;
		}
		(void)ma_udp_msg_release(msg);
	}
	return NULL;
}

static ma_error_t run_relay_discovery(ma_relay_discovery_request_t *self, ma_bool_t is_compat){
	ma_error_t rc = MA_OK;		
	ma_udp_msg_t *udp_msg[1] = {0};
	ma_bool_t sent_success = MA_FALSE;
	udp_msg[0] = is_compat ? compat_relay_discovery_message_create() : relay_discovery_message_create();
	
	if(udp_msg[0]){
		ma_udp_request_t *udp_request = NULL;
		MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Created relay discovery message in \"%s\" format.", (is_compat ? "compat": "msgbus"));
		
		if(MA_OK == (rc = ma_udp_request_create(self->udp_client, &udp_request))){
				
			(void)ma_udp_request_set_multicast_addr(udp_request, (is_compat ?  MA_UDP_MULTICAST_ADDR_STR : self->policy.discovery_multicast_addr) );
			(void)ma_udp_request_set_port(udp_request, (is_compat ? MA_RELAY_COMPAT_PORT : self->policy.discovery_port) );				
			(void)ma_udp_request_set_timeout(udp_request, self->policy.discovery_timeout_ms);				
			(void)ma_udp_request_set_callbacks(udp_request, NULL, relay_discovery_response_cb, self);	

			if(MA_OK == (rc = ma_udp_request_set_message(udp_request, udp_msg, 1))){								
				if(self->policy.sync_discovery){
					MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Sending relay discovery message via sync way.");
					if(MA_OK == (rc = ma_udp_request_send(udp_request)))
						self->session->discovery_started = MA_TRUE;													
					else
						MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Relay discovery in sync way failed, error = %d.", rc);
				}
				else{
					MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Sending relay discovery message via async way.");
					if(MA_OK == (rc = ma_udp_request_async_send(udp_request, relay_discovery_final_cb))){
						if(is_compat){
							MA_ATOMIC_INCREMENT(self->ref_count);
							self->session->discovery_compat = udp_request;
						}
						else{
							MA_ATOMIC_INCREMENT(self->ref_count);
							self->session->discovery_msgbus = udp_request;
						}
						udp_request = NULL;
						self->session->discovery_started = MA_TRUE;							
					}
					else
						MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Relay discovery in async way failed, error = %d.", rc);
				}
			}
			else
				MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Relay discovery message setting to udp request failed, error = %d.", rc);
		}
		else
			MA_LOG(self->logger, MA_LOG_SEV_ERROR, "udp request creation failed, error = %d.", rc);

		(void)ma_udp_msg_release(udp_msg[0]);		

		if(udp_request){	
			(void)ma_udp_request_release(udp_request);		
			udp_request = NULL;
		}
	}
	else
		MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to create relay discovery message.");

	/*if failed the free up the discovery data.*/
	return self->session->discovery_started ? MA_OK : MA_ERROR_RELAY_DISCOVERY_FAILED;
}

ma_error_t ma_relay_discovery_request_send(ma_relay_discovery_request_t *self){
	if(self){		
		ma_error_t rc = MA_ERROR_OUTOFMEMORY;
		if(is_relay_discovery_in_progress(self)){
			MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Relay discovery already active.");
			return MA_ERROR_RELAY_DISCOVERY_INPROGRESS;
		}

		if(!(self->policy.discovery_multicast_addr && self->policy.discovery_port))
			return MA_ERROR_PRECONDITION;
		
		self->session = (struct ma_relay_discovery_session_s*) calloc(1, sizeof(struct ma_relay_discovery_session_s));
		if(self->session){
			self->session->relay_count		 = 0;
			self->session->stop_discovery	 = MA_FALSE;
			self->session->discovery_started = MA_FALSE;

			if(MA_OK == (rc = ma_proxy_list_create(&self->session->relay_list))){														
				if(MA_OK != (rc = run_relay_discovery(self, MA_FALSE))){
					MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed in the relay discovery, rc = <%d>", rc);
				}
				if(!self->session->stop_discovery){
					if(MA_OK != (rc = run_relay_discovery(self, MA_TRUE))){
						MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed in the compat relay discovery, rc = <%d>", rc);
					}
				}				
			}
			else
				MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to create relay list, rc = <%d>", rc);

			if(self->policy.sync_discovery)				
				session_cleanup(self);			
			else if(!self->session->discovery_started){
				session_cleanup(self);	
				rc = MA_ERROR_RELAY_DISCOVERY_FAILED;
			}
		}
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_relay_discovery_request_cancel(ma_relay_discovery_request_t *self){
	if(self){
		if(self->session)
			self->session->stop_discovery = MA_TRUE;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_relay_discovery_request_get_relays(ma_relay_discovery_request_t *self, const ma_proxy_list_t **relay_list){
	if(self && relay_list){
		if(self->proxy_list){
			*relay_list = self->proxy_list;
			return MA_OK;
		}
		else if(self->session && self->session->relay_count){
			*relay_list = self->session->relay_list;	
			return MA_OK;
		}
		return MA_ERROR_RELAY_NONE;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_relay_discovery_request_release(ma_relay_discovery_request_t *self){
	if(self){
		if(!MA_ATOMIC_DECREMENT(self->ref_count)){
			MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Destroying relay discovery request.");
			if(self->proxy_list) ma_proxy_list_release(self->proxy_list);
			if(self->policy.discovery_multicast_addr) free(self->policy.discovery_multicast_addr);
			free(self);		
		}
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}




