#include "ma/internal/clients/relay/ma_relay_list.h"
#include "ma/internal/utils/datastructures/ma_vector.h"

#include "ma/internal/utils/threading/ma_atomic.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct ma_relay_list_s{	

	ma_atomic_counter_t			ref_cnt; 

    ma_vector_define(relay_vec, ma_relay_t*);
};

ma_error_t ma_relay_list_create(ma_uint16_t capacity, ma_relay_list_t **relay_list){
	if(relay_list){
		ma_relay_list_t *self = (ma_relay_list_t*) calloc(1, sizeof(ma_relay_list_t));
		if(self){
			MA_ATOMIC_INCREMENT(self->ref_cnt) ;
			ma_vector_init(self, capacity, relay_vec, ma_relay_t*);
			*relay_list = self;
			return MA_OK;
		}
		return MA_ERROR_OUTOFMEMORY;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_relay_list_add_relay(ma_relay_list_t *self, ma_relay_t *relay){
	if(self && relay){
		ma_relay_t *r = (ma_relay_t*) calloc(1, sizeof(ma_relay_t));
		if(r){
			memcpy(r, relay, sizeof(ma_relay_t));
			ma_vector_push_back(self, r, ma_relay_t*, relay_vec);
			return MA_OK;
		}
		return MA_ERROR_OUTOFMEMORY;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_relay_list_add_ref(ma_relay_list_t *self){
	if(self){
		MA_ATOMIC_INCREMENT(self->ref_cnt) ;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

static void ma_relay_release(ma_relay_t *relay){
	if(relay)free(relay);	
}

ma_error_t ma_relay_list_release(ma_relay_list_t *self){
	if(self){
		if(0 == MA_ATOMIC_DECREMENT(self->ref_cnt)) {
			ma_vector_clear(self, ma_relay_t*, relay_vec, ma_relay_release);			
			free(self) ;
		}
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_relay_list_get_count(ma_relay_list_t *self, ma_uint16_t *count){
	if(self && count){
		*count = ma_vector_get_size(self, relay_vec);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_relay_list_get_relay(ma_relay_list_t *self, ma_uint16_t index, ma_relay_t *relay){
	if(self && relay){
		ma_relay_t *r = ma_vector_at(self, index, ma_relay_t*, relay_vec);
		if(r){
			memcpy(relay, r, sizeof(ma_relay_t));
			return MA_OK;
		}
		return MA_ERROR_OBJECTNOTFOUND;
	}
	return MA_ERROR_INVALIDARG;
}
