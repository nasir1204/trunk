#include "ma/internal/clients/datachannel/ma_datachannel_internal.h"
#include "ma/datachannel/ma_datachannel.h"
#include "ma/internal/defs/ma_datachannel_defs.h"
#include "ma/internal/utils/datastructures/ma_map.h"
#include "ma/internal/utils/platform/ma_sys_apis.h"
#include "ma/internal/clients/ma/ma_client_internal.h"
#include "ma/internal/clients/ma/ma_client_base.h"
#include "ma/internal/clients/ma/client/ma_client_manager.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/ma_strdef.h"
#include "ma/ma_msgbus.h"
#include "ma/internal/defs/ma_object_defs.h"


#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define LOG_FACILITY_NAME "datachannel_client"
#define MA_DATACHANNEL_ASYNC_SEND_TIMEOUT	60 *1000
MA_MAP_DECLARE(ma_dc_subscribers, char *, ma_msgbus_subscriber_t *);
/*We are owning the responsibility to cleanup msgbus subscriber objects so before removing entry from map, get value and cleanup*/
MA_MAP_DEFINE(ma_dc_subscribers, char *, strdup, free, ma_msgbus_subscriber_t *, , , strcmp);

typedef struct ma_datachannel_client_s {
    ma_client_base_t                    base_client;
    ma_client_t                         *ma_client;
    //map of message_id to corresponding subscriber object
    ma_msgbus_subscriber_t              *notification_subscriber;
    MA_MAP_T(ma_dc_subscribers)         *subscribers;
    ma_datachannel_on_message_cb_t      on_message_cb;
    ma_datachannel_notification_cb_t    on_notification_cb;
    void                                *on_notify_cb_data;
    void                                *on_message_cb_data;
    char product_id[MAX_CLIENT_NAME_LENGTH];
}ma_datachannel_client_t, *ma_datachannel_client_h;

typedef ma_error_t (*on_message_)(ma_client_base_t *client, const char *topic, ma_message_t *message);
typedef ma_error_t (*release_)(ma_client_base_t *client);

static ma_error_t ma_datachannel_client_release(ma_datachannel_client_t *client);
static ma_error_t ma_datachannel_notify_on_message(ma_client_base_t *client, const char *topic, ma_message_t *message);
static ma_error_t ma_datachannel_notification_subscription_callback(const char *topic, ma_message_t *payload, void *cb_data);
static ma_error_t ma_datachannel_msgbus_subscription_callback(const char *topic, ma_message_t *payload, void *cb_data);

struct ma_client_base_methods_s base_methods = {
    (on_message_) ma_datachannel_notify_on_message,
    (release_) ma_datachannel_client_release
};

static ma_error_t ma_datachannel_client_create(ma_client_t *ma_client, const char *product_id, ma_datachannel_client_t **dc_client) {
    if(ma_client && product_id ) {
        ma_error_t rc = MA_ERROR_OUTOFMEMORY;
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;
        ma_msgbus_t *msgbus = NULL;
        ma_logger_t *logger = NULL;
        ma_client_get_context(ma_client, &ma_context);
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);
        msgbus = MA_CONTEXT_GET_MSGBUS(ma_context);
        logger = MA_CONTEXT_GET_LOGGER(ma_context);

        if(ma_context && client_manager && msgbus) {
            ma_datachannel_client_t *self = NULL;

            if(NULL != (self = (ma_datachannel_client_t*)calloc(1,sizeof(ma_datachannel_client_t)))) {
                ma_msgbus_consumer_reachability_t reach = MSGBUS_CONSUMER_REACH_OUTPROC;
                ma_msgbus_callback_thread_options_t t_option = MA_MSGBUS_CALLBACK_THREAD_POOL;

                self->ma_client = ma_client;
                strncpy(self->product_id, product_id, strlen(product_id));
                (void)ma_client_base_init((ma_client_base_t *)self, &base_methods, self);
                if(MA_OK == (rc = MA_MAP_CREATE(ma_dc_subscribers,self->subscribers))) {
                    (void) ma_client_manager_get_consumer_reachability(client_manager, &reach);
                    (void) ma_client_manager_get_thread_option(client_manager, &t_option);

                    if(MA_OK == (rc = ma_msgbus_subscriber_create(msgbus, &self->notification_subscriber))) {
                        (void)ma_msgbus_subscriber_setopt(self->notification_subscriber,MSGBUSOPT_THREADMODEL, t_option);
                        (void)ma_msgbus_subscriber_setopt(self->notification_subscriber,MSGBUSOPT_REACH, reach);

                        if(MA_OK == (rc = ma_msgbus_subscriber_register(self->notification_subscriber, EPO_DATACHANNEL_NOTIFICATION_PUBLISHER_TOPIC_STR, &ma_datachannel_notification_subscription_callback, self))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "registered for notifications on %s for product %s.", EPO_DATACHANNEL_NOTIFICATION_PUBLISHER_TOPIC_STR, product_id);
                            *dc_client = self;
                            return MA_OK;
                        }
                        ma_msgbus_subscriber_release(self->notification_subscriber);
                    }
                    MA_MAP_RELEASE(ma_dc_subscribers,self->subscribers);
                    self->subscribers = NULL;
                }
                free(self);
                *dc_client = NULL;
            }
        }
        MA_LOG(logger, MA_LOG_SEV_CRITICAL, "Unexpected error. rc = %d",rc);
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_datachannel_client_release(ma_datachannel_client_t *self) {
    if(self) {
        size_t size = 0;

        MA_MAP_SIZE(ma_dc_subscribers,self->subscribers, size);
        while(size > 0) {
            MA_MAP_FOREACH(ma_dc_subscribers, self->subscribers, iter){
                ma_msgbus_subscriber_t *subscriber = iter.second;
                MA_MAP_REMOVE_ENTRY(ma_dc_subscribers, self->subscribers, iter.first);
                ma_msgbus_subscriber_unregister(subscriber);
                ma_msgbus_subscriber_release(subscriber);
                MA_MAP_SIZE(ma_dc_subscribers,self->subscribers, size);
                break;
            }
        }
        MA_MAP_RELEASE(ma_dc_subscribers, self->subscribers);

        (void) ma_msgbus_subscriber_unregister(self->notification_subscriber);
        (void) ma_msgbus_subscriber_release(self->notification_subscriber);
        self->notification_subscriber = NULL;
        free(self);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_datachannel_register_notification_callback(ma_client_t *ma_client, const char *product_id, ma_datachannel_notification_cb_t notify_cb, void *cb_data) {
    if(ma_client && product_id && notify_cb) {
        ma_error_t rc = MA_OK;
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;
        ma_msgbus_t *msgbus = NULL;
        ma_logger_t *logger = NULL;
        ma_client_get_context(ma_client, &ma_context);
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);
        msgbus = MA_CONTEXT_GET_MSGBUS(ma_context);
        logger = MA_CONTEXT_GET_LOGGER(ma_context);

        if(ma_context && client_manager && msgbus) {
            char client_name[MAX_CLIENT_NAME_LENGTH] = {0};
            ma_datachannel_client_t *dc_client = NULL;
            MA_MSC_SELECT(_snprintf, snprintf)(client_name, MAX_CLIENT_NAME_LENGTH,"%s.%s",MA_DATACHANNEL_CLIENT_NAME_STR, product_id);
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "The client %s",client_name);
            if(ma_client_manager_has_client(client_manager, client_name)) {
                (void)ma_client_manager_get_client(client_manager, client_name, (ma_client_base_t **) &dc_client);
                if(dc_client->on_notification_cb) {
                    rc = MA_ERROR_ALREADY_REGISTERED;
                    MA_LOG(logger, MA_LOG_SEV_WARNING, "The client %s is already registered with notification callback. error code = %d", product_id, rc);
                }
                else {
                    dc_client->on_notification_cb = notify_cb;
                    dc_client->on_notify_cb_data = cb_data;
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "Registered notifcation callback succesfully.");
                }
            }
            else {
                if(MA_OK == (rc = ma_datachannel_client_create(ma_client, product_id, &dc_client))) {
                    dc_client->on_notification_cb = notify_cb;
                    dc_client->on_notify_cb_data = cb_data;
                    (void)ma_client_manager_add_client(client_manager, client_name, (ma_client_base_t *) dc_client);
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "Registered notifcation callback succesfully.");
                }
            }
            return rc;
        }
        return MA_ERROR_INVALID_CLIENT_OBJECT;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_datachannel_register_message_handler_callback(ma_client_t *ma_client, const char *product_id, ma_datachannel_on_message_cb_t on_message_cb, void *cb_data) {
    if(ma_client && product_id && on_message_cb) {
        ma_error_t rc = MA_OK;
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;
        ma_msgbus_t *msgbus = NULL;
        ma_logger_t *logger = NULL;
        ma_client_get_context(ma_client, &ma_context);
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);
        msgbus = MA_CONTEXT_GET_MSGBUS(ma_context);
        logger = MA_CONTEXT_GET_LOGGER(ma_context);

        if(ma_context && client_manager && msgbus) {
            char client_name[MAX_CLIENT_NAME_LENGTH] = {0};
            ma_datachannel_client_t *dc_client = NULL;
            MA_MSC_SELECT(_snprintf, snprintf)(client_name, MAX_CLIENT_NAME_LENGTH,"%s.%s",MA_DATACHANNEL_CLIENT_NAME_STR, product_id);
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "The client %s",client_name);
            if(ma_client_manager_has_client(client_manager, client_name)) {
                (void)ma_client_manager_get_client(client_manager, client_name, (ma_client_base_t **) &dc_client);
                if(dc_client->on_message_cb) {
                    rc = MA_ERROR_ALREADY_REGISTERED;
                    MA_LOG(logger, MA_LOG_SEV_WARNING, "The client %s is already registered with on message callback. error code = %d", product_id, rc);
                }
                else {
                    dc_client->on_message_cb = on_message_cb;
                    dc_client->on_message_cb_data = cb_data;
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "Registered on message callback succesfully to already present client.");
                }
            }
            else {
                if(MA_OK == (rc = ma_datachannel_client_create(ma_client, product_id, &dc_client))) {
                    dc_client->on_message_cb = on_message_cb;
                    dc_client->on_message_cb_data = cb_data;
                    (void)ma_client_manager_add_client(client_manager, client_name,(ma_client_base_t *) dc_client);
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "Registered on message callback succesfully.");
                }
            }
            return rc;
        }
        return MA_ERROR_INVALID_CLIENT_OBJECT;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_datachannel_unregister_notification_callback(ma_client_t *ma_client, const char *product_id) {
    if(ma_client && product_id) {
        ma_error_t rc = MA_OK;
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;
        ma_logger_t *logger = NULL;
        ma_client_get_context(ma_client, &ma_context);
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);
        logger= MA_CONTEXT_GET_LOGGER(ma_context);

        if(ma_context && client_manager) {
            ma_datachannel_client_t *dc_client = NULL;
            char client_name[MAX_CLIENT_NAME_LENGTH] = {0};
            MA_MSC_SELECT(_snprintf, snprintf)(client_name, MAX_CLIENT_NAME_LENGTH,"%s.%s",MA_DATACHANNEL_CLIENT_NAME_STR, product_id);
            if(MA_OK == (rc = ma_client_manager_get_client(client_manager, client_name, (ma_client_base_t **)&dc_client))) {
                if(dc_client->on_notification_cb) {
                    dc_client->on_notification_cb = NULL; dc_client->on_notify_cb_data = NULL;
                }
                if(!dc_client->on_message_cb) {
                    (void)ma_client_manager_remove_client(client_manager, (ma_client_base_t *) dc_client);
                    (void)ma_datachannel_client_release(dc_client);
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "Client [ %s ] removed from client manager", product_id);
                }
                rc = MA_OK;
            }
            else {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "Client [ %s ] not found", product_id);
                rc = MA_ERROR_CLIENT_NOT_FOUND;
            }
            return rc;
        }
        return MA_ERROR_INVALID_CLIENT_OBJECT;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_datachannel_unregister_message_handler_callback(ma_client_t *ma_client, const char *product_id) {
    if(ma_client && product_id) {
        ma_error_t rc = MA_OK;
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;
        ma_logger_t *logger = NULL;
        ma_client_get_context(ma_client, &ma_context);
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);
        logger= MA_CONTEXT_GET_LOGGER(ma_context);

        if(ma_context && client_manager) {
            ma_datachannel_client_t *dc_client = NULL;
            char client_name[MAX_CLIENT_NAME_LENGTH] = {0};
            MA_MSC_SELECT(_snprintf, snprintf)(client_name, MAX_CLIENT_NAME_LENGTH,"%s.%s",MA_DATACHANNEL_CLIENT_NAME_STR, product_id);
            if(MA_OK == (rc = ma_client_manager_get_client(client_manager, client_name, (ma_client_base_t **)&dc_client))) {
                if(dc_client->on_message_cb) {
                    dc_client->on_message_cb = NULL; dc_client->on_message_cb_data = NULL;
                }
                if(!dc_client->on_notification_cb) {
                    (void)ma_client_manager_remove_client(client_manager, (ma_client_base_t *) dc_client);
                    (void)ma_datachannel_client_release(dc_client);
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "Client [ %s ] removed from client manager", product_id);
                }
                rc = MA_OK;
            }
            else {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "Client [ %s ] not found", product_id);
                rc = MA_ERROR_CLIENT_NOT_FOUND;
            }
            return rc;
        }
        return MA_ERROR_INVALID_CLIENT_OBJECT;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_datachannel_notification_subscription_callback(const char *topic, ma_message_t *message, void *cb_data) {
    ma_datachannel_client_t *dc_client = (ma_datachannel_client_t *)cb_data;
    const char *item_id = NULL;
    const char *delivery_status = NULL;
    const char *cid = NULL;
    const char *origin = NULL;
    const char *reason_str = NULL;
    ma_int64_t correlation_id = 0;
    ma_datachannel_item_notification_t status = MA_DC_NOTIFICATION_NOSTATUS;
    if(!topic || !message || !cb_data)
        return MA_ERROR_INVALIDARG;
    
    if(	MA_OK == ma_message_get_property(message, EPO_DATACHANNEL_ITEM_PROP_MESSAGE_TYPE_STR, &item_id) &&
        MA_OK == ma_message_get_property(message, EPO_DATACHANNEL_ITEM_PROP_CORRELATION_ID_STR, &cid) &&
        MA_OK == ma_message_get_property(message, EPO_DATACHANNEL_ITEM_PROP_ORIGIN_STR, &origin) &&
        MA_OK == ma_message_get_property(message, EPO_DATACHANNEL_ITEM_PROP_STATUS_STR, &delivery_status) &&
        MA_OK == ma_message_get_property(message, EPO_DATACHANNEL_ITEM_PROP_REASON_STR, &reason_str)) {
            ma_error_t rc = MA_OK;
            rc = (ma_error_t) (reason_str?atoi(reason_str):MA_ERROR_UNEXPECTED);
            if(origin) {
                if(0 == strcmp(origin, dc_client->product_id)) {
                    int stat = atoi(delivery_status);
                    correlation_id = ma_atoint64(cid);
                    status = (ma_datachannel_item_notification_t) (stat >= 0 && stat<= 2?stat:0);
                    if(dc_client->on_notification_cb)
                        dc_client->on_notification_cb(dc_client->ma_client, dc_client->product_id, item_id, correlation_id, status, rc, dc_client->on_notify_cb_data);
                }
            }
    }
    return MA_OK;
}

static ma_error_t ma_datachannel_msgbus_subscription_callback(const char *topic, ma_message_t *message, void *cb_data) {
    ma_datachannel_client_t *dc_client = (ma_datachannel_client_t *)cb_data;
    const char *item_id = NULL;
    const char *cid = NULL;
    const char *flags_str = NULL;
    const char *origin = NULL;
    ma_int64_t correlation_id = 0;

    if(!topic || !message || !cb_data || !dc_client->on_message_cb)
        return MA_ERROR_INVALIDARG;
    if(	MA_OK == ma_message_get_property(message, EPO_DATACHANNEL_ITEM_PROP_MESSAGE_TYPE_STR, &item_id) &&
        MA_OK == ma_message_get_property(message, EPO_DATACHANNEL_ITEM_PROP_CORRELATION_ID_STR, &cid) &&
        MA_OK == ma_message_get_property(message, EPO_DATACHANNEL_ITEM_PROP_ORIGIN_STR, &origin) &&
        MA_OK == ma_message_get_property(message, EPO_DATACHANNEL_ITEM_PROP_FLAGS_STR, &flags_str)) {
        ma_datachannel_item_t *dc_item = NULL;
        ma_int32_t flags = atoi(flags_str);
        ma_variant_t *variant = NULL;
        correlation_id = ma_atoint64(cid);
        if(MA_OK == ma_message_get_payload(message, &variant)) {
            if(MA_OK == ma_datachannel_item_create(item_id, variant, &dc_item)) {
                (void)ma_datachannel_item_set_option(dc_item, MA_DATACHANNEL_ENCRYPT, (MA_DATACHANNEL_ENCRYPT & flags) ? MA_TRUE : MA_FALSE);
                (void)ma_datachannel_item_set_option(dc_item, MA_DATACHANNEL_DELIVERY_NOTIFICATION, (MA_DATACHANNEL_DELIVERY_NOTIFICATION & flags) ? MA_TRUE : MA_FALSE);
                (void)ma_datachannel_item_set_option(dc_item, MA_DATACHANNEL_PURGED_NOTIFICATION, (MA_DATACHANNEL_PURGED_NOTIFICATION & flags) ? MA_TRUE : MA_FALSE);
				(void)ma_datachannel_item_set_correlation_id(dc_item, correlation_id);
                (void)ma_datachannel_item_set_origin(dc_item, origin);
                dc_client->on_message_cb(dc_client->ma_client, dc_client->product_id, item_id, correlation_id, dc_item, dc_client->on_message_cb_data);
                (void)ma_datachannel_item_release(dc_item);
            }
            (void)ma_variant_release(variant);
        }
    }
    return MA_OK;
}

typedef struct ma_async_data_s {
    ma_datachannel_client_t *dc_client;
    char product_id[MA_DATACHANNEL_MAX_ID_LEN];
    char message_id[MA_DATACHANNEL_MAX_ID_LEN];
    ma_int64_t correlation_id;
    ma_int64_t flags;
}ma_async_data_t;

static ma_error_t ma_datachannel_async_send_data_create(ma_datachannel_client_t *client, const char *product_id, const char *message_id, ma_int64_t correlation_id, ma_int64_t flags, ma_async_data_t **data) {
    if(product_id && message_id && data) {
        ma_async_data_t *self = (ma_async_data_t *) calloc(1,sizeof(ma_async_data_t));
        if(self) {
            self->dc_client = client;
            strncpy(self->product_id, product_id, MA_DATACHANNEL_MAX_ID_LEN-1);
            strncpy(self->message_id, message_id, MA_DATACHANNEL_MAX_ID_LEN-1);
            self->correlation_id = correlation_id;
            self->flags = flags;
            *data = self;
            return MA_OK;
        }
        return MA_ERROR_OUTOFMEMORY;
    }
    return MA_ERROR_INVALIDARG;
}
static void ma_datachannel_async_send_data_release(ma_async_data_t *data) {
    free(data);
}
static ma_error_t ma_msgbus_async_send_primary_subscription_cb(ma_error_t status, ma_message_t *response,void *cb_data, ma_msgbus_request_t *request) {
    ma_datachannel_client_t *dc_client = (ma_datachannel_client_t *)cb_data;
    ma_error_t rc = MA_OK;
    if((status == MA_OK) && response) ma_message_release(response);	
    if(request) {
        ma_msgbus_endpoint_t * ep = NULL;
        (void) ma_msgbus_request_get_endpoint(request, &ep);
        (void) ma_msgbus_request_release(request);
        if(ep) (void) ma_msgbus_endpoint_release(ep);
    }

    return rc;
}
static ma_error_t ma_msgbus_async_send_cb(ma_error_t status, ma_message_t *response,void *cb_data, ma_msgbus_request_t *request) {
    ma_async_data_t *data = (ma_async_data_t *) cb_data;
    ma_datachannel_client_t *dc_client = data->dc_client;
    ma_logger_t *logger = NULL;
    ma_context_t *ma_context = NULL;
    ma_error_t rc = MA_OK;
    
    if(MA_OK != status && dc_client) {
        ma_client_get_context(dc_client->ma_client, &ma_context);
        logger = MA_CONTEXT_GET_LOGGER(ma_context);
    
        MA_LOG(logger, MA_LOG_SEV_DEBUG, "Received response from server :%d", status);
        if(dc_client->on_notification_cb) {
            if((data->flags & MA_DATACHANNEL_PURGED_NOTIFICATION ) == MA_DATACHANNEL_PURGED_NOTIFICATION) {
                dc_client->on_notification_cb(dc_client->ma_client, data->product_id, data->message_id, data->correlation_id, MA_DC_NOTIFICATION_PURGED, status, dc_client->on_notify_cb_data);
            }
        }
    }
    if((status == MA_OK) && response) ma_message_release(response);	
    if(request) {
        ma_msgbus_endpoint_t * ep = NULL;
        (void) ma_msgbus_request_get_endpoint(request, &ep);
        (void) ma_msgbus_request_release(request);
        if(ep) (void) ma_msgbus_endpoint_release(ep);
    }

    ma_datachannel_async_send_data_release(data);
    return rc;
}


ma_error_t ma_datachannel_subscribe(ma_client_t *ma_client, const char *product_id, const char *dc_message_id, ma_datachannel_subscription_type_t subcription_type ) {
    if(ma_client && product_id && dc_message_id) {
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;
        ma_msgbus_t *msgbus = NULL;
        ma_logger_t *logger = NULL;
        ma_error_t rc = MA_ERROR_APIFAILED;
        ma_client_get_context(ma_client, &ma_context);
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);
        msgbus = MA_CONTEXT_GET_MSGBUS(ma_context);
        logger = MA_CONTEXT_GET_LOGGER(ma_context);


        if(ma_context && client_manager && msgbus) {
            char client_name[MAX_CLIENT_NAME_LENGTH] = {0};
            ma_datachannel_client_t *self = NULL;
            MA_MSC_SELECT(_snprintf, snprintf)(client_name, MAX_CLIENT_NAME_LENGTH,"%s.%s",MA_DATACHANNEL_CLIENT_NAME_STR, product_id);
            if(MA_OK == (rc = ma_client_manager_get_client(client_manager, client_name, (ma_client_base_t **) &self))) {
                ma_msgbus_subscriber_t *subscriber = NULL;
                if(!self->on_message_cb)
                    return MA_ERROR_DATACHANNEL_NO_CALLBACK_REGISTERED;
                if(MA_OK == MA_MAP_GET_VALUE(ma_dc_subscribers, self->subscribers, dc_message_id, subscriber)){
                    MA_LOG(logger, MA_LOG_SEV_DEBUG,"The messageType [ %s ] is already subscribed. rc = %d",dc_message_id, MA_ERROR_DATACHANNEL_SUBSCRIPTION_ALREADY_REGISTERED);
                    return MA_ERROR_DATACHANNEL_SUBSCRIPTION_ALREADY_REGISTERED;
                }
                else {
                    ma_msgbus_consumer_reachability_t reach = MSGBUS_CONSUMER_REACH_OUTPROC;
                    ma_msgbus_callback_thread_options_t t_option = MA_MSGBUS_CALLBACK_THREAD_POOL;
                    (void) ma_client_manager_get_thread_option(client_manager, &t_option);
                    (void) ma_client_manager_get_consumer_reachability(client_manager, &reach);

                    if(MA_OK == (rc = ma_msgbus_subscriber_create(msgbus, &subscriber))) {
                        ma_msgbus_subscriber_setopt(subscriber,MSGBUSOPT_THREADMODEL, t_option);
                        ma_msgbus_subscriber_setopt(subscriber,MSGBUSOPT_REACH, reach);
                        if(MA_OK == (rc = ma_msgbus_subscriber_register(subscriber, dc_message_id,
                            &ma_datachannel_msgbus_subscription_callback, self))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "Registering for datachannel messages of type [ %s ]",dc_message_id);
                            MA_MAP_ADD_ENTRY(ma_dc_subscribers, self->subscribers, dc_message_id, subscriber);
                            if(MA_DC_SUBSCRIBER_PRIMARY == subcription_type) {
                                ma_message_t *message = NULL;
                                if(MA_OK == ma_message_create(&message)) {
                                    if(MA_OK == ma_message_set_property(message, EPO_DATACHANNEL_MESSAGES_TYPE_STR, EPO_DATACHANNEL_MESSAGES_TYPE_SUBSCRIPTION_STR) &&
                                        MA_OK == ma_message_set_property(message, EPO_DATACHANNEL_ITEM_PROP_MESSAGE_TYPE_STR,dc_message_id)) {
                                        ma_msgbus_endpoint_t *endpoint = NULL;
                                        ma_msgbus_request_t *request = NULL;
                                        if(MA_OK == (rc = ma_msgbus_endpoint_create(msgbus, MA_DATACHANNEL_SERVICE_NAME_STR, NULL, &endpoint))) {
                                            (void) ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, t_option);
                                            (void) ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, reach);
                                            /*(void) ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_TIMEOUT, MA_DATACHANNEL_ASYNC_SEND_TIMEOUT);*/
                                            if(MA_OK == (rc = ma_msgbus_async_send(endpoint, message, ma_msgbus_async_send_primary_subscription_cb, self, &request))) {
                                                ma_message_release(message);
                                                MA_LOG(logger, MA_LOG_SEV_INFO, "Successfully registered as primary subscriber for [%s]", dc_message_id);
                                                return MA_OK;
                                            }
                                            MA_LOG(logger, MA_LOG_SEV_CRITICAL, "Failed to send message to datachannel service. rc = %d",rc);
                                            ma_msgbus_endpoint_release(endpoint);
                                        }
                                    }
                                    (void)ma_message_release(message);
                                }
                            }
                            else if( MA_DC_SUBSCRIBER_LISTENER == subcription_type) {
                                MA_LOG(logger, MA_LOG_SEV_INFO, "Successfully registered as listener subscriber for [%s]", dc_message_id);
                                return MA_OK;
                            }
                            else {
                                rc= MA_ERROR_INVALIDARG;
                            }
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "unregistering subscriber [ %s ].",dc_message_id);
                            MA_MAP_REMOVE_ENTRY(ma_dc_subscribers, self->subscribers, dc_message_id);
                            ma_msgbus_subscriber_unregister(subscriber);
                        }
                        (void)ma_msgbus_subscriber_release(subscriber);
                    }
                }
            }
            else {
                rc= MA_ERROR_CLIENT_NOT_FOUND;
                MA_LOG(logger, MA_LOG_SEV_DEBUG,"The product id [ %s ] has not registered any callbacks. rc = %d",product_id, rc);
            }
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}



ma_error_t ma_datachannel_unsubscribe(ma_client_t *ma_client, const char *product_id, const char *dc_message_id) {
    if(ma_client && product_id && dc_message_id) {
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;
        ma_logger_t *logger = NULL;
        ma_client_get_context(ma_client, &ma_context);
        logger = MA_CONTEXT_GET_LOGGER(ma_context);
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);

        if(ma_context && client_manager) {
            ma_error_t rc = MA_ERROR_OBJECTNOTFOUND;
            ma_datachannel_client_t * dc_client = NULL;
            char client_name[MAX_CLIENT_NAME_LENGTH] = {0};
            ma_msgbus_subscriber_t *subscriber = NULL;
            MA_MSC_SELECT(_snprintf, snprintf)(client_name, MAX_CLIENT_NAME_LENGTH,"%s.%s",MA_DATACHANNEL_CLIENT_NAME_STR, product_id);
            if(MA_OK == (rc = ma_client_manager_get_client(client_manager, client_name, (ma_client_base_t **) &dc_client))) {
                if(MA_OK == (rc = MA_MAP_GET_VALUE(ma_dc_subscribers,dc_client->subscribers, dc_message_id, subscriber))) {
                    MA_MAP_REMOVE_ENTRY(ma_dc_subscribers, dc_client->subscribers, dc_message_id);
                    ma_msgbus_subscriber_unregister(subscriber);
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "Unsubscribed from [ %s ]",dc_message_id);
                    (void)ma_msgbus_subscriber_release(subscriber);
                }
                else
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "Cannot find %s in subscriber list", dc_message_id);
            }
            return rc;
        }
        return MA_ERROR_INVALID_CLIENT_OBJECT;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_datachannel_async_send(ma_client_t *ma_client, const char *product_id, ma_datachannel_item_t *dc_item) {
    if(ma_client && product_id && dc_item) {
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;
        ma_msgbus_t *msgbus = NULL;
        ma_client_get_context(ma_client, &ma_context);
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);
        msgbus = MA_CONTEXT_GET_MSGBUS(ma_context);

        if(ma_context && client_manager && msgbus) {
            ma_error_t rc = MA_ERROR_CLIENT_NOT_FOUND;
            ma_datachannel_client_t * dc_client = NULL;
            ma_message_t *message = NULL;
            char client_name[MAX_CLIENT_NAME_LENGTH] = {0};
            MA_MSC_SELECT(_snprintf, snprintf)(client_name, MAX_CLIENT_NAME_LENGTH,"%s.%s",MA_DATACHANNEL_CLIENT_NAME_STR, product_id);
            if(MA_OK == (rc = ma_client_manager_get_client(client_manager, client_name, (ma_client_base_t **) &dc_client))) {
            }

            if(!*dc_item->origin) {
                const char *pid = MA_CONTEXT_GET_PRODUCT_ID(ma_context);
                ma_datachannel_item_set_origin(dc_item, pid);
            }

            if( MA_OK == (rc = ma_datachannel_item_prepare_message(dc_item, product_id, &message))) {
                ma_msgbus_request_t *request = NULL;
                ma_msgbus_endpoint_t *endpoint = NULL;

                if(MA_OK == (rc = ma_msgbus_endpoint_create(msgbus, MA_DATACHANNEL_SERVICE_NAME_STR, NULL, &endpoint))) {
                        ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;
                    (void) ma_client_manager_get_thread_option(client_manager, &thread_option);
                    (void) ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);
                    if(MA_OK == (rc = ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC))) {
                        ma_async_data_t *cb_data = NULL;
                        if(MA_OK == ma_datachannel_async_send_data_create(dc_client, product_id, dc_item->item_id, dc_item->correlation_id, dc_item->flags, &cb_data)) {
                            ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_TIMEOUT, MA_DATACHANNEL_MAX_ASYNC_SEND_TIMEOUT);
                            if(MA_OK == (rc = ma_msgbus_async_send(endpoint, message, ma_msgbus_async_send_cb, cb_data, &request))) {
                                rc = MA_OK;
                            }
                            else {
                                ma_datachannel_async_send_data_release(cb_data);
                                (void)ma_msgbus_endpoint_release(endpoint);
                            }
                        }
                        else {
                            (void)ma_msgbus_endpoint_release(endpoint);
                        }
                    }
                    else {
                        (void)ma_msgbus_endpoint_release(endpoint);
                    }
                }
                (void)ma_message_release(message);
            }
            return rc;
        }
        return MA_ERROR_INVALID_CLIENT_OBJECT;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_datachannel_notify_on_message(ma_client_base_t *client, const char *topic, ma_message_t *message) {
    /*We are not interested in any of ma.* messages from client controller*/
    return MA_OK;
}

