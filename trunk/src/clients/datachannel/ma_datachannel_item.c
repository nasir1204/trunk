#include "ma/datachannel/ma_datachannel_item.h"
#include "ma/ma_variant.h"
#include "ma/internal/defs/ma_datachannel_defs.h"
#include "ma/internal/defs/ma_ah_defs.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/clients/datachannel/ma_datachannel_internal.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static ma_error_t validate_item_id(const char *item_id) {
    if(item_id && *item_id) {
        static const char *valid_chars = " _-.0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        size_t len = strlen(item_id);
        if(strspn(item_id, valid_chars) == len && len >0 && len < 255)
            return MA_OK;
        else
            return MA_ERROR_INVALID_OPTION;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_datachannel_item_create(const char *item_id, ma_variant_t *payload, ma_datachannel_item_t **datachannel_message) {
    if(item_id && payload && datachannel_message) {
        ma_error_t rc = MA_OK;
        if(MA_OK == (rc = validate_item_id(item_id))) {
            ma_datachannel_item_t *self = (ma_datachannel_item_t *) calloc(1, sizeof(ma_datachannel_item_t));
            if(self){
                if(MA_OK == ma_variant_is_type(payload,MA_VARTYPE_RAW) || MA_OK == ma_variant_is_type(payload,MA_VARTYPE_STRING)) {
                    ma_int32_t len = strlen(item_id);
                    strncpy(self->item_id, item_id, len < MA_DATACHANNEL_MAX_ID_LEN?len:(MA_DATACHANNEL_MAX_ID_LEN-1));
                    self->ttl = MA_DATACHANNEL_MAX_TTL;
                    if(MA_OK == (rc = ma_datachannel_item_set_payload(self, payload))) {
                        self->payload = payload;
                        ma_variant_add_ref(self->payload);
                        *datachannel_message = self;
                        return MA_OK;
                    }
                }
                free(self);
                return rc;
            }
            return MA_ERROR_OUTOFMEMORY;
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_datachannel_item_release(ma_datachannel_item_t *self) {
    if(self) {
        if(self->payload) ma_variant_release(self->payload);
        free(self);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_datachannel_item_get_origin(ma_datachannel_item_t *self, ma_buffer_t **buffer) {
    if(self && buffer ) {
        ma_error_t rc = MA_ERROR_OBJECTNOTFOUND;
        if(MA_OK == ma_buffer_create(buffer, strlen(self->origin))) {
            if(MA_OK == ma_buffer_set(*buffer, self->origin, strlen(self->origin))) {
                rc = MA_OK;
            }
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_datachannel_item_get_payload(ma_datachannel_item_t *self, ma_variant_t **payload) {
    if(self && payload) {
        ma_error_t rc = MA_ERROR_OBJECTNOTFOUND;
        if(self->payload) {
            (void)ma_variant_add_ref(self->payload);
            *payload = self->payload;
            rc = MA_OK;
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_datachannel_item_get_ttl(ma_datachannel_item_t *self, ma_int64_t *ttl_in_seconds) {
    if(self && ttl_in_seconds) {
        *ttl_in_seconds = self->ttl;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t validate_payload(ma_variant_t *payload) {
    if(payload) {
        ma_error_t rc = MA_ERROR_DATACHANNEL_BAD_REQUEST;
        ma_vartype_t var_type = MA_VARTYPE_NULL;
        if(MA_OK == ma_variant_get_type(payload, &var_type)) {
            if(MA_OK == ma_variant_is_type(payload, MA_VARTYPE_RAW)  || MA_OK == ma_variant_is_type(payload, MA_VARTYPE_STRING) ) {
                ma_buffer_t *buffer = NULL;
                rc = ((MA_VARTYPE_STRING == var_type)?ma_variant_get_string_buffer(payload, &buffer):ma_variant_get_raw_buffer(payload, &buffer));
                if(MA_OK == rc) {
                    size_t size = 0;
                    const char *p = NULL;
                    rc = ((MA_VARTYPE_STRING == var_type)?ma_buffer_get_string(buffer, &p, &size):ma_buffer_get_raw(buffer, (const unsigned char **) &p, &size));
                    if(MA_OK == rc) {
                        if(size > MA_MAX_SPIPE_SIZE) {
                            rc = MA_ERROR_DATACHANNEL_MESSAGE_TOO_LARGE;
                        }
                        else {
                            rc = MA_OK;
                        }
                    }
                    (void)ma_buffer_release(buffer);
                }
            }
        }	
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_datachannel_item_set_payload(ma_datachannel_item_t *self, ma_variant_t *payload) {
    if(self && payload) {
        ma_error_t rc = MA_OK;
        if(MA_OK == (rc = validate_payload(payload))) {
            if(self->payload) {
                (void)ma_variant_release(self->payload);
            }
            self->payload = payload;
            (void)ma_variant_add_ref(self->payload);
            return MA_OK;
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_datachannel_item_set_ttl(ma_datachannel_item_t *self, ma_int64_t ttl) {
    if(self) {
        if(ttl <=0)
            ttl = MA_DATACHANNEL_MAX_TTL;
        self->ttl = ttl;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_datachannel_item_set_correlation_id(ma_datachannel_item_t *self, ma_int64_t correlation_id) {
    if(self) {
        self->correlation_id = correlation_id;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_datachannel_item_get_correlation_id(ma_datachannel_item_t *self, ma_int64_t *correlation_id) {
    if(self && correlation_id) {
        *correlation_id = self->correlation_id;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}
/*Internal*/
ma_error_t ma_datachannel_item_set_origin(ma_datachannel_item_t *self, const char *origin) {
    if(self && origin && *origin) {
        size_t size = strlen(origin);
        memset(self->origin, 0, MA_DATACHANNEL_MAX_ID_LEN);
        strncpy(self->origin, origin, size < MA_DATACHANNEL_MAX_ID_LEN? size : (MA_DATACHANNEL_MAX_ID_LEN-1));
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_datachannel_item_prepare_message(ma_datachannel_item_t *self, const char *product_id,  ma_message_t **message) {
    if(self && self->payload && message) {
        ma_error_t rc = MA_OK;
        char flag_buf[MA_DATACHANNEL_MAX_ID_LEN] = {0};
        char ttl_buf[MA_DATACHANNEL_MAX_ID_LEN] = {0};
        char correlation_id_buf[MA_DATACHANNEL_MAX_ID_LEN] = {0};
        
        if(MA_OK == (rc = ma_message_create(message))) {
            if(strlen(self->origin) > 0 )
                (void)ma_message_set_property(*message, EPO_DATACHANNEL_ITEM_PROP_ORIGIN_STR, self->origin);
            else
                (void)ma_message_set_property(*message, EPO_DATACHANNEL_ITEM_PROP_ORIGIN_STR, product_id);

            (void)ma_message_set_property(*message, EPO_DATACHANNEL_MESSAGES_TYPE_STR, EPO_DATACHANNEL_MESSAGES_TYPE_MSGUPLOAD_STR);
            (void)ma_message_set_property(*message, EPO_DATACHANNEL_ITEM_PROP_MESSAGE_TYPE_STR, self->item_id);

            MA_MSC_SELECT(_snprintf, snprintf)(flag_buf, MA_DATACHANNEL_MAX_ID_LEN, "%d", self->flags);
            (void)ma_message_set_property(*message, EPO_DATACHANNEL_ITEM_PROP_FLAGS_STR, flag_buf);

            MA_MSC_SELECT(_snprintf, snprintf)(ttl_buf, MA_DATACHANNEL_MAX_ID_LEN, "%lld", self->ttl);
            (void)ma_message_set_property(*message, EPO_DATACHANNEL_ITEM_PROP_TTL_STR, ttl_buf);

            MA_MSC_SELECT(_snprintf, snprintf)(correlation_id_buf, MA_DATACHANNEL_MAX_ID_LEN, "%lld", self->correlation_id);
            (void)ma_message_set_property(*message, EPO_DATACHANNEL_ITEM_PROP_CORRELATION_ID_STR, correlation_id_buf);
            (void)ma_message_set_payload(*message, self->payload);
        }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_datachannel_item_set_option(ma_datachannel_item_t *self, ma_uint32_t option, ma_bool_t b_enable) {
    if(self) {
        if(MA_DATACHANNEL_DEFAULT == option && !b_enable)
            return MA_ERROR_INVALID_OPTION;

        if(b_enable) {
            if(option == MA_DATACHANNEL_DEFAULT) {
                self->flags = 0 ;
                return MA_OK;
            }
            if(option & MA_DATACHANNEL_PERSIST) {
                self->flags |= MA_DATACHANNEL_PERSIST;
            }
            if(option & MA_DATACHANNEL_ENCRYPT) {
                self->flags |= MA_DATACHANNEL_ENCRYPT;
            }
            if(option & MA_DATACHANNEL_DELIVERY_NOTIFICATION) {
                self->flags |= MA_DATACHANNEL_DELIVERY_NOTIFICATION;
            }
            if(option & MA_DATACHANNEL_PURGED_NOTIFICATION) {
                self->flags |= MA_DATACHANNEL_PURGED_NOTIFICATION;
            }
        }
        else {
            if(option & MA_DATACHANNEL_PERSIST) {
                self->flags &= ~MA_DATACHANNEL_PERSIST;
            }
            if(option & MA_DATACHANNEL_ENCRYPT) {
                self->flags &= ~MA_DATACHANNEL_ENCRYPT;
            }
            if(option & MA_DATACHANNEL_DELIVERY_NOTIFICATION) {
                self->flags &= ~MA_DATACHANNEL_DELIVERY_NOTIFICATION;
            }
            if(option & MA_DATACHANNEL_PURGED_NOTIFICATION) {
                self->flags &= ~MA_DATACHANNEL_PURGED_NOTIFICATION;
            }
        }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_datachannel_item_get_option(ma_datachannel_item_t *self, ma_uint32_t *option) {
    if(self && option) {
        *option = self->flags & 0xf;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_datachannel_item_get_message_type(ma_datachannel_item_t *self, ma_buffer_t **buffer) {
	if(self && buffer) {
		ma_error_t rc = MA_OK;
		if(MA_OK == (rc = ma_buffer_create(buffer, strlen(self->item_id) + 1))) {
			rc = ma_buffer_set(*buffer, self->item_id, strlen(self->item_id));
		}
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}