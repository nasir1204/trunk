#include "ma/internal/clients/policy/ma_policy_uri_internal.h"
#include "ma/ma_variant.h"
#include "ma/internal/utils/threading/ma_atomic.h"
#include "ma/internal/defs/ma_policy_defs.h"
#include "ma/internal/ma_macros.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


static ma_error_t ma_policy_uri_set_property(ma_policy_uri_t *uri, const char *key, const char *value);
static ma_error_t ma_policy_uri_set_property_as_array(ma_policy_uri_t *uri, const char *key, ma_array_t *list);
static ma_error_t ma_policy_uri_get_property(const ma_policy_uri_t *uri, const char *key, const char **value); 

struct ma_policy_uri_s {    
    ma_table_t              *properties;
    ma_atomic_counter_t     ref_count;
};

ma_error_t ma_policy_uri_create(ma_policy_uri_t **uri) {
    if(uri) {
        ma_policy_uri_t *self = (ma_policy_uri_t *)calloc(1, sizeof(ma_policy_uri_t));
        if(self) {
            ma_error_t rc = MA_OK;
            if(MA_OK == (rc = ma_table_create(&self->properties))) {
                (void)ma_policy_uri_add_ref(self);
                *uri = self;                
            }
            else
                free(self);            
            return rc;
        }
        return MA_ERROR_OUTOFMEMORY;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_policy_uri_create_from_table(ma_table_t *prop_table, ma_policy_uri_t **uri) {
    if(uri && prop_table) {
        ma_policy_uri_t *self = (ma_policy_uri_t *)calloc(1, sizeof(ma_policy_uri_t));
        if(self) {
            ma_error_t rc = MA_OK;
            if(MA_OK == (rc = ma_table_add_ref(self->properties = prop_table))) {
                (void)ma_policy_uri_add_ref(self);
                *uri = self;                
            }
            else {
				free(self); self = NULL; }
            return rc;
        }
        return MA_ERROR_OUTOFMEMORY;
    }
    return MA_ERROR_INVALIDARG;
}

static void foreach_copy_properties(char const *key, ma_variant_t *value, void *cb_args, ma_bool_t *stop_loop) {
    ma_table_t *dest_table = (ma_table_t *)cb_args;
    (void)ma_table_add_entry(dest_table, key, value);
}

ma_error_t ma_policy_uri_copy(const ma_policy_uri_t *self, ma_policy_uri_t **uri_dest) {
    if(self && uri_dest) {
        ma_error_t rc = MA_OK;
        if(MA_OK == (rc = ma_policy_uri_create(uri_dest))) {
            rc = ma_table_foreach(self->properties, foreach_copy_properties, (*uri_dest)->properties);
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_policy_uri_get_table(const ma_policy_uri_t *self, ma_table_t **prop_table) {
    if(self && prop_table) {
        return ma_table_add_ref(*prop_table = self->properties);        
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_policy_uri_destroy(ma_policy_uri_t *self) {
    if(self) {
        if(self->properties)
            (void)ma_table_release(self->properties);        
        free(self);
		self = NULL;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_policy_uri_release(ma_policy_uri_t *self) {
    if(self) {
        ma_error_t rc = MA_OK;
        if(0 == MA_ATOMIC_DECREMENT(self->ref_count)) {
            rc = ma_policy_uri_destroy(self);
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_policy_uri_add_ref(ma_policy_uri_t *self) {
    if(self) {
        MA_ATOMIC_INCREMENT(self->ref_count);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_policy_uri_set_property(ma_policy_uri_t *self, const char *key, const char *value) {
    if(self && key && value) {
        ma_error_t rc = MA_OK;
        ma_variant_t *value_var;
        if(MA_OK == (rc = ma_variant_create_from_string(value, strlen(value), &value_var))) {
            rc = ma_table_add_entry(self->properties, key, value_var);
            (void)ma_variant_release(value_var); value_var = NULL;
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_policy_uri_set_property_as_array(ma_policy_uri_t *self, const char *key, ma_array_t *list) {
    if(self && key && list) {
        ma_error_t rc = MA_OK;
        ma_variant_t *value_var;
        if(MA_OK == (rc = ma_variant_create_from_array(list, &value_var))) {
            rc = ma_table_add_entry(self->properties, key, value_var);
            (void)ma_variant_release(value_var); value_var = NULL;
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_policy_uri_get_property(const ma_policy_uri_t *self, const char *key, const char **value) {
    if(self && key && value) {
        ma_variant_t *value_var = NULL;
        ma_error_t rc = MA_OK;
        if(self->properties && (MA_OK == (rc = ma_table_get_value(self->properties, key, &value_var)))){
            ma_buffer_t *buffer = NULL;
            if(MA_OK == (rc = ma_variant_get_string_buffer(value_var, &buffer))) {
                size_t size = 0;
                rc = ma_buffer_get_string(buffer, value, &size);
                (void)ma_buffer_release(buffer); buffer = NULL;
            }
            (void)ma_variant_release(value_var); value_var = NULL;
        }
        return rc;        
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_policy_uri_get_notification_type(const ma_policy_uri_t *uri, ma_policy_notification_type_t *type) {
    ma_error_t rc = MA_OK;
    const char *type_str = NULL;
    rc = ma_policy_uri_get_property(uri, NOTIFY_TYPE_STR, &type_str);
    if(MA_OK == rc && type_str)
        *type = (ma_policy_notification_type_t)(atoi(type_str));
    return rc;
}

ma_error_t ma_policy_uri_get_software_id(const ma_policy_uri_t *uri, const char **software_id) {
    return ma_policy_uri_get_property(uri, SOFTWARE_ID_STR, software_id);
}

ma_error_t ma_policy_uri_get_version(const ma_policy_uri_t *uri, const char **version) {
    return ma_policy_uri_get_property(uri, VERSION_STR, version);
}

ma_error_t ma_policy_uri_get_user(const ma_policy_uri_t *uri, const char **user) {
    return ma_policy_uri_get_property(uri, USER_ID_STR, user);
}

ma_error_t ma_policy_uri_get_user_session_id(const ma_policy_uri_t *uri, ma_uint32_t *session_id) {
     ma_error_t rc = MA_OK;
    const char *session_str = NULL;
    rc = ma_policy_uri_get_property(uri, SESSION_ID_STR, &session_str);
    if(MA_OK == rc && session_str)
        *session_id = atoi(session_str);
    return rc;
}

ma_error_t ma_policy_uri_get_user_logon_state(const ma_policy_uri_t *uri, ma_user_logon_state_t *logon_state) {
    ma_error_t rc = MA_OK;
    const char *state_str = NULL;
    rc = ma_policy_uri_get_property(uri, LOGON_STATE_STR, &state_str);
    if(MA_OK == rc && state_str) {
		if(!strcmp(LOG_ON_STR, state_str)) 
			*logon_state = MA_USER_LOG_ON;
		else if(!strcmp(LOG_OFF_STR, state_str)) 
			*logon_state = MA_USER_LOG_OFF;
		else 
			*logon_state = MA_USER_NOT_LOGGED_ON;
	}
    return rc;
}

ma_error_t ma_policy_uri_get_location_id (const ma_policy_uri_t *uri, const char **location_id) {
    return ma_policy_uri_get_property(uri, LOCATION_ID_STR, location_id);
}

ma_error_t ma_policy_uri_get_feature(const ma_policy_uri_t *uri, const char **feature) {
    return ma_policy_uri_get_property(uri, PO_FEATURE_STR, feature);
}

ma_error_t ma_policy_uri_get_category(const ma_policy_uri_t *uri, const char **category) {
    return ma_policy_uri_get_property(uri, PO_CATEGORY_STR, category);
}

ma_error_t ma_policy_uri_get_type(const ma_policy_uri_t *uri, const char **type) {
    return ma_policy_uri_get_property(uri, PO_TYPE_STR, type);
}

ma_error_t ma_policy_uri_get_name(const ma_policy_uri_t *uri, const char **name) {
    return ma_policy_uri_get_property(uri, PO_NAME_STR, name);
}

ma_error_t ma_policy_uri_get_pso_name(const ma_policy_uri_t *uri, const char **pso_name) {
	return ma_policy_uri_get_property(uri, PSO_NAME_STR, pso_name);
}

ma_error_t ma_policy_uri_get_pso_param_int(const ma_policy_uri_t *uri, const char **pso_param_int) {
	return ma_policy_uri_get_property(uri, PSO_PARAM_INT_STR, pso_param_int);
}

ma_error_t ma_policy_uri_get_pso_param_str(const ma_policy_uri_t *uri, const char **pso_param_str) {
	return ma_policy_uri_get_property(uri, PSO_PARAM_STR_STR, pso_param_str);
}

ma_error_t ma_policy_uri_set_software_id(ma_policy_uri_t *uri, const char *software_id) {
    return ma_policy_uri_set_property(uri, SOFTWARE_ID_STR, software_id);
}

ma_error_t ma_policy_uri_set_version(ma_policy_uri_t *uri, const char *version) {
    return ma_policy_uri_set_property(uri, VERSION_STR, version);
}

ma_error_t ma_policy_uri_set_user(ma_policy_uri_t *uri, const char *user) {
    return ma_policy_uri_set_property(uri, USER_ID_STR, user);
}

ma_error_t ma_policy_uri_set_user_list(ma_policy_uri_t *uri, ma_array_t *user_list) {
    return ma_policy_uri_set_property_as_array(uri, USER_LIST_STR, user_list);
}

ma_error_t ma_policy_uri_set_location_id(ma_policy_uri_t *uri, const char *location_id) {
    return ma_policy_uri_set_property(uri, LOCATION_ID_STR, location_id);
}

ma_error_t ma_policy_uri_set_feature(ma_policy_uri_t *uri, const char *feature) {
    return ma_policy_uri_set_property(uri, PO_FEATURE_STR, feature);
}

ma_error_t ma_policy_uri_set_category(ma_policy_uri_t *uri, const char *category) {
    return ma_policy_uri_set_property(uri, PO_CATEGORY_STR, category);
}

ma_error_t ma_policy_uri_set_type(ma_policy_uri_t *uri, const char *type) {
    return ma_policy_uri_set_property(uri, PO_TYPE_STR, type);
}

ma_error_t ma_policy_uri_set_name(ma_policy_uri_t *uri, const char *name) {
    return ma_policy_uri_set_property(uri, PO_NAME_STR, name);
}

ma_error_t ma_policy_uri_set_pso_name(ma_policy_uri_t *uri, const char *pso_name) {
	return ma_policy_uri_set_property(uri, PSO_NAME_STR, pso_name);
}

ma_error_t ma_policy_uri_set_pso_param_int(ma_policy_uri_t *uri, const char *pso_param_int) {
	return ma_policy_uri_set_property(uri, PSO_PARAM_INT_STR, pso_param_int);
}

ma_error_t ma_policy_uri_set_pso_param_str(ma_policy_uri_t *uri, const char *pso_param_str) {
	return ma_policy_uri_set_property(uri, PSO_PARAM_STR_STR, pso_param_str);
}
