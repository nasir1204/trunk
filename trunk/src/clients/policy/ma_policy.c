#include "ma/internal/clients/ma/ma_client_internal.h"
#include "ma/internal/clients/ma/ma_client_base.h"
#include "ma/internal/clients/ma/client/ma_client_manager.h"
#include "ma/internal/clients/policy/ma_policy_internal.h"
#include "ma/internal/clients/policy/ma_policy_bag_internal.h"
#include "ma/internal/clients/policy/ma_policy_uri_internal.h"
#include "ma/policy/ma_policy.h"
#include "ma/internal/defs/ma_policy_defs.h"
#include "ma/policy/ma_policy_utils.h"
#include "ma/internal/ma_macros.h"
#include "ma/ma_message.h"
#include "ma/internal/defs/ma_general_defs.h"
#include "ma/internal/defs/ma_object_defs.h"

#include "ma/internal/clients/ma/ma_client_event.h"
#include "ma/internal/utils/threading/ma_thread.h"

#include "ma/info/ma_info.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "policy"

typedef struct ma_policy_client_s ma_policy_client_t;

static ma_error_t ma_policy_client_on_message(ma_client_base_t *client, const char *topic, ma_message_t *message);
static ma_error_t ma_policy_client_release(ma_client_base_t *client);

static struct ma_client_base_methods_s methods =  {
	&ma_policy_client_on_message,
	&ma_policy_client_release
};

struct ma_policy_client_s{	
	ma_client_base_t                base;
	char                            *software_id;         
    ma_uint32_t                     notification_type;
	ma_policy_notification_cb_t     user_cb;   
	void                            *user_data;
	ma_client_t						*client;
	ma_mutex_t                      mutex;
	ma_bool_t						is_last_pe_failed;
};

static ma_error_t request_to_policy_service(ma_client_t *ma_client, const ma_policy_uri_t *uri, const char *request_type, ma_variant_t **response_var);

static ma_error_t ma_policy_client_create(ma_client_t *ma_client, const char *product_id, ma_policy_client_t **policy_client){
    if(ma_client && product_id && policy_client) {
        ma_policy_client_t *self = (ma_policy_client_t *)calloc(1, sizeof(ma_policy_client_t));
        if(self){
			if(!ma_mutex_init(&self->mutex)) {
				ma_client_base_init( (ma_client_base_t*)self, &methods, ma_client);

				self->software_id = strdup(product_id);
				*policy_client = self;
				return MA_OK;
			}
			free(self);
        }
        return MA_ERROR_OUTOFMEMORY;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_policy_client_release(ma_client_base_t *ma_client){
	if(ma_client){	
        ma_policy_client_t *self = (ma_policy_client_t *)ma_client;
		if(self->software_id)
			free(self->software_id);
		(void) ma_mutex_destroy(&self->mutex);		
		free(self);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}


static ma_bool_t is_valid_type(ma_uint32_t notify_type) {
    return (notify_type <= (MA_POLICY_TIMEOUT | MA_POLICY_CHANGED | MA_USER_CHANGED | MA_LOCATION_CHANGED | MA_SERVICE_STARTUP | MA_POLICY_FORCED_ENFORCEMENT)) ;
}

ma_error_t ma_policy_register_notification_callback(ma_client_t *ma_client, const char *product_id, ma_uint32_t notify_type, ma_policy_notification_cb_t cb, void *cb_data){
	if(ma_client && cb && is_valid_type(notify_type)){		
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;
        ma_client_get_context(ma_client, &ma_context);
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);		
        product_id = (product_id) ? product_id: MA_CONTEXT_GET_PRODUCT_ID(ma_context);

        if(ma_context && client_manager && product_id) {
            ma_error_t rc = MA_OK;
            ma_policy_client_t *policy_client = NULL;
            char client_name[MAX_CLIENT_NAME_LENGTH] = {0};
            
		    /* Make the policy consumer name*/
		    MA_MSC_SELECT(_snprintf, snprintf)(client_name, MAX_CLIENT_NAME_LENGTH,"%s.%s",MA_POLICY_CLIENT_NAME_STR, product_id);           

            if(ma_client_manager_has_client(client_manager, client_name)) {		
                return MA_ERROR_POLICY_CALLBACK_ALREADY_REGISTERED;			
		    }
		    else {
			    if(MA_OK == (rc = ma_policy_client_create(ma_client, product_id, &policy_client) ) ){
				    policy_client->notification_type = notify_type;
				    policy_client->user_cb = cb;
				    policy_client->user_data = cb_data;
					policy_client->client = ma_client;
				    if(MA_OK != (rc = ma_client_manager_add_client(client_manager, client_name, (ma_client_base_t*)policy_client) ) ) {
					    (void)ma_policy_client_release((ma_client_base_t*)policy_client);					
				    }								
			    }		
		    }		
		    return rc;
        }
        return MA_ERROR_INVALID_CLIENT_OBJECT;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_policy_unregister_notification_callback(ma_client_t *ma_client, const char *product_id){
	if(ma_client) {
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;
        ma_client_get_context(ma_client, &ma_context);
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);
        product_id = (product_id) ? product_id: MA_CONTEXT_GET_PRODUCT_ID(ma_context);
        
        if(ma_context && client_manager && product_id) {
            ma_error_t rc = MA_OK;
            char client_name[MAX_CLIENT_NAME_LENGTH] = {0};
            ma_client_base_t *policy_client = NULL;		
		    
		    MA_MSC_SELECT(_snprintf, snprintf)(client_name, MAX_CLIENT_NAME_LENGTH,"%s.%s",MA_POLICY_CLIENT_NAME_STR, product_id);
		    if(MA_OK == (rc = ma_client_manager_get_client(client_manager, client_name, &policy_client))) {
                (void)ma_client_manager_remove_client(client_manager, policy_client);
		        rc = ma_client_base_release(policy_client);
            }
            else
			    rc = MA_ERROR_POLICY_CALLBACK_NOT_REGISTERED;           
		 
		     return rc ;
        }
        return MA_ERROR_INVALID_CLIENT_OBJECT;
	}
	return MA_ERROR_INVALIDARG;
}

static char *get_locale(ma_client_t *client, char *str, int size){
	ma_variant_t *var = NULL;
	MA_MSC_SELECT(_snprintf, snprintf)(str, size, "%s", "0409");	

	if(MA_OK == ma_info_get(client, MA_INFO_LOCALE_STR, &var) && var){
		ma_buffer_t *buffer = NULL;
		if(MA_OK == ma_variant_get_string_buffer(var, &buffer)){
			const char *cstr = NULL;
			size_t size = 0;
			if(MA_OK == ma_buffer_get_string(buffer, &cstr, &size)){
				//MA_MSC_SELECT(_snprintf, snprintf)(str, size, "%s", cstr);	
			}
			(void)ma_buffer_release(buffer);
		}
		(void)ma_variant_release(var);
	}
	return str;
}

static void geterate_pe_failure_event(ma_policy_client_t *self, ma_client_t *client, char *software_id, ma_policy_notification_type_t recv_notify_type){
	struct ma_event_parameter_s param = {0};
	ma_event_parameter_init(&param);

	param.eventid = MA_POLICY_ENFORCEMENT_FAILURE_EVENT_ID;
	param.severity = MA_EVENT_SEVERITY_CRITICAL;
	param.source_productid = software_id;
	param.type = MA_POLICY_EVENT_THREAT_TYPE_STR;
	param.initiator_id = MA_SOFTWAREID_GENERAL_STR;
	get_locale(client, param.locale, sizeof(param.locale));
	MA_MSC_SELECT(_snprintf, snprintf)(param.error, sizeof(param.error), "%d", MA_POLICY_ENFORCMENT_ERROR);	
	ma_client_event_generate(client, &param);
}

/*
Handle the policy notification messages.
*/
static ma_error_t notify_caller(ma_policy_client_t *self, ma_message_t *message, ma_policy_notification_type_t recv_notify_type) {
    ma_error_t rc = MA_OK;
    ma_variant_t *payload = NULL;
    ma_table_t  *info_table = NULL;
    ma_uint32_t type = self->notification_type & recv_notify_type;

    /* If received notification type does not match with actual client registered type, ignore the message */
    if(!type) return MA_OK;

    if(MA_OK == (rc = ma_message_get_payload(message, &payload))) {
        rc = ma_variant_get_table(payload, &info_table);

        if(info_table) {
            ma_bool_t product_found = MA_TRUE;
            /* filter to block the notifications */
            if((MA_POLICY_CHANGED == type) || (MA_POLICY_TIMEOUT == type)) /* This payload has key "product_list" value - list of products to be notified for this policy change/policy timeout */
                rc = search_product_id(self->software_id, info_table, &product_found);
           
            if((MA_OK == rc) && product_found) {
                ma_variant_t *type_var = NULL;
                char type_str[8] = {0};
                MA_MSC_SELECT(_snprintf, snprintf)(type_str, 8, "%d", type);
                if(MA_OK == (rc = ma_variant_create_from_string(type_str, strlen(type_str), &type_var))) {
                    if(MA_OK == (rc = ma_table_add_entry(info_table, NOTIFY_TYPE_STR, type_var))) {
                        ma_variant_t *sw_id_var = NULL;
                        if(MA_OK == (rc = ma_variant_create_from_string(self->software_id, strlen(self->software_id), &sw_id_var))) {
                            if(MA_OK == (rc = ma_table_add_entry(info_table, SOFTWARE_ID_STR, sw_id_var))) {
								ma_error_t error = MA_OK;
                                ma_policy_uri_t *info_uri = NULL;
                                rc = ma_policy_uri_create_from_table(info_table, &info_uri);
								
								if(!ma_mutex_lock(&self->mutex)) {
									error = self->user_cb((ma_client_t *)self->base.data, self->software_id, info_uri, self->user_data);
									(void) ma_mutex_unlock(&self->mutex);
								}
								else
									error = MA_ERROR_UNEXPECTED;

								if(MA_OK != error) {
									if(!self->is_last_pe_failed) 
										geterate_pe_failure_event(self, (ma_client_t *)self->base.data, self->software_id, recv_notify_type);
									self->is_last_pe_failed = MA_TRUE;
								}
								else
									self->is_last_pe_failed = MA_FALSE;

                                (void)ma_policy_uri_release(info_uri);
                            }
                            (void)ma_variant_release(sw_id_var);
                        }
                    }
                    (void)ma_variant_release(type_var);
                }
            }
            (void)ma_table_release(info_table);
        }
        (void)ma_variant_release(payload);
    }
    return rc;
}

static ma_error_t ma_policy_client_on_message(ma_client_base_t *ma_client, const char *topic, ma_message_t *message){    
    ma_error_t rc = MA_OK;
    ma_policy_client_t *self = (ma_policy_client_t *)ma_client;
    if(self && topic && message) {
        if(!strncmp(topic, MA_POLICY_NOTIFICATION_TOPIC_STR, strlen(MA_POLICY_NOTIFICATION_TOPIC_STR))) {
            char const *value = NULL;
            if(MA_OK == (rc = ma_message_get_property(message, MA_PROP_KEY_POLICY_NOTIFICATION_STR, &value))) {
                if(!strncmp(value, MA_PROP_VALUE_NOTIFICATION_POLICY_TIMEOUT_STR, strlen(MA_PROP_VALUE_NOTIFICATION_POLICY_TIMEOUT_STR)))
                    rc = notify_caller(self, message, MA_POLICY_TIMEOUT);
                else if(!strncmp(value, MA_PROP_VALUE_NOTIFICATION_POLICY_CHANGED_STR, strlen(MA_PROP_VALUE_NOTIFICATION_POLICY_CHANGED_STR)))
                    rc = notify_caller(self, message, MA_POLICY_CHANGED);
                else if(!strncmp(value, MA_PROP_VALUE_NOTIFICATION_USER_CHANGED_STR, strlen(MA_PROP_VALUE_NOTIFICATION_USER_CHANGED_STR)))
                    rc = notify_caller(self, message, MA_USER_CHANGED);
                else if(!strncmp(value, MA_PROP_VALUE_NOTIFICATION_LOCATION_CHANGED_STR, strlen(MA_PROP_VALUE_NOTIFICATION_LOCATION_CHANGED_STR)))
                    rc = notify_caller(self, message, MA_LOCATION_CHANGED);
				else if(!strncmp(value, MA_PROP_VALUE_NOTIFICATION_SERVICE_STARTUP_STR, strlen(MA_PROP_VALUE_NOTIFICATION_SERVICE_STARTUP_STR)))
                    rc = notify_caller(self, message, MA_SERVICE_STARTUP) ;
                else if(!strncmp(value, MA_PROP_VALUE_NOTIFICATION_FORCED_ENFORCEMENT_STR, strlen(MA_PROP_VALUE_NOTIFICATION_FORCED_ENFORCEMENT_STR)))
                    rc = notify_caller(self, message, MA_POLICY_FORCED_ENFORCEMENT) ;
                else
                    rc = MA_ERROR_POLICY_INTERNAL;   /* log statement - client cannot handle the received notificaiton type */
            }
        }
    }
    return rc;
}

/*
    Get Policy UIRs from the server and returns to point product.
	Fetech actual policies when PP calls the get or iterate interfaces.
*/
ma_error_t ma_policy_get(ma_client_t *ma_client, const ma_policy_uri_t *uri, ma_policy_bag_t **policy_bag) {
	ma_error_t rc = MA_OK;
	ma_variant_t *value = NULL;
	if(MA_OK == (rc = request_to_policy_service(ma_client, uri, MA_PROP_VALUE_GET_POLICY_URI_LIST_REQUEST_STR, &value))) {	
		if(MA_OK == (rc =  ma_policy_bag_create_from_variant(value, policy_bag))) {
			const char *product_id = NULL;
			ma_policy_uri_get_software_id(uri, &product_id);
			if(MA_OK == (rc = ma_policy_bag_set_product_id(*policy_bag, product_id)))
				rc = ma_policy_bag_set_client(*policy_bag, ma_client);
		}
		(void) ma_variant_release(value);
	}
	return rc;
}

/*
    Get All Policies from the server and returns to caller.
*/
ma_error_t ma_policy_get_internal(ma_client_t *ma_client, const ma_policy_uri_t *uri, ma_policy_bag_t **policy_bag) {
	ma_error_t rc = MA_OK;
	ma_variant_t *value = NULL;
	if(MA_OK == (rc = request_to_policy_service(ma_client, uri, MA_PROP_VALUE_GET_POLICIES_REQUEST_STR, &value))) {	
		rc =  ma_policy_bag_create_from_variant(value, policy_bag);
		(void) ma_variant_release(value);
	}
	return rc;
}

static ma_error_t request_to_policy_service(ma_client_t *ma_client, const ma_policy_uri_t *uri, const char *request_type, ma_variant_t **response_var) {
    if(ma_client && uri && response_var) {
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;
        ma_msgbus_t *msgbus = NULL;
        ma_client_get_context(ma_client, &ma_context);
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);		
        msgbus = MA_CONTEXT_GET_MSGBUS(ma_context);
        if(ma_context && client_manager && msgbus) {
            ma_error_t rc = MA_OK;        
            ma_msgbus_endpoint_t *policy_ep = NULL;
	        if(MA_OK == (rc = ma_msgbus_endpoint_create(msgbus, MA_POLICY_SERVICE_NAME_STR, MA_POLICY_SERVICE_HOSTNAME_STR, &policy_ep))) {
		        ma_message_t *request_msg = NULL;   
                ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;
                (void)ma_msgbus_endpoint_setopt(policy_ep, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);

                (void) ma_client_manager_get_thread_option(client_manager, &thread_option);
                (void)ma_msgbus_endpoint_setopt(policy_ep, MSGBUSOPT_THREADMODEL, thread_option);
            
		        if(MA_OK == (rc = ma_message_create(&request_msg))) {
			        ma_variant_t *payload= NULL;
                    ma_table_t *info_table = NULL;
                    if(MA_OK == (rc = ma_policy_uri_get_table((ma_policy_uri_t *)uri, &info_table))) {
			            if(MA_OK == (rc = ma_variant_create_from_table(info_table, &payload))) {
                            ma_message_t *response_msg = NULL;
				            (void)ma_message_set_property(request_msg, MA_PROP_KEY_POLICY_REQUEST_TYPE_STR, request_type);
				            (void)ma_message_set_payload(request_msg, payload);
				            if(MA_OK == (rc = ma_msgbus_send(policy_ep, request_msg, &response_msg))) {
                                ma_variant_t *res_payload = NULL;
                                if(MA_OK == (rc = ma_message_get_payload(response_msg, &res_payload))) {
                                    ma_variant_add_ref(*response_var = res_payload);
                                    (void)ma_variant_release(res_payload);
                                }
                                (void)ma_message_release(response_msg);
                            }                    
				            (void)ma_variant_release(payload);
			            }
                        (void)ma_table_release(info_table);
                    }
			        (void)ma_message_release(request_msg);
		        }
		        (void)ma_msgbus_endpoint_release(policy_ep);
	        }
            return (MA_OK == rc) ? MA_OK : MA_ERROR_POLICY_INTERNAL;
        }
        return MA_ERROR_INVALID_CLIENT_OBJECT;
    }
    return MA_ERROR_INVALIDARG;
}

/* Refresh the policies i.,e fetch policies from ePO */
/* TBD - Refresh policies will fetch all the policies from server - currently there is not support to refresh policies based on input uri data */
ma_error_t ma_policy_refresh(ma_client_t *ma_client, const ma_policy_uri_t *uri) {
    if(ma_client && uri) {
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;
        ma_msgbus_t *msgbus = NULL;
        ma_client_get_context(ma_client, &ma_context);
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);		
        msgbus = MA_CONTEXT_GET_MSGBUS(ma_context);        
        if(ma_context && client_manager && msgbus) {
            ma_error_t rc = MA_OK;
            ma_msgbus_endpoint_t *policy_ep = NULL;
	        if(MA_OK == (rc = ma_msgbus_endpoint_create(msgbus, MA_POLICY_SERVICE_NAME_STR, MA_POLICY_SERVICE_HOSTNAME_STR, &policy_ep))) {
		        ma_message_t *request_msg = NULL;   
                ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;
                (void)ma_msgbus_endpoint_setopt(policy_ep, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);

                (void) ma_client_manager_get_thread_option(client_manager, &thread_option);
                (void)ma_msgbus_endpoint_setopt(policy_ep, MSGBUSOPT_THREADMODEL, thread_option);
            
		        if(MA_OK == (rc = ma_message_create(&request_msg))) {
			        ma_variant_t *payload= NULL;
                    ma_table_t *info_table = NULL;
                    if(MA_OK == (rc = ma_policy_uri_get_table((ma_policy_uri_t *)uri, &info_table))) {
					    if(MA_OK == (rc = ma_variant_create_from_table(info_table, &payload))) {
						    ma_message_t *response_msg = NULL;
						    (void)ma_message_set_property(request_msg, MA_PROP_KEY_POLICY_REQUEST_TYPE_STR, MA_PROP_VALUE_REFRESH_POLICIES_REQUEST_STR);
						    (void)ma_message_set_payload(request_msg, payload);
						    if(MA_OK == (rc = ma_msgbus_send(policy_ep, request_msg, &response_msg)))
							    (void)ma_message_release(response_msg); /* no reponse expecting from server */
						    (void)ma_variant_release(payload);
					    }
					    (void)ma_table_release(info_table);
				    }
			        (void)ma_message_release(request_msg);
		        }
		        (void)ma_msgbus_endpoint_release(policy_ep);
	        }
            return (MA_OK == rc) ? MA_OK : MA_ERROR_POLICY_INTERNAL;
        }
        return MA_ERROR_INVALID_CLIENT_OBJECT;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_policy_enforce(ma_client_t *ma_client, const char *product_id) {
    if(ma_client) {
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;
        ma_msgbus_t *msgbus = NULL;
        ma_client_get_context(ma_client, &ma_context);
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);		
        msgbus = MA_CONTEXT_GET_MSGBUS(ma_context);        
        if(ma_context && client_manager && msgbus) {
            ma_message_t *request = NULL;
            ma_error_t rc = MA_OK;
            ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(ma_context);
            MA_LOG(logger, MA_LOG_SEV_TRACE, "sending enforce policies command.");
	        if(MA_OK == (rc = ma_message_create(&request))) {
                ma_msgbus_endpoint_t *endpoint = NULL;
                (void)ma_message_set_property(request, MA_PROP_KEY_POLICY_REQUEST_TYPE_STR, MA_PROP_VALUE_ENFORCE_POLICIES_REQUEST_STR);
                (void)ma_message_set_property(request, MA_PROP_KEY_POLICY_ENFORCE_REASON_STR, MA_PROP_VALUE_POLICY_TIMEOUT_STR);
                 (void)ma_message_set_property(request, MA_PROP_KEY_POLICY_INITIATOR_STR, product_id);
                if(MA_OK == (rc = ma_msgbus_endpoint_create(msgbus, MA_POLICY_SERVICE_NAME_STR, MA_POLICY_SERVICE_HOSTNAME_STR, &endpoint))) {
			        (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
				    if(MA_OK == (rc = ma_msgbus_send_and_forget(endpoint, request))) 
                        MA_LOG(logger, MA_LOG_SEV_INFO, "Policy enforcement command initiated by cmdagent.");								
					else
                        MA_LOG(logger, MA_LOG_SEV_ERROR, "Policy enforcement command initiated by cmdagent failed, %d.", rc);
                    (void)ma_msgbus_endpoint_release(endpoint);
                }
            }
            (void)ma_message_release(request);
            return (MA_OK == rc) ? MA_OK : MA_ERROR_POLICY_INTERNAL;
        }
        return MA_ERROR_INVALID_CLIENT_OBJECT;
    }
    return MA_ERROR_INVALIDARG;
}

/*
    Set Policies from the caller to save a a local policies.
*/
ma_error_t ma_policy_set(ma_client_t *ma_client, const ma_policy_uri_t *uri, const ma_policy_bag_t *policy_bag) {
    if(ma_client && uri && policy_bag) {
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;
        ma_msgbus_t *msgbus = NULL;
        ma_client_get_context(ma_client, &ma_context);
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);		
        msgbus = MA_CONTEXT_GET_MSGBUS(ma_context);
        if(ma_context && client_manager && msgbus) {
            ma_error_t rc = MA_OK;        
            ma_msgbus_endpoint_t *policy_ep = NULL;
	        if(MA_OK == (rc = ma_msgbus_endpoint_create(msgbus, MA_POLICY_SERVICE_NAME_STR, MA_POLICY_SERVICE_HOSTNAME_STR, &policy_ep))) {
		        ma_message_t *request_msg = NULL;   
                ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;
                (void)ma_msgbus_endpoint_setopt(policy_ep, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);

                (void) ma_client_manager_get_thread_option(client_manager, &thread_option);
                (void)ma_msgbus_endpoint_setopt(policy_ep, MSGBUSOPT_THREADMODEL, thread_option);
            
		        if(MA_OK == (rc = ma_message_create(&request_msg))) {			        
                    ma_array_t *payload_arr = NULL;
                    if(MA_OK == (rc = ma_array_create(&payload_arr))) {
                        ma_table_t *info_table = NULL;
                        if(MA_OK == (rc = ma_policy_uri_get_table((ma_policy_uri_t *)uri, &info_table))) {
                            ma_variant_t *policy_data_var=NULL, *policy_info_var = NULL;
                            if(MA_OK == (rc = ma_policy_bag_get_variant((ma_policy_bag_t *)policy_bag, &policy_data_var))) {
                                if(MA_OK == (rc = ma_variant_create_from_table(info_table, &policy_info_var))) {
                                    ma_variant_t *payload= NULL;
                                    (void)ma_array_push(payload_arr, policy_info_var);
                                    (void)ma_array_push(payload_arr, policy_data_var);
                                    if(MA_OK == (rc = ma_variant_create_from_array(payload_arr, &payload))) {
                                        ma_message_t *response_msg = NULL;
				                        (void)ma_message_set_property(request_msg, MA_PROP_KEY_POLICY_REQUEST_TYPE_STR, MA_PROP_VALUE_SET_POLICIES_REQUEST_STR);
				                        (void)ma_message_set_payload(request_msg, payload);
				                        if(MA_OK == (rc = ma_msgbus_send(policy_ep, request_msg, &response_msg))) {                                            
                                            (void)ma_message_release(response_msg);
                                        }                    
				                        (void)ma_variant_release(payload);
                                    }
                                    (void) ma_variant_release(policy_info_var);
                                }
                                (void) ma_variant_release(policy_data_var);
                            }
                            (void)ma_table_release(info_table);
                        }
                        (void) ma_array_release(payload_arr);
                    }
			        (void)ma_message_release(request_msg);
		        }
		        (void)ma_msgbus_endpoint_release(policy_ep);
	        }
            return (MA_OK == rc) ? MA_OK : MA_ERROR_POLICY_INTERNAL;
        }
        return MA_ERROR_INVALID_CLIENT_OBJECT;
    }
    return MA_ERROR_INVALIDARG;
}


