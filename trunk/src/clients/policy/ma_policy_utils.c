
#include "ma/policy/ma_policy_utils.h"
#include "ma/internal/defs/ma_policy_defs.h"

#include <string.h>

struct foreach_data_s {
    const char *product_id;
    ma_bool_t   product_found;
    ma_error_t  status;
};

void product_arr_foreach(size_t index, ma_variant_t *value, void *cb_args, ma_bool_t *stop_loop) {
    struct foreach_data_s *foreach_data = (struct foreach_data_s *)cb_args;
    ma_buffer_t *buffer = NULL;
    ma_error_t rc = MA_OK;
    if(MA_OK == (rc = ma_variant_get_string_buffer(value, &buffer))) {
        const char *product_id = NULL;
        size_t size = 0;
        if(MA_OK == (rc = ma_buffer_get_string(buffer, &product_id, &size))) {
            foreach_data->product_found = !strcmp(product_id, foreach_data->product_id) ;
        }
        (void)ma_buffer_release(buffer);
    }
        
    if(MA_OK != rc) {
        *stop_loop = MA_TRUE;
        foreach_data->status = rc;
    }
    else {
        /* If product found in list, stop searching */
        if(foreach_data->product_found)
            *stop_loop = MA_TRUE;
    }
}

ma_error_t search_product_id(const char *product_id, ma_table_t *table, ma_bool_t *product_found) {
    /* checking whether we notify policy change to this client or not based on product id */
    ma_error_t rc = MA_OK;
    ma_variant_t *prod_var = NULL;
    if(MA_OK == (rc = ma_table_get_value(table, PRODUCT_LIST_STR, &prod_var))) {              
        ma_array_t *prod_arr = NULL;
        if(MA_OK == (rc = ma_variant_get_array(prod_var, &prod_arr))) {
            struct foreach_data_s foreach_data;
            foreach_data.product_id = product_id;
            foreach_data.product_found = MA_FALSE;
            foreach_data.status = MA_OK;
            if(MA_OK == (rc = ma_array_foreach(prod_arr, product_arr_foreach, &foreach_data))) {
                if(MA_OK == foreach_data.status)
                    *product_found = foreach_data.product_found;
                rc = foreach_data.status;
            }
            (void)ma_array_release(prod_arr);                
        }
        (void)ma_variant_release(prod_var);                
    }
    return rc;
}


