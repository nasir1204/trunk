#include "ma/internal/clients/ma/ma_client_internal.h"
#include "ma/internal/clients/ma/ma_client_base.h"
#include "ma/internal/clients/ma/client/ma_client_manager.h"
#include "ma/internal/clients/policy/ma_policy_bag_internal.h"
#include "ma/internal/clients/policy/ma_policy_uri_internal.h"
#include "ma/internal/defs/ma_policy_defs.h"
#include "ma/internal/utils/context/ma_context.h"
#include "ma/ma_client.h"
#include "ma/internal/utils/datastructures/ma_vector.h"
#include "ma/internal/utils/threading/ma_atomic.h"

#include <stdlib.h>
#include <string.h>


struct ma_policy_bag_s {
	ma_client_t			*ma_client;
	char				*product_id;
	ma_atomic_counter_t	ref_count;
    ma_array_t			*policy_data; /* Array of variants which contains another array (it has two tables first one is props table and second one is sections table) */
};

typedef enum ma_policy_bag_iterator_type_e {
	MA_POLICY_BAG_ITERATOR = 0,
	MA_POLICY_BAG_GENERAL_ITERATOR,
	MA_POLICY_BAG_URI_ITERATOR,
	MA_POLICY_BAG_SECTION_ITERATOR,
	MA_POLICY_BAG_SETTING_ITERATOR	
} ma_policy_bag_iterator_type_t;

struct ma_policy_bag_iterator_s {
	ma_policy_bag_iterator_type_t		type;
	size_t								index;
	size_t								count;
};

typedef struct ma_policy_bag_uri_iterator_s {	
	ma_policy_bag_iterator_t	base;
	ma_vector_define(uri_list, ma_policy_uri_t *);	
} ma_policy_bag_uri_iterator_t; 

typedef struct ma_policy_bag_section_iterator_s {
	ma_policy_bag_iterator_t	base;
	ma_array_t					*secitons_array;  
} ma_policy_bag_section_iterator_t; 

typedef struct ma_policy_bag_setting_iterator_s {
	ma_policy_bag_iterator_t	base;
	ma_array_t					*settings_array; 
	ma_table_t					*settings_table;
} ma_policy_bag_setting_iterator_t; 

typedef struct ma_policy_bag_general_iterator_s {
	ma_policy_bag_iterator_t		base;
	ma_policy_bag_t					*bag;
	ma_policy_bag_iterator_t		*uri_it;
	ma_policy_bag_iterator_t		*sec_it;
	ma_policy_bag_iterator_t		*set_it;

	ma_policy_uri_t					*cur_uri;
	char							*cur_section;
} ma_policy_bag_general_iterator_t; 

static ma_error_t get_policy_data_from_server(const ma_policy_bag_t *self, const char *request_type, const ma_policy_uri_t *uri, const char *section, ma_variant_t **value);

ma_error_t ma_policy_bag_get_value_internal(const ma_policy_bag_t *self, const ma_policy_uri_t *uri, const char *section_name, const char *key_name, ma_variant_t **value) {
    if(self && section_name && value) {
        ma_error_t rc = MA_ERROR_OBJECTNOTFOUND;
        size_t policies_count = 0, policy_index = 1;
        ma_bool_t b_policy_found = MA_FALSE;
		
        (void)ma_array_size(self->policy_data, &policies_count);

        while(!b_policy_found && policy_index <= policies_count) {            
            ma_variant_t *policy_var = NULL;
            (void) ma_array_get_element_at(self->policy_data, policy_index, &policy_var);
            if(policy_var) {
                ma_array_t *table_array = NULL;
                /* Expecting array of two tables, first one is pso properties table and another is section data table */
                if(MA_OK == (rc = ma_variant_get_array(policy_var, &table_array))) {
                    size_t size = 0;
                    (void) ma_array_size(table_array, &size);
                    if(2 == size) {                        
                        ma_variant_t *props_var=NULL, *sections_var=NULL;
                        (void) ma_array_get_element_at(table_array, 1, &props_var);
                        (void) ma_array_get_element_at(table_array, 2, &sections_var);
                        if(props_var && sections_var) { 
                            ma_table_t *props_table=NULL, *sections_table = NULL;
                            (void) ma_variant_get_table(props_var, &props_table);
                            (void) ma_variant_get_table(sections_var, &sections_table);
                            if(props_table && sections_table) {
                                ma_table_t *settings_table = NULL;            
                                ma_variant_t *settings_var = NULL;          
                                /* props table --> pso properties(key value pair), sections table --> Key = section_name; (Value = array of settings -> Key = key_name; Value = setting value) */     
                                if(uri){
                                    ma_table_t *uri_table = NULL;
                                    ma_bool_t b_result = MA_FALSE;
                                    (void)ma_policy_uri_get_table((ma_policy_uri_t *)uri, &uri_table);
                                    if(uri_table) {
                                        (void)ma_table_is_equal(uri_table, props_table, &b_result);
                                        if(b_result) {
                                            if(MA_OK == (rc = ma_table_get_value(sections_table, section_name, &settings_var))) {  
                                                if(key_name) {
                                                    if(MA_OK == (rc = ma_variant_get_table(settings_var, &settings_table))) {                                                   
                                                        rc = ma_table_get_value(settings_table, key_name, value);                                                    
                                                        b_policy_found = (MA_OK == rc)? MA_TRUE: MA_FALSE;
                                                        (void)ma_table_release(settings_table); settings_table = NULL;
                                                    }             
                                                }
                                                else {
                                                    (void)ma_variant_add_ref(*value = settings_var);
                                                    b_policy_found = MA_TRUE;
                                                }
                                                (void)ma_variant_release(settings_var); settings_var = NULL;
                                            }
											else
												b_policy_found = MA_TRUE; /* Failed to find the section name in bag, will get it from server if it set to MA_TRUE */
                                        }                                        
                                        (void)ma_table_release(uri_table);
                                    }
                                }
                                else {
                                    if(MA_OK == (rc = ma_table_get_value(sections_table, section_name, &settings_var))) {
                                        if(key_name) {
                                            if(MA_OK == (rc = ma_variant_get_table(settings_var, &settings_table))) {
                                                rc = ma_table_get_value(settings_table, key_name, value);
                                                b_policy_found = (MA_OK == rc)? MA_TRUE: MA_FALSE;
                                                (void)ma_table_release(settings_table); settings_table = NULL;
                                            }
                                        }
                                        else {
                                            (void)ma_variant_add_ref(*value = settings_var);
                                            b_policy_found = MA_TRUE;
                                        }
                                        (void)ma_variant_release(settings_var); settings_var = NULL;
                                    }
                                }
                            }
                            (void)ma_table_release(sections_table), (void)ma_table_release(props_table);                                   
                        }
                        (void)ma_variant_release(sections_var), (void)ma_variant_release(props_var);
                    }
                    (void)ma_array_release(table_array); table_array = NULL;
                }
                (void)ma_variant_release(policy_var); policy_var = NULL;
            }
            policy_index++;
        }
	   
		return (b_policy_found) ? rc : MA_ERROR_OBJECTNOTFOUND;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t update_policies(const ma_policy_bag_t *self, const ma_policy_uri_t *uri, ma_variant_t *sections) {
	size_t index = 1, count = 0;
	ma_bool_t b_uri_found = MA_FALSE;
	ma_error_t rc = MA_ERROR_POLICY_INTERNAL;

	(void)ma_array_size(self->policy_data, &count);
						
	while(!b_uri_found && index <= count) {
		ma_variant_t *policy_var = NULL;
		(void) ma_array_get_element_at(self->policy_data, index, &policy_var);
		if(policy_var) {
			ma_array_t *table_array = NULL;
			/* Expecting array of two tables, first one is pso properties table and another is section data table */
			if(MA_OK == (rc = ma_variant_get_array(policy_var, &table_array))) {
				size_t size = 0;
				(void) ma_array_size(table_array, &size);
				if(2 == size) {                        
					ma_variant_t *props_var=NULL, *sec_var=NULL;
					(void) ma_array_get_element_at(table_array, 1, &props_var);
					(void) ma_array_get_element_at(table_array, 2, &sec_var);
					if(props_var && sec_var) { 
						ma_table_t *props_table=NULL;
						(void) ma_variant_get_table(props_var, &props_table);
						if(props_table) {
							ma_table_t *uri_table = NULL;
							ma_bool_t b_result = MA_FALSE;
							(void)ma_policy_uri_get_table((ma_policy_uri_t *)uri, &uri_table);
							if(uri_table) {
								(void)ma_table_is_equal(props_table, uri_table, &b_result); 
								if(b_result)  {	
									rc = ma_array_set_element_at(table_array, 2, sections);
									break;
								}
								(void)ma_table_release(uri_table);
							}  
							(void) ma_table_release(props_table);
						}
						(void)ma_variant_release(sec_var), (void)ma_variant_release(props_var);                                       
					}                        
				}
				(void)ma_array_release(table_array); table_array = NULL;
			}
			(void)ma_variant_release(policy_var); policy_var = NULL;
		}
		index++;
	}
						
	return rc;
}

ma_error_t ma_policy_bag_get_value(const ma_policy_bag_t *self, const ma_policy_uri_t *uri, const char *section_name, const char *key_name, ma_variant_t **value) {
	if(self && uri && section_name && key_name && value) {
		ma_error_t rc = MA_OK;
		ma_variant_t *settings_var = NULL;
		if(MA_OK != (rc = ma_policy_bag_get_value_internal(self, uri, section_name, NULL, &settings_var))) {			
			ma_variant_t *sections_var = NULL;
			/* Get it from server using msgbus */
			if(MA_OK == (rc = get_policy_data_from_server(self, MA_PROP_VALUE_GET_POLICIES_BY_URI_REQUEST_STR, uri, NULL, &sections_var))) {
				ma_table_t *sections_tb = NULL;
				if(MA_OK == (rc = ma_variant_get_table(sections_var, &sections_tb))) {				
					if(MA_OK == (rc = ma_table_get_value(sections_tb, section_name, &settings_var))) {
						/* add received data into policy bag */		
						rc = update_policies(self, uri, sections_var);
					}
					(void) ma_table_release(sections_tb);
				}
				(void) ma_variant_release(sections_var);
			}
		}
		
		if((MA_OK == rc) && settings_var) {
			ma_table_t *settings_tb = NULL;
			if(MA_OK == (rc = ma_variant_get_table(settings_var, &settings_tb))) {
				rc = ma_table_get_value(settings_tb, key_name, value);
				(void)ma_table_release(settings_tb);
			}
			(void) ma_variant_release(settings_var);
		}

		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

static ma_bool_t compare_uris(ma_table_t *props_table, ma_table_t *uri_table) {
    ma_bool_t b_rc = MA_FALSE;
    ma_error_t rc = MA_OK;

    if(props_table && uri_table) {   
        ma_variant_t *prop_value = NULL, *uri_value = NULL;        
        
        if(MA_OK == (rc = ma_table_get_value(props_table, PO_FEATURE_STR, &prop_value))) {            
            if(MA_OK == (rc = ma_table_get_value(uri_table, PO_FEATURE_STR, &uri_value))) {   
                rc = ma_variant_is_equal(prop_value, uri_value, &b_rc);
                (void) ma_variant_release(uri_value); uri_value = NULL;
            }
            (void) ma_variant_release(prop_value); prop_value = NULL;
        }
                
        if(b_rc) {
            if(MA_OK == (rc = ma_table_get_value(props_table, PO_CATEGORY_STR, &prop_value))) {            
                if(MA_OK == (rc = ma_table_get_value(uri_table, PO_CATEGORY_STR, &uri_value))) {   
                    rc = ma_variant_is_equal(prop_value, uri_value, &b_rc);
                    (void) ma_variant_release(uri_value); uri_value = NULL;
                }
                (void) ma_variant_release(prop_value); prop_value = NULL;
            }
        }
        
        if(b_rc) {
            if(MA_OK == (rc = ma_table_get_value(props_table, PO_TYPE_STR, &prop_value))) {            
                if(MA_OK == (rc = ma_table_get_value(uri_table, PO_TYPE_STR, &uri_value))) {   
                    rc = ma_variant_is_equal(prop_value, uri_value, &b_rc);
                    (void) ma_variant_release(uri_value); uri_value = NULL;
                }
                (void) ma_variant_release(prop_value); prop_value = NULL;
            }
        }
        /*TBD - Compare pso names using strstr if needed */
    }
    return (MA_OK ==rc) ? b_rc : MA_FALSE;
}

/* TBD - Fallback mechanism shouldn't be used for PP policy set, it is only applicable only for MA */
ma_error_t ma_policy_bag_set_value(ma_policy_bag_t *self, const ma_policy_uri_t *uri, const char *section_name, const char *key_name, const ma_variant_t *value) {
    if(self && uri && section_name && key_name && value) {
        ma_error_t rc = MA_OK;
        ma_table_t *sections_table = NULL;
        size_t index = 1, count = 0;
        ma_bool_t b_uri_found = MA_FALSE;

        (void)ma_array_size(self->policy_data, &count);

        while(!b_uri_found && index <= count) {
            ma_variant_t *policy_var = NULL;
            (void) ma_array_get_element_at(self->policy_data, index, &policy_var);
            if(policy_var) {
				ma_array_t *table_array = NULL;
                /* Expecting array of two tables, first one is pso properties table and another is section data table */
                if(MA_OK == (rc = ma_variant_get_array(policy_var, &table_array))) {
                    size_t size = 0;
                    (void) ma_array_size(table_array, &size);
                    if(2 == size) {                        
                        ma_variant_t *props_var=NULL, *sections_var=NULL;
                        (void) ma_array_get_element_at(table_array, 1, &props_var);
                        (void) ma_array_get_element_at(table_array, 2, &sections_var);
                        if(props_var && sections_var) { 
                            ma_table_t *props_table=NULL;
                            (void) ma_variant_get_table(props_var, &props_table);
                            (void) ma_variant_get_table(sections_var, &sections_table);
                            if(props_table && sections_table) {
                                ma_table_t *uri_table = NULL;
                                ma_bool_t b_result = MA_FALSE;
                                (void)ma_policy_uri_get_table((ma_policy_uri_t *)uri, &uri_table);
                                if(uri_table) {
                                    (void)ma_table_is_equal(props_table, uri_table, &b_result); 
                                    
                                    /* Fallback to compare with feature, category and type only as name is dynamic in legacy policies case */
                                    if(!b_result) {
                                        b_result = compare_uris(props_table, uri_table);
                                    }

                                    if(b_result)  {
                                        rc = ma_table_add_ref(sections_table);
                                        b_uri_found = MA_TRUE;
                                    }

                                    (void)ma_table_release(uri_table);
                                }  
                                (void)ma_table_release(sections_table), (void)ma_table_release(props_table);
                            }
                            (void)ma_variant_release(sections_var), (void)ma_variant_release(props_var);                                       
                        }                        
                    }
                    (void)ma_array_release(table_array); table_array = NULL;
                }
                (void)ma_variant_release(policy_var); policy_var = NULL;
            }
            index++;
        }

        if(!b_uri_found && index > count) {
            /* create new uri entry and section table in array */          
            ma_table_t *uri_table = NULL;            
            if(MA_OK == (rc = ma_policy_uri_get_table((ma_policy_uri_t *)uri, &uri_table))) {                
                ma_variant_t *uri_var = NULL;
                if(MA_OK == (rc = ma_variant_create_from_table(uri_table, &uri_var))) {                    
                    ma_array_t *new_arr= NULL; 
                    if(MA_OK == (rc = ma_array_create(&new_arr))) {
                        ma_variant_t *sec_var = NULL, *policy_var = NULL;
                        if(MA_OK == (rc = ma_array_push(new_arr, uri_var))) {
                            if(MA_OK == (rc = ma_table_create(&sections_table))) {
                                if(MA_OK == (rc = ma_variant_create_from_table(sections_table, &sec_var))) {                    
                                    if(MA_OK == (rc = ma_array_push(new_arr, sec_var))) {
                                        if(MA_OK == (rc = ma_variant_create_from_array(new_arr, &policy_var))) {
                                            rc = ma_array_push(self->policy_data, policy_var);
                                            b_uri_found = MA_TRUE;
											(void)ma_variant_release(policy_var); policy_var = NULL;
                                        }
                                    }
                                    (void)ma_variant_release(sec_var); sec_var = NULL;
                                }
                            }
                        }
                        (void)ma_array_release(new_arr); new_arr = NULL;
                    }
                    (void)ma_variant_release(uri_var); uri_var = NULL;
                }
                (void)ma_table_release(uri_table); uri_table = NULL;
            }            
        }
        
        if(b_uri_found && (MA_OK == rc)) {            
            ma_variant_t *section_entry = NULL;
            ma_table_t   *setting_table = NULL;
            if(MA_OK != (rc = ma_table_get_value(sections_table, section_name, &section_entry))) {
                /* Create if section entry is not available */
                ma_table_t *set_tb = NULL;
                if(MA_OK == (rc = ma_table_create(&set_tb))) {                
                    if(MA_OK == (rc = ma_variant_create_from_table(set_tb, &section_entry)))
                        rc = ma_table_add_entry(sections_table, section_name, section_entry); 
                    if(set_tb) (void)ma_table_release(set_tb);
                }
            }                       

            if((MA_OK == rc) && (MA_OK == (rc = ma_variant_get_table(section_entry, &setting_table)))) {
                /* Add new entry here.. Assune that the exiting entry will be deleted and new value will be copied */
                rc = ma_table_add_entry(setting_table, key_name, (ma_variant_t *)value);
                (void) ma_table_release(setting_table);
            }               
            if(section_entry) (void)ma_variant_release(section_entry);
            (void)ma_table_release(sections_table);
        }
        return rc;        
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_policy_bag_create(ma_policy_bag_t **bag) {
    if(bag) {        
        ma_policy_bag_t *self = (ma_policy_bag_t *)calloc(1, sizeof(ma_policy_bag_t));
        if(self) {
			MA_ATOMIC_INCREMENT(self->ref_count);
            (void)ma_array_create(&self->policy_data);
            *bag = self;
            return MA_OK;
        }
        return MA_ERROR_OUTOFMEMORY;
    }
    return MA_ERROR_INVALIDARG;
}


size_t ma_policy_bag_size(ma_policy_bag_t  *bag) {
    if(bag) {
        size_t size = 0;
        (void)ma_array_size(bag->policy_data, &size);
        return size;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t  ma_policy_bag_create_from_variant(ma_variant_t *data, ma_policy_bag_t **bag) {
    if(data && bag) {
        ma_policy_bag_t *self = (ma_policy_bag_t *)calloc(1, sizeof(ma_policy_bag_t));
        if(self) {
            (void)ma_variant_get_array(data, &self->policy_data);
			MA_ATOMIC_INCREMENT(self->ref_count);
            *bag = self;
            return MA_OK;
        }
        return MA_ERROR_OUTOFMEMORY;        
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_policy_bag_set_client(ma_policy_bag_t *self, struct ma_client_s *ma_client) {
	if(self && ma_client) {
		self->ma_client = ma_client;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_policy_bag_set_product_id(ma_policy_bag_t *self, const char *product_id) {
	if(self && product_id) {
		self->product_id = strdup(product_id);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t  ma_policy_bag_get_variant(ma_policy_bag_t *self, ma_variant_t **data) {    
    return (self && data) ? ma_variant_create_from_array(self->policy_data, data) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_policy_bag_add_ref(ma_policy_bag_t *self) {
    if(self){
		MA_ATOMIC_INCREMENT(self->ref_count);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_policy_bag_release(ma_policy_bag_t *self) {
    if(self){
		if(0 == MA_ATOMIC_DECREMENT(self->ref_count)) {
			if(self->product_id)
				free(self->product_id);

			if(self->policy_data)
				(void)ma_array_release(self->policy_data);
			free(self);
		}
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

void ma_policy_bag_iterator_init(ma_policy_bag_iterator_t *iterator, ma_policy_bag_iterator_type_t type, ma_uint32_t count) {
	if(iterator) {
		iterator->type = type;
		iterator->count = count;
		iterator->index = 0;
	}
}

ma_error_t ma_policy_bag_get_iterator(ma_policy_bag_t *self,  ma_policy_bag_iterator_t **iterator) {
    if(self && iterator) {
		ma_policy_bag_general_iterator_t *it = (ma_policy_bag_general_iterator_t *)calloc(1, sizeof(ma_policy_bag_general_iterator_t));
        if(it) {
            ma_error_t rc = MA_OK; 			
			ma_policy_bag_iterator_init((ma_policy_bag_iterator_t *)it, MA_POLICY_BAG_GENERAL_ITERATOR, 0);
			ma_policy_bag_add_ref(it->bag = self);
			*iterator = &it->base;			
			return rc;
        }
        return MA_ERROR_OUTOFMEMORY;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_policy_bag_iterator_get_next(ma_policy_bag_iterator_t *iterator, const ma_policy_uri_t **uri, const char **section_name, const char **key_name, const ma_variant_t **key_value) {
    if(iterator && uri && section_name && key_name && key_value) {
        ma_variant_t *element = NULL;
        ma_error_t  rc = MA_OK;
		ma_policy_bag_general_iterator_t *it = (ma_policy_bag_general_iterator_t *)iterator;

		if(!it->bag || (MA_POLICY_BAG_GENERAL_ITERATOR != it->base.type))
			return MA_ERROR_POLICY_INVALID_BAG_ITERATOR;

		if(!it->uri_it) {
			if(MA_OK == (rc = ma_policy_bag_get_uri_iterator(it->bag, &it->uri_it))) {
				if(MA_ERROR_NO_MORE_ITEMS == (rc = ma_policy_bag_uri_iterator_get_next(it->uri_it, &it->cur_uri))) {
					ma_policy_bag_iterator_release(it->uri_it), it->uri_it = NULL;
					return rc;
				}
			} 
			
			if(rc != MA_OK)				
				return rc;			
		}

		if(!it->sec_it) {
			if(MA_OK == (rc = ma_policy_bag_get_section_iterator(it->bag, it->cur_uri, &it->sec_it))) {
				if(MA_ERROR_NO_MORE_ITEMS == (rc = ma_policy_bag_section_iterator_get_next(it->sec_it, &it->cur_section))) {
					ma_policy_bag_iterator_release(it->sec_it), it->sec_it = NULL;
					if(MA_ERROR_NO_MORE_ITEMS ==  (rc = ma_policy_bag_uri_iterator_get_next(it->uri_it, &it->cur_uri))) {
						ma_policy_bag_iterator_release(it->uri_it), it->uri_it = NULL;
						return rc;
					}
					else {
						if(MA_OK == rc)
							return ma_policy_bag_iterator_get_next(iterator, uri, section_name, key_name, key_value);
					}
				}
			}

			if(MA_OK != rc)
				return rc;
		}
		
		if(!it->set_it) {
			if(MA_OK != (rc = ma_policy_bag_get_setting_iterator(it->bag, it->cur_uri, it->cur_section, &it->set_it)))
				return rc;
		}
		
		if(it->set_it) {
			const char *name = NULL;
			const ma_variant_t *value = NULL;
			if(MA_OK == (rc = ma_policy_bag_setting_iterator_get_next(it->set_it, &name, &value))) {
				*uri = it->cur_uri;
				*section_name = it->cur_section;
				*key_name = name;
				*key_value = value;
				return MA_OK;
			}
			
			if(MA_ERROR_NO_MORE_ITEMS == rc) {
				ma_policy_bag_iterator_release(it->set_it), it->set_it = NULL;
				if(MA_ERROR_NO_MORE_ITEMS == ma_policy_bag_section_iterator_get_next(it->sec_it, &it->cur_section)) {
					ma_policy_bag_iterator_release(it->sec_it), it->sec_it = NULL;
					if(MA_ERROR_NO_MORE_ITEMS ==  (rc = ma_policy_bag_uri_iterator_get_next(it->uri_it, &it->cur_uri))) {
						ma_policy_bag_iterator_release(it->uri_it), it->uri_it = NULL;
						return rc;
					}
					else
						return ma_policy_bag_iterator_get_next(iterator, uri, section_name, key_name, key_value);
				}
				else 
					return ma_policy_bag_iterator_get_next(iterator, uri, section_name, key_name, key_value);
			}			
		}

		return rc;
	}
    return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_policy_bag_general_iterator_release(ma_policy_bag_general_iterator_t *iterator) {  
	if(iterator->uri_it)
		ma_policy_bag_iterator_release(iterator->uri_it);
	if(iterator->sec_it)
		ma_policy_bag_iterator_release(iterator->sec_it);
	if(iterator->set_it)
		ma_policy_bag_iterator_release(iterator->set_it);

	if(iterator->bag)
		ma_policy_bag_release(iterator->bag);
	
	free(iterator);
    return MA_OK;
}

ma_error_t ma_policy_bag_get_uri_iterator(ma_policy_bag_t *self,  ma_policy_bag_iterator_t **iterator) {
	if(self && iterator) {
		ma_policy_bag_uri_iterator_t *it = (ma_policy_bag_uri_iterator_t *)calloc(1, sizeof(ma_policy_bag_uri_iterator_t));
		if(it) {
			ma_error_t rc = MA_OK;
			size_t count = 0, i = 0;
			(void) ma_array_size(self->policy_data, &count);			
			
			ma_policy_bag_iterator_init((ma_policy_bag_iterator_t *)it, MA_POLICY_BAG_URI_ITERATOR, count);

			if(count) {
				ma_vector_init(it, count, uri_list, ma_policy_uri_t *);
				for(i = 1; i <= count; i++) {
					ma_variant_t *policy_element = NULL;
					if(MA_OK == (rc = ma_array_get_element_at(self->policy_data, i, &policy_element))) {
						ma_array_t *policy_arr = NULL;
						if(MA_OK == (rc = ma_variant_get_array(policy_element, &policy_arr))) {
							ma_variant_t *props_var = NULL;
							if(MA_OK == (rc = ma_array_get_element_at(policy_arr, 1, &props_var))) {
								ma_table_t *props_tb = NULL;
								if(MA_OK == (rc = ma_variant_get_table(props_var, &props_tb))) {
									ma_policy_uri_t *uri = NULL;
									ma_policy_uri_create_from_table(props_tb, &uri);
									ma_vector_push_back(it, uri, ma_policy_uri_t *, uri_list);
									ma_table_release(props_tb);
								}
								(void) ma_variant_release(props_var);
							}
							(void) ma_array_release(policy_arr);
						}
						(void) ma_variant_release(policy_element);
					}					
				}
			}
			
			*iterator = &it->base;

			if(MA_OK != rc) ma_policy_bag_iterator_release(*iterator), *iterator = NULL;
			return rc;
		}
		return MA_ERROR_OUTOFMEMORY;
	}
	return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_policy_bag_uri_iterator_release(ma_policy_bag_uri_iterator_t *iterator) {
	if(iterator->base.count)
		ma_vector_clear(iterator, ma_policy_uri_t *, uri_list, ma_policy_uri_release);
	free(iterator);
	return MA_OK;
}

ma_error_t ma_policy_bag_get_section_iterator(ma_policy_bag_t *self, const ma_policy_uri_t *uri, ma_policy_bag_iterator_t **iterator) {
	if(self && iterator) {
		ma_policy_bag_section_iterator_t *it = (ma_policy_bag_section_iterator_t *)calloc(1, sizeof(ma_policy_bag_section_iterator_t));
		if(it) {
			ma_error_t rc = MA_OK;
			ma_variant_t *sections_var = NULL;			
			if(MA_OK == (rc = get_policy_data_from_server(self, MA_PROP_VALUE_GET_SECTIONS_BY_URI_REQUEST_STR, uri, NULL, &sections_var))) {
				ma_array_t *sections_array = NULL;
				if(MA_OK == (rc = ma_variant_get_array(sections_var, &sections_array))) {	
					size_t count = 0;
					(void) ma_array_size(sections_array, &count);
					ma_policy_bag_iterator_init((ma_policy_bag_iterator_t *)it, MA_POLICY_BAG_SECTION_ITERATOR, count);
					it->secitons_array = sections_array;
					*iterator = &it->base;
				}
				(void) ma_variant_release(sections_var);
			}

			if(MA_OK != rc)	free(it);
			return rc;
		}
		return MA_ERROR_OUTOFMEMORY;
	}
	return MA_ERROR_INVALIDARG;	
}

static ma_error_t ma_policy_bag_section_iterator_release(ma_policy_bag_section_iterator_t *iterator) {
	ma_array_release(iterator->secitons_array);
	free(iterator);
	return MA_OK;
}

ma_error_t ma_policy_bag_get_setting_iterator(ma_policy_bag_t *self, const ma_policy_uri_t *uri, const char *section, ma_policy_bag_iterator_t **iterator) {
	if(self && iterator) {
		ma_policy_bag_setting_iterator_t *it = (ma_policy_bag_setting_iterator_t *)calloc(1, sizeof(ma_policy_bag_setting_iterator_t));
		if(it) {
			ma_error_t rc = MA_OK;
			ma_variant_t *settings_var = NULL;			
			if(MA_OK == (rc = get_policy_data_from_server(self, MA_PROP_VALUE_GET_SECTTINGS_BY_SECITON_REQUEST_STR, uri, section, &settings_var))) {
				ma_table_t *settings_tb = NULL;
				if(MA_OK == (rc = ma_variant_get_table(settings_var, &settings_tb))) {	
					ma_array_t *settings_names_array = NULL;
					if(MA_OK == (rc = ma_table_get_keys(settings_tb, &settings_names_array))) {
						size_t count = 0;
						(void) ma_array_size(settings_names_array, &count);
						ma_policy_bag_iterator_init((ma_policy_bag_iterator_t *)it, MA_POLICY_BAG_SETTING_ITERATOR, count);
						it->settings_array = settings_names_array;
						it->settings_table = settings_tb;
						*iterator = &it->base;
					}
				}
				(void) ma_variant_release(settings_var);
			}

			if(MA_OK != rc)	free(it);
			return rc;
		}
		return MA_ERROR_OUTOFMEMORY;
	}
	return MA_ERROR_INVALIDARG;	
}

static ma_error_t ma_policy_bag_setting_iterator_release(ma_policy_bag_setting_iterator_t *iterator) {
	ma_array_release(iterator->settings_array);
	ma_table_release(iterator->settings_table);
	free(iterator);
	return MA_OK;
}

ma_error_t ma_policy_bag_iterator_release(ma_policy_bag_iterator_t *iterator) {
    if(iterator) {
        ma_error_t rc = MA_OK;		
		switch(iterator->type) {
		case MA_POLICY_BAG_GENERAL_ITERATOR:
			rc = ma_policy_bag_general_iterator_release((ma_policy_bag_general_iterator_t *)iterator);			
			break;
		case MA_POLICY_BAG_URI_ITERATOR:
			rc = ma_policy_bag_uri_iterator_release((ma_policy_bag_uri_iterator_t *)iterator);
			break;
		case MA_POLICY_BAG_SECTION_ITERATOR:
			rc = ma_policy_bag_section_iterator_release((ma_policy_bag_section_iterator_t *)iterator);
			break;
		case MA_POLICY_BAG_SETTING_ITERATOR:
			rc = ma_policy_bag_setting_iterator_release((ma_policy_bag_setting_iterator_t *)iterator);
			break;
		default:
			break;
		}

        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_policy_bag_uri_iterator_get_next(ma_policy_bag_iterator_t *iterator, const ma_policy_uri_t **uri) {
	if(iterator && uri) {
		ma_policy_bag_uri_iterator_t *it = (ma_policy_bag_uri_iterator_t *)iterator;
		if(MA_POLICY_BAG_URI_ITERATOR == it->base.type) {
			if(it->base.count && it->base.index < it->base.count) {				
				*uri = ma_vector_at(it, it->base.index, ma_policy_uri_t *, uri_list);
				it->base.index++;
				return MA_OK;
			}
			return MA_ERROR_NO_MORE_ITEMS;
		}
		return MA_ERROR_INVALID_OPERATION;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_policy_bag_section_iterator_get_next(ma_policy_bag_iterator_t *iterator, const char **section_name) {
	if(iterator && section_name) {
		ma_policy_bag_section_iterator_t *it = (ma_policy_bag_section_iterator_t *)iterator;
		if(MA_POLICY_BAG_SECTION_ITERATOR == it->base.type) {
			if(it->base.count && it->base.index < it->base.count) {				
				ma_error_t rc = MA_OK;
				ma_variant_t *sec_var = NULL;
				it->base.index++;
				if(MA_OK == (rc = ma_array_get_element_at(it->secitons_array, it->base.index, &sec_var))) {
					ma_buffer_t *buffer = NULL;
					if(MA_OK == (rc = ma_variant_get_string_buffer(sec_var, &buffer))) {
						const char *str = NULL;
						size_t size = 0;
						if(MA_OK == (rc = ma_buffer_get_string(buffer, &str, &size))) {
							*section_name = str;
						}
						(void) ma_buffer_release(buffer);
					}
					(void) ma_variant_release(sec_var);
				}
				return rc;
			}
			return MA_ERROR_NO_MORE_ITEMS;
		}
		return MA_ERROR_INVALID_OPERATION;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_policy_bag_setting_iterator_get_next(ma_policy_bag_iterator_t *iterator, const char **key_name, const ma_variant_t **key_value) {
	if(iterator && key_name && key_value) {
		ma_policy_bag_setting_iterator_t *it = (ma_policy_bag_setting_iterator_t *)iterator;
		if(MA_POLICY_BAG_SETTING_ITERATOR == it->base.type) {
			if(it->base.count && it->base.index < it->base.count) {				
				ma_error_t rc = MA_OK;
				ma_variant_t *sec_var = NULL;
				it->base.index++;
				if(MA_OK == (rc = ma_array_get_element_at(it->settings_array, it->base.index, &sec_var))) {
					ma_buffer_t *buffer = NULL;
					if(MA_OK == (rc = ma_variant_get_string_buffer(sec_var, &buffer))) {
						const char *str = NULL;
						size_t size = 0;
						if(MA_OK == (rc = ma_buffer_get_string(buffer, &str, &size))) {
							ma_variant_t *value = NULL;
							if(MA_OK == (rc = ma_table_get_value(it->settings_table, str, &value))) {								
								*key_name = str;
								*key_value = value;
								(void) ma_variant_release(value);
							}							
						}
						(void) ma_buffer_release(buffer);
					}
					(void) ma_variant_release(sec_var);
				}
				return rc;
			}
			return MA_ERROR_NO_MORE_ITEMS;
		}
		return MA_ERROR_INVALID_OPERATION;
	}
	return MA_ERROR_INVALIDARG;
}

/* 
	MA_PROP_VALUE_GET_POLICIES_BY_URI_REQUEST_STR    -> uri is mandotory, section is NULL  -> returns all sections including settings for specified uri
	MA_PROP_VALUE_GET_SECTIONS_BY_URI_REQUEST_STR    -> uri is mandotory, section is NULL  -> returns array of section names for specified uri
	MA_PROP_VALUE_GET_SECTTINGS_BY_SECITON_REQUEST_STR  -> uri and section are mandotory   -> returns settings table for specified section and uri
*/

static ma_error_t get_policy_data_from_server(const ma_policy_bag_t *self, const char *request_type, const ma_policy_uri_t *uri, const char *section, ma_variant_t **value) {	
	if(self && self->ma_client) {
		ma_error_t rc = MA_OK;
        ma_context_t *ma_context = NULL;
        ma_client_manager_t *client_manager = NULL;
        ma_msgbus_t *msgbus = NULL;
		ma_client_get_context(self->ma_client, &ma_context);
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);		
        msgbus = MA_CONTEXT_GET_MSGBUS(ma_context);
        if(ma_context && client_manager && msgbus) {
            ma_error_t rc = MA_OK;        
            ma_msgbus_endpoint_t *policy_ep = NULL;
	        if(MA_OK == (rc = ma_msgbus_endpoint_create(msgbus, MA_POLICY_SERVICE_NAME_STR, MA_POLICY_SERVICE_HOSTNAME_STR, &policy_ep))) {
		        ma_message_t *request_msg = NULL;   
                ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;
                (void)ma_msgbus_endpoint_setopt(policy_ep, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);

                (void) ma_client_manager_get_thread_option(client_manager, &thread_option);
                (void)ma_msgbus_endpoint_setopt(policy_ep, MSGBUSOPT_THREADMODEL, thread_option);
            
		        if(MA_OK == (rc = ma_message_create(&request_msg))) {
			        ma_variant_t *payload= NULL;
                    ma_table_t *info_table = NULL;
                    if(MA_OK == (rc = ma_policy_uri_get_table((ma_policy_uri_t *)uri, &info_table))) {
			            if(MA_OK == (rc = ma_variant_create_from_table(info_table, &payload))) {
                            ma_message_t *response_msg = NULL;
				            (void)ma_message_set_property(request_msg, MA_PROP_KEY_POLICY_REQUEST_TYPE_STR, request_type);

							if(self->product_id)
								(void)ma_message_set_property(request_msg, MA_PROP_KEY_PRODUCT_ID_STR, self->product_id);

							if(section)
								(void)ma_message_set_property(request_msg, MA_PROP_KEY_SECTION_NAME_STR, section);
								
				            (void)ma_message_set_payload(request_msg, payload);
				            if(MA_OK == (rc = ma_msgbus_send(policy_ep, request_msg, &response_msg))) {
                                ma_variant_t *res_payload = NULL;
                                if(MA_OK == (rc = ma_message_get_payload(response_msg, &res_payload))) {
                                    ma_variant_add_ref(*value = res_payload);
                                    (void)ma_variant_release(res_payload);
                                }
                                (void)ma_message_release(response_msg);
                            }                    
				            (void)ma_variant_release(payload);
			            }
                        (void)ma_table_release(info_table);
                    }
			        (void)ma_message_release(request_msg);
		        }
		        (void)ma_msgbus_endpoint_release(policy_ep);
	        }
            return (MA_OK == rc) ? MA_OK : MA_ERROR_POLICY_INTERNAL;
        }
	}
	return MA_ERROR_INVALID_OPERATION;
}