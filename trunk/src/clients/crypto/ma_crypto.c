#include "ma/crypto/ma_crypto.h"
#include "ma/ma_message.h"
#include "ma/ma_msgbus.h"

#include "ma/internal/clients/ma/client/ma_client_manager.h"
#include "ma/internal/clients/ma/ma_client_internal.h"
#include "ma/internal/defs/ma_crypto_defs.h"
#include "ma/internal/defs/ma_object_defs.h"

#include <string.h>

#define IS_VALID_ENC_ALGO_TYPE(s)       (!strcmp(s, MA_CRYPTO_ALGO_TYPE_3DES_STR) || !strcmp(s, MA_CRYPTO_ALGO_TYPE_AES128_STR))
#define IS_VALID_DEC_ALGO_TYPE(s)       (!strcmp(s, MA_CRYPTO_ALGO_TYPE_3DES_STR) || !strcmp(s, MA_CRYPTO_ALGO_TYPE_AES128_STR) || !strcmp(s, MA_CRYPTO_ALGO_TYPE_POLICY_AES128_STR))
#define IS_VALID_DIGEST_ALGO_TYPE(s)    (!strcmp(s, MA_CRYPTO_DIGEST_TYPE_SHA1_STR) || !strcmp(s, MA_CRYPTO_DIGEST_TYPE_SHA256_STR))


static ma_error_t request_to_crypto_service(ma_client_t *client, const char *req_type, const char *algo_type, const unsigned char *data, size_t data_len, ma_buffer_t **buffer) {
    ma_error_t rc = MA_OK;
    ma_context_t *context = NULL;
    if((MA_OK == (rc = ma_client_get_context(client, &context)))) {
        ma_msgbus_t *msgbus = MA_CONTEXT_GET_MSGBUS(context);
        ma_client_manager_t *client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(context);
        if(msgbus && client_manager) {
            ma_msgbus_endpoint_t * endpoint = NULL;        
            if(MA_OK == (rc = ma_msgbus_endpoint_create(msgbus, MA_CRYPTO_SERVICE_NAME_STR, NULL, &endpoint) )) {
                ma_message_t *req_msg = NULL, *res_msg = NULL;
               
                ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;                

                (void) ma_client_manager_get_thread_option(client_manager, &thread_option);
                ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);
                ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                if((MA_OK == (rc = ma_message_create(&req_msg)))) {
                    ma_variant_t *req_payload = NULL;
                    (void) ma_message_set_property(req_msg, MA_PROP_KEY_CRYPTO_REQUEST_TYPE_STR, req_type);
                    (void) ma_message_set_property(req_msg, MA_PROP_KEY_ATTR_ALGO_ID_STR, algo_type);                    
                    if((MA_OK == (rc = ma_variant_create_from_raw(data, data_len, &req_payload))))  {
                        if((MA_OK == (rc = ma_message_set_payload(req_msg, req_payload)))) {
                            if((MA_OK == (rc = ma_msgbus_send(endpoint, req_msg, &res_msg)))) {
                                ma_variant_t *res_payload = NULL;
                                if((MA_OK == (rc = ma_message_get_payload(res_msg, &res_payload)))) {
                                    rc = ma_variant_get_raw_buffer(res_payload, buffer);
                                    (void) ma_variant_release(res_payload);
                                }
                                (void) ma_message_release(res_msg);
                            }                            
                        }
                        (void) ma_variant_release(req_payload);
                    }                  
                    (void) ma_message_release(req_msg);
                }
                (void) ma_msgbus_endpoint_release(endpoint);
            }
        }
        else
            rc = MA_ERROR_INVALID_CLIENT_OBJECT;
    }
    return rc;
}

ma_error_t ma_crypto_encrypt_data_by_type(ma_client_t *client, const char *algo_type, const unsigned char *data, size_t data_len, ma_buffer_t **buffer) {
    if(client && IS_VALID_ENC_ALGO_TYPE(algo_type) && data && data_len > 0 && buffer) {               
        return request_to_crypto_service(client, MA_PROP_VALUE_ENCRYPT_REQUEST_STR, algo_type, data, data_len, buffer);
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_crypto_decrypt_data_by_type(ma_client_t *client, const char *algo_type, const unsigned char *data, size_t data_len, ma_buffer_t **buffer) {
    if(client && IS_VALID_DEC_ALGO_TYPE(algo_type) && data && data_len > 0 && buffer) {
        return request_to_crypto_service(client, MA_PROP_VALUE_DECRYPT_REQUEST_STR, algo_type, data, data_len, buffer);
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_crypto_hash_data_by_type(ma_client_t *client,  const char *digest_type, const unsigned char *data, size_t data_len, ma_buffer_t **buffer) {
    if(client && IS_VALID_DIGEST_ALGO_TYPE(digest_type) && data && data_len > 0 && buffer) {
        return request_to_crypto_service(client, MA_PROP_VALUE_HASH_REQUEST_STR, digest_type, data, data_len, buffer);
    }
    return MA_ERROR_INVALIDARG;
}


