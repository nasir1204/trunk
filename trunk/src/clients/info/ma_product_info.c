#include "ma/info/ma_product_info.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/defs/ma_io_defs.h"
#include "ma/internal/clients/ma/ma_client_internal.h"

#include "ma/ma_msgbus.h"
#include "ma/ma_message.h"


ma_error_t ma_product_info_get_license_state(ma_client_t *client, const char *software_id, ma_buffer_t **license_state) {
    if(client && software_id && license_state) {
        ma_context_t *context = NULL;      
        ma_client_get_context(client, &context);
		if(context) {
            ma_error_t rc = MA_OK;
            ma_message_t *req_msg = NULL, *res_msg = NULL;
            if(MA_OK == (rc = ma_message_create(&req_msg))) {                
                ma_msgbus_endpoint_t *endpoint = NULL;
                if((MA_OK == (rc = ma_message_set_property(req_msg, MA_PROP_KEY_REQUEST_TYPE_STR, MA_PROP_VALUE_GET_PRODUCT_LICENSE_STATE_STR)))
                    && (MA_OK == (rc = ma_message_set_property(req_msg, MA_PROP_KEY_PRODUCT_ID_STR, software_id)))
                  ) {
                    if(MA_OK == (rc = ma_msgbus_endpoint_create(MA_CONTEXT_GET_MSGBUS(context), MA_IO_SERVICE_NAME_STR, NULL, &endpoint))) {
				        (void) ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                        if(MA_OK == (rc = ma_msgbus_send(endpoint, req_msg, &res_msg))) { 
                            ma_variant_t *value = NULL;
                            if(MA_OK == (rc = ma_message_get_payload(res_msg, &value))) {
                                rc = ma_variant_get_string_buffer(value, license_state);
                                (void) ma_variant_release(value);
                            }
                            ma_message_release(res_msg) ;
                        }
                        else
                            rc = (MA_ERROR_OBJECTNOTFOUND == rc) ? MA_ERROR_LICENSE_STATE_NOT_FOUND : rc;
                        (void) ma_msgbus_endpoint_release(endpoint) ;
                    }
                }
                (void) ma_message_release(req_msg) ;
            }
			return rc;
        }
		return MA_ERROR_INVALID_CLIENT_OBJECT;
    }
    return MA_ERROR_INVALIDARG;
}

