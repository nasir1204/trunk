#include "ma/info/ma_info.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/defs/ma_io_defs.h"
#include "ma/internal/clients/ma/ma_client_internal.h"
#include "ma/internal/clients/ma/client/ma_client_manager.h"
#include "ma/internal/defs/ma_info_defs.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_message.h"

#include "ma/internal/ma_macros.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "agent_info_client"



typedef struct ma_info_client_s ma_info_client_t, *ma_info_client_h ;

struct ma_info_client_s {
    ma_client_base_t            base_client ;
    ma_client_t					*ma_client ;
    char                        *product_id ;
	ma_table_t					*ma_info_table ;
    ma_info_events_cb_t			cb ;
    void                        *cb_data ;
};


static ma_error_t ma_info_client_create(ma_client_t *ma_client, const char *product_id, ma_info_events_cb_t cb, void *cb_data, ma_client_base_t **info_client) ;
static ma_error_t ma_info_client_on_message(ma_client_base_t *client, const char *topic, ma_message_t *message) ;
static ma_error_t ma_info_client_release(ma_client_base_t *info_client) ;

static const ma_client_base_methods_t info_client_methods = {
    &ma_info_client_on_message,
    &ma_info_client_release
};

static ma_error_t ma_info_events_cb(ma_client_t *ma_client, const char *product_id, const char *info_event, void *cb_data) {
	return MA_OK ;
}

ma_error_t ma_info_events_register_callback(ma_client_t *ma_client, const char *product_id, ma_info_events_cb_t cb, void *cb_data) {
    if(ma_client && cb) {
        ma_context_t *ma_context = NULL ;
        ma_client_manager_t *client_manager = NULL ; 
		ma_logger_t *logger = NULL ;

        (void)ma_client_get_context(ma_client, &ma_context) ;
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context);		
		
		logger = MA_CONTEXT_GET_LOGGER(ma_context) ;
		
		product_id = (product_id) ? product_id: MA_CONTEXT_GET_PRODUCT_ID(ma_context) ;

        if(ma_context && client_manager && product_id) {
            ma_error_t rc = MA_OK ;
            char client_name[MAX_CLIENT_NAME_LENGTH] = {0} ;
            ma_client_base_t *client = NULL ;	    

            MA_MSC_SELECT(_snprintf, snprintf)(client_name, MAX_CLIENT_NAME_LENGTH,"%s.%s", MA_INFO_EVENTS_CLIENT_NAME_STR, product_id) ;

            if(ma_client_manager_has_client(client_manager, client_name)) {
				MA_LOG(logger, MA_LOG_SEV_ERROR, "Client %s already registered", client_name) ;
                return MA_ERROR_ALREADY_REGISTERED ;
			}

			if(MA_OK == (rc =  ma_info_client_create(ma_client, product_id, cb , cb_data, &client))) {			
				if(MA_OK != (rc = ma_client_manager_add_client(client_manager, client_name, client))) {
					(void)ma_info_client_release(client) ;
				}
				else {
					MA_LOG(logger, MA_LOG_SEV_DEBUG, "Client %s registered", client_name) ;
				}
            }
			return rc ;
        }
        return MA_ERROR_INVALID_CLIENT_OBJECT ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_info_events_unregister_callback(ma_client_t *ma_client, const char *product_id) {
    if(ma_client) {
        ma_context_t *ma_context = NULL ;
        ma_client_manager_t *client_manager = NULL ;

        (void)ma_client_get_context(ma_client, &ma_context) ;
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context) ;
        
		product_id = (product_id) ? product_id: MA_CONTEXT_GET_PRODUCT_ID(ma_context) ;

        if(client_manager && product_id) {
            ma_error_t rc = MA_OK ;
            char client_name[MAX_CLIENT_NAME_LENGTH] = {0} ;
            ma_client_base_t *client = NULL ; 

            MA_MSC_SELECT(_snprintf, snprintf)(client_name, MAX_CLIENT_NAME_LENGTH,"%s.%s", MA_INFO_EVENTS_CLIENT_NAME_STR, product_id) ;
            if(MA_OK != (rc = ma_client_manager_get_client(client_manager, client_name, &client)))
                return MA_ERROR_CLIENT_NOT_FOUND ;
            
            (void)ma_client_manager_remove_client(client_manager, client) ;

            return ma_client_base_release(client) ;
        }
        return MA_ERROR_INVALID_CLIENT_OBJECT ;
    }
    return MA_ERROR_INVALIDARG ;
}


static ma_error_t ma_info_client_create(ma_client_t *ma_client, const char *product_id, ma_info_events_cb_t cb, void *cb_data, ma_client_base_t **info_client) {
	ma_context_t *ma_context = NULL ;
	ma_client_manager_t *client_manager = NULL ;
	ma_error_t rc = MA_OK ;

	ma_info_client_t *client = (ma_info_client_t *)calloc(1,sizeof(ma_info_client_t)) ;
	if(!client)     return MA_ERROR_OUTOFMEMORY ;

    ma_client_get_context(ma_client, &ma_context) ;   
    client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(ma_context) ;

	client->product_id = strdup(product_id) ;
	client->cb = cb ;
	client->cb_data = cb_data ;
	client->ma_client = ma_client ;
	ma_client_base_init((ma_client_base_t *)client, &info_client_methods, client) ;
		
	*info_client = (ma_client_base_t *)client ;

	return MA_OK ;
}

static ma_error_t ma_info_client_on_message(ma_client_base_t *client, const char *topic, ma_message_t *recv_msg) {
	if(client && topic && recv_msg) {
		if(0 == strcmp(MA_INFO_EVENTS_PUBSUB_TOPIC_STR , topic)) {
			ma_info_client_t *self = (ma_info_client_t *)client ;
			ma_error_t rc = MA_OK ;
			char *info_event = NULL ;
			ma_variant_t *payload = NULL ;
			ma_table_t *info_table = NULL ;

			(void)ma_message_get_property(recv_msg, MA_INFO_MSG_KEY_EVENT_TYPE_STR, &info_event) ;
			(void)ma_message_get_payload(recv_msg, &payload) ;

			if(MA_OK == ma_variant_get_table(payload, &info_table)) {
				if(self->ma_info_table) {
					ma_table_release(self->ma_info_table) ; self->ma_info_table = NULL ;
				}
				self->ma_info_table = info_table ;
			}
			ma_variant_release(payload) ; payload = NULL ;

			self->cb(self->ma_client, self->product_id, info_event, self->cb_data) ;
		}
		return MA_OK ;
	}
	return MA_ERROR_INVALIDARG ;
}

static ma_error_t ma_info_client_release(ma_client_base_t *info_client) {
    if(info_client) {
        ma_info_client_t *self = (ma_info_client_t *)info_client ;

        if(self->product_id) free(self->product_id) ;
		self->product_id = NULL ;
		
		if(self->ma_info_table)	ma_table_release(self->ma_info_table) ;
		self->ma_info_table = NULL ;

        free(self) ;
        
		return MA_OK;
    }
    return MA_ERROR_INVALIDARG ;
}

static ma_error_t get_info(ma_info_client_t *info_client, char *client_name, ma_msgbus_callback_thread_options_t thread_option) ;

ma_error_t ma_info_get(ma_client_t *ma_client, const char *key, ma_variant_t **agent_info_variant) {
	if(ma_client && key && agent_info_variant) {
		ma_context_t *context = NULL ;
        ma_client_manager_t *client_manager = NULL ;
		const char *product_id = NULL ;
		ma_error_t rc  = MA_OK ;		

        (void)ma_client_get_context(ma_client, &context) ;
        client_manager = MA_CONTEXT_GET_CLIENT_MANAGER(context) ;
        
		product_id = MA_CONTEXT_GET_PRODUCT_ID(context) ;

		if(client_manager && product_id) {
			char client_name[MAX_CLIENT_NAME_LENGTH] = {0} ;
			 ma_client_base_t *client = NULL ;

			MA_MSC_SELECT(_snprintf, snprintf)(client_name, MAX_CLIENT_NAME_LENGTH,"%s.%s", MA_INFO_EVENTS_CLIENT_NAME_STR, product_id);
            rc = ma_client_manager_get_client(client_manager, client_name, &client) ;

			if(MA_ERROR_CLIENT_NOT_FOUND == rc){
				if(MA_OK == (rc = ma_info_events_register_callback(ma_client, product_id, ma_info_events_cb, NULL))) {
					/*Do a sync call to get the info , as this is the first call */
					if(MA_OK == (rc = ma_client_manager_get_client(client_manager, client_name, &client))){
                        ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL ;
                         (void) ma_client_manager_get_thread_option(client_manager, &thread_option);
      
						rc = get_info((ma_info_client_t *)client, client_name, thread_option) ;
                    }
				}
			}
			
			if(client) {
				ma_info_client_t *self = (ma_info_client_t*)client;
				rc = ma_table_get_value(self->ma_info_table, key, agent_info_variant);
				/*Case 1: Client is registered but callback registration or first time get_info() failed (Bug 1006752)*/
				/*Case 2: Client is registered but always expect sync call */
				if(MA_OK != rc){
					ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL ;
                    (void) ma_client_manager_get_thread_option(client_manager, &thread_option);
					if(MA_OK == (rc = get_info((ma_info_client_t *)client, client_name, thread_option))) {
						rc = ma_table_get_value(self->ma_info_table, key, agent_info_variant);
					}
				}
			}

			return rc ;
		}
		return MA_ERROR_INVALID_CLIENT_OBJECT ;
	}
	return MA_ERROR_INVALIDARG ;
}

static ma_error_t get_info(ma_info_client_t *self, char *client_name, ma_msgbus_callback_thread_options_t to) {
	if(self && client_name) {
        ma_error_t rc = MA_OK ;
		ma_context_t *context = NULL ;
        ma_message_t *req_msg = NULL, *response = NULL ;
        ma_variant_t *payload = NULL ;

        if(MA_MSGBUS_CALLBACK_THREAD_IO == to) return MA_ERROR_INVALID_OPERATION;

		(void)ma_client_get_context(self->ma_client, &context) ;

		if(MA_OK == (rc = ma_message_create(&req_msg))) {
            ma_msgbus_endpoint_t *endpoint = NULL ;

			ma_message_set_property(req_msg, MA_PROP_KEY_REQUEST_TYPE_STR, MA_PROP_VALUE_AGENT_INFO_COLLECT) ;     

            if(MA_OK == (rc = ma_msgbus_endpoint_create(MA_CONTEXT_GET_MSGBUS(context), MA_IO_SERVICE_NAME_STR, NULL, &endpoint))) {
				(void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC) ;
                (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, to) ;
                if(MA_OK == (rc = ma_msgbus_send(endpoint, req_msg, &response))) {
                    if(MA_OK == (rc = ma_message_get_payload(response, &payload))) {
						if(self->ma_info_table) ma_table_release(self->ma_info_table), self->ma_info_table = NULL;
						ma_variant_get_table(payload, &self->ma_info_table) ;
						ma_variant_release(payload) ; payload = NULL ;
					}
                    ma_message_release(response) ;	response = NULL ;
                }
                ma_msgbus_endpoint_release(endpoint) ;	endpoint = NULL ;
            }
            ma_message_release(req_msg) ;	req_msg = NULL ;
        }
        
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}