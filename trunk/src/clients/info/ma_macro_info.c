#include "ma/info/ma_macro_info.h"

#ifdef MA_WINDOWS
#include <Shlobj.h>
#include <stdio.h>
#include <windows.h>
#include <tchar.h>
#include <lm.h>
#include <intrin.h>
#include <ntsecapi.h>
#include <wbemidl.h>
#include <ClusApi.h>
#include <WtsApi32.h>

static int wide_char_to_utf8(char *dest , size_t dest_length, const wchar_t *src , size_t src_length) {            
    size_t space_needed = 0 ;
    /*
        If this parameter is -1, the function processes the entire input string, including the terminating null character. 
        Therefore, the resulting Unicode string has a terminating null character, and the length returned by the function includes this character.
    */
    if(0 == (space_needed = WideCharToMultiByte(CP_UTF8, 0 , src ,  (0 == src_length) ? -1 : src_length, NULL , 0 , NULL, NULL)))
        return -1 ; //Failed to get the size 

    if(!dest)   return (0 == src_length) ? space_needed : space_needed + 1;

    return dest_length < space_needed
            ? -1
            : WideCharToMultiByte( CP_UTF8,0, src , src_length , dest ,  dest_length  , NULL, NULL) ;
            
}

static ma_error_t cstr_to_mabuffer(const char *buffer, size_t size, ma_buffer_t **macro_value){
	ma_error_t rc = MA_OK;
	ma_buffer_t *m_value = NULL;
	if(MA_OK == (rc = ma_buffer_create(&m_value, size))){
		ma_buffer_set(m_value, buffer, size);		
		*macro_value = m_value;
	}
	return rc;
}

ma_error_t ma_macro_info_get(const char *macro_name, ma_buffer_t **macro_value){
	if(	macro_name && macro_value){
		char value[1024] = {0};
		unsigned long size = sizeof(value)-1;
		ma_error_t rc = MA_ERROR_APIFAILED;

		if(0 == strcmp(macro_name, MA_AGENT_MACRO_COMPUTERNAME) ) {
			if(GetComputerNameA(value, &size))
				return cstr_to_mabuffer(value, size, macro_value);
				
		}
		else if(0 == strcmp(macro_name, MA_AGENT_MACRO_USERNAME)) {
			if(GetUserNameA(value, &size))
				return cstr_to_mabuffer(value, size, macro_value);
		}
		else if(0 == strcmp(macro_name, MA_AGENT_MACRO_DOMAINNAME)) {
			DWORD dw_level = 102;
			LPWKSTA_INFO_102 p_buf = NULL;	
			NET_API_STATUS nwstatus;
			LPWSTR server_name = NULL;
			size_t wlen = 0, clen = 0;  

			if (0 == NetWkstaGetInfo(server_name, dw_level, (LPBYTE *)&p_buf)) {
				wide_char_to_utf8(value, size, p_buf->wki102_langroup, wcsnlen(p_buf->wki102_langroup, size-1));
				NetApiBufferFree(p_buf);
			}
			return cstr_to_mabuffer(value, size, macro_value);				
		}
		else if(0 == strcmp(macro_name, MA_AGENT_MACRO_SYS_DRIVE)) {			
			GetWindowsDirectoryA(value, size);
			if(value[2] == '\\')
					value[2] = '\0';
			return cstr_to_mabuffer(value, size, macro_value);				
		}
		else if(0 == strcmp(macro_name, MA_AGENT_MACRO_SYS_ROOT_DRIVE)) {
			GetWindowsDirectoryA(value, size);				
			return cstr_to_mabuffer(value, size, macro_value);				
		}
		else if(0 == strcmp(macro_name, MA_AGENT_MACRO_SYS_DIR)) {
			GetSystemDirectoryA(value, size);
			return cstr_to_mabuffer(value, size, macro_value);				
		}
		else if(0 == strcmp(macro_name, MA_AGENT_MACRO_PROG_FILES_DIR)) {
			HKEY    h_key = NULL;

			if(ERROR_SUCCESS == RegOpenKeyA(HKEY_LOCAL_MACHINE, "SOFTWARE\\Microsoft\\Windows\\CurrentVersion", &h_key)){
				if(ERROR_SUCCESS == RegQueryValueExA(h_key, "ProgramFilesDir", NULL, NULL, (LPBYTE)value, &size)){
					rc = cstr_to_mabuffer(value, size, macro_value);				
				}
				RegCloseKey(h_key);
			}
			return rc;
		}
		else if(0 == strcmp(macro_name, MA_AGENT_MACRO_PROG_FILES_COMMON_DIR)) {
			HKEY    h_key = NULL;

			if(ERROR_SUCCESS == RegOpenKeyA(HKEY_LOCAL_MACHINE, "SOFTWARE\\Microsoft\\Windows\\CurrentVersion", &h_key)){
				if(ERROR_SUCCESS == RegQueryValueExA(h_key, "CommonFilesDir", NULL, NULL, (LPBYTE)value, &size)){
					rc = cstr_to_mabuffer(value, size, macro_value);				
				}
				RegCloseKey(h_key);
			}
			return rc;
		}
		else if(0 == strcmp(macro_name, MA_AGENT_MACRO_TEMP_DIR)) {
			if(GetEnvironmentVariableA("TEMP", value, size))
				return cstr_to_mabuffer(value, size, macro_value);
		}
		else if(0 == strcmp(macro_name, MA_AGENT_MACRO_MY_DOCUMENTS)) {
			HMODULE hShell32 = LoadLibraryA("shell32.dll");
			if(hShell32) {
				typedef BOOL (WINAPI *SHGETSPECIALFOLDERPATHA) (HWND hwndOwner, LPSTR lpszPath, int nFolder, BOOL fCreate);
				SHGETSPECIALFOLDERPATHA pSHGetSpecialFolderPathA	= NULL;
				pSHGetSpecialFolderPathA = (SHGETSPECIALFOLDERPATHA)(GetProcAddress(hShell32, "SHGetSpecialFolderPathA"));

				if(pSHGetSpecialFolderPathA) {
							
					if(pSHGetSpecialFolderPathA(NULL, value, CSIDL_PERSONAL, TRUE)) 
						rc = cstr_to_mabuffer(value, size, macro_value);										
				}
				else {

					char	szUserDesktop[MAX_PATH]		= {0};
					char	szSysDriveRoot[MAX_PATH]	= {0};
					char	szUserName[MAX_PATH]		= {0};
					DWORD	dwSize						= 0;

					if(GetVersion() < 0x80000000) {
								
						dwSize = sizeof(szUserName);
								
						GetWindowsDirectoryA(szSysDriveRoot, sizeof(szSysDriveRoot));
						GetUserNameA(szUserName, &dwSize);
						sprintf(szUserDesktop, "%s\\Profiles\\%s\\Desktop", szSysDriveRoot, szUserName);
					}
					else {
						GetWindowsDirectoryA(szSysDriveRoot, sizeof(szSysDriveRoot));
						sprintf(szUserDesktop, "%s\\Desktop", szSysDriveRoot);
					}
					rc = cstr_to_mabuffer(szUserDesktop, strlen(szUserDesktop), macro_value);										
				}
  				FreeLibrary(hShell32);
				return rc;
			}
		}
		else{
			return MA_ERROR_INVALID_OPTION;
		}
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}
#else
ma_error_t ma_client_utils_get_macro_value(const char *macro_name, ma_buffer_t **macro_value) { return MA_ERROR_NOT_SUPPORTED;}
#endif

