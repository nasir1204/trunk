#include "ma/ma_common.h"
#include "ma/internal/apps/masvc/ma_app_driver.h"
#include "ma/internal/apps/masvc/ma_masvc.h"
#include <iostream>
#include <tnt/tntnet.h>
#include <tnt/tntconfig.h>
#include <cxxtools/log.h>

MA_CPP(extern "C" {)
void app_thread_func(void *arg) {
    service_data_t *sd = (service_data_t*)arg;
    //tnt::Maptarget ci(std::string("controller/register"));

    // initialize logging - this is optional. If log_init is not called, no
    // logging is done
    log_init("tntnet.properties");

    // instantiate the tnt::Tntnet application class
    tnt::Tntnet app;

    app_driver::get_instance().set_profile_service(sd->profile_service);
    app_driver::get_instance().set_io_service(sd->io_service);
    app_driver::get_instance().set_sensor_service(sd->sensor_service);
    app_driver::get_instance().set_booking_service(sd->booking_service);
    app_driver::get_instance().set_mail_service(sd->mail_service);
    app_driver::get_instance().set_inventory_service(sd->inventory_service);
    app_driver::get_instance().set_search_service(sd->search_service);
    app_driver::get_instance().set_season_service(sd->season_service);
    app_driver::get_instance().set_board_service(sd->board_service);

    app.setAppName("mileaccess.com");

    // set up your mappings

#if 0
    app.mapUrl("^/$", "index");

    app.mapUrl("^/(.+)$", "ma_season_pin_controller")
       .setMethod("POST");

    app.mapUrl("^/(.+)$", "ma_season_pin_controller")
       .setMethod("GET");

#endif
    app.mapUrl("^/order/(.*)$", "ma_create_order_controller")
       .setMethod("^POST$");

    app.mapUrl("^/price/(.*)$", "ma_price_controller")
       .setMethod("^POST$");

    app.mapUrl("^/order/(.*)$", "ma_update_order_controller")
       .setMethod("^PATCH$")
       .setArg("id", "$1");

    app.mapUrl("^/order/(.*)$", "ma_view_order_controller")
       .setMethod("^GET$")
       .setArg("id", "$1");

    app.mapUrl("^/order/(.*)$", "ma_delete_order_controller")
       .setMethod("^DELETE$")
       .setArg("id", "$1");

    app.mapUrl("^/pincode/(.*)$", "ma_view_operation_controller")
       .setMethod("^GET$")
       .setArg("id", "$1");

    app.mapUrl("^/user/(.*)$", "ma_add_user_controller")
       .setMethod("^POST$");

    app.mapUrl("^/user/(.*)$", "ma_update_user_controller")
       .setMethod("^PATCH$")
       .setArg("id", "$1");

    app.mapUrl("^/user/(.*)$", "ma_get_user_controller")
       .setMethod("^GET$")
       .setArg("id", "$1");

    app.mapUrl("^/user/(.*)$", "ma_delete_user_controller")
       .setMethod("^DELETE$")
       .setArg("id", "$1");

    app.mapUrl("^/(.*)/order/(.*)$", "ma_detail_order_controller")
       .setMethod("^GET$")
       .setArg("id", "$1");

    app.mapUrl("^/(.*)/distance/(.*)$", "ma_distance_controller")
       .setMethod("^GET$")
       .setArg("contact", "$1")
       .setArg("id", "$1");
#if 0
    app.mapUrl("^/(.+)$", "ma_register_controller")
       .setMethod("POST");

    app.mapUrl("^/(.+)$", "ma_login_controller")
       .setMethod("POST");

    app.mapUrl("^/(.+)$", "ma_logged_on_controller")
       .setMethod("POST");

    app.mapUrl("^/(.+)$", "ma_password_reset_controller")
       .setMethod("POST");

    app.mapUrl("^/(.+)$", "ma_logged_out_controller");

    app.mapUrl("^/(.+)$", "ma_settings_controller")
       .setMethod("POST");


    app.mapUrl("^/(.+)$", "ma_add_user_controller")
       .setMethod("POST");

    app.mapUrl("^/(.+)$", "ma_remove_user_controller")
       .setMethod("POST");

    app.mapUrl("^/(.+)$", "ma_delete_order_controller")
       .setMethod("POST");

    app.mapUrl("^/(.+)$", "ma_track_order_controller")
       .setMethod("POST");

    app.mapUrl("^/(.+)$", "ma_update_order_controller")
       .setMethod("POST");

    app.mapUrl("^/(.+)$", "ma_view_operation_controller")
       .setMethod("POST");

    app.mapUrl("^/(.+)$", "ma_view_order_controller")
       .setMethod("POST");
    app.mapUrl("^/(.+)$", "ma_update_operation_controller")
       .setMethod("POST");
    app.mapUrl("^/(.+)$", "ma_report_operation_controller")
       .setMethod("POST");
    app.mapUrl("^/(.+)$", "ma_report_order_controller")
       .setMethod("POST");
    app.mapUrl("^/(.+)$", "ma_detail_order_controller")
       .setMethod("POST");
#endif
    app.mapUrl("^/([^.]+)(..+)?", "$1");

    // set more settings
    //tnt::TntConfig::it().minThreads = 3;
    tnt::TntConfig::it().sessionTimeout = 600;

    // configure listener
    app.listen("", 8000);  // note that a empty ip address tells tntnet to listen on all local interfaces

    // run the application
    app.run();
}
MA_CPP(})

app_driver::app_driver(): io_service(NULL), profile_service(NULL), booking_service(NULL), sensor_service(NULL) {}

app_driver::app_driver(const app_driver& o) {}

void *app_driver::operator new(unsigned long size) throw() {return NULL;}

app_driver& app_driver::operator=(const app_driver& o) {return app_driver::get_instance();}

void app_driver::operator delete(void *p) {}

app_driver& app_driver::get_instance() {
    static app_driver instance;

    return instance;
}

app_driver::~app_driver() {}

void app_driver::set_io_service(ma_io_service_t *service) { io_service = service; }

void app_driver::set_booking_service(ma_booking_service_t *service) {booking_service = service;}

void app_driver::set_profile_service(ma_profile_service_t *service) {profile_service = service;}

void app_driver::set_sensor_service(ma_sensor_service_t *service) {sensor_service = service;}

void app_driver::set_mail_service(ma_mail_service_t *service) {mail_service = service;}

void app_driver::set_inventory_service(ma_inventory_service_t *service) {inventory_service = service;}

void app_driver::set_board_service(ma_board_service_t *service) {board_service = service;}

void app_driver::set_search_service(ma_search_service_t *service) {search_service = service;}

void app_driver::set_season_service(ma_season_service_t *service) {season_service = service;}

ma_io_service_t *app_driver::get_io_service() { return io_service;}

ma_booking_service_t *app_driver::get_booking_service() {return booking_service;}

ma_profile_service_t *app_driver::get_profile_service() {return profile_service;}

ma_sensor_service_t *app_driver::get_sensor_service() {return sensor_service;}

ma_mail_service_t *app_driver::get_mail_service() {return mail_service;}

ma_inventory_service_t *app_driver::get_inventory_service() {return inventory_service;}

ma_search_service_t *app_driver::get_search_service() {return search_service;}

ma_season_service_t *app_driver::get_season_service() {return season_service;}

ma_board_service_t *app_driver::get_board_service() {return board_service;}
