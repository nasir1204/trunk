#include "ma/internal/apps/masvc/ma_settings_controller.h"
#include "ma/ma_common.h"
#include "ma/ma_log.h"
#include "ma/internal/utils/profiler/ma_profiler.h"
#include "ma/internal/utils/profiler/ma_profile_info.h"
#include "ma/internal/services/profile/ma_profile_service.h"
#include "ma/internal/utils/inventory/ma_inventory_info.h"
#include "ma/internal/services/mail/ma_mail_service.h"
#include "ma/internal/apps/masvc/ma_app_driver.h"
#include "ma/internal/ma_macros.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_message.h"

#include <tnt/sessionscope.h>
#include <stdio.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "masvc"

extern ma_logger_t *masvc_logger;

static tnt::ComponentFactoryImpl<settings_controller> factory("ma_settings_controller");

unsigned settings_controller::operator() (tnt::HttpRequest& request, tnt::HttpReply& reply, tnt::QueryParams& qparam) {
    if(strcmp("/settings.html", request.getUrl().c_str())) {
        MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "url(%s) is not login.html, so ignoring", request.getUrl().c_str());
        return DECLINED;
    }
    string name(qparam.arg<string>("user"));
    string pwd1(qparam.arg<string>("pwd1"));
    string pwd2(qparam.arg<string>("pwd2"));
    string delete_account(qparam.arg<string>("delete_account"));
    string email(request.getCookie("email"));
  
    MA_LOG(masvc_logger, MA_LOG_SEV_INFO, " HTTP Post request  name(%s) password(%s) delete_account(%s)",  name.c_str(), pwd1.c_str(), delete_account.c_str());

    ma_error_t err = MA_OK;
    ma_profile_info_t *info = NULL;
    ma_profiler_t *profiler = NULL;

    (void)ma_profile_service_get_profiler(app_driver::get_instance().get_profile_service(), &profiler);
    if(!strcmp(delete_account.c_str(), "delete")) {
       if(MA_OK == (err = ma_profiler_get(profiler, email.c_str(), &info))) {
           ma_profile_info_user_type_t type = MA_PROFILE_INFO_USER_TYPE_NORMAL;

           (void)ma_profile_info_get_user_type(info, &type);
           (void)ma_profile_info_release(info);
           if(MA_PROFILE_INFO_USER_TYPE_ADMIN == type) {
               MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "Admin user cannot be deleted");
               reply.setCookie("error", std::string("Admin user cannot be deleted!"), 2);
               reply.setCookie("status", std::string("failed"), 20);
               reply.redirect(string("logged_on.html"));
               return HTTP_OK;
           }
       }

       ma_msgbus_t *msgbus = NULL;

       if(MA_OK == (err = ma_mail_service_get_msgbus(app_driver::get_instance().get_mail_service(), &msgbus))) {
           ma_msgbus_endpoint_t *endpoint = NULL;
           ma_message_t *request = NULL;
           ma_message_t *response = NULL;

           if(MA_OK == (err = ma_msgbus_endpoint_create(msgbus, MA_PROFILE_SERVICE_NAME_STR, NULL, &endpoint)) &&
              MA_OK == (err = ma_message_create(&request))) {
               ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;

               (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);
               (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
               (void)ma_message_set_property(request, MA_PROFILE_SERVICE_MSG_TYPE, MA_PROFILE_SERVICE_MSG_UNREGISTER_TYPE);
               (void)ma_message_set_property(request, MA_PROFILE_ID, email.c_str());
               if(MA_OK == (err = ma_msgbus_send(endpoint, request, &response))) {
                   const char *reply_status = NULL;

                   (void)ma_message_get_property(response, MA_PROFILE_STATUS, &reply_status);
                   if(!strcmp(reply_status, MA_PROFILE_ID_DELETED)) {
                       ma_msgbus_endpoint_t *pendpoint = NULL;
                       ma_message_t *prequest = NULL;
                       ma_message_t *presponse = NULL;

                       MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "sending confirmation mail to email id(%s)", email.c_str());
                       if(MA_OK == (err = ma_msgbus_endpoint_create(msgbus, MA_MAIL_SERVICE_NAME_STR, NULL, &pendpoint)) &&
                          MA_OK == (err = ma_message_create(&prequest))) {
                          ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;
                          const char *sub = "Unregistration mileaccess.com";

                          (void)ma_msgbus_endpoint_setopt(pendpoint, MSGBUSOPT_THREADMODEL, thread_option);
                          (void)ma_msgbus_endpoint_setopt(pendpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                          (void)ma_message_set_property(prequest, MA_MAIL_SERVICE_MSG_TYPE, MA_MAIL_SERVICE_MSG_SEND_TYPE);
                          (void)ma_message_set_property(prequest, MA_MAIL_ID_FROM, MA_EMAIL_ID);
                          (void)ma_message_set_property(prequest, MA_MAIL_ID_TO, email.c_str());      
                          (void)ma_message_set_property(prequest, MA_MAIL_SUBJECT, sub);                
                          char envelope[MA_MAX_BUFFER_LEN+1] = {0};

                          MA_MSC_SELECT(_snprintf, snprintf)(envelope, MA_MAX_BUFFER_LEN, "Dear %s,\n\nWe have successfully unsubscribed your mail id. We expect that you will reconnect with us soon.\n\nHappy Booking!\nRegards,\ncs@mileaccess.com", email.c_str());
                          ma_variant_t *payload = NULL;

                          if(MA_OK == (err = ma_variant_create_from_string(envelope, strlen(envelope), &payload))) {
                              (void)ma_message_set_payload(prequest, payload);
                              if(MA_OK == (err = ma_msgbus_send(pendpoint, prequest, &presponse))) {
                                  (void)ma_message_get_property(presponse, MA_MAIL_STATUS, &reply_status);
                                  if(!strcmp(reply_status, MA_MAIL_SEND_SUCCESS)) {
                                      ma_msgbus_endpoint_t *bendpoint = NULL;
                                      ma_message_t *brequest = NULL;

                                      if(MA_OK == (err = ma_msgbus_endpoint_create(msgbus, MA_BOOKING_SERVICE_NAME_STR, NULL, &bendpoint)) &&
                                         MA_OK == (err = ma_message_create(&brequest))) {
                                          (void)ma_msgbus_endpoint_setopt(bendpoint, MSGBUSOPT_THREADMODEL, thread_option);
                                          (void)ma_msgbus_endpoint_setopt(bendpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                                          (void)ma_message_set_property(brequest, MA_BOOKING_SERVICE_MSG_TYPE, MA_BOOKING_SERVICE_MSG_BOOKING_DELETE_TYPE);
                                          (void)ma_message_set_property(brequest, MA_BOOKING_ID, email.c_str());
                                          if(MA_OK == (err = ma_msgbus_send_and_forget(bendpoint, brequest))) {
                                              ma_msgbus_endpoint_t *iendpoint = NULL;
                                              ma_message_t *irequest = NULL;

                                              if(MA_OK == (err = ma_msgbus_endpoint_create(msgbus, MA_INVENTORY_SERVICE_NAME_STR, NULL, &iendpoint)) &&
                                                 MA_OK == (err = ma_message_create(&irequest))) {
                                                  (void)ma_msgbus_endpoint_setopt(iendpoint, MSGBUSOPT_THREADMODEL, thread_option);
                                                  (void)ma_msgbus_endpoint_setopt(iendpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                                                  (void)ma_message_set_property(irequest, MA_INVENTORY_SERVICE_MSG_TYPE, MA_INVENTORY_SERVICE_MSG_DEREGISTER);
                                                  (void)ma_message_set_property(irequest, MA_INVENTORY_ID, email.c_str());
                                                  if(MA_OK == (err = ma_msgbus_send_and_forget(iendpoint, irequest))) {
                                                      MA_LOG(masvc_logger, MA_LOG_SEV_INFO, "Inventory id(%s) removed successfully", email.c_str());
                                                  }
                                                  (void)ma_msgbus_endpoint_release(iendpoint);
                                                  (void)ma_message_release(irequest);
                                              }

                                              MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "profile id(%s) booking information deleted successfully", email.c_str());
                                              reply.setCookie("status", std::string("success"), 20);
                                              reply.setCookie("error", std::string("Profile and booking information deleted successfully!"), 3);
                                          }
                                          (void)ma_message_release(brequest);
                                          (void)ma_msgbus_endpoint_release(bendpoint);
                                      }
                                  }
                                  (void)ma_message_release(presponse);
                              }
                              (void)ma_variant_release(payload);
                          }
                          (void)ma_message_release(prequest);
                          (void)ma_msgbus_endpoint_release(pendpoint);
                       }
                   } else {
                       reply.setCookie("error", std::string("Profile information does not exist!"), 3);
                       reply.setCookie("status", std::string("failed"), 20);
                   }
                   (void)ma_message_release(response);
               }
               (void)ma_message_release(request);
               (void)ma_msgbus_endpoint_release(endpoint);
           }
       } 
       reply.clearCookie(string("email"));
       reply.clearSession();
       reply.setCookie("error", std::string("Your profile deleted successfully!"), 2);
       reply.redirect(string("index.html"));
    } else {
        if(MA_OK == (err = ma_profiler_get(profiler, email.c_str(), &info))) {
            if(info) {
                ma_variant_t *var = NULL;
                char old_password[MA_MAX_LEN+1] = {0};

                MA_LOG(masvc_logger, MA_LOG_SEV_DEBUG, "Profile id(%s) already exist in DB", email.c_str());
                (void)ma_profile_info_get_password(info, old_password);
                if(!strcmp(old_password, pwd2.c_str())) {
                    (void)ma_profile_info_set_password(info, pwd1.c_str());
                    if(MA_OK == (err = ma_profile_info_convert_to_variant(info, &var))) {
                        if(MA_OK == (err = ma_profiler_add_variant(profiler, email.c_str(), var))) {
                            MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "Profile id(%s) updated successfully", email.c_str());
                            reply.setCookie("error", std::string("Profile information updated successfully!"), 2);
                            reply.setCookie("status", std::string("success"), 20);
                            reply.redirect(string("logged_on.html"));
                        } else {
                            MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "Profile id(%s) failed to update, last error(%d)", email.c_str(), err);
                            reply.setCookie("status", std::string("failed"), 20);
                        }
                        (void)ma_variant_release(var);
                    }
                } else {
                    reply.setCookie("status", std::string("old password mismatch"), 20);
                }
                free(info);
            }
        }
    }
    MA_LOG(masvc_logger, MA_LOG_SEV_DEBUG, " settings handler ends");

    return HTTP_OK;
}

