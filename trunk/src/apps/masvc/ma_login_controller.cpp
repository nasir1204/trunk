#include "ma/internal/apps/masvc/ma_login_controller.h"
#include "ma/ma_common.h"
#include "ma/ma_log.h"
#include "ma/internal/utils/profiler/ma_profiler.h"
#include "ma/internal/utils/profiler/ma_profile_info.h"
#include "ma/internal/services/profile/ma_profile_service.h"
#include "ma/internal/apps/masvc/ma_app_driver.h"
#include "ma/internal/ma_macros.h"

#include <tnt/sessionscope.h>
#include <stdio.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "masvc"

extern ma_logger_t *masvc_logger;

static tnt::ComponentFactoryImpl<login_controller> factory("ma_login_controller");

unsigned login_controller::operator() (tnt::HttpRequest& request, tnt::HttpReply& reply, tnt::QueryParams& qparam) {
    if(strcmp("/login.html", request.getUrl().c_str())) {
        MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "url(%s) is not login.html, so ignoring", request.getUrl().c_str());
        return DECLINED;
    }
    string password(qparam.arg<string>("pwd1"));
    string email(qparam.arg<string>("email"));
  
    request.clearSession();
    MA_LOG(masvc_logger, MA_LOG_SEV_INFO, " HTTP Post request  password(%s) email(%s) ",  password.c_str(), email.c_str());

    ma_error_t err = MA_OK;
    ma_profile_info_t *info = NULL;
    ma_profiler_t *profiler = NULL;

    (void)ma_profile_service_get_profiler(app_driver::get_instance().get_profile_service(), &profiler);
    if(MA_OK == (err = ma_profiler_get(profiler, email.c_str(), &info))) {
        if(info) {
            char passwd[MA_MAX_PATH_LEN] = {0};

            MA_LOG(masvc_logger, MA_LOG_SEV_DEBUG, "Profile id(%s) already exist in DB", email.c_str());
            if(MA_OK == (err = ma_profile_info_get_password(info, passwd))) {
                if(strcmp(password.c_str(), passwd)) {
                    MA_LOG(masvc_logger, MA_LOG_SEV_DEBUG, "Profile id(%s) password did not match against DB password(%s)", email.c_str(), passwd);
                    reply.setCookie("error", std::string("Password did not match!"), 2);
                    reply.redirect(std::string("login.html"));
                } else {
                    char role[MA_MAX_LEN+1]= {0};

                    MA_LOG(masvc_logger, MA_LOG_SEV_DEBUG, "Profile password(%s) for profile id(%s)", passwd, email.c_str());
                    (void)ma_profile_info_get_user_type_str(info, role);
                    reply.setCookie("role", std::string(role), 500);
                    reply.setCookie("email", email, 500);
                    reply.redirect(string("logged_on.html"));
                    MA_LOG(masvc_logger, MA_LOG_SEV_DEBUG, " login handler ends user(%s) ", request.getUsername().c_str());
                }
            }
            free(info);
        }
    } else {
        MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "user email id(%s) does not exist", email.c_str());
        reply.setCookie("error", std::string("User does not exist, please register"), 2);
        reply.redirect(string("index.html"));
    }
      
    MA_LOG(masvc_logger, MA_LOG_SEV_DEBUG, " login handler ends");

    return HTTP_OK;
}

