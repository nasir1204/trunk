#include "ma/internal/apps/masvc/ma_distance_controller.h"
#include "ma/internal/defs/ma_booking_defs.h"
#include "ma/internal/defs/ma_general_defs.h"
#include "ma/internal/utils/booker/ma_booking_info.h"
#include "ma/internal/utils/booker/ma_booker.h"
#include "ma/ma_common.h"
#include "ma/ma_log.h"
#include "ma/internal/utils/profiler/ma_profiler.h"
#include "ma/internal/utils/profiler/ma_profile_info.h"
#include "ma/internal/services/profile/ma_profile_service.h"
#include "ma/internal/apps/masvc/ma_app_driver.h"
#include "ma/internal/ma_macros.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_message.h"
#include "ma/internal/utils/threading/ma_atomic.h"
#include "ma/internal/utils/uuid/ma_uuid.h"

#include <tnt/sessionscope.h>
#include <stdio.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "masvc"

extern ma_logger_t *masvc_logger;

static tnt::ComponentFactoryImpl<distance_controller> factory("ma_distance_controller");

ma_error_t distance_controller::process(const char *contact, char *distance) {
    if(!contact || !distance)return MA_ERROR_INVALIDARG;
    ma_error_t err = MA_OK;
    ma_msgbus_t *msgbus = NULL;

    if(MA_OK == (err = ma_mail_service_get_msgbus(app_driver::get_instance().get_mail_service(), &msgbus))) {
        ma_msgbus_endpoint_t *endpoint = NULL;
        ma_message_t *request = NULL;
        ma_message_t *response = NULL;

        if(MA_OK == (err = ma_msgbus_endpoint_create(msgbus, MA_CONSIGN_SERVICE_NAME_STR, NULL, &endpoint)) &&
            MA_OK == (err = ma_message_create(&request))) {
            ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;

            (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);
            (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
            (void)ma_message_set_property(request, MA_CONSIGN_SERVICE_MSG_TYPE, MA_CONSIGN_SERVICE_MSG_GET_RAN_DISTANCE_TYPE);
            (void)ma_message_set_property(request, MA_CONSIGN_ID, contact);
            if(MA_OK == (err = ma_msgbus_send(endpoint, request, &response))) {
                const char *status = NULL;
                if(MA_OK == (err = ma_message_get_property(response, MA_CONSIGN_STATUS, &status))) {
                    if(!strcmp(status, MA_CONSIGN_ID_EXIST)) {
                        (void)ma_message_get_property(response, MA_CONSIGN_RAN_DISTANCE, &status);
                        strcpy(distance, status);
                        MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "consign contact(%s) distance(%s) successfully", contact, distance);
                    } else {
                        strncpy(distance, "0", MA_MAX_LEN);
                        MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "consign contact(%s) distance calculation failed, last error(%d)", contact, err);
                    }
                }
                (void)ma_message_release(response);
            }

            (void)ma_msgbus_endpoint_release(endpoint);
            (void)ma_message_release(request);
        }
    }
    return err;
}

unsigned distance_controller::operator() (tnt::HttpRequest& request, tnt::HttpReply& reply, tnt::QueryParams& qparam) {
    const char *contact = request.getArg("contact").c_str();
    const char *id = request.getArg("id").c_str();
    ma_error_t err = MA_OK;
    ma_json_t *jout = NULL;
    char distance[MA_MAX_LEN+1] = {0};

    MA_LOG(masvc_logger, MA_LOG_SEV_INFO, "processing distance calculator operation (%s)", request.getBody().c_str());
    if(MA_OK == (err = process(contact, distance)) && MA_OK == ma_json_create(&jout)) {
        (void)ma_json_add_element(jout, MA_CONSIGN_RAN_DISTANCE, distance);
        ma_bytebuffer_t *buffer = NULL;

        ma_bytebuffer_create(MA_MAX_BUFFER_LEN, &buffer);
        if(MA_OK == ma_json_get_data(jout, buffer)) {
            reply.setContentType("Content-Type: application/json");
            reply.out() << (const char*)ma_bytebuffer_get_bytes(buffer);
        }
        (void)ma_json_release(jout);
    }

    return HTTP_OK;
}

