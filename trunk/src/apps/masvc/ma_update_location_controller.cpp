#include "ma/internal/apps/masvc/ma_update_location_controller.h"
#include "ma/internal/defs/ma_booking_defs.h"
#include "ma/internal/defs/ma_general_defs.h"
#include "ma/internal/utils/booker/ma_booking_info.h"
#include "ma/internal/utils/booker/ma_booker.h"
#include "ma/internal/utils/location/ma_location_info.h"
#include "ma/internal/utils/location/ma_location.h"
#include "ma/ma_common.h"
#include "ma/ma_log.h"
#include "ma/internal/utils/profiler/ma_profiler.h"
#include "ma/internal/utils/profiler/ma_profile_info.h"
#include "ma/internal/services/profile/ma_profile_service.h"
#include "ma/internal/services/mail/ma_mail_service.h"
#include "ma/internal/apps/masvc/ma_app_driver.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/ma_strdef.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_message.h"
#include "ma/internal/utils/threading/ma_atomic.h"
#include "ma/internal/utils/uuid/ma_uuid.h"
#include "ma/internal/utils/context/ma_context.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"

#include <tnt/sessionscope.h>
#include <stdio.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "masvc"

extern ma_logger_t *masvc_logger;

static tnt::ComponentFactoryImpl<update_location_controller> factory("ma_update_location_controller");
static ma_context_t *context = NULL;

static ma_error_t add_location_entry(ma_location_t *location, const char *id, const char *details) {
    if(location && id && details) {
        ma_error_t err = MA_OK;
        ma_task_time_t today = {0};
        char today_str[MA_MAX_LEN+1]= {0};
        ma_location_info_t *info = NULL;

        get_localtime(&today);
        today.hour = today.minute = today.secs = 0;
        MA_MSC_SELECT(_snprintf, snprintf)(today_str, MA_MAX_LEN, "%hu-%hu-%hu", today.month, today.day, today.year);
        if(MA_OK == (err = ma_location_info_create(&info))) {
            (void)ma_location_info_set_date(info, today);
            (void)ma_location_info_set_email_id(info, id);
            get_localtime(&today);
            (void)ma_location_info_set_entry_date(info, today);
            ma_strtime(today, today_str);
            (void)ma_location_info_set_entry(info, today_str);
            (void)ma_location_info_set_details(info, details);
            if(MA_OK == (err = ma_location_add(location, info))) {
                //update lat long details against profile
                ma_profile_info_t *profile = NULL;

                if(MA_OK == (err = ma_profiler_get(MA_CONTEXT_GET_PROFILER(context), id, &profile))) {
                   (void)ma_profile_info_set_last_seen(profile, today);
                   err = ma_profiler_add(MA_CONTEXT_GET_PROFILER(context), profile);
                   (void)ma_profile_info_release(profile);
                }
            }
            (void)ma_location_info_release(info);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

unsigned update_location_controller::operator() (tnt::HttpRequest& request, tnt::HttpReply& reply, tnt::QueryParams& qparam) {
    if(strcmp("/update_location.html", request.getUrl().c_str()) &&  strcmp("/update_location", request.getUrl().c_str())) {
        MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "url(%s) is not update_location.html, so ignoring", request.getUrl().c_str());
        return DECLINED;
    }
    string location(qparam.arg<string>("location"));
    string email(request.getCookie("email"));
    replace(location.begin(), location.end(), MA_COMMA, MA_SPACE);

    if(email.empty()) {
        reply.setCookie("error", std::string("Session is already expired. Please login again!"), 2);
        reply.redirect(string("index.html"));
        return HTTP_OK;
    }
    MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "url(%s) is update_location.html for id(%s) location(%s)", request.getUrl().c_str(), email.c_str(), location.c_str());

    ma_error_t err = MA_OK;

    (void)ma_booking_service_get_context(app_driver::get_instance().get_booking_service(), &context);
    if(MA_OK == (err = add_location_entry(MA_CONTEXT_GET_LOCATION(context), email.c_str(), location.c_str())))
        MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "Update lat-long location details for profile id(%s)", email.c_str());
    else
        MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "Failed to update lat-long location details for profile id(%s), error(%d)", email.c_str(), err);
    MA_LOG(masvc_logger, MA_LOG_SEV_DEBUG, " update location handler ends");
    reply.redirect(std::string("logged_on.html"));

    return HTTP_OK;
}

