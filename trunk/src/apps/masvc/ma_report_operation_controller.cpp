#include "ma/internal/apps/masvc/ma_report_operation_controller.h"
#include "ma/internal/defs/ma_booking_defs.h"
#include "ma/internal/utils/booker/ma_booking_info.h"
#include "ma/internal/utils/booker/ma_booker.h"
#include "ma/ma_common.h"
#include "ma/ma_log.h"
#include "ma/internal/utils/profiler/ma_profiler.h"
#include "ma/internal/utils/profiler/ma_profile_info.h"
#include "ma/internal/services/profile/ma_profile_service.h"
#include "ma/internal/services/mail/ma_mail_service.h"
#include "ma/internal/apps/masvc/ma_app_driver.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/ma_strdef.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_message.h"
#include "ma/internal/utils/threading/ma_atomic.h"
#include "ma/internal/utils/uuid/ma_uuid.h"

#include <tnt/sessionscope.h>
#include <stdio.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "masvc"

extern ma_logger_t *masvc_logger;

static tnt::ComponentFactoryImpl<report_operation_controller> factory("ma_report_operation_controller");

unsigned report_operation_controller::operator() (tnt::HttpRequest& request, tnt::HttpReply& reply, tnt::QueryParams& qparam) {
   if(strcmp("/report_operation.html", request.getUrl().c_str()) &&  strcmp("/report_operation", request.getUrl().c_str())) {
       MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "url(%s) is not report_operation.html, so ignoring", request.getUrl().c_str());
       return DECLINED;
   }
   string from_date(qparam.arg<string>("from_date"));
   string email_id(qparam.arg<string>("email_id"));
   string to_date(qparam.arg<string>("to_date"));
   string email(request.getCookie("email"));

   MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "url(%s) is report_operation.html executing profile(%s)", request.getUrl().c_str(), email.c_str());
   if(email.empty()) {
       reply.setCookie("error", std::string("Session is already expired. Please login again!"), 2);
       reply.redirect(string("index.html"));
       return HTTP_OK;
   }
   MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "HTTP Post report operation request email_id(%s) from_date(%s) to_date(%s)", email_id.c_str(), from_date.c_str(), to_date.c_str());
  
   reply.setCookie("from_date", from_date, 5);
   reply.setCookie("to_date", to_date, 5);
   reply.setCookie("email_id", email_id, 5);
   reply.redirect(string("report_operation_detail.html"));

   return HTTP_OK;
}

