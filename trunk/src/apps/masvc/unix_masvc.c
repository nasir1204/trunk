#include "ma/ma_common.h"

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "masvc"

#include "ma/ma_log.h"
#include "ma/logger/ma_file_logger.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/defs/ma_io_defs.h"
#include "ma/internal/utils/conf/ma_conf_log.h"
#include "ma/internal/services/io/ma_io_service.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/utils/platform/ma_process.h"
#include "ma/internal/services/sensor/ma_sensor_service.h"
#include "ma/sensor/ma_sensor.h"
#include "ma/sensor/ma_sensor_msg.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/utils/conf/ma_conf.h"
#include "ma/internal/apps/masvc/ma_masvc.h"

#include <stdio.h>
#include <uv.h>
#include <pthread.h>
#include <unistd.h>

/* To be changed the below macros */
#define MASVC_LOG_BASE_DIR      "/var/McAfee/agent/logs/"
#define MASVC_LOG_FILE_NAME     "masvc"

#define START_PARAM             "start"
#define SELF_START_PARAM        "self_start"
#define STOP_PARAM              "stop"
#define STATUS_PARAM            "status"

static service_data_t sd;
pthread_t app_tid = 0;
static pthread_attr_t attr;
MA_CPP(extern "C" {)
void* app_thread_func(void *arg);
MA_CPP(})

ma_logger_t *masvc_logger;
#ifdef MACX

#include <assert.h>
#include <errno.h>

#include <CoreFoundation/CoreFoundation.h>
#include <SystemConfiguration/SystemConfiguration.h>

static pthread_t sensor_tid;
static ma_sensor_service_t *gsensor_service_p;
static ma_bool_t link_status;

static ma_error_t post_network_sensor_msg(ma_bool_t status) {
    if(gsensor_service_p) {
        ma_error_t err = MA_OK;
        ma_network_sensor_msg_t *network_msg = NULL;

        if(MA_OK == (err = ma_network_sensor_msg_create(&network_msg, 1))) {
            ma_sensor_msg_t *msg = NULL;

            if(MA_OK == (err = ma_sensor_msg_create_from_network_sensor_msg(network_msg, &msg))) {
                (void)ma_network_sensor_msg_set_event_type(network_msg, MA_NETWORK_SENSOR_CONNECTIVITY_STATUS_CHANGE_EVENT);
                if(status) {
                    (void)ma_network_sensor_msg_set_connectivity_status(network_msg, MA_NETWORK_CONNECTIVITY_STATUS_CONNECTED);
                } else {
                    (void)ma_network_sensor_msg_set_connectivity_status(network_msg, MA_NETWORK_CONNECTIVITY_STATUS_DISCONNECTED);
                }
                (void)ma_sensor_service_post_sensor_msg(gsensor_service_p, msg);
                (void)ma_sensor_msg_release(msg);
            }
            (void)ma_network_sensor_msg_release(network_msg);
        }
    }
    return MA_ERROR_INVALIDARG;
}

static ma_bool_t get_link_status(SCDynamicStoreRef store, CFArrayRef changedKeys, void  *info) {
    CFIndex count = CFArrayGetCount(changedKeys);
    CFIndex i = 0;
    ma_bool_t status = MA_FALSE;

    for(i=0; i<count; ++i) {
        CFStringRef key = (CFStringRef)CFArrayGetValueAtIndex(changedKeys, i);
        CFDictionaryRef newValue = (CFDictionaryRef)SCDynamicStoreCopyValue(store, key);
        if(newValue) {
            CFRelease(newValue);
            status = MA_TRUE;
            break;
        }
    }
    return  status;
}


static void scCallback(SCDynamicStoreRef store, CFArrayRef changedKeys, void  *info) {
    CFIndex count = CFArrayGetCount(changedKeys);
    CFIndex i = 0;
    ma_bool_t status = MA_FALSE;
	
    //we have to again fetch both keys and do a comparison
    CFArrayRef refKeys = SCDynamicStoreCopyKeyList(store, CFSTR("State:/Network/Interface/en.*/IPv.*"));
    count = CFArrayGetCount(refKeys);

    for(i=0; i<count; ++i) {
        CFStringRef key = (CFStringRef)CFArrayGetValueAtIndex(refKeys, i);
        CFDictionaryRef newValue = (CFDictionaryRef)SCDynamicStoreCopyValue(store, key);
        if(newValue) {
            CFRelease(newValue);
            status = MA_TRUE;
            break;
        }
    }
    if(refKeys) CFRelease(refKeys);
    //MA_LOG(sd->logger, MA_LOG_SEV_DEBUG, "network ip status changed (%s) ", status ? "MA_TRUE" : "MA_FALSE");
    if(link_status != status) {
        link_status = status;
        post_network_sensor_msg(status);
    }
}


static void* sense_event_cb(void *ptr) {
    if(ptr && gsensor_service_p) {
        service_data_t *sd = (service_data_t*)ptr;
        SCDynamicStoreRef dynStore;
        SCDynamicStoreContext context = {0, NULL, NULL, NULL, NULL};

        context.info = (void*)sd;
        dynStore = SCDynamicStoreCreate(kCFAllocatorDefault,
                                  CFBundleGetIdentifier(CFBundleGetMainBundle()),
                                  scCallback,
                                  &context);

        CFArrayRef refKeys = SCDynamicStoreCopyKeyList(dynStore, CFSTR("State:/Network/Interface/en.*/IPv.*"));

        const CFStringRef keys[2] = {
            CFSTR("State:/Network/Interface/en.*/IPv.*") };
        
        CFArrayRef watchedKeys = CFArrayCreate(kCFAllocatorDefault,
                                         (const void **)keys,
                                         1,
                                         &kCFTypeArrayCallBacks);

        link_status = get_link_status(dynStore, refKeys, NULL);

	if(refKeys) CFRelease(refKeys);

        if (!SCDynamicStoreSetNotificationKeys(dynStore,
                                         NULL,
                                         watchedKeys)) {
            CFRelease(watchedKeys);
            fprintf(stderr, "SCDynamicStoreSetNotificationKeys() failed: %s", SCErrorString(SCError()));
            CFRelease(dynStore);
            dynStore = NULL;
            return ptr;
        }

        CFRelease(watchedKeys);
        CFRunLoopSourceRef rlSrc = SCDynamicStoreCreateRunLoopSource(kCFAllocatorDefault, dynStore, 0);
        CFRunLoopAddSource(CFRunLoopGetCurrent(), rlSrc, kCFRunLoopDefaultMode);
        CFRelease(rlSrc);    
        CFRunLoopRun ( );    
        return ptr;
    }
    return NULL;
}
#endif

static void usage() {
    fprintf(stderr, "usage: masvc start/stop/status \n");
}

static void ma_process_stop_handler(void *data) {
    service_data_t *sd = (service_data_t *) data;
    ma_error_t rc = MA_OK;

    if(MA_OK == (rc = ma_io_service_stop(sd->io_service))) {
        MA_LOG(sd->logger, MA_LOG_SEV_DEBUG, "io service stop succeeded") ;
    }
    else {
        MA_LOG(sd->logger, MA_LOG_SEV_ERROR, "io service stop failed(%d)", rc) ;
    }
#if 0
    if(app_tid) {
        MA_LOG(sd->logger, MA_LOG_SEV_INFO, "app thread exiting");
        if(!pthread_kill(app_tid, SIGINT))
           MA_LOG(sd->logger, MA_LOG_SEV_INFO, "app thread stopped successfully");
        else
           MA_LOG(sd->logger, MA_LOG_SEV_ERROR, "app thread stop failed");
    }
#endif
}

static ma_error_t masvc_start() {
    ma_error_t rc = MA_OK ;
    ma_temp_buffer_t logs_path = MA_TEMP_BUFFER_INIT;
    if(MA_OK == (rc = ma_conf_get_logs_path(&logs_path))) {
        ma_temp_buffer_t log_file_name = MA_TEMP_BUFFER_INIT;
        if(MA_OK == (rc = ma_conf_get_agent_log_file_name(&log_file_name))) {
            if(MA_OK == (rc =  ma_file_logger_create((const char *)ma_temp_buffer_get(&logs_path),(const char *)ma_temp_buffer_get(&log_file_name),".log", (ma_file_logger_t **) &sd.logger))) {
                masvc_logger = sd.logger;
                (void)ma_generic_log_filter_create("*.Info", &sd.filter);
                (void)ma_logger_add_filter(sd.logger, sd.filter);
                if(MA_OK == (rc = ma_io_service_create(MA_IO_SERVICE_NAME_STR, sd.logger, &sd.io_service))) {
	            ma_event_loop_t *event_loop = NULL;
                    uv_thread_t app_id = (uv_thread_t)0; /* app thread id */ 

                    ma_io_service_get_service(sd.io_service, MA_SENSOR_SERVICE_NAME_STR, (ma_service_t **)(&sd.sensor_service));
                    ma_io_service_get_service(sd.io_service, MA_PROFILE_SERVICE_NAME_STR, (ma_service_t **)(&sd.profile_service));
                    ma_io_service_get_service(sd.io_service, MA_BOOKING_SERVICE_NAME_STR, (ma_service_t **)(&sd.booking_service));
                    ma_io_service_get_service(sd.io_service, MA_MAIL_SERVICE_NAME_STR, (ma_service_t **)(&sd.mail_service));
                    ma_io_service_get_service(sd.io_service, MA_INVENTORY_SERVICE_NAME_STR, (ma_service_t **)(&sd.inventory_service));
                    ma_io_service_get_service(sd.io_service, MA_SEASON_SERVICE_NAME_STR, (ma_service_t **)(&sd.season_service));
                    ma_io_service_get_service(sd.io_service, MA_SEARCH_SERVICE_NAME_STR, (ma_service_t **)(&sd.search_service));
                    ma_io_service_get_service(sd.io_service, MA_BOARD_SERVICE_NAME_STR, (ma_service_t **)(&sd.board_service));
                    (void)pthread_attr_init(&attr);
                    (void)pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_DETACHED);
#ifdef MACX
                    gsensor_service_p = sd.sensor_service;
                    if((rc = (ma_error_t)pthread_create(&sensor_tid, &attr, &sense_event_cb, (void*)&sd)))
                        MA_LOG(sd.logger, MA_LOG_SEV_ERROR, "failed to create sensor thread %d", rc);
                    else
                        MA_LOG(sd.logger, MA_LOG_SEV_DEBUG, "sensor thread created successfully");
#endif
                    /* app thread code */
                    {
                        if((rc = (ma_error_t)pthread_create(&app_tid, &attr, &app_thread_func, (void*)&sd)))
                            MA_LOG(sd.logger, MA_LOG_SEV_ERROR, "failed to create app thread %d", rc);
                        else
                            MA_LOG(sd.logger, MA_LOG_SEV_INFO, "app thread created successfully"); 
                    }
                    /* app thread ends */

                    MA_LOG(sd.logger, MA_LOG_SEV_DEBUG, "io service create succeeded");
                    if((MA_OK == (rc = ma_io_service_get_event_loop(sd.io_service, &event_loop))) && event_loop) {
                        if(!ma_process_register_stop_handler(event_loop, ma_process_stop_handler, &sd)) {
                            if(MA_OK != (rc = ma_io_service_run(sd.io_service)))
                                MA_LOG(sd.logger, MA_LOG_SEV_ERROR, "io service run failed(%d)", rc) ;
                     
	                }
                        else
                            MA_LOG(sd.logger, MA_LOG_SEV_ERROR, "Process stop handler registration failed") ;
                    }
                    (void) ma_io_service_release(sd.io_service);
                }
                else {
                    MA_LOG(sd.logger, MA_LOG_SEV_ERROR, "io service create Failed(%d)", rc) ;
                }
                (void) ma_file_logger_release((ma_file_logger_t *)sd.logger); sd.logger = NULL;
            }
            ma_temp_buffer_uninit(&log_file_name);
        }
    }
    ma_temp_buffer_uninit(&logs_path);
    return rc ;
}

void masvc_stop() {
	ma_error_t rc = MA_OK ;
	if(sd.io_service) {
		if(MA_OK == (rc = ma_io_service_stop(sd.io_service))) {
			MA_LOG(sd.logger, MA_LOG_SEV_DEBUG, "io service stop succeeded") ;
#ifdef MACX
                        if(gsensor_service_p) {
                            gsensor_service_p = NULL;
                        }
#endif
			if(MA_OK == (rc = ma_io_service_release(sd.io_service))) {
				MA_LOG(sd.logger, MA_LOG_SEV_DEBUG, "io service release succeeded") ;
			}
			else
				MA_LOG(sd.logger, MA_LOG_SEV_ERROR, "io service release failed(%d)", rc) ;
		}
		else {
			MA_LOG(sd.logger, MA_LOG_SEV_ERROR, "io service stop failed(%d)", rc) ;
		}
	}
}

int main(int argc, char **argv) {
#ifdef HPUX_ALIGNMENT_TRAP_HANDLER
	allow_unaligned_data_access();
#endif

    ma_int32_t status = -1;

    if( argc != 2 ) {
        usage();
        return status;
    }

	if(!strncmp(argv[1], START_PARAM, strlen(START_PARAM)))  {
        status = ma_process_start();
        if(0 == status)
			fprintf(stderr, "%s Process started \n", argv[0]);
        else if(1 == status)
			fprintf(stderr, "%s Process is already running \n", argv[0]);
        else
            fprintf(stderr, "Failed to start process %s \n", argv[0]);
	}
	else if(!strncmp(argv[1], SELF_START_PARAM, strlen(SELF_START_PARAM))) {
        ma_error_t rc = MA_OK;
        if(MA_OK != (rc = masvc_start())) {
			fprintf(stderr, "masvc process start failed (%d)", rc) ;
		}
        status = (MA_OK == rc) ? 0 : -1;
    }
	else if(!strncmp(argv[1], STOP_PARAM, strlen(STOP_PARAM))) {
        status = ma_process_stop();
        if(0 == status)
			fprintf(stderr, "%s Process stopped \n", argv[0]);
		else if(1 == status)
			fprintf(stderr, "%s Process is not running \n", argv[0]);
        else
            fprintf(stderr, "Failed to stop process %s \n", argv[0]);
    }
	else if(!strncmp(argv[1], STATUS_PARAM, strlen(STATUS_PARAM))) {
        status = ma_process_status();
        if(0 == status)
			fprintf(stderr, "%s Process is running \n", argv[0]);
		else if(1 == status)
			fprintf(stderr, "%s Process is not running \n", argv[0]);
        else
            fprintf(stderr, "Failed to get process %s status \n", argv[0]);
    }
    else {
        fprintf(stderr, "Invalid argument(%s)\n", argv[1]);
        usage();
    }

    return status ;
}


