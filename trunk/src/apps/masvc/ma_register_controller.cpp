#include "ma/internal/apps/masvc/ma_register_controller.h"
#include "ma/ma_common.h"
#include "ma/ma_log.h"
#include "ma/internal/utils/profiler/ma_profiler.h"
#include "ma/internal/utils/profiler/ma_profile_info.h"
#include "ma/internal/services/profile/ma_profile_service.h"
#include "ma/internal/services/mail/ma_mail_service.h"
#include "ma/internal/defs/ma_mail_defs.h"
#include "ma/internal/utils/inventory/ma_inventory_info.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/apps/masvc/ma_app_driver.h"
#include "ma/internal/defs/ma_general_defs.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_message.h"


#include <assert.h>
#include <stdio.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "masvc"

extern ma_logger_t *masvc_logger;

static tnt::ComponentFactoryImpl<register_controller> factory("ma_register_controller");


unsigned register_controller::operator() (tnt::HttpRequest& request, tnt::HttpReply& reply, tnt::QueryParams& qparam) {
    MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "query string(%s) url(%s) pathinfo(%s) method(%s)", request.getQuery().c_str(), request.getUrl().c_str(), request.getPathInfo().c_str(), request.getMethod().c_str());

    if(strcmp("/register.html", request.getUrl().c_str())) {
        MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "url(%s) is not register.html, so ignoring", request.getUrl().c_str());
        return DECLINED;
    } else {
        MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "url(%s) data(%s)", request.getUrl().c_str(), request.getBody().c_str());
    }
    string username(qparam.arg<string>("username"));
    string password(qparam.arg<string>("pwd1"));
    string email(qparam.arg<string>("email"));
    string contact(qparam.arg<string>("contact"));
  
    MA_LOG(masvc_logger, MA_LOG_SEV_INFO, " HTTP Post request username(%s) password(%s) email(%s) contact(%s)", username.c_str(), password.c_str(), email.c_str(), contact.c_str());

    ma_error_t err = MA_OK;
    ma_profile_info_t *info = NULL;
    ma_profiler_t *profiler = NULL;

    (void)ma_profile_service_get_profiler(app_driver::get_instance().get_profile_service(), &profiler);
    if(MA_OK == (err = ma_profiler_get(profiler, email.c_str(), &info))) {
        if(info) {
            MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "Profile id(%s) already exist in DB", email.c_str());
            reply.setCookie("error", std::string("Profile already exist, please go ahead and login!"), 3);
            //reply.redirect(std::string("index.html"));
            (void)ma_profile_info_release(info);
            return DECLINED;
        }
    }
    MA_LOG(masvc_logger, MA_LOG_SEV_INFO, "Profile id(%s) does not exist in DB, creating...", email.c_str());
    if(MA_OK == (err = ma_profile_info_create(&info, MA_PROFILE_INFO_USER_TYPE_NORMAL))) {
        (void)ma_profile_info_set_user(info, username.c_str());
        (void)ma_profile_info_set_email_id(info, email.c_str());
        (void)ma_profile_info_set_password(info, password.c_str());
        (void)ma_profile_info_set_contact(info, contact.c_str());

        ma_variant_t *info_var = NULL;

        if(MA_OK == (err = ma_profile_info_convert_to_variant(info, &info_var))) {
            ma_msgbus_t *msgbus = NULL;
            (void)ma_profiler_set_logger(masvc_logger);


            if(MA_OK == (err = ma_mail_service_get_msgbus(app_driver::get_instance().get_mail_service(), &msgbus))) {
                ma_msgbus_endpoint_t *endpoint = NULL;
                ma_message_t *request = NULL;
                ma_message_t *response = NULL;
 
                if(MA_OK == (err = ma_msgbus_endpoint_create(msgbus, MA_MAIL_SERVICE_NAME_STR, NULL, &endpoint)) &&
                   MA_OK == (err = ma_message_create(&request))) {
                    ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;
                    const char *welcome_sub = "Welcome to mileaccess";

                    (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);
                    (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                    if(MA_OK == (err = ma_message_set_property(request, MA_MAIL_SERVICE_MSG_TYPE, MA_MAIL_SERVICE_MSG_SEND_TYPE)) &&
                       MA_OK == (err = ma_message_set_property(request, MA_MAIL_ID_FROM, MA_EMAIL_ID)) &&
                       MA_OK == (err = ma_message_set_property(request, MA_MAIL_ID_TO, email.c_str())) &&
                       MA_OK == (err = ma_message_set_property(request, MA_MAIL_SUBJECT, welcome_sub))
                      ) {
                          char envelope[MA_MAX_BUFFER_LEN+1]={0};
                          ma_variant_t *payload = NULL;

                         MA_MSC_SELECT(_snprintf, snprintf)(envelope, MA_MAX_BUFFER_LEN, "Dear %s,\n\nWelcome to Mileaccess Technologies Pvt Limited (online instant delivery service).\nWe deliver all from the doorstep to desired location at cheap shipping price and we ensure shipping to be instant and same day delivery in and around the city.\n\nHappy Ordering!!\n\nRegards,\nCustomer Support", username.c_str());
                         if(MA_OK == (err = ma_variant_create_from_string(envelope, strlen(envelope), &payload))) {
                             (void)ma_message_set_payload(request, payload);
                             if(MA_OK == (err = ma_msgbus_send(endpoint, request, &response))) {
                                 const char *reply_status = NULL;

                                 if(MA_OK == (err = ma_message_get_property(response, MA_MAIL_STATUS, &reply_status))) {
                                     if(!strcmp(reply_status, MA_MAIL_SEND_SUCCESS)) {
                                         ma_msgbus_endpoint_t *pendpoint = NULL;
                                         ma_message_t *prequest = NULL;
                                         ma_message_t *presponse = NULL;

                                         MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "mail id(%s) exist", email.c_str());
                                         if(MA_OK == (err = ma_msgbus_endpoint_create(msgbus, MA_PROFILE_SERVICE_NAME_STR, NULL, &pendpoint)) &&
                                            MA_OK == (err = ma_message_create(&prequest))) {
                                             (void)ma_msgbus_endpoint_setopt(pendpoint, MSGBUSOPT_THREADMODEL, thread_option);
                                             (void)ma_msgbus_endpoint_setopt(pendpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                                             (void)ma_message_set_property(prequest, MA_PROFILE_SERVICE_MSG_TYPE, MA_PROFILE_SERVICE_MSG_REGISTER_TYPE);
                                             (void)ma_message_set_property(prequest, MA_PROFILE_ID, email.c_str());
                                             if(MA_OK == (err = ma_message_set_payload(prequest, info_var))) {
                                                 if(MA_OK == (err = ma_msgbus_send(pendpoint, prequest, &presponse))) {
                                                     const char *status = NULL;

                                                     (void)ma_message_get_property(presponse, MA_PROFILE_STATUS, &status);
                                                     if(!strcmp(status, MA_PROFILE_ID_WRITTEN)) {
                                                         ma_msgbus_endpoint_t *iendpoint = NULL;
                                                         ma_message_t *irequest = NULL;
                                                         ma_message_t *iresponse = NULL;

                                                         if(MA_OK == (err = ma_msgbus_endpoint_create(msgbus, MA_INVENTORY_SERVICE_NAME_STR, NULL, &iendpoint)) &&
                                                            MA_OK == (err = ma_message_create(&irequest))) {
                                                             ma_inventory_info_t *inventory_info = NULL;

                                                             (void)ma_msgbus_endpoint_setopt(iendpoint, MSGBUSOPT_THREADMODEL, thread_option);
                                                             (void)ma_msgbus_endpoint_setopt(iendpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                                                             (void)ma_message_set_property(irequest, MA_INVENTORY_SERVICE_MSG_TYPE, MA_INVENTORY_SERVICE_MSG_REGISTER);
                                                             (void)ma_message_set_property(irequest, MA_INVENTORY_ID, email.c_str());
                                                             if(MA_OK == (err = ma_inventory_info_create(&inventory_info))) {
                                                                 ma_task_time_t date = {0};
                                                                 ma_variant_t *ivar = NULL;

                                                                 get_localtime(&date);
                                                                 (void)ma_inventory_info_set_date(inventory_info, date);
                                                                 (void)ma_inventory_info_set_email_id(inventory_info, email.c_str());
                                                                 if(MA_OK == (err = ma_inventory_info_convert_to_variant(inventory_info, &ivar))) {
                                                                     (void)ma_message_set_payload(irequest, ivar);
                                                                     if(MA_OK == (err = ma_msgbus_send(iendpoint, irequest, &iresponse))) {
                                                                         const char *inventory_status = NULL;

                                                                         (void)ma_message_get_property(iresponse, MA_INVENTORY_STATUS, &inventory_status);
                                                                         if(!strcmp(inventory_status, MA_INVENTORY_ID_WRITTEN)) {
                                                                             MA_LOG(masvc_logger, MA_LOG_SEV_INFO, "Inventory id(%s) information written succesfully", email.c_str());
                                                                         } else
                                                                             MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "Inventory id(%s) information write failed", email.c_str());
                                                                         (void)ma_message_release(iresponse);
                                                                     }
                                                                     (void)ma_variant_release(ivar);
                                                                 }
                                                                 (void)ma_inventory_info_release(inventory_info);
                                                             }
                                                             (void)ma_msgbus_endpoint_release(iendpoint);
                                                             (void)ma_message_release(irequest);
                                                         }
                                                         MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "profile info written successfully into profile DB");
                                                         reply.setCookie("error", std::string("Profile information saved successfully, please go ahead and login!"), 3);
                                                     } else {
                                                         MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "profile info write failed");
                                                         reply.setCookie("error", std::string("Profile information save unsuccessfully, please go ahead and register again!"), 3);
                                                     }
                                                     (void)ma_message_release(presponse);
                                                 } else
                                                     MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "profile registration failed, last error(%d)", err);
                                                 (void)ma_variant_release(info_var);
                                             }
                                             (void)ma_message_release(prequest);
                                             (void)ma_msgbus_endpoint_release(pendpoint);
                                         }
                                     } else
                                        reply.setCookie("error", std::string("Mail ID does not exist, please provide valid email Id!"), 3);
                                 } 
                                 (void)ma_message_release(response);
                             }
                             (void)ma_variant_release(payload);
                         }
                    }
                    (void)ma_message_release(request);
                    (void)ma_msgbus_endpoint_release(endpoint);
                }
            }
            (void)ma_variant_release(info_var);
        } else
            MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "profile info variant conversion failed, last error(%d)", err);
        (void)ma_profile_info_release(info);
        /* setting http reply */
        //reply.redirect(std::string("index.html"));
    } else
        MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "profile info creation failed, last error(%d)", err);

    return HTTP_OK;
}

