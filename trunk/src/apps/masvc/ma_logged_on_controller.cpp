#include "ma/internal/apps/masvc/ma_logged_on_controller.h"
#include "ma/ma_common.h"
#include "ma/ma_log.h"
#include "ma/internal/utils/profiler/ma_profiler.h"
#include "ma/internal/utils/profiler/ma_profile_info.h"
#include "ma/internal/services/profile/ma_profile_service.h"
#include "ma/internal/apps/masvc/ma_app_driver.h"
#include "ma/internal/ma_macros.h"

#include <tnt/sessionscope.h>
#include <stdio.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "masvc"

extern ma_logger_t *masvc_logger;

static tnt::ComponentFactoryImpl<logged_on_controller> factory("ma_logged_on_controller");

unsigned logged_on_controller::operator() (tnt::HttpRequest& request, tnt::HttpReply& reply, tnt::QueryParams& qparam) {
    if(strcmp("/logged_on.html", request.getUrl().c_str())) {
        MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "url(%s) is not logged_on.html, so ignoring", request.getUrl().c_str());
        return DECLINED;
    }
    string name(qparam.arg<string>("name"));
    string pickupdate(qparam.arg<string>("pickupdate"));
    string contact(qparam.arg<string>("contact"));
    string pickupaddress(qparam.arg<string>("pickupaddress"));
    string deliveryaddress(qparam.arg<string>("deliveryaddress"));
    string servicetype(qparam.arg<string>("servicetype"));
    string sourcecity(qparam.arg<string>("sourcecity"));
    string destinationcity(qparam.arg<string>("destinationcity"));
    string email(request.getCookie("email"));

    MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "url(%s) is logged on executing", request.getUrl().c_str());
    if(!email.empty()) {
        reply.setCookie("error", std::string("Session is already expired. Please login again!"), 2);
        reply.redirect(string("index.html"));
        return HTTP_OK;
    }
  
    MA_LOG(masvc_logger, MA_LOG_SEV_INFO, " HTTP Post request  servicetype(%s) name(%s) pickup(%s) contact(%s) pickupaddress(%s) pickupcity(%s) deliveryaddress(%s) deliverycity(%s)",  servicetype.c_str(), name.c_str(), pickupdate.c_str(), contact.c_str(), pickupaddress.c_str(), sourcecity.c_str(), deliveryaddress.c_str(), destinationcity.c_str());

    ma_error_t err = MA_OK;
    ma_profile_info_t *info = NULL;
    ma_profiler_t *profiler = NULL;

    (void)ma_profile_service_get_profiler(app_driver::get_instance().get_profile_service(), &profiler);
    if(MA_OK == (err = ma_profiler_get(profiler, email.c_str(), &info))) {
        if(info) {
            char passwd[MA_MAX_PATH_LEN] = {0};

            MA_LOG(masvc_logger, MA_LOG_SEV_DEBUG, "Profile id(%s) already exist in DB", email.c_str());
            if(MA_OK == (err = ma_profile_info_get_password(info, passwd))) {
                MA_LOG(masvc_logger, MA_LOG_SEV_DEBUG, "Profile password(%s) for profile id(%s)", passwd, email.c_str());
            }
            reply.redirect(string("logged_on.html"));
            free(info);
        }
    }
    MA_LOG(masvc_logger, MA_LOG_SEV_DEBUG, " logged on handler ends");

    return HTTP_OK;
}

