#include "ma/internal/apps/masvc/ma_add_user_controller.h"
#include "ma/internal/utils/inventory/ma_inventory_info.h"
#include "ma/internal/defs/ma_booking_defs.h"
#include "ma/internal/utils/context/ma_context.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"
#include "ma/internal/utils/booker/ma_booking_info.h"
#include "ma/internal/utils/booker/ma_booker.h"
#include "ma/internal/utils/sms/ma_sms.h"
#include "ma/ma_common.h"
#include "ma/ma_log.h"
#include "ma/internal/utils/profiler/ma_profiler.h"
#include "ma/internal/utils/profiler/ma_profile_info.h"
#include "ma/internal/utils/location/ma_location_info.h"
#include "ma/internal/utils/consignor/ma_consign_info.h"
#include "ma/internal/utils/inventory/ma_inventory_info.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/services/profile/ma_profile_service.h"
#include "ma/internal/services/mail/ma_mail_service.h"
#include "ma/internal/apps/masvc/ma_app_driver.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/ma_strdef.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_message.h"
#include "ma/internal/utils/threading/ma_atomic.h"
#include "ma/internal/utils/uuid/ma_uuid.h"

#include <tnt/sessionscope.h>
#include <stdio.h>
#include <curl/curl.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "masvc"

extern ma_logger_t *masvc_logger;

static tnt::ComponentFactoryImpl<add_user_controller> factory("ma_add_user_controller");

static void update_service_availability_for_pincode(const char *protocol, const char *server, const char *port, const char *path, const char *pincode, const char *status) {
  CURL *curl;
  CURLcode res;
 
  /* In windows, this will init the winsock stuff */ 
  curl_global_init(CURL_GLOBAL_ALL);
 
  /* get a curl handle */ 
  curl = curl_easy_init();
  if(curl) {
    char buf[1024+1] = {0};
    snprintf(buf, 1024, "%s://%s:%s%s?pincode=%s&status=%s", protocol, server, port, path, pincode, status);
    MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "pincode enable url (%s) ", buf);
    /* First set the URL that is about to receive our POST. This URL can
       just as well be a https:// URL if that is what should receive the
       data. */ 
    //curl_easy_setopt(curl, CURLOPT_URL, "http://mileaccess.com:8080/autmunIS/users/enableservice?pincode=560083&status=true");
    curl_easy_setopt(curl, CURLOPT_URL, buf);
    curl_easy_setopt(curl, CURLOPT_URL, buf);
    /* Now specify the POST data */ 
    /*curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "name=daniel&project=curl");*/
    /*curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "{\"contact\":\"9886412843\",\"email\":\"zabeen.azra1202@gmail.com\"}");*/
 
    /* Perform the request, res will get the return code */ 
    res = curl_easy_perform(curl);
    /* Check for errors */ 
    if(res != CURLE_OK)
      MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "pincode enable failed (%s) ", curl_easy_strerror(res));
 
    /* always cleanup */ 
    curl_easy_cleanup(curl);
  }
  curl_global_cleanup();
}

static ma_error_t consign_register_handler(ma_consign_info_t *linfo, const char *contact) {
   if(linfo) {
        ma_error_t err = MA_OK;
        ma_msgbus_t *msgbus = NULL;

        if(MA_OK == (err = ma_mail_service_get_msgbus(app_driver::get_instance().get_mail_service(), &msgbus))) {
            ma_msgbus_endpoint_t *endpoint = NULL;
            ma_message_t *request = NULL;
            ma_message_t *response = NULL;
 
            if(MA_OK == (err = ma_msgbus_endpoint_create(msgbus, MA_CONSIGN_SERVICE_NAME_STR, NULL, &endpoint)) &&
               MA_OK == (err = ma_message_create(&request))) {
                ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;

                (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);
                (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                if(MA_OK == (err = ma_message_set_property(request, MA_CONSIGN_SERVICE_MSG_TYPE, MA_CONSIGN_SERVICE_MSG_REGISTER_TYPE)) &&
                   MA_OK == (err = ma_message_set_property(request, MA_CONSIGN_ID, contact))
                  ) {
                      ma_variant_t *payload = NULL;

                     if(MA_OK == (err = ma_consign_info_convert_to_variant(linfo, &payload))) {
                         (void)ma_message_set_payload(request, payload);
                         if(MA_OK == (err = ma_msgbus_send(endpoint, request, &response))) {
                             const char *reply_status = NULL;

                             if(MA_OK == (err = ma_message_get_property(response, MA_CONSIGN_STATUS, &reply_status))) {
                                 if(!strcmp(reply_status, MA_CONSIGN_ID_WRITTEN)) {
                                     MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "consign id (%s) saved successfully", contact);
                                 } else {
                                     MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "consign id (%s) save failed", contact);
                                 }
                             } 
                             (void)ma_message_release(response);
                         }
                         (void)ma_variant_release(payload);
                     }
                }
                (void)ma_message_release(request);
                (void)ma_msgbus_endpoint_release(endpoint);
            }
        }
        return err;
    } 
    return MA_ERROR_INVALIDARG;
}

static ma_error_t location_register_handler(ma_location_info_t *linfo, const char *contact) {
   if(linfo) {
        ma_error_t err = MA_OK;
        ma_msgbus_t *msgbus = NULL;
        char location_id[MA_MAX_LEN+1] = {0};  

        (void)ma_location_info_get_pincode(linfo, location_id);
        (void)ma_location_info_add_contact(linfo, location_id, contact);
        if(MA_OK == (err = ma_mail_service_get_msgbus(app_driver::get_instance().get_mail_service(), &msgbus))) {
            ma_msgbus_endpoint_t *endpoint = NULL;
            ma_message_t *request = NULL;
            ma_message_t *response = NULL;
 
            if(MA_OK == (err = ma_msgbus_endpoint_create(msgbus, MA_LOCATION_SERVICE_NAME_STR, NULL, &endpoint)) &&
               MA_OK == (err = ma_message_create(&request))) {
                ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;

                (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);
                (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                if(MA_OK == (err = ma_message_set_property(request, MA_LOCATION_SERVICE_MSG_TYPE, MA_LOCATION_SERVICE_MSG_REGISTER)) &&
                   MA_OK == (err = ma_message_set_property(request, MA_LOCATION_ID, location_id)) &&
                   MA_OK == (err = ma_message_set_property(request, MA_LOCATION_CRN, contact))
                  ) {
                      ma_variant_t *payload = NULL;

                     if(MA_OK == (err = ma_location_info_convert_to_variant(linfo, &payload))) {
                         (void)ma_message_set_payload(request, payload);
                         if(MA_OK == (err = ma_msgbus_send(endpoint, request, &response))) {
                             const char *reply_status = NULL;

                             if(MA_OK == (err = ma_message_get_property(response, MA_LOCATION_STATUS, &reply_status))) {
                                 if(!strcmp(reply_status, MA_LOCATION_ID_WRITTEN)) {
                                     MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "location id (%s) saved successfully", location_id);
                                 } else {
                                     MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "location id (%s) save failed", location_id);
                                 }
                             } 
                             (void)ma_message_release(response);
                         }
                         (void)ma_variant_release(payload);
                     }
                }
                (void)ma_message_release(request);
                (void)ma_msgbus_endpoint_release(endpoint);
            }
        }
        return err;
    } 
    return MA_ERROR_INVALIDARG;
}

ma_error_t add_user_controller::add(ma_profile_info_t *info, ma_bool_t *res) {
    if(!info)return MA_ERROR_INVALIDARG;
    ma_error_t err = MA_OK;
    char email[MA_MAX_LEN+1] = {0};
    char contact[MA_MAX_LEN+1] = {0};
    char user[MA_MAX_NAME+1] = {0};

    (void)ma_profile_info_get_contact(info, contact); 
    if(!strcmp(contact, MA_EMPTY)) {
        MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "profile contact cannot be empty (%s) ", contact);
        return MA_OK;
    }
    (void)ma_profile_info_get_email_id(info, email); 
    (void)ma_profile_info_get_user(info, user);
    ma_profile_info_user_type_t type = MA_PROFILE_INFO_USER_TYPE_CUSTOMER_EXECUTIVE; 
    (void)ma_profile_info_get_user_type(info, &type);
    
    MA_LOG(masvc_logger, MA_LOG_SEV_INFO, "Profile id(%s) does not exist in DB, creating...", contact);
    ma_variant_t *info_var = NULL;

    if(MA_OK == (err = ma_profile_info_convert_to_variant(info, &info_var))) {
        ma_msgbus_t *msgbus = NULL;

        (void)ma_profiler_set_logger(masvc_logger);
        (void)ma_mail_service_get_msgbus(app_driver::get_instance().get_mail_service(), &msgbus);
        ma_msgbus_endpoint_t *pendpoint = NULL;
        ma_message_t *prequest = NULL;
        ma_message_t *presponse = NULL;

        if(MA_OK == (err = ma_msgbus_endpoint_create(msgbus, MA_PROFILE_SERVICE_NAME_STR, NULL, &pendpoint)) &&
           MA_OK == (err = ma_message_create(&prequest))) {
            ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;

            (void)ma_msgbus_endpoint_setopt(pendpoint, MSGBUSOPT_THREADMODEL, thread_option);
            (void)ma_msgbus_endpoint_setopt(pendpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
            (void)ma_message_set_property(prequest, MA_PROFILE_SERVICE_MSG_TYPE, MA_PROFILE_SERVICE_MSG_REGISTER_TYPE);
            (void)ma_message_set_property(prequest, MA_PROFILE_ID, contact);
            if(MA_OK == (err = ma_message_set_payload(prequest, info_var))) {
                if(MA_OK == (err = ma_msgbus_send(pendpoint, prequest, &presponse))) {
                    const char *status = NULL;

                    (void)ma_message_get_property(presponse, MA_PROFILE_STATUS, &status);
                    if(!strcmp(status, MA_PROFILE_ID_WRITTEN)) {
                        ma_msgbus_endpoint_t *iendpoint = NULL;
                        ma_message_t *irequest = NULL;
                        ma_message_t *iresponse = NULL;

                        *res = MA_TRUE;
                        {
                            ma_consign_info_t *consign = NULL;

                            if(MA_OK == (err = ma_consign_info_create(&consign))) {
                                (void)ma_consign_info_set_contact(consign, contact);
                                if(MA_OK == (err = consign_register_handler(consign, contact))) {
                                     MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "executive consign (%s) saved successfully", contact);
                                } else {
                                     MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "executive consign (%s) saved failed (%d)", contact, err);
                                }
                                (void)ma_consign_info_release(consign);
                            }
                        }
                        if(type == MA_PROFILE_INFO_USER_TYPE_OPERATION) {
                            char pincode[MA_MAX_LEN+1] = {0};
                            ma_location_info_t *linfo = NULL;

                            if(MA_OK == (err = ma_location_info_create(&linfo))) {
                                (void)ma_profile_info_get_source_pincode(info, pincode);
                                (void)ma_location_info_set_pincode(linfo, pincode);
                                (void)ma_location_info_add_contact(linfo, pincode, contact);
                                update_service_availability_for_pincode(MA_HTTP_NON_SECURE_PROTOCOL, MA_MILEACCESS_HOST, MA_MILEACCESS_PINCODE_MAPPER_PORT, MA_AUTUMNIS_ENABLESERVICE_ENDPOINT, pincode, "true");
                                if(MA_OK == (err = location_register_handler(linfo, contact))) {
                                     MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "executive location (%s) saved successfully", contact);
                                } else {
                                     MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "executive location (%s) saved failed (%d)", contact, err);
                                }
                                (void)ma_location_info_release(linfo);
                            }
                        }
                        if(MA_OK == (err = ma_msgbus_endpoint_create(msgbus, MA_INVENTORY_SERVICE_NAME_STR, NULL, &iendpoint)) &&
                           MA_OK == (err = ma_message_create(&irequest))) {
                            ma_inventory_info_t *inventory_info = NULL;

                            (void)ma_msgbus_endpoint_setopt(iendpoint, MSGBUSOPT_THREADMODEL, thread_option);
                            (void)ma_msgbus_endpoint_setopt(iendpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                            (void)ma_message_set_property(irequest, MA_INVENTORY_SERVICE_MSG_TYPE, MA_INVENTORY_SERVICE_MSG_REGISTER);
                            (void)ma_message_set_property(irequest, MA_INVENTORY_ID, contact);
                            if(MA_OK == (err = ma_inventory_info_create(&inventory_info))) {
                                ma_task_time_t date = {0};
                                ma_variant_t *ivar = NULL;

                                get_localtime(&date);
                                (void)ma_inventory_info_set_date(inventory_info, date);
                                (void)ma_inventory_info_set_email_id(inventory_info, email);
                                (void)ma_inventory_info_set_contact(inventory_info, contact);
                                if(MA_OK == (err = ma_inventory_info_convert_to_variant(inventory_info, &ivar))) {
                                    (void)ma_message_set_payload(irequest, ivar);
                                    if(MA_OK == (err = ma_msgbus_send(iendpoint, irequest, &iresponse))) {
                                        const char *inventory_status = NULL;

                                        (void)ma_message_get_property(iresponse, MA_INVENTORY_STATUS, &inventory_status);
                                        if(!strcmp(inventory_status, MA_INVENTORY_ID_WRITTEN)) {
                                            MA_LOG(masvc_logger, MA_LOG_SEV_INFO, "Inventory id(%s) information written succesfully", contact);
                                        } else
                                            MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "Inventory id(%s) information write failed", contact);
                                        (void)ma_message_release(iresponse);
                                    }
                                    (void)ma_variant_release(ivar);
                                }
                                (void)ma_inventory_info_release(inventory_info);
                            }
                            (void)ma_msgbus_endpoint_release(iendpoint);
                            (void)ma_message_release(irequest);
                        }
                        MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "profile info written successfully into profile DB");
                    } else {
                        MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "profile info write failed");
                    }
                    (void)ma_message_release(presponse);
                } else
                    MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "profile registration failed, last error(%d)", err);
                (void)ma_variant_release(info_var);
            }
            (void)ma_message_release(prequest);
            (void)ma_msgbus_endpoint_release(pendpoint);
        }
        (void)ma_variant_release(info_var);
    } else
        MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "profile info variant conversion failed, last error(%d)", err);

    return err;
}

unsigned add_user_controller::operator() (tnt::HttpRequest& request, tnt::HttpReply& reply, tnt::QueryParams& qparam) {
    ma_profile_info_t *info = NULL;
    ma_bool_t res = MA_FALSE;
    ma_json_t *jout = NULL;
    ma_error_t err = MA_OK;

    (void)ma_sms_set_logger(masvc_logger);
    if(MA_OK == (err = ma_profile_info_convert_from_json(request.getBody().c_str(), &info))) {
        if(MA_OK == (err = add(info, &res))) {
            if(MA_OK == (err = ma_json_create(&jout))) {
                MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "user registration status %s", res ? "success" : "failure");
                (void)ma_json_add_element(jout, MA_PROFILE_INFO_ATTR_STATUS, res ? "success" : "failure");
                ma_bytebuffer_t *buffer = NULL;

                ma_bytebuffer_create(MA_MAX_BUFFER_LEN, &buffer);
                if(MA_OK == ma_json_get_data(jout, buffer)) {
                    reply.setContentType("Content-Type: application/json");
                    reply.out() << (const char*)ma_bytebuffer_get_bytes(buffer);
                }
                if(res) {
                    char user_contact[MA_MAX_LEN+1] = {0};
                    char msg[MA_MAX_BUFFER_LEN+1] = {0};
                    char user[MA_MAX_NAME+1] = {0};

                    (void)ma_profile_info_get_contact(info, user_contact);
                    (void)ma_profile_info_get_user(info, user);
                    MA_MSC_SELECT(_snprintf, snprintf)(msg, MA_MAX_BUFFER_LEN, "Dear %s, Thank you for successfully registering with mileaccess.com", user);
                    MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "sending user registeration message to user contact api %s", msg);
                    (void)ma_sms_send(MA_HTTP_NON_SECURE_PROTOCOL, MA_MILEACCESS_HOST, MA_MILEACCESS_PINCODE_MAPPER_PORT, MA_SMS_SEND_PATH, user_contact, msg); 
                }
                (void)ma_json_release(jout);
            }
        }
        (void)ma_profile_info_release(info);
    }


    return HTTP_OK;
}

