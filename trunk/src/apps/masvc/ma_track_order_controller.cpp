#include "ma/internal/apps/masvc/ma_track_order_controller.h"
#include "ma/internal/utils/location/ma_location_info.h"
#include "ma/internal/utils/location/ma_location.h"
#include "ma/internal/defs/ma_booking_defs.h"
#include "ma/internal/utils/booker/ma_booking_info.h"
#include "ma/internal/utils/booker/ma_booker.h"
#include "ma/internal/utils/context/ma_context.h"
#include "ma/ma_common.h"
#include "ma/ma_log.h"
#include "ma/internal/utils/profiler/ma_profiler.h"
#include "ma/internal/utils/profiler/ma_profile_info.h"
#include "ma/internal/services/profile/ma_profile_service.h"
#include "ma/internal/services/mail/ma_mail_service.h"
#include "ma/internal/apps/masvc/ma_app_driver.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/ma_strdef.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_message.h"
#include "ma/internal/utils/threading/ma_atomic.h"
#include "ma/internal/utils/uuid/ma_uuid.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"

#include <tnt/sessionscope.h>
#include <stdio.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "masvc"

extern ma_logger_t *masvc_logger;

static tnt::ComponentFactoryImpl<track_order_controller> factory("ma_track_order_controller");
static ma_context_t *context = NULL;

unsigned track_order_controller::operator() (tnt::HttpRequest& request, tnt::HttpReply& reply, tnt::QueryParams& qparam) {
   if(strcmp("/track_order.html", request.getUrl().c_str()) &&  strcmp("/track_order", request.getUrl().c_str())) {
       MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "url(%s) is not track_order.html, so ignoring", request.getUrl().c_str());
       return DECLINED;
   }
   string order(qparam.arg<string>("order"));
   string email(request.getCookie("email"));

   MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "url(%s) is track_order.html executing profile(%s)", request.getUrl().c_str(), email.c_str());
   if(email.empty()) {
       reply.setCookie("error", std::string("Session is already expired. Please login again!"), 2);
       reply.redirect(string("index.html"));
       return HTTP_OK;
   }
   MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "HTTP Get track order  request email_id(%s) order(%s)", email.c_str(), order.c_str());
  

   ma_error_t err = MA_OK;
   ma_booker_t *booker = NULL;
   ma_booking_info_t *info = NULL;
   ma_profile_info_t *profile_info = NULL;
   ma_profiler_t *profiler = NULL;

   (void)ma_booking_service_get_context(app_driver::get_instance().get_booking_service(), &context);
   if(MA_OK == (err = ma_booking_service_get_booker(app_driver::get_instance().get_booking_service(), &booker))) {
       (void)ma_booker_set_logger(masvc_logger);
       if(MA_OK == (err = ma_booker_get(booker, email.c_str(), order.c_str(), &info))) {
           char booking_email_id[MA_MAX_BUFFER_LEN+1] = {0};
           char picker_id[MA_MAX_LEN+1] = {0};

           (void)ma_booking_info_get_email_id(info, booking_email_id);
           (void)ma_booking_info_get_picker_email_id(info, picker_id);
           if(picker_id[0]) {
               ma_profile_info_t *picker_profile = NULL;

               if(MA_OK == (err = ma_profiler_get(profiler, picker_id, &picker_profile))) {
                   ma_task_time_t last_seen = {0};
                   ma_task_time_t dummy = {0};
                   char last_seen_str[MA_MAX_LEN+1] = {0};

                   (void)ma_profile_info_get_last_seen(picker_profile, &last_seen);
                   ma_strtime(last_seen, last_seen_str);
                   if(memcmp(&last_seen, &dummy, sizeof(ma_task_time_t))) {
                       ma_location_info_t *loc_info = NULL;

                       if(MA_OK == (err = ma_location_get(MA_CONTEXT_GET_LOCATION(context), picker_id, last_seen_str, &loc_info))) {
                           char loc[MA_MAX_BUFFER_LEN+1] = {0};


                           (void)ma_location_info_get_details(loc_info, loc);
                           reply.setCookie("location", std::string(loc), 500);
                           MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "Picker id(%s) last seen date(%s) loc(%s)", picker_id, last_seen_str, loc);
                           (void)ma_location_info_release(loc_info);
                       }
                   }
                   (void)ma_profile_info_release(profile_info);
               }
           }
           MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "booking email id(%s) profile email(%s)", booking_email_id, email.c_str());
           if(strcmp(booking_email_id, email.c_str())) {
               if(MA_OK == (err = ma_profile_service_get_profiler(app_driver::get_instance().get_profile_service(), &profiler))) {
                   if(MA_OK == (err = ma_profiler_get(profiler, email.c_str(), &profile_info))) {
                       ma_profile_info_user_type_t type = MA_PROFILE_INFO_USER_TYPE_NORMAL;

                       (void)ma_profile_info_get_user_type(profile_info, &type);                   
                       (void)ma_profile_info_release(profile_info);
                       if(MA_PROFILE_INFO_USER_TYPE_ADMIN != type) {
                           reply.setCookie("error", std::string("You don't have privilege to track the status of the order!"), 2);
                           reply.redirect(string("track_order.html"));
                           return HTTP_OK;
                       }
                   }
               }
           } else {
               char status[MA_MAX_NAME+1] = {0};

               (void)ma_booking_info_get_status_str(info, status);
               MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "order(%s) status(%s)", order.c_str(), status);
               reply.setCookie("order", order, 500);
               reply.setCookie("status", std::string(status), 500);
               reply.redirect(string("track_order.html"));
           }
           (void)ma_booking_info_release(info);
       } else {
           reply.setCookie("error", std::string("Order does not exist!"), 2);
           reply.redirect(string("track_order.html"));
           return HTTP_OK;
       }
   }
    

   MA_LOG(masvc_logger, MA_LOG_SEV_DEBUG, " track order handler ends");
   reply.redirect(std::string("track_order.html"));

   return HTTP_OK;
}

