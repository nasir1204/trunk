#include <windows.h>
#include <tchar.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "masvc"

#include "ma/ma_log.h"

#include "ma/logger/ma_file_logger.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/conf/ma_conf_log.h"
#include "ma/internal/services/io/ma_io_service.h"
#include "ma/internal/services/sensor/ma_sensor_service.h"
#include "ma/sensor/ma_sensor.h"
#include "ma/sensor/ma_sensor_msg.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/defs/ma_logger_defs.h"
#include "ma/internal/apps/masvc/ma_masvc.h"

#include "ma/internal/buildinfo.h"

#define inline MA_INLINE /* Mfetrust incorrectly uses inline which is not a C valid in c89 */
#include "mfetrust.h" /* to bypass VSE AP */
#undef inline

#include <WtsApi32.h>
#pragma comment(lib, "WtsApi32.lib")

/*
 Register this as a service:

 sc[.exe] create masvc binPath= c:\dev\ma\src\trunk\build\msvc\Debug\masvc.exe start= auto
 */

#if 0
typedef struct service_data_s {
  SERVICE_STATUS_HANDLE     service_status_handle;
  SERVICE_STATUS            service_status;

  ma_log_filter_t           *filter;
  
  ma_logger_t               *logger;

  ma_io_service_t           *io_service;

  ma_sensor_service_t       *sensor_service;


} service_data_t;

#endif

static service_data_t sd = {0};

void set_service_state(service_data_t *sd, DWORD state) {
    sd->service_status.dwCurrentState = state;
    SetServiceStatus(sd->service_status_handle, &sd->service_status);
}

static int service_run(void);

static void WINAPI service_main(DWORD dwNumServicesArgs, LPTSTR *lpServiceArgVectors);

static DWORD WINAPI handler_ex(DWORD dwControl, DWORD dwEventType, LPVOID lpEventData, LPVOID lpContext);

/* for debug printing */

static struct service_description {
    const char *event_description;
    const char *event_details;
} service_control_code_text(DWORD dwControl, DWORD dwEventType, LPVOID lpEventData);


int WINAPI _tWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PTSTR pCmdLine, int nCmdShow) {
    /* check it is actually being started as a service (as there could be several cmd line options) */
    return service_run();
}


static int service_run(void) {
    static const SERVICE_TABLE_ENTRY service_start_table[] = {{_T(""),&service_main}, {0,0}};
    if (!StartServiceCtrlDispatcher(service_start_table)) {/* This call blocks, see you in service_main()*/
        return GetLastError();
    }

    return NO_ERROR;
}

static void WINAPI service_main(DWORD dwNumServicesArgs, LPTSTR *lpServiceArgVectors) {
    ma_error_t rc = MA_OK;
    ma_temp_buffer_t logs_path = MA_TEMP_BUFFER_INIT;
    HANDLE hTrust = MfeTrustOpen();
    sd.service_status.dwServiceType = SERVICE_WIN32_OWN_PROCESS;
    sd.service_status.dwControlsAccepted = SERVICE_ACCEPT_SHUTDOWN | SERVICE_ACCEPT_STOP |  SERVICE_ACCEPT_POWEREVENT | SERVICE_ACCEPT_SESSIONCHANGE;
    sd.service_status.dwWin32ExitCode = NO_ERROR;
    sd.service_status.dwServiceSpecificExitCode = NO_ERROR;
    sd.service_status.dwCheckPoint = 0;
    sd.service_status.dwWaitHint = 0;
    
    MfeApTrustProcessStart(hTrust);
    
    if(MA_OK == (rc = ma_conf_get_logs_path(&logs_path))) {        
        ma_temp_buffer_t log_file_name = MA_TEMP_BUFFER_INIT;
        if(MA_OK == (rc = ma_conf_get_agent_log_file_name(&log_file_name))) {
            if(MA_OK == (rc =  ma_file_logger_create((const char *)ma_temp_buffer_get(&logs_path),(const char *)ma_temp_buffer_get(&log_file_name),".log", (ma_file_logger_t **) &sd.logger))) {				
                ma_generic_log_filter_create("*.Info", &sd.filter);
                ma_logger_add_filter(sd.logger, sd.filter);
                if(NULL != (sd.service_status_handle = RegisterServiceCtrlHandlerEx(_T(""), &handler_ex, &sd))) {
					MA_LOG(sd.logger,MA_LOG_SEV_INFO,"Starting masvc v." MA_VERSION_STRING " Windows Service...");
					set_service_state(&sd, SERVICE_START_PENDING);

					if(S_OK == CoInitializeEx(NULL,COINIT_MULTITHREADED)) {
						if(MA_OK == (rc = ma_io_service_create(MA_IO_SERVICE_NAME_STR, sd.logger, &sd.io_service))) {
							set_service_state(&sd, SERVICE_RUNNING);
							MA_LOG(sd.logger,MA_LOG_SEV_INFO,"Running MA Service...");
                                                        ma_io_service_get_service(sd.io_service, MA_PROFILE_SERVICE_NAME_STR, (ma_service_t **)(&sd.profile_service));
                                                        ma_io_service_get_service(sd.io_service, MA_BOOKING_SERVICE_NAME_STR, (ma_service_t **)(&sd.booking_service));
							ma_io_service_get_service(sd.io_service, MA_SENSOR_SERVICE_NAME_STR, (ma_service_t **)(&sd.sensor_service));
							/* Below will block */
							ma_io_service_run(sd.io_service);

							MA_LOG(sd.logger,MA_LOG_SEV_INFO,"MA service returned from msg loop, continuing shutdown...");
			
							(void)ma_io_service_release(sd.io_service);
							sd.io_service = 0;
						}
						else 
							MA_LOG(sd.logger,MA_LOG_SEV_ERROR,"Unable to Create I/O Service,error: <%d>,",rc);
						
						CoUninitialize();
					}
					else
						MA_LOG(sd.logger, MA_LOG_SEV_ERROR, "com object init failed.");
	            }
	            else
                   MA_LOG(sd.logger,MA_LOG_SEV_ERROR,"Unable to register handle, GetLastError = <%d>", GetLastError());
                
                MA_LOG(sd.logger,MA_LOG_SEV_INFO,"Stopped Service");
                ma_logger_dec_ref(sd.logger);	
                ma_log_filter_release(sd.filter);				
			}
        }
        ma_temp_buffer_uninit(&log_file_name);
    }
    ma_temp_buffer_uninit(&logs_path);

    MfeApTrustProcessEnd(hTrust);
    MfeTrustClose(hTrust);
	    

	
	/*
     * Note, below SERVICE_STOPPED should be the last call. From MSDN:
     * Do not attempt to perform any additional work after calling SetServiceStatus with SERVICE_STOPPED, because the service process can be terminated at any time    
     */
    set_service_state(&sd, SERVICE_STOPPED);
}

ma_error_t get_session_user_domain_name(service_data_t *sd, DWORD sessionID, wchar_t *user, wchar_t *domain) {
    if(sd && user && domain) {
        ma_error_t err = MA_ERROR_APIFAILED;
        LPTSTR user_buf = NULL;
        LPTSTR domain_buf = NULL;
        DWORD len = 0;
        BOOL status = 0;
        
        
        if(status = WTSQuerySessionInformation(WTS_CURRENT_SERVER_HANDLE, sessionID, WTSDomainName, &domain_buf, &len)) {
            /*MA_WLOG(sd->logger,MA_LOG_SEV_INFO, L"WTSQuerySessionInformation domain(%ls)", domain_buf);*/
            if(len < MA_MAX_NAME) {
                wcsncpy(domain, domain_buf, len); err = MA_OK;
                if(status = WTSQuerySessionInformation(WTS_CURRENT_SERVER_HANDLE, sessionID, WTSUserName, &user_buf, &len)) {
                    /*MA_WLOG(sd->logger,MA_LOG_SEV_INFO, L"WTSQuerySessionInformation user(%ls)", user_buf);*/
                    err = MA_ERROR_APIFAILED;
                    if(len < MA_MAX_NAME) {
                        wcsncpy(user, user_buf, len); err = MA_OK;
                    }
                    WTSFreeMemory(user_buf);
                } else 
                    MA_LOG(sd->logger,MA_LOG_SEV_INFO, "WTSQuerySessionInformation on WTSUserName failed with error 0x%.8X\n", GetLastError());
            }
            WTSFreeMemory(domain_buf);
        } else
            MA_LOG(sd->logger,MA_LOG_SEV_INFO,"WTSQuerySessionInformation on WTSDomainName failed with error 0x%.8X\n", GetLastError());

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t post_user_sensor_message(service_data_t *sd, DWORD sessionId, ma_sensor_event_type_t type, ma_user_logged_state_t state) {
    if(sd) {
        ma_user_sensor_msg_t *user_msg = NULL;
        wchar_t domain[MA_MAX_NAME] = {0};
        wchar_t user[MA_MAX_NAME] = {0};
        ma_error_t err = MA_OK;
        ma_sensor_msg_t *msg = NULL;
        size_t index = 0;

        if(MA_OK == (err = ma_user_sensor_msg_create(&user_msg, 1))) {
            (void)get_session_user_domain_name(sd, sessionId, user, domain);
            (void)ma_user_sensor_msg_set_event_type(user_msg, type);
            MA_LOG(sd->logger, MA_LOG_SEV_DEBUG, "posting user sensor event<%d>...", type);
            (void)ma_user_sensor_msg_set_session_id(user_msg, index, sessionId);
            (void)ma_user_sensor_msg_set_wuser(user_msg, index, user);
            (void)ma_user_sensor_msg_set_wdomain(user_msg, index, domain);
            (void)ma_user_sensor_msg_set_logon_state(user_msg, state);
            if(MA_OK == (err = ma_sensor_msg_create_from_user_sensor_msg(user_msg, &msg))) {
                MA_LOG(sd->logger, MA_LOG_SEV_DEBUG, "posting user sensor event<%d>...", type);
                if(MA_OK != (err = ma_sensor_service_post_sensor_msg(sd->sensor_service, msg))) {
                    MA_LOG(sd->logger, MA_LOG_SEV_ERROR, "posting user sensor event<%d> message failed, errorcode<%d>", type, err);
                }
                (void)ma_sensor_msg_release(msg);
            }
            (void)ma_user_sensor_msg_release(user_msg);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t post_power_sensor_message(service_data_t *sd, ma_sensor_event_type_t type) {
    if(sd) {
        ma_error_t err = MA_OK;
        ma_power_sensor_msg_t *pwr_msg = NULL;
        ma_sensor_msg_t *msg = NULL;
        size_t index = 0;

        if(MA_OK == (err = ma_power_sensor_msg_create(&pwr_msg, 1))) {
            SYSTEM_POWER_STATUS power_status = {0};

            if(!GetSystemPowerStatus(&power_status)) {
                {
                    ma_power_status_t status = MA_POWER_STATUS_OFFLINE;

                    switch(power_status.ACLineStatus) {
                        case 0x00: status = MA_POWER_STATUS_OFFLINE; break;
                        case 0x01: status = MA_POWER_STATUS_ONLINE; break;
                        case 0xff: status = MA_POWER_STATUS_UNKNOWN; break;
                    }
                    if(MA_OK != (err = ma_power_sensor_msg_set_power_status(pwr_msg, index, status))) {
                        MA_LOG(sd->logger,MA_LOG_SEV_ERROR,"sensor(ma.power.sensor) failed to set power status");
                    }
                }
                {
                    ma_battery_charge_status_t status = MA_BATTERY_CHARGE_STATUS_HIGH;

                    switch(power_status.BatteryFlag) {
                        case 0x01: status = MA_BATTERY_CHARGE_STATUS_HIGH; break;
                        case 0x02: status = MA_BATTERY_CHARGE_STATUS_LOW; break;
                        case 0x04: status = MA_BATTERY_CHARGE_STATUS_CRITICAL; break;
                        case 0x08: status = MA_BATTERY_CHARGE_STATUS_CHARGING; break;
                        case 0x80: status = MA_BATTERY_CHARGE_STATUS_NO_BATTERY; break;
                        case 0xff: status = MA_BATTERY_CHARGE_STATUS_UNKNOWN; break;
                    }
                    if(MA_OK != (err = ma_power_sensor_msg_set_battery_charge_status(pwr_msg, index, status))) {
                        MA_LOG(sd->logger,MA_LOG_SEV_ERROR,"sensor(ma.power.sensor) failed to set battery charge status");
                    }
                }
                if(MA_OK != (err = ma_power_sensor_msg_set_battery_lifetime(pwr_msg, index, power_status.BatteryLifeTime))) {
                    MA_LOG(sd->logger,MA_LOG_SEV_ERROR,"sensor(ma.power.sensor) failed to set battery life time");
                }
                if(MA_OK != (err = ma_power_sensor_msg_set_battery_percent(pwr_msg, index, power_status.BatteryLifePercent))) {
                    MA_LOG(sd->logger,MA_LOG_SEV_ERROR,"sensor(ma.power.sensor) failed to set battery life percentage");
                }
            }
            (void)ma_power_sensor_msg_set_event_type(pwr_msg, type);
            if(MA_OK == (err = ma_sensor_msg_create_from_power_sensor_msg(pwr_msg, &msg))) {
                if(MA_OK != (err = ma_sensor_service_post_sensor_msg(sd->sensor_service, msg))) {
                    MA_LOG(sd->logger, MA_LOG_SEV_ERROR, "posting power sensor event<%d> message failed, errorcode<%d>", type, err);
                }
                (void)ma_sensor_msg_release(msg);
            }
            (void)ma_power_sensor_msg_release(pwr_msg);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static DWORD WINAPI handler_ex(DWORD dwControl, DWORD dwEventType, LPVOID lpEventData, LPVOID lpContext) {
    service_data_t *sd = (service_data_t*)lpContext;
    ma_sensor_service_t *sensor = sd->sensor_service;
    ma_error_t err = MA_OK;
    struct service_description service_control_description = service_control_code_text(dwControl, dwEventType, lpEventData);
    ma_sensor_msg_t *msg = NULL;

    ma_sensor_msg_set_logger(sd->logger);
    MA_LOG(sd->logger,MA_LOG_SEV_INFO,"Received handler control code <%s(0x%X)>, event_type <%s(0x%X)> context <%p>", service_control_description.event_description, (int) dwControl, service_control_description.event_details, (int) dwEventType, lpEventData);
    switch (dwControl) {
        case SERVICE_CONTROL_INTERROGATE: return NO_ERROR;

        case SERVICE_CONTROL_SHUTDOWN: {
            ma_system_sensor_msg_t *sys_msg = NULL;

            MA_LOG(sd->logger,MA_LOG_SEV_INFO,"Shut down system...");
            MA_LOG(sd->logger, MA_LOG_SEV_ERROR, "posting system sensor event<%d>...", MA_SYSTEM_SENSOR_EVENT_SHUTDOWN);
            if(MA_OK == (err = ma_system_sensor_msg_create(&sys_msg, 1))) {
                (void)ma_system_sensor_msg_set_event_type(sys_msg, MA_SYSTEM_SENSOR_EVENT_SHUTDOWN);
                if(MA_OK == (err = ma_sensor_msg_create_from_system_sensor_msg(sys_msg, &msg))) {
                    if(MA_OK != (err = ma_sensor_service_post_sensor_msg(sensor, msg))) {
                        MA_LOG(sd->logger, MA_LOG_SEV_ERROR, "posting system sensor event<%d> message failed, errorcode<%d>", MA_SYSTEM_SENSOR_EVENT_SHUTDOWN, err);
                    }
                    (void)ma_sensor_msg_release(msg);
                }
                (void)ma_system_sensor_msg_release(sys_msg);
            }
            set_service_state(sd, SERVICE_STOP_PENDING);           
			ma_io_service_shutdown(sd->io_service);
            return NO_ERROR;
        }

        case SERVICE_CONTROL_STOP: {
            ma_system_sensor_msg_t *sys_msg = NULL;

            MA_LOG(sd->logger,MA_LOG_SEV_INFO,"Stopping Service...");
            MA_LOG(sd->logger, MA_LOG_SEV_ERROR, "posting system sensor event<%d>...", MA_SYSTEM_SENSOR_EVENT_STOP);
            if(MA_OK == (err = ma_system_sensor_msg_create(&sys_msg, 1))) {
                (void)ma_system_sensor_msg_set_event_type(sys_msg, MA_SYSTEM_SENSOR_EVENT_STOP);
                if(MA_OK == (err = ma_sensor_msg_create_from_system_sensor_msg(sys_msg, &msg))) {
                    if(MA_OK != (err = ma_sensor_service_post_sensor_msg(sensor, msg))) {
                        MA_LOG(sd->logger, MA_LOG_SEV_ERROR, "posting system sensor event<%d> message failed, errorcode<%d>", MA_SYSTEM_SENSOR_EVENT_STOP, err);
                    }
                    (void)ma_sensor_msg_release(msg);
                }
                (void)ma_system_sensor_msg_release(sys_msg);
            }
            set_service_state(sd, SERVICE_STOP_PENDING);           
			ma_io_service_stop(sd->io_service);
            return NO_ERROR;
        }

        case SERVICE_CONTROL_POWEREVENT : {
                    MA_LOG(sd->logger, MA_LOG_SEV_INFO, "masvc power event notification...");
            switch(dwEventType) {
                case PBT_APMBATTERYLOW: {
                    MA_LOG(sd->logger, MA_LOG_SEV_DEBUG, "masvc posting battery low notification...");
                    err = post_power_sensor_message(sd, MA_POWER_SENSOR_BATTERY_LOW_EVENT);
                    break;
                }
                case PBT_APMPOWERSTATUSCHANGE: {
                    MA_LOG(sd->logger, MA_LOG_SEV_DEBUG, "masvc posting power status change notification...");
                    err = post_power_sensor_message(sd, MA_POWER_SENSOR_STATUS_CHANGE_EVENT);
                    break;
                }
                case PBT_POWERSETTINGCHANGE: {
                    MA_LOG(sd->logger, MA_LOG_SEV_DEBUG, "masvc posting power setting change notification...");
                    err = post_power_sensor_message(sd, MA_POWER_SENSOR_SETTING_CHANGE_EVENT);
                    break;
                }
				default:
					/*If there are any handling that needs to be done while suspending or entering standby, code should go here adding the 
					corresponding PBT_XXX macro
					*/
					break;
            }
			return NO_ERROR;
        }

        case SERVICE_CONTROL_SESSIONCHANGE: {
            DWORD sessionId = ((WTSSESSION_NOTIFICATION *)lpEventData)->dwSessionId;
            switch (dwEventType)
            {
                case WTS_CONSOLE_CONNECT: {
                    MA_LOG(sd->logger, MA_LOG_SEV_DEBUG, "masvc posting console connect notification...");
                    err = post_user_sensor_message(sd, sessionId, MA_USER_SENSOR_CONSOLE_CONNECT_EVENT, MA_USER_LOGGED_STATE_ON);
                    break;
                }

                case WTS_REMOTE_CONNECT: {
                    MA_LOG(sd->logger, MA_LOG_SEV_DEBUG, "masvc posting remote connect notification...");
                    err = post_user_sensor_message(sd, sessionId, MA_USER_SENSOR_REMOTE_CONNECT_EVENT, MA_USER_LOGGED_STATE_ON);
                    break;
                }

                case WTS_SESSION_LOGON: {
                    MA_LOG(sd->logger, MA_LOG_SEV_DEBUG, "masvc posting logon notification...");
                    err = post_user_sensor_message(sd, sessionId, MA_USER_SENSOR_LOG_ON_EVENT, MA_USER_LOGGED_STATE_ON);
                    break;
                }

                case WTS_SESSION_LOGOFF: {
                    MA_LOG(sd->logger, MA_LOG_SEV_DEBUG, "masvc posting logoff notification...");
                    err = post_user_sensor_message(sd, sessionId, MA_USER_SENSOR_LOG_OFF_EVENT, MA_USER_LOGGED_STATE_OFF);
                    break;
                }
                case WTS_CONSOLE_DISCONNECT: {
                    MA_LOG(sd->logger, MA_LOG_SEV_DEBUG, "masvc posting console disconnect notification...");
                    err = post_user_sensor_message(sd, sessionId, MA_USER_SENSOR_CONSOLE_DISCONNECT_EVENT, MA_USER_LOGGED_STATE_OFF);
                    break;
                }
                case WTS_REMOTE_DISCONNECT: {
                    MA_LOG(sd->logger, MA_LOG_SEV_DEBUG, "masvc posting remote disconnect notification...");
                    err = post_user_sensor_message(sd, sessionId, MA_USER_SENSOR_REMOTE_DISCONNECT_EVENT, MA_USER_LOGGED_STATE_OFF);
                    break;
                }
                case WTS_SESSION_LOCK:
                    //logDetail (UBPLOGGROUP, L"WTS_SESSION_LOCK");
                    break;
                case WTS_SESSION_UNLOCK:
                    //logDetail (UBPLOGGROUP, L"WTS_SESSION_UNLOCK");
                    break;
                case WTS_SESSION_REMOTE_CONTROL:
                    //logDetail (UBPLOGGROUP, L"WTS_SESSION_REMOTE_CONTROL");
                    break;
                default:
                    //logDetail (UBPLOGGROUP, AString () << L"unknown SERVICE_CONTROL_SESSIONCHANGE event type received: " << dwEventType);
                    break;
            }
			return NO_ERROR;
        }
    }

    return ERROR_CALL_NOT_IMPLEMENTED;

}

#define MK_TEXT_TRIPLE(x,fn) {x,#x,fn}

static const char *session_change_details(DWORD dwEventType, LPVOID lpEventData);

static struct service_description service_control_code_text(DWORD dwControl, DWORD dwEventType, LPVOID lpEventData) {
    struct service_description result = {"Unknown Service Control Code",""};
    struct service_control_names {
        DWORD control;
        const char *name;
        const char * (*event_details_text)(DWORD, LPVOID);
    } control_names[] = {
        MK_TEXT_TRIPLE(SERVICE_CONTROL_CONTINUE, 0),
        MK_TEXT_TRIPLE(SERVICE_CONTROL_INTERROGATE, 0),
        MK_TEXT_TRIPLE(SERVICE_CONTROL_NETBINDADD, 0),
        MK_TEXT_TRIPLE(SERVICE_CONTROL_NETBINDDISABLE, 0),
        MK_TEXT_TRIPLE(SERVICE_CONTROL_NETBINDENABLE, 0),
        MK_TEXT_TRIPLE(SERVICE_CONTROL_NETBINDREMOVE, 0),
        MK_TEXT_TRIPLE(SERVICE_CONTROL_PARAMCHANGE, 0),
        MK_TEXT_TRIPLE(SERVICE_CONTROL_PAUSE, 0),
        MK_TEXT_TRIPLE(SERVICE_CONTROL_PRESHUTDOWN, 0),
        MK_TEXT_TRIPLE(SERVICE_CONTROL_SHUTDOWN, 0),
        MK_TEXT_TRIPLE(SERVICE_CONTROL_STOP, 0),
        MK_TEXT_TRIPLE(SERVICE_CONTROL_DEVICEEVENT, 0),
        MK_TEXT_TRIPLE(SERVICE_CONTROL_HARDWAREPROFILECHANGE, 0),
        MK_TEXT_TRIPLE(SERVICE_CONTROL_POWEREVENT, 0),
        MK_TEXT_TRIPLE(SERVICE_CONTROL_SESSIONCHANGE, &session_change_details),
        MK_TEXT_TRIPLE(SERVICE_CONTROL_TIMECHANGE, 0),
        MK_TEXT_TRIPLE(SERVICE_CONTROL_TRIGGEREVENT, 0),
        {0x00000040, "SERVICE_CONTROL_USERMODEREBOOT", 0}
    };

    int i;
    for (i = 0; i!=MA_COUNTOF(control_names);++i) {
        if (control_names[i].control == dwControl) {
            result.event_description = control_names[i].name;
            if (control_names[i].event_details_text) {
                result.event_details = control_names[i].event_details_text(dwEventType, lpEventData);
            }
            break;
        }
    }

    return result;
}


#define MK_TEXT_TUPLE(x) {x,#x}


static const char *session_change_details(DWORD dwEventType, LPVOID lpEventData) {
    struct session_change_names {
        DWORD type;
        const char *name;
    } session_change_names[] = {
        MK_TEXT_TUPLE(WTS_CONSOLE_CONNECT),
        MK_TEXT_TUPLE(WTS_CONSOLE_DISCONNECT),
        MK_TEXT_TUPLE(WTS_REMOTE_CONNECT),
        MK_TEXT_TUPLE(WTS_REMOTE_DISCONNECT),
        MK_TEXT_TUPLE(WTS_SESSION_LOGON),
        MK_TEXT_TUPLE(WTS_SESSION_LOGOFF),
        MK_TEXT_TUPLE(WTS_SESSION_LOCK),
        MK_TEXT_TUPLE(WTS_SESSION_UNLOCK),
        MK_TEXT_TUPLE(WTS_SESSION_REMOTE_CONTROL), /* GetSystemMetrics(SM_REMOTECONTROL, ...)  */
        {0xA, "WTS_SESSION_CREATE"},
        {0xB, "WTS_SESSION_TERMINATE"}
    };
    int i;
    for (i = 0; i!=MA_COUNTOF(session_change_names);++i) {
        if (session_change_names[i].type == dwEventType) {
            return session_change_names[i].name;
        }
    }

    return "Unknown session change event";
}
