#include "ma/internal/apps/masvc/ma_logged_out_controller.h"
#include "ma/ma_common.h"
#include "ma/ma_log.h"
#include "ma/internal/utils/profiler/ma_profiler.h"
#include "ma/internal/utils/profiler/ma_profile_info.h"
#include "ma/internal/services/profile/ma_profile_service.h"
#include "ma/internal/apps/masvc/ma_app_driver.h"
#include "ma/internal/ma_macros.h"

#include <tnt/sessionscope.h>
#include <stdio.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "masvc"

extern ma_logger_t *masvc_logger;

static tnt::ComponentFactoryImpl<logged_out_controller> factory("ma_logged_out_controller");

unsigned logged_out_controller::operator() (tnt::HttpRequest& request, tnt::HttpReply& reply, tnt::QueryParams& qparam) {
    if(strcmp("/logged_out.html", request.getUrl().c_str())) {
        MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "url(%s) is not logged out.html, so ignoring", request.getUrl().c_str());
        return DECLINED;
    }
    string email(request.getCookie("email"));

    if(!email.empty()) {
        reply.clearCookie(string("email"));
        reply.clearSession();
        MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "user email id(%s) logged out successfully", email.c_str());
        reply.setCookie("error", std::string("You have logged out successfully!"), 2);
        reply.setCookie("status", std::string("success"), 200);
    }
    reply.redirect(string("index.html"));
  
    MA_LOG(masvc_logger, MA_LOG_SEV_DEBUG, " logged out handler ends");

    return HTTP_OK;
}

