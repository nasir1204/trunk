#include "ma/internal/apps/masvc/ma_view_operation_controller.h"
#include "ma/internal/defs/ma_booking_defs.h"
#include "ma/internal/utils/booker/ma_booking_info.h"
#include "ma/internal/utils/location/ma_location_info.h"
#include "ma/internal/utils/location/ma_location.h"
#include "ma/internal/utils/booker/ma_booker.h"
#include "ma/ma_common.h"
#include "ma/ma_log.h"
#include "ma/internal/utils/profiler/ma_profiler.h"
#include "ma/internal/utils/profiler/ma_profile_info.h"
#include "ma/internal/services/profile/ma_profile_service.h"
#include "ma/internal/services/mail/ma_mail_service.h"
#include "ma/internal/apps/masvc/ma_app_driver.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/ma_strdef.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_message.h"
#include "ma/internal/utils/threading/ma_atomic.h"
#include "ma/internal/utils/uuid/ma_uuid.h"

#include <tnt/sessionscope.h>
#include <stdio.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "masvc"

extern ma_logger_t *masvc_logger;

static tnt::ComponentFactoryImpl<view_operation_controller> factory("ma_view_operation_controller");

ma_error_t view_operation_controller::process(const char *pincode, ma_variant_t **payload) {
    if(!pincode || !payload)return MA_ERROR_INVALIDARG;
    ma_error_t err = MA_OK;
    ma_msgbus_t *msgbus = NULL;

    if(MA_OK == (err = ma_mail_service_get_msgbus(app_driver::get_instance().get_mail_service(), &msgbus))) {
        ma_msgbus_endpoint_t *endpoint = NULL;
        ma_message_t *request = NULL;
        ma_message_t *response = NULL;

        if(MA_OK == (err = ma_msgbus_endpoint_create(msgbus, MA_LOCATION_SERVICE_NAME_STR, NULL, &endpoint)) &&
            MA_OK == (err = ma_message_create(&request))) {
            ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;

            (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);
            (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
            (void)ma_message_set_property(request, MA_LOCATION_SERVICE_MSG_TYPE, MA_LOCATION_SERVICE_MSG_VALIDATE_PINCODE_TYPE);
            (void)ma_message_set_property(request, MA_LOCATION_ID, pincode);


            if(MA_OK == (err = ma_msgbus_send(endpoint, request, &response))) {
                const char *status = NULL;

                if(MA_OK == (err = ma_message_get_property(response, MA_LOCATION_STATUS, &status))) {
                    ma_json_t *jout = NULL;

                    MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "pincode id(%s) get failed, last error(%d)", pincode, err);
                    if(MA_OK == (err = ma_json_create(&jout))) {
                        if(strcmp(status, MA_LOCATION_ID_EXIST)) {
                            MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "pincode id(%s) does not exist", pincode);
                            (void)ma_json_add_element(jout, MA_LOCATION_STATUS, "failure");
                        } else {
                            MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "pincode id(%s) exist", pincode);
                            (void)ma_json_add_element(jout, MA_LOCATION_STATUS, "success");
                        }
                        ma_bytebuffer_t *buffer = NULL;

                        ma_bytebuffer_create(MA_MAX_BUFFER_LEN, &buffer);
                        if(MA_OK == ma_json_get_data(jout, buffer)) {
                            err = ma_variant_create_from_string((const char*)ma_bytebuffer_get_bytes(buffer), ma_bytebuffer_get_size(buffer), payload);
                        }
                        (void)ma_json_release(jout);
                    }
                }
                (void)ma_message_release(response);
            }

            (void)ma_msgbus_endpoint_release(endpoint);
            (void)ma_message_release(request);
        }
    }
    return err;
}

unsigned view_operation_controller::operator() (tnt::HttpRequest& request, tnt::HttpReply& reply, tnt::QueryParams& qparam) {
    MA_LOG(masvc_logger, MA_LOG_SEV_INFO, "processing pincode operations(%s) code(%s)", request.getBody().c_str(), request.getArg("id").c_str());
    ma_error_t err = MA_OK;
    ma_bool_t res = MA_FALSE;
    ma_json_t *jout = NULL;
    ma_variant_t *payload = NULL;
    const char *id = request.getArg("id").c_str();

    if(MA_OK == (err = process(id, &payload))) {
        ma_buffer_t *buffer = NULL;

        if(MA_OK == (err = ma_variant_get_string_buffer(payload, &buffer))) {
            const char *pstr = NULL;
            size_t len = 0;

            if(MA_OK == (err = ma_buffer_get_string(buffer, &pstr, &len))) {
                if(pstr) {
                    reply.setContentType("Content-Type: application/json");
                    reply.out() << pstr;
                } else
                    MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "pincode id(%s)  empty", id);
            } else
                MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "pincode id(%s) get  failed, last error(%d)",id, err);
            (void)ma_buffer_release(buffer);
        }
         (void)ma_variant_release(payload);
    }

   return HTTP_OK;
}

