#include "ma/internal/apps/masvc/ma_price_controller.h"
#include "ma/internal/defs/ma_booking_defs.h"
#include "ma/internal/defs/ma_general_defs.h"
#include "ma/internal/utils/booker/ma_booking_info.h"
#include "ma/internal/utils/booker/ma_booker.h"
#include "ma/ma_common.h"
#include "ma/ma_log.h"
#include "ma/internal/utils/profiler/ma_profiler.h"
#include "ma/internal/utils/profiler/ma_profile_info.h"
#include "ma/internal/services/profile/ma_profile_service.h"
#include "ma/internal/apps/masvc/ma_app_driver.h"
#include "ma/internal/ma_macros.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_message.h"
#include "ma/internal/utils/threading/ma_atomic.h"
#include "ma/internal/utils/uuid/ma_uuid.h"
#include "ma/internal/utils/json/ma_cjson.h"

#include <tnt/sessionscope.h>
#include <stdio.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "masvc"

extern ma_logger_t *masvc_logger;

struct ma_price_info_s {
    ma_uint32_t distance;
    char type[MA_MAX_LEN+1];
    size_t weight;
    size_t length;
};

ma_error_t ma_price_info_convert_from_json(const char *json_str, ma_price_info_t *pinfo) {
    if(json_str && pinfo) {
        ma_error_t err = MA_OK;
        ma_cjson *json = NULL;
        ma_cjson *j  = NULL;
        
        if((json =  ma_cjson_Parse(json_str))) {
            if(ma_cjson_HasObjectItem(json, "distance")) {
                j = ma_cjson_GetObjectItem(json, "distance");
                if(j->valuestring) {
                    MA_LOG(masvc_logger, MA_LOG_SEV_DEBUG, "distance (%s)", j->valuestring);
                    pinfo->distance = (ma_uint32_t)atoi(j->valuestring);
                }
            }
            if(ma_cjson_HasObjectItem(json, "parcel.type")) {
                j = ma_cjson_GetObjectItem(json, "parcel.type");
                if(j->valuestring) {
                    MA_LOG(masvc_logger, MA_LOG_SEV_DEBUG, "parcel.type (%s)", j->valuestring);
                    strncpy(pinfo->type, j->valuestring, MA_MAX_LEN);
                }
            }
            if(ma_cjson_HasObjectItem(json, "weight")) {
                j = ma_cjson_GetObjectItem(json, "weight");
                if(j->valuestring) {
                    MA_LOG(masvc_logger, MA_LOG_SEV_DEBUG, "weight (%s)", j->valuestring);
                    pinfo->weight = (size_t)atoi(j->valuestring);
                }
            }
            if(ma_cjson_HasObjectItem(json, "length")) {
                j = ma_cjson_GetObjectItem(json, "length");
                if(j->valuestring) {
                    MA_LOG(masvc_logger, MA_LOG_SEV_DEBUG, "length (%s)", j->valuestring);
                    pinfo->length = (size_t)atoi(j->valuestring);
                }
            }
            ma_cjson_Delete(json);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}
static tnt::ComponentFactoryImpl<price_controller> factory("ma_price_controller");

ma_error_t price_controller::process(ma_price_info_t *info, ma_float_t *price) {
    if(info) {
        ma_error_t err = MA_OK;

        if(!strcmp(info->type, MA_PARCEL_STR)) {
        } else if(!strcmp(info->type, MA_COVER_STR)) {
            //flat 30 bugs
            *price = 30.0f;
        } else {
        }
       
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

unsigned price_controller::operator() (tnt::HttpRequest& request, tnt::HttpReply& reply, tnt::QueryParams& qparam) {
    ma_error_t err = MA_OK;
    ma_json_t *jout = NULL;
    ma_price_info_t info = {0};
    ma_float_t price = 0.0;

    MA_LOG(masvc_logger, MA_LOG_SEV_INFO, "processing pricing operation (%s)", request.getBody().c_str());
    if(MA_OK == (err = ma_price_info_convert_from_json(request.getBody().c_str(), &info))) {
        if(MA_OK == (err = process(&info, &price))) {
            if(MA_OK == (err = ma_json_create(&jout))) {
                char price_buf[MA_MAX_LEN+1] = {0};

                MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "price %f", price);
                MA_MSC_SELECT(_snprintf, snprintf)(price_buf, MA_MAX_LEN, "%f", price);
                (void)ma_json_add_element(jout, "price", price_buf);
                ma_bytebuffer_t *buffer = NULL;

                ma_bytebuffer_create(MA_MAX_BUFFER_LEN, &buffer);
                if(MA_OK == ma_json_get_data(jout, buffer)) {
                    reply.setContentType("Content-Type: application/json");
                    reply.out() << (const char*)ma_bytebuffer_get_bytes(buffer);
                }
                (void)ma_json_release(jout);
            }
        }
    }

    return HTTP_OK;
}

