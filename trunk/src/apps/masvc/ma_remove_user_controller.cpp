#include "ma/internal/apps/masvc/ma_remove_user_controller.h"
#include "ma/internal/defs/ma_booking_defs.h"
#include "ma/internal/utils/booker/ma_booking_info.h"
#include "ma/internal/utils/booker/ma_booker.h"
#include "ma/internal/utils/context/ma_context.h"
#include "ma/ma_common.h"
#include "ma/ma_log.h"
#include "ma/internal/utils/profiler/ma_profiler.h"
#include "ma/internal/utils/profiler/ma_profile_info.h"
#include "ma/internal/utils/inventory/ma_inventory_info.h"
#include "ma/internal/services/mail/ma_mail_service.h"
#include "ma/internal/services/profile/ma_profile_service.h"
#include "ma/internal/apps/masvc/ma_app_driver.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/ma_strdef.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_message.h"
#include "ma/internal/utils/threading/ma_atomic.h"
#include "ma/internal/utils/uuid/ma_uuid.h"
#include "ma/scheduler/ma_triggers.h"
#include "ma/scheduler/ma_task.h"
#include "ma/internal/services/scheduler/ma_scheduler_core.h"

#include <tnt/sessionscope.h>
#include <stdio.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "masvc"

extern ma_logger_t *masvc_logger;

static tnt::ComponentFactoryImpl<remove_user_controller> factory("ma_remove_user_controller");

unsigned remove_user_controller::operator() (tnt::HttpRequest& request, tnt::HttpReply& reply, tnt::QueryParams& qparam) {
   if(strcmp("/remove_user.html", request.getUrl().c_str())) {
       MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "url(%s) is not remove_user.html, so ignoring", request.getUrl().c_str());
       return DECLINED;
   }
   string email_id(qparam.arg<string>("email"));
   string email(request.getCookie("email"));

    MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "url(%s) is remove_user.html executing profile(%s)", request.getUrl().c_str(), email.c_str());
   if(email.empty()) {
       reply.setCookie("error", std::string("Session is already expired. Please login again!"), 2);
       reply.redirect(string("index.html"));
       return HTTP_OK;
   }
   MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "HTTP Post request email_id(%s)", email_id.c_str());
  

   ma_error_t err = MA_OK;
   ma_profiler_t *profiler = NULL;
   ma_profile_info_t *info = NULL;
   ma_context_t *context = NULL;
   ma_task_t **task_array = NULL;
   size_t task_array_size = 0;

   (void)ma_booking_service_get_context(app_driver::get_instance().get_booking_service(), &context);
   ma_scheduler_t *scheduler = MA_CONTEXT_GET_SCHEDULER(context);
   ma_msgbus_t *msgbus = NULL;

   (void)ma_mail_service_get_msgbus(app_driver::get_instance().get_mail_service(), &msgbus);

   if(MA_OK == (err = ma_scheduler_enumerate_task_server(scheduler, MA_SOFTWAREID_GENERAL_STR, MA_BOOKING_SERVICE_SECTION_NAME_STR, MA_BOOKING_TASK_TYPE, &task_array, &task_array_size))) {
       for(int i = 0; i < task_array_size; ++i) {
           ma_variant_t *booking_payload = NULL;
           const char *task_id = NULL;

           (void)ma_task_get_id(task_array[i], &task_id);
           if(MA_OK == (err = ma_task_get_app_payload(task_array[i], &booking_payload))) {
               ma_booking_info_t *book_info = NULL;

               if(MA_OK == (err = ma_booking_info_convert_from_variant(booking_payload, &book_info))) {
                   char picker_id[MA_MAX_LEN+1] = {0};
                   char user_id[MA_MAX_LEN+1] = {0};


                   (void)ma_booking_info_get_picker_email_id(book_info, picker_id);
                   (void)ma_booking_info_get_email_id(book_info, user_id);
                   if(!strcmp(picker_id, email_id.c_str())) {
                       ma_booking_info_status_t book_status = MA_BOOKING_INFO_STATUS_NOT_ASSIGNED;

                       (void)ma_booking_info_get_status(book_info, &book_status);
                       if(MA_BOOKING_INFO_STATUS_ASSIGNED == book_status) {
                           (void)ma_booking_info_set_status(book_info, MA_BOOKING_INFO_STATUS_NOT_ASSIGNED);
                           ma_variant_t *b_var = NULL;

                           if(MA_OK == (err = ma_booking_info_convert_to_variant(book_info, &b_var))) {
                               ma_msgbus_endpoint_t *endpoint = NULL;
                               ma_message_t *request = NULL;
                               ma_message_t *response = NULL;

                               if(MA_OK == (err = ma_msgbus_endpoint_create(msgbus, MA_BOOKING_SERVICE_NAME_STR, NULL, &endpoint)) &&
                                  MA_OK == (err = ma_message_create(&request))) {
                                   ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;

                                   (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);
                                   (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                                   (void)ma_message_set_property(request, MA_BOOKING_SERVICE_MSG_TYPE, MA_BOOKING_SERVICE_MSG_ORDER_MODIFY_TYPE);
                                   (void)ma_message_set_property(request, MA_BOOKING_ID, user_id);
                                   (void)ma_message_set_payload(request, b_var);
                                   if(MA_OK == (err = ma_msgbus_send(endpoint, request, &response))) {
                                       const char *stats = NULL;

                                       (void)ma_message_get_property(response, MA_BOOKING_STATUS, &stats);
                                       if(!strcmp(stats, MA_BOOKING_ID_WRITTEN)) {
                                           MA_LOG(masvc_logger, MA_LOG_SEV_INFO, "Booking id(%s) updated successfully", task_id);
                                       } else
                                           MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "Booking id(%s) update failed", task_id);
                                       (void)ma_message_release(response);
                                   }
                                   (void)ma_msgbus_endpoint_release(endpoint);
                                   (void)ma_message_release(request);
                               }
                               (void)ma_task_set_app_payload(task_array[i], b_var);
                               (void)ma_variant_release(b_var);
                           }
                       }
                   }
                   (void)ma_booking_info_release(book_info);
               }
               (void)ma_variant_release(booking_payload);
           }
       }
       (void)ma_scheduler_release_task_set(task_array);
   }
   if(MA_OK == (err = ma_profile_service_get_profiler(app_driver::get_instance().get_profile_service(), &profiler))) {
       if(MA_OK == (err = ma_profiler_get(profiler, email_id.c_str(), &info))) {
           ma_profile_info_user_type_t type = MA_PROFILE_INFO_USER_TYPE_NORMAL;

           (void)ma_profile_info_get_user_type(info, &type);
           (void)ma_profile_info_release(info);
           if(MA_PROFILE_INFO_USER_TYPE_ADMIN == type) {
               MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "Admin user cannot be deleted");
               reply.setCookie("error", std::string("Admin user cannot be deleted!"), 2);
               reply.redirect(string("logged_on.html"));
               return HTTP_OK;
           } 
       } else {
           MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "user does not exist");
           reply.setCookie("error", std::string("Profile does not exist!"), 2);
           reply.redirect(string("logged_on.html"));
           return HTTP_OK;
       }
   }
    
   if(msgbus) {
       ma_msgbus_endpoint_t *endpoint = NULL;
       ma_message_t *request = NULL;
       ma_message_t *response = NULL;

       if(MA_OK == (err = ma_msgbus_endpoint_create(msgbus, MA_PROFILE_SERVICE_NAME_STR, NULL, &endpoint)) &&
          MA_OK == (err = ma_message_create(&request))) {
           ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;

           (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);
           (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
           (void)ma_message_set_property(request, MA_PROFILE_SERVICE_MSG_TYPE, MA_PROFILE_SERVICE_MSG_UNREGISTER_TYPE);
           (void)ma_message_set_property(request, MA_PROFILE_ID, email_id.c_str());
           if(MA_OK == (err = ma_msgbus_send(endpoint, request, &response))) {
               const char *reply_status = NULL;

               (void)ma_message_get_property(response, MA_PROFILE_STATUS, &reply_status);
               if(!strcmp(reply_status, MA_PROFILE_ID_DELETED)) {
                   ma_msgbus_endpoint_t *pendpoint = NULL;
                   ma_message_t *prequest = NULL;
                   ma_message_t *presponse = NULL;

                   MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "sending confirmation mail to email id(%s)", email_id.c_str());
                   if(MA_OK == (err = ma_msgbus_endpoint_create(msgbus, MA_MAIL_SERVICE_NAME_STR, NULL, &pendpoint)) &&
                      MA_OK == (err = ma_message_create(&prequest))) {
                      ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;
                      const char *sub = "Unregistration mileaccess.com";

                      (void)ma_msgbus_endpoint_setopt(pendpoint, MSGBUSOPT_THREADMODEL, thread_option);
                      (void)ma_msgbus_endpoint_setopt(pendpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                      (void)ma_message_set_property(prequest, MA_MAIL_SERVICE_MSG_TYPE, MA_MAIL_SERVICE_MSG_SEND_TYPE);
                      (void)ma_message_set_property(prequest, MA_MAIL_ID_FROM, MA_EMAIL_ID);
                      (void)ma_message_set_property(prequest, MA_MAIL_ID_TO, email_id.c_str());      
                      (void)ma_message_set_property(prequest, MA_MAIL_SUBJECT, sub);                
                      char envelope[MA_MAX_BUFFER_LEN+1] = {0};

                      MA_MSC_SELECT(_snprintf, snprintf)(envelope, MA_MAX_BUFFER_LEN, "Dear %s,\n\nWe have successfully unsubscribed your mail id. We expect that you will reconnect with us soon.\n\nHappy Booking!\nRegards,\ncs@mileaccess.com", email_id.c_str());
                      ma_variant_t *payload = NULL;

                      if(MA_OK == (err = ma_variant_create_from_string(envelope, strlen(envelope), &payload))) {
                          (void)ma_message_set_payload(prequest, payload);
                          if(MA_OK == (err = ma_msgbus_send(pendpoint, prequest, &presponse))) {
                              (void)ma_message_get_property(presponse, MA_MAIL_STATUS, &reply_status);
                              if(!strcmp(reply_status, MA_MAIL_SEND_SUCCESS)) {
                                  ma_msgbus_endpoint_t *bendpoint = NULL;
                                  ma_message_t *brequest = NULL;
                                  ma_message_t *bresponse = NULL;

                                  if(MA_OK == (err = ma_msgbus_endpoint_create(msgbus, MA_BOOKING_SERVICE_NAME_STR, NULL, &bendpoint)) &&
                                     MA_OK == (err = ma_message_create(&brequest))) {
                                      (void)ma_msgbus_endpoint_setopt(bendpoint, MSGBUSOPT_THREADMODEL, thread_option);
                                      (void)ma_msgbus_endpoint_setopt(bendpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                                      (void)ma_message_set_property(brequest, MA_BOOKING_SERVICE_MSG_TYPE, MA_BOOKING_SERVICE_MSG_BOOKING_DELETE_TYPE);
                                      (void)ma_message_set_property(brequest, MA_BOOKING_ID, email_id.c_str());
                                      if(MA_OK == (err = ma_msgbus_send(bendpoint, brequest, &bresponse))) {
                                          ma_msgbus_endpoint_t *iendpoint = NULL;
                                          ma_message_t *irequest = NULL;

                                          if(MA_OK == (err = ma_msgbus_endpoint_create(msgbus, MA_INVENTORY_SERVICE_NAME_STR, NULL, &iendpoint)) &&
                                             MA_OK == (err = ma_message_create(&irequest))) {
                                              (void)ma_msgbus_endpoint_setopt(iendpoint, MSGBUSOPT_THREADMODEL, thread_option);
                                              (void)ma_msgbus_endpoint_setopt(iendpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                                              (void)ma_message_set_property(irequest, MA_INVENTORY_SERVICE_MSG_TYPE, MA_INVENTORY_SERVICE_MSG_DEREGISTER);
                                              (void)ma_message_set_property(irequest, MA_INVENTORY_ID, email_id.c_str());
                                              if(MA_OK == (err = ma_msgbus_send_and_forget(iendpoint, irequest))) {
                                                  MA_LOG(masvc_logger, MA_LOG_SEV_INFO, "Inventory id(%s) removed successfully", email_id.c_str());
                                              }
                                              (void)ma_msgbus_endpoint_release(iendpoint);
                                              (void)ma_message_release(irequest);
                                          }

                                          MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "profile id(%s) booking information deleted successfully", email_id.c_str());
                                          reply.setCookie("error", std::string("Profile and booking information deleted successfully!"), 3);
                                          (void)ma_message_release(bresponse);
                                      }
                                      (void)ma_message_release(brequest);
                                      (void)ma_msgbus_endpoint_release(bendpoint);
                                  }
                              }
                              (void)ma_message_release(presponse);
                          }
                          (void)ma_variant_release(payload);
                      }
                      (void)ma_message_release(prequest);
                      (void)ma_msgbus_endpoint_release(pendpoint);
                   }
               } else {
                   reply.setCookie("error", std::string("Profile information does not exist!"), 3);
               }
               (void)ma_message_release(response);
           }
           (void)ma_message_release(request);
           (void)ma_msgbus_endpoint_release(endpoint);
       }
   } 
    MA_LOG(masvc_logger, MA_LOG_SEV_DEBUG, " remove user handler ends");
    reply.redirect(std::string("logged_on.html"));

    return HTTP_OK;
}

