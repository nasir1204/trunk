#include "ma/internal/apps/masvc/ma_delete_order_controller.h"
#include "ma/internal/defs/ma_booking_defs.h"
#include "ma/internal/utils/booker/ma_booking_info.h"
#include "ma/internal/utils/booker/ma_booker.h"
#include "ma/ma_common.h"
#include "ma/ma_log.h"
#include "ma/internal/utils/profiler/ma_profiler.h"
#include "ma/internal/utils/profiler/ma_profile_info.h"
#include "ma/internal/services/profile/ma_profile_service.h"
#include "ma/internal/services/mail/ma_mail_service.h"
#include "ma/internal/apps/masvc/ma_app_driver.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/ma_strdef.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_message.h"
#include "ma/internal/utils/threading/ma_atomic.h"
#include "ma/internal/utils/uuid/ma_uuid.h"

#include <tnt/sessionscope.h>
#include <stdio.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "masvc"

extern ma_logger_t *masvc_logger;

static tnt::ComponentFactoryImpl<delete_order_controller> factory("ma_delete_order_controller");

static ma_error_t del(const char *id, ma_variant_t **payload) {
    if(payload && id) {
        ma_error_t err = MA_OK;
            ma_msgbus_t *msgbus = NULL;

            if(MA_OK == (err = ma_mail_service_get_msgbus(app_driver::get_instance().get_mail_service(), &msgbus))) {
                ma_msgbus_endpoint_t *endpoint = NULL;
                ma_message_t *request = NULL;
                ma_message_t *response = NULL;
 
                if(MA_OK == (err = ma_msgbus_endpoint_create(msgbus, MA_BOOKING_SERVICE_NAME_STR, NULL, &endpoint)) &&
                   MA_OK == (err = ma_message_create(&request))) {
                    ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;

                    (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);
                    (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                    (void)ma_message_set_property(request, MA_BOOKING_SERVICE_MSG_TYPE, MA_BOOKING_SERVICE_MSG_ORDER_DELETE_TYPE);
                    (void)ma_message_set_property(request, MA_BOOKING_ID, id);
                    if(MA_OK == (err = ma_msgbus_send(endpoint, request, &response))) {
                        const char *status = NULL;

                        (void)ma_message_get_property(response, MA_BOOKING_STATUS, &status);
                        *payload = NULL;
                        if(!strcmp(status, MA_BOOKING_ID_DELETED)) {
                            MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "booking id(%s) delete successfull", id);
                        } else {
                            MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "booking id(%s) delete failed", id);
                        }
                        const char *empty = "{}";
                        err = ma_variant_create_from_string(empty, strlen(empty), payload);
                        (void)ma_message_release(response);
                    }

                    (void)ma_message_release(request);
                    (void)ma_msgbus_endpoint_release(endpoint);
                }
            }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}
unsigned delete_order_controller::operator() (tnt::HttpRequest& request, tnt::HttpReply& reply, tnt::QueryParams& qparam) {
    ma_error_t err = MA_OK;
    ma_variant_t *pin_payload = NULL;
    const char *id = NULL;

    MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "processing delete order operation (%s)", id = request.getArg("id").c_str());
    if(MA_OK == (err = del(id, &pin_payload))) {
        ma_buffer_t *buffer = NULL;

        if(MA_OK == (err = ma_variant_get_string_buffer(pin_payload, &buffer))) {
            const char *pstr = NULL;
            size_t len = 0;

            if(MA_OK == (err = ma_buffer_get_string(buffer, &pstr, &len))) {
                if(pstr) {
                    reply.setContentType("Content-Type: application/json");
                    reply.out() << pstr;
                } else
                    MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "booking id(%s) does not exist", id);
            } else
                MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "booking id(%s) delete failed, last error(%d)", id, err);
            (void)ma_buffer_release(buffer);
        }
        (void)ma_variant_release(pin_payload);
    }
    return HTTP_OK;
}

