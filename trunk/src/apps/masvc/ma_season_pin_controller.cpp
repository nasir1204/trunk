#include "ma/internal/apps/masvc/ma_season_pin_controller.h"
#include "ma/internal/apps/masvc/ma_create_order_controller.h"
#include "ma/ma_common.h"
#include "ma/ma_log.h"
#include "ma/internal/utils/profiler/ma_profiler.h"
#include "ma/internal/utils/board/ma_board.h"
#include "ma/internal/utils/board/ma_board_info.h"
#include "ma/internal/utils/profiler/ma_profile_info.h"
#include "ma/internal/utils/location/ma_location_info.h"
#include "ma/internal/services/profile/ma_profile_service.h"
#include "ma/internal/services/mail/ma_mail_service.h"
#include "ma/internal/utils/json/ma_json_utils.h"
#include "ma/internal/utils/json/ma_cjson.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/defs/ma_mail_defs.h"
#include "ma/internal/utils/inventory/ma_inventory_info.h"
#include "ma/internal/utils/season/ma_season_info.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/apps/masvc/ma_app_driver.h"
#include "ma/internal/defs/ma_general_defs.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_message.h"


#include <assert.h>
#include <stdio.h>
#include <string>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "masvc"

extern ma_logger_t *masvc_logger;


static const char *gseason_operations[] = {
#define MA_SEASON_PIN_OPERATION_EXPAND(status, index, status_str) status_str,
    MA_SEASON_PIN_OPERATION_EXPANDER
#undef MA_SEASON_PIN_OPERATION_EXPAND
};

static tnt::ComponentFactoryImpl<season_pin_controller> factory("ma_season_pin_controller");
static ma_error_t location_register_handler(ma_location_info_t *linfo, const char *contact);
static ma_error_t location_update_handler(ma_location_info_t *linfo, const char *contact);
static ma_error_t location_delete_contact_handler(ma_location_info_t *linfo, const char *contact);

using namespace std;

ma_season_pin_operation_type_t season_pin_controller::get_op_type(const char *op) {
    ma_season_pin_operation_type_t type = MA_SEASON_PIN_OPERATION_UNKNOWN;

    if(op) {
        for(size_t i = 0; i < MA_COUNTOF(gseason_operations); ++i) {
            MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "season pin operation(%s) ", gseason_operations[i]);
            if(!strcmp(gseason_operations[i], op)) {
                MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "season pin operation(%s) ", gseason_operations[i]);
                type = static_cast<ma_season_pin_operation_type_t>(i+1);
                break;
            }
        }
        if(MA_SEASON_PIN_OPERATION_UNKNOWN == type) {
            MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "season pin operation(%s) is invalid ", op);
        }
    }

    return type;
}

const char* season_pin_controller::get_op_str(ma_season_pin_operation_type_t type) {
    if(type <= MA_SEASON_PIN_OPERATION_UNKNOWN) {
        return gseason_operations[type-1];
    }
    return NULL;
}

static ma_error_t send_add_pin_request(ma_variant_t *payload, const char *id) {
    if(payload && id) {
        ma_error_t err = MA_OK;
            ma_msgbus_t *msgbus = NULL;

            if(MA_OK == (err = ma_mail_service_get_msgbus(app_driver::get_instance().get_mail_service(), &msgbus))) {
                ma_msgbus_endpoint_t *endpoint = NULL;
                ma_message_t *request = NULL;
                ma_message_t *response = NULL;
 
                if(MA_OK == (err = ma_msgbus_endpoint_create(msgbus, MA_SEASON_SERVICE_NAME_STR, NULL, &endpoint)) &&
                   MA_OK == (err = ma_message_create(&request))) {
                    ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;

                    (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);
                    (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                    (void)ma_message_set_property(request, MA_SEASON_SERVICE_MSG_TYPE, MA_SEASON_SERVICE_MSG_REGISTER_TYPE);
                    (void)ma_message_set_property(request, MA_SEASON_ID, id);
                    if(MA_OK == (err = ma_message_set_payload(request, payload))) {
#if 1
                        if(MA_OK == (err = ma_msgbus_send(endpoint, request, &response))) {
                            const char *status = NULL;

                            (void)ma_message_get_property(response, MA_SEASON_STATUS, &status);
                            if(!strcmp(status, MA_SEASON_ID_WRITTEN)) {
                                MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "season id(%s) written successfully", id);
                            } else {
                                MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "season id(%s) write failed", id);
                            }
                            (void)ma_message_release(response);
                        }
#endif
#if 0
                        if(MA_OK == (err = ma_msgbus_send_and_forget(endpoint, request))) {
                            MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "season id(%s) written successfully", id);
                            (void)ma_message_release(response);
                        }
#endif
                        (void)ma_variant_release(payload);
                    }

                    (void)ma_message_release(request);
                    (void)ma_msgbus_endpoint_release(endpoint);
                }
            }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t send_get_by_day_pin_request(const char *id, const char *day, ma_variant_t **payload) {
    if(payload && id) {
        ma_error_t err = MA_OK;
            ma_msgbus_t *msgbus = NULL;

            if(MA_OK == (err = ma_mail_service_get_msgbus(app_driver::get_instance().get_mail_service(), &msgbus))) {
                ma_msgbus_endpoint_t *endpoint = NULL;
                ma_message_t *request = NULL;
                ma_message_t *response = NULL;
 
                if(MA_OK == (err = ma_msgbus_endpoint_create(msgbus, MA_SEASON_SERVICE_NAME_STR, NULL, &endpoint)) &&
                   MA_OK == (err = ma_message_create(&request))) {
                    ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;

                    (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);
                    (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                    (void)ma_message_set_property(request, MA_SEASON_SERVICE_MSG_TYPE, MA_SEASON_SERVICE_MSG_GET_BY_DAY_TYPE);
                    (void)ma_message_set_property(request, MA_SEASON_ID, id);
                    (void)ma_message_set_property(request, MA_SEASON_WHICH_DAY, day);
                    if(MA_OK == (err = ma_msgbus_send(endpoint, request, &response))) {
                        const char *status = NULL;

                        (void)ma_message_get_property(response, MA_SEASON_STATUS, &status);
                        *payload = NULL;
                        if(!strcmp(status, MA_SEASON_OP_SUCCESS)) {
                            ma_variant_t *res_payload = NULL;

                            MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "profile id(%s) get successfully", id);
                            if(MA_OK == (err = ma_message_get_payload(response, &res_payload))) {
                                err = ma_variant_add_ref(*payload = res_payload);
                                (void)ma_variant_release(res_payload);
                            }
                        } else {
                            MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "profile id(%s) get failed", id);
                        }
                        (void)ma_message_release(response);
                    }

                    (void)ma_message_release(request);
                    (void)ma_msgbus_endpoint_release(endpoint);
                }
            }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}
static ma_error_t send_get_pin_request(const char *id, ma_variant_t **payload) {
    if(payload && id) {
        ma_error_t err = MA_OK;
            ma_msgbus_t *msgbus = NULL;

            if(MA_OK == (err = ma_mail_service_get_msgbus(app_driver::get_instance().get_mail_service(), &msgbus))) {
                ma_msgbus_endpoint_t *endpoint = NULL;
                ma_message_t *request = NULL;
                ma_message_t *response = NULL;
 
                if(MA_OK == (err = ma_msgbus_endpoint_create(msgbus, MA_SEASON_SERVICE_NAME_STR, NULL, &endpoint)) &&
                   MA_OK == (err = ma_message_create(&request))) {
                    ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;

                    (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);
                    (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                    (void)ma_message_set_property(request, MA_SEASON_SERVICE_MSG_TYPE, MA_SEASON_SERVICE_MSG_GET_TYPE);
                    (void)ma_message_set_property(request, MA_SEASON_ID, id);
                    if(MA_OK == (err = ma_msgbus_send(endpoint, request, &response))) {
                        const char *status = NULL;

                        (void)ma_message_get_property(response, MA_SEASON_STATUS, &status);
                        *payload = NULL;
                        if(!strcmp(status, MA_SEASON_OP_SUCCESS)) {
                            ma_variant_t *res_payload = NULL;

                            MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "profile id(%s) get successfully", id);
                            if(MA_OK == (err = ma_message_get_payload(response, &res_payload))) {
                                err = ma_variant_add_ref(*payload = res_payload);
                                (void)ma_variant_release(res_payload);
                            }
                        } else {
                            MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "profile id(%s) get failed", id);
                        }
                        (void)ma_message_release(response);
                    }

                    (void)ma_message_release(request);
                    (void)ma_msgbus_endpoint_release(endpoint);
                }
            }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t send_get_all_board_subscriptions_request(const char *id, ma_variant_t **payload) {
    if(payload && id) {
        ma_error_t err = MA_OK;
            ma_msgbus_t *msgbus = NULL;

            if(MA_OK == (err = ma_mail_service_get_msgbus(app_driver::get_instance().get_mail_service(), &msgbus))) {
                ma_msgbus_endpoint_t *endpoint = NULL;
                ma_message_t *request = NULL;
                ma_message_t *response = NULL;
 
                if(MA_OK == (err = ma_msgbus_endpoint_create(msgbus, MA_BOARD_SERVICE_NAME_STR, NULL, &endpoint)) &&
                   MA_OK == (err = ma_message_create(&request))) {
                    ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;

                    (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);
                    (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                    (void)ma_message_set_property(request, MA_BOARD_SERVICE_MSG_TYPE, MA_BOARD_SERVICE_MSG_GET_BOARD_SELECTION);
                    if(MA_OK == (err = ma_msgbus_send(endpoint, request, &response))) {
                        const char *status = NULL;

                        (void)ma_message_get_property(response, MA_BOARD_STATUS, &status);
                        *payload = NULL;
                        if(!strcmp(status, MA_BOARD_ID_WRITTEN)) {
                            ma_variant_t *res_payload = NULL;

                            MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "profile id(%s) board get all subscription successfully", id);
                            if(MA_OK == (err = ma_message_get_payload(response, &res_payload))) {
                                err = ma_variant_add_ref(*payload = res_payload);
                                (void)ma_variant_release(res_payload);
                            }
                        } else {
                            MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "profile id(%s) get failed", id);
                        }
                        (void)ma_message_release(response);
                    }

                    (void)ma_message_release(request);
                    (void)ma_msgbus_endpoint_release(endpoint);
                }
            }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t send_add_board_request(const char *id, ma_variant_t *payload) {
    if(payload && id) {
        ma_error_t err = MA_OK;
            ma_msgbus_t *msgbus = NULL;

            if(MA_OK == (err = ma_mail_service_get_msgbus(app_driver::get_instance().get_mail_service(), &msgbus))) {
                ma_msgbus_endpoint_t *endpoint = NULL;
                ma_message_t *request = NULL;
                ma_message_t *response = NULL;
 
                if(MA_OK == (err = ma_msgbus_endpoint_create(msgbus, MA_BOARD_SERVICE_NAME_STR, NULL, &endpoint)) &&
                   MA_OK == (err = ma_message_create(&request))) {
                    ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;

                    (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);
                    (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                    (void)ma_message_set_property(request, MA_BOARD_SERVICE_MSG_TYPE, MA_BOARD_SERVICE_MSG_ADD_BOARD);
                    (void)ma_message_set_payload(request, payload);
                    if(MA_OK == (err = ma_msgbus_send(endpoint, request, &response))) {
                        const char *status = NULL;

                        (void)ma_message_get_property(response, MA_BOARD_STATUS, &status);
                        if(!strcmp(status, MA_BOARD_ID_WRITTEN)) {
                            MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "board id(%s) add successful", id);
                        } else {
                            MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "board id(%s) add failed", id);
                        }
                        (void)ma_message_release(response);
                    }

                    (void)ma_message_release(request);
                    (void)ma_msgbus_endpoint_release(endpoint);
                }
            }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t send_get_profile_request(const char *id, ma_variant_t **payload) {
    if(payload && id) {
        ma_error_t err = MA_OK;
            ma_msgbus_t *msgbus = NULL;

            if(MA_OK == (err = ma_mail_service_get_msgbus(app_driver::get_instance().get_mail_service(), &msgbus))) {
                ma_msgbus_endpoint_t *endpoint = NULL;
                ma_message_t *request = NULL;
                ma_message_t *response = NULL;
 
                if(MA_OK == (err = ma_msgbus_endpoint_create(msgbus, MA_PROFILE_SERVICE_NAME_STR, NULL, &endpoint)) &&
                   MA_OK == (err = ma_message_create(&request))) {
                    ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;

                    (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);
                    (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                    (void)ma_message_set_property(request, MA_PROFILE_SERVICE_MSG_TYPE, MA_PROFILE_SERVICE_MSG_GET_JSON_TYPE);
                    (void)ma_message_set_property(request, MA_PROFILE_ID, id);
                    if(MA_OK == (err = ma_msgbus_send(endpoint, request, &response))) {
                        const char *status = NULL;

                        (void)ma_message_get_property(response, MA_PROFILE_STATUS, &status);
                        *payload = NULL;
                        if(!strcmp(status, MA_PROFILE_ID_EXIST)) {
                            ma_variant_t *res_payload = NULL;

                            MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "profile id(%s) get successfull", id);
                            if(MA_OK == (err = ma_message_get_payload(response, &res_payload))) {
                                err = ma_variant_add_ref(*payload = res_payload);
                                (void)ma_variant_release(res_payload);
                            }
                        } else {
                            ma_json_t *jout = NULL;

                            if(MA_OK == (err = ma_json_create(&jout))) {
                                (void)ma_json_add_element(jout, MA_PROFILE_INFO_ATTR_STATUS, "failure");
                                ma_bytebuffer_t *buffer = NULL;

                                ma_bytebuffer_create(MA_MAX_BUFFER_LEN, &buffer);
                                if(MA_OK == ma_json_get_data(jout, buffer)) {
                                    err = ma_variant_create_from_string((const char*)ma_bytebuffer_get_bytes(buffer), ma_bytebuffer_get_size(buffer), payload);
                                }
                                (void)ma_json_release(jout);
                            }

                            MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "profile id(%s) get failed", id);
                        }
                        (void)ma_message_release(response);
                    }

                    (void)ma_message_release(request);
                    (void)ma_msgbus_endpoint_release(endpoint);
                }
            }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t send_query_request(const char *id, ma_variant_t **payload) {
    if(payload && id) {
        ma_error_t err = MA_OK;
            ma_msgbus_t *msgbus = NULL;

            if(MA_OK == (err = ma_mail_service_get_msgbus(app_driver::get_instance().get_mail_service(), &msgbus))) {
                ma_msgbus_endpoint_t *endpoint = NULL;
                ma_message_t *request = NULL;
                ma_message_t *response = NULL;
 
                if(MA_OK == (err = ma_msgbus_endpoint_create(msgbus, MA_SEARCH_SERVICE_NAME_STR, NULL, &endpoint)) &&
                   MA_OK == (err = ma_message_create(&request))) {
                    ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;

                    (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);
                    (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                    (void)ma_message_set_property(request, MA_SEARCH_SERVICE_MSG_TYPE, MA_SEARCH_SERVICE_MSG_GET_TYPE);
                    (void)ma_message_set_property(request, MA_SEARCH_ID, id);
                    if(MA_OK == (err = ma_msgbus_send(endpoint, request, &response))) {
                        const char *status = NULL;

                        (void)ma_message_get_property(response, MA_SEARCH_STATUS, &status);
                        *payload = NULL;
                        if(!strcmp(status, MA_SEARCH_OP_SUCCESS)) {
                            ma_variant_t *res_payload = NULL;

                            MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "search id(%s) query successfully", id);
                            if(MA_OK == (err = ma_message_get_payload(response, &res_payload))) {
                                err = ma_variant_add_ref(*payload = res_payload);
                                (void)ma_variant_release(res_payload);
                            }
                        } else {
                            MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "search id(%s) query failed", id);
                        }
                        (void)ma_message_release(response);
                    }

                    (void)ma_message_release(request);
                    (void)ma_msgbus_endpoint_release(endpoint);
                }
            }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t process_subscribe_board_request(ma_profile_info_t *pinfo) {
    if(!pinfo)return MA_ERROR_INVALIDARG;
    ma_error_t err = MA_OK;
    ma_profile_info_t *info = NULL;
    char email[MA_MAX_LEN+1] = {0};
    char contact[MA_MAX_LEN+1] = {0};
    char user[MA_MAX_NAME+1] = {0};

    (void)ma_profile_info_get_email_id(pinfo, email); 
    (void)ma_profile_info_get_contact(pinfo, contact); 
    (void)ma_profile_info_get_user(pinfo, user); 
    MA_LOG(masvc_logger, MA_LOG_SEV_INFO, "Profile id(%s) does not exist in DB, creating...", email);
    ma_variant_t *info_var = NULL;

    if(MA_OK == (err = ma_profile_info_convert_to_variant(info = pinfo, &info_var))) {
        ma_msgbus_t *msgbus = NULL;
        (void)ma_profiler_set_logger(masvc_logger);

        if(MA_OK == (err = ma_mail_service_get_msgbus(app_driver::get_instance().get_mail_service(), &msgbus))) {
            ma_msgbus_endpoint_t *pendpoint = NULL;
            ma_message_t *prequest = NULL;
            ma_message_t *presponse = NULL;

            if(MA_OK == (err = ma_msgbus_endpoint_create(msgbus, MA_PROFILE_SERVICE_NAME_STR, NULL, &pendpoint)) &&
               MA_OK == (err = ma_message_create(&prequest))) {
                ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;

                (void)ma_msgbus_endpoint_setopt(pendpoint, MSGBUSOPT_THREADMODEL, thread_option);
                (void)ma_msgbus_endpoint_setopt(pendpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                (void)ma_message_set_property(prequest, MA_PROFILE_SERVICE_MSG_TYPE, MA_PROFILE_SERVICE_MSG_UPDATE_TYPE);
                (void)ma_message_set_property(prequest, MA_PROFILE_ID, contact);
                if(MA_OK == (err = ma_message_set_payload(prequest, info_var))) {
                    if(MA_OK == (err = ma_msgbus_send(pendpoint, prequest, &presponse))) {
                        const char *status = NULL;

                        (void)ma_message_get_property(presponse, MA_PROFILE_STATUS, &status);
                        if(!strcmp(status, MA_PROFILE_ID_WRITTEN)) {
                            MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "profile info written successfully into profile DB");
                        } else {
                            MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "profile info write failed");
                        }
                        (void)ma_message_release(presponse);
                    } else
                        MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "profile registration failed, last error(%d)", err);
                    (void)ma_variant_release(info_var);
                }
                (void)ma_message_release(prequest);
                (void)ma_msgbus_endpoint_release(pendpoint);
            }
        }
        (void)ma_variant_release(info_var);
    } else
        MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "profile info variant conversion failed, last error(%d)", err);

    return err;
}

static ma_error_t profile_get_handler(const char *email, ma_profile_info_t **pinfo) {
    if(!email && !pinfo)return MA_ERROR_INVALIDARG;
    ma_error_t err = MA_OK;

    ma_msgbus_t *msgbus = NULL;
    (void)ma_profiler_set_logger(masvc_logger);

    if(MA_OK == (err = ma_mail_service_get_msgbus(app_driver::get_instance().get_mail_service(), &msgbus))) {
        ma_msgbus_endpoint_t *pendpoint = NULL;
        ma_message_t *prequest = NULL;
        ma_message_t *presponse = NULL;

        if(MA_OK == (err = ma_msgbus_endpoint_create(msgbus, MA_PROFILE_SERVICE_NAME_STR, NULL, &pendpoint)) &&
           MA_OK == (err = ma_message_create(&prequest))) {
            ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;

            (void)ma_msgbus_endpoint_setopt(pendpoint, MSGBUSOPT_THREADMODEL, thread_option);
            (void)ma_msgbus_endpoint_setopt(pendpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
            (void)ma_message_set_property(prequest, MA_PROFILE_SERVICE_MSG_TYPE, MA_PROFILE_SERVICE_MSG_GET_TYPE);
            (void)ma_message_set_property(prequest, MA_PROFILE_ID, email);
            if(MA_OK == (err = ma_msgbus_send(pendpoint, prequest, &presponse))) {
                const char *status = NULL;

                (void)ma_message_get_property(presponse, MA_PROFILE_STATUS, &status);
                if(!strcmp(status, MA_PROFILE_ID_EXIST)) {
                    ma_variant_t *info_var = NULL;
                     
                    MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "profile info get successfull");
                    if(MA_OK == (err = ma_message_get_payload(presponse, &info_var))) {
 
                        if(MA_OK != (err = ma_profile_info_convert_from_variant(info_var, pinfo))) {
                            MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "profile convert from variant failed, last error(%d)", err);
                        }
                        (void)ma_variant_release(info_var);
                    }
                } else {
                    MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "profile info get failed");
                }
                (void)ma_message_release(presponse);
            } else
                MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "profile get failed, last error(%d)", err);
            (void)ma_message_release(prequest);
            (void)ma_msgbus_endpoint_release(pendpoint);
        }
    }

    return err;
}

static ma_error_t profile_delete_hanlder(const char *contact, ma_bool_t *res) {
    if(!contact || !res)return MA_ERROR_INVALIDARG;
    ma_error_t err = MA_OK;
    char pincode[MA_MAX_LEN+1] = {0};
    ma_profile_info_t *profile = NULL;

    if(MA_OK == (err = profile_get_handler(contact, &profile))) {
        ma_profile_info_user_type_t type = MA_PROFILE_INFO_USER_TYPE_CUSTOMER_EXECUTIVE;
        (void)ma_profile_info_get_user_type(profile, &type);
        if(type == MA_PROFILE_INFO_USER_TYPE_OPERATION) {
            ma_location_info_t *linfo = NULL;

            (void)ma_profile_info_get_source_pincode(profile, pincode);
            if(MA_OK == (err = ma_location_info_create(&linfo))) {
                (void)ma_location_info_set_pincode(linfo, pincode);
                (void)ma_location_info_add_contact(linfo, pincode,contact);
                if(MA_OK == (err = location_delete_contact_handler(linfo, contact))) {
                    MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "location id(%s) contact(%s) deleted", pincode, contact);
                } else {
                    MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "location id(%s) contact(%s) delete failed last error(%d)", pincode, contact, err);
                }
                (void)ma_location_info_release(linfo);
            }
        }
        (void)ma_profile_info_release(profile);
    }
    ma_msgbus_t *msgbus = NULL;
    (void)ma_profiler_set_logger(masvc_logger);

    if(MA_OK == (err = ma_mail_service_get_msgbus(app_driver::get_instance().get_mail_service(), &msgbus))) {
        ma_msgbus_endpoint_t *pendpoint = NULL;
        ma_message_t *prequest = NULL;
        ma_message_t *presponse = NULL;

        if(MA_OK == (err = ma_msgbus_endpoint_create(msgbus, MA_PROFILE_SERVICE_NAME_STR, NULL, &pendpoint)) &&
           MA_OK == (err = ma_message_create(&prequest))) {
            ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;

            (void)ma_msgbus_endpoint_setopt(pendpoint, MSGBUSOPT_THREADMODEL, thread_option);
            (void)ma_msgbus_endpoint_setopt(pendpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
            (void)ma_message_set_property(prequest, MA_PROFILE_SERVICE_MSG_TYPE, MA_PROFILE_SERVICE_MSG_UNREGISTER_TYPE);
            (void)ma_message_set_property(prequest, MA_PROFILE_ID, contact);
            if(MA_OK == (err = ma_msgbus_send(pendpoint, prequest, &presponse))) {
                const char *status = NULL;

                (void)ma_message_get_property(presponse, MA_PROFILE_STATUS, &status);
                if(!strcmp(status, MA_PROFILE_ID_DELETED)) {
                    *res = MA_TRUE;
                    MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "profile %s deleted", contact);
                } else {
                    MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "profile %s delete failed", contact);
                }
                (void)ma_message_release(presponse);
            } else
                MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "profile delete failed, last error(%d)", err);
            (void)ma_message_release(prequest);
            (void)ma_msgbus_endpoint_release(pendpoint);
        }
    }

    return err;
}

static ma_error_t send_mail(const char *email, const char *sub, const char *body) {
    if(email && sub && body) {
        ma_error_t err = MA_OK;
        ma_msgbus_t *msgbus = NULL;

        if(MA_OK == (err = ma_mail_service_get_msgbus(app_driver::get_instance().get_mail_service(), &msgbus))) {
            ma_msgbus_endpoint_t *endpoint = NULL;
            ma_message_t *request = NULL;
            ma_message_t *response = NULL;
 
            if(MA_OK == (err = ma_msgbus_endpoint_create(msgbus, MA_MAIL_SERVICE_NAME_STR, NULL, &endpoint)) &&
               MA_OK == (err = ma_message_create(&request))) {
                ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;

                (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);
                (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                if(MA_OK == (err = ma_message_set_property(request, MA_MAIL_SERVICE_MSG_TYPE, MA_MAIL_SERVICE_MSG_SEND_TYPE)) &&
                   MA_OK == (err = ma_message_set_property(request, MA_MAIL_ID_FROM, MA_EMAIL_ID)) &&
                   MA_OK == (err = ma_message_set_property(request, MA_MAIL_ID_TO, email)) &&
                   MA_OK == (err = ma_message_set_property(request, MA_MAIL_SUBJECT, sub))
                  ) {
                      ma_variant_t *payload = NULL;

                     if(MA_OK == (err = ma_variant_create_from_string(body, strlen(body), &payload))) {
                         (void)ma_message_set_payload(request, payload);
                         if(MA_OK == (err = ma_msgbus_send(endpoint, request, &response))) {
                             const char *reply_status = NULL;

                             if(MA_OK == (err = ma_message_get_property(response, MA_MAIL_STATUS, &reply_status))) {
                                 if(!strcmp(reply_status, MA_MAIL_SEND_SUCCESS)) {
                                     MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "mail sent(%s) successfully", email);
                                 } else {
                                     MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "mail sent(%s) failed", email);
                                 }
                             } 
                             (void)ma_message_release(response);
                         }
                         (void)ma_variant_release(payload);
                     }
                }
                (void)ma_message_release(request);
                (void)ma_msgbus_endpoint_release(endpoint);
            }
        }
    } 
    return MA_ERROR_INVALIDARG;
}

static ma_error_t profile_update_handler(ma_profile_info_t *pinfo) {
    if(!pinfo)return MA_ERROR_INVALIDARG;
    ma_error_t err = MA_OK;
    ma_profile_info_t *info = NULL;
    char email[MA_MAX_LEN+1] = {0};
    char contact[MA_MAX_LEN+1] = {0};
    char user[MA_MAX_NAME+1] = {0};
    char pincode[MA_MAX_LEN+1] = {0};
    bool location_update_required = false;
    ma_profile_info_user_type_t type =  MA_PROFILE_INFO_USER_TYPE_NORMAL;

    (void)ma_profile_info_get_user_type(pinfo, &type); 
    (void)ma_profile_info_get_contact(pinfo, contact);
    (void)ma_profile_info_get_source_pincode(pinfo, pincode);
    if(!strcmp(contact, MA_EMPTY)) {
        MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "profile does not exist(%s) ", contact);
        return MA_OK;
    }
    if(type == MA_PROFILE_INFO_USER_TYPE_OPERATION) {
        ma_profile_info_t *old_profile = NULL;
        char old_pincode[MA_MAX_LEN+1] = {0};

        if(MA_OK == (err = profile_get_handler(contact, &old_profile))) {
            (void)ma_profile_info_get_source_pincode(old_profile, old_pincode);
            (void)ma_profile_info_release(old_profile);
        }
        if(strncmp(old_pincode, pincode, MA_MAX_LEN)) {
            ma_location_info_t *linfo = NULL;
 
            location_update_required = true;
            if(MA_OK == (err = ma_location_info_create(&linfo))) {
                (void)ma_location_info_set_pincode(linfo, old_pincode);
                if(MA_OK == (err = location_update_handler(linfo, contact))) {
                    MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "register location request(%s) contact(%s) ", old_pincode, contact);
                } else {
                    MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "updated location request(%s) contact(%s) failed err(%d) ", old_pincode, contact, err);
                }
                (void)ma_location_info_release(linfo);
            }
            ma_location_info_t *new_linfo = NULL;

            if(MA_OK == (err = ma_location_info_create(&new_linfo))) {
                (void)ma_location_info_set_pincode(new_linfo, pincode);
                if(MA_OK == (err = location_register_handler(linfo, contact))) {
                    MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "register location request(%s) contact(%s) ", pincode, contact);
                } else {
                    MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "updated location request(%s) contact(%s) failed err(%d) ", pincode, contact, err);
                }
                (void)ma_location_info_release(new_linfo);
            }
        }
    }
    MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "processing profile update request(%s) ", contact);
    (void)ma_profile_info_get_email_id(pinfo, email); 
    if(strcmp(email, MA_EMPTY)) {
        MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "mail id(%s) exist", email);
    }
    (void)ma_profile_info_get_user(pinfo, user); 
    ma_variant_t *info_var = NULL;

    if(MA_OK == (err = ma_profile_info_convert_to_variant(info = pinfo, &info_var))) {
        ma_msgbus_t *msgbus = NULL;

        (void)ma_profiler_set_logger(masvc_logger);
        (void)ma_mail_service_get_msgbus(app_driver::get_instance().get_mail_service(), &msgbus);
        ma_msgbus_endpoint_t *pendpoint = NULL;
        ma_message_t *prequest = NULL;
        ma_message_t *presponse = NULL;

        if(MA_OK == (err = ma_msgbus_endpoint_create(msgbus, MA_PROFILE_SERVICE_NAME_STR, NULL, &pendpoint)) &&
           MA_OK == (err = ma_message_create(&prequest))) {
            ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;

            (void)ma_msgbus_endpoint_setopt(pendpoint, MSGBUSOPT_THREADMODEL, thread_option);
            (void)ma_msgbus_endpoint_setopt(pendpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
            (void)ma_message_set_property(prequest, MA_PROFILE_SERVICE_MSG_TYPE, MA_PROFILE_SERVICE_MSG_UPDATE_TYPE);
            (void)ma_message_set_property(prequest, MA_PROFILE_ID, contact);
            if(MA_OK == (err = ma_message_set_payload(prequest, info_var))) {
                if(MA_OK == (err = ma_msgbus_send(pendpoint, prequest, &presponse))) {
                    const char *status = NULL;

                    (void)ma_message_get_property(presponse, MA_PROFILE_STATUS, &status);
                    if(!strcmp(status, MA_PROFILE_ID_WRITTEN)) {
                        ma_msgbus_endpoint_t *iendpoint = NULL;
                        ma_message_t *irequest = NULL;
                        ma_message_t *iresponse = NULL;

                        if(location_update_required) {
                            ma_location_info_t *linfo = NULL;
 
                            location_update_required = true;
                            if(MA_OK == (err = ma_location_info_create(&linfo))) {
                                (void)ma_location_info_set_pincode(linfo, pincode);
                                if(MA_OK == (err = location_register_handler(linfo, contact))) {
                                    MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "register location request(%s) ", pincode);
                                } else {
                                    MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "register location request(%s) failed err(%d) ", pincode, err);
                                }
                                (void)ma_location_info_release(linfo);
                            }
                        }
                        if(MA_OK == (err = ma_msgbus_endpoint_create(msgbus, MA_INVENTORY_SERVICE_NAME_STR, NULL, &iendpoint)) &&
                           MA_OK == (err = ma_message_create(&irequest))) {
                            ma_inventory_info_t *inventory_info = NULL;

                            (void)ma_msgbus_endpoint_setopt(iendpoint, MSGBUSOPT_THREADMODEL, thread_option);
                            (void)ma_msgbus_endpoint_setopt(iendpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                            (void)ma_message_set_property(irequest, MA_INVENTORY_SERVICE_MSG_TYPE, MA_INVENTORY_SERVICE_MSG_REGISTER);
                            (void)ma_message_set_property(irequest, MA_INVENTORY_ID, contact);
                            if(MA_OK == (err = ma_inventory_info_create(&inventory_info))) {
                                ma_task_time_t date = {0};
                                ma_variant_t *ivar = NULL;

                                get_localtime(&date);
                                (void)ma_inventory_info_set_date(inventory_info, date);
                                (void)ma_inventory_info_set_email_id(inventory_info, email);
                                (void)ma_inventory_info_set_contact(inventory_info, contact);
                                if(MA_OK == (err = ma_inventory_info_convert_to_variant(inventory_info, &ivar))) {
                                    (void)ma_message_set_payload(irequest, ivar);
                                    if(MA_OK == (err = ma_msgbus_send(iendpoint, irequest, &iresponse))) {
                                        const char *inventory_status = NULL;

                                        (void)ma_message_get_property(iresponse, MA_INVENTORY_STATUS, &inventory_status);
                                        if(!strcmp(inventory_status, MA_INVENTORY_ID_WRITTEN)) {
                                            MA_LOG(masvc_logger, MA_LOG_SEV_INFO, "Inventory id(%s) information written succesfully", contact);
                                        } else
                                            MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "Inventory id(%s) information write failed", contact);
                                        (void)ma_message_release(iresponse);
                                    }
                                    (void)ma_variant_release(ivar);
                                }
                                (void)ma_inventory_info_release(inventory_info);
                            }
                            (void)ma_msgbus_endpoint_release(iendpoint);
                            (void)ma_message_release(irequest);
                        }
                        MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "profile info written successfully into profile DB");
                    } else {
                        MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "profile info write failed");
                    }
                    (void)ma_message_release(presponse);
                } else
                    MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "profile registration failed, last error(%d)", err);
                (void)ma_variant_release(info_var);
            }
            (void)ma_message_release(prequest);
            (void)ma_msgbus_endpoint_release(pendpoint);
        }
        (void)ma_variant_release(info_var);
    } else
        MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "profile info variant conversion failed, last error(%d)", err);

    return err;
}

static ma_error_t location_update_handler(ma_location_info_t *linfo, const char *contact) {
   if(linfo) {
        ma_error_t err = MA_OK;
        ma_msgbus_t *msgbus = NULL;
        char location_id[MA_MAX_LEN+1] = {0};  

        (void)ma_location_info_get_pincode(linfo, location_id);
        (void)ma_location_info_add_contact(linfo, location_id, contact);
        if(MA_OK == (err = ma_mail_service_get_msgbus(app_driver::get_instance().get_mail_service(), &msgbus))) {
            ma_msgbus_endpoint_t *endpoint = NULL;
            ma_message_t *request = NULL;
            ma_message_t *response = NULL;
 
            if(MA_OK == (err = ma_msgbus_endpoint_create(msgbus, MA_LOCATION_SERVICE_NAME_STR, NULL, &endpoint)) &&
               MA_OK == (err = ma_message_create(&request))) {
                ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;

                (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);
                (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                if(MA_OK == (err = ma_message_set_property(request, MA_LOCATION_SERVICE_MSG_TYPE, MA_LOCATION_SERVICE_MSG_UPDATE)) &&
                   MA_OK == (err = ma_message_set_property(request, MA_LOCATION_ID, location_id)) &&
                   MA_OK == (err = ma_message_set_property(request, MA_LOCATION_CRN, contact))
                  ) {
                      ma_variant_t *payload = NULL;

                     if(MA_OK == (err = ma_location_info_convert_to_variant(linfo, &payload))) {
                         (void)ma_message_set_payload(request, payload);
                         if(MA_OK == (err = ma_msgbus_send(endpoint, request, &response))) {
                             const char *reply_status = NULL;

                             if(MA_OK == (err = ma_message_get_property(response, MA_LOCATION_STATUS, &reply_status))) {
                                 if(!strcmp(reply_status, MA_LOCATION_ID_WRITTEN)) {
                                     MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "location id (%s) saved successfully", location_id);
                                 } else {
                                     MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "location id (%s) save failed", location_id);
                                 }
                             } 
                             (void)ma_message_release(response);
                         }
                         (void)ma_variant_release(payload);
                     }
                }
                (void)ma_message_release(request);
                (void)ma_msgbus_endpoint_release(endpoint);
            }
        }
    } 
    return MA_ERROR_INVALIDARG;
}
static ma_error_t location_delete_contact_handler(ma_location_info_t *linfo, const char *contact) {
   if(linfo) {
        ma_error_t err = MA_OK;
        ma_msgbus_t *msgbus = NULL;
        char location_id[MA_MAX_LEN+1] = {0};  

        (void)ma_location_info_get_pincode(linfo, location_id);
        (void)ma_location_info_add_contact(linfo, location_id, contact);
        if(MA_OK == (err = ma_mail_service_get_msgbus(app_driver::get_instance().get_mail_service(), &msgbus))) {
            ma_msgbus_endpoint_t *endpoint = NULL;
            ma_message_t *request = NULL;
            ma_message_t *response = NULL;
 
            if(MA_OK == (err = ma_msgbus_endpoint_create(msgbus, MA_LOCATION_SERVICE_NAME_STR, NULL, &endpoint)) &&
               MA_OK == (err = ma_message_create(&request))) {
                ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;

                (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);
                (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                if(MA_OK == (err = ma_message_set_property(request, MA_LOCATION_SERVICE_MSG_TYPE, MA_LOCATION_SERVICE_MSG_LOCATION_DELETE_CONTACT)) &&
                   MA_OK == (err = ma_message_set_property(request, MA_LOCATION_ID, location_id)) &&
                   MA_OK == (err = ma_message_set_property(request, MA_LOCATION_CRN, contact))
                  ) {
                      ma_variant_t *payload = NULL;

                     if(MA_OK == (err = ma_location_info_convert_to_variant(linfo, &payload))) {
                         (void)ma_message_set_payload(request, payload);
                         if(MA_OK == (err = ma_msgbus_send(endpoint, request, &response))) {
                             const char *reply_status = NULL;

                             if(MA_OK == (err = ma_message_get_property(response, MA_LOCATION_STATUS, &reply_status))) {
                                 if(!strcmp(reply_status, MA_LOCATION_ID_DELETED)) {
                                     MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "location id (%s) deleted successfully", location_id);
                                 } else {
                                     MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "location id (%s) deleted failed", location_id);
                                 }
                             } 
                             (void)ma_message_release(response);
                         }
                         (void)ma_variant_release(payload);
                     }
                }
                (void)ma_message_release(request);
                (void)ma_msgbus_endpoint_release(endpoint);
            }
        }
        return err;
    } 
    return MA_ERROR_INVALIDARG;
}
static ma_error_t location_register_handler(ma_location_info_t *linfo, const char *contact) {
   if(linfo) {
        ma_error_t err = MA_OK;
        ma_msgbus_t *msgbus = NULL;
        char location_id[MA_MAX_LEN+1] = {0};  

        (void)ma_location_info_get_pincode(linfo, location_id);
        (void)ma_location_info_add_contact(linfo, location_id, contact);
        if(MA_OK == (err = ma_mail_service_get_msgbus(app_driver::get_instance().get_mail_service(), &msgbus))) {
            ma_msgbus_endpoint_t *endpoint = NULL;
            ma_message_t *request = NULL;
            ma_message_t *response = NULL;
 
            if(MA_OK == (err = ma_msgbus_endpoint_create(msgbus, MA_LOCATION_SERVICE_NAME_STR, NULL, &endpoint)) &&
               MA_OK == (err = ma_message_create(&request))) {
                ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;

                (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);
                (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                if(MA_OK == (err = ma_message_set_property(request, MA_LOCATION_SERVICE_MSG_TYPE, MA_LOCATION_SERVICE_MSG_REGISTER)) &&
                   MA_OK == (err = ma_message_set_property(request, MA_LOCATION_ID, location_id)) &&
                   MA_OK == (err = ma_message_set_property(request, MA_LOCATION_CRN, contact))
                  ) {
                      ma_variant_t *payload = NULL;

                     if(MA_OK == (err = ma_location_info_convert_to_variant(linfo, &payload))) {
                         (void)ma_message_set_payload(request, payload);
                         if(MA_OK == (err = ma_msgbus_send(endpoint, request, &response))) {
                             const char *reply_status = NULL;

                             if(MA_OK == (err = ma_message_get_property(response, MA_LOCATION_STATUS, &reply_status))) {
                                 if(!strcmp(reply_status, MA_LOCATION_ID_WRITTEN)) {
                                     MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "location id (%s) saved successfully", location_id);
                                 } else {
                                     MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "location id (%s) save failed", location_id);
                                 }
                             } 
                             (void)ma_message_release(response);
                         }
                         (void)ma_variant_release(payload);
                     }
                }
                (void)ma_message_release(request);
                (void)ma_msgbus_endpoint_release(endpoint);
            }
        }
        return err;
    } 
    return MA_ERROR_INVALIDARG;
}

static ma_error_t profile_register_request_handler(ma_profile_info_t *pinfo, ma_bool_t *res) {
    if(!pinfo)return MA_ERROR_INVALIDARG;
    ma_error_t err = MA_OK;
    ma_profile_info_t *info = NULL;
    char email[MA_MAX_LEN+1] = {0};
    char contact[MA_MAX_LEN+1] = {0};
    char user[MA_MAX_NAME+1] = {0};

    (void)ma_profile_info_get_contact(pinfo, contact); 
    if(!strcmp(contact, MA_EMPTY)) {
        MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "profile contact cannot be empty (%s) ", contact);
        return MA_OK;
    }
    (void)ma_profile_info_get_email_id(pinfo, email); 
    (void)ma_profile_info_get_user(pinfo, user);
    ma_profile_info_user_type_t type = MA_PROFILE_INFO_USER_TYPE_CUSTOMER_EXECUTIVE; 
    (void)ma_profile_info_get_user_type(pinfo, &type);
    
    MA_LOG(masvc_logger, MA_LOG_SEV_INFO, "Profile id(%s) does not exist in DB, creating...", email);
    ma_variant_t *info_var = NULL;

    if(MA_OK == (err = ma_profile_info_convert_to_variant(info = pinfo, &info_var))) {
        ma_msgbus_t *msgbus = NULL;

        (void)ma_profiler_set_logger(masvc_logger);
        (void)ma_mail_service_get_msgbus(app_driver::get_instance().get_mail_service(), &msgbus);
        ma_msgbus_endpoint_t *pendpoint = NULL;
        ma_message_t *prequest = NULL;
        ma_message_t *presponse = NULL;

        if(MA_OK == (err = ma_msgbus_endpoint_create(msgbus, MA_PROFILE_SERVICE_NAME_STR, NULL, &pendpoint)) &&
           MA_OK == (err = ma_message_create(&prequest))) {
            ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;

            (void)ma_msgbus_endpoint_setopt(pendpoint, MSGBUSOPT_THREADMODEL, thread_option);
            (void)ma_msgbus_endpoint_setopt(pendpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
            (void)ma_message_set_property(prequest, MA_PROFILE_SERVICE_MSG_TYPE, MA_PROFILE_SERVICE_MSG_REGISTER_TYPE);
            (void)ma_message_set_property(prequest, MA_PROFILE_ID, contact);
            if(MA_OK == (err = ma_message_set_payload(prequest, info_var))) {
                if(MA_OK == (err = ma_msgbus_send(pendpoint, prequest, &presponse))) {
                    const char *status = NULL;

                    (void)ma_message_get_property(presponse, MA_PROFILE_STATUS, &status);
                    if(!strcmp(status, MA_PROFILE_ID_WRITTEN)) {
                        ma_msgbus_endpoint_t *iendpoint = NULL;
                        ma_message_t *irequest = NULL;
                        ma_message_t *iresponse = NULL;

                        *res = MA_TRUE;
                        if(type == MA_PROFILE_INFO_USER_TYPE_OPERATION) {
                            char pincode[MA_MAX_LEN+1] = {0};
                            ma_location_info_t *linfo = NULL;

                            if(MA_OK == (err = ma_location_info_create(&linfo))) {
                                (void)ma_profile_info_get_source_pincode(info, pincode);
                                (void)ma_location_info_set_pincode(linfo, pincode);
                                (void)ma_location_info_add_contact(linfo, pincode, contact);
                                if(MA_OK == (err = location_register_handler(linfo, contact))) {
                                     MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "executive location (%s) saved successfully", contact);
                                } else {
                                     MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "executive location (%s) saved failed (%d)", contact, err);
                                }
                                (void)ma_location_info_release(linfo);
                            }
                        }
                        if(MA_OK == (err = ma_msgbus_endpoint_create(msgbus, MA_INVENTORY_SERVICE_NAME_STR, NULL, &iendpoint)) &&
                           MA_OK == (err = ma_message_create(&irequest))) {
                            ma_inventory_info_t *inventory_info = NULL;

                            (void)ma_msgbus_endpoint_setopt(iendpoint, MSGBUSOPT_THREADMODEL, thread_option);
                            (void)ma_msgbus_endpoint_setopt(iendpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                            (void)ma_message_set_property(irequest, MA_INVENTORY_SERVICE_MSG_TYPE, MA_INVENTORY_SERVICE_MSG_REGISTER);
                            (void)ma_message_set_property(irequest, MA_INVENTORY_ID, contact);
                            if(MA_OK == (err = ma_inventory_info_create(&inventory_info))) {
                                ma_task_time_t date = {0};
                                ma_variant_t *ivar = NULL;

                                get_localtime(&date);
                                (void)ma_inventory_info_set_date(inventory_info, date);
                                (void)ma_inventory_info_set_email_id(inventory_info, email);
                                (void)ma_inventory_info_set_contact(inventory_info, contact);
                                if(MA_OK == (err = ma_inventory_info_convert_to_variant(inventory_info, &ivar))) {
                                    (void)ma_message_set_payload(irequest, ivar);
                                    if(MA_OK == (err = ma_msgbus_send(iendpoint, irequest, &iresponse))) {
                                        const char *inventory_status = NULL;

                                        (void)ma_message_get_property(iresponse, MA_INVENTORY_STATUS, &inventory_status);
                                        if(!strcmp(inventory_status, MA_INVENTORY_ID_WRITTEN)) {
                                            MA_LOG(masvc_logger, MA_LOG_SEV_INFO, "Inventory id(%s) information written succesfully", contact);
                                        } else
                                            MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "Inventory id(%s) information write failed", contact);
                                        (void)ma_message_release(iresponse);
                                    }
                                    (void)ma_variant_release(ivar);
                                }
                                (void)ma_inventory_info_release(inventory_info);
                            }
                            (void)ma_msgbus_endpoint_release(iendpoint);
                            (void)ma_message_release(irequest);
                        }
                        MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "profile info written successfully into profile DB");
                    } else {
                        MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "profile info write failed");
                    }
                    (void)ma_message_release(presponse);
                } else
                    MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "profile registration failed, last error(%d)", err);
                (void)ma_variant_release(info_var);
            }
            (void)ma_message_release(prequest);
            (void)ma_msgbus_endpoint_release(pendpoint);
        }
        (void)ma_variant_release(info_var);
    } else
        MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "profile info variant conversion failed, last error(%d)", err);

    return err;
}

unsigned season_pin_controller::operator() (tnt::HttpRequest& request, tnt::HttpReply& reply, tnt::QueryParams& qparam) {
    MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "query string(%s) url(%s) pathinfo(%s) method(%s)", request.getQuery().c_str(), request.getUrl().c_str(), request.getPathInfo().c_str(), request.getMethod().c_str());
    ma_season_pin_operation_type_t type = get_op_type(request.getUrl().c_str());
    ma_error_t err = MA_OK;
    string email(request.getCookie("email"));
    string contact(request.getCookie("contact"));
    ma_profile_info_t *info = NULL;
    ma_bool_t done = MA_FALSE;
    char user[MA_MAX_NAME+1] = {0};

    if(type != MA_SEASON_PIN_OPERATION_REGISTER && type != MA_SEASON_PIN_OPERATION_AUTHENTICATE && type != MA_SEASON_PIN_OPERATION_DELETE_USER && type != MA_SEASON_PIN_OPERATION_GET_PROFILE) {
        MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "user email(%s)", email.c_str());
        MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "user contact(%s)", contact.c_str());
        (void)profile_get_handler(contact.c_str(), &info);
    }
    switch(type) {
        case MA_SEASON_PIN_OPERATION_ADD: {
            MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "processing season pin add operation (%s)", request.getBody().c_str());
            ma_season_info_t *info  = NULL;

            if(MA_OK == (err = ma_season_info_convert_from_json(request.getBody().c_str(), &info))) {
                ma_variant_t *v = NULL;
                const char *id = NULL;

                (void)ma_season_info_get_id(info, &id);
                (void)ma_season_info_set_email_id(info, email.c_str());
                (void)ma_season_info_set_contact(info, contact.c_str());
                (void)ma_season_info_set_saved_by(info, user);
                if(MA_OK == (err = ma_season_info_convert_to_variant(info, &v))) {
                    if(MA_OK == (err = send_add_pin_request(v, id))) {
                        MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "season pin(%s) added successfully", id);
                        done = MA_TRUE;
                    } else {
                        MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "season pin(%s) added failed, last error(%d)", id, err);
                    }
                    (void)ma_variant_release(v);
                }
                (void)ma_season_info_release(info);
            }
            break;
        }
        case MA_SEASON_PIN_OPERATION_GET: {
            if(!email.empty()) {
                ma_variant_t *pin_payload = NULL;

                if(MA_OK == (err = send_get_pin_request(email.c_str(), &pin_payload))) {
                    ma_buffer_t *buffer = NULL;

                    if(MA_OK == (err = ma_variant_get_string_buffer(pin_payload, &buffer))) {
                        const char *pstr = NULL;
                        size_t len = 0;

                        if(MA_OK == (err = ma_buffer_get_string(buffer, &pstr, &len))) {
                            if(pstr) {
                                MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "pins(%s) length(%d)", pstr, len);
                                reply.setContentType("Content-Type: application/json");
                                reply.out() << pstr;
                                done = MA_TRUE;
                            } else
                                MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "no pins");
                        } else
                            MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "get pins string failed, last error(%d)", err);
                        (void)ma_buffer_release(buffer);
                    }
                    (void)ma_variant_release(pin_payload);
                }
            }
            break;
        }
        case MA_SEASON_PIN_OPERATION_DELETE: {
            MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "processing season pin delete operation");
            break;
        }
        case MA_SEASON_PIN_OPERATION_REGISTER: {
            MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "processing user registration operation (%s)", request.getBody().c_str());
            ma_profile_info_t *info  = NULL;
            ma_bool_t res = MA_FALSE;
            ma_json_t *jout = NULL;

            if(MA_OK == (err = ma_profile_info_convert_from_json(request.getBody().c_str(), &info))) {
                if(MA_OK == (err = profile_register_request_handler(info, &res))) {
                    if(MA_OK == (err = ma_json_create(&jout))) {
                        MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "user registration status %s", res ? "success" : "failure");
                        (void)ma_json_add_element(jout, MA_PROFILE_INFO_ATTR_STATUS, res ? "success" : "failure");
                        ma_bytebuffer_t *buffer = NULL;

                        ma_bytebuffer_create(MA_MAX_BUFFER_LEN, &buffer);
                        if(MA_OK == ma_json_get_data(jout, buffer)) {
                            reply.setContentType("Content-Type: application/json");
                            reply.out() << (const char*)ma_bytebuffer_get_bytes(buffer);
                        }
                        (void)ma_json_release(jout);
                    }
                    done = MA_TRUE;
                }
                (void)ma_profile_info_release(info);
            }
            break;
        }
        case MA_SEASON_PIN_OPERATION_DELETE_USER: {
            MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "processing user delete operation (%s)", email.c_str());
            ma_bool_t res = MA_FALSE;
            ma_json_t *jout = NULL;

            (void)profile_delete_hanlder(contact.c_str(), &res);
            if(MA_OK == (err = ma_json_create(&jout))) {
                MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "user deletion status %s", res ? "success" : "failure");
                (void)ma_json_add_element(jout, MA_PROFILE_INFO_ATTR_STATUS, res ? "success" : "failure");
                ma_bytebuffer_t *buffer = NULL;

                ma_bytebuffer_create(MA_MAX_BUFFER_LEN, &buffer);
                if(MA_OK == ma_json_get_data(jout, buffer)) {
                    reply.setContentType("Content-Type: application/json");
                    reply.out() << (const char*)ma_bytebuffer_get_bytes(buffer);
                }
                (void)ma_json_release(jout);
            }
            done = MA_TRUE;
            break;
        }
        case MA_SEASON_PIN_OPERATION_SUBSCRIBE_BOARDS: {
            MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "processing user board subscription operation (%s)", request.getBody().c_str());
            ma_cjson *json_array = ma_cjson_Parse(request.getBody().c_str());
            ma_bool_t res = MA_FALSE;

            if(json_array) {
                for(int i = 0; i < ma_cjson_GetArraySize(json_array); ++i) {
                    ma_cjson *json = ma_cjson_GetArrayItem(json_array, i);

                    if(!json)continue;
                    if(ma_cjson_HasObjectItem(json, MA_SEASON_INFO_ATTR_BOARD)) {
                        ma_cjson *j  = ma_cjson_GetObjectItem(json, MA_SEASON_INFO_ATTR_BOARD);
                        if(j->valuestring) {
                            MA_LOG(masvc_logger, MA_LOG_SEV_DEBUG, "board (%s)", j->valuestring);
                            (void)ma_profile_info_add_board(info, j->valuestring);
                            res = MA_TRUE;
                            done = MA_TRUE;
                        }
                    }
                }
                if(done) {
                    err = process_subscribe_board_request(info);
                }
                ma_cjson_Delete(json_array);
            }
                
            ma_json_t *jout = NULL;

            if(MA_OK == (err = ma_json_create(&jout))) {
                MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "board subscription status %s", res ? "success" : "failure");
                (void)ma_json_add_element(jout, MA_PROFILE_INFO_ATTR_STATUS, res ? "success" : "failure");
                ma_json_array_t *jarray = NULL;

                if(MA_OK == (err = ma_json_array_create(&jarray))) {
                    ma_json_array_add_object(jarray, jout);
                    ma_bytebuffer_t *buffer = NULL;

                    ma_bytebuffer_create(MA_MAX_BUFFER_LEN, &buffer);
                    if(MA_OK == ma_json_array_get_data(jarray, buffer)) {
                        reply.setContentType("Content-Type: application/json");
                        reply.out() << (const char*)ma_bytebuffer_get_bytes(buffer);
                    }
                    (void)ma_json_array_release(jarray);
                }
                (void)ma_json_release(jout);
            }
            break;
        }
        case MA_SEASON_PIN_OPERATION_QUERY: {
            MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "processing query operation (%s)", request.getBody().c_str());
            ma_cjson *json_array = ma_cjson_Parse(request.getBody().c_str());
            std::string query;

            if(json_array) {
                for(int i = 0; i < ma_cjson_GetArraySize(json_array); ++i) {
                    ma_cjson *json = ma_cjson_GetArrayItem(json_array, i);

                    if(!json)continue;
                    if(ma_cjson_HasObjectItem(json, MA_SEARCH_OP_QUERY)) {
                        ma_cjson *j  = ma_cjson_GetObjectItem(json, MA_SEARCH_OP_QUERY);
                        if(j->valuestring) {
                            query += j->valuestring;
                            query += " ";
                            MA_LOG(masvc_logger, MA_LOG_SEV_DEBUG, "query (%s)", j->valuestring);
                            done = MA_TRUE;
                        }
                    }
                }
                if(done) {
                    ma_variant_t *pin_payload = NULL;

                    done = MA_FALSE;
                    if(MA_OK == (err = send_query_request(query.c_str(), &pin_payload))) {
                        ma_buffer_t *buffer = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(pin_payload, &buffer))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buffer, &pstr, &len))) {
                                if(pstr) {
                                    //MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "pins(%s) length(%d)", pstr, len);
                                    reply.setContentType("Content-Type: application/json");
                                    reply.out() << pstr;
                                    done = MA_TRUE;
                                } else
                                    MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "query no pins");
                            } else
                                MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "query pins string failed, last error(%d)", err);
                            (void)ma_buffer_release(buffer);
                        }
                        (void)ma_variant_release(pin_payload);
                    }
                }
                
                ma_cjson_Delete(json_array);
            }
            break;
        }
        case MA_SEASON_PIN_OPERATION_BOARD_SELECTION: {
            MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "processing board selection");
            ma_variant_t *pin_payload = NULL;

            done = MA_FALSE;
            if(MA_OK == (err = send_get_all_board_subscriptions_request(email.c_str(), &pin_payload))) {
                ma_buffer_t *buffer = NULL;

                if(MA_OK == (err = ma_variant_get_string_buffer(pin_payload, &buffer))) {
                    const char *pstr = NULL;
                    size_t len = 0;

                    if(MA_OK == (err = ma_buffer_get_string(buffer, &pstr, &len))) {
                        if(pstr) {
                            reply.setContentType("Content-Type: application/json");
                            reply.out() << pstr;
                            done = MA_TRUE;
                        } else
                            MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "board subscripton empty");
                    } else
                        MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "board subscription failed, last error(%d)", err);
                    (void)ma_buffer_release(buffer);
                }
                (void)ma_variant_release(pin_payload);
            }
            break;
        }
        case MA_SEASON_PIN_OPERATION_UPDATE_PROFILE: {
            MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "processing user updation operation (%s)", request.getBody().c_str());
            ma_profile_info_t *info  = NULL;

            if(MA_OK == (err = ma_profile_info_convert_from_json(request.getBody().c_str(), &info))) {
                if(MA_OK == (err = profile_update_handler(info))) {
                    MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "user updation successful");
                    done = MA_TRUE;
                }
                (void)ma_profile_info_release(info);
            }
            break;
        }
        case MA_SEASON_PIN_OPERATION_GET_PROFILE: {
            MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "processing get user profile operation (%s)", request.getBody().c_str());
            ma_variant_t *pin_payload = NULL;

            done = MA_FALSE;
            if(MA_OK == (err = send_get_profile_request(contact.c_str(), &pin_payload))) {
                ma_buffer_t *buffer = NULL;

                if(MA_OK == (err = ma_variant_get_string_buffer(pin_payload, &buffer))) {
                    const char *pstr = NULL;
                    size_t len = 0;

                    if(MA_OK == (err = ma_buffer_get_string(buffer, &pstr, &len))) {
                        if(pstr) {
                            reply.setContentType("Content-Type: application/json");
                            reply.out() << pstr;
                            done = MA_TRUE;
                        } else
                            MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "profile empty");
                    } else
                        MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "board subscription failed, last error(%d)", err);
                    (void)ma_buffer_release(buffer);
                }
                (void)ma_variant_release(pin_payload);
            }
            break;
        }
        case MA_SEASON_PIN_OPERATION_ADD_BOARD: {
            MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "processing add board operation (%s)", request.getBody().c_str());
            ma_cjson *json = ma_cjson_Parse(request.getBody().c_str());

            if(json) {
                ma_board_info_t *board = NULL;

                if(MA_OK == ma_board_info_create(&board)) {
                    ma_variant_t *var = NULL;

                    if(ma_cjson_HasObjectItem(json, MA_BOARD_INFO_ATTR_BOARDS)) {
                        ma_cjson *j = ma_cjson_GetObjectItem(json, MA_BOARD_INFO_ATTR_BOARDS);
                        (void)ma_board_info_set_id(board, j->valuestring);
                        MA_LOG(masvc_logger, MA_LOG_SEV_DEBUG, "board (%s)", j->valuestring);
                    }
                    if(ma_cjson_HasObjectItem(json, MA_BOARD_INFO_ATTR_IMAGE_URL)) {
                        ma_cjson *j = ma_cjson_GetObjectItem(json, MA_BOARD_INFO_ATTR_IMAGE_URL);
                        (void)ma_board_info_set_image_url(board, j->valuestring);
                        MA_LOG(masvc_logger, MA_LOG_SEV_DEBUG, "board image url(%s)", j->valuestring);
                    }
                    if(MA_OK == (err = ma_board_info_convert_to_variant(board, &var))) {
                        const char *id = NULL;

                        (void)ma_board_info_get_id(board, &id);
                        if(strcmp(id, MA_EMPTY)) {
                            if(MA_OK == (err = send_add_board_request(id, var))) {
                                done = MA_TRUE;
                            }
                        }
                        (void)ma_variant_release(var);
                    }
                    (void)ma_board_info_release(board);
                }
            }
            break;
        }
        case MA_SEASON_PIN_OPERATION_AUTHENTICATE: {
            MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "processing authenticate operation (%s)", request.getBody().c_str());
            ma_cjson *json = ma_cjson_Parse(request.getBody().c_str());
            ma_json_t *jout = NULL;
            const char *status = "failed";

            if(json && MA_OK == ma_json_create(&jout)) {
                char email[MA_MAX_BUFFER_LEN+1] = {0};
                char passwd[MA_MAX_BUFFER_LEN+1] = {0};

                if(ma_cjson_HasObjectItem(json, MA_PROFILE_INFO_ATTR_EMAIL_ID)) {
                    ma_cjson *j = ma_cjson_GetObjectItem(json, MA_PROFILE_INFO_ATTR_EMAIL_ID);
                    strncpy(email, j->valuestring, MA_MAX_BUFFER_LEN);
                    MA_LOG(masvc_logger, MA_LOG_SEV_DEBUG, "user email(%s)", j->valuestring);
                }
                if(ma_cjson_HasObjectItem(json, MA_PROFILE_INFO_ATTR_PASSWORD)) {
                    ma_cjson *j = ma_cjson_GetObjectItem(json, MA_PROFILE_INFO_ATTR_PASSWORD);
                    strncpy(passwd, j->valuestring, MA_MAX_BUFFER_LEN);
                }
                ma_profile_info_t *pinfo = NULL;

                if(MA_OK == (err = profile_get_handler(email, &pinfo))) {
                    if(pinfo) {
                        char paswd[MA_MAX_BUFFER_LEN+1] = {0};

                        if(MA_OK == ma_profile_info_get_password(pinfo, paswd)) {
                            if(strcmp(paswd, "") && strcmp(passwd, "") && !strcmp(paswd, passwd)) {
                                status = "success";
                                MA_LOG(masvc_logger, MA_LOG_SEV_INFO, "user email(%s) authentication successful", email);
                            }
                        }
                        (void)ma_profile_info_release(pinfo);
                    }
                }
                (void)ma_json_add_element(jout, MA_PROFILE_INFO_ATTR_STATUS, status);
                ma_bytebuffer_t *buffer = NULL;

                ma_bytebuffer_create(MA_MAX_BUFFER_LEN, &buffer);
                if(MA_OK == ma_json_get_data(jout, buffer)) {
                    reply.setContentType("Content-Type: application/json");
                    reply.out() << (const char*)ma_bytebuffer_get_bytes(buffer);
                    done = MA_TRUE;
                }
                (void)ma_json_release(jout);
            }
            break;
        }
        case MA_SEASON_PIN_OPERATION_GET_BY_DAY: {
            MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "processing get by days operation (%s)", request.getBody().c_str());
            int which_day = 0;
#if 0
            ma_cjson *json = ma_cjson_Parse(request.getBody().c_str());

            if(json) {
                if(ma_cjson_HasObjectItem(json, MA_SEASON_WHICH_DAY)) {
                    ma_cjson *j = ma_cjson_GetObjectItem(json, MA_SEASON_WHICH_DAY);
                    MA_LOG(masvc_logger, MA_LOG_SEV_DEBUG, "which_day (%s)", j->valuestring);
                    int day = atoi(j->valuestring);
                    which_day = day < 0 ? 0 : day;
                }
            }
#endif
            ma_cjson *json_array = ma_cjson_Parse(request.getBody().c_str());
            if(json_array) {
                for(int i = 0; i < ma_cjson_GetArraySize(json_array); ++i) {
                    ma_cjson *json = ma_cjson_GetArrayItem(json_array, i);

                    if(!json)continue;
                    if(ma_cjson_HasObjectItem(json, MA_SEASON_WHICH_DAY)) {
                        ma_cjson *j  = ma_cjson_GetObjectItem(json, MA_SEASON_WHICH_DAY);
                        if(j->valuestring) {
                            MA_LOG(masvc_logger, MA_LOG_SEV_DEBUG, "which_day (%s)", j->valuestring);
                            int day = atoi(j->valuestring);
                            which_day = day < 0 ? 0 : day;
                        }
                    }
                }
            }
            char buf[MA_MAX_LEN+1] = {0};
            MA_MSC_SELECT(_snprintf, snprintf)(buf, MA_MAX_LEN, "%d", which_day);
            if(!email.empty()) {
                ma_variant_t *pin_payload = NULL;

                if(MA_OK == (err = send_get_by_day_pin_request(email.c_str(), buf, &pin_payload))) {
                    ma_buffer_t *buffer = NULL;

                    if(MA_OK == (err = ma_variant_get_string_buffer(pin_payload, &buffer))) {
                        const char *pstr = NULL;
                        size_t len = 0;

                        if(MA_OK == (err = ma_buffer_get_string(buffer, &pstr, &len))) {
                            if(pstr) {
                                MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "pins(%s) length(%d)", pstr, len);
                                reply.setContentType("Content-Type: application/json");
                                reply.out() << pstr;
                                done = MA_TRUE;
                            } else
                                MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "no pins");
                        } else
                            MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "get pins string failed, last error(%d)", err);
                        (void)ma_buffer_release(buffer);
                    }
                    (void)ma_variant_release(pin_payload);
                }
            }
            break;
        }
        case MA_BOOKING_OPERATION_CREATE_ORDER: {
            break;
        }
        case MA_BOOKING_OPERATION_DELETE_ORDER: {
            MA_LOG(masvc_logger, MA_LOG_SEV_INFO, "processing booking delete order operation");
            break;
        }
        case MA_SEASON_PIN_OPERATION_UNKNOWN:
        default: {
            MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "processing season pin unknown operation");
            break;
        }
    }
    if(type != MA_SEASON_PIN_OPERATION_REGISTER && type != MA_SEASON_PIN_OPERATION_AUTHENTICATE && type != MA_SEASON_PIN_OPERATION_DELETE_USER) {
        if(info) {
            (void)ma_profile_info_release(info);
        }
    }
    return MA_TRUE == done ? HTTP_OK : DECLINED;
}

