#include "ma/internal/apps/masvc/ma_update_operation_controller.h"
#include "ma/internal/defs/ma_booking_defs.h"
#include "ma/internal/utils/booker/ma_booking_info.h"
#include "ma/internal/utils/booker/ma_booker.h"
#include "ma/ma_common.h"
#include "ma/ma_log.h"
#include "ma/internal/utils/profiler/ma_profiler.h"
#include "ma/internal/utils/profiler/ma_profile_info.h"
#include "ma/internal/utils/inventory/ma_inventory_info.h"
#include "ma/internal/utils/inventory/ma_inventory.h"
#include "ma/internal/services/profile/ma_profile_service.h"
#include "ma/internal/services/mail/ma_mail_service.h"
#include "ma/internal/apps/masvc/ma_app_driver.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/ma_strdef.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_message.h"
#include "ma/internal/utils/threading/ma_atomic.h"
#include "ma/internal/utils/uuid/ma_uuid.h"
#include "ma/scheduler/ma_task.h"
#include "ma/scheduler/ma_triggers.h"
#include "ma/internal/services/scheduler/ma_scheduler_core.h"
#include "ma/internal/utils/context/ma_context.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"

#include <tnt/sessionscope.h>
#include <stdio.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "masvc"

extern ma_logger_t *masvc_logger;

static tnt::ComponentFactoryImpl<update_operation_controller> factory("ma_update_operation_controller");

static ma_error_t add_inventory_record(ma_inventory_t *inventory, const char *email_id, ma_variant_t *var, const char *cmd) {
   if(inventory && var && cmd) {
       ma_error_t err = MA_OK;
       ma_msgbus_t *msgbus = NULL;

       if(MA_OK == (err = ma_mail_service_get_msgbus(app_driver::get_instance().get_mail_service(), &msgbus))) {
           ma_msgbus_endpoint_t *endpoint = NULL;
           ma_message_t *request = NULL;
           ma_message_t *response = NULL;

           if(MA_OK == (err = ma_msgbus_endpoint_create(msgbus, MA_INVENTORY_SERVICE_NAME_STR, NULL, &endpoint)) &&
              MA_OK == (err = ma_message_create(&request))) {
               ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;

               (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);
               (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC); 
               //(void)ma_message_set_property(request, MA_INVENTORY_SERVICE_MSG_TYPE, MA_INVENTORY_SERVICE_MSG_ORDER_CREATE_TYPE);
               (void)ma_message_set_property(request, MA_INVENTORY_SERVICE_MSG_TYPE, cmd);
               (void)ma_message_set_property(request, MA_INVENTORY_ID, email_id);
               (void)ma_message_set_payload(request, var);
               if(MA_OK == (err = ma_msgbus_send(endpoint, request, &response))) {
                   const char *reply_status = NULL;

                   (void)ma_message_get_property(response, MA_INVENTORY_STATUS, &reply_status);
                   if(!strcmp(MA_INVENTORY_ID_WRITTEN, reply_status)) {
                       MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "Operation ID(%s) updated successfully", email_id);
                   } else {
                       err = MA_ERROR_UNEXPECTED;
                       MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "Operation ID(%s) update failed", email_id);
                   }
                   (void)ma_message_release(response);
               }

               (void)ma_msgbus_endpoint_release(endpoint);
               (void)ma_message_release(request);
           }
       }
       return err;
   }
   return MA_ERROR_INVALIDARG;
}

static ma_error_t update_pending_orders(ma_context_t *context, ma_inventory_info_t *info) {
   if(context && info) {
       ma_error_t err = MA_OK;
       ma_scheduler_t *scheduler = MA_CONTEXT_GET_SCHEDULER(context);
       ma_task_t **task_array = NULL;
       size_t task_array_size = 0;
       char inventory_id[MA_MAX_LEN+1] = {0};
       size_t count = 0;

       (void)ma_inventory_info_get_email_id(info, inventory_id);
       if(MA_OK == (err = ma_scheduler_enumerate_task_server(scheduler, MA_SOFTWAREID_GENERAL_STR, MA_BOOKING_SERVICE_SECTION_NAME_STR, MA_BOOKING_TASK_TYPE, &task_array, &task_array_size))) {
           for(int i = 0; i < task_array_size; ++i) {
               ma_variant_t *booking_payload = NULL;

               if(MA_OK == (err = ma_task_get_app_payload(task_array[i], &booking_payload))) {
                   ma_booking_info_t *book_info = NULL;

                   if(MA_OK == (err = ma_booking_info_convert_from_variant(booking_payload, &book_info))) {
                       char picker_id[MA_MAX_LEN+1] = {0};

                       (void)ma_booking_info_get_picker_email_id(book_info, picker_id);
                       if(!strcmp(picker_id, inventory_id)) {
                           ma_booking_info_status_t status = MA_BOOKING_INFO_STATUS_NOT_ASSIGNED;

                           (void)ma_booking_info_get_status(book_info, &status);
                           if(MA_BOOKING_INFO_STATUS_NOT_ASSIGNED != status) ++count;
                       }
                       (void)ma_booking_info_release(book_info);
                   }
                   (void)ma_variant_release(booking_payload);
               }
           }

           (void)ma_scheduler_release_task_set(task_array);
       }

       (void)ma_inventory_info_set_order_pending_count(info, count);
       return err;
   }
   return MA_ERROR_INVALIDARG;
}

unsigned update_operation_controller::operator() (tnt::HttpRequest& request, tnt::HttpReply& reply, tnt::QueryParams& qparam) {
   if((strcmp("/update_operation.html", request.getUrl().c_str()) &&  strcmp("/update_operation", request.getUrl().c_str())) &&
     (strcmp("/update_workstatus.html", request.getUrl().c_str()) &&  strcmp("/update_workstatus", request.getUrl().c_str()))) {
       MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "url(%s) is not update_operation.html or update_workstatus.html, so ignoring", request.getUrl().c_str());
       return DECLINED;
   }
   string email(request.getCookie("email"));
   string email_ID(qparam.arg<string>("email"));
   string work_status(qparam.arg<string>("status"));
   string today(qparam.arg<string>("pickupdate"));
   string start_time(qparam.arg<string>("start_time"));
   string end_time(qparam.arg<string>("end_time"));
   string start_km(qparam.arg<string>("start_km"));
   string end_km(qparam.arg<string>("end_km"));
   string vehicle_type(qparam.arg<string>("vehicle_type"));
   string pincode(qparam.arg<string>("pincode"));

   MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "update_operation.html executing profile(%s) operation Id(%s) work_status(%s) date(%s) start_time(%s) end_time(%s) start_km(%s) end_km(%s) vehicle_type(%s) pincode(%s)", email.c_str(), email_ID.c_str(), work_status.c_str(), today.c_str(), start_time.c_str(), end_time.c_str(), start_km.c_str(), end_km.c_str(), vehicle_type.c_str(), pincode.c_str());
   if(email.empty()) {
       reply.setCookie("error", std::string("Session is already expired. Please login again!"), 2);
       reply.redirect(string("index.html"));
       return HTTP_OK;
   }
  

   ma_error_t err = MA_OK;
   ma_inventory_info_t *info = NULL;
   ma_profile_info_t *prof_info = NULL;
   ma_profiler_t *profiler = NULL;
   ma_context_t *context = NULL;
   char today_str[MA_MAX_LEN+1] ={0};
   ma_inventory_t *inventory = MA_CONTEXT_GET_INVENTORY(context);
   ma_task_time_t _today = {0};

   (void)ma_booking_service_get_context(app_driver::get_instance().get_booking_service(), &context);
   inventory = MA_CONTEXT_GET_INVENTORY(context);
   get_localtime(&_today);
   _today.hour = _today.minute = _today.secs = 0;
   ma_strtime(_today, today_str);

   (void)ma_profile_service_get_profiler(app_driver::get_instance().get_profile_service(), &profiler);    
   if(MA_OK == (err = ma_profiler_get(profiler, email_ID.c_str(), &prof_info))) {
       (void)ma_profile_info_release(prof_info);
   } else {
       reply.setCookie("error", std::string("Profile does not exist!"), 2);
       reply.redirect(string("logged_on.html"));
       return HTTP_OK;
   }
   if(MA_OK == (err = ma_inventory_get(inventory, email_ID.c_str(), today_str, &info))) {
       if(!pincode.empty())(void)ma_inventory_info_set_pincode(info, pincode.c_str());
       if(!work_status.empty())(void)ma_inventory_info_set_work_status_from_str(info, work_status.c_str());
       ma_task_time_t date = {0};
       char date_str[MA_MAX_LEN+1] = {0};

       if(!today.empty()) {
           sscanf(today.c_str(), "%hu-%hu-%hu", &date.month, &date.day, &date.year);
           ma_strtime(date, date_str);
           MA_LOG(masvc_logger, MA_LOG_SEV_DEBUG, "update operation input date(%s)", date_str);
           (void)ma_inventory_info_set_date(info, date);
           if(!start_time.empty()) {
               sscanf(start_time.c_str(), "%hu", &date.hour);
               ma_strtime(date, date_str);
               MA_LOG(masvc_logger, MA_LOG_SEV_DEBUG, "update operation input date(%s)", date_str);
               (void)ma_inventory_info_set_start_time(info, date);
           }
           if(!end_time.empty()) {
               sscanf(end_time.c_str(), "%hu", &date.hour);
               ma_strtime(date, date_str);
               MA_LOG(masvc_logger, MA_LOG_SEV_DEBUG, "update operation input date(%s)", date_str);
               (void)ma_inventory_info_set_end_time(info, date);
           }
           date.minute = date.hour = date.secs = 0;
           ma_strtime(date, date_str);
       }
       if(!start_km.empty())(void)ma_inventory_info_set_start_kilometers(info, (ma_uint32_t)atoi(start_km.c_str()));
       if(!end_km.empty())(void)ma_inventory_info_set_end_kilometers(info, (ma_uint32_t)atoi(end_km.c_str()));
       if(!vehicle_type.empty())(void)ma_inventory_info_set_vehicle_type_from_str(info, vehicle_type.c_str());
       (void)update_pending_orders(context, info);
       ma_variant_t *var = NULL;

       if(MA_OK == (err = ma_inventory_info_convert_to_variant(info, &var))) {
           if(MA_OK == (err = add_inventory_record(inventory, email_ID.c_str(), var, MA_INVENTORY_SERVICE_MSG_ORDER_MODIFY_TYPE))) {
               MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "Inventory ID(%s) update successfully", email_ID.c_str());
               reply.setCookie("error", std::string("Operation details updated successfully!"), 2);
               reply.setCookie("status", std::string("success"), 100);
           } else {
               MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "Inventory ID(%s) update failed, last error(%d) ", email_ID.c_str(), err);
               reply.setCookie("error", std::string("Operation detials update failed!"), 2);
               reply.setCookie("status", std::string("failed"), 100);
           }
           (void)ma_variant_release(var);
       }

       (void)ma_inventory_info_release(info);
   } else  {
       if(MA_OK == (err = ma_inventory_info_create(&info))) {
           (void)ma_inventory_info_set_email_id(info, email_ID.c_str());
           if(!pincode.empty())(void)ma_inventory_info_set_pincode(info, pincode.c_str());
           if(!work_status.empty())(void)ma_inventory_info_set_work_status_from_str(info, work_status.c_str());
           ma_task_time_t date = {0};
           char date_str[MA_MAX_LEN+1] = {0};

           if(!today.empty()) {
               sscanf(today.c_str(), "%hu-%hu-%hu", &date.month, &date.day, &date.year);
               ma_strtime(date, date_str);
               MA_LOG(masvc_logger, MA_LOG_SEV_DEBUG, "update operation input date(%s)", date_str);
               (void)ma_inventory_info_set_date(info, date);
               if(!start_time.empty()) {
                   sscanf(start_time.c_str(), "%hu", &date.hour);
                   ma_strtime(date, date_str);
                   MA_LOG(masvc_logger, MA_LOG_SEV_DEBUG, "update operation input date(%s)", date_str);
                   (void)ma_inventory_info_set_start_time(info, date);
               }
               if(!end_time.empty()) {
                   sscanf(end_time.c_str(), "%hu", &date.hour);
                   ma_strtime(date, date_str);
                   MA_LOG(masvc_logger, MA_LOG_SEV_DEBUG, "update operation input date(%s)", date_str);
                   (void)ma_inventory_info_set_end_time(info, date);
               }
               date.minute = date.hour = date.secs = 0;
               ma_strtime(date, date_str);
           }
           if(!start_km.empty())(void)ma_inventory_info_set_start_kilometers(info, (ma_uint32_t)atoi(start_km.c_str()));
           if(!end_km.empty())(void)ma_inventory_info_set_end_kilometers(info, (ma_uint32_t)atoi(end_km.c_str()));
           if(!vehicle_type.empty())(void)ma_inventory_info_set_vehicle_type_from_str(info, vehicle_type.c_str());
           (void)update_pending_orders(context, info);
           ma_variant_t *var = NULL;

           if(MA_OK == (err = ma_inventory_info_convert_to_variant(info, &var))) {
               if(MA_OK == (err = add_inventory_record(inventory, email_ID.c_str(), var, MA_INVENTORY_SERVICE_MSG_ORDER_CREATE_TYPE))) {
                   MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "Inventory ID(%s) update successfully", email_ID.c_str());
                   reply.setCookie("error", std::string("Operation details updated successfully!"), 2);
                   reply.setCookie("status", std::string("success"), 100);
               } else  {
                   MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "Inventory ID(%s) update failed, last error(%d) ", email_ID.c_str(), err);
                   reply.setCookie("error", std::string("Operation detials update failed!"), 2);
                   reply.setCookie("status", std::string("failed"), 100);
               }
               (void)ma_variant_release(var);
           }
           (void)ma_inventory_info_release(info);
       }
   }

   MA_LOG(masvc_logger, MA_LOG_SEV_DEBUG, " update operation handler ends");
   reply.redirect(std::string("logged_on.html"));

   return HTTP_OK;
}

