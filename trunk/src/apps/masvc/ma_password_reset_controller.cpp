#include "ma/internal/apps/masvc/ma_password_reset_controller.h"
#include "ma/ma_common.h"
#include "ma/ma_log.h"
#include "ma/internal/utils/profiler/ma_profiler.h"
#include "ma/internal/utils/profiler/ma_profile_info.h"
#include "ma/internal/services/profile/ma_profile_service.h"
#include "ma/internal/apps/masvc/ma_app_driver.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/defs/ma_general_defs.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_message.h"

#include <tnt/sessionscope.h>
#include <stdio.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "masvc"

extern ma_logger_t *masvc_logger;

static tnt::ComponentFactoryImpl<password_reset_controller> factory("ma_password_reset_controller");

unsigned password_reset_controller::operator() (tnt::HttpRequest& request, tnt::HttpReply& reply, tnt::QueryParams& qparam) {
    if(strcmp("/password_reset.html", request.getUrl().c_str())) {
        MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "url(%s) is not password_reset.html, so ignoring", request.getUrl().c_str());
        return DECLINED;
    }
    string email(qparam.arg<string>("email"));
  
    request.clearSession();
    MA_LOG(masvc_logger, MA_LOG_SEV_INFO, " HTTP Post request  email(%s) ",  email.c_str());

    ma_error_t err = MA_OK;
    ma_profile_info_t *info = NULL;
    ma_profiler_t *profiler = NULL;

    (void)ma_profiler_set_logger(masvc_logger);
    (void)ma_profile_service_get_profiler(app_driver::get_instance().get_profile_service(), &profiler);
    if(MA_OK == (err = ma_profiler_get(profiler, email.c_str(), &info))) {
        if(info) {
            char passwd[MA_MAX_NAME+1] = {0};
            char user[MA_MAX_NAME+1] = {0};

            MA_LOG(masvc_logger, MA_LOG_SEV_DEBUG, "Profile id(%s) already exist in DB", email.c_str());
            (void)ma_profile_info_get_user(info, user);
            if(MA_OK == (err = ma_profile_info_get_password(info, passwd))) {
                MA_LOG(masvc_logger, MA_LOG_SEV_DEBUG, "Profile password(%s) for profile id(%s)", passwd, email.c_str());
                MA_LOG(masvc_logger, MA_LOG_SEV_DEBUG, " password reset handler ends user(%s) ", request.getUsername().c_str());
                ma_msgbus_t *msgbus = NULL;

                if(MA_OK == (err = ma_mail_service_get_msgbus(app_driver::get_instance().get_mail_service(), &msgbus))) {
                    ma_msgbus_endpoint_t *endpoint = NULL;
                    ma_message_t *request = NULL;
                    ma_message_t *response = NULL;

                    if(MA_OK == (err = ma_msgbus_endpoint_create(msgbus, MA_MAIL_SERVICE_NAME_STR, NULL, &endpoint)) &&
                       MA_OK == (err = ma_message_create(&request))) {
                        ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;
                        const char *welcome_sub = "Profile password reset";

                        (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);
                        (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                        if(MA_OK == (err = ma_message_set_property(request, MA_MAIL_SERVICE_MSG_TYPE, MA_MAIL_SERVICE_MSG_SEND_TYPE)) &&
                           MA_OK == (err = ma_message_set_property(request, MA_MAIL_ID_FROM, MA_EMAIL_ID)) &&
                           MA_OK == (err = ma_message_set_property(request, MA_MAIL_ID_TO, email.c_str())) &&
                           MA_OK == (err = ma_message_set_property(request, MA_MAIL_SUBJECT, welcome_sub))
                          ) {
                              char envelope[MA_MAX_BUFFER_LEN+1] = {0};
                              ma_variant_t *payload = NULL;

                              MA_MSC_SELECT(_snprintf, snprintf)(envelope, MA_MAX_BUFFER_LEN, "Dear %s,\n\nPlease find your password(%s) and try reconnecting back to mileaccess.\nHappy Booking!\n\n Regards,\n mileaccess support team\n", user, passwd);

                              if(MA_OK == (err = ma_variant_create_from_string(envelope, strlen(envelope), &payload))) {
                                  (void)ma_message_set_payload(request, payload);
                                  if(MA_OK == (err = ma_msgbus_send(endpoint, request, &response))) {
                                      const char *reply_status = NULL;

                                      (void)ma_message_get_property(response, MA_MAIL_STATUS, &reply_status);
                                      if(!strcmp(reply_status, MA_MAIL_SEND_SUCCESS)) {
                                          MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "profile (%s) password reset mail send successful", email.c_str()); 
                                          reply.setCookie("error", std::string("Profile password sent to your mail id, please check your inbox!"), 3);
                                          reply.setCookie("status", std::string("success"),100);
                                          reply.redirect(string("index.html"));
                                      } else
                                          MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "profile (%s) password reset mail send failed!", email.c_str()); 
                                      (void)ma_message_release(response);
                                  }
                                 (void)ma_variant_release(payload);
                              }
                        }
                        (void)ma_msgbus_endpoint_release(endpoint);
                        (void)ma_message_release(request);
                    }
                }
            }
            free(info);
        } else {
            reply.setCookie("error", std::string("Profile does not exist, please go ahead and register!"), 3);
            reply.setCookie("status", std::string("failed"),100);
            reply.redirect(std::string("index.html"));
        }
    } else {
        reply.setCookie("error", std::string("Profile does not exist, please go ahead and register!"), 3);
        reply.setCookie("status", std::string("failed"),100);
        reply.redirect(std::string("index.html"));
    }
    MA_LOG(masvc_logger, MA_LOG_SEV_DEBUG, " password reset handler ends");

    return HTTP_OK;
}

