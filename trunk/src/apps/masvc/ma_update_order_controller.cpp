#include "ma/internal/apps/masvc/ma_update_order_controller.h"
#include "ma/internal/defs/ma_booking_defs.h"
#include "ma/internal/defs/ma_general_defs.h"
#include "ma/internal/utils/booker/ma_booking_info.h"
#include "ma/internal/utils/booker/ma_booker.h"
#include "ma/ma_common.h"
#include "ma/ma_log.h"
#include "ma/internal/utils/profiler/ma_profiler.h"
#include "ma/internal/utils/profiler/ma_profile_info.h"
#include "ma/internal/utils/inventory/ma_inventory_info.h"
#include "ma/internal/services/profile/ma_profile_service.h"
#include "ma/internal/services/mail/ma_mail_service.h"
#include "ma/internal/apps/masvc/ma_app_driver.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/ma_strdef.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_message.h"
#include "ma/internal/utils/threading/ma_atomic.h"
#include "ma/internal/utils/uuid/ma_uuid.h"
#include "ma/scheduler/ma_task.h"
#include "ma/scheduler/ma_triggers.h"
#include "ma/internal/services/scheduler/ma_scheduler_core.h"
#include "ma/internal/utils/context/ma_context.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"
#include "ma/ma_log.h"

#include <tnt/sessionscope.h>
#include <stdio.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "masvc"

extern ma_logger_t *masvc_logger;

static tnt::ComponentFactoryImpl<update_order_controller> factory("ma_update_order_controller");

ma_error_t update_order_controller::process(ma_booking_info_t *info) {
    if(!info)return MA_ERROR_INVALIDARG;
    ma_error_t err = MA_OK;
    ma_msgbus_t *msgbus = NULL;
    char crn[MA_MAX_LEN+1] = {0};

    (void)ma_booking_info_get_id(info, crn);
    if(MA_OK == (err = ma_mail_service_get_msgbus(app_driver::get_instance().get_mail_service(), &msgbus))) {
        ma_msgbus_endpoint_t *endpoint = NULL;
        ma_message_t *request = NULL;
        ma_message_t *response = NULL;

        if(MA_OK == (err = ma_msgbus_endpoint_create(msgbus, MA_BOOKING_SERVICE_NAME_STR, NULL, &endpoint)) &&
            MA_OK == (err = ma_message_create(&request))) {
            ma_msgbus_callback_thread_options_t thread_option = MA_MSGBUS_CALLBACK_THREAD_POOL;

            (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, thread_option);
            (void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
            (void)ma_message_set_property(request, MA_BOOKING_SERVICE_MSG_TYPE, MA_BOOKING_SERVICE_MSG_ORDER_UPDATE_STATUS_TYPE);
            (void)ma_message_set_property(request, MA_BOOKING_ID, crn);

            ma_variant_t *booking_var = NULL;

            if(MA_OK == (err = ma_booking_info_convert_to_variant(info, &booking_var))) {
                (void)ma_message_set_payload(request, booking_var);
                if(MA_OK == (err = ma_msgbus_send(endpoint, request, &response))) {
                    const char *status = NULL;
                    if(MA_OK == (err = ma_message_get_property(response, MA_BOOKING_STATUS, &status))) {
                        if(!strcmp(status, MA_BOOKING_ID_WRITTEN)) {
                            MA_LOG(masvc_logger, MA_LOG_SEV_TRACE, "booking id(%s) created successfully", crn);
                        } else
                            MA_LOG(masvc_logger, MA_LOG_SEV_ERROR, "booking id(%s) creation failed, last error(%d)", crn, err);
                    }
                    (void)ma_message_release(response);
                }
                (void)ma_variant_release(booking_var);
            }

            (void)ma_msgbus_endpoint_release(endpoint);
            (void)ma_message_release(request);
        }
    }
    return err;
}

unsigned update_order_controller::operator() (tnt::HttpRequest& request, tnt::HttpReply& reply, tnt::QueryParams& qparam) {
    MA_LOG(masvc_logger, MA_LOG_SEV_INFO, "processing booking update order operation (%s) order id(%s)", request.getBody().c_str(), request.getArg("id").c_str());
    ma_booking_info_t *info  = NULL;
    ma_error_t err = MA_OK;
    ma_bool_t res = MA_FALSE;
    ma_json_t *jout = NULL;

    if(MA_OK == (err = ma_booking_info_convert_from_json(request.getBody().c_str(), &info))) {
        (void)ma_booking_info_set_id(info, request.getArg("id").c_str());
        res = (MA_OK == (err = process(info))) ? MA_TRUE : MA_FALSE;
        if(MA_OK == ma_json_create(&jout)) {
            (void)ma_json_add_element(jout, MA_PROFILE_INFO_ATTR_STATUS, res ? "success" : "failure");
            ma_bytebuffer_t *buffer = NULL;

            ma_bytebuffer_create(MA_MAX_BUFFER_LEN, &buffer);
            if(MA_OK == ma_json_get_data(jout, buffer)) {
                reply.setContentType("Content-Type: application/json");
                reply.out() << (const char*)ma_bytebuffer_get_bytes(buffer);
            }
            (void)ma_json_release(jout);
        }
        (void)ma_booking_info_release(info);
    }

    return HTTP_OK;
}
