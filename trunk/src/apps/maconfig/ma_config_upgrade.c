#include "ma_config_upgrade.h"
#include "ma_config_internal.h"
#include "ma/internal/utils/spipe/ma_spipe_package_defs.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/datastore/ma_ds_registry.h"
#include "ma/internal/defs/ma_compat_defs.h"
#include "ma/internal/utils/platform/ma_sys_apis.h"
#include "ma/internal/utils/filesystem/filesystem_utils.h"

#include <stdio.h>
#include <sys/stat.h>

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "maconfig"

static ma_error_t add_certificates(ma_config_context_t *self, const char *certstore_path);
static ma_error_t revoke_certificates(ma_config_context_t *self, const char *certstore_path);

/* updating Agent mode */
static ma_error_t update_agent_mode(ma_config_context_t *self) {   
    ma_error_t rc = MA_OK;
    unsigned short int agent_mode = MA_AGENT_MODE_UNMANAGED_INT;
    ma_ds_t *ds = ma_configurator_get_datastore(self->agent_configurator);
    if(MA_OK == (rc = check_for_spipe_entries_in_sitelist(self))) {
        agent_mode = MA_AGENT_MODE_MANAGED_INT;
		rc = ma_ds_set_int(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_MODE_INT, MA_AGENT_MODE_MANAGED_INT);                        
    }
    else 
        rc = ma_ds_set_int(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_MODE_INT, MA_AGENT_MODE_UNMANAGED_INT);

    return rc;
}

ma_error_t ma_config_upgrade_import_data(ma_config_context_t *self) {
    ma_error_t rc = MA_OK;
    
    if(MA_OK == (rc = ma_config_configurator_create(self)))	{	        
        if(MA_OK == (rc = ma_config_crypto_create(self))) {
		    ma_datastore_configuration_request_t req = {{MA_TRUE,MA_DB_OPEN_READWRITE}, {MA_FALSE,0}, {MA_FALSE,0}, {MA_FALSE,0}, {MA_FALSE,0}, {MA_FALSE, 0}, {MA_FALSE, 0}, {MA_FALSE, 0}, {MA_FALSE, 0}, {MA_FALSE, 0}, {MA_FALSE, 0}, {MA_FALSE, 0}, {MA_FALSE, 0}, {MA_FALSE, 0}, {MA_FALSE, 0}};
			if(MA_OK == (rc = ma_configurator_intialize_datastores(self->agent_configurator, &req))) {                
                ma_temp_buffer_t path;
                ma_temp_buffer_t path_lowercase;
		        ma_temp_buffer_init(&path_lowercase);
		        ma_temp_buffer_init(&path);
                form_path(&path, self->params->agent_provisioning_dir, STR_SITELIST_NAME, MA_FALSE);
		        form_path(&path_lowercase, self->params->agent_provisioning_dir, STR_SITELIST_NAME_LOWERCASE, MA_FALSE);
		        if( (MA_OK == ma_config_get_file_buffer((char*)ma_temp_buffer_get(&path), &self->sitelist_data)) || (MA_OK == ma_config_get_file_buffer((char*)ma_temp_buffer_get(&path_lowercase), &self->sitelist_data)) ) {                   

                    if(MA_OK == (rc = update_agent_mode(self))) {
			            if(MA_OK == (rc = ma_config_sitelist_handle(self)))
				            MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Sitelist upgrade succeeded");                
				        else
				            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to handle sitelist db, error = (%d).", rc);
                    }
                    else
                        MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to set agent mode, error = (%d).", rc);
                 }
                 else
                    MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to get sitelist buffer fron sitelist xml file, error = (%d)", rc);	
                
                ma_temp_buffer_uninit(&path);
		        ma_temp_buffer_uninit(&path_lowercase);
            }
			else						
			    MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to create data store, error = (%d)", rc);			
		}
		else
			MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to create crypto, error = (%d)", rc);              
	}
	else
		MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to create maconfig configurator, error = (%d)", rc);

    return rc;
}

ma_error_t ma_config_import_certs(ma_config_context_t *self) {
	ma_error_t rc = MA_OK;
	char certstore_path[MAX_PATH_LEN] = {0};
	
	if(MA_OK == (rc = ma_config_configurator_create(self)))	{
		const char *data_path = ma_configurator_get_agent_data_path(self->agent_configurator);
		if(data_path) {
			MA_MSC_SELECT(_snprintf, snprintf)(certstore_path, MAX_PATH_LEN, "%s%c%s", data_path, MA_PATH_SEPARATOR, MA_CONFIGURATION_CERTSTORE_DIR_NAME_STR);

			(MA_OK == (rc = add_certificates(self, certstore_path))) &&
			(MA_OK == (rc = revoke_certificates(self, certstore_path)));
		}
		else {
			MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to get certstore path");
			rc = MA_ERROR_UNEXPECTED;
		}
	}
	else
		MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to create configurator");

	return rc;
}

static ma_bool_t file_exists(const char *file_path) {
	struct stat st = {0};
	return 0 == stat(file_path , &st);
}

static ma_bool_t is_cert_file(ma_temp_buffer_t *file_buffer) {
	const char *file = (char *)ma_temp_buffer_get(file_buffer);	
	size_t file_len = strlen(file);
	return !(file_len <= 4 || (file_len > 4 && strncmp(file + file_len - 4, ".cer", 4)));		
}

static ma_error_t add_certificates(ma_config_context_t *self, const char *certstore_path) {
	ma_error_t rc = MA_OK;
	ma_filesystem_iterator_t *iterator = NULL;

	if(MA_OK == (rc = ma_filesystem_iterator_create(self->params->agent_provisioning_dir, &iterator))) {
		ma_temp_buffer_t file_buffer = {0};
		(void)ma_filesystem_iterator_set_mode(iterator, MA_FILESYSTEM_ITERATE_FILES);
		while (MA_OK == ma_filesystem_iterator_get_next(iterator, &file_buffer)) {
			if(is_cert_file(&file_buffer)) {
				ma_temp_buffer_t src_cert_path, dest_cert_path;
				ma_temp_buffer_init(&src_cert_path);
				ma_temp_buffer_init(&dest_cert_path);

				form_path(&src_cert_path, self->params->agent_provisioning_dir, (char *)ma_temp_buffer_get(&file_buffer), MA_FALSE);
				form_path(&dest_cert_path, certstore_path, (char *)ma_temp_buffer_get(&file_buffer), MA_FALSE);
						
				if(MA_OK != (rc = ma_filesystem_copy_file((char *)ma_temp_buffer_get(&src_cert_path), (char *)ma_temp_buffer_get(&dest_cert_path)))) {
					MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to copy certfile (%s) into certstore", (char *)ma_temp_buffer_get(&file_buffer));
				}
				else
					MA_LOG(self->logger, MA_LOG_SEV_INFO, "Added certificate (%s) into certstore", (char *)ma_temp_buffer_get(&file_buffer));

				ma_temp_buffer_uninit(&src_cert_path);			
				ma_temp_buffer_uninit(&dest_cert_path);
			}

			ma_temp_buffer_uninit(&file_buffer);
		}
		(void)ma_filesystem_iterator_release(iterator);
	}

	return rc;
}

static ma_error_t revoke_certificates(ma_config_context_t *self, const char *certstore_path) {
	ma_error_t rc = MA_OK;
	ma_filesystem_iterator_t *iterator = NULL;

	if(MA_OK == (rc = ma_filesystem_iterator_create(certstore_path, &iterator))) {
		ma_temp_buffer_t file_buffer = {0};
		(void)ma_filesystem_iterator_set_mode(iterator, MA_FILESYSTEM_ITERATE_FILES);
		while (MA_OK == ma_filesystem_iterator_get_next(iterator, &file_buffer)) {
			if(is_cert_file(&file_buffer)) {
				ma_temp_buffer_t src_cert_path, dest_cert_path;
			
				ma_temp_buffer_init(&src_cert_path);
				ma_temp_buffer_init(&dest_cert_path);

				form_path(&src_cert_path, self->params->agent_provisioning_dir, (char *)ma_temp_buffer_get(&file_buffer), MA_FALSE);
				form_path(&dest_cert_path, certstore_path, (char *)ma_temp_buffer_get(&file_buffer), MA_FALSE);
						
				if(!file_exists((char *)ma_temp_buffer_get(&src_cert_path))) {
					if(MA_OK == (rc = ma_filesystem_delete_file((char *)ma_temp_buffer_get(&dest_cert_path))))
						MA_LOG(self->logger, MA_LOG_SEV_INFO, "Revoked certificate (%s) from certstore", (char *)ma_temp_buffer_get(&file_buffer));
				}
			

				ma_temp_buffer_uninit(&src_cert_path);			
				ma_temp_buffer_uninit(&dest_cert_path);
			}

			ma_temp_buffer_uninit(&file_buffer);
		}
		(void)ma_filesystem_iterator_release(iterator);
	}

	return rc;
}
