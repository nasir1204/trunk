#include "ma_agent_provisioner.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal/utils/macrypto/ma_crypto.h"
#include "ma/internal/utils/zip/ma_zip_utils.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/utils/network/ma_url_request.h"
#include <stdio.h>

#ifdef MA_WINDOWS
	#include <windows.h>
#else
	#include <termios.h>
	#include <unistd.h>
#endif

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "maconfig"

char *epo_keys_temp_file = NULL;

static void ma_stdin_echo_set(ma_bool_t on){
#ifdef MA_WINDOWS
	DWORD  mode;
	HANDLE hConIn = GetStdHandle( STD_INPUT_HANDLE );
	GetConsoleMode( hConIn, &mode );
	mode = on
		? (mode |   ENABLE_ECHO_INPUT )
		: (mode & ~(ENABLE_ECHO_INPUT));
	SetConsoleMode( hConIn, mode );
#else
	struct termios settings;
	tcgetattr( STDIN_FILENO, &settings );
	settings.c_lflag = on
					? (settings.c_lflag |   ECHO )
					: (settings.c_lflag & ~(ECHO));
	tcsetattr( STDIN_FILENO, TCSANOW, &settings );
#endif

  }

static ma_error_t epo_url_sanity(epo_provision_t *epo_provision){
	if(epo_provision && epo_provision->url && (450 > strlen(epo_provision->url)) ){
		char url_buff[512] = {0};
		char *https_pos = NULL;
		char *port_pos = NULL;

		if(!(https_pos = strstr(epo_provision->url, "https://"))) {
			strcpy(url_buff, "https://");
			https_pos = url_buff;
		}
		strncat(url_buff, epo_provision->url, 510);
		https_pos += strlen("https://");

		if(strchr(https_pos, '[') && (port_pos = strchr(https_pos, ']'))) {
			if(!strchr(port_pos, ':')) {
				strcat(url_buff, ":");
				strcat(url_buff, MA_EPO_DEFAULT_PORT);
			}
		}
		else {
			if(!strrchr(https_pos, ':')) {
				strcat(url_buff, ":");
				strcat(url_buff, MA_EPO_DEFAULT_PORT);
			}
		}
		strcat(url_buff, MA_EPO_PROVISION_LOCATION);
		if(epo_provision->url) free(epo_provision->url);
		epo_provision->url = strdup(url_buff);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_epo_keys_download(ma_config_context_t *context, ma_bytebuffer_t **dn_buff ){
	ma_error_t rc;
	ma_event_loop_t *ev_loop = NULL;
	if(MA_OK == (rc = ma_event_loop_create(&ev_loop))){
		ma_net_client_service_t *net_service = NULL;
		if(MA_OK == (rc = ma_net_client_service_create(ev_loop, &net_service))){ 
            if(MA_OK == (rc = ma_config_configurator_create(context))) {
                ma_crypto_t *crypto = NULL;
                if(MA_OK == (rc = ma_config_crypto_create(context))) {
			        ma_url_request_t *url_request = NULL ;
			        ma_url_request_auth_t auth;
			        auth.password = context->params->epo_provision->user_passwd;
			        auth.user = context->params->epo_provision->user_name;

                    (void)ma_net_client_service_setlogger(net_service, context->logger);                    
			        ma_net_client_service_start(net_service);
			        if(MA_OK == (rc = ma_url_request_create(net_service, context->params->epo_provision->url, &url_request))){
				        ma_url_request_io_t				io_streams = {0};
				        ma_temp_buffer_t path;
				        ma_temp_buffer_init(&path);
				        rc = form_path(&path, context->params->agent_provisioning_dir, MA_EPO_TEMP_KEYS_NAME, MA_FALSE);
				        epo_keys_temp_file = strdup((char*)ma_temp_buffer_get(&path));
				        if(MA_OK == (rc = ma_mstream_create(1024, NULL, &io_streams.write_stream))){
					        unlink(context->params->agent_provisioning_dir);
					        (void)ma_url_request_set_option(url_request, MA_URL_REQUEST_OPTION_CONNECT_TIMEOUT, 60);
					        (void)ma_url_request_set_option(url_request, MA_URL_REQUEST_OPTION_VERBOSE_LOG, 1);
					        (void)ma_url_request_set_option(url_request, MA_URL_REQUEST_OPTION_TRANSFER_TIMEOUT, 30*60);
					        (void)ma_url_request_set_option(url_request, MA_URL_REQUEST_OPTION_REQUEST_TYPE, MA_URL_REQUEST_TYPE_GET);
					        (void)ma_url_request_set_io_callback(url_request, &io_streams);
					        ma_url_request_set_url_auth_info(url_request, &auth);
					        rc = ma_url_request_sync_send(url_request, NULL);
					        if(MA_ERROR_NETWORK_REQUEST_SUCCEEDED == rc){
						        io_streams.write_stream->vtable->get_buffer(io_streams.write_stream, dn_buff);
						        MA_LOG(context->logger, MA_LOG_SEV_DEBUG,"Downloading file from %s to %s succeed", context->params->epo_provision->url, context->params->agent_provisioning_dir);
						        rc = MA_OK;
					        }
					        else{
						        MA_LOG(context->logger, MA_LOG_SEV_ERROR,"Downloading file from %s to %s failed", context->params->epo_provision->url, context->params->agent_provisioning_dir);
					        }
					        ma_stream_release(io_streams.write_stream);
				        }
				        ma_temp_buffer_uninit(&path);
				        ma_url_request_release(url_request);
			        }
			        ma_net_client_service_stop(net_service);             
                }
            }
			ma_net_client_service_release(net_service);            
		}
		ma_event_loop_release(ev_loop);
	}
	return rc;
}



static ma_error_t ma_epo_keys_decode(ma_config_param_t *params, ma_bytebuffer_t *dn_buff){
	ma_error_t rc = MA_ERROR_INVALIDARG;
	if(dn_buff && params) {
		unsigned char *keys_buff = ma_bytebuffer_get_bytes(dn_buff);
		int keys_buff_len= ma_bytebuffer_get_size(dn_buff) ;
		if( (memcmp(keys_buff, "OK:\r\n", 5) == 0) ) {
			ma_bytebuffer_t *decoded_buff;
			(void)ma_bytebuffer_create(1024, &decoded_buff);
			keys_buff = keys_buff + 5;			/* skip over the "OK:\r\n" */
			keys_buff_len -= 5;
			if(MA_OK == (rc = ma_crypto_decode(keys_buff, keys_buff_len, decoded_buff, MA_TRUE))){
				FILE *fp = NULL;
				if(fp = fopen(epo_keys_temp_file, "w")){
					keys_buff_len = fwrite((void*)ma_bytebuffer_get_bytes(decoded_buff), 1,  ma_bytebuffer_get_size(decoded_buff), fp);
					fclose(fp);
					rc = ma_unzip(epo_keys_temp_file, params->agent_provisioning_dir );
					unlink(epo_keys_temp_file);
				}
			}
			(void)ma_bytebuffer_release(decoded_buff);
		}
	}
	return rc;
}

static ma_error_t ma_epo_credentials_get(epo_provision_t *epo_provision){
	char credentials[256] = {0};
	int len;

	if(!epo_provision->user_name){
		printf("%s", "Enter ePO user name:");
		fgets(credentials, 255, stdin);
		len = strlen(credentials);
		/* Removing new line character */
		credentials[len - 1] = '\0';
		epo_provision->user_name = strdup(credentials);
		memset(credentials, 0, 256);
	}

	if(!epo_provision->user_passwd){
		printf("%s", "Enter ePO user password:");
		ma_stdin_echo_set(0);
		fgets(credentials, 255, stdin);
		len = strlen(credentials);
		credentials[len - 1] = '\0';
		epo_provision->user_passwd = strdup(credentials);
		ma_stdin_echo_set(1);
		memset(credentials, 0, 256);
		printf("\n");
	}
	return MA_OK;
}

ma_error_t ma_agent_process_epo_provision(ma_config_context_t *context){
	ma_error_t rc = MA_OK;
	if(MA_OK == (rc = ma_epo_credentials_get(context->params->epo_provision))){
		if(MA_OK == (rc = epo_url_sanity(context->params->epo_provision))){
			ma_bytebuffer_t *dn_buff = NULL;
			if( (MA_OK == (rc = ma_epo_keys_download(context, &dn_buff)))  && (MA_OK == (rc = ma_epo_keys_decode(context->params, dn_buff))) )
				MA_LOG(context->logger, MA_LOG_SEV_DEBUG, "keys from epo downloaded & decode success");
			else
				MA_LOG(context->logger, MA_LOG_SEV_DEBUG, "keys from epo downloaded/decode/uncompress failed");
			ma_bytebuffer_release(dn_buff);
		}else {
			MA_LOG(context->logger, MA_LOG_SEV_ERROR, "Server URL %s  is either not supported or invalid", context->params->epo_provision->url);
		}
	}
	return rc;
}
