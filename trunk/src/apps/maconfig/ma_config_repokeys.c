#include "ma_agent_provisioner.h"
#include "ma/internal/utils/spipe/ma_spipe_package_defs.h"
#include "ma/internal/utils/filesystem/path.h"

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "maconfig"

ma_error_t ma_repository_keys_process(ma_ds_t *ds, char *repo_keys, size_t repo_keys_size);

ma_error_t import_repokeys(ma_config_context_t *self) {
	if(self) {
		ma_error_t rc = MA_ERROR_APIFAILED;
		ma_bytebuffer_t *repokey_buffer = NULL;
		ma_temp_buffer_t path;
		ma_temp_buffer_init(&path);
		form_path(&path, self->params->agent_provisioning_dir, MA_STR_SPIPEPKG_DATA_REPOKEYS_INI, MA_FALSE);
		if( (MA_OK == (rc = ma_config_get_file_buffer((char*)ma_temp_buffer_get(&path), &repokey_buffer))) ) {
			if(ma_bytebuffer_get_size(repokey_buffer)) {
				rc = ma_repository_keys_process(ma_configurator_get_datastore(self->agent_configurator), (char*)ma_bytebuffer_get_bytes(repokey_buffer), ma_bytebuffer_get_size(repokey_buffer));				                
			}
            (void) ma_bytebuffer_release(repokey_buffer);
		}
		ma_temp_buffer_uninit(&path);
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}