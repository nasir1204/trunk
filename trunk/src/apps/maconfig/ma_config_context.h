#ifndef MA_CONFIG_CONTEXT_H_INCLUDED
#define MA_CONFIG_CONTEXT_H_INCLUDED


#include "ma/ma_common.h"
#include "ma/ma_log.h"
#include "ma_config_param.h"
#include "ma_config_utils.h"
#include "ma/logger/ma_log_filter.h"
#include "ma/internal/utils/macrypto/ma_crypto.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/utils/configurator/ma_configurator_utils.h"

MA_CPP( extern "C" { )

typedef struct ma_config_context_s ma_config_context_t, *ma_config_context_h;

struct ma_config_context_s {    
    ma_config_param_t			*params;
    ma_logger_t					*logger;
    ma_crypto_t                 *crypto;	
	ma_configurator_t           *agent_configurator;	
	ma_bytebuffer_t			    *sitelist_data;
    ma_log_filter_t             *log_filter;
    ma_bool_t                   is_file_logger;
};

ma_error_t ma_config_context_create(ma_config_param_t *params, ma_config_context_t **config_context);

ma_error_t ma_config_crypto_create(ma_config_context_t *config_context);

ma_error_t ma_config_configurator_create(ma_config_context_t *config_context);

ma_error_t ma_config_context_release(ma_config_context_t *config_context);

MA_CPP(})

#endif /* MA_CONFIG_CONTEXT_H_INCLUDED */
