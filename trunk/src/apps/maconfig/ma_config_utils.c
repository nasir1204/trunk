#include "ma_config_utils.h"
#include "ma/internal/utils/datastructures/ma_stream.h"

ma_error_t ma_config_get_file_buffer(const char *filename, ma_bytebuffer_t **buffer){
	ma_stream_t *stream = NULL;	
    ma_error_t rc = MA_OK;
	if(MA_OK == (rc = ma_fstream_create(filename, MA_STREAM_ACCESS_FLAG_READ, NULL, &stream))) {		
        rc = ma_stream_get_buffer(stream, buffer);                
		(void) ma_stream_release(stream);
    }
	return rc;
}