#include "ma_agent_provisioner.h"
#include "ma/internal/utils/spipe/ma_spipe_package_defs.h"
#include "ma/internal/utils/datastructures/ma_stream.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal/utils/xml/ma_xml.h"
#include "ma/internal/utils/xml/ma_xml_node.h"
#include "ma/internal/utils/repository/ma_sitelist.h"
#include <stdio.h>

#ifdef MA_WINDOWS
#include <windows.h>
#endif

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "maconfig"
char *fallback_site_xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> <ns:SiteLists xmlns:ns=\"naSiteList\" LocalVersion=\"20030131002737\" Type=\"Client\" GlobalVersion=\"20030131003110\"> <SiteList Default=\"1\" Name=\"Default\"> <HttpSite Name=\"NAIHttp\" ID=\"NAIHttp\" Server=\"update.nai.com:80\" Enabled=\"1\" Order=\"1\" Type=\"fallback\" Local=\"0\"> <RelativePath>products/commonupdater</RelativePath> <UseAuth>0</UseAuth> <UserName/> <Password Encrypted=\"1\">f2mwBTzPQdtnY6QNOsVexH9psAU8z0HbZ2OkDTrFXsR/abAFPM9B3Q==</Password> </HttpSite> <FTPSite Name=\"NAIFtp\" ID=\"NAIFtp\" Server=\"ftp.nai.com:21\" Enabled=\"1\" Order=\"2\" Type=\"fallback\" Local=\"0\"> <RelativePath>CommonUpdater</RelativePath> <UserName>anonymous</UserName> <Password Encrypted=\"1\">MQCBNesmh4xsoov8E4KA/i9ukpwRoD3RDId9bU+InCJ/abAFPM9B3Q==</Password> </FTPSite> </SiteList> </ns:SiteLists>";

ma_error_t check_for_spipe_entries_in_sitelist(ma_config_context_t *self) {
	ma_xml_t *sitelist_xml = NULL;
	ma_error_t rc = MA_ERROR_INVALIDARG;
	if(MA_OK == (rc = ma_xml_create(&sitelist_xml)) ){
        if(MA_OK == (rc = ma_xml_load_from_buffer(sitelist_xml, (char *)ma_bytebuffer_get_bytes(self->sitelist_data), ma_bytebuffer_get_size(self->sitelist_data)))) {
			ma_xml_node_t *root_sitelist_node = ma_xml_get_node(sitelist_xml);
			if(root_sitelist_node) {
				ma_xml_node_t *node = ma_xml_node_search_by_path(root_sitelist_node,MA_SITELISTS_SPIPE_SITE_PATH);
				if(node) { 
					rc = MA_OK;
				}
				else{
					MA_LOG(self->logger, MA_LOG_SEV_ERROR, "SPIPE entry not present in sitelist.");
					rc = MA_ERROR_OBJECTNOTFOUND;
				}
			}
			else{
				MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to get root node.");
				rc = MA_ERROR_OBJECTNOTFOUND;
			}
		}
		else{
			MA_LOG(self->logger, MA_LOG_SEV_ERROR, "xml load from buffre failed, err <%d>", rc);
		}

		ma_xml_release(sitelist_xml);
	}
	else {
		MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to create xml, err <%d>", rc);
	}
	
	return rc;
}


static ma_error_t ma_agent_sitelist_buffer_fill(ma_config_context_t *self){
	ma_error_t rc = MA_OK;	
	if(self->params->agent_mode == MA_AGENT_MODE_MANAGED_INT){
		ma_temp_buffer_t path;
		/*ePO send inconsistent sitelist file name (i.e. "SiteList.xml" or "sitelist.xml"), so need to handle both case. */
		ma_temp_buffer_t path_lowercase;
		ma_temp_buffer_init(&path_lowercase);
		ma_temp_buffer_init(&path);
		form_path(&path, self->params->agent_provisioning_dir, STR_SITELIST_NAME, MA_FALSE);
		form_path(&path_lowercase, self->params->agent_provisioning_dir, STR_SITELIST_NAME_LOWERCASE, MA_FALSE);
        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Trying to load sitelist from location <%s>", self->params->agent_provisioning_dir);
		if( (MA_OK == (rc = ma_config_get_file_buffer((char*)ma_temp_buffer_get(&path), &self->sitelist_data))) ||
				(MA_OK == (rc = ma_config_get_file_buffer((char*)ma_temp_buffer_get(&path_lowercase), &self->sitelist_data))) )
			MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "successfully loaded sitelist.");
		else{
			MA_LOG(self->logger, MA_LOG_SEV_ERROR, "sitelist loading failed, err <%d>. Using fallback sites & starting in unmanaged mode", rc);
			self->params->agent_mode = MA_AGENT_MODE_UNMANAGED_INT;
            if(MA_OK == (rc = ma_bytebuffer_create(strlen(fallback_site_xml)+1, &self->sitelist_data))) {
                ma_bytebuffer_set_bytes(self->sitelist_data, 0, (unsigned char *)fallback_site_xml, strlen(fallback_site_xml));				
            }
		}
		ma_temp_buffer_uninit(&path);
		ma_temp_buffer_uninit(&path_lowercase);
	}
	else{
		MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Unmanaged mode, filling with fall back sites");
		if(MA_OK == (rc = ma_bytebuffer_create(strlen(fallback_site_xml)+1, &self->sitelist_data))) {
			ma_bytebuffer_set_bytes(self->sitelist_data, 0, (unsigned char *)fallback_site_xml, strlen(fallback_site_xml));	
		}		
	}
	return rc;
}

ma_error_t ma_agent_sitelist_process(ma_config_context_t *self){
	if(MA_OK == ma_agent_sitelist_buffer_fill(self)){
		if(self->params->agent_mode == MA_AGENT_MODE_MANAGED_INT){
			if(MA_OK != check_for_spipe_entries_in_sitelist(self)) {
				MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "starting in unmanaged mode, filling with fall back sites.");
				if(self->sitelist_data) {
                    ma_bytebuffer_release(self->sitelist_data);
				}
				if(MA_OK == ma_bytebuffer_create(strlen(fallback_site_xml)+1, &self->sitelist_data)) {
					ma_bytebuffer_set_bytes(self->sitelist_data, 0, (unsigned char *)fallback_site_xml, strlen(fallback_site_xml));	
					self->params->agent_mode = MA_AGENT_MODE_UNMANAGED_INT;
		        }	
				else
					return MA_ERROR_APIFAILED;
			}
		}
		return MA_OK;
	}
	MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Site list fetching failed.");
	return MA_ERROR_APIFAILED;
}
