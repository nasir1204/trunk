#include "ma_config_param.h"
#include "ma_agent_provisioner.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/services/ma_policy_settings_bag.h"
#include "ma/internal/services/ma_agent_policy_settings_bag.h"
#include "ma/policy/ma_policy_uri.h"
#include "ma/../../src/utils/configurator/ma_configurator_internal.h"
#include <stdio.h>

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "maconfig"

ma_error_t ma_config_msgbus_create(ma_msgbus_t **msgbus){
	ma_error_t rc = MA_ERROR_INVALIDARG;
	if(msgbus){
		if(MA_OK == (rc = ma_msgbus_create(MA_CONFIG_PRODUCT_ID_STR, msgbus)))
			rc = ma_msgbus_start(*msgbus);
	}
	return rc;
}

void ma_config_msgbus_release(ma_msgbus_t *msgbus){
	if(msgbus){
		(void)ma_msgbus_stop(msgbus, MA_TRUE);
		(void)ma_msgbus_release(msgbus);
	}
}

ma_error_t update_policy_db_with_logsettings(ma_config_context_t *context){
	ma_policy_settings_bag_t *policy_settings_bag = NULL;
	ma_error_t rc = MA_ERROR_APIFAILED;
	if(MA_OK == (rc = ma_config_configurator_create(context))){
		ma_datastore_configuration_request_t req = {{MA_FALSE,0}, {MA_TRUE,MA_DB_OPEN_READWRITE}, {MA_FALSE,0}, {MA_FALSE,0}, {MA_FALSE,0}, {MA_FALSE,0}, {MA_FALSE, 0}, {MA_FALSE, 0}, {MA_FALSE, 0}, {MA_FALSE, 0}, {MA_FALSE, 0}, {MA_FALSE, 0}, {MA_FALSE, 0}, {MA_FALSE, 0}, {MA_FALSE, 0}};
		if(MA_OK == (rc = ma_configurator_intialize_datastores(context->agent_configurator, &req))) { 
		if(MA_OK == (rc = ma_agent_policy_settings_bag_create(ma_configurator_get_policies_database(context->agent_configurator), &policy_settings_bag))){
			ma_policy_bag_t** policy_bag = NULL;
			if(policy_bag = ma_agent_policy_settings_bag_get_policy_bag(policy_settings_bag)){
				char *filter_pattern = NULL;
				char *is_app_log_enabled = NULL;
				if(!strcmp("0", context->params->loglevel)){
					MA_LOG(context->logger, MA_LOG_SEV_INFO, "Disabling logging");
					is_app_log_enabled = strdup("0");
				}else if(!strcmp("1", context->params->loglevel)){
					MA_LOG(context->logger, MA_LOG_SEV_INFO, "Setting loglevel to Info");
					is_app_log_enabled = strdup("1");
					filter_pattern = strdup("*.Info");
				}else if(!strcmp("2", context->params->loglevel)){
					MA_LOG(context->logger, MA_LOG_SEV_INFO, "Setting loglevel to Debug");
					is_app_log_enabled = strdup("1");
					filter_pattern = strdup("*.Debug");
				}else if(!strcmp("3", context->params->loglevel)){
					MA_LOG(context->logger, MA_LOG_SEV_INFO, "Setting loglevel to Trace");
					is_app_log_enabled = strdup("1");
					filter_pattern = strdup("*.Trace");
				}else{
					MA_LOG(context->logger, MA_LOG_SEV_ERROR, "io service recieved invalid loglevel <%s>", context->params->loglevel);
				}
                
				if(filter_pattern || is_app_log_enabled){
					ma_policy_uri_t *general_uri = NULL;              
					if(MA_OK == (rc = ma_policy_uri_create(&general_uri))) {
						ma_variant_t *value = NULL;
						ma_policy_uri_set_feature(general_uri, MA_POLICY_FEATURE_ID); 
						ma_policy_uri_set_category(general_uri, MA_POLICY_CATEGORY_ID);    
						ma_policy_uri_set_type(general_uri, MA_POLICY_TYPE_ID); 
						ma_policy_uri_set_software_id(general_uri, MA_SOFTWAREID_GENERAL_STR);    
						if(is_app_log_enabled){
							if(MA_OK == ma_variant_create_from_string(is_app_log_enabled, strlen(is_app_log_enabled), &value)) {
								ma_policy_bag_set_value(*policy_bag, general_uri, MA_LOGGER_SERVICE_SECTION_NAME_STR, MA_LOGGER_KEY_LOG_APP_LOGGING_ENABLED_INT, value);
								(void) ma_variant_release(value); value = NULL;
							}
							free(is_app_log_enabled);
						}
						if(filter_pattern){
							if(MA_OK == ma_variant_create_from_string(filter_pattern, strlen(filter_pattern), &value)) {
								ma_policy_bag_set_value(*policy_bag, general_uri, MA_LOGGER_SERVICE_SECTION_NAME_STR, MA_LOGGER_KEY_FILTER_PATTERN_STR, value);
								(void) ma_variant_release(value); value = NULL;
							}
							free(filter_pattern);
						}
						if(MA_OK != (rc = ma_import_policies(((ma_configurator_ex_t*)context->agent_configurator)->db_policy, general_uri, *policy_bag))){
							MA_LOG(context->logger, MA_LOG_SEV_ERROR, "import policies failed");
						}
						(void)ma_policy_uri_release(general_uri);
					}
					else{
						if(is_app_log_enabled)
							free(is_app_log_enabled);
						if(filter_pattern)
							free(filter_pattern);
					}
				}
			}
			policy_settings_bag->methods->destroy(policy_settings_bag);
		}
		}
	}
	return rc;
}

ma_error_t update_agent_loglevel_settings(ma_config_context_t *context) {
    if(context) {
        ma_error_t err = MA_OK;
        ma_msgbus_endpoint_t *endpoint = NULL;
        ma_message_t *request = NULL;	
		ma_msgbus_t *msgbus = NULL;
        MA_LOG(context->logger, MA_LOG_SEV_TRACE, "sending loglevel settings..");
		if(MA_OK == (err = (ma_config_msgbus_create(&msgbus)))) {
			if(MA_OK == (err = ma_message_create(&request))) {
				(void)ma_message_set_property(request, MA_SERVICE_NAME_MSG, "logger");
				(void)ma_message_set_property(request, MA_IO_SERVICE_MSG_TYPE, MA_IO_MSG_TYPE_LOGLEVEL_SETTING_CHANGED_STR);
				(void)ma_message_set_property(request, MA_IO_CONFIGURED_LOGLEVEL_STR, context->params->loglevel);
				if(MA_OK == (err = ma_msgbus_endpoint_create(msgbus, MA_IO_SERVICE_NAME_STR, MA_IO_SERVICE_NAME_STR, &endpoint))) {
					if(MA_OK == (err = ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC))) {
						ma_message_t *response = NULL;
						if(MA_OK == (err = ma_msgbus_send(endpoint, request, &response))) {
							MA_LOG(context->logger, MA_LOG_SEV_INFO, "sending loglevel settings successful");
							ma_message_release(response);
						}
						else{
							MA_LOG(context->logger, MA_LOG_SEV_ERROR, "sending loglevel settings failed, last error(%d). Adding directly to DB", err);
							err = update_policy_db_with_logsettings(context);
						}
					}
					(void)ma_msgbus_endpoint_release(endpoint);
				}
				(void)ma_message_release(request);
			}
			ma_config_msgbus_release(msgbus);
			return err;
		}
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t update_agent_language_change_settings(ma_config_context_t *context) {
    if(context) {
        ma_error_t err = MA_OK;
        ma_msgbus_endpoint_t *endpoint = NULL;
        ma_message_t *request = NULL;	
		ma_msgbus_t *msgbus = NULL;
        MA_LOG(context->logger, MA_LOG_SEV_TRACE, "sending lanugage settings..");
		if(MA_OK == (err = (ma_config_msgbus_create(&msgbus)))) {
			if(MA_OK == (err = ma_message_create(&request))) {
				(void)ma_message_set_property(request, MA_IO_SERVICE_MSG_TYPE, MA_IO_MSG_TYPE_LANG_SETTING_CHANGED_STR);
				(void)ma_message_set_property(request, MA_IO_CONFIGURED_LANGUAGE_STR, context->params->language);
				if(MA_OK == (err = ma_msgbus_endpoint_create(msgbus, MA_IO_SERVICE_NAME_STR, MA_IO_SERVICE_NAME_STR, &endpoint))) {
					if(MA_OK == (err = ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC))) {
						ma_message_t *response = NULL;
						if(MA_OK == (err = ma_msgbus_send(endpoint, request, &response))) {
							MA_LOG(context->logger, MA_LOG_SEV_INFO, "sending lanugage settings successful");
							ma_message_release(response);
						}
						else
							MA_LOG(context->logger, MA_LOG_SEV_ERROR, "sending lanugage settings failed, last error(%d)", err);
					}
					(void)ma_msgbus_endpoint_release(endpoint);
				}
				(void)ma_message_release(request);
			}
			ma_config_msgbus_release(msgbus);
			return err;
		}
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t reset_agent_language_settings(ma_config_context_t *context) {
    if(context) {
        ma_error_t err = MA_OK;
        ma_msgbus_endpoint_t *endpoint = NULL;
        ma_message_t *request = NULL;	
		ma_msgbus_t *msgbus = NULL;
        MA_LOG(context->logger, MA_LOG_SEV_TRACE, "resetting agent lanugage settings..");
		if(MA_OK == (err = (ma_config_msgbus_create(&msgbus)))) {
			if(MA_OK == (err = ma_message_create(&request))) {
				(void)ma_message_set_property(request, MA_IO_SERVICE_MSG_TYPE, MA_IO_MSG_TYPE_LANG_SETTING_RESET_STR);
				if(MA_OK == (err = ma_msgbus_endpoint_create(msgbus, MA_IO_SERVICE_NAME_STR, MA_IO_SERVICE_NAME_STR, &endpoint))) {
					if(MA_OK == (err = ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC))) {
						ma_message_t *response = NULL;
						if(MA_OK == (err = ma_msgbus_send(endpoint, request, &response))) {
							MA_LOG(context->logger, MA_LOG_SEV_INFO, "resetting agent lanugage settings successful");
							ma_message_release(response);
						}
						else
							MA_LOG(context->logger, MA_LOG_SEV_ERROR, "resetting agent lanugage settings failed, last error(%d)", err);
					}
					(void)ma_msgbus_endpoint_release(endpoint);
				}
				(void)ma_message_release(request);
			}
			ma_config_msgbus_release(msgbus);
			return err;
		}
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_config_custom_props_set(ma_config_context_t *context) {
    if(context) {
        ma_error_t err = MA_OK;
        ma_msgbus_endpoint_t *endpoint = NULL;
        ma_message_t *request = NULL;	
		ma_msgbus_t *msgbus = NULL;
        MA_LOG(context->logger, MA_LOG_SEV_TRACE, "sending custom props settings..");
		if(MA_OK == (err = (ma_config_msgbus_create(&msgbus)))) {
			if(MA_OK == (err = ma_message_create(&request))) {
				(void)ma_message_set_property(request, MA_PROP_KEY_REQUEST_TYPE_STR, MA_CONFIG_MSG_TYPE_CUSTOM_PROPS_STR);
				if(context->params->custome_props->custom_prop1)
					(void)ma_message_set_property(request, MA_CONFIG_CUSTOM_PROP1_STR, context->params->custome_props->custom_prop1);
				if(context->params->custome_props->custom_prop2)
					(void)ma_message_set_property(request, MA_CONFIG_CUSTOM_PROP2_STR, context->params->custome_props->custom_prop2);
				if(context->params->custome_props->custom_prop3)
					(void)ma_message_set_property(request, MA_CONFIG_CUSTOM_PROP3_STR, context->params->custome_props->custom_prop3);
				if(context->params->custome_props->custom_prop4)
					(void)ma_message_set_property(request, MA_CONFIG_CUSTOM_PROP4_STR, context->params->custome_props->custom_prop4);
				if(MA_OK == (err = ma_msgbus_endpoint_create(msgbus, MA_IO_SERVICE_NAME_STR, MA_IO_SERVICE_NAME_STR, &endpoint))) {
					if(MA_OK == (err = ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC))) {
						ma_message_t *response = NULL;
						if(MA_OK == (err = ma_msgbus_send(endpoint, request, &response))) {
							MA_LOG(context->logger, MA_LOG_SEV_INFO, "sending custom props successful");
							ma_message_release(response);
						}
						else
							MA_LOG(context->logger, MA_LOG_SEV_ERROR, "sending custom props failed, last error(%d)", err);
					}
					(void)ma_msgbus_endpoint_release(endpoint);
				}
				(void)ma_message_release(request);
			}
			ma_config_msgbus_release(msgbus);
			return err;
		}
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_config_license_key_set(ma_config_context_t *context) {
    if(context) {
        ma_error_t err = MA_OK;
        ma_msgbus_endpoint_t *endpoint = NULL;
        ma_message_t *request = NULL;	
		ma_msgbus_t *msgbus = NULL;
        MA_LOG(context->logger, MA_LOG_SEV_TRACE, "sending license key..");
		if(MA_OK == (err = (ma_config_msgbus_create(&msgbus)))) {
			if(MA_OK == (err = ma_message_create(&request))) {
				(void)ma_message_set_property(request, MA_PROP_KEY_REQUEST_TYPE_STR, MA_CONFIG_MSG_TYPE_LICENSE_KEY_STR);
				(void)ma_message_set_property(request, MA_PROP_VALUE_LICENSE_KEY_STR, context->params->license_key);
				if(MA_OK == (err = ma_msgbus_endpoint_create(msgbus, MA_IO_SERVICE_NAME_STR, MA_IO_SERVICE_NAME_STR, &endpoint))) {
					if(MA_OK == (err = ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC))) {
						ma_message_t *response = NULL;
						if(MA_OK == (err = ma_msgbus_send(endpoint, request, &response))) {
							MA_LOG(context->logger, MA_LOG_SEV_INFO, "sending license key successful");
							ma_message_release(response);
						}
						else
							MA_LOG(context->logger, MA_LOG_SEV_ERROR, "sending license key failed, last error(%d)", err);
					}
					(void)ma_msgbus_endpoint_release(endpoint);
				}
				(void)ma_message_release(request);
			}
			ma_config_msgbus_release(msgbus);
		}
		return err;
	}
    return MA_ERROR_INVALIDARG;
}
