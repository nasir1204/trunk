#ifndef MA_AGENT_PROVISIONER_H_INCLUDED
#define MA_AGENT_PROVISIONER_H_INCLUDED

#include "ma/ma_common.h"
#include "ma_config_context.h"

MA_CPP( extern "C" { )

ma_error_t ma_agent_provisioner_do(ma_config_context_t *config_context);

ma_error_t ma_agent_provisioner_key_import(ma_config_context_t *config_context);

ma_error_t ma_agent_process_epo_provision(ma_config_context_t *config_context);

ma_error_t ma_config_sitelist_handle(ma_config_context_t *config_context);

MA_CPP(})

#endif /* MA_AGENT_PROVISIONER_H_INCLUDED */

