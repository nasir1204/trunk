#ifndef MA_CONFIG_INTERNAL_H_INCLUDED
#define MA_CONFIG_INTERNAL_H_INCLUDED

#include "ma/ma_common.h"
#include "ma_agent_provisioner.h"

MA_CPP( extern "C" { )

ma_error_t ma_config_crypto_initialize(ma_config_context_t *config_context);

ma_error_t ma_config_crypto_keystore_setup(ma_config_context_t *self, char *key_location);

void ma_config_keystore_remove(char *location);

ma_error_t ma_agent_sitelist_process(ma_config_context_t *self);

ma_error_t check_for_spipe_entries_in_sitelist(ma_config_context_t *self);

ma_error_t update_agent_language_change_settings(ma_config_context_t *config);

ma_error_t reset_agent_language_settings(ma_config_context_t *config);

ma_error_t ma_config_custom_props_set(ma_config_context_t *config);

ma_error_t ma_config_license_key_set(ma_config_context_t *config);

ma_error_t update_agent_loglevel_settings(ma_config_context_t *context);

MA_CPP(})
#endif
