#include "ma_config_param.h"
#include "ma_config_internal.h"
#include "ma_agent_provisioner.h"
#include "ma_config_upgrade.h"

#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/utils/spipe/ma_spipe_package_defs.h"
#include "ma/internal/utils/datastructures/ma_stream.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/utils/macrypto/ma_crypto.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/getopt/ma_getopt.h"
#include "ma/internal/defs/ma_io_defs.h"
#include "ma/internal/defs/ma_general_defs.h"
#include "ma/internal/utils/platform/ma_sys_apis.h"
#include "ma/internal/utils/portability/fs.h"
#include "ma/internal/utils/filesystem/filesystem_utils.h"
#include "ma/internal/utils/filesystem/ma_fs_utils.h"
#include "ma/internal/utils/xml/ma_xml.h"
#include "ma/internal/utils/xml/ma_xml_node.h"
#include "ma/datastore/ma_ds_registry.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "maconfig"

#define MA_DEFAULT_LANG "english"

#if defined(MA_WINDOWS)
# include <windows.h>
# include <tchar.h>
#else 
# include <unistd.h>
# include <sys/types.h>
# include <dirent.h>

# ifndef MA_AGENT_SOFTWARE_ID
#  define MA_AGENT_SOFTWARE_ID            "EPOAGENT3000"
# endif								

# define MA_SERVICE_CONFIG_CONFIGURATION_NODE "Configuration"				//TBD: double check this 
# define MA_SERVICE_CONFIG_STARTCOMMAND "StartCommand"						//TBD: double check this 
# define MA_SERVICE_CONFIG_STOPCOMMAND "StopCommand"							//TBD: double check this 
#endif

#define MA_FRAMEWORK_MANIFEST_FILE_NAME	"FrameworkManifest.xml"

static ma_error_t service_start(ma_config_context_t *self);
static ma_error_t service_stop(ma_config_context_t *self);
static ma_error_t custome_prop_update(ma_config_context_t *context);
static ma_error_t language_update(ma_config_context_t *context);
ma_error_t import_repokeys(ma_config_context_t *self);

ma_error_t ma_config_param_init(ma_config_param_t *config){
	if(config) {
		config->agent_provisioning_dir = NULL;
		config->agent_mode = MA_AGENT_MODE_UNMANAGED_INT;
		config->original_agent_mode = MA_AGENT_MODE_UNMANAGED_INT;
		config->agent_start_mode = 1;					/*Default it should be start*/
		config->vdi_mode = 0;
		config->seq_num = -1;
		config->agent_crypto_mode = MA_CRYPTO_MODE_NON_FIPS;
		config->agent_crypto_role = MA_CRYPTO_ROLE_OFFICER;
		config->command = NULL;
		strcpy(config->language, MA_DEFAULT_LANG);
        config->need_check = 0;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_config_param_deinit(ma_config_param_t *config){
	if(config) {
		if(config->tenant_id)free(config->tenant_id);
		if(config->agent_provisioning_dir)free(config->agent_provisioning_dir);
		if(config->agent_guid)free(config->agent_guid);
		if(config->custome_props){
			if(config->custome_props->custom_prop1)free(config->custome_props->custom_prop1);
			if(config->custome_props->custom_prop2)free(config->custome_props->custom_prop2);
			if(config->custome_props->custom_prop3)free(config->custome_props->custom_prop3);
			if(config->custome_props->custom_prop4)free(config->custome_props->custom_prop4);
			free(config->custome_props);
		}

		if(config->epo_provision){
			if(config->epo_provision->url) free(config->epo_provision->url);
			if(config->epo_provision->user_name) free(config->epo_provision->user_name);
			if(config->epo_provision->user_passwd) free(config->epo_provision->user_passwd);
			free(config->epo_provision);
		}
		if(config->license_key) free(config->license_key);
		if(config->agent_log_dir) free(config->agent_log_dir);
		if(config->loglevel) free(config->loglevel);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

static ma_error_t temp_directory_create(ma_config_context_t *self){
	if(self && self->params && self->params->agent_provisioning_dir) {
		ma_temp_buffer_t path = MA_TEMP_BUFFER_INIT;
		char temp_path[256] = {0};
		MA_MSC_SELECT(_snprintf, snprintf)(temp_path, 255, "%u", ma_sys_rand());
		form_path(&path, self->params->agent_provisioning_dir, temp_path, MA_FALSE);
		free(self->params->agent_provisioning_dir);
		self->params->agent_provisioning_dir = strdup((char*)ma_temp_buffer_get(&path));
		ma_temp_buffer_uninit(&path);
		if (MA_OK != ma_mkpath(self->params->agent_provisioning_dir)) {
			return MA_ERROR_APIFAILED;
		}
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

/*TBD:	Need to be replace with uv fs function.
		But currently that function is not working, so using it.
*/
static ma_error_t temp_directory_delete(const char *path) {
	
	ma_temp_buffer_t directory_buffer;
	ma_temp_buffer_init(&directory_buffer);
	form_path(&directory_buffer, path, "", MA_FALSE);
	
#ifdef MA_WINDOWS
	{
		WIN32_FIND_DATAA fdd; 
		HANDLE hFindFile = INVALID_HANDLE_VALUE;
		char *p_directory_buffer = NULL;
		form_path(&directory_buffer, (char*)ma_temp_buffer_get(&directory_buffer), "*", MA_FALSE);
		p_directory_buffer = (char*)ma_temp_buffer_get(&directory_buffer);
		hFindFile = FindFirstFileA(p_directory_buffer, &fdd);
		if(hFindFile != INVALID_HANDLE_VALUE) {
			ma_temp_buffer_t subfolder_path;
			ma_temp_buffer_init(&subfolder_path);
			do{
				if(0 == strcmp(fdd.cFileName, ".") || 0 == strcmp(fdd.cFileName,"..")){
					FindNextFileA(hFindFile, &fdd);
					continue;
				}
				form_path(&subfolder_path, path, fdd.cFileName,MA_FALSE);
				unlink((char*)ma_temp_buffer_get(&subfolder_path));
			}while(FindNextFileA(hFindFile, &fdd));
			ma_temp_buffer_uninit(&subfolder_path);
			FindClose(hFindFile);
		}
		RemoveDirectoryA(path);
	}
#else
	{
		DIR *p_dir;
		struct dirent *p_ent;

		p_dir = opendir((char*)ma_temp_buffer_get(&directory_buffer));
		if(p_dir) {
			while(NULL != (p_ent = readdir(p_dir))){
				ma_temp_buffer_t temp_buffer;
				struct stat st;
				char *p_temp_buffer = NULL;
				ma_temp_buffer_init(&temp_buffer);
				ma_temp_buffer_reserve(&temp_buffer, strlen(path) + strlen(p_ent->d_name));
				form_path(&temp_buffer, path, p_ent->d_name,MA_FALSE);
				p_temp_buffer = (char *) ma_temp_buffer_get(&temp_buffer);
				if(	-1 == stat(p_temp_buffer, &st) ||
					0 == strcmp(p_ent->d_name,".") ||
					0 == strcmp(p_ent->d_name,"..")) {
					ma_temp_buffer_uninit(&temp_buffer);
					continue;
				}

				unlink(p_temp_buffer);
				ma_temp_buffer_uninit(&temp_buffer);
			}
			closedir(p_dir);
			rmdir(path);
		}
	}
#endif
	ma_temp_buffer_uninit(&directory_buffer);
	return MA_OK;
}

static ma_error_t update_framework_manifest(ma_config_context_t *self) {
	ma_error_t rc = MA_OK;
#ifdef MA_WINDOWS
	rc = MA_ERROR_INVALIDARG;
	if(self){
		char framework_manifest_path[MA_MAX_LEN] = {0};
		const char *data_path= NULL;
		if(data_path = ma_configurator_get_agent_data_path(self->agent_configurator)){
			ma_xml_t *framework_manifest_xml = NULL ;
			MA_MSC_SELECT(_snprintf, snprintf)(framework_manifest_path, MA_MAX_LEN - 1,"%s%c%s", data_path, MA_PATH_SEPARATOR, MA_FRAMEWORK_MANIFEST_FILE_NAME);
			if( MA_OK == (rc = ma_xml_create(&framework_manifest_xml))){
				if(MA_OK == (rc = ma_xml_load_from_file(framework_manifest_xml, framework_manifest_path))) {
					ma_xml_node_t *framework_manifest_root = NULL ;
					if(NULL != (framework_manifest_root = ma_xml_get_node(framework_manifest_xml))) {
						ma_xml_node_t *componenat_id = NULL ;
						if(componenat_id = ma_xml_node_search_by_path(framework_manifest_root, "FrameworkManifest/Components")){
							ma_xml_node_t *agent_root = NULL ;
							if(NULL != (agent_root = ma_xml_node_get_first_child(componenat_id))) {
								const char *name = ma_xml_node_attribute_get(agent_root, "name");
								if(name && !strcmp(name, "Agent")){
									if(MA_OK ==  (rc = ma_xml_node_attribute_set(agent_root, "refcount", (self->params->agent_mode?"1":"0"))))
										rc = ma_xml_save_to_file(framework_manifest_xml, framework_manifest_path);
								}
							}
						}
					}
				}
				(void)ma_xml_release(framework_manifest_xml);
			}
		}
	}
#endif
	return rc;
}

static void remove_guid_from_registry(){
	ma_error_t rc = MA_ERROR_INVALIDARG;
#ifdef MA_WINDOWS    
	ma_ds_t *ds = NULL;
	if(MA_OK == ma_ds_registry_open(HKEY_LOCAL_MACHINE, SZ_COMPAT_ePO_AGENT_REG_PATH,  MA_DS_REGISTRY_32BIT_VIEW, MA_FALSE, KEY_ALL_ACCESS, (ma_ds_registry_t **) &ds)) {
		ma_ds_rem(ds, "", MA_CONFIGURATION_KEY_AGENT_GUID_STR, MA_FALSE);
		ma_ds_release(ds);
	}else if(MA_OK == ma_ds_registry_open(HKEY_LOCAL_MACHINE, SZ_COMPAT_ePO_AGENT_REG_PATH,  MA_FALSE, MA_FALSE, KEY_ALL_ACCESS, (ma_ds_registry_t **) &ds)){
		ma_ds_rem(ds, "", MA_CONFIGURATION_KEY_AGENT_GUID_STR, MA_FALSE);
		ma_ds_release(ds);
	}

	if(MA_OK == ma_ds_registry_open(HKEY_LOCAL_MACHINE, SZ_COMPAT_TVD_FRAMEWORK_REG_PATH,  MA_DS_REGISTRY_32BIT_VIEW, MA_FALSE, KEY_ALL_ACCESS, (ma_ds_registry_t **) &ds)) {
		ma_ds_rem(ds, "", MA_CONFIGURATION_KEY_AGENT_GUID_STR, MA_FALSE);
		ma_ds_release(ds);
	}else if(MA_OK == ma_ds_registry_open(HKEY_LOCAL_MACHINE, SZ_COMPAT_TVD_FRAMEWORK_REG_PATH,  MA_FALSE, MA_FALSE, KEY_ALL_ACCESS, (ma_ds_registry_t **) &ds)){
		ma_ds_rem(ds, "", MA_CONFIGURATION_KEY_AGENT_GUID_STR, MA_FALSE);
		ma_ds_release(ds);
	}
#endif
}

/*		  1) stop service if it running
		  2) remove guid from db & registry
		  3) do not start service.
		  4) Its user responsibility to set guid before starting service, otherwise agent create ity at service startup
*/
static ma_error_t ma_config_noguid_handler(ma_config_context_t *context) {
	ma_error_t rc = MA_ERROR_INVALIDARG;
    if(context) {
		service_stop(context);
		if(MA_OK == (rc = ma_config_configurator_create(context)))	{
			ma_datastore_configuration_request_t req = {{MA_TRUE,MA_DB_OPEN_READWRITE}, {MA_TRUE,MA_DB_OPEN_READWRITE}, {MA_TRUE,MA_DB_OPEN_READWRITE}, {MA_TRUE,MA_DB_OPEN_READWRITE},{MA_TRUE,MA_DB_OPEN_READWRITE}, {MA_TRUE, MA_DB_OPEN_READWRITE}, {MA_TRUE, MA_DB_OPEN_READWRITE}, {MA_TRUE, MA_DB_OPEN_READWRITE}, {MA_TRUE, MA_DB_OPEN_READWRITE}, {MA_TRUE, MA_DB_OPEN_READWRITE}, {MA_TRUE, MA_DB_OPEN_READWRITE}, {MA_TRUE, MA_DB_OPEN_READWRITE}, {MA_TRUE, MA_DB_OPEN_READWRITE}, {MA_TRUE, MA_DB_OPEN_READWRITE}, {MA_TRUE, MA_DB_OPEN_READWRITE}};
			if(MA_OK == (rc = ma_configurator_intialize_datastores(context->agent_configurator, &req))) {
				/*change the ownership of macmnsvc db file*/
				if(MA_OK == (rc = ma_configurator_reconfigure_datastores_permissions(context->agent_configurator))){
					ma_ds_t *ds = NULL;
					if(ds = ma_configurator_get_datastore(context->agent_configurator)) {
						ma_ds_rem(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_GUID_STR, MA_FALSE);
						remove_guid_from_registry();
					}else
						MA_LOG(context->logger, MA_LOG_SEV_ERROR, "get datastore failed, last error(%d)", rc);
				}else
					MA_LOG(context->logger, MA_LOG_SEV_ERROR, "datastore permision chage failed, last error(%d)", rc);
			}else
				MA_LOG(context->logger, MA_LOG_SEV_ERROR, "Initializing datastore failed, last error(%d)", rc);
		}else
			MA_LOG(context->logger, MA_LOG_SEV_ERROR, "configurator create failed, last error(%d)", rc);
	}
	return rc;
}

ma_error_t ma_config_param_handle(ma_config_context_t *self){
    ma_error_t rc = MA_OK;
    char *command = self->params->command;
	
    if(!strcmp(MA_CONFIG_PROVISION,command)){	
        rc = MA_ERROR_OUTOFMEMORY;
        MA_LOG(self->logger, MA_LOG_SEV_INFO, "Starting agent provisioning.");
		service_stop(self);
        rc = ma_agent_provisioner_do(self);
		/* Apply custom props */
		if(self->params->custome_props){
			if(MA_OK != (rc = custome_prop_update(self))) {
				MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Applying Custom Props failed, last error(%d)", rc);
			}
		}

		if(self->params->set_lang){
			if(MA_OK != (rc = language_update(self)))
				MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Setting agent language, last error(%d)", rc);
		}

		(void)import_repokeys(self);
		(void)update_framework_manifest(self);

		if(self->params->agent_start_mode)
			service_start(self);

    }
	else if(!strcmp(MA_CONFIG_PROVISION_MANAGED_AUTO,command)){
		/*create temp download directory, inside user specified location*/
		if(MA_OK == temp_directory_create(self)) {
			if(MA_OK != (rc = ma_agent_process_epo_provision(self))){
				MA_LOG(self->logger, MA_LOG_SEV_ERROR, "provision processing from epo failed.");
				temp_directory_delete(self->params->agent_provisioning_dir);
				return rc;
			}
			service_stop(self);
			rc = ma_agent_provisioner_do(self);
			/* Apply custom props */
			if(self->params->custome_props){
				if(MA_OK != (rc = custome_prop_update(self))) {
					MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Applying Custom Props failed, last error(%d)", rc);
				}
			}

			if(self->params->set_lang){
				if(MA_OK != (rc = language_update(self)))
					MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Setting agent language, last error(%d)", rc);
			}

			(void)import_repokeys(self);
			(void)update_framework_manifest(self);

			/*Remove downloaded keys & temp directory*/
			temp_directory_delete(self->params->agent_provisioning_dir);

			if(self->params->agent_start_mode)
				service_start(self);
		}
		else {
			MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Temp directory creation failed, last error(%d)", rc);
		}
    }
    else if(!strcmp(MA_CONFIG_START, command)){
        MA_LOG(self->logger, MA_LOG_SEV_INFO, "Starting McAfee agent.");
		service_start(self);
    }
    else if(!strcmp(MA_CONFIG_STOP, command)){
        MA_LOG(self->logger, MA_LOG_SEV_INFO, "Stopping McAfee agent.");
        service_stop(self);
    }
    else if(!strcmp(MA_CONFIG_IMPORT_KEYS, command)){
        MA_LOG(self->logger, MA_LOG_SEV_INFO, "Importing keys.");	
        service_stop(self);
        rc = ma_agent_provisioner_key_import(self);
		
		/* Apply custom props */
		if(self->params->custome_props){
			if(MA_OK != (rc = custome_prop_update(self))) {
				MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Applying Custom Props failed, last error(%d)", rc);
			}
		}

		if(self->params->set_lang){
			if(MA_OK != (rc = language_update(self)))
				MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Setting agent language, last error(%d)", rc);
		}

        service_start(self);
    }
	else if(!strcmp(MA_CONFIG_IMPORT_CERTS, command)) {
		MA_LOG(self->logger, MA_LOG_SEV_INFO, "Importing certs.");
		if(MA_OK != (rc = ma_config_import_certs(self)))
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to import certs, last error(%d)", rc);    
		else
			MA_LOG(self->logger, MA_LOG_SEV_INFO, "succeeded to import certs");    
	}
    else if(!strcmp(MA_CONFIG_CUSTOM, command)){
        MA_LOG(self->logger, MA_LOG_SEV_INFO, "Applying Custom Props.");	
        if(MA_OK != (rc = ma_config_custom_props_set(self)))
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Applying Custom Props failed, last error(%d)", rc);
		else
			MA_LOG(self->logger, MA_LOG_SEV_INFO, "Applying Custom Props passed");
    } 
    else if(!strcmp(MA_CONFIG_IMPORT_DATA, command)){      
        MA_LOG(self->logger, MA_LOG_SEV_INFO, "Upgrading Data from(%s).", self->params->agent_provisioning_dir );
        if(MA_OK != (rc = ma_config_upgrade_import_data(self)))
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to upgrade data, last error(%d)", rc);    
		else
			MA_LOG(self->logger, MA_LOG_SEV_INFO, "succeeded to upgrade data");    

        (void)import_repokeys(self);
    } 
	else if(!strcmp(MA_CONFIG_LICENSE, command)){   
        MA_LOG(self->logger, MA_LOG_SEV_INFO, "Applying license key.");	
        if(MA_OK != (rc = ma_config_license_key_set(self)))
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Applying license key failed, last error(%d)", rc);
		else
			MA_LOG(self->logger, MA_LOG_SEV_INFO, "Applying license key passed");
    }
	else if(!strcmp(MA_CONFIG_ENFORCE_NOGUID, command)){  
        MA_LOG(self->logger, MA_LOG_SEV_INFO, "enforcing noguid.");	
        if(MA_OK != (rc = ma_config_noguid_handler(self)))
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "enforcing noguid failed, last error(%d)", rc);
        else
			MA_LOG(self->logger, MA_LOG_SEV_INFO, "enforcing noguid passed");
    }
	else if(self->params->reset_lang) {
        strcpy(self->params->language, MA_DEFAULT_LANG);
        self->params->reset_lang = MA_FALSE;
        if(MA_OK != (rc = reset_agent_language_settings(self)))
			MA_LOG(self->logger, MA_LOG_SEV_ERROR, "language resetting failed, last error(%d)", rc);
		else
			MA_LOG(self->logger, MA_LOG_SEV_INFO, "language resetting passed.");
    } 
	else if(self->params->set_lang) {
        self->params->set_lang = MA_FALSE;
        if(MA_OK != (rc = update_agent_language_change_settings(self)))
			MA_LOG(self->logger, MA_LOG_SEV_ERROR, "language setting failed, last error(%d)", rc);
		else
			MA_LOG(self->logger, MA_LOG_SEV_INFO, "language resetting passed.");
    }
	else if(self->params->loglevel) {
        if(MA_OK != (rc = update_agent_loglevel_settings(self)))
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "loglevel setting failed, last error(%d)", rc);
		else
			MA_LOG(self->logger, MA_LOG_SEV_INFO, "loglevel setting passed.");
    }
	MA_LOG(self->logger, MA_LOG_SEV_INFO, "configuration finished");
    return rc;
}		


static ma_error_t custome_prop_update(ma_config_context_t *context){
	ma_ds_t *ds = NULL;
	ma_error_t rc = MA_ERROR_INVALIDARG;
	MA_LOG(context->logger, MA_LOG_SEV_DEBUG, "Applying custom props.");
	ds = ma_configurator_get_datastore(context->agent_configurator);
	if(ds){
		if(context->params->custome_props->custom_prop1)
			rc = ma_ds_set_str(ds, MA_PROPERTY_PROPERTIES_PATH_NAME_STR, MA_PROPERTY_PROPERTIES_CUSTOM1_STR, context->params->custome_props->custom_prop1, strlen(context->params->custome_props->custom_prop1));
		if(context->params->custome_props->custom_prop2)
			rc = ma_ds_set_str(ds, MA_PROPERTY_PROPERTIES_PATH_NAME_STR, MA_PROPERTY_PROPERTIES_CUSTOM2_STR, context->params->custome_props->custom_prop2, strlen(context->params->custome_props->custom_prop2));
		if(context->params->custome_props->custom_prop3)
			rc = ma_ds_set_str(ds, MA_PROPERTY_PROPERTIES_PATH_NAME_STR, MA_PROPERTY_PROPERTIES_CUSTOM3_STR, context->params->custome_props->custom_prop3, strlen(context->params->custome_props->custom_prop3));
		if(context->params->custome_props->custom_prop4)
			rc = ma_ds_set_str(ds, MA_PROPERTY_PROPERTIES_PATH_NAME_STR, MA_PROPERTY_PROPERTIES_CUSTOM4_STR, context->params->custome_props->custom_prop4, strlen(context->params->custome_props->custom_prop4));
	}
	return rc;
}

static ma_error_t language_update(ma_config_context_t *context){
	if(context && context->params) {
		ma_ds_t *ds = ma_configurator_get_datastore(context->agent_configurator);
		if(ds) {
			MA_LOG(context->logger, MA_LOG_SEV_INFO, "setting agent language");
			return ma_ds_set_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_LANGUAGE_STR, context->params->language, strlen(context->params->language));
		}
	}
	return MA_ERROR_INVALIDARG;
}

#ifdef MA_WINDOWS

/*
   NOTE:NOTE: - The same code is in installer too should have been in utils
	http://msdn.microsoft.com/en-us/library/windows/desktop/aa373653%28v=vs.85%29.aspx

	Guidelines for Services:
		Critical services should use the following recovery settings to specify 
		that the service be restarted one minute after the first failure to restart the service, 
		restarted two minutes after the second failure, and that the computer be restarted one minute 
		after the third failure. The failure count is reset to 0 after 300 seconds.

		Recovery Actions: Restart/60000/Restart/120000/Reboot/60000 & Reset =300 

		Critical services should be started before non-critical services. 
		Services that are not critical services should use the following recovery 
		settings to specify that the service be restarted two minutes after the first 
		failure to restart the service. The service is not restarted after the second failure, 
		and an administrator would need to intervene in this case. 
		The failure count is reset to 0 after 900 seconds.

		Recovery Actions: Restart/120000/Restart/300000/None/0 & Reset = 900 
*/
static void handle_failoveraction_for_services(ma_config_context_t *self, const wchar_t *service_name, ma_bool_t enable_action) {
	SC_HANDLE hSCM, hService;
	DWORD dwRet = 0;
	MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "%sabling service configuration failover actions", enable_action ? "en" : "dis");
	hSCM = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
	if (hSCM)  	{
		hService = OpenService( hSCM, service_name, SERVICE_ALL_ACCESS);  
		if (hService ) {
			SERVICE_FAILURE_ACTIONS serviceFailActions;
			SC_ACTION sc_failActions[3];

			sc_failActions[0].Type = (MA_TRUE == enable_action) ? SC_ACTION_RESTART : SC_ACTION_NONE; 
			sc_failActions[0].Delay = (MA_TRUE == enable_action) ? 60*1000 : 0;
			
			sc_failActions[1].Type = (MA_TRUE == enable_action) ? SC_ACTION_RESTART : SC_ACTION_NONE; 
			sc_failActions[1].Delay = (MA_TRUE == enable_action) ? 2 * 60*1000 : 0;
			
			sc_failActions[2].Type = SC_ACTION_NONE;
			sc_failActions[2].Delay = 0;

			serviceFailActions.dwResetPeriod = 300;
			serviceFailActions.lpCommand = NULL;		// Command to perform due to service failure, not used
			serviceFailActions.lpRebootMsg = NULL;		// Message during rebooting computer due to service failure, not used
			serviceFailActions.cActions = 3;			// Number of failure action to manage
			serviceFailActions.lpsaActions = sc_failActions;

			if( !ChangeServiceConfig2(hService, SERVICE_CONFIG_FAILURE_ACTIONS, &serviceFailActions) ) {
				MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "%sabling service configuration failover actions failed %d", enable_action ? "en" : "dis", GetLastError());
			}
			else 
				MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "%sabling service configuration failover actions passed", enable_action ? "en" : "dis");

			CloseServiceHandle(hService); 
		}
		else
			MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "open service failed, %d", GetLastError());
		CloseServiceHandle(hSCM);
	}
	else 
		MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "open service manager failed, %d", GetLastError());
}


static ma_error_t agent_service_start(ma_config_context_t *self, const wchar_t* service_name){
    ma_error_t rc = MA_ERROR_APIFAILED;
    SC_HANDLE schSCManager;
    SC_HANDLE schService;
    BOOL bRet = FALSE;
    SERVICE_STATUS ss = { 0 };

    // Get a handle to the SCM database.
    schSCManager = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);  // full access rights 

    if (NULL == schSCManager){
        MA_LOG(self->logger, MA_LOG_SEV_ERROR, "OpenSCManager failed (%d)", GetLastError());
        return rc; 
    }
    // Get a handle to the service.
    schService = OpenService( schSCManager, service_name, SERVICE_START | SERVICE_QUERY_STATUS);

    if (schService == NULL){ 
        MA_LOG(self->logger, MA_LOG_SEV_ERROR, "OpenService failed (%d)", GetLastError()); 
        CloseServiceHandle(schSCManager);
        return rc;
    }

    if(StartService(schService, 0, NULL)) {
		do{
			BOOL tmpRet = QueryServiceStatus(schService, &ss);
			if (tmpRet){
				if (ss.dwCurrentState == SERVICE_START_PENDING){
					Sleep(100);
				}
				rc = MA_OK;
			}else
				break;
		} while (ss.dwCurrentState == SERVICE_START_PENDING);
	}else {
		MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Service start failed, (%d)", GetLastError());
	}

	CloseServiceHandle(schService);
    CloseServiceHandle(schSCManager);
    
    return rc;
}

static ma_error_t agent_service_stop(ma_config_context_t *self, const wchar_t* service_name){
    ma_error_t rc = MA_ERROR_APIFAILED;
    SC_HANDLE schSCManager;
    SC_HANDLE schService;
    BOOL bRet = FALSE;
    SERVICE_STATUS ss = { 0 };

    // Get a handle to the SCM database.
    schSCManager = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);  // full access rights 

    if (NULL == schSCManager){
		MA_LOG(self->logger, MA_LOG_SEV_ERROR, "OpenSCManager failed (%d)", GetLastError());
        return rc;
    }
    // Get a handle to the service.
    schService = OpenService( schSCManager, service_name, SERVICE_STOP | SERVICE_QUERY_STATUS);

    if (schService == NULL){ 
		MA_LOG(self->logger, MA_LOG_SEV_ERROR, "OpenService failed (%d)", GetLastError()); 
        CloseServiceHandle(schSCManager);
        return rc;
    }
    
    if(ControlService(schService, SERVICE_CONTROL_STOP, (LPSERVICE_STATUS) &ss)) {
		do{
			BOOL tmpRet = QueryServiceStatus(schService, &ss);
			if (tmpRet){
				if (ss.dwCurrentState == SERVICE_STOP_PENDING){
					Sleep(100);
				}
				rc = MA_OK;
			}else
				break;
		} while (ss.dwCurrentState == SERVICE_STOP_PENDING);
	}else {
		MA_LOG(self->logger, MA_LOG_SEV_TRACE, "Service stop failed, (%d)", GetLastError());
	}
	CloseServiceHandle(schService);
    CloseServiceHandle(schSCManager);
    return rc;
}
#else

static ma_error_t service_config_path_get(char config_path[MAX_PATH_LEN]){
    ma_error_t rc = MA_ERROR_INVALIDARG;
    DIR *pdir ;
    struct dirent *pent ;
    struct stat stat_buffer ;
    char *config_dir = "/etc/ma.d/";
    if(0 == stat(config_dir, &stat_buffer))
    {
        //owned by the root and not writable by the non root
        if(stat_buffer.st_uid == 0 &&  ((stat_buffer.st_mode & S_IWOTH) != S_IWOTH) )
        {
            pdir = opendir(config_dir);
            if(pdir){
                while((pent = readdir(pdir))){
                    /*stat return -1 for valid directory. so chnaging logic. */
                    /*struct stat st;
                    if( -1 == stat(pent->d_name, &st)) continue;
                    if(!S_ISDIR(st.st_mode)) continue;*/
					/* Some platforms (SuSE Linux 9.3) return UNKNOWN file type for directory,
					As well as we are sure that MA_AGENT_SOFTWARE_ID can not be as file type, so removing type check */
                    /*if((pent->d_type == DT_DIR) && (strcmp(pent->d_name ,MA_AGENT_SOFTWARE_ID) == NULL)) */
					if(strcmp(pent->d_name ,MA_AGENT_SOFTWARE_ID) == NULL) {
                        if( MAX_PATH_LEN > (strlen(config_dir) + strlen(pent->d_name) + strlen("/config.xml"))){
                            strcpy(config_path, config_dir);
                            strcat(config_path, pent->d_name);
                            strcat(config_path, "/config.xml");
                            rc = MA_OK;
                        }
                    }
                }
				closedir(pdir);
                return rc;
            }
        }
    }
    return rc;	
}

static ma_error_t execute_cmd(const char *cmd){
    if (-1 == system(cmd)){
        fprintf(stdout, "Can not start service, system(%s) failed.\n", cmd);
        return MA_ERROR_APIFAILED;
    }
    else{
        return MA_OK;
    }
}

static ma_error_t execute_service(const char *service_name){
    char config_path[MAX_PATH_LEN] = {0};
    ma_xml_t *service_config_xml = NULL;
    ma_xml_node_t *node = NULL;
    ma_xml_node_t *service_node = NULL;
    ma_xml_node_t *configuration_node = NULL;
    char *command = NULL;
    ma_error_t rc = MA_ERROR_INVALIDARG;
    if(MA_OK == (rc = ma_xml_create(&service_config_xml))){
        if(MA_OK !=(rc = service_config_path_get(config_path))){
            fprintf(stdout, "Can not start service, config path fetching failed.\n");
			(void)ma_xml_release(service_config_xml);
            return rc;
        }

        if(MA_OK == (rc = ma_xml_load_from_file(service_config_xml, config_path))){
            if(node = ma_xml_get_node(service_config_xml)) {
                if(configuration_node = ma_xml_node_search_by_path(node, MA_SERVICE_CONFIG_CONFIGURATION_NODE)){
                    if(service_node = ma_xml_node_search_by_path(configuration_node, service_name)){
                        if(command = ma_xml_node_get_data(service_node)){
                            rc = execute_cmd(command);
                        }
                    }
                }
            }
        }
        (void)ma_xml_release(service_config_xml);
    }
    return rc;
}

static ma_error_t agent_service_start(ma_config_context_t *self, const wchar_t* service_name){
    return execute_service(MA_SERVICE_CONFIG_STARTCOMMAND);
}

static ma_error_t agent_service_stop(ma_config_context_t *self, const wchar_t* service_name){
    return execute_service(MA_SERVICE_CONFIG_STOPCOMMAND);
}
#endif


static ma_error_t service_stop(ma_config_context_t *self){
	ma_error_t rc = MA_OK;

	
#ifdef MA_WINDOWS
	/* first disable the failover actions*/
	handle_failoveraction_for_services(self, MA_AGENT_MAIN_SERVICE_NAME_WSTR, MA_FALSE);
	handle_failoveraction_for_services(self, MA_AGENT_COMMON_SERVICE_NAME_WSTR, MA_FALSE);
	handle_failoveraction_for_services(self, MA_AGENT_COMPAT_SERVICE_NAME_WSTR, MA_FALSE);

	if(MA_OK != (rc = agent_service_stop(self, MA_AGENT_MAIN_SERVICE_NAME_WSTR))){
		MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Agent Service stop failed.");
	}
	if(MA_OK != (rc = agent_service_stop(self, MA_AGENT_COMMON_SERVICE_NAME_WSTR))){
		MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Common Services stop failed.");
	}
#else
	if(MA_OK != (rc = agent_service_stop(self, NULL))){
		MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Service stop failed.");
	}
#endif
	return rc;
}

static ma_error_t service_start(ma_config_context_t *self){
	ma_error_t rc = MA_OK;
#ifdef MA_WINDOWS
	/* enable the failover actions first */
	handle_failoveraction_for_services(self, MA_AGENT_MAIN_SERVICE_NAME_WSTR, MA_TRUE);
	handle_failoveraction_for_services(self, MA_AGENT_COMMON_SERVICE_NAME_WSTR, MA_TRUE);
	handle_failoveraction_for_services(self, MA_AGENT_COMPAT_SERVICE_NAME_WSTR, MA_TRUE);

	if(MA_OK != (rc = agent_service_start(self, MA_AGENT_COMMON_SERVICE_NAME_WSTR))){
		MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Common Service start failed.");
	}
	if(MA_OK != (rc = agent_service_start(self, MA_AGENT_MAIN_SERVICE_NAME_WSTR))){
		MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Service start failed.");
	}
#else
	if(MA_OK != (rc = agent_service_start(self,NULL))){
		MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Service start failed.");
	}
#endif
	return rc;
}
