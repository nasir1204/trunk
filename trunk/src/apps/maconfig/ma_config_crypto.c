#include "ma_config_context.h"
#include "ma/internal/utils/spipe/ma_spipe_package_defs.h"
#include "ma/internal/utils/datastructures/ma_stream.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal/ma_strdef.h"
#include "ma/datastore/ma_ds_registry.h"
#include "ma/datastore/ma_ds_xml.h"

#include <stdio.h>

#ifdef MA_WINDOWS
#include <windows.h>
#endif

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "maconfig"

# ifndef MA_AGENT_SOFTWARE_ID
#  define MA_AGENT_SOFTWARE_ID            "EPOAGENT3000"
# endif								

# ifndef MA_AGENT_CONFIG_DIR
#  define MA_AGENT_CONFIG_DIR            "/etc/ma.d/"
# endif						

#define MA_CONFIG_XML_ROOT_PATH_STR			"Configuration"

#define MA_CONFIG_KEY_BINSPHASH_STR			"binsphash"



void ma_config_keystore_remove(char *location){
	ma_temp_buffer_t path = MA_TEMP_BUFFER_INIT;
	if(location){
		if(MA_OK == form_path(&path, location, STR_SERVER_2048_PUB_KEY_FILE, MA_FALSE)) {
			unlink((char*)ma_temp_buffer_get(&path));
			ma_temp_buffer_uninit(&path);
		}
		if(MA_OK == form_path(&path, location, STR_SERVER_REQ_2048_SEC_KEY_FILE, MA_FALSE)) {
			unlink((char*)ma_temp_buffer_get(&path));
			ma_temp_buffer_uninit(&path);
		}
		if(MA_OK == form_path(&path, location, STR_SERVER_REQ_SEC_KEY_FILE, MA_FALSE)) {
			unlink((char*)ma_temp_buffer_get(&path));
			ma_temp_buffer_uninit(&path);
		}
		if(MA_OK == form_path(&path, location, STR_SERVER_PUB_KEY_FILE, MA_FALSE)) {
			unlink((char*)ma_temp_buffer_get(&path));
			ma_temp_buffer_uninit(&path);
		}
		if(MA_OK == form_path(&path, location, STR_AGENT_FIPS_MODE_FLAG_FILE, MA_FALSE)) {
			unlink((char*)ma_temp_buffer_get(&path));
			ma_temp_buffer_uninit(&path);
		}
	}
}

static void save_key_hash_in_registry_for_key_update(ma_bytebuffer_t *buffer_decoded);
ma_error_t ma_config_crypto_keystore_setup(ma_config_context_t *self, char *key_location){
	const ma_crypto_ctx_t *crypto_context_object = NULL;	
	ma_bytebuffer_t *sr2048pubkey = NULL;
	ma_bytebuffer_t *req2048seckey = NULL;
    ma_error_t rc = MA_ERROR_INVALIDARG;
    if((self->crypto) && (crypto_context_object = ma_crypto_get_context(self->crypto))) {
		/*TBD: Read ePO Server version and throw it out if we are not happy.*/
		ma_temp_buffer_t path = MA_TEMP_BUFFER_INIT;
		form_path(&path, key_location, STR_SERVER_2048_PUB_KEY_FILE, MA_FALSE);		
		if(MA_OK == ma_config_get_file_buffer((char*)ma_temp_buffer_get(&path), &sr2048pubkey)) {
			ma_temp_buffer_uninit(&path);
			form_path(&path, key_location, STR_SERVER_REQ_2048_SEC_KEY_FILE, MA_FALSE);	
			if(MA_OK != ma_config_get_file_buffer((char*)ma_temp_buffer_get(&path), &req2048seckey)){
				ma_temp_buffer_uninit(&path);
				MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "server keys not valid");
				(void) ma_bytebuffer_release(sr2048pubkey);
				return rc;
			}            
		}
		else{
            MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "server keys not valid");
			ma_temp_buffer_uninit(&path);
			return rc;
		}

        if(MA_OK == (rc = ma_crypto_import_server_public_key(self->crypto, ma_bytebuffer_get_bytes(sr2048pubkey), ma_bytebuffer_get_size(sr2048pubkey)))) {
            if(MA_OK == (rc = ma_crypto_import_server_signing_key(self->crypto, ma_bytebuffer_get_bytes(req2048seckey), ma_bytebuffer_get_size(req2048seckey)))) {
                ma_bytebuffer_t *byte_buffer = NULL;
                if(MA_OK == (rc = ma_crypto_get_server_public_key_hash(self->crypto, &byte_buffer))) {
                    ma_bytebuffer_t *buffer_decoded = NULL;
                    if(MA_OK == (rc = ma_bytebuffer_create(ma_bytebuffer_get_size(byte_buffer), &buffer_decoded))) {
                        if(MA_OK == (rc = ma_crypto_decode(ma_bytebuffer_get_bytes(byte_buffer), ma_bytebuffer_get_size(byte_buffer), buffer_decoded, MA_TRUE))) {
                            save_key_hash_in_registry_for_key_update(buffer_decoded);
                        }
                        ma_bytebuffer_release(buffer_decoded);
                    }
                }
            }
        }    
		
		ma_temp_buffer_uninit(&path);		
		if(req2048seckey) (void) ma_bytebuffer_release(req2048seckey);
		if(sr2048pubkey) (void) ma_bytebuffer_release(sr2048pubkey);
		return rc;
    }
            
    MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Setting up agent key store failed , %d.", rc);	
	return rc;
}

ma_error_t ma_config_crypto_initialize(ma_config_context_t *self){
	ma_error_t rc = MA_ERROR_APIFAILED;
	if(MA_OK == (rc = ma_config_crypto_create(self))){
		if(self->params->agent_mode == MA_AGENT_MODE_MANAGED_INT){
			if(!strcmp(MA_CONFIG_IMPORT_KEYS, self->params->command))
				rc = ma_config_crypto_keystore_setup(self, (char*)ma_configurator_get_agent_data_path(self->agent_configurator));
			else
				rc = ma_config_crypto_keystore_setup(self, self->params->agent_provisioning_dir);

			if(rc != MA_OK){
				MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Setting up agent key store failed , moving to unmanaged mode");
				self->params->agent_mode = MA_AGENT_MODE_UNMANAGED_INT;
				rc = MA_OK;
			}
		}
	}
	return rc;
}


static ma_bool_t  hex_encode(const unsigned char *raw,const unsigned int size,unsigned char *hex,unsigned int max_size) {
    if(raw && hex && max_size >= size*2) {
        unsigned int i, j;
        const char hex_char[16]={'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
        for(i=0,j=0;i<size;i++)
        {
	        char temp=*(raw+i);
	        char first=(temp>>4) & 0x0F;
	        char second=temp & 0x0F;

            *(hex+j)=hex_char[(int)first];j++;
	        *(hex+j)=hex_char[(int)second];j++;
        }
        return MA_TRUE;
    }
    return MA_FALSE;
}

static void save_key_hash_in_registry_for_key_update(ma_bytebuffer_t *buffer_decoded) {    
    unsigned char binsphash[512] = {0};
    ma_ds_t *ds = NULL;
    
    hex_encode(ma_bytebuffer_get_bytes(buffer_decoded), ma_bytebuffer_get_size(buffer_decoded), binsphash, 512);

    #ifdef MA_WINDOWS    
	if(MA_OK == ma_ds_registry_open(HKEY_LOCAL_MACHINE, SZ_COMPAT_TVD_FRAMEWORK_REG_PATH,  MA_DS_REGISTRY_32BIT_VIEW, MA_FALSE, KEY_ALL_ACCESS, (ma_ds_registry_t **) &ds)) {
		ma_ds_set_str(ds, "", SZ_COMPAT_SERVER_KEY_HASH_A,(char *)binsphash, strlen((char*)binsphash));
		ma_ds_release(ds);
	}
	#else
	{
		ma_temp_buffer_t full_path;
		ma_ds_xml_t *ds_xml;
		ma_temp_buffer_init(&full_path);
		form_path(&full_path, MA_AGENT_CONFIG_DIR, MA_AGENT_SOFTWARE_ID, MA_FALSE);
		form_path(&full_path, (char *) ma_temp_buffer_get(&full_path), "config.xml", MA_FALSE);
		if( MA_OK == ma_ds_xml_open((char*)ma_temp_buffer_get(&full_path), &ds_xml)) {
			ma_ds_set_str((ma_ds_t *)ds_xml, MA_CONFIG_XML_ROOT_PATH_STR, MA_CONFIG_KEY_BINSPHASH_STR, (char *)binsphash, strlen((char*)binsphash));
			ma_ds_release((ma_ds_t *)ds_xml);
		}
	}
    #endif
}