#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ma_config_param.h"
#include "ma/internal/defs/ma_general_defs.h"

//#define MA_CONFIG_INVALID_INPUT_MSG printf("%s\n\n", "Invalid Input")
#define MA_CONFIG_INVALID_INPUT_MSG 
/* Usage help  maconfig message */
static char *maconfig_main_usage[] = {
	"usage: maconfig [options][args]\n",
	"where options are:\n",
	" -provision         provisions agent in managed/un-managed mode\n",
	" -enforce           enforces agent policies/configuration locally\n",
	" -custom            set custom properties\n",
	" -start             start agent\n",
	" -stop              stop agent\n",
	" -help              displays help\n\n",
	" e.g.  \n"
#ifdef MA_WINDOWS
	"       maconfig -provision -managed -dir \"C:\\temp\"\n",
#else
	"       maconfig -provision -managed -dir \"/temp\"\n",
#endif
	"       maconfig -enforce   -resetlang \n",
	"       maconfig -custom    -prop1 \"value\"\n",
	"       maconfig -start\n",
	"       maconfig -stop\n",
	"       maconfig -help\n\n",
	NULL
};

/* Usage help provision message */
static char *maconfig_provision_usage[] = {
	"usage: maconfig -provision \n",    
	" -managed        provisions agent in managed mode\n",
	" -unmanaged      provisions agent in un-managed mode\n",
	" e.g.  \n"
#ifdef MA_WINDOWS
	"       maconfig -provision -managed -dir \"C:\\temp\"\n",
#else
	"       maconfig -provision -managed -dir \"/temp\"\n",
#endif
	"       maconfig -provision -unmanaged \n\n",
	NULL
};

/* Usage help custom message */
static char *maconfig_custom_usage[] = {
	"usage: maconfig -custom \n",    
	" -prop1          value of first custom property\n",
	" -prop2          value of second custom property\n",
	" -prop3          value of third custom property\n",
	" -prop4          value of forth custom property\n",
	" e.g.  \n"
	"       maconfig -custom -prop1 \"value string\"\n",
	"       maconfig -custom -prop2 \"value string\"\n",
	"       maconfig -custom -prop3 \"value string\"\n",
	"       maconfig -custom -prop4 \"value string\"\n",
	NULL
};

/* Usage help provision managed message */
static char *maconfig_provision_managed_usage[] = {
	"usage: maconfig -provision -managed\n",    
	" -auto           using ePO credentials\n",
	" -dir            using ePO files listed in directory\n",
	" e.g.  \n"
#ifdef MA_WINDOWS
	"       maconfig -provision -managed -dir \"C:\\temp\"\n",
	"       maconfig -provision -managed -auto -dir \"C:\\temp\" \n",
#else
	"       maconfig -provision -managed -dir \"/temp\"\n",
	"       maconfig -provision -managed -auto -dir \"/temp\" \n",
#endif
	"       -epo ePOServerMachine[:443] [-user admin] [-password admin]\n\n",
	NULL
};

/* Usage help provision managed message */
static char *maconfig_provision_managed_dir_usage[] = {
	"usage: maconfig -provision -managed\n",    
	" -dir            using ePO files listed in directory\n",
	" e.g.  \n"
#ifdef MA_WINDOWS
	"       maconfig -provision -managed -dir \"C:\\temp\"\n",
#else
	"       maconfig -provision -managed -dir \"/temp\"\n",
#endif
	NULL
};

/* Usage help provision managed message */
static char *maconfig_provision_managed_auto_dir_usage[] = {
	"usage: maconfig -provision -managed -auto\n",    
	" -dir            directory where epo files get downloaded for temporary\n",
	" e.g.  \n"
#ifdef MA_WINDOWS
	"       maconfig -provision -managed -auto -dir \"C:\\temp\"\n",
#else
	"       maconfig -provision -managed -auto -dir \"/temp\"\n",
#endif
	"       -epo ePOServerMachine[:443] [-user admin] [-password admin]\n\n",
	NULL
}; 

/* Usage help provision managed auto message */
static char *maconfig_provision_managed_auto_usage[] = {
	"usage: maconfig -provision -managed -auto -dir <directory name> \n",    
	" -epo            epo url[:port]\n",
	" -user           epo user-id\n",
	" -password       epo password\n",
	" e.g.  \n"
#ifdef MA_WINDOWS
	"       maconfig -provision -managed -auto -dir \"C:\\temp\" \n",
#else
	"       maconfig -provision -managed -auto -dir \"/temp\" \n",
#endif
	"        -epo ePOServerMachine[:443] [-user admin] [-password admin]\n\n",
	NULL
};

/* Usage help enforce message */
static char *maconfig_enforce_usage[] = {
	"usage: maconfig -enforce \n",
	" -lang           set agent language\n",
	" -resetlang      set agent language to default(OS) language\n",
	" -license        set license key\n",
	" -loglevel       log level number(0(Disable)\\1(Info)\\2(Debug)\\3(Detail))\n",
/*	" -selfprotection on(enables)/off(disables) access protection\n", */
	" -noguid         deletes guid entry.\n", 
	" e.g.  \n"
	"       maconfig -enforce -lang <language Id>\n",
	"       maconfig -enforce -resetlang\n",
	"       maconfig -enforce -license \"license string\"\n",
	"       maconfig -enforce -loglevel 1\n",
/*	"       maconfig -enforce -selfprotection off\n", */
	"       maconfig -enforce -noguid\n\n", 
	NULL
};

/* Usage help enforce loglevel message */
static char *maconfig_enforce_loglevel_usage[] = {
	"Invalid loglevel \n",
	"Allowed loglevel values are 0(Disable)\\1(Info)\\2(Debug)\\3(Detail)\n",
	" e.g.  \n",
	"	maconfig -enforce -loglevel 2\n",
	NULL

};
/* Usage help import message */
static char *maconfig_import_usage[] = {
	"usage: maconfig -import \n",
	" -keys            imports server keys from directory\n",
	" -data            imports data from directory\n",
	" -certs		   imports certificates from directory\n",
	" e.g.  \n",
#ifdef MA_WINDOWS
	"       maconfig -keys -dir \"C:\\temp\"\n\n",
#else
	"       maconfig -keys -dir \"/temp\"\n\n",
#endif
	NULL
};

/* Usage help import keys message */
static char *maconfig_import_kes_usage[] = {
	"usage: maconfig -import -keys \n",
	" -dir            imports server keys from directory\n",
	" e.g.  \n"
#ifdef MA_WINDOWS
	"       maconfig -keys -dir \"C:\\temp\"\n\n",
#else
	"       maconfig -keys -dir \"C:/temp\"\n\n",
#endif
	NULL
};

/* Usage help import data message */
static char *maconfig_import_data_usage[] = {
	"usage: maconfig -import -data \n",
	" -dir            imports server keys from directory\n",
	" e.g.  \n"
#ifdef MA_WINDOWS 
	"       maconfig -data -dir \"C:\\temp\"\n\n",
#else
	"       maconfig -data -dir \"/temp\"\n\n",
#endif
	NULL
};

static void  print_usage(char **usage) {
	char **pp = NULL;
	for(pp = usage; (*pp != NULL); pp++) {
		printf("%s", *pp);
	}
}

/*custom command has to handle two different ways, bases on usage. Following is used when it called along with other main command (e.g. -provision) */
static ma_error_t process_custom_request(int *argc, int *argv_index, char **argv , ma_config_param_t *params) {
	ma_error_t rc = MA_ERROR_INVALIDARG;
	if( 0 == strcmp(argv[*argv_index], MA_CONFIG_CUSTOM)){
		*argc -=1; *argv_index += 1;
		if(!params->custome_props)
			params->custome_props = (ma_config_custome_props_t*)calloc(1, sizeof(ma_config_custome_props_t));
		if(params->custome_props)
		{
			while(*argc){
				if(0 == strcmp(argv[*argv_index], MA_CONFIG_CUSTOM_PROP1)) {
					*argc -=1; *argv_index += 1;
					if(*argc){
						params->custome_props->custom_prop1 = strdup(argv[*argv_index]);
						*argc -=1; *argv_index += 1;
						rc = MA_OK;
					}
				}else if(0 == strcmp(argv[*argv_index], MA_CONFIG_CUSTOM_PROP2)) {
					*argc -=1; *argv_index += 1;
					if(*argc){
						params->custome_props->custom_prop2 = strdup(argv[*argv_index]);
						*argc -=1; *argv_index += 1;
						rc = MA_OK;
					}
				}else if(0 == strcmp(argv[*argv_index], MA_CONFIG_CUSTOM_PROP3)) {
					*argc -=1; *argv_index += 1;
					if(*argc){
						params->custome_props->custom_prop3 = strdup(argv[*argv_index]);
						*argc -=1; *argv_index += 1;
						rc = MA_OK;
					}
				}else if(0 == strcmp(argv[*argv_index], MA_CONFIG_CUSTOM_PROP4)) {
					*argc -=1; *argv_index += 1;
					if(*argc){
						params->custome_props->custom_prop4 = strdup(argv[*argv_index]);
						*argc -=1; *argv_index += 1;
						rc = MA_OK;
					}
				}else{
					break;
				}
			}
		}
		if(MA_OK == rc){
			if(!params->command)
				params->command = MA_CONFIG_CUSTOM;
		}
	}
	return rc;
}

/*custom command has to handle two different ways, bases on usage. Following is used when it called as main command */
static ma_error_t process_custom_command_request(int *argc, int *argv_index, char **argv , ma_config_param_t *params) {
	ma_error_t rc = MA_ERROR_INVALIDARG;
	if(MA_OK == (rc = process_custom_request(argc, argv_index, argv, params))){
		if(*argc){
			/*some other input(unexpected) passed along with custom, show usage*/ 
			MA_CONFIG_INVALID_INPUT_MSG;
			print_usage(maconfig_custom_usage);
			rc = MA_ERROR_INVALIDARG;
		}
	}else{
		MA_CONFIG_INVALID_INPUT_MSG;
		print_usage(maconfig_custom_usage);
	}
	return rc;
}

/*Bug 969215: user can pass single quote using "\" (e.g. "C:\tmp\") , but it translate as "C:\tmp"" , which consider as invalid directory */
static void remove_extra_quote(char *str){
#ifdef MA_WINDOWS
	if(str){
		size_t len = strlen(str);
		if(len){
			if(str[len-1] == '"'){
				str[len-1] = '\0';
			}
		}
	}
	return;
#else
	return;
#endif
}

static ma_error_t process_dir_request(int *argc, int *argv_index, char **argv, ma_config_param_t *params) {
	ma_error_t rc = MA_ERROR_INVALIDARG;
	if(*argc >= 2) {
		if(0 == strcmp(argv[*argv_index], MA_CONFIG_DIR)) {
			*argc -=1; *argv_index += 1;
			params->agent_provisioning_dir = strdup(argv[*argv_index]);
			remove_extra_quote(params->agent_provisioning_dir);
			*argc -=1; *argv_index += 1;
			rc = MA_OK;
		}
	}
	return rc;
}

static ma_error_t process_license_key_request(int *argc, int *argv_index, char **argv , ma_config_param_t *params) {
	ma_error_t rc = MA_ERROR_INVALIDARG;
	if(*argc >= 2) {
		if(0 == strcmp(argv[*argv_index], MA_CONFIG_LICENSE)) {
			*argc -=1; *argv_index += 1;
			params->license_key = strdup(argv[*argv_index]);
			*argc -=1; *argv_index += 1;
			rc = MA_OK;
		}
	}
	return rc;
}

static ma_error_t process_guid_request(int *argc, int *argv_index, char **argv , ma_config_param_t *params) {
	ma_error_t rc = MA_ERROR_INVALIDARG;
	if(*argc >= 2) {
		if(0 == strcmp(argv[*argv_index], MA_CONFIG_GUID)) {
			*argc -=1; *argv_index += 1;
			params->agent_guid = strdup(argv[*argv_index]);
			*argc -=1; *argv_index += 1;
			rc = MA_OK;
		}
	}
	return rc;
}

static ma_error_t process_vdi_request(int *argc, int *argv_index, char **argv , ma_config_param_t *params) {
	ma_error_t rc = MA_ERROR_INVALIDARG;
	if(*argc >= 1) {
		if(0 == strcmp(argv[*argv_index], MA_CONFIG_VDI_MODE)) {
			*argc -=1; *argv_index += 1;
			params->vdi_mode = 1;
			rc = MA_OK;
		}
	}
	return rc;
}

static ma_error_t process_seq_num_request(int *argc, int *argv_index, char **argv , ma_config_param_t *params) {
	ma_error_t rc = MA_ERROR_INVALIDARG;
	if(*argc >= 2) {
		if(0 == strcmp(argv[*argv_index], MA_CONFIG_SEQ_NUM)) {
			*argc -=1; *argv_index += 1;
			params->seq_num = atoi(argv[*argv_index]);
			*argc -=1; *argv_index += 1;
			rc = MA_OK;
		}
	}
	return rc;
}

static ma_error_t process_tenantid_request(int *argc, int *argv_index, char **argv , ma_config_param_t *params) {
	ma_error_t rc = MA_ERROR_INVALIDARG;
	if(*argc >= 2) {
		if(0 == strcmp(argv[*argv_index], MA_CONFIG_TENANT_ID)) {
			*argc -=1; *argv_index += 1;
			params->tenant_id = strdup(argv[*argv_index]);
			*argc -=1; *argv_index += 1;
			rc = MA_OK;
		}
	}
	return rc;
}

static ma_error_t process_logging_request(int *argc, int *argv_index, char **argv , ma_config_param_t *params) {
	ma_error_t rc = MA_ERROR_INVALIDARG;
	if(*argc >= 2) {
		if(0 == strcmp(argv[*argv_index], MA_CONFIG_LOG_LOCATION)) {
			*argc -=1; *argv_index += 1;
			params->agent_log_dir = strdup(argv[*argv_index]);
			*argc -=1; *argv_index += 1;
			rc = MA_OK;
		}
	}
	return rc;
}

static ma_error_t process_nostart_request(int *argc, int *argv_index, char **argv , ma_config_param_t *params) {
	ma_error_t rc = MA_ERROR_INVALIDARG;
	if(*argc >= 1) {
		if(0 == strcmp(argv[*argv_index], MA_CONFIG_NOSTART)) {
			*argc -=1; *argv_index += 1;
			params->agent_start_mode = 0;
			rc = MA_OK;
		}
	}
	return rc;
}

static ma_error_t process_lang_request(int *argc, int *argv_index, char **argv , ma_config_param_t *params) {
	ma_error_t rc = MA_ERROR_INVALIDARG;
	if(*argc >= 2) {
		if(0 == strcmp(argv[*argv_index], MA_CONFIG_ENFORCE_LANGUAGE)) {
			*argc -=1; *argv_index += 1;
			strncpy(params->language, argv[*argv_index], MA_MAX_LANG_NAME-1);
			params->set_lang = MA_TRUE;
			*argc -=1; *argv_index += 1;
			rc = MA_OK;
		}
	}
	return rc;
}

static ma_error_t process_check_request(int *argc, int *argv_index, char **argv , ma_config_param_t *params) {
	ma_error_t rc = MA_ERROR_INVALIDARG;
	if(*argc >= 1) {
		if(0 == strcmp(argv[*argv_index], MA_CONFIG_CHECK)) {
			*argc -=1; *argv_index += 1;
			params->need_check = MA_TRUE;
			rc = MA_OK;
		}
	}
	return rc;
}

static ma_error_t process_non_cli_options_request(int *argc, int *argv_index, char **argv, ma_config_param_t *params) {
	ma_error_t rc = MA_OK;
	while(*argc && (MA_OK == rc)) {
		if(0 == strcmp(argv[*argv_index], MA_CONFIG_LICENSE)){
			rc = process_license_key_request(argc, argv_index, argv, params);
		}else if(0 == strcmp(argv[*argv_index], MA_CONFIG_GUID)){
			rc = process_guid_request(argc, argv_index, argv, params);
		}else if(0 == strcmp(argv[*argv_index], MA_CONFIG_VDI_MODE)){
			rc = process_vdi_request(argc, argv_index, argv, params);
		}else if(0 == strcmp(argv[*argv_index], MA_CONFIG_SEQ_NUM)){
			rc = process_seq_num_request(argc, argv_index, argv, params);
		}else if(0 == strcmp(argv[*argv_index], MA_CONFIG_TENANT_ID)){
			rc = process_tenantid_request(argc, argv_index, argv, params);
		}else if(0 == strcmp(argv[*argv_index], MA_CONFIG_LOG_LOCATION)){
			rc = process_logging_request(argc, argv_index, argv, params);
		}else if(0 == strcmp(argv[*argv_index], MA_CONFIG_NOSTART)){
			rc = process_nostart_request(argc, argv_index, argv, params);
		}else if(0 == strcmp(argv[*argv_index], MA_CONFIG_CUSTOM)){
			rc = process_custom_request(argc, argv_index, argv, params);
		}else if(0 == strcmp(argv[*argv_index], MA_CONFIG_ENFORCE_LANGUAGE)){
			rc = process_lang_request(argc, argv_index, argv, params);
		}else if(0 == strcmp(argv[*argv_index], MA_CONFIG_CHECK)){
			rc = process_check_request(argc, argv_index, argv, params);
		}
        else{
			rc = MA_ERROR_INVALIDARG;
			break;
		} 
	}
	return rc;
}

static ma_error_t process_provision_managed_dir_request(int *argc, int *argv_index, char **argv, ma_config_param_t *params) {
	ma_error_t rc = MA_ERROR_INVALIDARG;
	if( !( (MA_OK == (rc = process_dir_request(argc, argv_index, argv, params))) && (MA_OK == (rc = process_non_cli_options_request(argc, argv_index, argv, params))) ) ) {
		MA_CONFIG_INVALID_INPUT_MSG;
		print_usage(maconfig_provision_managed_dir_usage);
	}
	return rc;
}

static ma_error_t process_provision_managed_auto_details_request(int *argc, int *argv_index, char **argv , ma_config_param_t *params) {
	ma_error_t rc = MA_ERROR_INVALIDARG;
	if(*argc >= 2) {
		params->epo_provision = (epo_provision_t*)calloc(1, sizeof(epo_provision_t));
		rc = MA_OK;
		if(params->epo_provision){
			while(*argc){
				if(0 == strcmp(argv[*argv_index], MA_CONFIG_PROVISION_MANAGED_AUTO_EPO)){
					*argc -=1; *argv_index += 1;
					params->epo_provision->url = strdup(argv[*argv_index]);
					*argc -=1; *argv_index += 1;
				}
				else if(0 == strcmp(argv[*argv_index], MA_CONFIG_PROVISION_MANAGED_AUTO_USER)){
					*argc -=1; *argv_index += 1;
					params->epo_provision->user_name = strdup(argv[*argv_index]);
					*argc -=1; *argv_index += 1;
				}
				else if(0 == strcmp(argv[*argv_index], MA_CONFIG_PROVISION_MANAGED_AUTO_PASSWD)){
					*argc -=1; *argv_index += 1;
					params->epo_provision->user_passwd = strdup(argv[*argv_index]);
					*argc -=1; *argv_index += 1;
				}
				else
					break;
			}
		}
	}else {
		MA_CONFIG_INVALID_INPUT_MSG;
		print_usage(maconfig_provision_managed_auto_usage);
	}
	return rc;
}

static ma_error_t process_provision_managed_auto_request(int *argc, int *argv_index, char **argv , ma_config_param_t *params) {
	ma_error_t rc = MA_ERROR_INVALIDARG;
	if( MA_OK == (rc = process_dir_request(argc, argv_index, argv, params))) {
		if (*argc && (0 == strcmp(argv[*argv_index], MA_CONFIG_PROVISION_MANAGED_AUTO_EPO))) {
			if(MA_OK == (rc = process_provision_managed_auto_details_request(argc, argv_index, argv, params))){
				if(MA_OK != (rc = process_non_cli_options_request(argc, argv_index, argv, params))){
					MA_CONFIG_INVALID_INPUT_MSG;
					print_usage(maconfig_provision_managed_auto_usage);
				}
			}
		}else{
			MA_CONFIG_INVALID_INPUT_MSG;
			print_usage(maconfig_provision_managed_auto_usage);
			rc = MA_ERROR_INVALIDARG;
		}
	}else {
			//MA_CONFIG_INVALID_INPUT_MSG;
			print_usage(maconfig_provision_managed_auto_dir_usage);
	}
	return rc;
}

static ma_error_t process_provision_managed_request(int *argc, int *argv_index, char **argv, ma_config_param_t *params) {
	ma_error_t rc = MA_ERROR_INVALIDARG;
	if(*argc >= 1) {
		params->agent_mode = MA_AGENT_MODE_MANAGED_INT;
		params->original_agent_mode = MA_AGENT_MODE_MANAGED_INT;
		if(0 == strcmp(argv[*argv_index], MA_CONFIG_DIR)) {
			rc = process_provision_managed_dir_request(argc, argv_index, argv, params);
		}else if(0 == strcmp(argv[*argv_index], MA_CONFIG_PROVISION_MANAGED_AUTO)) {
			params->command = MA_CONFIG_PROVISION_MANAGED_AUTO;
			*argc -=1; *argv_index += 1;
			rc = process_provision_managed_auto_request(argc, argv_index, argv, params);
		}else {
			MA_CONFIG_INVALID_INPUT_MSG;
			print_usage(maconfig_provision_managed_usage);
		}
	}else {
		print_usage(maconfig_provision_managed_usage);
	}
	return rc;
}

static ma_error_t process_provision_unmanaged_request(int *argc, int *argv_index, char **argv , ma_config_param_t *params) {
	ma_error_t rc = MA_ERROR_INVALIDARG;
	params->agent_mode = MA_AGENT_MODE_UNMANAGED_INT;
	params->original_agent_mode = MA_AGENT_MODE_UNMANAGED_INT;
	if( (MA_OK != (rc = process_non_cli_options_request(argc, argv_index, argv, params))) ) {
		MA_CONFIG_INVALID_INPUT_MSG;
		print_usage(maconfig_provision_usage);
	}
	return rc;
}

static ma_error_t process_provision_request(int *argc, int *argv_index, char **argv, ma_config_param_t *params) {
	ma_error_t rc = MA_ERROR_INVALIDARG;
	if( 0 == strcmp(argv[*argv_index], MA_CONFIG_PROVISION)){
		*argc -=1; *argv_index += 1;
		if(*argc >= 1) {
			if(0 == strcmp(argv[*argv_index], MA_CONFIG_PROVISION_MANAGED)) {
				*argc -=1; *argv_index += 1;
				rc = process_provision_managed_request(argc, argv_index, argv, params);
			}else if(0 == strcmp(argv[*argv_index], MA_CONFIG_PROVISION_UNMANAGED)) {
				*argc -=1; *argv_index += 1;
				rc = process_provision_unmanaged_request(argc, argv_index, argv, params);
			}else {
				MA_CONFIG_INVALID_INPUT_MSG;
				print_usage(maconfig_provision_usage);
			}
		}else {
			print_usage(maconfig_provision_usage);
		}
	}
	return rc;
}

static ma_error_t process_enforce_protection_request(int *argc, int *argv_index, char **argv , ma_config_param_t *params) {
	ma_error_t rc = MA_ERROR_INVALIDARG;
	if(*argc >= 2) {
		if(0 == strcmp(argv[*argv_index], MA_CONFIG_ENFORCE_PROTECTION)) {
			/*TBD: Handle here */
			printf("This option is yet to implement \n\n");
			if(MA_OK != (rc = process_non_cli_options_request(argc, argv_index, argv, params))){
				MA_CONFIG_INVALID_INPUT_MSG;
				print_usage(maconfig_enforce_usage);
			}
		}
	}else {
		MA_CONFIG_INVALID_INPUT_MSG;
		print_usage(maconfig_enforce_usage);
	}
	return rc;
}

static ma_error_t process_enforce_noguid_request(int *argc, int *argv_index, char **argv , ma_config_param_t *params) {
	/* noguid wont expect any additional parameter */
	if(*argc == 1) {
		params->command = MA_CONFIG_ENFORCE_NOGUID;
		return MA_OK;
	}else {
		MA_CONFIG_INVALID_INPUT_MSG;
		print_usage(maconfig_enforce_usage);
	}
	return MA_ERROR_INVALIDARG;
}

static ma_error_t process_enforce_language_request(int *argc, int *argv_index, char **argv , ma_config_param_t *params) {
	ma_error_t rc = MA_ERROR_INVALIDARG;
	if(*argc >= 2) {
		if(0 == strcmp(argv[*argv_index], MA_CONFIG_ENFORCE_LANGUAGE)) {
			*argc -=1; *argv_index += 1;
			strncpy(params->language, argv[*argv_index], MA_MAX_LANG_NAME-1);
			params->set_lang = MA_TRUE;
			*argc -=1; *argv_index += 1;
			if(MA_OK != (rc = process_non_cli_options_request(argc, argv_index, argv, params))){
				MA_CONFIG_INVALID_INPUT_MSG;
				print_usage(maconfig_enforce_usage);
			}
		}
	}else {
		MA_CONFIG_INVALID_INPUT_MSG;
		print_usage(maconfig_enforce_usage);
	}
	return rc;
}

static ma_error_t process_enforce_licensekey_request(int *argc, int *argv_index, char **argv , ma_config_param_t *params) {
	ma_error_t rc = MA_ERROR_INVALIDARG;
	if(*argc >= 2) {
		if(0 == strcmp(argv[*argv_index], MA_CONFIG_LICENSE)) {
			params->command = MA_CONFIG_LICENSE;
			*argc -=1; *argv_index += 1;
			params->license_key = strdup(argv[*argv_index]);
			*argc -=1; *argv_index += 1;
			if(MA_OK != (rc = process_non_cli_options_request(argc, argv_index, argv, params))){
				MA_CONFIG_INVALID_INPUT_MSG;
				print_usage(maconfig_enforce_usage);
			}
		}
	}else {
		MA_CONFIG_INVALID_INPUT_MSG;
		print_usage(maconfig_enforce_usage);
	}
	return rc;
}

static ma_error_t process_enforce_reset_language_request(int *argc, int *argv_index, char **argv , ma_config_param_t *params) {
	ma_error_t rc = MA_ERROR_INVALIDARG;
	if(*argc >= 1) {
		if(0 == strcmp(argv[*argv_index], MA_CONFIG_ENFORCE_LANGUAGE_RESET)) {
			*argc -=1; *argv_index += 1;
			params->reset_lang = MA_TRUE;
			if(MA_OK != (rc = process_non_cli_options_request(argc, argv_index, argv, params))){
				MA_CONFIG_INVALID_INPUT_MSG;
				print_usage(maconfig_enforce_usage);
			}
		}
	}else {
		MA_CONFIG_INVALID_INPUT_MSG;
		print_usage(maconfig_enforce_usage);
	}
	return rc;
}

static ma_error_t process_enforce_loglevel_request(int *argc, int *argv_index, char **argv , ma_config_param_t *params) {
	ma_error_t rc = MA_ERROR_INVALIDARG;
	if(*argc >= 2) {
		if(0 == strcmp(argv[*argv_index], MA_CONFIG_ENFORCE_LOGLEVEL)) {
			*argc -=1; *argv_index += 1;
			if(!strcmp("0", argv[*argv_index]) || !strcmp("1", argv[*argv_index]) || !strcmp("2", argv[*argv_index]) || !strcmp("3", argv[*argv_index])){
				params->loglevel = strdup(argv[*argv_index]);
				*argc -=1; *argv_index += 1;
				if(MA_OK != (rc = process_non_cli_options_request(argc, argv_index, argv, params))){
					MA_CONFIG_INVALID_INPUT_MSG;
					print_usage(maconfig_enforce_usage);
				}
			}else{
				MA_CONFIG_INVALID_INPUT_MSG;
				print_usage(maconfig_enforce_loglevel_usage);
			}
		}
	}else {
		MA_CONFIG_INVALID_INPUT_MSG;
		print_usage(maconfig_enforce_usage);
	}
	return rc;
}

static ma_error_t process_enforce_request(int *argc, int *argv_index, char **argv , ma_config_param_t *params) {
	ma_error_t rc = MA_ERROR_INVALIDARG;
	if( 0 == strcmp(argv[*argv_index], MA_CONFIG_ENFORCE)){
		*argc -=1; *argv_index += 1;
		if(*argc >= 1) {
			if(0 == strcmp(argv[*argv_index], MA_CONFIG_ENFORCE_PROTECTION)) {
				rc = process_enforce_protection_request(argc, argv_index, argv, params);
			}else if(0 == strcmp(argv[*argv_index], MA_CONFIG_ENFORCE_LANGUAGE)) {
				rc = process_enforce_language_request(argc, argv_index, argv, params);
			}else if(0 == strcmp(argv[*argv_index], MA_CONFIG_ENFORCE_NOGUID)) {
				rc = process_enforce_noguid_request(argc, argv_index, argv, params);
			}else if(0 == strcmp(argv[*argv_index], MA_CONFIG_ENFORCE_LANGUAGE_RESET)) {
				rc = process_enforce_reset_language_request(argc, argv_index, argv, params);
			}else if(0 == strcmp(argv[*argv_index], MA_CONFIG_LICENSE)) {
				rc = process_enforce_licensekey_request(argc, argv_index, argv, params);
			}else if(0 == strcmp(argv[*argv_index], MA_CONFIG_ENFORCE_LOGLEVEL)) {
				rc = process_enforce_loglevel_request(argc, argv_index, argv, params);
			}else {
				MA_CONFIG_INVALID_INPUT_MSG;
				print_usage(maconfig_enforce_usage);
			}
		}else {
			print_usage(maconfig_enforce_usage);
		}
	}
	return rc;
}

static ma_error_t process_import_key_request(int *argc, int *argv_index, char **argv , ma_config_param_t *params) {
	ma_error_t rc = MA_ERROR_INVALIDARG;
	if((MA_OK == (rc = process_dir_request(argc, argv_index, argv, params))) &&
       (MA_OK == (rc = process_non_cli_options_request(argc, argv_index, argv, params))) ){
		params->command = MA_CONFIG_IMPORT_KEYS;
		return rc;
	}else {
		MA_CONFIG_INVALID_INPUT_MSG;
		print_usage(maconfig_import_kes_usage);
	}
	return rc;
}

static ma_error_t process_import_data_request(int *argc, int *argv_index, char **argv , ma_config_param_t *params) {
	ma_error_t rc = MA_ERROR_INVALIDARG;
	if((MA_OK == (rc = process_dir_request(argc, argv_index, argv, params))) &&
       (MA_OK == (rc = process_non_cli_options_request(argc, argv_index, argv, params))) ){
		params->command = MA_CONFIG_IMPORT_DATA;
		return rc;
	}else {
		MA_CONFIG_INVALID_INPUT_MSG;
		print_usage(maconfig_import_data_usage);
	}
	return rc;
}

static ma_error_t process_import_certs_request(int *argc, int *argv_index, char **argv , ma_config_param_t *params) {
	ma_error_t rc = MA_ERROR_INVALIDARG;
	if((MA_OK == (rc = process_dir_request(argc, argv_index, argv, params))) &&
       (MA_OK == (rc = process_non_cli_options_request(argc, argv_index, argv, params))) ){
		params->command = MA_CONFIG_IMPORT_CERTS;
		return rc;
	}else {
		MA_CONFIG_INVALID_INPUT_MSG;
		print_usage(maconfig_import_data_usage);
	}
	return rc;
}

static ma_error_t process_import_request(int *argc, int *argv_index, char **argv , ma_config_param_t *params) {
	ma_error_t rc = MA_ERROR_INVALIDARG;
	if(0 == strcmp(argv[*argv_index], MA_CONFIG_IMPORT)){
		*argc -=1; *argv_index += 1;
		if(*argc >= 1) {
			params->agent_mode = MA_AGENT_MODE_MANAGED_INT;  
			params->original_agent_mode = MA_AGENT_MODE_MANAGED_INT;
			if(0 == strcmp(argv[*argv_index], MA_CONFIG_IMPORT_KEYS)) {
				*argc -=1; *argv_index += 1;
				rc = process_import_key_request(argc, argv_index, argv, params);
			}else if(0 == strcmp(argv[*argv_index], MA_CONFIG_IMPORT_DATA)) {
				*argc -=1; *argv_index += 1;
				rc = process_import_data_request(argc, argv_index, argv, params);
			}else if(0 == strcmp(argv[*argv_index], MA_CONFIG_IMPORT_CERTS)) {
				*argc -=1; *argv_index += 1;
				rc = process_import_certs_request(argc, argv_index, argv, params);
			}else {
				MA_CONFIG_INVALID_INPUT_MSG;
				print_usage(maconfig_import_usage);
			}
		}else {
			//MA_CONFIG_INVALID_INPUT_MSG;
			print_usage(maconfig_import_usage);
		}
	}
	return rc;
}

ma_error_t ma_config_param_read(int argc, char *argv[], ma_config_param_t *params){
	ma_error_t rc = MA_ERROR_INVALIDARG;
	int argv_index = 0;
	/* Skip the program name */
	--argc;	++argv_index ;

	/* Parse the command line parameters */
	if((argc >= 1) && ( 0 == strcmp(argv[argv_index], MA_CONFIG_PROVISION))) {
		params->command = MA_CONFIG_PROVISION;
		rc = process_provision_request(&argc, &argv_index, argv, params);
	} else if((argc >= 1) && ( 0 == strcmp(argv[argv_index], MA_CONFIG_ENFORCE))) {
		params->command = MA_CONFIG_ENFORCE;
		rc = process_enforce_request(&argc, &argv_index, argv, params);
	} else if((argc >= 1) && ( 0 == strcmp(argv[argv_index], MA_CONFIG_IMPORT))) {
		params->command = MA_CONFIG_IMPORT;
		rc = process_import_request(&argc, &argv_index, argv, params);
	} else if((argc >= 1) && ( 0 == strcmp(argv[argv_index], MA_CONFIG_CUSTOM))) {
		params->command = MA_CONFIG_CUSTOM;
		rc = process_custom_command_request(&argc, &argv_index, argv, params);
	} else if((argc == 1) && ( 0 == strcmp(argv[argv_index], MA_CONFIG_START))) {
		params->command = MA_CONFIG_START;
		--argc ; ++argv_index;
		rc = MA_OK;
	} else if((argc == 1) && ( 0 == strcmp(argv[argv_index], MA_CONFIG_STOP))) {
		params->command = MA_CONFIG_STOP;
		--argc; ++argv_index;
		rc = MA_OK;
	} else if((argc == 1) && ( 0 == strcmp(argv[argv_index], MA_CONFIG_HELP))) {
		print_usage(maconfig_main_usage);
	} else {
		MA_CONFIG_INVALID_INPUT_MSG;
		print_usage(maconfig_main_usage);
	}
	return rc;

}


