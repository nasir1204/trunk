#ifndef MA_CONFIG_UPGRADE_H_INCLUDED
#define MA_CONFIG_UPGRADE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma_config_context.h"

MA_CPP( extern "C" { )

ma_error_t ma_config_upgrade_import_data(ma_config_context_t *config_context);

ma_error_t ma_config_import_certs(ma_config_context_t *config_context);

MA_CPP(})

#endif /* MA_CONFIG_UPGRADE_H_INCLUDED */

