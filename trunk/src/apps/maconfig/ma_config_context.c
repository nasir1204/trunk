#include "ma_config_context.h"
#include "ma/logger/ma_file_logger.h"
#include "ma/logger/ma_console_logger.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/utils/spipe/ma_spipe_package_defs.h"

#include <stdio.h>

#ifdef MA_WINDOWS
#include <windows.h>
#endif

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "maconfig"

#define MA_LOG_CONFIG_FILE_NAME_STR "maconfig"

static ma_error_t ma_config_log_create(ma_config_context_t *self);

ma_error_t ma_config_context_create(ma_config_param_t *params, ma_config_context_t **context) {
    if(context) {
        ma_config_context_t *self = (ma_config_context_t *)calloc(1,sizeof(ma_config_context_t));
        if(!self)       
			return MA_ERROR_OUTOFMEMORY;
		self->params = params;
		if(MA_OK != ma_config_log_create(self)){
			fprintf(stdout, "config context create failed.\n");
			free(self);
			return MA_ERROR_APIFAILED;
		}
		*context = self;
        return MA_OK ;
	}
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_config_context_release(ma_config_context_t *self) {
    if(self) {
		if(self->agent_configurator) (void)ma_configurator_release(self->agent_configurator);
		if(self->logger) (void)ma_logger_release(self->logger);
        if(self->log_filter) (void)ma_log_filter_release(self->log_filter);
        if(self->sitelist_data) (void)ma_bytebuffer_release(self->sitelist_data);
        if(self->crypto) {
			(void)ma_crypto_deinitialize(self->crypto);
			(void)ma_crypto_release(self->crypto);
		}
		free(self); self = NULL;
	}
	return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_config_log_create(ma_config_context_t *self){
	ma_error_t rc = MA_OK;
    
    if(self->params->agent_log_dir){
		if(MA_OK != (rc = ma_file_logger_create(self->params->agent_log_dir, MA_LOG_CONFIG_FILE_NAME_STR, ".log", (ma_file_logger_t **)&self->logger)))	{
			fprintf(stdout, "file logger create failed, last error(%d). Creating console logger.\n", rc);
			if(MA_OK != (rc = ma_console_logger_create((ma_console_logger_t **)&self->logger))) {
				fprintf(stdout, "console logger create failed, last error(%d).\n", rc);
				return rc;
			}
		}else { 
            self->is_file_logger = MA_TRUE;
		    fprintf(stdout, "Check log file at %s%s%s%s\n", self->params->agent_log_dir, MA_PATH_SEPARATOR_STR, MA_LOG_CONFIG_FILE_NAME_STR, ".log");
        }
	}
	else{
		if(MA_OK != (rc = ma_console_logger_create((ma_console_logger_t **)&self->logger))) {
			fprintf(stdout, "console logger create failed, last error(%d).\n", rc);
			return rc;
		}
	}

    (void)ma_generic_log_filter_create(self->is_file_logger ? "*.Debug" : "*.Info", &self->log_filter);
    (void)ma_logger_add_filter(self->logger, self->log_filter);
	return rc;
}

static void check_and_init_crypto_mode(ma_config_context_t *self) {    
    self->params->agent_crypto_mode = MA_CRYPTO_MODE_NON_FIPS;
    if(self->params->agent_provisioning_dir) {
        ma_temp_buffer_t path = MA_TEMP_BUFFER_INIT;
        ma_bytebuffer_t *agentfipsmode = NULL;
	(void)form_path(&path, self->params->agent_provisioning_dir, STR_AGENT_FIPS_MODE_FLAG_FILE, MA_FALSE);	
	if((MA_OK == ma_config_get_file_buffer((char*)ma_temp_buffer_get(&path), &agentfipsmode)) && agentfipsmode){
            unsigned short m = atoi((char*)ma_bytebuffer_get_bytes(agentfipsmode));
	    if((1 == m) || (0 == m)) 
                self->params->agent_crypto_mode = m;
	    else 
	        MA_LOG(self->logger, MA_LOG_SEV_WARNING, "Invalid FIPS input, falling back to non-fips");
                (void) ma_bytebuffer_release(agentfipsmode);
        }else
        	MA_LOG(self->logger, MA_LOG_SEV_TRACE, "Setting it to Non-FIPS mode as fips file is not present.");
	    ma_temp_buffer_uninit(&path);
    }
}

static ma_error_t populate_agent_configuration(ma_config_context_t *self, ma_agent_configuration_t *agent_configuration){	
	if(self->params->agent_guid)
		MA_MSC_SELECT(_snprintf, snprintf)(agent_configuration->agent_guid, UUID_LEN_TXT, "%s", self->params->agent_guid);
	if(self->params->tenant_id)
		MA_MSC_SELECT(_snprintf, snprintf)(agent_configuration->tenant_id, MA_MAX_KEY_LEN, "%s", self->params->tenant_id);

    check_and_init_crypto_mode(self);
    agent_configuration->agent_crypto_mode = self->params->agent_crypto_mode;
	agent_configuration->agent_mode			= self->params->agent_mode;
	agent_configuration->agent_crypto_role	= MA_CRYPTO_ROLE_OFFICER;
	agent_configuration->vdi_mode			= self->params->vdi_mode;
	agent_configuration->seq_num			= self->params->seq_num;
    return MA_OK;
}

ma_error_t ma_config_configurator_create(ma_config_context_t *self){
	ma_error_t rc = MA_OK;		
	ma_agent_configuration_t agent_configuration = {0};
 
    if(self->agent_configurator) 
		(void)ma_configurator_release(self->agent_configurator);
	if(MA_OK == (rc = populate_agent_configuration(self, &agent_configuration))) {
		if(MA_OK == (rc = ma_configurator_create(&agent_configuration, &self->agent_configurator))) {
			MA_LOG(self->logger, MA_LOG_SEV_TRACE, "configurator created successfully");
		}
		else
			MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to create agent configurator, error = (%d)", rc);							
	}		
	else
		MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to populate agent configuration, error = (%d)", rc);	

	return rc;
}


ma_error_t ma_config_crypto_create(ma_config_context_t *self) {	
    if(self) {
        ma_crypto_ctx_t *crypto_context_object = NULL;
        ma_error_t rc = MA_OK;

        /*already initialized */
        if(self->crypto) return MA_OK;
        if(MA_OK == (rc = ma_crypto_ctx_create(&crypto_context_object))) {
            ma_temp_buffer_t path = MA_TEMP_BUFFER_INIT;
		    (void)ma_crypto_ctx_set_fips_mode(crypto_context_object, (ma_crypto_mode_t )self->params->agent_crypto_mode);
            (void)ma_crypto_ctx_set_role(crypto_context_object, (ma_crypto_role_t)self->params->agent_crypto_role);
		    (void)ma_crypto_ctx_set_agent_mode(crypto_context_object, (ma_crypto_agent_mode_t)self->params->agent_mode);
            if(self->is_file_logger) (void)ma_crypto_ctx_set_logger(crypto_context_object, self->logger);
        	
            if(MA_OK == (rc = form_path(&path, ma_configurator_get_agent_data_path(self->agent_configurator), MA_CONFIGURATION_KEYSTORE_DIR_NAME_STR, MA_FALSE))) {
			    rc = ma_crypto_ctx_set_keystore(crypto_context_object, (char*)ma_temp_buffer_get(&path));
			    ma_temp_buffer_uninit(&path);
		    }

            if((MA_OK == rc) && (MA_OK == (rc = form_path(&path, ma_configurator_get_agent_data_path(self->agent_configurator), MA_CONFIGURATION_CERTSTORE_DIR_NAME_STR, MA_FALSE)))) {
			    rc = ma_crypto_ctx_set_certstore(crypto_context_object, (char*)ma_temp_buffer_get(&path));
			    ma_temp_buffer_uninit(&path);
		    }

            if((MA_OK == rc) && (MA_OK == (rc = ma_crypto_ctx_set_crypto_lib_path(crypto_context_object, ma_configurator_get_agent_lib_path(self->agent_configurator, MA_FALSE))))) {
                if(MA_OK == (rc = ma_crypto_create(&self->crypto))) {
                    if(MA_OK == (rc = ma_crypto_initialize(crypto_context_object, self->crypto))) {
                        (void)ma_crypto_ctx_release(crypto_context_object);
                        return MA_OK;
                    }
                }
            }
            (void)ma_crypto_ctx_release(crypto_context_object);
        }
        MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Setting up agent keystore failed , %d.", rc);	
	    return rc;
    }
    return MA_ERROR_INVALIDARG;
}
