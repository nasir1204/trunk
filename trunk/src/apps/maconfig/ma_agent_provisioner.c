#include "ma_agent_provisioner.h"
#include "ma_config_internal.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal/utils/repository/ma_sitelist.h"

#include "ma/internal/defs/ma_compat_defs.h"
#include "ma/datastore/ma_ds_registry.h"
#include "ma/internal/utils/repository/ma_repository_db_manager.h"
#include "ma/internal/buildinfo.h"
#include "ma/datastore/ma_ds_ini.h"
#include "ma/internal/utils/xml/ma_xml.h"
#include "ma/internal/utils/xml/ma_xml_node.h"
#include "ma/internal/defs/ma_ah_defs.h"
#include "ma/internal/defs/ma_property_defs.h"
#include "ma/internal/defs/ma_scheduler_defs.h"
#include "ma/internal/services/io/ma_task_service.h"
#include <stdio.h>

#ifdef MA_WINDOWS
#include <windows.h>
#endif

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "maconfig"

#define TASK_DATA MA_DATASTORE_PATH_SEPARATOR "task_data"
#define TASK_ID MA_DATASTORE_PATH_SEPARATOR "task_id"
#define TASK_SCHEDULER_DATA_PATH MA_SCHEDULER_BASE_PATH_STR TASK_DATA TASK_ID MA_DATASTORE_PATH_SEPARATOR

ma_error_t ma_config_configure_agent_policies(ma_config_context_t *self){
	ma_error_t rc = MA_OK;		
	if(MA_OK == (rc = ma_configurator_configure_agent_policies(self->agent_configurator))) {
		/*Update agent mode, which got change in-between because of intermediate failure/conditions */
		if(self->params->agent_mode != self->params->original_agent_mode){
			 ma_ds_t *ds = ma_configurator_get_datastore(self->agent_configurator);
			 rc = ma_ds_set_int(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_MODE_INT, self->params->agent_mode);
		}
	}

	return rc;
}

static ma_bool_t check_sitelist_has_epo(ma_ds_t *ds, ma_db_t *db, ma_config_context_t *self) {
    ma_bool_t b_rc = MA_TRUE;
    ma_error_t rc = MA_OK;

    ma_db_recordset_t *spipe_records = NULL ;
    if(MA_OK == (rc = ma_repository_db_get_spipe_repositories(db, &spipe_records))) {
        while(MA_ERROR_NO_MORE_ITEMS != (rc = ma_db_recordset_next(spipe_records))) {
            const char *name = NULL;
            (void)ma_db_recordset_get_string(spipe_records, MA_REPO_TABLE_FILED_NAME, &name);
            if(name) {
                ma_xml_t *sitelist_xml = NULL;
	            if(MA_OK == (rc = ma_xml_create(&sitelist_xml)) ) {
                    if(MA_OK == (rc = ma_xml_load_from_buffer(sitelist_xml, (char *)ma_bytebuffer_get_bytes(self->sitelist_data), ma_bytebuffer_get_size(self->sitelist_data)))) {
			            ma_xml_node_t *root_sitelist_node = ma_xml_get_node(sitelist_xml);
			            if(root_sitelist_node) {
				            ma_xml_node_t *node = ma_xml_node_search_by_path(root_sitelist_node,MA_SITELISTS_SPIPE_SITE_PATH);
				            while(node) { 
                                const char *spipe_name = (char *) ma_xml_node_attribute_get(node, MA_SITE_NAME);
                                if(spipe_name && !strcmp(spipe_name, name)) {
                                    b_rc = MA_FALSE;
                                    break;
                                }
                                node = ma_xml_node_get_next_sibling(node);
                            }
			            }
			            else
				            MA_LOG(self->logger, MA_LOG_SEV_WARNING, "Failed to get root node.");
		            }
		            else
			            MA_LOG(self->logger, MA_LOG_SEV_WARNING, "Faield to load xml from buffer, error = (%d)", rc);
		            
		            ma_xml_release(sitelist_xml);
	            }
            }

            if(!b_rc) break;
        }

        if(MA_OK != rc)
            MA_LOG(self->logger, MA_LOG_SEV_WARNING, "Failed to get next record from spipe entries, error = (%d).", rc);

        if(spipe_records) ma_db_recordset_release(spipe_records);
    }
    else
        MA_LOG(self->logger, MA_LOG_SEV_WARNING, "Failed to get spipe entries from MA db, error = (%d).", rc);
    
    if(MA_OK != rc) 
        MA_LOG(self->logger, MA_LOG_SEV_WARNING, "Failed to compare the DB data with sitelist.xml, error = (%d).", rc);

    return b_rc;
}

ma_error_t ma_config_sitelist_handle(ma_config_context_t *self) {
	ma_error_t rc = MA_OK;
	ma_db_t *db = ma_configurator_get_database(self->agent_configurator);
    ma_ds_t *ds = ma_configurator_get_datastore(self->agent_configurator);    
    ma_bool_t is_sitelist_import_required = MA_TRUE;

    if(MA_AGENT_MODE_MANAGED_INT == self->params->agent_mode) {
        if(self->params->need_check) {
            is_sitelist_import_required = check_sitelist_has_epo(ds, db, self);
        }
    }

    if(is_sitelist_import_required) {
	    ma_sitelist_handler_t handler;
	    if(MA_OK == (rc = ma_sitelist_handler_init(&handler, self->crypto, ds, db, self->logger, ma_configurator_get_agent_data_path(self->agent_configurator)))) {
            if(MA_OK == (rc = ma_sitelist_handler_import_xml(&handler, (char*)ma_bytebuffer_get_bytes(self->sitelist_data), ma_bytebuffer_get_size(self->sitelist_data), MA_FALSE))) {
			    MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "successfully imported the sitelist xml.");
		    }
		    else
			    MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to import sitelist, error = (%d).", rc);
	    }
	    else						
		    MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to init sitelist handle, error = (%d).", rc);
    }
    else
        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Sitelist import skipped");
  
    /* Set the sitelist version always as 0 to get the updated sitelist from ePO in first ASCI */
    (void)ma_ds_set_str(ds, MA_AH_POLICIES_SITELIST_PATH_NAME_STR, MA_AH_KEY_SITELIST_GLOBAL_VERSION_STR , "0", -1);

	return rc;
}

/* In case of GUID/TenantId present in DB, delete it, if Agent is in unmanaged mode (957795) */
static void ma_config_handle_unmanaged_mode_datastore(ma_config_context_t *self){
	if(self->params->agent_mode == MA_AGENT_MODE_UNMANAGED_INT) {
		ma_ds_t *ds = NULL;
		if(ds = ma_configurator_get_datastore(self->agent_configurator)) {
			(void)ma_ds_remove(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_GUID_STR);
			(void)ma_ds_remove(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_TENANT_ID_STR);
			(void)ma_ds_remove(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_EPO_VERSION_STR);
		}
	}
}


/* remove all internal task */
static ma_error_t ma_config_handle_localtask(ma_config_context_t *self){
	ma_ds_t *ds = NULL;
	if(ds = ma_configurator_get_scheduler_datastore(self->agent_configurator)) {
		ma_ds_rem(ds, TASK_SCHEDULER_DATA_PATH, MA_PROPERTY_COLLECT_TASK_ID_STR, MA_FALSE);
		ma_ds_rem(ds, TASK_SCHEDULER_DATA_PATH, MA_PRIORITY_EVENT_TRIGGER_TASK_ID_STR, MA_FALSE);
		ma_ds_rem(ds, TASK_SCHEDULER_DATA_PATH, MA_P2P_CONTENT_PURGE_TASK_ID_STR, MA_FALSE);
		ma_ds_rem(ds, TASK_SCHEDULER_DATA_PATH, MA_HTTP_SERVER_SUPERAGENT_REPO_PURGE_TASK_ID_STR, MA_FALSE);
		ma_ds_rem(ds, TASK_SCHEDULER_DATA_PATH, MA_HTTP_SERVER_SUPERAGENT_LAZYCACHE_FLUSH_TASK_ID_STR, MA_FALSE);
		ma_ds_rem(ds, TASK_SCHEDULER_DATA_PATH, MA_POLICY_ENFORCE_TASK_ID_STR, MA_FALSE);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}
/*Update registry */
static ma_error_t registry_update_with_mainfo(ma_config_context_t *self){
	ma_error_t rc = MA_ERROR_INVALIDARG;	
	if(self && self->params){
	#ifdef MA_WINDOWS    
		ma_ds_t *ds = NULL;
		MA_LOG(self->logger, MA_LOG_SEV_TRACE, "writing ma info in registry");
		if(MA_OK == (rc = ma_ds_registry_open(HKEY_LOCAL_MACHINE, MA_CONFIGURATION_REGISTRY_AGENT_FULL_PATH_STR,  MA_DS_REGISTRY_32BIT_VIEW, MA_FALSE, KEY_ALL_ACCESS, (ma_ds_registry_t **) &ds))) {
			char mode[5] = {0};
			MA_MSC_SELECT(_snprintf, snprintf)(mode, 3, "%d", self->params->agent_mode);
			ma_ds_set_str(ds, "", MA_CONFIGURATION_KEY_AGENT_MODE_INT,mode, strlen(mode));
			ma_ds_set_str(ds, "", MA_CONFIGURATION_KEY_AGENT_VERSION_STR,MA_VERSION_STRING, strlen(MA_VERSION_STRING));
			if(self->params->tenant_id)
				ma_ds_set_str(ds, "", MA_CONFIGURATION_KEY_TENANT_ID_STR,self->params->tenant_id, strlen(self->params->tenant_id));

			ma_ds_release(ds);
		}

		if(MA_OK != rc)
			MA_LOG(self->logger, MA_LOG_SEV_INFO, "setting ma info in registry failed error <%d>", rc);
		
	#else
		ma_ds_t    *ini_ds = NULL;
		const char *mainfo_file = "/etc/ma.d/mainfo.ini";
		MA_LOG(self->logger, MA_LOG_SEV_TRACE, "writing ma info in mainfo.ini");
		if(MA_OK == (rc = ma_ds_ini_open(mainfo_file, 0, (ma_ds_ini_t **)&ini_ds))) {			
			rc = ma_ds_set_int(ini_ds, "",MA_CONFIGURATION_KEY_AGENT_MODE_INT, self->params->agent_mode);
			if(self->params->tenant_id)
				rc = ma_ds_set_str(ini_ds, "", MA_CONFIGURATION_KEY_TENANT_ID_STR, self->params->tenant_id, -1);
			ma_ds_release(ini_ds);
		}
		if(MA_OK != rc)
				MA_LOG(self->logger, MA_LOG_SEV_INFO, "setting ma info in mainfo.ini failed error <%d>", rc);

	#endif
	}
	return rc;
}
ma_error_t ma_agent_provisioner_do(ma_config_context_t *self){	
	ma_error_t rc = MA_OK;	
    MA_LOG(self->logger, MA_LOG_SEV_INFO, "agent provisioning started");
	if(MA_OK == (rc = ma_config_configurator_create(self)))	{	
		if(MA_OK == ma_agent_sitelist_process(self)){
			if(MA_OK == (rc = ma_config_crypto_initialize(self))) {
				ma_datastore_configuration_request_t req = {{MA_TRUE,MA_DB_OPEN_READWRITE}, {MA_TRUE,MA_DB_OPEN_READWRITE}, {MA_TRUE,MA_DB_OPEN_READWRITE}, {MA_TRUE,MA_DB_OPEN_READWRITE},{MA_TRUE,MA_DB_OPEN_READWRITE}, {MA_TRUE, MA_DB_OPEN_READWRITE}, {MA_TRUE, MA_DB_OPEN_READWRITE} , {MA_TRUE, MA_DB_OPEN_READWRITE}, {MA_TRUE, MA_DB_OPEN_READWRITE}, {MA_TRUE, MA_DB_OPEN_READWRITE}, {MA_TRUE, MA_DB_OPEN_READWRITE}, {MA_TRUE, MA_DB_OPEN_READWRITE}, {MA_TRUE, MA_DB_OPEN_READWRITE}, {MA_TRUE, MA_DB_OPEN_READWRITE}, {MA_TRUE, MA_DB_OPEN_READWRITE}};
				/*reconfigure configurator,  if agent mode got change in-between because of intermediate failure/conditions */
				if(self->params->agent_mode != self->params->original_agent_mode){
					MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "agent moved form managed to unmanaged, reconfiguring configurator");
					if(MA_OK != (rc = ma_config_configurator_create(self))){
						MA_LOG(self->logger, MA_LOG_SEV_ERROR, "configurator reconfigure failed, error = (%d)", rc);
						return rc;
					}
				}

				if(MA_OK == (rc = ma_configurator_intialize_datastores(self->agent_configurator, &req))) {
					/*change the ownership of macmnsvc db file*/
					if(MA_OK == (rc = ma_configurator_reconfigure_datastores_permissions(self->agent_configurator))){
						if(MA_OK == (rc = ma_config_configure_agent_policies(self))) {
							if(MA_OK == (rc = ma_config_sitelist_handle(self))){
								(void)registry_update_with_mainfo(self);
								(void)ma_config_handle_unmanaged_mode_datastore(self);
								(void)ma_config_handle_localtask(self);
								MA_LOG(self->logger, MA_LOG_SEV_INFO, "agent provisioning finished successfully");
							}
							else
								MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to handle sitelist db, error = (%d).", rc);
						}
						else						
							MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to configure agent policies, error = (%d).", rc);	
					}
					else						
						MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to reconfigure datastore permissions, error = (%d).", rc);	
				}
				else
					MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to create data store, error = (%d)", rc);		
			}
			else
				MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to setup key store, error = (%d)", rc);
		}
		else
			MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Site list processing failed.");
	}
	else
		MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to create maconfig configurator, error = (%d)", rc);
	
	return rc; 
}

ma_error_t ma_agent_provisioner_key_import(ma_config_context_t *self){	
	ma_error_t rc = MA_OK;	
	if(MA_OK == (rc = ma_config_configurator_create(self))) {
		if(MA_OK == (rc = ma_config_crypto_create(self))){
			if(MA_OK == (rc = ma_config_crypto_keystore_setup(self, self->params->agent_provisioning_dir))){
				ma_config_keystore_remove(self->params->agent_provisioning_dir);
				MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "keys import successful");
				return rc;
			}
		}
		else
			MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to setup key store, error = (%d)", rc);						
	}
	else
		MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to create maconfig configurator, error = (%d)", rc);						
	return rc; 
}

