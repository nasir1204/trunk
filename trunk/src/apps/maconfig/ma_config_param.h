#ifndef MA_CONFIG_PARAM_H_INCLUDED
#define MA_CONFIG_PARAM_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_log.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_message.h"

#define MA_MAX_LANG_NAME    256

#define MA_CONFIG_PROVISION						"-provision"
#define MA_CONFIG_ENFORCE						"-enforce"
#define MA_CONFIG_IMPORT						"-import"
#define MA_CONFIG_CUSTOM						"-custom"
#define MA_CONFIG_START							"-start"
#define MA_CONFIG_STOP							"-stop"
#define MA_CONFIG_HELP							"-help"
#define MA_CONFIG_DIR							"-dir"

#define MA_CONFIG_PROVISION_MANAGED				"-managed"
#define MA_CONFIG_PROVISION_UNMANAGED			"-unmanaged"

#define MA_CONFIG_PROVISION_MANAGED_AUTO		"-auto"
#define MA_CONFIG_PROVISION_MANAGED_AUTO_EPO	"-epo"
#define MA_CONFIG_PROVISION_MANAGED_AUTO_USER	"-user"
#define MA_CONFIG_PROVISION_MANAGED_AUTO_PASSWD	"-password"

#define MA_CONFIG_ENFORCE_PROTECTION			"-selfprotection"
#define MA_CONFIG_ENFORCE_NOGUID				"-noguid"
#define MA_CONFIG_ENFORCE_LANGUAGE				"-lang"
#define MA_CONFIG_ENFORCE_LANGUAGE_RESET		"-resetlang"
#define MA_CONFIG_ENFORCE_LOGLEVEL				"-loglevel"

#define MA_CONFIG_IMPORT_KEYS					"-keys"
#define MA_CONFIG_IMPORT_DATA					"-data"
#define MA_CONFIG_IMPORT_CERTS					"-certs"

#define MA_CONFIG_CUSTOM_PROP1				    "-prop1"
#define MA_CONFIG_CUSTOM_PROP2				    "-prop2"
#define MA_CONFIG_CUSTOM_PROP3				    "-prop3"
#define MA_CONFIG_CUSTOM_PROP4				    "-prop4"

#define MA_CONFIG_GUID							"-guid"
#define MA_CONFIG_TENANT_ID						"-tenantid"
#define MA_CONFIG_OTHER_OPTION					"-other"
#define MA_CONFIG_LOG_LOCATION					"-logdir"
#define MA_CONFIG_VDI_MODE						"-vdi"
#define MA_CONFIG_SEQ_NUM						"-seq"
#define MA_CONFIG_LICENSE						"-license"

#define MA_CONFIG_CHECK     					"-check"
#define MA_CONFIG_NOSTART						"-nostart"

#define MA_CONFIG_PRODUCT_ID_STR            "maconfig"
#define MA_EPO_PROVISION_LOCATION			"/remote/RepositoryMgmt.PackageManagedAgentFilesCmd.do"
#define MA_EPO_DEFAULT_PORT					"8443"
#define MA_EPO_TEMP_KEYS_NAME				"msaconfig_keystemp.zip"

MA_CPP( extern "C" { )
typedef struct epo_provision_s{
	char *url;
	char *user_name;
	char *user_passwd;
}epo_provision_t;

typedef struct ma_config_custome_props_s{
	char *custom_prop1;
	char *custom_prop2;
	char *custom_prop3;
	char *custom_prop4;
}ma_config_custome_props_t;

typedef struct ma_config_param_s{
	char						*command;
	char						*agent_provisioning_dir;	/*agent install data dir.*/
	char						*agent_guid;
	char						*tenant_id;
	char						*license_key;
	char						*agent_log_dir;				/*location at which log file to be created*/
	char						*loglevel;
	unsigned short int			vdi_mode;
	unsigned short int			agent_mode;
	unsigned short int			original_agent_mode;
    unsigned short int			agent_crypto_mode;
    unsigned short int			agent_crypto_role;
	int							seq_num;
	unsigned short int			agent_start_mode;			/*start(0) / no start(1) agent. Default behavior is start*/
	epo_provision_t				*epo_provision;
	ma_config_custome_props_t	*custome_props;
    char						language[MA_MAX_LANG_NAME];
    unsigned int				set_lang:1;
    unsigned int				reset_lang:1;				/* language reset switch */ 
    unsigned int                need_check:1;
}ma_config_param_t;

ma_error_t ma_config_param_init(ma_config_param_t *self);
ma_error_t ma_config_param_deinit(ma_config_param_t *self);

ma_error_t ma_config_param_read(int argc, char *argv[], ma_config_param_t *params);

MA_CPP(})
#endif
