#include "ma/ma_common.h"
#include "ma_config_param.h"
#include "ma_agent_provisioner.h"
#include "ma/internal/ma_strdef.h"
#include "ma/ma_log.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef MA_WINDOWS
# define inline MA_INLINE /* Mfetrust incorrectly uses inline which is not a C valid in c89 */
# include "mfetrust.h" /* to bypass VSE AP */
# undef inline

# define START_AP_EXCLUSION(x) \
    HANDLE x = MfeTrustOpen(); \
    BOOL dummy_ap_worked = MfeApTrustProcessStart(x);
# define STOP_AP_EXCLUSION(x)     do {MfeApTrustProcessEnd(x), MfeTrustClose(x), x=0;} while(0);
#else
#define START_AP_EXCLUSION(x) 
#define STOP_AP_EXCLUSION(x)
#endif

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "ma_config"

ma_error_t ma_config_param_handle(ma_config_context_t *self);
ma_error_t ma_config_param_read(int argc, char *argv[], ma_config_param_t *params);

int main(int argc, char *argv[]){

#ifdef HPUX_ALIGNMENT_TRAP_HANDLER
	allow_unaligned_data_access();
#endif

    ma_error_t rc = MA_ERROR_APIFAILED ;
    ma_config_param_t params = {0};     
    START_AP_EXCLUSION(hTrust); 
    if(MA_OK == (rc = ma_config_param_init(&params))) {
        if(MA_OK == (rc = ma_config_param_read(argc, argv, &params))) {
			ma_config_context_t *context = NULL;
			if(MA_OK == (rc = ma_config_context_create(&params, &context))){
				if(MA_OK != (rc = ma_config_param_handle(context)))
					printf("maconfig operation failed with error <%d>", rc);
				ma_config_context_release(context);
			}
        }
        (void)ma_config_param_deinit(&params);
    }
    STOP_AP_EXCLUSION(hTrust); 
	return (int) rc;
}

