#ifndef MA_CONFIG_UTILS_H_INCLUDED
#define MA_CONFIG_UTILS_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/datastructures/ma_bytebuffer.h"

MA_CPP( extern "C" { )

ma_error_t ma_config_get_file_buffer(const char *filename, ma_bytebuffer_t **buffer);

MA_CPP(})

#endif /* MA_CONFIG_UTILS_H_INCLUDED */