#ifndef MA_CMDAGENT_H_INCLUDED
#define MA_CMDAGENT_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_log.h"
#include "ma/logger/ma_log_filter.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_message.h"


#define MA_CMDAGENT_SUCCESS								0
#define MA_CMDAGENT_FAILED								-1

#define MA_CMDAGENT_PARAM_AGENT_LOG_LOCATION			'l'
#define MA_CMDAGENT_PARAM_AGENT_INFO					'i'
#define MA_CMDAGENT_PARAM_CHECK_NEW_POLICY				'c'
#define MA_CMDAGENT_PARAM_ENFORCE_POLICY_LOCALLY		'e'
#define MA_CMDAGENT_PARAM_SEND_PROPERTIES				'p'
#define MA_CMDAGENT_PARAM_SEND_EVENTS					'f'
#define MA_CMDAGENT_PARAM_DISPLY_AGENT_UI				's'
#define MA_CMDAGENT_PARAM_HELP							'h'
#define MA_CMDAGENT_PRODUCT_ID_STR						"cmdagent"

MA_CPP( extern "C" { )

typedef struct ma_cmdagent_s {
    ma_msgbus_t                 *msgbus;
    ma_log_filter_t             *log_filter;
    ma_logger_t                 *logger;
    char						*product_id;
    ma_bool_t                   is_file_logger;
}ma_cmdagent_t;

ma_error_t ma_cmdagent_create(char *product_id, ma_cmdagent_t **cmdagent);
ma_error_t ma_cmdagent_release(ma_cmdagent_t *cmdagent);

ma_error_t ma_cmdagent_send_refresh_policies(ma_cmdagent_t *cmdagent);
ma_error_t ma_cmdagent_send_enforce_policies(ma_cmdagent_t *cmdagent);
ma_error_t ma_cmdagent_send_collect_properties(ma_cmdagent_t *cmdagent);
ma_error_t ma_cmdagent_send_forward_events(ma_cmdagent_t *cmdagent);
ma_error_t ma_cmdagent_check_ui_running();
ma_error_t ma_cmdagent_get_all_agent_info(ma_cmdagent_t *cmdagent);
ma_error_t ma_cmdagent_get_agent_mode(ma_cmdagent_t *cmdagent, int *agent_mode);

MA_CPP(})

#endif /*MA_CMDAGENT_H_INCLUDED*/
