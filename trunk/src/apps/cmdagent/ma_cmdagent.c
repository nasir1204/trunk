#include "ma_cmdagent.h"
#include "ma/logger/ma_console_logger.h"

#include "ma/internal/ma_strdef.h"
#include "ma/datastore/ma_ds_registry.h"
#include "ma/internal/utils/text/ma_utf8.h"
#include "ma/internal/utils/getopt/ma_getopt.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/utils/configurator/ma_configurator_utils.h"
#include "ma/logger/ma_file_logger.h"
#include "ma/internal/defs/ma_cmdagent_defs.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/utils/filesystem/path.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#ifdef MA_WINDOWS
#include <windows.h>
#include <tchar.h>

#define ID_TRAY_STATUSMONITOR           40004
#define SZ_FRAMEWORK_UPDATER_UI			TEXT("UpdaterUI.exe")
#define STR_UPDATERUI_MAIN_WND          TEXT("McAfeeCommonUpdaterUIMainWindow")

HWND FindAgentMonitor(){
    HWND hwndAgentMonitor = NULL;
    hwndAgentMonitor = FindWindow(STR_UPDATERUI_MAIN_WND, NULL);
    return hwndAgentMonitor;
}
#endif

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "cmdagent"

#define MA_CMDAGENT_LOG_FILE_NAME_STR "cmdagent"
char *log_location = NULL;

static ma_error_t ma_cmdagent_log_create(ma_cmdagent_t *cmdagent){
	ma_error_t rc = MA_OK;

    if(log_location){
	    if(MA_OK != (rc = ma_file_logger_create(log_location, MA_CMDAGENT_LOG_FILE_NAME_STR, ".log", (ma_file_logger_t **)&cmdagent->logger)))	{
		    fprintf(stdout, "file logger create failed, last error(%d). Creating console logger.\n", rc);
		    if(MA_OK != (rc = ma_console_logger_create((ma_console_logger_t **)&cmdagent->logger))) {
			    fprintf(stdout, "console logger create failed, last error(%d).\n", rc);
			    return rc;
		    }
	    }else {
            cmdagent->is_file_logger = MA_TRUE;
	        fprintf(stdout, "Check logfile at %s%s%s%s\n", log_location, MA_PATH_SEPARATOR_STR, MA_CMDAGENT_LOG_FILE_NAME_STR, ".log");
        }
    }
    else{
	    if(MA_OK != (rc = ma_console_logger_create((ma_console_logger_t **)&cmdagent->logger))) {
		    fprintf(stdout, "console logger create failed, last error(%d).\n", rc);
		    return rc;
	    }
    }

    (void)ma_generic_log_filter_create(cmdagent->is_file_logger ? "*.Debug" : "*.Info", &cmdagent->log_filter);
    (void)ma_logger_add_filter(cmdagent->logger, cmdagent->log_filter);
    return rc;
}

ma_error_t ma_cmdagent_create(char *product_id, ma_cmdagent_t **cmdagent) {
	ma_error_t rc = MA_ERROR_INVALIDARG;
    if(product_id && cmdagent) {
        ma_cmdagent_t *self = (ma_cmdagent_t *)calloc(1,sizeof(ma_cmdagent_t));
        
        if(!self) return MA_ERROR_OUTOFMEMORY;
        
        if(MA_OK == (rc = ma_cmdagent_log_create(self))) {
		    if(MA_OK == (rc = ma_msgbus_create(product_id, &self->msgbus))) {
                if(self->is_file_logger) (void)ma_msgbus_set_logger(self->msgbus, self->logger);
			    if(MA_OK == (rc = ma_msgbus_start(self->msgbus))) {
					self->product_id = strdup(product_id);
					*cmdagent = self;
					return MA_OK;
				}
                MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to start msgbus, %d.", rc);
                ma_msgbus_release(self->msgbus);
            }
            else
                MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to create msgbus, %d.", rc);
            ma_logger_release(self->logger);
        }
        else
            fprintf(stderr, "Failed to create logger.\n");

        free(self);
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_cmdagent_release(ma_cmdagent_t *self) {
    if(self) {
		if(self->msgbus) {
			ma_msgbus_stop(self->msgbus, MA_TRUE);
			ma_msgbus_release(self->msgbus);
		}

        if(self->log_filter) ma_log_filter_release(self->log_filter);
        if(self->logger) ma_logger_release(self->logger);

		free(self->product_id);  
        free(self);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_cmdagent_check_ui_running() {
#ifdef MA_WINDOWS
	
    HWND hwndAgentMonitor = NULL;

    hwndAgentMonitor = FindAgentMonitor();

    if (!IsWindow(hwndAgentMonitor)) {
        PROCESS_INFORMATION pi = { 0 };
        STARTUPINFO         si = { 0 };
        TCHAR               szFileName[MAX_PATH] = { 0 };
		TCHAR *pStr = _tcsrchr(szFileName, '\\');
		ma_ds_t *reg_ds = NULL;
		if(MA_OK == ma_ds_registry_open(HKEY_LOCAL_MACHINE, MA_CONFIGURATION_REGISTRY_AGENT_FULL_PATH_STR, MA_DS_REGISTRY_32BIT_VIEW, MA_FALSE, KEY_READ, (ma_ds_registry_t **) &reg_ds)) {
			ma_buffer_t *buffer = NULL;
			if(MA_OK == ma_ds_get_str(reg_ds, "", MA_CONFIGURATION_AGENT_KEY_INSTALL_PATH32_STR, &buffer)) {
				const char *p = NULL;
				size_t size = 0;
				if(MA_OK == ma_buffer_get_string(buffer, &p, &size)) {
					ma_utf8_to_wide_char(szFileName, MAX_PATH, p, size);
				}
				ma_buffer_release(buffer);
			}
			ma_ds_release(reg_ds);
			reg_ds = NULL;
		}

        _tcsncat(szFileName, SZ_FRAMEWORK_UPDATER_UI, (MAX_PATH - _tcslen(szFileName) - 1));
		
		if (CreateProcess(szFileName, _T("-UserSpace"), NULL, NULL, FALSE, NULL, NULL, NULL, &si, &pi)) {
			int i = 0;
            for (i = 0; i < 10; ++i) {
				//ret_code = WaitForInputIdle(pi.hProcess, 1000);
				Sleep(1000);
                if (hwndAgentMonitor = FindAgentMonitor()) {
                    break;
                }
            }

            CloseHandle(pi.hProcess);
            CloseHandle(pi.hThread);
        }
        else {
            return MA_ERROR_APIFAILED;
        }
    }

	return MA_OK;
#else
	return MA_ERROR_NOT_SUPPORTED;
#endif
}

static int ma_cmdagent_handle_param(char option){
	ma_error_t rc = MA_OK;
	ma_cmdagent_t *cmdagent  = NULL;
	switch(option) {
		case MA_CMDAGENT_PARAM_DISPLY_AGENT_UI:
			{
				#ifdef MA_WINDOWS
				rc = ma_cmdagent_check_ui_running();
				if(MA_OK == rc) {
					HWND hwndAgentMonitor = FindAgentMonitor();

					if (IsWindow(hwndAgentMonitor)){
						PostMessage(hwndAgentMonitor, WM_COMMAND, MAKELPARAM(ID_TRAY_STATUSMONITOR, 1), 0);
					}
				}
				#endif
				return 0;
			}
			break;
		default:
			break;
	}

	if(MA_OK != (rc = ma_cmdagent_create(MA_CMDAGENT_PRODUCT_ID_STR, &cmdagent))){
		fprintf(stderr, "Failed to create context, exiting.");
		return MA_CMDAGENT_FAILED;
	}

	switch(option){
		int agent_mode = 0;
		case MA_CMDAGENT_PARAM_ENFORCE_POLICY_LOCALLY:			
			rc = ma_cmdagent_send_enforce_policies(cmdagent);
			break;

		case MA_CMDAGENT_PARAM_AGENT_INFO:			
			rc = ma_cmdagent_get_all_agent_info(cmdagent);
			break;

		case MA_CMDAGENT_PARAM_CHECK_NEW_POLICY:
			if(MA_OK == ma_cmdagent_get_agent_mode(cmdagent, &agent_mode)){
				if(MA_AGENT_MODE_UNMANAGED_INT == agent_mode)
					MA_LOG(cmdagent->logger, MA_LOG_SEV_INFO, "Agent is running in unmanaged mode. Can not check new policies.");
				else
					rc = ma_cmdagent_send_refresh_policies(cmdagent);
			}
			else{
				MA_LOG(cmdagent->logger, MA_LOG_SEV_ERROR, "Get agent mode failed. Operation failed");
			}
		    break;

		case MA_CMDAGENT_PARAM_SEND_PROPERTIES:		
			if(MA_OK == ma_cmdagent_get_agent_mode(cmdagent, &agent_mode)){
				if(MA_AGENT_MODE_UNMANAGED_INT == agent_mode)
					MA_LOG(cmdagent->logger, MA_LOG_SEV_INFO, "Agent is running in unmanaged mode. Can not sent properties.");
				else
					rc = ma_cmdagent_send_collect_properties(cmdagent);
			}
			else{
				MA_LOG(cmdagent->logger, MA_LOG_SEV_ERROR, "Get agent mode failed. Operation failed");
			}
			break;

		case MA_CMDAGENT_PARAM_SEND_EVENTS:			
			if(MA_OK == ma_cmdagent_get_agent_mode(cmdagent, &agent_mode)){
				if(MA_AGENT_MODE_UNMANAGED_INT == agent_mode)
					MA_LOG(cmdagent->logger, MA_LOG_SEV_INFO, "Agent is running in unmanaged mode. Can not sent events.");
				else
					rc = ma_cmdagent_send_forward_events(cmdagent);
			}
			else{
				MA_LOG(cmdagent->logger, MA_LOG_SEV_ERROR, "Get agent mode failed. Operation failed.");
			}
			break;
		
		default:
			break;
	}

	ma_cmdagent_release(cmdagent);
	return rc;
}		


/* Usage help  message */
static char *cmdagent_usage[] = {
    "usage: cmdagent [options] \n",
    "where options are:\n",
    "  -c:          checks for new policies\n",
    "  -e:          enforce policies locally\n",
    "  -p:          collect and send properties\n",
    "  -f:          send events\n",
    "  -s:          displays the agent monitor UI\n",
    "  -i:          displays agent info\n",
    "  -l:          set logfile location\n",
    "  -h:          displays help\n\n",
    "   e.g. cmdagent -p \n",
#ifdef MA_WINDOWS
    "        cmdagent -p -l \"E:\\DirectoryName\"\n",
#else
	"        cmdagent -p -l \"/DirectoryName\"\n",
#endif
    NULL
};

static void  print_usage(char **usage) {
    char **pp = NULL;
    for(pp = usage; (*pp != NULL); pp++) {
        printf(*pp);
    }
}
ma_error_t ma_cmdagent_read_param(int argc, char *argv[], char *cmdagent_option){
	int opt;
	ma_error_t rc = MA_ERROR_INVALIDARG;
	while(-1 != (opt = ma_getopt(argc, argv, "icepfshl:"))){		
		if(MA_CMDAGENT_PARAM_CHECK_NEW_POLICY == opt){
			*cmdagent_option = MA_CMDAGENT_PARAM_CHECK_NEW_POLICY;
			rc = MA_OK;
		}
		else if(MA_CMDAGENT_PARAM_AGENT_INFO == opt){
			*cmdagent_option = MA_CMDAGENT_PARAM_AGENT_INFO;
			rc = MA_OK;
		}
		else if(MA_CMDAGENT_PARAM_ENFORCE_POLICY_LOCALLY == opt){
			*cmdagent_option = MA_CMDAGENT_PARAM_ENFORCE_POLICY_LOCALLY;
			rc = MA_OK;
		}
		else if(MA_CMDAGENT_PARAM_SEND_PROPERTIES == opt){
			*cmdagent_option = MA_CMDAGENT_PARAM_SEND_PROPERTIES;
			rc = MA_OK;
		}
		else if(MA_CMDAGENT_PARAM_SEND_EVENTS == opt){
			*cmdagent_option = MA_CMDAGENT_PARAM_SEND_EVENTS;
			rc = MA_OK;
		}
		else if(MA_CMDAGENT_PARAM_AGENT_LOG_LOCATION == opt){
			log_location = strdup(ma_getoptarg());
			rc = MA_OK;
		}
		else if(MA_CMDAGENT_PARAM_DISPLY_AGENT_UI == opt){
			*cmdagent_option = MA_CMDAGENT_PARAM_DISPLY_AGENT_UI;
			#ifndef MA_WINDOWS
			fprintf(stderr, " Displays the Agent Monitor (Not Supported).\n");
			#endif
			rc = MA_OK;
		}
		else if((MA_CMDAGENT_PARAM_HELP == opt)){
			*cmdagent_option = MA_CMDAGENT_PARAM_HELP;
			rc = MA_ERROR_NOT_SUPPORTED;
			break;
		}
		else{
			rc = MA_ERROR_NOT_SUPPORTED;	
			break;
		}
	}  
	return rc;
}

#ifndef UT_MAIN
int main(int argc, char *argv[]){		 
#ifdef HPUX_ALIGNMENT_TRAP_HANDLER
	allow_unaligned_data_access();
#endif
	
	if(argc >= 1) {
        ma_error_t rc = MA_OK;
        char option = 0;	
	    if(MA_OK == (rc =  ma_cmdagent_read_param(argc, argv, &option))) {
	        return ma_cmdagent_handle_param(option);
        }
    }
    print_usage(cmdagent_usage);
	return 0;
}
#endif 

