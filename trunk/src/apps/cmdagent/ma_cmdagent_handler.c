#include "ma_cmdagent.h"
#include "ma/internal/utils/context/ma_context.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/info/ma_info.h"
#include "ma/internal/ma_strdef.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "cmdagent"


ma_error_t ma_cmdagent_send_forward_events(ma_cmdagent_t *cmdagent);

ma_error_t ma_cmdagent_send_collect_properties(ma_cmdagent_t *cmdagent) {
	ma_error_t rc = MA_OK;
	ma_msgbus_endpoint_t *endpoint = NULL;
	ma_message_t *request = NULL;	
	
	MA_LOG(cmdagent->logger, MA_LOG_SEV_TRACE, "sending collect props command.");
	if(MA_OK == (rc = ma_message_create(&request))) {
        if(MA_OK == (rc = ma_message_set_property(request, MA_PROPERTY_MSG_KEY_TYPE_STR, MA_PROPERTY_MSG_TYPE_COLLECT_STR))) {
            if(MA_OK == (rc = ma_message_set_property(request, MA_PROPERTY_MSG_KEY_PROPS_TYPE_STR, MA_PROPERTY_MSG_VALUE_PROPS_TYPE_VERSION_STR))) {
                if(MA_OK == (rc = ma_msgbus_endpoint_create(cmdagent->msgbus, MA_PROPERTY_SERVICE_NAME_STR, NULL, &endpoint))) {
			        if(MA_OK == (rc = ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC))) {
                        ma_message_t *response = NULL;
				        if(MA_OK == (rc = ma_msgbus_send(endpoint, request, &response))) {
                            MA_LOG(cmdagent->logger, MA_LOG_SEV_INFO, "Properties collect and send command initiated by cmdagent.");							
                            ma_message_release(response);
                        }
						else
                            MA_LOG(cmdagent->logger, MA_LOG_SEV_ERROR, "Properties collect and send command initiated by cmdagent failed, %d.", rc);
                    }
                    (void)ma_msgbus_endpoint_release(endpoint);
                }
            }
        }
        (void)ma_message_release(request);
    }
	return rc;
}

ma_error_t ma_cmdagent_send_enforce_policies(ma_cmdagent_t *cmdagent) {
	ma_error_t rc = MA_OK;
	ma_msgbus_endpoint_t *endpoint = NULL;
	ma_message_t *request = NULL;	
	
	MA_LOG(cmdagent->logger, MA_LOG_SEV_TRACE, "sending enforce policies command.");
	if(MA_OK == (rc = ma_message_create(&request))) {
        if(MA_OK == (rc = ma_message_set_property(request, MA_PROP_KEY_POLICY_REQUEST_TYPE_STR, MA_PROP_VALUE_ENFORCE_POLICIES_REQUEST_STR))) {
            if(MA_OK == (rc = ma_message_set_property(request, MA_PROP_KEY_POLICY_ENFORCE_REASON_STR, MA_PROP_VALUE_POLICY_TIMEOUT_STR))) {
                if(MA_OK == (rc = ma_msgbus_endpoint_create(cmdagent->msgbus, MA_POLICY_SERVICE_NAME_STR, MA_POLICY_SERVICE_HOSTNAME_STR, &endpoint))) {
			        if(MA_OK == (rc = ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC))) {
                        ma_message_t *response = NULL;
				        if(MA_OK == (rc = ma_msgbus_send(endpoint, request, &response))) {
                            MA_LOG(cmdagent->logger, MA_LOG_SEV_INFO, "Policy enforcement command initiated by cmdagent.");								
                            ma_message_release(response);
                        }
						else
                            MA_LOG(cmdagent->logger, MA_LOG_SEV_ERROR, "Policy enforcement command initiated by cmdagent failed, %d.", rc);
                    }
                    (void)ma_msgbus_endpoint_release(endpoint);
                }
            }
        }
        (void)ma_message_release(request);
    }
	return rc;
}

ma_error_t ma_cmdagent_send_refresh_policies(ma_cmdagent_t *cmdagent) {
	ma_error_t rc = MA_OK;
	ma_msgbus_endpoint_t *endpoint = NULL;
	ma_message_t *request = NULL;	
	
	MA_LOG(cmdagent->logger, MA_LOG_SEV_TRACE, "sending refresh policies command.");
	if(MA_OK == (rc = ma_message_create(&request))) {
        if(MA_OK == (rc = ma_message_set_property(request, MA_PROP_KEY_POLICY_REQUEST_TYPE_STR, MA_PROP_VALUE_REFRESH_POLICIES_REQUEST_STR))) {
            if(MA_OK == (rc = ma_msgbus_endpoint_create(cmdagent->msgbus, MA_POLICY_SERVICE_NAME_STR, MA_POLICY_SERVICE_HOSTNAME_STR, &endpoint))) {
			    if(MA_OK == (rc = ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC))) {
                    ma_message_t *response = NULL;
				    if(MA_OK == (rc = ma_msgbus_send(endpoint, request, &response))) {
                        MA_LOG(cmdagent->logger, MA_LOG_SEV_INFO, "Refresh policy command initiated by cmdagent.");
                        ma_message_release(response);
                    }
					else
                        MA_LOG(cmdagent->logger, MA_LOG_SEV_ERROR, "Refresh policy command initiated by cmdagent failed, %d.", rc);
                }
                (void)ma_msgbus_endpoint_release(endpoint);
            }
        }
        (void)ma_message_release(request);
    }
	return rc;
}

ma_error_t ma_cmdagent_send_forward_events(ma_cmdagent_t *cmdagent) {
	ma_error_t rc ;
	ma_msgbus_endpoint_t *endpoint = NULL;
	ma_message_t *request = NULL;	
	
    MA_LOG(cmdagent->logger, MA_LOG_SEV_TRACE, "sending upload events command.");
    if(MA_OK == (rc = ma_message_create(&request))) {
        if(MA_OK == (rc = ma_message_set_property(request, MA_EVENT_MSG_PROP_KEY_MSG_TYPE, MA_EVENT_MSG_PROP_VALUE_REQ_UPLOAD_EVENT))) {
            if(MA_OK == (rc = ma_message_set_property(request, MA_EVENT_MSG_PROP_KEY_EVENT_PRIORITY, "1"))) {
                if(MA_OK == (rc = ma_msgbus_endpoint_create(cmdagent->msgbus, MA_EVENT_SERVICE_NAME_STR, MA_EVENT_SERVICE_HOSTNAME_STR, &endpoint))) {
			        if(MA_OK == (rc = ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC))) {
                        ma_message_t *response = NULL;
				        if(MA_OK == (rc = ma_msgbus_send(endpoint, request, &response))) {
                            MA_LOG(cmdagent->logger, MA_LOG_SEV_INFO, "Events send to epo, command initiated by cmdagent");
                            ma_message_release(response);
                        }
					    else
                            MA_LOG(cmdagent->logger, MA_LOG_SEV_ERROR, "Events send to epo, command initiated by cmdagent failed, %d.", rc);
                    }
                    (void)ma_msgbus_endpoint_release(endpoint);
                }
            }
        }
        (void)ma_message_release(request);
    }
	return rc;
}

static ma_error_t ma_cmdagent_get_info(ma_cmdagent_t *cmdagent, ma_variant_t **agent_info_variant) {
    if(cmdagent && agent_info_variant) {
		ma_error_t rc = MA_OK ;
        ma_message_t *get_agent_info_msg = NULL, *response = NULL ;
        if(MA_OK == (rc = ma_message_create(&get_agent_info_msg))) {
            ma_msgbus_endpoint_t *endpoint = NULL ;
            ma_message_set_property(get_agent_info_msg, MA_PROP_KEY_REQUEST_TYPE_STR, MA_PROP_VALUE_AGENT_INFO_COLLECT) ;     
            if(MA_OK == (rc = ma_msgbus_endpoint_create(cmdagent->msgbus, MA_IO_SERVICE_NAME_STR, NULL, &endpoint))) {
				ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                if(MA_OK == (rc = ma_msgbus_send(endpoint, get_agent_info_msg, &response))) {
					rc = ma_message_get_payload(response, agent_info_variant);
                    (void)ma_message_release(response) ;
                }
                (void)ma_msgbus_endpoint_release(endpoint) ;
            }
            (void)ma_message_release(get_agent_info_msg) ;			
        }
        return rc ;		
		
    }
    return MA_ERROR_INVALIDARG;
}

static void remove_bracket(char *key, const char *val){
	if(key && val){
		char *temp_val = NULL;
		if(temp_val = strdup(val)){
			char *ch;
			/*Removing {} from GUID */
			if((strchr(temp_val, '{')) && (ch = strchr(temp_val, '}'))){
				*ch = '\0';
				fprintf(stdout, "%s: %s \n", key, temp_val + 1);
			}
			else
				fprintf(stdout, "%s: %s \n", key, temp_val);

			free(temp_val);
		}
	}
}

static ma_error_t agent_info_value_print(ma_table_t *info_table, char *key){
	ma_error_t rc = MA_ERROR_INVALIDARG;
	if(info_table && key){
		ma_variant_t *key_info_var = NULL;
		if(MA_OK == (rc = ma_table_get_value(info_table, key, &key_info_var))){
			ma_buffer_t *buffer = NULL;
			const char *buff_value = NULL;                    
			if(MA_OK == (rc= ma_variant_get_string_buffer(key_info_var, &buffer))) {
				size_t size = 0;
				if(MA_OK == (rc = ma_buffer_get_string(buffer, &buff_value, &size))) {
					if(!strcmp(MA_INFO_GUID_STR, key)){
						remove_bracket(key, buff_value);
					}else{
						fprintf(stdout, "%s: %s \n", key, buff_value);
					}
				}
				(void)ma_buffer_release(buffer);
			}
			(void)ma_variant_release(key_info_var);
		}
	}
	return rc;
}

static ma_error_t agent_info_value_get(ma_table_t *info_table, char *key, char **val){
	ma_error_t rc = MA_ERROR_INVALIDARG;
	if(info_table && key && val){
		ma_variant_t *key_info_var = NULL;
		if(MA_OK == (rc = ma_table_get_value(info_table, key, &key_info_var))){
			ma_buffer_t *buffer = NULL;
			const char *buff_value = NULL;                    
			if(MA_OK == (rc= ma_variant_get_string_buffer(key_info_var, &buffer))) {
				size_t size = 0;
				if((MA_OK == (rc = ma_buffer_get_string(buffer, &buff_value, &size))) && buff_value) {
					if(!strcmp(MA_INFO_GUID_STR, key)){
						remove_bracket(key, buff_value);
					}else{
						if(!(*val = strdup(buff_value))){
							fprintf(stdout, "error: memory allocation failed for %s: %s \n", key, buff_value);
							rc = MA_ERROR_APIFAILED;
						}
					}
				}
				(void)ma_buffer_release(buffer);
			}
			(void)ma_variant_release(key_info_var);
		}
	}
	return rc;
}
/*Note: Used separate message for each key, instead of single message because of utilization of existing handler as well as this feature is used non-frequently. So not much overhead*/
ma_error_t ma_cmdagent_get_all_agent_info(ma_cmdagent_t *cmdagent){
	ma_error_t rc = MA_ERROR_INVALIDARG;
	ma_variant_t *agent_info_variant = NULL;
	if(MA_OK == (rc = ma_cmdagent_get_info(cmdagent, &agent_info_variant))){
		ma_table_t *info_table = NULL;
		if(MA_OK == (rc = ma_variant_get_table(agent_info_variant, &info_table))){
			fprintf(stdout, "Component: McAfee Agent \n");
			if(MA_OK != agent_info_value_print(info_table, MA_INFO_AGENT_MODE_STR)){
				fprintf(stdout, "Failed to get value of %s \n", MA_INFO_AGENT_MODE_STR);
			}
			if(MA_OK != agent_info_value_print(info_table, MA_INFO_VERSION_STR)){
				fprintf(stdout, "Failed to get value of %s \n", MA_INFO_VERSION_STR);
			}
			if(MA_OK != agent_info_value_print(info_table, MA_INFO_GUID_STR)){
				fprintf(stdout, "Failed to get value of %s \n", MA_INFO_GUID_STR);
			}
			if(MA_OK != agent_info_value_print(info_table, MA_INFO_TENANTID_STR)){
				fprintf(stdout, "Failed to get value of %s \n", MA_INFO_TENANTID_STR);
			}
			if(MA_OK != agent_info_value_print(info_table, MA_INFO_LOG_LOCATION_STR)){
				fprintf(stdout, "Failed to get value of %s \n", MA_INFO_LOG_LOCATION_STR);
			}
			if(MA_OK != agent_info_value_print(info_table, MA_INFO_INSTALL_LOCATION_STR)){
				fprintf(stdout, "Failed to get value of %s \n", MA_INFO_INSTALL_LOCATION_STR);
			}
			if(MA_OK != agent_info_value_print(info_table, MA_INFO_CRYPTO_MODE_STR)){
				fprintf(stdout, "Failed to get value of %s \n", MA_INFO_CRYPTO_MODE_STR);
			}
			if(MA_OK != agent_info_value_print(info_table, MA_INFO_DATA_LOCATION_STR)){
				fprintf(stdout, "Failed to get value of %s \n", MA_INFO_DATA_LOCATION_STR);
			}
			if(MA_OK != agent_info_value_print(info_table, MA_INFO_EPO_SERVER_LIST)){
				fprintf(stdout, "Failed to get value of %s \n", MA_INFO_EPO_SERVER_LIST);
			}
			if(MA_OK != agent_info_value_print(info_table, MA_INFO_EPO_PORT_LIST)){
				fprintf(stdout, "Failed to get value of %s \n", MA_INFO_EPO_PORT_LIST);
			}
			if(MA_OK != agent_info_value_print(info_table, MA_INFO_EPO_SERVER_LAST_USED_STR)){
				fprintf(stdout, "Failed to get value of %s \n", MA_INFO_EPO_SERVER_LAST_USED_STR);
			}
			if(MA_OK != agent_info_value_print(info_table, MA_INFO_LAST_ASC_TIME)){
				fprintf(stdout, "Failed to get value of %s \n", MA_INFO_LAST_ASC_TIME);
			}
			if(MA_OK != agent_info_value_print(info_table, MA_INFO_LAST_POLICY_UPDATE_TIME)){
				fprintf(stdout, "Failed to get value of %s \n", MA_INFO_LAST_POLICY_UPDATE_TIME);
			}
			if(MA_OK != agent_info_value_print(info_table, MA_INFO_EPO_VERSION_STR)){
				fprintf(stdout, "Failed to get value of %s \n", MA_INFO_EPO_VERSION_STR);
			}
			(void)agent_info_value_print(info_table, MA_PROPERTY_PROPERTIES_CUSTOM1_STR);
			(void)agent_info_value_print(info_table, MA_PROPERTY_PROPERTIES_CUSTOM2_STR);
			(void)agent_info_value_print(info_table, MA_PROPERTY_PROPERTIES_CUSTOM3_STR);
			(void)agent_info_value_print(info_table, MA_PROPERTY_PROPERTIES_CUSTOM4_STR);

			(void)ma_table_release(info_table);
		}
		(void)ma_variant_release(agent_info_variant);
	}
	if(MA_OK != rc)
		fprintf(stdout, "Failed to get ma info, error code <%d>\n", rc);
	return rc;
}

ma_error_t ma_cmdagent_get_agent_mode(ma_cmdagent_t *cmdagent, int *agent_mode){
	ma_error_t rc = MA_ERROR_INVALIDARG;
	if(cmdagent) {
		if(agent_mode){
			ma_variant_t *agent_info_variant = NULL;
			if(MA_OK == (rc = ma_cmdagent_get_info(cmdagent, &agent_info_variant))){
				ma_table_t *info_table = NULL;
				if(MA_OK == (rc = ma_variant_get_table(agent_info_variant, &info_table))){
					char *mode = NULL;
					if( (MA_OK == (rc = agent_info_value_get(info_table, MA_INFO_AGENT_MODE_STR, &mode))) && mode)
						*agent_mode = atoi(mode);
					else
						MA_LOG(cmdagent->logger, MA_LOG_SEV_ERROR, "Failed to get value of %s.", MA_INFO_AGENT_MODE_STR);
					(void)ma_table_release(info_table);
				}
				(void)ma_variant_release(agent_info_variant);
			}
		}
		if(MA_OK != rc)
			MA_LOG(cmdagent->logger, MA_LOG_SEV_ERROR, "Failed to get ma info, error code <%d>.", rc);
	}
	return rc;
}
