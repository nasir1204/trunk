#include "ma_crypto_service.h"
#include "ma/internal/utils/macrypto/ma_crypto.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/ma_strdef.h"

#include "ma/ma_msgbus.h"
#include "ma/ma_message.h"
#include "ma/crypto/ma_crypto.h"

#include <stdlib.h>
#include <string.h>

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "cryptoservice"

struct ma_crypto_service_s {
    ma_service_t                        base;
	
	ma_context_t                        *context;

    ma_crypto_t                         *crypto;
    
    ma_crypto_mode_t                    crypto_mode;
	
};

static ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint);

static ma_error_t service_start(ma_service_t *service);

static ma_error_t service_stop(ma_service_t *service);

static void service_destroy(ma_service_t *service);

static const ma_service_methods_t methods = {
    &service_configure,
    &service_start,
    &service_stop,
    &service_destroy
};	


ma_error_t ma_crypto_service_create(const char *service_name, ma_service_t **service) {
    if(service) {
        ma_error_t rc = MA_OK;
        ma_crypto_service_t *self = (ma_crypto_service_t *)calloc(1, sizeof(ma_crypto_service_t));
        
        if(!self)   return MA_ERROR_OUTOFMEMORY;

        if(MA_OK == (rc = ma_crypto_create(&self->crypto))) {
            ma_service_init((ma_service_t *)self, &methods, service_name, self);
            *service = (ma_service_t *)self;
            return MA_OK;
        }

        free(self);
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

/* TODO - register for prevent of loop run and then do the whole thing */
static ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint) {
    if(service && context) {
        ma_error_t rc = MA_OK;
        if(MA_SERVICE_CONFIG_NEW == hint) {
            ma_crypto_service_t *self = (ma_crypto_service_t *)service;
            ma_configurator_t *configurator = MA_CONTEXT_GET_CONFIGURATOR(context);
            ma_ds_t *ds = ma_configurator_get_datastore(configurator);

            if(!configurator || !ds) return MA_ERROR_PRECONDITION;
            else {
                ma_crypto_ctx_t *ctx = NULL;
                if(MA_OK == (rc = ma_crypto_ctx_create(&ctx))) {
                    char *p = NULL; 
                    size_t size = 0;        
                    ma_buffer_t *buffer = NULL;
                    ma_crypto_role_t crypto_role = MA_CRYPTO_ROLE_USER;
                    ma_crypto_agent_mode_t agent_mode = MA_CRYPTO_AGENT_UNMANAGED;
                    
                    self->crypto_mode = MA_CRYPTO_MODE_NON_FIPS;
                    (void)ma_crypto_ctx_set_logger(ctx,MA_CONTEXT_GET_LOGGER(context));
                    (void)ma_crypto_ctx_set_role(ctx, MA_CRYPTO_ROLE_USER);
                    (void)ma_crypto_ctx_set_crypto_lib_path(ctx, ma_configurator_get_agent_lib_path(configurator, MA_FALSE));

                    if(MA_OK == (rc = ma_ds_get_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_KEYSTORE_PATH_STR, &buffer))) {
                        if(MA_OK == (rc = ma_buffer_get_string(buffer, &p, &size))) 
                            ma_crypto_ctx_set_keystore(ctx, p);
                        ma_buffer_release(buffer);
                    }

                    if((MA_OK == rc) && (MA_OK == (rc = ma_ds_get_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_CERTSTORE_PATH_STR, &buffer)))) {
                        if(MA_OK == (rc = ma_buffer_get_string(buffer, &p, &size))) 
                            ma_crypto_ctx_set_certstore(ctx, p);
                        ma_buffer_release(buffer);
                    }

                    if((MA_OK == rc) && (MA_OK == (rc = ma_ds_get_int16(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_MODE_INT, (ma_int16_t*)(&agent_mode))))) 
                        ma_crypto_ctx_set_agent_mode(ctx, agent_mode);

                    if((MA_OK == rc) && (MA_OK == (rc = ma_ds_get_int16(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_CRYPTO_MODE_INT, (ma_int16_t*)(&self->crypto_mode)))))
                        ma_crypto_ctx_set_fips_mode(ctx, self->crypto_mode);
                    
                    if((MA_OK == rc) && (MA_OK == (rc = ma_crypto_initialize(ctx, self->crypto)))) {
                        (void) ma_crypto_ctx_release(ctx);

                        ma_context_add_object_info(context, MA_OBJECT_CRYPTO_NAME_STR, &self->crypto);   
                        self->context = context;
                        return MA_OK;
                    }
                    (void) ma_crypto_ctx_release(ctx);
                }
            }
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}


static ma_error_t service_start(ma_service_t *service) {
	return MA_OK;
}

static ma_error_t service_stop(ma_service_t *service) {
    return MA_OK;
}

static void service_destroy(ma_service_t *service) {
    if(service) {
        ma_crypto_service_t *self = (ma_crypto_service_t *)service;   
		(void) ma_crypto_deinitialize(self->crypto);
        ma_crypto_release(self->crypto);
        free(self);
    }
}