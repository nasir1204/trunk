#include "ma/internal/apps/macmnsvc/ma_service_manager_service.h"
#include "ma/internal/ma_strdef.h"

#include "ma/internal/apps/macmnsvc/ma_common_services_db.h"
#include "ma/internal/core/msgbus/ma_msgbus_defs.h"
#include "ma/internal/utils/regex/ma_regex.h"
#include "ma/internal/utils/configurator/ma_configurator.h"

#include "ma/ma_message.h"
#include "ma/internal/ma_macros.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "svcmgr"

struct ma_services_manager_service_s {
    ma_service_t     base_service;
    ma_msgbus_server_t	*srvc_mgr_server;

    ma_logger_t         *logger;

    ma_db_t             *db_msgbus;

    ma_msgbus_t         *msgbus;
} ; 

static ma_error_t ma_services_manager_service_start(ma_services_manager_service_t *self);
static ma_error_t ma_services_manager_service_stop(ma_services_manager_service_t *self);
static ma_error_t ma_services_manager_service_release(ma_services_manager_service_t *self);


static ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint);


static ma_error_t service_start(ma_service_t *service) {
    return ma_services_manager_service_start((ma_services_manager_service_t *)service);
}

static ma_error_t service_stop(ma_service_t *service) {
    return ma_services_manager_service_stop((ma_services_manager_service_t *)service);
}

static void service_release(ma_service_t *service) {
    ma_services_manager_service_release((ma_services_manager_service_t *)service);
}

static const ma_service_methods_t service_methods = {
    &service_configure,
    &service_start,
    &service_stop,
    &service_release
};	

ma_error_t ma_services_manager_service_create(char const *service_name, ma_service_t **srvc_mgr_service) {
	if (service_name && srvc_mgr_service)
	{		
		ma_services_manager_service_t *self = (ma_services_manager_service_t *) calloc(1, sizeof(ma_services_manager_service_t));
		if(self) {
			ma_service_init(&self->base_service, &service_methods, service_name, self);
			*srvc_mgr_service = &self->base_service;                
			return MA_OK;
		}            
		return MA_ERROR_OUTOFMEMORY;
	}
	return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_services_manager_service_cb(ma_msgbus_server_t *server, ma_message_t *request_payload, void *cb_data, ma_msgbus_client_request_t *c_request);
static ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint) {
    ma_services_manager_service_t *self = (ma_services_manager_service_t *)service;
    ma_error_t rc = MA_OK;
    ma_configurator_t *configurator = NULL;

    /* optional, ok if not there */
    self->logger = MA_CONTEXT_GET_LOGGER(context);

    /* required, fail if any not there */
    if (!(self->msgbus = MA_CONTEXT_GET_MSGBUS(context)) || 
        !(configurator = MA_CONTEXT_GET_CONFIGURATOR(context))) {
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "some required information missing from context - (msgbus,configurator) = (%p,%p)", self->msgbus, configurator);
            return MA_ERROR_INVALIDARG;
    }

    self->db_msgbus = ma_configurator_get_msgbus_database(configurator);

    if(MA_SERVICE_CONFIG_NEW == hint) {
        if(MA_OK == (rc = ma_msgbus_server_create(self->msgbus, MA_MSGBUS_SERVICE_MANAGER_SERVICE_NAME_STR, &self->srvc_mgr_server))){
            (void)ma_msgbus_server_setopt(self->srvc_mgr_server, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
		    (void)ma_msgbus_server_setopt(self->srvc_mgr_server, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
		    if(MA_OK != (rc = ma_msgbus_server_start(self->srvc_mgr_server, ma_services_manager_service_cb, self)))
                MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to Start Services Manager service - rc <%d>", rc);    
            return rc;
        }else
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "could not register name_service with msgbus, error = <%ld>", rc);
    }
    return rc;

}

static ma_error_t get_subscribers_list(ma_services_manager_service_t *self, ma_message_t *request_payload, ma_msgbus_client_request_t *c_request) {
	ma_error_t rc = MA_OK;
	char *topic_filter = NULL;
	
	if((MA_OK == (rc = ma_message_get_property(request_payload, MA_PROP_KEY_SUBSCRIBER_TOPIC_FILTER_STR, &topic_filter))) && topic_filter) {
		ma_db_recordset_t *record_set = NULL;	
		ma_message_t *reply = NULL;
		ma_array_t *subscribers_array = NULL;
		char *query_input = NULL, *tmp_str = NULL;
        ma_bool_t is_query_reg_exp = MA_FALSE;

		/* If topic filter contains the .(dot), we can query with regular expression.
			e.g: topic name = ma.policy.timeout, query input can be ma.% */
		if(NULL != (tmp_str = strchr(topic_filter, '.'))) {
			ma_int32_t len = tmp_str - topic_filter ;
			query_input = (char *)calloc(len + 2 + 1, sizeof(char));
			strncpy(query_input, topic_filter , len);
			strncat(query_input, ".%", 2);
            is_query_reg_exp = MA_TRUE;
		}
		
		if(MA_OK != (rc = db_get_subscribers_list(self->db_msgbus, (is_query_reg_exp) ? query_input : topic_filter, &record_set)))
			MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to get subscribers list for input topic name <%s> - rc <%d>",  (is_query_reg_exp) ? query_input : topic_filter, rc);
	 
		if(query_input) free(query_input);

		if(MA_OK == rc) {
			rc = ma_array_create(&subscribers_array);
		}

		while((MA_OK == rc) && (MA_ERROR_NO_MORE_ITEMS != (rc = ma_db_recordset_next(record_set)))) {
			char *topic_name = NULL, *product_id = NULL;			
			ma_table_t *subscriber_table = NULL;
			ma_bool_t is_record_match = MA_FALSE;

			if(MA_OK != rc)
				break;

			(void)ma_db_recordset_get_string(record_set, 1, &topic_name);

            if(is_query_reg_exp) {
			    if(topic_name) {
				    ma_regex_t *regex_engine = NULL;				
				    if(MA_OK == (rc = ma_regex_create(topic_filter , &regex_engine))) {			
					    is_record_match = ma_regex_match(regex_engine, topic_name);							
					    (void)ma_regex_release(regex_engine); regex_engine = NULL;
				    }	
                    if(!is_record_match) {
                        if(MA_OK == (rc = ma_regex_create(topic_name , &regex_engine))) {			
					        is_record_match = ma_regex_match(regex_engine, topic_filter);							
					        (void)ma_regex_release(regex_engine); regex_engine = NULL;
				        }	
                    }
			    }
            }
            else
                is_record_match = MA_TRUE;

			if(is_record_match) {
				(void)ma_db_recordset_get_string(record_set, 3, &product_id); 				
			}
			
			if((MA_OK == rc) && is_record_match && product_id) {
				ma_variant_t *var = NULL;
				char pid_str[MAX_PID_LEN] = {0};

				if(MA_OK != (rc = ma_table_create(&subscriber_table)))
					continue;

				MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Got New record from subscriber table: topic_name <%s> product_id <%s>", topic_name, product_id);

				if(MA_OK == (rc = ma_variant_create_from_string(topic_name, strlen(topic_name), &var))) {
					(void)ma_table_add_entry(subscriber_table, MA_SUBSCRIBER_TOPIC_NAME_STR, var);
					(void)ma_variant_release(var); var = NULL;
				}

				if(MA_OK == (rc = ma_variant_create_from_string(product_id, strlen(product_id), &var))) {
					(void)ma_table_add_entry(subscriber_table, MA_SUBSCRIBER_PRODUCT_ID_STR, var);					
					(void)ma_variant_release(var); var = NULL;
				}				

				if(MA_OK == (rc = ma_variant_create_from_table(subscriber_table, &var))) {
					(void)ma_array_push(subscribers_array, var);
					(void)ma_variant_release(var); var = NULL;
				}				

				if(subscriber_table) {
					(void)ma_table_release(subscriber_table);
					subscriber_table = NULL;
				}
			}					
		}
	
		if(MA_ERROR_NO_MORE_ITEMS == rc)
			rc = MA_OK;
		
		if(record_set) (void)ma_db_recordset_release(record_set);
		
		if(subscribers_array) {
			ma_variant_t *payload = NULL;
			if(MA_OK == (rc = ma_variant_create_from_array(subscribers_array, &payload))) {
				if (MA_OK == (rc = ma_message_create(&reply))) {
					if(MA_OK == (rc = ma_message_set_payload(reply, payload))) {					
						(void)ma_message_set_property(reply , MA_PROP_KEY_SERVICE_MANAGER_REPLY_TYPE_STR , MA_PROP_VALUE_GET_SUBSCRIBERS_LIST_REQUEST_STR);					
						(void)ma_msgbus_server_client_request_post_result(c_request, MA_OK, reply);
						MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Successfully send the reply message for request <%s>", MA_PROP_VALUE_GET_SUBSCRIBERS_LIST_REQUEST_STR);					
					}
					(void)ma_message_release(reply);
				}
				(void)ma_variant_release(payload); payload = NULL;
			}
			(void)ma_array_release(subscribers_array);
		}				
	}	 
	return rc;
}

static ma_error_t get_services_list(ma_services_manager_service_t *self, ma_message_t *request_payload, ma_msgbus_client_request_t *c_request) {
	ma_error_t rc = MA_OK;
	char *service_filter = NULL;
	
	if((MA_OK == (rc = ma_message_get_property(request_payload, MA_PROP_KEY_SERVICE_NAME_FILTER_STR, &service_filter))) && service_filter) {
		ma_db_recordset_t *record_set = NULL;	
		ma_message_t *reply = NULL;
		ma_array_t *services_array = NULL;
		char *query_input = NULL;
        ma_bool_t is_query_reg_exp = MA_FALSE;
		ma_int32_t len = strlen(service_filter);

		if(!strcmp(service_filter+(len-2), ".*")) {			
			query_input = (char *)calloc(len + 1, sizeof(char));
			strncpy(query_input, service_filter , len-2);
			strncat(query_input, ".%", 2);
            is_query_reg_exp = MA_TRUE;
		}
		
		if(MA_OK != (rc = db_get_services_list(self->db_msgbus, (is_query_reg_exp) ? query_input : service_filter, &record_set)))
			MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to get services list for input service name filter <%s> - rc <%d>",  (is_query_reg_exp) ? query_input : service_filter, rc);
	 
		if(query_input) free(query_input);

		if(MA_OK == rc) {
			rc = ma_array_create(&services_array);
		}

		while((MA_OK == rc) && (MA_ERROR_NO_MORE_ITEMS != (rc = ma_db_recordset_next(record_set)))) {
			char *service_name = NULL, *product_id = NULL;			
			ma_table_t *service_table = NULL;
			ma_bool_t is_record_match = MA_FALSE;

			if(MA_OK != rc)
				break;

			(void)ma_db_recordset_get_string(record_set, 1, &service_name);
           	(void)ma_db_recordset_get_string(record_set, 3, &product_id); 
			
			if(service_name && product_id) {
				ma_variant_t *var = NULL;				

				if(MA_OK != (rc = ma_table_create(&service_table)))
					continue;

				MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Got New record from service table: service name <%s> product_id <%s>", service_name, product_id);
				
				if(MA_OK == (rc = ma_variant_create_from_string(service_name, strlen(service_name), &var))) {
					(void)ma_table_add_entry(service_table, MA_SERVICE_NAME_STR, var);
					(void)ma_variant_release(var); var = NULL;
				}

				if(MA_OK == (rc = ma_variant_create_from_string(product_id, strlen(product_id), &var))) {
					(void)ma_table_add_entry(service_table, MA_SERVICE_PRODUCT_ID_STR, var);					
					(void)ma_variant_release(var); var = NULL;
				}				

				if(MA_OK == (rc = ma_variant_create_from_table(service_table, &var))) {
					(void)ma_array_push(services_array, var);
					(void)ma_variant_release(var); var = NULL;
				}				

				if(service_table) {
					(void)ma_table_release(service_table);
					service_table = NULL;
				}
			}					
		}
	
		if(MA_ERROR_NO_MORE_ITEMS == rc)
			rc = MA_OK;
		
		if(record_set) (void)ma_db_recordset_release(record_set);
		
		if(services_array) {
			ma_variant_t *payload = NULL;
			if(MA_OK == (rc = ma_variant_create_from_array(services_array, &payload))) {
				if (MA_OK == (rc = ma_message_create(&reply))) {
					if(MA_OK == (rc = ma_message_set_payload(reply, payload))) {					
						(void)ma_message_set_property(reply , MA_PROP_KEY_SERVICE_MANAGER_REPLY_TYPE_STR , MA_PROP_VALUE_GET_SERVICES_LIST_REQUEST_STR);					
						(void)ma_msgbus_server_client_request_post_result(c_request, MA_OK, reply);
						MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Successfully send the reply message for request <%s>", MA_PROP_VALUE_GET_SUBSCRIBERS_LIST_REQUEST_STR);					
					}
					(void)ma_message_release(reply);
				}
				(void)ma_variant_release(payload); payload = NULL;
			}
			(void)ma_array_release(services_array);
		}				
	}	 
	return rc;
}

static ma_error_t ma_services_manager_service_cb(ma_msgbus_server_t *server, ma_message_t *request_payload, void *cb_data, ma_msgbus_client_request_t *c_request) {
	ma_error_t rc = MA_OK;
    char *value = NULL;
	
	ma_services_manager_service_t *self = (ma_services_manager_service_t *) cb_data;

    MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Service Manager Service server callback Invoked");

    /* Message Filter */
    if(MA_OK == (rc = ma_message_get_property(request_payload, MA_PROP_KEY_SERVICE_MANAGER_REQUEST_TYPE_STR, &value))) {

        if (0 == strcmp(MA_PROP_VALUE_GET_SUBSCRIBERS_LIST_REQUEST_STR, value))  {
			rc = get_subscribers_list(self, request_payload, c_request);
		} 
        else if (0 == strcmp(MA_PROP_VALUE_GET_SERVICES_LIST_REQUEST_STR, value))  {
			rc = get_services_list(self, request_payload, c_request);
		} 
        else {
            rc = MA_BROKER_INVALID_REQUEST;
            MA_LOG(self->logger, MA_LOG_SEV_TRACE, "Invalied request type <%s>", value);
        }

        if(MA_OK != rc) {
            ma_message_t *reply = NULL;
            ma_error_t reply_status = rc;
            if (MA_OK == (rc = ma_message_create(&reply))) {
		        (void)ma_message_set_property(reply , MA_PROP_KEY_SERVICE_MANAGER_REPLY_TYPE_STR , value);					
		        (void)ma_msgbus_server_client_request_post_result(c_request, reply_status, reply);
	            (void)ma_message_release(reply);
            }
        }
    }

	return rc;
}

ma_error_t ma_services_manager_service_start(ma_services_manager_service_t *self) {
    if(self) {
        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Services Manager Service Starts");
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_services_manager_service_stop(ma_services_manager_service_t *self) {    
    if(self) {
        ma_error_t rc = MA_OK;
		
        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Services Manager Service Stops");
		       
		if(MA_OK != (rc = ma_msgbus_server_stop(self->srvc_mgr_server)))
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to stop Services Manager service - rc <%d>", rc);

        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_services_manager_service_release(ma_services_manager_service_t *self) {
    if(self) {
        ma_error_t rc = MA_OK;
		if(MA_OK != (rc = ma_msgbus_server_release(self->srvc_mgr_server)))
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Services Manager service release failed rc = <%d>", rc);

        free(self);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

