#include "ma/internal/apps/macmnsvc/ma_common_services_db.h"
#include <string.h>

ma_error_t db_add_broker_instance(ma_db_t *db_handle) {
	if(db_handle){
		ma_db_statement_t *db_stmt = NULL;
		ma_error_t rc = MA_OK ;

		if(MA_OK == (rc = ma_db_transaction_begin(db_handle))) {
			if( MA_OK == rc && MA_OK == (rc = ma_db_statement_create(db_handle, CREATE_MSGBUS_SERVICES_TABLE_STMT, &db_stmt))) {
				rc = ma_db_statement_execute(db_stmt) ;
				ma_db_statement_release(db_stmt); db_stmt = NULL ;
				if( MA_OK == rc && MA_OK == (rc = ma_db_statement_create(db_handle, CREATE_MSGBUS_SUBSCRIBERS_TABLE_STMT, &db_stmt))) {
					if(MA_OK == (rc = ma_db_statement_execute(db_stmt)))
						(void)ma_db_transaction_end(db_handle) ;
					ma_db_statement_release(db_stmt); db_stmt = NULL ;
				}
			}    
			if(MA_OK != rc)
				ma_db_transaction_cancel(db_handle) ;
		}
		return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}


#define ADD_MSGBUS_SERVICE_STMT "INSERT INTO MA_MSGBUS_SERVICES VALUES(?, ?, ?, ?, ?)"
ma_error_t db_add_service(ma_db_t *db_handle, const char *service_name, const char *service_id, const char *product_id, ma_uint32_t process_id, const char *host_name) {
	if(db_handle && service_name && service_id && product_id && process_id && host_name) {
        ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;

        if( MA_OK == (rc = ma_db_statement_create(db_handle, ADD_MSGBUS_SERVICE_STMT, &db_stmt))) 
        {
            if( MA_OK == (rc = ma_db_statement_set_string(db_stmt, 1, 1, service_name, strlen(service_name)))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 2, 1, service_id, strlen(service_id)))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 3, 1, product_id, strlen(product_id)))
                && MA_OK == (rc = ma_db_statement_set_int(db_stmt, 4, process_id)) 
				&& MA_OK == (rc = ma_db_statement_set_string(db_stmt, 5, 1, host_name, strlen(host_name))) )
            {
                rc = ma_db_statement_execute(db_stmt) ;
            }
            ma_db_statement_release(db_stmt) ;
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

#define ADD_MSGBUS_SUBSCRIBER_STMT "INSERT INTO MA_MSGBUS_SUBSCRIBERS VALUES(?, ?, ?, ?, ?)"
ma_error_t db_add_subscriber(ma_db_t *db_handle, const char *topic_name, const char *service_id, const char *product_id, ma_uint32_t process_id, const char *host_name) {
	if(db_handle && topic_name && service_id && product_id && process_id && host_name) {
        ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;

        if( MA_OK == (rc = ma_db_statement_create(db_handle, ADD_MSGBUS_SUBSCRIBER_STMT, &db_stmt))) 
        {
            if( MA_OK == (rc = ma_db_statement_set_string(db_stmt, 1, 1, topic_name, strlen(topic_name)))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 2, 1, service_id, strlen(service_id)))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 3, 1, product_id, strlen(product_id)))
                && MA_OK == (rc = ma_db_statement_set_int(db_stmt, 4, process_id)) 
				&& MA_OK == (rc = ma_db_statement_set_string(db_stmt, 5, 1, host_name, strlen(host_name))) )
            {
                rc = ma_db_statement_execute(db_stmt) ;
            }
            ma_db_statement_release(db_stmt) ;
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

#define REMOVE_MSGBUS_SERVICE_STMT    "DELETE FROM MA_MSGBUS_SERVICES WHERE SERVICE_NAME = ?"
ma_error_t db_remove_service(ma_db_t *db_handle, const char *service_name) {
	 if(db_handle && service_name) {
        ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;

        if( MA_OK == (rc = ma_db_statement_create(db_handle, REMOVE_MSGBUS_SERVICE_STMT, &db_stmt)))
        {
            if( MA_OK == (rc = ma_db_statement_set_string(db_stmt, 1, 1, service_name, strlen(service_name))) )
				rc = ma_db_statement_execute(db_stmt) ;            
            ma_db_statement_release(db_stmt) ;
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

#define REMOVE_MSGBUS_SUBSCRIBER_STMT    "DELETE FROM MA_MSGBUS_SUBSCRIBERS WHERE TOPIC_NAME = ? AND PRODUCT_ID = ? AND PROCESS_ID = ?"
ma_error_t db_remove_subscriber(ma_db_t *db_handle, const char *topic_name, const char *product_id, ma_uint32_t process_id) {
	if(db_handle && topic_name && product_id && process_id) {
        ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;

        if( MA_OK == (rc = ma_db_statement_create(db_handle, REMOVE_MSGBUS_SUBSCRIBER_STMT, &db_stmt)))
        {
            if( MA_OK == (rc = ma_db_statement_set_string(db_stmt, 1, 1, topic_name, strlen(topic_name)))
				&& MA_OK == (rc = ma_db_statement_set_string(db_stmt, 2, 1, product_id, strlen(product_id)))
				&& MA_OK == (rc = ma_db_statement_set_int(db_stmt, 3, process_id)) )
            {
                rc = ma_db_statement_execute(db_stmt) ;
            }
            ma_db_statement_release(db_stmt) ;
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

#define GET_ALL_SERVICES_STMT "SELECT DISTINCT * FROM MA_MSGBUS_SERVICES"
ma_error_t db_get_all_services(ma_db_t *db_handle, ma_db_recordset_t **db_record) {
	if(db_handle && db_record) {
        ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;

        if( MA_OK == (rc = ma_db_statement_create(db_handle, GET_ALL_SERVICES_STMT, &db_stmt)) ) {   
            rc = ma_db_statement_execute_query(db_stmt, db_record) ;            
            ma_db_statement_release(db_stmt) ;
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

#define GET_ALL_SUBSCRIBERS_STMT "SELECT DISTINCT * FROM MA_MSGBUS_SUBSCRIBERS"
ma_error_t db_get_all_subscribers(ma_db_t *db_handle, ma_db_recordset_t **db_record) {
	if(db_handle && db_record) {
        ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;

        if( MA_OK == (rc = ma_db_statement_create(db_handle, GET_ALL_SUBSCRIBERS_STMT, &db_stmt)) ) {   
            rc = ma_db_statement_execute_query(db_stmt, db_record) ;            
            ma_db_statement_release(db_stmt) ;
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

#define REMOVE_ALL_SERVICES_STMT "DELETE FROM MA_MSGBUS_SERVICES"
ma_error_t db_remove_all_services(ma_db_t *db_handle) {
	if(db_handle) {
        ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;

        if( MA_OK == (rc = ma_db_statement_create(db_handle, REMOVE_ALL_SERVICES_STMT, &db_stmt)) ) {   
            rc = ma_db_statement_execute(db_stmt);
            ma_db_statement_release(db_stmt) ;
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

#define REMOVE_ALL_SUBSCRIBERS_STMT "DELETE FROM MA_MSGBUS_SUBSCRIBERS"
ma_error_t db_remove_all_subscribers(ma_db_t *db_handle) {
	if(db_handle) {
        ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;

        if( MA_OK == (rc = ma_db_statement_create(db_handle, REMOVE_ALL_SUBSCRIBERS_STMT, &db_stmt)) ) {   
            rc = ma_db_statement_execute(db_stmt);
            ma_db_statement_release(db_stmt) ;
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

#define REMOVE_SUBSCRIBERS_BY_PROCESS_ID_STMT "DELETE FROM MA_MSGBUS_SUBSCRIBERS WHERE PROCESS_ID = ?"
ma_error_t db_remove_subscribers_by_process_id(ma_db_t *db_handle, const char *process_id) {
    if(db_handle && process_id) {
        ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;

        if( MA_OK == (rc = ma_db_statement_create(db_handle, REMOVE_SUBSCRIBERS_BY_PROCESS_ID_STMT, &db_stmt)) ) {   
            if(MA_OK == (rc = ma_db_statement_set_string(db_stmt, 1, 1, process_id, strlen(process_id))))
                rc = ma_db_statement_execute(db_stmt);
            ma_db_statement_release(db_stmt) ;
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

#define REMOVE_SERVICES_BY_PROCESS_ID_STMT "DELETE FROM MA_MSGBUS_SERVICES WHERE PROCESS_ID = ?"
ma_error_t db_remove_services_by_process_id(ma_db_t *db_handle, const char *process_id) {
    if(db_handle && process_id) {
        ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;

        if( MA_OK == (rc = ma_db_statement_create(db_handle, REMOVE_SERVICES_BY_PROCESS_ID_STMT, &db_stmt)) ) {   
            if(MA_OK == (rc = ma_db_statement_set_string(db_stmt, 1, 1, process_id, strlen(process_id))))
                rc = ma_db_statement_execute(db_stmt);
            ma_db_statement_release(db_stmt) ;
        }
        return rc ;
    }
	return MA_ERROR_INVALIDARG ;
}

#define GET_SUBSCRIBERS_LIST_STMT "SELECT * FROM MA_MSGBUS_SUBSCRIBERS  WHERE TOPIC_NAME LIKE ?"
ma_error_t db_get_subscribers_list(ma_db_t *db_handle, const char *topic_name, ma_db_recordset_t **db_record) {
	if(db_handle && topic_name && db_record) {
        ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;

        if(MA_OK == (rc = ma_db_statement_create(db_handle, GET_SUBSCRIBERS_LIST_STMT, &db_stmt))) {   
			if(MA_OK == (rc = ma_db_statement_set_string(db_stmt, 1, 1, topic_name, strlen(topic_name))))
				rc = ma_db_statement_execute_query(db_stmt, db_record) ;

            ma_db_statement_release(db_stmt) ;
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

#define GET_SERVICES_LIST_STMT "SELECT * FROM MA_MSGBUS_SERVICES WHERE SERVICE_NAME LIKE ?"
ma_error_t db_get_services_list(ma_db_t *db_handle, const char *service_name, ma_db_recordset_t **db_record) {
	if(db_handle && service_name && db_record) {
        ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;

        if(MA_OK == (rc = ma_db_statement_create(db_handle, GET_SERVICES_LIST_STMT, &db_stmt))) {   
			if(MA_OK == (rc = ma_db_statement_set_string(db_stmt, 1, 1, service_name, strlen(service_name))))
				rc = ma_db_statement_execute_query(db_stmt, db_record) ;

            ma_db_statement_release(db_stmt) ;
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}