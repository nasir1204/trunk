#include <windows.h>
#include <tchar.h>

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME	"macmnsvc"

#include "ma/ma_log.h"

#include "ma/logger/ma_file_logger.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/apps/macmnsvc/ma_common_services.h"
#include "ma/internal/defs/ma_common_services_defs.h"
#include "ma/internal/utils/conf/ma_conf.h"
#include "ma/internal/utils/conf/ma_conf_log.h"
#include "ma/internal/defs/ma_logger_defs.h"
#include "ma/internal/buildinfo.h"

#include <windows.h>
#define inline MA_INLINE /* Mfetrust incorrectly uses inline which is not a C valid in c89 */
#include "mfetrust.h" /* to bypass VSE AP */
#undef inline



#ifdef _MSC_VER
# include <crtdbg.h> /* */
# define MA_TRACKLEAKS _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF)
#else
# define MA_TRACKLEAKS ((void *)0)
#endif


/*
 Register this as a service:

 sc[.exe] create ma_brokersvc binPath= "\"c:\dev\ma\src\trunk\build\msvc\Debug\ma_brokersvc.exe\" /ServiceStart" start= auto
 */

typedef struct service_data_s {
  SERVICE_STATUS_HANDLE     service_status_handle;
  SERVICE_STATUS            service_status;

  ma_log_filter_t           *filter;

  ma_logger_t               *logger;

  ma_common_services_t       *common_services;
} service_data_t;

static service_data_t *psd;
static ma_bool_t is_service = MA_FALSE;

void set_service_state(service_data_t *sd, DWORD state) {
    sd->service_status.dwCurrentState = state;
    if (sd->service_status_handle) SetServiceStatus(sd->service_status_handle, &sd->service_status);
}

static int service_run(void);

static int WINAPI service_main(DWORD dwNumServicesArgs, LPTSTR *lpServiceArgVectors);

static DWORD WINAPI handler_ex(DWORD dwControl, DWORD dwEventType, LPVOID lpEventData, LPVOID lpContext);

static ma_bool_t string_array_contains(LPCTSTR *sa, LPCTSTR value) {
    for (;*sa;++sa) {
        if (0 == _tcsicmp(*sa, value)) return MA_TRUE;
    }
    return MA_FALSE;
}


static BOOL WINAPI handler_routine(DWORD ctrl_type) {
    switch( ctrl_type ) 
    { 
        // Handle the CTRL+C signal. 
    case CTRL_C_EVENT: 
    case CTRL_BREAK_EVENT:
    case CTRL_CLOSE_EVENT:
        ma_common_services_stop(psd->common_services);
        return( TRUE );
    default:{}
    }

    return FALSE;
}


#ifdef _CONSOLE
int _tmain(int argc, TCHAR *argv[]) {
#else
int WINAPI _tWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PTSTR pCmdLine, int nCmdShow) {
    int argc;
    LPWSTR *argv = CommandLineToArgvW(GetCommandLineW(), &argc);
#endif
    MA_TRACKLEAKS;
    is_service = string_array_contains(argv, _T("/ServiceStart"));

    if (is_service) {
        return service_run();
    }

#ifdef _CONSOLE
    SetConsoleCtrlHandler(&handler_routine, TRUE);


    return service_main(argc,argv);
#endif
	return 0;

}

static int service_run(void) {
    static const SERVICE_TABLE_ENTRY service_start_table[] = {{_T(""),&service_main}, {0,0}};
    if (!StartServiceCtrlDispatcher(service_start_table)) {/* This call blocks, see you in service_main() */
        return GetLastError();
    }
    return NO_ERROR;
}

static int WINAPI service_main(DWORD dwNumServicesArgs, LPTSTR *lpServiceArgVectors) {
    ma_error_t rc = MA_OK;
    service_data_t sd = {0};
    HANDLE hTrust = MfeTrustOpen();
    
    psd = &sd;
    sd.service_status.dwServiceType = SERVICE_WIN32_OWN_PROCESS;
    sd.service_status.dwControlsAccepted = SERVICE_ACCEPT_SHUTDOWN | SERVICE_ACCEPT_STOP;
    sd.service_status.dwWin32ExitCode = NO_ERROR;
    sd.service_status.dwServiceSpecificExitCode = NO_ERROR;
    sd.service_status.dwCheckPoint = 0;
    sd.service_status.dwWaitHint = 0;

    
    do {
        CHAR trust_bits[] = "xx is the trust bits obtained by macmnsvc\n";
        trust_bits[0] = MfeApTrustProcessStart(hTrust) ? 'p' : '-' ;
        trust_bits[1] = MfeApTrustThreadStart(hTrust) ? 't' : '-';
        OutputDebugStringA(trust_bits);
    } while (0);

    /* Invoke RegisterServiceCtrlHandlerEx early to give SCM a clue we are on-board */ 
    if ( !is_service || (NULL != (sd.service_status_handle = RegisterServiceCtrlHandlerEx(_T(""), &handler_ex, &sd)))) {
        ma_temp_buffer_t logs_path = MA_TEMP_BUFFER_INIT;
        set_service_state(&sd, SERVICE_START_PENDING);
        if(MA_OK == (rc = ma_conf_get_logs_path(&logs_path))) {     
            ma_temp_buffer_t log_file_name = MA_TEMP_BUFFER_INIT;
            if(MA_OK == (rc = ma_conf_get_common_services_log_file_name(&log_file_name))) {
                if(MA_OK == (rc =  ma_file_logger_create((const char *)ma_temp_buffer_get(&logs_path),(const char *)ma_temp_buffer_get(&log_file_name),".log", (ma_file_logger_t **) &sd.logger))) {    
                    ma_generic_log_filter_create("*.Info", &sd.filter);
                    ma_logger_add_filter(sd.logger, sd.filter);
                    MA_LOG(sd.logger,MA_LOG_SEV_INFO,"Starting macmnsvc v." MA_VERSION_STRING " Windows Service...");
                    if (MA_OK == (rc = ma_common_services_create(MA_COMMON_SERVICES_NAME_STR, sd.logger, &sd.common_services))) {
                        set_service_state(&sd, SERVICE_RUNNING);
                        MA_LOG(sd.logger, MA_LOG_SEV_INFO, "Running common services...");
                        
                        /* Below will block */
                        rc = ma_common_services_run(sd.common_services);

                        MA_LOG(sd.logger,MA_LOG_SEV_INFO,"common services returned from msg loop, continuing shutdown...");
                        
                        (void)ma_common_services_release(sd.common_services);
                        sd.common_services = 0;
                    } else {
                        MA_LOG(sd.logger,MA_LOG_SEV_ERROR,"Error creating common service, rc = <%d>", rc);
                    } 

                    MA_LOG(sd.logger,MA_LOG_SEV_INFO,"Stopped Service");

                    ma_logger_dec_ref(sd.logger);
                    ma_log_filter_release(sd.filter);
                }
                ma_temp_buffer_uninit(&log_file_name);    
            }
        }
        ma_temp_buffer_uninit(&logs_path);
    } else {
        /* RegisterServiceCtrlHandlerEx failed but we have nowhere to log the error */
        /* Consider logging to the Windows Event Log... */
    }

    MfeApTrustThreadEnd(hTrust);
    MfeApTrustProcessEnd(hTrust);
    MfeTrustClose(hTrust);

    /*
     * Note, below SERVICE_STOPPED should be the last call. From MSDN:
     * Do not attempt to perform any additional work after calling SetServiceStatus with SERVICE_STOPPED, because the service process can be terminated at any time    
     */
    set_service_state(&sd, SERVICE_STOPPED);
    return rc;
}


static DWORD WINAPI handler_ex(DWORD dwControl, DWORD dwEventType, LPVOID lpEventData, LPVOID lpContext) {
    service_data_t *sd = lpContext;

    switch (dwControl) {
    case SERVICE_CONTROL_INTERROGATE: return NO_ERROR;

    case SERVICE_CONTROL_SHUTDOWN: 
        /* fall through */

    case SERVICE_CONTROL_STOP: {
        MA_LOG(sd->logger,MA_LOG_SEV_INFO,"Stopping macmnsvc Service...");
        set_service_state(sd, SERVICE_STOP_PENDING);
        ma_common_services_stop(sd->common_services);
        return NO_ERROR;
        }
    }

    return ERROR_CALL_NOT_IMPLEMENTED;
}
