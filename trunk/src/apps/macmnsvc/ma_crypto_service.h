#ifndef MA_CRYPTO_SERVICE_H_INCLUDED
#define MA_CRYPTO_SERVICE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/services/ma_service.h"

MA_CPP(extern "C" {)
/* crypto service class */
typedef struct ma_crypto_service_s ma_crypto_service_t, *ma_crypto_service_h;

/* crypto service ctor */
MA_CRYPTO_API ma_error_t ma_crypto_service_create(const char *service_name, ma_service_t **service);


MA_CPP(})

//#include "ma/dispatcher/ma_crypto_service_dispatcher.h"
#endif /* MA_CRYPTO_SERVICE_H_INCLUDED */

