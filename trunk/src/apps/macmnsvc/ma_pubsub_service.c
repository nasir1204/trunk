#include "ma/internal/apps/macmnsvc/ma_pubsub_service.h"
#include "ma/internal/ma_strdef.h"

#include "ma/internal/core/msgbus/ma_message_header.h"
#include "ma/internal/core/msgbus/ma_msgbus_defs.h"
#include "ma/internal/apps/macmnsvc/ma_common_services_db.h"
#include "ma/internal/utils/configurator/ma_configurator.h"

#include "ma/internal/utils/datastructures/ma_slist.h"
#include "ma/internal/core/msgbus/ma_msgbus_private.h"

#include "ma/ma_message.h"

#include <stdlib.h>
#include <string.h>

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "pubsubsvc"

typedef struct ma_subscriber_info_s ma_subscriber_info_t;

struct ma_subscriber_info_s {
	char			*topic_name;
	char			*service_id;
	char			*product_id;
	char			*host_name;
	ma_int32_t	    pid;
	MA_SLIST_NODE_DEFINE(ma_subscriber_info_t);
} ;

struct ma_pubsub_service_s {
    ma_service_t                base_service;
    
	ma_msgbus_server_t			*pubsub_server;
	
	MA_SLIST_DEFINE(sub_info, ma_subscriber_info_t);

    ma_logger_t                 *logger;
    
    ma_db_t                     *db_msgbus;
    
    ma_msgbus_t                 *msgbus;
} ; 

static ma_error_t ma_pubsub_service_start(ma_pubsub_service_t *self);
static ma_error_t ma_pubsub_service_stop(ma_pubsub_service_t *self);
static ma_error_t ma_pubsub_service_release(ma_pubsub_service_t *self);

static ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint);

static ma_error_t service_start(ma_service_t *service) {
    return ma_pubsub_service_start((ma_pubsub_service_t *)service);
}

static ma_error_t service_stop(ma_service_t *service) {
    return ma_pubsub_service_stop((ma_pubsub_service_t *)service);
}

static void service_release(ma_service_t *service) {
    ma_pubsub_service_release((ma_pubsub_service_t *)service);
}

static const ma_service_methods_t service_methods = {
    &service_configure,
    &service_start,
    &service_stop,
    &service_release
};

ma_error_t ma_pubsub_service_create(char const *service_name, ma_service_t **pubsub_service) {
    if(service_name && pubsub_service) {
        ma_pubsub_service_t *self = (ma_pubsub_service_t *)calloc(1, sizeof(ma_pubsub_service_t));
        if(self) {
            ma_service_init(&self->base_service, &service_methods, service_name, self );
			MA_SLIST_INIT(self, sub_info);
            *pubsub_service = &self->base_service;                
			return MA_OK;
        }
        return MA_ERROR_OUTOFMEMORY;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_pubsub_service_cb(ma_msgbus_server_t *server, ma_message_t *request_payload, void *cb_data, ma_msgbus_client_request_t *c_request);
static ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint) {
    ma_pubsub_service_t *self = (ma_pubsub_service_t *)service;
    ma_error_t rc = MA_OK;
    ma_configurator_t *configurator = NULL;

    /* optional, ok if not there */
    self->logger = MA_CONTEXT_GET_LOGGER(context);

    /* required, fail if any not there */
    if (!(self->msgbus = MA_CONTEXT_GET_MSGBUS(context)) || 
        !(configurator = MA_CONTEXT_GET_CONFIGURATOR(context))) {
			MA_LOG(self->logger, MA_LOG_SEV_ERROR, "some required information missing from context - (msgbus,database) = (%p,%p)", self->msgbus, configurator);
            return MA_ERROR_INVALIDARG;
    }

    self->db_msgbus = ma_configurator_get_msgbus_database(configurator);

    if(MA_SERVICE_CONFIG_NEW == hint) {
        /* remove if any exist in DB due to macmnsvc abrupt terminaiton */
        (void)db_remove_all_subscribers(self->db_msgbus);
        if(MA_OK == (rc = ma_msgbus_server_create(self->msgbus, MA_MSGBUS_PUBSUB_SERVICE_NAME_STR, &self->pubsub_server))){
            ma_msgbus_server_setopt(self->pubsub_server, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
		    ma_msgbus_server_setopt(self->pubsub_server, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
		    if(MA_OK != (rc = ma_msgbus_server_start(self->pubsub_server, ma_pubsub_service_cb, self)))
                MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to Start Pub Sub service - rc <%d>", rc);
        }else
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "could not register name_service with msgbus, error = <%ld>", rc);
    }

    return rc;
} 



static ma_error_t add_subscriber(ma_pubsub_service_t *self, const char *topic_name, const char *service_id, const char *product_id, ma_int32_t pid, const char *host_name) {
    ma_subscriber_info_t *subscriber_node = (ma_subscriber_info_t *)calloc(1, sizeof(ma_subscriber_info_t));
    if(!subscriber_node) 
		return MA_ERROR_OUTOFMEMORY;
	subscriber_node->topic_name = strdup(topic_name);
	subscriber_node->service_id = strdup(service_id);
	subscriber_node->product_id = strdup(product_id);
	subscriber_node->host_name = strdup(host_name);
	subscriber_node->pid = pid;
    MA_SLIST_PUSH_BACK(self, sub_info, subscriber_node) ;
    return MA_OK;
}

static ma_error_t publish_subscriber_change_notification(ma_pubsub_service_t *self, const char *notification_type, const char* topic_name, const char *product_id) {
    ma_message_t *publish_msg = NULL;
    ma_error_t rc = MA_OK;

    if(MA_OK == (rc = ma_message_create(&publish_msg))) {
        (void)ma_message_set_property(publish_msg, MA_PROP_KEY_SUBSCRIBER_CHANGE_NOTIFICATION_STR, notification_type);
		(void)ma_message_set_property(publish_msg, MA_PROP_KEY_SUBSCRIBER_TOPIC_NAME_STR, topic_name);
        (void)ma_message_set_property(publish_msg, MA_PROP_KEY_SUBSCRIBER_PRODUCT_ID_STR, product_id);
		if(MA_OK == (rc = ma_msgbus_publish(self->msgbus, MA_MSGBUS_SUBSCRIBER_CHANGE_NOTIFICATION_TOPIC_STR, MSGBUS_CONSUMER_REACH_OUTBOX, publish_msg)))
            MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Succeeded to publish subscriber change notification type <%s> to subscribers", notification_type);
        (void)ma_message_release(publish_msg);
    }
    else
        MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to create the subscriber change publish message");
    return rc;
}

static ma_error_t register_subscriber(ma_pubsub_service_t *self, ma_message_t *request_payload, ma_msgbus_client_request_t *c_request) {
	ma_error_t rc = MA_OK;
	char *topic_name = NULL, *service_id = NULL, *product_id = NULL, *host_name = NULL, *pid = NULL, *skip_repsonse = NULL; 
    ma_bool_t is_reply_required = MA_TRUE;

    if(MA_OK == ma_message_get_property(request_payload, MA_PROP_KEY_SKIP_RESPONSE_STR, &skip_repsonse)) {
        is_reply_required = !(skip_repsonse && !strcmp(skip_repsonse, "1"));
    } 

	rc = ((MA_OK == (rc = ma_message_get_property(request_payload, MA_PROP_KEY_SUBSCRIBER_TOPIC_STR, &topic_name)))
		   && (MA_OK == (rc = ma_message_get_property(request_payload, MA_PROP_KEY_SERVICE_ID_STR, &service_id)))
		   && (MA_OK == (rc = ma_message_get_property(request_payload, MA_PROP_KEY_PRODUCT_ID_STR, &product_id)))
		   && (MA_OK == (rc = ma_message_get_property(request_payload, MA_PROP_KEY_HOST_NAME_STR, &host_name))) 
		   ) ? MA_OK : rc ;

	if(MA_OK == rc) {		
		if(topic_name && service_id && product_id && host_name) {		
			ma_int32_t pid = (ma_int32_t) GET_MESSAGE_SOURCE_PID(request_payload); 

			add_subscriber(self, topic_name, service_id, product_id, pid, host_name);
												
			MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Subscriber <%s> registering to broker", topic_name);

			if(MA_OK == (rc = ma_db_transaction_begin(self->db_msgbus))) {
				if(MA_OK != (rc = db_add_subscriber(self->db_msgbus, topic_name, service_id, product_id, pid, host_name)))
					ma_db_transaction_cancel(self->db_msgbus);
				ma_db_transaction_end(self->db_msgbus);
			}		

            if(is_reply_required) {
                ma_message_t *reply = NULL;
			    if (MA_OK == (rc = ma_message_create(&reply))) {
				    ma_message_set_property(reply , MA_PROP_KEY_PUBSUB_SERVICE_REPLY_TYPE_STR , MA_PROP_VALUE_REGISTER_SUBSCRIBER_REQUEST_STR);
				    ma_msgbus_server_client_request_post_result(c_request, MA_OK, reply);
				    (void)ma_message_release(reply);
			    }		
            }

			if(MA_OK == rc) 
				publish_subscriber_change_notification(self, MA_PROP_VALUE_NOTIFICATION_SUBSCRIBER_REGISTERED_STR, topic_name, product_id);			
		}
	}
	return (is_reply_required) ? rc : MA_OK;
}

static ma_error_t ma_subscriber_info_release(ma_subscriber_info_t *node) {
	if(node) {
		if(node->topic_name) free(node->topic_name);
		if(node->service_id) free(node->service_id);
		if(node->product_id) free(node->product_id);
		if(node->host_name) free(node->host_name);
		free(node); node = NULL;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

static ma_error_t unregister_subscriber(ma_pubsub_service_t *self, ma_message_t *request_payload, ma_msgbus_client_request_t *c_request) {
	ma_error_t rc = MA_OK;
	char *topic_name = NULL, *service_id = NULL, *product_id = NULL, *host_name = NULL, *pid = NULL, *skip_repsonse = NULL; 
    ma_bool_t is_reply_required = MA_TRUE;

    if(MA_OK == ma_message_get_property(request_payload, MA_PROP_KEY_SKIP_RESPONSE_STR, &skip_repsonse)) {
        is_reply_required = !(skip_repsonse && !strcmp(skip_repsonse, "1"));
    }
		
	rc = ((MA_OK == (rc = ma_message_get_property(request_payload, MA_PROP_KEY_SUBSCRIBER_TOPIC_STR, &topic_name)))
		   && (MA_OK == (rc = ma_message_get_property(request_payload, MA_PROP_KEY_SERVICE_ID_STR, &service_id)))
		   && (MA_OK == (rc = ma_message_get_property(request_payload, MA_PROP_KEY_PRODUCT_ID_STR, &product_id)))
		   && (MA_OK == (rc = ma_message_get_property(request_payload, MA_PROP_KEY_HOST_NAME_STR, &host_name))) 
		   ) ? MA_OK : rc ;

	if(MA_OK == rc) {		
		if(topic_name && service_id && product_id && host_name) {
			ma_int32_t process_id = (ma_int32_t) GET_MESSAGE_SOURCE_PID(request_payload);

			ma_subscriber_info_t *del_node = NULL;
			MA_SLIST_FOREACH(self, sub_info, del_node) {
				if(!strcmp(topic_name, del_node->topic_name) && !strcmp(product_id, del_node->product_id) && (process_id == del_node->pid)) {
					MA_SLIST_REMOVE_NODE(self, sub_info, del_node, ma_subscriber_info_t) ;
					rc = ma_subscriber_info_release(del_node);	
					break ;
				}
			}

			/* Deletes Subscriber info from database */
			if(MA_OK == rc) {

				if(MA_OK == (rc = ma_db_transaction_begin(self->db_msgbus))) {
					if(MA_OK != (rc = db_remove_subscriber(self->db_msgbus, topic_name, product_id, process_id)))
						ma_db_transaction_cancel(self->db_msgbus);
					ma_db_transaction_end(self->db_msgbus);
				}
			}

			MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Subscriber <%s> unregistered from broker", topic_name);

            if(is_reply_required) {
                ma_message_t *reply = NULL;
			    if (MA_OK == (rc = ma_message_create(&reply))) {
				    ma_message_set_property(reply , MA_PROP_KEY_PUBSUB_SERVICE_REPLY_TYPE_STR , MA_PROP_VALUE_UNREGISTER_SUBSCRIBER_REQUEST_STR);
				    ma_msgbus_server_client_request_post_result(c_request, MA_OK, reply);
				    (void)ma_message_release(reply);
			    }  
            }

			if(MA_OK == rc) 
				rc = publish_subscriber_change_notification(self, MA_PROP_VALUE_NOTIFICATION_SUBSCRIBER_UNREGISTERED_STR, topic_name, product_id);
		}
	}
	return (is_reply_required) ? rc : MA_OK;
}

static ma_error_t load_subscriber_info_from_database(ma_pubsub_service_t *self) {
	ma_error_t rc = MA_OK;

	ma_db_recordset_t *record_set = NULL;
	
	if(MA_OK != (rc = db_get_all_subscribers(self->db_msgbus, &record_set)))
		MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to get subscriber record set from database - rc <%d>", rc);
	 
	while((MA_OK == rc) && (MA_ERROR_NO_MORE_ITEMS != (rc = ma_db_recordset_next(record_set)))) {
		char *topic_name = NULL, *service_id = NULL, *product_id = NULL, *host_name = NULL;
		ma_int32_t	process_id = 0;

		if(MA_OK != rc)
			break;

		(void)ma_db_recordset_get_string(record_set, 1, &topic_name);    
        (void)ma_db_recordset_get_string(record_set, 2, &service_id);  
        (void)ma_db_recordset_get_string(record_set, 3, &product_id); 
        (void)ma_db_recordset_get_int(record_set, 4, &process_id); 
		(void)ma_db_recordset_get_string(record_set, 5, &host_name);

		if(topic_name && service_id && product_id && host_name) {
			/* MA_LOG(self->logger, MA_LOG_SEV_TRACE, "Reading record from subscriber table: topic_name <%s> service_id <%s> product_id <%s> process_id <%d>", topic_name, service_id, product_id, process_id);	*/
			rc = add_subscriber(self, topic_name, service_id, product_id, process_id, host_name);
		}
	}

	if(MA_ERROR_NO_MORE_ITEMS == rc)
		rc = MA_OK;

	if(record_set) (void)ma_db_recordset_release(record_set);

	return rc;
}


static ma_error_t delete_subscribers(ma_pubsub_service_t *self, ma_message_t *request_payload, ma_msgbus_client_request_t *c_request) {
    ma_error_t rc = MA_OK;
    const char *pid_str = NULL;
    if(MA_OK == (rc = ma_message_get_property(request_payload, MA_PROP_KEY_PEER_PID_STR, &pid_str))) {        
        if(MA_OK == (rc = db_remove_subscribers_by_process_id(self->db_msgbus, pid_str))) {
			MA_SLIST_CLEAR(self, sub_info, ma_subscriber_info_t, ma_subscriber_info_release) ;
            rc = load_subscriber_info_from_database(self);
		}
    }
    return rc;
}

static ma_error_t ma_pubsub_service_cb(ma_msgbus_server_t *server, ma_message_t *request_payload, void *cb_data, ma_msgbus_client_request_t *c_request) {
	ma_error_t rc = MA_OK;
    char *value = NULL;

    ma_pubsub_service_t *self = (ma_pubsub_service_t *) cb_data;

    MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Pub Sub Service server callback Invoked");

    /* Message Filter */
    if(MA_OK == (rc = ma_message_get_property(request_payload, MA_PROP_KEY_PUBSUB_SERVICE_REQUEST_TYPE_STR, &value))) {

        if (0 == strcmp(MA_PROP_VALUE_REGISTER_SUBSCRIBER_REQUEST_STR, value))  {
			rc = register_subscriber(self, request_payload, c_request);
		}
        else if (0 == strcmp(MA_PROP_VALUE_UNREGISTER_SUBSCRIBER_REQUEST_STR, value)) {
			rc = unregister_subscriber(self, request_payload, c_request);
		}  
        else if (0 == strcmp(MA_PROP_VALUE_DELETE_SUBSCRIBERS_REQUEST_STR, value)) {
			rc = delete_subscribers(self, request_payload, c_request);
		}  
        else {
            rc = MA_BROKER_INVALID_REQUEST;
            MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Request type <%s> is not for me", value);
        }

        if(MA_OK != rc) {
            ma_message_t *reply = NULL;
            ma_error_t reply_status = rc;
            if(MA_OK == (rc = ma_message_create(&reply))) {
		        ma_message_set_property(reply , MA_PROP_KEY_PUBSUB_SERVICE_REPLY_TYPE_STR , value);
		        ma_msgbus_server_client_request_post_result(c_request, reply_status, reply);
		        (void)ma_message_release(reply);
	        }
        }
    }    

	return rc;
}

ma_error_t ma_pubsub_service_start(ma_pubsub_service_t *self) {
    if(self) {
        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Pub Sub Service Starts");
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_pubsub_service_stop(ma_pubsub_service_t *self) {    
    if(self) {
        ma_error_t rc = MA_OK;

        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Pub Sub Service Stops");
		
		if(MA_OK != (rc = ma_msgbus_server_stop(self->pubsub_server)))
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to stop Pub Sub service - rc <%d>", rc);

        if(MA_OK != (rc = db_remove_all_subscribers(self->db_msgbus)))
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to delete all subscribers from DB - rc <%d>", rc);

        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_pubsub_service_release(ma_pubsub_service_t *self) {
    if(self) {
        ma_error_t rc = MA_OK;
		if(MA_OK != (rc = ma_msgbus_server_release(self->pubsub_server)))
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Pub Sub service release failed rc = <%d>", rc);

		MA_SLIST_CLEAR(self, sub_info, ma_subscriber_info_t, ma_subscriber_info_release) ;

        free(self);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

