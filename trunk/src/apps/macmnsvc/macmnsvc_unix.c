#include "ma/ma_common.h"

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME   "macmnsvc"

#include "ma/ma_log.h"
#include "ma/logger/ma_file_logger.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/apps/macmnsvc/ma_common_services.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/defs/ma_common_services_defs.h"
#include "ma/internal/utils/conf/ma_conf.h"
#include "ma/internal/utils/conf/ma_conf_log.h"
#include "ma/internal/defs/ma_logger_defs.h"
#include "ma/internal/defs/ma_general_defs.h"
#include "ma/internal/utils/platform/ma_process.h"


#include <stdio.h>

/* To be changed the below macros */
#define BROKERSVC_LOG_BASE_DIR      "/var/McAfee/agent/logs/"


#define START_PARAM         "start"
#define SELF_START_PARAM    "self_start"
#define STOP_PARAM          "stop"
#define STATUS_PARAM        "status"

typedef struct service_data_s {
    ma_log_filter_t           *filter;

    ma_logger_t               *logger;

    ma_common_services_t       *common_services;
} service_data_t;

static service_data_t sd;

static void usage() {
    fprintf(stderr, "usage: macmnsvc start/stop/status \n");
}

static void ma_process_stop_handler(void *data) {
    service_data_t *sd = (service_data_t *) data;
    ma_error_t rc = MA_OK;
     if(MA_OK == (rc = ma_common_services_stop(sd->common_services))) {
        MA_LOG(sd->logger, MA_LOG_SEV_DEBUG, "common services stop succeeded") ;
    }
    else {
        MA_LOG(sd->logger, MA_LOG_SEV_ERROR, "common services stop failed(%d)", rc) ;
    }
}

static ma_error_t ma_brokersvc_start() {
    ma_error_t rc = MA_OK ;
    ma_temp_buffer_t logs_path = MA_TEMP_BUFFER_INIT;
    if(MA_OK == (rc = ma_conf_get_logs_path(&logs_path))) {
        ma_temp_buffer_t log_file_name = MA_TEMP_BUFFER_INIT;
        if(MA_OK == (rc = ma_conf_get_common_services_log_file_name(&log_file_name))) {
            if(MA_OK == (rc =  ma_file_logger_create((const char *)ma_temp_buffer_get(&logs_path),(const char *)ma_temp_buffer_get(&log_file_name),".log", (ma_file_logger_t **) &sd.logger))) {
                (void)ma_generic_log_filter_create("*.Info", &sd.filter);
                (void)ma_logger_add_filter(sd.logger, sd.filter);
                if(MA_OK == (rc = ma_common_services_create(MA_COMMON_SERVICES_NAME_STR, sd.logger, &sd.common_services))) {
                    ma_event_loop_t *event_loop = NULL;

                    MA_LOG(sd.logger, MA_LOG_SEV_DEBUG, "common services create succeeded");
                    if((MA_OK == (rc = ma_common_services_get_event_loop(sd.common_services, &event_loop))) && event_loop) {
                        if(!ma_process_register_stop_handler(event_loop, ma_process_stop_handler, &sd)) {
                            if(MA_OK != (rc = ma_common_services_run(sd.common_services)))
                                MA_LOG(sd.logger, MA_LOG_SEV_ERROR, "common services run failed(%d)", rc) ;
                        }
                        else
                            MA_LOG(sd.logger, MA_LOG_SEV_ERROR, "Process stop handler registration failed") ;
                    }
                    (void) ma_common_services_release(sd.common_services);
                }
                else {
                    MA_LOG(sd.logger, MA_LOG_SEV_ERROR, "common services create Failed(%d)", rc) ;
                }
                (void) ma_file_logger_release((ma_file_logger_t *)sd.logger); sd.logger = NULL;
                ma_log_filter_release(sd.filter); sd.filter = NULL;
            }
        }
        ma_temp_buffer_uninit(&log_file_name);
    }
    ma_temp_buffer_uninit(&logs_path);
    return rc ;
}

static void ma_brokersvc_stop() {
    ma_error_t rc = MA_OK ;
    if(sd.common_services) {
        if(MA_OK == (rc = ma_common_services_stop(sd.common_services))) {
            MA_LOG(sd.logger, MA_LOG_SEV_DEBUG, "common services stop succeeded") ;
            if(MA_OK == (rc = ma_common_services_release(sd.common_services))) {
                MA_LOG(sd.logger, MA_LOG_SEV_DEBUG, "common services release succeeded") ;
            }
            else
                MA_LOG(sd.logger, MA_LOG_SEV_ERROR, "common services release failed(%d)", rc) ;
        }
        else {
            MA_LOG(sd.logger, MA_LOG_SEV_ERROR, "common services stop failed(%d)", rc) ;
        }
    }
}

int main(int argc, char **argv) {
#ifdef HPUX_ALIGNMENT_TRAP_HANDLER
	allow_unaligned_data_access();
#endif
	
    ma_int32_t status = -1;
    ma_error_t rc = MA_OK;
    if( argc != 2 ) {
        usage();
        return status;
    }

    if(MA_OK != (rc = (ma_error_t)ma_sys_chprocessown(MA_MFEAGENT_USERNAME_STR))){
       fprintf(stderr, "failed to drop macmnsvc privileges, error = <%d>\n", rc);
    }

    if(!strncmp(argv[1], START_PARAM, strlen(START_PARAM)))  {
        status = ma_process_start();
        if(0 == status)
			fprintf(stderr, "%s Process started \n", argv[0]);
        else if(1 == status)
			fprintf(stderr, "%s Process is already running \n", argv[0]);
        else
            fprintf(stderr, "Failed to start process %s \n", argv[0]);
    }
    else if(!strncmp(argv[1], SELF_START_PARAM, strlen(SELF_START_PARAM))) {
        ma_error_t rc = MA_OK;
        if(MA_OK != (rc = ma_brokersvc_start())) {
			fprintf(stderr, "macmnsvc process start failed (%d)", rc) ;
		}
        status = (MA_OK == rc) ? 0 : -1;
    }
    else if(!strncmp(argv[1], STOP_PARAM, strlen(STOP_PARAM))) {
        status = ma_process_stop();
        if(0 == status)
			fprintf(stderr, "%s Process stopped \n", argv[0]);
		else if(1 == status)
			fprintf(stderr, "%s Process is not running \n", argv[0]);
        else
            fprintf(stderr, "Failed to stop process %s \n", argv[0]);
    }
    else if(!strncmp(argv[1], STATUS_PARAM, strlen(STATUS_PARAM))) {
        status = ma_process_status();
        if(0 == status)
			fprintf(stderr, "%s Process is running \n", argv[0]);
		else if(1 == status)
			fprintf(stderr, "%s Process is not running \n", argv[0]);
        else
            fprintf(stderr, "Failed to get process %s status \n", argv[0]);
    }
    else {
        fprintf(stderr, "Invalid argument(%s)\n", argv[1]);
        usage();
    }

    return status ;
}

