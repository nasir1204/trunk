#include "ma/internal/apps/macmnsvc/ma_common_services.h"
#include "ma/internal/services/ma_service.h"
#include "ma/internal/services/ma_service_manager.h"

#include "ma/internal/services/ma_agent_policy_settings_bag.h"
#include "ma/internal/utils/macrypto/ma_crypto.h"
#include "ma/internal/ma_strdef.h"

/* Objects */
#include "ma/internal/utils/context/ma_context.h"
#include "ma/internal/utils/configurator/ma_configurator_utils.h"
#include "ma/logger/ma_file_logger.h"
#include "ma/internal/services/ma_policy_settings_bag.h"
#include "ma/policy/ma_policy_uri.h"
#include "ma/internal/utils/network/ma_net_client_service.h"
#include "ma/internal/utils/platform/ma_system_property.h"

/* clients */
#include "ma/policy/ma_policy.h"
#include "ma/internal/clients/ma/ma_client_internal.h"

/* Services */
#include "ma/internal/apps/macmnsvc/ma_name_service.h"
#include "ma/internal/apps/macmnsvc/ma_pubsub_service.h"
#include "ma/internal/apps/macmnsvc/ma_service_manager_service.h"
#include "ma/internal/services/http_server/ma_http_server.h"
#include "ma_crypto_service.h"
#include "ma/internal/utils/network/ma_network_service.h"


#include "ma/internal/services/p2p/ma_p2p_service.h"
#include "ma/internal/services/udp_server/ma_udp_server_service.h"
#include "ma/internal/services/stats/ma_stats_service.h"

#include "ma/internal/core/msgbus/ma_msgbus_defs.h"
#include "ma/internal/core/msgbus/ma_msgbus_private.h"
#include "ma/internal/utils/logger/ma_logger_internal.h"

#include "ma/ma_msgbus.h"
#include "ma/ma_message.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#ifdef MA_WINDOWS
#include <windows.h>
#include <process.h>
#endif

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "cmnsvc"

typedef struct ma_commmon_services_logger_s {
    /* logger */
    ma_logger_t                         *logger;

    ma_file_logger_policy_t             file_logger_policy;

    char                                *filter_pattern;

    ma_log_filter_t                     *logger_filter;

} ma_commmon_services_logger_t;

struct ma_common_services_s {
    ma_service_t                        base_service;/* Notice that this top level object both contains other services AND is also a service itself*/

    /* main context */
    ma_context_t                        *context;

    /* keep list of all services */
    ma_service_manager_t                service_manager;

    /* logger */
    ma_commmon_services_logger_t        logger_context;

    /* configurator */
    ma_configurator_t                   *configurator;

    /* policy settings bag */
    ma_policy_settings_bag_t            *policy_settings_bag;

	/* system property */
    ma_system_property_t                *system_property ;

    /* msgbus */
    ma_msgbus_t                         *msgbus;

    /* MA client */
    ma_client_t                         *client;  

    /* subscriber */
    ma_msgbus_subscriber_t              *broker_subscriber;

    ma_msgbus_subscriber_t              *inproc_subscriber;

    uv_prepare_t                        prepare_event;
};


static ma_error_t start_services_as_per_policies(ma_common_services_t *self);
static ma_error_t configure_common_services_logger(ma_common_services_t *self, ma_bool_t is_first_time);

static ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint);
static ma_error_t service_start(ma_service_t *service);
static ma_error_t service_stop(ma_service_t *service);

static const ma_service_methods_t service_methods = {
    &service_configure,
    &service_start,
    &service_stop,
    NULL /* don't use service manager's memory management, lifetime is controlled through other means */
};

ma_error_t ma_common_services_create(const char *service_name, ma_logger_t *logger, ma_common_services_t **broker_service) {
    if(service_name && broker_service) {
        ma_error_t rc = MA_OK;
		ma_msgbus_server_t *server = NULL;
        ma_datastore_configuration_request_t datastore_required = {{MA_TRUE,MA_DB_OPEN_READ_WITH_REPAIR}, {MA_TRUE,MA_DB_OPEN_READ_WITH_REPAIR}, {MA_FALSE,0}, {MA_FALSE,0},{MA_TRUE,MA_DB_OPEN_READWRITE} , {MA_TRUE, MA_DB_OPEN_READWRITE}, {MA_TRUE, MA_DB_OPEN_READWRITE}, {MA_TRUE, MA_DB_OPEN_READWRITE}, {MA_TRUE, MA_DB_OPEN_READWRITE}, {MA_TRUE, MA_DB_OPEN_READWRITE}, {MA_TRUE, MA_DB_OPEN_READWRITE}, {MA_TRUE, MA_DB_OPEN_READWRITE}, {MA_TRUE, MA_DB_OPEN_READWRITE}, {MA_TRUE, MA_DB_OPEN_READWRITE}, {MA_TRUE, MA_DB_OPEN_READWRITE}};

        ma_common_services_t *self = (ma_common_services_t *)calloc(1, sizeof(ma_common_services_t));
        
        if(!self)   return MA_ERROR_OUTOFMEMORY;

        ma_service_manager_init(&self->service_manager);
        ma_service_init(&self->base_service, &service_methods, service_name, self);
		
        self->logger_context.logger = logger;        
        rc =((MA_OK == (rc = ma_context_create(&self->context))) &&            
		    (MA_OK == (rc = ma_service_configurator_create(&self->configurator))) &&            
            (MA_OK == (rc = ma_configurator_intialize_datastores(self->configurator, &datastore_required))) &&
            (MA_OK == (rc = ma_agent_policy_settings_bag_create(ma_configurator_get_policies_database(self->configurator), &self->policy_settings_bag))) &&
            (MA_OK == (rc = ma_policy_settings_bag_set_logger((ma_policy_settings_bag_t *)self->policy_settings_bag, logger))) &&
            (MA_OK == (rc = configure_common_services_logger(self, MA_TRUE))) &&
            (MA_OK == (rc = ma_msgbus_create(MA_SOFTWAREID_GENERAL_STR, &self->msgbus))) &&
            (MA_OK == (rc = ma_msgbus_set_logger(self->msgbus, self->logger_context.logger))) && 
			(MA_OK == (rc = ma_msgbus_set_msgbus_db(self->msgbus, ma_configurator_get_msgbus_database(self->configurator)))) && 
            (MA_OK == (rc = ma_client_create(MA_SOFTWAREID_GENERAL_STR, &self->client))) &&
            (MA_OK == (rc = ma_client_set_msgbus(self->client, self->msgbus))) &&
			(MA_OK == (rc = ma_client_set_logger(self->client, self->logger_context.logger))) &&
            (MA_OK == (rc = ma_client_set_thread_option(self->client, MA_MSGBUS_CALLBACK_THREAD_IO))) &&
			(MA_OK == (rc = ma_system_property_create(&self->system_property, self->logger_context.logger)))
            ) ? MA_OK : rc;
	
        if(MA_OK != rc) {
            ma_common_services_release(self);
            return rc;
        }

        /* add objects into context */
        ma_context_add_object_info(self->context, MA_OBJECT_MA_CONFIGURATOR_NAME_STR, &self->configurator);
        ma_context_add_object_info(self->context, MA_OBJECT_POLICY_SETTINGS_BAG_STR, &self->policy_settings_bag);
        ma_context_add_object_info(self->context, MA_OBJECT_LOGGER_NAME_STR, &self->logger_context.logger);        
        ma_context_add_object_info(self->context, MA_OBJECT_MSGBUS_NAME_STR, &self->msgbus);        
		ma_context_add_object_info(self->context, MA_OBJECT_SYSTEM_PROPERTY_STR, &self->system_property) ;
		ma_context_add_object_info(self->context, MA_OBJECT_CLIENT_NAME_STR, &self->client) ;

        ma_service_manager_set_logger(&self->service_manager, self->logger_context.logger);
                
        /* start services as per policies and add/remove with service manager */
        if(MA_OK != (rc = start_services_as_per_policies(self))) {
            ma_common_services_release(self);
            return rc;
        }        

        /* add ourself as a service also */
        ma_service_manager_add_service(&self->service_manager, &self->base_service, service_name);           
        *broker_service = self;      
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

static void broker_service_prepare_cb(uv_prepare_t* handle, int status) {
    ma_common_services_t *self = (ma_common_services_t *)handle->data;
    if(!status) {
        ma_error_t rc = MA_OK;
        /* configure all of the services */
        if(MA_OK == (rc = ma_service_manager_configure_all(&self->service_manager, self->context, MA_SERVICE_CONFIG_NEW))) {
            (void)ma_msgbus_set_crypto(self->msgbus, MA_CONTEXT_GET_CRYPTO(self->context));
            if(MA_OK == (rc = ma_service_manager_start_all(&self->service_manager))) {
                MA_LOG(self->logger_context.logger, MA_LOG_SEV_INFO, "Broker service is running");        
            }
            else
                MA_LOG(self->logger_context.logger, MA_LOG_SEV_ERROR, "One or more services did not start!, rc = %d", rc);
        }
        else
            MA_LOG(self->logger_context.logger, MA_LOG_SEV_ERROR, "One or more services did not configure!, rc = %d", rc);
    }
    uv_prepare_stop(&self->prepare_event);
    uv_close((uv_handle_t *)&self->prepare_event, NULL);
}


ma_error_t ma_common_services_run(ma_common_services_t *self) {
    if(self) {
        if(!uv_prepare_init( ma_event_loop_get_uv_loop(ma_msgbus_get_event_loop(self->msgbus)), &self->prepare_event)) {
            self->prepare_event.data = self;
            if(!uv_prepare_start(&self->prepare_event, broker_service_prepare_cb)) {
		        /* Below will block */
                return ma_msgbus_run(self->msgbus);
            }
            uv_close((uv_handle_t *)&self->prepare_event, NULL);
        }
    }
    return MA_ERROR_INVALIDARG;
}

static void loop_stop_handler(ma_event_loop_t *loop, int reason, void *cb_data) {
    ma_common_services_t *self = (ma_common_services_t *)cb_data;

    if(self) {
        ma_error_t rc; 
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);        
        MA_LOG(logger, MA_LOG_SEV_INFO, "Broker service got STOP request, now stopping all services ...");

        /* Name service should be stopped last to accept the others un-registration */		
        rc = ma_service_manager_stop_all(&self->service_manager);

        ma_msgbus_stop(self->msgbus, MA_FALSE);
    }
}

ma_error_t ma_common_services_stop(ma_common_services_t *self) {    
    if(self) {
        ma_event_loop_t *loop = ma_msgbus_get_event_loop(self->msgbus);
        ma_event_loop_register_stop_handler(loop, &loop_stop_handler, self);
        return ma_event_loop_stop(loop);
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_common_services_release(ma_common_services_t *self) {
    if(self) {
		ma_error_t rc = MA_OK;  
        ma_service_manager_release_all(&self->service_manager);
        
        if(self->broker_subscriber) ma_msgbus_subscriber_release(self->broker_subscriber);

        if(self->inproc_subscriber) ma_msgbus_subscriber_release(self->inproc_subscriber);

        if(self->client) ma_client_release(self->client);
        
        if(self->msgbus) ma_msgbus_release(self->msgbus);
     
        if(self->policy_settings_bag) ma_policy_settings_bag_release(self->policy_settings_bag);

		if(self->system_property) ma_system_property_release(self->system_property) ;

        if(self->logger_context.logger_filter) {
            ma_logger_remove_filter(self->logger_context.logger);
            ma_log_filter_release(self->logger_context.logger_filter);
        }                
        if(self->logger_context.filter_pattern) free(self->logger_context.filter_pattern);
        if(self->logger_context.file_logger_policy.format_pattern) free(self->logger_context.file_logger_policy.format_pattern);
                
        self->logger_context.logger = NULL;

        if(self->configurator)  ma_configurator_release(self->configurator);

        if(self->context) ma_context_release(self->context);	    
        
        free(self);

        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint) {
	if(service && context) {
        ma_error_t rc = MA_OK;
		ma_common_services_t *self = (ma_common_services_t *)service ;
        if(MA_SERVICE_CONFIG_NEW == hint) {
            /* Create the subscriber */
            if(MA_OK != (rc = ma_msgbus_subscriber_create(self->msgbus, &self->broker_subscriber))) {
                MA_LOG(self->logger_context.logger, MA_LOG_SEV_ERROR, "Failed to create the broker subscriber in broker service, rc = %d.", rc);
                return rc;
            }
            
            if(MA_OK != (rc = ma_msgbus_subscriber_create(self->msgbus, &self->inproc_subscriber))) {
                ma_msgbus_subscriber_release(self->broker_subscriber);
                MA_LOG(self->logger_context.logger, MA_LOG_SEV_ERROR, "Failed to create the inproc subscriber in broker service, rc = %d.", rc);
                return rc;
            }
        }
        else {
			(void)ma_db_provision(ma_configurator_get_msgbus_database(self->configurator)) ;
            rc = configure_common_services_logger(self, MA_FALSE);
		}
		return rc;    
	}
	return MA_ERROR_INVALIDARG ;
}


static ma_error_t policy_notification_cb(ma_client_t *client, const char *product_id, const ma_policy_uri_t *info_uri, void *cb_data) {
    if(client && product_id && info_uri && cb_data) {
        ma_common_services_t *self = (ma_common_services_t *)cb_data;
        ma_error_t rc = MA_OK;

        /*TODO - we need to see policies to see actually for any services enablement/disablement, objects reconfiguration(crypto) */
        /*TODO -service manager needs to be somehow abstracted out, loading of service dll , start stop etc */
        if(MA_OK == (rc = ma_agent_policy_settings_bag_refresh(self->policy_settings_bag)))
            rc = ma_service_manager_configure_all(&self->service_manager, self->context, MA_SERVICE_CONFIG_MODIFIED);

        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_common_services_subscriber_handler(const char *topic, ma_message_t *msg, void *cb_data) {
    ma_error_t rc = MA_OK;
    ma_common_services_t *self = (ma_common_services_t *)cb_data;
    if(topic && msg && self) {
        if(!strcmp(topic, MA_SERVER_SETTINGS_CHANGE_PUB_TOPIC_STR)) {
            if(MA_OK == (rc = ma_agent_policy_settings_bag_refresh(self->policy_settings_bag)))
                rc = ma_service_manager_configure_all(&self->service_manager, self->context, MA_SERVICE_CONFIG_MODIFIED);
        }
    }
    return rc;

}

static ma_error_t ma_common_services_inproc_subscriber_handler(const char *topic, ma_message_t *msg, void *cb_data) {
    ma_error_t rc = MA_OK;
    ma_common_services_t *self = (ma_common_services_t *)cb_data;
    if(topic && msg && self) {
        if(!strcmp(topic, MA_MSGBUS_PEER_END_TERMINATION_PUB_SUB_TOPIC)) {
            ma_msgbus_endpoint_t *ns_ep = NULL, *psb_ep = NULL;
            if(MA_OK == (rc = ma_msgbus_endpoint_create(self->msgbus, MA_MSGBUS_NAME_SERVICE_NAME_STR, MA_MSGBUS_NAME_SERVICE_HOST_NAME_STR, &ns_ep))) {
                ma_message_t *ns_msg = NULL;
                if(MA_OK == (rc = ma_message_clone(msg, &ns_msg))) {
                    (void) ma_message_set_property(ns_msg, MA_PROP_KEY_NAME_SERVICE_REQUEST_TYPE_STR, MA_PROP_VALUE_DELETE_SERVICES_REQUEST_STR);
                    rc = ma_msgbus_send_and_forget(ns_ep, ns_msg);
                    ma_message_release(ns_msg);
                }
                (void) ma_msgbus_endpoint_release(ns_ep); ns_ep = NULL;
            }

            if(MA_OK == (rc = ma_msgbus_endpoint_create(self->msgbus, MA_MSGBUS_PUBSUB_SERVICE_NAME_STR, MA_MSGBUS_PUBSUB_SERVICE_HOST_NAME_STR, &psb_ep))) {
                ma_message_t *psb_msg = NULL;
                if(MA_OK == (rc = ma_message_clone(msg, &psb_msg))) {
                    (void) ma_message_set_property(psb_msg, MA_PROP_KEY_PUBSUB_SERVICE_REQUEST_TYPE_STR, MA_PROP_VALUE_DELETE_SUBSCRIBERS_REQUEST_STR);
                    rc = ma_msgbus_send_and_forget(psb_ep, psb_msg);
                    ma_message_release(psb_msg);
                }
                (void) ma_msgbus_endpoint_release(psb_ep); psb_ep = NULL;
            }
        }
    }
    return rc;

}

static ma_error_t service_start(ma_service_t *service) {
    ma_common_services_t *self = (ma_common_services_t *)service;
    ma_error_t rc;

    /* start client */
    if(MA_OK != (rc = ma_client_start(self->client))) {
        MA_LOG(self->logger_context.logger, MA_LOG_SEV_ERROR, "Failed to start client manager, %d.", rc);
        return rc;
    }
  
    if(MA_OK != (rc = ma_policy_register_notification_callback(self->client, MA_SOFTWAREID_GENERAL_STR, MA_POLICY_CHANGED|MA_POLICY_TIMEOUT, policy_notification_cb, self))) {
        MA_LOG(self->logger_context.logger, MA_LOG_SEV_ERROR, "Failed to register for policy notifications, %d.", rc);
        ma_client_stop(self->client);
        return rc;
    }   
 
    (void) ma_msgbus_subscriber_setopt(self->broker_subscriber, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
    (void) ma_msgbus_subscriber_setopt(self->broker_subscriber, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
    if(MA_OK != (rc = ma_msgbus_subscriber_register(self->broker_subscriber, MA_GENERAL_SUBSCRIBER_TOPIC_STR, ma_common_services_subscriber_handler, self))) {
        ma_client_stop(self->client);      
        MA_LOG(self->logger_context.logger, MA_LOG_SEV_ERROR, "Failed to register with subscriber for %s  , rc = %d.", MA_GENERAL_SUBSCRIBER_TOPIC_STR, rc);
        return rc;
    }

    (void) ma_msgbus_subscriber_setopt(self->inproc_subscriber, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);    
    if(MA_OK != (rc = ma_msgbus_subscriber_register(self->inproc_subscriber, MA_MSGBUS_PEER_END_TERMINATION_PUB_SUB_TOPIC, ma_common_services_inproc_subscriber_handler, self))) {
        ma_msgbus_subscriber_unregister(self->broker_subscriber);
        ma_client_stop(self->client);      
        MA_LOG(self->logger_context.logger, MA_LOG_SEV_ERROR, "Failed to register with subscriber for %s  , rc = %d.", MA_MSGBUS_PEER_END_TERMINATION_PUB_SUB_TOPIC, rc);
        return rc;
    }

    MA_LOG(self->logger_context.logger, MA_LOG_SEV_DEBUG, "%s service started.", MA_COMMON_SERVICES_NAME_STR);

    return rc;
}

static ma_error_t service_stop(ma_service_t *service) {
    ma_common_services_t *self = (ma_common_services_t *)service;
    ma_error_t rc = MA_OK;

    (void) ma_msgbus_subscriber_unregister(self->broker_subscriber);

    (void) ma_msgbus_subscriber_unregister(self->inproc_subscriber);

    (void)ma_policy_unregister_notification_callback(self->client, MA_SOFTWAREID_GENERAL_STR);

    (void) ma_client_stop(self->client);  

    MA_LOG(self->logger_context.logger, MA_LOG_SEV_DEBUG, "%s service stoped.", MA_COMMON_SERVICES_NAME_STR);
    return rc;
}

ma_error_t start_services_as_per_policies(ma_common_services_t *self) {
    ma_error_t rc = MA_OK;
    unsigned short agent_mode = ma_configurator_get_agent_mode(self->configurator);
    ma_int32_t is_http_server_on = agent_mode ? 1 :0, is_crypto_on = 1, is_network_on = 1, is_p2p_on = agent_mode ?1 :0, is_udp_server_on = agent_mode ?1 :0, is_stats_on = agent_mode ?1:0 ;
    ma_service_t *service = NULL;
    
    ma_policy_settings_bag_t *policy_bag = MA_CONTEXT_GET_POLICY_SETTINGS_BAG(self->context);

    /* try get configuration, or stay with default values */
    (void) ma_policy_settings_bag_get_int(policy_bag, MA_HTTP_SERVER_SERVICE_SECTION_NAME_STR, MA_HTTP_SERVER_KEY_IS_ENABLED_INT, MA_FALSE,&is_http_server_on, is_http_server_on);
    (void) ma_policy_settings_bag_get_int(policy_bag, MA_CRYPTO_SERVICE_SECTION_NAME_STR, MA_CRYPTO_KEY_IS_ENABLED_INT, MA_FALSE, &is_crypto_on, is_crypto_on);
    (void) ma_policy_settings_bag_get_int(policy_bag, MA_P2P_SERVICE_SECTION_NAME_STR, MA_P2P_KEY_IS_ENABLED_INT, MA_FALSE,&is_p2p_on, is_p2p_on) ;
    (void) ma_policy_settings_bag_get_int(policy_bag, MA_UDP_SERVICE_SECTION_NAME_STR, MA_UDP_KEY_IS_ENABLED_INT, MA_FALSE,&is_udp_server_on, is_udp_server_on) ;
	(void) ma_policy_settings_bag_get_int(policy_bag, MA_STATS_SERVICE_SECTION_NAME_STR, MA_STATS_KEY_IS_ENABLED_INT, MA_FALSE,&is_stats_on, is_stats_on) ;


    MA_LOG(self->logger_context.logger, MA_LOG_SEV_INFO, "is_crypto_on (%d)", is_crypto_on);
    MA_LOG(self->logger_context.logger, MA_LOG_SEV_INFO, "is_network_on (%d)", is_network_on);
    MA_LOG(self->logger_context.logger, MA_LOG_SEV_INFO, "is_http_server_on (%d)", is_http_server_on);
    MA_LOG(self->logger_context.logger, MA_LOG_SEV_INFO, "is_p2p_on (%d)", is_p2p_on) ;
    MA_LOG(self->logger_context.logger, MA_LOG_SEV_INFO, "is_udp_server_on (%d)", is_udp_server_on) ;
	MA_LOG(self->logger_context.logger, MA_LOG_SEV_INFO, "is_stats_on (%d)", is_stats_on) ;


    do { 
        ma_service_manager_service_entry_t services[] = {
            {0, MA_MSGBUS_NAME_SERVICE_NAME_STR, &ma_name_service_create},
            {0, MA_MSGBUS_PUBSUB_SERVICE_NAME_STR, &ma_pubsub_service_create},
            {0, MA_MSGBUS_SERVICE_MANAGER_SERVICE_NAME_STR, &ma_services_manager_service_create},
            {MA_SERVICE_ENABLE_IF(is_crypto_on), MA_CRYPTO_SERVICE_NAME_STR, &ma_crypto_service_create},
            {MA_SERVICE_ENABLE_IF(is_network_on), MA_NETWORK_SERVICE_NAME_STR, &ma_network_service_create},
            {MA_SERVICE_ENABLE_IF(is_http_server_on) | MA_SERVICE_OPTION_OPTIONAL, MA_HTTP_SERVER_SERVICE_NAME_STR, &ma_http_server_service_create},
            {MA_SERVICE_ENABLE_IF(is_udp_server_on) | MA_SERVICE_OPTION_OPTIONAL, MA_UDP_SERVICE_NAME_STR, &ma_udp_server_service_create},

            {MA_SERVICE_ENABLE_IF(is_p2p_on) | MA_SERVICE_OPTION_OPTIONAL, MA_P2P_SERVICE_NAME_STR, &ma_p2p_service_create},
			{MA_SERVICE_ENABLE_IF(is_stats_on) | MA_SERVICE_OPTION_OPTIONAL, MA_STATS_SERVICE_NAME_STR, &ma_stats_service_create},
    #ifdef MA_WINDOWS
            {MA_SERVICE_OPTION_OPTIONAL|MA_SERVICE_OPTION_DYNLOAD, "aac_service", NULL, "ma_aac_service.dll", "ma_aac_service_create"},
    #endif
            {0 ,0 ,0},
        };

        return ma_service_manager_create_services(&self->service_manager, services);
    } while (0);
}


ma_error_t  ma_common_services_get_event_loop(ma_common_services_t *self, struct ma_event_loop_s **event_loop) {
    if(self && event_loop) {
        *event_loop = ma_msgbus_get_event_loop(self->msgbus);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static void on_log_msg_cb(const ma_log_msg_t *log_msg, void *cb_data) {
    ma_common_services_t *self = (ma_common_services_t *)cb_data;
    if(log_msg && self) {
        if(log_msg->is_localized) {
            ma_error_t rc = MA_OK;
            ma_message_t *msg = NULL;
            ma_variant_t *payload = NULL;            
            const char *severity = ma_log_get_severity_label(log_msg->severity);
            char time_str[MA_MAX_LEN] = {0};

            MA_MSC_SELECT(_snprintf, snprintf)(time_str, MA_MAX_LEN, "%.4d-%.2d-%.2dT%.2d:%.2d:%.2d", log_msg->time.year, log_msg->time.month, log_msg->time.day, log_msg->time.hour, log_msg->time.minute, log_msg->time.second);

            if(MA_OK == (rc = ma_message_create(&msg))) {                
                ma_table_t *log_msg_tb = NULL;
                if(MA_OK == (rc = ma_table_create(&log_msg_tb))) {
                    ma_variant_t *var = NULL;
                    if(MA_OK == (rc = ma_variant_create_from_string(time_str, strlen(time_str), &var))) {
                        rc = ma_table_add_entry(log_msg_tb, MA_LOG_MSG_KEY_TIME_STR, var);
                        (void) ma_variant_release(var); var = NULL;
                    }

                    if(severity && (MA_OK == (rc = ma_variant_create_from_string(severity, strlen(severity), &var)))) {
                        rc = ma_table_add_entry(log_msg_tb, MA_LOG_MSG_KEY_SEVERITY_STR, var);
                        (void) ma_variant_release(var); var = NULL;
                    }

                    if(log_msg->facility && (MA_OK == (rc = ma_variant_create_from_string(log_msg->facility, strlen(log_msg->facility), &var)))) {
                        rc = ma_table_add_entry(log_msg_tb, MA_LOG_MSG_KEY_FACILITY_STR, var);
                        (void) ma_variant_release(var); var = NULL;
                    }

                    /* TBD - Will handle wide chars - rc = log_msg->msg_is_wide_char ? 
                        ma_variant_create_from_wstring(log_msg->message.wchar_message, wcslen(log_msg->message.wchar_message), &payload)  :
                        ma_variant_create_from_string(log_msg->message.utf8_message, strlen(log_msg->message.utf8_message), &payload);
                    */

                    if(log_msg->message.utf8_message && (MA_OK == (rc = ma_variant_create_from_string(log_msg->message.utf8_message, strlen(log_msg->message.utf8_message), &var)))) {
                        rc = ma_table_add_entry(log_msg_tb, MA_LOG_MSG_KEY_MESSAGE_STR, var);
                        (void) ma_variant_release(var); var = NULL;
                    }

                    if(MA_OK == (rc = ma_variant_create_from_table(log_msg_tb, &var))) {
                        if(MA_OK == (rc = ma_message_set_payload(msg, var))) {
                            ma_msgbus_endpoint_t * ep = NULL;
                            ma_message_set_property(msg, MA_PROP_KEY_LOGGER_REQUEST_TYPE_STR, MA_PROP_VALUE_LOG_LOCALIZE_MESSAGE_STR);
                            if(MA_OK == (rc = ma_msgbus_endpoint_create(self->msgbus, MA_LOGGER_SERVICE_NAME_STR, MA_LOGGER_SERVICE_HOSTNAME_STR, &ep))) {
                                ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
                                ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                                rc = ma_msgbus_send_and_forget(ep, msg);
                                (void)ma_msgbus_endpoint_release(ep);
                            }
                        }                           
                        (void) ma_variant_release(var);
                    }
                    (void) ma_table_release(log_msg_tb);                    
                }
                (void)ma_message_release(msg);
            }           
        }
    }
}

static ma_error_t configure_common_services_logger(ma_common_services_t *self, ma_bool_t is_first_time) {
    if(self) {
        ma_error_t rc = MA_OK;                
        char const *p_filter_pattern = "%d %+t %p(%P.%T) %f.%s: %m", *p_format_pattern = "*.Info";
        ma_int32_t log_size_limit = 0, max_rollovers = 0;
        ma_bool_t rollover_enable = MA_FALSE;
		ma_bool_t app_logging_enable = MA_FALSE;

        if(is_first_time) {
            rc = ma_logger_register_visitor(self->logger_context.logger, on_log_msg_cb, self);
        }

        /* Enforce the policies if changed */
        rc = ( (MA_OK == (rc = ma_policy_settings_bag_get_str(self->policy_settings_bag, MA_LOGGER_SERVICE_SECTION_NAME_STR, MA_LOGGER_KEY_FILTER_PATTERN_STR, MA_FALSE, &p_filter_pattern, p_filter_pattern)))
            && (MA_OK == (rc = ma_policy_settings_bag_get_str(self->policy_settings_bag, MA_LOGGER_SERVICE_SECTION_NAME_STR, MA_LOGGER_KEY_FORMAT_PATTERN_STR, MA_FALSE, &p_format_pattern,p_format_pattern )))
            && (MA_OK == (rc = ma_policy_settings_bag_get_int(self->policy_settings_bag, MA_LOGGER_SERVICE_SECTION_NAME_STR, MA_LOGGER_KEY_LOG_SIZE_LIMIT_INT, MA_FALSE, &log_size_limit, 2)))
			&& (MA_OK == (rc = ma_policy_settings_bag_get_bool(self->policy_settings_bag, MA_LOGGER_SERVICE_SECTION_NAME_STR, MA_LOGGER_KEY_LOG_APP_LOGGING_ENABLED_INT, MA_FALSE, &app_logging_enable, MA_TRUE)))
            && (MA_OK == (rc = ma_policy_settings_bag_get_bool(self->policy_settings_bag, MA_LOGGER_SERVICE_SECTION_NAME_STR, MA_LOGGER_KEY_LOG_ENABLE_ROLLOVER_INT,MA_FALSE,&rollover_enable, MA_TRUE)))
            && (MA_OK == (rc = ma_policy_settings_bag_get_int(self->policy_settings_bag, MA_LOGGER_SERVICE_SECTION_NAME_STR, MA_LOGGER_KEY_LOG_MAX_ROLLOVER_INT, MA_FALSE,&max_rollovers, 1))))            
            ? MA_OK : rc;

        if(MA_OK == rc) {
            ma_bool_t is_policy_changed = MA_FALSE;           

            if(!self->logger_context.filter_pattern || (p_filter_pattern && strcmp(p_filter_pattern, self->logger_context.filter_pattern))) {
                if(self->logger_context.filter_pattern) free(self->logger_context.filter_pattern);
                ma_logger_remove_filter(self->logger_context.logger);
                if(self->logger_context.logger_filter) ma_log_filter_release(self->logger_context.logger_filter);
                if(MA_OK == (rc = ma_generic_log_filter_create(p_filter_pattern, &self->logger_context.logger_filter))) {
                    self->logger_context.filter_pattern = strdup(p_filter_pattern);
					ma_logger_add_filter(self->logger_context.logger, self->logger_context.logger_filter);
                }
            }

            if((MA_OK == rc ) && (!self->logger_context.file_logger_policy.format_pattern || (p_format_pattern && !strcmp(p_format_pattern, self->logger_context.file_logger_policy.format_pattern)))) {
                if(self->logger_context.file_logger_policy.format_pattern) free(self->logger_context.file_logger_policy.format_pattern);
                self->logger_context.file_logger_policy.format_pattern = strdup(p_format_pattern);
                is_policy_changed = MA_TRUE;
            }

			if((MA_OK == rc) && (self->logger_context.file_logger_policy.is_logging_enabled != app_logging_enable)) {                
                self->logger_context.file_logger_policy.is_logging_enabled = app_logging_enable;
                is_policy_changed = MA_TRUE;
            }

            if((MA_OK == rc) && (self->logger_context.file_logger_policy.is_rollover_enabled != rollover_enable)) {                
                self->logger_context.file_logger_policy.is_rollover_enabled = rollover_enable;
                is_policy_changed = MA_TRUE;
            }

            if((MA_OK == rc) && (self->logger_context.file_logger_policy.log_size_limit != log_size_limit)) {
                self->logger_context.file_logger_policy.log_size_limit = log_size_limit;
                is_policy_changed = MA_TRUE;
            }

            if((MA_OK == rc) && (self->logger_context.file_logger_policy.rollover_policy.max_number_of_rollovers != max_rollovers)) {
                self->logger_context.file_logger_policy.rollover_policy.max_number_of_rollovers = max_rollovers;
                is_policy_changed = MA_TRUE;
            }
                                
            if(MA_TRUE == is_policy_changed) {
                rc = ma_file_logger_set_policy((ma_file_logger_t *)self->logger_context.logger, &self->logger_context.file_logger_policy);
            }
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}
