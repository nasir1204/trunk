#include "ma/internal/apps/macmnsvc/ma_name_service.h"
#include "ma/internal/ma_strdef.h"


#include "ma/internal/core/msgbus/ma_msgbus_defs.h"
#include "ma/internal/core/msgbus/ma_message_header.h"
#include "ma/internal/apps/macmnsvc/ma_common_services_db.h"
#include "ma/internal/utils/configurator/ma_configurator.h"

#include "ma/internal/core/msgbus/ma_msgbus_private.h"
#include "ma/datastore/ma_ds_ini.h"

#include "ma/ma_log.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_datastore.h"
#include "ma/ma_message.h"
#include "ma/internal/ma_macros.h"

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "nameservice"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#ifdef MA_WINDOWS
#include <process.h>
#else
#include <unistd.h>
#endif

typedef struct ma_service_info_s {
	char			service_name[MA_MAX_NAME];
	char			service_id[MA_MAX_NAME];
	char			product_id[MA_MAX_NAME];
	char			host_name[MA_MAX_NAME];
	ma_int32_t	    pid;
} ma_service_info_t;

struct ma_name_service_s {
    /* base class */
    ma_service_t             base_service;

    /* for msgbus registration */
    ma_msgbus_server_t			*name_server;

	ma_table_t					*server_info_table;

    ma_logger_t                 *logger;

    ma_msgbus_t                 *msgbus;

    ma_db_t                     *db_msgbus;

	ma_bool_t					is_broker_info_deleted;
} ; 

static ma_error_t ma_name_service_start(ma_name_service_t *self);
static ma_error_t ma_name_service_stop(ma_name_service_t *self);
static ma_error_t ma_name_service_release(ma_name_service_t *self);

static ma_error_t service_start(ma_service_t *service) {
    return ma_name_service_start((ma_name_service_t *)service);
}

static ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint);


static ma_error_t service_stop(ma_service_t *service) {
    return ma_name_service_stop((ma_name_service_t *)service);
}

static void service_release(ma_service_t *service) {
    ma_name_service_release((ma_name_service_t *)service);
}

static const ma_service_methods_t service_methods = {
    &service_configure,
    &service_start,
    &service_stop,
    &service_release
};	

ma_error_t ma_name_service_create(char const *service_name, ma_service_t **name_service) {
    ma_error_t rc = MA_OK;
	if ( service_name && name_service)
	{
		ma_name_service_t *self = (ma_name_service_t *)calloc(1, sizeof(ma_name_service_t));
		if(self) {
			ma_service_init(&self->base_service, &service_methods, service_name, self);
			if(MA_OK == (rc = ma_table_create(&self->server_info_table))) {
				*name_service = &self->base_service;
				return MA_OK;
			}
			free(self);
			return rc;
		}
		return MA_ERROR_OUTOFMEMORY;
	}
	return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_name_service_cb(ma_msgbus_server_t *server, ma_message_t *request_payload, void *cb_data, ma_msgbus_client_request_t *c_request);
static ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint) {
    ma_name_service_t *self = (ma_name_service_t *)service;
    ma_error_t rc = MA_OK;
    ma_configurator_t *configurator = NULL;
    /* optional, ok if not there */
    self->logger = MA_CONTEXT_GET_LOGGER(context);
    
    /* required, fail if any not there */
	if (!(self->msgbus = MA_CONTEXT_GET_MSGBUS(context)) || 
		!(configurator = MA_CONTEXT_GET_CONFIGURATOR(context))) {
			MA_LOG(self->logger, MA_LOG_SEV_ERROR, "some required information missing from context - (configurator,database) = (%p,%p)", self->msgbus, configurator);
            return MA_ERROR_INVALIDARG;
    }

    self->db_msgbus = ma_configurator_get_msgbus_database(configurator);
    
	if(MA_SERVICE_CONFIG_NEW == hint) {
        /* remove if any exist in DB due to macmnsvc abrupt terminaiton */
        (void)db_remove_all_services(self->db_msgbus);
        
		if(MA_OK == (rc = ma_msgbus_server_create(self->msgbus, MA_MSGBUS_NAME_SERVICE_NAME_STR, &self->name_server))) {
            ma_msgbus_server_setopt(self->name_server, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
		    ma_msgbus_server_setopt(self->name_server, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
            if(MA_OK != (rc = ma_msgbus_server_start(self->name_server, ma_name_service_cb, self)))
			    MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to Start Name service - rc <%d>", rc);		    
		} else
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "could not register name_service with msgbus, error = <%d>", rc);
	}

    return rc;
}

static ma_error_t write_broker_info_into_ds(ma_name_service_t *self, ma_service_info_t *service_info) {
	ma_error_t rc = MA_ERROR_MSGBUS_SET_BROKER_INFO_FAILED;
    ma_ds_t    *ini_ds = NULL;
	const char *config_file = NULL;
	if(config_file = ma_msgbus_get_conf_file(self->msgbus)) {
		/* Truncate config.ini before writing broker info as it might be corrupted already */ 
		FILE *fp = fopen(config_file,"w");
		if (fp != NULL) fclose(fp);

		MA_LOG(self->logger, MA_LOG_SEV_INFO, "Writing broker info in file <%s>", config_file);
		if(MA_OK == (rc = ma_ds_ini_open(config_file, 1, (ma_ds_ini_t **)&ini_ds))) {			
			(MA_OK == (rc = ma_ds_set_int(ini_ds, MA_MSGBUS_PATH_NAME_STR, MA_MSGBUS_KEY_BROKER_PID_INT, service_info->pid))) &&
			(MA_OK == (rc = ma_ds_set_str(ini_ds, MA_MSGBUS_PATH_NAME_STR, MA_MSGBUS_KEY_BROKER_ID_STR, service_info->service_id, -1)));			
			ma_ds_release(ini_ds);
		}
		if(MA_OK != rc)
				MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to set broker information in config.ini, rc <%d>", rc);
	}
	else
		MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to get msgbus config file");

    return rc ; 
}

 static ma_error_t delete_broker_info_from_ds(ma_name_service_t *self) {
	 ma_error_t rc = MA_ERROR_MSGBUS_SET_BROKER_INFO_FAILED;
	 ma_ds_t    *ini_ds = NULL;
	 const char *config_file = NULL;
	 if(config_file = ma_msgbus_get_conf_file(self->msgbus)) {
		 if(MA_OK == ma_ds_ini_open(config_file, 0, (ma_ds_ini_t **)&ini_ds)) {
			 (void)ma_ds_rem(ini_ds, MA_MSGBUS_PATH_NAME_STR, MA_MSGBUS_KEY_BROKER_PID_INT, 0);
			 (void)ma_ds_rem(ini_ds, MA_MSGBUS_PATH_NAME_STR, MA_MSGBUS_KEY_BROKER_ID_STR, 0);
			 (void)ma_ds_release(ini_ds);
			 self->is_broker_info_deleted = MA_TRUE;
		 }
	 }
	 else
		MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to get msgbus config file");
	 
	 return rc ;
 }


static ma_error_t publish_service_change_notification(ma_name_service_t *self, const char *notification_type, const char* service_name) {
    ma_message_t *publish_msg = NULL;
    ma_error_t rc = MA_OK;

    if(MA_OK == (rc = ma_message_create(&publish_msg))) {
        (void)ma_message_set_property(publish_msg, MA_PROP_KEY_SERVICE_CHANGE_NOTIFICATION_STR, notification_type);
		(void)ma_message_set_property(publish_msg, MA_PROP_KEY_SERVICE_NAME_STR, service_name);
		if(MA_OK == (rc = ma_msgbus_publish(self->msgbus, MA_MSGBUS_SERVICE_CHANGE_NOTIFICATION_TOPIC_STR, MSGBUS_CONSUMER_REACH_OUTBOX, publish_msg)))
            MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Ok, published service change notification type <%s> to subscribers", notification_type);
        (void)ma_message_release(publish_msg);
    }
    if (MA_OK != rc)
        MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to create the service change publish message, err=<%ld>", rc);
    return rc;
}

static ma_error_t register_service(ma_name_service_t *self, ma_message_t *request_payload, ma_msgbus_client_request_t *c_request) {
	ma_error_t rc = MA_OK;
	char *service_name = NULL, *service_id = NULL, *host_name = NULL, *pid = NULL, *product_id = NULL, *skip_repsonse = NULL; 
    ma_bool_t is_reply_required = MA_TRUE;

    if(MA_OK == ma_message_get_property(request_payload, MA_PROP_KEY_SKIP_RESPONSE_STR, &skip_repsonse)) {
        is_reply_required = !(skip_repsonse && !strcmp(skip_repsonse, "1"));
    }

    rc   =  ((MA_OK == (rc = ma_message_get_property(request_payload, MA_PROP_KEY_SERVICE_NAME_STR, &service_name))) &&
            (MA_OK == (rc = ma_message_get_property(request_payload, MA_PROP_KEY_SERVICE_ID_STR, &service_id))) &&
            (MA_OK == (rc = ma_message_get_property(request_payload, MA_PROP_KEY_PRODUCT_ID_STR, &product_id))) &&
            (MA_OK == (rc = ma_message_get_property(request_payload, MA_PROP_KEY_HOST_NAME_STR, &host_name)))
            ) ? MA_OK : rc;

	if(MA_OK == rc) {		
		if(service_name && service_id && product_id && host_name) {
			ma_service_info_t service_info = { 0 } ;
			ma_variant_t *ser_info_var = NULL;

			strncpy(service_info.service_name, service_name, MA_MAX_LEN-1);
			strncpy(service_info.service_id, service_id, MA_MAX_LEN-1);
			strncpy(service_info.product_id, product_id, MA_MAX_LEN-1);
			strncpy(service_info.host_name, host_name, MA_MAX_LEN-1);
			service_info.pid = (ma_int32_t) GET_MESSAGE_SOURCE_PID(request_payload);      
			
            
            /* name, pubsub and service manager services can only be registered from broker process, reject if any one registered from outside broker process */
            if(!strcmp(service_info.service_name, MA_MSGBUS_NAME_SERVICE_NAME_STR) ||
               !strcmp(service_info.service_name, MA_MSGBUS_PUBSUB_SERVICE_NAME_STR) ||
               !strcmp(service_info.service_name, MA_MSGBUS_SERVICE_MANAGER_SERVICE_NAME_STR)
              ) {
                  if(service_info.pid != getpid()) {
                      MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Rejecting service <%s> registration from out side of broker", service_info.service_name);
                      return MA_BROKER_SERVICE_REGISTRATION_REJECTED;
                  }
            }
            

			MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Service <%s> registering to broker", service_info.service_name);

			if(MA_OK == (rc = ma_variant_create_from_raw(&service_info, sizeof(ma_service_info_t), &ser_info_var))) {
				if(MA_OK == (rc = ma_table_add_entry(self->server_info_table, service_info.service_name, ser_info_var))) {

					/* Write broker info into ini file when Name service registers to itself */
					if(!strcmp(service_info.service_name, MA_MSGBUS_NAME_SERVICE_NAME_STR))
						rc = write_broker_info_into_ds(self, &service_info);

					if(MA_OK == rc) {
						if(MA_OK == (rc = ma_db_transaction_begin(self->db_msgbus))) {
							if(MA_OK != (rc = db_add_service(self->db_msgbus, service_info.service_name, service_info.service_id, service_info.product_id, service_info.pid, service_info.host_name)))
								ma_db_transaction_cancel(self->db_msgbus);
							ma_db_transaction_end(self->db_msgbus);
						}
					}

                    if(is_reply_required) {
                        ma_message_t *reply = NULL;
					    if (MA_OK == (rc = ma_message_create(&reply))) {
						    ma_message_set_property(reply , MA_PROP_KEY_NAME_SERVICE_REPLY_TYPE_STR , MA_PROP_VALUE_REGISTER_SERVICE_REQUEST_STR);
						    ma_msgbus_server_client_request_post_result(c_request, MA_OK, reply);
						    (void)ma_message_release(reply);
					    }
                    }

                    /* Commenting the publishing of service registration and unregistration code as it is not required now.
                       We will exclude the broker services(name and pub_sub services) when we enable this code.
                    */
					/*if(MA_OK == rc) 
						rc = publish_service_change_notification(self, MA_PROP_VALUE_NOTIFICATION_SERVICE_REGISTERED_STR, service_info.service_name);
					else
						MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to publish Service <%s> registeration message", service_info.service_name);*/
				}
				(void)ma_variant_release(ser_info_var);
			}			
		}
	}

	return (is_reply_required) ? rc : MA_OK;
}

static ma_error_t unregister_service(ma_name_service_t *self, ma_message_t *request_payload, ma_msgbus_client_request_t *c_request) {
	ma_error_t rc = MA_OK;
	char *service_name = NULL, *skip_repsonse = NULL; 
    ma_bool_t is_reply_required = MA_TRUE;

    if(MA_OK == ma_message_get_property(request_payload, MA_PROP_KEY_SKIP_RESPONSE_STR, &skip_repsonse)) {
        is_reply_required = !(skip_repsonse && !strcmp(skip_repsonse, "1"));
    }
		
	if((MA_OK == (rc = ma_message_get_property(request_payload, MA_PROP_KEY_SERVICE_NAME_STR, &service_name))) && service_name) {

		/* Deletes broker info from DS when Name Service unregisters to itself */
		if(!strcmp(service_name, MA_MSGBUS_NAME_SERVICE_NAME_STR))
			rc = delete_broker_info_from_ds(self);

		if(MA_OK == rc)
			rc = ma_table_remove_entry(self->server_info_table, service_name); 

		/* Deletes server info from database */
		if(MA_OK == rc) {
			if(MA_OK == (rc = ma_db_transaction_begin(self->db_msgbus))) {
				if(MA_OK != (rc = db_remove_service(self->db_msgbus, service_name)))
					ma_db_transaction_cancel(self->db_msgbus);
				ma_db_transaction_end(self->db_msgbus);
			}
		}

        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Service <%s> unregistered from broker", service_name);


        if(is_reply_required) {
            ma_message_t *reply = NULL;
		    if (MA_OK == (rc = ma_message_create(&reply))) {
			    ma_message_set_property(reply , MA_PROP_KEY_NAME_SERVICE_REPLY_TYPE_STR , MA_PROP_VALUE_UNREGISTER_SERVICE_REQUEST_STR);
			    ma_msgbus_server_client_request_post_result(c_request, MA_OK, reply);
			    (void)ma_message_release(reply);
		    }
        }

        /* Commenting the publishing of service registration and unregistration code as it is not required now.
           We will exclude the broker services(name and pub_sub services) when we enable this code.
        */
		/*if(MA_OK == rc) 
			rc = publish_service_change_notification(self, MA_PROP_VALUE_NOTIFICATION_SERVICE_UNREGISTERED_STR, service_name);
		else
			MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to publish Service <%s> unregistration message", service_name);*/
	}

	return (is_reply_required) ? rc : MA_OK;
}

static ma_error_t discover_service(ma_name_service_t *self, ma_message_t *request_payload, ma_msgbus_client_request_t *c_request) {
	ma_error_t rc = MA_OK;
	char *service_name = NULL;
	ma_message_t *reply = NULL;
	ma_bool_t is_service_found = MA_FALSE;

	if((MA_OK == (rc = ma_message_get_property(request_payload, MA_PROP_KEY_SERVICE_NAME_STR, &service_name))) && service_name) {
		ma_variant_t *ser_info_var = NULL;
		if(MA_OK == (rc = ma_table_get_value(self->server_info_table, service_name, &ser_info_var))) {
			ma_buffer_t *buffer = NULL;
			if(MA_OK == (rc = ma_variant_get_raw_buffer(ser_info_var, &buffer))) {
				unsigned char *raw = NULL;
				size_t size = 0;
				if(MA_OK == (rc = ma_buffer_get_raw(buffer, &raw, &size))) {
					char pid_str[MAX_PID_LEN] = {0};

					ma_service_info_t *service_info = (ma_service_info_t *)raw;

				/*	MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Discovered info for service: %s\n \
													  Service ID: %s\n \
													  Product ID: %s\n \
													  Host name: %s\n \
													  Pid      : %d", service_info->service_name, service_info->service_id, service_info->product_id, service_info->host_name, service_info->pid);*/

					MA_MSC_SELECT(_snprintf, snprintf)(pid_str, MAX_PID_LEN - 1, "%ld", (long) service_info->pid);                    				

					if (MA_OK == (rc = ma_message_create(&reply))) {
						ma_message_set_property(reply , MA_PROP_KEY_NAME_SERVICE_REPLY_TYPE_STR , MA_PROP_VALUE_DISCOVER_SERVICE_REQUEST_STR);
						
						ma_message_set_property(reply , MA_PROP_KEY_SERVICE_NAME_STR , service_info->service_name);
						ma_message_set_property(reply , MA_PROP_KEY_SERVICE_ID_STR , service_info->service_id);
						ma_message_set_property(reply , MA_PROP_KEY_PRODUCT_ID_STR , service_info->product_id);
						ma_message_set_property(reply , MA_PROP_KEY_HOST_NAME_STR , service_info->host_name);
						ma_message_set_property(reply , MA_PROP_KEY_PID_STR , pid_str);
						
                        is_service_found = MA_TRUE;

						ma_msgbus_server_client_request_post_result(c_request, MA_OK, reply);
						(void)ma_message_release(reply);
					}   					
				}
				(void)ma_buffer_release(buffer);
			}
			(void)ma_variant_release(ser_info_var);
		}
		
		if(!is_service_found) {								
            rc = MA_ERROR_MSGBUS_SERVICE_NOT_FOUND;				
		}
	}
 
	return rc;
}

static ma_error_t load_server_info_from_database(ma_name_service_t *self) {
	ma_error_t rc = MA_OK;
	ma_db_recordset_t *record_set = NULL;
	
	if(MA_OK != (rc = db_get_all_services(self->db_msgbus, &record_set)))
		MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to get services record set from database - rc <%d>", rc);
	 
	while((MA_OK == rc) && (MA_ERROR_NO_MORE_ITEMS != (rc = ma_db_recordset_next(record_set)))) {
		char *service_name = NULL, *service_id = NULL, *product_id = NULL, *host_name = NULL;
		ma_int32_t	process_id = 0;

		if(MA_OK != rc)
			break;

		(void)ma_db_recordset_get_string(record_set, 1, &service_name);    
        (void)ma_db_recordset_get_string(record_set, 2, &service_id);  
        (void)ma_db_recordset_get_string(record_set, 3, &product_id); 
        (void)ma_db_recordset_get_int(record_set, 4, &process_id); 
		(void)ma_db_recordset_get_string(record_set, 5, &host_name); 

		if(service_name && service_id && product_id && host_name) {
			ma_service_info_t service_info = { 0 } ;
			ma_variant_t *server_info_var = NULL;

			strncpy(service_info.service_name, service_name, MA_MAX_LEN-1);
			strncpy(service_info.service_id, service_id, MA_MAX_LEN-1);
			strncpy(service_info.product_id, product_id, MA_MAX_LEN-1);
			service_info.pid = process_id;
			strncpy(service_info.host_name, host_name, MA_MAX_LEN-1);

			if(MA_OK == (rc = ma_variant_create_from_raw(&service_info, sizeof(ma_service_info_t), &server_info_var))) {
				rc = ma_table_add_entry(self->server_info_table, service_info.service_name, server_info_var);
				(void)ma_variant_release(server_info_var);
			}
		}
	}

	if(MA_ERROR_NO_MORE_ITEMS == rc)
		rc = MA_OK;

	if(record_set) (void)ma_db_recordset_release(record_set);

	return rc;
}

static ma_error_t delete_services(ma_name_service_t *self, ma_message_t *request_payload, ma_msgbus_client_request_t *c_request) {
    ma_error_t rc = MA_OK;
    const char *pid_str = NULL;

    if(MA_OK == (rc = ma_message_get_property(request_payload, MA_PROP_KEY_PEER_PID_STR, &pid_str))) {        
        if(MA_OK == (rc = db_remove_services_by_process_id(self->db_msgbus, pid_str)))
            rc = load_server_info_from_database(self);
    }
    return rc;
}

static ma_error_t ma_name_service_cb(ma_msgbus_server_t *server, ma_message_t *request_payload, void *cb_data, ma_msgbus_client_request_t *c_request) {
	ma_error_t rc = MA_OK;
    char *value = NULL;
    
	ma_name_service_t *self = (ma_name_service_t *) cb_data;

    MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Name Service server callback Invoked");

    /* Message Filter */
    if (MA_OK == (rc = ma_message_get_property(request_payload, MA_PROP_KEY_NAME_SERVICE_REQUEST_TYPE_STR, &value))) {

        if (0 == strcmp(MA_PROP_VALUE_REGISTER_SERVICE_REQUEST_STR, value))  {
			rc = register_service(self, request_payload, c_request);
		}
        else if (0 == strcmp(MA_PROP_VALUE_UNREGISTER_SERVICE_REQUEST_STR, value)) {
			rc = unregister_service(self, request_payload, c_request);
		}
        else if (0 == strcmp(MA_PROP_VALUE_DISCOVER_SERVICE_REQUEST_STR, value)) {
			rc = discover_service(self, request_payload, c_request);
		}
        else if (0 == strcmp(MA_PROP_VALUE_DELETE_SERVICES_REQUEST_STR, value)) {
			rc = delete_services(self, request_payload, c_request);
		}
        else {
            rc = MA_BROKER_INVALID_REQUEST;
            MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Invalid request <%s>", value);
        }

        if(MA_OK != rc) {
            ma_message_t *reply = NULL;
            ma_error_t reply_status = rc;
            if(MA_OK == (rc = ma_message_create(&reply))) {
		        ma_message_set_property(reply , MA_PROP_KEY_NAME_SERVICE_REPLY_TYPE_STR , value);
		        ma_msgbus_server_client_request_post_result(c_request, reply_status, reply);
		        (void)ma_message_release(reply);
	        }
        }
    }    

	return rc;
}

ma_error_t ma_name_service_start(ma_name_service_t *self) {
    if(self) {
        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Name Service Starts");
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_name_service_stop(ma_name_service_t *self) {    
    if(self) {
        ma_error_t rc = MA_OK;

        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Name Service Stops");
		
		if(MA_OK != (rc = ma_msgbus_server_stop(self->name_server)))
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to stop Name service - rc <%d>", rc);

        if(MA_OK != (rc = db_remove_all_services(self->db_msgbus)))
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to delete all services from DB - rc <%d>", rc);

	    return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_name_service_release(ma_name_service_t *self) {
    if(self) {
        ma_error_t rc = MA_OK;

		/* one more attempt to delete the data in name service unregistration failure scenarios */
		if(!self->is_broker_info_deleted) 
			delete_broker_info_from_ds(self);
		
		if(MA_OK != (rc = ma_msgbus_server_release(self->name_server)))
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Name service release failed rc = <%d>", rc);

		(void) ma_table_release(self->server_info_table);

        free(self);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

