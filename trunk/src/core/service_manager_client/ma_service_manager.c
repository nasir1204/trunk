#include "ma/ma_service_manager.h"
#include "ma/internal/defs/ma_service_manager_service_defs.h"
#include "ma/internal/utils/regex/ma_regex.h"

#include "ma/ma_message.h"
#include "ma/internal/utils/datastructures/ma_slist.h"

#include "stdlib.h"
#include "string.h"

typedef struct ma_notification_data_s ma_notification_data_t;

struct ma_notification_data_s {
	char									*topic_filter;	
	
	void									*cb_data;

	ma_service_manager_on_subscribe_cb_t	cb;

	MA_SLIST_NODE_DEFINE(ma_notification_data_t);
} ;


struct ma_service_manager_s {
	ma_msgbus_t								*msgbus;
	
	ma_msgbus_subscriber_t                  *subscriber;

	ma_logger_t                             *logger;

	MA_SLIST_DEFINE(notify_data, ma_notification_data_t);			
};

struct ma_service_manager_subscriber_list_s {
	ma_array_t			*subscribers_list;
}; 

struct ma_service_manager_service_list_s {
	ma_array_t			*services_list;
}; 

static ma_error_t ma_service_manager_subscriber_cb(const char *topic, ma_message_t *message, void *cb_data) {
	ma_service_manager_t *self = (ma_service_manager_t *)cb_data;

	if(self && topic && message) {
        if(!strncmp(topic, MA_MSGBUS_SUBSCRIBER_CHANGE_NOTIFICATION_TOPIC_STR, strlen(MA_MSGBUS_SUBSCRIBER_CHANGE_NOTIFICATION_TOPIC_STR))) {
			const char *notify_type = NULL, *topic_name = NULL, *product_id = NULL;			
			(void)ma_message_get_property(message, MA_PROP_KEY_SUBSCRIBER_CHANGE_NOTIFICATION_STR, &notify_type);
			(void)ma_message_get_property(message, MA_PROP_KEY_SUBSCRIBER_TOPIC_NAME_STR, &topic_name);
            (void)ma_message_get_property(message, MA_PROP_KEY_SUBSCRIBER_PRODUCT_ID_STR, &product_id);

			if(notify_type && topic_name && product_id) {
				ma_bool_t status = !strcmp(notify_type, MA_PROP_VALUE_NOTIFICATION_SUBSCRIBER_REGISTERED_STR) ;
				ma_notification_data_t *data = NULL;
				ma_regex_t *regex_engine = NULL;
                MA_SLIST_FOREACH(self, notify_data, data) {	
				    if(MA_OK == ma_regex_create(data->topic_filter , &regex_engine)) {										
                        if(ma_regex_match(regex_engine, topic_name)) {
							data->cb(self, status, topic_name, product_id, data->cb_data);							
						}	
					    (void)ma_regex_release(regex_engine);
                    }
				}
			}
		}
    }
    return MA_OK;
}

ma_error_t ma_service_manager_create(ma_msgbus_t *msgbus, ma_service_manager_t **service_manager){
	if(msgbus && service_manager) {
		ma_service_manager_t *self = (ma_service_manager_t *)calloc(1, sizeof(ma_service_manager_t));
		if(self) {
			ma_error_t rc = MA_OK;
			self->msgbus = msgbus;	
            if(MA_OK == (rc = ma_msgbus_subscriber_create(self->msgbus, &self->subscriber))) {
                (void) ma_msgbus_subscriber_setopt(self->subscriber,MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
                (void) ma_msgbus_subscriber_setopt(self->subscriber,MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
			    MA_SLIST_INIT(self, notify_data);
			    *service_manager = self;
                return MA_OK;
            }
            if(self->subscriber) (void)ma_msgbus_subscriber_release(self->subscriber);
            free(self);
			return rc;
		}
		return MA_ERROR_OUTOFMEMORY;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_service_manager_set_logger(ma_service_manager_t *self, ma_logger_t *logger) {
    if(self && logger) {
        self->logger = logger;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;

}
ma_error_t ma_service_manager_register_subscriber_notification(ma_service_manager_t *self, const char *topic_filter, ma_service_manager_on_subscribe_cb_t cb, void *cb_data){
	if(self && topic_filter && cb) {
		ma_notification_data_t *data = (ma_notification_data_t *)calloc(1, sizeof(ma_notification_data_t));
		if(data) {							
            ma_error_t rc = MA_OK;
            if(MA_SLIST_EMPTY(self, notify_data)) {
                /* TBD - We can subscriber for "ma.msgbus.*" for services and subscriber changes - currently subscribing only for seubscriber changes */
                if(MA_OK != (rc = ma_msgbus_subscriber_register(self->subscriber, MA_MSGBUS_SUBSCRIBER_CHANGE_NOTIFICATION_TOPIC_STR, ma_service_manager_subscriber_cb, self))){
				    free(data);
					return rc;
				}
            }

			data->topic_filter = strdup(topic_filter);
			data->cb = cb;
			data->cb_data = cb_data;
			MA_SLIST_PUSH_BACK(self, notify_data, data) ;
			return MA_OK;
		}
		return MA_ERROR_OUTOFMEMORY;
	}
	return MA_ERROR_INVALIDARG;
};

static ma_error_t ma_notificaiton_data_release(ma_notification_data_t *data) {
	if(data) {		
		if(data->topic_filter) free(data->topic_filter);		
		free(data); data = NULL;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;

}
ma_error_t ma_service_manager_unregister_subscriber_notification(ma_service_manager_t *self, const char *topic_filter){
	if(self && topic_filter) {
		ma_error_t rc = MA_OK;
		ma_notification_data_t *data = NULL;
		MA_SLIST_FOREACH(self, notify_data, data) {
			if(!strcmp(topic_filter, data->topic_filter)) {
				MA_SLIST_REMOVE_NODE(self, notify_data, data, ma_notification_data_t) ;
				rc = ma_notificaiton_data_release(data);	
				break ;
			}
		}
        
        if(MA_SLIST_EMPTY(self, notify_data)) {
            (void)ma_msgbus_subscriber_unregister(self->subscriber);		
        }
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_service_manager_get_data(ma_service_manager_t *self, const char *filter, ma_array_t **list, size_t *count, ma_bool_t is_service){
	ma_error_t rc = MA_OK;
	ma_msgbus_endpoint_t *svcs_mgr_ep = NULL;
	if(MA_OK == (rc = ma_msgbus_endpoint_create(self->msgbus, MA_MSGBUS_SERVICE_MANAGER_SERVICE_NAME_STR, MA_MSGBUS_SERVICE_MANAGER_HOST_NAME_STR, &svcs_mgr_ep))) {
		ma_message_t *request_msg = NULL;   
        (void)ma_msgbus_endpoint_setopt(svcs_mgr_ep, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
		(void)ma_msgbus_endpoint_setopt(svcs_mgr_ep, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
		if(MA_OK == (rc = ma_message_create(&request_msg))) {
			ma_message_t *response_msg = NULL;
			(void)ma_message_set_property(request_msg, MA_PROP_KEY_SERVICE_MANAGER_REQUEST_TYPE_STR, (is_service) ? MA_PROP_VALUE_GET_SERVICES_LIST_REQUEST_STR : MA_PROP_VALUE_GET_SUBSCRIBERS_LIST_REQUEST_STR);
			(void)ma_message_set_property(request_msg, (is_service) ? MA_PROP_KEY_SERVICE_NAME_FILTER_STR : MA_PROP_KEY_SUBSCRIBER_TOPIC_FILTER_STR, filter);
			if(MA_OK == (rc = ma_msgbus_send(svcs_mgr_ep, request_msg, &response_msg))) {
				ma_variant_t *res_payload = NULL;
                if(MA_OK == (rc = ma_message_get_payload(response_msg, &res_payload))) {				
					if(MA_OK == (rc = ma_variant_get_array(res_payload, list))) {
						(void)ma_array_size(*list, count);						
					}								
                    (void)ma_variant_release(res_payload);
                }
				(void)ma_message_release(response_msg); 				
			}				
			(void)ma_message_release(request_msg);
		}
		(void)ma_msgbus_endpoint_release(svcs_mgr_ep);
	}
	return rc;	
}

ma_error_t ma_service_manager_release(ma_service_manager_t *self){
	if(self) {	
        if(!MA_SLIST_EMPTY(self, notify_data)) {            
            MA_SLIST_CLEAR(self, notify_data, ma_notification_data_t, ma_notificaiton_data_release);            	
			(void)ma_msgbus_subscriber_unregister(self->subscriber);
        }	
        (void)ma_msgbus_subscriber_release(self->subscriber), self->subscriber = NULL;
		free(self);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_service_manager_get_subscribers(ma_service_manager_t *self, const char *topic_filter, ma_service_manager_subscriber_list_t **subscribers_list, size_t *count){
    if(self && topic_filter && subscribers_list && count) {
        ma_error_t  rc = MA_OK;
        ma_array_t *list = NULL;
        if(MA_OK == (rc = ma_service_manager_get_data(self, topic_filter, &list, count, MA_FALSE)) && list) {
            ma_service_manager_subscriber_list_t *tmp_subscriber_list = (ma_service_manager_subscriber_list_t *)calloc(1, sizeof(ma_service_manager_subscriber_list_t));
            if(tmp_subscriber_list) {
                tmp_subscriber_list->subscribers_list = list;
                *subscribers_list = tmp_subscriber_list;
                return rc;
            }
            (void)ma_array_release(list);
            return MA_ERROR_OUTOFMEMORY;
        }
    }
    return MA_ERROR_INVALIDARG;
}


typedef struct get_subscribers_async_data_s {
    char                                    *topic_filter;    
    ma_service_manager_get_subscribes_cb_t  cb;
    void                                    *cb_data;
    ma_service_manager_t                    *self;
} get_subscribers_async_data_t;

ma_error_t get_subscribers_async_cb(ma_error_t status, ma_message_t *response, void *cb_data, ma_msgbus_request_t *request) {
    get_subscribers_async_data_t *async_data = (get_subscribers_async_data_t *)cb_data;   

    if(async_data) {
        ma_error_t rc = MA_OK;
        ma_variant_t *res_payload = NULL;        
        if(MA_OK == (rc = ma_message_get_payload(response, &res_payload))) {
            ma_array_t *list = NULL;            
            if(MA_OK == (rc = ma_variant_get_array(res_payload, &list))) {
                ma_service_manager_subscriber_list_t subscribers_list;
                size_t count = 0;
	            (void)ma_array_size(list, &count);
                subscribers_list.subscribers_list = list;
                async_data->cb(status, async_data->self, async_data->topic_filter, &subscribers_list, count, async_data->cb_data);
                (void) ma_array_release(list);
            }				            
            (void)ma_variant_release(res_payload);
        }  
        if(async_data->topic_filter) free(async_data->topic_filter);
        free(async_data); async_data = NULL;
    }

    if((status == MA_OK) && response) ma_message_release(response);
	if(request) {
		ma_msgbus_endpoint_t *ep = NULL;
		(void)ma_msgbus_request_get_endpoint(request, &ep);         
        (void)ma_msgbus_request_release(request);          
        if(ep) (void)ma_msgbus_endpoint_release(ep);
	}
	return MA_OK;
}

ma_error_t ma_service_manager_get_subscribers_async(ma_service_manager_t *self, const char *topic_filter, ma_service_manager_get_subscribes_cb_t cb, void *cb_data) {
    if(self && topic_filter && cb) {
        ma_error_t rc = MA_OK;
	    ma_msgbus_endpoint_t *svcs_mgr_ep = NULL;
        get_subscribers_async_data_t *async_data = (get_subscribers_async_data_t *)calloc(1, sizeof(get_subscribers_async_data_t));
        if(async_data) {
            async_data->topic_filter = strdup(topic_filter);
            async_data->cb = cb;
            async_data->cb_data = cb_data;
            async_data->self = self;
            if(MA_OK == (rc = ma_msgbus_endpoint_create(self->msgbus, MA_MSGBUS_SERVICE_MANAGER_SERVICE_NAME_STR, MA_MSGBUS_SERVICE_MANAGER_HOST_NAME_STR, &svcs_mgr_ep))) {
		        ma_message_t *request_msg = NULL;   
                (void)ma_msgbus_endpoint_setopt(svcs_mgr_ep, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
		        (void)ma_msgbus_endpoint_setopt(svcs_mgr_ep, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
		        if(MA_OK == (rc = ma_message_create(&request_msg))) {
			        ma_message_t *response_msg = NULL;
                    ma_msgbus_request_t  *msgbus_request = NULL;
			        (void)ma_message_set_property(request_msg, MA_PROP_KEY_SERVICE_MANAGER_REQUEST_TYPE_STR, MA_PROP_VALUE_GET_SUBSCRIBERS_LIST_REQUEST_STR);
			        (void)ma_message_set_property(request_msg, MA_PROP_KEY_SUBSCRIBER_TOPIC_FILTER_STR, topic_filter);
			        if(MA_OK == (rc = ma_msgbus_async_send(svcs_mgr_ep, request_msg, get_subscribers_async_cb, async_data, &msgbus_request))) {
                        (void)ma_message_release(request_msg);
                        return MA_OK;
                    }
                    (void)ma_message_release(request_msg);
                }
                (void)ma_msgbus_endpoint_release(svcs_mgr_ep);
            } 
            if(async_data->topic_filter) free(async_data->topic_filter);
            free(async_data); async_data = NULL;
        }
        return MA_ERROR_OUTOFMEMORY;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_service_manager_subscriber_list_release(ma_service_manager_subscriber_list_t *subscriber_list){
	if(subscriber_list) {
		if(subscriber_list->subscribers_list) {
			ma_error_t rc = MA_OK;
			size_t i = 0, count = 0;
			(void)ma_array_size(subscriber_list->subscribers_list, &count);
			for(i = 1; i <= count; i++) {
				ma_variant_t *var = NULL;
				ma_table_t *tb = NULL;
				if(MA_OK == (rc = ma_array_get_element_at(subscriber_list->subscribers_list, i, &var))) {
					if(MA_OK == (rc = ma_variant_get_table(var, &tb)))
						(void)ma_table_release(tb); tb = NULL;
					(void)ma_variant_release(var); var = NULL;
				}
			}
			(void)ma_array_release(subscriber_list->subscribers_list);
			free(subscriber_list);
			return rc;
		}
	}
	return MA_ERROR_INVALIDARG;
}

static ma_error_t get_value_from_array(ma_array_t *list, ma_uint32_t index, const char *key, const char **value) {
	if(list && key && value) {
		size_t count = 0;		
		(void)ma_array_size(list, &count);
		if(index < count) {
			ma_error_t rc = MA_OK;
			ma_variant_t *var = NULL;
			ma_table_t *tb = NULL;
			if(MA_OK == (rc = ma_array_get_element_at(list, index+1, &var))) {
				if(MA_OK == (rc = ma_variant_get_table(var, &tb))) {
					ma_variant_t *value_var = NULL;
					if(MA_OK == (rc = ma_table_get_value(tb, key, &value_var))){
						ma_buffer_t *buf = NULL;
						if(MA_OK == (rc = ma_variant_get_string_buffer(value_var, &buf))) {
							size_t len = 0;
							rc = ma_buffer_get_string(buf, value, &len);
							(void)ma_buffer_release(buf); buf = NULL;
						}
						(void)ma_variant_release(value_var); value_var = NULL;
					}
					(void)ma_table_release(tb); tb = NULL;
				}
				(void)ma_variant_release(var); var = NULL;
			}
			return rc;
		}
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_service_manager_subscriber_list_get_topic_name(ma_service_manager_subscriber_list_t *subscriber_list, ma_uint32_t index, const char **topic) {
    if(subscriber_list && topic && subscriber_list->subscribers_list) {
	    return get_value_from_array(subscriber_list->subscribers_list, index, MA_SUBSCRIBER_TOPIC_NAME_STR, topic);
    }
    return MA_ERROR_INVALIDARG;
}
ma_error_t ma_service_manager_subscriber_list_get_product_id(ma_service_manager_subscriber_list_t *subscriber_list, ma_uint32_t index, const char **product_id){
    if(subscriber_list && product_id && subscriber_list->subscribers_list) {
	    return get_value_from_array(subscriber_list->subscribers_list, index, MA_SUBSCRIBER_PRODUCT_ID_STR, product_id);
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_service_manager_get_services(ma_service_manager_t *self, const char *service_name_filter, ma_service_manager_service_list_t **service_list, size_t *count) {
    if(self && service_name_filter && service_list && count) {
        ma_error_t  rc = MA_OK;
        ma_array_t *list = NULL;
        if(MA_OK == (rc = ma_service_manager_get_data(self, service_name_filter, &list, count, MA_TRUE)) && list) {
            ma_service_manager_service_list_t *tmp_service_list = (ma_service_manager_service_list_t *)calloc(1, sizeof(ma_service_manager_service_list_t));
            if(tmp_service_list) {
                tmp_service_list->services_list = list;
                *service_list = tmp_service_list;
                return rc;
            }
            (void) ma_array_release(list);
            return MA_ERROR_OUTOFMEMORY;
        }
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_service_manager_service_list_release(ma_service_manager_service_list_t *service_list) {
    if(service_list && service_list->services_list) {
		ma_error_t rc = MA_OK;
		size_t i = 0, count = 0;
		(void)ma_array_size(service_list->services_list, &count);
		for(i = 1; i <= count; i++) {
			ma_variant_t *var = NULL;
			ma_table_t *tb = NULL;
			if(MA_OK == (rc = ma_array_get_element_at(service_list->services_list, i, &var))) {
				if(MA_OK == (rc = ma_variant_get_table(var, &tb)))
					(void)ma_table_release(tb); tb = NULL;
				(void)ma_variant_release(var); var = NULL;
			}
		}
		(void)ma_array_release(service_list->services_list);
		free(service_list); service_list = NULL;
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}


ma_error_t ma_service_manager_service_list_get_service_name(ma_service_manager_service_list_t *service_list, ma_uint32_t index, const char **service_name) {
    if(service_list && service_name && service_list->services_list) {
        return get_value_from_array(service_list->services_list, index, MA_SERVICE_NAME_STR, service_name);
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_service_manager_service_list_get_product_id(ma_service_manager_service_list_t *service_list, ma_uint32_t index, const char **product_id) {
    if(service_list && product_id && service_list->services_list) {
	    return get_value_from_array(service_list->services_list, index, MA_SERVICE_PRODUCT_ID_STR, product_id);
    }
    return MA_ERROR_INVALIDARG;
}


