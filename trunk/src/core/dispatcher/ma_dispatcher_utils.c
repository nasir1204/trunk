#include "ma/internal/utils/threading/ma_atomic.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/ma_common.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include "ma/internal/core/dispatcher/ma_dispatcher_utils.h"
#include "ma/internal/ma_strdef.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>


MA_CPP(extern "C" {)

#ifdef MA_WINDOWS
#include <Windows.h>
#else
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <dirent.h>
#endif


static ma_error_t ma_trim_begin(char *xml_value) {
    if(xml_value && *xml_value) {
        char *pstr = xml_value;
        while(pstr && *pstr) {
            if(*pstr == ' ')
                strncpy(pstr, pstr+1, (strlen(pstr)-1));
            else break;
        }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_trim_end(char *xml_value) {
    if(xml_value && *xml_value) {
        size_t len = strlen(xml_value);
        len--;
        while(len > 0) {
            if((xml_value[len] == '\n') || (xml_value[len] == ' ')) {
                xml_value[len] = '\0';
                --len;
            }
            else break;
        }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}
 
static ma_error_t ma_parse_xml_tag(const char *xml_buf, const char *tag, char **path) {
    if(xml_buf && tag && path) {
        ma_error_t err = MA_OK;
        char start_tag[MA_MAX_LEN] = {0};
        char end_tag[MA_MAX_LEN] = {0};
        char *pstart  = NULL;
        char *pend    = NULL;
        size_t offset_len = 0;

        sprintf(start_tag, "<%s>", tag);
        sprintf(end_tag, "</%s>", tag);
        
        pstart = (char*)strstr(xml_buf, start_tag);
        if(!pstart)return MA_ERROR_INVALIDARG;
        pend = (char*)strstr(xml_buf, end_tag);
        if(!pend)return MA_ERROR_INVALIDARG;

        if(pstart > pend)return MA_ERROR_UNEXPECTED; //no idea how to hit this..

        offset_len = pend - (pstart + strlen(start_tag));
        *path = (char*)calloc(offset_len+1, sizeof(char));
        if(!(*path))return MA_ERROR_OUTOFMEMORY;

        memcpy(*path, pstart+strlen(start_tag), offset_len);
        err = ma_trim_begin(*path);
        if(MA_OK != err)return err;
        return ma_trim_end(*path);
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_get_product_rsdk_path(const char *product_id, char **path) {
    if(product_id && path) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;
#ifdef MA_WINDOWS
        HKEY hKey;
        char value[MA_MAX_LEN*2] = {0};
        DWORD type = REG_SZ;
        DWORD size = sizeof(value);
        char *reg_path = NULL;
        size_t path_len = 0;

        *path = NULL;
        path_len = strlen(MA_CONFIGURATION_REGISTRY_APPLICATIONS_PATH_STR)+strlen(product_id)+1;
        if((reg_path = (char*)calloc(path_len+1, sizeof(char)))) {
            MA_MSC_SELECT(_snprintf, snprintf)(reg_path, path_len, MA_CONFIGURATION_REGISTRY_APPLICATIONS_PATH_STR"%s", product_id);
            err = MA_ERROR_DISPATCHER_INVALID_SOFTWARE_ID_PATH;
            
            /* Always check for 32bit view but key name is different for 32 and 64 bit applications */
            if(ERROR_SUCCESS == RegOpenKeyExA(HKEY_LOCAL_MACHINE, reg_path, 0, KEY_READ | KEY_WOW64_32KEY, &hKey)) {
                if(ERROR_SUCCESS == RegQueryValueExA(hKey, MA_CONFIGURATION_APPLICATIONS_KEY_RSDK_PATH_STR, 0, &type, (BYTE *)value, &size)) {
                    err = MA_ERROR_OUTOFMEMORY;                                         
                    if((*path= (char*)calloc(size + 1, sizeof(char)))) {
                        strncpy(*path, value, size);
                        err = MA_OK;
                    }
                } 
                RegCloseKey(hKey);
            }
            free(reg_path);
        }
        
        return err;
#else
        /* TBD - will handle cross bit communication between PPs and MA */
        ma_temp_buffer_t temp_buf = MA_TEMP_BUFFER_INIT;
        char          config_path[MA_MAX_LEN] = {0};    
        size_t        len = 0;
        struct stat   st;

        err = MA_ERROR_DISPATCHER_INVALID_SOFTWARE_ID_PATH;
        ma_temp_buffer_init(&temp_buf);
        len = strlen(MA_CONFIGURATION_REGISTRY_APPLICATIONS_PATH_STR)+ strlen(product_id)+strlen(MA_CONFIGURATION_REGISTRY_CONFIG_FILE_NAME_STR)*2;
        ma_temp_buffer_reserve(&temp_buf, len);
        sprintf(config_path, "%s%s%c%s", MA_CONFIGURATION_REGISTRY_APPLICATIONS_PATH_STR, product_id,MA_PATH_SEPARATOR,MA_CONFIGURATION_REGISTRY_CONFIG_FILE_NAME_STR);

        if(0 == stat(config_path, &st)) {
            if(st.st_size > 0) {
                char *xml_buf = NULL;
                ma_temp_buffer_t temp_xml_buf = MA_TEMP_BUFFER_INIT;

                ma_temp_buffer_init(&temp_xml_buf);
                ma_temp_buffer_reserve(&temp_xml_buf, st.st_size+1);
                xml_buf = (char*)ma_temp_buffer_get(&temp_xml_buf);
                if(xml_buf) {
                    FILE *fp = NULL;
                    fp = fopen(config_path, "r");
                    if(fp) {                        
                        if(st.st_size == fread(xml_buf, 1, st.st_size, fp)) {
							xml_buf[st.st_size] = '\0';
                            if(MA_OK == ma_parse_xml_tag(xml_buf, MA_CONFIGURATION_APPLICATIONS_KEY_RSDK_PATH_STR, path)) {
                                err = MA_OK;
                            }
                        }
                        fclose(fp);
                    }
                }
                ma_temp_buffer_uninit(&temp_xml_buf);
            }
        }
        ma_temp_buffer_uninit(&temp_buf);
        return (MA_OK != err)?MA_ERROR_DISPATCHER_INVALID_SOFTWARE_ID_PATH:err;
#endif
    }
    return MA_ERROR_INVALIDARG;
}


MA_CPP(})
