#include "ma/internal/core/dispatcher/ma_library_grouper.h"
#include "ma/internal/core/dispatcher/ma_dispatcher_utils.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal/core/dispatcher/ma_dispatcher_grouper.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"

#include <stdlib.h>
#include <stdio.h>

static ma_error_t ma_library_grouper_find_member(ma_grouper_t *grouper, const char *member_name, void *member) {
    if(grouper && member_name && member) {
        ma_library_grouper_t *manager = (ma_library_grouper_t*)grouper;
        ma_library_t **ret_member = (ma_library_t**)member;
        size_t iter = 0;

        *ret_member = NULL;
        for(iter = 0; iter < manager->size; ++iter) {
            if(manager->group[iter].name && !strncmp(manager->group[iter].name, member_name, strlen(manager->group[iter].name))) {
                *ret_member = &manager->group[iter];
                break;
            }
        }

        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static void ma_library_destroy(ma_library_t *library) {
    if(library) {
        if(library->is_loaded) {
            library->is_loaded = MA_FALSE;
            MA_DL_CLOSE(&library->lib);
            if(library->plugin_ver)*((unsigned int*)library->plugin_ver) = 0; //resetting version
            if(library->sym_addr_table_set) {
                size_t itr = 0;
                for(itr = 0; itr < library->sym_table_set_size; ++itr) {
                    free(*(void**)library->sym_addr_table_set[itr]);
                    *(void**)library->sym_addr_table_set[itr] = NULL;
                }
                library->sym_table_set_size = 0;
                free(library->sym_addr_table_set);
            }
            free(library->name);
        }
    }
}

static ma_error_t ma_library_grouper_add_member(ma_grouper_t *grouper, const char *member) {
    if(grouper && member) {
        ma_error_t err = MA_OK;
        ma_library_t *library = NULL;

        if(MA_OK == (err = ma_grouper_find_member(grouper, member, &library))) {
            if(library) {
                ++library->ref_count;
            } else {
                ma_library_grouper_t *manager = (ma_library_grouper_t*)grouper;
                ma_temp_buffer_t temp_buffer = MA_TEMP_BUFFER_INIT;
                char *lib_path = NULL;
                size_t len = 0;

                if(manager->capacity > 0) {
                    --manager->capacity;
                } else {
                    manager->group = (ma_library_t*)realloc(manager->group, (sizeof(ma_library_t) * (manager->size + 1)));
                    if(!manager->group)return MA_ERROR_OUTOFMEMORY;
                }
                memset(&manager->group[manager->size], 0 , sizeof(struct ma_library_s));
                ++manager->capacity;
                manager->group[manager->size].name = strdup(member);
                if(!manager->group[manager->size].name)return MA_ERROR_OUTOFMEMORY;
                len = (manager->path ? strlen(manager->path)*2 : 0);
                ma_temp_buffer_reserve(&temp_buffer, len);
                lib_path = (char*)ma_temp_buffer_get(&temp_buffer);
                if(lib_path) {
					if(manager->path)
						sprintf(lib_path, "%s%c%s", manager->path, MA_PATH_SEPARATOR, member);
					else
						sprintf(lib_path, "%s", member);

                    err = MA_DL_OPEN(lib_path, &manager->group[manager->size].lib);
                    if(MA_OK == err) {
                        manager->group[manager->size].is_loaded = MA_TRUE;
                        MA_ATOMIC_INCREMENT(manager->group[manager->size].ref_count);
                        --manager->capacity;
                        ++manager->size;
                    } else {
                        if(manager->group[manager->size].name)free(manager->group[manager->size].name);
                    }
                }
                ma_temp_buffer_uninit(&temp_buffer);
            }
        }
        
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_library_grouper_remove_member(ma_grouper_t *grouper, const char *member) {
    if(grouper && member) {
        ma_library_grouper_t *manager = (ma_library_grouper_t*)grouper;
        ma_error_t err = MA_OK;
        size_t iter = 0;
        ma_bool_t found = MA_FALSE;

        for(iter = 0; iter < manager->size; ++iter) {
            if(manager->group[iter].name && !strcmp(manager->group[iter].name, member)) {
                if(manager->group[iter].ref_count > 0) {
                    if(0 == MA_ATOMIC_DECREMENT(manager->group[iter].ref_count)) {
                        ma_library_destroy(&manager->group[iter]);
                        found = MA_TRUE;
                        break;
                    }
                }
            }
        }

        if(found) {            
            if(manager->size != (iter+1)) {
                memcpy(manager->group+iter, manager->group+(iter+1), (sizeof(ma_library_t)*(manager->size - (iter+1))));
            }
            --manager->size;
            ++manager->capacity;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static void ma_library_grouper_destroy_group(ma_grouper_t *grouper) {
    if(grouper) {
        ma_library_grouper_t *manager = (ma_library_grouper_t*)grouper;
        size_t iter = 0;
        for(iter = 0; iter < manager->size;  ++iter) {
            ma_library_destroy(&manager->group[iter]);
        }
        manager->size = 0;
        if(manager->name)free(manager->name);
        if(manager->path)free(manager->path);
        if(manager->group)free(manager->group);		
    }
}

static ma_error_t find_sym_table_set(ma_library_t *library, void *sym_addr_table, ma_bool_t *found) {
    if(library && sym_addr_table && found) {
        size_t itr = 0;

        *found = MA_FALSE;
        for(itr =0; itr < library->sym_table_set_size; ++itr) {
            if(library->sym_addr_table_set[itr] == sym_addr_table) {
                *found = MA_TRUE; break;
            }
        }

        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_library_grouper_load_symbol_table(ma_grouper_t *grouper, const char *library_name, const char **sym_name_table, void *sym_addr_table, size_t sym_table_size) {
    if(grouper && library_name && sym_name_table && sym_addr_table && sym_table_size) {
        ma_error_t err = MA_OK;
        ma_library_t *library = NULL;

        if(MA_OK == (err = ma_grouper_find_member(grouper, library_name, &library))) {
            ma_bool_t is_sym_table_exist = MA_FALSE;
            if((MA_OK == (err = find_sym_table_set(library, sym_addr_table, &is_sym_table_exist))) && !is_sym_table_exist) {
                err = MA_ERROR_OUTOFMEMORY;
                if((library->sym_addr_table_set = (void**)realloc(library->sym_addr_table_set, sizeof(void*) * ++library->sym_table_set_size))) {
                    size_t iter = 0;
                    void **sym_set = NULL;

                    if((sym_set = (void**)calloc(sym_table_size, sizeof(void*)))) {
                        for(iter = 0; iter < sym_table_size; ++iter) {
                            sym_set[iter] = (void*)MA_DL_SYM(&library->lib, sym_name_table[iter]);
                        }
                        library->sym_addr_table_set[library->sym_table_set_size - 1] = sym_addr_table;
                        *(void**)sym_addr_table = sym_set;
                        err = MA_OK;
                    }
                }
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static const ma_grouper_methods_t library_methods = {
    &ma_library_grouper_add_member,
    &ma_library_grouper_remove_member,
    &ma_library_grouper_find_member,
    &ma_library_grouper_load_symbol_table,
    &ma_library_grouper_destroy_group
};

void ma_library_grouper_init(ma_library_grouper_t *library_grouper, void *data) {
    ma_grouper_init((ma_grouper_t*)library_grouper, &library_methods, &library_grouper);
    library_grouper->group = NULL;
    library_grouper->name = NULL;
    library_grouper->size = 0;
    library_grouper->capacity = 0;
    library_grouper->data = data;
}
