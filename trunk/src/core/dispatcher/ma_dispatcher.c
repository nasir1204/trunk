#include "ma/ma_dispatcher.h"
#include "ma/internal/core/dispatcher/ma_dispatcher_base.h"
#include "ma/internal/core/dispatcher/ma_dispatcher_utils.h"
#include "ma/internal/core/dispatcher/ma_product_grouper.h"
#include "ma/internal/core/dispatcher/ma_library_grouper.h"
#include "ma/internal/utils/platform/ma_dl.h"
#include "ma/internal/utils/threading/ma_atomic.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/threading/ma_thread.h"
#include "ma/ma_errors.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef MA_WINDOWS
#include<Windows.h>
#else
#include<pthread.h>
#include<assert.h>
#endif

static ma_dispatcher_t *dispatcher_instance;
static ma_once_t dispatcher_once = MA_ONCE_INIT;
#define MA_DISPATCHER_RESET_ONCE(once)\
    {ma_once_t reset = MA_ONCE_INIT;memcpy(&once, &reset, sizeof(ma_once_t));}

struct ma_dispatcher_s {
    ma_dispatcher_base_t   dispatcher_base;
    ma_product_grouper_t   product_grouper;
    ma_atomic_counter_t    ref_count;
};

static ma_error_t ma_dispatcher_init(ma_dispatcher_base_t *base, const char *default_rsdk_path) {
    if(base) {
        ma_dispatcher_t *dispatcher = (ma_dispatcher_t*)base;
        ma_product_grouper_init(&dispatcher->product_grouper, default_rsdk_path, &dispatcher);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_dispatcher_load(ma_dispatcher_base_t *base, const char *module, const char *product_name, const char *library_name,unsigned int *version, const char **sym_name_table, void *sym_addr_table, size_t sym_table_size) {
    if(base && product_name && library_name && sym_name_table && sym_addr_table && sym_table_size) {
        ma_error_t err  = MA_ERROR_DISPATCHER_LIBRARY_LOAD_FAILED;

        if(product_name) {
            if(MA_OK != (err = ma_grouper_add_member((ma_grouper_t*)&dispatcher_instance->product_grouper, product_name))) {
                return err;
            }
        }
        if(library_name) {
            ma_library_grouper_t *product = NULL;
            if(MA_OK == (err = ma_grouper_find_member((ma_grouper_t*)&dispatcher_instance->product_grouper, product_name, &product))) {
                if(MA_OK == (err = ma_grouper_add_member((ma_grouper_t*)product, library_name))) {
                    ma_library_t *library = NULL;

                    if(MA_OK == (err = ma_grouper_find_member((ma_grouper_t*)product, library_name, &library))) {
                        if(!*(void***)sym_addr_table) {
                            if(library->ref_count > 1)--library->ref_count;  
                            err = ma_grouper_load_sym_table((ma_grouper_t*)product, library_name, sym_name_table, sym_addr_table, sym_table_size);
                        }
                        if(version && !*version) {
                            const char *get_version_str = "_get_version";
                            char temp[MA_MAX_LEN] = {0};
                            typedef ma_error_t (*version_func_t)(unsigned int*);
                            version_func_t version_func;

                            MA_MSC_SELECT(_snprintf, snprintf)(temp, MA_MAX_LEN, "%s%s", module, get_version_str);
                            version_func = (version_func_t)MA_DL_SYM(&library->lib, temp);
                            if(version_func && (MA_OK == (*version_func)(version))) {
                                library->plugin_ver = version;
                            }
                        }
                    }
                }
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_dispatcher_unload(ma_dispatcher_base_t *base, const char *product_name, const char *library_name) {
    if(base) {
        ma_dispatcher_t *dispatcher = (ma_dispatcher_t*)base;
        ma_error_t err  = MA_ERROR_DISPATCHER_LIBRARY_UNLOAD_FAILED;

        if(product_name && library_name) {
            ma_library_grouper_t *product = NULL;
            if(MA_OK == (err = ma_grouper_find_member((ma_grouper_t*)&dispatcher->product_grouper, product_name, &product))) {
                err = ma_grouper_remove_member((ma_grouper_t*)product, library_name);
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static void ma_dispatcher_destroy(ma_dispatcher_base_t *base) {
    if(base) {
        ma_dispatcher_t *dispatcher = (ma_dispatcher_t*)base;
        ma_grouper_destroy_group((ma_grouper_t*)&dispatcher->product_grouper);
    }
}

static const ma_dispatcher_base_methods_t dispatcher_methods = {
    &ma_dispatcher_init,
    &ma_dispatcher_load,
    &ma_dispatcher_unload,
    &ma_dispatcher_destroy
};

static ma_error_t ma_dispatcher_add_ref(ma_dispatcher_t *dispatcher) {
    if(dispatcher) {
        MA_ATOMIC_INCREMENT(dispatcher->ref_count);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_dispatcher_release(ma_dispatcher_t *dispatcher) {
    if(dispatcher) {
        if(dispatcher->ref_count > 0) {
            if(0 == MA_ATOMIC_DECREMENT(dispatcher->ref_count)) {
                ma_dispatcher_base_t *base = (ma_dispatcher_base_t*)dispatcher;
                ma_dispatcher_base_release(base);
                if(dispatcher)free(dispatcher);
                dispatcher_instance = NULL;
                MA_DISPATCHER_RESET_ONCE(dispatcher_once);
            }
        }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static void ma_dispatcher_once(void) {
    if(!dispatcher_instance) {
        dispatcher_instance = (ma_dispatcher_t*)calloc(1,sizeof(struct ma_dispatcher_s));
    }
}

ma_error_t ma_dispatcher_initialize(const char *default_rsdk_path, ma_dispatcher_t **dispatcher) {
    if(dispatcher) {
        ma_error_t err = MA_OK;

        if(!dispatcher_instance) {
            ma_once(&dispatcher_once, &ma_dispatcher_once);
            if(dispatcher_instance) {
                *dispatcher = (ma_dispatcher_t*)dispatcher_instance;
                ma_dispatcher_add_ref(*dispatcher);
                ma_dispatcher_base_init((ma_dispatcher_base_t*)dispatcher_instance, &dispatcher_methods, dispatcher);          
                err = ma_dispatcher_base_initialize((ma_dispatcher_base_t*)dispatcher_instance, default_rsdk_path);
            } else {
                return MA_ERROR_OUTOFMEMORY;
            }
        } else {
            *dispatcher = (ma_dispatcher_t*)dispatcher_instance;
            err = ma_dispatcher_add_ref(*dispatcher);
        }

        return (MA_OK != err)?MA_ERROR_DISPATCHER_INIT_FAILED:err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_dispatcher_deinitialize(ma_dispatcher_t *dispatcher) {
    return dispatcher?ma_dispatcher_release(dispatcher):MA_ERROR_INVALIDARG;
}

MA_DISPATCHER_API ma_error_t ma_dispatcher_unload_library(const char *product_name, const char *library_name) {
    if(product_name && library_name && dispatcher_instance) {
        return (MA_OK == ma_dispatcher_base_unload((ma_dispatcher_base_t*)dispatcher_instance, product_name, library_name))
                ?MA_OK
                :MA_ERROR_DISPATCHER_LIBRARY_UNLOAD_FAILED;
    }
    return MA_ERROR_INVALIDARG;
}

MA_DISPATCHER_API ma_error_t ma_dispatcher_load_library(const char *module, const char *product_name, const char *library_name, unsigned int *version, const char *sym_name_table[], void *sym_addr_table, size_t sym_table_size) {
    if(product_name && library_name && dispatcher_instance) {
        return (MA_OK == ma_dispatcher_base_load((ma_dispatcher_base_t*)dispatcher_instance, module, product_name, library_name, version, sym_name_table, sym_addr_table, sym_table_size))
                ?MA_OK
                :MA_ERROR_DISPATCHER_LIBRARY_LOAD_FAILED;
    }
    return MA_ERROR_INVALIDARG;
}


