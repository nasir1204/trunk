#include "ma/internal/core/dispatcher/ma_product_grouper.h"
#include "ma/internal/core/dispatcher/ma_library_grouper.h"
#include "ma/internal/core/dispatcher/ma_dispatcher_utils.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include "ma/internal/ma_macros.h"
#include "ma/ma_common.h"

#include <stdlib.h>
#include <string.h>


static ma_error_t ma_product_grouper_add_member(ma_grouper_t *grouper, const char *member) {
    if(grouper && member) {
        ma_error_t err  = MA_OK;
        ma_library_grouper_t *librarian = NULL;

        if(MA_OK == (err = ma_grouper_find_member(grouper, member, &librarian))) {
            ma_product_grouper_t *manager = (ma_product_grouper_t*)grouper;
            if(!librarian) {
                char *path = NULL;
                if(manager->capacity > 0) {
                    --manager->capacity;
                } else {
                    manager->group = (ma_library_grouper_t*)realloc(manager->group, (sizeof(ma_library_grouper_t) * (manager->size + 1)));
                    if(!manager->group)return MA_ERROR_OUTOFMEMORY;
                }
                ma_get_product_rsdk_path(member, &path);

                memset(&manager->group[manager->size], 0 , sizeof(ma_library_grouper_t));
                ma_library_grouper_init(&manager->group[manager->size],&manager);
                manager->group[manager->size].name = strdup(member);
                if(!manager->group[manager->size].name) {
                    if(path)free(path);
                    ++manager->capacity;
                    return MA_ERROR_OUTOFMEMORY;
                }
				manager->group[manager->size].path = (path ? path : (manager->default_rsdk_path ? strdup(manager->default_rsdk_path) : NULL));
                ++manager->size;
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_product_grouper_remove_member(ma_grouper_t *grouper, const char *member) {
    if(grouper && member) {
        ma_product_grouper_t *manager = (ma_product_grouper_t*)grouper;
        ma_error_t err = MA_OK;
        size_t iter = 0;
        ma_bool_t found = MA_FALSE;

        for(iter = 0; iter < manager->size; ++iter) {
            if(manager->group[iter].name && !strcmp(manager->group[iter].name, member)) {
                ma_grouper_t *group_manager = (ma_grouper_t*)&manager->group[iter];
                err = ma_grouper_remove_member(group_manager, member);
                found = MA_TRUE;
                break;
            }
        }

        if(found) {            
            if(manager->size != (iter+1)) {
                memcpy(manager->group+iter, manager->group+(iter+1), (sizeof(ma_library_grouper_t)*(manager->size - (iter+1))));
            }
            --manager->size;
            ++manager->capacity;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_product_grouper_find_member(ma_grouper_t *grouper, const char *member_name, void *member) {
    if(grouper && member_name && member) {
        ma_product_grouper_t *manager = (ma_product_grouper_t*)grouper;
        ma_library_grouper_t **ret_member = (ma_library_grouper_t**)member;
        size_t iter = 0;

        *ret_member = NULL;
        for(iter = 0; iter < manager->size; ++iter) {
            if(manager->group[iter].name && !strcmp(manager->group[iter].name, member_name)) {
                *ret_member = &manager->group[iter];
                break;
            }
        }

        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static void ma_product_grouper_destroy_group(ma_grouper_t *grouper) {
    if(grouper) {
        ma_product_grouper_t *manager = (ma_product_grouper_t*)grouper;
        size_t iter = 0;
        for(iter = 0; iter < manager->size;  ++iter) {
            ma_grouper_t *group_manager = (ma_grouper_t*)&manager->group[iter];
            ma_grouper_destroy_group(group_manager);
        }
        manager->size = 0;
        if(manager->group)free(manager->group);
		if(manager->default_rsdk_path) free(manager->default_rsdk_path);
    }
}

static const ma_grouper_methods_t product_methods = {
    &ma_product_grouper_add_member,
    &ma_product_grouper_remove_member,
    &ma_product_grouper_find_member,
    0,
    &ma_product_grouper_destroy_group
};

void ma_product_grouper_init(ma_product_grouper_t *product_grouper, const char *default_rsdk_path, void *data) {
    ma_grouper_init((ma_grouper_t*)product_grouper, &product_methods, &product_grouper);
	if(default_rsdk_path)
		product_grouper->default_rsdk_path = strdup(default_rsdk_path);
    product_grouper->data = data;
    product_grouper->group = NULL;
    product_grouper->size = 0;
    product_grouper->capacity = 0;
}
