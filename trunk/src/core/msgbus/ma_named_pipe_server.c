#include "ma/internal/core/msgbus/ma_named_pipe_server.h"
#include "ma/internal/core/msgbus/ma_named_pipe_connection.h"

#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/utils/event_loop/ma_uv_helpers.h"

#include "ma/internal/ma_macros.h"
#include "ma/internal/core/msgbus/ma_msgbus_private.h"
#include "ma/internal/defs/ma_pubsub_service_defs.h"


#include <stdlib.h>
#include <string.h>
#include <stdio.h> /* snprintf */


#if defined(MA_WINDOWS)
# define MA_PIPE_PREFIX "\\\\.\\pipe\\"
#else
# define MA_PIPE_PREFIX "/var/tmp/.msgbus/"
#endif



struct ma_named_pipe_server_s {
    /*! os pipe handle */
    uv_pipe_t								uv_pipe;

    /*! */
    ma_event_loop_t							*loop;

    /*! extra cache  */
    struct uv_loop_s						*uv_loop;

    ma_logger_t								*logger;

    /*! the full name of the name pipe (after being started) */
    char									*full_pipe_name;

    /*! callback to call on new incoming connections */
    ma_named_pipe_server_connection_cb		cb;

    /*! and some data to give that callback */
    void									*cb_data;

    struct ma_message_auth_info_provider_s *auth_info_provider;
    
	struct ma_msgbus_passphrase_handler_s	*passphrase_handler;

	struct ma_msgbus_crypto_provider_s      *crypto_provider;

    
};

static void on_accept_cb(ma_named_pipe_connection_t *connection, ma_error_t status, void *cb_data) {
    ma_named_pipe_server_t *self = (ma_named_pipe_server_t *)cb_data;

    MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "accept callback invoked pipe name <%s> with status <%d>", self->full_pipe_name, status);
    if(MA_OK == status) {
        if(self->cb) {
            MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Calling user callback on this new connection on pipe name <%s>", self->full_pipe_name);
            self->cb(connection, self->cb_data); /* Assume callback takes ownership */
        }
        else {
            /* Who owns the connection now? */
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "There is no callback, who should own the connection now, should not happen.");
            (void) ma_named_pipe_connection_close(connection, 0);
            (void) ma_named_pipe_connection_release(connection);
        }
    } else {
            (void) ma_named_pipe_connection_close(connection, 0);
            (void) ma_named_pipe_connection_release(connection);
    }
}

/* libuv callback */
static void on_new_connection(uv_stream_t* server, int status) {
    ma_named_pipe_server_t *self = server->data;
    ma_named_pipe_connection_t *new_connection = NULL;
    ma_error_t err;
	char pipe_name[MAX_PIPENAME_LEN];
    pipe_name[MA_COUNTOF(pipe_name)-1] = 0;  
    
    MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "New connection on pipe name <%s> with status <%d>, provider: %p", self->full_pipe_name, status, self->auth_info_provider);

    if (MA_OK == (err = ma_named_pipe_connection_create(self->loop, &new_connection))) {
	
		MA_MSC_CONCAT(_,snprintf)(pipe_name, MA_COUNTOF(pipe_name)-1, "%s_%p", self->full_pipe_name, new_connection);
		(void) ma_named_pipe_connection_set_pipe_name(new_connection, pipe_name);
		
        (void) ma_named_pipe_connection_set_logger(new_connection, self->logger);
        (void) ma_named_pipe_connection_set_auth_info_provider(new_connection, self->auth_info_provider);
		(void) ma_named_pipe_connection_set_passphrase_handler(new_connection, self->passphrase_handler);
		
        if(MA_OK != (err = ma_named_pipe_connection_accept(new_connection,  self->crypto_provider, server, on_accept_cb, self))) {
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to accept the connection <%d>.", err);
            (void) ma_named_pipe_connection_release(new_connection);
        }
    }
}



ma_error_t ma_named_pipe_server_create(ma_event_loop_t *loop, ma_named_pipe_server_t **pipe_server) {
    if(loop && pipe_server) {
        ma_named_pipe_server_t *self = (ma_named_pipe_server_t *) calloc(1, sizeof(ma_named_pipe_server_t));
        if (self) {
            self->loop = loop;
            ma_event_loop_get_loop_impl(loop, &self->uv_loop);
            *pipe_server = self; 
            return MA_OK;
        }
        return MA_ERROR_OUTOFMEMORY;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_named_pipe_server_release(ma_named_pipe_server_t *self) {
    if (self) {
        if (self->full_pipe_name) free(self->full_pipe_name);        
        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Name pipe server destroys");
        free(self);
    }
    return MA_OK;
}

ma_error_t ma_named_pipe_server_start(ma_named_pipe_server_t *self, struct ma_msgbus_crypto_provider_s *crypto_provider, const char *pipe_name, ma_named_pipe_server_connection_cb connection_cb, void *cb_data) {
    if (self) {
        char name[256] = MA_PIPE_PREFIX;
		int uv_rc = 0;
        uv_strlcat(name, pipe_name, MA_COUNTOF(name) );

		self->crypto_provider = crypto_provider;
        self->cb = connection_cb;
        self->cb_data = cb_data;

        uv_pipe_init(self->uv_loop, &self->uv_pipe, 0);
        self->uv_pipe.data = self;

#ifndef MA_WINDOWS
		mode_t cur_umask = umask(0000); /* socket should be created in mode 0777 to connect other users */
#endif
        uv_rc = uv_pipe_bind(&self->uv_pipe, name);

#ifndef MA_WINDOWS
		umask(cur_umask); /* restoring old umask */
#endif
		if (0 != uv_rc) {
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "pipe bind failed - uv error <%d> <%s>", MA_ERROR_FROM_UV_LAST_ERROR(self->uv_loop), UV_ERROR_STR_FROM_LAST_UV_ERROR(self->uv_loop));
            return MA_ERROR_MSGBUS_INTERNAL;            
        }
        if (0 != uv_listen((uv_stream_t*)&self->uv_pipe, 128, &on_new_connection)) {
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "pipe listen failed - uv error <%d> <%s>", MA_ERROR_FROM_UV_LAST_ERROR(self->uv_loop), UV_ERROR_STR_FROM_LAST_UV_ERROR(self->uv_loop));
            return MA_ERROR_MSGBUS_INTERNAL; 
        }

        self->full_pipe_name = strdup(name);
        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Name pipe server started for pipe <%s>", self->full_pipe_name);
    }
    return MA_OK;
}

static void pipe_close_cb(uv_handle_t *handle) {   
    /* MA_LOG(msgbus_logger, MA_LOG_SEV_DEBUG, "Pipe handle close callback is Invoked in named pipe server");     */
}

ma_error_t ma_named_pipe_server_stop(ma_named_pipe_server_t *self) {
    if (self) {
        uv_close((uv_handle_t *)&self->uv_pipe, pipe_close_cb);
        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Named pipe server stopped for pipe <%s>", self->full_pipe_name);
    }
    return MA_OK;
}

ma_error_t ma_named_pipe_server_get_pipe_name(ma_named_pipe_server_t *self, const char **pipe_name) {
    if (self && pipe_name) {
        *pipe_name = self->full_pipe_name;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_named_pipe_server_set_logger(ma_named_pipe_server_t *self, ma_logger_t *logger) {
    if(self && logger) {
        self->logger = logger;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_named_pipe_server_set_auth_info_provider(ma_named_pipe_server_t *self, struct ma_message_auth_info_provider_s *provider) {
    return self 
        ? (self->auth_info_provider = provider, MA_OK)
        : MA_ERROR_INVALIDARG;
}

ma_error_t ma_named_pipe_server_set_passphrase_handler(ma_named_pipe_server_t *self, struct ma_msgbus_passphrase_handler_s *passphrase_handler) {
	    return self 
        ? (self->passphrase_handler = passphrase_handler, MA_OK)
        : MA_ERROR_INVALIDARG;

}
