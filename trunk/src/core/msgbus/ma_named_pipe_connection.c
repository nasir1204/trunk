#include "ma/internal/core/msgbus/ma_named_pipe_connection.h"
#include "ma/internal/core/msgbus/ma_named_pipe_auth.h"
#include "ma/internal/core/msgbus/ma_message_private.h"
#include "ma/internal/core/msgbus/ma_message_auth_info_private.h"
#include "ma/ma_message.h"
#include "ma/internal/core/msgbus/ma_message_header.h"
#include "ma/internal/utils/serialization/ma_serialize.h"
#include "ma/internal/utils/serialization/ma_deserialize.h"
#include "ma/internal/utils/event_loop/ma_uv_helpers.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include "ma/internal/utils/text/ma_utf8.h"
#include "ma/internal/ma_macros.h"

#include "ma/internal/core/msgbus/ma_msgbus_private.h"

#include <stdio.h>

#ifdef MA_WINDOWS
#include <process.h>
#include <Psapi.h>
#else
#include <sys/types.h>
#include <unistd.h>
#endif


#include "uv-private/ngx-queue.h" /*If they can use it, so can we */

#include <stdlib.h>
#include <assert.h>

/* named pipe authentication timeout 2 minutes = 2 * 60 * 1000 milliseconds */
#define MA_NAMED_PIPE_AUTHENTICATION_TIME_OUT_INT           (2 * 60 * 1000) 

typedef struct qhead_item_s {
    ngx_queue_t qlink;
    ma_message_t *msg;
} qhead_item_t;

/* link layer protocol encapsulation */
typedef enum protocol_type_e {
    /*! for control messages, handled by ma_named_pipe_connection internally */
    PROTOCOL_TYPE_CONTROL = 0x37,

    /*! application layer message, forwarded to upper layers */
    PROTOCOL_TYPE_MESSAGE = 0x23
} protocol_type_t;

struct ma_named_pipe_connection_s {
    /*! yes, I am a message consumer */
    /* Please keep as first field in this struct */
    ma_message_consumer_t   consumer;

    /*! and also a producer, this is where I will deposit my messages to */
    ma_message_consumer_t   *message_sink;

    /* weak reference (e.g. not reference counted), so we can reject incoming messages that we send! */
    ma_message_t            *outgoing_message;

    /*! queue of messages to be sent */
    ngx_queue_t             qhead;

    /*! os pipe handle */
    uv_pipe_t               uv_pipe;

    /*! */
    uv_write_t              uv_write_request;

    /*! serializer for saving outgoing messages */
    ma_serializer_t         *serializer;

    /*! deserializer for bytes coming from the pipe */
    ma_deserializer_t       *deserializer;

    /*! our glory event loop */
    ma_event_loop_t         *loop;

	/*! logger instance */
	ma_logger_t				*logger;

    
	/*! pipe name */
	char					*pipe_name;

    /*! extra cache  */
    struct uv_loop_s        *uv_loop;

    /* used during shutdown of pipe */
    uv_idle_t               idle_watcher; 

    /*! message authentication info object  */
    ma_message_auth_info_t  *peer_auth_info;

    /*! optional auth provider */
    ma_message_auth_info_provider_h auth_provider;

	/*! optional crypto provider */
	struct ma_msgbus_crypto_provider_s *crypto_provider;

	/*! pipe auth */
	ma_named_pipe_auth_t	*pipe_auth;

	/*! optional passphrase provider */
	struct ma_msgbus_passphrase_handler_s *passphrase_handler;

    /* peer process id, for routing only */
    unsigned long           peer_pid;

    /* peer_pid obtained in trusted manner (via OS) */
    unsigned long           secure_peer_pid;

    unsigned is_server:1;

    unsigned write_pending:1;

    unsigned has_connected:1;

    unsigned is_pipe_connection_closed:1;

    union {
        struct { /* only for clients */
            uv_connect_t                uv_connect;
            ma_named_pipe_connect_cb    connect_cb;
            void                        *connect_cb_data;
            ma_named_pipe_accept_cb     accept_cb;
            void                        *accept_cb_data;
        } client_data;
    } extra;
};

typedef void (*ma_named_pipe_authenication_cb)(ma_named_pipe_connection_t *connection, ma_error_t status, void *cb_data);

static void send_variant(ma_named_pipe_connection_t *self, ma_variant_t *v_msg);
static void check_send(ma_named_pipe_connection_t *self);

static ma_error_t consumer_on_message(ma_message_consumer_t *consumer, ma_message_t *msg) {
    ma_named_pipe_connection_t *self = consumer->data;

    if (self->outgoing_message != msg) {
        qhead_item_t *item = calloc(1, sizeof(qhead_item_t));
        if (item) {
            item->msg = msg;
            ma_message_add_ref(msg);
            MA_LOG(self->logger, MA_LOG_SEV_TRACE, "Adding received message into queue");
            ngx_queue_insert_tail(&self->qhead, &item->qlink);
			check_send(self);
            return MA_OK;
        }
        MA_LOG(self->logger, MA_LOG_SEV_CRITICAL, "Out of Memory");
        return MA_ERROR_OUTOFMEMORY;
    }
    //MA_LOG(msgbus_logger, MA_LOG_SEV_DEBUG, "Rejecting messages coming back to same pipe connection that we are in the process of sending");
    return MA_ERROR_INVALIDARG; /* Reject messages coming back to us that we are in the process of sending... Probably need a better error code */
}

static ma_error_t ma_named_pipe_connection_destroy(ma_named_pipe_connection_t *self);

static void consumer_destroy(ma_message_consumer_t *base) {
    ma_named_pipe_connection_destroy((ma_named_pipe_connection_t*) base);
}

static const ma_message_consumer_methods_t consumer_methods = {
    &consumer_on_message,
    &consumer_destroy
};


ma_error_t ma_named_pipe_connection_create(ma_event_loop_t *loop, ma_named_pipe_connection_t **connection) {
	if(loop && connection) {
		ma_named_pipe_connection_t *self = calloc(1, sizeof(ma_named_pipe_connection_t));
		if (self) {
			self->loop = loop;
			ma_event_loop_get_loop_impl(loop, &self->uv_loop);

			uv_pipe_init(self->uv_loop, &self->uv_pipe, 1);
			self->uv_pipe.data = self;			
			ma_message_consumer_init(&self->consumer, &consumer_methods, 1, self);

			ngx_queue_init(&self->qhead);

			*connection = self;
			return MA_OK;
		}
		return MA_ERROR_OUTOFMEMORY;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_named_pipe_connection_add_ref(ma_named_pipe_connection_t *self) {
    return ma_message_consumer_inc_ref((ma_message_consumer_t*) self);
}


ma_error_t ma_named_pipe_connection_release(ma_named_pipe_connection_t *self) {
    return ma_message_consumer_dec_ref((ma_message_consumer_t*) self);
}


static ma_error_t ma_named_pipe_connection_destroy(ma_named_pipe_connection_t *self) {
    if (self) {

        ma_message_consumer_dec_ref(self->message_sink);

        ma_message_auth_info_release(self->peer_auth_info);

		if(self->pipe_auth) {
			ma_named_pipe_auth_stop(self->pipe_auth);
		    ma_named_pipe_auth_release(self->pipe_auth), self->pipe_auth = NULL;				
		}

        if (self->deserializer) {
            if(MA_OK != ma_deserializer_destroy(self->deserializer)) {
                MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to destroy deserializer");
            }
        }

        if (self->serializer) {
            if(MA_OK != ma_serializer_destroy(self->serializer)) {
                MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to destroy serializer");
            }
        }

        while (!ngx_queue_empty(&self->qhead)) {
            ngx_queue_t *ngx_item = ngx_queue_head(&self->qhead);

            qhead_item_t *item = ngx_queue_data(ngx_item, qhead_item_t, qlink);

            ngx_queue_remove(ngx_item);
            ma_message_release(item->msg);
            free(item);
        }

	if(self->pipe_name) free(self->pipe_name);
        free(self);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t create_framepackage(protocol_type_t type, ma_variant_t *payload, ma_variant_t **frame_package) {
    ma_array_t *pa = 0;
    ma_variant_t *vt = 0, *v= 0;
    ma_error_t e;

    ma_bool_t result = (MA_OK == (e = ma_array_create(&pa)))
        && (MA_OK == (e = ma_variant_create_from_uint8(type, &vt)))
        && (MA_OK == (e = ma_array_push(pa, vt)))
        && (MA_OK == (e = ma_array_push(pa, payload)))
        && (MA_OK == (e = ma_variant_create_from_array(pa, &v)));

    ma_variant_release(vt);
    ma_array_release(pa);

    if (MA_OK == e) {
        *frame_package = v; /* transfer ownership to caller */
    } else {
        ma_variant_release(v);
    }
    return e;
}

/* creates a control package containing the process id
 * Should probably use a subtype field, so more than one message type can be send
 */
static void send_pid(ma_named_pipe_connection_t *self) {
    ma_variant_t *p = 0, *fp = 0;
    assert("should not try to send pid when write is busy" && !self->write_pending);
    if ((MA_OK == ma_variant_create_from_uint32(getpid(), &p))
        && (MA_OK == create_framepackage(PROTOCOL_TYPE_CONTROL, p, &fp))) {
            MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Sending pid <%d>", getpid());
            send_variant(self, fp);
    }
    else {
        MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to create framepackage with pid <%d>", getpid());
    }

    ma_variant_release(fp);
    ma_variant_release(p);
}

static void on_client_authenticate_cb(ma_named_pipe_auth_t *pipe_auth, ma_error_t status, void *cb_data, ma_message_auth_info_t *peer_auth_info) {
    ma_named_pipe_connection_t *self = (ma_named_pipe_connection_t *)cb_data;
    MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "on_client_authenticate_cb callback called with status <%d>.", status);
	self->uv_pipe.data = self;			
    if(MA_OK == status) {        
        ma_message_auth_info_add_ref(self->peer_auth_info = peer_auth_info);
        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "named pipe connection accepted.");        
        self->is_server = 1;
		self->has_connected = 1;
    } else {
        /* connection refused */
        status = MA_ERROR_MSGBUS_SERVICE_CONNECT_FAILED;
        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "named pipe connection authentication failed.");
    }
    ma_message_auth_info_release(peer_auth_info);
	ma_named_pipe_auth_release(self->pipe_auth), self->pipe_auth = NULL;
    self->extra.client_data.accept_cb(self, status, self->extra.client_data.accept_cb_data);
}

ma_error_t ma_named_pipe_connection_accept(ma_named_pipe_connection_t *self, struct ma_msgbus_crypto_provider_s *crypto_provider, uv_stream_t *server, ma_named_pipe_accept_cb cb,  void *cb_data) {
    if (self ) {
        self->crypto_provider = crypto_provider;
        if (0 == uv_accept(server, (uv_stream_t *)&self->uv_pipe)) {
            /* Server (e.g. self) just accepted the connection, now is a good time to do some authentication of the client */
            ma_error_t rc = MA_ERROR_MSGBUS_INTERNAL;
			if(MA_OK == (rc = ma_named_pipe_auth_create(self->uv_loop, self->crypto_provider, &self->uv_pipe, &self->pipe_auth))) {
                (void)ma_named_pipe_auth_set_logger(self->pipe_auth, self->logger);
                (void)ma_named_pipe_auth_set_passphrase_handler(self->pipe_auth,self->passphrase_handler);
				(void)ma_named_pipe_auth_set_server(self->pipe_auth);
                self->extra.client_data.accept_cb = cb;
                self->extra.client_data.accept_cb_data = cb_data;
                if(MA_OK == (rc = ma_named_pipe_auth_start(self->pipe_auth, MA_NAMED_PIPE_AUTHENTICATION_TIME_OUT_INT, on_client_authenticate_cb, self))) {
                    MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "named pipe authentication started.");                    
                    return MA_OK;
                }
				(void)ma_named_pipe_auth_release(self->pipe_auth), self->pipe_auth = NULL;
                MA_LOG(self->logger, MA_LOG_SEV_ERROR, "named pipe auth start failed, <%d>.", rc);
            }
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "named pipe auth creation failed, <%d>.", rc);
            
            return rc ;
        }
        MA_LOG(self->logger, MA_LOG_SEV_ERROR, "UV accept Failed - uv error <%d> <%s>", MA_ERROR_FROM_UV_LAST_ERROR(self->uv_loop), UV_ERROR_STR_FROM_LAST_UV_ERROR(self->uv_loop));
        return MA_ERROR_MSGBUS_INTERNAL;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_named_pipe_connection_set_pipe_name(ma_named_pipe_connection_t *self, const char *pipe_name) {
    return (pipe_name && (self->pipe_name = strdup(pipe_name))) ? MA_OK : MA_ERROR_INVALIDARG; 
}

ma_error_t ma_named_pipe_connection_set_consumer(ma_named_pipe_connection_t *connection, ma_message_consumer_t *consumer) {
    ma_message_consumer_dec_ref(connection->message_sink);
    ma_message_consumer_inc_ref(connection->message_sink = consumer);
    MA_LOG(connection->logger, MA_LOG_SEV_DEBUG, "set consumer in named pipe connection");
    return MA_OK;
}

static void on_server_authenticate_cb(ma_named_pipe_auth_t *pipe_auth, ma_error_t status, void *cb_data, ma_message_auth_info_t *peer_auth_info) {
    ma_named_pipe_connection_t *self = (ma_named_pipe_connection_t *)cb_data;
    MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "on_server_authenticate_cb callback called with status <%d>.", status);
	self->uv_pipe.data = self;
    if(MA_OK == status) {
        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "named pipe connection authenticated");
        ma_message_auth_info_add_ref(self->peer_auth_info = peer_auth_info);
        send_pid(self);/* start pid exchange (for addressing, not for security) */
        if (self->extra.client_data.connect_cb) 
            self->extra.client_data.connect_cb(self, status, self->extra.client_data.connect_cb_data);

        self->has_connected = 1;
        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Sending pending messages..", status);
        //check_send(self); /* avoid sending any message, it may lost till client received the pid from server. */
    } else {
        /* connection refused */
        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "named pipe connection authentication failed.");
        if (self->extra.client_data.connect_cb) 
            self->extra.client_data.connect_cb(self, MA_ERROR_MSGBUS_SERVICE_CONNECT_FAILED, self->extra.client_data.connect_cb_data);
    }
    ma_message_auth_info_release(peer_auth_info);
	ma_named_pipe_auth_release(self->pipe_auth), self->pipe_auth = NULL;
}

/* libuv client connection callback, invoked when client(self) has just established connection to a server */
static void connect_cb(uv_connect_t* req, int status) {
    ma_named_pipe_connection_t *self = req->data;
    ma_error_t rc = !status ? MA_OK: MA_ERROR_MSGBUS_SERVICE_CONNECT_FAILED;
    MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Name pipe connection callback with status <%d>", status);

    if(MA_OK == rc) {
        /* now is a good time to authenticate the server */
        if(MA_OK == (rc = ma_named_pipe_auth_create(self->uv_loop, self->crypto_provider, &self->uv_pipe, &self->pipe_auth))) {
            (void)ma_named_pipe_auth_set_logger(self->pipe_auth, self->logger);
             (void)ma_named_pipe_auth_set_passphrase_handler(self->pipe_auth,self->passphrase_handler);
            if(MA_OK == (rc = ma_named_pipe_auth_start(self->pipe_auth, MA_NAMED_PIPE_AUTHENTICATION_TIME_OUT_INT, on_server_authenticate_cb, self))) {
                MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "named pipe authentication started.");
                return;
            }else {
                (void)ma_named_pipe_auth_release(self->pipe_auth), self->pipe_auth = NULL;
                MA_LOG(self->logger, MA_LOG_SEV_ERROR, "named pipe auth start failed, <%d>.", rc);
            }
        } else {
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "named pipe auth creation failed <%d>.", rc);
        }
    }else {
        MA_LOG(self->logger, MA_LOG_SEV_ERROR, "UV connect Failed - uv error <%d> <%s>", MA_ERROR_FROM_UV_LAST_ERROR(self->uv_loop), UV_ERROR_STR_FROM_LAST_UV_ERROR(self->uv_loop));
    }

    
    if (self->extra.client_data.connect_cb) {
        self->extra.client_data.connect_cb(self, (MA_OK == rc ) ? MA_OK : MA_ERROR_MSGBUS_SERVICE_CONNECT_FAILED, self->extra.client_data.connect_cb_data);
    }    
}


ma_error_t ma_named_pipe_connection_connect(ma_named_pipe_connection_t *self, struct ma_msgbus_crypto_provider_s *crypto_provider,  const char *name, ma_named_pipe_connect_cb cb, void *cb_data) {
    if (self && name) {
        assert(!self->is_server);
        self->crypto_provider = crypto_provider;
		self->pipe_name = strdup(name);
        self->extra.client_data.connect_cb = cb;
        self->extra.client_data.connect_cb_data = cb_data;
        self->extra.client_data.uv_connect.data = self;
        uv_pipe_connect(&self->extra.client_data.uv_connect, &self->uv_pipe, name, &connect_cb);
        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "connecting to named pipe....");
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static void handle_control_packet(ma_named_pipe_connection_t *self, ma_variant_t *var_obj) {
    ma_uint32_t pid;
    if (MA_OK == ma_variant_get_uint32(var_obj, &pid)) {
        self->peer_pid = pid;
        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "received peer process pid <%d>", pid);
        
        if (self->secure_peer_pid && self->secure_peer_pid != self->peer_pid) {
            MA_LOG(self->logger, MA_LOG_SEV_WARNING, "mismatched pid from peer, using secure pid instead for auth, s=<%u>, i=<%u>", self->secure_peer_pid, self->peer_pid);
            pid = self->secure_peer_pid; 
        }

        if (self->auth_provider) {
            ma_error_t rc = ma_message_auth_info_provider_decorate_auth_info(self->auth_provider,  pid, self->peer_auth_info);
            if (MA_FAILED(rc)) {
                MA_LOG(self->logger, MA_LOG_SEV_WARNING, "Failed getting additional auth information, rc = <%ld>", (long) rc);
            }
        }

        /* conditions have changed, check if we can send*/
        /* we may consider deferring this to next loop iteration (via a timer)*/
		if(self->is_server){
			/*responds the client process pid with its pid.*/
			send_pid(self);
		}
		else{
			/*client has received the server pid so start sending the original message.*/
			check_send(self);
		}
    }
    else {
        MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to get the pid from variant object");
    }
}


static void handle_message_packet(ma_named_pipe_connection_t *self, ma_variant_t *var_obj) {
    ma_message_t *msg;
    if (MA_OK == ma_message_from_variant(var_obj , &msg)) {
        (void)ma_message_set_auth_info(msg, self->peer_auth_info);
        /* We need to set the destination PID to so that router doesn't fan out to other named pipe connection now */
        /* But if the message type is of publish we do want it to fanout from broker router's  , comparison with broker pid which is set by publisher*/
        if( ! ( (MA_MESSAGE_TYPE_PUBLISH == GET_MESSAGE_TYPE(msg)) && (GET_MESSAGE_DESTINATION_PID(msg) == getpid())) ) {
            MA_LOG(self->logger, MA_LOG_SEV_TRACE, "Set the destination pid to zero to stop the sending this message to other named pipe connections");
            SET_MESSAGE_DESTINATION_PID(msg , 0 );
        }

        /* remember outgoing message so we can reject it if it comes back to us right away */
        self->outgoing_message = msg;
        /* now push up */
        if (self->message_sink) {
            ma_message_consumer_send(self->message_sink, msg);
        }
        self->outgoing_message = NULL;

        ma_message_release(msg);
    }
    else {
        MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to get the message from variant object");
    }
}

static void handle_link_packet(ma_named_pipe_connection_t *self, ma_variant_t *packet) {
    /* a link packet should be an array where first element is the type(ma_uint8), next element is the message payload (variant) */
    ma_array_t *pa;
    if (MA_OK == ma_variant_get_array(packet, &pa)) {
        ma_variant_t *pa1 = 0, *pa2 = 0;
        if ((MA_OK == ma_array_get_element_at(pa, 1, &pa1)) && (MA_OK == ma_array_get_element_at(pa, 2, &pa2))) {
            ma_uint8_t protocol_type;
            if(MA_OK == ma_variant_get_uint8(pa1, &protocol_type)) {

                switch (protocol_type) {
                case PROTOCOL_TYPE_CONTROL: {
                    handle_control_packet(self, pa2);
                    break;
                    }
                case PROTOCOL_TYPE_MESSAGE: {
                    handle_message_packet(self, pa2);
                    break;
                    }
                default:
                    MA_LOG(self->logger, MA_LOG_SEV_ERROR, "unknown protocol type");
                    assert(!"unknown protocol type");
                    break;
                }
            }
            else {
                MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to get protocol type from packet");
            }
        }
        ma_variant_release(pa2);
        ma_variant_release(pa1);
        ma_array_release(pa);
    }
}

static void on_incoming_data(ma_named_pipe_connection_t *self, const char *data, size_t len) {

    if (!self->deserializer) {
        if(MA_OK != ma_deserializer_create(&self->deserializer)) {
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "deserializer create Failed");
        }
    }

    if (self->deserializer) {
        if(MA_OK == ma_deserializer_put(self->deserializer, data, len)) {
            for (;;) {
                ma_variant_t *var_obj = NULL; /* init to null for safety - MA_OK from ma_deserializer_get_next() may not mean anything! */
                if (MA_OK == ma_deserializer_get_next(self->deserializer, &var_obj)) {
                    if (var_obj) {
                        handle_link_packet(self, var_obj);
                        ma_variant_release(var_obj);
                    } else {
                        MA_LOG(self->logger, MA_LOG_SEV_WARNING, "no object returned from parser on pipe <%s>", self->pipe_name);
                    }
                } else {
                    /* TODO Need better error handling so we can close the connection when receiving invalid data */
                    break; /* could be because of: No more data, Parsing error, Not enough data, ... */
                }
            }
			if(MA_OK != ma_deserializer_reset(self->deserializer, MA_FALSE)) {
                (void)ma_deserializer_destroy(self->deserializer) ; self->deserializer = NULL ;
            }
        }
        else {
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "deserializer put Failed");
        }    
    }
}

/* close named pipe connection and clear local cache when other end connection is terminated */
static void signal_connection_terminated_message(ma_named_pipe_connection_t *self) {
	ma_error_t rc = MA_OK;
	ma_error_t status = MA_ERROR_MSGBUS_SERVICE_CONNECTION_TERMINATED;
	ma_message_t *msg = NULL;

	if(MA_OK == (rc = ma_message_create(&msg))) {
		SET_MESSAGE_TYPE(msg, MA_MESSAGE_TYPE_SIGNAL);
		SET_MESSAGE_DESTINATION_PID(msg, 0);
		SET_MESSAGE_STATUS(msg, status);		
		ma_message_set_property(msg, MA_PROP_KEY_PIPE_NAME_STR, self->pipe_name);        
        	if(0 < self->peer_pid) {
            		char pid_str[16] = {0};
            		MA_MSC_SELECT(_snprintf, snprintf)(pid_str, 16 - 1, "%d",  self->peer_pid);
            		ma_message_set_property(msg, MA_PROP_KEY_PEER_PID_STR, pid_str);
        	}
		ma_message_consumer_send(self->message_sink, msg);			
		ma_message_release(msg); msg = NULL;
	}
}

static void read_cb(uv_stream_t *stream, ssize_t nread, uv_buf_t buf) {
    ma_named_pipe_connection_t *self = stream->data;
    if (-1 == nread) {
		/* This is an error, the other end probably closed the connection. We should do the same */
		uv_err_t uv_err = uv_last_error(self->uv_loop);		
        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "got <%s> on pipe <%s>", uv_err_name(uv_err), self->pipe_name);
        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "peer end connection of <%s> was terminated", self->pipe_name);
        self->has_connected = 0 ; /* It ensures that no one will try to send the data on this pipe */
        signal_connection_terminated_message(self);            
    } else if (0 < nread) {
        MA_LOG(self->logger, MA_LOG_SEV_TRACE, "Received %ld bytes from pipe <%s>", (long) nread, self->pipe_name);
        /* We actually got some data */
        on_incoming_data(self, buf.base, (size_t)nread);
    } else {
        /* no data, but may still need to free the buffer */
    }

    if (buf.base) free(buf.base);
}

static uv_buf_t alloc_cb(uv_handle_t *handle, size_t suggested_size) {
    uv_buf_t buffer = {0};
    for (;suggested_size; suggested_size /= 2) {
        if (NULL != (buffer.base = malloc(suggested_size))) {
            buffer.len = suggested_size;
            return buffer;
        }
    }
    return buffer;
}

ma_error_t ma_named_pipe_connection_start_receiving(ma_named_pipe_connection_t *self) {
    if (self) {
        if (0 == uv_read_start((uv_stream_t*)&self->uv_pipe, &alloc_cb, &read_cb)) {
            MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "named pipe <%s> start receiving...", self->pipe_name);
            return MA_OK;
        } else {
            //uv_err_t uv_err = uv_last_error(self->uv_loop);            
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "UV read start failed - uv error <%d> <%s> ", MA_ERROR_FROM_UV_LAST_ERROR(self->uv_loop), UV_ERROR_STR_FROM_LAST_UV_ERROR(self->uv_loop));
            //"error from uv_read_start() %s", uv_strerror(uv_err);
            //return (ma_error_t) uv_err.code;            
            return MA_ERROR_MSGBUS_INTERNAL;
        }
    }
    return MA_ERROR_INVALIDARG;
}

static void pipe_close_cb(uv_handle_t *handle) { 
    ma_named_pipe_connection_t *self = (ma_named_pipe_connection_t *)handle->data;
    MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "<%s> pipe closed completely", self->pipe_name);    	
    ma_named_pipe_connection_release(self);
}

static void idle_close_cb(uv_handle_t *handle) {
    ma_named_pipe_connection_t *self = handle->data;
    MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "named pipe <%s> connection closeing from idle close cb...", self->pipe_name);    
	self->uv_pipe.data = self;
    uv_close((uv_handle_t *) &self->uv_pipe, pipe_close_cb);
}
    
static void idle_cb(uv_idle_t* handle, int status) {
    ma_named_pipe_connection_t *self = handle->data;
    MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "named pipe <%s> connection idle cb...", self->pipe_name);
    uv_idle_stop(handle);
    self->idle_watcher.data = self;
    self->has_connected = 0 ;
    uv_close((uv_handle_t*)&self->idle_watcher, &idle_close_cb);
}

/*!
 * stops all communication and releases the underlying OS resource
 */
ma_error_t ma_named_pipe_connection_close(ma_named_pipe_connection_t *self, int options) {
    if (self) {
        
		if(self->pipe_auth) {
			ma_named_pipe_auth_stop(self->pipe_auth);
		    ma_named_pipe_auth_release(self->pipe_auth), self->pipe_auth = NULL;				
		}
		
        if(0 == uv_read_stop((uv_stream_t *) &self->uv_pipe)) {
            /* options - 1, will send all the messages from queue and close */
            if(options == 1) {
                if(!self->is_pipe_connection_closed) {
                    ma_named_pipe_connection_add_ref(self); /* adding reference for completion of the pipe data cleanup and close cb execution */
                    MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Send all the messages from connection Q..");
                    check_send(self); 
  
                    uv_idle_init(ma_event_loop_get_uv_loop(self->loop), &self->idle_watcher);
                    self->idle_watcher.data = self;
                    self->is_pipe_connection_closed = MA_TRUE;
                    uv_idle_start(&self->idle_watcher,&idle_cb);

                    MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "named pipe <%s> connection takes sometime to send pending messages", self->pipe_name);
                }
            }
            else {
                if(!self->is_pipe_connection_closed) {
                    ma_named_pipe_connection_add_ref(self); /* adding reference for completion of the pipe data cleanup and close cb execution */
                    self->has_connected = 0 ;
                    self->is_pipe_connection_closed = MA_TRUE;
					self->uv_pipe.data = self;
                    uv_close((uv_handle_t *) &self->uv_pipe, pipe_close_cb);
                    MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "named pipe <%s> connection closed...", self->pipe_name);
                }
            }
            return MA_OK;
        }
        else {
            //uv_err_t uv_err = uv_last_error(self->uv_loop);
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "UV read stop failed - uv error <%d> <%s> ", MA_ERROR_FROM_UV_LAST_ERROR(self->uv_loop), UV_ERROR_STR_FROM_LAST_UV_ERROR(self->uv_loop));            
            return MA_ERROR_MSGBUS_INTERNAL;
        }
    }
    return MA_ERROR_INVALIDARG;
}

static void write_cb(uv_write_t* req, int status) {
    ma_named_pipe_connection_t *self = req->data;

    MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Write callback is invoked in named pipe connection of pipe name <%s> with status <%d>", self->pipe_name, status);

    self->write_pending = 0;

    if(MA_OK != ma_serializer_reset(self->serializer)) {
        MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Serializer reset failed, destroying this object.");
        ma_serializer_destroy(self->serializer), self->serializer = NULL;
    }

    // see if there's more to do
    check_send(self);
}


typedef void (*ma_named_pipe_connection_send_cb)(ma_named_pipe_connection_t *connection, int status, void *cb_data);

ma_error_t ma_named_pipe_connection_send(ma_named_pipe_connection_t *self, const void *data, size_t bytes_to_send, ma_named_pipe_connection_send_cb cb, void *cb_data) {
    uv_buf_t buf;
    buf.base = (void*) data;
    buf.len = bytes_to_send;

    self->uv_write_request.data = self;
    self->write_pending = 1;
    if (0 != uv_write(&self->uv_write_request, (uv_stream_t*) &self->uv_pipe, &buf, 1, &write_cb)) {        
        self->write_pending = 0;     
        MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to write data on pipe name <%s> ", self->pipe_name);
        MA_LOG(self->logger, MA_LOG_SEV_ERROR, "UV write failed - uv error <%d> <%s> ", MA_ERROR_FROM_UV_LAST_ERROR(self->uv_loop), UV_ERROR_STR_FROM_LAST_UV_ERROR(self->uv_loop));                            
        return MA_ERROR_MSGBUS_INTERNAL;
    }

    return MA_OK;
}


static void send_variant(ma_named_pipe_connection_t *self, ma_variant_t *v_msg) {
    /* */
    if (!self->serializer) {
        if(MA_OK != ma_serializer_create(&self->serializer)) {
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to create serializer");
        }
    }
    if (self->serializer) {
        if (MA_OK == ma_serializer_add_variant(self->serializer, v_msg) ) {
            char *data;
            size_t len;
            if (MA_OK == ma_serializer_get(self->serializer, &data, &len) ) {
                MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Sending data on connected name pipe");
                ma_named_pipe_connection_send(self, data, len, NULL, NULL);
            }
        }
    }
}

static void check_send(ma_named_pipe_connection_t *self) {
    /* start sending if not already sending */
    /* Note, this could be called when a connect is in progress... */
    if ( !self->write_pending && self->has_connected && 0<self->peer_pid && !ngx_queue_empty(&self->qhead)) {
        ngx_queue_t *item = ngx_queue_head(&self->qhead);
        qhead_item_t *msg_item = ngx_queue_data(item, qhead_item_t, qlink);
        ma_message_t *msg = msg_item->msg;
        ma_variant_t *v_msg;

        /* if the message type is publish, no need to check the peer pid..As we do want want it to broadcast it with the help of broker */
        const ma_bool_t should_send = (MA_MESSAGE_TYPE_PUBLISH == GET_MESSAGE_TYPE(msg)) || (GET_MESSAGE_DESTINATION_PID(msg) == self->peer_pid);        
        ngx_queue_remove(item);
        free(msg_item);


        if(should_send) {            
            SET_MESSAGE_SOURCE_PID(msg , getpid());
            if (MA_OK == ma_message_as_variant(msg, &v_msg)) {
                /* now frame it */
                ma_variant_t *f;
                if (MA_OK == create_framepackage(PROTOCOL_TYPE_MESSAGE, v_msg, &f)) {
                    send_variant(self, f);
                    ma_variant_release(f);
                }


                ma_variant_release(v_msg);
            }
            ma_message_release(msg);
        } else {
            /* getting here means we are dropping messages, which may or may not be ok...it may be okay if we delete it from queue and continue.*/
            ma_message_release(msg);
            MA_LOG(self->logger, MA_LOG_SEV_TRACE, "cleaning up messages which doesnt belongs to self.");
            check_send(self);
        }
        
    }
}

ma_error_t ma_named_pipe_connection_set_logger(ma_named_pipe_connection_t *self, ma_logger_t *logger) {
	if(self && logger) {
		self->logger = logger;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_named_pipe_connection_set_auth_info_provider(ma_named_pipe_connection_t *self, struct ma_message_auth_info_provider_s *provider) {
    if (self) {
        self->auth_provider = provider;
    }
    return MA_OK;
}

ma_error_t ma_named_pipe_connection_set_passphrase_handler(ma_named_pipe_connection_t *self, struct ma_msgbus_passphrase_handler_s *passphrase_handler) {
	if (self) {
        self->passphrase_handler = passphrase_handler;
    }
    return MA_OK;

}
