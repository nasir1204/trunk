#include "ma/internal/core/msgbus/ma_msgbus_passphrase_handler_user.h"
#include "ma/internal/core/msgbus/ma_msgbus_defs.h"
#include "ma/internal/core/msgbus/ma_msgbus_private.h"

#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include "ma/internal/utils/conf/ma_conf_application.h"

#include "ma/ma_log.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>

struct ma_msgbus_passphrase_handler_user_s {
    
	ma_msgbus_passphrase_handler_t				base;

	ma_msgbus_passphrase_provider_callback_t	passphrase_provider_cb;

	void										*passphrase_provider_cb_data;	

	/*extra copy */
	ma_msgbus_t									*msgbus;

	ma_logger_t									*logger;
};

static void passphrase_handler_destroy(ma_msgbus_passphrase_handler_t *handler);
static ma_error_t passphrase_acquire(ma_msgbus_passphrase_handler_t *handler, char product_id[64],  unsigned char *passphrase, size_t *size);
static ma_error_t validate_passphrase(ma_msgbus_passphrase_handler_t *handler, char product_id[64], unsigned char *passphrase, size_t size, int *is_validated);

static ma_msgbus_passphrase_handler_functions_t passphrase_handler_methods = {
    &passphrase_handler_destroy,
	&passphrase_acquire,
    &validate_passphrase
};

ma_error_t ma_msgbus_passphrase_handler_user_create(ma_msgbus_t *msgbus, ma_msgbus_passphrase_handler_user_t **passphrase_handler) {
	if(msgbus && passphrase_handler) {
		ma_msgbus_passphrase_handler_user_t *self = (ma_msgbus_passphrase_handler_user_t *)calloc(1, sizeof(ma_msgbus_passphrase_handler_user_t));
		if(!self) return MA_ERROR_OUTOFMEMORY;
		ma_msgbus_passphrase_handler_init((ma_msgbus_passphrase_handler_t *)self, &passphrase_handler_methods, self);
		self->msgbus = msgbus;
		*passphrase_handler = self;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_msgbus_passphrase_handler_user_set_passphrase_callback(ma_msgbus_passphrase_handler_user_t *self, ma_msgbus_passphrase_provider_callback_t cb, void *cb_data) {
	if(self) {
		self->passphrase_provider_cb = cb ;
		self->passphrase_provider_cb_data = cb_data;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_msgbus_passphrase_handler_user_set_logger(ma_msgbus_passphrase_handler_user_t *self, ma_logger_t *logger) {
	if(self) {
		self->logger = logger;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

static void passphrase_handler_destroy(ma_msgbus_passphrase_handler_t *handler) {
	ma_msgbus_passphrase_handler_user_t *self = (ma_msgbus_passphrase_handler_user_t *)handler;
	if(self) free(self);
}

static ma_error_t passphrase_acquire(ma_msgbus_passphrase_handler_t *handler, char product_id[64], unsigned char *passphrase, size_t *passphrase_size) {
	ma_msgbus_passphrase_handler_user_t *self = (ma_msgbus_passphrase_handler_user_t *)handler;
	if(self && self->passphrase_provider_cb) {
	 	strncpy(product_id, ma_msgbus_get_product_id(self->msgbus), 63);	
		return self->passphrase_provider_cb(self->msgbus,self->passphrase_provider_cb_data, passphrase, passphrase_size);
	}
	if(passphrase_size) *passphrase_size  = 0;
	return MA_OK;
}

static void convert_to_upper(char *buffer, size_t size) {
	int i = 0;
	for(;i < size ; i++) 
		buffer[i] = toupper(buffer[i]);
}

static ma_error_t validate_passphrase(ma_msgbus_passphrase_handler_t *handler, char product_id[64], unsigned char *passphrase, size_t passphrase_size, int *is_validated) {
	ma_msgbus_passphrase_handler_user_t *self = (ma_msgbus_passphrase_handler_user_t *)handler;
	if(self && passphrase && passphrase_size && is_validated) {
		ma_temp_buffer_t hash_str_from_registry = MA_TEMP_BUFFER_INIT;
		ma_error_t rc = MA_OK;

		*is_validated = 0;
		if(MA_OK == (rc = ma_conf_application_get_property_str(product_id, MA_MSGBUS_PASSPHRASE_HASH_STR, &hash_str_from_registry))) {
			unsigned char passphrase_digest[32] = {0};
			if(MA_OK == (rc = ma_msgbus_passphrase_digest(self->msgbus, passphrase, passphrase_size, passphrase_digest))) {
				char hash_str_from_user[64] = {0};
				unsigned int i = 0;
				char *t = hash_str_from_user;

				/*convert it into string representation format */
				for(i = 0 ; i < 32; i++,t+=2) 
					sprintf(t,"%02x",passphrase_digest[i]);

				convert_to_upper((char *)ma_temp_buffer_get(&hash_str_from_registry), 64);
				convert_to_upper(hash_str_from_user, 64);
	
				if(0 == memcmp(hash_str_from_user, ma_temp_buffer_get(&hash_str_from_registry), 64)) 
					*is_validated = 1;
			}
		}else 
			MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "failed to get authorization hash, %d", rc);
            
		ma_temp_buffer_uninit(&hash_str_from_registry);
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}
