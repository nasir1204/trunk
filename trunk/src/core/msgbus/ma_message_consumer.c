#include "ma/internal/core/msgbus/ma_message_consumer.h"

/* you can disable reference counting (for static objects?) by setting the initial ref_count to a negative value. You must then clean-up it yourself by other means - or not. Don't use bio fuel for this */

ma_error_t ma_message_consumer_inc_ref(ma_message_consumer_t *self) {
    if (self && (0 <= self->ref_count)) {
        MA_ATOMIC_INCREMENT(self->ref_count);
    }
    return MA_OK;
}

ma_error_t ma_message_consumer_dec_ref(ma_message_consumer_t *self) {
    if (self && (0 <= self->ref_count)) {
        if (0 == MA_ATOMIC_DECREMENT(self->ref_count)) {
            self->methods->destroy(self);
        }
    }
    return MA_OK;
}

