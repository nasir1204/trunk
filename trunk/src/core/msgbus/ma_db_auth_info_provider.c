#include "ma/internal/core/msgbus/ma_db_auth_info_provider.h"
#include "ma/internal/utils/database/ma_db_statement.h"

#include <stdlib.h>
#include <string.h>


struct ma_db_auth_info_provider_s {
    ma_message_auth_info_provider_t base;

    ma_db_h db;

	ma_bool_t	is_db_set;	/* MA_TRUE -if set by caller, MA_FALSE - if it is internal */

    ma_logger_t *logger;

    /* buffer will typically be longer than 1, see ma_db_auth_info_provider_create() */
    char dp_path[1];
};

static void db_destroy(ma_message_auth_info_provider_h);

static ma_error_t db_decorate_auth_info(ma_message_auth_info_provider_h, ma_pid_t pid, ma_message_auth_info_t *auth_info);

ma_message_auth_info_provider_functions_t provider_methods = {
    &db_destroy,
    &db_decorate_auth_info
};


ma_error_t ma_db_auth_info_provider_create(const char *db_path, ma_db_auth_info_provider_h *provider) {
    ma_db_auth_info_provider_t *self = calloc(1, sizeof(ma_db_auth_info_provider_t) + strlen(db_path));/* allocate extra space for path */
    if (self) {
        ma_message_auth_info_provider_init(&self->base, &provider_methods, self);
        strcpy(self->dp_path, db_path);
        *provider = self;
        return MA_OK;
    }
    return MA_ERROR_OUTOFMEMORY;
}

ma_error_t ma_db_auth_info_provider_set_logger(ma_db_auth_info_provider_h self, ma_logger_t *logger) {
    if (self) {
        self->logger = logger;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_db_auth_info_provider_set_msgbus_db(ma_db_auth_info_provider_h self, struct ma_db_s *msgbus_db) {
	if (self && msgbus_db) {
		self->db = msgbus_db;
		self->is_db_set = MA_TRUE;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

void db_destroy(ma_message_auth_info_provider_h provider) {
    free(provider);
}

ma_error_t db_decorate_auth_info(ma_message_auth_info_provider_h provider, ma_pid_t pid, ma_message_auth_info_t *auth_info) {
    ma_db_auth_info_provider_t *self = provider->data;
    if (self && auth_info) {
        ma_error_t rc;
        ma_db_statement_h stmt = NULL;
        ma_db_recordset_h rs = NULL;

        char const *exe_path = NULL, *subject_name = NULL, *signer_name = NULL;

		if(!self->is_db_set) {
			if (MA_FAILED(rc = ma_db_open(self->dp_path, MA_DB_OPEN_READONLY, &self->db))) {
				MA_LOG(self->logger, MA_LOG_SEV_WARNING, "Unable to open auth db at <%s>, rc = <%ld>", self->dp_path, (long) rc);
				return rc;
			}
		}

        MA_SUCCEEDED(rc = ma_db_statement_create(self->db, "SELECT exe_path, subject_name, signer_name FROM auth_process_info WHERE pid = ?", &stmt)) &&
        MA_SUCCEEDED(rc = ma_db_statement_set_int(stmt, 1, pid)) &&
        MA_SUCCEEDED(rc = ma_db_statement_execute_query(stmt, &rs)) &&
        MA_SUCCEEDED(rc = ma_db_recordset_next(rs)) &&
        MA_SUCCEEDED(rc = ma_db_recordset_get_string(rs, 1, &exe_path)) &&
        MA_SUCCEEDED(rc = ma_db_recordset_get_string(rs, 2, &subject_name)) &&
        MA_SUCCEEDED(rc = ma_db_recordset_get_string(rs, 3, &signer_name));
        
        if (MA_SUCCEEDED(rc)) {
            ma_error_t rc_p = ma_message_auth_info_set_attribute(auth_info, MA_MSGBUS_AUTH_INFO_ATTRIBUTE_VERIFIED_PUBLISHER, subject_name);
            ma_error_t rc_s = ma_message_auth_info_set_attribute(auth_info, MA_MSGBUS_AUTH_INFO_ATTRIBUTE_VERIFIED_SIGNER, signer_name);
            if (MA_SUCCEEDED(rc_p) && MA_SUCCEEDED(rc_s)) {
                MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Successfully set publisher: <%s>, signer: <%s>", subject_name, signer_name);
            } else {
                rc = MA_FAILED(rc_p) ? rc_p : rc_s;
                MA_LOG(self->logger, MA_LOG_SEV_WARNING, "Failed setting publisher/signer: publisher(<%s>, rc=<%ld>), signer(<%s>, rc=<%ld>)", subject_name, rc_p, signer_name, rc_s);
            }

        } else {
            MA_LOG(self->logger, MA_LOG_SEV_WARNING, "Error retrieving auth info from db for pid = <%u>, rc=<%ld>", (unsigned) pid, (long) rc);
        }
		
		ma_db_recordset_release(rs);
        ma_db_statement_release(stmt);
		
		if(!self->is_db_set)
			ma_db_close(self->db), 	self->db = NULL;
		
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}
