#include "ma/internal/core/msgbus/ma_local_broker.h"
#include "ma/internal/core/msgbus/ma_message_consumer.h"
#include "ma/internal/defs/ma_name_service_defs.h"
#include "ma/internal/defs/ma_pubsub_service_defs.h"
#include "ma/internal/core/msgbus/ma_message_header.h"
#include "ma/internal/core/msgbus/ma_ep_lookup.h" 
#include "ma/internal/core/msgbus/ma_named_pipe_server.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/core/msgbus/ma_msgbus_defs.h"


#include "ma/ma_msgbus.h"
#include "ma/internal/core/msgbus/ma_msgbus_private.h"

#include <stdlib.h>
#include <assert.h>
#include <stdio.h> /* sprintf */
#include <string.h>

#ifdef MA_WINDOWS
#include <process.h>
#else
#include <unistd.h>
#endif

const static char local_broker_name_template[] = "ma.local.broker.internal.%08X";
static char       local_broker_name_buffer[8+sizeof(local_broker_name_template)];

/* Consumer Status of registration of external broker */
typedef enum ma_message_consumer_status_e {
    MA_MSGBUS_CONSUMER_UNKNOWN_STATUS   = 0,
	MA_MSGBUS_CONSUMER_REGISTERED,   
	MA_MSGBUS_CONSUMER_REGISTRATION_PENDING,
    MA_MSGBUS_CONSUMER_UNREGISTRED,	/* No entry with this status because, entry must be deletedby that time. it's just for status */
	MA_MSGBUS_CONSUMER_UNREGISTRATION_PENDING,
} ma_message_consumer_status_t;

/* set of services and subscribers consumers for outproc, these must be synced with external broker */ 
typedef struct ma_local_broker_list_entry_s {
    ngx_queue_t						qlink;

    ma_message_consumer_t			*consumer;
	
	char							*consumer_name; /* service name or topic filter name */

    ma_bool_t						is_service;

	ma_message_consumer_status_t	consumer_reg_status;

    ma_message_consumer_status_t	consumer_unreg_status;

	ma_message_t					*request_msg;

	ma_bool_t						is_registration;

	ma_local_broker_t               *self;	

} ma_local_broker_list_entry_t;

struct ma_local_broker_s {	 
    /*! It is a message consumer -- Please keep as first field in this struct */
    ma_message_consumer_t       consumer_impl;

    ma_msgbus_t                 *mbus;

    ma_message_router_t         *router;  

    ma_named_pipe_server_t      *pipe_server;

    const char                  *server_pipe_name;

    ma_bool_t                   is_pipe_server_running;

    ma_logger_t                 *logger;

	ngx_queue_t		            qhead;	
};

static void forward_pipe_terminate_message_to_broker(ma_local_broker_t *self, ma_message_t *msg);

static ma_error_t consumer_on_message(ma_message_consumer_t *consumer, ma_message_t *msg) {    
	ma_local_broker_t *self = (ma_local_broker_t *) consumer ;

    //Filter the messages only which we send 
	if( msg && ((MA_MESSAGE_TYPE_REPLY == GET_MESSAGE_TYPE(msg)) 			
			&& (0 == strcmp(GET_MESSAGE_DESTINATION_NAME(msg) , local_broker_name_buffer)))) {
		const char *value = NULL;
        ma_error_t status = (ma_error_t)GET_MESSAGE_STATUS(msg);

        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Received response from broker with status <%d>", status);

        if( (MA_OK == ma_message_get_property(msg, MA_PROP_KEY_NAME_SERVICE_REPLY_TYPE_STR, &value))
			|| (MA_OK == ma_message_get_property(msg, MA_PROP_KEY_PUBSUB_SERVICE_REPLY_TYPE_STR, &value))) {                      

            MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Received response type <%s> from broker", value);
           
            if(!strcmp(MA_PROP_VALUE_REGISTER_SERVICE_REQUEST_STR, value) || !strcmp(MA_PROP_VALUE_UNREGISTER_SERVICE_REQUEST_STR, value)
				|| !strcmp(MA_PROP_VALUE_REGISTER_SUBSCRIBER_REQUEST_STR, value) || !strcmp(MA_PROP_VALUE_UNREGISTER_SUBSCRIBER_REQUEST_STR, value))
                MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Not expecting any content in reply from broker for register and unregister requests");
            else
                MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Unknown Response type <%s> from broker", value);                     
        }		
        return status;
	}
    else if(MA_MESSAGE_TYPE_SIGNAL == GET_MESSAGE_TYPE(msg)) {			
	    const char *value = NULL;
	    if((MA_OK == ma_message_get_property(msg, MA_PROP_KEY_PIPE_NAME_STR, &value)) && value) { 
            if(self->server_pipe_name && !strncmp(value, self->server_pipe_name, strlen(self->server_pipe_name))) {
			    ma_error_t status = (ma_error_t)GET_MESSAGE_STATUS(msg);
			    if(MA_ERROR_MSGBUS_SERVICE_CONNECTION_TERMINATED == status) {
				    ma_named_pipe_connection_t *connection = NULL;
                    (void) forward_pipe_terminate_message_to_broker(self, msg);
				    if(MA_OK == ma_named_pipe_connection_manager_search(ma_msgbus_get_named_pipe_connection_manager(self->mbus), value, (ma_message_consumer_t **)&connection)) { 
                        ma_message_router_remove_consumer(self->router, (ma_message_consumer_t *)connection);
					    if(MA_OK == ma_named_pipe_connection_manager_remove(ma_msgbus_get_named_pipe_connection_manager(self->mbus), connection))
						    (void)ma_named_pipe_connection_close(connection , 0);
                        ma_named_pipe_connection_release(connection); /* This ref decrement corresponding to the add ref in search function */
                        ma_named_pipe_connection_release(connection); /* This ref decrement corresponding to the new connection object from named pipe server */
				    }
			    }
		    }
	    }
    }
    return MA_OK;
}

static void consumer_destroy(ma_message_consumer_t *base) {
    ma_local_broker_t *self = (ma_local_broker_t *) base;
    if(self) {	
        if(self->pipe_server)
            ma_named_pipe_server_release(self->pipe_server);
        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "local broker destroyed");
        free(self);
	}  
}

static const ma_message_consumer_methods_t consumer_methods = {
    &consumer_on_message,
    &consumer_destroy
};

ma_error_t ma_local_broker_create(ma_event_loop_t *ma_loop, ma_message_router_t *router, ma_local_broker_t **broker) {        
	if(broker) {   	    	 	   
        ma_error_t rc = MA_OK; 
        ma_named_pipe_server_t *pipe_server = NULL;
        ma_local_broker_t *self = NULL;

        if(MA_OK != (rc = ma_named_pipe_server_create(ma_loop, &pipe_server)))  /* create the server, but don't start it yet */
            return MA_ERROR_MSGBUS_PIPE_SERVER_CREATE_FAILED;
        
        
        self = (ma_local_broker_t *)calloc(1, sizeof(ma_local_broker_t));
		if(self) { 	
            self->router = router;
            self->pipe_server = pipe_server;
            ma_message_consumer_init(&self->consumer_impl, &consumer_methods, 1, self);
			ngx_queue_init(&self->qhead);
			*broker = self;            
            sprintf(local_broker_name_buffer, local_broker_name_template, MA_WIN_SELECT(_getpid(), getpid())); /*adding pid in name buffer */
            return MA_OK;
		}
		return MA_ERROR_OUTOFMEMORY ;              
	}
	return MA_ERROR_INVALIDARG;	   
}

static ma_error_t register_unregister_subscriber_with_pubsub_service(ma_local_broker_t *self,  ma_local_broker_list_entry_t *entry);
static ma_error_t register_unregister_service_with_name_service(ma_local_broker_t *self, ma_local_broker_list_entry_t *entry);

ma_error_t ma_local_broker_start(ma_msgbus_t *mbus, ma_local_broker_t *self) {
    if(self) {        
        self->mbus = mbus;        
        /* Local broker is added into router, it waits for responses from ma broker */
        return ma_message_router_add_consumer(self->router, (ma_message_consumer_t *) self, NULL, MA_MSGBUS_LOCAL_BROKER);		    
    }
	return MA_ERROR_INVALIDARG;	   
}

ma_error_t ma_local_broker_check_registrations(ma_local_broker_t *self) {
	if(self) {
		ma_error_t rc = MA_OK;
		ngx_queue_t  *ngx_item = NULL;		
		MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Trying to register/unregister pending services/subscribers");
		ngx_queue_foreach(ngx_item, &self->qhead)  {
			ma_local_broker_list_entry_t *entry = ngx_queue_data(ngx_item, ma_local_broker_list_entry_t, qlink);        
			if(entry ) {
                if(MA_MSGBUS_CONSUMER_REGISTRATION_PENDING == entry->consumer_reg_status) {
                    entry->is_registration = MA_TRUE;
				    rc = (entry->is_service) ? register_unregister_service_with_name_service(self, entry): register_unregister_subscriber_with_pubsub_service(self, entry);
                }
                else {
                    if(MA_MSGBUS_CONSUMER_UNREGISTRATION_PENDING == entry->consumer_unreg_status) {
                        entry->is_registration = MA_FALSE;
                        rc = (entry->is_service) ? register_unregister_service_with_name_service(self, entry): register_unregister_subscriber_with_pubsub_service(self, entry);
                    }
                }
            }
		}
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_local_broker_stop(ma_local_broker_t *self) {
    if(self) {
		if(self->is_pipe_server_running && (MA_OK == ma_named_pipe_server_stop(self->pipe_server)))
            MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Named pipe server stopped");
		
        return ma_message_router_remove_consumer(self->router , (ma_message_consumer_t *) self);
    }
	return MA_ERROR_INVALIDARG;	   
}

void ma_local_broker_list_entry_cleanup(ma_local_broker_list_entry_t *entry) {
    if(entry) {
	    if(entry->consumer_name) free(entry->consumer_name);
	    if(entry->request_msg) ma_message_release(entry->request_msg);
        free(entry);
    }
}
ma_error_t ma_local_broker_release(ma_local_broker_t *self) {	
	if(self) {
       MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "local broker Releases");
       /* No items pending in this queue at this point.. but trying to clean-up if any */
       while (!ngx_queue_empty(&self->qhead)) {
            ngx_queue_t *ngx_item = ngx_queue_head(&self->qhead);
            ma_local_broker_list_entry_t *entry = ngx_queue_data(ngx_item, ma_local_broker_list_entry_t, qlink);
            ngx_queue_remove(ngx_item);            
			ma_local_broker_list_entry_cleanup(entry);							
       }
       ma_message_consumer_dec_ref(&self->consumer_impl);       
       return MA_OK;		
    }
    return MA_ERROR_INVALIDARG;    
}

/* This callback can be used to send the Service/Subscriber reg/unreg messages to the name/pubsub service */ 
static void broker_lookup_cb(ma_error_t status, ma_message_consumer_t *broker_consumer, void *broker_cookie, void *cb_data) {
    ma_local_broker_list_entry_t *entry = (ma_local_broker_list_entry_t *)cb_data;   
    ma_local_broker_t *self = (ma_local_broker_t *)entry->self;
	
    if(MA_OK == status) {		
		ma_message_consumer_send(broker_consumer, entry->request_msg); /* sending reg/unreg request meesage directly to the name service endpoint consumer */
        ma_message_release(entry->request_msg); entry->request_msg = NULL;

        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Local broker lookup cb received status <%d>", status);
        if(MA_MSGBUS_CONSUMER_REGISTRATION_PENDING == entry->consumer_reg_status) {
            MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Local broker lookup cb execution on service/subscriber <%s> registration", entry->consumer_name);
			entry->consumer_reg_status = MA_MSGBUS_CONSUMER_REGISTERED;
		}
		else {
            if(MA_MSGBUS_CONSUMER_UNREGISTRATION_PENDING == entry->consumer_unreg_status) {
                MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Local broker lookup cb execution on service/subscriber <%s> unregistration", entry->consumer_name);	
                entry->consumer_unreg_status = MA_MSGBUS_CONSUMER_UNREGISTRED;
                if(broker_cookie) {
					ma_ep_lookup_stop((ma_ep_lookup_t *)broker_consumer, broker_cookie);
				}

				ma_message_consumer_release(broker_consumer);
	
                ma_local_broker_list_entry_cleanup(entry);			
            }
		}		
    }
	else {
        if(MA_MSGBUS_CONSUMER_REGISTRATION_PENDING == entry->consumer_reg_status || MA_MSGBUS_CONSUMER_REGISTERED == entry->consumer_reg_status) {
            if(broker_cookie) {
				ma_ep_lookup_stop((ma_ep_lookup_t *)broker_consumer, broker_cookie);				
			}

            entry->consumer_reg_status = MA_MSGBUS_CONSUMER_REGISTRATION_PENDING;
        }
      
        ma_message_consumer_release(broker_consumer);
	}
}

static ma_error_t register_unregister_subscriber_with_pubsub_service(ma_local_broker_t *self,  ma_local_broker_list_entry_t *entry) {
    ma_error_t rc = MA_OK ;
    ma_message_t *reg_unreg_msg = NULL;        

    if(MA_OK == (rc = ma_message_create(&reg_unreg_msg))) {  

        SET_MESSAGE_TYPE(reg_unreg_msg, MA_MESSAGE_TYPE_REQUEST);
        SET_MESSAGE_SOURCE_NAME(reg_unreg_msg, local_broker_name_buffer);				    
        SET_MESSAGE_DESTINATION_NAME(reg_unreg_msg , MA_MSGBUS_PUBSUB_SERVICE_NAME_STR);
        SET_MESSAGE_DESTINATION_PID(reg_unreg_msg , ma_msgbus_get_broker_pid(self->mbus));
		SET_MESSAGE_SOURCE_PID(reg_unreg_msg, getpid());

		ma_message_set_property(reg_unreg_msg , MA_PROP_KEY_PUBSUB_SERVICE_REQUEST_TYPE_STR , entry->is_registration ? MA_PROP_VALUE_REGISTER_SUBSCRIBER_REQUEST_STR : MA_PROP_VALUE_UNREGISTER_SUBSCRIBER_REQUEST_STR);
		ma_message_set_property(reg_unreg_msg , MA_PROP_KEY_SUBSCRIBER_TOPIC_STR , entry->consumer_name);
		ma_message_set_property(reg_unreg_msg , MA_PROP_KEY_PRODUCT_ID_STR , ma_msgbus_get_product_id(self->mbus));
        ma_message_set_property(reg_unreg_msg , MA_PROP_KEY_SERVICE_ID_STR , ((self->server_pipe_name)? self->server_pipe_name:"0"));
		ma_message_set_property(reg_unreg_msg , MA_PROP_KEY_HOST_NAME_STR , "localhost"); /* TBD - Will set actual hostname */
        ma_message_set_property(reg_unreg_msg , MA_PROP_KEY_SKIP_RESPONSE_STR , entry->is_registration ? "0" : "1");

		MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Message created for %s of <%s> subscriber", (entry->is_registration?"Registration":"Unregistration"), entry->consumer_name);
		entry->request_msg = reg_unreg_msg;

        if(MA_OK != (rc = ma_ep_lookup_start(self->mbus, MA_MSGBUS_PUBSUB_SERVICE_NAME_STR, MSGBUS_CONSUMER_REACH_OUTPROC, broker_lookup_cb, entry)))
		{
            MA_LOG(self->logger, MA_LOG_SEV_TRACE, "Subscriber <%s> %s is not done as broker information is not available, however it works for inproc clients", entry->consumer_name, (entry->is_registration?"Registration":"Unregistration"));
			ma_message_release(reg_unreg_msg);
		}
    }
    else
        MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to create reg/unreg request message for <%s>", entry->consumer_name);

	return rc;
}


static ma_error_t register_unregister_service_with_name_service(ma_local_broker_t *self, ma_local_broker_list_entry_t *entry) {
    ma_error_t rc = MA_OK ;

     ma_message_t *reg_unreg_msg = NULL;        

    if(MA_OK == (rc = ma_message_create(&reg_unreg_msg))) {  

        SET_MESSAGE_TYPE(reg_unreg_msg, MA_MESSAGE_TYPE_REQUEST);
        SET_MESSAGE_SOURCE_NAME(reg_unreg_msg, local_broker_name_buffer);				    
        SET_MESSAGE_DESTINATION_NAME(reg_unreg_msg , MA_MSGBUS_NAME_SERVICE_NAME_STR);
        /* If registration is happening for name service itself, igonore the get pid call as the pid is not available in config.ini */
        SET_MESSAGE_DESTINATION_PID(reg_unreg_msg , strcmp(entry->consumer_name, MA_MSGBUS_NAME_SERVICE_NAME_STR) ? ma_msgbus_get_broker_pid(self->mbus) : -1);
		SET_MESSAGE_SOURCE_PID(reg_unreg_msg, getpid());

		ma_message_set_property(reg_unreg_msg , MA_PROP_KEY_SERVICE_NAME_STR , entry->consumer_name);
		ma_message_set_property(reg_unreg_msg , MA_PROP_KEY_NAME_SERVICE_REQUEST_TYPE_STR , entry->is_registration ? MA_PROP_VALUE_REGISTER_SERVICE_REQUEST_STR : MA_PROP_VALUE_UNREGISTER_SERVICE_REQUEST_STR);
		ma_message_set_property(reg_unreg_msg , MA_PROP_KEY_PRODUCT_ID_STR , ma_msgbus_get_product_id(self->mbus));
        ma_message_set_property(reg_unreg_msg , MA_PROP_KEY_SERVICE_ID_STR , self->server_pipe_name);
		ma_message_set_property(reg_unreg_msg , MA_PROP_KEY_HOST_NAME_STR , "localhost"); /* TBD - Will set actual hostname */
        ma_message_set_property(reg_unreg_msg , MA_PROP_KEY_SKIP_RESPONSE_STR , entry->is_registration ? "0" : "1");

		entry->request_msg = reg_unreg_msg;
		MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Message created for %s of <%s> service", (entry->is_registration?"Registration":"Unregistration"), entry->consumer_name);
            
        if(MA_OK != (rc = ma_ep_lookup_start(self->mbus, MA_MSGBUS_NAME_SERVICE_NAME_STR, MSGBUS_CONSUMER_REACH_OUTPROC, broker_lookup_cb, entry)))
		{
			MA_LOG(self->logger, MA_LOG_SEV_TRACE, "Service <%s> %s is not done as broker information is not available, however it works for inproc clients", entry->consumer_name, (entry->is_registration?"Registration":"Unregistration"));
			ma_message_release(reg_unreg_msg);
		}
    }
    else
        MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to create reg/unreg request message for <%s>", entry->consumer_name);

	return rc;
}

static void new_connection_cb(ma_named_pipe_connection_t *connection, void *cb_data) {
    ma_local_broker_t *self = (ma_local_broker_t *)cb_data;
    char pipe_name[MAX_PIPENAME_LEN];
    pipe_name[MA_COUNTOF(pipe_name)-1] = 0;  
        
    /* creating proxy of server pipe name with connection object address suffix ( i.e, <server_pipe_name_<conneciton object address>)
       to add new unique entry in connection manager for each new connection. Anyhow we clean-up these conneciton objects when other end terminates.
    */  

    MA_MSC_CONCAT(_,snprintf)(pipe_name, MA_COUNTOF(pipe_name)-1, "%s_%p", self->server_pipe_name, connection);

    MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Adding new accepted connection object with pipe name <%s>", pipe_name);

    ma_named_pipe_connection_manager_add(ma_msgbus_get_named_pipe_connection_manager(self->mbus), pipe_name , connection);

    /* connect the router and new connection to each other */
    ma_message_router_add_consumer(self->router, (ma_message_consumer_t*)  connection, NULL, MA_MSGBUS_NAMED_PIPE_CONNECTION);

    ma_named_pipe_connection_set_consumer(connection, (ma_message_consumer_t*) self->router);

    ma_named_pipe_connection_set_pipe_name(connection, pipe_name);

    /* don't forget to start processing incoming bytes */
    ma_named_pipe_connection_start_receiving(connection);
}

static const char name_template[] = "ma_5bd9fa52-9d71-e8fd-20b0-306ab91d3db1_%lu.%p"; /* system unique pid X pointer value */

static ma_error_t named_pipe_server_start(ma_local_broker_t *self) {
    ma_error_t rc = MA_OK;
    char pipe_name[MAX_PIPENAME_LEN];
    
    pipe_name[MA_COUNTOF(pipe_name)-1] = 0;
    MA_MSC_CONCAT(_,snprintf)(pipe_name, MA_COUNTOF(pipe_name)-1, name_template, (unsigned long) getpid(), self->pipe_server);
    ma_named_pipe_server_set_auth_info_provider(self->pipe_server, ma_msgbus_get_auth_info_provider(self->mbus));
	ma_named_pipe_server_set_passphrase_handler(self->pipe_server, ma_msgbus_get_passphrase_handler(self->mbus));
    if(MA_OK == ( rc = ma_named_pipe_server_start(self->pipe_server, ma_msgbus_get_crypto_provider(self->mbus), pipe_name, &new_connection_cb, self))) {
        if(MA_OK == (rc = ma_named_pipe_server_get_pipe_name(self->pipe_server, &self->server_pipe_name)))
        self->is_pipe_server_running = MA_TRUE;
    }
    return rc;
}

ma_error_t ma_local_broker_consumer_register(ma_local_broker_t *self, ma_message_consumer_t *ep_consumer, const char *ep_name , ma_msgbus_consumer_reachability_t reachability, ma_bool_t is_service) {	
	if(self && ep_consumer && ep_name) {      
        ma_error_t rc = MA_OK;             

        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "%s <%s> Registration", (is_service) ? "Service" : "Subscriber",  ep_name);

        ma_message_router_add_consumer(self->router, ep_consumer, ep_name /* topic filter name for subscribers */, (is_service) ? MA_MSGBUS_SERVER : MA_MSGBUS_SUBSCRIBER);

        if(MSGBUS_CONSUMER_REACH_INPROC != reachability) { 
			ma_local_broker_list_entry_t *entry = NULL;
            /* start named pipe server only when any server register first time with outproc reachability */
            if(is_service && !self->is_pipe_server_running) {
                if(MA_OK == (rc = named_pipe_server_start(self))) {              
                    MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "named pipe server started from local broker");
                }
                else {                        
                    ma_message_router_remove_consumer(self->router, ep_consumer);
                    MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Failed to start named pipe server");
                    return rc;
                }
            }
			
			entry = (ma_local_broker_list_entry_t *) calloc(1, sizeof(ma_local_broker_list_entry_t));
			if (entry) {				
				entry->consumer_name = strdup(ep_name);  /* topic filter name for subscribers */
				entry->consumer_reg_status = MA_MSGBUS_CONSUMER_REGISTRATION_PENDING;
                entry->consumer_unreg_status = MA_MSGBUS_CONSUMER_UNKNOWN_STATUS;
				entry->is_service = is_service;
				entry->is_registration = MA_TRUE;
				entry->consumer = ep_consumer; /* incremented ref count when add this into router. so no need to inc for this */
                entry->self = self;
				ngx_queue_insert_tail(&self->qhead, &entry->qlink);				
			}
			else
				return MA_ERROR_OUTOFMEMORY;

			rc = (is_service) ? register_unregister_service_with_name_service(self, entry) : register_unregister_subscriber_with_pubsub_service(self, entry) ;
						
        }
		/* TBD - Should we return the outproc registration as Failure */
        return MA_OK;       
	}
	return MA_ERROR_INVALIDARG ;	
}

ma_error_t ma_local_broker_consumer_unregister(ma_local_broker_t *self, ma_message_consumer_t *ep_consumer, const char *ep_name, ma_msgbus_consumer_reachability_t reachability, ma_bool_t is_service) {	
	if(self && ep_consumer && ep_name) {
		ma_error_t rc = MA_OK;

        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "%s <%s> Unregistration", (is_service) ? "Service" : "Subscriber", ep_name);

        if(MSGBUS_CONSUMER_REACH_INPROC != reachability) {
			ngx_queue_t  *ngx_item = NULL;

            ngx_queue_foreach(ngx_item, &self->qhead)  {
				ma_local_broker_list_entry_t *entry = ngx_queue_data(ngx_item, ma_local_broker_list_entry_t, qlink);
				if (entry && (entry->consumer == ep_consumer)) {
					entry->is_registration = MA_FALSE;
					entry->is_service = is_service;
					entry->consumer_unreg_status = MA_MSGBUS_CONSUMER_UNREGISTRATION_PENDING;
                    entry->self = self;
					ngx_queue_remove(ngx_item);
					rc = (is_service) ? register_unregister_service_with_name_service(self, entry) : register_unregister_subscriber_with_pubsub_service(self, entry);				
					break;
				}
			}
		}

		ma_message_router_remove_consumer(self->router, ep_consumer);
		
		/* TBD - Should we return the outproc unregistration as Failure */
        return MA_OK;     
	}
	return MA_ERROR_INVALIDARG;	
}

ma_error_t ma_local_broker_set_logger(ma_local_broker_t *self, ma_logger_t *logger) {
    if(self && logger) {
        self->logger = logger;
        ma_named_pipe_server_set_logger(self->pipe_server, logger);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


static void forward_pipe_terminate_message_to_broker(ma_local_broker_t *self, ma_message_t *msg) {
    const char *peer_pid_str = NULL;
    if(MA_OK == ma_message_get_property(msg, MA_PROP_KEY_PEER_PID_STR, &peer_pid_str)) {
        ma_message_t *fw_msg = NULL;
        if(MA_OK == ma_message_create(&fw_msg)) {
            ma_message_set_property(fw_msg, MA_PROP_KEY_PEER_PID_STR, peer_pid_str);
            (void) ma_msgbus_publish(self->mbus, MA_MSGBUS_PEER_END_TERMINATION_PUB_SUB_TOPIC, MSGBUS_CONSUMER_REACH_INPROC, fw_msg);
            ma_message_release(fw_msg);
        }
    }
}