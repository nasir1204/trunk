#include "ma_vscore_vtp.h"
#include "ma/internal/utils/text/ma_utf8.h"

#ifndef _WIN32
# error  "This file should be compiled only for Windows"
#endif

#include <windows.h>
#include "mfevtpa.h" 

#include <memory>

/* Custom deleter class for any VSCore interface as per std::unique_ptr */
template <class T>
struct VsCoreDeleter {
    void operator ()(T *self) const {
        if (self) self->Release();
    }
};

typedef std::unique_ptr<VtpValidationControl, VsCoreDeleter<VtpValidationControl> > VtpValidationControlPtr;
typedef std::unique_ptr<VtpServiceReport, VsCoreDeleter<VtpServiceReport> > VtpServiceReportPtr;

/* now define this as a C++ class */
struct ma_vscore_vtp_s {

    bool init();

    int validate_process_ex(unsigned long, ma_vscore_vtp_validate_process_ex_cb cb, void *cb_data);

    VtpValidationControlPtr vtp_control;
};


struct ma_vscore_vtp_module_s {

    ma_vscore_vtp_module_s(VtpBaseReportData *rpd) : report_data(rpd), name(nullptr), signer(nullptr) {
    }

    ~ma_vscore_vtp_module_s() {
        delete [] name;
    }

    VtpBaseReportData *report_data;

    char *name, *signer; // cached names
};

bool ma_vscore_vtp_s::init() {
    if (!vtp_control) {
        VtpValidationControl *v(nullptr);
        if (ERROR_SUCCESS == ::VtpValidationControlCreate(&v)) {
            vtp_control.reset(v);
        }
    }
    return !!vtp_control;
}

int ma_vscore_vtp_s::validate_process_ex(unsigned long pid, ma_vscore_vtp_validate_process_ex_cb cb, void *cb_data) {
    if (init()) {
        VTP_TASKFLAG flags(VTP_TASKFLAG_RETURN_ALL_FOUND);
        VtpServiceReport *service_report = nullptr;
        if (ERROR_SUCCESS == vtp_control->ValidateProcessEx(pid, flags, &service_report)) {
            VtpServiceReportPtr svc_report_ptr(service_report);
            if (cb) for (VtpBaseReportData *report_data(0); report_data = svc_report_ptr->GetNextReport(report_data); report_data) {
                ma_vscore_vtp_module_s module(report_data);
                
                if (ma_error_t err = cb(&module, cb_data)) return err;
                
                report_data->vtpTrustResult,
                report_data->moduleName, 
                report_data->moduleSigner,

                report_data->entryPoint;
            }
        }
        return 0;
    }

    return 1; /* or some better error */
}


extern "C" ma_error_t ma_vscore_vtp_create(ma_vscore_vtp_t **vtp) {
    ma_vscore_vtp_t *self = new ma_vscore_vtp_t;
    if (self) {

        *vtp = self;
        return MA_OK;
        
    }
    return MA_ERROR_OUTOFMEMORY;
}

ma_error_t ma_vscore_vtp_release(ma_vscore_vtp_t *vtp) {
    delete vtp;
    return MA_OK;
}

ma_error_t ma_vscore_vtp_validate_process_ex(ma_vscore_vtp_t *vtp, /* USE ma_pid_t */unsigned long  pid, ma_vscore_vtp_validate_process_ex_cb cb, void *cb_data ) {
    if (vtp) {
        return (ma_error_t) vtp->validate_process_ex(pid, cb, cb_data);
    }

    return MA_ERROR_INVALIDARG;
}




const char *ma_vscore_vtp_module_get_name(ma_vscore_vtp_module_h module) {
    /* function of &module->name, module->report_data->moduleName with new operator */
    if (module) {
        if (!module->name) {
            int len = ma_wide_char_to_utf8(NULL,0,module->report_data->moduleName,-1);
            if (len) {
                if (nullptr != (module->name = new (std::nothrow) char[1+len])) {
                    ma_wide_char_to_utf8(module->name, 1+len, module->report_data->moduleName, -1);
                }
            }
        }
        return module->name;
    }
    return NULL;
}

const char *ma_vscore_vtp_module_get_signer(ma_vscore_vtp_module_h module) {
    if (module) {
        if (!module->signer) {
            int len = ma_wide_char_to_utf8(NULL, 0, module->report_data->moduleSigner, -1);
            if (len) {
                if (nullptr != (module->signer = new (std::nothrow) char[len])) {
                    ma_wide_char_to_utf8(module->signer, 1+len, module->report_data->moduleSigner, -1);
                }
            }
        }
        return module->signer;
    }
    return NULL;
}

ma_bool_t ma_vscore_vtp_module_is_main_executable(ma_vscore_vtp_module_h module) {
    return module && module->report_data->entryPoint;
}

ma_bool_t ma_vscore_vtp_module_is_trusted(ma_vscore_vtp_module_h module) {
    return !!(module->report_data->vtpTrustResult.validation_state & VALIDATION_STATE_VALID) && !!module->report_data->vtpTrustResult.privileges;
}
