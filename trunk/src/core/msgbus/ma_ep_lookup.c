#include "ma/internal/core/msgbus/ma_ep_lookup.h"
#include "ma/internal/core/msgbus/ma_message_header.h"
#include "ma/internal/defs/ma_name_service_defs.h"
#include "ma/internal/defs/ma_pubsub_service_defs.h"
#include "ma/internal/ma_macros.h"

#include "ma/internal/core/msgbus/ma_msgbus_private.h"

#include <stdlib.h>
#include <assert.h>
#include <stdio.h> /* sprintf */
#include <string.h>

#ifndef MA_WINDOWS
    #include <unistd.h>
#endif

static ma_uint32_t g_correlation_id = 0;
const static char ephemeral_name_template[] = "ma.ep.lookup.internal.%08X.%lu";

typedef struct ep_lookup_info_s {
    
    char        *ep_name;
	
    ma_int64_t  pid;
	
    char		*pipe_name ;
	
    char		*host_name ;

    ma_error_t  status;

} ep_lookup_info_t ;

typedef struct ma_lookup_cb_entry_s {
    
    ngx_queue_t     qlink;

    ep_lookup_cb    cb;

    void            *cb_data;

} ma_lookup_cb_entry_t ;

enum ma_ep_lookup_status_e {
    EP_NOT_CONNECTED,
    EP_CONNECTING,
    EP_CONNECTED,
};

typedef enum ma_ep_lookup_status_e ma_ep_lookup_status_t;

struct ma_ep_lookup_s {
    /*! It is a message consumer -- Please keep as first field in this struct */
    ma_message_consumer_t       consumer_impl;

    ma_msgbus_t                 *mbus;

    /*! this objects ephemeral name */
    char                        ephemeral_name_buffer[8+8+sizeof(ephemeral_name_template)];    

    /*! CORRELATION ID of this lookup object */
    ma_uint32_t                 correlation_id;    

    ep_lookup_info_t            lookup_info;

    ma_ep_lookup_status_t       lookup_status;

    ma_named_pipe_connection_t  *connection;

    ma_message_t                *request_message;

    /*! This variable is used to point to broker consumer in the process of retriving the pipe name from broker */
    ma_message_consumer_t       *broker_consumer;
    void                        *broker_cookie;

    /*! list of clients intersted on this connection */
    ngx_queue_t                 qhead;

	uv_timer_t					destroy_timer;

	ma_bool_t					is_destroy_started;
};

// This data structure is used to pass the data to timer callback in calling lookup callback in async way..
typedef struct async_call_data_s {

    ma_error_t                 status;

    ep_lookup_cb               cb;

    void                       *cb_data;

    ma_message_consumer_t      *ep_consumer;

    void                        *ep_cookie;
} async_call_data_t;

static ma_error_t resolve_start(ma_ep_lookup_t *self);
static void destroy_ep_lookup_asynchronously(ma_ep_lookup_t *self);

static void set_lookup_info(ep_lookup_info_t *lookup_info, ma_error_t status, ma_int64_t pid , const char *pipe_name , const char *host_name) {
    if(lookup_info) {
        lookup_info->status = status;
        lookup_info->pid = pid ;
        lookup_info->pipe_name = pipe_name ? strdup(pipe_name) : NULL;
        lookup_info->host_name = host_name ? strdup(host_name) : NULL;        
    }
}

static void timer_close_cb(uv_handle_t *handle ) {
	if(handle)	free(handle);
}

static void timer_cb(uv_timer_t* handle, int status) {
	async_call_data_t *async_call_data = (async_call_data_t *)handle->data;	
    
	uv_close((uv_handle_t *)handle, timer_close_cb); 
    
    /* MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "Calling lookup callback in timer callback");*/
    if(async_call_data && async_call_data->cb) {
        async_call_data->cb(async_call_data->status, async_call_data->ep_consumer, async_call_data->ep_cookie, async_call_data->cb_data);         
        free(async_call_data);
    }    
}

static void call_lookup_callback_async_way(ma_msgbus_t *mbus, ma_error_t status, ep_lookup_cb cb, void *cb_data, ma_message_consumer_t *ep_consumer, void *ep_cookie) {    
    if(mbus && cb) {
        uv_loop_t *uv_loop = NULL ; 	
	    uv_timer_t *uv_timer = NULL ;
        async_call_data_t *async_call_data = NULL;
        
        MA_LOG(ma_msgbus_get_logger(mbus), MA_LOG_SEV_DEBUG, "Calling lookup callback in async way with status <%d> from ep lookup <%p>", status, ep_consumer);

	    uv_timer = (uv_timer_t *)calloc(1 , sizeof(uv_timer_t));
        if(!uv_timer) {
            cb(MA_ERROR_OUTOFMEMORY, ep_consumer, ep_cookie, cb_data);
		    return ;
	    }

        async_call_data = (async_call_data_t *)calloc(1 , sizeof(async_call_data_t));
         if(!async_call_data) {
            cb(MA_ERROR_OUTOFMEMORY, ep_consumer, ep_cookie, cb_data);
            free(uv_timer);
		    return ;
	    }
        ma_event_loop_get_loop_impl(ma_msgbus_get_event_loop(mbus), &uv_loop);

        async_call_data->cb = cb;
        async_call_data->cb_data = cb_data;
        async_call_data->status = status;
        async_call_data->ep_consumer = ep_consumer;
        async_call_data->ep_cookie = ep_cookie;
	    uv_timer_init(uv_loop, uv_timer);
	    uv_timer->data = async_call_data;
        MA_LOG(ma_msgbus_get_logger(mbus), MA_LOG_SEV_DEBUG, "uv timer data(%p)\n", async_call_data);
	    uv_timer_start(uv_timer, timer_cb, 0, 0);	
    }   
}

static ma_error_t lookup_cb_entry_clean(ma_ep_lookup_t *self) {
    if (self) {
        while (!ngx_queue_empty(&self->qhead)) {
            ngx_queue_t *ngx_item = ngx_queue_head(&self->qhead);
            ma_lookup_cb_entry_t *entry = ngx_queue_data(ngx_item, ma_lookup_cb_entry_t, qlink);
            ngx_queue_remove(ngx_item);  
            free(entry);            
        }
        MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_TRACE, "lookup cb data cleaned");
    }
    return MA_OK;
}

static ma_error_t lookup_cb_entry_execute(ma_ep_lookup_t *self) {
    if(self) {
        ma_error_t status = (EP_CONNECTED == self->lookup_status) ? MA_OK : self->lookup_info.status;
        ngx_queue_t  *ngx_item = ngx_queue_head(&self->qhead);
        while(ngx_item != ngx_queue_sentinel(&self->qhead) && !ngx_queue_empty(&self->qhead))  {
			ngx_queue_t  *ngx_next_item = ngx_queue_next(ngx_item);
            ma_lookup_cb_entry_t *entry = ngx_queue_data(ngx_item, ma_lookup_cb_entry_t, qlink);
            ma_message_consumer_inc_ref((ma_message_consumer_t *)self);
			/*MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_TRACE, "executing entry <%p> from ep lookup <%p>", entry, self);*/
            call_lookup_callback_async_way(self->mbus, status, entry->cb, entry->cb_data, (ma_message_consumer_t *)self, (void *)entry);          
			ngx_item = ngx_next_item;
        }    
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG; 
}

static ma_error_t lookup_cb_entry_delete(ma_ep_lookup_t *self, ma_lookup_cb_entry_t *delete_entry) {
    if(self) {
        ngx_queue_t  *ngx_item = ngx_queue_head(&self->qhead);
        while(ngx_item != ngx_queue_sentinel(&self->qhead) && !ngx_queue_empty(&self->qhead))  {
			ngx_queue_t  *ngx_next_item = ngx_queue_next(ngx_item);
            ma_lookup_cb_entry_t *entry = ngx_queue_data(ngx_item, ma_lookup_cb_entry_t, qlink);
            if(entry == delete_entry) {
				/*MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_TRACE, "deleting entry <%p> from ep lookup <%p>", entry, self);*/
                ngx_queue_remove(ngx_item);  
                free(entry);
                return MA_OK;
            }
			ngx_item = ngx_next_item;
        }    
        return MA_ERROR_OBJECTNOTFOUND;
    }
    return MA_ERROR_INVALIDARG; 
}

static ma_lookup_cb_entry_t *lookup_cb_entry_add(ma_ep_lookup_t *self, ep_lookup_cb cb, void *cb_data) {
    if(self && cb) {
        ma_lookup_cb_entry_t *entry = (ma_lookup_cb_entry_t *) calloc(1, sizeof(ma_lookup_cb_entry_t));
        if (entry) {
            entry->cb = cb;
            entry->cb_data = cb_data;
			/*MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_TRACE, "adding entry <%p> into ep lookup <%p>", entry, self);*/
            ngx_queue_insert_tail(&self->qhead, &entry->qlink);
            return entry;;
        }
        MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_CRITICAL, "Out of Memory");
        return NULL;
    }
    return NULL; 
}


static ma_error_t consumer_on_message(ma_message_consumer_t *base, ma_message_t *msg) {
    ma_ep_lookup_t *self = (ma_ep_lookup_t *) base ;

    if(msg) {
        /* Forward the received message to named pipe conenction consumer if the destination name matches with ep lookup endpoint name */	
        if(GET_MESSAGE_DESTINATION_NAME(msg) && !strcmp(GET_MESSAGE_DESTINATION_NAME(msg) , self->lookup_info.ep_name)) { 
            SET_MESSAGE_DESTINATION_PID(msg, self->lookup_info.pid);
            ma_message_consumer_send((ma_message_consumer_t *)self->connection, msg);
        }
        else if( (MA_MESSAGE_TYPE_REPLY == GET_MESSAGE_TYPE(msg)) && GET_MESSAGE_DESTINATION_NAME(msg) && (0 == strcmp(GET_MESSAGE_DESTINATION_NAME(msg) , self->ephemeral_name_buffer))) {
            const char *value = NULL;
            ma_error_t status = (ma_error_t)GET_MESSAGE_STATUS(msg);

            MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "Recevied response from broker with status <%d>", status);

            if(MA_OK == ma_message_get_property(msg, MA_PROP_KEY_NAME_SERVICE_REPLY_TYPE_STR, &value)) {                      

                MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "Recevied response type <%s> from broker", value);

                if(!strcmp(MA_PROP_VALUE_DISCOVER_SERVICE_REQUEST_STR, value)) {                
                    const char *ep_name = NULL ;
                    const char *pipe_name = NULL , *host_name = NULL  , *pid = NULL;
                    
                    if(MA_OK == status) {
                        ma_message_get_property(msg , MA_PROP_KEY_SERVICE_NAME_STR , &ep_name);
                        ma_message_get_property(msg , MA_PROP_KEY_SERVICE_ID_STR , &pipe_name);
                        ma_message_get_property(msg , MA_PROP_KEY_HOST_NAME_STR , &host_name);
                        ma_message_get_property(msg , MA_PROP_KEY_PID_STR , &pid);
                        set_lookup_info(&self->lookup_info, status, atol(pid) , pipe_name , host_name);
                        MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "Recevied info from broker, service name: <%s>\n pipe name: <%s>\n pid : <%ld>", ep_name, pipe_name, atol(pid));
                    }
                    else {
                        set_lookup_info(&self->lookup_info, status, -1 , NULL , NULL); /* Set only status in failure case */
                        MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "Failed to Recevie service info from broker and response status is <%d>", status);                     
                    }
                    
					if(self->broker_cookie) 
						ma_ep_lookup_stop((ma_ep_lookup_t *)self->broker_consumer, self->broker_cookie), self->broker_cookie = NULL;
					
					if(self->broker_consumer)
						ma_message_consumer_release(self->broker_consumer), self->broker_consumer  = NULL;

                    resolve_start(self);
                }
            }
        }
		else if(MA_MESSAGE_TYPE_SIGNAL == GET_MESSAGE_TYPE(msg)) {			
			const char *value = NULL;
			if((MA_OK == ma_message_get_property(msg, MA_PROP_KEY_PIPE_NAME_STR, &value)) && value) { 
				if(self->lookup_info.pipe_name && !strcmp(value, self->lookup_info.pipe_name)) {
					ma_error_t status = (ma_error_t)GET_MESSAGE_STATUS(msg);
					if(MA_ERROR_MSGBUS_SERVICE_CONNECTION_TERMINATED == status) {
						ma_named_pipe_connection_t *connection = NULL;
						MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "Received pipe <%s> connection termination", self->lookup_info.pipe_name);
						if(MA_OK == ma_named_pipe_connection_manager_search(ma_msgbus_get_named_pipe_connection_manager(self->mbus), self->lookup_info.pipe_name, (ma_message_consumer_t **)&connection)) {								                           
							if(MA_OK == ma_named_pipe_connection_manager_remove(ma_msgbus_get_named_pipe_connection_manager(self->mbus), connection))
								(void)ma_named_pipe_connection_close(connection , 0);
                             ma_named_pipe_connection_release(connection); /* This decrements the ref count which increases in search funciton */
						}
                        self->lookup_status = EP_NOT_CONNECTED;
                        self->lookup_info.status = MA_ERROR_MSGBUS_SERVICE_CONNECT_FAILED;
                        lookup_cb_entry_execute(self);
						destroy_ep_lookup_asynchronously(self);
					}
				}
			}
		}
    }    
    return MA_OK;
}

static void consumer_destroy(ma_message_consumer_t *base) {
    ma_ep_lookup_t *self = (ma_ep_lookup_t *) base;
    if(self) {	
        MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "ep lookup <%p> destroyed", self);
        lookup_cb_entry_clean(self);
        if(self->lookup_info.ep_name) free(self->lookup_info.ep_name);
		if(self->lookup_info.pipe_name) free(self->lookup_info.pipe_name);
		if(self->lookup_info.host_name) free(self->lookup_info.host_name);    

        if(self->connection) 
            ma_named_pipe_connection_release(self->connection);
        free(self);	
    }  
}

static const ma_message_consumer_methods_t consumer_methods = {
    &consumer_on_message,
    &consumer_destroy
};

//ma_error_t ma_ep_lookup_release(ma_ep_lookup_t *self) {
//    if(self) {
//		MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "ep lookup <%p> released, ref count <%d>", self, self->consumer_impl.ref_count);
//        ma_message_consumer_dec_ref(&self->consumer_impl);
//        return MA_OK;
//    }
//    return MA_ERROR_INVALIDARG;
//}

static ma_error_t create_pipe_name_request_message(ma_ep_lookup_t *self, char *name_service_name , ma_int64_t pid, ma_message_t **pipe_name_req_msg) {
    ma_message_t *msg = NULL ;
    ma_error_t rc = MA_OK ;
    if(MA_OK == (rc = ma_message_create(&msg) )) {
        self->correlation_id = ++g_correlation_id ;
	    sprintf(self->ephemeral_name_buffer,ephemeral_name_template, MA_MSC_SELECT(_getpid(), getpid()), (unsigned long) self->correlation_id);       
        SET_MESSAGE_SOURCE_NAME(msg , self->ephemeral_name_buffer);
        SET_MESSAGE_TYPE(msg , MA_MESSAGE_TYPE_REQUEST);
        SET_MESSAGE_DESTINATION_NAME(msg , name_service_name);
        SET_MESSAGE_DESTINATION_PID(msg , pid);
        ma_message_set_property(msg , MA_PROP_KEY_NAME_SERVICE_REQUEST_TYPE_STR , MA_PROP_VALUE_DISCOVER_SERVICE_REQUEST_STR);
        ma_message_set_property(msg , MA_PROP_KEY_SERVICE_NAME_STR , self->lookup_info.ep_name);
        *pipe_name_req_msg = msg;		
        MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "resolve request message created source name <%s> for service <%s>", self->ephemeral_name_buffer, self->lookup_info.ep_name);
    }
    return rc;
}

static void name_service_lookup_cb(ma_error_t status, ma_message_consumer_t *broker_consumer, void *broker_cookie, void *cb_data) {
    ma_ep_lookup_t *self = (ma_ep_lookup_t *)cb_data;
	
    if( MA_OK == status) {
        MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "Received broker consumer, sending service <%s> pipe name request msg", self->lookup_info.ep_name);
        self->broker_consumer = broker_consumer;
        self->broker_cookie = broker_cookie;
        ma_message_consumer_send(broker_consumer , self->request_message);
        ma_message_release(self->request_message);
    }
    else {		
        MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_ERROR, "Unable to send the service <%s> pipe name request msg to broker", self->lookup_info.ep_name);  
	
		self->lookup_status = EP_NOT_CONNECTED;
        self->lookup_info.status = (MA_ERROR_MSGBUS_SERVICE_CONNECT_FAILED == status) ? MA_ERROR_MSGBUS_BROKER_CONNECT_FAILED : status;
        lookup_cb_entry_execute(self);
		
		if(broker_cookie) ma_ep_lookup_stop((ma_ep_lookup_t *)broker_consumer, broker_cookie);
		ma_message_consumer_release(broker_consumer);	    
    }
}

static void signal_connection_failure_message(ma_ep_lookup_t *self) {
	ma_error_t rc = MA_OK;
	ma_error_t status = MA_ERROR_MSGBUS_SERVICE_CONNECTION_TERMINATED;
	ma_message_t *msg = NULL;

	if(MA_OK == (rc = ma_message_create(&msg))) {
		SET_MESSAGE_TYPE(msg, MA_MESSAGE_TYPE_SIGNAL);
		SET_MESSAGE_DESTINATION_PID(msg, 0);
		SET_MESSAGE_STATUS(msg, status);		
		ma_message_set_property(msg, MA_PROP_KEY_PIPE_NAME_STR, self->lookup_info.pipe_name);
		ma_message_consumer_send((ma_message_consumer_t *)ma_msgbus_get_message_router(self->mbus), msg);			
		ma_message_release(msg); msg = NULL;
	}
}


static void c_connection_cb(ma_named_pipe_connection_t *connection, ma_error_t status, void *cb_data) {
    ma_ep_lookup_t *self = (ma_ep_lookup_t *) cb_data ;

	MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "Status from connect callback <%d>", status); 

    if(MA_OK == status) {
        /* Now that the connection is established  */ 
        MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "Connection Established");
        ma_named_pipe_connection_start_receiving(connection);
    }
	else if(MA_ERROR_MSGBUS_SERVICE_CONNECT_FAILED == status) { //Free the connection 
		MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_ERROR, "Connection Failed");        
   
     	signal_connection_failure_message(self);
		/* Dont access self after this call. Self might be invalid after this... */
	}
	else {
		MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_ERROR, "Unknown status from connect callback"); 
	}
	ma_message_consumer_release((ma_message_consumer_t *)self);
}


static ma_error_t resolve_start(ma_ep_lookup_t *self) {
    if(self) {
        ma_error_t rc = MA_OK ;
        if(MA_OK == (rc = self->lookup_info.status)) {            
            if(MA_OK != (rc = ma_named_pipe_connection_manager_search(ma_msgbus_get_named_pipe_connection_manager(self->mbus), self->lookup_info.pipe_name, (ma_message_consumer_t**)&self->connection))) {
				if(MA_OK == (rc = (ma_named_pipe_connection_create(ma_msgbus_get_event_loop(self->mbus), &self->connection)))) {
                    (void)ma_named_pipe_connection_set_logger(self->connection, ma_msgbus_get_logger(self->mbus));
                    ma_named_pipe_connection_set_auth_info_provider(self->connection, ma_msgbus_get_auth_info_provider(self->mbus));
					ma_named_pipe_connection_set_passphrase_handler(self->connection, ma_msgbus_get_passphrase_handler(self->mbus));
                    ma_message_consumer_inc_ref((ma_message_consumer_t *)self);
					if(MA_OK == (rc = ma_named_pipe_connection_connect(self->connection, ma_msgbus_get_crypto_provider(self->mbus), self->lookup_info.pipe_name , c_connection_cb , self))) {
                        /* Add it to named pipe connection manager */					
                        ma_named_pipe_connection_manager_add(ma_msgbus_get_named_pipe_connection_manager(self->mbus), self->lookup_info.pipe_name , self->connection);
                        ma_named_pipe_connection_set_consumer(self->connection, (ma_message_consumer_t*)ma_msgbus_get_message_router(self->mbus));
                        MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "named pipe connect succeeded");
                    }
                    else{
                        MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_ERROR, "named pipe connection connect failed");
                        ma_named_pipe_connection_release(self->connection) ;				
						ma_message_consumer_release((ma_message_consumer_t *)self);
                    }
                }
                else
                    MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_ERROR, "named pipe connection create failed");
            }
            else {               
                MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "named pipe connection is already available in named pipe connection manager");
            }            
        }

        self->lookup_status = (MA_OK == rc)? EP_CONNECTED : EP_NOT_CONNECTED;
        rc = lookup_cb_entry_execute(self);

        if(EP_NOT_CONNECTED == self->lookup_status)  /* remove consumer from router if the resolve fails */            
            destroy_ep_lookup_asynchronously(self);
        
        return rc; 
    }
    return MA_ERROR_INVALIDARG;
}


static ma_error_t lookup_start(ma_ep_lookup_t *self, ma_lookup_cb_entry_t *entry) {
    if(self) {
        ma_error_t rc = MA_OK ;
		ma_int64_t pid = -1 ;

        /* If user is trying to resolve the one of the broker services, call resolve_start for broker to start the connection */
        if(!strcmp(self->lookup_info.ep_name, MA_MSGBUS_NAME_SERVICE_NAME_STR) || 
		   !strcmp(self->lookup_info.ep_name, MA_MSGBUS_PUBSUB_SERVICE_NAME_STR)) {
			const char *broker_pipe_name = NULL ;
			
			if( !(broker_pipe_name = ma_msgbus_get_broker_pipe_name(self->mbus)) ||
				 (-1 == (pid =  ma_msgbus_get_broker_pid(self->mbus)))) {
                MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_TRACE, "Broker information is not available, It seems Broker is not runnng....");
                return MA_ERROR_MSGBUS_GET_BROKER_INFO_FAILED ;
			}

            MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "Resolving broker services connection, broker pipe name <%s>", broker_pipe_name);
            set_lookup_info(&self->lookup_info, MA_OK, pid , broker_pipe_name , NULL);
            return resolve_start(self);
        }

		if(-1 != (pid =  ma_msgbus_get_broker_pid(self->mbus))) {
			if(MA_OK  != (rc = create_pipe_name_request_message(self, MA_MSGBUS_NAME_SERVICE_NAME_STR, pid, &self->request_message))) {
				MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_ERROR, "Failed to create the pipe name request message - error <%d>", rc);
			}
			else {
				MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "Searching broker consumer and sending the service <%s> resolve request to name service", self->lookup_info.ep_name);
				ma_message_consumer_inc_ref((ma_message_consumer_t *)self);
				call_lookup_callback_async_way(self->mbus, MA_ERROR_MSGBUS_EP_LOOKUP_IN_PROGRESS, entry->cb, entry->cb_data, (ma_message_consumer_t *)self, entry);
				rc = ma_ep_lookup_start(self->mbus, MA_MSGBUS_NAME_SERVICE_NAME_STR, MSGBUS_CONSUMER_REACH_OUTPROC, name_service_lookup_cb, self);				
			}     
		}
		else {
			rc = MA_ERROR_MSGBUS_GET_BROKER_INFO_FAILED;
			MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_TRACE, "Broker pid is not available, It seems Broker is not runnng....");
		}

        return rc;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_ep_lookup_start(ma_msgbus_t *mbus, const char *ep_name, ma_msgbus_consumer_reachability_t lookup_reach, ep_lookup_cb cb, void *cb_data) {
    if(mbus && ep_name && cb) {        
        ma_error_t rc = MA_OK;
        ma_message_consumer_t *ep_consumer = NULL;
        ma_ep_lookup_t *self= NULL; 
        ma_message_consumer_type_t consumer_type = MA_MSGBUS_UNKNOWN;
                  
        if(MA_OK == (rc = ma_message_router_search_consumer(ma_msgbus_get_message_router(mbus), ep_name, lookup_reach, &consumer_type, &ep_consumer) )) {
            if(MA_MSGBUS_PROXY_SERVER == consumer_type && ep_consumer) { // proxy of outproc service
                self = (ma_ep_lookup_t *)ep_consumer;
                MA_LOG(ma_msgbus_get_logger(mbus), MA_LOG_SEV_DEBUG, "Service <%s> found in local search and status of ep lookup is <%d>", ep_name, self->lookup_status);

                if(EP_CONNECTED == self->lookup_status)  {                    
                    ma_lookup_cb_entry_t *entry = lookup_cb_entry_add(self, cb, cb_data);
                    if(entry) {
                        ma_message_consumer_inc_ref(ep_consumer);
                        call_lookup_callback_async_way(mbus, rc, cb, cb_data, ep_consumer, entry);
                    }
                    else
                        rc = MA_ERROR_MSGBUS_INTERNAL;
                }
                else if(EP_CONNECTING == self->lookup_status) {  
                    ma_lookup_cb_entry_t *entry = lookup_cb_entry_add(self, cb, cb_data); /* Add the cb data in list to notify when connection object is ready.*/
                    if(!entry) return MA_ERROR_MSGBUS_INTERNAL;
					ma_message_consumer_inc_ref(ep_consumer);
					call_lookup_callback_async_way(self->mbus, MA_ERROR_MSGBUS_EP_LOOKUP_IN_PROGRESS, entry->cb, entry->cb_data, (ma_message_consumer_t *)self, entry);
                }
                else
                    return MA_ERROR_MSGBUS_SERVICE_CONNECT_FAILED;
            }
            else if(MA_MSGBUS_SERVER == consumer_type && ep_consumer) { // local service  
                ma_message_consumer_inc_ref(ep_consumer);
                call_lookup_callback_async_way(mbus, rc, cb, cb_data, ep_consumer, NULL);   /* Pass NULL cookie for local services */         
            }
            else
                rc = MA_ERROR_MSGBUS_INTERNAL; // There is a no chance to reach here...
        }
        else {
            /* ep_lookup object is created only once for each endpoint, 
               the same ep_lookup object is returned from local search in router*/
            MA_LOG(ma_msgbus_get_logger(mbus), MA_LOG_SEV_DEBUG, "Service <%s> not found in local search and reachability is <%d>", ep_name, lookup_reach);
            if(MSGBUS_CONSUMER_REACH_INPROC != lookup_reach) {                           
                self = (ma_ep_lookup_t *)calloc(1, sizeof(ma_ep_lookup_t));        
                if(self) {   
                    ma_lookup_cb_entry_t *entry = NULL;
                    self->mbus = mbus;
                    self->lookup_info.ep_name = strdup(ep_name);
                    MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "<%s> service lookup start, ep lookup <%p>", ep_name, self);
                    ngx_queue_init(&self->qhead); 
                    ma_message_consumer_init(&self->consumer_impl, &consumer_methods, 1, self);
                    self->lookup_status = EP_CONNECTING;
                    ma_message_router_add_consumer(ma_msgbus_get_message_router(self->mbus), (ma_message_consumer_t *)self, self->lookup_info.ep_name, MA_MSGBUS_PROXY_SERVER);
                    if((entry = lookup_cb_entry_add(self, cb, cb_data))) {
                        if(MA_OK != (rc = lookup_start(self, entry)))  {                    
                            lookup_cb_entry_delete(self, entry);
                            ma_message_router_remove_consumer(ma_msgbus_get_message_router(self->mbus) , (ma_message_consumer_t *)self);   
                        }
                    }
                }
                else                     
                    rc = MA_ERROR_OUTOFMEMORY ;                
            }
        }
		return rc;
    }
    return MA_ERROR_INVALIDARG; 
}

static void destroy_close_cb(uv_handle_t *handle) {
	ma_ep_lookup_t *self = (ma_ep_lookup_t *)handle->data;
	ma_message_consumer_release((ma_message_consumer_t *)self);
}

static void destroy_timer_cb(uv_timer_t *timer, int status) {
	ma_ep_lookup_t *self = (ma_ep_lookup_t *)timer->data;
	uv_close((uv_handle_t *)timer, destroy_close_cb);
}

static void destroy_ep_lookup_asynchronously(ma_ep_lookup_t *self) {
	if(self) {
		if(!self->is_destroy_started) {
			uv_loop_t *uv_loop = NULL;
			ma_event_loop_get_loop_impl(ma_msgbus_get_event_loop(self->mbus), &uv_loop);
			uv_timer_init(uv_loop, &self->destroy_timer);
			self->destroy_timer.data = self;
            MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "uv timer data(%p)\n", self);
			ma_message_router_remove_consumer(ma_msgbus_get_message_router(self->mbus) , (ma_message_consumer_t *)self);
			
			if(self->broker_cookie) 
				ma_ep_lookup_stop((ma_ep_lookup_t *)self->broker_consumer, self->broker_cookie), self->broker_cookie = NULL;
					
			if(self->broker_consumer)
				ma_message_consumer_release(self->broker_consumer), self->broker_consumer  = NULL;

			uv_timer_start(&self->destroy_timer, destroy_timer_cb, 0, 0);
			self->is_destroy_started = MA_TRUE;
		}
	}
}


ma_error_t ma_ep_lookup_stop(ma_ep_lookup_t *self, void *ep_cookie) {
    if(self && ep_cookie) {
        ma_error_t rc = MA_OK;
		MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "ep lookup <%p> stop", self);	
        rc = lookup_cb_entry_delete(self, (ma_lookup_cb_entry_t *)ep_cookie);
		if(ngx_queue_empty(&self->qhead)) {
			destroy_ep_lookup_asynchronously(self);
		}		
		return rc;
    }
    return MA_ERROR_INVALIDARG;
}

