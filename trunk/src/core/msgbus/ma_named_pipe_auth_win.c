#include "ma/internal/core/msgbus/ma_named_pipe_auth.h"
#include "ma/internal/core/msgbus/ma_named_pipe_credentials.h"
#include "ma/internal/core/msgbus/ma_message_auth_info_private.h"
#include "ma_vscore_vtp.h"

#include "ma/internal/utils/event_loop/ma_uv_helpers.h"
#include "ma/internal/utils/threading/ma_atomic.h"
#include "ma/internal/utils/platform/ma_path_utils.h"
#include "ma/internal/ma_macros.h"

#include <stdlib.h>
#include <string.h>


struct ma_named_pipe_auth_s {
    ma_atomic_counter_t                         ref_count;

    ma_named_pipe_on_authenticate_cb            authenticate_cb;

    void                                        *authenticate_cb_data;
    
    ma_logger_t                                 *logger;

    uv_loop_t                                   *uv_loop;

    uv_pipe_t                                   *peer_pipe;

    ma_message_auth_info_t                      *peer_auth_info;

    uv_timer_t                                  uv_timer;

    /* copy of crypto provider */
    struct ma_msgbus_crypto_provider_s			*crypto_provider;

    /* vscore vtp service (wrapper) This should be set from the outside */
    ma_vscore_vtp_t                             *vtp;

    trust_level_t                               trust_level;
};


ma_error_t ma_named_pipe_auth_create(uv_loop_t *uv_loop, struct ma_msgbus_crypto_provider_s *crypto_provider, uv_pipe_t *pipe, ma_named_pipe_auth_t **pipe_auth) {
    if(uv_loop && pipe && pipe_auth) {
        ma_named_pipe_auth_t *self = (ma_named_pipe_auth_t*)calloc(1, sizeof(ma_named_pipe_auth_t));
        if (self) {
            ma_error_t rc = MA_OK;
            if(MA_OK == (rc = ma_message_auth_info_create(&self->peer_auth_info))) {
                self->ref_count = 1;
                self->uv_loop = uv_loop;
                self->crypto_provider = crypto_provider;
                self->peer_pipe = pipe;
                *pipe_auth = self;
                return MA_OK;
            }
            return rc;
        }
        return MA_ERROR_OUTOFMEMORY;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_named_pipe_auth_add_ref(ma_named_pipe_auth_t *self) {
    if (self && (0 <= self->ref_count)) {
        MA_ATOMIC_INCREMENT(self->ref_count);
    }
    return MA_OK;
}

static void ma_named_pipe_auth_destroy(ma_named_pipe_auth_t *self) {
    ma_message_auth_info_release(self->peer_auth_info);
    free(self);
}

ma_error_t ma_named_pipe_auth_release(ma_named_pipe_auth_t *self) {
    if (self && (0 <= self->ref_count)) {
        if (0 == MA_ATOMIC_DECREMENT(self->ref_count)) {
            ma_named_pipe_auth_destroy(self);
        }
    }
    return MA_OK;
}

ma_error_t ma_named_pipe_auth_set_logger(ma_named_pipe_auth_t *self, ma_logger_t *logger) {
    if(self) {
        self->logger = logger;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_named_pipe_auth_set_passphrase_handler(ma_named_pipe_auth_t *self, struct ma_msgbus_passphrase_handler_s *passphrase_handler) {
	/*TBD- On windows we are not using it yet */
	return MA_OK;
}


static void timer_close_cb(uv_handle_t *handle) {
    ma_named_pipe_auth_release((ma_named_pipe_auth_t *)handle->data);
}

ma_error_t ma_named_pipe_auth_stop(ma_named_pipe_auth_t *self) {
    if (self) {
        /*TBD check the state if performing stop it */
		if(uv_is_active((uv_handle_t *) &self->uv_timer))
			uv_timer_stop(&self->uv_timer);

		ma_named_pipe_auth_add_ref(self);
		uv_close((uv_handle_t *)&self->uv_timer, timer_close_cb);

        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static void timer_cb(uv_timer_t *handle, int status) {
    ma_named_pipe_auth_t *self = (ma_named_pipe_auth_t *)handle->data;
    ma_named_pipe_auth_add_ref(self);
    uv_close((uv_handle_t *)handle, timer_close_cb); 
    ma_message_auth_info_add_ref(self->peer_auth_info);
    self->authenticate_cb(self, MA_OK, self->authenticate_cb_data, self->peer_auth_info);    
}


static void get_os_credentials(ma_named_pipe_auth_t *self, long peer_pid) {
    HANDLE hProcess;
    hProcess = OpenProcess(PROCESS_QUERY_INFORMATION ,FALSE, peer_pid);
    if (hProcess) {
        HANDLE hToken;
        if (OpenProcessToken(hProcess, TOKEN_READ, &hToken)) {
            TOKEN_USER *token_user;
            /*TOKEN_GROUPS token_groups;*/
            DWORD token_size;

            GetTokenInformation(hToken, TokenUser, NULL, 0, &token_size);
            token_user = malloc(token_size);

            if (token_user && GetTokenInformation(hToken, TokenUser, token_user, token_size, &token_size)) {
               (void)ma_message_auth_info_set_attribute(self->peer_auth_info, MA_MSGBUS_AUTH_INFO_ATTRIBUTE_USER_SID, token_user->User.Sid);               
            } else {
                MA_LOG(self->logger, MA_LOG_SEV_WARNING, "NamedPipe authentication problem, not all peer credentials could be obtained. GetTokenInformation(,TokenUser,...) returned a failure, err = %u", (unsigned int) GetLastError() );
            }
            free(token_user);
            CloseHandle(hToken);
        } else {
            MA_LOG(self->logger, MA_LOG_SEV_WARNING, "NamedPipe authentication problem, not all peer credentials could be obtained. OpenProcessToken() returned a failure, err = %u", (unsigned int) GetLastError() );
        }
        CloseHandle(hProcess);
    } else {
        MA_LOG(self->logger, MA_LOG_SEV_WARNING, "NamedPipe authentication problem, not all peer credentials could be obtained. OpenProcess() returned a failure, err = %u", (unsigned int) GetLastError() );
    }
} 

static ma_error_t vtp_validate_cb(ma_vscore_vtp_module_h module, void *cb_data) {
    ma_named_pipe_auth_t *self = cb_data;
    if (ma_vscore_vtp_module_is_main_executable(module)) {
        (void)ma_message_auth_info_set_attribute(self->peer_auth_info, MA_MSGBUS_AUTH_INFO_ATTRIBUTE_VERIFIED_SIGNER, ma_vscore_vtp_module_get_signer(module));
        (void)ma_message_auth_info_set_attribute(self->peer_auth_info, MA_MSGBUS_AUTH_INFO_ATTRIBUTE_VERIFIED_PUBLISHER, ma_vscore_vtp_module_get_name(module));
        self->trust_level = ma_vscore_vtp_module_is_trusted(module) ? TRUST_LEVEL_TRUSTED : TRUST_LEVEL_UNTRUSTED;

        return 1; /* return non 0 to stop iteration as we have what we came for */
    }

    return 0;
}

static void get_vtp_credentials(ma_named_pipe_auth_t *self, long peer_pid) {
    ma_vscore_vtp_t *vtp_service;
    if (MA_OK == ma_vscore_vtp_create(&vtp_service)) { /* Note, vtp_service should really be injected into this class, not created here! */
        ma_vscore_vtp_validate_process_ex(vtp_service, peer_pid, vtp_validate_cb, self);
        ma_vscore_vtp_release(vtp_service);
    }
}


ma_error_t ma_named_pipe_auth_start(ma_named_pipe_auth_t *self, long timeout_in_milliseconds, ma_named_pipe_on_authenticate_cb cb, void *cb_data) {
    if(self) {
        /*TBD - need to figure out how to integrate with VS core here , Poul do it do it */
        ma_pipe_t peer_pipe_handle = self->peer_pipe->handle;
        ma_uid_t peer_uid;
        ma_gid_t peer_gid;
        ma_pid_t peer_pid;
        ma_error_t rc = MA_OK;

        /*ignore timeout in milliseconds for windows as of now */
        (timeout_in_milliseconds);

         /*TBD check the state if not performing start it */
        self->authenticate_cb = cb;
        self->authenticate_cb_data = cb_data;
        
        if(MA_OK == (rc = ma_named_pipe_credentials_get(peer_pipe_handle, &peer_pid, &peer_uid, &peer_gid))) {
            MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Received secure credentials for peer pipe handle=<%ld>, pid=<%ld>.", (long) peer_pipe_handle, (long)peer_pid);
            (void)ma_message_auth_info_set_attribute(self->peer_auth_info, MA_MSGBUS_AUTH_INFO_ATTRIBUTE_PID, peer_pid);
            /*TBD commenting for now 
            get_os_credentials(self, peer_pid);
            get_vtp_credentials(self, peer_pid);
            */
        }
        else
            MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Failed to receive secure credentials for peer pipe handle=<%ld> ,<%d>.",peer_pipe_handle, rc);

        uv_timer_init(self->uv_loop, &self->uv_timer);
        self->uv_timer.data = self;
        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "uv timer data(%p)\n", self);
        uv_timer_start(&self->uv_timer, timer_cb, 0, 0);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_named_pipe_auth_set_server(ma_named_pipe_auth_t *self) { 
	return MA_OK; 
}