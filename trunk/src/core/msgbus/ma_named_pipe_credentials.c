#include "ma/internal/core/msgbus/ma_named_pipe_credentials.h"

#ifdef MA_WINDOWS

#define SMB_PID 0xFEFF /* ProcessId of the SMB2 SYNC Packet header - apparently this is passed on by GetNamedPipeClientProcessId()  */

#include <Windows.h>

typedef struct _FILE_PIPE_LOCAL_INFORMATION {
    ULONG NamedPipeType;
    ULONG NamedPipeConfiguration;
    ULONG MaximumInstances;
    ULONG CurrentInstances;
    ULONG InboundQuota;
    ULONG ReadDataAvailable;
    ULONG OutboundQuota;
    ULONG WriteQuotaAvailable;
    ULONG NamedPipeState;
    ULONG NamedPipeEnd;
} FILE_PIPE_LOCAL_INFORMATION, *PFILE_PIPE_LOCAL_INFORMATION;

typedef struct _IO_STATUS_BLOCK {
    union {
        DWORD Status;
        PVOID Pointer;
    } u;
    ULONG_PTR Information;
} IO_STATUS_BLOCK, *PIO_STATUS_BLOCK;

typedef enum _FILE_INFORMATION_CLASS {
    FilePipeLocalInformation = 24
} FILE_INFORMATION_CLASS, *PFILE_INFORMATION_CLASS;

#ifndef FILE_PIPE_CLIENT_END
# define FILE_PIPE_CLIENT_END 0x0
# define FILE_PIPE_SERVER_END 0x1
#endif

typedef BOOL (WINAPI *GETNAMEDPIPECLIENTPROCESSID)(HANDLE,PULONG);

typedef DWORD (WINAPI *PNTQUERYINFORMATIONFILE)
    (HANDLE, IO_STATUS_BLOCK *, VOID *, ULONG, FILE_INFORMATION_CLASS);

#else
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>

#if defined(SCM_CREDENTIALS)
#define MA_SCM_CRED_CMSG_TYPE  SCM_CREDENTIALS;
#else
#define MA_SCM_CRED_CMSG_TYPE SCM_CREDS;
#endif

#endif /* MA_WINDOWS*/


/* Linux and OpenBSD */
#if defined(SO_PEERCRED)

ma_error_t ma_named_pipe_credentials_get(ma_pipe_t pipe, ma_pid_t *pid, ma_uid_t *uid, ma_gid_t *gid) {
    if(pipe > 0 && pid && uid && gid) {
	    #if defined(__linux__)
            struct ucred cred;
	    #elif defined(__OpenBSD__)
            struct sockpeercred cred;
	    #endif

        socklen_t len = sizeof(cred);

        /* initialize client information (in case getsockopt() breaks) */
        /* look up process information from peer */
        if(getsockopt(pipe, SOL_SOCKET, SO_PEERCRED, &cred, &len) < 0)
            return MA_ERROR_PIPE_CREDENTIALS_RECV_FAILED;

        /* return the data */
        *pid = cred.pid; *uid = cred.uid; *gid = cred.gid;
	return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

/* FreeBSD */
#elif defined(LOCAL_PEERCRED)
#include <sys/ucred.h>
#if !defined(LOCAL_PEERPID)
#define LOCAL_PEERPID           0x002           /* retrieve peer pid */
#endif

ma_error_t ma_named_pipe_credentials_get(ma_pipe_t pipe, ma_pid_t *pid, ma_uid_t *uid, ma_gid_t *gid) {
    if(pipe > 0 && pid && uid && gid) {
        struct xucred cred;
        socklen_t len = sizeof(cred);
        ma_pid_t      p = (ma_pid_t)-1;
        /* look up process information from peer */
        if(getsockopt(pipe, 0, LOCAL_PEERCRED, &cred, &len) < 0) 
            return MA_ERROR_PIPE_CREDENTIALS_RECV_FAILED;

        if(XUCRED_VERSION != cred.cr_version) {
            return MA_ERROR_PIPE_CREDENTIALS_RECV_FAILED;
        }
        /* return the data */
        *uid = cred.cr_uid; *gid = cred.cr_gid;

        len = sizeof(ma_pid_t);
        if(getsockopt(pipe, 0, LOCAL_PEERPID, &p, &len) < 0)
            return MA_ERROR_PIPE_CREDENTIALS_RECV_FAILED;

        /* return the data */
        *pid = p; 
	    return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

#elif defined(LOCAL_PEEREID)
/* NetBSD */

ma_error_t ma_named_pipe_credentials_get(ma_pipe_t pipe, ma_pid_t *pid, ma_uid_t *uid, ma_gid_t *gid) {
    if(pipe > 0 && pid && uid && gid) {
        ma_uid_t u = (ma_uid_t)0;
        ma_gid_t g = (ma_gid_t)0;
        ma_gid_t p = (ma_pid_t)-1;	
        socklen_t len = 0;

        if(getpeereid(pipe, &u, &g))
            return MA_ERROR_PIPE_CREDENTIALS_RECV_FAILED;

        /* return the data */
        *uid = u; *gid = g;

        len = sizeof(ma_pid_t);
        if(getsockopt(pipe, 0, LOCAL_PEERPID, &p, &len) < 0) 
            return MA_ERROR_PIPE_CREDENTIALS_RECV_FAILED;

        /* return the data */
        *pid = p; 
	    return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}  

/* Aix */
#elif defined(AIX)

ma_error_t ma_named_pipe_credentials_get(ma_pipe_t pipe, ma_pid_t *pid, ma_uid_t *uid, ma_gid_t *gid) {
    if(pipe > 0 && pid && uid && gid) {
	   struct peercred_struct cr;
       socklen_t crl = sizeof(struct peercred_struct);
	   int ret = 0;
       if((ret = getsockopt(pipe, SOL_SOCKET, SO_PEERID, &cr, &crl)) < 0)
			return MA_ERROR_PIPE_CREDENTIALS_RECV_FAILED;
		*pid = cr.pid;
		*uid = cr.euid;
		*gid = cr.egid;
	    return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}  

/* Solaris */
#elif defined(SunOS)
#include <alloca.h>
#include <ucred.h>

ma_error_t ma_named_pipe_credentials_get(ma_pipe_t pipe, ma_pid_t *pid, ma_uid_t *uid, ma_gid_t *gid) {
    if(pipe > 0 && pid && uid && gid) {
        ucred_t *cred = NULL;
	    /*TBD - do we need to alloca it ?*/
        if(getpeerucred(pipe, &cred))
           return MA_ERROR_PIPE_CREDENTIALS_RECV_FAILED;
			
        /* return the data */
        *pid = ucred_geteuid(cred); 
		*uid = ucred_getegid(cred); 
		*gid = ucred_geteuid(cred);

        /* free cred and return */
        ucred_free(cred);
	    return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

/* Windows */
#elif defined(MA_WINDOWS) 

ma_error_t ma_named_pipe_credentials_get(ma_pipe_t pipe, ma_pid_t *pid, ma_uid_t *uid, ma_gid_t *gid) {
    if(pipe > 0 && pid && uid && gid) {
        PNTQUERYINFORMATIONFILE NtQueryInformationFile = (PNTQUERYINFORMATIONFILE) GetProcAddress (GetModuleHandleA("ntdll.dll"),"NtQueryInformationFile");
        /*TODO - do we need the UID and GID for windows and what they are */
        *uid = *gid = 0;
        if (NtQueryInformationFile) {
            FILE_PIPE_LOCAL_INFORMATION fpli;
            IO_STATUS_BLOCK iosb;
            if (0 == NtQueryInformationFile(pipe,&iosb,&fpli,sizeof(fpli),FilePipeLocalInformation)) {
                GETNAMEDPIPECLIENTPROCESSID pid_fn = (GETNAMEDPIPECLIENTPROCESSID) GetProcAddress(GetModuleHandleA("kernel32"), FILE_PIPE_SERVER_END == fpli.NamedPipeEnd ? "GetNamedPipeClientProcessId" : "GetNamedPipeServerProcessId" );/* Vista and above */
                if (!pid_fn) 
                    return MA_ERROR_PIPE_CREDENTIALS_RECV_FAILED;
                if ( (*pid_fn)(pipe, pid) ) {
                    if (SMB_PID == *pid) {
                        /* Probably came via SMB (network), set to 0 */
                        *pid = 0;
                    }
                } else {
                    /* Failed to get pipe credentials */
                    return MA_ERROR_FROM_SYS_LAST_ERROR(GetLastError());
                }
            }
            return MA_OK;
        }/* NtQueryInformationFile */
    }
    return MA_ERROR_INVALIDARG;
}

#elif defined(SCM_CREDENTIALS)  || defined (SCM_CREDS)
/*
#include <sys/socket.h>

static ma_error_t send_creds(ma_pipe_t pipe) {
    int bytes_written;
    char buf[1] = { '\0' };
    union {
       struct cmsghdr cmh;
       char   control[CMSG_SPACE(sizeof(struct cmsgcred))];
    }cmsg;
    struct iovec iov;
    struct msghdr msg = {0};

    iov.iov_base = buf;
    iov.iov_len = 1;
    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;

    msg.msg_control = (caddr_t) &cmsg;
    msg.msg_controllen = CMSG_SPACE (sizeof (struct cmsgcred));
    cmsg.hdr.cmsg_len = CMSG_LEN (sizeof (struct cmsgcred));
    cmsg.hdr.cmsg_level = SOL_SOCKET;
    cmsg.hdr.cmsg_type = MA_SCM_CRED_CMSG_TYPE;

    do {
      bytes_written = sendmsg(pipe, &msg, 0);
    }while(bytes_written == -1 && errno == EINTR);

    return bytes_written > 0 : MA_OK : MA_ERROR_PIPE_CREDENTIALS_RECV_FAILED;
}

static ma_error_t recv_creds(ma_pipe_t pipe, ma_pid_t *pid, ma_uid_t *uid, ma_gid_t *gid) {
    struct msghdr msg = {0};
    struct iovec iov;
    ma_error_t rc = MA_ERROR_PIPE_CREDENTIALS_RECV_FAILED;
    char buf;
    int bytes_read;
    union {
        struct cmsghdr hdr;
        char cred[CMSG_SPACE (sizeof (struct cmsgcred))];
    } cmsg;

    iov.iov_base = &buf;
    iov.iov_len = 1;

    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;

    msg.msg_control = (caddr_t) &cmsg;
    msg.msg_controllen = CMSG_SPACE (sizeof (struct cmsgcred));

    do {
        bytes_read = recvmsg(pipe, &msg, 0);
    } while (bytes_read < 0 && errno == EINTR);

    if(bytes_read < 0) {
        //TBD - introduce loop here as socket may be busy or read again 
        return MA_ERROR_PIPE_CREDENTIALS_RECV_FAILED;
    }
    else if(0 == bytes_read) {
        //This should not happen et all 
        return MA_ERROR_PIPE_CREDENTIALS_RECV_FAILED;
    }
    else if('\0' != buf) {
        //This should not happen et all 
        return MA_ERROR_PIPE_CREDENTIALS_RECV_FAILED;
    }

    cmsg = CMSG_FIRSTHDR(&msg);
    while(cmsg != NULL) {
        if(SOL_SOCKET == cmsg->cmsg_level && MA_SCM_CRED_CMSG_TYPE == cmsg->cmsg_type) {
            struct cmsgcred *cred = NULL;
            cred = (struct cmsgcred *) CMSG_DATA (&cmsg.hdr);
            *pid = cred->cmcred_pid;
            *uid = cred->cmcred_euid;
            *gid = cred->cmcred_gid
            rc = MA_OK;
        } else if( SOL_SOCKET == cmsg->cmsg_level && SCM_RIGHTS == cmsg->cmsg_type){
            //TBD - check for handle leaks someone can flood us 
        }
        cmsg = CMSG_NXTHDR(&msg, cmsg);
   }
   return rc;
}

ma_error_t ma_named_pipe_credentials_get(ma_pipe_t pipe, ma_pid_t *pid, ma_uid_t *uid, ma_gid_t *gid) {
    if(pipe > 0 && pid && uid && gid) {
        ma_error_t rc = MA_OK;
        if(MA_OK == (rc = send_creds(pipe))) {
            rc = recv_creds(pipe, pid, uid, gid);
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
} 
*/
#else
#warning "This platform needs an implementation of getting credentials"

ma_error_t ma_named_pipe_credentials_get(ma_pipe_t pipe, ma_pid_t *pid, ma_uid_t *uid, ma_gid_t *gid) {
    if(pipe > 0 && pid && uid && gid) {
        /* nothing found that is supported */
        return MA_ERROR_PIPE_CREDENTIALS_RECV_FAILED;
    }
    return MA_ERROR_INVALIDARG;
}

#endif
