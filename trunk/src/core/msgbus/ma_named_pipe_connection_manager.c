#include "ma/internal/core/msgbus/ma_named_pipe_connection_manager.h"
#include "ma/internal/core/msgbus/ma_msgbus_private.h"
#include "uv-private/ngx-queue.h" /* for ngx_ */

#include <stdlib.h>
#include <assert.h>
#include <string.h>


typedef struct q_connection_item_s {
    ngx_queue_t qlink;
	char *pipe_name ;
    ma_named_pipe_connection_t *connection;
} q_connection_item_t;



struct ma_named_pipe_connection_manager_s {
    ngx_queue_t qhead;
    ma_logger_t *logger;
};

ma_error_t ma_named_pipe_connection_manager_create(ma_named_pipe_connection_manager_t **conn_mgr) {
    ma_named_pipe_connection_manager_t *self = calloc(1, sizeof(ma_named_pipe_connection_manager_t));
    if (self) {
        ngx_queue_init(&self->qhead);
        *conn_mgr = self;
        return MA_OK;
    }
    return MA_ERROR_OUTOFMEMORY;
}

static ma_error_t ma_named_pipe_connection_manager_clear(ma_named_pipe_connection_manager_t *self) {
    if (self) {
        while (!ngx_queue_empty(&self->qhead)) {
            ngx_queue_t *ngx_item = ngx_queue_head(&self->qhead);
            q_connection_item_t *item = ngx_queue_data(ngx_item, q_connection_item_t, qlink);
            ngx_queue_remove(ngx_item);
            ma_named_pipe_connection_release(item->connection);
            if(item->pipe_name) free(item->pipe_name);
            free(item);
        }
        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "named pipe connection manager cleared");
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_named_pipe_connection_manager_release(ma_named_pipe_connection_manager_t *self) {
    if (self) {
        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "named pipe connection manager destroying...");
        ma_named_pipe_connection_manager_clear(self);
        free(self);        
    }
    return MA_OK;
}


ma_error_t ma_named_pipe_connection_manager_add(ma_named_pipe_connection_manager_t *self, const char *pipe_name , ma_named_pipe_connection_t *connection) {
    if (self && connection) {
		/* ma_message_consumer_t *consumer = NULL ; */
        /* commented search here as caller is already searching before adding the entry wherever it search required */
		/* if(MA_OK != ma_named_pipe_connection_manager_search(self , pipe_name , &consumer)) {*/
			q_connection_item_t *item = calloc(1, sizeof(q_connection_item_t));
			if (item) {
				item->connection = connection;
				item->pipe_name = strdup(pipe_name);
				ma_named_pipe_connection_add_ref(connection);
				ngx_queue_init(&item->qlink);
				ngx_queue_insert_tail(&self->qhead, &item->qlink);
                MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "named pipe <%s> is added into connection manager", pipe_name);
				return MA_OK;
			}
            MA_LOG(self->logger, MA_LOG_SEV_CRITICAL, "Out of Memory");
			return MA_ERROR_OUTOFMEMORY;
		/*}
		else {//Internal search
			ma_message_consumer_dec_ref(consumer);
            return MA_OK;
        }*/
    }
    return MA_ERROR_INVALIDARG;

}

ma_error_t ma_named_pipe_connection_manager_search(ma_named_pipe_connection_manager_t *self, const char *pipe_name , ma_message_consumer_t **consumer) {	
	if(self && pipe_name) {
        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "searching named pipe <%s> in connection manager", pipe_name);
		if(!ngx_queue_empty(&self->qhead))
		{		
			ngx_queue_t  *ngx_item;        
			q_connection_item_t *entry ;				
			ngx_queue_foreach(ngx_item, &self->qhead)  {
				entry = ngx_queue_data(ngx_item, q_connection_item_t, qlink);			
				if (entry && (!strcmp(entry->pipe_name , pipe_name))) {
					ma_message_consumer_inc_ref((ma_message_consumer_t *) entry->connection);
					*consumer = (ma_message_consumer_t *)entry->connection;
                    MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "named pipe <%s> found in connection manager", pipe_name);
					return MA_OK;					
				}
			}		
		}
        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "named pipe <%s> not found in connection manager", pipe_name);
		return MA_ERROR_MSGBUS_CONNECTION_NOT_FOUND;
	}
	return MA_ERROR_INVALIDARG;
	
}

ma_error_t ma_named_pipe_connection_manager_remove(ma_named_pipe_connection_manager_t *self, ma_named_pipe_connection_t *connection) {
    if (self && connection) {
        ngx_queue_t  *ngx_item;
        // find it, then yank it
        ngx_queue_foreach(ngx_item, &self->qhead)  {
            q_connection_item_t *entry = ngx_queue_data(ngx_item, q_connection_item_t, qlink);
            if (entry && (entry->connection == connection)) {
                MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Removed named pipe <%s> from connection manager", entry->pipe_name);
                ngx_queue_remove(ngx_item);
                ma_named_pipe_connection_release(connection);
                free(entry->pipe_name);
                free(entry);
                return MA_OK;
            }
        }
		// not there
		return MA_ERROR_OBJECTNOTFOUND;        
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_named_pipe_connection_manager_close_all(ma_named_pipe_connection_manager_t *self) {
    if (self) {
        ngx_queue_t  *ngx_item;
        ngx_queue_foreach(ngx_item, &self->qhead)  {
            q_connection_item_t *item = ngx_queue_data(ngx_item, q_connection_item_t, qlink);
            ma_named_pipe_connection_close(item->connection, 1);
        }
        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "named pipe connection manager closed all the connections");
        return MA_OK;        
    } 
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_named_pipe_connection_manager_set_logger(ma_named_pipe_connection_manager_t *self, ma_logger_t *logger) {
    if(self && logger) {
        self->logger = logger;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


