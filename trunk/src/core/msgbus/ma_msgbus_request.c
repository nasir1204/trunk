#include "ma/internal/core/msgbus/ma_msgbus_request.h"
#include "ma/internal/core/msgbus/ma_message_consumer.h"
#include "ma/internal/core/msgbus/ma_message_router.h"
#include "ma/internal/core/msgbus/ma_ep_lookup.h"
#include "ma/internal/core/msgbus/ma_msgbus_private.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/core/msgbus/ma_message_header.h"
#include "ma/internal/core/msgbus/ma_named_pipe_connection_manager.h"
#include "ma/internal/ma_macros.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h> /* sprintf */

#ifndef MA_WINDOWS
    #include <unistd.h>
#endif

/* global counter for correlation id in message bus requests */
static ma_uint32_t g_correlation_id = 0;

//Forward declarations
static void timer_close_cb(uv_handle_t *handle);
static ma_error_t ma_msgbus_request_destroy(ma_msgbus_request_t *self);

static void ma_msgbus_request_notify_caller(ma_msgbus_request_t *self);

static void ma_msgbus_request_close(ma_msgbus_request_t *self);

static void work_cb(uv_work_t *work) {
    ma_msgbus_request_t *self = (ma_msgbus_request_t *) work->data;
    MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "Worker callback is Invoked in msgbus request");
    self->callback(self->status, self->response_message, self->callback_data, self);

}

static void after_work_cb(uv_work_t *work UV_AFTER_WORK_PARAMS) {
    /* pool work no longer pending, release the corresponding ref count */
    /*MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "After Worker callback is Invoked in msgbus request"); */
    ma_msgbus_request_release((ma_msgbus_request_t*)work->data);
}


void ma_msgbus_request_notify_caller(ma_msgbus_request_t *self) {
    //cancel flags to be handled here if any..
    if(!(self->cancel_flags & MA_MSGBUS_NO_NOTIFY) && self->callback) {				
        switch (self->thread_model & MA_MSGBUS_CALLBACK_THREAD_MASK) {
        case MA_MSGBUS_CALLBACK_THREAD_POOL: {
            uv_loop_t *uv_loop = ma_event_loop_get_uv_loop(ma_msgbus_get_event_loop(self->mbus));

            MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "Invoking user callback in different thread");

            /* increment reference count while pool work is pending */
            ma_msgbus_request_add_ref(self);

            self->uv_work.data = self;
            if (0 == uv_queue_work(uv_loop, &self->uv_work, &work_cb, &after_work_cb)) {
                return;
            } 
            /* Something went wrong */
            /* this is fairly catastrophic, how can we let the caller know it's gone wrong ? */
			if(self->response_message) ma_message_release(self->response_message);
            ma_msgbus_request_release(self);
            break;
                                             }

        case MA_MSGBUS_CALLBACK_THREAD_IO:
            MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "Invoking user callback in I/O thread");
            self->callback(self->status, self->response_message, self->callback_data, self);
        }
    }
}

static void timer_close_cb(uv_handle_t *handle) {
    /* request no longer in route so release it from that perspective */
   /*  MA_LOG(msgbus_logger, MA_LOG_SEV_DEBUG, "Timer close callback is Invoked"); */
    ma_msgbus_request_release((ma_msgbus_request_t*)handle->data);
}

static void ma_msgbus_request_close(ma_msgbus_request_t *self) {

    MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "Request Object Closes");
   
    // removes the consumer from the router once it receives the expected response or failure message
    // Note, ref count is affected
    if (MA_MESSAGE_SUB_TYPE_ONEWAY != GET_MESSAGE_SUB_TYPE(self->request_message))
        ma_message_router_remove_consumer(ma_msgbus_get_message_router(self->mbus), (ma_message_consumer_t*)self);

    uv_close((uv_handle_t *)&self->uv_timer, timer_close_cb);        
}


static ma_error_t consumer_on_message(ma_message_consumer_t *consumer, ma_message_t *response) {

    ma_msgbus_request_t *self = (ma_msgbus_request_t *)consumer->data;	
    if(response) {				
        //Either response from endpoint or failure messages from ep_connect.
        if((!strcmp(GET_MESSAGE_DESTINATION_NAME(response) , self->ephemeral_name_buffer)
            && MA_MESSAGE_TYPE_REPLY == GET_MESSAGE_TYPE(response)
            && self->correlation_id == GET_MESSAGE_CORRELATION_ID(response)))
        {		
            MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "Recevied expected response to message bus request");
            uv_timer_stop(&self->uv_timer);

            self->status = (ma_error_t)GET_MESSAGE_STATUS(response);

            /* hold on to response to be presented in callback */
            ma_message_add_ref(self->response_message = response);

            ma_msgbus_request_notify_caller(self);

			if(self->ep_cookie) ma_ep_lookup_stop((ma_ep_lookup_t *)self->ep_consumer, self->ep_cookie);
			ma_message_consumer_release(self->ep_consumer);
            ma_msgbus_request_close(self);
        }
    }
    return MA_OK;
}


static void consumer_destroy(ma_message_consumer_t *consumer) {	
    ma_msgbus_request_destroy((ma_msgbus_request_t*)consumer->data);
}

static const ma_message_consumer_methods_t consumer_methods = {
    &consumer_on_message,
    &consumer_destroy
};	

// Creates a handle to the specified message request
ma_error_t ma_msgbus_request_create(ma_msgbus_t *mbus, ma_msgbus_request_t **request)
{	
    ma_msgbus_request_t *self = (ma_msgbus_request_t*)calloc(1, sizeof(ma_msgbus_request_t));    
    if(self){				
        self->mbus = mbus;

        // Assigning consumer methods, data and ref count.
        ma_message_consumer_init(&self->consumer, &consumer_methods, 1, self);       

        *request = self; 

        MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "Messagebus Request object Created");

        return MA_OK;		   	
    }
    MA_LOG(ma_msgbus_get_logger(mbus), MA_LOG_SEV_CRITICAL, "Out of Memory");
    return MA_ERROR_OUTOFMEMORY;   
}

// Destroys the message request handle and free memory allocations in this object if any.
static ma_error_t ma_msgbus_request_destroy(ma_msgbus_request_t *self) {
    if(self){
        MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "Messagebus Request object Destroys");
		ma_message_release(self->request_message);
        ma_msgbus_endpoint_release(self->endpoint);
        if(self->ep_name) free(self->ep_name);        
        free(self);
    }
    return MA_OK;
}

static void timer_cb(uv_timer_t* handle, int status) {
    ma_msgbus_request_t *self = (ma_msgbus_request_t *)handle->data;

    if(self) {
		self->status = MA_ERROR_TIMEOUT;
		MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "Timer callback is Invoked for request <%p>, request name <%s>", self, self->ephemeral_name_buffer);     
        if (MA_MESSAGE_SUB_TYPE_ONEWAY != GET_MESSAGE_SUB_TYPE(self->request_message)) {                   
            ma_msgbus_request_notify_caller(self);
        }
        
		if(self->ep_cookie)
			ma_ep_lookup_stop((ma_ep_lookup_t *)self->ep_consumer, self->ep_cookie);
		
		if(self->is_request_send)
			ma_message_consumer_release(self->ep_consumer);

        self->ep_consumer = NULL; self->ep_cookie = NULL;

        ma_msgbus_request_close(self);
    }
}

static ma_error_t ma_msgbus_request_send(ma_msgbus_request_t *self) {
	ma_error_t rc = MA_OK;
	
	// setting headers to the requested message..
	SET_MESSAGE_TYPE(self->request_message, MA_MESSAGE_TYPE_REQUEST);
	SET_MESSAGE_CORRELATION_ID(self->request_message, self->correlation_id);

    
    SET_MESSAGE_DESTINATION_NAME(self->request_message, self->ep_name);

    // Destination pid is set to zero here. Actual pid is set in proxy server of out proc server.
    SET_MESSAGE_DESTINATION_PID(self->request_message, 0);

	SET_MESSAGE_SOURCE_NAME(self->request_message , self->ephemeral_name_buffer);
	
    MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "Sending message to router - correlation id <%d> ephemeral name buffer <%s>", self->correlation_id, self->ephemeral_name_buffer);  

	// Registers and starts timer for time out ... if it fails to create the uv_timer object, it returns the error.

    if(MA_MESSAGE_SUB_TYPE_ONEWAY != GET_MESSAGE_SUB_TYPE(self->request_message))
        ma_message_router_add_consumer(ma_msgbus_get_message_router(self->mbus), (ma_message_consumer_t*)self, NULL, MA_MSGBUS_CLIENT);	
    	
    /* Let it go. Any response may arrive on message_consumer's on_message*/
    rc = ma_message_consumer_send((ma_message_consumer_t *)ma_msgbus_get_message_router(self->mbus), self->request_message);
 
    /* Once message is added into router, request object can be deleted immediately for one way requests */
    if(MA_MESSAGE_SUB_TYPE_ONEWAY == GET_MESSAGE_SUB_TYPE(self->request_message)) {
        if(uv_is_active((uv_handle_t *)&self->uv_timer)) 
            uv_timer_stop(&self->uv_timer);
        uv_timer_start(&self->uv_timer, timer_cb, 0, 0);  /* Starts zero timer callback to clean-up the request object */
    }

    return rc;
}

static void lookup_cb(ma_error_t status, ma_message_consumer_t *consumer, void *ep_cookie, void *cb_data) {
	ma_msgbus_request_t *self = (ma_msgbus_request_t *)cb_data;	
	
    MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "Message bus request <%p> lookup cb is invoked with status <%d> from ep lookup <%p> ", self, status, consumer);

	if(MA_ERROR_TIMEOUT == self->status) {
		MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "Message bus request <%p> expired", self);
		ma_message_consumer_release(consumer);
		return;
	}

	if(MA_ERROR_MSGBUS_EP_LOOKUP_IN_PROGRESS == status) {
		ma_message_consumer_release(consumer);
		self->ep_consumer = consumer;   
        self->ep_cookie = ep_cookie;
		return;
	}

	if(MA_OK == status) {
		self->ep_consumer = consumer;   
        self->ep_cookie = ep_cookie;
        
		if(MA_OK != ma_msgbus_request_send(self))
			MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_ERROR, "Failed to send message request, ephemeral name buffer <%s>", self->ephemeral_name_buffer);

        self->is_request_send = MA_TRUE;
   	}   
    	  
	if(MA_OK != status) {
        self->status = status;
		
		if(MA_MESSAGE_SUB_TYPE_ONEWAY != GET_MESSAGE_SUB_TYPE(self->request_message)) {
	        MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "Notifying error status <%d> to caller", status);
            
			uv_timer_stop(&self->uv_timer); /* Stops the timer also */            
			ma_msgbus_request_notify_caller(self);
            
			if(ep_cookie) 
				ma_ep_lookup_stop((ma_ep_lookup_t *)consumer, ep_cookie);
			
			ma_message_consumer_release(consumer);
            ma_msgbus_request_close(self);
		}
        else {
            /* I think it don't hit the is_request_send=MA_TRUE scenario as we have already deleted reference from ep_lookup for one way calls with timer 0 */
            if(!self->is_request_send) {
                if(ep_cookie) 
					ma_ep_lookup_stop((ma_ep_lookup_t *)consumer, ep_cookie);
				ma_message_consumer_release(consumer);
                ma_msgbus_request_close(self);
            }
        }
    }
}

/*  */
ma_error_t ma_msgbus_request_start(ma_msgbus_request_t *self, const char *service_name, ma_msgbus_consumer_reachability_t reachability) {
	
	if(self && service_name){	
		ma_error_t rc = MA_OK; 
        // Storing service name in this object to set this service name as destination name in message header in ma_msgbus_request_send()..
        self->ep_name = strdup(service_name);
        uv_timer_init(ma_event_loop_get_uv_loop(ma_msgbus_get_event_loop(self->mbus)), &self->uv_timer);
        self->uv_timer.data = self;
        MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "uv timer data(%p)\n", self);
     
        if(MA_OK == (rc = ma_ep_lookup_start(self->mbus, service_name, reachability, lookup_cb, self))) {            
            MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "local broker look up succeeded for service <%s> ans started timer with timeout <%d> for request <%p>", service_name, self->timeout_ms, self);
			self->correlation_id = ++g_correlation_id ;
			sprintf(self->ephemeral_name_buffer,ephemeral_name_template, MA_MSC_SELECT(_getpid(), getpid()), (unsigned long) self->correlation_id);
            uv_timer_start(&self->uv_timer, timer_cb, self->timeout_ms, 0);
		}
        else {
            /* Taking ref count on request object as timer close callback will do clean-up finally */
            (void) ma_msgbus_request_add_ref(self);
            uv_close((uv_handle_t *)&self->uv_timer, timer_close_cb); 
            MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_ERROR, "local broker look up failed for service <%s>", service_name);
		}

		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

// Increments request message reference count
ma_error_t ma_msgbus_request_add_ref(ma_msgbus_request_t *self) {    
    return ma_message_consumer_inc_ref(&self->consumer);
}

// Decrements request message reference count. release request-message object when the ref count is zero.
// Releases resources associated with a request
ma_error_t ma_msgbus_request_release(ma_msgbus_request_t *self) {
    MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "message bus request Released");
    return ma_message_consumer_dec_ref(&self->consumer);
}

// Cancels a request
ma_error_t ma_msgbus_request_cancel(ma_msgbus_request_t *self, int flags) {	
    if(self){
        MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "message bus request Cancelled");
        self->cancel_flags |= flags;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

// Retrieves the endpoint associated with the specified request
ma_error_t ma_msgbus_request_get_endpoint(ma_msgbus_request_t *self, ma_msgbus_endpoint_t **endpoint)
{	
    if(self){
        if(NULL != (*endpoint = self->endpoint))
            return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

