/*
 * C wrapper over VSCore's C++ VtpValidationControl interface
 */

#ifndef MA_VSCORE_VTP_INCLUDED_H
#define MA_VSCORE_VTP_INCLUDED_H

#include "ma/ma_common.h"

MA_CPP(extern "C" {)

typedef struct ma_vscore_vtp_s ma_vscore_vtp_t, *ma_vscore_vtp_h;
typedef struct ma_vscore_vtp_module_s ma_vscore_vtp_module_t, *ma_vscore_vtp_module_h;

/*
static const validation_state_t	VALIDATION_STATE_UNKNOWN =	0x00;
static const validation_state_t	VALIDATION_STATE_INVALID =	0x01;
static const validation_state_t	VALIDATION_STATE_VALID	 =	0x02;
static const validation_state_t VALIDATION_STATE_INFLUX	 =	0x04;
static const validation_state_t	VALIDATION_STATE_ERROR	 =	0x08;
static const validation_state_t	VALIDATION_STATE_CHAINED =	0x10;
static const validation_state_t	VALIDATION_STATE_FROM_CACHE =	0x20;
*/


/*!
 * Creates an instance of vtp control interface
 */
ma_error_t ma_vscore_vtp_create(ma_vscore_vtp_t **vtp);

/*!
 * Releases the vtp control interface and any associated resources
 */
ma_error_t ma_vscore_vtp_release(ma_vscore_vtp_t *vtp);


typedef ma_error_t (*ma_vscore_vtp_validate_process_ex_cb)(ma_vscore_vtp_module_h module, void *cb_data);

/*!
 * wrapper over VtpValidationControl::ValidateProcessEx()
 * The supplied callback is invoked for each module that vtp finds 
 */
ma_error_t ma_vscore_vtp_validate_process_ex(ma_vscore_vtp_t *vtp, /*ma_pid_t*/unsigned long pid, ma_vscore_vtp_validate_process_ex_cb cb, void *cb_data);

ma_bool_t ma_vscore_vtp_module_is_main_executable(ma_vscore_vtp_module_h module);

const char *ma_vscore_vtp_module_get_name(ma_vscore_vtp_module_h module);

const char *ma_vscore_vtp_module_get_signer(ma_vscore_vtp_module_h module);

ma_bool_t ma_vscore_vtp_module_is_trusted(ma_vscore_vtp_module_h module);


MA_CPP(})

#endif /* MA_VSCORE_VTP_INCLUDED_H */
