
#include "ma/internal/core/msgbus/ma_domain_name_server.h"
#include "ma/internal/core/msgbus/ma_local_broker.h"
#include "ma/internal/core/msgbus/ma_msgbus_impl.h"
#include "ma/internal/core/msgbus/ma_message_consumer.h"
#include "ma/internal/core/msgbus/ma_ep_connect.h"
#include "ma/internal/core/msgbus/ma_named_pipe_connection_manager.h"
#include "ma/internal/core/msgbus/ma_message_router.h"
#include "ma/internal/core/msgbus/ma_message_header.h"

#include "uv-private/ngx-queue.h" /*If they can use it, so can we */


#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdio.h> /* sprintf */

const static char ephemeral_name_template[] = "ma.lookup.internal.request.%08X";

struct ma_lookup_entry_s {	
	char					*endpoint_name ;
	long					pid;
	char					*pipe_name;
	char					*host_name;
} ;


typedef struct lookup_callbacks_entry_s {
	ngx_queue_t				qlink;
	ma_dns_lookup_cb		cb ;
	void					*cb_data;

}lookup_callbacks_entry_t;

typedef struct lookup_entry_s {
	ngx_queue_t				qlink;
	char					*endpoint_name ;
	ngx_queue_t				qhead_lookup_callbacks;	
}lookup_entry_t ;


typedef struct resolve_callbacks_entry_s {
	ngx_queue_t				qlink;
	ma_dns_resolve_cb		cb ;
	void					*cb_data;
	
}resolve_callbacks_entry_t;

struct ma_resolve_entry_s {
	ngx_queue_t				qlink;
	ma_atomic_counter_t     ref_count;
	char					*endpoint_name ;
	ma_lookup_entry_t		info ;
	ma_message_consumer_t	*ep_consumer;
	ngx_queue_t				qhead_resolve_callbacks;	
};

struct ma_dns_s {
    /*! yes, I am a message consumer */
    /* Please keep as first field in this struct */
    ma_message_consumer_t   consumer;

    /*! queue of lookups to be resolved */ 
    ngx_queue_t             qhead_lookup;

	/*! queue of lookups to be resolved */ 
    ngx_queue_t             qhead_resolve;

	ma_ep_connect_t			*ep_broker_consumer;
    
	ma_msgbus_t				*mbus;

	ma_uint32_t				correlation_id;

	/*! this objects ephemeral name */
    char                    ephemeral_name_buffer[8+sizeof(ephemeral_name_template)]; /* */

};

static void invoke_lookups_callbacks_for_this_endpoint(ma_dns_t *self , ma_message_t *msg);

static ma_error_t consumer_on_message(ma_message_consumer_t *consumer, ma_message_t *msg)  {
	ma_dns_t *self = (ma_dns_t *) consumer ;

	//Filter the messages only which are targeted for us
	if(msg && !strcmp(GET_MESSAGE_DESTINATION_NAME(msg) , self->ephemeral_name_buffer) 
		&& (MA_MESSAGE_TYPE_REPLY == GET_MESSAGE_TYPE(msg)) 
		&& (self->correlation_id == GET_MESSAGE_CORRELATION_ID(msg))) {

		ma_error_t rc = MA_OK ;		
		invoke_lookups_callbacks_for_this_endpoint(self , msg );

		if(ngx_queue_empty(&self->qhead_lookup)) {		
			ma_message_router_remove_consumer(ma_msgbus_get_message_router(self->mbus) , (ma_message_consumer_t *) self);
			ma_ep_connect_release(self->ep_broker_consumer);
		}
		//ma_message_dec_ref(msg);		
	}	
	return MA_OK ;
}

static void consumer_destroy(ma_message_consumer_t *base) {    
}

static const ma_message_consumer_methods_t consumer_methods = {
    &consumer_on_message,
    &consumer_destroy
};



ma_error_t ma_dns_create(ma_msgbus_t *bus , ma_dns_t **dns) {
	ma_dns_t *self = (ma_dns_t *)calloc(1, sizeof(ma_dns_t));
	if(bus && self) {
		self->mbus = bus ;
		
		ma_message_consumer_init(&self->consumer, &consumer_methods, 1, self);
		ngx_queue_init(&self->qhead_lookup);
		ngx_queue_init(&self->qhead_resolve);
		*dns = self;
		return MA_OK ;
	}
	return MA_ERROR_OUTOFMEMORY ;
}

ma_error_t ma_dns_release(ma_dns_t *self) {
	if(self) {
		//TODO - clean up the stuff it will be tricky
		free(self);
		return MA_OK ;
	}
	return MA_ERROR_INVALIDARG;
}


static ma_error_t start_lookup(ma_dns_t *self ,const char *endpoint_name, ma_dns_lookup_cb cb, void *cb_data) ;
static	ma_lookup_entry_t *create_lookup_entry(const char *endpoint_name , long pid , const char *pipe_name , const char *host_name);

ma_error_t ma_dns_lookup(ma_dns_t *self ,  ma_msgbus_consumer_reachability_t lookup_reach ,  const char *endpoint_name, ma_dns_lookup_cb cb , void *cb_data) {
	if(self && endpoint_name && cb) {
		ma_error_t rc = MA_OK ;		
		ma_message_consumer_t *consumer = NULL;
		ma_lookup_entry_t  *lookup = NULL ;		

		if(MA_OK == (rc = ma_local_broker_consumer_search(ma_msgbus_get_local_broker(self->mbus) , MSGBUS_REQ_REACH_INPROC , endpoint_name , &consumer) )) {		
			lookup = create_lookup_entry(endpoint_name  , 0 , NULL , NULL);
			if(!lookup) return MA_ERROR_OUTOFMEMORY ;
		}			

		switch(lookup_reach) {
		case MSGBUS_REQ_REACH_INPROC :				
			if(MA_OK == rc ) 
				cb(self , MA_OK , lookup , cb_data);				
			else
				rc = MA_ERROR_SERVICE_NOT_FOUND ;
			break;
		case MSGBUS_REQ_REACH_OUTPROC :
		case MSGBUS_REQ_REACH_OUTBOX :
			if(MA_OK == rc ) 
				cb(self , MA_OK , lookup , cb_data );			
			else
				rc = start_lookup(self, endpoint_name, cb, cb_data);
			break ;
		default:
			rc = MA_ERROR_UNEXPECTED ;
			break;
		}	
		return rc ;
	}
	return MA_ERROR_INVALIDARG;
}


ma_error_t ma_dns_lookup_stop(ma_dns_t *self , const char *endpoint_name) {
	return MA_OK;
}

ma_error_t ma_dns_resolve(ma_dns_t *self ,  ma_lookup_entry_t *lookup_entry, ma_dns_resolve_cb cb , void *cb_data) {
	return MA_OK ;
}

ma_error_t ma_dns_resolve_stop(ma_dns_t *self ,ma_lookup_entry_t *lookup_entry) {
	return MA_OK;
}

long ma_lookup_entry_get_pid(ma_lookup_entry_t *self) {
	if(self)
		self->pid ;
	return -1 ;
}

ma_error_t ma_lookup_entry_release(ma_lookup_entry_t *self) {
	if(self) {
		if(self->endpoint_name) free(self->endpoint_name);
		if(self->pipe_name) free(self->pipe_name);
		if(self->host_name) free(self->host_name);
		free(self);
	}
	return MA_ERROR_INVALIDARG;

}

ma_error_t ma_resolve_entry_release(ma_resolve_entry_t *self) {
	return MA_OK ;
}


static lookup_entry_t *search_entry(ma_dns_t *self , const char *endpoint_name) {
	
	if(!ngx_queue_empty(&self->qhead_lookup))
	{		
		ngx_queue_t  *ngx_item = NULL;        
		lookup_entry_t *entry = NULL ;				
		ngx_queue_foreach(ngx_item, &self->qhead_lookup)  {
			entry = ngx_queue_data(ngx_item, lookup_entry_t, qlink);			
			if (entry && (!strcmp(entry->endpoint_name , endpoint_name))) 
				return entry;							
		}
	}
	return NULL;		
}


static ma_error_t create_lookup_request_message(ma_dns_t *self  , const char *endpoint_name  , ma_message_t **lookup_request) {
	ma_message_t *msg = NULL ;
	ma_error_t rc = MA_OK ;
	if(MA_OK == (rc = ma_message_create(MA_MESSAGE_TYPE_REQUEST , &msg) )) {		
		sprintf(self->ephemeral_name_buffer,ephemeral_name_template,self->correlation_id);
		SET_MESSAGE_SOURCE_NAME(msg , self->ephemeral_name_buffer);		
		SET_MESSAGE_CORRELATION_ID(msg , self->correlation_id++); 
		SET_MESSAGE_TYPE(msg , MA_MESSAGE_TYPE_REQUEST);
		SET_MESSAGE_DESTINATION_NAME(msg , ma_msgbus_get_broker_name(self->mbus));
		SET_MESSAGE_DESTINATION_PID(msg , ma_msgbus_get_broker_pid(self->mbus));
		ma_message_set_property(msg , "REQUEST_TYPE" , "GET_PIPE_NAME");
		ma_message_set_property(msg , "ENDPOINT_NAME" , endpoint_name);
		*lookup_request = msg;		
	}
	return rc;
}

//TODO - Need to clean up if the connection fails for some reason
static void ep_connect_start_cb(ma_ep_connect_t *connect, int status, char *pipe_name , char *host_name , void *cb_data) {
	
}

static ma_error_t create_connection_to_broker(ma_dns_t *self) {
	ma_error_t rc = MA_OK ;
	if(MA_OK == (rc = ma_ep_connect_create(ma_msgbus_get_broker_pipe_name(self->mbus) , NULL , &self->ep_broker_consumer))){
		if(MA_OK == (rc = ma_ep_connect_start(self->ep_broker_consumer , ep_connect_start_cb , self))) 
			return MA_OK ;
		else
			ma_ep_connect_release(self->ep_broker_consumer);
	}
	return rc;
}

static ma_error_t start_lookup(ma_dns_t *self , const char *endpoint_name, ma_dns_lookup_cb cb, void *cb_data) {	
	lookup_callbacks_entry_t *cb_item = NULL ;
	lookup_entry_t *lookup_item = NULL ;	
	ma_error_t rc = MA_OK ;

	cb_item = (lookup_callbacks_entry_t *)calloc(1, sizeof(lookup_callbacks_entry_t));

	if(!cb_item) 	return MA_ERROR_OUTOFMEMORY;		

	cb_item->cb = cb ;
	cb_item->cb_data = cb_data ;

	/*Search it in the list may be we are already		looking for it */
	lookup_item = search_entry(self , endpoint_name) ;
	if(lookup_item) {
		ngx_queue_insert_tail(&lookup_item->qhead_lookup_callbacks, &cb_item->qlink);			
	}
	else {
		ma_message_t *lookup_request = NULL ;		
	
		lookup_item = (lookup_entry_t *)calloc(1, sizeof(lookup_entry_t));
		if(!lookup_item) {
			free(cb_item);		
			return MA_ERROR_OUTOFMEMORY;
		}							

		if(MA_OK  != (rc = create_lookup_request_message(self , endpoint_name , &lookup_request))) {
			free(cb_item);
			free(lookup_item);
			return rc ;
		}

		if(MA_OK != (rc = ma_named_pipe_connection_manager_search(ma_msgbus_get_named_pipe_connection_manager(self->mbus) ,ma_msgbus_get_broker_pipe_name(self->mbus))))
			rc = create_connection_to_broker(self);

		if(MA_OK != rc ) {
			free(cb_item);
			free(lookup_item);
			ma_message_dec_ref(lookup_request);			
			return rc; 
		}

		ngx_queue_insert_tail(&self->qhead_lookup, &lookup_item->qlink);

		ngx_queue_init(&lookup_item->qhead_lookup_callbacks);
		ngx_queue_insert_tail(&lookup_item->qhead_lookup_callbacks, &cb_item->qlink);

		ma_message_router_add_consumer(ma_msgbus_get_message_router(self->mbus) , (ma_message_consumer_t *) self) ;
		ma_message_consumer_send((ma_message_consumer_t *)ma_msgbus_get_message_router(self->mbus) , lookup_request);		
	}

	return rc;
	
}

static	ma_lookup_entry_t *create_lookup_entry(const char *endpoint_name , long pid , const char *pipe_name , const char *host_name) {
	ma_lookup_entry_t *self = (ma_lookup_entry_t *)calloc(1 , sizeof(ma_lookup_entry_t));
	if(self) {
		if(endpoint_name) self->endpoint_name = strdup(endpoint_name);
		self->pid = pid;
		if(pipe_name) self->pipe_name = strdup(pipe_name);
		if(host_name) self->host_name = strdup(host_name);
		return self;
	}
	return NULL;
}


static void invoke_lookups_callbacks_for_this_endpoint(ma_dns_t *self , ma_message_t *msg ) {	
	char *endpoint_name = NULL ;
	lookup_entry_t *lookup_item = NULL ;
		
	
	if(MA_OK != ma_message_get_property(msg , "ENDPOINT_NAME" , &endpoint_name))
		return ;
	
	lookup_item = search_entry(self , endpoint_name) ;
	if(!lookup_item)
		return ;
	else {	
		ma_error_t rc = MA_OK ;					
		ma_lookup_entry_t *lookup_entry = NULL ;

		if(MA_OK == (rc = (ma_error_t)GET_MESSAGE_STATUS(msg))) {
			char *pipe_name = NULL , *host_name = NULL ;
			ma_message_get_property(msg , "PIPE_NAME" , &pipe_name);
			ma_message_get_property(msg , "HOST_NAME" , &host_name);
			
			lookup_entry = create_lookup_entry(endpoint_name , GET_MESSAGE_SOURCE_PID(msg) ,pipe_name , host_name);
			if(!lookup_entry) rc = MA_ERROR_OUTOFMEMORY ;			
		}
		
		while (!ngx_queue_empty(&lookup_item->qhead_lookup_callbacks)) {
			ngx_queue_t *item = ngx_queue_head(&lookup_item->qhead_lookup_callbacks);
			lookup_callbacks_entry_t *cb_entry = ngx_queue_data(item, lookup_callbacks_entry_t, qlink);
			if(cb_entry ) {
				if(cb_entry->cb) 	cb_entry->cb(self , rc , lookup_entry , cb_entry->cb_data);
				ngx_queue_remove(item);
				free(cb_entry);
			}
		}
	}	
}



