#include "ma/internal/core/msgbus/ma_message_router.h"
#include "ma/internal/core/msgbus/ma_message_header.h"
#include "ma/internal/core/msgbus/ma_msgbus_private.h"

#include "uv-private/ngx-queue.h" /* for ngx_ */

#include <stdlib.h>
#include <assert.h>
#include <string.h>

typedef struct ma_router_list_entry_s {
    /*! set of consumers */ 
    ngx_queue_t                 qlink;

    ma_message_consumer_t       *consumer;

    char                        *consumer_name;

    ma_message_consumer_type_t  consumer_type;

} ma_router_list_entry_t;

struct ma_message_router_s {
    /* ma_message_router_s technically derives from ma_message_consumer_t, let it be the first member... */
    ma_message_consumer_t consumer_impl;

    ngx_queue_t             qhead;

    ma_logger_t             *logger;

	ngx_queue_t				*ngx_next_item;
};

static unsigned short matches_generic_filter(ma_message_consumer_type_t type, ma_message_t *msg) {
    switch(type) {
        case MA_MSGBUS_SERVER ://Server only needs Request type messages
            return MA_MESSAGE_TYPE_REQUEST == GET_MESSAGE_TYPE(msg) ;
        case MA_MSGBUS_PROXY_SERVER: // proxy of outproc server - it needs request or reply type messages. Signal type messages too..
            return (MA_MESSAGE_TYPE_REQUEST == GET_MESSAGE_TYPE(msg)) || (MA_MESSAGE_TYPE_REPLY == GET_MESSAGE_TYPE(msg)) || (MA_MESSAGE_TYPE_SIGNAL == GET_MESSAGE_TYPE(msg));
        case MA_MSGBUS_CLIENT ://Client only needs Reply type messages
            return MA_MESSAGE_TYPE_REPLY == GET_MESSAGE_TYPE(msg) ;
        case MA_MSGBUS_LOCAL_BROKER ://Local broker needs Reply type messages and signal type
            return MA_MESSAGE_TYPE_REPLY == GET_MESSAGE_TYPE(msg) || (MA_MESSAGE_TYPE_SIGNAL == GET_MESSAGE_TYPE(msg)) ;
        case MA_MSGBUS_SUBSCRIBER : //Subscriber only needs publish type messages
            return MA_MESSAGE_TYPE_PUBLISH == GET_MESSAGE_TYPE(msg) ;
        case MA_MSGBUS_NAMED_PIPE_CONNECTION ://Name pipe connection should only send messages which are sent to be outside , i.e. PID should not be zero
            return 0 != GET_MESSAGE_DESTINATION_PID(msg) ;        
        default:
            return 0 ;
    }
}

static ma_error_t consumer_on_message(ma_message_consumer_t *consumer, ma_message_t *msg) {
    ma_message_router_t *self = (ma_message_router_t *) consumer->data;
    /* fan out to every attached consumer */
 	ngx_queue_t  *ngx_item = ngx_queue_head(&self->qhead);
	while(ngx_item != ngx_queue_sentinel(&self->qhead) && !ngx_queue_empty(&self->qhead))
	{		
		ma_router_list_entry_t *entry = NULL;
		self->ngx_next_item = ngx_queue_next(ngx_item);
		entry = ngx_queue_data(ngx_item, ma_router_list_entry_t, qlink);
        /* Someone can pass the NULL message too and in that case we will not do generic filter match
        , not sure who will do it though other than our unit test cases.
        */
        if (entry && entry->consumer && (!msg || matches_generic_filter(entry->consumer_type , msg)) ) {
            ma_message_consumer_send(entry->consumer, msg);
        }
		ngx_item = self->ngx_next_item;
	}
    return MA_OK;
}

static ma_error_t ma_message_router_destroy(ma_message_router_t *self);

static void consumer_destroy(ma_message_consumer_t *self) {
    ma_message_router_destroy((ma_message_router_t *) self->data);
}


static const ma_message_consumer_methods_t methods = {
    &consumer_on_message,
    &consumer_destroy
};


ma_error_t ma_message_router_create(ma_message_router_t **router) {
    if (router) {
        ma_message_router_t *self = (ma_message_router_t *) calloc(1, sizeof(ma_message_router_t));
        if (self) {
            ma_message_consumer_init(&self->consumer_impl, &methods, 1, self);

            ngx_queue_init(&self->qhead);

            *router = self;    
            return MA_OK;
        }        
        return MA_ERROR_OUTOFMEMORY;
    }    
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_message_router_release(ma_message_router_t *router) {
    return ma_message_consumer_dec_ref(&router->consumer_impl);
}


static ma_error_t ma_message_router_destroy(ma_message_router_t *self) {
    if (self) {
        while (!ngx_queue_empty(&self->qhead)) {
            ngx_queue_t *ngx_item = ngx_queue_head(&self->qhead);
            ma_router_list_entry_t *item = ngx_queue_data(ngx_item, ma_router_list_entry_t, qlink);
            ngx_queue_remove(ngx_item);
            if(item->consumer) free(item->consumer_name);
            ma_message_consumer_dec_ref(item->consumer);
            free(item);            
        }
        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Message router destroyed");
        free(self);
    }
    return MA_OK;
}

ma_error_t ma_message_router_add_consumer(ma_message_router_t *self, ma_message_consumer_t *consumer, const char *consumer_name, ma_message_consumer_type_t consumer_type) {
    if (self && consumer) {
        ma_router_list_entry_t *item = (ma_router_list_entry_t *) calloc(1, sizeof(ma_router_list_entry_t));
        if (item) {
            item->consumer = consumer;
            if(consumer_name) item->consumer_name = strdup(consumer_name);
            item->consumer_type = consumer_type;
            ma_message_consumer_inc_ref(consumer);
            ngx_queue_insert_tail(&self->qhead, &item->qlink);
            return MA_OK;
        }
        MA_LOG(self->logger, MA_LOG_SEV_CRITICAL, "Out of Memory");
        return MA_ERROR_OUTOFMEMORY;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_message_router_remove_consumer(ma_message_router_t *self, ma_message_consumer_t *consumer) {
    if (self && consumer) {
        ngx_queue_t  *ngx_item;
        // find it, then yank it
        ngx_queue_foreach(ngx_item, &self->qhead)  {
            ma_router_list_entry_t *entry = ngx_queue_data(ngx_item, ma_router_list_entry_t, qlink);
            if (entry && (entry->consumer == consumer)) {
				
				/* If the remove item matches with next item in router on message call, update with next item */
				if(self->ngx_next_item == ngx_item)
					self->ngx_next_item = ngx_queue_next(ngx_item);

				ngx_queue_remove(ngx_item);
                if(entry->consumer_name) free(entry->consumer_name);
                ma_message_consumer_dec_ref(entry->consumer);
                free(entry);
                return MA_OK;
            }
        }
        // not there
    }
    return MA_ERROR_INVALIDARG;
}

void ma_message_router_dump_consumers(ma_message_router_t *self) {
    if (self) {
        if(!ngx_queue_empty(&self->qhead))
	    {		
		    ngx_queue_t  *ngx_item = NULL;        
		    ma_router_list_entry_t *entry = NULL;				
		    ngx_queue_foreach(ngx_item, &self->qhead)  {
			    entry = ngx_queue_data(ngx_item, ma_router_list_entry_t, qlink);				
                if (entry && (entry->consumer_name)) 
					MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "consumer name <%s> , consumer <%p> type <%d>", entry->consumer_name, entry->consumer, entry->consumer_type);			    
		    }
	    }        
    }
}

ma_error_t ma_message_router_search_consumer(ma_message_router_t *self, const char *consumer_name , ma_msgbus_consumer_reachability_t lookup_reach, ma_message_consumer_type_t *consumer_type, ma_message_consumer_t **endpoint_consumer) {
    if (self && consumer_name && endpoint_consumer && consumer_type) {          
        if(!ngx_queue_empty(&self->qhead))
	    {		
		    ngx_queue_t  *ngx_item = NULL;        
		    ma_router_list_entry_t *entry = NULL;				
		    ngx_queue_foreach(ngx_item, &self->qhead)  {
			    entry = ngx_queue_data(ngx_item, ma_router_list_entry_t, qlink);				
                if (entry && (entry->consumer_name) && (!strcmp(entry->consumer_name, consumer_name))) {
                    // If caller is searching only for local services , then return MA_MSGBUS_SERVER type consumer only
                    if((MSGBUS_CONSUMER_REACH_INPROC == lookup_reach) && (MA_MSGBUS_SERVER != entry->consumer_type))
                        continue;
                    *endpoint_consumer = entry->consumer;
                    *consumer_type = entry->consumer_type;
                    MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "<%s> service found in router", consumer_name);
				    return MA_OK ;                    
			    }
		    }
	    }
        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "<%s> service is not available in router", consumer_name);
	    return MA_ERROR_MSGBUS_SERVICE_NOT_FOUND;	
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_message_router_set_logger(ma_message_router_t *self, ma_logger_t *logger) {
    if(self && logger) {
        self->logger = logger;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

