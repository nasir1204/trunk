#include <stdlib.h>
#include <string.h>

#include "uv.h"

#include "ma/internal/core/msgbus/ma_ep_connect_request.h"
#include "ma/internal/core/msgbus/ma_message_consumer.h"
#include "ma/internal/core/msgbus/ma_message_router.h"
#include "ma/internal/core/msgbus/ma_msgbus_impl.h"
#include "ma/internal/core/msgbus/ma_named_pipe_connection.h"
#include "ma/internal/core/msgbus/ma_named_pipe_connection_manager.h"

#include "ma/internal/core/msgbus/ma_local_broker.h"
#include "ma/internal/core/msgbus/ma_message_header.h"



//TODO - Optimize the container , as mostly we dont need broker connection handle
struct ma_ep_connect_request_s {  
	 /*! It is a message consumer -- Please keep as first field in this struct */
    ma_message_consumer_t			consumer;	

	/*! endpoint name whom we are trying to connect */
	char *endpoint_name ;
	
	/*! connection to the end point */
	ma_named_pipe_connection_t *connection ;				

	/*! broker  connection consumer */
	ma_message_consumer_t	*broker_consumer ;

	/*! self correlation id , we will not much of coorelation id */
	ma_uint32_t		correlation_id ;

	/*! extra copy of the bus */
	ma_msgbus_t *bus ;

};

//TODO - need to think about filtering properly 
//TODO - neeto to think about proper connection error messages
//TODO - check reference counting

/* This will be used when he already have the pipe name or may be it's MA Broker , whose pipe name we already know
*/
static ma_error_t create_connection_failure_message(ma_ep_connect_request_t *self  , ma_error_t status ,  ma_message_t **connection_failure_msg) {
	ma_message_t *msg = NULL ;
	ma_error_t rc = MA_OK ;
	if(MA_OK == (rc = ma_message_create(MA_MESSAGE_TYPE_SIGNAL , &msg) )) {		
		SET_MESSAGE_STATUS(msg ,  status );				
		SET_MESSAGE_TYPE(msg , MA_MESSAGE_TYPE_SIGNAL);
		SET_MESSAGE_DESTINATION_NAME(msg , self->endpoint_name);
		*connection_failure_msg = msg;		
	}
	return rc;
}

static void c_endpoint_connection_cb(ma_named_pipe_connection_t *connection, int status, void *cb_data) {
	ma_ep_connect_request_t *self = (ma_ep_connect_request_t *) cb_data ;	
	if(MA_OK == status) {		
		/*Now that the connection is established  ????*/ 
		ma_named_pipe_connection_start_receiving(connection);			
	}
	else { //error condition		  						
		ma_message_t *connection_failure_msg = NULL ;
		ma_error_t status = MA_ERROR_SERVICE_CONNECT_FAILED ;
		//Unregister from local broker as no new request should be able to find it out
		ma_local_broker_consumer_unregister(ma_msgbus_get_local_broker(self->bus) , self->endpoint_name);

		//If it is broker then send the status as MA_ERROR_BROKER_CONNECT_FAILED
		status = !(strcmp(self->endpoint_name ,  ma_msgbus_get_broker_name(self->bus))) ? MA_ERROR_BROKER_CONNECT_FAILED : MA_ERROR_SERVICE_CONNECT_FAILED;
		if(MA_OK == create_connection_failure_message(self ,status ,  &connection_failure_msg)) 
			ma_message_consumer_send((ma_message_consumer_t*) ma_msgbus_get_message_router(self->bus), connection_failure_msg);
		
	}	
}

static ma_error_t consumer_on_message(ma_message_consumer_t *consumer, ma_message_t *msg) {			
	ma_ep_connect_request_t *self = (ma_ep_connect_request_t *) consumer ;	

	//Filter only from broker and any if connection failed to broker 
	if(msg) {
		if( ((MA_MESSAGE_TYPE_REPLY == GET_MESSAGE_TYPE(msg)) 
			&& ((self->correlation_id == GET_MESSAGE_CORRELATION_ID(msg)))
			&& (0 == strcmp(GET_MESSAGE_DESTINATION_NAME(msg) , ma_msgbus_get_broker_name(self->bus)))) 
			||( (MA_MESSAGE_TYPE_SIGNAL == GET_MESSAGE_TYPE(msg))  
			&& ( MA_ERROR_BROKER_CONNECT_FAILED == GET_MESSAGE_STATUS(msg)))) {
		
			ma_error_t rc = MA_OK ;		

			///Remove yourself from consumer , now that we got our message we don't need any more message 
			ma_message_router_remove_consumer(ma_msgbus_get_message_router(self->bus) ,(ma_message_consumer_t*) self);

			if(MA_OK == (rc = (ma_error_t)GET_MESSAGE_STATUS(msg))) {
				//TODO if pipe name needs to be copied locally as we are going to decrease the message reference count
				char *value = NULL ;	
				if((MA_OK == rc ) && (MA_OK == (rc = ma_message_get_property(msg , "PIPE_NAME" , &value)))) 
					rc = ma_named_pipe_connection_connect(self->connection , value , c_endpoint_connection_cb , self);
			}
			if(MA_OK != rc ) {
				ma_message_t *connection_failure_msg = NULL ;
				if(MA_OK == create_connection_failure_message(self ,  rc , &connection_failure_msg))
					ma_message_consumer_send((ma_message_consumer_t*) ma_msgbus_get_message_router(self->bus), connection_failure_msg);		
				//Unregister from local broker as no new request should be able to find it out
				ma_local_broker_consumer_unregister(ma_msgbus_get_local_broker(self->bus) , self->endpoint_name);
			}
		
			ma_message_dec_ref(msg);		
		}
	}
	return MA_OK;
}

static ma_error_t ma_ep_connect_request_destroy(ma_ep_connect_request_t *self);

static void consumer_destroy(ma_message_consumer_t *consumer) {	
	ma_ep_connect_request_destroy((ma_ep_connect_request_t *)consumer->data);
}
 
static const ma_message_consumer_methods_t consumer_methods = {
    &consumer_on_message,
    &consumer_destroy
};	

// Creates a handle to the specified endpoint connection request
ma_error_t ma_ep_connect_request_create(const char *endpoint_name ,  ma_ep_connect_request_t **request){	
	if(endpoint_name && request ) {
		ma_ep_connect_request_t *self = (ma_ep_connect_request_t *)calloc(1, sizeof(ma_ep_connect_request_t));	    
		if(self) {								
			if(NULL == (self->bus = ma_msgbus_get_instance())) {
				free(self);
				return MA_ERROR_UNEXPECTED;
			}				
			self->endpoint_name = strdup(endpoint_name);				

			// Assigning consumer methods , data and ref count.
			ma_message_consumer_init(&self->consumer, &consumer_methods, 1, self);			
			*request = self; 
			return MA_OK ;
		}
		return MA_ERROR_OUTOFMEMORY ;
	}
	return MA_ERROR_INVALIDARG ;
}


static ma_error_t create_request_message_for_pipe_name_to_broker(ma_ep_connect_request_t *self ,  ma_message_t **pipe_name_msg) {
	ma_message_t *msg = NULL ;
	ma_error_t rc = MA_OK ;
	if(MA_OK == (rc = ma_message_create(MA_MESSAGE_TYPE_REQUEST , &msg) )) {
		SET_MESSAGE_SOURCE_NAME(msg , self->endpoint_name);		
		SET_MESSAGE_CORRELATION_ID(msg , self->correlation_id); 
		SET_MESSAGE_TYPE(msg , MA_MESSAGE_TYPE_REQUEST);
		ma_message_set_property(msg , "REQUEST_TYPE" , "GET_PIPE_NAME");
		ma_message_set_property(msg , "ENDPOINT_NAME" , self->endpoint_name);
		*pipe_name_msg = msg;		
	}
	return rc;
}

static ma_error_t create_reply_message_as_pipe_name_from_broker(ma_ep_connect_request_t *self ,  ma_message_t **pipe_name_msg) {
	ma_message_t *msg = NULL ;
	ma_error_t rc = MA_OK ;
	if(MA_OK == (rc = ma_message_create(MA_MESSAGE_TYPE_REQUEST , &msg) )) {
		SET_MESSAGE_DESTINATION_NAME(msg , ma_msgbus_get_broker_name(self->bus));		
		SET_MESSAGE_CORRELATION_ID(msg , self->correlation_id); 
		SET_MESSAGE_TYPE(msg , MA_MESSAGE_TYPE_REPLY);
		ma_message_set_property(msg , "PIPE_NAME" , ma_msgbus_get_broker_pipe_name(self->bus));		
		*pipe_name_msg = msg;		
	}
	return rc;
}


ma_error_t ma_ep_connect_request_start( ma_ep_connect_request_t *self ) {	
	if(self){				
		ma_error_t rc = MA_OK ;
		ma_message_t *pipe_msg = NULL ;		
		unsigned short is_endpoint_ma_broker = 0;

		
		self->correlation_id++;		//TODO - need to think about global static counter		
		
		//If it is broker then send the connection message right away
		rc = !(strcmp(self->endpoint_name ,  ma_msgbus_get_broker_name(self->bus))) ? 
				create_reply_message_as_pipe_name_from_broker(self , &pipe_msg) : 
				create_request_message_for_pipe_name_to_broker(self , &pipe_msg);

		if(MA_OK == rc) {
			if(MA_OK == (rc = ma_named_pipe_connection_create(ma_msgbus_get_event_loop(self->bus) , &self->connection))) {	
				ma_message_router_t *router = ma_msgbus_get_message_router(self->bus);				
				//Add this connection into router 
				ma_message_router_add_consumer(router , (ma_message_consumer_t*)self->connection);
				//add yourself into router
				ma_message_router_add_consumer(router , (ma_message_consumer_t*)self);
				//Register yourself local broker 
				ma_local_broker_consumer_register( ma_msgbus_get_local_broker(self->bus) , CONSUMER_TYPE_NAMED_PIPE , self->endpoint_name , (ma_message_consumer_t *)self);

				ma_message_consumer_send((ma_message_consumer_t*)router, pipe_msg);						
			}
			else 
				ma_message_dec_ref(pipe_msg);
		}
		
		return rc ;
	}
	return MA_ERROR_INVALIDARG;
}


static ma_error_t ma_ep_connect_request_destroy(ma_ep_connect_request_t *self) {
	//TODO - check are we freeing up properly
	if(self) {		
		ma_message_router_t *router = ma_msgbus_get_message_router(self->bus);
		if(self->connection) {					
			ma_message_router_remove_consumer(router , (ma_message_consumer_t *)self->connection);
			ma_message_consumer_dec_ref((ma_message_consumer_t*)(self->connection));
		}
		ma_message_router_remove_consumer(router, (ma_message_consumer_t*)self);	
		ma_local_broker_consumer_unregister(ma_msgbus_get_local_broker(self->bus) , self->endpoint_name);				
		
		free(self->endpoint_name);								
		free(self);		
	}		
	return MA_ERROR_INVALIDARG;
}


// Decrements ep connect request message reference count. release request-message object when the ref count is zero.
ma_error_t ma_ep_connect_request_dec_ref(ma_ep_connect_request_t *self) {
    return ma_message_consumer_dec_ref(&self->consumer);
}

/*Increment the reference count */
ma_error_t ma_ep_connect_request_inc_ref(ma_ep_connect_request_t *self) {
    return ma_message_consumer_inc_ref(&self->consumer);
}
