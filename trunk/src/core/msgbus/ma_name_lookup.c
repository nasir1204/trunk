
#include "ma/internal/core/msgbus/ma_name_lookup.h"
#include "ma/internal/core/msgbus/ma_local_broker.h"
#include "ma/internal/core/msgbus/ma_message_consumer.h"
#include "ma/internal/core/msgbus/ma_named_pipe_connection_manager.h"
#include "ma/internal/core/msgbus/ma_ep_connect.h"
#include "ma/internal/core/msgbus/ma_message_router.h"
#include "ma/internal/core/msgbus/ma_message_header.h"
#include "ma/internal/core/msgbus/ma_name_service_macros.h"


#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdio.h> /* sprintf */

const static char ephemeral_name_template[] = "ma.lookup.internal.request.%08X";

extern ma_logger_t     *msgbus_logger;

struct ma_name_lookup_s {
    /*! yes, I am a message consumer */
    /* Please keep as first field in this struct */
    ma_message_consumer_t   consumer;

	//char					*endpoint_name ;

	ma_name_lookup_info_t	lookup_info;

	ma_name_lookup_start_cb	cb ;
	 
	void					*cb_data ;
    
	ma_message_consumer_t 	*ep_broker_consumer;  

	ma_message_consumer_t	*ep_consumer ;

	ma_error_t				status ;
	
	ma_uint32_t				correlation_id;

	/*! this objects ephemeral name */
    char                    ephemeral_name_buffer[8+sizeof(ephemeral_name_template)]; /* */

	ma_msgbus_t				*mbus;
};

void set_lookup_info(ma_name_lookup_info_t *self, long pid , const char *pipe_name , const char *host_name) {
    if(self) {
        self->pid = pid ;
        self->pipe_name = pipe_name ? strdup(pipe_name) : NULL;
        self->host_name = host_name ? strdup(host_name) : NULL;
    }
}


static ma_error_t consumer_on_message(ma_message_consumer_t *consumer, ma_message_t *msg) {    
	ma_name_lookup_t *self = (ma_name_lookup_t *) consumer ;	

	//Filter the messages only which we send 
	if(msg) {
		if( ((MA_MESSAGE_TYPE_REPLY == GET_MESSAGE_TYPE(msg)) 
			&& ((self->correlation_id == GET_MESSAGE_CORRELATION_ID(msg)))
			&& (0 == strcmp(GET_MESSAGE_DESTINATION_NAME(msg) , self->ephemeral_name_buffer)))) {
		
			ma_error_t rc = MA_OK ;		
			const char *endpoint_name = NULL ;
			
            MA_LOG(msgbus_logger, MA_LOG_SEV_DEBUG, "Recevied response to name look-up consumer");

			rc = (ma_error_t)GET_MESSAGE_STATUS(msg) ;
			ma_message_get_property(msg , ENDPOINT_NAME , &endpoint_name);

			if(MA_OK == rc) {
				const char *pipe_name = NULL , *host_name = NULL  , *pid = NULL;
				ma_message_get_property(msg , SERVER_PIPE_NAME , &pipe_name);
				ma_message_get_property(msg , SERVER_HOST_NAME , &host_name);
				ma_message_get_property(msg , SERVER_PID , &pid);
				set_lookup_info(&self->lookup_info, atol(pid) , pipe_name , host_name);
			}
			ma_message_router_remove_consumer(ma_msgbus_get_message_router(self->mbus) , (ma_message_consumer_t *) self);

			self->cb(self , rc , NULL , self->cb_data);
					
		}
	}
    return MA_OK; 
}

static void consumer_destroy(ma_message_consumer_t *base) {
    ma_name_lookup_release((ma_name_lookup_t*) base);
}

static const ma_message_consumer_methods_t consumer_methods = {
    &consumer_on_message,
    &consumer_destroy
};


ma_error_t ma_name_lookup_create(ma_msgbus_t *bus , ma_name_lookup_t **lookup) {
    if(bus && lookup) {
        ma_name_lookup_t *self = (ma_name_lookup_t *)calloc(1, sizeof(ma_name_lookup_t));
        if (self) {
            self->mbus = bus;
            ma_message_consumer_init(&self->consumer, &consumer_methods, 1, self);		
            *lookup = self;
            MA_LOG(msgbus_logger, MA_LOG_SEV_DEBUG, "name look-up created");
            return MA_OK;
        }
        MA_LOG(msgbus_logger, MA_LOG_SEV_CRITICAL, "Out of Memory");
        return MA_ERROR_OUTOFMEMORY;
    }
    MA_LOG(msgbus_logger, MA_LOG_SEV_ERROR, "Invalid Argument");
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_name_lookup_release(ma_name_lookup_t *self) {
	if(self) {
        if(self->lookup_info.endpoint_name) free(self->lookup_info.endpoint_name);
		if(self->lookup_info.pipe_name) free(self->lookup_info.pipe_name);
		if(self->lookup_info.host_name) free(self->lookup_info.host_name);
        ma_message_consumer_dec_ref(self->ep_consumer);
		ma_message_consumer_dec_ref(self->ep_broker_consumer);
		free(self);		
        MA_LOG(msgbus_logger, MA_LOG_SEV_DEBUG, "name look-up released");
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}


static ma_error_t start_lookup(ma_name_lookup_t *self) ;

static void call_usercallback_async_way(ma_name_lookup_t *self);

ma_error_t ma_name_lookup_start(ma_name_lookup_t *self ,ma_msgbus_consumer_reachability_t lookup_reach  , const char *endpoint_name, ma_name_lookup_start_cb cb, void *cb_data) {
	if(self && endpoint_name && cb) {
		ma_error_t rc = MA_OK ;		
		ma_message_consumer_t *consumer = NULL;

		self->lookup_info.endpoint_name = strdup(endpoint_name);
		self->cb = cb ;
		self->cb_data = cb_data ;
		
        MA_LOG(msgbus_logger, MA_LOG_SEV_DEBUG, "name look-up started");

		if(MA_OK == (rc = ma_local_broker_consumer_lookup(ma_msgbus_get_local_broker(self->mbus) , endpoint_name , &consumer) )) {		
			self->status = rc ;
			self->ep_consumer = consumer ;/* transfer ownership <- */
            MA_LOG(msgbus_logger, MA_LOG_SEV_DEBUG, "Service <%s> found in local broker", endpoint_name);
		}			

		switch(lookup_reach) {
		case MSGBUS_CONSUMER_REACH_INPROC :				
			if(MA_OK == rc ) 
				call_usercallback_async_way(self);				
			else {
                MA_LOG(msgbus_logger, MA_LOG_SEV_DEBUG, "Service <%s> not found in local broker and reachability is <%d>", endpoint_name, lookup_reach);
				rc = MA_ERROR_MSGBUS_SERVICE_NOT_FOUND ;
            }
			break;
		case MSGBUS_CONSUMER_REACH_OUTPROC :
		case MSGBUS_CONSUMER_REACH_OUTBOX :
			if(MA_OK == rc ) 
				call_usercallback_async_way(self);				
			else
				rc = start_lookup(self);
			break ;
		default:
            MA_LOG(msgbus_logger, MA_LOG_SEV_ERROR, "Reachability is not set");
			rc = MA_ERROR_MSGBUS_INTERNAL ;
			break;
		}	
		return rc ;
	}
    MA_LOG(msgbus_logger, MA_LOG_SEV_ERROR, "Invalid Argument");
	return MA_ERROR_INVALIDARG;
}


ma_error_t ma_name_lookup_stop(ma_name_lookup_t *self ) {
	return MA_OK ;
}

ma_name_lookup_info_t *ma_name_lookup_get_info(ma_name_lookup_t *self ) {
	return self ?  &self->lookup_info : NULL ;	
}

static ma_error_t create_lookup_request_message(ma_name_lookup_t *self  , char *broker_name , long pid ,  ma_message_t **lookup_request) {
	ma_message_t *msg = NULL ;
	ma_error_t rc = MA_OK ;
	if(MA_OK == (rc = ma_message_create(MA_MESSAGE_TYPE_REQUEST , &msg) )) {		
		SET_MESSAGE_CORRELATION_ID(msg , ++self->correlation_id); 
		sprintf(self->ephemeral_name_buffer,ephemeral_name_template,self->correlation_id);
		SET_MESSAGE_SOURCE_NAME(msg , self->ephemeral_name_buffer);		
		SET_MESSAGE_TYPE(msg , MA_MESSAGE_TYPE_REQUEST);
		SET_MESSAGE_DESTINATION_NAME(msg ,broker_name);
		SET_MESSAGE_DESTINATION_PID(msg , pid);
		ma_message_set_property(msg , REQUEST_TYPE , GET_SERVER_PIPE_NAME);
		ma_message_set_property(msg , ENDPOINT_NAME , self->lookup_info.endpoint_name);
		*lookup_request = msg;		
        MA_LOG(msgbus_logger, MA_LOG_SEV_DEBUG, "look-up request message created source name <%s> destination pid <%d>", self->ephemeral_name_buffer, pid);
	}
	return rc;
}

//TODO - Need to clean up if the connection fails for some reason
static void ep_connect_connect_cb(ma_ep_connect_t *connect, ma_error_t status , void *cb_data) {
	ma_name_lookup_t *self = (ma_name_lookup_t *)cb_data ;
    MA_LOG(msgbus_logger, MA_LOG_SEV_DEBUG, "ep connect callback is Invoked in name look-up with status <%d>", status);
	if(MA_OK != status) {
		self->cb(self , status , NULL , self->cb_data);	
	}	
}

static ma_error_t create_connection_to_broker(ma_name_lookup_t *self , char *broker_pipe_name) {
	ma_error_t rc = MA_OK ;	

	if(MA_OK == (rc = ma_ep_connect_create(self->mbus ,broker_pipe_name , NULL ,  (ma_ep_connect_t **)&self->ep_broker_consumer))){
		if(MA_OK == (rc = ma_ep_connect_start((ma_ep_connect_t*)self->ep_broker_consumer , ep_connect_connect_cb , self))) {
            MA_LOG(msgbus_logger, MA_LOG_SEV_DEBUG, "ep connect start to broker Succeeded in name look-up");
			return MA_OK ;
        }
		else {
            MA_LOG(msgbus_logger, MA_LOG_SEV_ERROR, "ep connect start to broker Failed in name look-up return value <%d>", rc);
			ma_message_consumer_dec_ref(self->ep_broker_consumer);
        }
	}
    else
        MA_LOG(msgbus_logger, MA_LOG_SEV_ERROR, "ep connect create for broker Failed in name look-up return value <%d>", rc);

	return rc;
}

static ma_error_t start_lookup(ma_name_lookup_t *self ) {	
	ma_error_t rc = MA_OK ;
	ma_message_t *lookup_request = NULL ;		
	char *broker_name = NULL , *broker_pipe_name = NULL ;
	long pid = -1 ;

	if( !(broker_name = ma_msgbus_get_broker_name(self->mbus)) || 
		!(broker_pipe_name = ma_msgbus_get_broker_pipe_name(self->mbus)) 
		|| (-1 == (pid =  ma_msgbus_get_broker_pid(self->mbus)))) {
        MA_LOG(msgbus_logger, MA_LOG_SEV_ERROR, "Failed to get the broker information");
		return MA_ERROR_MSGBUS_GET_BROKER_INFO_FAILED ;
    }
	
	
	/*If user is looking up for the broker itself , return it to the user right away as we already have the information */
	if(!strcmp(self->lookup_info.endpoint_name , broker_name)) {
		set_lookup_info(&self->lookup_info, pid, broker_pipe_name, NULL);
		self->status = MA_OK ;
		self->ep_consumer = NULL ;
        MA_LOG(msgbus_logger, MA_LOG_SEV_DEBUG, "Started lookup for broker itself, return it to the user");
		call_usercallback_async_way(self);
		return MA_OK ;
	}

	if(MA_OK  != (rc = create_lookup_request_message(self , broker_name , pid ,  &lookup_request))) {	
        MA_LOG(msgbus_logger, MA_LOG_SEV_ERROR, "Failed to create the look-up request message with error <%d>", rc);
		return rc ;
	}

	if(MA_OK != (rc = ma_named_pipe_connection_manager_search(ma_msgbus_get_named_pipe_connection_manager(self->mbus) ,broker_pipe_name, &self->ep_broker_consumer))) {
        MA_LOG(msgbus_logger, MA_LOG_SEV_DEBUG, "Creating conneciton to the Broker");
		rc = create_connection_to_broker(self , broker_pipe_name);
    }
   
	if(MA_OK != rc ) {	
		ma_message_dec_ref(lookup_request);			
		return rc; 
	}

	ma_message_router_add_consumer(ma_msgbus_get_message_router(self->mbus) , (ma_message_consumer_t *) self, NULL, MA_MSGBUS_CLIENT) ;
	ma_message_consumer_send((ma_message_consumer_t *)ma_msgbus_get_message_router(self->mbus) , lookup_request);		

	return rc ;
	
}

static void timer_close_cb(uv_handle_t *handle ) {
	if(handle)	free(handle);
}

static void timer_cb(uv_timer_t* handle, int status) {
	ma_name_lookup_t *self = (ma_name_lookup_t *)handle->data;	

	uv_close((uv_handle_t *)handle, timer_close_cb); 
	
    MA_LOG(msgbus_logger, MA_LOG_SEV_DEBUG, "Calling user callback in timer callback");

	if(self->cb)
		self->cb(self , self->status , self->ep_consumer , self->cb_data);    
}

static void call_usercallback_async_way(ma_name_lookup_t *self) {
	
	uv_loop_t *uv_loop = NULL ; 	
	uv_timer_t *uv_timer = NULL ;
	uv_timer = (uv_timer_t *)calloc(1 , sizeof(uv_timer_t));
	if(!uv_timer && self->cb ) {
		self->cb(self ,MA_ERROR_OUTOFMEMORY ,NULL , self->cb_data);
		return ;
	}
	ma_event_loop_get_loop_impl(ma_msgbus_get_event_loop(self->mbus), &uv_loop);

    MA_LOG(msgbus_logger, MA_LOG_SEV_DEBUG, "Calling user callback in async way with status <%d>", self->status);

	uv_timer_init(uv_loop, uv_timer);
	uv_timer->data = self;
	uv_timer_start(uv_timer, timer_cb, 0, 0);
	
}
