#include "ma/ma_msgbus.h"

#include "ma/internal/core/msgbus/ma_message_private.h"
#include "ma/internal/core/msgbus/ma_msgbus_request.h"
#include "ma/internal/utils/event_loop/ma_loop_worker.h"
#include "ma/internal/core/msgbus/ma_msgbus_private.h"
#include "ma/internal/utils/event_loop/ma_uv_helpers.h"
#include "ma/internal/core/msgbus/ma_message_header.h"


#include "uv.h"

#include <stdlib.h>
#include <assert.h>
#include <stdarg.h>

#include <string.h>

/* TBD -  10ms for conneciton timeout - will add interface to set this value by caller */
#define DEFAULT_TIMEOUT_INPROC		5*1000	// default timeout for inproc in ms
#define DEFAULT_TIMEOUT_OUTPROC		10*1000	+ 10*1000 // default timeout for outproc in ms
#define DEFAULT_TIMEOUT_OUTBOX		90*1000	+ 10*1000 // default timeout for outbox in ms
#define DEFAULT_PRIORITY			0	// TBD -- Will set the default value
#define DEFAULT_THREAD_MODEL		0	// TBD -- will set the default value

struct ma_msgbus_endpoint_s {
    /*! message bus instance to access all message bus objects */

    /* * * User Settings * * */

    /*! Service name to be connected */
    char								*service_name;

    
    /*! Time out */
    long								timeout_ms;  
    
	ma_bool_t							is_timeout_set_by_caller;

    /*! Priority */
    int									priority;

    /*! Thread model */
    int									thread_model;

    /*! Service reachability */
    ma_msgbus_consumer_reachability_t	service_reachability;

    ma_msgbus_t                         *mbus;

    /*! reference count */
    ma_atomic_counter_t                 ref_count;
};
    

static long get_default_timeout(ma_msgbus_consumer_reachability_t reachability) {
    return (MSGBUS_CONSUMER_REACH_INPROC == reachability) ? DEFAULT_TIMEOUT_INPROC : ((MSGBUS_CONSUMER_REACH_OUTPROC == reachability) ? DEFAULT_TIMEOUT_OUTPROC:DEFAULT_TIMEOUT_OUTBOX);
}


// Creates a handle to the specified endpoint service
/* return values from this call 
    MA_OK
    MA_ERROR_INVALIDARG
    MA_ERROR_OUTOFMEMORY
*/
ma_error_t ma_msgbus_endpoint_create(ma_msgbus_t *mbus, const char *server_name, const void *reserved, ma_msgbus_endpoint_t **endpoint)
{	
    ma_msgbus_endpoint_t *self = NULL;
    
    if(!mbus || !server_name || !strlen(server_name) || !endpoint)
        return MA_ERROR_INVALIDARG;    
       
    self = (ma_msgbus_endpoint_t*)calloc(1, sizeof(ma_msgbus_endpoint_t));
    
    if(self) {
        self->mbus = mbus;
        self->service_name	= strdup(server_name); 	
        self->service_reachability = MSGBUS_CONSUMER_REACH_INPROC;
        self->timeout_ms	= get_default_timeout(self->service_reachability);
        self->priority		= DEFAULT_PRIORITY; 
        self->thread_model	= DEFAULT_THREAD_MODEL;         
        MA_ATOMIC_INCREMENT(self->ref_count);        
        *endpoint = self;

        MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "endpoint creation succeeded service name <%s> reachability <%d> timeout <%d> priority <%d> thread model <%d>", self->service_name, self->service_reachability, self->timeout_ms, self->priority, self->thread_model);

        return MA_OK;		   	
    }    
    return MA_ERROR_OUTOFMEMORY;  		
}

static ma_error_t ma_msgbus_endpoint_add_ref(ma_msgbus_endpoint_t *self) {
    if(self) {
        MA_ATOMIC_INCREMENT(self->ref_count);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
   
}

static ma_error_t ma_msgbus_endpoint_destroy(ma_msgbus_endpoint_t *self) {
    if(self) {
        MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "endpoint destroys");
        if(self->service_name)
            free(self->service_name);
        free(self);        
        return MA_OK;
    }    
    return MA_ERROR_INVALIDARG;
}

// Releases the endpoint handle 
/* return values from this call 
    MA_OK
    MA_ERROR_INVALIDARG
*/
ma_error_t ma_msgbus_endpoint_release(ma_msgbus_endpoint_t *self) {
    if(self) {
        MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "endpoint Releases");
        if(0 == MA_ATOMIC_DECREMENT(self->ref_count))
            (void)ma_msgbus_endpoint_destroy(self);        
		return MA_OK;
    }    
    return MA_ERROR_INVALIDARG;
}

MA_MSGBUS_API ma_error_t ma_msgbus_endpoint_vsetopt(ma_msgbus_endpoint_t *self ,  ma_msgbus_option_t option, va_list ap) {
    ma_error_t rc = MA_OK;
    long timeout_ms = 0;
    ma_msgbus_consumer_reachability_t reachability = MSGBUS_CONSUMER_REACH_INPROC;

    if(!self)
        return MA_ERROR_INVALIDARG;    

    switch(option)
    {
    case MSGBUSOPT_TIMEOUT:					
        timeout_ms = va_arg(ap, long);
        if(0 < timeout_ms) {
			self->is_timeout_set_by_caller = MA_TRUE;
            self->timeout_ms = timeout_ms;
		}
        else
            rc = MA_ERROR_INVALIDARG;		
        break;

    case MSGBUSOPT_REACH:
        reachability = (ma_msgbus_consumer_reachability_t) va_arg(ap, long);
        self->service_reachability = reachability;
		if((MSGBUS_CONSUMER_REACH_OUTPROC == reachability) && !self->is_timeout_set_by_caller)
			self->timeout_ms = DEFAULT_TIMEOUT_OUTPROC;
        break;

    case MSGBUSOPT_PRIORITY:
        self->priority = va_arg(ap, long);
        break;

    case MSGBUSOPT_THREADMODEL:
        self->thread_model = va_arg(ap, long);
        break;

    default:
        MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_ERROR, "Invalid option <%d>", option);
        rc = MA_ERROR_INVALID_OPTION;
        break;
    }
    MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "<%d> option is set and return value is <%d>", option, rc);
    return rc;
}
// Sets endpoint options
/* return values from this call 
    MA_OK
    MA_ERROR_INVALIDARG
    MA_ERROR_INVALID_OPTION
*/
ma_error_t  ma_msgbus_endpoint_setopt(ma_msgbus_endpoint_t *self ,  ma_msgbus_option_t option, ...) {
    va_list arg;   
    ma_error_t rc = MA_OK;

    va_start(arg, option);
    rc = ma_msgbus_endpoint_vsetopt(self, option, arg);
    va_end(arg);
    return rc;
}



/* Client work function - It is executed from i/o thread */
static void inner_submit_request(void *work_data) {
    ma_msgbus_request_t *request = (ma_msgbus_request_t *) work_data;

    /* now transfer processing to the request class  */
    if( MA_OK != (request->status = ma_msgbus_request_start(request, request->endpoint->service_name, request->endpoint->service_reachability))) {
        /* unable to start request, so sending error code to the user */
        MA_LOG(ma_msgbus_get_logger(request->mbus), MA_LOG_SEV_ERROR, "Failed to start request, so sending the error code to the callback in main thread");
        request->callback(request->status, NULL, request->callback_data, request);
         /* undo the ref count which is taken before submit request */
        (void) ma_msgbus_request_release(request);
    }
    else
        MA_LOG(ma_msgbus_get_logger(request->mbus), MA_LOG_SEV_DEBUG, "Succeeded to start request");
}

static ma_error_t sumbit_request(ma_msgbus_endpoint_t *self, ma_message_t *msg, ma_msgbus_request_callback_t cb, void *cb_data, ma_msgbus_request_t **retval) {
    ma_error_t rc;
    ma_logger_t *logger = ma_msgbus_get_logger(self->mbus);
    ma_msgbus_request_t *request;
    if (MA_OK == (rc = ma_msgbus_request_create(self->mbus, &request))) {
        request->callback = cb;
        request->callback_data = cb_data;
        ma_msgbus_endpoint_add_ref(request->endpoint = self);
        request->mbus = self->mbus;
        request->timeout_ms = self->timeout_ms;
        request->thread_model = self->thread_model;

        ma_message_add_ref(request->request_message = msg);
        
        /* Note we keep an extra ref count until request has been processed */
        /* we must take this reference _before_ submitting it or else it may be dec_ref'ed and deleted before ma_loop_worker_submit_work returns! */
        (void) ma_msgbus_request_add_ref(request);

        if (MA_OK == (rc = ma_loop_worker_submit_work(ma_msgbus_get_loop_worker(self->mbus), &inner_submit_request, request, MA_FALSE))) {
            /* Don't reference self after this as it may already have been destroyed by some callback */
            *retval = request;
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "Succeeded to submit the request to loop worker");
            return MA_OK;
        }
        /* oh, well, couldn't even send it over, undo the ref count taken above */
        (void) ma_msgbus_request_release(request);

        /* also release the ref count from the creation */
        (void) ma_msgbus_request_release(request);
    }
    else
        MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to Create request");

    return rc;
}


// Initiates an asynchronous request
/* return values from this call 
    MA_OK
    MA_ERROR_INVALIDARG
    return codes from the message_request calls
*/
ma_error_t ma_msgbus_async_send(ma_msgbus_endpoint_t *self, ma_message_t *payload, ma_msgbus_request_callback_t cb, void *cb_data, ma_msgbus_request_t **request)
{  
    if(!self || !payload || !request || !cb) 
        return MA_ERROR_INVALIDARG;    

    MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "Async send call");

    return sumbit_request(self, payload, cb, cb_data, request);		
}


/* It is used in sync_send call to wait for status and response*/
typedef struct sync_request_data_s{

    ma_msgbus_request_t *request; 

    uv_mutex_t		mutex;

    uv_cond_t		condition;

    ma_bool_t		is_msg_received;

	ma_message_t	*response;
} sync_request_data_t;


/* Call back to use in sync_send to receive the response in async way */
static ma_error_t sync_request_callback(ma_error_t status, ma_message_t *response, void *cb_data, ma_msgbus_request_t *request){
    sync_request_data_t *sync_request_data = (sync_request_data_t*) cb_data;

    MA_LOG(ma_msgbus_get_logger(request->mbus), MA_LOG_SEV_DEBUG, "Sync send callback is Invoked");

	/* grab necessary information out of request before letting it go */ 
	sync_request_data->response = response;
	
    uv_mutex_lock(&sync_request_data->mutex);
    sync_request_data->is_msg_received = MA_TRUE;	
    uv_cond_signal(&sync_request_data->condition);
    uv_mutex_unlock(&sync_request_data->mutex);    

    return MA_OK;
}

// Initiates a synchronous request - blocks the thread until a reply is received or timeout or error!
/* return values from this call 
    MA_OK
    MA_ERROR_INVALIDARG	
    return codes from the message_request calls
    status code received from response
*/
MA_MSGBUS_API ma_error_t ma_msgbus_send(ma_msgbus_endpoint_t *self, ma_message_t *payload, ma_message_t **response) 
{	
    ma_error_t rc = MA_OK;
    sync_request_data_t sync_request_data;		
    
    if(!self || !payload || !response)
        return MA_ERROR_INVALIDARG;
    

    MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "Sync send call");

    sync_request_data.is_msg_received = MA_FALSE;
    
    if(0 == uv_mutex_init(&sync_request_data.mutex)) {
        if(0 == uv_cond_init(&sync_request_data.condition)) {
            (void) ma_msgbus_endpoint_setopt(self, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
            if(MA_OK == (rc = sumbit_request(self, payload, sync_request_callback, &sync_request_data, &sync_request_data.request))) {
        
                MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "Submitted loop worker request");

                uv_mutex_lock(&sync_request_data.mutex);

                while( !sync_request_data.is_msg_received ){
                    MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "Waiting for loop worker submit request acknowledgement ");
                    uv_cond_wait(&sync_request_data.condition, &sync_request_data.mutex);		
                }
    
                uv_mutex_unlock(&sync_request_data.mutex);

                ///* grab necessary information out of request before letting it go */ 
                //ma_message_add_ref(*response = sync_request_data.request->response_message);

				*response = sync_request_data.response;

                rc = sync_request_data.request->status;

                // releases the request object
                ma_msgbus_request_release(sync_request_data.request);                
            }

            uv_cond_destroy(&sync_request_data.condition);
            uv_mutex_destroy(&sync_request_data.mutex);
        }
        else {
            rc = MA_ERROR_MSGBUS_INTERNAL;
            MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_ERROR, "cond init Failed - uv error <%d> <%s>", MA_ERROR_FROM_UV_LAST_ERROR(ma_event_loop_get_uv_loop(ma_msgbus_get_event_loop(self->mbus))), UV_ERROR_STR_FROM_LAST_UV_ERROR(ma_event_loop_get_uv_loop(ma_msgbus_get_event_loop(self->mbus))));
            uv_mutex_destroy(&sync_request_data.mutex);            
        }
    }
    else {
        rc = MA_ERROR_MSGBUS_INTERNAL;
        MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_ERROR, "mutex init Failed - uv error <%d> <%s>", MA_ERROR_FROM_UV_LAST_ERROR(ma_event_loop_get_uv_loop(ma_msgbus_get_event_loop(self->mbus))), UV_ERROR_STR_FROM_LAST_UV_ERROR(ma_event_loop_get_uv_loop(ma_msgbus_get_event_loop(self->mbus))));
    }

    return rc;	
}

/* Dummy callback. It will never be called */
static ma_error_t oneway_request_callback(ma_error_t status, ma_message_t *response, void *cb_data, ma_msgbus_request_t *request){
    /* We can use this callback to pass any errors to the caller in send_and_forgot execution */
    MA_LOG(ma_msgbus_get_logger(request->mbus), MA_LOG_SEV_DEBUG, "One Way request callback is Invoked");
    return MA_OK;
}

// Initiates fire and forgot request - does not wait for response
/* return values from this call 
    MA_OK
    MA_ERROR_INVALIDARG
*/
MA_MSGBUS_API ma_error_t ma_msgbus_send_and_forget(ma_msgbus_endpoint_t *self, ma_message_t *payload)
{	
    ma_error_t rc = MA_OK;
    ma_msgbus_request_t *request = NULL;

    if(!self || !payload) 
        return MA_ERROR_INVALIDARG;	
        
    MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "Send And forget call");
    SET_MESSAGE_SUB_TYPE(payload, MA_MESSAGE_SUB_TYPE_ONEWAY);

    if(MA_OK == (rc = sumbit_request(self, payload, oneway_request_callback, self, &request))) {
        // TBD -- Should we block here for network or service not found error messages?
        ma_msgbus_request_release(request);
        MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "Submitted loop worker request");
    }

    return rc;
}

