#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdarg.h>

#include "ma/ma_msgbus.h"
#include "ma/internal/core/msgbus/ma_message_consumer.h"
#include "ma/internal/core/msgbus/ma_msgbus_private.h"
#include "ma/internal/utils/event_loop/ma_loop_worker.h"
#include "ma/internal/core/msgbus/ma_message_router.h"
#include "ma/internal/core/msgbus/ma_local_broker.h"
#include "ma/internal/core/msgbus/ma_msgbus_request.h"
#include "ma/internal/core/msgbus/ma_named_pipe_connection_manager.h"
#include "ma/internal/core/msgbus/ma_message_header.h"
#include "ma/internal/utils/regex/ma_regex.h"
#include "ma/internal/utils/event_loop/ma_uv_helpers.h"
#include "ma/internal/utils/threading/ma_thread.h"

/*NOTE: For now we are doing the filtering at subscriber side only */
typedef enum ma_filter_point_e  {
    MA_SUBSCRIBER_FILTER_POINT_SUBSCRIBER = 0 , /* Does filtering at subscriber side */
    MA_SUBSCRIBER_FILTER_POINT_BROKER  = 1 , /* Does filtering at broker , subscription informations is forwarded to broker */
    MA_SUBSCRIBER_FILTER_POINT_PUBLISHER = 2,  /* Does filtering at publisher side , subscription information is forwarded all the way to the publisher */
}ma_filter_point_t ;



typedef struct user_defined_s {    
    /*! Subscriber reachability */
    ma_msgbus_consumer_reachability_t   subscriber_reachability ;    

    /*! Thread model */
    ma_msgbus_callback_thread_options_t	thread_model;
} user_defined_t;

typedef struct topic_data_s { 
    /*! topic which published */
    char *topic ;

    /*! work object for callback invocation from thread pool */
    uv_work_t      uv_work;

    /*! Response message - pointer*/
    ma_message_t    *response_message;

    ma_msgbus_subscriber_t  *subscriber;

} topic_data_t;

struct ma_msgbus_subscriber_s {
    /*! It is a message consumer -- Please keep as first field in this struct */
    ma_message_consumer_t	consumer;

    /*! Topic filter to which it subscribed */
    char    *topic_filter ;

    /*! values set by the user */
    user_defined_t	set;
    
    ma_regex_t		*regex_engine;

    /*! bus pointer */
    ma_msgbus_t	*mbus ;

    /*! Subscriber callback */
    ma_msgbus_subscription_callback_t	cb ;

    /*! Subscriber callback user data */
    void	*cb_data;

	/*! mutex for msg processing in thread pool scenarios */
	ma_mutex_t	mutex;
};


static void lock(ma_msgbus_subscriber_t *self);
static void unlock(ma_msgbus_subscriber_t *self);

static void work_cb(uv_work_t *work) {	
    topic_data_t *topic_data = (topic_data_t *) work->data;   
 
	if(topic_data && topic_data->subscriber) {
	    MA_LOG(ma_msgbus_get_logger(topic_data->subscriber->mbus), MA_LOG_SEV_DEBUG, "Worker callback is Invoked in Subscriber");
		lock(topic_data->subscriber);
		if(topic_data->subscriber->cb) topic_data->subscriber->cb(topic_data->topic, topic_data->response_message, topic_data->subscriber->cb_data);
		unlock(topic_data->subscriber);
	}
}

static void after_work_cb(uv_work_t *work UV_AFTER_WORK_PARAMS) {
    topic_data_t *topic_data = (topic_data_t *) work->data;
	  	
	if(topic_data) {
        MA_LOG((topic_data->subscriber) ? ma_msgbus_get_logger(topic_data->subscriber->mbus) : NULL, MA_LOG_SEV_DEBUG, "After Worker callback is Invoked in Subscriber");
    	/* pool work no longer pending, release the corresponding ref count */
    	ma_message_release(topic_data->response_message);
		ma_message_consumer_dec_ref((ma_message_consumer_t *)topic_data->subscriber);
		free(topic_data);
	}
}

void ma_msgbus_sub_notify_caller(ma_msgbus_subscriber_t *self, const char *topic, ma_message_t *msg) {
    if(self && topic) {
        /* Determine how to call the user's callback */
        switch (self->set.thread_model & MA_MSGBUS_CALLBACK_THREAD_MASK) {
        case MA_MSGBUS_CALLBACK_THREAD_POOL: {
            uv_loop_t *uv_loop = ma_event_loop_get_uv_loop(ma_msgbus_get_event_loop(self->mbus));

            topic_data_t  *topic_data = (topic_data_t *)calloc(1, sizeof(topic_data_t));
            
            if(topic_data) {
                 /* Hold on to the message till the time work is done */
                ma_message_add_ref(topic_data->response_message = msg) ;
                topic_data->topic = (char *)topic;
                topic_data->subscriber = self;
            }
            else {
                MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_CRITICAL, "Out of Memory");
                return;
            }

			/* increment reference count while pool work is pending */
        	ma_message_consumer_inc_ref((ma_message_consumer_t *)self);
		
            MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "Invoking subscriber callback in different thread");

            topic_data->uv_work.data = topic_data;
            if (0 == uv_queue_work(uv_loop, &topic_data->uv_work, &work_cb, &after_work_cb)) {
                return;
            } 
            /* Something went wrong */
            /* this is fairly catastrophic, how can we let the caller know it's gone wrong ? */
            MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_ERROR, "Failed to start the subscriber callback in separate thread - uv error <%s>", UV_ERROR_STR_FROM_LAST_UV_ERROR(uv_loop));
            ma_message_consumer_dec_ref((ma_message_consumer_t *)self);
			free(topic_data);
            break;
                                             }

        case MA_MSGBUS_CALLBACK_THREAD_IO: {
            MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_TRACE, "Invoking subscriber callback in I/O thread");
            ma_message_add_ref(msg) ;
			lock(self);
			if(self->cb) self->cb(topic, msg, self->cb_data);
			unlock(self);
            ma_message_release(msg);
            break;
                                           }
        case MA_MSGBUS_CALLBACK_THREAD_APC: 
            /* not yet implemented, fall through... */

        default: {
            MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_ERROR, "Unsupported thread option");
            assert(!"unsupported thread option, sorry I didn't tell you that when you tried set it...");                
                 }
        }
    }
}

static ma_error_t consumer_on_message(ma_message_consumer_t *consumer, ma_message_t *msg) {
    ma_msgbus_subscriber_t *self = (ma_msgbus_subscriber_t *) consumer ;	
    if(msg && (MA_MESSAGE_TYPE_PUBLISH == GET_MESSAGE_TYPE(msg))) {
        const char *value = NULL ;

       /* If subscriber have shown interest only for local published messages  then return from here*/
        if((MSGBUS_CONSUMER_REACH_INPROC  == self->set.subscriber_reachability)  && ( 0 != GET_MESSAGE_SOURCE_PID(msg)))
            return MA_OK ;

        if((MA_OK == ma_message_get_property(msg , MA_MSG_PUBLISH_TOPIC , &value) && 
            (MA_TRUE == ma_regex_match(self->regex_engine , value )))) {
            MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "Received topic <%s>", value);
            ma_msgbus_sub_notify_caller(self, value, msg);               
        }		
    }
    return MA_OK ;
}
/* destructor, may be called on any thread, we don't know*/
static ma_error_t ma_msgbus_subscriber_destroy(ma_msgbus_subscriber_t *self);

static void consumer_destroy(ma_message_consumer_t *consumer) {	
    ma_msgbus_subscriber_destroy((ma_msgbus_subscriber_t*)consumer->data);
}

static const ma_message_consumer_methods_t consumer_methods = {
    &consumer_on_message,
    &consumer_destroy
};


ma_error_t ma_msgbus_subscriber_create(ma_msgbus_t *mbus, ma_msgbus_subscriber_t **subscriber) {
    ma_msgbus_subscriber_t *self = NULL ;
    if(mbus && subscriber) {        
        self = (ma_msgbus_subscriber_t *)calloc(1 , sizeof(ma_msgbus_subscriber_t)) ;
        if(self) {
            self->mbus = mbus;
            ma_message_consumer_init(&self->consumer, &consumer_methods, 1, self);
			ma_mutex_init(&self->mutex);
            *subscriber = self ;
            MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "Subscriber created");
            return MA_OK ;
        }		
        MA_LOG(ma_msgbus_get_logger(mbus), MA_LOG_SEV_CRITICAL, "Out of Memory");
        return MA_ERROR_OUTOFMEMORY ;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_msgbus_subscriber_release(ma_msgbus_subscriber_t *self) {
    if(self) {
		MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "Subscriber released");
		ma_message_consumer_dec_ref(&self->consumer);
        return MA_OK ;		
    }
    return MA_ERROR_INVALIDARG ;
}

static void inner_subscriber_register(void *data) {
    ma_msgbus_subscriber_t *self = ( ma_msgbus_subscriber_t *)data;

    MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "Subscribing topic filter <%s>", self->topic_filter);
	
	/* Local broker will take care about registration to pubsub service if the reachability is either outproc or outbox */
    /* local broker will add this consumer into router */
	ma_local_broker_consumer_register(ma_msgbus_get_local_broker(self->mbus), &self->consumer, self->topic_filter, self->set.subscriber_reachability, MA_FALSE);
	
    /* request no longer in route so release it from that perspective */
    ma_message_consumer_dec_ref(&self->consumer);
}

ma_error_t ma_msgbus_subscriber_register(ma_msgbus_subscriber_t *self,  const char *topic_filter, ma_msgbus_subscription_callback_t cb, void *cb_data) {
    if(self && topic_filter && cb) {		
        /* TODO - check the topic flter sanity */
        if(MA_OK != ma_regex_create(topic_filter , &self->regex_engine))
            return MA_ERROR_REGEX_CREATE_FAILED;

        self->topic_filter = strdup(topic_filter);
        self->cb = cb ;
        self->cb_data = cb_data;

        /* increment ref count while en-route so it doesn't get deleted  */
        ma_message_consumer_inc_ref(&self->consumer);

        MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "Submitting loop worker request to subscribe topic filter <%s>", self->topic_filter);

        return ma_loop_worker_submit_work(ma_msgbus_get_loop_worker(self->mbus), &inner_subscriber_register, self, MA_FALSE);		
    }
    return MA_ERROR_INVALIDARG ;
}


static void inner_subscriber_unregister(void *data) {
    ma_msgbus_subscriber_t *self = ( ma_msgbus_subscriber_t *)data;

    MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "Unsubscribing topic filter <%s>", self->topic_filter);

	/* Local broker will take care of unregistration from pubsub service based on reachability*/
    /* local broker will remove this consumer from router */
	ma_local_broker_consumer_unregister(ma_msgbus_get_local_broker(self->mbus), &self->consumer, self->topic_filter, self->set.subscriber_reachability, MA_FALSE);

     /* request no longer in route so release it from that perspective */
    ma_message_consumer_dec_ref(&self->consumer);
}

ma_error_t ma_msgbus_subscriber_unregister(ma_msgbus_subscriber_t *self) {
    if (self) {	
        ma_message_consumer_inc_ref(&self->consumer);
        MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "Submitting loop worker request to unsubscribe topic filter <%s>", self->topic_filter);
		lock(self);
		self->cb = NULL; /* Ignore all pending messages */
		unlock(self);
        return ma_loop_worker_submit_work(ma_msgbus_get_loop_worker(self->mbus), &inner_subscriber_unregister, self, MA_FALSE);		
    }
    return MA_ERROR_INVALIDARG;

}

MA_MSGBUS_API ma_error_t ma_msgbus_subscriber_vsetopt(ma_msgbus_subscriber_t *self,  ma_msgbus_option_t option, va_list ap) {
    ma_error_t rc = MA_OK;
    ma_msgbus_consumer_reachability_t reachability = MSGBUS_CONSUMER_REACH_INPROC;

    if(!self)
        return MA_ERROR_INVALIDARG;
    
    switch(option) {
    case MSGBUSOPT_REACH: {
        reachability = (ma_msgbus_consumer_reachability_t) va_arg(ap, long);		
        self->set.subscriber_reachability = reachability;
        break;
                          }
    case MSGBUSOPT_THREADMODEL: {
        self->set.thread_model = (ma_msgbus_callback_thread_options_t)va_arg(ap, long);
        break;
                                }
    default: {
        MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_ERROR, "Invalid option <%d>", option);
        rc = MA_ERROR_INVALID_OPTION;
        break;
             }
    }
    MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "Set subscriber option <%d> - return value <%d>", option, rc);
    return rc;
}

ma_error_t ma_msgbus_subscriber_setopt(ma_msgbus_subscriber_t *self,  ma_msgbus_option_t option, ...) {
    va_list arg;
    ma_error_t rc = MA_OK;

    va_start(arg, option);
    rc = ma_msgbus_subscriber_vsetopt(self, option, arg);
    va_end(arg);
    return rc;	
}

static ma_error_t ma_msgbus_subscriber_destroy(ma_msgbus_subscriber_t *self) {
    if(self) {	
        /* TODO - need to think about what if the user just goes aways with unsubscribing then the connection and list to the consumer is still there */
        MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "Subscriber Destroys");
		ma_mutex_destroy(&self->mutex);
        if(self->regex_engine) (void)ma_regex_release(self->regex_engine);
        if(self->topic_filter) free(self->topic_filter);		
        free(self); self = NULL;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

static void lock(ma_msgbus_subscriber_t *self) {
	if(MA_MSGBUS_CALLBACK_THREAD_POOL == (self->set.thread_model & MA_MSGBUS_CALLBACK_THREAD_MASK) || !ma_msgbus_has_single_thread(self->mbus)) {
		MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "msgbus subscriber - lock <%p>", self);
		ma_mutex_lock(&self->mutex);
	}
}

static void unlock(ma_msgbus_subscriber_t *self) {
	if(MA_MSGBUS_CALLBACK_THREAD_POOL == (self->set.thread_model & MA_MSGBUS_CALLBACK_THREAD_MASK) || !ma_msgbus_has_single_thread(self->mbus)) {
		ma_mutex_unlock(&self->mutex);
		MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "msgbus subscriber - unlock <%p>", self);
	}
}