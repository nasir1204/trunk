#undef MA_NO_DISPATCHER
#include "ma/internal/utils/macrypto/ma_crypto.h"
#define MA_NO_DISPATCHER	1

#include "ma/internal/core/msgbus/ma_msgbus_agent_crypto_provider.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include "ma/internal/utils/conf/ma_conf_application.h"
#include "ma/internal/utils/conf/ma_conf.h"
#include "ma/ma_dispatcher.h"
#include "ma/internal/ma_macros.h"


#include <stdlib.h>
#include <stdio.h>

#ifndef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "msgbus"
#endif

#if defined(MA_WINDOWS)
#include <io.h>
#endif


struct ma_msgbus_agent_crypto_provider_s {
    
	ma_msgbus_crypto_provider_t				base;

	ma_crypto_t								*crypto;
	
	ma_bool_t								is_crypto_owner;

	/*extra copy */
	ma_logger_t								*logger;

    ma_dispatcher_t                         *dispatcher;
};

static void crypto_provider_destroy(ma_msgbus_crypto_provider_t *crypto_provider);
static ma_error_t crypto_provider_get(ma_msgbus_crypto_provider_t *crypto_provider, ma_crypto_t **crypto);
static ma_error_t get_hash256(ma_msgbus_crypto_provider_t *crypto_provider, const unsigned char *buffer, size_t buffer_size, unsigned char digest[32]);
static ma_error_t get_hash256_file(ma_msgbus_crypto_provider_t *crypto_provider, int fd, unsigned char digest[32]);
static ma_error_t cms_verify(ma_msgbus_crypto_provider_t *crypto_provider, int cms_fd, ma_temp_buffer_t *manifest, ma_temp_buffer_t *issuer, ma_temp_buffer_t *subject, int *is_verified);
static ma_error_t hex_decode(ma_msgbus_crypto_provider_t *crypto_provider, const unsigned char *hex,const unsigned int size,unsigned char *raw,unsigned int max_size);

static ma_msgbus_crypto_provider_functions_t crypto_provider_methods = {
    &crypto_provider_destroy,
	&get_hash256,
	&get_hash256_file,
	&cms_verify,
	&hex_decode,
	&crypto_provider_get
};

ma_error_t ma_msgbus_agent_crypto_provider_create(ma_msgbus_agent_crypto_provider_t **provider) {
	if(provider) {
		ma_msgbus_agent_crypto_provider_t *self = (ma_msgbus_agent_crypto_provider_t *)calloc(1, sizeof(ma_msgbus_agent_crypto_provider_t));
		if(!self) return MA_ERROR_OUTOFMEMORY;
		ma_msgbus_crypto_provider_init((ma_msgbus_crypto_provider_t *)self, &crypto_provider_methods, self);
		*provider = self;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_msgbus_agent_crypto_provider_set_logger(ma_msgbus_agent_crypto_provider_t *self, ma_logger_t *logger) {
	if(self) {
		self->logger = logger;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_msgbus_agent_crypto_provider_set_crypto(ma_msgbus_agent_crypto_provider_t *self, ma_crypto_t *crypto) {
	if(self) {
		self->crypto = crypto;
		self->is_crypto_owner = MA_FALSE;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

static void crypto_provider_destroy(ma_msgbus_crypto_provider_t *crypto_provider) {
	ma_msgbus_agent_crypto_provider_t *self = (ma_msgbus_agent_crypto_provider_t *)crypto_provider;
	if(self) {
		if(self->crypto && self->is_crypto_owner ) {
			(void)ma_crypto_deinitialize(self->crypto);
			(void)ma_crypto_release(self->crypto);
            
			self->crypto = NULL;
		}
		if(self->dispatcher) (void)ma_dispatcher_deinitialize(self->dispatcher);
		free(self);
	}
}

static ma_error_t crypto_provider_get(ma_msgbus_crypto_provider_t *crypto_provider, ma_crypto_t **crypto) {
	ma_msgbus_agent_crypto_provider_t *self = (ma_msgbus_agent_crypto_provider_t *)crypto_provider;
	if(self && crypto) {
		if(!self->dispatcher) ma_dispatcher_initialize(NULL, &self->dispatcher);
		if(self->crypto) {
            *crypto = self->crypto;
            return MA_OK;
        }else {
            ma_error_t rc = MA_OK;            
            ma_temp_buffer_t crypto_lib_path = MA_TEMP_BUFFER_INIT;
			if(MA_OK == (rc = ma_conf_get_crypto_lib_path(&crypto_lib_path))) {
                ma_temp_buffer_t certstore_path = MA_TEMP_BUFFER_INIT;
                if(MA_OK == (rc = ma_conf_get_certstore_path(&certstore_path))) {
                    
                    if(MA_OK == (rc = ma_crypto_create(&self->crypto))) {                        
                        ma_crypto_ctx_t *ctx = NULL;
                        self->is_crypto_owner = MA_TRUE;
                        if(MA_OK == (rc = ma_crypto_ctx_create(&ctx))) {
                            (void)ma_crypto_ctx_set_agent_mode(ctx, MA_CRYPTO_AGENT_UNMANAGED);
                            (void)ma_crypto_ctx_set_fips_mode(ctx, MA_CRYPTO_MODE_FIPS);
                            (void)ma_crypto_ctx_set_logger(ctx, self->logger);
                            if(MA_OK == (rc = ma_crypto_ctx_set_certstore(ctx, (const char *)ma_temp_buffer_get(&certstore_path)))) {
                                if(MA_OK == (rc = ma_crypto_ctx_set_crypto_lib_path(ctx, (const char *)ma_temp_buffer_get(&crypto_lib_path)))) {                                    
                                    if(MA_OK == (rc = ma_crypto_initialize(ctx, self->crypto))) {
                                        ma_temp_buffer_uninit(&crypto_lib_path), ma_temp_buffer_uninit(&certstore_path);
                                        ma_crypto_ctx_release(ctx);
                                        *crypto = self->crypto;
                                        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "created crypto object successfully");
                                        return MA_OK;
                                    }
                                }
                            }
                            (void)ma_crypto_ctx_release(ctx);
                        }
                        (void)ma_crypto_release(self->crypto), self->crypto = NULL;
                    }
                
                }
                ma_temp_buffer_uninit(&certstore_path);
            }
            ma_temp_buffer_uninit(&crypto_lib_path);
			MA_LOG(self->logger, MA_LOG_SEV_ERROR, "failed to create crypto object, %d", rc);
            return rc;
        }
	}
	return MA_ERROR_INVALIDARG;
}



static ma_error_t get_hash256(ma_msgbus_crypto_provider_t *crypto_provider, const unsigned char *buffer, size_t buffer_size, unsigned char digest[32]) {
	ma_msgbus_agent_crypto_provider_t *self = (ma_msgbus_agent_crypto_provider_t *)crypto_provider;
	if(self && buffer && buffer_size > 0 && digest) {
		ma_error_t rc = MA_OK;
		ma_crypto_t *crypto = NULL;
		
		if(MA_OK == (rc = ma_msgbus_crypto_provider_get((ma_msgbus_crypto_provider_t *)self, &crypto))) {
			ma_crypto_hash_t *hash_object = NULL;
			if(MA_OK == (rc = ma_crypto_hash_create(crypto, MA_CRYPTO_HASH_SHA256, &hash_object))) {
				ma_bytebuffer_t *hash_buffer = NULL;
				if(MA_OK == (rc = ma_bytebuffer_create(32, &hash_buffer))) {
					if(MA_OK == (rc = ma_crypto_hash_buffer(hash_object, buffer, (int)buffer_size, hash_buffer))) {
						memcpy(digest, ma_bytebuffer_get_bytes(hash_buffer), 32);
					}
					(void)ma_bytebuffer_release(hash_buffer);
				}
				(void)ma_crypto_hash_release(hash_object);
			}
		}
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}



static ma_error_t get_hash256_file(ma_msgbus_crypto_provider_t *crypto_provider, int fd, unsigned char digest[32]) {
	ma_msgbus_agent_crypto_provider_t *self = (ma_msgbus_agent_crypto_provider_t *)crypto_provider;
	if(self && fd > 0 && digest) {
		ma_error_t rc = MA_OK;
		ma_crypto_t *crypto = NULL;
		if(MA_OK == (rc = ma_msgbus_crypto_provider_get(crypto_provider, &crypto))) {
			ma_crypto_hash_t *hash_object = NULL;
			if(MA_OK == (rc = ma_crypto_hash_create(crypto, MA_CRYPTO_HASH_SHA256, &hash_object))) {
				unsigned char buffer[2048]= {0};
				int bytes_read = 0;
				ma_bytebuffer_t *bytebuffer = NULL;
				while((bytes_read = read(fd,buffer, 2048)) > 0) {
					if(MA_OK != (rc = ma_crypto_hash_data_update(hash_object, buffer, bytes_read)))
						break;
				}
				if(MA_OK == rc) {
					if(MA_OK == (rc = ma_bytebuffer_create(32, &bytebuffer))) {
						if(MA_OK == (rc = ma_crypto_hash_data_final(hash_object, bytebuffer))) {
							memcpy(digest, ma_bytebuffer_get_bytes(bytebuffer), 32);
						}
						(void)ma_bytebuffer_release(bytebuffer);
					}
				}
				else
					MA_LOG(self->logger, MA_LOG_SEV_ERROR, "failed to  calculate hash  for %d, %d", fd, rc);
				(void)ma_crypto_hash_release(hash_object);
			}else
				MA_LOG(self->logger, MA_LOG_SEV_ERROR, "failed to create hash object for %d, %d", fd, rc);
		}else
			MA_LOG(self->logger, MA_LOG_SEV_ERROR, "failed to get underlying crypto object for hash calculation, %d", rc);
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

static ma_error_t cms_verify(ma_msgbus_crypto_provider_t *crypto_provider, int cms_fd, ma_temp_buffer_t *manifest, ma_temp_buffer_t *issuer, ma_temp_buffer_t *subject, int *is_verified) {
	ma_msgbus_agent_crypto_provider_t *self = (ma_msgbus_agent_crypto_provider_t *)crypto_provider;
	if(self && cms_fd > 0 && manifest && issuer && subject && is_verified) {
		ma_error_t rc = MA_OK;
		ma_crypto_t *crypto = NULL;
		if(MA_OK == (rc = ma_msgbus_crypto_provider_get(crypto_provider, &crypto))) {
			ma_crypto_cms_t *cms = NULL;
			if(MA_OK == (rc = ma_crypto_cms_create(crypto, &cms))) {
				if(MA_OK == (rc = ma_crypto_cms_verify(cms, cms_fd, is_verified))) {
					if(*is_verified) {
						const char *m = NULL, *i = NULL, *s = NULL;
							
						(void)ma_crypto_cms_get_auth_manifest(cms, &m),(void)ma_crypto_cms_get_issuer_name(cms, &i),(void)ma_crypto_cms_get_subject_name(cms, &s);
						ma_temp_buffer_reserve(manifest, strlen(m)),ma_temp_buffer_reserve(issuer, strlen(i)),ma_temp_buffer_reserve(subject, strlen(s));

						MA_MSC_SELECT(_snprintf, snprintf)((char*)ma_temp_buffer_get(manifest), ma_temp_buffer_capacity(manifest), "%s", m);
						MA_MSC_SELECT(_snprintf, snprintf)((char*)ma_temp_buffer_get(issuer), ma_temp_buffer_capacity(issuer), "%s", i);
						MA_MSC_SELECT(_snprintf, snprintf)((char*)ma_temp_buffer_get(subject), ma_temp_buffer_capacity(subject), "%s", s);
					}else 
						MA_LOG(self->logger, MA_LOG_SEV_ERROR, "failed to verify signature file %d, rc %d",cms_fd, rc);
				}else 
					MA_LOG(self->logger, MA_LOG_SEV_ERROR, "cms verify failed for sig fd %d, rc %d", cms_fd, rc);
				(void)ma_crypto_cms_release(cms);
			}else 
				MA_LOG(self->logger, MA_LOG_SEV_ERROR, "cms create failed for sig fd %d, rc %d", cms_fd, rc);
		}else
			MA_LOG(self->logger, MA_LOG_SEV_ERROR, "failed to get underlying crypto object, %d", rc);
		//return rc;
		return MA_OK; /* intentional Nasir */
	}
	return MA_ERROR_INVALIDARG;

}

static ma_error_t hex_decode(ma_msgbus_crypto_provider_t *crypto_provider, const unsigned char *hex,const unsigned int size,unsigned char *raw,unsigned int max_size) {
	ma_msgbus_agent_crypto_provider_t *self = (ma_msgbus_agent_crypto_provider_t *)crypto_provider;
	if(self) {
		return ma_crypto_hex_decode(hex, size, raw, max_size);
	}
	return MA_ERROR_INVALIDARG;
}

