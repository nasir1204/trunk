#include "ma/internal/core/msgbus/ma_msgbus_private.h"

#include "ma/internal/core/msgbus/ma_msgbus_passphrase_handler_user.h"
#include "ma/internal/core/msgbus/ma_msgbus_agent_crypto_provider.h"
#include "ma/internal/core/msgbus/ma_db_auth_info_provider.h"
#include "ma/internal/core/msgbus/ma_msgbus_defs.h"
#include "ma/internal/defs/ma_name_service_defs.h"
#include "ma/internal/defs/ma_pubsub_service_defs.h"
#include "ma/internal/defs/ma_common_services_defs.h"
#include "ma/internal/defs/ma_general_defs.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/ma_datastore.h"
#include "ma/datastore/ma_ds_registry.h"
#include "ma/datastore/ma_ds_ini.h"

#include "ma/logger/ma_logger.h"
#include "ma/internal/utils/conf/ma_conf.h"
#include "ma/internal/utils/conf/ma_conf_application.h"
#include "ma/internal/utils/text/ma_ini_parser.h"
#include "ma/internal/utils/watcher/ma_file_watcher.h"

#include "uv.h"

#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>

#define MA_MSGBUS_CONF_FILE_NAME_STR                "config.ini"

#ifndef MA_WINDOWS
#include <unistd.h>
#include <signal.h>
void (*user_sigpipe_handler)(int) = SIG_IGN;
#define MA_MSGBUS_CONF_DIR_NAME_STR                 ".msgbus"
#else
#define MA_MSGBUS_CONF_DIR_NAME_STR                 "msgbus"
#endif


struct ma_msgbus_s {

    char                                    *product_id;

    ma_event_loop_t                         *ma_loop;

    ma_loop_worker_t                        *loop_worker;

    ma_message_router_t                     *router;

    ma_named_pipe_connection_manager_t      *pipe_connections;

    ma_local_broker_t                       *local_broker;

    char	                                broker_pipe_name[MA_MAX_NAME];

    ma_int64_t                              broker_pid;

    ma_temp_buffer_t                        msgbus_conf_file;

    ma_logger_t                             *msgbus_logger;

    ma_msgbus_crypto_provider_t				*crypto_provider;

	ma_db_t									*msgbus_db;
    
	ma_watcher_t							*watcher_on_broker; /* File watcher */

    /*! thread id of the optional worker thread */
    uv_thread_t		                        worker_thread_id;

	ma_bool_t								has_single_thread;

    /* optional auth info provider */
    ma_message_auth_info_provider_h         auth_info_provider;

	ma_msgbus_passphrase_handler_t			*passphrase_handler;
};

typedef struct io_thread_data_s {
    uv_cond_t		cond;

    uv_mutex_t		mutex;

    ma_bool_t		is_initialized;

    ma_msgbus_t		*mbus;

    ma_error_t		last_error;
} io_thread_data_t;

static void set_initialized(io_thread_data_t *td, ma_bool_t initialized) {
    if (td) {
        uv_mutex_lock(&td->mutex);
        td->is_initialized = MA_TRUE;
        uv_cond_signal(&td->cond);
        uv_mutex_unlock(&td->mutex);
    }
}

#ifndef MA_WINDOWS
static void ma_msgbus_sigpipe_handler(int signum) {
    if(SIG_IGN != user_sigpipe_handler  && SIG_DFL != user_sigpipe_handler) {
	user_sigpipe_handler(signum);
    }
}
#endif



static void walk_cb(uv_handle_t *h, void *arg) {
    ma_msgbus_t *self = arg;
    const char *type;
	
    switch (h->type) {
#define X(uc, lc) case UV_##uc: type = #lc; break;
        UV_HANDLE_TYPE_MAP(X)
#undef X
default: type = "<unknown>";
    }

    MA_LOG(self->msgbus_logger, MA_LOG_SEV_DEBUG,  "%c %-8s %p .data=%p\n", "A-"[!uv_is_active(h)], type, (void*)h,  h->data);

#if 0
    fprintf(stderr,
        "%c %-8s %p .data=%p\n",
        "A-"[!uv_is_active(h)],
        type,
        (void*)h,
        h->data);
#endif 
}

static ma_error_t ma_msgbus_destroy(ma_msgbus_t *self) {
    if (self) {
		/* uv_loop_t *uv_loop = ma_event_loop_get_uv_loop(self->ma_loop); */

        MA_LOG(self->msgbus_logger, MA_LOG_SEV_DEBUG, "message bus destroying");

	if(self->crypto_provider) ma_msgbus_crypto_provider_release(self->crypto_provider);

        ma_message_auth_info_provider_release(self->auth_info_provider);

        if(self->passphrase_handler) ma_msgbus_passphrase_handler_release(self->passphrase_handler);

        free(self->product_id);

        ma_local_broker_release(self->local_broker);

        ma_named_pipe_connection_manager_release(self->pipe_connections);

        ma_message_router_release(self->router);

        ma_loop_worker_release(self->loop_worker);

		/* uv_walk(uv_loop, &walk_cb, self); */

        ma_event_loop_release(self->ma_loop);

        ma_logger_release(self->msgbus_logger);

        ma_temp_buffer_uninit(&self->msgbus_conf_file);

        free(self);
    }
    return MA_OK;
}


static void msgbus_io_thread_fun(void *data);

static void msgbus_stop_handler_cb(ma_event_loop_t *loop, int status, void *cb_data);
static ma_error_t setup_msgbus_conf_file(ma_msgbus_t *self);
static ma_error_t create_auth_provider(ma_msgbus_t *self);
static ma_error_t create_passphrase_provider(ma_msgbus_t *self);
static ma_error_t create_crypto_provider(ma_msgbus_t *self);

ma_error_t ma_msgbus_create(const char *product_id, ma_msgbus_t **msgbus) {    
    if(product_id && msgbus) {
        ma_msgbus_t *self = (ma_msgbus_t *) calloc(1, sizeof(ma_msgbus_t));
        if (self) {
            ma_error_t rc;
            self->product_id = strdup(product_id);
            ma_temp_buffer_init(&self->msgbus_conf_file);

            (void) setup_msgbus_conf_file(self);            
            /* create all contained objects and attempt to start */
            MA_OK == (rc = ma_event_loop_create(&self->ma_loop)) &&
            MA_OK == (rc = ma_loop_worker_create(self->ma_loop, &self->loop_worker)) &&
            MA_OK == (rc = ma_message_router_create(&self->router)) &&
            MA_OK == (rc = ma_named_pipe_connection_manager_create(&self->pipe_connections)) &&
            MA_OK == (rc = ma_local_broker_create(self->ma_loop, self->router, &self->local_broker)) &&
            MA_OK == (rc = create_passphrase_provider(self)) && 
            MA_OK == (rc = create_crypto_provider(self));


            /* Registers the default stop handler */
            ma_event_loop_register_stop_handler(self->ma_loop, &msgbus_stop_handler_cb, self);
            
            if (MA_OK != rc) {
                ma_msgbus_destroy(self);
                return rc;
            }
			self->broker_pid = -1;

            return *msgbus = self, MA_OK;               
        }
        return MA_ERROR_OUTOFMEMORY; 
    }
    return MA_ERROR_INVALIDARG;
}

/* 
 * starts msgbus in a worker thread
 */
ma_error_t ma_msgbus_start(ma_msgbus_t *self) {
    ma_error_t rc = MA_OK;
    io_thread_data_t io_data = {0};
    if (0 == uv_mutex_init(&io_data.mutex)) {
        if(0 == uv_cond_init(&io_data.cond)) {
            io_data.is_initialized = MA_FALSE;
            io_data.mbus = self;
            io_data.last_error = MA_OK;

            MA_LOG(self->msgbus_logger, MA_LOG_SEV_DEBUG, "starting msgbus i/o thread...");
            if(-1 == uv_thread_create(&self->worker_thread_id, msgbus_io_thread_fun, &io_data)) {
                rc = MA_ERROR_MSGBUS_CREATE_FAILED;
            } else {
                MA_LOG(self->msgbus_logger, MA_LOG_SEV_DEBUG, "Ok, started msgbus i/o thread, waiting for thread to complete initialization...");
                uv_mutex_lock(&io_data.mutex);
                while( !io_data.is_initialized ){
                    uv_cond_wait(&io_data.cond, &io_data.mutex);		
                }	
                uv_mutex_unlock(&io_data.mutex);
		if(MA_OK == (rc = io_data.last_error))
			MA_LOG(self->msgbus_logger, MA_LOG_SEV_DEBUG, "i/o thread initialization complete, bus now running");
		else
			MA_LOG(self->msgbus_logger, MA_LOG_SEV_ERROR, "i/o thread initialization Failed, rc = <%d>", rc);
            }
            uv_cond_destroy(&io_data.cond);
            uv_mutex_destroy(&io_data.mutex);
        } else {
            uv_mutex_destroy(&io_data.mutex);
            rc = MA_ERROR_MSGBUS_CREATE_FAILED;
        }
    }
    return rc;
}

const char *ma_msgbus_get_product_id(ma_msgbus_t *self) {
    return (self)?self->product_id:NULL;	
}

ma_event_loop_t *ma_msgbus_get_event_loop(ma_msgbus_t *self) {
    return (self)?self->ma_loop:NULL;	
}

ma_loop_worker_t *ma_msgbus_get_loop_worker(ma_msgbus_t *self) {
    return (self)?self->loop_worker:NULL;	
}


struct ma_message_auth_info_provider_s *ma_msgbus_get_auth_info_provider(ma_msgbus_t *self) {
    return (self) ? self->auth_info_provider : NULL;
}

struct ma_msgbus_crypto_provider_s *ma_msgbus_get_crypto_provider(ma_msgbus_t *self) {
	return (self) ? self->crypto_provider : NULL;
}

struct ma_msgbus_passphrase_handler_s  *ma_msgbus_get_passphrase_handler(ma_msgbus_t *self) {
	return (self) ? self->passphrase_handler : NULL;
}

ma_message_router_t *ma_msgbus_get_message_router(ma_msgbus_t *self) {
    return (self)?self->router:NULL;	
}

ma_local_broker_t *ma_msgbus_get_local_broker(ma_msgbus_t *self) {
    return (self)?self->local_broker:NULL;		
}

ma_named_pipe_connection_manager_t *ma_msgbus_get_named_pipe_connection_manager(ma_msgbus_t *self) {
    return (self)?self->pipe_connections:NULL;	
}

ma_error_t ma_msgbus_set_crypto(ma_msgbus_t *self, struct ma_crypto_s *crypto) {
	if(self && self->crypto_provider) {
		(void)ma_msgbus_agent_crypto_provider_set_crypto((ma_msgbus_agent_crypto_provider_t *)self->crypto_provider, crypto);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_msgbus_set_msgbus_db(ma_msgbus_t *self, ma_db_t *msgbus_db) {
	if(self && msgbus_db) {
		self->msgbus_db = msgbus_db;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_logger_t *ma_msgbus_get_logger(ma_msgbus_t *self) {
    return (self)?self->msgbus_logger:NULL;	
}

static ma_error_t setup_msgbus_conf_file(ma_msgbus_t *self) {
    ma_error_t rc = MA_OK;
    ma_temp_buffer_t temp_buf = MA_TEMP_BUFFER_INIT;   
    char sub_path[MA_MAX_PATH_LEN] = {0};
#ifdef MA_WINDOWS
    if(MA_OK != (rc = ma_conf_get_data_path(&temp_buf)))
        return rc;
#else
    strncpy((char*)ma_temp_buffer_get(&temp_buf), MA_MSGBUS_CONF_BASE_DIR_STR, sizeof(MA_MSGBUS_CONF_BASE_DIR_STR) -1);
#endif
    MA_MSC_SELECT(_snprintf, snprintf)(sub_path, MA_MAX_PATH_LEN-1, "%s%c%s", MA_MSGBUS_CONF_DIR_NAME_STR, MA_PATH_SEPARATOR, MA_MSGBUS_CONF_FILE_NAME_STR);
    rc = form_path(&self->msgbus_conf_file, (char*)ma_temp_buffer_get(&temp_buf), sub_path , MA_FALSE);
    ma_temp_buffer_uninit(&temp_buf);
    return rc;
}

const char *ma_msgbus_get_conf_file(ma_msgbus_t *self) {
    if(self) {
        const char *conf_file = (const char *)ma_temp_buffer_get(&self->msgbus_conf_file);
        return conf_file && strlen(conf_file) ? conf_file : NULL;
    }
    return NULL;
}

static ma_bool_t get_msgbus_config_data(uv_loop_t *uv_loop, char *file_name, ma_temp_buffer_t *config_data, size_t *size) {
    ma_bool_t b_rc = MA_FALSE;
    if(uv_loop && file_name && config_data && size) {
        uv_fs_t req = {0};
        uv_file file ;
        if(!uv_fs_stat(uv_loop, &req, file_name, NULL)) {
#ifdef MA_WINDOWS
            struct _stat64 s = req.statbuf;
#else
            struct stat s = req.statbuf;
#endif
            *size = s.st_size;
            uv_fs_req_cleanup(&req);
            ma_temp_buffer_reserve(config_data, *size);
            if(-1 != uv_fs_open(uv_loop, &req, file_name, O_RDONLY, 0644, NULL)) {
			    file = req.result ;
			    uv_fs_req_cleanup(&req);
                if(-1 != uv_fs_read(uv_loop, &req, file, ma_temp_buffer_get(config_data), *size, -1, NULL)) {
                    b_rc = MA_TRUE;
                    uv_fs_req_cleanup(&req) ;                
                }
                uv_fs_close(uv_loop, &req, file, NULL);
                uv_fs_req_cleanup(&req) ; 
            }
       }
    }
    return b_rc;
}

static int config_ini_handler(const char *section, const char *key, const char *value, void *cb_data) {
    ma_msgbus_t *self = (ma_msgbus_t *)cb_data;
    if(section && !strcmp(section, MA_MSGBUS_PATH_NAME_STR) && value) {
        if(key && !strcmp(key, MA_MSGBUS_KEY_BROKER_PID_INT)) {
            self->broker_pid = value ? atol(value) : -1;
        }
        if(key && !strcmp(key, MA_MSGBUS_KEY_BROKER_ID_STR) && value) {
            memset(&self->broker_pipe_name, 0, sizeof(self->broker_pipe_name)); 
            MA_MSC_SELECT(_snprintf, snprintf)(self->broker_pipe_name, sizeof(self->broker_pipe_name)-1, "%s", value);
        }
    }
    return 0;
}

static ma_error_t read_fresh_broker_information(ma_msgbus_t *self)  {
    ma_error_t rc = MA_ERROR_MSGBUS_INTERNAL;    
    uv_loop_t *uv_loop = ma_event_loop_get_uv_loop(self->ma_loop);    
    ma_temp_buffer_t config_data = MA_TEMP_BUFFER_INIT;
    size_t size = 0;
    if(get_msgbus_config_data(uv_loop, (char *)ma_msgbus_get_conf_file(self), &config_data, &size)) {
        if(MA_OK == (rc = ma_ini_parser_start((char *)ma_temp_buffer_get(&config_data), size, config_ini_handler, self))) {
            if(-1 == self->broker_pid) {
                rc = MA_ERROR_MSGBUS_INTERNAL;
                MA_LOG(self->msgbus_logger, MA_LOG_SEV_TRACE, "Broker pid is not available in config.ini");
            }
            if('\0' == self->broker_pipe_name[0]) {
                rc = MA_ERROR_MSGBUS_INTERNAL;
                MA_LOG(self->msgbus_logger, MA_LOG_SEV_TRACE, "Broker pipe name is not available in config.ini");
            }            
        }
        ma_temp_buffer_uninit(&config_data);
    }
    
    if(MA_OK != rc) {
        MA_LOG(self->msgbus_logger, MA_LOG_SEV_WARNING, "Broker information is not available, rc <%d>", rc);
		memset(&self->broker_pipe_name, 0, sizeof(self->broker_pipe_name)); 
		self->broker_pid = -1;
        rc = MA_ERROR_MSGBUS_GET_BROKER_INFO_FAILED ;
    }

    return rc ; 
}

ma_int64_t ma_msgbus_get_broker_pid(ma_msgbus_t *self)  {
    if(-1 == self->broker_pid) 
        (void) read_fresh_broker_information(self);

    return self->broker_pid;
}

const char *ma_msgbus_get_broker_pipe_name(ma_msgbus_t *self) {
	if(self->broker_pipe_name[0] == '\0') 
        (void) read_fresh_broker_information(self);

    return (self->broker_pipe_name[0] != '\0') ? self->broker_pipe_name : NULL;
}

static ma_error_t broker_watcher_callback(ma_watcher_t *watcher, const char *name, void *cb_data) {
	ma_error_t rc = MA_OK;
	ma_msgbus_t *self = (ma_msgbus_t *)cb_data;
	char cur_pipe_name[MA_MAX_NAME] = {0};

	MA_LOG(self->msgbus_logger, MA_LOG_SEV_TRACE, "File watcher callback invoked on broker config change, file name <%s>", name ? name : "NULL");
	if(name) {
		strncpy(cur_pipe_name, self->broker_pipe_name, MA_MAX_NAME-1);
		if(MA_OK == (rc = read_fresh_broker_information(self))) {		
			/* Ignore if pid matches with brokerpid or no change in pipe name */
			if(self->broker_pid != getpid() && strcmp(cur_pipe_name, self->broker_pipe_name)) {
				rc = ma_local_broker_check_registrations(self->local_broker);
			}
		}
	}
	else { /* NULL -> directory might be deleted while watching */
#ifdef MA_WINDOWS
	const char *conf_file = ma_msgbus_get_conf_file(self);
	MA_LOG(self->msgbus_logger, MA_LOG_SEV_TRACE, "broker_watcher_callback:Deleting file watcher");
	if(self->watcher_on_broker && conf_file && (MA_OK == ma_watcher_delete(self->watcher_on_broker, conf_file)))
		(void)ma_watcher_release(self->watcher_on_broker), self->watcher_on_broker = NULL;
#endif
	}
	return rc;
}

static void add_watcher_on_broker(ma_msgbus_t *self) {	
    const char *conf_file = ma_msgbus_get_conf_file(self);
    if(conf_file) {
		ma_error_t rc = MA_OK;
		if(MA_OK == (rc = ma_file_watcher_create(ma_msgbus_get_event_loop(self), &self->watcher_on_broker))) {
			if(MA_OK != (rc = ma_watcher_add(self->watcher_on_broker, conf_file , broker_watcher_callback, MA_FALSE, self))) {
				MA_LOG(self->msgbus_logger, MA_LOG_SEV_WARNING, "Unable to add file watcher on broker config file <%s>, rc <%d>", conf_file, rc);
				(void)ma_watcher_release(self->watcher_on_broker); self->watcher_on_broker = NULL;
			}
			else
				MA_LOG(self->msgbus_logger, MA_LOG_SEV_TRACE, "Added file watcher on broker config file <%s>", conf_file);
		}
    }
}

static void delete_watcher_on_broker(ma_msgbus_t * self) {
	const char *conf_file = ma_msgbus_get_conf_file(self);
	if(self->watcher_on_broker && conf_file && (MA_OK == ma_watcher_delete(self->watcher_on_broker, conf_file)))
		(void)ma_watcher_release(self->watcher_on_broker),  self->watcher_on_broker = NULL;
        
	MA_LOG(self->msgbus_logger, MA_LOG_SEV_TRACE, "Removed file watcher on broker config file <%s>", conf_file);
}

ma_error_t ma_msgbus_disable_watcher(ma_msgbus_t *self) {
	if(self) {
		MA_LOG(self->msgbus_logger, MA_LOG_SEV_TRACE, "Disabling file watcher on broker config file");
		(void) delete_watcher_on_broker(self);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}


static ma_error_t do_run(ma_msgbus_t *self, io_thread_data_t *io_data) {
    ma_error_t rc;
#ifndef MA_WINDOWS
    user_sigpipe_handler = signal(SIGPIPE, SIG_IGN);
    if(SIG_IGN != user_sigpipe_handler) 
	signal(SIGPIPE, ma_msgbus_sigpipe_handler);
#endif
    ma_msgbus_agent_crypto_provider_set_logger((ma_msgbus_agent_crypto_provider_t *)self->crypto_provider, self->msgbus_logger);
    if(MA_OK == (rc = ma_local_broker_start(self, self->local_broker))) {
        if(MA_OK == (rc = ma_loop_worker_start(self->loop_worker))) {
            /* (void) read_fresh_broker_information(self); */
            /* TBD - should we not even allow to start without it */
            create_auth_provider(self);
	    add_watcher_on_broker(self);
            /* Start event loop */
  	    ma_event_loop_start(self->ma_loop);
            set_initialized(io_data, MA_TRUE);
            /* MA_LOG(self->msgbus_logger, MA_LOG_SEV_DEBUG, "local broker and loop worker started... and Event loop is running..."); */
            return ma_event_loop_run(self->ma_loop); /* This will block! */
        }
	delete_watcher_on_broker(self);
        ma_local_broker_stop(self->local_broker);
    }
    if(io_data) io_data->last_error = rc;
    set_initialized(io_data, MA_TRUE); /* so that parent thread will continue regardless of do_run failure */
    return rc;
}

ma_error_t ma_msgbus_run(ma_msgbus_t *self) {
    if (self) {
		self->has_single_thread = MA_TRUE;
        return do_run(self, NULL);
    }
    return MA_ERROR_INVALIDARG;
}

/* i/o thread function */
static void msgbus_io_thread_fun(void *data) {
    io_thread_data_t *io_data = (io_thread_data_t*)data;
    (void) do_run(io_data->mbus, io_data);
}

/* 
 * This function is expected to release all registered watchers to allow the loop to then terminate
 * Function is invoked on the i/o thread
 */
static void ma_msgbus_stop_inner(void *data) {
    ma_msgbus_t *self = (ma_msgbus_t*)data;

    /* TODO - wait for all request completion or destroy the instance ????? */
    MA_LOG(self->msgbus_logger, MA_LOG_SEV_DEBUG, "Stopping msgbus...");
	

	delete_watcher_on_broker(self);

    /* Stop named pipe server */
    if(MA_OK == ma_local_broker_stop(self->local_broker)) {
        MA_LOG(self->msgbus_logger, MA_LOG_SEV_DEBUG, "Stopped local broker");
    } else {
        MA_LOG(self->msgbus_logger, MA_LOG_SEV_ERROR, "Failed to stop local broker");
    }

    /* Stop loop worker */
    if (MA_OK == ma_loop_worker_stop(self->loop_worker)) {
        MA_LOG(self->msgbus_logger, MA_LOG_SEV_DEBUG, "Stopped loop worker");
    } else {
        MA_LOG(self->msgbus_logger, MA_LOG_SEV_ERROR, "Failed to stop loop worker");
    }

    /* Close named pipe connection manager */
    if (MA_OK == ma_named_pipe_connection_manager_close_all(self->pipe_connections)) {
        MA_LOG(self->msgbus_logger, MA_LOG_SEV_DEBUG, "Closed named pipe connection manager");
    } else {
        MA_LOG(self->msgbus_logger, MA_LOG_SEV_ERROR, "Failed to close named pipe connection manager");
    }

    MA_LOG(self->msgbus_logger, MA_LOG_SEV_DEBUG, "OK, stopped msgbus, now waiting for loop to run dry");
}

MA_MSGBUS_API ma_error_t ma_msgbus_stop(ma_msgbus_t *self, ma_bool_t wait) {
    if(self) {
        ma_error_t rc;
        MA_LOG(self->msgbus_logger, MA_LOG_SEV_DEBUG, "Message bus stop...");
        /* Note, that if bus is shut down directly from the worker thread, the underlying ma_event_loop's async watcher is */
        if (ma_event_loop_is_loop_thread(self->ma_loop)) {
            MA_LOG(self->msgbus_logger, MA_LOG_SEV_DEBUG, "ma_msgbus_stop invoked from i/o thread");
            ma_msgbus_stop_inner(self);
            return MA_OK;
        } else if(MA_OK == (rc = ma_event_loop_stop(self->ma_loop))) {
            if (wait && self->worker_thread_id) {
                MA_LOG(self->msgbus_logger, MA_LOG_SEV_DEBUG, "Waiting for i/o thread termination...");
                rc = (0 == uv_thread_join(&self->worker_thread_id)) ? MA_OK : MA_ERROR_MSGBUS_RELEASE_FAILED;
                MA_LOG(self->msgbus_logger, MA_LOG_SEV_DEBUG, "Ok, i/o thread terminated, now continuing...");
            }
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

static void msgbus_stop_handler_cb(ma_event_loop_t *loop, int status, void *cb_data) {
     ma_msgbus_stop_inner(cb_data);
}

static ma_error_t create_auth_provider(ma_msgbus_t *self) {
#ifdef MA_WINDOWS
    ma_error_t rc;
    ma_temp_buffer_t db_folder = MA_TEMP_BUFFER_INIT;
    char db_path[MAX_PATH];
    ma_db_auth_info_provider_h provider = NULL;

    MA_SUCCEEDED(rc = ma_conf_get_db_path(&db_folder)) &&
    MA_SUCCEEDED(rc = 0 < _snprintf(db_path, MA_COUNTOF(db_path), "%s" MA_PATH_SEPARATOR_STR MA_DB_COMMON_SERVICES_FILENAME_STR, ma_temp_buffer_get(&db_folder)) ? MA_OK : MA_ERROR_INVALIDARG) &&
    MA_SUCCEEDED(rc = ma_db_auth_info_provider_create(db_path, &provider));

    MA_LOG(self->msgbus_logger, MA_LOG_SEV_DEBUG, "Configuring auth info from db = <%s>", db_path);

    ma_temp_buffer_uninit(&db_folder);

	if(self->msgbus_db)
		ma_db_auth_info_provider_set_msgbus_db(provider, self->msgbus_db);

    ma_db_auth_info_provider_set_logger(provider, self->msgbus_logger); /* TODO what if logger changes after this */

    if (MA_SUCCEEDED(rc)) {
        self->auth_info_provider = (void*) provider;
    } else {

    }

    return rc;

#else
    return MA_OK;
#endif
}

static ma_error_t create_passphrase_provider(ma_msgbus_t *self) {
#ifndef MA_WINDOWS
	ma_error_t rc = MA_OK;
	if(MA_OK == (rc = ma_msgbus_passphrase_handler_user_create(self, (ma_msgbus_passphrase_handler_user_t **)&self->passphrase_handler))) {
		ma_msgbus_passphrase_handler_user_set_logger((ma_msgbus_passphrase_handler_user_t *)self->passphrase_handler, self->msgbus_logger);
	}
	return rc;
#else
    return MA_OK;
#endif
}

static ma_error_t create_crypto_provider(ma_msgbus_t *self) {
	ma_error_t rc = MA_OK;
	if(MA_OK == (rc = ma_msgbus_agent_crypto_provider_create((ma_msgbus_agent_crypto_provider_t **)&self->crypto_provider))) {
	}
	return rc;
}


MA_MSGBUS_API ma_error_t ma_msgbus_release(ma_msgbus_t *self) {
    if(self) {
		MA_LOG(self->msgbus_logger, MA_LOG_SEV_DEBUG, "Message bus release...");        
        return ma_msgbus_destroy(self);
    }
    return MA_ERROR_INVALIDARG;
}

 ma_error_t ma_msgbus_set_logger(ma_msgbus_t *self, ma_logger_t *logger) {
    if(self && logger) {
        ma_error_t rc = MA_OK;
        ma_logger_inc_ref(self->msgbus_logger = logger);
        if(MA_OK == (rc = ma_loop_worker_set_logger(self->loop_worker, logger)) &&
            MA_OK == (rc = ma_message_router_set_logger(self->router, logger)) &&
            MA_OK == (rc = ma_named_pipe_connection_manager_set_logger(self->pipe_connections, logger)) &&
            MA_OK == (rc = ma_local_broker_set_logger(self->local_broker, logger)))
            return MA_OK;        
        ma_logger_dec_ref(self->msgbus_logger);
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_msgbus_passphrase_digest(ma_msgbus_t *self, const void *passphrase, size_t passphrase_size, unsigned char passphrase_digest[32]) {
	return self ? 
		ma_msgbus_crypto_provider_get_hash256(self->crypto_provider, passphrase, passphrase_size, passphrase_digest)
		   : MA_ERROR_INVALIDARG;
}

ma_error_t ma_msgbus_set_passphrase_provider_callback(ma_msgbus_t *self, ma_msgbus_passphrase_provider_callback_t cb, void *cb_data) {
	if(self && self->passphrase_handler) {
		(void)ma_msgbus_passphrase_handler_user_set_passphrase_callback((ma_msgbus_passphrase_handler_user_t *)self->passphrase_handler, cb, cb_data);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_bool_t ma_msgbus_has_single_thread(ma_msgbus_t *self) {
	if(self)
		return self->has_single_thread;
	return MA_TRUE; /* this case may not hit, but safe side returns true */
}


