#include "ma/internal/core/msgbus/ma_msgbus_auth_rule.h"
#include "ma/internal/core/msgbus/ma_msgbus_defs.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#ifndef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "msgbus"
#endif



struct ma_msgbus_auth_rule_s {

	ma_uint16_t         auth_conditions;

    ma_array_t			*users_list;

	ma_array_t			*groups_list;

	ma_array_t			*platforms_list;

	ma_logger_t			*logger;

};

static ma_error_t parse_auth_rule(ma_msgbus_auth_rule_t *self, const char *rule) ;
static ma_error_t update_auth_rule(ma_msgbus_auth_rule_t *self, ma_array_t *rule_list) ;

ma_error_t ma_msgbus_auth_rule_create(const char *rule, ma_msgbus_auth_rule_t **auth_rule) {
	if(rule && auth_rule) {
		ma_error_t rc = MA_OK;
		ma_msgbus_auth_rule_t *self = (ma_msgbus_auth_rule_t *)calloc(1, sizeof(ma_msgbus_auth_rule_t));
		
		if(!self) return MA_ERROR_OUTOFMEMORY;
		if(MA_OK == (rc = parse_auth_rule(self, rule))) {
			*auth_rule = self;
			return MA_OK;
		}
		(void)ma_msgbus_auth_rule_release(self);
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_msgbus_auth_rule_release(ma_msgbus_auth_rule_t *self) {
	if(self) {
		(void)ma_array_release(self->users_list);
		(void)ma_array_release(self->platforms_list);
		(void)ma_array_release(self->groups_list);
		free(self);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_msgbus_auth_rule_set_logger(ma_msgbus_auth_rule_t *self, ma_logger_t *logger) {
	return self ? self->logger = logger, MA_OK: MA_ERROR_INVALIDARG;
}

ma_error_t ma_msgbus_auth_rule_get_auth_conditions(ma_msgbus_auth_rule_t *self, ma_uint16_t *auth_conditions) {
	if(self && auth_conditions) {
		*auth_conditions = self->auth_conditions;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_msgbus_auth_rule_get_platforms(ma_msgbus_auth_rule_t *self, ma_array_t **platforms_list)  {
	if(self && platforms_list && self->platforms_list) {
		ma_array_add_ref(*platforms_list = self->platforms_list);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_msgbus_auth_rule_get_users(ma_msgbus_auth_rule_t *self, ma_array_t **users_list) {
	if(self && users_list && self->users_list) {
		ma_array_add_ref(*users_list = self->users_list);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_msgbus_auth_rule_get_groups(ma_msgbus_auth_rule_t *self, ma_array_t **groups_list) {
	if(self && groups_list && self->groups_list) {
		ma_array_add_ref(*groups_list = self->groups_list);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

static ma_error_t parse_auth_rule_expression(const char *expression, ma_array_t **exp_list) ;

/*TBD - needs to parse more for challenge and response atleast */
static ma_error_t parse_auth_rule(ma_msgbus_auth_rule_t *self, const char *rule) {
	ma_error_t rc = MA_OK ;
    ma_array_t *rule_list = NULL ;

    MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Parsing auth rule expression %s", rule) ;

    if(MA_OK == (rc = parse_auth_rule_expression(rule, &rule_list))) {
        rc = update_auth_rule(self, rule_list) ;
        (void)ma_array_release(rule_list) ; rule_list = NULL ;
    }

    if(MA_OK != rc) {
         MA_LOG(self->logger, MA_LOG_SEV_ERROR, "failed to parse auth rule expression %s, <rc = %d>.", rule, rc) ;
    }
	
    return rc ;
}

static ma_error_t parse_auth_rule_expression(const char *expression, ma_array_t **exp_list) {
    ma_error_t rc = MA_OK ;

    if(MA_OK == (rc = ma_array_create(exp_list))) {
        char *tmp_exp = strdup(expression) ;
        char *token = strtok(tmp_exp, " ") ;
        ma_variant_t *var = NULL ;

        while(token) {
            if(strcmp(token, MA_MSGBUS_AUTH_RULE_COND_SEPERATOR_STR)) {
                if(MA_OK == ma_variant_create_from_string(token, strlen(token), &var)) {
                    (void)ma_array_push(*exp_list, var) ;
                    (void)ma_variant_release(var) ; var = NULL ;
                }
            }
            token = strtok(NULL, " ") ;
        }
        free(tmp_exp) ;
    }
    return rc ;
}


static void set_auth_type(ma_msgbus_auth_rule_t *self, const char *value) {
    if(strstr(value, MA_MSGBUS_VALUE_AUTH_TYPE_EXECUTABLE_VALIDATION_STR))   self->auth_conditions |= MA_AUTH_COND_EXECUTABLE_VALIDATION ;     
    if(strstr(value, MA_MSGBUS_VALUE_AUTH_TYPE_CHALLENGE_RESPONSE_STR))   self->auth_conditions |= MA_AUTH_COND_CHALLENGE_AND_RESPONSE ;
    
    MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "auth rule set %s authtype", value) ;
    return ;
}

static void set_user(ma_msgbus_auth_rule_t *self, const char *value) {
    ma_error_t rc = MA_OK ;

	self->auth_conditions |= MA_AUTH_COND_USER_VALIDATION;

    if(!self->users_list) {
        rc = ma_array_create(&self->users_list) ;
    }

    if(MA_OK != rc) {
        MA_LOG(self->logger, MA_LOG_SEV_ERROR, "auth rule failed to add user(%s), <rc = %d>.", value, rc) ;
    }
    else {
        ma_variant_t *var = NULL ;
        char *temp = strdup(value) ;
        char *token = strtok(temp, MA_MSGBUS_AUTH_RULE_VALUE_SEPERATOR_STR) ;

        while(token) {
            if(MA_OK == (rc = ma_variant_create_from_string(token, strlen(token), &var))) {
                if(MA_OK == (rc = ma_array_push(self->users_list, var))) {
                    MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "auth rule set user(%s).", token) ;
                }
                (void)ma_variant_release(var) ; var = NULL ;
            }
            if(MA_OK != rc) MA_LOG(self->logger, MA_LOG_SEV_ERROR, "auth rule failed to add user(%s), <rc = %d>.", token, rc) ;
            token = strtok(NULL, MA_MSGBUS_AUTH_RULE_VALUE_SEPERATOR_STR) ;
        }
        free(temp) ;
    }
    return ;
}

static void set_group(ma_msgbus_auth_rule_t *self, const char *value) {
    ma_error_t rc = MA_OK ;

	self->auth_conditions |= MA_AUTH_COND_GROUP_VALIDATION;

    if(!self->groups_list) {
        rc = ma_array_create(&self->groups_list) ;
    }

    if(MA_OK != rc) {
        MA_LOG(self->logger, MA_LOG_SEV_ERROR, "auth rule failed to add group(%s), <rc = %d>.", value, rc) ;
    }
    else {
        ma_variant_t *var = NULL ;
        char *temp = strdup(value) ;
        char *token = strtok(temp, MA_MSGBUS_AUTH_RULE_VALUE_SEPERATOR_STR) ;

        while(token) {
            if(MA_OK == (rc = ma_variant_create_from_string(token, strlen(token), &var))) {
                if(MA_OK == (rc = ma_array_push(self->groups_list, var))) {
                    MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "auth rule set group(%s).", token) ;
                }
                (void)ma_variant_release(var) ; var = NULL ;
            }
            if(MA_OK != rc) MA_LOG(self->logger, MA_LOG_SEV_ERROR, "auth rule failed to add group(%s), <rc = %d>.", token, rc) ;
            token = strtok(NULL, MA_MSGBUS_AUTH_RULE_VALUE_SEPERATOR_STR) ;
        }
        free(temp) ;
    }
    return ;
}

static void set_platform(ma_msgbus_auth_rule_t *self, const char *value) {
    ma_error_t rc = MA_OK ;

	self->auth_conditions |= MA_AUTH_COND_PLATFORM_VALIDATION;

    if(!self->platforms_list) {
        rc = ma_array_create(&self->platforms_list) ;
    }

    if(MA_OK != rc) {
        MA_LOG(self->logger, MA_LOG_SEV_ERROR, "auth rule failed to add platform(%s), <rc = %d>.", value, rc) ;
    }
    else {
        ma_variant_t *var = NULL ;
        char *temp = strdup(value) ;
        char *token = strtok(temp, MA_MSGBUS_AUTH_RULE_VALUE_SEPERATOR_STR) ;

        while(token) {
            if(MA_OK == (rc = ma_variant_create_from_string(token, strlen(token), &var))) {
                if(MA_OK == (rc = ma_array_push(self->platforms_list, var))) {
                    MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "auth rule set platform(%s).", token) ;
                }
                (void)ma_variant_release(var) ; var = NULL ;
            }
            if(MA_OK != rc) MA_LOG(self->logger, MA_LOG_SEV_ERROR, "auth rule failed to add platform(%s), <rc = %d>.", token, rc) ;
            token = strtok(NULL, MA_MSGBUS_AUTH_RULE_VALUE_SEPERATOR_STR) ;
        }
        free(temp) ;
    }
    return ;
}

static void set_rule(ma_msgbus_auth_rule_t *self, const char *rule) {
    char *temp = strdup(rule) ;
    char *token = strtok(temp, MA_MSGBUS_AUTH_RULE_KEY_VALUE_SEPERATOR_STR) ;

    char *key = NULL, *value = NULL ;
    
    if(token) {
        key = strdup(token) ;
        token = strtok(NULL, MA_MSGBUS_AUTH_RULE_KEY_VALUE_SEPERATOR_STR) ;
        if(token){
			if((value = strdup(token))) {
        
				if(!strcmp(key , MA_MSGBUS_KEY_AUTH_TYPE_STR)) {
					set_auth_type(self, value) ;
				}
				if(!strcmp(key , MA_MSGBUS_KEY_AUTH_USER_STR)) {
					set_user(self, value) ;
				}
				if(!strcmp(key , MA_MSGBUS_KEY_AUTH_GROUP_STR)) {
					set_group(self, value) ;
				}
				if(!strcmp(key , MA_MSGBUS_KEY_AUTH_PLATFORM_ID_STR)) {
					set_platform(self, value) ;
				}
				free(value);
			}
		}
        free(key) ; 
    }
	free(temp);
}

static void auth_rule_array_cb(size_t index, ma_variant_t *value, void *cb_args, ma_bool_t *stop_loop) {
    ma_buffer_t *buf = NULL ;

    ma_msgbus_auth_rule_t *self = (ma_msgbus_auth_rule_t *)cb_args ;

    if(MA_OK == ma_variant_get_string_buffer(value , &buf)) {
    	const char *rule = NULL ;
    	size_t len = 0 ;
        (void)ma_buffer_get_string(buf, &rule, &len) ;
        set_rule(self, rule) ;
        (void)ma_buffer_release(buf) ; buf = NULL ;
    }

}
static ma_error_t update_auth_rule(ma_msgbus_auth_rule_t *self, ma_array_t *rule_list) {
    return ma_array_foreach(rule_list, auth_rule_array_cb, (void *)self) ; 
}
