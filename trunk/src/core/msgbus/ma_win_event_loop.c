#include "ma/internal/ma_win_event_loop.h"

#include <stdlib.h>
#include <assert.h>

#include <windows.h>

enum {
    max_objects = MAXIMUM_WAIT_OBJECTS,
    };

struct ma_win_event_loop_s {
    ma_win_event_watcher_t  signal_watcher;

    unsigned                flags;

    DWORD                   wait_mask; // for MsgWaitForMultipleObjects

    ma_win_event_watcher_t  *watchers[max_objects];
    int                     watcher_count; // number of active watchers

    int                     stop_pending;
    };

static void signal_cb(ma_win_event_watcher_t *w, int unused) {
    ResetEvent(w->h);
    }


ma_error_t ma_win_event_loop_create(unsigned flags, ma_win_event_loop_t **loop) {

    ma_win_event_loop_t *self = (ma_win_event_loop_t*) calloc(1,sizeof(ma_win_event_loop_t));
    if (self) {
        self->flags = flags;
        self->wait_mask = QS_ALLEVENTS;
        self->signal_watcher.h = CreateEvent(0,TRUE,FALSE,0); // anonymous, manual reset event for internal use
        self->signal_watcher.cb = &signal_cb;
        ma_win_event_loop_add_watcher(self, &self->signal_watcher);
        *loop = self;
        return MA_OK;
        }

    return MA_ERROR_OUTOFMEMORY;
    }

ma_error_t ma_win_event_loop_destroy(ma_win_event_loop_t *self) {
    ma_win_event_loop_remove_watcher(self, &self->signal_watcher);
    if (self->signal_watcher.h) {
        CloseHandle(self->signal_watcher.h);
        self->signal_watcher.h = 0;
        }

    assert(0 == self->watcher_count);

    free(self);
    return MA_OK;
    }


ma_error_t ma_win_event_loop_add_watcher(ma_win_event_loop_t *self, ma_win_event_watcher_t *w) {
    if (0 == self || 0 == w) {
        return MA_ERROR_INVALIDARG;
        }

    if (self->watcher_count < max_objects) {
        self->watchers[self->watcher_count++] = w;
        return MA_NOERROR;
        }

    return MA_ERROR_OUTOFMEMORY;
    }

ma_error_t ma_win_event_loop_remove_watcher(ma_win_event_loop_t *self, ma_win_event_watcher_t *w) {
    int i;
    if (0 == self || 0 == w) {
        return MA_ERROR_INVALIDARG;
        }

    for (i=0; i!=self->watcher_count; ++i) {
        if (self->watchers[i] == w) {
            // watcher found, remove it and move remaining up the list
            const int remaining = --self->watcher_count - i;
            if (remaining) {
                memmove(self->watchers+i, 1+self->watchers+i, remaining * sizeof(self->watchers[0]) );
                }
            self->watchers[self->watcher_count] = NULL;
            return MA_NOERROR;
            }
        }
    return MA_ERROR_INVALIDARG;
    }

ma_error_t ma_win_event_loop_get_watcher_count(const ma_win_event_loop_t *self, size_t *count) {
    if (self && count) {
        *count = self->watcher_count;
        return MA_OK;
        }
    return MA_ERROR_INVALIDARG;
    }

// returns when there are no more messages available
ma_error_t ma_win_event_loop_run_once(ma_win_event_loop_t *self, int wait, unsigned *break_reason) {

    if (0 == self) {
        return MA_ERROR_INVALIDARG;
        }

    if (0 == self->watcher_count) {
        return MA_ERROR_PRECONDITION;
        }

    // will try to dispatch all events, possibly waiting for any to become signaled first

    if (break_reason) {*break_reason = MA_WIN_EVENT_LOOP_BREAK_NO_EVENTS;}

    // TODO honor timers
    // TODO provide multiple back-ends, e.g. all 4 of [Msg]WaitForMultipleObjects[Ex]

    for (;;) {
        HANDLE waitHandles[max_objects];
        ma_win_event_watcher_t* watchers[max_objects];
        DWORD result;
        int i;
        const int handle_count = self->watcher_count;
        const unsigned check_messages = (MA_WIN_EVENT_LOOP_OPT_MESSAGES & self->flags);

        // TODO Can these be set outside the loop?
        for (i=0; handle_count!=i; ++i) {
            ma_win_event_watcher_t *w = self->watchers[i];
            waitHandles[i] = w->h;
            watchers[i] = w;
            }

        assert(handle_count <= MAXIMUM_WAIT_OBJECTS); 

        result = check_messages 
            ? MsgWaitForMultipleObjects(handle_count, waitHandles, FALSE, wait && !self->stop_pending ? INFINITE : 0, self->wait_mask)
            : WaitForMultipleObjects(handle_count, waitHandles, FALSE, wait && !self->stop_pending ? INFINITE : 0); 

        wait = 0; // only wait the first time through

        if (WAIT_OBJECT_0 <= result && result < WAIT_OBJECT_0 + handle_count) {
            // an event was signaled
            int index = result - WAIT_OBJECT_0;
            //should we collect the handler or just invoke it?
            //we will just invoke it for now

            ma_win_event_watcher_t *w = watchers[index];
            if (w) w->cb(w,0);
            if (break_reason) *break_reason |= MA_WIN_EVENT_LOOP_BREAK_PENDING_WIN_MESSAGES; 
            continue;
            }

        if (WAIT_ABANDONED_0 <= result && result < WAIT_ABANDONED_0 + handle_count) {
            // an event was abandoned
            int index = result - WAIT_ABANDONED_0;
            ma_win_event_watcher_t *w = watchers[index];
            if (w) w->cb(w,1);
            continue;
            }

        if (WAIT_OBJECT_0 + handle_count == result ) {
            if (break_reason) *break_reason |= MA_WIN_EVENT_LOOP_BREAK_PENDING_WIN_MESSAGES; 
            assert(check_messages);

            return MA_NOERROR;
            }

        if (WAIT_TIMEOUT == result) {
            return MA_NOERROR;
            }

        if (WAIT_FAILED == result) {
            return MA_ERROR_FROM_WIN32_LASTERROR(GetLastError());
            }

        // we shouldn't get here
        return MA_ERROR_UNEXPECTED;
        }
    }

ma_error_t ma_win_event_loop_stop(ma_win_event_loop_t *self) {
    if (!self) {
        return MA_ERROR_INVALIDARG;
        }
    self->stop_pending = 1;
    SetEvent(self->signal_watcher.h);
    return MA_NOERROR;
    }

ma_error_t ma_win_event_loop_run(ma_win_event_loop_t *self) {
    if (0 == self) {
        return MA_ERROR_INVALIDARG;
        }

    while (!self->stop_pending) {
        unsigned result;
        ma_error_t e = ma_win_event_loop_run_once(self,1,&result);
        if (result & MA_WIN_EVENT_LOOP_BREAK_PENDING_WIN_MESSAGES) {
            // TODO pump Windows messages or call some registered callback
            }
        }

    return MA_NOERROR;
    }


void ma_win_handle_watcher_init(ma_win_event_watcher_t *w, ma_win_handle_t h, ma_win_event_watcher_cb cb, void *data) {
    w->h = h;
    w->cb = cb;
    w->data = data;
    }
