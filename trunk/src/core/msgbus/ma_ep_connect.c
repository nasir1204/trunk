#include "ma/internal/core/msgbus/ma_ep_connect.h"
#include "ma/internal/core/msgbus/ma_named_pipe_connection_manager.h"
#include "ma/internal/core/msgbus/ma_named_pipe_connection.h"
#include "ma/internal/core/msgbus/ma_msgbus_impl.h"
#include "ma/internal/core/msgbus/ma_message_router.h"

#include <stdlib.h>

/* This adds the named pipe connection into named pipe connection manager  , and add the named pipe connection into router */
/* Even if you release this object , it is not closing the connection and not removing from named pipe connection manager and not removing it from consumer , who will do that */


//TODO - Poul- How to deal with this named pipe connection which are added and when we will remove them 
//TODO = Poul - When someone wants to stop recieving that means that consumer can just remove it from the consumer , but still the problem remain same who will close this connection

extern ma_logger_t     *msgbus_logger;

struct ma_ep_connect_s {	
	/*Yes I am consumer object */
	ma_message_consumer_t consumer ;
	/* pipe name of the connection */
	char	*pipe_name ; 
	/* host name of the connection */
	char	*host_name ;	
	/*connection object */
	ma_named_pipe_connection_t *connection;
	/* bus instance pointer */
	ma_msgbus_t	*mbus;

	/* call back and callback data*/
	ma_ep_connect_connect_cb	cb ;

	void	*cb_data;
	
};
static ma_error_t consumer_on_message(ma_message_consumer_t *consumer, ma_message_t *msg) {    	
    ma_ep_connect_t *self = (ma_ep_connect_t*)consumer;
    /* If the message comes to it just forward it to the namedpipe connection */
    if(msg && self->connection)
        ma_message_consumer_send((ma_message_consumer_t *)self->connection , msg);
    return MA_OK; 
}

static void consumer_destroy(ma_message_consumer_t *base) {    
    ma_ep_connect_release((ma_ep_connect_t*) base);
}

static const ma_message_consumer_methods_t consumer_methods = {
    &consumer_on_message,
    &consumer_destroy
};


/* Pipe name to connect to , not sure we need host name or not , lets keep it for now*/
ma_error_t ma_ep_connect_create(ma_msgbus_t *bus , const char *pipe_name , const char *host_name ,   ma_ep_connect_t **connect) {
	if(bus && pipe_name && connect) {
		ma_ep_connect_t *self = (ma_ep_connect_t*)calloc(1 , sizeof(ma_ep_connect_t));
		if(!self) {
            MA_LOG(msgbus_logger, MA_LOG_SEV_CRITICAL, "Out of Memory");
			return MA_ERROR_OUTOFMEMORY ;
        }

		self->mbus = bus ;			
		self->pipe_name = strdup(pipe_name);
		if(host_name) self->host_name = strdup(host_name);        
		ma_message_consumer_init(&self->consumer, &consumer_methods, 1, self);		
		*connect = self ;
        MA_LOG(msgbus_logger, MA_LOG_SEV_DEBUG, "ep connect created and Pipe Name is <%s>", pipe_name);
		return MA_OK ;	
	}
    MA_LOG(msgbus_logger, MA_LOG_SEV_ERROR, "Invalid Argument");
	return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_ep_connect_release(ma_ep_connect_t *self) {
	if(self) {
		if(self->host_name) free(self->host_name);
		if(self->pipe_name) free(self->pipe_name);
		if(self->connection)
			ma_message_consumer_dec_ref((ma_message_consumer_t *)self->connection ) ;
		//TODO - we are not removing it from consumer and named pipe connection manager
		free(self);
        MA_LOG(msgbus_logger, MA_LOG_SEV_DEBUG, "ep connect is released");
		return MA_OK ;
	}
    MA_LOG(msgbus_logger, MA_LOG_SEV_ERROR, "Invalid Argument");
	return MA_ERROR_INVALIDARG;
}

static void c_connection_cb(ma_named_pipe_connection_t *connection, int status, void *cb_data) ;

ma_error_t ma_ep_connect_start(ma_ep_connect_t *self ,  ma_ep_connect_connect_cb cb, void *cb_data) {
	if(self) {		
		ma_error_t rc = MA_OK ;
				
		self->cb = cb ;
		self->cb_data = cb_data;
		if(MA_OK == (rc = (ma_named_pipe_connection_create(ma_msgbus_get_event_loop(self->mbus) , &self->connection)))) {
			if(MA_OK == (rc = ma_named_pipe_connection_connect(self->connection , self->pipe_name , c_connection_cb , self))) {
				//Add it to named pipe connection manager 							
				ma_named_pipe_connection_manager_add(ma_msgbus_get_named_pipe_connection_manager(self->mbus) , self->pipe_name , self->connection);
				 /* connect the router and new connection to each other */
                ma_message_router_add_consumer(ma_msgbus_get_message_router(self->mbus) , (ma_message_consumer_t *)self->connection, NULL, MA_MSGBUS_NAMED_PIPE_CONNECTION);
				ma_named_pipe_connection_set_consumer(self->connection, (ma_message_consumer_t*)ma_msgbus_get_message_router(self->mbus));

				ma_message_consumer_inc_ref((ma_message_consumer_t *)self->connection);				
                MA_LOG(msgbus_logger, MA_LOG_SEV_DEBUG, "ep connect is started");
			}
			else{
                MA_LOG(msgbus_logger, MA_LOG_SEV_ERROR, "named pipe connection is not connected");
				ma_named_pipe_connection_release(self->connection) ;				
            }
		}
        else
            MA_LOG(msgbus_logger, MA_LOG_SEV_ERROR, "named pipw connection is createed");

		return rc ; 
	}
    MA_LOG(msgbus_logger, MA_LOG_SEV_ERROR, "Invalid Argument");
	return MA_ERROR_INVALIDARG;

}

ma_error_t ma_ep_connect_stop(ma_ep_connect_t *self) {
	if(self) {
		//TODO - /* What to do here * , I don't think we can remove the connection from here and close the connection 
		//TODO - Poul ?
		//(void)ma_named_pipe_connection_stop_receiving(self->connection);

		//(void)ma_named_pipe_connection_close(self->connection);
		//ma_named_pipe_connection_manager_remove(ma_msgbus_get_named_pipe_connection_manager(self->mbus) , self->connection);
		//TODO - should remove it from 
        MA_LOG(msgbus_logger, MA_LOG_SEV_DEBUG, "ep connect stopped");
		return MA_OK ;
	}
    MA_LOG(msgbus_logger, MA_LOG_SEV_ERROR, "Invalid Argument");
	return MA_ERROR_INVALIDARG;
}


static void c_connection_cb(ma_named_pipe_connection_t *connection, int status, void *cb_data) {
	ma_ep_connect_t *self = (ma_ep_connect_t *) cb_data ;	
	if(MA_OK == status) {				 
		/*Now that the connection is established  */ 
        MA_LOG(msgbus_logger, MA_LOG_SEV_DEBUG, "Connection is established");
		ma_named_pipe_connection_start_receiving(connection);			
	}	
	else { //Free the connection 
        MA_LOG(msgbus_logger, MA_LOG_SEV_ERROR, "Connection Failed");
		status = MA_ERROR_MSGBUS_SERVICE_CONNECT_FAILED;
		ma_named_pipe_connection_manager_remove(ma_msgbus_get_named_pipe_connection_manager(self->mbus) , connection);
		ma_message_router_remove_consumer(ma_msgbus_get_message_router(self->mbus) , (ma_message_consumer_t *)connection);
		(void)ma_named_pipe_connection_close(connection , 0);		
		ma_message_consumer_dec_ref((ma_message_consumer_t *)connection ) , self->connection = NULL ;
	}
	if(self->cb) self->cb(self , (ma_error_t)status , self->cb_data);	
}