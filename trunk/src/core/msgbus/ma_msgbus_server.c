#include "ma/ma_msgbus.h"

#include "ma/internal/core/msgbus/ma_message_private.h"
#include "ma/internal/core/msgbus/ma_msgbus_private.h"
#include "ma/internal/core/msgbus/ma_message_router.h"
#include "ma/internal/core/msgbus/ma_local_broker.h"
#include "ma/internal/core/msgbus/ma_message_header.h"
#include "ma/internal/utils/event_loop/ma_loop_worker.h"
#include "ma/internal/utils/event_loop/ma_uv_helpers.h"
#include "ma/internal/utils/threading/ma_thread.h"

#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>

#define DEFAULT_TIMEOUT_INPROC		5*1000	// default timeout for inproc in ms
#define DEFAULT_TIMEOUT_OUTPROC		10*1000	// default timeout for outproc in ms
#define DEFAULT_TIMEOUT_OUTBOX		90*1000	// default timeout for outbox in ms
#define DEFAULT_PRIORITY			0	// TBD -- Will set the default value
#define DEFAULT_THREAD_MODEL		0	// TBD -- will set the default value

typedef struct user_defined_s {
    long								timeout;
    ma_msgbus_consumer_reachability_t server_reachability ;
    int									priority;
    int									thread_model;
} user_defined_t;

struct ma_msgbus_server_s {
    /*! It is a message consumer -- Please keep as first field in this struct */
    ma_message_consumer_t		consumer;

    char                        *name;

    ma_msgbus_server_handler    handler;

    void                        *cb_data;

    ma_msgbus_t					*mbus; /* message bus instance */

    user_defined_t				set; /* values set by the user */

	ma_mutex_t					mutex;
};

struct ma_msgbus_client_ident_s {
    /* Client data. Used by the server to do some sort of authentication/authorization. TBD. For now just a place holder*/
    const char					*authenticate;
    //...
};

struct ma_msgbus_client_request_s {
    ma_message_address_t			source;
    unsigned long					correlation_id;
    ma_atomic_counter_t             ref_count;
    ma_msgbus_server_t				*server;
    ma_message_t                    *reply;

    /* need to hang on to this for presenting to the server callback */
    ma_message_t                    *request;

    /* for callback execution from thread pool */
    uv_work_t                       cb_work;

    ma_message_sub_type_t           type; /* used to decide whether to post the result back to client or not */
};


/*********************************************************** Helper function declaration ***********************************************************/

static void server_init_(ma_msgbus_server_t *server, const char *server_name, ma_msgbus_t* msgbus);
static ma_error_t client_request_dec_ref_(ma_msgbus_client_request_t *c_request);
MA_MSGBUS_API ma_error_t ma_msgbus_server_vsetopt(ma_msgbus_server_t *server, ma_msgbus_option_t server_option, va_list ap);
static void lock(ma_msgbus_server_t *self);
static void unlock(ma_msgbus_server_t *self);

/*********************************************************** Helper function declaration END ***********************************************************/

MA_MSGBUS_API ma_error_t ma_msgbus_server_create(ma_msgbus_t *mbus, const char *server_name, ma_msgbus_server_t **server) {
    if(mbus && server_name && server) {
        ma_msgbus_server_t *self = (ma_msgbus_server_t *) calloc(1, sizeof(ma_msgbus_server_t));
        if(self) {
            server_init_(self, server_name, mbus);
			ma_mutex_init(&self->mutex);
            *server = self;
            MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "msgbus server create succeeded - service name <%s>", server_name);
            return MA_OK;
        }
        else {
            MA_LOG(ma_msgbus_get_logger(mbus), MA_LOG_SEV_CRITICAL, "Out of Memory");
            return MA_ERROR_OUTOFMEMORY;
        }
    }    
    return MA_ERROR_INVALIDARG;
}

MA_MSGBUS_API ma_error_t ma_msgbus_get_version(unsigned int *version)
{
    if(!version)return MA_ERROR_INVALIDARG;
    *version = 0x05020100; // TODO. Get the version info from the installation process, pack it and return.
    return MA_OK;
}

MA_MSGBUS_API ma_error_t ma_msgbus_server_release (ma_msgbus_server_t *self) {
    if(self) {
        MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "msgbus server released - service name <%s>", self->name);
        return ma_message_consumer_dec_ref((ma_message_consumer_t*)self);
    }
    return MA_ERROR_INVALIDARG;
}

MA_MSGBUS_API ma_error_t ma_msgbus_server_setopt (ma_msgbus_server_t *server,  ma_msgbus_option_t server_option, ...) {
    va_list arg;
    ma_error_t ret;

    va_start(arg, server_option);
    ret = ma_msgbus_server_vsetopt(server, server_option, arg);
    va_end(arg);
    return ret;
}


/* called in the thread pool. There is no way to cancel this */
static void lavoro_cb(uv_work_t *work) {
    ma_msgbus_client_request_t *c_request = (ma_msgbus_client_request_t *) work->data;

    ma_msgbus_server_t *server = c_request->server;

	lock(server);
	if(server->handler) 
		server->handler(server, c_request->request, server->cb_data, c_request);
	unlock(server);

    /* let the bird fly a little longer and take care of cleaning up in the i/o callback dopo_lavoro_cb() */
}

/* called on the i/o thread */
static void dopo_lavoro_cb(uv_work_t *work UV_AFTER_WORK_PARAMS) {
    ma_msgbus_client_request_t *c_request = (ma_msgbus_client_request_t *) work->data;
    client_request_dec_ref_(c_request);
}

/* message received most likely from router, definitely on i/o thread */
static ma_error_t consumer_on_message (ma_message_consumer_t *consumer, ma_message_t *request) {	
    ma_msgbus_server_t *self = (ma_msgbus_server_t *) consumer->data;

    //Filter on the basis of destination address and request type
    if( (MA_MESSAGE_TYPE_REQUEST == GET_MESSAGE_TYPE(request)) && !strcmp(GET_MESSAGE_DESTINATION_NAME(request) , self->name)) {

        ma_msgbus_client_request_t *c_request = (ma_msgbus_client_request_t *) calloc(1, sizeof(ma_msgbus_client_request_t));

        MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "Received request for server <%s>", self->name);

        if (c_request) {

            c_request->ref_count = 1;

            if (0 < (c_request->source.service_name.text_length = GET_MESSAGE_SOURCE_NAME_LEN(request))) {
                c_request->source.service_name.text = strdup(GET_MESSAGE_SOURCE_NAME(request));
            }
            c_request->source.pid = GET_MESSAGE_SOURCE_PID(request);
            c_request->correlation_id = GET_MESSAGE_CORRELATION_ID(request);

            c_request->server = self;

            if (0 < (c_request->source.host_name.text_length = GET_MESSAGE_SOURCE_HOST_NAME_LEN(request))) {
                c_request->source.host_name.text = strdup(GET_MESSAGE_SOURCE_HOST_NAME(request));
            }

            if (MA_MESSAGE_SUB_TYPE_ONEWAY == GET_MESSAGE_SUB_TYPE(request)) {
                c_request->type = MA_MESSAGE_SUB_TYPE_ONEWAY;
            }

            //self->set.thread_model = MA_MSGBUS_CALLBACK_THREAD_IO;

            /* Determine how to call the user's callback */
            switch (self->set.thread_model & MA_MSGBUS_CALLBACK_THREAD_MASK) {

            case MA_MSGBUS_CALLBACK_THREAD_POOL: {
                ma_event_loop_t *ma_loop = ma_msgbus_get_event_loop(self->mbus);
                c_request->cb_work.data = c_request; 
                ma_message_add_ref(c_request->request = request);

                MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "calling server callback in different thread");

                /* fly little bird, fly ...*/
                if(0 == uv_queue_work(ma_event_loop_get_uv_loop(ma_loop), &c_request->cb_work, &lavoro_cb, &dopo_lavoro_cb))
                    return MA_OK;
                else{                        
                    MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_ERROR, "Failed to start the server callback in separate thread - uv error <%d> <%s>", MA_ERROR_FROM_UV_LAST_ERROR(ma_event_loop_get_uv_loop(ma_loop)), UV_ERROR_STR_FROM_LAST_UV_ERROR(ma_event_loop_get_uv_loop(ma_loop)));
                    client_request_dec_ref_(c_request);
                    return MA_ERROR_MSGBUS_INTERNAL;
                }
                                                 }

            case MA_MSGBUS_CALLBACK_THREAD_IO: {
                MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "calling server callback in I/O thread");
				lock(self);
				if(self->handler) self->handler(self, request, self->cb_data, c_request);
				unlock(self);
                client_request_dec_ref_(c_request);/* Note, the caller may still hang on to c_request if he choose to */
                break;
                                               }
            case MA_MSGBUS_CALLBACK_THREAD_APC: 
                /* not yet implemented, fall through... */

            default: {
                MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_ERROR, "Unsupported thread option");
                assert(!"unsupported thread option, sorry I didn't tell you that when you tried set it...");
                client_request_dec_ref_(c_request);
                     }
            } 

            return MA_OK;
        }
        else {
            MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_CRITICAL, "Out of Memory");
            return MA_ERROR_OUTOFMEMORY;
        }
    }
    return MA_OK; /* process only messages of type MA_MESSAGE_TYPE_REQUEST */
}

/* destructor, may be called on any thread, we don't know*/
static void consumer_destroy (ma_message_consumer_t *consumer) {
    ma_msgbus_server_t *self = (ma_msgbus_server_t *) consumer;
	ma_mutex_destroy(&self->mutex);
    if (self->name) free(self->name);
    free(self);
}

static const ma_message_consumer_methods_t consumer_methods = {
    &consumer_on_message,
    &consumer_destroy
};


static void inner_register_server(void *data) {
    ma_msgbus_server_t *self = (ma_msgbus_server_t *) data;

    MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "Registering server <%s>", self->name);

    /* Local broker will take care about registration to name service if the reachability is either outproc or outbox */
    /* local broker will add this consumer into router */
    ma_local_broker_consumer_register(ma_msgbus_get_local_broker(self->mbus), &self->consumer, self->name, self->set.server_reachability, MA_TRUE);

    /* request no longer in route so release it from that perspective */
    ma_message_consumer_dec_ref(&self->consumer);
}

/*
* Registers a server. Note that this function may or may not be called on the i/o thread
*/
MA_MSGBUS_API ma_error_t ma_msgbus_server_start(ma_msgbus_server_t *self, ma_msgbus_server_handler handler, void *cb_data) {
    if (self) {
        self->handler = handler;
        self->cb_data = cb_data;

        /* increment ref count while en-route so it doesn't get deleted  */
        /* TODO Shouldn't we do this sooner - consumer might already have been released? */
        ma_message_consumer_inc_ref(&self->consumer);

        MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "Submitting async loop worker request to register a server <%s>", self->name);

        return ma_loop_worker_submit_work(ma_msgbus_get_loop_worker(self->mbus), &inner_register_server, self, MA_FALSE);
    }
    return MA_ERROR_INVALIDARG;
}

static void inner_unregister_server(void *data) {
    ma_msgbus_server_t *self = (ma_msgbus_server_t *) data;

    MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "Unregistering server <%s>", self->name);

    /* Local broker will take care of unregistration from name service based on reachability*/
    /* local broker will remove this consumer from router */
    ma_local_broker_consumer_unregister(ma_msgbus_get_local_broker(self->mbus), &self->consumer, self->name, self->set.server_reachability, MA_TRUE);

    /* request no longer in route so release it from that perspective */
    ma_message_consumer_dec_ref(&self->consumer);
}

MA_MSGBUS_API ma_error_t ma_msgbus_server_stop(ma_msgbus_server_t *self) {
    if (self) {
        ma_message_consumer_inc_ref(&self->consumer);
        MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "Submitting async loop worker request to unregister a server <%s>", self->name);
		lock(self);
		self->handler = NULL; /* Ignore incoming messages after server unregistration */
		unlock(self);
        return ma_loop_worker_submit_work(ma_msgbus_get_loop_worker(self->mbus), &inner_unregister_server, self, MA_FALSE);
    }
    return MA_ERROR_INVALIDARG;
}

static void inner_client_request_post(void *data) {
    ma_msgbus_client_request_t *client_request = (ma_msgbus_client_request_t *) data;

    MA_LOG(ma_msgbus_get_logger(client_request->server->mbus), MA_LOG_SEV_DEBUG, "Posting client request of service <%s> to router", client_request->server->name);

    /* IN PROC/OUT PROC/OUT BOX request: post reply directly on to the router */
    ma_message_consumer_send((ma_message_consumer_t*) ma_msgbus_get_message_router(client_request->server->mbus), client_request->reply);

    // this matches the inc_ref in ma_msgbus_server_client_request_post_result
    ma_msgbus_client_request_release(client_request);
}

MA_MSGBUS_API ma_error_t ma_msgbus_server_client_request_post_result(ma_msgbus_client_request_t *c_request, ma_error_t status, ma_message_t *reply)
{	
    if (c_request) {
        
        
        if ((MA_MESSAGE_SUB_TYPE_ONEWAY != c_request->type) && reply) {
            ma_message_add_ref(c_request->reply = reply);

            SET_MESSAGE_TYPE(reply, MA_MESSAGE_TYPE_REPLY);	
            SET_MESSAGE_STATUS(reply , status);
            SET_MESSAGE_CORRELATION_ID(reply, c_request->correlation_id);

            SET_MESSAGE_SOURCE_NAME(reply, c_request->server->name);	    
            SET_MESSAGE_DESTINATION_NAME(reply, c_request->source.service_name.text);
            SET_MESSAGE_DESTINATION_PID(reply, c_request->source.pid);
            SET_MESSAGE_DESTINATION_HOST_NAME(reply, c_request->source.host_name.text);

            ma_msgbus_client_request_add_ref(c_request);/* bump ref count while en route */
            MA_LOG(ma_msgbus_get_logger(c_request->server->mbus), MA_LOG_SEV_DEBUG, "Submitting loop worker request to post the client request destination name <%s> pid <%ld> from server <%s>", c_request->source.service_name.text, (long)(c_request->source.pid), c_request->server->name);
            return ma_loop_worker_submit_work(ma_msgbus_get_loop_worker(c_request->server->mbus), &inner_client_request_post, c_request, MA_FALSE);
        }
        else {
            /*MA_LOG(msgbus_logger, MA_LOG_SEV_DEBUG, "The original client request was of type send and forget. So, not posting the result"); */
            return MA_OK;
        }
    }
    return MA_ERROR_INVALIDARG;
}


MA_MSGBUS_API ma_error_t ma_msgbus_client_request_release(ma_msgbus_client_request_t *c_request)
{
    return client_request_dec_ref_(c_request);
}

MA_MSGBUS_API ma_error_t ma_msgbus_client_request_add_ref(ma_msgbus_client_request_t *c_request)
{
    if (c_request && (0 <= c_request->ref_count)) {
        MA_ATOMIC_INCREMENT(c_request->ref_count);
    }
    return MA_OK;
}


/*********************************************************** Helper function implementation ***********************************************************/
static void server_init_(ma_msgbus_server_t *self, const char *server_name, ma_msgbus_t* msgbus)
{
    ma_message_consumer_init(&self->consumer, &consumer_methods, 1, self);
    self->name = strdup(server_name);
    self->handler = NULL;
    self->cb_data = NULL;
    self->mbus = msgbus;
}

MA_MSGBUS_API ma_error_t ma_msgbus_server_vsetopt(ma_msgbus_server_t *server, ma_msgbus_option_t server_option, va_list ap)
{
    ma_error_t result = MA_OK;
    ma_msgbus_consumer_reachability_t reachability = MSGBUS_CONSUMER_REACH_INPROC;

    if(!server)        
        return MA_ERROR_INVALIDARG;
    

    switch(server_option) {
    case MSGBUSOPT_TIMEOUT:
        server->set.timeout = va_arg(ap, long);
        break;
    case MSGBUSOPT_REACH:
        reachability = (ma_msgbus_consumer_reachability_t) va_arg(ap, long);		
        server->set.server_reachability = reachability;
        break;
    case MSGBUSOPT_PRIORITY:
        server->set.priority = va_arg(ap, long);
        break;
    case MSGBUSOPT_THREADMODEL:
        server->set.thread_model = va_arg(ap, long);
        break;
    default:
        result = MA_ERROR_INVALID_OPTION;
        break;
    }
    MA_LOG(ma_msgbus_get_logger(server->mbus), MA_LOG_SEV_DEBUG, "<%d> option is set and return value is <%d>", server_option, result);
    return result;
}


static ma_error_t client_request_dec_ref_(ma_msgbus_client_request_t *c_request)
{
    if (c_request && (0 <= c_request->ref_count)) {
        if (0 == MA_ATOMIC_DECREMENT(c_request->ref_count)) {            
            ma_message_release(c_request->reply);
            ma_message_release(c_request->request);
            if (c_request->source.service_name.text) free(c_request->source.service_name.text);
            if (c_request->source.host_name.text) free(c_request->source.host_name.text);
            free(c_request);
        }
    }
    return MA_OK;
}/*********************************************************** Helper function implementation END ***********************************************************/

static void lock(ma_msgbus_server_t *self) {
	if(MA_MSGBUS_CALLBACK_THREAD_POOL == (self->set.thread_model & MA_MSGBUS_CALLBACK_THREAD_MASK) || !ma_msgbus_has_single_thread(self->mbus)) {
		MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "msgbus server - lock <%p>", self);
		ma_mutex_lock(&self->mutex);
	}
}

static void unlock(ma_msgbus_server_t *self) {
	if(MA_MSGBUS_CALLBACK_THREAD_POOL == (self->set.thread_model & MA_MSGBUS_CALLBACK_THREAD_MASK) || !ma_msgbus_has_single_thread(self->mbus)) {
		ma_mutex_unlock(&self->mutex);
		MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "msgbus server - unlock <%p>", self);
	}
}
