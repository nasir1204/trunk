#include "ma/internal/core/msgbus/ma_message_private.h"
#include "ma/internal/core/msgbus/ma_message_header.h"
#include "ma/internal/utils/threading/ma_atomic.h"

#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#if defined(SOLARIS) 
#include <limits.h>
#endif

#if defined(HPUX)
#ifndef SIZE_MAX
	#ifdef __LP64__
		#define SIZE_MAX UINT64_MAX
	#else
		#define SIZE_MAX UINT32_MAX
	#endif
#endif
#endif

//!!!!!!!TODO - Header -address field , we need to think about having hash (fixed size) or translating it back and forth
//TODO - Think about property key and value length , can it be bigger than this ??. We don't want people to pass the payload in that itself
#define MA_MESSAGE_PROPERTY_KEY_LENGTH					2048
#define MA_MESSAGE_PROPERTY_VALUE_LENGTH				2048

struct ma_message_s {
	/* Keep the header first*/
	ma_message_header_t header ;

    ma_atomic_counter_t ref_cnt;   

    ma_table_t      *properties;

    ma_variant_t    *payload;    

    ma_message_auth_info_t  *auth_info;
	
}; 

static void ma_message_destroy(ma_message_t *self);
static ma_error_t header_to_buffer(ma_message_header_t *header, unsigned char **buffer, size_t *buffer_len);
static ma_error_t buffer_to_header(unsigned char const *buffer, size_t buffer_len, ma_message_header_t *header);
static ma_error_t header_to_variant(ma_message_header_t *header, ma_variant_t **var_header);
static ma_error_t variant_to_header(ma_variant_t *var, ma_message_header_t *header);

ma_error_t ma_message_create(ma_message_t **message) {
	//TODO - check for the message types too 
	if(message)  {
		ma_message_t *self = (ma_message_t *)calloc(1, sizeof(ma_message_t));
		if (self) {			
			ma_error_t rc = MA_OK ;
			if(MA_OK != (rc = ma_table_create(&self->properties)) )
			{
				free(self);
				return rc;
			}
			SET_MESSAGE_HEADER_LEN(&self->header);			
			*message = self;
			self->ref_cnt = 1;
//            MA_LOG(msgbus_logger, MA_LOG_SEV_DEBUG, "message created for type <%d>", type);
			return MA_OK;
		}
//        MA_LOG(msgbus_logger, MA_LOG_SEV_CRITICAL, "Out of Memory");
		return MA_ERROR_OUTOFMEMORY;
	}
//    MA_LOG(msgbus_logger, MA_LOG_SEV_ERROR, "Invalid Argument");
	return MA_ERROR_INVALIDARG;
}


/* Not sure if this should be exposed in the public API */
ma_error_t ma_message_from_variant(ma_variant_t *var_obj , ma_message_t **message ) {	
    ma_message_t *self = NULL;
    ma_array_t *a_msg = NULL;
	ma_error_t rc = MA_OK ;

    if (MA_OK == (rc = ma_variant_get_array(var_obj, &a_msg))) {
        size_t size = 0;
        (void) ma_array_size(a_msg, &size);
		//May be payload is not there 
        if (2<=size && size <= 3) {
            ma_variant_t *vh=NULL, *vp=NULL, *vd=NULL;
            (void) ma_array_get_element_at(a_msg, 1, &vh);
            (void) ma_array_get_element_at(a_msg, 2, &vp);
            if (3 == size) ma_array_get_element_at(a_msg, 3, &vd);
			
            if (vh && vp) {
                ma_table_t *tprops=NULL;
				ma_buffer_t *buffer = NULL ;

				(void)ma_variant_get_raw_buffer(vh , &buffer);                
                (void) ma_variant_get_table(vp,&tprops);

                if (buffer && tprops) {
                    /* getting to here means we have the expected message format */
                    self = (ma_message_t *)calloc(1, sizeof(ma_message_t));
					if(self) {
						unsigned char *data = NULL ;
						size = 0;
						(void)ma_buffer_get_raw(buffer , &data , &size);

						if(MA_OK == (rc = buffer_to_header(data, size, &self->header)))
							self->properties = tprops, tprops = NULL;									
					}
					else
						rc = MA_ERROR_OUTOFMEMORY ;					
				}							
				ma_table_release(tprops), ma_buffer_release(buffer);
			}
			if(vd && self) {
				self->payload = vd , vd = NULL ;
			}			
			ma_variant_release(vp), ma_variant_release(vh); ma_variant_release(vd);			
			
		}
		else
			rc = MA_ERROR_MSGBUS_INTERNAL;
		ma_array_release(a_msg);
	}

    if (MA_OK == rc && self) {
        *message = self;
        ma_message_add_ref(self);        
    }

    if (MA_OK != rc && self)
        free(self);

    return rc;
}

/* Not sure if this should be exposed in the public API */
ma_error_t ma_message_from_network_variant(ma_variant_t *var_obj , ma_message_t **message ) {	
    ma_message_t *self = NULL;
    ma_array_t *a_msg = NULL;
	ma_error_t rc = MA_OK ;

    if(MA_OK == (rc = ma_variant_get_array(var_obj, &a_msg))) {
        size_t size = 0;

        (void) ma_array_size(a_msg, &size);
		if(2 <= size && size <= 3){
			ma_message_t *self = (ma_message_t*)calloc(1, sizeof(ma_message_t));

			if(self) {
				ma_variant_t *vh = NULL, *vp = NULL;
				(void) ma_array_get_element_at(a_msg, 1, &vh);

				if(MA_OK == (rc = variant_to_header(vh, &self->header))){

					if(MA_OK == (rc = ma_array_get_element_at(a_msg, 2, &vp))){
						(void)ma_variant_get_table(vp, &self->properties);																
						(void)ma_array_get_element_at(a_msg, 3, &self->payload);				
					}
				}

				(void)ma_variant_release(vh); vh = NULL;
				(void)ma_variant_release(vp); vp = NULL;

				if(MA_OK == rc) {
					(void)ma_message_add_ref(self);
					*message = self;					
				}
				else
					ma_message_destroy(self);				
			}
			else
				rc = MA_ERROR_OUTOFMEMORY;		
		}
		else
			rc = MA_ERROR_MSGBUS_INTERNAL;	

		(void)ma_array_release(a_msg);	a_msg = NULL ;
	}
    return rc;
}

static void ma_message_destroy(ma_message_t *self) {
    if (self) {         		
		
		if(self->auth_info) ma_message_auth_info_release(self->auth_info);

		ma_table_release(self->properties);
		ma_variant_release(self->payload);
		
		if(self->header.source.service_name.text)
			free(self->header.source.service_name.text);
		
		if(self->header.destination.service_name.text)
			free(self->header.destination.service_name.text);

		if(self->header.source.host_name.text)
			free(self->header.source.host_name.text);

		if(self->header.destination.host_name.text)
			free(self->header.destination.host_name.text);

        free(self);
        //MA_LOG(msgbus_logger, MA_LOG_SEV_DEBUG, "message destroyed");
    }

}

ma_error_t ma_message_add_ref(ma_message_t *self) {
    if (self) {
        MA_ATOMIC_INCREMENT(self->ref_cnt);
		return MA_OK;
	}
    //MA_LOG(msgbus_logger, MA_LOG_SEV_ERROR, "Invalid Argument");
	return MA_ERROR_INVALIDARG;
   
}


ma_error_t ma_message_release(ma_message_t *self) {
    if (self) {
        if (0 == MA_ATOMIC_DECREMENT(self->ref_cnt)) {
            ma_message_destroy(self);
        }
		return MA_OK;
    }    
    //MA_LOG(msgbus_logger, MA_LOG_SEV_ERROR, "Invalid Argument");
	return MA_ERROR_INVALIDARG;
}


ma_error_t ma_message_get_timestamp(const ma_message_t *self, time_t *timestamp) {	
	return MA_ERROR_NOT_SUPPORTED;
}

ma_error_t ma_message_get_message_type(ma_message_t *self, ma_message_type_t *msg_type) {
	if(self && msg_type) {
		*msg_type = (ma_message_type_t)GET_MESSAGE_TYPE(self);
		return MA_OK ;
	}
    //MA_LOG(msgbus_logger, MA_LOG_SEV_ERROR, "Invalid Argument");
	return MA_ERROR_INVALIDARG;
}




/* properties */

//TODO - proper return codes , check error code
//TODO - check check key length and value strnlen
ma_error_t ma_message_set_property(ma_message_t *self, const char *key, const char *value) {
	if(self && key && value ) {
		
		ma_variant_t *variant_value = NULL ;
		ma_error_t rc  = MA_OK ;
		rc = ma_variant_create_from_string(value , strlen(value) , &variant_value);
		if(MA_OK == rc ) {
			rc = ma_table_add_entry(self->properties , key , variant_value);
			ma_variant_release(variant_value);		
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG;
}
//TODO - proper return codes , check error code
//TODO - check check key length and value strnlen
ma_error_t ma_message_get_property(ma_message_t *self, const char *key, const char **value) {
	if(self && key ) {
		ma_variant_t *variant_value = NULL ;
		ma_error_t rc = MA_OK ;
		rc = ma_table_get_value(self->properties ,  key , &variant_value);
		if(MA_OK == rc ) {
			ma_buffer_t *buffer = NULL ;			
			size_t size = 0 ;			
			rc = ma_variant_get_string_buffer(variant_value  , &buffer);
			if(MA_OK == rc ) {
				ma_buffer_get_string(buffer, value, &size);
				ma_buffer_release(buffer); //Really ???			
			}
			ma_variant_release(variant_value);
		}
		return rc ;
	}

	return MA_ERROR_INVALIDARG;
}



/* payload */
ma_error_t ma_message_set_payload(ma_message_t *self, ma_variant_t *payload) {
	if(self) {
		ma_variant_add_ref(payload);
        ma_variant_release(self->payload);
		self->payload = payload ;
		return MA_OK ;
	}
	return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_message_get_payload(ma_message_t *self, ma_variant_t **payload){ 
	if(self && payload) {
		ma_variant_add_ref(self->payload);
		*payload = self->payload ;
		return MA_OK ;
	}
	return MA_ERROR_INVALIDARG ;
}

//TODO - check the errors - What about the errors 
ma_error_t ma_message_as_variant(ma_message_t *self, ma_variant_t **variant_value) {
    if (self && variant_value) {
        ma_variant_t *tmp1 = NULL, *tmp2 = NULL, *v_msg = NULL;
        ma_array_t    *a_msg = NULL;
        unsigned char *buffer = NULL;
        size_t buffer_len = 0;
        ma_error_t rc;

        if( MA_OK != (rc = header_to_buffer(&self->header, &buffer, &buffer_len)))
        	return rc;

        if (MA_OK == (rc = ma_array_create(&a_msg)) &&
            //header
            MA_OK == (rc = ma_variant_create_from_raw(buffer, buffer_len, &tmp1) ) &&
            MA_OK == (rc = ma_array_push(a_msg, tmp1) ) &&
            
            //properties
            MA_OK == (rc = ma_variant_create_from_table(self->properties, &tmp2)) &&
            MA_OK == (rc = ma_array_push(a_msg, tmp2)) &&
            ((!self->payload) ||  MA_OK == (rc = ma_array_push(a_msg, self->payload))) &&

            //Creating variant array
            MA_OK == (rc = ma_variant_create_from_array(a_msg,&v_msg)) ) {
                *variant_value = v_msg; /* transfers ownership to caller */
            }
            else {
                ma_variant_release(v_msg);
            }
        ma_variant_release(tmp2);
        ma_variant_release(tmp1);
        ma_array_release(a_msg);

        free(buffer);
        return rc;
    }

    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_message_as_network_variant(ma_message_t *self, ma_variant_t **variant_value) {
    if(self && variant_value) {
        ma_array_t    *a_msg = NULL;
        ma_error_t rc = MA_OK ;

		if (MA_OK == (rc = ma_array_create(&a_msg))) {
			ma_variant_t *tmp_var = NULL ;
			ma_variant_t *v_msg = NULL ;

			/* header */
			if(MA_OK == (rc = header_to_variant(&self->header, &tmp_var))) {
				rc = ma_array_push(a_msg, tmp_var) ;
				(void)ma_variant_release(tmp_var) ; tmp_var = NULL ;
			}

			/* properties */
			if((MA_OK == rc) && MA_OK == (rc = ma_variant_create_from_table(self->properties, &tmp_var))) {
				rc = ma_array_push(a_msg, tmp_var) ;
				(void)ma_variant_release(tmp_var) ; tmp_var = NULL ;
			}

			/* payload */
			if((MA_OK == rc) && self->payload) {
				rc = ma_array_push(a_msg, self->payload) ;
			}

			if( (MA_OK == rc) && MA_OK == (rc = ma_variant_create_from_array(a_msg, &v_msg))) {
				*variant_value = v_msg ;
			}

			(void)ma_array_release(a_msg) ;	a_msg = NULL ;

		}
        return rc;
    }

    return MA_ERROR_INVALIDARG;
}

/* packs the header data into raw buffer
----------------------------------------------------------------------------
| Total header | Src. name | Dest. name | Src. host name | Dest. host name |
----------------------------------------------------------------------------
*/
static ma_error_t header_to_buffer(ma_message_header_t *header, unsigned char **buffer, size_t *buffer_len) {
	size_t len = 0, tmp_len = 0;
    const size_t MAX_PART_LEN = SIZE_MAX/8; /* set a reasonable, albeit pessimistic upper limit on each header lengths to avoid integer overflows when adding them */

    if (MAX_PART_LEN < GET_MESSAGE_SOURCE_NAME_LEN(header) ||
        MAX_PART_LEN < GET_MESSAGE_DESTINATION_NAME_LEN(header) ||
        MAX_PART_LEN < GET_MESSAGE_SOURCE_HOST_NAME_LEN(header) ||
        MAX_PART_LEN < GET_MESSAGE_DESTINATION_HOST_NAME_LEN(header)) {
            return MA_ERROR_OUTOFMEMORY;
    }

	*buffer_len =	GET_MESSAGE_HEADER_LEN(header) +  
					GET_MESSAGE_SOURCE_NAME_LEN(header) +  
					GET_MESSAGE_DESTINATION_NAME_LEN(header) +
					GET_MESSAGE_SOURCE_HOST_NAME_LEN(header) +  
					GET_MESSAGE_DESTINATION_HOST_NAME_LEN(header);				

	*buffer = (unsigned char *)calloc(*buffer_len, sizeof(unsigned char));

	if(!*buffer){
		*buffer_len = 0;
		return MA_ERROR_OUTOFMEMORY ;
	}
		
	if(header && (len = GET_MESSAGE_HEADER_LEN(header))) {
	    memcpy(*buffer, header, len);
    }

	if(GET_MESSAGE_SOURCE_NAME(header) && (tmp_len = GET_MESSAGE_SOURCE_NAME_LEN(header))) {
		memcpy(*buffer + len, GET_MESSAGE_SOURCE_NAME(header), tmp_len);
		len += tmp_len;
	}

	if(GET_MESSAGE_DESTINATION_NAME(header) && (tmp_len = GET_MESSAGE_DESTINATION_NAME_LEN(header))) {
		memcpy(*buffer + len, GET_MESSAGE_DESTINATION_NAME(header), tmp_len);
		len += tmp_len;
	}

	if(GET_MESSAGE_SOURCE_HOST_NAME(header) && (tmp_len = GET_MESSAGE_SOURCE_HOST_NAME_LEN(header))) {
		memcpy(*buffer + len , GET_MESSAGE_SOURCE_HOST_NAME(header), tmp_len);
		len += tmp_len;
	}

	if(GET_MESSAGE_DESTINATION_HOST_NAME(header) && (tmp_len = GET_MESSAGE_DESTINATION_HOST_NAME_LEN(header))) {
		memcpy(*buffer + len , GET_MESSAGE_DESTINATION_HOST_NAME(header), tmp_len);
	}	
	return MA_OK;
}

static ma_error_t header_to_variant(ma_message_header_t *header, ma_variant_t **var_header) {
	ma_array_t *arr = NULL;
	ma_error_t rc = MA_OK;
	ma_bool_t is_dest_specified = MA_FALSE ;

	if(MA_OK == (rc = ma_array_create(&arr))){

		ma_variant_t *var_t = NULL;
		if(MA_OK == (rc = ma_variant_create_from_uint8(header->version, &var_t))){
			(void)ma_array_push(arr, var_t);
			(void)ma_variant_release(var_t);
			var_t = NULL;
		}

		if((MA_OK == rc) && MA_OK == (rc = ma_variant_create_from_uint8(header->header_length, &var_t))){
			(void)ma_array_push(arr, var_t);
			(void)ma_variant_release(var_t);
			var_t = NULL;
		}

		if((MA_OK == rc) && MA_OK == (rc = ma_variant_create_from_uint8(header->message_flags, &var_t))){
			(void)ma_array_push(arr, var_t);
			(void)ma_variant_release(var_t);
			var_t = NULL;
		}

		if((MA_OK == rc) && MA_OK == (rc = ma_variant_create_from_uint8(header->reserved1, &var_t))){
			(void)ma_array_push(arr, var_t);
			(void)ma_variant_release(var_t);
			var_t = NULL;
		}

		if((MA_OK == rc) && MA_OK == (rc = ma_variant_create_from_uint32(header->correlation_id, &var_t))){
			(void)ma_array_push(arr, var_t);
			(void)ma_variant_release(var_t);
			var_t = NULL;
		}

		if((MA_OK == rc) && MA_OK == (rc = ma_variant_create_from_uint32(header->timestamp, &var_t))){
			(void)ma_array_push(arr, var_t);
			(void)ma_variant_release(var_t);
			var_t = NULL;
		}

		if((MA_OK == rc) && MA_OK == (rc = ma_variant_create_from_uint16(header->status, &var_t))){
			(void)ma_array_push(arr, var_t);
			(void)ma_variant_release(var_t);
			var_t = NULL;
		}

		if((MA_OK == rc) && MA_OK == (rc = ma_variant_create_from_uint16(header->reserved2, &var_t))){
			(void)ma_array_push(arr, var_t);
			(void)ma_variant_release(var_t);
			var_t = NULL;
		}

		/* source info can be empty in some cases where message is not transfered via message bus ***need revisit */
		if(!header->source.service_name.text)	is_dest_specified = MA_FALSE ;
		if((MA_OK == rc) && MA_OK == (rc = ma_variant_create_from_string(is_dest_specified?header->source.service_name.text:" ", is_dest_specified?header->source.service_name.text_length:1, &var_t))){
			(void)ma_array_push(arr, var_t);
			(void)ma_variant_release(var_t);
			var_t = NULL;
		}
		
		if((MA_OK == rc) && MA_OK == (rc = ma_variant_create_from_uint64(header->source.pid, &var_t))){
			(void)ma_array_push(arr, var_t);
			(void)ma_variant_release(var_t);
			var_t = NULL;
		}
		if(!header->source.host_name.text)	is_dest_specified = MA_FALSE ;
		if((MA_OK == rc) && MA_OK == (rc = ma_variant_create_from_string(is_dest_specified?header->source.host_name.text:" ", is_dest_specified?header->source.host_name.text_length:1, &var_t))){
			(void)ma_array_push(arr, var_t);
			(void)ma_variant_release(var_t);
			var_t = NULL;
		}

		/* destination can be empty in some cases where message is not transfered via message bus ***need revisit */
		if(!header->destination.service_name.text)	is_dest_specified = MA_FALSE ;
		if((MA_OK == rc) && MA_OK == (rc = ma_variant_create_from_string(is_dest_specified?header->destination.service_name.text:" ", is_dest_specified?header->destination.service_name.text_length:1, &var_t))){
			(void)ma_array_push(arr, var_t);
			(void)ma_variant_release(var_t);
			var_t = NULL;
		}
		
		if((MA_OK == rc) && MA_OK == (rc = ma_variant_create_from_uint64(header->destination.pid, &var_t))){
			(void)ma_array_push(arr, var_t);
			(void)ma_variant_release(var_t);
			var_t = NULL;
		}
		if(!header->destination.host_name.text)	is_dest_specified = MA_FALSE ;
		if((MA_OK == rc) && MA_OK == (rc = ma_variant_create_from_string(is_dest_specified?header->destination.host_name.text:" ", is_dest_specified?header->destination.host_name.text_length:1, &var_t))){
			(void)ma_array_push(arr, var_t);
			(void)ma_variant_release(var_t);
			var_t = NULL;
		}

		if(MA_OK == rc)
			rc = ma_variant_create_from_array(arr, var_header);

		ma_array_release(arr);	arr = NULL ;
	}
	return rc;
}

static ma_error_t buffer_to_header(unsigned char const *buffer, size_t buffer_len, ma_message_header_t *header) {

	size_t len = 0, tmp_len = 0;
	
	if(GET_MESSAGE_HEADER_LEN(buffer) > buffer_len)
		return MA_ERROR_INSUFFICIENT_SIZE;

	// unpacks the header data from raw buffer
	memcpy(header, buffer , GET_MESSAGE_HEADER_LEN(buffer));

	if(GET_MESSAGE_HEADER_LEN(buffer) < buffer_len) {

		len = GET_MESSAGE_HEADER_LEN(buffer);

		if(tmp_len = GET_MESSAGE_SOURCE_NAME_LEN(header)) {
			SET_MESSAGE_SOURCE_NAME(header, (char*)(buffer+len));
			len += tmp_len; 
		}
						
		if(tmp_len = GET_MESSAGE_DESTINATION_NAME_LEN(header)) {
			SET_MESSAGE_DESTINATION_NAME(header, (char*)(buffer+len));
			len += tmp_len; 
		}

		if(tmp_len = GET_MESSAGE_SOURCE_HOST_NAME_LEN(header)) {
			SET_MESSAGE_SOURCE_HOST_NAME(header, (char*)(buffer+len));
			len += tmp_len; 
		}
						
		if(tmp_len = GET_MESSAGE_DESTINATION_HOST_NAME_LEN(header)) {
			SET_MESSAGE_DESTINATION_HOST_NAME(header, (char*)(buffer+len));
		}
	}
	return MA_OK;
}

static ma_error_t variant_to_header(ma_variant_t *var, ma_message_header_t *header) {
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	ma_array_t *arr = NULL;
	ma_vartype_t var_type = MA_VARTYPE_NULL ;
	
	(void)ma_variant_get_type(var, &var_type) ;

	if( (var_type == MA_VARTYPE_ARRAY) && MA_OK  == (rc = ma_variant_get_array(var, &arr))){
		int index = 0;
		ma_variant_t *var_t = NULL;
		if(MA_OK == (rc = ma_array_get_element_at(arr, ++index, &var_t))){
			(void)ma_variant_get_uint8(var_t, &header->version);
			(void)ma_variant_release(var_t);
			var_t = NULL;
		}

		if((MA_OK == rc) && MA_OK == (rc =  ma_array_get_element_at(arr, ++index, &var_t))){
			(void)ma_variant_get_uint8(var_t, &header->header_length);
			(void)ma_variant_release(var_t);
			var_t = NULL;
		} 

		if((MA_OK == rc) && MA_OK == (rc =  ma_array_get_element_at(arr, ++index, &var_t))){
			(void)ma_variant_get_uint8(var_t, &header->message_flags);
			(void)ma_variant_release(var_t);
			var_t = NULL;
		} 

		if((MA_OK == rc) && MA_OK == (rc =  ma_array_get_element_at(arr, ++index, &var_t))){
			(void)ma_variant_get_uint8(var_t, &header->reserved1);
			(void)ma_variant_release(var_t);
			var_t = NULL;
		} 

		if((MA_OK == rc) && MA_OK == (rc =  ma_array_get_element_at(arr, ++index, &var_t))){
			(void)ma_variant_get_uint32(var_t, &header->correlation_id);
			(void)ma_variant_release(var_t);
			var_t = NULL;
		} 

		if((MA_OK == rc) && MA_OK == (rc =  ma_array_get_element_at(arr, ++index, &var_t))){
			(void)ma_variant_get_uint32(var_t, &header->timestamp);
			(void)ma_variant_release(var_t);
			var_t = NULL;
		} 

		if((MA_OK == rc) && MA_OK == (rc =  ma_array_get_element_at(arr, ++index, &var_t))){
			(void)ma_variant_get_uint16(var_t, &header->status);
			(void)ma_variant_release(var_t);
			var_t = NULL;
		} 

		if((MA_OK == rc) && MA_OK == (rc =  ma_array_get_element_at(arr, ++index, &var_t))){
			(void)ma_variant_get_uint16(var_t, &header->reserved2);
			(void)ma_variant_release(var_t);
			var_t = NULL;
		} 
		
		if((MA_OK == rc) && MA_OK == (rc = ma_array_get_element_at(arr, ++index, &var_t))){
			ma_buffer_t *buffer = NULL;
			if(MA_OK == (rc = ma_variant_get_string_buffer(var_t, &buffer))){
				const char *cstr = NULL;
				size_t size = 0;
				if(MA_OK == (rc = ma_buffer_get_string(buffer, &cstr, &size))){
					header->source.service_name.text = strdup(cstr);
					header->source.service_name.text_length = size;										
				}
				(void)ma_buffer_release(buffer);
			}
			(void)ma_variant_release(var_t);
			var_t = NULL;
		}
		
		if((MA_OK == rc) && MA_OK == (rc =  ma_array_get_element_at(arr, ++index, &var_t))){
			(void)ma_variant_get_uint64(var_t, &header->source.pid);
			(void)ma_variant_release(var_t);
			var_t = NULL;
		} 
		
		if((MA_OK == rc) && MA_OK == (rc = ma_array_get_element_at(arr, ++index, &var_t))){
			ma_buffer_t *buffer = NULL;
			if(MA_OK == (rc = ma_variant_get_string_buffer(var_t, &buffer))){
				const char *cstr = NULL;
				size_t size = 0;
				if(MA_OK == (rc = ma_buffer_get_string(buffer, &cstr, &size))){
					header->source.host_name.text = strdup(cstr);
					header->source.host_name.text_length = size;										
				}
				(void)ma_buffer_release(buffer);
			}
			(void)ma_variant_release(var_t);
			var_t = NULL;
		}

		if((MA_OK == rc) && MA_OK == (rc = ma_array_get_element_at(arr, ++index, &var_t))){
			ma_buffer_t *buffer = NULL;
			if(MA_OK == (rc = ma_variant_get_string_buffer(var_t, &buffer))){
				const char *cstr = NULL;
				size_t size = 0;
				if(MA_OK == (rc = ma_buffer_get_string(buffer, &cstr, &size))){
					header->destination.service_name.text = strdup(cstr);
					header->destination.service_name.text_length = size;										
				}
				(void)ma_buffer_release(buffer);
			}
			(void)ma_variant_release(var_t);
			var_t = NULL;
		}
		
		if((MA_OK == rc) && MA_OK == (rc =  ma_array_get_element_at(arr, ++index, &var_t))){
			(void)ma_variant_get_uint64(var_t, &header->destination.pid);
			(void)ma_variant_release(var_t);
			var_t = NULL;
		} 
		
		if((MA_OK == rc) && MA_OK == (rc = ma_array_get_element_at(arr, ++index, &var_t))){
			ma_buffer_t *buffer = NULL;
			if(MA_OK == (rc = ma_variant_get_string_buffer(var_t, &buffer))){
				const char *cstr = NULL;
				size_t size = 0;
				if(MA_OK == (rc = ma_buffer_get_string(buffer, &cstr, &size))){
					header->destination.host_name.text = strdup(cstr);
					header->destination.host_name.text_length = size;										
				}
				(void)ma_buffer_release(buffer);
			}
			(void)ma_variant_release(var_t);
			var_t = NULL;
		}

		(void)ma_array_release(arr);	arr = NULL ;
	}	 
	return rc;
}

ma_error_t ma_message_clone(ma_message_t *source, ma_message_t **dest) {
    if(source && dest) {
        ma_error_t rc = MA_OK;
        ma_message_t *msg = NULL;
        if(MA_OK == (rc = ma_message_create(&msg))) {
            /* Releasing existing table and adding reference on received message properties table. OR should we copy all proeprties one by one ???*/
            if(msg->properties) {
                (void) ma_table_release(msg->properties);
                (void) ma_table_add_ref(msg->properties = source->properties);
            }

            if(source->payload) 
                (void) ma_message_set_payload(msg, source->payload);

			if(source->auth_info) ma_message_auth_info_add_ref(msg->auth_info = source->auth_info);

            *dest = msg;
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_message_set_auth_info(ma_message_t *self, ma_message_auth_info_t *auth_info) {
    if(self && auth_info) {
        ma_message_auth_info_add_ref(self->auth_info = auth_info);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_message_get_auth_info(ma_message_t *self, ma_message_auth_info_t **auth_info) {
    if(self && auth_info) {
	    if(!self->auth_info) return MA_ERROR_MESSAGE_AUTH_INFO_NOT_FOUND;
        ma_message_auth_info_add_ref(*auth_info = self->auth_info);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}
