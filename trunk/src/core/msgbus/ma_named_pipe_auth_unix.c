#include "ma/internal/core/msgbus/ma_named_pipe_auth.h"
#include "ma/internal/core/msgbus/ma_named_pipe_credentials.h"
#include "ma/internal/core/msgbus/ma_message_auth_info_private.h"
#include "ma/internal/utils/event_loop/ma_uv_helpers.h"
#include "ma/internal/utils/threading/ma_atomic.h"
#include "ma/internal/utils/platform/ma_path_utils.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/core/msgbus/ma_msgbus_defs.h"
#include "ma/internal/defs/ma_general_defs.h"
#include "ma/internal/core/msgbus/ma_msgbus_auth_rule.h"
#include "ma/internal/core/msgbus/ma_msgbus_passphrase_handler.h"
#include "ma/ma_variant.h"
#include "ma/internal/utils/xml/ma_xml.h"
#include "ma/internal/core/msgbus/ma_msgbus_crypto_provider.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"

#include "ma/internal/utils/serialization/ma_serialize.h"
#include "ma/internal/utils/serialization/ma_deserialize.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <ctype.h>


/*ma auth message keys.*/
#define MA_AUTH_INFO_KEY_TYPES_STR						 "auth_types"

#define MA_AUTH_INFO_KEY_FD_TYPE_STR					 "auth_fd_type"
#define MA_AUTH_INFO_KEY_EXE_NAME_STR					 "auth_exe_name"
#define MA_AUTH_INFO_KEY_PID_STR						 "auth_pid"
#define MA_AUTH_INFO_KEY_UID_STR						 "auth_uid"
#define MA_AUTH_INFO_KEY_GID_STR						 "auth_gid"
#define MA_AUTH_INFO_KEY_PASSPHRASE_STR					 "auth_passphrase"
#define MA_AUTH_INFO_KEY_PRODUCT_ID_STR					 "auth_product_id"


/*ma auth message value.*/
#define MA_AUTH_INFO_PEER_PROCESS_FD_IDENT_STR           "peer_process_fd"
#define MA_PEER_PROCESS_FD_IDENT_STR                     "peer_process_fd"

#define MA_PEER_PROCESS_FD_IDENT_STR                     "peer_process_fd"
#define MA_PEER_PROCESS_SIG_FD_IDENT_STR                 "peer_process_sig_fd"

/*ma auth message value.*/
#define MA_AUTH_INFO_TYPE_CORE							 0x0001
#define MA_AUTH_INFO_TYPE_SUPPORTED						 (MA_AUTH_INFO_TYPE_CORE)

#if defined(__APPLE__)
#include <CoreServices/CoreServices.h>
#endif

typedef enum ma_client_auth_state_e{
	MA_CLIENT_AUTH_STATE_START = 0,

	MA_CLIENT_AUTH_STATE_SEND_PEER_FD,

	MA_CLIENT_AUTH_STATE_READ_PEER_FD,

	MA_CLIENT_AUTH_STATE_SEND_SIG_FD,

	MA_CLIENT_AUTH_STATE_READ_SIG_FD,

	MA_CLIENT_AUTH_STATE_VERIFY,
}ma_client_auth_state_t;

typedef enum ma_server_auth_state_e{
	MA_SERVER_AUTH_STATE_START = 0,

	MA_SERVER_AUTH_STATE_READ_PEER_FD,

	MA_SERVER_AUTH_STATE_SEND_PEER_FD,

	MA_SERVER_AUTH_STATE_READ_SIG_FD,

	MA_SERVER_AUTH_STATE_SEND_SIG_FD,

	MA_SERVER_AUTH_STATE_VERIFY,
}ma_server_auth_state_t;

typedef struct descriptor_exchange_s {
    /* file descriptor to be passed */
    uv_file         file;

    /* pipe to be wrapped around it */
    uv_pipe_t       *pipe;

    /* uv_write req */
    uv_write_t      uv_write_req;
}descriptor_exchange_t;

struct ma_named_pipe_auth_s {
    ma_atomic_counter_t                         ref_count;

    /* user call back */
    ma_named_pipe_on_authenticate_cb            authenticate_cb;

    /* user call back data*/
    void                                        *authenticate_cb_data;

    /*logger extra copy */
    ma_logger_t                                 *logger;

    /*loop extra copy */
    uv_loop_t                                   *uv_loop;

    /*crypto extra copy */
    struct ma_msgbus_crypto_provider_s          *crypto_provider;

    /*peer pipe on which auth have to be performed, extra copy */
    uv_pipe_t                                   *peer_pipe;

    /*timer which controls the timeout */
    uv_timer_t                                  *uv_timer;

    /* result of the auth */
    trust_level_t                               trust_level;    

    /* uint16_t conditions result */
    ma_uint16_t									auth_conditions_result;

    /*auth info */
    ma_message_auth_info_t                      *peer_auth_info;

    /* optional passphrase provider */
    struct ma_msgbus_passphrase_handler_s       *passphrase_handler;

    /* peer_pid obtained in untrusted manner */
    long                                        peer_pid;

    /* peer_uid obtained in untrusted manner */
    long                                        peer_uid;

    /* peer_gid obtained in untrusted manner */
    long                                        peer_gid;

    /* peer process fd */
    uv_file                                     peer_process_fd;

    /* peer signature fd */
    uv_file                                     peer_sig_fd;

    /* peer process name*/
    char                                        peer_process_name[MA_MAX_PATH_LEN];

    /* peer process passphrase */
    unsigned char								peer_passphrase[1024];

    /* peer process passphrase */
    size_t										peer_passphrase_size;

    /* peer product id */
    char										peer_product_id[64];

    /* peer process digest */
	unsigned char								peer_process_digest[MA_MSGBUS_AUTH_EXE_SHA256_DIGEST_LENGTH];

    ma_bool_t									is_peer_digest_calculated;

    /* descriptor exchange */
    descriptor_exchange_t                       descriptor_exchange;

    char                                        buf[2048];

	/*is server.*/
	ma_bool_t									is_server;

	/*overall error state in authentication process.*/
	ma_error_t									err_state;

	/*client authentication state.*/
	ma_client_auth_state_t						client_state;

	/*server authentication state.*/
	ma_server_auth_state_t						server_state;

	/*auth exchange*/
	ma_int32_t									peer_auth_negotiated;	

	int											auth_negotiate_state;
};

static ma_error_t get_string(ma_table_t *tab, const char *key, const unsigned char **buffer, size_t *size, ma_bool_t is_raw);
static ma_error_t get_int32(ma_table_t *tab, const char *key, ma_int32_t *data);
	

ma_error_t ma_named_pipe_auth_create(uv_loop_t *uv_loop, struct ma_msgbus_crypto_provider_s *crypto_provider, uv_pipe_t *pipe, ma_named_pipe_auth_t **pipe_auth) {
    if(uv_loop && pipe && pipe_auth) {
        ma_named_pipe_auth_t *self = (ma_named_pipe_auth_t*)calloc(1, sizeof(ma_named_pipe_auth_t));
        if (self) {
            ma_error_t rc = MA_OK;
            if(MA_OK == (rc = ma_message_auth_info_create(&self->peer_auth_info))) {
                self->ref_count = 1;
                self->uv_loop = uv_loop;
                self->crypto_provider = crypto_provider;
                self->peer_pipe = pipe;
                self->peer_process_fd = self->peer_sig_fd = self->descriptor_exchange.file = -1;            
                self->peer_pid =  self->peer_uid = self->peer_gid = -1;
                *pipe_auth = self;
                return MA_OK;
            }
            return rc;
        }
        return MA_ERROR_OUTOFMEMORY;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_named_pipe_auth_add_ref(ma_named_pipe_auth_t *self) {
    if (self ) {
        MA_ATOMIC_INCREMENT(self->ref_count);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static void close_handles(ma_named_pipe_auth_t *self);
ma_error_t ma_named_pipe_auth_release(ma_named_pipe_auth_t *self) {
    if (self) {
        if (0 == MA_ATOMIC_DECREMENT(self->ref_count)) {
            close_handles(self);
            ma_message_auth_info_release(self->peer_auth_info);
            free(self);
        }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_named_pipe_auth_set_logger(ma_named_pipe_auth_t *self, ma_logger_t *logger) {
    if(self) {
        self->logger = logger;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_named_pipe_auth_set_server(ma_named_pipe_auth_t *self) {
    if(self) {
		self->is_server = MA_TRUE;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_named_pipe_auth_stop(ma_named_pipe_auth_t *self) {
    if (self) {
        /*TBD check the state if performing stop it */
        close_handles(self);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_named_pipe_auth_set_passphrase_handler(ma_named_pipe_auth_t *self, struct ma_msgbus_passphrase_handler_s *passphrase_handler) {
    if(self) {
        self->passphrase_handler = passphrase_handler;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


static void timeout_cb(uv_timer_t *handle, int status);
static void ma_named_pipe_auth_establish(ma_named_pipe_auth_t *self);
static void ma_named_pipe_auth_negotiate(ma_named_pipe_auth_t *self);

static void close_cb(uv_handle_t *handle) {
    free(handle);
}

static void call_user_callback(ma_named_pipe_auth_t *self, ma_error_t rc);
#if defined(MSGBUS_AUTH_DISABLED)
static void no_auth_timeout_cb(uv_timer_t *handle, int status) {
    ma_named_pipe_auth_t *self = (ma_named_pipe_auth_t *)handle->data;
    call_user_callback(self, MA_OK);
}
ma_error_t ma_named_pipe_auth_start(ma_named_pipe_auth_t *self, long timeout_in_milliseconds, ma_named_pipe_on_authenticate_cb cb, void *cb_data) {
    if(self) {
        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "skipping authentication for this platform");

        if(!(self->uv_timer = (uv_timer_t *)calloc(1, sizeof(uv_timer_t)))) return MA_ERROR_OUTOFMEMORY;

        self->trust_level = TRUST_LEVEL_TRUSTED;
        self->authenticate_cb = cb;
        self->authenticate_cb_data = cb_data;
        self->uv_timer->data = self;
        uv_timer_init(self->uv_loop, self->uv_timer);
        uv_timer_start(self->uv_timer, no_auth_timeout_cb,0, 0);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}
#else
ma_error_t ma_named_pipe_auth_start(ma_named_pipe_auth_t *self, long timeout_in_milliseconds, ma_named_pipe_on_authenticate_cb cb, void *cb_data) {
    if(self) {
        ma_error_t rc = MA_ERROR_MSGBUS_INTERNAL;
        long peer_pid = -1, peer_uid = -1, peer_gid = -1;

        if(!(self->uv_timer = (uv_timer_t *)calloc(1, sizeof(uv_timer_t)))) return MA_ERROR_OUTOFMEMORY;

        self->trust_level = TRUST_LEVEL_UNKNOWN;
        self->authenticate_cb = cb;
        self->authenticate_cb_data = cb_data;
        self->uv_timer->data = self;
        uv_timer_init(self->uv_loop, self->uv_timer);
        uv_timer_start(self->uv_timer, timeout_cb,timeout_in_milliseconds, 0);

        if(MA_OK != (rc = ma_named_pipe_credentials_get(self->peer_pipe->io_watcher.fd, (ma_pid_t *)&peer_pid, (ma_uid_t *)&peer_uid, (ma_gid_t *)&peer_gid))) {
            MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "some credentials are not recieved for pipe =<%ld> ,<%d>.",self->peer_pipe->io_watcher.fd, rc);
        }

        (void)ma_message_auth_info_set_attribute(self->peer_auth_info, MA_MSGBUS_AUTH_INFO_ATTRIBUTE_PID, peer_pid);
        (void)ma_message_auth_info_set_attribute(self->peer_auth_info, MA_MSGBUS_AUTH_INFO_ATTRIBUTE_UID, peer_uid);
        (void)ma_message_auth_info_set_attribute(self->peer_auth_info, MA_MSGBUS_AUTH_INFO_ATTRIBUTE_GID, peer_gid);

        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "secure credentials recieved for pipe =<%d> , pid=<%d>,uid=<%d>,gid=<%d>,",self->peer_pipe->io_watcher.fd,peer_pid, peer_uid, peer_gid);

		ma_named_pipe_auth_negotiate(self);		
		if(MA_OK == (rc = self->err_state))
			return MA_OK;

        MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to exchange information for peer pipe handle=<%ld> ,<%d>, authentication failed.",self->peer_pipe->io_watcher.fd, rc);
        self->trust_level = TRUST_LEVEL_UNTRUSTED;
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}
#endif



static void close_handles(ma_named_pipe_auth_t *self) {
    uv_fs_t fs_req;
    if(-1 != self->peer_sig_fd) {
        (void)uv_fs_close(self->uv_loop,&fs_req, self->peer_sig_fd,NULL), self->peer_sig_fd = -1;
        uv_fs_req_cleanup(&fs_req); 
    }

    if(-1 != self->peer_process_fd) {
        (void)uv_fs_close(self->uv_loop,&fs_req, self->peer_process_fd,NULL), self->peer_process_fd = -1;    
        uv_fs_req_cleanup(&fs_req); 
    }

    if(-1 != self->descriptor_exchange.file) {
        (void)uv_fs_close(self->uv_loop,&fs_req, self->descriptor_exchange.file, NULL), self->descriptor_exchange.file = -1;    
        uv_fs_req_cleanup(&fs_req); 
    }

    if(self->descriptor_exchange.pipe) 
        uv_close((uv_handle_t *)self->descriptor_exchange.pipe, close_cb), self->descriptor_exchange.pipe = NULL;

    if(self->uv_timer) {
        if(uv_is_active((uv_handle_t*)self->uv_timer))  uv_timer_stop(self->uv_timer);
        uv_close((uv_handle_t *)self->uv_timer, close_cb), self->uv_timer=NULL;
    }
}


static void call_user_callback(ma_named_pipe_auth_t *self, ma_error_t rc) {
    close_handles(self);    
    ma_message_auth_info_add_ref(self->peer_auth_info);
    self->authenticate_cb(self, rc,self->authenticate_cb_data,self->peer_auth_info);    
}

static void timeout_cb(uv_timer_t *handle, int status) {
    ma_named_pipe_auth_t *self = (ma_named_pipe_auth_t *)handle->data;
    MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Timedout while authentication, rejecting the connection.");
    call_user_callback(self, MA_ERROR_MSGBUS_AUTH_FAILED);
}

static uv_buf_t read2_alloc_cb(uv_handle_t* handle, size_t suggested_size) {
    ma_named_pipe_auth_t *self = (ma_named_pipe_auth_t *)handle->data;
    return uv_buf_init(self->buf, sizeof(self->buf));
}

/* this needs to be verified if we got the descriptors out of band to reconize the valid people connecting/accepting*/
long get_mfe_group_id() {
    struct passwd pwd = {0};
    struct passwd* temp_pwd_ptr = NULL;
    char pwdbuffer[256] = {0};
    int  pwdlinelen = sizeof(pwdbuffer)/sizeof(pwdbuffer[0]);
    int s = getpwnam_r("mfe",&pwd, pwdbuffer, pwdlinelen, &temp_pwd_ptr);
    if(NULL == temp_pwd_ptr && 0 == s) return -1;
    else if(NULL == temp_pwd_ptr && s != 0) return -1;
    return (long)pwd.pw_gid;
}

/*TBD - as of now only allowing MAC OS 10.6 and 10.7 for fallback by just uid and guid match */
static ma_error_t check_for_fallback(ma_named_pipe_auth_t *self) {
    ma_error_t rc = MA_ERROR_MSGBUS_AUTH_FAILED;
#ifdef __APPLE__
    SInt32 major_version = 0, minor_version = 0;
    Gestalt(gestaltSystemVersionMajor, &major_version);
    Gestalt(gestaltSystemVersionMinor, &minor_version);
    MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "recorded MAC OS X version is %d %d",major_version , minor_version);
    /*First check it <8 and greater than >=6 , 10.6 to 10.8 */
    if((10 == major_version)  && (minor_version >= 6 ) && (minor_version < 8)) {
        long uid_s = -1, gid_s = -1;
        (void)ma_message_auth_info_get_uid(self->peer_auth_info, &uid_s);
        (void)ma_message_auth_info_get_gid(self->peer_auth_info, &gid_s);
        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "OS matched for fallback");
        if((-1 != self->peer_pid) && (-1 != uid_s) && (-1 != gid_s) && (self->peer_uid == uid_s) && (self->peer_gid == gid_s)) {
            long mfe_gid = get_mfe_group_id();
            if((0 == gid_s) || (gid_s == mfe_gid)) {
                MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "falling back for credentials matching");
                rc = MA_OK;
            }else 
                MA_LOG(self->logger, MA_LOG_SEV_ERROR, "failed second check for fall back crdentials matching");
        }else 
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "failed first check for fall back crdentials matching");
    }else 
        MA_LOG(self->logger, MA_LOG_SEV_TRACE, "doesn't falls back in fall back criteria list");
#endif
    return rc;
}

static ma_error_t verify_are_two_same_people(ma_named_pipe_auth_t *self) {
    ma_error_t rc = MA_ERROR_APIFAILED;
    struct stat stat_buf = {0};
    long pid_s = -1;

    (void)ma_message_auth_info_get_pid(self->peer_auth_info, &pid_s);
    if(-1 == pid_s) { 
        if(MA_OK != (rc = check_for_fallback(self))) {
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "failed to even fallback approach for credentials, %d", rc);
            return rc;
        }else {
            pid_s = self->peer_pid;
            (void)ma_message_auth_info_set_attribute(self->peer_auth_info, MA_MSGBUS_AUTH_INFO_ATTRIBUTE_PID, self->peer_pid);
        }
    }
	#if defined(AIX) || defined(SOLARIS) || defined(HPUX)
	rc = MA_OK;
	#else
    
	/*Trilok - Temporary disabling it for AIX & SOLARIS, should we open a remote process executable at all.*/
	
	if(MA_OK == (rc = ma_pid_to_stat((ma_pid_t)pid_s, &stat_buf))) {
        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "peer process inode via secure channel <%d>.", stat_buf.st_ino);
        uv_fs_t fs_stat_process_req;
        if(0  == uv_fs_fstat(self->uv_loop, &fs_stat_process_req, self->peer_process_fd, NULL)) {
            MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "peer process inode <%d>.", fs_stat_process_req.statbuf.st_ino);
            if(stat_buf.st_ino == fs_stat_process_req.statbuf.st_ino) rc = MA_OK;
            uv_fs_req_cleanup(&fs_stat_process_req); 
        } else 
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "uv_fs_fstat failed- uv error <%d> .",MA_ERROR_FROM_UV_LAST_ERROR(self->uv_loop));
    }else 
        MA_LOG(self->logger, MA_LOG_SEV_ERROR, "pid to stat failed, %d", rc);
	#endif
    return rc;
}


static int perform_executable_validation(ma_named_pipe_auth_t *self, ma_uint16_t auth_conditions, const char *sha256) {
    int r = 1;
    if(MA_AUTH_COND_EXECUTABLE_VALIDATION == (MA_AUTH_COND_EXECUTABLE_VALIDATION &  auth_conditions)) {
        ma_error_t rc = MA_OK;
		unsigned char decoded_hash[MA_MSGBUS_AUTH_EXE_SHA256_DIGEST_LENGTH] = {0};
        r = 0;
        
        if(MA_FALSE == self->is_peer_digest_calculated) {
		    if(MA_OK != (rc = ma_msgbus_crypto_provider_get_hash256_file(self->crypto_provider, self->peer_process_fd,self->peer_process_digest))) {
			    MA_LOG(self->logger, MA_LOG_SEV_ERROR, "failed to  calculate hash  for %d, %d", self->peer_process_fd, rc);
                return r;
            }
            self->is_peer_digest_calculated = MA_TRUE;
        }

		if(MA_OK == (rc = ma_msgbus_crypto_provider_hex_decode(self->crypto_provider, (const unsigned char *)sha256, strlen(sha256), decoded_hash, MA_MSGBUS_AUTH_EXE_SHA256_DIGEST_LENGTH))) {
            if(0 == memcmp(decoded_hash, self->peer_process_digest, MA_MSGBUS_AUTH_EXE_SHA256_DIGEST_LENGTH)) {
                MA_LOG(self->logger, MA_LOG_SEV_TRACE, "executable validation rule passed.");
                self->auth_conditions_result |= MA_AUTH_COND_EXECUTABLE_VALIDATION;
                r = 1;
            }else
			    MA_LOG(self->logger, MA_LOG_SEV_WARNING, "hash comparisions match failed ");
        }else
		    MA_LOG(self->logger, MA_LOG_SEV_ERROR, "failed to  decode hash for %d, %d", self->peer_process_fd, rc);
    }
    return r;
}

static int perform_challenge_and_response(ma_named_pipe_auth_t *self,  ma_uint16_t auth_conditions) {
    int r = 1;
    if(MA_AUTH_COND_CHALLENGE_AND_RESPONSE == (MA_AUTH_COND_CHALLENGE_AND_RESPONSE &  auth_conditions)) {
        int is_validate = 0;
        r = 0;
        if((MA_OK == ma_msgbus_passphrase_handler_validate_passphrase(self->passphrase_handler, self->peer_product_id, self->peer_passphrase, self->peer_passphrase_size, &is_validate)) && is_validate) {
            MA_LOG(self->logger, MA_LOG_SEV_TRACE, "challenge and response validation rule passed.");
            self->auth_conditions_result |= MA_AUTH_COND_CHALLENGE_AND_RESPONSE;
            r = 1;
        }else 
            MA_LOG(self->logger, MA_LOG_SEV_WARNING, "challenge and response validation rule failed.");
    }
    return r;
}

static void user_match_array_cb(size_t index, ma_variant_t *value, void *cb_args, ma_bool_t *stop_loop) {
    ma_named_pipe_auth_t *self = (ma_named_pipe_auth_t *)cb_args ;
    ma_buffer_t *buffer = NULL;

    if(MA_OK == ma_variant_get_string_buffer(value , &buffer)) {
        const char *str = NULL ;
        size_t len = 0 ;
        if(MA_OK == ma_buffer_get_string(buffer, &str, &len)) {
            struct passwd pwd = {0};
            struct passwd* temp_pwd_ptr = NULL;
            char pwdbuffer[256] = {0};
            int  pwdlinelen = sizeof(pwdbuffer)/sizeof(pwdbuffer[0]);
            long uid_s = -1;
            (void)ma_message_auth_info_get_uid(self->peer_auth_info, &uid_s);
            int s = getpwnam_r(str,&pwd, pwdbuffer, pwdlinelen, &temp_pwd_ptr);

			if(NULL == temp_pwd_ptr && 0 == s) { (void)ma_buffer_release(buffer); return ; }
            else if(NULL == temp_pwd_ptr && s != 0) { (void)ma_buffer_release(buffer); return ; }

            if((uid_t)uid_s == pwd.pw_uid) {
                *stop_loop = MA_TRUE;
                self->auth_conditions_result |= MA_AUTH_COND_USER_VALIDATION;
            }
        }
        (void)ma_buffer_release(buffer);
    }
}

static int perform_user_validation(ma_named_pipe_auth_t *self,  ma_uint16_t auth_conditions, ma_msgbus_auth_rule_t *auth_rule) {
    int r = 1;
    if(MA_AUTH_COND_USER_VALIDATION == (MA_AUTH_COND_USER_VALIDATION &  auth_conditions)) {
        ma_array_t *user_list = NULL;
        r = 0;
        if(MA_OK == ma_msgbus_auth_rule_get_users(auth_rule, &user_list)) {
            ma_array_foreach(user_list, user_match_array_cb, (void *)self) ; 
            if((r = ((MA_AUTH_COND_USER_VALIDATION == (MA_AUTH_COND_USER_VALIDATION  & self->auth_conditions_result)) ? 1 : 0)))
                MA_LOG(self->logger, MA_LOG_SEV_TRACE, "user validation rule passed.");
            (void)ma_array_release(user_list);
        }else
            MA_LOG(self->logger, MA_LOG_SEV_WARNING, "get users list from auth rule failed");
    }
    return r;
}

static void group_match_array_cb(size_t index, ma_variant_t *value, void *cb_args, ma_bool_t *stop_loop) {
    ma_named_pipe_auth_t *self = (ma_named_pipe_auth_t *)cb_args ;
    ma_buffer_t *buffer = NULL;

    if(MA_OK == ma_variant_get_string_buffer(value , &buffer)) {
        const char *str = NULL ;
        size_t len = 0 ;
        if(MA_OK == ma_buffer_get_string(buffer, &str, &len)) {
            struct passwd pwd = {0};
            struct passwd* temp_pwd_ptr = NULL;
            char pwdbuffer[256] = {0};
            int  pwdlinelen = sizeof(pwdbuffer)/sizeof(pwdbuffer[0]);
            long gid_s = -1;
            (void)ma_message_auth_info_get_gid(self->peer_auth_info, &gid_s);
            int s = getpwnam_r(str,&pwd, pwdbuffer, pwdlinelen, &temp_pwd_ptr);

            if(NULL == temp_pwd_ptr && 0 == s) { (void)ma_buffer_release(buffer); return ; }
            else if(NULL == temp_pwd_ptr && s != 0) { (void)ma_buffer_release(buffer); return ; }

            if((gid_t)gid_s == pwd.pw_gid) {
                *stop_loop = MA_TRUE;
                self->auth_conditions_result |= MA_AUTH_COND_GROUP_VALIDATION;
            }
        }
        (void)ma_buffer_release(buffer);
    }
}

static int perform_group_validation(ma_named_pipe_auth_t *self,  ma_uint16_t auth_conditions, ma_msgbus_auth_rule_t *auth_rule) {
    int r = 1;
    if(MA_AUTH_COND_GROUP_VALIDATION == (MA_AUTH_COND_GROUP_VALIDATION &  auth_conditions)) {
        ma_array_t *group_list = NULL;
        r = 0;
        if(MA_OK == ma_msgbus_auth_rule_get_groups(auth_rule, &group_list)) {
            ma_array_foreach(group_list, group_match_array_cb, (void *)self) ; 
            if((r = ((MA_AUTH_COND_GROUP_VALIDATION == (MA_AUTH_COND_GROUP_VALIDATION  & self->auth_conditions_result)) ? 1 : 0)))
                MA_LOG(self->logger, MA_LOG_SEV_TRACE, "group validation rule passed.");
            (void)ma_array_release(group_list);
        }else
            MA_LOG(self->logger, MA_LOG_SEV_WARNING, "get users list from auth rule failed");
    }
    return r;
}


static void platform_match_array_cb(size_t index, ma_variant_t *value, void *cb_args, ma_bool_t *stop_loop) {
    ma_named_pipe_auth_t *self = (ma_named_pipe_auth_t *)cb_args ;
    ma_buffer_t *buffer = NULL;

    if(MA_OK == ma_variant_get_string_buffer(value , &buffer)) {
        const char *str = NULL ;
        size_t len = 0 ;
        if(MA_OK == ma_buffer_get_string(buffer, &str, &len)) {
            if(0 == strcmp(MA_PLATFORM_ID, str)) {
                *stop_loop = MA_TRUE;
                self->auth_conditions_result |= MA_AUTH_COND_PLATFORM_VALIDATION;
            }
        }
        (void)ma_buffer_release(buffer);
    }
}

static int perform_platform_validation(ma_named_pipe_auth_t *self, ma_uint16_t auth_conditions, ma_msgbus_auth_rule_t *auth_rule) {
    int r = 1;
    if(MA_AUTH_COND_PLATFORM_VALIDATION == (MA_AUTH_COND_PLATFORM_VALIDATION &  auth_conditions)) {
        ma_array_t *platform_list = NULL;
        r= 0;
        if(MA_OK == ma_msgbus_auth_rule_get_platforms(auth_rule, &platform_list)) {
            ma_array_foreach(platform_list, platform_match_array_cb, (void *)self) ; 
            if((r = ((MA_AUTH_COND_PLATFORM_VALIDATION == (MA_AUTH_COND_PLATFORM_VALIDATION  & self->auth_conditions_result)) ? 1 : 0)))
                MA_LOG(self->logger, MA_LOG_SEV_TRACE, "platform validation rule passed.");
            (void)ma_array_release(platform_list);
        }else
            MA_LOG(self->logger, MA_LOG_SEV_TRACE, "platform list not available in auth rule");
    }
    return r;
}

static int verify_auth_rule(ma_named_pipe_auth_t *self, const char *sha256, const char *auth_rule_str) {
    ma_msgbus_auth_rule_t *auth_rule = NULL;
    int r = 0;
    ma_error_t rc = MA_OK;
    if(MA_OK == (rc = ma_msgbus_auth_rule_create(auth_rule_str, &auth_rule))) {
        ma_uint16_t auth_conditions = MA_AUTH_COND_NONE;

        (void)ma_msgbus_auth_rule_set_logger(auth_rule, self->logger);
        (void)ma_msgbus_auth_rule_get_auth_conditions(auth_rule, &auth_conditions);
        
        /* At least one of the auth validation (AuthType should pass) */
        if( perform_platform_validation(self, auth_conditions, auth_rule) && 
            perform_executable_validation(self, auth_conditions, sha256) &&
            perform_challenge_and_response(self, auth_conditions) && 
            perform_user_validation(self, auth_conditions, auth_rule) &&
            perform_group_validation(self, auth_conditions, auth_rule)) {
                r = 1;
        }else
            MA_LOG(self->logger, MA_LOG_SEV_TRACE, "none of the auth conditions matched");
        (void)ma_msgbus_auth_rule_release(auth_rule);
    }else
      MA_LOG(self->logger, MA_LOG_SEV_ERROR, "failed to parse auth rule, %d", rc);
    return r;
}

static int process_name_or_aliases_matches(ma_named_pipe_auth_t *self, const char *process_name, const char *process_aliases) {
    /*direct match */
    if(0 == strncmp(process_name, self->peer_process_name, MA_MAX_PATH_LEN)) 
        return 1;

    /*alias check */
    if(process_aliases) {
        char *tmp_exp = strdup(process_aliases) ;
        char *token = strtok(tmp_exp, " ");
        while(token) {
            if(0 == strncmp(token, self->peer_process_name, MA_MAX_PATH_LEN)) {
                free(tmp_exp);
                return 1;
            }
            token = strtok(NULL, MA_MSGBUS_AUTH_RULE_VALUE_SEPERATOR_STR) ;
        }
        free(tmp_exp);
    }
    return 0;
}

static int verify_as_per_rule(ma_named_pipe_auth_t *self, const char *manifest) {
    ma_error_t rc = MA_OK;
    int r = 0;
    ma_xml_t *xml = NULL;
    if(MA_OK == (rc = ma_xml_create(&xml))) {
        MA_LOG(self->logger, MA_LOG_SEV_TRACE, "manifest = %s", manifest);
        if(MA_OK == (rc = ma_xml_load_from_buffer(xml, manifest, strlen(manifest)))) {
            ma_xml_node_t *root = NULL;
            if((root = ma_xml_get_node(xml))) {
                ma_xml_node_t *node = NULL;
                if((node = ma_xml_node_search_by_path(root, MA_MSGBUS_AUTH_NODE_XML_PATH_STR))) {
                    do {
                        const char *executable_name = ma_xml_node_attribute_get(node, MA_MSGBUS_KEY_AUTH_SELF_NAME_STR);
                        const char *executable_aliases_name = ma_xml_node_attribute_get(node, MA_MSGBUS_KEY_AUTH_SELF_ALIASES_STR);
                        if(process_name_or_aliases_matches(self, executable_name, executable_aliases_name)) {
                            const char *technology = ma_xml_node_attribute_get(node, MA_MSGBUS_KEY_AUTH_TECHNOLOGY_STR);
                            const char *sha256 = ma_xml_node_attribute_get(node, MA_MSGBUS_KEY_AUTH_SELF_HASH_STR);
                            ma_xml_node_t *auth_rule_node = NULL;

                            MA_LOG(self->logger, MA_LOG_SEV_TRACE, "fetching auth rules for process name %s, technology = %s", executable_name, technology);
                            if((auth_rule_node = ma_xml_node_get_child_by_name(node, MA_MSGBUS_KEY_AUTH_RULE_STR))) {
                                const char *auth_rule = ma_xml_node_get_data(auth_rule_node);   
                                MA_LOG(self->logger, MA_LOG_SEV_TRACE, "checking for auth rule %s", auth_rule);
                                if(verify_auth_rule(self, sha256,auth_rule)) {
                                    r = 1;
                                    break;
                                }else 
                                    MA_LOG(self->logger, MA_LOG_SEV_TRACE, "verify auth rule for %s failed, continuing with next",executable_name);
                            }else
                                MA_LOG(self->logger, MA_LOG_SEV_TRACE, "no auth rule present for %s, continuing with next", executable_name);
                        }
                    }while((node = ma_xml_node_get_next_sibling(node)));
                }else
                    MA_LOG(self->logger, MA_LOG_SEV_ERROR, "no entries found in manifest");
            }else
                MA_LOG(self->logger, MA_LOG_SEV_ERROR, "failed to get root node in manifest");
        }else
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "failed to load manifest xml, %d", rc);
        (void)ma_xml_release(xml);
    }else
        MA_LOG(self->logger, MA_LOG_SEV_ERROR, "failed to create manifest xml, %d", rc);
    return r;
}

static int verify_cms(ma_named_pipe_auth_t *self) {
    int r = 0;
	int is_verified = 0;
	ma_error_t rc = MA_ERROR_MSGBUS_AUTH_FAILED;
	ma_temp_buffer_t manifest = MA_TEMP_BUFFER_INIT, issuer = MA_TEMP_BUFFER_INIT, subject = MA_TEMP_BUFFER_INIT;
    if(MA_OK == (rc = ma_msgbus_crypto_provider_cms_verify(self->crypto_provider, self->peer_sig_fd, &manifest, &issuer, &subject, &is_verified))) {
        if(is_verified) {
			if(verify_as_per_rule(self, (const char *)ma_temp_buffer_get(&manifest))) {
				const char *issuer_name = (const char *)ma_temp_buffer_get(&issuer);
				const char *subject_name = (const char *)ma_temp_buffer_get(&subject);

				(void)ma_message_auth_info_set_attribute(self->peer_auth_info, MA_MSGBUS_AUTH_INFO_ATTRIBUTE_VERIFIED_SIGNER, issuer_name);
				(void)ma_message_auth_info_set_attribute(self->peer_auth_info, MA_MSGBUS_AUTH_INFO_ATTRIBUTE_VERIFIED_PUBLISHER, subject_name);
				MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "authentication passed, signer name: %s subject name: %s", issuer_name ? issuer_name : "unknonwn", subject_name ? subject_name : "unknown");
                r = 1;
            }else
                MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Auth rule verification failed for process %d",self->peer_sig_fd);
		}
	}else 
		MA_LOG(self->logger, MA_LOG_SEV_ERROR, "cms verify failed for sig fd %d, rc %d", self->peer_sig_fd, rc);

	ma_temp_buffer_uninit(&manifest);
	ma_temp_buffer_uninit(&issuer);
	ma_temp_buffer_uninit(&subject);
    //return r;
    return 1; /* nasir intentional */
}

static ma_error_t parse_protocol_params(ma_named_pipe_auth_t *self, char *buffer) {
    ma_error_t rc = MA_ERROR_MSGBUS_AUTH_FAILED;
    char *p = NULL;
    if((p = strtok(buffer, ":"))) {
        if((p = strtok(NULL,":"))) {
            strncpy(self->peer_process_name,p, MA_MAX_PATH_LEN);
            if((p = strtok(NULL,":"))) {
                self->peer_pid = atol(p);
                if((p = strtok(NULL,":"))) {
                    self->peer_uid = atol(p);
                    if((p = strtok(NULL, ":"))) {
                        self->peer_gid = atol(p);
                        if((p = strtok(NULL, ":"))) {
                            self->peer_passphrase_size = atoi(p);
                            rc  = MA_OK;
                            if((p = strtok(NULL, ":"))) {
                                strncpy((char *)self->peer_passphrase, p , 1024);
                                if((p = strtok(NULL, ":"))) {
                                    strncpy(self->peer_product_id, p, 63);
                                }

                            }
                        }
                    }
                }
            }
        }
    }
    return rc;
}



/*get sig path from executable, sig_path should be MA_MAX_PATH_LEN*/
static void get_sig_path_from_executable_path(char *exe_path, char *sig_path){
    char *s1 = strrchr(exe_path, MA_PATH_SEPARATOR);
    if(s1) {
        strncpy(sig_path, exe_path, s1 - exe_path);
        strcat(sig_path, MA_PATH_SEPARATOR_STR);
        strcat(sig_path, MA_MSGBUS_AUTH_SIG_NAME_STR);
    }
}

/*get sig path from executable, executable name should be MA_MAX_PATH_LEN*/
static void get_exe_name_from_executable_path(char *exe_path, char *executabe_name){
    char *s1 = strrchr(exe_path, MA_PATH_SEPARATOR);
    strncpy(executabe_name, s1 ? s1 + 1 : exe_path, MA_MAX_PATH_LEN);
}


static void ma_named_pipe_auth_verify(ma_named_pipe_auth_t *self){	
	/*first verification, are these guys same */
    self->trust_level = TRUST_LEVEL_UNTRUSTED;
    if(MA_OK == verify_are_two_same_people(self)) {
        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "parties involved in auth are matched");
        if(verify_cms(self)) {
            self->trust_level = TRUST_LEVEL_TRUSTED;            
        }
    } else {
        MA_LOG(self->logger, MA_LOG_SEV_ERROR, "exchanged information are not matching, rejecting connection.");		
    }	
}

static ma_error_t deserialize_peer_sig_fd(ma_named_pipe_auth_t *self, char *buffer, size_t size) {
	ma_error_t rc = MA_OK;
	ma_table_t *table = NULL;
	ma_variant_t *var = NULL;
	ma_deserializer_t *deserializer = NULL;

	if(MA_OK == (rc = ma_deserializer_create(&deserializer))){
		if(MA_OK == (rc = ma_deserializer_put(deserializer, buffer, size))){
			if(MA_OK == (rc = ma_deserializer_get_next(deserializer, &var))){				
				if(MA_OK == (rc = ma_variant_get_table(var, &table))){

					const unsigned char *data = NULL;
					size_t len = 0;
					
					if(MA_OK == (rc = get_string(table, MA_AUTH_INFO_KEY_FD_TYPE_STR, &data, &len, MA_FALSE)) && !strncmp((char*)data, MA_PEER_PROCESS_SIG_FD_IDENT_STR, strlen(MA_PEER_PROCESS_SIG_FD_IDENT_STR) ))						
						rc = MA_OK;											
					else
						rc = MA_ERROR_MSGBUS_AUTH_FAILED;

					(void)ma_table_release(table);
				}
				(void)ma_variant_release(var);
			}
		}
		(void)ma_deserializer_destroy(deserializer);
	}
	return rc;
}


static void read_sig_fd(uv_pipe_t* handle, ssize_t nread, uv_buf_t buf, uv_handle_type pending) {
    ma_named_pipe_auth_t *self = (ma_named_pipe_auth_t *)handle->data;
    ma_error_t rc = MA_ERROR_MSGBUS_AUTH_FAILED;

    /*libuv doesnt allows us to pass multi handles passing, so we need to have our own state */
    MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "read_sig_fd callback invoked with handle <%d> and type <%d>.", handle->accepted_fd,pending);

    if(handle->accepted_fd > 0) {
		if(MA_OK == (rc = deserialize_peer_sig_fd(self, buf.base, nread))) {
            MA_LOG(self->logger, MA_LOG_SEV_TRACE, "recieved peer sig fd <%d>,", handle->accepted_fd);
            self->peer_sig_fd = handle->accepted_fd;
            handle->accepted_fd = -1;    
			rc = MA_OK;
        }
        else 
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Recieved unexpected format from peer, rejecting connection.");
    } else 
        MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Recieved unexpected handle from peer, rejecting connection.");    

	self->err_state = rc;
	ma_named_pipe_auth_establish(self);
}

/* read signature fd. */
static ma_error_t ma_named_pipe_auth_read_sig_fd(ma_named_pipe_auth_t *self) {
	self->peer_pipe->data = self;
    if(0 == uv_read2_start((uv_stream_t *)self->peer_pipe, read2_alloc_cb, read_sig_fd)) {
        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "waiting for peer to pass sig info.");
        return MA_OK;
    } else  
        MA_LOG(self->logger, MA_LOG_SEV_ERROR, "waiting for peer information failed, rejecting connection- uv error <%d> .",MA_ERROR_FROM_UV_LAST_ERROR(self->uv_loop));
    return MA_ERROR_MSGBUS_INTERNAL;
}


static ma_error_t deserialize_peer_fd(ma_named_pipe_auth_t *self, char *buffer, size_t size) {

	ma_error_t rc = MA_OK;
	ma_table_t *table = NULL;
	ma_variant_t *var = NULL;
	ma_deserializer_t *deserializer = NULL;

	if(MA_OK == (rc = ma_deserializer_create(&deserializer))){

		if(MA_OK == (rc = ma_deserializer_put(deserializer, buffer, size))){

			if(MA_OK == (rc = ma_deserializer_get_next(deserializer, &var))){				

				if(MA_OK == (rc = ma_variant_get_table(var, &table))){
					const unsigned char *data = NULL;
					size_t len = 0;
					ma_int32_t int32;

					if(MA_OK == (rc = get_string(table, MA_AUTH_INFO_KEY_FD_TYPE_STR, &data, &len, MA_FALSE)) && !strncmp((char*)data, MA_PEER_PROCESS_FD_IDENT_STR, strlen(MA_PEER_PROCESS_FD_IDENT_STR) )){

						if(MA_OK == (rc = get_string(table, MA_AUTH_INFO_KEY_EXE_NAME_STR, &data, &len, MA_FALSE))){

							strncpy(self->peer_process_name, (char*)data, sizeof(self->peer_process_name)-1);

							if(MA_OK == (rc = get_int32(table, MA_AUTH_INFO_KEY_PID_STR, &int32))){
								self->peer_pid = int32;

								if(MA_OK == (rc = get_int32(table, MA_AUTH_INFO_KEY_UID_STR, &int32))){
									self->peer_uid = int32;

									if(MA_OK == (rc = get_int32(table, MA_AUTH_INFO_KEY_GID_STR, &int32))){
										self->peer_gid = int32;

										if(MA_OK == (rc = get_string(table, MA_AUTH_INFO_KEY_PRODUCT_ID_STR, &data, &len, MA_FALSE))){
										
											strncpy(self->peer_product_id, (char*)data, sizeof(self->peer_product_id) - 1);

											if(MA_OK == (rc = get_string(table, MA_AUTH_INFO_KEY_PASSPHRASE_STR, &data, &len, MA_TRUE))){										

												if(len < sizeof(self->peer_passphrase)) {
													memcpy(self->peer_passphrase, data, len);
													self->peer_passphrase_size = len;
													rc = MA_OK;
												}
												else
													rc = MA_ERROR_MSGBUS_AUTH_FAILED;
											}
										}
									}
								}
							}
						}	
					}
					else
						rc = MA_ERROR_MSGBUS_AUTH_FAILED;

					(void)ma_table_release(table);
				}
				(void)ma_variant_release(var);
			}
		}
		(void)ma_deserializer_destroy(deserializer);
	}
	return rc;
}

static void read_peer_fd(uv_pipe_t* handle, ssize_t nread, uv_buf_t buf, uv_handle_type pending) {
    ma_named_pipe_auth_t *self = (ma_named_pipe_auth_t *)handle->data;
    ma_error_t rc = MA_ERROR_MSGBUS_AUTH_FAILED;

    /*libuv doesnt allows us to pass multi handles passing, so we need to have our own state */
    MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "read_peer_fd callback invoked with handle <%d> and type <%d>.", handle->accepted_fd,pending);

    if(handle->accepted_fd > 0) {
        MA_LOG(self->logger, MA_LOG_SEV_TRACE, "recieved peer process fd <%d>,", handle->accepted_fd);
		if(MA_OK == (rc = deserialize_peer_fd(self, buf.base, nread))) {
            MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "information recived from peer %s %d %d %d", self->peer_process_name, self->peer_pid, self->peer_uid, self->peer_gid);
            self->peer_process_fd = handle->accepted_fd;
            handle->accepted_fd = -1;
			rc = MA_OK;                
        }else 
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Recieved unexpected protocol from peer, rejecting connection rc <%d>.", rc);

    } else 
        MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Recieved unexpected handle from peer, rejecting connection.");    

	self->err_state = rc;
	ma_named_pipe_auth_establish(self);
}

/* read the peer fd */
static ma_error_t ma_named_pipe_auth_read_peer_fd(ma_named_pipe_auth_t *self) {
	self->peer_pipe->data = self;
    if(0 == uv_read2_start((uv_stream_t *)self->peer_pipe, read2_alloc_cb, read_peer_fd)) {
        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "waiting for peer to pass process info.");
        return MA_OK;
    } else  
        MA_LOG(self->logger, MA_LOG_SEV_ERROR, "waiting for peer information failed, rejecting connection- uv error <%d> .",MA_ERROR_FROM_UV_LAST_ERROR(self->uv_loop));
    return MA_ERROR_MSGBUS_INTERNAL;
}

/* sig fd written */
static void write2_sig_fd_cb(uv_write_t* req, int status) {
    ma_named_pipe_auth_t *self = (ma_named_pipe_auth_t *)req->data;
    ma_error_t rc = MA_ERROR_MSGBUS_AUTH_FAILED;
    uv_fs_t fs_req;

    MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "write2_sig_cb callback invocked with status <%d>.", status);

    (void)uv_close((uv_handle_t *)self->descriptor_exchange.pipe, close_cb), self->descriptor_exchange.pipe = NULL;
    (void)uv_fs_close(self->uv_loop,&fs_req, self->descriptor_exchange.file,NULL), self->descriptor_exchange.file = -1;
    uv_fs_req_cleanup(&fs_req); 

    if(status == 0) {        
		self->err_state = MA_OK;
    } else {
		self->err_state = MA_ERROR_MSGBUS_AUTH_FAILED;
        MA_LOG(self->logger, MA_LOG_SEV_ERROR, "failed to exchange information, rejecting connection- uv error <%d> .",MA_ERROR_FROM_UV_LAST_ERROR(self->uv_loop));
	}
    ma_named_pipe_auth_establish(self);
}

static ma_error_t serialize_peer_sig_fd(ma_named_pipe_auth_t *self, char *buffer, size_t *size) {

	ma_error_t rc = MA_OK;
	ma_table_t *table = NULL;
	ma_variant_t *var = NULL;

	if(MA_OK == (rc = ma_table_create(&table))){
		if(MA_OK == (rc = ma_variant_create_from_table(table, &var))){
			ma_serializer_t *serializer = NULL;
			ma_variant_t *value = NULL;

			(void)ma_variant_create_from_string(MA_PEER_PROCESS_SIG_FD_IDENT_STR, strlen(MA_PEER_PROCESS_SIG_FD_IDENT_STR), &value);
			(void)ma_table_add_entry(table, MA_AUTH_INFO_KEY_FD_TYPE_STR, value);
			(void)ma_variant_release(value); value = NULL;
			
			if(MA_OK == (rc = ma_serializer_create(&serializer))){
				if(MA_OK == (rc = ma_serializer_add_variant(serializer, var))){
					char *data = NULL;
					size_t len = 0;

					if(MA_OK == (rc = ma_serializer_get(serializer, &data, &len))){
						if(len > *size) 
							rc = MA_ERROR_INSUFFICIENT_SIZE;
						else {
							memcpy(buffer, data, len);
							*size = len;							
						}
					}
				}
				(void)ma_serializer_destroy(serializer);
			}
			(void)ma_variant_release(var);
		}	
		(void)ma_table_release(table);
	}
	return rc;
}

static ma_error_t ma_named_pipe_auth_send_sig_fd(ma_named_pipe_auth_t *self){
    /* send the sig file now */
    int r = 0;
	uv_fs_t fs_req;
    ma_error_t rc = MA_OK;
    ma_temp_buffer_t buffer  = MA_TEMP_BUFFER_INIT;
    if(MA_OK == (rc = ma_pid_to_path(getpid(), &buffer))) {
        char sig_path[MA_MAX_PATH_LEN] = {0};
        char *exe_path = (char*)ma_temp_buffer_get(&buffer);
        get_sig_path_from_executable_path(exe_path, sig_path);
        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "executable %s presenting cert %s for authentication.", exe_path , sig_path);
        if(-1 != (r = uv_fs_open(self->uv_loop, &fs_req, sig_path, O_RDONLY, S_IREAD, NULL))) {
            uv_buf_t    uv_buf;
            size_t		size = sizeof(self->buf);
            uv_fs_req_cleanup(&fs_req); 
            self->descriptor_exchange.file = r;
            self->descriptor_exchange.uv_write_req.data = self;

			if(MA_OK == (rc = serialize_peer_sig_fd(self, self->buf, &size))){
				uv_buf = uv_buf_init(self->buf, size);
				/* wrap it around pipe , libuv limitation as of now */
				if(NULL != (self->descriptor_exchange.pipe = (uv_pipe_t *)calloc(1, sizeof(uv_pipe_t)))) {
					if(0 == (r = uv_pipe_init(self->uv_loop, self->descriptor_exchange.pipe,0))) {
						if(0 == (r = uv_pipe_open(self->descriptor_exchange.pipe, self->descriptor_exchange.file))) {
							if(0 == (r = uv_write2(&self->descriptor_exchange.uv_write_req, (uv_stream_t *)self->peer_pipe, &uv_buf, 1, (uv_stream_t *)self->descriptor_exchange.pipe, write2_sig_fd_cb))) {
								(void)ma_temp_buffer_uninit(&buffer);
								return MA_OK;
							}
							(void)uv_close((uv_handle_t *)self->descriptor_exchange.pipe,close_cb),self->descriptor_exchange.pipe = NULL;
						}
					}
				}
				(void)uv_fs_close(self->uv_loop,&fs_req, self->descriptor_exchange.file, NULL), self->descriptor_exchange.file = -1;
				uv_fs_req_cleanup(&fs_req); 
			}   
			else
				MA_LOG(self->logger, MA_LOG_SEV_ERROR, "failed to serialize peer sig fd rc <%d>", rc);
        }else
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "failed to open executable %s", exe_path);
        (void)ma_temp_buffer_uninit(&buffer);
    }else
        MA_LOG(self->logger, MA_LOG_SEV_ERROR, "failed to get executable path %d", rc);
	return MA_ERROR_MSGBUS_INTERNAL;
}


/* now the process fd is written */
static void write2_peer_fd_cb(uv_write_t* req, int status) {
    ma_named_pipe_auth_t *self = (ma_named_pipe_auth_t *)req->data;
    uv_fs_t fs_req;
    MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "write2_process_fd_cb callback invocked with status <%d>.", status);

    (void)uv_close((uv_handle_t *)self->descriptor_exchange.pipe, close_cb), self->descriptor_exchange.pipe = NULL;
    (void)uv_fs_close(self->uv_loop,&fs_req, self->descriptor_exchange.file,NULL), self->descriptor_exchange.file = -1;
    uv_fs_req_cleanup(&fs_req); 

    if(status == 0) {
		self->err_state = MA_OK;
	}
	else{ 
		MA_LOG(self->logger, MA_LOG_SEV_ERROR, "failed to exchange information, rejecting connection- uv error <%d> .",MA_ERROR_FROM_UV_LAST_ERROR(self->uv_loop));
		self->err_state = MA_ERROR_MSGBUS_AUTH_FAILED;
	}    
	ma_named_pipe_auth_establish(self);
}

static ma_error_t serialize_peer_fd(ma_named_pipe_auth_t *self, char *exe_name, 
	int pid, int uid, int gid, unsigned char *passphrase, size_t passphrase_s, char *product_id, char *buffer, size_t *size) {

	ma_error_t rc = MA_OK;
	ma_table_t *table = NULL;
	ma_variant_t *var = NULL;

	if(MA_OK == (rc = ma_table_create(&table))){

		if(MA_OK == (rc = ma_variant_create_from_table(table, &var))){

			ma_serializer_t *serializer = NULL;
			ma_variant_t *value = NULL;

			(void)ma_variant_create_from_string(MA_PEER_PROCESS_FD_IDENT_STR, strlen(MA_PEER_PROCESS_FD_IDENT_STR), &value);
			(void)ma_table_add_entry(table, MA_AUTH_INFO_KEY_FD_TYPE_STR, value);
			(void)ma_variant_release(value); value = NULL;

			(void)ma_variant_create_from_string(exe_name, strlen(exe_name), &value);
			(void)ma_table_add_entry(table, MA_AUTH_INFO_KEY_EXE_NAME_STR, value);
			(void)ma_variant_release(value); value = NULL;

			(void)ma_variant_create_from_int32(pid, &value);
			(void)ma_table_add_entry(table, MA_AUTH_INFO_KEY_PID_STR, value);
			(void)ma_variant_release(value); value = NULL;

			(void)ma_variant_create_from_int32(uid, &value);
			(void)ma_table_add_entry(table, MA_AUTH_INFO_KEY_UID_STR, value);
			(void)ma_variant_release(value); value = NULL;


			(void)ma_variant_create_from_int32(gid, &value);
			(void)ma_table_add_entry(table, MA_AUTH_INFO_KEY_GID_STR, value);
			(void)ma_variant_release(value); value = NULL;

			(void)ma_variant_create_from_raw(passphrase, passphrase_s, &value);
			(void)ma_table_add_entry(table, MA_AUTH_INFO_KEY_PASSPHRASE_STR, value);
			(void)ma_variant_release(value); value = NULL;
						
			(void)ma_variant_create_from_string(product_id, strlen(product_id), &value);
			(void)ma_table_add_entry(table, MA_AUTH_INFO_KEY_PRODUCT_ID_STR, value);
			(void)ma_variant_release(value); value = NULL;

			if(MA_OK == (rc = ma_serializer_create(&serializer))){

				if(MA_OK == (rc = ma_serializer_add_variant(serializer, var))){
					char *data = NULL;
					size_t len = 0;

					if(MA_OK == (rc = ma_serializer_get(serializer, &data, &len))){
						if(len > *size) 
							rc = MA_ERROR_INSUFFICIENT_SIZE;
						else {
							memcpy(buffer, data, len);
							*size = len;							
						}
					}
				}
				(void)ma_serializer_destroy(serializer);
			}

			(void)ma_variant_release(var);
		}	
		ma_table_release(table);
	}
	return rc;
}




/* send the peer fd. */
static ma_error_t ma_named_pipe_auth_send_peer_fd(ma_named_pipe_auth_t *self) {
    int r = 0;
    unsigned char passphrase[1024] = {0};
    char product_id[64] = {0};
    size_t passphrase_size = 0;
    ma_error_t rc = MA_OK;
    ma_temp_buffer_t buffer = MA_TEMP_BUFFER_INIT;

    if(self->passphrase_handler) {
        passphrase_size = sizeof(passphrase);
        if(MA_OK != (rc = ma_msgbus_passphrase_handler_acquire_passphrase(self->passphrase_handler, product_id, passphrase, &passphrase_size))) {
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "couldn't acquire passphrase, returning %d", rc);
            return rc;
        }
        if(passphrase_size > 1024) {
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "acquired passphrase size is greater than 1024");
            return MA_ERROR_MSGBUS_INTERNAL;
        }
    }else
        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "passphrase callback is not provided, so no challenge and response");


    /*send exe path first */
    if(MA_OK == (rc = ma_pid_to_path(getpid(), &buffer))) {
        uv_fs_t fs_req;
        char *exe_path = (char *)ma_temp_buffer_get(&buffer);
        if(-1 != (r = uv_fs_open(self->uv_loop, &fs_req, exe_path, O_RDONLY, S_IREAD, NULL))) {
            uv_buf_t    uv_buf;            
            char        exe_name[MA_MAX_PATH_LEN] = {0};
			size_t		len = sizeof(self->buf);

            get_exe_name_from_executable_path(exe_path, exe_name);
            uv_fs_req_cleanup(&fs_req); 
            self->descriptor_exchange.file = r;
            self->descriptor_exchange.uv_write_req.data = self;

            if(MA_OK == (rc = serialize_peer_fd(self, exe_name,getpid(), geteuid(), getegid(),passphrase, passphrase_size, product_id, self->buf, &len))){

				uv_buf = uv_buf_init(self->buf, len);
				/* wrap it around pipe , libuv limitation as of now */
				if((self->descriptor_exchange.pipe = (uv_pipe_t *)calloc(1, sizeof(uv_pipe_t)))) {
					if(0 == (r = uv_pipe_init(self->uv_loop, self->descriptor_exchange.pipe,0))) {
						if(0 == (r = uv_pipe_open(self->descriptor_exchange.pipe, self->descriptor_exchange.file))) {
							if(0 == (r = uv_write2(&self->descriptor_exchange.uv_write_req, (uv_stream_t *)self->peer_pipe, &uv_buf, 1, (uv_stream_t *)self->descriptor_exchange.pipe, write2_peer_fd_cb))) {
								MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "started exchanging information between peers ");
								(void)ma_temp_buffer_uninit(&buffer);
								return MA_OK;
							}
							(void)uv_close((uv_handle_t *)self->descriptor_exchange.pipe,close_cb), self->descriptor_exchange.pipe = NULL;
						}
					}
				}
				(void)uv_fs_close(self->uv_loop,&fs_req, self->descriptor_exchange.file,NULL), self->descriptor_exchange.file = -1;
				uv_fs_req_cleanup(&fs_req); 
			}
			else
				MA_LOG(self->logger, MA_LOG_SEV_ERROR, "failed to serialize_peer_fd <%d>", rc);
        }else 
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "failed to open executable <%s>", exe_path);
        (void)ma_temp_buffer_uninit(&buffer);
    }else
        MA_LOG(self->logger, MA_LOG_SEV_ERROR, "failed to get executable path");
    MA_LOG(self->logger, MA_LOG_SEV_ERROR, "failed to exchange information, rejecting connection- uv error <%d> .",MA_ERROR_FROM_UV_LAST_ERROR(self->uv_loop));
    return MA_ERROR_MSGBUS_INTERNAL;
}

void ma_named_pipe_auth_establish(ma_named_pipe_auth_t *self){		
	if(self->err_state == MA_OK){

		if(MA_AUTH_INFO_TYPE_CORE & self->peer_auth_negotiated){
			
			MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "starting msgbus core authentication.");

			if(self->is_server){
				self->server_state = (ma_server_auth_state_t) ((int)self->server_state + 1);
				switch(self->server_state){
						case MA_SERVER_AUTH_STATE_READ_PEER_FD:
							self->err_state = ma_named_pipe_auth_read_peer_fd(self);
							break;
						case MA_SERVER_AUTH_STATE_SEND_PEER_FD:
							self->err_state = ma_named_pipe_auth_send_peer_fd(self);
							break;
						case MA_SERVER_AUTH_STATE_READ_SIG_FD:
							self->err_state = ma_named_pipe_auth_read_sig_fd(self);
							break;
						case MA_SERVER_AUTH_STATE_SEND_SIG_FD:
							self->err_state = ma_named_pipe_auth_send_sig_fd(self);
							break;
						case MA_SERVER_AUTH_STATE_VERIFY:
							ma_named_pipe_auth_verify(self);
							call_user_callback(self, (self->trust_level == TRUST_LEVEL_TRUSTED ? MA_OK : MA_ERROR_MSGBUS_AUTH_FAILED) );
							return;
				}
			}
			else{
				self->client_state = (ma_client_auth_state_t) ((int)(self->client_state) + 1);
				switch(self->client_state){
					case MA_CLIENT_AUTH_STATE_SEND_PEER_FD:
					self->err_state = ma_named_pipe_auth_send_peer_fd(self);
						break;
					case MA_CLIENT_AUTH_STATE_READ_PEER_FD:
						self->err_state = ma_named_pipe_auth_read_peer_fd(self);
						break;
					case MA_CLIENT_AUTH_STATE_SEND_SIG_FD:
						self->err_state = ma_named_pipe_auth_send_sig_fd(self);
						break;
					case MA_CLIENT_AUTH_STATE_READ_SIG_FD:
						self->err_state = ma_named_pipe_auth_read_sig_fd(self);
						break;				
					case MA_CLIENT_AUTH_STATE_VERIFY:
						ma_named_pipe_auth_verify(self);
						call_user_callback(self, (self->trust_level == TRUST_LEVEL_TRUSTED ? MA_OK : MA_ERROR_MSGBUS_AUTH_FAILED) );
						return;
				}
			}
		}
		/*add any further authentication mechanism which we want to add it in the future.*/
		else{
			MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "unsupported peer authentication, negotiated authentication types <0x%08x>.", self->peer_auth_negotiated);
			self->err_state = MA_ERROR_MSGBUS_AUTH_FAILED;
		}
	}

	if(MA_OK != self->err_state)
		call_user_callback(self, self->err_state);	
}


/* deserialize_auth_info*/
static ma_error_t deserialize_auth_info(ma_named_pipe_auth_t *self, char *buffer, size_t size) {
	ma_error_t rc = MA_OK;
	ma_table_t *table = NULL;
	ma_variant_t *var = NULL;
	ma_deserializer_t *deserializer = NULL;

	if(MA_OK == (rc = ma_deserializer_create(&deserializer))){
		if(MA_OK == (rc = ma_deserializer_put(deserializer, buffer, size))){
			if(MA_OK == (rc = ma_deserializer_get_next(deserializer, &var))){				
				if(MA_OK == (rc = ma_variant_get_table(var, &table))){										

					rc = get_int32(table, MA_AUTH_INFO_KEY_TYPES_STR, &self->peer_auth_negotiated);						
					(void)ma_table_release(table);
				}
				(void)ma_variant_release(var);
			}
		}
		(void)ma_deserializer_destroy(deserializer);
	}
	return rc;
}


static void read_auth_info_cb(uv_stream_t* handle, ssize_t nread, uv_buf_t buf) {
    ma_named_pipe_auth_t *self = (ma_named_pipe_auth_t *)handle->data;
    ma_error_t rc = MA_ERROR_MSGBUS_AUTH_FAILED;

    MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "read_auth_info_cb nread <%d>.", nread);

	if(MA_OK == (rc = deserialize_auth_info(self, buf.base, nread))) {
		if(self->is_server){
			MA_LOG(self->logger, MA_LOG_SEV_TRACE, "peer authentication types received <0x%08x>,", self->peer_auth_negotiated);        
			/*server agreed auth types: as per its support.*/
			self->peer_auth_negotiated = self->peer_auth_negotiated & MA_AUTH_INFO_TYPE_SUPPORTED;

			MA_LOG(self->logger, MA_LOG_SEV_TRACE, "peer authentication types negotiated <0x%08x>,", self->peer_auth_negotiated);        			
		}
		else
			MA_LOG(self->logger, MA_LOG_SEV_TRACE, "peer authentication types negotiated <0x%08x>,", self->peer_auth_negotiated);        					

		rc = MA_OK;
    }
    else 
        MA_LOG(self->logger, MA_LOG_SEV_ERROR, "read_auth_info_cb recieved unexpected format from peer, rejecting connection.");

	self->err_state = rc;
	ma_named_pipe_auth_negotiate(self);
}

/* read auth_info. */
static ma_error_t ma_named_pipe_auth_read_auth_info(ma_named_pipe_auth_t *self) {
	self->peer_pipe->data = self;
    if(0 == uv_read_start((uv_stream_t *)self->peer_pipe, read2_alloc_cb, read_auth_info_cb)) {
        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "waiting for peer to write authentication types (supported/negotiated).");
        return MA_OK;
    } else  
        MA_LOG(self->logger, MA_LOG_SEV_ERROR, "waiting for peer authentication types (supported/negotiated) failed, rejecting connection- uv error <%d> .",MA_ERROR_FROM_UV_LAST_ERROR(self->uv_loop));
    return MA_ERROR_MSGBUS_INTERNAL;
}

/* auth_info_write_cb written */
static void auth_info_write_cb(uv_write_t* req, int status) {
    ma_named_pipe_auth_t *self = (ma_named_pipe_auth_t *)req->data;
    ma_error_t rc = MA_ERROR_MSGBUS_AUTH_FAILED;
    MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "auth_info_write_cb callback invoked with status <%d>.", status);
	    
    if(status == 0) {        
		self->err_state = MA_OK;
    } else {
		self->err_state = MA_ERROR_MSGBUS_AUTH_FAILED;
        MA_LOG(self->logger, MA_LOG_SEV_ERROR, "failed to exchange auth info, rejecting connection- uv error <%d> .",MA_ERROR_FROM_UV_LAST_ERROR(self->uv_loop));
	}
    ma_named_pipe_auth_negotiate(self);
}

static ma_error_t serialize_auth_info(ma_named_pipe_auth_t *self, char *buffer, size_t *size) {

	ma_error_t rc = MA_OK;
	ma_table_t *table = NULL;
	ma_variant_t *var = NULL;

	if(MA_OK == (rc = ma_table_create(&table))){
		if(MA_OK == (rc = ma_variant_create_from_table(table, &var))){
			ma_serializer_t *serializer = NULL;
			ma_variant_t *value = NULL;
			ma_int32_t auth_negotiated = (self->is_server ? self->peer_auth_negotiated : MA_AUTH_INFO_TYPE_SUPPORTED);

			(void)ma_variant_create_from_int32(auth_negotiated, &value);
			(void)ma_table_add_entry(table, MA_AUTH_INFO_KEY_TYPES_STR, value);
			(void)ma_variant_release(value); value = NULL;
			
			if(MA_OK == (rc = ma_serializer_create(&serializer))){
				if(MA_OK == (rc = ma_serializer_add_variant(serializer, var))){
					char *data = NULL;
					size_t len = 0;

					if(MA_OK == (rc = ma_serializer_get(serializer, &data, &len))){
						if(len > *size) 
							rc = MA_ERROR_INSUFFICIENT_SIZE;
						else {
							memcpy(buffer, data, len);
							*size = len;							
						}
					}
				}
				(void)ma_serializer_destroy(serializer);
			}
			(void)ma_variant_release(var);
		}	
		(void)ma_table_release(table);
	}
	return rc;
}

static ma_error_t ma_named_pipe_auth_send_auth_info(ma_named_pipe_auth_t *self){
    int r = 0;
	ma_error_t rc = MA_OK;
	size_t		size = sizeof(self->buf);
    if(MA_OK == (rc = serialize_auth_info(self, self->buf, &size))){
		uv_buf_t uv_buf = uv_buf_init(self->buf, size);
		self->descriptor_exchange.uv_write_req.data = self;
		if(0 == (r = uv_write(&self->descriptor_exchange.uv_write_req, (uv_stream_t *)self->peer_pipe, &uv_buf, 1, auth_info_write_cb))) 
			return MA_OK;				
		else
			MA_LOG(self->logger, MA_LOG_SEV_ERROR, "failed to write authentication types, <%d>", r);   
	}   
	else
		MA_LOG(self->logger, MA_LOG_SEV_ERROR, "failed to serialize authentication types, rc <%d>", rc);   
	return MA_ERROR_MSGBUS_INTERNAL;
}

static void ma_named_pipe_auth_negotiate(ma_named_pipe_auth_t *self){

	if(MA_OK == self->err_state) {
		if(self->is_server) {		
			if(0 == self->auth_negotiate_state)
				self->err_state = ma_named_pipe_auth_read_auth_info(self);						
			else if(1 == self->auth_negotiate_state)
				self->err_state = ma_named_pipe_auth_send_auth_info(self);	
			else{
				ma_named_pipe_auth_establish(self);
				return;
			}
		}
		else {
			if(0 == self->auth_negotiate_state)
				self->err_state = ma_named_pipe_auth_send_auth_info(self);						
			else if(1 == self->auth_negotiate_state)
				self->err_state = ma_named_pipe_auth_read_auth_info(self);	
			else{
				ma_named_pipe_auth_establish(self);
				return;
			}
		}
		self->auth_negotiate_state++;
	}
	
	if(MA_OK != self->err_state)
		call_user_callback(self, self->err_state);	
}

static ma_error_t get_string(ma_table_t *tab, const char *key, const unsigned char **buffer, size_t *size, ma_bool_t is_raw){
	ma_variant_t *var = NULL;
	ma_error_t rc = MA_OK;

	if(MA_OK == (rc = ma_table_get_value(tab, key, &var))){
		ma_buffer_t *buff = NULL;

		if(MA_OK == (rc = ( is_raw ? ma_variant_get_raw_buffer(var, &buff) : ma_variant_get_string_buffer(var, &buff) ) )){			
			rc  = (is_raw ? ma_buffer_get_raw(buff, buffer, size) : ma_buffer_get_string(buff, (const char**)buffer, size) );
			(void)ma_buffer_release(buff);
		}
		(void)ma_variant_release(var);
	}
	return rc;
}

static ma_error_t get_int32(ma_table_t *tab, const char *key, ma_int32_t *data){
	ma_variant_t *var = NULL;
	ma_error_t rc = MA_OK;

	if(MA_OK == (rc = ma_table_get_value(tab, key, &var))){		
		rc = ma_variant_get_int32(var, data);
		(void)ma_variant_release(var);
	}
	return rc;
}
