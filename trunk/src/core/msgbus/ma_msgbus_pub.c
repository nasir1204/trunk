#include "ma/ma_msgbus.h"
#include "ma/internal/core/msgbus/ma_message_private.h"
#include "ma/internal/core/msgbus/ma_msgbus_private.h"
#include "ma/internal/utils/event_loop/ma_loop_worker.h"
#include "ma/internal/core/msgbus/ma_message_header.h"
#include "ma/internal/core/msgbus/ma_message_consumer.h"
#include "ma/internal/core/msgbus/ma_named_pipe_connection_manager.h"
#include "ma/internal/core/msgbus/ma_ep_lookup.h"
#include "ma/internal/defs/ma_pubsub_service_defs.h"

#include <string.h>
#include <stdlib.h>

#ifndef MA_WINDOWS
    #include <unistd.h>
#endif

typedef struct ma_msgbus_publisher_s {
	/*! Publisher reachability */
	ma_msgbus_consumer_reachability_t	publisher_reachability ;

	/*! topic to be published */
	char								*topic ;

	/*! message to be passed with publishing */
	ma_message_t						*payload ;

	/*! bus pointer */
	ma_msgbus_t							*mbus ; 
}ma_msgbus_publisher_t ;


static void inner_publish_message(void *data)  ;

ma_error_t ma_msgbus_publish(ma_msgbus_t *mbus, const char *topic, ma_msgbus_consumer_reachability_t pub_reachability,  ma_message_t *payload) {
	if(mbus && topic && payload) {
		ma_msgbus_publisher_t *self = (ma_msgbus_publisher_t *)calloc(1  , sizeof(ma_msgbus_publisher_t));
		if(!self){
            MA_LOG(ma_msgbus_get_logger(mbus), MA_LOG_SEV_CRITICAL, "Out of Memory");
			return MA_ERROR_OUTOFMEMORY ;
        }
		else {
			self->mbus  = mbus;		
			//TODO- check on the topic validity
			self->topic = strdup(topic);
			
			self->publisher_reachability = pub_reachability;

			ma_message_add_ref(payload) ;
			self->payload = payload ;
            MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "topic <%s> reachability <%d>", topic, pub_reachability);
		}

		return ma_loop_worker_submit_work(ma_msgbus_get_loop_worker(self->mbus), &inner_publish_message, self, MA_FALSE);		
	}
	return MA_ERROR_INVALIDARG ;
}


static void cleanup_self(ma_msgbus_publisher_t *self) ;

static void lookup_cb(ma_error_t  status, ma_message_consumer_t *ep_consumer, void *ep_cookie, void *cb_data) {	
	ma_msgbus_publisher_t *self = (ma_msgbus_publisher_t *)cb_data;	
	
	if(status == MA_OK) {
		MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "look up callback is Invoked with status <%d>, Sending msg to broker", status);
		ma_message_consumer_send(ep_consumer, self->payload);	
	}
	
	if(ep_consumer) {
		if(ep_cookie) 
			ma_ep_lookup_stop((ma_ep_lookup_t *)ep_consumer, ep_cookie); 
		ma_message_consumer_release(ep_consumer); 
	}
	
	cleanup_self(self);	
}
	
static void inner_publish_message(void *data) {
	ma_msgbus_publisher_t *self = (ma_msgbus_publisher_t *)data ;
	ma_error_t rc = MA_OK ;	

	// settting headers to the requested message..
	SET_MESSAGE_TYPE(self->payload, MA_MESSAGE_TYPE_PUBLISH);	
	ma_message_set_property(self->payload , MA_MSG_PUBLISH_TOPIC , self->topic);
    
	/* If Published message from broker process itself */
	if(ma_msgbus_get_broker_pid(self->mbus) == getpid()) {
		if(MSGBUS_CONSUMER_REACH_INPROC == self->publisher_reachability) {
			SET_MESSAGE_DESTINATION_PID(self->payload, 0); // sets pid as zero to publish only to the inproc services
		}
		else {
			SET_MESSAGE_DESTINATION_NAME(self->payload, MA_MSGBUS_PUBSUB_SERVICE_NAME_STR);
			SET_MESSAGE_DESTINATION_PID(self->payload, ma_msgbus_get_broker_pid(self->mbus)); // Sets pid as broker pid to send all the consumers of broker */		
		}
		/* Send it locally */
		ma_message_consumer_send((ma_message_consumer_t *)ma_msgbus_get_message_router(self->mbus), self->payload) ;
		cleanup_self(self);
	}
	else { /* If Published message from other processes */	
		SET_MESSAGE_DESTINATION_PID(self->payload, 0); // sets pid as zero to publish only to the inproc services

		ma_message_consumer_send((ma_message_consumer_t *)ma_msgbus_get_message_router(self->mbus), self->payload) ;
	
		/* Will set the up the connection and send it to the broker */
		if(MSGBUS_CONSUMER_REACH_INPROC != self->publisher_reachability) {
			MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "Publishing message to Broker");

			SET_MESSAGE_DESTINATION_NAME(self->payload, MA_MSGBUS_PUBSUB_SERVICE_NAME_STR);
			SET_MESSAGE_DESTINATION_PID(self->payload, ma_msgbus_get_broker_pid(self->mbus)); // Sets pid as broker pid to send the message to the broker
			rc = ma_ep_lookup_start(self->mbus, MA_MSGBUS_PUBSUB_SERVICE_NAME_STR, self->publisher_reachability, lookup_cb, self);
			
			if(MA_OK != rc) 
				cleanup_self(self);
			        
		}
		else {//We are done 
			cleanup_self(self);
		}
	}
}

static void cleanup_self(ma_msgbus_publisher_t *self)  {
	if(self) {
		MA_LOG(ma_msgbus_get_logger(self->mbus), MA_LOG_SEV_DEBUG, "Clean up publisher data");
		if(self->topic) free(self->topic) ;
		/*Remove the dec ref of the payload*/
		if(self->payload) ma_message_release(self->payload);	
		free(self); self = NULL;
	}
}
