#include "ma/internal/core/msgbus/ma_message_auth_info_private.h"
#include "ma/internal/utils/threading/ma_atomic.h"


#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

struct ma_message_auth_info_s {

    ma_atomic_counter_t             ref_count;

    char                            *issuer_cn;

    char                            *subject_cn;

    void                            *psid;

    long                            pid;

    long                            uid;

    long                            gid;

    ma_bool_t                       is_privileged;
};

static void ma_msgbus_auth_info_destroy(ma_message_auth_info_t *self) {
    free(self->issuer_cn);
    free(self->subject_cn);
    free(self->psid);
    free(self);
}

ma_error_t ma_message_auth_info_create(ma_message_auth_info_t **auth_info) {
    ma_message_auth_info_t *self = calloc(1,sizeof(ma_message_auth_info_t));
    if (self) {

        self->ref_count = 1;
        *auth_info = self;
        return MA_OK;
    }

    return MA_ERROR_OUTOFMEMORY;
}


ma_error_t ma_message_auth_info_add_ref(ma_message_auth_info_t *self) {
    if (self) {
        MA_ATOMIC_INCREMENT(self->ref_count);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_message_auth_info_release(ma_message_auth_info_t *self) {
    if (self) {
        if (0 == MA_ATOMIC_DECREMENT(self->ref_count)) {
            ma_msgbus_auth_info_destroy(self);
        }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;

}

MA_MSGBUS_API  ma_error_t ma_message_auth_info_vgetopt(const ma_message_auth_info_t *self, ma_message_auth_info_attribute_t option, va_list ap) {
    ma_error_t rc = MA_OK;	
    switch(option) {
        case MA_MSGBUS_AUTH_INFO_ATTRIBUTE_VERIFIED_SIGNER:	{        
            char const **p = va_arg(ap, const char **);
            if(p) {
                if(!self->issuer_cn) rc = MA_ERROR_MESSAGE_AUTH_INFO_NOT_FOUND;
                else *p = self->issuer_cn;
            }
            else  rc = MA_ERROR_INVALIDARG;		 
            break;	
        }
        case MA_MSGBUS_AUTH_INFO_ATTRIBUTE_VERIFIED_PUBLISHER:	{
            char const **p = va_arg(ap, const char **);
            if(p) {
                if(!self->subject_cn) rc  = MA_ERROR_MESSAGE_AUTH_INFO_NOT_FOUND;
                else  *p = self->subject_cn;
            }
            else  rc = MA_ERROR_INVALIDARG;		 
            break;	
        }        
        case MA_MSGBUS_AUTH_INFO_ATTRIBUTE_USER_SID: {
            #if defined(MA_WINDOWS)
                void const **p = va_arg(ap, const void **);
                if(p) {
                    if(!self->psid) rc = MA_ERROR_MESSAGE_AUTH_INFO_NOT_FOUND;
                    else  *p = self->psid;
                }
                else  rc = MA_ERROR_INVALIDARG;		 
            #else
                rc = MA_ERROR_INVALIDARG;
            #endif
            break;	
        }
        case MA_MSGBUS_AUTH_INFO_ATTRIBUTE_PID:	{
            long *p = va_arg(ap, long *);
            if(p) *p = self->pid;
            else  rc = MA_ERROR_INVALIDARG;		 
            break;	
        }
        case MA_MSGBUS_AUTH_INFO_ATTRIBUTE_UID:	{
            #if    !defined(MA_WINDOWS)
                long *p = va_arg(ap, long *);
                if(p) *p = self->uid;
                else  rc = MA_ERROR_INVALIDARG;		 
            #else
                rc = MA_ERROR_INVALIDARG;
            #endif
            break;	

        }
        case MA_MSGBUS_AUTH_INFO_ATTRIBUTE_GID:	{
            #if    !defined(MA_WINDOWS)
                long *p = va_arg(ap, long *);
                if(p) *p = self->gid;
                else  rc = MA_ERROR_INVALIDARG;		 
            #else
                rc = MA_ERROR_INVALIDARG;
            #endif
            break;	
        }
        case MA_MSGBUS_AUTH_INFO_ATTRIBUTE_USER_IS_PRIVILEGED:	{
            ma_bool_t *p = va_arg(ap, ma_bool_t *);
            if(p) *p = self->is_privileged;
            else  rc = MA_ERROR_INVALIDARG;		 
            break;	
        }
        default:
            rc = MA_ERROR_INVALID_OPTION;
            break;
    }
    return rc;
}

MA_MSGBUS_API ma_error_t ma_message_auth_info_vsetopt(ma_message_auth_info_t *self, ma_message_auth_info_attribute_t option, va_list ap) {
    ma_error_t rc = MA_OK;	
    switch(option) {
        case MA_MSGBUS_AUTH_INFO_ATTRIBUTE_VERIFIED_SIGNER:	{
            const char *p = va_arg(ap, const char *);
            if(p) self->issuer_cn = strdup(p);
            else  rc = MA_ERROR_INVALIDARG;		 
            break;	
        }
        case MA_MSGBUS_AUTH_INFO_ATTRIBUTE_VERIFIED_PUBLISHER:	{        
            const char *p = va_arg(ap, const char *);
            if(p) self->subject_cn = strdup(p);
            else  rc = MA_ERROR_INVALIDARG;		 
            break;	
        }
        case MA_MSGBUS_AUTH_INFO_ATTRIBUTE_USER_SID: {        
            #if defined(MA_WINDOWS)
                void *psid = va_arg(ap, void *);                
                DWORD sid_size = GetLengthSid(psid);
                self->psid = malloc(sid_size);
                if (self->psid) {
                    if (!CopySid(sid_size, self->psid, psid)) {
                        free(self->psid),self->psid = NULL;                        
                        rc = MA_ERROR_UNEXPECTED;
                    }
                }else
                    rc = MA_ERROR_OUTOFMEMORY;
            #else 
                rc = MA_ERROR_INVALIDARG;
            #endif
            break;	
        }
        case MA_MSGBUS_AUTH_INFO_ATTRIBUTE_PID:	{
            self->pid = va_arg(ap, long);	 	 
            break;	
        }
        case MA_MSGBUS_AUTH_INFO_ATTRIBUTE_UID: {
            #if    !defined(MA_WINDOWS)
                self->uid = va_arg(ap, long);
            #else
                rc = MA_ERROR_INVALIDARG;
            #endif
            break;	

        }
        case MA_MSGBUS_AUTH_INFO_ATTRIBUTE_GID:	{
            #if    !defined(MA_WINDOWS)
                self->gid = va_arg(ap, long);
            #else
                rc = MA_ERROR_INVALIDARG;
            #endif
            break;	
        }
        case MA_MSGBUS_AUTH_INFO_ATTRIBUTE_USER_IS_PRIVILEGED:	{
            self->is_privileged =  va_arg(ap, ma_bool_t);	 
            break;	
        }
        default:
            rc = MA_ERROR_INVALID_OPTION;
            break;
    }
    return rc;
}

ma_error_t ma_message_auth_info_get_attribute(const ma_message_auth_info_t *self, ma_message_auth_info_attribute_t attrib, ...) {
    if (self) {
        ma_error_t rc = MA_OK;	
        va_list arg;   	

        va_start(arg, attrib);
        rc = ma_message_auth_info_vgetopt(self, attrib, arg);
        va_end(arg);
        return rc;
    }

    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_message_auth_info_set_attribute(ma_message_auth_info_t *self, ma_message_auth_info_attribute_t attrib, ...) {
    if (self) {
        ma_error_t rc = MA_OK;
        va_list arg;   
        va_start(arg, attrib);
        rc = ma_message_auth_info_vsetopt(self, attrib, arg);
        va_end(arg);
        return rc;
    }

    return MA_ERROR_INVALIDARG;
}


