#ifndef MA_TABLE_INTERNAL_H_INCLUDED
#define MA_TABLE_INTERNAL_H_INCLUDED

#ifdef _MSC_VER
#pragma once
#endif // MSC_VER

#include "ma/ma_common.h"
#include "ma/ma_variant.h"

#include "tree.h"

MA_CPP(extern "C" {)

struct ma_tablenode_t {
    RB_ENTRY(ma_tablenode_t) tree_entry;
    char*                  key;
    size_t                 key_len;
    ma_variant_t*          value;    
};

typedef struct ma_tablenode_t ma_tablenode_t, *ma_tablenode_h;
typedef struct ma_table_tree_s ma_table_tree_t;
RB_HEAD (ma_table_tree_s, ma_tablenode_t);


ma_error_t ma_create_tablenode(ma_tablenode_t **self);

ma_error_t ma_set_tablenode_key(ma_tablenode_h self, const void* key, ma_uint32_t key_is_wchar);

ma_error_t ma_insert_tablenode_in_rbtree(ma_table_tree_t *tree, ma_tablenode_h node);

ma_error_t ma_table_get_node_count(ma_tablenode_t *tree, size_t *size);

ma_error_t ma_table_get_rbtree_key_list(ma_table_tree_t *tree, ma_array_t *keys);

ma_error_t ma_table_search_key_in_rbtree(ma_table_tree_t *tree,const void* key,ma_uint32_t key_is_wchar, ma_variant_t **table_value);

ma_error_t ma_table_remove_key_from_rbtree(ma_table_tree_t *root, const void *key, ma_uint32_t key_is_wchar);

ma_error_t ma_table_iterate_rbtree(ma_table_tree_t *tree, MA_TABLE_FOREACH_CALLBACK char_cb, MA_TABLE_WFOREACH_CALLBACK wchar_cb, void *user_arg);

MA_CPP(})
#endif
