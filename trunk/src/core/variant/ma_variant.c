#include "ma/ma_variant.h"
#include "ma/internal/utils/threading/ma_atomic.h"
#include "ma/internal/ma_macros.h"
#include "ma_buffer_internal.h"
#include "ma_variant_macros.h"
#include "ma_buffer_internal.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>


#define MA_CHAR_STR "char"

/*this is to know the */
typedef enum ma_val_type_e {
    MA_VAL_TYPE_NULL   = 0x0001,
	MA_VAL_TYPE_BOOL   = 0x0002,
	
	/*unsigned integer type.*/
    MA_VAL_TYPE_UINT64 = 0x0004,
    MA_VAL_TYPE_UINT32 = 0x0008,
	MA_VAL_TYPE_UINT16 = 0x0010,
	MA_VAL_TYPE_UINT8  = 0x0020,

	/*signed integer type.*/
	MA_VAL_TYPE_INT64  = 0x0040,
    MA_VAL_TYPE_INT16  = 0x0080,
    MA_VAL_TYPE_INT32  = 0x0100,
	MA_VAL_TYPE_INT8   = 0x0200,
    
	/*float types*/
	MA_VAL_TYPE_DOUBLE = 0x0400,
    MA_VAL_TYPE_FLOAT  = 0x0800,
    
	/*raw types*/
	MA_VAL_TYPE_RAW    = 0x1000,
	MA_VAL_TYPE_STRING = 0x2000,

	/*complex type.*/
    MA_VAL_TYPE_TABLE  = 0x4000,
    MA_VAL_TYPE_ARRAY  = 0x8000,    
}ma_val_type_t;

static ma_uint16_t get_value_type(ma_uint16_t var_type)
{
	static ma_uint16_t var_value_type[]  = { 
		MA_VAL_TYPE_NULL, 
		MA_VAL_TYPE_UINT64, 
		MA_VAL_TYPE_INT64, 
		MA_VAL_TYPE_DOUBLE, 
		MA_VAL_TYPE_UINT32, 
		MA_VAL_TYPE_INT32, 
		MA_VAL_TYPE_FLOAT,
		MA_VAL_TYPE_UINT16,
		MA_VAL_TYPE_INT16,
		MA_VAL_TYPE_UINT8,
		MA_VAL_TYPE_INT8,
		MA_VAL_TYPE_BOOL,
		MA_VAL_TYPE_TABLE,
		MA_VAL_TYPE_ARRAY,
		MA_VAL_TYPE_RAW,
		MA_VAL_TYPE_STRING
	};
	return var_value_type[var_type];
}

#define MA_BUFFER_TYPE					(MA_VAL_TYPE_RAW | MA_VAL_TYPE_STRING)

/** @brief Opaque variant structure definition
 *  @field  [1]  ma_object       Union     consist of different ma_variant 
 *                                         types supported
 *  @field  [2]   ref_count       Integer   Reference count
 *  @field  [3]   ma_type         in16      Holds variant type as per creation
 *  @field  [4]   value_type      in16      Holds variant value type.
 *  @field  [5]   weak_type       in16      Holds ORed of types, so that value can be accessed in other possible type.
 */
struct ma_variant_s {
    union {
        ma_bool_t   ma_obj_bool;  /*boolean value represented in this memory*/
        ma_uint8_t  ma_obj_uint8; /*uint8 value represented in this memory*/
        ma_int8_t   ma_obj_int8;  /*int8 value represented in this memory*/
        ma_uint16_t ma_obj_uint16;/*uint16 value represented in this memory*/
        ma_int16_t  ma_obj_int16; /*int16 value represented in this memory*/
        ma_uint32_t ma_obj_uint32;/*uint32 value represented in this memory*/
        ma_int32_t  ma_obj_int32; /*int32 value represented in this memory*/
        ma_uint64_t ma_obj_uint64;/*uint64 value represented in this memory*/
        ma_int64_t  ma_obj_int64; /*int64 value represented in this memory*/
        ma_double_t ma_obj_double;/*double value represented in this memory*/
        ma_float_t  ma_obj_float; /*float value represented in this memory*/
        ma_table_t  *ma_obj_table;/*table representation mapped in this memory*/
        ma_array_t  *ma_obj_array;/*array representation mapped in this memory*/
        ma_buffer_t *ma_obj_raw;  /*raw value represented in this memory*/        
    }ma_object;
    ma_atomic_counter_t ref_count; /*object reference counting mechanism for memory management*/
    ma_uint16_t ma_type; /* variant creation type (ma_vartype_t)*/	
	ma_uint16_t value_type; /* variant value type, value type can be ORED of multiple value types (ma_val_type_t).*/    
};


/** @brief Validates for cyclic dependency(self referential) 
 *         for arrays and tables
 *  @param   [in]  l        Generic pointer that holds address of 
                                        variant's ma_object
 *  @param   [in]  r        variant handler
 *  @param   [out] result   Pointer to memory location that stores the result
 *                          MA_TRUE for both the variant refers to same 
 *                          memory location otherwise MA_FALSE
 *  @return                 MA_OK on successfull
 *                          MA_ERROR_INVALIDARG on invalid argument passed
 */
ma_error_t ma_variant_validate_self_reference(void* l, const ma_variant_t *r, int *result) {
    if(l && r && result) {
        ma_error_t err = MA_OK;
        ma_vartype_t var_type = MA_VARTYPE_NULL;

        *result = MA_FALSE; /*initialize result to MA_FALSE*/
        /* Check given second variant is table or array */
        if(MA_OK == (err = ma_variant_get_type(r, &var_type))) {   
            /*Array must not store same array as an element*/
            if(MA_VARTYPE_ARRAY == var_type) {
                if((ma_array_h)l == r->ma_object.ma_obj_array) 
                    *result = MA_TRUE;
            } else if(MA_VARTYPE_TABLE == var_type) {
            /* Table must not store same table as value, for different keys*/
                if((ma_table_h)l == r->ma_object.ma_obj_table)
                    *result = MA_TRUE;
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

/** @brief  Destroy variant object
 *  @param   [in]  self  variant handle
 *  @result        MA_OK on successful freeing variant object
 */
static ma_error_t ma_variant_destroy(ma_variant_t *self) {
    if(self) {
        switch(self->ma_type) {/*switch start*/
            case MA_VARTYPE_TABLE: {
                /*decrements table reference count*/
                ma_table_release(self->ma_object.ma_obj_table);
                break;
            }
            case MA_VARTYPE_ARRAY: {
                /*decrements array reference count*/
                ma_array_release(self->ma_object.ma_obj_array);
                break;
            }
			case MA_VARTYPE_STRING: 
            case MA_VARTYPE_RAW: {
                /*decrements raw reference count*/
                ma_buffer_release(self->ma_object.ma_obj_raw);
                break;
            }            
        }/* switch ends */
        free(self); /*free variant handle*/
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_variant_add_ref(ma_variant_t *self) {
    if(self) {
        MA_ATOMIC_INCREMENT(self->ref_count);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_variant_release(ma_variant_t *self)
{
    if(self) {
        ma_error_t err = MA_OK;
        if(0 == MA_ATOMIC_DECREMENT(self->ref_count)) {
            err = ma_variant_destroy(self);
        }
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_variant_create(ma_variant_t **variant)
{
    if(variant) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;

        if((*variant = (ma_variant_t*)calloc(1, sizeof(ma_variant_t)))) {
            /*Initializing variant as NULL type*/
            (*variant)->ma_type = MA_VARTYPE_NULL;			
			(*variant)->value_type = MA_VAL_TYPE_NULL;
            /*Increment refrence count on after successful creation of variant*/
            if(MA_OK != (err = ma_variant_add_ref(*variant))) {
                free(*variant);
                *variant = NULL;
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_variant_get_version(unsigned int *version) {
   if(version) {
       *version = VARIANTLIBRARYVER;
       return MA_OK;
   }
   return MA_ERROR_INVALIDARG;
}

ma_error_t ma_variant_is_equal(const ma_variant_t *first, const ma_variant_t *second, ma_bool_t *result) {
    if(first && second && result) {
        ma_error_t err = MA_OK;
        
        *result = MA_FALSE;

		if(!(first->value_type & second->value_type)) {
			/*no match.*/
            return err;
        }

        if(first == second) {
        /* self referential check */
            *result = MA_TRUE;
        } else {
			switch(first->ma_type) {
                case MA_VARTYPE_NULL: {
                    *result = MA_TRUE;
                    break;
                }
                case MA_VARTYPE_BOOL: {
                    *result = first->ma_object.ma_obj_bool == second->ma_object.ma_obj_bool;
                    break;
                }				
				case MA_VARTYPE_INT8:
				case MA_VARTYPE_INT16:
				case MA_VARTYPE_INT32:
				case MA_VARTYPE_INT64: {
                    *result = first->ma_object.ma_obj_int64 == second->ma_object.ma_obj_int64;
                    break;
                }
				case MA_VARTYPE_UINT8:
				case MA_VARTYPE_UINT16:
				case MA_VARTYPE_UINT32:
                case MA_VARTYPE_UINT64: {
                    *result = first->ma_object.ma_obj_uint64 == second->ma_object.ma_obj_uint64;
                    break;
                }
				case MA_VARTYPE_FLOAT:
                case MA_VARTYPE_DOUBLE: {
                    *result = first->ma_object.ma_obj_double == second->ma_object.ma_obj_double;
                    break;
                }
				case MA_VARTYPE_STRING:
                case MA_VARTYPE_RAW: {/*raw buffer comparison start*/
                    /*compares the raw buffer*/
                    err = ma_buffer_is_equal(first->ma_object.ma_obj_raw,
                                          second->ma_object.ma_obj_raw,
                                          result);
                    break;
                }/*raw buffer comparison end*/
                case MA_VARTYPE_ARRAY: {/*array comparison start*/
                    /*array comparison*/
                    err = ma_array_is_equal(first->ma_object.ma_obj_array,
                                                    second->ma_object.ma_obj_array,
                                                    result);
                    break;
                } /*array comparison end*/
                case MA_VARTYPE_TABLE: {/*table comparison start*/
                    /*table comparison*/
                    err = ma_table_is_equal(first->ma_object.ma_obj_table,
                                                    second->ma_object.ma_obj_table,
                                                    result);
                    break;
                } /*table comparison end*/
            } /*end of switch*/
        } /*end of else*/
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_variant_create_from_raw(const void *value, size_t size, ma_variant_t** self) {
    if(value && self) {
		ma_error_t err = MA_OK;
        if(MA_OK == (err = ma_variant_create(self))) {
			size_t i = 0;
            (*self)->ma_type = MA_VARTYPE_RAW; 			
			(*self)->value_type = MA_VAL_TYPE_RAW; 

			while(i < size && *((char*)value + i)) ++i;			
			if(i == size) /* no null found, so it can be treated as string too. */
				(*self)->value_type |= MA_VAL_TYPE_STRING; 

			if(MA_OK != (err = ma_buffer_create_and_set( &((*self)->ma_object.ma_obj_raw), (char*)value, size) ) )  {
				(void)ma_variant_release(*self); 
				*self = NULL;
            } 
        }             
        return err; 
    }
    return MA_ERROR_INVALIDARG;
} 

ma_error_t ma_variant_create_from_string(const char* value, size_t len, ma_variant_t** self) {
	if(value && self) {
		ma_error_t err = MA_OK;
		size_t ac_len = strlen(value);
		if(MA_OK == (err = ma_variant_create_from_raw(value, (ac_len > len ? len : ac_len), self))){
			(*self)->ma_type = MA_VARTYPE_STRING; 
			(*self)->value_type = MA_BUFFER_TYPE; 
		}
		return err;
	}
	return MA_ERROR_INVALIDARG;
} 

ma_error_t ma_variant_create_from_wstring(const wchar_t* value, size_t len, ma_variant_t** self) {
    if(value && self) {
		ma_error_t err = MA_OK;
        char *buffer_string = NULL; 
		size_t buffer_length = 0; 		
		size_t ac_len = wcslen(value);
		if(MA_OK == (err = ma_buffer_convert_wchar_t_to_char(value, (ac_len > len ? len : ac_len), &buffer_string, &buffer_length))) {
			err = ma_variant_create_from_string(buffer_string, buffer_length, self);
			free(buffer_string);
        }             
        return err; 
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_variant_get_raw_buffer(ma_variant_t *self, ma_buffer_t **buffer){
	if(self && buffer){
		if(MA_VAL_TYPE_RAW & self->value_type){
			ma_buffer_add_ref(*buffer = self->ma_object.ma_obj_raw);
			return MA_OK;
		}
		return MA_ERROR_PRECONDITION;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_variant_get_string_buffer(ma_variant_t *self, ma_buffer_t **buffer){
	return ma_variant_get_raw_buffer(self, buffer);
}

ma_error_t ma_variant_get_raw(ma_variant_t *self, const unsigned char **raw, size_t *size){
	if(self && raw && size){
		if(MA_VAL_TYPE_RAW & self->value_type){
			return ma_buffer_get_raw(self->ma_object.ma_obj_raw, raw, size);			
		}
		return MA_ERROR_PRECONDITION;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_variant_get_string(ma_variant_t *self, const char **str){
	if(self && str){
		if(MA_VAL_TYPE_STRING & self->value_type){
			size_t size = 0;
			return ma_buffer_get_string(self->ma_object.ma_obj_raw, str, &size);			
		}
		return MA_ERROR_PRECONDITION;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_variant_get_wstring_buffer(ma_variant_t *self, ma_wbuffer_t **wbuffer){
	if(self && wbuffer){		
		ma_error_t err = MA_ERROR_PRECONDITION;
		if(MA_VAL_TYPE_RAW & self->value_type){
			
			unsigned char *cstr = NULL;
			size_t clen = 0;
			if(MA_OK == (err = ma_buffer_get_raw(self->ma_object.ma_obj_raw, &cstr, &clen))){
				wchar_t *wstr = NULL;
				size_t wlen = 0;
				if(MA_OK == (err = ma_buffer_convert_char_to_wchar_t((char*)cstr, clen, &wstr, &wlen))) {
					err = ma_wbuffer_create_and_set(wbuffer, wstr, wlen);
					free(wstr);
				} 
			}			
		}
		return err;
	}
	return MA_ERROR_INVALIDARG;
}

/** @brief macro definition to generate methods for strong value data types
 *         supported by the variant library 
 *           ma_variant_create_from_uint64()
 *           ma_variant_create_from_int64()
 *           ma_variant_create_from_double()
 *           ma_variant_create_from_uint32()
 *           ma_variant_create_from_int32()
 *           ma_variant_create_from_float()
 *           ma_variant_create_from_uint16()
 *           ma_variant_create_from_int16()
 *           ma_variant_create_from_uint8()
 *           ma_variant_create_from_int8()
 *           ma_variant_create_from_bool()
 *  @summary Methods will create variant and assign the value
 *           ma_variant_get_uint64()
 *           ma_variant_get_int64()
 *           ma_variant_get_double()
 *           ma_variant_get_uint32()
 *           ma_variant_get_int32()
 *           ma_variant_get_float()
 *           ma_variant_get_uint16()
 *           ma_variant_get_int16()
 *           ma_variant_get_uint8()
 *           ma_variant_get_int8()
 *           ma_variant_get_bool()
 *  @summary Methods to fetch the strong type value from the variant object
 */

#define MA_VARIANT_GENERATE_BOOL_VALUE_TYPE_METHODS(data_type, DATA_TYPE) \
    ma_error_t ma_variant_create_from_##data_type(MA_VARIANT_TOKEN_CONSTRUCTOR(ma_##data_type, _t) value, ma_variant_t **variant) {\
        if(variant) {\
            ma_error_t err = MA_OK; \
            \
            if(MA_OK == (err = ma_variant_create(variant))) {\
                (*variant)->ma_type = MA_VARTYPE_##DATA_TYPE; \
				(*variant)->value_type = MA_VAL_TYPE_##DATA_TYPE; \
                !value?(value = MA_FALSE):(value = MA_TRUE);\
				(*variant)->ma_object.ma_obj_bool = value; \
			} \
            \
            return err; \
        }\
        return MA_ERROR_INVALIDARG;\
    } \
    ma_error_t ma_variant_get_##data_type(const ma_variant_t *self, MA_VARIANT_TOKEN_CONSTRUCTOR(ma_##data_type, _t) *value) {\
        if(self && value) {\
			ma_error_t err = MA_ERROR_PRECONDITION; \
            \
            if(self->value_type & MA_VAL_TYPE_##DATA_TYPE) {\
				*value = self->ma_object.ma_obj_##data_type; \
				err = MA_OK;\
			}\
			return err; \
        }\
        return MA_ERROR_INVALIDARG;\
    } 

MA_VARIANT_GENERATE_BOOL_VALUE_TYPE_METHODS(bool, BOOL)

static ma_error_t variant_create_from_double(ma_double_t value, ma_variant_t *variant, ma_uint16_t value_type) {	
	(variant)->value_type = MA_VAL_TYPE_DOUBLE | value_type; 
	(variant)->ma_object.ma_obj_double = value; 	         
	if((variant)->ma_object.ma_obj_double == (ma_double_t)(variant)->ma_object.ma_obj_float)
		(variant)->value_type |= MA_VAL_TYPE_FLOAT; 
	return MA_OK;
}

#define MA_VARIANT_GENERATE_FLOAT_VALUE_TYPE_METHODS(data_type, DATA_TYPE) \
    ma_error_t ma_variant_create_from_##data_type(MA_VARIANT_TOKEN_CONSTRUCTOR(ma_##data_type, _t) value, ma_variant_t **variant) {\
        if(variant) {\
            ma_error_t err = MA_OK; \
            \
            if(MA_OK == (err = ma_variant_create(variant))) {\
                (*variant)->ma_type = MA_VARTYPE_##DATA_TYPE; \
				err = variant_create_from_double(value, *variant, MA_VAL_TYPE_##DATA_TYPE); \
			} \
            \
            return err; \
        }\
        return MA_ERROR_INVALIDARG;\
    } \
    ma_error_t ma_variant_get_##data_type(const ma_variant_t *self, MA_VARIANT_TOKEN_CONSTRUCTOR(ma_##data_type, _t) *value) {\
        if(self && value) {\
			ma_error_t err = MA_ERROR_PRECONDITION; \
            \
            if(self->value_type & MA_VAL_TYPE_##DATA_TYPE) {\
				*value = (MA_VARIANT_TOKEN_CONSTRUCTOR(ma_##data_type, _t))self->ma_object.ma_obj_double; \
				err = MA_OK;\
            }\
            \
            return err; \
        }\
        return MA_ERROR_INVALIDARG;\
    }

MA_VARIANT_GENERATE_FLOAT_VALUE_TYPE_METHODS(double, DOUBLE)
MA_VARIANT_GENERATE_FLOAT_VALUE_TYPE_METHODS(float, FLOAT)

static ma_error_t ma_variant_create_from_unsigned_integer(ma_uint64_t value, ma_variant_t *variant, ma_uint16_t value_type) {	
    (variant)->value_type = MA_VAL_TYPE_UINT64 | value_type; 
	(variant)->ma_object.ma_obj_uint64 = value; 

	if((variant)->ma_object.ma_obj_uint64 == (ma_uint64_t)(variant)->ma_object.ma_obj_uint32){
		(variant)->value_type |= MA_VAL_TYPE_UINT32;

		if((variant)->ma_object.ma_obj_uint64 == (ma_uint64_t)(variant)->ma_object.ma_obj_uint16){
			(variant)->value_type |= MA_VAL_TYPE_UINT16;

			if((variant)->ma_object.ma_obj_uint64 == (ma_uint64_t)(variant)->ma_object.ma_obj_uint8){
				(variant)->value_type |= MA_VAL_TYPE_UINT8;
			}
		}
	}
	if((variant)->ma_object.ma_obj_uint64 == (ma_uint64_t)(variant)->ma_object.ma_obj_int64){
		(variant)->value_type |= MA_VAL_TYPE_INT64;

		if((variant)->ma_object.ma_obj_uint64 == (ma_uint64_t)(variant)->ma_object.ma_obj_int32){
			(variant)->value_type |= MA_VAL_TYPE_INT32;

			if((variant)->ma_object.ma_obj_uint64 == (ma_uint64_t)(variant)->ma_object.ma_obj_int16){
				(variant)->value_type |= MA_VAL_TYPE_INT16;

				if((variant)->ma_object.ma_obj_uint64 == (ma_uint64_t)(variant)->ma_object.ma_obj_int8){
					(variant)->value_type |= MA_VAL_TYPE_INT8;
				}
			}
		}
	}
    return MA_OK;     
} 

static ma_error_t ma_variant_create_from_signed_integer(ma_int64_t value, ma_variant_t *variant, ma_uint16_t value_type) {	    
	(variant)->value_type = MA_VAL_TYPE_INT64 | value_type; 
	(variant)->ma_object.ma_obj_int64 = value; 	      

	if((variant)->ma_object.ma_obj_int64 == (ma_int64_t)(variant)->ma_object.ma_obj_int32){
		(variant)->value_type |= MA_VAL_TYPE_INT32;

		if((variant)->ma_object.ma_obj_int64 == (ma_int64_t)(variant)->ma_object.ma_obj_int16){
			(variant)->value_type |= MA_VAL_TYPE_INT16;

			if((variant)->ma_object.ma_obj_int64 == (ma_int64_t)(variant)->ma_object.ma_obj_int8){
				(variant)->value_type |= MA_VAL_TYPE_INT8;
			}
		}
	}

    return MA_OK;     
} 

#define MA_VARIANT_GENERATE_INT_VALUE_TYPE_METHODS(data_type, DATA_TYPE) \
    ma_error_t ma_variant_create_from_##data_type(MA_VARIANT_TOKEN_CONSTRUCTOR(ma_##data_type, _t) value, ma_variant_t **variant) {\
        if(variant) {\
            ma_error_t err = MA_OK; \
            \
            if(MA_OK == (err = ma_variant_create(variant))) {\
                (*variant)->ma_type = MA_VARTYPE_##DATA_TYPE; \
                if(value >= 0) {\
                    err = ma_variant_create_from_unsigned_integer(value, *variant, MA_VAL_TYPE_##DATA_TYPE); \
				}\
				else {\
                    err = ma_variant_create_from_signed_integer(value, *variant, MA_VAL_TYPE_##DATA_TYPE); \
				}\
			} \
            \
            return err; \
        }\
        return MA_ERROR_INVALIDARG;\
    } \
    ma_error_t ma_variant_get_##data_type(const ma_variant_t *self, MA_VARIANT_TOKEN_CONSTRUCTOR(ma_##data_type, _t) *value) {\
        if(self && value) {\
            ma_error_t err = MA_ERROR_PRECONDITION; \
            \
            if(self->value_type & MA_VAL_TYPE_##DATA_TYPE) {\
				*value = self->ma_object.ma_obj_##data_type; \
				err = MA_OK;\
            }\
            \
            return err; \
        }\
        return MA_ERROR_INVALIDARG;\
    }
	
MA_VARIANT_GENERATE_INT_VALUE_TYPE_METHODS(uint64, UINT64)
MA_VARIANT_GENERATE_INT_VALUE_TYPE_METHODS(int64, INT64)
MA_VARIANT_GENERATE_INT_VALUE_TYPE_METHODS(uint32, UINT32)
MA_VARIANT_GENERATE_INT_VALUE_TYPE_METHODS(int32, INT32)
MA_VARIANT_GENERATE_INT_VALUE_TYPE_METHODS(uint16, UINT16)
MA_VARIANT_GENERATE_INT_VALUE_TYPE_METHODS(int16, INT16)
MA_VARIANT_GENERATE_INT_VALUE_TYPE_METHODS(uint8, UINT8)
MA_VARIANT_GENERATE_INT_VALUE_TYPE_METHODS(int8, INT8)


/** @brief Macro definition to generate create and get methods
 *         for tables and arrays
 *           ma_variant_create_from_array()
 *           ma_variant_create_from_table()
 *  @summary Methods will create variant and assign the handler
 *           of array/table to the variant object
 *           ma_variant_get_array()
 *           ma_variant_get_table()
 *  @summary Methods to fetch the handle of array/table from the variant object
 */
#define MA_VARIANT_GENERATE_TABLE_ARRAY_METHODS(data_type, DATA_TYPE) \
    ma_error_t ma_variant_create_from_##data_type(MA_VARIANT_TOKEN_CONSTRUCTOR(ma_##data_type,_t) *value, ma_variant_t** variant) {\
        if(value && variant) {\
            ma_error_t err = MA_OK; \
            \
            if(MA_OK == (err = ma_variant_create(variant))) {\
                if(MA_OK != (err = MA_VARIANT_TOKEN_CONSTRUCTOR(ma_##data_type, _add_ref)(value))) {\
                    ma_variant_release(*variant); \
                    *variant = NULL; \
                    return err; \
                }\
                (*variant)->ma_type = MA_VARTYPE_##DATA_TYPE; \
				(*variant)->value_type = MA_VAL_TYPE_##DATA_TYPE; \
                (*variant)->ma_object.ma_obj_##data_type = value; \
            } \
            \
            return err; \
        }\
        return MA_ERROR_INVALIDARG;\
    } \
    ma_error_t ma_variant_get_##data_type(ma_variant_t *self, MA_VARIANT_TOKEN_CONSTRUCTOR(ma_##data_type, _t) **value) {\
        if(self && value) {\
            ma_error_t err = MA_ERROR_PRECONDITION; \
            \
            if((self)->value_type & MA_VAL_TYPE_##DATA_TYPE) {\
                *value = self->ma_object.ma_obj_##data_type; \
                err = ma_##data_type##_add_ref(*value);\
            }\
            \
            return err; \
        }\
        return MA_ERROR_INVALIDARG;\
    }

MA_VARIANT_GENERATE_TABLE_ARRAY_METHODS(array, ARRAY)
MA_VARIANT_GENERATE_TABLE_ARRAY_METHODS(table, TABLE)


ma_error_t ma_variant_is_type(const ma_variant_t *self, ma_vartype_t var_type){
	if(self){
		if(self->ma_type == var_type || self->value_type & get_value_type(var_type))
			return MA_OK;		
		return MA_ERROR_TYPE_MISMATCH;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_variant_get_type(const ma_variant_t *self, ma_vartype_t *var_type) {
    if(self && var_type) {
		*var_type = (ma_vartype_t)self->ma_type;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

