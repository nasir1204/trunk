#include "ma/ma_buffer.h"
#include "ma/internal/utils/threading/ma_atomic.h"
#include "ma/internal/ma_macros.h"
#include "ma_buffer_internal.h"
#include "ma_variant_macros.h"
#include "ma/internal/utils/text/ma_utf8.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

/** @brief Macro definition to generate buffer methods:
 * ma_buffer_get_string_length()
 * ma_buffer_get_string()
 * ma_buffer_release()
 * ma_buffer_create()
 * ma_buffer_add_ref()
 * ma_buffer_set()
 * ma_buffer_size()
 * ma_buffer_is_equal()
 * 
 * ma_wbuffer_get_string_length()
 * ma_wbuffer_get_string()
 * ma_wbuffer_release()
 * ma_wbuffer_create()
 * ma_wbuffer_add_ref()
 * ma_wbuffer_set()
 * ma_wbuffer_size()
 * ma_wbuffer_is_equal()
 *
 * @summary ma_buffer_create
 *          allocates memory for the buffer
 *          and increments the reference and updates the buffer length in bytes,
 *          in case of any failures it will deallocates the buffer
 *          and returns error code 
 *
 * @summary ma_buffer_set
 *          It copies the content into the buffer
 *          if i/p buffer length > buffer created length, returns error
 *
  */
#define MA_VARIANT_GENERATE_BUFFER_METHODS(buffer, buffer_type, char_type) \
    ma_error_t MA_VARIANT_TOKEN_CONSTRUCTOR(ma_##buffer, _get_string_length)(const char_type* str, size_t *len) {\
        if(str && len) {\
            const char_type* s = str;\
            \
            for(*len = 0; *s; ++s, ++(*len));\
            \
            return MA_OK;\
        }\
        return MA_ERROR_INVALIDARG;\
    } \
    ma_error_t MA_VARIANT_TOKEN_CONSTRUCTOR(ma_##buffer, _add_ref)(buffer_type *self) {\
        if(self) { \
            MA_ATOMIC_INCREMENT(self->ref_count); \
            return MA_OK; \
        }\
        return MA_ERROR_INVALIDARG;\
    } \
    ma_error_t MA_VARIANT_TOKEN_CONSTRUCTOR(ma_##buffer, _release)(buffer_type* self) { \
        if(self) {\
            if(0 == MA_ATOMIC_DECREMENT(self->ref_count)) free(self); \
            return MA_OK; \
        }\
        return MA_ERROR_INVALIDARG; \
    } \
    ma_error_t MA_VARIANT_TOKEN_CONSTRUCTOR(ma_##buffer, _create)(buffer_type **self, size_t len) {\
        if(self) {\
            ma_error_t err = MA_ERROR_OUTOFMEMORY; \
            \
            if((*self = (buffer_type*)calloc(1, sizeof(buffer_type) + ((len+1) * sizeof(char_type))))) {\
                (*self)->length = len * sizeof(char_type); \
                if(MA_OK != (err = MA_VARIANT_TOKEN_CONSTRUCTOR(ma_##buffer, _add_ref)(*self))) {\
                    if(*self)free(*self);\
                    *self = NULL;\
                }\
            }\
            \
            return err; \
        }\
        return MA_ERROR_INVALIDARG;\
    } \
    ma_error_t MA_VARIANT_TOKEN_CONSTRUCTOR(ma_##buffer, _set)(buffer_type *self, const char_type *src, size_t buffer_length) {\
        if(self && src) { \
            if(buffer_length) {\
                if(buffer_length > (self->length/sizeof(char_type))) return MA_ERROR_INVALIDARG;\
                memset(self->buff, 0, self->length);\
                memcpy(self->buff, src, (sizeof(char_type) * buffer_length)); \
            }\
            return MA_OK; \
        }\
        return MA_ERROR_INVALIDARG;\
    } \
    ma_error_t MA_VARIANT_TOKEN_CONSTRUCTOR(ma_##buffer, _get_string)(buffer_type *self, const char_type **value, size_t *size) {\
        if(self && value && size) {\
            ma_error_t err = MA_OK; \
            \
            *value = NULL; *size = 0; \
            if(MA_OK == (err = MA_VARIANT_TOKEN_CONSTRUCTOR(ma_##buffer, _get_string_length)(self->buff, size))) {\
                (*value) = (const char_type*)self->buff; \
            }\
            \
            return err; \
        }\
        return MA_ERROR_INVALIDARG;\
    } \
    ma_error_t MA_VARIANT_TOKEN_CONSTRUCTOR(ma_##buffer, _size)(buffer_type *self, size_t *buffer_length) {\
        if(self && buffer_length) {\
            *buffer_length = self->length; \
            return MA_OK; \
        }\
        return MA_ERROR_INVALIDARG; \
    }\
    ma_error_t MA_VARIANT_TOKEN_CONSTRUCTOR(ma_##buffer,_create_and_set)(buffer_type** self, const char_type* src, size_t len) {\
        if(self && src) {\
            ma_error_t err = MA_OK;\
            \
            if(MA_OK == (err = MA_VARIANT_TOKEN_CONSTRUCTOR(ma_##buffer, _create)(self, len))) {\
                if(MA_OK != (err = MA_VARIANT_TOKEN_CONSTRUCTOR(ma_##buffer, _set)(*self,src,len))) {\
                    (void)MA_VARIANT_TOKEN_CONSTRUCTOR(ma_##buffer, _release)(*self); \
                }\
            }\
            \
            return err;\
        }\
        return MA_ERROR_INVALIDARG;\
    }\
    ma_error_t ma_##buffer##_is_equal(const buffer_type *first, const buffer_type *second, ma_bool_t *result) {\
       if(first && second && result) {\
            *result = MA_FALSE;\
            if(first->length == second->length) {\
                *result = MA_VARIANT_BYTE_COMPARE(first->buff, second->buff, (first->length/sizeof(char_type)), char_type);\
            }\
            return MA_OK;\
       }\
       return MA_ERROR_INVALIDARG;\
    }


MA_VARIANT_GENERATE_BUFFER_METHODS(buffer, ma_buffer_t, char)
#ifdef MA_HAS_WCHAR_T
MA_VARIANT_GENERATE_BUFFER_METHODS(wbuffer, ma_wbuffer_t, ma_wchar_t)

ma_error_t ma_buffer_convert_wchar_t_to_char(wchar_t const *w_value, size_t w_value_length, char **value, size_t *value_length) {
    if(w_value && value && value_length) {
        size_t out_len = 0;
        if((unsigned int)-1 != (out_len =  ma_wide_char_to_utf8(NULL, 0 , w_value, w_value_length))) {   
            *value = (char *)calloc(out_len, sizeof(char));
            if(*value) {
                if((unsigned int)-1 != (*value_length = ma_wide_char_to_utf8(*value, out_len, w_value, w_value_length)))
                    return MA_OK;
                free(*value);
                return MA_ERROR_UNEXPECTED;
            }
            return MA_ERROR_OUTOFMEMORY;
        }
        return MA_ERROR_UNEXPECTED;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_buffer_convert_char_to_wchar_t(char const *value, size_t value_length, wchar_t **w_value, size_t *w_value_length) {
    if(value && w_value && w_value_length) {
        size_t out_len = 0;
        if((unsigned int)-1 != (out_len =  ma_utf8_to_wide_char(NULL, 0 , value, value_length))) {   
            *w_value = (wchar_t *)calloc(out_len, sizeof(wchar_t));
            if(*w_value) {
                if((unsigned int)-1 != (*w_value_length = ma_utf8_to_wide_char(*w_value, out_len, value, value_length)))
                    return MA_OK;
                free(*w_value);
                return MA_ERROR_UNEXPECTED;
            }
            return MA_ERROR_OUTOFMEMORY;
        }
        return MA_ERROR_UNEXPECTED;
    }
    return MA_ERROR_INVALIDARG;    
}

#endif


ma_error_t ma_buffer_get_raw(ma_buffer_t *self, const unsigned char **value, size_t *size) { 
    if(self && value && size) { 
        *size = self->length; 
        *value = (const unsigned char*)self->buff; 
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
} 
