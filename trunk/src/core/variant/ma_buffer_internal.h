#ifndef MA_BUFFER_INTERNAL_H_INCLUDED
#define MA_BUFFER_INTERNAL_H_INCLUDED

MA_CPP(extern "C" {)
#pragma pack(push, 1)

/** @brief Opaque buffer structure definition
 *  @field  [1]  length       Integer    length in bytes
 *  @field  [2]  ref_count    Integer    Reference count
 *  @field  [3]  buff         array      buffer data
 */
struct ma_buffer_s
{
    size_t  length;
    size_t  ref_count;
    char         buff[1];
};

/* Holds the size of ma_buffer_s structure*/
#define MA_BUFFER_SIZE (sizeof(struct ma_buffer_s))

/** @brief creates a buffer object and sets the value
 *  @param  [in]   self   buffer handle
 *  @param  [in]   src    string
 *  @param  [in]  len     length
 *  @result               MA_OK on success
 *                        MA_ERROR_INVALIDARG on invalid argument passed
 *                        MA_ERROR_OUTOFMEMORY on memory allocation failure
 */
ma_error_t ma_buffer_create_and_set(ma_buffer_t **self,
                                    const char *src, 
                                    size_t len);

/** @brief computes string length stored in char buffer
 *  @param  [in]  str  char string
 *  @param  [out] len  pointer to memory location that holds length of string
 *  @result            MA_OK on success
 *                     MA_ERROR_INVALIDARG on invalid argument passed
 */
ma_error_t ma_buffer_get_string_length(const char* str,
                                       size_t *len);

#ifdef MA_HAS_WCHAR_T
/** @brief Opaque wide buffer structure definition
 *  @field  [1]  length       Integer    length in bytes
 *  @field  [2]  ref_count    Integer    Reference count
 *  @field  [3]  buff         array      wide buffer data
 */
struct ma_wbuffer_s
{
    size_t  length;
    size_t  ref_count;
    wchar_t      buff[1];
};

/*Holds the sizeof ma_wbuffer_s structure*/
#define MA_WBUFFER_SIZE (sizeof(struct ma_wbuffer_s))

/** @brief converts the wide char string to char string
 *  @param  [in]    w_value         wide char buffer handle
 *  @param  [in]    w_value_length  wide char length
 *  @param  [out]   value           char buffer handle
 *  @param  [out]   value_length    char length
 *  @result                      MA_OK on success
 *                               MA_ERROR_INVALIDARG on invalid argument passed
 *                               MA_ERROR_UNEXPECTED on buffer operation malfunction
 */
ma_error_t ma_buffer_convert_wchar_t_to_char(wchar_t const *w_value, size_t w_value_length, char **value, size_t *value_length);

/** @brief converts the char string to wide char string
 *  @param  [in]   value         char buffer handle
 *  @param  [in]   value_length  char length
 *  @param  [out]  w_value       wide char string
 *  @param  [out]  w_value_length   char length
 *  @result                      MA_OK on success
 *                               MA_ERROR_INVALIDARG on invalid argument passed
 *                               MA_ERROR_UNEXPECTED on buffer operation malfunction
 */
ma_error_t ma_buffer_convert_char_to_wchar_t(char const *value, size_t value_length, wchar_t **w_value, size_t *w_value_length);

/** @brief creates a wide char buffer object and sets the value
 *  @param  [in]   self   wide char buffer handle
 *  @param  [in]   src    string
 *  @param  [in]  len     length
 *  @result               MA_OK on success
 *                        MA_ERROR_INVALIDARG on invalid argument passed
 *                        MA_ERROR_OUTOFMEMORY on memory allocation failure
 */
ma_error_t ma_wbuffer_create_and_set(ma_wbuffer_t **self,
                                    const wchar_t *src, 
                                    size_t len);

/** @brief computes string length stored in wide char buffer
 *  @param  [in]  str  wide char string
 *  @param  [out] len  pointer to memory location that holds length of string
 *  @result            MA_OK on success
 *                     MA_ERROR_INVALIDARG on invalid argument passed
 */             
ma_error_t ma_wbuffer_get_string_length(const wchar_t* str,
                                       size_t *len);
#endif
 
#pragma pack(pop)

MA_CPP(})
#endif
