#include "ma/ma_variant.h"
#include "ma/internal/utils/threading/ma_atomic.h"
#include "ma/internal/ma_macros.h"
#include "ma_table_internal.h"
#include "ma_variant_macros.h"


#include <string.h>
#include <stdlib.h>
#include <stdio.h>

/*1 byte alignment*/
#pragma pack(push, 1)

extern ma_error_t ma_variant_validate_self_reference(void* l, const ma_variant_h r, int *result);
extern ma_error_t ma_table_destroy_tablenode(ma_tablenode_h table_node);
extern ma_error_t ma_table_erase_table(ma_tablenode_h table_node);

/** @brief Opaque table structure definition
 *  @field  [1]  rb_tree      ma_tablenode_t    memory location where the table 
 *                                                node starts
 *  @field  [2]  ref_count      Integer           Reference count
 */
struct ma_table_s {
    ma_table_tree_t     rb_tree;
    ma_atomic_counter_t ref_count;
};


/** @brief  Destroy table object
            This API will be called after deallocating
            all the other nodes in the table.
 *  @param   [in]  self  table handle
 *  @result        MA_OK on successful freeing table object
 */
static ma_error_t ma_table_destroy(ma_table_h self) {
    if(self) {
        ma_error_t err = MA_OK;
            /*deallocate the root table node of the table*/
        ma_table_destroy_tablenode(self->rb_tree.rbh_root);
        free(self);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_table_add_ref(ma_table_t *self) {
    if(self) {
        MA_ATOMIC_INCREMENT(self->ref_count);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_table_release(ma_table_t *self) {
    if(self) {
        ma_error_t err = MA_OK;

        if(0 == MA_ATOMIC_DECREMENT(self->ref_count)) {
            if(self->rb_tree.rbh_root) {
                /*first deallocate all the nodes to the left side of root node*/
                ma_table_erase_table(self->rb_tree.rbh_root->tree_entry.rbe_left);
                /*then deallocate all the nodes to the right side of root node*/
                ma_table_erase_table(self->rb_tree.rbh_root->tree_entry.rbe_right);
            }
            /*finally deallocate the root node and destroy the table object*/
            ma_table_destroy(self);
        }
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_table_create(ma_table_t **table) {
    if(table) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;

        if((*table = (ma_table_t*)calloc(1, sizeof(ma_table_t)))) {
            //(*table)->rb_tree = RB_INITIALIZER((*table)->rb_tree);
            if(MA_OK != (err = ma_table_add_ref(*table))) {
                /*deallocate the table object*/
                free(*table);
                *table = NULL;
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_table_size(const ma_table_t *self, size_t *size) {
    if(size && self) {
        ma_error_t err = MA_OK;

        *size = 0;
        /*iterate through the table nodes and count the number of nodes*/
        if(MA_OK != (err = ma_table_get_node_count(self->rb_tree.rbh_root, size))) {
            *size = 0;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_table_get_keys(const ma_table_t *self, ma_array_t **keys) {
    if(self && keys) {
        ma_error_t err = MA_OK;

        /*create an array handle to store the key list*/
        if(MA_OK == (err = ma_array_create(keys))) {
            /*iterate through the table nodes and store the node key in the array*/
            if (MA_OK == (err = ma_table_get_rbtree_key_list((ma_table_tree_t*)&self->rb_tree, *keys))) {
                return MA_OK;
            }
            /* oh well, didn't quite work, unwind */
            ma_array_release(*keys);
            *keys=NULL;
        }
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_table_is_equal(ma_table_t const *l, ma_table_t const *r, ma_bool_t *result) {
    if(l && r && result) {
        ma_error_t err = MA_OK;
        size_t lsize = 0;
        size_t rsize = 0;
        ma_array_h  larray = NULL;
        ma_array_h  rarray = NULL;

        *result = MA_FALSE;

        err = ma_table_size(l, &lsize);
        if(MA_OK != err) return err;

        err = ma_table_size(r, &rsize);
        if(MA_OK != err) return err;

        if(lsize != rsize) return MA_OK;

        if(0 == lsize) {
            *result = MA_TRUE;
            return MA_OK;
        }

        if (MA_OK == (err = ma_table_get_keys(l, &larray)) && (MA_OK == (err = ma_table_get_keys(r, &rarray)))) {
            if (MA_OK == (err = ma_array_is_equal(larray, rarray, result)) && MA_TRUE == *result) {
                size_t larray_size = 0;
                size_t index = 0;

                *result = MA_FALSE;
                if (MA_OK == (err = ma_array_size(larray, &larray_size))) {
                    *result = MA_TRUE; /* true until one pair is different */
                    for(index = 0; (MA_FALSE != *result) && (MA_OK==err) && index < larray_size; index++) {
                        ma_variant_h lvariant = NULL;
                        ma_variant_h rvariant = NULL;

                        if (MA_OK == (err = ma_array_get_element_at(larray,index + 1,&lvariant)) && (MA_OK == (err = ma_array_get_element_at(rarray,index + 1, &rvariant)))) {
                            ma_buffer_h lbuffer = NULL;
                            ma_buffer_h rbuffer = NULL;

                            if (MA_OK == (err = ma_variant_get_string_buffer(lvariant, &lbuffer)) && (MA_OK == (err = ma_variant_get_string_buffer(rvariant, &rbuffer)))) {
                                size_t lbuffer_length = 0;
                                size_t rbuffer_length = 0;
                                char const *lvalue = NULL, *rvalue = NULL;

                                if (MA_OK == (err = ma_buffer_get_string(lbuffer,&lvalue, &lbuffer_length)) && (MA_OK == (err = ma_buffer_get_string(rbuffer,&rvalue, &rbuffer_length)))) {
                                    ma_variant_h lvariant = NULL;
                                    ma_variant_h rvariant = NULL;

                                    MA_OK == (err = ma_table_get_value((ma_table_t*)l,lvalue, &lvariant)) &&
                                    MA_OK == (err = ma_table_get_value((ma_table_t*)r,rvalue, &rvariant)) &&
                                    MA_OK == (err = ma_variant_is_equal(lvariant, rvariant, result));

                                    ma_variant_release(rvariant);
                                    ma_variant_release(lvariant);
                                }
                            }
                            ma_buffer_release(rbuffer);
                            ma_buffer_release(lbuffer);
                        }
                        ma_variant_release(rvariant);
                        ma_variant_release(lvariant);
                    }
                }
            }
        }

        ma_array_release(rarray);
        ma_array_release(larray);


        /* make it false if anything failed */
        if (MA_OK != err) *result = MA_FALSE;

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

#define MA_GENERATE_TABLE_ENTRY_METHODS(value_type, data_type, string_type)\
    ma_error_t MA_VARIANT_TOKEN_CONSTRUCTOR(ma_table_add_##value_type, entry)(ma_table_t *self, data_type *table_key, ma_variant_t *value) {\
        if(self && table_key && value) {\
            ma_error_t err = MA_OK, rc = MA_OK;\
            int result = MA_FALSE;\
            ma_vartype_t var_type = MA_VARTYPE_NULL;\
            ma_tablenode_h node = NULL;\
            \
            if(MA_OK != (err= ma_variant_get_type(value, &var_type))) return err;\
            if(MA_VARTYPE_TABLE == var_type) {\
                if(MA_OK != (err= ma_variant_validate_self_reference(self, value, &result)))return err;\
                if(result == MA_TRUE) return MA_ERROR_INVALIDARG;\
            }\
            if(MA_OK != (err = ma_create_tablenode(&node))) {\
                return (MA_OK != (rc = ma_table_destroy_tablenode(node)))?rc: err;\
            }\
            if(MA_OK != (err = ma_set_tablenode_key(node, table_key, string_type))) {\
                return (MA_OK !=  (rc = ma_table_destroy_tablenode(node)))?rc: err;\
            }\
            node->value = value;\
            if(MA_OK != (err = ma_variant_add_ref(node->value))) {\
                return (MA_OK != (rc = ma_table_destroy_tablenode(node)))?rc: err;\
            }\
            if(!self->rb_tree.rbh_root) {\
                self->rb_tree.rbh_root = node;\
            } else {\
                if(MA_OK != (err = ma_insert_tablenode_in_rbtree((ma_table_tree_t*)&self->rb_tree, node))) {\
                    if(MA_OK != (rc = ma_table_destroy_tablenode(node))) err = rc;\
                }\
            }\
            return err;\
        }\
        return MA_ERROR_INVALIDARG;\
    }\
    ma_error_t MA_VARIANT_TOKEN_CONSTRUCTOR(ma_table_get_##value_type, value)(ma_table_t *self, data_type *table_key, ma_variant_t **table_value) {\
        if(self && table_key && table_value) {\
            *table_value = NULL;\
            return ma_table_search_key_in_rbtree((ma_table_tree_t*)&self->rb_tree, table_key,string_type, table_value);\
        }\
        return MA_ERROR_INVALIDARG;\
    }\
    ma_error_t MA_VARIANT_TOKEN_CONSTRUCTOR(ma_table_remove_##value_type, entry)(ma_table_t *self, data_type *table_key) {\
        return(self && table_key)?ma_table_remove_key_from_rbtree((ma_table_tree_t*)&self->rb_tree, table_key,string_type):MA_ERROR_INVALIDARG;\
    }

#define MA_GENERATE_TABLE_FOREACH_METHODS(value_type, cb_type, fn_arg1, fn_arg2)\
    ma_error_t MA_VARIANT_TOKEN_CONSTRUCTOR(ma_table_##value_type, foreach)(const ma_table_t *self, cb_type cb,void *cb_args) {\
        if(self) {\
            return cb?ma_table_iterate_rbtree((ma_table_tree_t*)&self->rb_tree, fn_arg1, fn_arg2, cb_args):MA_ERROR_PRECONDITION;\
        }\
        return MA_ERROR_INVALIDARG;\
    }

MA_GENERATE_TABLE_ENTRY_METHODS( , const char, MA_ASCII_STRING)

MA_GENERATE_TABLE_FOREACH_METHODS( , MA_TABLE_FOREACH_CALLBACK, cb, NULL)

#ifdef MA_HAS_WCHAR_T
MA_GENERATE_TABLE_ENTRY_METHODS(w, const wchar_t, MA_WCHAR_STRING)

MA_GENERATE_TABLE_FOREACH_METHODS(w, MA_TABLE_WFOREACH_CALLBACK, NULL, cb)
#endif

