#include "ma/ma_variant.h"
#include "ma/internal/utils/threading/ma_atomic.h"
#include "ma/internal/ma_macros.h"
#include "ma_table_internal.h"
#include "ma_buffer_internal.h"
#include "ma_variant_macros.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include "ma/internal/utils/text/ma_utf8.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#ifndef MA_WINDOWS
#include <wchar.h>
#endif

static int ma_table_node_compare(const ma_tablenode_t *t1, const ma_tablenode_t *t2);
static ma_tablenode_t* find_table_entry(ma_table_tree_t *root, const void *key, ma_uint32_t key_is_wchar);
RB_GENERATE_STATIC(ma_table_tree_s, ma_tablenode_t, tree_entry, ma_table_node_compare);

int ma_table_node_compare(const ma_tablenode_t *t1, const ma_tablenode_t *t2) {
    return strcmp( (const char*)t1->key, (const char*)t2->key);    
}

ma_error_t ma_table_destroy_tablenode(ma_tablenode_t *table_node) {
    if(table_node) {
        ma_error_t err = MA_OK;

        if(table_node->value)err = ma_variant_release(table_node->value);
        if(table_node->key)free(table_node->key);
        free(table_node);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_table_erase_table(ma_tablenode_t *table_node) {
    if(table_node) {
        /*First deallocate all the nodes lying to the left of the root node,
          traverse through the table to mark the node for deletion*/
        ma_table_erase_table(table_node->tree_entry.rbe_left);
        /*First deallocate all the nodes lying to the right of the root node,
          traverse through the table to mark the node for deletion*/
        ma_table_erase_table(table_node->tree_entry.rbe_right);
        /*actual call to deallocate the node marked for deletion*/
        return ma_table_destroy_tablenode(table_node);
    }
    return MA_OK; //intentional
}

ma_error_t ma_table_get_node_count(ma_tablenode_t *node, size_t *size) {
    if(size) {
        if(node) {
            /*increment the count value while traversing through each node*/
            *size = *size + 1;
            /*First count all the nodes lying to the left of the root node,
              traverse through the table to count all the left nodes*/
            ma_table_get_node_count(node->tree_entry.rbe_left, size);
            /*First count all the nodes lying to the right of the root node,
              traverse through the table to count all the right nodes*/
            ma_table_get_node_count(node->tree_entry.rbe_right, size);
        }
        return MA_OK; //intentional
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_create_tablenode(ma_tablenode_t** self) {
    if(self) {
        return ((*self) = (ma_tablenode_t*)calloc(1, sizeof(ma_tablenode_t)))?MA_OK:MA_ERROR_OUTOFMEMORY;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_set_tablenode_key_char(ma_tablenode_t *self, const char* key) {
	self->key_len = strlen(key);
	if(self->key_len && (self->key = strdup(key))) 
		return MA_OK;            
	return MA_ERROR_OUTOFMEMORY;
}

ma_error_t ma_set_tablenode_key(ma_tablenode_t *self, const void* key, ma_uint32_t key_is_wchar) {
    if(self && key) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;
        if(key_is_wchar) {
			ma_temp_buffer_t conv_buff = {0};
			ma_temp_buffer_init(&conv_buff);
			if(MA_OK == (err = ma_convert_wide_char_to_utf8((wchar_t *)key, &conv_buff))) {
				err = ma_set_tablenode_key_char(self, (const char*)ma_temp_buffer_get(&conv_buff));				
			}
			ma_temp_buffer_uninit(&conv_buff);
		} else {
            err = ma_set_tablenode_key_char(self, (const char*)key);				
        } 
		return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_insert_tablenode_in_rbtree(ma_table_tree_t *tree, ma_tablenode_t *node) {
    if(tree && node) {
        ma_tablenode_t *found_node = NULL;

        if((found_node = RB_INSERT(ma_table_tree_s, tree, node))) {
            ma_variant_release(found_node->value);
            ma_variant_add_ref(found_node->value = node->value);
            ma_table_destroy_tablenode(node);
        }

        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_table_get_rbtree_key_list(ma_table_tree_t *tree, ma_array_t *keys) {
    if(tree && keys) {
        ma_error_t err = MA_OK;
        ma_tablenode_t *table_entry = NULL;

        if(!tree->rbh_root) return MA_OK;
        RB_FOREACH(table_entry, ma_table_tree_s, tree) {
            ma_variant_t *key_variant = NULL;

            if(MA_OK == (err = ma_variant_create_from_string((const char*)table_entry->key, table_entry->key_len, &key_variant))) {
                err = ma_array_push(keys, key_variant);
                (void) ma_variant_release(key_variant);
            }
            if(MA_OK != err) return err;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_tablenode_t* find_table_entry_char(ma_table_tree_t *root, const char *key) {
    if(root && key) {
        ma_tablenode_t search_node = {{0}};
        ma_tablenode_t *entry = NULL;
        
        if((search_node.key_len = strlen((const char*)key)) > 0) {
            search_node.key = (char*)key;            
            entry =  RB_FIND(ma_table_tree_s, root, &search_node);
        }        
        return entry;
    }
    return NULL;
}

ma_tablenode_t* find_table_entry(ma_table_tree_t *root, const void *key, ma_uint32_t key_is_wchar) {
    if(root && key) {
        ma_tablenode_t search_node = {{0}};
        ma_tablenode_t *entry = NULL;
        if(key_is_wchar){
			ma_temp_buffer_t conv_buff = {0};
			ma_temp_buffer_init(&conv_buff);
			if(MA_OK == ma_convert_wide_char_to_utf8((wchar_t *)key, &conv_buff)) 
				return find_table_entry_char(root, (const char*)ma_temp_buffer_get(&conv_buff));
			else
				return NULL;
		}
		else{
			return find_table_entry_char(root, (char*)key);
		}
    }
    return NULL;
}

ma_error_t ma_table_search_key_in_rbtree(ma_table_tree_t *root, const void* key, ma_uint32_t key_is_wchar, ma_variant_t **table_value) {
    if(key && table_value) {
        ma_error_t err = MA_ERROR_OBJECTNOTFOUND;
        ma_tablenode_t *entry = NULL;

        if((entry = find_table_entry(root, key, key_is_wchar))) {
            err = ma_variant_add_ref(*table_value = entry->value);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_table_remove_key_from_rbtree(ma_table_tree_t *root, const void *key, ma_uint32_t key_is_wchar) {
    if(root && key) {
        ma_error_t err = MA_ERROR_OBJECTNOTFOUND;
        ma_tablenode_t *entry = NULL;

        if((entry = find_table_entry(root, key, key_is_wchar))) {
            RB_REMOVE(ma_table_tree_s, root, entry);
            err = ma_table_destroy_tablenode(entry);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_table_iterate_rbtree(ma_table_tree_t *tree, MA_TABLE_FOREACH_CALLBACK char_cb, MA_TABLE_WFOREACH_CALLBACK wchar_cb, void *user_arg) {
    if(tree) {
        ma_error_t err = MA_OK;
        ma_tablenode_t *table_entry = NULL;
        ma_bool_t stop_loop = MA_FALSE;

        if(!tree->rbh_root || (!char_cb && !wchar_cb)) return MA_OK;
        RB_FOREACH(table_entry, ma_table_tree_s,tree) {
            if(char_cb) {
                (*char_cb)((const char*)table_entry->key, table_entry->value, user_arg, &stop_loop);                
            } else if(wchar_cb) {
                ma_temp_buffer_t conv_buff = {0};
                ma_temp_buffer_init(&conv_buff);

                if(MA_OK == (err = ma_convert_utf8_to_wide_char((char const *)table_entry->key, &conv_buff))) {
                    (*wchar_cb)((const wchar_t*)ma_temp_buffer_get(&conv_buff), table_entry->value, user_arg,&stop_loop);
                }
                ma_temp_buffer_uninit(&conv_buff);                
            }
            if(MA_TRUE == stop_loop)break;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

