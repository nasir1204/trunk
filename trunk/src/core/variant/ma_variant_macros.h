#ifndef MA_VARIANT_MACROS_H_INCLUDED
#define MA_VARIANT_MACROS_H_INCLUDED

#define MA_VARIANT_LIBRARY_VERSION "1.0"

#define MA_TRUE  1
#define MA_FALSE 0

#define MA_UNLOCK MA_FALSE
#define MA_LOCK   MA_TRUE

#define MA_VARIANT_TOKEN_CONSTRUCTOR(token1, token2) token1##token2

#define MA_VARIANT_BYTE_COMPARE(first, second, len, type) \
         (!memcmp(first, second, (sizeof(type) * len))?MA_TRUE:MA_FALSE)

#endif
