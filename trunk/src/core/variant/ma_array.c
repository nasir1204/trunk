#include "ma/ma_variant.h"
#include "ma/internal/utils/threading/ma_atomic.h"
#include "ma/internal/ma_macros.h"
#include "ma_variant_macros.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

MA_CPP(extern "C" {)
ma_error_t ma_variant_validate_self_reference(void* l, const ma_variant_h r, int *result);
MA_CPP(})

/** @brief Opaque array structure definition
 *  @field  [1]  array       ma_variant_t    Dangling pointer for collection of array element
 *  @field  [2]  size  Integer         The number of variant elements in the array
 *  @field  [3]  ref_count      Integer         Reference count
 */
struct ma_array_s {
    ma_variant_t **array;
    size_t size;
    ma_atomic_counter_t ref_count;
};


ma_error_t ma_array_release(ma_array_t *self) {
    if(self) {
        if(!MA_ATOMIC_DECREMENT(self->ref_count)) {
            if (self->array)  {
                /*iterate through each element of the array until array size reaches 0*/
                while(self->size) {
                    /*decrement the reference of each of the variant element in the array*/
                    (void)ma_variant_release(self->array[self->size - 1]);
                    --self->size;
                }
                free(self->array);
            }
            /*deallocate the array object*/
            free(self);
        }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_array_add_ref(ma_array_t *self) {
    if(self) {
        MA_ATOMIC_INCREMENT(self->ref_count);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_array_create(ma_array_t **new_list) {
    if(new_list) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;

        if((*new_list = (ma_array_t*)calloc(1, sizeof(ma_array_t)))) {
            if(MA_OK != (err = ma_array_add_ref(*new_list))) {
                free(*new_list);
                *new_list = NULL;
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_array_size(const ma_array_t *self, size_t *size) {
    if(self && size) {
        *size = self->array?self->size:0;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_array_push(ma_array_t *self, ma_variant_t *entry) {
    if(self && entry) {
        ma_error_t err = MA_OK;
        int result = MA_FALSE;

        /*validate self referential check */
        if(MA_OK == (err = ma_variant_validate_self_reference(self, entry, &result))) {
            err = MA_ERROR_INVALIDARG;
            if(!result) {
                err = MA_ERROR_OUTOFMEMORY;
                if((self->array = (ma_variant_t**)realloc(self->array,((self->size + 1) *sizeof(ma_variant_t*))))) {
                    /*Increment the ref count on the variant object*/
                    err = ma_variant_add_ref(self->array[self->size++] = entry);
                }
            }
        }
    
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_array_get_element_at(ma_array_t *self, size_t index, ma_variant_t **entry) {
    if(self && entry) {
        ma_error_t err = MA_OK;

        if((0 == index) || (index > self->size)) return MA_ERROR_INVALIDARG;
        if(index <= self->size) {
            if(!self->array) return MA_ERROR_UNEXPECTED;
            if(MA_OK != (err = ma_variant_add_ref(self->array[index -1]))) {
                *entry = NULL;
                return err;
            }
            *entry = self->array[index -1];
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_array_set_element_at(ma_array_t *self, size_t index, ma_variant_t *entry) {
    if(self && entry) {
        ma_error_t err = MA_OK;

        if((0 == index) || (index > self->size)) return MA_ERROR_INVALIDARG;
        if(index <= self->size) {
            if(!self->array) return MA_ERROR_UNEXPECTED;
			if(self->array[index - 1]) ma_variant_release(self->array[index - 1]);
			err = ma_variant_add_ref(self->array[index - 1] = entry);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_array_foreach(const ma_array_t *self, MA_ARRAY_FOREACH_CALLBACK cb, void *cb_args) {
    if(self) {
        ma_error_t err = MA_ERROR_PRECONDITION;
        ma_bool_t stop_loop = MA_FALSE;
        size_t count = 0;

        if(cb) {
            err = MA_OK;
            for(count = 0; !stop_loop && (count < self->size); ++count) {	
                if(!self->array) return MA_ERROR_UNEXPECTED;
                (*cb)(count+1, self->array[count], cb_args, &stop_loop);
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_array_is_equal(ma_array_t const *l, ma_array_t const *r, ma_bool_t *result) {
    if(l && r && result) {
        ma_error_t err = MA_OK;
        size_t lsize = 0;
        size_t rsize = 0;
        size_t index = 0;

        *result = MA_FALSE;

        if(MA_OK != (err= ma_array_size(l, &lsize))) return err;
        if(MA_OK != (err= ma_array_size(r, &rsize))) return err;

        if(lsize != rsize) return MA_OK;

        if(0 == lsize) *result = MA_TRUE; // empty array's

        for(index = 0; ((index < lsize) && (MA_OK == (err = ma_variant_is_equal(l->array[index], r->array[index], result)))); index++) {
            if(MA_FALSE == *result) break;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

