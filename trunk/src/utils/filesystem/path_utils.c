#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

ma_error_t form_path( ma_temp_buffer_t *destination, const char *root_path_, const char *sub_path_, ma_bool_t b_datastore_path) {
	if(destination && root_path_ && sub_path_) {
		size_t len1 = strlen(root_path_);
		size_t len2 = strlen(sub_path_);
		size_t cap = 0;
		char *dest = NULL;
		/*Better to duplicate and work because we may get the root path or the subpath as the destination, and we don't want to mess up the buffers*/
		char *root_path = strdup(root_path_);
		char *sub_path = strdup(sub_path_);
        char path_seperator = b_datastore_path? MA_DATASTORE_PATH_C: MA_PATH_SEPARATOR;
		if((!len1) && (!len2)){
			free(root_path);
			free(sub_path);
			return MA_ERROR_INVALIDARG;
		}
		else if((len1 && (!len2)) || ((!len1) && len2)) {
			ma_temp_buffer_reserve(destination,len1? len1:len2);
			cap = ma_temp_buffer_capacity(destination);
			dest = (char*) ma_temp_buffer_get(destination);
			MA_MSC_SELECT(_snprintf, snprintf)(dest, cap, "%s", len1? root_path:sub_path);
		}
		else if(len1 && len2) {
			ma_temp_buffer_reserve(destination, len1+len2+1);
			cap = ma_temp_buffer_capacity(destination);
			dest = (char*)ma_temp_buffer_get(destination);
			if((root_path[len1 - 1] != path_seperator) && (sub_path[0] != path_seperator)){
				MA_MSC_SELECT(_snprintf, snprintf)(dest,cap, "%s%c%s", root_path,path_seperator,sub_path);
			}
			else {
				if((root_path[len1 - 1] == path_seperator) && (sub_path[0]== path_seperator))
					MA_MSC_SELECT(_snprintf, snprintf)(dest, cap, "%s%s", root_path, sub_path+1);
				else
					MA_MSC_SELECT(_snprintf, snprintf)(dest, cap, "%s%s", root_path, sub_path);
			}
		}
		free(root_path);
		free(sub_path);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}
