#include "ma/internal/utils/filesystem/ma_fs_utils.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/platform/ma_sys_apis.h"
#include "ma/internal/utils/acl/ma_acl.h"
#include "ma/internal/defs/ma_common_services_defs.h"

#include <stdio.h>
#include <fcntl.h>

#define MA_GET_LAST_CHARACTER(s)	                 (s == NULL || strlen(s) == 0 ? 0 : *(s + (strlen(s)-1)))

ma_bool_t ma_fs_is_file_exists(uv_loop_t *uv_loop, const char *file_path) {
    ma_bool_t b_rc = MA_FALSE;
    if(uv_loop && file_path) {
        uv_fs_t req = {0};
        b_rc = !uv_fs_stat(uv_loop, &req, file_path, NULL) && !req.result;
        uv_fs_req_cleanup(&req);
    }
    return b_rc;
}

ma_bool_t	ma_fs_is_dir(uv_loop_t *uv_loop, const char *file_path) {
	ma_bool_t b_rc = MA_FALSE ;
	if(uv_loop && file_path) {		/* TODO , in case of failure .. returning false, caller will treat it as file */
        uv_fs_t req ;
		if(!uv_fs_stat(uv_loop, &req, file_path, NULL)) {

			#ifdef MA_WINDOWS
            struct _stat64 s = req.statbuf;
			#else
            struct stat s = req.statbuf;
			#endif

			/*b_rc = (((struct stat*)&req.ptr)->st_mode & S_IFDIR) ? MA_TRUE : MA_FALSE ;*/

			b_rc = (s.st_mode & S_IFDIR) ? MA_TRUE : MA_FALSE ;
            uv_fs_req_cleanup(&req) ;
		}
	}
	return b_rc ;
}

/* TBD - length field in uv_fs_t structure is not providing the size of file, so using internal structure data for file size for now */
/* These structures might be changed in UV newer versions, so this code might be changed according to UV changes */
size_t ma_fs_get_file_size(uv_loop_t *uv_loop, const char *file_path) {
    size_t size = 0;
    if(uv_loop && file_path) {
        uv_fs_t stat_req = {0};
        if(!uv_fs_stat(uv_loop, &stat_req, file_path, NULL)) {
#ifdef MA_WINDOWS
            struct _stat64 s = stat_req.statbuf;
#else
            struct stat s = stat_req.statbuf;
#endif
            size = s.st_size;
        }
		uv_fs_req_cleanup(&stat_req);
    }
    return size;
}

void ma_fs_make_recursive_dir(uv_loop_t *uv_loop, char *base_name, char *dir_name) {
    uv_fs_t req;
    char path[MAX_PATH_LEN] = {0};

    char *p = strrchr(dir_name, MA_PATH_SEPARATOR);
    if(p) {
        *p = '\0';
        ma_fs_make_recursive_dir(uv_loop, base_name, dir_name);
        *p = MA_PATH_SEPARATOR;
    }

    if(base_name)
        MA_MSC_SELECT(_snprintf,snprintf)(path, MAX_PATH_LEN, "%s%c%s", base_name, MA_PATH_SEPARATOR, dir_name);
    else
        strncpy(path, dir_name, MAX_PATH_LEN-1);

    uv_fs_mkdir(uv_loop, &req, path, 0755, NULL); /* TBD - Permission to be reviewed */
    uv_fs_req_cleanup(&req);

    return;
}

static ma_error_t change_ownership(uv_loop_t *uv_loop, char *obj_name, const char *username, ma_int64_t inheritance_flags,  ma_bool_t allow_inheritance){
	ma_acl_t *acl = NULL;
	ma_error_t rc = MA_OK;
	if(ma_fs_is_file_exists(uv_loop, obj_name)) {
		if(MA_OK == (rc = ma_acl_create(MA_SID_LOCAL, username, &acl))){
			if(MA_OK == (rc = ma_acl_add(acl, MA_TRUE, MA_CMNSVC_ACL_PERMISSIONS, inheritance_flags))){
				rc = ma_acl_attach_object(acl, obj_name, MA_ACL_OBJECT_FILE, allow_inheritance);
			}
			ma_acl_release(acl);
		}
	}
	return rc;
}

void ma_fs_make_recursive_dir_as_user(uv_loop_t *uv_loop, char *base_name, char *dir_name, const char *username) {
    uv_fs_t req;
    char path[MAX_PATH_LEN] = {0};

    char *p = strrchr(dir_name, MA_PATH_SEPARATOR);
    if(p) {
        *p = '\0';
        ma_fs_make_recursive_dir_as_user(uv_loop, base_name, dir_name, username);
        *p = MA_PATH_SEPARATOR;
    }

    if(base_name)
        MA_MSC_SELECT(_snprintf,snprintf)(path, MAX_PATH_LEN, "%s%c%s", base_name, MA_PATH_SEPARATOR, dir_name);
    else
        strncpy(path, dir_name, MAX_PATH_LEN-1);

    uv_fs_mkdir(uv_loop, &req, path, 0755, NULL); /* TBD - Permission to be reviewed */
	//ma_sys_chfileown(path, username) ;
	change_ownership(uv_loop, path, username, MA_CMNSVC_ACL_INHERITANCE, MA_TRUE);

    uv_fs_req_cleanup(&req);

    return;
}

void ma_fs_create_file(uv_loop_t *uv_loop, const char *file_name) {
    uv_fs_t req;
    uv_file file = uv_fs_open(uv_loop, &req, file_name, O_RDWR | O_CREAT, 0755, NULL);
    if(-1 != file) {
        uv_fs_req_cleanup(&req);
        uv_fs_close(uv_loop, &req, file, NULL);
        uv_fs_req_cleanup(&req);
    }
}

ma_bool_t ma_fs_remove_dir(uv_loop_t *uv_loop, char *dir_name) {
	ma_bool_t b_rc = MA_FALSE ;
    if(uv_loop && dir_name) {
        uv_fs_t req ;
        if(ma_fs_is_file_exists(uv_loop, dir_name)) {
            if(-1 != uv_fs_rmdir(uv_loop, &req, dir_name, NULL)) {
                b_rc = MA_TRUE ;
                uv_fs_req_cleanup(&req) ;
            }
        }
        else
            b_rc = MA_TRUE;
    }
    return b_rc;
}

ma_bool_t ma_fs_remove_file(uv_loop_t *uv_loop, const char *file) {
    ma_bool_t b_rc = MA_FALSE ;

	if(uv_loop && file) {
        uv_fs_t req ;
		if(ma_fs_is_file_exists(uv_loop, file)) {
			if(!uv_fs_unlink(uv_loop, &req, file, NULL)) {
				b_rc = MA_TRUE ;
				uv_fs_req_cleanup(&req) ;
			}
		}
		else
			b_rc = MA_TRUE ;
    }
    return b_rc;
}

ma_bool_t ma_fs_remove_recursive_dir(uv_loop_t *uv_loop, char *dir_name) {
	ma_bool_t b_rc = MA_TRUE ;
	ma_error_t rc = MA_OK ;
    if(uv_loop && dir_name && strlen(dir_name) > 0) {
		ma_filesystem_iterator_t *fs_iter = NULL ;
        if(MA_OK == (rc = ma_filesystem_iterator_create(dir_name, &fs_iter)) && b_rc) {
			ma_temp_buffer_t buf = MA_TEMP_BUFFER_INIT ;
			(void)ma_filesystem_iterator_set_mode(fs_iter, MA_FILESYSTEM_ITERATE_ALL) ;
			while (MA_OK == ma_filesystem_iterator_get_next(fs_iter, &buf)) {
				char path[MAX_PATH_LEN] = {0} ;

				MA_MSC_SELECT(_snprintf,snprintf)(path, MAX_PATH_LEN, "%s%c%s", dir_name, MA_PATH_SEPARATOR, (const char*)ma_temp_buffer_get(&buf)) ;

				if(ma_fs_is_dir(uv_loop, path)) {
					b_rc = ma_fs_remove_recursive_dir(uv_loop, path) ;		/* TODO check return code */
				}
				else
					b_rc = ma_fs_remove_file(uv_loop, path) ;

				ma_temp_buffer_uninit(&buf) ;
			}
			ma_filesystem_iterator_release(fs_iter) ;
			b_rc = ma_fs_remove_dir(uv_loop, dir_name) ;
		}
    }
    return b_rc;
}

ma_bool_t ma_fs_move_file(uv_loop_t *uv_loop, const char *src, const char *dst) {
    ma_bool_t b_rc = MA_FALSE;
    if(uv_loop && src && dst) {
        uv_fs_t req;
        if(-1 != uv_fs_rename(uv_loop, &req, src, dst, NULL)) {
            b_rc = MA_TRUE;
            uv_fs_req_cleanup(&req);
        }
    }
    return b_rc;
}

ma_bool_t ma_fs_move_file_as_user(uv_loop_t *uv_loop, char *src, char *dst, const char *username) {
	if(ma_fs_move_file(uv_loop, src, dst)) {
		change_ownership(uv_loop, dst, username, MA_CMNSVC_ACL_INHERITANCE, MA_TRUE);
		return MA_TRUE ;
	}
	return MA_FALSE ;
}

ma_bool_t ma_fs_copy_file(uv_loop_t *uv_loop, char *src, char *dst) {
    ma_bool_t b_rc = MA_FALSE ;

	if(uv_loop && src && dst) {
        uv_fs_t req ;
		uv_file file ;
		size_t src_size = 0 ;

		src_size = ma_fs_get_file_size(uv_loop, src) ;

		if(src_size > 0 && (-1 != uv_fs_open(uv_loop, &req, src, O_RDONLY, 0, NULL))) {
			unsigned char *buffer = (unsigned char *)calloc(1, src_size) ;

			file = req.result ;
			uv_fs_req_cleanup(&req) ;

			if(-1 != uv_fs_read(uv_loop, &req, file, buffer, src_size, -1, NULL)) {
				uv_fs_t dst_req ;
				uv_fs_req_cleanup(&req) ;

				if(-1 != uv_fs_open(uv_loop, &dst_req, dst, O_CREAT | O_RDWR, 00700 , NULL)) {
					uv_file dst_file ;

					dst_file = dst_req.result ;
					uv_fs_req_cleanup(&dst_req) ;

					if(-1 != uv_fs_write(uv_loop, &dst_req, dst_file, buffer, src_size, -1, NULL)) {
						uv_fs_req_cleanup(&dst_req) ;
						b_rc = MA_TRUE ;
					}
					uv_fs_close(uv_loop, &dst_req, dst_file, NULL) ;
					uv_fs_req_cleanup(&dst_req) ;
				}
			}
			free(buffer) ;	buffer = NULL ;
			uv_fs_close(uv_loop, &req, file, NULL) ;
			uv_fs_req_cleanup(&req) ;
		}

    }
    return b_rc;
}

ma_bool_t ma_fs_copy_file_with_timestamp(uv_loop_t *uv_loop, char *src, char *dst) {
	ma_bool_t b_rc = MA_FALSE ;

	if(uv_loop && src && dst) {
		if(b_rc = ma_fs_copy_file(uv_loop, src, dst)) {
			uv_fs_t s_req = {0};
			int r = uv_fs_stat(uv_loop, &s_req, src, NULL);
			
			if(r == 0 && s_req.result == 0) {
				uv_fs_t req = {0};		  
				uv_fs_utime(uv_loop, &req, dst, s_req.statbuf.st_atime, s_req.statbuf.st_mtime, NULL);
				uv_fs_req_cleanup(&req);
			}
			uv_fs_req_cleanup(&s_req);		
		}
	}
	return b_rc;
}

ma_bool_t ma_fs_copy_file_as_user(uv_loop_t *uv_loop, char *src, char *dst, const char *username) {
	if(ma_fs_copy_file(uv_loop, src, dst)) {
		change_ownership(uv_loop, dst, username, MA_CMNSVC_ACL_INHERITANCE, MA_TRUE);
		return MA_TRUE ;
	}
	return MA_FALSE ;
}


ma_bool_t ma_fs_write_data(uv_loop_t *uv_loop, char *file_name, unsigned char *data, size_t size) {
	ma_bool_t b_rc = MA_FALSE ;

	if(uv_loop && file_name && data) {
		uv_fs_t req ;
		uv_file file ;

		if(-1 != uv_fs_open(uv_loop, &req, file_name, O_CREAT | O_RDWR, 0644, NULL)) {
			file = req.result ;
			uv_fs_req_cleanup(&req) ;

			if(-1 != uv_fs_write(uv_loop, &req, file, data, size, -1, NULL)) {
				uv_fs_req_cleanup(&req) ;
				b_rc = MA_TRUE ;
			}
			uv_fs_close(uv_loop, &req, file, NULL) ;
			uv_fs_req_cleanup(&req) ;
		}
	}
	return b_rc ;
}

ma_bool_t ma_fs_get_data(uv_loop_t *uv_loop, const char *file_name, unsigned char *data, size_t *size) {
    ma_bool_t b_rc = MA_FALSE;
    if(uv_loop && file_name && size) {
        if(!data) {
            *size = ma_fs_get_file_size(uv_loop, file_name) ;
            b_rc = (*size > 0) ? MA_TRUE : MA_FALSE;
        }
        else {
            uv_fs_t req ;
		    uv_file file ;
		    if(-1 != uv_fs_open(uv_loop, &req, file_name, O_CREAT | O_RDWR, 0644, NULL)) {
			    file = req.result ;
			    uv_fs_req_cleanup(&req) ;
                if(-1 != (*size = uv_fs_read(uv_loop, &req, file, data, *size, -1, NULL))) {
                    b_rc = MA_TRUE;
                    uv_fs_req_cleanup(&req) ;                
                }
                uv_fs_close(uv_loop, &req, file, NULL);
                uv_fs_req_cleanup(&req) ; 
            }
       }
    }
    return b_rc;
}
