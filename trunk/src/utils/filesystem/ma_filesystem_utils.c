#include "ma/internal/utils/filesystem/filesystem_utils.h"
#include "ma/internal/utils/datastructures/ma_stream.h"
#include "ma/internal/utils/platform/ma_sys_apis.h"

#include "ma/internal/ma_macros.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

ma_error_t ma_filesystem_directory_tree_create(char *base, char *dir_path) {
	if(base && dir_path) {
		ma_temp_buffer_t temp_path = MA_TEMP_BUFFER_INIT ;
		ma_error_t rc = MA_OK ;

		if(MA_OK == (rc = form_path(&temp_path, base, dir_path, MA_FALSE))) {
			rc = ma_filesystem_directory_create((char *)ma_temp_buffer_get(&temp_path)) ;
		}
		if(MA_OK != rc)
		{
			char *sub_path = strdup(dir_path) ;
			char *p = NULL, *token = NULL ;
			rc = MA_OK ;

			ma_temp_buffer_uninit(&temp_path) ;
			ma_temp_buffer_reserve(&temp_path, strlen(base)+1) ;
			MA_MSC_SELECT(_snprintf, snprintf)(p = (char *)ma_temp_buffer_get(&temp_path), strlen(base), "%s", base) ;
			token = strtok(sub_path, MA_PATH_SEPARATOR_STR) ;
			while(token && rc == MA_OK ) {
				if(MA_OK == (rc = form_path(&temp_path, p = (char *)ma_temp_buffer_get(&temp_path), token, MA_FALSE))) {
					rc = ma_filesystem_directory_create((char *)ma_temp_buffer_get(&temp_path)) ;
				}
				token = strtok(NULL, MA_PATH_SEPARATOR_STR) ;
			}
			free(sub_path); sub_path = NULL;
		}
		ma_temp_buffer_uninit(&temp_path) ;
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_filesystem_write_data_to_file(char *file, unsigned char *data, size_t data_size) {
	if(file && data) {
		ma_error_t rc = MA_OK ;
		ma_stream_t *file_stream = NULL ;

		if(MA_OK == (rc = ma_fstream_create(file, MA_STREAM_ACCESS_FLAG_WRITE, (void *)file, &file_stream))) {
			size_t written_size = 0 ;
			if(MA_OK == (rc = ma_stream_write(file_stream, data, data_size, &written_size))) {
				if(data_size == written_size) {
					rc = MA_ERROR_STREAM_WRITE_ERROR ;
				}
			}
			ma_stream_release(file_stream) ; file_stream = NULL ;
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

#ifdef MA_WINDOWS
#include <Windows.h>
ma_error_t ma_filesystem_directory_create(char *path) {
	if(path) {
		if(!CreateDirectoryA(path, NULL)) {
			DWORD ret = 0 ;
			if(ERROR_ALREADY_EXISTS != (ret = GetLastError()))
				return MA_ERROR_DIRECTORY_CREATE_FAILED ;
		}
		return MA_OK ;
	}
	return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_filesystem_copy_file(char *source,  char *dest) {
	if(source && dest) {
		ma_error_t rc = MA_OK ;

		if(!CopyFileA(source, dest, MA_FALSE)) {
			DWORD ret = GetLastError() ;
			rc = MA_ERROR_FILE_COPY_FAILED ;
		}
		else {
			/*TBD set file attributes */
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_filesystem_move_file(char *source,  char *dest) {
	if(source && dest) {
		ma_error_t rc = MA_OK ;

		if(!MoveFileA(source, dest)) {
			DWORD ret = GetLastError() ;
			rc = MA_ERROR_FILE_MOVE_FAILED ;
		}
		else {
			/* TBD set file attributes */
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_filesystem_delete_file(char *file) {
	if(file) {
		ma_error_t rc = MA_OK ;

		if(!DeleteFileA(file)) {
			if(ERROR_FILE_NOT_FOUND != GetLastError())
				rc = MA_ERROR_FILE_DELETE_FAILED ;
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}
#else
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>
#include <unistd.h>

ma_error_t ma_filesystem_directory_create(char *path) {
	if(path) {
		if(-1 == mkdir(path, 755))
			if(EEXIST != errno)
				return MA_ERROR_DIRECTORY_CREATE_FAILED ;
		return MA_OK ;
	}
	return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_filesystem_copy_file(char *source,  char *dest) {
	if(source && dest) {
		ma_error_t rc = MA_OK ;
		ma_stream_t *src_stream = NULL, *dest_stream = NULL ;

		if(MA_OK == (rc = ma_fstream_create(source, MA_STREAM_ACCESS_FLAG_READ, (void *)source, &src_stream))) {
			if(MA_OK == (rc = ma_fstream_create(dest, MA_STREAM_ACCESS_FLAG_WRITE, (void *)dest, &dest_stream))) {
				size_t src_stream_size = 0, read_bytes = 0, write_bytes = 0 ;
				unsigned char *buffer = NULL ;
				
				ma_stream_get_size(src_stream, &src_stream_size) ;
				buffer = (unsigned char *)calloc(1, src_stream_size+1) ;

				ma_stream_read(src_stream, buffer, src_stream_size, &read_bytes) ;
				ma_stream_write(dest_stream, buffer, src_stream_size, &write_bytes) ;

				free(buffer) ; buffer = NULL ;
				ma_stream_release(dest_stream) ; dest_stream = NULL ;
			}
			ma_stream_release(src_stream) ; src_stream = NULL ;
		}
		return rc;
	}
	return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_filesystem_move_file(char *source,  char *dest) {
	if(source && dest) {
		if(0 == rename( source , dest ))
			return MA_OK ;
		else
			return MA_ERROR_FILE_MOVE_FAILED ;
	}
	return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_filesystem_delete_file(char *file) {
	if(file) {
		ma_error_t rc = MA_OK ;

		if(0 != remove(file)) {
			if(errno != ENOENT)
				rc = MA_ERROR_FILE_DELETE_FAILED ;
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}
#endif
