#include "ma/internal/utils/location/ma_location_info.h"
#include "ma/internal/utils/location/ma_location.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/defs/ma_location_defs.h"
#include "ma/ma_log.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

extern ma_logger_t *location_logger;

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "location"

struct ma_location_info_s {
    char email_id[MA_MAX_LEN+1];
    char pincode[MA_MAX_LEN+1];
    ma_table_t *pincodeMap;
    unsigned int exec_size;
    ma_task_time_t date;
    ma_task_time_t entry_date;
    ma_location_info_type_t type;
    char entry[MA_MAX_LEN+1];
    char details[MA_MAX_BUFFER_LEN+1];
};


ma_error_t ma_location_info_create(ma_location_info_t **info) {
    if(!info) return MA_ERROR_INVALIDARG;
    if(!(*info = (ma_location_info_t*)calloc(1, sizeof(ma_location_info_t))))return MA_ERROR_OUTOFMEMORY;
    return  ma_table_create(&(*info)->pincodeMap);
}

ma_error_t ma_location_info_release(ma_location_info_t *info) {
    if(info) {
        if(info->pincodeMap)(void)ma_table_release(info->pincodeMap);
        free(info);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_location_info_remove_contact(ma_location_info_t *info, const char *pincode, const char *contact) {
    if(info && pincode && contact) {
        ma_error_t err = MA_OK;
        ma_variant_t *pincodev = NULL;

        if(MA_OK == (err = ma_table_get_value(info->pincodeMap, pincode, &pincodev))) {
            ma_table_t *contact_map = NULL;

            MA_LOG(location_logger, MA_LOG_SEV_DEBUG, "pincode (%s) exist in pincode map", pincode);
            if(MA_OK == (err = ma_variant_get_table(pincodev, &contact_map))) {
                if(MA_OK == (err = ma_table_remove_entry(contact_map, contact))) {
                    MA_LOG(location_logger, MA_LOG_SEV_DEBUG, "contact (%s) removed from pincode contact list", contact);
                } else {
                    MA_LOG(location_logger, MA_LOG_SEV_DEBUG, "contact (%s) does not exit in  pincode contact list", contact);
                }
                (void)ma_table_release(contact_map);
            }
            (void)ma_variant_release(pincodev);
        } else {
            MA_LOG(location_logger, MA_LOG_SEV_DEBUG, "pincode (%s) does not exist in pincode map", pincode);
        }
        
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_location_info_add_contact(ma_location_info_t *info, const char *pincode, const char *contact) {
    if(!info || !pincode || !contact)return MA_ERROR_INVALIDARG;
    else {
        ma_error_t err = MA_OK;
        ma_variant_t *pincodev = NULL; 

        MA_LOG(location_logger, MA_LOG_SEV_DEBUG, "pincode (%s) exist in pincode map", pincode);
        if(MA_OK == (err = ma_table_get_value(info->pincodeMap, pincode, &pincodev))) {
            ma_table_t *contact_map = NULL;

            if(MA_OK == (err = ma_variant_get_table(pincodev, &contact_map))) {
                ma_variant_t *contactv = NULL;

                if(MA_OK == (err = ma_table_get_value(contact_map, contact, &contactv))) {
                    MA_LOG(location_logger, MA_LOG_SEV_DEBUG, "executive (%s) exist in executive map", contact);
                } else {
                    ma_variant_t *contactv = NULL;

                    if(MA_OK == (err = ma_variant_create(&contactv))) {
                        if(MA_OK == (err = ma_table_add_entry(contact_map, contact, contactv))) {
                            MA_LOG(location_logger, MA_LOG_SEV_DEBUG, "executive (%s) added in executive map", contact);
                        }
                        (void)ma_variant_release(contactv);
                    }
                }
                (void)ma_table_release(contact_map);
            }
            (void)ma_variant_release(pincodev);
        } else {
            ma_table_t *contact_map = NULL;

            MA_LOG(location_logger, MA_LOG_SEV_DEBUG, "pincode (%s) does not exist in pincode map", pincode);
            if(MA_OK == (err = ma_table_create(&contact_map))) {
                ma_variant_t *v = NULL;

                if(MA_OK == (err = ma_variant_create(&v))) {
                    if(MA_OK == (err = ma_table_add_entry(contact_map, contact, v))) {
                        MA_LOG(location_logger, MA_LOG_SEV_DEBUG, "executive (%s) added in executive map", contact);
                    }
                    (void)ma_variant_release(v);
                }
                if(MA_OK == (err = ma_variant_create_from_table(contact_map, &v))) {
                    if(MA_OK == (err = ma_table_add_entry(info->pincodeMap, pincode, v))) {
                        MA_LOG(location_logger, MA_LOG_SEV_DEBUG, "executive (%s) added in executive map for pincode(%s)", contact, pincode);
                    }
                    (void)ma_variant_release(v);
                }
                (void)ma_table_release(contact_map);
            }
        }
        return err;
    }
    return MA_OK;
}

ma_error_t ma_location_info_get_contact(ma_location_info_t *info, const char *pincode,  ma_variant_t **v) {
    if(info && pincode && v) {
        ma_error_t err = MA_OK;

        if(MA_OK == (err = ma_table_get_value(info->pincodeMap, pincode, &v))) {
            MA_LOG(location_logger, MA_LOG_SEV_DEBUG, "pincode(%s) exist", pincode);
        } else {
            MA_LOG(location_logger, MA_LOG_SEV_DEBUG, "pincode(%s) does not exist error(%d)", pincode, err);
        }
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

/* setter */
ma_error_t ma_location_info_set_contact_list(ma_location_info_t *info, ma_table_t *table) {
   return info && table ? ma_table_add_ref(info->pincodeMap = table) :  MA_ERROR_INVALIDARG;
}

ma_error_t ma_location_info_set_entry_date(ma_location_info_t *info, ma_task_time_t date) {
    return info && memcpy(&info->entry_date, &date, sizeof(ma_task_time_t)) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_location_info_set_entry(ma_location_info_t *info, const char *entry) {
    return info && entry && strncpy(info->entry, entry, MA_MAX_LEN) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_location_info_set_details(ma_location_info_t *info, const char *details) {
    return info && details && (strlen(details) < MA_MAX_BUFFER_LEN) && strcpy(info->details, details) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_location_info_set_email_id(ma_location_info_t *info, const char *id) {
    return info && id && strncpy(info->email_id, id, MA_MAX_LEN) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_location_info_set_pincode(ma_location_info_t *info, const char *id) {
    return info && id && strncpy(info->pincode, id, MA_MAX_LEN) ? MA_OK : MA_ERROR_INVALIDARG;
}
ma_error_t ma_location_info_set_date(ma_location_info_t *info, ma_task_time_t date) {
    return info && memcpy(&info->date, &date, sizeof(ma_task_time_t)) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_location_info_set_type(ma_location_info_t *info, ma_location_info_type_t type) {
    return info && type && (info->type = type) ? MA_OK : MA_ERROR_INVALIDARG;
}

/* getters */
ma_error_t ma_location_info_get_contact_list(ma_location_info_t *info, ma_table_t **table) {
    return info && table ? ma_table_add_ref(*table = info->pincodeMap) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_location_info_get_details(ma_location_info_t *info, char *details) {
    return info && details && strcpy(details, info->details) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_location_info_get_entry(ma_location_info_t *info, char *entry) {
    return info && entry && strncpy(entry, info->entry, MA_MAX_LEN) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_location_info_get_entry_date(ma_location_info_t *info, ma_task_time_t *date) {
    return info && date && memcpy(date, &info->entry_date, sizeof(ma_task_time_t)) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_location_info_get_type(ma_location_info_t *info, ma_location_info_type_t *type) {
    return info && type && (*type = info->type) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_location_info_get_email_id(ma_location_info_t *info, char *id) {
    return info && id && strncpy(id, info->email_id, MA_MAX_LEN) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_location_info_get_pincode(ma_location_info_t *info, char *id) {
    return info && id && strncpy(id, info->pincode, MA_MAX_LEN) ? MA_OK : MA_ERROR_INVALIDARG;
}
ma_error_t ma_location_info_get_date(ma_location_info_t *info, ma_task_time_t *date) {
    return info && date && memcpy(date, &info->date, sizeof(ma_task_time_t)) ? MA_OK : MA_ERROR_INVALIDARG;
}



ma_error_t ma_location_info_convert_to_variant(ma_location_info_t *info, ma_variant_t **variant) {
    if(info && variant) {
        ma_error_t err = MA_OK;
        ma_table_t *info_table = NULL;

        if(MA_OK == (err = ma_table_create(&info_table))) {
            {
                 char value[MA_MAX_BUFFER_LEN+1] = {0};

                 if(MA_OK == (err = ma_location_info_get_email_id(info, value))) {
                     ma_variant_t *v =NULL;

                     if(MA_OK == (err = ma_variant_create_from_string(value, strlen(value), &v))) {
                         (void)ma_table_add_entry(info_table, MA_LOCATION_INFO_ATTR_EMAIL_ID, v);
                         (void)ma_variant_release(v);
                     }
                 }
            }
            {
                 char value[MA_MAX_BUFFER_LEN+1] = {0};

                 if(MA_OK == (err = ma_location_info_get_pincode(info, value))) {
                     ma_variant_t *v =NULL;

                     if(MA_OK == (err = ma_variant_create_from_string(value, strlen(value), &v))) {
                         (void)ma_table_add_entry(info_table, MA_LOCATION_INFO_ATTR_PINCODE, v);
                         (void)ma_variant_release(v);
                     }
                 }
            }
            {
                 ma_task_time_t date = {0};

                 if(MA_OK == (err = ma_location_info_get_date(info, &date))) {
                     ma_variant_t *v = NULL;

                     if(MA_OK == (err = ma_variant_create_from_raw(&date, sizeof(date), &v))) {
                         (void)ma_table_add_entry(info_table, MA_LOCATION_INFO_ATTR_DATE, v);
                         (void)ma_variant_release(v);
                     }
                 }
            }
             {
                 ma_location_info_type_t value;

                 if(MA_OK == (err = ma_location_info_get_type(info, &value))) {
                     ma_variant_t *v = NULL;

                     if(MA_OK == (err = ma_variant_create_from_uint32(value, &v))) {
                         (void)ma_table_add_entry(info_table, MA_LOCATION_INFO_ATTR_TYPE, v);
                         (void)ma_variant_release(v);
                     }
                 }
             }
            {
                 ma_task_time_t date = {0};

                 if(MA_OK == (err = ma_location_info_get_entry_date(info, &date))) {
                     ma_variant_t *v = NULL;

                     if(MA_OK == (err = ma_variant_create_from_raw(&date, sizeof(date), &v))) {
                         (void)ma_table_add_entry(info_table, MA_LOCATION_INFO_ATTR_ENTRY_DATE, v);
                         (void)ma_variant_release(v);
                     }
                 }
            }
            {
                 char value[MA_MAX_BUFFER_LEN+1] = {0};

                 if(MA_OK == (err = ma_location_info_get_entry(info, value))) {
                     ma_variant_t *v =NULL;

                     if(MA_OK == (err = ma_variant_create_from_string(value, strlen(value), &v))) {
                         (void)ma_table_add_entry(info_table, MA_LOCATION_INFO_ATTR_ENTRY, v);
                         (void)ma_variant_release(v);
                     }
                 }
            }
            {
                 char value[MA_MAX_BUFFER_LEN+1] = {0};

                 if(MA_OK == (err = ma_location_info_get_details(info, value))) {
                     ma_variant_t *v =NULL;

                     if(MA_OK == (err = ma_variant_create_from_string(value, strlen(value), &v))) {
                         (void)ma_table_add_entry(info_table, MA_LOCATION_INFO_ATTR_ENTRY_DETAILS, v);
                         (void)ma_variant_release(v);
                     }
                 }
            }
            {
                 ma_table_t *table = NULL;

                 if(MA_OK == (err = ma_location_info_get_contact_list(info, &table))) {
                     ma_variant_t *v =NULL;

                     if(MA_OK == (err = ma_variant_create_from_table(table, &v))) {
                         (void)ma_table_add_entry(info_table, MA_LOCATION_INFO_ATTR_CONTACTS_LIST, v);
                         (void)ma_variant_release(v);
                     }
                     (void)ma_table_release(table);
                 }
            }

             err = ma_variant_create_from_table(info_table, variant);
             (void)ma_table_release(info_table);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_location_info_convert_from_variant(ma_variant_t *variant, ma_location_info_t **info) {
    if(info && variant) {
        ma_error_t err = MA_OK;

        if(MA_OK == (err = ma_location_info_create(info))) {
            ma_table_t *info_table = NULL;

            if(MA_OK == (err = ma_variant_get_table(variant, &info_table))) {
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_LOCATION_INFO_ATTR_EMAIL_ID, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                (void)ma_location_info_set_email_id((*info), pstr);
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_LOCATION_INFO_ATTR_PINCODE, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                (void)ma_location_info_set_pincode((*info), pstr);
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_LOCATION_INFO_ATTR_DATE, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_raw_buffer(v, &buf))) {
                            ma_task_time_t *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_raw(buf, (const unsigned char**)&pstr, &len))) {
                                (void)ma_location_info_set_date((*info), *pstr);
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_LOCATION_INFO_ATTR_TYPE, &v))) {
                        ma_location_info_type_t value = 0;

                        if(MA_OK == (err = ma_variant_get_uint32(v, &value)))
                            (void)ma_location_info_set_type((*info), (ma_location_info_type_t)value);
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_LOCATION_INFO_ATTR_CONTACTS_LIST, &v))) {
                        ma_table_t *value = NULL;

                        if(MA_OK == (err = ma_variant_get_table(v, &value))) {
                            (void)ma_location_info_set_contact_list((*info),  value);
                            (void)ma_table_release(value);
                        }
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_LOCATION_INFO_ATTR_ENTRY_DATE, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_raw_buffer(v, &buf))) {
                            ma_task_time_t *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_raw(buf, (const unsigned char**)&pstr, &len))) {
                                (void)ma_location_info_set_entry_date((*info), *pstr);
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_LOCATION_INFO_ATTR_ENTRY, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                (void)ma_location_info_set_entry((*info), pstr);
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_LOCATION_INFO_ATTR_ENTRY_DETAILS, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                (void)ma_location_info_set_details((*info), pstr);
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    }
                }

                (void)ma_table_release(info_table);
            }
        }

        return MA_OK; /* intentional */
    }
    return MA_ERROR_INVALIDARG;
}

