#include "ma/internal/utils/location/ma_location.h"
#include "ma/internal/utils/location/ma_location_info.h"
#include "ma/internal/utils/location/ma_location_datastore.h"
#include "ma/ma_msgbus.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/defs/ma_location_defs.h"
#include "ma/internal/ma_macros.h"

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "location"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <uv.h>

ma_logger_t *location_logger;
ma_location_t *locator;

struct ma_location_s {
    char path[MA_MAX_PATH_LEN+1];
    ma_msgbus_t *msgbus;
    ma_ds_t* datastore;
};


ma_error_t ma_location_create(ma_msgbus_t *msgbus, ma_ds_t *datastore, const char *base_path, ma_location_t **location) {
    if(msgbus && datastore && base_path && location) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;

        if((*location = (ma_location_t*)calloc(1, sizeof(ma_location_t)))) {
            (*location)->msgbus = msgbus;
            strncpy((*location)->path, base_path, MA_MAX_PATH_LEN);
            (*location)->datastore = datastore;
            err = MA_OK;
            MA_LOG(location_logger, MA_LOG_SEV_INFO, "location created successfully");
        }
        if(MA_OK != err) {
            MA_LOG(location_logger, MA_LOG_SEV_ERROR, "location creation failed, last error(%d)", err);
            if(*location)
                (void)ma_location_release(*location);
            *location = NULL;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_location_add(ma_location_t *location, ma_location_info_t *info) {
    return location && info ? ma_location_datastore_write(location, info, location->datastore, location->path) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_location_add_variant(ma_location_t *location, const char *location_id, ma_variant_t *var) {
    return location && location_id && var ? ma_location_datastore_write_variant(location, location_id, location->datastore, location->path, var) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_location_delete_crn(ma_location_t *location, const char *location_id, const char *crn) {
    return location && location_id && crn ? ma_location_datastore_remove_location_crn(location, location_id, crn, location->datastore, location->path) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_location_delete(ma_location_t *location, const char *location_id) {
    return location && location_id ? ma_location_datastore_remove_location(location_id, location->datastore, location->path) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_location_enumerate_order_until_date(ma_location_t *location, const char *from, const char *to, ma_variant_t **var) {
   return location && from && to && var ? ma_location_datastore_enumerate_order_until_date(location, location->datastore, location->path,from, to, var) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_location_delete_all(ma_location_t *location) {
    return location ? ma_location_datastore_clear(location->datastore, location->path) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_location_get(ma_location_t *location, const char *location_id, ma_location_info_t **info) {
    return location && location_id && info ? ma_location_datastore_get(location, location->datastore, location->path, location_id, info) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_location_get_all(ma_location_t *location, const char *location_id, ma_variant_t **var) {
    return location && location_id && var ? ma_location_datastore_get_all(location, location->datastore, location->path, location_id, var) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_location_set_logger(ma_logger_t *logger) {
    if(logger) {
        location_logger = logger;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_location_get_msgbus(ma_location_t *location, ma_msgbus_t **msgbus) {
    if(location && msgbus) {
        *msgbus = location->msgbus;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_location_start(ma_location_t *location) {
    if(location) {
        MA_LOG(location_logger, MA_LOG_SEV_TRACE, "location started");
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_location_stop(ma_location_t *location) {
    if(location) {
        MA_LOG(location_logger, MA_LOG_SEV_TRACE, "location stopped");
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_location_release(ma_location_t *location) {
    if(location) {
        free(location);
    }
    return MA_ERROR_INVALIDARG;
}

