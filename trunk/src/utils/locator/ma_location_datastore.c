#include "ma/internal/utils/location/ma_location_datastore.h"
#include "ma/internal/defs/ma_location_defs.h"
#include "ma/scheduler/ma_enumerator.h"
#include "ma/internal/ma_macros.h"
#include "ma/ma_log.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "location"

extern ma_logger_t *location_logger;

#define BOOKING_DATA MA_DATASTORE_PATH_SEPARATOR "location_data"
#define BOOKING_ID MA_DATASTORE_PATH_SEPARATOR "location_id"

#define BOOKING_DATA_PATH BOOKING_DATA BOOKING_ID MA_DATASTORE_PATH_SEPARATOR/* \"location_data"\"location_id"\ */


ma_error_t ma_location_datastore_read(ma_location_t *location, ma_ds_t *datastore, const char *ds_path) {
    if(location && datastore && ds_path) {
        ma_error_t err = MA_OK;
        ma_ds_iterator_t *location_iterator = NULL;
        ma_buffer_t *location_id_buf = NULL;
        char location_data_base_path[MA_MAX_PATH_LEN] = {0};

        MA_MSC_SELECT(_snprintf, snprintf)(location_data_base_path, MA_MAX_PATH_LEN, "%s%s", ds_path, BOOKING_DATA_PATH);
        MA_LOG(location_logger, MA_LOG_SEV_DEBUG, "location data store path(%s) reading...", location_data_base_path);
        if(MA_OK == (err = ma_ds_iterator_create(datastore, 0, location_data_base_path, &location_iterator))) {
            while(MA_ERROR_NO_MORE_ITEMS != ma_ds_iterator_get_next(location_iterator, &location_id_buf) && location_id_buf) {
                const char *location_id = NULL;
                size_t size = 0;
                
                if(MA_OK == (err = ma_buffer_get_string(location_id_buf, &location_id, &size))) {
                    char location_data_path[MA_MAX_PATH_LEN] = {0};
                    ma_variant_t *location_var = NULL;
                    MA_MSC_SELECT(_snprintf, snprintf)(location_data_path, MA_MAX_PATH_LEN, "%s", location_data_base_path);/* base_path\"location_data"\"location_id"\4097 */
                    if(MA_OK == (err = ma_ds_get_variant(datastore, location_data_path, location_id, MA_VARTYPE_TABLE, &location_var))) {

                        (void)ma_variant_release(location_var);
                    }
                }
                (void)ma_buffer_release(location_id_buf);
            }
            (void)ma_ds_iterator_release(location_iterator);
        }

        return ((MA_ERROR_DS_PATH_NOT_FOUND == err) || (MA_ERROR_SCHEDULER_TASK_ID_CONFLICT == err)) ? MA_OK : err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_location_datastore_enumerate_order_until_date(ma_location_t *location, ma_ds_t *datastore, const char *ds_path, const char *from, const char *to, ma_variant_t **ret_var) {
    if(location && datastore && ds_path && from && to && ret_var) {
         ma_error_t err = MA_OK;
         ma_table_t *location_table = NULL;
         size_t location_table_size = 0;

         if(MA_OK == (err = ma_table_create(&location_table))) {
             ma_ds_iterator_t *location_iterator = NULL;
             ma_buffer_t *location_id_buf = NULL;
             char location_data_base_path[MA_MAX_PATH_LEN] = {0};

             MA_MSC_SELECT(_snprintf, snprintf)(location_data_base_path, MA_MAX_PATH_LEN, "%s%s", ds_path, BOOKING_DATA_PATH);
             MA_LOG(location_logger, MA_LOG_SEV_DEBUG, "location data store path(%s) reading...", location_data_base_path);
             if(MA_OK == (err = ma_ds_iterator_create(datastore, 0, location_data_base_path, &location_iterator))) {
                 while(MA_ERROR_NO_MORE_ITEMS != ma_ds_iterator_get_next(location_iterator, &location_id_buf) && location_id_buf) {
                     const char *location_id = NULL;
                     size_t size = 0;

                     (void)ma_buffer_get_string(location_id_buf, &location_id, &size);
                     MA_LOG(location_logger, MA_LOG_SEV_TRACE, "location id(%s) reading from(%s) to(%s) date...", location_id, from, to);
                     if(strcmp(location_id, from) >= 0 && strcmp(location_id, to) <= 0) {
                         char location_data_path[MA_MAX_PATH_LEN] = {0};
                         ma_variant_t *location_var = NULL;
                         MA_MSC_SELECT(_snprintf, snprintf)(location_data_path, MA_MAX_PATH_LEN, "%s", location_data_base_path);/* base_path\"location_data"\"location_id"\4097 */
                         MA_LOG(location_logger, MA_LOG_SEV_TRACE, "location path(%s) id(%s) reading...", location_data_path, location_id);
                         if(MA_OK == (err = ma_ds_get_variant(datastore, location_data_path, location_id, MA_VARTYPE_TABLE, &location_var))) {
                             ma_table_t *book_entry = NULL;

                             if(MA_OK == (err = ma_variant_get_table(location_var, &book_entry))) {
                                 ma_array_t *arr = NULL;

                                 if(MA_OK == (err = ma_table_get_keys(book_entry, &arr))) {
                                     size_t arr_size = 0, i = 0;

                                     (void)ma_array_size(arr, &arr_size);
                                     for(i = 1; i <= arr_size; ++i) {
                                         ma_variant_t *keyv = NULL;

                                         if(MA_OK == (err = ma_array_get_element_at(arr, i, &keyv))) {
                                             ma_buffer_t *buf = NULL;

                                             if(MA_OK == (err = ma_variant_get_string_buffer(keyv, &buf))) {
                                                 const char *key = NULL; size_t key_len = 0;

                                                 if(MA_OK == (err = ma_buffer_get_string(buf, &key, &key_len))) {
                                                     ma_variant_t *location_value = NULL;

                                                     MA_LOG(location_logger, MA_LOG_SEV_TRACE, "location id(%s) key(%s)", location_id, key);
                                                     if(MA_OK == (err = ma_table_get_value(book_entry, key, &location_value))) {
                                                         ma_location_info_t *book_info = NULL;

                                                         if(MA_OK == (err = ma_location_info_convert_from_variant(location_value, &book_info))) {
                                                             char mail_id[MA_MAX_LEN+1] = {0};
                                                             ma_location_info_type_t type = MA_LOCATION_INFO_TYPE_ORDER;

                                                             (void)ma_location_info_get_email_id(book_info, mail_id);
                                                             (void)ma_location_info_get_type(book_info, &type);
                                                             if(MA_LOCATION_INFO_TYPE_ORDER == type) {
                                                                 ma_variant_t *v = NULL;

                                                                 if(MA_OK == (err = ma_variant_create_from_string(mail_id, strlen(mail_id), &v))) {
                                                                     char entry[MA_MAX_LEN+1] = {0};

                                                                     (void)ma_location_info_get_entry(book_info, entry);
                                                                     MA_LOG(location_logger, MA_LOG_SEV_TRACE, "location id(%s) entry(%s)  matched", mail_id, entry);
                                                                     (void)ma_table_add_entry(location_table, entry, v);
                                                                     (void)ma_variant_release(v);
                                                                 }
                                                             }

                                                             (void)ma_location_info_release(book_info);
                                                         }
                                                         (void)ma_variant_release(location_value);
                                                     }
                                                 }
                                                 (void)ma_buffer_release(buf);
                                             }
                                             (void)ma_variant_release(keyv);
                                         }
                                     }
                                     (void)ma_array_release(arr);
                                 }
                                 (void)ma_table_release(book_entry);
                             }
                             (void)ma_variant_release(location_var);
                         }
                     }
                     (void)ma_buffer_release(location_id_buf);
                 }
                 (void)ma_ds_iterator_release(location_iterator);
             }


             (void)ma_table_size(location_table, &location_table_size);
             if(location_table_size) {
                 err = ma_variant_create_from_table(location_table, ret_var);
             }
             (void)ma_table_release(location_table);
         }

         return err;
    }
    return MA_ERROR_INVALIDARG;
}



ma_error_t ma_location_datastore_write(ma_location_t *location, ma_location_info_t *info, ma_ds_t *datastore, const char *ds_path) {
    if(location && info && datastore && ds_path) {
        char location_data_path[MA_MAX_PATH_LEN] = {0};
        char location_id[MA_MAX_LEN+1] = {0};
        ma_error_t err = MA_OK;
        ma_variant_t *var = NULL;

        (void)ma_location_info_get_pincode(info, location_id);
        MA_MSC_SELECT(_snprintf, snprintf)(location_data_path, MA_MAX_PATH_LEN, "%s%s", ds_path, BOOKING_DATA_PATH);
        MA_LOG(location_logger, MA_LOG_SEV_INFO, "location id(%s), data path(%s) location_data_path(%s)", location_id, ds_path, location_data_path);
        if(MA_OK == (err = ma_location_info_convert_to_variant(info, &var))) {
            if(MA_OK != (err = ma_ds_set_variant(datastore, location_data_path, location_id, var)))
                MA_LOG(location_logger, MA_LOG_SEV_DEBUG, "location datastore write location id(%s) failed", location_id);
            (void)ma_variant_release(var);
        } else
            MA_LOG(location_logger, MA_LOG_SEV_DEBUG, "location datastore write location id(%s) conversion location info to variant failed, last error(%d)", location_id, err);
#if 0
        char location_data_path[MA_MAX_PATH_LEN] = {0};
        char location_id[MA_MAX_LEN+1] = {0};
        ma_error_t err = MA_OK;
        ma_variant_t *var = NULL;
        ma_variant_t *bookv = NULL;
        ma_table_t *location_table = NULL;
        ma_task_time_t date = {0};
        ma_task_time_t entry_date = {0};
        char crn[MA_MAX_LEN+1] = {0};

        (void)ma_location_info_get_date(info, &date);
        ma_strtime(date, location_id);
        MA_MSC_SELECT(_snprintf, snprintf)(location_data_path, MA_MAX_PATH_LEN, "%s%s", ds_path, BOOKING_DATA_PATH);
        MA_LOG(location_logger, MA_LOG_SEV_INFO, "location id(%s), data path(%s) location_data_path(%s)", location_id, ds_path, location_data_path);
        (void)ma_location_info_get_entry_date(info, &entry_date);
        MA_MSC_SELECT(_snprintf, snprintf)(crn, MA_MAX_LEN, "%hu-%hu-%hu:%hu:%hu:%hu", entry_date.month, entry_date.day, entry_date.year, entry_date.hour, entry_date.minute, entry_date.secs);
        if(MA_OK == (err = ma_ds_get_variant(datastore, location_data_path, location_id, MA_VARTYPE_TABLE, &bookv))) {
            if(MA_OK == (err = ma_variant_get_table(bookv, &location_table))) {
                if(MA_OK == (err = ma_location_info_convert_to_variant(info, &var))) {
                    if(MA_OK == (err = ma_table_add_entry(location_table, crn, var))) {
                        if(MA_OK == (err = ma_ds_set_variant(datastore, location_data_path, location_id, bookv)))
                            MA_LOG(location_logger, MA_LOG_SEV_TRACE, "location datastore write location id(%s) for crn(%s) successful", location_id, crn);
                        else
                            MA_LOG(location_logger, MA_LOG_SEV_ERROR, "location datastore write location id(%s) failed for crn(%s), last error(%d)", location_id, crn, err);
                    }
                    (void)ma_variant_release(var);
                } else
                    MA_LOG(location_logger, MA_LOG_SEV_ERROR, "location datastore write location id(%s) conversion location info to variant failed for crn(%s), last error(%d)", location_id, crn, err);
                (void)ma_table_release(location_table);
            }
            (void)ma_variant_release(bookv);
        } else {
            MA_LOG(location_logger, MA_LOG_SEV_DEBUG, "location table for location id(%s) does not exist, so creating..", location_id);
            if(MA_OK == (err = ma_table_create(&location_table))) {
                if(MA_OK == (err = ma_location_info_convert_to_variant(info, &var))) {
                    if(MA_OK == (err = ma_table_add_entry(location_table, crn, var))) {
                        if(MA_OK == (err = ma_variant_create_from_table(location_table, &bookv))) {
                            if(MA_OK == (err = ma_ds_set_variant(datastore, location_data_path, location_id, bookv)))
                                MA_LOG(location_logger, MA_LOG_SEV_TRACE, "location datastore write location id(%s) for crn(%s) successful", location_id, crn);
                            else
                                MA_LOG(location_logger, MA_LOG_SEV_ERROR, "location datastore write location id(%s) failed for crn(%s), last error(%d)", location_id, crn, err);
                            (void)ma_variant_release(bookv);
                        }
                    }
                    (void)ma_variant_release(var);
                } else
                    MA_LOG(location_logger, MA_LOG_SEV_ERROR, "location datastore write location id(%s) conversion location info to variant failed for crn(%s), last error(%d)", location_id, crn, err);
                (void)ma_table_release(location_table);
            }
        }
#endif

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_location_datastore_write_variant(ma_location_t *location, const char *location_id, ma_ds_t *datastore, const char *ds_path, ma_variant_t *var) {
    if(location && location_id && var && datastore && ds_path) {
        char location_data_path[MA_MAX_PATH_LEN] = {0};
        ma_error_t err = MA_OK;

        MA_MSC_SELECT(_snprintf, snprintf)(location_data_path, MA_MAX_PATH_LEN, "%s%s", ds_path, BOOKING_DATA_PATH);
        MA_LOG(location_logger, MA_LOG_SEV_INFO, "location id(%s), data path(%s) location_data_path(%s)", location_id, ds_path, location_data_path);
        if(MA_OK == (err = ma_ds_set_variant(datastore, location_data_path, location_id, var)))
            MA_LOG(location_logger, MA_LOG_SEV_INFO, "location datastore write location id(%s) success", location_id);
        else
            MA_LOG(location_logger, MA_LOG_SEV_ERROR, "location datastore write location id(%s) failed, last error(%d)", location_id, err);

#if 0
        char location_data_path[MA_MAX_PATH_LEN] = {0};
        ma_error_t err = MA_OK;
        ma_variant_t *bookv = NULL;
        ma_table_t *location_table = NULL;
        char crn[MA_MAX_LEN+1] = {0};
        ma_task_time_t entry_date = {0};
        ma_location_info_t *info = NULL;

        MA_MSC_SELECT(_snprintf, snprintf)(location_data_path, MA_MAX_PATH_LEN, "%s%s", ds_path, BOOKING_DATA_PATH);
        MA_LOG(location_logger, MA_LOG_SEV_INFO, "location id(%s), data path(%s) location_data_path(%s)", location_id, ds_path, location_data_path);
        if(MA_OK == (err = ma_ds_get_variant(datastore, location_data_path, location_id, MA_VARTYPE_TABLE, &bookv))) {
            if(MA_OK == (err = ma_variant_get_table(bookv, &location_table))) {
                if(MA_OK == (err = ma_location_info_convert_from_variant(var, &info))) {
                    (void)ma_location_info_get_entry_date(info, &entry_date);
                    MA_MSC_SELECT(_snprintf, snprintf)(crn, MA_MAX_LEN, "%hu-%hu-%hu:%hu:%hu:%hu", entry_date.month, entry_date.day, entry_date.year, entry_date.hour, entry_date.minute, entry_date.secs);
                    if(MA_OK == (err = ma_table_add_entry(location_table, crn, var))) {
                        if(MA_OK == (err = ma_ds_set_variant(datastore, location_data_path, location_id, bookv)))
                            MA_LOG(location_logger, MA_LOG_SEV_TRACE, "location datastore write location id(%s) for crn(%s) successful", location_id, crn);
                        else
                            MA_LOG(location_logger, MA_LOG_SEV_ERROR, "location datastore write location id(%s) failed for crn(%s), last error(%d)", location_id, crn, err);
                    }
                    (void)ma_location_info_release(info);
                } else
                    MA_LOG(location_logger, MA_LOG_SEV_ERROR, "location datastore write location id(%s) conversion location info to variant failed for crn(%s), last error(%d)", location_id, crn, err);
                (void)ma_table_release(location_table);
            }
            (void)ma_variant_release(bookv);
        } else {
            MA_LOG(location_logger, MA_LOG_SEV_DEBUG, "location table for location id(%s) does not exist, so creating..", location_id);
            if(MA_OK == (err = ma_table_create(&location_table))) {
                if(MA_OK == (err = ma_location_info_convert_from_variant(var, &info))) {
                    (void)ma_location_info_get_entry_date(info, &entry_date);
                    MA_MSC_SELECT(_snprintf, snprintf)(crn, MA_MAX_LEN, "%hu-%hu-%hu:%hu:%hu:%hu", entry_date.month, entry_date.day, entry_date.year, entry_date.hour, entry_date.minute, entry_date.secs);
                    if(MA_OK == (err = ma_table_add_entry(location_table, crn, var))) {
                        if(MA_OK == (err = ma_variant_create_from_table(location_table, &bookv))) {
                            if(MA_OK == (err = ma_ds_set_variant(datastore, location_data_path, location_id, bookv)))
                                MA_LOG(location_logger, MA_LOG_SEV_TRACE, "location datastore write location id(%s) for crn(%s) successful", location_id, crn);
                            else
                                MA_LOG(location_logger, MA_LOG_SEV_ERROR, "location datastore write location id(%s) failed for crn(%s), last error(%d)", location_id, crn, err);
                            (void)ma_variant_release(bookv);
                        }
                    }
                    (void)ma_location_info_release(info);
                } else
                    MA_LOG(location_logger, MA_LOG_SEV_ERROR, "location datastore write location id(%s) conversion location info from variant failed, last error(%d)", location_id, err);
                (void)ma_table_release(location_table);
            }
        }
#endif
        return err;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_location_datastore_remove_location(const char *location, ma_ds_t *datastore, const char *ds_path) {
    if(location && datastore && ds_path) {
        char location_path[MA_MAX_PATH_LEN] = {0};
        
        MA_LOG(location_logger, MA_LOG_SEV_DEBUG, "location removing (%s) from datastore", location);
        MA_MSC_SELECT(_snprintf, snprintf)(location_path, MA_MAX_PATH_LEN, "%s%s",ds_path, BOOKING_DATA_PATH );/* base_path\"location_data"\4097 */
        return ma_ds_rem(datastore, location_path, location, 1);
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_location_datastore_get(ma_location_t *location, ma_ds_t *datastore, const char *ds_path, const char *location_id,  ma_location_info_t **info) {
    if(location && datastore && ds_path && location_id && info) {
        ma_error_t err = MA_OK;
        char location_data_base_path[MA_MAX_PATH_LEN] = {0};
        ma_variant_t *location_var = NULL;

        MA_MSC_SELECT(_snprintf, snprintf)(location_data_base_path, MA_MAX_PATH_LEN, "%s%s", ds_path, BOOKING_DATA_PATH);
        if(MA_OK == (err = ma_ds_get_variant(datastore, location_data_base_path, location_id, MA_VARTYPE_TABLE, &location_var))) {
            if(MA_OK == (err = ma_location_info_convert_from_variant(location_var, info)))
                MA_LOG(location_logger, MA_LOG_SEV_TRACE, "location id(%s) exist in location DB", location_id);
            (void)ma_variant_release(location_var);
        } else 
            MA_LOG(location_logger, MA_LOG_SEV_ERROR, "location id(%s) does not exist in location DB, last error(%d)", location_id, err);

#if 0
        ma_error_t err = MA_OK;
        char location_data_base_path[MA_MAX_PATH_LEN] = {0};
        ma_variant_t *location_var = NULL;

        MA_MSC_SELECT(_snprintf, snprintf)(location_data_base_path, MA_MAX_PATH_LEN, "%s%s", ds_path, BOOKING_DATA_PATH);
        if(MA_OK == (err = ma_ds_get_variant(datastore, location_data_base_path, location_id, MA_VARTYPE_TABLE, &location_var))) {
            ma_table_t *location_table = NULL;

            if(MA_OK == (err = ma_variant_get_table(location_var, &location_table))) {
                ma_variant_t *bookv = NULL;

                if(MA_OK == (err = ma_table_get_value(location_table, crn, &bookv))) {
                    if(MA_OK == (err = ma_location_info_convert_from_variant(bookv, info)))
                        MA_LOG(location_logger, MA_LOG_SEV_TRACE, "location(%s) exist in location DB", location_id);
                    else
                        MA_LOG(location_logger, MA_LOG_SEV_ERROR, "location(%s) variant to location conversion failed, last error(%d)", crn, err);
                    (void)ma_variant_release(bookv);
                } else
                    MA_LOG(location_logger, MA_LOG_SEV_ERROR, "location id(%s) crn(%s) does not exist, last error(%d)", location_id, crn, err);
                (void)ma_table_release(location_table);
            }
            (void)ma_variant_release(location_var);
        } else 
            MA_LOG(location_logger, MA_LOG_SEV_ERROR, "location(%s) does not exist in location DB, last error(%d)", location_id, err);

#endif
        return err;
    }
    return MA_ERROR_INVALIDARG;

}

ma_error_t ma_location_datastore_clear(ma_ds_t *datastore, const char *ds_path) {
    return(datastore && ds_path) ? ma_ds_rem(datastore, ds_path, NULL, 1) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_location_datastore_get_all(ma_location_t *location, ma_ds_t *datastore, const char *ds_path, const char *location_id, ma_variant_t **var) {
    if(location && datastore && ds_path && location_id && var) {
        ma_error_t err = MA_OK;
        char location_data_base_path[MA_MAX_PATH_LEN] = {0};

        MA_MSC_SELECT(_snprintf, snprintf)(location_data_base_path, MA_MAX_PATH_LEN, "%s%s", ds_path, BOOKING_DATA_PATH);
        if(MA_OK == (err = ma_ds_get_variant(datastore, location_data_base_path, location_id, MA_VARTYPE_TABLE, var))) {
            MA_LOG(location_logger, MA_LOG_SEV_TRACE, "location id(%s) DB get successful", location_id);
        } else
            MA_LOG(location_logger, MA_LOG_SEV_ERROR, "location id(%s) does not exist in DB, last error(%d)", location_id, err);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_location_datastore_remove_location_crn(ma_location_t *location, const char *location_id, const char *crn, ma_ds_t *datastore, const char *ds_path) {
    if(location && location_id && crn && datastore && ds_path) {
         ma_error_t err = MA_OK;
         ma_variant_t *var = NULL;

         if(MA_OK == (err = ma_location_datastore_get_all(location, datastore, ds_path, location_id, &var))) {
             ma_table_t *location_table = NULL;

             if(MA_OK == (err = ma_variant_get_table(var, &location_table))) {
                 if(MA_OK == (err = ma_table_remove_entry(location_table,crn))) {
                     char location_data_path[MA_MAX_PATH_LEN] = {0};

                     MA_LOG(location_logger, MA_LOG_SEV_TRACE, "location id(%s) crn (%s) removed successfully", location_id, crn);
                     MA_MSC_SELECT(_snprintf, snprintf)(location_data_path, MA_MAX_PATH_LEN, "%s%s", ds_path, BOOKING_DATA_PATH);
                     if(MA_OK == (err = ma_ds_set_variant(datastore, location_data_path, location_id, var)))
                         MA_LOG(location_logger, MA_LOG_SEV_TRACE, "location id(%s) updated successfully", location_id);
                     else
                         MA_LOG(location_logger, MA_LOG_SEV_ERROR, "location id(%s) update fialed, last error(%d)", location_id, err);
                 } else
                     MA_LOG(location_logger, MA_LOG_SEV_ERROR, "location id(%s) crn(%s) does not exist in DB, last error(%d)", location_id ,crn, err);
                 (void)ma_table_release(location_table);
             }
             (void)ma_variant_release(var);
         }

         return err;
    }
    return MA_ERROR_INVALIDARG;
}

