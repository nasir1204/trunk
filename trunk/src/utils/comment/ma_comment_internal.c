#include "ma/internal/utils/comment/ma_comment_internal.h"
#include "ma/internal/utils/threading/ma_atomic.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/defs/ma_comment_defs.h"
#include "ma/ma_log.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

extern ma_logger_t *comment_logger;

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "comment"

struct ma_comment_internal_s {
    char id[MA_MAX_NAME+1];
    char email[MA_MAX_LEN+1];
    char comment[MA_MAX_BUFFER_LEN+1];
    char date[MA_MAX_NAME+1];
    ma_atomic_counter_t ref_count;
    ma_json_t *json;
};


ma_error_t ma_comment_internal_create(ma_comment_internal_t **info) {
    if(info) {
         ma_error_t err = MA_ERROR_OUTOFMEMORY;

         if((*info = (ma_comment_internal_t*)calloc(1, sizeof(ma_comment_internal_t)))) {
             if(MA_OK == (err = ma_json_create(&(*info)->json))) {
                 (*info)->ref_count++;
             }
         }

         return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_comment_internal_release(ma_comment_internal_t *info) {
    if(info) {
        if(!--info->ref_count) {
            (void)ma_json_release(info->json);
            free(info);
        }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_comment_internal_add(ma_comment_internal_t *info) {
    if(info) {
        info->ref_count++;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


/* setter */
ma_error_t ma_comment_internal_set_ref_count(ma_comment_internal_t *info, size_t count) {
    if(info) {
        info->ref_count = count;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_comment_internal_set_email(ma_comment_internal_t *info, const char *id) {
    if(info && id && (strlen(id) < MA_MAX_LEN)) {
        strncpy(info->email, id, MA_MAX_LEN);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_comment_internal_set_id(ma_comment_internal_t *info, const char *id) {
    if(info && id && (strlen(id) < MA_MAX_NAME)) {
        strncpy(info->id, id, MA_MAX_NAME);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_comment_internal_set_date(ma_comment_internal_t *info, const char *date) {
    if(info && date && (strlen(date) <  MA_MAX_NAME)) {
        strncpy(info->date, date, MA_MAX_NAME);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_comment_internal_set_comment(ma_comment_internal_t *info, const char *comment) {
    if(info && comment && (strlen(comment) < MA_MAX_BUFFER_LEN)) {
        strncpy(info->comment, comment, MA_MAX_BUFFER_LEN);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


/* getter */

ma_error_t ma_comment_internal_get_ref_count(ma_comment_internal_t *info, size_t *count) {
    if(info && count) {
        *count = info->ref_count;
        return MA_OK;
    }
    return MA_OK;
}

ma_error_t ma_comment_internal_get_email(ma_comment_internal_t *info, const char **id) {
    if(info && id) {
        *id = info->email;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_comment_internal_get_id(ma_comment_internal_t *info, const char **id) {
    if(info && id) {
        *id = info->id;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_comment_internal_get_date(ma_comment_internal_t *info, const char **date) {
    if(info && date) {
        *date = info->date;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_comment_internal_get_comment(ma_comment_internal_t *info, const char **comment) {
    if(info && comment) {
        *comment = info->comment;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_comment_internal_get_json(ma_comment_internal_t *info, ma_json_t **json) {
    if(info && json) {
        *json = info->json;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_comment_internal_convert_to_variant(ma_comment_internal_t *info, ma_variant_t **variant) {
    if(info && variant) {
        ma_error_t err = MA_OK;
        ma_table_t *info_table = NULL;

        if(MA_OK == (err = ma_table_create(&info_table))) {
             {
                 const char *p = NULL;

                 if(MA_OK == (err = ma_comment_internal_get_email(info, &p))) {
                     ma_variant_t *v =NULL;

                     if(MA_OK == (err = ma_variant_create_from_string(p, strlen(p), &v))) {
                         err = ma_table_add_entry(info_table, MA_COMMENT_INTERNAL_ATTR_EMAIL, v);
                         (void)ma_variant_release(v);
                     }
                 }
             }

             {
                  const char *p = NULL;

                  if(MA_OK == (err = ma_comment_internal_get_id(info, &p))) {
                     ma_variant_t *v = NULL;

                     if(MA_OK == (err = ma_variant_create_from_string(p, strlen(p), &v))) {
                         err = ma_table_add_entry(info_table, MA_COMMENT_INTERNAL_ATTR_ID, v);
                         (void)ma_variant_release(v);
                     }
                  }
             }
             {
                  const char *p = NULL;

                  if(MA_OK == (err = ma_comment_internal_get_date(info, &p))) {
                     ma_variant_t *v = NULL;

                     if(MA_OK == (err = ma_variant_create_from_string(p, strlen(p), &v))) {
                         err = ma_table_add_entry(info_table, MA_COMMENT_INTERNAL_ATTR_DATE, v);
                         (void)ma_variant_release(v);
                     }
                  }
             }
             {
                  const char *p = NULL;

                  if(MA_OK == (err = ma_comment_internal_get_comment(info, &p))) {
                     ma_variant_t *v = NULL;

                     if(MA_OK == (err = ma_variant_create_from_string(p, strlen(p), &v))) {
                         err = ma_table_add_entry(info_table, MA_COMMENT_INTERNAL_ATTR_COMMENT, v);
                         (void)ma_variant_release(v);
                     }
                  }
             }
             err = ma_variant_create_from_table(info_table, variant);
             (void)ma_table_release(info_table);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_comment_internal_convert_from_variant(ma_variant_t *variant, ma_comment_internal_t **info) {
    if(info && variant) {
        ma_error_t err = MA_OK;

        if(MA_OK == (err = ma_comment_internal_create(info))) {
            ma_table_t *info_table = NULL;

            if(MA_OK == (err = ma_variant_get_table(variant, &info_table))) {
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_COMMENT_INTERNAL_ATTR_EMAIL, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    err = ma_comment_internal_set_email((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_COMMENT_INTERNAL_ATTR_DATE, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    err = ma_comment_internal_set_date((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_COMMENT_INTERNAL_ATTR_ID, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    err = ma_comment_internal_set_id((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_COMMENT_INTERNAL_ATTR_COMMENT, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    err = ma_comment_internal_set_comment((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    }
                }

                (void)ma_table_release(info_table);
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static void for_each(const char *key, ma_variant_t *value, void *cb_data, ma_bool_t *stop_loop) {
    if(key && value && cb_data && stop_loop) {
        ma_comment_internal_t *info = (ma_comment_internal_t*)cb_data;
        ma_vartype_t type = MA_VARTYPE_NULL;

        (void)ma_variant_get_type(value, &type);
        if(MA_VARTYPE_STRING == type) {
            ma_buffer_t *buf = NULL;

            if(MA_OK == ma_variant_get_string_buffer(value, &buf)) {
                const char *pstr = NULL;
                size_t len = 0;

                if(MA_OK == ma_buffer_get_string(buf, &pstr, &len)) {
                    if(pstr) {
                        (void)ma_json_add_element(info->json, key, pstr);
                    }
                }
                (void)ma_buffer_release(buf);
            }
        }
    }
}

ma_error_t ma_comment_internal_convert_to_json(ma_comment_internal_t *info, ma_json_t **json) {
    if(info && json) {
        ma_error_t err = MA_OK;
        ma_variant_t *v = NULL;

        if(MA_OK == (err = ma_comment_internal_convert_to_variant(info, &v))) {
            ma_table_t *table = NULL;

            if(MA_OK == (err = ma_variant_get_table(v, &table))) {
                if(MA_OK == (err = ma_table_foreach(table, &for_each, info))) {
                    *json = info->json;
                }
                (void)ma_table_release(table);
            }
            (void)ma_variant_release(v);
        }
        return err;
    }
    return MA_ERROR_INVALIDARG;
}
