#include "ma/internal/utils/comment/ma_comment_datastore.h"
#include "ma/internal/defs/ma_comment_defs.h"
#include "ma/scheduler/ma_enumerator.h"
#include "ma/internal/ma_macros.h"
#include "ma/ma_log.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "comment"

extern ma_logger_t *comment_logger;

#define COMMENT_DATA MA_DATASTORE_PATH_SEPARATOR "comment_data"
#define COMMENT_ID MA_DATASTORE_PATH_SEPARATOR "comment_id"

#define COMMENT_DATA_PATH COMMENT_DATA COMMENT_ID MA_DATASTORE_PATH_SEPARATOR/* \"comment_data"\"comment_id"\ */


ma_error_t ma_comment_datastore_read(ma_comment_t *comment, ma_ds_t *datastore, const char *ds_path) {
    if(comment && datastore && ds_path) {
        ma_error_t err = MA_OK;
        ma_ds_iterator_t *comment_iterator = NULL;
        ma_buffer_t *comment_id_buf = NULL;
        char comment_data_base_path[MA_MAX_PATH_LEN] = {0};

        MA_MSC_SELECT(_snprintf, snprintf)(comment_data_base_path, MA_MAX_PATH_LEN, "%s%s", ds_path, COMMENT_DATA_PATH);
        MA_LOG(comment_logger, MA_LOG_SEV_DEBUG, "comment data store path(%s) reading...", comment_data_base_path);
        if(MA_OK == (err = ma_ds_iterator_create(datastore, 0, comment_data_base_path, &comment_iterator))) {
            while(MA_ERROR_NO_MORE_ITEMS != ma_ds_iterator_get_next(comment_iterator, &comment_id_buf) && comment_id_buf) {
                const char *comment_id = NULL;
                size_t size = 0;
                
                if(MA_OK == (err = ma_buffer_get_string(comment_id_buf, &comment_id, &size))) {
                    char comment_data_path[MA_MAX_PATH_LEN] = {0};
                    ma_variant_t *comment_var = NULL;
                    MA_MSC_SELECT(_snprintf, snprintf)(comment_data_path, MA_MAX_PATH_LEN, "%s", comment_data_base_path);/* base_path\"comment_data"\"comment_id"\4097 */
                    if(MA_OK == (err = ma_ds_get_variant(datastore, comment_data_path, comment_id, MA_VARTYPE_TABLE, &comment_var))) {

                        (void)ma_variant_release(comment_var);
                    }
                }
                (void)ma_buffer_release(comment_id_buf);
            }
            (void)ma_ds_iterator_release(comment_iterator);
        }

        return ((MA_ERROR_DS_PATH_NOT_FOUND == err) || (MA_ERROR_SCHEDULER_TASK_ID_CONFLICT == err)) ? MA_OK : err;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_comment_datastore_write(ma_comment_t *comment, ma_comment_info_t *info, ma_ds_t *datastore, const char *ds_path) {
    if(comment && info && datastore && ds_path) {
        char comment_data_path[MA_MAX_PATH_LEN] = {0};
        const char *comment_id = NULL;
        ma_error_t err = MA_OK;
        ma_variant_t *var = NULL;

        (void)ma_comment_info_get_id(info, &comment_id);
        MA_MSC_SELECT(_snprintf, snprintf)(comment_data_path, MA_MAX_PATH_LEN, "%s%s", ds_path, COMMENT_DATA_PATH);
        MA_LOG(comment_logger, MA_LOG_SEV_INFO, "comment id(%s), data path(%s) comment_data_path(%s)", comment_id, ds_path, comment_data_path);
        if(MA_OK == (err = ma_comment_info_convert_to_variant(info, &var))) {
            if(MA_OK != (err = ma_ds_set_variant(datastore, comment_data_path, comment_id, var)))
                MA_LOG(comment_logger, MA_LOG_SEV_DEBUG, "comment datastore write comment id(%s) failed", comment_id);
            (void)ma_variant_release(var);
        } else
            MA_LOG(comment_logger, MA_LOG_SEV_DEBUG, "comment datastore write comment id conversion comment info to variant failed, last error(%d)", err);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_comment_datastore_write_variant(ma_comment_t *comment, const char *comment_id, ma_ds_t *datastore, const char *ds_path, ma_variant_t *var) {
    if(comment && comment_id && var && datastore && ds_path) {
        char comment_data_path[MA_MAX_PATH_LEN] = {0};
        ma_error_t err = MA_OK;

        MA_MSC_SELECT(_snprintf, snprintf)(comment_data_path, MA_MAX_PATH_LEN, "%s%s", ds_path, COMMENT_DATA_PATH);
        MA_LOG(comment_logger, MA_LOG_SEV_INFO, "comment id(%s), data path(%s) comment_data_path(%s)", comment_id, ds_path, comment_data_path);
        if(MA_OK == (err = ma_ds_set_variant(datastore, comment_data_path, comment_id, var)))
            MA_LOG(comment_logger, MA_LOG_SEV_INFO, "comment datastore write comment id(%s) success", comment_id);
        else
            MA_LOG(comment_logger, MA_LOG_SEV_ERROR, "comment datastore write comment id(%s) failed, last error(%d)", comment_id, err);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_comment_datastore_remove_comment(const char *comment, ma_ds_t *datastore, const char *ds_path) {
    if(comment && datastore && ds_path) {
        char comment_path[MA_MAX_PATH_LEN] = {0};
        
        MA_LOG(comment_logger, MA_LOG_SEV_DEBUG, "comment removing (%s) from datastore", comment);
        MA_MSC_SELECT(_snprintf, snprintf)(comment_path, MA_MAX_PATH_LEN, "%s%s",ds_path, COMMENT_DATA_PATH );/* base_path\"comment_data"\4097 */
        return ma_ds_rem(datastore, comment_path, comment, 1);
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_comment_datastore_get(ma_comment_t *comment, ma_ds_t *datastore, const char *ds_path, const char *comment_id, ma_comment_info_t **info) {
    if(comment && datastore && ds_path && comment_id && info) {
        ma_error_t err = MA_OK;
        char comment_data_base_path[MA_MAX_PATH_LEN] = {0};
        ma_variant_t *comment_var = NULL;

        MA_MSC_SELECT(_snprintf, snprintf)(comment_data_base_path, MA_MAX_PATH_LEN, "%s%s", ds_path, COMMENT_DATA_PATH);
        if(MA_OK == (err = ma_ds_get_variant(datastore, comment_data_base_path, comment_id, MA_VARTYPE_TABLE, &comment_var))) {
            if(MA_OK == (err = ma_comment_info_convert_from_variant(comment_var, info)))
                MA_LOG(comment_logger, MA_LOG_SEV_TRACE, "comment(%s) exist in comment DB", comment_id);
            (void)ma_variant_release(comment_var);
        } else 
            MA_LOG(comment_logger, MA_LOG_SEV_ERROR, "comment(%s) does not exist in comment DB, last error(%d)", comment_id, err);

        return err;
    }
    return MA_ERROR_INVALIDARG;

}

ma_error_t ma_comment_datastore_clear(ma_ds_t *datastore, const char *ds_path) {
    return(datastore && ds_path) ? ma_ds_rem(datastore, ds_path, NULL, 1) : MA_ERROR_INVALIDARG;
}

