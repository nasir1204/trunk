#include "ma/internal/utils/comment/ma_comment_info.h"
#include "ma/internal/utils/comment/ma_comment_internal.h"
#include "ma/internal/utils/threading/ma_atomic.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/defs/ma_comment_defs.h"
#include "ma/ma_log.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

extern ma_logger_t *comment_logger;

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "comment"
#define MA_COMMENT_DEFAULT_LIMIT                 1024

struct ma_comment_info_s {
    char id[MA_MAX_NAME+1];
    ma_atomic_counter_t ref_count;
    ma_comment_internal_t **comments;
    size_t capacity;
    size_t size;
    ma_json_t *json;
};

static ma_error_t comment_info_add(ma_comment_info_t *self, ma_comment_internal_t *comment);
static ma_error_t comment_info_remove(ma_comment_info_t *self, ma_comment_internal_t *comment);
static ma_error_t comment_info_find(ma_comment_info_t *self, const char *id, ma_comment_internal_t **comment);

ma_error_t ma_comment_info_create(ma_comment_info_t **info) {
    if(info) {
         ma_error_t err = MA_ERROR_OUTOFMEMORY;

         if((*info = (ma_comment_info_t*)calloc(1, sizeof(ma_comment_info_t)))) {
             if(MA_OK == (err = ma_json_create(&(*info)->json))) {
                 if(((*info)->comments = (ma_comment_internal_t**)calloc(MA_COMMENT_DEFAULT_LIMIT, sizeof(ma_comment_internal_t*)))) {
                     (*info)->ref_count++;
                 }
             }
         }

         return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_comment_info_release(ma_comment_info_t *info) {
    if(info) {
        if(!--info->ref_count) {
            int i = 0;

            for(i = 0; i < info->size; ++i) {
                (void)ma_comment_internal_release(info->comments[i]);
            }
            free(info->comments);
            (void)ma_json_release(info->json);
            free(info);
        }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t comment_info_add(ma_comment_info_t *self, ma_comment_internal_t *comment) {
    if(self && comment) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;

        if(!self->capacity) {
            if((self->comments = (ma_comment_internal_t**)realloc(self->comments, (sizeof(ma_comment_internal_t*) * (self->size + MA_COMMENT_DEFAULT_LIMIT))))) {
                self->capacity = MA_COMMENT_DEFAULT_LIMIT;
                --self->capacity;
                self->comments[self->size++] = comment;
                err = MA_OK;
            }
        } else {
            self->comments[self->size++] = comment;
            --self->capacity;
            err = MA_OK;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t comment_info_find(ma_comment_info_t *self, const char *id, ma_comment_internal_t **comment) {
    if(self && id && comment) {
        ma_error_t err = MA_ERROR_PRECONDITION;
        int i = 0;
        
        *comment = NULL;
        for(i = 0; i < self->size; ++i) {
            const char *tid = NULL;

            if(MA_OK == (err = ma_comment_internal_get_id(self->comments[i], &tid))) {
                if(!strcmp(tid, id)) {
                    return ma_comment_internal_add(*comment = self->comments[i]);
                }
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t comment_info_remove(ma_comment_info_t *self, ma_comment_internal_t *comment) {
    if(self && comment) {
        ma_error_t err = MA_ERROR_PRECONDITION;
        int i = 0;

        for(i = 0; i < self->size; ++i) {
            if(self->comments[i] == comment) {
                memcpy(self->comments + i, self->comments + i +1, (self->size - i) * sizeof(ma_comment_internal_t*));
                --self->size;
                ++self->capacity;
                err = MA_OK;
                break;
            }
        }
        
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_comment_info_add(ma_comment_info_t *info) {
    if(info) {
        info->ref_count++;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_comment_info_remove(ma_comment_info_t *info, const char *id) {
    if(info && id) {
        ma_error_t err = MA_OK;
        ma_comment_internal_t *p = NULL;

        if(MA_OK == (err = comment_info_find(info, id, &p))) {
            (void)ma_comment_internal_release(p);
            err  = comment_info_remove(info, p);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;

}

/* setter */
ma_error_t ma_comment_info_set_id(ma_comment_info_t *self, const char *id) {
    if(self && id && (strlen(id) < MA_MAX_NAME)) {
        strncpy(self->id, id, MA_MAX_NAME);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_comment_info_set_comment(ma_comment_info_t *self, const char *id, ma_comment_internal_t *comment) {
    if(self && id && comment) {
        ma_error_t err = MA_OK;
        ma_comment_internal_t *p = NULL;

        if(MA_OK == (err = comment_info_find(self, id, &p))) {
            err = ma_comment_internal_release(p);
        } else {
            err = comment_info_add(self, comment);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_comment_info_set_ref_count(ma_comment_info_t *info, size_t count) {
    if(info) {
        info->ref_count = count;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

/* getter */
ma_error_t ma_comment_info_get_id(ma_comment_info_t *self, const char **id) {
    if(self && id) {
        *id = self->id;
         return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_comment_info_get_comment(ma_comment_info_t *self, const char *id, ma_comment_internal_t **comment) {
    return self && id && comment ? comment_info_find(self, id, comment) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_comment_info_get_comments(ma_comment_info_t *info, const ma_comment_internal_t ***comments, size_t *size) {
    if(info && comments && size) {
        *comments = (const ma_comment_internal_t**)info->comments;
        *size = info->size;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_comment_info_get_ref_count(ma_comment_info_t *info, size_t *count) {
    if(info && count) {
        *count = info->ref_count;
        return MA_OK;
    }
    return MA_OK;
}


ma_error_t ma_comment_info_get_json(ma_comment_info_t *info, ma_json_t **json) {
    if(info && json) {
        *json = info->json;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_comment_info_convert_to_variant(ma_comment_info_t *info, ma_variant_t **variant) {
    if(info && variant) {
        ma_error_t err = MA_OK;
        ma_table_t *info_table = NULL;

        if(MA_OK == (err = ma_table_create(&info_table))) {
             {
                  const char *p = NULL;

                  if(MA_OK == (err = ma_comment_info_get_id(info, &p))) {
                     ma_variant_t *v = NULL;

                     if(MA_OK == (err = ma_variant_create_from_string(p, strlen(p), &v))) {
                         err = ma_table_add_entry(info_table, MA_COMMENT_INFO_ATTR_ID, v);
                         (void)ma_variant_release(v);
                     }
                  }
             }
             {
                  ma_array_t *array = NULL;

                  if(MA_OK == (err = ma_array_create(&array))) {
                      size_t i = 0;

                      for(i = 0; i < info->size; ++i) {
                          ma_variant_t *v = NULL;

                          if(MA_OK == (err = ma_comment_internal_convert_to_variant(info->comments[i], &v))) {
                              if(MA_OK == (err = ma_array_push(array, v))) {
                                  (void)ma_comment_internal_release(info->comments[i]);
                              }
                              (void)ma_variant_release(v);
                          }
                      }
                      {
                          ma_variant_t *v = NULL;

                          if(MA_OK == (err = ma_variant_create_from_array(array, &v))) {
                              err = ma_table_add_entry(info_table, MA_COMMENT_INFO_ATTR_COMMENTS, v);
                              (void)ma_variant_release(v);
                          }
                      }
                      (void)ma_array_release(array);
                  }
             }
             err = ma_variant_create_from_table(info_table, variant);
             (void)ma_table_release(info_table);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}
static void for_each_comments(size_t index, ma_variant_t *v, void *self, ma_bool_t *stop_loop) {
    if(v && self && stop_loop) {
        ma_error_t err = MA_OK;
        ma_comment_internal_t *comment = NULL;

        if(MA_OK == (err = ma_comment_internal_convert_from_variant(v, &comment))) {
            (void)comment_info_add(self, comment);
            (void)ma_comment_internal_release(comment);
        }
    }
}

ma_error_t ma_comment_info_convert_from_variant(ma_variant_t *variant, ma_comment_info_t **info) {
    if(info && variant) {
        ma_error_t err = MA_OK;

        if(MA_OK == (err = ma_comment_info_create(info))) {
            ma_table_t *info_table = NULL;

            if(MA_OK == (err = ma_variant_get_table(variant, &info_table))) {
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_COMMENT_INFO_ATTR_ID, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    err = ma_comment_info_set_id((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_COMMENT_INFO_ATTR_COMMENTS, &v))) {
                        ma_array_t *array = NULL;

                        if(MA_OK == (err = ma_variant_get_array(v, &array))) {
                            ma_array_foreach(array, &for_each_comments, info);
                            (void)ma_array_release(array);
                        }
                        (void)ma_variant_release(v);
                    }
                }
                (void)ma_table_release(info_table);
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

void for_each(const char *key, ma_variant_t *value, void *cb_data, ma_bool_t *stop_loop) {
    if(key && value && cb_data && stop_loop) {
        ma_comment_info_t *info = (ma_comment_info_t*)cb_data;
        ma_vartype_t type = MA_VARTYPE_NULL;
        char buf[MA_MAX_LEN+1] = {0};

        (void)ma_variant_get_type(value, &type);
        if(MA_VARTYPE_INT32 == type) {
            ma_uint32_t t = 0;

            (void)ma_variant_get_uint32(value, &t);
            MA_MSC_SELECT(_snprintf, snprintf)(buf, MA_MAX_LEN, "%u", t);
            (void)ma_json_add_element(info->json, key, buf);
        } else if(MA_VARTYPE_INT32 == type) {
            ma_int32_t t = 0;

            (void)ma_variant_get_int32(value, &t);
            MA_MSC_SELECT(_snprintf, snprintf)(buf, MA_MAX_LEN, "%d", t);
            (void)ma_json_add_element(info->json, key, buf);
        } else if(MA_VARTYPE_BOOL == type) {
            ma_bool_t t = MA_FALSE;

            (void)ma_variant_get_bool(value, &t);
            MA_MSC_SELECT(_snprintf, snprintf)(buf, MA_MAX_LEN, "%d", t);
            (void)ma_json_add_element(info->json, key, buf);
        } else if(MA_VARTYPE_STRING == type) {
            ma_buffer_t *buf = NULL;

            if(MA_OK == ma_variant_get_string_buffer(value, &buf)) {
                const char *pstr = NULL;
                size_t len = 0;

                if(MA_OK == ma_buffer_get_string(buf, &pstr, &len)) {
                    if(pstr) {
                        (void)ma_json_add_element(info->json, key, pstr);
                    }
                }
                (void)ma_buffer_release(buf);
            }
        }
    }
}

ma_error_t ma_comment_info_convert_to_json(ma_comment_info_t *info, ma_json_t **json) {
    if(info && json) {
        ma_error_t err = MA_OK;
        ma_variant_t *v = NULL;

        if(MA_OK == (err = ma_comment_info_convert_to_variant(info, &v))) {
            ma_table_t *table = NULL;

            if(MA_OK == (err = ma_variant_get_table(v, &table))) {
                if(MA_OK == (err = ma_table_foreach(table, &for_each, info))) {
                    *json = info->json;
                }
                (void)ma_table_release(table);
            }
            (void)ma_variant_release(v);
        }
        return err;
    }
    return MA_ERROR_INVALIDARG;
}
