#include "ma/internal/utils/comment/ma_comment.h"
#include "ma/internal/utils/comment/ma_comment_info.h"
#include "ma/internal/utils/comment/ma_comment_datastore.h"
#include "ma/ma_msgbus.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/defs/ma_comment_defs.h"
#include "ma/internal/ma_macros.h"

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "comment"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <uv.h>

ma_logger_t *comment_logger;

struct ma_comment_s {
    char path[MA_MAX_PATH_LEN+1];
    ma_msgbus_t *msgbus;
    ma_ds_t* datastore;
};


ma_error_t ma_comment_create(ma_msgbus_t *msgbus, ma_ds_t *datastore, const char *base_path, ma_comment_t **comment) {
    if(msgbus && datastore && base_path && comment) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;

        if((*comment = (ma_comment_t*)calloc(1, sizeof(ma_comment_t)))) {
            (*comment)->msgbus = msgbus;
            strncpy((*comment)->path, base_path, MA_MAX_PATH_LEN);
            (*comment)->datastore = datastore;
            err = MA_OK;
            MA_LOG(comment_logger, MA_LOG_SEV_INFO, "comment created successfully");
        }
        if(MA_OK != err) {
            MA_LOG(comment_logger, MA_LOG_SEV_ERROR, "comment creation failed, last error(%d)", err);
            if(*comment)
                (void)ma_comment_release(*comment);
            *comment = NULL;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_comment_add(ma_comment_t *comment, ma_comment_info_t *info) {
    return comment && info ? ma_comment_datastore_write(comment, info, comment->datastore, comment->path) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_comment_add_variant(ma_comment_t *comment, const char *comment_id, ma_variant_t *var) {
    return comment && comment_id && var ? ma_comment_datastore_write_variant(comment, comment_id, comment->datastore, comment->path, var) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_comment_delete(ma_comment_t *comment, const char *comment_id) {
    return comment && comment_id ? ma_comment_datastore_remove_comment(comment_id, comment->datastore, comment->path) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_comment_delete_all(ma_comment_t *comment) {
    return comment ? ma_comment_datastore_clear(comment->datastore, comment->path) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_comment_get(ma_comment_t *comment, const char *comment_id, ma_comment_info_t **info) {
    return comment && comment && info ? ma_comment_datastore_get(comment, comment->datastore, comment->path, comment_id, info) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_comment_set_logger(ma_logger_t *logger) {
    if(logger) {
        comment_logger = logger;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_comment_get_msgbus(ma_comment_t *comment, ma_msgbus_t **msgbus) {
    if(comment && msgbus) {
        *msgbus = comment->msgbus;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_comment_start(ma_comment_t *comment) {
    if(comment) {
        MA_LOG(comment_logger, MA_LOG_SEV_TRACE, "comment started");
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_comment_stop(ma_comment_t *comment) {
    if(comment) {
        MA_LOG(comment_logger, MA_LOG_SEV_TRACE, "comment stopped");
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_comment_release(ma_comment_t *comment) {
    if(comment) {
        free(comment);
    }
    return MA_ERROR_INVALIDARG;
}

