#include "ma/internal/utils/text/ma_ini_parser.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

/* 
strip whitespaces end of given string.
return the start. 
*/
static char *r_strip(char *start)
{
    char *end = start + strlen(start);
    while (end > start && isspace((unsigned char)(*--end)))
        *end = '\0';
    return start;
}

/* 
skips the lef white chars.
return pointer to first non whitespace char in given string.
*/
static char *l_skip(const char *start)
{
    while (*start && isspace((unsigned char)(*start)))
        start++;
    return (char*)start;
}

static char *find_char(const char *start, char c)
{    
    while (*start && *start != c && *start != '\n')
        start++;    
    return (char*)start;
}

static char *find_line_end(const char *start)
{    
    while (*start && *start != '\n')
        start++;    
    return (char*)start;
}

ma_error_t ma_ini_parser_start(char *buffer, size_t size, ini_handler handler, void *userdata){	    
	if(buffer && size && handler){    
		char *section = NULL;
		char *start = NULL;
		char *end = NULL;		
		start = (char*)buffer;

		/* strip the last white spaces*/
		start = r_strip(start);		
		while(*start)
		{		
			start = l_skip(start); /*remove left spaces if any*/ 
			if(!*start) /* reach at the end */
			{
				return MA_OK;
			}
			else if (*start == '#')	/*comment*/
			{
				start = find_line_end(start);
				if(!*start) /*reach at the end*/
				{
					return MA_OK;
				}
				if(*start != '\n') /*if we dont find new line then we will break.*/
				{
					return MA_ERROR_NO_NEW_LINE;
				}				
			}		
			else if (*start == '[')  /*start of a section*/
			{         
				end = find_char(start + 1, ']');
				if(!*end) /* reach at the end */
				{
					return MA_OK;
				}
				else if (*end == ']') /* found section end so set it.*/
				{
					*end = '\0';
					section = start + 1;									
				}						
				start = end + 1;				
			}
			else if(section) /*if we have got the section then we can look the key & value*/
			{	
				char *key = NULL;
				char *value = NULL;    
				end = find_char(start, '=');				
				if(!*end) /*reach at the end*/
				{
					return MA_OK;
				}
				else if (*end == '=') 
				{
					*end = '\0';
					key = r_strip(start);
					value = l_skip(end + 1);
					end = find_line_end(value);
					if(!*end) /* Reach at the end of buffer*/
					{
						value = r_strip(value);
						if(section && key && value)
						{
							handler(section, key, value, userdata);							
						}
						return MA_OK;
					}
					else if (*end == '\n') /* Reach at the end of current line */
					{
						*end = '\0';
						value = r_strip(value);
						if(section && key && value)
						{
							if (handler(section, key, value, userdata))
								return MA_OK;
						}
					}					
					start = end + 1;				
				}
				else
				{
					return MA_ERROR_UNEXPECTED;
				}
			}
			else
			{
				start++;
			}
		}/*end while*/
		return MA_OK;
    }
	return MA_ERROR_INVALIDARG;
}

