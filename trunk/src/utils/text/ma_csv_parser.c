#include "ma/internal/utils/text/ma_csv_parser.h"

#include <wctype.h>
#include <assert.h>


#define MA_CSV_SEPARATOR_CHAR ','
#define MA_CSV_ASSIGN_CHAR '='
#define MA_CSV_ESCAPE_CHAR '"'


#define MA_CSV_KEY_STUFF 0
#define MA_CSV_VALUE_STUFF 1

static void emit_stuff(ma_csv_parser_t *csv_parser, wchar_t const *pos, unsigned what_stuff) {

}

ma_error_t MA_UTILS_API ma_csv_parser_parse(ma_csv_parser_t *csv_parser, wchar_t const *input, size_t max_chars) {
    for (; max_chars--; ++input) {
        wchar_t c = *input;

        switch (csv_parser->state) {

        case MA_CSV_PARSER_STATE_ERROR:
            break;

        case MA_CSV_PARSER_STATE_START:
            if (iswalpha(c)) {
                csv_parser->state = MA_CSV_PARSER_STATE_PARSING_KEY;
                csv_parser->start_capture = input;
            } else if (iswspace(c)) {
                /* Same state */
            } else {
                csv_parser->state = MA_CSV_PARSER_STATE_ERROR;
                return MA_ERROR_INVALIDARG;
            }
            break;

        case MA_CSV_PARSER_STATE_PARSING_KEY:
            if (iswalpha(c)) {
                /* Same state */
            } else if (MA_CSV_ASSIGN_CHAR == c) {
                csv_parser->state = MA_CSV_PARSER_STATE_AWAITING_VALUE;
                emit_stuff(csv_parser, input, MA_CSV_KEY_STUFF);
            } else if (iswspace(c)) {
                csv_parser->state = MA_CSV_PARSER_STATE_AWAITING_ASSIGN;
                emit_stuff(csv_parser, input, MA_CSV_KEY_STUFF);
            }

            break;

        case MA_CSV_PARSER_STATE_AWAITING_ASSIGN:
            if (MA_CSV_ASSIGN_CHAR == c) {
                csv_parser->state = MA_CSV_PARSER_STATE_AWAITING_VALUE;
            } if (iswspace(c)) {
                /* Same state */
            } else {
                csv_parser->state = MA_CSV_PARSER_STATE_ERROR;
                return MA_ERROR_INVALIDARG;
            }
            break;

        case MA_CSV_PARSER_STATE_AWAITING_VALUE:
            if (iswspace(c)) {
                /* Same state */
            } else if (MA_CSV_ESCAPE_CHAR == c) {
                csv_parser->state = MA_CSV_PARSER_STATE_PARSING_ESCAPED_VALUE;
                csv_parser->start_capture = 1+input; /* is this ok? */
            } else if (MA_CSV_SEPARATOR_CHAR == c) {
                /* Ok, empty value? */
                csv_parser->state = MA_CSV_PARSER_STATE_AWAITING_KEY;
                csv_parser->start_capture = input;
                emit_stuff(csv_parser, input, MA_CSV_VALUE_STUFF);
            } else {
                csv_parser->state = MA_CSV_PARSER_STATE_ERROR;
                return MA_ERROR_INVALIDARG;
            }
            break;

        case MA_CSV_PARSER_STATE_PARSING_VALUE:
            if (MA_CSV_SEPARATOR_CHAR == c) {
                csv_parser->state = MA_CSV_PARSER_STATE_AWAITING_SEPARATOR;
                emit_stuff(csv_parser, input, MA_CSV_VALUE_STUFF);
            }
            break;

        case MA_CSV_PARSER_STATE_PARSING_ESCAPED_VALUE:
            break;

        case MA_CSV_PARSER_STATE_AWAITING_ESCAPED_CHAR:
            break;

        case MA_CSV_PARSER_STATE_AWAITING_SEPARATOR:
            break;

        case MA_CSV_PARSER_STATE_AWAITING_KEY:
            break;

        default:
            assert(0 && "Unexpected parser state");
        }
    }

    return MA_OK;
}
