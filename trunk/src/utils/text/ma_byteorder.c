#include "ma/internal/utils/text/ma_byteorder.h"


const static unsigned short ma_check_machine_endianness=0x0001;
#define IS_MACHINE_LE() ( *((unsigned char*)(&ma_check_machine_endianness)) == 0x01 )
#define IS_MACHINE_BE() ( *((unsigned char*)(&ma_check_machine_endianness)) == 0x00 )

#define SWAP_BYTES(byte1,byte2) { unsigned char byte; byte=*byte1; *byte1=*byte2; *byte2=byte; }
#define REVERSE_BYTES_2(data)	{ SWAP_BYTES(data,(data+1)); }
#define REVERSE_BYTES_4(data) 	{ SWAP_BYTES(data,(data+3)); SWAP_BYTES((data+1),(data+2)); }
#define REVERSE_BYTES_8(data)   { SWAP_BYTES((data),(data+7)); \
								  SWAP_BYTES((data+1),(data+6)); \
								  SWAP_BYTES((data+2),(data+5)); \
								  SWAP_BYTES((data+3),(data+4)); }
	
ma_bool_t ma_byteorder_is_host_le(){
	return (IS_MACHINE_LE() ? MA_TRUE : MA_FALSE);
}

ma_uint16_t ma_byteorder_host_to_le_uint16( ma_uint16_t data ){
	ma_uint16_t converted=data;
	if(IS_MACHINE_BE())
		REVERSE_BYTES_2((unsigned char*)&converted);	
	return converted;
}

ma_uint16_t ma_byteorder_host_to_be_uint16( ma_uint16_t data ){
	ma_uint16_t converted=data;
	if(IS_MACHINE_LE())
		REVERSE_BYTES_2((unsigned char*)&converted);	
	return converted;
}

ma_uint32_t ma_byteorder_host_to_le_uint32( ma_uint32_t data ){
	ma_uint32_t converted=data;
	if(IS_MACHINE_BE())
		REVERSE_BYTES_4((unsigned char*)&converted);	
	return converted;
}

ma_uint32_t ma_byteorder_host_to_be_uint32( ma_uint32_t data ){
	ma_uint32_t converted=data;
	if(IS_MACHINE_LE())
		REVERSE_BYTES_4((unsigned char*)&converted);	
	return converted;
}

ma_uint64_t ma_byteorder_host_to_le_uint64(ma_uint64_t data) {
	ma_uint64_t converted = data;
	if(IS_MACHINE_BE())
		REVERSE_BYTES_8((unsigned char *) &converted);
	return converted;
}

ma_uint64_t ma_byteorder_host_to_be_uint64(ma_uint64_t data) {
	ma_uint64_t converted = data;
	if(IS_MACHINE_LE())
		REVERSE_BYTES_8((unsigned char *) &converted);
	return converted;
}

ma_uint16_t ma_byteorder_le_to_host_uint16( ma_uint16_t data ){
	return ma_byteorder_host_to_le_uint16(data);
}

ma_uint16_t ma_byteorder_be_to_host_uint16( ma_uint16_t data ){
	return ma_byteorder_host_to_be_uint16(data);
}

ma_uint32_t ma_byteorder_le_to_host_uint32( ma_uint32_t data ){
	return ma_byteorder_host_to_le_uint32(data);
}

ma_uint32_t ma_byteorder_be_to_host_uint32( ma_uint32_t data ){
	return ma_byteorder_host_to_be_uint32(data);
}

ma_uint64_t ma_byteorder_le_to_host_uint64(ma_uint64_t data){
	return ma_byteorder_host_to_le_uint64(data);
}

ma_uint64_t ma_byteorder_be_to_host_uint64(ma_uint64_t data){
	return ma_byteorder_host_to_be_uint64(data);
}
