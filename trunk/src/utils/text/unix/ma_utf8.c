#include "ma/internal/utils/text/ma_utf8.h"

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <time.h>
#include <string.h>

#include <wchar.h>
#include <iconv.h>

#ifdef MACX
#include <sys/sysctl.h>
#endif
#ifdef HPUX
#include <sys/param.h>
#include <sys/pstat.h>
#endif

#define WCHAR_MAX_SIZE_IN_BYTES	(4)	

static int is_big_endian()
{
	int x = 1;
	return ! (*(char *)&x == 1);
}
	
int ma_wide_char_to_utf8(char *dest , size_t dest_length, const wchar_t *src , size_t src_length) {            
    #ifdef HAVE_ICONV
	size_t c_len = 0;
	size_t w_len = src_length;		
	c_len = w_len * WCHAR_MAX_SIZE_IN_BYTES;	
	
	if(!dest) {
		return c_len + 1;	
	}
	
	if(dest_length < c_len) {
		return -1;
	}
	else {	
		/*iconv stuff*/
		int res = -1;
		char *ucsString = NULL;
		iconv_t cd;
		
		/*base ptr*/		
		char *base_c_ptr = (char*) calloc(c_len + 1, sizeof(char));
		wchar_t *base_w_ptr = (wchar_t*)calloc(w_len + 1, sizeof(wchar_t));
		
		/*temp ptrs*/
		char *c_out_str = base_c_ptr;		
		wchar_t *w_in_str = base_w_ptr;
		
		/*in & out byte size */
		size_t inbytesleft = (w_len) * sizeof(wchar_t);
		size_t outbytesleft = c_len;		
		dest[0] = '\0';
		
		if(base_c_ptr && base_w_ptr){
			#if defined(MACX)
				ucsString = "UCS-4-INTERNAL" ;
			#elif defined(SOLARIS) ||  defined(HPUX) 
				ucsString = "UCS-4BE" ;
			#elif defined (AIX)
				ucsString = "UCS-2";
			#else
				ucsString = "UCS-4LE" ;
			#endif

			#if !defined(SOLARIS ) && !defined(HPUX) && !defined (AIX)
				cd = iconv_open("UTF8",ucsString);
			#else
				cd = iconv_open("UTF-8",ucsString);
			#endif
			
			memcpy(w_in_str, src, w_len * sizeof(wchar_t));
			
			#if defined (MACX)  
			{
				char *in = (char *)w_in_str;
				res = iconv(cd, &in, &inbytesleft, &c_out_str, &outbytesleft);
			}
			#elif defined(SOLARIS)
			{
				const char *in = (const char *)w_in_str;
				res = iconv(cd, &in, &inbytesleft, &c_out_str, &outbytesleft);
			}
			#else						
			res = iconv(cd, (char **)(&w_in_str), &inbytesleft, &c_out_str, &outbytesleft);
			#endif
						
			iconv_close(cd);
			strcpy(dest, base_c_ptr);							
		}
		if(base_c_ptr)
			free(base_c_ptr);
		if(base_w_ptr)
			free(base_w_ptr);			
		return strlen(dest);
	}
    #else
		const wchar_t *tmp = src;    
		size_t space_needed = 0 ;    	
        mbstate_t state = {0}; 
        if( (size_t) -1 == ( space_needed = wcsrtombs(NULL, &src ,src_length, &state)))
            return -1 ; //Failed to get the size

        if(!dest)   return space_needed + 1;
        return dest_length <  space_needed 
            ? -1
            : wcsrtombs(dest, &tmp , dest_length , &state);
    #endif  /* HAVE_ICONV*/
}


int ma_utf8_to_wide_char(wchar_t *dest , size_t dest_length, const char *src , size_t src_length) {               
    #ifdef HAVE_ICONV    
	size_t c_len = src_length;
	size_t w_len = c_len;	
	
	if(!dest){
		return w_len + 1;
	}
	
	if(dest_length < w_len){
		return -1;
	}
	else {      
	  /*buffer*/
	  wchar_t *base_w_ptr = (wchar_t*) calloc(w_len + 1, sizeof(wchar_t));
      char *base_c_ptr = (char*) calloc(c_len + 1, sizeof(char));
	  
	  /*in out buffer temp.*/
      wchar_t *w_out_str = base_w_ptr;
      char *c_in_str = base_c_ptr;
	  
	  /*iconv stuff*/
      char *ucsString = NULL;
	  iconv_t cd;
	  int res = -1;
	  
	  /*in out bytes size*/
	  size_t inbytesleft = c_len;
	  size_t outbytesleft = w_len * sizeof(wchar_t) ;
	  
	  dest[0] = L'\0';
	  
	  if (is_big_endian()) {
		#if defined (AIX) 
		ucsString = "UCS-2" ;
		#else		
		ucsString = "UCS-4BE" ;
		#endif
	  }
	  else
	  	ucsString = "UCS-4LE" ;
		
	  if(base_w_ptr && base_c_ptr){
		  #if !defined(SOLARIS) && !defined(HPUX) &&!defined(AIX)
		  cd = iconv_open(ucsString,"UTF8");
		  #else
		  cd = iconv_open(ucsString,"UTF-8");
		  #endif
		  
		  memcpy(c_in_str, src, c_len * sizeof(char));
		  
		  #if defined(_SOLARIS_)
		  {
			  const char *in = c_in_str;
			  res = iconv(cd, &in, &inbytesleft, (char **)(&w_out_str), &outbytesleft);
		  }
		  #else
		  {
			  char *in = c_in_str;
			  res = iconv(cd, &in, &inbytesleft, (char **)(&w_out_str), &outbytesleft);
		  }
		  #endif		  
		  
		  /*copy to destination.*/
		  wcscpy(dest, base_w_ptr);
		  iconv_close(cd);		  
	  }
      if(base_c_ptr)
			free(base_c_ptr);
	  if(base_w_ptr)
			free(base_w_ptr);			
	  return wcslen(dest);  
	}
    #else
		size_t space_needed = 0 ;
		const char *tmp = src;
        mbstate_t state = {0}; 
        if( (size_t) -1 == ( space_needed = mbsrtowcs(NULL, &src , src_length , &state)))
            return -1 ; //Failed to get the size

        if(!dest)   return space_needed + 1;
        return dest_length <  space_needed 
            ?  -1
            : mbsrtowcs(dest, &tmp , dest_length , &state);
    #endif  /* HAVE_ICONV*/
}



ma_error_t ma_convert_wide_char_to_utf8(const wchar_t *wstr , ma_temp_buffer_t *buffer) {
    int len = 0 ;    
    if(-1 == (len =  ma_wide_char_to_utf8(NULL , 0 , wstr , wcslen(wstr) )))
        return MA_ERROR_CONVERSION_FAILED ;

    ma_temp_buffer_reserve(buffer , len);

    if(-1 == (len = ma_wide_char_to_utf8((char*)ma_temp_buffer_get(buffer)  , len  , wstr , wcslen(wstr))))
        return MA_ERROR_CONVERSION_FAILED;
		
	return MA_OK;	
}

ma_error_t ma_convert_utf8_to_wide_char(const char *str , ma_temp_buffer_t *buffer) {
    int len = 0 ;    
    if(-1 == (len =  ma_utf8_to_wide_char(NULL , 0 , str , strlen(str) )))
        return MA_ERROR_CONVERSION_FAILED ;

    ma_temp_buffer_reserve(buffer , len * sizeof(wchar_t));

    if(-1 == (len = ma_utf8_to_wide_char((wchar_t*)ma_temp_buffer_get(buffer)  , len  , str , strlen(str))))
       return MA_ERROR_CONVERSION_FAILED;
	
	return MA_OK;
}
