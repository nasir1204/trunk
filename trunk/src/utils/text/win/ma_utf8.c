#include "ma/internal/utils/text/ma_utf8.h"
#include <Windows.h>

int ma_wide_char_to_utf8(char *dest , size_t dest_length, const wchar_t *src , size_t src_length) {            
    size_t space_needed = 0 ;
    /*
        If this parameter is -1, the function processes the entire input string, including the terminating null character. 
        Therefore, the resulting Unicode string has a terminating null character, and the length returned by the function includes this character.
    */
    if(0 == (space_needed = WideCharToMultiByte(CP_UTF8, 0 , src ,  (0 == src_length) ? -1 : src_length, NULL , 0 , NULL, NULL)))
        return -1 ; //Failed to get the size 

    if(!dest)   return (0 == src_length) ? space_needed : space_needed + 1;

    return dest_length < space_needed
            ? -1
            : WideCharToMultiByte( CP_UTF8,0, src , src_length , dest ,  dest_length  , NULL, NULL) ;
            
}

int ma_utf8_to_wide_char(wchar_t *dest , size_t dest_length, const char *src , size_t src_length) {
    size_t space_needed = 0 ;
    /*
        If this parameter is -1, the function processes the entire input string, including the terminating null character. 
        Therefore, the resulting Unicode string has a terminating null character, and the length returned by the function includes this character.
    */
    if(0 == (space_needed = MultiByteToWideChar(CP_UTF8, 0 , src ,  (0 == src_length) ? -1 : src_length , NULL , 0 )))    
        return -1 ; //Failed to get the size

    if(!dest)    return (0 == src_length) ? space_needed : space_needed + 1;    
    
    return dest_length < space_needed 
            ?  -1 
            : MultiByteToWideChar(CP_UTF8, 0 , src , src_length , dest , dest_length);
}


ma_error_t ma_convert_wide_char_to_utf8(const wchar_t *wstr , ma_temp_buffer_t *buffer) {
    int len = 0 ;    
    if(-1 == (len =  ma_wide_char_to_utf8(NULL , 0 , wstr , wcslen(wstr))))
        return MA_ERROR_CONVERSION_FAILED ;

    ma_temp_buffer_reserve(buffer , len);

    return (-1 == ma_wide_char_to_utf8((char*)ma_temp_buffer_get(buffer)  , len  , wstr , wcslen(wstr)))
            ? MA_ERROR_CONVERSION_FAILED : MA_OK;
}

ma_error_t ma_convert_utf8_to_wide_char(const char *str , ma_temp_buffer_t *buffer) {
    int len = 0 ;    
    if(-1 == (len =  ma_utf8_to_wide_char(NULL , 0 , str , 0)))
        return MA_ERROR_CONVERSION_FAILED ;

    ma_temp_buffer_reserve(buffer , len * sizeof(wchar_t));

    return (-1 == ma_utf8_to_wide_char((wchar_t*)ma_temp_buffer_get(buffer)  , len  , str , strlen(str)))
            ? MA_ERROR_CONVERSION_FAILED : MA_OK;
}

