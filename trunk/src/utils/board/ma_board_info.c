#include "ma/internal/utils/board/ma_board_info.h"
#include "ma/internal/utils/threading/ma_atomic.h"
#include "ma/internal/utils/board/ma_board_topics.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/defs/ma_board_defs.h"
#include "ma/ma_log.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

extern ma_logger_t *board_logger;

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "board"
#define MA_BOARD_DEFAULT_TOPICS                 1024

struct ma_board_info_s {
    char id[MA_MAX_LEN+1];
    char url[MA_MAX_LEN+1];
    ma_board_topic_t **topics;
    size_t capacity;
    size_t size;
    ma_atomic_counter_t ref_count;
};

static ma_error_t ma_board_info_add(ma_board_info_t *self, ma_board_topic_t *topic);
static ma_error_t ma_board_info_remove(ma_board_info_t *self, ma_board_topic_t *topic);
static ma_error_t ma_board_info_find(ma_board_info_t *self, const char *id, ma_board_topic_t **topic);

ma_error_t ma_board_info_create(ma_board_info_t **self) {
    if(self) {
         ma_error_t err = MA_ERROR_OUTOFMEMORY;
 
         if((*self = (ma_board_info_t*)calloc(1, sizeof(ma_board_info_t)))) {
             if(((*self)->topics = (ma_board_topic_t**)calloc(MA_BOARD_DEFAULT_TOPICS, sizeof(ma_board_topic_t*)))) {
                 (*self)->capacity = MA_BOARD_DEFAULT_TOPICS;
                 (*self)->size = 0;
                 ++(*self)->ref_count;
             }
             err = MA_OK;
         }
         return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_info_release(ma_board_info_t *self) {
    if(self) {
        if(!(--self->ref_count)) {
            if(self->topics) {
                int i = 0;

                for(i = 0; i < self->size; ++i) {
                    (void)ma_board_topic_release(self->topics[i]);
                }
                free(self->topics);
            }
            free(self);
        }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_info_add(ma_board_info_t *self, ma_board_topic_t *topic) {
    if(self && topic) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;

        if(!self->capacity) {
            if((self->topics = (ma_board_topic_t**)realloc(self->topics, (sizeof(ma_board_topic_t*) * (self->size + MA_BOARD_DEFAULT_TOPICS))))) {
                self->capacity = MA_BOARD_DEFAULT_TOPICS;
                --self->capacity;
                err = ma_board_topic_add(self->topics[self->size++] = topic);
            }
        } else {
            err =  ma_board_topic_add(self->topics[self->size++] = topic);
            --self->capacity;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_info_find(ma_board_info_t *self, const char *id, ma_board_topic_t **topic) {
    if(self && id && topic) {
        ma_error_t err = MA_ERROR_PRECONDITION;
        int i = 0;
        
        *topic = NULL;
        for(i = 0; i < self->size; ++i) {
            const char *tid = NULL;

            if(MA_OK == ma_board_topic_get_id(self->topics[i], &tid)) {
                if(!strcmp(tid, id)) {
                    return ma_board_topic_add(*topic = self->topics[i]);
                }
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_info_remove(ma_board_info_t *self, ma_board_topic_t *topic) {
    if(self && topic) {
        ma_error_t err = MA_ERROR_PRECONDITION;
        int i = 0;

        for(i = 0; i < self->size; ++i) {
            if(self->topics[i] == topic) {
                memcpy(self->topics + i, self->topics + i +1, (self->size - i) * sizeof(ma_board_topic_t*));
                --self->size;
                ++self->capacity;
                err = ma_board_topic_release(topic);
                break;
            }
        }
        
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_info_remove_topic(ma_board_info_t *self, const char *id) {
    if(self && id) {
        ma_error_t err = MA_OK;
        ma_board_topic_t *topic = NULL;

        if(MA_OK == (err = ma_board_info_find(self, id, &topic))) {
            err = ma_board_info_remove(self, topic);
            (void)ma_board_topic_release(topic);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_info_copy(ma_board_info_t *dest, const ma_board_info_t *src) {
    if(src && dest) {
        ma_error_t err = MA_OK;
        size_t i = 0;
        const char *board_image_url = NULL;

       (void)ma_board_info_get_image_url(src, &board_image_url);
       (void)ma_board_info_set_image_url(dest, board_image_url);
        for(i = 0; i < src->size; ++i) {
            const char *id = NULL;
            ma_board_topic_t *topic = NULL;

            (void)ma_board_topic_get_id(src->topics[i], &id);
            if(MA_OK == (err = ma_board_info_find(dest, id, &topic))) {
                err = ma_board_topic_copy(topic, src->topics[i]);
                (void)ma_board_topic_release(topic);
            } else {
                err = ma_board_info_add(dest, src->topics[i]);
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

/* setter */
ma_error_t ma_board_info_set_id(ma_board_info_t *self, const char *id) {
    if(self && id && (strlen(id) < MA_MAX_NAME)) {
        strncpy(self->id, id, MA_MAX_NAME);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_info_set_topic(ma_board_info_t *self, const char *id, ma_board_topic_t *topic) {
    if(self && id && topic) {
        ma_error_t err = MA_OK;
        ma_board_topic_t *p = NULL;

        MA_LOG(board_logger, MA_LOG_SEV_TRACE, "%s start id(%s)", __FUNCTION__, id);
        if(MA_OK == (err = ma_board_info_find(self, id, &p))) {
            err = ma_board_topic_copy(p, topic);
            (void)ma_board_topic_release(p); 
        } else{
            err = ma_board_info_add(self, topic);
        }

        MA_LOG(board_logger, MA_LOG_SEV_TRACE, "%s end err(%d)", __FUNCTION__, err);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_info_set_size(ma_board_info_t *self, size_t no_of_topics) {
    if(self) {
        self->size = no_of_topics;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_info_set_image_url(ma_board_info_t *self, const char *url) {
    if(self && url && strlen(url) < MA_MAX_LEN) {
        strncpy(self->url, url, MA_MAX_LEN);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

/* getter */
ma_error_t ma_board_info_get_id(ma_board_info_t *self, const char **id) {
    if(self && id) {
        *id = self->id;
         return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_info_get_topic(ma_board_info_t *self, const char *id, ma_board_topic_t **topic) {
    return self && id && topic ? ma_board_info_find(self, id, topic) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_info_get_topics(ma_board_info_t *self, const ma_board_topic_t ***topics, int *no_of_topics) {
    if(self && topics && no_of_topics) {
        *topics = (const ma_board_topic_t**)self->topics;
        *no_of_topics = self->size;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_info_get_size(ma_board_info_t *self, size_t *no_of_topics) {
    if(self && no_of_topics) {
        *no_of_topics = self->size;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_info_get_image_url(ma_board_info_t *self, const char **url) {
    if(self && url) {
        *url = self->url;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_info_convert_to_variant(ma_board_info_t *self, ma_variant_t **variant) {
    if(self && variant) {
        ma_error_t err = MA_OK;
        ma_table_t *info_table = NULL;

        if(MA_OK == (err = ma_table_create(&info_table))) {
             {
                 const char *p = NULL;

                 if(MA_OK == (err = ma_board_info_get_id(self, &p))) {
                     ma_variant_t *v =NULL;

                     if(MA_OK == (err = ma_variant_create_from_string(p, strlen(p), &v))) {
                         err = ma_table_add_entry(info_table, MA_BOARD_INFO_ATTR_ID, v);
                         (void)ma_variant_release(v);
                     }
                 }
             }
             {
                 const char *p = NULL;

                 if(MA_OK == (err = ma_board_info_get_image_url(self, &p))) {
                     ma_variant_t *v =NULL;

                     if(MA_OK == (err = ma_variant_create_from_string(p, strlen(p), &v))) {
                         err = ma_table_add_entry(info_table, MA_BOARD_INFO_ATTR_IMAGE_URL, v);
                         (void)ma_variant_release(v);
                     }
                 }
             }
             {
                  size_t value = 0;

                  if(MA_OK == (err = ma_board_info_get_size(self, &value))) {
                      ma_variant_t *v = NULL;

                      if(MA_OK == (err = ma_variant_create_from_uint32(value, &v))) {
                          err = ma_table_add_entry(info_table, MA_BOARD_INFO_ATTR_SIZE, v);
                          (void)ma_variant_release(v);
                      }
                  }
             }
             {
                  if(self->topics) {
                      ma_array_t *array = NULL;

                      if(MA_OK == (err = ma_array_create(&array))) {
                          size_t i = 0;

                          for(i = 0; i < self->size; ++i) {
                              ma_variant_t *v = NULL;

                              if(MA_OK == (err = ma_board_topic_convert_to_variant(self->topics[i], &v))) {
                                  if(MA_OK != (err = ma_array_push(array, v))) {
                                      MA_LOG(board_logger, MA_LOG_SEV_ERROR, "topics adding into board array failed, last error(%d)", err);
                                  }
                                  (void)ma_variant_release(v);
                              }
                          }
                          {
                              ma_variant_t *v = NULL;

                              if(MA_OK == (err = ma_variant_create_from_array(array, &v))) {
                                  err = ma_table_add_entry(info_table, MA_BOARD_INFO_ATTR_TOPICS, v);
                                  (void)ma_variant_release(v);
                              }
                          }
                          (void)ma_array_release(array);
                      } 
                  }
             }

             err = ma_variant_create_from_table(info_table, variant);
             (void)ma_table_release(info_table);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static void for_each_topics(size_t index, ma_variant_t *v, void *self, ma_bool_t *stop_loop) {
    if(v && self && stop_loop) {
        ma_error_t err = MA_OK;
        ma_board_topic_t *topic = NULL;
        ma_board_info_t *info = (ma_board_info_t*)self;

        if(MA_OK == (err = ma_board_topic_convert_from_variant(v, &topic))) {
            (void)ma_board_info_add(info, topic);
            (void)ma_board_topic_release(topic);
        }
    }
}

ma_error_t ma_board_info_convert_from_variant(ma_variant_t *variant, ma_board_info_t **self) {
    if(self && variant) {
        ma_error_t err = MA_OK;

        if(MA_OK == (err = ma_board_info_create(self))) {
            ma_table_t *info_table = NULL;

            if(MA_OK == (err = ma_variant_get_table(variant, &info_table))) {
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_BOARD_INFO_ATTR_ID, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    err = ma_board_info_set_id((*self), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_BOARD_INFO_ATTR_IMAGE_URL, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    err = ma_board_info_set_image_url((*self), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_BOARD_INFO_ATTR_TOPICS, &v))) {
                        ma_array_t *array = NULL;

                        if(MA_OK == (err = ma_variant_get_array(v, &array))) {
                            ma_array_foreach(array, &for_each_topics, *self);
                            (void)ma_array_release(array);
                        }
                        (void)ma_variant_release(v);
                    }
                }

                (void)ma_table_release(info_table);
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static void bsort(ma_board_topic_t ***self, int n) {
   int i, j;
   ma_board_topic_t **topics = *self;
   for (i = 0; i < n-1; i++) {
       // Last i elements are already in place   
       for (j = 0; j < n-i-1; j++) {
           size_t l1  = 0, l2 = 0;
           (void)ma_board_topic_get_size(topics[j], &l1);
           (void)ma_board_topic_get_size(topics[j+1], &l2);
           if (l1 < l2) {
               ma_board_topic_t *temp = topics[j];
               topics[j] = topics[j+1];
               topics[j+1] = temp;
           }
       }
   }
}

ma_error_t ma_board_info_convert_to_json(ma_board_info_t *self, ma_json_table_t **json) {
    if(self && json) {
        ma_error_t err = MA_OK;

        if(MA_OK == (err = ma_json_table_create(json))) {
            size_t i = 0;

            bsort(&self->topics, self->size);
            for(i = 0; i < self->size; ++i) {
                const char *id = NULL;
                ma_json_array_t *array = NULL;

                (void)ma_board_topic_get_id(self->topics[i], &id);
                if(MA_OK == (err = ma_board_topic_convert_to_json(self->topics[i], &array))) {
                    err = ma_json_table_add_object(*json, id, array);
                    (void)ma_json_array_release(array);
                }
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}
