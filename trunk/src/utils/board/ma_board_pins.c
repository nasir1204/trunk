#include "ma/internal/utils/board/ma_board_pins.h"
#include "ma/internal/utils/threading/ma_atomic.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/defs/ma_board_defs.h"
#include "ma/internal/utils/context/ma_context.h"
#include "ma/internal/utils/season/ma_season.h"
#include "ma/internal/utils/season/ma_season_info.h"
#include "ma/internal/defs/ma_object_defs.h"

#include <string.h>
#include <stdlib.h>

extern ma_context_t *gcontext;

struct ma_board_pin_s {
    char id[MA_MAX_NAME+1];
    char topic[MA_MAX_NAME+1];
    ma_atomic_counter_t ref_count;
};

ma_error_t ma_board_pin_create(ma_board_pin_t **self) {
    if(self) {
         ma_error_t err = MA_ERROR_OUTOFMEMORY;

         if((*self = (ma_board_pin_t*)calloc(1, sizeof(ma_board_pin_t)))) {
             ++(*self)->ref_count;
             err = MA_OK;
         }

         return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_pin_release(ma_board_pin_t *self) {
    if(self) {
        if(!(--self->ref_count)) {
            free(self);
        }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_pin_add(ma_board_pin_t *self) {
    if(self) {
        ++self->ref_count;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_pin_copy(ma_board_pin_t *dest, const ma_board_pin_t *src) {
    if(src && dest) {
        ma_error_t err = MA_OK;

        if(src->id)strcpy(dest->id, src->id);
        if(src->topic)strcpy(dest->topic, src->topic);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

/* setter */
ma_error_t ma_board_pin_set_id(ma_board_pin_t *self, const char *id) {
    if(self && id && (strlen(id) < MA_MAX_NAME)) {
        strncpy(self->id, id, MA_MAX_NAME);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_pin_set_topic(ma_board_pin_t *self, const char *topic) {
    if(self && topic && (strlen(topic) < MA_MAX_NAME)) {
        strncpy(self->topic, topic, MA_MAX_NAME);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_pin_get_id(ma_board_pin_t *self, const char **id) {
    if(self && id) {
        *id = self->id;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_pin_get_topic(ma_board_pin_t *self, const char **topic) {
    if(self && topic) {
        *topic = self->topic;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_pin_convert_to_variant(ma_board_pin_t *self, ma_variant_t **variant) {
    if(self && variant) {
        ma_error_t err = MA_OK;
        ma_table_t *info_table = NULL;

        if(MA_OK == (err = ma_table_create(&info_table))) {
             {
                 const char *p = NULL;

                 if(MA_OK == (err = ma_board_pin_get_id(self, &p))) {
                     ma_variant_t *v =NULL;

                     if(MA_OK == (err = ma_variant_create_from_string(p, strlen(p), &v))) {
                         err = ma_table_add_entry(info_table, MA_BOARD_PINS_ATTR_ID, v);
                         (void)ma_variant_release(v);
                     }
                 }
             }

             {
                  const char *p = NULL;

                  if(MA_OK == (err = ma_board_pin_get_topic(self, &p))) {
                     ma_variant_t *v = NULL;

                     if(MA_OK == (err = ma_variant_create_from_string(p, strlen(p), &v))) {
                         err = ma_table_add_entry(info_table, MA_BOARD_PINS_ATTR_TOPIC, v);
                         (void)ma_variant_release(v);
                     }
                  }
             }


             err = ma_variant_create_from_table(info_table, variant);
             (void)ma_table_release(info_table);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_pin_convert_from_variant(ma_variant_t *variant, ma_board_pin_t **self) {
    if(self && variant) {
        ma_error_t err = MA_OK;

        if(MA_OK == (err = ma_board_pin_create(self))) {
            ma_table_t *info_table = NULL;

            if(MA_OK == (err = ma_variant_get_table(variant, &info_table))) {
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_BOARD_PINS_ATTR_ID, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    err = ma_board_pin_set_id((*self), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_BOARD_PINS_ATTR_TOPIC, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    err = ma_board_pin_set_topic((*self), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    }
                }

                (void)ma_table_release(info_table);
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_pin_convert_to_json(ma_board_pin_t *pin, ma_json_t **json) {
    if(pin && json) {
        ma_error_t err = MA_OK;
        ma_season_info_t *info = NULL;

        if(MA_OK == (err = ma_season_get((ma_season_t*)MA_CONTEXT_GET_SEASON(gcontext), pin->id, &info))) {
            ma_json_t *t = NULL;

            if(MA_OK == (err = ma_season_info_convert_to_json(info, &t))) {
                if(MA_OK == (err = ma_json_create(json))) {
                    ma_table_t *jtable = NULL;

                    if(MA_OK == (err = ma_json_get_table(t, &jtable))) {
                        err = ma_json_set_table(*json, jtable);
                        (void)ma_table_release(jtable);
                    }
                }
            }
            (void)ma_season_info_release(info);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}


