#include "ma/internal/utils/board/ma_board.h"
#include "ma/internal/utils/board/ma_board_info.h"
#include "ma/internal/utils/board/ma_board_datastore.h"
#include "ma/ma_msgbus.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/defs/ma_board_defs.h"
#include "ma/internal/ma_macros.h"

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "board"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <uv.h>

ma_logger_t *board_logger;
ma_context_t *gcontext;

struct ma_board_s {
    char path[MA_MAX_PATH_LEN+1];
    ma_msgbus_t *msgbus;
    ma_ds_t* datastore;
};

static const char *gdashboards[] = {
#define MA_DASHBOARD_EXPAND(status, index, status_str) status_str,
    MA_DASHBOARD_EXPANDER
#undef MA_DASHBOARD_EXPAND
};

static ma_error_t initialize_dashboard(ma_board_t *board);

ma_error_t ma_board_create(ma_msgbus_t *msgbus, ma_ds_t *datastore, const char *base_path, ma_board_t **board) {
    if(msgbus && datastore && base_path && board) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;

        if((*board = (ma_board_t*)calloc(1, sizeof(ma_board_t)))) {
            (*board)->msgbus = msgbus;
            strncpy((*board)->path, base_path, MA_MAX_PATH_LEN);
            (*board)->datastore = datastore;
            MA_LOG(board_logger, MA_LOG_SEV_INFO, "board created successfully");
            err = MA_OK;
        }
        if(MA_OK != err) {
            MA_LOG(board_logger, MA_LOG_SEV_ERROR, "board creation failed, last error(%d)", err);
            if(*board)
                (void)ma_board_release(*board);
            *board = NULL;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t initialize_dashboard(ma_board_t *board) {
    if(board) {
        ma_error_t err = MA_OK;
        size_t no_of_dashboards = MA_COUNTOF(gdashboards);
        size_t i = 0;

        for(i = 0; i < no_of_dashboards; ++i) {
            ma_board_info_t *info = NULL;

            if(MA_OK == (err = ma_board_get(board, gdashboards[i], &info))) {
                MA_LOG(board_logger, MA_LOG_SEV_TRACE, "dashboard(%s) already exist", gdashboards[i]);
                (void)ma_board_info_release(info);
            } else {
                MA_LOG(board_logger, MA_LOG_SEV_TRACE, "dashboard(%s) creation", gdashboards[i]);
                if(MA_OK == (err = ma_board_info_create(&info))) {
                    err = ma_board_add(board, info);
                    (void)ma_board_info_release(info);
                }
            } 
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_add(ma_board_t *board, ma_board_info_t *info) {
    return board && info ? ma_board_datastore_write(board, info, board->datastore, board->path) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_add_variant(ma_board_t *board, const char *board_id, ma_variant_t *var) {
    return board && board_id && var ? ma_board_datastore_write_variant(board, board_id, board->datastore, board->path, var) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_delete(ma_board_t *board, const char *board_id) {
    return board && board_id ? ma_board_datastore_remove_board(board_id, board->datastore, board->path) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_delete_all(ma_board_t *board) {
    return board ? ma_board_datastore_clear(board->datastore, board->path) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_get(ma_board_t *board, const char *board_id, ma_board_info_t **info) {
    return board && board && info ? ma_board_datastore_get(board, board->datastore, board->path, board_id, info) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_get_all(ma_board_t *board, ma_variant_t **var) {
    return board && var ? ma_board_datastore_read(board, board->datastore, board->path, var) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_set_logger(ma_logger_t *logger) {
    if(logger) {
        board_logger = logger;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_set_context(ma_context_t *context) {
    if(context) {
        gcontext = context;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_get_msgbus(ma_board_t *board, ma_msgbus_t **msgbus) {
    if(board && msgbus) {
        *msgbus = board->msgbus;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_start(ma_board_t *board) {
    if(board) {
        MA_LOG(board_logger, MA_LOG_SEV_TRACE, "board started");
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_stop(ma_board_t *board) {
    if(board) {
        MA_LOG(board_logger, MA_LOG_SEV_TRACE, "board stopped");
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_board_release(ma_board_t *board) {
    if(board) {
        free(board);
    }
    return MA_ERROR_INVALIDARG;
}

