#include "ma/internal/utils/board/ma_board_datastore.h"
#include "ma/internal/defs/ma_board_defs.h"
#include "ma/scheduler/ma_enumerator.h"
#include "ma/internal/ma_macros.h"
#include "ma/ma_log.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "board"

extern ma_logger_t *board_logger;

#define BOARD_DATA MA_DATASTORE_PATH_SEPARATOR "board_data"
#define BOARD_ID MA_DATASTORE_PATH_SEPARATOR "board_id"

#define BOARD_DATA_PATH BOARD_DATA BOARD_ID MA_DATASTORE_PATH_SEPARATOR/* \"board_data"\"board_id"\ */


ma_error_t ma_board_datastore_read(ma_board_t *board, ma_ds_t *datastore, const char *ds_path, ma_variant_t **var) {
    if(board && datastore && ds_path && var) {
        ma_error_t err = MA_OK;
        ma_ds_iterator_t *board_iterator = NULL;
        ma_buffer_t *board_id_buf = NULL;
        char board_data_base_path[MA_MAX_PATH_LEN] = {0};

        MA_MSC_SELECT(_snprintf, snprintf)(board_data_base_path, MA_MAX_PATH_LEN, "%s%s", ds_path, BOARD_DATA_PATH);
        MA_LOG(board_logger, MA_LOG_SEV_DEBUG, "board data store path(%s) reading...", board_data_base_path);
        if(MA_OK == (err = ma_ds_iterator_create(datastore, 0, board_data_base_path, &board_iterator))) {
            ma_table_t *table = NULL;

            if(MA_OK == (err = ma_table_create(&table))) {
                while(MA_ERROR_NO_MORE_ITEMS != ma_ds_iterator_get_next(board_iterator, &board_id_buf) && board_id_buf) {
                    const char *board_id = NULL;
                    size_t size = 0;
                
                    if(MA_OK == (err = ma_buffer_get_string(board_id_buf, &board_id, &size))) {
                        char board_data_path[MA_MAX_PATH_LEN] = {0};
                        ma_variant_t *board_var = NULL;
                        MA_MSC_SELECT(_snprintf, snprintf)(board_data_path, MA_MAX_PATH_LEN, "%s", board_data_base_path);/* base_path\"board_data"\"board_id"\4097 */
                        if(MA_OK == (err = ma_ds_get_variant(datastore, board_data_path, board_id, MA_VARTYPE_TABLE, &board_var))) {
                            if(MA_OK != (err = ma_table_add_entry(table, board_id, board_var))) {
                                MA_LOG(board_logger, MA_LOG_SEV_ERROR, "board id(%s) failed to add into board table, last error(%d)", board_id, err);
                            }
                            (void)ma_variant_release(board_var);
                        }
                    }
                    (void)ma_buffer_release(board_id_buf);
                }
                err = ma_variant_create_from_table(table, var);
                (void)ma_table_release(table);
            }
            (void)ma_ds_iterator_release(board_iterator);
        }

        return ((MA_ERROR_DS_PATH_NOT_FOUND == err) || (MA_ERROR_SCHEDULER_TASK_ID_CONFLICT == err)) ? MA_OK : err;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_board_datastore_write(ma_board_t *board, ma_board_info_t *info, ma_ds_t *datastore, const char *ds_path) {
    if(board && info && datastore && ds_path) {
        char board_data_path[MA_MAX_PATH_LEN] = {0};
        const char *board_id = NULL;
        ma_error_t err = MA_OK;
        ma_variant_t *var = NULL;

        (void)ma_board_info_get_id(info, &board_id);
        MA_MSC_SELECT(_snprintf, snprintf)(board_data_path, MA_MAX_PATH_LEN, "%s%s", ds_path, BOARD_DATA_PATH);
        MA_LOG(board_logger, MA_LOG_SEV_INFO, "board id(%s), data path(%s) board_data_path(%s)", board_id, ds_path, board_data_path);
        if(MA_OK == (err = ma_board_info_convert_to_variant(info, &var))) {
            if(MA_OK != (err = ma_ds_set_variant(datastore, board_data_path, board_id, var)))
                MA_LOG(board_logger, MA_LOG_SEV_DEBUG, "board datastore write board id(%s) failed", board_id);
            (void)ma_variant_release(var);
        } else
            MA_LOG(board_logger, MA_LOG_SEV_DEBUG, "board datastore write board id conversion board info to variant failed, last error(%d)", err);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_datastore_write_variant(ma_board_t *board, const char *board_id, ma_ds_t *datastore, const char *ds_path, ma_variant_t *var) {
    if(board && board_id && var && datastore && ds_path) {
        char board_data_path[MA_MAX_PATH_LEN] = {0};
        ma_error_t err = MA_OK;

        MA_MSC_SELECT(_snprintf, snprintf)(board_data_path, MA_MAX_PATH_LEN, "%s%s", ds_path, BOARD_DATA_PATH);
        MA_LOG(board_logger, MA_LOG_SEV_INFO, "board id(%s), data path(%s) board_data_path(%s)", board_id, ds_path, board_data_path);
        if(MA_OK == (err = ma_ds_set_variant(datastore, board_data_path, board_id, var)))
            MA_LOG(board_logger, MA_LOG_SEV_INFO, "board datastore write board id(%s) success", board_id);
        else
            MA_LOG(board_logger, MA_LOG_SEV_ERROR, "board datastore write board id(%s) failed, last error(%d)", board_id, err);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_board_datastore_remove_board(const char *board, ma_ds_t *datastore, const char *ds_path) {
    if(board && datastore && ds_path) {
        char board_path[MA_MAX_PATH_LEN] = {0};
        
        MA_LOG(board_logger, MA_LOG_SEV_DEBUG, "board removing (%s) from datastore", board);
        MA_MSC_SELECT(_snprintf, snprintf)(board_path, MA_MAX_PATH_LEN, "%s%s",ds_path, BOARD_DATA_PATH );/* base_path\"board_data"\4097 */
        return ma_ds_rem(datastore, board_path, board, 1);
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_datastore_get(ma_board_t *board, ma_ds_t *datastore, const char *ds_path, const char *board_id, ma_board_info_t **info) {
    if(board && datastore && ds_path && board_id && info) {
        ma_error_t err = MA_OK;
        char board_data_base_path[MA_MAX_PATH_LEN] = {0};
        ma_variant_t *board_var = NULL;

        MA_MSC_SELECT(_snprintf, snprintf)(board_data_base_path, MA_MAX_PATH_LEN, "%s%s", ds_path, BOARD_DATA_PATH);
        if(MA_OK == (err = ma_ds_get_variant(datastore, board_data_base_path, board_id, MA_VARTYPE_TABLE, &board_var))) {
            if(MA_OK == (err = ma_board_info_convert_from_variant(board_var, info)))
                MA_LOG(board_logger, MA_LOG_SEV_TRACE, "board(%s) exist in board DB", board_id);
            (void)ma_variant_release(board_var);
        } else 
            MA_LOG(board_logger, MA_LOG_SEV_ERROR, "board(%s) does not exist in board DB, last error(%d)", board_id, err);

        return err;
    }
    return MA_ERROR_INVALIDARG;

}

ma_error_t ma_board_datastore_clear(ma_ds_t *datastore, const char *ds_path) {
    return(datastore && ds_path) ? ma_ds_rem(datastore, ds_path, NULL, 1) : MA_ERROR_INVALIDARG;
}

