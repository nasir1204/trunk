#include "ma/internal/utils/board/ma_board_topics.h"
#include "ma/internal/utils/board/ma_board_pins.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/utils/context/ma_context.h"
#include "ma/internal/utils/threading/ma_atomic.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/defs/ma_board_defs.h"
#include "ma/ma_log.h"

#include <string.h>
#include <stdlib.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "board"
#define MA_BOARD_TOPICS_DEFAULT_PINS                 1024

extern ma_context_t *gcontext;
extern ma_logger_t *board_logger;

struct ma_board_topic_s {
    char id[MA_MAX_NAME+1];
    char board[MA_MAX_NAME+1];
    ma_board_pin_t **pins;
    size_t capacity;
    size_t size;
    ma_atomic_counter_t ref_count;
};

static ma_error_t topic_add(ma_board_topic_t *self, ma_board_pin_t *pin);
static ma_error_t topic_remove(ma_board_topic_t *self, ma_board_pin_t *pin);
static ma_error_t topic_find(ma_board_topic_t *self, const char *id, ma_board_pin_t **pin);
static ma_error_t topic_update(ma_board_topic_t *self, ma_board_pin_t *pin);

/* ctor & dtor */
ma_error_t ma_board_topic_create(ma_board_topic_t **self) {
    if(self) {
         ma_error_t err = MA_ERROR_OUTOFMEMORY;
 
         if((*self = (ma_board_topic_t*)calloc(1, sizeof(ma_board_topic_t)))) {
             if(((*self)->pins = (ma_board_pin_t**)calloc(MA_BOARD_TOPICS_DEFAULT_PINS, sizeof(ma_board_pin_t*)))) {
                 (*self)->capacity = MA_BOARD_TOPICS_DEFAULT_PINS;
                 ++(*self)->ref_count;
             }
             err = MA_OK;
         }
         return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_topic_release(ma_board_topic_t *self) {
    if(self) {
        if(!(--self->ref_count)) {
            if(self->pins) {
                size_t i = 0;

                for(i = 0; i < self->size; ++i)
                    (void)ma_board_pin_release(self->pins[i]);
                free(self->pins); 
            }
            free(self);
        }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_topic_add(ma_board_topic_t *self) {
    if(self) {
        ++self->ref_count;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t topic_add(ma_board_topic_t *self, ma_board_pin_t *pin) {
    if(self && pin) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;

        MA_LOG(board_logger, MA_LOG_SEV_DEBUG, "%s starts", __FUNCTION__);
        if(!self->capacity) {
            if((self->pins = (ma_board_pin_t**)realloc(self->pins, (sizeof(ma_board_pin_t*) * (self->size + MA_BOARD_TOPICS_DEFAULT_PINS))))) {
                self->capacity = MA_BOARD_TOPICS_DEFAULT_PINS;
                --self->capacity;
                err = ma_board_pin_add(self->pins[self->size++] = pin);
            }
        } else {
            --self->capacity;
            err = ma_board_pin_add(self->pins[self->size++] = pin);
        }

        MA_LOG(board_logger, MA_LOG_SEV_DEBUG, "%s ends err(%d)", __FUNCTION__, err);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t topic_find(ma_board_topic_t *self, const char *id, ma_board_pin_t **pin) {
    if(self && id && pin) {
        ma_error_t err = MA_ERROR_PRECONDITION;
        ma_error_t rc = MA_OK;
        int i = 0;
        
        *pin = NULL;
        MA_LOG(board_logger, MA_LOG_SEV_DEBUG, "%s starts pin(%s)",__FUNCTION__, id);
        for(i = 0; i < self->size; ++i) {
            const char *pid = NULL;

            if(MA_OK == (rc = ma_board_pin_get_id(self->pins[i], &pid))) {
                if(!strcmp(pid, id)) {
                    MA_LOG(board_logger, MA_LOG_SEV_ERROR, "pin (%s) found", id);
                    return ma_board_pin_add(*pin = self->pins[i]);
                }
            } else {
                MA_LOG(board_logger, MA_LOG_SEV_ERROR, "pin get failed, last  error(%d)", rc);
            }
        }
        MA_LOG(board_logger, MA_LOG_SEV_DEBUG, "%s end pin(%s) err(%d)",__FUNCTION__,id, err);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t topic_update(ma_board_topic_t *self, ma_board_pin_t *pin) {
    if(self && pin) {
        ma_error_t err = MA_OK;
        size_t i = 0;
        const char *id = NULL;

        (void)ma_board_pin_get_id(pin, &id);
        for(i = 0; i < self->size; ++i) {
            const char *pid = NULL;

            (void)ma_board_pin_get_id(self->pins[i], &pid);
            if(!strcmp(pid, id)) {
               err = ma_board_pin_copy(self->pins[i], pin);
               break;
            }
        }
        if(i == self->size) {
            err = topic_add(self, pin);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t topic_remove(ma_board_topic_t *self, ma_board_pin_t *pin) {
    if(self && pin) {
        ma_error_t err = MA_ERROR_PRECONDITION;
        int i = 0;

        for(i = 0; i < self->size; ++i) {
            if(self->pins[i] == pin) {
                (void)ma_board_pin_release(pin);
                memcpy(self->pins + i, self->pins + i +1, (self->size - i) * sizeof(ma_board_pin_t*));
                --self->size;
                ++self->capacity;
                err = MA_OK;
                break;
            }
        }
        
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_topic_remove_pin(ma_board_topic_t *self, const char *id) {
    if(self && id) {
        ma_error_t err = MA_OK;
        ma_board_pin_t *pin = NULL;

        if(MA_OK == (err = topic_find(self, id, &pin))) {
            err = topic_remove(self, pin);
            (void)ma_board_pin_release(pin);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_topic_copy(ma_board_topic_t *dest, const ma_board_topic_t *src) {
    if(dest && src) {
        ma_error_t err = MA_OK;
        size_t i = 0;

        MA_LOG(board_logger, MA_LOG_SEV_DEBUG, "%s start", __FUNCTION__);
        MA_LOG(board_logger, MA_LOG_SEV_DEBUG, "topic size(%d)", src->size);
        for(i = 0; i < src->size; ++i) {
            ma_board_pin_t *pin = NULL;
            const char *id = NULL;

            (void)ma_board_pin_get_id(src->pins[i], &id);
            if(MA_OK == (err = topic_find(dest, id, &pin))) {
                err = topic_update(dest, src->pins[i]);
                (void)ma_board_pin_release(pin);
            } else {
                MA_LOG(board_logger, MA_LOG_SEV_DEBUG, "pin id(%s) does not exist", id);
                err = topic_add(dest, src->pins[i]);
            }
        }

        MA_LOG(board_logger, MA_LOG_SEV_TRACE, "%s end error(%d)", __FUNCTION__, err);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}
/* setter */
ma_error_t ma_board_topic_set_id(ma_board_topic_t *self, const char *id) {
    if(self && id && (strlen(id) < MA_MAX_NAME)) {
        strncpy(self->id, id, MA_MAX_NAME);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_topic_set_pin(ma_board_topic_t *self, const char *id, ma_board_pin_t *pin) {
    if(self && id && pin) {
        ma_error_t err = MA_OK;
        ma_board_pin_t *p = NULL;

        MA_LOG(board_logger, MA_LOG_SEV_DEBUG, " %s starts topic(%s)", __FUNCTION__, id); 
        if(MA_OK == (err = topic_find(self, id, &p))) {
            MA_LOG(board_logger, MA_LOG_SEV_DEBUG, "pin id(%s) found", id); 
            err = ma_board_pin_release(p); 
        } else{
            MA_LOG(board_logger, MA_LOG_SEV_DEBUG, "pin id(%s) not found adding", id); 
            err = topic_add(self, pin);
        }

        MA_LOG(board_logger, MA_LOG_SEV_DEBUG, " %s end topic(%s) err(%d)", __FUNCTION__, id, err); 
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_topic_set_board(ma_board_topic_t *self, const char *id) {
    if(self && id && (strlen(id) < MA_MAX_NAME)) {
        strncpy(self->board, id, MA_MAX_NAME);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_topic_set_size(ma_board_topic_t *self, size_t no_of_pins) {
    if(self) {
        self->size = no_of_pins;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

/* getter */
ma_error_t ma_board_topic_get_id(ma_board_topic_t *self, const char **id) {
    if(self && id) {
        *id = self->id;
         return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_topic_get_pin(ma_board_topic_t *self, const char *id, ma_board_pin_t **pin) {
    return self && id && pin ? topic_find(self, id, pin) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_topic_get_pins(ma_board_topic_t *self, ma_board_pin_t ***pins, int no_of_pins) {
    if(self && pins && no_of_pins && no_of_pins < self->size) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;

        if((*pins = (ma_board_pin_t**)calloc(no_of_pins, sizeof(ma_board_pin_t*)))) {
            size_t i = 0;
        
            for(i = 0; i < no_of_pins; ++i) {
                err  =ma_board_pin_add((*pins)[i] = self->pins[i]);
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_topic_get_size(ma_board_topic_t *self, size_t *no_of_pins) {
    if(self && no_of_pins) {
        *no_of_pins = self->size;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_topic_get_board(ma_board_topic_t *self, const char **id) {
    if(self && id) {
        *id = self->board;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_topic_convert_to_variant(ma_board_topic_t *self, ma_variant_t **variant) {
    if(self && variant) {
        ma_error_t err = MA_OK;
        ma_table_t *info_table = NULL;

        if(MA_OK == (err = ma_table_create(&info_table))) {
             {
                 const char *p = NULL;

                 if(MA_OK == (err = ma_board_topic_get_id(self, &p))) {
                     ma_variant_t *v =NULL;

                     if(MA_OK == (err = ma_variant_create_from_string(p, strlen(p), &v))) {
                         err = ma_table_add_entry(info_table, MA_BOARD_TOPICS_ATTR_ID, v);
                         (void)ma_variant_release(v);
                     }
                 }
             }
             {
                  const char *p = NULL;

                  if(MA_OK == (err = ma_board_topic_get_board(self, &p))) {
                      ma_variant_t *v = NULL;

                      if(MA_OK == (err = ma_variant_create_from_string(p, strlen(p), &v))) {
                          err = ma_table_add_entry(info_table, MA_BOARD_TOPICS_ATTR_BOARD, v);
                          (void)ma_variant_release(v);
                      }
                  }
             }
             {
                  size_t value = 0;

                  if(MA_OK == (err = ma_board_topic_get_size(self, &value))) {
                      ma_variant_t *v = NULL;

                      if(MA_OK == (err = ma_variant_create_from_uint32(value, &v))) {
                          err = ma_table_add_entry(info_table, MA_BOARD_TOPICS_ATTR_SIZE, v);
                          (void)ma_variant_release(v);
                      }
                  }
             }
             {

                  if(self->pins && self->size > 0) {
                      ma_array_t *array = NULL;

                      if(MA_OK == (err = ma_array_create(&array))) {
                          size_t i = 0;

                          for(i = 0; i < self->size; ++i) {
                              ma_variant_t *v = NULL;

                              if(MA_OK == (err = ma_board_pin_convert_to_variant(self->pins[i], &v))) {
                                  if(MA_OK != (err = ma_array_push(array, v))) {
                                      MA_LOG(board_logger, MA_LOG_SEV_ERROR, "pushing pins into topic pin array failed, last error(%d)", err);
                                  }
                                  (void)ma_variant_release(v);
                              }
                          }
                          {
                              ma_variant_t *v = NULL;

                              if(MA_OK == (err = ma_variant_create_from_array(array, &v))) {
                                  err = ma_table_add_entry(info_table, MA_BOARD_TOPICS_ATTR_PINS, v);
                                  (void)ma_variant_release(v);
                              }
                          }
                          (void)ma_array_release(array);
                      } 
                  }
             }

             err = ma_variant_create_from_table(info_table, variant);
             (void)ma_table_release(info_table);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static void for_each_topic_pins(size_t index, ma_variant_t *v, void *self, ma_bool_t *stop_loop) {
    if(v && self && stop_loop) {
        ma_error_t err = MA_OK;
        ma_board_pin_t *pin = NULL;
        ma_board_topic_t *topic = (ma_board_topic_t*)self;

        if(MA_OK == (err = ma_board_pin_convert_from_variant(v, &pin))) {
            (void)topic_add(topic, pin);
            (void)ma_board_pin_release(pin);
        }
    }
}

ma_error_t ma_board_topic_convert_from_variant(ma_variant_t *variant, ma_board_topic_t **self) {
    if(self && variant) {
        ma_error_t err = MA_OK;

        if(MA_OK == (err = ma_board_topic_create(self))) {
            ma_table_t *info_table = NULL;

            if(MA_OK == (err = ma_variant_get_table(variant, &info_table))) {
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_BOARD_TOPICS_ATTR_ID, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    err = ma_board_topic_set_id((*self), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_BOARD_TOPICS_ATTR_BOARD, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    err = ma_board_topic_set_board((*self), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_BOARD_TOPICS_ATTR_PINS, &v))) {
                        ma_array_t *array = NULL;

                        if(MA_OK == (err = ma_variant_get_array(v, &array))) {
                            ma_array_foreach(array, &for_each_topic_pins, *self);
                            (void)ma_array_release(array);
                        }
                        (void)ma_variant_release(v);
                    }
                }

                (void)ma_table_release(info_table);
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_topic_convert_to_json(ma_board_topic_t *self, ma_json_array_t **array) {
    if(self && array) {
        ma_error_t err = MA_OK;

        if(MA_OK == (err = ma_json_array_create(array))) {
            size_t i = 0;

            //for(i = 0; i < self->size; ++i) {
            for(i = self->size; i > 0; --i) {
                ma_json_t *json = NULL;

                if(MA_OK == (err = ma_board_pin_convert_to_json(self->pins[i-1], &json))) {
                    err = ma_json_array_add_object(*array, json);
                    (void)ma_json_release(json); //TODO check for reference
                }
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}


