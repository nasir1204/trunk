#include "ma/internal/utils/logger/ma_logger_internal.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal/utils/threading/ma_thread.h"
#include "ma/internal/utils/text/ma_utf8.h"


#include <stdio.h>
#include <string.h>
#include <wchar.h>
#include <assert.h>

#ifndef MA_WINDOWS
#include <unistd.h>
#endif
/*! 
 * formats a log message according to the specification in 'format' 
 * returns the number of characters that would have been copied into buffer if enough room was available.
 * 
 * can also be used to calculate how many bytes are required to store the full message by passing buffer=NULL,
 * @param buffer Buffer to receive the formatted string. If null, this function will instead calculate how many characters are needed to store the string, not including the terminating null
 * @param count Maximum number of characters to copy
 * @param format A null terminated format string, see below for the syntax
 * @param msg The message where values are extracted from
 *  
 */


MA_INLINE static const char *ma_safe_str(const char *str) {
    return str ? str : "";
}

MA_INLINE static const wchar_t *ma_safe_wstr(const wchar_t *str) {
    return str ? str : L"";
}

static const char *current_process_name() {
    static const char *process_name = NULL;
    /* the following code isn't strictly thread safe, should probably use some uv_once construct */
    if (NULL == process_name) {
#ifdef MA_WINDOWS
        static char path_name[MAX_PATH] = {0};
        wchar_t wpath_name[MAX_PATH];
        DWORD nChars = GetModuleFileNameW(NULL, wpath_name, MA_COUNTOF(wpath_name)-1);
        wchar_t *p, c;
        /* search backwards for the first occurrence of a path separator, yanking any file extensions on the way... */
        for (p = wpath_name + nChars; nChars && '\\' != (c=*p); --p,--nChars) {
            if ('.' == c) *p = 0;/* yank the extension portion (if there are more than one this will yank them all) */
        }
        (void) ma_wide_char_to_utf8(path_name, MA_COUNTOF(path_name)-1, nChars ? 1+p : p, -1);
        process_name = path_name;
#else
        process_name = ""; /* TODO figure out how to do this on *nix */
#endif
    }
    return process_name;
}

#define EMIT_CHAR(c) do {++chars_copied; if (dest && count) {--count, *dest++ =c;}} while(0) 

//#define EMIT_STRING(s,len) do {chars_copied+=len; if (dest) while (count--) {*dest++ = *s++;} } while (0)

int ma_log_utils_format_msg(char *buffer, size_t count, const char *format, const ma_log_msg_t *msg) {
    const char *p = format;
    char c, *dest = buffer;
    size_t chars_copied = 0;

    enum parse_state_e {
        parse_state_normal,
        parse_state_percent

    } state = parse_state_normal;

    int log_more = 0, log_less = 0;

    if (dest && 0 == count) {
        assert(!"must specify a non empty buffer");
        return -1;
    }

    while ( 0 != (c = *p++) ) {
        if (parse_state_normal == state) {
            if ('%' == c) {
                state = parse_state_percent;
                continue;
            } else {
                EMIT_CHAR(c);
            }
        } else {
            int res = 0;
            switch (c) {
                case '+' : {
                    log_more = 1;
                    continue; /* do not reset state */
                }
                case '-' : {
                    log_less = 1;
                    continue; /* do not reset state */
                }
                case '%' : {
                    EMIT_CHAR(c);
                    break;
                }
                case 'd' : { /* date */
                    const int chars_needed = 10;
                    res = dest 
                        ? MA_MSC_SELECT(_snprintf, snprintf)(dest, count, "%04u-%02u-%02u", (unsigned) msg->time.year, (unsigned) msg->time.month, (unsigned) msg->time.day)
                        : chars_needed;
                    break;
                }
                case 'f' : { /* facility */
                    res = dest 
                        ? MA_MSC_SELECT(_snprintf, snprintf)(dest, count, "%s", ma_safe_str(msg->facility)) 
                        : (int) strlen(ma_safe_str(msg->facility));
                    break;
                }
                case 'F' : { /* file */
                    const char *file_name = ma_safe_str(msg->file_name);
                    if (log_less) {
                        const char *p = strrchr(file_name, MA_PATH_SEPARATOR);
                        if (p) file_name = 1+p;
                    }
                    res = dest
                        ? MA_MSC_SELECT(_snprintf, snprintf)(dest, count, "%s", file_name)
                        : (int) strlen(file_name);
                    break;
                }
                case 'm' : { /* message */
                    if (msg->msg_is_wide_char) {
                        /* convert utf16 to utf8*/
                        res = ma_wide_char_to_utf8(dest , count , ma_safe_wstr(msg->message.wchar_message) ,
                                        wcslen(ma_safe_wstr(msg->message.wchar_message)));
                    } else {
                        res = dest 
                            ? MA_MSC_SELECT(_snprintf,snprintf)(dest, count, "%s", ma_safe_str(msg->message.utf8_message))
                            : (int) strlen(ma_safe_str(msg->message.utf8_message));
                    }
                    break;
                }
                case 'l' : { /* line num */
                    res = dest 
                        ? MA_MSC_SELECT(_snprintf, snprintf)(dest, count, "%04d", (int) msg->line_num)
                        : 4; 
                    break;
                }
                case 'p' : { /* process name */
                    res = dest 
                        ? MA_MSC_SELECT(_snprintf,snprintf)(dest, count, "%s", current_process_name())
                        : (int) strlen(current_process_name());
                    break;
                }
                case 'P' : { /* process id */
                    res = dest
                        ? MA_MSC_SELECT(_snprintf, snprintf)(dest, count, log_more ? "%x" : "%d", msg->pid)
                        : 7; /* just a guess */
                    break;
                }
                case 's' : { /* severity */
                    const char *txt = ma_log_get_severity_label(msg->severity);
                    res = dest 
                        ? MA_MSC_SELECT(_snprintf, snprintf)(dest, count, log_less ? "%.1s" : "%s", txt) 
                        : log_less ? 1 : (int) strlen(txt);
                    break;
                }
#ifdef MA_WINDOWS
                case 'S' : { /* Session-id */
                    res = dest
                        ? MA_MSC_SELECT(_snprintf, snprintf)(dest, count, log_more ? "%x" : "%d", msg->session_id)
                        : 7; /* just a guess */
                    break;
                }
#else
                case 'S' : { /* Session-id */
                    res = dest
                        ? MA_MSC_SELECT(_snprintf, snprintf)(dest, count, log_more ? "%x" : "%d",getsid(msg->pid))
                        : 7; /* just a guess */
                    break;
                }
				
#endif
                case 't' : { /* time */
                    const int chars_needed = log_more ? 12 : 8;
                    res = dest
                        ? MA_MSC_SELECT(_snprintf, snprintf)(dest, count, log_more ? "%02u:%02u:%02u.%03u" : "%02u:%02u:%02u", (unsigned) msg->time.hour, (unsigned) msg->time.minute, (unsigned) msg->time.second, (unsigned) msg->time.milliseconds)
                        : chars_needed;
                    break;
                }
                case 'T' : { /* thread-id */
                    res = dest
                        ? MA_MSC_SELECT(_snprintf, snprintf)(dest, count, log_more ? "%x" : "%d", msg->thread_id)
                        : 7; /* just a guess */
                    break;
                }
#ifndef MA_WINDOWS
                case 'u' : { /* uid */
                    res = dest
                        ? MA_MSC_SELECT(_snprintf, snprintf)(dest, count, "%04d", (int) msg->uid)
                        : 5;
                    break;
                }
#endif
                case 'U' : { /* function */
                    res = dest
                        ? MA_MSC_SELECT(_snprintf, snprintf)(dest, count, "%s", msg->func_name)
                        : (int) strlen(msg->func_name);
                    break;
                }
#ifdef MA_WINDOWS
                case 'w' : { /* window station */
                    res = dest
                        ? MA_MSC_SELECT(_snprintf, snprintf)(dest, count, "%p", (void const *)msg->window_stn)
                        : 16;
                    break;
                }
#endif
                default :
                    break; /* simply drop unknown format specifiers, resetting state*/
            }

            if (dest) {
                if (-1 < res && res < count) {
                    dest += res;
                    count -= res;
                    chars_copied += res;
                } else return -1;
            } else {
                chars_copied += res;
            }
        state = parse_state_normal;
        log_less = log_more = 0;
        }
    }

    /* ensure null termination */
    if (dest) {
        dest[count ? 0 : -1] = 0;
    }

    return (int) chars_copied;
}


