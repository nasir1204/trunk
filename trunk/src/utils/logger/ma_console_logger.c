#include "ma/logger/ma_console_logger.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal/utils/threading/ma_thread.h"
#include "ma/internal/utils/logger/ma_logger_internal.h"


#include "ma_logger_appender.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct ma_console_logger_policy_s {
    char    *format_pattern ;
}ma_console_logger_policy_t;

typedef struct ma_console_appender_s {
    ma_logger_appender_t        appender_impl ; //Appender implementer
    FILE                        *fp;
    ma_console_logger_policy_t   policy ;
}ma_console_appender_t ;

struct ma_console_logger_s {
    ma_logger_t                 logger_impl ; //Logger base class
    ma_console_appender_t       console_appender;
};

static ma_error_t ma_console_logger_on_log_message(ma_logger_t *logger, ma_log_msg_t *msg);
static void console_logger_release(ma_logger_t *logger);

static const ma_logger_methods_t methods = {
    &ma_console_logger_on_log_message,
    &console_logger_release
};


static ma_error_t ma_console_appender_init(ma_console_appender_t *appender) ;

ma_error_t ma_console_logger_create(ma_console_logger_t **logger) {
    if(logger) {
        ma_console_logger_t *self = (ma_console_logger_t *)calloc(1 , sizeof(ma_console_logger_t));
        ma_error_t rc = MA_OK ;

        if(!self)   return MA_ERROR_OUTOFMEMORY ;
        if(MA_OK != (rc = ma_console_appender_init(&self->console_appender))) {
            free(self);
            return rc ;
        }
        ma_logger_init((ma_logger_t *)self , &methods , 1 , self);
        *logger = self;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_console_logger_release(ma_console_logger_t *logger) {
    ma_logger_release(&logger->logger_impl);
    return MA_OK;
}


static ma_error_t ma_console_logger_on_log_message(ma_logger_t *logger, ma_log_msg_t *msg) {
    if(logger &&  msg) {
        ma_console_logger_t *self = (ma_console_logger_t *)logger ;
        char *format = self->console_appender.policy.format_pattern ?
            self->console_appender.policy.format_pattern :
        DEFAULT_CONSOLE_LOG_FORMAT_PATTERN ;
        int r;
        ma_temp_buffer_t b = {0};
        ma_temp_buffer_init(&b);
        if ((r = ma_log_utils_format_msg((char *)ma_temp_buffer_get(&b), ma_temp_buffer_capacity(&b),format , msg)) < 0) {
            int chars_needed = ma_log_utils_format_msg(NULL, 0, format, msg);
            ma_temp_buffer_reserve(&b,1+chars_needed);
            r = ma_log_utils_format_msg((char *)ma_temp_buffer_get(&b), ma_temp_buffer_capacity(&b), format, msg);
        }
        if (-1 < r) {
            ma_logger_appender_append((ma_logger_appender_t *)&self->console_appender , (const char *)ma_temp_buffer_get(&b));
        }
        ma_temp_buffer_uninit(&b);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static void console_logger_release(ma_logger_t *logger) {
    ma_console_logger_t *self = (ma_console_logger_t *)logger;
    if(self) {
        ma_logger_appender_release((ma_logger_appender_t *)&self->console_appender);
        free(self);
    }
    return;
}



static ma_error_t ma_console_appender_append(ma_logger_appender_t *self , const char *msg);
static void ma_console_appender_release(ma_logger_appender_t *self);

static const ma_logger_appender_methods_t appender_methods = {
    &ma_console_appender_append,
    NULL ,
    NULL ,
    &ma_console_appender_release
};

static ma_error_t ma_console_appender_init(ma_console_appender_t *appender) {
    if(stdout) appender->fp = stdout;
    else if(stderr)	appender->fp = stderr;
    else return MA_ERROR_UNEXPECTED;

    ma_logger_appender_init((ma_logger_appender_t *)appender , &appender_methods , appender);
    return MA_OK ;
}


static ma_error_t ma_console_appender_append(ma_logger_appender_t *appender , const char *msg ) {
    if(appender && msg) {
        ma_console_appender_t *self = (ma_console_appender_t *)appender;

        if(!self->fp)  return MA_ERROR_UNEXPECTED;
        fprintf(self->fp, "%s\n", (char*)msg);
        fflush(self->fp);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static void ma_console_appender_release(ma_logger_appender_t *appender) {
    ma_console_appender_t *self = (ma_console_appender_t *)appender;

    if(self && self->fp)
        fflush(self->fp);
}
