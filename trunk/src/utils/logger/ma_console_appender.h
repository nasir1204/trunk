/** @file ma_console_appender.h
 *  @brief console appender declarations
 *
 * This file contains prototypes console appender's utilities
*/

#ifndef MA_CONSOLE_APPENDER_H_INCLUDED
#define MA_CONSOLE_APPENDER_H_INCLUDED

MA_CPP(extern "C" {)

typedef struct ma_console_appender_s ma_console_appender_t, *ma_console_appender_h;

/* console appender structure defination */
struct ma_console_appender_s
{
    ma_logger_appender_t appender_base;/* appender base class *//
    FILE *fp;
};

static ma_error_t ma_console_appender_init(ma_logger_appender_t **self)
{
    ma_console_appender_t *_this = NULL;

    if(!self) return MA_ERROR_INVALIDARG;
    _this = (ma_console_appender_t*)*self;
    if(stdout) _this->fp = stdout;
    else if(stderr)	_this->fp = stderr;
    else return MA_ERROR_UNEXPECTED;
    return MA_OK;
}

static ma_error_t ma_console_write(ma_logger_appender_t *self, void *msg)
{
    ma_console_appender_t *_this = NULL;

    if(!self || !msg) return MA_ERROR_INVALIDARG;
    _this = (ma_console_appender_t*)self;
    if(!_this->fp) return MA_ERROR_UNEXPECTED;
    fprintf(_this->fp, "%s", (char*)msg);
    fprintf(_this->fp,"\n");
    fflush(_this->fp);
    return MA_OK;
}

static ma_error_t ma_console_appender_dinit(ma_logger_appender_t *self)
{
    ma_console_appender_t *_this = NULL;
    if(!self) return MA_ERROR_INVALIDARG;
    _this = (ma_console_appender_t*)self;
    fflush(_this->fp);
    return MA_OK;
}

static struct ma_logger_appender_methods_s console_appender_methods = {
    &ma_console_appender_init,
    &ma_console_write,
    &ma_console_appender_dinit
};

static ma_error_t ma_console_appender_create(ma_console_appender_t **self)
{
    ma_error_t ma_error_ret = MA_OK;

    if(!self) return MA_ERROR_INVALIDARG;
    *self = (ma_console_appender_t*)calloc(1, sizeof(struct ma_console_appender_s));
    if(!(*self)) return MA_ERROR_OUTOFMEMORY;
    ma_logger_appender_init(&(*self)->appender_base, &console_appender_methods, self);
    ma_error_ret = (*self)->appender_base.methods->init((ma_logger_appender_t**)self);
    if(MA_OK != ma_error_ret)free(*self);
    return ma_error_ret;
}

MA_CPP(})

#endif //#ifndef MA_CONSOLE_APPENDER_H_INCLUDED


