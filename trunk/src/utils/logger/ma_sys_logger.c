#include "ma/logger/ma_sys_logger.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal/utils/threading/ma_thread.h"
#include "ma/logger/ma_log_utils.h"
#include "ma_logger_appender.h"
#include "ma_logger_utils.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include<syslog.h>
 
struct ma_sys_logger_s {
    ma_logger_t             logger_impl; //Logger base class
    ma_sys_logger_policy_t      policy ;

};

static ma_error_t ma_sys_logger_on_log_message(ma_logger_t *logger, ma_log_msg_t *msg);
ma_error_t ma_sys_logger_set_policy(ma_sys_logger_t *self , ma_sys_logger_policy_t *policy) ;
ma_error_t ma_sys_logger_get_policy(ma_sys_logger_t *self , const ma_sys_logger_policy_t *policy);
 static void sys_logger_release(ma_logger_t *logger);

static const ma_logger_methods_t methods = {
    &ma_sys_logger_on_log_message,  
	&ma_sys_logger_set_policy,
    &ma_sys_logger_get_policy,
    &sys_logger_release
};



ma_error_t ma_sys_logger_create(ma_sys_logger_t **logger) {

    if(logger) {
        ma_sys_logger_t *self = NULL;
    
        self = (ma_sys_logger_t *)calloc(1 , sizeof(ma_sys_logger_t));
        if(!self) return MA_ERROR_OUTOFMEMORY ;
//		ma_sys_logger_set_policy(self,&self->policy);
        ma_logger_init((ma_logger_t *)self , &methods , 1 , self);     
		openlog("ma_syslog", self->policy.options, self->policy.facility);		
        *logger = self;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG;
}


void ma_sys_logger_release(ma_sys_logger_t *logger) {
    ma_logger_release(&logger->logger_impl);
}

int  ma_sys_get_severity(ma_sys_logger_policy_t *policy)
{
	switch(policy->severity)
	{
	case MA_SYS_LOG_EMERG: 		return LOG_EMERG;
	case MA_SYS_LOG_ALERT: 		return LOG_ALERT;
	case MA_SYS_LOG_CRIT:		return LOG_CRIT;
	case MA_SYS_LOG_ERR:		return LOG_ERR;
	case MA_SYS_LOG_WARNING:	return LOG_WARNING; 
	case MA_SYS_LOG_NOTICE:		return LOG_NOTICE;
	case MA_SYS_LOG_INFO: 		return LOG_INFO;
	case MA_SYS_LOG_DEBUG:		return LOG_DEBUG;
	default:					return LOG_DEBUG;
	}
}
int  ma_sys_set_severity(ma_sys_logger_policy_t *policy)
{
	switch(policy->severity)
	{
	case MA_SYS_LOG_EMERG: 		return LOG_EMERG;
	case MA_SYS_LOG_ALERT: 		return LOG_ALERT;
	case MA_SYS_LOG_CRIT:		return LOG_CRIT;
	case MA_SYS_LOG_ERR:		return LOG_ERR;
	case MA_SYS_LOG_WARNING:	return LOG_WARNING; 
	case MA_SYS_LOG_NOTICE:		return LOG_NOTICE;
	case MA_SYS_LOG_INFO: 		return LOG_INFO;
	case MA_SYS_LOG_DEBUG:		return LOG_DEBUG;
	default:					return LOG_DEBUG;
	
	}

}

int  ma_sys_get_options(ma_sys_logger_policy_t *policy )
{
	switch(policy->options)
	{
	case MA_SYS_LOG_PID: 	return LOG_PID;
	case MA_SYS_LOG_CONS: 	return LOG_CONS;
	case MA_SYS_LOG_NDELAY: return LOG_NDELAY; 
	case MA_SYS_LOG_NOWAIT: return LOG_NOWAIT;
	case MA_SYS_LOG_ODELAY: return LOG_ODELAY;
#if !defined(SOLARIS)
	case MA_SYS_LOG_PERROR: return LOG_PERROR;
#endif
	default: 				return LOG_PID;
	}
	
}

int  ma_sys_get_facility(ma_sys_logger_policy_t *policy)
{
	switch(policy->facility)
	{
	case MA_SYS_LOG_USER: 		return LOG_USER;
	case MA_SYS_LOG_AUTH: 		return LOG_AUTH;
#if !defined(SOLARIS)
	case MA_SYS_LOG_AUTHPRIV: 	return LOG_AUTHPRIV; 
   	case MA_SYS_LOG_FTP: 		return LOG_FTP;
#endif
    case MA_SYS_LOG_CRON: 		return LOG_CRON;
	case MA_SYS_LOG_DAEMON: 	return LOG_DAEMON;
	case MA_SYS_LOG_KERN: 		return LOG_KERN;
	case MA_SYS_LOG_LPR: 		return LOG_LPR;
	case MA_SYS_LOG_MAIL: 		return LOG_MAIL;
	case MA_SYS_LOG_NEWS: 		return LOG_NEWS;
	case MA_SYS_LOG_SYSLOG: 	return LOG_SYSLOG;
	case  MA_SYS_LOG_UUCP: 		return LOG_UUCP;
	default:					return LOG_USER;
	}
	
}




static ma_error_t ma_sys_logger_on_log_message(ma_logger_t *logger, ma_log_msg_t *msg) {
    if(logger &&  msg) {
        ma_sys_logger_t *self = (ma_sys_logger_t *)logger ;    
        char *format = NULL;
        int r = 0;
        ma_temp_buffer_t b = {0};
//		self->policy.options = LOG_DEBUG;
        format = self->policy.format_pattern ? 
          self->policy.format_pattern :
			DEFAULT_SYS_LOG_FORMAT_PATTERN ;  
//		self->policy.severity = ma_sys_get_severity(self->policy.severity);
//	self->policy.severity = LOG_DEBUG;	
        ma_temp_buffer_init(&b);
		if ((r = ma_log_utils_format_msg((char *)ma_temp_buffer_get(&b), ma_temp_buffer_capacity(&b),format , msg)) < 0) {
            int chars_needed = ma_log_utils_format_msg(NULL, 0, format, msg);
            ma_temp_buffer_reserve(&b,1+chars_needed);
            r = ma_log_utils_format_msg((char *)ma_temp_buffer_get(&b), ma_temp_buffer_capacity(&b), format, msg);
        }
        if (-1 < r) { 
//			syslog(self->policy.severity,":nrh:%s\n",(char *)ma_temp_buffer_get(&b));
			syslog(self->policy.severity,"nrh:%s\n",(char *)ma_temp_buffer_get(&b));
//			syslog(LOG_DEBUG,"%s\n",(char *)ma_temp_buffer_get(&b));
        
		}
        
		
		ma_temp_buffer_uninit(&b);      
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG; 
} 

 ma_error_t ma_sys_logger_set_policy(ma_sys_logger_t *self , ma_sys_logger_policy_t *policy) 
{
  if(self && policy) {
	  self->policy.options = 0;  
	  self->policy.severity = 0;
	  self->policy.facility = 0;
  
  }

 }

ma_error_t ma_sys_logger_get_policy(ma_sys_logger_t *self , const ma_sys_logger_policy_t *policy) 
 {
 if(self && policy) {
	  self->policy.options =  ma_sys_get_options(policy);  
	  self->policy.severity = ma_sys_get_severity(policy);
	  self->policy.facility = ma_sys_get_facility(policy);
  }
}


static void sys_logger_release(ma_logger_t *logger) {    
    if(logger) {
	ma_sys_logger_t *self = (ma_sys_logger_t *)logger;
//	ma_logger_appender_release((ma_logger_appender_t *)&self->sys_appender);
	free(self);
    }
}
