#include "ma/logger/ma_logger_collector.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/logger/ma_logger_internal.h"
#include "ma/ma_log.h"

#include <stdlib.h>


struct ma_logger_collector_s {
    ma_logger_t logger_impl;
    ma_logger_t **collection;
    size_t      size;
};


static ma_error_t ma_logger_collector_on_message(ma_logger_t *logger, ma_log_msg_t *msg) {
    if(logger && msg) {
        ma_error_t err = MA_OK;
        ma_logger_collector_t *collector = (ma_logger_collector_t*)logger; //up cast
        unsigned int iter = 0 ;
       
        for(iter = 0; ((err == MA_OK) && iter<collector->size); ++iter) {
           err =  collector->collection[iter]->filter 
                            ? (MA_TRUE == ma_log_filter_accept(collector->collection[iter]->filter, msg) 
                                ? collector->collection[iter]->methods->on_log_message(collector->collection[iter], msg)
                                : MA_ERROR_LOG_FILTERED)
                            : collector->collection[iter]->methods->on_log_message(collector->collection[iter], msg);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_logger_collector_release(ma_logger_collector_t *collector) {
    return collector?ma_logger_dec_ref((ma_logger_t*)collector):MA_ERROR_INVALIDARG;
}

static void ma_logger_collector_destroy(ma_logger_t *logger) {
    ma_logger_collector_t *collector = (ma_logger_collector_t*)logger; //up cast
    if(collector) {
        if(collector->collection) {
            size_t iter = 0;
            for(iter = 0; iter < collector->size; iter++)
                (void)ma_logger_dec_ref(collector->collection[iter]);
            free(collector->collection);
        }
        free(collector);
    }
}

static const struct ma_logger_methods_s methods = {
    &ma_logger_collector_on_message,
    &ma_logger_collector_destroy
};

ma_error_t ma_logger_collector_create(ma_logger_collector_t **collector) {
    if(collector) {
        if((*collector = (ma_logger_collector_t*)calloc(1, sizeof(struct ma_logger_collector_s)))) {
            ma_logger_init((ma_logger_t*)*collector, &methods, 1, collector);
            return MA_OK;
        }
        return MA_ERROR_OUTOFMEMORY;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_logger_collector_is_logger_exist(ma_logger_collector_t *collector, ma_logger_t *logger, ma_bool_t *result) {
    if(collector && logger && result) {
        ma_error_t   err = MA_OK;
        size_t       iter = 0;
        *result = MA_FALSE;

        for(iter = 0; iter < collector->size; ++iter){
            if(collector->collection[iter] == logger) {
                *result = MA_TRUE; break;
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_logger_collector_add_logger(ma_logger_collector_t *collector, ma_logger_t *logger) {
    if(collector && logger) {
        ma_error_t err = MA_OK;
        ma_bool_t  result = MA_FALSE;
    
        if(MA_OK == (err = ma_logger_collector_is_logger_exist(collector, logger, &result))) {
            if(MA_FALSE == result) {
                err = MA_ERROR_OUTOFMEMORY;
                if((collector->collection = (ma_logger_t**)realloc(collector->collection,(sizeof(ma_logger_t*) * (collector->size + 1))))) {
                    err = ma_logger_inc_ref(collector->collection[collector->size] = logger);
                    collector->size++;
                }
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_logger_collector_remove_logger(ma_logger_collector_t *collector, ma_logger_t *logger) {
    if(collector && logger) {
        ma_error_t  err = MA_OK;
        size_t      iter = 0;

        for(iter = 0; ((iter < collector->size) && (collector->collection[iter] == logger)); ++iter) {
            if(MA_OK == (err = ma_logger_dec_ref(collector->collection[iter]))) {
                memcpy(collector->collection + iter,collector->collection + iter +1,(collector->size - iter) * sizeof(ma_logger_t*));
                collector->size--;
                break;
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_logger_collector_get_impl(ma_logger_collector_t *collector, ma_logger_t **logger) {
    return (collector && logger)?ma_logger_inc_ref(*logger = (ma_logger_t*)collector):MA_ERROR_INVALIDARG;
}

