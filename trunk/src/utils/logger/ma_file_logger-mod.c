#include "ma/logger/ma_file_logger.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal/utils/threading/ma_thread.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/logger/ma_logger_internal.h"

#include "ma_logger_appender.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>


typedef struct ma_file_appender_s {
    ma_logger_appender_t        appender_impl ; //Appender implementer
    FILE                        *fp;
    char                        *log_file_name ;
    char                        *log_base_dir;
    char                        *log_file_ext ;
    unsigned long               current_file_size ;
    ma_file_logger_policy_t     policy ;
}ma_file_appender_t ;

struct ma_file_logger_s {
    ma_logger_t                 logger_impl ; //Logger base class
    ma_file_appender_t          file_appender;
};

static ma_error_t ma_file_logger_on_log_message(ma_logger_t *logger, ma_log_msg_t *msg);
static void file_logger_release(ma_logger_t *logger);
static ma_bool_t rollover_policy_rollover_possible(ma_file_appender_t *self);
static ma_error_t rollover_policy_roll(ma_file_appender_t *self);

static const ma_logger_methods_t methods = {
    &ma_file_logger_on_log_message,
    &file_logger_release
};


static ma_error_t ma_file_appender_init( const char *log_base_dir , const char *log_file_name  , const char *log_file_ext , ma_file_appender_t *appender) ;

ma_error_t ma_file_logger_create( const char *log_base_dir , const char *log_file_name  , const char *log_file_ext  ,  ma_file_logger_t **logger) {
    if(log_file_name && logger) {
        ma_file_logger_t *self = (ma_file_logger_t *)calloc(1 , sizeof(ma_file_logger_t));
        ma_error_t rc = MA_OK ;

        if(!self)   return MA_ERROR_OUTOFMEMORY ;

        if(MA_OK != (rc = ma_file_appender_init(log_base_dir , log_file_name ,log_file_ext , &self->file_appender))) {
            free(self);
            return rc ;
        }

        ma_logger_init((ma_logger_t *)self , &methods , 1 , self);
        *logger = self;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_file_logger_release(ma_file_logger_t *logger) {
    ma_logger_release(&logger->logger_impl);
    return MA_OK;
}


ma_error_t ma_file_logger_set_policy(ma_file_logger_t *self , ma_file_logger_policy_t *policy) {
    if(self && policy) {
        return ma_logger_appender_set_policy((ma_logger_appender_t *)&self->file_appender , policy);
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_file_logger_get_policy(ma_file_logger_t *self , const ma_file_logger_policy_t **policy) {
    if(self && policy) {
        return ma_logger_appender_get_policy((ma_logger_appender_t *)&self->file_appender , (const void **)policy);
    }
    return MA_ERROR_INVALIDARG;
}



static ma_error_t ma_file_logger_on_log_message(ma_logger_t *logger, ma_log_msg_t *msg) {
    if(logger &&  msg) {
        ma_file_logger_t *self = (ma_file_logger_t *)logger ;
        char *format = self->file_appender.policy.format_pattern ? self->file_appender.policy.format_pattern :DEFAULT_FILE_LOG_FORMAT_PATTERN ;
        int r;
        ma_temp_buffer_t b = {0};
        ma_temp_buffer_init(&b);
        if ((r = ma_log_utils_format_msg((char *)ma_temp_buffer_get(&b), ma_temp_buffer_capacity(&b),format , msg)) < 0) {
            int chars_needed = ma_log_utils_format_msg(NULL, 0, format, msg);
            ma_temp_buffer_reserve(&b,1+chars_needed);
            r = ma_log_utils_format_msg((char *)ma_temp_buffer_get(&b), ma_temp_buffer_capacity(&b), format, msg);
        }
        if (-1 <= r) {
            ma_logger_appender_append((ma_logger_appender_t *)&self->file_appender , (const char *)ma_temp_buffer_get(&b));
        }
        ma_logger_pass_message_to_visitor(logger, msg);
        ma_temp_buffer_uninit(&b);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static void file_logger_release(ma_logger_t *logger) {
    ma_file_logger_t *self = (ma_file_logger_t *)logger;
    if(self) {
        ma_logger_appender_release((ma_logger_appender_t *)&self->file_appender);
        free(self);
    }
    return;
}



static ma_error_t make_file_path(const char *base_dir , const char *file_name , const char *extension , ma_temp_buffer_t *buffer) {
    char *temp_path = NULL;
    size_t len = strlen(base_dir) + sizeof(MA_PATH_SEPARATOR) + strlen(file_name) + strlen(extension);

    if(MAX_PATH_LEN < len)
        return MA_ERROR_INVALIDARG;

    ma_temp_buffer_reserve(buffer, 1+len);
    temp_path = ma_temp_buffer_get(buffer);
    if (MA_MSC_CONCAT(_,snprintf)(temp_path, ma_temp_buffer_capacity(buffer)-1, "%s%c%s%s", base_dir, MA_PATH_SEPARATOR, file_name, extension) < 0) {
        return MA_ERROR_INVALIDARG;
    }
    temp_path[len] = 0;

    return MA_OK;
}



static ma_error_t ma_file_appender_append(ma_logger_appender_t *self , const char *msg);
static ma_error_t ma_file_appender_set_policy(ma_logger_appender_t *self , void  *policy);
static ma_error_t ma_file_appender_get_policy(ma_logger_appender_t *self ,const void **policy);
static void ma_file_appender_release(ma_logger_appender_t *self);


static const ma_logger_appender_methods_t appender_methods = {
    &ma_file_appender_append,
    &ma_file_appender_set_policy,
    &ma_file_appender_get_policy,
    &ma_file_appender_release
};

static ma_error_t ma_file_appender_init(const char *log_base_dir , const char *log_file_name  , const char *log_file_ext  , ma_file_appender_t *appender) {
    ma_error_t rc = MA_OK ;
    ma_temp_buffer_t path_b = {0};
    struct stat st = {0};

    ma_temp_buffer_init(&path_b);
    /*This will validate the parameters too */
    if(MA_OK != (rc = make_file_path(log_base_dir ? log_base_dir : DEFAULT_LOG_BASE_DIR ,
        log_file_name ,
        log_file_ext ? log_file_ext: DEFAULT_LOG_FILE_EXTENSION ,
        &path_b))) {
        return rc;
    }

    //TODO - put BOM in the start of the file
    appender->fp = fopen((char *)ma_temp_buffer_get(&path_b), "a+");
    if(!appender->fp) {
        ma_temp_buffer_uninit(&path_b);
        return MA_ERROR_UNEXPECTED;
    }

    if( 0 == stat((char *)ma_temp_buffer_get(&path_b) , &st))
        appender->current_file_size = st.st_size ;

    ma_temp_buffer_uninit(&path_b);

    appender->log_file_name = strdup(log_file_name);
    appender->log_file_ext = strdup(log_file_ext ? log_file_ext: DEFAULT_LOG_FILE_EXTENSION);
    appender->log_base_dir = strdup(log_base_dir ? log_base_dir : DEFAULT_LOG_BASE_DIR );

    appender->policy.log_size_limit = DEFAULT_LOG_SIZE_LIMIT;
    appender->policy.is_rollover_enabled = MA_FALSE;

    ma_logger_appender_init((ma_logger_appender_t *)appender , &appender_methods , appender);
    return MA_OK ;
}


static ma_error_t rollover_policy_rollover(ma_file_appender_t *self) ;
static int is_rollover_policy_triggered(ma_file_appender_t *self);

static ma_error_t ma_file_appender_append(ma_logger_appender_t *appender , const char *msg ) {
    if(appender && msg) {
        ma_file_appender_t *self = (ma_file_appender_t *)appender;
        ma_error_t rc = MA_OK ;

        if(!self->fp)  return MA_ERROR_UNEXPECTED;

        //TODO - What if someone deletes the file , do we need to do the extra check ??
        if(self->policy.is_rollover_enabled && is_rollover_policy_triggered(self)) {
			if(rollover_policy_rollover_possible(self)) {
				rollover_policy_roll(self);
			}
			else {
				if(MA_OK == (rc = rollover_policy_rollover(self)))
					self->current_file_size = 0 ;
			}
        }
        if(MA_OK == rc ) {
            rc = (ma_error_t)fprintf(self->fp, "%s\n", msg);
            self->current_file_size += strlen(msg);
            fflush(self->fp);
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}


static ma_error_t ma_file_appender_set_policy(ma_logger_appender_t *appender  , void *policy_appender)  {
    ma_file_appender_t *self = (ma_file_appender_t *)appender;
    ma_file_logger_policy_t *policy = (ma_file_logger_policy_t *) policy_appender ;
    if(self && policy) {
        if(policy->format_pattern) {
            if(self->policy.format_pattern) free(self->policy.format_pattern);
            self->policy.format_pattern = strdup(policy->format_pattern);
        }
        self->policy.log_size_limit = policy->log_size_limit ;
        self->policy.is_rollover_enabled = policy->is_rollover_enabled ;
        if(self->policy.is_rollover_enabled) {
            self->policy.rollover_policy.max_number_of_rollovers = policy->rollover_policy.max_number_of_rollovers;
            if(policy->rollover_policy.rollover_file_suffix) {
                if(self->policy.rollover_policy.rollover_file_suffix)   free(self->policy.rollover_policy.rollover_file_suffix);
                self->policy.rollover_policy.rollover_file_suffix = strdup(policy->rollover_policy.rollover_file_suffix);
            }
        }
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_file_appender_get_policy(ma_logger_appender_t *appender ,const void  **policy) {
    ma_file_appender_t *self = (ma_file_appender_t *)appender;
    if(self && policy) {
        *policy = (ma_file_logger_policy_t * )&self->policy ;
        return MA_OK ;
    }
      return MA_ERROR_INVALIDARG;

}

static void ma_file_appender_release(ma_logger_appender_t *appender) {
    ma_file_appender_t *self = (ma_file_appender_t *)appender;

    //TODO - may be we don't need to check before freeing
    if(self->fp) fclose(self->fp);
    if(self->log_base_dir) free(self->log_base_dir);
    if(self->log_file_ext) free(self->log_file_ext);
    if(self->log_file_name) free(self->log_file_name);

    if(self->policy.format_pattern) free(self->policy.format_pattern);
    if(self->policy.is_rollover_enabled) {
        if(self->policy.rollover_policy.rollover_file_suffix)
            free(self->policy.rollover_policy.rollover_file_suffix);
    }
}
static int is_rollover_policy_triggered(ma_file_appender_t *self) {
    //TODO - We need to keep some extra space for keeping the message that we are rotating the log
    return self->current_file_size >= self->policy.log_size_limit * 1024 * 1024;     //Size limit is in MB
}


#define ROLLOVER_FILE_NAME_DELIMITER    '_'


/* Rollover file format
    <LOG_BASE_DIR><PATH_SEPARATOR><LOG_FILE_NAME>_<ROLLOVER_FILE_SUFFIX>_<(MAX_NUMBER_OF_ROLLOVER--)><LOG_FILE_EXTENSION>
*/
static ma_error_t  initialize_rollback_filename(ma_file_appender_t *self , int roll_index , ma_temp_buffer_t *buffer) {
    size_t len = 0 ;
    char *p_buffer = NULL ;

    len = strlen(self->log_base_dir) + 1 +  strlen(self->log_file_name) + 1 +
          strlen(self->policy.rollover_policy.rollover_file_suffix ? self->policy.rollover_policy.rollover_file_suffix : DEFAULT_ROLLOVER_FILE_SUFFIX )
          + 1 + 32 + strlen(self->log_file_ext)  + 1;

    if(MAX_PATH_LEN < len)
        return MA_ERROR_INVALIDARG;

    ma_temp_buffer_reserve(buffer , len);
    p_buffer = (char *)ma_temp_buffer_get(buffer);

    strncpy(p_buffer , self->log_base_dir , len - 1);
    p_buffer[strlen(p_buffer)] = MA_PATH_SEPARATOR;
    strncat(p_buffer , self->log_file_name , len - 1);
    p_buffer[strlen(p_buffer)] = ROLLOVER_FILE_NAME_DELIMITER;
    strncat(p_buffer ,  self->policy.rollover_policy.rollover_file_suffix ? self->policy.rollover_policy.rollover_file_suffix : DEFAULT_ROLLOVER_FILE_SUFFIX , len - 1);
    p_buffer[strlen(p_buffer)] = ROLLOVER_FILE_NAME_DELIMITER;
    sprintf(p_buffer + strlen(p_buffer) , "%d" , roll_index);
    strncat(p_buffer , self->log_file_ext , len - 1);

    return MA_OK ;
}

ma_bool_t rollover_policy_rollover_possible(ma_file_appender_t *self) {
	if(self) {
		ma_error_t err = MA_OK;
		ma_bool_t rotate = MA_TRUE;
		int i = 0;

        for(i = self->policy.rollover_policy.max_number_of_rollovers ; i > 0 ; i--) {
            struct stat st = {0};
            ma_temp_buffer_t new_file_name_b = MA_TEMP_BUFFER_INIT;
            char *new_file_name = NULL ;

            ma_temp_buffer_init(&new_file_name_b);
            if(MA_OK == (err = initialize_rollback_filename(self , i , &new_file_name_b))) {
                new_file_name = (char*)ma_temp_buffer_get(&new_file_name_b);
				if(stat(new_file_name , &st)) {
					rotate = MA_FALSE;
				}
			}
			ma_temp_buffer_uninit(&new_file_name_b);
		}

		return rotate;
	}
	return MA_FALSE;
}

ma_error_t rollover_policy_roll(ma_file_appender_t *self) {
	if(self) {
		ma_error_t err = MA_OK;
		int i = 0;				
		ma_temp_buffer_t file_name_b = MA_TEMP_BUFFER_INIT ;
		char *file_name = NULL;

		ma_temp_buffer_init(&file_name_b);
		if(MA_OK == (err = make_file_path(self->log_base_dir , self->log_file_name , self->log_file_ext , &file_name_b))) {
			file_name = (char*)ma_temp_buffer_get(&file_name_b);
			for(i = self->policy.rollover_policy.max_number_of_rollovers ; i > 0 ; i--) {
				ma_temp_buffer_t temp_file_b = MA_TEMP_BUFFER_INIT;
				ma_temp_buffer_t left_file_b = MA_TEMP_BUFFER_INIT;
				ma_temp_buffer_t right_file_b = MA_TEMP_BUFFER_INIT;
				char *left_file = NULL, *right_file = NULL, *temp_file = NULL;
				
				ma_temp_buffer_init(&left_file_b);
				ma_temp_buffer_init(&right_file_b);
				ma_temp_buffer_init(&temp_file_b);
				if(MA_OK ==(err = initialize_rollback_filename(self , i , &right_file_b))) {
					right_file = (char*)ma_temp_buffer_get(&right_file_b);
					if(1 == i ) {
						unlink(right_file);
						fclose(self->fp)  , self->fp = NULL ;
						rename(file_name, right_file);
					} else {
						if(MA_OK == (err = initialize_rollback_filename(self , i -1  , &left_file_b)))
							left_file = (char*)ma_temp_buffer_get(&left_file_b);
						if(MA_OK == (err = initialize_rollback_filename(self , i -1  , &temp_file_b)))
							temp_file = (char*)ma_temp_buffer_get(&temp_file_b); /* tmp file holding left file */
						if(left_file && temp_file && right_file) {
							err = rename(left_file, right_file);
							err = rename(right_file, temp_file);
						}
					}
				}
				ma_temp_buffer_uninit(&temp_file_b);
				ma_temp_buffer_uninit(&left_file_b);
				ma_temp_buffer_uninit(&right_file_b);
			}
		}
		if(MA_OK == err ) {
			self->fp = fopen(file_name , "w");
			if(self->fp)
				self->current_file_size = 0 ;
			else
				err = MA_ERROR_UNEXPECTED ;
		}
		ma_temp_buffer_uninit(&file_name_b);

		return err;
	}
	return MA_ERROR_INVALIDARG;
}

static ma_error_t rollover_policy_rollover(ma_file_appender_t *self) {
    ma_error_t rc = MA_OK ;
    ma_temp_buffer_t file_name_b = {0} ;

    ma_temp_buffer_init(&file_name_b);
    rc = make_file_path(self->log_base_dir , self->log_file_name , self->log_file_ext , &file_name_b);

    if(MA_OK == rc ) {
        int i = 0 ;
        char *file_name =  (char *)ma_temp_buffer_get(&file_name_b);
        for(i = self->policy.rollover_policy.max_number_of_rollovers ; i > 0 ; i--) {
            struct stat st = {0};
            ma_temp_buffer_t old_file_name_b = {0} , new_file_name_b = {0};
            char *old_file_name = NULL , *new_file_name = NULL ;
            ma_temp_buffer_init(&new_file_name_b);
            ma_temp_buffer_init(&old_file_name_b);
            rc = initialize_rollback_filename(self , i , &new_file_name_b) ;
            if(MA_OK == rc) {
                new_file_name = (char*)ma_temp_buffer_get(&new_file_name_b);
                if(1 != i ) {
                    rc = initialize_rollback_filename(self , i -1  , &old_file_name_b) ;
                    old_file_name = (char*)ma_temp_buffer_get(&old_file_name_b) ;
                }
                else {
                    fclose(self->fp)  , self->fp = NULL ;
                    old_file_name = file_name ;
                }
                if((MA_OK == rc) && (0 == stat(old_file_name , &st)))
                        rc = rename(old_file_name , new_file_name);
            }
            ma_temp_buffer_uninit(&new_file_name_b);
            ma_temp_buffer_uninit(&old_file_name_b);

        }
        if(MA_OK == rc ) {
            self->fp = fopen(file_name , "w");
            if(self->fp)
                self->current_file_size = 0 ;
            else
                rc = MA_ERROR_UNEXPECTED ;
        }
    }
    ma_temp_buffer_uninit(&file_name_b);
    return rc ;
}





