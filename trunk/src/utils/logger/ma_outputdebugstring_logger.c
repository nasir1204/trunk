#include "ma/logger/ma_outputdebugstring_logger.h"
#include "ma/internal/utils/logger/ma_logger_internal.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"

#include <stdlib.h>

#ifdef MA_WINDOWS
# include <windows.h>
# define EMIT(t,f,s) OutputDebugStringA(t)
#else
# define EMIT(t,f,s) ((void)(0)) 
#endif

static ma_error_t on_log_message(ma_logger_t *self, ma_log_msg_t *msg);
static void destroy(ma_logger_t *self);

static const ma_logger_methods_t methods = {on_log_message, destroy};

MA_LOGGER_API ma_error_t ma_outputdebugstring_logger_create(ma_logger_t **logger) {
    ma_logger_t *self = calloc(1, sizeof(ma_logger_t));
    if (self) {
        ma_logger_init(self, &methods, 1, 0);
        *logger = self;
        return MA_OK;
    }

    return MA_ERROR_OUTOFMEMORY;
}

static ma_error_t on_log_message(ma_logger_t *self, ma_log_msg_t *msg) {
    ma_temp_buffer_t tempbuffer = MA_TEMP_BUFFER_INIT;
    
    ma_log_utils_format_msg(ma_temp_buffer_get(&tempbuffer), ma_temp_buffer_capacity(&tempbuffer), "%+t %f.%s : %m\n", msg);

    EMIT(ma_temp_buffer_get(&tempbuffer), msg->facility, msg->severity);
    return MA_OK;
}

static void destroy(ma_logger_t *self) {

}
