#include "ma/internal/utils/logger/ma_logger_internal.h"
#include <string.h>
#include <wchar.h>



ma_error_t ma_log_msg_get_severity(ma_log_msg_t *self, ma_log_severity_t *severity) {
    if(self && severity) {
        *severity = self->severity;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_log_msg_get_severity_str(ma_log_msg_t *self, ma_buffer_t **severity_str) {
    if(self && severity_str) {
        const char *str = ma_log_get_severity_label(self->severity);
        if(str) {
            ma_error_t rc = MA_OK;
            ma_int32_t len = strlen(str);
            if(MA_OK == (rc = ma_buffer_create(severity_str, len)))
                rc = ma_buffer_set(*severity_str, str, len);
            return rc;
        }
        return MA_ERROR_OBJECTNOTFOUND;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_log_msg_get_facility(ma_log_msg_t *self, ma_buffer_t **facility) {
    if(self && facility) {
        if(self->facility) {
            ma_error_t rc = MA_OK;
            ma_int32_t len = strlen(self->facility);
            if(MA_OK == (rc = ma_buffer_create(facility, len)))
                rc = ma_buffer_set(*facility, self->facility, len);
            return rc;
        }
        return MA_ERROR_OBJECTNOTFOUND;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_log_msg_is_wchar_message(ma_log_msg_t *self, ma_bool_t *is_wide_char) {
    if(self && is_wide_char) {
        *is_wide_char = self->msg_is_wide_char;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_log_msg_get_wchar_message(ma_log_msg_t *self, ma_wbuffer_t **message) {
    if(self && message) {
        if(self->message.wchar_message) {
            ma_error_t rc = MA_OK;
            ma_int32_t len = wcslen(self->message.wchar_message);
            if(MA_OK == (rc = ma_wbuffer_create(message, len)))
                rc = ma_wbuffer_set(*message, self->message.wchar_message, len);
            return rc;
        }
        return MA_ERROR_OBJECTNOTFOUND;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_log_msg_get_message(ma_log_msg_t *self, ma_buffer_t **message) {
    if(self && message) {
        if(self->message.utf8_message) {
            ma_error_t rc = MA_OK;
            ma_int32_t len = strlen(self->message.utf8_message);
            if(MA_OK == (rc = ma_buffer_create(message, len)))
                rc = ma_buffer_set(*message, self->message.utf8_message, len);
            return rc;
        }
        return MA_ERROR_OBJECTNOTFOUND;
    }
    return MA_ERROR_INVALIDARG;
}

