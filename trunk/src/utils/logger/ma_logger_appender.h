#ifndef MA_LOGGER_APPENDER_H_INCLUDED
#define MA_LOGGER_APPENDER_H_INCLUDED

#include "ma/ma_common.h"

#ifdef MA_THREADING_ENABLED
    #include "ma/internal/utils/threading/ma_thread.h"
    #define MA_MUTEX_INIT(x) ma_mutex_init(&x->mutex)
    #define MA_MUTEX_LOCK(x)   ma_mutex_lock(&x->mutex)
    #define MA_MUTEX_UNLOCK(x) ma_mutex_unlock(&x->mutex)
    #define MA_MUTEX_DESTROY(x) ma_mutex_destroy(&x->mutex)
#else
    #define MA_MUTEX_INIT(x) /* NOP */
    #define MA_MUTEX_LOCK(x)   /* NOP */
    #define MA_MUTEX_UNLOCK(x) /* NOP */
    #define MA_MUTEX_DESTROY(x)/* NOP */
#endif

MA_CPP(extern "C" {)

typedef struct ma_logger_appender_s ma_logger_appender_t ;

typedef struct ma_logger_appender_methods_s {    
    ma_error_t (*append)(ma_logger_appender_t *self , const char *msg);
    ma_error_t (*set_policy)(ma_logger_appender_t *self  , void *policy) ; /* If appender have some policies , optional */
    ma_error_t (*get_policy)(ma_logger_appender_t *self  , const void **policy) ; /* If appender have some policies , optional */
    void (*release)(ma_logger_appender_t *self); /*< virtual destructor */
} ma_logger_appender_methods_t;


struct ma_logger_appender_s {
    ma_logger_appender_methods_t const *methods;   /*< v-table */
    void                                *data ; /* extension */
#ifdef MA_THREADING_ENABLED
    ma_mutex_t                         mutex;
#endif
};



static MA_INLINE void ma_logger_appender_init(ma_logger_appender_t *appender, ma_logger_appender_methods_t  const *methods,  void *data) {
    appender->methods = methods;
    appender->data = data;
    #ifdef MA_THREADING_ENABLED
        MA_MUTEX_INIT(appender);
    #endif
}

static MA_INLINE ma_error_t ma_logger_appender_append(ma_logger_appender_t *appender , const char *msg) {
    if(appender) {
        if(appender->methods->append) {
            ma_error_t rc = MA_OK ;
            #ifdef MA_THREADING_ENABLED
                MA_MUTEX_LOCK(appender);
            #endif
            rc = appender->methods->append(appender, msg);
            #ifdef MA_THREADING_ENABLED
                MA_MUTEX_UNLOCK(appender);
            #endif 
            return rc ;
        }
    }
    return MA_ERROR_UNEXPECTED;
 }

static MA_INLINE void ma_logger_appender_release(ma_logger_appender_t *appender) {
    if(appender) {       
        if(appender->methods->release) {
            #ifdef MA_THREADING_ENABLED
                MA_MUTEX_LOCK(appender);
            #endif
            appender->methods->release(appender);
            #ifdef MA_THREADING_ENABLED
                MA_MUTEX_UNLOCK(appender);            
            #endif
        }
        #ifdef MA_THREADING_ENABLED
            MA_MUTEX_DESTROY(appender);
        #endif
        
    }
}


static MA_INLINE ma_error_t ma_logger_appender_set_policy(ma_logger_appender_t *appender , void *policy) {
    if(appender) {
        if(appender->methods->set_policy) {
            ma_error_t rc = MA_OK ;
            #ifdef MA_THREADING_ENABLED
                MA_MUTEX_LOCK(appender);
            #endif
            rc = appender->methods->set_policy(appender, policy);
            #ifdef MA_THREADING_ENABLED
                MA_MUTEX_UNLOCK(appender);
            #endif 
            return rc ;
        }
    }
    return MA_ERROR_UNEXPECTED;
 }

static MA_INLINE ma_error_t ma_logger_appender_get_policy(ma_logger_appender_t *appender , const void **policy) {
    if(appender) {
        if(appender->methods->get_policy) {
            ma_error_t rc = MA_OK ;
            #ifdef MA_THREADING_ENABLED
                MA_MUTEX_LOCK(appender);
            #endif
            rc = appender->methods->get_policy(appender, policy);
            #ifdef MA_THREADING_ENABLED
                MA_MUTEX_UNLOCK(appender);
            #endif 
            return rc ;
        }
    }
    return MA_ERROR_UNEXPECTED;
 }

MA_CPP(})

#endif //#ifndef MA_LOGGER_APPENDER_H_INCLUDED

