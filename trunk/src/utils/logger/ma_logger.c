#include "ma/logger/ma_logger.h"
#include "ma/internal/utils/portability/stdarg.h"
#include "ma/internal/utils/threading/ma_atomic.h"
#include "ma/internal/utils/logger/ma_logger_internal.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/text/ma_utf8.h"
#include <stdio.h>
#include <assert.h>
#ifdef MA_WINDOWS
#include <Windows.h>
#else
#include <stdio.h>
#include <wchar.h>
#endif

ma_error_t ma_logger_inc_ref(ma_logger_t *self) {
    if (self && (0 <= self->ref_count)) {
        MA_ATOMIC_INCREMENT(self->ref_count);
    }
    return MA_OK;
}

ma_error_t ma_logger_dec_ref(ma_logger_t *self) {
    if (self && (0 <= self->ref_count)) {
        if (0 == MA_ATOMIC_DECREMENT(self->ref_count)) {
            self->methods->release(self);
        }
    }
    return MA_OK;
}

ma_error_t ma_logger_add_filter(ma_logger_t *self, ma_log_filter_t *filter) {
    if (!self || !filter) {
        return MA_ERROR_INVALIDARG;
    }
    self->filter = filter;
    return MA_OK;
}

ma_error_t ma_logger_remove_filter(ma_logger_t *self) {
    if (!self) {
        return MA_ERROR_INVALIDARG;
    }
    self->filter = NULL;
    return MA_OK;
}

ma_error_t ma_logger_get_filter(ma_logger_t *self,  ma_log_filter_t **filter) {
    if (!self || !filter) {
        return MA_ERROR_INVALIDARG;
    }
    *filter = self->filter;
    return MA_OK;
}

/* Maximum size (in chars) of buffer we allocate from heap */
#define LOG_MAX_BUFFER_SIZE 2048

ma_error_t ma_log_v(ma_logger_t          *logger,
                    int severity,
                    const char           *module,
                    const char           *file,
                    int                  line,
                    const char           *func,
                    const char           *fmt,va_list ap) {
    int iterations;
    ma_temp_buffer_t temp_buffer = {0};
    ma_temp_buffer_t fmt_buf = MA_TEMP_BUFFER_INIT;
    ma_log_msg_t log_msg = {0};
    const char *pfmt = fmt;
    va_list ap_copy;

    memset(&log_msg, 0, sizeof(ma_log_msg_t));
    if (!logger) {
        return MA_ERROR_INVALIDARG;
    }

    if (MA_BYTE_CHECK_BIT(severity, MA_LOG_FLAG_LOCALIZE)) {
        if (logger->localizer_cb) {
            ma_buffer_t *buffer = NULL;

            ma_temp_buffer_reserve(&fmt_buf, strlen(pfmt));
            strncpy((char*)ma_temp_buffer_get(&fmt_buf), pfmt, strlen(pfmt));

            if ((MA_OK == logger->localizer_cb((const char *)ma_temp_buffer_get(&fmt_buf), &buffer, logger->localizer_cb_data) && buffer)) {
                const char *str = NULL;
                size_t size = 0;
                if (MA_OK == ma_buffer_get_string(buffer, &str, &size)) {

                    ma_temp_buffer_uninit(&fmt_buf);
                    ma_temp_buffer_init(&fmt_buf);
                    ma_temp_buffer_reserve(&fmt_buf, size+1);
                    strncpy((char*)ma_temp_buffer_get(&fmt_buf), str, size);
                    pfmt = (const char*)ma_temp_buffer_get(&fmt_buf);
                }
                ma_buffer_release(buffer);
            }
        }
        log_msg.is_localized = 1;
        MA_BYTE_CLEAR_BIT(severity, MA_LOG_FLAG_LOCALIZE);
    }

    ma_temp_buffer_init(&temp_buffer);

    ma_log_msg_init(&log_msg, (ma_log_severity_t)severity, module, file, line, func);

    /*
    Windows:
    _vsnprintf and _vsnwprintf return the number of characters written, not including the terminating null character,
    or a negative value if an output error occurs.
    If the number of characters to write exceeds count, then count characters are written and �1 is returned.

    Others:
    Upon successful return, these functions return the number of characters printed (not including the trailing '\0' used to end output to strings).
    The functions snprintf and vsnprintf do not write more than size bytes (including the trailing '\0').
    If the output was truncated due to this limit then the return value is the number of characters (not including the trailing '\0') which would have been written to the final string if enough space had been available.
    Thus, a return value of size or more means that the output was truncated. (See also below under NOTES.)
    If an output error is encountered, a negative value is returned.
    */

    for (iterations = 0; ; ++iterations) {
        size_t buf_size = ma_temp_buffer_capacity(&temp_buffer);
        char *p = (char *)ma_temp_buffer_get(&temp_buffer);
        int result;
        va_copy(ap_copy, ap);
        result = vsnprintf(p, buf_size, pfmt, ap_copy);
        va_end(ap_copy);

        if (-1 < result && result < buf_size) {
            /* got what we came for*/
            break;
        }

#ifdef MA_WINDOWS
        /* msvcrt provides _scprintf/_vscprintf which calculates the requested # of chars */
        if (iterations) {
            /* second time through and still a problem; use truncated buffer (or maybe bail)!*/
            p[buf_size-1] = 0;
            break;
        }

        if (result == buf_size) {
            buf_size = 1+result;
        } else {
            va_copy(ap_copy, ap);
            buf_size = 1 + _vscprintf(pfmt, ap_copy);
            va_end(ap_copy);
        }
#else
        if (buf_size <= result) {
            /* most unixes will return the length needed, excluding null term so we use that */
            if (iterations) {
                /* second time through and still a problem; use truncated buffer (or maybe bail)!*/
                p[buf_size-1] = 0;
                break;
            } else {
                /* try the bigger buffer up to LOG_MAX_BUFFER_SIZE */
                buf_size = (++result < LOG_MAX_BUFFER_SIZE) ? result : LOG_MAX_BUFFER_SIZE;
            }
        } else { /*result < 0, not sure when we get that */
            if (EILSEQ == errno) {
                return MA_ERROR_UNEXPECTED;
            }

#ifdef MA_HAS_VASPRINTF /* Guard for vasprintf availability ... */
            assert(0==iterations);

            /* note, there is no way to limit the number of chars that vasprintf will use */
            va_copy(ap_copy, ap);
            result = vasprintf((char **)ma_temp_buffer_address_of(&temp_buffer, &free), pfmt, ap_copy);
            va_end(ap_copy);
            if (result < 0) {
                return MA_ERROR_UNEXPECTED;
            }
            buf_size = 1+result;
            break;
#else
            if (LOG_MAX_BUFFER_SIZE < (buf_size *= 2)) {
                return MA_ERROR_UNEXPECTED;    /*bail if this requires too big of a buffer*/
            }
#endif /* MA_HAS_VASPRINTF */
        }
#endif /* MA_WINDOWS */
        /* (re)allocate a new buffer and try again */
        ma_temp_buffer_reserve(&temp_buffer, buf_size);
    }

    log_msg.message.utf8_message = ma_temp_buffer_get(&temp_buffer);

    (void)ma_logger_log_message(logger, &log_msg);

    ma_temp_buffer_uninit(&temp_buffer);
    ma_temp_buffer_uninit(&fmt_buf);

    return MA_OK;
}

#ifdef MA_HAS_WCHAR_T

/* TODO - Can we do some majic here so that size for wide char support changes
or even we can ask user to pass the static buffer to the temp buffer class

#undef MA_TEMP_BUFFER_STATIC_CAPACITY
#define MA_TEMP_BUFFER_STATIC_CAPACITY 256 * sizeof(whar_t)
*/



ma_error_t ma_wlog_v(ma_logger_t          *logger,
                     int severity,
                     const char           *module,
                     const char           *file,
                     int                  line,
                     const char           *func,
                     const ma_wchar_t     *fmt, va_list ap) {
    int iterations;
    ma_temp_buffer_t temp_buffer = {0};
    ma_log_msg_t log_msg;
    ma_temp_buffer_t fmt_buf = MA_TEMP_BUFFER_INIT;
    const wchar_t *pfmt = fmt;
    va_list ap_copy;

    if (!logger) {
        return MA_ERROR_INVALIDARG;
    }



    if (MA_BYTE_CHECK_BIT(severity, MA_LOG_FLAG_LOCALIZE))  {
        if (logger->localizer_cb && (MA_OK == ma_convert_wide_char_to_utf8(pfmt, &fmt_buf))) {
            ma_buffer_t *buffer = NULL;
            if ((MA_OK == logger->localizer_cb((const char *)ma_temp_buffer_get(&fmt_buf), &buffer, logger->localizer_cb_data)) && buffer) {
                const char *str = NULL;
                size_t size = 0;
                if (MA_OK == ma_buffer_get_string(buffer, &str, &size)) {
                    ma_temp_buffer_uninit(&fmt_buf);
                    ma_temp_buffer_init(&fmt_buf);
                    if (MA_OK == ma_convert_utf8_to_wide_char(str, &fmt_buf)) {
                        pfmt = (const wchar_t*)ma_temp_buffer_get(&fmt_buf);

                    }
                }
                ma_buffer_release(buffer);
            }
        }
        log_msg.is_localized = 1;
        MA_BYTE_CLEAR_BIT(severity, MA_LOG_FLAG_LOCALIZE);
    }

    ma_log_msg_init(&log_msg, (ma_log_severity_t)severity, module, file, line, func);
    ma_temp_buffer_init(&temp_buffer);
    /*
    Windows:
    _vsnprintf and _vsnwprintf return the number of characters written, not including the terminating null character,
    or a negative value if an output error occurs.
    If the number of characters to write exceeds count, then count characters are written and �1 is returned.

    Others:
    Upon successful return, these functions return the number of characters printed (not including the trailing '\0' used to end output to strings).
    The functions snprintf and vsnprintf do not write more than size bytes (including the trailing '\0').
    If the output was truncated due to this limit then the return value is the number of characters (not including the trailing '\0') which would have been written to the final string if enough space had been available.
    Thus, a return value of size or more means that the output was truncated. (See also below under NOTES.)
    If an output error is encountered, a negative value is returned.
    */


    for (iterations = 0; ; ++iterations) {
        size_t buf_size = ma_temp_buffer_capacity(&temp_buffer);
        wchar_t *p = (wchar_t *)ma_temp_buffer_get(&temp_buffer);
        int result;

        va_copy(ap_copy, ap);
        result = MA_MSC_SELECT(_vsnwprintf,vswprintf)(p, buf_size/sizeof(wchar_t), pfmt, ap_copy);
        va_end(ap_copy);


        if (-1 < result && result < buf_size) {
            /* got what we came for*/
            break;
        }
#ifdef MA_WINDOWS
        /* msvcrt provides _scwprintf/_vscwprintf which calculates the requested # of chars */
        if (iterations) {
            /* second time through and still a problem; use truncated buffer (or maybe bail)!*/
            p[buf_size-1] = 0;
            break;
        }

        if (result == buf_size) {
            buf_size = 1+result;
        } else {
            va_copy(ap_copy, ap);
            buf_size = 1 + _vscwprintf(pfmt, ap_copy);
            va_end(ap_copy);
        }
#else
        /* Illegal character sequence */
        if (EILSEQ == errno) {
            return MA_ERROR_UNEXPECTED ;
        }

        /* vswprintf will never return the number of characters needed , it just returns -1
        No such API to get the size of the buffer *nix
        */
        //Try doubling the buffer size
        if (LOG_MAX_BUFFER_SIZE < (buf_size *= 2)) {
            return MA_ERROR_UNEXPECTED;    /*bail if this requires too big of a buffer*/
        }

#endif /* MA_WINDOWS */

        /* (re)allocate a new buffer and try again */
        ma_temp_buffer_reserve(&temp_buffer, buf_size * sizeof(wchar_t));
    }

    log_msg.msg_is_wide_char = 1;
    log_msg.message.wchar_message = (wchar_t *)ma_temp_buffer_get(&temp_buffer);;

    (void)ma_logger_log_message(logger, &log_msg);

    ma_temp_buffer_uninit(&temp_buffer);
    ma_temp_buffer_uninit(&fmt_buf);

    return MA_OK;
}

#endif /* MA_HAS_WCHAR_T */


ma_error_t  ma_log(ma_logger_t          *self,
                   int severity,
                   const char           *module,
                   const char           *file,
                   int                  line,
                   const char           *func,
                   const char           *fmt,...) {
    va_list arg;
    ma_error_t rc = MA_OK;

    va_start(arg, fmt);
    rc = ma_log_v(self, severity, module, file, line, func, fmt, arg);
    va_end(arg);
    return rc;
}


ma_error_t  ma_wlog(ma_logger_t          *self,
                    int severity,
                    const char           *module,
                    const char           *file,
                    int                  line,
                    const char           *func,
                    const ma_wchar_t     *fmt,...) {
    va_list arg;
    ma_error_t rc = MA_OK;

    va_start(arg, fmt);
    rc = ma_wlog_v(self, severity, module, file, line, func, fmt, arg);
    va_end(arg);
    return rc;
}


ma_error_t ma_logger_register_visitor(ma_logger_t *logger, ma_logger_on_visit_cb_t cb, void *data) {
    if (logger && cb) {
        logger->visitor_cb = cb;
        logger->vistor_cb_data = data;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_logger_register_localizer(ma_logger_t *logger, ma_logger_on_localize_cb_t cb, void *data) {
    if (logger && cb) {
        logger->localizer_cb = cb;
        logger->localizer_cb_data = data;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_logger_unregister_visitor(ma_logger_t *logger) {
    if (logger) {
        logger->visitor_cb = NULL;
        logger->vistor_cb_data = NULL;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_logger_unregister_localizer(ma_logger_t *logger) {
    if (logger) {
        logger->localizer_cb = NULL;
        logger->localizer_cb_data = NULL;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

void ma_logger_pass_message_to_visitor(ma_logger_t *logger,  ma_log_msg_t *log_msg) {
    if (logger && log_msg && logger->visitor_cb) {
        logger->visitor_cb(log_msg, logger->vistor_cb_data);
    }
}

