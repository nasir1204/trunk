#include "ma/logger/ma_log_filter.h"
#include "ma/internal/utils/logger/ma_logger_internal.h"
#include "ma/internal/ma_macros.h"
#include <stdlib.h>
#include <string.h>

#ifndef MA_WINDOWS
#include <strings.h>
#endif

typedef struct ma_generic_log_filter_s {
    ma_log_filter_t         filter_impl ; //Filter implementation
    char                    *filter_pattern;  
    char                    **rules;
    unsigned short int      n_rules;
}ma_generic_log_filter_t;


static ma_bool_t ma_generic_log_filter_accept(ma_log_filter_t *filter, ma_log_msg_t *msg);
static void ma_generic_log_filter_destroy(ma_log_filter_t *filter);

static const ma_log_filter_methods_t methods = {
    &ma_generic_log_filter_accept,  
    &ma_generic_log_filter_destroy
};


/*
Filter Pattern 
 Each rule(separated by '|' ),
 'aRule' is in the form [~]x.y where x is * or facility, y is severity string and ~means negate.    

 e.g. 
 1. MsgBus.Info 
    will allow only message from msgbus facility and severity level is greater than or equal to Info 
 2. *.Info 
    will allow all the messages whose severity level is greater than or equal to Info 
 3. ~Msgbus.*
    will allow all the messages where facility is not msgbus
 4. msgbus.info|network.debug
    will allow messages from msgbus facility where msgbus severity level is <= Info
        OR
    will allow message from the network facility where network severity level is <= Info
*/


ma_error_t ma_generic_log_filter_create(const char *filter_pattern , ma_log_filter_t **filter) {
    if(filter) {
        ma_generic_log_filter_t *self = (ma_generic_log_filter_t *) calloc(1 , sizeof(ma_generic_log_filter_t));        
        if(!self)   return MA_ERROR_OUTOFMEMORY ;      

        //TODO - check the sanity of pattern , e.g. length 
        if(filter_pattern) {
            char *tokenize = NULL  , *context = NULL ,  *a_rule = NULL;            

            /*Count the number of rules */
            tokenize = strdup(filter_pattern);
            for(a_rule =  MA_MSC_SELECT(strtok_s, strtok_r)(tokenize , "|"  , &context) ; a_rule  ;
                a_rule = MA_MSC_SELECT(strtok_s, strtok_r)(NULL , "|"  , &context) , self->n_rules++) 
                ;
            
            free(tokenize);

            /*Assign the rules pointer separate */
            self->rules = (char **)calloc(self->n_rules , sizeof(char*));
            if(!self->rules) {
                free(self);
                return MA_ERROR_OUTOFMEMORY ;
            }
            else {
                int i = 0 ;
                tokenize = self->filter_pattern = strdup(filter_pattern);
                for(a_rule =  MA_MSC_SELECT(strtok_s, strtok_r)(tokenize , "|"  , &context) ; a_rule  ;
                    a_rule = MA_MSC_SELECT(strtok_s, strtok_r)(NULL , "|"  , &context) , ++i)  {
                        self->rules[i] = a_rule ;
                }                            
            }
        }
            
        self->filter_impl.methods = &methods;
        self->filter_impl.data = self ;
        *filter = (ma_log_filter_t *)self;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG;
}

void ma_generic_log_filter_destroy(ma_log_filter_t *filter) {  
    ma_generic_log_filter_t *self = (ma_generic_log_filter_t *)filter;
    if(self) {
        if(self->filter_pattern) free(self->filter_pattern);
        if(self->rules) free(self->rules);
        free(self);
    }
}

static int rule_matches(const char *rule, const ma_log_msg_t *msg);


ma_bool_t ma_generic_log_filter_accept(ma_log_filter_t  *filter, ma_log_msg_t *msg) {
    ma_generic_log_filter_t *self = (ma_generic_log_filter_t *)filter;
    if(self && msg) {
        /*If there are any rules else match everything */
        if(self->n_rules > 0 ) {
            int i = 0 ;             
            unsigned short int b_found_match = 0;
            
            for(; i < self->n_rules ; ++i) {
                char *a_rule = self->rules[i]; 
                unsigned short int b_negate = 0 ; 

                if('~'  == a_rule[0] ) {
                    ++a_rule ;
                    b_negate = 1 ;
                }

                // no need to evaluate as we already have at least one matching rule
                if(b_found_match && !b_negate)  continue ;

                b_found_match  = rule_matches(a_rule , msg) ;

                if(b_found_match && b_negate )      return MA_FALSE; // found a negative rule, we are done

                if(!b_found_match && b_negate) b_found_match = MA_TRUE;
                // found a matching rule, still need to look through the rest to see if there is a negative rule
            }

            return b_found_match  ? MA_TRUE : MA_FALSE;
        }
        return MA_TRUE;
    }
    return MA_FALSE;
}


//! Determines if a single rule matches
static int rule_matches(const char *rule, const ma_log_msg_t *msg)  {
    //rule is in the form of "facility.severity"
    const char *delimiter = strchr(rule , '.') ;
    size_t  c_facility_chars = 0 ;
    if(NULL == delimiter )  return 0 ;
        
    //Attempt to match the facility 
    c_facility_chars = delimiter - rule ;
    if( (0 != MA_MSC_SELECT(strnicmp, strncasecmp)("*" , rule ,  c_facility_chars)) && (0 != MA_MSC_SELECT(strnicmp, strncasecmp)(msg->facility , rule , c_facility_chars)))
        return 0;

    //facility matched , Attempt to match the severity    
    if(!(++delimiter))
        return 0 ;

    return msg->severity <= ma_log_get_severity_from_label(delimiter);  
}
