#include "ma/logger/ma_mailslot_logger.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal/utils/threading/ma_thread.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/logger/ma_logger_internal.h"


#include "ma_logger_appender.h"


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <Windows.h>

#define MA_AVIVIEW_LINE_SIZE 400

typedef struct ma_mail_slot_appender_s {
    ma_logger_appender_t        appender_impl ; //Appender implementer
    HANDLE                      slot;
    char                        *log_file_name;
    ma_mail_slot_logger_policy_t policy;
}ma_mail_slot_appender_t ;

struct ma_mail_slot_logger_s {
    ma_logger_t                 logger_impl ; //Logger base class
    ma_mail_slot_appender_t      appender;
};

/* logger methods decleration(s) */
static ma_error_t on_log_message(ma_logger_t *logger, ma_log_msg_t *msg);
static void on_release(ma_logger_t *logger);

/* logger appender methods decleration(s) */
static ma_error_t mail_slot_appender_append(ma_logger_appender_t *mail_slot , const char *msg);
static ma_error_t mail_slot_appender_set_policy(ma_logger_appender_t *mail_slot , void  *policy);
static ma_error_t mail_slot_appender_get_policy(ma_logger_appender_t *mail_slot ,const void **policy);
static void mail_slot_appender_release(ma_logger_appender_t *mail_slot);
static ma_error_t mail_slot_appender_init(const char *log_file_name, ma_mail_slot_appender_t *appender);

static const ma_logger_methods_t methods = {
    &on_log_message,
    &on_release
};

ma_error_t ma_mail_slot_logger_create(const char *slot, ma_mail_slot_logger_t **logger) {
    if(slot && logger) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;

        if((*logger = (ma_mail_slot_logger_t*)calloc(1, sizeof(ma_mail_slot_logger_t)))) {
            (void)mail_slot_appender_init(slot, &(*logger)->appender);
            (void)ma_logger_init((ma_logger_t *)*logger , &methods , 1 , *logger); err = MA_OK;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_mail_slot_logger_release(ma_mail_slot_logger_t *logger) {
    if(logger) {
        ma_logger_release((ma_logger_t*)logger);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_mail_slot_logger_set_policy(ma_mail_slot_logger_t *mail_slot , ma_mail_slot_logger_policy_t *policy) {
    if(mail_slot && policy) {
        return ma_logger_appender_set_policy((ma_logger_appender_t *)&mail_slot->appender , policy);
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_mail_slot_logger_get_policy(ma_mail_slot_logger_t *mail_slot , const ma_mail_slot_logger_policy_t **policy) {
    if(mail_slot && policy) {
        return ma_logger_appender_get_policy((ma_logger_appender_t *)&mail_slot->appender , (const void **)policy);
    }
    return MA_ERROR_INVALIDARG;
}


/* logger methods implementation */
ma_error_t on_log_message(ma_logger_t *logger, ma_log_msg_t *msg) {
    if(logger &&  msg) {
        ma_mail_slot_logger_t *mail_slot = (ma_mail_slot_logger_t *)logger ;
        char *format = mail_slot->appender.policy.format_pattern ? mail_slot->appender.policy.format_pattern :DEFAULT_MAILSLOT_LOG_FORMAT_PATTERN;
        int r = 0;

        ma_temp_buffer_t b = {0};
        ma_temp_buffer_init(&b);
        if ((r = ma_log_utils_format_msg((char *)ma_temp_buffer_get(&b), ma_temp_buffer_capacity(&b),format , msg)) < 0) {
            int chars_needed = ma_log_utils_format_msg(NULL, 0, format, msg);
            ma_temp_buffer_reserve(&b,1+chars_needed);
            r = ma_log_utils_format_msg((char *)ma_temp_buffer_get(&b), ma_temp_buffer_capacity(&b), format, msg);
        }
        if (-1 <= r) {
            ma_logger_appender_append((ma_logger_appender_t *)&mail_slot->appender , (const char *)ma_temp_buffer_get(&b));
        }
        ma_temp_buffer_uninit(&b);

        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

void on_release(ma_logger_t *logger) {
    if(logger) {
        ma_mail_slot_logger_t *mail_slot = (ma_mail_slot_logger_t *)logger;
        ma_logger_appender_release((ma_logger_appender_t *)&mail_slot->appender);
        free(mail_slot);
    }
    return;
}


static const ma_logger_appender_methods_t appender_methods = {
    &mail_slot_appender_append,
    &mail_slot_appender_set_policy,
    &mail_slot_appender_get_policy,
    &mail_slot_appender_release
};
/* appender method definations */
ma_error_t mail_slot_appender_init(const char *log_file_name, ma_mail_slot_appender_t *appender) {
    if(log_file_name && appender) {
        HANDLE mail_slot = 0;

        appender->log_file_name = strdup(log_file_name);
        ma_logger_appender_init((ma_logger_appender_t *)appender , &appender_methods , appender);

        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t mail_slot_appender_append(ma_logger_appender_t *appender , const char *msg ) {
    if(appender && msg) {
        ma_mail_slot_appender_t *mail_slot = (ma_mail_slot_appender_t *)appender;
        ma_error_t err = MA_ERROR_UNEXPECTED;

        if(INVALID_HANDLE_VALUE != (mail_slot->slot = CreateFileA(mail_slot->log_file_name, GENERIC_WRITE, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL))) {
            static struct
            {
                UINT    ui_index;   /* A serial number, but AVIView actually ignores it. */
                UINT    ui_type;    /* 1 = ANSI. 2 would be Unicode */
                UINT    ui_version; /* 1 = version 1 */
                DWORD   dw_length;  /* Length of message line */
            } header = {0, 1, 1, 0};
            DWORD bytes_written = 0;
            DWORD len = 0;
            DWORD line_size = MA_AVIVIEW_LINE_SIZE;
            DWORD opset = 0;

            /* Update binary header structure. */
            ++header.ui_index;
            len = (DWORD) strlen (msg) + 1;
            header.dw_length = line_size;
            
            /*Write line by line on to AVIVIEW it, has 400 characters per line*/
            while(len > 0){
                err = MA_ERROR_UNEXPECTED;
                if(len < line_size){
                    line_size = len;
                    header.dw_length = line_size;
                }
                /* Write them. Mailslots are message-oriented, not streams of bytes, therefore
                there has to be two separate writes, as that is how AVIView reads them. */
                if(WriteFile (mail_slot->slot, (const void *)&header, sizeof header, &bytes_written, NULL)) {
                    if(WriteFile (mail_slot->slot, (msg+opset),line_size, &bytes_written, NULL)) {
                        len = len - line_size;
                        opset += line_size; err = MA_OK;
                    } else break;
                } else break;
            }
            CloseHandle (mail_slot->slot);
        }
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t mail_slot_appender_set_policy(ma_logger_appender_t *appender  , void *policy_appender)  {
    if(appender && policy_appender) {
        ma_mail_slot_appender_t *mail_slot = (ma_mail_slot_appender_t *)appender;
        ma_mail_slot_logger_policy_t *policy = (ma_mail_slot_logger_policy_t *)policy_appender;

        if(policy->format_pattern) {
            if(mail_slot->policy.format_pattern) free(mail_slot->policy.format_pattern);
            mail_slot->policy.format_pattern = strdup(policy->format_pattern);
        }

        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t mail_slot_appender_get_policy(ma_logger_appender_t *appender ,const void  **policy) {
    if(appender && policy) {
        ma_mail_slot_appender_t *mail_slot = (ma_mail_slot_appender_t *)appender;

        *policy = (ma_mail_slot_logger_policy_t * )&mail_slot->policy;

        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG;

}

void mail_slot_appender_release(ma_logger_appender_t *appender) {
    if(appender) {
        ma_mail_slot_appender_t *mail_slot = (ma_mail_slot_appender_t *)appender;

        if(mail_slot->slot) CloseHandle(mail_slot->slot);
        if(mail_slot->log_file_name) free(mail_slot->log_file_name);
        if(mail_slot->policy.format_pattern) free(mail_slot->policy.format_pattern);
    }
}




