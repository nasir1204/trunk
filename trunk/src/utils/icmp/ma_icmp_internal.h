#ifndef MA_ICMP_INTERNAL_H_INCLUDED
#define MA_ICMP_INTERNAL_H_INCLUDED

#if !defined(MA_WINDOWS)
	#include <unistd.h>
	#include <netinet/in.h>
	#include <netinet/icmp6.h>
	#include <sys/types.h>
	#include <sys/socket.h>
	#define closesocket close
#endif
#include "ma/ma_common.h"
#include "ma/internal/utils/datastructures/ma_map.h"
#include "ma/internal/utils/icmp/ma_icmp.h"

MA_CPP(extern "C" {)
#define MA_DEFAULT_ICMP_TTL_VALUE			60
#define MA_DEFAULT_MAX_PING_TIME			60
#define MA_DEFAULT_PACKET_SIZE				32
#define MA_PSEUDO_HEADER_SIZE				40
#define MA_MAX_PING_DATA_SIZE				1024
#define MA_ICMP_ECHO_REPLY					0
#define MA_ICMP_ECHO_REPLY_V6				129
#define MA_ICMP_TTL_EXPIRE					11
#define MA_ICMP_TTL_EXPIRE_V6				3
#define MA_ICMP_ECHO_REQUEST				8
#define MA_ICMP_ECHO_REQUEST_V6				128
#define MA_ICMP_DEST_UNREACH				3
#define MA_ICMP_DEST_UNREACH_V6				1
#define MA_IPV6_IP_HEADER_SIZE              40

typedef struct ma_icmp_handle_s ma_icmp_handle_t;
typedef struct ma_icmp_url_list_s ma_icmp_url_list_t;
MA_MAP_DECLARE(ma_icmp_map_t, ma_uint16_t, ma_icmp_handle_t *);
MA_MAP_DECLARE(ma_addrinfo_map_t, uv_getaddrinfo_t*, uv_getaddrinfo_t*);
MA_MAP_DECLARE(ma_url_map_t, char*, ma_icmp_url_list_t*);

ma_error_t ma_icmp_url_list_create(ma_icmp_url_list_t **icmp_url_list);
ma_error_t ma_icmp_url_list_init(ma_icmp_url_list_t *icmp_url_list, char *url);
ma_error_t ma_icmp_url_list_release(ma_icmp_url_list_t *icmp_url_list);

struct ma_icmp_context_s {
	int							ipv4_socket_raw;
	int							ipv6_socket_raw;
	int							url_remaining;
	uv_poll_t					*ipv4_uv_poll_watcher;
	uv_poll_t					*ipv6_uv_poll_watcher;
	ma_icmp_policy_t			icmp_policy;
	ma_logger_t					*logger;
	uv_loop_t					*uv_loop ;
	ma_icmp_methods_t			*methods;
	MA_MAP_T(ma_icmp_map_t)		*icmp_pkts_map;		/*echo packet id to handle */
	MA_MAP_T(ma_addrinfo_map_t)	*icmp_addrinfo_map; /*uv getaddrinfo pointer map, which received in callback*/
	MA_MAP_T(ma_url_map_t)		*icmp_url_map;		/*duplicate repo node mapping */
	void						*data;
};

typedef struct ma_icmp_hdr_s {
    unsigned char			type;
    unsigned char			code;
    ma_uint16_t				checksum;
    ma_uint16_t				id;
    ma_uint16_t				seq;
	ma_uint32_t				time_stamp;
} ma_icmp_hdr_t;

typedef struct ma_icmp_subnet_s {
    ma_uint16_t				subnet_distance;
	ma_uint16_t				subnet_distance_last_succes;
	ma_uint16_t				subnet_distance_ceil;
	ma_uint16_t				subnet_distance_floor;
}ma_icmp_subnet_t;

struct ma_icmp_handle_s {
	char					*name ;
	char					*url;
	char					*ma_icmp_request_hdr;
	int						socket;
	ma_bool_t				is_numeric_url;
	ma_uint16_t				packet_id;
	ma_uint16_t				packet_seq;
	ma_uint16_t				ping_time_avg;
	uv_timer_t				*uv_timer;
	ma_icmp_hdr_t			*ma_icmp_response_hdr;
	ma_icmp_subnet_t		subnet;
	struct addrinfo			*dest;
	ma_icmp_context_t		*icmp_context;
	ma_bool_t				sendto_failed;
} ;

struct ma_icmp_url_list_s {
	char								*url;
	ma_queue_t							qlink;
};

MA_CPP(})
#endif /*MA_ICMP_INTERNAL_H_INCLUDED*/

