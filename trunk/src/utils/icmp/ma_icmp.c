#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ma/internal/ma_macros.h>
#include "ma/internal/utils/network/ma_net_interface.h"
#include "ma/internal/utils/platform/ma_net_interface_list_internal.h"
#include "ma/internal/utils/datastructures/ma_slist.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/utils/platform/ma_sys_apis.h"
#include "ma/internal/utils/icmp/ma_icmp.h"
#include "ma_icmp_internal.h"

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "icmp"

#ifndef MA_WINDOWS
	#include <sys/time.h>
#endif
typedef struct poll_watcher_data_s{
	int socket;
	ma_icmp_context_t *icmp_context;
}poll_watcher_data_t;

static int int_compartor(ma_uint16_t a, ma_uint16_t b) {
	return (a < b) ? -1 : ( a == b ? 0 : 1 );
}
MA_MAP_DEFINE(ma_icmp_map_t, ma_uint16_t, , , ma_icmp_handle_t *, , ,int_compartor);

static int ptr_compartor(uv_getaddrinfo_t *a, uv_getaddrinfo_t *b) {
	return (a < b) ? -1 : ( a == b ? 0 : 1 );
}
MA_MAP_DEFINE(ma_addrinfo_map_t, uv_getaddrinfo_t*, , , uv_getaddrinfo_t *, , ,ptr_compartor);

static int str_compartor(char *a, char *b) {
	return strcmp(a, b);
}
MA_MAP_DEFINE(ma_url_map_t, char*, , , ma_icmp_url_list_t *, , ,str_compartor);

#define icmp_pkt_size (MA_DEFAULT_PACKET_SIZE + sizeof(ma_icmp_hdr_t))

static ma_error_t ma_sitelist_dns_resolution(ma_icmp_handle_t *icmp_hdl);
static ma_error_t ma_icmp_context_release(ma_icmp_context_t **icmp_context);
static ma_error_t ma_icmp_data_reinit(ma_icmp_handle_t *icmp_hdl);

static ma_uint32_t ma_calculate_system_time(){
	ma_uint32_t sys_time ;
#if defined(MA_WINDOWS)
	sys_time = GetTickCount() ;	
#else
	struct timeval  tv;
	gettimeofday(&tv, NULL);
	sys_time = (tv.tv_sec) * 1000 + (tv.tv_usec) / 1000;
#endif
	return sys_time ;
}

static ma_uint16_t checksum_compute(ma_uint16_t* checksum_buffer, int size){
    unsigned long checksum = 0;
    while(size > 1) {
        checksum += *checksum_buffer++;
        size -= sizeof(ma_uint16_t);
    }  
	if (size) {
        checksum += *(unsigned char*)checksum_buffer;
    }
    checksum = (checksum >> 16) + (checksum & 0xffff);
    checksum += (checksum >> 16);
    return (ma_uint16_t)(~checksum);
}

static void uv_watcher_close_cb(uv_handle_t* handle){
	if(handle) {
		free(handle);
		handle = NULL;
	}
	return;
}

static void timer_close_cb(uv_handle_t *handle) {
	if(handle){
		free(handle);
		handle = NULL;
	}
}

static ma_error_t ma_icmp_timer_deinit(ma_icmp_handle_t *icmp_hdl){
	if(icmp_hdl && icmp_hdl->uv_timer){
		uv_timer_stop(icmp_hdl->uv_timer);
		uv_close((uv_handle_t *)icmp_hdl->uv_timer, &timer_close_cb);
		icmp_hdl->uv_timer = NULL;
	}
	return MA_OK;
}

static void ma_icmp_hdl_release(ma_icmp_handle_t *icmp_hdl){
	if(icmp_hdl){
		if(icmp_hdl->dest){
			uv_freeaddrinfo(icmp_hdl->dest);
		}
		if(icmp_hdl->url) free(icmp_hdl->url);
		if(icmp_hdl->ma_icmp_response_hdr) free(icmp_hdl->ma_icmp_response_hdr);
		if(icmp_hdl->name) free(icmp_hdl->name);
		if(icmp_hdl->ma_icmp_request_hdr) free(icmp_hdl->ma_icmp_request_hdr);
		ma_icmp_timer_deinit(icmp_hdl);
		free(icmp_hdl);
		icmp_hdl = NULL;
	}
}

static void ma_icmp_update_result(ma_icmp_handle_t *icmp_hdl){
	if(icmp_hdl && icmp_hdl->icmp_context && icmp_hdl->icmp_context->methods) {
		ma_icmp_callback_t callback_data ={0};
		ma_icmp_context_t *icmp_context = icmp_hdl->icmp_context;
		ma_icmp_url_list_t *url_list = NULL;
		MA_MAP_GET_VALUE(ma_url_map_t, icmp_context->icmp_url_map, icmp_hdl->url, url_list);
		if(url_list){
			while (!ma_queue_empty(&url_list->qlink)) {
				ma_queue_t *item = ma_queue_head(&url_list->qlink);
				ma_icmp_url_list_t *entry = ma_queue_data(item, ma_icmp_url_list_t, qlink);
				if(entry && entry->url){
					callback_data.icmp_context = icmp_context;
					callback_data.name = entry->url;
					callback_data.ping_time = icmp_hdl->ping_time_avg;
					callback_data.subnet_distance = icmp_hdl->subnet.subnet_distance;
					--icmp_context->url_remaining;
					if(!icmp_context->url_remaining)
						callback_data.computation_finishied = MA_TRUE;
					callback_data.icmp_context->methods->update(&callback_data);
					MA_LOG(icmp_hdl->icmp_context->logger, MA_LOG_SEV_TRACE, "updating repository <%s> to db, repository remain to be process <%d> ", entry->url, icmp_hdl->icmp_context->url_remaining);
				}
				ma_queue_remove(item);            
				ma_icmp_url_list_release(entry);
			}
			MA_MAP_REMOVE_ENTRY(ma_url_map_t, icmp_context->icmp_url_map, icmp_hdl->url);
			ma_icmp_url_list_release(url_list);
		}
		MA_MAP_REMOVE_ENTRY(ma_icmp_map_t, icmp_context->icmp_pkts_map, icmp_hdl->packet_id);
		ma_icmp_hdl_release(icmp_hdl);
	
		if(!icmp_context->url_remaining){
			MA_LOG(icmp_context->logger, MA_LOG_SEV_DEBUG, "all repositories processed, stopping operation.");
			icmp_context->methods->stop(icmp_context->data);
		}
	}
}

static void ma_icmp_socket_timeout_cb(uv_timer_t *handle, int status){
	if( (UV_OK == status) && handle && handle->data){
		ma_icmp_handle_t *icmp_hdl = (ma_icmp_handle_t*) handle->data;
		if(icmp_hdl->icmp_context) {
			ma_icmp_handle_t *icmp_hdl_from_map = NULL;
			MA_MAP_GET_VALUE(ma_icmp_map_t, icmp_hdl->icmp_context->icmp_pkts_map, icmp_hdl->packet_id, icmp_hdl_from_map);
			if(!icmp_hdl_from_map){
				/* does not belong to us. Ignoring */
				return;
			}

			if(icmp_hdl_from_map->sendto_failed){
				icmp_hdl_from_map->sendto_failed = 0;
				if(MA_OK != ma_icmp_data_reinit(icmp_hdl_from_map))
				{
					MA_LOG(icmp_hdl->icmp_context->logger, MA_LOG_SEV_DEBUG, "re-init failed");
					ma_icmp_update_result(icmp_hdl_from_map);
				}
			}
			else{
				MA_LOG(icmp_hdl->icmp_context->logger, MA_LOG_SEV_TRACE, "timed out for repository <%s> , updating DS", icmp_hdl_from_map->name);
				icmp_hdl_from_map->subnet.subnet_distance = icmp_hdl_from_map->subnet.subnet_distance_last_succes;
				ma_icmp_update_result(icmp_hdl_from_map);
			}
		}
	}
}

static int ma_icmp_sendto(ma_icmp_handle_t *icmp_hdl){  
	return sendto( icmp_hdl->socket, (AF_INET6 == icmp_hdl->dest->ai_family)?icmp_hdl->ma_icmp_request_hdr + MA_PSEUDO_HEADER_SIZE:icmp_hdl->ma_icmp_request_hdr,
					icmp_pkt_size, 0, icmp_hdl->dest->ai_addr, (int) icmp_hdl->dest->ai_addrlen); 
}

static ma_error_t prepend_psudo_hdr_for_ipv6(ma_icmp_handle_t *icmp_hdl , int packet_size , char *pseudo_hdr){
	/*As per 4.8, no need to have psudo header*/
	return MA_OK;
#if 0
	struct addrinfo *dest = icmp_hdl->dest;
	struct addrinfo *res = dest;

	if(!dest || !pseudo_hdr ){
		MA_LOG(icmp_hdl->icmp_context->logger, MA_LOG_SEV_ERROR, "Invalid arguments");
		return MA_ERROR_INVALIDARG;
	}
	while(res)
	{
		uv_os_sock_t sock = socket(AF_INET6 , SOCK_DGRAM , IPPROTO_UDP);	
		if( (uv_os_sock_t) -1 != sock) {
			/*	Since this UDP connect() : doesn't do anything to the other end, it just conditions the local API to know
				who you are sending to and receiving from. So no worry of blocking */
			if( 0 == connect(sock , dest->ai_addr , dest->ai_addrlen)) {
				socklen_t slen;
				struct sockaddr_storage ss;
				slen = sizeof(ss);
				if( 0 == getsockname(sock, (struct sockaddr *)&ss, &slen))	{
					/* Fill source address in the pseudo header */
					memcpy( pseudo_hdr  , (void *)&( ((struct sockaddr_in6 *)&ss)->sin6_addr ) , 16);
					closesocket(sock);
					/* Destination address */
					memcpy( pseudo_hdr + 16 , (void *)&(((struct sockaddr_in6 *)dest->ai_addr)->sin6_addr ) , 16);
					/* Payload size */
					*(pseudo_hdr + 35) = packet_size;
					/* Next header */
					*(pseudo_hdr + 39) = IPPROTO_ICMPV6;
					return MA_OK;
				}
			}
			closesocket(sock);
		}
		res = res->ai_next;							
	}	

	MA_LOG(icmp_hdl->icmp_context->logger, MA_LOG_SEV_DEBUG, "get socket name failed for <%s>", icmp_hdl->url);
	return MA_ERROR_APIFAILED;
#endif
}

static ma_error_t ma_icmp_set_ttl(ma_icmp_handle_t *icmp_hdl, int family, ma_uint32_t value){
	int rc = setsockopt(icmp_hdl->socket, ((family == AF_INET6) ? IPPROTO_IPV6 : IPPROTO_IP), ((family == AF_INET6) ? IPV6_UNICAST_HOPS : IP_TTL), (const char*)&value, sizeof(value));		
	if(rc != 0) {
		MA_LOG(icmp_hdl->icmp_context->logger, MA_LOG_SEV_ERROR, "setsockopt with TTL failed with err <%d> ", rc);  
		return MA_ERROR_APIFAILED;
	}
	MA_LOG(icmp_hdl->icmp_context->logger, MA_LOG_SEV_TRACE, "setting ttl value<%d> for repository <%s>", value, icmp_hdl->name);
	return MA_OK;
}

static ma_error_t ma_icmp_data_reinit(ma_icmp_handle_t *icmp_hdl){
	if(icmp_hdl) {
		int				rc;
		ma_icmp_hdr_t	*icmp_req_hdr = NULL;
		int				family	= icmp_hdl->dest->ai_family ;
		icmp_hdl->packet_seq +=1;  
		MA_MAP_ADD_ENTRY(ma_icmp_map_t, icmp_hdl->icmp_context->icmp_pkts_map, icmp_hdl->packet_id, icmp_hdl);
		icmp_req_hdr = (AF_INET6 == family) ? (ma_icmp_hdr_t*)(icmp_hdl->ma_icmp_request_hdr + MA_PSEUDO_HEADER_SIZE) :(ma_icmp_hdr_t*)icmp_hdl->ma_icmp_request_hdr;
		icmp_req_hdr->seq = icmp_hdl->packet_seq;
		icmp_req_hdr->time_stamp = ma_calculate_system_time();
		icmp_req_hdr->checksum = 0;
		if(icmp_hdl->icmp_context->icmp_policy.rank_type == MA_REPOSITORY_RANK_BY_SUBNET_DISTANCE){
			if(MA_OK != ma_icmp_set_ttl(icmp_hdl, family, icmp_hdl->subnet.subnet_distance))
				return MA_ERROR_APIFAILED;
		}/*else{*/
			uv_timer_stop(icmp_hdl->uv_timer);
			uv_timer_start(icmp_hdl->uv_timer, ma_icmp_socket_timeout_cb , icmp_hdl->icmp_context->icmp_policy.max_ping_time *1000, 0);
		/*}*/

		icmp_req_hdr->checksum = checksum_compute((ma_uint16_t *)icmp_hdl->ma_icmp_request_hdr, 
													(AF_INET6 == icmp_hdl->dest->ai_family) ? (icmp_pkt_size + MA_PSEUDO_HEADER_SIZE) : icmp_pkt_size);
		rc =  ma_icmp_sendto(icmp_hdl);
		if(rc < 0)
		{
			MA_LOG(icmp_hdl->icmp_context->logger, MA_LOG_SEV_DEBUG, "sendto() failed again for repository <%s>, will not try again", icmp_hdl->name);
		}
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

static void ma_process_ping_responce(ma_icmp_handle_t *icmp_hdl){
	if( (icmp_hdl->ma_icmp_response_hdr->type == MA_ICMP_ECHO_REPLY) || (icmp_hdl->ma_icmp_response_hdr->type == MA_ICMP_ECHO_REPLY_V6) ){
		ma_uint32_t current_time = ma_calculate_system_time();
		ma_uint32_t time_diff = current_time - icmp_hdl->ma_icmp_response_hdr->time_stamp;
		/*Some ping response come within micro seconds(i.e. 0 millisecond), But zero ping time doesn't make sense, so adding as 1 millisecond */
		icmp_hdl->ping_time_avg = (time_diff ? time_diff : 1); 
		ma_icmp_update_result(icmp_hdl);
	}
}

static void ma_process_subnet_distance_responce(ma_icmp_handle_t *icmp_hdl){
	if(icmp_hdl) {
		if(icmp_hdl->ma_icmp_response_hdr->type == MA_ICMP_ECHO_REPLY || icmp_hdl->ma_icmp_response_hdr->type == MA_ICMP_ECHO_REPLY_V6){
			/*This is very first prob with (TTL = max_subnet_distance) & subnet_distance was initialize to max_subnet_distance + 1, so resetting it to correct one */
			if(icmp_hdl->subnet.subnet_distance == icmp_hdl->icmp_context->icmp_policy.max_subnet_distance + 1)
				icmp_hdl->subnet.subnet_distance = icmp_hdl->icmp_context->icmp_policy.max_subnet_distance;

			if(((icmp_hdl->subnet.subnet_distance_ceil - icmp_hdl->subnet.subnet_distance_floor) < 2)){
				MA_LOG(icmp_hdl->icmp_context->logger, MA_LOG_SEV_TRACE, "repository <%s> reached with <%d> hops", icmp_hdl->name, icmp_hdl->subnet.subnet_distance);
				ma_icmp_update_result(icmp_hdl); /* Got correct subnet distance */
				return;
			}
			else{
				icmp_hdl->subnet.subnet_distance_ceil = icmp_hdl->subnet.subnet_distance;
				icmp_hdl->subnet.subnet_distance_last_succes = icmp_hdl->subnet.subnet_distance;
				icmp_hdl->subnet.subnet_distance = (icmp_hdl->subnet.subnet_distance_floor + icmp_hdl->subnet.subnet_distance_ceil ) / 2;
				if(0 == icmp_hdl->subnet.subnet_distance) {
					/*No need to prob further, update with last success */
					icmp_hdl->subnet.subnet_distance = icmp_hdl->subnet.subnet_distance_last_succes;
					ma_icmp_update_result(icmp_hdl);
					return;
				}
				MA_LOG(icmp_hdl->icmp_context->logger, MA_LOG_SEV_TRACE, "repository <%s> reached with <%d> hops, now trying with smaller ttl", icmp_hdl->name, icmp_hdl->subnet.subnet_distance_last_succes);
				if(MA_OK != ma_icmp_data_reinit(icmp_hdl)) {
					MA_LOG(icmp_hdl->icmp_context->logger, MA_LOG_SEV_DEBUG, "re-init failed for repository <%s>, updating DS", icmp_hdl->name);
					ma_icmp_update_result(icmp_hdl);
					return;
				}
			}
		}
		else if(icmp_hdl->ma_icmp_response_hdr->type == MA_ICMP_TTL_EXPIRE || icmp_hdl->ma_icmp_response_hdr->type == MA_ICMP_TTL_EXPIRE_V6){
			if(icmp_hdl->subnet.subnet_distance >= icmp_hdl->icmp_context->icmp_policy.max_subnet_distance){
				MA_LOG(icmp_hdl->icmp_context->logger, MA_LOG_SEV_TRACE, "repository <%s> not reachable in maximum <%d> TTL", icmp_hdl->name, icmp_hdl->icmp_context->icmp_policy.max_subnet_distance);
				ma_icmp_update_result(icmp_hdl);
				return;
			} 
			if((icmp_hdl->subnet.subnet_distance + 1) >= icmp_hdl->subnet.subnet_distance_last_succes){
				MA_LOG(icmp_hdl->icmp_context->logger, MA_LOG_SEV_TRACE, "prob for repository <%s> expired with <%d> TTL , but earlier prob with TTL <%d> was successful, so updating DS", icmp_hdl->name, icmp_hdl->subnet.subnet_distance, icmp_hdl->subnet.subnet_distance_last_succes);
				icmp_hdl->subnet.subnet_distance = icmp_hdl->subnet.subnet_distance_last_succes;
				ma_icmp_update_result(icmp_hdl);
				return;
			}
			icmp_hdl->subnet.subnet_distance_floor = icmp_hdl->subnet.subnet_distance;
			icmp_hdl->subnet.subnet_distance = ((icmp_hdl->subnet.subnet_distance_floor + icmp_hdl->subnet.subnet_distance_ceil ) / 2) + 1;
			MA_LOG(icmp_hdl->icmp_context->logger, MA_LOG_SEV_TRACE, "repository <%s> not reached with <%d> hops, now trying with greter ttl", icmp_hdl->name, icmp_hdl->subnet.subnet_distance_floor);
			if(MA_OK != ma_icmp_data_reinit(icmp_hdl))
			{
				MA_LOG(icmp_hdl->icmp_context->logger, MA_LOG_SEV_TRACE, "re-init failed for repository <%s>, updating DS", icmp_hdl->name);
				ma_icmp_update_result(icmp_hdl);
				return;
			}
		}
		else{
			MA_LOG(icmp_hdl->icmp_context->logger, MA_LOG_SEV_TRACE, "repository <%s> not reachable", icmp_hdl->name);
			ma_icmp_update_result(icmp_hdl);
			return;
		}	
	}
}

static void ma_process_icmp_response(ma_icmp_context_t *icmp_context, ma_icmp_hdr_t *ma_icmp_response_hdr){
	if(icmp_context && ma_icmp_response_hdr){
		ma_icmp_handle_t *icmp_hdl_from_map = NULL;
		/*response is not necessary to come for same handle, because we are using same socket for all handle */
		MA_MAP_GET_VALUE(ma_icmp_map_t, icmp_context->icmp_pkts_map, ma_icmp_response_hdr->id, icmp_hdl_from_map);
		if(!icmp_hdl_from_map){
			/* does not belong to us or already processed. Ignoring */
			free(ma_icmp_response_hdr);
			return;
		}

		MA_LOG(icmp_context->logger, MA_LOG_SEV_TRACE, "received echo response callback for repository <%s>.", icmp_hdl_from_map->name);
		if(icmp_hdl_from_map->packet_seq == ma_icmp_response_hdr->seq){
			if(icmp_hdl_from_map->ma_icmp_response_hdr)
				free(icmp_hdl_from_map->ma_icmp_response_hdr);
			icmp_hdl_from_map->ma_icmp_response_hdr = ma_icmp_response_hdr;
			if(icmp_context->icmp_policy.rank_type == MA_REPOSITORY_RANK_BY_SUBNET_DISTANCE){
				ma_process_subnet_distance_responce(icmp_hdl_from_map);
			}
			else if(icmp_context->icmp_policy.rank_type == MA_REPOSITORY_RANK_BY_PING_TIME){
				ma_process_ping_responce(icmp_hdl_from_map);
			}
		}
		else{
			MA_LOG(icmp_context->logger, MA_LOG_SEV_TRACE, "but its old/redundant. ignoring it");
			free(ma_icmp_response_hdr);
			ma_icmp_response_hdr = NULL;
			return;
		}
	}
}

static void ma_icmp_uv_poll_perform_cb(uv_poll_t *req, int status, int events){
	if(req) {
		if(req->data) {
			if( (UV_OK == status) && (UV_READABLE == events) ){
				poll_watcher_data_t *poll_watcher_data = (poll_watcher_data_t*)req->data;
				ma_icmp_context_t *icmp_context = poll_watcher_data->icmp_context;
				if(icmp_context) {
					ma_icmp_hdr_t *icmp_response_header = (ma_icmp_hdr_t*)calloc(1, sizeof(ma_icmp_hdr_t));
					ma_icmp_hdr_t temp_icmp_request_header = {0};
					ma_uint16_t header_len;
					struct	sockaddr_storage from_addr = {0};
					char	header_rec_buffer[MA_MAX_PING_DATA_SIZE] = {0};
					int		responce_len = MA_MAX_PING_DATA_SIZE;
					int		size;
					char *icmp_hdr_original = NULL;
					unsigned short icmp_hdr_len = 0;
					size = recvfrom(poll_watcher_data->socket, header_rec_buffer, MA_MAX_PING_DATA_SIZE, 
											0, (struct sockaddr *)&from_addr, (socklen_t *) &responce_len);
					if(size > 0){
						header_len = (header_rec_buffer[0] & 0x0F) * 4;
						if(AF_INET6 == from_addr.ss_family){
							memcpy(icmp_response_header, header_rec_buffer, sizeof (ma_icmp_hdr_t));
						}
						else{
							memcpy(icmp_response_header, (header_rec_buffer + header_len), sizeof (ma_icmp_hdr_t));
						}

						/* expired or unreachable response doesn't contain message id. Extracting from request, which is appended to such response */
						if(icmp_response_header->type == MA_ICMP_TTL_EXPIRE || icmp_response_header->type == MA_ICMP_DEST_UNREACH ){
							icmp_hdr_original = header_rec_buffer + header_len + sizeof(icmp_response_header->type) + sizeof(icmp_response_header->code) + sizeof(icmp_response_header->checksum) +  4 ;
							icmp_hdr_len = (icmp_hdr_original[0] & 0x0F) * 4;
							memcpy(&temp_icmp_request_header ,(icmp_hdr_original + icmp_hdr_len),sizeof(ma_icmp_hdr_t));
							icmp_response_header->id = temp_icmp_request_header.id;
							icmp_response_header->seq = temp_icmp_request_header.seq;
						} 
						else if(icmp_response_header->type == MA_ICMP_DEST_UNREACH_V6 || icmp_response_header->type == MA_ICMP_DEST_UNREACH_V6 ){
							memcpy(&temp_icmp_request_header,(header_rec_buffer + MA_IPV6_IP_HEADER_SIZE + 4 + MA_IPV6_IP_HEADER_SIZE),sizeof(ma_icmp_hdr_t));
							icmp_response_header->id = temp_icmp_request_header.id;
							icmp_response_header->seq = temp_icmp_request_header.seq;
						}
						
						ma_process_icmp_response(icmp_context, icmp_response_header);
					}
					else
						free(icmp_response_header);
				}
			}
		}
		else {
			/* Got stale watcher, cleaning it */
			/*free(req);
			req = NULL;*/
		}
	}
}

static ma_error_t ma_socket_blocking_set(int fd, ma_bool_t blocking)
{
   if (fd < 0) 
	   return MA_ERROR_APIFAILED;

#if defined(MA_WINDOWS)
   {
   unsigned long mode = blocking ? 0 : 1;
   return (ioctlsocket(fd, FIONBIO, &mode) == 0) ? MA_OK : MA_ERROR_APIFAILED;
   }
#else
   {
   int flags = fcntl(fd, F_GETFL, 0);
   if (flags < 0) 
	   return MA_ERROR_APIFAILED;
   flags = blocking ? (flags&~O_NONBLOCK) : (flags|O_NONBLOCK);
   return (fcntl(fd, F_SETFL, flags) == 0) ? MA_OK : MA_ERROR_APIFAILED;
   }
#endif
}

static ma_error_t ma_icmp_set_socket_option(ma_icmp_handle_t *icmp_hdl, int family){
#if !defined(MA_WINDOWS)
	/* As large number of messages are received on an ICMPv6 socket on Non-Windows, so input filters are required to restrict incoming ICMPv6 messages.
	Therefore it blocks all incoming ICMP messages and allow only Echo reply, Destination unreachable and TTL expire
	*/
	struct icmp6_filter filter;
	if(family == AF_INET6 )	{		
		/* Blocks all incoming ICMP messages */
		ICMP6_FILTER_SETBLOCKALL(&filter);
		/* Allows only following ICMP messages. The messages are Echo reply, Destination unreachable and TTL expire */
		ICMP6_FILTER_SETPASS(MA_ICMP_DEST_UNREACH_V6, &filter);
		ICMP6_FILTER_SETPASS(MA_ICMP_TTL_EXPIRE_V6, &filter);
		ICMP6_FILTER_SETPASS(MA_ICMP_ECHO_REPLY_V6, &filter);

		/* ICMP6_FILTER is set as socket option with ICMPv6 filters set above */
		setsockopt(icmp_hdl->socket, IPPROTO_ICMPV6, ICMP6_FILTER, &filter, sizeof(struct icmp6_filter));     
	}
#endif
	return ma_socket_blocking_set(icmp_hdl->socket, MA_TRUE);
}

static ma_error_t ma_icmp_timer_init(ma_icmp_handle_t *icmp_hdl){
	uv_timer_t		*uv_timer = NULL;
	int				rc;

	uv_timer = (uv_timer_t*) calloc (1, sizeof(uv_timer_t));
	if(!uv_timer) {
		return MA_ERROR_OUTOFMEMORY;
	}
	icmp_hdl->uv_timer = uv_timer;
	uv_timer->data = icmp_hdl;
    MA_LOG(icmp_hdl->icmp_context->logger, MA_LOG_SEV_DEBUG, "uv timer data(%p)\n", icmp_hdl);
	if(!((UV_OK == (rc = uv_timer_init(icmp_hdl->icmp_context->uv_loop, uv_timer))) &&
		(UV_OK == (rc = uv_timer_start(uv_timer, ma_icmp_socket_timeout_cb , (icmp_hdl->icmp_context->icmp_policy.max_ping_time * 1000), 0))))){
			MA_LOG(icmp_hdl->icmp_context->logger, MA_LOG_SEV_ERROR, "timer initialization failed with uv err <%d>.", rc);  
			free(icmp_hdl->uv_timer);
			icmp_hdl->uv_timer = NULL;
			return MA_ERROR_APIFAILED;
	}

	return MA_OK;
}

static ma_error_t ma_icmp_data_init(ma_icmp_handle_t *icmp_hdl){
	/*	Here we are making 128 as fixed. Please keep in mind that it can be at the max icmp_hdr_size + MA_PSEUDO_HEADER_SIZE + 1 = 85
		and this function is internal and we are passing the fixed packet size=44	*/
	char			*hdr_buffer = NULL;
	ma_icmp_hdr_t	*icmp_header_hdl = NULL;
	char			*datapart;
	int				family	= icmp_hdl->dest->ai_family ;
	int				rc;
	ma_uint16_t		packet_id = (ma_uint16_t)ma_sys_rand();

	if(icmp_hdl->icmp_context->icmp_policy.rank_type == MA_REPOSITORY_RANK_BY_SUBNET_DISTANCE){
		if(MA_OK != ma_icmp_set_ttl(icmp_hdl, family, icmp_hdl->icmp_context->icmp_policy.max_subnet_distance))
			return MA_ERROR_APIFAILED;
	}

	hdr_buffer = (char*)calloc(1, 128);	
	icmp_header_hdl					= (AF_INET6 == family) ? (ma_icmp_hdr_t*)(hdr_buffer + MA_PSEUDO_HEADER_SIZE) :(ma_icmp_hdr_t*)hdr_buffer;	
	icmp_header_hdl->type			= AF_INET6 == family ? MA_ICMP_ECHO_REQUEST_V6:MA_ICMP_ECHO_REQUEST;
    icmp_header_hdl->code			= 0;
    icmp_header_hdl->checksum		= 0;   
	icmp_header_hdl->time_stamp		= ma_calculate_system_time();
	icmp_header_hdl->id	= icmp_hdl->packet_id = packet_id;  
	icmp_hdl->packet_seq = 1;
	icmp_header_hdl->seq = 1; 
	datapart = (char *)icmp_header_hdl +  sizeof(ma_icmp_hdr_t);
	memset(datapart,'E', 32);
	if(MA_OK != ma_icmp_timer_init(icmp_hdl)){
		return MA_ERROR_APIFAILED;
	}
	if(AF_INET6 == family){
		if(MA_OK != prepend_psudo_hdr_for_ipv6(icmp_hdl, icmp_pkt_size, hdr_buffer))	{
            ma_icmp_timer_deinit(icmp_hdl);
			free(hdr_buffer);
			MA_LOG(icmp_hdl->icmp_context->logger, MA_LOG_SEV_DEBUG, "ipv6 echo header initialization failed for <%s>.", icmp_hdl->name);
			return MA_ERROR_APIFAILED;;
		}
	}
	
	icmp_header_hdl->checksum = checksum_compute((ma_uint16_t *)hdr_buffer, 
												(AF_INET6 == family) ? (icmp_pkt_size + MA_PSEUDO_HEADER_SIZE) : icmp_pkt_size); 
	icmp_hdl->ma_icmp_request_hdr = hdr_buffer;
	rc =  ma_icmp_sendto(icmp_hdl);
	if(rc < 0){
		/*there are multiple reason for failure, Instead of checking exact reason. By default will try for 1 more times with some time interval (socket timer time out) */
		MA_LOG(icmp_hdl->icmp_context->logger, MA_LOG_SEV_DEBUG, "echo pkg send to repository <%s> failed, lets try one more time.", icmp_hdl->name);
		icmp_hdl->sendto_failed = 1;
	}else
		MA_LOG(icmp_hdl->icmp_context->logger, MA_LOG_SEV_TRACE, "echo pkg sent to repository <%s>.", icmp_hdl->name);

	MA_MAP_ADD_ENTRY(ma_icmp_map_t, icmp_hdl->icmp_context->icmp_pkts_map, icmp_hdl->packet_id, icmp_hdl);
	return MA_OK;
}

static int initiate_uv_construct(uv_poll_t *watcher, poll_watcher_data_t *poll_watcher_data){
    int rc = 1;
    if(watcher && poll_watcher_data) {
	    watcher->data = poll_watcher_data;
	    if(0 == (rc = uv_poll_init_socket(poll_watcher_data->icmp_context->uv_loop, watcher, poll_watcher_data->socket)))
		    rc = uv_poll_start(watcher, UV_READABLE, ma_icmp_uv_poll_perform_cb);
    }
	return rc;
}

static void ma_icmp_uv_getaddrinfo_cb (uv_getaddrinfo_t* req, int status, struct addrinfo* dest){
	if(req){
		if(req->data){
			uv_err_t uv_err = uv_last_error(req->loop) ;
			ma_icmp_handle_t *icmp_hdl = (ma_icmp_handle_t*)req->data;
			req->data = NULL;
			if(icmp_hdl->icmp_context) {
				MA_LOG(icmp_hdl->icmp_context->logger, MA_LOG_SEV_TRACE, "received dns resolution callback for repository <%s>.", icmp_hdl->name);
				MA_MAP_REMOVE_ENTRY(ma_addrinfo_map_t, icmp_hdl->icmp_context->icmp_addrinfo_map, req);
				if((UV_OK == status) && dest) {
					if(dest->ai_family == AF_INET6){
						if(icmp_hdl->icmp_context->ipv6_socket_raw == 0){
							poll_watcher_data_t *poll_watcher_data = NULL;
							if(-1== (icmp_hdl->icmp_context->ipv6_socket_raw = (int) socket(AF_INET6, SOCK_RAW, IPPROTO_ICMPV6))){
								MA_LOG(icmp_hdl->icmp_context->logger, MA_LOG_SEV_ERROR, "socket creation failed.");
								ma_icmp_update_result(icmp_hdl);
								return;
							}
							icmp_hdl->socket = icmp_hdl->icmp_context->ipv6_socket_raw;
							if(MA_OK != ma_icmp_set_socket_option(icmp_hdl, AF_INET6)){
								MA_LOG(icmp_hdl->icmp_context->logger, MA_LOG_SEV_ERROR, "set socket option failed.");
								ma_icmp_update_result(icmp_hdl);
								return;
							}
							icmp_hdl->icmp_context->ipv6_uv_poll_watcher = (uv_poll_t*) calloc(1, sizeof(uv_poll_t));
							poll_watcher_data = (poll_watcher_data_t*) calloc(1, sizeof(poll_watcher_data_t));
							poll_watcher_data->socket = icmp_hdl->icmp_context->ipv6_socket_raw;
							poll_watcher_data->icmp_context = icmp_hdl->icmp_context;
							if(initiate_uv_construct(icmp_hdl->icmp_context->ipv6_uv_poll_watcher, poll_watcher_data)){
								MA_LOG(icmp_hdl->icmp_context->logger, MA_LOG_SEV_ERROR, "uv initialization failed.");
								ma_icmp_update_result(icmp_hdl);
								return;
							}
							MA_LOG(icmp_hdl->icmp_context->logger, MA_LOG_SEV_TRACE, "ipv6 socket <%d> created.", icmp_hdl->socket);
						}
						else
							icmp_hdl->socket = icmp_hdl->icmp_context->ipv6_socket_raw;
					}
					else if(dest->ai_family == AF_INET){ 
						if(icmp_hdl->icmp_context->ipv4_socket_raw == 0){
							poll_watcher_data_t *poll_watcher_data = NULL;
							if(-1 == (icmp_hdl->icmp_context->ipv4_socket_raw = (int) socket(AF_INET, SOCK_RAW, IPPROTO_ICMP))){
								MA_LOG(icmp_hdl->icmp_context->logger, MA_LOG_SEV_ERROR, "socket creation failed.");
								ma_icmp_update_result(icmp_hdl);
								return;
							}
					
							icmp_hdl->socket = icmp_hdl->icmp_context->ipv4_socket_raw;
							if(MA_OK != ma_icmp_set_socket_option(icmp_hdl, AF_INET)){
								MA_LOG(icmp_hdl->icmp_context->logger, MA_LOG_SEV_ERROR, "set socket option failed.");
								ma_icmp_update_result(icmp_hdl);
								return;
							}
							icmp_hdl->icmp_context->ipv4_uv_poll_watcher = (uv_poll_t*) calloc(1, sizeof(uv_poll_t));
							poll_watcher_data = (poll_watcher_data_t*) calloc(1, sizeof(poll_watcher_data_t));
							poll_watcher_data->socket = icmp_hdl->icmp_context->ipv4_socket_raw;
							poll_watcher_data->icmp_context = icmp_hdl->icmp_context;
							if(initiate_uv_construct(icmp_hdl->icmp_context->ipv4_uv_poll_watcher, poll_watcher_data)){
								MA_LOG(icmp_hdl->icmp_context->logger, MA_LOG_SEV_ERROR, "uv initialization failed.");
								ma_icmp_update_result(icmp_hdl);
								return;
							}
							MA_LOG(icmp_hdl->icmp_context->logger, MA_LOG_SEV_TRACE, "ipv4 socket <%d> created.", icmp_hdl->socket);
						}
						else
							icmp_hdl->socket = icmp_hdl->icmp_context->ipv4_socket_raw;
					}			

					icmp_hdl->dest = dest;
					if(MA_OK != ma_icmp_data_init(icmp_hdl)){
						ma_icmp_update_result(icmp_hdl);
					}
				}
				else { /* No domain exist case */
					MA_LOG(icmp_hdl->icmp_context->logger, MA_LOG_SEV_TRACE, "dns resolution failed for repository <name: %s> with uv err <%s>", icmp_hdl->name, uv_err_name(uv_err));
					ma_icmp_update_result(icmp_hdl);
				}
			}
		}
		free(req);
		req = NULL;
	}
}

static ma_bool_t is_url_ipv6(char *url){
	if(strstr(url, "[") && strstr(url, "]"))
		return MA_TRUE;
	else
		return MA_FALSE;
}

static void remove_brackets(char *url){
	int i;
	for(i = 0; url[i] != '\0'; i++){
		url[i] = url[i + 1];
	}
	i = i - 2;
	if(i >= 0)
		url[i] = '\0';
}

static void format_ipv6(char *url){
	if(is_url_ipv6(url)){
		remove_brackets(url);
	}
}

static ma_bool_t is_ipv6_addr(const char* address) {
	if(address) {
		const char *pos = address ;
		unsigned char ch = *pos ;
		while(ch != '\0'){
			if(ch == ':')
				return MA_TRUE;
			ch = *++pos;
		}
	}
	return MA_FALSE;
}

static ma_bool_t is_ipv4_ip(const char* address) {
	if(address) {
		const char *pos = address ;
		unsigned char ch = *pos ;
		unsigned short count = 0 ;

		while(ch != '\0') {
			if(ch != ':')
			if (!((ch >= '0' && ch <= '9') || ch == '.')) return MA_FALSE ;

			if (ch == '.')
				if (++count > 3) return MA_FALSE ;

			ch = *++pos;
		}

		if(count == 3 && *--pos != '.') 
			return MA_TRUE ;	
	}
	return MA_FALSE ;
}
static ma_bool_t is_numeric_address(const char* address) {	
    if(address) {
		if(is_ipv6_addr(address)){
			/*ipv6 address, so its numerical address*/
			return MA_TRUE;
		}
		return is_ipv4_ip(address);
	}
	return MA_FALSE ;
}

static ma_error_t ma_sitelist_dns_resolution(ma_icmp_handle_t *icmp_hdl) {
	if(icmp_hdl && icmp_hdl->url && icmp_hdl->name) {
		uv_getaddrinfo_t *getaddrinfo = (uv_getaddrinfo_t*) calloc(1, sizeof(uv_getaddrinfo_t));
		struct addrinfo hints;
		is_ipv6_addr(icmp_hdl->url)?(hints.ai_family=PF_INET6):(hints.ai_family=PF_INET);
		hints.ai_socktype	= SOCK_STREAM;
		hints.ai_protocol   = IPPROTO_TCP;
		hints.ai_flags = (icmp_hdl->is_numeric_url) ? AI_NUMERICHOST : 0 ;
		getaddrinfo->data = icmp_hdl;
		if (UV_OK != uv_getaddrinfo(icmp_hdl->icmp_context->uv_loop, getaddrinfo, ma_icmp_uv_getaddrinfo_cb, (char*)icmp_hdl->url, 0, &hints)) {	
			MA_LOG(icmp_hdl->icmp_context->logger, MA_LOG_SEV_INFO, "uv_getaddrinfo failed for repository <%s> ", icmp_hdl->name);
			free(getaddrinfo);
			return MA_ERROR_INVALIDARG;
		}
		MA_LOG(icmp_hdl->icmp_context->logger, MA_LOG_SEV_TRACE, "repository <%s>/url <%s> sent for dns resolution.", icmp_hdl->name, icmp_hdl->url);
		MA_MAP_ADD_ENTRY(ma_addrinfo_map_t, icmp_hdl->icmp_context->icmp_addrinfo_map, getaddrinfo, getaddrinfo);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG ;
}

static ma_error_t validiate_icmp_context(ma_icmp_context_t *icmp_context){
	if(icmp_context && icmp_context->logger && icmp_context->methods){ 
		if( (!icmp_context->icmp_policy.max_ping_time) || (icmp_context->icmp_policy.max_ping_time > MA_DEFAULT_MAX_PING_TIME) ){
			/*timeout is not set... setting to default */
			icmp_context->icmp_policy.max_ping_time = MA_DEFAULT_MAX_PING_TIME;
		}

		if( (!icmp_context->icmp_policy.max_subnet_distance) || (icmp_context->icmp_policy.max_subnet_distance > MA_DEFAULT_ICMP_TTL_VALUE) ){
			/*ttl is not set... setting to default */
			icmp_context->icmp_policy.max_subnet_distance = MA_DEFAULT_ICMP_TTL_VALUE;
		}

		if( !((icmp_context->icmp_policy.rank_type == MA_REPOSITORY_RANK_BY_PING_TIME) || (icmp_context->icmp_policy.rank_type == MA_REPOSITORY_RANK_BY_SUBNET_DISTANCE)) ) {
			/* Policy not set, setting default as ping count */
			icmp_context->icmp_policy.rank_type = MA_REPOSITORY_RANK_BY_PING_TIME;
		}
		return  MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_icmp_computation_start(ma_icmp_context_t *icmp_context){
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	if(icmp_context){
		MA_LOG(icmp_context->logger, MA_LOG_SEV_TRACE, "repository icmp computation started.");
		if(MA_OK == (rc = validiate_icmp_context(icmp_context))){
			ma_icmp_handle_t *icmp_hdl=NULL;
			ma_icmp_repo_node_list_t *list = NULL;
			MA_MAP_CREATE(ma_icmp_map_t, icmp_context->icmp_pkts_map);
			MA_MAP_CREATE(ma_addrinfo_map_t, icmp_context->icmp_addrinfo_map);
			MA_MAP_CREATE(ma_url_map_t, icmp_context->icmp_url_map);
			if(MA_OK == (rc = ma_icmp_repo_node_list_create(&list))){
				if(MA_OK == (rc = icmp_context->methods->get(icmp_context, &list))){
					if(ma_queue_empty(&list->qlink)) {
						MA_LOG(icmp_context->logger, MA_LOG_SEV_TRACE, "no qualified repositories for icmp calculations.");
						rc = MA_ERROR_INVALIDARG; /*Add appropriate error code */
					}
					else {
						while (!ma_queue_empty(&list->qlink)) {
							ma_queue_t *item = ma_queue_head(&list->qlink);
							ma_icmp_repo_node_list_t *entry = ma_queue_data(item, ma_icmp_repo_node_list_t, qlink);
							if(entry && entry->name && entry->url) {
								ma_icmp_url_list_t *url_list = NULL;
								format_ipv6(entry->url);
								MA_MAP_GET_VALUE(ma_url_map_t, icmp_context->icmp_url_map, entry->url, url_list);
								if(url_list){
									ma_icmp_url_list_t *icmp_url_node = NULL ;
									if(MA_OK == (rc = ma_icmp_url_list_create(&icmp_url_node))){
										ma_icmp_url_list_init(icmp_url_node, entry->name);
										ma_queue_insert_tail(&(url_list)->qlink, &(icmp_url_node)->qlink);
										MA_LOG(icmp_context->logger, MA_LOG_SEV_TRACE, "repo <%s> url<%s> already echoed through different request.", entry->name, entry->url);
										ma_queue_remove(item);            
										ma_icmp_repo_node_list_release(entry);
										++icmp_context->url_remaining;
										continue;
									}
								}else{
									ma_icmp_url_list_t *list = NULL;
									ma_icmp_url_list_t *icmp_url_node = NULL ;
									if( (MA_OK == (rc = ma_icmp_url_list_create(&list))) &&
											(MA_OK == (rc = ma_icmp_url_list_create(&icmp_url_node))) ){
										ma_icmp_url_list_init(icmp_url_node, entry->name);
										ma_queue_insert_tail(&(list)->qlink, &(icmp_url_node)->qlink);
										MA_MAP_ADD_ENTRY(ma_url_map_t, icmp_context->icmp_url_map, strdup(entry->url), list);
									}else{
										MA_LOG(icmp_context->logger, MA_LOG_SEV_TRACE, "url list node creation failed for repo <%d>, skipping it .", entry->name);
										ma_queue_remove(item);            
										ma_icmp_repo_node_list_release(entry);
										continue;
									}
								}
								icmp_hdl = (ma_icmp_handle_t*) calloc (1, sizeof(ma_icmp_handle_t));
								icmp_hdl->name = strdup(entry->name);
								icmp_hdl->url = strdup(entry->url);
								icmp_hdl->is_numeric_url = is_numeric_address(icmp_hdl->url);
								icmp_hdl->ping_time_avg = (icmp_context->icmp_policy.max_ping_time * 1000) + 1; /* from seconds to milliseconds */
								icmp_hdl->subnet.subnet_distance_ceil = icmp_context->icmp_policy.max_subnet_distance;
								icmp_hdl->subnet.subnet_distance_floor = 0;
								icmp_hdl->subnet.subnet_distance_last_succes = icmp_context->icmp_policy.max_subnet_distance + 1;
								icmp_hdl->subnet.subnet_distance = icmp_context->icmp_policy.max_subnet_distance + 1;
								ma_queue_remove(item);            
								ma_icmp_repo_node_list_release(entry);
								icmp_hdl->icmp_context = icmp_context;
								++icmp_hdl->icmp_context->url_remaining;
								if(MA_OK != ma_sitelist_dns_resolution(icmp_hdl))
									ma_icmp_update_result(icmp_hdl);
							}
						}
					}
				}
				ma_icmp_repo_node_list_release(list);
			}
		}
	}
	return rc;
}

void ma_icmp_computation_stop(ma_icmp_context_t **icmp_context){
	if(icmp_context && *icmp_context){
	    MA_LOG((*icmp_context)->logger, MA_LOG_SEV_TRACE, "repository icmp computation stopping.");
		ma_icmp_context_release(icmp_context);
	}
}

ma_error_t ma_icmp_context_create(ma_icmp_context_t **icmp_context, void *data){
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	if(icmp_context){
		if((*icmp_context = (ma_icmp_context_t*)calloc(1, sizeof(ma_icmp_context_t)))){
			(*icmp_context)->data = data;
			rc = MA_OK;
		}
		else
			rc = MA_ERROR_APIFAILED;
	}
	return rc;
}

ma_error_t ma_icmp_context_set_method(ma_icmp_context_t *icmp_context, ma_icmp_methods_t *methods){
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	if(icmp_context){
			icmp_context->methods = methods;
			rc = MA_OK;
	}
	return rc;
}

ma_error_t ma_icmp_policy_create(ma_icmp_policy_t *icmp_policy, ma_uint16_t max_ping_time, ma_uint16_t max_ttl_value, ma_repository_rank_type_t rank_type){
	if(icmp_policy){
		icmp_policy->rank_type = rank_type;
		icmp_policy->max_ping_time = max_ping_time;
		icmp_policy->max_subnet_distance = max_ttl_value;
		return  MA_OK;
	}
	
	return MA_ERROR_APIFAILED;
}

ma_error_t ma_icmp_context_set_logger(ma_icmp_context_t *icmp_context, ma_logger_t	*logger){
	if(icmp_context && logger){
		icmp_context->logger = logger;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_icmp_context_set_uv_loop(ma_icmp_context_t *icmp_context, uv_loop_t* uv_loop){
	if(icmp_context && uv_loop){
		icmp_context->uv_loop = uv_loop;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_icmp_context_set_policy(ma_icmp_context_t *icmp_context, ma_icmp_policy_t icmp_policy){
	if(icmp_context ){
		icmp_context->icmp_policy.max_ping_time = icmp_policy.max_ping_time;
		icmp_context->icmp_policy.max_subnet_distance = icmp_policy.max_subnet_distance;
		icmp_context->icmp_policy.rank_type = icmp_policy.rank_type;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_icmp_context_set_data(ma_icmp_context_t *icmp_context, void *data){
	if(icmp_context && data){
		icmp_context->data = data;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_icmp_context_get_policy(ma_icmp_context_t *icmp_context, ma_icmp_policy_t *icmp_policy){
	if(icmp_context && icmp_policy){
		icmp_policy->max_ping_time = icmp_context->icmp_policy.max_ping_time;
		icmp_policy->max_subnet_distance = icmp_context->icmp_policy.max_subnet_distance;
		icmp_policy->rank_type = icmp_context->icmp_policy.rank_type;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

void * ma_icmp_context_get_data(ma_icmp_context_t *icmp_context){
	if(icmp_context ){ 
		return icmp_context->data;
	}
	return NULL;
}

static ma_error_t ma_icmp_context_release(ma_icmp_context_t **icmp_context){
	if(icmp_context && *icmp_context){
		if((*icmp_context)->ipv4_uv_poll_watcher){
			uv_poll_stop((*icmp_context)->ipv4_uv_poll_watcher);
			uv_close((uv_handle_t*)(*icmp_context)->ipv4_uv_poll_watcher, uv_watcher_close_cb);
			if((*icmp_context)->ipv4_uv_poll_watcher->data){
				free((*icmp_context)->ipv4_uv_poll_watcher->data);
				(*icmp_context)->ipv4_uv_poll_watcher->data = NULL;
			}
			(*icmp_context)->ipv4_uv_poll_watcher = NULL;
		}	

		if((*icmp_context)->ipv6_uv_poll_watcher){
			uv_poll_stop((*icmp_context)->ipv6_uv_poll_watcher);
			uv_close((uv_handle_t*)(*icmp_context)->ipv6_uv_poll_watcher, uv_watcher_close_cb);
			if((*icmp_context)->ipv6_uv_poll_watcher->data){
				free((*icmp_context)->ipv6_uv_poll_watcher->data);
				(*icmp_context)->ipv6_uv_poll_watcher->data = NULL;
			}
			(*icmp_context)->ipv6_uv_poll_watcher = NULL;
		}

		if(0 != (*icmp_context)->ipv4_socket_raw){
			closesocket((*icmp_context)->ipv4_socket_raw);
			(*icmp_context)->ipv4_socket_raw = 0;
		}
		
		if(0 != (*icmp_context)->ipv6_socket_raw){
			closesocket((*icmp_context)->ipv6_socket_raw);
			(*icmp_context)->ipv6_socket_raw = 0;
		}

		free((*icmp_context)->methods);
		(*icmp_context)->methods = NULL;

		{
			MA_MAP_FOREACH(ma_icmp_map_t, (*icmp_context)->icmp_pkts_map, iter){
				ma_icmp_handle_t *icmp_hdl_map_val;
				icmp_hdl_map_val =(ma_icmp_handle_t *) iter.second;
				if(icmp_hdl_map_val){
					ma_icmp_hdl_release(icmp_hdl_map_val);
				}
			}
		}
		MA_MAP_CLEAR(ma_icmp_map_t, (*icmp_context)->icmp_pkts_map);
		free((*icmp_context)->icmp_pkts_map);
		(*icmp_context)->icmp_pkts_map = NULL;

		{
			MA_MAP_FOREACH(ma_addrinfo_map_t, (*icmp_context)->icmp_addrinfo_map, iter){
				uv_getaddrinfo_t *addr_info;
				addr_info =(uv_getaddrinfo_t *) iter.second;
				if(addr_info){
					addr_info->data = NULL;
					/*Memory should free in callback only */
				}
			}
			MA_MAP_CLEAR(ma_addrinfo_map_t, (*icmp_context)->icmp_addrinfo_map);
			free((*icmp_context)->icmp_addrinfo_map);
			(*icmp_context)->icmp_addrinfo_map = NULL;
		}

		{
			MA_MAP_FOREACH(ma_url_map_t, (*icmp_context)->icmp_url_map, iter){
				ma_icmp_url_list_t *icmp_url_list_map_val;
				icmp_url_list_map_val =(ma_icmp_url_list_t *) iter.second;
				if(icmp_url_list_map_val){
					while (!ma_queue_empty(&icmp_url_list_map_val->qlink)) {
						ma_queue_t *item = ma_queue_head(&icmp_url_list_map_val->qlink);
						ma_icmp_url_list_t *entry = ma_queue_data(item, ma_icmp_url_list_t, qlink);
						ma_queue_remove(item); 
						ma_icmp_url_list_release(entry);
					}
					ma_icmp_url_list_release(icmp_url_list_map_val);
				}
			}
		}
		MA_MAP_CLEAR(ma_url_map_t, (*icmp_context)->icmp_url_map);
		free((*icmp_context)->icmp_url_map);
		(*icmp_context)->icmp_url_map = NULL;

		MA_LOG((*icmp_context)->logger, MA_LOG_SEV_TRACE, "icmp computation Finished");
		free((*icmp_context));
		(*icmp_context) = NULL;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}


ma_error_t ma_icmp_repo_node_list_create(ma_icmp_repo_node_list_t **icmp_repo_node_list){
	if(icmp_repo_node_list){
		if((*icmp_repo_node_list = (ma_icmp_repo_node_list_t*)calloc(1, sizeof(ma_icmp_repo_node_list_t)))){
			ma_queue_init(&(*icmp_repo_node_list)->qlink);
			return MA_OK;
		}
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_icmp_repo_node_list_init(ma_icmp_repo_node_list_t *icmp_repo_node_list, char *name, char *url, ma_bool_t is_numeric_url){
	if(icmp_repo_node_list && name && url){
		icmp_repo_node_list->name = strdup(name);
		icmp_repo_node_list->url = strdup(url);
		icmp_repo_node_list->is_numeric_url = is_numeric_url;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_icmp_repo_node_list_release(ma_icmp_repo_node_list_t *icmp_repo_node_list){
	if(icmp_repo_node_list){
		free(icmp_repo_node_list->name);
		free(icmp_repo_node_list->url);
		free(icmp_repo_node_list);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}


ma_error_t ma_icmp_url_list_create(ma_icmp_url_list_t **icmp_url_list){
	if(icmp_url_list){
		if((*icmp_url_list = (ma_icmp_url_list_t*)calloc(1, sizeof(ma_icmp_url_list_t)))){
			ma_queue_init(&(*icmp_url_list)->qlink);
			return MA_OK;
		}
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_icmp_url_list_init(ma_icmp_url_list_t *icmp_url_list, char *name){
	if(icmp_url_list && name){
		icmp_url_list->url = strdup(name);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_icmp_url_list_release(ma_icmp_url_list_t *icmp_url_list){
	if(icmp_url_list && icmp_url_list->url){
		free(icmp_url_list->url);
		icmp_url_list->url = NULL;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}
