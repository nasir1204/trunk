#include "ma/internal/utils/search/ma_search_datastore.h"
#include "ma/internal/defs/ma_search_defs.h"
#include "ma/scheduler/ma_enumerator.h"
#include "ma/internal/ma_macros.h"
#include "ma/ma_log.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "search"

extern ma_logger_t *search_logger;

#define SEARCH_DATA MA_DATASTORE_PATH_SEPARATOR "search_data"
#define SEARCH_ID MA_DATASTORE_PATH_SEPARATOR "search_id"

#define SEARCH_DATA_PATH SEARCH_DATA SEARCH_ID MA_DATASTORE_PATH_SEPARATOR/* \"search_data"\"search_id"\ */


ma_error_t ma_search_datastore_read(ma_search_t *search, ma_ds_t *datastore, const char *ds_path) {
    if(search && datastore && ds_path) {
        ma_error_t err = MA_OK;
        ma_ds_iterator_t *search_iterator = NULL;
        ma_buffer_t *search_id_buf = NULL;
        char search_data_base_path[MA_MAX_PATH_LEN] = {0};

        MA_MSC_SELECT(_snprintf, snprintf)(search_data_base_path, MA_MAX_PATH_LEN, "%s%s", ds_path, SEARCH_DATA_PATH);
        MA_LOG(search_logger, MA_LOG_SEV_DEBUG, "search data store path(%s) reading...", search_data_base_path);
        if(MA_OK == (err = ma_ds_iterator_create(datastore, 0, search_data_base_path, &search_iterator))) {
            while(MA_ERROR_NO_MORE_ITEMS != ma_ds_iterator_get_next(search_iterator, &search_id_buf) && search_id_buf) {
                const char *search_id = NULL;
                size_t size = 0;
                
                if(MA_OK == (err = ma_buffer_get_string(search_id_buf, &search_id, &size))) {
                    char search_data_path[MA_MAX_PATH_LEN] = {0};
                    ma_variant_t *search_var = NULL;
                    MA_MSC_SELECT(_snprintf, snprintf)(search_data_path, MA_MAX_PATH_LEN, "%s", search_data_base_path);/* base_path\"search_data"\"search_id"\4097 */
                    if(MA_OK == (err = ma_ds_get_variant(datastore, search_data_path, search_id, MA_VARTYPE_TABLE, &search_var))) {

                        (void)ma_variant_release(search_var);
                    }
                }
                (void)ma_buffer_release(search_id_buf);
            }
            (void)ma_ds_iterator_release(search_iterator);
        }

        return ((MA_ERROR_DS_PATH_NOT_FOUND == err) || (MA_ERROR_SCHEDULER_TASK_ID_CONFLICT == err)) ? MA_OK : err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_search_datastore_write(ma_search_t *search, ma_search_info_t *info, ma_ds_t *datastore, const char *ds_path) {
    if(search && info && datastore && ds_path) {
        char search_data_path[MA_MAX_PATH_LEN] = {0};
        const char *search_id = NULL;
        ma_error_t err = MA_OK;
        ma_variant_t *var = NULL;

        (void)ma_search_info_get_id(info, &search_id);
        MA_MSC_SELECT(_snprintf, snprintf)(search_data_path, MA_MAX_PATH_LEN, "%s%s", ds_path, SEARCH_DATA_PATH);
        MA_LOG(search_logger, MA_LOG_SEV_INFO, "search id(%s), data path(%s) search_data_path(%s)", search_id, ds_path, search_data_path);
        if(MA_OK == (err = ma_search_info_convert_to_variant(info, &var))) {
            if(MA_OK != (err = ma_ds_set_variant(datastore, search_data_path, search_id, var)))
                MA_LOG(search_logger, MA_LOG_SEV_DEBUG, "search datastore write search id(%s) failed", search_id);
            (void)ma_variant_release(var);
        } else
            MA_LOG(search_logger, MA_LOG_SEV_DEBUG, "search datastore write search id conversion search info to variant failed, last error(%d)", err);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_search_datastore_write_variant(ma_search_t *search, const char *search_id, ma_ds_t *datastore, const char *ds_path, ma_variant_t *var) {
    if(search && search_id && var && datastore && ds_path) {
        char search_data_path[MA_MAX_PATH_LEN] = {0};
        ma_error_t err = MA_OK;

        MA_MSC_SELECT(_snprintf, snprintf)(search_data_path, MA_MAX_PATH_LEN, "%s%s", ds_path, SEARCH_DATA_PATH);
        MA_LOG(search_logger, MA_LOG_SEV_INFO, "search id(%s), data path(%s) search_data_path(%s)", search_id, ds_path, search_data_path);
        if(MA_OK == (err = ma_ds_set_variant(datastore, search_data_path, search_id, var)))
            MA_LOG(search_logger, MA_LOG_SEV_INFO, "search datastore write search id(%s) success", search_id);
        else
            MA_LOG(search_logger, MA_LOG_SEV_ERROR, "search datastore write search id(%s) failed, last error(%d)", search_id, err);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_search_datastore_remove_search(const char *search, ma_ds_t *datastore, const char *ds_path) {
    if(search && datastore && ds_path) {
        char search_path[MA_MAX_PATH_LEN] = {0};
        
        MA_LOG(search_logger, MA_LOG_SEV_DEBUG, "search removing (%s) from datastore", search);
        MA_MSC_SELECT(_snprintf, snprintf)(search_path, MA_MAX_PATH_LEN, "%s%s",ds_path, SEARCH_DATA_PATH );/* base_path\"search_data"\4097 */
        return ma_ds_rem(datastore, search_path, search, 1);
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_search_datastore_get(ma_search_t *search, ma_ds_t *datastore, const char *ds_path, const char *search_id, ma_search_info_t **info) {
    if(search && datastore && ds_path && search_id && info) {
        ma_error_t err = MA_OK;
        char search_data_base_path[MA_MAX_PATH_LEN] = {0};
        ma_variant_t *search_var = NULL;

        MA_MSC_SELECT(_snprintf, snprintf)(search_data_base_path, MA_MAX_PATH_LEN, "%s%s", ds_path, SEARCH_DATA_PATH);
        if(MA_OK == (err = ma_ds_get_variant(datastore, search_data_base_path, search_id, MA_VARTYPE_TABLE, &search_var))) {
            if(MA_OK == (err = ma_search_info_convert_from_variant(search_var, info)))
                MA_LOG(search_logger, MA_LOG_SEV_TRACE, "search(%s) exist in search DB", search_id);
            (void)ma_variant_release(search_var);
        } else 
            MA_LOG(search_logger, MA_LOG_SEV_ERROR, "search(%s) does not exist in search DB, last error(%d)", search_id, err);

        return err;
    }
    return MA_ERROR_INVALIDARG;

}

ma_error_t ma_search_datastore_clear(ma_ds_t *datastore, const char *ds_path) {
    return(datastore && ds_path) ? ma_ds_rem(datastore, ds_path, NULL, 1) : MA_ERROR_INVALIDARG;
}

