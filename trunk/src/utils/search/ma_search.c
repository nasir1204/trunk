#include "ma/internal/utils/search/ma_search.h"
#include "ma/internal/utils/search/ma_search_info.h"
#include "ma/internal/utils/search/ma_search_datastore.h"
#include "ma/ma_msgbus.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/defs/ma_search_defs.h"
#include "ma/internal/ma_macros.h"

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "search"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <uv.h>

ma_logger_t *search_logger;

struct ma_search_s {
    char path[MA_MAX_PATH_LEN+1];
    ma_msgbus_t *msgbus;
    ma_ds_t* datastore;
};


ma_error_t ma_search_create(ma_msgbus_t *msgbus, ma_ds_t *datastore, const char *base_path, ma_search_t **search) {
    if(msgbus && datastore && base_path && search) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;

        if((*search = (ma_search_t*)calloc(1, sizeof(ma_search_t)))) {
            (*search)->msgbus = msgbus;
            strncpy((*search)->path, base_path, MA_MAX_PATH_LEN);
            (*search)->datastore = datastore;
            err = MA_OK;
            MA_LOG(search_logger, MA_LOG_SEV_INFO, "search created successfully");
        }
        if(MA_OK != err) {
            MA_LOG(search_logger, MA_LOG_SEV_ERROR, "search creation failed, last error(%d)", err);
            if(*search)
                (void)ma_search_release(*search);
            *search = NULL;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_search_add(ma_search_t *search, ma_search_info_t *info) {
    return search && info ? ma_search_datastore_write(search, info, search->datastore, search->path) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_search_add_variant(ma_search_t *search, const char *search_id, ma_variant_t *var) {
    return search && search_id && var ? ma_search_datastore_write_variant(search, search_id, search->datastore, search->path, var) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_search_delete(ma_search_t *search, const char *search_id) {
    return search && search_id ? ma_search_datastore_remove_search(search_id, search->datastore, search->path) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_search_delete_all(ma_search_t *search) {
    return search ? ma_search_datastore_clear(search->datastore, search->path) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_search_get(ma_search_t *search, const char *search_id, ma_search_info_t **info) {
    return search && search && info ? ma_search_datastore_get(search, search->datastore, search->path, search_id, info) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_search_set_logger(ma_logger_t *logger) {
    if(logger) {
        search_logger = logger;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_search_get_msgbus(ma_search_t *search, ma_msgbus_t **msgbus) {
    if(search && msgbus) {
        *msgbus = search->msgbus;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_search_start(ma_search_t *search) {
    if(search) {
        MA_LOG(search_logger, MA_LOG_SEV_TRACE, "search started");
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_search_stop(ma_search_t *search) {
    if(search) {
        MA_LOG(search_logger, MA_LOG_SEV_TRACE, "search stopped");
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_search_release(ma_search_t *search) {
    if(search) {
        free(search);
    }
    return MA_ERROR_INVALIDARG;
}

