#include "ma_db_sqlite.h"

#include <stdlib.h>
#include <string.h>

ma_error_t ma_db_sqlite_stmt_create(void *db, const char *stmt, void **db_stmt) {
    if(db && stmt && db_stmt) {
        ma_db_sqlite_stmt_t *st = (ma_db_sqlite_stmt_t *)calloc(1,sizeof(ma_db_sqlite_stmt_t));
        if(!st) return MA_ERROR_OUTOFMEMORY;        
        
		st->db = ((ma_db_sqlite_t *)db);            

        //TODO - proper return code and stmt validataion
        if(SQLITE_OK != sqlite3_prepare_v2(st->db->db_handle,stmt,-1,&st->stmt,NULL)) {
            free(st);
            return MA_ERROR_DB_CREATE_STATEMENT_FAILED;
        }
        MA_ATOMIC_INCREMENT(st->ref_cnt);
        *db_stmt = st;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_db_sqlite_stmt_add_ref(void *stmt) {
    if (stmt) {
        ma_db_sqlite_stmt_t *self = (ma_db_sqlite_stmt_t *)stmt;
        MA_ATOMIC_INCREMENT(self->ref_cnt);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
   
}

static ma_error_t ma_db_sqlite_stmt_destroy(void *stmt) {
    if(stmt) {
        ma_db_sqlite_stmt_t *self = (ma_db_sqlite_stmt_t *)stmt;
        sqlite3_reset(self->stmt);
        sqlite3_finalize(self->stmt);
        free(self);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_db_sqlite_stmt_release(void *stmt) {
    if(stmt) {
        ma_db_sqlite_stmt_t *self = (ma_db_sqlite_stmt_t *)stmt;
        if(0 == MA_ATOMIC_DECREMENT(self->ref_cnt)) {
            (void)ma_db_sqlite_stmt_destroy(self);
        }
		return MA_OK;
    }
    return MA_ERROR_INVALIDARG;

}
ma_error_t ma_db_sqlite_stmt_set_int(void *stmt, int parameter_index, int value) {
    if(stmt  && parameter_index >0 ) {
        ma_db_sqlite_stmt_t *self = (ma_db_sqlite_stmt_t *)stmt;
        sqlite3_reset(self->stmt);
        return (SQLITE_OK == sqlite3_bind_int(self->stmt, parameter_index, value)) 
                ? MA_OK: MA_ERROR_DB_STATEMENT_SET_FAILED;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_db_sqlite_stmt_set_long(void *stmt, int parameter_index, ma_int64_t value) {
    if(stmt  && parameter_index >0 ) {
        ma_db_sqlite_stmt_t *self = (ma_db_sqlite_stmt_t *)stmt;
        sqlite3_reset(self->stmt);
        return (SQLITE_OK == sqlite3_bind_int64(self->stmt, parameter_index, value)) 
                ? MA_OK: MA_ERROR_DB_STATEMENT_SET_FAILED;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_db_sqlite_stmt_set_string(void *stmt, int parameter_index, unsigned short is_data_transient, const char *value, size_t size) {
    if(stmt && parameter_index > 0 ) { // && value) {
        ma_db_sqlite_stmt_t *self = (ma_db_sqlite_stmt_t *)stmt;
        sqlite3_reset(self->stmt);
        
        return (SQLITE_OK == sqlite3_bind_text(self->stmt, parameter_index, value, ((size_t)-1) == size ? strlen(value) : size, (1 == is_data_transient) ? SQLITE_TRANSIENT : SQLITE_STATIC))
                ? MA_OK: MA_ERROR_DB_STATEMENT_SET_FAILED;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_db_sqlite_stmt_set_blob(void *stmt, int parameter_index, unsigned short is_data_transient, const void *value, size_t size) {
      if(stmt && parameter_index > 0 ) { //&& value) {
        ma_db_sqlite_stmt_t *self = (ma_db_sqlite_stmt_t *)stmt;
        sqlite3_reset(self->stmt);
        return (SQLITE_OK == sqlite3_bind_blob(self->stmt, parameter_index, value, size, (1 == is_data_transient) ? SQLITE_TRANSIENT : SQLITE_STATIC))
                ? MA_OK: MA_ERROR_DB_STATEMENT_SET_FAILED;
    } 
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_db_sqlite_stmt_execute(void *stmt) {
    if(stmt) {
        ma_db_sqlite_stmt_t *self = (ma_db_sqlite_stmt_t *)stmt;
        int rc = SQLITE_OK;
        rc = sqlite3_step(self->stmt);
        switch(rc) {
            case SQLITE_DONE: 
                sqlite3_reset(self->stmt);
                return MA_OK;
            case SQLITE_ROW :
                sqlite3_reset(self->stmt);
                return MA_ERROR_DB_STATEMENT_NO_QUERY_ALLOWED;
            default:
                sqlite3_reset(self->stmt);
                return MA_ERROR_DB_STATEMENT_EXECUTE_FAILED;
        }
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_db_sqlite_stmt_execute_query(void *stmt, void **recordset) {
    if(stmt && recordset) {
        ma_db_sqlite_stmt_t *self = (ma_db_sqlite_stmt_t *)stmt;
        ma_db_sqlite_recordset_t *rs = (ma_db_sqlite_recordset_t*)calloc(1,sizeof(ma_db_sqlite_recordset_t));
        if(!rs) return MA_ERROR_OUTOFMEMORY;

        rs->column_count = sqlite3_column_count(self->stmt);
        ma_db_sqlite_stmt_add_ref(rs->db_stmt = self);
        *recordset = rs ;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

