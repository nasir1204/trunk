#ifndef MA_DB_SQLITE_RECORDSET_H_INCLUDED
#define MA_DB_SQLITE_RECORDSET_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_variant.h"

MA_CPP(extern "C" {)

ma_error_t ma_db_sqlite_recordset_release(void *recordset);
ma_error_t ma_db_sqlite_recordset_get_column_count(void *recordset, int *count);
ma_error_t ma_db_sqlite_recordset_get_column_name(void *recordset, int column_index, const char **column_name);
ma_error_t ma_db_sqlite_recordset_get_string(void *recordset, int column_index, const char **value);
ma_error_t ma_db_sqlite_recordset_get_int(void *recordset, int column_index, int *value);
ma_error_t ma_db_sqlite_recordset_get_long(void *recordset, int column_index, ma_int64_t *value);
ma_error_t ma_db_sqlite_recordset_get_blob(void *recordset, int column_index, const void **value, size_t *size);
ma_error_t ma_db_sqlite_recordset_next(void *recordset);

#define MA_DB_RECORDSET_RELEASE                 ma_db_sqlite_recordset_release
#define MA_DB_RECORDSET_GET_COLUMN_COUNT        ma_db_sqlite_recordset_get_column_count
#define MA_DB_RECORDSET_GET_COLUMN_NAME         ma_db_sqlite_recordset_get_column_name
#define MA_DB_RECORDSET_GET_STRING              ma_db_sqlite_recordset_get_string
#define MA_DB_RECORDSET_GET_INT                 ma_db_sqlite_recordset_get_int
#define MA_DB_RECORDSET_GET_LONG                ma_db_sqlite_recordset_get_long
#define MA_DB_RECORDSET_GET_BLOB                ma_db_sqlite_recordset_get_blob
#define MA_DB_RECORDSET_GET_VARIANT             ma_db_sqlite_recordset_get_variant
#define MA_DB_RECORDSET_NEXT                    ma_db_sqlite_recordset_next


MA_CPP(})

#endif	/* MA_DB_SQLITE_RECORDSET_H_INCLUDED */

