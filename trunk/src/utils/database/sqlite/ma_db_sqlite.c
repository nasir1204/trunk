#include "ma_db_sqlite.h"
#include "ma/internal/utils/database/ma_db.h"
#include "ma/internal/utils/threading/ma_atomic.h"

#include "sqlite3.h"

#include <stdlib.h>
#include <stdio.h>
#include <wchar.h>


#ifdef MA_WINDOWS
#include <Windows.h>
#include <WinBase.h>
#endif

static ma_atomic_counter_t g_ref_count = 0;

static void  ma_db_sqlite_initialize() {
	if(!g_ref_count)
		sqlite3_initialize();

	MA_ATOMIC_INCREMENT(g_ref_count);
}

static void ma_db_sqlite_shutdown() {
    if (0 == MA_ATOMIC_DECREMENT(g_ref_count)) {
        sqlite3_shutdown();
    }
}

/*Even for read we are opening up in read write mode, as this will repair the journal file if it exits and will not block us 
	http://sqlite.1065341.n5.nabble.com/Prepare-SQL-for-Read-Only-Database-with-Journal-File-td62039.html
*/
void  ma_db_sqlite_journal_repair_in_read_mode(const char *db_name) {
	sqlite3 *db_handle = NULL;
	if(0 == sqlite3_open_v2(db_name, &db_handle, SQLITE_OPEN_READWRITE | SQLITE_OPEN_NOMUTEX , NULL)) {        
		sqlite3_exec(db_handle, "PRAGMA synchronous = ON", NULL, NULL, NULL) ;
		sqlite3_close(db_handle) ;
	}
	return ;
}

ma_error_t  ma_db_sqlite_open(const char *db_name, int flags, void **db) {     
    /* NOTE : no need to check input params as it should be checked with upper layer */
    int sql_rc = 0; 
    ma_db_sqlite_t *self = (ma_db_sqlite_t *)calloc(1, sizeof(ma_db_sqlite_t));
    if(!self)   return MA_ERROR_OUTOFMEMORY;

	ma_db_sqlite_initialize();
    
	flags = (MA_DB_OPEN_READWRITE == flags) ? (SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE | SQLITE_OPEN_NOMUTEX) : (SQLITE_OPEN_READONLY | SQLITE_OPEN_NOMUTEX);

    if(0 != (sql_rc = sqlite3_open_v2(db_name, &self->db_handle, flags, NULL))) {        
		if(MA_DB_OPEN_READ_WITH_REPAIR == flags) {
			ma_db_sqlite_journal_repair_in_read_mode(db_name);
			sql_rc = sqlite3_open_v2(db_name, &self->db_handle, flags, NULL);
		}
    }

	if(0 != sql_rc) {
		free(self);
		ma_db_sqlite_shutdown();
		return MA_ERROR_DB_OPEN_FAILED;
	}

	/* set connection specific parameters 
	TODO need to move this to the place where DB's are created if individual needs different values 
	Default sqlite allocates 128*500 bytes as lookaside buffer*/
	(void)sqlite3_db_config(self->db_handle, SQLITE_DBCONFIG_LOOKASIDE, NULL, 128, 100) ;

	/* http://sqlite.org/pragma.html#pragma_cache_size default is 2000 pages
		is 10 enough, need to do an regression on DC and change if required lesser the size degrads the performace of sqlite */
	sqlite3_exec(self->db_handle, "PRAGMA cache_size = 10", NULL, NULL, NULL) ;

	/* http://www.sqlite.org/pragma.html#pragma_auto_vacuum */
	sqlite3_exec(self->db_handle, "PRAGMA auto_vacuum = 2", NULL, NULL, NULL) ;
	
	/* http://sqlite.org/pragma.html#pragma_incremental_vacuum */
	sqlite3_exec(self->db_handle, "PRAGMA incremental_vacuum(0)", NULL, NULL, NULL) ;


	/*   http://www.sqlite.org/c3ref/busy_timeout.html 
		It doesnt looks like a cumaltive lock , it retries in fractions till it reaches the max 
	*/
    sqlite3_busy_timeout(self->db_handle, SQLITE_BUSY_TIMEOUT);
	
	

    *db = self;
    return MA_OK;
}


/*Optimization for adding multiple records in one transcation
1. Inside the transaction 
2. Multiple values in one shot 
3. Create the Indexes - TODO
4. PRAGMA synchronous = OFF - TODO - should we use this ??????
        *****NOTE NOTE By default SQLite will pause after issuing a OS-level write command. This guarantees that the data is written to the disk. 
        By setting synchronous = OFF, we are instructing SQLite to simply hand-off the data to the OS for writing and then continue. 
        There's a chance that the database file may become corrupted if the computer suffers a catastrophic crash (or power failure) before the data is written.
5. PRAGMA journal_mode = MEMORY TODO - should we use this ??????
*/
ma_error_t  ma_db_sqlite_transaction_begin(void *db) {
    if(db) {
        ma_db_sqlite_t *self = (ma_db_sqlite_t *)db;
        int sql_rc = 0;  
        /*Experimentation with synchronous pragma */
        sqlite3_exec(self->db_handle, "PRAGMA synchronous = OFF", NULL, NULL, NULL);
        if(0 != (sql_rc = sqlite3_exec(self->db_handle, "BEGIN TRANSACTION", NULL, NULL, NULL))) {
            /*Experimentation with synchronous pragma */
            sqlite3_exec(self->db_handle, "PRAGMA synchronous = ON", NULL, NULL, NULL);            
        }
        return 0 == sql_rc ? MA_OK : MA_ERROR_DB_TX_BEGIN_FAILED;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t  ma_db_sqlite_transaction_cancel(void *db) {
    if(db) {
        ma_db_sqlite_t *self = (ma_db_sqlite_t *)db;
        int sql_rc = 0;  
        sql_rc = sqlite3_exec(self->db_handle, "ROLLBACK TRANSACTION", NULL, NULL, NULL);
        /*Experimentation with synchronous pragma */
        sqlite3_exec(self->db_handle, "PRAGMA synchronous = ON", NULL, NULL, NULL);
        return 0 == sql_rc ? MA_OK : MA_ERROR_DB_TX_CANCEL_FAILED;        
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t  ma_db_sqlite_transaction_end(void *db) {
    if(db) {
        ma_db_sqlite_t *self = (ma_db_sqlite_t *)db;
        int sql_rc = 0;  
        sql_rc = sqlite3_exec(self->db_handle, "END TRANSACTION", NULL, NULL, NULL);
        /*Experimentation with synchronous pragma */
        sqlite3_exec(self->db_handle, "PRAGMA synchronous = ON", NULL, NULL, NULL);
        return 0 == sql_rc ? MA_OK : MA_ERROR_DB_TX_END_FAILED;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t  ma_db_sqlite_provision(void *db) {
    if(db) {
        ma_db_sqlite_t *self = (ma_db_sqlite_t *)db;
        int sql_rc = 0;  
        sql_rc = sqlite3_exec(self->db_handle, "PRAGMA shrink_memory", NULL, NULL, NULL);
        return 0 == sql_rc ? MA_OK : MA_ERROR_DB_TX_END_FAILED;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t  ma_db_sqlite_close(void *db) {                
    if(db ){
        ma_db_sqlite_t *self = (ma_db_sqlite_t *)db;
        if(self->db_handle) {
            sqlite3_close(self->db_handle);
			ma_db_sqlite_shutdown();
        }
        free(self);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}



