#ifndef MA_DB_SQLITE_STMT_H_INCLUDED
#define MA_DB_SQLITE_STMT_H_INCLUDED

#include "ma/ma_common.h"

MA_CPP(extern "C" {)

ma_error_t ma_db_sqlite_stmt_create(void *handle, const char *stmt, void **db_stmt);
ma_error_t ma_db_sqlite_stmt_release(void *db_stmt);
ma_error_t ma_db_sqlite_stmt_set_int(void *db_stmt, int parameter_index, int value);
ma_error_t ma_db_sqlite_stmt_set_long(void *db_stmt, int parameter_index, ma_int64_t value);
ma_error_t ma_db_sqlite_stmt_set_string(void *db_stmt, int parameter_index, unsigned short is_data_transient, const char *value, size_t size);
ma_error_t ma_db_sqlite_stmt_set_blob(void *db_stmt, int parameter_index, unsigned short is_data_transient, const void *value, size_t size);
ma_error_t ma_db_sqlite_stmt_execute(void *db_stmt);
ma_error_t ma_db_sqlite_stmt_execute_query(void *db_stmt, void **recordset);

#define MA_DB_STATEMENT_CREATE          ma_db_sqlite_stmt_create
#define MA_DB_STATEMENT_RELEASE         ma_db_sqlite_stmt_release
#define MA_DB_STATEMENT_SET_INT         ma_db_sqlite_stmt_set_int
#define MA_DB_STATEMENT_SET_STRING      ma_db_sqlite_stmt_set_string
#define MA_DB_STATEMENT_SET_BLOB        ma_db_sqlite_stmt_set_blob
#define MA_DB_STATEMENT_SET_LONG        ma_db_sqlite_stmt_set_long
#define MA_DB_STATEMENT_EXECUTE         ma_db_sqlite_stmt_execute
#define MA_DB_STATEMENT_EXECUTE_QUERY   ma_db_sqlite_stmt_execute_query

MA_CPP(})

#endif	/* MA_DB_SQLITE_H_INCLUDED */

