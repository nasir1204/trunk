#include "ma_db_sqlite.h"

#include <stdlib.h>
#include <string.h>

ma_error_t ma_db_sqlite_recordset_release(void *rs) {
    if(rs) {
        ma_db_sqlite_recordset_t *self = (ma_db_sqlite_recordset_t *)rs;        
        sqlite3_reset(self->db_stmt->stmt);
        (void)ma_db_sqlite_stmt_release(self->db_stmt);
        free(self);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_db_sqlite_recordset_get_column_count(void *rs, int *count)  {
    if(rs && count) {
        ma_db_sqlite_recordset_t *self = (ma_db_sqlite_recordset_t *)rs;
        *count = self->column_count;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

#define TEST_INDEX\
        int i = column_index - 1; \
        if(self->current_row == 0) \
            return MA_ERROR_DB_RECORDSET_NEXT_NOT_CALLED; \
        if(self->column_count <= 0 || i < 0 || i >= self->column_count) \
            return MA_ERROR_DB_OUT_OF_RANGE;


ma_error_t ma_db_sqlite_recordset_get_column_name(void *rs, int column_index, const char **column_name) {
    if(rs && column_name) {
        ma_db_sqlite_recordset_t *self = (ma_db_sqlite_recordset_t *)rs;
        const char *p = NULL;
        TEST_INDEX
        p = sqlite3_column_name(self->db_stmt->stmt,i);
        if(!p) return MA_ERROR_DB_RECORDSET_GET_FAILED;

        *column_name = p ;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_db_sqlite_recordset_get_string(void *rs, int column_index, const char **value) {
    if(rs && value) {
        ma_db_sqlite_recordset_t *self = (ma_db_sqlite_recordset_t *)rs;
        const char *p = NULL;
        TEST_INDEX
        p = (char *)sqlite3_column_text(self->db_stmt->stmt,i);
        if(!p) {
			/* Returning NULL if there is no data in the specified column */
			*value = NULL;
			return MA_ERROR_DB_RECORDSET_GET_FAILED;
		}

        *value = p ;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_db_sqlite_recordset_get_int(void *rs, int column_index, int *value) {
    if(rs && value) {
        ma_db_sqlite_recordset_t *self = (ma_db_sqlite_recordset_t *)rs;
        TEST_INDEX
        *value = sqlite3_column_int(self->db_stmt->stmt,i);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_db_sqlite_recordset_get_long(void *rs, int column_index, ma_int64_t *value) {
    if(rs && value) {
        ma_db_sqlite_recordset_t *self = (ma_db_sqlite_recordset_t *)rs;
        TEST_INDEX
        *value = sqlite3_column_int64(self->db_stmt->stmt,i);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_db_sqlite_recordset_get_blob(void *rs, int column_index, const void **value, size_t *size) {
    if(rs && value) {
        ma_db_sqlite_recordset_t *self = (ma_db_sqlite_recordset_t *)rs;
        const void *p = NULL;
        TEST_INDEX
        p  = sqlite3_column_blob(self->db_stmt->stmt,i);
        if(!p) return MA_ERROR_DB_RECORDSET_GET_FAILED;
        if(size)
            *size = sqlite3_column_bytes(self->db_stmt->stmt,i);
        *value = p ;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_db_sqlite_recordset_next(void *rs) {
    if(rs) {
        ma_db_sqlite_recordset_t *self = (ma_db_sqlite_recordset_t *)rs;
        ma_error_t rc = MA_ERROR_NO_MORE_ITEMS;
        if(SQLITE_ROW == sqlite3_step(self->db_stmt->stmt)) {
            self->current_row++;
            rc = MA_OK;
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}
