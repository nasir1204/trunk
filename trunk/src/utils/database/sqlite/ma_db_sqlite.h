#ifndef MA_DB_SQLITE_H_INCLUDED
#define MA_DB_SQLITE_H_INCLUDED

#include "ma/ma_common.h"

#include "ma/internal/utils/platform/ma_dl.h"
#include "ma/internal/utils/threading/ma_atomic.h"

#include "ma_db_sqlite_stmt.h"
#include "ma_db_sqlite_recordset.h"

#include "sqlite3.h"

MA_CPP(extern "C" {)

ma_error_t  ma_db_sqlite_open(const char *db_name, int flags, void **handle);
ma_error_t  ma_db_sqlite_close(void *handle);    
ma_error_t  ma_db_sqlite_transaction_begin(void *handle);
ma_error_t  ma_db_sqlite_transaction_cancel(void *handle);
ma_error_t  ma_db_sqlite_transaction_end(void *handle);
ma_error_t  ma_db_sqlite_provision(void *handle);
ma_error_t  ma_db_sqlite_deinitialize();




#define MA_DB_OPEN                      ma_db_sqlite_open 
#define MA_DB_CLOSE                     ma_db_sqlite_close
#define MA_DB_TX_BEGIN                  ma_db_sqlite_transaction_begin
#define MA_DB_TX_CANCEL                 ma_db_sqlite_transaction_cancel
#define MA_DB_TX_END				    ma_db_sqlite_transaction_end
#define MA_DB_PROVISION				    ma_db_sqlite_provision
#define MA_DB_DEINITIALIZE              ma_db_sqlite_deinitialize


/*http://www.sqlite.org/c3ref/busy_timeout.html*/
#define SQLITE_BUSY_TIMEOUT  10 * 1000   /* 10 seconds sleep retry in case of locks on the table, timeout */

#define SQL_STMT    sqlite3_stmt


typedef struct ma_db_sqlite_s {        
    sqlite3	        *db_handle; 
}ma_db_sqlite_t;

typedef struct ma_db_sqlite_stmt_s {
    ma_db_sqlite_t      *db;
    SQL_STMT            *stmt;
    ma_atomic_counter_t ref_cnt;
}ma_db_sqlite_stmt_t;

typedef struct ma_db_sqlite_recordset_s {
    ma_db_sqlite_stmt_t *db_stmt;
    int                 column_count;
    long                current_row;              
}ma_db_sqlite_recordset_t;

MA_CPP(})

#endif	/* MA_DB_SQLITE_H_INCLUDED */

