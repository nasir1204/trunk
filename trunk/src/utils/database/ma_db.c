#include "ma/internal/utils/database/ma_db.h"

#include "sqlite/ma_db_sqlite.h"

#include <stdlib.h>
#include <string.h>



ma_error_t ma_db_open(const char *db_name, int flags, ma_db_t **database)  {
    
    if(db_name && (strlen(db_name) > 0) && database && ((MA_DB_OPEN_READONLY == flags) || (MA_DB_OPEN_READ_WITH_REPAIR == flags) || (MA_DB_OPEN_READWRITE == flags))) {
            return MA_DB_OPEN(db_name,flags,(void **)database);
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_db_close(ma_db_t *self) {
    if(self) {
        MA_DB_CLOSE(self);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_db_transaction_begin(ma_db_t *self) {
    return self ? MA_DB_TX_BEGIN(self): MA_ERROR_INVALIDARG;

}

ma_error_t ma_db_transaction_cancel(ma_db_t *self) {
    return self ? MA_DB_TX_CANCEL(self): MA_ERROR_INVALIDARG;
}

ma_error_t ma_db_transaction_end(ma_db_t *self) {
    return self ? MA_DB_TX_END(self): MA_ERROR_INVALIDARG;
}

ma_error_t ma_db_provision(ma_db_t *self) {
    return self ? MA_DB_PROVISION(self): MA_ERROR_INVALIDARG;
}