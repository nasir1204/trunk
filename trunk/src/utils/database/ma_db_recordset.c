#include "ma/internal/utils/database/ma_db_recordset.h"
#include "ma/internal/utils/serialization/ma_deserialize.h"
#include "ma/internal/ma_macros.h"

#include "sqlite/ma_db_sqlite.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

ma_error_t ma_db_recordset_release(ma_db_recordset_t *self) {
    return self ? MA_DB_RECORDSET_RELEASE(self) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_db_recordset_get_column_count(ma_db_recordset_t *self, int *count) {
    return (self && count) ? MA_DB_RECORDSET_GET_COLUMN_COUNT(self, count) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_db_recordset_get_column_name(ma_db_recordset_t *self, int column_index, const char **column_name) {
    return (self && column_index > 0 && column_name) ? MA_DB_RECORDSET_GET_COLUMN_NAME(self, column_index, column_name) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_db_recordset_get_string(ma_db_recordset_t *self, int column_index, const char **value) {
    return (self && column_index > 0  && value) ? MA_DB_RECORDSET_GET_STRING(self, column_index, value) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_db_recordset_get_int(ma_db_recordset_t *self, int column_index, int *value) {
    return (self && column_index > 0 && value) ? MA_DB_RECORDSET_GET_INT(self, column_index, value) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_db_recordset_get_long(ma_db_recordset_t *self, int column_index, ma_int64_t *value) {
    return (self && column_index > 0 && value) ? MA_DB_RECORDSET_GET_LONG(self, column_index, value) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_db_recordset_get_blob(ma_db_recordset_t *self, int column_index, const void **value, size_t *size) {
    return (self && column_index > 0 && value && size) ? MA_DB_RECORDSET_GET_BLOB(self, column_index, value, size) : MA_ERROR_INVALIDARG;
}


ma_error_t ma_db_recordset_get_variant(ma_db_recordset_t *self, int column_index, ma_vartype_t type, ma_variant_t **value) {
    if(self && column_index > 0 && value) {
        ma_error_t rc = MA_OK;
        char *p = NULL ;
        size_t size = 0;
		switch(type) {
			case MA_VARTYPE_INT8 :
			case MA_VARTYPE_UINT8 :
			case MA_VARTYPE_INT16 :
			case MA_VARTYPE_UINT16 :
			case MA_VARTYPE_INT32 :
			case MA_VARTYPE_UINT32 :
			case MA_VARTYPE_INT64: 
			case MA_VARTYPE_UINT64:
			case MA_VARTYPE_BOOL: {
				ma_int64_t val = 0;
				if(MA_OK == (rc = MA_DB_RECORDSET_GET_LONG((void *) self, column_index, &val))) {
					if(type == MA_VARTYPE_INT8 )
						rc = ma_variant_create_from_int8((ma_int8_t) val, value);
					else if(type == MA_VARTYPE_UINT8 )
						rc = ma_variant_create_from_uint8((ma_uint8_t) val, value);
					else if(type == MA_VARTYPE_INT16 )
						rc = ma_variant_create_from_int16((ma_int16_t) val, value);
					else if(type == MA_VARTYPE_UINT16 )
						rc = ma_variant_create_from_uint16((ma_uint16_t) val, value);
					else if(type == MA_VARTYPE_INT32 )
						rc = ma_variant_create_from_int32((ma_int32_t) val, value);
					else if(type == MA_VARTYPE_UINT32 )
						rc = ma_variant_create_from_uint32((ma_uint32_t) val, value);
					else if(type == MA_VARTYPE_INT64 )
						rc = ma_variant_create_from_int64((ma_int64_t) val, value);
					else if(type == MA_VARTYPE_UINT64 )
						rc = ma_variant_create_from_uint64((ma_uint64_t) val, value);
					else if(type == MA_VARTYPE_BOOL )
						rc = ma_variant_create_from_bool((ma_bool_t) val, value);
				}
				break;
			}
				
			case MA_VARTYPE_STRING: {   
				if(MA_OK == (rc = MA_DB_RECORDSET_GET_STRING((void *) self, column_index, &p))) {
					rc = ma_variant_create_from_string(p ? p:"", p?strlen(p):0, value);
				}
				break;
			}
			case MA_VARTYPE_RAW:{
				if(MA_OK == (rc = MA_DB_RECORDSET_GET_BLOB((void *)self, column_index, (const void **)&p, &size))) {
					rc = ma_variant_create_from_raw(p, size, value);
				}
				break;
			}
								
			case MA_VARTYPE_DOUBLE :
			case MA_VARTYPE_FLOAT :
			case MA_VARTYPE_TABLE :
			case MA_VARTYPE_ARRAY : 
			default:{
				if(MA_OK == (rc = MA_DB_RECORDSET_GET_BLOB((void *)self, column_index, (const void **)&p, &size))) {
					ma_deserializer_t *deserializer = NULL;
					if(MA_OK == (rc = ma_deserializer_create(&deserializer))) {
						if(MA_OK == (rc = ma_deserializer_put(deserializer, p, size))) {
							rc = ma_deserializer_get_next(deserializer,value);
						}
						(void)ma_deserializer_destroy(deserializer);
					}
				}
				break;
			}
		}
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_db_recordset_next(ma_db_recordset_t *self) {
    return self ? MA_DB_RECORDSET_NEXT(self) : MA_ERROR_INVALIDARG;
}

