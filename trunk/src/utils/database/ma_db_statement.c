#include "ma/internal/utils/database/ma_db_statement.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/serialization/ma_serialize.h"

#include "sqlite/ma_db_sqlite.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>


ma_error_t ma_db_statement_create(ma_db_t *db, const char *stmt , ma_db_statement_t **self) {
    return (db && stmt && self) ? MA_DB_STATEMENT_CREATE(db,stmt,(void **)self) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_db_statement_release(ma_db_statement_t *self) {
    return self ? MA_DB_STATEMENT_RELEASE(self) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_db_statement_set_int(ma_db_statement_t *self, int parameter_index, int value) {
    return (self && parameter_index > 0) ? MA_DB_STATEMENT_SET_INT(self, parameter_index, value) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_db_statement_set_long(ma_db_statement_t *self, int parameter_index, ma_int64_t value) {
    return (self && parameter_index > 0 ) ? MA_DB_STATEMENT_SET_LONG(self, parameter_index, value) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_db_statement_set_string(ma_db_statement_t *self, int parameter_index, unsigned short is_data_transient, const char *value, size_t size) {
    //return (self && parameter_index > 0 && value) ? MA_DB_STATEMENT_SET_STRING(self,parameter_index, is_data_transient, value,size) : MA_ERROR_INVALIDARG;
    return (self && parameter_index > 0 ) ? MA_DB_STATEMENT_SET_STRING(self,parameter_index, is_data_transient, value,size) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_db_statement_set_blob(ma_db_statement_t *self, int parameter_index, unsigned short is_data_transient, const void *value, size_t size) {
    return (self && parameter_index > 0 && value) ? MA_DB_STATEMENT_SET_BLOB(self,parameter_index, is_data_transient, value,size) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_db_statement_set_variant(ma_db_statement_t *self, int parameter_index, ma_variant_t *value) {
    if(self && parameter_index > 0 && value) {
        ma_error_t rc = MA_ERROR_DB_STATEMENT_SET_FAILED;
        ma_vartype_t var_type = MA_VARTYPE_NULL;
        size_t size = 0;
        const char *p = NULL;
		
        (void)ma_variant_get_type(value,&var_type);
        switch(var_type) {
			case MA_VARTYPE_INT8 :
			case MA_VARTYPE_INT16 :
			case MA_VARTYPE_INT32 :
			case MA_VARTYPE_INT64: 
				{				
					ma_int64_t int_value = 0;
					if(MA_OK == (rc = ma_variant_get_int64(value,&int_value))) 
						rc =  MA_DB_STATEMENT_SET_LONG(self,parameter_index,int_value);
				}
				break;
			case MA_VARTYPE_UINT8:			
			case MA_VARTYPE_UINT16:			
			case MA_VARTYPE_UINT32:			
			case MA_VARTYPE_UINT64:
				{				
					ma_uint64_t int_value = 0;
					if(MA_OK == (rc = ma_variant_get_uint64(value,&int_value))) 
						rc =  MA_DB_STATEMENT_SET_LONG(self,parameter_index, (ma_int64_t)int_value);
				}
				break;
			case MA_VARTYPE_BOOL: 
				{
					ma_bool_t i = 0;
					if(MA_OK == (rc = ma_variant_get_bool(value,&i))) 
						rc =  MA_DB_STATEMENT_SET_LONG(self,parameter_index, (ma_int64_t)i);					
				}
				break;			
			case MA_VARTYPE_STRING:{    
				ma_buffer_t *buffer = NULL;
                
                if(MA_OK == (rc = ma_variant_get_string_buffer(value,&buffer))) {
					(void)ma_buffer_get_string(buffer,&p, &size);
					rc = MA_DB_STATEMENT_SET_STRING(self,parameter_index,1,p,size); 
					(void)ma_buffer_release(buffer);
				}
				break;
			}
			case MA_VARTYPE_RAW:{
				ma_buffer_t *buffer = NULL;
                
				if(MA_OK == (rc = ma_variant_get_raw_buffer(value,&buffer))) {
					(void)ma_buffer_get_raw(buffer,(const unsigned char **)&p, &size);
					rc = MA_DB_STATEMENT_SET_BLOB((void*)self,parameter_index,1,p,size);
					(void)ma_buffer_release(buffer);
				}
				break;
			}
			case MA_VARTYPE_DOUBLE :
			case MA_VARTYPE_FLOAT :
			case MA_VARTYPE_TABLE :
			case MA_VARTYPE_ARRAY : {
				ma_serializer_t *serializer = NULL;
                
				if(MA_OK == (rc = ma_serializer_create(&serializer))) {
					if(MA_OK == (rc = ma_serializer_add_variant(serializer, value))) {
						if(MA_OK == (rc = ma_serializer_get(serializer, (char **)&p, &size))) {
							rc = MA_DB_STATEMENT_SET_BLOB(self,parameter_index,1,p,size);
						}
					}
					(void)ma_serializer_destroy(serializer);
				}
				break;
			}
			default:
				rc = MA_ERROR_INVALIDARG;
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_db_statement_execute(ma_db_statement_t *self) {
    return self ? MA_DB_STATEMENT_EXECUTE(self) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_db_statement_execute_query(ma_db_statement_t *self, ma_db_recordset_t **recordset) {
    return (self && recordset) ? MA_DB_STATEMENT_EXECUTE_QUERY(self,(void**)recordset) : MA_ERROR_INVALIDARG;
}
