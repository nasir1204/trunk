#include "ma/internal/utils/firewall_exceptions/ma_firewall_exceptions.h"
#include "ma/internal/defs/ma_general_defs.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include "ma/internal/utils/repository/ma_sitelist.h"

#include <stdio.h>

#ifdef MA_WINDOWS
#include "ma/datastore/ma_ds_registry.h"
#define MA_MACMNSVC_EXE_NAME		"macmnsvc.exe"
#define MA_MASVC_EXE_NAME			"masvc.exe"
#define MA_MACONFIG_EXE_NAME		"maconfig.exe"
#define MA_MAREPOMIRROR_EXE_NAME	"marepomirror.exe"
#define MA_MUE_EXE_NAME				"McScript_InUse.exe"
#else
#include "ma/datastore/ma_ds_ini.h"
#define MA_MACMNSVC_EXE_NAME		"macmnsvc"
#define MA_MASVC_EXE_NAME			"masvc"
#define MA_MACONFIG_EXE_NAME		"maconfig"
#define MA_MAREPOMIRROR_EXE_NAME	"marepomirror"
#define MA_MUE_EXE_NAME				"Mue_InUse"
#endif

static void add_constant_rule(ma_ds_t *ds, const char *rule_num, const char *rule, const char *exe_path, const char *program_name) {
	char name[16] = {0};
	char program_path[MA_MAX_PATH_LEN] = {0};
	ma_temp_buffer_t value = MA_TEMP_BUFFER_INIT;
	size_t value_len = 0;

	MA_MSC_SELECT(_snprintf, snprintf)(name, 15, MA_FIREWALL_RULE_CONSTANT_STR, rule_num);

	if(exe_path[strlen(exe_path)-1] != MA_PATH_SEPARATOR)
		MA_MSC_SELECT(_snprintf, snprintf)(program_path, MA_MAX_PATH_LEN - 1, "%s%c%s", exe_path, MA_PATH_SEPARATOR, program_name);
	else
		MA_MSC_SELECT(_snprintf, snprintf)(program_path, MA_MAX_PATH_LEN - 1, "%s%s", exe_path, program_name);

	value_len = strlen(rule) + strlen(program_path);

	if(value_len > ma_temp_buffer_capacity(&value)) {
		ma_temp_buffer_reserve(&value, value_len + 1);
	}

	MA_MSC_SELECT(_snprintf, snprintf)((char *)ma_temp_buffer_get(&value), value_len, rule, program_path);	
	ma_ds_set_str(ds, MA_FIREWALL_RULE_REGISTRY_STR, name, (char *)ma_temp_buffer_get(&value), value_len);
	ma_temp_buffer_uninit(&value);
}

ma_error_t ma_firewall_exceptions_add_constant_rules(ma_configurator_t *self) {
	ma_error_t rc = MA_OK;
	ma_ds_t *ds = NULL;
	const char *install_path = ma_configurator_get_agent_install_path(self);

#ifdef MA_WINDOWS
	if(MA_OK == (rc = ma_ds_registry_open(HKEY_LOCAL_MACHINE, MA_CONFIGURATION_REGISTRY_AGENT_FULL_PATH_STR,  MA_DS_REGISTRY_32BIT_VIEW, MA_FALSE, KEY_ALL_ACCESS, (ma_ds_registry_t **) &ds))) {	
		char exe_path[MA_MAX_PATH_LEN] = {0}, x86_exe_path[MA_MAX_PATH_LEN] = {0};

#ifdef _WIN64		
		MA_MSC_SELECT(_snprintf, snprintf)(exe_path, MA_MAX_PATH_LEN-1, "%s", install_path);
		if(install_path[strlen(install_path)-1] != MA_PATH_SEPARATOR)
			MA_MSC_SELECT(_snprintf, snprintf)(x86_exe_path, MA_MAX_PATH_LEN-1, "%s%cx86", install_path, MA_PATH_SEPARATOR);
		else
			MA_MSC_SELECT(_snprintf, snprintf)(x86_exe_path, MA_MAX_PATH_LEN-1, "%sx86", install_path);
#else
		MA_MSC_SELECT(_snprintf, snprintf)(exe_path, MA_MAX_PATH_LEN-1, "%s", install_path);
		MA_MSC_SELECT(_snprintf, snprintf)(x86_exe_path, MA_MAX_PATH_LEN-1, "%s", install_path);
#endif

#else  /* UNIX */
	const char *data_path = ma_configurator_get_agent_data_path(self);
	char ini_file[MA_MAX_PATH_LEN] = {0};
	MA_MSC_SELECT(_snprintf, snprintf)(ini_file, MA_MAX_PATH_LEN-1, "%s%c%s", data_path, MA_PATH_SEPARATOR, MA_FIREWALL_RULE_INI_STR);

	if(MA_OK == (rc = ma_ds_ini_open(ini_file, 1, (ma_ds_ini_t **)&ds))) {
		char exe_path[MA_MAX_PATH_LEN] = {0}, x86_exe_path[MA_MAX_PATH_LEN] = {0};
		MA_MSC_SELECT(_snprintf, snprintf)(exe_path, MA_MAX_PATH_LEN-1, "%s%c%s", install_path, MA_PATH_SEPARATOR, "bin");
		MA_MSC_SELECT(_snprintf, snprintf)(x86_exe_path, MA_MAX_PATH_LEN-1, "%s%c%s", install_path, MA_PATH_SEPARATOR, "bin");
#endif


		(void) add_constant_rule(ds, "1", MA_FIREWALL_RULE_MACMNSVC_TCP_OUT_ALLOW_ALL_STR, exe_path, MA_MACMNSVC_EXE_NAME);
		(void) add_constant_rule(ds, "2", MA_FIREWALL_RULE_MACMNSVC_UDP_OUT_ALLOW_ALL_STR, exe_path, MA_MACMNSVC_EXE_NAME);
		(void) add_constant_rule(ds, "5", MA_FIREWALL_RULE_MACMNSVC_UDP_COMPAT_IN_ALLOW_ALL_STR, exe_path, MA_MACMNSVC_EXE_NAME);
		(void) add_constant_rule(ds, "6", MA_FIREWALL_RULE_MASVC_TCP_OUT_ALLOW_ALL_STR, exe_path, MA_MASVC_EXE_NAME);
		(void) add_constant_rule(ds, "7", MA_FIREWALL_RULE_MASVC_UDP_OUT_ALLOW_ALL_STR, exe_path, MA_MASVC_EXE_NAME);
		(void) add_constant_rule(ds, "8", MA_FIREWALL_RULE_MACONFIG_TCP_OUT_ALLOW_ALL_STR, exe_path, MA_MACONFIG_EXE_NAME);
		(void) add_constant_rule(ds, "9", MA_FIREWALL_RULE_MUE_TCP_OUT_ALLOW_ALL_STR, x86_exe_path, MA_MUE_EXE_NAME);
		(void) add_constant_rule(ds, "10", MA_FIREWALL_RULE_MUE_UDP_OUT_ALLOW_ALL_STR, x86_exe_path, MA_MUE_EXE_NAME);
		(void) add_constant_rule(ds, "11", MA_FIREWALL_RULE_MAREPOMIRROR_TCP_OUT_ALLOW_ALL_STR, x86_exe_path, MA_MAREPOMIRROR_EXE_NAME);

		(void) ma_ds_release(ds);
	}

	return rc;
}

static void update_rule_identifier(ma_configurator_t *self) {
	ma_ds_t *ds = NULL;
	ma_error_t rc = MA_OK;

#ifdef MA_WINDOWS
	char identifier_reg_path[MA_MAX_PATH_LEN] = {0};
	MA_MSC_SELECT(_snprintf, snprintf)(identifier_reg_path, MA_MAX_PATH_LEN - 1, "%s%s%s", MA_CONFIGURATION_REGISTRY_AGENT_FULL_PATH_STR, MA_FIREWALL_RULE_REGISTRY_STR, MA_PATH_SEPARATOR_STR);			

	if(MA_OK == (rc = ma_ds_registry_open(HKEY_LOCAL_MACHINE, identifier_reg_path,  MA_DS_REGISTRY_32BIT_VIEW, MA_TRUE, KEY_ALL_ACCESS, (ma_ds_registry_t **) &ds))) {
#else
	const char *data_path = ma_configurator_get_agent_data_path(self);
	char ini_file[MA_MAX_PATH_LEN] = {0};
	MA_MSC_SELECT(_snprintf, snprintf)(ini_file, MA_MAX_PATH_LEN-1, "%s%c%s", data_path, MA_PATH_SEPARATOR, MA_FIREWALL_RULE_INI_STR);

	if(MA_OK == (rc = ma_ds_ini_open(ini_file, 1, (ma_ds_ini_t **)&ds))) {
#endif
		char uuid[UUID_LEN_TXT] = {0};	
		(void) ma_uuid_generate(0, NULL, uuid);	
		ma_ds_set_str(ds, MA_FIREWALL_RULE_IDENTIFIER_STR, MA_FIREWALL_RULE_GUID_STR, uuid, -1);
		(void) ma_ds_release(ds);
	}
}

ma_error_t ma_firewall_exceptions_add_dynamic_rules(ma_configurator_t *self, const char *tcp_port, const char *udp_port) {
	if(self && tcp_port && udp_port) {
		ma_error_t rc = MA_OK;
		ma_ds_t *ds = NULL;
		ma_bool_t is_rule_changed = MA_TRUE, need_uuid_update = MA_FALSE;
		const char *install_path = ma_configurator_get_agent_install_path(self);
		ma_temp_buffer_t rule = MA_TEMP_BUFFER_INIT;

		char program_path[MA_MAX_PATH_LEN] = {0};
		size_t value_len = 0;
		ma_buffer_t *buffer =  NULL;
		char exe_path[MA_MAX_PATH_LEN] = {0};
		
#ifdef MA_WINDOWS
		if(MA_OK == (rc = ma_ds_registry_open(HKEY_LOCAL_MACHINE, MA_CONFIGURATION_REGISTRY_AGENT_FULL_PATH_STR,  MA_DS_REGISTRY_32BIT_VIEW, MA_FALSE, KEY_ALL_ACCESS, (ma_ds_registry_t **) &ds))) {

			MA_MSC_SELECT(_snprintf, snprintf)(exe_path, MA_MAX_PATH_LEN-1, "%s", install_path);
#else /* UNIX */
		const char *data_path = ma_configurator_get_agent_data_path(self);
		char ini_file[MA_MAX_PATH_LEN] = {0};	

		MA_MSC_SELECT(_snprintf, snprintf)(ini_file, MA_MAX_PATH_LEN-1, "%s%c%s", data_path, MA_PATH_SEPARATOR, MA_FIREWALL_RULE_INI_STR);

		MA_MSC_SELECT(_snprintf, snprintf)(exe_path, MA_MAX_PATH_LEN-1, "%s%c%s", install_path, MA_PATH_SEPARATOR, "bin");

		if(MA_OK == (rc = ma_ds_ini_open(ini_file, 1, (ma_ds_ini_t **)&ds))) {
#endif
			if(install_path[strlen(install_path)-1] != MA_PATH_SEPARATOR)
				MA_MSC_SELECT(_snprintf, snprintf)(program_path, MA_MAX_PATH_LEN - 1, "%s%c%s", exe_path, MA_PATH_SEPARATOR, MA_MACMNSVC_EXE_NAME);			
			else
				MA_MSC_SELECT(_snprintf, snprintf)(program_path, MA_MAX_PATH_LEN - 1, "%s%s", exe_path, MA_MACMNSVC_EXE_NAME);			

			/* Rule - 3 MA_FIREWALL_RULE_MACMNSVC_TCP_IN_ALLOW_SERVER */
			value_len = MA_COUNTOF(MA_FIREWALL_RULE_MACMNSVC_TCP_IN_ALLOW_SERVER) + strlen(program_path) + strlen(tcp_port);

			if(value_len > ma_temp_buffer_capacity(&rule)) {
				ma_temp_buffer_reserve(&rule, value_len + 1);
			}
			MA_MSC_SELECT(_snprintf, snprintf)((char *)ma_temp_buffer_get(&rule), value_len, MA_FIREWALL_RULE_MACMNSVC_TCP_IN_ALLOW_SERVER, program_path, tcp_port);	
			
			if(MA_OK == ma_ds_get_str(ds, MA_FIREWALL_RULE_REGISTRY_STR, "Rule_3", &buffer)) {
				const char *str = NULL;
				size_t size = 0;
				if(MA_OK == ma_buffer_get_string(buffer, &str, &size)) {
					is_rule_changed = strncmp((char *)ma_temp_buffer_get(&rule), str, size);
				}
				(void) ma_buffer_release(buffer), buffer = NULL;
			}
			
			if(is_rule_changed) {
				ma_ds_set_str(ds, MA_FIREWALL_RULE_REGISTRY_STR, "Rule_3", (char *)ma_temp_buffer_get(&rule), value_len);
				need_uuid_update = MA_TRUE;
			}
			ma_temp_buffer_uninit(&rule);

			/* Rule - 4 MA_FIREWALL_RULE_MACMNSVC_UDP_IN_ALLOW_ALL_STR	*/
			ma_temp_buffer_init(&rule);
			is_rule_changed = MA_TRUE;
			value_len = MA_COUNTOF(MA_FIREWALL_RULE_MACMNSVC_UDP_IN_ALLOW_ALL_STR) + strlen(program_path) + strlen(udp_port);

			if(value_len > ma_temp_buffer_capacity(&rule)) {
				ma_temp_buffer_reserve(&rule, value_len + 1);
			}
			MA_MSC_SELECT(_snprintf, snprintf)((char *)ma_temp_buffer_get(&rule), value_len, MA_FIREWALL_RULE_MACMNSVC_UDP_IN_ALLOW_ALL_STR, program_path, udp_port);	
			
			if(MA_OK == ma_ds_get_str(ds, MA_FIREWALL_RULE_REGISTRY_STR, "Rule_4", &buffer)) {
				const char *str = NULL;
				size_t size = 0;
				if(MA_OK == ma_buffer_get_string(buffer, &str, &size)) {
					is_rule_changed = strncmp((char *)ma_temp_buffer_get(&rule), str, size);
				}
				(void) ma_buffer_release(buffer), buffer = NULL;
			}
			
			if(is_rule_changed) {
				ma_ds_set_str(ds, MA_FIREWALL_RULE_REGISTRY_STR, "Rule_4", (char *)ma_temp_buffer_get(&rule), value_len);
				need_uuid_update = MA_TRUE;
			}
			
			(void) ma_ds_release(ds);

			if(need_uuid_update) {
				(void) update_rule_identifier(self);				
			}
		}

		ma_temp_buffer_uninit(&rule);
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

/* TBD */
ma_error_t ma_firewall_exceptions_remove_constant_rules(ma_configurator_t *self) {
	ma_error_t rc = MA_OK;

	return MA_OK;
}

ma_error_t ma_firewall_exceptions_remove_dynamic_rules(ma_configurator_t *self) {
	ma_error_t rc = MA_OK;

	return MA_OK;
}
