#include "ma/internal/utils/macrypto/ma_crypto.h"
#include "ma/internal/utils/datastructures/ma_bytebuffer.h"
#include "ma_crypto_utils.h"
#include "mfecryptc_core.h"
#include "mfecryptc_defines.h"

#include <stdlib.h>

extern ma_logger_t *g_crypto_logger;

/* SHA1 length is 20 bytes and SHA256 length is 32 bytes */
#define HASH_ALGORITHM(x) ((x == MA_CRYPTO_HASH_SHA1) ? MC_DIGEST_ID_SHA1 : MC_DIGEST_ID_SHA256 )
#define HASH_LEN(x) ((HASH_ALGORITHM(x) == MC_DIGEST_ID_SHA1) ? 20 : 32)

struct ma_crypto_hash_s {
    M_DIGEST                *hash_handle;
    unsigned int                  hash_len;
    ma_crypto_hash_type_t   hash_type;
};

ma_error_t ma_crypto_hash_create(ma_crypto_t *crypto, ma_crypto_hash_type_t type, ma_crypto_hash_t **hash) {
    if(crypto && hash && type <= MA_CRYPTO_HASH_DEFAULT) {
        ma_crypto_hash_t *self = (ma_crypto_hash_t *)calloc(1, sizeof(ma_crypto_hash_t));
        if(self) {
            M_LIB_CTX *lib_context = ma_crypto_get_crypto_library_context(crypto);                 
            if(M_E_NO_ERROR == mc_DIGEST_new(lib_context, HASH_ALGORITHM(type), &self->hash_handle)) {
                if(M_E_NO_ERROR == mc_DIGEST_get_info(self->hash_handle, MC_DIGEST_INFO_DIGEST_LEN, &self->hash_len)) {
                    self->hash_type = type;
                    *hash = self;
                    return MA_OK;
                }
            }
            free(self);
            return MA_ERROR_CRYPTO_INTERNAL;
        }
        return MA_ERROR_OUTOFMEMORY;
    }
    MA_LOG(g_crypto_logger, MA_LOG_SEV_ERROR,"Invalid Argument");
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_crypto_hash_data_update(ma_crypto_hash_t *self, const unsigned char *data, unsigned int data_size) {
    if(self && data && data_size > 0) {
        return (M_E_NO_ERROR == mc_DIGEST_data_update(self->hash_handle, (unsigned char *)data, data_size)) ? MA_OK : MA_ERROR_CRYPTO_INTERNAL;
    }
    MA_LOG(g_crypto_logger, MA_LOG_SEV_ERROR,"Invalid Argument");
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_crypto_hash_data_final(ma_crypto_hash_t *self, ma_bytebuffer_t *hash_buffer) {
    if(self && hash_buffer && ((unsigned int)-1 != ma_bytebuffer_get_capacity(hash_buffer))) {
        if(self->hash_len > ma_bytebuffer_get_capacity(hash_buffer))
            ma_bytebuffer_reserve(hash_buffer, self->hash_len);
        if(M_E_NO_ERROR == mc_DIGEST_data_final(self->hash_handle, ma_bytebuffer_get_bytes(hash_buffer), &self->hash_len)) {
            ma_bytebuffer_shrink(hash_buffer, self->hash_len);
            return MA_OK;
        }
        return MA_ERROR_CRYPTO_INTERNAL;
    }
    MA_LOG(g_crypto_logger, MA_LOG_SEV_ERROR,"Invalid Argument");
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_crypto_hash_buffer(ma_crypto_hash_t *self, const unsigned char *data, unsigned int data_size, ma_bytebuffer_t *hash_buffer) {
    if(self && data && data_size > 0 && hash_buffer && ((unsigned int)-1 != ma_bytebuffer_get_capacity(hash_buffer))) {        
        if(self->hash_len > ma_bytebuffer_get_capacity(hash_buffer))
            ma_bytebuffer_reserve(hash_buffer, self->hash_len);        
        if(M_E_NO_ERROR == mc_DIGEST_buffer(self->hash_handle, data, data_size, ma_bytebuffer_get_bytes(hash_buffer), &self->hash_len)) {
            ma_bytebuffer_shrink(hash_buffer, self->hash_len);
            return MA_OK;
        }
        return MA_ERROR_CRYPTO_INTERNAL;
    }
    MA_LOG(g_crypto_logger, MA_LOG_SEV_ERROR,"Invalid Argument");
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_crypto_hash_and_encode(ma_crypto_hash_t *self, const unsigned char *data, unsigned int data_size, ma_bytebuffer_t *hash_buffer) {
    if(self && data && 0 < data_size && hash_buffer && ((unsigned int)-1 != ma_bytebuffer_get_capacity(hash_buffer))) {    
        ma_error_t rc = MA_ERROR_CRYPTO_INTERNAL;
        ma_bytebuffer_t *bytebuff = NULL;

        if(MA_OK == (rc = ma_bytebuffer_create(HASH_LEN(self->hash_type), &bytebuff))) { 
            if(MA_OK == (rc = ma_crypto_hash_buffer(self, data, data_size, bytebuff))) {
                unsigned char *encoded_buf = NULL;
                unsigned int encoded_buf_len = 2 * ma_bytebuffer_get_size(bytebuff);
                if(NULL != (encoded_buf = (unsigned char *)calloc(encoded_buf_len, sizeof(unsigned char)))) {
                    unsigned int encoded_bytes = 0;
                    int state = 0;
                    unsigned int bytes_processed = 0;
                    encoded_bytes = ma_base64_encode(ma_bytebuffer_get_bytes(bytebuff), ma_bytebuffer_get_size(bytebuff), (char *)encoded_buf, 
                                                     encoded_buf_len, &state, &bytes_processed);
                    if(encoded_bytes <= encoded_buf_len){
                        if(encoded_bytes > ma_bytebuffer_get_capacity(hash_buffer))
                            ma_bytebuffer_reserve(hash_buffer, encoded_bytes);
                        rc = ma_bytebuffer_set_bytes(hash_buffer, 0, encoded_buf, encoded_bytes);
                    }
                    else {
                        MA_LOG(g_crypto_logger, MA_LOG_SEV_ERROR,"base64 encode return value more than provided buffer length");
                        rc = MA_ERROR_CRYPTO_INTERNAL; 
                    }
                    free(encoded_buf);
                }
                else
                    rc = MA_ERROR_OUTOFMEMORY;
            }
            ma_bytebuffer_release(bytebuff);
        }        
        MA_LOG(g_crypto_logger, MA_LOG_SEV_DEBUG,"ma_macrypto_hash_and_encode returns <%d>", rc);
        return rc;
    }
    MA_LOG(g_crypto_logger, MA_LOG_SEV_ERROR,"Invalid Argument");
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_crypto_hash_release(ma_crypto_hash_t *self) {
    if(self) {
        if(self->hash_handle) mc_DIGEST_free(self->hash_handle);
        free(self);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


