#include "ma/internal/utils/macrypto/ma_crypto.h"
#include "ma_crypto_utils.h"
#include "ma/internal/utils/text/ma_utf8.h"

#include "mfecryptc_core.h"
#include "mfecryptc_defines.h"

#include "ma/internal/utils/platform/ma_sys_apis.h"

#include <string.h>
#include <stdlib.h>

extern ma_logger_t *g_crypto_logger;

#define MAX_ENCR_STRING_SIZE    40

#define ENCRYPT_HEADER_MAGIC_STR(algo_id)           ((MC_ALGID_ASYM_RSA_PKCS1 == algo_id) ? "EPORSAPKCS1:" : ((MC_ALGID_SYMMETRIC_AES_128_ECB == algo_id) ? "EPOAES128:" : "EPO\0"))
#define ENCRYPT_HEADER_MAGIC_STR_SIZE(algo_id)      ((MC_ALGID_ASYM_RSA_PKCS1 == algo_id) ? strlen("EPORSAPKCS1:") : ((MC_ALGID_SYMMETRIC_AES_128_ECB == algo_id) ? strlen("EPOAES128:") : strlen("EPO ")))

/* NOTE: The following macros and arrays must be updated if we add a new type for encryption or decryption in ma_crypto.h */
#define ENCRYPT_TYPE_MAX    2
#define DECRYPT_TYPE_MAX    5
const int encrypt_algorithm_id[ENCRYPT_TYPE_MAX] = {  MC_ALGID_SYMMETRIC_3DES, MC_ALGID_SYMMETRIC_AES_128_ECB };
const int decrypt_algorithm_id[DECRYPT_TYPE_MAX] = {  MC_ALGID_SYMMETRIC_3DES, MC_ALGID_SYMMETRIC_AES_128_ECB, MC_ALGID_SYMMETRIC_AES_128_ECB,MC_ALGID_SYMMETRIC_3DES,  MC_ALGID_ASYM_RSA_PKCS1 };

struct ma_crypto_encryption_s {
    int                                 type;               /* type of encryption/decryption received from caller */
    int                                 algorithm_id;       /* Algorithm id based ont the enc/dec type */
    ma_bool_t                           is_symmetric_key;   /* flag to decide whether it is symetric or asymetric encryption */
    M_CIPHER                            *enc_crypto_handle; /* it is encrypt handle */
    unsigned int                        block_size;         /* size of block */
    ma_crypto_mode_t                    fips_mode;          /* Fips Mode */
};

ma_error_t ma_crypto_encrypt_create(ma_crypto_t *macrypto, ma_crypto_encryption_type_t type, ma_crypto_encryption_t **encrypt) {
    if(macrypto && encrypt && type < ENCRYPT_TYPE_MAX) {
        ma_crypto_encryption_t *self = (ma_crypto_encryption_t *)calloc(1, sizeof(ma_crypto_encryption_t));
        if(self) {
            M_LIB_CTX *lib_context = NULL;
            M_RKEY *crypto_key_handle = NULL;

            self->type = type;
            self->algorithm_id = encrypt_algorithm_id[type];

            MA_LOG(g_crypto_logger, MA_LOG_SEV_DEBUG,"algorithim id <%d> for encrypt type <%d>", self->algorithm_id, self->type);

            /* get fips mode */
		    if(-1 == (self->fips_mode = ma_crypto_get_fips_mode(macrypto))) {
                free(self);
                return MA_ERROR_CRYPTO_INTERNAL;
            }

            MA_LOG(g_crypto_logger, MA_LOG_SEV_DEBUG,"fips mode <%d>", self->fips_mode);

            self->is_symmetric_key = MA_TRUE;  /* by default it is true, change this flag if the algorithm id is asymetric */

            /* gets key handle based on algorithim id and encryption key type */
            if(MC_ALGID_SYMMETRIC_AES_128_ECB == self->algorithm_id)
                crypto_key_handle = ma_crypto_get_rkey(macrypto, FIPS_SYMMETRIC_KEY);
            else  // MC_ALGID_SYMMETRIC_3DES
		        crypto_key_handle = ma_crypto_get_rkey(macrypto, NON_FIPS_SYMMETRIC_KEY);
   
            lib_context = ma_crypto_get_crypto_library_context(macrypto);

             /* gets encrypt handle */
            if(crypto_key_handle && (M_E_NO_ERROR == mc_CIPHER_new(lib_context, crypto_key_handle, self->algorithm_id, NULL, 0, MC_CIPHER_TYPE_ENCRYPT, &self->enc_crypto_handle))) {
		        /* Block size is required only for symetric key oprations */		
		        if(self->is_symmetric_key && (M_E_NO_ERROR != mc_CIPHER_get_info(self->enc_crypto_handle, MC_CIPHER_INFO_BLOCK_SIZE, (void*)&self->block_size))) {
                    mc_CIPHER_free(self->enc_crypto_handle);
			        /* Failed to get the block size */
			        free(self);
			        return MA_ERROR_CRYPTO_INTERNAL;
		        }
                *encrypt = self;
		        return MA_OK;
	        }

            free(self);
            return MA_ERROR_CRYPTO_INTERNAL;
        }
        return MA_ERROR_OUTOFMEMORY;
    }
    MA_LOG(g_crypto_logger, MA_LOG_SEV_ERROR,"Invalid Argument");
    return MA_ERROR_INVALIDARG;
}


static unsigned int get_magic_header_size(int algo_id) {
    return (ENCRYPT_HEADER_MAGIC_STR_SIZE(algo_id) + sizeof(int) + sizeof(ma_uint32_t));
}

static ma_bool_t is_data_encrypted(ma_crypto_encryption_t *self, const unsigned char *in_data, unsigned int in_data_size, ma_bytebuffer_t *encrypted_data) {
    if(0 == memcmp(in_data, ENCRYPT_HEADER_MAGIC_STR(self->algorithm_id), ENCRYPT_HEADER_MAGIC_STR_SIZE(self->algorithm_id))) {
	    if(in_data_size > ma_bytebuffer_get_capacity(encrypted_data))
		    ma_bytebuffer_reserve(encrypted_data, in_data_size); 
	    ma_bytebuffer_set_bytes(encrypted_data, 0, in_data, in_data_size); /* Copying same input data into out data buffer */
	    return MA_TRUE;  /* To be reviewed - returns OK or failure ??? */
    }
    return MA_FALSE;
}

static ma_error_t set_magic_header(ma_crypto_encryption_t *self, unsigned int data_size, ma_bytebuffer_t *encrypted_data) {
    ma_error_t rc = MA_OK;
    unsigned int header_size = get_magic_header_size(self->algorithm_id);
    int cipher_algorithm = 0;
    unsigned int size = 0;
    unsigned char *header_buffer = (unsigned char *)calloc(header_size, sizeof(unsigned char));    
    if(header_buffer) {
        memcpy(header_buffer, ENCRYPT_HEADER_MAGIC_STR(self->algorithm_id), ENCRYPT_HEADER_MAGIC_STR_SIZE(self->algorithm_id));        
        
		//cipher_algorithm = ma_little_to_big_endian_int(self->algorithm_id);
		cipher_algorithm = (self->algorithm_id == MC_ALGID_SYMMETRIC_3DES) ?2:3; /*3DES encrypt/decrypt we are changing the algo id as per the ePO algorithm id.*/
        cipher_algorithm = ma_little_to_big_endian_int(cipher_algorithm);
		
		size = ma_little_to_big_endian_int(data_size);
        memcpy(header_buffer+ENCRYPT_HEADER_MAGIC_STR_SIZE(self->algorithm_id), &cipher_algorithm, sizeof(int));
        memcpy(header_buffer+ENCRYPT_HEADER_MAGIC_STR_SIZE(self->algorithm_id)+sizeof(int), &size, sizeof(unsigned int));
        rc = ma_bytebuffer_set_bytes(encrypted_data, 0, header_buffer, header_size);
        free(header_buffer);
        return rc;
    }
    return MA_ERROR_OUTOFMEMORY;
}

static ma_bool_t add_data_security_before_encrypt(const unsigned char *data, unsigned int data_size, unsigned char *xord_data, unsigned int xord_data_size) {
	unsigned long i, j, offset;

	if(data_size > xord_data_size)
		return MA_FALSE; 

	for(i = 0, offset = 0; i < data_size; i += 8) {
		unsigned char ch;
		if(data_size - i >= 8)
			memcpy((xord_data + offset), (data + i), 8);
		else {
			//we don't have 8 bytes left, take care of padding
			memset(xord_data + offset, '\0', 8);
			memcpy((xord_data + offset), (data + i), (data_size - i));
		}
		// XOR the data first for extra security
		ch = 'T';
		for(j = 0; j < 8; j++) {
			*(xord_data + offset + j) = ch ^ *(xord_data + offset + j);
			ch = *(xord_data + offset + j);
		}
		offset += 8;
	}
	return MA_TRUE;
}


static ma_error_t encrypt_inner(ma_crypto_encryption_t *self, unsigned char *data, unsigned int data_size, ma_bytebuffer_t *encrypted_data, ma_bool_t skip_headers) {
	ma_error_t rc = MA_OK;
    unsigned char *pout_data = (skip_headers) ? ma_bytebuffer_get_bytes(encrypted_data) : ma_bytebuffer_get_bytes(encrypted_data) + get_magic_header_size(self->algorithm_id);
	unsigned int out_data_size = (skip_headers) ? ma_bytebuffer_get_capacity(encrypted_data) : ma_bytebuffer_get_capacity(encrypted_data) - get_magic_header_size(self->algorithm_id);

    if(self->is_symmetric_key) {
		/* Encrypt data */
		if(M_E_NO_ERROR == mc_CIPHER_encrypt_data_update(self->enc_crypto_handle, data, data_size, pout_data, &out_data_size)) {            
            /* Take care of last block encryption (if any)*/ 
			unsigned int last_block_size = self->block_size;
			unsigned char *plast_block_data = (unsigned char *)calloc(last_block_size, sizeof(unsigned char)); 
            if(plast_block_data) {
			    if(M_E_NO_ERROR == mc_CIPHER_encrypt_data_final(self->enc_crypto_handle, plast_block_data, &last_block_size)) {                               
                    if(last_block_size)
				        memcpy((pout_data + out_data_size), plast_block_data, last_block_size);
                    
                    out_data_size += (skip_headers) ? last_block_size : last_block_size + get_magic_header_size(self->algorithm_id) ;
                    // appending the data to the bytebuffer but not able to update the length, so using shrink API for this purpose.
                    rc = ma_bytebuffer_shrink(encrypted_data, out_data_size); 
			    }
			    else {
                    MA_LOG(g_crypto_logger, MA_LOG_SEV_ERROR,"mc_CIPHER_encrypt_data_final Failed");
				    rc = MA_ERROR_CRYPTO_INTERNAL;
                }
                free(plast_block_data);
            }
            else
                rc = MA_ERROR_OUTOFMEMORY;
		}
		else {
            MA_LOG(g_crypto_logger, MA_LOG_SEV_ERROR,"mc_CIPHER_encrypt_data_update Failed");
			rc = MA_ERROR_CRYPTO_INTERNAL;
        }
	}
	else /* MC_ALGID_ASYM_RSA_PKCS1 */ /* currently we are not supporting encryption in asymetric types, So the below code will be tested only when we add the asym type */
	{
		if(M_E_NO_ERROR == mc_CIPHER_encrypt_buffer(self->enc_crypto_handle, data, data_size, pout_data, &out_data_size)) {
            out_data_size = (skip_headers) ? out_data_size : out_data_size + get_magic_header_size(self->algorithm_id) ;
            rc = ma_bytebuffer_shrink(encrypted_data, out_data_size); 
        }
        else {
            MA_LOG(g_crypto_logger, MA_LOG_SEV_ERROR,"mc_CIPHER_encrypt_buffer Failed");
			rc =  MA_ERROR_CRYPTO_INTERNAL;
        }
	}
	return rc;
}

ma_error_t ma_crypto_encrypt(ma_crypto_encryption_t *self, const unsigned char *data, unsigned int data_size, ma_bytebuffer_t *encrypted_data)  {
    if(self && data_size > 0 && data && encrypted_data) {
        ma_error_t rc = MA_OK;       
        unsigned int input_data_size = 0;
        unsigned int out_data_size = 0;
        unsigned int padding_data_size = 0;                

        /* Check if data is already encrypted */
        if(is_data_encrypted(self, data, data_size, encrypted_data)) {
            MA_LOG(g_crypto_logger, MA_LOG_SEV_ERROR,"Input data is already encrypted");
            return MA_ERROR_CRYPTO_INTERNAL;     /* TBD -- what is the return value here,  OK or FAIL ??? */
        }

        /* This is only for symmetric encryption */
        if(self->is_symmetric_key)
            padding_data_size =  ((data_size % self->block_size) > 0) ? (self->block_size - (data_size % self->block_size)) : 0;                

        /* calculating the total out data -> magic header size + data size + padding size (if required) */
        out_data_size = get_magic_header_size(self->algorithm_id) + data_size + padding_data_size;

        /* reserve the out buffer if the input buffer size is less than out data size */
        if(out_data_size > ma_bytebuffer_get_capacity(encrypted_data))
            ma_bytebuffer_reserve(encrypted_data, out_data_size);

        input_data_size = data_size + padding_data_size;
        if(MA_OK == (rc = set_magic_header(self, data_size, encrypted_data))) {                
            /* XOR data(except magic header) for additional security. -- It is only for 3DES */
			if(MC_ALGID_SYMMETRIC_3DES == self->algorithm_id) {                    
				unsigned char *xord_input = (unsigned char *)calloc(input_data_size, sizeof(unsigned char));
				if(xord_input) {                        
                    MA_LOG(g_crypto_logger, MA_LOG_SEV_DEBUG,"XOR operations before encrypting the input data");
					if(add_data_security_before_encrypt(data, data_size, xord_input, input_data_size)) {
                        /* By default padding is enabled, so disable it for 3des type*/ 
                        mc_CIPHER_disable_padding_for_symmetric_cipher(self->enc_crypto_handle);
						rc = encrypt_inner(self, xord_input, input_data_size, encrypted_data, MA_FALSE);   /* encrypt data */
                    }
					else
						rc = MA_ERROR_CRYPTO_INTERNAL;
					free(xord_input); 
				}
				else
					rc = MA_ERROR_OUTOFMEMORY;
			}
			else {           
                if(self->is_symmetric_key && !padding_data_size) 
                    mc_CIPHER_disable_padding_for_symmetric_cipher(self->enc_crypto_handle);
				rc = encrypt_inner(self, (unsigned char *)data, data_size, encrypted_data, MA_FALSE);   /* encrypt data */
            }
        }
        MA_LOG(g_crypto_logger, MA_LOG_SEV_DEBUG,"ma_macrypto_encrypt returns <%d>", rc);
        return rc;
    }
    MA_LOG(g_crypto_logger, MA_LOG_SEV_ERROR,"Invalid Argument");
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_crypto_encrypt_and_encode(ma_crypto_encryption_t *self, const unsigned char *data, unsigned int data_size, ma_bytebuffer_t *encoded_data) {
    if(self && data_size >= 0 && data && encoded_data) {
        ma_error_t rc = MA_OK;
        ma_bytebuffer_t *encrypted_data = NULL;
		ma_bytebuffer_t *aligned_data = NULL;
        unsigned int out_data_size = 0;

        unsigned int input_size = (unsigned int)(data_size ? data_size : MAX_ENCR_STRING_SIZE);
        unsigned int padding_data_size = (((input_size % self->block_size) > 0) ? (self->block_size - (input_size % self->block_size)) : 0);
		out_data_size = input_size + padding_data_size;
		/* NOTE: making input data alligned with 8 byte blocks with padding 0 explicitly */
		if(MA_OK != (rc = ma_bytebuffer_create(out_data_size, &aligned_data))) {
			 MA_LOG(g_crypto_logger, MA_LOG_SEV_ERROR,"ma_macrypto_encrypt buffer creation failed");
			 return rc;
		}
		if(MA_OK != (rc = ma_bytebuffer_set_bytes(aligned_data, 0, data, data_size))){
			MA_LOG(g_crypto_logger, MA_LOG_SEV_ERROR,"ma_macrypto_encrypt buffer setting Failed");
			ma_bytebuffer_release(aligned_data);
			return rc;
		}

        ma_bytebuffer_shrink(aligned_data, out_data_size);

        //if(!padding_data_size)
            mc_CIPHER_disable_padding_for_symmetric_cipher(self->enc_crypto_handle);                    
            
        if(MA_OK == (rc = ma_bytebuffer_create(out_data_size, &encrypted_data))) {
            if(MA_OK == (rc = encrypt_inner(self, ma_bytebuffer_get_bytes(aligned_data),  ma_bytebuffer_get_size(aligned_data), encrypted_data, MA_TRUE)))
                rc = ma_crypto_encode(ma_bytebuffer_get_bytes(encrypted_data), ma_bytebuffer_get_size(encrypted_data), encoded_data, MA_FALSE);
            else
                MA_LOG(g_crypto_logger, MA_LOG_SEV_ERROR,"ma_macrypto_encrypt Failed");
            ma_bytebuffer_release(encrypted_data);
        }

		ma_bytebuffer_release(aligned_data);
        MA_LOG(g_crypto_logger, MA_LOG_SEV_DEBUG,"ma_macrypto_encrypt_and_encode returns <%d>", rc);        
        return rc;
    }
    MA_LOG(g_crypto_logger, MA_LOG_SEV_ERROR,"Invalid Argument");
    return MA_ERROR_INVALIDARG;    
}

ma_error_t ma_crypto_encrypt_release(ma_crypto_encryption_t *self) {
    if(self) {
	    if(self->enc_crypto_handle) mc_CIPHER_free(self->enc_crypto_handle);
        free(self);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


struct ma_crypto_decryption_s{
    int                                 type;               /* type of encryption/decryption received from caller */
    int                                 algorithm_id;       /* Algorithm id based ont the enc/dec type */
    ma_bool_t                           is_symmetric_key;   /* flag to decide whether it is symetric or asymetric encryption */
    M_CIPHER                            *dec_crypto_handle; /* it is decrypt handle */
    unsigned int                        block_size;         /* size of block */
    ma_crypto_mode_t                    fips_mode;          /* Fips Mode */
    ma_bool_t                           skip_conversion;
};

ma_error_t ma_crypto_decrypt_create(ma_crypto_t *macrypto, ma_crypto_decryption_type_t type, ma_crypto_decryption_t **decrypt) {
    if(macrypto && decrypt && type < DECRYPT_TYPE_MAX) {
        ma_crypto_decryption_t *self = (ma_crypto_decryption_t *)calloc(1, sizeof(ma_crypto_decryption_t));
        if(self) {
            M_LIB_CTX *lib_context = NULL;
            M_RKEY *crypto_key_handle = NULL;
  
            /* get fips mode */
		    if(-1 == (self->fips_mode = ma_crypto_get_fips_mode(macrypto))) {
                free(self);		    
			    return MA_ERROR_CRYPTO_INTERNAL;
            }
       
            self->skip_conversion = (MA_CRYPTO_DECRYPTION_PASSWORD_ASYM == type) ?  MA_TRUE : MA_FALSE;
                                    
            self->type = type;
            self->algorithm_id = decrypt_algorithm_id[self->type];
            MA_LOG(g_crypto_logger, MA_LOG_SEV_DEBUG,"algorithim id <%d> for decrypt type <%d>", self->algorithm_id, self->type);

            MA_LOG(g_crypto_logger, MA_LOG_SEV_DEBUG,"fips mode <%d>", self->fips_mode);
            
            self->is_symmetric_key = MA_TRUE;  /* by default it is true, change this flag if the algorithm id is asymetric */

	        /* gets key handle based on algorithim id and decryption key type */
            if(MC_ALGID_SYMMETRIC_AES_128_ECB == self->algorithm_id)
		        crypto_key_handle = ma_crypto_get_rkey(macrypto, FIPS_SYMMETRIC_KEY);
            else if(MC_ALGID_SYMMETRIC_3DES == self->algorithm_id)
		        crypto_key_handle = ma_crypto_get_rkey(macrypto, NON_FIPS_SYMMETRIC_KEY);
            else { // MC_ALGID_ASYM_RSA_PKCS1
                crypto_key_handle = ma_crypto_get_rkey(macrypto, SERVER_SIGNING_KEY);
                self->is_symmetric_key = MA_FALSE;
            }
 
            lib_context = ma_crypto_get_crypto_library_context(macrypto);

	        /* gets decrypt handle */
            if(crypto_key_handle && (M_E_NO_ERROR == mc_CIPHER_new(lib_context, crypto_key_handle, self->algorithm_id, NULL, 0, MC_CIPHER_TYPE_DECRYPT, &self->dec_crypto_handle))) {
		        /* Block size is required only for symetric key oprations */		
		        if(self->is_symmetric_key && (M_E_NO_ERROR != mc_CIPHER_get_info(self->dec_crypto_handle, MC_CIPHER_INFO_BLOCK_SIZE, (void*)&self->block_size))) {
			       /* Failed to get the block size */
			       mc_CIPHER_free(self->dec_crypto_handle);
                   free(self);
			       return MA_ERROR_CRYPTO_INTERNAL;
		        }

                *decrypt = self;
		        return MA_OK;
	        }
            
            free(self);
            return MA_ERROR_CRYPTO_INTERNAL;
        }
        return MA_ERROR_OUTOFMEMORY;
    }
    MA_LOG(g_crypto_logger, MA_LOG_SEV_ERROR,"Invalid Argument");
    return MA_ERROR_INVALIDARG;
}

static ma_bool_t remove_data_security_after_decrypt(unsigned char *data, unsigned int data_size) {
	unsigned long i, j;
	for(i = 0; i < data_size; i += 8 ) {
		unsigned char ch = 'T';
		// XOR the data first for extra security       
		for(j = 0; j < 8 && j+i < data_size; j++ ) {
			unsigned char chTemp = *(data + i + j);
			*(data + i + j) = ch ^ *(data + i + j);
			ch = chTemp;
		}
	}
	return MA_TRUE;
}

static ma_error_t validate_data(ma_crypto_decryption_t *self, unsigned char *data, unsigned int data_size, ma_bytebuffer_t *decrypted_data, unsigned int *cipher_len) {
    ma_error_t rc = MA_OK;
    int cipher_algorithm = 0;
    unsigned int magic_str_size = ENCRYPT_HEADER_MAGIC_STR_SIZE(self->algorithm_id); //get_magic_header_size(self->algorithm_id);

    unsigned char *magic_str = NULL;
	if(0 != memcmp(data, ENCRYPT_HEADER_MAGIC_STR(self->algorithm_id), ENCRYPT_HEADER_MAGIC_STR_SIZE(self->algorithm_id))) {
		if(data_size > ma_bytebuffer_get_capacity(decrypted_data))
			ma_bytebuffer_reserve(decrypted_data, data_size);
		ma_bytebuffer_set_bytes(decrypted_data, 0, data, data_size);
        MA_LOG(g_crypto_logger, MA_LOG_SEV_DEBUG,"input data doesn't contain magic string. looks plain text");		
		return MA_ERROR_CRYPTO_ALREADY_DECRYPTED;  
	}

	magic_str = (unsigned char *)calloc(magic_str_size, sizeof(unsigned char));    
    if(magic_str) {
        int algorithm = 0;
        unsigned int len = 0;
		memset(magic_str, '\0', magic_str_size);
        memcpy(magic_str, data, magic_str_size);
        memcpy(&algorithm, data+magic_str_size , sizeof(int));
        memcpy(&len, data+magic_str_size+sizeof(int), sizeof(unsigned int));   
                        
        cipher_algorithm = ma_little_to_big_endian_int(algorithm);
        *cipher_len = ma_little_to_big_endian_int(len);

        MA_LOG(g_crypto_logger, MA_LOG_SEV_DEBUG,"cipher algorithm in recevied data <%d> and algorithm id based on input type <%d>", cipher_algorithm, self->algorithm_id);

        if(((self->algorithm_id == MC_ALGID_SYMMETRIC_3DES) ?2:3 )== cipher_algorithm) {
			MA_LOG(g_crypto_logger, MA_LOG_SEV_DEBUG,"input data is valid");
			rc = MA_OK;              
        }
        else
            rc = MA_ERROR_CRYPTO_INTERNAL;
        
        free(magic_str);
        return rc;
    }
    return MA_ERROR_OUTOFMEMORY;
}

#define PREFIX_SIZE 4
static ma_error_t asym_decrypt(ma_crypto_decryption_t *self, const unsigned char *data, unsigned int data_size, ma_bytebuffer_t *decrypted_data) {
    ma_error_t rc = MA_OK;

    unsigned char *in_data = NULL;
    unsigned int input_size = 0;

    if(0 == memcmp(data, "RSA1", PREFIX_SIZE)) {
        unsigned char* input = (unsigned char *)data;
        unsigned int cb_input = data_size;
        unsigned int cb_header = 0;
	    unsigned int Key_id_len = input[4];
	    if (Key_id_len <= (cb_input - 5)) {
		    //Skip over header (not part of the encrypted payload)
		    cb_header =  PREFIX_SIZE + 1 + Key_id_len;
		    input += cb_header;
		    cb_input -= cb_header;
		    in_data = input;
		    input_size = cb_input;
	    }
	    else {
            MA_LOG(g_crypto_logger, MA_LOG_SEV_ERROR,"Bad data, size verification Failed");		    
		    rc = MA_ERROR_CRYPTO_INTERNAL;
        }
    }
    else {
        unsigned int cipher_len = 0;
	    /* Handle magic header - get magic header data from input */
	    if(MA_OK == (rc = validate_data(self, (unsigned char*)data, data_size, decrypted_data, &cipher_len))) {
		    /* Allocate out data buffer*/
            in_data = (unsigned char*)data + get_magic_header_size(self->algorithm_id);
            input_size = data_size - get_magic_header_size(self->algorithm_id);
        }                    
    }

    if(MA_OK == rc) {
        unsigned int out_buffer_size = input_size;  
        unsigned char *out_buffer = (unsigned char*)calloc(out_buffer_size, sizeof(unsigned char));		    
	    if(out_buffer) {
		    if(M_E_NO_ERROR == mc_CIPHER_decrypt_buffer(self->dec_crypto_handle, in_data, input_size, out_buffer, &out_buffer_size)) {
			    if(out_buffer_size > ma_bytebuffer_get_capacity(decrypted_data))
				    ma_bytebuffer_reserve(decrypted_data, out_buffer_size);

			    ma_bytebuffer_set_bytes(decrypted_data, 0, out_buffer, out_buffer_size);			   
		    }
		    else {
                MA_LOG(g_crypto_logger, MA_LOG_SEV_ERROR,"mc_CIPHER_decrypt_buffer Failed");
			    rc = MA_ERROR_CRYPTO_INTERNAL;
            }
            free(out_buffer);
	    }
	    else
		    rc = MA_ERROR_OUTOFMEMORY;
    }
    MA_LOG(g_crypto_logger, MA_LOG_SEV_DEBUG,"asym_decrypt returns <%d>", rc);
    return (MA_ERROR_CRYPTO_ALREADY_DECRYPTED == rc) ? MA_OK : rc;
}

/* This process must be done for policy sym decryption type */
static ma_bool_t process_decrypt_data(unsigned char *in_data, unsigned int in_data_size, ma_bytebuffer_t *out_data) {		
    if(in_data_size > 2) {

        ma_wchar_t *wdata = NULL;
        char *out_buf = NULL;
        unsigned int out_buf_len = 0;
        unsigned int wstr_len = 0;
        ma_bool_t b_rc = MA_TRUE;

        unsigned char *in_ptr = in_data;
	    /* check whther transferred data are little endian */	
	    ma_bool_t byte_order_le = !(((*in_ptr)^0xFF) | (*(in_ptr+1)^0xFE));
	
	    /*get the padding size and trim it from the input*/
	    unsigned int pad_size = (unsigned int)( *(in_ptr + in_data_size - 1) );
        
        MA_LOG(g_crypto_logger, MA_LOG_SEV_DEBUG,"padding size <%d>", pad_size);

	    if(pad_size > 0) {
	        /*trim padding*/
	        *(in_ptr+in_data_size-pad_size)='\0';
	        *(in_ptr+in_data_size-pad_size+1)='\0';
	        in_data_size = in_data_size - pad_size;
	    }

	    /* if machine's endianness is different from the in_data endianness then swap the consecutive bytes */
	    if( byte_order_le^machine_is_little_endian()) {
	        unsigned int i=0;
                in_ptr += 2;		
	        for(i=0;i<(in_data_size-2);) {
		        /*swap the consecutive bytes */
		        unsigned char ch=*in_ptr;
		        *in_ptr=*(in_ptr+1);
		        *(in_ptr+1)=ch;				
		        /* increment pointers */
		        in_ptr+=2;
		        i+=2;
	        }
	    }
#ifdef MA_WINDOWS
	    /* make wstring of data of the input data */
	    wdata=(ma_wchar_t *)(in_data+2);	
        /* convert wcs into mbs */         
        if((unsigned int)-1 != (out_buf_len = ma_wide_char_to_utf8(NULL,0,(ma_wchar_t *)wdata, (in_data_size-2)/sizeof(ma_wchar_t)))) {
            out_buf = (char*)calloc(out_buf_len - 1, sizeof(char));
            if(out_buf) {
                if(-1 == (out_buf_len = ma_wide_char_to_utf8(out_buf, out_buf_len - 1, (ma_wchar_t*)wdata, (in_data_size-2)/sizeof(ma_wchar_t)))) {
                    free(out_buf);
                    b_rc = MA_FALSE;
                }
            }
            else
                b_rc = MA_FALSE;
        }
#else  /* Non windows */
        wstr_len=(in_data_size-2)/2;	/* Data is coming from EPO(windows) and the wchar_t size is 2 bytes*/
    	wdata = calloc(wstr_len+1, sizeof(ma_wchar_t)); 	    
    	if(wdata) {
            memset(wdata, 0, (wstr_len+1)*sizeof(ma_wchar_t) );
		    unsigned char *w_ptr=(unsigned char*)wdata;
		    unsigned char *uni_ptr = in_data+2;
		    int j = 0;
		    for(j=0;j < wstr_len ; j++) {
			    (machine_is_little_endian())? memcpy(w_ptr,uni_ptr,2) : memcpy(w_ptr+sizeof(ma_wchar_t)-2,uni_ptr,2);
			    w_ptr += sizeof(ma_wchar_t);
			    uni_ptr += 2;
		    }    	
        	/* convert wcs into mbs */     
            if((unsigned int)-1 != (out_buf_len = ma_wide_char_to_utf8(NULL,0,(ma_wchar_t *)wdata, wstr_len))) {
            	out_buf = (char*)calloc(out_buf_len-1,sizeof(char));
            	if(out_buf) {
                    if(-1 == (out_buf_len = ma_wide_char_to_utf8(out_buf, out_buf_len - 1, (ma_wchar_t*)wdata, wstr_len))) {
                        free(out_buf);
                        b_rc = MA_FALSE;
                    }
                }	
	        }
	        free(wdata);
	    }
	    else
	        b_rc = MA_FALSE;
#endif    
        if(b_rc && out_buf) {
            /*allocate out_data and retun output */
            if(out_buf_len > ma_bytebuffer_get_capacity(out_data))
                ma_bytebuffer_reserve(out_data, out_buf_len);
        
            ma_bytebuffer_set_bytes(out_data, 0, (unsigned char*)out_buf, out_buf_len);
            free(out_buf);
        }
        else
            MA_LOG(g_crypto_logger, MA_LOG_SEV_ERROR,"conversion to wide char is failed");

        return b_rc;
    }
    return MA_FALSE;
}

/*
* funtion : char hexchar_to_asciichar(const char ch)
* It converts a hex char into binary char. 
* If input char is not a hex char then it will returns -1.
*/
static char hexchar_to_asciichar(const char ch) {
    if(ch>='0' && ch <= '9')
	    return ch-'0';
    else if (ch>='a' && ch <= 'f')
	    return (ch-'a')+10;
    else
	    return -1;
}

static ma_error_t policy_sym_decrypt(ma_crypto_decryption_t *self, const unsigned char *data, unsigned int data_size, ma_bytebuffer_t *decrypted_data) {   
    /* 	First check for STR_ENCRYPT_HEADER_MAGIC. If input data doesn't contain magic header then return */    
    if(0 != memcmp(data, ENCRYPT_HEADER_MAGIC_STR(self->algorithm_id), ENCRYPT_HEADER_MAGIC_STR_SIZE(self->algorithm_id))) {
        if(data_size > ma_bytebuffer_get_capacity(decrypted_data))
            ma_bytebuffer_reserve(decrypted_data, data_size);
        ma_bytebuffer_set_bytes(decrypted_data, 0, data, data_size);
        MA_LOG(g_crypto_logger, MA_LOG_SEV_ERROR,"Input doesn't contain the magic header");
        return MA_OK;  
    }	
    else {          
        ma_error_t rc = MA_OK;
        int i = 0;
        unsigned char *out_buffer = NULL;

        /* converted hex encoded format to raw bin format */	
        unsigned int hex_buffer_size = data_size - ENCRYPT_HEADER_MAGIC_STR_SIZE(self->algorithm_id);

        /*ptr is pointer to hex encoded encrypted data in the input*/
        unsigned char *hex_buffer_ptr = (unsigned char*)data + ENCRYPT_HEADER_MAGIC_STR_SIZE(self->algorithm_id);
        unsigned char *in_data = (unsigned char *)calloc(hex_buffer_size/2, sizeof(unsigned char));        

        if(in_data) {
            unsigned char *tmp_buf = in_data;
            unsigned int padding_data_size = 0, out_buffer_size = 0, input_size = 0;
            for(i = 0; i < hex_buffer_size; ) {
                /* convert first hex char into binary and make it first nibble in LSB 4bits */
                unsigned char first_half, second_half;
                first_half = hexchar_to_asciichar(*hex_buffer_ptr);;
                hex_buffer_ptr++;i++;
                first_half=first_half<<4;
                first_half=first_half & 0xF0;

                /* convert second hex char into binary and make it second nibble in MSB 4bits */
                second_half=hexchar_to_asciichar(*hex_buffer_ptr);		// TBD - will handle -1 return type .....
                hex_buffer_ptr++;i++;
                second_half=second_half & 0x0F;
			
                /* make char from both nibbles */
                *tmp_buf=first_half | second_half;			
                tmp_buf++;
            }

            /*decrypt the data*/ 
            input_size = hex_buffer_size/2;
            padding_data_size = (input_size % self->block_size);
            out_buffer_size = input_size + padding_data_size;
              
            /* By default padding is enabled, so disable it if padding not required*/
            if(!padding_data_size)
                mc_CIPHER_disable_padding_for_symmetric_cipher(self->dec_crypto_handle);

            /* decrypted out buffer */
            out_buffer = (unsigned char*)calloc(out_buffer_size+self->block_size, sizeof(unsigned char)); 

            if(out_buffer) {
                if(M_E_NO_ERROR == mc_CIPHER_decrypt_data_update(self->dec_crypto_handle, in_data, input_size, out_buffer, &out_buffer_size)) {               
                    /* Handle last data block decryption if any*/
                    unsigned int last_block_size = self->block_size;
                    unsigned char *last_data_block = (unsigned char *)calloc(last_block_size, sizeof(unsigned char)); // TBD - will chec kwith NULL	                    
               
	                if(last_data_block) {
                        if(M_E_NO_ERROR == mc_CIPHER_decrypt_data_final(self->dec_crypto_handle, last_data_block, &last_block_size)) {
	                        if(last_block_size)
	                            memcpy(out_buffer + out_buffer_size, last_data_block, last_block_size);
                         
                            if(!process_decrypt_data(out_buffer, out_buffer_size + last_block_size, decrypted_data))
                                rc = MA_ERROR_CRYPTO_INTERNAL;
                        }
                        else {
                            MA_LOG(g_crypto_logger, MA_LOG_SEV_ERROR,"mc_CIPHER_decrypt_data_final Failed");
                            rc = MA_ERROR_CRYPTO_INTERNAL;
                        }
                        free(last_data_block);
                    }
                    else
                        rc = MA_ERROR_OUTOFMEMORY;
                }
                else {
                    MA_LOG(g_crypto_logger, MA_LOG_SEV_ERROR,"mc_CIPHER_decrypt_data_update Failed");
                    rc = MA_ERROR_CRYPTO_INTERNAL;
                }
                free(out_buffer);
            }
            else
                rc = MA_ERROR_OUTOFMEMORY;
            free(in_data);
        }
        else
            rc = MA_ERROR_OUTOFMEMORY;

        MA_LOG(g_crypto_logger, MA_LOG_SEV_DEBUG,"policy_sym_decrypt returns <%d>", rc);
        return rc;
    }
}

static ma_error_t sym_decrypt(ma_crypto_decryption_t *self, const unsigned char *data, unsigned int data_size, ma_bytebuffer_t *decrypted_data) {
    ma_error_t rc = MA_OK;
    unsigned int cipher_len = 0;
    if(MA_OK == (rc = validate_data(self, (unsigned char*)data, data_size, decrypted_data, &cipher_len))) {
        unsigned int input_size = data_size - get_magic_header_size(self->algorithm_id);
        unsigned int padding_data_size = input_size % self->block_size;
        unsigned int out_buffer_size = input_size + padding_data_size; 
        unsigned char *in_data = (unsigned char *)data + get_magic_header_size(self->algorithm_id);
        unsigned char *out_buffer = (unsigned char *)calloc(out_buffer_size+self->block_size, sizeof(unsigned char));
        
        if(out_buffer) {
            if(!padding_data_size)
                mc_CIPHER_disable_padding_for_symmetric_cipher(self->dec_crypto_handle);

	        if(M_E_NO_ERROR == mc_CIPHER_decrypt_data_update(self->dec_crypto_handle, in_data, input_size, out_buffer, &out_buffer_size)) {
		        /* Handle last data block decryption if any*/
		        unsigned int last_block_size = self->block_size;
                unsigned char * last_data_block = (unsigned char *)calloc(self->block_size, sizeof(unsigned char));
                if(last_data_block) {
		            if(M_E_NO_ERROR == mc_CIPHER_decrypt_data_final(self->dec_crypto_handle, last_data_block, &last_block_size)) {
			            if(last_block_size)
				            memcpy(out_buffer + out_buffer_size, last_data_block, last_block_size);

			            if(MC_ALGID_SYMMETRIC_3DES == self->algorithm_id)
                            remove_data_security_after_decrypt(out_buffer, out_buffer_size + last_block_size);

			            if((out_buffer_size + last_block_size - padding_data_size) > ma_bytebuffer_get_capacity(decrypted_data))
				            ma_bytebuffer_reserve(decrypted_data, out_buffer_size + last_block_size - padding_data_size);

			            rc = ma_bytebuffer_set_bytes(decrypted_data, 0, out_buffer, cipher_len);
		            }
		            else {
                        MA_LOG(g_crypto_logger, MA_LOG_SEV_ERROR,"mc_CIPHER_decrypt_data_final Failed");
			            rc = MA_ERROR_CRYPTO_INTERNAL;
                    }
                    free(last_data_block);
                }
                else
                    rc = MA_ERROR_OUTOFMEMORY;
	        }
	        else {
                MA_LOG(g_crypto_logger, MA_LOG_SEV_ERROR,"mc_CIPHER_decrypt_data_update Failed");
		        rc = MA_ERROR_CRYPTO_INTERNAL;
            }
            free(out_buffer);
        }
        else
            rc = MA_ERROR_OUTOFMEMORY;
    }
    MA_LOG(g_crypto_logger, MA_LOG_SEV_DEBUG,"sym_decrypt returns <%d>", rc);
    return (MA_ERROR_CRYPTO_ALREADY_DECRYPTED == rc) ? MA_OK : rc;
} 

ma_error_t ma_crypto_decrypt(ma_crypto_decryption_t *self, const unsigned char *data, unsigned int data_size, ma_bytebuffer_t *decrypted_data) {
    if(self && data_size > 0 && data && decrypted_data) {
        ma_error_t rc = MA_OK;        
        if(MA_CRYPTO_DECRYPTION_PASSWORD_ASYM == self->type)
		    rc = asym_decrypt(self, data, data_size, decrypted_data);		    
	    else if(MA_CRYPTO_DECRYPTION_POLICY_SYM == self->type) 
            rc = policy_sym_decrypt(self, data, data_size, decrypted_data);
        else 
            rc = sym_decrypt(self, data, data_size, decrypted_data);          
     
        MA_LOG(g_crypto_logger, MA_LOG_SEV_DEBUG,"ma_macrypto_decrypt returns <%d>", rc);
        return rc;
    }
    MA_LOG(g_crypto_logger, MA_LOG_SEV_ERROR,"Invalid Argument");
    return MA_ERROR_INVALIDARG;
}

static ma_error_t decrypt_inner(ma_crypto_decryption_t *self, unsigned char *data, unsigned int data_size, ma_bytebuffer_t *decrypted_data) {
    ma_error_t rc = MA_OK;  
    unsigned int padding_data_size = (((data_size % self->block_size) > 0) ? (self->block_size - (data_size % self->block_size)) : 0);
    unsigned int out_buffer_size = data_size + padding_data_size;     
    unsigned char *out_buffer = (unsigned char *)calloc(out_buffer_size + self->block_size, sizeof(unsigned char));        
    if(out_buffer) {
        if(!padding_data_size)
            mc_CIPHER_disable_padding_for_symmetric_cipher(self->dec_crypto_handle);

	    if(M_E_NO_ERROR == mc_CIPHER_decrypt_data_update(self->dec_crypto_handle, data, data_size, out_buffer, &out_buffer_size)) {        
		    /* Handle last data block decryption if any*/
		    unsigned int last_block_size = self->block_size;
            unsigned char * last_data_block = (unsigned char *)calloc(self->block_size, sizeof(unsigned char));
            if(last_data_block) {
		        if(M_E_NO_ERROR == mc_CIPHER_decrypt_data_final(self->dec_crypto_handle, last_data_block, &last_block_size)) {
			        if(last_block_size)
				        memcpy(out_buffer + out_buffer_size, last_data_block, last_block_size);
			      
			        if((out_buffer_size + last_block_size - padding_data_size) > ma_bytebuffer_get_capacity(decrypted_data))
				        ma_bytebuffer_reserve(decrypted_data, out_buffer_size + last_block_size - padding_data_size);

			        rc = ma_bytebuffer_set_bytes(decrypted_data, 0, out_buffer, out_buffer_size+last_block_size-padding_data_size);  // TBD - will check the length
                    //rc = ma_bytebuffer_set_bytes(decrypted_data, 0, out_buffer, out_buffer_size);  // TBD - will check the length
		        }
		        else {
                    MA_LOG(g_crypto_logger, MA_LOG_SEV_ERROR,"mc_CIPHER_decrypt_data_final Failed");
			        rc = MA_ERROR_CRYPTO_INTERNAL;
                }
                free(last_data_block);
            }
            else
                rc = MA_ERROR_OUTOFMEMORY;
	    }
	    else {
            MA_LOG(g_crypto_logger, MA_LOG_SEV_ERROR,"mc_CIPHER_decrypt_data_update Failed");
		    rc = MA_ERROR_CRYPTO_INTERNAL;
        }
        free(out_buffer);
    }
    else
        rc = MA_ERROR_OUTOFMEMORY;

    return rc;
   
}

ma_error_t ma_crypto_decode_and_decrypt(ma_crypto_decryption_t *self, const unsigned char *data, unsigned int data_size, ma_bytebuffer_t *decrypted_data) {
    if(self && data_size > 0 && data && decrypted_data) {
        ma_bytebuffer_t *decoded_data = NULL;
        ma_error_t rc = MA_OK;
        if(MA_OK == (rc = ma_bytebuffer_create(data_size, &decoded_data))) {
            if(MA_OK == (rc = ma_crypto_decode(data, data_size, decoded_data, self->skip_conversion))) {
                    if(MA_CRYPTO_DECRYPTION_PASSWORD_ASYM == self->type)
                        rc = asym_decrypt(self, ma_bytebuffer_get_bytes(decoded_data), ma_bytebuffer_get_size(decoded_data), decrypted_data);		    
		            else if(MA_CRYPTO_DECRYPTION_POLICY_SYM == self->type) 
                        rc = policy_sym_decrypt(self, ma_bytebuffer_get_bytes(decoded_data), ma_bytebuffer_get_size(decoded_data), decrypted_data);
                    else { /* for 3DES and AES128 */                                                 
                        rc = decrypt_inner(self, ma_bytebuffer_get_bytes(decoded_data), ma_bytebuffer_get_size(decoded_data), decrypted_data);
                    }                    
            } 
            else
                MA_LOG(g_crypto_logger, MA_LOG_SEV_ERROR,"ma_macrypto_decode Failed");
            ma_bytebuffer_release(decoded_data);
        }
		/*Srinking it to its actual size (In encoding we chnage it to allign with 8 byte block */
		ma_bytebuffer_shrink(decrypted_data, ma_strnlen((const char*)ma_bytebuffer_get_bytes(decrypted_data), ma_bytebuffer_get_size(decrypted_data)));
        MA_LOG(g_crypto_logger, MA_LOG_SEV_DEBUG,"ma_macrypto_decode_and_decrypt returns <%d>", rc);
        return rc;
    }
    MA_LOG(g_crypto_logger, MA_LOG_SEV_ERROR,"Invalid Argument");
    return MA_ERROR_INVALIDARG; 
}

ma_error_t ma_crypto_decrypt_release(ma_crypto_decryption_t *self) {
    if(self) {
	    if(self->dec_crypto_handle) mc_CIPHER_free(self->dec_crypto_handle);
        free(self);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}
