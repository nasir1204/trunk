#include "ma/internal/utils/macrypto/ma_crypto.h"
#include "ma_crypto_utils.h"

#include <stdlib.h>
#include <string.h>

extern ma_logger_t *g_crypto_logger;

#define LUTSIZE 16
unsigned char lut[LUTSIZE]={0x12, 0x15, 0x0f, 0x10, 0x11, 0x1c, 0x1a, 0x06, 0x0a, 0x1f, 0x1b, 0x18, 0x17, 0x16, 0x05, 0x19};

ma_error_t ma_crypto_encode(const unsigned char *data, unsigned int data_size, ma_bytebuffer_t *encoded_data, ma_bool_t ignore_obfuscation) {
    if(data && 0 < data_size && encoded_data) {
        ma_error_t rc = MA_ERROR_CRYPTO_INTERNAL;        
        unsigned char *encoded_buf = NULL;
        unsigned int encoded_buf_len = 2 * data_size;
        unsigned char *data_copy = NULL;
        unsigned char *in_data = NULL;

         ma_bytebuffer_add_ref(encoded_data);

        if(!ignore_obfuscation) {
            unsigned char *p_start = NULL;
            unsigned char *p_end = NULL;
            int i = 0;

            data_copy = (unsigned char *)calloc(data_size, sizeof(unsigned char));
            if(!data_copy)
                return MA_ERROR_OUTOFMEMORY; 

            memcpy(data_copy, data, data_size);
            p_start = (unsigned char *)data_copy;
            p_end = (unsigned char *)data_copy + data_size;

            while( p_start < p_end ) {
                (*p_start) ^= lut[i++];
                i %= LUTSIZE;
                p_start++;
            }

            in_data = data_copy;
        }
        else
            in_data = (unsigned char*)data;

        if(NULL != (encoded_buf = (unsigned char *)calloc(encoded_buf_len, sizeof(unsigned char)))) {
            unsigned int encoded_bytes = 0;
            int state = 0;
            unsigned int bytes_processed = 0;
            encoded_bytes = ma_base64_encode(in_data, data_size, (char *)encoded_buf, encoded_buf_len, &state, &bytes_processed);
            if(encoded_bytes <= encoded_buf_len){
                if(encoded_bytes > ma_bytebuffer_get_capacity(encoded_data))
                    ma_bytebuffer_reserve(encoded_data, encoded_bytes);
                if(MA_OK == (rc = ma_bytebuffer_set_bytes(encoded_data, 0, encoded_buf, encoded_bytes))){
					rc = ma_bytebuffer_shrink(encoded_data, encoded_bytes);
				}
            }
            else {
                MA_LOG(g_crypto_logger, MA_LOG_SEV_ERROR,"base64 encode return value more than provided buffer length");
                rc = MA_ERROR_CRYPTO_INTERNAL;
            }
            free(encoded_buf);
        }
        else
            rc = MA_ERROR_OUTOFMEMORY;
        
        if(!ignore_obfuscation) 
            free(data_copy);

        ma_bytebuffer_release(encoded_data);
        return rc;
    }
    MA_LOG(g_crypto_logger, MA_LOG_SEV_ERROR,"Invalid Argument");
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_crypto_decode(const unsigned char *data, unsigned int data_size, ma_bytebuffer_t *decoded_data, ma_bool_t ignore_obfuscation) {
    if(data && 0 < data_size && decoded_data) { 
        ma_error_t rc = MA_ERROR_CRYPTO_INTERNAL;
        unsigned char *decoded_buf = NULL;
        unsigned int decoded_buf_len = 2 * data_size;

        ma_bytebuffer_add_ref(decoded_data);

        if(NULL != (decoded_buf = (unsigned char *)calloc(decoded_buf_len, sizeof(unsigned char)))) {
            unsigned int decoded_bytes = 0;
            int state = 0;
            unsigned int bytes_processed = 0;

            decoded_bytes = ma_base64_decode((const char*)data, data_size, (char *)decoded_buf, decoded_buf_len, &state, &bytes_processed);
            if(decoded_bytes <= decoded_buf_len){

                if(!ignore_obfuscation) {
                    unsigned char *p_start = decoded_buf;
                    unsigned char *p_end = decoded_buf + decoded_bytes;
                    int i = 0;
                    while(p_start < p_end)
                    {
                      (*p_start) ^= lut[i++];
                      i %= LUTSIZE;
                      p_start++;
                    }
                }

                if(decoded_bytes > ma_bytebuffer_get_capacity(decoded_data))
                    ma_bytebuffer_reserve(decoded_data, decoded_bytes);
				if(MA_OK == (rc = ma_bytebuffer_set_bytes(decoded_data, 0, decoded_buf, decoded_bytes))){
					rc = ma_bytebuffer_shrink(decoded_data, decoded_bytes);
				}
            }
            else {
                MA_LOG(g_crypto_logger, MA_LOG_SEV_ERROR,"base64 decode return value more than provided buffer length");
                rc = MA_ERROR_CRYPTO_INTERNAL; 
            }
            free(decoded_buf);
        }
        else
            rc = MA_ERROR_OUTOFMEMORY;
        
        ma_bytebuffer_release(decoded_data);
        return rc;
    }
    MA_LOG(g_crypto_logger, MA_LOG_SEV_ERROR,"Invalid Argument");
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_crypto_hex_encode(const unsigned char *raw,const unsigned int size,unsigned char *hex,unsigned int max_size) {
	if(raw && hex && max_size >= size*2) {
        unsigned int i, j;
        const char hex_char[16]={'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
        for(i=0,j=0;i<size;i++) {
	        char temp=*(raw+i);
	        char first=(temp>>4) & 0x0F;
	        char second=temp & 0x0F;

            *(hex+j)=hex_char[(int)first];j++;
	        *(hex+j)=hex_char[(int)second];j++;
        }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static unsigned char convert_to_bin(unsigned char s) {
    return s > 64 ? s -55 : s -48;
}

ma_error_t ma_crypto_hex_decode(const unsigned char *hex,const unsigned int size,unsigned char *raw,unsigned int max_size) {
	if(raw && hex && max_size >= size/2 ) {
        unsigned int i, j;
        for(i=0,j=0;i<size/2;i++) {
            unsigned char first= convert_to_bin(hex[j++]);
	        unsigned char second=convert_to_bin (hex[j++]);
            *(raw + i) =  (first << 4) | second;
        }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;

}

