#include "ma/internal/utils/macrypto/ma_crypto.h"
#include "ma_crypto_utils.h"
#include "mfecryptc_core.h"
#include "mfecryptc_defines.h"

#include <stdlib.h>

extern ma_logger_t *g_crypto_logger;

struct ma_crypto_cms_s {
    M_AUTH_R            *auth_result;
    M_LIB_CTX           *lib_context;
};

ma_error_t ma_crypto_cms_create(ma_crypto_t *crypto, ma_crypto_cms_t **cms) {
    if(crypto && cms) {
        ma_crypto_cms_t *self = (ma_crypto_cms_t *)calloc(1, sizeof(ma_crypto_cms_t));
        if(!self) return MA_ERROR_OUTOFMEMORY;
        if(!(self->lib_context = ma_crypto_get_crypto_library_context(crypto))) {
            MA_LOG(g_crypto_logger, MA_LOG_SEV_ERROR,"crypto object not initialized.");
            free(self);
            return MA_ERROR_CRYPTO_INTERNAL;
        }
        *cms = self;
        return MA_OK;
    }
    
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_crypto_cms_release(ma_crypto_cms_t *self) {
    if(self) {
        if(self->auth_result) mc_AUTH_R_free(self->auth_result);
        free(self);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_crypto_cms_verify(ma_crypto_cms_t *self, int cms_fd, int *is_verified) {
    if(self && is_verified) {
        int err = M_E_NO_ERROR;
        M_AUTH *auth = NULL;
        if(M_E_NO_ERROR == (err = mc_AUTH_new(self->lib_context, cms_fd, &auth))) {
            if(M_E_NO_ERROR == (err = mc_AUTH_verify(auth,  is_verified))) {
                if(*is_verified) {
                    if(M_E_NO_ERROR != (err = mc_AUTH_get_result(auth, &self->auth_result)))
                        MA_LOG(g_crypto_logger, MA_LOG_SEV_ERROR,"failed to get authentication result, %d.", err);
                }else {
                    MA_LOG(g_crypto_logger, MA_LOG_SEV_ERROR,"verification failed though api passed.");
                }
            }else {
                MA_LOG(g_crypto_logger, MA_LOG_SEV_ERROR,"verification failed due to %d.", err);
            }
            mc_AUTH_free(auth);
        }else {
            MA_LOG(g_crypto_logger, MA_LOG_SEV_ERROR,"auth object creation failed due to %d.", err);
        }
        return (M_E_NO_ERROR == err) ? MA_OK : MA_ERROR_CRYPTO_INTERNAL;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_crypto_cms_get_auth_manifest(ma_crypto_cms_t *self, const char **auth_manifest) {
    if(self && auth_manifest) {
		return (M_E_NO_ERROR == mc_AUTH_R_get_manifest(self->auth_result, auth_manifest)) ? MA_OK : MA_ERROR_CRYPTO_INTERNAL;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_crypto_cms_get_issuer_name(ma_crypto_cms_t *self, const char **issuer_name) {
    if(self && issuer_name) {
        return (M_E_NO_ERROR == mc_AUTH_R_get_issuer_common_name(self->auth_result, issuer_name)) ? MA_OK : MA_ERROR_CRYPTO_INTERNAL;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_crypto_cms_get_subject_name(ma_crypto_cms_t *self, const char **subject_name) {
    if(self && subject_name) {
        return (M_E_NO_ERROR == mc_AUTH_R_get_subject_common_name(self->auth_result, subject_name)) ? MA_OK : MA_ERROR_CRYPTO_INTERNAL;
    }
    return MA_ERROR_INVALIDARG;
}
