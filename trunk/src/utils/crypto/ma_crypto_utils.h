#ifndef MA_CRYPTO_UTILS_H_INCLUDED
#define MA_CRYPTO_UTILS_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/macrypto/ma_crypto.h"
#include "mfecryptc_core.h"

#define LOG_FACILITY_NAME "crypto"

MA_CPP(extern "C" {)

typedef enum ma_crypto_key_type_e {
    AGENT_PUBLIC_KEY = 0,
    SERVER_PUBLIC_KEY,
    AGENT_PRIVATE_KEY,    
    SERVER_SIGNING_KEY,
    FIPS_SYMMETRIC_KEY,
    NON_FIPS_SYMMETRIC_KEY
} ma_crypto_key_type_t;

M_RKEY *ma_crypto_get_rkey(ma_crypto_t *macrypto, ma_crypto_key_type_t type);

M_LIB_CTX *ma_crypto_get_crypto_library_context(ma_crypto_t *crypto);

ma_logger_t *ma_crypto_get_logger(ma_crypto_t *crypto);

ma_error_t get_legacy_symmetric_key(ma_crypto_t *crypto, ma_bytebuffer_t *buffer);

ma_error_t get_fips_symmetric_key(ma_crypto_t *crypto, ma_bytebuffer_t *buffer);

ma_crypto_mode_t ma_crypto_get_fips_mode(ma_crypto_t *crypto);

unsigned int ma_base64_encode(const void  *_source, unsigned int source_size, char *dest, unsigned int max_dest_chars, int *_state, unsigned int *source_bytes_processed);

unsigned int ma_base64_decode(const char *source, unsigned int source_length, void *_dest, unsigned int  max_dest_size, int *_state, unsigned int *source_chars_processed);

unsigned long ma_little_to_big_endian(const unsigned long inputBytes);
unsigned int ma_little_to_big_endian_int(unsigned int inputBytes);
ma_bool_t machine_is_little_endian();

MA_CPP(})

#endif /* MA_CRYPTO_UTILS_H_INCLUDED */
