#include "ma/internal/utils/macrypto/ma_crypto.h"
#include "ma_crypto_utils.h"

#include "mfecryptc_defines.h"

#include "stdlib.h"

extern ma_logger_t *g_crypto_logger;

#define RSA_SIGN_LENGTH 256

#define SIGN 0
#define VERIFY 1
#define SIGN_KEY_MAP(type) (type == MA_CRYPTO_SIGN_AGENT_PRIVATE_KEY ? AGENT_PRIVATE_KEY : SERVER_SIGNING_KEY)
#define VERIFY_KEY_MAP(type) (type == MA_CRYPTO_VERIFY_AGENT_PUBLIC_KEY ? AGENT_PUBLIC_KEY : SERVER_PUBLIC_KEY)
#define KEY_MAP(mode, type) (mode == SIGN ? SIGN_KEY_MAP(type) : VERIFY_KEY_MAP(type))

struct ma_crypto_sign_s {
    M_SIGNATURE                 *sign_handle;
    ma_crypto_sign_key_type_t   type;
    unsigned int                rsa_sign_len;
};

ma_error_t ma_crypto_sign_create(ma_crypto_t *crypto, ma_crypto_sign_key_type_t type, ma_crypto_sign_t **sign) {
    if(crypto && sign) {
        ma_crypto_sign_t *self = (ma_crypto_sign_t *)calloc(1, sizeof(ma_crypto_sign_t));
        if(self) {
			M_RKEY *sign_key_handle = ma_crypto_get_rkey(crypto, KEY_MAP(SIGN, type));
            if(sign_key_handle) {
                M_LIB_CTX *lib_context = ma_crypto_get_crypto_library_context(crypto);                
                if(M_E_NO_ERROR == mc_SIGN_new(lib_context, sign_key_handle, MC_SIGNATURE_ID_SHA256_RSA, &self->sign_handle)) {
                    self->rsa_sign_len = RSA_SIGN_LENGTH; 
                    self->type = type;
			        *sign = self;
                    return MA_OK;
                }
            }
            free(self);
            return MA_ERROR_CRYPTO_INTERNAL;
        }
        return MA_ERROR_OUTOFMEMORY;
    }
    MA_LOG(g_crypto_logger, MA_LOG_SEV_ERROR,"Invalid Argument");
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_crypto_sign_data_update(ma_crypto_sign_t *self, const unsigned char *data, unsigned int data_size) {
    if(self && data && data_size > 0) {
        return (M_E_NO_ERROR == mc_SIGN_sign_data_update(self->sign_handle, (unsigned char *)data, data_size)) ? MA_OK : MA_ERROR_CRYPTO_INTERNAL;            
    }
    MA_LOG(g_crypto_logger, MA_LOG_SEV_ERROR,"Invalid Argument");
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_crypto_sign_data_final(ma_crypto_sign_t *self, ma_bytebuffer_t *signature) {
    if(self && signature && ((unsigned int)-1 != ma_bytebuffer_get_capacity(signature))) {
        if(self->rsa_sign_len > ma_bytebuffer_get_capacity(signature))
            ma_bytebuffer_reserve(signature, self->rsa_sign_len);
        if(M_E_NO_ERROR == mc_SIGN_sign_data_final(self->sign_handle, ma_bytebuffer_get_bytes(signature), &self->rsa_sign_len)) {
            /*Accroding to specification :The leading zero byte  should be added back in signature accordance with PKCS1, 
                section 7-2-1 but RSA libraries keep missing it so adding the padding 
            */
            self->rsa_sign_len = RSA_SIGN_LENGTH;
            ma_bytebuffer_shrink(signature, self->rsa_sign_len);
            return MA_OK;
        }
        return MA_ERROR_CRYPTO_INTERNAL;
    }
    MA_LOG(g_crypto_logger, MA_LOG_SEV_ERROR,"Invalid Argument");
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_crypto_sign_buffer(ma_crypto_sign_t *self, const unsigned char *data, unsigned int data_size, ma_bytebuffer_t *signature) {
    if(self && data && 0 < data_size && signature) {	
		if(self->rsa_sign_len > ma_bytebuffer_get_capacity(signature))
            ma_bytebuffer_reserve(signature, self->rsa_sign_len);         
        
        if(M_E_NO_ERROR == mc_SIGN_sign_buffer(self->sign_handle, data, data_size, ma_bytebuffer_get_bytes(signature), &self->rsa_sign_len)) {
            /*Accroding to specification :The leading zero byte  should be added back in signature accordance with PKCS1, 
                section 7-2-1 but RSA libraries keep missing it so adding the padding 
            */
            self->rsa_sign_len = RSA_SIGN_LENGTH;
            ma_bytebuffer_shrink(signature, self->rsa_sign_len);
            return MA_OK;
        }
        return MA_ERROR_CRYPTO_INTERNAL;
    }    
    MA_LOG(g_crypto_logger, MA_LOG_SEV_ERROR,"Invalid Argument");
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_crypto_sign_release(ma_crypto_sign_t *self) {
    if(self) {
        if(self->sign_handle) mc_SIGN_free(self->sign_handle);
        free(self);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

/* VERIFY Implementation */

struct ma_crypto_verify_s {    
    M_SIGNATURE                     *verify_handle;
    ma_crypto_verify_key_type_t     type;

	ma_crypto_t						*crypto_;
	M_RKEY							*verify_key_handle;
};

ma_error_t ma_crypto_verify_create(ma_crypto_t *crypto, ma_crypto_verify_key_type_t type, ma_crypto_verify_t **verify) {
    if(crypto && verify) {
        ma_crypto_verify_t *self = (ma_crypto_verify_t *)calloc(1, sizeof(ma_crypto_verify_t));
        if(self) {
			M_RKEY *verify_key_handle = ma_crypto_get_rkey(crypto, KEY_MAP(VERIFY, type));
            if(verify_key_handle) {
                M_LIB_CTX *lib_context = ma_crypto_get_crypto_library_context(crypto);                
                if(M_E_NO_ERROR == mc_SIGN_new(lib_context, verify_key_handle, MC_SIGNATURE_ID_SHA256_RSA, &self->verify_handle)) {
                    self->type = type;
			        *verify = self;
                    return MA_OK;
                }
            }
            free(self);
            return MA_ERROR_CRYPTO_INTERNAL;
        }
        return MA_ERROR_OUTOFMEMORY;
    }
    MA_LOG(g_crypto_logger, MA_LOG_SEV_ERROR,"Invalid Argument");
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_crypto_verify_data_update(ma_crypto_verify_t *self, const unsigned char *data, unsigned int data_size) {
    if(self && data && data_size > 0) {
        return (M_E_NO_ERROR == mc_SIGN_verify_data_update(self->verify_handle,(unsigned char *)data, data_size)) ? MA_OK : MA_ERROR_CRYPTO_INTERNAL;
    }
    MA_LOG(g_crypto_logger, MA_LOG_SEV_ERROR,"Invalid Argument");
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_crypto_verify_data_final(ma_crypto_verify_t *self, const unsigned char *signature, unsigned int signature_size, ma_bool_t *is_success) {
    if(self && signature && signature_size > 0 && is_success) {
        int verify_result = M_E_FAILED;
        ma_error_t rc = MA_ERROR_CRYPTO_INTERNAL;
        ma_bytebuffer_t *buffer = NULL;
        /*Accroding to specification :The leading zero byte  should be added back in signature accordance with PKCS1, 
                section 7-2-1 but RSA libraries keep missing it so adding the padding 
        */
        if(MA_OK == (rc = ma_bytebuffer_create(RSA_SIGN_LENGTH,&buffer))) {
            if(MA_OK == (rc = ma_bytebuffer_set_bytes(buffer, 0, signature, signature_size))) {
                if(M_E_NO_ERROR == mc_SIGN_verify_data_final(self->verify_handle, ma_bytebuffer_get_bytes(buffer), RSA_SIGN_LENGTH, &verify_result)) {
                    *is_success = (M_E_NO_ERROR == verify_result);
                    (void)ma_bytebuffer_release(buffer);
			        return MA_OK;
                }
            }
            (void)ma_bytebuffer_release(buffer);
        }
        return rc;  
    }
    MA_LOG(g_crypto_logger, MA_LOG_SEV_ERROR,"Invalid Argument");
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_crypto_verify_buffer(ma_crypto_verify_t *self, const unsigned char *data, unsigned int data_size, const unsigned char *signature, unsigned int signature_size, ma_bool_t *is_success) {
    if(self && data && data_size > 0 && signature && signature_size > 0 && is_success) {            
		int verify_result = M_E_FAILED;
        ma_error_t rc = MA_ERROR_CRYPTO_INTERNAL;
        ma_bytebuffer_t *buffer = NULL;
        /*Accroding to specification :The leading zero byte  should be added back in signature accordance with PKCS1, 
                section 7-2-1 but RSA libraries keep missing it so adding the padding 
        */
        if(MA_OK == (rc = ma_bytebuffer_create(RSA_SIGN_LENGTH,&buffer))) {
            if(MA_OK == (rc = ma_bytebuffer_set_bytes(buffer, 0, signature, signature_size))) {        
                if(M_E_NO_ERROR == mc_SIGN_verify_buffer(self->verify_handle, data, data_size, ma_bytebuffer_get_bytes(buffer), RSA_SIGN_LENGTH, &verify_result)) {
                    *is_success = (M_E_NO_ERROR == verify_result);
                    (void)ma_bytebuffer_release(buffer);
			        return MA_OK;
                }
            }
            (void)ma_bytebuffer_release(buffer);
        }
        return rc;                
	}
    MA_LOG(g_crypto_logger, MA_LOG_SEV_ERROR,"Invalid Argument");
	return MA_ERROR_INVALIDARG;
}

static M_RKEY *get_rkey_from_pub_key(M_LIB_CTX *lib_context, const unsigned char *key, unsigned int key_size, unsigned int rsa_key_size) {
	M_ITEM *key_data = NULL;
    M_RKEY *key_handle = NULL;
    ma_bool_t b_rc = MA_TRUE;
    /* ePO gives "RSA1" as prefix , 4 bytes */
	const unsigned char *marker = key + sizeof(ma_uint32_t) ;
	key_size  = key_size - sizeof(ma_uint32_t);

	if (M_E_NO_ERROR == mc_ITEM_new(&key_data)) {
		if(M_E_NO_ERROR == mc_ITEM_set_data(key_data, marker, key_size))
            b_rc = (M_E_NO_ERROR == mc_RKEY_asym_import(lib_context , key_data , M_KEY_ASYM_TYPE_RSA, M_KEY_ASYM_KEY_TYPE_PUBLIC, rsa_key_size, M_KEY_DSA_KEY_FORMAT_BSAFE, &key_handle));                   
		mc_ITEM_free(key_data);
	}
	return (b_rc) ? key_handle : NULL;
}

ma_error_t ma_crypto_verify_create_with_key(ma_crypto_t *macrypto, const unsigned char *key, unsigned int key_size, unsigned int rsa_key_size, ma_crypto_verify_t **verify) {
     if(macrypto && key && key_size > 0 && rsa_key_size > 0 && verify) {
        ma_crypto_verify_t *self = (ma_crypto_verify_t *)calloc(1, sizeof(ma_crypto_verify_t));
        if(self) {            
            M_LIB_CTX *lib_context = ma_crypto_get_crypto_library_context(macrypto);
			/* Verify key handle will be freed only after completion of verification as it's pointers are referred by verify handle */
            if(NULL != (self->verify_key_handle = get_rkey_from_pub_key(lib_context, key, key_size, rsa_key_size))) {
                if(M_E_NO_ERROR == mc_SIGN_new(lib_context, self->verify_key_handle, MC_SIGNATURE_ID_SHA256_RSA, &self->verify_handle)) {
					self->crypto_ = macrypto;
			        *verify = self;                        
                     return MA_OK;
		        }
				mc_RKEY_free(lib_context , self->verify_key_handle);
                free(self);		        
                return MA_ERROR_CRYPTO_INTERNAL;
	        }
            else
                MA_LOG(g_crypto_logger, MA_LOG_SEV_ERROR,"Failed to get the RKEY for the input public key");			
            free(self);
            return MA_ERROR_CRYPTO_INTERNAL;
        }
        return MA_ERROR_OUTOFMEMORY;
    }
    MA_LOG(g_crypto_logger, MA_LOG_SEV_ERROR,"Invalid Argument");
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_crypto_verify_release(ma_crypto_verify_t *self) {
     if(self) {
		 if(self->crypto_ && self->verify_key_handle) 
			 mc_RKEY_free(ma_crypto_get_crypto_library_context(self->crypto_), self->verify_key_handle);

         if(self->verify_handle) mc_SIGN_free(self->verify_handle);        
         free(self);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_crypto_verify_legacy_cab_file(ma_crypto_t *macrypto, const unsigned char *key, size_t key_len, const unsigned char *data, size_t data_len, unsigned char *signature, size_t sig_len, ma_bool_t *result){
	if(macrypto && data && key && signature && result){
		int verify_result = 1;	
		*result = MA_FALSE;

		if( M_E_NO_ERROR != mc_SIGN_legacy_verify_buffer(ma_crypto_get_crypto_library_context(macrypto),
			key, key_len,data, data_len,signature, (unsigned int)sig_len, &verify_result)){			
			return MA_ERROR_CRYPTO_INTERNAL;
		}
		
		*result = ((M_E_NO_ERROR == verify_result) ? MA_TRUE : MA_FALSE);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

