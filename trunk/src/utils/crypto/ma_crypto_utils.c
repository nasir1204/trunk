#include "ma_crypto_utils.h"
#include <string.h>
#include <limits.h>
#include <stdlib.h>

unsigned int ma_base64_encode(const void  *_source, unsigned int source_size, char *dest, unsigned int max_dest_chars, int *_state, unsigned int *source_bytes_processed) {
    // encode data to base64, every three input bytes generates four output
    // bytes
    // 'A'-'Z' = 0-25  or 0x00-0x19
    // 'a'-'z' = 26-51 or 0x1A-0x33
    // '0'-'9' = 52-61 or 0x34-0x3D
    // '+'     = 62    or 0x3E
    // '/'     = 63    or 0x3F
    // '='     = pad, resets state
    static unsigned char base64Table[64] = { 'A','B','C','D','E','F','G','H','I','J',
        'K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','a',
        'b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r',
        's','t','u','v','w','x','y','z','0','1','2','3','4','5','6','7','8',
        '9','+','/'};

    const unsigned char* source = (const unsigned char*)(_source);
    const unsigned char* source_start = source;
    const unsigned char* source_end = source + source_size;
    const char* dest_start = dest;
    const char* dest_end = dest + max_dest_chars;
    int state = 0;
    unsigned char partial = 0;

    if(_state) {
        state = (*_state)>>6;
        partial = (unsigned char)((*_state)&0x3C);
    }

    if(source) {
        while(dest < dest_end && (source < source_end || state == 3))
        {
            if(state == 3)
            {
                //ASSERT(!(partial&0xC0));
                *dest = base64Table[partial];
                state = partial = 0;
            }
            else
            {
                unsigned char data = *source;
                source ++;
                switch(state)
                {
                case 0:
                    //ASSERT(!partial);
                    *dest = base64Table[data>>2];
                    partial = (data<<4)&0x3F;
                    state = 1;
                    break;
                case 1:
                    //ASSERT(!(partial&0xCF));
                    *dest = base64Table[(data>>4)|partial];
                    partial = (data<<2)&0x3F;
                    state = 2;
                    break;
                case 2:
                    //ASSERT(!(partial&0xC3));
                    *dest = base64Table[(data>>6)|partial];
                    partial = data&0x3F;
                    state = 3;
                    break;
                }
            }
            dest++;
        }
    }

    if(state && dest+3-state < dest_end)
    {
        // flush out remaining state and pad characters
        *dest = base64Table[partial];
        dest ++;
        while(state < 3)
        {
            *dest = '=';
            dest ++;
            state ++;
        }
        state = partial = 0;
    }

    if(_state)
        *_state = (state<<6)|partial;

    if(source_bytes_processed)
        *source_bytes_processed = source - source_start;

    return dest - dest_start;
}

unsigned int ma_base64_decode(const char *source, unsigned int source_length, void *_dest, unsigned int  max_dest_size, int *_state, unsigned int *source_chars_processed) {
    // decode base64 encoded data, every valid input character translates to
    // six significant bits, four valid input characters generates up to three
    // output bytes
    unsigned int dest_size = 0;
    int state = 0;
    unsigned char partial = 0;
    unsigned int source_index = 0;
    unsigned char* dest = (unsigned char*)(_dest);

    if(source_length >= INT_MAX)
        source_length = strlen(source);

    if(_state) {
        state = (*_state)&0x3;
        partial = (unsigned char)((*_state)&0xFC);
    }

    while(source_index < source_length && dest_size < max_dest_size)
    {
        // 'A'-'Z' = 0-25  or 0x00-0x19
        // 'a'-'z' = 26-51 or 0x1A-0x33
        // '0'-'9' = 52-61 or 0x34-0x3D
        // '+'     = 62    or 0x3E
        // '/'     = 63    or 0x3F
        // '='     = pad, resets state
        // all other characters are ignored
        char code = source[source_index];
        unsigned char decode = 0xFF;
        switch(code)
        {
        case '=':
            state = 0;
            break;
        case '+':
            decode = 62;
            break;
        case '/':
            decode = 63;
            break;
        default:
            if(code >= '0') {
                if(code <= '9')
                    decode = (unsigned char)(code)+52-'0';
                else if(code >= 'A')
                {
                    if(code <= 'Z')
                        decode = (unsigned char)(code)-'A';
                    else if(code >= 'a' && code <= 'z')
                        decode = (unsigned char)(code)+26-'a';
                }
            }
        }

        if(decode != 0xFF)
        {
            if(!state) {
                partial = decode<<2;
                state = 1;
            }
            else 
            {
                unsigned char data;
                switch(state)
                {
                case 1:
                    data = partial|(decode>>4);
                    partial = decode<<4;
                    state = 2;
                    break;
                case 2:
                    data = partial|(decode>>2);
                    partial = decode<<6;
                    state = 3;
                    break;
                    // case 3:
                default:
                    data = partial|decode;
                    state = 0;
                    break;
                }
                dest[dest_size] = data;
                dest_size ++;
            }
        }
        source_index++;
    }

    if(_state)
        *_state = partial|state;

    if(source_chars_processed)
        *source_chars_processed = source_index;

    return dest_size;
}

#define SWAP(a, b) (((a) ^= (b)), ((b) ^= (a)), ((a) ^= (b)))
#define INIT_XOR_STRING(a)		\
		a[0] = 0;				\
		a[0] += 32;				\
		a[1] = a [0] + 12;		\
		a[2] = a [1] + 32;		\
		a[3] = a [2] - 5;		\
		a[4] = a [3] + 33;		\
		a[5] = a [4] - 40;		\
		a[6] = a [5] + 13;		\
		a[7] = a [6] - 17;		\
		a[8] = a [7] - 5;		\
		a[9] = a [8] + 12;		\
		a[10] = a [9] - 6;		\
		a[11] = a [10] + 21;	\
		a[12] = a [11] - 1;		\
		a[13] = a [12] + 2;		\
		a[14] = a [13] + 13;	\
		a[15] = a [14] - 14;	\
		a[16] = a [15] + 60;	\
		a[17] = a [16] - 7;		\
		a[18] = a [17] + 33;	\
		a[19] = a [18] - 1;		\
		a[20] = a [19] + 10;	\
		a[21] = a [20] - 33;	\
		a[22] = a [21] + 17;	\
		a[23] = a [22] + 10;	\
		a[24] = a [23] - 22;	\
		a[25] = a [24] + 16;	\
		a[26] = a [25] - 7;		\
		a[27] = a [26] + 7;		\
		a[28] = a [27] - 15;	\
		a[29] = a [28] - 10;	\
		a[30] = a [29] + 19;	\
		a[31] = a [30] + 12




ma_error_t get_legacy_symmetric_key(ma_crypto_t *self, ma_bytebuffer_t *buffer) {
    unsigned char key[8] = { 0x68, 0x2d, 0x13, 0x10, 0x62, 0x61, 0x60, 0x2c };    
    unsigned char xor [32];
    unsigned int key_len = 8 , xor_len = 32 , xor_index = 0  , hash_size = 24 , swap_len = 0 ,i = 0;
	ma_bytebuffer_t *hash_buffer = NULL;
	ma_error_t rc = MA_ERROR_CRYPTO_INTERNAL;
	
	// Swap some bytes	
	for (i = 0 , swap_len = key_len/2; i < swap_len; i++)
		SWAP(key[i] , key[i + swap_len]);

	// Swap the bits per byte
	for (i  = 0; i < key_len; i++) {
        int temp = ((key [i] >> 1) ^ (key [i] >> 4)) & ((1 << 3) - 1);
		key[i] = key[i] ^ ((temp << 1) | (temp << 4));
	}

	// Initialize the value	
    INIT_XOR_STRING (xor);	
	
	// Xor the original string
	for (i  = 0 , xor_index = 0  ; i < key_len; i++) {
		key[i] ^= xor [xor_index];
        xor_index++;
        if(xor_index > xor_len) 
            xor_index = 0;				
	}

	if(MA_OK == (rc = ma_bytebuffer_create(hash_size, &hash_buffer))) {
        ma_crypto_hash_t *hash = NULL;
        if(MA_OK == (rc = ma_crypto_hash_create(self, MA_CRYPTO_HASH_SHA1, &hash))) {
		    if(MA_OK == (rc = ma_crypto_hash_buffer(hash, key, key_len, hash_buffer))) 
			    rc = ma_bytebuffer_set_bytes(buffer, 0, ma_bytebuffer_get_bytes(hash_buffer),hash_size);
		    ma_crypto_hash_release(hash);
        }
        ma_bytebuffer_release(hash_buffer);        
	}
	return rc;    
}

ma_error_t get_fips_symmetric_key(ma_crypto_t *self, ma_bytebuffer_t *buffer) {
    //EPO_AES128_PREFIX = "EPOAES128:";
    static unsigned char key[] = { -2, 39, -63, 92, -65, 57, 118, -37, -120, 101, 82, -13, -92, -13, 60, 81 };        
    return ma_bytebuffer_set_bytes(buffer, 0, key, sizeof(key));
}

static ma_bool_t computer_is_little_endian(void) {
    // The first byte of i will be 1 on LE
    // machines and 0 on BE machines
    static const unsigned long i = 1;
    return (*(const unsigned char *)&i != 0);
}

static unsigned long reverse_bytes( const unsigned long inputBytes ) {
    unsigned long result = 0;
    unsigned int ulSize = sizeof(unsigned long);
    register unsigned int i = 0;
    // Reverse the bytes
    for( i = 0; i < ulSize; i++ )  
        ((unsigned char *)&result)[i] = ((unsigned char *)&inputBytes)[abs((int)(i - (ulSize - 1)))];

    return result;
}

static unsigned long reverse_byte_when_computer_is_big_endian(const unsigned long inputBytes) {
    unsigned long result = inputBytes;

    // Scenario 1:  The computer is BE and the
    // input is LE.  Convert inputBytes to BE
    // so the machine interprets the data
    // properly.
    //
    // Scenario 2:  The computer is BE and the
    // input is BE.  Convert inputBytes to LE
    // so the data can be xfered to an LE
    // machine in the right format.
    if( !computer_is_little_endian() )  
        result = reverse_bytes( inputBytes );

    return result;
}

unsigned long ma_little_to_big_endian(const unsigned long inputBytes) {
    // The programmer is telling us their input
    // is LE and they want it converted to the
    // host's endian format. If the computer is
    // a BE machine, convert inputBytes to BE.
    return( reverse_byte_when_computer_is_big_endian( inputBytes ) );
}   


static unsigned int reverse_bytes_4( unsigned int inputBytes ) {
    unsigned int result = 0;
    unsigned int ulSize = sizeof(unsigned int);
    register unsigned int i = 0;
    // Reverse the bytes
    for( i = 0; i < ulSize; i++ )  
        ((unsigned char *)&result)[i] = ((unsigned char *)&inputBytes)[abs((int)(i - (ulSize - 1)))];

    return result;
}

static unsigned int reverse_byte_when_computer_is_big_endian_4(unsigned int inputBytes) {
    unsigned int result = inputBytes;

    // Scenario 1:  The computer is BE and the
    // input is LE.  Convert inputBytes to BE
    // so the machine interprets the data
    // properly.
    //
    // Scenario 2:  The computer is BE and the
    // input is BE.  Convert inputBytes to LE
    // so the data can be xfered to an LE
    // machine in the right format.
    if( !computer_is_little_endian() )  
        result = reverse_bytes_4( inputBytes );

    return result;
}

unsigned int ma_little_to_big_endian_int(unsigned int inputBytes) {
    // The programmer is telling us their input
    // is LE and they want it converted to the
    // host's endian format. If the computer is
    // a BE machine, convert inputBytes to BE.
    return( reverse_byte_when_computer_is_big_endian_4( inputBytes ) );
}   

ma_bool_t machine_is_little_endian() {
    // The first byte of i will be 1 on LE
    // machines and 0 on BE machines
    static const unsigned long i = 1;
    return( *(const unsigned char *)&i != 0 );
}

