#include "ma/internal/utils/macrypto/ma_crypto.h"
#include "ma/internal/utils/platform/ma_sys_apis.h"
#include "ma/internal/utils/text/ma_utf8.h"
#include "ma/internal/ma_macros.h"
#include "ma/logger/ma_logger.h"

#include "ma_crypto_utils.h"

#include "mfecryptc_logger.h"
#include "mfecryptc_core.h"
#include "mfecryptc_defines.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

ma_logger_t *g_crypto_logger = NULL;

#define MA_CRYPTO_KEYS_MAX   6
#define MA_CRYPTO_KEYS  2

#define MA_CRYPTO_ASYM_KEY_LENGTH 2048
#define MA_CRYPTO_KEY_HASH_SIZE   32

/* NOTE NOTE: this and ma_crypto_key_type_t in utils.h should be in the same order */
const char *g_persist_keys_name[]= {
    "agentpubkey.bin",
    "serverpubkey.bin",
    "agentprvkey.bin",		
    "serverreqseckey.bin"	
};

struct ma_crypto_ctx_s {
    ma_crypto_mode_t             mode;
    ma_crypto_role_t             role;
    ma_crypto_agent_mode_t       agent_mode;
    ma_logger_t                  *logger;
    char                         *keystore_path;
    char                         *certstore_path;
    char                         *crypto_lib_path;
};

typedef struct ma_crypto_key_mgr_s {
    M_RKEY              *rkeys[MA_CRYPTO_KEYS_MAX];
	ma_bytebuffer_t     *keys[MA_CRYPTO_KEYS];
    ma_bytebuffer_t     *keys_hash[MA_CRYPTO_KEYS];
}ma_crypto_key_mgr_t;

struct ma_crypto_s {
    ma_crypto_ctx_t         *macrypto_ctx;
    M_LIB_CTX               *crypt_ctx;
    ma_crypto_key_mgr_t     *key_mgr;
	char					crypto_lib_path_env[MA_MAX_LEN];
};


ma_error_t ma_crypto_ctx_create(ma_crypto_ctx_t **ctx) {
    if(ctx) {
        ma_crypto_ctx_t *self = (ma_crypto_ctx_t*)calloc(1,sizeof(ma_crypto_ctx_t));
        if(!self) return MA_ERROR_OUTOFMEMORY;
        *ctx = self;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_crypto_ctx_create_from_ctx(const ma_crypto_ctx_t *source , ma_crypto_ctx_t **ctx) {
    ma_error_t rc = MA_OK ;
    if(MA_OK == (rc = ma_crypto_ctx_create(ctx))) {
        (*ctx)->agent_mode = source->agent_mode;
        (*ctx)->mode = source->mode;
        (*ctx)->role = source->role;
        (*ctx)->logger = source->logger;
        if(source->keystore_path)
            (*ctx)->keystore_path = strdup(source->keystore_path);
        if(source->certstore_path)
            (*ctx)->certstore_path = strdup(source->certstore_path);
        (*ctx)->crypto_lib_path = strdup(source->crypto_lib_path);
    }
    return rc;
}


static ma_error_t ma_crypto_ctx_validate(ma_crypto_ctx_t *self) {
    if(!self->crypto_lib_path)
        return MA_ERROR_INVALIDARG;
    return MA_OK;
}

ma_error_t ma_crypto_ctx_set_fips_mode(ma_crypto_ctx_t *self, ma_crypto_mode_t mode) {
    if(self) {
        self->mode = mode ;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_crypto_ctx_set_role(ma_crypto_ctx_t *self, ma_crypto_role_t role) {
    if(self) {
        self->role = role;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_crypto_ctx_set_agent_mode(ma_crypto_ctx_t *self, ma_crypto_agent_mode_t agent_mode) {
    if(self) {
        self->agent_mode = agent_mode;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_crypto_ctx_set_logger(ma_crypto_ctx_t *self, ma_logger_t *logger) {
    if(self && logger) {
        if(g_crypto_logger) 
            ma_logger_release(g_crypto_logger);
		ma_logger_inc_ref(g_crypto_logger = logger);
        self->logger = g_crypto_logger;  /* Assigning global logger object which is used in other modules sign/hash/encryption */
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_crypto_ctx_set_keystore(ma_crypto_ctx_t *self, const char *keystore_path) {
    if(self && keystore_path) {
        self->keystore_path = strdup(keystore_path);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}
ma_error_t ma_crypto_ctx_set_certstore(ma_crypto_ctx_t *self, const char *certstore_path) {
    if(self && certstore_path) {
        self->certstore_path = strdup(certstore_path);
        return MA_OK;
        }
    return MA_ERROR_INVALIDARG;
        }
        
ma_error_t ma_crypto_ctx_set_crypto_lib_path(ma_crypto_ctx_t *self, const char *crypto_lib_path) {
    if(self && crypto_lib_path) {
        self->crypto_lib_path = strdup(crypto_lib_path);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_crypto_ctx_release(ma_crypto_ctx_t *self) {
    if(self) {
        if(self->keystore_path) free(self->keystore_path);
        if(self->crypto_lib_path) free(self->crypto_lib_path);
        if(self->certstore_path) free(self->certstore_path);
        free(self);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_crypto_ctx_t *ma_crypto_ctx_copy(const ma_crypto_ctx_t *self) {
    ma_crypto_ctx_t *ctx = NULL;
    return (self && (MA_OK == ma_crypto_ctx_create_from_ctx(self, &ctx))) ? ctx : NULL;
}

const ma_crypto_ctx_t *ma_crypto_get_context(ma_crypto_t *self) {
    return (self) ? self->macrypto_ctx : NULL;
}

ma_crypto_mode_t ma_crypto_ctx_get_fips_mode(const ma_crypto_ctx_t *ctx) {
    return (ctx) ? ctx->mode : -1;
}

ma_crypto_agent_mode_t ma_crypto_ctx_get_agent_mode(const ma_crypto_ctx_t *ctx) {
    return (ctx) ? ctx->agent_mode : -1;
}

ma_crypto_role_t ma_crypto_ctx_get_role(const ma_crypto_ctx_t *ctx) {
    return (ctx) ? ctx->role : -1;
}

/* Static function declartion for macrypto operations*/
static ma_error_t ma_crypto_load_agent_keys(ma_crypto_t *macrypto);
static ma_error_t ma_crypto_load_key(ma_crypto_t *self, ma_crypto_key_type_t type);
static ma_error_t ma_crypto_load_key_hash(ma_crypto_t *self, ma_crypto_key_type_t type);
static void log_message_mfecryptc(int level,const char *fmt,...);
static ma_error_t ma_crypto_import_key(ma_crypto_t *self, ma_crypto_key_type_t type, const unsigned char *key, unsigned int key_size);

static ma_logger_t *g_logger = NULL ;

ma_error_t ma_crypto_create(ma_crypto_t **macrypto) {
    if(macrypto) {
        ma_crypto_t *self = NULL;

        self = (ma_crypto_t*)calloc(1,sizeof(ma_crypto_t));
        if(!self) return MA_ERROR_OUTOFMEMORY;

		/* Default is empty string to overwrite the path if already set */
		MA_MSC_SELECT(_snprintf, snprintf)(self->crypto_lib_path_env, MA_MAX_LEN, "R_SHLIB_LD_LIBRARY_PATH=%s", "");

        *macrypto = self;

        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_crypto_release(ma_crypto_t *self) {
    if(self) {
		 if(g_crypto_logger) {
            ma_logger_release(g_crypto_logger);
            g_crypto_logger = NULL;            
        }
        free(self);   
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_crypto_initialize(ma_crypto_ctx_t *context, ma_crypto_t *self) {
    if(context && self) {
        ma_error_t rc = MA_OK;
        M_LIB_RESOURCE_LIST *p_resource_list = NULL;
		M_LIB_CTX *crypt_ctx = NULL;

        if(MA_OK != (rc = ma_crypto_ctx_validate(context)))
            return rc;

        if(MA_OK != (rc = ma_crypto_ctx_create_from_ctx(context, &self->macrypto_ctx))) {
            return rc;
        }

        self->key_mgr = (ma_crypto_key_mgr_t *)calloc(1,sizeof(ma_crypto_key_mgr_t));
        if(!self->key_mgr) {
            ma_crypto_deinitialize(self);
            return MA_ERROR_OUTOFMEMORY;
        }
        
        if(self->macrypto_ctx->logger) {
            mc_LIB_set_logger_cb(log_message_mfecryptc);
            g_logger = self->macrypto_ctx->logger;
        }

		/*Check the old crypto instance in the same process space and if it exists unload it */
        if(M_E_NO_ERROR == mc_LIB_load(NULL, &crypt_ctx)) {
            self->crypt_ctx = crypt_ctx;            
            MA_LOG(self->macrypto_ctx->logger,MA_LOG_SEV_DEBUG,"can't create new instance of crypto, using existing one.");            
        }
		else {
			p_resource_list = (MA_CRYPTO_MODE_FIPS == self->macrypto_ctx->mode)
				? mc_get_fips140_ssl_resource_list()
				: mc_get_default_resource_list();

			if(!p_resource_list) {
				MA_LOG(self->macrypto_ctx->logger,MA_LOG_SEV_ERROR,"Failed to create crypto resource list");
				ma_crypto_deinitialize(self);
				return MA_ERROR_CRYPTO_INTERNAL;
			}

			if(self->macrypto_ctx->keystore_path) {
				MA_LOG(self->macrypto_ctx->logger, MA_LOG_SEV_DEBUG,"Using %s as keystore path",self->macrypto_ctx->keystore_path);
				mc_LIB_set_keystore_path(p_resource_list, self->macrypto_ctx->keystore_path, strlen(self->macrypto_ctx->keystore_path));
			}

			if(self->macrypto_ctx->certstore_path) {
				MA_LOG(self->macrypto_ctx->logger, MA_LOG_SEV_DEBUG,"Using %s as certstore path",self->macrypto_ctx->certstore_path);
				mc_LIB_set_certstore_path(p_resource_list, self->macrypto_ctx->certstore_path, strlen(self->macrypto_ctx->certstore_path));
			}

			MA_LOG(self->macrypto_ctx->logger, MA_LOG_SEV_DEBUG,"Using %s as crypto lib path",self->macrypto_ctx->crypto_lib_path);
			mc_LIB_set_crypto_path(p_resource_list, self->macrypto_ctx->crypto_lib_path , strlen(self->macrypto_ctx->crypto_lib_path));

			/*In non-fips mode , role should be default as per RSA */
			if(MA_CRYPTO_ROLE_OFFICER == self->macrypto_ctx->role)
				mc_LIB_set_officer_role(p_resource_list);

			if(M_E_NO_ERROR != mc_LIB_load(p_resource_list,&self->crypt_ctx)){
				MA_LOG(self->macrypto_ctx->logger, MA_LOG_SEV_ERROR,"Failed to initialize crypto library");
				ma_crypto_deinitialize(self);
				return MA_ERROR_CRYPTO_INTERNAL;
			}
		}

        if( (MA_CRYPTO_AGENT_MANAGED == self->macrypto_ctx->agent_mode) &&             
            (MA_OK != (rc  = ma_crypto_load_agent_keys(self)))) {
            MA_LOG(self->macrypto_ctx->logger, MA_LOG_SEV_ERROR,"Failed to load agent keys");
            ma_crypto_deinitialize(self);
            return rc;
        }

        MA_LOG(self->macrypto_ctx->logger, MA_LOG_SEV_DEBUG,"Successfully initialized crypto library");
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_crypto_deinitialize(ma_crypto_t *self) {
     if(self) {        
        if(self->macrypto_ctx)
                ma_crypto_ctx_release(self->macrypto_ctx);
        if(self->key_mgr) {
            int i = 0;
            for(i = 0 ; i < MA_CRYPTO_KEYS_MAX ; i++) 
                if(self->key_mgr->rkeys[i])     mc_RKEY_free(self->crypt_ctx,self->key_mgr->rkeys[i]);
            for(i = 0 ; i < MA_CRYPTO_KEYS; i++) {
                if(self->key_mgr->keys_hash[i]) ma_bytebuffer_release(self->key_mgr->keys_hash[i]);
				if(self->key_mgr->keys[i]) ma_bytebuffer_release(self->key_mgr->keys[i]);
			}
            free(self->key_mgr);
        }

        if(self->crypt_ctx)
            mc_LIB_unload(&self->crypt_ctx);    
                
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_crypto_import_server_public_key(ma_crypto_t *self, const unsigned char *key, unsigned int key_size) {
    if(self && (MA_CRYPTO_AGENT_MANAGED == self->macrypto_ctx->agent_mode) && key && (0 != key_size)) {
        ma_error_t rc = MA_OK ;
        if(MA_OK == (rc = ma_crypto_import_key(self, SERVER_PUBLIC_KEY, key, key_size))) {
            if(self->key_mgr->keys_hash[SERVER_PUBLIC_KEY]) {
                ma_bytebuffer_release(self->key_mgr->keys_hash[SERVER_PUBLIC_KEY]);
                self->key_mgr->keys_hash[SERVER_PUBLIC_KEY] = NULL ;
            }
            if(MA_OK != ma_crypto_load_key_hash(self, SERVER_PUBLIC_KEY))
                MA_LOG(self->macrypto_ctx->logger, MA_LOG_SEV_ERROR,"Failed to load key hash for server public key");
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_crypto_import_server_signing_key(ma_crypto_t *self, const unsigned char *key, unsigned int key_size) {
    return (self && (MA_CRYPTO_AGENT_MANAGED == self->macrypto_ctx->agent_mode) && key && (0 != key_size)) 
            ? ma_crypto_import_key(self, SERVER_SIGNING_KEY, key, key_size) 
            : MA_ERROR_INVALIDARG;
}


ma_error_t ma_crypto_get_agent_public_key(ma_crypto_t *self, const ma_bytebuffer_t **key) {
	if(self && (MA_CRYPTO_AGENT_MANAGED == self->macrypto_ctx->agent_mode) && key) {
		ma_error_t rc = MA_OK;
		if(self->key_mgr->keys[AGENT_PUBLIC_KEY]) {
            *key = self->key_mgr->keys[AGENT_PUBLIC_KEY];
            return MA_OK;
        }
		if(MA_OK == (rc = ma_crypto_load_key(self,AGENT_PUBLIC_KEY))) {
            *key = self->key_mgr->keys[AGENT_PUBLIC_KEY];
            return MA_OK;
        }
        return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_crypto_get_server_public_key(ma_crypto_t *self, const ma_bytebuffer_t **key) {
	if(self && (MA_CRYPTO_AGENT_MANAGED == self->macrypto_ctx->agent_mode) && key) {
		ma_error_t rc = MA_OK;
		if(self->key_mgr->keys[SERVER_PUBLIC_KEY]) {
            *key = self->key_mgr->keys[SERVER_PUBLIC_KEY];
            return MA_OK;
        }
		if(MA_OK == (rc = ma_crypto_load_key(self,SERVER_PUBLIC_KEY))) {
            *key = self->key_mgr->keys[SERVER_PUBLIC_KEY];
            return MA_OK;
        }
        return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_crypto_get_agent_public_key_hash(ma_crypto_t *self, const ma_bytebuffer_t **key_hash) {
    if(self && (MA_CRYPTO_AGENT_MANAGED == self->macrypto_ctx->agent_mode) && key_hash) {
        ma_error_t rc = MA_OK;
        if(self->key_mgr->keys_hash[AGENT_PUBLIC_KEY]) {
            *key_hash = self->key_mgr->keys_hash[AGENT_PUBLIC_KEY];
            return MA_OK;
        }
        if(MA_OK == (rc = ma_crypto_load_key_hash(self,AGENT_PUBLIC_KEY))) {
            *key_hash = self->key_mgr->keys_hash[AGENT_PUBLIC_KEY];
            return MA_OK;
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_crypto_get_server_public_key_hash(ma_crypto_t *self, const ma_bytebuffer_t **key_hash) {
    if(self && (MA_CRYPTO_AGENT_MANAGED == self->macrypto_ctx->agent_mode) && key_hash ) {
        ma_error_t rc = MA_OK;
        if(self->key_mgr->keys_hash[SERVER_PUBLIC_KEY]) {
            *key_hash = self->key_mgr->keys_hash[SERVER_PUBLIC_KEY];
            return MA_OK;
        }
        if(MA_OK == (rc = ma_crypto_load_key_hash(self,SERVER_PUBLIC_KEY))) {
            *key_hash = self->key_mgr->keys_hash[SERVER_PUBLIC_KEY];
            return MA_OK;
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}


/*Utils funtions for other operations  to use */
static ma_error_t load_symmetric_key(ma_crypto_t *self, ma_crypto_key_type_t type );

M_RKEY *ma_crypto_get_rkey(ma_crypto_t *self, ma_crypto_key_type_t type ) {
    if(!self) return NULL ;

    /* In unmanaged mode, we don't allow any agent/server keys to be used */
    if( (MA_CRYPTO_AGENT_UNMANAGED == self->macrypto_ctx->agent_mode)  && 
        (type <= SERVER_SIGNING_KEY)) {
        MA_LOG(self->macrypto_ctx->logger, MA_LOG_SEV_ERROR,"Operation using agent/server keys for type %d are not allowed in unmanaged mode",type);
        return NULL;
    }

    if(self->key_mgr->rkeys[type]) 
        return self->key_mgr->rkeys[type] ;

    if((FIPS_SYMMETRIC_KEY == type) || (NON_FIPS_SYMMETRIC_KEY == type)) {
        if(MA_OK != load_symmetric_key(self, type)) {
            MA_LOG(self->macrypto_ctx->logger, MA_LOG_SEV_ERROR,"Failed to load symmetric key type %d", type);
            return NULL;
        }
    }
    else {
        if(M_E_NO_ERROR != mc_RKEY_from_persist(self->crypt_ctx, g_persist_keys_name[type], &(self->key_mgr->rkeys[type]))) {
            MA_LOG(self->macrypto_ctx->logger, MA_LOG_SEV_ERROR,"Failed to get the key type %d", type);
            return NULL;
        }
    }

    return self->key_mgr->rkeys[type];
}

M_LIB_CTX *ma_crypto_get_crypto_library_context(ma_crypto_t *self) {
    return self ? self->crypt_ctx : NULL;
}

ma_logger_t *ma_crypto_get_logger(ma_crypto_t *self) {
    return self ? self->macrypto_ctx->logger : NULL;
}

ma_crypto_mode_t ma_crypto_get_fips_mode(ma_crypto_t *self) {
    return (self && (self->macrypto_ctx)) ? self->macrypto_ctx->mode : -1 ;
}


MA_CRYPTO_API ma_error_t ma_crypto_add_cert(ma_crypto_t *self, const char *cert_name, const unsigned char *data, size_t data_len) {
    if(self && cert_name  && data && data_len) {
        if(self->macrypto_ctx->certstore_path) {
            int err = M_E_NO_ERROR;
            if(M_E_NO_ERROR != (err = mc_LIB_add_cert(self->crypt_ctx, cert_name, data, data_len))) {
                return MA_ERROR_CRYPTO_INTERNAL;
            }
            
        }else {
            MA_LOG(self->macrypto_ctx->logger, MA_LOG_SEV_ERROR,"certstore path is not set.");
            return MA_ERROR_INVALIDARG;
        }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_crypto_remove_cert(ma_crypto_t *self, const char *cert_name) {
    if(self && cert_name) {
        if(self->macrypto_ctx->certstore_path) {
            int err = M_E_NO_ERROR;
            if(M_E_NO_ERROR != (err = mc_LIB_remove_cert(self->crypt_ctx,(char*)cert_name))) {
                return MA_ERROR_CRYPTO_INTERNAL;
            }
            
        }else {
            MA_LOG(self->macrypto_ctx->logger, MA_LOG_SEV_ERROR,"certstore path is not set.");
            return MA_ERROR_INVALIDARG;
        }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static void change_key_perm(ma_crypto_t *self, const char *key_name) {
#ifndef MA_WINDOWS
    char key_path[MA_MAX_LEN] = {0};
    MA_MSC_SELECT(_snprintf, snprintf)(key_path, MA_MAX_LEN, "%s/%s", self->macrypto_ctx->keystore_path, key_name);
    chmod(key_path, 0644);
#endif
}
/* Static functions */

static ma_error_t ma_crypto_import_key(ma_crypto_t *self, ma_crypto_key_type_t type, const unsigned char *key, unsigned int key_size) {
    M_ITEM *key_item = NULL ;
    int result = 0 ;

    if(MA_CRYPTO_ROLE_OFFICER != self->macrypto_ctx->role) {
        MA_LOG(self->macrypto_ctx->logger, MA_LOG_SEV_ERROR,"Key import is not allowed in this role");        
        return MA_ERROR_INVALIDARG;
    }

    if(M_E_NO_ERROR != (result = mc_ITEM_new(&key_item))) {
        MA_LOG(self->macrypto_ctx->logger, MA_LOG_SEV_ERROR,"Failed to create new item,error %d", result);
        return MA_ERROR_OUTOFMEMORY ;
    }

    /* ePO gives "RSA1" as prefix , 4 bytes */
    if(M_E_NO_ERROR != (result = mc_ITEM_set_data(key_item, key + sizeof(ma_uint32_t), key_size - sizeof(ma_uint32_t)))) {
        MA_LOG(self->macrypto_ctx->logger, MA_LOG_SEV_ERROR,"Failed to set key in key item,error %d", result);
        mc_ITEM_free(key_item);
        return MA_ERROR_UNEXPECTED;
    }

    if(M_E_NO_ERROR != (result = mc_RKEY_asym_import(self->crypt_ctx, key_item , M_KEY_ASYM_TYPE_RSA, 
        (SERVER_PUBLIC_KEY == type )? M_KEY_ASYM_KEY_TYPE_PUBLIC : M_KEY_ASYM_KEY_TYPE_PRIVATE, MA_CRYPTO_ASYM_KEY_LENGTH, 
        M_KEY_DSA_KEY_FORMAT_BSAFE, &(self->key_mgr->rkeys[type])))) {
            mc_ITEM_free(key_item);
            MA_LOG(self->macrypto_ctx->logger, MA_LOG_SEV_ERROR,"Failed to import key %d, error %d", type,result);            
            return MA_ERROR_CRYPTO_INTERNAL;
    }

    mc_ITEM_free(key_item);

    /*Save the keys into keystore */
    if(M_E_NO_ERROR != (result = mc_RKEY_to_persist(self->crypt_ctx, self->key_mgr->rkeys[type], g_persist_keys_name[type]))) {
        mc_RKEY_free(self->crypt_ctx, self->key_mgr->rkeys[type]);
        self->key_mgr->rkeys[type] = NULL ;
        MA_LOG(self->macrypto_ctx->logger, MA_LOG_SEV_ERROR,"Failed to save key %d, error %d", type,result);        
        return MA_ERROR_CRYPTO_INTERNAL;
    }
    change_key_perm(self, g_persist_keys_name[type]);

    return MA_OK;
}


static ma_error_t ma_crypto_load_agent_keys(ma_crypto_t *self) {
    int result = M_E_KEY_NOT_EXISTS;
    ma_error_t rc = MA_ERROR_CRYPTO_INTERNAL ;


    if((M_E_NO_ERROR != mc_RKEY_persist_exists(self->crypt_ctx, g_persist_keys_name[AGENT_PUBLIC_KEY],&result)))        
        return MA_ERROR_CRYPTO_INTERNAL;

    //TODO - what about if the keys exists but corrupted and not able to load
    /*Load the keys */
    if(M_E_KEY_EXISTS == result) {

        rc = (M_E_NO_ERROR == mc_RKEY_from_persist(self->crypt_ctx, g_persist_keys_name[AGENT_PUBLIC_KEY], &(self->key_mgr->rkeys[AGENT_PUBLIC_KEY])))
            && (M_E_NO_ERROR == mc_RKEY_from_persist(self->crypt_ctx, g_persist_keys_name[AGENT_PRIVATE_KEY], &(self->key_mgr->rkeys[AGENT_PRIVATE_KEY])))
            ? MA_OK : MA_ERROR_CRYPTO_INTERNAL;        
        return rc; 
    }    

    /* Generate the keys */
    if(M_E_NO_ERROR != (result =  mc_RKEY_asym_new(self->crypt_ctx, M_KEY_ASYM_TYPE_RSA, MA_CRYPTO_ASYM_KEY_LENGTH,
        M_KEY_DSA_KEY_FORMAT_BSAFE, &(self->key_mgr->rkeys[AGENT_PUBLIC_KEY]), 
        &(self->key_mgr->rkeys[AGENT_PRIVATE_KEY])))) {
            MA_LOG(self->macrypto_ctx->logger, MA_LOG_SEV_ERROR,"Failed to generate agent keys, error %d", result);            
            return MA_ERROR_CRYPTO_INTERNAL;
    }

    /*Save the keys onto disks */
    if(M_E_NO_ERROR != (result = mc_RKEY_to_persist(self->crypt_ctx , self->key_mgr->rkeys[AGENT_PUBLIC_KEY] , g_persist_keys_name[AGENT_PUBLIC_KEY]))) {        
        mc_RKEY_free(self->crypt_ctx , self->key_mgr->rkeys[AGENT_PUBLIC_KEY]);
        mc_RKEY_free(self->crypt_ctx , self->key_mgr->rkeys[AGENT_PRIVATE_KEY]);
        self->key_mgr->rkeys[AGENT_PUBLIC_KEY] = self->key_mgr->rkeys[AGENT_PRIVATE_KEY] = NULL;
        MA_LOG(self->macrypto_ctx->logger, MA_LOG_SEV_ERROR,"Failed to save agent public key into keystore, error %d", result);        
        return MA_ERROR_CRYPTO_INTERNAL;
    }

    if(M_E_NO_ERROR != (result = mc_RKEY_to_persist(self->crypt_ctx , self->key_mgr->rkeys[AGENT_PRIVATE_KEY] , g_persist_keys_name[AGENT_PRIVATE_KEY]))) {

        mc_RKEY_free(self->crypt_ctx , self->key_mgr->rkeys[AGENT_PUBLIC_KEY]);
        mc_RKEY_free(self->crypt_ctx , self->key_mgr->rkeys[AGENT_PRIVATE_KEY]);
        self->key_mgr->rkeys[AGENT_PUBLIC_KEY] = self->key_mgr->rkeys[AGENT_PRIVATE_KEY] = NULL;
        mc_RKEY_destroy(self->crypt_ctx , g_persist_keys_name[AGENT_PUBLIC_KEY]);
        MA_LOG(self->macrypto_ctx->logger, MA_LOG_SEV_ERROR,"Failed to save agent private key into keystore, error %d", result);        
        return MA_ERROR_CRYPTO_INTERNAL;
    }

    change_key_perm(self, g_persist_keys_name[AGENT_PUBLIC_KEY]);
    change_key_perm(self, g_persist_keys_name[AGENT_PRIVATE_KEY]);
    return MA_OK;
}

static ma_error_t ma_crypto_load_key(ma_crypto_t *self, ma_crypto_key_type_t type) {   

    if(NULL == ma_crypto_get_rkey(self, type)) {
        MA_LOG(self->macrypto_ctx->logger, MA_LOG_SEV_ERROR,"Failed to load the key %d", type);
        return MA_ERROR_UNEXPECTED;
    }
    else {
        M_ITEM *key_item = NULL ;
        int result = 0 ;
        unsigned char *buffer = NULL ;
        unsigned int len = 0 ;
        
        if(M_E_NO_ERROR != (result = mc_ITEM_new(&key_item))) {
            MA_LOG(self->macrypto_ctx->logger, MA_LOG_SEV_ERROR,"Failed to create new item,error %d", result);
            return MA_ERROR_OUTOFMEMORY ;
        }

        if(M_E_NO_ERROR != (result = mc_RKEY_get_info(self->crypt_ctx, self->key_mgr->rkeys[type], M_KEY_INFO_ID_KEY_VALUE, key_item))) {
            mc_ITEM_free(key_item);
            MA_LOG(self->macrypto_ctx->logger, MA_LOG_SEV_ERROR,"Failed to get key info for key type %d,error %d", type, result);
            return MA_ERROR_OUTOFMEMORY ;
        }

        if(M_E_NO_ERROR != (result = mc_ITEM_get_len(key_item , &len))) {
            mc_ITEM_free(key_item);
            MA_LOG(self->macrypto_ctx->logger, MA_LOG_SEV_ERROR,"Failed to get key len,error %d", result);
            return MA_ERROR_OUTOFMEMORY ;
        }

        /* ePO expects "RSA1" as prefix for keys*/
        len += sizeof(ma_uint32_t); //4 bytes for RSA1
        buffer = (unsigned char *)calloc(len, sizeof(unsigned char));
        if(!buffer) {
            mc_ITEM_free(key_item);
            MA_LOG(self->macrypto_ctx->logger, MA_LOG_SEV_ERROR,"Failed to alloc memory");
            return MA_ERROR_OUTOFMEMORY ;
        }

        memcpy(buffer, "RSA1", sizeof(ma_uint32_t));
        if(M_E_NO_ERROR != mc_ITEM_get_data(key_item,buffer+sizeof(ma_uint32_t))) {
            mc_ITEM_free(key_item);
            free(buffer);
            MA_LOG(self->macrypto_ctx->logger, MA_LOG_SEV_ERROR,"Failed to get key data");
            return MA_ERROR_CRYPTO_INTERNAL ;
        }
        
        mc_ITEM_free(key_item);                        
        
        if(MA_OK != ma_bytebuffer_create(len,&(self->key_mgr->keys[type]))) {
            free(buffer);
            MA_LOG(self->macrypto_ctx->logger, MA_LOG_SEV_ERROR,"Failed to create bytebuffer");
            return MA_ERROR_CRYPTO_INTERNAL ;
        }

		ma_bytebuffer_set_bytes(self->key_mgr->keys[type], 0, buffer,len);
        free(buffer);
        return MA_OK;
    }

}

static ma_error_t ma_crypto_load_key_hash(ma_crypto_t *self, ma_crypto_key_type_t type) {   
	ma_error_t rc = MA_OK;
	unsigned int len = 0 ;
	unsigned char *buffer = NULL;
    ma_crypto_hash_t *hash = NULL;
	
	/*Load the key first if it is not loaded*/
	if((!self->key_mgr->keys[type]) && (MA_OK != (rc = ma_crypto_load_key(self,type)))) {
		return rc ;
	}

	buffer = ma_bytebuffer_get_bytes(self->key_mgr->keys[type]);    
	len = ma_bytebuffer_get_size(self->key_mgr->keys[type]);
		        
    if(MA_OK != ma_crypto_hash_create(self, MA_CRYPTO_HASH_SHA256, &hash)) {
        MA_LOG(self->macrypto_ctx->logger, MA_LOG_SEV_ERROR,"Failed to create hash object");
        return MA_ERROR_CRYPTO_INTERNAL ;
    }

    if(MA_OK != ma_bytebuffer_create(len,&(self->key_mgr->keys_hash[type]))) {
        ma_crypto_hash_release(hash);
        MA_LOG(self->macrypto_ctx->logger, MA_LOG_SEV_ERROR,"Failed to create bytebuffer");
        return MA_ERROR_CRYPTO_INTERNAL ;
    }

	if(MA_OK != ma_crypto_hash_and_encode(hash, buffer, len, self->key_mgr->keys_hash[type])) {            
		ma_bytebuffer_release(self->key_mgr->keys_hash[type]);
        self->key_mgr->keys_hash[type] = NULL ;
        MA_LOG(self->macrypto_ctx->logger, MA_LOG_SEV_ERROR,"Failed to create hash for key");
        return MA_ERROR_CRYPTO_INTERNAL ;
    }
    ma_crypto_hash_release(hash);
   
	return MA_OK;
}


static ma_error_t load_symmetric_key(ma_crypto_t *self, ma_crypto_key_type_t type ) {
    ma_bytebuffer_t *buffer = NULL;
    ma_error_t rc = MA_ERROR_UNEXPECTED ;

    if(MA_OK != ma_bytebuffer_create(32,&buffer))
        return MA_ERROR_OUTOFMEMORY;
    
    if(MA_OK == (rc = (FIPS_SYMMETRIC_KEY == type) ? get_fips_symmetric_key(self, buffer) : get_legacy_symmetric_key(self,buffer))) {    
        M_ITEM *key_set_item = NULL ;
	    if(M_E_NO_ERROR == mc_ITEM_new(&key_set_item)) {
            if(M_E_NO_ERROR == mc_ITEM_set_data(key_set_item ,ma_bytebuffer_get_bytes(buffer) , ma_bytebuffer_get_size(buffer))) {
                rc = (M_E_NO_ERROR == mc_RKEY_sym_import(self->crypt_ctx , key_set_item , (FIPS_SYMMETRIC_KEY == type) ? MC_ALGID_SYMMETRIC_AES_128_ECB : MC_ALGID_SYMMETRIC_3DES, &self->key_mgr->rkeys[type]))
                     ? MA_OK : MA_ERROR_CRYPTO_INTERNAL ; 
            }
            mc_ITEM_free(key_set_item);
        }        
    }
    
    ma_bytebuffer_release(buffer);
    return rc ;
}


static void log_message_mfecryptc(int level,const char *fmt,...) {
    ma_log_severity_t severity = MA_LOG_SEV_ERROR ;
    switch(level) {
    case eError :
        severity = MA_LOG_SEV_ERROR;
        break;
    case eInfo :
        severity = MA_LOG_SEV_INFO;
        break;
    case eWarning :
        severity = MA_LOG_SEV_WARNING;
        break;
    case eDbug :
        severity = MA_LOG_SEV_DEBUG;
        break;
    default:
        break;
    }
    
    MA_LOG(g_logger,severity,fmt);
}
