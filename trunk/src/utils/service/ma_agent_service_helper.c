#include "ma/internal/utils/service/ma_agent_service_helper.h"

#if defined(MA_WINDOWS)
    #include <windows.h>
    #include <tchar.h>
#else 
    #if defined(LINUX)
        #include "ma/internal/utils/xml/ma_xml.h"
        #include "ma/internal/utils/xml/ma_xml_node.h"
        #include <unistd.h>
        #include <dirent.h>
    #endif
    #define MA_SERVICE_CONFIG_CONFIGURATION_NODE "Configuration"				//TBD: double check this 
    #define MA_SERVICE_CONFIG_STARTCOMMAND "StartCommand"						//TBD: double check this 
    #define MA_SERVICE_CONFIG_STOPCOMMAND "StopCommand"							//TBD: double check this 
#endif


#ifdef MA_WINDOWS
ma_error_t ma_agent_service_start(const char* service_name, ma_bool_t wait_till_starts){
    ma_error_t rc = MA_ERROR_APIFAILED;
    SC_HANDLE schSCManager;

    if(!service_name)
        return MA_ERROR_INVALIDARG;

    /* Get a handle to the SCM database. */
    if((schSCManager = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS))) {
        SC_HANDLE schService;
        /* Get a handle to the service.*/
        if((schService = OpenServiceA( schSCManager, service_name, SERVICE_START | SERVICE_QUERY_STATUS))) {
            SERVICE_STATUS ss = { 0 };
            
            StartServiceA(schService, 0, NULL);
            if(MA_TRUE == wait_till_starts) {
                do {
                    if(!QueryServiceStatus(schService, &ss))
                        break;
                    if(ss.dwCurrentState == SERVICE_START_PENDING) 
                        Sleep(100); /* Wait for another 100 msec */
                    else if(ss.dwCurrentState == SERVICE_RUNNING)
                        rc = MA_OK;
                } while (ss.dwCurrentState == SERVICE_START_PENDING);
            }
			else 
				rc = MA_OK;
            CloseServiceHandle(schService);
        }
        CloseServiceHandle(schSCManager);
    }
    return rc;
}

ma_error_t ma_agent_service_stop(const char* service_name, ma_bool_t wait_till_stops){
    ma_error_t rc = MA_ERROR_APIFAILED;
    SC_HANDLE schSCManager;

    if(!service_name)
        return MA_ERROR_INVALIDARG;

    /* Get a handle to the SCM database. */
    if((schSCManager = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS))) {
        SC_HANDLE schService;
        /* Get a handle to the service.*/
        if((schService = OpenServiceA( schSCManager, service_name, SERVICE_STOP | SERVICE_QUERY_STATUS))) {
            SERVICE_STATUS ss = { 0 };
            
            ControlService(schService, SERVICE_CONTROL_STOP, (LPSERVICE_STATUS) &ss);

            if(MA_TRUE == wait_till_stops) {
				unsigned int   uCount = 20;		/* 20 * 50 = 1000 ms --> wait for 1 sec for service terminaiton */
				DWORD	timeOut = 50;	/* Wait for 50 msec */            
                do {
                    if(!QueryServiceStatus(schService, &ss))
                        break;
                    if(ss.dwCurrentState == SERVICE_STOP_PENDING) {
                        Sleep(timeOut); 
						uCount--;
					}
                    else if(ss.dwCurrentState == SERVICE_STOPPED)
                        rc = MA_OK;
                } while (ss.dwCurrentState == SERVICE_STOP_PENDING && 0 != uCount);

				/* 1 sec timeout occured, then terminate the process */
				if(ss.dwCurrentState == SERVICE_STOP_PENDING && 0 == uCount) {
					TerminateProcess(schService, 0);
					rc = MA_OK;
				}
            }
			else {
				rc = MA_OK;
			}
            CloseServiceHandle(schService);
        }
        CloseServiceHandle(schSCManager);
    }
    return rc;
}

ma_error_t ma_agent_service_is_running(const char* service_name, ma_bool_t *is_running){
    ma_error_t rc = MA_ERROR_APIFAILED;
    SC_HANDLE schSCManager;

    if(!service_name || !is_running)
        return MA_ERROR_INVALIDARG;

    /* Get a handle to the SCM database. */
    if((schSCManager = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS))) {
        SC_HANDLE schService;
        /* Get a handle to the service.*/
        if((schService = OpenServiceA( schSCManager, service_name, SERVICE_QUERY_STATUS))) {
            SERVICE_STATUS ss = { 0 };
            if(QueryServiceStatus(schService, &ss)) {
                *is_running = MA_FALSE;
                rc = MA_OK;
                if(ss.dwCurrentState == SERVICE_RUNNING)
                    *is_running = MA_TRUE;
            }
            CloseServiceHandle(schService);
        }
        CloseServiceHandle(schSCManager);
    }
    return rc;
}

#else

/*TODO TODO TODO - this is not write running system, we have to be generic which daemaon we want to run and then call
    /etc/init.d/dameon.d file by uv_spwan ot something...*/

static ma_error_t service_config_path_get(char config_path[MAX_PATH_LEN]){
    ma_error_t rc = MA_ERROR_INVALIDARG;
    DIR *pdir ;
    struct dirent *pent ;
    struct stat stat_buffer ;
    char *config_dir = "/etc/ma.d/";
    if(0 == stat(config_dir, &stat_buffer))
    {
        //owned by the root and not writable by the non root
        if(stat_buffer.st_uid == 0 &&  ((stat_buffer.st_mode & S_IWOTH) != S_IWOTH) )
        {
            pdir = opendir(config_dir);
            if(pdir){
                while((pent = readdir(pdir))){
                    /*stat return -1 for valid directory. so chnaging logic. */
                    /*struct stat st;
                    if( -1 == stat(pent->d_name, &st)) continue;
                    if(!S_ISDIR(st.st_mode)) continue;*/
                    if((pent->d_type == DT_DIR) && (strcmp(pent->d_name ,MA_AGENT_SOFTWARE_ID) == NULL))
                    {
                        if( MAX_PATH_LEN > (strlen(config_dir) + strlen(pent->d_name) + strlen("/config.xml"))){
                            strcpy(config_path, config_dir);
                            strcat(config_path, pent->d_name);
                            strcat(config_path, "/config.xml");
                            rc = MA_OK;
                        }
                        closedir(pdir);
                        return rc;
                    }
                }
            }
        }
    }
    return rc;	
}

static ma_error_t execute_cmd(const char *cmd){
    if (-1 == system(cmd)){
        fprintf(stdout, "Can not start service, system(%s) failed.\n", cmd);
        return MA_ERROR_APIFAILED;
    }
    else{
        return MA_OK;
    }
}

static ma_error_t execute_service(const char *service_name){
    char config_path[MAX_PATH_LEN] = {0};
    ma_xml_t *service_config_xml = NULL;
    ma_xml_node_t *node = NULL;
    ma_xml_node_t *service_node = NULL;
    ma_xml_node_t *configuration_node = NULL;
    char *command = NULL;
    ma_error_t rc = MA_ERROR_INVALIDARG;
    if(MA_OK == (rc = ma_xml_create(&service_config_xml))){
        if(MA_OK !=(rc = service_config_path_get(config_path))){
            fprintf(stdout, "Can not start service, config path fetching failed.\n");
            return rc;
        }

        if(MA_OK == (rc = ma_xml_load_from_file(service_config_xml, config_path))){
            if(node = ma_xml_get_node(service_config_xml)) {
                if(configuration_node = ma_xml_node_search_by_path(node, MA_SERVICE_CONFIG_CONFIGURATION_NODE)){
                    if(service_node = ma_xml_node_search_by_path(configuration_node, service_name)){
                        if(command = ma_xml_node_get_data(service_node)){
                            rc = execute_cmd(command);
                        }
                    }
                }
            }
        }
        ma_xml_release(service_config_xml);
    }
    return rc;
}

ma_error_t ma_agent_service_start(const char *service_name, ma_bool_t wait_till_starts){
    if(!service_name)
        return MA_ERROR_INVALIDARG;

    return execute_service(MA_SERVICE_CONFIG_STARTCOMMAND);
}

ma_error_t ma_agent_service_stop(const char *service_name, ma_bool_t wait_till_stops){
    if(!service_name)
        return MA_ERROR_INVALIDARG;
    return execute_service(MA_SERVICE_CONFIG_STOPCOMMAND);
}

ma_error_t ma_agent_service_is_running(const char *service_name, ma_bool_t *is_running {
    if(!service_name || !is_running)
        return MA_ERROR_INVALIDARG;
    /*TODO - fix all of this , for sure this is not the right way */
    *is_running = MA_TRUE;
    return MA_OK;
}

#endif

