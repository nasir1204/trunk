
#include "ma/datastore/ma_ds_mapper.h"
#include "ma/internal/utils/datastructures/ma_queue.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"


#include <stdlib.h>
#include <string.h>

typedef struct ma_ds_mapper_list_entry_s {
    /*! set of datastore */ 
    ma_queue_t                 qlink;

    ma_ds_type_t                type;

    char                        *filter;

    ma_ds_t                     *datastore;
} ma_ds_mapper_list_entry_t;

struct ma_ds_mapper_s {
    /* ma_ds_mapper derivers from ma_ds_t . it should be the first member*/
    ma_ds_t                datastore_impl;

    ma_queue_t              qhead;
};

static ma_error_t ma_ds_mapper_set(ma_ds_t *datastore, const char *path, const char *key, ma_variant_t *value);
static ma_error_t ma_ds_mapper_get(ma_ds_t *datastore, const char *path, const char *key, ma_vartype_t type, ma_variant_t **value);
static ma_error_t ma_ds_mapper_remove(ma_ds_t *datastore, const char *path, const char *key);
static ma_error_t ma_ds_mapper_destroy(ma_ds_t *datastore );

static const ma_ds_methods_t methods = {
    &ma_ds_mapper_set,
    &ma_ds_mapper_get,    
    NULL,
    &ma_ds_mapper_remove,
    &ma_ds_mapper_destroy,
	NULL
};

ma_error_t ma_ds_mapper_create(ma_ds_mapper_t **mapper) {
    if(mapper) {
        ma_ds_mapper_t *self = (ma_ds_mapper_t *)calloc(1, sizeof(ma_ds_mapper_t));
        if(!self)   return MA_ERROR_OUTOFMEMORY;

        ma_queue_init(&self->qhead);

        ma_ds_init((ma_ds_t *)self, &methods, self);
        *mapper = self;         
        return MA_OK;        
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_ds_mapper_ds_add(ma_ds_mapper_t *self, ma_ds_type_t type,  ma_ds_t *datastore, const char *filter) {
    if(self && datastore ) {
        ma_ds_mapper_list_entry_t *item = (ma_ds_mapper_list_entry_t *) calloc(1, sizeof(ma_ds_mapper_list_entry_t));
        if (item) {
            item->datastore = datastore;
            if(filter) item->filter = strdup(filter);
            item->type = type;
            ma_queue_insert_tail(&self->qhead, &item->qlink);
            return MA_OK;
        }
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_ds_mapper_ds_remove(ma_ds_mapper_t *self, ma_ds_t *datastore) {
    if(self && datastore) {
        ma_queue_t  *item = NULL;        
        ma_queue_foreach(item, &self->qhead)  {
            ma_ds_mapper_list_entry_t *entry = ma_queue_data(item, ma_ds_mapper_list_entry_t, qlink);
            if (entry && (entry->datastore == datastore)) {
                ma_queue_remove(item);                                
                if(entry->filter) free(entry->filter);
                free(entry); 
                return MA_OK;
            }
        }
        return MA_OK;        
    }
     return MA_ERROR_INVALIDARG;
}

ma_error_t ma_ds_mapper_release(ma_ds_mapper_t *self) {
    if(self) {
        ma_ds_release(&self->datastore_impl);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static unsigned short int match_generic_filter(const ma_ds_mapper_list_entry_t *entry, char *path, char **new_path) {
    size_t len_mapper_prefix = MA_DS_MAPPER_PREFIX_LEN;    
    unsigned short int rc =  0;
    switch(entry->type) {
        case MA_DS_TYPE_REG :
            rc =  strncmp(path, MA_DS_MAPPER_PREFIX_REG, len_mapper_prefix)
                    ? 0 : 1;
            break;
        case MA_DS_TYPE_INI :
            rc =  strncmp(path, MA_DS_MAPPER_PREFIX_INI, len_mapper_prefix)
                    ? 0 : 1;
            break;
        case MA_DS_TYPE_DBH :
            rc = strncmp(path, MA_DS_MAPPER_PREFIX_DBH, len_mapper_prefix)
                    ? 0 : 1;
            break;
        default :
            rc = 0;
    }
    if(rc) 
		*new_path = path + len_mapper_prefix;
	return rc;
}

static ma_error_t ma_ds_mapper_set(ma_ds_t *datastore, const char *path, const char *key, ma_variant_t *value) {    
    ma_ds_mapper_t *self = (ma_ds_mapper_t *)datastore->data;         
    /* No need of parameter validation as it is coming from front end */
    ma_queue_t  *item = ma_queue_head(&self->qhead);
	while(item != ma_queue_sentinel(&self->qhead) && !ma_queue_empty(&self->qhead)) {            
		char *new_path = NULL ;
		ma_queue_t *next_item = ma_queue_next(item);

        ma_ds_mapper_list_entry_t *entry = ma_queue_data(next_item, ma_ds_mapper_list_entry_t, qlink);          
        if (entry && entry->datastore  && match_generic_filter(entry, (char *)path, &new_path)) {               
            return ma_ds_set(entry->datastore, new_path, key,  value);            
        }
		item = next_item;
	}
    return MA_ERROR_DS_NOT_FOUND;    
}

static ma_error_t ma_ds_mapper_get(ma_ds_t *datastore, const char *path, const char *key, ma_vartype_t type, ma_variant_t **value) {
    ma_ds_mapper_t *self = (ma_ds_mapper_t *)datastore->data;           
    /* No need of parameter validation as it is coming from front end */
 	ma_queue_t  *item = ma_queue_head(&self->qhead);
	while(item != ma_queue_sentinel(&self->qhead) && !ma_queue_empty(&self->qhead)){
		ma_queue_t *next_item = ma_queue_next(item);
        char *new_path = NULL;

		ma_ds_mapper_list_entry_t *entry = ma_queue_data(next_item, ma_ds_mapper_list_entry_t, qlink);            
        if (entry && entry->datastore && match_generic_filter(entry, (char*)path, &new_path)) {
            return ma_ds_get(entry->datastore, new_path, key, type, value);                   
        }
		item = next_item;
	}
    return MA_ERROR_DS_NOT_FOUND;    
}

static ma_error_t ma_ds_mapper_remove(ma_ds_t *datastore, const char *path, const char *key)  {
    ma_ds_mapper_t *self = (ma_ds_mapper_t *)datastore->data;     
    /* No need of parameter validation as it is coming from front end */
    ma_queue_t  *item = ma_queue_head(&self->qhead);
	while(item != ma_queue_sentinel(&self->qhead) && !ma_queue_empty(&self->qhead)) {
		ma_queue_t *next_item = ma_queue_next(item);
		char *new_path = NULL;
        
		ma_ds_mapper_list_entry_t *entry = ma_queue_data(next_item, ma_ds_mapper_list_entry_t, qlink);            
        if (entry && entry->datastore && match_generic_filter(entry, (char*)path, &new_path) ) {
            return ma_ds_remove(entry->datastore, new_path, key);            
        }
		item = next_item;
	}
    return MA_ERROR_DS_NOT_FOUND;    
}


static ma_error_t  ma_ds_mapper_destroy(ma_ds_t *datastore ) {
    ma_ds_mapper_t *self = (ma_ds_mapper_t *)datastore->data;       
    if(self) {
        while (!ma_queue_empty(&self->qhead)) {
            ma_queue_t *item = ma_queue_head(&self->qhead);
            ma_ds_mapper_list_entry_t *entry = ma_queue_data(item, ma_ds_mapper_list_entry_t, qlink);
            ma_queue_remove(item);            
            if(entry->filter) free(entry->filter);
            free(item);            
        }
        free(self) ;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

