#include "ma/datastore/ma_ds_ini.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/portability/fs.h"
#include "ma/internal/utils/serialization/ma_serialize.h"
#include "ma/internal/utils/serialization/ma_deserialize.h"


#include <stdlib.h>
#include <stdio.h>
#include <libini.h>
#include <sys/stat.h>

#ifdef MA_THREADING_ENABLED
    #include "ma/internal/utils/threading/ma_thread.h"
    #define MA_INI_LOCK_INIT(x) ma_mutex_init(&x->mutex)
    #define MA_INI_LOCK(x)   ma_mutex_lock(&x->mutex)
    #define MA_INI_UNLOCK(x) ma_mutex_unlock(&x->mutex)
    #define MA_INI_LOCK_DESTROY(x) ma_mutex_destroy(&x->mutex)
#else
    #define MA_INI_LOCK_INIT(x) /* NOP */
    #define MA_INI_LOCK(x)   /* NOP */
    #define MA_INI_UNLOCK(x) /* NOP */
    #define MA_INI_LOCK_DESTROY(x)/* NOP */
#endif

struct ma_ds_ini_s {
    ma_ds_t     db_impl;
#ifdef MA_THREADING_ENABLED
    ma_mutex_t  mutex;
#endif
    ini_fd_t    fd;
};

static ma_error_t ds_ini_set(ma_ds_t *datastore, const char *path, const char *key, ma_variant_t *value);
static ma_error_t ds_ini_get(ma_ds_t *datastore, const char *path, const char *key, ma_vartype_t type,  ma_variant_t **value);
static ma_error_t ds_ini_remove(ma_ds_t *datastore, const char *path, const char *key);
static ma_error_t ds_ini_release(ma_ds_t *datastore );

static const ma_ds_methods_t ini_methods = {
    &ds_ini_set,
    &ds_ini_get,
    NULL,
    &ds_ini_remove,
    &ds_ini_release
};

static const char *get_file_mode(const char *ini_file) {
   struct stat st = {0};
   return 0 == stat(ini_file, &st) ? "a":"w";
}

static ma_error_t ma_validate_ini_file_extension(const char *file) {
    if(file) {
        const char *extn = strstr(file, MA_INI_FILE_EXT);
        return extn?MA_OK:MA_ERROR_INVALIDARG;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t mkdirs(const char *path) {
    ma_error_t rc = MA_OK;
    int err;
    size_t dirname_buflen;
    char* dirname_buf;

    dirname_buflen = sizeof(char) * (strlen(path) + 1);
    dirname_buf = (char *)malloc(dirname_buflen);
    if (dirname_buf) {
        err = ma_dirname(path, dirname_buf, dirname_buflen);
        if (0 == err) {
            err = ma_mkpath(dirname_buf);
        }
        if (err) {
            /*
               Mapping errno.h to ma_error_t is rough, so use a generic error.
            */
            rc = MA_ERROR_INVALIDARG;
        }
        free(dirname_buf);
    } else {
        rc = MA_ERROR_OUTOFMEMORY;
    }

    return rc;
}

ma_error_t ma_ds_ini_open(const char *ini_file, int flag, ma_ds_ini_t **datastore) {
    if(ini_file && datastore) {
        ma_ds_ini_t *self = NULL ;
   		struct stat st = {0};

        /* if(MA_OK != ma_validate_ini_file_extension(ini_file)) 
            return MA_ERROR_INVALIDARG;*/
		
        self = (ma_ds_ini_t *)calloc(1,sizeof(ma_ds_ini_t));
        if(!self) return MA_ERROR_OUTOFMEMORY;

		if(0 != stat(ini_file, &st)){ 
            if (MA_OK != mkdirs(ini_file)) {
                free(self);
                return MA_ERROR_DS_INI_OPEN_FAILED;
			}
        }
        MA_INI_LOCK_INIT(self);
        MA_INI_LOCK(self);
        self->fd = ini_open(ini_file, flag ? "w" : get_file_mode(ini_file), NULL);
        MA_INI_UNLOCK(self);
        if(!self->fd) {
            free(self);
            return MA_ERROR_DS_INI_OPEN_FAILED;
        }
        ma_ds_init((ma_ds_t*)self,&ini_methods,self);
        *datastore = self;
        return MA_OK;
    }               
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_ds_ini_release(ma_ds_ini_t *self) {
    return self ? ma_ds_release((ma_ds_t *)self) : MA_ERROR_INVALIDARG;   
}



/* It creates the empty section if not found */
static ma_error_t ini_locate_section(ini_fd_t fd, const char *section) {    
    if(-1 == ini_locateHeading(fd, section)) {
        const char *tmp_key = "k";
        const char *tmp_value = "v";

        return (-1 == ini_locateKey(fd, tmp_key)) &&
               (0 == ini_writeString(fd, tmp_value)) &&
               (0 == ini_flush(fd)) &&
               (0 == ini_deleteKey(fd)) &&
               (0 == ini_flush(fd)) 
               ? MA_OK : MA_ERROR_DS_SET_FAILED;
    }
    return MA_OK;
} 


static ma_error_t ds_ini_set(ma_ds_t *datastore, const char *path, const char *key, ma_variant_t *value) {
    ma_ds_ini_t *self = (ma_ds_ini_t *)datastore;        
    ma_error_t rc = MA_ERROR_DS_SET_FAILED;
    
	MA_INI_LOCK(self);
    if(MA_OK == (rc = ini_locate_section(self->fd, path))) {
        size_t size = 0;        
        ma_vartype_t var_type = MA_VARTYPE_NULL;
		
        //No need to check
        (void)ini_locateKey(self->fd, key);
		
		(void)ma_variant_get_type(value,&var_type);
        switch(var_type) {
			case MA_VARTYPE_INT8 :
			case MA_VARTYPE_INT16 :
			case MA_VARTYPE_INT32 :
			case MA_VARTYPE_INT64: 
				{
					char temp[21] = {0};
					ma_int64_t i = 0 ;
                
					if(MA_OK == (rc = ma_variant_get_int64(value, &i))) {
						MA_MSC_SELECT(_snprintf, snprintf)(temp,21,"%lld",i);
						rc = (0 == ini_writeString(self->fd, temp)) ? MA_OK: MA_ERROR_DS_SET_FAILED;
					}
				}
                break;
			case MA_VARTYPE_UINT8 :			
			case MA_VARTYPE_UINT16 :			
			case MA_VARTYPE_UINT32 :            
			case MA_VARTYPE_UINT64:
				{
					char temp[21] = {0};
					ma_uint64_t i = 0 ;
                
					if(MA_OK == (rc = ma_variant_get_uint64(value, &i))) {
						MA_MSC_SELECT(_snprintf, snprintf)(temp,21,"%lld", (ma_int64_t)i);
						rc = (0 == ini_writeString(self->fd, temp)) ? MA_OK: MA_ERROR_DS_SET_FAILED;
					}
				}
                break;
			case MA_VARTYPE_BOOL: 
				{
					char temp[21] = {0};
					ma_bool_t i = 0 ;
                
					if(MA_OK == (rc = ma_variant_get_bool(value, &i))) {
						MA_MSC_SELECT(_snprintf, snprintf)(temp,21,"%lld", (ma_int64_t)i);
						rc = (0 == ini_writeString(self->fd, temp)) ? MA_OK: MA_ERROR_DS_SET_FAILED;
					}
				}
                break;            
            case MA_VARTYPE_STRING:{    
				ma_buffer_t *buffer = NULL;
				const char *p = NULL;
                
				if(MA_OK == (rc = ma_variant_get_string_buffer(value,&buffer))) {
					(void)ma_buffer_get_string(buffer,&p, &size);
					rc = (0 == ini_writeStream(self->fd, p, size)) ? MA_OK: MA_ERROR_DS_SET_FAILED;
					(void)ma_buffer_release(buffer);
				}
                break;
			}
			case MA_VARTYPE_RAW:{
				ma_buffer_t *buffer = NULL;
				const unsigned char *p = NULL;
                
				if(MA_OK == (rc = ma_variant_get_raw_buffer(value,&buffer))) {
					(void)ma_buffer_get_raw(buffer,&p, &size);
					rc = (0 ==  ini_writeStream(self->fd, p, size)) ? MA_OK: MA_ERROR_DS_SET_FAILED;
					(void)ma_buffer_release(buffer);
				}
                break;
			}
			case MA_VARTYPE_DOUBLE :
			case MA_VARTYPE_FLOAT :
			case MA_VARTYPE_TABLE :
			case MA_VARTYPE_ARRAY :{
				char *p = NULL;
				ma_serializer_t *serializer = NULL;
				
				if(MA_OK == (rc = ma_serializer_create(&serializer))) {
					if(MA_OK == (rc = ma_serializer_add_variant(serializer, value))) {
						if(MA_OK == (rc = ma_serializer_get(serializer, (char**)&p, &size))) {
							rc = (0 == ini_writeStream(self->fd, p, size)) ? MA_OK : MA_ERROR_DS_SET_FAILED;
						}
					}
					(void)ma_serializer_destroy(serializer);
				}
				
				break;
			}
			default:
				rc = MA_ERROR_INVALIDARG;
				break;
        }
        rc = ((MA_OK == rc)  && ( 0 == ini_flush(self->fd))) ? MA_OK : MA_ERROR_DS_SET_FAILED;
    }
    MA_INI_UNLOCK(self);
	return rc;
}



static ma_error_t ds_ini_get(ma_ds_t *datastore, const char *path, const char *key, ma_vartype_t type, ma_variant_t **value) {
    ma_ds_ini_t *self = (ma_ds_ini_t *)datastore;        
	ma_error_t rc = MA_ERROR_DS_PATH_NOT_FOUND ;
    char *data = NULL ;
    int data_len = -1 ;

    //TODO - do we need to lock for getting the value ???
    MA_INI_LOCK(self);
    if(0 == ini_locateHeading(self->fd, path)) {
        rc = MA_ERROR_DS_KEY_NOT_FOUND;
        if(0  == ini_locateKey(self->fd, key)) {
            rc = MA_ERROR_DS_GET_FAILED;
            data_len = ini_dataLength(self->fd);
            if(-1 != data_len) {
                data = (char *)calloc(data_len+1, sizeof(char));
                if( data_len == ini_readString(self->fd, data, data_len+1)) 
                    rc = MA_OK;
            }
        }        
    }
    MA_INI_UNLOCK(self);
        
    if(MA_OK == rc) {
        /* Now convert the value the way you want */
        switch(type) {
			case MA_VARTYPE_UINT8:
			case MA_VARTYPE_INT8:
			case MA_VARTYPE_BOOL: {
				rc = ma_variant_create_from_int8((ma_int8_t)(MA_MSC_SELECT(_strtoi64,strtoll)((char*)data,NULL,0)), value);
				break;
			}
			case MA_VARTYPE_UINT16:
			case MA_VARTYPE_INT16: {
				rc = ma_variant_create_from_int16((ma_int16_t)(MA_MSC_SELECT(_strtoi64,strtoll)((char*)data,NULL,0)) , value);
				break;
			}
			case MA_VARTYPE_UINT32:
			case MA_VARTYPE_INT32:{
				rc = ma_variant_create_from_int32((ma_int32_t)(MA_MSC_SELECT(_strtoi64,strtoll)((char*)data,NULL,0)) , value);
				break;
			}
			case MA_VARTYPE_UINT64:
            case MA_VARTYPE_INT64:{
				rc = ma_variant_create_from_int64(MA_MSC_SELECT(_strtoi64,strtoll)((char*)data,NULL,0) , value);
				break;
			}
            case MA_VARTYPE_STRING:{
				rc = ma_variant_create_from_string(data, data_len, value);
                break;
			}
            case MA_VARTYPE_RAW: {
				rc = ma_variant_create_from_raw(data, data_len,value);
				break;
			}
			default:{
				ma_deserializer_t *deserializer = NULL;
				if(MA_OK == (rc = ma_deserializer_create(&deserializer))) {
					if(MA_OK == (rc = ma_deserializer_put(deserializer, data, data_len))) {
						rc = ma_deserializer_get_next(deserializer,value);
					}
					(void)ma_deserializer_destroy(deserializer);
				}
				break;
			}
            
        }       
	}

	if(data) { free(data); data = NULL; }
    return rc ;           
 }

                       
static ma_error_t ini_remove_section(ma_ds_ini_t *self, const char *section) {
    ma_error_t rc = MA_ERROR_DS_PATH_NOT_FOUND;
    MA_INI_LOCK(self);
    if(0 == ini_locateHeading(self->fd, section)) {
        rc = ((0 == ini_deleteHeading(self->fd)) && 
             (0 == ini_flush(self->fd))) ? MA_OK : MA_ERROR_DS_REMOVE_FAILED;
    }
    MA_INI_UNLOCK(self);
    return rc;
}

static ma_error_t ini_remove_key(ma_ds_ini_t *self, const char *section, const char *key) {
    ma_error_t rc = MA_ERROR_DS_PATH_NOT_FOUND;
    MA_INI_LOCK(self);
    if(0 == ini_locateHeading(self->fd, section)) {
        if(0 == ini_locateKey(self->fd, key)){
            rc = ((0 == ini_deleteKey(self->fd)) && 
                  (0 == ini_flush(self->fd))) 
                 ? MA_OK : MA_ERROR_DS_REMOVE_FAILED;
        }
        else
            rc = MA_ERROR_DS_KEY_NOT_FOUND;
    }
    MA_INI_UNLOCK(self);
    return rc;
}

static ma_error_t ds_ini_remove(ma_ds_t *datastore, const char *path, const char *key) {
    ma_ds_ini_t *self = (ma_ds_ini_t *)datastore;        
    /* Remove the section itself or key*/
	return key ? ini_remove_key(self,path,key) : ini_remove_section(self, path);
}

static ma_error_t ds_ini_release(ma_ds_t *datastore) {
    if(datastore) {
        ma_ds_ini_t *self = (ma_ds_ini_t*)datastore;
        MA_INI_LOCK(self);
        ini_close(self->fd);
        MA_INI_UNLOCK(self);

        MA_INI_LOCK_DESTROY(self);
        free(self);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


