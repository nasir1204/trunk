#include "ma/datastore/ma_ds_registry.h"
#include "ma/datastore/ma_ds_iterator.h"
#include "ma/internal/utils/text/ma_utf8.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/serialization/ma_serialize.h"
#include "ma/internal/utils/serialization/ma_deserialize.h"



#ifdef MA_WINDOWS

#include <Windows.h>
#include <stdlib.h>

#ifdef _WIN64
#define MA_REG_CROSS_ACCESS	KEY_WOW64_32KEY
#else
#define MA_REG_CROSS_ACCESS	KEY_WOW64_64KEY
#endif

extern ma_error_t ma_registry_delete_tree(void* hKeyRoot, const char *path, ma_bool_t b_default_view);

struct ma_ds_registry_s {
    ma_ds_t datastore_impl;
    HKEY    hkey;
	ma_bool_t b_default_value;
};


typedef struct ma_ds_registry_iterator_s {
	ma_ds_iterator_t iterator; /*base class pointer*/
    ma_uint8_t          is_path_iterator;
    HKEY				hkey;
    unsigned long		index;
    unsigned long		n_sub_keys;
    unsigned long		max_sub_key_len;
    unsigned long		max_value_name_len;
}ma_ds_registry_iterator_t;

static ma_error_t ds_registry_set(ma_ds_t *datastore, const char *path, const char *key, ma_variant_t *value);
static ma_error_t ds_registry_get(ma_ds_t *datastore, const char *path, const char *key, ma_vartype_t type, ma_variant_t **value);
static ma_error_t ds_registry_remove(ma_ds_t *datastore, const char *path, const char *key);
static ma_error_t ds_registry_release(ma_ds_t *datastore );
static ma_error_t ds_registry_get_iterator(ma_ds_t *datastore, ma_uint8_t is_path_iterator, const char *path, ma_ds_iterator_t **iterator);
static ma_error_t ds_registry_is_exists(ma_ds_t *datastore, const char *path, const char *key, ma_bool_t *is_exists);

static ma_error_t ds_registry_iterator_get_next(ma_ds_iterator_t *iterator, ma_buffer_t **buffer);
static ma_error_t ds_registry_iterator_release(ma_ds_iterator_t *iterator);

static const ma_ds_methods_t methods = {
    &ds_registry_set,
    &ds_registry_get,
    &ds_registry_is_exists,
    &ds_registry_remove,
    &ds_registry_release,
	&ds_registry_get_iterator
};

static const ma_ds_iterator_methods_t it_methods = {
    &ds_registry_iterator_get_next,
    &ds_registry_iterator_release
};


ma_error_t ma_ds_registry_open(HKEY base, const char *root_path,ma_bool_t b_default_value, ma_bool_t create_if_not_exist, REGSAM access, ma_ds_registry_t **datastore) {
    if(datastore) {
        ma_ds_registry_t *self = (ma_ds_registry_t *) calloc(1 , sizeof(ma_ds_registry_t));
        if(!self) return MA_ERROR_OUTOFMEMORY;
        self->hkey = base;
		self->b_default_value = b_default_value;
        if(root_path) {
			ma_temp_buffer_t buffer = {0};
			int retcode = ERROR_SUCCESS;
			
			if(!b_default_value){
				access |= MA_REG_CROSS_ACCESS;
			}

			ma_temp_buffer_init(&buffer);
			if(MA_OK != ma_convert_utf8_to_wide_char(root_path,&buffer)) {
                free(self);
				return MA_ERROR_CONVERSION_FAILED;
            }

			if(ERROR_SUCCESS != (retcode = 
				(create_if_not_exist ? 
                                     RegCreateKeyExW(base, (ma_wchar_t*)ma_temp_buffer_get(&buffer), 0, NULL, REG_OPTION_NON_VOLATILE,access, NULL, &self->hkey, NULL) :
                                     RegOpenKeyExW(base, (ma_wchar_t *)ma_temp_buffer_get(&buffer), 0 , access, &self->hkey)))) {
                free(self);
				ma_temp_buffer_uninit(&buffer);
                return MA_ERROR_DS_REG_OPEN_FAILED;
            }
			ma_temp_buffer_uninit(&buffer);
        }
        ma_ds_init((ma_ds_t*)self,&methods,self);
        *datastore = self;        
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_ds_registry_release(ma_ds_registry_t *self) {
    return self ? ma_ds_release((ma_ds_t *)self) : MA_ERROR_INVALIDARG;
}


static ma_error_t write_to_registry(HKEY handle , ma_temp_buffer_t *key, ma_variant_t *value)  {    
    ma_error_t rc = MA_ERROR_DS_SET_FAILED ;
    ma_vartype_t var_type = MA_VARTYPE_NULL;
	size_t size = 0;
	
	ma_variant_get_type(value,&var_type);
	switch(var_type) {
			case MA_VARTYPE_INT8 :
			case MA_VARTYPE_INT16 :
			case MA_VARTYPE_INT32 :
			case MA_VARTYPE_INT64: 
				{
					ma_int64_t i = 0 ;
                
					if(MA_OK == (rc = ma_variant_get_int64(value, &i))) {
						rc = (ERROR_SUCCESS == RegSetValueExW(handle, (ma_wchar_t *)ma_temp_buffer_get(key), 0, REG_QWORD, (unsigned char*)&i, sizeof(ma_int64_t)))                        
						? MA_OK : MA_ERROR_DS_SET_FAILED;
					}
				}
                break;
			case MA_VARTYPE_UINT8 :			
			case MA_VARTYPE_UINT16 :			
			case MA_VARTYPE_UINT32 :            
			case MA_VARTYPE_UINT64:
				{
					ma_uint64_t i = 0 ;
                
					if(MA_OK == (rc = ma_variant_get_uint64(value, &i))) {
						ma_int64_t int_v = i;
						rc = (ERROR_SUCCESS == RegSetValueExW(handle, (ma_wchar_t *)ma_temp_buffer_get(key), 0, REG_QWORD, (unsigned char*)&int_v, sizeof(ma_int64_t)))                        
						? MA_OK : MA_ERROR_DS_SET_FAILED;
					}
				}
                break;
			case MA_VARTYPE_BOOL: 
				{
					ma_bool_t i = 0 ;
                
					if(MA_OK == (rc = ma_variant_get_bool(value, &i))) {
						ma_int64_t int_v = i;
						rc = (ERROR_SUCCESS == RegSetValueExW(handle, (ma_wchar_t *)ma_temp_buffer_get(key), 0, REG_QWORD, (unsigned char*)&int_v, sizeof(ma_int64_t)))                        
						? MA_OK : MA_ERROR_DS_SET_FAILED;
					}
				}
                break;       		
		case MA_VARTYPE_STRING: {
			ma_wbuffer_t *buffer = NULL;
			const ma_wchar_t *p = NULL;
                
			if(MA_OK == (rc = ma_variant_get_wstring_buffer(value, &buffer))) {
					(void)ma_wbuffer_get_string(buffer, &p, &size);
					rc = (ERROR_SUCCESS == RegSetValueExW(handle, (ma_wchar_t *)ma_temp_buffer_get(key), 0, REG_SZ, (unsigned char*)p, size * sizeof(ma_wchar_t)))
						? MA_OK : MA_ERROR_DS_SET_FAILED;
					(void)ma_wbuffer_release(buffer);
			}
			break;
		}
		case MA_VARTYPE_RAW: {
			ma_buffer_t *buffer = NULL;
			const unsigned char *p = NULL;
                
			if(MA_OK == (rc = ma_variant_get_raw_buffer(value,&buffer))) {
				(void)ma_buffer_get_raw(buffer, &p, &size);
				rc = (ERROR_SUCCESS == RegSetValueExW(handle, (ma_wchar_t *)ma_temp_buffer_get(key), 0, REG_BINARY, p, size))
						? MA_OK : MA_ERROR_DS_SET_FAILED;
				(void)ma_buffer_release(buffer);
			}
			break;
		}
		case MA_VARTYPE_DOUBLE :
		case MA_VARTYPE_FLOAT :
		case MA_VARTYPE_TABLE :
		case MA_VARTYPE_ARRAY : {
			char *p = NULL;
			ma_serializer_t *serializer = NULL;
				
			if(MA_OK == (rc = ma_serializer_create(&serializer))) {
				if(MA_OK == (rc = ma_serializer_add_variant(serializer, value))) {
					if(MA_OK == (rc = ma_serializer_get(serializer, (char**)&p, &size))) {
						rc = (ERROR_SUCCESS == RegSetValueExW(handle, (ma_wchar_t *)ma_temp_buffer_get(key), 0, REG_BINARY, (unsigned char*)p, size))
						? MA_OK : MA_ERROR_DS_SET_FAILED;
					}
				}
				ma_serializer_destroy(serializer);
			}	
			break;
		}
		default:
			rc = MA_ERROR_INVALIDARG;
	}
    return rc ;     
}

static ma_error_t ds_registry_set(ma_ds_t *datastore , const char *path, const char *key, ma_variant_t *value) {
    /* No need of parameter validation as it is coming from front end */        
    ma_ds_registry_t *self = (ma_ds_registry_t *)datastore;        
    HKEY reg_handle = NULL;        
    ma_error_t rc = MA_ERROR_DS_ADD_PATH_FAILED ;
    ma_temp_buffer_t buffer_path = {0};
	REGSAM access = KEY_ALL_ACCESS;
    
	ma_temp_buffer_init(&buffer_path);
	/* Convert the path into wide char */
    if(MA_OK != ma_convert_utf8_to_wide_char(path, &buffer_path)) 
        return MA_ERROR_CONVERSION_FAILED;

	if(!self->b_default_value) 
		access |= MA_REG_CROSS_ACCESS;
    /*Create path if not present */
    if(ERROR_SUCCESS != RegCreateKeyExW(self->hkey, (ma_wchar_t *)ma_temp_buffer_get(&buffer_path), 0, NULL, REG_OPTION_NON_VOLATILE, access, NULL, &reg_handle, NULL))
        rc =  MA_ERROR_DS_ADD_PATH_FAILED;
    else {
        ma_temp_buffer_t buffer_key = {0};          
		
		ma_temp_buffer_init(&buffer_key);
		/* Convert the key into wide char if required */
		if(MA_OK != ma_convert_utf8_to_wide_char(key,&buffer_key))
			rc = MA_ERROR_CONVERSION_FAILED;
        else 
			rc = write_to_registry(reg_handle, &buffer_key, value);
        
		ma_temp_buffer_uninit(&buffer_key);    
        RegCloseKey(reg_handle);
    }
       
    ma_temp_buffer_uninit(&buffer_path);
    return rc;    
}


static ma_error_t read_from_registry(HKEY handle , ma_temp_buffer_t *key, ma_vartype_t var_type, ma_variant_t **value)  { 
	ma_error_t rc = MA_ERROR_DS_PATH_NOT_FOUND ;
	unsigned long data_len = 0;
	unsigned long  type = 0;
	unsigned char *data = NULL;
    LSTATUS status ;
	/* Get the type and byte s required */
	if(ERROR_SUCCESS != (status = RegQueryValueExW(handle, (ma_wchar_t *)ma_temp_buffer_get(key), 0, &type, NULL, &data_len))) {
        DWORD ret = GetLastError();
		rc = MA_ERROR_DS_KEY_NOT_FOUND;
    }
	else {
			data = (unsigned char *)calloc(data_len + 1, sizeof(wchar_t));
			if(!data) rc = MA_ERROR_OUTOFMEMORY;
			else {
				rc = (ERROR_SUCCESS == RegQueryValueExW(handle, (wchar_t *)ma_temp_buffer_get(key), 0, &type, data, &data_len))
					 ? MA_OK : MA_ERROR_DS_GET_FAILED;
			}
		
	}
	
	if(MA_OK == rc) {

	 /* Now convert the value the way you want */
        switch(var_type) {
			case MA_VARTYPE_UINT8:
			case MA_VARTYPE_INT8:
			case MA_VARTYPE_BOOL: {
				rc = ma_variant_create_from_int8(*((ma_int8_t*)data), value);
				break;
			}
			case MA_VARTYPE_UINT16:
			 case MA_VARTYPE_INT16:{
				rc = ma_variant_create_from_int16(*((ma_int16_t*)data), value);
				break;
			}
			case MA_VARTYPE_UINT32 :
			case MA_VARTYPE_INT32:{
				rc = ma_variant_create_from_int32(*((ma_int32_t*)data), value);
				break;
			}
			case MA_VARTYPE_UINT64:
            case MA_VARTYPE_INT64:{
				rc = ma_variant_create_from_int64(*((ma_int64_t*)data), value);
				break;
			}
            case MA_VARTYPE_STRING:{
				rc = ma_variant_create_from_wstring((ma_wchar_t*)data, data_len, value);
                break;
			}
            case MA_VARTYPE_RAW: {
				rc = ma_variant_create_from_raw(data, data_len,value);
				break;
			}
			default:{
				ma_deserializer_t *deserializer = NULL;
				if(MA_OK == (rc = ma_deserializer_create(&deserializer))) {
					if(MA_OK == (rc = ma_deserializer_put(deserializer, (const char*)data, data_len))) {
						rc = ma_deserializer_get_next(deserializer,value);
					}
					ma_deserializer_destroy(deserializer);
				}
				break;
			}
            
        }
	}
	
	if(data) free(data);
	return rc;
}

static ma_error_t ds_registry_get(ma_ds_t *datastore , const char *path, const char *key, ma_vartype_t type, ma_variant_t **value) {
    /* No need of parameter validation as it is coming from front end */    
    ma_ds_registry_t *self = (ma_ds_registry_t *)datastore;
    HKEY reg_handle = NULL;        
    ma_error_t rc = MA_ERROR_DS_PATH_NOT_FOUND ;
    ma_temp_buffer_t buffer_path = {0};
    
	ma_temp_buffer_init(&buffer_path);
	/* Convert the path into wide char*/
    if(MA_OK != ma_convert_utf8_to_wide_char(path, &buffer_path)) 
        return MA_ERROR_CONVERSION_FAILED;
    
    if(ERROR_SUCCESS != RegOpenKeyExW(self->hkey, (ma_wchar_t *)ma_temp_buffer_get(&buffer_path), 0 ,KEY_READ, &reg_handle)) 
        rc =  MA_ERROR_DS_PATH_NOT_FOUND;
    else {
        ma_temp_buffer_t buffer_key = {0};          
		
		ma_temp_buffer_init(&buffer_key);
		/* Convert the key into wide char */
		if(MA_OK != ma_convert_utf8_to_wide_char(key,&buffer_key))
			rc = MA_ERROR_CONVERSION_FAILED;
		else
			rc = read_from_registry(reg_handle, &buffer_key, type, value);
		
		ma_temp_buffer_uninit(&buffer_key);    
        RegCloseKey(reg_handle);
		
    }
    ma_temp_buffer_uninit(&buffer_path);
    return rc;
}

static ma_error_t ds_registry_remove(ma_ds_t *datastore , const char *path, const char *key)  {
    /* No need of parameter validation as it is coming from front end */    
    ma_ds_registry_t *self = (ma_ds_registry_t *)datastore;
    HKEY reg_handle = NULL;        
    ma_error_t rc = MA_ERROR_DS_REMOVE_FAILED ;
    ma_temp_buffer_t buffer_path = {0};
    
	ma_temp_buffer_init(&buffer_path);
	/* Convert the path into wide char*/
    if(MA_OK != ma_convert_utf8_to_wide_char(path, &buffer_path)) 
        return MA_ERROR_CONVERSION_FAILED;

    if(!key){ 
		if(MA_OK == ma_registry_delete_tree(self->hkey, path, self->b_default_value))
            rc = MA_OK;
    }
    else {
        if(ERROR_SUCCESS != RegOpenKeyExW(self->hkey, (ma_wchar_t*)ma_temp_buffer_get(&buffer_path), 0 ,KEY_WRITE, &reg_handle)) 
            rc =  MA_ERROR_DS_PATH_NOT_FOUND;
        else {
            ma_temp_buffer_t buffer_key = {0};          
		
			ma_temp_buffer_init(&buffer_key);
			/* Convert the key into wide char */
			if(MA_OK != ma_convert_utf8_to_wide_char(key,&buffer_key))
				rc = MA_ERROR_CONVERSION_FAILED;
            else 
				rc = (ERROR_SUCCESS == RegDeleteValueW(reg_handle,(ma_wchar_t*)ma_temp_buffer_get(&buffer_key))) ? MA_OK: MA_ERROR_DS_REMOVE_FAILED;                
			
			ma_temp_buffer_uninit(&buffer_key);
            RegCloseKey(reg_handle);
        }
    }
    ma_temp_buffer_uninit(&buffer_path);
    return rc;
}

static ma_error_t ds_registry_path_exists(ma_ds_registry_t *self, const char *path, ma_bool_t *is_exists) {
    ma_error_t rc = MA_OK ;
    ma_temp_buffer_t buffer_path = {0} ;
    HKEY reg_handle = NULL ;  
    *is_exists = MA_FALSE ;

    ma_temp_buffer_init(&buffer_path) ;
    if(MA_OK != ma_convert_utf8_to_wide_char(path, &buffer_path)) 
        return MA_ERROR_CONVERSION_FAILED ;

    if(ERROR_SUCCESS != RegOpenKeyExW(self->hkey, (ma_wchar_t *)ma_temp_buffer_get(&buffer_path), 0 ,KEY_READ, &reg_handle))
        rc =  MA_ERROR_DS_PATH_NOT_FOUND ;
    else {
        *is_exists = MA_TRUE ;
        RegCloseKey(reg_handle) ;
    }
    ma_temp_buffer_uninit(&buffer_path) ;
    return rc ;
}

static ma_error_t ds_registry_key_exists(ma_ds_registry_t *self, const char *path, const char *key, ma_bool_t *is_exists) {
    ma_error_t rc = MA_OK ;
    ma_temp_buffer_t buffer_path = {0} ;
    HKEY reg_handle = NULL ;  
    *is_exists = MA_FALSE ;

    ma_temp_buffer_init(&buffer_path) ;
    if(MA_OK != ma_convert_utf8_to_wide_char(path, &buffer_path)) 
        return MA_ERROR_CONVERSION_FAILED ;

    if(ERROR_SUCCESS != RegOpenKeyExW(self->hkey, (ma_wchar_t *)ma_temp_buffer_get(&buffer_path), 0 ,KEY_READ, &reg_handle))
        rc =  MA_ERROR_DS_KEY_NOT_FOUND ;
    else {
        unsigned long data_len = 0;
	    unsigned long  type = 0;
	    unsigned char *data = NULL ;
        ma_temp_buffer_t key_buffer = {0} ;

        ma_temp_buffer_init(&key_buffer) ;
        ma_convert_utf8_to_wide_char(key, &key_buffer) ;
    	if(ERROR_SUCCESS != RegQueryValueExW(reg_handle, (ma_wchar_t *)ma_temp_buffer_get(&key_buffer), 0, &type, NULL, &data_len))
	    	rc = MA_ERROR_DS_KEY_NOT_FOUND;
		else
			*is_exists = MA_TRUE;
        RegCloseKey(reg_handle) ;
    }
    ma_temp_buffer_uninit(&buffer_path) ;
    return rc ;
}

static ma_error_t ds_registry_is_exists(ma_ds_t *datastore, const char *path, const char *key, ma_bool_t *is_exists) {
    ma_ds_registry_t *self = (ma_ds_registry_t *) datastore ;
    return key ? ds_registry_key_exists(self, path, key, is_exists) :
                 ds_registry_path_exists(self, path, is_exists);
}


static ma_error_t ds_registry_release(ma_ds_t *datastore ) {
        /* No need of parameter validation as it is coming from front end */    
    ma_ds_registry_t *self = (ma_ds_registry_t *)datastore;       
    if(self) {
        if(self->hkey)
            RegCloseKey(self->hkey);
        free(self);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t ds_registry_get_iterator(ma_ds_t *datastore, ma_uint8_t is_path_iterator, const char *path, ma_ds_iterator_t **iterator) {
    /* No need of parameter validation as it is coming from front end */        
    ma_ds_registry_t *ds = (ma_ds_registry_t *)datastore;
    ma_temp_buffer_t buffer_path = {0};
    ma_ds_registry_iterator_t *it =  (ma_ds_registry_iterator_t *)calloc(1,sizeof(ma_ds_registry_iterator_t)); 
    unsigned long result = ERROR_SUCCESS;

    if(!it) return MA_ERROR_OUTOFMEMORY;
    
    ma_temp_buffer_init(&buffer_path);
	/* Convert the path into wide char*/
    if(MA_OK != ma_convert_utf8_to_wide_char(path, &buffer_path)) {
        free(it);
        return MA_ERROR_CONVERSION_FAILED;
    }

    if(ERROR_SUCCESS != RegOpenKeyExW(ds->hkey, (ma_wchar_t*)ma_temp_buffer_get(&buffer_path), 0 ,KEY_ENUMERATE_SUB_KEYS | KEY_QUERY_VALUE, &it->hkey)) {
        free(it);
        ma_temp_buffer_uninit(&buffer_path);
        return MA_ERROR_DS_PATH_NOT_FOUND;
    }
    ma_temp_buffer_uninit(&buffer_path);
    if((ERROR_SUCCESS != (result = RegQueryInfoKeyW(it->hkey,NULL,NULL,NULL,&it->n_sub_keys,&it->max_sub_key_len,NULL,NULL,&it->max_value_name_len,NULL,NULL,NULL)))) {
        RegCloseKey(it->hkey);
        free(it);
        ma_temp_buffer_uninit(&buffer_path);
        return MA_ERROR_DS_PATH_NOT_FOUND;
    }
    it->is_path_iterator = is_path_iterator;
    ma_ds_iterator_init((ma_ds_iterator_t*)it, &it_methods,it);
    *iterator = (ma_ds_iterator_t *)it;

    return MA_OK;
}

static ma_error_t ds_registry_iterator_get_next(ma_ds_iterator_t *iterator, ma_buffer_t **buffer) {
    /*No need of parameter validation as it will come from front end API*/
    ma_ds_registry_iterator_t *self = (ma_ds_registry_iterator_t *)iterator;
    ma_temp_buffer_t temp_buffer = {0};
    unsigned long len = 0 ;
    unsigned long result = ERROR_SUCCESS;

    ma_temp_buffer_init(&temp_buffer);
    if(self->is_path_iterator) {
        len = self->max_sub_key_len + 1;/*1 for string terminator*/
        ma_temp_buffer_reserve(&temp_buffer, len);
        result = RegEnumKeyExA(self->hkey,self->index,(char*)ma_temp_buffer_get(&temp_buffer),&len,NULL,NULL,NULL,NULL);
    }
    else {
        len = self->max_value_name_len + 1;/*1 for string terminator*/
        ma_temp_buffer_reserve(&temp_buffer, len);
        result = RegEnumValueA(self->hkey,self->index,(char*)ma_temp_buffer_get(&temp_buffer),&len,NULL,NULL,NULL,NULL);
    }

    if(ERROR_SUCCESS == result) {
        (void)ma_buffer_create(buffer,len);
        (void)ma_buffer_set(*buffer,(char*)ma_temp_buffer_get(&temp_buffer),len);
        self->index++;
        ma_temp_buffer_uninit(&temp_buffer);
        return MA_OK;
    }
    ma_temp_buffer_uninit(&temp_buffer);
    return (ERROR_NO_MORE_ITEMS == result) ? MA_ERROR_DS_NO_MORE_ITEMS : MA_ERROR_UNEXPECTED;
}

static ma_error_t ds_registry_iterator_release(ma_ds_iterator_t *iterator) {
    /*No need of parameter validation as it will come from front end API*/
    ma_ds_registry_iterator_t *self = (ma_ds_registry_iterator_t *)iterator;
    if(self) {
        RegCloseKey(self->hkey);
        free(self);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}
#endif /* MA_WINDOWS */


