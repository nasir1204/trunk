#include "ma/datastore/ma_ds_xml.h"
#include "ma/datastore/ma_ds_iterator.h"
#include "ma/internal/utils/xml/ma_xml.h"
#include "ma/internal/utils/xml/ma_xml_node.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include "ma/internal/ma_macros.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


struct ma_ds_xml_s {
    ma_ds_t datastore_impl ;
    char    *xml_file_name ;
    ma_xml_t *xml_obj ;
} ;

typedef struct ma_ds_xml_iterator_s {
    ma_ds_iterator_t iterator ;
    ma_ds_xml_t      *ds_xml ;
    ma_uint8_t       is_path_iterator ;
    ma_xml_node_t    *path_node ;
    ma_xml_node_t    *child_node ;
    ma_int32_t      no_child_nodes ;
} ma_ds_xml_iterator_t ;

static ma_error_t ds_xml_set(ma_ds_t *datastore, const char *path, const char *key, ma_variant_t *value) ;
static ma_error_t ds_xml_get(ma_ds_t *datastore, const char *path, const char *key, ma_vartype_t type, ma_variant_t **value) ;
static ma_error_t ds_xml_remove(ma_ds_t *datastore, const char *path, const char *key) ;
static ma_error_t ds_xml_release(ma_ds_t *datastore ) ;

static ma_error_t ds_xml_get_iterator(ma_ds_t *datastore, ma_uint8_t is_path_iterator, const char *path, ma_ds_iterator_t **iterator) ;
static ma_error_t ds_xml_iterator_get_next(ma_ds_iterator_t *iterator, ma_buffer_t **buffer) ;
static ma_error_t ds_xml_iterator_release(ma_ds_iterator_t *iterator) ;

static const ma_ds_methods_t methods = {
    &ds_xml_set,
    &ds_xml_get,
    NULL,
    &ds_xml_remove,
    &ds_xml_release,
    &ds_xml_get_iterator
};

static const ma_ds_iterator_methods_t iter_methods = {
    &ds_xml_iterator_get_next,
    &ds_xml_iterator_release
};

static void get_xmlpath_from_dspath(const char *ds_path, char **xml_path)
{
    char *new_path = strdup(ds_path) ;
    int i = 0;

    for( i= 0 ; new_path[i] !='\0'; i++) {
        if(new_path[i] == MA_DATASTORE_PATH_C_SEPARATOR)
            new_path[i] = '/';
    }
    *xml_path = new_path ;
}

static ma_error_t get_node_data_from_variant(ma_variant_t *value, char **node_data) {
    ma_vartype_t var_type = MA_VARTYPE_NULL ;
    char *data = NULL ;
    ma_temp_buffer_t data_buffer = {0} ;
    ma_error_t rc = MA_OK ;

    ma_temp_buffer_init(&data_buffer) ;
    (void)ma_variant_get_type(value, &var_type) ;
    switch(var_type)
    {
        case MA_VARTYPE_INT8 :
		case MA_VARTYPE_INT16 :
		case MA_VARTYPE_INT32 :
		case MA_VARTYPE_INT64 :
			{
                ma_int64_t i64 = 0 ;
                ma_temp_buffer_reserve(&data_buffer, 21) ;
                (void)ma_variant_get_int64(value, &i64) ;
                MA_MSC_SELECT(_snprintf, snprintf)(data = (char*)ma_temp_buffer_get(&data_buffer), ma_temp_buffer_capacity(&data_buffer), "%lld", i64) ;                
            }
			break ;
        case MA_VARTYPE_UINT8 :        
        case MA_VARTYPE_UINT16 :        
        case MA_VARTYPE_UINT32 :        
        case MA_VARTYPE_UINT64 :
			{
                ma_uint64_t i64 = 0 ;
                ma_temp_buffer_reserve(&data_buffer, 21) ;
                (void)ma_variant_get_uint64(value, &i64) ;
                MA_MSC_SELECT(_snprintf, snprintf)(data = (char*)ma_temp_buffer_get(&data_buffer), ma_temp_buffer_capacity(&data_buffer), "%lld", (ma_int64_t)i64) ;                
            }
			break ;
        case MA_VARTYPE_BOOL : 
			{
                ma_bool_t bool_v = MA_FALSE;
                ma_temp_buffer_reserve(&data_buffer, 21) ;
                (void)ma_variant_get_bool(value, &bool_v) ;
				MA_MSC_SELECT(_snprintf, snprintf)(data = (char*)ma_temp_buffer_get(&data_buffer), ma_temp_buffer_capacity(&data_buffer), "%lld", (ma_int64_t)bool_v) ;                
            }
			break ;
        case MA_VARTYPE_STRING : {
                ma_buffer_t *buffer = NULL ;
                const char *p = NULL ;
                size_t size = 0 ;
                (void)ma_variant_get_string_buffer(value, &buffer) ;
                (void)ma_buffer_get_string(buffer, &p, &size) ;
                ma_temp_buffer_reserve(&data_buffer, size) ;
                MA_MSC_SELECT(_snprintf, snprintf)(data = (char*)ma_temp_buffer_get(&data_buffer), ma_temp_buffer_capacity(&data_buffer), "%s", p) ;
                (void)ma_buffer_release(buffer);
                break ;
            }
        case MA_VARTYPE_RAW : {
                ma_buffer_t *buffer = NULL ;
                const unsigned char *p = NULL ;
                size_t size = 0 ;
                (void)ma_variant_get_raw_buffer(value, &buffer) ;
                (void)ma_buffer_get_raw(buffer, &p, &size) ;
                ma_temp_buffer_reserve(&data_buffer, size) ;
                MA_MSC_SELECT(_snprintf, snprintf)(data = (char*)ma_temp_buffer_get(&data_buffer), ma_temp_buffer_capacity(&data_buffer), "%s", p) ;
                (void)ma_buffer_release(buffer);
                break ;
            }
        default :
            rc = MA_ERROR_INVALID_OPTION ;
    }
    if(MA_OK == rc) {
        *node_data = (char *)calloc(1, strlen(data)+1) ;
        if(*node_data) 
            strncpy(*node_data, data, strlen(data)) ;
        else 
            rc = MA_ERROR_OUTOFMEMORY ;
    }
    ma_temp_buffer_uninit(&data_buffer) ;
    return rc ;
}
ma_error_t ds_xml_set(ma_ds_t *datastore, const char *path, const char *key, ma_variant_t *value) {
    if(datastore && path && key && value) {
        ma_vartype_t var_type = MA_VARTYPE_NULL ;

        (void)ma_variant_get_type(value, &var_type) ;
        if(var_type != MA_VARTYPE_TABLE && var_type != MA_VARTYPE_ARRAY)
        {
            ma_ds_xml_t *self = (ma_ds_xml_t *)datastore ;
            ma_error_t rc = MA_OK ;
            ma_xml_node_t *path_node = NULL ;
            ma_temp_buffer_t new_path_buffer= {0} ;
            char *new_path = NULL , *xml_path = NULL ;
            ma_xml_node_t *new_node = NULL ;

            get_xmlpath_from_dspath(path, &xml_path) ;
            ma_temp_buffer_init(&new_path_buffer) ;
            ma_temp_buffer_reserve(&new_path_buffer, strlen(path)+strlen(MA_DATASTORE_PATH_SEPARATOR)+strlen(key)) ;
            MA_MSC_SELECT(_snprintf, snprintf)(new_path = (char*)ma_temp_buffer_get(&new_path_buffer), ma_temp_buffer_capacity(&new_path_buffer), "%s%c%s", xml_path, '/', key) ;

          if(NULL == (path_node = ma_xml_node_search_by_path(ma_xml_get_node(self->xml_obj), new_path))) {
                if(NULL == (path_node = ma_xml_node_search_by_path(ma_xml_get_node(self->xml_obj), xml_path))) {
                    char *temp_path = strdup(xml_path);
                    char *token = strtok(temp_path, "/") ;
                    ma_xml_node_t *parent_node = ma_xml_get_node(self->xml_obj) ;
                    while(token != NULL) {
                        if(NULL == (path_node = ma_xml_node_get_child_by_name(parent_node, token))) {
                          if(MA_OK == (rc = ma_xml_node_create(parent_node, token, &new_node)))
                                parent_node = new_node ;
                        }
                        else {
                            parent_node = path_node ;
                        }
                        token = strtok(NULL, "/") ;
                    }
                    path_node = parent_node ;
                    free(temp_path);
                }
                rc = ma_xml_node_create(path_node, key, &new_node) ;
                path_node = new_node ;
            }
            if(MA_OK == rc) {
                char *node_value = NULL ;
                if(MA_OK == (rc = get_node_data_from_variant(value , &node_value))) {
                    ma_xml_node_set_data(path_node, node_value) ;
                    rc = ma_xml_save_to_file(self->xml_obj, self->xml_file_name) ;
                    free(node_value) ;
                }
            }
            ma_temp_buffer_uninit(&new_path_buffer) ;
            if(xml_path) free(xml_path) ;
           return rc ;
        }
        return MA_ERROR_INVALID_OPTION ;
    }
    return MA_ERROR_INVALIDARG ;
}

static ma_error_t construct_variant_from_node_data(ma_vartype_t type, const char *data, ma_variant_t **value) {
    ma_error_t rc = MA_OK ;
    switch(type)
    {
        case MA_VARTYPE_UINT8:
        case MA_VARTYPE_INT8:
        case MA_VARTYPE_BOOL:
            rc = ma_variant_create_from_int8((ma_int8_t)(MA_MSC_SELECT(_strtoi64,strtoll)(data,NULL,0)), value) ;
            break ;
        case MA_VARTYPE_UINT16:
            case MA_VARTYPE_INT16:
            rc = ma_variant_create_from_int16((ma_int16_t)(MA_MSC_SELECT(_strtoi64,strtoll)(data,NULL,0)), value) ;
            break ;
        case MA_VARTYPE_UINT32 :
        case MA_VARTYPE_INT32:
            rc = ma_variant_create_from_int32((ma_int32_t)(MA_MSC_SELECT(_strtoi64,strtoll)(data,NULL,0)), value) ;
            break ;
        case MA_VARTYPE_UINT64:
        case MA_VARTYPE_INT64:
            rc = ma_variant_create_from_int64((ma_int64_t)(MA_MSC_SELECT(_strtoi64,strtoll)(data,NULL,0)), value) ;
            break ;
        case MA_VARTYPE_STRING:
            rc = ma_variant_create_from_string(data, strlen(data), value) ;
            break ;
        case MA_VARTYPE_RAW:
            rc = ma_variant_create_from_raw(data, strlen(data), value) ;
            break ;
        default :
            rc = MA_ERROR_INVALID_OPTION ;
            break ;
    }
    return rc ;
}

ma_error_t ds_xml_get(ma_ds_t *datastore, const char *path, const char *key, ma_vartype_t type, ma_variant_t **value) {
    if(datastore && path && key && value && type != MA_VARTYPE_TABLE && type != MA_VARTYPE_ARRAY)
    {
        ma_ds_xml_t *self = (ma_ds_xml_t *)datastore ;
        ma_error_t rc = MA_ERROR_DS_GET_FAILED ;
        ma_xml_node_t *path_node = NULL ;
        char *new_path = NULL, *xml_path = NULL ;
        ma_temp_buffer_t new_path_buffer= {0} ;
        ma_xml_node_t *root = NULL ;

        get_xmlpath_from_dspath(path, &xml_path) ;
        ma_temp_buffer_init(&new_path_buffer) ;
        ma_temp_buffer_reserve(&new_path_buffer, strlen(path)+strlen(MA_DATASTORE_PATH_SEPARATOR)+strlen(key)) ;
        MA_MSC_SELECT(_snprintf, snprintf)(new_path = (char*)ma_temp_buffer_get(&new_path_buffer), ma_temp_buffer_capacity(&new_path_buffer), "%s%c%s", xml_path, '/', key) ;
        root = ma_xml_get_node(self->xml_obj) ;
        if(NULL != (path_node = ma_xml_node_search_by_path(root, new_path))) {
            if(ma_xml_node_has_data(path_node)) {
                rc = construct_variant_from_node_data(type, ma_xml_node_get_data(path_node), value) ;
            }
        }
        else
            rc = MA_ERROR_DS_KEY_NOT_FOUND ;
        free(xml_path) ;
        ma_temp_buffer_uninit(&new_path_buffer) ;
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ds_xml_remove(ma_ds_t *datastore, const char *path, const char *key)
{
    if(datastore && path)
    {
        ma_ds_xml_t *self = (ma_ds_xml_t *)datastore ;
        ma_error_t rc = MA_OK ;
        ma_xml_node_t *path_node = NULL ;
        char *new_path = NULL, *xml_path = NULL ;
        ma_temp_buffer_t new_path_buffer= {0} ;

        get_xmlpath_from_dspath(path, &xml_path) ;
        ma_temp_buffer_init(&new_path_buffer) ;
        if(key) {
            ma_temp_buffer_reserve(&new_path_buffer, strlen(path)+strlen(MA_DATASTORE_PATH_SEPARATOR)+strlen(key)) ;
            MA_MSC_SELECT(_snprintf, snprintf)(new_path = (char*)ma_temp_buffer_get(&new_path_buffer), ma_temp_buffer_capacity(&new_path_buffer), "%s%c%s", xml_path, '/', key) ;
            path_node = ma_xml_node_search_by_path(ma_xml_get_node(self->xml_obj), new_path) ;
        }
        else {
            path_node = ma_xml_node_search_by_path(ma_xml_get_node(self->xml_obj), xml_path) ;
        }
        
        if(path_node && MA_OK == rc) {
            if(MA_OK == (rc = ma_xml_node_delete(path_node))) {
                rc = ma_xml_node_release(path_node) ;
                ma_xml_save_to_file(self->xml_obj, self->xml_file_name) ;
            }
        }
        free(xml_path) ;
        ma_temp_buffer_uninit(&new_path_buffer) ;
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}
ma_error_t ds_xml_release(ma_ds_t *datastore ) {
    if(datastore)
    {
        ma_ds_xml_t *self = (ma_ds_xml_t *)datastore ;
        free(self->xml_file_name) ;
		(void)ma_xml_release(self->xml_obj) ;
		free(self) ;
		self = NULL ;
		return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_ds_xml_open(const char *xml_file, ma_ds_xml_t **datastore) {
    if(xml_file && strlen(xml_file) > 0 && datastore) {
        ma_ds_xml_t *self = (ma_ds_xml_t *)calloc(1, sizeof(ma_ds_xml_t)) ;
        ma_error_t rc = MA_OK ;
        if(self) {
            if(MA_OK == (rc = ma_xml_create(&self->xml_obj))) {
                if(MA_ERROR_XML_FILE_NOT_FOUND == (rc = ma_xml_load_from_file(self->xml_obj, xml_file))) {
                    if(MA_OK == (rc = ma_xml_construct(self->xml_obj)))
                        rc = ma_xml_save_to_file(self->xml_obj, xml_file) ;
                }
                if(MA_OK == rc) {
                    self->xml_file_name = strdup(xml_file) ;
                    self->datastore_impl.methods = &methods ;
                    *datastore = self ;
                    return rc ;
                }
                (void)ma_xml_release(self->xml_obj) ;
            }
            free(self) ;
            return rc ;
        }
        return MA_ERROR_OUTOFMEMORY ;
    }
    return MA_ERROR_INVALIDARG ;
}


ma_error_t ds_xml_get_iterator(ma_ds_t *datastore, ma_uint8_t is_path_iterator, const char *path, ma_ds_iterator_t **iterator)
{
    if(datastore && path && iterator) {
        ma_ds_xml_t *self = (ma_ds_xml_t *)datastore ;
        ma_xml_node_t *path_node = NULL ;
        char *xml_path = NULL ;
        ma_error_t rc = MA_OK ;

        get_xmlpath_from_dspath(path, &xml_path) ;
        if(NULL == (path_node = ma_xml_node_search_by_path(ma_xml_get_node(self->xml_obj), xml_path))) {
            rc = MA_ERROR_DS_PATH_NOT_FOUND ;
        }
        else {
            ma_ds_xml_iterator_t *iter = (ma_ds_xml_iterator_t *)calloc(1, sizeof(ma_ds_xml_iterator_t)) ;
            if(iter) {
                iter->iterator.methods = &iter_methods ;
                iter->ds_xml = self ;
                iter->is_path_iterator = is_path_iterator ;
                iter->path_node = path_node ;
                iter->child_node = NULL ;
                iter->no_child_nodes = ma_xml_node_get_child_count(path_node) ;
                *iterator = (ma_ds_iterator_t *)iter ;
            }
            else    rc = MA_ERROR_OUTOFMEMORY ;
        }
        free(xml_path) ;
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ds_xml_iterator_get_next(ma_ds_iterator_t *iterator, ma_buffer_t **buffer)
{
    ma_ds_xml_iterator_t *self = (ma_ds_xml_iterator_t *)iterator ;

    if(self->no_child_nodes > 0) {
        if(!self->child_node )
            self->child_node = ma_xml_node_get_first_child(self->path_node) ;
        else
            self->child_node = ma_xml_node_get_next_sibling(self->child_node) ;
        if(self->child_node) {
            ma_error_t rc = MA_OK ;
            //if(ma_xml_node_has_data(self->child_node)) {
                ma_buffer_t *node_buffer = NULL ;
                char *data = (char *)ma_xml_node_get_name(self->child_node) ;
                if(MA_OK == (rc = ma_buffer_create(&node_buffer, strlen(data)))) {
                    (void)ma_buffer_set(node_buffer, data, strlen(data)) ;
                    *buffer = node_buffer ;
                }
            //}
            return rc ;
        }
    }
    return MA_ERROR_NO_MORE_ITEMS ;
}
ma_error_t ds_xml_iterator_release(ma_ds_iterator_t *iterator) {
    ma_ds_xml_iterator_t *self = (ma_ds_xml_iterator_t *)iterator ;
    self->no_child_nodes = 0 ;
    self->ds_xml = NULL ;
    self->path_node = NULL ;
    self->child_node = NULL ;
    free(self) ; self = NULL ;
    return MA_OK ;
}

