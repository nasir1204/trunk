#include "ma/datastore/ma_ds_database.h"
#include "ma/datastore/ma_ds_iterator.h"
#include "ma/internal/utils/database/ma_db_statement.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include "ma/internal/ma_macros.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


struct ma_ds_database_s {
    ma_ds_t         datastore_impl;    
    char            *db_instance_name;    
    ma_db_t         *db_ptr;    
};

typedef struct ma_ds_database_iterator_s {
	ma_ds_iterator_t    iterator; /*base class pointer*/
    ma_db_statement_t   *db_stmt;
    ma_db_recordset_t    *recordset;
}ma_ds_database_iterator_t;



static ma_error_t ds_db_database_set(ma_ds_t *datastore, const char *path, const char *key, ma_variant_t *value);
static ma_error_t ds_db_database_get(ma_ds_t *datastore, const char *path, const char *key, ma_vartype_t type, ma_variant_t **value);
static ma_error_t ds_db_database_is_exists(ma_ds_t *datastore, const char *path, const char *key, ma_bool_t *is_exists);
static ma_error_t ds_db_database_remove(ma_ds_t *datastore, const char *path, const char *key);
static ma_error_t ds_db_database_release(ma_ds_t *datastore );

static ma_error_t ds_db_database_get_iterator(ma_ds_t *datastore, ma_uint8_t is_path_iterator, const char *path, ma_ds_iterator_t **iterator);

static const ma_ds_methods_t methods = {
    &ds_db_database_set,
    &ds_db_database_get,
    &ds_db_database_is_exists,
    &ds_db_database_remove,
    &ds_db_database_release,
	&ds_db_database_get_iterator
};

static ma_error_t db_iterator_get_next(ma_ds_iterator_t *self, ma_buffer_t **buffer);
static ma_error_t db_iterator_release(ma_ds_iterator_t *self);

static const ma_ds_iterator_methods_t it_methods = {
    &db_iterator_get_next,
    &db_iterator_release
};


static ma_error_t db_create_instance(ma_db_t *database, const char *db_instance_name);

ma_error_t ma_ds_database_create(ma_db_t *database, const char *db_instance_name, ma_ds_database_t **datastore) {
    //TODO -strnlen
    if(database && db_instance_name && (strlen(db_instance_name) > 0 ) && datastore) {
        ma_error_t rc = MA_OK;
        ma_ds_database_t *self = (ma_ds_database_t *) calloc(1, sizeof(ma_ds_database_t));
        if(!self)   return MA_ERROR_OUTOFMEMORY;

        self->db_ptr = database;
        if(MA_OK != (rc = db_create_instance(self->db_ptr, db_instance_name))) {
            free(self); return rc;
        }
        self->db_instance_name = strdup(db_instance_name);        
        ma_ds_init((ma_ds_t *)self, &methods, self);
        *datastore = self;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_ds_database_transaction_begin(ma_ds_database_t *self) {
    return self ? ma_db_transaction_begin(self->db_ptr) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_ds_database_transaction_cancel(ma_ds_database_t *self) {
    return self ? ma_db_transaction_cancel(self->db_ptr) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_ds_database_transaction_end(ma_ds_database_t *self) {
    return self ? ma_db_transaction_end(self->db_ptr) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_ds_database_release(ma_ds_database_t *self) {    
    return self ? ma_ds_release((ma_ds_t*)self) : MA_ERROR_INVALIDARG;
}

ma_db_t *ma_ds_database_get_dbhandle(ma_ds_database_t *self) {
    return self ? self->db_ptr : NULL ;
}


static ma_error_t ds_db_database_release(ma_ds_t *datastore) {    
    ma_ds_database_t *self = (ma_ds_database_t *)datastore;
    if(self) {        
        if(self->db_instance_name) free(self->db_instance_name);
        free(self);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t db_get_max_path_id(ma_db_t *db, const char *instance_name , int *path_id);
static ma_error_t db_get_path_id(ma_db_t *db, const char *instance_name, const char *path, size_t path_len, int *path_id);
static ma_error_t db_add_parent_record(ma_db_t *db, const char *instance_name, int path_id, const char *path, size_t path_len);
static ma_error_t db_add_child_record(ma_db_t *db, const char *instance_name, int path_id, const char *key,  ma_variant_t *value);
static void get_new_path_with_separator_at_last(const char *path,size_t *path_len, ma_temp_buffer_t *buffer) ;

static ma_error_t ds_db_database_set(ma_ds_t *datastore, const char *path, const char *key, ma_variant_t *value) { 
    if(datastore && path && key && value) {
        ma_ds_database_t *self = (ma_ds_database_t *)datastore;
        int path_id = -1;
        ma_error_t rc = MA_OK;
        size_t path_len = 0;
        ma_temp_buffer_t new_path_buffer= {0};
        char *new_path = NULL;

        ma_temp_buffer_init(&new_path_buffer);
        get_new_path_with_separator_at_last(path, &path_len, &new_path_buffer);
        new_path = (char*)ma_temp_buffer_get(&new_path_buffer);

        /*get the path id*/
        if(MA_OK != (rc = db_get_path_id(self->db_ptr, self->db_instance_name, new_path, path_len, &path_id))) {        
            /*if path  is not in database, create the path with max(ID)+1 */
            if(MA_OK == (rc = db_get_max_path_id(self->db_ptr, self->db_instance_name, &path_id)))
                /*add the parent data (path) */
                rc = db_add_parent_record(self->db_ptr, self->db_instance_name, path_id,  new_path, path_len);          
        }        
        
        /*Stil no property id then return path not found */
        if(MA_OK != rc) 
	        rc = MA_ERROR_DS_ADD_PATH_FAILED;
        else {
            /*Add the child data if evertyhing okay */
            rc =  db_add_child_record(self->db_ptr, self->db_instance_name, path_id, key , value);
        }
        ma_temp_buffer_uninit(&new_path_buffer);
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t db_get_child_record(ma_db_t *db, const char *instance_name, int path_id, const char *key, ma_vartype_t type, ma_variant_t **value);

static ma_error_t ds_db_database_get(ma_ds_t *datastore, const char *path, const char *key, ma_vartype_t type, ma_variant_t **value) {
    if(datastore && path && key && value) {
        ma_ds_database_t *self = (ma_ds_database_t *)datastore;
        int path_id = -1;
        size_t path_len = 0;
        ma_error_t rc = MA_ERROR_DS_PATH_NOT_FOUND;
        ma_temp_buffer_t new_path_buffer= {0};
        char *new_path = NULL;
        
        ma_temp_buffer_init(&new_path_buffer);
        get_new_path_with_separator_at_last(path, &path_len, &new_path_buffer);
        new_path = (char*)ma_temp_buffer_get(&new_path_buffer);

        
        /*get the path id */
        if(MA_OK == (rc = db_get_path_id(self->db_ptr, self->db_instance_name, new_path, path_len, &path_id))) 
            rc = db_get_child_record(self->db_ptr, self->db_instance_name, path_id, key, type, value);
      
         ma_temp_buffer_uninit(&new_path_buffer);
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t db_prepare_path_id_iterator(ma_db_t *db, const char *instance_name, const char *path, size_t path_len, ma_ds_database_iterator_t *it) ;
static ma_error_t db_remove_child_record(ma_db_t *db, const char *instance_name, int path_id, const char *key);
static ma_error_t db_remove_child_records(ma_db_t *db, const char *instance_name, int path_id);

static ma_error_t ds_db_database_remove(ma_ds_t *datastore, const char *path, const char *key)  {
    if(datastore && path) {
        ma_ds_database_t *self = (ma_ds_database_t *)datastore;
        int path_id = -1;        
        size_t path_len = 0;
        ma_error_t rc = MA_ERROR_DS_PATH_NOT_FOUND;
        ma_temp_buffer_t new_path_buffer= {0};
        char *new_path = NULL;
        
        ma_temp_buffer_init(&new_path_buffer);
        get_new_path_with_separator_at_last(path, &path_len, &new_path_buffer);
        new_path = (char*)ma_temp_buffer_get(&new_path_buffer);

        if(key) {
            if(MA_OK == (rc = db_get_path_id(self->db_ptr, self->db_instance_name, new_path, path_len, &path_id)))
                rc = db_remove_child_record(self->db_ptr, self->db_instance_name, path_id, key) ;
        }
        else {
            ma_ds_database_iterator_t *pathid_iter = (ma_ds_database_iterator_t *)calloc(1,sizeof(ma_ds_database_iterator_t));
        	if(!pathid_iter) return MA_ERROR_OUTOFMEMORY;
            /* get all the path id's which are hierarchically down the current path */
            if(MA_OK == (rc = db_prepare_path_id_iterator(self->db_ptr, self->db_instance_name, new_path, strlen(new_path), pathid_iter))) {
                ma_buffer_t *buffer = NULL ;
                const char *value = NULL ;
                size_t len = 0 ;
                while(MA_ERROR_NO_MORE_ITEMS != (rc = db_iterator_get_next((ma_ds_iterator_t *)pathid_iter, &buffer))) {
                    ma_buffer_get_string(buffer, &value, &len) ;
                    if(MA_OK != (rc = db_remove_child_records(self->db_ptr, self->db_instance_name, atoi(value)))) {
						ma_buffer_release(buffer);
                        break ;
					}
					ma_buffer_release(buffer);
                }
            }
            db_iterator_release((ma_ds_iterator_t *)pathid_iter) ;
            if(MA_ERROR_NO_MORE_ITEMS == rc) 
                rc = MA_OK ;
        }
        ma_temp_buffer_uninit(&new_path_buffer);
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}


static ma_error_t db_prepare_path_iterator(ma_db_t *db, const char *instance_name, const char *path, size_t path_len, ma_ds_database_iterator_t *it);
static ma_error_t db_prepare_child_iterator(ma_db_t *db, const char *instance_name, const char *path, size_t path_len, ma_ds_database_iterator_t *it);

static ma_error_t ds_db_database_get_iterator(ma_ds_t *datastore, ma_uint8_t is_path_iterator, const char *path, ma_ds_iterator_t **iterator) {
    if(datastore && path && iterator) {
        ma_ds_database_t *self = (ma_ds_database_t *)datastore; 
        ma_error_t rc = MA_OK;
        size_t path_len = 0 ;
        ma_temp_buffer_t new_path_buffer= {0};
        char *new_path = NULL;
        ma_ds_database_iterator_t *it = (ma_ds_database_iterator_t *)calloc(1,sizeof(ma_ds_database_iterator_t));
          
		if(!it) return MA_ERROR_OUTOFMEMORY;

        ma_temp_buffer_init(&new_path_buffer);
        get_new_path_with_separator_at_last(path, &path_len, &new_path_buffer);
        new_path = (char*)ma_temp_buffer_get(&new_path_buffer);
        
        
        rc = is_path_iterator ? db_prepare_path_iterator(self->db_ptr,self->db_instance_name,new_path,path_len, it)
                              : db_prepare_child_iterator(self->db_ptr,self->db_instance_name,new_path,path_len,it);
		if(MA_OK != rc) {
			free(it);
            ma_temp_buffer_uninit(&new_path_buffer);
			return rc;
		}
        ma_temp_buffer_uninit(&new_path_buffer);
        ma_ds_iterator_init((ma_ds_iterator_t*)it, &it_methods,it);
		*iterator = (ma_ds_iterator_t *)it;

		return MA_OK;
    }    
	return MA_ERROR_INVALIDARG;
}



#define CREATE_PARENT_TABLE_SQL_STATEMENT "CREATE TABLE IF NOT EXISTS %s_PARENT (ID INTEGER NOT NULL UNIQUE, PATH TEXT NOT NULL UNIQUE, PRIMARY KEY (ID) ON CONFLICT REPLACE )"
#define CREATE_CHILD_TABLE_SQL_STATEMENT  "CREATE TABLE IF NOT EXISTS %s_CHILD (ID INTEGER NOT NULL, NAME TEXT NOT NULL, VALUE BLOB NOT NULL, PRIMARY KEY (ID, NAME) ON CONFLICT REPLACE , FOREIGN KEY (ID) REFERENCES %s_PARENT(ID)  ON UPDATE CASCADE ON DELETE CASCADE )"
ma_error_t  db_create_instance(ma_db_t *db, const char *instance_name) {
    ma_temp_buffer_t buffer = {0};
    char *p = NULL;
    ma_db_statement_t *stmt = NULL;
    ma_error_t rc = MA_OK;
    
    ma_temp_buffer_init(&buffer);
    ma_temp_buffer_reserve(&buffer, strlen(CREATE_PARENT_TABLE_SQL_STATEMENT) + strlen(instance_name));        
    MA_MSC_SELECT(_snprintf, snprintf)(p = (char*)ma_temp_buffer_get(&buffer), ma_temp_buffer_capacity(&buffer), CREATE_PARENT_TABLE_SQL_STATEMENT, instance_name);
    /* Create parent table */
    if(MA_OK == (rc = ma_db_statement_create(db, p, &stmt))) {
        if(MA_OK == (rc = ma_db_statement_execute(stmt))) {
            /*Create child table */
            ma_db_statement_t *child_stmt = NULL;
            ma_temp_buffer_t child_buffer = {0};

            ma_temp_buffer_init(&child_buffer);
            ma_temp_buffer_reserve(&buffer, strlen(CREATE_CHILD_TABLE_SQL_STATEMENT) + strlen(instance_name) * 2);        
            MA_MSC_SELECT(_snprintf, snprintf)(p = (char*)ma_temp_buffer_get(&child_buffer), ma_temp_buffer_capacity(&child_buffer), CREATE_CHILD_TABLE_SQL_STATEMENT, instance_name, instance_name);
            
            if(MA_OK == (rc = ma_db_statement_create(db, p, &child_stmt))) {
                rc = ma_db_statement_execute(child_stmt);
                ma_db_statement_release(child_stmt);
            }
            ma_temp_buffer_uninit(&child_buffer);
        }
        ma_db_statement_release(stmt);
    }
    ma_temp_buffer_uninit(&buffer);
    return rc;
     
}


#define DROP_PARENT_TABLE_SQL_STATEMENT "DROP TABLE %s_PARENT "
#define DROP_CHILD_TABLE_SQL_STATEMENT  "DROP TABLE %s_CHILD "
ma_error_t ma_ds_database_delete_instance(ma_ds_database_t *self) {
    if(self) {
        ma_temp_buffer_t buffer = {0 };        
        ma_error_t rc = MA_ERROR_DS_DB_DELETE_INSTANCE_FAILED;
        char *pbuffer = NULL;
        ma_db_statement_t *parent_stmt = NULL;

        ma_temp_buffer_init(&buffer); 
        ma_temp_buffer_reserve(&buffer, strlen(DROP_PARENT_TABLE_SQL_STATEMENT) + strlen(self->db_instance_name));
        MA_MSC_SELECT(_snprintf, snprintf)(pbuffer = (char*)ma_temp_buffer_get(&buffer), ma_temp_buffer_capacity(&buffer), DROP_PARENT_TABLE_SQL_STATEMENT, self->db_instance_name);
        if(MA_OK == (rc = ma_db_statement_create(self->db_ptr, pbuffer, &parent_stmt))) {
            if(MA_OK == (rc = ma_db_statement_execute(parent_stmt))) {
                ma_db_statement_t *child_stmt = NULL;
                ma_temp_buffer_t child_buffer = {0};

                ma_temp_buffer_init(&child_buffer);
                ma_temp_buffer_reserve(&child_buffer, strlen(DROP_CHILD_TABLE_SQL_STATEMENT) + strlen(self->db_instance_name));
                MA_MSC_SELECT(_snprintf, snprintf)(pbuffer = (char*)ma_temp_buffer_get(&child_buffer), ma_temp_buffer_capacity(&child_buffer), DROP_CHILD_TABLE_SQL_STATEMENT, self->db_instance_name);
                
                if(MA_OK == (rc = ma_db_statement_create(self->db_ptr, pbuffer,&child_stmt))) {
                    rc = ma_db_statement_execute(child_stmt);
                    ma_db_statement_release(child_stmt);
                }
                ma_temp_buffer_uninit(&child_buffer);
            }
            ma_db_statement_release(parent_stmt);
        }
        ma_temp_buffer_uninit(&buffer);
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}


#define GET_PATH_ID_SQL_STATEMENT   "SELECT ID FROM %s_PARENT WHERE PATH = ?"
static ma_error_t  db_get_path_id(ma_db_t *db, const char *instance_name, const char *path, size_t path_len, int *path_id){
    ma_temp_buffer_t buffer = {0};
    char *pbuffer = NULL;
    ma_db_statement_t *stmt = NULL;
    ma_error_t rc = MA_OK;


    ma_temp_buffer_init(&buffer);       
    ma_temp_buffer_reserve(&buffer, strlen(GET_PATH_ID_SQL_STATEMENT) + strlen(instance_name) );        
    MA_MSC_SELECT(_snprintf, snprintf)(pbuffer = (char*)ma_temp_buffer_get(&buffer), ma_temp_buffer_capacity(&buffer), GET_PATH_ID_SQL_STATEMENT, instance_name);

    if(MA_OK == (rc = ma_db_statement_create(db, pbuffer, &stmt))) {
        if(MA_OK == (rc = ma_db_statement_set_string(stmt,1,1,path,path_len))) {
            ma_db_recordset_t *rs = NULL;
            if(MA_OK == (rc = ma_db_statement_execute_query(stmt, &rs))) {
                if(MA_OK == (rc = ma_db_recordset_next(rs))) 
                    rc = ma_db_recordset_get_int(rs,1,path_id);
                ma_db_recordset_release(rs);
            }
        }
        ma_db_statement_release(stmt);
    }    
    ma_temp_buffer_uninit(&buffer);
    
    return (MA_ERROR_NO_MORE_ITEMS == rc) ? MA_ERROR_DS_PATH_NOT_FOUND : rc;
}



#define ADD_PARENT_RECORD_SQL_STATEMENT   "INSERT INTO %s_PARENT VALUES (?, ?)"
static ma_error_t db_add_parent_record(ma_db_t *db, const char *instance_name, int path_id, const char *path, size_t path_len) {
    ma_temp_buffer_t buffer = {0};        
    ma_error_t rc = MA_ERROR_DS_SET_FAILED;
    char *pbuffer = NULL;
    ma_db_statement_t *stmt = NULL;

    ma_temp_buffer_init(&buffer);
    ma_temp_buffer_reserve(&buffer, strlen(ADD_PARENT_RECORD_SQL_STATEMENT) + strlen(instance_name) ); 
    MA_MSC_SELECT(_snprintf, snprintf)(pbuffer = (char*)ma_temp_buffer_get(&buffer), ma_temp_buffer_capacity(&buffer), ADD_PARENT_RECORD_SQL_STATEMENT, instance_name);

    if(MA_OK == (rc = ma_db_statement_create(db,pbuffer,&stmt))) {
        if(MA_OK == (rc = ma_db_statement_set_int(stmt,1,path_id))) {
            if(MA_OK == (rc = ma_db_statement_set_string(stmt,2,1,path,path_len))) {
                rc = ma_db_statement_execute(stmt);
            }
        }
        ma_db_statement_release(stmt);
    }
    return rc ;
}


#define GET_MAX_PATH_ID_SQL_STATEMENT    "SELECT MAX(ID) FROM %s_PARENT"
static ma_error_t db_get_max_path_id(ma_db_t *db, const char *instance_name , int *path_id) {
    ma_temp_buffer_t buffer = {0};
    char *pbuffer = NULL;
    ma_db_statement_t *stmt = NULL;
    ma_error_t rc = MA_OK;


    ma_temp_buffer_init(&buffer);       
    ma_temp_buffer_reserve(&buffer, strlen(GET_MAX_PATH_ID_SQL_STATEMENT) + strlen(instance_name) );        
    MA_MSC_SELECT(_snprintf, snprintf)(pbuffer = (char*)ma_temp_buffer_get(&buffer), ma_temp_buffer_capacity(&buffer), GET_MAX_PATH_ID_SQL_STATEMENT, instance_name);

    if(MA_OK == (rc = ma_db_statement_create(db, pbuffer, &stmt))) {
        ma_db_recordset_t *rs = NULL;
        if(MA_OK == (rc = ma_db_statement_execute_query(stmt, &rs))) {
            if(MA_OK == (rc = ma_db_recordset_next(rs))) {
                if(MA_OK == (rc = ma_db_recordset_get_int(rs,1,path_id)))
                    *path_id = *path_id + 1;
            }
            else if (MA_ERROR_NO_MORE_ITEMS == rc) {
                *path_id = 1;
                rc = MA_OK;
            }
            ma_db_recordset_release(rs);
        }
        ma_db_statement_release(stmt);
    }    
    ma_temp_buffer_uninit(&buffer);
    return rc;
}


#define ADD_CHILD_RECORDS_SQL_STATEMENT    "INSERT INTO %s_CHILD VALUES (?,?,?)"

static ma_error_t db_add_child_record(ma_db_t *db, const char *instance_name, int path_id, const char *key,  ma_variant_t *value)  {    
    ma_error_t rc = MA_ERROR_DS_SET_FAILED;
    ma_temp_buffer_t temp_buffer = {0 };        
    char *p = NULL;
    ma_db_statement_t *stmt = NULL;

            
    ma_temp_buffer_init(&temp_buffer);
    ma_temp_buffer_reserve(&temp_buffer, strlen(ADD_CHILD_RECORDS_SQL_STATEMENT) + strlen(instance_name) ); 
    MA_MSC_SELECT(_snprintf, snprintf)(p = (char*)ma_temp_buffer_get(&temp_buffer), ma_temp_buffer_capacity(&temp_buffer), ADD_CHILD_RECORDS_SQL_STATEMENT, instance_name);

    if(MA_OK == (rc = ma_db_statement_create(db, p, &stmt))) {
        if(MA_OK == (rc = ma_db_statement_set_int(stmt,1,path_id))) {
            if(MA_OK == (rc = ma_db_statement_set_string(stmt, 2, 1,key, strlen(key)))) {
                if(MA_OK == (rc = ma_db_statement_set_variant(stmt, 3 , value))) {
                    rc = ma_db_statement_execute(stmt);
                }
            }
        }
        ma_db_statement_release(stmt);
    }
    ma_temp_buffer_uninit(&temp_buffer);             
    return rc;
}

#define GET_CHILD_RECORD_SQL_STATEMENT    "SELECT VALUE FROM %s_CHILD WHERE ID = ? AND NAME = ?"
static ma_error_t db_get_child_record(ma_db_t *db, const char *instance_name, int path_id, const char *key, ma_vartype_t type, ma_variant_t **value) {   
    ma_error_t rc = MA_ERROR_DS_GET_FAILED;
	ma_temp_buffer_t temp_buffer = {0 };
	char *p = NULL;
    ma_db_statement_t *stmt = NULL;     

	ma_temp_buffer_init(&temp_buffer);    
	ma_temp_buffer_reserve(&temp_buffer, strlen(GET_CHILD_RECORD_SQL_STATEMENT) +  strlen(instance_name) ); 
    MA_MSC_SELECT(_snprintf, snprintf)(p = (char*)ma_temp_buffer_get(&temp_buffer), ma_temp_buffer_capacity(&temp_buffer), GET_CHILD_RECORD_SQL_STATEMENT, instance_name);

    if(MA_OK == (rc = ma_db_statement_create(db, p, &stmt))) {
        if(MA_OK == (rc = ma_db_statement_set_int(stmt, 1, path_id))) {
            if(MA_OK == (rc = ma_db_statement_set_string(stmt, 2,1, key, strlen(key)))) {
                ma_db_recordset_t *rs = NULL;
                if(MA_OK == (rc = ma_db_statement_execute_query(stmt,&rs))) {
                    if(MA_OK == (rc = ma_db_recordset_next(rs)))
                        rc = ma_db_recordset_get_variant(rs,1,type,value);
                    ma_db_recordset_release(rs);
                }
            }
        }
        ma_db_statement_release(stmt);
    }
	ma_temp_buffer_uninit(&temp_buffer);    
    return (MA_ERROR_NO_MORE_ITEMS == rc) ? MA_ERROR_DS_KEY_NOT_FOUND : rc;
}


#define DELETE_CHILD_RECORDS_SQL_STATEMENT "DELETE FROM %s_CHILD WHERE ID = %d"
#define DELETE_PARENT_RECORD_SQL_STATEMENT "DELETE FROM %s_PARENT WHERE ID = %d"
static ma_error_t db_remove_child_records(ma_db_t *db, const char *instance_name, int path_id) {
    ma_temp_buffer_t child_buffer = {0 };
    char *p = NULL;   
    ma_error_t rc = MA_ERROR_DS_REMOVE_FAILED;
    ma_db_statement_t *stmt = NULL;

    ma_temp_buffer_init(&child_buffer);
    ma_temp_buffer_reserve(&child_buffer, strlen(DELETE_CHILD_RECORDS_SQL_STATEMENT) + strlen(instance_name) + 16 ); /*16 for the path id */
    MA_MSC_SELECT(_snprintf, snprintf)(p = (char*)ma_temp_buffer_get(&child_buffer), ma_temp_buffer_capacity(&child_buffer), DELETE_CHILD_RECORDS_SQL_STATEMENT, instance_name, path_id);

    if(MA_OK == (rc = ma_db_statement_create(db,p,&stmt))) {
        if(MA_OK == (rc = ma_db_statement_execute(stmt))) {
            ma_temp_buffer_t parent_buffer = {0};
            ma_db_statement_t *parent_stmt = NULL;

            ma_temp_buffer_init(&parent_buffer);
            ma_temp_buffer_reserve(&parent_buffer, strlen(DELETE_PARENT_RECORD_SQL_STATEMENT) + strlen(instance_name) + 16 );/*16 for the path id */
            MA_MSC_SELECT(_snprintf, snprintf)(p = (char*)ma_temp_buffer_get(&parent_buffer), ma_temp_buffer_capacity(&parent_buffer), DELETE_PARENT_RECORD_SQL_STATEMENT, instance_name, path_id);
            if(MA_OK == (rc = ma_db_statement_create(db, p, &parent_stmt))) {
                rc = ma_db_statement_execute(parent_stmt);
                ma_db_statement_release(parent_stmt);
            }
            ma_temp_buffer_uninit(&parent_buffer);
        }
        ma_db_statement_release(stmt);
    }
    ma_temp_buffer_uninit(&child_buffer);
    return rc;
}

#define DELETE_CHILD_DATA_RECORD_SQL_STATEMENT "DELETE FROM %s_CHILD WHERE ID = %d AND NAME = ?"
static ma_error_t db_remove_child_record(ma_db_t *db, const char *instance_name, int path_id, const char *key) {
    ma_error_t rc = MA_ERROR_DS_REMOVE_FAILED ;
    ma_temp_buffer_t buffer = {0 };        
    char *pbuffer = NULL;    
    ma_db_statement_t *stmt = NULL;

    ma_temp_buffer_init(&buffer);
    ma_temp_buffer_reserve(&buffer, strlen(DELETE_CHILD_DATA_RECORD_SQL_STATEMENT) + strlen(instance_name) + 16 ); /*16 for path id */
    MA_MSC_SELECT(_snprintf, snprintf)(pbuffer = (char*)ma_temp_buffer_get(&buffer), ma_temp_buffer_capacity(&buffer), DELETE_CHILD_DATA_RECORD_SQL_STATEMENT, instance_name ,path_id);

    if(MA_OK == (rc = ma_db_statement_create(db, pbuffer, &stmt))) {
        if(MA_OK == (rc = ma_db_statement_set_string(stmt,1,1,key, strlen(key)))) {
            rc = ma_db_statement_execute(stmt);
        }
        ma_db_statement_release(stmt);
    }
    ma_temp_buffer_uninit(&buffer);
    return rc;
}

#define GET_SUB_PATH_ID_ITERATOR_STATEMENT "SELECT DISTINCT ID from %s_PARENT where PATH LIKE '%s%%'"
static ma_error_t db_prepare_path_id_iterator(ma_db_t *db, const char *instance_name, const char *path, size_t path_len, ma_ds_database_iterator_t *it) {
	ma_temp_buffer_t buffer = {0};
    char *pbuffer = NULL;
    ma_error_t rc = MA_ERROR_DS_PATH_NOT_FOUND;

	ma_temp_buffer_init(&buffer);       
    ma_temp_buffer_reserve(&buffer, strlen(GET_SUB_PATH_ID_ITERATOR_STATEMENT) + 1 + strlen(instance_name) + strlen(path)) ;        
    MA_MSC_SELECT(_snprintf, snprintf)(pbuffer = (char*)ma_temp_buffer_get(&buffer), ma_temp_buffer_capacity(&buffer), 
            GET_SUB_PATH_ID_ITERATOR_STATEMENT, instance_name, path,MA_DATASTORE_PATH_C_SEPARATOR ) ;
    
    if(MA_OK == (rc = ma_db_statement_create(db, pbuffer, &it->db_stmt))) {
        if(MA_OK != (rc = ma_db_statement_execute_query(it->db_stmt, &it->recordset)))
            ma_db_statement_release(it->db_stmt);
    }
	ma_temp_buffer_uninit(&buffer);
    return rc;
}


#define GET_SUB_PATH_ITERATOR_STATEMENT "SELECT DISTINCT SUBSTR(PATH,%d,INSTR(SUBSTR(PATH,%d),\"%c\") -1 ) from %s_PARENT where PATH LIKE '%s%%%c'"

static ma_error_t db_prepare_path_iterator(ma_db_t *db, const char *instance_name, const char *path, size_t path_len, ma_ds_database_iterator_t *it) {
	ma_temp_buffer_t buffer = {0};
    char *pbuffer = NULL;
    ma_error_t rc = MA_ERROR_DS_PATH_NOT_FOUND;
    int len_of_max_char_in_int = 8;

	ma_temp_buffer_init(&buffer);       
    /* STATEMENT + 2 * len_of_max_char_int + C_SEPARATOR + strlen INSTANCE_NAME + strlen PATH  + C_SEPARATOR*/
    ma_temp_buffer_reserve(&buffer, strlen(GET_SUB_PATH_ITERATOR_STATEMENT) + 2 * len_of_max_char_in_int + 1 + strlen(instance_name) + strlen(path));        
    MA_MSC_SELECT(_snprintf, snprintf)(pbuffer = (char*)ma_temp_buffer_get(&buffer), ma_temp_buffer_capacity(&buffer), 
        GET_SUB_PATH_ITERATOR_STATEMENT, path_len+1,path_len+1,MA_DATASTORE_PATH_C_SEPARATOR, instance_name, path,MA_DATASTORE_PATH_C_SEPARATOR );
    
    if(MA_OK == (rc = ma_db_statement_create(db, pbuffer, &it->db_stmt))) {
        if(MA_OK != (rc = ma_db_statement_execute_query(it->db_stmt, &it->recordset)))
            ma_db_statement_release(it->db_stmt);
    }
    	  
	ma_temp_buffer_uninit(&buffer);
    return rc;
}

#define GET_CHILD_ITERATOR_STATEMENT  "SELECT NAME FROM %s_CHILD WHERE ID = %d"

static ma_error_t db_prepare_child_iterator(ma_db_t *db, const char *instance_name, const char *path, size_t path_len, ma_ds_database_iterator_t *it) {
    int path_id = -1;
    ma_error_t rc = MA_ERROR_DS_PATH_NOT_FOUND;
    
    /*get the path id */
    if(MA_OK == (rc = db_get_path_id(db,instance_name,path,path_len, &path_id))) {
        ma_temp_buffer_t buffer = {0};
        char *pbuffer = NULL;
        
        ma_temp_buffer_init(&buffer);       
        ma_temp_buffer_reserve(&buffer, strlen(GET_CHILD_ITERATOR_STATEMENT) + strlen(instance_name) + 16); /*16 for path_id     */    
        MA_MSC_SELECT(_snprintf, snprintf)(pbuffer = (char*)ma_temp_buffer_get(&buffer), ma_temp_buffer_capacity(&buffer), GET_CHILD_ITERATOR_STATEMENT, instance_name, path_id);
        
        if(MA_OK == (rc = ma_db_statement_create(db, pbuffer, &it->db_stmt))) {
                if(MA_OK != (rc = ma_db_statement_execute_query(it->db_stmt, &it->recordset)))
                    ma_db_statement_release(it->db_stmt);
        }
        ma_temp_buffer_uninit(&buffer);
    }
    return rc;
}

static ma_error_t db_iterator_get_next(ma_ds_iterator_t *iterator, ma_buffer_t **buffer) {
	if(iterator && buffer) {
		ma_ds_database_iterator_t *self = (ma_ds_database_iterator_t *)iterator;
        ma_error_t rc = MA_OK;

        if(MA_OK == (rc = ma_db_recordset_next(self->recordset))) {
            const char *p = NULL;
            size_t len = 0;
            if(MA_OK == ( rc = ma_db_recordset_get_string(self->recordset, 1, &p))) {
                ma_buffer_t *temp_buffer = NULL;
                len = strlen(p);
                if(MA_OK == (rc = ma_buffer_create(&temp_buffer, len))) {
			        ma_buffer_set(temp_buffer,p ,len);
			        *buffer = temp_buffer;
                }
            }
        }

        return rc;
	}
	return MA_ERROR_INVALIDARG;
}

static ma_error_t db_iterator_release(ma_ds_iterator_t *iterator) {
	if(iterator) {
		ma_ds_database_iterator_t *self = (ma_ds_database_iterator_t *)iterator;

        ma_db_recordset_release(self->recordset);
        ma_db_statement_release(self->db_stmt);
        free(self);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}


static void get_new_path_with_separator_at_last(const char *path, size_t *path_len, ma_temp_buffer_t *buffer) {
    size_t len = strlen(path);
    if(MA_DATASTORE_PATH_C_SEPARATOR == path[len -1]) {
        ma_temp_buffer_reserve(buffer,len + 1);
        MA_MSC_SELECT(_snprintf, snprintf)((char*)ma_temp_buffer_get(buffer), len + 1,"%s",path);
        *path_len = len;
    }
    else {
        ma_temp_buffer_reserve(buffer,len + 2);
        MA_MSC_SELECT(_snprintf, snprintf)((char*)ma_temp_buffer_get(buffer), len + 2,"%s%c",path,MA_DATASTORE_PATH_C_SEPARATOR);
        *path_len = len + 1;
    } 
}

static ma_error_t ds_database_path_exists(ma_ds_database_t *datastore, const char *path, ma_bool_t *is_exists) {
    int path_id = 0 ;
    ma_error_t rc = MA_OK;
    size_t path_len = 0;
    ma_temp_buffer_t new_path_buffer = {0};
    char *new_path = NULL;

    ma_temp_buffer_init(&new_path_buffer);
    get_new_path_with_separator_at_last(path, &path_len, &new_path_buffer);
    new_path = (char*)ma_temp_buffer_get(&new_path_buffer);

	rc = db_get_path_id(datastore->db_ptr, datastore->db_instance_name, new_path, path_len, &path_id) ;
    if(MA_ERROR_DS_PATH_NOT_FOUND == rc)
        *is_exists = 0 ;
    else if(MA_OK == rc) 
        *is_exists = 1;

    ma_temp_buffer_uninit(&new_path_buffer);
    return (MA_ERROR_DS_PATH_NOT_FOUND == rc) ? MA_OK : rc;

}

#define KEY_EXISITS_SQL_STMT    "SELECT VALUE FROM %s_PARENT P, %s_CHILD C WHERE P.PATH = '%s' AND P.ID = C.ID AND C.NAME = '%s'"
static ma_error_t ds_database_key_exists(ma_ds_database_t *datastore, const char *path, const char *key, ma_bool_t *is_exists) {
    ma_error_t rc = MA_OK ;
    char *p = NULL ;
    ma_temp_buffer_t temp_buffer = {0 } ;
    ma_db_statement_t *stmt = NULL ;

    size_t path_len = 0;
    ma_temp_buffer_t new_path_buffer = {0};
    char *new_path = NULL;

    ma_temp_buffer_init(&new_path_buffer);
    get_new_path_with_separator_at_last(path, &path_len, &new_path_buffer);
    new_path = (char*)ma_temp_buffer_get(&new_path_buffer);

    ma_temp_buffer_init(&temp_buffer);    
    ma_temp_buffer_reserve(&temp_buffer, strlen(KEY_EXISITS_SQL_STMT) +  strlen(datastore->db_instance_name)*2 + path_len + strlen(key)) ;
    MA_MSC_SELECT(_snprintf, snprintf)(p = (char*)ma_temp_buffer_get(&temp_buffer), ma_temp_buffer_capacity(&temp_buffer), KEY_EXISITS_SQL_STMT, datastore->db_instance_name, datastore->db_instance_name, new_path, key);
	if(MA_OK == (rc = ma_db_statement_create(datastore->db_ptr, p, &stmt))) {
        ma_db_recordset_t *rs = NULL;
        if(MA_OK == (rc = ma_db_statement_execute_query(stmt, &rs))) {
            rc = ma_db_recordset_next(rs) ;
            ma_db_recordset_release(rs) ;
        }
        ma_db_statement_release(stmt) ;
    }
    ma_temp_buffer_uninit(&temp_buffer);    
    ma_temp_buffer_uninit(&new_path_buffer);

    rc = (MA_ERROR_NO_MORE_ITEMS == rc) ? MA_ERROR_DS_KEY_NOT_FOUND : rc;

    if(MA_ERROR_DS_KEY_NOT_FOUND == rc)
        *is_exists = 0 ;
    else if(MA_OK == rc) 
        *is_exists = 1;
    return (MA_ERROR_DS_KEY_NOT_FOUND == rc) ? MA_OK : rc;
    
}

static ma_error_t ds_db_database_is_exists(ma_ds_t *datastore, const char *path, const char *key, ma_bool_t *is_exists) {
    return key ? ds_database_key_exists((ma_ds_database_t*)datastore, path, key, is_exists) :
                 ds_database_path_exists((ma_ds_database_t*)datastore, path, is_exists);

}

