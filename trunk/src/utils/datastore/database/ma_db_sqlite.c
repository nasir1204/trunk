#include "ma_db_sqlite.h"
#include "ma/internal/utils/text/ma_utf8.h"
#include "ma/internal/utils/platform/ma_dl.h"
#include "ma/internal/utils/serialization/ma_deserialize.h"
#include "ma/internal/utils/serialization/ma_serialize.h"
#include "ma/internal/ma_macros.h"
#include "sqlite3.h"


#include <stdlib.h>
#include <stdio.h>
#include <wchar.h>


#ifdef MA_WINDOWS
#include <Windows.h>
#include <WinBase.h>

#define SQLITE_LIBRARY_NAME "sqlite.dll"
#else 
#define SQLITE_LIBRARY_NAME "libsqlite3.so"
#endif /* MA_WINDOWS */

void getsubpath(sqlite3_context *context, int argc, sqlite3_value **argv);

#define SQLITE_BUSY_TIMEOUT  500   /* 500 miliseconds wait before busy is returned */
#define SQLITE_BUSY_RETRIES  100   /* 100 times it will try */

#define SQL_STMT    sqlite3_stmt

/*sqlite3_open_v2*/
typedef int (*DB_OPEN_PROC)(const char *, sqlite3 **, int, const char *);
/*sqlite3_exec*/
typedef int (*DB_EXECUTE_PROC)(sqlite3 *, char *, void *, void *, char ** );
/*sqlite3_close_v2*/
typedef int (*DB_CLOSE_PROC)(sqlite3 *);
/*sqlite3_shutdown*/
typedef int (*DB_SHUTDOWN_PROC)();
/*sqlite3_busy_timeout*/
typedef int (*DB_WAIT_PROC)(sqlite3*, int);
/*sqlite3_busy_handler*/
typedef int (*DB_WAIT_HANDLER_PROC)(sqlite3*, int(*)(void*,int), void* );
/*sqlite3_prepare_v2*/
typedef int (*DB_PREPARE_SQLSTMT_PROC)(sqlite3 *, const char *, int, sqlite3_stmt **, const char ** );
/*sqlite3_bind_int*/
typedef int ( *DB_BIND_INT_PROC)(sqlite3_stmt *, int, int );
/*sqlite3_bind_text*/
typedef int ( *DB_BIND_TEXT_PROC)(sqlite3_stmt*, int, const char*, int n, void(*)(void*));
/*sqlite3_bind_blob*/
typedef int ( *DB_BIND_BLOB_PROC)(sqlite3_stmt*, int, const void*, int n, void(*)(void*));
/*sqlite3_step*/
typedef int (*DB_STEP_PROC)(sqlite3_stmt * );
/*sqlite3_finalize*/
typedef int (*DB_FINALIZE_PROC)(sqlite3_stmt * );
/*sqlite3_reset*/
typedef int (*DB_RESET_PROC)(sqlite3_stmt * );
/*sqlite3_column_int*/
typedef int (*DB_COLUMN_INT_PROC)(sqlite3_stmt *, int);
/*sqlite3_column_text*/
typedef char * (*DB_COLUMN_TEXT_PROC)(sqlite3_stmt *, int);
/*sqlite3_column_bytes*/
typedef void * (*DB_COLUMN_BLOB_PROC)(sqlite3_stmt *, int);
/*sqlite3_column_blob*/
typedef int (*DB_COLUMN_BYTES_PROC)(sqlite3_stmt *, int);
/*sqlite3_create_function*/
typedef void (*CREATE_FUNC_PARAM)(sqlite3_context *, int, sqlite3_value **);
typedef  int (*DB_CREATE_FUNCTION)(sqlite3 *db, const char *zFunctionName, int nArg, int eTextRep, void *pApp, CREATE_FUNC_PARAM, CREATE_FUNC_PARAM, CREATE_FUNC_PARAM);

/*sqlite3_result_null*/
typedef unsigned char* (*DB_VALUE_TEXT)(sqlite3_value*);
/*sqlite3_result_text*/
typedef void (*DB_RESULT_TEXT)(sqlite3_context*, const char*, int, void(*)(void*));

/*Declaration for select call backs -results*/
typedef int (*GET_CALL_BACK)(void *, int, char **, char **);

typedef struct  ma_sqlite_methods_s {    
    DB_OPEN_PROC                db_open;
    DB_EXECUTE_PROC             db_exec;    
    DB_CLOSE_PROC               db_close;
    DB_SHUTDOWN_PROC            db_shutdown;
    DB_WAIT_PROC                db_set_wait;
    DB_WAIT_HANDLER_PROC        db_set_wait_handler;
    DB_PREPARE_SQLSTMT_PROC     db_sqlstmt_prepare;
    DB_BIND_INT_PROC            db_bind_int;    
    DB_BIND_TEXT_PROC           db_bind_text;    
    DB_BIND_BLOB_PROC           db_bind_blob;    
    DB_STEP_PROC                db_step;
    DB_FINALIZE_PROC            db_sqlstmt_finalize;
    DB_RESET_PROC               db_sqlstmt_reset;    
    DB_COLUMN_INT_PROC          db_column_int;    
    DB_COLUMN_TEXT_PROC         db_column_text;
    DB_COLUMN_BLOB_PROC         db_column_blob;    
    DB_COLUMN_BYTES_PROC        db_column_bytes;
	DB_CREATE_FUNCTION			db_create_function;
	DB_VALUE_TEXT				db_value_text;
	DB_RESULT_TEXT				db_result_text;
} ma_sqlite_methods_t;

typedef struct ma_sqlite_lib_s {
    ma_sqlite_methods_t  db_methods; 
    ma_lib_t    lib_handle;
} ma_sqlite_lib_t;

typedef struct ma_db_sqlite_s {        
    sqlite3	    *dbref;        
}ma_db_sqlite_t;

 
/*SQLITE ITERATOR implementation*/
typedef struct ma_db_sqlite_iterator_s {
	ma_ds_iterator_t iterator; /*base class pointer*/
    char            *path;
    ma_uint8_t      is_path_iterator;
	SQL_STMT        *db_sql_stmt;
}ma_db_sqlite_iterator_t;

/*TODO - do we need to have ONCE guard to make it singleton or assume it will be called once and not use again*/
static ma_sqlite_lib_t *g_sqlite_lib = NULL;

static ma_error_t db_sqlite_iterator_get_next(ma_ds_iterator_t *self, ma_buffer_t **buffer);
static ma_error_t db_sqlite_iterator_release(ma_ds_iterator_t *self);

static const ma_ds_iterator_methods_t it_methods = {
    &db_sqlite_iterator_get_next,
    &db_sqlite_iterator_release
};

/* Initialize sqlite functions*/
static ma_error_t initialize_sqlite_functions(ma_sqlite_lib_t *self);

/* static helper functions for set/get/remove */
static int db_get_path_id_cb(void *cb_data, int argc, char **argv, char **column_name);
static int db_get_path_id(ma_db_sqlite_t *self, const char *instance_name, const char *path, size_t path_len );
static int db_get_max_path_id(ma_db_sqlite_t *self, const char *instance_name);

static ma_error_t db_add_parent_record(ma_db_sqlite_t *self, const char *instance_name, int path_id, const char *path, size_t path_len);
static ma_error_t db_add_child_record(ma_db_sqlite_t *self, const char *instance_name, int path_id, const char *key,  ma_variant_t *value);
static ma_error_t db_get_child_record(ma_db_sqlite_t *self, const char *instance_name, int path_id, const char *key, ma_vartype_t type, ma_variant_t **value);
static ma_error_t db_remove_child_record(ma_db_sqlite_t *self, const char *instance_name, int path_id, const char *key);
static ma_error_t db_remove_child_records(ma_db_sqlite_t *self, const char *instance_name, int path_id);

/*static fucntions for iteration helping*/
static ma_error_t db_sqlite_prepare_path_iterator(ma_db_sqlite_t *self, const char *instance_name, const char *path, ma_db_sqlite_iterator_t *it);
static ma_error_t db_sqlite_prepare_child_iterator(ma_db_sqlite_t *self, const char *instance_name, const char *path, ma_db_sqlite_iterator_t *it);


ma_error_t ma_db_sqlite_initialize() {
    if(!g_sqlite_lib) {
        ma_sqlite_lib_t *self = (ma_sqlite_lib_t *)calloc(1, sizeof(ma_sqlite_lib_t));
        ma_error_t rc = MA_OK;        
        if(!self) return MA_ERROR_OUTOFMEMORY;

        if(MA_OK != (rc = ma_dl_open(SQLITE_LIBRARY_NAME, 0, &self->lib_handle ))) {
            free(self); return MA_ERROR_DS_DB_OPEN_FAILED;
        }  
        if(MA_OK != (rc = initialize_sqlite_functions(self))) {
            ma_dl_close(&self->lib_handle);
            free(self); return rc;
        }
        g_sqlite_lib = self;  
    }    
    return MA_OK;
}

ma_error_t ma_db_sqlite_deinitialize() {
    if(g_sqlite_lib) {                
        ma_dl_close(&g_sqlite_lib->lib_handle);
        free(g_sqlite_lib), g_sqlite_lib = NULL;
    }
    return MA_OK;
}

static int sqlite_busy_handler(void *cb_data, int count);

ma_error_t  ma_db_sqlite_open(const char *db_name, int flags, void **handle) {     
    /* NOTE : no need to check input params as it should be checked with upper layer */
    if(g_sqlite_lib) {   
        int sql_rc = 0; 
        ma_db_sqlite_t *self = (ma_db_sqlite_t *)calloc(1, sizeof(ma_db_sqlite_t));
        if(!self)   return MA_ERROR_OUTOFMEMORY;

        /* TODO - Need to look at the flags what we can do */
        if(0 != (sql_rc = g_sqlite_lib->db_methods.db_open(db_name, &self->dbref, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE | SQLITE_OPEN_NOMUTEX, NULL))) {        
            free(self);
            return MA_ERROR_DS_DB_OPEN_FAILED;
        }

        g_sqlite_lib->db_methods.db_set_wait(self->dbref, SQLITE_BUSY_TIMEOUT);
        g_sqlite_lib->db_methods.db_set_wait_handler(self->dbref, sqlite_busy_handler, self);
		g_sqlite_lib->db_methods.db_create_function(self->dbref, "getsubpath", 2, SQLITE_ANY, NULL, &getsubpath, NULL, NULL);            
        *handle = self;
        return MA_OK;
    }

    return MA_ERROR_DS_DB_NOT_INIT;
}

static int check_table_exists_cb(void *cb_data, int argc, char **argv, char **column_name) {
    if( cb_data && argc > 0 && argv) {
         *((char*)cb_data) = argv[0][0];
         return 0;
    }
    return -1;
}

#define CHECK_TABLE_EXISTS_SQL_STATEMENT "SELECT COUNT(*) FROM sqlite_master WHERE type='table' AND name='%s_PARENT'"
#define CREATE_PARENT_TABLE_SQL_STATEMENT "CREATE TABLE IF NOT EXISTS %s_PARENT (ID INTEGER NOT NULL UNIQUE, PATH TEXT NOT NULL UNIQUE, PRIMARY KEY (ID, PATH) ON CONFLICT REPLACE )"
#define CREATE_CHILD_TABLE_SQL_STATEMENT  "CREATE TABLE IF NOT EXISTS %s_CHILD (ID INTEGER NOT NULL, NAME TEXT NOT NULL, VALUE BLOB NOT NULL, PRIMARY KEY (ID, NAME) ON CONFLICT REPLACE , FOREIGN KEY (ID) REFERENCES %s_PARENT(ID)  ON UPDATE CASCADE ON DELETE CASCADE )"

ma_error_t  ma_db_sqlite_create_instance(void *handle, const char *instance_name) {
    if(!g_sqlite_lib)   return MA_ERROR_DS_DB_NOT_INIT;

    if(handle && instance_name) {
        ma_db_sqlite_t *self = (ma_db_sqlite_t *)handle;
        ma_temp_buffer_t buffer = {0 };        
        int sql_rc = 0;
        char *pbuffer = NULL, c_table_exists = 0;

        ma_temp_buffer_init(&buffer);       
        ma_temp_buffer_reserve(&buffer, strlen(CHECK_TABLE_EXISTS_SQL_STATEMENT) + strlen(instance_name));        
        MA_MSC_SELECT(_snprintf, snprintf)(pbuffer = (char*)ma_temp_buffer_get(&buffer), ma_temp_buffer_capacity(&buffer), CHECK_TABLE_EXISTS_SQL_STATEMENT, instance_name);

        /*Check table already exists or not */
        if( (0 == (sql_rc = g_sqlite_lib->db_methods.db_exec(self->dbref, pbuffer, check_table_exists_cb, &c_table_exists, NULL))) 
            && ('0' == c_table_exists)) {
            ma_temp_buffer_uninit(&buffer);  ma_temp_buffer_init(&buffer);
            ma_temp_buffer_reserve(&buffer, strlen(CREATE_PARENT_TABLE_SQL_STATEMENT) + strlen(instance_name));        
            MA_MSC_SELECT(_snprintf, snprintf)(pbuffer = (char*)ma_temp_buffer_get(&buffer), ma_temp_buffer_capacity(&buffer), CREATE_PARENT_TABLE_SQL_STATEMENT, instance_name);
            /* Create parent table */
            if(0 == (sql_rc = g_sqlite_lib->db_methods.db_exec(self->dbref, pbuffer, NULL, NULL, NULL))) {        
                /*Create child table */
                ma_temp_buffer_uninit(&buffer);       ma_temp_buffer_init(&buffer);
                ma_temp_buffer_reserve(&buffer, strlen(CREATE_CHILD_TABLE_SQL_STATEMENT) + strlen(instance_name) * 2);        
                MA_MSC_SELECT(_snprintf, snprintf)(pbuffer = (char*)ma_temp_buffer_get(&buffer), ma_temp_buffer_capacity(&buffer), CREATE_CHILD_TABLE_SQL_STATEMENT, instance_name, instance_name);

                sql_rc = g_sqlite_lib->db_methods.db_exec(self->dbref, pbuffer, NULL, NULL, NULL);       
            }
        }
        ma_temp_buffer_uninit(&buffer);
        return (0 == sql_rc) ? MA_OK  : MA_ERROR_DS_DB_CREATE_INSTANCE_FAILED;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t  ma_db_sqlite_close_instance(void *handle, const char *instance_name) {
    if(!g_sqlite_lib)   return MA_ERROR_DS_DB_NOT_INIT;
    return MA_OK;
}


#define DROP_PARENT_TABLE_SQL_STATEMENT "DROP TABLE %s_PARENT "
#define DROP_CHILD_TABLE_SQL_STATEMENT  "DROP TABLE %s_CHILD "

ma_error_t  ma_db_sqlite_delete_instance(void *handle, const char *instance_name) {
    if(!g_sqlite_lib)   return MA_ERROR_DS_DB_NOT_INIT;
    if(handle && instance_name) {
        ma_db_sqlite_t *self = (ma_db_sqlite_t *)handle;
        ma_temp_buffer_t buffer = {0 };        
        int sql_rc = 0;
        char *pbuffer = NULL;

        ma_temp_buffer_init(&buffer); 
        ma_temp_buffer_reserve(&buffer, strlen(DROP_PARENT_TABLE_SQL_STATEMENT) + strlen(instance_name));
        MA_MSC_SELECT(_snprintf, snprintf)(pbuffer = (char*)ma_temp_buffer_get(&buffer), ma_temp_buffer_capacity(&buffer), DROP_PARENT_TABLE_SQL_STATEMENT, instance_name);
        if(0 == (sql_rc = g_sqlite_lib->db_methods.db_exec(self->dbref, pbuffer, NULL, NULL, NULL)) )
        {
            ma_temp_buffer_reserve(&buffer, strlen(DROP_CHILD_TABLE_SQL_STATEMENT) + strlen(instance_name));
            MA_MSC_SELECT(_snprintf, snprintf)(pbuffer = (char*)ma_temp_buffer_get(&buffer), ma_temp_buffer_capacity(&buffer), DROP_CHILD_TABLE_SQL_STATEMENT, instance_name);
            sql_rc = g_sqlite_lib->db_methods.db_exec(self->dbref, pbuffer, NULL, NULL, NULL);
        }
        return (0 == sql_rc) ? MA_OK  : MA_ERROR_DS_DB_DELETE_INSTANCE_FAILED;
    }
    return MA_ERROR_INVALIDARG;
}

/*Optimization for adding multiple records in one transcation
1. Inside the transaction 
2. Multiple values in one shot 
3. Create the Indexes - /*TODO
4. PRAGMA synchronous = OFF - /*TODO - should we use this ??????
        *****NOTE NOTE By default SQLite will pause after issuing a OS-level write command. This guarantees that the data is written to the disk. 
        By setting synchronous = OFF, we are instructing SQLite to simply hand-off the data to the OS for writing and then continue. 
        There's a chance that the database file may become corrupted if the computer suffers a catastrophic crash (or power failure) before the data is written.
5. PRAGMA journal_mode = MEMORY /*TODO - should we use this ??????
*/
ma_error_t  ma_db_sqlite_transaction_begin(void *handle, const char *instance_name) {
    if(!g_sqlite_lib)   return MA_ERROR_DS_DB_NOT_INIT;    
    if(handle && instance_name) {
        int sql_rc = 0;  
        ma_db_sqlite_t *self = (ma_db_sqlite_t *)handle;
        /*Experimentation with synchronous pragma */
        g_sqlite_lib->db_methods.db_exec(self->dbref, "PRAGMA synchronous = OFF", NULL, NULL, NULL);
        if(0 != (sql_rc = g_sqlite_lib->db_methods.db_exec(self->dbref, "BEGIN TRANSACTION", NULL, NULL, NULL))) {
            /*Experimentation with synchronous pragma */
            g_sqlite_lib->db_methods.db_exec(self->dbref, "PRAGMA synchronous = ON", NULL, NULL, NULL);            
        }
        return 0 == sql_rc ? MA_OK : MA_ERROR_DS_DB_TX_BEGIN_FAILED;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t  ma_db_sqlite_transaction_cancel(void *handle, const char *instance_name) {
    if(!g_sqlite_lib)   return MA_ERROR_DS_DB_NOT_INIT;    
    if(handle && instance_name) {
        int sql_rc = 0;  
        ma_db_sqlite_t *self = (ma_db_sqlite_t *)handle;
        sql_rc = g_sqlite_lib->db_methods.db_exec(self->dbref, "ROLLBACK TRANSACTION", NULL, NULL, NULL);
        /*Experimentation with synchronous pragma */
        g_sqlite_lib->db_methods.db_exec(self->dbref, "PRAGMA synchronous = ON", NULL, NULL, NULL);
        return 0 == sql_rc ? MA_OK : MA_ERROR_DS_DB_TX_CANCEL_FAILED;        
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t  ma_db_sqlite_transaction_end(void *handle, const char *instance_name) {
    if(!g_sqlite_lib)   return MA_ERROR_DS_DB_NOT_INIT;    
    if(handle && instance_name) {
        int sql_rc = 0;  
        ma_db_sqlite_t *self = (ma_db_sqlite_t *)handle;
        sql_rc = g_sqlite_lib->db_methods.db_exec(self->dbref, "END TRANSACTION", NULL, NULL, NULL);
        /*Experimentation with synchronous pragma */
        g_sqlite_lib->db_methods.db_exec(self->dbref, "PRAGMA synchronous = ON", NULL, NULL, NULL);
        return 0 == sql_rc ? MA_OK : MA_ERROR_DS_DB_TX_END_FAILED;
    }
    return MA_ERROR_INVALIDARG;
}



ma_error_t ma_db_sqlite_get_iterator(void *handle, const char *instance_name, ma_uint8_t is_path_iterator, const char *path, ma_ds_iterator_t **iterator) {
	if(!g_sqlite_lib)   return MA_ERROR_DS_DB_NOT_INIT;    
    if(handle && instance_name) {
		ma_db_sqlite_t *self = (ma_db_sqlite_t *)handle;
		ma_error_t rc = MA_OK;
        char *new_path = NULL;
        size_t len = 0 ;
        ma_db_sqlite_iterator_t *it = (ma_db_sqlite_iterator_t *)calloc(1,sizeof(ma_db_sqlite_iterator_t));
          
		if(!it) return MA_ERROR_OUTOFMEMORY;
        
        /*Remove SEPARATOR from last if it is there */
        len = strlen(path);
        new_path = strdup(path);
        if(MA_DATASTORE_PATH_C_SEPARATOR == new_path[len -1])
            new_path[len - 1] = '\0';

		rc = is_path_iterator ? db_sqlite_prepare_path_iterator(self,instance_name,new_path,it)
							: db_sqlite_prepare_child_iterator(self,instance_name,new_path,it);
		if(MA_OK != rc) {
			free(it);
            free(new_path);
			return rc;
		}

        it->path = strdup(new_path);
        it->is_path_iterator = is_path_iterator;
        free(new_path);
		ma_ds_iterator_init((ma_ds_iterator_t*)it, &it_methods,it);
		*iterator = (ma_ds_iterator_t *)it;
		return MA_OK;
    }    
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_db_sqlite_set(void *handle, const char *instance_name, const char *path, const char *key, ma_variant_t *value) {
    if(!g_sqlite_lib)   return MA_ERROR_DS_DB_NOT_INIT;

     /* No need of parameter validation which are coming from front end */    
    if( handle && instance_name) {
        ma_db_sqlite_t *self = (ma_db_sqlite_t *)handle;
        int path_id = -1;
        ma_error_t rc = MA_OK;
        size_t path_len = 0;
       
		path_len = strlen(path);
        
        /*get the path id*/
        if(-1 == (path_id = db_get_path_id(self, instance_name, path, path_len))) {        
            /*if path  is not in database, create the path with max(ID)+1 */
            if(-1 != (path_id = db_get_max_path_id(self, instance_name)))
                /*add the parent data (path) */
                rc = db_add_parent_record(self, instance_name, path_id,  path, path_len);          
        }        
        
        /*Stil no property id then return path not found */
        if(-1 == path_id ) 
			return MA_ERROR_DS_ADD_PATH_FAILED;
        
        /*Add the child data if evertyhing okay */
        if(MA_OK == rc) 
            rc =  db_add_child_record(self, instance_name, path_id, key , value);
     
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_db_sqlite_get(void *handle, const char *instance_name, const char *path, const char *key, ma_vartype_t type, ma_variant_t **value ){
    if(!g_sqlite_lib)        return MA_ERROR_DS_DB_NOT_INIT;

    /* No need of parameter validation which are coming from front end */ 
    if(handle && instance_name ) {
        ma_db_sqlite_t *self = (ma_db_sqlite_t *)handle;
        int path_id = -1;
        size_t path_len = 0;
        ma_error_t rc = MA_ERROR_DS_PATH_NOT_FOUND;
      
        path_len = strlen(path);        
        
        /*get the path id */
        if(-1 != (path_id = db_get_path_id(self, instance_name, path, path_len))) 
            rc = db_get_child_record(self, instance_name, path_id, key, type, value);
      
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_db_sqlite_remove(void *handle, const char *instance_name, const char *path, const char *key) {
    if(!g_sqlite_lib)   return MA_ERROR_DS_DB_NOT_INIT;

    /* No need of parameter validation which are coming from front end */ 
    if(handle && instance_name) {
        ma_db_sqlite_t *self = (ma_db_sqlite_t *)handle;
        int path_id = -1;        
        size_t path_len = 0;
        ma_error_t rc = MA_ERROR_DS_PATH_NOT_FOUND;
      

        path_len = strlen(path);
        
        /*get the path ID for the input path */
        if(-1 != (path_id = db_get_path_id(self, instance_name, path, path_len))) {
            rc = key ? db_remove_child_record(self, instance_name, path_id, key) 
                     : db_remove_child_records(self, instance_name,path_id);
        }
        
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t  ma_db_sqlite_close(void *handle) {                
    if(g_sqlite_lib && handle ){
        ma_db_sqlite_t *self = (ma_db_sqlite_t *)handle;
        if(self->dbref) {
			g_sqlite_lib->db_methods.db_create_function(self->dbref, "get_subpath", 2, SQLITE_ANY, NULL, NULL, NULL, NULL);
            g_sqlite_lib->db_methods.db_close(self->dbref);
            g_sqlite_lib->db_methods.db_shutdown();
        }
        free(self);
        return MA_OK;
    }
    return g_sqlite_lib ? MA_ERROR_INVALIDARG : MA_ERROR_DS_DB_NOT_INIT;   
    
}



static int db_get_path_id_cb(void *cb_data, int argc, char **argv, char **column_name) {
    int *path_id = (int *)cb_data;
    if( cb_data && argc > 0 && argv) {
        if(strstr(column_name[0], "MAX(ID)")){
            if(!argv[0])
                *path_id = 1;
            else
                *path_id = atoi(argv[0]);
            return 0;
        }
        if(strstr(column_name[0],"ID")) {
            if(argv[0])
                *path_id = atoi(argv[0]);
            return 0;
        }
    }
    return 1;
}

#define GET_PATH_ID_SQL_STATEMENT   "SELECT ID FROM %s_PARENT WHERE PATH = ?"

static int  db_get_path_id(ma_db_sqlite_t *self, const char *instance_name, const char *path, size_t path_len ){
    ma_temp_buffer_t buffer = {0};
    int sql_rc = 0, path_id = -1;
    char *pbuffer = NULL;
    SQL_STMT *db_sql_stmt;

    /*TODO - add logging */
    ma_temp_buffer_init(&buffer);       
    ma_temp_buffer_reserve(&buffer, strlen(GET_PATH_ID_SQL_STATEMENT) + strlen(instance_name) );        
    MA_MSC_SELECT(_snprintf, snprintf)(pbuffer = (char*)ma_temp_buffer_get(&buffer), ma_temp_buffer_capacity(&buffer), GET_PATH_ID_SQL_STATEMENT, instance_name);
    if(0 == (sql_rc = g_sqlite_lib->db_methods.db_sqlstmt_prepare(self->dbref, pbuffer, -1, &db_sql_stmt, NULL))) {  
        /*Remove SEPARATOR from last if it is there */
        path_len = ((MA_DATASTORE_PATH_C_SEPARATOR == path[path_len -1]) ? path_len - 1 : path_len);
        if( (0 == (sql_rc = g_sqlite_lib->db_methods.db_bind_text(db_sql_stmt, 1, path, path_len, SQLITE_STATIC)))
            && (SQLITE_ROW == (sql_rc = g_sqlite_lib->db_methods.db_step(db_sql_stmt))))
            path_id = g_sqlite_lib->db_methods.db_column_int(db_sql_stmt, 0);                    

        g_sqlite_lib->db_methods.db_sqlstmt_finalize(db_sql_stmt);
    }    
    ma_temp_buffer_uninit(&buffer);
    
    return path_id;
}



#define GET_MAX_PATH_ID_SQL_STATEMENT    "SELECT MAX(ID)+1 FROM %s_PARENT"
static int db_get_max_path_id(ma_db_sqlite_t *self, const char *instance_name) {
    ma_temp_buffer_t buffer = {0};        
    int sql_rc = 0, path_id = -1;
    char *pbuffer = NULL;   
        
    ma_temp_buffer_init(&buffer);
    ma_temp_buffer_reserve(&buffer, strlen(GET_MAX_PATH_ID_SQL_STATEMENT) + strlen(instance_name));        
    MA_MSC_SELECT(_snprintf, snprintf)(pbuffer = (char*)ma_temp_buffer_get(&buffer), ma_temp_buffer_capacity(&buffer), GET_MAX_PATH_ID_SQL_STATEMENT, instance_name);

    sql_rc = g_sqlite_lib->db_methods.db_exec(self->dbref, pbuffer, db_get_path_id_cb, &path_id, NULL);

    ma_temp_buffer_uninit(&buffer);

    return (0 == sql_rc) ? path_id : -1;
}


#define ADD_PARENT_RECORD_SQL_STATEMENT   "INSERT INTO %s_PARENT VALUES (?, ?)"

static ma_error_t db_add_parent_record(ma_db_sqlite_t *self, const char *instance_name, int path_id, const char *path, size_t path_len) {
    ma_temp_buffer_t buffer = {0};        
    int sql_rc = 0;
    char *pbuffer = NULL;
    SQL_STMT *db_sql_stmt;        

    ma_temp_buffer_init(&buffer);
    ma_temp_buffer_reserve(&buffer, strlen(ADD_PARENT_RECORD_SQL_STATEMENT) + strlen(instance_name) ); 
    MA_MSC_SELECT(_snprintf, snprintf)(pbuffer = (char*)ma_temp_buffer_get(&buffer), ma_temp_buffer_capacity(&buffer), ADD_PARENT_RECORD_SQL_STATEMENT, instance_name);
    if(0 == (sql_rc = g_sqlite_lib->db_methods.db_sqlstmt_prepare(self->dbref, pbuffer, -1, &db_sql_stmt, NULL))) {
        if(0 == (sql_rc = g_sqlite_lib->db_methods.db_bind_int(db_sql_stmt, 1, path_id))) {                    
            size_t len =  path_len;
            /*Remove SEPARATOR from last if it is there */
            len = ((MA_DATASTORE_PATH_C_SEPARATOR == path[len -1]) ? len - 1 : len);
            
            sql_rc = (!g_sqlite_lib->db_methods.db_bind_text(db_sql_stmt, 2, path, len, SQLITE_STATIC) && 
                     (SQLITE_DONE == g_sqlite_lib->db_methods.db_step(db_sql_stmt)))
                     ? 0 : SQLITE_ERROR;
        }
        g_sqlite_lib->db_methods.db_sqlstmt_finalize(db_sql_stmt);
    }
    ma_temp_buffer_uninit(&buffer);
    return (0 == sql_rc) ? MA_OK : MA_ERROR_DS_SET_FAILED;
}



#define ADD_CHILD_RECORDS_SQL_STATEMENT    "INSERT INTO %s_CHILD VALUES (?,?,?)"

static ma_error_t db_add_child_record(ma_db_sqlite_t *self, const char *instance_name, int path_id, const char *key,  ma_variant_t *value)  {    
    ma_error_t rc = MA_ERROR_DS_SET_FAILED;
    ma_temp_buffer_t temp_buffer = {0 };        
    char *p = NULL;
    SQL_STMT *db_sql_stmt;
            
    ma_temp_buffer_init(&temp_buffer);
    ma_temp_buffer_reserve(&temp_buffer, strlen(ADD_CHILD_RECORDS_SQL_STATEMENT) + strlen(instance_name) ); 
    MA_MSC_SELECT(_snprintf, snprintf)(p = (char*)ma_temp_buffer_get(&temp_buffer), ma_temp_buffer_capacity(&temp_buffer), ADD_CHILD_RECORDS_SQL_STATEMENT, instance_name);

	if(!g_sqlite_lib->db_methods.db_sqlstmt_prepare(self->dbref, p, -1, &db_sql_stmt, NULL)) {
		if(!g_sqlite_lib->db_methods.db_bind_int(db_sql_stmt, 1, path_id) &&
		   !g_sqlite_lib->db_methods.db_bind_text(db_sql_stmt, 2, key,  strlen(key), SQLITE_STATIC )) {
				ma_vartype_t var_type = MA_VARTYPE_NULL;
				size_t size = 0;

				ma_variant_get_type(value,&var_type);
				switch(var_type) {
					case MA_VARTYPE_INT8 :
					case MA_VARTYPE_UINT8 :
					case MA_VARTYPE_INT16 :
					case MA_VARTYPE_UINT16 :
					case MA_VARTYPE_INT32 :
					case MA_VARTYPE_UINT32 :
					case MA_VARTYPE_INT64: 
					case MA_VARTYPE_UINT64:
					case MA_VARTYPE_BOOL: {
						char temp[21] = {0};
						ma_int64_t i = 0 ;

						size = 21;
						if(MA_OK == (rc = ma_variant_get_int64(value,&i))) {
							MA_MSC_SELECT(_snprintf, snprintf)(temp,21,"%lld",i);
							rc = !g_sqlite_lib->db_methods.db_bind_blob(db_sql_stmt, 3, temp, 21, SQLITE_TRANSIENT)  ? MA_OK: MA_ERROR_DS_SET_FAILED;
						}
						break;
					}
					case MA_VARTYPE_STRING:{    
						ma_buffer_t *buffer = NULL;
						if(MA_OK == (rc = ma_variant_get_string_buffer(value,&buffer))) {
							ma_buffer_get_string(buffer,&p, &size);
							rc = !g_sqlite_lib->db_methods.db_bind_blob(db_sql_stmt, 3, p, size, SQLITE_TRANSIENT) ? MA_OK: MA_ERROR_DS_SET_FAILED;
							ma_buffer_release(buffer);
						}
						break;
					}
					case MA_VARTYPE_RAW:{
						ma_buffer_t *buffer = NULL;
						if(MA_OK == (rc = ma_variant_get_raw_buffer(value,&buffer))) {
							ma_buffer_get_raw(buffer,(unsigned char **)&p, &size);
							rc = !g_sqlite_lib->db_methods.db_bind_blob(db_sql_stmt, 3, p, size, SQLITE_TRANSIENT) ? MA_OK: MA_ERROR_DS_SET_FAILED;
							ma_buffer_release(buffer);
						}
						break;
					}
					case MA_VARTYPE_DOUBLE :
					case MA_VARTYPE_FLOAT :
					case MA_VARTYPE_TABLE :
					case MA_VARTYPE_ARRAY : {
						ma_serializer_t *serializer = NULL;
						if(MA_OK == (rc = ma_serializer_create(&serializer))) {
							if(MA_OK == (rc = ma_serializer_add_variant(serializer, value))) {
								if(MA_OK == (rc = ma_serializer_get(serializer, &p, &size))) {
									rc = !g_sqlite_lib->db_methods.db_bind_blob(db_sql_stmt, 3, p, size, SQLITE_TRANSIENT) ? MA_OK : MA_ERROR_DS_SET_FAILED;
								}
							}
							ma_serializer_destroy(serializer);
						}
						break;
					}
					default:
						rc = MA_ERROR_INVALIDARG;
				}

				rc = ((MA_OK == rc)  && 
					 (SQLITE_DONE == g_sqlite_lib->db_methods.db_step(db_sql_stmt)) && 
					 !g_sqlite_lib->db_methods.db_sqlstmt_reset(db_sql_stmt)) 
					 ? MA_OK : MA_ERROR_DS_SET_FAILED;


		}

		g_sqlite_lib->db_methods.db_sqlstmt_finalize(db_sql_stmt);
	}
    ma_temp_buffer_uninit(&temp_buffer);             
    return rc;
}
#define GET_CHILD_RECORD_SQL_STATEMENT    "SELECT VALUE FROM %s_CHILD WHERE ID = ? AND NAME = ?"

static ma_error_t db_get_child_record(ma_db_sqlite_t *self, const char *instance_name, int path_id, const char *key, ma_vartype_t type, ma_variant_t **value) {   
    ma_error_t rc = MA_ERROR_DS_GET_FAILED;
	ma_temp_buffer_t temp_buffer = {0 };
	char *p = NULL;
	SQL_STMT *db_sql_stmt = NULL;       

	ma_temp_buffer_init(&temp_buffer);    
	ma_temp_buffer_reserve(&temp_buffer, strlen(GET_CHILD_RECORD_SQL_STATEMENT) +  strlen(instance_name) ); 
    MA_MSC_SELECT(_snprintf, snprintf)(p = (char*)ma_temp_buffer_get(&temp_buffer), ma_temp_buffer_capacity(&temp_buffer), GET_CHILD_RECORD_SQL_STATEMENT, instance_name);

    if(!g_sqlite_lib->db_methods.db_sqlstmt_prepare(self->dbref, p, -1, &db_sql_stmt, NULL)) {        
		if(!g_sqlite_lib->db_methods.db_bind_int(db_sql_stmt, 1, path_id) && 
           !g_sqlite_lib->db_methods.db_bind_text(db_sql_stmt, 2, key, strlen(key), SQLITE_STATIC) &&
            SQLITE_ROW == g_sqlite_lib->db_methods.db_step(db_sql_stmt)) {
			size_t data_len = g_sqlite_lib->db_methods.db_column_bytes(db_sql_stmt, 0);

			p = (char*)g_sqlite_lib->db_methods.db_column_blob(db_sql_stmt, 0);
			/* Now convert the value the way you want */
			switch(type) {
				case MA_VARTYPE_UINT8:
				case MA_VARTYPE_INT8:
				case MA_VARTYPE_BOOL: {
						rc = ma_variant_create_from_int8((ma_int8_t)(MA_MSC_SELECT(_strtoi64,strtoll)((char*)p,NULL,0)) , value);
					break;
				
				}
				case MA_VARTYPE_UINT16:
				case MA_VARTYPE_INT16:{
					rc = ma_variant_create_from_int16((ma_int16_t)(MA_MSC_SELECT(_strtoi64,strtoll)((char*)p,NULL,0)) , value);
					break;
				}
				case MA_VARTYPE_UINT32:
				case MA_VARTYPE_INT32:{
					rc = ma_variant_create_from_int32((ma_int32_t)(MA_MSC_SELECT(_strtoi64,strtoll)((char*)p,NULL,0)) , value);
					break;
				}
				case MA_VARTYPE_UINT64:
				case MA_VARTYPE_INT64:{
					rc = ma_variant_create_from_int64(MA_MSC_SELECT(_strtoi64,strtoll)((char*)p,NULL,0) , value);
					break;
				}
				case MA_VARTYPE_STRING:{
					rc = ma_variant_create_from_string(p, data_len, value);
					break;
				}
				case MA_VARTYPE_RAW: {
					rc = ma_variant_create_from_raw(p, data_len,value);
					break;
				}
				default:{
					ma_deserializer_t *deserializer = NULL;
					if(MA_OK == (rc = ma_deserializer_create(&deserializer))) {
						if(MA_OK == (rc = ma_deserializer_put(deserializer, p, data_len))) {
							rc = ma_deserializer_get_next(deserializer,value);
						}
						ma_deserializer_destroy(deserializer);
					}
					break;
				}
			}
		}
		else
			rc = MA_ERROR_DS_KEY_NOT_FOUND;
		g_sqlite_lib->db_methods.db_sqlstmt_finalize(db_sql_stmt);        
	}

	ma_temp_buffer_uninit(&temp_buffer);    
    return rc;
}


#define DELETE_CHILD_RECORDS_SQL_STATEMENT "DELETE FROM %s_CHILD WHERE ID = %d"
#define DELETE_PARENT_RECORD_SQL_STATEMENT "DELETE FROM %s_PARENT WHERE ID = %d"

static ma_error_t db_remove_child_records(ma_db_sqlite_t *self, const char *instance_name, int path_id) {
    ma_temp_buffer_t buffer = {0 };
    int sql_rc = 0;
    char *pbuffer = NULL;   

    ma_temp_buffer_init(&buffer);
    ma_temp_buffer_reserve(&buffer, strlen(DELETE_CHILD_RECORDS_SQL_STATEMENT) + strlen(instance_name) + 16 ); /*16 for the path id */
    MA_MSC_SELECT(_snprintf, snprintf)(pbuffer = (char*)ma_temp_buffer_get(&buffer), ma_temp_buffer_capacity(&buffer), DELETE_CHILD_RECORDS_SQL_STATEMENT, instance_name, path_id);
    sql_rc = g_sqlite_lib->db_methods.db_exec(self->dbref, pbuffer, NULL, NULL, NULL);        
    ma_temp_buffer_uninit(&buffer);
    if(0 == sql_rc) {
        ma_temp_buffer_init(&buffer);
        ma_temp_buffer_reserve(&buffer, strlen(DELETE_PARENT_RECORD_SQL_STATEMENT) + strlen(instance_name) + 16 );/*16 for the path id */
        MA_MSC_SELECT(_snprintf, snprintf)(pbuffer = (char*)ma_temp_buffer_get(&buffer), ma_temp_buffer_capacity(&buffer), DELETE_PARENT_RECORD_SQL_STATEMENT, instance_name, path_id);
        sql_rc = g_sqlite_lib->db_methods.db_exec(self->dbref, pbuffer, NULL, NULL, NULL); 
        ma_temp_buffer_uninit(&buffer);
    }
        
    return 0 == sql_rc ? MA_OK : MA_ERROR_DS_REMOVE_FAILED;
}

#define DELETE_CHILD_DATA_RECORD_SQL_STATEMENT "DELETE FROM %s_CHILD WHERE ID = %d AND NAME = ?"

static ma_error_t db_remove_child_record(ma_db_sqlite_t *self, const char *instance_name, int path_id, const char *key) {
    ma_error_t rc = MA_ERROR_DS_REMOVE_FAILED ;
    ma_temp_buffer_t buffer = {0 };        
    char *pbuffer = NULL;    
    SQL_STMT *db_sql_stmt;

    ma_temp_buffer_init(&buffer);
    ma_temp_buffer_reserve(&buffer, strlen(DELETE_CHILD_DATA_RECORD_SQL_STATEMENT) + strlen(instance_name) + 16 ); /*16 for path id */
    MA_MSC_SELECT(_snprintf, snprintf)(pbuffer = (char*)ma_temp_buffer_get(&buffer), ma_temp_buffer_capacity(&buffer), DELETE_CHILD_DATA_RECORD_SQL_STATEMENT, instance_name ,path_id);

    if(!g_sqlite_lib->db_methods.db_sqlstmt_prepare(self->dbref, pbuffer, -1, &db_sql_stmt, NULL)) {     
		rc = (!g_sqlite_lib->db_methods.db_bind_text(db_sql_stmt, 1, key, strlen(key), NULL) &&
			 (SQLITE_DONE == g_sqlite_lib->db_methods.db_step(db_sql_stmt))) 
			 ? MA_OK : MA_ERROR_DS_KEY_NOT_FOUND ;

        g_sqlite_lib->db_methods.db_sqlstmt_finalize(db_sql_stmt);
    }

    ma_temp_buffer_uninit(&buffer);
    return rc;
}

#define GET_SUB_PATH_ITERATOR_STATEMENT   "SELECT DISTINCT getsubpath(PATH,'%s') FROM %s_PARENT WHERE PATH LIKE '%s\\%%'"

static ma_error_t db_sqlite_prepare_path_iterator(ma_db_sqlite_t *self, const char *instance_name, const char *path, ma_db_sqlite_iterator_t *it) {
	ma_temp_buffer_t buffer = {0};
    char *pbuffer = NULL;
    SQL_STMT *db_sql_stmt = NULL; 
	ma_error_t rc = MA_ERROR_DS_PATH_NOT_FOUND;

	ma_temp_buffer_init(&buffer);       
    ma_temp_buffer_reserve(&buffer, strlen(GET_PATH_ID_SQL_STATEMENT) + strlen(instance_name) + 2 * strlen(path));        
    MA_MSC_SELECT(_snprintf, snprintf)(pbuffer = (char*)ma_temp_buffer_get(&buffer), ma_temp_buffer_capacity(&buffer), GET_SUB_PATH_ITERATOR_STATEMENT, path, instance_name, path);
    if(0 == g_sqlite_lib->db_methods.db_sqlstmt_prepare(self->dbref, pbuffer, -1, &db_sql_stmt, NULL)) {
        it->db_sql_stmt = db_sql_stmt;
        rc = MA_OK;
    }
		  
	ma_temp_buffer_uninit(&buffer);
    return rc;
}

#define GET_CHILD_ITERATOR_STATEMENT  "SELECT NAME FROM %s_CHILD WHERE ID = %d"

static ma_error_t db_sqlite_prepare_child_iterator(ma_db_sqlite_t *self, const char *instance_name, const char *path, ma_db_sqlite_iterator_t *it) {
    long path_id = -1;
    size_t path_len = 0;
    ma_error_t rc = MA_ERROR_DS_PATH_NOT_FOUND;
    path_len = strl
        en(path);        
        
    /*get the path id */
    if(-1 != (path_id = db_get_path_id(self, instance_name, path, path_len))) {
        SQL_STMT *db_sql_stmt = NULL; 
        ma_temp_buffer_t buffer = {0};
        char *pbuffer = NULL;
        
        ma_temp_buffer_init(&buffer);       
        ma_temp_buffer_reserve(&buffer, strlen(GET_PATH_ID_SQL_STATEMENT) + strlen(instance_name) + 16); /*16 for path_id     */    
        MA_MSC_SELECT(_snprintf, snprintf)(pbuffer = (char*)ma_temp_buffer_get(&buffer), ma_temp_buffer_capacity(&buffer), GET_CHILD_ITERATOR_STATEMENT, instance_name, path_id);
        if(0 == g_sqlite_lib->db_methods.db_sqlstmt_prepare(self->dbref, pbuffer, -1, &db_sql_stmt, NULL)) {
            it->db_sql_stmt = db_sql_stmt;
            rc = MA_OK;
        }
    }
    return rc;
}

static ma_error_t db_sqlite_iterator_get_next(ma_ds_iterator_t *iterator, ma_buffer_t **buffer) {
	if(iterator && buffer) {
		ma_db_sqlite_iterator_t *self = (ma_db_sqlite_iterator_t *)iterator;
		if(SQLITE_ROW == g_sqlite_lib->db_methods.db_step(self->db_sql_stmt)) {
			ma_buffer_t *temp_buffer = NULL ;
			size_t len = g_sqlite_lib->db_methods.db_column_bytes(self->db_sql_stmt, 0);
			char *p =  (char*)g_sqlite_lib->db_methods.db_column_blob(self->db_sql_stmt, 0);
            if(self->is_path_iterator && self->path) {
                size_t path_len = strlen(self->path) + 1;/*1 for slash in the path */
            }
			ma_buffer_create(&temp_buffer, len);
			ma_buffer_set(temp_buffer,p ,len);
			*buffer = temp_buffer;
			return MA_OK;
		}
		return MA_ERROR_DS_NO_MORE_ITEMS;
	}
	return MA_ERROR_INVALIDARG;
}

static ma_error_t db_sqlite_iterator_release(ma_ds_iterator_t *iterator) {
	if(iterator) {
		ma_db_sqlite_iterator_t *self = (ma_db_sqlite_iterator_t *)iterator;

		g_sqlite_lib->db_methods.db_sqlstmt_reset(self->db_sql_stmt);
		g_sqlite_lib->db_methods.db_sqlstmt_finalize(self->db_sql_stmt);
        if(self->path) free(self->path);
		free(self);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}



static int sqlite_busy_handler(void *cb_data, int count) {
    return 0;
}

static ma_error_t initialize_sqlite_functions(ma_sqlite_lib_t *self ) {
    unsigned short int rc = 1;
    rc = 
        (self->db_methods.db_open =  (DB_OPEN_PROC)ma_dl_sym(&self->lib_handle, "sqlite3_open_v2" ))  &&        
        (self->db_methods.db_exec =  (DB_EXECUTE_PROC)ma_dl_sym(&self->lib_handle, "sqlite3_exec" ))  &&
        (self->db_methods.db_close=  (DB_CLOSE_PROC)ma_dl_sym(&self->lib_handle, "sqlite3_close" ))  &&
        (self->db_methods.db_shutdown =  (DB_SHUTDOWN_PROC)ma_dl_sym(&self->lib_handle, "sqlite3_shutdown" ))  &&
        (self->db_methods.db_set_wait =  (DB_WAIT_PROC)ma_dl_sym(&self->lib_handle, "sqlite3_busy_timeout" ))  &&
        (self->db_methods.db_set_wait_handler =  (DB_WAIT_HANDLER_PROC)ma_dl_sym(&self->lib_handle, "sqlite3_busy_handler" ))  &&
        (self->db_methods.db_sqlstmt_prepare =  (DB_PREPARE_SQLSTMT_PROC)ma_dl_sym(&self->lib_handle, "sqlite3_prepare_v2" ))  &&
        (self->db_methods.db_bind_int =  (DB_BIND_INT_PROC)ma_dl_sym(&self->lib_handle, "sqlite3_bind_int" ))  &&        
        (self->db_methods.db_bind_text =  (DB_BIND_TEXT_PROC)ma_dl_sym(&self->lib_handle, "sqlite3_bind_text" ))    &&        
        (self->db_methods.db_bind_blob =  (DB_BIND_BLOB_PROC)ma_dl_sym(&self->lib_handle, "sqlite3_bind_blob" ))    &&        
        (self->db_methods.db_step =  (DB_STEP_PROC)ma_dl_sym(&self->lib_handle, "sqlite3_step" ))    &&
        (self->db_methods.db_sqlstmt_finalize =  (DB_FINALIZE_PROC)ma_dl_sym(&self->lib_handle, "sqlite3_finalize" ))  &&
        (self->db_methods.db_sqlstmt_reset =  (DB_RESET_PROC)ma_dl_sym(&self->lib_handle, "sqlite3_reset" ))   &&        
        (self->db_methods.db_column_int =  (DB_COLUMN_INT_PROC)ma_dl_sym(&self->lib_handle, "sqlite3_column_int" ))    &&        
        (self->db_methods.db_column_text =  (DB_COLUMN_TEXT_PROC)ma_dl_sym(&self->lib_handle, "sqlite3_column_text" ))    &&
        (self->db_methods.db_column_blob =  (DB_COLUMN_BLOB_PROC)ma_dl_sym(&self->lib_handle, "sqlite3_column_blob" ))    &&
        (self->db_methods.db_column_bytes =  (DB_COLUMN_BYTES_PROC)ma_dl_sym(&self->lib_handle, "sqlite3_column_bytes" )) &&
		(self->db_methods.db_create_function = (DB_CREATE_FUNCTION) ma_dl_sym(&self->lib_handle, "sqlite3_create_function")) &&
		(self->db_methods.db_value_text = (DB_VALUE_TEXT) ma_dl_sym(&self->lib_handle, "sqlite3_value_text")) &&
		(self->db_methods.db_result_text = (DB_RESULT_TEXT) ma_dl_sym(&self->lib_handle, "sqlite3_result_text"))
      ;   
    return rc ? MA_OK : MA_ERROR_DS_DB_LOAD_LIB_FAILED; 
}

void getsubpath(sqlite3_context *context, int argc, sqlite3_value **argv) {
	if(argc == 2) {
		char sub_path[256] = {0};
		char *full_path = ( char *) g_sqlite_lib->db_methods.db_value_text(argv[0]);
		char *root_path = ( char *) g_sqlite_lib->db_methods.db_value_text(argv[1]);
		int root_path_len =  strlen(root_path);

		char *ptr = strstr(full_path, root_path);
		if(ptr == NULL){
			g_sqlite_lib->db_methods.db_result_text(context,full_path,strlen(full_path),SQLITE_TRANSIENT);
			return;
		}
		else {
			char *ptr2 = NULL;
			ptr += root_path_len;
			if(*(ptr) == MA_DATASTORE_PATH_C_SEPARATOR)
				ptr++;
			ptr2 = strstr(ptr, MA_DATASTORE_PATH_SEPARATOR);
			if(ptr2 == NULL){
				strcpy(sub_path, ptr);
			}
			else {
				int length = ptr2 - ptr;
				strncpy(sub_path,ptr, length);
			}
			g_sqlite_lib->db_methods.db_result_text(context, sub_path, strlen(sub_path), SQLITE_TRANSIENT);
			return;
		}

	}
}
