#ifndef MA_DB_SQLITE_H_INCLUDED
#define MA_DB_SQLITE_H_INCLUDED

#include "ma/ma_common.h"
#ifdef MA_DS_USE_DB_SQLITE
#include "ma/datastore/ma_ds_iterator.h"

MA_CPP(extern "C" {)

ma_error_t  ma_db_sqlite_initialize();
ma_error_t  ma_db_sqlite_open(const char *db_name, int flags, void **handle);
ma_error_t  ma_db_sqlite_create_instance(void *handle, const char *instance_name);
ma_error_t  ma_db_sqlite_close_instance(void *handle, const char *instance_name);
ma_error_t  ma_db_sqlite_delete_instance(void *handle, const char *instance_name);
ma_error_t  ma_db_sqlite_close(void *handle);    
ma_error_t  ma_db_sqlite_transaction_begin(void *handle, const char *instance_name);
ma_error_t  ma_db_sqlite_transaction_cancel(void *handle, const char *instance_name);
ma_error_t  ma_db_sqlite_transaction_end(void *handle, const char *instance_name);
ma_error_t	ma_db_sqlite_get_iterator(void *handle, const char *instance_name, ma_uint8_t is_path_iterator, const char *path, ma_ds_iterator_t **iterator);
ma_error_t  ma_db_sqlite_set(void *handle, const char *instance_name, const char *path, const char *key, ma_variant_t *value);
ma_error_t  ma_db_sqlite_get(void *handle, const char *instance_name, const char *path, const char *key, ma_vartype_t type, ma_variant_t **value);
ma_error_t  ma_db_sqlite_remove(void *handle, const char *instance_name, const char *path, const char *key);
ma_error_t  ma_db_sqlite_deinitialize();


#define MA_DB_INITIALIZE            ma_db_sqlite_initialize
#define MA_DB_OPEN                  ma_db_sqlite_open 
#define MA_DB_CREATE_INSTANCE       ma_db_sqlite_create_instance 
#define MA_DB_DELETE_INSTANCE       ma_db_sqlite_delete_instance 
#define MA_DB_CLOSE_INSTANCE        ma_db_sqlite_close_instance 
#define MA_DB_CLOSE                 ma_db_sqlite_close
#define MA_DB_TX_BEGIN              ma_db_sqlite_transaction_begin
#define MA_DB_TX_CANCEL             ma_db_sqlite_transaction_cancel
#define MA_DB_TX_END				ma_db_sqlite_transaction_end
#define MA_DB_GET_ITERATOR			ma_db_sqlite_get_iterator
#define MA_DB_SET                   ma_db_sqlite_set
#define MA_DB_GET                   ma_db_sqlite_get
#define MA_DB_REMOVE                ma_db_sqlite_remove
#define MA_DB_DEINITIALIZE          ma_db_sqlite_deinitialize



MA_CPP(})

#endif /*   MA_DS_USE_DB_SQLITE */

#endif	/* MA_DB_SQLITE_H_INCLUDED */

