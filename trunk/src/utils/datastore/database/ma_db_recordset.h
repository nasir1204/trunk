#ifndef MA_DB_RECORDSET_H_INCLUDED
#define MA_DB_RECORDSET_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_variant.h"

MA_CPP(extern "C" {)

typedef struct ma_db_recordset_s ma_db_recordset_t, *ma_db_recordset_h;

ma_error_t ma_db_recordset_create(ma_db_recordset_t **recordset);

ma_error_t ma_db_recordset_release(ma_db_recordset_t *recordset);

ma_error_t ma_db_recordset_get_column_count(ma_db_recordset_t *recordset, size_t *count);

ma_error_t ma_db_recordset_get_column_name(ma_db_recordset_t *recordset, size_t column_index, const char **column_name);

ma_error_t ma_db_recordset_get_string(ma_db_recordset_t *recordset, size_t column_index, const char **value);

ma_error_t ma_db_recordset_get_int16(ma_db_recordset_t *recordset, size_t column_index, ma_int16_t *value);

ma_error_t ma_db_recordset_get_int32(ma_db_recordset_t *recordset, size_t column_index, ma_int32_t *value);

ma_error_t ma_db_recordset_get_int64(ma_db_recordset_t *recordset, size_t column_index, ma_int64_t *value);

ma_error_t ma_db_recordset_get_blob(ma_db_recordset_t *recordset, size_t column_index, const void **value);

ma_error_t ma_db_recordset_get_variant(ma_db_recordset_t *recordset, size_t column_index, const ma_variant_t **value);

ma_error_t ma_db_recordset_next(ma_db_recordset_t *recordset);

MA_CPP(})


#endif /* MA_DB_RECORDSET_H_INCLUDED */