#ifndef MA_DB_STATEMENT_H_INCLUDED
#define MA_DB_STATEMENT_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_variant.h"
#include "ma_db_recordset.h"

MA_CPP(extern "C" {)

typedef struct ma_db_statement_s ma_db_statement_t, *ma_db_statement_h;

/**
 * db statement represent a single SQL statement pre-compiled into byte code for later execution.
 * The SQL statement may contain  parameters of the form "?". Such parameters represent 
 * unspecified literal values (or "wildcards") to be filled in later by the various setter methods 
 * defined in this interface. Each parameter has an associated index number which is its 
 * sequence in the statement. The first '?' parameter has index 1, the next has index 2 
 * and so on. A db_statement is created by calling ma_db_statement_create() withh con.
 * 
 * Consider this statement: 
 *  INSERT INTO employee(name, salary) VALUES(?, ?)
 * There are two < parameters in this statement, the parameter for setting the name has index 1 
 * and the one for the picture has index 2. To set the values for the  parameters we use a setter method.
 * Assuming name has a string value we use ma_db_statement_set_string(). To set the value
 * of the salary we submit a int value using the method ma_db_statement_set_int().
 * Note that string and blob parameter values are set by reference and 
 * MUST not "disappear" before either ma_db_statement_execute() or ma_db_statement_execute_query() is called.

 * Example:
 * ma_db_statement_t *db_stmt = ma_db_statement_create(db, "INSERT INTO employee(name, salary) VALUES(?, ?)");
 * ma_db_statement_set_string(p, 1, "Kamiya Kaoru");
 * ma_db_statement_set_int(p, 2, 100000);
 * ma_db_statement_execute(p);
 
 * Reuse
 * A db statement can be reused. That is, the method 
 * ma_db_statement() can be called one or more times to execute 
 * the same statement. Clients can also set new parameter values and
 * re-execute the statement as shown in this example:
 *
 * ma_db_statement_t *db_stmt = ma_db_statement_create(db, "INSERT INTO employee(name, salary) VALUES(?, ?)");
 * for (int i = 0; employees[i].name; i++) 
 * {
 *      ma_db_statement_set_string(p, 1, "Kamiya Kaoru");
 *      ma_db_statement_set_int(p, 2, 100000);
 *      ma_db_statement_execute(p)
 * }
 * 
 * Rercordset
 * Here is another example where we use a db statementto execute a query
 * which returns a recordset
 * 
 * ma_db_statement_t *db_stmt p = ma_db_statement_create(db, "SELECT id FROM employee WHERE name LIKE ?"); 
 * ma_db_statement_execute_query(db_stmt, &recordset);
 * while (MA_ERROR_NO_MORE_ITEMS != ma_ds_recordset_next(recordset));
 */

struct ma_db_t;

ma_error_t ma_db_statement_create(ma_db_t *handle, ma_db_statement_t **stmt);

ma_error_t ma_db_statement_release(ma_db_statement_t *stmt);

ma_error_t ma_db_statement_set_int(ma_db_statement_t *stmt, size_t parameter_index, ma_int64_t value);

ma_error_t ma_db_statement_set_string(ma_db_statement_t *stmt, size_t parameter_index, const char *value, size_t size);

ma_error_t ma_db_statement_set_blob(ma_db_statement_t *stmt, size_t parameter_index, const void *value, size_t size);

ma_error_t ma_db_statement_set_variant(ma_db_statement_t *stmt, size_t parameter_index, const ma_variant_t *value);

ma_error_t ma_db_statement_execute(ma_db_statement_t *stmt);

ma_error_t ma_db_statement_execute_query(ma_db_statement_t *stmt, ma_db_recordset_t **recordset);

MA_CPP(})

#endif /* MA_DB_STATEMENT_H_INCLUDED */
