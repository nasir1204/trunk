#include "ma/ma_datastore.h"
#include "ma/datastore/ma_ds.h"
#include "ma/datastore/ma_ds_iterator.h"

ma_error_t ma_ds_set_int(ma_ds_t *datastore, const char *path, const char *key, ma_int64_t value) {  
	if(datastore && path && key) {
		ma_variant_t *variant = NULL;
		ma_error_t rc = MA_OK;

		if(MA_OK == (rc = ma_variant_create_from_int64(value, &variant))) {
			rc = ma_ds_set(datastore,path,key,variant);
			ma_variant_release(variant);
		}
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_ds_set_str(ma_ds_t *datastore, const char *path, const char *key, const char *value, size_t size) {    
    if(datastore && path && key && value) {
		ma_variant_t *variant = NULL;
		ma_error_t rc = MA_OK;

		if(MA_OK == (rc = ma_variant_create_from_string(value, size,  &variant))) {
			rc = ma_ds_set(datastore,path,key,variant);
			ma_variant_release(variant);
		}
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_ds_set_wstr(ma_ds_t *datastore, const char *path, const char *key, const ma_wchar_t *value, size_t size) {
	if(datastore && path && key && value) {
		ma_variant_t *variant = NULL;
		ma_error_t rc = MA_OK;

		if(MA_OK == (rc = ma_variant_create_from_wstring(value, size,  &variant))) {
			rc = ma_ds_set(datastore,path,key,variant);
			ma_variant_release(variant);
		}
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_ds_set_blob(ma_ds_t *datastore, const char *path, const char *key, const void *value, size_t size) {
	if(datastore && path && key && value) {
		ma_variant_t *variant = NULL;
		ma_error_t rc = MA_OK;

		if(MA_OK == (rc = ma_variant_create_from_raw(value, size,  &variant))) {
			rc = ma_ds_set(datastore,path,key,variant);
			ma_variant_release(variant);
		}
		return rc;
	}
	return MA_ERROR_INVALIDARG;

}

ma_error_t ma_ds_set_variant(ma_ds_t *datastore, const char *path, const char *key, ma_variant_t *value) {
	return (datastore && path && key && value)  ? ma_ds_set(datastore, path, key, value) : MA_ERROR_INVALIDARG;
}



ma_error_t ma_ds_get_int16(ma_ds_t *datastore, const char *path, const char *key, ma_int16_t *value){
    if(datastore && path && key && value) {
        ma_error_t rc = MA_OK;
		ma_variant_t *variant = NULL;
		
		if(MA_OK == (rc = ma_ds_get(datastore, path, key, MA_VARTYPE_INT16, &variant))) {
			rc = ma_variant_get_int16(variant,value);
			ma_variant_release(variant);
		}
		return rc;
    }    
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_ds_get_int32(ma_ds_t *datastore, const char *path, const char *key, ma_int32_t *value) {
	if(datastore && path && key && value) {
        ma_error_t rc = MA_OK;
		ma_variant_t *variant = NULL;
		
		if(MA_OK == (rc = ma_ds_get(datastore, path, key, MA_VARTYPE_INT32, &variant))) {
			rc = ma_variant_get_int32(variant,value);
			ma_variant_release(variant);
		}
		return rc;
    }    
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_ds_get_int64(ma_ds_t *datastore, const char *path, const char *key, ma_int64_t *value) {
    if(datastore && path && key && value) {
        ma_error_t rc = MA_OK;
		ma_variant_t *variant = NULL;
		
		if(MA_OK == (rc = ma_ds_get(datastore, path, key, MA_VARTYPE_INT64, &variant))) {
			rc = ma_variant_get_int64(variant,value);
			ma_variant_release(variant);
		}
		return rc;
    }    
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_ds_get_str(ma_ds_t *datastore, const char *path, const char *key, ma_buffer_t **value) {
	if(datastore && path && key && value) {
        ma_error_t rc = MA_OK;
		ma_variant_t *variant = NULL;
		
		if(MA_OK == (rc = ma_ds_get(datastore, path, key, MA_VARTYPE_STRING, &variant))) {
			rc = ma_variant_get_string_buffer(variant,value);
			ma_variant_release(variant);
		}
		return rc;
    }    
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_ds_get_wstr(ma_ds_t *datastore, const char *path, const char *key, ma_wbuffer_t **value) {
    if(datastore && path && key && value) {
        ma_error_t rc = MA_OK;
		ma_variant_t *variant = NULL;
		
		if(MA_OK == (rc = ma_ds_get(datastore, path, key, MA_VARTYPE_STRING, &variant))) {
			rc = ma_variant_get_wstring_buffer(variant,value);
			ma_variant_release(variant);
		}
		return rc;
    }    
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_ds_get_blob(ma_ds_t *datastore, const char *path, const char *key, ma_buffer_t **value) {
     if(datastore && path && key && value) {
        ma_error_t rc = MA_OK;
		ma_variant_t *variant = NULL;
		
		if(MA_OK == (rc = ma_ds_get(datastore, path, key, MA_VARTYPE_RAW, &variant))) {
			rc = ma_variant_get_raw_buffer(variant,value);
			ma_variant_release(variant);
		}
		return rc;
    }    
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_ds_get_variant(ma_ds_t *datastore, const char *path, const char *key,ma_vartype_t var_type, ma_variant_t **value) {
	return (datastore && path && key && value) ? ma_ds_get(datastore, path, key, var_type, value) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_ds_rem(ma_ds_t *datastore, const char *path, const char *key, unsigned short need_recursive_remove) {
	if(datastore && path) {
		/* If recursive remove is not mentioned then key should be present */
		if((0 == need_recursive_remove) && !key)
			return MA_ERROR_INVALIDARG;
		return	ma_ds_remove(datastore, path, key);
	}
	return MA_ERROR_INVALIDARG;
}


ma_error_t ma_ds_is_path_exists(ma_ds_t *datastore, const char *path, ma_bool_t *is_exists) {
	if(datastore && path && is_exists) {
		return	ma_ds_entry_exists(datastore, path, NULL, is_exists);
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_ds_is_key_exists(ma_ds_t *datastore, const char *path, const char *key, ma_bool_t *is_exists) {
	if(datastore && path && key &&  is_exists) {
		return	ma_ds_entry_exists(datastore, path, key, is_exists);
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_ds_iterator_create(ma_ds_t *datastore, ma_uint8_t is_path_iterator, const char *path, ma_ds_iterator_t **iterator) {
	return ma_ds_get_iterator(datastore,is_path_iterator,path,iterator);
}

ma_error_t ma_ds_iterator_get_next(ma_ds_iterator_t *iterator, ma_buffer_t **buffer) {
	if(!iterator || !buffer) return MA_ERROR_PRECONDITION;
	return iterator->methods->get_next ? iterator->methods->get_next(iterator, buffer) : MA_ERROR_NOT_SUPPORTED;
}

ma_error_t ma_ds_iterator_release(ma_ds_iterator_t *iterator) {
	if(!iterator ) return MA_ERROR_PRECONDITION;
	return iterator->methods->release ? iterator->methods->release(iterator) : MA_ERROR_NOT_SUPPORTED;
}

