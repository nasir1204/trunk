#include "ma/internal/utils/context/ma_context.h"
#include "ma/internal/utils/datastructures/ma_queue.h"
#include "ma/internal/ma_macros.h"

#include <stdlib.h>
#include <string.h>

#ifndef MA_WINDOWS
#include <strings.h> /*strcasecmp*/
#endif

struct ma_context_s {
    ma_queue_t   qhead;
};

struct object_info_s {
    ma_queue_t qlink;
    char const *name;
    void * const*object_address;
};


/*!
 * creates a context
 */
ma_error_t ma_context_create(ma_context_t **context) {
    ma_context_t *self = (ma_context_t *)calloc(1, sizeof(ma_context_t));
    if (self) {
        ma_queue_init(&self->qhead);
        *context = self;
        return MA_OK;
    }
    return MA_ERROR_OUTOFMEMORY;
}


/*!
 * adds a mapping (string -> void**)
 * Note this implementation assumes that 'name' remains in memory
 * note this is taking the address of the object pointer
 */
ma_error_t ma_context_add_object_info(ma_context_t *self, char const *object_name, void * const *address) {
    if (self && object_name && address) {
        struct object_info_s *oi = (struct object_info_s *)calloc(1, sizeof(struct object_info_s));
        if (oi) {
            oi->name = object_name;
            oi->object_address = address;
            ma_queue_insert_tail(&self->qhead,&oi->qlink);
            return MA_OK;
        }
        return MA_ERROR_OUTOFMEMORY;
    }
    return MA_ERROR_INVALIDARG;
}

void *ma_context_get_object_info(ma_context_t *self, const char *object_name) {
    if(self && object_name) {
        ma_queue_t          *item;
        ma_queue_foreach(item, &self->qhead) {
            struct object_info_s *object_info = ma_queue_data(item, struct object_info_s, qlink);
            if (0==MA_WIN_SELECT(stricmp,strcasecmp)(object_info->name, object_name)) {
                return *object_info->object_address;
            }
        }
    }
    return 0;
}

ma_error_t ma_context_release(ma_context_t *self) {
    if(self) {
        while (!ma_queue_empty(&self->qhead)) {
            ma_queue_t *item = ma_queue_last(&self->qhead);
            ma_queue_remove(item);
            free(ma_queue_data(item,struct object_info_s, qlink));
        }
        free(self);

        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}
