#include "ma/internal/utils/datastructures/ma_hash.h"

#include <stdlib.h>

ma_uint32_t ma_hash_compute(const char *key, int len) {
    if(key) {
        unsigned char *p = (unsigned char*)key;
        ma_uint32_t h = 0;
        int i;

        for (i = 0; i < len; i++)
            h = 33 * h ^ p[i];

        return h;
    }
    return 0;
}

