#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include "ma/internal/utils/datastructures/ma_stream.h"


// implemention of memory stream
typedef struct ma_mstream_s {
	ma_stream_t base;
	ma_bytebuffer_t *buffer;
	size_t initial_size;
	size_t offset;		
}ma_mstream_t;

static ma_error_t ma_mstream_write(ma_stream_t *stream,const unsigned char *data,size_t size,size_t *writtenbytes);
static ma_error_t ma_mstream_read(ma_stream_t *stream,unsigned char *data,size_t size,size_t *readbytes);
static ma_error_t ma_mstream_seek(ma_stream_t *stream,size_t offset);
static ma_error_t ma_mstream_get_size(ma_stream_t *stream,size_t *size);
static ma_error_t ma_mstream_get_bytebuffer(ma_stream_t *stream,ma_bytebuffer_t**);	
static ma_error_t ma_mstream_release(ma_stream_t *stream);


static ma_stream_methods_t mstream_methods={
	&ma_mstream_write,
	&ma_mstream_read,
	&ma_mstream_seek,
	&ma_mstream_get_size,
	&ma_mstream_get_bytebuffer,
	&ma_mstream_release
};


ma_error_t ma_mstream_create(size_t initialsize ,void *userdata,ma_stream_t **stream){	
	if(stream && 0 != initialsize){
		ma_error_t err=MA_OK;
		ma_bytebuffer_t *buffer=NULL;
		ma_mstream_t *tmp=NULL;
		if( MA_OK == (err = ma_bytebuffer_create(initialsize,&buffer) ) && NULL != (tmp = (ma_mstream_t*)calloc(1,sizeof(ma_mstream_t)) )  ) {
			tmp->base.vtable=&mstream_methods;	
			tmp->base.userdata=userdata;			
			tmp->buffer=buffer;		
			tmp->initial_size=initialsize;			
			tmp->offset=0;			
		}
		else {
			if(buffer) ma_bytebuffer_release(buffer);
			return MA_ERROR_OUTOFMEMORY;
		}
        tmp->base.ref_count = 1;
		*stream=(ma_stream_t*)tmp;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_mstream_release(ma_stream_t *stream){
	ma_mstream_t *self = (ma_mstream_t *)stream;
	ma_bytebuffer_release(self->buffer);
	free(self);
	return MA_OK;
}

ma_error_t ma_mstream_write(ma_stream_t *stream,const unsigned char *data,size_t len,size_t *writtenbytes){
	ma_error_t err=MA_OK;
	ma_mstream_t *self = (ma_mstream_t *)stream;
	err=ma_bytebuffer_set_bytes(self->buffer,self->offset,data,len);
	if(MA_OK == err){
		self->offset+=len;		
		*writtenbytes=len;
	}
	else if( MA_ERROR_INSUFFICIENT_SIZE == err ) { //if size is not sufficient then we increase the buffer size.			
		if ( MA_OK != (err=ma_bytebuffer_reserve(self->buffer,ma_bytebuffer_get_size(self->buffer) + self->initial_size + len ) ) )
			return err;		
		if ( MA_OK == (err=ma_bytebuffer_set_bytes(self->buffer,self->offset,data,len) ) ) { //after write reset the read offset.
			self->offset+=len;			
			*writtenbytes=len;
		}
		return err;
	}
	return err;
}

ma_error_t ma_mstream_read(ma_stream_t *stream,unsigned char *data,size_t len,size_t *readbytes){	
	size_t datatoread=0;
	ma_mstream_t *self = (ma_mstream_t *)stream;
	const unsigned char *buffer=ma_bytebuffer_get_bytes(self->buffer);	

	datatoread=ma_bytebuffer_get_size(self->buffer) - self->offset;
	if(0 == datatoread){  //no more data to read.
		*readbytes=0;
		return MA_ERROR_STREAM_EOF;
	}	
	else if( datatoread >= len ){        // sufficient data to read.
		memcpy(data,buffer+self->offset,len);
		self->offset+=len;
		*readbytes=len;
	}
	else{                  // we have less data then the passed buffer.
		memcpy( data, buffer+self->offset,datatoread);
		self->offset+=datatoread;
		*readbytes=datatoread;
	}
	return MA_OK;

}
ma_error_t ma_mstream_get_bytebuffer(ma_stream_t *stream,ma_bytebuffer_t **buffer){
	ma_mstream_t *self = (ma_mstream_t *)stream;
	*buffer=self->buffer;
	 ma_bytebuffer_add_ref(self->buffer);
	 return MA_OK;
}

 ma_error_t ma_mstream_seek(ma_stream_t *stream,size_t offset){	
	ma_mstream_t *self = (ma_mstream_t *)stream;
	if(offset >= ma_bytebuffer_get_capacity(self->buffer) )
		return MA_ERROR_STREAM_EOF;
	self->offset=offset;
	return MA_OK;
}

ma_error_t ma_mstream_get_size(ma_stream_t *stream,size_t *size){
	ma_mstream_t *self = (ma_mstream_t *)stream;
	*size=ma_bytebuffer_get_size(self->buffer);		
	return MA_OK;
}

