#include "ma/internal/utils/datastructures/ma_temp_buffer.h"

#include <string.h>
#include <stdlib.h>

const static ma_temp_buffer_t null_buffer = MA_TEMP_BUFFER_INIT;

void ma_temp_buffer_init(ma_temp_buffer_t *b) {
    *b = null_buffer;
    /* for Wuzzies: */
    memset(&b->b.static_buffer, 0, sizeof(b->b.static_buffer) );
}

void ma_temp_buffer_uninit(ma_temp_buffer_t *b) {
    if (b->extra) {
		b->b.free_fn(b->extra);
    }
    *b = null_buffer;
}

size_t ma_temp_buffer_capacity(ma_temp_buffer_t *b) {
    return b->capacity;
}

#define GET_BUFFER(x) ((x)->extra ? (x)->extra : (x)->b.static_buffer) 

void *ma_temp_buffer_get(ma_temp_buffer_t *b) {
    return GET_BUFFER(b);
}
/*!
 * returns the address of the buffer. This is useful if memory is allocated outside of ma_temp_buffer. You must provide a deallocation function for freeing the memory though...  
 */
void **ma_temp_buffer_address_of(ma_temp_buffer_t *b, void (*free_fn)(void*) ) {
    ma_temp_buffer_uninit(b);
    return &b->extra;
}

static void ma_free(void *p) {free(p);}

void ma_temp_buffer_reserve(ma_temp_buffer_t *b, size_t bytes_requested) {
    if (b->capacity < bytes_requested) {
        void *p = calloc(1, bytes_requested);
        if (p) {
            ma_temp_buffer_uninit(b);
            b->capacity = bytes_requested;
            b->extra = p;
            b->b.free_fn = &ma_free;
        }
    }
}

void ma_temp_buffer_copy(ma_temp_buffer_t *dest, const ma_temp_buffer_t *source) {    
    ma_temp_buffer_uninit(dest);
    ma_temp_buffer_reserve(dest, source->capacity);
    memcpy(ma_temp_buffer_get(dest), GET_BUFFER(source), source->capacity);    
}

