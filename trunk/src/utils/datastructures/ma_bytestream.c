#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ma/internal/utils/datastructures/ma_bytestream.h"
#include "ma/internal/utils/text/ma_byteorder.h"

struct ma_bytestream_s{
	ma_bytestream_endianness_t endianness;
	ma_bool_t is_own_stream;
	ma_stream_t *stream;
};

ma_error_t ma_bytestream_create(ma_bytestream_endianness_t byteorder,size_t buffer_size,ma_bytestream_t **bstream){
	if(bstream){		
		ma_bytestream_t *tmp=NULL ; 
		ma_stream_t *mstream=NULL;
		if(NULL != (tmp=(ma_bytestream_t*)calloc(1,sizeof(ma_bytestream_t)))){
			tmp->endianness=byteorder;
			tmp->stream=NULL;			
			tmp->is_own_stream=MA_FALSE;
		}
		else 			
			return MA_ERROR_OUTOFMEMORY;
		if(buffer_size){
			if (MA_OK == ma_mstream_create(buffer_size,NULL,&mstream) ){
				tmp->stream=mstream;
				tmp->is_own_stream=MA_TRUE;
			}
			else{
				free(tmp);
				return MA_ERROR_OUTOFMEMORY;
			}
		}
		*bstream=tmp;
		return MA_OK;
			
	}
	return MA_ERROR_INVALIDARG;
}
ma_error_t ma_bytestream_set_stream(ma_bytestream_t *self,ma_stream_t *streamtouse){
	if(self && streamtouse){
		if(self->is_own_stream)
			ma_stream_release(self->stream);
		self->stream=streamtouse;
		self->is_own_stream=MA_FALSE;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_bytestream_release(ma_bytestream_t *self){
	if(self){		
		if(self->is_own_stream)
			ma_stream_release(self->stream);
		free(self);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

#define MA_BYTESTREAM_GENERATE_READ_INTEGER_FUNCTION(inttype) \
ma_error_t ma_bytestream_read_##inttype(ma_bytestream_t *self,ma_##inttype##_t *data){		 \
	if(self && data){ \
		ma_error_t err=MA_OK; \
		ma_##inttype##_t stream_data=0; \
		/*size_t read=0;*/ \
		if( MA_OK == (err = ma_bytestream_read_buffer(self,(unsigned char*)&stream_data,sizeof(ma_##inttype##_t),NULL ) ) ) \
		*data=( (self->endianness == MA_LITTLE_ENDIAN) ? ma_byteorder_le_to_host_##inttype(stream_data) : ma_byteorder_be_to_host_##inttype(stream_data) );		 \
		return err; \
	} \
	return MA_ERROR_INVALIDARG; \
}

#define MA_BYTESTREAM_GENERATE_WRITE_INTEGER_FUNCTION(inttype) \
ma_error_t ma_bytestream_write_##inttype(ma_bytestream_t *self,ma_##inttype##_t data){	\
	if(self ){ \
		/*size_t written=0;*/ \
		ma_##inttype##_t stream_data = ( (self->endianness == MA_LITTLE_ENDIAN) ? ma_byteorder_host_to_le_##inttype(data) : ma_byteorder_host_to_be_##inttype(data) ); \
		return ma_bytestream_write_buffer(self,(const unsigned char*)&stream_data,sizeof(ma_##inttype##_t),NULL);			\
	} \
	return MA_ERROR_INVALIDARG;\
}

MA_BYTESTREAM_GENERATE_WRITE_INTEGER_FUNCTION(uint16)
MA_BYTESTREAM_GENERATE_WRITE_INTEGER_FUNCTION(uint32)
MA_BYTESTREAM_GENERATE_WRITE_INTEGER_FUNCTION(uint64)
MA_BYTESTREAM_GENERATE_READ_INTEGER_FUNCTION(uint16)
MA_BYTESTREAM_GENERATE_READ_INTEGER_FUNCTION(uint32)
MA_BYTESTREAM_GENERATE_READ_INTEGER_FUNCTION(uint64)

static ma_error_t ma_bytestream_readwrite_buffer(ma_bytestream_t *self,unsigned char *data,size_t len ,size_t *processedbytes,ma_bool_t is_read){
	if(self && data){		
		size_t process_tillnow=0;
		while(len != process_tillnow){
			size_t process_now=0;
			is_read ? ma_stream_read(self->stream,data + process_tillnow,len-process_tillnow,&process_now) : ma_stream_write(self->stream,data + process_tillnow,len-process_tillnow,&process_now);		
			if(0 == process_now){
				if(processedbytes)*processedbytes=process_tillnow;
				return is_read ? MA_ERROR_STREAM_EOF : MA_ERROR_STREAM_WRITE_ERROR;
			}				
			process_tillnow+=process_now;
		}
		if(processedbytes)*processedbytes=process_tillnow;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_bytestream_write_buffer(ma_bytestream_t *self,const unsigned char *data,size_t len ,size_t *written){
	return ma_bytestream_readwrite_buffer(self,(unsigned char*)data,len,written,MA_FALSE);
}
ma_error_t ma_bytestream_read_buffer(ma_bytestream_t *self,unsigned char *data,size_t len,size_t *readbytes){
	return ma_bytestream_readwrite_buffer(self,data,len,readbytes,MA_TRUE);
}

ma_error_t ma_bytestream_seek(ma_bytestream_t *self,size_t offset){
	if(self){
		return ma_stream_seek(self->stream,offset);
	}
	return MA_ERROR_INVALIDARG;
}

ma_bytebuffer_t *ma_bytestream_get_buffer(ma_bytestream_t *self){
	if(self){
		ma_bytebuffer_t *buff=NULL;
		ma_stream_get_buffer(self->stream,&buff);
		return buff;
	}
	return NULL;
}

size_t ma_bytestream_get_size(ma_bytestream_t *self){
	if(self){
		size_t size=0;
		ma_stream_get_size(self->stream,&size);
		return size;
	}
	return -1;
}
