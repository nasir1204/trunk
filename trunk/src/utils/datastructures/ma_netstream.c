#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include "ma/internal/utils/datastructures/ma_stream.h"

typedef struct ma_netstream_s {	
	ma_stream_t         base;
	ma_netstream_cb_t   cb;
} ma_netstream_t;

static ma_error_t ma_netstream_write(ma_netstream_t *self,const unsigned char *ptr, size_t size, size_t *written_bytes);
static ma_error_t ma_netstream_read(ma_netstream_t *self,unsigned char *ptr, size_t size, size_t *read_bytes);
static ma_error_t ma_netstream_seek(ma_netstream_t *self,size_t offset) ;    	
static ma_error_t ma_netstream_get_size(ma_netstream_t *self,size_t *size);
static ma_error_t ma_netstream_get_bytebuffer(ma_netstream_t *self,ma_bytebuffer_t**);	
static ma_error_t ma_netstream_release(ma_netstream_t *self);

#pragma warning(disable : 4028)

static ma_stream_methods_t net_stream_methods={
	&ma_netstream_write,
	&ma_netstream_read,
	&ma_netstream_seek,
	&ma_netstream_get_size,
    &ma_netstream_get_bytebuffer,	
	&ma_netstream_release
};

#pragma warning(default : 4028) 


ma_error_t ma_netstream_create(ma_netstream_cb_t cb,  void *userdata, ma_stream_t **stream){	
	if(cb && stream) {
		ma_netstream_t *handle = NULL;
		
		if ( NULL == ( handle = (ma_netstream_t*)calloc(1,sizeof(ma_netstream_t) ) ) )
			return MA_ERROR_OUTOFMEMORY;
        		
		handle->base.vtable = &net_stream_methods;
		handle->cb = cb;
		handle->base.userdata=userdata;
        handle->base.ref_count = 1;
		*stream = (ma_stream_t*)handle;		
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_netstream_write(ma_netstream_t *self,const unsigned char *ptr, size_t size, size_t *written_bytes){	
    if(self->cb) 
        *written_bytes = self->cb(ptr, size, self->base.userdata);
	return MA_OK;
}

/* TBD - will add read support too */
ma_error_t ma_netstream_read(ma_netstream_t *self,unsigned char *ptr, size_t size, size_t *read_bytes){	
	return MA_ERROR_INVALID_OPTION;
}

ma_error_t ma_netstream_seek(ma_netstream_t *self,size_t offset){
    return MA_ERROR_INVALID_OPTION;	
}

ma_error_t ma_netstream_get_size(ma_netstream_t *self,size_t *size){
	return MA_ERROR_INVALID_OPTION;
}

ma_error_t ma_netstream_get_bytebuffer(ma_netstream_t *self, ma_bytebuffer_t **buffer){
	return MA_ERROR_INVALID_OPTION;
}

ma_error_t ma_netstream_release(ma_netstream_t *self){
    self->cb = NULL;
	free(self);
	return MA_OK;
}

