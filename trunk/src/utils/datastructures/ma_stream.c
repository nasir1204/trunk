#include <stdio.h>
#include <stdlib.h>
#include "ma/internal/utils/datastructures/ma_stream.h"

ma_error_t ma_stream_write(ma_stream_t *self,const unsigned char *ptr, size_t size,size_t *written_bytes){
	if(self && ptr && written_bytes){
			return self->vtable->write(self,ptr,size,written_bytes);
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_stream_read(ma_stream_t *self,unsigned char *ptr, size_t size,size_t *read_bytes){
	if(self && ptr && read_bytes ){
			return self->vtable->read(self,ptr,size,read_bytes);
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_stream_seek(ma_stream_t *self, size_t offset) {
	if(self){
			return self->vtable->seek(self,offset);
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_stream_get_size(ma_stream_t *self,size_t *size){
	if(self && size){
		return self->vtable->get_size(self,size);
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_stream_get_buffer(ma_stream_t *self,ma_bytebuffer_t **buffer){
	if(self && buffer){
		return self->vtable->get_buffer(self,buffer);
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_stream_release(ma_stream_t *self){
	if(self && (0 <= self->ref_count)){
        if (0 == MA_ATOMIC_DECREMENT(self->ref_count))
		    return self->vtable->release(self);
        return MA_OK;
	}
	return MA_ERROR_INVALIDARG;   
}

ma_error_t ma_stream_add_ref(ma_stream_t *self) {
    if(self && (0 <= self->ref_count)){
        MA_ATOMIC_INCREMENT(self->ref_count);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

