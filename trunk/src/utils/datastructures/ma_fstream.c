#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include "ma/internal/utils/datastructures/ma_stream.h"

typedef struct ma_fstream_s ma_fstream_t;
struct ma_fstream_s {	
	ma_stream_t base;
	char *filename;
	FILE *filehandle;
};

static ma_error_t ma_fstream_write(ma_stream_t *stream,const unsigned char *ptr, size_t size, size_t *written_bytes);
static ma_error_t ma_fstream_read(ma_stream_t *stream,unsigned char *ptr, size_t size, size_t *read_bytes);
static ma_error_t ma_fstream_seek(ma_stream_t *stream,size_t offset) ;    	
static ma_error_t ma_fstream_get_size(ma_stream_t *stream,size_t *size);
static ma_error_t ma_fstream_get_bytebuffer(ma_stream_t *stream,ma_bytebuffer_t**);	
static ma_error_t ma_fstream_release(ma_stream_t *stream);


static ma_stream_methods_t file_stream_methods={
	&ma_fstream_write,
	&ma_fstream_read,
	&ma_fstream_seek,
	&ma_fstream_get_size,
    &ma_fstream_get_bytebuffer,	
	&ma_fstream_release
};


const static char *file_access_mode[]={NULL,"rb","wb","wb+","ab","ab+","ab","ab+"};

ma_error_t ma_fstream_create(const char *filename , int falgs, void *userdata,ma_stream_t **stream){	
	if(filename && stream && falgs >0 && falgs < 8) {
		ma_fstream_t *handle = NULL;
		
		if ( NULL == ( handle = (ma_fstream_t*)calloc(1,sizeof(ma_fstream_t) ) ) )
			return MA_ERROR_OUTOFMEMORY;

		if ( NULL == ( handle->filehandle = fopen(filename,file_access_mode[falgs]) ) ) {
			free(handle);
			return MA_ERROR_FILE_OPEN_FAILED;
		}		
		handle->base.vtable = &file_stream_methods;
		handle->filename=strdup(filename);
		handle->base.userdata=userdata;
        handle->base.ref_count = 1;
		*stream = (ma_stream_t*)handle;		
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_fstream_write(ma_stream_t *stream,const unsigned char *ptr, size_t size, size_t *written_bytes){
	ma_fstream_t *self = (ma_fstream_t *)stream;
	*written_bytes = fwrite(ptr,1,size,self->filehandle);
	return MA_OK;
}

ma_error_t ma_fstream_read(ma_stream_t *stream,unsigned char *ptr, size_t size, size_t *read_bytes){
	ma_fstream_t *self = (ma_fstream_t *)stream;
	*read_bytes = fread(ptr,1,size,self->filehandle);
	if( 0  ==  *read_bytes )
		return MA_ERROR_STREAM_EOF;
	return MA_OK;
}

ma_error_t ma_fstream_seek(ma_stream_t *stream,size_t offset){
	ma_fstream_t *self = (ma_fstream_t *)stream;
	if(0 == fseek(self->filehandle, offset, SEEK_SET) )
        return MA_OK;	
    else
        return MA_ERROR_FILE_SEEK_FAILED ;
}

ma_error_t ma_fstream_get_size(ma_stream_t *stream,size_t *size){
	ma_fstream_t *self = (ma_fstream_t *)stream;
	long int currentpos=ftell(self->filehandle);
	if(-1 != currentpos){
		if ( 0 == fseek(self->filehandle,0,SEEK_END ) ){
			*size=ftell(self->filehandle);
			fseek(self->filehandle,currentpos,SEEK_SET);
			return MA_OK;
		}
	}
	return MA_ERROR_FILE_SEEK_FAILED;
}

ma_error_t ma_fstream_get_bytebuffer(ma_stream_t *stream,ma_bytebuffer_t **buffer){    
    ma_error_t rc = MA_OK;
    size_t size = 0;
    if(MA_OK == (rc = ma_fstream_get_size(stream, &size))) {
        if(MA_OK == (rc = ma_bytebuffer_create(size + 1, buffer))) {
            size_t read_bytes = 0;
            if(MA_OK == (rc = ma_fstream_read(stream, ma_bytebuffer_get_bytes(*buffer), size, &read_bytes))) {
                ma_bytebuffer_shrink(*buffer, size);
                return MA_OK;
            }
            ma_bytebuffer_release(*buffer); *buffer = NULL;
        }
    }
	return rc;
}

ma_error_t ma_fstream_release(ma_stream_t *stream){
    ma_fstream_t *self = (ma_fstream_t *)stream;
	fclose(self->filehandle);
	free(self->filename);
	free(self);
	return MA_OK;
}

