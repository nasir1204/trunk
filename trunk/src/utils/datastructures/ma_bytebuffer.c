#include "ma/internal/utils/datastructures/ma_bytebuffer.h"
#include "ma/internal/utils/threading/ma_atomic.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct ma_bytebuffer_s{
	unsigned char *start;			
	size_t len;
	size_t capacity;	
	ma_atomic_counter_t ref_count;
};

ma_error_t ma_bytebuffer_create(size_t capacity,ma_bytebuffer_t **byte_buffer){
	if(byte_buffer && 0 != capacity) {
		ma_bytebuffer_t *b_buff=(ma_bytebuffer_t*)calloc(1,sizeof(ma_bytebuffer_t) );
		if(b_buff) {
			if ( NULL != (b_buff->start=(unsigned char*)calloc(capacity,1) ) ){
				b_buff->len=0;
			}
			else {
				free(b_buff);
				return MA_ERROR_OUTOFMEMORY;
			}
		}
		else
			return MA_ERROR_OUTOFMEMORY;
		MA_ATOMIC_INCREMENT(b_buff->ref_count);
		b_buff->capacity=capacity;		
		*byte_buffer=b_buff;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_bytebuffer_add_ref(ma_bytebuffer_t *self){
	if(self){
		MA_ATOMIC_INCREMENT(self->ref_count);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}
ma_error_t ma_bytebuffer_reserve(ma_bytebuffer_t *self,size_t capacity){
	if(self && 0 != capacity){
		unsigned char *new_buffer=NULL;
		if(self->len > capacity) //if existing data is bigger than the new capacity then we return. We dont want to lose the data.
			return MA_ERROR_INSUFFICIENT_SIZE;
		if ( NULL != ( new_buffer=(unsigned char*)realloc(self->start,capacity) ) ) {
			self->start=new_buffer;	
			self->capacity=capacity;
		}
		else
			return MA_ERROR_OUTOFMEMORY;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_bytebuffer_shrink(ma_bytebuffer_t *self, size_t size){
	if(self){
		if(size > self->capacity )
			return MA_ERROR_INSUFFICIENT_SIZE;
		self->len=size;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_bytebuffer_append_bytes(ma_bytebuffer_t *self,const unsigned char *buffer,size_t len){
	if(self && buffer){
		if( (self->len + len) > self->capacity )
			return MA_ERROR_INSUFFICIENT_SIZE;
		memcpy(self->start + self->len,buffer,len);
		self->len+=len;		
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_bytebuffer_set_bytes(ma_bytebuffer_t *self,size_t offset,const unsigned char *buffer,size_t len){
	if(self && buffer){
		if( (offset + len) > self->capacity )
			return MA_ERROR_INSUFFICIENT_SIZE;
		memcpy(self->start + offset,buffer,len);
		if( (offset + len) > self->len) // if buffer writing cross the existing size then reset the size.
			self->len=offset + len;					
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

unsigned char *ma_bytebuffer_get_bytes(ma_bytebuffer_t *self){
	return self ? self->start : NULL;
}

size_t ma_bytebuffer_get_capacity(ma_bytebuffer_t *self){
	return self ? self->capacity : -1;	
}

size_t ma_bytebuffer_get_size(ma_bytebuffer_t *self){
	return self ? self->len : -1;
}

ma_error_t ma_bytebuffer_release(ma_bytebuffer_t *self){
	if(self){
		MA_ATOMIC_DECREMENT(self->ref_count);
		if( 0 == self->ref_count ){
			free(self->start);
			free(self);
		}
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

