#ifndef MA_APPLICATION_INTERNAL_H_
#define MA_APPLICATION_INTERNAL_H_
#include "ma/internal/utils/datastructures/ma_slist.h"

#ifndef MA_AGENT_SOFTWARE_ID
#define MA_AGENT_SOFTWARE_ID            "EPOAGENT3000"
#endif

#ifdef MA_UNIT_TEST_ENABLED
#ifdef MA_WINDOWS
#define MA_REGISTRY_STORE_SCAN_PATH				"Software\\"
#define MA_REGISTRY_STORE_AGENT_SCAN_PATH		"McAfee_T1\\Agent\\"
#define MA_REGISTRY_STORE_APPLICATON_LIST_PATH	"McAfee_T1\\Agent\\Applications\\"
#define MA_REGISTRY_STORE_APPLICATON_LIST_NA_PATH	"Network Associates\\ePolicy Orchestrator\\Application Plugins\\"
#define MA_REGISTRY_STORE_XML_PATH				    "Configuration"
#define MA_REGISTRY_STORE_USE_IPC					"Use Ipc"
#define MA_REGISTRY_STORE_PLIUGIN_PATH				"Plugin Path"
#else
#define CMA_REGISTRY_STORE_SCAN_PATH			"./AGENT/"
#define MA_REGISTRY_STORE_SCAN_PATH				"./AGENT/"
#define MA_REGISTRY_STORE_AGENT_SCAN_PATH		"EPOAGENT3000"
#define MA_REGISTRY_STORE_APPLICATON_LIST_PATH	"./AGENT/"
#define MA_REGISTRY_STORE_XML_PATH				"Configuration"
#define MA_REGISTRY_STORE_PLIUGIN_PATH			"Path"
#define MA_REGISTRY_STORE_USE_IPC				"UseIpc"
#endif
#else
#ifdef MA_WINDOWS
#define MA_REGISTRY_STORE_SCAN_PATH				    "Software\\"
#define MA_REGISTRY_STORE_AGENT_SCAN_PATH		    "McAfee\\Agent\\"
#define MA_REGISTRY_STORE_APPLICATON_LIST_PATH	    "McAfee\\Agent\\Applications\\"
#define MA_REGISTRY_STORE_APPLICATON_LIST_NA_PATH	"Network Associates\\ePolicy Orchestrator\\Application Plugins\\"
#define MA_REGISTRY_STORE_XML_PATH				    "Configuration"
#define MA_REGISTRY_STORE_USE_IPC					"Use Ipc"
#define MA_REGISTRY_STORE_PLIUGIN_PATH				"Plugin Path"
#else
#define CMA_REGISTRY_STORE_SCAN_PATH			"/etc/cma.d/"
#define MA_REGISTRY_STORE_SCAN_PATH				"/etc/ma.d/"
#define MA_REGISTRY_STORE_AGENT_SCAN_PATH		"EPOAGENT3000"
#define MA_REGISTRY_STORE_APPLICATON_LIST_PATH	"/etc/ma.d/"
#define MA_REGISTRY_STORE_XML_PATH				"Configuration"
#define MA_REGISTRY_STORE_PLIUGIN_PATH			"Path"
#define MA_REGISTRY_STORE_USE_IPC				"UseIpc"
#endif
#endif



#define MA_APPLICATION_INFO_DATA_DIR							"DataPath"
#define MA_APPLICATION_INFO_INSTALL_DIR							"InstallPath"
#define MA_APPLICATION_INTEGRATION_INFO_SOFTWARE_ID				"SoftwareId"

#define MA_APPLICATION_INTEGRATION_INFO_PROPERTY_PROVIDER		"PropertyProvider"
#define MA_APPLICATION_INTEGRATION_INFO_POLICY_CONSUMER			"PolicyConsumer"
#define MA_APPLICATION_INTEGRATION_INFO_SCHEDULER_CONSUMER		"SchedulerConsumer"
#define MA_APPLICATION_INTEGRATION_INFO_EVENT_GENERATOR			"EventGenerator"
#define MA_APPLICATION_INTEGRATION_INFO_DATACHANNEL_CONSUMER	"DatachannelConsumer"
#define MA_APPLICATION_INTEGRATION_INFO_UPDATER_CONSUMER		"UpdaterConsumer"
#define MA_APPLICATION_INTEGRATION_INFO_PLUGIN_PATH				"PluginPath"

#endif

