#include "ma/internal/utils/app_info/ma_registry_store.h"
#include "ma/internal/utils/datastructures/ma_slist.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal/utils/platform/ma_sys_apis.h"
#include "ma_application_internal.h"
#include "ma/internal/ma_strdef.h"

#include <stdlib.h>
#include <string.h>

#ifdef MA_WINDOWS
#include "ma/datastore/ma_ds_registry.h"
#else
#include "ma/datastore/ma_ds_xml.h"
#endif

#ifdef MA_WINDOWS

#ifdef WIN32
#define MA_AGENT_DEFAULT_REGISTRY_VIEW				1 //32 bit by default
#define MA_AGENT_ALTERNATE_REGISTRY_VIEW			2
#elif  WIN64
#define MA_AGENT_DEFAULT_REGISTRY_VIEW				2 //64 bit
#define MA_AGENT_ALTERNATE_REGISTRY_VIEW			1
#endif
#else
#ifndef MA_AGENT_DEFAULT_REGISTRY_VIEW
#define MA_AGENT_DEFAULT_REGISTRY_VIEW				0 //32 bit by default
#define MA_AGENT_ALTERNATE_REGISTRY_VIEW			0
#endif
#endif

struct ma_application_s {
	char                                    *plugin_path;
    char                                    *software_id;
    ma_application_integration_method_t     integration_method;
    ma_uint64_t							    registered_services;	

    //TODO: Add more relevant properties beyond here.
	MA_SLIST_NODE_DEFINE(ma_application_t);
};

struct ma_registry_store_s {
	/*Identifies the type of applications that is stored in the list*/
	ma_application_integration_method_t		integration_method;
	ma_uint64_t								registered_services;
	char *data_path;
	char *install_path;
	MA_SLIST_DEFINE(applications, ma_application_t);
};



ma_error_t ma_application_create(ma_application_t **application);
ma_error_t ma_application_release(ma_application_t *application);


ma_error_t ma_registry_store_create(ma_registry_store_t **self) {
	if(self) {
		*self = (ma_registry_store_t *) calloc(1, sizeof(ma_registry_store_t));
		if(!(*self))
			return MA_ERROR_OUTOFMEMORY;
		MA_SLIST_INIT(*self, applications);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_registry_store_release(ma_registry_store_t *self) {
	if(self) {
		MA_SLIST_CLEAR(self, applications, ma_application_t, ma_application_release);
		if(self->data_path)
			free(self->data_path);
		if(self->install_path)
			free(self->install_path);
		free(self);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_registry_store_clear(ma_registry_store_t *self) {
	if(self) {
		MA_SLIST_CLEAR(self, applications, ma_application_t, ma_application_release);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}



const ma_application_t * ma_registry_store_get_application(ma_registry_store_t *self, const char *software_id) {
	if(self && software_id) {
		ma_application_t *application = NULL;
		MA_SLIST_FOREACH(self, applications, application) {
			if(0 == strcmp(software_id, application->software_id)) {
					return application;
			}
		}
	}
	return NULL;
}

const ma_application_t *ma_registry_store_get_next_application(ma_registry_store_t *self, const ma_application_t *application) {
	if(self) {
		if(!application)
			return MA_SLIST_GET_HEAD(self,applications);
		else
			return MA_SLIST_GET_NEXT_NODE(application);
	}
	return NULL;
}

static ma_bool_t is_product_already_scanned(ma_registry_store_t *self, const char *software_id) {
	if(self && software_id) {
		return ma_registry_store_get_application(self, software_id)?MA_TRUE:MA_FALSE;
	}
	return MA_FALSE;
}

/*
 * Windows registry, Software ID's are present in the path
 * where as in Non-windows, under the config path, each software id is present in
 */
static ma_error_t ma_registry_store_application_scan(ma_registry_store_t *self, ma_ds_t *ds, const char *path_, const char *p_software_id, ma_int8_t bitness_affinity) {
	if(self && ds && path_ && p_software_id) {
		/*
		 * The general idea is that we will load all the information from the provided datastore(database or registry or xml).
		 * I/O Controller Service will know which locations to look on which platform, (on windows registry, and on non-windows different config.xml files).
		 * Whether it be a registry or xml or reading from the database, the caller will provide the path that needs to be scanned with the datastore object from where it needs to be scanned.
		 * So for (eg.) in windows, we might have to read from both 32-bit view of registry and 64 bit view of the registry to get the entire information of the products installed in the system.
		 * So this API can be called multiple times, if the same SoftwareID is found which is already present in the list, then it can be skipped.
		 * Once  the scanning is done, the caller can use ma_application_list_store_info_to_database API to save the information to the datastore model he wishes to store.
		 * This will put all the collected information at one place.
		 *
		 * My understanding at this point, is that, I/O service will have sufficient monitors to detect changes of whether products are installed or uninstalled
		 * .(registry or file monitors), rescan (reset and scan again) the application the list, and
		 * restore the information to the central datastore object from which all services can access the application list.
		 * the application can be reconstructed by the services (if they need) by using this same API and pointing the database pointer and path. All the information will be reconstructed.
		 * We might need to have a way of figuring out which SoftwareID's are valid and which are not.
		 * To validate a SoftwareID, they must have atleast one of the following keys in them "Plugin Path" || "Use Ipc" || "Msgbus"
		 *
		 * This above comments will be removed once design is finalized :)
		 */
		/*
		 * Whatever is the datastore object (registry, xml or database), create an iterator at the path provided and iterate the key-value pairs
		 * If its registry, a seperate path iterator would be created by caller to scan the product information.
		 */

		ma_temp_buffer_t temp_path;
		ma_temp_buffer_init(&temp_path);

#ifdef MA_WINDOWS
		form_path(&temp_path,path_, p_software_id, MA_TRUE);
#else
		form_path(&temp_path,path_, "", MA_TRUE);
#endif
		if(MA_FALSE == is_product_already_scanned(self, p_software_id)) {
			const char *path = (const char *)ma_temp_buffer_get(&temp_path);
			ma_application_t *application = NULL;
			ma_int64_t services_required = MA_SERVICE_NONE;
			
			if(MA_OK == ma_application_create(&application)) {
				ma_buffer_t *buf = NULL;
				ma_int64_t value = 0;

				application->software_id = strdup(p_software_id);
				
				if(MA_OK == ma_ds_get_int64(ds, path, MA_SERVICE_REQUIRED_FLAGS_INT, &value)) {
					application->registered_services = value;
					application->integration_method = MA_INTEGRATION_METHOD_MESSAGE_BASED ;
				}			
				else{
					value = 0;
					if(MA_OK == ma_ds_get_int64(ds, path, MA_REGISTRY_STORE_USE_IPC, &value) && value) {
						application->registered_services = (MA_SERVICE_ALL & ~MA_SERVICE_MSGBUS);
						application->integration_method = MA_INTEGRATION_METHOD_LEGACY_LPC_BASED;
					}
					else if(MA_OK == ma_ds_get_str(ds, path, MA_REGISTRY_STORE_PLIUGIN_PATH, &buf)){
						const char *str_value = 0;
						size_t size = 0;
						if(MA_OK == ma_buffer_get_string(buf, &str_value, &size)){
							if(str_value && strlen(str_value)){
								application->integration_method = MA_INTEGRATION_METHOD_LEGACY_PLUGIN_BASED;
								application->registered_services = (MA_SERVICE_ALL & ~MA_SERVICE_MSGBUS);	
							}
						}							
						ma_buffer_release(buf);
					}
				}
				MA_SLIST_PUSH_BACK(self, applications, application) ;
				return MA_OK;				
			}
		}
		else  {
			ma_temp_buffer_uninit(&temp_path);
			return MA_ERROR_ALREADY_REGISTERED;
		}
	}
	return MA_ERROR_INVALIDARG;
}


static ma_error_t ma_registry_store_agent_information_scan(ma_registry_store_t *self, ma_ds_t *ds, const char *agent_info_path) {
	if(self && ds && agent_info_path) {
		ma_buffer_t *buffer = NULL;
		ma_error_t error = MA_OK;
		if(self->install_path){
			free(self->install_path);
			self->install_path=NULL;
		}
		if(self->data_path){
			free(self->data_path);
			self->data_path = NULL;
		}
		if(MA_OK == ma_ds_get_str(ds, agent_info_path, MA_APPLICATION_INFO_DATA_DIR, &buffer)) {
			size_t size = 0;
			const char *path = NULL;
			if(MA_OK == ma_buffer_get_string(buffer, &path, &size)) {
				self->data_path = strdup(path);
				error = MA_OK;
			}
			ma_buffer_release(buffer);
		}
		if(MA_OK == ma_ds_get_str(ds, agent_info_path, MA_APPLICATION_INFO_INSTALL_DIR, &buffer)) {
			size_t size = 0;
			const char *path = NULL;
			if(MA_OK == ma_buffer_get_string(buffer, &path, &size)) {
				self->install_path = strdup(path);
				error = MA_OK;
			}
			ma_buffer_release(buffer);
		}
		if(self->install_path && self->data_path)
			return MA_OK;
		else
			return MA_ERROR_DS_KEY_NOT_FOUND;
	}
	return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_registry_store_iterate_ids(ma_registry_store_t *self, ma_ds_t *ds, const char *path, ma_int8_t bitness) {
	if(self && path) {
#ifdef MA_WINDOWS
		if(ds) {
			ma_ds_iterator_t *path_iterator = NULL;
			if(MA_OK == ma_ds_iterator_create(ds, MA_TRUE, path, &path_iterator)) {
				ma_buffer_t *buffer = NULL;
				while(MA_OK == ma_ds_iterator_get_next(path_iterator, &buffer)) {
					const char *software_id = NULL;
					size_t size = 0;
					if(MA_OK == ma_buffer_get_string(buffer, &software_id, &size)) {
						ma_registry_store_application_scan(self, ds, path, software_id, bitness);
					}
					ma_buffer_release(buffer);
				}
				ma_ds_iterator_release(path_iterator);
			}
			return MA_OK;
		}
#else
		if(!ds)
		{
			ma_filesystem_iterator_t *iterator = NULL;
			ma_error_t error = MA_OK;
			if(MA_OK == ma_filesystem_iterator_create(path, &iterator)) {
				ma_temp_buffer_t buffer;
				ma_temp_buffer_init(&buffer);
				while(MA_OK == ma_filesystem_iterator_get_next(iterator, &buffer)) {
					const char *software_id = (const char*) ma_temp_buffer_get(&buffer);
					if(0 == strcmp("CMNUPD__3000", software_id) || 0 == strcmp("CMDAGENT1000", software_id)|| 0 == strcmp("EPOAGENT3700LYNX", software_id) 
						|| 0 == strcmp("EPOAGENT3700MACX", software_id) || 0 == strcmp("EPOAGENT4600MLOS", software_id)	|| 0 == strcmp("EPOAGENT4000AIXX", software_id) 
						|| 0 == strcmp("EPOAGENT4000HPUX", software_id) || 0 == strcmp("EPOAGENT3700SLRS", software_id) || 0 == strcmp("EPOAGENT3700WSHD", software_id) )
						continue;
					ma_temp_buffer_t full_path;
					ma_ds_xml_t *ds_xml;
					ma_temp_buffer_init(&full_path);
					form_path(&full_path, path, software_id, MA_FALSE);
					form_path(&full_path, (char *) ma_temp_buffer_get(&full_path), "config.xml", MA_FALSE);
					if( MA_OK == ma_ds_xml_open((char*)ma_temp_buffer_get(&full_path), &ds_xml)) {
						ds = (ma_ds_t *) ds_xml;
						if(0 == strcmp("EPOAGENT3000", software_id)) {
							error = ma_registry_store_agent_information_scan(self, ds, MA_REGISTRY_STORE_XML_PATH);
							if(error != MA_OK) {
								break;
							}
						}
						ma_registry_store_application_scan(self, ds, MA_REGISTRY_STORE_XML_PATH, software_id, bitness);
						ma_ds_release(ds);
					}
					ma_temp_buffer_uninit(&full_path);
				}
				ma_temp_buffer_uninit(&buffer);
				ma_filesystem_iterator_release(iterator);
			}
			return error;
		}

#endif
	}
	return MA_ERROR_INVALIDARG;
}
ma_error_t ma_registry_store_scan(ma_registry_store_t *self) {
	ma_error_t error = MA_ERROR_APIFAILED;
	if(self) {
		/* Windows, iterate through the registry for point product information
		 * Go to the Agent location in registry and read the data dir information and update the relevant information
		 */
		MA_SLIST_CLEAR(self, applications, ma_application_t, ma_application_release);



		if(MA_FALSE == is_product_already_scanned(self, MA_SYSPROPS_SOFTWAREID_STR)) {
			ma_application_t *application = NULL;
			if(MA_OK == ma_application_create(&application)) {
				application->software_id = strdup(MA_SYSPROPS_SOFTWAREID_STR);
				application->registered_services =  MA_SERVICE_PROPERTY;                
			    application->integration_method = MA_INTEGRATION_METHOD_MESSAGE_BASED ;
				MA_SLIST_PUSH_BACK(self, applications, application) ;
			}
		}

#ifdef MA_WINDOWS
		{
			ma_ds_registry_t *ds_registry = NULL;
			/*First open the default registry view, the bitness can be got from  */
			{
				if(MA_OK == ma_ds_registry_open(HKEY_LOCAL_MACHINE, MA_REGISTRY_STORE_SCAN_PATH, MA_DS_REGISTRY_DEFAULT_VIEW, MA_FALSE, KEY_READ, &ds_registry)) {
					/*We already have the registry datastore object opened in the default registry view that is accessible to the application. Scan the agent details as well*/
					if(MA_OK != (error =ma_registry_store_agent_information_scan(self, (ma_ds_t *)ds_registry, MA_REGISTRY_STORE_AGENT_SCAN_PATH))) {
						ma_ds_registry_release(ds_registry);
						return error;
					}
					/**/
					error = ma_registry_store_iterate_ids(self, (ma_ds_t *) ds_registry, MA_REGISTRY_STORE_APPLICATON_LIST_PATH, MA_AGENT_DEFAULT_REGISTRY_VIEW);
					ma_ds_registry_release(ds_registry);
					ds_registry = NULL;
				}
			}
			/*Now open the alternate view of the registry and scan the applications to the store */
			{
				if(MA_OK == ma_ds_registry_open(HKEY_LOCAL_MACHINE, MA_REGISTRY_STORE_SCAN_PATH, MA_DS_REGISTRY_ALTERNATE_VIEW, MA_FALSE, KEY_READ, &ds_registry)) {
					/**/
					error = ma_registry_store_iterate_ids(self, (ma_ds_t *) ds_registry, MA_REGISTRY_STORE_APPLICATON_LIST_PATH, MA_AGENT_ALTERNATE_REGISTRY_VIEW);
					ma_ds_registry_release(ds_registry);
					ds_registry = NULL;
				}
			}

            /* NOW do the scan in Network Associates/ePO/App plugin path */
            {
				if(MA_OK == ma_ds_registry_open(HKEY_LOCAL_MACHINE, MA_REGISTRY_STORE_SCAN_PATH, MA_DS_REGISTRY_DEFAULT_VIEW, MA_FALSE, KEY_READ, &ds_registry)) {
					error = ma_registry_store_iterate_ids(self, (ma_ds_t *) ds_registry, MA_REGISTRY_STORE_APPLICATON_LIST_NA_PATH, MA_AGENT_DEFAULT_REGISTRY_VIEW);
					ma_ds_registry_release(ds_registry);
					ds_registry = NULL;
				}
			}
			{
				if(MA_OK == ma_ds_registry_open(HKEY_LOCAL_MACHINE, MA_REGISTRY_STORE_SCAN_PATH, MA_DS_REGISTRY_ALTERNATE_VIEW, MA_FALSE, KEY_READ, &ds_registry)) {
					/**/
					error = ma_registry_store_iterate_ids(self, (ma_ds_t *) ds_registry, MA_REGISTRY_STORE_APPLICATON_LIST_NA_PATH, MA_AGENT_ALTERNATE_REGISTRY_VIEW);
					ma_ds_registry_release(ds_registry);
					ds_registry = NULL;
				}
			}

		}
#else
		/*
		 * Non-windows, open the /etc/cma.d/, iterate the file system open the config.xml with xml_datastore
		 * Iterate the settings of the config.xml
		 */
		error = ma_registry_store_iterate_ids(self, NULL, MA_REGISTRY_STORE_SCAN_PATH, MA_AGENT_DEFAULT_REGISTRY_VIEW);
		error = ma_registry_store_iterate_ids(self, NULL, CMA_REGISTRY_STORE_SCAN_PATH, MA_AGENT_DEFAULT_REGISTRY_VIEW);
#endif
	}
	return error;
}


const char *ma_registry_store_get_agent_install_path(ma_registry_store_t *self) {
	if(self) {
		return self->install_path;
	}
	return NULL;
}

const char *ma_registry_store_get_agent_data_path(ma_registry_store_t *self) {
	if(self) {
		return self->data_path;
	}
	return NULL;
}

ma_error_t ma_application_create(ma_application_t **self) {
	if(self) {
		*self = (ma_application_t *) calloc(1, sizeof(ma_application_t));
		if(!(*self))
			return MA_ERROR_OUTOFMEMORY;
		(*self)->registered_services = MA_SERVICE_NONE;
		(*self)->integration_method = MA_INTEGRATION_METHOD_UNKNOWN;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_application_release(ma_application_t *self) {
	if(self) {
		if(self->software_id ) {
			free(self->software_id);
			self->software_id = NULL;
		}

		free(self);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_application_integration_method_t ma_application_get_integration_method(const ma_application_t *self) {
	if(self) {
		return self->integration_method;
	}
	return MA_INTEGRATION_METHOD_LEGACY_PLUGIN_BASED;
}

ma_uint64_t ma_application_get_required_services_flag(const ma_application_t *self) {
	if(self) {
		return self->registered_services;
	}
	return 0;
}

const char *ma_application_get_software_id(const ma_application_t *self) {
	if(self) {
		return self->software_id;
	}
	return NULL;
}


