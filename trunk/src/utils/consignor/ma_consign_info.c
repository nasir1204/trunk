#include "ma/internal/utils/consignor/ma_consign_info.h"
#include "ma/internal/utils/json/ma_cjson.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/defs/ma_consign_defs.h"
#include "ma/ma_log.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

extern ma_logger_t *consignor_logger;

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "consignor"

struct ma_consign_info_s {
    char date[MA_MAX_LEN+1];
    char contact[MA_MAX_LEN+1];
    ma_table_t *completed_orders;
    ma_table_t *pending_orders;
};


ma_error_t ma_consign_info_create(ma_consign_info_t **info) {
    if(info) {
         ma_error_t err = MA_ERROR_OUTOFMEMORY;

         if((*info = (ma_consign_info_t*)calloc(1, sizeof(ma_consign_info_t)))) {
             if(MA_OK == (err = ma_table_create(&(*info)->completed_orders))) {
                 err = ma_table_create(&(*info)->pending_orders);
             }
         }

         return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_consign_info_release(ma_consign_info_t *info) {
    if(info) {
        if(info->completed_orders)(void)ma_table_release(info->completed_orders);
        if(info->pending_orders)(void)ma_table_release(info->pending_orders);
        free(info);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


/* setter */
ma_error_t ma_consign_info_set_contact(ma_consign_info_t *info, const char *contact) {
    if(!info || !contact)return MA_ERROR_INVALIDARG;
    strncpy(info->contact, contact, MA_MAX_LEN);
    return MA_OK;
}

ma_error_t ma_consign_info_set_date(ma_consign_info_t *info, const char *date) {
    if(!info || !date)return MA_ERROR_INVALIDARG;
    strncpy(info->date, date, MA_MAX_LEN);
    return MA_OK;
}

ma_error_t ma_consign_info_set_pending_orders_map(ma_consign_info_t *info, ma_table_t *table) {
    if(info && table) {
        (void)ma_table_release(info->pending_orders);
        return  ma_table_add_ref(info->pending_orders = table);
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_consign_info_set_completed_orders_map(ma_consign_info_t *info, ma_table_t *table) {
    if(info && table) {
        (void)ma_table_release(info->completed_orders);
        return  ma_table_add_ref(info->completed_orders = table);
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_consign_info_add_pending_order(ma_consign_info_t *info, const char *contact, const char *order) {
    if(info && contact && order) {
        ma_error_t err = MA_OK;

        if(!strncmp(info->contact, contact, MA_MAX_LEN)) {
           ma_variant_t *v = NULL;

           MA_LOG(consignor_logger, MA_LOG_SEV_DEBUG, "contact (%s) and consign contact (%s) match", contact, info->contact);
           if(MA_OK == (err = ma_variant_create(&v))) {
               if(MA_OK == (err = ma_table_add_entry(info->pending_orders, order, v))) {
                   MA_LOG(consignor_logger, MA_LOG_SEV_DEBUG, "consign order(%s) added into contact(%s) pending orders map", order, info->contact);
               } else {
                   MA_LOG(consignor_logger, MA_LOG_SEV_ERROR, "consign order(%s) added into contact(%s) pending orders map failed, last error(%d)", order, info->contact, err);
               }
               (void)ma_variant_release(v);
           }
        } else {
           MA_LOG(consignor_logger, MA_LOG_SEV_ERROR, "contact (%s) and consign contact (%s) does not match", contact, info->contact);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_consign_info_remove_pending_order(ma_consign_info_t *info, const char *contact, const char *order) {
    if(info && contact && order) {
        ma_error_t err = MA_OK;

        if(!strncmp(info->contact, contact, MA_MAX_LEN)) {

           MA_LOG(consignor_logger, MA_LOG_SEV_DEBUG, "contact (%s) and consign contact (%s) match", contact, info->contact);
           if(MA_OK == (err = ma_table_remove_entry(info->pending_orders, order))) {
               MA_LOG(consignor_logger, MA_LOG_SEV_DEBUG, "consign order(%s) removed into contact(%s) pending orders map", order, info->contact);
           } else {
               MA_LOG(consignor_logger, MA_LOG_SEV_ERROR, "consign order(%s) removed into contact(%s) pending orders map failed, last error(%d)", order, info->contact, err);
           }
        } else {
           MA_LOG(consignor_logger, MA_LOG_SEV_ERROR, "contact (%s) and consign contact (%s) does not match", contact, info->contact);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_consign_info_pending_order_list_size(ma_consign_info_t *info, const char *contact, size_t *size) {
    if(info && contact && size) {
        ma_error_t err = MA_OK;

        if(MA_OK == (err = ma_table_size(info->pending_orders, size))) {
            MA_LOG(consignor_logger, MA_LOG_SEV_TRACE, "consign(%s) pending orders count(%u)", contact, *size);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_consign_info_add_completed_order(ma_consign_info_t *info, const char *contact, const char *order) {
    if(info && contact && order) {
        ma_error_t err = MA_OK;

        if(!strncmp(info->contact, contact, MA_MAX_LEN)) {
           ma_variant_t *v = NULL;

           MA_LOG(consignor_logger, MA_LOG_SEV_DEBUG, "contact (%s) and consign contact (%s) match", contact, info->contact);
           if(MA_OK == (err = ma_variant_create(&v))) {
               if(MA_OK == (err = ma_table_add_entry(info->completed_orders, order, v))) {
                   MA_LOG(consignor_logger, MA_LOG_SEV_DEBUG, "consign order(%s) added into contact(%s) completed orders map", order, info->contact);
               } else {
                   MA_LOG(consignor_logger, MA_LOG_SEV_ERROR, "consign order(%s) added into contact(%s) completed orders map failed, last error(%d)", order, info->contact, err);
               }
               (void)ma_variant_release(v);
           }
        } else {
           MA_LOG(consignor_logger, MA_LOG_SEV_ERROR, "contact (%s) and consign contact (%s) does not match", contact, info->contact);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_consign_info_remove_completed_order(ma_consign_info_t *info, const char *contact, const char *order) {
    if(info && contact && order) {
        ma_error_t err = MA_OK;

        if(!strncmp(info->contact, contact, MA_MAX_LEN)) {

           MA_LOG(consignor_logger, MA_LOG_SEV_DEBUG, "contact (%s) and consign contact (%s) match", contact, info->contact);
           if(MA_OK == (err = ma_table_remove_entry(info->completed_orders, order))) {
               MA_LOG(consignor_logger, MA_LOG_SEV_DEBUG, "consign order(%s) removed into contact(%s) completed orders map", order, info->contact);
           } else {
               MA_LOG(consignor_logger, MA_LOG_SEV_ERROR, "consign order(%s) removed into contact(%s) completed orders map failed, last error(%d)", order, info->contact, err);
           }
        } else {
           MA_LOG(consignor_logger, MA_LOG_SEV_ERROR, "contact (%s) and consign contact (%s) does not match", contact, info->contact);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_consign_info_completed_order_list_size(ma_consign_info_t *info, const char *contact, size_t *size) {
    if(info && contact && size) {
        ma_error_t err = MA_OK;

        if(MA_OK == (err = ma_table_size(info->completed_orders, size))) {
            MA_LOG(consignor_logger, MA_LOG_SEV_TRACE, "consign(%s) completed orders count(%u)", contact, *size);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

/* getter */
ma_error_t ma_consign_info_get_contact(ma_consign_info_t *info, const char **contact) {
    if(info && contact) {
        *contact = info->contact;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_consign_info_get_date(ma_consign_info_t *info, const char **date) {
    if(info && date) {
        *date = info->date;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_consign_info_get_pending_orders_map(ma_consign_info_t *info, ma_table_t **table) {
    if(info && table) {
        return ma_table_add_ref(*table = info->pending_orders);
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_consign_info_get_completed_orders_map(ma_consign_info_t *info, ma_table_t **table) {
    if(info && table) {
        return ma_table_add_ref(*table = info->completed_orders);
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_consign_info_convert_to_variant(ma_consign_info_t *info, ma_variant_t **variant) {
    if(info && variant) {
        ma_error_t err = MA_OK;
        ma_table_t *info_table = NULL;

        if(MA_OK == (err = ma_table_create(&info_table))) {
             {
                 const char *contact = NULL;

                 if(MA_OK == (err = ma_consign_info_get_contact(info, &contact))) {
                     ma_variant_t *v =NULL;

                     MA_LOG(consignor_logger, MA_LOG_SEV_DEBUG, "contact (%s)", contact);
                     if(MA_OK == (err = ma_variant_create_from_string(contact, strlen(contact), &v))) {
                         err = ma_table_add_entry(info_table, MA_CONSIGN_INFO_ATTR_CONTACT, v);
                         (void)ma_variant_release(v);
                     }
                 }
             }

             {
                 const char *date = NULL;

                 if(MA_OK == (err = ma_consign_info_get_date(info, &date))) {
                    ma_variant_t *v = NULL;

                    MA_LOG(consignor_logger, MA_LOG_SEV_DEBUG, "date (%s)", date);
                    if(MA_OK == (err = ma_variant_create_from_string(date, strlen(date), &v))) {
                        err = ma_table_add_entry(info_table, MA_CONSIGN_INFO_ATTR_DATE, v);
                        (void)ma_variant_release(v);
                    }
                 }
             }
             {
                 ma_table_t *table = NULL;

                 if(MA_OK == (err = ma_consign_info_get_pending_orders_map(info, &table))) {
                     ma_variant_t *v = NULL;

                     MA_LOG(consignor_logger, MA_LOG_SEV_DEBUG, "pending order map");
                     if(MA_OK == (err = ma_variant_create_from_table(table, &v))) {
                        err = ma_table_add_entry(info_table, MA_CONSIGN_INFO_ATTR_PENDING_ORDERS, v);
                        (void)ma_variant_release(v);
                     }
                     (void)ma_table_release(table);
                 }
             }
             {
                 ma_table_t *table = NULL;

                 if(MA_OK == (err = ma_consign_info_get_completed_orders_map(info, &table))) {
                     ma_variant_t *v = NULL;

                     MA_LOG(consignor_logger, MA_LOG_SEV_DEBUG, "completed order map");
                     if(MA_OK == (err = ma_variant_create_from_table(table, &v))) {
                        err = ma_table_add_entry(info_table, MA_CONSIGN_INFO_ATTR_COMPLETED_ORDERS, v);
                        (void)ma_variant_release(v);
                     }
                     (void)ma_table_release(table);
                 }
             }

             err = ma_variant_create_from_table(info_table, variant);
             (void)ma_table_release(info_table);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_consign_info_convert_from_variant(ma_variant_t *variant, ma_consign_info_t **info) {
    if(info && variant) {
        ma_error_t err = MA_OK;

        if(MA_OK == (err = ma_consign_info_create(info))) {
            ma_table_t *info_table = NULL;

            if(MA_OK == (err = ma_variant_get_table(variant, &info_table))) {
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_CONSIGN_INFO_ATTR_CONTACT, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    err = ma_consign_info_set_contact((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_CONSIGN_INFO_ATTR_DATE, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    err = ma_consign_info_set_date((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_CONSIGN_INFO_ATTR_PENDING_ORDERS, &v))) {
                        ma_table_t *table = NULL;

                        if(MA_OK == (err = ma_variant_get_table(v, &table))) {
                            err = ma_consign_info_set_pending_orders_map((*info), table);
                            (void)ma_table_release(table);
                        }
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_CONSIGN_INFO_ATTR_COMPLETED_ORDERS, &v))) {
                        ma_table_t *table = NULL;

                        if(MA_OK == (err = ma_variant_get_table(v, &table))) {
                            err = ma_consign_info_set_completed_orders_map((*info), table);
                            (void)ma_table_release(table);
                        }
                        (void)ma_variant_release(v);
                    }
                }

                (void)ma_table_release(info_table);
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static void for_each(const char *key, ma_variant_t *value, void *cb_data, ma_bool_t *stop_loop) {
    if(key && value && cb_data && stop_loop) {
        ma_json_t *info = (ma_json_t*)cb_data;
        ma_vartype_t type = MA_VARTYPE_NULL;
        char buf[MA_MAX_LEN+1] = {0};
        ma_error_t err = MA_OK;

        (void)ma_variant_get_type(value, &type);
        if(MA_VARTYPE_UINT32 == type) {
            ma_uint32_t t = 0;

            (void)ma_variant_get_uint32(value, &t);
            MA_MSC_SELECT(_snprintf, snprintf)(buf, MA_MAX_LEN, "%u", t);
            (void)ma_json_add_element(info, key, buf);
        } else if(MA_VARTYPE_INT32 == type) {
            ma_int32_t t = 0;

            (void)ma_variant_get_int32(value, &t);
            MA_MSC_SELECT(_snprintf, snprintf)(buf, MA_MAX_LEN, "%d", t);
            (void)ma_json_add_element(info, key, buf);
        } else if(MA_VARTYPE_BOOL == type) {
            ma_bool_t t = MA_FALSE;

            (void)ma_variant_get_bool(value, &t);
            MA_MSC_SELECT(_snprintf, snprintf)(buf, MA_MAX_LEN, "%d", t);
            (void)ma_json_add_element(info, key, buf);
        } else if(MA_VARTYPE_STRING == type) {
            ma_buffer_t *buf = NULL;

            if(MA_OK == ma_variant_get_string_buffer(value, &buf)) {
                const char *pstr = NULL;
                size_t len = 0;

                if(MA_OK == ma_buffer_get_string(buf, &pstr, &len)) {
                    if(pstr) {
                        (void)ma_json_add_element(info, key, pstr);
                    }
                }
                (void)ma_buffer_release(buf);
            }
        } else if(MA_VARTYPE_TABLE == type) {
            ma_table_t *table = NULL;

            if(MA_OK == ma_variant_get_table(value, &table)) {
                size_t size = 0;

                (void)ma_table_size(table, &size);
                if(size) {
                    ma_json_t *transitions  = NULL;

                    if(MA_OK == (err = ma_json_create(&transitions))) {
                        if(MA_OK == (err = ma_table_foreach(table, &for_each, transitions))) {
                            ma_bytebuffer_t *buffer = NULL;

                            ma_bytebuffer_create(MA_MAX_BUFFER_LEN, &buffer);
                            if(MA_OK == (err = ma_json_get_data(transitions, buffer))) {
                                (void)ma_json_add_object(info, key, (const unsigned char*)ma_bytebuffer_get_bytes(buffer), ma_bytebuffer_get_size(buffer));
                            } 
                        }
                        (void)ma_json_release(transitions);
                    }
                } else {
                    //(void)ma_json_add_element(info, key, "");
                }
                
                (void)ma_table_release(table);
            }
        } else if(MA_VARTYPE_NULL == type) {
            char buf[MA_MAX_BUFFER_LEN+1] = {0};
            MA_MSC_SELECT(_snprintf, snprintf)(buf, MA_MAX_BUFFER_LEN, "http://%s:%s/order/%s", "mileaccess.com", "8000", key);
            (void)ma_json_add_element(info, key, buf);
        }
    }
}

ma_error_t ma_consign_info_convert_to_json(ma_consign_info_t *info, ma_json_t **json) {
    if(info && json) {
        ma_error_t err = MA_OK;
        ma_variant_t *v = NULL;

        if(MA_OK == (err = ma_consign_info_convert_to_variant(info, &v))) {
            ma_table_t *table = NULL;

            if(MA_OK == (err = ma_variant_get_table(v, &table))) {
                if(MA_OK == (err = ma_json_create(json))) {
                    err = ma_table_foreach(table, &for_each, *json);
                }
                (void)ma_table_release(table);
            }
            (void)ma_variant_release(v);
        }
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

