#include "ma/internal/utils/consignor/ma_consignor_datastore.h"
#include "ma/internal/defs/ma_consign_defs.h"
#include "ma/scheduler/ma_enumerator.h"
#include "ma/internal/ma_macros.h"
#include "ma/ma_log.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "consignor"

extern ma_logger_t *consignor_logger;

#define PROFILE_DATA MA_DATASTORE_PATH_SEPARATOR "consign_data"
#define PROFILE_ID MA_DATASTORE_PATH_SEPARATOR "consign_id"

#define PROFILE_DATA_PATH PROFILE_DATA PROFILE_ID MA_DATASTORE_PATH_SEPARATOR/* \"consign_data"\"consign_id"\ */


ma_error_t ma_consignor_datastore_read(ma_consignor_t *consignor, ma_ds_t *datastore, const char *ds_path) {
    if(consignor && datastore && ds_path) {
        ma_error_t err = MA_OK;
        ma_ds_iterator_t *consign_iterator = NULL;
        ma_buffer_t *consign_id_buf = NULL;
        char consign_data_base_path[MA_MAX_PATH_LEN] = {0};

        MA_MSC_SELECT(_snprintf, snprintf)(consign_data_base_path, MA_MAX_PATH_LEN, "%s%s", ds_path, PROFILE_DATA_PATH);
        MA_LOG(consignor_logger, MA_LOG_SEV_DEBUG, "consignor data store path(%s) reading...", consign_data_base_path);
        if(MA_OK == (err = ma_ds_iterator_create(datastore, 0, consign_data_base_path, &consign_iterator))) {
            while(MA_ERROR_NO_MORE_ITEMS != ma_ds_iterator_get_next(consign_iterator, &consign_id_buf) && consign_id_buf) {
                const char *consign_id = NULL;
                size_t size = 0;
                
                if(MA_OK == (err = ma_buffer_get_string(consign_id_buf, &consign_id, &size))) {
                    char consign_data_path[MA_MAX_PATH_LEN] = {0};
                    ma_variant_t *consign_var = NULL;
                    MA_MSC_SELECT(_snprintf, snprintf)(consign_data_path, MA_MAX_PATH_LEN, "%s", consign_data_base_path);/* base_path\"consign_data"\"consign_id"\4097 */
                    if(MA_OK == (err = ma_ds_get_variant(datastore, consign_data_path, consign_id, MA_VARTYPE_TABLE, &consign_var))) {

                        (void)ma_variant_release(consign_var);
                    }
                }
                (void)ma_buffer_release(consign_id_buf);
            }
            (void)ma_ds_iterator_release(consign_iterator);
        }

        return ((MA_ERROR_DS_PATH_NOT_FOUND == err) || (MA_ERROR_SCHEDULER_TASK_ID_CONFLICT == err)) ? MA_OK : err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_consignor_datastore_write(ma_consignor_t *consignor, ma_consign_info_t *info, ma_ds_t *datastore, const char *ds_path) {
    if(consignor && info && datastore && ds_path) {
        char consign_data_path[MA_MAX_PATH_LEN] = {0};
        char *consign_id = NULL;
        ma_error_t err = MA_OK;
        ma_variant_t *var = NULL;

        (void)ma_consign_info_get_contact(info, &consign_id);
        MA_MSC_SELECT(_snprintf, snprintf)(consign_data_path, MA_MAX_PATH_LEN, "%s%s", ds_path, PROFILE_DATA_PATH);
        MA_LOG(consignor_logger, MA_LOG_SEV_INFO, "consign id(%s), data path(%s) consign_data_path(%s)", consign_id, ds_path, consign_data_path);
        if(MA_OK == (err = ma_consign_info_convert_to_variant(info, &var))) {
            if(MA_OK != (err = ma_ds_set_variant(datastore, consign_data_path, consign_id, var)))
                MA_LOG(consignor_logger, MA_LOG_SEV_DEBUG, "consignor datastore write consign id(%s) failed", consign_id);
            (void)ma_variant_release(var);
        } else
            MA_LOG(consignor_logger, MA_LOG_SEV_DEBUG, "consignor datastore write consign id(%s) conversion consign info to variant failed, last error(%d)", consign_id, err);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_consignor_datastore_write_variant(ma_consignor_t *consignor, const char *consign_id, ma_ds_t *datastore, const char *ds_path, ma_variant_t *var) {
    if(consignor && consign_id && var && datastore && ds_path) {
        char consign_data_path[MA_MAX_PATH_LEN] = {0};
        ma_error_t err = MA_OK;

        MA_MSC_SELECT(_snprintf, snprintf)(consign_data_path, MA_MAX_PATH_LEN, "%s%s", ds_path, PROFILE_DATA_PATH);
        MA_LOG(consignor_logger, MA_LOG_SEV_INFO, "consign id(%s), data path(%s) consign_data_path(%s)", consign_id, ds_path, consign_data_path);
        if(MA_OK == (err = ma_ds_set_variant(datastore, consign_data_path, consign_id, var)))
            MA_LOG(consignor_logger, MA_LOG_SEV_INFO, "consignor datastore write consign id(%s) success", consign_id);
        else
            MA_LOG(consignor_logger, MA_LOG_SEV_ERROR, "consignor datastore write consign id(%s) failed, last error(%d)", consign_id, err);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_consignor_datastore_remove_consign(const char *consign, ma_ds_t *datastore, const char *ds_path) {
    if(consign && datastore && ds_path) {
        char consign_path[MA_MAX_PATH_LEN] = {0};
        
        MA_LOG(consignor_logger, MA_LOG_SEV_DEBUG, "consignor removing (%s) from datastore", consign);
        MA_MSC_SELECT(_snprintf, snprintf)(consign_path, MA_MAX_PATH_LEN, "%s%s",ds_path, PROFILE_DATA_PATH );/* base_path\"consign_data"\4097 */
        return ma_ds_rem(datastore, consign_path, consign, 1);
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_consignor_datastore_get(ma_consignor_t *consignor, ma_ds_t *datastore, const char *ds_path, const char *consign_id, ma_consign_info_t **info) {
    if(consignor && datastore && ds_path && consign_id && info) {
        ma_error_t err = MA_OK;
        char consign_data_base_path[MA_MAX_PATH_LEN] = {0};
        ma_variant_t *consign_var = NULL;

        MA_MSC_SELECT(_snprintf, snprintf)(consign_data_base_path, MA_MAX_PATH_LEN, "%s%s", ds_path, PROFILE_DATA_PATH);
        if(MA_OK == (err = ma_ds_get_variant(datastore, consign_data_base_path, consign_id, MA_VARTYPE_TABLE, &consign_var))) {
            if(MA_OK == (err = ma_consign_info_convert_from_variant(consign_var, info)))
                MA_LOG(consignor_logger, MA_LOG_SEV_TRACE, "consign(%s) exist in consignor DB", consign_id);
            (void)ma_variant_release(consign_var);
        } else 
            MA_LOG(consignor_logger, MA_LOG_SEV_ERROR, "consign(%s) does not exist in consignor DB, last error(%d)", consign_id, err);

        return err;
    }
    return MA_ERROR_INVALIDARG;

}

ma_error_t ma_consignor_datastore_clear(ma_ds_t *datastore, const char *ds_path) {
    return(datastore && ds_path) ? ma_ds_rem(datastore, ds_path, NULL, 1) : MA_ERROR_INVALIDARG;
}

