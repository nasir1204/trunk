#include "ma/internal/utils/consignor/ma_consignor.h"
#include "ma/internal/utils/consignor/ma_consign_info.h"
#include "ma/internal/utils/consignor/ma_consignor_datastore.h"
#include "ma/ma_msgbus.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/defs/ma_consign_defs.h"
#include "ma/internal/ma_macros.h"

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "consignor"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <uv.h>

ma_logger_t *consignor_logger;

struct ma_consignor_s {
    char path[MA_MAX_PATH_LEN+1];
    ma_msgbus_t *msgbus;
    ma_ds_t* datastore;
};


ma_error_t ma_consignor_create(ma_msgbus_t *msgbus, ma_ds_t *datastore, const char *base_path, ma_consignor_t **consignor) {
    if(msgbus && datastore && base_path && consignor) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;

        MA_LOG(consignor_logger, MA_LOG_SEV_DEBUG, "consignor creation processing");
        if((*consignor = (ma_consignor_t*)calloc(1, sizeof(ma_consignor_t)))) {
            (*consignor)->msgbus = msgbus;
            strncpy((*consignor)->path, base_path, MA_MAX_PATH_LEN);
            (*consignor)->datastore = datastore;
            err = MA_OK;
            MA_LOG(consignor_logger, MA_LOG_SEV_INFO, "consignor created successfully");
        }
        if(MA_OK != err) {
            MA_LOG(consignor_logger, MA_LOG_SEV_ERROR, "consignor creation failed, last error(%d)", err);
            if(*consignor)
                (void)ma_consignor_release(*consignor);
            *consignor = NULL;
        }

        MA_LOG(consignor_logger, MA_LOG_SEV_DEBUG, "consignor creation processing rc(%d)", err);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_consignor_add(ma_consignor_t *consignor, ma_consign_info_t *info) {
    return consignor && info ? ma_consignor_datastore_write(consignor, info, consignor->datastore, consignor->path) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_consignor_add_variant(ma_consignor_t *consignor, const char *id, ma_variant_t *var) {
    return consignor && id && var ? ma_consignor_datastore_write_variant(consignor, id, consignor->datastore, consignor->path, var) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_consignor_delete(ma_consignor_t *consignor, const char *consign) {
    return consignor && consign ? ma_consignor_datastore_remove_consign(consign, consignor->datastore, consignor->path) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_consignor_delete_all(ma_consignor_t *consignor) {
    return consignor ? ma_consignor_datastore_clear(consignor->datastore, consignor->path) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_consignor_get(ma_consignor_t *consignor, const char *consign, ma_consign_info_t **info) {
    return consignor && consign && info ? ma_consignor_datastore_get(consignor, consignor->datastore, consignor->path, consign, info) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_consignor_set_logger(ma_logger_t *logger) {
    if(logger) {
        consignor_logger = logger;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_consignor_get_msgbus(ma_consignor_t *consignor, ma_msgbus_t **msgbus) {
    if(consignor && msgbus) {
        *msgbus = consignor->msgbus;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_consignor_start(ma_consignor_t *consignor) {
    if(consignor) {
        MA_LOG(consignor_logger, MA_LOG_SEV_TRACE, "consignor started");
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_consignor_stop(ma_consignor_t *consignor) {
    if(consignor) {
        MA_LOG(consignor_logger, MA_LOG_SEV_TRACE, "consignor stopped");
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_consignor_release(ma_consignor_t *consignor) {
    if(consignor) {
        free(consignor);
    }
    return MA_ERROR_INVALIDARG;
}

