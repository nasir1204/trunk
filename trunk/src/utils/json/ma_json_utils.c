#include "ma/internal/utils/json/ma_json_utils.h"
#include "ma/internal/ma_macros.h"

#include "ma/ma_variant.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MA_JSON_MIN_BUFFER_INT      256

struct ma_json_s {
    ma_table_t          *table;  
    size_t              content_length;
};

struct ma_json_array_s {
    ma_array_t          *array_obj;
    size_t              content_length;
};

struct ma_json_table_s {
    ma_table_t     *table_obj;
    size_t         content_length;
};

struct ma_json_multi_table_s {
    ma_table_t     *table_obj;
    size_t         content_length;
};

ma_error_t ma_json_create(ma_json_t **json) {
    if(json) {
        ma_json_t *self = calloc(1, sizeof(ma_json_t));
        if(self) {
            ma_error_t rc = MA_OK;
            if(MA_OK == (rc = ma_table_create(&self->table))) {
                self->content_length = 2; /* { } */
                *json = self;
                return MA_OK;
            }
            free(self);
            return rc;
        }
        return MA_ERROR_OUTOFMEMORY;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_json_add_element(ma_json_t *self, const char *key, const char *value) {
    if(self && key && value) {
        ma_error_t rc = MA_OK;
        ma_variant_t *var = NULL;

        (void)ma_replace_char(value, '\n', ' ');
        (void)ma_replace_char(value, '\r', ' ');
        if(MA_OK == (rc = ma_variant_create_from_string(value, strlen(value), &var))) {            
            self->content_length += strlen(key) + strlen(value) + 6 /* 4 "- 1 :- 1 , */ ;
            rc = ma_table_add_entry(self->table, key, var);
            (void) ma_variant_release(var);
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_json_add_object(ma_json_t *self, const char *key, const unsigned char *value, size_t size) {
    if(self && key && value && size > 0) {
        ma_error_t rc = MA_OK;
        ma_variant_t *var = NULL;

        //void)ma_replace_char(value, '\n', ' ');
        //(void)ma_replace_char(value, '\r', ' ');
        if(MA_OK == (rc = ma_variant_create_from_raw(value, size, &var))) {            
            self->content_length += strlen(key) + size + 6 /* 4 "- 1 :- 1 , */ ;
            rc = ma_table_add_entry(self->table, key, var);
            (void) ma_variant_release(var);
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}
ma_error_t ma_json_set_table(ma_json_t *self, ma_table_t *table) {
    if(self && table) {
        (void)ma_table_release(self->table);
        return ma_table_add_ref(self->table = table);
    }
    return MA_ERROR_INVALIDARG;
}

/* {"Time":"2013-11-26T18:29:00","Severity":"Info","Facility":"io.service","Message":"configured io service"} */

void json_for_each_cb(char const *key,  ma_variant_t *table_value, void *cb_args, ma_bool_t *stop_loop) {
    ma_bytebuffer_t *data = (ma_bytebuffer_t *)cb_args;  
    ma_buffer_t *buffer = NULL;
    ma_vartype_t type = MA_VARTYPE_NULL;

    (void)ma_variant_get_type(table_value, &type);
    if(MA_VARTYPE_STRING == type) {
        if(MA_OK == ma_variant_get_string_buffer(table_value, &buffer)) {
            const char *str = NULL;
            size_t size = 0;
            if(MA_OK == ma_buffer_get_string(buffer, &str, &size)) {
                (void) ma_bytebuffer_append_bytes(data, "\"", 1);
                (void) ma_bytebuffer_append_bytes(data, (unsigned char *)key, strlen(key));
                (void) ma_bytebuffer_append_bytes(data, "\"", 1);

                (void) ma_bytebuffer_append_bytes(data, ":", 1);

                (void) ma_bytebuffer_append_bytes(data, "\"", 1);
    
    
                (void) ma_bytebuffer_append_bytes(data, (unsigned char *)str, size);
                (void) ma_bytebuffer_append_bytes(data, "\"", 1);   
    
                (void) ma_bytebuffer_append_bytes(data, ",", 1);   
            }
            (void) ma_buffer_release(buffer);
        }
    } else if (type == MA_VARTYPE_RAW) {
        unsigned char *raw = NULL;
        size_t sz = 0;

        if(MA_OK == ma_variant_get_raw(table_value, &raw, &sz)) {
            (void) ma_bytebuffer_append_bytes(data, "\"", 1);
            (void) ma_bytebuffer_append_bytes(data, (unsigned char *)key, strlen(key));
            (void) ma_bytebuffer_append_bytes(data, "\"", 1);

            (void) ma_bytebuffer_append_bytes(data, ":", 1);

            (void) ma_bytebuffer_append_bytes(data, (unsigned char *)raw, sz);
    
            (void) ma_bytebuffer_append_bytes(data, ",", 1);   
        }
    }
}


void json_for_each_len_cb(char const *key,  ma_variant_t *table_value, void *cb_args, ma_bool_t *stop_loop) {
    ma_json_t *self = (ma_json_t *)cb_args;  
    ma_buffer_t *buffer = NULL;
        
    if(MA_OK == ma_variant_get_string_buffer(table_value, &buffer)) {
        const char *str = NULL;
        size_t size = 0;
        if(MA_OK == ma_buffer_get_string(buffer, &str, &size)) {
            self->content_length += strlen(key) + strlen(str) + 6;
        }
        (void) ma_buffer_release(buffer);
    }
}


ma_error_t ma_json_get_data(ma_json_t *self, ma_bytebuffer_t *data) {
    if(self && data) {
        ma_error_t rc = MA_OK;
        (void)  ma_table_foreach(self->table, json_for_each_len_cb, self);       
        if(self->content_length > ma_bytebuffer_get_capacity(data))
            rc = ma_bytebuffer_reserve(data, self->content_length);
        
        if(MA_OK == rc) {
            (void) ma_bytebuffer_append_bytes(data, "{", 1);
            (void)  ma_table_foreach(self->table, json_for_each_cb, data);       
            //(void) ma_bytebuffer_set_bytes(data, self->content_length - 2,  "}", 1); /* Overwrite the last "," with "}" */
            (void) ma_bytebuffer_set_bytes(data, ma_bytebuffer_get_size(data) - 1,  "}", 1); /* Overwrite the last "," with "}" */
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_json_get_table(ma_json_t *self, ma_table_t **table) {
    if(self && table) {
        *table = NULL;
        if(self->table) {
            return ma_table_add_ref(*table = self->table);
        }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_json_release(ma_json_t *self) {
    if(self) {
        (void) ma_table_release(self->table);
        free(self);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


/* JSON array */
ma_error_t ma_json_array_create(ma_json_array_t **json_array) {
    if(json_array) {
        ma_error_t rc = MA_OK;
        ma_json_array_t *self = calloc(1, sizeof(ma_json_array_t));   
        if(self) {
            if(MA_OK == (rc = ma_array_create(&self->array_obj))) {
                self->content_length = 2; /* [ ] */
                *json_array = self;
                return MA_OK;
            }
            free(self);
            return rc;
        }
        return MA_ERROR_OUTOFMEMORY;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_json_array_add_object(ma_json_array_t *self, ma_json_t *json) {
    if(self && json) {                        
        ma_bytebuffer_t *buffer = NULL;
        ma_error_t rc = MA_OK;        
        if(MA_OK == (rc = ma_bytebuffer_create(MA_JSON_MIN_BUFFER_INT, &buffer))) {
            if(MA_OK == (rc = ma_json_get_data(json, buffer))) {
                ma_variant_t *var = NULL;                
                if(MA_OK == (rc = ma_variant_create_from_raw(ma_bytebuffer_get_bytes(buffer), ma_bytebuffer_get_size(buffer), &var))) {
                    self->content_length += ma_bytebuffer_get_size(buffer) + 1; /* , */
                    rc = ma_array_push(self->array_obj, var);                
                    (void) ma_variant_release(var);
                }
            }
            (void) ma_bytebuffer_release(buffer);
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_json_array_set_array(ma_json_array_t *self, ma_array_t *array) {
    if(self && array) {
        (void)ma_array_release(self->array_obj);
        return ma_array_add_ref(self->array_obj = array);
    }
    return MA_ERROR_INVALIDARG;
}
void json_array_for_each_cb(size_t index,  ma_variant_t *value, void *cb_args, ma_bool_t *stop_loop) {
    ma_bytebuffer_t *data = (ma_bytebuffer_t *)cb_args; 
    ma_buffer_t *buffer = NULL;
        
    if(MA_OK == ma_variant_get_raw_buffer(value, &buffer)) {
        unsigned char *str = NULL;
        size_t size = 0;
        if(MA_OK == ma_buffer_get_raw(buffer, &str, &size))
            (void) ma_bytebuffer_append_bytes(data, str, size); 
        (void) ma_buffer_release(buffer);
        (void) ma_bytebuffer_append_bytes(data, ",", 1);  
    }
}

/* [{"Time":"2013-11-26T18:29:00","Severity":"Info","Facility":"io.service","Message":"configured io service"},
    {"Time":"2013-11-26T18:29:00","Severity":"Info","Facility":"io.service","Message":"configured io service"},
    {"Time":"2013-11-26T18:29:00","Severity":"Info","Facility":"io.service","Message":"configured io service"}
   ]
*/

ma_error_t ma_json_array_get_data(ma_json_array_t *self, ma_bytebuffer_t *data) {
    if(self && data) {
        ma_error_t rc = MA_OK;
        if(self->content_length > ma_bytebuffer_get_capacity(data))
            rc = ma_bytebuffer_reserve(data, self->content_length);

        if(MA_OK == rc) {
            (void) ma_bytebuffer_append_bytes(data, "[", 1);
            (void) ma_array_foreach(self->array_obj, json_array_for_each_cb, data);       
            (void) ma_bytebuffer_set_bytes(data, self->content_length - 2,  "]", 1); /* Overwrite the last "," with "]" */
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_json_array_get_array(ma_json_array_t *self, ma_array_t **array) {
    if(self && array) {
        if(self->array_obj) {
            return ma_array_add_ref(*array = self->array_obj);
        }
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_json_array_release(ma_json_array_t *self) {
    if(self) {
        (void) ma_array_release(self->array_obj);
        free(self);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_bool_t ma_json_array_is_empty(ma_json_array_t *self) {
    if(self) {
        size_t size = 0;
        if(MA_OK == ma_array_size(self->array_obj, &size) && size > 0)
            return MA_FALSE;
    }
    return MA_TRUE;
}

size_t ma_json_array_size(ma_json_array_t *self) {
    if(self) {
        size_t size = 0;
        (void)ma_array_size(self->array_obj, &size);
        return size;
    }
    return 0;
}

/* JSON table  */
ma_error_t ma_json_table_create(ma_json_table_t **json_table) {
    if(json_table) {
        ma_error_t rc = MA_OK;
        ma_json_table_t *self = calloc(1, sizeof(ma_json_table_t));   

        if(self) {
            if(MA_OK == (rc = ma_table_create(&self->table_obj))) {
                self->content_length = 2; /* [ ] */
                *json_table = self;
                return MA_OK;
            }
            free(self);
            return rc;
        }
        return MA_ERROR_OUTOFMEMORY;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_json_table_add_object(ma_json_table_t *self, const char *key, ma_json_array_t *json) {
    if(self && key && json) {                        
        ma_bytebuffer_t *buffer = NULL;
        ma_error_t rc = MA_OK;

        if(MA_OK == (rc = ma_bytebuffer_create(MA_JSON_MIN_BUFFER_INT, &buffer))) {
            if(MA_OK == (rc = ma_json_array_get_data(json, buffer))) {
                ma_variant_t *var = NULL;                

                if(MA_OK == (rc = ma_variant_create_from_raw(ma_bytebuffer_get_bytes(buffer), ma_bytebuffer_get_size(buffer), &var))) {
                    self->content_length += ma_bytebuffer_get_size(buffer) + 1 + strlen(key) + 5; /* , */
                    rc = ma_table_add_entry(self->table_obj, key, var);                
                    (void) ma_variant_release(var);
                }
            }
            (void) ma_bytebuffer_release(buffer);
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_json_table_add_string(ma_json_table_t *self, const char *key, const char *value) {
    if(self && key && value) {                        
        ma_bytebuffer_t *buffer = NULL;
        ma_error_t rc = MA_OK;
        ma_variant_t *var = NULL;                

        if(MA_OK == (rc = ma_variant_create_from_string(value, strlen(value), &var))) {
            self->content_length += strlen(value) + 1 + strlen(key) + 2; /* , */
            rc = ma_table_add_entry(self->table_obj, key, var);                
            (void) ma_variant_release(var);
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

void json_table_for_each_cb(const char *key, ma_variant_t *value, void *cb_args, ma_bool_t *stop_loop) {
    ma_bytebuffer_t *data = (ma_bytebuffer_t *)cb_args; 
    ma_buffer_t *buffer = NULL;
        
    if(MA_OK == ma_variant_get_raw_buffer(value, &buffer)) {
        unsigned char *str = NULL;
        size_t size = 0;

        ma_bytebuffer_append_bytes(data, "\"", 1);
        ma_bytebuffer_append_bytes(data, key, strlen(key));
        ma_bytebuffer_append_bytes(data, "\" : ", 3);
        if(MA_OK == ma_buffer_get_raw(buffer, &str, &size))
            (void) ma_bytebuffer_append_bytes(data, str, size); 
        (void) ma_buffer_release(buffer);
        (void) ma_bytebuffer_append_bytes(data, ",", 1);  
    }
}
ma_error_t ma_json_table_get_data(ma_json_table_t *self, ma_bytebuffer_t *data) {
    if(self && data) {
        ma_error_t rc = MA_OK;
        if(self->content_length > ma_bytebuffer_get_capacity(data))
            rc = ma_bytebuffer_reserve(data, self->content_length);

        if(MA_OK == rc) {
            size_t size = 0;
            //(void) ma_bytebuffer_append_bytes(data, "{", 1);
            (void) ma_table_foreach(self->table_obj, json_table_for_each_cb, data);       
            size = ma_bytebuffer_get_size(data);
            if(size) { 
                (void) ma_bytebuffer_set_bytes(data, size-1, " ", 1);
            }
            //(void) ma_bytebuffer_set_bytes(data, self->content_length - 2,  "}", 1); /* Overwrite the last "," with "}" */
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_json_table_release(ma_json_table_t *self) {
    if(self) {
        (void) ma_table_release(self->table_obj);
        free(self);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_bool_t ma_json_table_is_empty(ma_json_table_t *self) {
    if(self) {
        size_t size = 0;
        if(MA_OK == ma_table_size(self->table_obj, &size) && size > 0)
            return MA_FALSE;
    }
    return MA_TRUE;
}

size_t ma_json_table_size(ma_json_table_t *self) {
    if(self) {
        size_t size = 0;
        (void)ma_table_size(self->table_obj, &size);
        return size;
    }
    return 0;
}


ma_error_t ma_json_multi_table_create(ma_json_multi_table_t **json_table) {
    if(json_table) {
        ma_error_t rc = MA_OK;
        ma_json_multi_table_t *self = calloc(1, sizeof(ma_json_multi_table_t));   

        if(self) {
            if(MA_OK == (rc = ma_table_create(&self->table_obj))) {
                self->content_length = 2; /* [ ] */
                *json_table = self;
                return MA_OK;
            }
            free(self);
            return rc;
        }
        return MA_ERROR_OUTOFMEMORY;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_json_multi_table_add_object(ma_json_multi_table_t *self, const char *key, ma_json_table_t *json) {
    if(self && key && json) {                        
        ma_bytebuffer_t *buffer = NULL;
        ma_error_t rc = MA_OK;

        if(MA_OK == (rc = ma_bytebuffer_create(MA_JSON_MIN_BUFFER_INT, &buffer))) {
            if(MA_OK == (rc = ma_json_table_get_data(json, buffer))) {
                ma_variant_t *var = NULL;                

                if(MA_OK == (rc = ma_variant_create_from_raw(ma_bytebuffer_get_bytes(buffer), ma_bytebuffer_get_size(buffer), &var))) {
                    self->content_length += ma_bytebuffer_get_size(buffer) + 1 + strlen(key) + 7; /* , */
                    rc = ma_table_add_entry(self->table_obj, key, var);                
                    (void) ma_variant_release(var);
                }
            }
            (void) ma_bytebuffer_release(buffer);
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

void json_multi_table_for_each_cb(const char *key, ma_variant_t *value, void *cb_args, ma_bool_t *stop_loop) {
    ma_bytebuffer_t *data = (ma_bytebuffer_t *)cb_args; 
    ma_buffer_t *buffer = NULL;
    ma_error_t err = MA_OK;
        
    if(MA_OK == ma_variant_get_raw_buffer(value, &buffer)) {
        unsigned char *str = NULL;
        size_t size = 0;

        ma_bytebuffer_append_bytes(data, "\"", 1);
        ma_bytebuffer_append_bytes(data, key, strlen(key));
        ma_bytebuffer_append_bytes(data, "\" : ", 3);
        (void) ma_bytebuffer_append_bytes(data, "{", 1);
        if(MA_OK == (err = ma_buffer_get_raw(buffer, &str, &size)))
            (void) ma_bytebuffer_append_bytes(data, str, size); 
        (void) ma_buffer_release(buffer);
        (void) ma_bytebuffer_append_bytes(data, "}", 1);  
        (void) ma_bytebuffer_append_bytes(data, ",", 1);  
    }
}

ma_error_t ma_json_multi_table_get_data(ma_json_multi_table_t *self, ma_bytebuffer_t *data) {
    if(self && data) {
        ma_error_t rc = MA_OK;
        if(self->content_length > ma_bytebuffer_get_capacity(data))
            rc = ma_bytebuffer_reserve(data, self->content_length);

        if(MA_OK == rc) {
            size_t size = 0;
            (void) ma_bytebuffer_append_bytes(data, "{", 1);
            (void) ma_table_foreach(self->table_obj, json_multi_table_for_each_cb, data);       
            size = ma_bytebuffer_get_size(data);
            if(size) { 
                (void) ma_bytebuffer_set_bytes(data, size-1, "}", 1);
            } else {
                (void) ma_bytebuffer_set_bytes(data, self->content_length - 3,  "", 1); /* Overwrite the last "," with "}" */
                (void) ma_bytebuffer_set_bytes(data, self->content_length - 2,  "}", 1); /* Overwrite the last "," with "}" */
            }
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_json_multi_table_release(ma_json_multi_table_t *self) {
    if(self) {
        (void) ma_table_release(self->table_obj);
        free(self);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_bool_t ma_json_multi_table_is_empty(ma_json_multi_table_t *self) {
    if(self) {
        size_t size = 0;
        if(MA_OK == ma_table_size(self->table_obj, &size) && size > 0)
            return MA_FALSE;
    }
    return MA_TRUE;
}

size_t ma_json_multi_table_size(ma_json_multi_table_t *self) {
    if(self) {
        size_t size = 0;
        (void)ma_table_size(self->table_obj, &size);
        return size;
    }
    return 0;
}


void ma_trim_string_by_char(char *str, char c) {
   if(str) {
       int len = 0;
       if(str[0] == c) {
           strcpy(str, str+1);
           len = strlen(str);
           if(str[len-1] == c) {
               str[len-1] = '\0';
           }
       } else {
           len = strlen(str);
           if(str[len-1] == c) {
               str[len-1] = '\0';
           }
       }
   }
}

void ma_replace_char(char *str, char old, char new) {
   if(str) {
       char *p = str;

       while(*p) {
           if(*p == old) *p = new;
           p++;
       }
   }
}
