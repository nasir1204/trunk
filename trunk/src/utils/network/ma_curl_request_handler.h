#ifndef MA_CURL_REQUEST_HANDLER_H_INCLUDED
#define MA_CURL_REQUEST_HANDLER_H_INCLUDED

#include "curl/curl.h"
#include "ma_curl.h"
#include "ma/internal/utils/threading/ma_atomic.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/utils/network/ma_url_request_handler.h"
#include "ma/internal/utils/datastructures/ma_vector.h"

#include "ma_net_log.h"


MA_CPP(extern "C" {)

typedef struct ma_curl_request_handler_s ma_curl_request_handler_t;
typedef struct curl_context_s curl_context_t;

struct curl_context_s {
    /*socket which we are watching */
    curl_socket_t                       sockfd;

    /*watcher for a socket */
    uv_poll_t                           poll_handle;

    /*back pointer to handler obejct */
    ma_curl_request_handler_t           *handler;
};



struct ma_curl_request_handler_s {

    ma_url_request_handler_t            base;

    ma_bool_t                           running_state;

    /* copy of uv_loop */
    uv_loop_t                           *uv_loop;

    /*curl multi inteface handle */
    CURLM                               *curlm_handle;                   

    /* currently active easy handles */
    ma_atomic_counter_t                 running_handles;

    /*timer for curl multi timer callback */
    uv_timer_t                          multi_timer;

    /* dynamic array of poller/curl context objects */
    ma_vector_define(curl_context_vec, curl_context_t*);

};



ma_error_t ma_curl_request_handler_create(ma_event_loop_t* event_loop, ma_url_request_handler_t** curl_req_handler) ;

MA_CPP(})

#endif

