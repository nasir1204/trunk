#include <stdlib.h>
#include "ma/internal/utils/network/ma_url_request_handler.h"
#include "ma_net_log.h"
#include "ma_curl.h"

#define REQUEST_HANDLER_COUNT 4

MA_CPP(extern "C" {)

struct ma_url_request_handlers_s{
	ma_url_request_handler_t *req_rh[REQUEST_HANDLER_COUNT];
};

ma_error_t ma_url_request_handler_start(ma_url_request_handler_t *self) {
    if(self) {
        return self->vtable->start(self) ;
    }
    MA_NET_LOG_INVARG() ;
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_url_request_handler_stop(ma_url_request_handler_t *self) {
    if(self) {
        return self->vtable->stop(self) ;
    }
    MA_NET_LOG_INVARG() ;
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_url_request_handler_is_running(ma_url_request_handler_t *self, ma_bool_t *is_running) {
    if(self && is_running) {
        return self->vtable->is_running(self, is_running) ;
    }
    MA_NET_LOG_INVARG() ;
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_url_request_handler_release(ma_url_request_handler_t *self) {
    if(self) {
        return self->vtable->release(self) ;
    }
    MA_NET_LOG_INVARG() ;
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_url_request_handler_create_request(ma_url_request_handler_t *self , const char *url, ma_url_request_t **url_request ) {
    if(self && url && url_request) {
        return self->vtable->create_request(self, url, url_request) ;
    }
    MA_NET_LOG_INVARG() ;
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_url_request_handler_submit_request(ma_url_request_handler_t *self , ma_url_request_t *url_request , ma_url_request_finalize_callback_t completion_cb, void *userdata) {
    if(self && url_request) {
        return self->vtable->submit_request(self, url_request, completion_cb, userdata) ;
    }
    MA_NET_LOG_INVARG() ;
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_url_request_handler_remove_request(ma_url_request_handler_t *self , ma_url_request_t *url_request ) {
    if(self && url_request) {
        return self->vtable->remove_request(self, url_request) ;
    }
    MA_NET_LOG_INVARG() ;
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_url_request_handler_query_request_state(ma_url_request_handler_t *self , ma_url_request_t *url_request, ma_url_request_state_t *state) {
    if(self && url_request && state) {
        return self->vtable->query_request_state(self, url_request, state) ;
    }
    MA_NET_LOG_INVARG() ;
    return MA_ERROR_INVALIDARG ;
}


ma_error_t ma_url_request_handlers_create(ma_url_request_handlers_t **rh_container){
	if(rh_container){
		ma_url_request_handlers_t *tmp=NULL;
		if ( NULL == (tmp = (ma_url_request_handlers_t*)calloc( 1,sizeof(ma_url_request_handlers_t) ) ) )
			return MA_ERROR_OUTOFMEMORY;
		*rh_container=tmp;
		return MA_OK;
	}
    MA_NET_LOG_INVARG() ;
	return MA_ERROR_INVALIDARG;
}
ma_error_t ma_url_request_handlers_release(ma_url_request_handlers_t *rh_container){
	ma_error_t err=MA_OK;
	if(rh_container){
		int i=0;
		for(i=0 ; i < REQUEST_HANDLER_COUNT ; i++) {
            if ( rh_container->req_rh[i] && MA_OK != ( err=ma_url_request_handler_release(rh_container->req_rh[i]) ) )
				return err;
			rh_container->req_rh[i]=NULL;
		}
		free(rh_container);
		return MA_OK;
	}
    MA_NET_LOG_INVARG() ;
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_url_request_handlers_add_rh(ma_url_request_handlers_t *rh_container,ma_network_transfer_protocol_t protocol,ma_url_request_handler_t *handler){
	if(rh_container && handler){
        MA_ATOMIC_INCREMENT(handler->ref_count) ;
		rh_container->req_rh[protocol]=handler;
		return MA_OK;
	}
    MA_NET_LOG_INVARG() ;
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_url_request_handlers_remove_rh(ma_url_request_handlers_t *rh_container,ma_network_transfer_protocol_t protocol,ma_url_request_handler_t **handler){
	if(rh_container && handler){
		if( rh_container->req_rh[protocol] ){
			*handler=rh_container->req_rh[protocol];
			rh_container->req_rh[protocol]=NULL;
		}
		return MA_OK;
	}
    MA_NET_LOG_INVARG() ;
	return MA_ERROR_INVALIDARG;
}
ma_error_t ma_url_request_handlers_get_rh(ma_url_request_handlers_t *rh_container,ma_network_transfer_protocol_t protocol,ma_url_request_handler_t **handler){
	if(rh_container && handler){
		if( rh_container->req_rh[protocol] ){
			*handler=rh_container->req_rh[protocol];
			return MA_OK;
		}
		else return MA_ERROR_NETWORK_NO_REQUEST_HANDLER;
	}
    MA_NET_LOG_INVARG() ;
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_url_request_handlers_start_all(ma_url_request_handlers_t *rh_container) {
    ma_error_t err=MA_OK;
	if(rh_container){
		int i=0;
		for(i=0 ; i < REQUEST_HANDLER_COUNT ; i++) {
			if ( rh_container->req_rh[i] && MA_OK != ( err = ma_url_request_handler_start(rh_container->req_rh[i]) ))
				return err;
		}
		return MA_OK;
	}
    MA_NET_LOG_INVARG() ;
	return MA_ERROR_INVALIDARG;
}
ma_error_t ma_url_request_handlers_stop_all(ma_url_request_handlers_t *rh_container){
    ma_error_t err=MA_OK;
	if(rh_container){
		int i=0;
		for(i=0 ; i < REQUEST_HANDLER_COUNT ; i++) {
			if ( rh_container->req_rh[i] && MA_OK != ( err = ma_url_request_handler_stop(rh_container->req_rh[i]) ))
				return err;
		}
		return MA_OK;
	}
    MA_NET_LOG_INVARG() ;
	return MA_ERROR_INVALIDARG;
}

MA_CPP(})
