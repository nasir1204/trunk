#include "ma/internal/utils/network/ma_net_client_service.h"
#include "ma/internal/utils/network/ma_url_request_handler.h"
#include "ma/internal/utils/network/ma_url_request.h"

#include "ma_curl.h"

#include <stdlib.h>

#if defined(MA_NETWORK_USE_CURL) || defined(MA_NETWORK_USE_ALL)
#include "ma_curl_request_handler.h"
#endif
#if defined(MA_NETWORK_USE_UNC) || defined(MA_NETWORK_USE_ALL)
#include "ma_unc_request_handler.h"
#endif

#if defined(MA_NETWORK_USE_LOCAL) || defined(MA_NETWORK_USE_ALL)
#include "ma_local_request_handler.h"
#endif

#if defined(MA_NETWORK_USE_MOCK)
#include "ma_mock_request.h"
#include "ma_mock_request_handler.h"
#endif

ma_logger_t *net_logger = NULL ;

MA_CPP(extern "C" {)

struct ma_net_client_service_s {
	ma_bool_t                   running_state ;
	ma_event_loop_t             *ev_loop ;
	ma_url_request_handlers_t   *rh_container ;
};

ma_error_t ma_net_client_service_create(ma_event_loop_t *ev_loop, ma_net_client_service_t **net_service){
	ma_error_t err=MA_OK;
	ma_url_request_handler_t *rh=NULL;
	if(ev_loop && net_service){   
		ma_net_client_service_t *net_svc=NULL;
		
        if( NULL == (net_svc=(ma_net_client_service_t*)calloc(1,sizeof(ma_net_client_service_t) ) ) )
			return MA_ERROR_OUTOFMEMORY;

		net_svc->ev_loop = ev_loop ;
		net_svc->running_state = MA_FALSE ;

		if(MA_OK == ( err = ma_url_request_handlers_create(&net_svc->rh_container) ) &&
		  MA_MOCK_REQUEST_HANDLER_CREATE((net_svc), rh) &&
		  MA_CURL_REQUEST_HANDLER_CREATE((net_svc), rh) &&
          MA_UNC_REQUEST_HANDLER_CREATE((net_svc), rh) &&
		  MA_LOCAL_REQUEST_HANDLER_CREATE((net_svc), rh) )
        {
			*net_service = net_svc ;  
			return MA_OK;
		}
		else{
			ma_net_client_service_release(net_svc);
			return err;
		}
	}
    MA_LOG(net_logger, MA_LOG_SEV_ERROR, "Invalid Argument") ;
	return MA_ERROR_INVALIDARG;
}
ma_error_t ma_net_client_service_release(ma_net_client_service_t *self){
	ma_error_t err=MA_OK;
	if(self){	
		if(self->running_state)
			return MA_ERROR_NETWORK_SERVICE_RUNNING;
		if(	self->rh_container && ( MA_OK != ( err=ma_url_request_handlers_release(self->rh_container) ) ) )
			return err;
        net_logger = NULL ;
		free(self);
		return MA_OK;
	}
    MA_LOG(net_logger, MA_LOG_SEV_ERROR, "Invalid Argument") ;
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_net_client_service_start(ma_net_client_service_t *self){
	ma_error_t err=MA_OK;
	if(self){	
		if(self->running_state) return MA_OK;
        if ( MA_OK == ( err = ma_url_request_handlers_start_all(self->rh_container) ) )
    		self->running_state=MA_TRUE;
		return err;
	}
    MA_LOG(net_logger, MA_LOG_SEV_ERROR, "Invalid Argument") ;
	return MA_ERROR_INVALIDARG;

}
ma_error_t ma_net_client_service_stop(ma_net_client_service_t *self){
	ma_error_t err=MA_OK;
	if(self){	
		if(!self->running_state) return MA_OK;
		if ( MA_OK == ( err = ma_url_request_handlers_stop_all(self->rh_container) ) )
			self->running_state=MA_FALSE;
		return err;
	}
    MA_LOG(net_logger, MA_LOG_SEV_ERROR, "Invalid Argument") ;
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_net_client_service_get_rh(ma_net_client_service_t *self,ma_network_transfer_protocol_t protocol,ma_url_request_handler_t **handler){
	if(self && handler){		
		if(!self->running_state) return MA_ERROR_NETWORK_SERVICE_NOT_RUNNING;
		return ma_url_request_handlers_get_rh(self->rh_container,protocol,handler);
	}
    MA_LOG(net_logger, MA_LOG_SEV_ERROR, "Invalid Argument") ;
	return MA_ERROR_INVALIDARG; 
}

ma_error_t ma_net_client_service_setlogger(ma_net_client_service_t *self, ma_logger_t *logger) {
    if(self) {
        net_logger = logger ;
        return MA_OK ;
    }
    MA_LOG(net_logger, MA_LOG_SEV_ERROR, "Invalid Argument") ;
    return MA_ERROR_INVALIDARG ;
}

MA_CPP(})

