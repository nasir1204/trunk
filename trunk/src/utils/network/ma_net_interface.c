#include "ma/internal/utils/network/ma_net_interface.h"
#include "ma/internal/utils/platform/ma_net_interface_list_internal.h"
#include "ma/internal/ma_macros.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define INET6_ADDRSTRLEN                      65

MA_CPP(extern "C" {)

ma_error_t ma_net_interface_initialize(ma_net_interface_list_t * list, ma_bool_t b_loopback);

ma_error_t ma_net_interface_list_create(ma_net_interface_list_t ** list) {
	if(list == NULL)
		return MA_ERROR_INVALIDARG;

	*list = (ma_net_interface_list_t *) (calloc)(1, sizeof(struct ma_net_interface_list_s));
	if(*list==NULL)
		return MA_ERROR_OUTOFMEMORY;

	MA_SLIST_INIT(*list,interfaces);
	(*list)->b_loopback = MA_FALSE;
	(*list)->family = MA_IPFAMILY_UNKNOWN;

	return MA_OK;
}

ma_error_t ma_net_interface_list_release(ma_net_interface_list_t *list) {

	if(list == NULL)
		return MA_ERROR_INVALIDARG;
	ma_net_interface_list_reset(list);
	free(list);
	return MA_OK;
}

ma_error_t ma_net_interface_add_address(ma_net_interface_t *itf, ma_net_address_info_t **address) {
	ma_net_address_info_t * addr = NULL;
	ma_bool_t dup_found = MA_FALSE;
	if(itf == NULL || address == NULL || *address == NULL)
		return MA_ERROR_INVALIDARG;

	MA_SLIST_FOREACH(itf,addresses,addr) {
		if(strcmp(addr->ip_addr,(*address)->ip_addr)==0){
			//This is duplicate address. Break the for loop
			dup_found = MA_TRUE;
			break;
		}
	}

	if(!dup_found) {
		MA_SLIST_PUSH_BACK(itf,addresses,*address);
		(void)ma_net_interface_set_ipfamily(itf,(*address)->family);
		*address = NULL;
	}
	else {
		(void)ma_address_info_release(*address);
		*address = NULL;
	}

	return MA_OK;
}

ma_error_t ma_net_interface_list_set_family(ma_net_interface_list_t *list, ma_ip_family_t family) {
	if(list == NULL)
		return MA_ERROR_INVALIDARG;
	if(list->family == MA_IPFAMILY_DUAL && family != MA_IPFAMILY_UNKNOWN)
		list->family = MA_IPFAMILY_DUAL;
	else if(list->family == MA_IPFAMILY_UNKNOWN || list->family == family)
		list->family = family;
	else if( (list->family == MA_IPFAMILY_IPV4 && family == MA_IPFAMILY_IPV6  ) || (list->family == MA_IPFAMILY_IPV6 && family == MA_IPFAMILY_IPV4))
		list->family = MA_IPFAMILY_DUAL;
	else
		list->family = family;

	return MA_OK;
}
ma_error_t ma_net_interface_list_add_interface(ma_net_interface_list_t *list, ma_net_interface_t **inf) {
	ma_net_interface_t * intf = NULL;
	ma_bool_t b_dup = MA_FALSE;
	if(list == NULL || inf == NULL || *inf == NULL)
		return MA_ERROR_INVALIDARG;

	MA_SLIST_FOREACH(list,interfaces,intf) {
		if(intf->index == (*inf)->index){
			b_dup = MA_TRUE;
			break;
		}
	}

	if(b_dup && intf != NULL) {
		ma_net_address_info_t * addr = NULL;

		while((*inf)->MA_SLIST_COUNT(addresses) > 0 ) {
			MA_SLIST_POP_FRONT(*inf,addresses,addr);
			if(addr) {
				ma_net_interface_add_address(intf,&addr);
			}
		}
		(void)ma_net_interface_list_set_family(list,intf->family);
		(void)ma_net_interface_release(*inf);
		*inf = NULL;
	}
	else {
		MA_SLIST_PUSH_BACK(list,interfaces,*inf);
		(void)ma_net_interface_list_set_family(list,(*inf)->family);
		*inf = NULL;
	}

	return MA_OK;
}
ma_error_t ma_net_interface_list_scan	(ma_net_interface_list_t	*list, ma_bool_t loopback)
{
	if(list == NULL)
		return MA_ERROR_INVALIDARG;

	return ma_net_interface_initialize(list,loopback);;
}

ma_error_t ma_net_interface_list_reset	(ma_net_interface_list_t *list) {
	ma_net_interface_t * intf = NULL;
	ma_net_address_info_t * addr = NULL;
	if(list == NULL)
		return MA_ERROR_INVALIDARG;

	MA_SLIST_FOREACH(list,interfaces,intf) {
		while(MA_SLIST_GET_COUNT(intf,addresses) > 0) {
			MA_SLIST_POP_FRONT(intf,addresses,addr);
			(void)ma_address_info_release(addr);
			addr = NULL;
		}
	}

	while(MA_SLIST_GET_COUNT(list,interfaces)>0) {
		MA_SLIST_POP_FRONT(list,interfaces,intf);
		(void)ma_net_interface_release(intf);
		intf = NULL;
	}

	list->family = MA_IPFAMILY_UNKNOWN;
	list->b_loopback = MA_FALSE;

	return MA_OK;
}

ma_error_t ma_net_interface_list_rescan	(ma_net_interface_list_t *list, ma_bool_t loopback) {
	ma_error_t error = MA_OK;
	if(list == NULL)
		return MA_ERROR_INVALIDARG;
	error = ma_net_interface_list_reset(list);
	if(error != MA_OK)
		return error;

	return ma_net_interface_list_scan(list,loopback);
}

ma_error_t ma_get_system_network_family(ma_net_interface_list_t *list, ma_ip_family_t *family) {
	if(list == NULL || family == NULL)
		return MA_ERROR_INVALIDARG;
	*family = list->family;
	return MA_OK;
}


ma_error_t ma_net_interface_create(ma_net_interface_t **itf) {
	if(itf == NULL)
		return MA_ERROR_INVALIDARG;
	*itf = (ma_net_interface_t *) calloc(1,sizeof(ma_net_interface_t));
	if(*itf == NULL)
		return MA_ERROR_OUTOFMEMORY;

	MA_SLIST_NODE_INIT(*itf);
	MA_SLIST_INIT(*itf,addresses);

	(*itf)->index = 0;
	(*itf)->family = MA_IPFAMILY_UNKNOWN;
	return MA_OK;
}

ma_error_t ma_net_interface_release(ma_net_interface_t *itf) {
	if(itf == NULL)
		return MA_ERROR_INVALIDARG;
	//Deinit this node. It would already be removed
	MA_SLIST_NODE_DEINIT(itf);
	//deinit the list by popping all the items and freeing up the addresses
	while(MA_SLIST_GET_COUNT(itf,addresses) > 0) {
		ma_net_address_info_t * address = NULL;

		MA_SLIST_POP_FRONT(itf,addresses,address);

		if(address)
			(void)ma_address_info_release(address);
		address = NULL;
	}
	//Free up the interface name if it was assigned.
	//Free the object itself.
	free(itf);
	return MA_OK;
}

ma_error_t ma_net_interface_set_ipfamily(ma_net_interface_t *itf, ma_ip_family_t family) {
	if(itf == NULL)
		return MA_ERROR_INVALIDARG;
	if(itf->family == MA_IPFAMILY_DUAL && family != MA_IPFAMILY_UNKNOWN)
		itf->family = MA_IPFAMILY_DUAL;
	else if(itf->family == MA_IPFAMILY_UNKNOWN || itf->family == family)
		itf->family = family;
	else if( (itf->family == MA_IPFAMILY_IPV4 && family == MA_IPFAMILY_IPV6  ) || (itf->family == MA_IPFAMILY_IPV6 && family == MA_IPFAMILY_IPV4))
		itf->family = MA_IPFAMILY_DUAL;
	else
		itf->family = family;

	return MA_OK;
}

ma_error_t ma_address_info_create(ma_net_address_info_t **address) {
	if(address == NULL)
		return MA_ERROR_INVALIDARG;
	*address = (ma_net_address_info_t *) calloc(1,sizeof(ma_net_address_info_t));
	if(*address == NULL)
		return MA_ERROR_OUTOFMEMORY;
	MA_SLIST_NODE_INIT(*address);

	(*address)->ipv6_scope_id = 0;
	(*address)->family = MA_IPFAMILY_UNKNOWN;

	return MA_OK;
}

ma_error_t ma_address_info_release(ma_net_address_info_t *address) {
	if(address == NULL)
		return MA_ERROR_INVALIDARG;
	
	MA_SLIST_NODE_DEINIT(address);

	free(address);

	return MA_OK;
}

ma_error_t ma_get_mac_from_ip(unsigned char *mac, ma_uint32_t *msize,const unsigned char *ip,ma_uint32_t isize, ma_net_interface_list_t * list) {
	if(mac == NULL || ip == NULL || list == NULL)
		return MA_ERROR_INVALIDARG;
	//TODO
	return MA_OK;
}

ma_error_t ma_get_net_interface_from_ip(ma_net_interface_t **itf, const unsigned char *ip, ma_uint32_t size, ma_net_interface_list_t * list) {
	if(itf == NULL || ip == NULL || list == NULL)
		return MA_ERROR_INVALIDARG;

	//TODO
	return MA_OK;
}

int char_occurences_count(const char *str , const char delim )
{
	int counter = 0 ;
	size_t i;
	size_t len = strlen(str);
	for(i = 0 ; i < len  ; i++ )
		counter = (str[i] == delim) ? counter +1 : counter ;
	return counter ;
}

ma_error_t expand_ipv6_address(char ret_addr[MA_IPV6_ADDR_LEN], const char ip[MA_IPV6_ADDR_LEN]) {
	char addr[MA_IPV6_ADDR_LEN] = {0};
	char *addr_temp=NULL;
	int pos;
	char local[MA_IPV6_ADDR_LEN] = {0};
	char *temp = NULL;
	int padding_count = 0;
	if(ret_addr == NULL || ip == NULL)
		return MA_ERROR_INVALIDARG;
	strncpy(addr, (char*)ip, MA_IPV6_ADDR_LEN-1);
	//This is to remove the scope id from the address e.g. fe80::f07b:ea37:53cf:c6b4%23
	if(strstr(addr, "fe80") || strstr(addr, "fec0")) //Link local or site local always starts with fe80 or feC0
	{
		temp = strchr(addr, '%');
		if(temp)
			memset(temp, '\0', strlen(temp));
	}

	if(addr_temp = strstr(addr, "::"))
	{
		/* This is special case where ip come as only "::" */
		if(!strcmp(addr, "::")){
			strncpy(ret_addr, "0:0:0:0:0:0:0:0", MA_IPV6_ADDR_LEN);
			return MA_OK;
		}
		for(pos=0; pos<(addr_temp - addr) && (pos < MA_IPV6_ADDR_LEN); pos++)
		{
			local[pos] = addr[pos];
		}

		addr_temp++; //ignoring one : from ::
		padding_count = 7 - (char_occurences_count(local, ':') + char_occurences_count(addr_temp, ':'));
		for(pos=0; pos < padding_count; pos++)
			strcat(local, ":0");
	
		if( (strlen(local) + strlen(addr_temp)) < MA_IPV6_ADDR_LEN)
			strcat(local, addr_temp);

		//This is the corner case of having address like "::2001:f:0:1" , then from above logic it will 
		//attach ":" at begining and we have to attach "0:0"
		if(local == strstr(local, "::"))
			memmove(local, "0", 1);

		//This is again corner case where address can be something like 2001:: , then from above logic
		//it will be ":" at last and we need to attach 0
		if(':' == local[strlen(local) - 1 ])
			strncpy(&local[strlen(local)], "0", 1);
		
		strncpy(ret_addr, local, MA_IPV6_ADDR_LEN);
		return MA_OK;
	}
	else
	{
		strncpy(ret_addr, addr, MA_IPV6_ADDR_LEN);
		return MA_OK;
	}
}

ma_error_t convert_v6_address_to_literal(unsigned char *ret_addr, ma_uint32_t *rsize, const unsigned char *ip, ma_uint32_t isize) {
	if(ret_addr == NULL || ip == NULL )
		return MA_ERROR_INVALIDARG;
	//TODO
	return MA_OK;
}

ma_error_t ma_format_mac_address(unsigned char mac_address[MA_MAC_ADDRESS_SIZE], unsigned char formatted_mac_address[MA_FORMATTED_MAC_ADDRESS_SIZE]) {
	if(formatted_mac_address && mac_address) {
	/*Probably need a better way to form it. But for now this temporary workaround*/
		MA_MSC_SELECT(_snprintf, snprintf)((char*)(formatted_mac_address), MA_FORMATTED_MAC_ADDRESS_SIZE, "%02X%02X%02X%02X%02X%02X",
				mac_address[0] & 0xff, mac_address[1] & 0xff,mac_address[2] & 0xff, mac_address[3] & 0xff,mac_address[4] & 0xff, mac_address[5] & 0xff);
		return MA_OK;
	}	
	return MA_ERROR_INVALIDARG;
}


/* Added below utility function to be used in http server */
/* TBD - if any duplications with existing utils, will be merged */
/* TBD - Will implement Ipv6 */


/* This function formats the v6 address in such a way that it can be passed to getaddrinfo or network system calls
	Check whether address is ipv6 or not on the basis of "["
    Check whether they are Link local fe80 or site local fec0
    all ipv6 addreess will be enclosed in []
    Now what to do
    Remove [] from address
    From Link local/SiteLocal remove % scope mentioned 

    ATTACH the scope id too - If with_scope is MA_TRUE.
*/
//static const char *format_v6_addr(char *str, ma_bool_t with_scope) {	
//    char *p = str, *start = NULL, *end = NULL;
//	if(start = strchr(p, '[')) {     //IPV6 address , enclosed in []			
//		if(end = strrchr(start, ']')) {  
//            *end = 0;  //Remove [ from start ] from end
//            if(strstr(start, "fe80") || strstr(start, "fec0")) {  //Link local or site local always starts with fe80 or feC0
//                char *t = NULL;
//                if(t = strstr(start, "%")) { //scope mentioned , remove it
//                    *t = 0;    //This will delete the complete URL after pos
//                }
//            }
//
//            if(with_scope) {
//                ma_uint32_t i_scope = 0 ;
//		        if(get_scope_id(start , &i_scope)) {
//			        //std::stringstream strm ;
//			        //strm << strNewURL  << "%" << iScope ;
//			        //strNewURL = strm.str();
//		        }
//            }
//
//            return start;
//		}
//	}
//	return str;
//}

ma_bool_t ma_compare_with_peer_address(const char *addr, const char *peer_addr) {
    ma_bool_t b_rc = MA_FALSE ;	
	if(addr && peer_addr) {
        /* 1st Case:  name == peer_name, return true immediately */
	    if(!MA_WIN_SELECT(stricmp,strcasecmp)(addr, peer_addr)) 
            return MA_TRUE;

	    /* 2nd Case: which as of now only possible in *nix, on listening on only V6 socket
	       getnameinfo IPV4 address will be IPV6 mapped address , e.g 172.16.220.80 will be 2001::172.16.220.80
	    */
		{
			const char *p_searched_addr = strstr(peer_addr, addr);
			if(p_searched_addr) { 
				if(!strcmp(p_searched_addr, addr))
					return MA_TRUE;
			}
		}
	    /* <<TBD>> 3rd case: when link local address is there but scopes are different */
		{
			char str_name[NI_MAXHOST] = {0};
			char str_peer[NI_MAXHOST] = {0};
			ma_format_v6_addr((char *)addr, str_name, MA_FALSE);
			ma_format_v6_addr((char *)peer_addr, str_peer, MA_FALSE);
			if(!strcmp(str_name,str_peer)) 
				return MA_TRUE;
		}
	    /* <<TBD>> 4th case: now if the client has so many address and same with server like IPV4 , global ,link etc..
	       request will come from the same server but it can come in different IP address , where all are valid.
	       we can not just to do the exact match keeping server in trouble with trying different address
        */
    }
	return b_rc;
}

ma_error_t ma_net_interface_list_get_ipv6_scope_ids(ma_net_interface_list_t *list, ma_uint32_t **scope_ids, size_t *size) {
	if(list && scope_ids && size) {
		ma_net_interface_t *interf = NULL;
		size_t count = 0;
		MA_SLIST_FOREACH(list, interfaces, interf) {
			ma_net_address_info_t *address = NULL;
			if(interf->family == MA_IPFAMILY_IPV4)
				continue;

			MA_SLIST_FOREACH(interf, addresses, address) {
				if(address->family == MA_IPFAMILY_IPV6) {
					count++;
					break;
				}
			}
		}
		if(count > 0) {
			ma_uint32_t *scope_id_list = NULL;
			scope_id_list = (ma_uint32_t *) calloc(count, sizeof(ma_uint32_t));
			if(scope_id_list) {
				ma_int32_t iter = 0;
				MA_SLIST_FOREACH(list, interfaces, interf) {
					ma_net_address_info_t *address = NULL;
					if(interf->family == MA_IPFAMILY_IPV4)
						continue;

					MA_SLIST_FOREACH(interf, addresses, address) {
						if(address->family == MA_IPFAMILY_IPV6) {
							scope_id_list[iter++] = address->ipv6_scope_id;
							break;
						}
					}
				}
				*scope_ids = scope_id_list;
				*size = count;
				return MA_OK;
			}
			else 
				return MA_ERROR_OUTOFMEMORY;
		}
		return MA_ERROR_APIFAILED;
	}
	return MA_ERROR_INVALIDARG;
}

void ma_format_v6_addr(char *str_url, char *str_out_url, ma_bool_t b_for_curl) {
	if(str_url && str_out_url) {
		char str_new_url[NI_MAXHOST] = {0};
		char *p_b = NULL, *p_e = NULL;
		ma_bool_t b_removed = MA_FALSE;
	
		/*Strip out the '[' ']' */
		if(p_b = strchr(str_url, '[')) {
			p_e = strchr(str_url, ']');
			strncpy(str_new_url, p_b + 1, ( p_e - (p_b + 1) ) );
			b_removed = MA_TRUE;
		}
		else {
			strcpy(str_new_url, str_url);
		}

		if(0 == strncmp(str_new_url, "fe80", 4 ) || 0 == strncmp( str_new_url, "fec0", 4 )) {
			/*Remove the scope attached*/
			char *pos = NULL;

			if(pos = strchr(str_new_url, '%')) {
				memset(pos, 0, NI_MAXHOST - (pos - str_new_url));
			}
		}
		
		if(b_for_curl && b_removed) {
			MA_MSC_SELECT(_snprintf, snprintf)(str_out_url, NI_MAXHOST, "[%s]", str_new_url);
		}
		else {
			MA_MSC_SELECT(_snprintf, snprintf)(str_out_url, NI_MAXHOST, "%s", str_new_url);
		}
	}
}


int ma_create_addrinfo_struct(ma_net_interface_list_t *list, char *addr, char *port, struct addrinfo **res, ma_bool_t b_ipv4) {
	if(list && addr && *addr && res) {
		struct addrinfo hints;
		char str_url[NI_MAXHOST] = {0};
		memset(&hints, 0, sizeof(struct addrinfo));
		hints.ai_family = b_ipv4? AF_INET:AF_UNSPEC;
		hints.ai_socktype = SOCK_STREAM;
		ma_format_v6_addr_with_scope(list, addr, str_url,  MA_FALSE);
		return getaddrinfo(str_url, (!port || !*port) ? NULL:port, &hints, res);
	}
	return -1;
}

ma_bool_t ma_format_v6_addr_with_scope(ma_net_interface_list_t *list, char *str_url, char *str_out_url, ma_bool_t b_for_curl) {
	if(list && str_url && str_out_url) {
		char str_new_url[NI_MAXHOST] = {0};
		char str_o_url[NI_MAXHOST] = {0};
		char *p_b = NULL, *p_e = NULL;
		ma_bool_t b_removed = MA_FALSE;
		ma_bool_t b_linklocal = MA_FALSE;
	
		/*Strip out the '[' ']' */
		if(p_b = strchr(str_url, '[')) {
			p_e = strchr(str_url, ']');
			strncpy(str_new_url, p_b + 1, ( p_e - (p_b + 1) ) );
			b_removed = MA_TRUE;
		}
		else {
			strcpy(str_new_url, str_url);
		}

		if(0 == strncmp(str_new_url, "fe80", 4 ) || 0 == strncmp( str_new_url, "fec0", 4 )) {
			/*Remove the scope attached*/
			char *pos = NULL;
			b_linklocal = MA_TRUE;

			if(pos = strchr(str_new_url, '%')) {
				memset(pos, 0, NI_MAXHOST - (pos - str_new_url));
			}

			/*Find the right scope and attach it*/
			{
				ma_uint32_t *scope_ids = NULL;
				size_t size = 0;
				if(MA_OK == ma_net_interface_list_get_ipv6_scope_ids( list, &scope_ids, &size)) {
					size_t i = 0;
					for( i = 0 ; i < size; i++ ){
						char str_s_url[NI_MAXHOST] = {0};
						struct addrinfo hints , *res;
						MA_MSC_SELECT(_snprintf, snprintf)(str_s_url, NI_MAXHOST, "%s%%%d", str_new_url, scope_ids[i]);
						memset(&hints, 0, sizeof(struct addrinfo));
						hints.ai_family   = AF_UNSPEC;
						hints.ai_socktype = SOCK_STREAM;
						//If get addrinfo passed with IPADDREESS%INTERFACEID then it means we are going to use this index
						if( 0 == getaddrinfo(str_s_url, NULL , &hints, &res) ) {
							strcpy(str_o_url, str_s_url);
							freeaddrinfo(res);
							break;
						}
					}
				}
			}

			if(b_for_curl && b_removed) {
				MA_MSC_SELECT(_snprintf, snprintf)(str_out_url, NI_MAXHOST, "[%s]", str_o_url);
			}
			else{ 
				MA_MSC_SELECT(_snprintf, snprintf)(str_out_url, NI_MAXHOST, "%s", str_o_url);
			}
		}
		return b_linklocal;
	}
	return MA_FALSE;
}

MA_CPP(})
