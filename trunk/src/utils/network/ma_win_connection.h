#ifndef MA_WIN_CONNECTION_H_INCLUDED
#define MA_WIN_CONNECTION_H_INCLUDED

#include "ma/ma_common.h"

#include <Windows.h>

MA_CPP(extern "C" {)

ma_error_t UserImpersonate(HANDLE *phToken , char *username , char *domainname , char *password);

ma_error_t UserImpersonateLoggedOnUser(HANDLE *);

ma_error_t CloseImpersonatedToken(HANDLE ) ;

#endif