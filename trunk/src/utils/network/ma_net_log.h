#ifndef MA_NET_LOG_H_INCLUDED
#define MA_NET_LOG_H_INCLUDED

#include "ma/ma_log.h"

extern ma_logger_t *net_logger ;

#define MA_NET_LOG_INVARG() MA_LOG(net_logger, MA_LOG_SEV_ERROR, "Invalid Argument")
#define MA_NET_LOG_MEMERR() MA_LOG(net_logger, MA_LOG_SEV_CRITICAL, "Out of Memory");

#endif //MA_NET_LOG_H_INCLUDED

