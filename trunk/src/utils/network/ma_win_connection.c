#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ma_win_connection.h"
#include "ma/ma_log.h"
#include "ma_net_log.h"

#include "ma/internal/utils/text/ma_utf8.h"

#define LOG_FACILITY_NAME "network"

#define TSTR_SIZEOF(s)	(sizeof(s) / sizeof(*(s)) )
#define BUF_SIZE          4096

typedef BOOL ( WINAPI * PFNENUMPROCESSES)( DWORD * lpidProcess, DWORD   cb, DWORD * cbNeeded );
typedef BOOL ( WINAPI * PFNENUMPROCESSMODULES)( HANDLE hProcess, HMODULE *lphModule, DWORD cb, LPDWORD lpcbNeeded );
typedef DWORD ( WINAPI * PFNGETMODULEBASENAMEW)( HANDLE hProcess, HMODULE hModule, LPWSTR lpBaseName, DWORD nSize );

PFNENUMPROCESSMODULES   pfnEnumProcessModules;
PFNGETMODULEBASENAMEW   pfnGetModuleFileNameEx;
PFNENUMPROCESSES        pfnEnumProcesses;

BOOL IsWinNT(void) {
	
	return GetVersion() < 0x80000000 ? TRUE : FALSE;
}

BOOL Init() {

	HINSTANCE   hPsApi = NULL ;
	if ( hPsApi == NULL )
	{
		TCHAR	*lastBackSlash = NULL;
		HMODULE hCommonLibProcess = NULL;


		hPsApi = LoadLibrary ( TEXT("PsApi.dll") );

		if(hPsApi) {

			pfnEnumProcesses = (PFNENUMPROCESSES)GetProcAddress(
													hPsApi, "EnumProcesses"
													);

			pfnEnumProcessModules = (PFNENUMPROCESSMODULES)GetProcAddress(
													hPsApi, "EnumProcessModules"
													);

			pfnGetModuleFileNameEx = (PFNGETMODULEBASENAMEW)GetProcAddress(
													hPsApi, "GetModuleFileNameExW"
													);

			if(!pfnEnumProcesses || !pfnEnumProcessModules || !pfnGetModuleFileNameEx) {

				FreeLibrary ( hPsApi );
				hPsApi = NULL;
				pfnEnumProcesses = NULL;
				pfnEnumProcessModules = NULL;
				pfnGetModuleFileNameEx = NULL;
			}
		}
	}

    return (hPsApi ? TRUE : FALSE);
}

BOOL GetProcessName(wchar_t *strName, size_t size,DWORD processID)
{
    BOOL bSuccess = FALSE;

    HANDLE hProcess = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, processID);

    if (hProcess)
    {
        HMODULE  hMod;
        DWORD	 cbNeeded;

        // Call EnumProcessModules() to get only the first module in the process.
        // This is will be the .EXE module for which we will retrieve the full
        // path name next.
        //
        if ((*pfnEnumProcessModules)(hProcess, &hMod, sizeof(hMod), &cbNeeded))
        {
            TCHAR szProcessName[MAX_PATH] = {0};

            if ((*pfnGetModuleFileNameEx)(hProcess, hMod, szProcessName, sizeof(szProcessName)))
            {
                bSuccess = TRUE;
				wcscpy_s(strName,size,szProcessName);
            }
        }

        CloseHandle(hProcess);
    }

    return(bSuccess);
}

static BOOL IsProcessRunning(LPCTSTR name, LPDWORD lpdwPID )
{
    BOOL        bRunning			= FALSE;
    DWORD       aProcesses[1024];
    DWORD       cbNeeded;
	wchar_t		strNameToKill[MAX_PATH] = {0};
	wchar_t strName[_MAX_FNAME] = {0};
	int i =0;
	
	
    while(*name != L'\0'){
		strNameToKill[i++] = towupper(*name);
		name++;
	}
	// Get the list of process identifiers.
    if((*pfnEnumProcesses)(aProcesses, sizeof(aProcesses), &cbNeeded))
    {
        wchar_t sDrive[_MAX_DRIVE], sDir[_MAX_DIR], sFilename[_MAX_FNAME], sExt[_MAX_EXT];

        // Calculate how many process identifiers were returned.
        UINT uProcesses = cbNeeded / sizeof(DWORD);
        UINT uIndex;
		

        for(uIndex = 0; uIndex < uProcesses && bRunning == FALSE; uIndex++)
        {
            if(GetProcessName(strName,_MAX_FNAME, aProcesses[uIndex] ) == TRUE)
            {
				wchar_t     pszName[_MAX_FNAME] = {0};
				//splitpath_s will put "." in the extension if any , so we don't need to add it
                if (_wsplitpath_s(strName, sDrive, _MAX_DRIVE, sDir,_MAX_DIR, sFilename,_MAX_FNAME, sExt,_MAX_EXT) == 0)
                {
					wcscpy_s(strName,_MAX_FNAME,sFilename);
               
                    if (wcslen(sExt) > 0)  {                 
						wcscat(strName,sExt);
					}               

					i = 0;
					while(strName[i] != L'\0'){
						pszName[i] = towupper(strName[i]);
						i++;
					}

                    if (!wcscmp(pszName,strNameToKill))
                    {
                        if ( lpdwPID != NULL )
                            *lpdwPID = aProcesses[uIndex];

                        bRunning = TRUE;
					    break;
                    }
                }
            }
        }
    }

    return(bRunning);
}

static void getWindowsSystemFolder(WCHAR *sDir,size_t dir_size)
{
    int size = GetSystemDirectory(NULL, 0);

    TCHAR *pFolder;
    pFolder = (TCHAR *)calloc(size,sizeof(TCHAR));
    if (pFolder)
    {
        GetSystemDirectory(pFolder, size);
		wcscpy_s(sDir,dir_size,pFolder);
        free(pFolder);
    }
}

static char *get_line(char *buffer, size_t buflen, DWORD *read, char *szLine ,size_t linelen)
{
	unsigned int i = 0;
	char *end = szLine + linelen - 1; 
	char *dst = szLine;
	char *pszBuffer = buffer;
	char *pszNext = NULL;
	unsigned int c;
	memset(szLine,0,linelen);

	while ((c = pszBuffer[i])  && c != '\n' && dst < end && i <buflen)
	{
		*dst++ = c;
		i++;
	}
	*dst = '\0';
	*read = i;

	if(i+1 < buflen && c== '\n')
	{
		pszNext = pszBuffer+i+1;
		(*read)++;
	}



	return pszNext;
}

//-----------------------------------------------------------------------------
// CheckIfProcessRunning
//
// This is a brut-force method for determining if a process is running and get-
// ting its process ID.  On Win64 systems, a 32-bit process is only returned
// the process modules for 32-bit processes when calling EnumProcessModules.
// Therefore, it cannot open the PID, get the process modules, and then get the
// name.  This function creates a pipe to <system32folder>\tasklist.exe with a
// command line to return the process list in CSV format.  Each line returned
// in the pipe stream is parsed and compared to pszName (in arg 1).  The first
// match causes the corressponding PID to be copied into plPID (out arg 2).
// If not found, the return is still TRUE, but no PID is returned.  If an error
// is encountered, it returns FALSE and plPID is not changed.
//
// Returns:  TRUE = success, plPID may contain PID (if match found)
//           FALSE = failure, plPID unchanged
//-----------------------------------------------------------------------------
#define SAFECLOSEHANDLE(h) if (h != INVALID_HANDLE_VALUE) { CloseHandle(h); h = INVALID_HANDLE_VALUE; }

static BOOL bCheckIfProcessRunningEx(LPCTSTR pszName, long *plPID ,BOOL bFilter)
{
	BOOL bResult = FALSE;    // default to Fail
	SECURITY_ATTRIBUTES saAttr;
	PROCESS_INFORMATION piProcInfo;
	STARTUPINFO siStartInfo;

	const int zBuf = 4096;
	wchar_t szCommandLine[MAX_PATH*2];
	wchar_t szTemp[MAX_PATH];
	DWORD dwRead =0;
	DWORD dwRemain =0;
	char chBuf[4096], chLine[4096];
	char *pOffset = NULL;
	char *pszProg, *pszPID;
	wchar_t swProg[512];
	char *pszReader = NULL;
	DWORD dwToRead =0;
	DWORD dwLineRead = 0;
	DWORD dwTotalRead =0;

	HANDLE hChildStdinRd  = INVALID_HANDLE_VALUE;
	HANDLE hChildStdinWr  = INVALID_HANDLE_VALUE;
	HANDLE hChildStdoutRd = INVALID_HANDLE_VALUE;
	HANDLE hChildStdoutWr = INVALID_HANDLE_VALUE;
	//HANDLE hStdout;
	//STRING sName(pszName);

	// Set the bInheritHandle flag so pipe handles are inherited.
	//
	saAttr.nLength = sizeof(SECURITY_ATTRIBUTES);
	saAttr.bInheritHandle = TRUE;
	saAttr.lpSecurityDescriptor = NULL;

	// Get the handle to the current STDOUT.
	//
	//    hStdout = GetStdHandle(STD_OUTPUT_HANDLE);

	// Create a pipe for the child process's STDOUT.
	//
	if (! CreatePipe(&hChildStdoutRd, &hChildStdoutWr, &saAttr, 0))
		return bResult;

	// Ensure the read handle to the pipe for STDOUT is not inherited.
	//
	SetHandleInformation( hChildStdoutRd, HANDLE_FLAG_INHERIT, 0);

	// Create a pipe for the child process's STDIN.
	//
	if (! CreatePipe(&hChildStdinRd, &hChildStdinWr, &saAttr, 0))
	{
		CloseHandle(hChildStdoutRd);
		CloseHandle(hChildStdoutWr);
		return bResult;
	}

	// Ensure the write handle to the pipe for STDIN is not inherited.
	//
	SetHandleInformation(hChildStdinWr, HANDLE_FLAG_INHERIT, 0);

	// Now create the child process.
	//


	// Set up members of the PROCESS_INFORMATION structure.
	//
	memset(&piProcInfo, 0, sizeof(PROCESS_INFORMATION));

	// Set up members of the STARTUPINFO structure.
	//
	memset(&siStartInfo, 0, sizeof(STARTUPINFO));
	siStartInfo.cb = sizeof(STARTUPINFO);
	siStartInfo.hStdError = hChildStdoutWr;
	siStartInfo.hStdOutput = hChildStdoutWr;
	siStartInfo.hStdInput = hChildStdinRd;
	siStartInfo.dwFlags |= STARTF_USESTDHANDLES | STARTF_USESHOWWINDOW;
	siStartInfo.wShowWindow = SW_HIDE;

	// Create the child process.
	getWindowsSystemFolder(szTemp,MAX_PATH);
	wcscat_s(szTemp,MAX_PATH,L"\\tasklist.exe");

	if(bFilter){
		swprintf(szCommandLine,MAX_PATH*2,L" /FI \"IMAGENAME eq %s \" /FO CSV /NH",pszName);
	}
	else
		wcscpy_s(szCommandLine,MAX_PATH*2,L" /FO CSV /NH");

	if (CreateProcess(szTemp,
		szCommandLine, // command line
		NULL,            // process security attributes
		NULL,            // primary thread security attributes
		TRUE,            // handles are inherited
		0,               // creation flags
		NULL,            // use parent's environment
		NULL,            // use parent's current directory
		&siStartInfo,    // STARTUPINFO pointer
		&piProcInfo))    // receives PROCESS_INFORMATION
	{
		CloseHandle(piProcInfo.hProcess);
		CloseHandle(piProcInfo.hThread);

		dwRemain = zBuf;

		pOffset = chBuf;

		// Close the write end of the pipe before reading from the
		// read end of the pipe.
		//
		SAFECLOSEHANDLE(hChildStdoutWr);
		pszReader =  pOffset;
		// Read all of the output from the child process' stdout (or at least as
		// much as will fit into the chBuf array.
		//
		// The input should look something like this if the process is found:
		//
		//   "explorer.exe","2668","Console","0","30,392 K"
		//   "explorer.exe","3716","Console","0","30,072 K"
		//
		// If not found, it should look something like this:
		//
		//   INFO: No tasks running with the specified criteria
		//

		while (dwRemain > 0)
		{
			// This read will most likely contain multiple, newline-delimited
			// lines of input.  We will need to iterate through this block of
			// data next.
			//
			if (!ReadFile(hChildStdoutRd, pOffset, dwRemain, &dwRead, NULL) || dwRead == 0)
				break;

			pOffset += dwRead;
			dwRemain -= dwRead;
			dwTotalRead += dwRead;
		}
		//
		dwToRead  =dwRead;



		// Parse the input, line by line
		do
		{
			pszReader = get_line(pszReader,dwTotalRead,&dwLineRead,chLine,sizeof(chLine));
			dwTotalRead -=dwLineRead;

			if (chLine[0] != '\"')
				continue;

			// Fetch the filename
			pszProg = strtok(chLine, "\",");
			if (pszProg)
			{
				if (mbstowcs(swProg, pszProg, sizeof(swProg)/sizeof(swProg[0])-1) > 0)
					if (_wcsicmp(swProg,pszName) == 0)
					{
						// Fetch the pid
						pszPID = strtok(NULL, "\",");
						if (!pszPID)
							break;    // (Unexpected) Error

						if( plPID )
						{
							*plPID = atol(pszPID);
						}
						bResult = TRUE;
						break;
					}
			}
		}while (pszReader != NULL);
	}

	SAFECLOSEHANDLE(hChildStdoutRd);
	SAFECLOSEHANDLE(hChildStdoutWr);
	SAFECLOSEHANDLE(hChildStdinRd);
	SAFECLOSEHANDLE(hChildStdinWr);

	return bResult;
}

static BOOL Is64BitProcessRunning(LPCTSTR pszName, LPDWORD lpdwPID ) {

    BOOL bSuccess = TRUE; 

    if (bSuccess == TRUE)
    {
		long lPID = -1;
        bSuccess = FALSE;
       
        // If there isn't a Wow64 directory, we are not on a 64 bit
        // machine.  Hence, we can't be running a 64 bit process.   
        
		bSuccess = bCheckIfProcessRunningEx(pszName, &lPID, TRUE);
		if(!bSuccess)
			bSuccess = bCheckIfProcessRunningEx(pszName, &lPID, FALSE);

        if (bSuccess && lPID > 0)
        {
			if (NULL != lpdwPID)
			{
                *lpdwPID = lPID;
			}

			bSuccess = TRUE;
        }             
    }

    return ( bSuccess );
}

static int bMyHasSysWow64Directory()
{
    static int bInitialized = 0;
    static int bResult = 0;
    UINT size = 0;
    HMODULE hmK32 = (HMODULE) 0;
    FARPROC pProc;
	OSVERSIONINFO os;

    if (bInitialized)
        return bResult;

    // GetSystemWow64Directory() requires at least Win XP or Win 2003 Server
    // and 64-bit is potentially only on at least XP.
    
    memset(&os, 0, sizeof(os));
    os.dwOSVersionInfoSize = sizeof(os);
            
    if (GetVersionEx(&os))
    {
        if (os.dwMajorVersion >= 4 || (os.dwMajorVersion >= 3 && os.dwMinorVersion >= 1))
        {
            // Since delay loading Kernel32.dll is not supported, we need to
            // check for the presence of GetSystemWow64Directory manually.
            //
            hmK32 = LoadLibrary(TEXT("Kernel32.dll"));
            if (hmK32)
            {
                pProc = GetProcAddress(hmK32, "GetSystemWow64DirectoryW");
                if (pProc)
                {
                    // GetSystemWow64Directory always fails on 32 bit operating systems.
                    // It returns 0 on failure or the length, so we must give it a chance
                    // to copy at least 1 character into the buffer so that we can dif-
                    // ferentiate between an error code and a size of 0.
                    //
                    TCHAR sTmp[1];

                    size = ((UINT (__stdcall *)(LPTSTR, UINT)) pProc)(sTmp, 1);
                    if (size > 0)
                        bResult = 1;
                }

                FreeLibrary(hmK32);
            }
        }
    }

    bInitialized = 1;
    return bResult;
}

static void cmnlib_GetNTShellExeName(
    LPTSTR		lpszShellExe,
    DWORD		dwSize
    )
{
       HKEY    hKey = NULL;
        DWORD   dwType = 0;
        LONG    lResult = 0;
        TCHAR   szFile [ MAX_PATH ] = { 0 };
       DWORD   dwcbData = sizeof( szFile );
    
    // First we check for the existence of the 'Winlogon' registry key.
    
    lResult = RegOpenKeyEx (
                            HKEY_LOCAL_MACHINE,
                            TEXT("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon"),
                            0,
                            KEY_READ,
                            &hKey
                            );

    if ( lResult == ERROR_SUCCESS )
    {

        lResult = RegQueryValueEx (
                            hKey,
                            TEXT("Shell"),
                            NULL,
                            &dwType,
                            (LPBYTE)szFile,
                            &dwcbData
                            );
        if(lResult == ERROR_SUCCESS && dwType == REG_SZ)
        {
            wcsncpy(lpszShellExe, szFile, dwSize);
        }
        else
        {
            wcsncpy(lpszShellExe, TEXT("Explorer.exe"), dwSize);
        }
				
		RegCloseKey(hKey);        
    }
    else
    {
        wcsncpy(lpszShellExe, TEXT("Explorer.exe"), dwSize);
    }
}

static HRESULT cmnlib_StealExplorersToken( HANDLE * lpToken )
{
	HRESULT						result			= E_FAIL;
	HANDLE						hProcessToken	= INVALID_HANDLE_VALUE;
	HANDLE						hProcessHandle;
	TCHAR						szShellExeName[MAX_PATH];
	DWORD						dwPID	=	0;
	BOOL    fGotPID = TRUE ;

	szShellExeName[0] = TEXT('\0');
	cmnlib_GetNTShellExeName(szShellExeName, TSTR_SIZEOF(szShellExeName));
    Init();
    //// BZ 429292 : On 64 bit systems IsProcessRunning() just doesn't work
    if( bMyHasSysWow64Directory() ){
		fGotPID = Is64BitProcessRunning(szShellExeName, &dwPID);
	}
    else
        fGotPID = IsProcessRunning(szShellExeName, &dwPID);
	if ( fGotPID )
	{
		hProcessHandle = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ , FALSE, dwPID );
		if ( hProcessHandle != NULL )
		{
			if (OpenProcessToken( hProcessHandle, TOKEN_QUERY | TOKEN_DUPLICATE | TOKEN_ASSIGN_PRIMARY, &hProcessToken ) && hProcessToken )
			{
				*lpToken = hProcessToken ;
				result = S_OK;
			}

			CloseHandle(hProcessHandle);
		}
	}

	return result;
}


ma_error_t UserImpersonate(HANDLE *phToken , char *username , char *domainname , char *password) 
{
	ma_error_t result = MA_OK;
	int retval = 1 ;

	HANDLE	hToken = NULL;		
	wchar_t w_username[MAX_PATH] = {0};
	wchar_t w_domainname[MAX_PATH] = {0};
	wchar_t w_password[MAX_PATH] = {0};
	
	//	User impersonation using specified account info..
	if (!username || !password || !domainname || *username == '\0'  || *password == '\0' || !phToken)
		return MA_ERROR_INVALIDARG;

	ma_utf8_to_wide_char(w_username,MAX_PATH,username,strlen(username));
	ma_utf8_to_wide_char(w_domainname,MAX_PATH,domainname,strlen(domainname));
	ma_utf8_to_wide_char(w_password,MAX_PATH,password,strlen(password));

	if (!IsWinNT()) // Win9X
		return MA_ERROR_UNSUPPORTED_PLATFORM;
	
	//	User impersonation using specified account info..

	MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "Impersonating user: %s\\%s", domainname, username);
	
	
	retval  = RevertToSelf();
	if ( !retval)
	{
		MA_LOG(net_logger, MA_LOG_SEV_ERROR,"RevertToSelf() failed <%d>.", GetLastError());  
		
		retval = MA_ERROR_NETWORK_IMPERSONATION_FAILED;
	}	
	if ( retval )
	{					
		retval = LogonUserW( w_username, w_domainname, 
							w_password,
                            // BZ 657476 : Don't use interactive login; it
                            //  causes the Windows logon cache to fill up
							LOGON32_LOGON_NEW_CREDENTIALS,
							LOGON32_PROVIDER_DEFAULT,
							&hToken );
						
		
		if ( !retval )
		{
			result = MA_ERROR_NETWORK_AUTHENTICATION_REQUIRED;
			MA_LOG(net_logger, MA_LOG_SEV_ERROR,"LogonUser() failed <%d>.", GetLastError());  
		}
		else
		{
			retval = ImpersonateLoggedOnUser ( hToken );
			if ( !retval )
			{
				result = MA_ERROR_NETWORK_IMPERSONATION_FAILED ;	
				MA_LOG(net_logger, MA_LOG_SEV_ERROR,"Failed to impersonate a user token <%d>.", GetLastError());
			
			}
			else{
				MA_LOG(net_logger, MA_LOG_SEV_DEBUG,"Impersonated user token successfully") ;
			}
		}	
	}

	if ( hToken != NULL )
		*phToken = hToken;
	
	return result;

}

ma_error_t	CloseImpersonatedToken(HANDLE hToken)
{
	if (IsWinNT())
	{		
		if ( hToken != NULL )
		{
			MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "Impersonated token closed" );	
			RevertToSelf();
			CloseHandle ( hToken ) ;
			hToken = NULL;
		}
	}
	else 
		return MA_ERROR_UNSUPPORTED_PLATFORM;
	return MA_OK; 

}

ma_error_t UserImpersonateLoggedOnUser ( HANDLE *hToken)
{
	ma_error_t result = MA_OK;
	HRESULT hResult;
	BOOL bOK;
	if (!IsWinNT()) // Win9X
		return MA_ERROR_UNSUPPORTED_PLATFORM;
	MA_LOG(net_logger, MA_LOG_SEV_INFO,"Trying logged on user account privilege for impersonation");
	
	hResult = cmnlib_StealExplorersToken ( hToken ) ;
	bOK = hResult == S_OK;
	if ( !bOK )
	{
		MA_LOG(net_logger, MA_LOG_SEV_ERROR,"Failed to steal Explorer token");
		result = MA_ERROR_NETWORK_IMPERSONATION_FAILED;
	}

	if ( bOK )
	{
		bOK = ImpersonateLoggedOnUser ( *hToken );
		if ( !bOK )
		{
			result  = MA_ERROR_NETWORK_IMPERSONATION_FAILED;
			MA_LOG(net_logger, MA_LOG_SEV_ERROR," Logged on user impersonation failed <%d>.", GetLastError());
		}
		else{
			MA_LOG(net_logger, MA_LOG_SEV_INFO, "Impersonated logged on user token successfully") ;
		}
	}		
	
	return result;

}


