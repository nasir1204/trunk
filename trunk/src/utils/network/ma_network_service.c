#include "ma/internal/services/ma_service.h"
#include "ma/internal/utils/network/ma_net_client_service.h"
#include "ma/internal/core/msgbus/ma_msgbus_private.h"

#include "ma/internal/ma_strdef.h"
#include "ma/ma_msgbus.h"

#include <stdlib.h>
#include <string.h>

typedef struct ma_network_service_s {
    ma_service_t                        base;

    ma_net_client_service_t             *net_service;

} ma_network_service_t ;

static ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint);

static ma_error_t service_start(ma_service_t *service);

static ma_error_t service_stop(ma_service_t *service);

static void service_destroy(ma_service_t *service);

static const ma_service_methods_t methods = {
    &service_configure,
    &service_start,
    &service_stop,
    &service_destroy
};


ma_error_t ma_network_service_create(const char *service_name, ma_service_t **service) {
    if(service) {
        ma_network_service_t *self = (ma_network_service_t *)calloc(1, sizeof(ma_network_service_t));

        if(!self)   return MA_ERROR_OUTOFMEMORY;

        ma_service_init((ma_service_t *)self, &methods, service_name, self);

        *service = (ma_service_t *)self;

        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


static ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint) {
    if(service && context) {
        ma_error_t rc = MA_OK;
        ma_network_service_t *self = (ma_network_service_t *)service;

        if(MA_SERVICE_CONFIG_NEW == hint) {
            ma_msgbus_t *msgbus = MA_CONTEXT_GET_MSGBUS(context);
            if(MA_OK == (rc = ma_net_client_service_create(ma_msgbus_get_event_loop(msgbus), &self->net_service))) {
                (void) ma_net_client_service_setlogger(self->net_service, MA_CONTEXT_GET_LOGGER(context));
                ma_context_add_object_info(context, MA_OBJECT_NET_CLIENT_NAME_STR, &self->net_service);
            }
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t service_start(ma_service_t *service) {
    ma_network_service_t *self = (ma_network_service_t *)service;
    return (self) ? ma_net_client_service_start(self->net_service) : MA_ERROR_INVALIDARG;
}

static ma_error_t service_stop(ma_service_t *service) {
    ma_network_service_t *self = (ma_network_service_t *)service;
    return (self) ? ma_net_client_service_stop(self->net_service) : MA_ERROR_INVALIDARG;
}

static void service_destroy(ma_service_t *service) {
    if(service) {
        ma_network_service_t *self = (ma_network_service_t *)service;
        ma_net_client_service_release(self->net_service);
        free(service);
    }
}
