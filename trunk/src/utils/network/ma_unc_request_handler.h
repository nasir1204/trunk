#ifndef MA_UNC_REQUEST_HANDLER_H_INCLUDED
#define MA_UNC_REQUEST_HANDLER_H_INCLUDED

#include "ma/internal/utils/threading/ma_atomic.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/utils/network/ma_url_request_handler.h"
#include "ma_net_log.h"

MA_CPP(extern "C" {)

typedef struct ma_unc_request_handler_s {
	ma_url_request_handler_t                base ;
	ma_event_loop_t                         *ma_loop ;
	ma_bool_t								state;
}ma_unc_request_handler_t ;

ma_error_t ma_unc_request_handler_create(ma_event_loop_t* event_loop, ma_url_request_handler_t** unc_req_handler) ;

MA_CPP(})

#endif /* MA_UNC_REQUEST_HANDLER_H_INCLUDED */