#if defined (MA_NETWORK_USE_CURL) || defined(MA_NETWORK_USE_ALL)
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include "ma_curl_request_handler.h"
#include "ma_curl_request.h"
#include "ma_url_utils.h"
#include "ma/internal/ma_macros.h"

#ifndef MA_WINDOWS
#include <unistd.h>
#endif

#define MA_SOCKET_CONNECTION_INITAL_VALUE_INT   10

static ma_error_t ma_curl_request_handler_start(ma_url_request_handler_t *self) ;
static ma_error_t ma_curl_request_handler_stop(ma_url_request_handler_t *self) ;
static ma_error_t ma_curl_request_handler_is_running(ma_url_request_handler_t *self,ma_bool_t*) ;
static ma_error_t ma_curl_request_handler_release(ma_url_request_handler_t *self) ;
static ma_error_t ma_curl_request_handler_create_request(ma_url_request_handler_t *self, const char *url, ma_url_request_t **url_request) ;
static ma_error_t ma_curl_request_handler_submit_request(ma_url_request_handler_t *self, ma_url_request_t *url_request, ma_url_request_finalize_callback_t completion_cb, void *userdata) ;
static ma_error_t ma_curl_request_handler_cancel_request(ma_url_request_handler_t *self , ma_url_request_t *url_request) ;
static ma_error_t ma_curl_request_handler_query_request_state(ma_url_request_handler_t *self , ma_url_request_t *url_request, ma_url_request_state_t *state) ;
static curl_socket_t open_socket_cb(void *clientp, curlsocktype purpose, struct curl_sockaddr *addr);
static int close_socket_cb(void *clientp, curl_socket_t socket);

/* curl callbacks */
static int curlm_socket_cb(CURL *easy, curl_socket_t curl_socket, int action, void *userp, void *socketp) ;
static void curl_perform_cb(uv_poll_t *req, int status, int events) ;
static int curlm_timer_cb(CURLM *multi,  long timeout_ms, void *userp) ;
static size_t  curl_write_cb(void *ptr, size_t size, size_t nmemb, void *userdata) ;
static size_t  curl_read_cb(void *ptr, size_t size, size_t nmemb, void *userdata) ;
static size_t  curl_header_cb(void *ptr, size_t size, size_t nmemb, void *userdata) ;
static int  curl_progress_cb(void *clientp, double dltotal, double dlnow, double ultotal, double ulnow) ;
static void curl_debugtrace_cb(CURL *handle, curl_infotype type, char *data, size_t size, void *userdata) ;

/* to check the completed curl operations */
static void curl_check_for_completed_requests(ma_curl_request_handler_t * curl_request_handler) ;
static void curl_clear_pending_requests(ma_curl_request_handler_t * curl_request_handler) ;

/* curl easy handle functions */
static ma_error_t curl_handle_create(ma_curl_request_handler_t *curl_request_handler, ma_curl_url_request_t *curl_url_request) ;

/* poller functions */
static curl_context_t *add_curl_context(ma_curl_request_handler_t *self, curl_socket_t socket);
static curl_context_t *find_curl_context(ma_curl_request_handler_t *self, curl_socket_t socket);
static void  remove_curl_context(ma_curl_request_handler_t *self, curl_socket_t socket);


static const struct ma_url_request_handler_methods_s curl_request_handler_methods = {
    &ma_curl_request_handler_start ,
    &ma_curl_request_handler_stop ,
    &ma_curl_request_handler_is_running ,
    &ma_curl_request_handler_release ,
    &ma_curl_request_handler_create_request ,
    &ma_curl_request_handler_submit_request ,
    &ma_curl_request_handler_cancel_request ,
    &ma_curl_request_handler_query_request_state
} ;

ma_error_t ma_curl_request_handler_create(ma_event_loop_t* event_loop, ma_url_request_handler_t** curl_request_handler) {
    if(event_loop && curl_request_handler) {
        ma_curl_request_handler_t *self = (ma_curl_request_handler_t *)calloc(1, sizeof(ma_curl_request_handler_t)) ;
        if(!self) {
            MA_NET_LOG_MEMERR() ;
            return MA_ERROR_OUTOFMEMORY ;
        }

        self->base.vtable = &curl_request_handler_methods ;    
		curl_global_init(CURL_GLOBAL_ALL) ;
        if(NULL == (self->curlm_handle = curl_multi_init())) {
            free(self) ;
            MA_LOG(net_logger, MA_LOG_SEV_ERROR, "CURL handler init failed") ;
            return MA_CURL_FAILURE ;
        }

        ma_vector_init(self, MA_SOCKET_CONNECTION_INITAL_VALUE_INT, curl_context_vec, curl_context_t *);    

        self->uv_loop = ma_event_loop_get_uv_loop(event_loop);
        self->multi_timer.data = self;
        MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "uv timer data(%p)\n", self);
        uv_timer_init(self->uv_loop, &self->multi_timer);
        self->running_handles = 0 ;
        self->running_state = MA_FALSE ;
        *curl_request_handler = (ma_url_request_handler_t *)self ;

        return MA_OK ;
    }
    MA_NET_LOG_INVARG() ;
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_curl_request_handler_start(ma_url_request_handler_t *url_request_handler) {
    ma_curl_request_handler_t *self = (ma_curl_request_handler_t *)url_request_handler ;
    if(MA_TRUE == self->running_state)
        return MA_OK ;

    /*Disabling pipelinig - need to explore on the usage*/
	curl_multi_setopt(self->curlm_handle, CURLMOPT_PIPELINING, 0L) ;
    //curl_multi_setopt(self->curlm_handle, CURLMOPT_MAX_HOST_CONNECTIONS, 2L) ;
    //curl_multi_setopt(self->curlm_handle, CURLMOPT_MAX_PIPELINE_LENGTH, 1L) ;   /* default is 5 */
    //curl_multi_setopt(self->curlm_handle, CURLMOPT_CONTENT_LENGTH_PENALTY_SIZE, 1048576L) ; /* 1 mb */

    curl_multi_setopt(self->curlm_handle, CURLMOPT_SOCKETFUNCTION, curlm_socket_cb) ;
    curl_multi_setopt(self->curlm_handle, CURLMOPT_SOCKETDATA, (void *)self) ;
    curl_multi_setopt(self->curlm_handle, CURLMOPT_TIMERFUNCTION, curlm_timer_cb) ;
    curl_multi_setopt(self->curlm_handle, CURLMOPT_TIMERDATA, (void *)self) ;

    self->running_state = MA_TRUE ;

    MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "CURL request handler started") ;

    return MA_OK ;
}

ma_error_t ma_curl_request_handler_stop(ma_url_request_handler_t *url_request_handler) {
    ma_curl_request_handler_t *self = (ma_curl_request_handler_t *)url_request_handler ;
        
    if(MA_FALSE == self->running_state)
        return MA_OK ;

    uv_timer_stop(&self->multi_timer);
    uv_close((uv_handle_t *)&self->multi_timer, NULL);

    self->running_state = MA_FALSE ;
    curl_check_for_completed_requests(self) ;
        
    if(self->running_handles) {
        MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "CURL request handler stop, cleaning active handles") ;
        curl_clear_pending_requests(self) ;
    }

    curl_multi_cleanup(self->curlm_handle) ;
    curl_global_cleanup();

    MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "CURL request handler stopped") ;
    return MA_OK ;
}

ma_error_t ma_curl_request_handler_is_running(ma_url_request_handler_t *url_request_handler , ma_bool_t *running) {
    ma_curl_request_handler_t *self = (ma_curl_request_handler_t *)url_request_handler ;
    *running = self->running_state ;
    return MA_OK ;
}

ma_error_t ma_curl_request_handler_release(ma_url_request_handler_t *url_request_handler) {
    ma_curl_request_handler_t *self = (ma_curl_request_handler_t *)url_request_handler ;

    MA_ATOMIC_DECREMENT(url_request_handler->ref_count) ;
    if(0 == url_request_handler->ref_count) {
        free(self) ;
        self = NULL ;
    }
    MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "CURL request handler released") ;
    return MA_OK ;
}

ma_error_t ma_curl_request_handler_create_request(ma_url_request_handler_t *url_request_handler, const char *url, ma_url_request_t **url_request) {
    ma_error_t rc = MA_OK ;
    if(MA_OK != (rc = ma_curl_url_request_create(url, (ma_curl_url_request_t **)url_request)) ) {
        MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "URL request create failed");
        return rc ;
    }
    (*url_request)->url_handler = url_request_handler ;
    return MA_OK ;
}


static void on_timeout(uv_timer_t* handle, int status) {
    ma_curl_request_handler_t *self = (ma_curl_request_handler_t *)handle->data;
    int running_handles = 0;;
    curl_multi_socket_action(self->curlm_handle, CURL_SOCKET_TIMEOUT, 0, &running_handles);
    curl_check_for_completed_requests(self);
}

static int curlm_timer_cb(CURLM *multi,  long timeout_ms, void *userp) {
    ma_curl_request_handler_t *self = (ma_curl_request_handler_t *)userp ;
    uv_timer_stop(&self->multi_timer);
    uv_timer_start(&self->multi_timer, on_timeout, timeout_ms < 0 ? 0 : timeout_ms , 0);
    return 0;
}


ma_error_t ma_curl_request_handler_submit_request(ma_url_request_handler_t *url_request_handler, ma_url_request_t *url_request , ma_url_request_finalize_callback_t completion_cb, void *userdata) {
    ma_curl_url_request_t *request = (ma_curl_url_request_t *)url_request ;
    ma_curl_request_handler_t *self = (ma_curl_request_handler_t *)url_request_handler ;
    ma_error_t ma_net_ret = MA_OK ;

    ma_url_request_add_ref(url_request);
    request->base.final_cb = completion_cb ;
    request->base.userdata = userdata ;
    request->base.request_state = MA_URL_REQUEST_STATE_INITIALIZING ;
    request->connect_notification_done = MA_FALSE;

    if(MA_OK != (ma_net_ret = curl_handle_create( self, request))) {
        MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "URL(%s) request, curl handle create failed", request->base.url) ;
        ma_url_request_release(url_request);
        return ma_net_ret ;
    }
	
	MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "URL(%s) request submitting", request->base.url) ;
	if(request->base.final_cb){ /*Async*/
		MA_ATOMIC_INCREMENT(self->running_handles) ;
		curl_easy_setopt(request->curl_easy->curl_handle, CURLOPT_OPENSOCKETFUNCTION, open_socket_cb);
        curl_easy_setopt(request->curl_easy->curl_handle, CURLOPT_OPENSOCKETDATA, self);
        curl_easy_setopt(request->curl_easy->curl_handle, CURLOPT_CLOSESOCKETFUNCTION, close_socket_cb);
        curl_easy_setopt(request->curl_easy->curl_handle, CURLOPT_CLOSESOCKETDATA, self);
        
		curl_multi_add_handle(self->curlm_handle, request->curl_easy->curl_handle) ;
        /*kick start */
        curlm_timer_cb(self->curlm_handle, 0, self);
		MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "URL(%s) request submitted, [curl handle = %p]", request->base.url, request->curl_easy->curl_handle) ;
		ma_net_ret = MA_OK;
	}else { /* sync */
		while(1){
			long response_code = 0;
			long http_connect_code = 0;

			CURLcode ret = CURLE_OK;
			ma_curl_set_ipresolve(request->curl_easy, MA_CURL_IP_RESOLVE_ANY);
			ret = curl_easy_perform(request->curl_easy->curl_handle);			
			curl_easy_getinfo(request->curl_easy->curl_handle, CURLINFO_RESPONSE_CODE, &response_code);
			curl_easy_getinfo(request->curl_easy->curl_handle, CURLINFO_HTTP_CONNECTCODE, &http_connect_code);

			if(is_curl_perform_success(request, ret, response_code)){
			  MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "URL(%s) request, completed with Response %ld", request->base.url , response_code) ;
			  ma_net_ret = MA_ERROR_NETWORK_REQUEST_SUCCEEDED ;	

			  /*set the proxy index.*/
			  if(request->proxy_list){
				  MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "Setting current used proxy index %ld in the proxy list.", request->proxy_list_index) ;
				  (void)ma_proxy_list_set_last_proxy_index(request->proxy_list, (ma_int32_t)request->proxy_list_index);
			  }
			}
			else{				
				ma_net_ret = MA_ERROR_NETWORK_REQUEST_FAILED ;
				MA_LOG(net_logger, MA_LOG_SEV_NOTICE, "URL(%s) request, failed with curl error %d, Response %ld, Connect code %ld,", request->base.url , ret, response_code, http_connect_code) ;

				if(should_go_to_next_proxy(request, ret, response_code, http_connect_code)){				
					/* check for next proxy to be tried. */
					if(request->proxy_list){
						
						if(MA_OK == set_proxy(request, MA_TRUE))
							continue;						
					}
				}				
			}            
            request->base.request_state = MA_URL_REQUEST_STATE_COMPLETED ;
            (void)ma_url_request_release(url_request);
			break;
		}
	}	
    return ma_net_ret ;
}

ma_error_t ma_curl_request_handler_query_request_state(ma_url_request_handler_t *url_request_handler , ma_url_request_t *url_request, ma_url_request_state_t *state) {
    ma_curl_request_handler_t *self = (ma_curl_request_handler_t *)url_request_handler ;
    ma_curl_url_request_t *request = (ma_curl_url_request_t *)url_request ;

    if(MA_FALSE == self->running_state)
        return MA_ERROR_NETWORK_SERVICE_NOT_STARTED ;

    *state = request->base.request_state ;     //any logic to calculate the state ???
    MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "URL(%s) request state %d ", request->base.url, *state ) ;
    return MA_OK ;
}

ma_error_t ma_curl_request_handler_cancel_request(ma_url_request_handler_t *url_request_handler , ma_url_request_t *url_request) {
    ma_curl_request_handler_t *self = (ma_curl_request_handler_t *)url_request_handler ;
    ma_curl_url_request_t *request = (ma_curl_url_request_t *)url_request ;
    /* remove the curl handle */
	request->base.request_state = MA_URL_REQUEST_STATE_CANCELLED;
	curl_multi_remove_handle(self->curlm_handle, request->curl_easy->curl_handle) ;		
	MA_ATOMIC_DECREMENT(self->running_handles) ;

	MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "URL(%s) request cancelled", request->base.url ) ;
	request->base.final_cb(MA_ERROR_NETWORK_REQUEST_ABORTED, -1, request->base.userdata, (ma_url_request_t *)request) ;
    (void)ma_url_request_release((ma_url_request_t *)request);
    return MA_OK ;
}
 
/*CURLMOPT_SOCKETFUNCTION */ 
static int curlm_socket_cb(CURL *curl_easy_handle, curl_socket_t socket, int action, void *userp, void *socketp) {
    ma_curl_url_request_t *request = NULL;
    ma_curl_request_handler_t *self = NULL ;
    static const char *whatstr[]={ "none", "IN", "OUT", "INOUT", "REMOVE" };
    curl_context_t *ctx = NULL;

    ma_curl_get_private_data(curl_easy_handle, (void **)&request) ;
    self = (ma_curl_request_handler_t *)request->base.url_handler ;

    MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "URL(%s) request, socket(%d) cb with action %s [curl handle = %p].", request->base.url,  socket, whatstr[action], request->curl_easy->curl_handle);

    if((ctx = find_curl_context(self, socket))) {
        if(CURL_POLL_REMOVE == action) {
            MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "URL(%s) request,  stoping watcher", request->base.url);
            curl_multi_assign(self->curlm_handle, socket, NULL);
            uv_poll_stop(&ctx->poll_handle);
        }else {
            MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "URL(%s) request,  setting watcher", request->base.url);

            if(!request->connect_notification_done && request->base.connect_cb ) {
                ma_error_t rc = MA_OK;
                request->connect_notification_done = MA_TRUE;
                if(MA_OK != (rc = request->base.connect_cb(request->base.userdata, (ma_url_request_t *)request))) {
                    MA_LOG(net_logger, MA_LOG_SEV_ERROR, "URL(%s) request, connection callback failed, %d", request->base.url, rc ) ;
                    ma_curl_request_handler_cancel_request((ma_url_request_handler_t*)self, (ma_url_request_t*)request) ;                
                    return 0;
                }
                MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "URL(%s) request, connection established", request->base.url);
                request->base.request_state = MA_URL_REQUEST_STATE_REQUESTING ;
            }
            curl_multi_assign(self->curlm_handle, socket, ctx);
            uv_poll_start(&ctx->poll_handle, (CURL_POLL_IN == action) ? UV_READABLE : UV_READABLE|UV_WRITABLE, curl_perform_cb);
        }   
    }
    return 0;
}

static void curl_perform_cb(uv_poll_t *req, int status, int events) {
    curl_context_t *ctx = (curl_context_t *)req->data;
    ma_curl_request_handler_t * self = (ma_curl_request_handler_t *)ctx->handler;
    int flags = 0 ;
    int running_handles = 0;

    /*TBD - for now lets keep this log to trouble shoot */
    MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "curl perform cb called with event %d", events ) ;
    
    if (events & UV_READABLE) flags |= CURL_CSELECT_IN ;
    if (events & UV_WRITABLE) flags |= CURL_CSELECT_OUT ;

    curl_multi_socket_action(self->curlm_handle, ctx->sockfd, flags, &running_handles) ;

    if(!running_handles) uv_timer_stop(&self->multi_timer);
    curl_check_for_completed_requests(self) ;
}


static void curl_check_for_completed_requests(ma_curl_request_handler_t * self) {
    CURLMsg *message ;
    int pending = 0 ;
    ma_curl_url_request_t *request = NULL;
    long response_code = 0 ;
	long http_connect_code = 0 ;
    ma_error_t ret_code = MA_OK ;
    /* check whether any requests are completed */
    while ((message = curl_multi_info_read(self->curlm_handle, &pending))) {
        switch (message->msg) {
            case CURLMSG_DONE: {
				CURLcode curl_rc = message->data.result;
                ma_curl_get_private_data(message->easy_handle, (void **)&request) ;
                ma_curl_get_http_response_code(request->curl_easy,  &response_code) ;
				ma_curl_get_http_connect_code(request->curl_easy,  &http_connect_code) ;
                curl_multi_remove_handle(self->curlm_handle, message->easy_handle) ;
				MA_ATOMIC_DECREMENT(self->running_handles) ;

                if(CURLE_OK == curl_rc) {
                    MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "URL(%s) request, completed with Response %ld", request->base.url , response_code) ;
                    ret_code = MA_ERROR_NETWORK_REQUEST_SUCCEEDED ;
					if(request->proxy_list){
						MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "Setting current used proxy index %ld in the proxy list.", request->proxy_list_index) ;
						(void)ma_proxy_list_set_last_proxy_index(request->proxy_list, (ma_int32_t)request->proxy_list_index);			  
					}
                }
                else  {/* failed to resolve proxy */					
                    if(CURLE_COULDNT_RESOLVE_PROXY == curl_rc || CURLE_COULDNT_CONNECT == curl_rc)  {
                        /* check for next proxy to be tried. */
						if(request->proxy_list) {
                            
							MA_LOG(net_logger, MA_LOG_SEV_NOTICE, "URL(%s) request, failed with curl error %d, Response %ld", request->base.url , curl_rc, response_code) ;

							if(MA_OK == set_proxy(request, MA_TRUE)) {
                                MA_LOG(net_logger, MA_LOG_SEV_INFO, "URL(%s) request, Trying for next proxy", request->base.url) ;
                                curl_multi_add_handle(self->curlm_handle, request->curl_easy->curl_handle) ;
                                MA_ATOMIC_INCREMENT(self->running_handles) ;
                                /* breaking, should not call final_cb as it is resubmitted */ 
                                break ;
                            }
                        }
                        ret_code = MA_ERROR_NETWORK_REQUEST_FAILED ;
                    }
					else if(request->proxy_list && (403 == http_connect_code || CURLE_OPERATION_TIMEDOUT == curl_rc) ){
						MA_LOG(net_logger, MA_LOG_SEV_NOTICE, "URL(%s) request, failed with curl error %d, http connect code %ld", request->base.url , curl_rc, http_connect_code) ;

						if(MA_OK == set_proxy(request, MA_TRUE)) {
							MA_LOG(net_logger, MA_LOG_SEV_INFO, "URL(%s) request, Trying for next proxy", request->base.url) ;
							curl_multi_add_handle(self->curlm_handle, request->curl_easy->curl_handle) ;
							MA_ATOMIC_INCREMENT(self->running_handles) ;
							/* breaking, should not call final_cb as it is resubmitted */ 
							break ;
						}                        
                        ret_code = MA_ERROR_NETWORK_REQUEST_FAILED ;
					}
                    else if (CURLE_COULDNT_RESOLVE_HOST == curl_rc) {    /* failed to resolve host */
	                    if(request->resolve) {
	                        MA_LOG(net_logger, MA_LOG_SEV_NOTICE, "URL(%s) request, failed with curl error %d, Response %ld, Trying with the resolve information.", request->base.url, curl_rc, response_code) ;
	                        ma_curl_set_resolve_info(request->curl_easy, request->resolve) ;
	                        curl_multi_add_handle(self->curlm_handle, request->curl_easy->curl_handle) ;
	                        MA_ATOMIC_INCREMENT(self->running_handles) ;
                            /* breaking, should not call final_cb as it is resubmitted */
	                        break ;
	                    }
						else if(request->ip_resolve == MA_CURL_IP_RESOLVE_IPV4) {
							MA_LOG(net_logger, MA_LOG_SEV_NOTICE, "URL(%s) request, failed with curl error %d, Response %ld, Trying with ipv6 resolution", request->base.url, curl_rc, response_code) ;
							request->ip_resolve = MA_CURL_IP_RESOLVE_IPV6 ;
							ma_curl_set_ipresolve(request->curl_easy, MA_CURL_IP_RESOLVE_IPV6) ;
							curl_multi_add_handle(self->curlm_handle, request->curl_easy->curl_handle) ;
	                        MA_ATOMIC_INCREMENT(self->running_handles) ;
							 /* breaking, should not call final_cb as it is resubmitted */
							break ;
						}
                        ret_code = MA_ERROR_NETWORK_RESOLVE_HOST_FAILED ;
                    }
                    else {
                        ret_code = MA_ERROR_NETWORK_REQUEST_FAILED ;
                    }
					MA_LOG(net_logger, MA_LOG_SEV_ERROR, "URL(%s) request, failed with curl error %d, Response %ld", request->base.url , curl_rc, response_code) ;
                }
                request->base.request_state = MA_URL_REQUEST_STATE_COMPLETED ;
                MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "URL(%s) request, completed with Response %ld calling final callback", request->base.url , response_code) ;
                request->base.final_cb(ret_code, response_code, request->base.userdata, (ma_url_request_t *)request) ;
                (void)ma_url_request_release((ma_url_request_t *)request);
                break ;
            }
            default:
                break ;
        }
    }
}

static void curl_clear_pending_requests(ma_curl_request_handler_t * self) {
    CURLMsg *message ;
    int pending ;
    ma_curl_url_request_t *request = NULL;
    long response_code = 0 ;
    ma_error_t ret_code = MA_OK ;

    /* check whether any pending requests */
    while ((message = curl_multi_info_read(self->curlm_handle, &pending))) {
        ma_curl_get_private_data(message->easy_handle, (void **)&request) ;
		switch (message->msg) {
            case CURLMSG_DONE: {
                ma_curl_get_http_response_code(request->curl_easy,  &response_code) ;
                if(CURLE_OK == message->data.result) {
                    MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "URL(%s) request, completed with Response %ld", request->base.url , response_code) ;
                    ret_code = MA_ERROR_NETWORK_REQUEST_SUCCEEDED ;
                }
                else {
                    MA_LOG(net_logger, MA_LOG_SEV_ERROR, "URL(%s) request, failed with curl error %d, Response %ld", request->base.url , message->data.result, response_code) ;
                    ret_code = MA_ERROR_NETWORK_REQUEST_FAILED ;
                }
                break ;
            }
            default: {
                MA_LOG(net_logger, MA_LOG_SEV_ERROR, "URL(%s) request, aborted", request->base.url ) ;
                ret_code = MA_ERROR_NETWORK_REQUEST_ABORTED ;
                break ;
            }
        }
        curl_multi_remove_handle(self->curlm_handle, message->easy_handle) ;
        MA_ATOMIC_DECREMENT(self->running_handles) ;
        request->base.request_state = MA_URL_REQUEST_STATE_COMPLETED ;
        request->base.final_cb(ret_code, response_code, request->base.userdata, (ma_url_request_t *)request) ;
        (void)ma_url_request_release((ma_url_request_t *)request);
        response_code = 0 ;
    }
    return ;
}

static size_t  curl_write_cb(void *ptr, size_t size, size_t nmemb, void *userdata) {
     ma_curl_url_request_t *curl_url_request = (ma_curl_url_request_t *)userdata ;
     size_t written_bytes ;
     
     if(curl_url_request->base.progress_cb && curl_url_request->base.progress_cb->progress_callback) {
        double dltotal = 0.0, dlnow = 0.0;
        curl_easy_getinfo(curl_url_request->curl_easy->curl_handle, CURLINFO_CONTENT_LENGTH_DOWNLOAD, &dltotal);
        curl_easy_getinfo(curl_url_request->curl_easy->curl_handle, CURLINFO_SIZE_DOWNLOAD, &dlnow);
        if(curl_url_request->base.progress_cb->progress_callback(dltotal, dlnow, 0.0, 0.0, curl_url_request->base.userdata)){
			MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "curl_write_cb user requested to stop download") ;    
			return 0;
		}
     }

     if(MA_OK != curl_url_request->base.io_cbs->write_stream->vtable->write(curl_url_request->base.io_cbs->write_stream, (unsigned char *)ptr, (size*nmemb), &written_bytes))
         return 0 ;         

     return written_bytes ;
}

static size_t  curl_read_cb(void *ptr, size_t size, size_t nmemb, void *userdata) {
     ma_curl_url_request_t *curl_url_request = (ma_curl_url_request_t *)userdata ;
     size_t read_bytes ;

     if(curl_url_request->base.progress_cb && curl_url_request->base.progress_cb->progress_callback) {
        double ultotal = 0.0, ulnow = 0.0;
        curl_easy_getinfo(curl_url_request->curl_easy->curl_handle, CURLINFO_CONTENT_LENGTH_UPLOAD, &ultotal);
        curl_easy_getinfo(curl_url_request->curl_easy->curl_handle, CURLINFO_SIZE_UPLOAD, &ulnow);

        if(curl_url_request->base.progress_cb->progress_callback(0.0, 0.0, ultotal, ulnow, curl_url_request->base.userdata)){
			MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "curl_read_cb user requested to stop download") ;    
			return 0;
		}
     }

     if(MA_OK != curl_url_request->base.io_cbs->read_stream->vtable->read(curl_url_request->base.io_cbs->read_stream, (unsigned char *)ptr, (size*nmemb), &read_bytes))
         return 0 ;

     return read_bytes ;
}

static int  curl_progress_cb(void *clientp, double dltotal, double dlnow, double ultotal, double ulnow) {
    ma_curl_url_request_t *curl_url_request = (ma_curl_url_request_t *)clientp ;
    if(dltotal > 0 || ultotal > 0) {
        
        //range is supported for download only
        if(curl_url_request->range_info && MA_URL_REQUEST_TYPE_GET == curl_url_request->base.request_type)
            curl_url_request->range_info->start_position = (unsigned long)dlnow ;

	    curl_url_request->base.progress_cb->progress_callback(dltotal, dlnow, ultotal, ulnow, curl_url_request->base.userdata) ;
        /* Disable the download progress call back now, and this callback gets called from write_cb or read_cb */
        curl_easy_setopt(curl_url_request->curl_easy->curl_handle, CURLOPT_PROGRESSFUNCTION, NULL);
        curl_easy_setopt(curl_url_request->curl_easy->curl_handle, CURLOPT_NOPROGRESS, MA_TRUE);        
    }
	return 0;
}

static char *filter_header_credentails(char *data, size_t size, void *userp){
	ma_curl_url_request_t *self = (ma_curl_url_request_t*) userp;
	if(self->auth && self->auth->password){		
		char message[256];
		memset(message, 0, sizeof(message));
		strncpy(message, data, sizeof(message)-1);
		if(MA_URL_TRANSER_PROTOCOL_FTP == self->base.protocol){
			if(strstr(message, self->auth->password)){
				return NULL;
			}
		}
	}
	return data;
}

static void curl_debugtrace_cb(CURL *handle, curl_infotype type,char *data, size_t size,void *userp) {
	char message[256];
	int len = 0;
	const char *description;
	const char *data_ptr = NULL;

	memset(message, '\0', sizeof(message));

	switch (type){		
		case CURLINFO_HEADER_OUT:
			description = "snd head";
			data_ptr = filter_header_credentails(data, size, userp);			
			break;
		case CURLINFO_HEADER_IN:
			description = "rcv head";
			data_ptr = data;
			break;
		case CURLINFO_TEXT:
		case CURLINFO_DATA_OUT:            
		case CURLINFO_DATA_IN:			
		case CURLINFO_SSL_DATA_OUT:			
		case CURLINFO_SSL_DATA_IN:						
		default:			
			return;
	}
		
	len = MA_MSC_SELECT(_snprintf, snprintf)(message, sizeof(message), "%s %ld ", description, size);	
	if(data_ptr){
		size_t i = 0;		
		for (; i < size && len < (sizeof(message)-1); ++i){
			if (data[i] == '`')
				message[len++] = data[i];				
			else if (data[i] >= ' ' && data[i] < 127)
				message[len++] = data[i];				
			else
				message[len++] = ' ';				
		}		
	}	
	MA_LOG(net_logger, MA_LOG_SEV_TRACE, message) ;	
    return;
}


static curl_socket_t open_socket_cb(void *clientp, curlsocktype purpose, struct curl_sockaddr *addr) {
    curl_socket_t sockfd = -1;
    if(-1 != (sockfd = socket(addr->family, addr->socktype, addr->protocol))) {
        if(add_curl_context((ma_curl_request_handler_t *)clientp, sockfd)) {
            MA_LOG(net_logger, MA_LOG_SEV_TRACE, "curl open socket callback, socket %d", sockfd);
            return sockfd;
        }
        MA_MSC_SELECT(closesocket,close)(sockfd);
    }
    return CURL_SOCKET_BAD;
}

static int close_socket_cb(void *clientp, curl_socket_t socket) {
    MA_LOG(net_logger, MA_LOG_SEV_TRACE, "curl close socket callback, socket %d", socket);
    remove_curl_context((ma_curl_request_handler_t *)clientp, socket);
    return 0;
}

static ma_error_t curl_handle_create(ma_curl_request_handler_t *self, ma_curl_url_request_t *request ) {
        ma_curl_t *curl_handle = request->curl_easy ;
        ma_error_t ma_net_return = MA_OK ;
        ma_url_info_t *url_info = NULL ;

        if(MA_OK != (ma_net_return = ma_curl_init(curl_handle)) )
            return ma_net_return ;


        
        if(MA_OK != (ma_net_return = ma_url_utils_url_info_create(request->base.url, MA_TRUE, &url_info)) ) {
            ma_curl_release(curl_handle) ;
            return ma_net_return ;
        }

		ma_curl_set_url(curl_handle,  (ma_curl_request_type_t)request->base.request_type, (ma_curl_transfer_protocol_t)request->base.protocol, request->base.connect_timeout, request->base.transfer_timeout, request->base.use_fresh_connection, url_info) ;

		if(!ma_url_utils_is_ip(url_info)) {
			request->ip_resolve = MA_CURL_IP_RESOLVE_IPV4 ;
			ma_curl_set_ipresolve(curl_handle, request->ip_resolve) ;
		}

        ma_curl_set_private_data(curl_handle, (void *)request) ;

        if(request->base.verbose_log)
           ma_curl_set_curltrace_cb(curl_handle, curl_debugtrace_cb, (void *)request) ;
        

        /* set proxy */
        if(request->proxy_list) 
            (void)set_proxy(request, MA_FALSE);        

        /* set auth */
        if(request->auth)
            ma_curl_set_url_auth_info(curl_handle, request->auth) ;

        /* set certificate */
        if(request->ssl_info)
            ma_curl_set_url_cert_path(curl_handle, request->ssl_info) ;

        /* set io callbacks */
        if(request->base.io_cbs) {
            if(request->base.io_cbs->read_stream)
                ma_curl_set_url_read_cb(curl_handle, curl_read_cb, (void *)request) ;
            if(request->base.io_cbs->write_stream)
                ma_curl_set_url_write_cb(curl_handle, curl_write_cb, (void *)request) ;
        }

        /* set progress callback */
        if(request->base.progress_cb)
            ma_curl_set_url_progress_cb(curl_handle, curl_progress_cb, (void *)request) ;

        /* set range -- for download only */
        if(request->range_info && MA_URL_REQUEST_TYPE_GET == request->base.request_type)
            ma_curl_set_range(curl_handle, request->range_info->start_position, request->range_info->end_position) ;

        /* set header */
		if(MA_URL_TRANSER_PROTOCOL_HTTP == request->base.protocol && MA_URL_REQUEST_TYPE_POST == request->base.request_type && request->base.io_cbs && request->base.io_cbs->read_stream) {
            char *pbuffer = NULL ;
            ma_temp_buffer_t buffer = {0} ;
            size_t content_length = 0 ;
			ma_curl_slist_release(curl_handle);

            ma_curl_set_slist_append(curl_handle, "Accept: application/octet-stream") ;
            ma_curl_set_slist_append(curl_handle, "Accept-Language: en-us") ;
            /*ma_curl_set_slist_append(curl_handle, "Expect: 100-continue") ;*/

            ma_temp_buffer_init(&buffer) ;
            ma_temp_buffer_reserve(&buffer, strlen("Host: %s") + IP_ADDRESS_LEN_MAX ) ;
            MA_MSC_SELECT(_snprintf, snprintf)(pbuffer = (char*)ma_temp_buffer_get(&buffer), ma_temp_buffer_capacity(&buffer), "Host: %s", url_info->host_ip_addr->address) ;
            ma_curl_set_slist_append(curl_handle, pbuffer) ;

            ma_curl_set_slist_append(curl_handle, "Content-Type: application/octet-stream") ;

            ma_curl_set_header(curl_handle) ;
            ma_temp_buffer_uninit(&buffer) ;
			request->base.io_cbs->read_stream->vtable->get_size(request->base.io_cbs->read_stream, &content_length) ;
            curl_easy_setopt (curl_handle->curl_handle,CURLOPT_POSTFIELDSIZE_LARGE,(curl_off_t) content_length); 

        }
		else if(MA_URL_TRANSER_PROTOCOL_HTTP == request->base.protocol && MA_URL_REQUEST_TYPE_GET == request->base.request_type) {
			ma_curl_slist_release(curl_handle);
			ma_curl_set_custom_header(curl_handle, &request->base.custom_header);
			ma_curl_set_header(curl_handle) ;
		}

		if(MA_URL_TRANSER_PROTOCOL_HTTP == request->base.protocol && MA_URL_REQUEST_TYPE_POST == request->base.request_type && request->base.post_fields) {
            ma_curl_set_post_fields(curl_handle, request->base.post_fields);
        }
        (void)ma_url_utils_url_info_release(url_info) ;
        return MA_OK ;
}


static void poller_close_cb(uv_handle_t *handle) {
    curl_context_t *ctx = (curl_context_t *) handle->data;
    MA_LOG(net_logger, MA_LOG_SEV_TRACE, "poller closed %d", ctx->sockfd);
    MA_MSC_SELECT(closesocket,close)(ctx->sockfd);
    free(ctx);
    
}

static curl_context_t *find_curl_context(ma_curl_request_handler_t *self, curl_socket_t socket) {
    size_t count = ma_vector_get_size(self,curl_context_vec);
    size_t i = 0 ;
    for(i = 0 ; i < count ; i++) {
        curl_context_t *ctx = NULL;
        if( (ctx = ma_vector_at(self, i, curl_context_t *, curl_context_vec)) && (ctx->sockfd == socket)) 
            return ctx;
    }
    return NULL;
}

static ma_error_t curl_context_release_cb(curl_context_t *ctx) {
    return MA_OK;
}

static void remove_curl_context(ma_curl_request_handler_t *self, curl_socket_t socket) {
    curl_context_t *ctx = NULL;
    if((ctx = find_curl_context(self, socket))) {
        uv_poll_stop(&ctx->poll_handle);
        uv_close((uv_handle_t *)&ctx->poll_handle, poller_close_cb);
        ma_vector_remove(self,ctx,curl_context_t *, curl_context_vec, curl_context_release_cb);
    }else {
        MA_LOG(net_logger, MA_LOG_SEV_TRACE, "poller object not found.");
    }
}

static curl_context_t *add_curl_context(ma_curl_request_handler_t *self, curl_socket_t socket) {
    curl_context_t *ctx = NULL;
    if(!(ctx = find_curl_context(self, socket))) {
        if(ctx = (curl_context_t *)calloc(1, sizeof(curl_context_t))) {
            ctx->sockfd = socket;
            ctx->handler = self;
            ctx->poll_handle.data = ctx;
			MA_LOG(net_logger, MA_LOG_SEV_TRACE, "curl context addr = %p", ctx);
            if(0 == uv_poll_init_socket(ctx->handler->uv_loop, &ctx->poll_handle, ctx->sockfd)) {
                ma_vector_push_back(self,ctx, curl_context_t *, curl_context_vec);
                return ctx;
            }
            free(ctx),ctx = NULL;
        }
    }
    return ctx;
}


#endif  /* ifdef (MA_NETWORK_USE_CURL) || defined(MA_NETWORK_USE_ALL) */


