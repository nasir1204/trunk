#if defined (MA_NETWORK_USE_UNC) || defined(MA_NETWORK_USE_ALL)
#include "ma_unc_request.h"
#include "ma_unc_request_handler.h"
#include "ma/internal/utils/text/ma_utf8.h"
#include "ma/internal/utils/network/ma_url_request_handler.h"
#include "ma_url_utils.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static ma_error_t ma_unc_request_release(ma_url_request_t *url_request);
static ma_error_t ma_unc_request_set_url_auth_info(ma_url_request_t *url_request, ma_url_request_auth_t *authenticate) ;
static ma_error_t ma_unc_request_set_ssl_info(ma_url_request_t *url_request, ma_url_request_ssl_info_t *ssl_info);
static ma_error_t ma_unc_request_set_range_info(ma_url_request_t *url_request, ma_url_request_range_t *range_info);
static ma_error_t ma_unc_request_set_proxy_info(ma_url_request_t *url_request, ma_proxy_list_t *proxy_list) ;
static ma_error_t ma_unc_request_set_resolve_info(ma_url_request_t *url_request, ma_url_request_resolve_t *resolve_info);

static const struct ma_url_request_methods_s curl_request_methods = {
    &ma_unc_request_release,
    &ma_unc_request_set_url_auth_info,
    &ma_unc_request_set_ssl_info,
    &ma_unc_request_set_range_info,
    &ma_unc_request_set_proxy_info,
    &ma_unc_request_set_resolve_info
};

ma_error_t ma_unc_url_request_create(const char *url, ma_unc_url_request_t **unc_url_request) {
    ma_error_t rc = MA_OK ;  
	ma_unc_url_info_t *ma_unc_url_info = NULL;
    ma_unc_url_request_t *unc_url_request_tmp = (ma_unc_url_request_t *)calloc(1, sizeof(ma_unc_url_request_t)) ;
    if(!unc_url_request_tmp) {
        MA_NET_LOG_MEMERR() ;
        return MA_ERROR_OUTOFMEMORY ;
    }
    
    unc_url_request_tmp->auth = NULL ;
	unc_url_request_tmp->networkshare_created = MA_TRUE;
	unc_url_request_tmp->use_WNet = MA_FALSE;
	unc_url_request_tmp->error_state = MA_OK;
	unc_url_request_tmp->fs_open_req.data = unc_url_request_tmp;
	unc_url_request_tmp->fs_read_req.data = unc_url_request_tmp;

    ma_url_request_init((ma_url_request_t *)unc_url_request_tmp, url, &curl_request_methods, 1);
    *unc_url_request = unc_url_request_tmp ;
	ma_url_utils_unc_url_info_create((*unc_url_request)->base.url,&ma_unc_url_info);
	(*unc_url_request)->ma_unc_url_info = ma_unc_url_info;
    if(MA_OK != (rc = ma_url_utils_get_protocol_type(url, &unc_url_request_tmp->base.protocol))) {
        ma_unc_request_release((ma_url_request_t *)unc_url_request) ;
        return rc ;
    }
    return rc ;
}
ma_error_t ma_unc_request_release(ma_url_request_t *url_request) {
    ma_unc_url_request_t *unc_url_request = (ma_unc_url_request_t *)url_request ;
    if(unc_url_request->base.url)  free(unc_url_request->base.url) ;
	ma_url_utils_unc_url_info_release(unc_url_request->ma_unc_url_info);
    free(unc_url_request) ;
    return MA_OK ;
}

ma_error_t ma_unc_request_set_url_auth_info(ma_url_request_t *url_request, ma_url_request_auth_t *authenticate) {
    ma_unc_url_request_t *unc_url_request = (ma_unc_url_request_t *)url_request ;
    if(authenticate)
        unc_url_request->auth = authenticate ;
    return MA_OK ;
}

ma_error_t ma_unc_request_set_ssl_info(ma_url_request_t *url_request, ma_url_request_ssl_info_t *ssl_info) {
   return MA_OK ;
}

ma_error_t ma_unc_request_set_range_info(ma_url_request_t *url_request, ma_url_request_range_t *range_info) {
   return MA_OK ;
}

ma_error_t ma_unc_request_set_proxy_info(ma_url_request_t *url_request, ma_proxy_list_t *proxy_list) {
    MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "Downloading files from UNC repository via proxy not supported") ;
	return MA_OK ;
}

ma_error_t ma_unc_request_set_resolve_info(ma_url_request_t *url_request, ma_url_request_resolve_t *resolve_info) {
    return MA_OK ;
}


#endif  //// ifdef (MA_NETWORK_USE_UNC) || defined(MA_NETWORK_USE_ALL)

