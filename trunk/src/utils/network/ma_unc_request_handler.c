#if defined (MA_NETWORK_USE_UNC) || defined(MA_NETWORK_USE_ALL)
#include "ma/internal/utils/text/ma_utf8.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma_unc_request_handler.h"
#include "ma_unc_request.h"
#include "ma_url_utils.h"
#include "ma/internal/ma_macros.h"
#include <Windows.h>
#include <lm.h>
#include <time.h>
#include <strsafe.h>
#include <LMCons.h>
#include <fcntl.h>
#include "ma_win_connection.h"

#define USE_WILDCARD            ( (DWORD) (-1) )
#define STR_SIZEOF(s)	(sizeof(s) / sizeof(*(s)) )
#define MAXREADBUF  32 * 1024

HMODULE g_hUNCNetapi32Lib	= NULL;
extern char g_share[MAX_PATH];

static ma_error_t unc_unmap_networkshare(ma_unc_url_request_t *unc_url_request);
static ma_error_t unc_map_networkshare(ma_unc_url_request_t *unc_url_request);
static ma_error_t unc_downloadfile (ma_unc_url_request_t *unc_url_request);
ma_error_t unc_downloadfile_async(ma_unc_request_handler_t *self,ma_unc_url_request_t *unc_url_request);

static void file_open_cb(uv_fs_t* req) ;
static void file_read_cb(uv_fs_t* req) ;
static void file_read(ma_unc_request_handler_t *self, ma_unc_url_request_t *unc_request);
static void file_close(ma_unc_request_handler_t *self, ma_unc_url_request_t *unc_request);
static void log_out(ma_unc_url_request_t *unc_url_request);

static ma_error_t ma_unc_request_handler_start(ma_url_request_handler_t *self) ;
static ma_error_t ma_unc_request_handler_stop(ma_url_request_handler_t *self) ;
static ma_error_t ma_unc_request_handler_is_running(ma_url_request_handler_t *self,ma_bool_t*) ;
static ma_error_t ma_unc_request_handler_release(ma_url_request_handler_t *self) ;
static ma_error_t ma_unc_request_handler_create_request(ma_url_request_handler_t *self, const char *url, ma_url_request_t **url_request) ;
static ma_error_t ma_unc_request_handler_submit_request(ma_url_request_handler_t *self, ma_url_request_t *url_request, ma_url_request_finalize_callback_t completion_cb, void *userdata) ;
static ma_error_t ma_unc_request_handler_cancel_request(ma_url_request_handler_t *self , ma_url_request_t *url_request) ;
static ma_error_t ma_unc_request_handler_query_request_state(ma_url_request_handler_t *self , ma_url_request_t *url_request, ma_url_request_state_t *state) ;



static const struct ma_url_request_handler_methods_s unc_request_handler_methods = {
    &ma_unc_request_handler_start ,
    &ma_unc_request_handler_stop ,
    &ma_unc_request_handler_is_running ,
    &ma_unc_request_handler_release ,
    &ma_unc_request_handler_create_request ,
    &ma_unc_request_handler_submit_request ,
    &ma_unc_request_handler_cancel_request ,
    &ma_unc_request_handler_query_request_state
} ;

ma_error_t ma_unc_request_handler_create(ma_event_loop_t* event_loop, ma_url_request_handler_t** unc_request_handler) {
    if(event_loop && unc_request_handler) {
        ma_unc_request_handler_t *self  = (ma_unc_request_handler_t *)calloc(1, sizeof(ma_unc_request_handler_t)) ;

        if(!self) return MA_ERROR_OUTOFMEMORY;

        self->base.vtable = &unc_request_handler_methods ;       
        self->ma_loop = event_loop ;
		
        *unc_request_handler = (ma_url_request_handler_t *)self ;

        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_unc_request_handler_start(ma_url_request_handler_t *self) {
    MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "UNC request handler started") ;
    return MA_OK ;
}

ma_error_t ma_unc_request_handler_stop(ma_url_request_handler_t *self) {
    MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "UNC request handler stopped") ;
	return MA_OK ;
}

ma_error_t ma_unc_request_handler_is_running(ma_url_request_handler_t *self , ma_bool_t *running) {
    return MA_OK ;
}

ma_error_t ma_unc_request_handler_release(ma_url_request_handler_t *self) {

    return MA_OK;
}

ma_error_t ma_unc_request_handler_create_request(ma_url_request_handler_t *self, const char *url, ma_url_request_t **url_request) {
 //   ma_error_t rc = MA_OK ;
 //   if(MA_OK != (rc = ma_unc_url_request_create(url, (ma_unc_url_request_t **)url_request)) ) {
 //       MA_LOG(net_logger, MA_LOG_SEV_ERROR, "URL request create failed");
 //       return rc ;
 //   }
	//MA_LOG(net_logger, MA_LOG_SEV_INFO, "UNC URL request created");
 //   (*url_request)->url_handler = self ;
	//
	//return MA_OK;

	ma_error_t rc = MA_OK ;
	ma_unc_url_request_t *request = NULL;

    if(MA_OK == (rc = ma_unc_url_request_create(url, &request))) {
		MA_LOG(net_logger, MA_LOG_SEV_INFO, "UNC URL request created");
		request->base.url_handler = self ;
		*url_request = (ma_url_request_t*)request;
		return MA_OK;
	}
	else{
        MA_LOG(net_logger, MA_LOG_SEV_ERROR, "UNC URL request create failed");
        return rc ;
	}	
	return MA_OK;
}

static void log_out(ma_unc_url_request_t *unc_url_request){
	ma_error_t ma_net_ret = MA_OK ;
	
	if(unc_url_request->m_hToken){
		MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "closed impersonation token");
		CloseImpersonatedToken(unc_url_request->m_hToken);
		unc_url_request->m_hToken = NULL;
	}
	if(unc_url_request->networkshare_created){
		ma_net_ret = unc_unmap_networkshare(unc_url_request);
			if(ma_net_ret != MA_OK)
				MA_LOG(net_logger, MA_LOG_SEV_ERROR, "Unmapping network share failed");
	}

	
}
ma_error_t ma_unc_request_handler_submit_request(ma_url_request_handler_t *self, ma_url_request_t *url_request , ma_url_request_finalize_callback_t completion_cb, void *userdata) {
    ma_unc_url_request_t *unc_url_request = (ma_unc_url_request_t *)url_request ;
    ma_unc_request_handler_t *unc_request_handler = (ma_unc_request_handler_t *)self ;
    ma_error_t ma_net_ret = MA_OK ;
	ma_error_t map_share_ret = MA_OK ;
	ma_bool_t bOK = MA_TRUE;
	ma_bool_t unc_sync_download = MA_TRUE;
    unc_url_request->base.final_cb = completion_cb ;
    unc_url_request->base.userdata = userdata ;
    unc_url_request->base.request_state = MA_URL_REQUEST_STATE_INITIALIZING ;

	if(unc_url_request->base.final_cb)
		unc_sync_download = MA_FALSE;
	MA_LOG(net_logger, MA_LOG_SEV_INFO, "UNC url to download %s",unc_url_request->base.url);
	if ( unc_url_request->auth && unc_url_request->auth->use_loggedOn_user_credentials)
	{
		RevertToSelf();
		ma_net_ret = UserImpersonateLoggedOnUser(&unc_url_request->m_hToken);			
		if(ma_net_ret == MA_OK){
			if(unc_sync_download)
				ma_net_ret = unc_downloadfile(unc_url_request);
			else
				ma_net_ret = unc_downloadfile_async(unc_request_handler,unc_url_request);
		}
	}
	else
	{
		map_share_ret = unc_map_networkshare(unc_url_request);
		if ( map_share_ret != MA_OK )
		{		
			RevertToSelf();
			ma_net_ret = UserImpersonateLoggedOnUser(&unc_url_request->m_hToken);
		}
		if(map_share_ret == MA_OK || ma_net_ret == MA_OK){
			if(unc_sync_download)
				ma_net_ret = unc_downloadfile(unc_url_request);
			else
				ma_net_ret = unc_downloadfile_async(unc_request_handler,unc_url_request);
		}
	}
	
	if(unc_sync_download)
		log_out(unc_url_request);

	unc_url_request->base.request_state = MA_URL_REQUEST_STATE_COMPLETED ;
	if(ma_net_ret == MA_OK)
		return MA_ERROR_NETWORK_REQUEST_SUCCEEDED;
	return MA_ERROR_NETWORK_REQUEST_FAILED ;
}

ma_error_t ma_unc_request_handler_query_request_state(ma_url_request_handler_t *self , ma_url_request_t *url_request, ma_url_request_state_t *state) {
    
    return MA_OK ;
}

ma_error_t ma_unc_request_handler_cancel_request(ma_url_request_handler_t *self , ma_url_request_t *url_request) {
	if(self && url_request) {
		url_request->request_state = MA_URL_REQUEST_STATE_CANCELLED;
	}
    return MA_OK ;
}

NET_API_STATUS NET_API_FUNCTION
naiNetUseAdd (
    IN LPWSTR UncServerName OPTIONAL,
    IN DWORD Level,
    IN LPBYTE Buf,
    OUT LPDWORD ParmError OPTIONAL
    )
{
	NET_API_STATUS	status;

    static NET_API_STATUS (NET_API_FUNCTION * lpfnNetUseAdd)(
		LPWSTR UncServerName,
		DWORD Level,
		LPBYTE Buf,
		LPDWORD ParmError
        ) = NULL;

	if(!g_hUNCNetapi32Lib) 
		g_hUNCNetapi32Lib = LoadLibraryA("NetApi32.dll");
    
    if(g_hUNCNetapi32Lib) {

        *((FARPROC*)&lpfnNetUseAdd) = GetProcAddress( g_hUNCNetapi32Lib, "NetUseAdd");
    }
    
    if (lpfnNetUseAdd)
        status = lpfnNetUseAdd(UncServerName, Level, Buf, ParmError);
    else
        status = 0;

	return status;
    
}

NET_API_STATUS NET_API_FUNCTION
naiNetUseDel (
    IN LPWSTR   UncServerName OPTIONAL,
    IN LPCWSTR  UseName,
    IN DWORD    ForceCond
    )
{
	NET_API_STATUS	status;

    static NET_API_STATUS (NET_API_FUNCTION * lpfnNetUseDel)(
		LPWSTR UncServerName,
		LPWSTR UseName,
		DWORD ForceCond
        ) = NULL;

	if(!g_hUNCNetapi32Lib) 
		g_hUNCNetapi32Lib = LoadLibraryA("NetApi32.dll");
    
    if(g_hUNCNetapi32Lib) {

        *((FARPROC*)&lpfnNetUseDel) = GetProcAddress( g_hUNCNetapi32Lib, "NetUseDel");
    }
    
    if (lpfnNetUseDel)
	{
		MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "Disconnecting UNC Server using NetUseDel(), Force ");
		status = lpfnNetUseDel(UncServerName, (wchar_t *)UseName, ForceCond);
		MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "NetUseDel() returned %d", status );
	}
    else
	{
        status = -1;
	}

	return status;
    
}

NET_API_STATUS NET_API_FUNCTION
naiNetUseGetInfo (
  LPCWSTR UncServerName,
  LPCWSTR UseName,
  DWORD Level,
  LPBYTE *BufPtr
)
{
	NET_API_STATUS	status;

    static NET_API_STATUS (NET_API_FUNCTION * lpfnNetUseGetInfo)(
        LPWSTR UncServerName,
        LPWSTR UseName,
        DWORD Level,
        LPBYTE *BufPtr
        ) = NULL;

	if(!g_hUNCNetapi32Lib) 
		g_hUNCNetapi32Lib = LoadLibraryA("NetApi32.dll");
    
    if(g_hUNCNetapi32Lib) {

        *((FARPROC*)&lpfnNetUseGetInfo) = GetProcAddress( g_hUNCNetapi32Lib, "NetUseGetInfo");
    }
    
    if (lpfnNetUseGetInfo)
        status = lpfnNetUseGetInfo((wchar_t *)UncServerName, (wchar_t *)UseName, Level, BufPtr);
    else
        status = 0;

	return status;
    
}

NET_API_STATUS NET_API_FUNCTION
naiNetApiBufferFree(
  LPVOID Buffer  
)
{
	NET_API_STATUS	status;

    static NET_API_STATUS (NET_API_FUNCTION * lpfnNetApiBufferFree)(
        LPVOID Buffer
        ) = NULL;

	if(!g_hUNCNetapi32Lib) 
		g_hUNCNetapi32Lib = LoadLibraryA("NetApi32.dll");
    
    if(g_hUNCNetapi32Lib) {

        *((FARPROC*)&lpfnNetApiBufferFree) = GetProcAddress( g_hUNCNetapi32Lib, "NetApiBufferFree");
    }
    
    if (lpfnNetApiBufferFree)
        status = lpfnNetApiBufferFree(Buffer);
    else
        status = 0;

	return status;
    
}


ma_error_t unc_map_networkshare(ma_unc_url_request_t *unc_url_request)
{
	wchar_t networkshare[MAX_PATH] = {0};
	DWORD dwParamErr;
	DWORD		dwErr = 0;
	BOOL	bOK = TRUE;
	size_t networkShareNameSize = 0;
	ma_error_t ma_net_ret = MA_OK ;

    typedef struct _NAINET_USE_INFO_2 {
        LPCWSTR   ui2_local;
        LPCWSTR   ui2_remote;
        LPCWSTR   ui2_password;
        DWORD    ui2_status;
        DWORD    ui2_asg_type;
        DWORD    ui2_refcount;
        DWORD    ui2_usecount;
        LPCWSTR   ui2_username;
        LPCWSTR   ui2_domainname;
    }NAINET_USE_INFO_2;

    typedef struct _NAINET_USE_INFO_0 {
        LPCWSTR  ui0_local;
        LPCWSTR  ui0_remote;
    }NAINET_USE_INFO_0;


    NAINET_USE_INFO_0   *pui0 = NULL;
	NAINET_USE_INFO_2	ui2 = { 0 };

	if(g_share[0] != '\0'){
		ma_utf8_to_wide_char(networkshare,MAX_PATH,g_share,strlen(g_share));
		MA_LOG(net_logger, MA_LOG_SEV_INFO, "Mapping network share %S",networkshare);
	
		dwErr = naiNetUseGetInfo(NULL, networkshare, 0, (LPBYTE *)&pui0);
		if ( dwErr == NERR_Success && pui0 != NULL )
		{
			naiNetApiBufferFree(pui0);
			pui0 = NULL;
			MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "Network Share %S is already existing",networkshare);
			unc_url_request->networkshare_created = MA_TRUE;
		}
		else
		{
			wchar_t username[UNLEN] = {0};
			wchar_t password[PWLEN] = {0};
			wchar_t domainname[UNLEN] = {0};
			
			ui2.ui2_local = NULL;
			ui2.ui2_remote = networkshare;
			ui2.ui2_asg_type = USE_WILDCARD;

			if(unc_url_request->auth && unc_url_request->auth->domainname != NULL && unc_url_request->auth->user != NULL && unc_url_request->auth->password != NULL){

				ma_utf8_to_wide_char(username,UNLEN,unc_url_request->auth->user,strlen(unc_url_request->auth->user));
				ma_utf8_to_wide_char(domainname,UNLEN,unc_url_request->auth->domainname,strlen(unc_url_request->auth->domainname));
				ma_utf8_to_wide_char(password,PWLEN,unc_url_request->auth->password,strlen(unc_url_request->auth->password));				
				
				ui2.ui2_password = password;
				ui2.ui2_domainname = domainname;
				ui2.ui2_username = username;
			}

		    MA_LOG(net_logger, MA_LOG_SEV_INFO, "Domain name=%S, User name=%S",ui2.ui2_domainname,ui2.ui2_username);
       
			networkShareNameSize = wcslen(ui2.ui2_remote);
			dwErr = naiNetUseAdd(NULL, 2, (LPBYTE)&ui2, &dwParamErr);
			bOK = dwErr == NERR_Success ;//|| dwErr == 53; // Netware share gets this error

			if ( !bOK )
			{
				DWORD res = 0;
				unc_url_request->networkshare_created = MA_FALSE;
				MA_LOG(net_logger, MA_LOG_SEV_ERROR, "Failed to MAP share using NetUseAdd error %u", dwErr);
				if (networkShareNameSize < MAX_PATH)
				{   
					bOK = TRUE;
					if (bOK)
					{
						wchar_t domainUsername[UNLEN*2] = {0};
						wchar_t shareName[MAX_PATH] = {0};
						NETRESOURCEW nr = {0};

						nr.dwType = RESOURCETYPE_ANY;
						wcscpy_s(shareName , MAX_PATH, ui2.ui2_remote);
						nr.lpRemoteName = shareName;

						swprintf_s(domainUsername,STR_SIZEOF(domainUsername),L"%s\\%s",ui2.ui2_domainname,ui2.ui2_username);
						res = WNetAddConnection2W(&nr, ui2.ui2_password, domainUsername, FALSE);
					
						bOK = res == NO_ERROR ;
					}

					if ( !bOK )
					{
						ma_net_ret = MA_ERROR_NETWORK_REQUEST_FAILED;
						MA_LOG(net_logger, MA_LOG_SEV_ERROR, "Failed to connect to network share by WNetAddConnection2W");
					}
					else
					{  
						unc_url_request->use_WNet = MA_TRUE;
						unc_url_request->networkshare_created = MA_TRUE;
						MA_LOG(net_logger, MA_LOG_SEV_INFO, "connected to network share by WNetAddConnection2W");
					}
				}
            
			}
			else
			{	
				unc_url_request->networkshare_created = MA_TRUE;
				MA_LOG(net_logger, MA_LOG_SEV_INFO, "network share connected using netuseadd");
			}
		}
	}
	else
		return MA_ERROR_NETWORK_REQUEST_FAILED;
    return ma_net_ret;
}

ma_error_t unc_unmap_networkshare(ma_unc_url_request_t *unc_url_request)
{
    wchar_t networkshare[MAX_PATH] = {0};
	ma_error_t ma_net_ret = MA_OK ;
	if(g_share[0] != '\0' && unc_url_request->networkshare_created)
        {
            NET_API_STATUS nas;
			ma_utf8_to_wide_char(networkshare,MAX_PATH,g_share,strlen(g_share));
            if (unc_url_request->use_WNet == MA_FALSE) // if initial connect was via NetUse api, use NetUseDel
            {
                nas = naiNetUseDel(NULL, networkshare, USE_FORCE);
                if ( nas == NERR_Success )
                {
					MA_LOG(net_logger, MA_LOG_SEV_INFO, "Network share %S disconnected successfully(1)",networkshare);
                }
                else
                {
					ma_net_ret = MA_ERROR_INVALIDARG;
					MA_LOG(net_logger, MA_LOG_SEV_ERROR, "Error disconnecting share using NetUseDel(), switching to WNetCancelConnection2() " );
                }
            }
            else    // Use WNet api to disconnect
            {
                if ( WNetCancelConnection2W( networkshare ,0 ,TRUE ) == NO_ERROR ){
					MA_LOG(net_logger, MA_LOG_SEV_INFO, "Network share %S disconnected successfully via WNet (2)", networkshare);
				}
                else
				{
					ma_net_ret = MA_ERROR_INVALIDARG;
					MA_LOG(net_logger, MA_LOG_SEV_ERROR, "Failed to disconnect Network share via WNet: %S", networkshare);
				}
            }
        }
	return ma_net_ret;		
}



ma_error_t unc_downloadfile(ma_unc_url_request_t *unc_url_request)
{
	BOOL	bOK					= TRUE;
	HANDLE	hLocalFile			= INVALID_HANDLE_VALUE;
	HANDLE	hRemoteFile			= INVALID_HANDLE_VALUE;
	DWORD	dwBytesRead			= 0;
	DWORD	dwBytesWritten		= 0;

	wchar_t	szRemFilePath[MAX_PATH] =  {0};     
	wchar_t filename[MAX_PATH] = {0};
	wchar_t *ptmp = NULL;
	ma_error_t ma_net_ret = MA_OK ;

	bOK = TRUE;
	if(*(unc_url_request->ma_unc_url_info->url_info.formatted_url)  != '\0'){
		ma_utf8_to_wide_char(szRemFilePath,MAX_PATH,unc_url_request->ma_unc_url_info->url_info.formatted_url,strlen(unc_url_request->ma_unc_url_info->url_info.formatted_url));

		MA_LOG(net_logger, MA_LOG_SEV_INFO, "Downloading file: %S from UNC Server", szRemFilePath );
		
		hRemoteFile =  CreateFile( szRemFilePath, 
								GENERIC_READ,
								FILE_SHARE_READ|FILE_SHARE_WRITE, //Support simultanious multiple uploads
								(LPSECURITY_ATTRIBUTES)NULL, 
								OPEN_EXISTING , 
								FILE_ATTRIBUTE_NORMAL, 
								NULL ); 
			
		bOK = hRemoteFile != INVALID_HANDLE_VALUE;
	}

	if(!bOK)
	{
		MA_LOG(net_logger, MA_LOG_SEV_ERROR, "File %S Open() failed %d", szRemFilePath, GetLastError ());  
		return MA_ERROR_INVALIDARG;
	}

    if ( bOK )
    { 
		unsigned char ReadBuff[MA_MAX_BUFFER_LEN] = {0};
		size_t written_bytes = 0;
	
		while ( ReadFile ( hRemoteFile, ReadBuff, MA_MAX_BUFFER_LEN-1, &dwBytesRead, NULL ) 
			&& dwBytesRead > 0 )
		{		
			if(unc_url_request->base.io_cbs != NULL){
				if(unc_url_request->base.io_cbs->write_stream != NULL){
					ma_net_ret = unc_url_request->base.io_cbs->write_stream->vtable->write(unc_url_request->base.io_cbs->write_stream,(unsigned char *)ReadBuff,dwBytesRead,&written_bytes);
				}
			}
		}	
		
	}		
		
	//	close handles
	if ( hRemoteFile != INVALID_HANDLE_VALUE )
	{
		CloseHandle (  hRemoteFile ) ;		
	}	

	if ( hLocalFile != INVALID_HANDLE_VALUE )
	{
		//We use SetEndOfFile() here incase we are writing over the top of an existing file
		//with a new one that is shorter.  This prevents the end of the file containing
		//content from the previous file.
		SetEndOfFile( hLocalFile );
		CloseHandle (  hLocalFile ) ;		
	}
	
	return ma_net_ret;
}

void file_open_cb(uv_fs_t* req) {
	ma_unc_url_request_t *unc_request = (ma_unc_url_request_t *)req->data ;
	ma_unc_request_handler_t *self = (ma_unc_request_handler_t*)(unc_request->base.url_handler);
	
	unc_request->error_state = MA_ERROR_FILE_OPEN_FAILED;
	MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "file_open_cb %d", req->result);
    if(req->result >= 0) {
        uv_fs_t stat_req ;
		unc_request->file = (uv_file)req->result ;    

		if(!uv_fs_fstat(ma_event_loop_get_uv_loop(self->ma_loop), &stat_req, unc_request->file, NULL)) {
			
			#ifdef MA_WINDOWS
            struct _stat64 s = stat_req.statbuf;
			#else
            struct stat s = stat_req.statbuf;
			#endif

			unc_request->error_state = MA_OK;            
			unc_request->content_size = s.st_size ;
			
			uv_fs_req_cleanup(&stat_req) ;			
        }
		else
			MA_LOG(net_logger, MA_LOG_SEV_ERROR, "uv_fs_fstat failed");
		
    }   
    
    uv_fs_req_cleanup(req) ;

	if(MA_OK == unc_request->error_state)
		file_read(self, unc_request) ;		
	else
        file_close(self, unc_request);    
}

void file_read_cb(uv_fs_t* req) {
	ma_unc_url_request_t *unc_request = (ma_unc_url_request_t *)req->data ;
	ma_unc_request_handler_t *self = (ma_unc_request_handler_t*)(unc_request->base.url_handler);
    ma_error_t ma_net_ret = MA_OK;
	size_t written_bytes = 0;

	MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "file_read_cb %d", req->result);
	if(req->result != -1) {
		if(unc_request->base.progress_cb && unc_request->base.progress_cb->progress_callback) {
			if(unc_request->base.progress_cb->progress_callback((double)unc_request->content_size, (double)req->result, 0.0, 0.0, unc_request->base.userdata)){
				MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "unc file_read_cb user requested to stop download") ;    			
				unc_request->base.request_state = MA_URL_REQUEST_STATE_CANCELLED;
			}
		}

		ma_net_ret = ma_stream_write(unc_request->base.io_cbs->write_stream, unc_request->buffer, req->result, &written_bytes);
		unc_request->cur_position = unc_request->cur_position + req->result;		
		MA_LOG(net_logger, MA_LOG_SEV_TRACE, "Write file returned %d, total size = <%ld>, total downloaded size = <%ld>", ma_net_ret, (long)unc_request->content_size, (long)unc_request->cur_position);
		uv_fs_req_cleanup(req);

        if(unc_request->content_size == unc_request->cur_position) {
			MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "Successfully downloaded file.");
			file_close(self, unc_request);
		}
		else if(unc_request->base.request_state == MA_URL_REQUEST_STATE_CANCELLED) {
			MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "UNC file download stopped.");
			unc_request->error_state = MA_ERROR_NETWORK_REQUEST_ABORTED;
			file_close(self, unc_request);
		}
		else
			file_read(self, unc_request);
    }
    else {
		
		uv_fs_req_cleanup(req);

		if(unc_request->content_size != unc_request->cur_position) unc_request->error_state = MA_ERROR_NETWORK_REQUEST_FAILED;
		file_close(self, unc_request);
	}
}

static void file_read(ma_unc_request_handler_t *self, ma_unc_url_request_t *unc_request) {
	MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "reading file");
	if(!uv_fs_read(ma_event_loop_get_uv_loop(self->ma_loop), &unc_request->fs_read_req, unc_request->file, unc_request->buffer, sizeof(unc_request->buffer), unc_request->cur_position, file_read_cb))
		unc_request->error_state = MA_OK;
	else
		unc_request->error_state = MA_ERROR_NETWORK_REQUEST_FAILED ;	  

	if(MA_OK != unc_request->error_state) file_close(self, unc_request);    
}

static void file_close(ma_unc_request_handler_t *self, ma_unc_url_request_t *unc_request) {
	if(unc_request->file){
		uv_fs_t fs_close_req;

		MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "Closing file.");
		uv_fs_close(ma_event_loop_get_uv_loop(self->ma_loop), &fs_close_req, unc_request->file, NULL) ;
		uv_fs_req_cleanup(&fs_close_req);
	}
	log_out(unc_request);
	unc_request->base.request_state = MA_URL_REQUEST_STATE_COMPLETED;

	MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "file download final callback.");
	unc_request->base.final_cb( (MA_OK == unc_request->error_state ? MA_ERROR_NETWORK_REQUEST_SUCCEEDED: unc_request->error_state), 200, unc_request->base.userdata, (ma_url_request_t*)unc_request);
}

ma_error_t unc_downloadfile_async(ma_unc_request_handler_t *self,ma_unc_url_request_t *unc_url_request)
{
	unc_url_request->error_state = MA_OK;
	MA_LOG(net_logger, MA_LOG_SEV_INFO, "Download from UNC repo asynchronously %s",unc_url_request->ma_unc_url_info->url_info.formatted_url);
	if(-1 != uv_fs_open(ma_event_loop_get_uv_loop(self->ma_loop), &unc_url_request->fs_open_req, unc_url_request->ma_unc_url_info->url_info.formatted_url , O_RDONLY, 0, file_open_cb))
		unc_url_request->error_state = MA_OK;
	else
        unc_url_request->error_state = MA_ERROR_NETWORK_REQUEST_FAILED;

	if(MA_OK != unc_url_request->error_state) file_close(self, unc_url_request);    
	return unc_url_request->error_state;
}

#endif  // ifdef (MA_NETWORK_USE_UNC) || defined(MA_NETWORK_USE_ALL)
