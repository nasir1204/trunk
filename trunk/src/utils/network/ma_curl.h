#ifndef MA_CURL_H_INCLUDED
#define MA_CURL_H_INCLUDED

#include "curl/curl.h"
#include "ma/internal/utils/network/ma_net_common.h"
#include "ma_url_utils.h"
#include "ma/internal/utils/network/ma_url_request_base.h"
#include "ma/ma_log.h"



/* default connect timeout 3 minutes */
#define MA_CURL_DEFAULT_CONNECT_TIMEOUT             3 * 60
/* default transfer timeout  10 minutes */
#define MA_CURL_DEFAULT_TRANSFER_TIMEOUT            10 * 60 
/* default operation timeout 5 minutes */
#define MA_CURL_FTP_RESPONSE_TIMEOUT                5 * 60

/*define max rquest to be pipelined on the same connection */
#define MA_CURL_MAX_PIPELINE_LENGTH                 256

/* define max parallel connection to the single server */
#define MA_CURL_MAX_HOST_CONNECTIONS_TO_SERVER       10

/* define max connextion curl should cache,default is 10 in curl */
#define MA_CURL_MAX_CONNECTS_CACHE                   10


MA_CPP(extern "C" {)

typedef enum ma_curl_request_type_e {
    MA_CURL_REQUEST_TYPE_POST = 0,    
    MA_CURL_REQUEST_TYPE_GET,
    MA_CURL_REQUEST_TYPE_RAW_CONNECT
}ma_curl_request_type_t;

typedef enum ma_curl_transfer_protocol_e {
    MA_CURL_TRANSER_PROTOCOL_HTTP = 0,
    MA_CURL_TRANSER_PROTOCOL_FTP,    
	MA_CURL_TRANSER_PROTOCOL_FILE
}ma_curl_transfer_protocol_t;

typedef enum ma_curl_ipresolve_e {
	MA_CURL_IP_RESOLVE_NONE = -1,
	MA_CURL_IP_RESOLVE_ANY = CURL_IPRESOLVE_WHATEVER ,
	MA_CURL_IP_RESOLVE_IPV4 = CURL_IPRESOLVE_V4 ,
	MA_CURL_IP_RESOLVE_IPV6 = CURL_IPRESOLVE_V6
} ma_curl_ipresolve_t ;

typedef void (*ma_curl_debugtrace_callback)(CURL *handle, curl_infotype type,char *data, size_t size,void *userdata);
typedef int (*ma_curl_progress_callback)(void *userdata,double dltotal, double dlnow, double ultotal, double ulnow);
typedef size_t (*ma_curl_write_callback)(void *ptr, size_t size, size_t nmemb, void *userdata) ;
typedef size_t (*ma_curl_read_callback)(void *ptr, size_t size, size_t nmemb, void *userdata) ;    
typedef size_t (*ma_curl_header_callback)(void *ptr, size_t size, size_t nmemb, void *userdata) ;    

typedef struct ma_curl_s{	
	CURL *curl_handle;
	struct curl_slist *slist;
	ma_curl_request_type_t req_type;
	ma_curl_transfer_protocol_t proto_type;
	ma_bool_t is_ssl_connection;
	CURLcode last_curl_error;
}ma_curl_t;

//create a raw handle.
ma_error_t ma_curl_create(ma_curl_t **handle);
void ma_curl_release(ma_curl_t *handle);

//set the info.
ma_error_t ma_curl_init(ma_curl_t *handle);

void ma_curl_set_url(ma_curl_t *handle, ma_curl_request_type_t, ma_curl_transfer_protocol_t, unsigned long connect_timeout, unsigned long connection_timeout, unsigned long use_fresh_conneciton, ma_url_info_t *url_info) ;

void ma_curl_slist_release(ma_curl_t *handle);

void ma_curl_set_slist_append(ma_curl_t *handle,const char *header);

void ma_curl_set_header(ma_curl_t *handle) ;

void ma_curl_set_custom_header(ma_curl_t *handle, ma_url_custom_header_t *custom_header);

void ma_curl_set_post_fields(ma_curl_t *handle, const char *post_filds);

void ma_curl_set_proxy_info(ma_curl_t *handle , ma_proxy_t *proxy, ma_logger_t *logger);

void ma_curl_set_url_auth_info(ma_curl_t *handle , ma_url_request_auth_t *auth_info);

void ma_curl_set_url_cert_path(ma_curl_t *handle , ma_url_request_ssl_info_t *ssl_info);

void ma_curl_set_resolve_info(ma_curl_t *handle , ma_url_request_resolve_t *resolve_info);

void ma_curl_set_url_read_cb(ma_curl_t *handle ,ma_curl_read_callback, void *userdata);

void ma_curl_set_url_write_cb(ma_curl_t *handle ,ma_curl_write_callback, void *userdata);

void ma_curl_set_url_header_cb(ma_curl_t *handle ,ma_curl_header_callback, void *userdata);

void ma_curl_set_url_progress_cb(ma_curl_t *handle , ma_curl_progress_callback progress_cb,void *userdata);

void ma_curl_set_curltrace_cb(ma_curl_t *handle , ma_curl_debugtrace_callback trace_cb,void *userdata);

//if end_position = 0, then curl will download till EOF.
void ma_curl_set_range(ma_curl_t *handle,unsigned long start_position,unsigned long end_position);

void ma_curl_set_private_data(ma_curl_t *handle,void *userdata);

void ma_curl_set_ipresolve(ma_curl_t *handle, ma_curl_ipresolve_t resolve_ip) ;

//get the  CURLcode  error message.
void ma_curl_get_error_message(CURLcode error_code,const char **messgae);
void ma_curl_get_private_data(CURL *easyhandle,void **userdata);
void ma_curl_get_http_response_code(ma_curl_t *easyhandle, long *response_code);
void ma_curl_get_http_connect_code(ma_curl_t *self, long *response_code);
void ma_curl_get_http_response_message(long response_code,const char **messgae);


MA_CPP(})

#endif
