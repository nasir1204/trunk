#if defined (MA_NETWORK_USE_CURL) || defined(MA_NETWORK_USE_ALL)
#include "ma_curl_request.h"
#include "ma_curl_request_handler.h"
#include "ma/internal/utils/network/ma_url_request_handler.h"
#include "ma_url_utils.h"

#include <stdlib.h>
#include <string.h>

static ma_error_t ma_curl_request_release(ma_url_request_t *url_request);
static ma_error_t ma_curl_request_set_url_auth_info(ma_url_request_t *url_request, ma_url_request_auth_t *authenticate) ;
static ma_error_t ma_curl_request_set_ssl_info(ma_url_request_t *url_request, ma_url_request_ssl_info_t *ssl_info);
static ma_error_t ma_curl_request_set_range_info(ma_url_request_t *url_request, ma_url_request_range_t *range_info);
static ma_error_t ma_curl_request_set_proxy_info(ma_url_request_t *url_request, ma_proxy_list_t *proxy_list) ;
static ma_error_t ma_curl_request_set_resolve_info(ma_url_request_t *url_request, ma_url_request_resolve_t *resolve_info);

static const struct ma_url_request_methods_s curl_request_methods = {
    &ma_curl_request_release,
    &ma_curl_request_set_url_auth_info,
    &ma_curl_request_set_ssl_info,
    &ma_curl_request_set_range_info,
    &ma_curl_request_set_proxy_info,
    &ma_curl_request_set_resolve_info
};

ma_error_t ma_curl_url_request_create(const char *url, ma_curl_url_request_t **curl_url_request) {
    ma_error_t rc = MA_OK ;  
    ma_curl_url_request_t *self = (ma_curl_url_request_t *)calloc(1, sizeof(ma_curl_url_request_t)) ;
    if(!self) {
        MA_NET_LOG_MEMERR() ;
        return MA_ERROR_OUTOFMEMORY ;
    }
    if(MA_OK != (rc = ma_curl_create(&self->curl_easy))) {
        free(self) ;
        return rc ;
    }
    self->auth = NULL ;
    self->proxy_list = NULL ;
	self->proxy_list_index =self->proxy_list_size = 0;
    self->range_info = NULL ;
    self->ssl_info = NULL ;
    self->ip_resolve = MA_CURL_IP_RESOLVE_ANY ;

    ma_url_request_init((ma_url_request_t *)self, url, &curl_request_methods, 1);
    *curl_url_request = self ;
    if(MA_OK != (rc = ma_url_utils_get_protocol_type(url, &self->base.protocol))) {
        ma_curl_request_release((ma_url_request_t *)curl_url_request) ;
        return rc ;
    }
    return rc ;
}


ma_error_t ma_curl_request_release(ma_url_request_t *url_request) {
    ma_curl_url_request_t *self = (ma_curl_url_request_t *)url_request ;
    if(self->base.url)  free(self->base.url) ;
	if(self->base.post_fields)  free(self->base.post_fields) ;
	if(self->base.custom_header.key) free(self->base.custom_header.key);
	if(self->base.custom_header.value) free(self->base.custom_header.value);
    if(self->curl_easy) {
        ma_curl_release(self->curl_easy) ; 
        self->curl_easy = NULL ;
    }
    free(self) ;
    return MA_OK ;
}

ma_error_t ma_curl_request_set_url_auth_info(ma_url_request_t *url_request, ma_url_request_auth_t *authenticate) {
    ma_curl_url_request_t *self = (ma_curl_url_request_t *)url_request ;
    if(authenticate)
        self->auth = authenticate ;
    return MA_OK ;
}

ma_error_t ma_curl_request_set_ssl_info(ma_url_request_t *url_request, ma_url_request_ssl_info_t *ssl_info) {
    ma_curl_url_request_t *self = (ma_curl_url_request_t *)url_request ;
    if(ssl_info)
        self->ssl_info = ssl_info ;
    return MA_OK ;
}

ma_error_t ma_curl_request_set_range_info(ma_url_request_t *url_request, ma_url_request_range_t *range_info) {
    ma_curl_url_request_t *self = (ma_curl_url_request_t *)url_request ;
    if(range_info)
        self->range_info = range_info ;
    return MA_OK ;
}

ma_error_t ma_curl_request_set_proxy_info(ma_url_request_t *url_request, ma_proxy_list_t *proxy_list) {
    ma_curl_url_request_t *self = (ma_curl_url_request_t *)url_request ;
    self->proxy_list = proxy_list;
	if(self->proxy_list){
		self->proxy_list_index = 0;
		(void)ma_proxy_list_size(self->proxy_list, &self->proxy_list_size);		
		(void)ma_proxy_list_get_last_proxy_index(self->proxy_list, (ma_int32_t *)&self->proxy_list_index);
		if(!self->proxy_list_size)
			self->proxy_list = NULL;		
	}
    return MA_OK ;
}

ma_error_t ma_curl_request_set_resolve_info(ma_url_request_t *url_request, ma_url_request_resolve_t *resolve_info) {
    ma_curl_url_request_t *self = (ma_curl_url_request_t *)url_request ;
    if(resolve_info)
        self->resolve = resolve_info ;
    return MA_OK ;
}


ma_error_t ma_url_request_get_connection_info(ma_url_request_t *url_request, ma_url_request_connection_info_t **connection){
	if(url_request && connection && url_request->protocol != MA_URL_TRANSER_PROTOCOL_UNC){
		ma_curl_url_request_t *self = (ma_curl_url_request_t*)url_request;
		ma_url_info_t *info = NULL;
		ma_error_t rc = MA_OK;
		if(MA_OK == (rc = ma_url_utils_url_info_create(self->base.url, MA_FALSE, &info))){			
			ma_url_request_connection_info_t *tmp = (ma_url_request_connection_info_t*) calloc(1, sizeof(ma_url_request_connection_info_t));
			rc = MA_ERROR_OUTOFMEMORY;
			if(tmp){
				rc = MA_OK;
				tmp->port	  = atoi(info->port ? info->port: "0");
				tmp->address  = strdup(info->host);
				tmp->is_secure_port = info->is_secure_port;
				
				if(self->proxy_list){
					ma_proxy_list_t *new_list = 0;	
					/*Set last used proxy to use later on.*/
					if(MA_OK == ma_proxy_list_create(&new_list)){
						ma_int32_t last_used_relay_index = 0;
						ma_proxy_t *last_used_proxy = NULL;

						(void)ma_proxy_list_get_last_proxy_index(self->proxy_list, &last_used_relay_index);
						if(MA_OK == ma_proxy_list_get_proxy(self->proxy_list, last_used_relay_index, &last_used_proxy)){
							(void)ma_proxy_list_add_proxy(new_list, last_used_proxy);
							(void)ma_proxy_release(last_used_proxy);
						}
						else{
							(void)ma_proxy_list_release(new_list);
							new_list = NULL;
						}
					}
					tmp->proxy_list = new_list;
				}
                if(*connection) ma_url_request_release_connection_info(*connection) ;
				*connection = tmp;
			}
			(void)ma_url_utils_url_info_release(info);
		}
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}


ma_bool_t is_curl_perform_success(ma_curl_url_request_t *self, CURLcode ret, long response_code){

	if(MA_URL_TRANSER_PROTOCOL_HTTP == self->base.protocol && MA_URL_REQUEST_TYPE_POST == self->base.request_type ){
		return (CURLE_OK == ret && (response_code == 200 || response_code == 202));
	}
	if(MA_URL_TRANSER_PROTOCOL_HTTP == self->base.protocol){
		return (CURLE_OK == ret && (response_code == 200));
	}
	if(MA_URL_TRANSER_PROTOCOL_FTP == self->base.protocol){
		if(550 == response_code || response_code > 400) return MA_FALSE;
		return (CURLE_OK == ret);
	}
	else{
		return (CURLE_OK == ret);
	}
}

ma_error_t set_proxy(ma_curl_url_request_t *self, ma_bool_t use_next){
	while(1) {
		if(self->proxy_list_index < self->proxy_list_size){						
			ma_proxy_t *proxy = NULL;
			
			if(use_next) /*use next proxy*/
				++self->proxy_list_index;

			if((MA_OK == ma_proxy_list_get_proxy(self->proxy_list, (ma_int32_t)self->proxy_list_index, &proxy)) && proxy){
				ma_proxy_protocol_type_t type;
				(void)ma_proxy_get_protocol_type(proxy, &type);
				if(MA_URL_TRANSER_PROTOCOL_HTTP == self->base.protocol && (MA_PROXY_TYPE_HTTP == type || MA_PROXY_TYPE_BOTH == type)){
					MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "URL(%s) request, Trying for next proxy", self->base.url) ;
					ma_curl_set_proxy_info(self->curl_easy, proxy, net_logger) ;			
					if(self->base.io_cbs->read_stream) ma_stream_seek(self->base.io_cbs->read_stream, 0);
					if(self->base.io_cbs->write_stream) ma_stream_seek(self->base.io_cbs->write_stream, 0);
					(void)ma_proxy_release(proxy); proxy = NULL;
					return MA_OK;
				}
				if(MA_URL_TRANSER_PROTOCOL_FTP == self->base.protocol && (MA_PROXY_TYPE_FTP == type || MA_PROXY_TYPE_BOTH == type)){
					MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "URL(%s) request, Trying for next proxy", self->base.url) ;
					ma_curl_set_proxy_info(self->curl_easy, proxy, net_logger) ;				
					if(self->base.io_cbs->read_stream) ma_stream_seek(self->base.io_cbs->read_stream, 0);
					if(self->base.io_cbs->write_stream) ma_stream_seek(self->base.io_cbs->write_stream, 0);
					(void)ma_proxy_release(proxy); proxy = NULL;
					return MA_OK;
				}
				(void)ma_proxy_release(proxy); proxy = NULL;
			}	

			if(!use_next) /*use next if failure.*/
				++self->proxy_list_index;			
		}		
		else
			return MA_ERROR_NO_MORE_ITEMS;
	}	
	return MA_ERROR_NO_MORE_ITEMS;
}


ma_bool_t should_go_to_next_proxy(ma_curl_url_request_t *self, CURLcode ret, long response_code, long http_connect_code){
	if(MA_URL_TRANSER_PROTOCOL_HTTP == self->base.protocol){
		if(404 == response_code) return MA_FALSE;
		if(self->proxy_list && (CURLE_COULDNT_RESOLVE_PROXY == ret || CURLE_COULDNT_CONNECT == ret))
			return MA_TRUE;
		if(self->proxy_list && 403 == http_connect_code) 
			return MA_TRUE;
	}

	if(MA_URL_TRANSER_PROTOCOL_FTP == self->base.protocol){
		if(550 == response_code) return MA_FALSE;
		if(self->proxy_list && (CURLE_COULDNT_RESOLVE_PROXY == ret || CURLE_COULDNT_CONNECT == ret))
			return MA_TRUE;
	}
	return MA_FALSE;
}


ma_error_t ma_url_request_release_connection_info(ma_url_request_connection_info_t *connection){
	if(connection){
		if(connection->address) free(connection->address);
		if(connection->proxy_list) (void)ma_proxy_list_release(connection->proxy_list);
		free(connection); connection = NULL;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_url_request_get_source_ip_info(ma_url_request_t *url_request, char **source_ip){
	if(url_request && source_ip){
		ma_curl_url_request_t *self = (ma_curl_url_request_t*)url_request;
		if(self->curl_easy && self->curl_easy->curl_handle){
			CURLcode rc = CURLE_OK ;
            char *ip = NULL ;

			if(CURLE_OK == (rc = curl_easy_getinfo(self->curl_easy->curl_handle, CURLINFO_LOCAL_IP, &ip)) && ip && strlen(ip) > 0) {
                *source_ip = strdup(ip) ;
				return MA_OK;
            }
			else{
				MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "curl_easy_getinfo for CURLINFO_LOCAL_IP failed with error <%d>", rc) ;
				return MA_ERROR_APIFAILED;
			}
		}
	}
	return MA_ERROR_INVALIDARG;
}

#endif  //// ifdef (MA_NETWORK_USE_CURL) || defined(MA_NETWORK_USE_ALL)

