#ifndef MA_LOCAL_REQUEST_H_INCLUDED
#define MA_LOCAL_REQUEST_H_INCLUDED

#include "ma/ma_common.h"
#include "ma_url_utils.h"
#include "ma/internal/utils/network/ma_url_request_base.h"

#include "uv.h"

MA_CPP(extern "C" {)

typedef struct ma_local_url_request_s {
	ma_url_request_t					base;

	ma_url_request_auth_t				*auth;
	
	uv_fs_t                             fs_open_req ;

    uv_fs_t                             fs_read_req ;

    uv_file                             file ;

	ma_int64_t							cur_position;

	ma_int64_t							content_size ;

	unsigned char						buffer[1024] ;

	ma_error_t							error_state;

	ma_bool_t							is_cancelled;

	char								*formatted_url;

	#ifdef MA_WINDOWS
	HANDLE	token;
	#endif

}ma_local_url_request_t ;

ma_error_t ma_local_url_request_create(const char *url, ma_local_url_request_t **url_request) ;

MA_CPP(})

#endif  //MA_LOCAL_REQUEST_H_INCLUDED

