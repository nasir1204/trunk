#ifndef MA_URL_UTILS_H_INCLUDED
#define MA_URL_UTILS_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/network/ma_net_common.h"

#define IP_ADDRESS_LEN_MAX  64
#define LOG_FACILITY_NAME "network"

MA_CPP(extern "C" {)
typedef struct ma_url_info_s ma_url_info_t, *ma_url_info_h ;

typedef struct ma_ip_address_s {
	char *address;
	ma_bool_t is_curl_addr;
	size_t if_scopeid ;
}ma_ip_address_t;

struct ma_url_info_s {
    ma_network_transfer_protocol_t protocol ;	
    char *url;	
	char *formatted_url;	
	char *porotol_str;
	char *host ;	
	char *port;
	char *uri;    
	ma_ip_address_t *host_ip_addr;
    ma_bool_t is_ipv6_ddr ;
	ma_bool_t is_secure_port;
};

typedef struct ma_unc_url_info_s{
	ma_url_info_t url_info;
	char *share;
	ma_bool_t useWNET;
}ma_unc_url_info_t;

ma_bool_t ma_url_utils_is_secure_http(const char *url );

ma_error_t ma_url_utils_get_protocol_type(const char *url, ma_network_transfer_protocol_t *transfer_protocol) ;

ma_error_t ma_url_utils_ip_address_create(const char *ipaddress , ma_bool_t is_curl_addr,ma_ip_address_t**);

ma_error_t ma_url_utils_ip_address_release(ma_ip_address_t *self);

ma_error_t ma_url_utils_url_info_create(const char *url,ma_bool_t is_curl_addr,ma_url_info_t** ) ;

ma_error_t ma_url_utils_get_formatted_url(ma_url_info_t*,const char** ) ;

ma_error_t ma_url_utils_set_formatted_url(ma_url_info_t *self) ;

ma_error_t ma_url_utils_url_info_release(ma_url_info_t*) ;

ma_error_t ma_url_utils_unc_url_info_create(const char *url,ma_unc_url_info_t** ) ;

ma_error_t ma_url_utils_unc_convert_v6address_to_literalformat(char *v6address,char *literalformat,size_t size);

ma_bool_t ma_url_utils_is_ip(ma_url_info_t *self) ;

ma_error_t ma_url_utils_unc_url_info_release(ma_unc_url_info_t*) ;



MA_CPP(})
#endif  //MA_URL_UTILS_H_INCLUDED

