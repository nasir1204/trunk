#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdio.h>
//#include <strsafe.h>

#include "ma_url_utils.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/text/ma_utf8.h"
#include "ma/internal/utils/network/ma_net_common.h"


static char *zero = "";


static int ma_stricmp(const char *str1, const char *str2, size_t count){
	size_t i=0;
	while( i < count && ( tolower(str1[i]) == tolower(str2[i]) ) )
		i++;
	return ( ( count == i ) ? 0 : -1 );
}

ma_bool_t ma_url_utils_is_secure_http(const char *url ) {

	if(url && (0 == ma_stricmp(url,"https:", strlen("https:")))) 
		return MA_TRUE;

	return MA_FALSE;
}


ma_error_t ma_url_utils_get_protocol_type(const char *url, ma_network_transfer_protocol_t *transfer_protocol) {
	if(url && transfer_protocol){
		if( 0 == ma_stricmp(url,"http:", strlen("http:")) )
			*transfer_protocol =  MA_URL_TRANSER_PROTOCOL_HTTP ;
		else if( 0 == ma_stricmp(url,"https:", strlen("https:")) )
		   *transfer_protocol = MA_URL_TRANSER_PROTOCOL_HTTP ;
		else if( 0 == ma_stricmp(url,"ftp:", strlen("ftp:")) )
			*transfer_protocol = MA_URL_TRANSER_PROTOCOL_FTP ;
		else if( 0 == ma_stricmp(url,"unc:", strlen("unc:")) )
			*transfer_protocol = MA_URL_TRANSER_PROTOCOL_UNC ;
		else if( 0 == ma_stricmp(url,"file:", strlen("file:")) )
		   *transfer_protocol = MA_URL_TRANSER_PROTOCOL_FILE ;
		else
			return MA_ERROR_NETWORK_INVALID_URL ;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;

}

enum ma_url_parse_state_e {
	MA_URL_PARSE_START=0,
	MA_URL_PARSE_PROTOCOL_PARSE_IN,
	MA_URL_PARSE_PROTOCOL_PARSE_OUT,
	MA_URL_PARSE_HOSTNAME_PARSE_IN,
	MA_URL_PARSE_HOSTNAME_PARSE_OUT,
	MA_URL_PARSE_PORT_PARSE_IN,
	MA_URL_PARSE_PORT_PARSE_OUT,
	MA_URL_PARSE_URI_PARSE_IN,
	MA_URL_PARSE_URI_PARSE_OUT,
	MA_URL_PARSE_END
};

static void ma_url_utils_parse_url(ma_url_info_t *url_info){
	char *pos=NULL;
	int colon=0;
	enum ma_url_parse_state_e state=MA_URL_PARSE_START;
	char * temp_hostend = NULL;
	pos=url_info->url;
	while( MA_URL_PARSE_END != state ) {
		switch(state){
			case MA_URL_PARSE_START:
				url_info->porotol_str=pos;
				state=MA_URL_PARSE_PROTOCOL_PARSE_IN;
				pos++;
				break;
			case MA_URL_PARSE_PROTOCOL_PARSE_IN:
				if(':' == *pos ) {
					*pos='\0';
					state=MA_URL_PARSE_PROTOCOL_PARSE_OUT;
					pos++;
				}
				else {
					pos++;
				}
				break;
			case MA_URL_PARSE_PROTOCOL_PARSE_OUT:
				if('/' == *pos){
					*pos='\0';
					 pos++;
				}
				else {
					url_info->host=pos;
					state=MA_URL_PARSE_HOSTNAME_PARSE_IN;
					pos++;
				}
				break;
			case MA_URL_PARSE_HOSTNAME_PARSE_IN:
				if( '/' == *pos ) {
					//*pos='\0';
					 state=MA_URL_PARSE_HOSTNAME_PARSE_OUT;
					 //pos++;
				}
				else if('[' == *pos) {
					url_info->is_ipv6_ddr = MA_TRUE;
					pos++;
				}
				else if ( ':' == *pos ) {
					colon++;
					if( url_info->is_ipv6_ddr ) {
				 		if ( 8 == colon || ']' == *(pos-1)){ // when we reached to 8th colon then we have started to port.
							*pos='\0';
							url_info->port=(pos+1);
							state=MA_URL_PARSE_PORT_PARSE_IN;
							pos++;
						}
						else{
							pos++;
						}
					}
					else {
						temp_hostend = pos;
						url_info->port=(pos+1);
						state=MA_URL_PARSE_PORT_PARSE_IN;
						pos++;
					}
				}
				else {
					pos++;
				}
				break;
			case MA_URL_PARSE_HOSTNAME_PARSE_OUT:
				if( '/' == *pos ) {
					if(url_info->is_ipv6_ddr) {
						char *port_start = strrchr(url_info->host, ':');
						url_info->port = port_start + 1;
						*port_start = '\0';
						if(*(pos+1) == '/')
							url_info->uri = pos + 2;
						else
							url_info->uri = pos + 1;
						state = MA_URL_PARSE_URI_PARSE_IN;
					}
					*pos='\0';
					 pos++;
				}
				else {
					url_info->uri=pos;
					state=MA_URL_PARSE_URI_PARSE_IN;
					pos++;
				}
				break;
			case MA_URL_PARSE_PORT_PARSE_IN:
				if( '/' == *pos) {
					//*pos='\0';
					 state=MA_URL_PARSE_PORT_PARSE_OUT;
					 //pos++;
				}
				else if ( ':' == *pos ) { //at port parsing if any colon comes up then it is ipv6 address, so move parsing to hostname.
					state=MA_URL_PARSE_HOSTNAME_PARSE_IN;
					url_info->is_ipv6_ddr=MA_TRUE;
					url_info->port=NULL;
					colon++;
					pos++;
				}
				else {
					pos++;
				}
				break;
			case MA_URL_PARSE_PORT_PARSE_OUT:
				if( '/' == *pos ) {
					if(url_info->is_ipv6_ddr == MA_FALSE){
						if(temp_hostend)
							*temp_hostend = '\0';
					 }
					*pos='\0';
					 pos++;
				}
				else {
					url_info->uri=pos;
					if(url_info->port)
						*(url_info->port-1)='\0';
					state=MA_URL_PARSE_URI_PARSE_IN;
					pos++;
				}
				break;
			case MA_URL_PARSE_URI_PARSE_IN:
				state=MA_URL_PARSE_END;
				if(url_info->host==NULL)
					url_info->host = zero;
				if(url_info->porotol_str == NULL)
					url_info->porotol_str = zero;
				if(url_info->port == NULL)
					url_info->port = zero;
				if(url_info->uri == NULL)
					url_info->uri = zero;
				break;
			case MA_URL_PARSE_URI_PARSE_OUT:
				state=MA_URL_PARSE_END;
				break;
			case MA_URL_PARSE_END:
				break;
		}
	}
}


static size_t ma_url_utils_get_if_scopeid(const char *ipaddress){
	// Will add the code to get the scopeid of ipaddress.
	return 0;
}

//#define IP_ADDRESS_LEN_MAX  64
ma_error_t ma_url_utils_ip_address_create(const char *ipaddress , ma_bool_t is_curl_addr,ma_ip_address_t **ipaddr){
	if(ipaddress && ipaddr && ipaddress[0]!= '\0'){
		//not an ipv6 address or it is a hostname.
		ma_ip_address_t *tmp=(ma_ip_address_t*)calloc(1, sizeof(ma_ip_address_t) ) ;
		if(!tmp)
			return MA_ERROR_OUTOFMEMORY;

		if( NULL == strstr(ipaddress,":") ) {
			tmp->address=strdup(ipaddress);
			tmp->is_curl_addr=MA_TRUE;
		}
		else {
			// Handle ipv6 address formatting.
			size_t i=0;
			char fmt_v6_addr[IP_ADDRESS_LEN_MAX]={0};
			char *fmt_v6addr_ptr=fmt_v6_addr;
			char *ipv6_ptr=(char*)ipaddress;
			ma_bool_t scopeid_trim_required=MA_FALSE;
			memset(fmt_v6_addr,'\0',IP_ADDRESS_LEN_MAX);

			// Trim "[", if it is there, as we will add ourself, if formation is for curl.
			if( NULL != strstr(ipv6_ptr,"[") )
				ipv6_ptr= strstr(ipv6_ptr,"[") + 1;

			// Is scope id trim needed in address. For link and site local we trim the scopeid if it there in ipaddress.
			// As we ourself determine the scopeid.
			if(is_curl_addr) {
				scopeid_trim_required=MA_TRUE;
				fmt_v6addr_ptr++;		// if curl, we need space for [, will add after ip address formation.
			}
			
			while( i < (IP_ADDRESS_LEN_MAX - 3) ){
				if( ( '\0' == *ipv6_ptr ) || ( '%' == *ipv6_ptr && scopeid_trim_required ) || ( ']' == *ipv6_ptr ) )
					break;
				*fmt_v6addr_ptr=*ipv6_ptr;
				ipv6_ptr++; fmt_v6addr_ptr++; i++;
			}

			if( '%' == *ipv6_ptr ) {
				char scope_id[21] = {0} ;
				char *p = scope_id ; i = 0 ;
				while( i < strlen(++ipv6_ptr)) {
					if(']' == *ipv6_ptr)	break ;
					*p++ = *ipv6_ptr ;
				}
				tmp->if_scopeid = atoi(scope_id) ;
			}
			else {	// Get scopeid.
				tmp->if_scopeid=ma_url_utils_get_if_scopeid( (is_curl_addr) ? (fmt_v6_addr + 1) : (fmt_v6_addr) );
			}

			if(is_curl_addr) {
				fmt_v6_addr[0]='[';     // Add at the starting of the address.
				*fmt_v6addr_ptr=']';    // Add at the ending of the address.
				fmt_v6addr_ptr++;
			}
			tmp->is_curl_addr=is_curl_addr;
			tmp->address=strdup(fmt_v6_addr);
		}
		*ipaddr=tmp;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_url_utils_ip_address_release(ma_ip_address_t *self){
	if(self){
		if(self->address) free(self->address);
		free(self);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_url_utils_url_info_create(const char *url,ma_bool_t is_curl_addr,ma_url_info_t **url_info){
	if(url && url_info){
		ma_url_info_t *ma_url_info = (ma_url_info_t *)calloc(1, sizeof(ma_url_info_t)) ;
		if(ma_url_info){
			ma_error_t err=MA_OK;
			ma_url_info->url = strdup(url) ;
			if ( MA_OK == (err = ma_url_utils_get_protocol_type(url, &ma_url_info->protocol) ) ){
				ma_url_utils_parse_url(ma_url_info);
				ma_url_info->is_secure_port = ma_url_utils_is_secure_http(url);
				if( MA_OK == (err=ma_url_utils_ip_address_create(ma_url_info->host,is_curl_addr,&ma_url_info->host_ip_addr)) )
                    err = ma_url_utils_set_formatted_url(ma_url_info) ;
			}
			if( MA_OK != err){
				(void)ma_url_utils_url_info_release(ma_url_info);
				return err;
			}
			*url_info=ma_url_info;
			return MA_OK;
		}
		return MA_ERROR_OUTOFMEMORY;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_url_utils_url_info_release(ma_url_info_t *self) {
    if(self) {
		if (self->url) free(self->url) ;
		if (self->formatted_url) free(self->formatted_url);
		(void)ma_url_utils_ip_address_release(self->host_ip_addr);
        free(self) ;
		return MA_OK;
    }
	return MA_ERROR_INVALIDARG;
}

static char *removeSpaces(char *url){
	char *new_url = (char*) calloc(strlen(url)*3+1, 1);
	if(new_url){
		char *src = url;
		char *dst = new_url;
		while(*src){			
			if(' ' == *src){ /*if space*/
				*dst++ = '%';
				*dst++ = '2';
				*dst++ = '0';
			}
			else{ 
				*dst++ = *src;
			}
			src++;
		}
		/*copy in old url*/
		free(url);

		/*move new in old url*/
		url = (char*)calloc(strlen(new_url)+1, 1);
		if(url)
			strcpy(url, new_url);

		/*free temp*/
		free(new_url);
	}
	return url;
}

#define MA_URL_FORMAT "%s://%s%s%s/%s"
ma_error_t ma_url_utils_get_formatted_url(ma_url_info_t *self,const char **formatted_url) {
	if(self && formatted_url){
		size_t len=0;
		len=strlen( self->porotol_str ) +
			strlen( self->host_ip_addr->address )  +
			( strcmp(self->port,"")!=0 ? strlen(self->port) : 0 ) +
			( strcmp(self->uri,"")!=0 ? strlen(self->uri) : 0 ) +
			strlen(MA_URL_FORMAT);

		if( NULL == self->formatted_url){
			self->formatted_url=(char*)calloc(len,1);
			if(self->formatted_url) {
				MA_MSC_SELECT(_snprintf,snprintf)(self->formatted_url,len,MA_URL_FORMAT,
					self->porotol_str,
					self->host_ip_addr->address,
					(strcmp(self->port,"")!=0 ? ":" : ""),
					(strcmp(self->port,"")!=0 ? self->port : ""),
					(strcmp(self->uri,"") !=0 ? self->uri : "") );
			}
			else
				return MA_ERROR_OUTOFMEMORY;
		}
		
		if(MA_URL_TRANSER_PROTOCOL_HTTP == self->protocol || MA_URL_TRANSER_PROTOCOL_FTP == self->protocol)
			self->formatted_url = removeSpaces(self->formatted_url);

		*formatted_url = self->formatted_url;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_url_utils_set_formatted_url(ma_url_info_t *self) {
    const char *formatted_url = NULL;
    return ma_url_utils_get_formatted_url(self, &formatted_url) ;
}

#if defined (MA_NETWORK_USE_UNC)
char g_share[MAX_PATH] = {0};
ma_error_t ma_url_utils_unc_parse(ma_unc_url_info_t **unc_url_info)
{
	char temp_url[MAX_PATH] = {0};
	char *purl = NULL;
	
	int i =0;
	purl = strstr((*unc_url_info)->url_info.url,"unc:");
	if(purl != NULL){
		purl = purl + strlen("unc:");

		if(strchr(purl,':') != NULL)
		{
			(void)ma_url_utils_unc_convert_v6address_to_literalformat(purl,temp_url,MAX_PATH);			
		}
		else{
			while(*purl != '\0')
			{
				if(*purl == '/')
					temp_url[i] = '\\';
				else
					temp_url[i] = *purl;
				purl++;i++;
			}
		}
		(*unc_url_info)->url_info.formatted_url = strdup(temp_url);

		if((purl = strstr(temp_url,"\\\\SiteStat.xml")) || (purl = strstr(temp_url,"\\SiteStat.xml")))
		{
			*purl = '\0';
			strcpy(g_share,temp_url);
			(*unc_url_info)->share = strdup(temp_url);
		}

		if(g_share[strlen(g_share)-1] == '\\')
			g_share[strlen(g_share)-1] = '\0';
	}
	return MA_OK;
}

ma_error_t ma_url_utils_unc_url_info_create(const char *url,ma_unc_url_info_t **url_info){
	if(url && url_info){
		ma_unc_url_info_t *ma_unc_url_info = (ma_unc_url_info_t *)calloc(1, sizeof(ma_unc_url_info_t)) ;
		if(ma_unc_url_info){
			ma_error_t err=MA_OK;
			ma_unc_url_info->url_info.url = strdup(url) ;
			err =ma_url_utils_unc_parse(&ma_unc_url_info);
			
		    if( MA_OK != err){
				(void)ma_url_utils_unc_url_info_release(ma_unc_url_info);
				return err;
			}
			*url_info=ma_unc_url_info;
			return MA_OK;
		}
		return MA_ERROR_OUTOFMEMORY;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_url_utils_unc_url_info_release(ma_unc_url_info_t *self) {
    if(self) {
		if (self->url_info.url) free(self->url_info.url) ;
		if (self->url_info.formatted_url) free(self->url_info.formatted_url);
		if (self->share) free(self->share);
		free(self) ;
		return MA_OK;
    }
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_url_utils_unc_convert_v6address_to_literalformat(char *v6address,char *literalformat,size_t size)
{
	char *pliteral = NULL,*pstart = NULL, *pend = NULL, *pitr = NULL, *prelativepath = NULL;
	char temp_path[MAX_PATH] = {0};
	size_t i =0;
	
	if(v6address == NULL)
		return MA_ERROR_INVALIDARG;
	pliteral = strstr(v6address,".ipv6-literal.net");
	if(pliteral != NULL){
		sprintf(literalformat,"%s",v6address);
		return MA_OK;
	}
	//  If IP Address enclosed in [] , we need to remove it
	//	Change all ":" to "-"
	//	Remove % and the zone id attached
	//	Attach ".ipv6-literal.net"
	//	eg: "[2001:db8:28:3:f98a:5b31:67b7:67ef%4]//share"  to "2001-db8-28-3-f98a-5b31-67b7-67ef.ipv6-literal.net//share"
	
	pstart = v6address;
	while(*pstart != '\0')
	{
		switch(*pstart)
		{
			case '[':
				pstart++;
				break;
			case ']':
				pstart++;
				break;
			case '%':
				strcat(temp_path,".ipv6-literal.net");
				i = i + strlen(".ipv6-literal.net");
				pstart++;
				if(isdigit(*pstart) && isdigit(*(pstart+1)))
					pstart = pstart+2;
				else if(isdigit(*pstart))
					pstart++;
				break;
			case ':':
				temp_path[i++] = '-';
				pstart++;
				break;
			case '\\':
				temp_path[i++] = '\\';
				pstart++;
				break;
			case '/':
				temp_path[i++] = '\\';
				pstart++;
				break;
			default:
			{
				temp_path[i++] = *pstart++; 
				break;
			}
		}
	}
	temp_path[i] = '\0';
	strcpy_s(literalformat,size,temp_path);
	return MA_OK;
}

#endif // (MA_NETWORK_USE_UNC)
ma_bool_t is_ipv4(const char* address)
{
    if(address) {
		const char *pos = address ;
		unsigned char ch = *pos ;
		unsigned short count = 0 ;

		while(ch != '\0')
		{
			if (!((ch >= '0' && ch <= '9') || ch == '.')) return MA_FALSE ;

			if (ch == '.')
				if (++count > 3) return MA_FALSE ;

			ch = *++pos;
		}

		if(count == 3 && *--pos != '.') 
			return MA_TRUE ;	 
	}
	return MA_FALSE ;
}

ma_bool_t ma_url_utils_is_ip(ma_url_info_t *self) {

	if(self->is_ipv6_ddr || is_ipv4(self->host_ip_addr->address))
		return MA_TRUE ;

	return MA_FALSE ;
}
