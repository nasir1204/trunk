#if defined (MA_NETWORK_USE_LOCAL) || defined(MA_NETWORK_USE_UNC)

#include "ma_local_request.h"
#include "ma_local_request_handler.h"
#include "ma/internal/utils/text/ma_utf8.h"
#include "ma/internal/utils/network/ma_url_request_handler.h"
#include "ma_url_utils.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static ma_error_t ma_local_request_release(ma_url_request_t *url_request);
static ma_error_t ma_local_request_set_url_auth_info(ma_url_request_t *url_request, ma_url_request_auth_t *authenticate) ;
static ma_error_t ma_local_request_set_ssl_info(ma_url_request_t *url_request, ma_url_request_ssl_info_t *ssl_info);
static ma_error_t ma_local_request_set_range_info(ma_url_request_t *url_request, ma_url_request_range_t *range_info);
static ma_error_t ma_local_request_set_proxy_info(ma_url_request_t *url_request, ma_proxy_list_t *proxy_list) ;
static ma_error_t ma_local_request_set_resolve_info(ma_url_request_t *url_request, ma_url_request_resolve_t *resolve_info);

static const struct ma_url_request_methods_s request_methods = {
    &ma_local_request_release,
    &ma_local_request_set_url_auth_info,
    &ma_local_request_set_ssl_info,
    &ma_local_request_set_range_info,
    &ma_local_request_set_proxy_info,
    &ma_local_request_set_resolve_info
};

static void format_file_url(ma_local_url_request_t *self){
	char *tmp = self->formatted_url;	
#ifdef MA_WINDOWS		
	for (; *tmp; ++tmp){
        if(*tmp == '/') *tmp = '\\';        
    }	  
#else
	for (; *tmp; ++tmp){
        if(*tmp == '\\') *tmp = '/';        
    }	  
#endif
}

ma_error_t ma_local_url_request_create(const char *url, ma_local_url_request_t **local_req) {
    
	if( strlen(url + strlen("file://")) > 0) {
		ma_error_t rc = MA_OK ;  

		ma_local_url_request_t *url_req = (ma_local_url_request_t *)calloc(1, sizeof(ma_local_url_request_t)) ;
		if(!url_req) {
			MA_NET_LOG_MEMERR() ;
			return MA_ERROR_OUTOFMEMORY ;
		}
   
		ma_url_request_init((ma_url_request_t *)url_req, url, &request_methods, 1);
		url_req->base.url = strdup(url);
		url_req->formatted_url = strdup(url+strlen("file://"));

		url_req->base.protocol = MA_URL_TRANSER_PROTOCOL_FILE;
		url_req->error_state = MA_OK;

		url_req->fs_open_req.data = url_req;
		url_req->fs_read_req.data = url_req;
		format_file_url(url_req);

		*local_req = url_req ;	
		return rc ;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_local_request_release(ma_url_request_t *url_request) {
    ma_local_url_request_t *self = (ma_local_url_request_t *)url_request ;
    if(self->base.url)  free(self->base.url) ;
    free(self) ;
    return MA_OK ;
}

ma_error_t ma_local_request_set_url_auth_info(ma_url_request_t *url_request, ma_url_request_auth_t *authenticate) {
	ma_local_url_request_t *self = (ma_local_url_request_t *)url_request ;
    if(authenticate)
        self->auth = authenticate ;
    return MA_OK ;
}

ma_error_t ma_local_request_set_ssl_info(ma_url_request_t *url_request, ma_url_request_ssl_info_t *ssl_info) {
   return MA_OK ;
}

ma_error_t ma_local_request_set_range_info(ma_url_request_t *url_request, ma_url_request_range_t *range_info) {
   return MA_OK ;
}

ma_error_t ma_local_request_set_proxy_info(ma_url_request_t *url_request, ma_proxy_list_t *proxy_list) {
   return MA_OK ;
}

ma_error_t ma_local_request_set_resolve_info(ma_url_request_t *url_request, ma_url_request_resolve_t *resolve_info) {
    return MA_OK ;
}

#endif  

