#if defined (MA_NETWORK_USE_CURL) || defined(MA_NETWORK_USE_ALL)

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include "ma_curl.h"


#define SAFE_DELETE(ptr)		if(ptr)	{ free(ptr);ptr=NULL; }


MA_CPP(extern "C" {)

static void ma_curl_initialize(ma_curl_t *handle,ma_curl_request_type_t request_type,ma_curl_transfer_protocol_t protocol,unsigned long connect_timeout,unsigned long connection_timeout, unsigned long use_fresh_conneciton);

ma_error_t ma_curl_create(ma_curl_t **handle){
	if(handle){
        ma_curl_t *self = NULL;
		if(self = (ma_curl_t*)calloc(1,sizeof(ma_curl_t))) {
			*handle = self;
			return MA_OK;
        }
        return MA_ERROR_OUTOFMEMORY;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_curl_init(ma_curl_t *self){
	if(self){		
		if( (NULL == self->curl_handle) && (NULL == (self->curl_handle = curl_easy_init())) ){
			self->last_curl_error=CURLE_FAILED_INIT;
			return MA_CURL_FAILURE;
		}
		else{
            curl_easy_reset(self->curl_handle);
			self->last_curl_error=CURLE_OK;
			return MA_OK;
		}
	}
	return MA_ERROR_INVALIDARG;
}

void ma_curl_set_url(ma_curl_t *self,ma_curl_request_type_t request_type,ma_curl_transfer_protocol_t protocol,unsigned long connect_timeout,unsigned long connection_timeout, unsigned long use_fresh_conneciton, ma_url_info_t *url_info){	
	ma_curl_initialize(self,request_type,protocol,connect_timeout,connection_timeout, use_fresh_conneciton);
	curl_easy_setopt(self->curl_handle, CURLOPT_URL, url_info->formatted_url) ;
	if(url_info->is_ipv6_ddr)
		curl_easy_setopt(self->curl_handle, CURLOPT_ADDRESS_SCOPE, url_info->host_ip_addr->if_scopeid) ;
	self->is_ssl_connection=0;
	self->proto_type=protocol;
	self->req_type=request_type;
	// TBD Add code to handle the scope id requied for IPV6 URL.	
}

void ma_curl_set_slist_append(ma_curl_t *self,const char *header){	
	self->slist = curl_slist_append(self->slist,header);	
}

void ma_curl_set_header(ma_curl_t *self) {	
	curl_easy_setopt(self->curl_handle, CURLOPT_HTTPHEADER, self->slist);
}

void ma_curl_set_post_fields(ma_curl_t *self, const char *post_filds) {	
	curl_easy_setopt(self->curl_handle, CURLOPT_POSTFIELDS, post_filds);
}


void ma_curl_set_custom_header(ma_curl_t *handle, ma_url_custom_header_t *custom_header) {
	if(handle && custom_header && custom_header->key && custom_header->value) {
		ma_temp_buffer_t header = MA_TEMP_BUFFER_INIT;
		/* key: value */
		ma_temp_buffer_reserve(&header, strlen(custom_header->key)  + 3 + strlen(custom_header->value));
		MA_MSC_SELECT(_snprintf, snprintf)((char *)ma_temp_buffer_get(&header), ma_temp_buffer_capacity(&header), "%s: %s",
			custom_header->key, custom_header->value);
		handle->slist = curl_slist_append(handle->slist, (char *)ma_temp_buffer_get(&header));
	}
}

void ma_curl_release(ma_curl_t *self){		
    if(self) {
	    if (self->curl_handle) curl_easy_cleanup(self->curl_handle);
	    if (self->slist) curl_slist_free_all(self->slist);
	    SAFE_DELETE(self);		
    }
}

void ma_curl_slist_release(ma_curl_t *self){		
	if(self->slist) curl_slist_free_all(self->slist);	
	self->slist = NULL;
}

struct proxy_s {
	const char *host;
	ma_int32_t port;
	ma_int32_t scopeid;
	ma_bool_t is_auth_req;
	const char *username;
	const char *password;
};

static ma_bool_t extact_proxy_info(ma_proxy_t *src, struct proxy_s *dst){
	dst->host = NULL;
	(void)ma_proxy_get_address(src, &dst->host);

	dst->port = 0;
	(void)ma_proxy_get_port(src, &dst->port);

	dst->scopeid = 0;
	(void)ma_proxy_get_scopeid(src, (ma_uint32_t *)&dst->scopeid);

	dst->is_auth_req = MA_FALSE;
	dst->username = NULL;
	dst->password = NULL;
	(void)ma_proxy_get_authentication(src, &dst->is_auth_req, &dst->username, &dst->password);

	if(dst->host != NULL && dst->port != 0)
		return MA_TRUE;

	return MA_FALSE;
};

void ma_curl_set_proxy_info(ma_curl_t *self , ma_proxy_t *proxy, ma_logger_t *logger){	
	
	struct proxy_s proxy_info;
	if(extact_proxy_info(proxy, &proxy_info)){
		MA_LOG(logger, MA_LOG_SEV_INFO, "Setting proxy info proxy:%s port:%d scope:%d", proxy_info.host, proxy_info.port, proxy_info.scopeid) ;
  
		curl_easy_setopt(self->curl_handle,CURLOPT_PROXY,proxy_info.host);
		curl_easy_setopt(self->curl_handle,CURLOPT_PROXYPORT,proxy_info.port);
		if(proxy_info.host == strstr(proxy_info.host,"[fe80:") )  {				
			curl_easy_setopt(self->curl_handle,CURLOPT_ADDRESS_SCOPE,proxy_info.scopeid);
		}
		if(MA_URL_REQUEST_TYPE_POST == self->req_type)
			curl_easy_setopt(self->curl_handle, CURLOPT_HTTPPROXYTUNNEL, 1L);

		if(proxy_info.username &&  proxy_info.password){
			//curl_easy_setopt(self->curl_handle,CURLOPT_PROXYAUTH,(CURLAUTH_ANY & ~(CURLAUTH_GSSNEGOTIATE)));
			curl_easy_setopt(self->curl_handle,CURLOPT_PROXYAUTH,CURLAUTH_NTLM | CURLAUTH_BASIC | CURLAUTH_DIGEST | CURLAUTH_DIGEST_IE);
			curl_easy_setopt( self->curl_handle, CURLOPT_PROXYUSERNAME, proxy_info.username);
			curl_easy_setopt( self->curl_handle, CURLOPT_PROXYPASSWORD, proxy_info.password );
		}	
	}
}

void ma_curl_set_resolve_info(ma_curl_t *self , ma_url_request_resolve_t *resolve_info) {
    if(resolve_info->host_name && resolve_info->ip_address) {
        char resolve_str[MA_MAX_LEN] = {0};
        MA_MSC_SELECT(_snprintf, snprintf)(resolve_str, MA_MAX_LEN-1, "%s:%d:%s", resolve_info->host_name, resolve_info->port, resolve_info->ip_address);
        ma_curl_set_slist_append(self, resolve_str);         
        curl_easy_setopt(self->curl_handle,CURLOPT_RESOLVE, self->slist);
    }
}

void ma_curl_set_url_auth_info(ma_curl_t *self , ma_url_request_auth_t *auth_info){	
	 if( auth_info->user && auth_info->password ){
		curl_easy_setopt(self->curl_handle,CURLOPT_SSL_VERIFYPEER,0);
		curl_easy_setopt(self->curl_handle,CURLOPT_SSL_VERIFYHOST,0);
		curl_easy_setopt( self->curl_handle,CURLOPT_HTTPAUTH,(CURLAUTH_ANY & ~(CURLAUTH_GSSNEGOTIATE)));
		curl_easy_setopt( self->curl_handle, CURLOPT_USERNAME, auth_info->user);
		curl_easy_setopt( self->curl_handle, CURLOPT_PASSWORD, auth_info->password );
	 }	 
}

void ma_curl_set_url_cert_path(ma_curl_t *self , ma_url_request_ssl_info_t *ssl_info){
	if ( ssl_info->certificate_path ){
		curl_easy_setopt(self->curl_handle,CURLOPT_SSL_VERIFYPEER,1);
		curl_easy_setopt(self->curl_handle,CURLOPT_SSL_VERIFYHOST,0);
		curl_easy_setopt(self->curl_handle,CURLOPT_CAINFO,ssl_info->certificate_path );
		/* openssl is not supporitn TLS 1.2 in FIPs mode so continuing TLSv1 for fips and non fips openssl */
#ifdef MA_NETWORK_WITH_OPENSSL
        curl_easy_setopt(self->curl_handle,CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1);
#else
        curl_easy_setopt(self->curl_handle,CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
#endif
		self->is_ssl_connection=1;
	}	
}

void ma_curl_set_url_read_cb(ma_curl_t *self ,ma_curl_read_callback read_cb, void *userdata){
	curl_easy_setopt(self->curl_handle, CURLOPT_READFUNCTION, read_cb);
	curl_easy_setopt(self->curl_handle, CURLOPT_READDATA, userdata);	
}

void ma_curl_set_url_write_cb(ma_curl_t *self ,ma_curl_write_callback write_cb, void *userdata){
    curl_easy_setopt(self->curl_handle, CURLOPT_WRITEFUNCTION, write_cb);
	curl_easy_setopt(self->curl_handle, CURLOPT_WRITEDATA, userdata);	
}

void ma_curl_set_url_header_cb(ma_curl_t *self ,ma_curl_header_callback header_cb, void *userdata){
    curl_easy_setopt(self->curl_handle, CURLOPT_HEADERFUNCTION, header_cb);
	curl_easy_setopt(self->curl_handle, CURLOPT_HEADERDATA, userdata);	
}

void ma_curl_set_url_progress_cb(ma_curl_t *self , ma_curl_progress_callback progress_cb,void *userdata){
	curl_easy_setopt(self->curl_handle,CURLOPT_NOPROGRESS, 0);	
    curl_easy_setopt(self->curl_handle, CURLOPT_PROGRESSFUNCTION, progress_cb);
	curl_easy_setopt(self->curl_handle, CURLOPT_PROGRESSDATA, userdata);	
}

void ma_curl_set_curltrace_cb(ma_curl_t *self , ma_curl_debugtrace_callback trace_cb,void *userdata){
	curl_easy_setopt(self->curl_handle, CURLOPT_VERBOSE  , 1);
    curl_easy_setopt(self->curl_handle, CURLOPT_DEBUGFUNCTION, trace_cb);
	curl_easy_setopt(self->curl_handle, CURLOPT_DEBUGDATA, userdata);	
}

#define RANGE_HEADER_LEN 32
void ma_curl_set_range(ma_curl_t *self,unsigned long start_position,unsigned long end_position){
	char range_str[RANGE_HEADER_LEN]={0};
	if(end_position)
		MA_MSC_SELECT(_snprintf,snprintf)(range_str,RANGE_HEADER_LEN,"%lu-%lu",start_position,end_position);	
	else
		MA_MSC_SELECT(_snprintf,snprintf)(range_str,RANGE_HEADER_LEN,"%lu-",start_position);	
	curl_easy_setopt(self->curl_handle , CURLOPT_RANGE  , range_str );	    	
}

void ma_curl_set_private_data(ma_curl_t *self,void *userdata){
	curl_easy_setopt(self->curl_handle , CURLOPT_PRIVATE  , userdata );	    	
}

void ma_curl_set_ipresolve(ma_curl_t *self, ma_curl_ipresolve_t resolve_ip) {
	curl_easy_setopt(self->curl_handle , CURLOPT_IPRESOLVE, resolve_ip) ;
}

void ma_curl_get_private_data(CURL *easyhandle,void **userdata){
	curl_easy_getinfo(easyhandle , CURLINFO_PRIVATE  , userdata );	    	
}

void ma_curl_get_error_message(CURLcode error_code, const char **msg){	
	*msg=curl_easy_strerror(error_code);	
}

void ma_curl_get_http_response_code(ma_curl_t *self, long *response_code){	
	switch(self->req_type){
		case MA_CURL_REQUEST_TYPE_RAW_CONNECT:	
			curl_easy_getinfo(self->curl_handle,CURLINFO_HTTP_CONNECTCODE,response_code);
			break;
		default:
			curl_easy_getinfo(self->curl_handle,CURLINFO_RESPONSE_CODE,response_code);
	}	
}

void ma_curl_get_http_connect_code(ma_curl_t *self, long *response_code){	
	curl_easy_getinfo(self->curl_handle,CURLINFO_HTTP_CONNECTCODE,response_code);	
}

void ma_curl_get_http_response_message(long reponse_code,const char **msg){	
	//TBD, will see when we integarate with agent handler.
	
}


/* Due to Bug 988601, 
	if the connection is not tear down immediately, network driver doesnt cleans up properly 
	which is wierd by itself but may be bug in XP / 2k3 network stack as per MPT team.
	Tearing the connection down immediataely does solves this but not convinced that it solves the problem but brings MA out of equation
*/

void set_connection_to_tear_down_for_windows_less_than_6(ma_curl_t *self) {
#if defined(MA_WINDOWS)
	OSVERSIONINFOEX Version = {0};
    ZeroMemory (&Version, sizeof (OSVERSIONINFOEX));
    Version.dwOSVersionInfoSize = sizeof (OSVERSIONINFOEX);
    if (GetVersionEx ((OSVERSIONINFO *)&Version)) {
        /* Just use LOCAL SERVICE account on XP/2K3 */
        if(Version.dwMajorVersion <6) {
			curl_easy_setopt(self->curl_handle,CURLOPT_FORBID_REUSE, 1L);
        }
	}
#endif
}
    
void ma_curl_initialize(ma_curl_t *self,ma_curl_request_type_t request_type,ma_curl_transfer_protocol_t protocol,unsigned long connect_timeout,unsigned long connection_timeout, unsigned long use_fresh_conneciton){		
	curl_easy_setopt(self->curl_handle,CURLOPT_PROXY , "");		
	switch(request_type){
		case MA_CURL_REQUEST_TYPE_RAW_CONNECT:	
			curl_easy_setopt(self->curl_handle,CURLOPT_CONNECT_ONLY, (long)1);
			curl_easy_setopt(self->curl_handle,CURLOPT_NOBODY,1);	
			set_connection_to_tear_down_for_windows_less_than_6(self);
			return;
		case MA_CURL_REQUEST_TYPE_POST: //upload.			
			curl_easy_setopt(self->curl_handle, CURLOPT_POST, 1);
			curl_easy_setopt(self->curl_handle,CURLOPT_USERAGENT, MA_HTTP_POST_USER_AGENT_STR);		
			break;
		case MA_CURL_REQUEST_TYPE_GET:  //download
			if(MA_CURL_TRANSER_PROTOCOL_HTTP == protocol) {
				curl_easy_setopt(self->curl_handle,CURLOPT_FORBID_REUSE, use_fresh_conneciton ? 1 : 0);
				curl_easy_setopt(self->curl_handle,CURLOPT_FRESH_CONNECT, use_fresh_conneciton ? 1 : 0);
			}
			curl_easy_setopt(self->curl_handle,CURLOPT_USERAGENT, MA_HTTP_GET_USER_AGENT_STR);		
			
			break;
	}	
	
    curl_easy_setopt(self->curl_handle,CURLOPT_NOSIGNAL,(long)1);                      
    curl_easy_setopt(self->curl_handle,CURLOPT_CONNECTTIMEOUT,connect_timeout ? connect_timeout : MA_CURL_DEFAULT_CONNECT_TIMEOUT);
    
    curl_easy_setopt(self->curl_handle,CURLOPT_LOW_SPEED_LIMIT, 1L);
    curl_easy_setopt(self->curl_handle,CURLOPT_LOW_SPEED_TIME, 600L);
		
	if( MA_CURL_TRANSER_PROTOCOL_HTTP == protocol ){
		curl_easy_setopt(self->curl_handle,CURLOPT_FOLLOWLOCATION, 1);
		curl_easy_setopt(self->curl_handle,CURLOPT_REDIR_PROTOCOLS, CURLPROTO_HTTP);
		curl_easy_setopt(self->curl_handle,CURLOPT_MAXREDIRS, 5);
		curl_easy_setopt(self->curl_handle,CURLOPT_TIMEOUT, connection_timeout ? connection_timeout : MA_CURL_DEFAULT_TRANSFER_TIMEOUT);
	}
	
	set_connection_to_tear_down_for_windows_less_than_6(self);
	return;
}


MA_CPP(})

#endif  /* ifdef (MA_NETWORK_USE_CURL) || defined(MA_NETWORK_USE_ALL) */

