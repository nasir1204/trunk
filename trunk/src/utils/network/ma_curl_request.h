#ifndef MA_CURL_REQUEST_H_INCLUDED
#define MA_CURL_REQUEST_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/network/ma_url_request_base.h"
#include "ma_curl.h"

MA_CPP(extern "C" {)

typedef struct ma_curl_url_request_s {
	ma_url_request_t                            base;

    /* authentication -- user/passwd */
	ma_url_request_auth_t                       *auth;

    /* host name resolve info */
    ma_url_request_resolve_t                    *resolve;

    /* SSL info */
    ma_url_request_ssl_info_t                   *ssl_info;       

    /* external proxy list to be used */
    ma_proxy_list_t                             *proxy_list;
	
    size_t                                      proxy_list_size;

	size_t                                      proxy_list_index;    

    /* range -- start and end */
	ma_url_request_range_t                      *range_info;
	
    /*curl easy interface handle */
	ma_curl_t                                   *curl_easy;

    ma_curl_ipresolve_t	                        ip_resolve ;

    ma_bool_t                                   connect_notification_done;

}ma_curl_url_request_t ;

ma_error_t ma_curl_url_request_create(const char *url, ma_curl_url_request_t **curl_url_request);

ma_bool_t should_go_to_next_proxy(ma_curl_url_request_t *self, CURLcode ret, long response_code, long http_connect_code);

ma_error_t set_proxy(ma_curl_url_request_t *self, ma_bool_t use_next);

ma_bool_t is_curl_perform_success(ma_curl_url_request_t *curl_url_request, CURLcode ret, long response_code);


MA_CPP(})

#endif  //MA_CURL_REQUEST_H_INCLUDED

