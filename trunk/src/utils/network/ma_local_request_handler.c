#if defined (MA_NETWORK_USE_LOCAL) || defined(MA_NETWORK_USE_ALL)
#include "ma/internal/utils/text/ma_utf8.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"

#include "ma_local_request.h"
#include "ma_local_request_handler.h"
#include "ma_url_utils.h"
#include "ma/internal/utils/network/ma_url_request_base.h"
#include "ma/internal/ma_macros.h"


#ifdef MA_WINDOWS
#include "ma_win_connection.h"
#endif

#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <math.h>
#include <stdio.h>

static ma_error_t local_downloadfile(ma_local_request_handler_t *self, ma_local_url_request_t *local_r);
static ma_error_t local_downloadfile_sync(ma_local_request_handler_t *self, ma_local_url_request_t *local_r);

static ma_error_t ma_local_request_handler_start(ma_url_request_handler_t *self) ;
static ma_error_t ma_local_request_handler_stop(ma_url_request_handler_t *self) ;
static ma_error_t ma_local_request_handler_is_running(ma_url_request_handler_t *self,ma_bool_t*) ;
static ma_error_t ma_local_request_handler_release(ma_url_request_handler_t *self) ;
static ma_error_t ma_local_request_handler_create_request(ma_url_request_handler_t *self, const char *url, ma_url_request_t **url_request) ;
static ma_error_t ma_local_request_handler_submit_request(ma_url_request_handler_t *self, ma_url_request_t *url_request, ma_url_request_finalize_callback_t completion_cb, void *userdata) ;
static ma_error_t ma_local_request_handler_cancel_request(ma_url_request_handler_t *self , ma_url_request_t *url_request) ;
static ma_error_t ma_local_request_handler_query_request_state(ma_url_request_handler_t *self , ma_url_request_t *url_request, ma_url_request_state_t *state) ;

static const struct ma_url_request_handler_methods_s local_request_handler_methods = {
    &ma_local_request_handler_start ,
    &ma_local_request_handler_stop ,
    &ma_local_request_handler_is_running ,
    &ma_local_request_handler_release ,
    &ma_local_request_handler_create_request ,
    &ma_local_request_handler_submit_request ,
    &ma_local_request_handler_cancel_request ,
    &ma_local_request_handler_query_request_state
} ;

ma_error_t ma_local_request_handler_create(ma_event_loop_t* event_loop, ma_url_request_handler_t** self) {
    if(event_loop && self) {
        ma_local_request_handler_t *local_rh  = (ma_local_request_handler_t *)calloc(1, sizeof(ma_local_request_handler_t)) ;

        if(!local_rh) return MA_ERROR_OUTOFMEMORY;

        local_rh->base.vtable = &local_request_handler_methods ;       
        local_rh->ma_loop = event_loop ;
		
        *self = (ma_url_request_handler_t *)local_rh ;

        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_local_request_handler_start(ma_url_request_handler_t *self) {    
	ma_local_request_handler_t *local_rh = (ma_local_request_handler_t*) self;
	local_rh->state = MA_TRUE;
	MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "local request handler started") ;
    return MA_OK ;
}

ma_error_t ma_local_request_handler_stop(ma_url_request_handler_t *self) {    
	ma_local_request_handler_t *local_rh = (ma_local_request_handler_t*) self;
	local_rh->state = MA_FALSE;
	MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "local request handler stopped") ;
	return MA_OK ;
}

ma_error_t ma_local_request_handler_is_running(ma_url_request_handler_t *self , ma_bool_t *running) {
	ma_local_request_handler_t *local_rh = (ma_local_request_handler_t*) self;
	*running = local_rh->state;
    return MA_OK ;
}

ma_error_t ma_local_request_handler_release(ma_url_request_handler_t *self) {
	free(self);
    return MA_OK;
}

ma_error_t ma_local_request_handler_create_request(ma_url_request_handler_t *self, const char *url, ma_url_request_t **url_request) {
    ma_error_t rc = MA_OK ;
	ma_local_url_request_t *request = NULL;

    if(MA_OK == (rc = ma_local_url_request_create(url, &request))) {
		MA_LOG(net_logger, MA_LOG_SEV_INFO, "Local URL request created");
		request->base.url_handler = self ;
		*url_request = (ma_url_request_t*)request;
		return MA_OK;
	}
	else{
        MA_LOG(net_logger, MA_LOG_SEV_ERROR, "URL request create failed");
        return rc ;
	}	
	return MA_OK;
}

static ma_error_t log_in(ma_local_url_request_t *local){
#ifdef MA_WINDOWS
	ma_error_t rc = MA_OK;
	if(local->auth && local->auth->use_loggedOn_user_credentials)	{
		MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "impersonate as logged on user");
		RevertToSelf();
		if(MA_OK != (rc = UserImpersonateLoggedOnUser(&local->token)))
			MA_LOG(net_logger, MA_LOG_SEV_ERROR, "Failed to impersonate as logged on user, error = %d", rc);
	}
	else if(local->auth && local->auth->domainname && local->auth->user && local->auth->password)
	{
		MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "impersonate as user %s\\%s", local->auth->domainname, local->auth->user);
		RevertToSelf();
		if(MA_OK != (rc = UserImpersonate(&local->token, local->auth->user, local->auth->domainname, local->auth->password))){
			MA_LOG(net_logger, MA_LOG_SEV_WARNING, "impersonate as user %s\\%s failed, error = %d", local->auth->domainname, local->auth->user, rc);

			if(MA_OK != (rc = UserImpersonateLoggedOnUser(&local->token))){
				MA_LOG(net_logger, MA_LOG_SEV_ERROR, "Failed to impersonate as logged on user, error = %d", rc);
			}
		}
	}
	return rc;
#endif
	return MA_OK;
}

static void log_out(ma_local_url_request_t *local){
#ifdef MA_WINDOWS
	if(local->token){
		MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "close impersonate token");
		CloseImpersonatedToken(local->token);
		local->token = NULL;
	}
#endif
}

ma_error_t ma_local_request_handler_submit_request(ma_url_request_handler_t *self, ma_url_request_t *url_request , ma_url_request_finalize_callback_t completion_cb, void *userdata) {
    ma_local_url_request_t *local = (ma_local_url_request_t *)url_request ;
    ma_local_request_handler_t *local_rh = (ma_local_request_handler_t *)self ;
    ma_error_t rc = MA_OK ;
	
	local->base.final_cb = completion_cb ;
    local->base.userdata = userdata ;
    local->base.request_state = MA_URL_REQUEST_STATE_INITIALIZING ;
	
	if(!local_rh->state)
		return MA_ERROR_NETWORK_SERVICE_NOT_STARTED;

	MA_LOG(net_logger, MA_LOG_SEV_INFO, "Local url to download %s",local->base.url);

	if(MA_OK == (rc = log_in(local)))	{
		if(local->base.final_cb){
			MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "Downloading file from %s in async way", local->formatted_url);
			rc = local_downloadfile(local_rh, local);
			return rc;
		}
		else{
			MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "Downloading file from %s in sync way", local->formatted_url);
			if(MA_OK == (rc = local_downloadfile_sync(local_rh, local))) {
				log_out(local);
				local->base.request_state = MA_URL_REQUEST_STATE_COMPLETED;
				return MA_ERROR_NETWORK_REQUEST_SUCCEEDED;
			}
			return rc;
		}
	}
	return rc;
}

ma_error_t ma_local_request_handler_query_request_state(ma_url_request_handler_t *self , ma_url_request_t *url_request, ma_url_request_state_t *state) {
	ma_local_url_request_t *local = (ma_local_url_request_t *)url_request ;
	*state = local->base.request_state;
    return MA_OK ;
}

ma_error_t ma_local_request_handler_cancel_request(ma_url_request_handler_t *self , ma_url_request_t *url_request) {
    ma_local_url_request_t *local = (ma_local_url_request_t *)url_request ;
	local->is_cancelled = MA_TRUE;
    return MA_OK ;
}

static void file_open_cb(uv_fs_t* req) ;
static void file_read_cb(uv_fs_t* req) ;
static void file_read(ma_local_request_handler_t *self, ma_local_url_request_t *local_r);
static void file_close(ma_local_request_handler_t *self, ma_local_url_request_t *local_r);

void file_open_cb(uv_fs_t* req) {
	ma_local_url_request_t *local = (ma_local_url_request_t *)req->data ;
	ma_local_request_handler_t *self = (ma_local_request_handler_t*)(local->base.url_handler);
	
	local->error_state = MA_ERROR_FILE_OPEN_FAILED;
	MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "file_open_cb %d", req->result);

    if(req->result >= 0) {
        uv_fs_t stat_req ;
		local->file = (uv_file)req->result ;    

		if(!uv_fs_fstat(ma_event_loop_get_uv_loop(self->ma_loop), &stat_req, local->file, NULL)) {
			
			#ifdef MA_WINDOWS
            struct _stat64 s = stat_req.statbuf;
			#else
            struct stat s = stat_req.statbuf;
			#endif

			local->error_state = MA_OK;            
			local->content_size = s.st_size ;
			
			uv_fs_req_cleanup(&stat_req) ;			
        }
		else
			MA_LOG(net_logger, MA_LOG_SEV_ERROR, "uv_fs_fstat failed");
		
    }   
    
    uv_fs_req_cleanup(req) ;

	if(MA_OK == local->error_state)
		file_read(self, local) ;		
	else
        file_close(self, local);    
}

void file_read_cb(uv_fs_t* req) {
	ma_local_url_request_t *local = (ma_local_url_request_t *)req->data ;
	ma_local_request_handler_t *self = (ma_local_request_handler_t*)(local->base.url_handler);

	MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "file_read_cb %d", req->result);

	if(req->result != -1) {
		size_t written_bytes = 0;
		
		if(local->base.progress_cb && local->base.progress_cb->progress_callback) {
			if(local->base.progress_cb->progress_callback((double)local->content_size, (double)req->result, 0.0, 0.0, local->base.userdata)){
				MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "file_read_cb user requested to stop download") ;    			
				local->is_cancelled = MA_TRUE;
			}
		}

		ma_stream_write(local->base.io_cbs->write_stream, local->buffer, req->result, &written_bytes);
		local->cur_position += written_bytes;

		MA_LOG(net_logger, MA_LOG_SEV_TRACE, "Download file, total size = <%ld>, total downloaded size = <%ld>", (long)local->content_size, (long)local->cur_position);		
		
		uv_fs_req_cleanup(req);

        if(local->content_size == local->cur_position) {
			MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "Successfully downloaded file.");
			file_close(self, local);
		}
		else if(local->is_cancelled || !self->state){
			MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "Local file download stopped.");
			local->error_state = MA_ERROR_NETWORK_REQUEST_ABORTED;
			file_close(self, local);
		}
		else
			file_read(self, local);
    }
    else {
		
		uv_fs_req_cleanup(req);

		if(local->content_size != local->cur_position) local->error_state = MA_ERROR_NETWORK_REQUEST_FAILED;
		file_close(self, local);
	}
}

static void file_read(ma_local_request_handler_t *self, ma_local_url_request_t *local) {
	MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "reading file");
	if(!uv_fs_read(ma_event_loop_get_uv_loop(self->ma_loop), &local->fs_read_req, local->file, local->buffer, sizeof(local->buffer), local->cur_position, file_read_cb))
		local->error_state = MA_OK;
	else
		local->error_state = MA_ERROR_NETWORK_REQUEST_FAILED ;	  

	if(MA_OK != local->error_state) file_close(self, local);    
}

static void file_close(ma_local_request_handler_t *self, ma_local_url_request_t *local) {
	if(local->file){
		uv_fs_t fs_close_req;

		MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "Closing file.");

		uv_fs_close(ma_event_loop_get_uv_loop(self->ma_loop), &fs_close_req, local->file, NULL) ;
		uv_fs_req_cleanup(&fs_close_req);
	}
	log_out(local);
	local->base.request_state = MA_URL_REQUEST_STATE_COMPLETED;

	MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "local file download final callback.");
	local->base.final_cb( (MA_OK == local->error_state ? MA_ERROR_NETWORK_REQUEST_SUCCEEDED: local->error_state), 0, local->base.userdata, (ma_url_request_t*)local);
}

static ma_error_t local_downloadfile(ma_local_request_handler_t *self, ma_local_url_request_t *local){
	local->error_state = MA_OK;
	if(-1 != uv_fs_open(ma_event_loop_get_uv_loop(self->ma_loop), &local->fs_open_req, local->formatted_url , O_RDONLY, 0, file_open_cb))
		local->error_state = MA_OK;
	else
        local->error_state = MA_ERROR_NETWORK_REQUEST_FAILED;

	if(MA_OK != local->error_state) file_close(self, local);    
	return local->error_state;
}

static ma_error_t local_downloadfile_sync(ma_local_request_handler_t *self, ma_local_url_request_t *local){
	ma_error_t rc = MA_ERROR_NETWORK_REQUEST_FAILED ;

	if(-1 != uv_fs_open(ma_event_loop_get_uv_loop(self->ma_loop), &local->fs_open_req, local->formatted_url , O_RDONLY, 0, NULL)){
		uv_fs_t stat_req ;
		local->file = (uv_file)local->fs_open_req.result;
		
		if(!uv_fs_fstat(ma_event_loop_get_uv_loop(self->ma_loop), &stat_req, local->file, NULL)) {
			 
			#ifdef MA_WINDOWS
            struct _stat64 s = stat_req.statbuf ;
			#else
            struct stat s = stat_req.statbuf;
			#endif

			local->content_size = s.st_size ;
			
			uv_fs_req_cleanup(&stat_req) ;	
			while(local->cur_position < local->content_size){
				if(-1 != uv_fs_read(ma_event_loop_get_uv_loop(self->ma_loop), &local->fs_read_req, local->file, local->buffer, sizeof(local->buffer), local->cur_position, NULL)){
					size_t written_bytes = 0;
					ma_stream_write(local->base.io_cbs->write_stream, local->buffer, local->fs_read_req.result, &written_bytes);
					local->cur_position += written_bytes;
					uv_fs_req_cleanup(&local->fs_read_req);
				}
				else{
					MA_LOG(net_logger, MA_LOG_SEV_DEBUG, "uv_fs_read failed.");
					break;
				}
			}
			if(local->content_size == local->cur_position) {
				MA_LOG(net_logger, MA_LOG_SEV_INFO, "file download complete.");
				rc = MA_OK;
				local->error_state = MA_OK;
			}
        }
		else{
			MA_LOG(net_logger, MA_LOG_SEV_INFO, "fs fstat failed.");
		}
		uv_fs_close(ma_event_loop_get_uv_loop(self->ma_loop), &local->fs_open_req, local->file, NULL);
		uv_fs_req_cleanup(&local->fs_open_req);
	}	
	else {
		MA_LOG(net_logger, MA_LOG_SEV_INFO, "fs open failed.");
	}
	return rc ;
}

#endif 
