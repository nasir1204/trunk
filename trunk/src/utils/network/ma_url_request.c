#include "ma_url_utils.h"
#include "ma/internal/utils/network/ma_url_request_handler.h"
#include "ma/internal/utils/network/ma_url_request.h"
#include "ma/internal/utils/network/ma_url_request_base.h"
#include "ma_net_log.h"
#include "ma_curl.h"

#include <string.h>
#include <stdlib.h>

MA_CPP(extern "C" {)

extern ma_error_t ma_net_client_service_get_rh(ma_net_client_service_t *self,ma_network_transfer_protocol_t protocol,ma_url_request_handler_t **handler);

ma_error_t ma_url_request_create(ma_net_client_service_t *net_service, const char *url, ma_url_request_t **url_request) {
	if(net_service && url && url_request){
        ma_error_t rc = MA_OK;
		ma_url_request_handler_t *url_rh=NULL;
		ma_network_transfer_protocol_t protocol;
		if ( !( MA_OK == ( rc = ma_url_utils_get_protocol_type(url, &protocol) ) &&
				MA_OK == ( rc = ma_net_client_service_get_rh(net_service,protocol,&url_rh) ) &&
				MA_OK == ( rc = ma_url_request_handler_create_request(url_rh,url,url_request) ) ) )
			return rc;
		return MA_OK;
	}
    MA_NET_LOG_INVARG() ;
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_url_request_add_ref(ma_url_request_t *self) {
    if(self) {
        MA_ATOMIC_INCREMENT(self->ref_count);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_url_request_release(ma_url_request_t *self){
	if(self){
        if( MA_URL_REQUEST_STATE_COMPLETED == self->request_state || MA_URL_REQUEST_STATE_CANCELLED == self->request_state) {
            if(!MA_ATOMIC_DECREMENT(self->ref_count)) 
                return self->methods->release(self);
        }
        return MA_ERROR_NETWORK_ACTIVE_URL_REQUEST;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_url_request_set_option(ma_url_request_t *self, ma_url_request_option_t option, unsigned long option_value) {
	if(self){
		switch(option){
			case MA_URL_REQUEST_OPTION_REQUEST_TYPE:	 self->request_type=option_value; break;
			case MA_URL_REQUEST_OPTION_VERBOSE_LOG:		 self->verbose_log=option_value; break;
			case MA_URL_REQUEST_OPTION_TRANSFER_TIMEOUT: self->transfer_timeout=option_value; break;
			case MA_URL_REQUEST_OPTION_CONNECT_TIMEOUT:  self->connect_timeout=option_value; break;
			case MA_URL_REQUEST_OPTION_USE_FRESH_CONNECTION: self->use_fresh_connection=option_value; break;	
			default:
				return MA_ERROR_INVALID_OPTION;
		}
		return MA_OK;
	}
    MA_NET_LOG_INVARG() ;
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_url_request_set_url(ma_url_request_t *self, const char *url) {
    if(self && url) {
        if(self->url && !strcmp(self->url, url))
            return MA_OK ;
        else {
            ma_network_transfer_protocol_t n_protocol ;
            if(MA_OK == ma_url_utils_get_protocol_type(url, &n_protocol) && self->protocol == n_protocol) {
                if(self->url) free(self->url) ;
                self->url = NULL;
                self->url = strdup(url) ;
                return MA_OK ;
            }
            return MA_ERROR_NETWORK_INVALID_URL ;
        }
    }
    MA_NET_LOG_INVARG() ;
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_url_request_set_post_fields(ma_url_request_t *self, const char *post_fileds){
	if(self && post_fileds){
		self->post_fields = strdup(post_fileds);
		return MA_OK ;
    }
    MA_NET_LOG_INVARG() ;
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_url_request_set_custom_header(ma_url_request_t *self, const char *key, const char *value){
	if(self && key && value){
		self->custom_header.key = strdup(key);
		self->custom_header.value = strdup(value);
		return MA_OK ;
    }
    MA_NET_LOG_INVARG() ;
    return MA_ERROR_INVALIDARG ;
}



ma_error_t ma_url_request_set_url_auth_info(ma_url_request_t *self, ma_url_request_auth_t *authenticate) {
	if(self && authenticate){
		return self->methods->set_url_auth_info(self,authenticate);
	}
    MA_NET_LOG_INVARG() ;
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_url_request_set_ssl_info(ma_url_request_t *self, ma_url_request_ssl_info_t *ssl_info) {
	if(self && ssl_info){
		return self->methods->set_ssl_info(self,ssl_info);
	}
    MA_NET_LOG_INVARG() ;
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_url_request_set_range_info(ma_url_request_t *self, ma_url_request_range_t *range_info) {
    if(self && range_info){
		return self->methods->set_range_info(self,range_info);
    }
    MA_NET_LOG_INVARG() ;
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_url_request_set_proxy_info(ma_url_request_t *self, ma_proxy_list_t *proxy_list) {
	if(self){
		return self->methods->set_proxy_info(self, proxy_list) ;
	}
    MA_NET_LOG_INVARG() ;
	return MA_ERROR_INVALIDARG;
}
ma_error_t ma_url_request_set_resolve_info(ma_url_request_t *self, ma_url_request_resolve_t *resolve_info) {
	if(self && resolve_info){
        return self->methods->set_resolve_info(self, resolve_info);
	}
    MA_NET_LOG_INVARG() ;
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_url_request_set_io_callback(ma_url_request_t *self, ma_url_request_io_t *rw_cbs) {
	if(self && rw_cbs){
		self->io_cbs=rw_cbs;
		return MA_OK;
	}
    MA_NET_LOG_INVARG() ;
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_url_request_set_connect_callback(ma_url_request_t *self, ma_url_request_connect_callback_t connect_cb) {
	if(self ){
		self->connect_cb = connect_cb;
		return MA_OK;
	}
    MA_NET_LOG_INVARG() ;
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_url_request_set_progress_callback(ma_url_request_t *self, ma_url_request_progress_t *progress_cb ) {
    if(self && progress_cb){
		self->progress_cb=progress_cb;
		return MA_OK;
	}
    MA_NET_LOG_INVARG() ;
	return MA_ERROR_INVALIDARG;
}


ma_error_t ma_url_request_send(ma_url_request_t *self, ma_url_request_finalize_callback_t completion_cb, void *userdata) {
	if(self && completion_cb)
		return ma_url_request_handler_submit_request(self->url_handler, self, completion_cb, userdata);
    MA_NET_LOG_INVARG() ;
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_url_request_sync_send(ma_url_request_t *self, void *userdata){
	if(self)
		return ma_url_request_handler_submit_request(self->url_handler, self, NULL, userdata);
    MA_NET_LOG_INVARG() ;
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_url_request_query_state(ma_url_request_t *self, ma_url_request_state_t *state){
	if(self && state)
		return ma_url_request_handler_query_request_state(self->url_handler,self,state);
    MA_NET_LOG_INVARG() ;
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_url_request_cancel(ma_url_request_t *self){
	if(self)
		return ma_url_request_handler_remove_request(self->url_handler,self) ;
    MA_NET_LOG_INVARG() ;
	return MA_ERROR_INVALIDARG;

}

ma_error_t ma_url_request_restart(ma_url_request_t *self) {
	if(self)
        return ma_url_request_handler_submit_request(self->url_handler, self, self->final_cb, self->userdata);
    MA_NET_LOG_INVARG() ;
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_url_response_message(long response_code, const char **message){
	if(message){
		//add code to return the response message for the passed response code.
	}
    MA_NET_LOG_INVARG() ;
	return MA_ERROR_INVALIDARG;
}

ma_bool_t ma_url_request_get_result(ma_url_request_t *self, long response_code){
	if(self){
		if(MA_URL_TRANSER_PROTOCOL_HTTP == self->protocol && MA_URL_REQUEST_TYPE_POST == self->request_type ){
			return (response_code == 200 || response_code == 202);
		}

		if(MA_URL_TRANSER_PROTOCOL_HTTP == self->protocol){
			return (response_code == 200);
		}
		else if(MA_URL_TRANSER_PROTOCOL_FTP == self->protocol){
			if(550 == response_code	|| response_code > 400) return MA_FALSE;
			return MA_TRUE;
		}
		else if(MA_URL_TRANSER_PROTOCOL_UNC == self->protocol){
			return (response_code == 200);
		}
		else if(MA_URL_TRANSER_PROTOCOL_FILE == self->protocol){
			return (response_code == 200);
		}
		return MA_FALSE;
	}
	return MA_FALSE;
}

MA_CPP(})
