#ifndef MA_UNC_REQUEST_H_INCLUDED
#define MA_UNC_REQUEST_H_INCLUDED

#include "ma/ma_common.h"
#include "ma_url_utils.h"
#include "ma/internal/utils/network/ma_url_request_base.h"

MA_CPP(extern "C" {)

typedef struct ma_unc_url_request_s {
	ma_url_request_t base ;
	ma_url_request_auth_t *auth ;               //authentication -- user/passwd
 	ma_unc_url_info_t *ma_unc_url_info;
    uv_poll_t   *io_watcher ;					//uv pool handle
    uv_timer_t *unc_timer ;
	ma_bool_t use_WNet;
	ma_bool_t networkshare_created;
	uv_fs_t fs_open_req ;
    uv_fs_t fs_read_req ;
    uv_file file ;
	ma_error_t error_state;
	ma_int64_t content_size ;
	ma_int64_t cur_position;
	ma_bool_t is_cancelled;
	unsigned char buffer[1024] ;
	HANDLE m_hToken;
}ma_unc_url_request_t ;

ma_error_t ma_unc_url_request_create(const char *url, ma_unc_url_request_t **unc_url_request) ;


MA_CPP(})

#endif  //MA_UNC_REQUEST_H_INCLUDED

