#include "ma/internal/utils/platform/ma_sys_apis.h"
#include "ma/internal/utils/filesystem/path.h"

#ifdef MA_WINDOWS
#include <Windows.h>
#ifdef _WIN64
#define MA_REG_CROSS_ACCESS	KEY_WOW64_32KEY
#else
#define MA_REG_CROSS_ACCESS	KEY_WOW64_64KEY
#endif

#include "ma/internal/utils/text/ma_utf8.h"
typedef LSTATUS (WINAPI *reg_delete_tree_handle)(HKEY h_key, LPCWSTR path);
typedef LSTATUS (WINAPI *reg_delete_key_ex_handle)(HKEY hkey, LPCWSTR lpSubKey, REGSAM samDesired, DWORD Reserved);

#endif

#ifdef MA_WINDOWS

static ma_error_t recursive_registry_delete(void* hKeyRoot_, const wchar_t *path,ma_bool_t b_default_view, reg_delete_key_ex_handle reg_delete_key_ex) {
	ma_error_t error = MA_ERROR_INVALIDARG;
	int len = 0;
	if(path && ((len = wcslen(path)) < MAX_PATH ) ) {
		HKEY hKeyRoot = (HKEY) hKeyRoot_;
		LONG lReturnValue = 0;
		HKEY hSubKey = NULL;
		ma_temp_buffer_t buffer;
		DWORD dwSize = 0;
		wchar_t *lEnd = NULL;
		FILETIME f_time_write;
		WCHAR key_value[ MAX_PATH] = {0};
		WCHAR sub_path[MAX_PATH] = {0};
		ma_temp_buffer_init(&buffer);
		wcsncpy(sub_path, path, len);
		/*This is just a protective check so that we don't delete the HKEY\\SOFTWARE */
		if(0 == wcscmp(sub_path, L"SOFTWARE") || 0 == wcscmp(sub_path,L"Software") || 0 == wcscmp(sub_path, L"software")) {
			return MA_ERROR_INVALIDARG;
		}
		if(reg_delete_key_ex)
			lReturnValue = reg_delete_key_ex(hKeyRoot, sub_path ,b_default_view? 0:MA_REG_CROSS_ACCESS, 0);
		else
			lReturnValue = RegDeleteKeyW(hKeyRoot, sub_path);

		if(ERROR_SUCCESS == lReturnValue)
			return MA_OK;


		if(ERROR_SUCCESS == (lReturnValue = RegOpenKeyExW(hKeyRoot, sub_path, 0, b_default_view? KEY_ALL_ACCESS: (KEY_ALL_ACCESS |MA_REG_CROSS_ACCESS), &hSubKey))) {
			lEnd = sub_path + len;
			if( (*(lEnd - 1) != '\\') && (len < MAX_PATH - 1)){
				*lEnd = '\\';
				lEnd++;
			}
			dwSize = MAX_PATH;

			lReturnValue = RegEnumKeyExW(hSubKey, 0, key_value, &dwSize, NULL, NULL, NULL, &f_time_write);
			if(ERROR_SUCCESS == lReturnValue) {
				do {
					if(dwSize > (MAX_PATH - len)){
						break;
					}
					wcsncpy(lEnd, key_value, dwSize);
					if(MA_OK != recursive_registry_delete(hKeyRoot, sub_path, b_default_view,reg_delete_key_ex))
						break;
					dwSize = MAX_PATH;
					lReturnValue = RegEnumKeyExW(hSubKey, 0, key_value, &dwSize, NULL, NULL, NULL, &f_time_write);
				}while(lReturnValue == ERROR_SUCCESS);
			}
			lEnd--;
			*lEnd = '\0';
			RegCloseKey(hSubKey);

			if(reg_delete_key_ex)
				lReturnValue = reg_delete_key_ex(hKeyRoot, sub_path, b_default_view?0:MA_REG_CROSS_ACCESS, 0 );
			else
				lReturnValue = RegDeleteKeyW(hKeyRoot, sub_path);
		}
		if(ERROR_FILE_NOT_FOUND == lReturnValue)
			error = MA_ERROR_OBJECTNOTFOUND;
		else if(ERROR_SUCCESS != lReturnValue)
			error = MA_ERROR_APIFAILED;
		else
			error = MA_OK;

		ma_temp_buffer_uninit(&buffer);
	}
	return error;
}
#endif

ma_error_t ma_registry_delete_tree(void* hKeyRoot_, const char *path_, ma_bool_t b_default_view) {
#if defined(MA_WINDOWS)
	if(hKeyRoot_ && path_) {
		HKEY hKeyRoot = (HKEY) hKeyRoot_;
		WCHAR path[MAX_PATH_LEN] = {0};
		reg_delete_tree_handle p_reg_delete_tree_handle= (reg_delete_tree_handle ) GetProcAddress(GetModuleHandleW(L"advapi32.dll"), "RegDeleteTreeW");
		if(-1 != ma_utf8_to_wide_char(path, MAX_PATH_LEN, path_, strlen(path_)) ){
			if(p_reg_delete_tree_handle) {
				/*
				 * Even though we have got RegDeleteTree Handle, we will not be able to delete entries in the alternate view.
				 * So open the key and delete it directly
				 */
				HKEY hkey = NULL;
				LSTATUS error = ERROR_NONE_MAPPED;
				if(ERROR_SUCCESS == (error = RegOpenKeyExW(hKeyRoot, path, 0, b_default_view? KEY_ALL_ACCESS: (KEY_ALL_ACCESS | MA_REG_CROSS_ACCESS), &hkey))) {
					(*p_reg_delete_tree_handle)(hkey, L"");
					/*The above call removes all the subkeys and values, but the node will remain. So delete it. Since the hkey is already opened in the view we want, RegDeleteKey will work just fine*/
					RegDeleteKeyW(hkey,L"");
					RegCloseKey(hkey);
					return MA_OK;
				}
				else {
					if(ERROR_ACCESS_DENIED == error)
						return MA_ERROR_ACCESS_DENIED;
					else if (ERROR_PATH_NOT_FOUND == error)
						return MA_ERROR_DS_PATH_NOT_FOUND;
					else
						return MA_ERROR_APIFAILED;
				}
			}
			else {
				/*Simple use RegDeleteKey since its */
				reg_delete_key_ex_handle p_reg_delete_keyex_handle = (reg_delete_key_ex_handle)GetProcAddress(GetModuleHandleW(L"advapi32.dll"), "RegDeleteKeyExW");
				return recursive_registry_delete(hKeyRoot, path, b_default_view, p_reg_delete_keyex_handle);
			}
		}
		return MA_ERROR_APIFAILED;
	}
	return MA_ERROR_INVALIDARG;
#else
	return MA_ERROR_NOT_SUPPORTED;
#endif
}

