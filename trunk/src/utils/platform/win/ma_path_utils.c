#include "ma/internal/utils/platform/ma_path_utils.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/text/ma_utf8.h"

#include <Windows.h>
#include <process.h>
#include <Psapi.h>

static DWORD get_windows_version_major(void) {
    OSVERSIONINFOEX osvinfox;
    osvinfox.dwOSVersionInfoSize = sizeof(osvinfox);
    GetVersionEx((LPOSVERSIONINFO) &osvinfox);
    return osvinfox.dwMajorVersion;
}

ma_error_t ma_pid_to_path(long pid, ma_temp_buffer_t *buffer) {
    HANDLE peer_process_handle;
    DWORD win_error;
    ma_error_t rc = MA_ERROR_APIFAILED;
    
    if (NULL != (peer_process_handle = OpenProcess( 5 < get_windows_version_major() ? PROCESS_QUERY_LIMITED_INFORMATION : PROCESS_QUERY_INFORMATION, FALSE, pid))) {
        WCHAR peer_path_name[MAX_PATH] = {0};
        DWORD buffer_length = MA_COUNTOF(peer_path_name);
        BOOL (WINAPI *fnQueryFullProcessImageNameW)(HANDLE , DWORD dwFlags, LPWSTR lpExeName, PDWORD lpdwSize) = (void*) GetProcAddress(GetModuleHandleA("kernel32"),"QueryFullProcessImageNameW");
        
        if (fnQueryFullProcessImageNameW) {
            if (fnQueryFullProcessImageNameW(peer_process_handle, PROCESS_NAME_NATIVE, peer_path_name, &buffer_length)) {
                rc = ma_convert_wide_char_to_utf8(peer_path_name, buffer);
            } 
        } else if (GetProcessImageFileNameW(peer_process_handle, peer_path_name, buffer_length)) {
            rc = ma_convert_wide_char_to_utf8(peer_path_name, buffer);
        } else {
             win_error = GetLastError();
            
        }
        CloseHandle(peer_process_handle);
    } else {
        win_error = GetLastError();
    }

    return rc;
}

ma_error_t ma_pid_to_stat(long pid, struct stat *stat_buf) {
    /*TBD*/
    return MA_OK;
}

