#include "ma/internal/utils/platform/ma_system_property.h"
#include "../ma_system_property_common.h"
#include "ma/internal/utils/text/ma_utf8.h"
#include "ma/internal/utils/datastructures/ma_bytebuffer.h"
#include "ma/internal/utils/datastructures/ma_slist.h"

#include <stdio.h>
#include <ws2tcpip.h>
#include <windows.h>
#include <tchar.h>
#include <lm.h>
#include <intrin.h>
#include <ntsecapi.h>
#include <wbemidl.h>
#include <ClusApi.h>
#include <WtsApi32.h>
#include <Resapi.h>

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "maconfigurator"

MA_CPP(extern "C" {)
#pragma comment(lib, "netapi32.lib")
#pragma comment(lib, "Secur32.lib")
#pragma comment(lib, "ClusApi.lib")
#pragma comment(lib, "Wtsapi32.lib")
	
static OSVERSIONINFOW g_osvi = {0};
static char g_os_type2[64] = {0};
static char g_os_nt_type[64] = {0};
static char g_platform_type[64] = {0};
extern ma_logger_t *g_system_property_logger;

typedef struct ma_locales_table_s{
    char* code;
    char* name;
} ma_locales_table_t;
/*TBD: Need to add more */
ma_locales_table_t lang_id_table[] =
{
    { "0404", "Chinese(TW)" },
    { "0405", "Czech" },
    { "0406", "Danish" },
    { "0407", "German" },
    { "0409", "English" },
    { "040A", "Spanish" },	/*Spanish_Traditional_Sort*/
    { "040B", "Finnish" },
    { "040C", "French" },
    { "0410", "Italian" },
    { "0411", "Japanese" },
    { "0412", "Korean" },
    { "0413", "Dutch" },
    { "0414", "Norwegian" },
    { "0415", "Polish" },
    { "0416", "Portugese-Brazil" },
    { "0419", "Russian" },
    { "041D", "Swedish" },
    { "041F", "Turkish" },
    { "0804", "Chinese(ZH)" },
    { "0816", "Portugese-Portugal" },
	{ "0C0A", "Spanish" },	/*Spanish_Modern_Sort*/
    { NULL,       NULL   }
};

#define MA_REG_NTPLATFORM			TEXT("SYSTEM\\CurrentControlSet\\Control\\ProductOptions")
#define MA_REG_NTCURRENTVERSION		TEXT("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion")
#define MA_REG_LANMANSERVER			TEXT("SYSTEM\\CurrentControlSet\\Services\\lanmanserver\\parameters")
#define MA_REG_PRODUCTTYPE			TEXT("ProductType")
#define MA_REG_PRODUCTID			TEXT("ProductId")


#define MA_ERROR             "ERROR"
#define MA_UNKNOWN           "Unknown"
#define MA_WINDOWSNT         "Windows NT"
#define MA_WINDOWS2000       "Windows 2000"
#define MA_WINDOWSME         "Windows Millennium"
#define MA_WINDOWSXP		 "Windows XP"
#define MA_WINDOWS2003		 "Windows 2003"
#define MA_WINDOWS2003R2	 "Windows 2003 R2"
#define MA_WINDOWSVISTA		 "Windows Vista"
#define MA_WINDOWS2008       "Windows 2008"
#define MA_WINDOWS2008R2     "Windows 2008 R2"
#define MA_WINDOWS7          "Windows 7"
#define MA_WINDOWS8WRK       "Windows 8"
#define MA_WINDOWS8SRV       "Windows Server 2012"
#define MA_WINDOWS81WRK      "Windows 8.1"
#define MA_WINDOWS81SRV      "Windows Server 2012 R2"
#define MA_NTSERVER          "Server"
#define MA_NTWORKSTATION     "Workstation"
#define MA_WINDOWS98         "Windows 98"
#define MA_WINDOWS95         "Windows 95"
#define MA_WINDOWS3X         "Windows 3.x"
#define MA_PROFESSIONAL      "Professional"
#define MA_ADVANCESERVER     "Advanced Server"
#define MA_DATACENTER	     "Data Center"
#define MA_EMBEDDED 	     "Embedded"
#define MA_PERSONAL 	     "Personal"

#define MA_PLATFORM_NT_SERVER                      "WNTS"
#define MA_PLATFORM_NT_WORKSTATION                 "WNTW"
#define MA_PLATFORM_WINDOWS95                      "WIN95"
#define MA_PLATFORM_WINDOWS98                      "WIN98"
#define MA_PLATFORM_WINDOWSME                      "WINME"
#define MA_PLATFORM_WINDOWS2000_PROFESSIONAL       "W2KW"
#define MA_PLATFORM_WINDOWS2000_SERVER             "W2KS"
#define MA_PLATFORM_WINDOWS2000_ADVANCEDSERVER     "W2KAS"
#define MA_PLATFORM_WINDOWS2000_DATACENTER         "W2KDC"
#define MA_PLATFORM_WINDOWSXP_PROFESSIONAL         "WXPW"
#define MA_PLATFORM_WINDOWSXP_SERVER               "WXPS"
#define MA_PLATFORM_WINDOWSXP_HOMEEDITION          "WXPHE"
#define MA_PLATFORM_WINDOWS_VISTA                  "WVST"
#define MA_PLATFORM_WINDOWS2008_SERVER             "WVSTS"
#define MA_PLATFORM_WINDOWS7_WORKSTATION           "WNT7W"
#define MA_PLATFORM_WINDOWS8_WORKSTATION           "WIN8W"
#define MA_PLATFORM_WINDOWS8_SERVER                "WIN8S"
#define MA_PLATFORM_WINDOWS81_WORKSTATION          "WIN81W"
#define MA_PLATFORM_WINDOWS81_SERVER               "WIN81S"
#define MA_PLATFORM_WINDOWSXP_EMBEDDED             "WXPE"
#define MA_NT_STATUS_SUCCESS ((NTSTATUS)0x00000000L)

typedef struct ma_loggedon_users_info_node_s ma_loggedon_users_info_node_t;
struct ma_loggedon_users_info_node_s {
	char			*name;
	PSID			sid;
	MA_SLIST_NODE_DEFINE(ma_loggedon_users_info_node_t);
};

typedef struct ma_loggedon_users_list_s {
	MA_SLIST_DEFINE(user_info, ma_loggedon_users_info_node_t);
}ma_loggedon_users_list_t;

ma_bool_t is_win_nt(void){
    return GetVersion() < 0x80000000 ? MA_TRUE : MA_FALSE;
}

void get_os_version_info_struct(){
    g_osvi.dwOSVersionInfoSize = sizeof( OSVERSIONINFO );
    GetVersionEx(&g_osvi);
    return;
}

ma_bool_t is_wow64()
{
    BOOL b_is_wow64 = FALSE;
    if (!IsWow64Process(GetCurrentProcess(),&b_is_wow64))
    {
        MA_LOG(g_system_property_logger, MA_LOG_SEV_INFO, "IsWow64Process API failed");
    }
    return (ma_bool_t)b_is_wow64;
}

ma_error_t os_version(ma_os_system_property_t *os_info){
    char       version_buf[16] = {0};
    char       buildnum[64] = {0};
    TCHAR       csdversion_buf[128] = {0};
    size_t clen =0, wlen = 0;
    get_os_version_info_struct();
    _snprintf(version_buf, 16, "%d.%01d", g_osvi.dwMajorVersion, g_osvi.dwMinorVersion );
    _snprintf(buildnum, 64, "%d", LOWORD(g_osvi.dwBuildNumber));
    if ( g_osvi.szCSDVersion[0] != _T('\0') )
        _tcscpy(csdversion_buf, g_osvi.szCSDVersion);
    else
        _tcscpy(csdversion_buf, _T(""));
    
    strncpy(os_info->version,version_buf,31);
    strncpy(os_info->buildnumber,buildnum, 63);
    if(-1 == ma_wide_char_to_utf8(os_info->servicepack, MA_MAX_LEN-1, csdversion_buf, wcslen(csdversion_buf)))
    {
        MA_LOG(g_system_property_logger, MA_LOG_SEV_ERROR, "ma_wide_char_to_utf8 call failed for OS Service pack, setting to NA");
        strncpy(os_info->servicepack, MA_NA, 4);
    }
    return MA_OK;
}

ma_error_t os_bit_mode_get(char *os_bit_mode){
    UINT size = 0;
    TCHAR tmp[1];

    get_os_version_info_struct();
    strncpy(os_bit_mode, "0", 3);
    /* GetSystemWow64Directory() requires at least Win XP or Win 2003 Server
    and 64-bit is potentially only on at least XP. */
    if (g_osvi.dwMajorVersion >= 4 || (g_osvi.dwMajorVersion >= 3 && g_osvi.dwMinorVersion >= 1))
    {        
            /* GetSystemWow64Directory always fails on 32 bit operating systems.
             It returns 0 on failure or the length, so we must give it a chance
             to copy at least 1 character into the buffer so that we can differentiate 
			 between an error code and a size of 0. */

            size =GetSystemWow64DirectoryW(tmp, 1);
            if (size > 0)
                strncpy(os_bit_mode, "1", 3);
    }
    return MA_OK;
}

void get_nt_type(){
    HKEY    h_key;
    LRESULT result;
    char   sz_buffer[64];
    DWORD   cb_value = 63; 
    DWORD   dw_reg_type;
    ma_bool_t    b_ok;
    strncpy(g_os_nt_type, MA_NA, 64);

    result = RegOpenKeyEx(HKEY_LOCAL_MACHINE, MA_REG_NTPLATFORM, 0, KEY_READ, &h_key);
    b_ok = result == ERROR_SUCCESS;  
    if ( b_ok )
    {
        result = RegQueryValueEx(h_key, MA_REG_PRODUCTTYPE, NULL, &dw_reg_type, (LPBYTE)sz_buffer, &cb_value);
        b_ok = result == ERROR_SUCCESS;
        if ( b_ok )
        {
            if (stricmp(sz_buffer, "WinNT") == 0)
                strncpy(g_os_nt_type, MA_NTWORKSTATION, 64);
            else if (stricmp(sz_buffer, "ServerNT") == 0 || stricmp(sz_buffer, "LANMANNT") == 0)
                strncpy(g_os_nt_type, MA_NTSERVER, 64);
        }

        RegCloseKey(h_key);
    }
    return ;   
}

void get_os_type_2(){
    get_os_version_info_struct();
    
    if (g_osvi.dwPlatformId == VER_PLATFORM_WIN32_NT)
    {
        if (g_osvi.dwMajorVersion == 4)
        {
            strncpy(g_os_type2, MA_WINDOWSNT, 64 - 1);
        }
        else if (g_osvi.dwMajorVersion == 5 && g_osvi.dwMinorVersion == 0)
        {
            strncpy(g_os_type2, MA_WINDOWS2000, 64 - 1);
        }
        else if (g_osvi.dwMajorVersion == 5 && g_osvi.dwMinorVersion == 1)
        {
            strncpy(g_os_type2, MA_WINDOWSXP, 64 - 1);
        }
        else if (g_osvi.dwMajorVersion == 5 && g_osvi.dwMinorVersion == 2)
        {
            // On 64-bit WinXP, for some reason we arrive here.  We need to
            // look further to determine if, in fact, we are XP and not 2003
            // server.
            //
            OSVERSIONINFOEX osvi;
            ma_bool_t bOsVersionInfoEx;

            ZeroMemory(&osvi, sizeof(OSVERSIONINFOEX));
            osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);

            bOsVersionInfoEx = GetVersionEx ((OSVERSIONINFO *) &osvi) ;
            if (bOsVersionInfoEx)
            {
                if (osvi.wProductType == VER_NT_WORKSTATION)
                {
                    strncpy(g_os_type2, MA_WINDOWSXP, 64 - 1);
                }
                else
                {
                  // WinUser.h - SM_SERVERR2 = 89
                  if(GetSystemMetrics(89) != 0)
                  {
                          strncpy(g_os_type2, MA_WINDOWS2003R2, 64 - 1);
                  }
                  else
                  {
                          strncpy(g_os_type2, MA_WINDOWS2003, 64 - 1);
                  }
                }
            }
            else
                strncpy(g_os_type2, MA_WINDOWS2003, 64 - 1);
        }
        else if (g_osvi.dwMajorVersion == 6)
        {
            OSVERSIONINFOEX osviEx;
            ma_bool_t bOsVersionInfoEx;

            ZeroMemory(&osviEx, sizeof(OSVERSIONINFOEX));
            osviEx.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);

            bOsVersionInfoEx = GetVersionEx ((OSVERSIONINFO *) &osviEx) ;
            if (bOsVersionInfoEx) {   
                switch(osviEx.dwMinorVersion) {
                    /* The following code assumes if it's not workstation it must be a server of some sort */
                    /* E.g. there is no distinction between VER_NT_DOMAIN_CONTROLLER and VER_NT_SERVER */
                case 0:
                    strncpy(g_os_type2, VER_NT_WORKSTATION == osviEx.wProductType ? MA_WINDOWSVISTA : MA_WINDOWS2008, 64 - 1);
                    break;

                case 1:
                    strncpy(g_os_type2, VER_NT_WORKSTATION == osviEx.wProductType ? MA_WINDOWS7 : MA_WINDOWS2008R2, 64 - 1);
                    break;
                
                case 2:
                    strncpy(g_os_type2, VER_NT_WORKSTATION == osviEx.wProductType ? MA_WINDOWS8WRK : MA_WINDOWS8SRV, 64 - 1);
                    break;
                case 3:
                    /* Windows 8.1, Server 2012R2 */
                    strncpy(g_os_type2, VER_NT_WORKSTATION == osviEx.wProductType ? MA_WINDOWS81WRK : MA_WINDOWS81SRV, 64 - 1);
                    break;
                default:
                    strncpy(g_os_type2, MA_UNKNOWN, 64 - 1);
                }
            }
            else
            {
                get_nt_type();
                if (strcmp(g_os_nt_type, MA_NTSERVER) == 0)
                {
                    switch( g_osvi.dwMinorVersion )
                    {
                        case 0:
                            strncpy(g_os_type2, MA_WINDOWS2008, 64 - 1);
                            break;
                        case 1:
                            strncpy(g_os_type2, MA_WINDOWS2008R2, 64 - 1);
                            break;
                        case 2:
                            strncpy(g_os_type2, MA_WINDOWS8SRV, 64 - 1);
                            break;
                        case 3:
                            strncpy(g_os_type2, MA_WINDOWS81SRV, 64 - 1);
                            break;
                        default:
                            strncpy(g_os_type2, MA_UNKNOWN, 64 - 1);
                            break;
                    }
                }
                else if (strcmp(g_os_nt_type, MA_NTWORKSTATION) == 0)
                {
                    switch( g_osvi.dwMinorVersion )
                    {
                        case 0:
                            strncpy(g_os_type2, MA_WINDOWSVISTA, 64 - 1);
                            break;
                        case 1:
                            strncpy(g_os_type2, MA_WINDOWS7, 64 - 1);
                            break;
                        case 2:
                            strncpy(g_os_type2, MA_WINDOWS8WRK, 64 - 1);
                            break;
                        case 3:
                            strncpy(g_os_type2, MA_WINDOWS81WRK, 64 - 1);
                            break;
                        default:
                            strncpy(g_os_type2, MA_UNKNOWN, 64 - 1);
                            break;
                    }
                }
                else
                {
                    strncpy(g_os_type2, MA_UNKNOWN, 64 - 1);
                }
            }
        }
    }
    else if (g_osvi.dwPlatformId == VER_PLATFORM_WIN32_WINDOWS)
    {
        if (g_osvi.dwMajorVersion == 4 && g_osvi.dwMinorVersion == 0)
        {
            strncpy(g_os_type2, MA_WINDOWS95, 64 - 1);
        }
        else if (g_osvi.dwMajorVersion == 4 && g_osvi.dwMinorVersion == 10)
        {
            strncpy(g_os_type2, MA_WINDOWS98, 64 - 1);
        }
        else if (g_osvi.dwMajorVersion == 4 && g_osvi.dwMinorVersion == 90)
        {
            strncpy(g_os_type2, MA_WINDOWSME, 64 - 1);
        }
        
    }
    else
    {
        strncpy(g_os_type2, MA_WINDOWS3X, 64 - 1);
    }
    return ;   
}

int wmi_oemid(char oemid_buf[64]){
    #pragma comment(lib, "wbemuuid.lib ")
    ma_bool_t rc = MA_FALSE;
    IWbemLocator         *locator  = 0;
    IWbemServices        *services = 0;
    IEnumWbemClassObject *results  = 0;
    BSTR resource = SysAllocString(L"ROOT\\CIMV2");
    BSTR language = SysAllocString(L"WQL");
    BSTR query    = SysAllocString(L"SELECT SerialNumber FROM Win32_OperatingSystem");
 
    if(S_OK == CoCreateInstance(&CLSID_WbemLocator, 0, CLSCTX_INPROC_SERVER, &IID_IWbemLocator, (LPVOID *) &locator)){
        locator->lpVtbl->ConnectServer(locator, resource, (BSTR)NULL,(BSTR) NULL, (BSTR)NULL, 0,(BSTR) NULL, (IWbemContext*)NULL, &services);
        if(services){
			if(S_OK == CoSetProxyBlanket(services, RPC_C_AUTHN_DEFAULT, RPC_C_AUTHZ_DEFAULT, COLE_DEFAULT_PRINCIPAL, RPC_C_AUTHN_LEVEL_DEFAULT, 
				RPC_C_IMP_LEVEL_IMPERSONATE, COLE_DEFAULT_AUTHINFO, EOAC_DEFAULT )) {
				services->lpVtbl->ExecQuery(services, language, query, WBEM_FLAG_RETURN_IMMEDIATELY, 0, &results);
				if (results != NULL) {
					IWbemClassObject *result = 0;
					ULONG returnedCount = 0;
					while(results->lpVtbl->Next(results, WBEM_INFINITE, 1, &result, &returnedCount) == S_OK) {
						VARIANT name;
						VariantInit(&name); 
						if(S_OK == result->lpVtbl->Get(result, L"SerialNumber", 0, &name, 0, 0)){
							if(-1 == ma_wide_char_to_utf8(oemid_buf, 63, name.bstrVal, wcsnlen(name.bstrVal, 64))) {
								MA_LOG(g_system_property_logger, MA_LOG_SEV_ERROR, "ma_wide_char_to_utf8 call failed for serial number, setting to NA");
								strncpy(oemid_buf, MA_NA, 3);
							}
							else
								rc = MA_TRUE;
							VariantClear(&name);
							result->lpVtbl->Release(result);
							break;
						}
						VariantClear(&name);
						result->lpVtbl->Release(result);
					}
					results->lpVtbl->Release(results);
				}
			}
			services->lpVtbl->Release(services);
        }
        locator->lpVtbl->Release(locator);
    }

    SysFreeString(query);
    SysFreeString(language);
    SysFreeString(resource);
    return rc;
}

ma_error_t os_oemid_get(char *os_oemid){
    static char			oemid_buf[64] = {0};
    static ma_bool_t	success = MA_FALSE;
    HKEY				hKey = NULL;
    DWORD				dwRes, dwType = REG_SZ;
    DWORD				cbData = sizeof(oemid_buf);
    
    if(MA_TRUE == success){
        strncpy(os_oemid, oemid_buf, 63);
        return MA_OK; 
    }

    get_os_type_2();
    if ( (strcmp(g_os_type2, MA_WINDOWSNT) == 0) || (strcmp(g_os_type2, MA_WINDOWSXP) == 0) )     {
        dwRes = RegOpenKeyEx(HKEY_LOCAL_MACHINE, MA_REG_NTCURRENTVERSION, 0, KEY_READ, &hKey);
        if( (dwRes == ERROR_SUCCESS) && (hKey != NULL)) {
			dwRes = RegQueryValueEx(hKey, MA_REG_PRODUCTID, NULL, &dwType, (LPBYTE)oemid_buf, &cbData);
			success = MA_TRUE;
			RegCloseKey(hKey);
		}
    }
    else {
        /* As registry access of OEM id is restricted on Win7 / W2k8 / Vista, use WMI to retrieve these values*/
        /* It can be access though only WMI */
        success = wmi_oemid(oemid_buf);
    }
    
    if(success)
		strncpy(os_oemid, oemid_buf, 63);
	else
        strncpy(os_oemid, MA_NA, 63);

    return MA_OK;   
}

void platform_type_get(){
    get_os_type_2();
    get_nt_type();
    /* MA_WINDOWSNT means NT 351, 40, 2K */
    if (strcmp(g_os_type2, MA_WINDOWSNT) == 0) 
    {
        if (strcmp(g_os_nt_type, MA_NTSERVER) == 0)
        {
            strncpy(g_platform_type, MA_PLATFORM_NT_SERVER, 64);
        }
        else if (strcmp(g_os_nt_type, MA_NTWORKSTATION) == 0)
        {
            strncpy(g_platform_type, MA_PLATFORM_NT_WORKSTATION, 64);
        }
    }
    /* Windows 2000 */
    else if (strcmp(g_os_type2, MA_WINDOWS2000) == 0) 
    {
        if (strcmp(g_os_nt_type, MA_NTSERVER) == 0)
        {
            strncpy(g_platform_type, MA_PLATFORM_WINDOWS2000_SERVER, 64);
        }
        else if (strcmp(g_os_nt_type, MA_NTWORKSTATION) == 0)
        {
            strncpy(g_platform_type, MA_PLATFORM_WINDOWS2000_PROFESSIONAL, 64);
        }
    }
    /* Windows XP */
    else if (strcmp(g_os_type2, MA_WINDOWSXP) == 0 ||
             strcmp(g_os_type2, MA_WINDOWS2003) == 0 ||
             strcmp(g_os_type2, MA_WINDOWS2003R2) == 0)
    {
        OSVERSIONINFOEX osvi;
        ma_bool_t bOsVersionInfoEx;

        /* Try calling GetVersionEx using the OSVERSIONINFOEX structure.
         If that fails, try using the OSVERSIONINFO structure. */

        ZeroMemory(&osvi, sizeof(OSVERSIONINFOEX));
        osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);

        bOsVersionInfoEx = GetVersionEx ((OSVERSIONINFO *) &osvi) ;

        if( bOsVersionInfoEx )
        {
            if ( osvi.wProductType == VER_NT_WORKSTATION )
            {
               if( osvi.wSuiteMask & VER_SUITE_PERSONAL )
                  strncpy(g_platform_type, MA_PLATFORM_WINDOWSXP_HOMEEDITION, 64);              
               else if (osvi.wSuiteMask & VER_SUITE_EMBEDDEDNT)
                  strncpy(g_platform_type, MA_PLATFORM_WINDOWSXP_EMBEDDED, 64);   
               else
                  strncpy(g_platform_type, MA_PLATFORM_WINDOWSXP_PROFESSIONAL, 64);
            }
            else
                  strncpy(g_platform_type, MA_PLATFORM_WINDOWSXP_SERVER, 64);
        }
        else
        {
            if (strcmp(g_os_nt_type, MA_NTSERVER) == 0)
            {
                strncpy(g_platform_type, MA_PLATFORM_WINDOWSXP_SERVER, 64);
            }
            else if (strcmp(g_os_nt_type, MA_NTWORKSTATION) == 0)
            {
                strncpy(g_platform_type, MA_PLATFORM_WINDOWSXP_PROFESSIONAL, 64);
            }
        }
    }
    else if (strcmp(g_os_type2, MA_WINDOWSVISTA) == 0)
    {
        strncpy(g_platform_type, MA_PLATFORM_WINDOWS_VISTA, 64);
    }
    else if (strcmp(g_os_type2, MA_WINDOWS2008) == 0 || strcmp(g_os_type2, MA_WINDOWS2008R2) == 0)
    {
        strncpy(g_platform_type, MA_PLATFORM_WINDOWS2008_SERVER, 64);
    }
    else if (strcmp(g_os_type2, MA_WINDOWS7) == 0)
    {
        strncpy(g_platform_type, MA_PLATFORM_WINDOWS7_WORKSTATION, 64);
    }
    else if (strcmp(g_os_type2, MA_WINDOWS8WRK) == 0)
    {
        strncpy(g_platform_type, MA_PLATFORM_WINDOWS8_WORKSTATION, 64);
    }
    else if (strcmp(g_os_type2, MA_WINDOWS8SRV) == 0)
    {
        strncpy(g_platform_type, MA_PLATFORM_WINDOWS8_SERVER, 64);
    }
    else if (strcmp(g_os_type2, MA_WINDOWS98) == 0)
    {
        strncpy(g_platform_type, MA_PLATFORM_WINDOWS98, 64);
    }
    else if (strcmp(g_os_type2, MA_WINDOWS95) == 0)
    {
        strncpy(g_platform_type, MA_PLATFORM_WINDOWS95, 64);
    }
    else if (strcmp(g_os_type2, MA_WINDOWSME) == 0)
    {
        strncpy(g_platform_type, MA_PLATFORM_WINDOWSME, 64);
    }
	else if (strcmp(g_os_type2, MA_WINDOWS81WRK) == 0)
	{
		strncpy(g_platform_type, MA_PLATFORM_WINDOWS81_WORKSTATION, 64);
	}
	else if (strcmp(g_os_type2, MA_WINDOWS81SRV) == 0)
	{
		strncpy(g_platform_type, MA_PLATFORM_WINDOWS81_SERVER, 64);
	}

    return ;
}

ma_error_t os_platform_type_get(char *os_type){
    get_os_type_2();
    strncpy(os_type, g_os_type2, 63);
    return MA_OK;
}

ma_bool_t wmi_smbios_uuid(char os_smbios_uuid[64]){
    #pragma comment(lib, "wbemuuid.lib ")
    ma_bool_t rc = MA_FALSE;
    IWbemLocator         *locator  = 0;
    IWbemServices        *services = 0;
    IEnumWbemClassObject *results  = 0;
    BSTR resource = SysAllocString(L"ROOT\\CIMV2");
    BSTR language = SysAllocString(L"WQL");
    BSTR query    = SysAllocString(L"SELECT UUID FROM Win32_ComputerSystemProduct");
    
    if(S_OK == CoCreateInstance(&CLSID_WbemLocator, 0, CLSCTX_INPROC_SERVER, &IID_IWbemLocator, (LPVOID *) &locator)){
        locator->lpVtbl->ConnectServer(locator, resource, (BSTR)NULL,(BSTR) NULL, (BSTR)NULL, 0,(BSTR) NULL, (IWbemContext*)NULL, &services);
        if(services){
			if(S_OK == CoSetProxyBlanket(services, RPC_C_AUTHN_DEFAULT, RPC_C_AUTHZ_DEFAULT, COLE_DEFAULT_PRINCIPAL, RPC_C_AUTHN_LEVEL_DEFAULT, 
				RPC_C_IMP_LEVEL_IMPERSONATE, COLE_DEFAULT_AUTHINFO, EOAC_DEFAULT )) {
				services->lpVtbl->ExecQuery(services, language, query, WBEM_FLAG_RETURN_IMMEDIATELY, 0, &results);
				if (results != NULL) {
					IWbemClassObject *result = 0;
					ULONG returnedCount = 0;
					while(results->lpVtbl->Next(results, WBEM_INFINITE, 1, &result, &returnedCount) == S_OK) {
						VARIANT name;
						VariantInit(&name);
						if(S_OK == result->lpVtbl->Get(result, L"UUID", 0, &name, 0, 0)){
							if(-1 == ma_wide_char_to_utf8(os_smbios_uuid, 63, name.bstrVal, wcsnlen(name.bstrVal, 64))) {
								MA_LOG(g_system_property_logger, MA_LOG_SEV_ERROR, "ma_wide_char_to_utf8 call failed for smbios uuid, setting to NA");
								strncpy(os_smbios_uuid, MA_NA, 3);
							}
							else
								rc = MA_TRUE;
							VariantClear(&name);
							result->lpVtbl->Release(result);
							break;
						}
						VariantClear(&name);
						result->lpVtbl->Release(result);
					}
					results->lpVtbl->Release(results);
				}
			}
			services->lpVtbl->Release(services);
        }
        locator->lpVtbl->Release(locator);
    }

    SysFreeString(query);
    SysFreeString(language);
    SysFreeString(resource);
    return rc;
}

ma_error_t os_smbios_uuid_get(char *os_smbios_uuid){
    static char	  os_smbios_uuid_store[64] = {0};
    static ma_bool_t success = MA_FALSE;

	/*Already scanned */
    if(success == MA_TRUE) {
        strncpy(os_smbios_uuid, os_smbios_uuid_store, 63);
        return MA_OK;
    }

	if(success = wmi_smbios_uuid(os_smbios_uuid_store)) 
		strncpy(os_smbios_uuid, os_smbios_uuid_store, 63);
	else
		strncpy(os_smbios_uuid, MA_NA, 4);
    return MA_OK;
}

ma_error_t os_platform_id_get(char *os_platform_id){
    char my_platform[32]={0};
    TCHAR *str = g_osvi.szCSDVersion;
    ma_int8_t    j    = 0;

    get_os_type_2();
    get_os_version_info_struct();
    
    while (str != NULL && j < (ma_int8_t)_tcslen(g_osvi.szCSDVersion)) {
        if (isdigit(*str)) {
            break;
        }
        else {
            j++;
            str++;
        }
    }

    _snprintf(my_platform, 32, "%s:%d:%d:%d", g_os_type2, g_osvi.dwMajorVersion, g_osvi.dwMinorVersion, _ttoi(str));
    strncpy(os_platform_id, my_platform, 31);
    return MA_OK;
}

ma_error_t os_platform_get(char *os_platform)
{
    platform_type_get();

    if (strcmp(g_platform_type, MA_PLATFORM_NT_SERVER) == 0)
    {
       strncpy(os_platform, MA_NTSERVER, 31);
    }
    else if (strcmp(g_platform_type, MA_PLATFORM_NT_WORKSTATION) == 0)
    {
        strncpy(os_platform, MA_NTWORKSTATION, 31);
    }
    else if (strcmp(g_platform_type, MA_PLATFORM_WINDOWS95) == 0)
    {
        strncpy(os_platform, MA_WINDOWS95, 31);
    }
    else if (strcmp(g_platform_type, MA_PLATFORM_WINDOWS98) == 0)
    {
        strncpy(os_platform, MA_WINDOWS98, 31);
    }
    else if (strcmp(g_platform_type, MA_PLATFORM_WINDOWSME) == 0)
    {
        strncpy(os_platform, MA_WINDOWSME, 31);
    }
    else if (strcmp(g_platform_type, MA_PLATFORM_WINDOWS_VISTA) == 0)
    {
        strncpy(os_platform, MA_NTWORKSTATION, 31);
    }
    else if (strcmp(g_platform_type, MA_PLATFORM_WINDOWS2008_SERVER) == 0 ||
             strcmp(g_platform_type, MA_PLATFORM_WINDOWS8_SERVER) == 0)
    {
        strncpy(os_platform, MA_NTSERVER, 31);
    }
    else if (strcmp(g_platform_type, MA_PLATFORM_WINDOWS7_WORKSTATION) == 0 ||
             strcmp(g_platform_type, MA_PLATFORM_WINDOWS8_WORKSTATION) == 0)
    {
        strncpy(os_platform, MA_NTWORKSTATION, 31);
    }
    else if (strcmp(g_platform_type, MA_PLATFORM_WINDOWS2000_PROFESSIONAL) == 0)
    {
        strncpy(os_platform, MA_PROFESSIONAL, 31);
    }
    else if (strcmp(g_platform_type, MA_PLATFORM_WINDOWS2000_SERVER) == 0)
    {
        strncpy(os_platform, MA_NTSERVER, 31);
    }
    else if (strcmp(g_platform_type, MA_PLATFORM_WINDOWS2000_ADVANCEDSERVER) == 0)
    {
        strncpy(os_platform, MA_ADVANCESERVER, 31);
    }
    else if (strcmp(g_platform_type, MA_PLATFORM_WINDOWS2000_DATACENTER) == 0)
    {
        strncpy(os_platform, MA_DATACENTER, 31);
    }
    else if (strcmp(g_platform_type, MA_PLATFORM_WINDOWSXP_PROFESSIONAL) == 0)
    {
        strncpy(os_platform, MA_PROFESSIONAL, 31);
    }
    else if (strcmp(g_platform_type, MA_PLATFORM_WINDOWSXP_EMBEDDED ) == 0)
    {
        strncpy(os_platform, MA_EMBEDDED, 31);
    }
    else if (strcmp(g_platform_type, MA_PLATFORM_WINDOWSXP_SERVER) == 0)
    {
        strncpy(os_platform, MA_NTSERVER, 31);
    }
    else if (strcmp(g_platform_type, MA_PLATFORM_WINDOWSXP_HOMEEDITION) == 0)
    {
        strncpy(os_platform, MA_PERSONAL, 31);
    }
	else if (strcmp(g_platform_type, MA_PLATFORM_WINDOWS81_WORKSTATION) == 0)
	{
		strncpy(os_platform, MA_NTWORKSTATION, 31);
	}
	else if (strcmp(g_platform_type, MA_PLATFORM_WINDOWS81_SERVER) == 0)
	{
		strncpy(os_platform, MA_NTSERVER, 31);
	}
    else
    {
        strncpy(os_platform, MA_UNKNOWN, 31);
    }
    return MA_OK;
}

ma_error_t system_property_os_get (ma_os_system_property_t *os_info){
    memset(os_info, 0, sizeof(ma_os_system_property_t));
    os_version(os_info);
    os_bit_mode_get(os_info->is64bit);
	os_oemid_get(os_info->oemid);
    os_platform_get(os_info->platform);
    os_platform_id_get(os_info->platformid);
    os_platform_type_get(os_info->type);
    os_smbios_uuid_get(os_info->smbios_uuid);
    os_info->is_available = MA_TRUE;
    return MA_OK;
}

static ma_bool_t is_windows_vista_above(){
	static ma_bool_t calculated_once = MA_FALSE;
	static ma_bool_t windows_vista_above = MA_FALSE;
	if(!calculated_once) {
		OSVERSIONINFOEX versionInfo = {0};
		DWORDLONG conditionMask = {0};
		versionInfo.dwOSVersionInfoSize = sizeof(versionInfo);
		versionInfo.dwMajorVersion = 6;
		versionInfo.dwMinorVersion = 0;
		versionInfo.dwPlatformId = VER_PLATFORM_WIN32_NT;
		VER_SET_CONDITION(conditionMask, VER_MAJORVERSION, VER_GREATER_EQUAL);
		VER_SET_CONDITION(conditionMask, VER_MINORVERSION, VER_GREATER_EQUAL);
		VER_SET_CONDITION(conditionMask, VER_PLATFORMID, VER_EQUAL);

		windows_vista_above = (VerifyVersionInfo(&versionInfo, VER_MAJORVERSION | VER_MINORVERSION | VER_PLATFORMID, conditionMask) == TRUE);
		calculated_once = MA_TRUE;
	}
	return windows_vista_above;
}

static ma_bool_t is_windows8_above(){
	static ma_bool_t calculated_once = MA_FALSE;
	static ma_bool_t windows8_above = MA_FALSE;
	if(!calculated_once) {
		OSVERSIONINFOEX versionInfo = {0};
		DWORDLONG conditionMask = {0};
		versionInfo.dwOSVersionInfoSize = sizeof(versionInfo);
		versionInfo.dwMajorVersion = 6;
		versionInfo.dwMinorVersion = 2;
		versionInfo.dwPlatformId = VER_PLATFORM_WIN32_NT;
		VER_SET_CONDITION(conditionMask, VER_MAJORVERSION, VER_EQUAL);
		/*This should be verified in a later version of windows if the issue of dwm users coming persists
		then the condition for MINOR VERSION should be set to VER_GREATER_EQUAL*/
		VER_SET_CONDITION(conditionMask, VER_MINORVERSION, VER_GREATER_EQUAL);
		VER_SET_CONDITION(conditionMask, VER_PLATFORMID, VER_EQUAL);

		windows8_above = (VerifyVersionInfo(&versionInfo, VER_MAJORVERSION | VER_MINORVERSION | VER_PLATFORMID, conditionMask) == TRUE);
		calculated_once = MA_TRUE;
	}
	return windows8_above;
}

static ma_bool_t is_unique_sid(ma_loggedon_users_list_t *user_list, PSID sid) {
	if(user_list) {
		ma_loggedon_users_info_node_t *node =NULL;
		MA_SLIST_FOREACH(user_list, user_info, node) {
			if(node && (node->sid == sid)) {
				return MA_FALSE;
			}
		}
	}
	return MA_TRUE;
}

static ma_bool_t is_unique_name(ma_loggedon_users_list_t *user_list, char *name) {
	if(user_list && name) {
		ma_loggedon_users_info_node_t *node =NULL;
		MA_SLIST_FOREACH(user_list, user_info, node) {
			if(node && node->name && !(strcmp(node->name, name))) {
				return MA_FALSE;
			}
		}
	}
	return MA_TRUE;
}

static ma_bool_t is_DWM_user(char *name) {
	if(name) {
		/*compare all number followed by "DWM-" i.e. DWM-0, DWM-1,................*/
		if((!strncmp(name, "DWM-0", 5)) || (!strncmp(name, "DWM-1", 5)) || (!strncmp(name, "DWM-2", 5)) || (!strncmp(name, "DWM-3", 5))
			|| (!strncmp(name, "DWM-4", 5)) || (!strncmp(name, "DWM-5", 5)) || (!strncmp(name, "DWM-6", 5)) || (!strncmp(name, "DWM-7", 5))
			|| (!strncmp(name, "DWM-8", 5)) || (!strncmp(name, "DWM-9", 5)))
			return MA_TRUE;
	}
	return MA_FALSE;
}

static ma_error_t user_info_list_release(ma_loggedon_users_info_node_t *user_info_node) {
	if(user_info_node) {
		if(user_info_node->name)
			free(user_info_node->name);
		free(user_info_node);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

/* As per ePO requirement, user name list should be less that 128 charactres, if user list exceeding 124 characters, then add "..." and terminate list string */
ma_error_t copy_list_to_buffer(ma_loggedon_users_list_t *user_list, char user_name_list[], size_t length) {
	if(user_list) {
		char *last_chr = NULL;
		ma_bool_t user_count_exceed_limit = MA_FALSE;
		ma_loggedon_users_info_node_t *node =NULL;
		MA_SLIST_FOREACH(user_list, user_info, node) {
			if(node && node->name) {
				if((strlen(user_name_list) + strlen(node->name)) > (MA_MAX_USER_LIST_LEN - 4)) {
					user_count_exceed_limit = MA_TRUE;
				}
				else {
					strncat(user_name_list, node->name, length-1);
					strncat(user_name_list, ",", length-1);
				}
			}
		}
		if(user_count_exceed_limit){
			/* Add "..." after "," */
			size_t len =  strlen(user_name_list);
			memset( (user_name_list + len), '.', (MA_MAX_USER_LIST_LEN - len - 1));
		}
		else if(last_chr = strrchr(user_name_list, ',')) /*removing last coma*/
			*last_chr = '\0';
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

static ma_error_t user_list_enqueue(ma_loggedon_users_list_t *user_list, ma_loggedon_users_info_node_t *users_info_node, ma_bool_t check_for_sid) {
	if(user_list && users_info_node && users_info_node->name) {
		/*check for DWM-[1234567890] users */
		if( (is_windows8_above() && (is_DWM_user(users_info_node->name))) ||
			(!is_unique_name(user_list, users_info_node->name)) ||
			((check_for_sid) && (!is_unique_sid(user_list, users_info_node->sid))) ){
				user_info_list_release(users_info_node);
				return MA_OK;
		}
		
		/*Add in list */
		MA_SLIST_PUSH_BACK(user_list, user_info, users_info_node);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

static ma_error_t loggedon_users_lsa_get(char user_name_list[], size_t length){
	PLUID sessions = 0;
    ULONG sessioncount = 0;
    int i;
	ma_loggedon_users_list_t *user_info_list = (ma_loggedon_users_list_t*)calloc(1, sizeof(ma_loggedon_users_list_t));
	ma_bool_t windowsvista_above = is_windows_vista_above();
	NTSTATUS ret;
    ret = LsaEnumerateLogonSessions(&sessioncount, &sessions);

	if (ret != MA_NT_STATUS_SUCCESS) {
        strncpy(user_name_list, MA_NA, length-1);
        MA_LOG(g_system_property_logger, MA_LOG_SEV_ERROR, "LsaEnumerateLogonSessions call failed, but setting user_name to N/A");
		free(user_info_list);
        return MA_OK;
    }
		
	MA_SLIST_INIT(user_info_list, user_info);
	for (i = 0; i < (int)(sessioncount); ++i) {
		SECURITY_LOGON_SESSION_DATA* session_data = 0;
		ret = LsaGetLogonSessionData(&sessions[i], &session_data);
		if((MA_NT_STATUS_SUCCESS == ret) && (NULL != session_data)) {
			if(((session_data->LogonType == Interactive) || (session_data->LogonType == RemoteInteractive)
				|| (session_data->LogonType == CachedInteractive) || (session_data->LogonType == CachedRemoteInteractive)) 
				&& (!(windowsvista_above && session_data->Session == 0)) && (session_data->UserName.Buffer)) {
					char *name = (char*) calloc(1, session_data->UserName.Length + 1);
					if(-1 != ma_wide_char_to_utf8(name, session_data->UserName.Length, session_data->UserName.Buffer, session_data->UserName.Length)) {
						if(strlen(name)) { /*some time getting empty name*/
							ma_loggedon_users_info_node_t *node =(ma_loggedon_users_info_node_t*)calloc(1, sizeof(ma_loggedon_users_info_node_t));
							node->name = name;
							node->sid = session_data->Sid;
							user_list_enqueue(user_info_list, node, MA_TRUE);
						}
						else
							free(name);
					}
					else
						free(name);
			}
			LsaFreeReturnBuffer(session_data);
		}
	}
	LsaFreeReturnBuffer(sessions);

	copy_list_to_buffer(user_info_list, user_name_list, length);
	MA_SLIST_CLEAR(user_info_list, user_info, ma_loggedon_users_info_node_t, user_info_list_release);
	free(user_info_list);
	return MA_OK;
}

static ma_error_t loggedon_users_wts_get(char user_name_list[], size_t length){
#if 0
	PWTS_SESSION_INFO pSessionInfo = NULL ;
	DWORD size = 0;
	if(WTSEnumerateSessions( WTS_CURRENT_SERVER_HANDLE, 0,	1, &pSessionInfo, &size)) {
		DWORD i;
		ma_loggedon_users_list_t *user_info_list = (ma_loggedon_users_list_t*)calloc(1, sizeof(ma_loggedon_users_list_t));
		MA_SLIST_INIT(user_info_list, user_info);
		for (i=0; i < size; i++) {
			DWORD sessionId = pSessionInfo[i].SessionId;
			wchar_t *pointerInfo = NULL;
			DWORD len = 0;
			if (WTSQuerySessionInformation (WTS_CURRENT_SERVER_HANDLE, sessionId, WTSUserName, &pointerInfo, &len)) {
                if(wcslen(pointerInfo)) {/*some time getting empty name*/
					size_t needed_size = ma_wide_char_to_utf8(NULL, 0, pointerInfo, 0);
					if(needed_size > 0 && needed_size <= len) {
						char *name = (char*) calloc(needed_size+1, sizeof(char)); 
						if(name) {
							if(-1 != ma_wide_char_to_utf8(name, needed_size, pointerInfo, wcslen(pointerInfo))) {
								ma_loggedon_users_info_node_t *node =(ma_loggedon_users_info_node_t*)calloc(1, sizeof(ma_loggedon_users_info_node_t));
								node->name = name;
								user_list_enqueue(user_info_list, node, MA_FALSE);
							}
							else
								free(name);                       
						}
					}
				}
				WTSFreeMemory(pointerInfo);
			}
			else {
				WTSFreeMemory(pSessionInfo);
				MA_SLIST_CLEAR(user_info_list, user_info, ma_loggedon_users_info_node_t, user_info_list_release);
				free(user_info_list);
				return MA_ERROR_APIFAILED;
			}
		}			
		WTSFreeMemory(pSessionInfo);
		copy_list_to_buffer(user_info_list, user_name_list, length);
		MA_SLIST_CLEAR(user_info_list, user_info, ma_loggedon_users_info_node_t, user_info_list_release);
		free(user_info_list);
		return MA_OK;
	}
	return MA_ERROR_APIFAILED;
#endif
    return loggedon_users_lsa_get(user_name_list, length);
}

ma_error_t system_property_user_name_get(char user_name_list[], size_t length){
	ma_error_t rc;
	if(MA_OK != (rc = loggedon_users_wts_get(user_name_list, length))) {
		/* if WTS is not ready during system startup, WTS function may fail, if it happens, we will call Lsa functions 
		   to get logged user info (from top of the lsa list) */
		rc = loggedon_users_lsa_get(user_name_list, length);
	}

	return rc;
}

ma_error_t system_property_is_portable_get(char is_portable[], size_t length){
    static ma_bool_t run_once = MA_FALSE;
    static char portability[4] = {0};
    if(!run_once) {
        SYSTEM_POWER_STATUS power_status;
        power_status.BatteryFlag = 255;
        if (GetSystemPowerStatus (&power_status)) {
            /* Battery Flag:
                0 = not being charged and the battery capacity is between low and high
                1 = High�the battery capacity is at more than 66 percent
                2 = Low�the battery capacity is at less than 33 percent
                4 = Critical�the battery capacity is at less than five percent
                8 = Charging
                128 = No system battery
                255 = Unknown status�unable to read the battery flag information */
            if (power_status.BatteryFlag == 128) {
                /* No system battery */
                strncpy(portability, "0", 1);
                
            }
            else if (power_status.BatteryFlag != 255) {
                /* Not no battery and not unknown, therefore must have battery. */
                strncpy(portability, "1", 1);
            }
            else {
                /* Otherwise leave portability == unknown */
                strncpy(portability, MA_NA, 3);
            }
            run_once = MA_TRUE;
        }
    }
    strncpy(is_portable, portability, length);
    return MA_OK;
}

ma_error_t system_property_domain_name_get(char domain_name[], size_t length){
    DWORD dw_level = 102;
    LPWKSTA_INFO_102 p_buf = NULL;	
    NET_API_STATUS nwstatus;
    LPWSTR server_name = NULL;
    size_t wlen = 0, clen = 0;  

    nwstatus = NetWkstaGetInfo(server_name,
                                dw_level,
                                (LPBYTE *)&p_buf);
    if (nwstatus == 0) {
        ma_wide_char_to_utf8(domain_name, length-1, p_buf->wki102_langroup, wcsnlen(p_buf->wki102_langroup, length-1));
        NetApiBufferFree(p_buf);
    }
    else
        strncpy(domain_name, MA_NA, length-1);
    return MA_OK;
}

ma_error_t system_property_host_name_get(char host_name[], size_t length){
    char host_name_buff[MA_MAX_PATH_LEN] = {0};
    if (gethostname(host_name_buff, MA_MAX_PATH_LEN)) {
        MA_LOG(g_system_property_logger, MA_LOG_SEV_ERROR, "gethostname api failed with return code %d, setting it to NA ", WSAGetLastError());
        strncpy(host_name, MA_NA, length-1);
        return MA_OK;
    }
    strncpy(host_name, host_name_buff, length-1);
    return MA_OK;
}

ma_error_t ma_fqdn_get(char *ip_addr, char fqdn[], size_t length){
    struct addrinfo hints = {0};
    struct addrinfo *res = NULL;
    char addr[MA_MAX_PATH_LEN] = {0};
    char temp[MA_MAX_PATH_LEN] = {0};
	ma_error_t rc = MA_ERROR_UNEXPECTED;

    hints.ai_family   = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    
	if(!getaddrinfo(ip_addr, NULL, &hints, &res)){
		/*We need FQDN too so  don't give "| NI_NOFQDN" */
		if((!getnameinfo( res->ai_addr, (socklen_t)res->ai_addrlen, addr, MA_MAX_PATH_LEN, NULL , 0 , NI_NAMEREQD )) && (strlen (addr)) ){
			strncpy(fqdn, addr, length-1);
			rc = MA_OK;
		}
		else
			MA_LOG(g_system_property_logger, MA_LOG_SEV_ERROR, "getnameinfo api failed with return code %d ", WSAGetLastError());

		freeaddrinfo(res);
	}
	else
		MA_LOG(g_system_property_logger, MA_LOG_SEV_ERROR, "getaddrinfo api failed with return code %d. ", WSAGetLastError());

	if(MA_OK != rc){
		system_property_host_name_get(fqdn, length);
		system_property_domain_name_get(temp, MA_MAX_PATH_LEN);
		if(strlen(temp) && strcmp(temp, MA_NA)){
			strncat(fqdn, ".",  length - strlen(fqdn) - 1);
			strncat(fqdn, temp, length - strlen(fqdn) - 1);
		}
	}
    return MA_OK;
}


ma_error_t system_property_ipx_addr_get(char ipx_addr[], size_t length){
    /* IPX address family is not supported on Windows Vista and later, so hard code it to NA */
    strncpy(ipx_addr, MA_NA, length);
    return MA_OK;
}


ma_error_t system_property_computer_name_get(char computer_name[], size_t length) {
    DWORD dw_level = 102;
    LPWKSTA_INFO_102 p_buf = NULL;	
    NET_API_STATUS nwstatus;
    LPWSTR server_name = NULL;

    nwstatus = NetWkstaGetInfo(server_name,
                                dw_level,
                                (LPBYTE *)&p_buf);
    if (nwstatus == 0)
    {
        if(-1 == ma_wide_char_to_utf8(computer_name, length-1, p_buf->wki102_computername, wcsnlen(p_buf->wki102_computername, length-1))){
			 MA_LOG(g_system_property_logger, MA_LOG_SEV_ERROR, "ma_wide_char_to_utf8 api for computer name failed, setting into NA");
             strncpy(computer_name, MA_NA, length-1);
		}
		NetApiBufferFree(p_buf);
    }
    else
        strncpy(computer_name, MA_NA, length);

    return MA_OK;
}

ma_error_t system_property_computer_description_get(char computer_desc[], size_t length) {
    HKEY    h_key = NULL;
    DWORD   dw_res , dw_size;
    TCHAR desc[MAX_PATH]={0};
    dw_res = RegOpenKeyEx(HKEY_LOCAL_MACHINE, MA_REG_LANMANSERVER, 0, KEY_READ, &h_key);
    if (dw_res == ERROR_SUCCESS)
    {
        dw_size = (DWORD) MAX_PATH;
        dw_res = RegQueryValueEx(h_key, _T("srvcomment"), NULL, NULL, (LPBYTE) desc, &dw_size);
        RegCloseKey(h_key);
        if( dw_res == ERROR_SUCCESS )
        {
            if(-1 == ma_wide_char_to_utf8(computer_desc, length-1, desc, wcsnlen(desc, length-1)))
            {
                MA_LOG(g_system_property_logger, MA_LOG_SEV_ERROR, "ma_wide_char_to_utf8 api for computer description failed, setting into NA");
                strncpy(computer_desc, MA_NA, length-1);
            }
            return MA_OK;
        }
    }
    
	strncpy(computer_desc, MA_NA, length-1);
	return MA_OK;
}

ma_error_t system_property_time_zone_info_get(char time_zone[], size_t length){
    DWORD dw_res=0;
    TIME_ZONE_INFORMATION tzinfo;
    dw_res = GetTimeZoneInformation(&tzinfo);

    if ( dw_res != TIME_ZONE_ID_INVALID ) {
		if(-1 == ma_wide_char_to_utf8(time_zone, length-1, tzinfo.StandardName, wcslen(tzinfo.StandardName))) {
			MA_LOG(g_system_property_logger, MA_LOG_SEV_ERROR, "ma_wide_char_to_utf8 api for system time zone failed, setting into NA");
			strncpy(time_zone, MA_NA, length-1);
		}
    }
    else {
        strncpy(time_zone, MA_NA, length-1);
    }
    return MA_OK;
}

ma_error_t system_property_time_zone_bias_get(char time_zone_bios[], size_t length){
    TIME_ZONE_INFORMATION time_zone = { 0 };
	LONG  bias = 0;

	DWORD dw_status = GetTimeZoneInformation(&time_zone);
    if (dw_status != TIME_ZONE_ID_INVALID) {
		bias = time_zone.Bias;
		if (dw_status == TIME_ZONE_ID_DAYLIGHT)	{
			bias += time_zone.DaylightBias;
		}
    }
	_snprintf(time_zone_bios, length-1, "%ld", bias);
  
    return MA_OK;
}

ma_error_t system_property_lang_code_get(char code[], size_t length) {
    char  lang_id_arr[16] = { 0 };
    ma_int8_t  i = 0;
    LANGID lang_id      = GetUserDefaultLangID();
    _snprintf(code, length, "%04x", lang_id);
    return MA_OK;
}

ma_error_t system_property_lang_id_get(char language[], size_t length){
#if 0
	/* ePO is expecting code instead of name */
	char  lang_id_arr[16] = { 0 };
    ma_int8_t  i = 0;
    LANGID lang_id      = GetUserDefaultLangID();
    _snprintf(lang_id_arr, 16, "%04x", lang_id);

    while (lang_id_table[i].code != NULL)
    {
        if (stricmp(lang_id_table[i].code, lang_id_arr) == 0)
        {
            strncpy(language, lang_id_table[i].name, length-1);
            return MA_OK;
        }
        i++;
    }
    strncpy(language, lang_id_arr, length-1);
    return MA_OK;
#endif
	return system_property_lang_code_get(language, length);
}

ma_error_t system_property_system_time_get(char system_time[], size_t length){
    char info[64] = {0};
    SYSTEMTIME  st_date_time = {0};
    GetLocalTime(&st_date_time);
    _snprintf(info, 64, "%02d/%02d/%04d %02d:%02d:%02d", 
        st_date_time.wMonth,
        st_date_time.wDay,
        st_date_time.wYear,
        st_date_time.wHour,
        st_date_time.wMinute,
        st_date_time.wSecond);
    strncpy(system_time, info, length-1);
    return MA_OK;
}


/*If cluster state is running(1), then all field should be non-null (otherwise caller will be crash). */
static void cluster_info_sanity(ma_cluster_system_property_t *cluster_info){
    MA_LOG(g_system_property_logger, MA_LOG_SEV_TRACE, "Cluster state is: %s", cluster_info->state) ;
    if(atoi(cluster_info->state) == 1 ){
        if(!cluster_info->host)
            cluster_info->host = strdup(MA_NA);
        if(!cluster_info->ipaddr)
            cluster_info->ipaddr = strdup(MA_NA);
        if(!cluster_info->name)
            cluster_info->name = strdup(MA_NA);
        if(!cluster_info->quorum_resource_path)
            cluster_info->quorum_resource_path = strdup(MA_NA);
		if(!cluster_info->nodes)
            cluster_info->nodes = strdup(MA_NA);
		/* TBD: for testing purpose only, clean later*/
		MA_LOG(g_system_property_logger, MA_LOG_SEV_INFO, "MA_PROPERTY_CLUSTER_IP_ADDR [%s]", cluster_info->ipaddr) ;
		MA_LOG(g_system_property_logger, MA_LOG_SEV_INFO, "MA_PROPERTY_CLUSTER_NAME [%s]", cluster_info->name) ;
		MA_LOG(g_system_property_logger, MA_LOG_SEV_INFO, "MA_PROPERTY_CLUSTER_HOST [%s]", cluster_info->host) ;
		MA_LOG(g_system_property_logger, MA_LOG_SEV_INFO, "MA_PROPERTY_CLUSTER_QUORUM_PATH [%s]", cluster_info->quorum_resource_path) ;
		MA_LOG(g_system_property_logger, MA_LOG_SEV_INFO, "MA_PROPERTY_CLUSTER_NODES [%s]", cluster_info->nodes) ;
    }
}

static HCLUSTER local_cluster_open(char cluster_state[4]) {
    HCLUSTER cluster_handle	= NULL;
    DWORD    cluster_name_len     = MAX_COMPUTERNAME_LENGTH + 1;
    LPWSTR   cluster_name = (LPWSTR)calloc(1, cluster_name_len * sizeof(WCHAR));
    if (cluster_name != NULL){
        if (GetComputerName(cluster_name, &cluster_name_len)){
            DWORD   cstate    = 0;
            if (ERROR_SUCCESS == GetNodeClusterState(cluster_name, &cstate)) {
                if (ClusterStateNotInstalled == cstate) {
                    strcpy(cluster_state, "0");
                }
                else if (ClusterStateRunning == cstate) {
                    strcpy(cluster_state, "1");
                }
                else if (ClusterStateNotRunning == cstate) {
                    strcpy(cluster_state, "2");
                }
                else if ( ClusterStateNotConfigured== cstate) {
                    strcpy(cluster_state, "3");
                }
            }
            else {
                strcpy(cluster_state, "4"); 
            }

            cluster_handle = OpenCluster(NULL);
        }

        free(cluster_name);
    }
    else
        strcpy(cluster_state, "4"); 

    return cluster_handle;
}

static ma_error_t cluster_quorum_resource_path_get(HCLUSTER cluster_handle, char **quorum_resource_path){
	if(cluster_handle && quorum_resource_path){
		wchar_t resource_name[MAX_PATH] = { 0 };
		DWORD   resource_name_len          = MAX_PATH;
		wchar_t device_name[MAX_PATH]   = { 0 };
		DWORD   device_name_len            = MAX_PATH;
		DWORD   quorum_size       = 0;

		DWORD dwRet = GetClusterQuorumResource(cluster_handle, resource_name, &resource_name_len,
												device_name, &device_name_len, &quorum_size);
		if (ERROR_SUCCESS == dwRet) {
			if (!device_name_len) {
				*quorum_resource_path = strdup(MA_NA);
			}
			else{
				*quorum_resource_path = (char*)calloc(1, _tcslen(device_name) * sizeof(wchar_t) + 1);
				if(-1 == ma_wide_char_to_utf8(*quorum_resource_path, (_tcslen(device_name) * sizeof(wchar_t)), device_name, wcslen(device_name))) {
					MA_LOG(g_system_property_logger, MA_LOG_SEV_ERROR, "ma_wide_char_to_utf8 api for cluster node name failed, setting into NA");
					free(*quorum_resource_path);
					*quorum_resource_path = strdup(MA_NA);
				}
			}
		}
		else
			*quorum_resource_path = strdup(MA_NA);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

static ma_error_t cluster_name_get(HCLUSTER cluster_handle, char **cluster_name){
   if(cluster_handle && cluster_name){
	   wchar_t cluster_name_buff[MAX_PATH]   = { 0 };
	   DWORD   cluster_name_len            = MAX_PATH;
	   if (ERROR_SUCCESS == GetClusterInformation(cluster_handle, cluster_name_buff, &cluster_name_len, 0)) {
		   *cluster_name = (char*)calloc(1, _tcslen(cluster_name_buff) * sizeof(wchar_t) + 1);
		   if(-1 == ma_wide_char_to_utf8(*cluster_name, (_tcslen(cluster_name_buff) * sizeof(wchar_t)), cluster_name_buff, wcslen(cluster_name_buff))) {
			   MA_LOG(g_system_property_logger, MA_LOG_SEV_ERROR, "ma_wide_char_to_utf8 api for cluster name failed, setting into NA");
			   free(*cluster_name);				
			   *cluster_name = strdup(MA_NA);
		   }
	   }
	   else{
		   *cluster_name = strdup(MA_NA);
	   }
	   return MA_OK;
   }
   return MA_ERROR_INVALIDARG;
}

static ma_bool_t cluster_info_by_cluster_database_get(HCLUSTER cluster_handle, ma_cluster_system_property_t *cluster_info){
	ma_bool_t attempt = MA_FALSE;
	HKEY hkey = GetClusterKey(cluster_handle, GENERIC_ALL);
    if(hkey){
        HKEY hk_result = NULL;
        LONG ret   = ClusterRegOpenKey(hkey, L"Resources", GENERIC_ALL, &hk_result);         
        if (hk_result) {
            wchar_t  name[MAX_PATH];
            DWORD    name_len = MAX_PATH;
            DWORD    index;
            for (index = 0; ; index++) {
				FILETIME ftLastWriteTime;
				name_len   = MAX_PATH;
                name[0] = 0;
                ret    = ClusterRegEnumKey(hk_result, index, name, &name_len, &ftLastWriteTime);
                if ((ERROR_NO_MORE_ITEMS == ret) || (ERROR_SUCCESS != ret)) {
                    break;
                }
                else {
                    HKEY hk_result1 = NULL;
                    ret = ClusterRegOpenKey(hk_result, name, GENERIC_ALL, &hk_result1);
                    if (hk_result1) {
                        DWORD   type          = 0;
                        wchar_t data[MAX_PATH] = { 0 };
                        DWORD   data_len          = MAX_PATH * sizeof(wchar_t);
                        ret = ClusterRegQueryValue(hk_result1, L"CoreCurrentName", &type, (PBYTE)data, &data_len);
                        if (ret == ERROR_SUCCESS) {
                            ret = ClusterRegQueryValue(hk_result1, L"Name", &type, (PBYTE)data, &data_len);
                            if (ret == ERROR_SUCCESS) {
                                HRESOURCE hResource = OpenClusterResource(cluster_handle, data);
                                if (hResource) {
									wchar_t node_name[MAX_PATH] = { 0 };
									DWORD   node_name_len       = MAX_PATH;
									CLUSTER_RESOURCE_STATE ResState = GetClusterResourceState(hResource, node_name, &node_name_len, 0, 0);
									if (node_name_len){
										if(!cluster_info->host)
											cluster_info->host = (char*)calloc(1, wcslen(node_name) * sizeof(wchar_t) + 1);
										else
											cluster_info->host = (char*)realloc(cluster_info->host, strlen(cluster_info->host) + wcslen(node_name) * sizeof(wchar_t) + 1);

										if(-1 == ma_wide_char_to_utf8(cluster_info->host, (wcslen(node_name) * sizeof(wchar_t)), node_name, wcslen(node_name))) {
											MA_LOG(g_system_property_logger, MA_LOG_SEV_INFO, "ma_wide_char_to_utf8 api for ClusterNodeActingAsHostToCluster failed, setting into NA");
										}
									}
                                    CloseClusterResource(hResource);
                                    hResource = NULL;
                                }
                            }
                            data_len   = MAX_PATH * sizeof(wchar_t);
                            data[0] = 0;
                            ret = ClusterRegQueryValue(hk_result1, L"DependsOn", &type, (PBYTE)data, &data_len);
                            if (ret == ERROR_SUCCESS) {
                                HKEY hk_result2 = NULL;
                                wchar_t path[MAX_PATH] = { 0 };
                                wcscpy_s(path, MAX_PATH, data);
                                wsprintf(path, L"%s\\Parameters", data);		
                                ret = ClusterRegOpenKey(hk_result, path, GENERIC_ALL, &hk_result2);
                                if (hk_result2) {
                                    data_len = MAX_PATH * sizeof(wchar_t);
                                    ret = ClusterRegQueryValue(hk_result2, L"Address", &type, (PBYTE)data, &data_len);
                                    if (ret == ERROR_SUCCESS) {
                                        if(!cluster_info->ipaddr)
                                            cluster_info->ipaddr = (char*)calloc(1, wcslen(data) * sizeof(wchar_t) + 1);
										else
											cluster_info->ipaddr = (char*)realloc(cluster_info->ipaddr, strlen(cluster_info->ipaddr) + wcslen(data) * sizeof(wchar_t) + 1);
                                        if(-1 == ma_wide_char_to_utf8(cluster_info->ipaddr, (wcslen(data) * sizeof(wchar_t)), data, wcslen(data))) {
                                            MA_LOG(g_system_property_logger, MA_LOG_SEV_ERROR, "ma_wide_char_to_utf8 api for cluster ip address failed, setting into NA");
                                        }
										attempt = MA_TRUE;
									}
                                    ClusterRegCloseKey(hk_result2);                            /* Cluster/Resources/uuid keys/parameters */
                                }
                            }
                        }
                        ClusterRegCloseKey(hk_result1);                        /* Cluster/Resources/uuid keys */
                    }
                }
            }
            ClusterRegCloseKey(hk_result);            /* Cluster/Resources */
        }
        ClusterRegCloseKey(hkey);        /* Cluster key */
    }
	return attempt;
}

static void cluster_info_by_cluster_object_get(HCLUSTER cluster_handle, ma_bool_t attempt, ma_cluster_system_property_t *cluster_info){
	HCLUSENUM hEnum = NULL;
	DWORD cluster_enum   = CLUSTER_ENUM_RESOURCE | CLUSTER_ENUM_NODE;
	if(attempt)
		cluster_enum = CLUSTER_ENUM_NODE;
	hEnum = ClusterOpenEnum(cluster_handle, cluster_enum);
	if (hEnum)	{
		DWORD dwIndex     = 0;
		DWORD dwReturn    = ERROR_SUCCESS;
		const int nFieldWidth = 1532; /*epo Field width is 3000 but (1532 + 4) is more than enough for our purpose */
		size_t nTotalLen = 0;
		ma_bool_t bFill = MA_FALSE;
		wchar_t *szCombinedName = (wchar_t*)calloc(1, sizeof(wchar_t[1532 + 4])); /*L"..." */
		if (szCombinedName) {
			for (dwIndex = 0; ; dwIndex++) {
				DWORD   dwType           = 0;
				wchar_t szName[MAX_PATH] = { 0 };
				DWORD   cchName          = MAX_PATH;
				dwReturn = ClusterEnum(hEnum, dwIndex, &dwType, szName, &cchName);
				if(ERROR_SUCCESS == dwReturn) {
					if(dwType == CLUSTER_ENUM_NODE) {
						size_t nStrLength = wcslen(szName) + 2; /* 2 because considering  L";" */
						nTotalLen = wcslen(szCombinedName) + 1;
						if(((nFieldWidth - nTotalLen) > nStrLength)) {
							wcscat_s(szCombinedName, nFieldWidth, szName);
							wcscat_s(szCombinedName, nFieldWidth , L";");
							bFill = MA_TRUE;
						}
						else {
							bFill = MA_FALSE;
						}
					}
					else if (dwType == CLUSTER_ENUM_RESOURCE) {
						HRESOURCE hResource = OpenClusterResource(cluster_handle, szName);
						if (hResource) {
							wchar_t OutBuffer[MAX_PATH] = { 0 };
							DWORD   cbOutBufferSize     = MAX_PATH * sizeof(wchar_t);
							DWORD   cbBytesReturned     = 0;
							if (ERROR_SUCCESS == ClusterResourceControl( hResource,  0, CLUSCTL_RESOURCE_GET_RESOURCE_TYPE, NULL, 0,
																		OutBuffer,  cbOutBufferSize, &cbBytesReturned)) {
                                if ((0 == wcscmp(OutBuffer, L"IP Address")) && (0 == wcscmp(szName, L"Cluster IP Address"))) {
									wchar_t szNodeName[MAX_PATH] = { 0 };
                                    DWORD   cchNodeName          = MAX_PATH;
									cbOutBufferSize = 0;
                                    cbBytesReturned = 0;
                                    /*Get the required buffer size for data */
                                    if (ERROR_SUCCESS == ClusterResourceControl(hResource, 0, CLUSCTL_RESOURCE_GET_PRIVATE_PROPERTIES, 
																	NULL, 0, 0, cbOutBufferSize, &cbBytesReturned))	{
                                        if (cbBytesReturned) {
											LPVOID lpOutBuffer = calloc(1, cbBytesReturned * sizeof(wchar_t) + 1);
											cbOutBufferSize = cbBytesReturned;
											cbBytesReturned = 0;
                                            if (lpOutBuffer) {
                                                if (ERROR_SUCCESS == ClusterResourceControl( hResource, 0, CLUSCTL_RESOURCE_GET_PRIVATE_PROPERTIES, NULL, 0,
																								lpOutBuffer, cbOutBufferSize, &cbBytesReturned)) {
                                                    wchar_t *ppszPropertyValue = NULL;
                                                    ResUtilFindSzProperty((PVOID)lpOutBuffer, cbBytesReturned, L"Address", &ppszPropertyValue);
                                                    if (ppszPropertyValue) {
														if(!cluster_info->ipaddr){
															cluster_info->ipaddr = (char*)calloc(1, wcslen(ppszPropertyValue) * sizeof(wchar_t) + 1);
															if(-1 == ma_wide_char_to_utf8(cluster_info->ipaddr, (wcslen(ppszPropertyValue) * sizeof(wchar_t)), ppszPropertyValue, wcslen(ppszPropertyValue))) {
																MA_LOG(g_system_property_logger, MA_LOG_SEV_ERROR, "ma_wide_char_to_utf8 api for cluster ip address failed.");
															}
														}
                                                        LocalFree(ppszPropertyValue);
                                                    }
                                                }
                                                free(lpOutBuffer);
                                                lpOutBuffer = NULL;
                                            }
                                        }
                                    }
									GetClusterResourceState( hResource, szNodeName, &cchNodeName, 0, 0);
                                    if (cchNodeName) {
										if(!cluster_info->host){
											cluster_info->host = (char*)calloc(1, wcslen(szNodeName) * sizeof(wchar_t) + 1);
											if(-1 == ma_wide_char_to_utf8(cluster_info->host, (wcslen(szNodeName) * sizeof(wchar_t)), szNodeName, wcslen(szNodeName))) {
												MA_LOG(g_system_property_logger, MA_LOG_SEV_INFO, "ma_wide_char_to_utf8 api for ClusterNodeActingAsHostToCluster failed.");
											}
										}
                                    }
                                }
                             }
							CloseClusterResource(hResource);
						}
					}
				}
                else if (ERROR_NO_MORE_ITEMS == dwReturn) {
                    size_t length = wcslen(szCombinedName);
                    if (length ) {
						if(bFill) { 
							szCombinedName[length - 1] = 0;
						}
						else {
							wcscat_s(szCombinedName, nFieldWidth + 4 , L"...");
						}
						cluster_info->nodes = (char*)calloc(1, wcslen(szCombinedName) * sizeof(wchar_t) + 1);
						if(-1 == ma_wide_char_to_utf8(cluster_info->nodes, (wcslen(szCombinedName) * sizeof(wchar_t)), szCombinedName, wcslen(szCombinedName))) {
							MA_LOG(g_system_property_logger, MA_LOG_SEV_INFO, "ma_wide_char_to_utf8 api for ClusterNodes failed.");
						}
                    }
                    break;
                }
                else {
                    break;
                }
			}
			free(szCombinedName);
            szCombinedName = NULL;
		}
		ClusterCloseEnum(hEnum);
	}
}
		
ma_error_t system_property_cluster_get(ma_cluster_system_property_t *cluster_info){
    HCLUSTER cluster_handle; 
	system_property_cluster_release(cluster_info);
    memset(cluster_info, 0, sizeof(ma_cluster_system_property_t));
	cluster_handle = local_cluster_open(cluster_info->state);
    if(cluster_handle){
		ma_bool_t attempt = MA_FALSE;
		(void)cluster_quorum_resource_path_get(cluster_handle, &cluster_info->quorum_resource_path);
		(void)cluster_name_get(cluster_handle, &cluster_info->name);
		attempt = cluster_info_by_cluster_database_get(cluster_handle, cluster_info);
		(void)cluster_info_by_cluster_object_get(cluster_handle, attempt, cluster_info);
        CloseCluster(cluster_handle);
    }
    cluster_info_sanity(cluster_info);
	cluster_info->is_available = MA_TRUE;
    return MA_OK;
}

ma_error_t system_property_disk_get(ma_disk_system_property_t *disk_info){
    char total_disk_space_buff[32] = { 0 }, free_disk_space[32] = { 0 }, hard_dire[4]={0};
    typedef ma_bool_t (WINAPI * PROCGETDISKFREESPACEEX)(LPCSTR lpDirectoryName, PULARGE_INTEGER lpFreeBytesAvailable, PULARGE_INTEGER lpTotalNumberOfBytes, PULARGE_INTEGER lpTotalNumberOfFreeBytes);
    ma_int8_t no_hard_drvs = 0;
	ma_int8_t hard_drv_counter = 0;
    ma_int8_t i = 0;
    DWORD dw_drives = GetLogicalDrives();
    LONGLONG total_drive_space = 0;
    LONGLONG free_drive_space  = 0;
    ma_uint8_t drv_type;
    char drv_string[16];
    char drv_string_toprint[16];
    LONGLONG free_bytes_tocaller, total_bytes, free_bytes;
	if(disk_info->filesysteminfo)
		free(disk_info->filesysteminfo);
    memset(disk_info, 0, sizeof(ma_disk_system_property_t));
    /* calculate number of drives */
    for (i = 0; i < 26; i++)    // test from A: to Z:
    {
        if (dw_drives & (1 << i))
        {
            memset(drv_string, 0, 16 * sizeof(CHAR));
            _snprintf(drv_string, 15, "%c:\\\0", 'A' + i);
            drv_type = GetDriveTypeA(drv_string);
            if (drv_type == DRIVE_FIXED)    // Hard Drive
            {
                no_hard_drvs++;
            }
        }
    }

    if(no_hard_drvs){
        disk_info->filesysteminfo = (ma_file_system_property_t*)calloc(no_hard_drvs, sizeof(ma_file_system_property_t));
        if(!disk_info->filesysteminfo)
        {
            MA_LOG(g_system_property_logger, MA_LOG_SEV_ERROR, "memory allocation for filesysteminfo failed, setting it default zero file systems");
            strncpy(disk_info->total_filesystems, "0", 3);
            strncpy(disk_info->total_diskspace, "0", 31);
            strncpy(disk_info->free_diskspace, "0", 31);
            return MA_OK;
        }
        for (i = 0; (i < 26) && (hard_drv_counter < no_hard_drvs) ; i++)    /* test from A: to Z: */
        {
            if (dw_drives & (1 << i))
            {
                /* the driver number exists */
                memset(drv_string, 0, 16 * sizeof(CHAR));
                _snprintf(drv_string, 15, "%c:\\\0", 'A' + i);
                _snprintf(drv_string_toprint, 15, "%c", 'A' + i);
                drv_type = GetDriveTypeA(drv_string);
                if (drv_type == DRIVE_FIXED)    /* Hard Drive */
                {
                    ma_bool_t rc = GetDiskFreeSpaceExA(drv_string,
                                            (PULARGE_INTEGER)&free_bytes_tocaller,
                                            (PULARGE_INTEGER)&total_bytes,
                                            (PULARGE_INTEGER)&free_bytes);
                    
                    if(!rc){
                        DWORD dwSectorPerCluster, dwBytePerSector, dwFreeCluster, dwClusters;
                        GetDiskFreeSpaceA(drv_string, &dwSectorPerCluster, &dwBytePerSector,
                                          &dwFreeCluster, &dwClusters);
                        total_bytes = (LONGLONG)dwBytePerSector * dwSectorPerCluster * dwClusters;
                        free_bytes  = (LONGLONG)dwBytePerSector * dwSectorPerCluster * dwFreeCluster;
                    }

                    total_drive_space += total_bytes;
                    free_drive_space  += free_bytes;
                    _snprintf(total_disk_space_buff, 32, "%.2f", (double)(total_bytes / 1024 / 1024));
                    _snprintf(free_disk_space, 32, "%.2f", (double)(free_bytes / 1024 / 1024));
                
                    strncpy(disk_info->filesysteminfo[hard_drv_counter].drive_name, drv_string_toprint, 63);
                    strncpy(disk_info->filesysteminfo[hard_drv_counter].drive_totalspace, total_disk_space_buff, 31);
                    strncpy(disk_info->filesysteminfo[hard_drv_counter].drive_freespace, free_disk_space, 31);
                    hard_drv_counter++;
                }
            }
        }
    }

    _snprintf(total_disk_space_buff, 32, "%.2f", (double)(total_drive_space / 1024 / 1024));
    _snprintf(free_disk_space, 32,  "%.2f",  (double)(free_drive_space / 1024 / 1024));
    _snprintf(hard_dire, 4, "%d", hard_drv_counter);
    strncpy(disk_info->total_filesystems, hard_dire, 3);
    strncpy(disk_info->total_diskspace, total_disk_space_buff, 31);
    strncpy(disk_info->free_diskspace, free_disk_space, 31);
	disk_info->is_available = MA_TRUE;
    return MA_OK;
  }

MA_CPP(})

