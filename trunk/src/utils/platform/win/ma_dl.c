#include "ma/internal/utils/platform/ma_dl.h"
#include "ma/internal/utils/text/ma_utf8.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"

#include <Windows.h>


ma_error_t  ma_dl_open(const char *library_name, unsigned long flags ,  ma_lib_t *lib) {
    if(library_name && lib ) {
        int size_needed = -1 ;
        
        lib->handle =  NULL ;        
        lib->handle = LoadLibraryExA(library_name, NULL , flags ? flags : LOAD_WITH_ALTERED_SEARCH_PATH) ;
        
        return lib->handle ? MA_OK : (ma_error_t)GetLastError();
    }
    return MA_ERROR_INVALIDARG;
}

void ma_dl_close(ma_lib_t  *lib) {
    if(lib && lib->handle) {
        FreeLibrary((HMODULE)lib->handle) , lib->handle = NULL ;
    }
}

void *ma_dl_sym(ma_lib_t* lib , const char* symbol_name ) {
    if(lib && symbol_name ) {                  
         return (void*) GetProcAddress((HMODULE)lib->handle, symbol_name) ;
    }
    return NULL ;
}
