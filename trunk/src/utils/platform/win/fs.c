/* ex: ts=8 sw=4 sts=4 et
** -*- coding: utf-8 -*-
*/
#include "ma/internal/utils/portability/fs.h"
#include "ma/internal/utils/text/ma_utf8.h"
#include <direct.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>

static int ma_dirname_w(const wchar_t* path_in,
                        wchar_t* const buf_out,
                        const size_t size_out)
{
    errno_t err;
    size_t len;
    err = _wsplitpath_s(path_in,
                        NULL, 0,
                        buf_out, size_out,
                        NULL, 0,
                        NULL, 0);
    if (0 == err) {
        /*
           _splitpath returns the dir with the trailing \ intact, so remove
           it to make behavior consistent with other targets.
        */
        len = wcslen(buf_out);
        if (len > 1) {
            buf_out[len - 1] = L'\0';
        }
    }    
    return err;
}

int ma_dirname(const char* path_in, char* const buf_out, const size_t size_out)
{
    wchar_t* wpath;
    wchar_t* wout;
    int wpath_len;
    int path_len;
    int dir_len;
    int err = 0;

    path_len = strlen(path_in) + 1;
    wpath_len = ma_utf8_to_wide_char(NULL, 0, path_in, path_len);
    if (wpath_len > 0) {
        /*
           Since the dirname can't possibly be longer than the input path,
           use that length for the output buffer.  Trade space for time.

           Since malloc always returns a pointer that's safe for free(),
           and free() knows how to deal with NULL, we can just blindly
           free both pointers no matter the outcome of the mallocs.
        */
        wpath = (wchar_t *)malloc(sizeof(wchar_t) * wpath_len);
        wout = (wchar_t *)malloc(sizeof(wchar_t) * wpath_len);

        if (wpath && wout) {
            /*
               We already know this function will succeed due to the
               previous call, so we can ignore the output.
            */
            (void)ma_utf8_to_wide_char(wpath, wpath_len, path_in, path_len);
            err = ma_dirname_w(wpath, wout, wpath_len);
            if (0 == err) {
                dir_len = ma_wide_char_to_utf8(NULL, 0, wout, 0);
                if (dir_len > 0 && dir_len <= (int)size_out) {
                    (void)ma_wide_char_to_utf8(buf_out, size_out, wout, dir_len);
                } else if (dir_len > (int)size_out) {
                    err = -ENAMETOOLONG;
                } else {
                    err = -EINVAL;
                }
            }
        } else {
            err = -ENOMEM;
        }

        free(wpath);
        free(wout);
    } else {
        err = -EINVAL;
    }
    return err;
}

static int ma_mkpath_w(const wchar_t* path_in)
{
    int err = 0;
    wchar_t* dirname;
    size_t path_size = wcslen(path_in) + 1;

    dirname = (wchar_t *)malloc(sizeof(wchar_t) * path_size);
    if (dirname) {
        err = ma_dirname_w(path_in, dirname, path_size);
        if (0 == err && 0 != wcscmp(dirname, path_in)) {
            err = ma_mkpath_w(dirname);
            if (0 == err) {
                err = _wmkdir(path_in);
                if (err && EEXIST == errno) {
                    err = 0; /* It's okay if path elements already exist */
                }
            }
        }
        free(dirname);
    } else {
        err = -ENOMEM;
    }
    return err;
}

int ma_mkpath(const char* path_in)
{
    wchar_t* wpath;
    int wpath_len;
    int path_len;
    int err = 0;

    path_len = strlen(path_in) + 1;
    wpath_len = ma_utf8_to_wide_char(NULL, 0, path_in, path_len);
    if (wpath_len > 0) {
        wpath = (wchar_t *)malloc(sizeof(wchar_t) * wpath_len);
        if (wpath) {
            /* no need to check return code, we know it'll work */
            (void)ma_utf8_to_wide_char(wpath, wpath_len, path_in, path_len);
            err = ma_mkpath_w(wpath);
        }
        free(wpath); /* see previous comment about blind free()s */
    } else {
        err = -EINVAL;
    }
    return err;
}
