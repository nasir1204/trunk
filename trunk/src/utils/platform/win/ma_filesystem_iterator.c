#include "ma/internal/utils/platform/ma_sys_apis.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include "ma/internal/utils/filesystem/path.h"
#include "../path_private.h"
#include <Windows.h>

struct filesystem_context_s {
	HANDLE hFindHandle;
	WIN32_FIND_DATAA findfiledescriptor;
	ma_bool_t bPrevResult;
};

ma_error_t filesystem_context_create(filesystem_context_t **context) {
	if(context){
		*context = (filesystem_context_t *) calloc(1,sizeof(filesystem_context_t));
		if(!*context)
			return MA_ERROR_OUTOFMEMORY;
		(*context)->hFindHandle = INVALID_HANDLE_VALUE;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t filesystem_context_release(filesystem_context_t *context) {
	if(context){
		if(context->hFindHandle != INVALID_HANDLE_VALUE){
			FindClose(context->hFindHandle);
		}
		free(context);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_filesystem_iterator_get_next(ma_filesystem_iterator_t *iterator, ma_temp_buffer_t *buffer) {
	if(iterator && buffer) {
		filesystem_context_t *context = iterator->context;
		ma_error_t error = MA_ERROR_NO_MORE_ITEMS;
		if(context == NULL) {
			if(MA_OK == ( filesystem_context_create(&context))) {
				size_t len = strlen(iterator->path);
				ma_temp_buffer_t search_path;
				char *p_search_path = NULL;

				ma_temp_buffer_init(&search_path);
				ma_temp_buffer_reserve(&search_path, len + 3);
				if(iterator->path[len - 1] == '\\')
					form_path(&search_path,(const char *)iterator->path, "*", MA_FALSE);
				else
					form_path(&search_path,(const char *)iterator->path, "\\*", MA_FALSE);
				p_search_path = (char *) ma_temp_buffer_get(&search_path);
				free(iterator->path);
				iterator->path = strdup(p_search_path);
				ma_temp_buffer_uninit(&search_path);


				context->hFindHandle = FindFirstFileA(iterator->path, &context->findfiledescriptor);
				if(context->hFindHandle == INVALID_HANDLE_VALUE) {
					filesystem_context_release(context);
					iterator->context = NULL;
					context = NULL;
					return MA_ERROR_APIFAILED;
				}
				else{
					context->bPrevResult = MA_TRUE;
				}
				iterator->context = context;
			}
		}
		if(!context->bPrevResult) {
			return MA_ERROR_NO_MORE_ITEMS;
		}

		for( ; context->bPrevResult == MA_TRUE; (context->bPrevResult = FindNextFileA(context->hFindHandle, &context->findfiledescriptor)?MA_TRUE:MA_FALSE)) {

			size_t len ;

			if(0 == strcmp(context->findfiledescriptor.cFileName,".") || (0 == strcmp(context->findfiledescriptor.cFileName, "..")))
				continue;
			
			if(((MA_FILESYSTEM_ITERATE_FILES == iterator->mode) && (context->findfiledescriptor.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)) )
				continue;

			if(((MA_FILESYSTEM_ITERATE_DIRECTORY == iterator->mode) && !(context->findfiledescriptor.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))) 
				continue;

			len = strlen(context->findfiledescriptor.cFileName) + 1;
			ma_temp_buffer_reserve(buffer,len);
			strncpy((char *)ma_temp_buffer_get(buffer), context->findfiledescriptor.cFileName, len);
			error = MA_OK;
			/*Update the FindNextPointer to the next file so that it will be accessed*/
			context->bPrevResult = FindNextFileA(context->hFindHandle, &context->findfiledescriptor)?MA_TRUE:MA_FALSE;
			break ;

		}

		return error;
	}
	return MA_ERROR_INVALIDARG;
}