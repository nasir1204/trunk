#include "ma/internal/utils/network/ma_net_interface.h"
#include "ma/internal/utils/platform/ma_net_interface_list_internal.h"
#include "ma/internal/utils/text/ma_utf8.h"
#include <winsock2.h>
#include <windows.h>
#include <ws2tcpip.h>
#include <iphlpapi.h>
#include <ipTypes.h>
#include <stdarg.h>
#include <wchar.h>

//Function pointer declaration for GetAdaptersAddresses , which we use to GetProcAddress
typedef DWORD (WINAPI * LPFNGETADAPTERSADDRESSES)
(
  ULONG Family,
  DWORD Flags,
  PVOID Reserved,
  PIP_ADAPTER_ADDRESSES pAdapterAddresses,
  PULONG pOutBufLen
);

static ma_bool_t get_ip_adapater_addresses(IP_ADAPTER_ADDRESSES **pAddresses) {
	HMODULE	hIPHelperLib = NULL;
	LPFNGETADAPTERSADDRESSES  lpfnGetAddresses = NULL;
	ma_bool_t bRet = MA_FALSE;

	if(NULL == pAddresses)
		return MA_FALSE;
	
	hIPHelperLib = LoadLibraryA("iphlpapi.dll");
	if( NULL == hIPHelperLib)
		return MA_FALSE;

	lpfnGetAddresses = (LPFNGETADAPTERSADDRESSES)GetProcAddress( hIPHelperLib, "GetAdaptersAddresses" );
	if( NULL != lpfnGetAddresses) {	
		ULONG ulOutBufLen;
		ULONG ulFlags;
		*pAddresses = (IP_ADAPTER_ADDRESSES *) malloc(sizeof(IP_ADAPTER_ADDRESSES)) ;
		ulOutBufLen = sizeof(IP_ADAPTER_ADDRESSES);
		ulFlags = GAA_FLAG_INCLUDE_PREFIX | GAA_FLAG_SKIP_MULTICAST | GAA_FLAG_SKIP_ANYCAST ;			
		// Make an initial call to GetAdaptersAddresses to get the necessary size into the ulOutBufLen variable
		if( ( NULL != *pAddresses)
				&& ( ERROR_BUFFER_OVERFLOW == lpfnGetAddresses(AF_UNSPEC,ulFlags , NULL, *pAddresses , &ulOutBufLen))) {

			free(*pAddresses);
			*pAddresses = NULL ;
			*pAddresses = (IP_ADAPTER_ADDRESSES*) malloc (ulOutBufLen);
			if((*pAddresses) == NULL) {
				FreeLibrary(hIPHelperLib) ;
				return MA_FALSE;
			}
		}
		// Make a second call to GetAdaptersAddresses to get the actual data we want
		// Caller/User of this function will free the memory for it
		if( NO_ERROR == lpfnGetAddresses(AF_UNSPEC,ulFlags , NULL, *pAddresses , &ulOutBufLen)) {
			bRet = MA_TRUE;
		}
		else{
			if(*pAddresses){
				free(*pAddresses);
				*pAddresses = NULL;
			}
		}
	}		
	FreeLibrary(hIPHelperLib) ;
	return bRet ;
}

ma_error_t get_addr_in_string(	 char *str_addr, 
								struct sockaddr *addr, ma_uint32_t addr_len) {
	char buf[NI_MAXHOST];
	memset(buf,0,NI_MAXHOST);

	if( 0 != getnameinfo(addr,addr_len,buf,NI_MAXHOST,NULL,0,NI_NUMERICHOST) ) {
		//Log API error message.
		return MA_ERROR_APIFAILED;
	}
	strncpy_s(str_addr, MA_IPV6_ADDR_LEN, buf,MA_IPV6_ADDR_LEN);
	return MA_OK;
}

ma_error_t get_net_mask( char *net_mask, char *net_addr, char *broadcast_addr, const char *ip_addr) {
	SOCKET sock;
	DWORD bytes_ret = 0;
	
	ma_int32_t numIf = 0;
	ma_int32_t i = 0;
	INTERFACE_INFO localIf[10] = { 0 };

	if( (sock = WSASocket(AF_INET, SOCK_DGRAM, IPPROTO_UDP, NULL, 0, 0)) == INVALID_SOCKET) {
		return MA_ERROR_APIFAILED;
	}
	if( SOCKET_ERROR == WSAIoctl(sock, SIO_GET_INTERFACE_LIST, NULL, 0, &localIf,
							sizeof(localIf), &bytes_ret, NULL, NULL))
	{
		closesocket(sock);
		return MA_ERROR_APIFAILED;
	}

	closesocket(sock);

	numIf = (bytes_ret/sizeof(INTERFACE_INFO));
	for ( i = 0; i < numIf ; i++)  {
		if(strcmp(ip_addr,inet_ntoa(localIf[i].iiAddress.AddressIn.sin_addr))==0) {
			struct in_addr addr;
			struct in_addr broad_addr;
			
			strncpy_s(net_mask , MA_IPV6_ADDR_LEN, (inet_ntoa(localIf[i].iiNetmask.AddressIn.sin_addr)), MA_IPV6_ADDR_LEN - 1);
			
			addr.s_addr = (localIf[i].iiNetmask.AddressIn.sin_addr.s_addr 
							&
							localIf[i].iiAddress.AddressIn.sin_addr.s_addr );

			strncpy_s(net_addr, MA_IPV6_ADDR_LEN, inet_ntoa(addr),MA_IPV6_ADDR_LEN);

			broad_addr.s_addr = addr.s_addr | ~localIf[i].iiNetmask.AddressIn.sin_addr.s_addr ;

			strncpy_s(broadcast_addr, MA_IPV6_ADDR_LEN, inet_ntoa(broad_addr),MA_IPV6_ADDR_LEN);
			return MA_OK;
		}
	}
	return MA_ERROR_APIFAILED;
}
/* getNetMaskForV6 forms a net mask for V6 on the basis of prefix length
	for e.g
		if the IP address is of the form 2001::db8:0:1/65
		where 65 is the prefix , that means 
		First 65 bits needs to be one for netmask ....
		and that means output should be 
		ffff:ffff:ffff:ffff:8000::
*/
static ma_error_t  ma_net_mask_for_ipv6(IP_ADAPTER_ADDRESSES *address, char subnet_mask[MA_IPV6_ADDR_LEN]){
	if(address && address->FirstPrefix && address->FirstPrefix->PrefixLength < 128){
		long prefix_length = address->FirstPrefix->PrefixLength;
		/* First fill the 1 for exact multiples of 8 , like 65/8 = 8  */
		unsigned short  n_octs = (unsigned short)prefix_length / 8;
		/*now for the remaining bits do 65%8 = 1 , put 1 for that */
		unsigned short  n_bits = prefix_length % 8 ;
		char ip_address[MA_IPV6_ADDR_LEN] = {0};
		struct sockaddr_in6 addr;

		memset(&addr , 0 , sizeof(addr));
		addr.sin6_family = AF_INET6;			
		memset(addr.sin6_addr.s6_bytes , 0xFF , n_octs);		
		if(n_bits)
			addr.sin6_addr.s6_bytes[n_octs] = 0xff << (8 - n_bits);
		if(MA_OK == get_addr_in_string(ip_address , (struct sockaddr*)&addr , sizeof(addr))){
			char *temp = NULL;
			expand_ipv6_address(subnet_mask, ip_address);
			/*if( (strlen(subnet_mask) < MA_IPV6_ADDR_LEN) &&  (temp = strrchr(subnet_mask, ':')) && !(*(temp+1)) )
				*(temp+1) = '0';*/
			return MA_OK;
		}
		return MA_ERROR_APIFAILED;
	}
	return MA_ERROR_INVALIDARG;
} 


#if 0
/*Calcualtes the subnet address on the basis of IP address and NetMask for V6 
  NOTE: Both IPADDRESS and NETMASK should be full expanded 
  It does the AND operation between IP Address and NetMask 
*/
//ma_error_t  ma_net_addr_for_ipv6(const char strIPAddr[MA_IPV6_ADDR_LEN] , const char strNetMask[MA_IPV6_ADDR_LEN], char subnet_addr[MA_IPV6_ADDR_LEN])
ma_error_t  ma_net_addr_for_ipv6(ma_net_address_info_t * addr)
{
	if(addr){
		char strIPAddr[MA_IPV6_ADDR_LEN] = {0};
		expand_ipv6_address(strIPAddr, addr->ip_addr);
		/*If any one of them is empty return blank */
		if(!strlen(strIPAddr) || !strlen(addr->subnet_mask)){
			addr->subnet_addr[0] = 0;
			return MA_OK;
		}
		else{
			char *ipPos = 0 ,  *maskPos = 0 ;
			char *temp = NULL;
			char *ipPos_str = NULL ,  *maskPos_str = NULL ;
			char localIP[MA_IPV6_ADDR_LEN + MA_IPV6_ADDR_LEN] = {0};
			char localMask[MA_IPV6_ADDR_LEN + MA_IPV6_ADDR_LEN] = {0};
			char strSubNetAddr[MA_IPV6_ADDR_LEN + MA_IPV6_ADDR_LEN] = {0};
			strcpy(localIP, strIPAddr);
			strcat(localIP, ":");

			strcpy(localMask, addr->subnet_mask);
			strcat(localMask, ":");
			ipPos = &localIP[0];
			ipPos_str = localIP;
			maskPos = &localMask[0];
			maskPos_str = localMask;
			//As this will be expanded address , so both will be same in octets in 8 forms
			while((ipPos_str = strchr(ipPos_str+1,':')) &&
					(maskPos_str = strchr(maskPos_str+1,':'))) {
				unsigned long  ipoctet = 0  , maskoctet= 0 ;	
				char localIP_substr[MA_IPV6_ADDR_LEN] = {0};
				char localMask_substr[MA_IPV6_ADDR_LEN] = {0};
				char hex[MA_IPV6_ADDR_LEN] = {0};
				strncpy(localIP_substr, localIP, (ipPos_str - ipPos ));
				strncpy(localMask_substr, localMask, (maskPos_str - maskPos));
				//Convert from str to ul with base 16
				ipoctet  = strtoul(localIP_substr , NULL , 16);			
				maskoctet  = strtoul(localMask_substr  , NULL , 16);

				//Now we are done with the string , so remove it 
				/*localIP = localIP.erase(0 , ipPos + 1);
				localMask = localMask.erase(0 , maskPos + 1);*/
		
				//Now put in strsubnet addr by doing AND between IP & mask octet
				sprintf(hex,"%x",(ipoctet  & maskoctet));		
				strcat(addr->subnet_addr, hex);
				strcat(addr->subnet_addr, ":");		
			}
			//Now erase the last ":" , which would have been added from the above logic
			if(temp = strrchr(addr->subnet_addr, ':'))
				*temp = '\0';
			return MA_OK;
		}
	}
}
#endif

ma_error_t ma_net_interface_initialize(ma_net_interface_list_t * list, ma_bool_t b_loopback) {
	ma_error_t error = MA_OK;
	IP_ADAPTER_ADDRESSES *adapter_address = NULL;
	IP_ADAPTER_ADDRESSES *address = NULL;
	PIP_ADAPTER_UNICAST_ADDRESS unicast_address = NULL;

	ma_net_interface_t * inf = NULL;

	ma_bool_t found = MA_FALSE;
	if(list == NULL)
		return MA_ERROR_INVALIDARG;

	if(!get_ip_adapater_addresses(&adapter_address))
		return MA_ERROR_APIFAILED;

	address = adapter_address;

	while(address) {
		ma_uint32_t if_index = address->IfIndex!=0?address->IfIndex: address->Ipv6IfIndex;
		
		if( (address->IfType  == IF_TYPE_TUNNEL ) ) {
			//Decide whether to skip it 
		}

		if ( (address->IfType ==  MIB_IF_TYPE_LOOPBACK  )  && !b_loopback) {
			address = address->Next;
			continue;
		}

		if(  address->OperStatus == IfOperStatusDown) {
			address = address->Next;
			continue ;
		}

		error = ma_net_interface_create(&inf);
		if(error != MA_OK) {
			ma_net_interface_list_reset(list);
			free(adapter_address);	
			return error;
		}

		ma_wide_char_to_utf8((char*)inf->interface_name, MA_INTERFACE_NAME_SIZ, address->FriendlyName, wcslen(address->FriendlyName));
		memcpy(inf->mac_address, address->PhysicalAddress, MA_MAC_ADDRESS_SIZE);
		
		for (unicast_address = address->FirstUnicastAddress; unicast_address; unicast_address = unicast_address->Next) {
			ma_net_address_info_t * addr = NULL;
			ma_uint32_t family = (unicast_address->Address.lpSockaddr)->sa_family ;
			socklen_t len = family==AF_INET?sizeof(struct sockaddr_in):sizeof(struct sockaddr_in6);
			//Just set the Family Type as only, internally it'll mark it as both if any of the other type is present.
			error = ma_address_info_create(&addr);
			if(error != MA_OK) {
				ma_net_interface_release(inf);
				ma_net_interface_list_reset(list);
				free(adapter_address);
				return error;
			}
			if( MA_OK == (error = get_addr_in_string(addr->ip_addr,unicast_address->Address.lpSockaddr, len)) ) {
				if(family==AF_INET) {
					if(MA_OK == get_net_mask(addr->subnet_mask,addr->subnet_addr, addr->broadcast_addr,addr->ip_addr)) {
						addr->family = MA_IPFAMILY_IPV4;
						addr->ipv6_scope_id = 0;
						
						ma_net_interface_set_ipfamily(inf,addr->family);
						ma_net_interface_add_address(inf,&addr);
						addr = NULL;
					}
					else
					{
						ma_address_info_release(addr);
						addr = NULL;
						continue;
					}
				}
				else {
					addr->ipv6_scope_id = address->Ipv6IfIndex;
					addr->family = MA_IPFAMILY_IPV6;
					memset(addr->subnet_mask, 0, MA_IPV6_ADDR_LEN);
					memset(addr->subnet_addr, 0, MA_IPV6_ADDR_LEN);
					memset(addr->broadcast_addr, 0, MA_IPV6_ADDR_LEN);
					
					if(MA_OK != ma_net_mask_for_ipv6(address, addr->subnet_mask))
						strcpy_s(addr->subnet_mask, MA_IPV6_ADDR_LEN, "N/A");
					strcpy_s(addr->subnet_addr, MA_IPV6_ADDR_LEN, "N/A");
					strcpy_s(addr->broadcast_addr, MA_IPV6_ADDR_LEN, "N/A");
					ma_net_interface_set_ipfamily(inf,addr->family);
					ma_net_interface_add_address(inf,&addr);
					addr = NULL;
				}
			}
		}
		inf->index = if_index;
		if(inf->addresses_count <= 0) {
			ma_net_interface_release(inf);
			inf = NULL;
		}
		else
			ma_net_interface_list_add_interface(list, &inf);
		address = address->Next;
	}

	free(adapter_address);
	return MA_OK;
}
