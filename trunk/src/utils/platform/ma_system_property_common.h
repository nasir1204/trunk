#ifndef MA_SYSTEM_PROPRTY_COMMON_H_INCLUDED
#define MA_SYSTEM_PROPRTY_COMMON_H_INCLUDED
#include "ma/ma_common.h"
#include "ma/ma_errors.h"
#include "ma/internal/utils/platform/ma_system_property.h"

#define LOG_FACILITY_NAME "maconfigurator"

MA_CPP(extern "C" {)
#define MA_NA                "N/A"

struct ma_system_property_s	{
	ma_cpu_system_property_t        cpu_information ;
	ma_memory_system_property_t		memory_information ;
	ma_os_system_property_t			os_information ;
	ma_misc_system_property_t		misc_information ;
	ma_disk_system_property_t		disk_information ;
	ma_network_system_property_t	network_information;
	ma_cluster_system_property_t	cluster_information;
};

ma_error_t system_property_cpu_get(ma_cpu_system_property_t *cpu_info);
ma_error_t system_property_os_get (ma_os_system_property_t *os_info);
ma_error_t system_property_disk_get(ma_disk_system_property_t *disk_info);
ma_error_t system_property_memory_get(ma_memory_system_property_t *memInfo);
ma_error_t system_property_misc_get(ma_misc_system_property_t *misc_info) ;
ma_error_t system_property_network_get(ma_network_system_property_t *net_info);
ma_error_t system_property_cluster_get(ma_cluster_system_property_t *cluster_info);

ma_error_t system_property_is_portable_get(char is_portable[], size_t length);
ma_error_t system_property_domain_name_get(char domain_name[], size_t length);
ma_error_t system_property_computer_name_get(char computer_name[], size_t length) ;
ma_error_t system_property_computer_description_get(char computer_desc[], size_t length);
ma_error_t system_property_time_zone_info_get(char time_zone[], size_t length);
ma_error_t system_property_time_zone_bias_get(char time_zone_bios[], size_t length);
ma_error_t system_property_lang_id_get(char language[], size_t length);
ma_error_t system_property_lang_code_get(char code[], size_t length);
ma_error_t system_property_system_time_get(char system_time[], size_t length);
ma_error_t system_property_user_name_get(char user_name[], size_t length);
ma_error_t system_property_host_name_get(char host_name[], size_t length);
ma_error_t system_property_ipx_addr_get(char ipx_addr[], size_t length);
ma_error_t ma_fqdn_get(char *ip_addr, char fqdn[], size_t length);
void	   system_property_cluster_release(ma_cluster_system_property_t *cluster_info);
MA_CPP(})
#endif /*MA_SYSTEM_PROPRTY_COMMON_H_INCLUDED*/

