#include "ma/internal/utils/platform/ma_sys_apis.h"
#include "ma/internal/utils/text/ma_utf8.h"
#include "ma/internal/utils/datastructures/ma_bytestream.h"

#ifdef MA_WINDOWS
#define HAVE_SLEEP 1
#include <process.h>
#include <WinSock.h>
#include <Windows.h>
#include <tlhelp32.h>
#else
#define HAVE_GETTIMEOFDAY  /* this might be changed to HAVE_CLOCK_GETTIME for other platforms if required */
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/time.h>
#include <time.h>
#include <sys/utsname.h>
#include <sys/statvfs.h>
#include <pwd.h>
#endif

#if !defined(__GNUC__)
#define EPOCHFILETIME 116444736000000000i64
#else
#define EPOCHFILETIME 116444736000000000LL
#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#ifndef MA_WINDOWS
#include <unistd.h>
#endif

#include <wchar.h>

#include "ma/internal/ma_macros.h"
#include "path_private.h"

/* env value should be const string in the form of "<name>=<value>" */
ma_error_t ma_sys_putenv(const char *env){
	if(env) {	
		return (-1 == MA_MSC_SELECT(_putenv, putenv)((char *)env)) ?  MA_ERROR_APIFAILED : MA_OK;
	}
    return MA_ERROR_INVALIDARG;
}

size_t ma_strnlen(const char *s, size_t maxlen) {
#if defined(SOLARIS) || defined(MACX) || defined(HPUX)
    size_t n;
    for (n = maxlen; n-- && *s; ++s); 
    return maxlen - n - 1;
#else
    return strnlen(s, maxlen);
#endif
}

wchar_t *ma_wcsdup(const wchar_t* string) {
#if defined(MACX) || defined(SOLARIS)
     if(!string) {
        return NULL;
    }
    wchar_t* destination;
    size_t len = wcslen(string);
    if(!(destination = (wchar_t*) malloc(sizeof(wchar_t) * len))) {
        return NULL;
    }
    return wcscpy(destination, string);
#else
    return wcsdup(string);
#endif
}


ma_uint32_t ma_sys_rand() {
    ma_uint32_t random_num = 0;
#ifdef MA_WINDOWS
    errno_t err = rand_s(&random_num);
    if(0 != err) {
        srand(GetTickCount());
        random_num = rand();
    }
#else
    unsigned int seed = 0;
    struct timeval tv;
    ma_uint32_t pid;
    ma_sys_gettimeofday(&tv);
    pid = getpid();
    seed = (unsigned int)(((unsigned int)pid << 16) ^ (unsigned int)pid ^ (unsigned int)tv.tv_sec ^ (unsigned int)tv.tv_usec);
    random_num = rand_r(&seed);
#endif
    return random_num;
}

ma_error_t ma_filesystem_iterator_create(const char *absolute_path, ma_filesystem_iterator_t **iterator) {
    if(absolute_path && iterator) {
        *iterator = (ma_filesystem_iterator_t *) calloc(1, sizeof(ma_filesystem_iterator_t));
        if(!*iterator)
            return MA_ERROR_OUTOFMEMORY;
        (*iterator)->path = strdup(absolute_path);
		(*iterator)->mode = MA_FILESYSTEM_ITERATE_DIRECTORY;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_filesystem_iterator_release(ma_filesystem_iterator_t *iterator) {
    if(iterator){
        free(iterator->path);
        filesystem_context_release(iterator->context);
        free(iterator);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_filesystem_iterator_set_mode(ma_filesystem_iterator_t *iterator, ma_filesystem_iterator_mode_t mode) {
	if(iterator) {
		iterator->mode = mode;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_filesystem_iterator_count(const char *path, ma_filesystem_iterator_mode_t mode, size_t *size) {
	if(path && size) { 
		ma_filesystem_iterator_t *iter = NULL;
		size_t count = 0;
		ma_temp_buffer_t temp_buf;
		ma_temp_buffer_init(&temp_buf);
		if(MA_OK == ma_filesystem_iterator_create(path, &iter)) {
			ma_filesystem_iterator_set_mode(iter, mode);
			while(MA_OK == ma_filesystem_iterator_get_next(iter, &temp_buf)) {
				count++;
			}
			*size = count;
			ma_filesystem_iterator_release(iter);
		}
		ma_temp_buffer_uninit(&temp_buf);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_filesystem_iterator_get_mode(ma_filesystem_iterator_t *iterator, ma_filesystem_iterator_mode_t *mode) {
	if(iterator && mode) {
		*mode = iterator->mode;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}


ma_error_t ma_sys_gettimeofday(struct timeval *time_val) {
#if defined(MA_WINDOWS)
    /* Windows emulation */
    FILETIME ft;
    LARGE_INTEGER li;
    __int64 t;
    static int tzflag;
    if (time_val != NULL) {
        GetSystemTimeAsFileTime(&ft);
        li.LowPart  = ft.dwLowDateTime;
        li.HighPart = ft.dwHighDateTime;
        t  = li.QuadPart;
        t -= EPOCHFILETIME;
        t /= 10;
        time_val->tv_sec  = (long)(t / 1000000);
        time_val->tv_usec = (long)(t % 1000000);
    }
    return MA_OK;
#else
    /* Unix/POSIX pass-through */
    return (-1 != gettimeofday(time_val, NULL)) ? MA_OK: MA_ERROR_APIFAILED;
#endif

    /* POSIX emulation
    struct timespec ts;
    if (clock_gettime(CLOCK_MONOTONIC, &ts) == -1)
        return -1;
    if (tv != NULL) {
        tv->tv_sec = (long)ts.tv_sec;
        tv->tv_usec = (long)ts.tv_nsec / 1000;
    }
    return 0;
    */
}

ma_error_t ma_sys_usleep(long usec) {
#if defined(MA_WINDOWS) && defined(HAVE_SLEEP)
    Sleep(usec / 1000);
#elif defined(MA_WINDOWS)
    _sleep(usec / 1000);
#elif defined(HAVE_NANOSLEEP)
    /* POSIX newer nanosleep(3) variant */
    struct timespec ts;
    ts.tv_sec  = 0;
    ts.tv_nsec = 1000 * usec;
    nanosleep(&ts, NULL);
#else
    /* POSIX older select(2) variant */
    struct timeval tv;
    tv.tv_sec  = 0;
    tv.tv_usec = usec;
    select(0, NULL, NULL, NULL, &tv);
#endif
    return MA_OK;
}

/* Windows random number generation */
#if defined(MA_WINDOWS)
ma_error_t  ma_prng_gen_random_number(void *data_ptr, size_t data_len) {
    size_t n = data_len;
    unsigned char *p = (unsigned char *) data_ptr;
    struct timeval tv;
    int pid;

    if(( NULL == data_ptr) || (0 == data_len ))	return MA_ERROR_INVALIDARG ;


    /* Do seed the srand but we will use rand only when rand_s fails */
    ma_sys_gettimeofday(&tv);
    pid = _getpid();

    srand( (unsigned int)(((unsigned int)pid << 16) ^ (unsigned int)pid ^ (unsigned int)tv.tv_sec ^ (unsigned int)tv.tv_usec));

    while(n > 0 ) {
        unsigned int randNumber = 0 , rand_length = sizeof(randNumber) < n ? sizeof(randNumber) : n; //MIN()
        errno_t err = rand_s(&randNumber);

        if(0 != err) randNumber = rand(); 

        memcpy(p , &randNumber , rand_length);
        n -= rand_length;
        p += rand_length; 
    }	
    return MA_OK ;
}
#else
/* *nix generation of random number */
ma_error_t  ma_prng_gen_random_number(void *data_ptr, size_t data_len) {
    size_t n;
    unsigned char *p = NULL ;
    int fd = -1;

    if(( NULL == data_ptr) || (0 == data_len ))	return MA_ERROR_INVALIDARG ;

    p = (unsigned char *) data_ptr ;
    n = data_len ;

    /* 1st Aprroach , use OS */

    if (-1 == (fd = open("/dev/urandom", O_RDONLY)))
        fd = open("/dev/random", O_RDONLY|O_NONBLOCK);

    if (-1 != fd )
    {
        int i = 0 ;
        fcntl(fd, F_SETFD, FD_CLOEXEC);
        while(n > 0 ) {
            i = read(fd, (void *)p, n);
            if(i <= 0 )
                break ;
            n -= (unsigned int) i ;
            p += (unsigned int) i ;
        }
        close(fd);
        /* PRAMOD: Should we return here?  NO. As may be n is not satisfied yet*/
    }

    /* 2nd approach , use system function */
    while(n > 0 ) {
        unsigned int randNumber = 0 , seed = 0 , rand_length = (sizeof(randNumber) < n ? sizeof(randNumber) : n); //MIN()
        struct timeval tv;
        pid_t pid;


        ma_sys_gettimeofday(&tv);
        pid = getpid();
        seed = (unsigned int)(((unsigned int)pid << 16) ^ (unsigned int)pid ^ (unsigned int)tv.tv_sec ^ (unsigned int)tv.tv_usec) ;
        /* rand_r undefined reference on android */
#if defined(ANDROID)
        srand(seed);
        randNumber = rand();
#else
        randNumber = rand_r(&seed);
#endif

        memcpy(p , &randNumber , rand_length);
        n -= (unsigned int)rand_length ;
        p += (unsigned int)rand_length  ;

    }
    return MA_OK ;
}
#endif 



/* TBD - Is there any better solution for this?? */
ma_int32_t ma_sys_get_process_status(const char *process_name) {
#ifdef MA_WINDOWS
	HANDLE hProcessSnap;
	PROCESSENTRY32 pe32;
	ma_bool_t b_rc = MA_FALSE;
	WCHAR w_process_name[MA_MAX_LEN] = {0};

	// Take a snapshot of all processes in the system.
	hProcessSnap = CreateToolhelp32Snapshot( TH32CS_SNAPPROCESS, 0 );
	if(hProcessSnap == INVALID_HANDLE_VALUE)      
		return -1;
  
	// Set the size of the structure before using it.
	pe32.dwSize = sizeof(PROCESSENTRY32);

	// Retrieve information about the first process,
	// and exit if unsuccessful
	if(!Process32First( hProcessSnap, &pe32)) { 
		CloseHandle(hProcessSnap);          // clean the snapshot object
		return -1;
	}

	if(-1 == ma_utf8_to_wide_char(w_process_name, MA_MAX_LEN, process_name, strlen(process_name))) {			
		CloseHandle(hProcessSnap);
		return -1;
	}

	// Now walk the snapshot of processes
	do {		
		if(!wcscmp(pe32.szExeFile, w_process_name)){
			b_rc = MA_TRUE;			
			break;
		}
	} while(Process32Next(hProcessSnap, &pe32));

	CloseHandle( hProcessSnap );
	return b_rc;
#elif MA_LINUX
	char cmd[MA_MAX_LEN] = {0};
	FILE *fp = NULL;
	MA_MSC_SELECT(_snprintf, snprintf)(cmd, MA_MAX_LEN, "pgrep %s > /dev/null", process_name);
	if((fp = popen(cmd, "r")) != NULL) {
            return  0 == pclose(fp);
        }
	return 0;
#else
	/* TBD - for *nix - will find commands for other platforms */
	return 0;
#endif
}


ma_int64_t ma_atoint64(const char *string) {
	ma_int64_t value = 0;
#ifdef MA_WINDOWS
	value = _atoi64(string);
#else
	value = strtoll(string, NULL, 10);
#endif
	return value;
}

#ifdef MA_WINDOWS 
ma_bool_t ma_get_computer_name(char computer_name[], size_t length) {
    ma_bool_t b_rc = MA_FALSE;
    DWORD   name_len     = MAX_COMPUTERNAME_LENGTH + 1;
    LPWSTR  name = (LPWSTR)calloc(1, name_len * sizeof(WCHAR));
    if(name) {
        if(GetComputerName(name, &name_len)){
            if(0 < ma_wide_char_to_utf8(computer_name, length-1, name, wcsnlen(name, length-1)))
                b_rc = MA_TRUE;        
        }
        free(name);
    }    
    return b_rc;  
}
#else
ma_bool_t ma_get_computer_name(char computer_name[], size_t length) {
    ma_bool_t b_rc = MA_FALSE;
    struct utsname info;
	int i=0;
    if( -1 != uname(&info)) {			
        if(strchr(info.nodename, '.')) {
	   for(i=0; (i < length-1) && (info.nodename[i] != '.'); i++)		    
	    computer_name[i] = info.nodename[i];		    
        }
	else {
	   strncpy(computer_name, info.nodename, length -1);
	}
        b_rc = strlen(computer_name) ? MA_TRUE : MA_FALSE;
	}
	
    return b_rc;  
}     
#endif   

uint64_t ma_get_disk_free_space(const char *dir_name) {
#ifdef MA_WINDOWS
    uint64_t free_bytes = 0;
    if(GetDiskFreeSpaceExA(dir_name, (PULARGE_INTEGER)&free_bytes, NULL, NULL)) {
        return free_bytes;
    }
    return 0;
#else
    struct statvfs stat_fs;

    if(!statvfs(dir_name, &stat_fs)) {
        return stat_fs.f_bsize * stat_fs.f_bfree;
    }
    return 0;
#endif
}

#ifndef MA_WINDOWS
#define MA_COMMAND_LEN 	16384
static ma_error_t get_passwd(const char *user, struct passwd *pwd){		
	struct passwd* pwd_ptr = pwd;
	struct passwd* temp_pwd_ptr = NULL;
	char pwdbuffer[MA_COMMAND_LEN] = {0};
	int  pwdlinelen = sizeof(pwdbuffer)/sizeof(pwdbuffer[0]);
	int s = getpwnam_r(user, pwd_ptr, pwdbuffer, pwdlinelen, &temp_pwd_ptr);
	if(NULL == temp_pwd_ptr && 0 == s) return MA_ERROR_SYSAPI_USER_NOT_EXIST;	
	else if(NULL == temp_pwd_ptr && s != 0) return MA_ERROR_SYSAPI_GETPASSWD_FAILED;			
	return MA_OK;
}
#endif

ma_int32_t ma_sys_chprocessown(const char *username){
#ifdef MA_WINDOWS
	return MA_OK;
#else
	if(username){
		ma_error_t rc = MA_OK;
		struct passwd pwd;		
		if(MA_OK == (rc = get_passwd(username ,&pwd))){
			if(0 == setgid(pwd.pw_gid)){ 
				if(0 != setuid(pwd.pw_uid)) 
					rc = MA_ERROR_SYSAPI_SETUID_FAILED;		
			}		
			else
				rc = MA_ERROR_SYSAPI_SETGID_FAILED;
		}
		return (ma_int32_t)rc;
	}
	return MA_ERROR_INVALIDARG;
#endif
}

ma_int32_t ma_sys_chfileown(const char *filename, const char *username){
#ifdef MA_WINDOWS
	return MA_OK;
#else
	if(filename && username){
		ma_error_t rc = MA_OK;
		struct passwd pwd;		
		if(MA_OK == (rc = get_passwd(username ,&pwd))){
			if(chown(filename, pwd.pw_uid, pwd.pw_gid))
				rc = MA_ERROR_SYSAPI_CHOWN_FAILED;
		}
		return rc;
	}
	return MA_ERROR_INVALIDARG;
#endif
}

unsigned long ma_get_tick_count() {
#ifdef MA_WINDOWS
	return GetTickCount();
#else
	struct timeval time_val;
	unsigned long tick_count = 0;
	(void) gettimeofday(&time_val, NULL);
	tick_count = time_val.tv_sec * 1000.0; 
    tick_count += time_val.tv_usec / 1000.0;
	return tick_count;
#endif
}