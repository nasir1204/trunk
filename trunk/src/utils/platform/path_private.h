#ifndef PATH_PRIVATE_H_
#define PATH_PRIVATE_H_

#include "ma/ma_common.h"
MA_CPP(extern "C" {)

typedef struct filesystem_context_s filesystem_context_t, *filesystem_context_h;

ma_error_t filesystem_context_create(filesystem_context_t **context);

ma_error_t filesystem_context_release(filesystem_context_t *context);

struct ma_filesystem_iterator_s {
	char *path;
	filesystem_context_t *context;
	ma_filesystem_iterator_mode_t mode;
};

MA_CPP(})
#endif

