#include "ma/internal/utils/platform/ma_process.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/filesystem/path.h"

#include "uv.h"

#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <libgen.h>

#define MAPROCESS_PID_FILE_PREFIX    ".ma."
#define MAPROCESS_PID_FILE_SUFFIX    ".pid"
#define ARRAY_SIZE(x) (sizeof(x)/sizeof((x)[0]))

static const char* pidfile_dirs[] = {
    "/var/mileaccess/agent/tmp", // legacy install location
    "/run/mileaccess", // /run mandated by FHS 3.x
    "/run",
    "/var/run/mileaccess", // /var/run used by systems still on FHS 2.x
    "/var/run"
};

static int is_group_writeable(struct stat s)
{
    const size_t max_groups = 16;
    gid_t groups[max_groups];
    int i;

    if (s.st_mode & S_IWGRP) {
        if(getegid() == s.st_gid)
            return 1;
#ifdef _HPUX_   
        // In Hp-Ux, right after installation, the getgroups returns 0 entries
        // Invoking initgroups helps to fix the getgroups problem.
        // TODO : Instead of passing "root" need to retrieve the logged in user name
        gid_t user_egid = getegid();        
        if(initgroups("root", user_egid) != 0)
        {            
            return 0;
        }
#endif        

        for (i = getgroups(max_groups, groups) - 1; i >= 0; --i) {
            if (s.st_gid == groups[i]) {
                return 1;
            }
        }
    }

    return 0;
}

static int is_writeable(const char* path)
{
    int rc = 0;
    struct stat s;

    if (stat(path, &s) == 0) {
        if (((s.st_uid == geteuid()) && (s.st_mode & S_IWUSR)) ||
                is_group_writeable(s) ||
                (s.st_mode & S_IWOTH)) {
            rc = 1;
        }
    }
    return rc;
}

static const char* get_pid_dir()
{
    int i;
    for (i = 0; i < ARRAY_SIZE(pidfile_dirs); ++i) {
        if (is_writeable(pidfile_dirs[i])) {
            return pidfile_dirs[i];
        }
    }

    return NULL;
}

static ma_int32_t get_pid_file(char *pid_file, size_t *length)
{
    const char* pid_dir = get_pid_dir();
    if(pid_dir && pid_file && length) {
        size_t size = MA_MAX_LEN;
        char ma_exepath[MA_MAX_LEN];

        if(!uv_exepath(ma_exepath, &size)) {
            size = strlen(pid_dir)
                   + 1
                   + strlen(MAPROCESS_PID_FILE_PREFIX)
                   + size
                   + strlen(MAPROCESS_PID_FILE_SUFFIX);
            if(size > *length) return -1;

            MA_MSC_SELECT(_snprintf, snprintf)(pid_file,
                                               size,
                                               "%s%c%s%s%s",
                                               pid_dir,
                                               MA_PATH_SEPARATOR,
                                               MAPROCESS_PID_FILE_PREFIX,
                                               basename(ma_exepath),
                                               MAPROCESS_PID_FILE_SUFFIX);
            *length = size;
            return 0;
        }
    }
    return -1;
}

static ma_int32_t delete_service_pid_file()
{
    int pid_file_fd = 0;
    size_t size = MA_MAX_LEN;
    char pid_file_name[MA_MAX_LEN] = {0};
    ma_int32_t status = get_pid_file(pid_file_name, &size);

    if(!status) {
        unlink(pid_file_name);
    }
    return status;
}

static void process_exit_cb(uv_process_t* process, int exit_status, int term_signal)
{
    delete_service_pid_file();
    uv_close((uv_handle_t*)process, NULL);
}

static ma_int32_t process_running_status(pid_t *pid)
{
    size_t size = MA_MAX_LEN;
    char pid_file_name[MA_MAX_LEN] = {0};
    ma_int32_t status = get_pid_file(pid_file_name, &size);

    if(!status) {
        int pid_file_fd = 0;
        if(-1 != (pid_file_fd = open(pid_file_name, O_RDWR, 0666))) {
            char pid_str[16] = {0};
            status = (0 == lockf(pid_file_fd, F_TLOCK, 0)) ? 1 : 0;
            read(pid_file_fd, pid_str, sizeof(pid_str));
            *pid = atoi(pid_str);
            close(pid_file_fd);
        } else
            status = 1;
    }
    return status;
}

/* Returns 0, 1, or -1 */
ma_int32_t ma_process_start()
{
    uv_loop_t              *loop  = NULL;
    uv_process_t            child_req = {0};
    uv_process_options_t    options = {0};

    char* args[3] = {0};
    uv_stdio_container_t child_stdio[3] ;

    char ma_exepath[MA_MAX_LEN];
    size_t ma_exepath_size = MA_MAX_LEN;

    pid_t pid = 0;
    ma_int32_t status = process_running_status(&pid);

    /* Process is already running, so return immediately */
    /* This process gets the lock on pid file, but this process may be terminated while launching the actual proceess */
    if(-1 == status || 0 == status)
        return 1;

    if(!uv_exepath(ma_exepath, &ma_exepath_size)) {
        ma_exepath[ma_exepath_size] = '\0';
        args[0] = ma_exepath;
        args[1] = "self_start";
        args[2] = NULL;
        options.file = ma_exepath;
        options.args = args;
        options.flags = UV_PROCESS_DETACHED;
        options.exit_cb = process_exit_cb;
        options.stdio_count = 3 ;

        child_stdio[0].flags = UV_IGNORE ;
        child_stdio[1].flags = UV_IGNORE ;
        child_stdio[2].flags = UV_INHERIT_FD ;
        child_stdio[2].data.fd = 2 ;
        options.stdio = child_stdio ;

        loop = uv_default_loop();

        if(uv_spawn(loop, &child_req, options)) {
            fprintf(stderr, "Failed to spawn the process due to error %s\n", uv_strerror(uv_last_error(loop))) ;
            return -1 ;
        }

        uv_unref((uv_handle_t*) &child_req) ;

#if (9 < UV_VERSION_MINOR)
        uv_run(loop, UV_RUN_DEFAULT);
#else
        uv_run(loop);
#endif
        return 0 ;
    }
    return -1;
}

static ma_int32_t write_service_pid()
{
    int pid_file_fd = 0;
    size_t size = MA_MAX_LEN;
    char pid_file_name[MA_MAX_LEN] = {0};
    ma_int32_t status = get_pid_file(pid_file_name, &size);

    if(!status) {
        if(0 < (pid_file_fd = open(pid_file_name, O_RDWR|O_CREAT, 0644)) &&
                0 == lockf(pid_file_fd, F_TLOCK, 0) &&
                0 == ftruncate(pid_file_fd, 0)
          ) {
            char pid_str[16] = {0};
            sprintf(pid_str,"%d\n", getpid());
            write(pid_file_fd, pid_str, strlen(pid_str)) ;
        } else
            status = -1;
    }
    return status;
}

typedef struct process_stop_user_data_s {
    ma_process_stop_handler_cb_t cb;
    void                        *cb_data;
} process_stop_user_data_t;

static void signal_close_cb(uv_handle_t *handle)
{
    free(handle);
}

static void ma_process_signal_handler(uv_signal_t *handle, int signum)
{
    process_stop_user_data_t *user_data = (process_stop_user_data_t *)handle->data;
    user_data->cb(user_data->cb_data);
    delete_service_pid_file();
    free(user_data);
    uv_signal_stop(handle);
    uv_close((uv_handle_t *)handle, signal_close_cb);
}

/* Returns 0 or -1 */
ma_int32_t ma_process_register_stop_handler(ma_event_loop_t *event_loop, ma_process_stop_handler_cb_t cb, void *cb_data)
{
    process_stop_user_data_t *user_data = (process_stop_user_data_t *)calloc(1, sizeof(process_stop_user_data_t));
    ma_int32_t status = -1;

    if(user_data) {
        struct uv_loop_s *uv_loop = NULL;

        uv_signal_t *sigterm = (uv_signal_t *)calloc(1, sizeof(uv_signal_t));
        if(sigterm) {

            /* Ignoring SIGPIPE as we will get this signal when msgbus other end closed */
            signal(SIGPIPE, SIG_IGN);

            ma_event_loop_get_loop_impl(event_loop, &uv_loop);

            user_data->cb = cb;
            user_data->cb_data = cb_data;

            uv_signal_init(uv_loop, sigterm);
            sigterm->data = user_data;
            uv_signal_start(sigterm, ma_process_signal_handler, SIGTERM);

            if(!(status = write_service_pid()))
                return status;

            uv_signal_stop(sigterm);
            free(sigterm);
        }
        free(user_data);
    }
    return status;
}

/* Returns 0, 1 or -1 */
ma_int32_t ma_process_stop()
{
    pid_t pid = 0;
    ma_int32_t status = process_running_status(&pid);

    if((0 == status) && 0 < pid)
        uv_kill(pid, SIGTERM);
    return status ;
}

/* Returns 0, 1 or -1 */
ma_int32_t ma_process_status()
{
    pid_t pid = 0;
    return process_running_status(&pid);
}
