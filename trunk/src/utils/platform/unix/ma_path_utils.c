#include "ma/internal/utils/platform/ma_path_utils.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/text/ma_utf8.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"

#include <limits.h> /* INT_MAX, PATH_MAX */
#include <errno.h>
#include <stdio.h>
#include <unistd.h>

#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>

#if defined(LINUX) || defined(MACX)
#include <sys/sysctl.h>
#endif

#if defined(AIX)
#include <fcntl.h>
#include <sys/procfs.h>
#endif

#if defined(SOLARIS)
#include <fcntl.h>
#include <procfs.h>
#endif

extern int errno;

#if defined(SOLARIS)
ma_error_t ma_pid_to_stat(long pid, struct stat *stat_buf) {
    if( (pid > 0) && stat_buf) {
        char link[MA_MAX_PATH_LEN] = {0};
        (void) snprintf(link, sizeof(link), "/proc/%lu/object/a.out", pid);
        if(-1 == lstat(link, stat_buf))
            return MA_ERROR_APIFAILED;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_pid_to_path(long pid, ma_temp_buffer_t *buffer) {
    if( (pid > 0) && buffer) {
        ssize_t res;
		char cwd[PATH_MAX] = {0}, exe_run_name[PATH_MAX] = {0} ;
		char pp[64];
		struct psinfo ps;
		int fd;
		
		snprintf(pp, sizeof(pp), "/proc/%lu/psinfo", (unsigned long) pid);

		fd = open(pp, O_RDONLY);
		if (fd < 0)
			return MA_ERROR_APIFAILED;

		res = read(fd, &ps, sizeof(ps));
		close(fd);
		if (res < 0)
			return MA_ERROR_APIFAILED;

		if (ps.pr_argv == 0)
			return MA_ERROR_APIFAILED;

		/* Get the current working directory to resolve the relative path. */
		getcwd(cwd, sizeof(cwd) - 1);				  
		
		/* get the run time name. */
		strcpy(exe_run_name, ((char**)(ps.pr_argv))[0]);

		/*form the path.*/
		ma_temp_buffer_reserve(buffer, MA_MAX_PATH_LEN + 1);				
		if(exe_run_name[0] == '/')
			snprintf((char*)ma_temp_buffer_get(buffer), MA_MAX_PATH_LEN, "%s", exe_run_name);				
		else
			snprintf((char*)ma_temp_buffer_get(buffer), MA_MAX_PATH_LEN, "%s/%s", cwd, exe_run_name);
			
		return MA_OK;    
    }
    return MA_ERROR_INVALIDARG;
}
#elif defined(AIX)
ma_error_t ma_pid_to_stat(long pid, struct stat *stat_buf) {
    if( (pid > 0) && stat_buf) {
		char link[MA_MAX_PATH_LEN] = {0};
        (void) snprintf(link, sizeof(link), "/proc/%lu/object/a.out", pid);
        if(-1 == stat(link, stat_buf))
            return MA_ERROR_APIFAILED;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_pid_to_path(long pid, ma_temp_buffer_t *buffer) {
    if( (pid > 0) && buffer) {
        ssize_t res;
		char cwd[PATH_MAX] = {0}, exe_run_name[PATH_MAX] = {0} ;
		char pp[64];
		struct psinfo ps;
		int fd;
		
		snprintf(pp, sizeof(pp), "/proc/%lu/psinfo", (unsigned long) pid);

		fd = open(pp, O_RDONLY);
		if (fd < 0)
			return MA_ERROR_APIFAILED;

		res = read(fd, &ps, sizeof(ps));
		close(fd);
		if (res < 0)
			return MA_ERROR_APIFAILED;

		if (ps.pr_argv == 0)
			return MA_ERROR_APIFAILED;

		/* Get the current working directory to resolve the relative path. */
		getcwd(cwd, sizeof(cwd) - 1);				  
		
		/* get the run time name. */
		strcpy(exe_run_name, ((char***)(ps.pr_argv))[0][0] );

		/*form the path.*/
		ma_temp_buffer_reserve(buffer, MA_MAX_PATH_LEN + 1);				
		if(exe_run_name[0] == '/')
			snprintf((char*)ma_temp_buffer_get(buffer), MA_MAX_PATH_LEN, "%s", exe_run_name);				
		else
			snprintf((char*)ma_temp_buffer_get(buffer), MA_MAX_PATH_LEN, "%s/%s", cwd, exe_run_name);
			
		return MA_OK;       
    }
    return MA_ERROR_INVALIDARG;
}
#elif defined(LINUX)
extern char * program_invocation_name;
extern const char * __progname_full;

ma_error_t ma_pid_to_stat(long pid, struct stat *stat_buf) {
    if( (pid > 0) && stat_buf) {
        char link[MA_MAX_PATH_LEN] = {0};
        (void) snprintf(link, sizeof(link), "/proc/%lu/exe", pid);
        if(-1 == lstat(link, stat_buf))
            return MA_ERROR_APIFAILED;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}
ma_error_t ma_pid_to_path(long pid, ma_temp_buffer_t *buffer) {
    if( (pid > 0) && buffer) {
        char link[MA_MAX_PATH_LEN] = {0};

        (void) snprintf(link, sizeof(link), "/proc/%lu/exe", pid);
		ma_temp_buffer_reserve(buffer, MA_MAX_PATH_LEN + 1);
        /*readlink can fail in some platforms go to the fallback */
        if(-1 == readlink(link, (char*)ma_temp_buffer_get(buffer), MA_MAX_PATH_LEN )) {
            /*just copy the same link */
            if(program_invocation_name)   
                snprintf((char*)ma_temp_buffer_get(buffer), ma_temp_buffer_capacity(buffer), "%s", program_invocation_name);
            else if(__progname_full)
                snprintf((char*)ma_temp_buffer_get(buffer), ma_temp_buffer_capacity(buffer), "%s", __progname_full);
            else 
                snprintf((char*)ma_temp_buffer_get(buffer), ma_temp_buffer_capacity(buffer), "%s", link);
            return MA_OK;
        }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}
#elif defined(__APPLE__)
#include <libproc.h>
ma_error_t ma_pid_to_stat(long pid, struct stat *stat_buf) {
    if( (pid > 0) && stat_buf) {
        char path[MA_MAX_PATH_LEN] = {0};
        if(!proc_pidpath(pid, path, MA_MAX_PATH_LEN))
            return MA_ERROR_APIFAILED;
        if(-1 == stat(path, stat_buf))
            return MA_ERROR_APIFAILED;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}
ma_error_t ma_pid_to_path(long pid, ma_temp_buffer_t *buffer) {
    if( (pid > 0) && buffer) {
        char path[MA_MAX_PATH_LEN] = {0};
        ma_temp_buffer_reserve(buffer, MA_MAX_PATH_LEN);
        if(!proc_pidpath(pid, (char*)ma_temp_buffer_get(buffer), MA_MAX_PATH_LEN))
            return MA_ERROR_APIFAILED;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}
#elif defined(HPUX)
ma_error_t ma_pid_to_stat(long pid, struct stat *stat_buf) {
    if( (pid > 0) && stat_buf) {
        /*TBD */
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}
ma_error_t ma_pid_to_path(long pid, ma_temp_buffer_t *buffer) {
    if( (pid > 0) && buffer) {
        /*TODO */
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

#endif

