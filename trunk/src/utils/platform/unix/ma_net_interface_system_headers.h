#ifndef MA_NET_INTERFACE_SYSTEM_HEADERS_H_INCLUDED
#define MA_NET_INTERFACE_SYSTEM_HEADERS_H_INCLUDED

#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>

#if defined(__linux__) || defined(LINUX)
	#include <net/if.h>
	#include <ifaddrs.h>
	#include <netdb.h>
	#include <sys/ioctl.h>
	#include <arpa/inet.h>

	#define USE_IFADDR	1
#endif
#if defined(MACX)
	#include <net/if.h>
	#include <ifaddrs.h>
	#include <netdb.h>
	#include <sys/ioctl.h>
	#include <net/if_dl.h>
	#include <arpa/inet.h>

	#define USE_IFADDR	1
#endif
#if defined(HPUX)
	#ifdef itanium
	typedef union mpinfou {         /* For lint */
			struct pdk_mpinfo *pdkptr;
			struct mpinfo *pikptr;
	} mpinfou_t;
	#endif 
	#include <net/if.h>
	#include <netdb.h>
	#include <sys/ioctl.h>
	#include <net/if_arp.h>
	#include <net/if_nd.h> //need only for getting mac address.
	#include <netinet/in.h>
	#include <arpa/inet.h>

	typedef ma_int32_t ma_ifnum;
	typedef struct ifconf ma_ifconf;
	typedef struct ifreq ma_ifreq;
	
	#define MA_SIO_IFNUM		SIOCGIFNUM
	#define MA_SIO_IFCONF		SIOCGIFCONF
	#define MA_SIO_IFINDEX		SIOCGIFINDEX
	#define MA_SIO_IFFLAGS		SIOCGIFFLAGS
	#define MA_SIO_IFADDR		SIOCGIFADDR
	#define MA_SIO_NETMASK		SIOCGIFNETMASK
	
	
	#define ma_ifc_iflen		ifc_len
	#define ma_ifc_ifbuf		ifc_buf
	#define ma_ifr_index		ifr_index
	#define ma_ifr_flags		ifr_flags
	#define ma_ifr_addr			ifr_addr
	#define ma_ifr_name			ifr_name
	
	#define MA_IFC_LEN(size) 	size * sizeof(ma_ifreq);
	#define MA_IFC_LEN_LOOP(i,size) 	for ( i=0 ; i <  size ; i++) 
	
	#define USE_IFCONF	1
	
	ma_error_t fill_V6_Data(ma_net_interface_list_t *list, ma_bool_t b_loopback) ;
#endif
#if defined(SOLARIS)
	#include <net/if.h>
	#include <netdb.h>
	#include <sys/ioctl.h>
	#include <sys/sockio.h>
	#include <net/if_arp.h>
	#include <arpa/inet.h>
	
	typedef struct lifnum ma_ifnum;
	typedef struct lifconf ma_ifconf;
	typedef struct lifreq ma_ifreq;
	
	#define MA_SIO_IFNUM		SIOCGLIFNUM
	#define MA_SIO_IFCONF		SIOCGLIFCONF
	#define MA_SIO_IFINDEX		SIOCGLIFINDEX
	#define MA_SIO_IFFLAGS		SIOCGLIFFLAGS
	#define MA_SIO_IFADDR		SIOCGLIFADDR
	#define MA_SIO_NETMASK		SIOCGLIFNETMASK
	
	
	#define ma_ifc_iflen		lifc_len
	#define ma_ifc_ifbuf		lifc_buf
	#define ma_ifr_index		lifr_index
	#define ma_ifr_flags		lifr_flags
	#define ma_ifr_addr			lifr_addr
	#define ma_ifr_name			lifr_name

	#define MA_IFC_LEN(size) 	size.lifn_count * sizeof(ma_ifreq);
	#define MA_IFC_LEN_LOOP(i,size) 	for ( i=0 ; i <  size.lifn_count ; i++) 
	
	#define USE_IFCONF	1
#endif
#if defined(AIX)
	#include <sys/ioctl.h>
	#include <stropts.h>
	#include <net/if.h>
	#include <netdb.h>
	#include <sys/ioctl.h>
	#include <net/if_arp.h>
	//#include <net/if_nd.h>
	#include <unistd.h>
	#include <arpa/inet.h>
	#include <sys/ndd_var.h>
	#include <sys/kinfo.h>
	
	typedef ma_int32_t ma_ifnum;
	typedef struct ifconf ma_ifconf;
	typedef struct ifreq ma_ifreq;
	
	#define MA_SIO_IFNUM		SIOCGSIZIFCONF
	#define MA_SIO_IFCONF		SIOCGIFCONF
	#define MA_SIO_IFINDEX		SIOCGIFINDEX
	#define MA_SIO_IFFLAGS		SIOCGIFFLAGS
	#define MA_SIO_IFADDR		SIOCGIFADDR
	#define MA_SIO_NETMASK		SIOCGIFNETMASK
	
	
	#define ma_ifc_iflen		ifc_len
	#define ma_ifc_ifbuf		ifc_buf
	#define ma_ifr_index		ifr_index
	#define ma_ifr_flags		ifr_flags
	#define ma_ifr_addr			ifr_addr
	#define ma_ifr_name			ifr_name
	
	#define MA_IFC_LEN(size) 	size * sizeof(ma_ifreq);
	//#define MA_IFC_LEN_LOOP(size) 	(size / sizeof(ma_ifreq) ) + 1
	#define MA_IFC_LEN_LOOP(counter,size) \
	ifrp = ifc.ifc_req; \
	cpPos = (char *)ifc.ifc_req;\
	cpLimit = cpPos+ifc.ifc_len; \
	for(; cpPos < cpLimit;cpPos+=(sizeof(ifrp->ifr_name) + SIZE(ifrp->ifr_addr)))
	#define USE_IFCONF	1
	
	#define MAX(x,y) ((x) > (y) ? (x) : (y))
	#define SIZE(p) MAX((p).sa_len, sizeof(p))
	
	ma_error_t get_net_mask_and_addr(char *str_net_mask, char *str_net_addr, char* if_name, struct sockaddr *ipaddr);
	
#endif

#if defined(USE_IFCONF) && !defined(MA_IFC_LEN_LOOP)
#error "Check definition for MA_IFC_LEN_LOOP"
#endif

#endif //MA_NET_INTERFACE_SYSTEM_HEADERS_H_INCLUDED
