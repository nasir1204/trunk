#include <ma/internal/utils/network/ma_net_interface.h>
#include "ma_net_interface_system_headers.h"

#ifdef USE_IFCONF
extern int errno;
#ifdef AIX
ma_error_t get_mac_address(int family, int sock, ma_ifreq *ifreq, char *mac_address) {
	size_t size;
	struct kinfo_ndd *nddp = NULL;
    struct kinfo_ndd *nddp_itr = NULL;
	ma_error_t error = MA_ERROR_APIFAILED;
	char if_name[IFNAMSIZ] = {0};
	void *end;
	
	strncpy(if_name, ifreq->ma_ifr_name, IFNAMSIZ);
	if ( 0 < (size = getkerninfo(KINFO_NDD, 0, 0, 0))) {
		if(NULL != (nddp = (struct kinfo_ndd *)calloc(1, size))) {
			if(0 <= getkerninfo(KINFO_NDD, nddp, &size, 0)) {
				nddp_itr = nddp;
				end = (void *)(nddp + size);
				while( (((void*)nddp_itr) < end ) && error != MA_OK) {
					if( 0 == strncmp(nddp_itr->ndd_alias, if_name, strlen(if_name)) || 
						0 == strncmp(nddp_itr->ndd_name, if_name, strlen(if_name))) {
						error = MA_OK;
						memcpy(mac_address, nddp_itr->ndd_addr, MA_MAC_ADDRESS_SIZE);
					}
					else
						nddp_itr++;
				}
			}
			
			if(nddp) {
				free(nddp);
				nddp = NULL;
			}
		}
	}
	return error;
}
#elif HPUX
ma_error_t get_mac_address(int family, int sock, void *addr1, char *mac_address) {
	//If the family is IPV4 , the use arpreq to get the MAC address , Don't know when MAC OS will make one funtion to do that */	
	if(AF_INET == family )	{
		ma_ifreq *addr = (ma_ifreq *) addr1;
		char *strIfName = addr->ma_ifr_name;
		struct arpreq arpreq ;
		memset( &arpreq , 0 , sizeof(struct arpreq));
		*(&(arpreq.arp_pa)) = (addr->ma_ifr_addr) ; //Assign IP address
		if( -1 == ioctl(sock , SIOCGARP , &arpreq))
			return MA_ERROR_APIFAILED;
		memcpy(mac_address, &arpreq.arp_ha.sa_data  , MA_MAC_ADDRESS_SIZE);
	}
	else
	{
		struct if_laddrreq *addr = (ma_ifreq *) addr1;
		char *strIfName = addr->iflr_name;
		struct ndreq ndreq ;
		memset(&ndreq , 0 , sizeof(struct ndreq));
		strncpy(ndreq.nd_name , strIfName, IFNAMSIZ -1);
		*(&(ndreq.nd_pa)) = *((struct sockaddr_storage *)addr) ; //Assign IP address
		if( -1 == ioctl( sock , SIOCGND , &ndreq))
			return MA_ERROR_APIFAILED;
		memcpy(mac_address, ((struct sockaddr *) &(ndreq.nd_ha))->sa_data  , MA_MAC_ADDRESS_SIZE);
	}
	return MA_OK;
}
#elif SOLARIS
ma_error_t get_mac_address(int family, int sock, ma_ifreq *ifreq, char *mac_address) {
	if(AF_INET == family ) {
		struct arpreq arpreq ;
		struct sockaddr *addr = (struct sockaddr *)&(ifreq->ma_ifr_addr) ;
		*(&arpreq.arp_pa) = *addr ; //Assign IP address
		if( -1 == ioctl(sock , SIOCGARP , &arpreq))
			return MA_ERROR_APIFAILED;
		memcpy(mac_address, &arpreq.arp_ha.sa_data  , 6);
	}
	//For IPV6 family , we have to use ioctl on solaris , really frustrated with all OS giving different methods , but live with it
	else {
		if( -1 == ioctl( sock , SIOCLIFGETND , ifreq  ))
			return MA_ERROR_APIFAILED ;
		memcpy(mac_address, &(ifreq->lifr_nd.lnr_hdw_addr)  , 6);
	}
	return MA_OK;
}
#endif

ma_error_t extract_ip_from_addr(char *str_ip_address, struct sockaddr *addr, ma_int32_t len) {
	char buf[NI_MAXHOST];
       
	if(str_ip_address == NULL || addr == NULL)
		return MA_ERROR_INVALIDARG;
	memset(buf,0,NI_MAXHOST);
	
	#ifdef AIX
	
	if( AF_INET == addr->sa_family)
		strcpy(buf,inet_ntoa(((struct sockaddr_in *)addr)->sin_addr));
	else if( AF_INET6 == addr->sa_family )
		inet_ntop(AF_INET6,(struct in6_addr *)&(((struct sockaddr_in6 *)addr)->sin6_addr),
			buf, NI_MAXHOST);
	else 
		return MA_ERROR_INVALIDARG;
	
	#else
	
	if( 0 != getnameinfo(addr,len,buf,NI_MAXHOST,NULL,0,NI_NUMERICHOST) ) {
		return MA_ERROR_APIFAILED;
	}
	
	#endif
	
	strncpy(str_ip_address , buf, MA_IPV6_ADDR_LEN);
	return MA_OK;
}

ma_error_t get_subnet_addr(char * strNetAddr, struct sockaddr *ipaddr , struct sockaddr *maskaddr)
{
	struct  in_addr inaddr ;

    if( !ipaddr || !maskaddr || !strNetAddr )
			return MA_ERROR_INVALIDARG;

	inaddr.s_addr = (((struct sockaddr_in *)ipaddr)->sin_addr.s_addr
										&
					((struct sockaddr_in *)maskaddr)->sin_addr.s_addr);

	strncpy(strNetAddr, inet_ntoa(inaddr), MA_IPV6_ADDR_LEN);
    return MA_OK;
}

ma_error_t get_broadcast_addr(char * bAddr, struct sockaddr *ipaddr , struct sockaddr *maskaddr)
{
	struct  in_addr inaddr ;

    if( !ipaddr || !maskaddr || !bAddr )
			return MA_ERROR_INVALIDARG;

	inaddr.s_addr = (((struct sockaddr_in *)ipaddr)->sin_addr.s_addr
										&
					((struct sockaddr_in *)maskaddr)->sin_addr.s_addr) | ~(((struct sockaddr_in *)maskaddr)->sin_addr.s_addr);

	strncpy(bAddr, inet_ntoa(inaddr), MA_IPV6_ADDR_LEN);
    return MA_OK;
}

ma_error_t ma_net_interface_initialize(ma_net_interface_list_t *list,ma_bool_t bIncludeLoopback) {
	ma_int32_t i,oi;
	ma_uint32_t family;
	ma_error_t ret;
	ma_error_t extra = MA_ERROR_APIFAILED;
	char strIPAddr[MA_IPV6_ADDR_LEN];
	char strNetMask[MA_IPV6_ADDR_LEN];
	char strNetAddr[MA_IPV6_ADDR_LEN];
	char bAddr[MA_IPV6_ADDR_LEN];
#if defined(SOLARIS) // for HPUX we don't need a loop
	for(oi = 0, family = AF_INET; oi < 2 ; ++oi, family = AF_INET6) 
#elif defined (AIX)
	family = AF_INET6;
	for(oi=0;oi<1;oi++)
#elif defined (HPUX)
	extra = fill_V6_Data(list,bIncludeLoopback);
	family = AF_INET;
	for(oi=0;oi<1;oi++)
#else
#error "Platform to be supported by this method needs to be looked up"
#endif
	{
		ma_int32_t sock = -1;
		ma_int32_t inf_num;
		ma_int32_t index;
		ma_int32_t len;
		ma_ifnum size;
		ma_ifconf ifc;		
		ma_ifreq *ifr; 	//Will be pointed to the dynamic allocation with the data
		ma_ifreq *ifrp;	//dummy pointer to iterate the array
		ma_ifreq *ifrc; // a copy of the pointer to get the IFFLAGS and such
		char *cpPos,*cpLimit; //for AIX only
		
		if(-1 == (sock = socket(family, SOCK_DGRAM, 0) )) {
			continue;
		}
		
		//Find out the size of ifconf that must be allocated
		#ifdef SOLARIS
		size.lifn_family = AF_UNSPEC;
		size.lifn_flags = 0;
		#endif
		
		
		if(-1 == ioctl(sock,MA_SIO_IFNUM,&size)){
			close(sock);
			if(MA_OK != extra) {
				ma_net_interface_list_reset(list);
				return MA_ERROR_APIFAILED;
			}
			else
				return MA_OK;
		}

		ifc.ma_ifc_iflen = MA_IFC_LEN(size)
		ifr = (ma_ifreq *) malloc(ifc.ma_ifc_iflen);
		if(NULL == ifr) {
			close(sock);
			ma_net_interface_list_reset(list);
			return MA_ERROR_OUTOFMEMORY;
		}
		
		#ifdef SOLARIS
		ifc.lifc_family = AF_UNSPEC;
		ifc.lifc_flags = 0;
		#endif 
		//perform ifconf
		ifc.ma_ifc_ifbuf = (char*) ifr;
		if(-1 == ioctl(sock,MA_SIO_IFCONF,&ifc)){
			free(ifr);
			close(sock);
			if(MA_OK != extra) {
				ma_net_interface_list_reset(list);
				return MA_ERROR_APIFAILED;
			}
			else
				return MA_OK;
		}
		
		MA_IFC_LEN_LOOP(i,size) {
			struct sockaddr ipaddr;
			struct sockaddr netmaskaddr;
			ma_net_interface_t * inf =  NULL;
			ma_net_address_info_t *addrinfo = NULL;
			ma_int32_t sizeof_ifreq = 0;
			char str_if_name[MA_INTERFACE_NAME_SIZ] = {0};
			char mac_address[MA_MAC_ADDRESS_SIZE]= {0};
			
			//Set the pointer to the loop counter
			#ifdef AIX
			sizeof_ifreq = (sizeof(ifrp->ifr_name) + SIZE(ifrp->ifr_addr));
			ifrp = (struct ifreq*) cpPos;
			#else
			sizeof_ifreq = sizeof(ma_ifreq);
			ifrp = ifr + i;
			#endif
			
			//Make a copy of the ifrp. Allocate the size 
			ifrc = calloc(1,sizeof_ifreq);
			if(!ifrc) {			
				free(ifr);
				close(sock);
				ma_net_interface_list_reset(list);
				return MA_ERROR_OUTOFMEMORY;
			}
			
			memcpy(ifrc, ifrp, sizeof_ifreq);
			strncpy(ifrc->ma_ifr_name,ifrp->ma_ifr_name,IFNAMSIZ);
			strncpy(str_if_name,ifrp->ma_ifr_name,IFNAMSIZ);
			
			//Get the index
			#ifdef AIX
			strncpy(str_if_name, ifrp->ifr_name, IFNAMSIZ);
			index = if_nametoindex(str_if_name);
			#else
			if( -1 == ioctl(sock,MA_SIO_IFINDEX,ifrc)){
				free(ifrc);
				ifrc = NULL;
				continue;
			}
			index = ifrc->ma_ifr_index;
			#endif
			
			//Get the if flags
			if( -1 == ioctl(sock, MA_SIO_IFFLAGS, ifrc )) {
				free(ifrc);
				ifrc = NULL;
				continue;
			}
			if(ifrc->ma_ifr_flags & IFF_UP == 0 ){
				free(ifrc);
				ifrc = NULL;
				continue;
			}
			if((ifrc->ma_ifr_flags & IFF_LOOPBACK == 0)  && !bIncludeLoopback){
				free(ifrc);
				ifrc = NULL;
				continue;
			}
			
			
			
			//Get the IP Address
			#ifdef AIX
			family = ifrp->ifr_addr.sa_family;
			len = ifrp->ifr_addr.sa_family == AF_INET? sizeof(struct sockaddr_in):sizeof(struct sockaddr_in6);
			memcpy(&ipaddr, &ifrp->ifr_addr, len);
			#else
			len = family == AF_INET ? sizeof(struct sockaddr_in) : sizeof(struct sockaddr_in6);
			if(-1 == ioctl(sock,MA_SIO_IFADDR,ifrc)){
				free(ifrc);
				continue;
			}
			memcpy(&ipaddr,(struct sockaddr*)&(ifrc->ma_ifr_addr),len);
			#endif
			
			if(MA_OK != extract_ip_from_addr(strIPAddr,&ipaddr,len)) {
				free(ifrc);
				ifrc = NULL;
				continue;
			}
			
			// Get the mac address 
			#ifdef AIX
			if(	(strncmp(str_if_name,"sit",3)!=0 ) &&
				(strncmp(str_if_name,"et",2)!=0 ) &&
				(ifrc->ma_ifr_flags & IFF_LOOPBACK )!= IFF_LOOPBACK ) {
				get_mac_address(family, 0, ifrc, mac_address);
			}
			#elif defined(SOLARIS) 
			if_indextoname(index,str_if_name);
			if(MA_OK !=get_mac_address(family, sock, ifrc, mac_address)){
					continue;
			}
			#elif defined(HPUX)
			if(MA_OK !=get_mac_address(family, sock, ifrc, mac_address)){
					continue;
			}
			#endif
			//Get the Net MASK and Broadcast Address
			#ifndef AIX
			if(-1 == ioctl(sock,MA_SIO_NETMASK,ifrp)){
				free(ifrc);
				ifrc = NULL;
				continue;
			}
			memcpy(&netmaskaddr,(struct sockaddr*) &(ifrp->ma_ifr_addr),len);
			if(MA_OK != extract_ip_from_addr(strNetMask,&netmaskaddr,len)) {
				free(ifrc);
				ifrc = NULL;
				continue;
			}
			
			if(family == AF_INET){
				get_subnet_addr(strNetAddr,&ipaddr,&netmaskaddr);
				get_broadcast_addr(bAddr,&ipaddr,&netmaskaddr);
			}
			#else
			if(MA_OK != get_net_mask_and_addr(strNetMask,strNetAddr,ifrc->ma_ifr_name,&ipaddr)) {
				free(ifrc);
				ifrc = NULL;
				continue;
			}
			#endif
			
			
			ret = ma_address_info_create(&addrinfo);
            if(ret != MA_OK) {
				free(ifrc);
                free(ifr);
                close(sock);
                ma_net_interface_list_reset(list);
                return ret;
            }   
			
			if(family == AF_INET) {
                addrinfo->family = MA_IPFAMILY_IPV4;
                addrinfo->ipv6_scope_id = 0;
                strncpy(addrinfo->ip_addr , strIPAddr, MA_IPV6_ADDR_LEN);
                strncpy(addrinfo->subnet_mask , strNetMask , MA_IPV6_ADDR_LEN);
				strncpy(addrinfo->subnet_addr , strNetAddr, MA_IPV6_ADDR_LEN);
                strncpy(addrinfo->broadcast_addr , bAddr, MA_IPV6_ADDR_LEN);
			}   
            else
            {   
                addrinfo->ipv6_scope_id = index;
                addrinfo->family = MA_IPFAMILY_IPV6;
                strncpy(addrinfo->ip_addr, strIPAddr, MA_IPV6_ADDR_LEN);
				memset(addrinfo->subnet_mask, 0, MA_IPV6_ADDR_LEN);
				memset(addrinfo->broadcast_addr, 0, MA_IPV6_ADDR_LEN);
                strncpy(addrinfo->subnet_mask , "N/A", strlen("N/A"));
                strncpy(addrinfo->broadcast_addr,"N/A", strlen("N/A"));
			}   
    
            ret = ma_net_interface_create(&inf);
            if(MA_OK != ret) {  
				free(ifrc);
                free(ifr);
                close(sock);
                ma_address_info_release(addrinfo);
                ma_net_interface_list_reset(list);
                return ret;
            }
			memcpy(inf->mac_address, mac_address, MA_MAC_ADDRESS_SIZE);
			strncpy(inf->interface_name, str_if_name, MA_INTERFACE_NAME_SIZ);
			ma_net_interface_set_ipfamily(inf,addrinfo->family);
            ma_net_interface_add_address(inf,&addrinfo);
            addrinfo = NULL;
            inf->index = index;
            ma_net_interface_list_add_interface(list, &inf);
			free(ifrc);			//Freeing it for every loop since in AIX there is a dependancy on structure size.
		}		
		if(ifr)
			free(ifr);
		close(sock);
		extra = MA_OK;
	}
	return extra == MA_OK? MA_OK : MA_ERROR_APIFAILED;
}	

#ifdef AIX
ma_error_t get_net_mask_and_addr(char *str_net_mask, char *str_net_addr, char* if_name, struct sockaddr *ipaddr) {
	char buff[2048];
	ma_int32_t sock;
	struct ifconf ifc;
	ma_int32_t i;
	
	if(str_net_mask == NULL || str_net_addr==NULL  || if_name== NULL || ipaddr == NULL)
		return MA_ERROR_INVALIDARG;
	if ((sock = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
	{
			close(sock);
			return MA_ERROR_APIFAILED;
	}

	ifc.ifc_len = sizeof(buff);
	ifc.ifc_buf = buff;

	if (ioctl(sock, SIOCGIFCONF, &ifc)  == -1) {
			close(sock);
			return MA_ERROR_APIFAILED;
	}

	for(i = 0 ; i <ifc.ifc_len; i+= sizeof(struct ifreq)) {
		struct ifreq *ifrp = (struct ifreq *) (ifc.ifc_buf + i );
		if(strncmp(if_name,ifrp->ifr_name, IFNAMSIZ)== 0) {
			if (ioctl(sock, SIOCGIFNETMASK, ifrp) == -1) {
					close(sock);
					return MA_ERROR_APIFAILED;
			}
			struct in_addr nmask;
			nmask = ((struct sockaddr_in *)&ifrp->ifr_addr)->sin_addr;
			get_subnet_addr(str_net_addr,ipaddr , (struct sockaddr *)&(ifrp->ifr_addr));
			strncpy(str_net_mask, inet_ntoa(nmask),MA_IPV6_ADDR_LEN);
			break;
		}
	}
	close(sock);
	return MA_OK;
}
#endif

#ifdef HPUX
ma_error_t fill_V6_Data(ma_net_interface_list_t *list, ma_bool_t b_loopback) {
	ma_int32_t sock = -1,i;
	ma_int32_t family = AF_INET6;
	ma_int32_t lifn  = 0 ;
	ma_error_t error = MA_OK;
	
	struct if_laddrconf lifc;
	struct if_laddrreq *lifreqp;
	
	if( -1 == (sock = socket(family , SOCK_DGRAM , 0 )))
		return MA_ERROR_APIFAILED;
	
	if( -1 == ioctl(sock, SIOCGLIFNUM, &lifn)) {
		close(sock);
		return MA_ERROR_APIFAILED;
	}
	
	lifc.iflc_len = lifn * sizeof ( struct if_laddrreq);
	lifreqp = (struct if_laddrreq *) malloc(lifc.iflc_len);
	if( NULL == lifreqp) {
		close(sock);
		return MA_ERROR_OUTOFMEMORY;
	}
	lifc.iflc_buf = (char *)lifreqp;

	if (ioctl(sock, SIOCGLIFCONF, (char *)&lifc) == -1) {
		free(lifreqp);
		close(sock);
		return MA_ERROR_APIFAILED;
	}	
	
	for( i = 0 ; i < lifn   ; i++) {
		struct if_laddrreq lifreq;
		unsigned long index ;
		ma_int32_t ret;
		socklen_t len;
		char ip_addr[MA_IPV6_ADDR_LEN];
		ma_net_address_info_t *addr = NULL;
		ma_net_interface_t* inf = NULL;
		char if_name[MA_INTERFACE_NAME_SIZ] = {0};
		char mac_address[MA_MAC_ADDRESS_SIZE] = {0};
		
		memcpy(&lifreq, lifreqp + i , sizeof lifreq);
		strncpy(if_name, lifreq.iflr_name, IFNAMSIZ);
		if( -1 == ioctl(sock , SIOCGLIFINDEX , &lifreq ))
			continue ;

		index = lifreq.iflr_index;

		if( -1 ==  ioctl(sock , SIOCGLIFFLAGS, (char *)&lifreq))
			continue ;			
		/* interface is not up or it is a loop back and user have not asked to include loopback,
					   or it is not from the AF_INET family
		*/
		
		if(( (lifreq.iflr_flags  & IFF_UP) == 0)
				||  ( (lifreq.iflr_flags  & IFF_LOOPBACK )  && !b_loopback) 
				)
			continue ;
		
		if( -1 == ioctl(sock ,SIOCGLIFADDR, (char *)&lifreq))
			continue ;
				
		len =  sizeof(struct sockaddr_in6) ;
		if(MA_OK != extract_ip_from_addr( ip_addr , (struct sockaddr *)&(lifreq.iflr_addr) , len))
			continue ;
		
		if(MA_OK !=get_mac_address(family, sock, &lifreq, mac_address)){
		}
			
		error = ma_address_info_create(&addr);
		if(MA_OK != error) {
			ma_net_interface_list_reset(list);
			close(sock);
			free(lifreqp);
			return MA_ERROR_OUTOFMEMORY;
		}
		error = ma_net_interface_create(&inf);
		if(MA_OK != error) {
			ma_address_info_release(addr);
			ma_net_interface_list_reset(list);
			close(sock);
			free(lifreqp);
			return MA_ERROR_OUTOFMEMORY;
		}
		
		addr->family = MA_IPFAMILY_IPV6;
		addr->ipv6_scope_id = index;
		strncpy(addr->ip_addr, ip_addr, MA_IPV6_ADDR_LEN);
		strncpy(addr->subnet_mask, "N/A" ,strlen("N/A"));
		strncpy(addr->broadcast_addr, "N/A" ,strlen("N/A"));
		
		memcpy(inf->mac_address, mac_address, MA_MAC_ADDRESS_SIZE);
		strncpy(inf->interface_name, if_name, MA_INTERFACE_NAME_SIZ);
		inf->index = index;
		ma_net_interface_set_ipfamily(inf,addr->family);
		ma_net_interface_add_address(inf,&addr);

		ma_net_interface_list_add_interface(list,&inf);
	}
	close (sock);
	free(lifreqp);
	return MA_TRUE;
}
#endif
#endif

