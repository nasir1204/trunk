#include "ma/internal/utils/platform/ma_system_property.h"
#include "ma_system_property_nix.h"

MA_CPP(extern "C" {)

typedef enum ma_platform_e{
		MA_PLAT_SERVER = 0,
		MA_PLAT_CLIENT,
		MA_PLAT_WORKSTATION
	}ma_platform_t;

extern ma_logger_t *g_system_property_logger;
ma_error_t ma_collect_logged_on_users(char user_name_list[], size_t length);


ma_locales_table_t lang_id_table[] =
{
	{ "0409", "en_US"  }, /* NOTE: please don't change the order */
	{ "0809", "en_UK"  },
	{ "0411", "ja_JP"  },
	{ "4009", "en_US"  }, /* MA Agent does not support en-IN(English India) culture identifier, so fall back to 0409(en-US) */
	{ NULL,       NULL }
};

ma_error_t system_property_computer_name_get(char computer_name[], size_t length)
{
	struct utsname info;
	int i=0;
    if( -1 == uname(&info))
	{
		MA_LOG(g_system_property_logger, MA_LOG_SEV_TRACE, "uname call failed for system name, setting it to NA");
		strncpy(computer_name, MA_NA, length-1);
        return MA_OK;
	}
    if(strchr(info.nodename, '.'))
	{
		for(i=0; (i < length-1) && (info.nodename[i] != '.'); i++)
		{
			computer_name[i] = info.nodename[i];
		}
	}
	else
		strncpy(computer_name, info.nodename, length-1);

	return MA_OK;
}

ma_error_t system_property_computer_description_get(char computer_desc[], size_t length)
{
	/* Copmputer Desc is not supported in non-windows system. Setting it to N/A" */
	strncpy(computer_desc, MA_NA, length-1);
	return MA_OK;
}

ma_error_t system_property_is_portable_get(char is_portable[], size_t length)
{
#if defined(MACX)
		char buf[256] = {0};
		size_t len = 256;
		char *p = buf;
		char temp;
		if (sysctlbyname("hw.model", &buf, &len, NULL, 0) == 0)
		{
			buf[sizeof(buf)-1] = 0;
			while (*p)
			{
				temp = tolower(*p);
                *p=temp;
                p++;
			}

			/* If we find the string "book" in the model, then we assume
			   it is one of the many Mac laptop/notebook models. */
			if (strstr(buf, "book"))
				strncpy(is_portable, "1", length-1);
			else
				strncpy(is_portable, "0", length-1);
		}
		else
			strncpy(is_portable, MA_NA, length-1);
#else
	strncpy(is_portable, MA_NA, length-1);
	MA_LOG(g_system_property_logger, MA_LOG_SEV_TRACE, "except non win & non mac, setting to NA");
#endif
	return MA_OK;
}

ma_error_t system_property_user_name_get(char user_name[], size_t length)
{
	return ma_collect_logged_on_users(user_name, length);
}

ma_error_t system_property_time_zone_info_get(char time_zone[], size_t length)
{
	tzset();
	if(tzname[0])
		strncpy(time_zone, tzname[0], length-1);
	else
		strncpy(time_zone, MA_NA, length-1);

	return MA_OK;
}

#if defined (HPUX) || defined (AIX)
static long altzone_get()
{
	struct tm local;
	time_t now1, now2;
	long timezone;
	#if defined (HPUX)
		long altzone;
		int daylight;
	#else
	time_t altzone;
	#endif


	now1 = time(NULL);
	local = *(localtime(&now1));

	/* force opposite STD/DST rules and re-process time */
	local.tm_isdst = !local.tm_isdst;
	now2 = mktime(&local);

	/* calculate altzone, or use 0 if no 'daylight' time */
	altzone = (daylight ? timezone-(now1-now2) : 0);
	return altzone ;
}
#endif

ma_error_t system_property_time_zone_bias_get(char time_zone[], size_t length)
{
	long offset = 0;
	time_t ct = time(NULL);
	struct tm t = {0} ;
	if( NULL != localtime_r(&ct , &t) )
	{
#if !defined(SOLARIS) && !defined(HPUX) && !defined(AIX)
	  offset = 0 - t.tm_gmtoff;
#else
#if defined (HPUX) || defined (AIX)
	long altzone = altzone_get();
#endif
	ma_bool_t dst = MA_FALSE;
	dst = t.tm_isdst > 0;
	offset = dst ? altzone : timezone;
#endif
	}

	snprintf(time_zone, length-1, "%ld", (offset/60));
	return MA_OK;
}

ma_error_t system_property_system_time_get(char sys_time[], size_t length)
{
	struct tm ct= {0};
    time_t tcd = time(NULL);
	char lu[64] = {0};
    if( NULL != localtime_r(&tcd , &ct) )
	{
		snprintf(lu, 64,"%02d/%02d/%04d %02d:%02d:%02d",ct.tm_mon + 1, ct.tm_mday,
				ct.tm_year+1900 , ct.tm_hour, ct.tm_min, ct.tm_sec);
		strncpy(sys_time,lu, length - 1);
	}
	else
		strncpy(sys_time,MA_NA, length - 1);

	return MA_OK;
}

static ma_error_t lang_environment_get(char language[], size_t length)
{
	char		*lang_id = getenv("LANG");
	if(lang_id){
		strncpy(language, lang_id, length-1);
	}
	else{
		strncpy(language, MA_NA, length-1);
	}
	return MA_OK;
}

ma_error_t system_property_lang_code_get(char code[], size_t length) {
	char language[32] = {0};

	if(MA_OK == lang_environment_get(language, 32)) {
		if(strcmp(language, MA_NA)) {
			size_t i= 0;
			while(lang_id_table[i].code) {
				/*seen language as "en_US.UTF-8", so looking for partial match*/
				if(strstr(language, lang_id_table[i].name)) {
					strncpy(code, lang_id_table[i].code, length-1);
					return MA_OK;
				}
				i++;
			}
		}
	}
	/*ePO expect valid code, so default will be English */
	strncpy(code, "0409", length-1);
	return MA_OK;
}

ma_error_t system_property_lang_id_get(char language[], size_t length)
{
	return system_property_lang_code_get(language, length);
}

ma_error_t system_property_ipx_addr_get(char ipx_addr[], size_t length)
{
	strncpy(ipx_addr,  MA_NA, length-1);
	return MA_OK;
}

ma_error_t system_property_host_name_get(char host_name[], size_t length)
{
	if(-1 ==  gethostname(host_name,length-1))
    {
     MA_LOG(g_system_property_logger, MA_LOG_SEV_TRACE, "gethostname api failed, setting it to NA");
	   strncpy(host_name, MA_NA, length-1);
    }
	return MA_OK;
}

ma_error_t system_property_domain_name_get(char domain_name[], size_t length)
{
	char domain_buff[MA_MAX_PATH_LEN] = {0};
#if defined(MACX)
	size_t oldlen = 1024;
	int mib[2];
	mib[0] = CTL_KERN;
	mib[1] = KERN_NISDOMAINNAME;

	int retval = sysctl(mib,2, domain_buff, &oldlen,NULL,0);
	if ((retval == -1) || (strlen(domain_buff ) == 0))
	{
		MA_LOG(g_system_property_logger, MA_LOG_SEV_TRACE, "domain name fetching failed or not available, setting to NA");
		strncpy(domain_name, MA_NA, length-1);
	}
	else
		strncpy(domain_name, domain_buff, length-1);
#elif defined(SOLARIS)
	if((sysinfo(SI_SRPC_DOMAIN,domain_buff,1024) == -1) || (strlen(domain_buff) == 0))
	{
		strncpy(domain_name, MA_NA, length-1);
	}
	else
		strncpy(domain_name, domain_buff, length-1);
#else
	if ((getdomainname(domain_buff,1024) == -1) || (strlen(domain_buff) == 0))
   	{
		strncpy(domain_name, MA_NA, length-1);
	}
	else
		strncpy(domain_name, domain_buff, length-1);
#endif

	return MA_OK;
}

ma_error_t add_drive_info(ma_file_system_property_t *fs, char *drive_name, double block_size, double drive_free_space, double drive_total_space, double *machine_total_mbs, double *machine_free_mbs)
{
	double disk_blocks_per_mb = 0;
	double disk_total_mbs     = 0;
	double disk_free_mbs      = 0;
	char total_mount_space[32] = {0};
	char free_mount_space[32] = {0};
	disk_blocks_per_mb = 1024*1024*1.0 / block_size;
	disk_total_mbs = drive_total_space / disk_blocks_per_mb;
	disk_free_mbs = drive_free_space / disk_blocks_per_mb;
	*machine_total_mbs += disk_total_mbs;
	*machine_free_mbs  += disk_free_mbs;
	snprintf(total_mount_space, 32,  "%0.2lf", disk_total_mbs);
	snprintf(free_mount_space, 32, "%0.2lf",  disk_free_mbs);
	strncpy(fs->drive_name, drive_name, 63);
	strncpy(fs->drive_totalspace, total_mount_space, 31);
	strncpy(fs->drive_freespace, free_mount_space, 31);
	return MA_OK;
}

ma_error_t system_property_disk_get(ma_disk_system_property_t *disk_info)
{
	double machine_total_mbs = 0;
	double machine_free_mbs  = 0;
	int no_hard_drvs = 0 ;
	char temp[64] = {0};
	memset(disk_info, 0, sizeof(ma_disk_system_property_t));
#if defined(MACX)
	int i =0;
	int num = getfsstat(NULL, 0, MNT_WAIT);
	struct statfs *buf =  (struct statfs*) malloc(sizeof(struct statfs) * num);
	if(!buf)
	{
		MA_LOG(g_system_property_logger, MA_LOG_SEV_TRACE, "memory allocation for statfs failed, setting it default zero file systems");
		strncpy(disk_info->total_filesystems, MA_NA, 3);
		strncpy(disk_info->total_diskspace, MA_NA, 31);
		strncpy(disk_info->free_diskspace, MA_NA, 31);
		return MA_OK;
	}
	getfsstat(buf, num*sizeof(struct statfs), MNT_WAIT);
	for (i =0; i < num; i++)
	{
		if (buf[i].f_flags & MNT_LOCAL)
		{
			if ((buf[i].f_bsize == 0) || (buf[i].f_blocks == 0))
				continue;
			no_hard_drvs++;
		}
	}

	disk_info->filesysteminfo = (ma_file_system_property_t*)calloc(no_hard_drvs, sizeof(ma_file_system_property_t));
	if(!disk_info->filesysteminfo)
	{
		MA_LOG(g_system_property_logger, MA_LOG_SEV_ERROR, "memory allocation for statfs failed, setting it default zero file systems");
		strncpy(disk_info->total_filesystems, MA_NA, 3);
		strncpy(disk_info->total_diskspace, MA_NA, 31);
		strncpy(disk_info->free_diskspace, MA_NA, 31);
		return MA_OK;
	}
	no_hard_drvs=0;
	for (i =0; i < num; i++)
	{
		if (buf[i].f_flags & MNT_LOCAL)
		{
			if ((buf[i].f_bsize == 0) || (buf[i].f_blocks == 0))
				continue;

			add_drive_info(&(disk_info->filesysteminfo[no_hard_drvs]),buf[i].f_mntonname, buf[i].f_bsize, buf[i].f_bfree, buf[i].f_blocks, &machine_total_mbs, &machine_free_mbs );
			no_hard_drvs++;
		}
	}
	free(buf);
#else

	char *fsname;
	char *mount_type;
	char *mount_directory;
	int res =0;
	#if (defined(LINUX) ||  defined(HPUX))
		char *colon= ":", *smbfs = "smbfs", *double_slash= "//";
		FILE *fmnt = NULL;
		struct mntent* me = NULL;
		ma_bool_t isRemote = MA_FALSE;
		fmnt = fopen(PATH_MOUNTED , "r");
		if (fmnt == NULL)
		{
			MA_LOG(g_system_property_logger, MA_LOG_SEV_TRACE, "setmntent to _PATH_MOUNTED failed");
			strncpy(disk_info->total_filesystems, MA_NA, 3);
			strncpy(disk_info->total_diskspace, MA_NA, 31);
			strncpy(disk_info->free_diskspace, MA_NA, 31);
			return MA_OK;
		}

		while ( ((me = getmntent(fmnt)) != NULL) )
		{
			fsname = me->mnt_fsname;
			mount_type = me->mnt_type;
			mount_directory = me->mnt_dir;
			isRemote = (strstr(fsname,colon) || !strcmp(mount_type, smbfs) || !memcmp(fsname, double_slash, 2));
			if (!isRemote  )
			{
				struct statfs stfs;
				res = statfs(me->mnt_dir, &stfs);
				if (res == -1)
				continue;
				if ((stfs.f_bsize == 0) || (stfs.f_blocks == 0))
				continue;
				no_hard_drvs++;
			}
		}
		/*Need to investigate, why need to call getmntent twice with reopene file */
		fclose(fmnt);
		me = NULL;
		fmnt = fopen(PATH_MOUNTED , "r");
		if (fmnt == NULL)
		{
			MA_LOG(g_system_property_logger, MA_LOG_SEV_TRACE, "fopen api failed, setting it default zero file systems");
			strncpy(disk_info->total_filesystems, MA_NA, 3);
			strncpy(disk_info->total_diskspace, MA_NA, 31);
			strncpy(disk_info->free_diskspace, MA_NA, 31);
			return MA_OK;
		}
		disk_info->filesysteminfo = (ma_file_system_property_t*)calloc(no_hard_drvs, sizeof(ma_file_system_property_t));
		if(!disk_info->filesysteminfo)
		{
			MA_LOG(g_system_property_logger, MA_LOG_SEV_ERROR, "memory allocation for statfs failed, setting it default zero file systems");
			strncpy(disk_info->total_filesystems, MA_NA, 3);
			strncpy(disk_info->total_diskspace, MA_NA, 31);
			strncpy(disk_info->free_diskspace, MA_NA, 31);
		    fclose(fmnt);
			return MA_OK;
		}
		no_hard_drvs = 0;
		while ( ((me = getmntent(fmnt)) != NULL) )
		{
			fsname = me->mnt_fsname;
			mount_type = me->mnt_type;
			mount_directory = me->mnt_dir;
			isRemote = (strstr(fsname,colon) || !strcmp(mount_type, smbfs) || !memcmp(fsname, double_slash, 2));
			if (!isRemote  )
			{
				struct statfs stfs;
				res = statfs(me->mnt_dir, &stfs);
				if (res == -1)
				continue;
				if ((stfs.f_bsize == 0) || (stfs.f_blocks == 0))
				continue;

				add_drive_info(&(disk_info->filesysteminfo[no_hard_drvs]),mount_directory, stfs.f_bsize, stfs.f_bfree , stfs.f_blocks, &machine_total_mbs, &machine_free_mbs);
				no_hard_drvs++;
			}
		}
		fclose(fmnt);

	#elif defined(AIX)
		char *colon= ":", *smbfs = "smbfs", *double_slash= "//";
		struct fstab *fsent;
		struct statvfs stfs;
		ma_bool_t isRemote = MA_FALSE;
		setfsent();
		while (((fsent=getfsent()) != 0 ) )
		{
			fsname = fsent->fs_spec;
			mount_type = fsent->fs_type;
			mount_directory = fsent->fs_file;
			isRemote = (strstr(fsname,colon) || !strcmp(mount_type, smbfs) || !memcmp(fsname, double_slash, 2));
			if (!isRemote  )
			{
			  res = statvfs(fsent->fs_file, &stfs);
			  if (res == -1)
				continue;
			  if ((stfs.f_bsize == 0) || (stfs.f_blocks == 0))
				continue;

			  no_hard_drvs++;
			}
		}
		endfsent();
		disk_info->filesysteminfo = (ma_file_system_property_t*)calloc(no_hard_drvs, sizeof(ma_file_system_property_t));
		if(!disk_info->filesysteminfo)
		{
			MA_LOG(g_system_property_logger, MA_LOG_SEV_ERROR, "memory allocation for statfs failed, setting it default zero file systems");
			strncpy(disk_info->total_filesystems, MA_NA, 3);
			strncpy(disk_info->total_diskspace, MA_NA, 31);
			strncpy(disk_info->free_diskspace, MA_NA, 31);
			return MA_OK;
		}
		no_hard_drvs = 0;
		while (((fsent=getfsent()) != 0 ) )
		{
			fsname = fsent->fs_spec;
			mount_type = fsent->fs_type;
			mount_directory = fsent->fs_file;
			isRemote = (strstr(fsname,colon) || !strcmp(mount_type, smbfs) || !memcmp(fsname, double_slash, 2));
			if (!isRemote  )
			{
			  res = statvfs(fsent->fs_file, &stfs);
			  if (res == -1)
				continue;
			  if ((stfs.f_bsize == 0) || (stfs.f_blocks == 0))
				continue;

			  add_drive_info(&(disk_info->filesysteminfo[no_hard_drvs]),mount_directory, stfs.f_frsize, stfs.f_bfree , stfs.f_blocks, &machine_total_mbs, &machine_free_mbs );
			  no_hard_drvs++;
			}
		}
		endfsent();
	#elif defined(SOLARIS)
		struct mnttab mnttab_entry ;
		struct statvfs stfs;
		FILE *fp = fopen("/etc/mnttab" , "r");
		if (fp == NULL)
		{
			MA_LOG(g_system_property_logger, MA_LOG_SEV_TRACE, "fopen to /etc/mnttab failed, setting it default zero file systems");
			strncpy(disk_info->total_filesystems, MA_NA, 3);
			strncpy(disk_info->total_diskspace, MA_NA, 31);
			strncpy(disk_info->free_diskspace, MA_NA, 31);
			return MA_OK;
		}
		while ((getmntent(fp , &mnttab_entry) == 0))
		{
			fsname =mnttab_entry.mnt_special;
			mount_type = mnttab_entry.mnt_fstype;
			mount_directory = mnttab_entry.mnt_mountp;

			res = statvfs(mnttab_entry.mnt_mountp, &stfs);
			if (res == -1)
				continue;
			if ((stfs.f_bsize == 0) || (stfs.f_blocks == 0))
				continue;

			no_hard_drvs++;
		}

		disk_info->filesysteminfo = (ma_file_system_property_t*)calloc(no_hard_drvs, sizeof(ma_file_system_property_t));
		if(!disk_info->filesysteminfo)
		{
			MA_LOG(g_system_property_logger, MA_LOG_SEV_ERROR, "memory allocation for filesysteminfo failed, setting it default zero file systems");
			strncpy(disk_info->total_filesystems, MA_NA, 3);
			strncpy(disk_info->total_diskspace, MA_NA, 31);
			strncpy(disk_info->free_diskspace, MA_NA, 31);
			fclose(fp);
			return MA_OK;
		}
		no_hard_drvs = 0;
		resetmnttab(fp);
		while ((getmntent(fp , &mnttab_entry) == 0))
		{
			fsname =mnttab_entry.mnt_special;
			mount_type = mnttab_entry.mnt_fstype;
			mount_directory = mnttab_entry.mnt_mountp;

			res = statvfs(mnttab_entry.mnt_mountp, &stfs);
			if (res == -1)
				continue;
			if ((stfs.f_bsize == 0) || (stfs.f_blocks == 0))
				continue;
			add_drive_info(&(disk_info->filesysteminfo[no_hard_drvs]),mount_directory, stfs.f_frsize, stfs.f_bfree , stfs.f_blocks, &machine_total_mbs, &machine_free_mbs );
			no_hard_drvs++;
		}
		fclose(fp);
	#endif
#endif

		snprintf(temp, 4, "%d",no_hard_drvs);
		strncpy(disk_info->total_filesystems, temp, 3);;
		snprintf(temp, 32,  "%0.2lf",machine_total_mbs);
		strncpy(disk_info->total_diskspace, temp, 31);
		snprintf(temp, 32, "%0.2lf",machine_free_mbs);
		strncpy(disk_info->free_diskspace, temp, 31);
		disk_info->is_available = MA_TRUE;
		MA_LOG(g_system_property_logger, MA_LOG_SEV_TRACE, "Total Number of Disk Drives : %s, Total Space: %s and Free Space is: %s", disk_info->total_filesystems,
					disk_info->total_diskspace, disk_info->free_diskspace) ;
		return MA_OK;
}

void os_bit_mode_get(char *os_bit_mode)
{
	char *mode = MA_NA;
#if defined(AIX)
	int kernelMode = sysconf(_SC_AIX_KERNEL_BITMODE);
	if (kernelMode == 64)
			mode = "1";
	else if(kernelMode == 32)
			mode = "0";

#elif defined(HPUX)
	int kernelMode = sysconf(_SC_KERNEL_BITS);
	if (kernelMode == 64)
			mode = "1";
	else if(kernelMode == 32)
			mode = "0";

#elif defined (SOLARIS)
	char buf[1024]  = {0}; /*as it will be sparcv9 or sparcv8 or sparc or amd64 on amd platforms*/
	if(-1 != sysinfo(SI_ISALIST , buf , sizeof(buf) - 1) )
	{
		if( ( 0 == strncmp(buf , "sparcv9" , 7) ) ||
	 	    ( 0 == strncmp(buf , "amd64" , 5) ) )
		{
			mode = "1" ;
		}
		else
			mode = "0" ;
	}

#elif defined(LINUX)
        struct utsname utsn;
        if(uname(&utsn) != -1) {
                if(strstr(utsn.machine, "_64"))
                        mode = "1";
                else if(strstr(utsn.machine, "86"))
                        mode = "0";
        }
	if(!strcmp(mode, MA_NA)) {
        	if(1 == sysconf(_SC_V6_LP64_OFF64))
                	mode = "1" ;
        	else if(1 == sysconf(_SC_V6_ILP32_OFF32))
                	mode = "0";
        	else
                	MA_LOG(g_system_property_logger, MA_LOG_SEV_TRACE, "could not get the bit mode");
	}
#elif defined(MACX)
	FILE* fp=NULL;
	char arch[5]={0};
	struct utsname utsn;
	if (uname(&utsn) != -1)
	{
		if(strstr(utsn.machine, "_64"))
			mode = "1" ;
		else if(strstr(utsn.machine, "86"))
			mode = "0" ;
	}
	else
	{
		fp = popen("sysctl hw | grep 64bit | awk '{ print $2 }'","r");
		if(fp)
		{
			fread(arch,1,sizeof(arch)-sizeof(char),fp);
			pclose(fp);
			if(atoi(arch))
				mode = "1" ;
			else
				mode = "0" ;
		}
		else
		{
			mode = MA_NA ;
		}
	}
#endif
	strncpy(os_bit_mode, mode, 4);
	return;
}


void get_service_pack(char *os_service_pack)
{
#if defined(AIX)
	/*GET TL and SP information --
	oslevel output is used to determie the TL and Service pack.
	bash-3.2# oslevel -s | cut -f2- -d "-"
	06-00-0000
	First field is TL Level
	Second and third field decides the Service Pack
	if both are zero -> No Service Pack
	else If second field iz 00 and third field is not zero ->	It is Service Pack1
	else ->	Second field is service pack*/

	FILE* fp=NULL;
	char tl_level[20]={0};
	char *oslevel;
	char service_pack_buff[256]={0};
	char *tlLevel;
	char *service_pack1;
	char *service_pack2;
	ma_bool_t isSp = MA_TRUE;
	char *servicepack="0";
	int i_tl_level=0,i_servicepack1=0,i_servicepack2=0;

	fp = popen("oslevel -s | cut -f2- -d-","r");
	if(fp)
	{
		fread(tl_level,1,20,fp);
		pclose(fp);
	}

	oslevel = tl_level;
	if(strlen(oslevel) >= 10)
	{
		/* Split and extract all the three field (00-00-0000)*/
		tlLevel=strndup(oslevel,2);
		service_pack1=strndup(oslevel[2],2);
		service_pack2=strndup(oslevel[6],4);

		/*convert all three fields to int*/

		i_tl_level=atoi(tlLevel);
		i_servicepack1=atoi(service_pack1);
		i_servicepack2=atoi(service_pack2);

		/*trim the prefixed zero in tl level if any and then assign to os_service_pack*/
		if(i_tl_level < 10)
		{
			tlLevel++;
		}
		snprintf(service_pack_buff, 63, "TL:%s", tlLevel);

		/*calculate the service pack*/
		if(i_servicepack1 == 0 && i_servicepack2 == 0)
			isSp=MA_FALSE;
		else if(i_servicepack1 == 0 && i_servicepack2 != 0)
			servicepack="1";
		else if (i_servicepack1 != 0)
		{
			if(i_servicepack1 < 10)
				servicepack=(service_pack1)++;
			else
				servicepack=service_pack1;
		}
		else
			isSp=MA_FALSE;

		/*If a valid service pack then assign it*/
		if(isSp)
		{
			snprintf(service_pack_buff, 63, "%s Service Pack %s", service_pack_buff, servicepack);
		}

	}
	else
	{
		strcpy(service_pack_buff,MA_NA);
	}
	strncpy(os_service_pack, service_pack_buff, 15);
	return;
#elif defined(HPUX)
	char service_pack_buff[256]={0};
	/*swlist takes long time (10+ mins) & didnt get other better way, so commenting this part & setting service pack property as N/A */
	/*
	FILE* fp=NULL;
	fp = popen("swlist -l bundle | grep Quality","r");
	if(fp)
	{
		fread(service_pack_buff,1,63,fp);
		pclose(fp);
	}
	else*/
		strcpy(service_pack_buff, MA_NA);
	strncpy(os_service_pack, service_pack_buff, 15);
	return;

#elif defined(SOLARIS)
	strncpy(os_service_pack, MA_NA, 15);
	return;
#endif
}


ma_error_t system_property_os_get (ma_os_system_property_t *os_info){
char *new_line_ptr = NULL;
memset(os_info, 0, sizeof(ma_os_system_property_t));
strncpy(os_info->smbios_uuid, MA_NA, 4);
#if defined(AIX)
	char version_buff[64] = {0};
	strncpy(os_info->platformid, MA_PLATFORM_AIX, 31);
	strncpy(os_info->type, MA_OS_AIX, 63);
	strncpy(os_info->oemid, MA_OS_AIX_OEMID, 63);
	#warning : did not find the way to identify Machine is Server/Workstation. Sending N/A instead of uncertain value
	/*os_info.m_osPlatform =  strdup(MA_WORKSTATION);*/
	strncpy(os_info->platform, MA_NA, 31);
	struct utsname utsn;
	if (uname(&utsn) != -1)
	{
		snprintf(version_buff, 63, "%s.%s", utsn.version, utsn.release);
		strncpy(os_info->version, version_buff, 31);
		strncpy(os_info->buildnumber, utsn.release, 63);
	}
	else
	{
		strncpy(os_info->version, MA_NA, 31);
		strncpy(os_info->buildnumber, MA_NA, 63);
	}

	get_service_pack(&(os_info->servicepack));

#elif defined(HPUX)
	char version_buff[64] = {0};
	strncpy(os_info->platformid, MA_PLATFORM_HPUX, 31);
	strncpy(os_info->oemid, MA_OS_HPUX_OEMID, 63) ;
	strncpy(os_info->type, MA_OS_HPUX, 63) ;
    
    #warning : Please find the way of finding the Machine is Server/Workstation
	strncpy(os_info->platform, MA_SERVER, 31);
	struct utsname utsn;
	if (uname(&utsn) != -1)
	{
		//snprintf(version_buff, 63, "%s.%s", utsn.version, utsn.release);
		strncpy(os_info->version, utsn.release, 31);        
		strncpy(os_info->buildnumber, utsn.release, 63);
	}
	else
	{
		strncpy(os_info->version, MA_NA, 31);
		strncpy(os_info->buildnumber, MA_NA, 63);
	}
	get_service_pack(&(os_info->servicepack));

#elif defined(SOLARIS)
	struct utsname utsn;
	char platform_buff[64] = {0};
	FILE* fp = NULL;
	strncpy(os_info->platformid, MA_PLATFORM_SOLARIS, 31);
	strncpy(os_info->type, MA_OS_SOLARIS, 63);
	strncpy(os_info->oemid, MA_OS_SOLARIS_OEMID, 63);
	fp = popen("/sbin/uname -i 2> /dev/null","r");
	if(fp)
	{
		fread(platform_buff,1,64,fp);
		pclose(fp);
	}
	else
		strncpy(platform_buff, MA_NA, 3);

	strncpy(os_info->platform, platform_buff, 31);
	if (uname(&utsn) != -1)
	{
		strncpy(os_info->version, utsn.release, 31);
		strncpy(os_info->buildnumber, utsn.version, 63);
	}
	else
	{
		strncpy(os_info->version, MA_NA, 31);
		strncpy(os_info->buildnumber, MA_NA, 63);
	}

	get_service_pack(&(os_info->servicepack));

#elif defined(LINUX)
	char *server_strings[]= {"Server" , "Enterprise" , "AS" , "ES" , "WS"};
	ma_bool_t is_redhat = MA_FALSE;
	ma_bool_t is_suse = MA_FALSE;
	ma_bool_t is_ubuntu = MA_FALSE;
	ma_bool_t is_debian = MA_FALSE;
	ma_platform_t sys_platform = MA_PLAT_WORKSTATION;
	ma_bool_t found = MA_FALSE;
	FILE* fp = NULL;
	struct utsname utsn;
	char *distro_file;
	char *str = MA_NA;
	char *line = NULL;
	size_t len, i;
	char *position = MA_NA;
	int index = 0;
	char version_buff[1024] = {0};

	strncpy(os_info->platformid, MA_PLATFORM_LINUX, 31);
	strncpy(os_info->type, MA_OS_LINUX, 63);
	if ( access(MA_REDHAT_RELEASE_FILE, F_OK) != -1 ) {
		is_redhat=MA_TRUE;
		distro_file = MA_REDHAT_RELEASE_FILE;
	}
	else if ( access(MA_SUSE_RELEASE_FILE,F_OK) != -1 ) {
	    is_suse=MA_TRUE;
	    distro_file = MA_SUSE_RELEASE_FILE;
	}
	else if ( access(MA_UBUNTU_RELEASE_FILE,F_OK) != -1 ){
		is_ubuntu = MA_TRUE;
		distro_file = MA_UBUNTU_RELEASE_FILE;
	}
	else {
		is_debian = MA_TRUE;
		distro_file = MA_DEBIAN_RELEASE_FILE;
	}

	fp = fopen(distro_file, "r");
	if (fp) {
		if(-1 != getline(&line, &len, fp)) {
			if(is_ubuntu) {
				if ( strstr(line, "DISTRIB_ID") ||  strstr(line, "DISTRIB_RELEASE") || strstr(line, "DISTRIB_CODENAME") ) {
					position = strrchr(line, '=');
					if ( position && (position++) )	{
						str = position;
					}
				}
				strncpy(os_info->oemid, str, 63);
			}
			else if(is_debian){
				if ( strstr(line, "PRETTY_NAME") ||  strstr(line, "NAME") || strstr(line, "ID") ) {
					position = strrchr(line, '=');
					if ( position && (position++) )	{
						str = position;
					}
				}
				strncpy(os_info->oemid, str, 63);
			}
			else {
				strncpy(os_info->oemid, line, 63);
			}
			/* search for platform type (i.e. server/workstation) */
			/* SuSe contains "Enterprise" string for workstation, so it need to handle separately (like 4.8) */
			if(is_suse){
				/* serch only "Server" string only */
				if (strstr(line, server_strings[0]) ) {
					sys_platform = MA_PLAT_SERVER;
				}
			}	
			else if(is_redhat){
				if(strstr(line, "Server")){
					sys_platform = MA_PLAT_SERVER;
				}
				else if(strstr(line, "Client")){
					sys_platform = MA_PLAT_CLIENT;
				}
				else if(strstr(line, "Workstation")){
					sys_platform = MA_PLAT_WORKSTATION;
				}
			}
			else{
				for(i =0; i < 5; i++) {
					if (strstr(line, server_strings[i]) ) {
						sys_platform = MA_PLAT_SERVER;
						break;
					}
				}
			}
			free(line);
			line = NULL;
		}
		else
			strncpy(os_info->oemid, MA_NA, 63);

		while((-1 != getline(&line, &len, fp)) && (!found)) {
			if(is_ubuntu) {
				if ( strstr(line, "DISTRIB_RELEASE") )	{
					position = strrchr(line, '=');
					if ( position && (position++) )	{
						str = position;
						found = MA_TRUE;
					}
				}
			}
			else{
				if ( strstr(line, "VERSION") )	{
					position = strrchr(line, '=');
					if ( position && (position++) )	{
						str = position;
						found = MA_TRUE;
					}
				}
			}
			strncpy(os_info->version, str, 31);
			free(line);
			line = NULL;
		}

		/*There is no standard format of release file*/
		/*To solve #963645, we are adding this hack*/
		if(!found){
			fseek(fp, 0, SEEK_SET);
			if(-1 != getline(&line, &len, fp)){
				char *ver_str = NULL;
				char ver[10] = {0};
				if ( (ver_str = strstr(line, "release")) ||  (ver_str = strstr(line, "Release"))){
					int i = 0;
					ver_str += strlen("release");
					while(++ver_str && ver_str[0] == ' '); /*pass spaces */
					while(ver_str && (i < 9)){
						if( (ver_str[0] >= '0') && (ver_str[0] <= '9') ){
							ver[i++] = ver_str[0];
							found = MA_TRUE;
							++ver_str;
						}
						else{
							break;
						}
					}
				}
				if(found)
					strncpy(os_info->version, ver, 31);
				free(line);
				line = NULL;
			}
		}

		if(!found)
			strncpy(os_info->version, MA_NA, 31);

		found = MA_FALSE;
		fseek(fp, 0, SEEK_SET);
		while((-1 != getline(&line, &len, fp)) && (!found)) {
			if ( strstr(line, "PATCHLEVEL") )	{
				position = strrchr(line, '=');
				if ( position && (position++) )	{
					str = position;
					found = MA_TRUE;
				}
			}
			strncpy(os_info->servicepack, str, 15);
			free(line);
			line = NULL;
		}

		/*RHEL*/
		if(!found){
			fseek(fp, 0, SEEK_SET);
			if(-1 != getline(&line, &len, fp)){
				char *spack_str = NULL;
				char pack[10] = {0};
				if(spack_str = strstr(line, os_info->version)){
					int i = 0;
					spack_str += strlen(os_info->version);
					if(spack_str && spack_str[0] == '.'){
						while(++spack_str && (spack_str[0] != ' ') && (i < 9)){
							if( (spack_str[0] >= '0') && (spack_str[0] <= '9') ){
								pack[i++] = spack_str[0];
								found = MA_TRUE;
							}
							else{
								break;
							}
						}
					}
				}
				if(found)
					strncpy(os_info->servicepack, pack, 15);
				free(line);
				line = NULL;
			}
		}

		if(!found)
			strncpy(os_info->servicepack, MA_NA, 15);

		fclose(fp);
	}

	switch(sys_platform){
		case MA_PLAT_SERVER:
			strncpy(os_info->platform, MA_SERVER, 31) ;
			break;		
		case MA_PLAT_CLIENT:
			strncpy(os_info->platform, MA_CLIENT, 31) ;
			break;
		default:
			strncpy(os_info->platform, MA_WORKSTATION, 31) ;
	}

	if (uname(&utsn) != -1) {
		strncpy(os_info->buildnumber, utsn.version, 63);
	}
	else {
		strncpy(os_info->buildnumber, MA_NA, 63);
	}

#elif defined(MACX)
	struct stat sb;
	static const char sysver[] =  MA_MAC_SYS_VERSION_PLIST;
	unsigned long len = 0;
	unsigned char* data ;
	char value[256]  = {0};
	char* vers[] = {NULL, NULL, NULL};
	char* last = NULL;
	char* it = NULL;
	int ii = 0;
	char version_buff[16]= {0};
	FILE *fptr = NULL;
	CFStringRef strvalue = NULL;
	CFStringRef error_string;
	CFDataRef cfdata;
	CFStringRef keystr;
	CFDictionaryRef cf_dict;

	strncpy(os_info->platformid, MA_PLATFORM_MAC, 31);
	strncpy(os_info->type, MA_OS_MAC, 63);
	strncpy(os_info->oemid, MA_OS_MAC_OEMID, 63);
	fptr = fopen(MA_MAC_SYS_VERSION_PLIST, "r");
	if (stat(sysver, &sb) == 0)
	{
		len = sb.st_size;
	}
	if (!len || !fptr)
	{
		strncpy(os_info->buildnumber, MA_NA, 4);
		strncpy(os_info->platform, MA_NA, 4);
		strncpy(os_info->servicepack, MA_NA, 4);
		strncpy(os_info->version, MA_NA, 4) ;
		MA_LOG(g_system_property_logger, MA_LOG_SEV_TRACE, "stat failed OR fopen faild, setting to NA");
		return MA_OK;
	}

	data = (unsigned char*)calloc(len, 1);

	fread(data, 1, len, fptr);
	fclose(fptr);
	if (!data[0])
	{
		free(data);
		strncpy(os_info->buildnumber, MA_NA, 4);
		strncpy(os_info->platform, MA_NA, 4);
		strncpy(os_info->servicepack, MA_NA, 4);
		strncpy(os_info->version, MA_NA, 4) ;
		MA_LOG(g_system_property_logger, MA_LOG_SEV_TRACE, "fread faild,  setting to NA");
		return MA_OK;
	}

	cfdata = CFDataCreate (NULL, data, len);
	free(data);
	if (cfdata == NULL)
	{
		strncpy(os_info->buildnumber, MA_NA, 4);
		strncpy(os_info->platform, MA_NA, 4);
		strncpy(os_info->servicepack, MA_NA, 4);
		strncpy(os_info->version, MA_NA, 4) ;
		MA_LOG(g_system_property_logger, MA_LOG_SEV_TRACE, "CFDataCreate faild, setting to NA");
		return MA_ERROR_APIFAILED;
	}

	keystr = CFStringCreateWithCString( NULL, MA_MAC_PRODUCT_VERSION, kCFStringEncodingUTF8 );
	cf_dict = (CFDictionaryRef)CFPropertyListCreateFromXMLData (NULL,
																		 cfdata,
																		 kCFPropertyListImmutable,
																		 &error_string);
	CFRelease(cfdata);
	cfdata= NULL;
	CFDictionaryGetValueIfPresent (cf_dict, keystr,(const void**)&strvalue);
	CFRelease(keystr);
	keystr = NULL;
	CFStringGetCString(strvalue, value, 256, kCFStringEncodingUTF8);

	for (it = strtok_r(value, ".", &last); it; it = strtok_r(NULL, ".", &last))
	{
		vers[ii++] = it;
	}

	if (vers[2])
	{
		snprintf(version_buff, 16, "%s.%s.%s", vers[0], vers[1], vers[2]);
		strncpy(os_info->version, version_buff, 31);
		strncpy(os_info->servicepack, vers[2], 15);
	}
	else
	{
		snprintf(version_buff, 16, "%s.%s", vers[0], vers[1]);
		strncpy(os_info->version, version_buff, 31);
		strncpy(os_info->servicepack, MA_NA, 4);
	}

	keystr = CFStringCreateWithCString( NULL, MA_MAC_BUILD_VERSION, kCFStringEncodingUTF8 );
	CFDictionaryGetValueIfPresent (cf_dict, keystr, (const void**)&strvalue);
	CFStringGetCString(strvalue, value, 256, kCFStringEncodingUTF8);
	CFRelease(keystr);            // Piyush - added this to fix mem leak
	if (cf_dict != NULL)  // Piyush  16 Aug 2004 - added this block to fix mem leak in MAC agent
	{
		CFRelease(cf_dict);
		cf_dict = NULL;
	}	
	strncpy(os_info->buildnumber, value, 63);
	{
		bool bServer = false;		
		if (atoi(vers[0]) >= 10 )
		{
			FILE *fp = NULL;
			char name[30] ={0};
			if (atoi(vers[1]) < 8 )
				fp = popen("sw_vers -productName","r");
			else
				fp = popen("serverinfo --productname","r");

			if (fp)
				fread(name,1,30,fp);
			pclose(fp);

			if( NULL == strcasestr(name ,"server"))
				bServer = false;
			else
				bServer =true;		
		}
		//bool bServer = (access(MA_MAC_SERVER_VERSION_PLIST,F_OK) != -1);
		strncpy(os_info->platform, (bServer?MA_SERVER:MA_WORKSTATION), 31);
	}
#else
		MA_LOG(g_system_property_logger, MA_LOG_SEV_ERROR, "Not supported platform");
		strncpy(os_info->buildnumber, MA_NA, 4);
		strncpy(os_info->platform, MA_NA, 4);
		strncpy(os_info->servicepack, MA_NA, 4);
		strncpy(os_info->version, MA_NA, 4) ;
		strncpy(os_info->platformid,MA_NA, 4) ;
		strncpy(os_info->type, MA_NA, 4) ;
		strncpy(os_info->oemid, MA_NA, 4) ;
		strncpy(os_info->is64bit, MA_NA, 4) ;
		return MA_OK;

#endif
	os_bit_mode_get(os_info->is64bit);
	/*Removing new line character */
	if(new_line_ptr = strchr(os_info->platformid, '\n'))
		*new_line_ptr = '\0';
	if(new_line_ptr = strchr(os_info->type, '\n'))
		*new_line_ptr = '\0';
	if(new_line_ptr = strchr(os_info->platform, '\n'))
		*new_line_ptr = '\0';
	if(new_line_ptr = strchr(os_info->version, '\n'))
		*new_line_ptr = '\0';
	if(new_line_ptr = strchr(os_info->servicepack, '\n'))
		*new_line_ptr = '\0';
	if(new_line_ptr = strchr(os_info->buildnumber, '\n'))
		*new_line_ptr = '\0';
	if(new_line_ptr = strchr(os_info->oemid, '\n'))
		*new_line_ptr = '\0';
	os_info->is_available = MA_TRUE;
	return 	MA_OK;
}

ma_error_t system_property_cluster_get(ma_cluster_system_property_t *cluster_info){
	strcpy(cluster_info->state, "4");
	cluster_info->is_available = MA_TRUE;
	return MA_OK;
}

ma_error_t ma_fqdn_get(char *ip_addr, char fqdn[], size_t length){
	char szName[MA_MAX_PATH_LEN] = {0};
    int rc;
	struct hostent* hptr = NULL;
	/*
	struct addrinfo hints = {0};
    struct addrinfo *res = NULL;
    char addr[MA_MAX_PATH_LEN] = {0};
    hints.ai_family   = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_NUMERICHOST;
	if(!(rc = getaddrinfo(ip_addr, NULL, &hints, &res))){
		//We need FQDN too so  don't give "| NI_NOFQDN" 
		if((!(rc = getnameinfo( res->ai_addr, (socklen_t)res->ai_addrlen, addr, MA_MAX_PATH_LEN, NULL , 0 , NI_NAMEREQD ))) && (strlen (addr)) ){
			strncpy(fqdn, addr, length-1);
            return MA_OK;
		}
		else
			MA_LOG(g_system_property_logger, MA_LOG_SEV_TRACE, "getnameinfo api failed with return code %d ", rc);
	}
	else
		MA_LOG(g_system_property_logger, MA_LOG_SEV_TRACE, "getaddrinfo api failed with return code %d. ", rc);
	*/
	system_property_host_name_get(szName, length);
	#ifdef AIX
	// In AIX gethostbyname will take 30 seconds to process, so avoiding gethostbyname call.
	if(strlen(szName))
	{
		strncpy(fqdn, szName, length-1);
		memset(szName , 0 , MA_MAX_PATH_LEN);
		system_property_domain_name_get(szName, MA_MAX_PATH_LEN);
		if (strlen(szName) && strcmp(szName, MA_NA))
		{
			strcat(fqdn,".");
			strcat(fqdn,szName);
		}	
	}	
    #else
	hptr = gethostbyname(szName);
	if (hptr)
	{
		strncpy(fqdn, hptr->h_name, length-1);
	}
	else
	{
		strncpy(fqdn, szName, length-1);
		memset(szName , 0 , MA_MAX_PATH_LEN);
		system_property_domain_name_get(szName, MA_MAX_PATH_LEN);
		if (strlen(szName) && strcmp(szName, MA_NA))
		{
			strcat(fqdn,".");
			strcat(fqdn,szName);
		}		
	}
	#endif
	return MA_OK;
}

MA_CPP(})
