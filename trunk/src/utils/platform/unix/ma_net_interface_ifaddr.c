#include <ma/internal/utils/network/ma_net_interface.h>
#include "ma_net_interface_system_headers.h"

#ifdef USE_IFADDR

#if defined(__linux__) || defined(LINUX)
ma_error_t get_mac_address(const unsigned int family, struct ifaddrs *if_addr, char mac_address[MA_MAC_ADDRESS_SIZE]) {
	int sock = socket(family, SOCK_DGRAM, 0);
	struct ifreq ifr;
	const char *if_name = if_addr->ifa_name;
	memset(&ifr, 0, sizeof(struct ifreq));
	if( -1 == sock)
		return MA_ERROR_APIFAILED;
	strncpy(ifr.ifr_name, if_name, IFNAMSIZ -1 );
	if( 0 > ioctl(sock, SIOCGIFHWADDR, &ifr)) {
		close(sock);
		return MA_ERROR_APIFAILED;
	}

	memcpy(mac_address, (unsigned char *) &ifr.ifr_hwaddr.sa_data, MA_MAC_ADDRESS_SIZE);
	close(sock);
	return MA_OK;
}
#elif MACX
ma_error_t get_mac_address(const unsigned int family,  struct ifaddrs *if_addr, char *mac_address) {
	struct sockaddr_dl *sdl ;
	struct ifaddrs *addrs, *ifa;
	if( 0 != getifaddrs(&addrs) )
		return MA_ERROR_APIFAILED;
	for(ifa = addrs ; ifa != NULL ; ifa = ifa->ifa_next) {
		//AF_LINK means it's the address of interface  or MAC address
		if ((ifa->ifa_addr->sa_family == AF_LINK)){
			if(strcmp(ifa->ifa_name,if_addr->ifa_name) == 0 ) {
				sdl = (struct sockaddr_dl *)ifa->ifa_addr;
				if(sdl)	{
					memcpy(mac_address, LLADDR(sdl), MA_MAC_ADDRESS_SIZE);
					freeifaddrs(addrs);
					return MA_OK;
				}
			}
		}
	}
	freeifaddrs(addrs);
	return MA_ERROR_APIFAILED;
}
#endif

ma_error_t extract_ip_from_addr(char *str_ip_address, struct sockaddr *addr) {
	char buf[NI_MAXHOST];

	if(str_ip_address == NULL || addr == NULL)
		return MA_ERROR_INVALIDARG;
	memset(buf,0,NI_MAXHOST);

	if( 0 != getnameinfo(addr,sizeof(struct sockaddr_storage),buf,NI_MAXHOST,NULL,0,NI_NUMERICHOST) ) {
		return MA_ERROR_APIFAILED;
	}
	strncpy(str_ip_address, buf ,MA_IPV6_ADDR_LEN);
	return MA_OK;
}

ma_error_t get_net_addr(char *net_addr, char *broad_addr, ma_uint32_t family, struct sockaddr* ipaddr, struct sockaddr *mask_addr) {
	if( !ipaddr  || !mask_addr || !net_addr || !broad_addr )
		return MA_ERROR_INVALIDARG;
	if(family == AF_INET) {
		struct in_addr in_addr;
		in_addr.s_addr = (((struct sockaddr_in*)ipaddr)->sin_addr.s_addr
								&
							((struct sockaddr_in*)mask_addr)->sin_addr.s_addr);
		strncpy(net_addr, inet_ntoa(in_addr) ,MA_IPV6_ADDR_LEN);

		in_addr.s_addr = (((struct sockaddr_in*)ipaddr)->sin_addr.s_addr
								&
							((struct sockaddr_in*)mask_addr)->sin_addr.s_addr | ~((struct sockaddr_in*)mask_addr)->sin_addr.s_addr);

		strncpy(broad_addr, inet_ntoa(in_addr) ,MA_IPV6_ADDR_LEN);
	}
	else{
		strncpy(net_addr, "N/A" ,strlen("N/A")+1);
		strncpy(broad_addr, "N/A" ,strlen("N/A")+1);
		}
	return MA_OK;
}


ma_error_t ma_net_interface_initialize(ma_net_interface_list_t * list, ma_bool_t b_loopback) {
	ma_error_t error = MA_OK;
	ma_uint32_t index = 0;

	struct ifaddrs *address, *ifa;
	char ip_addr[MA_IPV6_ADDR_LEN];
	char mask_addr[MA_IPV6_ADDR_LEN];
	char net_addr[MA_IPV6_ADDR_LEN];
	char broad_addr[MA_IPV6_ADDR_LEN];

	if( 0 != getifaddrs(&address)) {
		return MA_ERROR_APIFAILED;
	}

	for(ifa = address; ifa != NULL ; ifa = ifa->ifa_next) {
		ma_int32_t sock;
		ma_int32_t family;
		ma_net_interface_h inf = NULL;
		ma_net_address_info_h addr = NULL;
		char buf[NI_MAXHOST];
		memset(buf,0,NI_MAXHOST);

		if( (NULL == ifa->ifa_addr)
			|| ((ifa->ifa_flags & IFF_UP) == 0)
			|| ((ifa->ifa_flags & IFF_LOOPBACK ) && !b_loopback )
			|| ((ifa->ifa_addr->sa_family != AF_INET) && (ifa->ifa_addr->sa_family != AF_INET6)))
			continue;

		index = if_nametoindex(ifa->ifa_name);
		family = ifa->ifa_addr->sa_family;
		sock = socket(ifa->ifa_addr->sa_family, SOCK_DGRAM, 0);
		if( -1 == sock )
			continue;

		error = ma_net_interface_create(&inf);
		if(error != MA_OK) {
			(void)ma_address_info_release(addr);
			addr = NULL;
			close(sock);
			(void)ma_net_interface_list_reset(list);
			freeifaddrs(address);
			return error;
		}

		strncpy(inf->interface_name, ifa->ifa_name, IFNAMSIZ);

		if(MA_OK != get_mac_address(family, ifa, inf->mac_address)) {
			(void)ma_net_interface_release(inf);
			(void)ma_address_info_release(addr);
			close(sock);
			addr =NULL;
			continue;
		}

		error = ma_address_info_create(&addr);
		if(error != MA_OK) {
			(void)ma_net_interface_release(inf);
			close(sock);
			(void)ma_net_interface_list_reset(list);
			freeifaddrs(address);
			return error;
		}

		error = extract_ip_from_addr(ip_addr, ifa->ifa_addr);
		if(error != MA_OK ){
			(void)ma_net_interface_release(inf);
			(void)ma_address_info_release(addr);
			addr =NULL;
			close(sock);
			continue;
		}

		error = extract_ip_from_addr(mask_addr,ifa->ifa_netmask);
		if(error != MA_OK){
			(void)ma_net_interface_release(inf);
			(void)ma_address_info_release(addr);
			addr = NULL;
			close(sock);
			continue;
		}

		error = get_net_addr(net_addr, broad_addr, family, ifa->ifa_addr, ifa->ifa_netmask);
		if(error != MA_OK){
			(void)ma_net_interface_release(inf);
			(void)ma_address_info_release(addr);
			addr =NULL;
			close(sock);
			continue;
		}

		if(family == AF_INET) {
			addr->family = MA_IPFAMILY_IPV4;
			addr->ipv6_scope_id = 0;
			strncpy(addr->ip_addr, ip_addr ,MA_IPV6_ADDR_LEN);
			strncpy(addr->subnet_mask, mask_addr ,MA_IPV6_ADDR_LEN);
			strncpy(addr->subnet_addr, net_addr ,MA_IPV6_ADDR_LEN);
			strncpy(addr->broadcast_addr, broad_addr ,MA_IPV6_ADDR_LEN);

		}
		else {
			addr->family = MA_IPFAMILY_IPV6;
			addr->ipv6_scope_id = index;
			strncpy(addr->ip_addr, ip_addr ,MA_IPV6_ADDR_LEN);
			strncpy(addr->subnet_mask, mask_addr ,MA_IPV6_ADDR_LEN);
			strncpy(addr->subnet_addr, net_addr ,MA_IPV6_ADDR_LEN);
			strncpy(addr->broadcast_addr, broad_addr ,MA_IPV6_ADDR_LEN);

		}

		inf->index = index;

		(void)ma_net_interface_set_ipfamily(inf,addr->family);
		(void)ma_net_interface_add_address(inf,&addr);
		addr = NULL;

		(void)ma_net_interface_list_add_interface(list,&inf);
		inf = NULL;

		close(sock);
	}
	freeifaddrs(address);
	return MA_OK;
}

#endif // USE_IFADDR
