#include "ma/internal/utils/platform/ma_sys_apis.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include "../path_private.h"
#include <dirent.h>
#include <sys/stat.h>

struct filesystem_context_s {
	DIR *pdir ;
	struct dirent *pent ;
};

ma_error_t filesystem_context_create(filesystem_context_t **context) {
	if(context){ 
		*context = (filesystem_context_t *) calloc(1,sizeof(filesystem_context_t));
		if(!*context) 
			return MA_ERROR_OUTOFMEMORY;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t filesystem_context_release(filesystem_context_t *context) {
	if(context){ 
		if(context->pdir) {
			closedir(context->pdir);
		}
		free(context);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_filesystem_iterator_get_next(ma_filesystem_iterator_t *iterator, ma_temp_buffer_t *buffer) {
	if(iterator && buffer) {
		filesystem_context_t *context = iterator->context;
		ma_error_t error = MA_ERROR_NO_MORE_ITEMS;
		if(NULL == context) {
			if(MA_OK == filesystem_context_create(&context)) {
				context->pdir = opendir(iterator->path);
				if(!context->pdir) {
					filesystem_context_release(context);
					return error;
				}
			}
			iterator->context = context;
		}
		while(NULL != (context->pent = readdir(context->pdir))){
			ma_temp_buffer_t temp_buffer;
			struct stat st;
			char *p_temp_buffer = NULL;
			int buffer_len = 0;
			ma_temp_buffer_init(&temp_buffer);
			ma_temp_buffer_reserve(&temp_buffer, strlen(iterator->path) + strlen(context->pent->d_name));
			form_path(&temp_buffer, iterator->path, context->pent->d_name,MA_FALSE);
			p_temp_buffer = (char *) ma_temp_buffer_get(&temp_buffer);
			if(	-1 == stat(p_temp_buffer, &st) || 
				(0 == strcmp(context->pent->d_name,".")) || 
				(0 == strcmp(context->pent->d_name,"..")) ||
				((MA_FILESYSTEM_ITERATE_DIRECTORY == iterator->mode) && !S_ISDIR(st.st_mode)) || 
				((MA_FILESYSTEM_ITERATE_FILES == iterator->mode) && !S_ISREG(st.st_mode)))  {
				ma_temp_buffer_uninit(&temp_buffer);
				continue;
			}
			buffer_len = strlen(context->pent->d_name)+1;
			ma_temp_buffer_reserve(buffer, buffer_len);
			strncpy((char*)ma_temp_buffer_get(buffer), context->pent->d_name,buffer_len);
			error = MA_OK;
			ma_temp_buffer_uninit(&temp_buffer);
			break;
		}
		return error;
	}
	return MA_ERROR_INVALIDARG;
}
