/* ex: ts=8 sw=4 sts=4 et
** -*- coding: utf-8 -*-
*/
#include "ma/internal/utils/portability/fs.h"
#include <errno.h>
#include <libgen.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>

int ma_dirname(const char* path_in, char* const buf_out, const size_t size_out)
{
    int rc = 0;
    size_t size_in = strlen(path_in) + 1;
    char* path;

    if (size_out < size_in) {
        return -EOVERFLOW;
    }

    path = strdup(path_in); /* Must dup input: dirname will change it. */
    if (path) {
        strncpy(buf_out, dirname(path), size_out); /* now path != path_in */
        if (size_out > 0) {
            buf_out[size_out - 1] = '\0';
        }
        free(path);
    } else {
        rc = -errno;
    }

    return rc;
}

int ma_mkpath(const char* path_in) {
    int rc;
    char* buf;
    size_t buf_size = strlen(path_in) + 1;
   
    buf = malloc(sizeof(char) * buf_size);
    if (!buf) {
        return -ENOMEM;
    }

    rc = ma_dirname(path_in, buf, buf_size);
    if (0 == rc && strncmp(buf, path_in, buf_size)) {
        rc = ma_mkpath(buf);
        if (0 == rc) {
            rc = mkdir(path_in, 0777);
            if (rc && EEXIST == errno) {
                rc = 0; /* It's okay if path elements already exist */
            }
        }
    }

    free(buf);
    return rc;
}
