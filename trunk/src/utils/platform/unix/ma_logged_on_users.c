#include "ma_system_property_nix.h"

MA_CPP(extern "C" {)
#ifdef MACX

#define MA_STR_TO_LOWER(p) for ( ; *p; ++p) *p = tolower(*p);
#define DSCL_CMD_LEN    256
#define UPN_RECORD      1024
#define RECORD1         "OriginalAuthenticationAuthority"
#define RECORD2         "userPrincipalName"
#define COMMAND_LEN		16384

typedef struct ma_loggedon_users_info_node_s ma_loggedon_users_info_node_t;
struct ma_loggedon_users_info_node_s {
	char			*name;
	MA_SLIST_NODE_DEFINE(ma_loggedon_users_info_node_t);
};

typedef struct ma_loggedon_users_list_s {
	MA_SLIST_DEFINE(user_info, ma_loggedon_users_info_node_t);
}ma_loggedon_users_list_t;

static void ma_str_to_lower(char *str) {
	int i = 0;
	for(i = 0; str[i]; i++){
		str[i] = tolower(str[i]);
	}
}

static void trimmer(char *str) {
	char *end = str + strlen(str) - 1;

	/* Trim leading non-letters */
	while(!isalnum(*str)) 
		strcpy(str, str + 1);

	/* Trim trailing non-letters */
	while(end > str && !isalnum(*end)) end--;
	*end = '\0';
}

ma_bool_t find_domain_name_via_dscl(const char *user_name, const char *recordname, char *domain_name) {
	char dscl_cmd[DSCL_CMD_LEN] = {0};
	char output[UPN_RECORD] = {0};
	FILE* fp = NULL;

	char format[DSCL_CMD_LEN]= "/usr/bin/dscl /Search read /Users/%s  |  /usr/bin/grep %s";
	snprintf(dscl_cmd,DSCL_CMD_LEN,format,user_name,recordname);

	fp = popen(dscl_cmd,"r");
	if(fp) {
		fread(output,1,UPN_RECORD,fp);
		pclose(fp);
	}
	else
		return MA_FALSE;

	{
		char *pch = NULL;

		/* local users */
		if(strlen(output))return MA_FALSE;
		strncpy(domain_name, "", strlen(""));
		ma_str_to_lower(output);
		if((pch = strstr(output, "@"))) {
			if(!strcmp(RECORD1,recordname)) {
				char *pend = NULL;
				size_t itr = 0;

				if((pend = strstr(pch+1, ";"))) {
					pch++;
					while(pch != pend) {
						domain_name[itr++] = *pch;
					}
					domain_name[itr] = '\0';
				}
			} else if(!strcmp(RECORD2,recordname)) {
				strcpy(domain_name, pch+1);
			}
			if(strlen(domain_name)) {
				trimmer(domain_name);               
				return MA_TRUE;
			}

		}

	}
	return MA_FALSE;
}

ma_bool_t find_domain_name(const char *user_name,  char *domain_name) {	
	ma_bool_t bFound=MA_FALSE;
	char buffer[COMMAND_LEN]={0};
	int  bufflen = sizeof(buffer);	
	//get the user details.
	struct passwd* temp_pwd=NULL;
	struct passwd  pwd;	
	struct group *temp_gr=NULL;
	struct group grp;
	char *pch = NULL;
	//passwd = getpwnam(user_name.c_str());

	int s=getpwnam_r(user_name,&pwd,buffer,bufflen,&temp_pwd);

	if( (NULL == temp_pwd && s == 0) || (NULL == temp_pwd && s != 0) )
		return bFound;

	//now get the group.
	s = getgrgid_r(pwd.pw_gid,&grp,buffer,bufflen,&temp_gr);		
	if( (NULL == temp_gr && s == 0) || (NULL == temp_gr && s != 0) )
		return bFound;

	// gr will contain full path like "DOMAIN_NAME\domain users"
	if(grp.gr_name) {
		strcpy(domain_name, grp.gr_name);
		ma_str_to_lower(domain_name);

		// Check if this line is for domain user.
		if((pch = strstr(domain_name, "\\domain users"))) {
			*pch = '\0';
			bFound = MA_TRUE;
		}
	}
	return bFound;
}

static ma_bool_t is_unique_name(ma_loggedon_users_list_t *user_list, char *name) {
	if(user_list && name) {
		ma_loggedon_users_info_node_t *node =NULL;
		MA_SLIST_FOREACH(user_list, user_info, node) {
			if(node && node->name && !(strcmp(node->name, name))) {
				return MA_FALSE;
			}
		}
	}
	return MA_TRUE;
}

static ma_error_t user_info_list_release(ma_loggedon_users_info_node_t *user_info_node) {
	if(user_info_node) {
		if(user_info_node->name)
			free(user_info_node->name);
		free(user_info_node);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

/* As per ePO requirement, user name list should be less that 128 characters, if user list exceeding 124 characters, then add "..." and terminate list string */
ma_error_t copy_list_to_buffer(ma_loggedon_users_list_t *user_list, char user_name_list[], size_t length) {
	if(user_list) {
		char *last_chr = NULL;
		ma_bool_t user_count_exceed_limit = MA_FALSE;
		ma_loggedon_users_info_node_t *node =NULL;
		MA_SLIST_FOREACH(user_list, user_info, node) {
			if(node && node->name) {
				if((strlen(user_name_list) + strlen(node->name)) > (MA_MAX_USER_LIST_LEN - 4)) {
					user_count_exceed_limit = MA_TRUE;
				}
				else {
					strncat(user_name_list, node->name, length-1);
					strncat(user_name_list, ",", length-1);
				}
			}
		}
		if(user_count_exceed_limit){
			/* Add "..." after "," */
			size_t len =  strlen(user_name_list);
			memset( (user_name_list + len), '.', (MA_MAX_USER_LIST_LEN - len - 1));
		}
		else if(last_chr = strrchr(user_name_list, ',')) /*removing last coma*/
			*last_chr = '\0';
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

static ma_error_t user_list_enqueue(ma_loggedon_users_list_t *user_list, ma_loggedon_users_info_node_t *users_info_node) {
	if(user_list && users_info_node && users_info_node->name) {
		if(!is_unique_name(user_list, users_info_node->name)) {
			user_info_list_release(users_info_node);
			return MA_OK;
		}

		/*Add in list */
		MA_SLIST_PUSH_BACK(user_list, user_info, users_info_node);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_collect_logged_on_users(char user_name_list[], size_t length) {
	struct utmpx* utx=NULL;
	ma_loggedon_users_list_t *user_info_list;
	if(user_info_list= (ma_loggedon_users_list_t*)calloc(1, sizeof(ma_loggedon_users_list_t))) {
		MA_SLIST_INIT(user_info_list, user_info);
		while(1) {
			utx=NULL;		
			utx=getutxent();		
			if(NULL == utx) {			
				break;
			} else {
				if(USER_PROCESS == utx->ut_type) {
					const char *puser = utx->ut_user;
					if(puser) {
						char dname[MA_MAX_BUFFER_LEN] = {0};
						if(find_domain_name(puser,dname) || find_domain_name_via_dscl(puser,RECORD2,dname) || find_domain_name_via_dscl(puser,RECORD1,dname) ) ;
						{
							char *name = NULL; 
							if(name = (char*) calloc(strlen(dname) + strlen("\\") + strlen(puser) + 1, sizeof(char))) {
								ma_loggedon_users_info_node_t *node = NULL;
								if(node = (ma_loggedon_users_info_node_t*)calloc(1, sizeof(ma_loggedon_users_info_node_t))) {
									if(strlen(dname)){
										strncpy(name, dname, strlen(dname));
										strncat(name, "\\", strlen("\\"));
									}
									strncat(name, puser, strlen(puser));
									node->name = name;
									user_list_enqueue(user_info_list, node);
								}
								else
									free(name);

							}
						}
					}
				}
			}
		}
		copy_list_to_buffer(user_info_list, user_name_list, length);
		MA_SLIST_CLEAR(user_info_list, user_info, ma_loggedon_users_info_node_t, user_info_list_release);
		free(user_info_list);
		setutxent();
	}
	return MA_OK;
}

#else /*All *nix except mac */

ma_error_t ma_collect_logged_on_users(char user_name_list[], size_t length) {
	struct passwd *pw = getpwuid(getuid());
	strncpy(user_name_list, (pw ? pw->pw_name : "root"), length-1);
	return MA_OK;
}

#endif


MA_CPP(})
