#ifndef MA_SYSTEM_PROPERTY_NIX_H_INCLUDED
#define MA_SYSTEM_PROPERTY_NIX_H_INCLUDED

#include "../ma_system_property_common.h"

#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/utsname.h>
#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <pwd.h>
#include <time.h>
#include <netdb.h>

MA_CPP(extern "C" {)
#if defined(LINUX)
	#include <mntent.h>
	#include <sys/vfs.h>
	#define PATH_MOUNTED _PATH_MOUNTED 
	#define CPU_INFO_FILE "/proc/cpuinfo" 
#endif
#if defined(MACX)
	#include <sys/stat.h>
	#include <sys/mount.h>
	#include <Carbon/Carbon.h>
	#include <sys/sysctl.h>
	#include <utmpx.h>
	#include <grp.h>
#endif
#if defined(HPUX)
	#include <sys/types.h>
	#include <sys/statvfs.h>
	/*#include <net/if.h>*/
	#include <sys/vfs.h>
	#include <sys/pstat.h>
	#include <mntent.h>
	#define PATH_MOUNTED "/etc/mnttab"
#endif
#if defined(SOLARIS)
	#include <sys/systeminfo.h>
	#include <sys/processor.h>
	#include <sys/mnttab.h>
	#include <sys/statvfs.h>
#endif
#if defined(AIX)
	#include <fstab.h>
	#include <sys/statvfs.h>
	#include <sys/ioctl.h>
	#include <netdb.h>
	#include <net/if.h>
	#include <sys/systemcfg.h>
	#include <nlist.h>
#endif
#define MA_REDHAT_RELEASE_FILE "/etc/redhat-release"
#define MA_UBUNTU_RELEASE_FILE "/etc/lsb-release"
#define MA_SUSE_RELEASE_FILE "/etc/SuSE-release"
#define MA_DEBIAN_RELEASE_FILE "/etc/os-release"
#define MA_PLATFORM_LINUX "LINUX"
#define MA_PLATFORM_MAC "MAC"
#define MA_PLATFORM_SOLARIS "SLR"
#define MA_PLATFORM_HPUX "HPUX"
#define MA_PLATFORM_AIX "AIX"
#define MA_OS_LINUX "Linux"
#define MA_OS_MAC "Mac OS X"
#define MA_OS_SOLARIS "SunOS Sparc"
#define MA_OS_HPUX "HP-UX"
#define MA_OS_AIX "AIX"
#define MA_WORKSTATION "Workstation"
#define MA_CLIENT "Client"
#define MA_SERVER "Server"
#define MA_OS_MAC_OEMID "Apple Macintosh" 
#define MA_OS_SOLARIS_OEMID "Solaris Sparc" 
#define MA_OS_AIX_OEMID "AIX PowerPC"
#define MA_NA                "N/A"
#define MA_UNKNOWNDOMAIN	"UnknownDomain"
#define MA_MAC_SYS_VERSION_PLIST	"/System/Library/CoreServices/SystemVersion.plist"
#define MA_MAC_SERVER_VERSION_PLIST	"/System/Library/CoreServices/ServerVersion.plist"
#define MA_MAC_BUILD_VERSION 		"ProductBuildVersion"
#define MA_MAC_PRODUCT_VERSION 		"ProductVersion"
#ifdef MA_PLATFORM_HPIA
	#define MA_OS_HPUX_OEMID "Hpux Itanium" 
#else
	#define MA_OS_HPUX_OEMID "Hpux PA-RISC" 
#endif


typedef struct ma_locales_table_s{
	char* code;
	char* name;
} ma_locales_table_t;

MA_CPP(})
#endif //MA_SYSTEM_PROPERTY_NIX_H_INCLUDED
