#include "ma/internal/utils/platform/ma_dl.h"

#include <dlfcn.h>
#include <errno.h>
#include <string.h>

extern int errno ;

ma_error_t  ma_dl_open(const char *library_name, unsigned long flags ,  ma_lib_t *lib) {
    if(library_name && lib ) {
        lib->handle =  NULL ;        
        lib->handle = dlopen(library_name, flags ? flags : RTLD_LAZY) ;
        return lib->handle ? MA_OK :MA_ERROR_LOAD_LIBRARY_FAILED ;
    }
    return MA_ERROR_INVALIDARG;
}

void ma_dl_close(ma_lib_t  *lib) {
    if(lib && lib->handle) {
        dlclose(lib->handle) , lib->handle = NULL ;
    }
}

void *ma_dl_sym(ma_lib_t* lib , const char* symbol_name) {
    if(lib && symbol_name ) {                           
         return dlsym(lib->handle, symbol_name);
    }
    return NULL ;
}

