#include "ma/internal/utils/platform/ma_system_property.h"
#include "ma/internal/utils/network/ma_net_interface.h"
#include "ma/internal/utils/platform/ma_net_interface_list_internal.h"
#include "ma_system_property_common.h"
#include "ma/internal/utils/datastructures/ma_slist.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <uv.h>

MA_CPP(extern "C" {)

ma_logger_t *g_system_property_logger = NULL;

void system_property_cluster_release(ma_cluster_system_property_t *cluster_info){
	if(cluster_info->host){
		free(cluster_info->host);
		cluster_info->host = NULL;
	}
	if(cluster_info->ipaddr){
		free(cluster_info->ipaddr);
		cluster_info->ipaddr = NULL;
	}
	if(cluster_info->name){
		free(cluster_info->name);
		cluster_info->name=NULL;
	}
	if(cluster_info->nodes){
		free(cluster_info->nodes);
		cluster_info->nodes = NULL;
	}
	if(cluster_info->quorum_resource_path){
		free(cluster_info->quorum_resource_path);
		cluster_info->quorum_resource_path = NULL;
	}
}

void ma_system_property_release(ma_system_property_t *system_property_info){
	if(!system_property_info)
		return;

	if(system_property_info->disk_information.filesysteminfo)
		free(system_property_info->disk_information.filesysteminfo);

	system_property_cluster_release(&system_property_info->cluster_information);

	free(system_property_info);
	g_system_property_logger = NULL;
}

ma_error_t ma_system_property_create(ma_system_property_t **system_property_info, ma_logger_t *logger) {
	ma_system_property_t *self = (ma_system_property_t *)calloc(1, sizeof(ma_system_property_t)) ;
    if(!self) return MA_ERROR_OUTOFMEMORY;
	self->disk_information.filesysteminfo = NULL;
	self->cluster_information.host = NULL;
	self->cluster_information.ipaddr = NULL;
	self->cluster_information.name = NULL;
	self->cluster_information.nodes = NULL;
	self->cluster_information.quorum_resource_path = NULL;
    g_system_property_logger = logger ;
    *system_property_info = self ;
	return MA_OK ;
}

ma_error_t system_property_cpu_get(ma_cpu_system_property_t *cpu_info){
	uv_cpu_info_t *cpu_infos = NULL;
	int count=0;
	uv_err_t uv_err = uv_cpu_info(&cpu_infos, &count);
	memset(cpu_info, 0, sizeof(ma_cpu_system_property_t));
	if((uv_err.code == UV_OK) && (cpu_infos) ){
		MA_MSC_SELECT(_snprintf, snprintf)(cpu_info->numbers, 7, "%d", count);
		MA_MSC_SELECT(_snprintf, snprintf)(cpu_info->speed, 15, "%d", cpu_infos->speed);
		MA_MSC_SELECT(_snprintf, snprintf)(cpu_info->type, MA_MAX_PATH_LEN-1, "%s", cpu_infos->model);
		uv_free_cpu_info(cpu_infos, count);
	}
	else{
		MA_MSC_SELECT(_snprintf, snprintf)(cpu_info->numbers, 7, "%s", MA_NA);
		MA_MSC_SELECT(_snprintf, snprintf)(cpu_info->speed, 15, "%s", MA_NA);
		MA_MSC_SELECT(_snprintf, snprintf)(cpu_info->type, MA_MAX_PATH_LEN-1, "%s", MA_NA);
	}
	MA_MSC_SELECT(_snprintf, snprintf)(cpu_info->serialnumber, 31, "%s", MA_NA);
	cpu_info->is_available = MA_TRUE;

	return MA_OK;
}

ma_error_t system_property_memory_get(ma_memory_system_property_t *mem_info){
	uint64_t total_memory = uv_get_total_memory();
	uint64_t free_memory = uv_get_free_memory();
	memset(mem_info, 0, sizeof(ma_memory_system_property_t));
	/*In failure case: libuv 0.10.19 return -1 , but return type in unsigned*/
	if(total_memory != (uint64_t)(-1))
		MA_MSC_SELECT(_snprintf, snprintf)(mem_info->total_memory, 31, "%llu", total_memory);
	else
		MA_MSC_SELECT(_snprintf, snprintf)(mem_info->total_memory, 31, "%d", 0);

	if(free_memory != (uint64_t)(-1))
		MA_MSC_SELECT(_snprintf, snprintf)(mem_info->free_memory, 31, "%llu", free_memory);
	else
		MA_MSC_SELECT(_snprintf, snprintf)(mem_info->free_memory, 31, "%d", 0);

	mem_info->is_available = MA_TRUE;
	return MA_OK;
}

/*NOTE: In case of any functionality fails, main function will not return immediately (This function will try to collect as possible as information gathering)
 At end error code will be return, lets caller decide what to do (shall send partial info or complete info) */

ma_error_t ma_system_property_scan(ma_system_property_t *self){
	if(self) {
        ma_error_t rc = MA_OK, rc1 = MA_OK ;
        if(MA_OK != (rc = system_property_cpu_get(&self->cpu_information))) {
            MA_LOG(g_system_property_logger, MA_LOG_SEV_ERROR, "System Property CPU info failed") ;
        }
        if(MA_OK != (rc1 = system_property_os_get (&self->os_information))) {
            MA_LOG(g_system_property_logger, MA_LOG_SEV_ERROR, "System Property OS info failed") ;
            rc = rc1 ;
        }
        if(MA_OK != (rc1 = system_property_disk_get(&self->disk_information))) {
            MA_LOG(g_system_property_logger, MA_LOG_SEV_ERROR, "System Property Disk info failed") ;
            rc = rc1 ;
        }
        if(MA_OK != (rc1 = system_property_memory_get(&self->memory_information))) {
            MA_LOG(g_system_property_logger, MA_LOG_SEV_ERROR, "System Property Memory info failed") ;
            rc = rc1 ;
        }
        if(MA_OK != (rc1 = system_property_misc_get(&self->misc_information))) {
            MA_LOG(g_system_property_logger, MA_LOG_SEV_ERROR, "System Property Misc info failed") ;
            rc = rc1 ;
        }
        if(MA_OK != (rc1 = system_property_network_get(&self->network_information))) {
            MA_LOG(g_system_property_logger, MA_LOG_SEV_ERROR, "System Property Network info failed") ;
            rc = rc1 ;
        }
		if(MA_OK != (rc1 = system_property_cluster_get(&self->cluster_information))) {
            MA_LOG(g_system_property_logger, MA_LOG_SEV_ERROR, "System Property Cluster info failed") ;
            rc = rc1 ;
        }

	    return rc;
    }
    return MA_ERROR_INVALIDARG ;
}

const ma_network_system_property_t*	 ma_system_property_network_get(ma_system_property_t *self, ma_bool_t forced_scan){
	if(!self)
		return NULL;
    if( (!self->network_information.is_available) || (forced_scan) )
		system_property_network_get(&self->network_information);

	return &self->network_information;
}

const ma_memory_system_property_t * ma_system_property_memory_get (ma_system_property_t *self, ma_bool_t forced_scan){
	if(!self)
		return NULL;
    if( (!self->memory_information.is_available) || (forced_scan) )
		system_property_memory_get(&self->memory_information);

	return &self->memory_information;
}

const ma_misc_system_property_t * ma_system_property_misc_get( ma_system_property_t *self, ma_bool_t forced_scan){
	if(!self)
		return NULL;
    if( (!self->misc_information.is_available) || (forced_scan) )
		system_property_misc_get(&self->misc_information);

	return &self->misc_information;
}

const ma_disk_system_property_t	* ma_system_property_disk_get(ma_system_property_t *self, ma_bool_t forced_scan){
	if(!self)
		return NULL;
	if( (!self->disk_information.is_available) || (forced_scan) )
		system_property_disk_get(&self->disk_information);

	return &self->disk_information;
}

const ma_cpu_system_property_t * ma_system_property_cpu_get(ma_system_property_t *self, ma_bool_t forced_scan){
	if(!self)
		return NULL;
	if( (!self->cpu_information.is_available) || (forced_scan) )
		system_property_cpu_get(&self->cpu_information);

	return &self->cpu_information;
}

const ma_os_system_property_t * ma_system_property_os_get (ma_system_property_t *self, ma_bool_t forced_scan){
	if(!self)
		return NULL;
	if( (!self->os_information.is_available) || (forced_scan) )
		system_property_os_get (&self->os_information);
	return &self->os_information;
}

const ma_cluster_system_property_t * ma_system_property_cluster_get (ma_system_property_t *self, ma_bool_t forced_scan){
	if(!self)
		return NULL;
	if( (!self->cluster_information.is_available) || (forced_scan) )
		system_property_cluster_get (&self->cluster_information);
	return &self->cluster_information;
}

ma_error_t system_property_misc_get(ma_misc_system_property_t *misc_info) {
	memset(misc_info, 0, sizeof(ma_misc_system_property_t));
    system_property_is_portable_get(misc_info->isportable, 4);
    system_property_domain_name_get(misc_info->domainname, MA_MAX_PATH_LEN);
    system_property_computer_name_get(misc_info->computername, MA_MAX_PATH_LEN);  
    system_property_computer_description_get(misc_info->computerdesc, MA_MAX_PATH_LEN);
    system_property_time_zone_info_get(misc_info->timezone, 128);
    system_property_lang_id_get(misc_info->language, 32);
    system_property_lang_code_get(misc_info->language_code, 32);
    system_property_system_time_get(misc_info->systemtime, 64);
    system_property_user_name_get(misc_info->username, MA_MAX_USER_LIST_LEN);
    system_property_host_name_get(misc_info->hostname, MA_MAX_PATH_LEN);
    system_property_ipx_addr_get(misc_info->ipx_addr, 4);
    system_property_time_zone_bias_get(misc_info->timezonebios, 128);
    misc_info->is_available = MA_TRUE;
	return MA_OK;
}

static ma_error_t map_host_spipeip_to_net_interface(ma_net_interface_list_t	*list, char *host_spipe_ip, ma_net_interface_t **interf, ma_net_address_info_t **addr) {
	if(list && interf && addr) {
		ma_net_interface_t *temp_interf = NULL;
		if(host_spipe_ip && strlen(host_spipe_ip)){
			MA_SLIST_FOREACH(list, interfaces, temp_interf){
				ma_net_address_info_t *address = NULL;
				MA_SLIST_FOREACH(temp_interf, addresses, address){
					if(address && strlen(address->ip_addr)){
						char ip_address_format[MA_IPV6_ADDR_LEN] = {0};
						expand_ipv6_address(ip_address_format, address->ip_addr);
						if(!strcmp(ip_address_format, host_spipe_ip)){
							*addr = address;
							*interf = temp_interf;
							return MA_OK;
						}
					}
				}
			}
		}
		/* Either calculation started before spipe or  host spipe ip mapping to interface list failed. Give any first ip details. */
		{
			MA_SLIST_FOREACH(list, interfaces, temp_interf){
				ma_net_address_info_t *address = NULL;
				/* will give priority to ipv4 */
				MA_SLIST_FOREACH(temp_interf, addresses, address){
					if(MA_IPFAMILY_IPV4 == address->family){
						*addr = address;
						*interf = temp_interf;
						return MA_OK;
					}
				}

				MA_SLIST_FOREACH(temp_interf, addresses, address){
					*addr = address;
					*interf = temp_interf;
					return MA_OK;
				}
			}
		}		
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t system_property_network_get(ma_network_system_property_t *network_info) {
	ma_net_interface_list_t	*list = NULL;
	ma_error_t rc;
	if(MA_OK != (rc = ma_net_interface_list_create(&list)))
		return rc;

	if(MA_OK == (rc = ma_net_interface_list_scan(list, MA_FALSE)))	{
		ma_net_address_info_t *address = NULL;
		ma_net_interface_t *interf = NULL;
		if( (MA_OK == (rc = map_host_spipeip_to_net_interface(list, network_info->spipe_src_ip_address_formatted, &interf, &address))) && interf && address ){
			memset((void*)network_info->mac_address, 0, MA_MAC_ADDRESS_SIZE);
			memset((void*)network_info->ip_address, 0, MA_IPV6_ADDR_LEN);
			memset((void*)network_info->ip_address_formated, 0, MA_IPV6_ADDR_LEN);
			memset((void*)network_info->subnet_address, 0, MA_IPV6_ADDR_LEN);
			memset((void*)network_info->subnet_mask, 0, MA_IPV6_ADDR_LEN);
			memset((void*)network_info->fqdn, 0, MA_MAX_PATH_LEN);
			memcpy(network_info->mac_address, (const char *)interf->mac_address, MA_MAC_ADDRESS_SIZE);
			ma_format_mac_address((unsigned char*)interf->mac_address, (unsigned char*)network_info->mac_address_formated);
			strncpy(network_info->ip_address, address->ip_addr, MA_IPV6_ADDR_LEN);
			expand_ipv6_address(network_info->ip_address_formated, address->ip_addr);
			strncpy(network_info->subnet_address, address->subnet_addr, MA_IPV6_ADDR_LEN);
			strncpy(network_info->subnet_mask, address->subnet_mask, MA_IPV6_ADDR_LEN);
			ma_fqdn_get(network_info->ip_address, network_info->fqdn, MA_MAX_PATH_LEN);
			/*ePO communicating local ip get only after first asci (this value is set by ahclient), so first time copy with any existing local ip */
			if( (!strlen(network_info->spipe_src_ip_address_formatted)) || 
					(strncmp(network_info->spipe_src_ip_address_formatted, network_info->ip_address_formated, MA_IPV6_ADDR_LEN)) ){
				strncpy(network_info->spipe_src_ip_address_formatted, network_info->ip_address_formated, MA_IPV6_ADDR_LEN);
			}
			network_info->is_available = MA_TRUE;
			rc = MA_OK;
		}
	}
	else
		MA_LOG(g_system_property_logger, MA_LOG_SEV_ERROR, "ma_net_interface_list_scan failed.") ;

	(void)ma_net_interface_list_release(list);
	return rc;
}

/*
ma_error_t ma_system_property_host_spipe_ip_set(ma_network_system_property_t *network_info, char *host_spipe_ip){
if(network_info && host_spipe_ip){
strncpy((char*)network_info->spipe_src_ip_address_formatted, host_spipe_ip, MA_IPV6_ADDR_LEN);
return MA_OK;
}
return MA_ERROR_INVALIDARG;
}
*/
MA_CPP(})
