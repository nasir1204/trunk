#include "ma/internal/utils/profiler/ma_profile_info.h"
#include "ma/internal/utils/json/ma_cjson.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/defs/ma_profile_defs.h"
#include "ma/ma_log.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

extern ma_logger_t *profiler_logger;

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "profiler"

struct ma_profile_info_s {
    char email[MA_MAX_LEN+1];
    char name[MA_MAX_NAME+1];
    char contact[MA_MAX_LEN+1];
    char from_address[MA_MAX_BUFFER_LEN+1];
    char from_pincode[MA_MAX_LEN+1];
    char from_city[MA_MAX_LEN+1];
    char to_address[MA_MAX_BUFFER_LEN+1];
    char to_pincode[MA_MAX_LEN+1];
    char to_city[MA_MAX_LEN+1];
    ma_profile_info_user_type_t user_type;
    ma_profile_info_privilege_t privi_type;
    ma_uint32_t password_set:1;
    char passwd[MA_MAX_LEN+1];
    ma_profile_info_status_t status;
    ma_uint32_t last_login;
    ma_uint32_t reserved; 
    size_t order_count;
    ma_task_time_t last_seen;
    char subscribe_boards[MA_MAX_LEN+1][MA_MAX_LEN+1];
    size_t no_of_boards;
    ma_json_t *json;
    char profile_url[MA_MAX_LEN+1];
    ma_bool_t subscribed;
    char gender[MA_MAX_LEN+1];
    char dob[MA_MAX_LEN+1];
    char gstin[MA_MAX_LEN+1];
    char ssn[MA_MAX_LEN+1];
    char channel[MA_MAX_LEN+1];
};

const char *gprofile_status_str[] =  {
#define MA_PROFILE_INFO_STATUS_EXPAND(status, index, status_str) status_str,
    MA_PROFILE_INFO_STATUS_EXPANDER
#undef MA_PROFILE_INFO_STATUS_EXPAND
}; 

ma_error_t ma_profile_info_create(ma_profile_info_t **info, ma_profile_info_user_type_t type) {
    if(info) {
         ma_error_t err = MA_ERROR_OUTOFMEMORY;

         if((*info = (ma_profile_info_t*)calloc(1, sizeof(ma_profile_info_t)))) {
             if(MA_OK == (err = ma_json_create(&(*info)->json))) {
                 (*info)->user_type = type;
                 (*info)->status = MA_PROFILE_INFO_STATUS_ACTIVE;
             }
         }

         return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_info_release(ma_profile_info_t *info) {
    if(info) {
        (void)ma_json_release(info->json);
        free(info);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_info_order_increment(ma_profile_info_t *info) {
    if(info) {
        info->order_count++;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_info_order_decrement(ma_profile_info_t *info) {
    if(info) {
        info->order_count--;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_info_add_board(ma_profile_info_t *info, const char *board) {
    if(info && board) {
        size_t i = 0;
        ma_bool_t found = MA_FALSE;

        for(i = 0; i < info->no_of_boards; ++i) {
            if(!strcmp(info->subscribe_boards[i], board)) {
                found = MA_TRUE;
                break;
            }
        }
        if(MA_FALSE == found) {
            strncpy(info->subscribe_boards[info->no_of_boards++], board, MA_MAX_LEN);
            info->subscribed = MA_TRUE;
         }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

/* setter */

ma_error_t ma_profile_info_set_status_from_string(ma_profile_info_t *info, const char *str) {
    if(info && str) {
        ma_error_t err = MA_OK;
        if(!strcmp(str, MA_PROFILE_INFO_STATUS_NOT_ACTIVE_STR))
            info->status = MA_PROFILE_INFO_STATUS_NOT_ACTIVE;
        else if(!strcmp(str, MA_PROFILE_INFO_STATUS_ACTIVE_STR))
            info->status = MA_PROFILE_INFO_STATUS_ACTIVE;
        else if(!strcmp(str, MA_PROFILE_INFO_STATUS_DELETED_STR))
            info->status = MA_PROFILE_INFO_STATUS_DELETED;
        else;

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_info_set_channel(ma_profile_info_t *info, const char *channel) {
    if(!info || !channel)return MA_ERROR_INVALIDARG;
    strncpy(info->channel, channel, MA_MAX_LEN);
    return MA_OK;
}

ma_error_t ma_profile_info_set_ssn(ma_profile_info_t *info, const char *ssn) {
    if(!info || !ssn)return MA_ERROR_INVALIDARG;
    strncpy(info->ssn, ssn, MA_MAX_LEN);
    return MA_OK;
}

ma_error_t ma_profile_info_set_gstin(ma_profile_info_t *info, const char *gst) {
    if(!info || !gst)return MA_ERROR_INVALIDARG;
    strncpy(info->gstin, gst, MA_MAX_LEN);
    return MA_OK;
}

ma_error_t ma_profile_info_set_gender(ma_profile_info_t *info, const char *gender) {
    if(info && gender) {
        if(!strncmp(gender, "male", MA_MAX_LEN) ||
           !strncmp(gender, "MALE", MA_MAX_LEN) ||
           !strncmp(gender, "M", MA_MAX_LEN) ||
           !strncmp(gender, "m", MA_MAX_LEN)) {
            strncpy(info->gender, "male", MA_MAX_LEN);
        } else if(!strncmp(gender, "female", MA_MAX_LEN) ||
           !strncmp(gender, "FEMALE", MA_MAX_LEN) ||
           !strncmp(gender, "F", MA_MAX_LEN) ||
           !strncmp(gender, "f", MA_MAX_LEN)) {
            strncpy(info->gender, "female", MA_MAX_LEN);
        } else {
            strncpy(info->gender, "none", MA_MAX_LEN);
        }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_info_set_dob(ma_profile_info_t *info, const char *dob) {
    if(info && dob) {
        strncpy(info->dob, dob, MA_MAX_LEN);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_info_set_subscribed(ma_profile_info_t *info, ma_bool_t yes) {
    if(info) {
        info->subscribed = yes;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_info_set_profile_url(ma_profile_info_t *info, const char *url) {
    if(info && url && strlen(url) < MA_MAX_LEN) {
        strncpy(info->profile_url, url, MA_MAX_LEN);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_info_set_boards(ma_profile_info_t *info, const char *boards[], size_t no_of_boards) {
    if(info && boards && no_of_boards && (no_of_boards < MA_MAX_LEN)) {
        size_t i = 0;

        for(i = 0; i < no_of_boards; ++i, ++info->no_of_boards) {
            strncpy(info->subscribe_boards[info->no_of_boards], boards[i], MA_MAX_LEN);
        }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_info_set_last_seen(ma_profile_info_t *info, ma_task_time_t time) {
    if(info) {
        ma_task_time_t dummy = {0};

        if(memcmp(&dummy, &time, sizeof(ma_task_time_t))) {
            memcpy(&info->last_seen, &time, sizeof(ma_task_time_t));
        }

        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_info_set_order_count(ma_profile_info_t *info, size_t count) {
    if(info) {
        info->order_count = count;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_info_set_from_city(ma_profile_info_t *info, const char *city) {
    return info && city && strncpy(info->from_city, city, MA_MAX_LEN) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_info_set_to_city(ma_profile_info_t *info, const char *city) {
    return info && city && strncpy(info->to_city, city, MA_MAX_LEN) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_info_set_email_id(ma_profile_info_t *info, const char *id) {
    return info && id && memset(info->email, 0, MA_MAX_LEN) ? strncpy(info->email, id, MA_MAX_LEN) ? MA_OK : MA_ERROR_INVALIDARG : MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_info_set_user(ma_profile_info_t *info, const char *user) {
    return info && user && memset(info->name, 0, MA_MAX_NAME) ? strncpy(info->name, user,  MA_MAX_NAME) ? MA_OK : MA_ERROR_INVALIDARG : MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_info_set_contact(ma_profile_info_t *info, const char *contact) {
    if(info && contact) {
        ma_error_t err = MA_OK;

        /* TODO: contact number validation check */
        memset(info->contact, 0, MA_MAX_NAME);
        err = strncpy(info->contact, contact, MA_MAX_NAME) ? MA_OK : MA_ERROR_INVALIDARG;
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_info_set_from_address(ma_profile_info_t *info, const char *addr) {
    return info && addr && memset(info->from_address, 0, MA_MAX_BUFFER_LEN) ? strncpy(info->from_address, addr,  MA_MAX_BUFFER_LEN) ? MA_OK : MA_ERROR_INVALIDARG : MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_info_set_to_address(ma_profile_info_t *info, const char *addr) {
    return info && addr && memset(info->to_address, 0, MA_MAX_BUFFER_LEN) ? strncpy(info->to_address, addr,  MA_MAX_BUFFER_LEN) ? MA_OK : MA_ERROR_INVALIDARG : MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_info_set_password(ma_profile_info_t *info, const char *passwd) {
    return info && passwd && memset(info->passwd, 0, MA_MAX_NAME) ? strncpy(info->passwd, passwd,  MA_MAX_NAME) ? MA_OK : MA_ERROR_INVALIDARG : MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_info_set_last_login(ma_profile_info_t *info, ma_uint32_t time) {
    if(info) {
        info->last_login = time;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_info_set_status(ma_profile_info_t *info, ma_profile_info_status_t status) {
    if(info) {
        info->status = status;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_info_set_source_pincode(ma_profile_info_t *info, const char *pincode) {
    //TODO validate pin code 
    return info && pincode && memset(info->from_pincode, 0, MA_MAX_NAME) ? strncpy(info->from_pincode, pincode,  MA_MAX_NAME) ? MA_OK : MA_ERROR_INVALIDARG : MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_info_set_dest_pincode(ma_profile_info_t *info, const char *pincode) {
    //TODO validate pin code 
    return info && pincode && memset(info->to_pincode, 0, MA_MAX_NAME) ? strncpy(info->to_pincode, pincode,  MA_MAX_NAME) ? MA_OK : MA_ERROR_INVALIDARG : MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_info_set_user_type(ma_profile_info_t *info, ma_profile_info_user_type_t type) {
    if(info) {
        info->user_type = type;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_info_set_privilege(ma_profile_info_t *info, ma_profile_info_privilege_t priv) {
    if(info) {
        info->privi_type = priv;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}



/* getter */
ma_error_t ma_profile_info_get_status_str(ma_profile_info_t *info, char *str) {
    return info && str && strncpy(str, gprofile_status_str[info->status], MA_MAX_LEN) ? MA_OK : MA_ERROR_INVALIDARG;
}
ma_error_t ma_profile_info_get_channel(ma_profile_info_t *info, const char **channel) {
    if(info && channel) {
        *channel = info->channel;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_info_get_ssn(ma_profile_info_t *info, const char **ssn) {
    if(info && ssn) {
        *ssn = info->ssn;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_info_get_gstin(ma_profile_info_t *info, const char **gstin) {
    if(info && gstin) {
        *gstin = info->gstin;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_info_get_gender(ma_profile_info_t *info, const char **gender) {
   if(info && gender) {
       *gender = info->gender;
       return MA_OK;
   }
   return MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_info_get_dob(ma_profile_info_t *info, const char **dob) {
   if(info && dob) {
       *dob = info->dob;
       return MA_OK;
   }
   return MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_info_get_subscribed(ma_profile_info_t *info, ma_bool_t *yes) {
    if(info && yes) {
        *yes = info->subscribed;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_info_get_profile_url(ma_profile_info_t *info, const char **url) {
    if(info && url) {
        *url = info->profile_url;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_info_get_boards(ma_profile_info_t *info, const char ***boards, size_t *no_of_boards) {
    if(info && boards && no_of_boards) {
        *no_of_boards =  info->no_of_boards;
        *boards = (const char**)info->subscribe_boards;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_info_board_at(ma_profile_info_t *info, size_t index, const char **o) {
    if(info && index >= 0 && index < info->no_of_boards && o) {
        if(index >= 0 && index < info->no_of_boards) {
            *o = info->subscribe_boards[index];
            return MA_OK;
        }
        return MA_ERROR_OUTOFBOUNDS;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_info_board_size(ma_profile_info_t *info, size_t *size) {
    if(info && size) {
        *size = info->no_of_boards;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_profile_info_get_last_seen(ma_profile_info_t *info, ma_task_time_t *time) {
    if(info && time) {
        memcpy(time, &info->last_seen, sizeof(ma_task_time_t));
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_info_get_user_type_str(ma_profile_info_t *info, char *str) {
    if(info && str) {
        switch(info->user_type) {
           case MA_PROFILE_INFO_USER_TYPE_NORMAL: strcpy(str, MA_PROFILE_INFO_USER_TYPE_NORMAL_STR); break;
           case MA_PROFILE_INFO_USER_TYPE_CUSTOMER_EXECUTIVE: strcpy(str, MA_PROFILE_INFO_USER_TYPE_CUSTOMER_EXECUTIVE_STR); break;
           case MA_PROFILE_INFO_USER_TYPE_OPERATION: strcpy(str, MA_PROFILE_INFO_USER_TYPE_OPERATION_STR); break;
           case MA_PROFILE_INFO_USER_TYPE_ADMIN: strcpy(str, MA_PROFILE_INFO_USER_TYPE_ADMIN_STR); break;
        } 
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_info_get_order_count(ma_profile_info_t *info, size_t *count) {
    if(info && count) {
        *count = info->order_count;
        return MA_OK;
    }
    return MA_OK;
}

ma_error_t ma_profile_info_get_from_city(ma_profile_info_t *info, char *city) {
    return info && city && strncpy(city, info->from_city, MA_MAX_LEN) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_info_get_to_city(ma_profile_info_t *info, char *city) {
    return info && city && strncpy(city, info->to_city, MA_MAX_LEN) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_info_get_privilege(ma_profile_info_t *info, ma_profile_info_privilege_t *priv) {
   if(info && priv) {
      *priv = info->privi_type;
      return MA_OK;
   }
   return MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_info_get_user_type(ma_profile_info_t *info, ma_profile_info_user_type_t *type) {
    if(info && type) {
        *type = info->user_type;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}
ma_error_t ma_profile_info_get_email_id(ma_profile_info_t *info, char *id) {
    return info && id && strncpy(id, info->email, MA_MAX_LEN) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_info_get_user(ma_profile_info_t *info, char *user) {
    return info && user && strncpy(user, info->name, MA_MAX_NAME) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_info_get_contact(ma_profile_info_t *info, char *contact) {
    return info && contact && strncpy(contact, info->contact, MA_MAX_LEN) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_info_get_from_address(ma_profile_info_t *info, char *addr) {
    return info && addr && strncpy(addr, info->from_address, MA_MAX_BUFFER_LEN) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_info_get_to_address(ma_profile_info_t *info, char *addr) {
    return info && addr && strncpy(addr, info->to_address, MA_MAX_BUFFER_LEN) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_info_get_password(ma_profile_info_t *info, char *passwd) {
    return info && passwd &&  strncpy(passwd, info->passwd, MA_MAX_LEN) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_info_get_last_login(ma_profile_info_t *info, ma_uint32_t *time) {
    if(info) {
        *time = info->last_login;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_info_get_status(ma_profile_info_t *info, ma_profile_info_status_t *status) {
    if(info) {
        *status = info->status;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_info_get_source_pincode(ma_profile_info_t *info, char *pincode) {
    return info && pincode && strncpy(pincode, info->from_pincode, MA_MAX_LEN) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_info_get_dest_pincode(ma_profile_info_t *info, char *pincode) {
    return info && pincode && strncpy(pincode, info->to_pincode, MA_MAX_LEN) ? MA_OK : MA_ERROR_INVALIDARG;
}


ma_error_t ma_profile_info_convert_to_variant(ma_profile_info_t *info, ma_variant_t **variant) {
    if(info && variant) {
        ma_error_t err = MA_OK;
        ma_table_t *info_table = NULL;

        if(MA_OK == (err = ma_table_create(&info_table))) {
             {
                 char email_id[MA_MAX_BUFFER_LEN+1] = {0};

                 if(MA_OK == (err = ma_profile_info_get_email_id(info, email_id))) {
                     ma_variant_t *email =NULL;

                     MA_LOG(profiler_logger, MA_LOG_SEV_DEBUG, "email id (%s)", email_id);
                     if(MA_OK == (err = ma_variant_create_from_string(email_id, strlen(email_id), &email))) {
                         err = ma_table_add_entry(info_table, MA_PROFILE_INFO_ATTR_EMAIL_ID, email);
                         (void)ma_variant_release(email);
                     }
                 }
             }

             {
                  char user[MA_MAX_NAME+1] = {0};

                  if(MA_OK == (err = ma_profile_info_get_user(info, user))) {
                     ma_variant_t *name = NULL;

                     MA_LOG(profiler_logger, MA_LOG_SEV_DEBUG, "user (%s)", user);
                     if(MA_OK == (err = ma_variant_create_from_string(user, strlen(user), &name))) {
                         err = ma_table_add_entry(info_table, MA_PROFILE_INFO_ATTR_NAME, name);
                         (void)ma_variant_release(name);
                     }
                  }
             }

             {
                  char contact[MA_MAX_LEN+1] = {0};

                  if(MA_OK == (err = ma_profile_info_get_contact(info, contact))) {
                     ma_variant_t *number = NULL;

                     MA_LOG(profiler_logger, MA_LOG_SEV_DEBUG, "contact (%s)", contact);
                     if(MA_OK == (err = ma_variant_create_from_string(contact, strlen(contact), &number))) {
                         err = ma_table_add_entry(info_table, MA_PROFILE_INFO_ATTR_CONTACT, number);
                         (void)ma_variant_release(number);
                     }
                  }
             }

             {
                  char address[MA_MAX_BUFFER_LEN+1] = {0};

                  if(MA_OK == (err = ma_profile_info_get_from_address(info, address))) {
                     ma_variant_t *from = NULL;

                     MA_LOG(profiler_logger, MA_LOG_SEV_DEBUG, "address (%s)", address);
                     if(MA_OK == (err = ma_variant_create_from_string(address, strlen(address), &from))) {
                         err = ma_table_add_entry(info_table, MA_PROFILE_INFO_ATTR_FROM_ADDRESS, from);
                         (void)ma_variant_release(from);
                     }
                  }
             }
             {
                  char pincode[MA_MAX_LEN+1] = {0};

                  if(MA_OK == (err = ma_profile_info_get_source_pincode(info, pincode))) {
                     ma_variant_t *code = NULL;

                     MA_LOG(profiler_logger, MA_LOG_SEV_DEBUG, "pincode (%s)", pincode);
                     if(MA_OK == (err = ma_variant_create_from_string(pincode, strlen(pincode), &code))) {
                         err = ma_table_add_entry(info_table, MA_PROFILE_INFO_ATTR_FROM_PINCODE, code);
                         (void)ma_variant_release(code);
                     }
                  }
             }
             {
                  char to[MA_MAX_BUFFER_LEN+1] = {0};

                  if(MA_OK == (err = ma_profile_info_get_to_address(info, to))) {
                     ma_variant_t *addr = NULL;

                     MA_LOG(profiler_logger, MA_LOG_SEV_DEBUG, "address to (%s)", to);
                     if(MA_OK == (err = ma_variant_create_from_string(to, strlen(to), &addr))) {
                         err = ma_table_add_entry(info_table, MA_PROFILE_INFO_ATTR_TO_ADDRESS, addr);
                         (void)ma_variant_release(addr);
                     }
                  }
             }
             {
                  char pincode[MA_MAX_LEN+1] = {0};

                  if(MA_OK == (err = ma_profile_info_get_dest_pincode(info, pincode))) {
                     ma_variant_t *code = NULL;

                     if(MA_OK == (err = ma_variant_create_from_string(pincode, strlen(pincode), &code))) {
                         err = ma_table_add_entry(info_table, MA_PROFILE_INFO_ATTR_TO_PINCODE, code);
                         (void)ma_variant_release(code);
                     }
                  }
             }
             {
                  ma_profile_info_user_type_t type = MA_PROFILE_INFO_USER_TYPE_NORMAL;

                  if(MA_OK == (err = ma_profile_info_get_user_type(info, &type))) {
                      ma_variant_t *t = NULL;
                      char usrtype[MA_MAX_LEN+1] = {0};

                      ma_profile_info_get_user_type_str(info, usrtype);
                      MA_LOG(profiler_logger, MA_LOG_SEV_DEBUG, "user type (%s)", usrtype);
                      if(MA_OK == (err = ma_variant_create_from_uint32((ma_uint32_t)type, &t))) {
                          err = ma_table_add_entry(info_table, MA_PROFILE_INFO_ATTR_USER_TYPE, t);
                          (void)ma_variant_release(t);
                      }
                  }
             }
             {
                  ma_profile_info_privilege_t privi = MA_PROFILE_INFO_PRIVILEGE_RDONLY;

                  if(MA_OK == (err = ma_profile_info_get_privilege(info, &privi))) {
                      ma_variant_t *p = NULL;

                      if(MA_OK == (err = ma_variant_create_from_uint32(privi, &p))) {
                          err = ma_table_add_entry(info_table, MA_PROFILE_INFO_ATTR_PRIVILEGE_TYPE, p);
                          (void)ma_variant_release(p);
                      }
                  }
             }
             {
                  char pass[MA_MAX_LEN+1] = {0};
    
                  if(MA_OK == (err = ma_profile_info_get_password(info, pass))) {
                      ma_variant_t *passwd = NULL;

                      if(MA_OK == (err = ma_variant_create_from_string(pass, strlen(pass), &passwd))) {
                          err = ma_table_add_entry(info_table, MA_PROFILE_INFO_ATTR_PASSWORD, passwd);
                          (void)ma_variant_release(passwd);
                      }
                  }
             }
             {
                  char *gst = NULL;
    
                  if(MA_OK == (err = ma_profile_info_get_gstin(info, &gst))) {
                      ma_variant_t *gstin = NULL;

                      MA_LOG(profiler_logger, MA_LOG_SEV_DEBUG, "gstin (%s)", gst);
                      if(MA_OK == (err = ma_variant_create_from_string(gst, strlen(gst), &gstin))) {
                          err = ma_table_add_entry(info_table, MA_PROFILE_INFO_ATTR_GSTIN, gstin);
                          (void)ma_variant_release(gstin);
                      }
                  }
             }
             {
                  char *ssn = NULL;
    
                  if(MA_OK == (err = ma_profile_info_get_ssn(info, &ssn))) {
                      ma_variant_t *ssnv = NULL;

                      MA_LOG(profiler_logger, MA_LOG_SEV_DEBUG, "ssn (%s)", ssn);
                      if(MA_OK == (err = ma_variant_create_from_string(ssn, strlen(ssn), &ssnv))) {
                          err = ma_table_add_entry(info_table, MA_PROFILE_INFO_ATTR_SSN, ssnv);
                          (void)ma_variant_release(ssnv);
                      }
                  }
             }
             {
                  char *channel = NULL;
    
                  if(MA_OK == (err = ma_profile_info_get_channel(info, &channel))) {
                      ma_variant_t *channelv = NULL;

                      MA_LOG(profiler_logger, MA_LOG_SEV_DEBUG, "channel (%s)", channel);
                      if(MA_OK == (err = ma_variant_create_from_string(channel, strlen(channel), &channelv))) {
                          err = ma_table_add_entry(info_table, MA_PROFILE_INFO_ATTR_CHANNEL, channelv);
                          (void)ma_variant_release(channelv);
                      }
                  }
             }
             {
                  ma_profile_info_status_t status = MA_PROFILE_INFO_STATUS_NOT_ACTIVE;

                  if(MA_OK == (err = ma_profile_info_get_status(info, &status))) {
                      ma_variant_t *p = NULL;

                      if(MA_OK == (err = ma_variant_create_from_uint32(status, &p))) {
                          err = ma_table_add_entry(info_table, MA_PROFILE_INFO_ATTR_STATUS, p);
                          (void)ma_variant_release(p);
                      }
                  }
             }

             {
                  ma_uint32_t time = 0;

                  if(MA_OK == (err = ma_profile_info_get_last_login(info, &time))) {
                      ma_variant_t *p = NULL;

                      if(MA_OK == (err = ma_variant_create_from_uint32(time, &p))) {
                          err = ma_table_add_entry(info_table, MA_PROFILE_INFO_ATTR_LAST_LOGIN, p);
                          (void)ma_variant_release(p);
                      }
                  }
             }
             {
                  size_t count = 0;

                  if(MA_OK == (err = ma_profile_info_get_order_count(info, &count))) {
                      ma_variant_t *p = NULL;

                      if(MA_OK == (err = ma_variant_create_from_uint32(count, &p))) {
                          err = ma_table_add_entry(info_table, MA_PROFILE_INFO_ATTR_ORDER_COUNT, p);
                          (void)ma_variant_release(p);
                      }
                  }
             }
             {
                  char city[MA_MAX_LEN+1] = {0};
    
                  if(MA_OK == (err = ma_profile_info_get_from_city(info, city))) {
                      ma_variant_t *v = NULL;

                      MA_LOG(profiler_logger, MA_LOG_SEV_DEBUG, "city (%s)", city);
                      if(MA_OK == (err = ma_variant_create_from_string(city, strlen(city), &v))) {
                          err = ma_table_add_entry(info_table, MA_PROFILE_INFO_ATTR_FROM_CITY, v);
                          (void)ma_variant_release(v);
                      }
                  }
             }
             {
                  char city[MA_MAX_LEN+1] = {0};
    
                  if(MA_OK == (err = ma_profile_info_get_to_city(info, city))) {
                      ma_variant_t *v = NULL;

                      if(MA_OK == (err = ma_variant_create_from_string(city, strlen(city), &v))) {
                          err = ma_table_add_entry(info_table, MA_PROFILE_INFO_ATTR_TO_CITY, v);
                          (void)ma_variant_release(v);
                      }
                  }
             }
             {
                  const char *url = NULL;
    
                  if(MA_OK == (err = ma_profile_info_get_profile_url(info, &url))) {
                      ma_variant_t *v = NULL;

                      MA_LOG(profiler_logger, MA_LOG_SEV_DEBUG, "url (%s)", url);
                      if(MA_OK == (err = ma_variant_create_from_string(url, strlen(url), &v))) {
                          err = ma_table_add_entry(info_table, MA_PROFILE_INFO_ATTR_PROFILE_URL, v);
                          (void)ma_variant_release(v);
                      }
                  }
             }
             {
                  const char *gender = NULL;
    
                  if(MA_OK == (err = ma_profile_info_get_gender(info, &gender))) {
                      ma_variant_t *v = NULL;

                      MA_LOG(profiler_logger, MA_LOG_SEV_DEBUG, "gender (%s)", gender);
                      if(MA_OK == (err = ma_variant_create_from_string(gender, strlen(gender), &v))) {
                          err = ma_table_add_entry(info_table, MA_PROFILE_INFO_ATTR_GENDER, v);
                          (void)ma_variant_release(v);
                      }
                  }
             }
             {
                  const char *dob = NULL;
    
                  if(MA_OK == (err = ma_profile_info_get_dob(info, &dob))) {
                      ma_variant_t *v = NULL;

                      MA_LOG(profiler_logger, MA_LOG_SEV_DEBUG, "dob (%s)", dob);
                      if(MA_OK == (err = ma_variant_create_from_string(dob, strlen(dob), &v))) {
                          err = ma_table_add_entry(info_table, MA_PROFILE_INFO_ATTR_DOB, v);
                          (void)ma_variant_release(v);
                      }
                  }
             }
             {
                  ma_task_time_t last_seen = {0};
    
                  if(MA_OK == (err = ma_profile_info_get_last_seen(info, &last_seen))) {
                      ma_variant_t *v = NULL;

                      if(MA_OK == (err = ma_variant_create_from_raw(&last_seen, sizeof(last_seen), &v))) {
                          err = ma_table_add_entry(info_table, MA_PROFILE_INFO_ATTR_LAST_SEEN, v);
                          (void)ma_variant_release(v);
                      }
                  }
             }
             {
                  ma_bool_t subscribed = MA_FALSE;
    
                  if(MA_OK == (err = ma_profile_info_get_subscribed(info, &subscribed))) {
                      ma_variant_t *v = NULL;

                      if(MA_OK == (err = ma_variant_create_from_bool(subscribed, &v))) {
                          err = ma_table_add_entry(info_table, MA_PROFILE_INFO_ATTR_SUBSCRIBED, v);
                          (void)ma_variant_release(v);
                      }
                  }
             }
             {
                  ma_array_t *array = NULL;

                  if(MA_OK == (err = ma_array_create(&array))) {
                      size_t i = 0;

                      for(i = 0; i < info->no_of_boards; ++i) {
                          ma_variant_t *v = NULL;

                          if(MA_OK == (err = ma_variant_create_from_string(info->subscribe_boards[i], strlen(info->subscribe_boards[i]), &v))) {
                              (void)ma_array_push(array, v);
                              (void)ma_variant_release(v);
                          }
                      }
                      {
                          ma_variant_t *v = NULL;

                          if(MA_OK == (err = ma_variant_create_from_array(array, &v))) {
                              err = ma_table_add_entry(info_table, MA_PROFILE_INFO_ATTR_BOARDS, v);
                              (void)ma_variant_release(v);
                          }
                      }
                      (void)ma_array_release(array);
                  }
             }
             err = ma_variant_create_from_table(info_table, variant);
             (void)ma_table_release(info_table);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}


static void for_each_board(size_t index, ma_variant_t *v, void *self, ma_bool_t *stop_loop) {
    if(v && self && stop_loop) {
        ma_error_t err = MA_OK;
        ma_profile_info_t *info = (ma_profile_info_t*)self;
        ma_buffer_t *buf = NULL;

        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
            const char *pstr = NULL;
            size_t len = 0;

            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                if(pstr) {
                    err = ma_profile_info_add_board(info, pstr);
                }
            }
            (void)ma_buffer_release(buf);
        }
    }
}
ma_error_t ma_profile_info_convert_from_variant(ma_variant_t *variant, ma_profile_info_t **info) {
    if(info && variant) {
        ma_error_t err = MA_OK;

        if(MA_OK == (err = ma_profile_info_create(info, MA_PROFILE_INFO_USER_TYPE_NORMAL))) {
            ma_table_t *info_table = NULL;

            if(MA_OK == (err = ma_variant_get_table(variant, &info_table))) {
                {
                    ma_variant_t *email = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_PROFILE_INFO_ATTR_EMAIL_ID, &email))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(email, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    err = ma_profile_info_set_email_id((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(email);
                    }
                }
                {
                    ma_variant_t *name = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_PROFILE_INFO_ATTR_NAME, &name))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(name, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    err = ma_profile_info_set_user((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(name);
                    }
                }
                {
                    ma_variant_t *contact = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_PROFILE_INFO_ATTR_CONTACT, &contact))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(contact, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    err = ma_profile_info_set_contact((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(contact);
                    }
                }
                {
                    ma_variant_t *address = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_PROFILE_INFO_ATTR_FROM_ADDRESS, &address))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(address, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    err = ma_profile_info_set_from_address((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(address);
                    }
                }

                {
                    ma_variant_t *pincode = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_PROFILE_INFO_ATTR_FROM_PINCODE, &pincode))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(pincode, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    err = ma_profile_info_set_source_pincode((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(pincode);
                    }
                }

                {
                    ma_variant_t *address = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_PROFILE_INFO_ATTR_TO_ADDRESS, &address))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(address, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    err = ma_profile_info_set_to_address((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(address);
                    }
                }

                {
                    ma_variant_t *pincode = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_PROFILE_INFO_ATTR_TO_PINCODE, &pincode))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(pincode, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    err = ma_profile_info_set_dest_pincode((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(pincode);
                    }
                }
                {
                    ma_variant_t *type = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_PROFILE_INFO_ATTR_USER_TYPE, &type))) {
                        ma_profile_info_user_type_t value = 0;

                        if(MA_OK == (err = ma_variant_get_uint32(type, &value)))
                            err = ma_profile_info_set_user_type((*info), value);
                        (void)ma_variant_release(type);
                    }
                }
                {
                    ma_variant_t *type = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_PROFILE_INFO_ATTR_ORDER_COUNT, &type))) {
                        ma_uint32_t value = 0;

                        if(MA_OK == (err = ma_variant_get_uint32(type, &value)))
                            err = ma_profile_info_set_order_count((*info), value);
                        (void)ma_variant_release(type);
                    }
                }
                {
                    ma_variant_t *type = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_PROFILE_INFO_ATTR_PRIVILEGE_TYPE, &type))) {
                        ma_profile_info_privilege_t value = 0;

                        if(MA_OK == (err = ma_variant_get_uint32(type, &value)))
                            err = ma_profile_info_set_privilege((*info), value);
                        (void)ma_variant_release(type);
                    }
                }
                {
                    ma_variant_t *passwd = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_PROFILE_INFO_ATTR_PASSWORD, &passwd))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(passwd, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    err = ma_profile_info_set_password((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(passwd);
                    }
                }
                {
                    ma_variant_t *type = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_PROFILE_INFO_ATTR_STATUS, &type))) {
                        ma_profile_info_status_t value = 0;

                        if(MA_OK == (err = ma_variant_get_uint32(type, &value)))
                            err = ma_profile_info_set_status((*info), value);
                        (void)ma_variant_release(type);
                    }
                }
                {
                    ma_variant_t *type = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_PROFILE_INFO_ATTR_LAST_LOGIN, &type))) {
                        ma_profile_info_privilege_t value = 0;

                        if(MA_OK == (err = ma_variant_get_uint32(type, &value)))
                            err = ma_profile_info_set_last_login((*info), value);
                        (void)ma_variant_release(type);
                    }
                }
                {
                    ma_variant_t *type = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_PROFILE_INFO_ATTR_SUBSCRIBED, &type))) {
                        ma_bool_t value = MA_FALSE;

                        if(MA_OK == (err = ma_variant_get_bool(type, &value)))
                            err = ma_profile_info_set_subscribed((*info), value);
                        (void)ma_variant_release(type);
                    }
                }
                {
                    ma_variant_t *city = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_PROFILE_INFO_ATTR_FROM_CITY, &city))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(city, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    err = ma_profile_info_set_from_city((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(city);
                    }
                }
                {
                    ma_variant_t *city = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_PROFILE_INFO_ATTR_TO_CITY, &city))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(city, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    err = ma_profile_info_set_to_city((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(city);
                    }
                }
                {
                    ma_variant_t *url = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_PROFILE_INFO_ATTR_PROFILE_URL, &url))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(url, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    err = ma_profile_info_set_profile_url((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(url);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_PROFILE_INFO_ATTR_GENDER, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    err = ma_profile_info_set_gender((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_PROFILE_INFO_ATTR_DOB, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    err = ma_profile_info_set_dob((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_PROFILE_INFO_ATTR_GSTIN, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    err = ma_profile_info_set_gstin((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_PROFILE_INFO_ATTR_SSN, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    err = ma_profile_info_set_ssn((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_PROFILE_INFO_ATTR_CHANNEL, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    err = ma_profile_info_set_channel((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *last = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_PROFILE_INFO_ATTR_LAST_SEEN, &last))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_raw_buffer(last, &buf))) {
                            const unsigned char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_raw(buf, &pstr, &len))) {
                                if(pstr) {
                                    ma_task_time_t seen = {0};

                                    memcpy(&seen, pstr, sizeof(ma_task_time_t));
                                    err = ma_profile_info_set_last_seen((*info), seen);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(last);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_PROFILE_INFO_ATTR_BOARDS, &v))) {
                        ma_array_t *array = NULL;

                        if(MA_OK == (err = ma_variant_get_array(v, &array))) {
                            ma_array_foreach(array, &for_each_board, (*info));
                            (void)ma_array_release(array);
                        }
                        (void)ma_variant_release(v);
                    }
                }


                (void)ma_table_release(info_table);
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static void for_each(const char *key, ma_variant_t *value, void *cb_data, ma_bool_t *stop_loop) {
    if(key && value && cb_data && stop_loop) {
        ma_profile_info_t *info = (ma_profile_info_t*)cb_data;
        ma_vartype_t type = MA_VARTYPE_NULL;
        char buf[MA_MAX_LEN+1] = {0};

        (void)ma_variant_get_type(value, &type);
        if(MA_VARTYPE_UINT32 == type) {
            ma_uint32_t t = 0;

            (void)ma_variant_get_uint32(value, &t);
            MA_MSC_SELECT(_snprintf, snprintf)(buf, MA_MAX_LEN, "%u", t);
            if(!strcmp(key, MA_PROFILE_INFO_ATTR_USER_TYPE)) {
                (void)ma_profile_info_get_user_type_str(info, buf);
            }
            (void)ma_json_add_element(info->json, key, buf);
        } else if(MA_VARTYPE_INT32 == type) {
            ma_int32_t t = 0;

            (void)ma_variant_get_int32(value, &t);
            MA_MSC_SELECT(_snprintf, snprintf)(buf, MA_MAX_LEN, "%d", t);
            (void)ma_json_add_element(info->json, key, buf);
        } else if(MA_VARTYPE_BOOL == type) {
            ma_bool_t t = MA_FALSE;

            (void)ma_variant_get_bool(value, &t);
            MA_MSC_SELECT(_snprintf, snprintf)(buf, MA_MAX_LEN, "%d", t);
            (void)ma_json_add_element(info->json, key, buf);
        } else if(MA_VARTYPE_STRING == type) {
            ma_buffer_t *buf = NULL;

            if(MA_OK == ma_variant_get_string_buffer(value, &buf)) {
                const char *pstr = NULL;
                size_t len = 0;

                if(MA_OK == ma_buffer_get_string(buf, &pstr, &len)) {
                    if(pstr) {
                        (void)ma_json_add_element(info->json, key, pstr);
                    }
                }
                (void)ma_buffer_release(buf);
            }
        }
    }
}

ma_error_t ma_profile_info_convert_to_json(ma_profile_info_t *info, ma_json_t **json) {
    if(info && json) {
        ma_error_t err = MA_OK;
        ma_variant_t *v = NULL;

        if(MA_OK == (err = ma_profile_info_convert_to_variant(info, &v))) {
            ma_table_t *table = NULL;

            if(MA_OK == (err = ma_variant_get_table(v, &table))) {
                if(MA_OK == (err = ma_table_foreach(table, &for_each, info))) {
                    *json = info->json;
                }
                (void)ma_table_release(table);
            }
            (void)ma_variant_release(v);
        }
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_info_convert_from_json(const char *json_str, ma_profile_info_t **pinfo) {
    if(json_str && pinfo) {
        ma_error_t err = MA_ERROR_PRECONDITION;
        ma_cjson *json = ma_cjson_Parse(json_str);
        
        if(json) {
            if(MA_OK == (err = ma_profile_info_create(pinfo, MA_PROFILE_INFO_USER_TYPE_NORMAL))) {
                ma_cjson *j  = NULL;
                ma_profile_info_t *info  = *pinfo;

                if(ma_cjson_HasObjectItem(json, MA_PROFILE_INFO_ATTR_NAME)) {
                    j = ma_cjson_GetObjectItem(json, MA_PROFILE_INFO_ATTR_NAME);
                    if(j->valuestring) {
                        MA_LOG(profiler_logger, MA_LOG_SEV_DEBUG, "name (%s)", j->valuestring);
                        (void)ma_profile_info_set_user(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_PROFILE_INFO_ATTR_FROM_ADDRESS)) {
                    j = ma_cjson_GetObjectItem(json, MA_PROFILE_INFO_ATTR_FROM_ADDRESS);
                    if(j->valuestring) {
                        MA_LOG(profiler_logger, MA_LOG_SEV_DEBUG, "address (%s)", j->valuestring);
                        (void)ma_profile_info_set_from_address(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_PROFILE_INFO_ATTR_FROM_CITY)) {
                    j = ma_cjson_GetObjectItem(json, MA_PROFILE_INFO_ATTR_FROM_CITY);
                    if(j->valuestring) {
                        MA_LOG(profiler_logger, MA_LOG_SEV_DEBUG, "city (%s)", j->valuestring);
                        (void)ma_profile_info_set_from_city(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_PROFILE_INFO_ATTR_FROM_PINCODE)) {
                    j = ma_cjson_GetObjectItem(json, MA_PROFILE_INFO_ATTR_FROM_PINCODE);
                    if(j->valuestring) {
                        MA_LOG(profiler_logger, MA_LOG_SEV_DEBUG, "pincode (%s)", j->valuestring);
                        (void)ma_profile_info_set_source_pincode(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_PROFILE_INFO_ATTR_GENDER)) {
                    j = ma_cjson_GetObjectItem(json, MA_PROFILE_INFO_ATTR_GENDER);
                    if(j->valuestring) {
                        MA_LOG(profiler_logger, MA_LOG_SEV_DEBUG, "gender (%s)", j->valuestring);
                        (void)ma_profile_info_set_gender(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_PROFILE_INFO_ATTR_DOB)) {
                    j = ma_cjson_GetObjectItem(json, MA_PROFILE_INFO_ATTR_DOB);
                    if(j->valuestring) {
                        MA_LOG(profiler_logger, MA_LOG_SEV_DEBUG, "dob (%s)", j->valuestring);
                        (void)ma_profile_info_set_dob(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_PROFILE_INFO_ATTR_EMAIL_ID)) {
                    j = ma_cjson_GetObjectItem(json, MA_PROFILE_INFO_ATTR_EMAIL_ID);
                    if(j->valuestring) {
                        MA_LOG(profiler_logger, MA_LOG_SEV_DEBUG, "email (%s)", j->valuestring);
                        (void)ma_profile_info_set_email_id(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_PROFILE_INFO_ATTR_CONTACT)) {
                    j = ma_cjson_GetObjectItem(json, MA_PROFILE_INFO_ATTR_CONTACT);
                    if(j->valuestring) {
                        MA_LOG(profiler_logger, MA_LOG_SEV_DEBUG, "contact (%s)", j->valuestring);
                        (void)ma_profile_info_set_contact(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_PROFILE_INFO_ATTR_GSTIN)) {
                    j = ma_cjson_GetObjectItem(json, MA_PROFILE_INFO_ATTR_GSTIN);
                    if(j->valuestring) {
                        MA_LOG(profiler_logger, MA_LOG_SEV_DEBUG, "gstin (%s)", j->valuestring);
                        (void)ma_profile_info_set_gstin(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_PROFILE_INFO_ATTR_PASSWORD)) {
                    j = ma_cjson_GetObjectItem(json, MA_PROFILE_INFO_ATTR_PASSWORD);
                    if(j->valuestring) {
                        MA_LOG(profiler_logger, MA_LOG_SEV_DEBUG, "password (%s)", j->valuestring);
                        (void)ma_profile_info_set_password(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_PROFILE_INFO_ATTR_REFERRED_BY)) {
                    j = ma_cjson_GetObjectItem(json, MA_PROFILE_INFO_ATTR_REFERRED_BY);
                    if(j->valuestring) {
                        MA_LOG(profiler_logger, MA_LOG_SEV_DEBUG, "referred (%s)", j->valuestring);
                        // TODO add referred by
                        //(void)ma_profile_info_set_about(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_PROFILE_INFO_ATTR_PROFILE_URL)) {
                    j = ma_cjson_GetObjectItem(json, MA_PROFILE_INFO_ATTR_PROFILE_URL);
                    if(j->valuestring) {
                        MA_LOG(profiler_logger, MA_LOG_SEV_DEBUG, "profile url (%s)", j->valuestring);
                        (void)ma_profile_info_set_profile_url(info, j->valuestring);
                        // TODO add referred by
                        //(void)ma_profile_info_set_about(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_PROFILE_INFO_ATTR_SUBSCRIBED)) {
                    j = ma_cjson_GetObjectItem(json, MA_PROFILE_INFO_ATTR_SUBSCRIBED);
                    if(j->valuestring) {
                        MA_LOG(profiler_logger, MA_LOG_SEV_DEBUG, "subscribed (%s)", j->valuestring);
                        (void)ma_profile_info_set_subscribed(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_PROFILE_INFO_ATTR_SSN)) {
                    j = ma_cjson_GetObjectItem(json, MA_PROFILE_INFO_ATTR_SSN);
                    if(j->valuestring) {
                        MA_LOG(profiler_logger, MA_LOG_SEV_DEBUG, "ssn (%s)", j->valuestring);
                        (void)ma_profile_info_set_ssn(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_PROFILE_INFO_ATTR_CHANNEL)) {
                    j = ma_cjson_GetObjectItem(json, MA_PROFILE_INFO_ATTR_CHANNEL);
                    if(j->valuestring) {
                        MA_LOG(profiler_logger, MA_LOG_SEV_DEBUG, "channel (%s)", j->valuestring);
                        (void)ma_profile_info_set_channel(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_PROFILE_INFO_ATTR_USER_TYPE)) {
                    j = ma_cjson_GetObjectItem(json, MA_PROFILE_INFO_ATTR_USER_TYPE);
                    if(j->valuestring) {
                        MA_LOG(profiler_logger, MA_LOG_SEV_DEBUG, "user type (%s)", j->valuestring);
                        if(!strcmp(j->valuestring, MA_PROFILE_INFO_USER_TYPE_NORMAL_STR))
                            (void)ma_profile_info_set_user_type(info, MA_PROFILE_INFO_USER_TYPE_NORMAL);
                        else if(!strcmp(j->valuestring, MA_PROFILE_INFO_USER_TYPE_CUSTOMER_EXECUTIVE_STR))
                            (void)ma_profile_info_set_user_type(info, MA_PROFILE_INFO_USER_TYPE_CUSTOMER_EXECUTIVE);
                        else if(!strcmp(j->valuestring, MA_PROFILE_INFO_USER_TYPE_OPERATION_STR))
                            (void)ma_profile_info_set_user_type(info, MA_PROFILE_INFO_USER_TYPE_OPERATION);
                        else if(!strcmp(j->valuestring, MA_PROFILE_INFO_USER_TYPE_ADMIN_STR))
                            (void)ma_profile_info_set_user_type(info, MA_PROFILE_INFO_USER_TYPE_ADMIN);
                        else
                            (void)ma_profile_info_set_user_type(info, MA_PROFILE_INFO_USER_TYPE_NORMAL);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_PROFILE_INFO_ATTR_STATUS)) {
                    j = ma_cjson_GetObjectItem(json, MA_PROFILE_INFO_ATTR_STATUS);
                    if(j->valuestring) {
                        MA_LOG(profiler_logger, MA_LOG_SEV_DEBUG, "status (%s)", j->valuestring);
                        (void)ma_profile_info_set_status_from_string(info, j->valuestring);
                    }
                }
            }
            ma_cjson_Delete(json);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}
