#include "ma/internal/utils/profiler/ma_profiler.h"
#include "ma/internal/utils/profiler/ma_profile_info.h"
#include "ma/internal/utils/profiler/ma_profiler_datastore.h"
#include "ma/ma_msgbus.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/defs/ma_profile_defs.h"
#include "ma/internal/ma_macros.h"

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "profiler"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <uv.h>

ma_logger_t *profiler_logger;

struct ma_profiler_s {
    char path[MA_MAX_PATH_LEN+1];
    ma_msgbus_t *msgbus;
    ma_ds_t* datastore;
};


ma_error_t ma_profiler_create(ma_msgbus_t *msgbus, ma_ds_t *datastore, const char *base_path, ma_profiler_t **profiler) {
    if(msgbus && datastore && base_path && profiler) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;

        if((*profiler = (ma_profiler_t*)calloc(1, sizeof(ma_profiler_t)))) {
            (*profiler)->msgbus = msgbus;
            strncpy((*profiler)->path, base_path, MA_MAX_PATH_LEN);
            (*profiler)->datastore = datastore;
            err = MA_OK;
            MA_LOG(profiler_logger, MA_LOG_SEV_INFO, "profiler created successfully");
        }
        if(MA_OK != err) {
            MA_LOG(profiler_logger, MA_LOG_SEV_ERROR, "profiler creation failed, last error(%d)", err);
            if(*profiler)
                (void)ma_profiler_release(*profiler);
            *profiler = NULL;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_profiler_add(ma_profiler_t *profiler, ma_profile_info_t *info) {
    return profiler && info ? ma_profiler_datastore_write(profiler, info, profiler->datastore, profiler->path) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_profiler_add_variant(ma_profiler_t *profiler, const char *profile_id, ma_variant_t *var) {
    return profiler && profile_id && var ? ma_profiler_datastore_write_variant(profiler, profile_id, profiler->datastore, profiler->path, var) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_profiler_delete(ma_profiler_t *profiler, const char *profile) {
    return profiler && profile ? ma_profiler_datastore_remove_profile(profile, profiler->datastore, profiler->path) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_profiler_delete_all(ma_profiler_t *profiler) {
    return profiler ? ma_profiler_datastore_clear(profiler->datastore, profiler->path) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_profiler_get(ma_profiler_t *profiler, const char *profile, ma_profile_info_t **info) {
    return profiler && profile && info ? ma_profiler_datastore_get(profiler, profiler->datastore, profiler->path, profile, info) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_profiler_set_logger(ma_logger_t *logger) {
    if(logger) {
        profiler_logger = logger;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_profiler_get_msgbus(ma_profiler_t *profiler, ma_msgbus_t **msgbus) {
    if(profiler && msgbus) {
        *msgbus = profiler->msgbus;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_profiler_start(ma_profiler_t *profiler) {
    if(profiler) {
        MA_LOG(profiler_logger, MA_LOG_SEV_TRACE, "profiler started");
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_profiler_stop(ma_profiler_t *profiler) {
    if(profiler) {
        MA_LOG(profiler_logger, MA_LOG_SEV_TRACE, "profiler stopped");
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_profiler_release(ma_profiler_t *profiler) {
    if(profiler) {
        free(profiler);
    }
    return MA_ERROR_INVALIDARG;
}

