#include "ma/internal/utils/profiler/ma_profiler_datastore.h"
#include "ma/internal/defs/ma_profile_defs.h"
#include "ma/scheduler/ma_enumerator.h"
#include "ma/internal/ma_macros.h"
#include "ma/ma_log.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "profiler"

extern ma_logger_t *profiler_logger;

#define PROFILE_DATA MA_DATASTORE_PATH_SEPARATOR "profile_data"
#define PROFILE_ID MA_DATASTORE_PATH_SEPARATOR "profile_id"

#define PROFILE_DATA_PATH PROFILE_DATA PROFILE_ID MA_DATASTORE_PATH_SEPARATOR/* \"profile_data"\"profile_id"\ */


ma_error_t ma_profiler_datastore_read(ma_profiler_t *profiler, ma_ds_t *datastore, const char *ds_path) {
    if(profiler && datastore && ds_path) {
        ma_error_t err = MA_OK;
        ma_ds_iterator_t *profile_iterator = NULL;
        ma_buffer_t *profile_id_buf = NULL;
        char profile_data_base_path[MA_MAX_PATH_LEN] = {0};

        MA_MSC_SELECT(_snprintf, snprintf)(profile_data_base_path, MA_MAX_PATH_LEN, "%s%s", ds_path, PROFILE_DATA_PATH);
        MA_LOG(profiler_logger, MA_LOG_SEV_DEBUG, "profiler data store path(%s) reading...", profile_data_base_path);
        if(MA_OK == (err = ma_ds_iterator_create(datastore, 0, profile_data_base_path, &profile_iterator))) {
            while(MA_ERROR_NO_MORE_ITEMS != ma_ds_iterator_get_next(profile_iterator, &profile_id_buf) && profile_id_buf) {
                const char *profile_id = NULL;
                size_t size = 0;
                
                if(MA_OK == (err = ma_buffer_get_string(profile_id_buf, &profile_id, &size))) {
                    char profile_data_path[MA_MAX_PATH_LEN] = {0};
                    ma_variant_t *profile_var = NULL;
                    MA_MSC_SELECT(_snprintf, snprintf)(profile_data_path, MA_MAX_PATH_LEN, "%s", profile_data_base_path);/* base_path\"profile_data"\"profile_id"\4097 */
                    if(MA_OK == (err = ma_ds_get_variant(datastore, profile_data_path, profile_id, MA_VARTYPE_TABLE, &profile_var))) {

                        (void)ma_variant_release(profile_var);
                    }
                }
                (void)ma_buffer_release(profile_id_buf);
            }
            (void)ma_ds_iterator_release(profile_iterator);
        }

        return ((MA_ERROR_DS_PATH_NOT_FOUND == err) || (MA_ERROR_SCHEDULER_TASK_ID_CONFLICT == err)) ? MA_OK : err;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_profiler_datastore_write(ma_profiler_t *profiler, ma_profile_info_t *info, ma_ds_t *datastore, const char *ds_path) {
    if(profiler && info && datastore && ds_path) {
        char profile_data_path[MA_MAX_PATH_LEN] = {0};
        char profile_id[MA_MAX_LEN+1] = {0};
        ma_error_t err = MA_OK;
        ma_variant_t *var = NULL;

        (void)ma_profile_info_get_contact(info, profile_id);
        MA_MSC_SELECT(_snprintf, snprintf)(profile_data_path, MA_MAX_PATH_LEN, "%s%s", ds_path, PROFILE_DATA_PATH);
        MA_LOG(profiler_logger, MA_LOG_SEV_INFO, "profile id(%s), data path(%s) profile_data_path(%s)", profile_id, ds_path, profile_data_path);
        if(MA_OK == (err = ma_profile_info_convert_to_variant(info, &var))) {
            if(MA_OK != (err = ma_ds_set_variant(datastore, profile_data_path, profile_id, var)))
                MA_LOG(profiler_logger, MA_LOG_SEV_DEBUG, "profiler datastore write profile id(%s) failed", profile_id);
            (void)ma_variant_release(var);
        } else
            MA_LOG(profiler_logger, MA_LOG_SEV_DEBUG, "profiler datastore write profile id(%s) conversion profile info to variant failed, last error(%d)", profile_id, err);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_profiler_datastore_write_variant(ma_profiler_t *profiler, const char *profile_id, ma_ds_t *datastore, const char *ds_path, ma_variant_t *var) {
    if(profiler && profile_id && var && datastore && ds_path) {
        char profile_data_path[MA_MAX_PATH_LEN] = {0};
        ma_error_t err = MA_OK;

        MA_MSC_SELECT(_snprintf, snprintf)(profile_data_path, MA_MAX_PATH_LEN, "%s%s", ds_path, PROFILE_DATA_PATH);
        MA_LOG(profiler_logger, MA_LOG_SEV_INFO, "profile id(%s), data path(%s) profile_data_path(%s)", profile_id, ds_path, profile_data_path);
        if(MA_OK == (err = ma_ds_set_variant(datastore, profile_data_path, profile_id, var)))
            MA_LOG(profiler_logger, MA_LOG_SEV_INFO, "profiler datastore write profile id(%s) success", profile_id);
        else
            MA_LOG(profiler_logger, MA_LOG_SEV_ERROR, "profiler datastore write profile id(%s) failed, last error(%d)", profile_id, err);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_profiler_datastore_remove_profile(const char *profile, ma_ds_t *datastore, const char *ds_path) {
    if(profile && datastore && ds_path) {
        char profile_path[MA_MAX_PATH_LEN] = {0};
        
        MA_LOG(profiler_logger, MA_LOG_SEV_DEBUG, "profiler removing (%s) from datastore", profile);
        MA_MSC_SELECT(_snprintf, snprintf)(profile_path, MA_MAX_PATH_LEN, "%s%s",ds_path, PROFILE_DATA_PATH );/* base_path\"profile_data"\4097 */
        return ma_ds_rem(datastore, profile_path, profile, 1);
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_profiler_datastore_get(ma_profiler_t *profiler, ma_ds_t *datastore, const char *ds_path, const char *profile_id, ma_profile_info_t **info) {
    if(profiler && datastore && ds_path && profile_id && info) {
        ma_error_t err = MA_OK;
        char profile_data_base_path[MA_MAX_PATH_LEN] = {0};
        ma_variant_t *profile_var = NULL;

        MA_MSC_SELECT(_snprintf, snprintf)(profile_data_base_path, MA_MAX_PATH_LEN, "%s%s", ds_path, PROFILE_DATA_PATH);
        if(MA_OK == (err = ma_ds_get_variant(datastore, profile_data_base_path, profile_id, MA_VARTYPE_TABLE, &profile_var))) {
            if(MA_OK == (err = ma_profile_info_convert_from_variant(profile_var, info)))
                MA_LOG(profiler_logger, MA_LOG_SEV_TRACE, "profile(%s) exist in profiler DB", profile_id);
            (void)ma_variant_release(profile_var);
        } else 
            MA_LOG(profiler_logger, MA_LOG_SEV_ERROR, "profile(%s) does not exist in profiler DB, last error(%d)", profile_id, err);

        return err;
    }
    return MA_ERROR_INVALIDARG;

}

ma_error_t ma_profiler_datastore_clear(ma_ds_t *datastore, const char *ds_path) {
    return(datastore && ds_path) ? ma_ds_rem(datastore, ds_path, NULL, 1) : MA_ERROR_INVALIDARG;
}

