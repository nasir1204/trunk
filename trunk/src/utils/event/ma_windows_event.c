#include "ma/internal/utils/event/ma_windows_event.h"

/*TODO - Check the security */

typedef struct ma_authenticated_user_security_attrib_s ma_authenticated_user_security_attrib_t;
struct ma_authenticated_user_security_attrib_s {
    SECURITY_DESCRIPTOR         sd;
    SECURITY_ATTRIBUTES         s_attr;
    PSID                        sid[2];     /* one for local SYSTEM and one for admin */
    PACL                        acl;
    unsigned short              is_error;
};

static HRESULT initialize_authenticated_user_security_attrib(ma_authenticated_user_security_attrib_t *attrb, unsigned short b_restricted_access);
static void deinitialize_authenticated_user_security_attrib(ma_authenticated_user_security_attrib_t *attrb);


typedef HANDLE (WINAPI *CREATEEVENTEX_PROC)(LPSECURITY_ATTRIBUTES, LPCTSTR, DWORD, DWORD);

HRESULT ma_windows_event_create(const wchar_t *event_name, unsigned short is_manual_reset, unsigned short is_initial_state, ma_bool_t is_restricted_access, HANDLE *handle) {
    HMODULE hmodule = {0};
    CREATEEVENTEX_PROC pProc = NULL;
    ma_authenticated_user_security_attrib_t attribute = {0};
    HRESULT res = S_OK;

    if(!event_name || !handle)
        return E_FAIL;

    if(S_OK != (res = initialize_authenticated_user_security_attrib(&attribute, is_restricted_access)))
        return res;

    if((hmodule = LoadLibraryW(L"Kernel32.dll"))) {
        /* CreateEventEx is great. You can specify both what permissions the event
        will have (if it creates it) and what permissions you want (if it opens
        it). */
        if((pProc = (CREATEEVENTEX_PROC) GetProcAddress(hmodule, "CreateEventExW"))) {
            DWORD flags = 0;

            /* CREATE_EVENT_MANUAL_RESET*/
            if(is_manual_reset) flags |= 0x00000001;

            /*CREATE_EVENT_INITIAL_SET*/
            if(MA_TRUE == is_initial_state) flags |= 0x00000002;

            *handle = pProc((LPSECURITY_ATTRIBUTES )&(attribute.s_attr), event_name, flags, SYNCHRONIZE | EVENT_MODIFY_STATE);
            if(!(*handle)) res = GetLastError();
        }
        FreeLibrary(hmodule);
    }

    if(!pProc)
    {
        *handle = CreateEventW(&(attribute.s_attr), is_manual_reset, is_initial_state, event_name);
        if(!(*handle)) res = GetLastError();
    }

    (void)deinitialize_authenticated_user_security_attrib(&attribute);

    return res;
}

HRESULT ma_windows_event_open(const wchar_t *event_name, HANDLE *event_handle) {
    HRESULT res = S_OK;

    if(!event_name)
        return E_FAIL;

    *event_handle = OpenEventW(SYNCHRONIZE | EVENT_MODIFY_STATE, MA_FALSE, event_name) ;
    if(!(*event_handle)) res = GetLastError();

    return res;
}


static HRESULT initialize_authenticated_user_security_attrib(ma_authenticated_user_security_attrib_t *attrb, unsigned short b_restricted_access) {
    SID_IDENTIFIER_AUTHORITY sia = SECURITY_NT_AUTHORITY;

    attrb->sid[0]  = attrb->sid[1] = 0;

    if((AllocateAndInitializeSid(&sia, 1, SECURITY_AUTHENTICATED_USER_RID, 0, 0, 0, 0, 0, 0, 0, &attrb->sid[0]))
       && (AllocateAndInitializeSid(&sia, 1, SECURITY_LOCAL_SYSTEM_RID, 0, 0, 0, 0, 0, 0, 0, &attrb->sid[1]))) {

        /* Compute the size of the new ACL. */
        DWORD acl_size = GetLengthSid(attrb->sid[0]) + GetLengthSid(attrb->sid[1]) + sizeof(ACL) + ((sizeof(ACCESS_ALLOWED_ACE) - sizeof(DWORD)) * 2);

        /* Allocate a buffer for the new ACL.*/
        attrb->acl = (PACL)(HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, acl_size));
        if(attrb->acl) {
            /* Initialithe new ACL. */
            if(InitializeAcl(attrb->acl, acl_size, ACL_REVISION)) {
                /* Add the new ACE. */
                if(AddAccessAllowedAce(attrb->acl, ACL_REVISION, b_restricted_access ? GENERIC_READ|GENERIC_WRITE|GENERIC_EXECUTE : GENERIC_ALL, attrb->sid[0])) {
                    if(AddAccessAllowedAce(attrb->acl, ACL_REVISION, b_restricted_access ? GENERIC_READ|GENERIC_WRITE|GENERIC_EXECUTE : GENERIC_ALL, attrb->sid[1])) {
                        if(IsValidAcl(attrb->acl)) {
                            if(InitializeSecurityDescriptor(&attrb->sd, SECURITY_DESCRIPTOR_REVISION)) {
                                /* Set a new DACL for the security descriptor.*/
                                if(SetSecurityDescriptorDacl(&attrb->sd, TRUE, attrb->acl, FALSE)) {
                                    if(IsValidSecurityDescriptor(&attrb->sd)) {
                                        attrb->s_attr.nLength = sizeof(attrb->s_attr);
                                        attrb->s_attr.lpSecurityDescriptor = &attrb->sd;
                                        attrb->s_attr.bInheritHandle = FALSE;
                                        return S_OK;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    attrb->is_error = 1;
    deinitialize_authenticated_user_security_attrib(attrb);
    return GetLastError();
}


static void deinitialize_authenticated_user_security_attrib(ma_authenticated_user_security_attrib_t *attrb) {
    if(attrb) {
        if(attrb->sid[0]) FreeSid(attrb->sid[0]);
        if(attrb->sid[1]) FreeSid(attrb->sid[1]);
        if(attrb->acl)    HeapFree(GetProcessHeap(), 0, (LPVOID)(attrb->acl));
    }
}

