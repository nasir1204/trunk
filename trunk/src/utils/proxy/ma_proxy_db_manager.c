#include "ma/internal/utils/proxy/ma_proxy_internal.h"
#include "ma/internal/utils/database/ma_db_recordset.h"
#include "ma/internal/utils/database/ma_db_statement.h"
#include "ma/internal/utils/proxy/ma_proxy_db_manager.h"

/* Proxy DB API's*/
/* AGENT_PROXIES TABLE ATTRIBUTES
1. SERVER TEXT, 
2. PORT INTEGER, 
3. PROTOCOL_TYPE INTEGER NOT NULL, 
4. FLAG INTEGER NOT NULL, 
5. USE_AUTH INTEGER NOT NULL, 
6. USER_NAME TEXT, 
7. PASSWORD TEXT, 
PRIMARY KEY (SERVER, PORT, PROTOCOL_TYPE, FLAG)
*/

/* AGENT_PROXY_CONFIG TABLE ATTRIBUTES
1. PROXY_USAGE
2. BYPASS_LOCAL
3. ALLOW_USER_CONFIG
4. EXCLUSION_LIST
5. SYSTEM_PROXY_HTTP_AUTH_REQD
6. SYSTEM_PROXY_HTTP_USER
7. SYSTEM_PROXY_HTTP_PASSWD
5. SYSTEM_PROXY_FTP_AUTH_REQD
6. SYSTEM_PROXY_FTP_USER
7. SYSTEM_PROXY_FTP_PASSWD
PRIMARY KEY (SERVER, PORT, PROTOCOL_TYPE, FLAG)
*/

#define CREATE_PROXIES_TABLE_STMT       "CREATE TABLE IF NOT EXISTS AGENT_PROXIES(SERVER TEXT, PORT INTEGER, PROTOCOL_TYPE INTEGER NOT NULL, FLAG INTEGER NOT NULL, USE_AUTH INTEGER NOT NULL, USER_NAME TEXT, PASSWORD TEXT, PRIMARY KEY (SERVER, PORT, PROTOCOL_TYPE, FLAG) ON CONFLICT REPLACE)"

#define CREATE_PROXY_CONFIG_TABLE_STMT  "CREATE TABLE IF NOT EXISTS AGENT_PROXY_CONFIG(PROXY_USAGE INTEGER NOT NULL, BYPASS_LOCAL INTEGER NOT NULL, ALLOW_USER_CONFIG INTEGER NOT NULL, EXCLUSION_LIST TEXT, SYSTEM_PROXY_HTTP_AUTH_REQD INTEGER, SYSTEM_PROXY_HTTP_USER TEXT, SYSTEM_PROXY_HTTP_PASSWD TEXT, SYSTEM_PROXY_FTP_AUTH_REQD INTEGER, SYSTEM_PROXY_FTP_USER TEXT, SYSTEM_PROXY_FTP_PASSWD TEXT, PRIMARY KEY (PROXY_USAGE, BYPASS_LOCAL, ALLOW_USER_CONFIG) ON CONFLICT REPLACE)"

ma_error_t ma_proxy_db_create_schema(ma_db_t *db_handle) {
    ma_db_statement_t *db_stmt = NULL;
    ma_error_t rc;

	(void)ma_db_transaction_begin(db_handle) ;
    if( MA_OK == (rc = ma_db_statement_create(db_handle, CREATE_PROXIES_TABLE_STMT, &db_stmt))) {
        if(MA_OK == (rc = ma_db_statement_execute(db_stmt))) {
			(void)ma_db_statement_release(db_stmt) ;  db_stmt = NULL ;

			if( MA_OK == (rc = ma_db_statement_create(db_handle, CREATE_PROXY_CONFIG_TABLE_STMT, &db_stmt))) {
				rc = ma_db_statement_execute(db_stmt) ;
			}
		}
		(void)ma_db_statement_release(db_stmt) ;  db_stmt = NULL ;
	}
	(MA_OK == rc) ? (void)ma_db_transaction_end(db_handle) : (void)ma_db_transaction_cancel(db_handle) ;

    return rc ;
}

#define MA_PROXY_TABLE_FIELD_BASE					  0
#define MA_PROXY_TABLE_FIELD_NAME					  MA_PROXY_TABLE_FIELD_BASE + 1
#define MA_PROXY_TABLE_FIELD_PORT					  MA_PROXY_TABLE_FIELD_BASE + 2
#define MA_PROXY_TABLE_FIELD_PROTOCOL_TYPE			  MA_PROXY_TABLE_FIELD_BASE + 3
#define MA_PROXY_TABLE_FIELD_FLAG					  MA_PROXY_TABLE_FIELD_BASE + 4
#define MA_PROXY_TABLE_FIELD_USE_AUTH				  MA_PROXY_TABLE_FIELD_BASE + 5
#define MA_PROXY_TABLE_FIELD_USER_NAME				  MA_PROXY_TABLE_FIELD_BASE + 6
#define MA_PROXY_TABLE_FIELD_PASSWORD				  MA_PROXY_TABLE_FIELD_BASE + 7

#define ADD_PROXY_STMT "INSERT INTO  AGENT_PROXIES VALUES (?, ?, ?, ?, ?, ?, ?)"
ma_error_t ma_proxy_db_add_proxy(ma_db_t *db_handle, ma_proxy_t *proxy) {
    if(db_handle && proxy){
		ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;
		if( MA_OK == (rc = ma_db_statement_create(db_handle, ADD_PROXY_STMT, &db_stmt))
            && (MA_OK == (rc = ma_db_statement_set_string(db_stmt, MA_PROXY_TABLE_FIELD_NAME, 1, proxy->server, strlen(proxy->server))))
            && (MA_OK == (rc = ma_db_statement_set_int(db_stmt, MA_PROXY_TABLE_FIELD_PORT,proxy->server_port)))
            && (MA_OK == (rc = ma_db_statement_set_int(db_stmt, MA_PROXY_TABLE_FIELD_PROTOCOL_TYPE,proxy->type))) 
            && (MA_OK == (rc = ma_db_statement_set_int(db_stmt, MA_PROXY_TABLE_FIELD_FLAG,proxy->flag))) 
            && (MA_OK == (rc = ma_db_statement_set_int(db_stmt, MA_PROXY_TABLE_FIELD_USE_AUTH,proxy->is_auth_rqued))) 
            && (MA_OK == (rc = ma_db_statement_set_string(db_stmt, MA_PROXY_TABLE_FIELD_USER_NAME, 1, proxy->user_name, proxy->user_name?strlen(proxy->user_name):0)))
            && (MA_OK == (rc = ma_db_statement_set_string(db_stmt, MA_PROXY_TABLE_FIELD_PASSWORD, 1, proxy->user_passwd, proxy->user_passwd?strlen(proxy->user_passwd):0)))   )
		{
				rc = ma_db_statement_execute(db_stmt) ;
		}
		(void)ma_db_statement_release(db_stmt);
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

#define REMOVE_ALL_PROXIES_STMT "DELETE FROM AGENT_PROXIES"
ma_error_t ma_proxy_db_remove_all_proxies(ma_db_t *db_handle) {
    if(db_handle) {
		ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;

		if( MA_OK == (rc = ma_db_statement_create(db_handle, REMOVE_ALL_PROXIES_STMT, &db_stmt)))
		{
            rc = ma_db_statement_execute(db_stmt) ;
            (void)ma_db_statement_release(db_stmt) ;
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}


#define REMOVE_ALL_GLOBAL_PROXIES_STMT "DELETE FROM AGENT_PROXIES WHERE FLAG = 1"
ma_error_t ma_proxy_db_remove_all_global_proxies(ma_db_t *db_handle) {
	if(db_handle) {
		ma_db_statement_t *db_stmt = NULL ;
		ma_error_t rc = MA_OK ;

		if( MA_OK == (rc = ma_db_statement_create(db_handle, REMOVE_ALL_GLOBAL_PROXIES_STMT, &db_stmt)))
		{
			rc = ma_db_statement_execute(db_stmt) ;
			(void)ma_db_statement_release(db_stmt) ;
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

#define REMOVE_PROXY_STMT "DELETE FROM AGENT_PROXIES WHERE SERVER = ? AND PORT = ? AND PROTOCOL_TYPE = ? AND FLAG = ?"
ma_error_t ma_proxy_db_remove_proxy(ma_db_t *db_handle, ma_proxy_t *proxy) {
    if(db_handle && proxy){
		ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;

		if( MA_OK == (rc = ma_db_statement_create(db_handle, REMOVE_PROXY_STMT, &db_stmt))
            && (MA_OK == (rc = ma_db_statement_set_string(db_stmt, 1, 1, proxy->server, strlen(proxy->server))))
            && (MA_OK == (rc = ma_db_statement_set_int(db_stmt, 2,proxy->server_port)))
            && (MA_OK == (rc = ma_db_statement_set_int(db_stmt, 3,proxy->type))) 
            && (MA_OK == (rc = ma_db_statement_set_int(db_stmt, 4,proxy->flag)))    )
		{
            rc = ma_db_statement_execute(db_stmt) ;
		}
		(void)ma_db_statement_release(db_stmt) ;
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

#define GET_ALL_PROXIES_STMT   "SELECT * FROM AGENT_PROXIES"
#define GET_ALL_SERVER_PROXIES_STMT   "SELECT * FROM AGENT_PROXIES WHERE FLAG = 1"
#define GET_ALL_USER_PROXIES_STMT   "SELECT * FROM AGENT_PROXIES WHERE FLAG = 2"

ma_error_t ma_proxy_db_get_all_proxies(ma_db_t *db_handle, int flag, ma_proxy_list_t **proxy_list) {
    if(db_handle && proxy_list){
		ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;
		ma_db_recordset_t *db_record = NULL ;

        switch(flag) {
            case MA_PROXY_FLAG_SERVER_CONFIGURED :
                rc = ma_db_statement_create(db_handle, GET_ALL_SERVER_PROXIES_STMT, &db_stmt) ;
                break ;
            case MA_PROXY_FLAG_LOCAL_CONFIGURED :
                rc = ma_db_statement_create(db_handle, GET_ALL_USER_PROXIES_STMT, &db_stmt) ;
                break ;
            default :
                rc = ma_db_statement_create(db_handle, GET_ALL_PROXIES_STMT, &db_stmt) ;
                break ;
        }
        if(MA_OK == rc && (MA_OK == (rc = ma_db_statement_execute_query(db_stmt, &db_record)))) {
			ma_proxy_list_t *self = NULL ;

			if(MA_OK == (rc = ma_proxy_list_create(&self))) {
				ma_proxy_t *proxy = NULL ;
				size_t num_proxies = 0 ;

				while(MA_ERROR_NO_MORE_ITEMS != ma_db_recordset_next(db_record)) {
					if(MA_OK == ma_proxy_create(&proxy)) {
						char *str_value = NULL, *user = NULL, *passwd = NULL ;
						ma_int32_t int_value = 0 ;

						(void)ma_db_recordset_get_string(db_record, MA_PROXY_TABLE_FIELD_NAME, &str_value) ;
						(void)ma_proxy_set_address(proxy, str_value) ;	str_value = NULL ;

						(void)ma_db_recordset_get_int(db_record, MA_PROXY_TABLE_FIELD_PORT, &int_value) ;
						(void)ma_proxy_set_port(proxy, int_value) ;	int_value = 0 ;

						(void)ma_db_recordset_get_int(db_record, MA_PROXY_TABLE_FIELD_PROTOCOL_TYPE, &int_value) ;
						(void)ma_proxy_set_protocol_type(proxy, (ma_proxy_protocol_type_t)int_value) ;	int_value = 0 ;

						(void)ma_db_recordset_get_int(db_record, MA_PROXY_TABLE_FIELD_FLAG, &int_value) ;
						(void)ma_proxy_set_flag(proxy, int_value) ;	int_value = 0 ;

						(void)ma_db_recordset_get_int(db_record, MA_PROXY_TABLE_FIELD_USE_AUTH, &int_value) ;
						
						if(int_value != 0 ) {
							(void)ma_db_recordset_get_string(db_record, MA_PROXY_TABLE_FIELD_USER_NAME, &user) ;
							(void)ma_db_recordset_get_string(db_record, MA_PROXY_TABLE_FIELD_PASSWORD, &passwd) ;
						}
						(void)ma_proxy_set_authentication(proxy, (int_value == 0)?MA_FALSE:MA_TRUE, user, passwd) ;

						(void)ma_proxy_list_add_proxy(self, proxy) ;
						(void)ma_proxy_release(proxy) ;	proxy = NULL ;
					}
				}
				(void)ma_proxy_list_size(self, &num_proxies) ;
				if(num_proxies)	*proxy_list = self ;
				else	(void)ma_proxy_list_release(self) ;
			}
			(void)ma_db_recordset_release(db_record) ;	db_record = NULL ;
		}

		if(db_stmt)	{
			(void)ma_db_statement_release(db_stmt) ;
			db_stmt = NULL ;
		}

		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}



#define MA_PROXY_CONFIG_TABLE_FIELD_BASE							0
#define MA_PROXY_CONFIG_TABLE_FIELD_PROXY_USAGE						MA_PROXY_CONFIG_TABLE_FIELD_BASE + 1
#define MA_PROXY_CONFIG_TABLE_FIELD_BYPASS_LOCAL					MA_PROXY_CONFIG_TABLE_FIELD_BASE + 2
#define MA_PROXY_CONFIG_TABLE_FIELD_ALLOW_USER_CONFIG				MA_PROXY_CONFIG_TABLE_FIELD_BASE + 3
#define MA_PROXY_CONFIG_TABLE_FIELD_EXCLUSION_LIST					MA_PROXY_CONFIG_TABLE_FIELD_BASE + 4
#define MA_PROXY_CONFIG_TABLE_FIELD_SYSTEM_PROXY_HTTP_AUTH_REQD		MA_PROXY_CONFIG_TABLE_FIELD_BASE + 5
#define MA_PROXY_CONFIG_TABLE_FIELD_SYSTEM_PROXY_HTTP_USER			MA_PROXY_CONFIG_TABLE_FIELD_BASE + 6
#define MA_PROXY_CONFIG_TABLE_FIELD_SYSTEM_PROXY_HTTP_PASSWD		MA_PROXY_CONFIG_TABLE_FIELD_BASE + 7
#define MA_PROXY_CONFIG_TABLE_FIELD_SYSTEM_PROXY_FTP_AUTH_REQD		MA_PROXY_CONFIG_TABLE_FIELD_BASE + 8
#define MA_PROXY_CONFIG_TABLE_FIELD_SYSTEM_PROXY_FTP_USER			MA_PROXY_CONFIG_TABLE_FIELD_BASE + 9
#define MA_PROXY_CONFIG_TABLE_FIELD_SYSTEM_PROXY_FTP_PASSWD			MA_PROXY_CONFIG_TABLE_FIELD_BASE + 10

#define ADD_PROXY_CONFIG_STMT "INSERT INTO  AGENT_PROXY_CONFIG VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
ma_error_t ma_proxy_db_add_proxy_config(ma_db_t *db_handle, ma_proxy_config_t *self) {
    if(db_handle && self){
		ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;
		if( MA_OK == (rc = ma_db_statement_create(db_handle, ADD_PROXY_CONFIG_STMT, &db_stmt))) {

			(void)ma_db_statement_set_int(db_stmt, MA_PROXY_CONFIG_TABLE_FIELD_PROXY_USAGE, self->proxy_usage_type) ;
			(void)ma_db_statement_set_int(db_stmt, MA_PROXY_CONFIG_TABLE_FIELD_BYPASS_LOCAL, self->bypass_local_address) ;
			(void)ma_db_statement_set_int(db_stmt, MA_PROXY_CONFIG_TABLE_FIELD_ALLOW_USER_CONFIG, self->allow_user_config) ; 
			(void)ma_db_statement_set_string(db_stmt, MA_PROXY_CONFIG_TABLE_FIELD_EXCLUSION_LIST, 1, self->exclusion_list, self->exclusion_list?strlen(self->exclusion_list):0) ;

			if(self->system_proxy_auth) {
				(void)ma_db_statement_set_int(db_stmt, MA_PROXY_CONFIG_TABLE_FIELD_SYSTEM_PROXY_HTTP_AUTH_REQD, self->system_proxy_auth->http_auth_rqrd) ;
				(void)ma_db_statement_set_string(db_stmt, MA_PROXY_CONFIG_TABLE_FIELD_SYSTEM_PROXY_HTTP_USER, 1, self->system_proxy_auth->http_user_name, self->system_proxy_auth->http_user_name?strlen(self->system_proxy_auth->http_user_name):0) ;
				(void)ma_db_statement_set_string(db_stmt, MA_PROXY_CONFIG_TABLE_FIELD_SYSTEM_PROXY_HTTP_PASSWD, 1, self->system_proxy_auth->http_user_passwd, self->system_proxy_auth->http_user_passwd?strlen(self->system_proxy_auth->http_user_passwd):0) ;
				(void)ma_db_statement_set_int(db_stmt, MA_PROXY_CONFIG_TABLE_FIELD_SYSTEM_PROXY_FTP_AUTH_REQD, self->system_proxy_auth->ftp_auth_rqrd) ;
				(void)ma_db_statement_set_string(db_stmt, MA_PROXY_CONFIG_TABLE_FIELD_SYSTEM_PROXY_FTP_USER, 1, self->system_proxy_auth->ftp_user_name, self->system_proxy_auth->ftp_user_name?strlen(self->system_proxy_auth->ftp_user_name):0) ;
				(void)ma_db_statement_set_string(db_stmt, MA_PROXY_CONFIG_TABLE_FIELD_SYSTEM_PROXY_FTP_PASSWD, 1, self->system_proxy_auth->ftp_user_passwd, self->system_proxy_auth->ftp_user_passwd?strlen(self->system_proxy_auth->ftp_user_passwd):0) ;
			}
			else {
				(void)ma_db_statement_set_int(db_stmt, MA_PROXY_CONFIG_TABLE_FIELD_SYSTEM_PROXY_HTTP_AUTH_REQD, 0) ;
				(void)ma_db_statement_set_string(db_stmt, MA_PROXY_CONFIG_TABLE_FIELD_SYSTEM_PROXY_HTTP_USER, 1, NULL, 0) ;
				(void)ma_db_statement_set_string(db_stmt, MA_PROXY_CONFIG_TABLE_FIELD_SYSTEM_PROXY_HTTP_PASSWD, 1, NULL, 0) ;
				(void)ma_db_statement_set_int(db_stmt, MA_PROXY_CONFIG_TABLE_FIELD_SYSTEM_PROXY_FTP_AUTH_REQD, 0) ;
				(void)ma_db_statement_set_string(db_stmt, MA_PROXY_CONFIG_TABLE_FIELD_SYSTEM_PROXY_FTP_USER, 1, NULL, 0) ;
				(void)ma_db_statement_set_string(db_stmt, MA_PROXY_CONFIG_TABLE_FIELD_SYSTEM_PROXY_FTP_PASSWD, 1, NULL, 0) ;
			}

			rc = ma_db_statement_execute(db_stmt) ;

			if(MA_OK == rc) {
				size_t num_proxies, iter = 0 ;

				(void)ma_proxy_list_size(self->proxy_list, &num_proxies) ;

				for(iter = 0; iter < num_proxies; iter++) {
					ma_proxy_t *proxy ;
					(void)ma_proxy_list_get_proxy(self->proxy_list, iter, &proxy) ;
					(void)ma_proxy_db_add_proxy(db_handle, proxy) ;
					(void)ma_proxy_release(proxy) ;	proxy = NULL ;
				}
			}

			(void)ma_db_statement_release(db_stmt) ;
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

#define GET_PROXY_CONFIG_STMT   "SELECT * FROM AGENT_PROXY_CONFIG"
ma_error_t ma_proxy_db_get_proxy_config(ma_db_t *db_handle, ma_proxy_config_t **proxy_config) {
    if(db_handle && proxy_config){
		ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;
		ma_proxy_config_t *self = NULL ;

		if(MA_OK == (rc = ma_proxy_config_create(&self))) {
			if(MA_OK == (rc = ma_db_statement_create(db_handle, GET_PROXY_CONFIG_STMT, &db_stmt))) {
				ma_db_recordset_t *db_record = NULL ;

				if(MA_OK == (rc = ma_db_statement_execute_query(db_stmt, &db_record))) {
					if(MA_ERROR_NO_MORE_ITEMS != ma_db_recordset_next(db_record)) {
						ma_int32_t int_value = 0 ;
						char *str_value = NULL ;

						(void)ma_db_recordset_get_int(db_record, MA_PROXY_CONFIG_TABLE_FIELD_PROXY_USAGE, &int_value) ;
						self->proxy_usage_type = (ma_proxy_usage_type_t)int_value ;

						(void)ma_db_recordset_get_int(db_record, MA_PROXY_CONFIG_TABLE_FIELD_BYPASS_LOCAL, &int_value) ;
						self->bypass_local_address = (int_value == 0) ? MA_FALSE : MA_TRUE ;

						(void)ma_db_recordset_get_int(db_record, MA_PROXY_CONFIG_TABLE_FIELD_ALLOW_USER_CONFIG, &int_value) ;
						self->allow_user_config = (int_value == 0) ? MA_FALSE : MA_TRUE ;

						(void)ma_db_recordset_get_string(db_record, MA_PROXY_CONFIG_TABLE_FIELD_EXCLUSION_LIST, &str_value) ;
						if(str_value) self->exclusion_list = strdup(str_value) ;
						str_value = NULL ;

						if(self->proxy_usage_type == MA_PROXY_USAGE_TYPE_SYSTEM_CONFIGURED) {
							(void)ma_db_recordset_get_int(db_record, MA_PROXY_CONFIG_TABLE_FIELD_SYSTEM_PROXY_HTTP_AUTH_REQD, &int_value) ;
							self->system_proxy_auth->http_auth_rqrd = (int_value == 0) ? MA_FALSE : MA_TRUE ;

							if(self->system_proxy_auth->http_auth_rqrd) {
								(void)ma_db_recordset_get_string(db_record, MA_PROXY_CONFIG_TABLE_FIELD_SYSTEM_PROXY_HTTP_USER, &str_value) ;
								if(str_value) self->system_proxy_auth->http_user_name = strdup(str_value) ;
								str_value = NULL ;

								(void)ma_db_recordset_get_string(db_record, MA_PROXY_CONFIG_TABLE_FIELD_SYSTEM_PROXY_HTTP_PASSWD, &str_value) ;
								if(str_value) self->system_proxy_auth->http_user_passwd = strdup(str_value) ;
								str_value = NULL ;
							}

							(void)ma_db_recordset_get_int(db_record, MA_PROXY_CONFIG_TABLE_FIELD_SYSTEM_PROXY_FTP_AUTH_REQD, &int_value) ;
							self->system_proxy_auth->ftp_auth_rqrd = (int_value == 0) ? MA_FALSE : MA_TRUE ;

							if(self->system_proxy_auth->ftp_auth_rqrd) {
								(void)ma_db_recordset_get_string(db_record, MA_PROXY_CONFIG_TABLE_FIELD_SYSTEM_PROXY_FTP_USER, &str_value) ;
								if(str_value) self->system_proxy_auth->ftp_user_name = strdup(str_value) ;
								str_value = NULL ;

								(void)ma_db_recordset_get_string(db_record, MA_PROXY_CONFIG_TABLE_FIELD_SYSTEM_PROXY_FTP_PASSWD, &str_value) ;
								if(str_value) self->system_proxy_auth->ftp_user_passwd = strdup(str_value) ;
								str_value = NULL ;
							}					
						}
						*proxy_config = self ;
					}
					else
						rc = MA_ERROR_OBJECTNOTFOUND ;

					(void)ma_db_recordset_release(db_record) ;	db_record = NULL ;
				}
				(void)ma_db_statement_release(db_stmt) ;	db_stmt = NULL ;
			}
		}

		if(MA_OK != rc && self) {
			(void)ma_proxy_config_release(self) ; self = NULL ;
		}

		return rc ;     
	}
	return MA_ERROR_INVALIDARG ;
}


#define REMOVE_PROXY_CONFIG_STMT "DELETE FROM AGENT_PROXY_CONFIG"
ma_error_t ma_proxy_db_remove_proxy_config(ma_db_t *db_handle) {
    if(db_handle) {
		ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;

		if( MA_OK == (rc = ma_db_statement_create(db_handle, REMOVE_PROXY_CONFIG_STMT, &db_stmt)))
		{
            if(MA_OK == (rc = ma_db_statement_execute(db_stmt))) {
				/*(void)ma_proxy_db_remove_all_proxies(db_handle) ;*/
			}
            (void)ma_db_statement_release(db_stmt) ;
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}