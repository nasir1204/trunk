#include "ma/internal/utils/proxy/ma_proxy_internal.h"

#include "ma/internal/utils/proxy/ma_proxy_db_manager.h"


#include <stdlib.h>
#include <string.h>

static ma_error_t   get_value_from_table(ma_table_t *repo_tb, char *key, char **value){
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	if(repo_tb && key && value){
		const char *temp_str = NULL;
		size_t temp_str_len;
		ma_buffer_t *buff = NULL;
		ma_variant_t *variant = NULL;
		if(MA_OK == (rc = ma_table_get_value(repo_tb, key, &variant))){
			if(MA_OK == (rc = ma_variant_get_string_buffer(variant, &buff))) {
				if(MA_OK == (rc = ma_buffer_get_string(buff, &temp_str, &temp_str_len))) {
					if(temp_str)
						*value = strdup(temp_str);						
				}
				ma_buffer_release(buff);
			}
			ma_variant_release(variant);
		}
	}
	return rc;
}

ma_error_t   ma_proxy_from_variant(ma_variant_t *proxy_variant, ma_proxy_t **proxy){
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	if(proxy && proxy_variant){
		ma_variant_t *value = NULL;
		ma_table_t *proxy_tb = NULL;
		
		if(MA_OK == (rc = ma_proxy_create(proxy))){
			if(MA_OK == (rc = ma_variant_get_table(proxy_variant, &proxy_tb))){
				get_value_from_table(proxy_tb, "server", &(*proxy)->server);
				get_value_from_table(proxy_tb, "user_name", &(*proxy)->user_name);
				get_value_from_table(proxy_tb, "user_passwd", &(*proxy)->user_passwd);

				if(MA_OK == (rc = ma_table_get_value(proxy_tb, "server_port", &value))){
					rc = ma_variant_get_int32(value, &(*proxy)->server_port);
					(void)ma_variant_release(value); value = NULL;
				}

				if(MA_OK == (rc = ma_table_get_value(proxy_tb, "is_auth_rqued", &value))){
					rc = ma_variant_get_bool(value, &(*proxy)->is_auth_rqued);
					(void)ma_variant_release(value); value = NULL;
				}

				if(MA_OK == (rc = ma_table_get_value(proxy_tb, "type", &value))){
					rc = ma_variant_get_int32(value, (ma_int32_t*) &(*proxy)->type);
					(void)ma_variant_release(value); value = NULL;
				}

				if(MA_OK == (rc = ma_table_get_value(proxy_tb, "flag", &value))){
					rc = ma_variant_get_int32(value, &(*proxy)->flag);
					(void)ma_variant_release(value); value = NULL;
				}
				
				if(MA_OK == (rc = ma_table_get_value(proxy_tb, "nic_scope_id", &value))){
					rc = ma_variant_get_uint32(value, &(*proxy)->nic_scope_id);
					(void)ma_variant_release(value); value = NULL;
				}

				(void)ma_table_release(proxy_tb);
				return MA_OK;
			}
		}
	}
	return rc;
}

ma_error_t   ma_proxy_as_variant(ma_proxy_t *proxy, ma_variant_t **proxy_variant){
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	if(proxy && proxy_variant && proxy->server){
		ma_variant_t *value = NULL;
		ma_table_t *proxy_tb = NULL;
		if(MA_OK == (rc = ma_table_create(&proxy_tb))) {
			if(MA_OK == (rc = ma_variant_create_from_string(proxy->server, strlen(proxy->server), &value))) {
				rc = ma_table_add_entry(proxy_tb, "server", value);
				(void)ma_variant_release(value); value = NULL;
			}

			if((proxy->user_name) && (MA_OK == (rc = ma_variant_create_from_string(proxy->user_name, strlen(proxy->user_name), &value)))) {
					rc = ma_table_add_entry(proxy_tb, "user_name", value);
					(void)ma_variant_release(value); value = NULL;
			}

			if((proxy->user_passwd) && (MA_OK == (rc = ma_variant_create_from_string(proxy->user_passwd, strlen(proxy->user_passwd), &value)))) {
					rc = ma_table_add_entry(proxy_tb, "user_passwd", value);
					(void)ma_variant_release(value); value = NULL;
			}

			if((MA_OK == (rc = ma_variant_create_from_int32(proxy->server_port, &value)))) {
				rc = ma_table_add_entry(proxy_tb, "server_port", value);
				(void)ma_variant_release(value); value = NULL;
			}

			if((MA_OK == (rc = ma_variant_create_from_bool(proxy->is_auth_rqued, &value)))) {
				rc = ma_table_add_entry(proxy_tb, "is_auth_rqued", value);
				(void)ma_variant_release(value); value = NULL;
			}

			if((MA_OK == (rc = ma_variant_create_from_int32(proxy->type, &value)))) {
				rc = ma_table_add_entry(proxy_tb, "type", value);
				(void)ma_variant_release(value); value = NULL;
			}

			if((MA_OK == (rc = ma_variant_create_from_int32(proxy->flag, &value)))) {
				rc = ma_table_add_entry(proxy_tb, "flag", value);
				(void)ma_variant_release(value); value = NULL;
			}
			
			if((MA_OK == (rc = ma_variant_create_from_uint32(proxy->nic_scope_id, &value)))) {
				rc = ma_table_add_entry(proxy_tb, "nic_scope_id", value);
				(void)ma_variant_release(value); value = NULL;
			}

			rc = ma_variant_create_from_table(proxy_tb, proxy_variant);
			(void)ma_table_release(proxy_tb);
		}
	}
	return rc;
}

#ifndef MA_PROXY_CONFIG_DISABLED
ma_error_t   ma_proxy_config_from_variant(ma_variant_t *data, ma_proxy_config_t **proxy_config){
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	ma_proxy_config_t *p_config = NULL;
	if(proxy_config && data && (MA_OK == ma_proxy_config_create(&p_config)) ){
		ma_variant_t *value = NULL;
		ma_table_t *proxy_tb = NULL;
		if(MA_OK == (rc = ma_variant_get_table(data, &proxy_tb))){

			get_value_from_table(proxy_tb, "exclusion_list", &p_config->exclusion_list);

			if(MA_OK == (rc = ma_table_get_value(proxy_tb, "bypass_local_address", &value))){
				rc = ma_variant_get_bool(value, &p_config->bypass_local_address);
				(void)ma_variant_release(value); value = NULL;
			}

			if(MA_OK == (rc = ma_table_get_value(proxy_tb, "proxy_usage_type", &value))){
				rc = ma_variant_get_int32(value, (ma_int32_t*) &p_config->proxy_usage_type);
				(void)ma_variant_release(value); value = NULL;
			}
			if(MA_OK == (rc = ma_table_get_value(proxy_tb, "allow_user_config", &value))){
				rc = ma_variant_get_bool(value, &p_config->allow_user_config);
				(void)ma_variant_release(value); value = NULL;
			}

			/*ma_system_proxy_auth_info_t*/
			if(MA_OK == (rc = ma_table_get_value(proxy_tb, "system_proxy_auth", &value))){
				ma_table_t *system_proxy_auth_tb = NULL;
				ma_variant_t *var = NULL;
				if(MA_OK == (rc = ma_variant_get_table(value, &system_proxy_auth_tb))){
					get_value_from_table(system_proxy_auth_tb, "http_user_name", &(p_config->system_proxy_auth->http_user_name));
					get_value_from_table(system_proxy_auth_tb, "http_user_passwd", &(p_config->system_proxy_auth->http_user_passwd));
					get_value_from_table(system_proxy_auth_tb, "ftp_user_name", &(p_config->system_proxy_auth->ftp_user_name));
					get_value_from_table(system_proxy_auth_tb, "ftp_user_passwd", &(p_config->system_proxy_auth->ftp_user_passwd));	
					if(MA_OK == (rc = ma_table_get_value(system_proxy_auth_tb, "http_auth_rqrd", &var))){
						rc = ma_variant_get_bool(var, &(p_config->system_proxy_auth->http_auth_rqrd) );
						(void)ma_variant_release(var); var = NULL;
					}
					if(MA_OK == (rc = ma_table_get_value(system_proxy_auth_tb, "ftp_auth_rqrd", &var))){
						rc = ma_variant_get_bool(var, &(p_config->system_proxy_auth->ftp_auth_rqrd));
						(void)ma_variant_release(var); var = NULL;
					}
					(void)ma_table_release(system_proxy_auth_tb);
				}
				(void)ma_variant_release(value); value = NULL;
			}
			/* proxy list */
			if(MA_OK == (rc = ma_table_get_value(proxy_tb, "proxy_list", &value))){
				ma_array_t *proxy_arr = NULL;
				if(MA_OK == (rc = ma_variant_get_array(value, &proxy_arr))) {
					if(p_config->proxy_list->list) ma_array_release(p_config->proxy_list->list);					
					p_config->proxy_list->list = proxy_arr;
				}
				(void)ma_variant_release(value);
                value = NULL;
      		}

			(void)ma_table_release(proxy_tb);
			*proxy_config = p_config;
			return MA_OK;
		}
	}
	return rc;
}

ma_error_t   ma_proxy_config_as_variant(ma_proxy_config_t *proxy_config, ma_variant_t **proxy_config_variant){
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	if(proxy_config && proxy_config_variant){
		ma_variant_t *value = NULL;
		ma_table_t *proxy_tb = NULL;
		if(MA_OK == (rc = ma_table_create(&proxy_tb))) {
			ma_table_t *system_proxy_auth_tb = NULL;
			if((proxy_config->exclusion_list) && (MA_OK == (rc = ma_variant_create_from_string(proxy_config->exclusion_list, strlen(proxy_config->exclusion_list), &value)))) {
					rc = ma_table_add_entry(proxy_tb, "exclusion_list", value);
					(void)ma_variant_release(value); value = NULL;
			}

			if((MA_OK == (rc = ma_variant_create_from_bool(proxy_config->bypass_local_address, &value)))) {
				rc = ma_table_add_entry(proxy_tb, "bypass_local_address", value);
				(void)ma_variant_release(value); value = NULL;
			}

			if((MA_OK == (rc = ma_variant_create_from_int32(proxy_config->proxy_usage_type, &value)))) {
				rc = ma_table_add_entry(proxy_tb, "proxy_usage_type", value);
				(void)ma_variant_release(value); value = NULL;
			}

			if((MA_OK == (rc = ma_variant_create_from_bool(proxy_config->allow_user_config, &value)))) {
				rc = ma_table_add_entry(proxy_tb, "allow_user_config", value);
				(void)ma_variant_release(value); value = NULL;
			}

			/*ma_system_proxy_auth_info_t*/
			if((proxy_config->system_proxy_auth) && (MA_OK == (rc = ma_table_create(&system_proxy_auth_tb)))) {
				if((proxy_config->system_proxy_auth->ftp_user_name) && (MA_OK == (rc = ma_variant_create_from_string(proxy_config->system_proxy_auth->ftp_user_name, strlen(proxy_config->system_proxy_auth->ftp_user_name), &value)))) {
					rc = ma_table_add_entry(system_proxy_auth_tb, "ftp_user_name", value);
					(void)ma_variant_release(value); value = NULL;
				}
				if((proxy_config->system_proxy_auth->ftp_user_passwd) && (MA_OK == (rc = ma_variant_create_from_string(proxy_config->system_proxy_auth->ftp_user_passwd, strlen(proxy_config->system_proxy_auth->ftp_user_passwd), &value)))) {
					rc = ma_table_add_entry(system_proxy_auth_tb, "ftp_user_passwd", value);
					(void)ma_variant_release(value); value = NULL;
				}
				if((proxy_config->system_proxy_auth->http_user_name) && (MA_OK == (rc = ma_variant_create_from_string(proxy_config->system_proxy_auth->http_user_name, strlen(proxy_config->system_proxy_auth->http_user_name), &value)))) {
					rc = ma_table_add_entry(system_proxy_auth_tb, "http_user_name", value);
					(void)ma_variant_release(value); value = NULL;
				}
				if((proxy_config->system_proxy_auth->http_user_passwd) && (MA_OK == (rc = ma_variant_create_from_string(proxy_config->system_proxy_auth->http_user_passwd, strlen(proxy_config->system_proxy_auth->http_user_passwd), &value)))) {
					rc = ma_table_add_entry(system_proxy_auth_tb, "http_user_passwd", value);
					(void)ma_variant_release(value); value = NULL;
				}
				if((MA_OK == (rc = ma_variant_create_from_bool(proxy_config->system_proxy_auth->ftp_auth_rqrd, &value)))) {
					rc = ma_table_add_entry(system_proxy_auth_tb, "ftp_auth_rqrd", value);
					(void)ma_variant_release(value); value = NULL;
				}
				if((MA_OK == (rc = ma_variant_create_from_bool(proxy_config->system_proxy_auth->http_auth_rqrd, &value)))) {
					rc = ma_table_add_entry(system_proxy_auth_tb, "http_auth_rqrd", value);
					(void)ma_variant_release(value); value = NULL;
				}
				
				if((MA_OK == (rc = ma_variant_create_from_table(system_proxy_auth_tb, &value)))){
					rc = ma_table_add_entry(proxy_tb, "system_proxy_auth", value);
					(void)ma_variant_release(value); value = NULL;
				}
				(void)ma_table_release(system_proxy_auth_tb);
			}

			/*proxy list */
			if((proxy_config->proxy_list) && (MA_OK == (rc = ma_variant_create_from_array(proxy_config->proxy_list->list, &value)))) {
				rc = ma_table_add_entry(proxy_tb, "proxy_list", value);
				(void)ma_variant_release(value); value = NULL;
				}			

			rc = ma_variant_create_from_table(proxy_tb, proxy_config_variant);
			(void)ma_table_release(proxy_tb);
		}
	}

	return rc;
}


/* TBD - will add reference count on proxy config */
ma_error_t   ma_proxy_config_copy(ma_proxy_config_t *proxy_config_from, ma_proxy_config_t **proxy_config_to) {
    if(proxy_config_from && proxy_config_to){
        ma_error_t rc = MA_OK ;
	    ma_proxy_config_t *p_config = NULL;
	    if(MA_OK == (rc = ma_proxy_config_create(&p_config))) {
            p_config->allow_user_config = proxy_config_from->allow_user_config;
            p_config->bypass_local_address = proxy_config_from->bypass_local_address;
            p_config->exclusion_list = (proxy_config_from->exclusion_list) ? strdup(proxy_config_from->exclusion_list) : NULL;
            p_config->proxy_usage_type = proxy_config_from->proxy_usage_type;
			if(p_config->proxy_list->list) ma_array_release(p_config->proxy_list->list);
            ma_array_add_ref(p_config->proxy_list->list = proxy_config_from->proxy_list->list);
            
            p_config->system_proxy_auth->ftp_auth_rqrd = proxy_config_from->system_proxy_auth->ftp_auth_rqrd;
            p_config->system_proxy_auth->ftp_user_name = (proxy_config_from->system_proxy_auth->ftp_user_name) ? strdup(proxy_config_from->system_proxy_auth->ftp_user_name) : NULL;
            p_config->system_proxy_auth->ftp_user_passwd = (proxy_config_from->system_proxy_auth->ftp_user_passwd) ? strdup(proxy_config_from->system_proxy_auth->ftp_user_passwd) : NULL;
            p_config->system_proxy_auth->http_auth_rqrd = proxy_config_from->system_proxy_auth->http_auth_rqrd;
            p_config->system_proxy_auth->http_user_name = (proxy_config_from->system_proxy_auth->http_user_name) ? strdup(proxy_config_from->system_proxy_auth->http_user_name) : NULL;
            p_config->system_proxy_auth->http_user_passwd = (proxy_config_from->system_proxy_auth->http_user_passwd) ? strdup(proxy_config_from->system_proxy_auth->http_user_passwd) : NULL;
			*proxy_config_to = p_config;		
		}
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}
#endif
ma_error_t   ma_proxy_list_to_variant(ma_proxy_list_t *proxy_list, ma_variant_t **variant){
	if(proxy_list && variant){
		return ma_variant_create_from_array(proxy_list->list, variant);
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t   ma_proxy_list_from_variant(ma_variant_t *variant, ma_proxy_list_t **proxy_list){
	if(proxy_list && variant){
		ma_proxy_list_t *proxy_l = NULL;
		ma_error_t rc = MA_OK;

		if(MA_OK == (rc = ma_proxy_list_create(&proxy_l))){
			ma_array_t *proxy_arr = NULL;
		
			if(MA_OK == (rc = ma_variant_get_array(variant, &proxy_arr))) {
				if(proxy_l->list) ma_array_release(proxy_l->list);					
				proxy_l->list = proxy_arr;
				*proxy_list = proxy_l;
				return MA_OK;
			}		
			ma_proxy_list_release(proxy_l);
		}        
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}