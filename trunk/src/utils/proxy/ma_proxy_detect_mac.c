#include <CoreFoundation/CoreFoundation.h>
#include <CoreServices/CoreServices.h>
#include <SystemConfiguration/SystemConfiguration.h>
#include <CoreFoundation/CFDictionary.h>
#include <CoreFoundation/CFBase.h>
#include <SystemConfiguration/SCDynamicStore.h>
#include "ma/ma_log.h"
#include "ma/internal/utils/proxy/ma_proxy_internal.h"

#include <memory.h>
#include <stdlib.h>
#include <stdio.h>
#include <dlfcn.h>
#include <string.h>

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "proxy_detect"

#define SAFE_FREE(x) {if(x){free(x);x= NULL;}}
#define CFQRelease(cf) { if(cf) CFRelease(cf) ; }
#define kPrivateRunLoopMode CFSTR("com.apple.dts.CFProxySupportTool")

static ma_error_t getKeyConstantsProxy(ma_proxy_config_t *proxy_config_from, ma_logger_t *logger, const char *url, CFStringRef *, CFStringRef *, CFStringRef *) ;
static ma_error_t getProxyFromSystemPref(ma_proxy_config_t *proxy_config_from, ma_logger_t *logger, const char *url, CFStringRef, CFStringRef, CFStringRef, CFDictionaryRef, ma_proxy_list_t *) ;
static ma_error_t getProxyFromPACconfiguration(ma_proxy_config_t *proxy_config_from, ma_logger_t *logger, const char *url, CFDictionaryRef proxyDict, ma_proxy_list_t *ProxyList) ; 
static Boolean CFStringToCString(const CFStringRef str, char  **cstr) ;

static ma_error_t getProxiesForURLViaPAC(ma_proxy_config_t *proxy_config_from, ma_logger_t *logger, const char *url, const char *script_path, ma_proxy_list_t *ProxyList) ;
static ma_error_t proxiesForURLUsingScriptURL(ma_proxy_config_t *proxy_config_from, ma_logger_t *logger, const char *url, const char *path, ma_proxy_list_t *ProxyList) ;
static ma_error_t proxiesForURLUsingScript(ma_proxy_config_t *proxy_config_from, ma_logger_t *logger, const char *url, const char *script_path, ma_proxy_list_t *ProxyList) ;
static ma_error_t process_proxy(ma_proxy_config_t *proxy_config_from, ma_logger_t *logger, const char *, CFArrayRef, ma_proxy_list_t *) ;
void extractProxyAtIndex(ma_proxy_config_t *proxy_config_from, ma_logger_t *logger, const char *, CFIndex, CFDictionaryRef, ma_proxy_t **) ;

ma_error_t proxy_config_detect_system_proxy(ma_proxy_config_t *proxy_config_from, ma_logger_t *logger, const char *url, ma_proxy_list_t *ProxyList) {
    if(url && ProxyList) {
        ma_error_t ret = MA_OK ;
        
        CFStringRef enableKey ;
	    CFStringRef portKey ;
	    CFStringRef proxyKey ;
        CFDictionaryRef  proxyDict ;
	    SCDynamicStoreRef proxyStore ;

		MA_LOG(logger, MA_LOG_SEV_TRACE, "searching system proxy for url <%s>", url);
        if(NULL != (proxyStore = SCDynamicStoreCreate(NULL,CFSTR("MaProxyStore"),NULL,NULL))){
 			if( NULL == ( proxyDict = SCDynamicStoreCopyProxies(proxyStore)))
			{
				CFRelease(proxyStore) ;
				MA_LOG(logger, MA_LOG_SEV_ERROR, "SCDynamicStoreCopyProxies detect proxy failed");
            	return MA_ERROR_NETWORK_GET_PROXY_FAILED ;
			}
		}else{	
			MA_LOG(logger, MA_LOG_SEV_ERROR, "SCDynamicStoreCreate detect proxy failed");
			return MA_ERROR_NETWORK_GET_PROXY_FAILED ;
		}

        if(MA_OK ==(ret = getKeyConstantsProxy(proxy_config_from, logger, url, &enableKey, &portKey, &proxyKey))) {
            if(MA_OK != (ret = getProxyFromSystemPref(proxy_config_from, logger, url, enableKey, portKey, proxyKey, proxyDict, ProxyList))) {
                ret = getProxyFromPACconfiguration(proxy_config_from, logger, url, proxyDict, ProxyList) ;
            }
        }
        CFRelease(proxyDict) ;
        CFRelease(proxyStore) ;
        return ret ;
     }

	MA_LOG(logger, MA_LOG_SEV_ERROR, "Invalid arguments");
	return MA_ERROR_INVALIDARG ;
}

ma_error_t getKeyConstantsProxy(ma_proxy_config_t *proxy_config_from, ma_logger_t *logger, const char *url, CFStringRef *enableKey, CFStringRef *portKey, CFStringRef *proxyKey) {
    //char *protocol = strstr(url, "://") ;

    if(!strncmp(url, "http://", strlen("http://"))) {
        *enableKey = kSCPropNetProxiesHTTPEnable ;
        *portKey = kSCPropNetProxiesHTTPPort ;
        *proxyKey = kSCPropNetProxiesHTTPProxy ;
		MA_LOG(logger, MA_LOG_SEV_TRACE, "Searching http proxy");
        return MA_OK ;
    }
    else if(!strncmp(url, "https://", strlen("https://"))) {
        *enableKey = kSCPropNetProxiesHTTPSEnable ;
        *portKey = kSCPropNetProxiesHTTPSPort ;
        *proxyKey = kSCPropNetProxiesHTTPSProxy ;
		MA_LOG(logger, MA_LOG_SEV_TRACE, "Searching https proxy");
        return MA_OK ;
    }
    else if(!strncmp(url, "ftp://", strlen("ftp://"))) {
        *enableKey = kSCPropNetProxiesFTPEnable ;
        *portKey = kSCPropNetProxiesFTPPort ;
        *proxyKey = kSCPropNetProxiesFTPProxy ;
		MA_LOG(logger, MA_LOG_SEV_TRACE, "Searching ftp proxy");
        return MA_OK ;
    }
    else {
		MA_LOG(logger, MA_LOG_SEV_TRACE, "invalid/unsupported protocol");
        return MA_ERROR_NETWORK_INVALID_URL ;
    }
}

ma_error_t getProxyFromSystemPref(ma_proxy_config_t *proxy_config_from, ma_logger_t *logger, const char *url, CFStringRef enableKey, CFStringRef portKey, CFStringRef proxyKey, CFDictionaryRef proxyDict, ma_proxy_list_t *ProxyList) {
    CFNumberRef enableNum ;
	CFStringRef hostStr ;	
	CFNumberRef portNum = 0;
    int enable ;
    char *host = NULL ;
	int port ;
    ma_error_t ret = MA_OK ;
	ma_proxy_t *proxy = NULL;

    if(NULL != (enableNum = (CFNumberRef)CFDictionaryGetValue(proxyDict, enableKey))){
		if(CFGetTypeID(enableNum) != CFNumberGetTypeID()){
			MA_LOG(logger, MA_LOG_SEV_ERROR, "CFGetTypeID(enableNum) != CFNumberGetTypeID()");
			return MA_ERROR_NETWORK_GET_PROXY_FAILED ;
		}
	}else{
		MA_LOG(logger, MA_LOG_SEV_DEBUG, "Either proxy is not set Or CFDictionaryGetValue for enableNum failed");
		return MA_ERROR_NETWORK_GET_PROXY_FAILED ;
	}

	if(!CFNumberGetValue(enableNum, kCFNumberIntType, &enable)){
		MA_LOG(logger, MA_LOG_SEV_ERROR, "CFNumberGetValue for enable failed");
		return MA_ERROR_NETWORK_GET_PROXY_FAILED ;
	}

	if(!enable){
		MA_LOG(logger, MA_LOG_SEV_ERROR, "System proxy is not enabled");
		return MA_ERROR_NETWORK_GET_PROXY_FAILED ;
	}

	if(NULL != (hostStr = (CFStringRef)CFDictionaryGetValue(proxyDict, proxyKey))){
		if(CFGetTypeID(hostStr) != CFStringGetTypeID()){
			MA_LOG(logger, MA_LOG_SEV_ERROR, "CFGetTypeID(hostStr) != CFStringGetTypeID()");
			return MA_ERROR_NETWORK_GET_PROXY_FAILED ;
		}
	}else{
		MA_LOG(logger, MA_LOG_SEV_ERROR, "CFDictionaryGetValue for host string failed");
		return MA_ERROR_NETWORK_GET_PROXY_FAILED ;
	}

	if(!CFStringToCString(hostStr,&host)){
		MA_LOG(logger, MA_LOG_SEV_ERROR, "CFStringToCString(hostStr,&host) failed");
		return MA_ERROR_NETWORK_GET_PROXY_FAILED ;
	}

	if(NULL != (portNum = (CFNumberRef) CFDictionaryGetValue(proxyDict, portKey))){
		if(CFGetTypeID(portNum) != CFNumberGetTypeID()){
			MA_LOG(logger, MA_LOG_SEV_ERROR, "CFGetTypeID(portNum) == CFNumberGetTypeID()");
			return MA_ERROR_NETWORK_GET_PROXY_FAILED ;
		}
	}else{
		MA_LOG(logger, MA_LOG_SEV_ERROR, "CFDictionaryGetValue for port number failed");
		return MA_ERROR_NETWORK_GET_PROXY_FAILED ;
	}
	if(!CFNumberGetValue(portNum, kCFNumberIntType, &port)){
		MA_LOG(logger, MA_LOG_SEV_ERROR, "CFNumberGetValue(portNum, kCFNumberIntType, &port) failed");
		return MA_ERROR_NETWORK_GET_PROXY_FAILED ;
	}

    ret = ma_proxy_create(&proxy) ;
	if(proxy) {
		ma_proxy_set_address(proxy, host);
		ma_proxy_set_port(proxy, port);
		/*ftp proxy*/
		if(!strncmp(url, "ftp:", strlen("ftp:") )) {
			ma_proxy_set_protocol_type(proxy, MA_PROXY_TYPE_FTP);
			if(proxy_config_from->system_proxy_auth && proxy_config_from->system_proxy_auth->ftp_auth_rqrd)
				(void)ma_proxy_set_authentication(proxy, proxy_config_from->system_proxy_auth->ftp_auth_rqrd, proxy_config_from->system_proxy_auth->ftp_user_name, proxy_config_from->system_proxy_auth->ftp_user_passwd);							
		}
		else{ /*http proxy*/
			ma_proxy_set_protocol_type(proxy, MA_PROXY_TYPE_HTTP);
			if(proxy_config_from->system_proxy_auth && proxy_config_from->system_proxy_auth->http_auth_rqrd)
				(void)ma_proxy_set_authentication(proxy, proxy_config_from->system_proxy_auth->http_auth_rqrd, proxy_config_from->system_proxy_auth->http_user_name, proxy_config_from->system_proxy_auth->http_user_passwd);							
		}
		ma_proxy_list_add_proxy(ProxyList, proxy);		
		ma_proxy_release(proxy);
		MA_LOG(logger, MA_LOG_SEV_DEBUG, "setting proxy addr<%s:%d>", host, port);
	}           
    return ret ;
}


ma_error_t getProxyFromPACconfiguration(ma_proxy_config_t *proxy_config_from, ma_logger_t *logger, const char *url, CFDictionaryRef proxyDict, ma_proxy_list_t *ProxyList) {

    CFStringRef	enableKey = kSCPropNetProxiesProxyAutoConfigEnable ;
	CFStringRef	PACKey = kSCPropNetProxiesProxyAutoConfigURLString ;
    CFNumberRef enableNum ;
    int enable ;
    CFStringRef scriptloc ;
    char *script_path = NULL ;
    ma_error_t ret = MA_ERROR_NETWORK_GET_PROXY_FAILED ;
    
    if( (NULL != (enableNum = (CFNumberRef)CFDictionaryGetValue(proxyDict, enableKey)) && CFGetTypeID(enableNum) == CFNumberGetTypeID()
        && CFNumberGetValue(enableNum, kCFNumberIntType, &enable) && (enable != 0) ) // enable flag 
        //PAC file url/path
        && (NULL != (scriptloc = (CFStringRef)CFDictionaryGetValue(proxyDict, PACKey)) && CFGetTypeID(scriptloc) == CFStringGetTypeID()
        && CFStringToCString(scriptloc, &script_path) )
      ) {
            ret = getProxiesForURLViaPAC(proxy_config_from, logger, url, script_path, ProxyList) ;
    }else{
		MA_LOG(logger, MA_LOG_SEV_DEBUG, "getProxyFromPACconfiguration failed");
	}

    return ret ;
}



ma_error_t getProxiesForURLViaPAC(ma_proxy_config_t *proxy_config_from, ma_logger_t *logger, const char *url, const char *script_path, ma_proxy_list_t *ProxyList) {
    const char* prefix_http = "http://";		
	const char* prefix_filelocal="file://localhost";
	const char* prefix_file="file:";

	char *path = NULL;	

	if( strstr(script_path, prefix_http) ) /* It is a http location for the PAC file */
		return proxiesForURLUsingScriptURL(proxy_config_from, logger, url, script_path, ProxyList) ;
	else if(strstr(script_path, prefix_filelocal)) { /*file prefix */
		path = script_path + strlen(prefix_filelocal);
		return proxiesForURLUsingScript(proxy_config_from, logger, url, path, ProxyList) ;
	}
	else if( strstr(script_path, prefix_file)) {
		path = script_path + strlen(prefix_file);
		return proxiesForURLUsingScript(proxy_config_from, logger, url, path, ProxyList) ;
	}
	else /*No file prefix then it is a simple path*/
		return proxiesForURLUsingScript(proxy_config_from, logger, url, script_path, ProxyList) ;
}

void ResultCallback(void * client, CFArrayRef proxies, CFErrorRef error) {
	CFTypeRef *resultPtr;

	if( (proxies != NULL) != (error == NULL) )
		return;

	resultPtr = (CFTypeRef *)client;
	if( resultPtr == NULL)
		return;
	if(*resultPtr != NULL)
		return;

	if (error != NULL) {
		*resultPtr = CFRetain(error);
	} else {
		*resultPtr = CFRetain(proxies);
	}
	CFRunLoopStop(CFRunLoopGetCurrent()) ;
}



ma_error_t proxiesForURLUsingScriptURL(ma_proxy_config_t *proxy_config_from, ma_logger_t *logger, const char *url, const char *path, ma_proxy_list_t *ProxyList) {
    CFURLRef cfurl = NULL;
    CFURLRef cfscript_path = NULL ;
    CFTypeRef result = NULL ;
    CFRunLoopSourceRef runloop = NULL ;
    OSStatus err = noErr ;

    if(NULL != (cfurl = CFURLCreateWithBytes(NULL, (const UInt8 *) url, strlen(url), kCFStringEncodingUTF8, NULL))
       && (NULL != (cfscript_path = CFURLCreateWithBytes(NULL, (const UInt8 *) path, strlen(path), kCFStringEncodingUTF8, NULL))) ) {
         CFStreamClientContext	context = { 0, &result, NULL, NULL, NULL } ;
         CFNetworkCopyProxiesForURL(cfurl, NULL) ;
         if(runloop = CFNetworkExecuteProxyAutoConfigurationURL(cfscript_path, cfurl, ResultCallback, &context)) {
             CFRunLoopAddSource(CFRunLoopGetCurrent(), runloop, kPrivateRunLoopMode) ;
             CFRunLoopRunInMode(kPrivateRunLoopMode, 1.0e10, false) ;
             CFRunLoopRemoveSource(CFRunLoopGetCurrent(), runloop, kPrivateRunLoopMode) ;
             if(result != NULL && CFGetTypeID(result) == CFErrorGetTypeID()) {
                 if(CFEqual(CFErrorGetDomain( (CFErrorRef) result ), kCFErrorDomainOSStatus) ) {
                     err = CFErrorGetCode((CFErrorRef) result ) ;
	         }
		else {
		    err = coreFoundationUnknownErr ;
	        }
             }
             else if ( CFGetTypeID(result) == CFArrayGetTypeID() ) {
                 process_proxy(proxy_config_from, logger, url, (CFArrayRef)result, ProxyList) ;
             }
             else
                 err = kernelTimeoutErr ;
         }
    }
    else
        err = coreFoundationUnknownErr ;
    CFQRelease(result) ;
    CFQRelease(runloop) ;
    CFQRelease(cfurl) ;
    CFQRelease(cfscript_path) ;

    if(err == noErr){
		MA_LOG(logger, MA_LOG_SEV_DEBUG, "proxiesForURLUsingScriptURL succeeded");
		return MA_OK;
	}else{
		MA_LOG(logger, MA_LOG_SEV_ERROR, "proxiesForURLUsingScriptURL failed with err <%d>", err);
		return MA_ERROR_NETWORK_GET_PROXY_FAILED ;
	}
}

ma_error_t proxiesForURLUsingScript(ma_proxy_config_t *proxy_config_from, ma_logger_t *logger, const char *url, const char *path, ma_proxy_list_t *ProxyList) {
    CFURLRef cfurl = NULL;
    CFURLRef cfscript_path = NULL ;
    CFDataRef cfscriptData = NULL ;
    SInt32  readErr ;
    CFStringRef	cfscriptStr = NULL ;
    CFArrayRef proxies = NULL ;
    ma_error_t ret = MA_OK ;

    if(  NULL != (cfurl = CFURLCreateWithBytes(NULL, (const UInt8 *) url, strlen(url), kCFStringEncodingUTF8, NULL) )
      && (NULL != (cfscript_path = CFURLCreateWithBytes(NULL, (const UInt8 *) path, strlen(path), kCFStringEncodingUTF8, NULL)))
      && CFURLCreateDataAndPropertiesFromResource(NULL, cfscript_path, &cfscriptData, NULL, NULL, &readErr)
      && (NULL != (cfscriptStr = CFStringCreateWithBytes(NULL, CFDataGetBytePtr(cfscriptData), CFDataGetLength(cfscriptData), kCFStringEncodingUTF8, true))) ) {
        CFErrorRef err2 = NULL ;		
	CFNetworkCopyProxiesForURL(cfurl, NULL) ;
	if(NULL != (proxies = CFNetworkCopyProxiesForAutoConfigurationScript(cfscriptStr, cfurl, &err2)) )
            process_proxy(proxy_config_from, logger, url, proxies, ProxyList) ;
        else{
			MA_LOG(logger, MA_LOG_SEV_DEBUG, "CFNetworkCopyProxiesForAutoConfigurationScript return NULL");
            ret = MA_ERROR_NETWORK_GET_PROXY_FAILED ;
		}
    }
    else{
		MA_LOG(logger, MA_LOG_SEV_DEBUG, "system proxy detect failed");
        ret = MA_ERROR_NETWORK_GET_PROXY_FAILED ;
	}

    CFQRelease(cfurl) ;
    CFQRelease(cfscript_path) ;
    CFQRelease(cfscriptData) ;
    CFQRelease(cfscriptStr) ;
    CFQRelease(proxies) ;

    return ret ;
}


ma_error_t process_proxy(ma_proxy_config_t *proxy_config_from, ma_logger_t *logger, const char *url, CFArrayRef proxies, ma_proxy_list_t *ProxyList) {
    CFIndex	proxyCount = 0 ;
	CFIndex	proxyIndex = 0;
    ma_proxy_t *proxy = NULL ;
    
    //CFShow(proxies) ;
    proxyCount = CFArrayGetCount(proxies) ;
    if( 0 >= proxyCount) {
		MA_LOG(logger, MA_LOG_SEV_DEBUG, "system proxy count is zero");
        return MA_OK ;
    }
    else {        
        for(proxyIndex = 0; proxyIndex < proxyCount; proxyIndex++) {
            CFDictionaryRef		thisProxy;		
            thisProxy = (CFDictionaryRef) CFArrayGetValueAtIndex(proxies, proxyIndex);
            if(thisProxy != NULL && CFGetTypeID(thisProxy) == CFDictionaryGetTypeID()) {
                extractProxyAtIndex(proxy_config_from, logger, url, proxyIndex, thisProxy, &proxy) ;
                if (proxy){
                    ma_proxy_list_add_proxy(ProxyList, proxy) ; 
					ma_proxy_release(proxy);
                }
	    }
        }
    }
    return MA_OK ;
}

void getProxyFieldFromKey(CFMutableDictionaryRef proxyMutable, const CFStringRef key,const CFTypeID expectedTypeID,const char *name, char  **keyValue)	
{
    /*Check if key is there*/
    if (CFDictionaryContainsKey(proxyMutable, key)) {
	CFTypeRef value= NULL ;		
	CFStringRef	desc= NULL ;
	
	/* Get the value */
	if((value = CFDictionaryGetValue(proxyMutable, key)) &&  CFGetTypeID(value) == expectedTypeID) {
	    /* Convert the value in the correct format */
	    if( (CFGetTypeID(value) == CFStringGetTypeID()) || (CFGetTypeID(value) == CFNumberGetTypeID()) || (CFGetTypeID(value) == CFURLGetTypeID()) ) {
	        desc = CFStringCreateWithFormat(NULL, NULL, CFSTR("%@"), value) ;
            }
	    else {
	        desc = CFCopyDescription(value) ;
	    }
	}			
	if(desc) {		
            CFStringToCString(desc, keyValue) ;
            CFQRelease(desc) ;
	}
    }
    /* Remove the key from the proxyMutable */
    CFDictionaryRemoveValue(proxyMutable, key);
}


void extractProxyAtIndex(ma_proxy_config_t *proxy_config_from, ma_logger_t *logger, const char *url, CFIndex index, CFDictionaryRef dic_proxy, ma_proxy_t **proxy) {
    CFMutableDictionaryRef	proxyMutable ;
    CFStringRef proxyType ;
    char *hvalue = NULL ;
    char *pvalue = NULL ;

    if( (proxyMutable = CFDictionaryCreateMutableCopy(NULL, CFDictionaryGetCount(dic_proxy), dic_proxy))
        && (proxyType = (CFStringRef) CFDictionaryGetValue(proxyMutable, (kCFProxyTypeKey)))
        && (CFGetTypeID(proxyType) == CFStringGetTypeID()) ) {
            if( (!strncmp(url, "http://", strlen("http://")) && CFEqual(proxyType, kCFProxyTypeHTTP))
                || (!strncmp(url, "https://", strlen("https://")) && CFEqual(proxyType, kCFProxyTypeHTTPS))
                || (!strncmp(url, "ftp://", strlen("ftp://")) && CFEqual(proxyType, kCFProxyTypeFTP))  ) {
                    getProxyFieldFromKey(proxyMutable, kCFProxyHostNameKey, CFStringGetTypeID(), "host", &hvalue) ;
                    getProxyFieldFromKey(proxyMutable, kCFProxyPortNumberKey,  CFNumberGetTypeID(), "port", &pvalue) ;
                    if(hvalue && pvalue) {
						ma_proxy_create(proxy);
						if(*proxy){
							ma_proxy_set_address((*proxy), hvalue);
							ma_proxy_set_port((*proxy), (atoi(pvalue)));
							MA_LOG(logger, MA_LOG_SEV_DEBUG, "setting proxy addr<%s:%s>", hvalue, pvalue);

							if(!strncmp(url, "ftp:", strlen("ftp:") )) {
								ma_proxy_set_protocol_type((*proxy), MA_PROXY_TYPE_FTP);
								if(proxy_config_from->system_proxy_auth && proxy_config_from->system_proxy_auth->ftp_auth_rqrd)
									(void)ma_proxy_set_authentication((*proxy), proxy_config_from->system_proxy_auth->ftp_auth_rqrd, proxy_config_from->system_proxy_auth->ftp_user_name, proxy_config_from->system_proxy_auth->ftp_user_passwd);							
							}
							else{ /*http proxy*/
								ma_proxy_set_protocol_type((*proxy), MA_PROXY_TYPE_HTTP);
								if(proxy_config_from->system_proxy_auth && proxy_config_from->system_proxy_auth->http_auth_rqrd)
									(void)ma_proxy_set_authentication((*proxy), proxy_config_from->system_proxy_auth->http_auth_rqrd, proxy_config_from->system_proxy_auth->http_user_name, proxy_config_from->system_proxy_auth->http_user_passwd);							
							}
						}
						SAFE_FREE(hvalue) ; SAFE_FREE(pvalue) ;
                    }else{
						MA_LOG(logger, MA_LOG_SEV_DEBUG, "getProxyFromPACconfiguration failed to get host name & port for url <%s>", url);
					}
            }else{
				MA_LOG(logger, MA_LOG_SEV_DEBUG, "getProxyFromPACconfiguration condition failed for url <%s>", url);
			}
    }else{
		MA_LOG(logger, MA_LOG_SEV_DEBUG, "extractProxyAtIndex failed");
	}

    CFDictionaryRemoveValue(proxyMutable, kCFProxyTypeKey) ;
    //if(CFDictionaryGetCount(proxyMutable))
     //   CFShow(proxyMutable) ;

    CFQRelease(proxyMutable) ;
}

static Boolean CFStringToCString(const CFStringRef str,char  **out)
{
    char *sCStr = NULL;
    size_t	cStrSize;

    if(str == NULL)
        return FALSE;

    cStrSize = CFStringGetMaximumSizeForEncoding(CFStringGetLength(str), kCFStringEncodingUTF8);
    if(sCStr =(char*)calloc(cStrSize+1, sizeof(char))) {
        if(CFStringGetCString(str, sCStr, cStrSize, kCFStringEncodingUTF8)) {
	    *out = sCStr ;
            return TRUE ;
	}
    }
    SAFE_FREE(sCStr) ;
    return FALSE ;		
}
