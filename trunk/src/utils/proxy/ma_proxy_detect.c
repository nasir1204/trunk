#include "ma/ma_common.h"
#include "ma/internal/utils/proxy/ma_proxy_internal.h"
#include "ma/internal/utils/proxy/ma_proxy_config_internal.h"

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "proxy_detect"

static ma_error_t proxy_config_detect_system_proxy(ma_proxy_config_t *proxy_config_from, ma_logger_t *logger, const char *url, ma_proxy_list_t *ProxyList);


#ifdef MA_WINDOWS
#include "ma_proxy_detect_win.c"
#elif MACX
#include "ma_proxy_detect_mac.c"
#else
ma_error_t proxy_config_detect_system_proxy(ma_proxy_config_t *proxy_config_from, ma_logger_t *logger, const char *url, ma_proxy_list_t *ProxyList){
    return MA_ERROR_NOT_SUPPORTED ;
}
#endif 

static void fillup_alternate_proxy(ma_proxy_config_t *proxy_config_from, ma_logger_t *logger, const char *url, ma_proxy_list_t *new_list){
	if(proxy_config_from && new_list){
		size_t size = 0;
		(void)ma_proxy_list_size(proxy_config_from->proxy_list, &size);
		if(size){
			size_t i = 0;
			for(; i < size; i++){
				ma_proxy_t *proxy = NULL;
				if(MA_OK == ma_proxy_list_get_proxy(proxy_config_from->proxy_list, i, &proxy)){

					ma_proxy_protocol_type_t type = MA_PROXY_TYPE_END;
					(void)ma_proxy_get_protocol_type(proxy, &type);
					if(!strncmp(url, "ftp:", strlen("ftp:") )) {
						if(type == MA_PROXY_TYPE_BOTH || type == MA_PROXY_TYPE_FTP){
							MA_LOG(logger, MA_LOG_SEV_DEBUG, "adding ftp alternate proxy");
							(void)ma_proxy_list_add_proxy(new_list, proxy);		
						}
					}
					else if(!strncmp(url, "http", strlen("http") )) {
						if(type == MA_PROXY_TYPE_BOTH || type == MA_PROXY_TYPE_HTTP){
							MA_LOG(logger, MA_LOG_SEV_DEBUG, "adding http alternate proxy");
							(void)ma_proxy_list_add_proxy(new_list, proxy);		
						}
					}
					(void)ma_proxy_release(proxy); proxy = NULL;					
				}
			}
		}
		else
			MA_LOG(logger, MA_LOG_SEV_DEBUG, "no alternate proxy set by the user.");
	}	
}

ma_error_t ma_proxy_config_detect_system_proxy(ma_proxy_config_t *proxy_config_from, ma_logger_t *logger, const char *url, ma_proxy_list_t **proxy_list){
	if(proxy_config_from && url && proxy_list){
		ma_error_t rc = MA_OK;
		ma_proxy_list_t *list = NULL;
		size_t size = 0;
		if(MA_OK == (rc = ma_proxy_list_create(&list))){
			if(MA_OK != (rc = proxy_config_detect_system_proxy(proxy_config_from, logger, url, list)))
				MA_LOG(logger, MA_LOG_SEV_INFO, "no system proxy, proxy detect returns rc <%d>", rc);

			fillup_alternate_proxy(proxy_config_from, logger, url, list);

			(void)ma_proxy_list_size(list, &size);
			if(size){
				*proxy_list = list;
				return MA_OK;
			}
			rc = MA_ERROR_OBJECTNOTFOUND;			
			(void)ma_proxy_list_release(list);				
		}
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}



