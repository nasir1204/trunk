#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include "ma/internal/utils/text/ma_utf8.h"
#include "ma/internal/utils/network/ma_net_common.h"
#include "ma/ma_log.h"
#include "ma/internal/utils/proxy/ma_proxy_internal.h"

#include <windows.h>
#include <WinHttp.h>
#include <Wtsapi32.h>
#include <Strsafe.h>

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "proxy_detect"

#define BUFFER_SIZE 4096
#define SAFECLOSEHANDLE(x) {if (x){CloseHandle(x);x= NULL;}}
#define SAFE_FREE(x) {if(x){free(x);x= NULL;}}

static ma_error_t get_proxy_for_url(ma_logger_t *logger, HINTERNET hISession, wchar_t *urlBuffer, WINHTTP_AUTOPROXY_OPTIONS *proxyOptions, WINHTTP_PROXY_INFO *proxyInfo ) ;
static ma_error_t process_detected_proxy(ma_proxy_config_t *proxy_config_from, ma_logger_t *logger, const char *target_url, wchar_t *url, wchar_t *plist, ma_proxy_list_t *proxy_list);
static ma_error_t get_user_proxy_config_for_user(ma_logger_t *logger, WINHTTP_CURRENT_USER_IE_PROXY_CONFIG *) ;

ma_error_t proxy_config_detect_system_proxy(ma_proxy_config_t *proxy_config_from, ma_logger_t *logger,const char *url, ma_proxy_list_t *ProxyList) {
    if(url && ProxyList) {
        ma_error_t ret = MA_OK ;
        wchar_t *urlBuffer ;
        ma_temp_buffer_t buffer = {0} ;
        BOOL winApiResult = TRUE ;

        WINHTTP_CURRENT_USER_IE_PROXY_CONFIG proxyConfig ;
        HINTERNET hISession ;
        WINHTTP_PROXY_INFO proxyInfo ;
        WINHTTP_AUTOPROXY_OPTIONS proxyOptions ;

        ma_temp_buffer_init(&buffer) ;
        ma_convert_utf8_to_wide_char(url, &buffer) ;
        urlBuffer = (wchar_t *)ma_temp_buffer_get(&buffer) ;

        proxyOptions.lpvReserved = NULL ;	
	    proxyOptions.dwReserved = 0 ;
	    proxyOptions.fAutoLogonIfChallenged	= TRUE ;
		MA_LOG(logger, MA_LOG_SEV_DEBUG, "system proxy detect started for url <%s>", url);
        if(MA_OK != (ret = get_user_proxy_config_for_user(logger, &proxyConfig))) {
            ret = MA_ERROR_NETWORK_GET_PROXY_FAILED ;
            if(TRUE == (winApiResult = WinHttpGetDefaultProxyConfiguration(&proxyInfo))) {                
                if (proxyInfo.lpszProxy ) {
        		    (void)process_detected_proxy(proxy_config_from, logger, url, urlBuffer, proxyInfo.lpszProxy, ProxyList) ;
		            GlobalFree(proxyInfo.lpszProxy) ;
	            }
                ret = MA_OK ;
            }
            ma_temp_buffer_uninit(&buffer) ;
			MA_LOG(logger, MA_LOG_SEV_DEBUG, "system proxy detect returning with rc <%d>", ret);
            return ret ;
        }

        if(MA_OK == ret && !proxyConfig.fAutoDetect && !(proxyConfig.lpszAutoConfigUrl) && !(proxyConfig.lpszProxy) ){            
            ma_temp_buffer_uninit(&buffer) ;
			MA_LOG(logger, MA_LOG_SEV_DEBUG, "condition for proxy detection passed");
            return MA_OK ;
        }

        hISession = WinHttpOpen(MA_HTTP_POST_USER_AGENT_WSTR, WINHTTP_ACCESS_TYPE_DEFAULT_PROXY, WINHTTP_NO_PROXY_NAME, WINHTTP_NO_PROXY_BYPASS, 0 ) ;
        if(!hISession) {
            ma_temp_buffer_uninit(&buffer) ;
			MA_LOG(logger, MA_LOG_SEV_ERROR, "system get proxy(WinHttpOpen) failed");
            return MA_ERROR_NETWORK_GET_PROXY_FAILED ;
        }

        if(proxyConfig.fAutoDetect) {
            proxyOptions.dwFlags = WINHTTP_AUTOPROXY_RUN_INPROCESS | WINHTTP_AUTOPROXY_AUTO_DETECT ;
            proxyOptions.dwAutoDetectFlags = WINHTTP_AUTO_DETECT_TYPE_DHCP | WINHTTP_AUTO_DETECT_TYPE_DNS_A ;
            proxyOptions.lpszAutoConfigUrl = NULL ;
            if(MA_OK == (ret = get_proxy_for_url(logger, hISession, urlBuffer, &proxyOptions, &proxyInfo))) {
                ret = process_detected_proxy(proxy_config_from, logger, url, urlBuffer, proxyInfo.lpszProxy, ProxyList) ;
                GlobalFree(proxyInfo.lpszProxy) ;
            }
        }
        if((proxyConfig.fAutoDetect && MA_OK != ret && proxyConfig.lpszAutoConfigUrl)
            || (!proxyConfig.fAutoDetect && MA_OK == ret && proxyConfig.lpszAutoConfigUrl)) {
            proxyOptions.dwFlags = WINHTTP_AUTOPROXY_RUN_INPROCESS | WINHTTP_AUTOPROXY_CONFIG_URL ;
            proxyOptions.dwAutoDetectFlags = 0 ;
            proxyOptions.lpszAutoConfigUrl = proxyConfig.lpszAutoConfigUrl ;
            if(MA_OK == (ret = get_proxy_for_url(logger, hISession, urlBuffer, &proxyOptions, &proxyInfo))) {
                ret = process_detected_proxy(proxy_config_from, logger, url, urlBuffer, proxyInfo.lpszProxy, ProxyList) ;
                GlobalFree(proxyInfo.lpszProxy) ;
            }
        }
        if((MA_OK != ret && proxyConfig.lpszProxy)
            || (!proxyConfig.fAutoDetect && !proxyConfig.lpszAutoConfigUrl && proxyConfig.lpszProxy)) {
            ret = process_detected_proxy(proxy_config_from, logger, url, urlBuffer, proxyConfig.lpszProxy, ProxyList) ;
        }
        WinHttpCloseHandle(hISession) ;
        ma_temp_buffer_uninit(&buffer) ;
		MA_LOG(logger, MA_LOG_SEV_DEBUG, "system proxy detect returning with rc <%d>", ret);
        return ret ;
    }
	MA_LOG(logger, MA_LOG_SEV_ERROR, "Invalid arguments");
    return MA_ERROR_INVALIDARG ;
}

static ma_error_t get_proxy_for_url(ma_logger_t *logger, HINTERNET hISession, wchar_t *urlBuffer, WINHTTP_AUTOPROXY_OPTIONS *proxyOptions, WINHTTP_PROXY_INFO *proxyInfo ) {
    if(WinHttpGetProxyForUrl(hISession, urlBuffer, proxyOptions, proxyInfo)) {
        if(proxyInfo->dwAccessType == WINHTTP_ACCESS_TYPE_NO_PROXY){
			MA_LOG(logger, MA_LOG_SEV_ERROR, "WINHTTP_ACCESS_TYPE_NO_PROXY");
            return MA_ERROR_NETWORK_NO_PROXY_DEFINED ;
		}
        return MA_OK ;
    }
    else {
		MA_LOG(logger, MA_LOG_SEV_ERROR, "WinHttpGetProxyForUrl failed with windows err <%d>", GetLastError());
        return MA_ERROR_NETWORK_GET_PROXY_FAILED ;
    }
}

static DWORD active_usr_sessionid_get(ma_logger_t *logger){
	PWTS_SESSION_INFO pSessionInfo = NULL ;
	DWORD size = 0;
	DWORD  sessionid = 0 ;

	if(WTSEnumerateSessions( WTS_CURRENT_SERVER_HANDLE, 0,	1, &pSessionInfo, &size)) {
		DWORD i;
		for (i=0; i < size; i++) {
            //MA_LOG(logger, MA_LOG_SEV_TRACE, "session id = %d, state = %d", pSessionInfo[i].SessionId, pSessionInfo[i].State) ;

			if(pSessionInfo[i].State == WTSActive){
				sessionid = pSessionInfo[i].SessionId;
                break ;    
			}
		}					
		WTSFreeMemory(pSessionInfo);
	}
    else {
        MA_LOG(logger, MA_LOG_SEV_ERROR, "WTSEnumerateSessions failed %d", GetLastError()) ;
    }
	return sessionid;
}

static ma_error_t get_user_proxy_config_for_user(ma_logger_t *logger, WINHTTP_CURRENT_USER_IE_PROXY_CONFIG *proxyConfig) {
    if(proxyConfig) {
        BOOL winApiRet = TRUE ;
        ma_error_t ret = MA_ERROR_NETWORK_GET_PROXY_FAILED ;
        HANDLE current_process = NULL, cp_token = NULL, user_token = NULL ;
        TOKEN_PRIVILEGES tokenPrivs ;
        unsigned long session_id = 0 ;

        tokenPrivs.PrivilegeCount = 1 ;
        tokenPrivs.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED ;

        /*if(0 != (session_id = active_usr_sessionid_get(logger))) {*/
            if(NULL != (current_process = GetCurrentProcess() )){
				if(OpenProcessToken(current_process, TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &cp_token)){
					if(LookupPrivilegeValue(NULL, SE_TCB_NAME, &tokenPrivs.Privileges[0].Luid)){
						if(AdjustTokenPrivileges(cp_token, FALSE, &tokenPrivs, 0, (PTOKEN_PRIVILEGES)NULL, 0)){
                            /* just trying with different options of getting session id as the behaviour was different for different windows versions */
							if( (WTSQueryUserToken(session_id=active_usr_sessionid_get(logger), &user_token) && NULL != user_token)
                                || (WTSQueryUserToken(session_id=WTSGetActiveConsoleSessionId(), &user_token) && NULL != user_token)
                                || (ProcessIdToSessionId(GetCurrentProcessId(), &session_id) && WTSQueryUserToken(WTSGetActiveConsoleSessionId(), &user_token) && NULL != user_token)   )
                            {
								RevertToSelf() ;
								if(ImpersonateLoggedOnUser(user_token) && WinHttpGetIEProxyConfigForCurrentUser(proxyConfig))
									ret = MA_OK ;
								else
									MA_LOG(logger, MA_LOG_SEV_ERROR, "ImpersonateLoggedOnUser failed err <%d>", GetLastError());
								RevertToSelf() ;
							}
							else{
								MA_LOG(logger, MA_LOG_SEV_ERROR, "for session_id <%lu> , WTSQueryUserToken() failed or user_token return as NULL err <%d>", session_id, GetLastError());
							}
						}
						else{
							MA_LOG(logger, MA_LOG_SEV_ERROR, "AdjustTokenPrivileges() failed err <%d>", GetLastError());
						}
					}
					else{
						MA_LOG(logger, MA_LOG_SEV_ERROR, "LookupPrivilegeValue failed err <%d>", GetLastError());
					}
				}
				else{
					MA_LOG(logger, MA_LOG_SEV_ERROR, "OpenProcessToken failed err <%d>", GetLastError());
				}
			}
			else{
				MA_LOG(logger, MA_LOG_SEV_ERROR, "GetCurrentProcess failed err <%d>", GetLastError());
			}
        //}
		/*else{
			MA_LOG(logger, MA_LOG_SEV_ERROR, "active usr sessionid get failed");
		}*/
        SAFECLOSEHANDLE(current_process) ;
        SAFECLOSEHANDLE(cp_token) ;
        SAFECLOSEHANDLE(user_token) ;
        return ret ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t process_detected_proxy(ma_proxy_config_t *proxy_config_from, ma_logger_t *logger, const char *target_url, wchar_t *url, wchar_t *plist, ma_proxy_list_t *proxy_list) {
    if(plist && 0 < wcslen(plist) && proxy_list) {                
        const wchar_t *delimiters = L",; \t\n\r" ;
        wchar_t *token = NULL ;
        wchar_t *p ;
        size_t len = 0, last=0 ;
        ma_temp_buffer_t buffer = {0} ;
        char *proxy_buffer = NULL, *cp = NULL  ;  
        token = wcstok( plist, delimiters ) ;
        
        while(token) {
            len = wcsspn(url, L"://") ;
            if(0 == wcsncmp(url, token, len)) {
				char *host = NULL;
				size_t port = 0;
                /*len = wcsspn(token, L"=") ; ++len ;p = token + len ;*/
				if(p = wcsstr(token, L"="))
					++p;
				else
					p = token;

				/*if(!p)
					return MA_ERROR_INVALIDARG;*/

                ma_temp_buffer_init(&buffer) ;
                ma_convert_wide_char_to_utf8(p, &buffer) ;
                proxy_buffer = (char *)ma_temp_buffer_get(&buffer) ;
                cp = strrchr(proxy_buffer, ':') ;
                len = cp - proxy_buffer ;

                host = (char *)calloc(1, len+1) ;
                if(host) {
					ma_proxy_t *proxy = NULL ;
					strncpy(host, proxy_buffer, len) ;
                    port = atoi(++cp) ;

					ma_proxy_create(&proxy);
					if(proxy){					
						ma_proxy_set_address(proxy, host);
						ma_proxy_set_port(proxy, port);

						/*ftp proxy*/
						if(!strncmp(target_url, "ftp:", strlen("ftp:") )) {
							ma_proxy_set_protocol_type(proxy, MA_PROXY_TYPE_FTP);
							if(proxy_config_from->system_proxy_auth && proxy_config_from->system_proxy_auth->ftp_auth_rqrd)
								(void)ma_proxy_set_authentication(proxy, proxy_config_from->system_proxy_auth->ftp_auth_rqrd, proxy_config_from->system_proxy_auth->ftp_user_name, proxy_config_from->system_proxy_auth->ftp_user_passwd);							
						}
						else{ /*http proxy*/
							ma_proxy_set_protocol_type(proxy, MA_PROXY_TYPE_HTTP);
							if(proxy_config_from->system_proxy_auth && proxy_config_from->system_proxy_auth->http_auth_rqrd)
								(void)ma_proxy_set_authentication(proxy, proxy_config_from->system_proxy_auth->http_auth_rqrd, proxy_config_from->system_proxy_auth->http_user_name, proxy_config_from->system_proxy_auth->http_user_passwd);							
						}

						ma_proxy_list_add_proxy(proxy_list, proxy) ;
						ma_proxy_release(proxy);
						MA_LOG(logger, MA_LOG_SEV_DEBUG, "setting proxy addr<%s:%d>", host, port);
					}
                    free(host);					
				}                   				
            }
            token = wcstok( NULL, delimiters ) ;
        }
        return MA_OK ;
    }    
    return MA_ERROR_INVALIDARG ;
}


