#include "ma/internal/utils/database/ma_db.h"
#include "ma/internal/utils/macrypto/ma_crypto.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/utils/proxy/ma_proxy_internal.h"
#include "ma/internal/utils/proxy/ma_proxy_db_manager.h"
#include "ma/internal/utils/proxy/ma_proxy_config_internal.h"

static ma_error_t decrypt_proxy_passwords(ma_crypto_t *crypto, ma_proxy_config_t *config) {
	size_t num_proxies = 0, iter = 0 ;
	ma_error_t rc = MA_OK ;

	(void)ma_proxy_list_size(config->proxy_list, &num_proxies) ;

	if(num_proxies) {
		ma_proxy_list_t *p_list = NULL ;

		if(MA_OK == (rc = ma_proxy_list_create(&p_list) )) {
			for(iter = 0; iter < num_proxies ; iter++) {
				ma_crypto_decryption_t *decrypt = NULL ;
				ma_proxy_t *proxy = NULL ;

				if(MA_OK == ma_proxy_list_get_proxy(config->proxy_list, iter, &proxy)) {
					if(proxy->is_auth_rqued && proxy->user_passwd) {
						unsigned char *passwd_str = NULL ;
						char *user = strdup(proxy->user_name) ;

						if(MA_OK == (rc = ma_crypto_decrypt_create(crypto, MA_CRYPTO_DECRYPTION_3DES, &decrypt))) {
							ma_bytebuffer_t *decrypted_buffer = NULL ;

							if(MA_OK == ma_bytebuffer_create(strlen(proxy->user_passwd) , &decrypted_buffer)) {
								if(MA_OK == ma_crypto_decode_and_decrypt(decrypt, (const unsigned char *)proxy->user_passwd, strlen(proxy->user_passwd), decrypted_buffer)){
									passwd_str = (unsigned char*) calloc((ma_bytebuffer_get_size(decrypted_buffer) + 1), sizeof(unsigned char) );
									memcpy (passwd_str, ma_bytebuffer_get_bytes(decrypted_buffer), ma_bytebuffer_get_size(decrypted_buffer));
									*(passwd_str + ma_bytebuffer_get_size(decrypted_buffer)) ='\0';
								}
								(void)ma_bytebuffer_release(decrypted_buffer) ;
								(void)ma_crypto_decrypt_release(decrypt) ;
							}
						}
						(void)ma_proxy_set_authentication(proxy, proxy->is_auth_rqued, user, (const char *)passwd_str) ;
						free(user) ; user = NULL ;
						free(passwd_str) ;	passwd_str = NULL ;
					}
					(void)ma_proxy_list_add_proxy(p_list, proxy) ;
				}
			}
			(void)ma_proxy_config_set_proxy_list(config, p_list) ;
		}
	}

	if(config->system_proxy_auth->http_auth_rqrd && config->system_proxy_auth->http_user_passwd) {
		ma_crypto_decryption_t *decrypt = NULL ;
		unsigned char *passwd_str = NULL ;
		char *user = strdup(config->system_proxy_auth->http_user_name) ;

		if(MA_OK == (rc = ma_crypto_decrypt_create(crypto, MA_CRYPTO_DECRYPTION_3DES, &decrypt))) {
			ma_bytebuffer_t *decrypted_buffer = NULL ;

			if(MA_OK == ma_bytebuffer_create(strlen(config->system_proxy_auth->http_user_passwd) , &decrypted_buffer)) {
				if(MA_OK == ma_crypto_decode_and_decrypt(decrypt, (const unsigned char *)config->system_proxy_auth->http_user_passwd, strlen(config->system_proxy_auth->http_user_passwd), decrypted_buffer)){
					passwd_str = (unsigned char*) calloc((ma_bytebuffer_get_size(decrypted_buffer) + 1), sizeof(unsigned char) );
					memcpy (passwd_str, ma_bytebuffer_get_bytes(decrypted_buffer), ma_bytebuffer_get_size(decrypted_buffer));
					*(passwd_str + ma_bytebuffer_get_size(decrypted_buffer)) ='\0';
				}
				(void)ma_bytebuffer_release(decrypted_buffer) ;
				(void)ma_crypto_decrypt_release(decrypt) ;
			}
		}
		(void)ma_proxy_config_set_system_proxy_authentication(config, MA_PROXY_TYPE_HTTP, config->system_proxy_auth->http_auth_rqrd, user, (const char *)passwd_str) ;
		free(user) ;	user = NULL ;
		free(passwd_str) ;	passwd_str = NULL ;
	}
	if(config->system_proxy_auth->ftp_auth_rqrd && config->system_proxy_auth->ftp_user_passwd) {
		ma_crypto_decryption_t *decrypt = NULL ;
		unsigned char *passwd_str = NULL ;
		char *user = strdup(config->system_proxy_auth->ftp_user_name) ;

		if(MA_OK == (rc = ma_crypto_decrypt_create(crypto, MA_CRYPTO_DECRYPTION_3DES, &decrypt))) {
			ma_bytebuffer_t *decrypted_buffer = NULL ;

			if(MA_OK == ma_bytebuffer_create(strlen(config->system_proxy_auth->ftp_user_passwd) , &decrypted_buffer)) {
				if(MA_OK == ma_crypto_decode_and_decrypt(decrypt, (const unsigned char *)config->system_proxy_auth->ftp_user_passwd, strlen(config->system_proxy_auth->ftp_user_passwd), decrypted_buffer)){
					passwd_str = (unsigned char*) calloc((ma_bytebuffer_get_size(decrypted_buffer) + 1), sizeof(unsigned char) );
					memcpy (passwd_str, ma_bytebuffer_get_bytes(decrypted_buffer), ma_bytebuffer_get_size(decrypted_buffer));
					*(passwd_str + ma_bytebuffer_get_size(decrypted_buffer)) ='\0';
				}
				(void)ma_bytebuffer_release(decrypted_buffer) ;
				(void)ma_crypto_decrypt_release(decrypt) ;
			}
		}
		(void)ma_proxy_config_set_system_proxy_authentication(config, MA_PROXY_TYPE_FTP, config->system_proxy_auth->ftp_auth_rqrd, user, (const char *)passwd_str) ;
		free(user) ;	user = NULL ;
		free(passwd_str) ;	passwd_str = NULL ;
	}
	return rc ;
}

ma_error_t  ma_proxy_get_config(ma_context_t *context, ma_proxy_config_t **config) {
	if(context && config) {
		ma_error_t rc = MA_OK ;

		ma_db_t *db_handle = ma_configurator_get_database( MA_CONTEXT_GET_CONFIGURATOR(context)) ;
		ma_crypto_t *crypto = MA_CONTEXT_GET_CRYPTO(context) ;

		ma_proxy_config_t *config_tmp = NULL ;

		if(MA_OK == (rc = ma_proxy_db_get_proxy_config(db_handle, &config_tmp))) {
			ma_proxy_list_t *proxy_list_tmp = NULL ;
			size_t num_proxies = 0, iter = 0 ;

			if(config_tmp->proxy_usage_type == MA_PROXY_USAGE_TYPE_SERVER_CONFIGURED) {
				(void)ma_proxy_db_get_all_proxies(db_handle, MA_PROXY_FLAG_SERVER_CONFIGURED, &proxy_list_tmp) ;
			}
			else  if(config_tmp->proxy_usage_type == MA_PROXY_USAGE_TYPE_SYSTEM_CONFIGURED && config_tmp->allow_user_config) {
				(void)ma_proxy_db_get_all_proxies(db_handle, MA_PROXY_FLAG_LOCAL_CONFIGURED, &proxy_list_tmp) ;
			}
			if(proxy_list_tmp) {
				(void)ma_proxy_config_set_proxy_list(config_tmp, proxy_list_tmp) ;
			}
			(void)decrypt_proxy_passwords(crypto, config_tmp) ;

			*config = config_tmp ;
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}
