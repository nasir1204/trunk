#include "ma/proxy/ma_proxy.h"
#include "ma/internal/utils/proxy/ma_proxy_internal.h"
#include <stdlib.h>
#include <string.h>

ma_error_t ma_proxy_create(ma_proxy_t **proxy){
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	if(proxy){
		if(*proxy = (ma_proxy_t*)calloc(1, sizeof(ma_proxy_t)))
			rc = MA_OK;
	}
	return rc;
}

ma_error_t   ma_proxy_release(ma_proxy_t *proxy){
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	if(proxy){
		free(proxy->server);
		free(proxy->user_name);
		free(proxy->user_passwd);
		free(proxy);
		proxy = NULL;
		rc = MA_OK;
	}
	return rc;
}

/* This function embeds the address in bracket if its ipv6 and already not embedded */
static void format_ipv6_address(const char *address, char formated_addr[], size_t length){
	if(address[0] == '['){
		/*IPv6 and already embedded in bracket */
		strncpy(formated_addr, address, length-1);
		return;
	}

	if(strstr(address, ":")) {
		strncpy(formated_addr, "[", length-1);
		strcat(formated_addr, address);
		strcat(formated_addr, "]");
		return;
	}
	else{
		/* IPv4 address: Do nothing */
		strncpy(formated_addr, address, length-1);
		return;
	}		
}

ma_error_t ma_proxy_set_address(ma_proxy_t *proxy, const char *address){
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	if(proxy && address){
		char formated_addr[1024] = {0};
		free(proxy->server);
		format_ipv6_address(address, formated_addr, 1024);
		proxy->server = strdup(formated_addr);
		rc = MA_OK;
	}
	return rc;
}

ma_error_t ma_proxy_get_address(ma_proxy_t *proxy, const char **address){
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	if(proxy && address){
		*address = proxy->server;
		rc = MA_OK;
	}
	return rc;
}

ma_error_t ma_proxy_set_port(ma_proxy_t *proxy, ma_int32_t server_port){
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	if(proxy){
		proxy->server_port = server_port;
		rc = MA_OK;
	}
	return rc;
}

ma_error_t ma_proxy_get_port(ma_proxy_t *proxy, ma_int32_t *port){
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	if(proxy && port){
		*port = proxy->server_port;
		rc = MA_OK;
	}
	return rc;
}

ma_error_t ma_proxy_set_scopeid(ma_proxy_t *proxy, ma_uint32_t scopeid)
{
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	if(proxy){
		proxy->nic_scope_id = scopeid;
		rc = MA_OK;
	}
	return rc;
}
ma_error_t ma_proxy_get_scopeid(ma_proxy_t *proxy, ma_uint32_t *scopeid)
{
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	if(proxy && scopeid){
		*scopeid = proxy->nic_scope_id;
		rc = MA_OK;
	}
	return rc;
}

ma_error_t  ma_proxy_set_protocol_type(ma_proxy_t *proxy, ma_proxy_protocol_type_t type){
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	if(proxy){
		proxy->type = type;
		rc = MA_OK;
	}
	return rc;
}

ma_error_t  ma_proxy_get_protocol_type(ma_proxy_t *proxy, ma_proxy_protocol_type_t *type){
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	if(proxy && type){
		*type = proxy->type;
		rc = MA_OK;
	}
	return rc;
}

ma_error_t ma_proxy_set_authentication(ma_proxy_t *proxy, ma_bool_t auth_req, const char *user_name, const char *password){
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	if(proxy){
		if(auth_req){
			if(user_name && password){
				proxy->is_auth_rqued = auth_req;
				free(proxy->user_name);
				free(proxy->user_passwd);
				proxy->user_name = strdup(user_name);
				proxy->user_passwd = strdup(password);
				rc = MA_OK;
			}
		}
		else{
			proxy->is_auth_rqued = auth_req;
			free(proxy->user_name);
			free(proxy->user_passwd);
			proxy->user_name = NULL;
			proxy->user_passwd = NULL;
			rc = MA_OK;
		}
	}
	return rc;
}

ma_error_t ma_proxy_get_authentication(ma_proxy_t *proxy, ma_bool_t *auth_req, const char **user_name, const char **password){
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	if(proxy && auth_req && user_name && password){
		if(proxy->is_auth_rqued){
				*auth_req = proxy->is_auth_rqued;
				*user_name = proxy->user_name;
				*password = proxy->user_passwd;
				rc = MA_OK;
		}
		else{
				*auth_req = proxy->is_auth_rqued;
				*user_name = NULL;
				*password = NULL;
				rc = MA_OK;
			}
	}
	return rc;
}

 ma_error_t  ma_proxy_set_flag(ma_proxy_t *proxy, ma_int32_t flag){
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	if(proxy){
		proxy->flag = flag;
		rc = MA_OK;
	}
	return rc;
}

ma_error_t ma_proxy_get_flag(ma_proxy_t *proxy, ma_int32_t *flag){
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	if(proxy && flag){
		*flag = proxy->flag;
		rc = MA_OK;
	}
	return rc;
}


/*proxy list api */
ma_error_t ma_proxy_list_create(ma_proxy_list_t **list){
	if(list){
        ma_proxy_list_t *tmp = NULL ;
		if(NULL != (tmp = (ma_proxy_list_t*) calloc(1, sizeof(ma_proxy_list_t)))) {
            if(MA_OK == ma_array_create(&tmp->list)) {
				MA_ATOMIC_INCREMENT(tmp->ref_count);
                *list = tmp ;
			    return MA_OK;
            }
            else {
                free(tmp) ;
			}
		}
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_proxy_list_add_ref(ma_proxy_list_t *list){
	if(list) {
		MA_ATOMIC_INCREMENT(list->ref_count);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_proxy_list_add_proxy(ma_proxy_list_t *self, ma_proxy_t *proxy){
	ma_error_t    rc = MA_ERROR_INVALIDARG ;
	if(self && proxy){
		ma_variant_t *proxy_variant_value = NULL;
		if((proxy->server) && ( MA_OK == (rc = ma_proxy_as_variant(proxy, &proxy_variant_value)))){
			(void)ma_array_push(self->list, proxy_variant_value);
			(void)ma_variant_release(proxy_variant_value);
		}
	}
	return rc;
}

/*
ma_error_t ma_proxy_list_remove(ma_proxy_list_t *list, ma_proxy_t *proxy){
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	if(list && proxy){
		//TBD: How to remove from array
		//rc = ma_table_remove_entry(list->list, proxy->server);
	}
	return rc;
}
*/

ma_error_t ma_proxy_list_size(ma_proxy_list_t *self, size_t *size){
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	if(self && size){
		rc = ma_array_size(self->list, size);
	}
	return rc;
}

ma_error_t ma_proxy_list_get_proxy(ma_proxy_list_t *self, ma_int32_t index, ma_proxy_t **proxy){
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	if(self && proxy){
		ma_variant_t *table_value = NULL;
		if(MA_OK == (rc = ma_array_get_element_at(self->list, index+1, &table_value))){
			rc = ma_proxy_from_variant(table_value, proxy);
			(void)ma_variant_release(table_value); 
		}
	}
	return rc;
}

ma_error_t ma_proxy_list_set_last_proxy_index(ma_proxy_list_t *self, ma_int32_t index){
	if(self){
		self->last_used_proxy_index = index;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_proxy_list_get_last_proxy_index(ma_proxy_list_t *self, ma_int32_t *index){
	if(self && index){
		*index = self->last_used_proxy_index;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_proxy_list_release(ma_proxy_list_t *self){
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	if(self){
		rc = MA_OK;
		if(0 == MA_ATOMIC_DECREMENT(self->ref_count)) {
			if(self->list) 
				(void)ma_array_release(self->list) ;
			free(self) ; 
			self = NULL;		
		}
		return rc;
	}
	return rc;
}

