#include "ma/proxy/ma_proxy.h"
#include "ma/proxy/ma_proxy_config.h"
#include "ma/internal/utils/proxy/ma_proxy_internal.h"

#include <stdlib.h>
#include <string.h>


static ma_error_t ma_system_proxy_auth_info_create(ma_system_proxy_auth_info_t **system_proxy_auth){
	
	if(!system_proxy_auth) return MA_ERROR_INVALIDARG;
	
	*system_proxy_auth = (ma_system_proxy_auth_info_t*) calloc (1, sizeof(ma_system_proxy_auth_info_t));
	
	if(*system_proxy_auth)
	{
		return MA_OK;
	}
	
	return MA_ERROR_OUTOFMEMORY;
}

static ma_error_t ma_system_proxy_auth_info_release(ma_system_proxy_auth_info_t *system_proxy_auth){
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	if(system_proxy_auth){
		if(system_proxy_auth->http_user_name) {
			free(system_proxy_auth->http_user_name);
			system_proxy_auth->http_user_name = NULL;
		}
		if(system_proxy_auth->http_user_passwd) {
			free(system_proxy_auth->http_user_passwd);
			system_proxy_auth->http_user_passwd = NULL;
		}
		if(system_proxy_auth->ftp_user_name) {
			free(system_proxy_auth->ftp_user_name);
			system_proxy_auth->ftp_user_name = NULL;
		}
		if(system_proxy_auth->ftp_user_passwd) {
			free(system_proxy_auth->ftp_user_passwd);
			system_proxy_auth->ftp_user_passwd = NULL;
		}

		free(system_proxy_auth);
		system_proxy_auth = NULL;
		rc = MA_OK;
	}
	return rc;
}


ma_error_t ma_proxy_config_create(ma_proxy_config_t **configuration){
	if(configuration){
        ma_proxy_config_t *tmp = (ma_proxy_config_t *) calloc(1, sizeof(ma_proxy_config_t)) ;
        if(tmp) {
            if(MA_OK == ma_system_proxy_auth_info_create(&tmp->system_proxy_auth)) {
                if(MA_OK == ma_proxy_list_create(&tmp->proxy_list))
                {
					MA_ATOMIC_INCREMENT(tmp->ref_count) ;
                    *configuration = tmp ;
                    return MA_OK ;
                }
            }
            ma_proxy_config_release(tmp) ;
        }
        return MA_ERROR_OUTOFMEMORY ;
	}
	return MA_ERROR_INVALIDARG;
}


ma_error_t ma_proxy_config_add_ref(ma_proxy_config_t *self) {
	if(self) {
		MA_ATOMIC_INCREMENT(self->ref_count) ;
		return MA_OK ;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_proxy_config_release(ma_proxy_config_t *self){
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	if(self){
		if(0 == MA_ATOMIC_DECREMENT(self->ref_count)) {
			if(self->exclusion_list) free(self->exclusion_list);
			self->exclusion_list = NULL ;

			if(self->proxy_list) ma_proxy_list_release(self->proxy_list);
			self->proxy_list = NULL ;

			if(self->system_proxy_auth) ma_system_proxy_auth_info_release(self->system_proxy_auth);
			self->system_proxy_auth = NULL ;

			free(self); self = NULL;
		}
		rc = MA_OK;
	}
	return rc;
}

ma_error_t ma_proxy_config_set_proxy_usage_type(ma_proxy_config_t *configuration, ma_proxy_usage_type_t type){
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	if(configuration){
		configuration->proxy_usage_type = type;
		rc = MA_OK;
	}
	return rc;
}

ma_error_t ma_proxy_config_get_proxy_usage_type(ma_proxy_config_t *configuration, ma_proxy_usage_type_t *type){
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	if(configuration && type){
		*type = configuration->proxy_usage_type;
		rc = MA_OK;
	}
	return rc;
}

ma_error_t ma_proxy_config_set_proxy_list(ma_proxy_config_t *configuration, ma_proxy_list_t *list){
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	if(configuration){
		ma_proxy_list_release(configuration->proxy_list);
		configuration->proxy_list = list;
		rc = MA_OK;
	}
	return rc;
}

ma_error_t ma_proxy_config_get_proxy_list(ma_proxy_config_t *configuration, ma_proxy_list_t **list){
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	if(configuration && list && configuration->proxy_list){
		MA_ATOMIC_INCREMENT(configuration->proxy_list->ref_count);
		*list = configuration->proxy_list;
		rc = MA_OK;
	}
	return rc;
}

ma_error_t ma_proxy_config_set_bypass_local(ma_proxy_config_t *configuration, ma_bool_t bypass_local){
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	if(configuration){
		configuration->bypass_local_address = bypass_local;
		rc = MA_OK;
	}
	return rc;
}

ma_error_t ma_proxy_config_get_bypass_local(ma_proxy_config_t *configuration, ma_bool_t *bypass_local){
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	if(configuration && bypass_local){
		*bypass_local = configuration->bypass_local_address;
		rc = MA_OK;
	}
	return rc;
}

ma_error_t ma_proxy_config_set_allow_local_proxy_configuration(ma_proxy_config_t *configuration, ma_bool_t allow_or_not) {
    if(configuration) {
        configuration->allow_user_config = allow_or_not ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}
ma_error_t ma_proxy_config_get_allow_local_proxy_configuration(ma_proxy_config_t *configuration, ma_bool_t *allow_or_not) {
    if(configuration && allow_or_not) {
        *allow_or_not = configuration->allow_user_config ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_proxy_config_set_exclusion_urls(ma_proxy_config_t *configuration, const char *exclusion_str){
	if(configuration){
		if(exclusion_str){
			free(configuration->exclusion_list);
			configuration->exclusion_list = strdup(exclusion_str);
		}else{
			free(configuration->exclusion_list);
			configuration->exclusion_list = NULL;
		}
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_proxy_config_get_exclusion_urls(ma_proxy_config_t *configuration, const char **exclusion_list){
	if(configuration && exclusion_list){
		*exclusion_list = configuration->exclusion_list;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_proxy_config_set_system_proxy_authentication(ma_proxy_config_t *configuration, ma_proxy_protocol_type_t protocol_type, ma_bool_t auth_req, const char *user_name, const char *password){
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	if(auth_req && !(user_name && password))
		return rc;

	if(configuration){
		
		/*(void)ma_system_proxy_auth_info_release(configuration->system_proxy_auth);
		configuration->system_proxy_auth = NULL;*/
		if(!configuration->system_proxy_auth)
			(void)ma_system_proxy_auth_info_create(&configuration->system_proxy_auth);
		
		if(configuration->system_proxy_auth){
			if(protocol_type == MA_PROXY_TYPE_HTTP){
				configuration->system_proxy_auth->http_auth_rqrd = auth_req;
				if(user_name && password){
					if(configuration->system_proxy_auth->http_user_name)
						free(configuration->system_proxy_auth->http_user_name);
					if(configuration->system_proxy_auth->http_user_passwd)
						free(configuration->system_proxy_auth->http_user_passwd);

					configuration->system_proxy_auth->http_user_name= strdup(user_name);
					configuration->system_proxy_auth->http_user_passwd = strdup(password);
				}
			}
			else if(protocol_type == MA_PROXY_TYPE_FTP){
				configuration->system_proxy_auth->ftp_auth_rqrd = auth_req;
				if(user_name && password){
					if(configuration->system_proxy_auth->ftp_user_name)
						free(configuration->system_proxy_auth->ftp_user_name);
					if(configuration->system_proxy_auth->ftp_user_passwd)
						free(configuration->system_proxy_auth->ftp_user_passwd);
					configuration->system_proxy_auth->ftp_user_name = strdup(user_name);
					configuration->system_proxy_auth->ftp_user_passwd = strdup(password);
				}
			}
			else{
				configuration->system_proxy_auth->http_auth_rqrd = auth_req;
				configuration->system_proxy_auth->ftp_auth_rqrd = auth_req;
				if(user_name && password){
					if(configuration->system_proxy_auth->http_user_name)
						free(configuration->system_proxy_auth->http_user_name);
					if(configuration->system_proxy_auth->http_user_passwd)
						free(configuration->system_proxy_auth->http_user_passwd);
					if(configuration->system_proxy_auth->ftp_user_name)
						free(configuration->system_proxy_auth->ftp_user_name);
					if(configuration->system_proxy_auth->ftp_user_passwd)
						free(configuration->system_proxy_auth->ftp_user_passwd);

					configuration->system_proxy_auth->http_user_name= strdup(user_name);
					configuration->system_proxy_auth->http_user_passwd = strdup(password);
					configuration->system_proxy_auth->ftp_user_name = strdup(user_name);
					configuration->system_proxy_auth->ftp_user_passwd = strdup(password);
				}
			}
			rc= MA_OK;
		}
	}
	return rc;
}

ma_error_t ma_proxy_config_get_system_proxy_authentication(ma_proxy_config_t *configuration, ma_proxy_protocol_type_t protocol_type, ma_bool_t *auth_req, const char **user_name, const char **password){
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	if(configuration && configuration->system_proxy_auth && auth_req && user_name && password){
		if(protocol_type == MA_PROXY_TYPE_HTTP){
			*auth_req = configuration->system_proxy_auth->http_auth_rqrd;
			if(configuration->system_proxy_auth->http_user_name &&  configuration->system_proxy_auth->http_user_passwd){
				*user_name = configuration->system_proxy_auth->http_user_name;
				*password  = configuration->system_proxy_auth->http_user_passwd;
			}
			else{
				*user_name = NULL;
				*password = NULL;
			}
		}
		else{
			*auth_req = configuration->system_proxy_auth->ftp_auth_rqrd;
			if(configuration->system_proxy_auth->ftp_user_name &&  configuration->system_proxy_auth->ftp_user_passwd){
				*user_name = configuration->system_proxy_auth->ftp_user_name;
				*password  = configuration->system_proxy_auth->ftp_user_passwd;
			}
			else{
				*user_name = NULL;
				*password = NULL;
			}
		}
		rc= MA_OK;
	}
	return rc;
}

ma_error_t ma_proxy_config_get_system_proxy(ma_proxy_config_t *configuration, const char *url, ma_proxy_list_t **list){	
	return MA_ERROR_NOT_SUPPORTED;	
}

