#include "ma/internal/utils/booker/ma_booking_datastore.h"
#include "ma/internal/defs/ma_booking_defs.h"
#include "ma/scheduler/ma_enumerator.h"
#include "ma/internal/ma_macros.h"
#include "ma/ma_log.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "booker"

extern ma_logger_t *booker_logger;

#define BOOKING_DATA MA_DATASTORE_PATH_SEPARATOR "booking_data"
#define BOOKING_ID MA_DATASTORE_PATH_SEPARATOR "booking_id"

#define BOOKING_DATA_PATH BOOKING_DATA BOOKING_ID MA_DATASTORE_PATH_SEPARATOR/* \"booking_data"\"booking_id"\ */


ma_error_t ma_booking_datastore_read(ma_booker_t *booker, ma_ds_t *datastore, const char *ds_path) {
    if(booker && datastore && ds_path) {
        ma_error_t err = MA_OK;
        ma_ds_iterator_t *booking_iterator = NULL;
        ma_buffer_t *booking_id_buf = NULL;
        char booking_data_base_path[MA_MAX_PATH_LEN] = {0};

        MA_MSC_SELECT(_snprintf, snprintf)(booking_data_base_path, MA_MAX_PATH_LEN, "%s%s", ds_path, BOOKING_DATA_PATH);
        MA_LOG(booker_logger, MA_LOG_SEV_DEBUG, "booker data store path(%s) reading...", booking_data_base_path);
        if(MA_OK == (err = ma_ds_iterator_create(datastore, 0, booking_data_base_path, &booking_iterator))) {
            while(MA_ERROR_NO_MORE_ITEMS != ma_ds_iterator_get_next(booking_iterator, &booking_id_buf) && booking_id_buf) {
                const char *booking_id = NULL;
                size_t size = 0;
                
                if(MA_OK == (err = ma_buffer_get_string(booking_id_buf, &booking_id, &size))) {
                    char booking_data_path[MA_MAX_PATH_LEN] = {0};
                    ma_variant_t *booking_var = NULL;
                    MA_MSC_SELECT(_snprintf, snprintf)(booking_data_path, MA_MAX_PATH_LEN, "%s", booking_data_base_path);/* base_path\"booking_data"\"booking_id"\4097 */
                    if(MA_OK == (err = ma_ds_get_variant(datastore, booking_data_path, booking_id, MA_VARTYPE_TABLE, &booking_var))) {

                        (void)ma_variant_release(booking_var);
                    }
                }
                (void)ma_buffer_release(booking_id_buf);
            }
            (void)ma_ds_iterator_release(booking_iterator);
        }

        return ((MA_ERROR_DS_PATH_NOT_FOUND == err) || (MA_ERROR_SCHEDULER_TASK_ID_CONFLICT == err)) ? MA_OK : err;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_booking_datastore_write(ma_booker_t *booker, ma_booking_info_t *info, ma_ds_t *datastore, const char *ds_path) {
    if(booker && info && datastore && ds_path) {
        char booking_data_path[MA_MAX_PATH_LEN] = {0};
        char booking_id[MA_MAX_PATH_LEN] = {0};
        ma_error_t err = MA_OK;
        ma_variant_t *var = NULL;

        //(void)ma_booking_info_get_email_id(info, booking_id);
        (void)ma_booking_info_get_id(info, booking_id);
        MA_MSC_SELECT(_snprintf, snprintf)(booking_data_path, MA_MAX_PATH_LEN, "%s%s", ds_path, BOOKING_DATA_PATH);
        MA_LOG(booker_logger, MA_LOG_SEV_INFO, "booking id(%s), data path(%s) booking_data_path(%s)", booking_id, ds_path, booking_data_path);
        if(MA_OK == (err = ma_booking_info_convert_to_variant(info, &var))) {
            if(MA_OK == (err = ma_ds_set_variant(datastore, booking_data_path, booking_id, var)))
                MA_LOG(booker_logger, MA_LOG_SEV_TRACE, "booker datastore write booking id(%s) successful", booking_id);
            else
                MA_LOG(booker_logger, MA_LOG_SEV_ERROR, "booker datastore write booking id(%s) failed, last error(%d)", booking_id, err);
            (void)ma_variant_release(var);
        } else
            MA_LOG(booker_logger, MA_LOG_SEV_ERROR, "booker datastore write booking id(%s) conversion booking info to variant failed, last error(%d)", booking_id, err);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_datastore_write_variant(ma_booker_t *booker, const char *booking_id, ma_ds_t *datastore, const char *ds_path, ma_variant_t *var) {
    if(booker && booking_id && var && datastore && ds_path) {
        char booking_data_path[MA_MAX_PATH_LEN] = {0};
        ma_error_t err = MA_OK;

        MA_MSC_SELECT(_snprintf, snprintf)(booking_data_path, MA_MAX_PATH_LEN, "%s%s", ds_path, BOOKING_DATA_PATH);
        MA_LOG(booker_logger, MA_LOG_SEV_INFO, "booking id(%s), data path(%s) booking_data_path(%s)", booking_id, ds_path, booking_data_path);
        if(MA_OK == (err = ma_ds_set_variant(datastore, booking_data_path, booking_id, var)))
            MA_LOG(booker_logger, MA_LOG_SEV_TRACE, "booker datastore write booking id(%s) successful", booking_id);
        else
            MA_LOG(booker_logger, MA_LOG_SEV_ERROR, "booker datastore write booking id(%s) failed, last error(%d)", booking_id, err);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_booking_datastore_remove_booking(const char *booking, ma_ds_t *datastore, const char *ds_path) {
    if(booking && datastore && ds_path) {
        char booking_path[MA_MAX_PATH_LEN] = {0};
        
        MA_LOG(booker_logger, MA_LOG_SEV_DEBUG, "booker removing (%s) from datastore", booking);
        MA_MSC_SELECT(_snprintf, snprintf)(booking_path, MA_MAX_PATH_LEN, "%s%s",ds_path, BOOKING_DATA_PATH );/* base_path\"booking_data"\4097 */
        return ma_ds_rem(datastore, booking_path, booking, 1);
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_datastore_get(ma_booker_t *booker, ma_ds_t *datastore, const char *ds_path, const char *booking_id, ma_booking_info_t **info) {
    if(booker && datastore && ds_path && booking_id && info) {
        ma_error_t err = MA_OK;
        char booking_data_base_path[MA_MAX_PATH_LEN] = {0};
        ma_variant_t *booking_var = NULL;

        MA_MSC_SELECT(_snprintf, snprintf)(booking_data_base_path, MA_MAX_PATH_LEN, "%s%s", ds_path, BOOKING_DATA_PATH);
        if(MA_OK == (err = ma_ds_get_variant(datastore, booking_data_base_path, booking_id, MA_VARTYPE_TABLE, &booking_var))) {
            if(MA_OK == (err = ma_booking_info_convert_from_variant(booking_var, info)))
                MA_LOG(booker_logger, MA_LOG_SEV_TRACE, "booking(%s) exist in booker DB", booking_id);
            else
                MA_LOG(booker_logger, MA_LOG_SEV_ERROR, "booking(%s) variant to booking conversion failed, last error(%d)", booking_id, err);
            (void)ma_variant_release(booking_var);
        } else 
            MA_LOG(booker_logger, MA_LOG_SEV_ERROR, "booking(%s) does not exist in booker DB, last error(%d)", booking_id, err);

        return err;
    }
    return MA_ERROR_INVALIDARG;

}

ma_error_t ma_booking_datastore_clear(ma_ds_t *datastore, const char *ds_path) {
    return(datastore && ds_path) ? ma_ds_rem(datastore, ds_path, NULL, 1) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_datastore_get_all(ma_booker_t *booker, ma_ds_t *datastore, const char *ds_path, const char *booking_id, ma_variant_t **var) {
    if(booker && datastore && ds_path && booking_id && var) {
        ma_error_t err = MA_OK;
        char booking_data_base_path[MA_MAX_PATH_LEN] = {0};

        MA_MSC_SELECT(_snprintf, snprintf)(booking_data_base_path, MA_MAX_PATH_LEN, "%s%s", ds_path, BOOKING_DATA_PATH);
        if(MA_OK == (err = ma_ds_get_variant(datastore, booking_data_base_path, booking_id, MA_VARTYPE_TABLE, var))) {
            MA_LOG(booker_logger, MA_LOG_SEV_TRACE, "booking id(%s) DB get successful", booking_id);
        } else
            MA_LOG(booker_logger, MA_LOG_SEV_ERROR, "booking id(%s) does not exist in DB, last error(%d)", booking_id, err);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

