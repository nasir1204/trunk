#include "ma/internal/utils/booker/ma_booker.h"
#include "ma/internal/utils/booker/ma_booking_info.h"
#include "ma/internal/utils/booker/ma_booking_datastore.h"
#include "ma/ma_msgbus.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/defs/ma_booking_defs.h"
#include "ma/internal/ma_macros.h"

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "booker"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <uv.h>

ma_logger_t *booker_logger;

struct ma_booker_s {
    char path[MA_MAX_PATH_LEN+1];
    ma_msgbus_t *msgbus;
    ma_ds_t* datastore;
};


ma_error_t ma_booker_create(ma_msgbus_t *msgbus, ma_ds_t *datastore, const char *base_path, ma_booker_t **booker) {
    if(msgbus && datastore && base_path && booker) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;

        if((*booker = (ma_booker_t*)calloc(1, sizeof(ma_booker_t)))) {
            (*booker)->msgbus = msgbus;
            strncpy((*booker)->path, base_path, MA_MAX_PATH_LEN);
            (*booker)->datastore = datastore;
            err = MA_OK;
            MA_LOG(booker_logger, MA_LOG_SEV_INFO, "booker created successfully");
        }
        if(MA_OK != err) {
            MA_LOG(booker_logger, MA_LOG_SEV_ERROR, "booker creation failed, last error(%d)", err);
            if(*booker)
                (void)ma_booker_release(*booker);
            *booker = NULL;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booker_add(ma_booker_t *booker, ma_booking_info_t *info) {
    return booker && info ? ma_booking_datastore_write(booker, info, booker->datastore, booker->path) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_booker_add_variant(ma_booker_t *booker, const char *booking_id, ma_variant_t *var) {
    return booker && booking_id && var ? ma_booking_datastore_write_variant(booker, booking_id, booker->datastore, booker->path, var) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_booker_delete(ma_booker_t *booker, const char *booking) {
    return booker && booking ? ma_booking_datastore_remove_booking(booking, booker->datastore, booker->path) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_booker_delete_all(ma_booker_t *booker) {
    return booker ? ma_booking_datastore_clear(booker->datastore, booker->path) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_booker_get(ma_booker_t *booker, const char *booking, ma_booking_info_t **info) {
    return booker && booking && info ? ma_booking_datastore_get(booker, booker->datastore, booker->path, booking, info) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_booker_get_all(ma_booker_t *booker, const char *booking, ma_variant_t **var) {
    return booker && booking && var ? ma_booking_datastore_get_all(booker, booker->datastore, booker->path, booking, var) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_booker_set_logger(ma_logger_t *logger) {
    if(logger) {
        booker_logger = logger;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booker_get_msgbus(ma_booker_t *booker, ma_msgbus_t **msgbus) {
    if(booker && msgbus) {
        *msgbus = booker->msgbus;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booker_start(ma_booker_t *booker) {
    if(booker) {
        MA_LOG(booker_logger, MA_LOG_SEV_TRACE, "booker started");
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booker_stop(ma_booker_t *booker) {
    if(booker) {
        MA_LOG(booker_logger, MA_LOG_SEV_TRACE, "booker stopped");
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_booker_release(ma_booker_t *booker) {
    if(booker) {
        free(booker);
    }
    return MA_ERROR_INVALIDARG;
}

