#include "ma/internal/utils/booker/ma_booking_info.h"
#include "ma/internal/utils/json/ma_cjson.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/defs/ma_booking_defs.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"
#include "ma/ma_log.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

extern ma_logger_t *booker_logger;

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "booking"

struct ma_booking_info_s {
    char id[MA_MAX_NAME+1];
    char pickup_date[MA_MAX_NAME+1];
    char deliver_date[MA_MAX_NAME+1];
    ma_booking_info_service_type_t service_type;
    ma_booking_info_delivery_type_t delivery_type;
    char recipient[MA_MAX_NAME+1];
    char recipient_contact[MA_MAX_NAME+1];
    char email[MA_MAX_LEN+1];
    char name[MA_MAX_NAME+1];
    char contact[MA_MAX_LEN+1];
    char from_address[MA_MAX_BUFFER_LEN+1];
    char from_pincode[MA_MAX_LEN+1];
    char from_city[MA_MAX_LEN+1];
    char from_state[MA_MAX_LEN+1];
    char from_country[MA_MAX_LEN+1];
    char to_address[MA_MAX_BUFFER_LEN+1];
    char to_pincode[MA_MAX_LEN+1];
    char to_city[MA_MAX_LEN+1];
    char to_state[MA_MAX_LEN+1];
    char to_country[MA_MAX_LEN+1];
    ma_booking_info_user_type_t user_type;
    ma_booking_info_privilege_t privi_type;
    ma_uint32_t password_set:1;
    char picker_contact[MA_MAX_LEN+1];
    ma_booking_info_status_t status;
    ma_uint32_t last_login;
    ma_uint32_t reserved; 
    ma_uint32_t price;
    ma_uint32_t discount;
    char picker_email_id[MA_MAX_LEN+1];
    char deliver_email_id[MA_MAX_LEN+1];
    char details[MA_MAX_LEN+1];
    ma_booking_info_payment_type_t payment_type;
    ma_booking_info_payment_status_t payment_status;
    ma_uint32_t distance;
    long pick_time;
    long deliver_time;
    ma_uint32_t weight;
    ma_uint32_t length;
    ma_uint32_t estimate_delivery;
    ma_booking_info_location_type_t location_type;
    char transit[MA_MAX_BUFFER_LEN+1];
    char delivery_note[MA_MAX_BUFFER_LEN+1];
    ma_table_t *transitions;
    ma_bool_t active;
    ma_json_t *json;
    char package_img_url[MA_MAX_BUFFER_LEN+1];
};


const char *gbooking_status_str[] =  {
#define MA_BOOKING_INFO_STATUS_EXPAND(status, index, status_str) status_str,
    MA_BOOKING_INFO_STATUS_EXPANDER
#undef MA_BOOKING_INFO_STATUS_EXPAND
}; 

const char *gpayment_mode_str[] = {
#define MA_BOOKING_INFO_PAYMENT_EXPAND(status, index, status_str) status_str,
    MA_BOOKING_INFO_PAYMENT_EXPANDER
#undef MA_BOOKING_INFO_PAYMENT_EXPAND
};

const char *gpayment_status_str[] = {
#define MA_BOOKING_INFO_PAYMENT_STATUS_EXPAND(status, index, status_str) status_str,
    MA_BOOKING_INFO_PAYMENT_STATUS_EXPANDER
#undef MA_BOOKING_INFO_PAYMENT_STATUS_EXPAND
};

const char *gdelivery_type_str[] =  {
#define MA_BOOKING_INFO_DELIVERY_TYPE_EXPAND(status, index, status_str) status_str,
    MA_BOOKING_INFO_DELIVERY_TYPE_EXPANDER
#undef MA_BOOKING_INFO_DELIVERY_TYPE_EXPAND
}; 

const char *gservice_type_str[] =  {
#define MA_BOOKING_INFO_SERVICE_TYPE_EXPAND(status, index, status_str) status_str,
    MA_BOOKING_INFO_SERVICE_TYPE_EXPANDER
#undef MA_BOOKING_INFO_SERVICE_TYPE_EXPAND
}; 

ma_error_t ma_booking_info_create(ma_booking_info_t **info, ma_booking_info_user_type_t type) {
    if(info) {
         ma_error_t err = MA_ERROR_OUTOFMEMORY;

         if((*info = (ma_booking_info_t*)calloc(1, sizeof(ma_booking_info_t)))) {
             if(MA_OK == (err = ma_json_create(&(*info)->json))) {
                 (*info)->user_type = type;
                 (*info)->status = MA_BOOKING_INFO_STATUS_NOT_ASSIGNED;
                 (*info)->payment_status = MA_BOOKING_INFO_PAYMENT_STATUS_UNPAID;
                 (*info)->payment_type = MA_BOOKING_INFO_PAYMENT_TYPE_CASH;
                 (*info)->active = MA_TRUE;
                 err = ma_table_create(&(*info)->transitions);
             }
         }

         return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_release(ma_booking_info_t *info) {
    if(info) {
        if(info->transitions)(void)ma_table_release(info->transitions);
        (void)ma_json_release(info->json);
        free(info);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_add_transits(ma_booking_info_t *info, const char *transits) {
    if(info && transits) {
        ma_error_t err = MA_OK;
        ma_task_time_t now = {0};
        char date_str[MA_MAX_LEN+1] = {0};
        ma_variant_t *v = NULL;

        (void)get_localtime(&now);
        (void)ma_strtime(now, date_str);
        if(MA_OK == (err = ma_variant_create_from_string(transits, strlen(transits), &v))) {
            if(MA_OK == (err = ma_table_add_entry(info->transitions, date_str, v))) {
                MA_LOG(booker_logger, MA_LOG_SEV_DEBUG, "transits(%s) add", transits);
            } else {
                MA_LOG(booker_logger, MA_LOG_SEV_ERROR, "transits(%s) add failed last error(%d)", transits, err);
            }
            (void)ma_variant_release(v);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

/* setter */
ma_error_t ma_booking_info_set_payment_status_from_string(ma_booking_info_t *info, char *status) {
    if(info && status) {
        if(!strcmp(status, MA_BOOKING_INFO_PAYMENT_STATUS_UNPAID_STR))
            info->payment_status = MA_BOOKING_INFO_PAYMENT_STATUS_UNPAID;
        else if(!strcmp(status, MA_BOOKING_INFO_PAYMENT_STATUS_PAID_STR))
            info->payment_status = MA_BOOKING_INFO_PAYMENT_STATUS_PAID;
        else;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}
ma_error_t ma_booking_info_set_delivery_note(ma_booking_info_t *info, const char *note) {
    if(info && note) {
        strncpy(info->delivery_note, note, MA_MAX_BUFFER_LEN);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_set_payment_status(ma_booking_info_t *info, ma_booking_info_payment_status_t status) {
    if(info) {
        info->payment_status = status;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_set_package_image_url(ma_booking_info_t *info, const char *url) {
    if(info && url) {
         strncpy(info->package_img_url, url, MA_MAX_BUFFER_LEN);
         return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_set_active(ma_booking_info_t *info, ma_bool_t active) {
    if(info) {
        info->active = active;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_set_transitions(ma_booking_info_t *info, ma_table_t *transitions) {
    if(info && transitions) {
        (void)ma_table_release(info->transitions);
        (void)ma_table_add_ref(info->transitions = transitions);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_set_transit(ma_booking_info_t *info, const char *str) {
    return info && str && strcpy(info->transit, str) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_set_status_from_string(ma_booking_info_t *info, const char *str) {
    if(info && str) {
        ma_error_t err = MA_OK;

        if(!strcmp(str, MA_BOOKING_INFO_STATUS_NOT_ASSIGNED_STR))
            info->status = MA_BOOKING_INFO_STATUS_NOT_ASSIGNED;
        else if(!strcmp(str, MA_BOOKING_INFO_STATUS_ASSIGNED_STR))
            info->status = MA_BOOKING_INFO_STATUS_ASSIGNED;
        else if(!strcmp(str, MA_BOOKING_INFO_STATUS_CANCELLED_STR))
            info->status = MA_BOOKING_INFO_STATUS_CANCELLED;
        else if(!strcmp(str, MA_BOOKING_INFO_STATUS_DELIVERED_STR))
            info->status = MA_BOOKING_INFO_STATUS_DELIVERED;
        else if(!strcmp(str, MA_BOOKING_INFO_STATUS_NOT_DELIVERED_STR))
            info->status = MA_BOOKING_INFO_STATUS_NOT_DELIVERED;
        else if(!strcmp(str, MA_BOOKING_INFO_STATUS_PICKED_STR))
            info->status = MA_BOOKING_INFO_STATUS_PICKED;
        else if(!strcmp(str, MA_BOOKING_INFO_STATUS_TRANSIT_STR))
            info->status = MA_BOOKING_INFO_STATUS_TRANSIT;
        else {
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_set_payment_from_string(ma_booking_info_t *info, const char *str) {
    if(info && str) {
        ma_error_t err = MA_OK;

        if(!strcmp(str, MA_BOOKING_INFO_PAYMENT_TYPE_CREDIT_CARD_STR))
            info->payment_type = MA_BOOKING_INFO_PAYMENT_TYPE_CREDIT_CARD;
        else if(!strcmp(str, MA_BOOKING_INFO_PAYMENT_TYPE_DEBIT_CARD_STR))
            info->payment_type = MA_BOOKING_INFO_PAYMENT_TYPE_DEBIT_CARD;
        else if(!strcmp(str, MA_BOOKING_INFO_PAYMENT_TYPE_ONLINE_STR))
            info->payment_type = MA_BOOKING_INFO_PAYMENT_TYPE_ONLINE;
        else if(!strcmp(str, MA_BOOKING_INFO_PAYMENT_TYPE_CASH_STR))
            info->payment_type = MA_BOOKING_INFO_PAYMENT_TYPE_CASH;
        else {
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_set_location_from_string(ma_booking_info_t *info, const char *str) {
    if(info && str) {
        ma_error_t err = MA_OK;

        if(!strcmp(str, MA_BOOKING_INFO_LOCATION_TYPE_SAME_AREA_STR))
            info->location_type = MA_BOOKING_INFO_LOCATION_TYPE_SAME_AREA;
        else if(!strcmp(str, MA_BOOKING_INFO_LOCATION_TYPE_DIFF_AREA_STR))
            info->location_type = MA_BOOKING_INFO_LOCATION_TYPE_DIFF_AREA;
        else if(!strcmp(str, MA_BOOKING_INFO_LOCATION_TYPE_ACROSS_CITY_STR))
            info->location_type = MA_BOOKING_INFO_LOCATION_TYPE_ACROSS_CITY;
        else {
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_set_from_city(ma_booking_info_t *info, const char *city) {
    return info && city && strncpy(info->from_city, city, MA_MAX_LEN) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_set_from_state(ma_booking_info_t *info, const char *state) {
    return info && state && strncpy(info->from_state, state, MA_MAX_LEN) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_set_from_country(ma_booking_info_t *info, const char *country) {
    return info && country && strncpy(info->from_country, country, MA_MAX_LEN) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_set_to_city(ma_booking_info_t *info, const char *city) {
    return info && city && strncpy(info->to_city, city, MA_MAX_LEN) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_set_to_state(ma_booking_info_t *info, const char *state) {
    return info && state && strncpy(info->to_state, state, MA_MAX_LEN) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_set_to_country(ma_booking_info_t *info, const char *country) {
    return info && country && strncpy(info->to_country, country, MA_MAX_LEN) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_set_price(ma_booking_info_t *info, ma_uint32_t price) {
    if(info) {
        info->price = price;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_set_discount(ma_booking_info_t *info, ma_uint32_t discount) {
    if(info) {
        info->discount = discount;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_set_picker_email_id(ma_booking_info_t *info, const char *id) {
    return info && id && strncpy(info->picker_email_id, id, MA_MAX_LEN) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_set_deliver_email_id(ma_booking_info_t *info, const char *id) {
    return info && id && strncpy(info->deliver_email_id, id, MA_MAX_LEN) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_set_details(ma_booking_info_t *info, const char *details) {
    return info && details && strncpy(info->details, details, MA_MAX_LEN) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_set_payment_type(ma_booking_info_t *info, ma_booking_info_payment_type_t type) {
    if(info) {
        info->payment_type = type;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_set_distance(ma_booking_info_t *info, ma_uint32_t distance) {
    if(info) {
        info->distance = distance;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_set_pick_time(ma_booking_info_t *info, long time) {
    if(info) {
        info->pick_time = time;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_set_deliver_time(ma_booking_info_t *info, long time) {
    if(info) {
        info->deliver_time = time;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_set_weight(ma_booking_info_t *info, ma_uint32_t weight) {
    if(info) {
        info->weight = weight;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_set_length(ma_booking_info_t *info, ma_uint32_t length) {
    if(info) {
        info->length = length;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_set_estimate_delivery(ma_booking_info_t *info, ma_uint32_t days) {
    if(info) {
        info->estimate_delivery = days;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_set_location_type(ma_booking_info_t *info, ma_booking_info_location_type_t loc_type) {
    if(info) {
        info->location_type = loc_type;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_set_email_id(ma_booking_info_t *info, const char *id) {
    return info && id && memset(info->email, 0, MA_MAX_LEN) ? strncpy(info->email, id, MA_MAX_LEN) ? MA_OK : MA_ERROR_INVALIDARG : MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_set_user(ma_booking_info_t *info, const char *user) {
    return info && user && memset(info->name, 0, MA_MAX_NAME) ? strncpy(info->name, user,  MA_MAX_NAME) ? MA_OK : MA_ERROR_INVALIDARG : MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_set_contact(ma_booking_info_t *info, const char *contact) {
    if(info && contact) {
        ma_error_t err = MA_OK;

        /* TODO: contact number validation check */
        memset(info->contact, 0, MA_MAX_NAME);
        err = strncpy(info->contact, contact, MA_MAX_NAME) ? MA_OK : MA_ERROR_INVALIDARG;
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_set_from_address(ma_booking_info_t *info, const char *addr) {
    return info && addr && memset(info->from_address, 0, MA_MAX_BUFFER_LEN) ? strncpy(info->from_address, addr,  MA_MAX_BUFFER_LEN) ? MA_OK : MA_ERROR_INVALIDARG : MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_set_to_address(ma_booking_info_t *info, const char *addr) {
    return info && addr && memset(info->to_address, 0, MA_MAX_BUFFER_LEN) ? strncpy(info->to_address, addr,  MA_MAX_BUFFER_LEN) ? MA_OK : MA_ERROR_INVALIDARG : MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_set_picker_contact(ma_booking_info_t *info, const char *picker_contact) {
    return info && picker_contact && memset(info->picker_contact, 0, MA_MAX_NAME) ? strncpy(info->picker_contact, picker_contact,  MA_MAX_NAME) ? MA_OK : MA_ERROR_INVALIDARG : MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_set_last_login(ma_booking_info_t *info, ma_uint32_t time) {
    if(info) {
        info->last_login = time;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_set_status(ma_booking_info_t *info, ma_booking_info_status_t status) {
    if(info) {
        info->status = status;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_set_source_pincode(ma_booking_info_t *info, const char *pincode) {
    //TODO validate pin code 
    return info && pincode && memset(info->from_pincode, 0, MA_MAX_NAME) ? strncpy(info->from_pincode, pincode,  MA_MAX_NAME) ? MA_OK : MA_ERROR_INVALIDARG : MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_set_dest_pincode(ma_booking_info_t *info, const char *pincode) {
    //TODO validate pin code 
    return info && pincode && memset(info->to_pincode, 0, MA_MAX_NAME) ? strncpy(info->to_pincode, pincode,  MA_MAX_NAME) ? MA_OK : MA_ERROR_INVALIDARG : MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_set_user_type(ma_booking_info_t *info, ma_booking_info_user_type_t type) {
    if(info) {
        info->user_type = type;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_set_privilege(ma_booking_info_t *info, ma_booking_info_privilege_t priv) {
    if(info) {
        info->privi_type = priv;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_set_recipient(ma_booking_info_t *info, const char *recipient) {
    return (info && recipient && strncpy(info->recipient, recipient, MA_MAX_NAME)) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_set_delivery_type(ma_booking_info_t *info, ma_booking_info_delivery_type_t type) {
    if(info) {
        info->delivery_type = type;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_set_service_type(ma_booking_info_t *info, ma_booking_info_service_type_t type) {
    if(info) {
        info->service_type = type;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_set_id(ma_booking_info_t *info, const char *id) {
    return (info && id && strncpy(info->id, id, MA_MAX_NAME)) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_set_recipient_contact(ma_booking_info_t *info, const char *contact) {
    return info && contact && strncpy(info->recipient_contact, contact, MA_MAX_NAME) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_set_pickup_date(ma_booking_info_t *info, const char *date) {
    return info && date && strncpy(info->pickup_date, date, MA_MAX_NAME) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_set_deliver_date(ma_booking_info_t *info, const char *str) {
    return info && str && strncpy(info->deliver_date, str, MA_MAX_NAME) ? MA_OK : MA_ERROR_INVALIDARG;
}

/* getter */
ma_error_t ma_booking_info_get_delivery_note(ma_booking_info_t *info, char *note) {
    if(info && note) {
        strncpy(note, info->delivery_note, MA_MAX_BUFFER_LEN);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_get_payment_status(ma_booking_info_t *info, ma_booking_info_payment_status_t *status) {
    if(info && status) {
        *status = info->payment_status;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_get_payment_status_str(ma_booking_info_t *info, char *status) {
    if(info && status) {
        if(info->payment_status == MA_BOOKING_INFO_PAYMENT_STATUS_PAID)
            strncpy(status, MA_BOOKING_INFO_PAYMENT_STATUS_PAID_STR, MA_MAX_LEN);
        else if(info->payment_status == MA_BOOKING_INFO_PAYMENT_STATUS_UNPAID)
            strncpy(status, MA_BOOKING_INFO_PAYMENT_STATUS_UNPAID_STR, MA_MAX_LEN);
        else;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_get_package_image_url(ma_booking_info_t *info, char *url) {
    if(info && url && info->package_img_url) {
        strncpy(url, info->package_img_url, MA_MAX_BUFFER_LEN);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_get_active(ma_booking_info_t *info, ma_bool_t *active) {
   if(info && active) {
       *active = info->active;
       return MA_OK;
   }
   return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_get_transitions(ma_booking_info_t *info, ma_table_t **transitions) {
    if(info && transitions) {
        return ma_table_add_ref(*transitions = info->transitions);
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_get_transit(ma_booking_info_t *info, char *str) {
    return info && str && strcpy(str, info->transit) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_get_from_city(ma_booking_info_t *info, char *city) {
    return info && city && strncpy(city, info->from_city, MA_MAX_LEN) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_get_to_city(ma_booking_info_t *info, char *city) {
    return info && city && strncpy(city, info->to_city, MA_MAX_LEN) ? MA_OK : MA_ERROR_INVALIDARG;
}
ma_error_t ma_booking_info_get_price(ma_booking_info_t *info, ma_uint32_t *price) {
    if(info && price) {
        *price = info->price;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_get_discount(ma_booking_info_t *info, ma_uint32_t *discount) {
    if(info && discount) {
         *discount = info->discount;
         return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_get_picker_email_id(ma_booking_info_t *info, char *id) {
    return info && id && strncpy(id, info->picker_email_id, MA_MAX_LEN) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_get_deliver_email_id(ma_booking_info_t *info, char *id) {
    return info && id && strncpy(id, info->deliver_email_id, MA_MAX_LEN) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_get_details(ma_booking_info_t *info, char *details) {
    return info && details && strncpy(details, info->details, MA_MAX_LEN) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_get_payment_type(ma_booking_info_t *info, ma_booking_info_payment_type_t *type) {
    if(info && type) {
        *type = info->payment_type;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_get_distance(ma_booking_info_t *info, ma_uint32_t *distance) {
    if(info && distance) {
        *distance = info->distance;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_get_pick_time(ma_booking_info_t *info, long *time) {
    if(info && time) {
        *time = info->pick_time;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_get_deliver_time(ma_booking_info_t *info, long *time) {
    if(info && time) {
        *time = info->deliver_time;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_get_weight(ma_booking_info_t *info, ma_uint32_t *weight) {
    if(info && weight) {
        *weight = info->weight;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_get_length(ma_booking_info_t *info, ma_uint32_t *length) {
    if(info && length) {
        *length = info->length;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_get_estimate_delivery(ma_booking_info_t *info, ma_uint32_t *days) {
    if(info && days) {
        *days = info->estimate_delivery;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_get_location_type(ma_booking_info_t *info, ma_booking_info_location_type_t *loc_type) {
    if(info && loc_type) {
        *loc_type = info->location_type;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_booking_info_get_status_str(ma_booking_info_t *info, char *str) {
    return info && str && strncpy(str, gbooking_status_str[info->status - 1], MA_MAX_NAME) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_get_delivery_type_str(ma_booking_info_t *info, char *str) {
    return info && str && strncpy(str, gdelivery_type_str[info->delivery_type - 1], MA_MAX_NAME) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_get_service_type_str(ma_booking_info_t *info, char *str) {
    return info && str && strncpy(str, gservice_type_str[info->service_type - 1], MA_MAX_NAME) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_get_pickup_date(ma_booking_info_t *info, char *date) {
    return info && date && strncpy(date, info->pickup_date, MA_MAX_NAME) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_get_recipient_contact(ma_booking_info_t *info, char *contact) {
    return info && contact && strncpy(contact, info->recipient_contact, MA_MAX_NAME) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_get_id(ma_booking_info_t *info, char *id) {
    return (info && id && strncpy(id, info->id, MA_MAX_NAME)) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_get_service_type(ma_booking_info_t *info, ma_booking_info_service_type_t *type) {
    if(info && type) {
        *type = info->service_type;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_get_delivery_type(ma_booking_info_t *info, ma_booking_info_delivery_type_t *type) {
    if(info && type) {
        *type = info->delivery_type;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_get_recipient(ma_booking_info_t *info, char *recipient) {
    return (info && recipient && strncpy(recipient, info->recipient, MA_MAX_NAME)) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_get_privilege(ma_booking_info_t *info, ma_booking_info_privilege_t *priv) {
   if(info && priv) {
      *priv = info->privi_type;
      return MA_OK;
   }
   return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_get_user_type(ma_booking_info_t *info, ma_booking_info_user_type_t *type) {
    if(info && type) {
        *type = info->user_type;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}
ma_error_t ma_booking_info_get_email_id(ma_booking_info_t *info, char *id) {
    return info && id && strncpy(id, info->email, MA_MAX_LEN) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_get_user(ma_booking_info_t *info, char *user) {
    return info && user && strncpy(user, info->name, MA_MAX_NAME) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_get_contact(ma_booking_info_t *info, char *contact) {
    return info && contact && strncpy(contact, info->contact, MA_MAX_LEN) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_get_from_address(ma_booking_info_t *info, char *addr) {
    return info && addr && strncpy(addr, info->from_address, MA_MAX_BUFFER_LEN) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_get_to_address(ma_booking_info_t *info, char *addr) {
    return info && addr && strncpy(addr, info->to_address, MA_MAX_BUFFER_LEN) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_get_picker_contact(ma_booking_info_t *info, char *picker_contact) {
    return info && picker_contact &&  strncpy(picker_contact, info->picker_contact, MA_MAX_LEN) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_get_last_login(ma_booking_info_t *info, ma_uint32_t *time) {
    if(info) {
        *time = info->last_login;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_get_status(ma_booking_info_t *info, ma_booking_info_status_t *status) {
    if(info) {
        *status = info->status;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_get_source_pincode(ma_booking_info_t *info, char *pincode) {
    return info && pincode && strncpy(pincode, info->from_pincode, MA_MAX_LEN) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_get_dest_pincode(ma_booking_info_t *info, char *pincode) {
    return info && pincode && strncpy(pincode, info->to_pincode, MA_MAX_LEN) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_get_deliver_date(ma_booking_info_t *info, char *str) {
    return (info && str && strncpy(str, info->deliver_date, MA_MAX_NAME)) ? MA_OK : MA_ERROR_INVALIDARG;
}



ma_error_t ma_booking_info_convert_to_variant(ma_booking_info_t *info, ma_variant_t **variant) {
    if(info && variant) {
        ma_error_t err = MA_OK;
        ma_table_t *info_table = NULL;

        if(MA_OK == (err = ma_table_create(&info_table))) {
             {
                 char email_id[MA_MAX_BUFFER_LEN+1] = {0};

                 if(MA_OK == (err = ma_booking_info_get_email_id(info, email_id))) {
                     ma_variant_t *email =NULL;

                     if(MA_OK == (err = ma_variant_create_from_string(email_id, strlen(email_id), &email))) {
                         err = ma_table_add_entry(info_table, MA_BOOKING_INFO_ATTR_EMAIL_ID, email);
                         (void)ma_variant_release(email);
                     }
                 }
             }

             {
                 char url[MA_MAX_BUFFER_LEN+1] = {0};

                 if(MA_OK == (err = ma_booking_info_get_package_image_url(info, url))) {
                     ma_variant_t *v =NULL;

                     if(MA_OK == (err = ma_variant_create_from_string(url, strlen(url), &v))) {
                         err = ma_table_add_entry(info_table, MA_BOOKING_INFO_ATTR_PACKAGE_URL, v);
                         (void)ma_variant_release(v);
                     }
                 }
             }
             {
                  char user[MA_MAX_NAME+1] = {0};

                  if(MA_OK == (err = ma_booking_info_get_user(info, user))) {
                     ma_variant_t *name = NULL;

                     if(MA_OK == (err = ma_variant_create_from_string(user, strlen(user), &name))) {
                         err = ma_table_add_entry(info_table, MA_BOOKING_INFO_ATTR_NAME, name);
                         (void)ma_variant_release(name);
                     }
                  }
             }

             {
                  char contact[MA_MAX_LEN+1] = {0};

                  if(MA_OK == (err = ma_booking_info_get_contact(info, contact))) {
                     ma_variant_t *number = NULL;

                     if(MA_OK == (err = ma_variant_create_from_string(contact, strlen(contact), &number))) {
                         err = ma_table_add_entry(info_table, MA_BOOKING_INFO_ATTR_CONTACT, number);
                         (void)ma_variant_release(number);
                     }
                  }
             }

             {
                  char address[MA_MAX_BUFFER_LEN+1] = {0};

                  if(MA_OK == (err = ma_booking_info_get_from_address(info, address))) {
                     ma_variant_t *from = NULL;

                     if(MA_OK == (err = ma_variant_create_from_string(address, strlen(address), &from))) {
                         err = ma_table_add_entry(info_table, MA_BOOKING_INFO_ATTR_FROM_ADDRESS, from);
                         (void)ma_variant_release(from);
                     }
                  }
             }
             {
                  char pincode[MA_MAX_LEN+1] = {0};

                  if(MA_OK == (err = ma_booking_info_get_source_pincode(info, pincode))) {
                     ma_variant_t *code = NULL;

                     if(MA_OK == (err = ma_variant_create_from_string(pincode, strlen(pincode), &code))) {
                         err = ma_table_add_entry(info_table, MA_BOOKING_INFO_ATTR_FROM_PINCODE, code);
                         (void)ma_variant_release(code);
                     }
                  }
             }
             {
                  char to[MA_MAX_BUFFER_LEN+1] = {0};

                  if(MA_OK == (err = ma_booking_info_get_to_address(info, to))) {
                     ma_variant_t *addr = NULL;

                     if(MA_OK == (err = ma_variant_create_from_string(to, strlen(to), &addr))) {
                         err = ma_table_add_entry(info_table, MA_BOOKING_INFO_ATTR_TO_ADDRESS, addr);
                         (void)ma_variant_release(addr);
                     }
                  }
             }
             {
                  char pincode[MA_MAX_LEN+1] = {0};

                  if(MA_OK == (err = ma_booking_info_get_dest_pincode(info, pincode))) {
                     ma_variant_t *code = NULL;

                     if(MA_OK == (err = ma_variant_create_from_string(pincode, strlen(pincode), &code))) {
                         err = ma_table_add_entry(info_table, MA_BOOKING_INFO_ATTR_TO_PINCODE, code);
                         (void)ma_variant_release(code);
                     }
                  }
             }
             {
                  char delivery_note[MA_MAX_BUFFER_LEN+1] = {0};

                  if(MA_OK == (err = ma_booking_info_get_delivery_note(info, delivery_note))) {
                     ma_variant_t *code = NULL;

                     if(MA_OK == (err = ma_variant_create_from_string(delivery_note, strlen(delivery_note), &code))) {
                         err = ma_table_add_entry(info_table, MA_BOOKING_INFO_ATTR_DELIVERY_NOTE, code);
                         (void)ma_variant_release(code);
                     }
                  }
             }
             {
                  ma_booking_info_user_type_t type = MA_BOOKING_INFO_USER_TYPE_NORMAL;

                  if(MA_OK == (err = ma_booking_info_get_user_type(info, &type))) {
                      ma_variant_t *t = NULL;

                      if(MA_OK == (err = ma_variant_create_from_uint32((ma_uint32_t)type, &t))) {
                          err = ma_table_add_entry(info_table, MA_BOOKING_INFO_ATTR_USER_TYPE, t);
                          (void)ma_variant_release(t);
                      }
                  }
             }
             {
                  ma_booking_info_payment_status_t type = MA_BOOKING_INFO_PAYMENT_STATUS_UNPAID;

                  if(MA_OK == (err = ma_booking_info_get_payment_status(info, &type))) {
                      ma_variant_t *t = NULL;

                      if(MA_OK == (err = ma_variant_create_from_uint32((ma_uint32_t)type, &t))) {
                          err = ma_table_add_entry(info_table, MA_BOOKING_INFO_ATTR_PAYMENT_STATUS, t);
                          (void)ma_variant_release(t);
                      }
                  }
             }
             {
                  ma_booking_info_privilege_t privi = MA_BOOKING_INFO_PRIVILEGE_LOW;

                  if(MA_OK == (err = ma_booking_info_get_privilege(info, &privi))) {
                      ma_variant_t *p = NULL;

                      if(MA_OK == (err = ma_variant_create_from_uint32(privi, &p))) {
                          err = ma_table_add_entry(info_table, MA_BOOKING_INFO_ATTR_PRIVILEGE_TYPE, p);
                          (void)ma_variant_release(p);
                      }
                  }
             }
             {
                  char pass[MA_MAX_LEN+1] = {0};
    
                  if(MA_OK == (err = ma_booking_info_get_picker_contact(info, pass))) {
                      ma_variant_t *picker_contact = NULL;

                      if(MA_OK == (err = ma_variant_create_from_string(pass, strlen(pass), &picker_contact))) {
                          err = ma_table_add_entry(info_table, MA_BOOKING_INFO_ATTR_PICKER_CONTACT, picker_contact);
                          (void)ma_variant_release(picker_contact);
                      }
                  }
             }
             {
                  ma_booking_info_status_t status = 0;

                  if(MA_OK == (err = ma_booking_info_get_status(info, &status))) {
                      ma_variant_t *p = NULL;

                      if(MA_OK == (err = ma_variant_create_from_uint32(status, &p))) {
                          err = ma_table_add_entry(info_table, MA_BOOKING_INFO_ATTR_STATUS, p);
                          (void)ma_variant_release(p);
                      }
                  }
             }

             {
                  ma_uint32_t time = 0;

                  if(MA_OK == (err = ma_booking_info_get_last_login(info, &time))) {
                      ma_variant_t *p = NULL;

                      if(MA_OK == (err = ma_variant_create_from_uint32(time, &p))) {
                          err = ma_table_add_entry(info_table, MA_BOOKING_INFO_ATTR_LAST_LOGIN, p);
                          (void)ma_variant_release(p);
                      }
                  }
             }

             {
                  char id[MA_MAX_NAME+1] = {0};
    
                  if(MA_OK == (err = ma_booking_info_get_id(info, id))) {
                      ma_variant_t *idv = NULL;

                      if(MA_OK == (err = ma_variant_create_from_string(id, strlen(id), &idv))) {
                          err = ma_table_add_entry(info_table, MA_BOOKING_INFO_ATTR_ID, idv);
                          (void)ma_variant_release(idv);
                      }
                  }
             }

             {
                  char recip[MA_MAX_NAME+1] = {0};
    
                  if(MA_OK == (err = ma_booking_info_get_recipient(info, recip))) {
                      ma_variant_t *recipv = NULL;

                      if(MA_OK == (err = ma_variant_create_from_string(recip, strlen(recip), &recipv))) {
                          err = ma_table_add_entry(info_table, MA_BOOKING_INFO_ATTR_RECIPIENT, recipv);
                          (void)ma_variant_release(recipv);
                      }
                  }
             }

             {
                  char recip[MA_MAX_NAME+1] = {0};
    
                  if(MA_OK == (err = ma_booking_info_get_recipient_contact(info, recip))) {
                      ma_variant_t *recipv = NULL;

                      if(MA_OK == (err = ma_variant_create_from_string(recip, strlen(recip), &recipv))) {
                          err = ma_table_add_entry(info_table, MA_BOOKING_INFO_ATTR_RECIPIENT_CONTACT, recipv);
                          (void)ma_variant_release(recipv);
                      }
                  }
             }

             {
                  char date[MA_MAX_NAME+1] = {0};
    
                  if(MA_OK == (err = ma_booking_info_get_pickup_date(info, date))) {
                      ma_variant_t *recipv = NULL;

                      if(MA_OK == (err = ma_variant_create_from_string(date, strlen(date), &recipv))) {
                          err = ma_table_add_entry(info_table, MA_BOOKING_INFO_ATTR_PICKUP_DATE, recipv);
                          (void)ma_variant_release(recipv);
                      }
                  }
             }
             {
                  char date[MA_MAX_NAME+1] = {0};
    
                  if(MA_OK == (err = ma_booking_info_get_deliver_date(info, date))) {
                      ma_variant_t *recipv = NULL;

                      if(MA_OK == (err = ma_variant_create_from_string(date, strlen(date), &recipv))) {
                          err = ma_table_add_entry(info_table, MA_BOOKING_INFO_ATTR_DELIVER_DATE, recipv);
                          (void)ma_variant_release(recipv);
                      }
                  }
             }
             {
                  ma_booking_info_service_type_t type = 0;

                  if(MA_OK == (err = ma_booking_info_get_service_type(info, &type))) {
                      ma_variant_t *p = NULL;

                      if(MA_OK == (err = ma_variant_create_from_uint32(type, &p))) {
                          err = ma_table_add_entry(info_table, MA_BOOKING_INFO_ATTR_SERVICE_TYPE, p);
                          (void)ma_variant_release(p);
                      }
                  }
             }
             {
                  ma_booking_info_delivery_type_t type = 0;

                  if(MA_OK == (err = ma_booking_info_get_delivery_type(info, &type))) {
                      ma_variant_t *p = NULL;

                      if(MA_OK == (err = ma_variant_create_from_uint32(type, &p))) {
                          err = ma_table_add_entry(info_table, MA_BOOKING_INFO_ATTR_DELIVERY_TYPE, p);
                          (void)ma_variant_release(p);
                      }
                  }
             }
             {
                  ma_uint32_t value = 0;

                  if(MA_OK == (err = ma_booking_info_get_price(info, &value))) {
                      ma_variant_t *p = NULL;

                      if(MA_OK == (err = ma_variant_create_from_uint32(value, &p))) {
                          err = ma_table_add_entry(info_table, MA_BOOKING_INFO_ATTR_PRICE, p);
                          (void)ma_variant_release(p);
                      }
                  }
             }
             {
                  ma_uint32_t value = 0;

                  if(MA_OK == (err = ma_booking_info_get_discount(info, &value))) {
                      ma_variant_t *p = NULL;

                      if(MA_OK == (err = ma_variant_create_from_uint32(value, &p))) {
                          err = ma_table_add_entry(info_table, MA_BOOKING_INFO_ATTR_DISCOUNT, p);
                          (void)ma_variant_release(p);
                      }
                  }
             }
             {
                  char value[MA_MAX_LEN+1] = {0};
    
                  if(MA_OK == (err = ma_booking_info_get_to_city(info, value))) {
                      ma_variant_t *v = NULL;

                      if(MA_OK == (err = ma_variant_create_from_string(value, strlen(value), &v))) {
                          err = ma_table_add_entry(info_table, MA_BOOKING_INFO_ATTR_TO_CITY, v);
                          (void)ma_variant_release(v);
                      }
                  }
             }
             {
                  char value[MA_MAX_LEN+1] = {0};
    
                  if(MA_OK == (err = ma_booking_info_get_from_city(info, value))) {
                      ma_variant_t *v = NULL;

                      if(MA_OK == (err = ma_variant_create_from_string(value, strlen(value), &v))) {
                          err = ma_table_add_entry(info_table, MA_BOOKING_INFO_ATTR_FROM_CITY, v);
                          (void)ma_variant_release(v);
                      }
                  }
             }
             {
                  char value[MA_MAX_LEN+1] = {0};
    
                  if(MA_OK == (err = ma_booking_info_get_picker_email_id(info, value))) {
                      ma_variant_t *v = NULL;

                      if(MA_OK == (err = ma_variant_create_from_string(value, strlen(value), &v))) {
                          err = ma_table_add_entry(info_table, MA_BOOKING_INFO_ATTR_PICKER_EMAIL_ID, v);
                          (void)ma_variant_release(v);
                      }
                  }
             }
             {
                  char value[MA_MAX_LEN+1] = {0};
    
                  if(MA_OK == (err = ma_booking_info_get_deliver_email_id(info, value))) {
                      ma_variant_t *v = NULL;

                      if(MA_OK == (err = ma_variant_create_from_string(value, strlen(value), &v))) {
                          err = ma_table_add_entry(info_table, MA_BOOKING_INFO_ATTR_DELIVER_EMAIL_ID, v);
                          (void)ma_variant_release(v);
                      }
                  }
             }
             {
                  char value[MA_MAX_LEN+1] = {0};
    
                  if(MA_OK == (err = ma_booking_info_get_details(info, value))) {
                      ma_variant_t *v = NULL;

                      if(MA_OK == (err = ma_variant_create_from_string(value, strlen(value), &v))) {
                          err = ma_table_add_entry(info_table, MA_BOOKING_INFO_ATTR_DETAILS, v);
                          (void)ma_variant_release(v);
                      }
                  }
             }
             {
                  char value[MA_MAX_BUFFER_LEN+1] = {0};
    
                  if(MA_OK == (err = ma_booking_info_get_transit(info, value))) {
                      ma_variant_t *v = NULL;

                      if(MA_OK == (err = ma_variant_create_from_string(value, strlen(value), &v))) {
                          err = ma_table_add_entry(info_table, MA_BOOKING_INFO_ATTR_TRANSIT, v);
                          (void)ma_variant_release(v);
                      }
                  }
             }
             {
                  ma_booking_info_payment_type_t value = 0;

                  if(MA_OK == (err = ma_booking_info_get_payment_type(info, &value))) {
                      ma_variant_t *p = NULL;

                      if(MA_OK == (err = ma_variant_create_from_uint32(value, &p))) {
                          err = ma_table_add_entry(info_table, MA_BOOKING_INFO_ATTR_PAID_TYPE, p);
                          (void)ma_variant_release(p);
                      }
                  }
             }
             {
                  ma_uint32_t value = 0;

                  if(MA_OK == (err = ma_booking_info_get_distance(info, &value))) {
                      ma_variant_t *p = NULL;

                      if(MA_OK == (err = ma_variant_create_from_uint32(value, &p))) {
                          err = ma_table_add_entry(info_table, MA_BOOKING_INFO_ATTR_DISTANCE, p);
                          (void)ma_variant_release(p);
                      }
                  }
             }
             {
                  long value = 0;

                  if(MA_OK == (err = ma_booking_info_get_pick_time(info, &value))) {
                      ma_variant_t *p = NULL;

                      if(MA_OK == (err = ma_variant_create_from_int32((ma_int32_t)value, &p))) {
                          err = ma_table_add_entry(info_table, MA_BOOKING_INFO_ATTR_PICK_TIME, p);
                          (void)ma_variant_release(p);
                      }
                  }
             }
             {
                  long value = 0;

                  if(MA_OK == (err = ma_booking_info_get_deliver_time(info, &value))) {
                      ma_variant_t *p = NULL;

                      if(MA_OK == (err = ma_variant_create_from_uint32((ma_int32_t)value, &p))) {
                          err = ma_table_add_entry(info_table, MA_BOOKING_INFO_ATTR_DELIVER_TIME, p);
                          (void)ma_variant_release(p);
                      }
                  }
             }
             {
                  ma_uint32_t value = 0;

                  if(MA_OK == (err = ma_booking_info_get_weight(info, &value))) {
                      ma_variant_t *p = NULL;

                      if(MA_OK == (err = ma_variant_create_from_uint32(value, &p))) {
                          err = ma_table_add_entry(info_table, MA_BOOKING_INFO_ATTR_WEIGHT, p);
                          (void)ma_variant_release(p);
                      }
                  }
             }
             {
                  ma_uint32_t value = 0;

                  if(MA_OK == (err = ma_booking_info_get_length(info, &value))) {
                      ma_variant_t *p = NULL;

                      if(MA_OK == (err = ma_variant_create_from_uint32(value, &p))) {
                          err = ma_table_add_entry(info_table, MA_BOOKING_INFO_ATTR_LENGTH, p);
                          (void)ma_variant_release(p);
                      }
                  }
             }
             {
                  ma_uint32_t value = 0;

                  if(MA_OK == (err = ma_booking_info_get_estimate_delivery(info, &value))) {
                      ma_variant_t *p = NULL;

                      if(MA_OK == (err = ma_variant_create_from_uint32(value, &p))) {
                          err = ma_table_add_entry(info_table, MA_BOOKING_INFO_ATTR_ESTIMATE_DELIVERY, p);
                          (void)ma_variant_release(p);
                      }
                  }
             }
             {
                  ma_booking_info_location_type_t value;

                  if(MA_OK == (err = ma_booking_info_get_location_type(info, &value))) {
                      ma_variant_t *p = NULL;

                      if(MA_OK == (err = ma_variant_create_from_uint32(value, &p))) {
                          err = ma_table_add_entry(info_table, MA_BOOKING_INFO_ATTR_LOCATION_TYPE, p);
                          (void)ma_variant_release(p);
                      }
                  }
             }
             {
                  ma_bool_t value;

                  if(MA_OK == (err = ma_booking_info_get_active(info, &value))) {
                      ma_variant_t *p = NULL;

                      if(MA_OK == (err = ma_variant_create_from_bool(value, &p))) {
                          err = ma_table_add_entry(info_table, MA_BOOKING_INFO_ATTR_ACTIVE, p);
                          (void)ma_variant_release(p);
                      }
                  }
             }
             {
                  ma_table_t *value = NULL;

                  if(MA_OK == (err = ma_booking_info_get_transitions(info, &value))) {
                      ma_variant_t *p = NULL;

                      if(MA_OK == (err = ma_variant_create_from_table(value, &p))) {
                          err = ma_table_add_entry(info_table, MA_BOOKING_INFO_ATTR_TRANSITIONS, p);
                          (void)ma_variant_release(p);
                      }
                      (void)ma_table_release(value);
                  }
             }



             err = ma_variant_create_from_table(info_table, variant);
             (void)ma_table_release(info_table);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}
ma_error_t ma_booking_info_convert_from_variant(ma_variant_t *variant, ma_booking_info_t **info) {
    if(info && variant) {
        ma_error_t err = MA_OK;

        if(MA_OK == (err = ma_booking_info_create(info, MA_BOOKING_INFO_USER_TYPE_NORMAL))) {
            ma_table_t *info_table = NULL;

            if(MA_OK == (err = ma_variant_get_table(variant, &info_table))) {
                {
                    ma_variant_t *email = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_BOOKING_INFO_ATTR_EMAIL_ID, &email))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(email, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    err = ma_booking_info_set_email_id((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(email);
                    }
                }
                {
                    ma_variant_t *dv = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_BOOKING_INFO_ATTR_DELIVERY_NOTE, &dv))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(dv, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    err = ma_booking_info_set_delivery_note((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(dv);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_BOOKING_INFO_ATTR_PACKAGE_URL, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    err = ma_booking_info_set_package_image_url((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *name = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_BOOKING_INFO_ATTR_NAME, &name))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(name, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    err = ma_booking_info_set_user((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(name);
                    }
                }
                {
                    ma_variant_t *contact = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_BOOKING_INFO_ATTR_CONTACT, &contact))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(contact, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    err = ma_booking_info_set_contact((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(contact);
                    }
                }
                {
                    ma_variant_t *address = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_BOOKING_INFO_ATTR_FROM_ADDRESS, &address))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(address, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    err = ma_booking_info_set_from_address((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(address);
                    }
                }

                {
                    ma_variant_t *pincode = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_BOOKING_INFO_ATTR_FROM_PINCODE, &pincode))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(pincode, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    err = ma_booking_info_set_source_pincode((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(pincode);
                    }
                }

                {
                    ma_variant_t *address = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_BOOKING_INFO_ATTR_TO_ADDRESS, &address))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(address, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    err = ma_booking_info_set_to_address((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(address);
                    }
                }

                {
                    ma_variant_t *pincode = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_BOOKING_INFO_ATTR_TO_PINCODE, &pincode))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(pincode, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    err = ma_booking_info_set_dest_pincode((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(pincode);
                    }
                }
                {
                    ma_variant_t *type = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_BOOKING_INFO_ATTR_USER_TYPE, &type))) {
                        ma_booking_info_user_type_t value = 0;

                        if(MA_OK == (err = ma_variant_get_uint32(type, &value)))
                            err = ma_booking_info_set_user_type((*info), value);
                        (void)ma_variant_release(type);
                    }
                }
                {
                    ma_variant_t *type = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_BOOKING_INFO_ATTR_PAYMENT_STATUS, &type))) {
                        ma_booking_info_payment_status_t value = 0;

                        if(MA_OK == (err = ma_variant_get_uint32(type, &value)))
                            err = ma_booking_info_set_payment_status((*info), value);
                        (void)ma_variant_release(type);
                    }
                }
                {
                    ma_variant_t *type = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_BOOKING_INFO_ATTR_PRIVILEGE_TYPE, &type))) {
                        ma_booking_info_privilege_t value = 0;

                        if(MA_OK == (err = ma_variant_get_uint32(type, &value)))
                            err = ma_booking_info_set_privilege((*info), value);
                        (void)ma_variant_release(type);
                    }
                }
                {
                    ma_variant_t *picker_contact = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_BOOKING_INFO_ATTR_PICKER_CONTACT, &picker_contact))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(picker_contact, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    err = ma_booking_info_set_picker_contact((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(picker_contact);
                    }
                }
                {
                    ma_variant_t *type = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_BOOKING_INFO_ATTR_STATUS, &type))) {
                        ma_booking_info_status_t value = 0;

                        if(MA_OK == (err = ma_variant_get_uint32(type, &value)))
                            err = ma_booking_info_set_status((*info), value);
                        (void)ma_variant_release(type);
                    }
                }
                {
                    ma_variant_t *type = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_BOOKING_INFO_ATTR_LAST_LOGIN, &type))) {
                        ma_booking_info_privilege_t value = 0;

                        if(MA_OK == (err = ma_variant_get_uint32(type, &value)))
                            err = ma_booking_info_set_last_login((*info), value);
                        (void)ma_variant_release(type);
                    }
                }
                {
                    ma_variant_t *id = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_BOOKING_INFO_ATTR_ID, &id))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(id, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    err = ma_booking_info_set_id((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(id);
                    }
                }
                {
                    ma_variant_t *recip = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_BOOKING_INFO_ATTR_RECIPIENT, &recip))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(recip, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    err = ma_booking_info_set_recipient((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(recip);
                    }
                }
                {
                    ma_variant_t *recip = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_BOOKING_INFO_ATTR_RECIPIENT_CONTACT, &recip))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(recip, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    err = ma_booking_info_set_recipient_contact((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(recip);
                    }
                }
                {
                    ma_variant_t *date = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_BOOKING_INFO_ATTR_PICKUP_DATE, &date))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(date, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    err = ma_booking_info_set_pickup_date((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(date);
                    }
                }

                {
                    ma_variant_t *date = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_BOOKING_INFO_ATTR_DELIVER_DATE, &date))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(date, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    err = ma_booking_info_set_deliver_date((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(date);
                    }
                }
                {
                    ma_variant_t *type = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_BOOKING_INFO_ATTR_SERVICE_TYPE, &type))) {
                        ma_booking_info_service_type_t value = 0;

                        if(MA_OK == (err = ma_variant_get_uint32(type, &value)))
                            err = ma_booking_info_set_service_type((*info), value);
                        (void)ma_variant_release(type);
                    }
                }
                {
                    ma_variant_t *type = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_BOOKING_INFO_ATTR_DELIVERY_TYPE, &type))) {
                        ma_booking_info_delivery_type_t value = 0;

                        if(MA_OK == (err = ma_variant_get_uint32(type, &value)))
                            err = ma_booking_info_set_delivery_type((*info), value);
                        (void)ma_variant_release(type);
                    }
                }

                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_BOOKING_INFO_ATTR_PRICE, &v))) {
                        ma_uint32_t value = 0;

                        if(MA_OK == (err = ma_variant_get_uint32(v, &value)))
                            err = ma_booking_info_set_price((*info), value);
                        (void)ma_variant_release(v);
                    }
                }

                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_BOOKING_INFO_ATTR_DISCOUNT, &v))) {
                        ma_uint32_t value = 0;

                        if(MA_OK == (err = ma_variant_get_uint32(v, &value)))
                            err = ma_booking_info_set_discount((*info), value);
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_BOOKING_INFO_ATTR_PICKER_EMAIL_ID, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    err = ma_booking_info_set_picker_email_id((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_BOOKING_INFO_ATTR_DELIVER_EMAIL_ID, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    err = ma_booking_info_set_deliver_email_id((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_BOOKING_INFO_ATTR_DETAILS, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    err = ma_booking_info_set_details((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_BOOKING_INFO_ATTR_TO_CITY, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    err = ma_booking_info_set_to_city((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_BOOKING_INFO_ATTR_FROM_CITY, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    err = ma_booking_info_set_from_city((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_BOOKING_INFO_ATTR_TRANSIT, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    err = ma_booking_info_set_transit((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_BOOKING_INFO_ATTR_PAID_TYPE, &v))) {
                        ma_booking_info_payment_type_t value ;

                        if(MA_OK == (err = ma_variant_get_uint32(v, &value)))
                            err = ma_booking_info_set_payment_type((*info), value);
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_BOOKING_INFO_ATTR_DISTANCE, &v))) {
                        ma_uint32_t value = 0 ;

                        if(MA_OK == (err = ma_variant_get_uint32(v, &value)))
                            err = ma_booking_info_set_distance((*info), value);
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_BOOKING_INFO_ATTR_PICK_TIME, &v))) {
                        ma_int32_t value = 0 ;

                        if(MA_OK == (err = ma_variant_get_int32(v, &value)))
                            err = ma_booking_info_set_pick_time((*info), (long)value);
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_BOOKING_INFO_ATTR_DELIVER_TIME, &v))) {
                        ma_int32_t value = 0 ;

                        if(MA_OK == (err = ma_variant_get_int32(v, &value)))
                            err = ma_booking_info_set_deliver_time((*info), (long)value);
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_BOOKING_INFO_ATTR_WEIGHT, &v))) {
                        ma_uint32_t value = 0 ;

                        if(MA_OK == (err = ma_variant_get_uint32(v, &value)))
                            err = ma_booking_info_set_weight((*info), value);
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_BOOKING_INFO_ATTR_LENGTH, &v))) {
                        ma_uint32_t value = 0 ;

                        if(MA_OK == (err = ma_variant_get_uint32(v, &value)))
                            err = ma_booking_info_set_length((*info), value);
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_BOOKING_INFO_ATTR_ESTIMATE_DELIVERY, &v))) {
                        ma_uint32_t value = 0 ;

                        if(MA_OK == (err = ma_variant_get_uint32(v, &value)))
                            err = ma_booking_info_set_estimate_delivery((*info), value);
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_BOOKING_INFO_ATTR_LOCATION_TYPE, &v))) {
                        ma_booking_info_location_type_t value ;

                        if(MA_OK == (err = ma_variant_get_uint32(v, &value)))
                            err = ma_booking_info_set_location_type((*info), value);
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_BOOKING_INFO_ATTR_ACTIVE, &v))) {
                        ma_bool_t value = MA_FALSE;

                        if(MA_OK == (err = ma_variant_get_bool(v, &value)))
                            err = ma_booking_info_set_active((*info), value);
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_BOOKING_INFO_ATTR_TRANSITIONS, &v))) {
                        ma_table_t *value ;

                        if(MA_OK == (err = ma_variant_get_table(v, &value))) {
                            err = ma_booking_info_set_transitions((*info), value);
                            (void)ma_table_release(value);
                        }
                        (void)ma_variant_release(v);
                    }
                }


                (void)ma_table_release(info_table);
            }
        }

        return MA_OK; /* intentional */
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_info_convert_from_json(const char *json_str, ma_booking_info_t **binfo) {
    if(json_str && binfo) {
        ma_error_t err = MA_ERROR_PRECONDITION;
        ma_cjson *json = ma_cjson_Parse(json_str);
        
        if(json) {
            if(MA_OK == (err = ma_booking_info_create(binfo, MA_BOOKING_INFO_USER_TYPE_NORMAL))) {
                ma_cjson *j  = NULL;
                ma_booking_info_t *info  = *binfo;

                if(ma_cjson_HasObjectItem(json, MA_BOOKING_INFO_ATTR_PICKUP_DATE)) {
                    j = ma_cjson_GetObjectItem(json, MA_BOOKING_INFO_ATTR_PICKUP_DATE);
                    if(j->valuestring) {
                        MA_LOG(booker_logger, MA_LOG_SEV_DEBUG, "pickup date (%s)", j->valuestring);
                        (void)ma_booking_info_set_pickup_date(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_BOOKING_INFO_ATTR_CONTACT)) {
                    j = ma_cjson_GetObjectItem(json, MA_BOOKING_INFO_ATTR_CONTACT);
                    if(j->valuestring) {
                        MA_LOG(booker_logger, MA_LOG_SEV_DEBUG, "contact (%s)", j->valuestring);
                        (void)ma_booking_info_set_contact(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_BOOKING_INFO_ATTR_NAME)) {
                    j = ma_cjson_GetObjectItem(json, MA_BOOKING_INFO_ATTR_NAME);
                    if(j->valuestring) {
                        MA_LOG(booker_logger, MA_LOG_SEV_DEBUG, "name (%s)", j->valuestring);
                        (void)ma_booking_info_set_user(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_BOOKING_INFO_ATTR_EMAIL_ID)) {
                    j = ma_cjson_GetObjectItem(json, MA_BOOKING_INFO_ATTR_EMAIL_ID);
                    if(j->valuestring) {
                        MA_LOG(booker_logger, MA_LOG_SEV_DEBUG, "email id (%s)", j->valuestring);
                        (void)ma_booking_info_set_email_id(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_BOOKING_INFO_ATTR_FROM_ADDRESS)) {
                    j = ma_cjson_GetObjectItem(json, MA_BOOKING_INFO_ATTR_FROM_ADDRESS);
                    if(j->valuestring) {
                        MA_LOG(booker_logger, MA_LOG_SEV_DEBUG, "from address (%s)", j->valuestring);
                        (void)ma_booking_info_set_from_address(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_BOOKING_INFO_ATTR_FROM_CITY)) {
                    j = ma_cjson_GetObjectItem(json, MA_BOOKING_INFO_ATTR_FROM_CITY);
                    if(j->valuestring) {
                        MA_LOG(booker_logger, MA_LOG_SEV_DEBUG, "from city (%s)", j->valuestring);
                        (void)ma_booking_info_set_from_city(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_BOOKING_INFO_ATTR_FROM_PINCODE)) {
                    j = ma_cjson_GetObjectItem(json, MA_BOOKING_INFO_ATTR_FROM_PINCODE);
                    if(j->valuestring) {
                        MA_LOG(booker_logger, MA_LOG_SEV_DEBUG, "from pincode (%s)", j->valuestring);
                        (void)ma_booking_info_set_source_pincode(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_BOOKING_INFO_ATTR_RECIPIENT)) {
                    j = ma_cjson_GetObjectItem(json, MA_BOOKING_INFO_ATTR_RECIPIENT);
                    if(j->valuestring) {
                        MA_LOG(booker_logger, MA_LOG_SEV_DEBUG, "recipient (%s)", j->valuestring);
                        (void)ma_booking_info_set_recipient(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_BOOKING_INFO_ATTR_RECIPIENT_CONTACT)) {
                    j = ma_cjson_GetObjectItem(json, MA_BOOKING_INFO_ATTR_RECIPIENT_CONTACT);
                    if(j->valuestring) {
                        MA_LOG(booker_logger, MA_LOG_SEV_DEBUG, "recipient contact (%s)", j->valuestring);
                        (void)ma_booking_info_set_recipient_contact(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_BOOKING_INFO_ATTR_TO_ADDRESS)) {
                    j = ma_cjson_GetObjectItem(json, MA_BOOKING_INFO_ATTR_TO_ADDRESS);
                    if(j->valuestring) {
                        MA_LOG(booker_logger, MA_LOG_SEV_DEBUG, "to address (%s)", j->valuestring);
                        (void)ma_booking_info_set_to_address(info, j->valuestring);
                        // TODO add referred by
                        //(void)ma_booking_info_set_about(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_BOOKING_INFO_ATTR_TO_PINCODE)) {
                    j = ma_cjson_GetObjectItem(json, MA_BOOKING_INFO_ATTR_TO_PINCODE);
                    if(j->valuestring) {
                        MA_LOG(booker_logger, MA_LOG_SEV_DEBUG, "to pincode (%s)", j->valuestring);
                        (void)ma_booking_info_set_dest_pincode(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_BOOKING_INFO_ATTR_TO_CITY)) {
                    j = ma_cjson_GetObjectItem(json, MA_BOOKING_INFO_ATTR_TO_CITY);
                    if(j->valuestring) {
                        MA_LOG(booker_logger, MA_LOG_SEV_DEBUG, "to city (%s)", j->valuestring);
                        (void)ma_booking_info_set_to_city(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_BOOKING_INFO_ATTR_DELIVER_EMAIL_ID)) {
                    j = ma_cjson_GetObjectItem(json, MA_BOOKING_INFO_ATTR_TO_PINCODE);
                    if(j->valuestring) {
                        MA_LOG(booker_logger, MA_LOG_SEV_DEBUG, "deliver email id (%s)", j->valuestring);
                        (void)ma_booking_info_set_deliver_email_id(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_BOOKING_INFO_ATTR_PRICE)) {
                    j = ma_cjson_GetObjectItem(json, MA_BOOKING_INFO_ATTR_PRICE);
                    if(j->valuestring) {
                        MA_LOG(booker_logger, MA_LOG_SEV_DEBUG, "cost (%s)", j->valuestring);
                        (void)ma_booking_info_set_price(info, atoi(j->valuestring));
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_BOOKING_INFO_ATTR_DISCOUNT)) {
                    j = ma_cjson_GetObjectItem(json, MA_BOOKING_INFO_ATTR_DISCOUNT);
                    if(j->valuestring) {
                        MA_LOG(booker_logger, MA_LOG_SEV_DEBUG, "discout (%s)", j->valuestring);
                        (void)ma_booking_info_set_discount(info, atoi(j->valuestring));
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_BOOKING_INFO_ATTR_DISTANCE)) {
                    j = ma_cjson_GetObjectItem(json, MA_BOOKING_INFO_ATTR_DISTANCE);
                    if(j->valuestring) {
                        MA_LOG(booker_logger, MA_LOG_SEV_DEBUG, "distance (%s)", j->valuestring);
                        (void)ma_booking_info_set_distance(info, atoi(j->valuestring));
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_BOOKING_INFO_ATTR_PAID_TYPE)) {
                    j = ma_cjson_GetObjectItem(json, MA_BOOKING_INFO_ATTR_PAID_TYPE);
                    if(j->valuestring) {
                        MA_LOG(booker_logger, MA_LOG_SEV_DEBUG, "paid type (%s)", j->valuestring);
                        (void)ma_booking_info_set_payment_from_string(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_BOOKING_INFO_ATTR_WEIGHT)) {
                    j = ma_cjson_GetObjectItem(json, MA_BOOKING_INFO_ATTR_WEIGHT);
                    if(j->valuestring) {
                        MA_LOG(booker_logger, MA_LOG_SEV_DEBUG, "weight (%s)", j->valuestring);
                        (void)ma_booking_info_set_weight(info, atoi(j->valuestring));
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_BOOKING_INFO_ATTR_LENGTH)) {
                    j = ma_cjson_GetObjectItem(json, MA_BOOKING_INFO_ATTR_LENGTH);
                    if(j->valuestring) {
                        MA_LOG(booker_logger, MA_LOG_SEV_DEBUG, "length (%s)", j->valuestring);
                        (void)ma_booking_info_set_length(info, atoi(j->valuestring));
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_BOOKING_INFO_ATTR_STATUS)) {
                    j = ma_cjson_GetObjectItem(json, MA_BOOKING_INFO_ATTR_STATUS);
                    if(j->valuestring) {
                        MA_LOG(booker_logger, MA_LOG_SEV_DEBUG, "status (%s)", j->valuestring);
                        (void)ma_booking_info_set_status_from_string(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_BOOKING_INFO_ATTR_PICK_TIME)) {
                    j = ma_cjson_GetObjectItem(json, MA_BOOKING_INFO_ATTR_PICK_TIME);
                    if(j->valuestring) {
                        MA_LOG(booker_logger, MA_LOG_SEV_DEBUG, "pick time (%s)", j->valuestring);
                        (void)ma_booking_info_set_pick_time(info, atoi(j->valuestring));
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_BOOKING_INFO_ATTR_SERVICE_TYPE)) {
                    j = ma_cjson_GetObjectItem(json, MA_BOOKING_INFO_ATTR_SERVICE_TYPE);
                    if(j->valuestring) {
                        MA_LOG(booker_logger, MA_LOG_SEV_DEBUG, "service type (%s)", j->valuestring);
                        if(!strcmp(j->valuestring, MA_BOOKING_INFO_SERVICE_TYPE_PARCEL_STR))
                            (void)ma_booking_info_set_service_type(info, MA_BOOKING_INFO_SERVICE_TYPE_PARCEL);
                        else                
                            (void)ma_booking_info_set_service_type(info, MA_BOOKING_INFO_SERVICE_TYPE_DOCUMENT);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_BOOKING_INFO_ATTR_PICKER_CONTACT)) {
                    j = ma_cjson_GetObjectItem(json, MA_BOOKING_INFO_ATTR_PICKER_CONTACT);
                    if(j->valuestring) {
                        MA_LOG(booker_logger, MA_LOG_SEV_DEBUG, "picker contact (%s)", j->valuestring);
                        (void)ma_booking_info_set_picker_contact(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_BOOKING_INFO_ATTR_TRANSIT)) {
                    j = ma_cjson_GetObjectItem(json, MA_BOOKING_INFO_ATTR_TRANSIT);
                    if(j->valuestring) {
                        MA_LOG(booker_logger, MA_LOG_SEV_DEBUG, "transit (%s)", j->valuestring);
                        (void)ma_booking_info_set_transit(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_BOOKING_INFO_ATTR_DETAILS)) {
                    j = ma_cjson_GetObjectItem(json, MA_BOOKING_INFO_ATTR_DETAILS);
                    if(j->valuestring) {
                        MA_LOG(booker_logger, MA_LOG_SEV_DEBUG, "details (%s)", j->valuestring);
                        (void)ma_booking_info_set_details(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_BOOKING_INFO_ATTR_FROM_STATE)) {
                    j = ma_cjson_GetObjectItem(json, MA_BOOKING_INFO_ATTR_FROM_STATE);
                    if(j->valuestring) {
                        MA_LOG(booker_logger, MA_LOG_SEV_DEBUG, "from state (%s)", j->valuestring);
                        (void)ma_booking_info_set_from_state(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_BOOKING_INFO_ATTR_FROM_COUNTRY)) {
                    j = ma_cjson_GetObjectItem(json, MA_BOOKING_INFO_ATTR_FROM_COUNTRY);
                    if(j->valuestring) {
                        MA_LOG(booker_logger, MA_LOG_SEV_DEBUG, "from country (%s)", j->valuestring);
                        (void)ma_booking_info_set_from_country(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_BOOKING_INFO_ATTR_TO_STATE)) {
                    j = ma_cjson_GetObjectItem(json, MA_BOOKING_INFO_ATTR_TO_STATE);
                    if(j->valuestring) {
                        MA_LOG(booker_logger, MA_LOG_SEV_DEBUG, "to state (%s)", j->valuestring);
                        (void)ma_booking_info_set_to_state(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_BOOKING_INFO_ATTR_TO_COUNTRY)) {
                    j = ma_cjson_GetObjectItem(json, MA_BOOKING_INFO_ATTR_TO_COUNTRY);
                    if(j->valuestring) {
                        MA_LOG(booker_logger, MA_LOG_SEV_DEBUG, "to country (%s)", j->valuestring);
                        (void)ma_booking_info_set_to_country(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_BOOKING_INFO_ATTR_TRANSITS)) {
                    j = ma_cjson_GetObjectItem(json, MA_BOOKING_INFO_ATTR_TRANSITS);
                    if(j->valuestring) {
                        MA_LOG(booker_logger, MA_LOG_SEV_DEBUG, "transits (%s)", j->valuestring);
                        (void)ma_booking_info_add_transits(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_BOOKING_INFO_ATTR_PACKAGE_URL)) {
                    j = ma_cjson_GetObjectItem(json, MA_BOOKING_INFO_ATTR_PACKAGE_URL);
                    if(j->valuestring) {
                        MA_LOG(booker_logger, MA_LOG_SEV_DEBUG, "package_img_url (%s)", j->valuestring);
                        (void)ma_booking_info_set_package_image_url(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_BOOKING_INFO_ATTR_DELIVERY_NOTE)) {
                    j = ma_cjson_GetObjectItem(json, MA_BOOKING_INFO_ATTR_DELIVERY_NOTE);
                    if(j->valuestring) {
                        MA_LOG(booker_logger, MA_LOG_SEV_DEBUG, "delivery note (%s)", j->valuestring);
                        (void)ma_booking_info_set_delivery_note(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_BOOKING_INFO_ATTR_PAYMENT_STATUS)) {
                    j = ma_cjson_GetObjectItem(json, MA_BOOKING_INFO_ATTR_PAYMENT_STATUS);
                    if(j->valuestring) {
                        MA_LOG(booker_logger, MA_LOG_SEV_DEBUG, "payment status (%s)", j->valuestring);
                        (void)ma_booking_info_set_payment_status_from_string(info, j->valuestring);
                    }
                }
            }
            ma_cjson_Delete(json);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static void for_each(const char *key, ma_variant_t *value, void *cb_data, ma_bool_t *stop_loop) {
    if(key && value && cb_data && stop_loop) {
        ma_json_t *info = (ma_json_t*)cb_data;
        ma_vartype_t type = MA_VARTYPE_NULL;
        char buf[MA_MAX_LEN+1] = {0};
        ma_error_t err = MA_OK;

        MA_LOG(booker_logger, MA_LOG_SEV_DEBUG, "Booking order key (%s)", key);
        (void)ma_variant_get_type(value, &type);
        if(MA_VARTYPE_UINT32 == type) {
            ma_uint32_t t = 0;

            (void)ma_variant_get_uint32(value, &t);
            MA_MSC_SELECT(_snprintf, snprintf)(buf, MA_MAX_LEN, "%u", t);
            if(!strcmp(key, MA_BOOKING_INFO_ATTR_STATUS)) {
                strcpy(buf, gbooking_status_str[t - 1]);
            } else if(!strcmp(key, MA_BOOKING_INFO_ATTR_DELIVERY_TYPE)) {
                strcpy(buf, gdelivery_type_str[t - 1]);
            } else if(!strcmp(key, MA_BOOKING_INFO_ATTR_PAID_TYPE)) {
                strcpy(buf, gpayment_mode_str[t-1]);
            } else if(!strcmp(key, MA_BOOKING_INFO_ATTR_SERVICE_TYPE)) {
                strcpy(buf, gservice_type_str[t - 1]);
            } else if(!strcmp(key, MA_BOOKING_INFO_ATTR_PAYMENT_STATUS)) {
                strcpy(buf, gpayment_status_str[t]);
            } else {
            }
            (void)ma_json_add_element(info, key, buf);
        } else if(MA_VARTYPE_INT32 == type) {
            ma_int32_t t = 0;

            (void)ma_variant_get_int32(value, &t);
            MA_MSC_SELECT(_snprintf, snprintf)(buf, MA_MAX_LEN, "%d", t);
            (void)ma_json_add_element(info, key, buf);
        } else if(MA_VARTYPE_BOOL == type) {
            ma_bool_t t = MA_FALSE;

            (void)ma_variant_get_bool(value, &t);
            MA_MSC_SELECT(_snprintf, snprintf)(buf, MA_MAX_LEN, "%d", t);
            (void)ma_json_add_element(info, key, buf);
        } else if(MA_VARTYPE_STRING == type || MA_VARTYPE_RAW == type) {
            ma_buffer_t *buf = NULL;

            if(MA_OK == ma_variant_get_string_buffer(value, &buf)) {
                const char *pstr = NULL;
                size_t len = 0;

                if(MA_OK == ma_buffer_get_string(buf, &pstr, &len)) {
                    if(pstr) {
                        (void)ma_json_add_element(info, key, pstr);
                    }
                }
                (void)ma_buffer_release(buf);
            }
        } else if(MA_VARTYPE_TABLE == type) {
            ma_table_t *table = NULL;

            if(MA_OK == ma_variant_get_table(value, &table)) {
                size_t size = 0;

                (void)ma_table_size(table, &size);
                if(size) {
                    ma_json_t *transitions  = NULL;

                    if(MA_OK == (err = ma_json_create(&transitions))) {
                        if(MA_OK == (err = ma_table_foreach(table, &for_each, transitions))) {
                            ma_bytebuffer_t *buffer = NULL;

                            ma_bytebuffer_create(MA_MAX_BUFFER_LEN, &buffer);
                            if(MA_OK == (err = ma_json_get_data(transitions, buffer))) {
                                (void)ma_json_add_object(info, key, (const char*)ma_bytebuffer_get_bytes(buffer), ma_bytebuffer_get_size(buffer));
                            } 
                        }
                        (void)ma_json_release(transitions);
                    }
                } else {
                    //(void)ma_json_add_element(info, key, "");
                }
                (void)ma_table_release(table);
            }
        }
    }
}

ma_error_t ma_booking_info_convert_to_json(ma_booking_info_t *info, ma_json_t **json) {
    if(info && json) {
        ma_error_t err = MA_OK;
        ma_variant_t *v = NULL;

        if(MA_OK == (err = ma_booking_info_convert_to_variant(info, &v))) {
            ma_table_t *table = NULL;

            if(MA_OK == (err = ma_variant_get_table(v, &table))) {
                if(MA_OK == (err = ma_table_foreach(table, &for_each, info->json))) {
                    *json = info->json;
                }
                (void)ma_table_release(table);
            }
            (void)ma_variant_release(v);
        }
        return err;
    }
    return MA_ERROR_INVALIDARG;
}
