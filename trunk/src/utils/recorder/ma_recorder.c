#include "ma/internal/utils/recorder/ma_recorder.h"
#include "ma/internal/utils/recorder/ma_recorder_info.h"
#include "ma/internal/utils/recorder/ma_recorder_datastore.h"
#include "ma/ma_msgbus.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/defs/ma_recorder_defs.h"
#include "ma/internal/ma_macros.h"

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "recorder"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <uv.h>

ma_logger_t *recorder_logger;

struct ma_recorder_s {
    char path[MA_MAX_PATH_LEN+1];
    ma_msgbus_t *msgbus;
    ma_ds_t* datastore;
};


ma_error_t ma_recorder_create(ma_msgbus_t *msgbus, ma_ds_t *datastore, const char *base_path, ma_recorder_t **recorder) {
    if(msgbus && datastore && base_path && recorder) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;

        if((*recorder = (ma_recorder_t*)calloc(1, sizeof(ma_recorder_t)))) {
            (*recorder)->msgbus = msgbus;
            strncpy((*recorder)->path, base_path, MA_MAX_PATH_LEN);
            (*recorder)->datastore = datastore;
            err = MA_OK;
            MA_LOG(recorder_logger, MA_LOG_SEV_INFO, "recorder created successfully");
        }
        if(MA_OK != err) {
            MA_LOG(recorder_logger, MA_LOG_SEV_ERROR, "recorder creation failed, last error(%d)", err);
            if(*recorder)
                (void)ma_recorder_release(*recorder);
            *recorder = NULL;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_recorder_add(ma_recorder_t *recorder, ma_recorder_info_t *info) {
    return recorder && info ? ma_recorder_datastore_write(recorder, info, recorder->datastore, recorder->path) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_recorder_add_variant(ma_recorder_t *recorder, const char *recorder_id, ma_variant_t *var) {
    return recorder && recorder_id && var ? ma_recorder_datastore_write_variant(recorder, recorder_id, recorder->datastore, recorder->path, var) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_recorder_delete(ma_recorder_t *recorder, const char *recorder_id) {
    return recorder && recorder_id ? ma_recorder_datastore_remove_recorder(recorder_id, recorder->datastore, recorder->path) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_recorder_delete_all(ma_recorder_t *recorder) {
    return recorder ? ma_recorder_datastore_clear(recorder->datastore, recorder->path) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_recorder_get(ma_recorder_t *recorder, const char *recorder_id, ma_recorder_info_t **info) {
    return recorder && recorder_id && info ? ma_recorder_datastore_get(recorder, recorder->datastore, recorder->path, recorder_id, info) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_recorder_get_all(ma_recorder_t *recorder, const char *recorder_id, ma_variant_t **var) {
    return recorder && recorder_id && var ? ma_recorder_datastore_get_all(recorder, recorder->datastore, recorder->path, recorder_id, var) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_recorder_set_logger(ma_logger_t *logger) {
    if(logger) {
        recorder_logger = logger;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_recorder_get_msgbus(ma_recorder_t *recorder, ma_msgbus_t **msgbus) {
    if(recorder && msgbus) {
        *msgbus = recorder->msgbus;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_recorder_start(ma_recorder_t *recorder) {
    if(recorder) {
        MA_LOG(recorder_logger, MA_LOG_SEV_TRACE, "recorder started");
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_recorder_stop(ma_recorder_t *recorder) {
    if(recorder) {
        MA_LOG(recorder_logger, MA_LOG_SEV_TRACE, "recorder stopped");
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_recorder_release(ma_recorder_t *recorder) {
    if(recorder) {
        free(recorder);
    }
    return MA_ERROR_INVALIDARG;
}

