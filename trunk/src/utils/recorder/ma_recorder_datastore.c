#include "ma/internal/utils/recorder/ma_recorder_datastore.h"
#include "ma/internal/defs/ma_recorder_defs.h"
#include "ma/scheduler/ma_enumerator.h"
#include "ma/internal/ma_macros.h"
#include "ma/ma_log.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "recorder"

extern ma_logger_t *recorder_logger;

#define BOOKING_DATA MA_DATASTORE_PATH_SEPARATOR "recorder_data"
#define BOOKING_ID MA_DATASTORE_PATH_SEPARATOR "recorder_id"

#define BOOKING_DATA_PATH BOOKING_DATA BOOKING_ID MA_DATASTORE_PATH_SEPARATOR/* \"recorder_data"\"recorder_id"\ */


ma_error_t ma_recorder_datastore_read(ma_recorder_t *recorder, ma_ds_t *datastore, const char *ds_path) {
    if(recorder && datastore && ds_path) {
        ma_error_t err = MA_OK;
        ma_ds_iterator_t *recorder_iterator = NULL;
        ma_buffer_t *recorder_id_buf = NULL;
        char recorder_data_base_path[MA_MAX_PATH_LEN] = {0};

        MA_MSC_SELECT(_snprintf, snprintf)(recorder_data_base_path, MA_MAX_PATH_LEN, "%s%s", ds_path, BOOKING_DATA_PATH);
        MA_LOG(recorder_logger, MA_LOG_SEV_DEBUG, "recorder data store path(%s) reading...", recorder_data_base_path);
        if(MA_OK == (err = ma_ds_iterator_create(datastore, 0, recorder_data_base_path, &recorder_iterator))) {
            while(MA_ERROR_NO_MORE_ITEMS != ma_ds_iterator_get_next(recorder_iterator, &recorder_id_buf) && recorder_id_buf) {
                const char *recorder_id = NULL;
                size_t size = 0;
                
                if(MA_OK == (err = ma_buffer_get_string(recorder_id_buf, &recorder_id, &size))) {
                    char recorder_data_path[MA_MAX_PATH_LEN] = {0};
                    ma_variant_t *recorder_var = NULL;
                    MA_MSC_SELECT(_snprintf, snprintf)(recorder_data_path, MA_MAX_PATH_LEN, "%s", recorder_data_base_path);/* base_path\"recorder_data"\"recorder_id"\4097 */
                    if(MA_OK == (err = ma_ds_get_variant(datastore, recorder_data_path, recorder_id, MA_VARTYPE_TABLE, &recorder_var))) {

                        (void)ma_variant_release(recorder_var);
                    }
                }
                (void)ma_buffer_release(recorder_id_buf);
            }
            (void)ma_ds_iterator_release(recorder_iterator);
        }

        return ((MA_ERROR_DS_PATH_NOT_FOUND == err) || (MA_ERROR_SCHEDULER_TASK_ID_CONFLICT == err)) ? MA_OK : err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_recorder_datastore_write(ma_recorder_t *recorder, ma_recorder_info_t *info, ma_ds_t *datastore, const char *ds_path) {
    if(recorder && info && datastore && ds_path) {
        char recorder_data_path[MA_MAX_PATH_LEN] = {0};
        const char *recorder_id = NULL;
        ma_error_t err = MA_OK;
        ma_variant_t *var = NULL;

        (void)ma_recorder_info_get_date(info, &recorder_id);
        MA_MSC_SELECT(_snprintf, snprintf)(recorder_data_path, MA_MAX_PATH_LEN, "%s%s", ds_path, BOOKING_DATA_PATH);
        MA_LOG(recorder_logger, MA_LOG_SEV_INFO, "recorder id(%s), data path(%s) recorder_data_path(%s)", recorder_id, ds_path, recorder_data_path);
        if(MA_OK == (err = ma_recorder_info_convert_to_variant(info, &var))) {
            if(MA_OK == (err = ma_ds_set_variant(datastore, recorder_data_path, recorder_id, var)))
                MA_LOG(recorder_logger, MA_LOG_SEV_TRACE, "recorder datastore write recorder id(%s) successful", recorder_id);
            else
                MA_LOG(recorder_logger, MA_LOG_SEV_ERROR, "recorder datastore write recorder id(%s) failed, last error(%d)", recorder_id, err);
            (void)ma_variant_release(var);
        } else
            MA_LOG(recorder_logger, MA_LOG_SEV_ERROR, "recorder datastore write recorder id(%s) conversion recorder info to variant failed, last error(%d)", recorder_id, err);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_recorder_datastore_write_variant(ma_recorder_t *recorder, const char *recorder_id, ma_ds_t *datastore, const char *ds_path, ma_variant_t *var) {
    if(recorder && recorder_id && var && datastore && ds_path) {
        char recorder_data_path[MA_MAX_PATH_LEN] = {0};
        ma_error_t err = MA_OK;

        MA_MSC_SELECT(_snprintf, snprintf)(recorder_data_path, MA_MAX_PATH_LEN, "%s%s", ds_path, BOOKING_DATA_PATH);
        MA_LOG(recorder_logger, MA_LOG_SEV_INFO, "recorder id(%s), data path(%s) recorder_data_path(%s)", recorder_id, ds_path, recorder_data_path);
        if(MA_OK == (err = ma_ds_set_variant(datastore, recorder_data_path, recorder_id, var)))
            MA_LOG(recorder_logger, MA_LOG_SEV_TRACE, "recorder datastore write recorder id(%s) successful", recorder_id);
        else
            MA_LOG(recorder_logger, MA_LOG_SEV_ERROR, "recorder datastore write recorder id(%s) failed, last error(%d)", recorder_id, err);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_recorder_datastore_remove_recorder(const char *recorder, ma_ds_t *datastore, const char *ds_path) {
    if(recorder && datastore && ds_path) {
        char recorder_path[MA_MAX_PATH_LEN] = {0};
        
        MA_LOG(recorder_logger, MA_LOG_SEV_DEBUG, "recorder removing (%s) from datastore", recorder);
        MA_MSC_SELECT(_snprintf, snprintf)(recorder_path, MA_MAX_PATH_LEN, "%s%s",ds_path, BOOKING_DATA_PATH );/* base_path\"recorder_data"\4097 */
        return ma_ds_rem(datastore, recorder_path, recorder, 1);
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_recorder_datastore_get(ma_recorder_t *recorder, ma_ds_t *datastore, const char *ds_path, const char *recorder_id, ma_recorder_info_t **info) {
    if(recorder && datastore && ds_path && recorder_id && info) {
        ma_error_t err = MA_OK;
        char recorder_data_base_path[MA_MAX_PATH_LEN] = {0};
        ma_variant_t *recorder_var = NULL;

        MA_MSC_SELECT(_snprintf, snprintf)(recorder_data_base_path, MA_MAX_PATH_LEN, "%s%s", ds_path, BOOKING_DATA_PATH);
        if(MA_OK == (err = ma_ds_get_variant(datastore, recorder_data_base_path, recorder_id, MA_VARTYPE_TABLE, &recorder_var))) {
            if(MA_OK == (err = ma_recorder_info_convert_from_variant(recorder_var, info)))
                MA_LOG(recorder_logger, MA_LOG_SEV_TRACE, "recorder(%s) exist in recorder DB", recorder_id);
            else
                MA_LOG(recorder_logger, MA_LOG_SEV_ERROR, "recorder(%s) variant to recorder conversion failed, last error(%d)", recorder_id, err);
            (void)ma_variant_release(recorder_var);
        } else 
            MA_LOG(recorder_logger, MA_LOG_SEV_ERROR, "recorder(%s) does not exist in recorder DB, last error(%d)", recorder_id, err);

        return err;
    }
    return MA_ERROR_INVALIDARG;

}

ma_error_t ma_recorder_datastore_clear(ma_ds_t *datastore, const char *ds_path) {
    return(datastore && ds_path) ? ma_ds_rem(datastore, ds_path, NULL, 1) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_recorder_datastore_get_all(ma_recorder_t *recorder, ma_ds_t *datastore, const char *ds_path, const char *recorder_id, ma_variant_t **var) {
    if(recorder && datastore && ds_path && recorder_id && var) {
        ma_error_t err = MA_OK;
        char recorder_data_base_path[MA_MAX_PATH_LEN] = {0};

        MA_MSC_SELECT(_snprintf, snprintf)(recorder_data_base_path, MA_MAX_PATH_LEN, "%s%s", ds_path, BOOKING_DATA_PATH);
        if(MA_OK == (err = ma_ds_get_variant(datastore, recorder_data_base_path, recorder_id, MA_VARTYPE_TABLE, var))) {
            MA_LOG(recorder_logger, MA_LOG_SEV_TRACE, "recorder id(%s) DB get successful", recorder_id);
        } else
            MA_LOG(recorder_logger, MA_LOG_SEV_ERROR, "recorder id(%s) does not exist in DB, last error(%d)", recorder_id, err);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}


