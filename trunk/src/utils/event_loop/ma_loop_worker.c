#include "ma/internal/utils/event_loop/ma_loop_worker.h"
#include "ma/internal/utils/event_loop/ma_uv_helpers.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/threading/ma_atomic.h"

#define LOG_FACILITY_NAME "loop_worker"

#include <uv.h>

#include <stdlib.h>
#include <assert.h>
#include <stdio.h>

#ifdef MA_WINDOWS
# include <io.h> /* _open_osfhandle */
#else
#include <uv-private/uv-unix.h> /* Why we cant's use it directly ,  make_pipe stuff */
#include <unistd.h>
#include <errno.h>
#endif

enum work_object_type {
    work_object_async = 0,

    work_object_sync
};

typedef struct work_object_s {
    unsigned            work_type; /* 0: async, 1: sync (stack based) */

    ma_work_function    work_fun;

    void                *work_data;
} work_object_t;

typedef struct sync_work_object_s {
    unsigned            work_type; /* 0: async, 1: sync (stack based) */

    ma_work_function    work_fun;

    void                *work_data;

    uv_cond_t           cond;
    uv_mutex_t          mux;

    ma_bool_t           done;

} sync_work_object_t;



enum {
    POINTER_SIZE = sizeof(work_object_t*),
    QUEUE_SIZE = 1024 /* Max # of messages in queue */
};

struct ma_loop_worker_s {
    uv_pipe_t       recv_pipe;

    uv_file         send_descriptor; /* synchronous send file descriptor */

    ma_event_loop_t *loop;

    char            in_buffer[POINTER_SIZE]; /* buffer to collect incoming bytes - we may not get a full buffer in a single read operation! */

    size_t          buf_pos; /* next available write position in in_buffer */

    ma_logger_t     *logger;

    uv_idle_t       idle_watcher; /* used during shutdown */

    ma_atomic_counter_t msg_count; /* # of pending messages */

	ma_bool_t		is_stopped;

	ma_int16_t		handle_count;
};

static void process_work(ma_loop_worker_t *self, work_object_t *work);

static void read_cb(uv_stream_t *stream, ssize_t nread, uv_buf_t buf) {
    ma_loop_worker_t *self = (ma_loop_worker_t *) stream;

    if (-1 == nread) {
        /* This is an error, the other end probably closed the connection. We should do the same */
        MA_LOG(self->logger, MA_LOG_SEV_ERROR, "error in read_cb, the other end probably closed the connection");
    } else if (0 < nread) {
        /* We actually got some data */

        assert(nread + self->buf_pos <= POINTER_SIZE);

        if (POINTER_SIZE == (self->buf_pos += nread)) {
            ma_atomic_counter_t remaining = MA_ATOMIC_DECREMENT(self->msg_count);
            process_work(self, * (work_object_t**) self->in_buffer);
            //MA_LOG(self->logger, MA_LOG_SEV_TRACE, "loop worker successfully processed a message, still %ld more to go...", (long) remaining);
            self->buf_pos = 0;
        }
    } else {
        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "No Data");
        /* no data */
    }

}

static uv_buf_t alloc_cb(uv_handle_t *handle, size_t suggested_size) {
    ma_loop_worker_t *self = (ma_loop_worker_t *) handle;
    uv_buf_t buffer;

    buffer.base = self->in_buffer + self->buf_pos;
    buffer.len = POINTER_SIZE - self->buf_pos;
    return buffer;
}

/**
 * creates uni-directional pipe pair that will be used for inter thread marshalling of message
 * This function is expected to populate
 * ma_loop_worker_t::send_descriptor (of type uv_file) and
 * ma_loop_worker_t::self->recv_pipe (of type uv_pipe_t)
 */
static ma_error_t setup_pipe(ma_loop_worker_t *self) {
    ma_error_t result;
#ifdef MA_WINDOWS
    /* Note that, although tempting, we cannot use CreatePipe API for this since it doesn't support OVERLAPPED i/o! */
    DWORD win_error;
    static const wchar_t name_template[] = L"\\\\.\\pipe\\ma_d5599bbe-4623-46a0-98a0-fa5e985813e2_%p";
    wchar_t handle_name[MAX_PIPENAME_LEN];
    HANDLE server_handle = INVALID_HANDLE_VALUE, client_handle = INVALID_HANDLE_VALUE;
    handle_name[MA_COUNTOF(handle_name)-1] = 0;
  
    for (;;) {
        _snwprintf(handle_name, MA_COUNTOF(handle_name)-1, name_template, EncodePointer(self));
        server_handle = CreateNamedPipeW(handle_name, 
            PIPE_ACCESS_DUPLEX | FILE_FLAG_FIRST_PIPE_INSTANCE | FILE_FLAG_OVERLAPPED, /* Note, although we could create the pipe with only PIPE_ACCESS_INBOUND, this will cause the uv_pipe_open() below to fail (because the underlying ) */
            PIPE_TYPE_BYTE | PIPE_READMODE_BYTE | PIPE_WAIT,
            1, /* nMaxInstances  */
            0, QUEUE_SIZE * POINTER_SIZE,
            0, /*nDefaultTimeOut, 0=50ms */
            NULL
            );

        if (INVALID_HANDLE_VALUE != server_handle) {
            /* found a good name */
            MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Created name pipe");
            break;
        }
        if (ERROR_ACCESS_DENIED != (win_error=GetLastError())) {
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to create named pipe - windows error <%d>", MA_ERROR_FROM_SYS_LAST_ERROR(win_error));
            return MA_ERROR_MSGBUS_INTERNAL;
        }        
        MA_LOG(self->logger, MA_LOG_SEV_NOTICE, "pipe name <%ls> already taken, trying another name...", (const wchar_t *) handle_name);
    }

    /* Ok, got a server, now try to connect */

    client_handle = CreateFileW( 
        handle_name,   // pipe name 
        GENERIC_WRITE, 
        0,              // no sharing 
        NULL,           // default security attributes
        OPEN_EXISTING,  // opens existing pipe 
        0,              // default attributes 
        NULL);

    if (INVALID_HANDLE_VALUE != client_handle) {
        DWORD win_error;
        if (ConnectNamedPipe(server_handle,NULL) || ERROR_PIPE_CONNECTED == (win_error=GetLastError())) {
            int server_fd = _open_osfhandle((intptr_t)server_handle, 0);
            (void) uv_pipe_init(ma_event_loop_get_uv_loop(self->loop), &self->recv_pipe, 0);
            self->recv_pipe.data = self;
            self->send_descriptor = _open_osfhandle((intptr_t)client_handle, 0);
            if ( 0 == uv_pipe_open(&self->recv_pipe, server_fd)) {
                MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Opened named pipe");
                return MA_OK;
            }            
			uv_close((uv_handle_t*)&self->recv_pipe, 0);
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to open pipe - uv error <%d> <%s>", MA_ERROR_FROM_UV_LAST_ERROR(ma_event_loop_get_uv_loop(self->loop)), UV_ERROR_STR_FROM_LAST_UV_ERROR(ma_event_loop_get_uv_loop(self->loop)));
            result = MA_ERROR_MSGBUS_INTERNAL;
            close(self->send_descriptor);
            close(server_fd);
        } else {            
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to connect named pipe - windows error <%d>", MA_ERROR_FROM_SYS_LAST_ERROR(win_error));
            result = MA_ERROR_MSGBUS_INTERNAL;
        }
        CloseHandle(client_handle);
    } else {       
        result = MA_ERROR_FROM_SYS_LAST_ERROR(win_error=GetLastError());
        MA_LOG(self->logger, MA_LOG_SEV_ERROR, "named pipe Create failed - windows error <%d>", result);
        result = MA_ERROR_MSGBUS_INTERNAL;
    }

    CloseHandle(server_handle);
#else
    int pipe_fd[2] = {0} ;
    if(uv__make_pipe(pipe_fd, UV__F_NONBLOCK)) {     
        MA_LOG(self->logger, MA_LOG_SEV_ERROR, "make pipe failed - uv error <%d> <%s>", MA_ERROR_FROM_UV_LAST_ERROR(ma_event_loop_get_uv_loop(self->loop)), UV_ERROR_STR_FROM_LAST_UV_ERROR(ma_event_loop_get_uv_loop(self->loop)));
        result = MA_ERROR_MSGBUS_INTERNAL;
    }
    else {
        (void) uv_pipe_init(ma_event_loop_get_uv_loop(self->loop), &self->recv_pipe, 0);
        self->recv_pipe.data = self;
        self->send_descriptor = pipe_fd[1]; /*Write fd */
        if (0 == uv_pipe_open(&self->recv_pipe, pipe_fd[0])) {
            MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "pipe open is succeeded");
            return MA_OK;
        }    
		uv_close((uv_handle_t*)&self->recv_pipe, 0);
        MA_LOG(self->logger, MA_LOG_SEV_ERROR, "pipe open failed - uv error <%d> <%s>", MA_ERROR_FROM_UV_LAST_ERROR(ma_event_loop_get_uv_loop(self->loop)), UV_ERROR_STR_FROM_LAST_UV_ERROR(ma_event_loop_get_uv_loop(self->loop)));
        result = MA_ERROR_MSGBUS_INTERNAL;
        close(pipe_fd[1]);
        close(pipe_fd[0]);
    }
#endif
    return result ;
}

ma_error_t ma_loop_worker_create(ma_event_loop_t *loop, ma_loop_worker_t **loop_worker) {
    if (loop && loop_worker) {
        ma_loop_worker_t *self = (ma_loop_worker_t *)calloc(1, sizeof(ma_loop_worker_t));
        if (self) {
			self->loop = loop;
			*loop_worker = self;
            return MA_OK;
        }        
        return MA_ERROR_OUTOFMEMORY;
    }    
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_loop_worker_release(ma_loop_worker_t *self) {
    if (self) { 
        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Loop worker destroys");
        free(self);
    }
    return MA_OK;
}


ma_error_t ma_loop_worker_start(ma_loop_worker_t *self) {
    if (self) {
		ma_error_t rc = setup_pipe(self);
		if (MA_OK == rc) {
			if (0 == uv_read_start((uv_stream_t*)&self->recv_pipe, &alloc_cb, &read_cb) ) {
				MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Loop worker started");
				return MA_OK;
			}
			MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Loop worker start Failed - uv error <%d> <%s>", MA_ERROR_FROM_UV_LAST_ERROR(ma_event_loop_get_uv_loop(self->loop)), UV_ERROR_STR_FROM_LAST_UV_ERROR(ma_event_loop_get_uv_loop(self->loop)));
			rc = MA_ERROR_MSGBUS_INTERNAL;
		}
		else
			MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Loop worker setup pipe Failed, rc = <%d>", rc);

		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

static void walk_cb(uv_handle_t *h, void *arg) {
    ma_loop_worker_t *self = (ma_loop_worker_t *) arg;
    const char *type;
	
	/* TBD - The below check will be removed once we identify the invalid handle in uv loop */
	if(200 == ++self->handle_count) {
		MA_LOG(self->logger, MA_LOG_SEV_DEBUG,  "Max UV handles reached");
		exit(1);
	}

    switch (h->type) {
#define X(uc, lc) case UV_##uc: type = #lc; break;
        UV_HANDLE_TYPE_MAP(X)
#undef X
default: type = "<unknown>";
    }

    MA_LOG(self->logger, MA_LOG_SEV_DEBUG,  "%c %-8s %p .data=%p\n", "A-"[!uv_is_active(h)], type, (void*)h,  h->data);

#if 0
    fprintf(stderr,
        "%c %-8s %p .data=%p\n",
        "A-"[!uv_is_active(h)],
        type,
        (void*)h,
        h->data);
#endif 
}

static void pipe_close_cb(uv_handle_t *handle) {
    ma_loop_worker_t *self = (ma_loop_worker_t *) handle->data;

	MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Loop worker pipe close cb");
    /* we pretty much expect this to be the last operation on the loop, it will be a (negative) surprise if anything is still hanging around
    * hence print it!
    */
    uv_walk(handle->loop, &walk_cb, handle->data);
}

static void idle_close_cb(uv_handle_t *handle) {
	ma_loop_worker_t *self = (ma_loop_worker_t *) handle->data;

	MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Loop worker idle close cb");
	
	uv_close((uv_handle_t*)&self->recv_pipe, pipe_close_cb);
	if (0 <= self->send_descriptor) {
		close(self->send_descriptor);
    }
}

static void idle_cb(uv_idle_t* handle, int status) {
    ma_loop_worker_t *self = (ma_loop_worker_t *) handle->data;

	/* Generlly idle cb gets executed only once uv process all the pending messages. It is workign as expected on windows.
	   But idle gets executed before processing the messages on *nix. so checking no of messages one more time to wait for completion.
	*/

	if(!self->msg_count) {
		uv_idle_stop(handle);
    
		if (0 == uv_read_stop((uv_stream_t*)&self->recv_pipe)) {
			MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Loop worker completely stopped");
		} else {
			MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Loop worker stop Failed - uv error <%d> <%s>", MA_ERROR_FROM_UV_LAST_ERROR(ma_event_loop_get_uv_loop(self->loop)), UV_ERROR_STR_FROM_LAST_UV_ERROR(ma_event_loop_get_uv_loop(self->loop)));
		}
		uv_close((uv_handle_t*)&self->idle_watcher, &idle_close_cb);
	}
	else {
		MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "# of pending messages are <%d>, so waiting for loop idle", self->msg_count);
		uv_idle_start(&self->idle_watcher,&idle_cb);
	}
}

ma_error_t ma_loop_worker_stop(ma_loop_worker_t *self) {

    /* defer closing the handle till the loop has nothing else to process (this ensures the queue will be emptied) */

    uv_idle_init(ma_event_loop_get_uv_loop(self->loop), &self->idle_watcher);
    self->idle_watcher.data = self;
    uv_idle_start(&self->idle_watcher,&idle_cb);
	
	self->is_stopped = MA_TRUE;
    return MA_OK;
}

static void process_work(ma_loop_worker_t *self, work_object_t *work) {
    switch (work->work_type) { 
        case work_object_sync: {
            sync_work_object_t *sync_work = (sync_work_object_t *) work;
            work->work_fun(work->work_data);
            uv_mutex_lock(&sync_work->mux);
            sync_work->done = MA_TRUE;
            uv_cond_signal(&sync_work->cond);
            uv_mutex_unlock(&sync_work->mux);
            return;
        }
        case work_object_async: {
            //MA_LOG(self->logger, MA_LOG_SEV_TRACE, "BEGIN loop work %p(%p) ... ", (const void *)work->work_fun, (const void *) work->work_data);
            work->work_fun(work->work_data);
            //MA_LOG(self->logger, MA_LOG_SEV_TRACE, "END loop work %p(%p) ... ", (const void *)work->work_fun, (const void *) work->work_data);
            free(work);
            return;
        }
        MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Invalid work type");
    }
    assert(!"invalid work type");
}


/* safe to call from any thread */
ma_error_t ma_loop_worker_submit_work(ma_loop_worker_t *self, ma_work_function work_fun, void *work_data, ma_bool_t wait) {
    if (!self) return MA_ERROR_INVALIDARG;

	if (self->is_stopped || self->send_descriptor < 0) {
        MA_LOG(self->logger, MA_LOG_SEV_WARNING, "cannot submit loop work after(before) loop_worker has been stopped(started)");
        return MA_ERROR_PRECONDITION;
    }

    if (!wait) {
        ma_atomic_counter_t pending = MA_ATOMIC_INCREMENT(self->msg_count);
        if (pending <= QUEUE_SIZE) {
            work_object_t *work = (work_object_t *) calloc(1, sizeof(work_object_t));
            if (work) {
                work->work_type = work_object_async;
                work->work_fun = work_fun;
                work->work_data = work_data;
                if (sizeof(work_object_t *) == write(self->send_descriptor, &work, sizeof(work_object_t *))) {
                    MA_LOG(self->logger, MA_LOG_SEV_TRACE, "scheduled an async work request, now has %ld pending work requests", (long) pending);
                    return MA_OK;
                } else {
                    MA_LOG(self->logger, MA_LOG_SEV_ERROR, "ma_loop_worker_submit_work failed writing to queue, rc=%d", (int) (errno));
                }
                free(work);
            }
        }
        MA_ATOMIC_DECREMENT(self->msg_count);
        MA_LOG(self->logger, MA_LOG_SEV_WARNING, "loop worker queue is full or write failed; # of messages in queue: %ld of %d", (long) pending, (int) QUEUE_SIZE);
        return MA_ERROR_OUTOFMEMORY;        
    } else {
        /* uses a stack based work object with a condition variable to */		        
        sync_work_object_t swork = {0}, *pswork = &swork;

        if (ma_event_loop_is_loop_thread(self->loop)) {
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "synchronous work requests cannot be submitted from i/o thread");
            assert(!"synchronous work requests cannot be submitted from i/o thread");
            return MA_ERROR_MSGBUS_INTERNAL;
        }

        swork.work_type = work_object_sync;
        swork.work_fun = work_fun;
        swork.work_data = work_data;

        if(0 == uv_mutex_init(&swork.mux)) {
            if(0 == uv_cond_init(&swork.cond)) {
                ma_atomic_counter_t pending = MA_ATOMIC_INCREMENT(self->msg_count);
                ma_error_t rc = sizeof(pswork) == write(self->send_descriptor, &pswork, sizeof(pswork))
                    ? MA_OK 
                    : (ma_error_t) errno;
                if (MA_OK == rc) {
                    MA_LOG(self->logger, MA_LOG_SEV_TRACE, "scheduled a sync work request and now waiting, now has %ld pending work requests", pending);
                    uv_mutex_lock(&swork.mux);
                    while (!swork.done) {
                        uv_cond_wait(&swork.cond, &swork.mux);
                    } 
                    uv_mutex_unlock(&swork.mux);
                    MA_LOG(self->logger, MA_LOG_SEV_TRACE, "sync work request completed");
                } else {
                    MA_ATOMIC_DECREMENT(self->msg_count);
                    MA_LOG(self->logger, MA_LOG_SEV_ERROR, "unable to schedule a sync work request, error = %ld", (long) rc);
                }

                uv_cond_destroy(&swork.cond);
                uv_mutex_destroy(&swork.mux);
                return MA_OK;
            }
            else {
                uv_mutex_destroy(&swork.mux);
                MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Cond Init Failed - uv error <%d> <%s>", MA_ERROR_FROM_UV_LAST_ERROR(ma_event_loop_get_uv_loop(self->loop)), UV_ERROR_STR_FROM_LAST_UV_ERROR(ma_event_loop_get_uv_loop(self->loop)));
                return MA_ERROR_MSGBUS_INTERNAL;
            }
        }
        else {
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Mutex Init Failed - uv error <%d> <%s>", MA_ERROR_FROM_UV_LAST_ERROR(ma_event_loop_get_uv_loop(self->loop)), UV_ERROR_STR_FROM_LAST_UV_ERROR(ma_event_loop_get_uv_loop(self->loop)));
            return MA_ERROR_MSGBUS_INTERNAL;
        }
    }
    return MA_ERROR_MSGBUS_INTERNAL;
}

ma_error_t ma_loop_worker_set_logger(ma_loop_worker_t *self, ma_logger_t *logger) {
    if(self && logger) {
        self->logger = logger;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}
