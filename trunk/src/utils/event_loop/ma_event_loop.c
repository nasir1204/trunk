#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/utils/threading/ma_thread.h"
#include "ma/ma_log.h"

#include <stdlib.h>
#include <assert.h>



struct ma_event_loop_s {
    /*! loop structure from libuv */
    uv_loop_t                       *uv_loop;

    uv_async_t                      async_watcher;

    /*! loops thread id */
    ma_thread_id_t                  thread_id;

    /*! a registered stop handler */
    ma_event_loop_stop_handler_cb   stop_cb;

    void                            *cb_data;
};

static void async_cb(uv_async_t* handle, int status);

ma_error_t ma_event_loop_create(ma_event_loop_t **loop) {
    ma_event_loop_t *self = (ma_event_loop_t *) calloc(1, sizeof(ma_event_loop_t));
    if (self) {
        self->uv_loop = uv_loop_new();

        self->thread_id = 0;

        *loop = self;
        return MA_OK;
    }
    return MA_ERROR_OUTOFMEMORY;
}

ma_error_t ma_event_loop_release(ma_event_loop_t *self) {
    if (self) {
		if(self->uv_loop) uv_loop_delete(self->uv_loop);
        free(self);
    }
    return MA_OK;
}

ma_error_t ma_event_loop_start(ma_event_loop_t *self) {
	if(self) {
		self->thread_id = ma_gettid();
		uv_async_init(self->uv_loop, &self->async_watcher, &async_cb);
		self->async_watcher.data = self;
	    uv_unref((uv_handle_t *) &self->async_watcher); /* don't let this async_watcher keep the loop alive or we will never exit */
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

/*!
 * starts event loop processing until explicitly stopped. This is a blocking call
 */
ma_error_t ma_event_loop_run(ma_event_loop_t *self) {
    if (!self) {
        return MA_ERROR_INVALIDARG;
    }

#if (9 < UV_VERSION_MINOR)
    (void) uv_run(self->uv_loop, UV_RUN_DEFAULT);
#else
    (void) uv_run(self->uv_loop);
#endif

    /* give libuv one more shot at eliminating our async_watcher residue because we closed it after loop termination */
#if (9 < UV_VERSION_MINOR)
    (void) uv_run(self->uv_loop, UV_RUN_NOWAIT);
#else
    (void) uv_run(self->uv_loop);
#endif

    return MA_OK;
}


ma_error_t ma_event_loop_stop(ma_event_loop_t *self) {
    return self 
        ? (ma_error_t) uv_async_send(&self->async_watcher) 
        : MA_ERROR_INVALIDARG;
}

ma_error_t ma_event_loop_get_loop_impl(ma_event_loop_t *self, struct uv_loop_s **uv_loop) {
    if (self && uv_loop) {
        *uv_loop = self->uv_loop;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

/*
struct uv_loop_s *ma_event_loop_get_uv_loop(ma_event_loop_t *self) {
    return self ? self->uv_loop : NULL;
}
*/
ma_error_t ma_event_loop_get_thread_id(const ma_event_loop_t *self, unsigned int *tid) {
    if (!self || !tid) {
        return MA_ERROR_INVALIDARG;
    }
    *tid = self->thread_id;
    return MA_OK;
}

ma_bool_t ma_event_loop_is_loop_thread(const ma_event_loop_t *self) {
    assert("'self' must be non-NULL" && self);
    return (ma_gettid() == self->thread_id) 
        ? MA_TRUE 
        : MA_FALSE;
}

ma_error_t ma_event_loop_register_stop_handler(ma_event_loop_t *loop, ma_event_loop_stop_handler_cb cb, void *cb_data) {
    if (loop) {
        loop->stop_cb = cb;
        loop->cb_data = cb_data;
    }
    return MA_OK;
}


static void async_cb(uv_async_t* handle, int status) {
    ma_event_loop_t *self = (ma_event_loop_t *) handle->data;
    if (self->stop_cb) {
        self->stop_cb(self, 0, self->cb_data);
    }
	uv_close( (uv_handle_t *) &self->async_watcher, 0);
}
