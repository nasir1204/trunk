#include "ma/internal/utils/spipe/ma_spipe_package.h"
#include "ma_spipe_pack.h"
#include "ma/internal/utils/threading/ma_atomic.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


struct ma_spipe_package_s {
    struct ma_spipe_header_s pkgheader;				/* Spipe pkg header. */
    struct ma_spipe_infos_s infos;					/* Holds info details. */
    struct ma_spipe_datas_s datas;					/* Holds data details. */			
    ma_crypto_t *crypto;                            /* Crypto for sign verify. */
    ma_logger_t *logger;
    ma_atomic_counter_t ref_count;
};

ma_error_t ma_spipe_package_create(ma_crypto_t *crypto, ma_spipe_package_t **spipepkg){
    if(crypto && spipepkg){		
        ma_error_t err = MA_OK;
        ma_spipe_package_t *pkgr = NULL;
        if( NULL != (pkgr = (ma_spipe_package_t*)calloc(1, sizeof(ma_spipe_package_t) ) ) &&
            MA_OK == (err = ma_table_create( &(pkgr->infos.table) ) ) &&
            MA_OK == (err = ma_table_create( &(pkgr->datas.base.table) ) ) ) {		
                pkgr->pkgheader.sig[0] = 'P';
                pkgr->pkgheader.sig[1] = 'O';
                pkgr->pkgheader.spipe_version = MA_SPIPE_VERSION6;
                pkgr->crypto = crypto;
                MA_ATOMIC_INCREMENT(pkgr->ref_count);
                *spipepkg = pkgr;
                return MA_OK;
        }
        (void)ma_spipe_package_release(pkgr);
        return MA_ERROR_OUTOFMEMORY;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_spipe_package_add_ref(ma_spipe_package_t *self) {
    if(self) {
        MA_ATOMIC_INCREMENT(self->ref_count);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_spipe_package_release(ma_spipe_package_t *self){
    if(self){
        if(0 == MA_ATOMIC_DECREMENT(self->ref_count)) {
            (void)ma_table_release(self->datas.base.table);
            (void)ma_table_release(self->infos.table);		
            free(self);
        }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_spipe_package_set_logger(ma_spipe_package_t *self, ma_logger_t *logger){
    if(self && logger){
        self->logger = logger;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_spipe_package_set_package_type(ma_spipe_package_t *self, ma_spipe_package_type_t type){
    if(self){
        self->pkgheader.package_type = type;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_spipe_package_set_header_property(ma_spipe_package_t *self, ma_spipe_package_property_t key, char *value){
    if(self && value){
        switch(key){
        case MA_SPIPE_PROPERTY_SENDER_GUID:
            strncpy((char*)self->pkgheader.sender_guid, value, MA_GUID_SIZE);
            break;
        case MA_SPIPE_PROPERTY_PACKAGE_GUID:
            strncpy((char*)self->pkgheader.package_guid, value, MA_GUID_SIZE);
            break;
        case MA_SPIPE_PROPERTY_COMPUTER_NAME:
            strncpy((char*)self->pkgheader.computer_name, value, MA_MAX_COMPUTER_NAME_SIZE);
            break;
        default:
            return MA_ERROR_INVALID_OPTION;
        }		
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}
ma_error_t ma_spipe_package_add_info(ma_spipe_package_t *self, const char *key, const unsigned char *value, size_t length){
    if(self && key && value){
        return ma_spipe_infos_add_entry(&(self->infos), key, (void*)value, length, MA_FALSE);
    }
    return MA_ERROR_INVALIDARG;
}
ma_error_t ma_spipe_package_add_data(ma_spipe_package_t *self, const char *key, void *value, size_t length){
    if(self && key && value){
        return ma_spipe_infos_add_entry( (struct ma_spipe_infos_s*)&(self->datas), key, value, length, MA_TRUE);
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_spipe_package_get_sign_object(ma_spipe_package_t *self, ma_crypto_sign_t **signer){	
    return ma_crypto_sign_create(self->crypto, self->pkgheader.package_type == 1 ? MA_CRYPTO_SIGN_SERVER_SIGNING_KEY : MA_CRYPTO_SIGN_AGENT_PRIVATE_KEY, signer);	
}

#ifndef UNIT_TEST
#define MA_SPIPE_VERIFY_KEY() MA_CRYPTO_VERIFY_SERVER_PUBLIC_KEY
#else
extern ma_crypto_verify_key_type_t MA_SPIPE_VERIFY_KEY();
#endif

static MA_INLINE ma_error_t ma_spipe_package_get_verify_object(ma_spipe_package_t *self, ma_crypto_verify_t **verifier){		
    return ma_crypto_verify_create(self->crypto, MA_SPIPE_VERIFY_KEY(), verifier);		
}

/* 
Spipepackage serialize is as below.
(1) Calculate the package size.
(2) Compress and pack the datas.
(3) Get signer.
(4) pack header and update the spipe and signer. 
(5) pack infos and update the spipe and signer.
(6) pack datas and update the spipe and signer. 
(7) Get the signature and update the spipe. 
*/
ma_error_t ma_spipe_package_serialize(ma_spipe_package_t *self, ma_stream_t *spipestream){
    if(self && spipestream){		
        ma_error_t err = MA_OK;
        ma_crypto_sign_t *signer = NULL;		
        ma_bytestream_t *bstream = NULL;
        ma_bytebuffer_t *signature = NULL;
        size_t pkg_length = 0;		
        ma_uint32_t signature_length = 0;	

        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Starting spipe package serialization.");			
        /* Calculate the package size. */
        pkg_length = MA_SPIPE_HEADER_SIZE + self->infos.binary_size + MA_SPIPE_UINT32_FIELD_SIZE /*signature size*/ + MA_MAX_SIGNATURE_SIZE;

		MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Spipe package: Header size(%u) Infos size(%u) Datas size(%u).", MA_SPIPE_HEADER_SIZE, self->infos.binary_size, self->datas.base.binary_size);			
		MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Spipe package: Infos count(%u) Datas count(%u).", self->infos.element_count, self->datas.base.element_count);			
		
        /* Compress and pack the datas.*/
        self->pkgheader.datablock_data_count = self->datas.base.element_count;
        self->pkgheader.datablock_offset = MA_SPIPE_HEADER_SIZE + self->infos.binary_size;			
        self->pkgheader.datablock_size = 0;
        if(MA_OK == (err = ma_spipe_pack_datas(&self->datas, self->logger)) ) {						
            self->pkgheader.datablock_size = ma_bytebuffer_get_size(self->datas.base.packed_buffer);			
            pkg_length += self->pkgheader.datablock_size;			
        }
		else
			MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to pack spipe data, error = %d.", err);
		
		MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Serialization spipe package length is (%u).", pkg_length);

        /* Get the signer object signer.*/
		if(MA_OK == err){
			if(MA_OK != (err = ma_spipe_package_get_sign_object(self, &signer) ) ) 
				MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to get the spipe package signer, error=%d.", err);						
		}

		if(MA_OK == err){
			if(MA_OK == (err = ma_bytestream_create(MA_LITTLE_ENDIAN,0,&bstream) ) ) {
				(void)ma_bytestream_set_stream(bstream, spipestream);
			}	
			else
				MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to create bytestream, error = %d.", err);			
		}
        /* Pack header and update the spipe and signer. */
		if(MA_OK == err){
			if (MA_OK == (err = ma_spipe_pack_header(&self->pkgheader, self->logger) ) ){
				ma_crypto_sign_data_update(signer, ma_bytebuffer_get_bytes(self->pkgheader.packedbuffer), MA_SPIPE_HEADER_SIZE);
				ma_xor_buffer_with_byte( ma_bytebuffer_get_bytes(self->pkgheader.packedbuffer), MA_SPIPE_HEADER_SIZE, 0xAA);
				(void)ma_bytestream_write_buffer(bstream, ma_bytebuffer_get_bytes(self->pkgheader.packedbuffer), MA_SPIPE_HEADER_SIZE, NULL);
				(void)ma_bytebuffer_release(self->pkgheader.packedbuffer);
				self->pkgheader.packedbuffer = NULL;

				MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Spipe stream: header written, length(%u).", ma_bytestream_get_size(bstream));
			}				
			else
				MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to pack spipe header, error = %d.", err);
		}
        /* Pack infos and update the spipe and signer. */
		if(MA_OK == err){
			if(self->infos.binary_size && MA_OK == (err = ma_spipe_pack_infos(&self->infos, self->logger) ) ){ 
				ma_crypto_sign_data_update(signer, ma_bytebuffer_get_bytes(self->infos.packed_buffer), ma_bytebuffer_get_size(self->infos.packed_buffer) );
				(void)ma_bytestream_write_buffer(bstream, ma_bytebuffer_get_bytes(self->infos.packed_buffer), ma_bytebuffer_get_size(self->infos.packed_buffer), NULL);
				(void)ma_bytebuffer_release(self->infos.packed_buffer);
				self->infos.packed_buffer = NULL;

				MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Spipe stream: infos written, length(%u).", ma_bytestream_get_size(bstream));
			}	
			else
				MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to pack spipe infos, error = %d.", err);
		}
        /* Pack datas and update the spipe and signer. */
        if(MA_OK == err){
            ma_crypto_sign_data_update(signer, ma_bytebuffer_get_bytes(self->datas.base.packed_buffer), ma_bytebuffer_get_size(self->datas.base.packed_buffer) );
            (void)ma_bytestream_write_buffer(bstream, ma_bytebuffer_get_bytes(self->datas.base.packed_buffer), ma_bytebuffer_get_size(self->datas.base.packed_buffer), NULL);

			MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Spipe stream: datas written, length(%u).", ma_bytestream_get_size(bstream));
        }
        (void)ma_bytebuffer_release(self->datas.base.packed_buffer);
        self->datas.base.packed_buffer=NULL;

        /* Get the signature and update the spipe. */
		if(MA_OK == err){
			if(MA_OK == (err = ma_bytebuffer_create(MA_MAX_SIGNATURE_SIZE,&signature) ) ) {			
				err = MA_ERROR_SPIPE_SIGN_FAILED;
				if (MA_OK == ma_crypto_sign_data_final(signer,signature) ) {
					err = MA_OK;
					signature_length = ma_bytebuffer_get_size(signature);
					(void)ma_bytestream_write_uint32(bstream, signature_length);
					(void)ma_bytestream_write_buffer(bstream, ma_bytebuffer_get_bytes(signature), signature_length, NULL);

					MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Spipe signature length (%u).", signature_length);
					MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Spipe stream: signature written, length(%u).", ma_bytestream_get_size(bstream));
				}
				else
					MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed in crypto sign final, error = %d.", err);
				(void)ma_bytebuffer_release(signature);		
			}		
			else
				MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to create signature buffer, error = %d.", err);
		}

		/* Check if everything written well in the stream. */
		if(MA_OK == err){
			MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Spipe stream: final length(%u).", ma_bytestream_get_size(bstream));

			if(ma_bytestream_get_size(bstream) !=  pkg_length){
				err = MA_ERROR_STREAM_WRITE_ERROR;
			}
		}

        ma_crypto_sign_release(signer);
        (void)ma_bytestream_release(bstream); 		
        if(MA_OK != err)
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Spipe package serialization failed, error=%d.", err);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_spipe_package_verify(ma_spipe_package_t *self, ma_crypto_verify_t *verifier, struct ma_spipe_header_s *header, ma_bytestream_t *spipestream, ma_bool_t *result){
    ma_error_t err = MA_OK;			
    ma_uint32_t signature_length = 0;
    size_t signature_length_read = 0;
    ma_bytebuffer_t *signature = NULL;	
    *result = MA_FALSE;

    MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Extracting the spipe package signature.");			
    /* get the signature.*/
    if ( MA_OK == (err = ma_bytebuffer_create(MA_MAX_SIGNATURE_SIZE,&signature) ) &&
        MA_OK == (err = ma_bytestream_seek(spipestream,header->datablock_offset + header->datablock_size) ) &&
        MA_OK == (err = ma_bytestream_read_uint32(spipestream, &signature_length) ) &&
        MA_OK == (err = ma_bytestream_read_buffer(spipestream, ma_bytebuffer_get_bytes(signature), signature_length, &signature_length_read) ) )
    {	
        
        size_t infos_datas_size = 0;
        size_t read_till_now = 0;

        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Signature retrieved successfully, signature length (%d) signature length read (%d).", signature_length, signature_length_read);			

        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Spipe verifier update, spipe package header update size (%d).", MA_SPIPE_HEADER_SIZE);			
        ma_crypto_verify_data_update(verifier, ma_bytebuffer_get_bytes(header->packedbuffer), MA_SPIPE_HEADER_SIZE);		
        /*read infos and datas and update the verifier.*/
        (void)ma_bytestream_seek(spipestream, MA_SPIPE_HEADER_SIZE);	
        
        infos_datas_size = header->datablock_offset + header->datablock_size - MA_SPIPE_HEADER_SIZE;
        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Spipe info & data block size %d", infos_datas_size);			

        while(read_till_now < infos_datas_size){
            unsigned char buffer[4096] = {0};		
            size_t read_now = 0;
            if ( MA_OK == (err = ma_bytestream_read_buffer(spipestream, buffer, (infos_datas_size - read_till_now) > 4096 ? 4096 : (infos_datas_size - read_till_now), &read_now) ) ){
                
                read_till_now += read_now;
                ma_crypto_verify_data_update(verifier, buffer, read_now);			
                MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Spipe verifier update, current update size (%d), total info&data buffer update = (%d).", read_now, read_till_now);			
            }
            else
                break;
        }
        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Spipe verifier update: total info&data buffer update (%d), total header&info&data buffer update (%d).", read_till_now, MA_SPIPE_HEADER_SIZE + read_till_now);			

        if( MA_OK == err){			
            MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Calling spipe package signature verifier final.");			

            if ( MA_OK == (err = ma_crypto_verify_data_final(verifier, ma_bytebuffer_get_bytes(signature), signature_length, result) ) && !(*result) )
                err = MA_ERROR_SPIPE_VERIFY_FAILED;
        }
    }
    (void)ma_bytebuffer_release(signature);	
    return err;
}

static void xor_with_AA(unsigned char *buffer, size_t start, size_t size) {
    size_t i = 0;
    buffer += start;
    for(i = 0; i < size; ++i)
    {
        buffer[i] ^= 0xAA;
    }
}

static ma_error_t ma_spipe_package_decrypt(ma_spipe_package_t *self, ma_bytestream_t *bstream) {
	ma_error_t err = MA_OK;
	ma_crypto_decryption_t *decrypt = NULL;

	if(MA_OK == (err = ma_crypto_decrypt_create(self->crypto, MA_CRYPTO_DECRYPTION_3DES, &decrypt))) {
		ma_bytebuffer_t *decrypted_buffer = NULL;
		if(MA_OK == (err = ma_bytebuffer_create(ma_bytestream_get_size(bstream), &decrypted_buffer))) {
			if(MA_OK == (err = ma_crypto_decrypt(decrypt, ma_bytebuffer_get_bytes(ma_bytestream_get_buffer(bstream)) + MA_SPIPE_HEADER_SIZE,
						ma_bytestream_get_size(bstream) - MA_SPIPE_HEADER_SIZE, decrypted_buffer))) {
				size_t size = 0;
				(void)ma_bytestream_seek(bstream, MA_SPIPE_HEADER_SIZE);
				(void)ma_bytestream_write_buffer(bstream, ma_bytebuffer_get_bytes(decrypted_buffer), ma_bytebuffer_get_size(decrypted_buffer), &size);
			}
			(void)ma_bytebuffer_release(decrypted_buffer);
		}
	}
	return err;
}

ma_error_t ma_spipe_package_deserialize(ma_spipe_package_t *self, ma_stream_t *spipepkgstream){
    if(self && spipepkgstream){		
        ma_bool_t  result = MA_FALSE;
        ma_error_t err = MA_OK;	
        ma_bytestream_t *bstream = NULL;
        ma_crypto_verify_t *verifier = NULL;
        size_t spipesize = 0;

        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Starting spipe package deserialization.");			
        (void)ma_stream_get_size(spipepkgstream, &spipesize);

        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Received spipe package size %d.", spipesize);			
        if (spipesize < (MA_SPIPE_HEADER_SIZE + MA_SPIPE_UINT32_FIELD_SIZE + MA_MAX_SIGNATURE_SIZE) ){
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Received spipe package size %d is lesser than the expected size, malformed package.", spipesize);			
            return MA_ERROR_SPIPE_INVALID_FORMAT;
        }

        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Creating spipe verifier");			

        /* Get the sign verification object.*/
        if( MA_OK == (err = ma_spipe_package_get_verify_object(self, &verifier) ) )
        {
            MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Created spipe verifier");			
            /* Create a bytestream for deserialize it.*/
            if(MA_OK == (err = ma_bytestream_create(MA_LITTLE_ENDIAN, 0, &bstream) ) )
            {
                (void)ma_bytestream_set_stream(bstream, spipepkgstream);			
                MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Unpacking header.");			
                /* Unpack spipe header */
                if(MA_OK == (err=ma_spipe_unpack_header(bstream, self->logger, &(self->pkgheader)) ) ) 
                {		
                    /* Handling Version 2 and 3 SPIPE packages also. It is only for spipe requests(wakeup and datachannel etc..) coming via http server
                    The below code may not hit for spipe responses packages */
                    if((MA_SPIPE_VERSION6 == self->pkgheader.spipe_version) || (MA_SPIPE_VERSION2 == self->pkgheader.spipe_version) || (MA_SPIPE_VERSION3 == self->pkgheader.spipe_version) || ((MA_SPIPE_VERSION5 == self->pkgheader.spipe_version))) {
                        ma_bool_t signature_verification = MA_FALSE;
                        ma_bool_t need_uncompress = MA_TRUE;
						if((MA_SPIPE_VERSION2 == self->pkgheader.spipe_version) || (MA_SPIPE_VERSION3 == self->pkgheader.spipe_version))
                        {		
                            need_uncompress = MA_FALSE;
                            xor_with_AA(ma_bytebuffer_get_bytes(ma_bytestream_get_buffer(bstream)), MA_SPIPE_HEADER_SIZE, spipesize - MA_SPIPE_HEADER_SIZE);
                        }

						if(MA_SPIPE_VERSION5 == self->pkgheader.spipe_version) {
							MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "decrypting package because of spipe version 5");
							if(MA_OK != (err = ma_spipe_package_decrypt(self,bstream))) 
								MA_LOG(self->logger, MA_LOG_SEV_ERROR, "failed to decrypt spipe version 5, %d", err);
						}

						if(MA_OK == err) {
							MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Verify spipe package.");			
							/* Verify the spipe signature. */					
							if(MA_OK == (err=ma_spipe_package_verify(self, verifier, &(self->pkgheader), bstream, &signature_verification)) && signature_verification )
							{
								MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Unpacking spipe datas fields.");			
								/* Unpack datas.*/
								if( MA_OK == (err=ma_spipe_unpack_datas(bstream, &(self->pkgheader), self->logger, &(self->datas), need_uncompress) ) )
								{
									MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Unpacking spipe infos fields.");			
									/* Unpack infos */
									if( MA_OK == (err=ma_spipe_unpack_infos(bstream, &(self->pkgheader), self->logger, &(self->infos)) ) )
									{
										result = MA_TRUE;
									}
								}
							}
						}
                    }else
						err = MA_ERROR_SPIPE_UNSUPPORTED_VERSION;
                }
            }		
        }	
        else{
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to get the spipe package verifier, error=%d.",err);			
        }
        (void)ma_crypto_verify_release(verifier);
        (void)ma_bytebuffer_release(self->pkgheader.packedbuffer); self->pkgheader.packedbuffer = NULL;
        (void)ma_bytestream_release(bstream);
        if(!result)
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Spipe package deserialization failed, error=%d.", err);		
        return result ? MA_OK : err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_spipe_package_get_package_type(ma_spipe_package_t *self, ma_spipe_package_type_t *package_type){
    if(self && package_type){
        *package_type = (ma_spipe_package_type_t)self->pkgheader.package_type;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_spipe_package_get_header_property(ma_spipe_package_t *self, ma_spipe_package_property_t key, const unsigned char **buffer, size_t *buffersize){
    if(self && buffer && buffersize){		
        switch(key){
        case MA_SPIPE_PROPERTY_SENDER_GUID:
            *buffer = self->pkgheader.sender_guid;
            *buffersize = MA_GUID_SIZE;
            break;
        case MA_SPIPE_PROPERTY_PACKAGE_GUID:
            *buffer = self->pkgheader.package_guid;
            *buffersize = MA_GUID_SIZE;				
            break;
        case MA_SPIPE_PROPERTY_COMPUTER_NAME:
            *buffer = self->pkgheader.computer_name;
            *buffersize = MA_MAX_COMPUTER_NAME_SIZE;								
            break;
        default:
            return MA_ERROR_INVALID_OPTION;
        }				
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_spipe_package_get_info(ma_spipe_package_t *self, const char *key, const unsigned char **value, size_t *size){
    if(self){
        return ma_spipe_infos_get_entry(&(self->infos), key, (void**)value, size, MA_FALSE);		
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_spipe_package_get_data(ma_spipe_package_t *self, const char *key, void **value, size_t *size){
    if(self){
        return ma_spipe_infos_get_entry(&(self->datas.base), key, (void**)value, size, MA_TRUE);		
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_spipe_package_get_data_element_count(ma_spipe_package_t *self, size_t *size){
	if(self && size){
		*size = self->datas.base.element_count;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;

}
