#include "ma_spipe_pack.h"
#include "ma/ma_buffer.h"
#include "ma/ma_variant.h"

#include "zlib.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


//extern ma_logger_t *ah_client_service_logger;

ma_error_t ma_xor_buffer_with_byte(unsigned char *buffer, size_t size, unsigned char byte){
    if(buffer){
        size_t i = 0;
        for(i = 0; i < size ; ++i)
            buffer[i] ^= byte;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_spipe_infos_add_entry(struct ma_spipe_infos_s *spipeinfos, const char *key, void *value, size_t size, ma_bool_t is_raw){
    size_t keysize = strlen(key);
    if( keysize > 0 && size) {
        ma_error_t err = MA_OK;
        ma_variant_t *variant = NULL;
        err = ma_table_get_value(spipeinfos->table, key, &variant);
        if(MA_ERROR_OBJECTNOTFOUND == err){
            err= (is_raw ? ma_variant_create_from_raw(value, size, &variant) : ma_variant_create_from_string((const char*)value, size, &variant) ) ;
            if(MA_OK == err && MA_OK == (err = ma_table_add_entry(spipeinfos->table, key, variant) ) ){
                spipeinfos->binary_size += MA_SPIPE_UINT32_FIELD_SIZE*2 + keysize + size;
                ++spipeinfos->element_count;
            }
        }
        ma_variant_release(variant);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_spipe_infos_get_entry(struct ma_spipe_infos_s *datas, const char *key, void **value, size_t *size, ma_bool_t is_raw){
    if(datas && key && value && size){
        ma_error_t err = MA_OK;
        ma_buffer_t *mabuffer = NULL;
        ma_variant_t *variant = NULL;
        if ( MA_OK == (err = ma_table_get_value(datas->table, key, &variant) ) &&
             MA_OK == (err = ( is_raw ? ma_variant_get_raw_buffer(variant, &mabuffer) : ma_variant_get_string_buffer(variant, &mabuffer) ) ) &&
             MA_OK == (err = ( is_raw ? ma_buffer_get_raw(mabuffer, (const unsigned char**)value, size):ma_buffer_get_string(mabuffer, (const char**)value, size) ) ) ) {
                 /*found */
        }
        ma_buffer_release(mabuffer);
        ma_variant_release(variant);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_bytebuffer_compress(ma_bytebuffer_t *uncompressedbuffer, ma_bytebuffer_t *compressedbuffer){
    ma_error_t err=MA_OK;
    size_t uncompress_size = ma_bytebuffer_get_size(uncompressedbuffer);
    size_t max_compressed_size = compressBound(uncompress_size);
    if( MA_OK == (err = ma_bytebuffer_reserve(compressedbuffer, max_compressed_size) ) ){
        unsigned long actul_compress_size = max_compressed_size;
        int zlib_code = compress(ma_bytebuffer_get_bytes(compressedbuffer), &actul_compress_size, ma_bytebuffer_get_bytes(uncompressedbuffer), ma_bytebuffer_get_size(uncompressedbuffer) );
        if (zlib_code == Z_OK){
            ma_bytebuffer_shrink(compressedbuffer, actul_compress_size);
            return MA_OK;
        }
        else
            return MA_ERROR_SPIPE_COMPRESS_FAILED;
    }
    return err;
}

static ma_error_t ma_bytebuffer_uncompress(ma_bytebuffer_t *compressedbuffer, ma_bytebuffer_t *uncompressedbuffer){
    unsigned long uncompress_size=ma_bytebuffer_get_capacity(uncompressedbuffer);
    int zlib_code = uncompress(ma_bytebuffer_get_bytes(uncompressedbuffer), &uncompress_size, ma_bytebuffer_get_bytes(compressedbuffer), ma_bytebuffer_get_size(compressedbuffer) );
    if (zlib_code == Z_OK){
        ma_bytebuffer_shrink(uncompressedbuffer, uncompress_size);
        return MA_OK;
    }
    else
        return MA_ERROR_SPIPE_UNCOMPRESS_FAILED;
}

static void dump_header(struct ma_spipe_header_s *header, ma_logger_t *logger){
    MA_LOG(logger, MA_LOG_SEV_DEBUG, "Spipe property : version = %x", header->spipe_version);
    MA_LOG(logger, MA_LOG_SEV_DEBUG, "Spipe property : package type = %s", (1 == header->package_type ? "KEY" : "DATA"));
    MA_LOG(logger, MA_LOG_SEV_DEBUG, "Spipe property : sender guid = %s", header->sender_guid);
    MA_LOG(logger, MA_LOG_SEV_DEBUG, "Spipe property : package guid = %s", header->package_guid);
    MA_LOG(logger, MA_LOG_SEV_DEBUG, "Spipe property : computer name = %s", header->computer_name);
    MA_LOG(logger, MA_LOG_SEV_DEBUG, "Spipe property : data block size = %d", header->datablock_size);
    MA_LOG(logger, MA_LOG_SEV_DEBUG, "Spipe property : data block offset = %d", header->datablock_offset);
    MA_LOG(logger, MA_LOG_SEV_DEBUG, "Spipe property : data block element count = %d", header->datablock_data_count);
}

ma_error_t ma_spipe_pack_header(struct ma_spipe_header_s *header, ma_logger_t *logger){
    ma_error_t err = MA_ERROR_SPIPE_HEADER_PACKING_FAILED;
    ma_bytestream_t *bstream = NULL;
    if( MA_OK == ma_bytestream_create(MA_LITTLE_ENDIAN, MA_SPIPE_HEADER_SIZE, &bstream)){
        ma_bytestream_write_buffer(bstream, header->sig, sizeof(header->sig), NULL);
        ma_bytestream_write_uint32(bstream, header->spipe_version);
        ma_bytestream_write_uint32(bstream, header->datablock_offset);
        ma_bytestream_write_uint32(bstream, header->datablock_data_count);
        ma_bytestream_write_uint32(bstream, header->reserved);
        ma_bytestream_write_uint32(bstream, header->datablock_size);
        ma_bytestream_write_buffer(bstream, header->sender_guid, MA_GUID_SIZE, NULL);
        ma_bytestream_write_buffer(bstream, header->package_guid, MA_GUID_SIZE, NULL);
        ma_bytestream_write_uint32(bstream, header->package_type);
        ma_bytestream_write_buffer(bstream, header->computer_name, MA_MAX_COMPUTER_NAME_SIZE, NULL);

        if( MA_SPIPE_HEADER_SIZE == ma_bytestream_get_size(bstream) ){
            header->packedbuffer = ma_bytestream_get_buffer(bstream);
            dump_header(header, logger);
            err=MA_OK;
        }

    }
    ma_bytestream_release(bstream);
    return err;
}

ma_error_t ma_spipe_unpack_header(ma_bytestream_t *spipestream, ma_logger_t *logger, struct ma_spipe_header_s *header){
    ma_error_t err = MA_ERROR_SPIPE_HEADER_UNPACKING_FAILED;
    ma_bytestream_t *bstream = NULL;
    ma_bytestream_seek(spipestream,0);
    if( MA_OK == ma_bytestream_create(MA_LITTLE_ENDIAN, MA_SPIPE_HEADER_SIZE, &bstream) ){
        size_t read = 0;
        header->packedbuffer = ma_bytestream_get_buffer(bstream);
        ma_bytestream_read_buffer(spipestream, ma_bytebuffer_get_bytes(header->packedbuffer), MA_SPIPE_HEADER_SIZE, &read);

        if(MA_SPIPE_HEADER_SIZE == read){
            ma_bytebuffer_shrink(header->packedbuffer, MA_SPIPE_HEADER_SIZE);
            ma_xor_buffer_with_byte( ma_bytebuffer_get_bytes(header->packedbuffer), MA_SPIPE_HEADER_SIZE, 0xAA);
            ma_bytestream_read_buffer(bstream, header->sig, sizeof(header->sig), NULL);
            ma_bytestream_read_uint32(bstream, &header->spipe_version);
            ma_bytestream_read_uint32(bstream, &header->datablock_offset);
            ma_bytestream_read_uint32(bstream, &header->datablock_data_count);
            ma_bytestream_read_uint32(bstream, &header->reserved);
            ma_bytestream_read_uint32(bstream, &header->datablock_size);
            ma_bytestream_read_buffer(bstream, header->sender_guid, MA_GUID_SIZE, NULL);
            ma_bytestream_read_buffer(bstream, header->package_guid, MA_GUID_SIZE, NULL);
            ma_bytestream_read_uint32(bstream, &header->package_type);
            ma_bytestream_read_buffer(bstream, header->computer_name, MA_MAX_COMPUTER_NAME_SIZE, NULL);
            dump_header(header, logger);
            err = MA_OK;
        }
    }
    ma_bytestream_release(bstream);
    return err;
}

struct spipe_info_stream_holder_s{
    void *info;
    void *stream;
    ma_logger_t *logger;
};
/* Each info serialize as : 4 bytes(size of name string) + N bytes (name string) +  4 bytes (size of value string) + N bytes (value string) */
static void ma_spipe_infos_write_cb(char const *table_key, ma_variant_t *table_value, void *cb_args, ma_bool_t *stop_loop){
    unsigned char *value = NULL;
    size_t valuesize = 0;
    struct spipe_info_stream_holder_s *holder = (struct spipe_info_stream_holder_s*)cb_args;
    struct ma_spipe_infos_s *infos = (struct ma_spipe_infos_s*)holder->info;
    ma_bytestream_t *workingstream = (ma_bytestream_t*)holder->stream;

    ma_spipe_infos_get_entry(infos, table_key, (void**)&value, &valuesize, MA_FALSE);
    MA_LOG(holder->logger, MA_LOG_SEV_DEBUG, "Spipe info : %s = %s", table_key, value);
    ma_bytestream_write_uint32(workingstream, (ma_uint32_t)strlen(table_key) );
    ma_bytestream_write_buffer(workingstream, (unsigned char*)table_key, strlen(table_key), NULL);
    ma_bytestream_write_uint32(workingstream, (ma_uint32_t)valuesize);
    ma_bytestream_write_buffer(workingstream, value, valuesize, NULL);
}

ma_error_t ma_spipe_pack_infos(struct ma_spipe_infos_s *infos, ma_logger_t *logger){
    struct spipe_info_stream_holder_s info_stream_holder;
    ma_error_t err = MA_ERROR_SPIPE_INFOBLOCK_PACKING_FAILED;
    ma_bytestream_t *bstream = NULL;
    if( MA_OK == ma_bytestream_create(MA_LITTLE_ENDIAN, infos->binary_size, &bstream)){
        info_stream_holder.info = infos;
        info_stream_holder.stream = bstream;
        info_stream_holder.logger = logger;
        ma_table_foreach(infos->table, ma_spipe_infos_write_cb, &info_stream_holder);
        if( ma_bytestream_get_size(bstream) == infos->binary_size){
            infos->packed_buffer = ma_bytestream_get_buffer(bstream);
            err = MA_OK;
        }
    }
    ma_bytestream_release(bstream);
    return err;
}

ma_error_t ma_spipe_unpack_infos(ma_bytestream_t *spipestream, const struct ma_spipe_header_s *header, ma_logger_t *logger, struct ma_spipe_infos_s *infos){
    ma_bool_t no_error = MA_TRUE;
    size_t read_tillnow = MA_SPIPE_HEADER_SIZE;
    ma_bytestream_seek(spipestream, read_tillnow);
    while(no_error && read_tillnow < header->datablock_offset){
        ma_uint32_t keylen = 0;
        ma_uint32_t valuelen = 0;
        ma_bytebuffer_t *key = NULL,*value = NULL;
        if ( MA_OK == ma_bytestream_read_uint32(spipestream, &keylen) &&
             MA_OK == ma_bytebuffer_create(keylen + 1, &key) &&
             MA_OK == ma_bytestream_read_buffer(spipestream, ma_bytebuffer_get_bytes(key), keylen, NULL) &&
             MA_OK == ma_bytestream_read_uint32(spipestream, &valuelen) &&
             MA_OK == ma_bytebuffer_create(valuelen+1, &value) &&
             MA_OK == ma_bytestream_read_buffer(spipestream, ma_bytebuffer_get_bytes(value), valuelen, NULL) &&
             MA_OK == ma_spipe_infos_add_entry(infos, (char*)ma_bytebuffer_get_bytes(key), (void*)ma_bytebuffer_get_bytes(value), valuelen, MA_FALSE) ){
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "Spipe info : %s = %s", (char*)ma_bytebuffer_get_bytes(key), (char*)ma_bytebuffer_get_bytes(value));
                read_tillnow += MA_SPIPE_UINT32_FIELD_SIZE + MA_SPIPE_UINT32_FIELD_SIZE + keylen+valuelen;
        }
        else{
            no_error = MA_FALSE;
        }
        ma_bytebuffer_release(key);
        ma_bytebuffer_release(value);
    }
    return (no_error && read_tillnow == header->datablock_offset ? MA_OK : MA_ERROR_SPIPE_INFOBLOCK_UNPACKING_FAILED);
}

/* Each data serialize as : 2 bytes(index number) + 2 bytes (size of key) + N byte (key string) + 4 byte (size of value buffer) + N bytes (value buffer) */
static void ma_spipe_datas_write_cb(char const *table_key, ma_variant_t *table_value, void *cb_args, ma_bool_t *stop_loop){
    unsigned char *value = NULL;
    size_t valuesize = 0;
    struct spipe_info_stream_holder_s *holder = (struct spipe_info_stream_holder_s*)cb_args;
    struct ma_spipe_datas_s *datas = (struct ma_spipe_datas_s*)holder->info;
    ma_bytestream_t *workingstream = (ma_bytestream_t*)holder->stream;

    ma_spipe_infos_get_entry((struct ma_spipe_infos_s*)datas, table_key, (void**)&value, &valuesize, MA_TRUE);
    MA_LOG(holder->logger, MA_LOG_SEV_DEBUG, "Spipe data : %s", table_key);

    ma_bytestream_write_uint16(workingstream, datas->packing_index);
    ma_bytestream_write_uint16(workingstream, (ma_uint16_t)strlen(table_key));
    ma_bytestream_write_buffer(workingstream, (unsigned char*)table_key, strlen(table_key), NULL);
    ma_bytestream_write_uint32(workingstream, (ma_uint32_t)valuesize);
    ma_bytestream_write_buffer(workingstream, (unsigned char*)value, valuesize, NULL);
    datas->packing_index++;
}

static ma_error_t ma_spipe_pack_and_compress_datas(struct ma_spipe_datas_s *datas, ma_logger_t *logger, ma_bytebuffer_t *compressed_data){
    ma_error_t  err = MA_OK;
    ma_bytestream_t *bdatastream = NULL;
    ma_bytebuffer_t *uncompressed_data = NULL;
    struct spipe_info_stream_holder_s info_stream_holder;
    datas->packing_index = 1;
    if(0 == datas->base.binary_size)
        return MA_OK;
    if ( MA_OK == (err=ma_bytestream_create(MA_LITTLE_ENDIAN, datas->base.binary_size, &bdatastream) ) ){
        info_stream_holder.info = datas;
        info_stream_holder.stream = bdatastream;
        info_stream_holder.logger = logger;
        err = MA_ERROR_SPIPE_DATABLOCK_PACKING_FAILED;
        ma_table_foreach(datas->base.table, ma_spipe_datas_write_cb, &info_stream_holder);
        if(	datas->base.binary_size == ma_bytestream_get_size(bdatastream) ){
            uncompressed_data = ma_bytestream_get_buffer(bdatastream);
            err = ma_bytebuffer_compress(uncompressed_data, compressed_data);
        }
    }
    ma_bytebuffer_release(uncompressed_data);
    ma_bytestream_release(bdatastream);
    return err;
}

ma_error_t ma_spipe_pack_datas(struct ma_spipe_datas_s *datas, ma_logger_t *logger){
    ma_error_t err = MA_OK;
    ma_bytestream_t *bstream = NULL;
    ma_bytebuffer_t *compress_data = NULL;
    if( MA_OK == (err = ma_bytebuffer_create(MA_SPIPE_UINT32_FIELD_SIZE*2, &compress_data)) &&
        MA_OK == (err = ma_spipe_pack_and_compress_datas(datas, logger, compress_data)) &&
        MA_OK == (err = ma_bytestream_create(MA_LITTLE_ENDIAN, MA_SPIPE_UINT32_FIELD_SIZE*2 + ma_bytebuffer_get_size(compress_data), &bstream) ) ){
        err = MA_ERROR_SPIPE_DATABLOCK_PACKING_FAILED;
        if(0 != datas->base.binary_size){
            ma_bytestream_write_uint32(bstream, datas->base.binary_size);
            ma_bytestream_write_uint32(bstream, ma_bytebuffer_get_size(compress_data) );
            ma_bytestream_write_buffer(bstream, ma_bytebuffer_get_bytes(compress_data), ma_bytebuffer_get_size(compress_data), NULL);
            if( ma_bytestream_get_size(bstream) == ( MA_SPIPE_UINT32_FIELD_SIZE*2 + ma_bytebuffer_get_size(compress_data) ) ){
                datas->base.packed_buffer = ma_bytestream_get_buffer(bstream);
                err = MA_OK;
            }
        }
        else{ /*If no data is attached to spipe then we are setting this in the data section too.*/
            ma_bytestream_write_uint32(bstream, 0);
            ma_bytestream_write_uint32(bstream, 0);		
            datas->base.packed_buffer = ma_bytestream_get_buffer(bstream);
            err = MA_OK;
        }
    }
    ma_bytebuffer_release(compress_data);
    ma_bytestream_release(bstream);
    return err;
}

ma_error_t ma_spipe_unpack_datas(ma_bytestream_t *spipestream, const struct ma_spipe_header_s *header, ma_logger_t *logger, struct ma_spipe_datas_s *datas, ma_bool_t need_uncompress){
    ma_error_t err = MA_OK;
    ma_uint32_t uncompress_size = 0;
    ma_uint32_t compress_size = 0;
    size_t read_tillnow = 0;
    ma_bytestream_t *uncompressed_data_bstream = NULL;
    ma_bytebuffer_t *compressed_datablock = NULL;
    if(header->datablock_size <= MA_SPIPE_UINT32_FIELD_SIZE*2)
        return MA_OK;

    ma_bytestream_seek(spipestream, header->datablock_offset);

    if(need_uncompress) {
        /*get the uncompressed data from spipe.*/
        if ( MA_OK == (err = ma_bytestream_read_uint32(spipestream, &uncompress_size) ) &&
             MA_OK == (err = ma_bytestream_read_uint32(spipestream, &compress_size) ) &&
             MA_OK == (err = ma_bytebuffer_create(compress_size, &compressed_datablock)) &&
             MA_OK == (err = ma_bytestream_read_buffer(spipestream, ma_bytebuffer_get_bytes(compressed_datablock), compress_size, NULL)) ) {
            ma_bytebuffer_t *uncompress_buffer = NULL;
            ma_bytebuffer_shrink(compressed_datablock, compress_size);
            /*uncompressed the data.*/
            if( MA_OK == (err = ma_bytestream_create(MA_LITTLE_ENDIAN, uncompress_size, &uncompressed_data_bstream)) ){
                uncompress_buffer = ma_bytestream_get_buffer(uncompressed_data_bstream);
                err = ma_bytebuffer_uncompress(compressed_datablock, uncompress_buffer);
            }
            ma_bytebuffer_release(uncompress_buffer);
        }
        ma_bytebuffer_release(compressed_datablock);
    }
    else {
        uncompressed_data_bstream = spipestream;
    }

    /*unpack the datas and populate the spipepackage.*/
    read_tillnow = 0;
    while( MA_OK == err && read_tillnow < uncompress_size) {
        ma_uint16_t index = 0;
        ma_uint16_t keylen = 0;
        ma_uint32_t valuelen = 0;
        ma_bytebuffer_t *key = NULL,*value = NULL;
        if ( MA_OK == ma_bytestream_read_uint16(uncompressed_data_bstream, &index) &&
             MA_OK == ma_bytestream_read_uint16(uncompressed_data_bstream, &keylen) &&
             MA_OK == ma_bytebuffer_create(keylen+1, &key) &&
             MA_OK == ma_bytestream_read_buffer(uncompressed_data_bstream, ma_bytebuffer_get_bytes(key), keylen, NULL) &&
             MA_OK == ma_bytestream_read_uint32(uncompressed_data_bstream, &valuelen) &&
             MA_OK == ma_bytebuffer_create(valuelen, &value) &&
             MA_OK == ma_bytestream_read_buffer(uncompressed_data_bstream, ma_bytebuffer_get_bytes(value), valuelen, NULL) &&
             MA_OK == ma_spipe_infos_add_entry(&(datas->base), (char*)ma_bytebuffer_get_bytes(key), (void*)ma_bytebuffer_get_bytes(value), valuelen, MA_TRUE) ) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "Spipe data : %s", (char*)ma_bytebuffer_get_bytes(key));
                read_tillnow += MA_SPIPE_UINT16_FILED_SIZE + MA_SPIPE_UINT16_FILED_SIZE + MA_SPIPE_UINT32_FIELD_SIZE + keylen + valuelen;
        }
        else{
            err = MA_ERROR_SPIPE_DATABLOCK_UNPACKING_FAILED;
        }
        ma_bytebuffer_release(key);
        ma_bytebuffer_release(value);
    }
    if(read_tillnow != uncompress_size)
        err = MA_ERROR_SPIPE_DATABLOCK_UNPACKING_FAILED;
    ma_bytestream_release(uncompressed_data_bstream);
    return err;
}


