#ifndef MA_SPIPE_PACK_H_INCLUDED
#define MA_SPIPE_PACK_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_variant.h"
#include "ma/logger/ma_logger.h"
#include "ma/internal/utils/datastructures/ma_stream.h"
#include "ma/internal/utils/datastructures/ma_bytebuffer.h"
#include "ma/internal/utils/datastructures/ma_bytestream.h"

#define LOG_FACILITY_NAME "spipe"

#define MA_GUID_SIZE				64
#define MA_MAX_COMPUTER_NAME_SIZE   80
#define MA_MAX_SIGNATURE_SIZE		256
#define MA_SPIPE_HEADER_SIZE        234

#define MA_SPIPE_UINT16_FILED_SIZE  2
#define MA_SPIPE_UINT32_FIELD_SIZE  4

MA_CPP(extern "C" {)

struct ma_spipe_infos_s {
    ma_uint32_t element_count;
    ma_uint32_t binary_size;	
    ma_table_t *table;		
    ma_bytebuffer_t *packed_buffer;	/* after packing */	
};

struct ma_spipe_datas_s {
    struct ma_spipe_infos_s base;	
    ma_uint16_t packing_index;	 
};

/*
Spipe header format & size.
 2    // signature 
 + 4  // spipe version 
 + 4  // offset to data block 
 + 4  // data count 
 + 4  // reserved 
 + 4  // data block length 
 + MA_GUID_SIZE // sender GUID 
 + MA_GUID_SIZE // package GUID 
 + 4  // package type 
 + MA_MAX_COMPUTER_NAME_SIZE // sender computer name  
 */
struct ma_spipe_header_s{
    unsigned char sig[2];
    ma_uint32_t spipe_version;
    ma_uint32_t datablock_offset;
    ma_uint32_t datablock_data_count;
    ma_uint32_t reserved;
    ma_uint32_t datablock_size;
    unsigned char sender_guid[MA_GUID_SIZE+1];
    unsigned char package_guid[MA_GUID_SIZE+1];
    ma_uint32_t package_type;
    unsigned char computer_name[MA_MAX_COMPUTER_NAME_SIZE+1];	
    ma_bytebuffer_t *packedbuffer;	/*after packing.*/
};

ma_error_t ma_xor_buffer_with_byte(unsigned char *buffer, size_t size, unsigned char byte);	
ma_error_t ma_spipe_infos_add_entry(struct ma_spipe_infos_s *table, const char *key, void *value, size_t size, ma_bool_t is_raw);
ma_error_t ma_spipe_infos_get_entry(struct ma_spipe_infos_s *datas, const char *key, void **value, size_t *size, ma_bool_t is_raw);

/* helper function for header packing/unpacking */
ma_error_t ma_spipe_pack_header(struct ma_spipe_header_s *header, ma_logger_t *logger);	
ma_error_t ma_spipe_unpack_header(ma_bytestream_t *spipepkg, ma_logger_t *logger, struct ma_spipe_header_s *header);

/* helper function for infos packing/unpacking */
ma_error_t ma_spipe_pack_infos(struct ma_spipe_infos_s *infos, ma_logger_t *logger);
ma_error_t ma_spipe_unpack_infos(ma_bytestream_t *spipepkg, const struct ma_spipe_header_s *header, ma_logger_t *logger, struct ma_spipe_infos_s *infos);

/* helper function for datas packing/unpacking */
ma_error_t ma_spipe_pack_datas(struct ma_spipe_datas_s *datas, ma_logger_t *logger);
ma_error_t ma_spipe_unpack_datas(ma_bytestream_t *spipepkg, const struct ma_spipe_header_s *header, ma_logger_t *logger, struct ma_spipe_datas_s *datas, ma_bool_t need_uncompress);


MA_CPP(})

#endif /* MA_SPIPE_PACK_H_INCLUDED */
