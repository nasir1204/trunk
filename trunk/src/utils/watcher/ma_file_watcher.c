#include "ma/internal/utils/watcher/ma_file_watcher.h"
#include "ma/internal/utils/datastructures/ma_slist.h"

#include "ma/internal/ma_macros.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef __linux__
#include <sys/utsname.h>
#endif

#define FILE_WATCHER_TIMEOUT    30 * 1000 /* 30 second interval */

typedef struct ma_file_watcher_item_s ma_file_watcher_item_t;

struct ma_file_watcher_item_s {
	/*next link*/
	MA_SLIST_NODE_DEFINE(ma_file_watcher_item_t);
	ma_watcher_t			*watcher;
	char					*name;
	ma_watcher_callback_t	cb;
	void					*cb_data;
	uv_fs_event_t 			*fs_handle;
    uv_timer_t 			    *timer_handle;
    double                  file_mtime;
	ma_bool_t				is_recursive_dir;
};

typedef struct ma_file_watcher_s {
	ma_watcher_t	watcher;

    MA_SLIST_DEFINE(watcher_list, ma_file_watcher_item_t);
} ma_file_watcher_t;

static ma_error_t file_watcher_add(ma_watcher_t *self, const char *name, ma_watcher_callback_t cb, ma_bool_t option, void *cb_data);
static ma_error_t file_watcher_delete(ma_watcher_t *self, const char *name);
static ma_error_t file_watcher_release(ma_watcher_t *self);

static const ma_watcher_methods_t methods = {
    &file_watcher_add,
    &file_watcher_delete,
    &file_watcher_release
};

static ma_bool_t is_valid_file(uv_loop_t *uv_loop, const char *name) {
	ma_bool_t b_rc = MA_FALSE;
    if(uv_loop && name) {
        uv_fs_t req = {0};
        b_rc = !uv_fs_stat(uv_loop, &req, name, NULL) && !req.result;
        uv_fs_req_cleanup(&req);
    }
    return b_rc;
}

ma_error_t ma_file_watcher_create(ma_event_loop_t *ma_loop, ma_watcher_t **watcher) {
	if(ma_loop && watcher) {
		ma_error_t rc = MA_OK;
		ma_file_watcher_t *self = NULL;
		self = (ma_file_watcher_t *)calloc(1, sizeof(ma_file_watcher_t));
		if(self) {
			ma_watcher_init((ma_watcher_t *)self, &methods, ma_loop, self);
			MA_SLIST_INIT(self, watcher_list);
			*watcher = (ma_watcher_t *)self;
			return rc;
		}
		return MA_ERROR_OUTOFMEMORY;
	}
	return MA_ERROR_INVALIDARG;
}

static void fs_event_cb(uv_fs_event_t* handle, const char* filename, int events, int status) {
	ma_file_watcher_item_t *item = (ma_file_watcher_item_t *) handle->data ;

	if(item && item->cb)
#ifdef MA_WINDOWS
		item->cb((ma_watcher_t *)item->watcher, filename, item->cb_data);
#else
		item->cb((ma_watcher_t *)item->watcher, item->name, item->cb_data);
#endif
}

static void timer_cb(uv_timer_t* handle, int status) {
	ma_file_watcher_item_t *item = (ma_file_watcher_item_t *) handle->data ;
    if(item) {
        uv_fs_t req ;
        ma_bool_t is_file_modified = MA_FALSE;
        if(!uv_fs_stat(ma_event_loop_get_uv_loop(item->watcher->ma_loop), &req, item->name, NULL)) {
            is_file_modified = (item->file_mtime != req.mtime);
            item->file_mtime = req.mtime;
            uv_fs_req_cleanup(&req) ;
	    }
      
	    if(is_file_modified && item->cb)
		    item->cb((ma_watcher_t *)item->watcher, item->name, item->cb_data);
    }
}

/* Fs event doesn't support on Linux which has kernel version less than 2.6.13 */
static ma_bool_t has_fs_event_support() {
    ma_bool_t b_rc = MA_TRUE;
#ifdef __linux__   
    struct utsname buf;
    b_rc = MA_FALSE;
    if(!uname(&buf)) {
        char *p = NULL, *v = NULL;
        int maj =0, min = 0, build = 0;
        if(p = strtok(buf.release, " ")) {
            if(v = strtok(p, ".")) {
                maj = atoi(v);
                if(v = strtok(NULL, ".")) {
                    min = atoi(v);
                    if(v = strtok(NULL, ".")) {
                        build = atoi(v);
                        b_rc = (maj > 2) || 
                               ((maj == 2) && (min > 6)) || 
                               ((maj == 2) && (min == 6) && (build >= 13));
                    }
                }
            }
        }
    }
#endif  /* __linux__ */
   
#if defined(MA_PLATFORM_AIX) || defined(MA_PLATFORM_SOLARIS)
b_rc = MA_FALSE;
#endif
	return b_rc;
}

static ma_error_t watcher_start(ma_file_watcher_item_t *item) {
	ma_error_t rc = MA_OK;
    if(has_fs_event_support()) {
        item->fs_handle = (uv_fs_event_t *)calloc(1 , sizeof(uv_fs_event_t));
	    if(item->fs_handle) {
		    item->fs_handle->data = item;
		    if(uv_fs_event_init(ma_event_loop_get_uv_loop(item->watcher->ma_loop), item->fs_handle, item->name, fs_event_cb, item->is_recursive_dir ? UV_FS_EVENT_RECURSIVE : 0))
			    rc = MA_ERROR_UNEXPECTED;           
	    }
	    else
		    rc = MA_ERROR_OUTOFMEMORY;
    }
    else {
        item->timer_handle = (uv_timer_t *)calloc(1 , sizeof(uv_timer_t));
	    if(item->timer_handle) {
            item->timer_handle->data = item;
            if(uv_timer_init(ma_event_loop_get_uv_loop(item->watcher->ma_loop), item->timer_handle)) 
			    rc = MA_ERROR_UNEXPECTED;
            else
                uv_timer_start(item->timer_handle, timer_cb, FILE_WATCHER_TIMEOUT, FILE_WATCHER_TIMEOUT);
	    }
	    else
		    rc = MA_ERROR_OUTOFMEMORY;
    }
	return rc;
}

ma_error_t file_watcher_add(ma_watcher_t *watcher, const char *name, ma_watcher_callback_t cb, ma_bool_t option, void *cb_data) {
	if(watcher && name && cb) {
		ma_error_t rc = MA_OK;
		ma_file_watcher_item_t *item = NULL;
		ma_file_watcher_t *self = (ma_file_watcher_t *)watcher;

		if(!is_valid_file(ma_event_loop_get_uv_loop(watcher->ma_loop), name))
			return MA_ERROR_INVALIDARG;

		item = (ma_file_watcher_item_t *)calloc(1,sizeof(ma_file_watcher_item_t));

        if(!item)
			return MA_ERROR_OUTOFMEMORY;

        item->name = strdup(name);
		item->cb = cb;
		item->cb_data = cb_data;
		item->watcher = (ma_watcher_t *)self;
		item->is_recursive_dir = option;
		if(MA_OK != (rc = watcher_start(item))) {
			free(item->name); item->name = NULL;
		}
		else
			MA_SLIST_PUSH_BACK(self, watcher_list, item) ;
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}


static void close_cb(uv_handle_t* handle) {
	if(handle) free(handle);
}

static ma_error_t ma_watcher_item_release(ma_file_watcher_item_t *item) {
    if(item) {
		ma_error_t rc = MA_OK;
        if(item->fs_handle) 
            uv_close((uv_handle_t*)item->fs_handle, close_cb);
        else {
			if(item->timer_handle) {
	            uv_timer_stop(item->timer_handle);
    	        uv_close((uv_handle_t*)item->timer_handle, close_cb);
			}
        }

		if(item->name) free(item->name);
		free(item);
		return rc;
    }
	return MA_ERROR_INVALIDARG;
}

ma_error_t file_watcher_delete(ma_watcher_t *self, const char *name) {
	if(self && name) {
		ma_error_t rc = MA_OK;
		ma_file_watcher_item_t *item = NULL ;
		MA_SLIST_FOREACH((ma_file_watcher_t *)self, watcher_list, item) {
			if(!strcmp(name,item->name)) {
				MA_SLIST_REMOVE_NODE((ma_file_watcher_t *)self, watcher_list, item, ma_file_watcher_item_t) ;
				ma_watcher_item_release(item);
				break ;
			}
		}
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t file_watcher_release(ma_watcher_t *self) {
	if(self) {
		ma_error_t rc = MA_OK;
		MA_SLIST_CLEAR((ma_file_watcher_t *)self, watcher_list, ma_file_watcher_item_t, ma_watcher_item_release) ;
		free(self);
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

