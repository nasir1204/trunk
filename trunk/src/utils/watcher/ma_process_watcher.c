#include "ma/internal/utils/watcher/ma_process_watcher.h"
#include "ma/internal/utils/datastructures/ma_slist.h"
#include "ma/internal/utils/text/ma_utf8.h"
#include "ma/internal/utils/platform/ma_sys_apis.h"

#include "ma/internal/ma_macros.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct ma_procsess_watcher_item_s ma_procsess_watcher_item_t;

struct ma_procsess_watcher_item_s {
	/*next link*/
	MA_SLIST_NODE_DEFINE(ma_procsess_watcher_item_t);
	ma_watcher_t			*watcher;
	char					*name;
	ma_watcher_callback_t	cb;
	void					*cb_data;
	ma_int32_t				process_status;
};

typedef struct ma_process_watcher_s {
	ma_watcher_t	watcher;

	uv_idle_t		*idle_handle;
	
    MA_SLIST_DEFINE(watcher_list, ma_procsess_watcher_item_t);	   
} ma_process_watcher_t;

static ma_error_t process_watcher_add(ma_watcher_t *self, const char *name, ma_watcher_callback_t cb, ma_bool_t option, void *cb_data);
static ma_error_t process_watcher_delete(ma_watcher_t *self, const char *name);
static ma_error_t process_watcher_release(ma_watcher_t *self);

static const ma_watcher_methods_t methods = {
    &process_watcher_add,
    &process_watcher_delete,    
    &process_watcher_release    
};

/* TBD - Validates the name parameter and existance of name */
static ma_bool_t is_valid_name(const char *name) {
	return MA_TRUE;
}

ma_error_t ma_process_watcher_create(ma_event_loop_t *ma_loop, ma_watcher_t **watcher) {
	if(ma_loop && watcher) {
		ma_error_t rc = MA_OK;
		ma_process_watcher_t *self = NULL;	
		self = (ma_process_watcher_t *)calloc(1, sizeof(ma_process_watcher_t));
		if(self) {
			ma_watcher_init((ma_watcher_t *)self, &methods, ma_loop, self); 
			MA_SLIST_INIT(self, watcher_list);
			*watcher = (ma_watcher_t *)self;			
			return rc;
		}
		return MA_ERROR_OUTOFMEMORY;
	}
	return MA_ERROR_INVALIDARG;
}

static void idle_cb(uv_idle_t* handle, int status) {
	ma_procsess_watcher_item_t *item = NULL;
	ma_process_watcher_t *self = (ma_process_watcher_t *) handle->data ;

	item = MA_SLIST_GET_HEAD(self, watcher_list);
	while(item) {
		ma_procsess_watcher_item_t *next_item = MA_SLIST_GET_NEXT_NODE(item);
		ma_int32_t cur_status = ma_sys_get_process_status(item->name);
		if(cur_status != item->process_status) {
			item->process_status = cur_status;
			if(item && item->cb)
				item->cb((ma_watcher_t *)item->watcher, item->name, item->cb_data);	
		}
		item = next_item;
	}
}

ma_error_t process_watcher_add(ma_watcher_t *watcher, const char *name, ma_watcher_callback_t cb, ma_bool_t option, void *cb_data) {
	if(watcher && name && cb) {
		ma_error_t rc = MA_OK;
		ma_procsess_watcher_item_t *item = NULL;
		ma_process_watcher_t *self = (ma_process_watcher_t *)watcher;

		if(!is_valid_name(name))
			return MA_ERROR_INVALIDARG;

		item = (ma_procsess_watcher_item_t *)calloc(1,sizeof(ma_procsess_watcher_item_t));

        if(!item) 
			return MA_ERROR_OUTOFMEMORY;

        item->name = strdup(name);		
		item->cb = cb;
		item->cb_data = cb_data;
		item->watcher = (ma_watcher_t *)self;
		item->process_status = -1; 

		/* For first watcher add call */
		if(MA_SLIST_EMPTY(self, watcher_list)) {
			self->idle_handle = (uv_idle_t *)calloc(1 , sizeof(uv_idle_t));
			if(self->idle_handle) {
				self->idle_handle->data = self;
				if(uv_idle_init(ma_event_loop_get_uv_loop(((ma_watcher_t *)self)->ma_loop), self->idle_handle))
					rc = MA_ERROR_UNEXPECTED;
				else {				
					if(uv_idle_start(self->idle_handle, idle_cb))	
						rc = MA_ERROR_UNEXPECTED;
				}				
			}
			else
				rc = MA_ERROR_OUTOFMEMORY;
			
			if(MA_OK != rc) {
				// TBD
				free(item->name); item->name = NULL;
			}
		}
		
		if(MA_OK == rc)
			MA_SLIST_PUSH_BACK(self, watcher_list, item) ;
		
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_watcher_item_release(ma_procsess_watcher_item_t *item) {
    if(item) {
		ma_error_t rc = MA_OK;	
		if(item->name) free(item->name);
		free(item);
		return rc;
    }
	return MA_ERROR_INVALIDARG;
}

static void close_cb(uv_handle_t* handle) {
	if(handle) free(handle);
}

ma_error_t process_watcher_delete(ma_watcher_t *watcher, const char *name) {
	if(watcher && name) {
		ma_error_t rc = MA_OK;		
		ma_procsess_watcher_item_t *item = NULL ;
		ma_process_watcher_t *self = (ma_process_watcher_t *)watcher;

		MA_SLIST_FOREACH(self, watcher_list, item) {
			if(!strcmp(name,item->name)) {				
				MA_SLIST_REMOVE_NODE(self, watcher_list, item, ma_procsess_watcher_item_t) ;
				ma_watcher_item_release(item);	
				break ;
			}
		}

		if(MA_SLIST_EMPTY(self, watcher_list)) {		
			uv_idle_stop(self->idle_handle);
			uv_close((uv_handle_t *)self->idle_handle, close_cb);
		}

		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t process_watcher_release(ma_watcher_t *self) {
	if(self) {
		ma_error_t rc = MA_OK;
		MA_SLIST_CLEAR((ma_process_watcher_t *)self, watcher_list, ma_procsess_watcher_item_t, ma_watcher_item_release) ;
		free(self);
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}