#include "ma/internal/utils/watcher/ma_registry_watcher.h"
#include "ma/internal/utils/datastructures/ma_slist.h"
#include "ma/internal/utils/datastructures/ma_vector.h"
#include "ma/internal/utils/threading/ma_atomic.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal/utils/text/ma_utf8.h"

#include "ma/internal/ma_macros.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MA_WATCHER_ADD_MSG	WM_USER + 1
#define MA_WATCHER_DEL_MSG	WM_USER + 2

typedef struct ma_watcher_item_s ma_watcher_item_t;

struct ma_watcher_item_s {
	/*next link*/
	MA_SLIST_NODE_DEFINE(ma_watcher_item_t);
	char					*name;
	ma_watcher_callback_t	cb;
	void					*cb_data;
	HKEY					h_key;
	HANDLE					reg_event;
	ma_bool_t				default_view;
};

typedef struct ma_registry_watcher_s {
	ma_watcher_t			watcher;
    
    ma_loop_worker_t        *loop_worker;

	MA_SLIST_DEFINE(watcher_list, ma_watcher_item_t);
	ma_atomic_counter_t		item_count;

    DWORD					thread_id;	
	HANDLE					stop_event;	
	DWORD					reg_filter;
} ma_registry_watcher_t;

typedef struct handle_array_s {
    ma_vector_define(event_handle, HANDLE);
} handle_array_t;

typedef struct thread_data_s {
    uv_cond_t				cond;

    uv_mutex_t				mutex;

    ma_bool_t				is_initialized;

    ma_registry_watcher_t	*watcher;
} thread_data_t;

typedef struct asyn_data_s {
	ma_watcher_t			*watcher;
	char					*name;
	ma_watcher_callback_t	cb;
	void					*cb_data;
} asyn_data_t;

static ma_error_t registry_watcher_add(ma_watcher_t *self, const char *name, ma_watcher_callback_t cb, ma_bool_t option, void *cb_data);
static ma_error_t registry_watcher_delete(ma_watcher_t *self, const char *name);
static ma_error_t registry_watcher_release(ma_watcher_t *self);

static const ma_watcher_methods_t methods = {
    &registry_watcher_add,
    &registry_watcher_delete,    
    &registry_watcher_release    
};

/* TBD - Validates the name parameter and existance of name */
static ma_bool_t is_valid_name(const char *name) {
	return MA_TRUE;	
}

static ma_error_t register_for_notification(ma_watcher_item_t *item, DWORD reg_filter, handle_array_t *handle_array) {
	LONG win_error ;

	if(!item->reg_event) {
		item->reg_event = CreateEvent(NULL, TRUE, FALSE, NULL);
		if (!item->reg_event) {
			RegCloseKey(item->h_key);
			return MA_ERROR_UNEXPECTED;
		}
		ma_vector_push_back(handle_array, item->reg_event, HANDLE, event_handle);
	}

	win_error = RegNotifyChangeKeyValue(item->h_key, TRUE, reg_filter, item->reg_event, TRUE);
	if (win_error != ERROR_SUCCESS) {
		RegCloseKey(item->h_key);
		return MA_ERROR_UNEXPECTED;
	}	

	return MA_OK;
}

#ifdef _WIN64
#define REG_CROSS_ACCESS	KEY_WOW64_32KEY
#else
#define REG_CROSS_ACCESS	KEY_WOW64_64KEY 
#endif

static ma_error_t  open_registry_key(ma_watcher_item_t *item) {
	ma_error_t rc = MA_OK;
	HKEY h_main_key;
	LONG win_error;
	char main_key[MA_MAX_LEN] = {0};
	char *str = NULL, *sub_key = NULL;
	WCHAR w_sub_key[MAX_PATH_LEN] = {0};
	REGSAM access = KEY_NOTIFY;
		
	if(!item->default_view){
		access |= REG_CROSS_ACCESS;
	}

	MA_MSC_SELECT(_snprintf, snprintf)(main_key, MA_MAX_LEN,"%s",item->name); 
	str = strchr(main_key, '\\');
	if(str) {
		*str = 0;
		sub_key = str + 1;
	}
	else
		return MA_ERROR_INVALIDARG;

	if(!strcmp("HKEY_LOCAL_MACHINE", main_key)) h_main_key=HKEY_LOCAL_MACHINE;
	else if(!strcmp("HKEY_USERS", main_key)) h_main_key=HKEY_USERS;
	else if(!strcmp("HKEY_CURRENT_USER", main_key)) h_main_key=HKEY_CURRENT_USER;
	else if(!strcmp("HKEY_CLASSES_ROOT", main_key)) h_main_key=HKEY_CLASSES_ROOT;
	else if(!strcmp("HKEY_CURRENT_CONFIG", main_key)) h_main_key=HKEY_CURRENT_CONFIG;
	else 
		return MA_ERROR_INVALIDARG;

	if(-1 != ma_utf8_to_wide_char(w_sub_key, MAX_PATH_LEN, sub_key, strlen(sub_key)) ){			
		win_error = RegOpenKeyExW(h_main_key, w_sub_key, 0, access, &item->h_key);
		if (win_error != ERROR_SUCCESS) 		
			rc = MA_ERROR_UNEXPECTED;	
	}
	else
		rc = MA_ERROR_UNEXPECTED;
	
	return rc;
}

static ma_error_t close_registry_key(ma_watcher_item_t *item) {
	LONG win_error = RegCloseKey(item->h_key);
	if (win_error != ERROR_SUCCESS) {	
		return MA_ERROR_UNEXPECTED;
	}
	return MA_OK;
}

static void invoke_user_callback(void *data) {
	asyn_data_t *async_data = (asyn_data_t *)data;
	async_data->cb((ma_watcher_t *)async_data->watcher, async_data->name, async_data->cb_data);
	if(async_data->name) free(async_data->name);
	free(async_data); async_data = NULL;
}

static ma_error_t ma_watcher_item_release(ma_watcher_item_t *item) {
    if(item) {
		ma_error_t rc = MA_OK;			
		close_registry_key(item);		
		if(item->name) free(item->name);
		free(item);
		return rc;
    }
	return MA_ERROR_INVALIDARG;
}

static DWORD WINAPI watcher_thread_start(void* arg) {
	ma_error_t rc = MA_OK;
	ma_bool_t b_stop = MA_FALSE;
	thread_data_t *td = (thread_data_t *)arg;
	ma_registry_watcher_t *self = (ma_registry_watcher_t *)td->watcher;

	handle_array_t handle_array;
	
	/* system to create the message queue */
	MSG msg;
	PeekMessage(&msg, NULL, WM_USER, WM_USER, PM_NOREMOVE);

	MA_SLIST_INIT(self, watcher_list);

	/* Windows limitation of objects count MAXIMUM_WAIT_OBJECTS */
	ma_vector_init(&handle_array, MAXIMUM_WAIT_OBJECTS, event_handle, HANDLE); 
	
	ma_vector_push_back(&handle_array, self->stop_event, HANDLE, event_handle);
	
	if (td) {
		uv_mutex_lock(&td->mutex);
		td->is_initialized = MA_TRUE;
		uv_cond_signal(&td->cond);
		uv_mutex_unlock(&td->mutex);
	}

	while(!b_stop) {	
		DWORD dw_event = MsgWaitForMultipleObjectsEx(ma_vector_get_size(&handle_array, event_handle), ma_vector_get_ptr(&handle_array, event_handle), INFINITE, QS_ALLEVENTS, MWMO_INPUTAVAILABLE) ;
		
		if(dw_event == WAIT_OBJECT_0 + 0) {			/* Stop event */  
			ma_vector_clear(&handle_array, HANDLE, event_handle, CloseHandle);
			MA_SLIST_CLEAR(self, watcher_list, ma_watcher_item_t, ma_watcher_item_release) ;
			free(self);
			b_stop = MA_TRUE;
		}		
		else if(dw_event < ma_vector_get_size(&handle_array, event_handle)) {
			HANDLE h_event = ma_vector_at(&handle_array, dw_event, HANDLE, event_handle); 
			ma_watcher_item_t *notified_item = NULL;
			ResetEvent(h_event);
			MA_SLIST_FOREACH(self, watcher_list, notified_item) {
				if(h_event == notified_item->reg_event) {
					asyn_data_t *async_data = NULL;
					/* Re-register for another notification before calling user callback */
					register_for_notification(notified_item, self->reg_filter, &handle_array);

					async_data = (asyn_data_t*)calloc(1, sizeof(asyn_data_t));
					if(async_data) {						
						async_data->name = strdup(notified_item->name);
						async_data->cb = notified_item->cb;
						async_data->cb_data = notified_item->cb_data;
						async_data->watcher = (ma_watcher_t *)self;
						/* Notifying to caller by submitting the request to loop worker */
                        if(MA_OK != (rc = ma_loop_worker_submit_work(self->loop_worker, invoke_user_callback, async_data, MA_FALSE)))
                            free(async_data);
					}
					break ;
				}
			}			
		}
		else if(dw_event == WAIT_OBJECT_0 + ma_vector_get_size(&handle_array, event_handle)) {
			MSG recvd_msg;
			if(PeekMessage(&recvd_msg, NULL, 0, 0, PM_REMOVE)) {
				if(recvd_msg.message == MA_WATCHER_ADD_MSG) {
					ma_watcher_item_t *new_item = (ma_watcher_item_t *) recvd_msg.lParam;
					rc = open_registry_key(new_item);
					rc = register_for_notification(new_item, self->reg_filter, &handle_array);
					MA_SLIST_PUSH_BACK(self, watcher_list, new_item);
				}
				else if(recvd_msg.message == MA_WATCHER_DEL_MSG) {
					char *name = (char *) recvd_msg.lParam;
					if(name) {
						ma_watcher_item_t *del_item = NULL;
						MA_SLIST_FOREACH(self, watcher_list, del_item) {
							if(!strcmp(name, del_item->name)) {
								ma_vector_remove(&handle_array, del_item->reg_event, HANDLE, event_handle, CloseHandle);
								MA_SLIST_REMOVE_NODE(self, watcher_list, del_item, ma_watcher_item_t) ;
								ma_watcher_item_release(del_item);	
								break ;
							}
						}
						free(name);
					}
				}
				else {
					/* Ignore the message */
					continue;
				}
			}
		}
		else{	      
			b_stop = MA_TRUE;
			continue;
		}
	}
	return 0;
}

ma_error_t ma_regitry_watcher_set_filter(ma_watcher_t *reg_watcher, DWORD reg_filter) {
	if(reg_watcher) {
		ma_registry_watcher_t *self = (ma_registry_watcher_t *)reg_watcher;
		self->reg_filter = reg_filter;
	}
	return MA_OK;
}

ma_error_t ma_registry_watcher_create(ma_event_loop_t *ma_loop, ma_loop_worker_t *loop_worker, ma_watcher_t **watcher) {
	if(ma_loop && watcher) {
		ma_error_t rc = MA_OK;
		ma_registry_watcher_t *self = NULL;	
		self = (ma_registry_watcher_t *)calloc(1, sizeof(ma_registry_watcher_t));
		if(self) {			
			ma_watcher_init((ma_watcher_t *)self, &methods, ma_loop, self); 					
            self->loop_worker = loop_worker;

			if(MA_OK == rc) {
				self->stop_event = CreateEvent(NULL, TRUE, FALSE, NULL);
				if (!self->stop_event) {
					rc = MA_ERROR_UNEXPECTED;
				}
			}

			if(MA_OK == rc) {
				HANDLE h = NULL;
				thread_data_t td = {0};
				if (0 == uv_mutex_init(&td.mutex)) {
					if(0 == uv_cond_init(&td.cond)) {
						td.is_initialized = MA_FALSE;
						td.watcher = self;
						h = CreateThread(NULL, 0, watcher_thread_start, &td, 0, &self->thread_id);
						if(h != NULL) {
							uv_mutex_lock(&td.mutex);
							while( !td.is_initialized ){
								uv_cond_wait(&td.cond, &td.mutex);		
							}	
							uv_mutex_unlock(&td.mutex);
						}
						else
							rc = MA_ERROR_UNEXPECTED;
					}
					else {
						uv_mutex_destroy(&td.mutex);
						rc = MA_ERROR_UNEXPECTED;
					}
				}
				else
					rc = MA_ERROR_UNEXPECTED;
			}

			if(MA_OK != rc) {
				if(self->stop_event) CloseHandle(self->stop_event);
				free(self);
				return rc;
			}

			*watcher = (ma_watcher_t *)self;			
			return MA_OK;
		}
		return MA_ERROR_OUTOFMEMORY;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t registry_watcher_add(ma_watcher_t *watcher, const char *name, ma_watcher_callback_t cb, ma_bool_t option, void *cb_data) {
	if(watcher && name && cb) {
		ma_error_t rc = MA_OK;
		ma_watcher_item_t *item = NULL;
		ma_registry_watcher_t *self = (ma_registry_watcher_t *)watcher;		

		if(!is_valid_name(name))
			return MA_ERROR_INVALIDARG;			
					
		item = (ma_watcher_item_t *)calloc(1,sizeof(ma_watcher_item_t));

		if(!item) 
			return MA_ERROR_OUTOFMEMORY;			
		

		MA_ATOMIC_INCREMENT(self->item_count);
		item->name = strdup(name);		
		item->cb = cb;
		item->cb_data = cb_data;
		item->default_view = option;

		if(!PostThreadMessage(self->thread_id, MA_WATCHER_ADD_MSG, 0, (LPARAM)item)) {
			free(item->name);
			free(item); item = NULL;
			rc = MA_ERROR_UNEXPECTED;
		}				
				
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t registry_watcher_delete(ma_watcher_t *watcher, const char *name) {
	if(watcher && name) {
		ma_error_t rc = MA_OK;		
		ma_registry_watcher_t *self = (ma_registry_watcher_t *)watcher;
		char *del_name = strdup(name);
				
		if(!PostThreadMessage(self->thread_id, MA_WATCHER_DEL_MSG, 0, (LPARAM)del_name)) {
			free(del_name);
			return MA_ERROR_UNEXPECTED;
		}
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t registry_watcher_release(ma_watcher_t *watcher) {
	if(watcher) {
		ma_error_t rc = MA_OK;
		ma_registry_watcher_t *self = (ma_registry_watcher_t *)watcher;
				
		SetEvent(self->stop_event);

		return rc;
	}
	return MA_ERROR_INVALIDARG;
}