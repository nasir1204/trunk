#include "ma/internal/utils/inventory/ma_inventory.h"
#include "ma/internal/utils/inventory/ma_inventory_info.h"
#include "ma/internal/utils/inventory/ma_inventory_datastore.h"
#include "ma/ma_msgbus.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/defs/ma_inventory_defs.h"
#include "ma/internal/ma_macros.h"

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "inventory"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <uv.h>

ma_logger_t *inventory_logger;

struct ma_inventory_s {
    char path[MA_MAX_PATH_LEN+1];
    ma_msgbus_t *msgbus;
    ma_ds_t* datastore;
};


ma_error_t ma_inventory_create(ma_msgbus_t *msgbus, ma_ds_t *datastore, const char *base_path, ma_inventory_t **inventory) {
    if(msgbus && datastore && base_path && inventory) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;

        if((*inventory = (ma_inventory_t*)calloc(1, sizeof(ma_inventory_t)))) {
            (*inventory)->msgbus = msgbus;
            strncpy((*inventory)->path, base_path, MA_MAX_PATH_LEN);
            (*inventory)->datastore = datastore;
            err = MA_OK;
            MA_LOG(inventory_logger, MA_LOG_SEV_INFO, "inventory created successfully");
        }
        if(MA_OK != err) {
            MA_LOG(inventory_logger, MA_LOG_SEV_ERROR, "inventory creation failed, last error(%d)", err);
            if(*inventory)
                (void)ma_inventory_release(*inventory);
            *inventory = NULL;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_add(ma_inventory_t *inventory, ma_inventory_info_t *info) {
    return inventory && info ? ma_inventory_datastore_write(inventory, info, inventory->datastore, inventory->path) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_add_variant(ma_inventory_t *inventory, const char *inventory_id, ma_variant_t *var) {
    return inventory && inventory_id && var ? ma_inventory_datastore_write_variant(inventory, inventory_id, inventory->datastore, inventory->path, var) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_delete_crn(ma_inventory_t *inventory, const char *inventory_id, const char *crn) {
    return inventory && inventory_id && crn ? ma_inventory_datastore_remove_inventory_crn(inventory, inventory_id, crn, inventory->datastore, inventory->path) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_delete(ma_inventory_t *inventory, const char *inventory_id) {
    return inventory && inventory_id ? ma_inventory_datastore_remove_inventory(inventory_id, inventory->datastore, inventory->path) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_enumerate_city_pincode(ma_inventory_t *inventory, const char *pincode, const char *city, ma_variant_t **var) {
   return inventory && pincode && city && var ? ma_inventory_datastore_enumerate_city_pincode(inventory, inventory->datastore, inventory->path,city,pincode, var) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_delete_all(ma_inventory_t *inventory) {
    return inventory ? ma_inventory_datastore_clear(inventory->datastore, inventory->path) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_enumerate(ma_inventory_t *inventory, ma_variant_t **var) {
    return inventory && var ? ma_inventory_datastore_enumerate(inventory, inventory->datastore, inventory->path, var) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_get(ma_inventory_t *inventory, const char *inventory_id, const char *crn, ma_inventory_info_t **info) {
    return inventory && inventory_id && crn && info ? ma_inventory_datastore_get(inventory, inventory->datastore, inventory->path, inventory_id, crn, info) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_get_all(ma_inventory_t *inventory, const char *inventory_id, ma_variant_t **var) {
    return inventory && inventory_id && var ? ma_inventory_datastore_get_all(inventory, inventory->datastore, inventory->path, inventory_id, var) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_set_logger(ma_logger_t *logger) {
    if(logger) {
        inventory_logger = logger;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_get_msgbus(ma_inventory_t *inventory, ma_msgbus_t **msgbus) {
    if(inventory && msgbus) {
        *msgbus = inventory->msgbus;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_start(ma_inventory_t *inventory) {
    if(inventory) {
        MA_LOG(inventory_logger, MA_LOG_SEV_TRACE, "inventory started");
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_stop(ma_inventory_t *inventory) {
    if(inventory) {
        MA_LOG(inventory_logger, MA_LOG_SEV_TRACE, "inventory stopped");
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_inventory_release(ma_inventory_t *inventory) {
    if(inventory) {
        free(inventory);
    }
    return MA_ERROR_INVALIDARG;
}

