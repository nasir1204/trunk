#include "ma/internal/utils/inventory/ma_inventory_info.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/defs/ma_inventory_defs.h"
#include "ma/ma_log.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

extern ma_logger_t *inventory_logger;

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "inventory"

const char *gvehicle_type_str[] = {
#define MA_INVENTORY_INFO_VEHICLE_EXPAND(status, index, status_str) status_str,
    MA_INVENTORY_INFO_VEHICLE_EXPANDER
#undef MA_INVENTORY_INFO_VEHICLE_EXPAND
};

const char *gwork_status_str[] = {
#define MA_INVENTORY_INFO_WORK_STATUS_EXPAND(status, index, status_str) status_str,
    MA_INVENTORY_INFO_WORK_STATUS_EXPANDER
#undef MA_INVENTORY_INFO_WORK_STATUS_EXPAND
};

struct ma_inventory_info_s {
    char email_id[MA_MAX_LEN+1];
    char contact[MA_MAX_LEN+1];
    ma_task_time_t date;
    char pincode[MA_MAX_LEN+1];
    ma_uint32_t norder_completed;
    ma_uint32_t norder_pendings;
    ma_uint32_t norder_cancelled;
    ma_uint32_t norder_not_delivered;
    ma_uint32_t start_km;
    ma_uint32_t end_km;
    ma_task_time_t start_time;
    ma_task_time_t end_time;
    ma_inventory_info_work_status_t status;
    ma_inventory_info_vehicle_type_t vehicle_type;
};


ma_error_t ma_inventory_info_create(ma_inventory_info_t **info) {
    return info ?  ((*info = (ma_inventory_info_t*)calloc(1, sizeof(ma_inventory_info_t)))) ? MA_OK : MA_ERROR_OUTOFMEMORY :  MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_info_release(ma_inventory_info_t *info) {
    if(info) {
        free(info);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


/* setter */
ma_error_t ma_inventory_info_set_contact(ma_inventory_info_t *info, const char *contact) {
    return info && contact && strncpy(info->contact, contact, MA_MAX_LEN) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_info_set_work_status_from_str(ma_inventory_info_t *info, const char *str) {
    if(info && str) {
        if(!strcmp(str, MA_INVENTORY_INFO_WORK_STATUS_PRESENT_STR))
            info->status = MA_INVENTORY_INFO_WORK_STATUS_PRESENT;
        else if(!strcmp(str, MA_INVENTORY_INFO_WORK_STATUS_ABSENT_STR))
            info->status = MA_INVENTORY_INFO_WORK_STATUS_ABSENT;
        else {
        }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_info_set_vehicle_type_from_str(ma_inventory_info_t *info, const char *str) {
    if(info && str) {
        ma_error_t err = MA_OK;

        if(!strcmp(str, MA_INVENTORY_INFO_VEHICLE_TYPE_TWO_WHEELER_STR))
            info->vehicle_type = MA_INVENTORY_INFO_VEHICLE_TYPE_TWO_WHEELER;
        else if(!strcmp(str, MA_INVENTORY_INFO_VEHICLE_TYPE_THREE_WHEELER_STR))
            info->vehicle_type = MA_INVENTORY_INFO_VEHICLE_TYPE_THREE_WHEELER;
        else if(!strcmp(str, MA_INVENTORY_INFO_VEHICLE_TYPE_FOUR_WHEELER_STR))
            info->vehicle_type = MA_INVENTORY_INFO_VEHICLE_TYPE_FOUR_WHEELER;
        else {
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_info_set_order_not_delivered_count(ma_inventory_info_t *info, ma_uint32_t count) {
    if(info) {
        info->norder_not_delivered = count;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_info_set_vehicle_type(ma_inventory_info_t *info, ma_inventory_info_vehicle_type_t type) {
    return info && type && (info->vehicle_type =  type) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_info_set_email_id(ma_inventory_info_t *info, const char *id) {
    return info && id && strncpy(info->email_id, id, MA_MAX_LEN) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_info_set_date(ma_inventory_info_t *info, ma_task_time_t date) {
    return info && memcpy(&info->date, &date, sizeof(ma_task_time_t)) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_info_set_pincode(ma_inventory_info_t *info, const char *pin) {
    return info && pin && strncpy(info->pincode, pin, MA_MAX_LEN) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_info_set_order_completed_count(ma_inventory_info_t *info, ma_uint32_t count) {
    return info && count && (info->norder_completed = count) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_info_set_order_pending_count(ma_inventory_info_t *info, ma_uint32_t count) {
    if(info) {
        info->norder_pendings = count;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_info_set_order_cancelled_count(ma_inventory_info_t *info, ma_uint32_t count) {
    if(info) {
        info->norder_cancelled = count;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_info_set_start_kilometers(ma_inventory_info_t *info, ma_uint32_t km) {
    return info && km && (info->start_km = km) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_info_set_end_kilometers(ma_inventory_info_t *info, ma_uint32_t km) {
    return info && km && (info->end_km = km) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_info_set_start_time(ma_inventory_info_t *info, ma_task_time_t start) {
    return info && memcpy(&info->start_time, &start, sizeof(ma_task_time_t)) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_info_set_end_time(ma_inventory_info_t *info, ma_task_time_t end) {
    return info && memcpy(&info->end_time, &end, sizeof(ma_task_time_t)) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_info_set_work_status(ma_inventory_info_t *info, ma_inventory_info_work_status_t status) {
    return info && status && (info->status = status) ? MA_OK : MA_ERROR_INVALIDARG;
}
                                                                       
/* getters */
ma_error_t ma_inventory_info_get_contact(ma_inventory_info_t *info, char *id) {
    return info && id && strncpy(id, info->contact, MA_MAX_LEN) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_info_get_order_not_delivered_count(ma_inventory_info_t *info, ma_uint32_t *count) {
    if(info && count) {
        *count = info->norder_not_delivered;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_info_get_work_status_str(ma_inventory_info_t *info, char *str) {
    return info && str && info->status && strcpy(str, gwork_status_str[info->status -1 ]) ? MA_OK : MA_ERROR_INVALIDARG;
}
ma_error_t ma_inventory_info_get_vehicle_type_str(ma_inventory_info_t *info, char *str) {
    return info && str && info->vehicle_type && strcpy(str, gvehicle_type_str[info->vehicle_type -1 ]) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_info_get_vehicle_type(ma_inventory_info_t *info, ma_inventory_info_vehicle_type_t *type) {
    return info && type && (*type = info->vehicle_type) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_info_get_email_id(ma_inventory_info_t *info, char *id) {
    return info && id && strncpy(id, info->email_id, MA_MAX_LEN) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_info_get_date(ma_inventory_info_t *info, ma_task_time_t *date) {
    return info && date && memcpy(date, &info->date, sizeof(ma_task_time_t)) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_info_get_pincode(ma_inventory_info_t *info, char *pin) {
    return info && pin && strncpy(pin, info->pincode, MA_MAX_LEN) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_info_get_order_completed_count(ma_inventory_info_t *info, ma_uint32_t *count) {
    if(info && count) {
        *count = info->norder_completed;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_info_get_order_pending_count(ma_inventory_info_t *info, ma_uint32_t *count) {
    if(info && count) {
        *count = info->norder_pendings;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_info_get_order_cancelled_count(ma_inventory_info_t *info, ma_uint32_t *count) {
    if(info && count) {
        *count = info->norder_cancelled;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_info_get_start_kilometers(ma_inventory_info_t *info, ma_uint32_t *km) {
    if(info && km) {
        *km = info->start_km;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_info_get_end_kilometers(ma_inventory_info_t *info, ma_uint32_t *km) {
    if(info && km) {
        *km = info->end_km;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_info_get_start_time(ma_inventory_info_t *info, ma_task_time_t *start) {
    return info && start && memcpy(start, &info->start_time, sizeof(ma_task_time_t)) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_info_get_end_time(ma_inventory_info_t *info, ma_task_time_t *end) {
    return info && end && memcpy(end, &info->end_time, sizeof(ma_task_time_t)) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_info_get_work_status(ma_inventory_info_t *info, ma_inventory_info_work_status_t *status) {
    return info && status && (*status = info->status) ? MA_OK : MA_ERROR_INVALIDARG;
}



ma_error_t ma_inventory_info_order_add(ma_inventory_info_t *info, const char *order) {
    if(info && order) {
        ma_error_t err = MA_OK;

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_info_order_remove(ma_inventory_info_t *info, const char *order) {
    if(info && order) {
        ma_error_t err = MA_OK;

        return err;
    }
    return MA_ERROR_INVALIDARG;
}



ma_error_t ma_inventory_info_convert_to_variant(ma_inventory_info_t *info, ma_variant_t **variant) {
    if(info && variant) {
        ma_error_t err = MA_OK;
        ma_table_t *info_table = NULL;

        if(MA_OK == (err = ma_table_create(&info_table))) {
            {
                 char value[MA_MAX_BUFFER_LEN+1] = {0};

                 if(MA_OK == (err = ma_inventory_info_get_email_id(info, value))) {
                     ma_variant_t *v =NULL;

                     if(MA_OK == (err = ma_variant_create_from_string(value, strlen(value), &v))) {
                         (void)ma_table_add_entry(info_table, MA_INVENTORY_INFO_ATTR_EMAIL_ID, v);
                         (void)ma_variant_release(v);
                     }
                 }
            }
            {
                 char value[MA_MAX_BUFFER_LEN+1] = {0};

                 if(MA_OK == (err = ma_inventory_info_get_contact(info, value))) {
                     ma_variant_t *v =NULL;

                     if(MA_OK == (err = ma_variant_create_from_string(value, strlen(value), &v))) {
                         (void)ma_table_add_entry(info_table, MA_INVENTORY_INFO_ATTR_CONTACT, v);
                         (void)ma_variant_release(v);
                     }
                 }
            }
            {
                 ma_task_time_t date = {0};

                 if(MA_OK == (err = ma_inventory_info_get_date(info, &date))) {
                     ma_variant_t *v = NULL;

                     if(MA_OK == (err = ma_variant_create_from_raw(&date, sizeof(date), &v))) {
                         (void)ma_table_add_entry(info_table, MA_INVENTORY_INFO_ATTR_DATE, v);
                         (void)ma_variant_release(v);
                     }
                 }
            }
            {
                 char value[MA_MAX_BUFFER_LEN+1] = {0};

                 if(MA_OK == (err = ma_inventory_info_get_pincode(info, value))) {
                     ma_variant_t *v =NULL;

                     if(MA_OK == (err = ma_variant_create_from_string(value, strlen(value), &v))) {
                         (void)ma_table_add_entry(info_table, MA_INVENTORY_INFO_ATTR_PINCODE, v);
                         (void)ma_variant_release(v);
                     }
                 }
             }
             {
                 ma_uint32_t value = 0;

                 if(MA_OK == (err = ma_inventory_info_get_order_completed_count(info, &value))) {
                     ma_variant_t *v = NULL;

                     if(MA_OK == (err = ma_variant_create_from_uint32(value, &v))) {
                         (void)ma_table_add_entry(info_table, MA_INVENTORY_INFO_ATTR_ORDER_COMPLETE_COUNT, v);
                         (void)ma_variant_release(v);
                     }
                 }
             }
             {
                 ma_uint32_t value = 0;

                 if(MA_OK == (err = ma_inventory_info_get_order_pending_count(info, &value))) {
                     ma_variant_t *v = NULL;

                     if(MA_OK == (err = ma_variant_create_from_uint32(value, &v))) {
                         (void)ma_table_add_entry(info_table, MA_INVENTORY_INFO_ATTR_ORDER_PENDING_COUNT, v);
                         (void)ma_variant_release(v);
                     }
                 }
             }
             {
                 ma_uint32_t value = 0;

                 if(MA_OK == (err = ma_inventory_info_get_order_cancelled_count(info, &value))) {
                     ma_variant_t *v = NULL;

                     if(MA_OK == (err = ma_variant_create_from_uint32(value, &v))) {
                         (void)ma_table_add_entry(info_table, MA_INVENTORY_INFO_ATTR_ORDER_CANCELLED_COUNT, v);
                         (void)ma_variant_release(v);
                     }
                 }
             }
             {
                 ma_uint32_t value = 0;

                 if(MA_OK == (err = ma_inventory_info_get_order_not_delivered_count(info, &value))) {
                     ma_variant_t *v = NULL;

                     if(MA_OK == (err = ma_variant_create_from_uint32(value, &v))) {
                         (void)ma_table_add_entry(info_table, MA_INVENTORY_INFO_ATTR_ORDER_NOT_DELIVERED_COUNT, v);
                         (void)ma_variant_release(v);
                     }
                 }
             }
             {
                 ma_uint32_t value = 0;

                 if(MA_OK == (err = ma_inventory_info_get_start_kilometers(info, &value))) {
                     ma_variant_t *v = NULL;

                     if(MA_OK == (err = ma_variant_create_from_uint32(value, &v))) {
                         (void)ma_table_add_entry(info_table, MA_INVENTORY_INFO_ATTR_START_KILOMETER, v);
                         (void)ma_variant_release(v);
                     }
                 }
             }
             {
                 ma_uint32_t value = 0;

                 if(MA_OK == (err = ma_inventory_info_get_end_kilometers(info, &value))) {
                     ma_variant_t *v = NULL;

                     if(MA_OK == (err = ma_variant_create_from_uint32(value, &v))) {
                         (void)ma_table_add_entry(info_table, MA_INVENTORY_INFO_ATTR_END_KILOMETER, v);
                         (void)ma_variant_release(v);
                     }
                 }
             }
             {
                 ma_task_time_t date = {0};

                 if(MA_OK == (err = ma_inventory_info_get_start_time(info, &date))) {
                     ma_variant_t *v = NULL;

                     if(MA_OK == (err = ma_variant_create_from_raw(&date, sizeof(date), &v))) {
                         (void)ma_table_add_entry(info_table, MA_INVENTORY_INFO_ATTR_START_TIME, v);
                         (void)ma_variant_release(v);
                     }
                 }
             }
             {
                 ma_task_time_t date = {0};

                 if(MA_OK == (err = ma_inventory_info_get_end_time(info, &date))) {
                     ma_variant_t *v = NULL;

                     if(MA_OK == (err = ma_variant_create_from_raw(&date, sizeof(date), &v))) {
                         (void)ma_table_add_entry(info_table, MA_INVENTORY_INFO_ATTR_END_TIME, v);
                         (void)ma_variant_release(v);
                     }
                 }
             }
             {
                 ma_inventory_info_work_status_t value;

                 if(MA_OK == (err = ma_inventory_info_get_work_status(info, &value))) {
                     ma_variant_t *v = NULL;

                     if(MA_OK == (err = ma_variant_create_from_uint32(value, &v))) {
                         (void)ma_table_add_entry(info_table, MA_INVENTORY_INFO_ATTR_WORK_STATUS, v);
                         (void)ma_variant_release(v);
                     }
                 }
             }
             {
                 ma_inventory_info_vehicle_type_t value;

                 if(MA_OK == (err = ma_inventory_info_get_vehicle_type(info, &value))) {
                     ma_variant_t *v = NULL;

                     if(MA_OK == (err = ma_variant_create_from_uint32(value, &v))) {
                         (void)ma_table_add_entry(info_table, MA_INVENTORY_INFO_ATTR_VEHICLE_TYPE, v);
                         (void)ma_variant_release(v);
                     }
                 }
             }

             err = ma_variant_create_from_table(info_table, variant);
             (void)ma_table_release(info_table);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_info_convert_from_variant(ma_variant_t *variant, ma_inventory_info_t **info) {
    if(info && variant) {
        ma_error_t err = MA_OK;

        if(MA_OK == (err = ma_inventory_info_create(info))) {
            ma_table_t *info_table = NULL;

            if(MA_OK == (err = ma_variant_get_table(variant, &info_table))) {
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_INVENTORY_INFO_ATTR_EMAIL_ID, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                (void)ma_inventory_info_set_email_id((*info), pstr);
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_INVENTORY_INFO_ATTR_CONTACT, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                (void)ma_inventory_info_set_contact((*info), pstr);
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_INVENTORY_INFO_ATTR_DATE, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_raw_buffer(v, &buf))) {
                            ma_task_time_t *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_raw(buf, (const unsigned char**)&pstr, &len))) {
                                (void)ma_inventory_info_set_date((*info), *pstr);
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_INVENTORY_INFO_ATTR_PINCODE, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                (void)ma_inventory_info_set_pincode((*info), pstr);
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_INVENTORY_INFO_ATTR_ORDER_COMPLETE_COUNT, &v))) {
                        ma_uint32_t value = 0;

                        if(MA_OK == (err = ma_variant_get_uint32(v, &value)))
                            (void)ma_inventory_info_set_order_completed_count((*info), value);
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_INVENTORY_INFO_ATTR_ORDER_PENDING_COUNT, &v))) {
                        ma_uint32_t value = 0;

                        if(MA_OK == (err = ma_variant_get_uint32(v, &value)))
                            (void)ma_inventory_info_set_order_pending_count((*info), value);
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_INVENTORY_INFO_ATTR_ORDER_CANCELLED_COUNT, &v))) {
                        ma_uint32_t value = 0;

                        if(MA_OK == (err = ma_variant_get_uint32(v, &value)))
                            (void)ma_inventory_info_set_order_cancelled_count((*info), value);
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_INVENTORY_INFO_ATTR_ORDER_NOT_DELIVERED_COUNT, &v))) {
                        ma_uint32_t value = 0;

                        if(MA_OK == (err = ma_variant_get_uint32(v, &value)))
                            (void)ma_inventory_info_set_order_not_delivered_count((*info), value);
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_INVENTORY_INFO_ATTR_START_KILOMETER, &v))) {
                        ma_uint32_t value = 0;

                        if(MA_OK == (err = ma_variant_get_uint32(v, &value)))
                            (void)ma_inventory_info_set_start_kilometers((*info), value);
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_INVENTORY_INFO_ATTR_END_KILOMETER, &v))) {
                        ma_uint32_t value = 0;

                        if(MA_OK == (err = ma_variant_get_uint32(v, &value)))
                            (void)ma_inventory_info_set_end_kilometers((*info), value);
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_INVENTORY_INFO_ATTR_START_TIME, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_raw_buffer(v, &buf))) {
                            ma_task_time_t *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_raw(buf, (const unsigned char**)&pstr, &len))) {
                                (void)ma_inventory_info_set_start_time((*info), *pstr);
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_INVENTORY_INFO_ATTR_END_TIME, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_raw_buffer(v, &buf))) {
                            ma_task_time_t *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_raw(buf, (const unsigned char**)&pstr, &len))) {
                                (void)ma_inventory_info_set_end_time((*info), *pstr);
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_INVENTORY_INFO_ATTR_WORK_STATUS, &v))) {
                        ma_inventory_info_work_status_t value = 0;

                        if(MA_OK == (err = ma_variant_get_uint32(v, &value)))
                            (void)ma_inventory_info_set_work_status((*info), (ma_inventory_info_work_status_t)value);
                        (void)ma_variant_release(v);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_INVENTORY_INFO_ATTR_VEHICLE_TYPE, &v))) {
                        ma_inventory_info_vehicle_type_t value = 0;

                        if(MA_OK == (err = ma_variant_get_uint32(v, &value)))
                            (void)ma_inventory_info_set_vehicle_type((*info), (ma_inventory_info_vehicle_type_t)value);
                        (void)ma_variant_release(v);
                    }
                }



                (void)ma_table_release(info_table);
            }
        }

        return MA_OK; /* intentional */
    }
    return MA_ERROR_INVALIDARG;
}

