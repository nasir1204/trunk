#include "ma/internal/utils/inventory/ma_inventory_datastore.h"
#include "ma/internal/defs/ma_inventory_defs.h"
#include "ma/scheduler/ma_enumerator.h"
#include "ma/internal/ma_macros.h"
#include "ma/ma_log.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "inventory"

extern ma_logger_t *inventory_logger;

#define BOOKING_DATA MA_DATASTORE_PATH_SEPARATOR "inventory_data"
#define BOOKING_ID MA_DATASTORE_PATH_SEPARATOR "inventory_id"

#define BOOKING_DATA_PATH BOOKING_DATA BOOKING_ID MA_DATASTORE_PATH_SEPARATOR/* \"inventory_data"\"inventory_id"\ */


ma_error_t ma_inventory_datastore_read(ma_inventory_t *inventory, ma_ds_t *datastore, const char *ds_path) {
    if(inventory && datastore && ds_path) {
        ma_error_t err = MA_OK;
        ma_ds_iterator_t *inventory_iterator = NULL;
        ma_buffer_t *inventory_id_buf = NULL;
        char inventory_data_base_path[MA_MAX_PATH_LEN] = {0};

        MA_MSC_SELECT(_snprintf, snprintf)(inventory_data_base_path, MA_MAX_PATH_LEN, "%s%s", ds_path, BOOKING_DATA_PATH);
        MA_LOG(inventory_logger, MA_LOG_SEV_DEBUG, "inventory data store path(%s) reading...", inventory_data_base_path);
        if(MA_OK == (err = ma_ds_iterator_create(datastore, 0, inventory_data_base_path, &inventory_iterator))) {
            while(MA_ERROR_NO_MORE_ITEMS != ma_ds_iterator_get_next(inventory_iterator, &inventory_id_buf) && inventory_id_buf) {
                const char *inventory_id = NULL;
                size_t size = 0;
                
                if(MA_OK == (err = ma_buffer_get_string(inventory_id_buf, &inventory_id, &size))) {
                    char inventory_data_path[MA_MAX_PATH_LEN] = {0};
                    ma_variant_t *inventory_var = NULL;
                    MA_MSC_SELECT(_snprintf, snprintf)(inventory_data_path, MA_MAX_PATH_LEN, "%s", inventory_data_base_path);/* base_path\"inventory_data"\"inventory_id"\4097 */
                    if(MA_OK == (err = ma_ds_get_variant(datastore, inventory_data_path, inventory_id, MA_VARTYPE_TABLE, &inventory_var))) {

                        (void)ma_variant_release(inventory_var);
                    }
                }
                (void)ma_buffer_release(inventory_id_buf);
            }
            (void)ma_ds_iterator_release(inventory_iterator);
        }

        return ((MA_ERROR_DS_PATH_NOT_FOUND == err) || (MA_ERROR_SCHEDULER_TASK_ID_CONFLICT == err)) ? MA_OK : err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_datastore_enumerate(ma_inventory_t *inventory, ma_ds_t *datastore, const char *ds_path, ma_variant_t **var) {
    if(inventory && datastore && ds_path) {
        ma_error_t err = MA_OK;
        ma_array_t *array = NULL;

        if(MA_OK == (err = ma_array_create(&array))) {
            ma_ds_iterator_t *inventory_iterator = NULL;
            ma_buffer_t *inventory_id_buf = NULL;
            char inventory_data_base_path[MA_MAX_PATH_LEN] = {0};

            MA_MSC_SELECT(_snprintf, snprintf)(inventory_data_base_path, MA_MAX_PATH_LEN, "%s%s", ds_path, BOOKING_DATA_PATH);
            MA_LOG(inventory_logger, MA_LOG_SEV_DEBUG, "inventory data store path(%s) reading...", inventory_data_base_path);
            if(MA_OK == (err = ma_ds_iterator_create(datastore, 0, inventory_data_base_path, &inventory_iterator))) {
                while(MA_ERROR_NO_MORE_ITEMS != ma_ds_iterator_get_next(inventory_iterator, &inventory_id_buf) && inventory_id_buf) {
                    const char *inventory_id = NULL;
                    size_t size = 0;
                
                    if(MA_OK == (err = ma_buffer_get_string(inventory_id_buf, &inventory_id, &size))) {
                        ma_variant_t *inventory_var = NULL;

                        MA_LOG(inventory_logger, MA_LOG_SEV_DEBUG, "Inventory id(%s)", inventory_id);
                        if(MA_OK == (err = ma_variant_create_from_string(inventory_id, size, &inventory_var))) {
                            err = ma_array_push(array, inventory_var);
                            (void)ma_variant_release(inventory_var);
                        }
                    }
                    (void)ma_buffer_release(inventory_id_buf);
                }
                (void)ma_ds_iterator_release(inventory_iterator);
            }
            if(MA_OK == err) {
                err = ma_variant_create_from_array(array, var);
            }
            (void)ma_array_release(array);
        }

        return  err;
    }
    return MA_ERROR_INVALIDARG;
}
#if 1
ma_error_t ma_inventory_datastore_enumerate_city_pincode(ma_inventory_t *inventory, ma_ds_t *datastore, const char *ds_path, const char *city, const char *pincode, ma_variant_t **ret_var) {
    if(inventory && datastore && ds_path && city && pincode && ret_var) {
         ma_error_t err = MA_OK;
#if 0
         ma_table_t *inventory_table = NULL;
         size_t inventory_table_size = 0;

         if(MA_OK == (err = ma_table_create(&inventory_table))) {
             ma_ds_iterator_t *inventory_iterator = NULL;
             ma_buffer_t *inventory_id_buf = NULL;
             char inventory_data_base_path[MA_MAX_PATH_LEN] = {0};

             MA_MSC_SELECT(_snprintf, snprintf)(inventory_data_base_path, MA_MAX_PATH_LEN, "%s%s", ds_path, BOOKING_DATA_PATH);
             MA_LOG(inventory_logger, MA_LOG_SEV_DEBUG, "inventory data store path(%s) reading...", inventory_data_base_path);
             if(MA_OK == (err = ma_ds_iterator_create(datastore, 0, inventory_data_base_path, &inventory_iterator))) {
                 while(MA_ERROR_NO_MORE_ITEMS != ma_ds_iterator_get_next(inventory_iterator, &inventory_id_buf) && inventory_id_buf) {
                     const char *inventory_id = NULL;
                     size_t size = 0;
                
                     if(MA_OK == (err = ma_buffer_get_string(inventory_id_buf, &inventory_id, &size))) {
                         char inventory_data_path[MA_MAX_PATH_LEN] = {0};
                         ma_variant_t *inventory_var = NULL;
                         MA_MSC_SELECT(_snprintf, snprintf)(inventory_data_path, MA_MAX_PATH_LEN, "%s", inventory_data_base_path);/* base_path\"inventory_data"\"inventory_id"\4097 */
                         MA_LOG(inventory_logger, MA_LOG_SEV_TRACE, "inventory path(%s) id(%s) reading...", inventory_data_path, inventory_id);
                         if(MA_OK == (err = ma_ds_get_variant(datastore, inventory_data_path, inventory_id, MA_VARTYPE_TABLE, &inventory_var))) {
                             ma_table_t *book_entry = NULL;

                             if(MA_OK == (err = ma_variant_get_table(inventory_var, &book_entry))) {
                                 ma_array_t *arr = NULL;

                                 if(MA_OK == (err = ma_table_get_keys(book_entry, &arr))) {
                                     size_t arr_size = 0, i = 0;

                                     (void)ma_array_size(arr, &arr_size);
                                     for(i = 1; i <= arr_size; ++i) {
                                         ma_variant_t *keyv = NULL;

                                         if(MA_OK == (err = ma_array_get_element_at(arr, i, &keyv))) {
                                             ma_buffer_t *buf = NULL;

                                             if(MA_OK == (err = ma_variant_get_string_buffer(keyv, &buf))) {
                                                 const char *key = NULL; size_t key_len = 0;

                                                 if(MA_OK == (err = ma_buffer_get_string(buf, &key, &key_len))) {
                                                     ma_variant_t *inventory_value = NULL;

                                                     MA_LOG(inventory_logger, MA_LOG_SEV_TRACE, "inventory id(%s) key(%s)", inventory_id, key);
                                                     if(MA_OK == (err = ma_table_get_value(book_entry, key, &inventory_value))) {
                                                         ma_inventory_info_t *book_info = NULL;

                                                         if(MA_OK == (err = ma_inventory_info_convert_from_variant(inventory_value, &book_info))) {
                                                             char from_pin[MA_MAX_LEN+1] = {0};
                                                             char from_city[MA_MAX_LEN+1] = {0};
                                                             char id[MA_MAX_LEN+1] = {0};
                                                             ma_task_time_t date = {0};

                                                             (void)ma_inventory_info_get_from_city(book_info, from_city);
                                                             (void)ma_inventory_info_get_source_pincode(book_info, from_pin);
                                                             (void)ma_inventory_info_get_date(info, &date);
                                                             MA_LOG(inventory_logger, MA_LOG_SEV_TRACE, "inventory id(%s) key(%s) city(%s) pincode(%s)", inventory_id, key, from_city, from_pin);
                                                             if(!strcmp(from_pin, pincode) && !strcmp(from_city, city)) {
                                                                 (void)ma_table_add_entry(inventory_table, id, inventory_value);
                                                                 MA_LOG(inventory_logger, MA_LOG_SEV_TRACE, "inventory id(%s) pincode(%s) city(%s) matched", id, pincode, city);
                                                             }

                                                             (void)ma_inventory_info_release(book_info);
                                                         }
                                                         (void)ma_variant_release(inventory_value);
                                                     }
                                                 }
                                                 (void)ma_buffer_release(buf);
                                             }
                                             (void)ma_variant_release(keyv);
                                         }
                                     }
                                     (void)ma_array_release(arr);
                                 }
                                 (void)ma_table_release(book_entry);
                             }
                             (void)ma_variant_release(inventory_var);
                         }
                     }
                     (void)ma_buffer_release(inventory_id_buf);
                 }
                 (void)ma_ds_iterator_release(inventory_iterator);
             }


             (void)ma_table_size(inventory_table, &inventory_table_size);
             if(inventory_table_size) {
                 err = ma_variant_create_from_table(inventory_table, ret_var);
             }
             (void)ma_table_release(inventory_table);
         }

         return err;
#endif
    }
    return MA_ERROR_INVALIDARG;
}
#endif



ma_error_t ma_inventory_datastore_write(ma_inventory_t *inventory, ma_inventory_info_t *info, ma_ds_t *datastore, const char *ds_path) {
    if(inventory && info && datastore && ds_path) {
        char inventory_data_path[MA_MAX_PATH_LEN] = {0};
        char inventory_id[MA_MAX_PATH_LEN] = {0};
        ma_error_t err = MA_OK;
        ma_variant_t *var = NULL;
        ma_variant_t *bookv = NULL;
        ma_table_t *inventory_table = NULL;
        ma_task_time_t date = {0};
        char crn[MA_MAX_NAME+1] = {0};

        //(void)ma_inventory_info_get_email_id(info, inventory_id);
        (void)ma_inventory_info_get_contact(info, inventory_id);
        MA_MSC_SELECT(_snprintf, snprintf)(inventory_data_path, MA_MAX_PATH_LEN, "%s%s", ds_path, BOOKING_DATA_PATH);
        MA_LOG(inventory_logger, MA_LOG_SEV_INFO, "inventory id(%s), data path(%s) inventory_data_path(%s)", inventory_id, ds_path, inventory_data_path);
        (void)ma_inventory_info_get_date(info, &date);
        ma_strtime(date, crn);
        if(MA_OK == (err = ma_ds_get_variant(datastore, inventory_data_path, inventory_id, MA_VARTYPE_TABLE, &bookv))) {
            if(MA_OK == (err = ma_variant_get_table(bookv, &inventory_table))) {
                if(MA_OK == (err = ma_inventory_info_convert_to_variant(info, &var))) {
                    if(MA_OK == (err = ma_table_add_entry(inventory_table, crn, var))) {
                        if(MA_OK == (err = ma_ds_set_variant(datastore, inventory_data_path, inventory_id, bookv)))
                            MA_LOG(inventory_logger, MA_LOG_SEV_TRACE, "inventory datastore write inventory id(%s) for crn(%s) successful", inventory_id, crn);
                        else
                            MA_LOG(inventory_logger, MA_LOG_SEV_ERROR, "inventory datastore write inventory id(%s) failed for crn(%s), last error(%d)", inventory_id, crn, err);
                    }
                    (void)ma_variant_release(var);
                } else
                    MA_LOG(inventory_logger, MA_LOG_SEV_ERROR, "inventory datastore write inventory id(%s) conversion inventory info to variant failed for crn(%s), last error(%d)", inventory_id, crn, err);
                (void)ma_table_release(inventory_table);
            }
            (void)ma_variant_release(bookv);
        } else {
            MA_LOG(inventory_logger, MA_LOG_SEV_DEBUG, "inventory table for inventory id(%s) does not exist, so creating..", inventory_id);
            if(MA_OK == (err = ma_table_create(&inventory_table))) {
                if(MA_OK == (err = ma_inventory_info_convert_to_variant(info, &var))) {
                    if(MA_OK == (err = ma_table_add_entry(inventory_table, crn, var))) {
                        if(MA_OK == (err = ma_variant_create_from_table(inventory_table, &bookv))) {
                            if(MA_OK == (err = ma_ds_set_variant(datastore, inventory_data_path, inventory_id, bookv)))
                                MA_LOG(inventory_logger, MA_LOG_SEV_TRACE, "inventory datastore write inventory id(%s) for crn(%s) successful", inventory_id, crn);
                            else
                                MA_LOG(inventory_logger, MA_LOG_SEV_ERROR, "inventory datastore write inventory id(%s) failed for crn(%s), last error(%d)", inventory_id, crn, err);
                            (void)ma_variant_release(bookv);
                        }
                    }
                    (void)ma_variant_release(var);
                } else
                    MA_LOG(inventory_logger, MA_LOG_SEV_ERROR, "inventory datastore write inventory id(%s) conversion inventory info to variant failed for crn(%s), last error(%d)", inventory_id, crn, err);
                (void)ma_table_release(inventory_table);
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_datastore_write_variant(ma_inventory_t *inventory, const char *inventory_id, ma_ds_t *datastore, const char *ds_path, ma_variant_t *var) {
    if(inventory && inventory_id && var && datastore && ds_path) {
        char inventory_data_path[MA_MAX_PATH_LEN] = {0};
        ma_error_t err = MA_OK;
        ma_variant_t *bookv = NULL;
        ma_table_t *inventory_table = NULL;
        char crn[MA_MAX_NAME+1] = {0};
        ma_task_time_t date = {0};
        ma_inventory_info_t *info = NULL;

        MA_MSC_SELECT(_snprintf, snprintf)(inventory_data_path, MA_MAX_PATH_LEN, "%s%s", ds_path, BOOKING_DATA_PATH);
        MA_LOG(inventory_logger, MA_LOG_SEV_INFO, "inventory id(%s), data path(%s) inventory_data_path(%s)", inventory_id, ds_path, inventory_data_path);
        if(MA_OK == (err = ma_ds_get_variant(datastore, inventory_data_path, inventory_id, MA_VARTYPE_TABLE, &bookv))) {
            if(MA_OK == (err = ma_variant_get_table(bookv, &inventory_table))) {
                if(MA_OK == (err = ma_inventory_info_convert_from_variant(var, &info))) {
                    (void)ma_inventory_info_get_date(info, &date);
                    ma_strtime(date, crn);
                    if(MA_OK == (err = ma_table_add_entry(inventory_table, crn, var))) {
                        if(MA_OK == (err = ma_ds_set_variant(datastore, inventory_data_path, inventory_id, bookv)))
                            MA_LOG(inventory_logger, MA_LOG_SEV_TRACE, "inventory datastore write inventory id(%s) for crn(%s) successful", inventory_id, crn);
                        else
                            MA_LOG(inventory_logger, MA_LOG_SEV_ERROR, "inventory datastore write inventory id(%s) failed for crn(%s), last error(%d)", inventory_id, crn, err);
                    }
                    (void)ma_inventory_info_release(info);
                } else
                    MA_LOG(inventory_logger, MA_LOG_SEV_ERROR, "inventory datastore write inventory id(%s) conversion inventory info to variant failed for crn(%s), last error(%d)", inventory_id, crn, err);
                (void)ma_table_release(inventory_table);
            }
            (void)ma_variant_release(bookv);
        } else {
            MA_LOG(inventory_logger, MA_LOG_SEV_DEBUG, "inventory table for inventory id(%s) does not exist, so creating..", inventory_id);
            if(MA_OK == (err = ma_table_create(&inventory_table))) {
                if(MA_OK == (err = ma_inventory_info_convert_from_variant(var, &info))) {
                    (void)ma_inventory_info_get_date(info, &date);
                    ma_strtime(date, crn);
                    if(MA_OK == (err = ma_table_add_entry(inventory_table, crn, var))) {
                        if(MA_OK == (err = ma_variant_create_from_table(inventory_table, &bookv))) {
                            if(MA_OK == (err = ma_ds_set_variant(datastore, inventory_data_path, inventory_id, bookv)))
                                MA_LOG(inventory_logger, MA_LOG_SEV_TRACE, "inventory datastore write inventory id(%s) for crn(%s) successful", inventory_id, crn);
                            else
                                MA_LOG(inventory_logger, MA_LOG_SEV_ERROR, "inventory datastore write inventory id(%s) failed for crn(%s), last error(%d)", inventory_id, crn, err);
                            (void)ma_variant_release(bookv);
                        }
                    }
                    (void)ma_inventory_info_release(info);
                } else
                    MA_LOG(inventory_logger, MA_LOG_SEV_ERROR, "inventory datastore write inventory id(%s) conversion inventory info from variant failed, last error(%d)", inventory_id, err);
                (void)ma_table_release(inventory_table);
            }
        }
        return err;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_inventory_datastore_remove_inventory(const char *inventory, ma_ds_t *datastore, const char *ds_path) {
    if(inventory && datastore && ds_path) {
        char inventory_path[MA_MAX_PATH_LEN] = {0};
        
        MA_LOG(inventory_logger, MA_LOG_SEV_DEBUG, "inventory removing (%s) from datastore", inventory);
        MA_MSC_SELECT(_snprintf, snprintf)(inventory_path, MA_MAX_PATH_LEN, "%s%s",ds_path, BOOKING_DATA_PATH );/* base_path\"inventory_data"\4097 */
        return ma_ds_rem(datastore, inventory_path, inventory, 1);
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_datastore_get(ma_inventory_t *inventory, ma_ds_t *datastore, const char *ds_path, const char *inventory_id, const char *crn, ma_inventory_info_t **info) {
    if(inventory && datastore && ds_path && inventory_id && info) {
        ma_error_t err = MA_OK;
        char inventory_data_base_path[MA_MAX_PATH_LEN] = {0};
        ma_variant_t *inventory_var = NULL;

        MA_MSC_SELECT(_snprintf, snprintf)(inventory_data_base_path, MA_MAX_PATH_LEN, "%s%s", ds_path, BOOKING_DATA_PATH);
        if(MA_OK == (err = ma_ds_get_variant(datastore, inventory_data_base_path, inventory_id, MA_VARTYPE_TABLE, &inventory_var))) {
            ma_table_t *inventory_table = NULL;

            if(MA_OK == (err = ma_variant_get_table(inventory_var, &inventory_table))) {
                ma_variant_t *bookv = NULL;

                if(MA_OK == (err = ma_table_get_value(inventory_table, crn, &bookv))) {
                    if(MA_OK == (err = ma_inventory_info_convert_from_variant(bookv, info)))
                        MA_LOG(inventory_logger, MA_LOG_SEV_TRACE, "inventory(%s) exist in inventory DB", inventory_id);
                    else
                        MA_LOG(inventory_logger, MA_LOG_SEV_ERROR, "inventory(%s) variant to inventory conversion failed, last error(%d)", crn, err);
                    (void)ma_variant_release(bookv);
                } else
                    MA_LOG(inventory_logger, MA_LOG_SEV_ERROR, "inventory id(%s) crn(%s) does not exist, last error(%d)", inventory_id, crn, err);
                (void)ma_table_release(inventory_table);
            }
            (void)ma_variant_release(inventory_var);
        } else 
            MA_LOG(inventory_logger, MA_LOG_SEV_ERROR, "inventory(%s) does not exist in inventory DB, last error(%d)", inventory_id, err);

        return err;
    }
    return MA_ERROR_INVALIDARG;

}

ma_error_t ma_inventory_datastore_clear(ma_ds_t *datastore, const char *ds_path) {
    return(datastore && ds_path) ? ma_ds_rem(datastore, ds_path, NULL, 1) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_datastore_get_all(ma_inventory_t *inventory, ma_ds_t *datastore, const char *ds_path, const char *inventory_id, ma_variant_t **var) {
    if(inventory && datastore && ds_path && inventory_id && var) {
        ma_error_t err = MA_OK;
        char inventory_data_base_path[MA_MAX_PATH_LEN] = {0};

        MA_MSC_SELECT(_snprintf, snprintf)(inventory_data_base_path, MA_MAX_PATH_LEN, "%s%s", ds_path, BOOKING_DATA_PATH);
        if(MA_OK == (err = ma_ds_get_variant(datastore, inventory_data_base_path, inventory_id, MA_VARTYPE_TABLE, var))) {
            MA_LOG(inventory_logger, MA_LOG_SEV_TRACE, "inventory id(%s) DB get successful", inventory_id);
        } else
            MA_LOG(inventory_logger, MA_LOG_SEV_ERROR, "inventory id(%s) does not exist in DB, last error(%d)", inventory_id, err);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_datastore_remove_inventory_crn(ma_inventory_t *inventory, const char *inventory_id, const char *crn, ma_ds_t *datastore, const char *ds_path) {
    if(inventory && inventory_id && crn && datastore && ds_path) {
         ma_error_t err = MA_OK;
         ma_variant_t *var = NULL;

         if(MA_OK == (err = ma_inventory_datastore_get_all(inventory, datastore, ds_path, inventory_id, &var))) {
             ma_table_t *inventory_table = NULL;

             if(MA_OK == (err = ma_variant_get_table(var, &inventory_table))) {
                 if(MA_OK == (err = ma_table_remove_entry(inventory_table,crn))) {
                     char inventory_data_path[MA_MAX_PATH_LEN] = {0};

                     MA_LOG(inventory_logger, MA_LOG_SEV_TRACE, "inventory id(%s) crn (%s) removed successfully", inventory_id, crn);
                     MA_MSC_SELECT(_snprintf, snprintf)(inventory_data_path, MA_MAX_PATH_LEN, "%s%s", ds_path, BOOKING_DATA_PATH);
                     if(MA_OK == (err = ma_ds_set_variant(datastore, inventory_data_path, inventory_id, var)))
                         MA_LOG(inventory_logger, MA_LOG_SEV_TRACE, "inventory id(%s) updated successfully", inventory_id);
                     else
                         MA_LOG(inventory_logger, MA_LOG_SEV_ERROR, "inventory id(%s) update fialed, last error(%d)", inventory_id, err);
                 } else
                     MA_LOG(inventory_logger, MA_LOG_SEV_ERROR, "inventory id(%s) crn(%s) does not exist in DB, last error(%d)", inventory_id ,crn, err);
                 (void)ma_table_release(inventory_table);
             }
             (void)ma_variant_release(var);
         }

         return err;
    }
    return MA_ERROR_INVALIDARG;
}

