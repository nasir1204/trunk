#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include "zlib.h"
#include "unzip.h"
#include "ma/ma_common.h"
#if defined(WIN32)
	#include <direct.h>
	#include <Windows.h>
#else
	# include <unistd.h>
	# include <utime.h>
#endif

#define UNIX_FILETYPE_MASK    0170000
#define UNIX_FILETYPE_SYMLINK 0120000
#define WRITEBUFFERSIZE (8192)


static int version_does_symlinks(unsigned int version)
{
   int retval = 0;
   unsigned char hosttype = (unsigned char) ((version >> 8) & 0xFF);

   switch (hosttype)
   {
        /*
            * These are the platforms that can NOT build an archive with
            *  symlinks, according to the Info-ZIP project.
        */
        case 0:  /* FS_FAT_  */
        case 1:  /* AMIGA_   */
        case 2:  /* VMS_     */
        case 4:  /* VM_CSM_  */
        case 6:  /* FS_HPFS_ */
        case 11: /* FS_NTFS_ */
        case 14: /* FS_VFAT_ */
        case 13: /* ACORN_   */
        case 15: /* MVS_     */
        case 18: /* THEOS_   */
            break;  /* do nothing. */
        default:  /* assume the rest to be unix-like. */
            retval = 1;
            break;
    } /* switch */

    return(retval);
} /* zip_version_does_symlinks */

static int  entry_is_symlink(unz_file_info *info)
{
    unsigned int xattr = ((info->external_fa  >> 16) & 0xFFFF);

    return (
              (version_does_symlinks(info->version)) &&
              (info->uncompressed_size > 0) &&
              ((xattr & UNIX_FILETYPE_MASK) == UNIX_FILETYPE_SYMLINK)
           );
} /* zip_has_symlink_attr */

static void strip_file_name(char *pathname , char *dirname)
{
	int len ;
    int i  ;
    if((NULL == dirname) || (NULL == pathname))
            return  ;
	i = len = strlen(pathname);        

    for( ; i >=0 ; i-- )
            if(pathname[i] == '/')
                    break;
    if(i < 0 )
            return ;
    strncpy(dirname , pathname , i );
    dirname[i] = '\0';
}


/* change_file_date : change the date/time of a file
    filename : the filename of the file where date/time must be modified
    dosdate : the new date at the MSDos format (4 bytes)
    tmu_date : the SAME new date at the tm_unz format */
static void change_file_date(const char *filename,uLong dosdate,tm_unz tmu_date)
{
#if defined(WIN32)
  HANDLE hFile;
  FILETIME ftm,ftLocal,ftCreate,ftLastAcc,ftLastWrite;

  hFile = CreateFileA(filename,GENERIC_READ | GENERIC_WRITE,
                      0,NULL,OPEN_EXISTING,0,NULL);
  GetFileTime(hFile,&ftCreate,&ftLastAcc,&ftLastWrite);
  DosDateTimeToFileTime((WORD)(dosdate>>16),(WORD)dosdate,&ftLocal);
  LocalFileTimeToFileTime(&ftLocal,&ftm);
  SetFileTime(hFile,&ftm,&ftLastAcc,&ftm);
  CloseHandle(hFile);
#else
  struct utimbuf ut;
  struct tm newdate;
  newdate.tm_sec = tmu_date.tm_sec;
  newdate.tm_min=tmu_date.tm_min;
  newdate.tm_hour=tmu_date.tm_hour;
  newdate.tm_mday=tmu_date.tm_mday;
  newdate.tm_mon=tmu_date.tm_mon;
  if (tmu_date.tm_year > 1900)
      newdate.tm_year=tmu_date.tm_year - 1900;
  else
      newdate.tm_year=tmu_date.tm_year ;
  newdate.tm_isdst=-1;

  ut.actime=ut.modtime=mktime(&newdate);
  utime(filename,&ut);
#endif
}

/* mymkdir and change_file_date are not 100 % portable
   As I don't know well Unix, I wait feedback for the unix portion */

int mymkdir(const char* dirname)
   
{
    int ret=0;
#if defined(WIN32)
    ret = mkdir(dirname);
#else
    ret = mkdir (dirname,0775);
#endif
    return ret;
}

static int makedir (char *newdir){
  char *buffer ;
  char *p;
  int  len = (int)strlen(newdir);

  if (len <= 0)
    return 0;

  buffer = (char*)calloc(1, len+1);
  if(NULL == buffer)
		return 0 ;
  strncpy(buffer,newdir , len);
  if (buffer[len-1] == '/') {
    buffer[len-1] = '\0';
  }
  if (mymkdir(buffer) == 0) {
      free(buffer);
      return 1;
  }
  p = buffer+1;
  while (1){
      char hold;
	  while(*p && *p != '\\' && *p != '/')
        p++;
      hold = *p;
      *p = 0;
      if ((mymkdir(buffer) == -1) && (errno == ENOENT)) {
          free(buffer);
          return 0;
	  }
      if (hold == 0)
        break;
      *p++ = hold;
  }
  free(buffer);
  return 1;
}

static int do_extract_currentfile(unzFile uf){	
	char *filename_inzip = NULL;
	int err=UNZ_OK;
	FILE *fout=NULL;
	void* buf;
	uInt size_buf;
	int  sym_link = 0 ;
	int filesize = 4096 ;
	unz_file_info file_info;
	filename_inzip = (char *)calloc(1, filesize);
	if(filename_inzip == NULL)
			return UNZ_INTERNALERROR ;	
	err = unzGetCurrentFileInfo(uf,&file_info,filename_inzip,filesize,NULL,0,NULL,0);

	if (err != UNZ_OK){
		free(filename_inzip);
        return err;
	}

	size_buf = WRITEBUFFERSIZE;
	buf = (void*)calloc(1, size_buf);
	if (buf == NULL){
		free(filename_inzip);
		return UNZ_INTERNALERROR;
	}

	{
		char *dirname = NULL;
		dirname = (char *)calloc(1, filesize);
		if(NULL == dirname)
		{
			free(filename_inzip);
			free(buf);
			return UNZ_INTERNALERROR;
		}

		strip_file_name(filename_inzip , dirname );
		if(dirname[0] != '\0')
        		makedir(dirname);
		free(dirname);
	}

	err = unzOpenCurrentFilePassword(uf,NULL); 
	if(err != UNZ_OK ){
		free(filename_inzip);
		free(buf);
		return err ;
	}

#if !defined(WIN32)/* No symlinks on windows */

	if((err == UNZ_OK) && entry_is_symlink(&file_info)) {
                sym_link = 1 ;
                err = unzReadCurrentFile(uf,buf,size_buf);
		if(err > 0 )
			err = UNZ_OK ;
                if(symlink(buf , filename_inzip) != 0 )
                	err = UNZ_ERRNO ;
        }
#endif


	if ((err == UNZ_OK) && !sym_link )
		fout=fopen(filename_inzip,"wb");

	if (fout != NULL){
		do{
			err = unzReadCurrentFile(uf,buf,size_buf);
			if (err<0)
				break;
			if (err>0)
				if (fwrite(buf,err,1,fout)!=1){
					err=UNZ_ERRNO;
					break;
				}
		}
		while ( err > 0 );
		if (fout)
			fclose(fout);
			
		if (err == 0)
			change_file_date(filename_inzip,file_info.dosDate,
								file_info.tmu_date);
	}

	err = unzCloseCurrentFile (uf);
	free(buf);
	free(filename_inzip);
	return err;
}

static ma_error_t do_extract(unzFile uf){
    uLong i;
    unz_global_info gi;
    int err;

    err = unzGetGlobalInfo (uf,&gi);
    if(err != UNZ_OK)
		return MA_ERROR_APIFAILED;

    for (i=0;i<gi.number_entry;i++) {
        if (do_extract_currentfile(uf ) != UNZ_OK)
		break ;

        if ((i+1)<gi.number_entry) {
            err = unzGoToNextFile(uf);
            if (err != UNZ_OK)	
				return MA_ERROR_APIFAILED ;
        }
    }
	
	return MA_OK;
}

ma_error_t ma_unzip(const char *zipfilename , const char *destdirname){
	ma_error_t rc = MA_ERROR_APIFAILED;	
	char *old_dir_name = NULL ;
	unzFile uf=NULL;
	old_dir_name = (char *)calloc(1, (4096));
	if(NULL == old_dir_name )
			return MA_ERROR_APIFAILED ;
	
    if(zipfilename != NULL)	{

		#ifdef USEWIN32IOAPI
        		zlib_filefunc_def ffunc;
		#endif

		#ifdef USEWIN32IOAPI
        		fill_win32_filefunc(&ffunc);
        		uf = unzOpen2(zipfilename,&ffunc);
		#else
        		uf = unzOpen(zipfilename);
		#endif
	}

	if (uf == NULL) {
		free(old_dir_name);
		return MA_ERROR_APIFAILED ;
	}
	
	getcwd(old_dir_name , 4096);
	if (destdirname && chdir(destdirname)){
		unzClose(uf);
		free(old_dir_name);
		return MA_ERROR_APIFAILED ;
	}
	rc =  do_extract( uf );
	// PR# 438111 - use unzClose() instead of unzCloseCurrentFile()
	unzClose(uf);
	if(destdirname)
		chdir(old_dir_name);

	free(old_dir_name);
	return rc ;
}
