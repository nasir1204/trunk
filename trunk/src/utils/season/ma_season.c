#include "ma/internal/utils/season/ma_season.h"
#include "ma/internal/utils/season/ma_season_info.h"
#include "ma/internal/utils/season/ma_season_datastore.h"
#include "ma/ma_msgbus.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/defs/ma_season_defs.h"
#include "ma/internal/ma_macros.h"

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "season"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <uv.h>

ma_logger_t *season_logger;

struct ma_season_s {
    char path[MA_MAX_PATH_LEN+1];
    ma_msgbus_t *msgbus;
    ma_ds_t* datastore;
};


ma_error_t ma_season_create(ma_msgbus_t *msgbus, ma_ds_t *datastore, const char *base_path, ma_season_t **season) {
    if(msgbus && datastore && base_path && season) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;

        if((*season = (ma_season_t*)calloc(1, sizeof(ma_season_t)))) {
            (*season)->msgbus = msgbus;
            strncpy((*season)->path, base_path, MA_MAX_PATH_LEN);
            (*season)->datastore = datastore;
            err = MA_OK;
            MA_LOG(season_logger, MA_LOG_SEV_INFO, "season created successfully");
        }
        if(MA_OK != err) {
            MA_LOG(season_logger, MA_LOG_SEV_ERROR, "season creation failed, last error(%d)", err);
            if(*season)
                (void)ma_season_release(*season);
            *season = NULL;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_add(ma_season_t *season, ma_season_info_t *info) {
    return season && info ? ma_season_datastore_write(season, info, season->datastore, season->path) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_add_variant(ma_season_t *season, const char *season_id, ma_variant_t *var) {
    return season && season_id && var ? ma_season_datastore_write_variant(season, season_id, season->datastore, season->path, var) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_delete(ma_season_t *season, const char *season_id) {
    return season && season_id ? ma_season_datastore_remove_season(season_id, season->datastore, season->path) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_delete_all(ma_season_t *season) {
    return season ? ma_season_datastore_clear(season->datastore, season->path) : MA_ERROR_INVALIDARG;
}

static ma_bool_t update_season_attributes(ma_season_info_t *info) {
    if(info) {
        const char *url = NULL;
        ma_bool_t update_req = MA_FALSE;

        if(MA_OK == ma_season_info_get_image_small_url(info, &url) && url && !strstr(url, "compact")) {
            char buf[MA_MAX_BUFFER_LEN+1] = {0};
            MA_MSC_SELECT(_snprintf, snprintf)(buf, MA_MAX_BUFFER_LEN, "%s%s", MA_SEASON_SMALL_IMAGE_URL, url); 
            (void)ma_season_info_set_image_small_url(info, buf);
            update_req = MA_TRUE;
        }
        if(MA_OK == ma_season_info_get_url(info, &url) && url && !strstr(url, "large")) {
            char buf[MA_MAX_BUFFER_LEN+1] = {0};
            MA_MSC_SELECT(_snprintf, snprintf)(buf, MA_MAX_BUFFER_LEN, "%s%s", MA_SEASON_LARGE_IMAGE_URL, url); 
            (void)ma_season_info_set_url(info, buf);
            update_req = MA_TRUE;
        }

        return update_req;
    }
    return MA_FALSE;
}

ma_error_t ma_season_get(ma_season_t *season, const char *season_id, ma_season_info_t **info) {
    if(season && season_id && info) {
        ma_error_t err = MA_OK;

        if(MA_OK == (err = ma_season_datastore_get(season, season->datastore, season->path, season_id, info))) {
            if(update_season_attributes(*info)) {
                err = ma_season_add(season, *info);
            }
        }
        return err;
    }
    return MA_ERROR_INVALIDARG;
    //return season && season && info ? ma_season_datastore_get(season, season->datastore, season->path, season_id, info) : MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_set_logger(ma_logger_t *logger) {
    if(logger) {
        season_logger = logger;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_get_msgbus(ma_season_t *season, ma_msgbus_t **msgbus) {
    if(season && msgbus) {
        *msgbus = season->msgbus;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_start(ma_season_t *season) {
    if(season) {
        MA_LOG(season_logger, MA_LOG_SEV_TRACE, "season started");
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_stop(ma_season_t *season) {
    if(season) {
        MA_LOG(season_logger, MA_LOG_SEV_TRACE, "season stopped");
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_season_release(ma_season_t *season) {
    if(season) {
        free(season);
    }
    return MA_ERROR_INVALIDARG;
}

