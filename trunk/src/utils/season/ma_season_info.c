#include "ma/internal/utils/season/ma_season_info.h"
#include "ma/internal/utils/json/ma_cjson.h"
#include "ma/internal/utils/threading/ma_atomic.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/defs/ma_season_defs.h"
#include "ma/ma_log.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

extern ma_logger_t *season_logger;

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "season"

const char g_double_quote_char = '\"';

/*
{
			"domain": "Uploaded by user",
			"attribution": null,
			"description": " ",
			"pinner": {
				"about": "Buzzintown.com is India's leading Events &amp; Entertainment portal. Know about events in your city-Buy Tickets/Passes for Concerts, Movies, Plays, Nightlife",
				"location": "Bangalore",
				"full_name": "Buzzintown .com",
				"follower_count": 870,
				"image_small_url": "http://media-cache-ak0.pinimg.com/avatars/buzzintown_1336480713_30.jpg",
				"pin_count": 3762,
				"id": "65443138243662511",
				"profile_url": "http://www.pinterest.com/buzzintown/"
			},
			"repin_count": 4,
			"dominant_color": "#bf6d52",
			"like_count": 2,
			"link": null,
			"images": {
				"237x": {
					"url": "http://media-cache-ec0.pinimg.com/237x/0f/07/16/0f07160cd374a8fcbb0705ef86afc2f7.jpg",
					"width": 237,
					"height": 297
				}
			},
			"embed": null,
			"is_video": false,
			"id": "65443000812615023"
		}
*/

struct ma_season_info_s {
    char domain[MA_MAX_NAME+1];
    char attribution[MA_MAX_NAME+1];
    char description[MA_MAX_LEN+1];
    char about[MA_MAX_BUFFER_LEN+1];
    char location[MA_MAX_LEN+1];
    char full_name[MA_MAX_LEN+1];
    int follower_count;
    char image_small_url[MA_MAX_BUFFER_LEN+1];
    int pin_count;
    char pin_id[MA_MAX_NAME+1];
    char profile_url[MA_MAX_LEN+1];
    int repin_count;
    char dominant_color[MA_MAX_NAME+1];
    int like_count;
    char link[MA_MAX_LEN+1];
    char url[MA_MAX_BUFFER_LEN+1];
    int width;
    int height;
    char embed[MA_MAX_NAME+1];
    ma_bool_t is_video;
    char id[MA_MAX_NAME+1];
    char email[MA_MAX_LEN+1];
    char contact[MA_MAX_LEN+1];
    ma_atomic_counter_t ref_count;
    ma_json_t *json;
    char topic[MA_MAX_NAME+1];
    char board[MA_MAX_NAME+1];
    char board_image_url[MA_MAX_LEN+1];
    char saved_by[MA_MAX_NAME+1];
};


ma_error_t ma_season_info_create(ma_season_info_t **info) {
    if(info) {
         ma_error_t err = MA_ERROR_OUTOFMEMORY;

         if((*info = (ma_season_info_t*)calloc(1, sizeof(ma_season_info_t)))) {
             if(MA_OK == (err = ma_json_create(&(*info)->json))) {
                 (*info)->ref_count++;
             }
         }

         return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_release(ma_season_info_t *info) {
    if(info) {
        if(!--info->ref_count) {
            (void)ma_json_release(info->json);
            free(info);
        }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_add(ma_season_info_t *info) {
    if(info) {
        info->ref_count++;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


/* setter */
ma_error_t ma_season_info_set_board_image_url(ma_season_info_t *info, const char *image_url) {
    if(info && image_url && (strlen(image_url) < MA_MAX_LEN)) {
        strncpy(info->board_image_url, image_url, MA_MAX_LEN);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_set_saved_by(ma_season_info_t *info, const char *user) {
    if(info && user && (strlen(user) < MA_MAX_NAME)) {
        strncpy(info->saved_by, user, MA_MAX_NAME);
        ma_trim_string_by_char(info->saved_by, g_double_quote_char);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_set_ref_count(ma_season_info_t *info, size_t count) {
    if(info) {
        info->ref_count = count;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_set_email_id(ma_season_info_t *info, const char *id) {
    if(info && id && (strlen(id) < MA_MAX_LEN)) {
        strncpy(info->email, id, MA_MAX_LEN);
        ma_trim_string_by_char(info->email, g_double_quote_char);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_set_contact(ma_season_info_t *info, const char *id) {
    if(info && id && (strlen(id) < MA_MAX_LEN)) {
        strncpy(info->contact, id, MA_MAX_LEN);
        ma_trim_string_by_char(info->contact, g_double_quote_char);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_set_domain(ma_season_info_t *info, const char *domain) {
    if(info && domain && (strlen(domain) < MA_MAX_NAME)) {
        strncpy(info->domain, domain, MA_MAX_NAME);
        ma_trim_string_by_char(info->domain, g_double_quote_char);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_set_attribution(ma_season_info_t *info, const char *attri) {
    if(info && attri && (strlen(attri) < MA_MAX_NAME)) {
        strncpy(info->attribution, attri, MA_MAX_NAME);
        ma_trim_string_by_char(info->attribution, g_double_quote_char);
        return MA_OK;
    } 
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_set_description(ma_season_info_t *info, const char *descr) {
    if(info && descr && (strlen(descr) < MA_MAX_LEN)) {
        strncpy(info->description, descr, MA_MAX_LEN);
        ma_trim_string_by_char(info->description, g_double_quote_char);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_set_about(ma_season_info_t *info, const char *about) {
    if(info && about && (strlen(about) < MA_MAX_BUFFER_LEN)) {
        strncpy(info->about, about, MA_MAX_BUFFER_LEN);
        ma_trim_string_by_char(info->about, g_double_quote_char);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_set_location(ma_season_info_t *info, const char *loc) {
    if(info && loc && (strlen(loc) < MA_MAX_LEN)) {
        strncpy(info->location, loc, MA_MAX_LEN);
        ma_trim_string_by_char(info->location, g_double_quote_char);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_set_full_name(ma_season_info_t *info, const char *name) {
    if(info && name && (strlen(name) < MA_MAX_LEN)) {
        strncpy(info->full_name, name, MA_MAX_LEN);
        ma_trim_string_by_char(info->full_name, g_double_quote_char);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_set_follower_count(ma_season_info_t *info, int count) {
    if(info) {
        info->follower_count = count;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_set_image_small_url(ma_season_info_t *info, const char *url) {
    if(info && url && (strlen(url) < MA_MAX_BUFFER_LEN)) {
        strncpy(info->image_small_url, url, MA_MAX_BUFFER_LEN);
        return MA_TRUE;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_set_pin_count(ma_season_info_t *info, int count) {
    if(info) {
        info->pin_count = count;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_set_pin_id(ma_season_info_t *info, const char *id) {
    if(info && id && (strlen(id) < MA_MAX_NAME)) {
        strncpy(info->pin_id, id, MA_MAX_NAME);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_set_profile_url(ma_season_info_t *info, const char *url) {
    if(info && url && (strlen(url) < MA_MAX_LEN)) {
        strncpy(info->profile_url, url, MA_MAX_LEN);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_set_repin_count(ma_season_info_t *info, int count) {
    if(info) {
        info->repin_count = count;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_set_dominant_color(ma_season_info_t *info, const char *color) {
    if(info && color && (strlen(color) < MA_MAX_NAME)) {
        strncpy(info->dominant_color, color, MA_MAX_NAME);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_set_like_count(ma_season_info_t *info, int count) {
    if(info) {
        info->like_count = count;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_set_link(ma_season_info_t *info, const char *link) {
    if(info && link && (strlen(link) < MA_MAX_LEN)) {
        strncpy(info->link, link, MA_MAX_LEN);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_set_url(ma_season_info_t *info, const char *url) {
    if(info && url && (strlen(url) < MA_MAX_BUFFER_LEN)) {
        strncpy(info->url, url, MA_MAX_BUFFER_LEN);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_set_width(ma_season_info_t *info, int width) {
    if(info) {
        info->width = width;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_set_height(ma_season_info_t *info, int height) {
    if(info) {
        info->height = height;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_set_embed(ma_season_info_t *info, const char *embed) {
    if(info && embed && (strlen(embed) < MA_MAX_NAME)) {
        strncpy(info->embed, embed, MA_MAX_NAME);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_set_is_video(ma_season_info_t *info, ma_bool_t is_video) {
    if(info) {
        info->is_video = is_video;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_set_id(ma_season_info_t *info, const char *id) {
    if(info && id && (strlen(id) < MA_MAX_NAME)) {
        strncpy(info->id, id, MA_MAX_NAME);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_set_topic(ma_season_info_t *info, const char *topic) {
    if(info && topic && (strlen(topic) < MA_MAX_NAME)) {
        strncpy(info->topic, topic, MA_MAX_NAME);
        ma_trim_string_by_char(info->topic, g_double_quote_char);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_set_board(ma_season_info_t *info, const char *board) {
    if(info && board && (strlen(board) < MA_MAX_NAME)) {
        strncpy(info->board, board, MA_MAX_NAME);
        ma_trim_string_by_char(info->board, g_double_quote_char);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}
/* getter */
ma_error_t ma_season_info_get_board_image_url(ma_season_info_t *info, const char **image_url) {
    if(info && image_url) {
        *image_url = info->board_image_url;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_get_saved_by(ma_season_info_t *info, const char **user) {
    if(info && user) {
        *user = info->saved_by;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_get_topic(ma_season_info_t *info, const char **topic) {
    if(info && topic) {
        *topic = info->topic;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_get_board(ma_season_info_t *info, const char **board) {
    if(info && board) {
        *board = info->board;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_get_ref_count(ma_season_info_t *info, size_t *count) {
    if(info && count) {
        *count = info->ref_count;
        return MA_OK;
    }
    return MA_OK;
}

ma_error_t ma_season_info_get_email_id(ma_season_info_t *info, const char **id) {
    if(info && id) {
        *id = info->email;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_get_contact(ma_season_info_t *info, const char **id) {
    if(info && id) {
        *id = info->contact;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}
ma_error_t ma_season_info_get_domain(ma_season_info_t *info, const char **domain) {
    if(info && domain) {
        *domain = info->domain;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_get_attribution(ma_season_info_t *info, const char **attri) {
    if(info && attri) {
        *attri = info->attribution;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_get_description(ma_season_info_t *info, const char **descr) {
    if(info && descr) {
        *descr = info->description;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_get_about(ma_season_info_t *info, const char **about) {
    if(info && about) {
        *about = info->about;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_get_location(ma_season_info_t *info, const char **loc) {
    if(info && loc) {
        *loc = info->location;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_get_full_name(ma_season_info_t *info, const char **name) {
    if(info && name) {
        *name = info->full_name;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_get_follower_count(ma_season_info_t *info, int *count) {
    if(info && count) {
        *count = info->follower_count;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_get_image_small_url(ma_season_info_t *info, const char **url) {
    if(info && url) {
        *url = info->image_small_url;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_get_pin_count(ma_season_info_t *info, int *count) {
    if(info && count) {
        *count = info->pin_count;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_get_pin_id(ma_season_info_t *info, const char **id) {
    if(info && id) {
        *id = info->pin_id;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_get_profile_url(ma_season_info_t *info, const char **url) {
    if(info && url) {
        *url = info->profile_url;
        return MA_OK;
    }
    return MA_OK;
}

ma_error_t ma_season_info_get_repin_count(ma_season_info_t *info, int *count) {
    if(info && count) {
        *count = info->repin_count;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_get_dominant_color(ma_season_info_t *info, const char **color) {
    if(info && color) {
        *color = info->dominant_color;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_get_like_count(ma_season_info_t *info, int *count) {
    if(info && count) {
        *count = info->like_count;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_get_link(ma_season_info_t *info, const char **link) {
    if(info && link) {
        *link = info->link;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_get_url(ma_season_info_t *info, const char **url) {
    if(info && url) {
        *url = info->url;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_get_width(ma_season_info_t *info, int *width) {
    if(info && width) {
        *width = info->width;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_get_height(ma_season_info_t *info, int *height) {
    if(info && height) {
        *height = info->height;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_get_embed(ma_season_info_t *info, const char **embed) {
    if(info && embed) {
        *embed = info->embed;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_get_is_video(ma_season_info_t *info, ma_bool_t *is_video) {
    if(info && is_video) {
        *is_video = info->is_video;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_get_id(ma_season_info_t *info, const char **id) {
    if(info && id) {
        *id = info->id;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_get_json(ma_season_info_t *info, ma_json_t **json) {
    if(info && json) {
        *json = info->json;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_convert_to_variant(ma_season_info_t *info, ma_variant_t **variant) {
    if(info && variant) {
        ma_error_t err = MA_OK;
        ma_table_t *info_table = NULL;

        if(MA_OK == (err = ma_table_create(&info_table))) {
             {
                 const char *p = NULL;

                 if(MA_OK == (err = ma_season_info_get_email_id(info, &p))) {
                     ma_variant_t *v =NULL;

                     if(MA_OK == (err = ma_variant_create_from_string(p, strlen(p), &v))) {
                         if(MA_OK != (err = ma_table_add_entry(info_table, MA_SEASON_INFO_ATTR_EMAIL_ID, v))) {
                             MA_LOG(season_logger, MA_LOG_SEV_ERROR, "%s failed to add in season table, last error(%d)", MA_SEASON_INFO_ATTR_EMAIL_ID, err);
                         }
                         (void)ma_variant_release(v);
                     }
                 }
             }

             {
                 const char *p = NULL;

                 if(MA_OK == (err = ma_season_info_get_contact(info, &p))) {
                     ma_variant_t *v =NULL;

                     if(MA_OK == (err = ma_variant_create_from_string(p, strlen(p), &v))) {
                         if(MA_OK != (err = ma_table_add_entry(info_table, MA_SEASON_INFO_ATTR_CONTACT, v))) {
                             MA_LOG(season_logger, MA_LOG_SEV_ERROR, "%s failed to add in season table, last error(%d)", MA_SEASON_INFO_ATTR_CONTACT, err);
                         }
                         (void)ma_variant_release(v);
                     }
                 }
             }
             {
                  const char *p = NULL;

                  if(MA_OK == (err = ma_season_info_get_domain(info, &p))) {
                     ma_variant_t *v = NULL;

                     if(MA_OK == (err = ma_variant_create_from_string(p, strlen(p), &v))) {
                         if(MA_OK != (err = ma_table_add_entry(info_table, MA_SEASON_INFO_ATTR_DOMAIN, v))) {
                             MA_LOG(season_logger, MA_LOG_SEV_ERROR, "%s failed to add in season table, last error(%d)", MA_SEASON_INFO_ATTR_DOMAIN, err);
                         }
                         (void)ma_variant_release(v);
                     }
                  }
             }

             {
                  const char *p = NULL;

                  if(MA_OK == (err = ma_season_info_get_attribution(info, &p))) {
                     ma_variant_t *v = NULL;

                     if(MA_OK == (err = ma_variant_create_from_string(p, strlen(p), &v))) {
                         if(MA_OK != (err = ma_table_add_entry(info_table, MA_SEASON_INFO_ATTR_ATTRIBUTION, v))) {
                             MA_LOG(season_logger, MA_LOG_SEV_ERROR, "%s failed to add in season table, last error(%d)", MA_SEASON_INFO_ATTR_ATTRIBUTION, err);
                         }
                         (void)ma_variant_release(v);
                     }
                  }
             }

             {
                  const char *p = NULL;

                  if(MA_OK == (err = ma_season_info_get_description(info, &p))) {
                     ma_variant_t *v = NULL;

                     if(MA_OK == (err = ma_variant_create_from_string(p, strlen(p), &v))) {
                         if(MA_OK != (err = ma_table_add_entry(info_table, MA_SEASON_INFO_ATTR_DESCRIPTION, v))) {
                             MA_LOG(season_logger, MA_LOG_SEV_ERROR, "%s failed to add in season table, last error(%d)", MA_SEASON_INFO_ATTR_DESCRIPTION, err);
                         }
                         (void)ma_variant_release(v);
                     }
                  }
             }
             {
                  const char *p = NULL;

                  if(MA_OK == (err = ma_season_info_get_about(info, &p))) {
                     ma_variant_t *v = NULL;

                     if(MA_OK == (err = ma_variant_create_from_string(p, strlen(p), &v))) {
                         if(MA_OK != (err = ma_table_add_entry(info_table, MA_SEASON_INFO_ATTR_ABOUT, v))) {
                             MA_LOG(season_logger, MA_LOG_SEV_ERROR, "%s failed to add in season table, last error(%d)", MA_SEASON_INFO_ATTR_ABOUT, err); 
                         }
                         (void)ma_variant_release(v);
                     }
                  }
             }
             {
                  const char *p = NULL;

                  if(MA_OK == (err = ma_season_info_get_location(info, &p))) {
                     ma_variant_t *v = NULL;

                     if(MA_OK == (err = ma_variant_create_from_string(p, strlen(p), &v))) {
                         if(MA_OK != (err = ma_table_add_entry(info_table, MA_SEASON_INFO_ATTR_LOCATION, v))) {
                             MA_LOG(season_logger, MA_LOG_SEV_ERROR, "%s failed to add in season table, last error(%d)", MA_SEASON_INFO_ATTR_LOCATION, err); 
                         }
                         (void)ma_variant_release(v);
                     }
                  }
             }
             {
                  const char *p = NULL;

                  if(MA_OK == (err = ma_season_info_get_full_name(info, &p))) {
                     ma_variant_t *v = NULL;

                     if(MA_OK == (err = ma_variant_create_from_string(p, strlen(p), &v))) {
                         if(MA_OK != (err = ma_table_add_entry(info_table, MA_SEASON_INFO_ATTR_FULL_NAME, v))) {
                             MA_LOG(season_logger, MA_LOG_SEV_ERROR, "%s failed to add in season table, last error(%d)", MA_SEASON_INFO_ATTR_FULL_NAME, err); 
                         }
                         (void)ma_variant_release(v);
                     }
                  }
             }
             {
                  int c = 0;

                  if(MA_OK == (err = ma_season_info_get_follower_count(info, &c))) {
                      ma_variant_t *v = NULL;

                      if(MA_OK == (err = ma_variant_create_from_int32(c, &v))) {
                          if(MA_OK != (err = ma_table_add_entry(info_table, MA_SEASON_INFO_ATTR_FOLLOWER_COUNT, v))) {
                              MA_LOG(season_logger, MA_LOG_SEV_ERROR, "%s failed to add in season table, last error(%d)", MA_SEASON_INFO_ATTR_FOLLOWER_COUNT, err); 
                          }
                          (void)ma_variant_release(v);
                      }
                  }
             }
             {
                  const char *p = NULL;

                  if(MA_OK == (err = ma_season_info_get_image_small_url(info, &p))) {
                     ma_variant_t *v = NULL;

                     if(MA_OK == (err = ma_variant_create_from_string(p, strlen(p), &v))) {
                         if(MA_OK != (err = ma_table_add_entry(info_table, MA_SEASON_INFO_ATTR_IMAGE_SMALL_URL, v))) {
                             MA_LOG(season_logger, MA_LOG_SEV_ERROR, "%s failed to add in season table, last error(%d)", MA_SEASON_INFO_ATTR_IMAGE_SMALL_URL, err); 
                         }
                         (void)ma_variant_release(v);
                     }
                  }
             }
             {
                  int c = 0;

                  if(MA_OK == (err = ma_season_info_get_pin_count(info, &c))) {
                      ma_variant_t *v = NULL;

                      if(MA_OK == (err = ma_variant_create_from_int32(c, &v))) {
                          if(MA_OK != (err = ma_table_add_entry(info_table, MA_SEASON_INFO_ATTR_PIN_COUNT, v))) {
                              MA_LOG(season_logger, MA_LOG_SEV_ERROR, "%s failed to add in season table, last error(%d)", MA_SEASON_INFO_ATTR_PIN_COUNT, err); 
                          }
                          (void)ma_variant_release(v);
                      }
                  }
             }
             {
                  const char *p = NULL;

                  if(MA_OK == (err = ma_season_info_get_pin_id(info, &p))) {
                     ma_variant_t *v = NULL;

                     if(MA_OK == (err = ma_variant_create_from_string(p, strlen(p), &v))) {
                         if(MA_OK != (err = ma_table_add_entry(info_table, MA_SEASON_INFO_ATTR_PINNER_ID, v))) {
                             MA_LOG(season_logger, MA_LOG_SEV_ERROR, "%s failed to add in season table, last error(%d)", MA_SEASON_INFO_ATTR_PINNER_ID, err); 
                         }
                         (void)ma_variant_release(v);
                     }
                  }
             }
             {
                  const char *p = NULL;

                  if(MA_OK == (err = ma_season_info_get_profile_url(info, &p))) {
                     ma_variant_t *v = NULL;

                     if(MA_OK == (err = ma_variant_create_from_string(p, strlen(p), &v))) {
                         if(MA_OK != (err = ma_table_add_entry(info_table, MA_SEASON_INFO_ATTR_PROFILE_URL, v))) {
                             MA_LOG(season_logger, MA_LOG_SEV_ERROR, "%s failed to add in season table, last error(%d)", MA_SEASON_INFO_ATTR_PROFILE_URL, err); 
                         }
                         (void)ma_variant_release(v);
                     }
                  }
             }
             {
                  int c = 0;

                  if(MA_OK == (err = ma_season_info_get_repin_count(info, &c))) {
                      ma_variant_t *v = NULL;

                      if(MA_OK == (err = ma_variant_create_from_int32(c, &v))) {
                          if(MA_OK != (err = ma_table_add_entry(info_table, MA_SEASON_INFO_ATTR_REPIN_COUNT, v))) {
                              MA_LOG(season_logger, MA_LOG_SEV_ERROR, "%s failed to add in season table, last error(%d)", MA_SEASON_INFO_ATTR_REPIN_COUNT, err); 
                          }
                          (void)ma_variant_release(v);
                      }
                  }
             }
             {
                  const char *p = NULL;

                  if(MA_OK == (err = ma_season_info_get_dominant_color(info, &p))) {
                     ma_variant_t *v = NULL;

                     if(MA_OK == (err = ma_variant_create_from_string(p, strlen(p), &v))) {
                         if(MA_OK != (err = ma_table_add_entry(info_table, MA_SEASON_INFO_ATTR_DOMINANT_COLOR, v))) {
                             MA_LOG(season_logger, MA_LOG_SEV_ERROR, "%s failed to add in season table, last error(%d)", MA_SEASON_INFO_ATTR_DOMINANT_COLOR, err); 
                         }
                         (void)ma_variant_release(v);
                     }
                  }
             }
             {
                  int c = 0;

                  if(MA_OK == (err = ma_season_info_get_like_count(info, &c))) {
                      ma_variant_t *v = NULL;

                      if(MA_OK == (err = ma_variant_create_from_int32(c, &v))) {
                          if(MA_OK != (err = ma_table_add_entry(info_table, MA_SEASON_INFO_ATTR_LIKE_COUNT, v))) {
                              MA_LOG(season_logger, MA_LOG_SEV_ERROR, "%s failed to add in season table, last error(%d)", MA_SEASON_INFO_ATTR_LIKE_COUNT, err); 
                          }
                          (void)ma_variant_release(v);
                      }
                  }
             }
             {
                  const char *p = NULL;

                  if(MA_OK == (err = ma_season_info_get_link(info, &p))) {
                     ma_variant_t *v = NULL;

                     if(MA_OK == (err = ma_variant_create_from_string(p, strlen(p), &v))) {
                         if(MA_OK != (err = ma_table_add_entry(info_table, MA_SEASON_INFO_ATTR_LINK, v))) {
                             MA_LOG(season_logger, MA_LOG_SEV_ERROR, "%s failed to add in season table, last error(%d)", MA_SEASON_INFO_ATTR_LINK, err); 
                         }
                         (void)ma_variant_release(v);
                     }
                  }
             }
             {
                  const char *p = NULL;

                  if(MA_OK == (err = ma_season_info_get_url(info, &p))) {
                     ma_variant_t *v = NULL;

                     if(MA_OK == (err = ma_variant_create_from_string(p, strlen(p), &v))) {
                         if(MA_OK != (err = ma_table_add_entry(info_table, MA_SEASON_INFO_ATTR_URL, v))) {
                             MA_LOG(season_logger, MA_LOG_SEV_ERROR, "%s failed to add in season table, last error(%d)", MA_SEASON_INFO_ATTR_URL, err); 
                         }
                         (void)ma_variant_release(v);
                     }
                  }
             }
             {
                  int c = 0;

                  if(MA_OK == (err = ma_season_info_get_width(info, &c))) {
                      ma_variant_t *v = NULL;

                      if(MA_OK == (err = ma_variant_create_from_int32(c, &v))) {
                          if(MA_OK != (err = ma_table_add_entry(info_table, MA_SEASON_INFO_ATTR_WIDTH, v))) {
                              MA_LOG(season_logger, MA_LOG_SEV_ERROR, "%s failed to add in season table, last error(%d)", MA_SEASON_INFO_ATTR_WIDTH, err); 
                          }
                          (void)ma_variant_release(v);
                      }
                  }
             }
             {
                  int c = 0;

                  if(MA_OK == (err = ma_season_info_get_height(info, &c))) {
                      ma_variant_t *v = NULL;

                      if(MA_OK == (err = ma_variant_create_from_int32(c, &v))) {
                          if(MA_OK != (err = ma_table_add_entry(info_table, MA_SEASON_INFO_ATTR_HEIGHT, v))) {
                              MA_LOG(season_logger, MA_LOG_SEV_ERROR, "%s failed to add in season table, last error(%d)", MA_SEASON_INFO_ATTR_HEIGHT, err); 
                          }
                          (void)ma_variant_release(v);
                      }
                  }
             }
             {
                  const char *p = NULL;

                  if(MA_OK == (err = ma_season_info_get_embed(info, &p))) {
                     ma_variant_t *v = NULL;

                     if(MA_OK == (err = ma_variant_create_from_string(p, strlen(p), &v))) {
                         if(MA_OK != (err = ma_table_add_entry(info_table, MA_SEASON_INFO_ATTR_EMBED, v))) {
                             MA_LOG(season_logger, MA_LOG_SEV_ERROR, "%s failed to add in season table, last error(%d)", MA_SEASON_INFO_ATTR_EMBED, err); 
                         }
                         (void)ma_variant_release(v);
                     }
                  }
             }
             {
                  int c = 0;

                  if(MA_OK == (err = ma_season_info_get_height(info, &c))) {
                      ma_variant_t *v = NULL;

                      if(MA_OK == (err = ma_variant_create_from_bool(c, &v))) {
                          if(MA_OK != (err = ma_table_add_entry(info_table, MA_SEASON_INFO_ATTR_IS_VIDEO, v))) {
                              MA_LOG(season_logger, MA_LOG_SEV_ERROR, "%s failed to add in season table, last error(%d)", MA_SEASON_INFO_ATTR_IS_VIDEO, err); 
                          }
                          (void)ma_variant_release(v);
                      }
                  }
             }
             {
                  const char *p = NULL;

                  if(MA_OK == (err = ma_season_info_get_id(info, &p))) {
                     ma_variant_t *v = NULL;

                     if(MA_OK == (err = ma_variant_create_from_string(p, strlen(p), &v))) {
                         if(MA_OK != (err = ma_table_add_entry(info_table, MA_SEASON_INFO_ATTR_ID, v))) {
                             MA_LOG(season_logger, MA_LOG_SEV_ERROR, "%s failed to add in season table, last error(%d)", MA_SEASON_INFO_ATTR_ID, err); 
                         }
                         (void)ma_variant_release(v);
                     }
                  }
             }
             {
                  const char *p = NULL;

                  if(MA_OK == (err = ma_season_info_get_topic(info, &p))) {
                     ma_variant_t *v = NULL;

                     if(MA_OK == (err = ma_variant_create_from_string(p, strlen(p), &v))) {
                         if(MA_OK != (err = ma_table_add_entry(info_table, MA_SEASON_INFO_ATTR_TOPIC, v))) {
                             MA_LOG(season_logger, MA_LOG_SEV_ERROR, "%s failed to add in season table, last error(%d)", MA_SEASON_INFO_ATTR_TOPIC, err); 
                         }
                         (void)ma_variant_release(v);
                     }
                  }
             }
             {
                  const char *p = NULL;

                  if(MA_OK == (err = ma_season_info_get_board(info, &p))) {
                     ma_variant_t *v = NULL;

                     if(MA_OK == (err = ma_variant_create_from_string(p, strlen(p), &v))) {
                         if(MA_OK != (err = ma_table_add_entry(info_table, MA_SEASON_INFO_ATTR_BOARD, v))) {
                             MA_LOG(season_logger, MA_LOG_SEV_ERROR, "%s failed to add in season table, last error(%d)", MA_SEASON_INFO_ATTR_BOARD, err); 
                         }
                         (void)ma_variant_release(v);
                     }
                  }
             }
             {
                  const char *p = NULL;

                  if(MA_OK == (err = ma_season_info_get_saved_by(info, &p))) {
                     ma_variant_t *v = NULL;

                     if(MA_OK == (err = ma_variant_create_from_string(p, strlen(p), &v))) {
                         if(MA_OK != (err = ma_table_add_entry(info_table, MA_SEASON_INFO_ATTR_SAVED_BY, v))) {
                             MA_LOG(season_logger, MA_LOG_SEV_ERROR, "%s failed to add in season table, last error(%d)", MA_SEASON_INFO_ATTR_SAVED_BY, err); 
                         }
                         (void)ma_variant_release(v);
                     }
                  }
             }
             {
                  const char *p = NULL;

                  if(MA_OK == (err = ma_season_info_get_board_image_url(info, &p))) {
                     ma_variant_t *v = NULL;

                     if(MA_OK == (err = ma_variant_create_from_string(p, strlen(p), &v))) {
                         if(MA_OK != (err = ma_table_add_entry(info_table, MA_SEASON_INFO_ATTR_BOARD_IMAGE_URL, v))) {
                             MA_LOG(season_logger, MA_LOG_SEV_ERROR, "%s failed to add in season table, last error(%d)", MA_SEASON_INFO_ATTR_BOARD_IMAGE_URL, err); 
                         }
                         (void)ma_variant_release(v);
                     }
                  }
             }
             if(MA_OK != (err = ma_variant_create_from_table(info_table, variant))) {
                 MA_LOG(season_logger, MA_LOG_SEV_ERROR, "season variant table creation failed, last error(%d)", err);
             }
             (void)ma_table_release(info_table);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_convert_from_variant(ma_variant_t *variant, ma_season_info_t **info) {
    if(info && variant) {
        ma_error_t err = MA_OK;

        if(MA_OK == (err = ma_season_info_create(info))) {
            ma_table_t *info_table = NULL;

            MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "season info created successfully");
            if(MA_OK == (err = ma_variant_get_table(variant, &info_table))) {
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_SEASON_INFO_ATTR_EMAIL_ID, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "email (%s)", pstr);
                                    err = ma_season_info_set_email_id((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    } else {
                        MA_LOG(season_logger, MA_LOG_SEV_ERROR, "email does not exist, last error(%d)", err);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_SEASON_INFO_ATTR_CONTACT, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "contact (%s)", pstr);
                                    err = ma_season_info_set_contact((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    } else {
                        MA_LOG(season_logger, MA_LOG_SEV_ERROR, "contact does not exist, last error(%d)", err);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_SEASON_INFO_ATTR_DOMAIN, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "domain (%s)", pstr);
                                    err = ma_season_info_set_domain((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    } else {
                        MA_LOG(season_logger, MA_LOG_SEV_ERROR, "domain fetch failed, last error(%d)", err);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_SEASON_INFO_ATTR_ATTRIBUTION, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "attribution (%s)", pstr);
                                    err = ma_season_info_set_attribution((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    } else {
                        MA_LOG(season_logger, MA_LOG_SEV_ERROR, "attribution fetch failed, last error(%d)", err);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_SEASON_INFO_ATTR_DESCRIPTION, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "description (%s)", pstr);
                                    err = ma_season_info_set_description((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    } else {
                        MA_LOG(season_logger, MA_LOG_SEV_ERROR, "description fetch failed, last error(%d)", err);
                    }
                }

                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_SEASON_INFO_ATTR_ABOUT, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "about (%s)", pstr);
                                    err = ma_season_info_set_about((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    } else {
                        MA_LOG(season_logger, MA_LOG_SEV_ERROR, "description fetch failed, last error(%d)", err);
                    }
                }

                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_SEASON_INFO_ATTR_LOCATION, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "location (%s)", pstr);
                                    err = ma_season_info_set_location((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    } else {
                        MA_LOG(season_logger, MA_LOG_SEV_ERROR, "location fetch failed, last error(%d)", err);
                    }
                }

                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_SEASON_INFO_ATTR_FULL_NAME, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "full name (%s)", pstr);
                                    err = ma_season_info_set_full_name((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    } else {
                        MA_LOG(season_logger, MA_LOG_SEV_ERROR, "fullname fetch failed, last error(%d)", err);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_SEASON_INFO_ATTR_FOLLOWER_COUNT, &v))) {
                        int value = 0;

                        if(MA_OK == (err = ma_variant_get_int32(v, &value)))
                            err = ma_season_info_set_follower_count((*info), value);
                        MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "follower count(%d)", value);
                        (void)ma_variant_release(v);
                    } else {
                        MA_LOG(season_logger, MA_LOG_SEV_ERROR, "follower count failed, last error(%d)", err);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_SEASON_INFO_ATTR_IMAGE_SMALL_URL, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "small url (%s)", pstr);
                                    err = ma_season_info_set_image_small_url((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    } else {
                        MA_LOG(season_logger, MA_LOG_SEV_ERROR, "image small url fetch failed, last error(%d)", err);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_SEASON_INFO_ATTR_PIN_COUNT, &v))) {
                        int value = 0;

                        if(MA_OK == (err = ma_variant_get_int32(v, &value)))
                            err = ma_season_info_set_pin_count((*info), value);
                        MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "pin count(%d)", value);
                        (void)ma_variant_release(v);
                    } else {
                        MA_LOG(season_logger, MA_LOG_SEV_ERROR, "pin count fetch failed, last error(%d)", err);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_SEASON_INFO_ATTR_PINNER_ID, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "pinner id (%s)", pstr);
                                    err = ma_season_info_set_pin_id((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    } else {
                        MA_LOG(season_logger, MA_LOG_SEV_ERROR, "pinner id fetch failed, last error(%d)", err);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_SEASON_INFO_ATTR_PROFILE_URL, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "profile url (%s)", pstr);
                                    err = ma_season_info_set_profile_url((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    } else {
                        MA_LOG(season_logger, MA_LOG_SEV_ERROR, "profile url fetch failed, last error(%d)", err);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_SEASON_INFO_ATTR_REPIN_COUNT, &v))) {
                        int value = 0;

                        if(MA_OK == (err = ma_variant_get_int32(v, &value)))
                            err = ma_season_info_set_repin_count((*info), value);
                        MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "repin count(%d)", value);
                        (void)ma_variant_release(v);
                    } else {
                        MA_LOG(season_logger, MA_LOG_SEV_ERROR, "repin count fetch failed, last error(%d)", err);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_SEASON_INFO_ATTR_DOMINANT_COLOR, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "dominant color (%s)", pstr);
                                    err = ma_season_info_set_dominant_color((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    } else {
                        MA_LOG(season_logger, MA_LOG_SEV_ERROR, "dominant color fetch failed, last error(%d)", err);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_SEASON_INFO_ATTR_LIKE_COUNT, &v))) {
                        int value = 0;

                        if(MA_OK == (err = ma_variant_get_int32(v, &value)))
                            err = ma_season_info_set_like_count((*info), value);
                        MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "like count(%d)", value);
                        (void)ma_variant_release(v);
                    } else {
                        MA_LOG(season_logger, MA_LOG_SEV_ERROR, "like count failed, last error(%d)", err);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_SEASON_INFO_ATTR_LINK, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "link (%s)", pstr);
                                    err = ma_season_info_set_link((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    } else {
                        MA_LOG(season_logger, MA_LOG_SEV_ERROR, "link fetch failed, last error(%d)", err);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_SEASON_INFO_ATTR_URL, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "url (%s)", pstr);
                                    err = ma_season_info_set_url((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    } else {
                        MA_LOG(season_logger, MA_LOG_SEV_ERROR, "url fetch failed, last error(%d)", err);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_SEASON_INFO_ATTR_WIDTH, &v))) {
                        int value = 0;

                        if(MA_OK == (err = ma_variant_get_int32(v, &value)))
                            err = ma_season_info_set_width((*info), value);
                        MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "width (%d)", value);
                        (void)ma_variant_release(v);
                    } else {
                        MA_LOG(season_logger, MA_LOG_SEV_ERROR, "width fetch failed, last error(%d)", err);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_SEASON_INFO_ATTR_HEIGHT, &v))) {
                        int value = 0;

                        if(MA_OK == (err = ma_variant_get_int32(v, &value)))
                            err = ma_season_info_set_height((*info), value);
                        MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "height (%d)", value);
                        (void)ma_variant_release(v);
                    } else {
                        MA_LOG(season_logger, MA_LOG_SEV_ERROR, "height fetch failed, last error(%d)", err);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_SEASON_INFO_ATTR_EMBED, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "embed (%s)", pstr);
                                    err = ma_season_info_set_embed((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    } else {
                        MA_LOG(season_logger, MA_LOG_SEV_ERROR, "embed fetch failed, last error(%d)", err);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_SEASON_INFO_ATTR_IS_VIDEO, &v))) {
                        ma_bool_t value = 0;

                        if(MA_OK == (err = ma_variant_get_bool(v, &value)))
                            err = ma_season_info_set_is_video((*info), value);
                        MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "is_video count(%d)", value);
                        (void)ma_variant_release(v);
                    } else {
                        MA_LOG(season_logger, MA_LOG_SEV_ERROR, "is_video fetch failed, last error(%d)", err);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_SEASON_INFO_ATTR_ID, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "Id (%s)", pstr);
                                    err = ma_season_info_set_id((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    } else {
                        MA_LOG(season_logger, MA_LOG_SEV_ERROR, "id fetch failed, last error(%d)", err);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_SEASON_INFO_ATTR_TOPIC, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "topic (%s)", pstr);
                                    err = ma_season_info_set_topic((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    } else {
                        MA_LOG(season_logger, MA_LOG_SEV_ERROR, "topic fetch failed, last error(%d)", err);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_SEASON_INFO_ATTR_BOARD, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "board (%s)", pstr);
                                    err = ma_season_info_set_board((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    } else {
                        MA_LOG(season_logger, MA_LOG_SEV_ERROR, "board fetch failed, last error(%d)", err);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_SEASON_INFO_ATTR_SAVED_BY, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "saved by (%s)", pstr);
                                    err = ma_season_info_set_saved_by((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    } else {
                        MA_LOG(season_logger, MA_LOG_SEV_ERROR, "saved by fetch failed, last error(%d)", err);
                    }
                }
                {
                    ma_variant_t *v = NULL;

                    if(MA_OK == (err = ma_table_get_value(info_table, MA_SEASON_INFO_ATTR_BOARD_IMAGE_URL, &v))) {
                        ma_buffer_t *buf = NULL;

                        if(MA_OK == (err = ma_variant_get_string_buffer(v, &buf))) {
                            const char *pstr = NULL;
                            size_t len = 0;

                            if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                                if(pstr) {
                                    MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "board image url (%s)", pstr);
                                    err = ma_season_info_set_board_image_url((*info), pstr);
                                }
                            }
                            (void)ma_buffer_release(buf);
                        }
                        (void)ma_variant_release(v);
                    } else {
                        MA_LOG(season_logger, MA_LOG_SEV_ERROR, "saved by fetch failed, last error(%d)", err);
                    }
                }

                if(MA_OK != (err = ma_table_release(info_table))) {
                    MA_LOG(season_logger, MA_LOG_SEV_ERROR, "table release failed, last error(%d)", err);
                }
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static void replace_all(char *pstr, char pat, char c) {
    while(pstr && *pstr) {
        if(*pstr == pat)*pstr = c;
        pstr++;
    }
}

void for_each(const char *key, ma_variant_t *value, void *cb_data, ma_bool_t *stop_loop) {
    if(key && value && cb_data && stop_loop) {
        ma_season_info_t *info = (ma_season_info_t*)cb_data;
        ma_vartype_t type = MA_VARTYPE_NULL;
        char buf[MA_MAX_LEN+1] = {0};

        (void)ma_variant_get_type(value, &type);
        if(MA_VARTYPE_INT32 == type) {
            ma_uint32_t t = 0;

            (void)ma_variant_get_uint32(value, &t);
            MA_MSC_SELECT(_snprintf, snprintf)(buf, MA_MAX_LEN, "%u", t);
            (void)ma_json_add_element(info->json, key, buf);
        } else if(MA_VARTYPE_INT32 == type) {
            ma_int32_t t = 0;

            (void)ma_variant_get_int32(value, &t);
            MA_MSC_SELECT(_snprintf, snprintf)(buf, MA_MAX_LEN, "%d", t);
            (void)ma_json_add_element(info->json, key, buf);
        } else if(MA_VARTYPE_BOOL == type) {
            ma_bool_t t = MA_FALSE;

            (void)ma_variant_get_bool(value, &t);
            MA_MSC_SELECT(_snprintf, snprintf)(buf, MA_MAX_LEN, "%d", t);
            (void)ma_json_add_element(info->json, key, buf);
        } else if(MA_VARTYPE_STRING == type) {
            ma_buffer_t *buf = NULL;

            if(MA_OK == ma_variant_get_string_buffer(value, &buf)) {
                const char *pstr = NULL;
                size_t len = 0;

                if(MA_OK == ma_buffer_get_string(buf, &pstr, &len)) {
                    if(pstr) {
                        replace_all(pstr, '\n', ' ');
                        replace_all(pstr, '\r', ' ');
                        replace_all(pstr, '\"', ' ');
                        (void)ma_json_add_element(info->json, key, pstr);
                    }
                }
                (void)ma_buffer_release(buf);
            }
        }
    }
}

ma_error_t ma_season_info_convert_to_json(ma_season_info_t *info, ma_json_t **json) {
    if(info && json) {
        ma_error_t err = MA_OK;
        ma_variant_t *v = NULL;

        if(MA_OK == (err = ma_season_info_convert_to_variant(info, &v))) {
            ma_table_t *table = NULL;

            if(MA_OK == (err = ma_variant_get_table(v, &table))) {
                if(MA_OK == (err = ma_table_foreach(table, &for_each, info))) {
                    *json = info->json;
                }
                (void)ma_table_release(table);
            }
            (void)ma_variant_release(v);
        }
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_info_convert_from_json(const char *json_str, ma_season_info_t **pinfo) {
    if(json_str && pinfo) {
        ma_error_t err = MA_ERROR_PRECONDITION;
        ma_cjson *json = ma_cjson_Parse(json_str);
        
        if(json) {
            if(MA_OK == (err = ma_season_info_create(pinfo))) {
                ma_cjson *j  = NULL;
                ma_season_info_t *info  = *pinfo;

                if(ma_cjson_HasObjectItem(json, MA_SEASON_INFO_ATTR_DOMAIN)) {
                    j = ma_cjson_GetObjectItem(json, MA_SEASON_INFO_ATTR_DOMAIN);
                    if(j->valuestring) {
                        MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "domain (%s)", j->valuestring);
                        (void)ma_season_info_set_domain(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_SEASON_INFO_ATTR_ATTRIBUTION)) {
                    j = ma_cjson_GetObjectItem(json, MA_SEASON_INFO_ATTR_ATTRIBUTION);
                    if(j->valuestring) {
                        MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "attribution (%s)", j->valuestring);
                        (void)ma_season_info_set_attribution(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_SEASON_INFO_ATTR_LOCATION)) {
                    j = ma_cjson_GetObjectItem(json, MA_SEASON_INFO_ATTR_LOCATION);
                    if(j->valuestring) {
                        MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "location (%s)", j->valuestring);
                        (void)ma_season_info_set_location(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_SEASON_INFO_ATTR_DESCRIPTION)) {
                    j = ma_cjson_GetObjectItem(json, MA_SEASON_INFO_ATTR_DESCRIPTION);
                    if(j->valuestring) {
                        MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "description (%s)", j->valuestring);
                        (void)ma_season_info_set_description(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_SEASON_INFO_ATTR_ABOUT)) {
                    j = ma_cjson_GetObjectItem(json, MA_SEASON_INFO_ATTR_ABOUT);
                    if(j->valuestring) {
                        MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "about (%s)", j->valuestring);
                        (void)ma_season_info_set_about(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_SEASON_INFO_ATTR_FULL_NAME)) {
                    j = ma_cjson_GetObjectItem(json, MA_SEASON_INFO_ATTR_FULL_NAME);
                    if(j->valuestring) {
                        MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "fullname (%s)", j->valuestring);
                        (void)ma_season_info_set_full_name(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_SEASON_INFO_ATTR_FOLLOWER_COUNT)) {
                    j = ma_cjson_GetObjectItem(json, MA_SEASON_INFO_ATTR_FOLLOWER_COUNT);
                    MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "follower count (%d)", j->valueint);
                    (void)ma_season_info_set_follower_count(info, j->valueint);
                }
                if(ma_cjson_HasObjectItem(json, MA_SEASON_INFO_ATTR_IMAGE_SMALL_URL)) {
                    j = ma_cjson_GetObjectItem(json, MA_SEASON_INFO_ATTR_IMAGE_SMALL_URL);
                    if(j->valuestring) {
                        MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "image small url (%s)", j->valuestring);
                        (void)ma_season_info_set_image_small_url(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_SEASON_INFO_ATTR_PIN_COUNT)) {
                    j = ma_cjson_GetObjectItem(json, MA_SEASON_INFO_ATTR_PIN_COUNT);
                    MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "pin count (%d)", j->valueint);
                    (void)ma_season_info_set_pin_count(info, j->valueint);
                }
                if(ma_cjson_HasObjectItem(json, MA_SEASON_INFO_ATTR_PINNER_ID)) {
                    j = ma_cjson_GetObjectItem(json, MA_SEASON_INFO_ATTR_PINNER_ID);
                    if(j->valuestring) {
                        MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "pinner id (%s)", j->valuestring);
                        (void)ma_season_info_set_pin_id(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_SEASON_INFO_ATTR_PROFILE_URL)) {
                    j = ma_cjson_GetObjectItem(json, MA_SEASON_INFO_ATTR_PROFILE_URL);
                    if(j->valuestring) {
                        MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "profile url (%s)", j->valuestring);
                        (void)ma_season_info_set_profile_url(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_SEASON_INFO_ATTR_REPIN_COUNT)) {
                    j = ma_cjson_GetObjectItem(json, MA_SEASON_INFO_ATTR_REPIN_COUNT);
                    MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "repin count (%d)", j->valueint);
                    (void)ma_season_info_set_repin_count(info, j->valueint);
                }
                if(ma_cjson_HasObjectItem(json, MA_SEASON_INFO_ATTR_DOMINANT_COLOR)) {
                    j = ma_cjson_GetObjectItem(json, MA_SEASON_INFO_ATTR_DOMINANT_COLOR);
                    if(j->valuestring) {
                        MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "dominant color (%s)", j->valuestring);
                        (void)ma_season_info_set_dominant_color(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_SEASON_INFO_ATTR_LIKE_COUNT)) {
                    j = ma_cjson_GetObjectItem(json, MA_SEASON_INFO_ATTR_LIKE_COUNT);
                    MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "like count (%d)", j->valueint);
                    (void)ma_season_info_set_like_count(info, j->valueint);
                }
                if(ma_cjson_HasObjectItem(json, MA_SEASON_INFO_ATTR_LINK)) {
                    j = ma_cjson_GetObjectItem(json, MA_SEASON_INFO_ATTR_LINK);
                    if(j->valuestring) {
                        MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "link (%s)", j->valuestring);
                        (void)ma_season_info_set_link(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_SEASON_INFO_ATTR_URL)) {
                    j = ma_cjson_GetObjectItem(json, MA_SEASON_INFO_ATTR_URL);
                    if(j->valuestring) {
                        MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "url (%s)", j->valuestring);
                        (void)ma_season_info_set_url(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_SEASON_INFO_ATTR_WIDTH)) {
                    j = ma_cjson_GetObjectItem(json, MA_SEASON_INFO_ATTR_WIDTH);
                    MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "width (%d)", j->valueint);
                    (void)ma_season_info_set_width(info, j->valueint);
                }
                if(ma_cjson_HasObjectItem(json, MA_SEASON_INFO_ATTR_HEIGHT)) {
                    j = ma_cjson_GetObjectItem(json, MA_SEASON_INFO_ATTR_HEIGHT);
                    MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "height (%d)", j->valueint);
                    (void)ma_season_info_set_height(info, j->valueint);
                }
                if(ma_cjson_HasObjectItem(json, MA_SEASON_INFO_ATTR_EMBED)) {
                    j = ma_cjson_GetObjectItem(json, MA_SEASON_INFO_ATTR_EMBED);
                    if(j->valuestring) {
                        MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "embed (%s)", j->valuestring);
                        (void)ma_season_info_set_embed(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_SEASON_INFO_ATTR_IS_VIDEO)) {
                    j = ma_cjson_GetObjectItem(json, MA_SEASON_INFO_ATTR_IS_VIDEO);
                    MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "is_video (%d)", j->valueint);
                    (void)ma_season_info_set_is_video(info, j->valueint ? MA_TRUE : MA_FALSE);
                }
                if(ma_cjson_HasObjectItem(json, MA_SEASON_INFO_ATTR_ID)) {
                    j = ma_cjson_GetObjectItem(json, MA_SEASON_INFO_ATTR_ID);
                    if(j->valuestring) {
                        MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "id (%s)", j->valuestring);
                        (void)ma_season_info_set_id(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_SEASON_INFO_ATTR_TOPIC)) {
                    j = ma_cjson_GetObjectItem(json, MA_SEASON_INFO_ATTR_TOPIC);
                    if(j->valuestring) {
                        MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "topic (%s)", j->valuestring);
                        (void)ma_season_info_set_topic(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_SEASON_INFO_ATTR_BOARD)) {
                    j = ma_cjson_GetObjectItem(json, MA_SEASON_INFO_ATTR_BOARD);
                    if(j->valuestring) {
                        MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "board (%s)", j->valuestring);
                        (void)ma_season_info_set_board(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_SEASON_INFO_ATTR_SAVED_BY)) {
                    j = ma_cjson_GetObjectItem(json, MA_SEASON_INFO_ATTR_SAVED_BY);
                    if(j->valuestring) {
                        MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "saved by (%s)", j->valuestring);
                        (void)ma_season_info_set_saved_by(info, j->valuestring);
                    }
                }
                if(ma_cjson_HasObjectItem(json, MA_SEASON_INFO_ATTR_BOARD_IMAGE_URL)) {
                    j = ma_cjson_GetObjectItem(json, MA_SEASON_INFO_ATTR_BOARD_IMAGE_URL);
                    if(j->valuestring) {
                        MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "board image url(%s)", j->valuestring);
                        (void)ma_season_info_set_board_image_url(info, j->valuestring);
                    }
                }
            }
            ma_cjson_Delete(json);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}
