#include "ma/internal/utils/season/ma_season_datastore.h"
#include "ma/internal/defs/ma_season_defs.h"
#include "ma/scheduler/ma_enumerator.h"
#include "ma/internal/ma_macros.h"
#include "ma/ma_log.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "season"

extern ma_logger_t *season_logger;

#define SEASON_DATA MA_DATASTORE_PATH_SEPARATOR "season_data"
#define SEASON_ID MA_DATASTORE_PATH_SEPARATOR "season_id"

#define SEASON_DATA_PATH SEASON_DATA SEASON_ID MA_DATASTORE_PATH_SEPARATOR/* \"season_data"\"season_id"\ */


ma_error_t ma_season_datastore_read(ma_season_t *season, ma_ds_t *datastore, const char *ds_path) {
    if(season && datastore && ds_path) {
        ma_error_t err = MA_OK;
        ma_ds_iterator_t *season_iterator = NULL;
        ma_buffer_t *season_id_buf = NULL;
        char season_data_base_path[MA_MAX_PATH_LEN] = {0};

        MA_MSC_SELECT(_snprintf, snprintf)(season_data_base_path, MA_MAX_PATH_LEN, "%s%s", ds_path, SEASON_DATA_PATH);
        MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "season data store path(%s) reading...", season_data_base_path);
        if(MA_OK == (err = ma_ds_iterator_create(datastore, 0, season_data_base_path, &season_iterator))) {
            while(MA_ERROR_NO_MORE_ITEMS != ma_ds_iterator_get_next(season_iterator, &season_id_buf) && season_id_buf) {
                const char *season_id = NULL;
                size_t size = 0;
                
                if(MA_OK == (err = ma_buffer_get_string(season_id_buf, &season_id, &size))) {
                    char season_data_path[MA_MAX_PATH_LEN] = {0};
                    ma_variant_t *season_var = NULL;
                    MA_MSC_SELECT(_snprintf, snprintf)(season_data_path, MA_MAX_PATH_LEN, "%s", season_data_base_path);/* base_path\"season_data"\"season_id"\4097 */
                    if(MA_OK == (err = ma_ds_get_variant(datastore, season_data_path, season_id, MA_VARTYPE_TABLE, &season_var))) {

                        (void)ma_variant_release(season_var);
                    }
                }
                (void)ma_buffer_release(season_id_buf);
            }
            (void)ma_ds_iterator_release(season_iterator);
        }

        return ((MA_ERROR_DS_PATH_NOT_FOUND == err) || (MA_ERROR_SCHEDULER_TASK_ID_CONFLICT == err)) ? MA_OK : err;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_season_datastore_write(ma_season_t *season, ma_season_info_t *info, ma_ds_t *datastore, const char *ds_path) {
    if(season && info && datastore && ds_path) {
        char season_data_path[MA_MAX_PATH_LEN] = {0};
        const char *season_id = NULL;
        ma_error_t err = MA_OK;
        ma_variant_t *var = NULL;

        (void)ma_season_info_get_id(info, &season_id);
        MA_MSC_SELECT(_snprintf, snprintf)(season_data_path, MA_MAX_PATH_LEN, "%s%s", ds_path, SEASON_DATA_PATH);
        MA_LOG(season_logger, MA_LOG_SEV_INFO, "season id(%s), data path(%s) season_data_path(%s)", season_id, ds_path, season_data_path);
        if(MA_OK == (err = ma_season_info_convert_to_variant(info, &var))) {
            if(MA_OK != (err = ma_ds_set_variant(datastore, season_data_path, season_id, var)))
                MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "season datastore write season id(%s) failed", season_id);
            (void)ma_variant_release(var);
        } else
            MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "season datastore write season id conversion season info to variant failed, last error(%d)", err);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_datastore_write_variant(ma_season_t *season, const char *season_id, ma_ds_t *datastore, const char *ds_path, ma_variant_t *var) {
    if(season && season_id && var && datastore && ds_path) {
        char season_data_path[MA_MAX_PATH_LEN] = {0};
        ma_error_t err = MA_OK;

        MA_MSC_SELECT(_snprintf, snprintf)(season_data_path, MA_MAX_PATH_LEN, "%s%s", ds_path, SEASON_DATA_PATH);
        MA_LOG(season_logger, MA_LOG_SEV_INFO, "season id(%s), data path(%s) season_data_path(%s)", season_id, ds_path, season_data_path);
        if(MA_OK == (err = ma_ds_set_variant(datastore, season_data_path, season_id, var)))
            MA_LOG(season_logger, MA_LOG_SEV_INFO, "season datastore write season id(%s) success", season_id);
        else
            MA_LOG(season_logger, MA_LOG_SEV_ERROR, "season datastore write season id(%s) failed, last error(%d)", season_id, err);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_season_datastore_remove_season(const char *season, ma_ds_t *datastore, const char *ds_path) {
    if(season && datastore && ds_path) {
        char season_path[MA_MAX_PATH_LEN] = {0};
        
        MA_LOG(season_logger, MA_LOG_SEV_DEBUG, "season removing (%s) from datastore", season);
        MA_MSC_SELECT(_snprintf, snprintf)(season_path, MA_MAX_PATH_LEN, "%s%s",ds_path, SEASON_DATA_PATH );/* base_path\"season_data"\4097 */
        return ma_ds_rem(datastore, season_path, season, 1);
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_datastore_get(ma_season_t *season, ma_ds_t *datastore, const char *ds_path, const char *season_id, ma_season_info_t **info) {
    if(season && datastore && ds_path && season_id && info) {
        ma_error_t err = MA_OK;
        char season_data_base_path[MA_MAX_PATH_LEN] = {0};
        ma_variant_t *season_var = NULL;

        MA_MSC_SELECT(_snprintf, snprintf)(season_data_base_path, MA_MAX_PATH_LEN, "%s%s", ds_path, SEASON_DATA_PATH);
        if(MA_OK == (err = ma_ds_get_variant(datastore, season_data_base_path, season_id, MA_VARTYPE_TABLE, &season_var))) {
            if(MA_OK == (err = ma_season_info_convert_from_variant(season_var, info)))
                MA_LOG(season_logger, MA_LOG_SEV_TRACE, "season(%s) exist in season DB", season_id);
            (void)ma_variant_release(season_var);
        } else 
            MA_LOG(season_logger, MA_LOG_SEV_ERROR, "season(%s) does not exist in season DB, last error(%d)", season_id, err);

        return err;
    }
    return MA_ERROR_INVALIDARG;

}

ma_error_t ma_season_datastore_clear(ma_ds_t *datastore, const char *ds_path) {
    return(datastore && ds_path) ? ma_ds_rem(datastore, ds_path, NULL, 1) : MA_ERROR_INVALIDARG;
}

