#include "ma/internal/utils/conf/ma_conf_log.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/defs/ma_general_defs.h"
#include "ma/internal/defs/ma_logger_defs.h"
#include "ma/internal/utils/platform/ma_sys_apis.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static void get_complete_log_file_name(const char *file_name, ma_temp_buffer_t *full_log_file_name) {
    char computer_name[64] = {0};
    char log_file_name[128]= {0};

    strncpy(log_file_name, file_name, 127);
    if(MA_TRUE ==  ma_get_computer_name(computer_name, 64)) {
        strcat(log_file_name, "_");
        strcat(log_file_name, computer_name);
    }            
    ma_temp_buffer_reserve(full_log_file_name, 128);
    MA_MSC_SELECT(_snprintf, snprintf)((char *)ma_temp_buffer_get(full_log_file_name), ma_temp_buffer_capacity(full_log_file_name),"%s", log_file_name);
}
ma_error_t ma_conf_get_agent_log_file_name(ma_temp_buffer_t *agent_log_file_name) {
    if(agent_log_file_name) {
        get_complete_log_file_name(MA_LOG_AGENT_FILE_NAME_STR, agent_log_file_name);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_conf_get_common_services_log_file_name(ma_temp_buffer_t *cmn_svc_log_file_name) {
    if(cmn_svc_log_file_name) {
        get_complete_log_file_name(MA_LOG_COMMON_SVCS_FILE_NAME_STR, cmn_svc_log_file_name);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_conf_get_compat_service_log_file_name(ma_temp_buffer_t *compat_svc_log_file_name) {
    if(compat_svc_log_file_name) {
        get_complete_log_file_name(MA_LOG_COMPAT_FILE_NAME_STR, compat_svc_log_file_name);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}
