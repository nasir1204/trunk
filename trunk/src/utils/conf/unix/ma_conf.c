#include "ma/internal/utils/conf/ma_conf.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/defs/ma_general_defs.h"
#include "ma/internal/defs/ma_logger_defs.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <dirent.h>


static ma_error_t read_install_path_from_xml(char static_install_path[MA_MAX_PATH_LEN]);
ma_error_t ma_conf_get_install_path(ma_temp_buffer_t *install_path) {
    if(install_path) {
        static char static_install_path[MA_MAX_PATH_LEN] = {0}; 
        ma_error_t rc = MA_OK;
        
        if('\0' == static_install_path[0]) 
            rc = read_install_path_from_xml(static_install_path);

        if(MA_OK == rc) {
            ma_temp_buffer_reserve(install_path, strlen(static_install_path));
            MA_MSC_SELECT(_snprintf, snprintf)((char*)ma_temp_buffer_get(install_path), ma_temp_buffer_capacity(install_path), "%s", static_install_path);
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t read_data_path_from_xml(char static_data_path[MA_MAX_PATH_LEN]);
ma_error_t ma_conf_get_data_path(ma_temp_buffer_t *data_path) {
    if(data_path) {
        static char static_data_path[MA_MAX_PATH_LEN] = {0}; 
        ma_error_t rc = MA_OK;
        
        if('\0' == static_data_path[0]) 
            rc = read_data_path_from_xml(static_data_path);

        if(MA_OK == rc) {
            ma_temp_buffer_reserve(data_path, strlen(static_data_path));
            MA_MSC_SELECT(_snprintf, snprintf)((char*)ma_temp_buffer_get(data_path), ma_temp_buffer_capacity(data_path), "%s", static_data_path);
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_conf_get_logs_path(ma_temp_buffer_t *logs_path) {
    if(logs_path) {
        ma_temp_buffer_t data_path = MA_TEMP_BUFFER_INIT;
        ma_error_t rc = MA_OK;
        if(MA_OK == (rc = ma_conf_get_data_path(&data_path))) {
            ma_temp_buffer_init(logs_path);
            /* strlen of data path + separator + strlen(log directory name) +  NULL terminator*/
            ma_temp_buffer_reserve(logs_path, strlen((char *)ma_temp_buffer_get(&data_path)) + 1 + strlen(MA_CONFIGURATION_LOGS_DIR_NAME_STR) + 1);
            MA_MSC_SELECT(_snprintf, snprintf)((char *)ma_temp_buffer_get(logs_path), ma_temp_buffer_capacity(logs_path),"%s%c%s", (char *)ma_temp_buffer_get(&data_path), MA_PATH_SEPARATOR, MA_CONFIGURATION_LOGS_DIR_NAME_STR);
        }
        ma_temp_buffer_uninit(&data_path);
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_conf_get_keystore_path(ma_temp_buffer_t *keystore_path) {
    if(keystore_path) {
        ma_temp_buffer_t data_path = MA_TEMP_BUFFER_INIT;
        ma_error_t rc = MA_OK;
        if(MA_OK == (rc = ma_conf_get_data_path(&data_path))) {
            ma_temp_buffer_init(keystore_path);
            /* strlen of data path + separator + strlen(keystore directory name) +  NULL terminator*/
            ma_temp_buffer_reserve(keystore_path, strlen((char *)ma_temp_buffer_get(&data_path)) + 1 + strlen(MA_CONFIGURATION_KEYSTORE_DIR_NAME_STR) + 1);
            MA_MSC_SELECT(_snprintf, snprintf)((char *)ma_temp_buffer_get(keystore_path), ma_temp_buffer_capacity(keystore_path),"%s%c%s", (char *)ma_temp_buffer_get(&data_path), MA_PATH_SEPARATOR, MA_CONFIGURATION_KEYSTORE_DIR_NAME_STR);
        }
        ma_temp_buffer_uninit(&data_path);
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_conf_get_certstore_path(ma_temp_buffer_t *certstore_path) {
    if(certstore_path) {
        ma_temp_buffer_t data_path = MA_TEMP_BUFFER_INIT;
        ma_error_t rc = MA_OK;
        if(MA_OK == (rc = ma_conf_get_data_path(&data_path))) {
            ma_temp_buffer_init(certstore_path);
            /* strlen of data path + separator + strlen(certstore_path) +  NULL terminator*/
            ma_temp_buffer_reserve(certstore_path, strlen((char *)ma_temp_buffer_get(&data_path)) + 1 + strlen(MA_CONFIGURATION_CERTSTORE_DIR_NAME_STR) + 1);
            MA_MSC_SELECT(_snprintf, snprintf)((char *)ma_temp_buffer_get(certstore_path), ma_temp_buffer_capacity(certstore_path),"%s%c%s", (char *)ma_temp_buffer_get(&data_path), MA_PATH_SEPARATOR, MA_CONFIGURATION_CERTSTORE_DIR_NAME_STR);
        }
        ma_temp_buffer_uninit(&data_path);
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_conf_get_db_path(ma_temp_buffer_t *db_path) {
    if(db_path) {
        ma_temp_buffer_t data_path = MA_TEMP_BUFFER_INIT;
        ma_error_t rc = MA_OK;
        if(MA_OK == (rc = ma_conf_get_data_path(&data_path))) {
            ma_temp_buffer_init(db_path);
            /* strlen of data path + separator + strlen(db_path) +  NULL terminator*/
            ma_temp_buffer_reserve(db_path, strlen((char *)ma_temp_buffer_get(&data_path)) + 1 + strlen(MA_CONFIGURATION_DB_DIR_NAME_STR) + 1);
            MA_MSC_SELECT(_snprintf, snprintf)((char *)ma_temp_buffer_get(db_path), ma_temp_buffer_capacity(db_path),"%s%c%s", (char *)ma_temp_buffer_get(&data_path), MA_PATH_SEPARATOR, MA_CONFIGURATION_DB_DIR_NAME_STR);
        }
        ma_temp_buffer_uninit(&data_path);
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_conf_get_lib_path(ma_temp_buffer_t *lib_path) {
	if(lib_path) {
        ma_temp_buffer_t install_path = MA_TEMP_BUFFER_INIT;
        ma_error_t rc = MA_OK;
        if(MA_OK == (rc = ma_conf_get_install_path(&install_path))) {
            ma_temp_buffer_init(lib_path);
            /* strlen of install_path + separator + strlen(lib dir name) +  NULL terminator*/
            ma_temp_buffer_reserve(lib_path, strlen((char *)ma_temp_buffer_get(&install_path)) + 1 + strlen(MA_CONFIGURATION_LIB_DIR_NAME_STR) + 1);
            MA_MSC_SELECT(_snprintf, snprintf)((char *)ma_temp_buffer_get(lib_path), ma_temp_buffer_capacity(lib_path),"%s%c%s", (char *)ma_temp_buffer_get(&install_path), MA_PATH_SEPARATOR, MA_CONFIGURATION_LIB_DIR_NAME_STR);
        }
        ma_temp_buffer_uninit(&install_path);
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_conf_get_crypto_lib_path(ma_temp_buffer_t *crypto_lib_path) {
    return ma_conf_get_tools_lib_path(crypto_lib_path);
}

ma_error_t ma_conf_get_tools_lib_path(ma_temp_buffer_t *tools_lib_path) {
    if(tools_lib_path) {
        ma_temp_buffer_t lib_path = MA_TEMP_BUFFER_INIT;
        ma_error_t rc = MA_OK;
        if(MA_OK == (rc = ma_conf_get_lib_path(&lib_path))) {
            ma_temp_buffer_init(tools_lib_path);
            /* strlen of lib_path + separator + strlen(tools dir)  + strlen( NULL terminator*/
            ma_temp_buffer_reserve(tools_lib_path, strlen((char *)ma_temp_buffer_get(&lib_path)) + 1 + strlen(MA_CONFIGURATION_TOOLS_DIR_NAME_STR) + 1);
            MA_MSC_SELECT(_snprintf, snprintf)((char *)ma_temp_buffer_get(tools_lib_path), ma_temp_buffer_capacity(tools_lib_path),"%s%c%s", (char *)ma_temp_buffer_get(&lib_path), MA_PATH_SEPARATOR, MA_CONFIGURATION_TOOLS_DIR_NAME_STR);
        }
        ma_temp_buffer_uninit(&lib_path);
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}


/*TBD  - may be we need to fix this one ?? */
ma_error_t ma_conf_get_rsdk_lib_path(ma_temp_buffer_t *rsdk_lib_path) {
    return ma_conf_get_install_path(rsdk_lib_path);
}


static ma_error_t ma_trim_begin(char *xml_value) {
    if(xml_value && *xml_value) {
        char *pstr = xml_value;
        while(pstr && *pstr) {
            if(*pstr == ' ')
                strncpy(pstr, pstr+1, (strlen(pstr)-1));
            else break;
        }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_trim_end(char *xml_value) {
    if(xml_value && *xml_value) {
        size_t len = strlen(xml_value);
        len--;
        while(len > 0) {
            if((xml_value[len] == '\n') || (xml_value[len] == ' ')) {
                xml_value[len] = '\0';
                --len;
            }
            else break;
        }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}
 
static ma_error_t ma_parse_xml_tag(const char *xml_buf, const char *tag, char path[], size_t dest_size) {
    if(xml_buf && tag && path) {
        ma_error_t err = MA_OK;
        char start_tag[MA_MAX_LEN] = {0};
        char end_tag[MA_MAX_LEN] = {0};
        char *pstart  = NULL;
        char *pend    = NULL;
        size_t offset_len = 0;

        sprintf(start_tag, "<%s>", tag);
        sprintf(end_tag, "</%s>", tag);
        
        pstart = (char*)strstr(xml_buf, start_tag);
        if(!pstart)return MA_ERROR_INVALIDARG;
        pend = (char*)strstr(xml_buf, end_tag);
        if(!pend)return MA_ERROR_INVALIDARG;

        if(pstart > pend)return MA_ERROR_UNEXPECTED; //no idea how to hit this..

        offset_len = pend - (pstart + strlen(start_tag));
	if(offset_len > dest_size - 1) return MA_ERROR_INVALIDARG;

        memcpy(path, pstart+strlen(start_tag), offset_len);
        err = ma_trim_begin(path);
        if(MA_OK != err)return err;
        return ma_trim_end(path);
    }
    return MA_ERROR_INVALIDARG;
}


static ma_error_t read_install_path_from_xml(char static_install_path[MA_MAX_PATH_LEN]) {    
    ma_error_t rc = MA_ERROR_DS_PATH_NOT_FOUND; 
    char path[MA_MAX_PATH_LEN] = {0};
    struct stat   st;

    MA_MSC_SELECT(_snprintf, snprintf)(path, MA_MAX_PATH_LEN, "%s%c%s%c%s", MA_CONFIGURATION_REGISTRY_APPLICATIONS_PATH_STR, 
		MA_PATH_SEPARATOR, MA_SOFTWAREID_GENERAL_STR, MA_PATH_SEPARATOR, MA_CONFIGURATION_REGISTRY_CONFIG_FILE_NAME_STR);
    if(0 == stat(path, &st)) {
        if(st.st_size > 0) {
            char *xml_buf = NULL;
            ma_temp_buffer_t temp_xml_buf = MA_TEMP_BUFFER_INIT;

            ma_temp_buffer_init(&temp_xml_buf);
            ma_temp_buffer_reserve(&temp_xml_buf, st.st_size+1);
            xml_buf = (char*)ma_temp_buffer_get(&temp_xml_buf);
            if(xml_buf) {
                FILE *fp = NULL;
                fp = fopen(path, "r");
                if(fp) {                        
                    if(st.st_size == fread(xml_buf, 1, st.st_size, fp)) {
						xml_buf[st.st_size] = '\0';
                        if(MA_OK == ma_parse_xml_tag(xml_buf, MA_CONFIGURATION_AGENT_KEY_INSTALL_PATH_STR, static_install_path, MA_MAX_PATH_LEN)) {
                            rc = MA_OK;
                        }
                    }
                    fclose(fp);
                }
            }
            ma_temp_buffer_uninit(&temp_xml_buf);
        }
    }
    return rc;
}

static ma_error_t read_data_path_from_xml(char static_data_path[MA_MAX_PATH_LEN]) {
    ma_error_t rc = MA_ERROR_DS_PATH_NOT_FOUND;
    char path[MA_MAX_PATH_LEN] = {0};
    struct stat   st;

    MA_MSC_SELECT(_snprintf, snprintf)(path, MA_MAX_PATH_LEN, "%s%c%s%c%s", MA_CONFIGURATION_REGISTRY_APPLICATIONS_PATH_STR, 
		MA_PATH_SEPARATOR, MA_SOFTWAREID_GENERAL_STR, MA_PATH_SEPARATOR, MA_CONFIGURATION_REGISTRY_CONFIG_FILE_NAME_STR);
    if(0 == stat(path, &st)) {
        if(st.st_size > 0) {
            char *xml_buf = NULL;
            ma_temp_buffer_t temp_xml_buf = MA_TEMP_BUFFER_INIT;

            ma_temp_buffer_init(&temp_xml_buf);
            ma_temp_buffer_reserve(&temp_xml_buf, st.st_size+1);
            xml_buf = (char*)ma_temp_buffer_get(&temp_xml_buf);
            if(xml_buf) {
                FILE *fp = NULL;
                fp = fopen(path, "r");
                if(fp) {                        
                    if(st.st_size == fread(xml_buf, 1, st.st_size, fp)) {
						xml_buf[st.st_size] = '\0';
                        if(MA_OK == ma_parse_xml_tag(xml_buf, MA_CONFIGURATION_AGENT_KEY_DATA_PATH_STR, static_data_path, MA_MAX_PATH_LEN)) {
                            rc = MA_OK;
                        }
                    }
                    fclose(fp);
                }
            }
            ma_temp_buffer_uninit(&temp_xml_buf);
        }
    }
    return rc;
}
