#include "ma/internal/utils/conf/ma_conf_application.h"
#include "ma/internal/utils/datastructures/ma_vector.h"
#include "ma/internal/utils/platform/ma_sys_apis.h"

#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/defs/ma_general_defs.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct ma_conf_application_list_s {
    ma_vector_define(application_list, char *);
    size_t	size;
};

ma_error_t ma_conf_applications_create(ma_conf_application_list_t **list) {
    if(list) {
        ma_conf_application_list_t *self = (ma_conf_application_list_t *)calloc(1, sizeof(ma_conf_application_list_t));
        if(self) {
            ma_vector_init(self, MA_MAX_APPLICATIONS_INT, application_list, char *);
            *list = self;
            return MA_OK;
        }
        return MA_ERROR_OUTOFMEMORY;

    }
    return MA_ERROR_INVALIDARG;
}
static ma_error_t ma_trim_begin(char *xml_value) {
    if(xml_value && *xml_value) {
        char *pstr = xml_value;
        while(pstr && *pstr) {
            if(*pstr == ' ')
                strncpy(pstr, pstr+1, (strlen(pstr)-1));
            else break;
        }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_trim_end(char *xml_value) {
    if(xml_value && *xml_value) {
        size_t len = strlen(xml_value);
        len--;
        while(len > 0) {
            if((xml_value[len] == '\n') || (xml_value[len] == ' ')) {
                xml_value[len] = '\0';
                --len;
            }
            else break;
        }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_parse_xml_tag(const char *xml_buf, const char *tag, ma_temp_buffer_t *value) {
    if(xml_buf && tag && value) {
        ma_error_t rc = MA_OK;
        char start_tag[MA_MAX_LEN] = {0};
        char end_tag[MA_MAX_LEN] = {0};
        char *pstart  = NULL;
        char *pend    = NULL;
        size_t offset_len = 0;

        sprintf(start_tag, "<%s>", tag);
        sprintf(end_tag, "</%s>", tag);

        pstart = (char*)strstr(xml_buf, start_tag);
        if(!pstart)return MA_ERROR_INVALIDARG;

        pend = (char*)strstr(xml_buf, end_tag);
        if(!pend)return MA_ERROR_INVALIDARG;

        if(pstart > pend)return MA_ERROR_UNEXPECTED; //no idea how to hit this..

        offset_len = pend - (pstart + strlen(start_tag));
        ma_temp_buffer_reserve(value, offset_len+1);

        memcpy((char *)ma_temp_buffer_get(value), pstart+strlen(start_tag), offset_len);
        rc = ma_trim_begin((char *)ma_temp_buffer_get(value));
        if(MA_OK != rc) return rc;
        return ma_trim_end((char *)ma_temp_buffer_get(value));
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t	ma_conf_application_get_property_str(const char *product_id, const char *key, ma_temp_buffer_t *value) {
    if(product_id && key && value) {
        ma_error_t rc = MA_ERROR_UNEXPECTED;
        const char *config_path = NULL;
        struct stat   st;	
        ma_temp_buffer_t full_path = MA_TEMP_BUFFER_INIT;

        form_path(&full_path, MA_CONFIGURATION_REGISTRY_APPLICATIONS_PATH_STR, product_id, MA_FALSE);
        form_path(&full_path, (char *) ma_temp_buffer_get(&full_path), MA_CONFIGURATION_REGISTRY_CONFIG_FILE_NAME_STR, MA_FALSE);				
        config_path = (const char *) ma_temp_buffer_get(&full_path);

        if(0 == stat(config_path, &st)) {
            if(st.st_size > 0) {
                char *xml_buf = NULL;
                ma_temp_buffer_t temp_xml_buf = MA_TEMP_BUFFER_INIT;
                ma_temp_buffer_reserve(&temp_xml_buf, st.st_size + 1);
                xml_buf = (char*)ma_temp_buffer_get(&temp_xml_buf);
                if(xml_buf) {
                    FILE *fp = NULL;
                    fp = fopen(config_path, "r");
                    if(fp) {                        
                        if(st.st_size == fread(xml_buf, 1, st.st_size, fp)) {
							xml_buf[st.st_size] = '\0';
                            if(MA_OK == (rc = ma_parse_xml_tag(xml_buf, key, value))) {
                                rc = MA_OK;
                            }
                        }
                        fclose(fp);
                    }
                }
                ma_temp_buffer_uninit(&temp_xml_buf);
            }
        }
        ma_temp_buffer_uninit(&full_path);
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t	ma_conf_application_get_property_int(const char *product_id, const char *key, unsigned int *value) {
    if(product_id && key && value) {
        ma_error_t rc = MA_OK;
        ma_temp_buffer_t temp_buf = MA_TEMP_BUFFER_INIT;
        if(MA_OK == (rc = ma_conf_application_get_property_str(product_id, key, &temp_buf))) {
            *value = atoi((char *)ma_temp_buffer_get(&temp_buf));
        }
        ma_temp_buffer_uninit(&temp_buf);
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

static void ma_conf_application_release(char *product_id) {
    if(product_id) {
        free(product_id), product_id = NULL;
    }
}

ma_error_t ma_conf_applications_scan(ma_conf_application_list_t *self) {
    if(self) {	
        ma_filesystem_iterator_t *iterator = NULL;
        ma_error_t rc = MA_OK;
        if(MA_OK == ma_filesystem_iterator_create(MA_CONFIGURATION_REGISTRY_APPLICATIONS_PATH_STR, &iterator)) {
            ma_temp_buffer_t temp_buf = MA_TEMP_BUFFER_INIT;
            while(MA_OK == ma_filesystem_iterator_get_next(iterator, &temp_buf)) {
                char *product_id = strdup((const char *) ma_temp_buffer_get(&temp_buf));
                if(product_id)
                    ma_vector_push_back(self, product_id, char *, application_list);
                ma_temp_buffer_uninit(&temp_buf);
            }			
            ma_filesystem_iterator_release(iterator);
        }

        if(MA_OK != rc)  ma_vector_clear(self, ma_conf_applications_t *, application_list, ma_conf_application_release);
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_conf_applications_get_size(ma_conf_application_list_t *self, size_t *list_size) {
    if(self && list_size) {
        *list_size = ma_vector_get_size(self, application_list);
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_conf_applications_release(ma_conf_application_list_t *self) {
    if(self) {
        ma_vector_clear(self, ma_conf_applications_t *, application_list, ma_conf_application_release);
        free(self);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_conf_applications_get_product_id(ma_conf_application_list_t *self, size_t index, const char **product_id) {
    if(self && index >= 0 && product_id) {
        *product_id = ma_vector_at(self, index, char *, application_list);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}
