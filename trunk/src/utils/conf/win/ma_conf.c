#include "ma/internal/utils/conf/ma_conf.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/defs/ma_general_defs.h"

#include <Windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static ma_error_t read_install_path_from_registry(char static_install_path[MA_MAX_PATH_LEN]);
ma_error_t ma_conf_get_install_path(ma_temp_buffer_t *install_path) {
    if(install_path) {
        static char static_install_path[MA_MAX_PATH_LEN] = {0}; 
        ma_error_t rc = MA_OK;
        
        if('\0' == static_install_path[0]) 
            rc = read_install_path_from_registry(static_install_path);

        if(MA_OK == rc) {
            ma_temp_buffer_reserve(install_path, strlen(static_install_path));
            MA_MSC_SELECT(_snprintf, snprintf)((char*)ma_temp_buffer_get(install_path), ma_temp_buffer_capacity(install_path), "%s", static_install_path);
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t read_data_path_from_registry(char static_data_path[MA_MAX_PATH_LEN]);
ma_error_t ma_conf_get_data_path(ma_temp_buffer_t *data_path) {
    if(data_path) {
        static char static_data_path[MA_MAX_PATH_LEN] = {0}; 
        ma_error_t rc = MA_OK;
        
        if('\0' == static_data_path[0]) 
            rc = read_data_path_from_registry(static_data_path);

        if(MA_OK == rc) {
            ma_temp_buffer_reserve(data_path, strlen(static_data_path));
            MA_MSC_SELECT(_snprintf, snprintf)((char*)ma_temp_buffer_get(data_path), ma_temp_buffer_capacity(data_path), "%s", static_data_path);
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_conf_get_logs_path(ma_temp_buffer_t *logs_path) {
    if(logs_path) {
        ma_temp_buffer_t data_path = MA_TEMP_BUFFER_INIT;
        ma_error_t rc = MA_OK;
        if(MA_OK == (rc = ma_conf_get_data_path(&data_path))) {            
            /* strlen of data path + separator + strlen(log directory name) +  NULL terminator*/
            ma_temp_buffer_reserve(logs_path, strlen((char *)ma_temp_buffer_get(&data_path)) + 1 + strlen(MA_CONFIGURATION_LOGS_DIR_NAME_STR) + 1);
            MA_MSC_SELECT(_snprintf, snprintf)((char *)ma_temp_buffer_get(logs_path), ma_temp_buffer_capacity(logs_path),"%s%c%s", (char *)ma_temp_buffer_get(&data_path), MA_PATH_SEPARATOR, MA_CONFIGURATION_LOGS_DIR_NAME_STR);
        }
        ma_temp_buffer_uninit(&data_path);
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_conf_get_keystore_path(ma_temp_buffer_t *keystore_path) {
    if(keystore_path) {
        ma_temp_buffer_t data_path = MA_TEMP_BUFFER_INIT;
        ma_error_t rc = MA_OK;
        if(MA_OK == (rc = ma_conf_get_data_path(&data_path))) {            
            /* strlen of data path + separator + strlen(keystore directory name) +  NULL terminator*/
            ma_temp_buffer_reserve(keystore_path, strlen((char *)ma_temp_buffer_get(&data_path)) + 1 + strlen(MA_CONFIGURATION_KEYSTORE_DIR_NAME_STR) + 1);
            MA_MSC_SELECT(_snprintf, snprintf)((char *)ma_temp_buffer_get(keystore_path), ma_temp_buffer_capacity(keystore_path),"%s%c%s", (char *)ma_temp_buffer_get(&data_path), MA_PATH_SEPARATOR, MA_CONFIGURATION_KEYSTORE_DIR_NAME_STR);
        }
        ma_temp_buffer_uninit(&data_path);
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_conf_get_certstore_path(ma_temp_buffer_t *certstore_path) {
    if(certstore_path) {
        ma_temp_buffer_t data_path = MA_TEMP_BUFFER_INIT;
        ma_error_t rc = MA_OK;
        if(MA_OK == (rc = ma_conf_get_data_path(&data_path))) {
            /* strlen of data path + separator + strlen(certstore_path) +  NULL terminator*/
            ma_temp_buffer_reserve(certstore_path, strlen((char *)ma_temp_buffer_get(&data_path)) + 1 + strlen(MA_CONFIGURATION_CERTSTORE_DIR_NAME_STR) + 1);
            MA_MSC_SELECT(_snprintf, snprintf)((char *)ma_temp_buffer_get(certstore_path), ma_temp_buffer_capacity(certstore_path),"%s%c%s", (char *)ma_temp_buffer_get(&data_path), MA_PATH_SEPARATOR, MA_CONFIGURATION_CERTSTORE_DIR_NAME_STR);
        }
        ma_temp_buffer_uninit(&data_path);
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_conf_get_db_path(ma_temp_buffer_t *db_path) {
    if(db_path) {
        ma_temp_buffer_t data_path = MA_TEMP_BUFFER_INIT;
        ma_error_t rc = MA_OK;
        if(MA_OK == (rc = ma_conf_get_data_path(&data_path))) {            
            /* strlen of data path + separator + strlen(db_path) +  NULL terminator*/
            ma_temp_buffer_reserve(db_path, strlen((char *)ma_temp_buffer_get(&data_path)) + 1 + strlen(MA_CONFIGURATION_DB_DIR_NAME_STR) + 1);
            MA_MSC_SELECT(_snprintf, snprintf)((char *)ma_temp_buffer_get(db_path), ma_temp_buffer_capacity(db_path),"%s%c%s", (char *)ma_temp_buffer_get(&data_path), MA_PATH_SEPARATOR, MA_CONFIGURATION_DB_DIR_NAME_STR);
        }
        ma_temp_buffer_uninit(&data_path);
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_conf_get_lib_path(ma_temp_buffer_t *lib_path) {
    return ma_conf_get_install_path(lib_path);
}

ma_error_t ma_conf_get_crypto_lib_path(ma_temp_buffer_t *crypto_lib_path) {
    return ma_conf_get_install_path(crypto_lib_path);
}

ma_error_t ma_conf_get_tools_lib_path(ma_temp_buffer_t *tools_lib_path) {
    return ma_conf_get_install_path(tools_lib_path);
}

/*TBD  - may be we need to fix this one ?? */
ma_error_t ma_conf_get_rsdk_lib_path(ma_temp_buffer_t *rsdk_lib_path) {
    return ma_conf_get_install_path(rsdk_lib_path);
}



static ma_error_t read_install_path_from_registry(char static_install_path[MA_MAX_PATH_LEN]) {
    HKEY hKey;
    ma_error_t rc = MA_ERROR_DS_PATH_NOT_FOUND;

    /* Always try with 32bit view for install path, only key name is different for 64bit and 32bit applicaitons */
    if(ERROR_SUCCESS == RegOpenKeyExA(HKEY_LOCAL_MACHINE, MA_CONFIGURATION_REGISTRY_AGENT_FULL_PATH_STR, 0, KEY_READ | KEY_WOW64_32KEY, &hKey)) {
        DWORD type = REG_SZ;
        DWORD size = MA_MAX_PATH_LEN;        
        if(ERROR_SUCCESS == RegQueryValueExA(hKey, MA_CONFIGURATION_AGENT_KEY_INSTALL_PATH_STR, 0, &type, (BYTE *)static_install_path, &size)) {
            rc = MA_OK;
        } 
        RegCloseKey(hKey);
    }
    return rc;
}

static ma_error_t read_data_path_from_registry(char static_data_path[MA_MAX_PATH_LEN]) {
    HKEY hKey;
    ma_error_t rc = MA_ERROR_DS_PATH_NOT_FOUND;

    /* Always try with 32bit view for data path */
    if(ERROR_SUCCESS == RegOpenKeyExA(HKEY_LOCAL_MACHINE, MA_CONFIGURATION_REGISTRY_AGENT_FULL_PATH_STR, 0, KEY_READ | KEY_WOW64_32KEY, &hKey)) {
        DWORD type = REG_SZ;
        DWORD size = MA_MAX_PATH_LEN;        
        if(ERROR_SUCCESS == RegQueryValueExA(hKey, MA_CONFIGURATION_AGENT_KEY_DATA_PATH_STR, 0, &type, (BYTE *)static_data_path, &size)) {
            rc = MA_OK;
        } 
        RegCloseKey(hKey);
    }
    return rc;
}
