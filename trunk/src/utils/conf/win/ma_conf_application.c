#include "ma/internal/utils/conf/ma_conf_application.h"
#include "ma/internal/utils/datastructures/ma_vector.h"

#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal/defs/ma_general_defs.h"

#include <Windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct ma_conf_application_list_s {
    ma_vector_define(application_list, char *);
    size_t	size;
};

ma_error_t ma_conf_applications_create(ma_conf_application_list_t **list) {
    if(list) {
        ma_conf_application_list_t *self = (ma_conf_application_list_t *)calloc(1, sizeof(ma_conf_application_list_t));
        if(self) {
            ma_vector_init(self, MA_MAX_APPLICATIONS_INT, application_list, char *);
            *list = self;
            return MA_OK;
        }
        return MA_ERROR_OUTOFMEMORY;

    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t	ma_conf_application_get_property_str(const char *product_id, const char *key, ma_temp_buffer_t *value) {
    if(product_id && key && value) {
        HKEY hKey;
        ma_error_t rc = MA_ERROR_DS_PATH_NOT_FOUND;
        ma_temp_buffer_t temp_path = MA_TEMP_BUFFER_INIT;

        form_path(&temp_path, MA_CONFIGURATION_REGISTRY_APPLICATIONS_PATH_STR, product_id, MA_FALSE);        

        if(ERROR_SUCCESS == RegOpenKeyExA(HKEY_LOCAL_MACHINE, (const char *)ma_temp_buffer_get(&temp_path), 0, KEY_READ | KEY_WOW64_32KEY, &hKey)) {
            unsigned long data_len = 0;
            unsigned long  type = 0;			
            if(ERROR_SUCCESS == RegQueryValueExA(hKey, key, 0, &type, NULL, &data_len)) {
                ma_temp_buffer_reserve(value, data_len);
                rc = (ERROR_SUCCESS == RegQueryValueExA(hKey, key, 0, &type, (unsigned char *)ma_temp_buffer_get(value), &data_len))
                     ? MA_OK : MA_ERROR_DS_GET_FAILED;
            }
            else
                rc = MA_ERROR_DS_KEY_NOT_FOUND;

            RegCloseKey(hKey);
        }

        ma_temp_buffer_uninit(&temp_path);
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t	ma_conf_application_get_property_int(const char *product_id, const char *key, unsigned int *value) {
    if(product_id && key && value) {
        ma_error_t rc = MA_OK;
        ma_temp_buffer_t temp_buf = MA_TEMP_BUFFER_INIT;
        if(MA_OK == (rc = ma_conf_application_get_property_str(product_id, key, &temp_buf))) {
            *value = atoi((char *)ma_temp_buffer_get(&temp_buf));
        }
        ma_temp_buffer_uninit(&temp_buf);
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

static void ma_conf_application_release(char *product_id) {
    if(product_id) {
        free(product_id), product_id = NULL;
    }
}

ma_error_t ma_conf_applications_scan(ma_conf_application_list_t *self) {
    if(self) {
        HKEY hKey;
        ma_error_t rc = MA_ERROR_DS_PATH_NOT_FOUND;

        /*Read 32 bit */
        if(ERROR_SUCCESS == RegOpenKeyExA(HKEY_LOCAL_MACHINE, MA_CONFIGURATION_REGISTRY_APPLICATIONS_PATH_STR, 0, KEY_READ | KEY_WOW64_32KEY, &hKey)) {
            unsigned long total_sub_keys = 0, max_sub_key_len = 0;
            if((ERROR_SUCCESS == RegQueryInfoKeyA(hKey, NULL, NULL, NULL, &total_sub_keys, &max_sub_key_len, NULL, NULL, NULL, NULL, NULL, NULL))) {
                unsigned int i = 0;
                for(i = 0; i < total_sub_keys; i++) {
                    unsigned long len = max_sub_key_len + 1;
                    char *product_id = (char *) calloc(len, sizeof(char));
                    if(!product_id) {
                        rc = MA_ERROR_OUTOFMEMORY;
                        break;
                    }
                    if(ERROR_SUCCESS == RegEnumKeyExA(hKey, i, (char *)product_id, &len, NULL, NULL, NULL, NULL)) {						
                        ma_vector_push_back(self, product_id, char *, application_list);
                    }
                }
                if(i == total_sub_keys) rc = MA_OK;
            }
            RegCloseKey(hKey);
        }

        /*Read 64 bit */
        if(ERROR_SUCCESS == RegOpenKeyExA(HKEY_LOCAL_MACHINE, MA_CONFIGURATION_REGISTRY_APPLICATIONS_PATH_STR, 0, KEY_READ | KEY_WOW64_64KEY, &hKey)) {
            unsigned long total_sub_keys = 0, max_sub_key_len = 0;
            if((ERROR_SUCCESS == RegQueryInfoKeyA(hKey, NULL, NULL, NULL, &total_sub_keys, &max_sub_key_len, NULL, NULL, NULL, NULL, NULL, NULL))) {
                unsigned int i = 0;
                for(i = 0; i < total_sub_keys; i++) {
                    unsigned long len = max_sub_key_len + 1;
                    char *product_id = (char *) calloc(len, sizeof(char));
                    if(!product_id) {
                        rc = MA_ERROR_OUTOFMEMORY;
                        break;
                    }
                    if(ERROR_SUCCESS == RegEnumKeyExA(hKey, i, (char *)product_id, &len, NULL, NULL, NULL, NULL)) {						
                        ma_vector_push_back(self, product_id, char *, application_list);
                    }
                }
                if(i == total_sub_keys) rc = MA_OK;
            }
            RegCloseKey(hKey);
        }

        if(MA_OK != rc)  ma_vector_clear(self, ma_conf_applications_t *, application_list, ma_conf_application_release);
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_conf_applications_get_size(ma_conf_application_list_t *self, size_t *list_size) {
    if(self && list_size) {
        *list_size = ma_vector_get_size(self, application_list);
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_conf_applications_release(ma_conf_application_list_t *self) {
    if(self) {
        ma_vector_clear(self, ma_conf_applications_t *, application_list, ma_conf_application_release);
        free(self);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_conf_applications_get_product_id(ma_conf_application_list_t *self, size_t index, const char **product_id) {
    if(self && product_id) {
        *product_id = ma_vector_at(self, index, char *, application_list);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

