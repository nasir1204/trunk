#ifndef MA_UUID_INTERNAL_H_INCLUDED
#define MA_UUID_INTERNAL_H_INCLUDED
#include "ma/ma_common.h"

MA_CPP(extern "C" {)

typedef struct ma_uuid_s ma_uuid_t, *ma_uuid_h ;

/* UUID import and export formats */
typedef enum ma_uuid_format_e {
    UUID_FMT_BIN = 0,        /* binary representation (import/export)  , 16bytes raw */
    UUID_FMT_TXT = 1,        /* string representation (import/export)  , displaying as per RFC , separated with -  e.g f81d4fae-7dec-11d0-a765-00a0c91e6bf6*/
} ma_uuid_format_t ;

/*NOTE: We need to have the call back for SHA1 functions and we will SHA1 each and every field of UUID*/

/* ma_uuid object creation and control */

/* MAC address octet length */
#define MAC_OCTETS 6

ma_error_t ma_uuid_create(ma_uuid_t **ma_uuid);

ma_error_t ma_uuid_release(ma_uuid_t *ma_uuid) ;

ma_error_t ma_uuid_copy(const ma_uuid_t *ma_uuid , ma_uuid_t **dest);

/* uuid object comparisions */
ma_error_t ma_uuid_compare(const ma_uuid_t *first , const ma_uuid_t *second , short int *result);

ma_error_t ma_uuid_is_nil(const ma_uuid_t *first , short int *result);

/* Generation of uuid */
ma_error_t ma_uuid_generate_inner(ma_uuid_t *ma_uuid, unsigned short int use_mac_address , unsigned char mac_address[MAC_OCTETS]);

/*Export and import functions */
ma_error_t ma_uuid_export(const ma_uuid_t *ma_uuid , ma_uuid_format_t format , void  *data_ptr, size_t data_len);

ma_error_t ma_uuid_import(ma_uuid_t *ma_uuid , ma_uuid_format_t format , void *data_ptr , size_t data_len);

MA_CPP(})

#endif /* MA_UUID_INTERNAL_H_INCLUDED */


/********************************************************* 	Archietcure of UUID  	*******************************/

/*
Summary of UUID speciaification derived from  http://www.itu.int/ITU-T/studygroups/com17/oid/X.667-E.pdf 

1  UUID structure and representation 
1.1	 UUID field structure 
	1.1.1 A UUID is specified as an ordered sequence of six fields. A UUID is specified in terms of the concatenation 
	of these UUID fields. The UUID fields are named: 
			a) the "TimeLow" field; 
			b) the "TimeMid" field; 
			c) the "VersionAndTimeHigh" field; 
			d) the "VariantAndClockSeqHigh" field; 
			e) the "ClockSeqLow" field; 
			f) the "Node" field. 
	6.1.2 The UUID fields are defined to have a significance in the order listed above, with "TimeLow" as the most 
	significant field (bit 31 of "TimeLow" is bit 127 of the UUID), and "Node" as the least significant field (bit 0 of "Node" 
	is bit 0 of the UUID). 

1.2 Binary representation 
	1.2.1 A UUID shall be represented in binary as 16 octets formed by the concatenation of the unsigned integer fixedlength encoding 
	of each of its fields into one or more octets. The number of octets to be used for each field shall be:  
		a) the "TimeLow" field: four octets; 
		b) the "TimeMid" field: two octets; 
		c) the "VersionAndTimeHigh" field: two octets; 
		d) the "VariantAndClockSeqHigh" field: one octet; 
		e) the "ClockSeqLow" field: one octet; 
		f) the "Node" field: six octets. 


1.3 TEXT representation 

For the hexadecimal format, the octets of the binary format shall be represented by a string of hexadecimal digits, using 
two hexadecimal digits for each octet of  the binary format, the first being the value of the four high-order bits of 
octet 15, the second being the value of the four low-order bits of octet 15, and so on, with the last being the value of the 
low-order bits of octet 0 . A HYPHEN-MINUS (45) character  shall be inserted between 
the hexadecimal representations of each pair of adjacent fields, except between the "VariantAndClockSeqHigh" field 
and the "ClockSeqLow" field (e.g f81d4fae-7dec-11d0-a765-00a0c91e6bf6).

NOTE: It is recommended that the hexadecimal representation used in all human-readable formats be restricted to lower-case
letters.

The "hexdigit" lexical item is used in the BNF specification and is defined as follows: 
A B C D E F a b c d e f 0 1 2 3 4 5 6 7 8 9

The hexadecimal representation of a UUID shall be the production "UUID":
UUID ::= 
 TimeLow 
  "-" TimeMid 
  "-" VersionAndTimeHigh 
  "-" VariantAndClockSeqHigh ClockSeqLow 
  "-" Node ISO/IEC 9834-8:2005 (E) 
  ITU-T Rec. X.667 (09/2004)  5 
 TimeLow ::= 
  HexOctet HexOctet HexOctet HexOctet 
 TimeMid ::= 
  HexOctet HexOctet 
 VersionAndTimeHigh ::= 
  HexOctet HexOctet 
 VariantAndClockSeqHigh ::= 
  HexOctet 
 ClockSeqLow ::= 
  HexOctet 
 Node ::=  
  HexOctet HexOctet HexOctet HexOctet HexOctet HexOctet 
 HexOctet ::= 
  hexdigit hexdigit

*/




