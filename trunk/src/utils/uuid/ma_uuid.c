#include "ma/internal/utils/platform/ma_sys_apis.h"
#include "ma/internal/utils/uuid/ma_uuid.h"
#include "ma/internal/ma_macros.h"
#include "ma_uuid_internal.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <time.h>

#if defined(MA_WINDOWS)
#define WIN32_LEAN_AND_MEAN
#include <Winsock2.h>
//#define snprintf _snprintf
#else
#include <sys/time.h>
#endif

#define UUID_LEN_BIN  (128 /*bit*/ / 8 /*bytes*/)
//#define UUID_LEN_TXT  (128 /*bit*/ / 4 /*nibbles*/ + 4 /*hyphens*/ + 1 /*last \0*/)

/* maximum number of 100ns ticks of the actual resolution of system clock
   (which in our case is 1us (= 1000ns) because we use gettimeofday(2) */
#define UUIDS_PER_TICK 10

/* generate a bitmask consisting of 1 bits from (and including)
   bit position `l' (left) to (and including) bit position `r' */
#define BM_MASK(l,r) \
		((((ma_uint16_t)1<<(((l)-(r))+1))-1)<<(r))

/* generate a single bit `b' (0 or 1) at bit position `n' */
#define BM_BIT(n,b) \
		((b)<<(n))

/* generate the value 2^n */
#define BM_POW2(n) \
		BM_BIT(n,1)


/* UUID binary object */
typedef struct ma_uuid_obj_s{
	ma_uint32_t  time_low;                  /* bits  0-31 of time field */
	ma_uint16_t  time_mid;                  /* bits 32-47 of time field */
	ma_uint16_t  time_hi_and_version;       /* bits 48-59 of time field plus 4 bit version */
	ma_uint8_t   clock_seq_hi_and_reserved;  /* bits  8-13 of clock sequence field plus 2 bit variant */
	ma_uint8_t   clock_seq_low;             /* bits  0-7  of clock sequence field */
	ma_uint8_t   node[MAC_OCTETS];          /* bits  0-47 of node MAC address */
}ma_uuid_obj_t;

struct ma_uuid_s {
	ma_uuid_obj_t   obj ;
	struct timeval  time_last;         /* last retrieved timestamp */
	unsigned long   time_seq;	  /* last sequence counter */		
};


/* NOTE NOTE : This is just for POC ...for actual stuff we need to consider the following 
	1. first there will be no unsigned long long which is 64 bit ...may be 
*/

ma_error_t ma_uuid_create(ma_uuid_t **ma_uuid) {
    if(ma_uuid) {
        *ma_uuid = (struct ma_uuid_s *)calloc(1 , sizeof(struct ma_uuid_s));         
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_uuid_release(ma_uuid_t *self) {
    if(self) {
	    free(self);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_uuid_copy(const ma_uuid_t *self , ma_uuid_t **dest) {
        return MA_ERROR_NOT_SUPPORTED ;
}

ma_error_t ma_uuid_copmare(const ma_uuid_t *self , const ma_uuid_t *dest , short int *result) {
	return MA_ERROR_NOT_SUPPORTED ;
}

ma_error_t ma_uuid_is_nil(const ma_uuid_t *self , short int *result) {
	return MA_ERROR_NOT_SUPPORTED ;
}

ma_error_t ma_uuid_generate_inner(ma_uuid_t *self , unsigned short int use_mac_address  , unsigned char mac_address[6]) {
	struct timeval time_now; 
	unsigned long long t ;
	ma_uint16_t clck = 0;

	if(!self) 	
        return MA_ERROR_INVALIDARG ;

    /* determine current system time and sequence counter */
	for (;;) {
        	/* determine current system time */
		if(MA_OK != ma_sys_gettimeofday(&time_now) )
			return MA_ERROR_UNEXPECTED ;

		/* check whether system time changed since last retrieve */
		if (!(time_now.tv_sec  == self->time_last.tv_sec && time_now.tv_usec == self->time_last.tv_usec)) {
			/* reset time sequence counter and continue */
			self->time_seq = 0;
			break;
		}

		/* until we are data_ptr of UUIDs per tick, increment
		the time/tick sequence counter and continue */
		if (self->time_seq < UUIDS_PER_TICK) {
			self->time_seq++;
			break ;
		}

		/* stall the UUID generation until the catches up */
		ma_sys_usleep(1);
	}

	t = ((unsigned long long )time_now .tv_sec * 10000000)
		+ ((unsigned long long )time_now.tv_usec * 10)
		+ 0x01B21DD213814000LL;
	

	/* compensate for low resolution system clock by adding
	the time/tick sequence counter */
	if (self->time_seq > 0)
		t += self->time_seq ;

	self->obj.time_low = (ma_uint32_t)(t & 0xFFFFFFFF);
	self->obj.time_mid = (ma_uint16_t)((t >> 32) & 0xFFFF);
	self->obj.time_hi_and_version =(ma_uint16_t)((t >> 48) & 0x0FFF);
	self->obj.time_hi_and_version |= (1 << 12); 


	/* retrieve current clock sequence */
	clck = ((self->obj.clock_seq_hi_and_reserved & BM_MASK(5,0)) << 8)
		+ self->obj.clock_seq_low;

	/* generate new random clock sequence (initially or if the
		time has stepped backwards) or else just increase it */
	if (clck == 0
		|| (time_now.tv_sec < self->time_last.tv_sec
		|| (time_now.tv_sec == self->time_last.tv_sec
		&& time_now.tv_usec < self->time_last.tv_usec))) {

		ma_prng_gen_random_number( (void *)&clck , sizeof(clck));
	}
	else
		clck++;

	clck %= BM_POW2(14);

	/* store back new clock sequence */
	self->obj.clock_seq_hi_and_reserved = (self->obj.clock_seq_hi_and_reserved & BM_MASK(7,6)) | (ma_uint8_t)((clck >> 8) & 0xff);
	self->obj.clock_seq_low = (ma_uint8_t) (clck & 0xff); 

	if(use_mac_address && (NULL != mac_address))
		memcpy(self->obj.node , mac_address , MAC_OCTETS) ;
	else 
		ma_prng_gen_random_number( (void *)self->obj.node , MAC_OCTETS);

	/* remember current system time for next iteration */
	self->time_last.tv_sec  = time_now.tv_sec;
	self->time_last.tv_usec = time_now.tv_usec;

	/*NOTE VERSION and reserverd we are not using it as of now */
	
	return MA_OK ;
}

 ma_error_t uuid_export_bin(const ma_uuid_t *self , void  *data_ptr, size_t data_len)
{
	ma_uint32_t tmp32 ;
	ma_uint16_t tmp16 ;
	unsigned int i = 0 ;
	unsigned char *out = (unsigned char *)data_ptr ;


	memset(data_ptr , 0 , data_len);

	/* pack time_low */
	tmp32 = self->obj.time_low;
	out[3] = (ma_uint8_t)(tmp32 & 0xff); tmp32 >>= 8;
	out[2] = (ma_uint8_t)(tmp32 & 0xff); tmp32 >>= 8;
	out[1] = (ma_uint8_t)(tmp32 & 0xff); tmp32 >>= 8;
	out[0] = (ma_uint8_t)(tmp32 & 0xff);

	/* pack "time_mid"  */
	tmp16 = self->obj.time_mid;
	out[5] = (ma_uint8_t)(tmp16 & 0xff); tmp16 >>= 8;
	out[4] = (ma_uint8_t)(tmp16 & 0xff);

	/* pack "time_hi_and_version"  */
	tmp16 = self->obj.time_hi_and_version;
	out[7] = (ma_uint8_t)(tmp16 & 0xff); tmp16 >>= 8;
	out[6] = (ma_uint8_t)(tmp16 & 0xff);

	/* pack "clock_seq_hi_and_reserved"  */
	out[8] = self->obj.clock_seq_hi_and_reserved;

	/* pack "clock_seq_low" field */
	out[9] = self->obj.clock_seq_low;

	/* pack "node" field */
	for (i = 0; i < (unsigned int)sizeof(self->obj.node); i++)
        	out[10+i] = self->obj.node[i];
	
	return MA_OK;
}
ma_error_t uuid_export_txt(const ma_uuid_t *self , void  *data_ptr, size_t data_len) {

	memset(data_ptr , 0 , data_len);

// %02x%02x%02x%02x%02x%02x" 
    MA_MSC_SELECT(_snprintf, snprintf)((char*)data_ptr , data_len  , "CRN-%08lx%04x%04x%02x%02x", \
			                            (unsigned long)self->obj.time_low,\
			                            (unsigned int)self->obj.time_mid,\
			                            (unsigned int)self->obj.time_hi_and_version,\
			                            (unsigned int)self->obj.clock_seq_hi_and_reserved,\
			                            (unsigned int)self->obj.clock_seq_low);
#if 0
			                            (unsigned int)self->obj.node[0],\
			                            (unsigned int)self->obj.node[1],\
			                            (unsigned int)self->obj.node[2],\
			                            (unsigned int)self->obj.node[3],\
			                            (unsigned int)self->obj.node[4],\
			                            (unsigned int)self->obj.node[5] );	
#endif
	return MA_OK ;
}


ma_error_t ma_uuid_export(const ma_uuid_t *self , ma_uuid_format_t format , void  *data_ptr, size_t data_len) {
	ma_error_t rc = MA_OK ;
	if(NULL == self || NULL == data_ptr ) 
        return MA_ERROR_INVALIDARG ;

	switch (format) {
		case UUID_FMT_BIN : 
			if( data_len < UUID_LEN_BIN)		return MA_ERROR_INVALIDARG ;
			rc = uuid_export_bin(self , data_ptr , data_len) ;
			break ;
		case UUID_FMT_TXT:
			if( data_len < UUID_LEN_TXT)		return MA_ERROR_INVALIDARG ;
			rc = uuid_export_txt(self , data_ptr , data_len) ;
			break ;
		default :
			return MA_ERROR_INVALIDARG ;
	}
	return rc;		
}

ma_error_t ma_uuid_import(ma_uuid_t *self , ma_uuid_format_t format , void *data_ptr , size_t data_len) {
    return MA_ERROR_NOT_SUPPORTED;
}

ma_error_t ma_uuid_generate(unsigned short int use_mac_address , unsigned char mac_address[MAC_OCTETS], char uuid[UUID_LEN_TXT]) {
    if(uuid) {
        ma_error_t rc = MA_OK;
        ma_uuid_t self;
    
        memset(&self, 0, sizeof(ma_uuid_t));
        if(MA_OK == (rc = ma_uuid_generate_inner(&self, use_mac_address, mac_address)))
                rc = ma_uuid_export(&self , UUID_FMT_TXT, uuid, UUID_LEN_TXT);        
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}
