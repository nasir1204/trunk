#include <string.h>
#include <curl/curl.h>

#include "ma/internal/utils/sms/ma_sms.h"
#include "ma/logger/ma_logger.h"
#include "ma/internal/ma_macros.h"

#define MA_MAX_SMS_SEND_LIMIT 1600

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "sms"


static ma_logger_t *logger;
/*
* _twilio_null_write is a portable way to ignore the response from 
* curl_easy_perform
*/
size_t _twilio_null_write(char *ptr, size_t size, size_t nmemb, void *userdata)
{
    return size * nmemb;
}

void ma_sms_set_logger(ma_logger_t *logr) {
    logger = logr;
}

static void replace_space_per20(char str[]) 
{ 
    // count spaces and find current length 
    int space_count = 0, i, j, new_length = 0, index = 0; 
    for (i = 0; str[i]; i++) 
        if (str[i] == ' ') 
            space_count++; 
  
    // Remove trailing spaces 
    while (str[i-1] == ' ') 
    { 
       space_count--; 
       i--; 
    } 
  
    // Find new length. 
    new_length = i + space_count * 2 + 1; 
  
    // New length must be smaller than length 
    // of string provided. 
    if (new_length > MA_MAX_BUFFER_LEN) 
        return ; 
  
    // Start filling character from end 
    index = new_length - 1; 
  
    // Fill string termination. 
    str[index--] = '\0'; 
  
    // Fill rest of the string from end 
    for (j=i-1; j>=0; j--) 
    { 
        // inserts %20 in place of space 
        if (str[j] == ' ') 
        { 
            str[index] = '0'; 
            str[index - 1] = '2'; 
            str[index - 2] = '%'; 
            index = index - 3; 
        } 
        else
        { 
            str[index] = str[j]; 
            index--; 
        } 
    } 
  
} 
/*
* twilio_send_message gathers the necessary parameters for a Twilio SMS or MMS.
* 
* Inputs:
*         - account_sid: Account SID from the Twilio console.
*         - auth_token: Authorization from the Twilio console.
*         - to_number: Where to send the MMS or SMS
*         - from_number: Number in your Twilio account to use as a sender.
*         - message_body: (Max: 1600 characters) The body of the MMS or SMS 
*               message which will be sent to the to_number.
*         - verbose: Whether to print all the responses
* 
*  Optional:
*         - picture_url: If picture URL is not NULL and is a valid image url, a
                MMS will be sent by Twilio.
*/
int ma_sms_send_message(char *account_sid,
                        char *auth_token,
                        char *message,
                        char *from_number,
                        char *to_number,
                        char *picture_url,
                        boolean verbose)
{

        // See: https://www.twilio.com/docs/api/rest/sending-messages for
        // information on Twilio body size limits.
        if (strlen(message) > MA_MAX_SMS_SEND_LIMIT) {
            MA_LOG(logger,MA_LOG_SEV_DEBUG, "SMS send failed.Message body must be less than 1601 characters.The message had %zu characters.", strlen(message));
            return -1;
        }

        CURL *curl;
        CURLcode res;
        curl_global_init(CURL_GLOBAL_ALL);
        curl = curl_easy_init();

        char url[MA_MAX_SMS_MESSAGE_SIZE];
        snprintf(url,
                 sizeof(url),
                 "%s%s%s",
                 "https://api.twilio.com/2010-04-01/Accounts/",
                 account_sid,
                 "/Messages");

        char parameters[MA_MAX_SMS_MESSAGE_SIZE];
        if (!picture_url) {
            snprintf(parameters,
                     sizeof(parameters),
                     "%s%s%s%s%s%s",
                     "To=",
                     to_number,
                     "&From=",
                     from_number,
                     "&Body=",
                     message);
        } else {
            snprintf(parameters,
                     sizeof(parameters),
                     "%s%s%s%s%s%s%s%s",
                     "To=",
                     to_number,
                     "&From=",
                     from_number,
                     "&Body=",
                     message,
                     "&MediaUrl=",
                     picture_url);
        }


        curl_easy_setopt(curl, CURLOPT_POST, 1);
        curl_easy_setopt(curl, CURLOPT_URL, url);
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, parameters);
        curl_easy_setopt(curl, CURLOPT_USERNAME, account_sid);
        curl_easy_setopt(curl, CURLOPT_PASSWORD, auth_token);

        if (!verbose) {
                curl_easy_setopt(curl, 
                                 CURLOPT_WRITEFUNCTION, 
                                 _twilio_null_write);
        }

        res = curl_easy_perform(curl);
        curl_easy_cleanup(curl);

        long http_code = 0;
        curl_easy_getinfo (curl, CURLINFO_RESPONSE_CODE, &http_code);

        if (res != CURLE_OK) {
            MA_LOG(logger,MA_LOG_SEV_ERROR, "SMS send failed: %s.\n", curl_easy_strerror(res));
                return -1;
        } else if (http_code != 200 && http_code != 201) {
            MA_LOG(logger,MA_LOG_SEV_ERROR, "SMS send failed, HTTP Status Code: %ld.\n", http_code);
                return -1;
        } else {
            MA_LOG(logger,MA_LOG_SEV_DEBUG, "SMS send successfully\n");
                return 0;
        }
}

void ma_sms_send(const char *protocol, const char *server, const char *port, const char *path, const char *contact, const char *msg) {
  CURL *curl;
  CURLcode res;
 
  /* In windows, this will init the winsock stuff */ 
  curl_global_init(CURL_GLOBAL_ALL);
 
  /* get a curl handle */ 
  curl = curl_easy_init();
  if(curl) {
    char buf[MA_MAX_BUFFER_LEN*2+1] = {0};
    char replace_buf[MA_MAX_BUFFER_LEN+1] = {0};

    strncpy(replace_buf, msg, MA_MAX_BUFFER_LEN);
    replace_space_per20(replace_buf);
    snprintf(buf, MA_MAX_BUFFER_LEN*2, "%s://%s:%s%s?number=%s&msg=%s", protocol, server, port, path, contact, replace_buf);
    MA_LOG(logger, MA_LOG_SEV_TRACE, "message send url (%s) ", buf);
    /* First set the URL that is about to receive our POST. This URL can
       just as well be a https:// URL if that is what should receive the
       data. */ 
    //curl_easy_setopt(curl, CURLOPT_URL, "http://mileaccess.com:8080/autmunIS/users/enableservice?contact=560083&msg=true");
    curl_easy_setopt(curl, CURLOPT_URL, buf);
    curl_easy_setopt(curl, CURLOPT_URL, buf);
    /* Now specify the POST data */ 
    /*curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "name=daniel&project=curl");*/
    /*curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "{\"contact\":\"9886412843\",\"email\":\"zabeen.azra1202@gmail.com\"}");*/
 
    /* Perform the request, res will get the return code */ 
    res = curl_easy_perform(curl);
    /* Check for errors */ 
    if(res != CURLE_OK)
      MA_LOG(logger, MA_LOG_SEV_ERROR, "message send failed (%s) ", curl_easy_strerror(res));
 
    /* always cleanup */ 
    curl_easy_cleanup(curl);
  }
  curl_global_cleanup();
}
