#include "ma/internal/utils/serialization/ma_deserialize.h"
#include "ma/internal/ma_macros.h"
#include "ma/ma_buffer.h"
#include "ma/internal/utils/text/ma_utf8.h"

#define	MSGPACK_UNPACKER_INIT_BUFFER_SIZE 1024

#include <msgpack.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#define MA_ASCII_INT8	0x61

static ma_error_t ma_deserializer_validate(const ma_deserializer_t *deserializer);

struct ma_deserializer_s {
    msgpack_unpacker *unpk;
};

ma_error_t ma_deserializer_validate(const ma_deserializer_t *deserializer) {
    return (deserializer && deserializer->unpk) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_deserializer_create(ma_deserializer_t **deserializer) {
    if(deserializer) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;

        if((*deserializer = (ma_deserializer_t*)calloc(1, sizeof(ma_deserializer_t)))) {
            if(((*deserializer)->unpk = msgpack_unpacker_new(MSGPACK_UNPACKER_INIT_BUFFER_SIZE))) {
                err = MA_OK;
            }
        }
        if(MA_OK != err) {
            (void)ma_deserializer_destroy(*deserializer);
            *deserializer = NULL;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_deserializer_destroy(ma_deserializer_t *deserializer) {
    if(deserializer) {
        if(deserializer->unpk)msgpack_unpacker_free(deserializer->unpk);
        free(deserializer);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_deserializer_put(ma_deserializer_t *deserializer, const char *serialized_buffer, size_t len) {
    if(deserializer && serialized_buffer && (MA_OK ==  ma_deserializer_validate(deserializer))) {
        ma_error_t err = MA_OK;

        if(len) {
            err = MA_ERROR_OUTOFMEMORY;
            if(msgpack_unpacker_reserve_buffer(deserializer->unpk, len)) {
                memcpy(msgpack_unpacker_buffer(deserializer->unpk), serialized_buffer, len);
                msgpack_unpacker_buffer_consumed(deserializer->unpk, len); err = MA_OK;
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_deserializer_unpacker(msgpack_object o, ma_variant_t **var_obj) {
    ma_error_t err = MA_OK;

    switch(o.type) {
        case MSGPACK_OBJECT_NIL: {
            err = ma_variant_create(var_obj);
            break;
        }
        case MSGPACK_OBJECT_BOOLEAN: {
			err = ma_variant_create_from_bool(o.via.boolean, var_obj);            
            break;
        }
		case MSGPACK_OBJECT_POSITIVE_INTEGER: {
			err = ma_variant_create_from_uint64(o.via.u64, var_obj);            
            break;
        }
		case MSGPACK_OBJECT_NEGATIVE_INTEGER: {
			err = ma_variant_create_from_int64(o.via.i64, var_obj);            
            break;
        }
		case MSGPACK_OBJECT_DOUBLE: {
			err = ma_variant_create_from_double(o.via.dec, var_obj);            
            break;
        }
		case MSGPACK_OBJECT_RAW: {
			err = ma_variant_create_from_raw(o.via.raw.ptr ? o.via.raw.ptr : "", o.via.raw.size, var_obj);
            break;
		}
		case MSGPACK_OBJECT_MAP: {
            msgpack_object_kv *pend = NULL;
            ma_table_t *var_table = NULL;

            if(MA_OK == (err = ma_table_create(&var_table))) {
                if(o.via.map.size != 0) {
					/*list of pairs.*/
                    msgpack_object_kv* p = o.via.map.ptr;

                    if(p) {
                        pend = o.via.map.ptr + o.via.map.size;
                        for(; p < pend; ++p) {
                            ma_variant_t *v = NULL;

                            if(MA_OK == (err = ma_deserializer_unpacker(p->val, &v))) {
								char *key = NULL;
								if(key = (char*)calloc(p->key.via.raw.size+1, 1)){
									memcpy(key, p->key.via.raw.ptr, p->key.via.raw.size);
									key[p->key.via.raw.size] = '\0';
									err = ma_table_add_entry(var_table, key, v);
									free(key);
								}
								(void)ma_variant_release(v);
                            }
                        }
                    }
                }
                if(MA_OK == err)
                    err = ma_variant_create_from_table(var_table, var_obj);
                (void)ma_table_release(var_table);
            }
            break;
        }
        case MSGPACK_OBJECT_ARRAY: {
            ma_array_t *var_array = NULL;

            if(MA_OK == (err = ma_array_create(&var_array))) {
                if(o.via.array.size != 0) {
                    msgpack_object *p = NULL;
                    msgpack_object *pend = NULL;
                
                    p = o.via.array.ptr; err = MA_ERROR_UNEXPECTED;
                    if(p) {
                        pend = o.via.array.ptr + o.via.array.size;
                        for(; p < pend; ++p) {
                            ma_variant_t *v = NULL;

                            if(MA_OK == (err = ma_deserializer_unpacker(*p, &v))) {
                                (void) ma_array_push(var_array, v);                                
								(void) ma_variant_release(v);
							}
                        }
                    }
                }            
                if(MA_OK == err)
                    err = ma_variant_create_from_array(var_array, var_obj);
                (void)ma_array_release(var_array);
            }
            break;
        }       
        default:
            err = MA_ERROR_UNEXPECTED;
            break;
    }
    return err;
}
/*Message completely parsed or some data data leftover (e.g. incomplete data) */
static ma_bool_t ma_deserializer_buffer_parsed(msgpack_unpacker* mpac) {
	if(mpac){
		if(mpac->used == mpac->off){
			return MA_TRUE;
		}
	}
	return MA_FALSE;
}

static ma_error_t ma_deserializer_get(ma_deserializer_t *deserializer, ma_variant_t **var_obj) {
#ifdef HPUX_ALIGNMENT_TRAP_HANDLER
	allow_unaligned_data_access();
#endif
    *var_obj = NULL;
    if(MA_OK ==  ma_deserializer_validate(deserializer)) {
        ma_error_t err = MA_ERROR_UNEXPECTED;
        msgpack_unpacked unpack_object = {0};

        msgpack_unpacked_init(&unpack_object);
        if (msgpack_unpacker_next(deserializer->unpk, &unpack_object)) {
            err = ma_deserializer_unpacker(unpack_object.data, var_obj);
        }
        msgpack_unpacked_destroy(&unpack_object);

		/*free the created objects if final deserialization failed.*/
		if(MA_OK != err) {			
			(void)ma_variant_release(*var_obj);
			*var_obj = NULL;
		}
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_deserializer_get_next(ma_deserializer_t *deserializer, ma_variant_t **var_obj) {
#ifdef HPUX_ALIGNMENT_TRAP_HANDLER
	allow_unaligned_data_access();
#endif   
	if(deserializer && var_obj)
		return ma_deserializer_get(deserializer, var_obj);	
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_deserializer_reset(ma_deserializer_t *deserializer, ma_bool_t forced) {
    if(deserializer) {
        ma_error_t rc = MA_ERROR_OUTOFMEMORY ;	
		if(MA_FALSE == forced){
			if(!ma_deserializer_buffer_parsed(deserializer->unpk)){
				return MA_OK ; /*message not completly parsed*/
			}
		}
	
        msgpack_unpacker_destroy(deserializer->unpk) ;
        if(msgpack_unpacker_init(deserializer->unpk, MSGPACK_UNPACKER_INIT_BUFFER_SIZE)) {
            return MA_OK ;
        }
        free(deserializer->unpk) ; deserializer->unpk = NULL ;
        return rc ; 
    }
	return MA_ERROR_INVALIDARG;
}