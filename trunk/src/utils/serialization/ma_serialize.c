#include "ma/internal/utils/serialization/ma_serialize.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/text/ma_utf8.h"

#define	MSGPACK_UNPACKER_INIT_BUFFER_SIZE 1024

#include <msgpack.h>
#include <wchar.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>


static ma_error_t ma_serializer_validate(const ma_serializer_t* serializer);

struct ma_serializer_s {
    msgpack_sbuffer *buffer;
    msgpack_packer  *pk;
};

#define MA_SERIALIZER_TYPE_GENERATOR(data_type) \
        { \
            MA_TOKEN_CONSTRUCTOR(ma_##data_type,_t) value; \
			\
            if(MA_OK == (err = MA_TOKEN_CONSTRUCTOR(ma_variant_get_, data_type)(var_obj, &value))) { \
                err = MA_ERROR_UNEXPECTED;\
                if(!(rc = MA_TOKEN_CONSTRUCTOR(msgpack_pack_, data_type)(serializer->pk, value))) {\
                    err = MA_OK; \
				}\
            }\
            \
            break; \
		}


ma_error_t ma_serializer_validate(const ma_serializer_t* serializer) {
    return (serializer && serializer->buffer && serializer->pk) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_serializer_create(ma_serializer_t **serializer) {
    if(serializer) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;

        if((*serializer = (ma_serializer_t*)calloc(1, sizeof(ma_serializer_t)))) {
            if(((*serializer)->buffer = msgpack_sbuffer_new())) {
                if(((*serializer)->pk = msgpack_packer_new((*serializer)->buffer, msgpack_sbuffer_write))) {
                    err = MA_OK;
                }
            }
        }
        if(MA_OK != err) {
            (void)ma_serializer_destroy(*serializer); *serializer = NULL;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_serializer_destroy(ma_serializer_t *serializer) {
    if(serializer) {
        if(serializer->buffer)msgpack_sbuffer_free(serializer->buffer);
        if(serializer->pk)msgpack_packer_free(serializer->pk);
        free(serializer);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_serializer_add_variant(ma_serializer_t *serializer, const ma_variant_t *var_obj) {
    if(serializer && var_obj && (MA_OK == ma_serializer_validate(serializer))) {
        ma_error_t err = MA_OK;
        int rc = 0; /*ret code*/
        ma_vartype_t var_type = MA_VARTYPE_NULL;

        (void)ma_variant_get_type(var_obj, &var_type);
        switch(var_type) {
            case MA_VARTYPE_NULL: {
                if(0 != (rc = msgpack_pack_nil(serializer->pk)))
                    err = MA_ERROR_UNEXPECTED;
                break;
            }
            case MA_VARTYPE_BOOL: {
                ma_bool_t value = MA_FALSE;

                if(MA_OK == (err = ma_variant_get_bool(var_obj, &value))) {
                    if(0 != (rc = (value?msgpack_pack_true(serializer->pk):msgpack_pack_false(serializer->pk)))) {
                        err = MA_ERROR_UNEXPECTED;
                    }
                }
                break;
            }
            case MA_VARTYPE_INT64:
                MA_SERIALIZER_TYPE_GENERATOR(int64)
            case MA_VARTYPE_UINT64:
                MA_SERIALIZER_TYPE_GENERATOR(uint64)
            case MA_VARTYPE_INT32:
                MA_SERIALIZER_TYPE_GENERATOR(int32)
            case MA_VARTYPE_UINT32:
                MA_SERIALIZER_TYPE_GENERATOR(uint32)
            case MA_VARTYPE_INT16:
                MA_SERIALIZER_TYPE_GENERATOR(int16)
            case MA_VARTYPE_UINT16:
                MA_SERIALIZER_TYPE_GENERATOR(uint16)
            case MA_VARTYPE_INT8:
                MA_SERIALIZER_TYPE_GENERATOR(int8)
            case MA_VARTYPE_UINT8:
                MA_SERIALIZER_TYPE_GENERATOR(uint8)
            case MA_VARTYPE_FLOAT:
                MA_SERIALIZER_TYPE_GENERATOR(float)
            case MA_VARTYPE_DOUBLE:
                MA_SERIALIZER_TYPE_GENERATOR(double)
            case MA_VARTYPE_STRING: 
            case MA_VARTYPE_RAW: {
                ma_buffer_t *raw_buffer = NULL;
                unsigned char* raw = NULL;
                size_t raw_len = 0;
                
                if(MA_OK == (err = ma_variant_get_raw_buffer((ma_variant_t*)var_obj, &raw_buffer))) {
                    if(MA_OK == (err = ma_buffer_get_raw(raw_buffer, (const unsigned char**)&raw, &raw_len))) {
						err = MA_ERROR_UNEXPECTED;
                        if(!(rc = msgpack_pack_raw(serializer->pk, raw_len))) {
                            if(!(rc = msgpack_pack_raw_body(serializer->pk, raw, raw_len))) {
                                err = MA_OK;
                            }
                        }                        
                    }
                    (void)ma_buffer_release(raw_buffer);
                }
                break;
            }
            case MA_VARTYPE_ARRAY: {
                ma_array_t *array = NULL;
                size_t array_size = 0;
                size_t index = 0;

                err = ma_variant_get_array((ma_variant_t*)var_obj, &array);
                if(MA_OK == err) {
                    if (MA_OK == (err = ma_array_size(array, &array_size))) {
                        err = MA_ERROR_UNEXPECTED;
                        if (0 == msgpack_pack_array(serializer->pk, (unsigned int)array_size)) {
                            err = MA_OK;
                            for(index = 1; index <= array_size; ++index) {
                                ma_variant_t *v = NULL;
                                if (MA_OK == (err = ma_array_get_element_at(array, index, &v))) {
                                    err = ma_serializer_add_variant(serializer, v);
                                    ma_variant_release(v);
                                }
                                if (MA_OK != err) break; /* break out of loop if one fails*/
                            }
                        }
                    }
                    ma_array_release(array);
                } else {
                    return err;
                }
                break;
            }
            case MA_VARTYPE_TABLE: {
                ma_table_t *table = NULL;
                ma_array_t *array = NULL;
                size_t array_size = 0;
                size_t index = 0;

                if(MA_OK == (err = ma_variant_get_table((ma_variant_t*)var_obj, &table))) {
                    if(MA_OK == (err = ma_table_get_keys(table, &array))) {
                        if(MA_OK == (err = ma_array_size(array, &array_size))) {

                            if(!(rc = msgpack_pack_map(serializer->pk, (unsigned int)array_size))) {
								
                                for(index = 1; index <= array_size; ++index) { /*values*/
                                    ma_variant_t *k = NULL, *v = NULL;
									if (MA_OK == (err = ma_array_get_element_at(array, index, &k))) {
										ma_buffer_t *buffer = NULL;
										if (MA_OK == (err = ma_variant_get_string_buffer(k, &buffer))) {											
											const char *key_name = NULL;
											size_t size = 0;
											if(MA_OK == (err = ma_buffer_get_string(buffer, &key_name, &size))){
												if(MA_OK == (err = ma_table_get_value(table, key_name, &v))){
													if(MA_OK == (err = ma_serializer_add_variant(serializer, k)))
														err = ma_serializer_add_variant(serializer, v);
													(void)ma_variant_release(v);
												}
											}
											(void)ma_buffer_release(buffer);
										}
										(void)ma_variant_release(k);
									}
									if (MA_OK != err) break; /* break out of loop if one fails*/
								}
	                        }
							(void)ma_array_release(array);
						}
						(void)ma_table_release(table);
					}
				}
                break;
            }
        }
		return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_serializer_get(ma_serializer_t *serializer, char  **bptr, size_t *len) {
    if(serializer && bptr && len) {
        ma_error_t err = MA_ERROR_INVALIDARG;

        if(serializer->buffer) {
            *bptr = serializer->buffer->data;
            *len = serializer->buffer->size; err = MA_OK;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_serializer_length(ma_serializer_t *serializer, size_t *len) {
    if(serializer && len) {
        ma_error_t err = MA_ERROR_INVALIDARG;

        if(serializer->buffer) {
            *len= serializer->buffer->size; err = MA_OK;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_serializer_reset(ma_serializer_t *serializer) {
    if(serializer) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;

        if(serializer->buffer)msgpack_sbuffer_free(serializer->buffer);
        if(serializer->pk)msgpack_packer_free(serializer->pk); serializer->pk = NULL;
		if((serializer->buffer = msgpack_sbuffer_new())) {
            if((serializer->pk = msgpack_packer_new(serializer->buffer, msgpack_sbuffer_write))) {
                err = MA_OK;
            }
			else{
				msgpack_sbuffer_free(serializer->buffer);
				serializer->buffer = NULL;
			}
        }

        return err;

    }
    return MA_ERROR_INVALIDARG;
}

