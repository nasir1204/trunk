#include "ma/internal/utils/regex/ma_regex.h"

#include <trex.h>
#include <stdlib.h>


struct ma_regex_s {
    /*! TRex object from Trex */
    TRex       *exp;    
};

ma_error_t ma_regex_create(const char *pattern , ma_regex_t **regex) {
    if(pattern && regex) {
        ma_regex_t *self = (ma_regex_t *) malloc(sizeof(ma_regex_t));
        if (self) {
            char const *error_trex = NULL ;
            self->exp = trex_compile(pattern, &error_trex);
            if(self->exp) {
                *regex = self ;
                return MA_OK;
            }
            free(self);
            return MA_ERROR_REGEX_CREATE_FAILED;
        }        
        return MA_ERROR_OUTOFMEMORY;
    }
    
    return MA_ERROR_INVALIDARG;    
}

ma_error_t ma_regex_release(ma_regex_t *self) {
    if (self) {    
        trex_free(self->exp);
        free(self);
		return MA_OK;
    }
	return MA_ERROR_INVALIDARG;
}

ma_bool_t ma_regex_match(ma_regex_t *self , const char *text) {
    if(self && text ) {
        return trex_match(self->exp , text);
    }
    return MA_FALSE;
}

