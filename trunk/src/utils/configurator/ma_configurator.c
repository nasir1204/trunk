#include "ma_configurator_internal.h"
#include "ma/internal/utils/configurator/ma_configurator_utils.h"
#include "ma/internal/utils/conf/ma_conf.h"

#include "ma/internal/utils/repository/ma_repository_db_manager.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"

#include "ma/datastore/ma_ds_database.h"
#include "ma/internal/utils/database/ma_db_statement.h"

/* Policy and task DB schemas */
#include "ma/internal/services/policy/ma_policy_db.h"
#include "ma/internal/services/policy/ma_task_db.h"
#include "ma/internal/utils/xml/ma_xml.h"
#include "ma/internal/buildinfo.h"

#include "ma/policy/ma_policy.h"
#include "ma/internal/clients/policy/ma_policy_bag_internal.h"
#include "ma/internal/clients/policy/ma_policy_uri_internal.h"
#include "ma/internal/services/policy/ma_policy_service_internal.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#ifdef MA_WINDOWS
#include <windows.h>
#endif

static void ma_configurator_destroy(ma_configurator_ex_t *self);
static ma_error_t get_ds_info(ma_configurator_ex_t *configurator, int ds_type, struct ma_datastore_info_s *ds_info);
static ma_error_t populate_agent_configuration(ma_configurator_ex_t *configurator, struct ma_configuration_s *configuration);


static ma_configurator_methods_t methods = {&get_ds_info, &populate_agent_configuration, &ma_configurator_destroy};

static ma_error_t basic_configurator_create(ma_configurator_ex_t **configurator){
	if(configurator) {
        ma_error_t rc = MA_OK;
        ma_configurator_ex_t *self = NULL;
        self = (ma_configurator_ex_t *)calloc(1, sizeof(ma_configurator_ex_t));
        if(self) {
            ma_temp_buffer_t install_path_buf= MA_TEMP_BUFFER_INIT;
            if(MA_OK == (rc = ma_conf_get_install_path(&install_path_buf))) {
                ma_temp_buffer_t data_path_buf = MA_TEMP_BUFFER_INIT;

                strncpy(self->agent_install_path, (const char *)ma_temp_buffer_get(&install_path_buf), MA_MAX_LEN -1);
                ma_temp_buffer_uninit(&install_path_buf);

                if(MA_OK == (rc = ma_conf_get_data_path(&data_path_buf))) {
                    strncpy(self->agent_data_path, (const char *)ma_temp_buffer_get(&data_path_buf), MA_MAX_LEN -1);
                    ma_temp_buffer_uninit(&data_path_buf);

                    MA_MSC_SELECT(_snprintf, snprintf)(self->agent_lib_path, MA_MAX_LEN, "%s%c%s", self->agent_install_path, MA_PATH_SEPARATOR, MA_CONFIGURATION_LIB_DIR_NAME_STR);
		            MA_MSC_SELECT(_snprintf, snprintf)(self->agent_tools_lib_path, MA_MAX_LEN, "%s%c%s%c%s", self->agent_install_path, MA_PATH_SEPARATOR,MA_CONFIGURATION_LIB_DIR_NAME_STR, MA_PATH_SEPARATOR,MA_CONFIGURATION_TOOLS_DIR_NAME_STR);
		        
		            ma_configurator_init((ma_configurator_t*)self, &methods, self);
                    *configurator = self;
                    return MA_OK;
                }
            }
            free(self);
		    return rc;
        }
        return MA_ERROR_OUTOFMEMORY;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_configurator_create(ma_agent_configuration_t *agent_configuration, ma_configurator_t **configurator){
    if(agent_configuration && configurator) {
        ma_error_t rc = MA_OK;
        ma_configurator_ex_t *self = NULL;

        if(MA_OK != (rc = basic_configurator_create(&self)))
            return rc;
        
         memcpy(&self->agent_configuration, agent_configuration, sizeof(ma_agent_configuration_t));

        *configurator = (ma_configurator_t*)self;
        return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_service_configurator_create(ma_configurator_t **configurator){
	if(configurator) {
		ma_error_t rc = MA_OK;
		ma_configurator_ex_t *self = NULL;
		ma_datastore_configuration_request_t req = {{MA_TRUE,MA_DB_OPEN_READ_WITH_REPAIR}, {MA_FALSE,0}, {MA_FALSE,0}, {MA_FALSE,0}, {MA_FALSE,0}, {MA_FALSE, 0}, {MA_FALSE,0}, {MA_FALSE, 0}, {MA_FALSE, 0}, {MA_FALSE, 0}, {MA_FALSE, 0}, {MA_FALSE, 0}};
		if(MA_OK != (rc = basic_configurator_create(&self)))
			return rc;

		/*Fill the agent configuration by reading from the database.*/
		if(MA_OK == (rc = ma_configurator_intialize_datastores((ma_configurator_t*)self, &req))){			
			if(MA_OK == (rc = ma_ds_get_int16((ma_ds_t*)self->ds_default, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_MODE_INT, (ma_int16_t *)&self->agent_configuration.agent_mode))){
				if(MA_OK == (rc = ma_ds_get_int16((ma_ds_t*)self->ds_default, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_CRYPTO_MODE_INT, (ma_int16_t *)&self->agent_configuration.agent_crypto_mode))){
					if(MA_OK == (rc = ma_ds_get_int16((ma_ds_t*)self->ds_default, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_CRYPTO_ROLE_INT, (ma_int16_t *)&self->agent_configuration.agent_crypto_role))){
						if(MA_OK == (rc = ma_ds_get_int16((ma_ds_t*)self->ds_default, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_VDI_MODE_STR, (ma_int16_t *)&self->agent_configuration.vdi_mode))){

							ma_buffer_t *guid = NULL;
							ma_buffer_t *tenant_id = NULL;

							if(MA_OK != ma_ds_get_int32((ma_ds_t*)self->ds_default, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_SEQ_NUM_INT, &self->agent_configuration.seq_num)){
								self->agent_configuration.seq_num = 0;
							}

							if(MA_OK == ma_ds_get_str((ma_ds_t*)self->ds_default, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_GUID_STR, &guid)){
								const char *guid_str = NULL;
								size_t size = 0;
								ma_buffer_get_string(guid, &guid_str, &size);
								if(guid_str){
									strncpy(self->agent_configuration.agent_guid, guid_str, UUID_LEN_TXT);
								}
								ma_buffer_release(guid);
								if(MA_OK == ma_ds_get_str((ma_ds_t*)self->ds_default, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_TENANT_ID_STR, &tenant_id)){
									const char *tenant_id_str = NULL;
									size_t size = 0;
									ma_buffer_get_string(tenant_id, &tenant_id_str, &size);
									if(tenant_id_str){
										strncpy(self->agent_configuration.tenant_id, tenant_id_str, MA_MAX_KEY_LEN);
									}
									ma_buffer_release(tenant_id);
								}
							}
						}
					}
				}
			}
		}			

		if(MA_OK != rc){
			ma_configurator_destroy(self);
			return rc;
		}

		*configurator = (ma_configurator_t*)self;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

void ma_configurator_destroy(ma_configurator_ex_t *self) {
    if(self) {
        if(self->ds_scheduler) ma_ds_database_release(self->ds_scheduler);
        if(self->db_scheduler) ma_db_close(self->db_scheduler);
        if(self->ds_booking) ma_ds_database_release(self->ds_booking);
        if(self->db_booking) ma_db_close(self->db_booking);
        if(self->ds_profile) ma_ds_database_release(self->ds_profile);
        if(self->db_profile) ma_db_close(self->db_profile);
        if(self->ds_comment) ma_ds_database_release(self->ds_comment);
        if(self->db_comment) ma_db_close(self->db_comment);
        if(self->ds_consignment) ma_ds_database_release(self->ds_consignment);
        if(self->db_consignment) ma_db_close(self->db_consignment);
        if(self->ds_inventory) ma_ds_database_release(self->ds_inventory);
        if(self->db_inventory) ma_db_close(self->db_inventory);
        if(self->db_policy) ma_db_close(self->db_policy);
		if(self->db_task)   ma_db_close(self->db_task);

        if(self->ds_default) ma_ds_database_release(self->ds_default);
        if(self->db_default) ma_db_close(self->db_default);

        free(self);
    }
}

/*You can do lazy initialization of DS here if wanted.*/
ma_error_t get_ds_info(ma_configurator_ex_t *self, int ds_type, struct ma_datastore_info_s *ds_info){
	if(self && ds_info){
		switch(ds_type){
			case MA_DS_INFO_DEFAULT:
				ds_info->ds = (ma_ds_t*)self->ds_default;
				ds_info->db = self->db_default;
				break;
			case MA_DS_INFO_POLICY:
				ds_info->ds = NULL;
				ds_info->db = self->db_policy;
				break;
			case MA_DS_INFO_TASK:
				ds_info->ds = NULL;
				ds_info->db = self->db_task;
				break;
			case MA_DS_INFO_SCHEDULER:
				ds_info->ds = (ma_ds_t*)self->ds_scheduler;
				ds_info->db = NULL;
				break;
			case MA_DS_INFO_MSGBUS:
				ds_info->ds = NULL;
				ds_info->db = self->db_msgbus;
                                break;
                        case MA_DS_INFO_BOOKING:
                                ds_info->ds = (ma_ds_t*)self->ds_booking;
                                ds_info->db = NULL;
                                break;
                        case MA_DS_INFO_PROFILE:
                                ds_info->ds = (ma_ds_t*)self->ds_profile;
                                ds_info->db = NULL;
                                break;
                        case MA_DS_INFO_INVENTORY:
                                ds_info->ds = (ma_ds_t*)self->ds_inventory;
                                ds_info->db = NULL;
                                break;
                        case MA_DS_INFO_RECORDER:
                                ds_info->ds = (ma_ds_t*)self->ds_recorder;
                                ds_info->db = NULL;
                                break;
                        case MA_DS_INFO_LOCATION:
                                ds_info->ds = (ma_ds_t*)self->ds_location;
                                ds_info->db = NULL;
                                break;
                        case MA_DS_INFO_SEASON:
                                ds_info->ds = (ma_ds_t*)self->ds_season;
                                ds_info->db = NULL;
                                break;
                        case MA_DS_INFO_SEARCH:
                                ds_info->ds = (ma_ds_t*)self->ds_search;
                                ds_info->db = NULL;
                                break;
                        case MA_DS_INFO_BOARD:
                                ds_info->ds = (ma_ds_t*)self->ds_board;
                                ds_info->db = NULL;
                                break;
                        case MA_DS_INFO_COMMENT:
                                ds_info->ds = (ma_ds_t*)self->ds_comment;
                                ds_info->db = NULL;
                                break;
                        case MA_DS_INFO_CONSIGNMENT:
                                ds_info->ds = (ma_ds_t*)self->ds_consignment;
                                ds_info->db = NULL;
                                break;
			default:
				return MA_ERROR_INVALID_OPTION;
		}
	}
	return MA_ERROR_INVALIDARG;
}

static ma_error_t populate_agent_configuration(ma_configurator_ex_t *self, struct ma_configuration_s *configuration){
	if(self && configuration){
		configuration->agent_configuration		= &self->agent_configuration;
		configuration->agent_data_path			= self->agent_data_path;
		configuration->agent_install_path		= self->agent_install_path;
		configuration->agent_lib_path			= self->agent_lib_path;
		configuration->agent_tools_lib_path     = self->agent_tools_lib_path;
	}
	return MA_OK;
}

