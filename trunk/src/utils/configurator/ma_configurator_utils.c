#include "ma/internal/utils/configurator/ma_configurator_utils.h"
#include "ma/internal/utils/acl/ma_acl.h"

#include "ma_configurator_internal.h"
#include "ma/internal/utils/app_info/ma_registry_store.h"

#include "ma/internal/utils/proxy/ma_proxy_db_manager.h"
#include "ma/internal/utils/repository/ma_repository_db_manager.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"

#include "ma/datastore/ma_ds_database.h"
#include "ma/internal/utils/database/ma_db_statement.h"

/* Policy and task DB schemas */
#include "ma/internal/services/policy/ma_policy_db.h"
#include "ma/internal/services/policy/ma_task_db.h"
#include "ma/internal/apps/macmnsvc/ma_common_services_db.h"
#include "ma/internal/utils/xml/ma_xml.h"
#include "ma/internal/buildinfo.h"

#include "ma/policy/ma_policy.h"
#include "ma/internal/clients/policy/ma_policy_bag_internal.h"
#include "ma/internal/clients/policy/ma_policy_uri_internal.h"
#include "ma/internal/services/policy/ma_policy_service_internal.h"
#include "ma/internal/services/io/ma_logger_db.h"

#include "ma/internal/services/ma_service_utils.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#ifdef MA_WINDOWS
#include <windows.h>
#endif

#define MA_POLICY_FEATURE_ID            "EPOAGENTMETA"
#define MA_POLICY_CATEGORY_ID           "General"
#define MA_POLICY_TYPE_ID               "General"
#define MA_POLICY_NAME_ID               "My Default"
#define MA_REPO_POLICY_CATEGORY_ID      "Repository"
#define MA_REPO_POLICY_TYPE_ID          "Repository"


ma_error_t ma_configurator_intialize_datastores(ma_configurator_t *configurator,  ma_datastore_configuration_request_t *request) {
    if(configurator && request){
        ma_error_t rc = MA_OK;
        char buffer[MA_MAX_LEN] = {0};
		ma_configurator_ex_t *self = (ma_configurator_ex_t *)configurator;

		/*Close all db if already opened.*/
		if(self->ds_scheduler) ma_ds_database_release(self->ds_scheduler); self->ds_scheduler = NULL;
        if(self->db_scheduler) ma_db_close(self->db_scheduler); self->db_scheduler = NULL;

		if(self->ds_booking) ma_ds_database_release(self->ds_booking); self->ds_booking = NULL;
        if(self->db_booking) ma_db_close(self->db_booking); self->db_booking = NULL;

		if(self->ds_inventory) ma_ds_database_release(self->ds_inventory); self->ds_inventory = NULL;
        if(self->db_inventory) ma_db_close(self->db_inventory); self->db_inventory = NULL;

		if(self->ds_consignment) ma_ds_database_release(self->ds_consignment); self->ds_consignment = NULL;
        if(self->db_consignment) ma_db_close(self->db_consignment); self->db_consignment = NULL;

		if(self->ds_recorder) ma_ds_database_release(self->ds_recorder); self->ds_recorder = NULL;
        if(self->db_recorder) ma_db_close(self->db_recorder); self->db_recorder = NULL;

		if(self->ds_location) ma_ds_database_release(self->ds_location); self->ds_location = NULL;
        if(self->db_location) ma_db_close(self->db_location); self->db_location = NULL;

		if(self->ds_season) ma_ds_database_release(self->ds_season); self->ds_season = NULL;
        if(self->db_season) ma_db_close(self->db_season); self->db_season = NULL;

		if(self->ds_search) ma_ds_database_release(self->ds_search); self->ds_search = NULL;
        if(self->db_search) ma_db_close(self->db_search); self->db_search = NULL;

		if(self->ds_profile) ma_ds_database_release(self->ds_profile); self->ds_profile = NULL;
        if(self->db_profile) ma_db_close(self->db_profile); self->db_profile = NULL;

		if(self->ds_board) ma_ds_database_release(self->ds_board); self->ds_board = NULL;
        if(self->db_board) ma_db_close(self->db_board); self->db_board = NULL;

		if(self->ds_comment) ma_ds_database_release(self->ds_comment); self->ds_comment = NULL;
        if(self->db_comment) ma_db_close(self->db_comment); self->db_comment = NULL;

        if(self->db_policy) ma_db_close(self->db_policy); self->db_policy = NULL;
		if(self->db_task)   ma_db_close(self->db_task); self->db_task = NULL;

        if(self->ds_default) ma_ds_database_release(self->ds_default); self->ds_default = NULL;
        if(self->db_default) ma_db_close(self->db_default); self->db_default = NULL;
		
        if(request->ma_default.is_required){
			MA_MSC_SELECT(_snprintf, snprintf)(buffer, MA_MAX_LEN -1,"%s%c%s%c%s", self->agent_data_path,MA_PATH_SEPARATOR, MA_CONFIGURATION_DB_DIR_NAME_STR,MA_PATH_SEPARATOR, MA_DB_DEFAULT_FILENAME_STR);
            if(MA_OK == (rc = ma_db_open(buffer, request->ma_default.db_flags,&self->db_default))) {
				rc = ma_ds_database_create(self->db_default, MA_DS_DEFAULT_INSTANCE_NAME, &self->ds_default);
				/*over-right agent version, to support dll to dll upgrade scenario */
				ma_ds_set_str((ma_ds_t*)self->ds_default, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_VERSION_STR, MA_VERSION_STRING, -1);
			}		
		}
		
        if(MA_OK == rc && request->ma_policy.is_required) {
			memset(buffer, 0, MA_MAX_LEN);      
			MA_MSC_SELECT(_snprintf, snprintf)(buffer, MA_MAX_LEN -1,"%s%c%s%c%s", self->agent_data_path,MA_PATH_SEPARATOR, MA_CONFIGURATION_DB_DIR_NAME_STR,MA_PATH_SEPARATOR, MA_DB_POLICY_FILENAME_STR);
			rc = ma_db_open(buffer, request->ma_policy.db_flags, &self->db_policy);			
        }

        if(MA_OK == rc && request->ma_task.is_required) {
			memset(buffer, 0, MA_MAX_LEN);      
			MA_MSC_SELECT(_snprintf, snprintf)(buffer, MA_MAX_LEN -1,"%s%c%s%c%s", self->agent_data_path,MA_PATH_SEPARATOR, MA_CONFIGURATION_DB_DIR_NAME_STR,MA_PATH_SEPARATOR, MA_DB_TASK_FILENAME_STR);
			rc = ma_db_open(buffer, request->ma_task.db_flags, &self->db_task);			
        }          
        
        if(MA_OK == rc && request->ma_scheduler.is_required) {			
            memset(buffer, 0, MA_MAX_LEN);      
            MA_MSC_SELECT(_snprintf, snprintf)(buffer, MA_MAX_LEN -1,"%s%c%s%c%s", self->agent_data_path,MA_PATH_SEPARATOR, MA_CONFIGURATION_DB_DIR_NAME_STR,MA_PATH_SEPARATOR, MA_DB_SCHEDULER_FILENAME_STR);
            if(MA_OK == (rc = ma_db_open(buffer, request->ma_scheduler.db_flags,&self->db_scheduler))) {
                rc = ma_ds_database_create(self->db_scheduler, MA_DS_SCHEDULER_INSTANCE_NAME, &self->ds_scheduler);
            }
        }
        if(MA_OK == rc && request->ma_msgbus.is_required) {
            memset(buffer, 0, MA_MAX_LEN);      
            MA_MSC_SELECT(_snprintf, snprintf)(buffer, MA_MAX_LEN -1,"%s%c%s%c%s", self->agent_data_path,MA_PATH_SEPARATOR, MA_CONFIGURATION_DB_DIR_NAME_STR,MA_PATH_SEPARATOR, MA_DB_COMMON_SERVICES_FILENAME_STR);
            rc = ma_db_open(buffer, request->ma_msgbus.db_flags, &self->db_msgbus);
        }
        if(MA_OK == rc && request->ma_booking.is_required) {
            memset(buffer, 0, MA_MAX_LEN);      
            MA_MSC_SELECT(_snprintf, snprintf)(buffer, MA_MAX_LEN -1,"%s%c%s%c%s", self->agent_data_path,MA_PATH_SEPARATOR, MA_CONFIGURATION_DB_DIR_NAME_STR,MA_PATH_SEPARATOR, MA_DB_BOOKING_SERVICES_FILENAME_STR);
            if(MA_OK == (rc = ma_db_open(buffer, request->ma_booking.db_flags, &self->db_booking))) {
                rc = ma_ds_database_create(self->db_booking, MA_DS_BOOKING_INSTANCE_NAME, &self->ds_booking);
            }
        }
        if(MA_OK == rc && request->ma_profile.is_required) {
            memset(buffer, 0, MA_MAX_LEN);      
            MA_MSC_SELECT(_snprintf, snprintf)(buffer, MA_MAX_LEN -1,"%s%c%s%c%s", self->agent_data_path,MA_PATH_SEPARATOR, MA_CONFIGURATION_DB_DIR_NAME_STR,MA_PATH_SEPARATOR, MA_DB_PROFILE_SERVICES_FILENAME_STR);
            if(MA_OK == (rc = ma_db_open(buffer, request->ma_profile.db_flags, &self->db_profile))) {
                rc = ma_ds_database_create(self->db_profile, MA_DS_PROFILE_INSTANCE_NAME, &self->ds_profile);
            }
        }
        if(MA_OK == rc && request->ma_inventory.is_required) {
            memset(buffer, 0, MA_MAX_LEN);      
            MA_MSC_SELECT(_snprintf, snprintf)(buffer, MA_MAX_LEN -1,"%s%c%s%c%s", self->agent_data_path,MA_PATH_SEPARATOR, MA_CONFIGURATION_DB_DIR_NAME_STR,MA_PATH_SEPARATOR, MA_DB_INVENTORY_SERVICES_FILENAME_STR);
            if(MA_OK == (rc = ma_db_open(buffer, request->ma_inventory.db_flags, &self->db_inventory))) {
                rc = ma_ds_database_create(self->db_inventory, MA_DS_INVENTORY_INSTANCE_NAME, &self->ds_inventory);
            }
        }
        if(MA_OK == rc && request->ma_recorder.is_required) {
            memset(buffer, 0, MA_MAX_LEN);      
            MA_MSC_SELECT(_snprintf, snprintf)(buffer, MA_MAX_LEN -1,"%s%c%s%c%s", self->agent_data_path,MA_PATH_SEPARATOR, MA_CONFIGURATION_DB_DIR_NAME_STR,MA_PATH_SEPARATOR, MA_DB_RECORDER_SERVICES_FILENAME_STR);
            if(MA_OK == (rc = ma_db_open(buffer, request->ma_recorder.db_flags, &self->db_recorder))) {
                rc = ma_ds_database_create(self->db_recorder, MA_DS_RECORDER_INSTANCE_NAME, &self->ds_recorder);
            }
        }
        if(MA_OK == rc && request->ma_location.is_required) {
            memset(buffer, 0, MA_MAX_LEN);      
            MA_MSC_SELECT(_snprintf, snprintf)(buffer, MA_MAX_LEN -1,"%s%c%s%c%s", self->agent_data_path,MA_PATH_SEPARATOR, MA_CONFIGURATION_DB_DIR_NAME_STR,MA_PATH_SEPARATOR, MA_DB_LOCATION_SERVICES_FILENAME_STR);
            if(MA_OK == (rc = ma_db_open(buffer, request->ma_location.db_flags, &self->db_location))) {
                rc = ma_ds_database_create(self->db_location, MA_DS_LOCATION_INSTANCE_NAME, &self->ds_location);
            }
        }
        if(MA_OK == rc && request->ma_season.is_required) {
            memset(buffer, 0, MA_MAX_LEN);      
            MA_MSC_SELECT(_snprintf, snprintf)(buffer, MA_MAX_LEN -1,"%s%c%s%c%s", self->agent_data_path,MA_PATH_SEPARATOR, MA_CONFIGURATION_DB_DIR_NAME_STR,MA_PATH_SEPARATOR, MA_DB_SEASON_SERVICES_FILENAME_STR);
            if(MA_OK == (rc = ma_db_open(buffer, request->ma_season.db_flags, &self->db_season))) {
                rc = ma_ds_database_create(self->db_season, MA_DS_SEASON_INSTANCE_NAME, &self->ds_season);
            }
        }
        if(MA_OK == rc && request->ma_search.is_required) {
            memset(buffer, 0, MA_MAX_LEN);      
            MA_MSC_SELECT(_snprintf, snprintf)(buffer, MA_MAX_LEN -1,"%s%c%s%c%s", self->agent_data_path,MA_PATH_SEPARATOR, MA_CONFIGURATION_DB_DIR_NAME_STR,MA_PATH_SEPARATOR, MA_DB_SEARCH_SERVICES_FILENAME_STR);
            if(MA_OK == (rc = ma_db_open(buffer, request->ma_search.db_flags, &self->db_search))) {
                rc = ma_ds_database_create(self->db_search, MA_DS_SEARCH_INSTANCE_NAME, &self->ds_search);
            }
        }
        if(MA_OK == rc && request->ma_board.is_required) {
            memset(buffer, 0, MA_MAX_LEN);      
            MA_MSC_SELECT(_snprintf, snprintf)(buffer, MA_MAX_LEN -1,"%s%c%s%c%s", self->agent_data_path,MA_PATH_SEPARATOR, MA_CONFIGURATION_DB_DIR_NAME_STR,MA_PATH_SEPARATOR, MA_DB_BOARD_SERVICES_FILENAME_STR);
            if(MA_OK == (rc = ma_db_open(buffer, request->ma_board.db_flags, &self->db_board))) {
                rc = ma_ds_database_create(self->db_board, MA_DS_BOARD_INSTANCE_NAME, &self->ds_board);
            }
        }
        if(MA_OK == rc && request->ma_comment.is_required) {
            memset(buffer, 0, MA_MAX_LEN);      
            MA_MSC_SELECT(_snprintf, snprintf)(buffer, MA_MAX_LEN -1,"%s%c%s%c%s", self->agent_data_path,MA_PATH_SEPARATOR, MA_CONFIGURATION_DB_DIR_NAME_STR,MA_PATH_SEPARATOR, MA_DB_COMMENT_SERVICES_FILENAME_STR);
            if(MA_OK == (rc = ma_db_open(buffer, request->ma_comment.db_flags, &self->db_comment))) {
                rc = ma_ds_database_create(self->db_comment, MA_DS_COMMENT_INSTANCE_NAME, &self->ds_comment);
            }
        }
        if(MA_OK == rc && request->ma_consignment.is_required) {
            memset(buffer, 0, MA_MAX_LEN);      
            MA_MSC_SELECT(_snprintf, snprintf)(buffer, MA_MAX_LEN -1,"%s%c%s%c%s", self->agent_data_path,MA_PATH_SEPARATOR, MA_CONFIGURATION_DB_DIR_NAME_STR,MA_PATH_SEPARATOR, MA_DB_CONSIGNMENT_SERVICES_FILENAME_STR);
            if(MA_OK == (rc = ma_db_open(buffer, request->ma_consignment.db_flags, &self->db_consignment))) {
                rc = ma_ds_database_create(self->db_consignment, MA_DS_CONSIGNMENT_INSTANCE_NAME, &self->ds_consignment);
            }
        }

        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t change_ownership(char *obj_name, const char *username, ma_int64_t inheritance_flags, ma_bool_t allow_inheritance){
	ma_acl_t *acl = NULL;
	ma_error_t rc = MA_OK;
	#ifndef MA_WINDOWS
	if(MA_OK == (rc = ma_acl_create(MA_SID_LOCAL, username, &acl))){
		if(MA_OK == (rc = ma_acl_add(acl, MA_TRUE, 0644, inheritance_flags))){
			rc = ma_acl_attach_object(acl, obj_name, MA_ACL_OBJECT_FILE, allow_inheritance);
		}
		ma_acl_release(acl);
	}	
	#endif
	return rc;
}

ma_error_t ma_configurator_reconfigure_datastores_permissions(ma_configurator_t *configurator){
	#ifndef MA_WINDOWS
	if(configurator) {
		ma_error_t rc = MA_OK;
		ma_configurator_ex_t *self = (ma_configurator_ex_t *)configurator;
		char buffer[MA_MAX_LEN] = {0};        

		MA_MSC_SELECT(_snprintf, snprintf)(buffer, MA_MAX_LEN -1,"%s%c%s%c%s", self->agent_data_path,MA_PATH_SEPARATOR, MA_CONFIGURATION_DB_DIR_NAME_STR,MA_PATH_SEPARATOR, MA_DB_DEFAULT_FILENAME_STR);
        if(MA_OK != (rc = change_ownership(buffer, MA_MFEAGENT_USERNAME_STR, MA_ACL_NO_INHERITANCE, MA_FALSE))) return rc;
		
        MA_MSC_SELECT(_snprintf, snprintf)(buffer, MA_MAX_LEN -1,"%s%c%s%c%s", self->agent_data_path,MA_PATH_SEPARATOR, MA_CONFIGURATION_DB_DIR_NAME_STR,MA_PATH_SEPARATOR, MA_DB_POLICY_FILENAME_STR);
		if(MA_OK != (rc = change_ownership(buffer, MA_MFEAGENT_USERNAME_STR, MA_ACL_NO_INHERITANCE, MA_FALSE))) return rc;

		MA_MSC_SELECT(_snprintf, snprintf)(buffer, MA_MAX_LEN -1,"%s%c%s%c%s", self->agent_data_path,MA_PATH_SEPARATOR, MA_CONFIGURATION_DB_DIR_NAME_STR,MA_PATH_SEPARATOR, MA_DB_COMMON_SERVICES_FILENAME_STR);        
		rc = change_ownership(buffer, MA_MFEAGENT_USERNAME_STR, MA_ACL_NO_INHERITANCE, MA_FALSE);

		return rc;
	}
	return MA_ERROR_INVALIDARG;
	#else
	return MA_OK;
	#endif
}


 ma_error_t ma_configurator_set_guid(ma_configurator_t *configurator, char guid[UUID_LEN_TXT]) {
     if(configurator) {
         ma_configurator_ex_t *self = (ma_configurator_ex_t *)configurator;
		 memset(self->agent_configuration.agent_guid, 0, UUID_LEN_TXT);
         strncpy(self->agent_configuration.agent_guid, guid, UUID_LEN_TXT-1);
         return MA_OK;
     }
     return MA_ERROR_INVALIDARG;
}

 ma_error_t ma_configurator_set_tenant_id(ma_configurator_t *configurator, char tenant_id[MA_MAX_KEY_LEN]) {
     if(configurator) {
         ma_configurator_ex_t *self = (ma_configurator_ex_t *)configurator;
		 strncpy(self->agent_configuration.tenant_id, tenant_id, MA_MAX_KEY_LEN-1);
         return MA_OK;
     }
     return MA_ERROR_INVALIDARG;
}

static ma_error_t add_agent_configuration_settings(ma_configurator_ex_t *self) {
    ma_ds_t *ds = (ma_ds_t*)self->ds_default;
    ma_error_t rc = MA_OK;
    char buffer[MA_MAX_LEN] = {0};        
    const char *language = "N/A", *server_key_hash = "N/A", *prop_version = "0", *last_asci_time = "0", *last_policy_update_time = "0";
    char *last_prop_collection_status = "1";
	char *last_policy_enforcement_status = "1";
	ma_int32_t str_len = 0;
    ma_int16_t current_mode = 0;
    ma_bool_t is_exists = MA_FALSE;
	ma_buffer_t *value = NULL;

    self->agent_configuration.mode_changed = MA_FALSE;
    if(MA_OK == ma_ds_get_int16(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_MODE_INT, &current_mode)) {
        if(self->agent_configuration.agent_mode != current_mode) 
            self->agent_configuration.mode_changed = MA_TRUE;
    }
    
    rc = ( (MA_OK == (rc = ma_ds_set_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_INSTALL_PATH_STR, self->agent_install_path, -1)))
        && (MA_OK == (rc = ma_ds_set_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_DATA_PATH_STR, self->agent_data_path, -1)))
        && (MA_OK == (rc = ma_ds_set_int(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_MODE_INT, self->agent_configuration.agent_mode)))
		&& (MA_OK == (rc = ma_ds_set_int(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_VDI_MODE_STR, self->agent_configuration.vdi_mode)))
        && (MA_OK == (rc = ma_ds_set_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_SOFTWARE_ID_STR, MA_SOFTWAREID_GENERAL_STR, -1)))
        && (MA_OK == (rc = ma_ds_set_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_LANGUAGE_STR, language, -1)))
        && (MA_OK == (rc = ma_ds_set_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_SERVER_KEY_HASH_STR, server_key_hash, -1)))
        && (MA_OK == (rc = ma_ds_set_int(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_CRYPTO_ROLE_INT, self->agent_configuration.agent_crypto_role)))
        && (MA_OK == (rc = ma_ds_set_int(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_CRYPTO_MODE_INT, self->agent_configuration.agent_crypto_mode)))
        && (MA_OK == (rc = ma_ds_set_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_VERSION_STR, MA_VERSION_STRING, -1))))
        ? MA_OK : rc;

	if(self->agent_configuration.seq_num >= 0) {
		ma_int32_t seq_no_in_db = 0;
		if(MA_OK == ma_ds_get_int32(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_SEQ_NUM_INT, &seq_no_in_db)){
			if(seq_no_in_db > self->agent_configuration.seq_num)
				self->agent_configuration.seq_num = seq_no_in_db;
		}
		if (MA_OK != (rc = ma_ds_set_int(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_SEQ_NUM_INT, self->agent_configuration.seq_num + 1)))
			return rc;
	}

	if(0 < (str_len = strlen(self->agent_data_path)) ){
		if(self->agent_data_path[str_len - 1] == MA_PATH_SEPARATOR)
			self->agent_data_path[str_len - 1] = '\0';
	}

    if((MA_OK == rc) && ('\0' != self->agent_configuration.agent_guid[0]))
        rc = ma_ds_set_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_GUID_STR, self->agent_configuration.agent_guid, -1);
	
	if((MA_OK == rc) && ('\0' != self->agent_configuration.tenant_id[0]))
		rc = ma_ds_set_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_TENANT_ID_STR, self->agent_configuration.tenant_id, -1);
    
	if(MA_OK == rc) {
        MA_MSC_SELECT(_snprintf, snprintf)(buffer, MA_MAX_LEN -1,"%s%c%s", self->agent_data_path, MA_PATH_SEPARATOR, MA_CONFIGURATION_LOGS_DIR_NAME_STR);
        rc = ma_ds_set_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_LOGS_PATH_STR,buffer, -1);
    }

    if(MA_OK == rc) {
        memset(buffer,0,MA_MAX_LEN);
        MA_MSC_SELECT(_snprintf, snprintf)(buffer, MA_MAX_LEN -1,"%s%c%s", self->agent_data_path, MA_PATH_SEPARATOR, MA_CONFIGURATION_KEYSTORE_DIR_NAME_STR);
        rc = ma_ds_set_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_KEYSTORE_PATH_STR, buffer, -1);
    }

    if(MA_OK == rc) {
        memset(buffer,0,MA_MAX_LEN);
        MA_MSC_SELECT(_snprintf, snprintf)(buffer, MA_MAX_LEN -1,"%s%c%s", self->agent_data_path, MA_PATH_SEPARATOR, MA_CONFIGURATION_CERTSTORE_DIR_NAME_STR);
        rc = ma_ds_set_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_CERTSTORE_PATH_STR, buffer, -1);
    }

    if(MA_OK == rc) {
        memset(buffer,0,MA_MAX_LEN);
        MA_MSC_SELECT(_snprintf, snprintf)(buffer, MA_MAX_LEN -1,"%s%c%s", self->agent_data_path, MA_PATH_SEPARATOR, MA_CONFIGURATION_DB_DIR_NAME_STR);
        rc = ma_ds_set_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_DB_PATH_STR, buffer, -1);
    }   

    if(MA_OK == rc) {        
#ifdef MA_WINDOWS
        rc = ma_ds_set_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_RSDK_PATH_STR, self->agent_install_path, -1);
#else
        memset(buffer,0,MA_MAX_LEN);
        MA_MSC_SELECT(_snprintf, snprintf)(buffer, MA_MAX_LEN -1,"%s%c%s", self->agent_data_path, MA_PATH_SEPARATOR, "rsdk");  /* TBD - rsdk string must be macro */
        rc = ma_ds_set_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_RSDK_PATH_STR, buffer, -1);
#endif          
    }
    
	if(MA_OK == ma_ds_get_str(ds, MA_PROPERTY_DATA_PATH_NAME_STR, MA_PROPERTY_DATA_KEY_PROPERTY_VERSION_STR, &value))
        (void)ma_buffer_release(value);
	else
		rc = ma_ds_set_str(ds, MA_PROPERTY_DATA_PATH_NAME_STR, MA_PROPERTY_DATA_KEY_PROPERTY_VERSION_STR, prop_version, -1);

	
	if(MA_OK == ma_ds_get_str(ds, MA_POLICY_DATA_PATH_NAME_STR, MA_POLICY_UPDATE_TIME_STR, &value))
        (void)ma_buffer_release(value);
	else
		rc = ma_ds_set_str(ds, MA_POLICY_DATA_PATH_NAME_STR, MA_POLICY_UPDATE_TIME_STR, last_policy_update_time, -1);

	if(MA_OK == ma_ds_get_str(ds, MA_PROPERTY_PROPERTIES_PATH_NAME_STR, MA_PROPERTY_PROPERTIES_KEY_LAST_COLLECTION_STATUS, &value))
        (void)ma_buffer_release(value);
	else
		rc = ma_ds_set_str(ds, MA_PROPERTY_PROPERTIES_PATH_NAME_STR, MA_PROPERTY_PROPERTIES_KEY_LAST_COLLECTION_STATUS, last_prop_collection_status, -1);

	if(MA_OK == ma_ds_get_str(ds, MA_PROPERTY_PROPERTIES_PATH_NAME_STR, MA_PROPERTY_PROPERTIES_KEY_LAST_POLICY_ENFORCEMENT_STATUS, &value))
        (void)ma_buffer_release(value);
	else
		rc = ma_ds_set_str(ds, MA_PROPERTY_PROPERTIES_PATH_NAME_STR, MA_PROPERTY_PROPERTIES_KEY_LAST_POLICY_ENFORCEMENT_STATUS, last_policy_enforcement_status, -1);

    return rc;
}

static ma_error_t add_property_service_policies(ma_configurator_ex_t *self, ma_policy_uri_t *general_uri, ma_policy_bag_t *policy_bag) {
    const char *properties_collection_timeout = "60"; /* 60 minutes */
    const char *property_collect_session_timeout = "1" ; /* 1 seconds per provider */
    const char *property_collect_full_props = "1" ; /* full collect props */
	const char *property_collect_if_delay_by_days = "1" ; /* incr props */
    const char *props_version = "0" ;   
    const char *is_installed = "1", *is_enabled = (MA_CRYPTO_AGENT_MANAGED == self->agent_configuration.agent_mode) ? "1" : "0" ;

    ma_error_t rc = MA_OK;
    ma_variant_t *value = NULL;

    if(MA_OK == (rc = ma_variant_create_from_string(is_installed, strlen(is_installed), &value))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_PROPERTY_SERVICE_SECTION_NAME_STR, MA_PROPERTY_KEY_IS_INSTALLED_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }
    
    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(is_enabled, strlen(is_enabled), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_PROPERTY_SERVICE_SECTION_NAME_STR, MA_PROPERTY_KEY_IS_ENABLED_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }

    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(props_version, strlen(props_version), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_PROPERTY_SERVICE_SECTION_NAME_STR, MA_PROPERTY_DATA_KEY_PROPERTY_VERSION_STR, value); 
        (void) ma_variant_release(value); value = NULL;
    }
    
    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(properties_collection_timeout, strlen(properties_collection_timeout), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_PROPERTY_SERVICE_SECTION_NAME_STR, MA_PROPERTY_KEY_COLLECTION_TIMEOUT_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }

    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(property_collect_session_timeout, strlen(property_collect_session_timeout), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_PROPERTY_SERVICE_SECTION_NAME_STR, MA_PROPERTY_KEY_COLLECT_SESSION_TIMEOUT_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }    
      
    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(property_collect_full_props, strlen(property_collect_full_props), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_PROPERTY_SERVICE_SECTION_NAME_STR, MA_PROPERTY_KEY_COLLECT_FULL_PROPS_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }
    
	if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(property_collect_if_delay_by_days, strlen(property_collect_if_delay_by_days), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_PROPERTY_SERVICE_SECTION_NAME_STR, MA_PROPERTY_KEY_COLLECT_IF_DELAYS_BY_DAYS_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }
    return rc ;   
}


static ma_error_t add_agent_general_policies(ma_configurator_ex_t *self, ma_policy_uri_t *general_uri, ma_policy_bag_t *policy_bag) {
    const char *show_ui = "0", *show_reboot_ui= "0" , *reboot_timeout = "-1", *allow_update = "0";   
    const char *extn_ver = "5.0.0", *forceInstall = "0", *isSefProtecitonenabled = "1", *reduceProcessPriority = "1";
    ma_error_t rc = MA_OK;     
    ma_variant_t *value = NULL;

	/*ShowAgentUi policy should be enabled by default.*/
	show_ui = "1";
	/*AllowUpdate policy should be enabled by default in unmanaged mode and disabled by default in managed mode.*/
	allow_update = (0 == self->agent_configuration.agent_mode)? "1":"0";

	if(MA_OK == (rc = ma_variant_create_from_string(show_ui, strlen(show_ui), &value))) {
		rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_AGENT_GENERAL_SECTION_NAME_STR, MA_AGENT_KEY_SHOW_AGENT_UI_INT, value); 
		(void) ma_variant_release(value); value = NULL;
	}

	if(MA_OK == (rc = ma_variant_create_from_string(allow_update, strlen(allow_update), &value))) {
		rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_AGENT_GENERAL_SECTION_NAME_STR, MA_AGENT_KEY_ALLOW_UPDATE_SECURITY_INT, value); 
		(void) ma_variant_release(value); value = NULL;
	}

    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(show_reboot_ui, strlen(show_reboot_ui), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_AGENT_GENERAL_SECTION_NAME_STR, MA_AGENT_KEY_SHOW_REBOOT_UI_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }

    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(reboot_timeout, strlen(reboot_timeout), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_AGENT_GENERAL_SECTION_NAME_STR, MA_AGENT_KEY_REBOOT_TIMEOUT_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }

    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(extn_ver, strlen(extn_ver), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_AGENT_GENERAL_SECTION_NAME_STR, MA_AGENT_EXENSION_VERSION_STR, value); 
        (void) ma_variant_release(value); value = NULL;
    }

    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(forceInstall, strlen(forceInstall), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_AGENT_GENERAL_SECTION_NAME_STR, MA_AGENT_FORCE_INSTALL_STR, value); 
        (void) ma_variant_release(value); value = NULL;
    }

    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(isSefProtecitonenabled, strlen(isSefProtecitonenabled), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_AGENT_GENERAL_SECTION_NAME_STR, MA_AGENT_SELF_PROTECTION_ENABLED_STR, value); 
        (void) ma_variant_release(value); value = NULL;
    }

    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(reduceProcessPriority, strlen(reduceProcessPriority), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_AGENT_GENERAL_SECTION_NAME_STR, MA_AGENT_REDUCE_PROCESS_PRIORITY_STR, value); 
        (void) ma_variant_release(value); value = NULL;
    }

    return rc;    
}

static ma_error_t add_policy_service_policies(ma_configurator_ex_t *self, ma_policy_uri_t *general_uri, ma_policy_bag_t *policy_bag) {
    const char *policy_enforcement_timeout = "60"; /* 60 minutes */
    const char *is_installed = "1", *is_enabled = "1" ; /* Policy service can be run in unmanaged mode to server local policies */
    ma_error_t rc = MA_OK;     
    ma_variant_t *value = NULL;

    if(MA_OK == (rc = ma_variant_create_from_string(is_installed, strlen(is_installed), &value))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_POLICY_SERVICE_SECTION_NAME_STR, MA_POLICY_KEY_IS_INSTALLED_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }
    
    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(is_enabled, strlen(is_enabled), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_POLICY_SERVICE_SECTION_NAME_STR, MA_POLICY_KEY_IS_ENABLED_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }

    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(policy_enforcement_timeout, strlen(policy_enforcement_timeout), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_POLICY_SERVICE_SECTION_NAME_STR, MA_POLICY_KEY_ENFORCEMENT_TIMEOUT_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }

    return rc;    
}

static ma_error_t add_event_service_policies(ma_configurator_ex_t *self, ma_policy_uri_t *general_uri, ma_policy_bag_t *policy_bag) {
    const char *event_upload_timeout = "5"; /* 5 minutes */   
    const char *is_enabled_priority_forward = "1";
    const char *event_upload_threshold = "10"; /* 10 events per upload */
    const char *event_priority_level = "3" ; 
    const char *event_filter_version="0";
    const char *event_filtered_list = "0";
    const char *is_installed = "1", *is_enabled = (MA_CRYPTO_AGENT_MANAGED == self->agent_configuration.agent_mode) ? "1" : "0" ;
    ma_error_t rc = MA_OK;     
    ma_variant_t *value = NULL;

    if(MA_OK == (rc = ma_variant_create_from_string(is_installed, strlen(is_installed), &value))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_EVENT_SERVICE_SECTION_NAME_STR, MA_EVENT_KEY_IS_INSTALLED_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }
    
    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(is_enabled, strlen(is_enabled), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_EVENT_SERVICE_SECTION_NAME_STR, MA_EVENT_KEY_IS_ENABLED_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }

    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(event_upload_timeout, strlen(event_upload_timeout), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_EVENT_SERVICE_SECTION_NAME_STR, MA_EVENT_KEY_UPLOAD_TIMEOUT_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }

    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(event_filter_version, strlen(event_filter_version), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_EVENT_SERVICE_SECTION_NAME_STR, MA_EVENT_KEY_FILTER_VERSION_STR, value); 
        (void) ma_variant_release(value); value = NULL;
    }

    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(event_filtered_list, strlen(event_filtered_list), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_EVENT_SERVICE_SECTION_NAME_STR, MA_EVENT_KEY_FILTERED_LIST_STR, value); 
        (void) ma_variant_release(value); value = NULL;
    }

    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(is_enabled_priority_forward, strlen(is_enabled_priority_forward), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_EVENT_SERVICE_SECTION_NAME_STR, MA_EVENT_KEY_IS_ENABLED_PRIORITY_FORWARD_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }

    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(event_upload_threshold, strlen(event_upload_threshold), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_EVENT_SERVICE_SECTION_NAME_STR, MA_EVENT_KEY_UPLOAD_THRESHOLD_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }

    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(event_priority_level, strlen(event_priority_level), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_EVENT_SERVICE_SECTION_NAME_STR, MA_EVENT_KEY_PRIORITY_LEVEL_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }
    
    return rc;    
}

static ma_error_t add_scheduler_service_policies(ma_configurator_ex_t *self, ma_policy_uri_t *general_uri, ma_policy_bag_t *policy_bag) {    
    const char *is_installed = "1", *is_enabled =  "1";
    ma_error_t rc = MA_OK;     
    ma_variant_t *value = NULL;

    if(MA_OK == (rc = ma_variant_create_from_string(is_installed, strlen(is_installed), &value))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_SCHEDULER_SERVICE_SECTION_NAME_STR, MA_SCHEDULER_KEY_IS_INSTALLED_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }
    
    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(is_enabled, strlen(is_enabled), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_SCHEDULER_SERVICE_SECTION_NAME_STR, MA_SCHEDULER_KEY_IS_ENABLED_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }
    return rc;
}

static ma_error_t add_logger_service_policies(ma_configurator_ex_t *self, ma_policy_uri_t *general_uri, ma_policy_bag_t *policy_bag) {
    const char *format_pattern = "%d %+t %p(%P.%T) %f.%s: %m";
    const char *filter_pattern = "*.Info"; 
    const char *log_size_limit = "2"; /* 2 MB */
    const char *rollover_enabled = "1";
    const char *rollover_limit = "1"; /* 1 rollovers allowed */
    const char *is_installed = "1", *is_enabled = "1"; /* it can run in unmanaged mode also */
	const char *is_app_log_enabled = "1"; /* default app log on */
    const char *is_remote_log_enabled = "0"; /* default remote log view disabled */
	const char *agent_lang_selection_enabled = "0";
	const char *agent_lang = "0000";
    const char *is_log_recording_enabled = "0", *log_records_size = "200";   

    ma_error_t rc = MA_OK;
    ma_variant_t *value = NULL;

     if(MA_OK == (rc = ma_variant_create_from_string(is_installed, strlen(is_installed), &value))) {
         rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_LOGGER_SERVICE_SECTION_NAME_STR, MA_LOGGER_KEY_IS_INSTALLED_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }
    
    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(is_enabled, strlen(is_enabled), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_LOGGER_SERVICE_SECTION_NAME_STR, MA_LOGGER_KEY_IS_ENABLED_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }  

    if((MA_OK == rc) &&MA_OK == (rc = ma_variant_create_from_string(format_pattern, strlen(format_pattern), &value))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_LOGGER_SERVICE_SECTION_NAME_STR, MA_LOGGER_KEY_FORMAT_PATTERN_STR, value); 
        (void) ma_variant_release(value); value = NULL;
    }
    
    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(filter_pattern, strlen(filter_pattern), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_LOGGER_SERVICE_SECTION_NAME_STR, MA_LOGGER_KEY_FILTER_PATTERN_STR, value); 
        (void) ma_variant_release(value); value = NULL;
    }

    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(log_size_limit, strlen(log_size_limit), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_LOGGER_SERVICE_SECTION_NAME_STR, MA_LOGGER_KEY_LOG_SIZE_LIMIT_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }

    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(rollover_enabled, strlen(rollover_enabled), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_LOGGER_SERVICE_SECTION_NAME_STR, MA_LOGGER_KEY_LOG_ENABLE_ROLLOVER_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }

    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(rollover_limit, strlen(rollover_limit), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_LOGGER_SERVICE_SECTION_NAME_STR, MA_LOGGER_KEY_LOG_MAX_ROLLOVER_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }
    
    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(is_app_log_enabled, strlen(is_app_log_enabled), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_LOGGER_SERVICE_SECTION_NAME_STR, MA_LOGGER_KEY_LOG_APP_LOGGING_ENABLED_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }

	if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(is_remote_log_enabled, strlen(is_remote_log_enabled), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_LOGGER_SERVICE_SECTION_NAME_STR, MA_LOGGER_KEY_IS_REMOTE_LOG_ENABLED_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }

	if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(is_log_recording_enabled, strlen(is_log_recording_enabled), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_LOGGER_SERVICE_SECTION_NAME_STR, MA_LOGGER_KEY_IS_LOG_RECORDING_ENABLED_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }	

    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(log_records_size, strlen(log_records_size), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_LOGGER_SERVICE_SECTION_NAME_STR, MA_LOGGER_KEY_LOG_RECORDS_SIZE_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }

    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(agent_lang_selection_enabled, strlen(agent_lang_selection_enabled), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_AGENT_LANGUAGE_OPTIONS_SECTION_NAME_STR, MA_AGENT_ENABLE_LANG_SELECTION_STR, value); 
        (void) ma_variant_release(value); value = NULL;
    }

	if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(agent_lang, strlen(agent_lang), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_AGENT_LANGUAGE_OPTIONS_SECTION_NAME_STR, MA_AGENT_LANGUAGE_SELECTION_STR, value); 
        (void) ma_variant_release(value); value = NULL;
    }

    return rc;
}

static ma_error_t add_crypto_service_policies(ma_configurator_ex_t *self, ma_policy_uri_t *general_uri, ma_policy_bag_t *policy_bag) {    
    const char *is_installed = "1", *is_enabled = "1";
    ma_error_t rc = MA_OK;     
    ma_variant_t *value = NULL;

    if(MA_OK == (rc = ma_variant_create_from_string(is_installed, strlen(is_installed), &value))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_CRYPTO_SERVICE_SECTION_NAME_STR, MA_CRYPTO_KEY_IS_INSTALLED_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }
    
    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(is_enabled, strlen(is_enabled), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_CRYPTO_SERVICE_SECTION_NAME_STR, MA_CRYPTO_KEY_IS_ENABLED_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }  

    return rc;
}

static ma_error_t add_updater_service_policies(ma_configurator_ex_t *self, ma_policy_uri_t *general_uri, ma_policy_bag_t *policy_bag) {
     ma_error_t rc = MA_OK;
    const char *is_installed = "1";   
#if !defined(__WIN64__) && defined(MA_WINDOWS)
    const char *is_enabled = "1"; /* it can run in unmanaged mode also */ 
#else
    const char *is_enabled = "1" ;
#endif
    ma_variant_t *value = NULL;

    if(MA_OK == (rc = ma_variant_create_from_string(is_installed, strlen(is_installed), &value))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_UPDATER_SERVICE_SECTION_NAME_STR , MA_UPDATER_KEY_IS_INSTALLED_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }

    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(is_enabled, strlen(is_enabled), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_UPDATER_SERVICE_SECTION_NAME_STR, MA_UPDATER_KEY_IS_ENABLED_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }

	if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string("0", strlen("0"), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_UPDATER_SERVICE_SECTION_NAME_STR, MA_UPDATER_KEY_ENABLE_REBOOT_UI_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }

	if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string("-1", strlen("-1"), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_UPDATER_SERVICE_SECTION_NAME_STR, MA_UPDATER_KEY_REBOOT_TIMEOUT_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }

	if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string("0", strlen("0"), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_UPDATER_SERVICE_SECTION_NAME_STR, MA_UPDATER_KEY_ENABLE_DAT_DOWNGRADE_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }

	if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string("0", strlen("0"), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_UPDATER_SERVICE_SECTION_NAME_STR, MA_UPDATER_KEY_ENABLE_RUN_EXE_AFTER_UPDATE_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }
    return rc;
}


static ma_error_t add_ah_service_policies(ma_configurator_ex_t *self, ma_policy_uri_t *general_uri, ma_policy_bag_t *policy_bag) {
    ma_error_t rc = MA_OK;
    const char *is_installed = "1", *is_enabled = (MA_CRYPTO_AGENT_MANAGED == self->agent_configuration.agent_mode) ? "1" : "0" ;      
    ma_variant_t *value = NULL;
      
    if(MA_OK == (rc = ma_variant_create_from_string(is_installed, strlen(is_installed), &value))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri,MA_AH_SERVICE_SECTION_NAME_STR, MA_AH_KEY_IS_INSTALLED_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }

    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(is_enabled, strlen(is_enabled), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_AH_SERVICE_SECTION_NAME_STR, MA_AH_KEY_IS_ENABLED_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }

    return rc;
}

static ma_error_t add_compat_service_policies(ma_configurator_ex_t *self, ma_policy_uri_t *general_uri, ma_policy_bag_t *policy_bag) {
    ma_error_t rc = MA_OK;
    const char *is_installed = "1";   
    const char *is_enabled = "1";
    ma_variant_t *value = NULL;

    if(MA_OK == (rc = ma_variant_create_from_string(is_installed, strlen(is_installed), &value))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_COMPAT_SERVICE_SECTION_NAME_STR, MA_COMPAT_KEY_IS_INSTALLED_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }

    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(is_enabled, strlen(is_enabled), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_COMPAT_SERVICE_SECTION_NAME_STR, MA_COMPAT_KEY_IS_ENABLED_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }
    return rc;
}

static ma_error_t add_sensor_service_policies(ma_configurator_ex_t *self, ma_policy_uri_t *general_uri, ma_policy_bag_t *policy_bag) {
    ma_error_t rc = MA_OK;
    const char *is_installed = "1", *is_enabled = "1";    
    ma_variant_t *value = NULL;

    if(MA_OK == (rc = ma_variant_create_from_string(is_installed, strlen(is_installed), &value))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_SENSOR_SERVICE_SECTION_NAME_STR, MA_SENSOR_KEY_IS_INSTALLED_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }

    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(is_enabled, strlen(is_enabled), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_SENSOR_SERVICE_SECTION_NAME_STR, MA_SENSOR_KEY_IS_ENABLED_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }
    return rc;
}

static ma_error_t add_datachannel_service_policies(ma_configurator_ex_t *self, ma_policy_uri_t *general_uri, ma_policy_bag_t *policy_bag) {
    ma_error_t rc = MA_OK;
    const char *is_installed = "1", *is_enabled = (MA_CRYPTO_AGENT_MANAGED == self->agent_configuration.agent_mode) ? "1" : "0" ;       
    ma_variant_t *value = NULL;

    if(MA_OK == (rc = ma_variant_create_from_string(is_installed, strlen(is_installed), &value))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_DATACHANNEL_SERVICE_SECTION_NAME_STR, MA_DATACHANNEL_KEY_IS_INSTALLED_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }

    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(is_enabled, strlen(is_enabled), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_DATACHANNEL_SERVICE_SECTION_NAME_STR, MA_DATACHANNEL_KEY_IS_ENABLED_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }
    return rc;
}


static ma_error_t add_repository_service_policies(ma_configurator_ex_t *self, ma_policy_uri_t *general_uri, ma_policy_uri_t *repo_uri, ma_policy_bag_t *policy_bag) {
    ma_error_t rc = MA_OK;
    const char *is_installed = "1", *is_enabled = "1"; /* it can run in unmanaged mode also */      
    const char *rank_type = "0", *hop_limit = "15", *ping_timeout = "30", *overwrite_client_list = "0";
    const char *proxy_usage = "1", *allow_user_to_configure_proxy = "0", *by_pass_local_address = "0"; 
	const char *license_key ="";
    ma_variant_t *value = NULL ;
    
    if(MA_OK == (rc = ma_variant_create_from_string(is_installed, strlen(is_installed), &value))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_REPOSITORY_SERVICE_SECTION_NAME_STR, MA_REPOSITORY_KEY_IS_INSTALLED_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }

    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(is_enabled, strlen(is_enabled), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_REPOSITORY_SERVICE_SECTION_NAME_STR, MA_REPOSITORY_KEY_IS_ENABLED_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }

    if(MA_OK == (rc = ma_variant_create_from_string(rank_type, strlen(rank_type), &value))) {
        rc = ma_policy_bag_set_value(policy_bag, repo_uri, MA_REPOSITORY_ADVANCED_SECTION_NAME_STR, MA_REPOSITORY_KEY_RANK_TYPE_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }
    
    if(MA_OK == (rc = ma_variant_create_from_string(hop_limit, strlen(hop_limit), &value))) {
        rc = ma_policy_bag_set_value(policy_bag, repo_uri, MA_REPOSITORY_ADVANCED_SECTION_NAME_STR, MA_REPOSITORY_KEY_HOP_LIMIT_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }
    
    if(MA_OK == (rc = ma_variant_create_from_string(ping_timeout, strlen(ping_timeout), &value))) {
        rc = ma_policy_bag_set_value(policy_bag, repo_uri, MA_REPOSITORY_ADVANCED_SECTION_NAME_STR, MA_REPOSITORY_KEY_PING_TIMEOUT_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }
    
    if(MA_OK == (rc = ma_variant_create_from_string(overwrite_client_list, strlen(overwrite_client_list), &value))) {
        rc = ma_policy_bag_set_value(policy_bag, repo_uri, MA_REPOSITORY_ADVANCED_SECTION_NAME_STR, MA_REPOSITORY_KEY_OVERWRITE_CLIENT_REPOS_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }
    
    if(MA_OK == (rc = ma_variant_create_from_string(proxy_usage, strlen(proxy_usage), &value))) {
        rc = ma_policy_bag_set_value(policy_bag, repo_uri, MA_REPOSITORY_PROXY_SECTION_NAME_STR, MA_REPOSITORY_PROXY_KEY_PROXY_USAGE_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }
    
    if(MA_OK == (rc = ma_variant_create_from_string(allow_user_to_configure_proxy, strlen(allow_user_to_configure_proxy), &value))) {
        rc = ma_policy_bag_set_value(policy_bag, repo_uri, MA_REPOSITORY_PROXY_SECTION_NAME_STR, MA_REPOSITORY_PROXY_KEY_ENABLE_USER_CONFIG_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }
    
    if(MA_OK == (rc = ma_variant_create_from_string(by_pass_local_address, strlen(by_pass_local_address), &value))) {
        rc = ma_policy_bag_set_value(policy_bag, repo_uri, MA_REPOSITORY_PROXY_SECTION_NAME_STR, MA_REPOSITORY_PROXY_KEY_ENABLE_BYPASS_LOCAL_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }

	if(MA_OK == (rc = ma_variant_create_from_string(license_key, strlen(license_key), &value))) {
        rc = ma_policy_bag_set_value(policy_bag, repo_uri, MA_REPOSITORY_LICENSE_SECTION_NAME_STR, MA_REPOSITORY_KEY_LICENSE_STR, value); 
        (void) ma_variant_release(value); value = NULL;
    }
    
    return rc;
}

static ma_error_t add_http_server_service_policies(ma_configurator_ex_t *self, ma_policy_uri_t *general_uri, ma_policy_bag_t *policy_bag) {
    ma_error_t rc = MA_OK;
    const char *is_installed = "1", *is_enabled = (MA_CRYPTO_AGENT_MANAGED == self->agent_configuration.agent_mode) ? "1" : "0" ;      
    /* TBD - default policies of HTTP server to  be reviewed */
    const char *port = MA_HTTP_SERVER_DEFAULT_TCP_PORT_STR, *is_agent_ping_enabled = "1", *is_lc_enabled = "0"; 
    const char *is_sa_enabled = "0", *is_sa_repo_enabled = "0", *is_listen_epo_server_only = "0", *repo_sync_interval = "30", *virtual_directory = "DEFAULT", *disk_quota = "1", *purge_interval = "1" ;
	const char *concurrent_connections_limit = "1024" ;
    ma_variant_t *value = NULL;

    if(MA_OK == (rc = ma_variant_create_from_string(is_installed, strlen(is_installed), &value))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_HTTP_SERVER_SERVICE_SECTION_NAME_STR, MA_HTTP_SERVER_KEY_IS_INSTALLED_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }

    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(is_enabled, strlen(is_enabled), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_HTTP_SERVER_SERVICE_SECTION_NAME_STR, MA_HTTP_SERVER_KEY_IS_ENABLED_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }

    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(port, strlen(port), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_HTTP_SERVER_SERVICE_SECTION_NAME_STR, MA_HTTP_SERVER_KEY_PORT_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }
    
    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(is_agent_ping_enabled, strlen(is_agent_ping_enabled), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_HTTP_SERVER_SERVICE_SECTION_NAME_STR, MA_HTTP_SERVER_KEY_ENABLE_AGENT_PING_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }
    
    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(is_lc_enabled, strlen(is_lc_enabled), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_HTTP_SERVER_SERVICE_SECTION_NAME_STR, MA_HTTP_SERVER_KEY_ENABLE_LAZY_CACHING_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }
    
    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(is_sa_enabled, strlen(is_sa_enabled), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_HTTP_SERVER_SERVICE_SECTION_NAME_STR, MA_HTTP_SERVER_KEY_ENABLE_SUPER_AGENT_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }
    
    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(is_sa_repo_enabled, strlen(is_sa_repo_enabled), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_HTTP_SERVER_SERVICE_SECTION_NAME_STR, MA_HTTP_SERVER_KEY_ENABLE_SUPER_AGENT_REPOSITORY_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }
    
    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(is_listen_epo_server_only, strlen(is_listen_epo_server_only), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_HTTP_SERVER_SERVICE_SECTION_NAME_STR, MA_HTTP_SERVER_KEY_LISTEN_TO_EPO_SERVER_ONLY_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }
    
    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(repo_sync_interval, strlen(repo_sync_interval), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_HTTP_SERVER_SERVICE_SECTION_NAME_STR, MA_HTTP_SERVER_KEY_REPOSITORY_SYNC_INTERVAL_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }
    
    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(virtual_directory, strlen(virtual_directory), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_HTTP_SERVER_SERVICE_SECTION_NAME_STR, MA_HTTP_SERVER_KEY_VIRTUAL_DIRECTORY_STR, value); 
        (void) ma_variant_release(value); value = NULL;
    }

	if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(disk_quota, strlen(disk_quota), &value)))) {
		rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_HTTP_SERVER_SERVICE_SECTION_NAME_STR, MA_HTTP_SERVER_KEY_REPOSITORY_DISK_QUOTA_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }

	if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(purge_interval, strlen(purge_interval), &value)))) {
		rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_HTTP_SERVER_SERVICE_SECTION_NAME_STR, MA_HTTP_SERVER_KEY_REPOSITORY_PURGE_INTERVAL_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }
	
	if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(concurrent_connections_limit, strlen(concurrent_connections_limit), &value)))) {
		rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_HTTP_SERVER_SERVICE_SECTION_NAME_STR, MA_HTTP_SERVER_KEY_SAREPO_CONCURRENT_CONNECTIONS_LIMIT_INT, value) ; 
        (void) ma_variant_release(value); value = NULL ;
    }

    return rc;
}

static ma_error_t add_p2p_service_policies(ma_configurator_ex_t *self, ma_policy_uri_t *general_uri, ma_policy_bag_t *policy_bag) {
    ma_error_t rc = MA_OK ;
    const char *is_installed = "1", *is_enabled = (MA_CRYPTO_AGENT_MANAGED == self->agent_configuration.agent_mode) ? "1" : "0" ;
	const char *serving_enabled = (MA_CRYPTO_AGENT_MANAGED == self->agent_configuration.agent_mode) ? "1" : "0" ;
	const char *client_enabled = (MA_CRYPTO_AGENT_MANAGED == self->agent_configuration.agent_mode) ? "1" : "0" ;
	const char *disk_quota = "512", *purge_interval = "1", *repo_path = "DEFAULT", *concurrent_connections_limit = "10" ;
    ma_variant_t *value = NULL ;

    if(MA_OK == (rc = ma_variant_create_from_string(is_installed, strlen(is_installed), &value))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_P2P_SERVICE_SECTION_NAME_STR, MA_P2P_KEY_IS_INSTALLED_INT, value) ; 
        (void) ma_variant_release(value); value = NULL ;
    }

    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(is_enabled, strlen(is_enabled), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_P2P_SERVICE_SECTION_NAME_STR, MA_P2P_KEY_IS_ENABLED_INT, value) ; 
        (void) ma_variant_release(value); value = NULL ;
    }

	if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(serving_enabled, strlen(serving_enabled), &value)))) {
		rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_P2P_SERVICE_SECTION_NAME_STR, MA_P2P_KEY_ENABLE_SERVING_INT, value) ; 
        (void) ma_variant_release(value); value = NULL ;
    }

	if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(client_enabled, strlen(client_enabled), &value)))) {
		rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_P2P_SERVICE_SECTION_NAME_STR, MA_P2P_KEY_ENABLE_CLIENT_INT, value) ; 
        (void) ma_variant_release(value); value = NULL ;
    }

	if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(disk_quota, strlen(disk_quota), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_P2P_SERVICE_SECTION_NAME_STR, MA_P2P_KEY_DISK_QUOTA_INT, value) ; 
        (void) ma_variant_release(value); value = NULL ;
    }
	
	if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(purge_interval, strlen(purge_interval), &value)))) {
		rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_P2P_SERVICE_SECTION_NAME_STR, MA_P2P_KEY_LONGEVITY_INT, value) ; 
        (void) ma_variant_release(value); value = NULL ;
    }

	if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(repo_path, strlen(repo_path), &value)))) {
		rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_P2P_SERVICE_SECTION_NAME_STR, MA_P2P_KEY_REPO_PATH_STR, value) ; 
        (void) ma_variant_release(value); value = NULL ;
    }

	if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(concurrent_connections_limit, strlen(concurrent_connections_limit), &value)))) {
		rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_P2P_SERVICE_SECTION_NAME_STR, MA_P2P_KEY_CONCURRENT_CONNECTIONS_LIMIT_INT, value) ; 
        (void) ma_variant_release(value); value = NULL ;
    }

    return rc ;
}

static ma_error_t add_udp_service_policies(ma_configurator_ex_t *self, ma_policy_uri_t *general_uri, ma_policy_bag_t *policy_bag) {
    ma_error_t rc = MA_OK ;
    const char *is_installed = "1", *is_enabled = (MA_CRYPTO_AGENT_MANAGED == self->agent_configuration.agent_mode) ? "1" : "0";
	const char *udp_port = MA_UDP_DEFAULT_PORT_STR, *is_broadcast_ping_enabled = "1", *multicast_address = MA_UDP_MULTICAST_ADDR_STR ;  
    ma_variant_t *value = NULL ;

    if(MA_OK == (rc = ma_variant_create_from_string(is_installed, strlen(is_installed), &value))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_UDP_SERVICE_SECTION_NAME_STR, MA_UDP_KEY_IS_INSTALLED_INT, value) ; 
        (void) ma_variant_release(value); value = NULL ;
    }

    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(is_enabled, strlen(is_enabled), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_UDP_SERVICE_SECTION_NAME_STR, MA_UDP_KEY_IS_ENABLED_INT, value) ; 
        (void) ma_variant_release(value); value = NULL ;
    }

	if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(multicast_address, strlen(multicast_address), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_UDP_SERVICE_SECTION_NAME_STR, MA_UDP_KEY_MULTICAST_ADDRESS_STR, value); 
        (void) ma_variant_release(value); value = NULL;
    }

	if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(udp_port, strlen(udp_port), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_UDP_SERVICE_SECTION_NAME_STR, MA_UDP_KEY_BROADCAST_PORT_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }
      
    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(is_broadcast_ping_enabled, strlen(is_broadcast_ping_enabled), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_UDP_SERVICE_SECTION_NAME_STR, MA_UDP_KEY_ENABLE_BROADCAST_PING_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }

    return rc ;
}

static ma_error_t add_relay_service_policies(ma_configurator_ex_t *self, ma_policy_uri_t *general_uri, ma_policy_bag_t *policy_bag) {
    ma_error_t rc = MA_OK ;
    const char *is_installed = "1", *is_enabled = "0", *client_enabled = (MA_CRYPTO_AGENT_MANAGED == self->agent_configuration.agent_mode) ? "1" : "0", *relay_discovery_port = "8083", *multicast_address = MA_UDP_MULTICAST_ADDR_STR;
	const char *concurrent_connections_limit = "1024" ;
    ma_variant_t *value = NULL ;

    if(MA_OK == (rc = ma_variant_create_from_string(is_installed, strlen(is_installed), &value))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_RELAY_SERVICE_SECTION_NAME_STR, MA_RELAY_KEY_IS_INSTALLED_INT, value) ; 
        (void) ma_variant_release(value); value = NULL ;
    }

    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(is_enabled, strlen(is_enabled), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_RELAY_SERVICE_SECTION_NAME_STR, MA_RELAY_KEY_IS_ENABLED_INT, value) ; 
        (void) ma_variant_release(value); value = NULL ;
    }
	
	if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(client_enabled, strlen(client_enabled), &value)))) {
		rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_RELAY_SERVICE_SECTION_NAME_STR, MA_RELAY_KEY_ENABLE_CLIENT_INT, value) ; 
        (void) ma_variant_release(value); value = NULL ;
    }

	if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(multicast_address, strlen(multicast_address), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_RELAY_SERVICE_SECTION_NAME_STR, MA_RELAY_KEY_MULTICAST_ADDRESS_STR, value); 
        (void) ma_variant_release(value); value = NULL;
    }

	if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(relay_discovery_port, strlen(relay_discovery_port), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_RELAY_SERVICE_SECTION_NAME_STR, MA_RELAY_KEY_SERVER_PORT_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }

	if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(concurrent_connections_limit, strlen(concurrent_connections_limit), &value)))) {
		rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_RELAY_SERVICE_SECTION_NAME_STR, MA_RELAY_KEY_CONCURRENT_CONNECTIONS_LIMIT_INT, value) ; 
        (void) ma_variant_release(value); value = NULL ;
    }

    return rc ;
}


static ma_error_t add_stats_service_policies(ma_configurator_ex_t *self, ma_policy_uri_t *general_uri, ma_policy_bag_t *policy_bag) {
    ma_error_t rc = MA_OK ;
    const char *is_installed = "1", *is_enabled = (MA_CRYPTO_AGENT_MANAGED == self->agent_configuration.agent_mode) ? "1" : "0" ;    
    ma_variant_t *value = NULL ;

    if(MA_OK == (rc = ma_variant_create_from_string(is_installed, strlen(is_installed), &value))) {
		rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_STATS_SERVICE_SECTION_NAME_STR, MA_STATS_KEY_IS_INSTALLED_INT, value) ; 
        (void) ma_variant_release(value); value = NULL ;
    }

    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(is_enabled, strlen(is_enabled), &value)))) {
		rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_STATS_SERVICE_SECTION_NAME_STR, MA_STATS_KEY_IS_ENABLED_INT, value) ; 
        (void) ma_variant_release(value); value = NULL ;
    }

    return rc ;
}

ma_error_t ma_import_policies(ma_db_t *policy_db, ma_policy_uri_t *general_uri, ma_policy_bag_t *policy_bag) {
    ma_error_t rc = MA_OK;
    ma_table_t *prop_table = NULL;
    ma_variant_t *policy_data = NULL;
    if(MA_OK == (rc = ma_policy_uri_get_table(general_uri, &prop_table))) {
        if(MA_OK == (rc = ma_policy_bag_get_variant(policy_bag, &policy_data))) {
            rc = ma_policy_service_set_policies(prop_table, policy_db, policy_data);
            (void) ma_variant_release(policy_data);
        }
        (void) ma_table_release(prop_table);
    }
    return rc;
}

static ma_error_t configure_agent_policies(ma_configurator_t *configurator) {
    ma_error_t rc = MA_OK;
    ma_policy_bag_t *policy_bag = NULL;
    ma_policy_uri_t *agent_uri = NULL, *general_uri = NULL, *repo_uri = NULL ;
	ma_configurator_ex_t *self = (ma_configurator_ex_t *)configurator;
    
    /*TBD - handle provisioning , change ePO , unmanged mode in better way, best will be to fix it in ma config*/
    /* checking whether policies are already avaialble or not */
    if(MA_OK == (rc = ma_policy_uri_create(&agent_uri))) {
        ma_policy_uri_set_software_id(agent_uri, MA_SOFTWAREID_GENERAL_STR);
        if((MA_OK == get_agent_policies(agent_uri, self->db_policy, &policy_bag)) && policy_bag) {
            (void) ma_policy_bag_release(policy_bag); policy_bag = NULL;            
            /*If agent configuration is not changed and we found the old policies then continue, else continue changing it as per new mode */
            if(MA_FALSE == self->agent_configuration.mode_changed) {
                (void) ma_policy_uri_release(agent_uri); agent_uri = NULL;
                return MA_OK;
            }
        }        
    }

    if((MA_OK == rc) && (MA_OK == (rc = ma_policy_bag_create(&policy_bag)))) {
        if(MA_OK == (rc = ma_policy_uri_create(&general_uri)) && 
           MA_OK == (rc = ma_policy_uri_create(&repo_uri)) ) {
        
            /* For adding policies in General PSO */
            ma_policy_uri_set_feature(general_uri, MA_POLICY_FEATURE_ID); 
            ma_policy_uri_set_category(general_uri, MA_POLICY_CATEGORY_ID);    
            ma_policy_uri_set_type(general_uri, MA_POLICY_TYPE_ID);   
            ma_policy_uri_set_name(general_uri, MA_POLICY_NAME_ID);      

            /* for adding policies in Repository PSO */
            ma_policy_uri_set_feature(repo_uri, MA_POLICY_FEATURE_ID);
            ma_policy_uri_set_category(repo_uri, MA_REPO_POLICY_CATEGORY_ID);    
            ma_policy_uri_set_type(repo_uri, MA_REPO_POLICY_TYPE_ID);   
            ma_policy_uri_set_name(repo_uri, MA_POLICY_NAME_ID);      

            rc = ((MA_OK == (rc = add_agent_general_policies(self, general_uri, policy_bag)))
					&& (MA_OK == (rc = add_property_service_policies(self, general_uri, policy_bag)))
                    && (MA_OK == (rc = add_policy_service_policies(self, general_uri, policy_bag)))
                    && (MA_OK == (rc = add_event_service_policies(self, general_uri, policy_bag)))
                    && (MA_OK == (rc = add_scheduler_service_policies(self, general_uri, policy_bag)))
                    && (MA_OK == (rc = add_ah_service_policies(self, general_uri, policy_bag)))
                    && (MA_OK == (rc = add_logger_service_policies(self, general_uri, policy_bag)))
                    && (MA_OK == (rc = add_crypto_service_policies(self, general_uri, policy_bag)))
                    && (MA_OK == (rc = add_compat_service_policies(self, general_uri, policy_bag))) 
                    && (MA_OK == (rc = add_sensor_service_policies(self, general_uri, policy_bag))) 
                    && (MA_OK == (rc = add_datachannel_service_policies(self, general_uri, policy_bag))) 
                    && (MA_OK == (rc = add_repository_service_policies(self, general_uri, repo_uri, policy_bag)))
                    && (MA_OK == (rc = add_updater_service_policies(self, general_uri, policy_bag)))
                    && (MA_OK == (rc = add_http_server_service_policies(self, general_uri, policy_bag)))
					&& (MA_OK == (rc = add_p2p_service_policies(self, general_uri, policy_bag)))
					&& (MA_OK == (rc = add_udp_service_policies(self, general_uri, policy_bag)))	
					&& (MA_OK == (rc = add_relay_service_policies(self, general_uri, policy_bag)))	
					&& (MA_OK == (rc= add_stats_service_policies(self, general_uri, policy_bag)))	)
                    ? MA_OK : rc; 

            if(MA_OK == rc) {
                /* For Adding policy bag in policy DB */                          
                rc = ma_import_policies(self->db_policy, agent_uri, policy_bag);            
            }		
        }

		if(general_uri)
			(void) ma_policy_uri_release(general_uri);

		if(repo_uri)
			(void) ma_policy_uri_release(repo_uri);
		
        (void) ma_policy_bag_release(policy_bag);
    }

	if(agent_uri) (void)ma_policy_uri_release(agent_uri);

    return rc;
}

static ma_error_t configure_policy_database_schema(ma_configurator_ex_t *self) {
    ma_db_t *db_handle = self->db_policy;
    ma_db_statement_t *db_stmt = NULL;
    ma_error_t rc = MA_OK ;

    if(MA_OK == (rc = ma_db_transaction_begin(db_handle))) {
        if( MA_OK == (rc = ma_db_statement_create(db_handle, CREATE_MPO_TABLE_STMT, &db_stmt))) {
            rc = ma_db_statement_execute(db_stmt) ;
            ma_db_statement_release(db_stmt); db_stmt = NULL ;
            if( MA_OK == rc && MA_OK == (rc = ma_db_statement_create(db_handle, CREATE_MPSO_TABLE_STMT, &db_stmt)) ) {
                rc = ma_db_statement_execute(db_stmt) ;
                ma_db_statement_release(db_stmt); db_stmt = NULL ;
                if( MA_OK == rc && MA_OK == (rc = ma_db_statement_create(db_handle, CREATE_MPPA_TABLE_STMT, &db_stmt))) {
                    if(MA_OK == (rc = ma_db_statement_execute(db_stmt) ))
                        (void)ma_db_transaction_end(db_handle) ;
		            ma_db_statement_release(db_stmt); db_stmt = NULL ;                   
                }
            }
        }
        if(MA_OK != rc)
            ma_db_transaction_cancel(db_handle) ;
    }
    return rc ;
}


static ma_error_t configure_task_database_schema(ma_configurator_ex_t *self) {
    ma_db_t *db_handle = self->db_task;
    ma_db_statement_t *db_stmt = NULL;
    ma_error_t rc = MA_OK ;

    if(MA_OK == (rc = ma_db_transaction_begin(db_handle))) {
        if( MA_OK == rc && MA_OK == (rc = ma_db_statement_create(db_handle, CREATE_MET_TABLE_STMT, &db_stmt))) {
            rc = ma_db_statement_execute(db_stmt) ;
	        ma_db_statement_release(db_stmt); db_stmt = NULL ;
            if( MA_OK == rc && MA_OK == (rc = ma_db_statement_create(db_handle, CREATE_META_TABLE_STMT, &db_stmt))) {
                if(MA_OK == (rc = ma_db_statement_execute(db_stmt)))
                    (void)ma_db_transaction_end(db_handle) ;
                ma_db_statement_release(db_stmt); db_stmt = NULL ;
            }
        }    
        if(MA_OK != rc)
            ma_db_transaction_cancel(db_handle) ;
    }
    return rc ;
}

static ma_error_t configure_repository_database_schema(ma_configurator_ex_t *self) {
	return ma_repository_db_create_schema(self->db_default);
}

static ma_error_t configure_proxy_database_schema(ma_configurator_ex_t *self) {
	return ma_proxy_db_create_schema(self->db_default);
}

static ma_error_t configure_logger_database_schema(ma_configurator_ex_t *self) {
	return ma_logger_db_schema_create(self->db_default);
}

static ma_error_t ma_msgbus_db_create_schema(ma_configurator_ex_t *self){
    ma_db_statement_t *db_stmt = NULL;
    ma_error_t rc;
    ma_db_t *db_handle = self->db_msgbus;

     if(MA_OK == (rc = ma_db_transaction_begin(db_handle))) {
        if( MA_OK == rc && MA_OK == (rc = ma_db_statement_create(db_handle, CREATE_MSGBUS_SERVICES_TABLE_STMT, &db_stmt))) {
            rc = ma_db_statement_execute(db_stmt) ;
	        ma_db_statement_release(db_stmt); db_stmt = NULL ;
            if( MA_OK == rc && MA_OK == (rc = ma_db_statement_create(db_handle, CREATE_MSGBUS_SUBSCRIBERS_TABLE_STMT, &db_stmt))) {
                if(MA_OK == (rc = ma_db_statement_execute(db_stmt)))
                    (void)ma_db_transaction_end(db_handle) ;
                ma_db_statement_release(db_stmt); db_stmt = NULL ;
            }
        }    
        if(MA_OK != rc)
            ma_db_transaction_cancel(db_handle) ;
    }

    return rc ;
}

ma_error_t ma_configurator_configure_agent_policies(ma_configurator_t *configurator) {
    if(configurator) {
        ma_error_t rc = MA_OK;
		ma_configurator_ex_t *self = (ma_configurator_ex_t *)configurator;               

		if(MA_OK != (rc = configure_policy_database_schema(self)))
            return rc;

		if(MA_OK != (rc = configure_task_database_schema(self)))
            return rc;

		if(MA_OK != (rc = configure_repository_database_schema(self)))
            return rc;

		if(MA_OK != (rc = configure_proxy_database_schema(self)))
			return rc;

        if(MA_OK != (rc = configure_logger_database_schema(self)))
            return rc;

        if(MA_OK != (rc = ma_msgbus_db_create_schema(self)))
            return rc;

        if(MA_OK != (rc = add_agent_configuration_settings(self))) 
            return rc;

        return configure_agent_policies((ma_configurator_t *)self);
    }
    return MA_ERROR_INVALIDARG;
}


