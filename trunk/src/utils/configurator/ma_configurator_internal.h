#ifndef MA_CONFIGURATOR_INTERNAL_H_INCLUDED
#define MA_CONFIGURATOR_INTERNAL_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_datastore.h"
#include "ma/datastore/ma_ds_database.h"
#include "ma/datastore/ma_ds.h"
#include "ma/internal/utils/database/ma_db.h"
#include "ma/internal/utils/macrypto/ma_crypto.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/uuid/ma_uuid.h"
#include "ma/internal/utils/app_info/ma_registry_store.h"
#include "ma/internal/defs/ma_general_defs.h"

#include "ma/internal/utils/configurator/ma_configurator.h"

MA_CPP( extern "C" { )

typedef struct ma_configurator_ex_s ma_configurator_ex_t;
struct ma_configurator_ex_s {
    ma_configurator_t         base;
    ma_agent_configuration_t  agent_configuration;
    ma_db_t                   *db_default;
    ma_db_t                   *db_policy;
    ma_db_t                   *db_task;    
    ma_db_t                   *db_scheduler;
    ma_db_t                   *db_msgbus;
    ma_db_t                   *db_booking;
    ma_db_t                   *db_profile;
    ma_db_t                   *db_inventory;
    ma_db_t                   *db_recorder;
    ma_db_t                   *db_location;
    ma_db_t                   *db_season;
    ma_db_t                   *db_search;
    ma_db_t                   *db_board;
    ma_db_t                   *db_comment;
    ma_db_t                   *db_consignment;
    struct ma_ds_database_s   *ds_default;    
    struct ma_ds_database_s   *ds_scheduler;
    struct ma_ds_database_s   *ds_booking;
    struct ma_ds_database_s   *ds_profile;
    struct ma_ds_database_s   *ds_inventory;
    struct ma_ds_database_s   *ds_recorder;
    struct ma_ds_database_s   *ds_location;
    struct ma_ds_database_s   *ds_season;
    struct ma_ds_database_s   *ds_search;
    struct ma_ds_database_s   *ds_board;
    struct ma_ds_database_s   *ds_comment;
    struct ma_ds_database_s   *ds_consignment;
    char                      agent_install_path[MA_MAX_LEN];
    char                      agent_data_path[MA_MAX_LEN];
    char                      agent_lib_path[MA_MAX_LEN];
    char                      agent_tools_lib_path[MA_MAX_LEN]; 	
    void                      *data;
};


MA_CPP(})

#endif /* MA_CONFIGURATOR_H_INCLUDED */

