#include "ma/internal/utils/repository/ma_repository_db_manager.h"
#include "ma/repository/ma_repository_list.h"

#include "ma/internal/utils/database/ma_db_recordset.h"
#include "ma/internal/utils/database/ma_db_statement.h"
#include "ma/internal/utils/macrypto/ma_crypto.h"
#include "ma/repository/ma_repository_client_defs.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include "ma/internal/ma_macros.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#ifndef MA_WINDOWS
#include <limits.h>
#endif

//DDL'S
/* AGENT_REPOSITORIES - columns
NAME TEXT NOT NULL UNIQUE, 
REPO_TYPE INTEGER NOT NULL, 
URL_TYPE INTEGER NOT NULL,
NAMESPACE INTEGER NOT NULL, 
PROXY_USAGE INTEGER NOT NULL,
AUTH_TYPE INTEGER NOT NULL,
ENABLED INTEGER NOT NULL,
SERVER_FQDN TEXT, 
SERVER_IP TEXT,
SERVER_NAME TEXT,
PORT INTEGER,
SSL_PORT INTEGER,
PATH TEXT NOT NULL, 
DOMAIN TEXT, 
AUTH_USER TEXT, 
AUTH_PASSWD TEXT, 
IS_PASSWD_ENCRYPTED INTEGER NOT NULL, 
PING_TIME INTEGER NOT NULL, 
SUBNET_DISTANCE INTEGER NOT NULL, 
SITELIST_ORDER INTEGER NOT NULL,
STATE INTEGER NOT NULL,
PRIMARY KEY (NAME)
*/


//DML's
#define ADD_REPOSITORY_STMT     "INSERT INTO AGENT_REPOSITORIES VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"

#define REMOVE_ALL_REPOSITORIES_STMT        "DELETE FROM AGENT_REPOSITORIES"
#define REMOVE_REPOSITORY_STMT_FOR_NAME     "DELETE FROM AGENT_REPOSITORIES WHERE NAME = ?"

#define UPDATE_REPOSITORY_PING_COUNT_STMT       "UPDATE AGENT_REPOSITORIES SET STATE = 0, PING_TIME = ? WHERE NAME = ?"
#define UPDATE_REPOSITORY_SUBNET_DISTANCE_STMT  "UPDATE AGENT_REPOSITORIES SET STATE = 0, SUBNET_DISTANCE = ? WHERE NAME = ?"
#if 0
#define UPDATE_REPOSITORY_SITELIST_ORDER_STMT   "UPDATE AGENT_REPOSITORIES SET SITELIST_ORDER = ? WHERE NAME = ?"
#define UPDATE_REPOSITORY_ENABLED_STMT          "UPDATE AGENT_REPOSITORIES SET ENABLED = ? WHERE NAME = ?"
#endif
/*934493: In some cases, SPIPE site name in sitelist.xml & policy is different in character case.*/
/*Till ePO fix this issue, using work around*/
#define UPDATE_REPOSITORY_SITELIST_ORDER_STMT	"UPDATE AGENT_REPOSITORIES SET SITELIST_ORDER = ? WHERE (URL_TYPE != 4 AND NAME = ?) OR  (URL_TYPE = 4 AND NAME = ? COLLATE NOCASE)"
#define UPDATE_REPOSITORY_ENABLED_STMT          "UPDATE AGENT_REPOSITORIES SET ENABLED = ? WHERE (URL_TYPE != 4 AND NAME = ?) OR  (URL_TYPE = 4 AND NAME = ? COLLATE NOCASE)"

#define UPDATE_ALL_REPOSITORIES_ENABLED_STMT    "UPDATE AGENT_REPOSITORIES SET ENABLED = ?"



#define CREATE_REPOSITORIES_TABLE_STMT  "CREATE TABLE IF NOT EXISTS AGENT_REPOSITORIES(NAME TEXT NOT NULL UNIQUE, REPO_TYPE INTEGER NOT NULL, URL_TYPE INTEGER NOT NULL, NAMESPACE INTEGER NOT NULL, PROXY_USAGE INTEGER NOT NULL, AUTH_TYPE INTEGER NOT NULL, ENABLED INTEGER NOT NULL, SERVER_FQDN TEXT, SERVER_IP TEXT, SERVER_NAME TEXT,PORT INTEGER, SSL_PORT INTEGER,PATH TEXT, DOMAIN TEXT, AUTH_USER TEXT, AUTH_PASSWD TEXT, IS_PASSWD_ENCRYPTED INTEGER NOT NULL, PING_TIME INTEGER NOT NULL, SUBNET_DISTANCE INTEGER NOT NULL, SITELIST_ORDER INTEGER NOT NULL, STATE INTEGER NOT NULL, PRIMARY KEY (NAME) ON CONFLICT REPLACE)"
ma_error_t ma_repository_db_create_schema(ma_db_t *db_handle){
    ma_db_statement_t *db_stmt = NULL;
    ma_error_t rc;

    if( MA_OK == (rc = ma_db_statement_create(db_handle, CREATE_REPOSITORIES_TABLE_STMT, &db_stmt))) {
        rc = ma_db_statement_execute(db_stmt) ;
       (void) ma_db_statement_release(db_stmt); 
	}
    return rc ;
}

/*#define MA_REPO_TABLE_FIELD_BASE					  0
#define MA_REPO_TABLE_FILED_NAME					  MA_REPO_TABLE_FIELD_BASE + 1
#define MA_REPO_TABLE_FILED_REPO_TYPE				  MA_REPO_TABLE_FIELD_BASE + 2
#define MA_REPO_TABLE_FILED_URL_TYPE				  MA_REPO_TABLE_FIELD_BASE + 3
#define MA_REPO_TABLE_FILED_NAMESPACE				  MA_REPO_TABLE_FIELD_BASE + 4
#define MA_REPO_TABLE_FILED_PROXY_USAGE				  MA_REPO_TABLE_FIELD_BASE + 5
#define MA_REPO_TABLE_FILED_AUTH_TYPE				  MA_REPO_TABLE_FIELD_BASE + 6
#define MA_REPO_TABLE_FILED_ENABLED					  MA_REPO_TABLE_FIELD_BASE + 7
#define MA_REPO_TABLE_FILED_SERVER_FQDN				  MA_REPO_TABLE_FIELD_BASE + 8
#define MA_REPO_TABLE_FILED_SERVER_IP				  MA_REPO_TABLE_FIELD_BASE + 9
#define MA_REPO_TABLE_FILED_SERVER_NAME				  MA_REPO_TABLE_FIELD_BASE + 10
#define MA_REPO_TABLE_FILED_PORT					  MA_REPO_TABLE_FIELD_BASE + 11
#define MA_REPO_TABLE_FILED_SSL_PORT				  MA_REPO_TABLE_FIELD_BASE + 12
#define MA_REPO_TABLE_FILED_PATH					  MA_REPO_TABLE_FIELD_BASE + 13
#define MA_REPO_TABLE_FILED_DOMAIN					  MA_REPO_TABLE_FIELD_BASE + 14
#define MA_REPO_TABLE_FILED_AUTH_USER				  MA_REPO_TABLE_FIELD_BASE + 15
#define MA_REPO_TABLE_FILED_AUTH_PASSWD				  MA_REPO_TABLE_FIELD_BASE + 16
#define MA_REPO_TABLE_FILED_IS_PASSWD_ENCRYPTED		  MA_REPO_TABLE_FIELD_BASE + 17
#define MA_REPO_TABLE_FILED_PING_TIME				  MA_REPO_TABLE_FIELD_BASE + 18
#define MA_REPO_TABLE_FILED_SUBNET_DISTANCE			  MA_REPO_TABLE_FIELD_BASE + 19
#define MA_REPO_TABLE_FILED_SITELIST_ORDER			  MA_REPO_TABLE_FIELD_BASE + 20
#define MA_REPO_TABLE_FILED_STATE					  MA_REPO_TABLE_FIELD_BASE + 21*/

ma_error_t ma_repository_db_add_repository(ma_db_t *db_handle, ma_repository_t *repo){
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	if(db_handle && repo ){
		ma_db_statement_t *db_stmt = NULL ;
		if( MA_OK == (rc = ma_db_statement_create(db_handle, ADD_REPOSITORY_STMT, &db_stmt))) {
			const char *str_val = NULL;
			int int_val;
			ma_repository_type_t repo_type = MA_REPOSITORY_TYPE_REPO; 
			ma_repository_url_type_t url_type = MA_REPOSITORY_URL_TYPE_HTTP;
			if(MA_OK == (rc = ma_repository_get_name(repo, &str_val))){
				ma_db_statement_set_string(db_stmt, MA_REPO_TABLE_FILED_NAME, 1, str_val, strlen(str_val));
			}
			if(MA_OK == (rc = ma_repository_get_type(repo, (ma_repository_type_t*)&int_val))){
				ma_db_statement_set_int(db_stmt, MA_REPO_TABLE_FILED_REPO_TYPE, int_val);
				repo_type = (ma_repository_type_t)int_val;
			}
			if(MA_OK == (rc = ma_repository_get_url_type(repo, (ma_repository_url_type_t*)&int_val))){
				ma_db_statement_set_int(db_stmt, MA_REPO_TABLE_FILED_URL_TYPE, int_val);
				url_type = (ma_repository_url_type_t)int_val;
			}
			if(MA_OK == (rc = ma_repository_get_namespace(repo, (ma_repository_namespace_t*)&int_val))){
				ma_db_statement_set_int(db_stmt, MA_REPO_TABLE_FILED_NAMESPACE, int_val);
			}
			if(MA_OK == (rc = ma_repository_get_proxy_usage_type(repo, (ma_proxy_usage_type_t*)&int_val))){
				ma_db_statement_set_int(db_stmt, MA_REPO_TABLE_FILED_PROXY_USAGE, int_val);
			}
			if(MA_OK == (rc = ma_repository_get_authentication(repo, (ma_repository_auth_t*)&int_val))){
				ma_db_statement_set_int(db_stmt, MA_REPO_TABLE_FILED_AUTH_TYPE, int_val);
			}
			if(MA_OK == (rc = ma_repository_get_enabled(repo, (ma_bool_t*)&int_val))){
				ma_db_statement_set_int(db_stmt, MA_REPO_TABLE_FILED_ENABLED, int_val);
			}
			if(MA_OK == (rc = ma_repository_get_server_fqdn(repo, &str_val))){
				ma_db_statement_set_string(db_stmt, MA_REPO_TABLE_FILED_SERVER_FQDN, 1, str_val, strlen(str_val));
			}
			if(MA_OK == (rc = ma_repository_get_server_ip(repo, &str_val))){
				ma_db_statement_set_string(db_stmt, MA_REPO_TABLE_FILED_SERVER_IP, 1, str_val, strlen(str_val));
			}
			if(MA_OK == (rc = ma_repository_get_server_name(repo, &str_val))){
				ma_db_statement_set_string(db_stmt, MA_REPO_TABLE_FILED_SERVER_NAME, 1, str_val, strlen(str_val));
			}
			if(MA_OK == (rc = ma_repository_get_port(repo, (ma_uint32_t*)&int_val))){
				ma_db_statement_set_int(db_stmt, MA_REPO_TABLE_FILED_PORT, int_val);
			}
			if(MA_OK == (rc = ma_repository_get_ssl_port(repo, (ma_uint32_t*)&int_val))){
				ma_db_statement_set_int(db_stmt, MA_REPO_TABLE_FILED_SSL_PORT, int_val);
			}
			if(MA_OK == (rc = ma_repository_get_server_path(repo, &str_val))){
				ma_db_statement_set_string(db_stmt, MA_REPO_TABLE_FILED_PATH, 1, str_val, strlen(str_val));
			}
			if(MA_OK == (rc = ma_repository_get_domain_name(repo, &str_val))){
				ma_db_statement_set_string(db_stmt, MA_REPO_TABLE_FILED_DOMAIN, 1, str_val, strlen(str_val));
			}
			if(MA_OK == (rc = ma_repository_get_user_name(repo, &str_val))){
				ma_db_statement_set_string(db_stmt, MA_REPO_TABLE_FILED_AUTH_USER, 1, str_val, strlen(str_val));
			}
			if(MA_OK == (rc = ma_repository_get_password(repo, &str_val))){
				ma_db_statement_set_string(db_stmt, MA_REPO_TABLE_FILED_AUTH_PASSWD, 1, str_val, strlen(str_val));
			}
			ma_db_statement_set_int(db_stmt, MA_REPO_TABLE_FILED_IS_PASSWD_ENCRYPTED, 1); /*password encrypted */

			if(MA_OK == (rc = ma_repository_get_pingtime(repo, (ma_uint32_t*)&int_val))){
				if(repo_type == MA_REPOSITORY_TYPE_FALLBACK)
					ma_db_statement_set_int(db_stmt, MA_REPO_TABLE_FILED_PING_TIME, INT_MAX);
				else if(url_type == MA_REPOSITORY_URL_TYPE_LOCAL)
					ma_db_statement_set_int(db_stmt, MA_REPO_TABLE_FILED_PING_TIME, 0);
				else
					ma_db_statement_set_int(db_stmt, MA_REPO_TABLE_FILED_PING_TIME, int_val);
			}
			if(MA_OK == (rc = ma_repository_get_subnetdistance(repo, (ma_uint32_t*)&int_val))){
				if(repo_type == MA_REPOSITORY_TYPE_FALLBACK)
					ma_db_statement_set_int(db_stmt, MA_REPO_TABLE_FILED_SUBNET_DISTANCE, INT_MAX);
				else if(url_type == MA_REPOSITORY_URL_TYPE_LOCAL)
					ma_db_statement_set_int(db_stmt, MA_REPO_TABLE_FILED_SUBNET_DISTANCE, 0);
				else
					ma_db_statement_set_int(db_stmt, MA_REPO_TABLE_FILED_SUBNET_DISTANCE, int_val);
				
			}
			if(MA_OK == (rc = ma_repository_get_siteorder(repo, (ma_uint32_t*)&int_val))){
				ma_db_statement_set_int(db_stmt, MA_REPO_TABLE_FILED_SITELIST_ORDER, int_val);
			}
			if(MA_OK == (rc = ma_repository_get_state(repo, (ma_repository_state_t*)&int_val))){
				ma_db_statement_set_int(db_stmt, MA_REPO_TABLE_FILED_STATE, int_val);
			}

			rc = ma_db_statement_execute(db_stmt) ;
			(void)ma_db_statement_release(db_stmt) ;
		}
	}

    return rc  ;
}

ma_error_t ma_repository_db_remove_all_repositories(ma_db_t *db_handle){
	if(db_handle){
		ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;
		if( MA_OK == (rc = ma_db_statement_create(db_handle, REMOVE_ALL_REPOSITORIES_STMT, &db_stmt))) 
        {
			rc = ma_db_statement_execute(db_stmt);
		}
		(void)ma_db_statement_release(db_stmt);
		return rc;
	}
	return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_repository_db_remove_repository_by_name(ma_db_t *db_handle, const char *name){
	if(db_handle && name){
		ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;
		if( MA_OK == (rc = ma_db_statement_create(db_handle, REMOVE_REPOSITORY_STMT_FOR_NAME, &db_stmt))
			&& (MA_OK == (rc = ma_db_statement_set_string(db_stmt, 1, 1, name, strlen(name)))) )
		{
				rc = ma_db_statement_execute(db_stmt);
		}
		(void)ma_db_statement_release(db_stmt);
		return rc;
	}
	return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_repository_db_set_repository_ping_count(ma_db_t *db_handle, const char *name, ma_int32_t ping_count){
	if(db_handle && name){
		ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;
		if( MA_OK == (rc = ma_db_statement_create(db_handle, UPDATE_REPOSITORY_PING_COUNT_STMT, &db_stmt))
			&& (MA_OK == (rc = ma_db_statement_set_int(db_stmt, 1, ping_count))) 
			&& (MA_OK == (rc = ma_db_statement_set_string(db_stmt, 2, 1, name, strlen(name)))))
		{
				rc = ma_db_statement_execute(db_stmt);
		}
		(void)ma_db_statement_release(db_stmt);
		return rc;
	}
	return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_repository_db_set_repository_subnet_distance(ma_db_t *db_handle, const char *name, ma_int32_t subnet_distance){
	if(db_handle && name){
		ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;
		if( MA_OK == (rc = ma_db_statement_create(db_handle, UPDATE_REPOSITORY_SUBNET_DISTANCE_STMT, &db_stmt))
			&& (MA_OK == (rc = ma_db_statement_set_int(db_stmt, 1, subnet_distance))) 
			&& (MA_OK == (rc = ma_db_statement_set_string(db_stmt, 2, 1, name, strlen(name)))))
		{
				rc = ma_db_statement_execute(db_stmt);
		}
		(void)ma_db_statement_release(db_stmt);
		return rc;
	}
	return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_repository_db_set_all_repositories_enabled(ma_db_t *db_handle, ma_bool_t enabled) {
    if(db_handle){
		ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;
		if( MA_OK == (rc = ma_db_statement_create(db_handle, UPDATE_ALL_REPOSITORIES_ENABLED_STMT, &db_stmt))
			&& (MA_OK == (rc = ma_db_statement_set_int(db_stmt, 1, (ma_int32_t)enabled))) )
		{
				rc = ma_db_statement_execute(db_stmt);
		}
		(void)ma_db_statement_release(db_stmt);
		return rc;
	}
	return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_repository_db_set_repository_enabled(ma_db_t *db_handle, const char *name, ma_bool_t enabled) {
    if(db_handle && name){
		ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;
		if( MA_OK == (rc = ma_db_statement_create(db_handle, UPDATE_REPOSITORY_ENABLED_STMT, &db_stmt))
			&& (MA_OK == (rc = ma_db_statement_set_int(db_stmt, 1, (ma_int32_t)enabled))) 
			&& (MA_OK == (rc = ma_db_statement_set_string(db_stmt, 2, 1, name, strlen(name))))
			&& (MA_OK == (rc = ma_db_statement_set_string(db_stmt, 3, 1, name, strlen(name)))))
		{
				rc = ma_db_statement_execute(db_stmt);
		}
		(void)ma_db_statement_release(db_stmt);
		return rc;
	}
	return MA_ERROR_INVALIDARG ;
}

/* This STATE = (STATE + 10) logic used to retrieve the original state */
#define SET_LOCAL_REPOSITORY_STATE_STMT "UPDATE AGENT_REPOSITORIES SET STATE = (STATE + 10) WHERE NAMESPACE = 1"
ma_error_t ma_repository_db_set_local_repositories(ma_db_t *db_handle) {
    if(db_handle) {
		ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;

		if( MA_OK == (rc = ma_db_statement_create(db_handle, SET_LOCAL_REPOSITORY_STATE_STMT, &db_stmt)))
		{
            rc = ma_db_statement_execute(db_stmt) ;
            ma_db_statement_release(db_stmt) ;
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

#define REMOVE_REPOSITORY_FOR_STATE_STMT    "DELETE FROM AGENT_REPOSITORIES WHERE NAMESPACE = 1 AND STATE >= 10"
ma_error_t ma_repository_db_remove_repositories_by_state(ma_db_t *db_handle){
	if(db_handle){
		ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;
		if( MA_OK == (rc = ma_db_statement_create(db_handle, REMOVE_REPOSITORY_FOR_STATE_STMT, &db_stmt))) 
        {
			rc = ma_db_statement_execute(db_stmt);
		}
		(void)ma_db_statement_release(db_stmt);
		return rc;
	}
	return MA_ERROR_INVALIDARG ;
}

#define REMOVE_GLOBAL_REPOSITORY_STMT    "DELETE FROM AGENT_REPOSITORIES WHERE NAMESPACE = 0"
ma_error_t ma_repository_db_remove_all_global_repositories(ma_db_t *db_handle) {

	if(db_handle){
		ma_db_statement_t *db_stmt = NULL ;
		ma_error_t rc = MA_OK ;
		if( MA_OK == (rc = ma_db_statement_create(db_handle, REMOVE_GLOBAL_REPOSITORY_STMT, &db_stmt))) 
		{
			rc = ma_db_statement_execute(db_stmt);
		}
		(void)ma_db_statement_release(db_stmt);
		return rc;
	}
	return MA_ERROR_INVALIDARG ;
}

#define REMOVE_LOCAL_REPOSITORY_STMT    "DELETE FROM AGENT_REPOSITORIES WHERE NAMESPACE = 1"
ma_error_t ma_repository_db_remove_all_local_repositories(ma_db_t *db_handle) {
	if(db_handle) {
		ma_db_statement_t *db_stmt = NULL ;
		ma_error_t rc = MA_OK ;
		if( MA_OK == (rc = ma_db_statement_create(db_handle, REMOVE_LOCAL_REPOSITORY_STMT, &db_stmt))) 
		{
			rc = ma_db_statement_execute(db_stmt);
		}
		(void)ma_db_statement_release(db_stmt);
		return rc;
	}
	return MA_ERROR_INVALIDARG ;
}

/*#define UPDATE_GLOBLE_REPOSITORY_REMOVE_STATE       "UPDATE AGENT_REPOSITORIES SET STATE = 2 WHERE NAMESPACE = 0" */
/* This STATE = (STATE + 10) logic used to retrieve the original state */

#define UPDATE_GLOBLE_REPOSITORY_REMOVE_STATE       "UPDATE AGENT_REPOSITORIES SET STATE = (STATE + 10) WHERE NAMESPACE = 0"
ma_error_t ma_repository_db_mark_remove_all_global_repositories(ma_db_t *db_handle) {
	if(db_handle){
		ma_db_statement_t *db_stmt = NULL ;
		ma_error_t rc = MA_OK ;
		if( MA_OK == (rc = ma_db_statement_create(db_handle, UPDATE_GLOBLE_REPOSITORY_REMOVE_STATE, &db_stmt))) 
		{
			rc = ma_db_statement_execute(db_stmt);
		}
		(void)ma_db_statement_release(db_stmt);
		return rc;
	}
	return MA_ERROR_INVALIDARG ;
}

#define DELETE_GLOBLE_REPOSITORY_STATE       "DELETE FROM AGENT_REPOSITORIES WHERE STATE >= 10 AND NAMESPACE = 0"
ma_error_t ma_repository_db_remove_all_global_repositories_by_state(ma_db_t *db_handle) {
	if(db_handle){
		ma_db_statement_t *db_stmt = NULL ;
		ma_error_t rc = MA_OK ;
		if( MA_OK == (rc = ma_db_statement_create(db_handle, DELETE_GLOBLE_REPOSITORY_STATE, &db_stmt))) 
		{
			rc = ma_db_statement_execute(db_stmt);
		}
		(void)ma_db_statement_release(db_stmt);
		return rc;
	}
	return MA_ERROR_INVALIDARG ;
}

#define GET_RPOSITORY_NON_SITELIST_XML_ATTR_STMT  "SELECT ENABLED, PING_TIME, SUBNET_DISTANCE, STATE FROM AGENT_REPOSITORIES WHERE NAME = ?"
ma_error_t ma_repository_get_non_sitelist_xml_attr(ma_db_t *db_handle, const char *repo_name, int *enabled, int *ping_time, int *subnet, int *state) {
	if(db_handle && repo_name && enabled && ping_time && state) {
		ma_error_t rc = MA_OK ;
		ma_db_statement_t *db_stmt = NULL ;
		ma_db_recordset_t *db_record = NULL ;

		if( MA_OK == (rc = ma_db_statement_create(db_handle, GET_RPOSITORY_NON_SITELIST_XML_ATTR_STMT, &db_stmt))
			&& (MA_OK == (rc = ma_db_statement_set_string(db_stmt, 1, 1, repo_name, strlen(repo_name))))    )
		{
			if(MA_OK == (rc = ma_db_statement_execute_query(db_stmt, &db_record))) {
				if(MA_ERROR_NO_MORE_ITEMS != ma_db_recordset_next(db_record)) {
					ma_db_recordset_get_int(db_record, 1, enabled) ;
					ma_db_recordset_get_int(db_record, 2, ping_time) ;
					ma_db_recordset_get_int(db_record, 3, subnet) ;
					ma_db_recordset_get_int(db_record, 4, state) ;
				}
				else {
					rc = MA_ERROR_OBJECTNOTFOUND;
				}

			}
			(void)ma_db_statement_release(db_stmt) ;
			(void)ma_db_recordset_release(db_record) ;
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_repository_db_set_repository_sitelistorder(ma_db_t *db_handle, const char *name, ma_int32_t sitelist_order) {
    if(db_handle && name){
		ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;
		if( MA_OK == (rc = ma_db_statement_create(db_handle, UPDATE_REPOSITORY_SITELIST_ORDER_STMT, &db_stmt))
			&& (MA_OK == (rc = ma_db_statement_set_int(db_stmt, 1, sitelist_order))) 
			&& (MA_OK == (rc = ma_db_statement_set_string(db_stmt, 2, 1, name, strlen(name))))
			&& (MA_OK == (rc = ma_db_statement_set_string(db_stmt, 3, 1, name, strlen(name)))))
		{
				rc = ma_db_statement_execute(db_stmt);
		}
		(void)ma_db_statement_release(db_stmt);
		return rc;
	}
	return MA_ERROR_INVALIDARG ;
}

#define GET_ALL_REPOSITORIES_STMT								"SELECT * FROM AGENT_REPOSITORIES ORDER BY %s ASC"
#define GET_ALL_ENABLED_REPOSITORIES_STMT						"SELECT * FROM AGENT_REPOSITORIES WHERE ENABLED = 1 ORDER BY %s ASC"		
#define GET_SPIPE_ENABLED_REPOSITORIES_BY_STMT					"SELECT * FROM AGENT_REPOSITORIES WHERE URL_TYPE = 4 AND ENABLED = 1 ORDER BY %s ASC"
#define GET_SPIPE_REPOSITORIES_BY_STMT							"SELECT * FROM AGENT_REPOSITORIES WHERE URL_TYPE = 4 ORDER BY %s ASC"

/*Give all site except disabled, fallback & local url type */
#define GET_ALL_REPOSITORIES_FOR_RANKING_STMT           "SELECT * FROM AGENT_REPOSITORIES WHERE STATE != 0 AND REPO_TYPE != 2 AND URL_TYPE != 3 AND ENABLED = 1 ORDER BY %s ASC"

static ma_error_t get_db_statement_by_filter(ma_db_t *db_handle, const char *filter, ma_repository_rank_type_t repository_rank, ma_db_statement_t **db_stmt) {
	char *query = NULL ;

	if(!strcmp(filter, MA_REPOSITORY_GET_REPOSITORIES_FILTER_NONE)) {
		query = strdup(GET_ALL_REPOSITORIES_STMT) ;
	}
	else if(!strcmp(filter, MA_REPOSITORY_GET_REPOSITORIES_FILTER_ALL_ENABLED)) {
		query = strdup(GET_ALL_ENABLED_REPOSITORIES_STMT) ;
	}
	else if(!strcmp(filter, MA_REPOSITORY_GET_REPOSITORIES_FILTER_SPIPE_ENABLED)) {
		query = strdup(GET_SPIPE_ENABLED_REPOSITORIES_BY_STMT) ;
	}
	else if(!strcmp(filter, MA_REPOSITORY_GET_REPOSITORIES_FILTER_SPIPE)) {
		query = strdup(GET_SPIPE_REPOSITORIES_BY_STMT) ;
	}
	else if(!strcmp(filter, MA_REPOSITORY_GET_REPOSITORIES_FILTER_FOR_RANKING)) {
		query = strdup(GET_ALL_REPOSITORIES_FOR_RANKING_STMT) ;
	}

	if(query) {
		char *rank = NULL, *p = NULL ;
		ma_temp_buffer_t buf = MA_TEMP_BUFFER_INIT ;

		if(repository_rank == MA_REPOSITORY_RANK_BY_PING_TIME) {
			rank = strdup("PING_TIME") ;
		}
		else if(repository_rank == MA_REPOSITORY_RANK_BY_SUBNET_DISTANCE) {
			rank = strdup("SUBNET_DISTANCE") ;
		}
		else {
			rank = strdup("SITELIST_ORDER") ;
		}

		(void)ma_temp_buffer_reserve(&buf, strlen(query) + strlen(query) + 1) ;
		
		MA_MSC_SELECT(_snprintf, snprintf)(p = (char *)ma_temp_buffer_get(&buf), ma_temp_buffer_capacity(&buf), query, rank) ;

		free(query);
		free(rank);
		return ma_db_statement_create(db_handle, p, db_stmt) ;
	}
	return MA_ERROR_INVALID_REQUEST ;
}

ma_error_t ma_repository_db_get_repositories(ma_db_t *db_handle, ma_crypto_t *crypto, const char *filter, ma_repository_rank_type_t repository_rank, ma_repository_list_t **repo_list){
	if(db_handle && repo_list){
		ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;

		if(MA_OK == (rc = get_db_statement_by_filter(db_handle, filter, repository_rank, &db_stmt)))
		{
			ma_db_recordset_t *db_record = NULL ;
			if(MA_OK == (rc = ma_db_statement_execute_query(db_stmt, &db_record))) {
				ma_repository_list_t *repo_list_tmp = NULL ;

				if(MA_OK == (rc = ma_repository_list_create(&repo_list_tmp))) {
					while(MA_ERROR_NO_MORE_ITEMS != ma_db_recordset_next(db_record)) {
						ma_repository_t *repo_tmp = NULL ;
				
						if(MA_OK == (rc = ma_repository_create(&repo_tmp))) {
							const char *str_value  = NULL ;
							int int_value = 0 ;

							(void)ma_db_recordset_get_string(db_record, MA_REPO_TABLE_FILED_NAME, &str_value) ;
							(void)ma_repository_set_name(repo_tmp, str_value) ; str_value = NULL ;
                    
							(void)ma_db_recordset_get_int(db_record, MA_REPO_TABLE_FILED_REPO_TYPE, &int_value) ;
							(void)ma_repository_set_type(repo_tmp, (ma_repository_type_t)int_value) ;
                    
							(void)ma_db_recordset_get_int(db_record, MA_REPO_TABLE_FILED_URL_TYPE, &int_value) ;
							(void)ma_repository_set_url_type(repo_tmp, (ma_repository_url_type_t)int_value) ;
                    
							(void)ma_db_recordset_get_int(db_record, MA_REPO_TABLE_FILED_NAMESPACE, &int_value) ;
							(void)ma_repository_set_namespace(repo_tmp, (ma_repository_namespace_t)int_value) ;

							(void)ma_db_recordset_get_int(db_record, MA_REPO_TABLE_FILED_PROXY_USAGE, &int_value) ;
							(void)ma_repository_set_proxy_usage_type(repo_tmp, (ma_proxy_usage_type_t)int_value) ;

							(void)ma_db_recordset_get_int(db_record, MA_REPO_TABLE_FILED_AUTH_TYPE, &int_value) ;
							(void)ma_repository_set_authentication(repo_tmp, (ma_repository_auth_t)int_value) ;

							(void)ma_db_recordset_get_int(db_record, MA_REPO_TABLE_FILED_ENABLED, &int_value) ;
							(void)ma_repository_set_enabled(repo_tmp, (int_value == 0)?MA_FALSE:MA_TRUE) ;

							(void)ma_db_recordset_get_string(db_record, MA_REPO_TABLE_FILED_SERVER_FQDN, &str_value) ;
							(void)ma_repository_set_server_fqdn(repo_tmp, str_value) ; str_value = NULL ;
					
							(void)ma_db_recordset_get_string(db_record, MA_REPO_TABLE_FILED_SERVER_IP, &str_value) ;
							(void)ma_repository_set_server_ip(repo_tmp, str_value) ; str_value = NULL ;

							(void)ma_db_recordset_get_string(db_record, MA_REPO_TABLE_FILED_SERVER_NAME, &str_value) ;
							(void)ma_repository_set_server_name(repo_tmp, str_value) ; str_value = NULL ;
					                    
							(void)ma_db_recordset_get_int(db_record, MA_REPO_TABLE_FILED_PORT, &int_value) ;
							(void)ma_repository_set_port(repo_tmp, (ma_int32_t)int_value) ;
		            
							(void)ma_db_recordset_get_int(db_record, MA_REPO_TABLE_FILED_SSL_PORT, &int_value) ;
							(void)ma_repository_set_ssl_port(repo_tmp, (ma_int32_t)int_value) ;

							(void)ma_db_recordset_get_string(db_record, MA_REPO_TABLE_FILED_PATH, &str_value) ;
							(void)ma_repository_set_server_path(repo_tmp, str_value) ; str_value = NULL ;

							(void)ma_db_recordset_get_string(db_record, MA_REPO_TABLE_FILED_DOMAIN, &str_value) ;
							(void)ma_repository_set_domain_name(repo_tmp, str_value) ; str_value = NULL ;
		            
							(void)ma_db_recordset_get_string(db_record, MA_REPO_TABLE_FILED_AUTH_USER, &str_value) ;
							(void)ma_repository_set_user_name(repo_tmp, str_value) ; str_value = NULL ;

							(void)ma_db_recordset_get_string(db_record, MA_REPO_TABLE_FILED_AUTH_PASSWD, &str_value) ;
							if(str_value) {
								ma_bytebuffer_t *decrypted_buffer = NULL ;

								if(MA_OK == ma_bytebuffer_create(strlen(str_value), &decrypted_buffer)) {
									ma_crypto_decryption_t *decrypt = NULL ;

									if(MA_OK == ma_crypto_decrypt_create(crypto, MA_CRYPTO_DECRYPTION_3DES, &decrypt)) {
										if(MA_OK == ma_crypto_decode_and_decrypt(decrypt, (const unsigned char *)str_value, strlen(str_value), decrypted_buffer)){
											unsigned char *passwd_str = (unsigned char*) calloc((ma_bytebuffer_get_size(decrypted_buffer) + 1), sizeof(unsigned char) ) ;

											memcpy (passwd_str, ma_bytebuffer_get_bytes(decrypted_buffer), ma_bytebuffer_get_size(decrypted_buffer)) ;
											*(passwd_str + ma_bytebuffer_get_size(decrypted_buffer)) ='\0' ;
						
											(void)ma_repository_set_password(repo_tmp, (const char*)passwd_str) ; 
						
											free(passwd_str) ;
										}
										(void)ma_crypto_decrypt_release(decrypt) ;
									}
									(void)ma_bytebuffer_release(decrypted_buffer) ;
								}
							}

							(void)ma_db_recordset_get_int(db_record, MA_REPO_TABLE_FILED_PING_TIME, (int*)&repo_tmp->ping_time) ;
							(void)ma_db_recordset_get_int(db_record, MA_REPO_TABLE_FILED_SUBNET_DISTANCE, (int*)&repo_tmp->subnet_distance) ;
							(void)ma_db_recordset_get_int(db_record, MA_REPO_TABLE_FILED_SITELIST_ORDER, (int*)&repo_tmp->sitelist_order) ;
							(void)ma_db_recordset_get_int(db_record, MA_REPO_TABLE_FILED_STATE, (int*)&repo_tmp->state) ;
                    
							(void)ma_repository_list_add_repository(repo_list_tmp, repo_tmp) ;                    
							(void)ma_repository_release(repo_tmp) ; repo_tmp = NULL ;
						}
					}
					*repo_list = repo_list_tmp ;
				}
				(void)ma_db_recordset_release(db_record) ;	db_record = NULL ;
			}
			(void)ma_db_statement_release(db_stmt) ;	db_stmt = NULL ;
		}
		return rc;
	}
	return MA_ERROR_INVALIDARG ;
}

#define GET_ALL_SPIPE_REPOSITORIES                      "SELECT * FROM AGENT_REPOSITORIES WHERE URL_TYPE = 4 ORDER BY SITELIST_ORDER ASC"
ma_error_t ma_repository_db_get_spipe_repositories(ma_db_t *db_handle, ma_db_recordset_t **db_record) {
    if(db_handle && db_record){
		ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;

        if(MA_OK == (rc =  ma_db_statement_create(db_handle, GET_ALL_SPIPE_REPOSITORIES, &db_stmt))) {
            rc = ma_db_statement_execute_query(db_stmt, db_record) ;
			(void)ma_db_statement_release(db_stmt);
		}
		return rc;
	}
	return MA_ERROR_INVALIDARG ;
}

#define GET_RPOSITORY_PING_COUNT_FOR_REPOSITORY_NAME_STMT  "SELECT PING_TIME FROM AGENT_REPOSITORIES WHERE NAME = ?"
ma_error_t ma_repository_db_get_repository_ping_count(ma_db_t *db_handle, const char *repo_name, ma_int32_t *ping_count) {
	if(db_handle && repo_name && ping_count) {
		ma_error_t rc = MA_OK ;
		ma_db_statement_t *db_stmt = NULL ;
		ma_db_recordset_t *db_record = NULL ;

		if( MA_OK == (rc = ma_db_statement_create(db_handle, GET_RPOSITORY_PING_COUNT_FOR_REPOSITORY_NAME_STMT, &db_stmt))
			&& (MA_OK == (rc = ma_db_statement_set_string(db_stmt, 1, 1, repo_name, strlen(repo_name))))    )
		{
			if(MA_OK == (rc = ma_db_statement_execute_query(db_stmt, &db_record))) {
				if(MA_ERROR_NO_MORE_ITEMS != ma_db_recordset_next(db_record)) {
					ma_db_recordset_get_int(db_record, 1, ping_count) ;
				}
				else {
					*ping_count = 0 ;
					rc = MA_ERROR_OBJECTNOTFOUND;
				}
			}
			else
				*ping_count = 0 ;
			(void)ma_db_statement_release(db_stmt) ;
			(void)ma_db_recordset_release(db_record) ;
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

#define GET_RPOSITORY_SUBNET_DISTANCE_FOR_REPOSITORY_NAME_STMT  "SELECT SUBNET_DISTANCE FROM AGENT_REPOSITORIES WHERE NAME = ?"
ma_error_t ma_repository_db_get_repository_subnet_distance(ma_db_t *db_handle, const char *repo_name, ma_int32_t *subnet_distance) {
	if(db_handle && repo_name && subnet_distance) {
		ma_error_t rc = MA_OK ;
		ma_db_statement_t *db_stmt = NULL ;
		ma_db_recordset_t *db_record = NULL ;

		if( MA_OK == (rc = ma_db_statement_create(db_handle, GET_RPOSITORY_SUBNET_DISTANCE_FOR_REPOSITORY_NAME_STMT, &db_stmt))
			&& (MA_OK == (rc = ma_db_statement_set_string(db_stmt, 1, 1, repo_name, strlen(repo_name))))    )
		{
			if(MA_OK == (rc = ma_db_statement_execute_query(db_stmt, &db_record))) {
				if(MA_ERROR_NO_MORE_ITEMS != ma_db_recordset_next(db_record)) {
					ma_db_recordset_get_int(db_record, 1, subnet_distance) ;
				}
				else {
					*subnet_distance = 0 ;
					rc = MA_ERROR_OBJECTNOTFOUND;
				}
			}
			else
				*subnet_distance = 0 ;
			(void)ma_db_statement_release(db_stmt) ;
			(void)ma_db_recordset_release(db_record) ;
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}


#define GET_RPOSITORY_SERVER_FQDN_FOR_REPOSITORY_NAME_STMT  "SELECT SERVER_FQDN FROM AGENT_REPOSITORIES WHERE NAME = ?"
ma_error_t ma_repository_db_get_repository_fqdn(ma_db_t *db_handle, const char *repo_name, char **server) {
    if(db_handle && repo_name && server) {
        ma_error_t rc = MA_OK ;
        ma_db_statement_t *db_stmt = NULL ;
        ma_db_recordset_t *db_record = NULL ;
		const char *server_fqdn = NULL;
		*server = NULL ;

        if( MA_OK == (rc = ma_db_statement_create(db_handle, GET_RPOSITORY_SERVER_FQDN_FOR_REPOSITORY_NAME_STMT, &db_stmt))
            && (MA_OK == (rc = ma_db_statement_set_string(db_stmt, 1, 1, repo_name, strlen(repo_name))))    )
        {
            if(MA_OK == (rc = ma_db_statement_execute_query(db_stmt, &db_record))) {
                if(MA_ERROR_NO_MORE_ITEMS != ma_db_recordset_next(db_record)) {
                    ma_db_recordset_get_string(db_record, 1, &server_fqdn) ;
					if(server_fqdn)
						*server = strdup(server_fqdn);
				}
                else {
					rc = MA_ERROR_OBJECTNOTFOUND;
				}

            }              
            (void)ma_db_statement_release(db_stmt) ;
            (void)ma_db_recordset_release(db_record) ;
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

#define GET_RPOSITORY_SERVER_IP_FOR_REPOSITORY_NAME_STMT  "SELECT SERVER_IP FROM AGENT_REPOSITORIES WHERE NAME = ?"
ma_error_t ma_repository_db_get_repository_ip(ma_db_t *db_handle, const char *repo_name, char **server) {
	if(db_handle && repo_name && server) {
		ma_error_t rc = MA_OK ;
		ma_db_statement_t *db_stmt = NULL ;
		ma_db_recordset_t *db_record = NULL ;
		const char *server_ip = NULL;
		*server = NULL ;

		if( MA_OK == (rc = ma_db_statement_create(db_handle, GET_RPOSITORY_SERVER_IP_FOR_REPOSITORY_NAME_STMT, &db_stmt))
			&& (MA_OK == (rc = ma_db_statement_set_string(db_stmt, 1, 1, repo_name, strlen(repo_name))))    )
		{
			if(MA_OK == (rc = ma_db_statement_execute_query(db_stmt, &db_record))) {
				if(MA_ERROR_NO_MORE_ITEMS != ma_db_recordset_next(db_record)) {
					ma_db_recordset_get_string(db_record, 1, &server_ip) ;
					if(server_ip)
						*server = strdup(server_ip);
				}
				else {
					rc = MA_ERROR_OBJECTNOTFOUND;
				}

			}
			(void)ma_db_statement_release(db_stmt) ;
			(void)ma_db_recordset_release(db_record) ;
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

#define GET_RPOSITORY_SERVER_NAME_FOR_REPOSITORY_NAME_STMT  "SELECT SERVER_NAME FROM AGENT_REPOSITORIES WHERE NAME = ?"
ma_error_t ma_repository_db_get_repository_server(ma_db_t *db_handle, const char *repo_name, char **server) {
	if(db_handle && repo_name && server) {
		ma_error_t rc = MA_OK ;
		ma_db_statement_t *db_stmt = NULL ;
		ma_db_recordset_t *db_record = NULL ;
		const char *server_name = NULL;
		*server = NULL ;

		if( MA_OK == (rc = ma_db_statement_create(db_handle, GET_RPOSITORY_SERVER_NAME_FOR_REPOSITORY_NAME_STMT, &db_stmt))
			&& (MA_OK == (rc = ma_db_statement_set_string(db_stmt, 1, 1, repo_name, strlen(repo_name))))    )
		{
			if(MA_OK == (rc = ma_db_statement_execute_query(db_stmt, &db_record))) {
				if(MA_ERROR_NO_MORE_ITEMS != ma_db_recordset_next(db_record)) {
					ma_db_recordset_get_string(db_record, 1, &server_name) ;
					if(server_name)
						*server = strdup(server_name);
				}
				else {
						rc = MA_ERROR_OBJECTNOTFOUND;
				}
			}		
			(void)ma_db_statement_release(db_stmt) ;
			(void)ma_db_recordset_release(db_record) ;
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

#define GET_RPOSITORY_SERVER_PATH_FOR_REPOSITORY_NAME_STMT  "SELECT PATH FROM AGENT_REPOSITORIES WHERE NAME = ?"
ma_error_t ma_repository_db_get_repository_path(ma_db_t *db_handle, const char *repo_name, char **path) {
    if(db_handle && repo_name && path) {
        ma_error_t rc = MA_OK ;
        ma_db_statement_t *db_stmt = NULL ;
        ma_db_recordset_t *db_record = NULL ;
		const char *relative_path = NULL;
		*path = NULL ;

        if( MA_OK == (rc = ma_db_statement_create(db_handle, GET_RPOSITORY_SERVER_PATH_FOR_REPOSITORY_NAME_STMT, &db_stmt))
            && (MA_OK == (rc = ma_db_statement_set_string(db_stmt, 1, 1, repo_name, strlen(repo_name))))    )
        {
            if(MA_OK == (rc = ma_db_statement_execute_query(db_stmt, &db_record))) {
                if(MA_ERROR_NO_MORE_ITEMS != ma_db_recordset_next(db_record)) {
                    (void)ma_db_recordset_get_string(db_record, 1, &relative_path) ;
					if(relative_path)
						*path = strdup(relative_path);
				}
                else {
					rc = MA_ERROR_OBJECTNOTFOUND;
				}

            }
                
            (void)ma_db_statement_release(db_stmt) ;
            (void)ma_db_recordset_release(db_record) ;
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

/* UPDATE AGENT_REPOSITORIES SET STATE = ADDED/REMOVED/ALTERED/DEFAULT WHERE REPO_TYPE != FALLBACK */
#define SET_ALL_REPOSITORY_STATE_STMT  "UPDATE AGENT_REPOSITORIES SET STATE = ? WHERE REPO_TYPE != 2"
ma_error_t ma_repository_db_set_all_repository_state(ma_db_t *db_handle, ma_repository_state_t state) {
	if(db_handle){
		ma_db_statement_t *db_stmt = NULL ;
		ma_error_t rc = MA_OK ;
		if( MA_OK == (rc = ma_db_statement_create(db_handle, SET_ALL_REPOSITORY_STATE_STMT, &db_stmt))
			&& (MA_OK == (rc = ma_db_statement_set_int(db_stmt, 1, (ma_int32_t)state))) )
		{
			rc = ma_db_statement_execute(db_stmt);
		}
		(void)ma_db_statement_release(db_stmt);
		return rc;
	}
	return MA_ERROR_INVALIDARG ;
}

#define GET_REPOSITORY_URL_TYPE_BY_NAME_STMT  "SELECT URL_TYPE FROM AGENT_REPOSITORIES WHERE NAME = ? COLLATE NOCASE"
ma_error_t ma_repository_db_get_url_type_by_name(ma_db_t *db_handle, const char *repo_name, ma_repository_url_type_t *url_type) {
    if(db_handle && repo_name && url_type) {
		ma_error_t rc = MA_OK ;
		ma_db_statement_t *db_stmt = NULL ;
		ma_db_recordset_t *db_record = NULL ;
        *url_type = MA_REPOSITORY_URL_TYPE_END ;

		if( MA_OK == (rc = ma_db_statement_create(db_handle, GET_REPOSITORY_URL_TYPE_BY_NAME_STMT, &db_stmt))
			&& (MA_OK == (rc = ma_db_statement_set_string(db_stmt, 1, 1, repo_name, strlen(repo_name))))    )
		{
			if(MA_OK == (rc = ma_db_statement_execute_query(db_stmt, &db_record))) {
				if(MA_ERROR_NO_MORE_ITEMS != ma_db_recordset_next(db_record)) {
                    int type = 0;
					ma_db_recordset_get_int(db_record, 1, &type) ;
					*url_type = (ma_repository_url_type_t) type ;
				}
				else 
					rc = MA_ERROR_OBJECTNOTFOUND;
			}
			
            (void)ma_db_statement_release(db_stmt) ;
			(void)ma_db_recordset_release(db_record) ;
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

#define GET_REPOSITORY_STATE_STMT  "SELECT STATE FROM AGENT_REPOSITORIES WHERE NAME = ?"
ma_error_t ma_repository_db_get_repository_state(ma_db_t *db_handle, const char *repo_name, ma_repository_state_t *state) {
	if(db_handle && repo_name && state) {
		ma_error_t rc = MA_OK ;
		ma_db_statement_t *db_stmt = NULL ;
		ma_db_recordset_t *db_record = NULL ;
		int state_tmp = 0;

		if( MA_OK == (rc = ma_db_statement_create(db_handle, GET_REPOSITORY_STATE_STMT, &db_stmt))
			&& (MA_OK == (rc = ma_db_statement_set_string(db_stmt, 1, 1, repo_name, strlen(repo_name))))    )
		{
			if(MA_OK == (rc = ma_db_statement_execute_query(db_stmt, &db_record))) {
				if(MA_ERROR_NO_MORE_ITEMS != ma_db_recordset_next(db_record)) {
					ma_db_recordset_get_int(db_record, 1, &state_tmp) ;
					*state = (ma_repository_state_t) state_tmp ;
				}
				else {
					*state = (ma_repository_state_t)0 ;
					rc = MA_ERROR_OBJECTNOTFOUND;
				}
			}
			else
				*state = (ma_repository_state_t)0 ;
			(void)ma_db_statement_release(db_stmt) ;
			(void)ma_db_recordset_release(db_record) ;
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

#define RESET_ALL_REPOSITORY_ICMP_ATTR_STMT  "UPDATE AGENT_REPOSITORIES SET STATE = 3,  PING_TIME= ?, SUBNET_DISTANCE = ? WHERE REPO_TYPE != 2 AND URL_TYPE != 3"
ma_error_t ma_repository_db_reset_all_repository_icmp_attr(ma_db_t *db_handle, int ping_time, int subnet) {
	if(db_handle){
		ma_db_statement_t *db_stmt = NULL ;
		ma_error_t rc = MA_OK ;
		if( MA_OK == (rc = ma_db_statement_create(db_handle, RESET_ALL_REPOSITORY_ICMP_ATTR_STMT, &db_stmt))
			&& (MA_OK == (rc = ma_db_statement_set_int(db_stmt, 1, (ma_int32_t)ping_time)))
			&& (MA_OK == (rc = ma_db_statement_set_int(db_stmt, 2, (ma_int32_t)subnet))))
		{
			rc = ma_db_statement_execute(db_stmt);
		}
		(void)ma_db_statement_release(db_stmt);
		return rc;
	}
	return MA_ERROR_INVALIDARG ;
}

#define RESET_ALL_REPOSITORY_PING_ATTR_STMT  "UPDATE AGENT_REPOSITORIES SET STATE = 3,  PING_TIME= ? WHERE REPO_TYPE != 2 AND URL_TYPE != 3"
ma_error_t ma_repository_db_reset_all_repository_ping_attr(ma_db_t *db_handle, int ping_time) {
	if(db_handle){
		ma_db_statement_t *db_stmt = NULL ;
		ma_error_t rc = MA_OK ;
		if( MA_OK == (rc = ma_db_statement_create(db_handle, RESET_ALL_REPOSITORY_PING_ATTR_STMT, &db_stmt))
			&& (MA_OK == (rc = ma_db_statement_set_int(db_stmt, 1, (ma_int32_t)ping_time))))
		{
			rc = ma_db_statement_execute(db_stmt);
		}
		(void)ma_db_statement_release(db_stmt);
		return rc;
	}
	return MA_ERROR_INVALIDARG ;
}

#define RESET_ALL_REPOSITORY_SUBNET_DIST_ATTR_STMT  "UPDATE AGENT_REPOSITORIES SET STATE = 3,  SUBNET_DISTANCE= ? WHERE REPO_TYPE != 2 AND URL_TYPE != 3"
ma_error_t ma_repository_db_reset_all_repository_subnet_dist_attr(ma_db_t *db_handle, int subnet) {
	if(db_handle){
		ma_db_statement_t *db_stmt = NULL ;
		ma_error_t rc = MA_OK ;
		if( MA_OK == (rc = ma_db_statement_create(db_handle, RESET_ALL_REPOSITORY_SUBNET_DIST_ATTR_STMT, &db_stmt))
			&& (MA_OK == (rc = ma_db_statement_set_int(db_stmt, 1, (ma_int32_t)subnet))))
		{
			rc = ma_db_statement_execute(db_stmt);
		}
		(void)ma_db_statement_release(db_stmt);
		return rc;
	}
	return MA_ERROR_INVALIDARG ;
}

#define GET_REPOSITORY_POLICIES_FROM_POLICY_DB_STMT  "SELECT DISTINCT MPSO.PSO_DATA FROM MA_POLICY_OBJECT MPO, MA_POLICY_PRODUCT_ASSIGNMENT MPPA, MA_POLICY_SETTINGS_OBJECT MPSO WHERE MPO.PO_ID = MPSO.PO_ID AND MPO.PO_ID = MPPA.PO_ID AND MPPA.PPA_PRODUCT = ? AND MPO.PO_CATEGORY = ? AND MPO.PO_TYPE = ? AND MPSO.PSO_TIMESTAMP != ?"
#define REPOSITORY_POLICY_CATEGORY      "Repository"
ma_error_t ma_repository_db_get_policy(ma_db_t *db_handle, const char *product, const char *timestamp, ma_db_recordset_t **db_record) {
    if(db_handle && product && timestamp && db_record) {
        ma_error_t rc = MA_OK ;
        ma_db_statement_t *db_stmt = NULL ;

        if( MA_OK == (rc = ma_db_statement_create(db_handle, GET_REPOSITORY_POLICIES_FROM_POLICY_DB_STMT, &db_stmt))
            && (MA_OK == (rc = ma_db_statement_set_string(db_stmt, 1, 1, product, strlen(product))))
            && (MA_OK == (rc = ma_db_statement_set_string(db_stmt, 2, 1, REPOSITORY_POLICY_CATEGORY, strlen(REPOSITORY_POLICY_CATEGORY))))
            && (MA_OK == (rc = ma_db_statement_set_string(db_stmt, 3, 1, REPOSITORY_POLICY_CATEGORY, strlen(REPOSITORY_POLICY_CATEGORY))))
            && (MA_OK == (rc = ma_db_statement_set_string(db_stmt, 4, 1, timestamp, strlen(timestamp))))    )
        {
            rc = ma_db_statement_execute_query(db_stmt, db_record) ;
			(void)ma_db_statement_release(db_stmt);
	    }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}


#define GET_URL_TYPE_BY_SERVER "SELECT URL_TYPE FROM AGENT_REPOSITORIES WHERE SERVER_IP = ? OR SERVER_FQDN = ? OR SERVER_NAME = ?"
ma_error_t ma_repository_db_get_url_type_by_server(ma_db_t *db_handle, const char *server, ma_db_recordset_t **db_record) {
    if(db_handle && server && db_record) {
        ma_error_t rc = MA_OK ;
        ma_db_statement_t *db_stmt = NULL ;

        if( MA_OK == (rc = ma_db_statement_create(db_handle, GET_URL_TYPE_BY_SERVER, &db_stmt))
            && (MA_OK == (rc = ma_db_statement_set_string(db_stmt, 1, 1, server, strlen(server)))) 
            && (MA_OK == (rc = ma_db_statement_set_string(db_stmt, 2, 1, server, strlen(server)))) 
            && (MA_OK == (rc = ma_db_statement_set_string(db_stmt, 3, 1, server, strlen(server)))) )
        {
            rc = ma_db_statement_execute_query(db_stmt, db_record);
            (void)ma_db_statement_release(db_stmt) ;            
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

static ma_error_t   ma_repository_check_state_internal(ma_db_t *db, ma_repository_t *repository, const char *server_fqdn, const char *server_ip, const char *server_name, ma_repository_state_t *state){
	ma_error_t rc = MA_ERROR_INVALIDARG;
	if(repository && state){
		if(MA_OK == ma_repository_db_get_repository_state(db, repository->name, state)){
			if(*state >= 10)
				*state = (ma_repository_state_t)(*state - 10);
		}
		else
			*state = MA_REPOSITORY_STATE_DEFAULT;

		if(MA_REPOSITORY_URL_TYPE_SA == repository->url_type){
			if( ((repository->params.sa_repository.server_fqdn) && (!server_fqdn)) || ((!repository->params.sa_repository.server_fqdn) && (server_fqdn)) || 
				((repository->params.sa_repository.server_ip) && (!server_ip)) || ((!repository->params.sa_repository.server_ip) && (server_ip)) ||
				((repository->params.sa_repository.server_name) && (!server_name)) || ((!repository->params.sa_repository.server_name) && (server_name)) ) {
					*state = MA_REPOSITORY_STATE_ALTERED;
					return MA_OK;
			}
			if((repository->params.sa_repository.server_fqdn) && server_fqdn && strcmp(repository->params.sa_repository.server_fqdn, server_fqdn)) {
				*state = MA_REPOSITORY_STATE_ALTERED;
				return MA_OK;
			}
			if((repository->params.sa_repository.server_ip) && server_ip && strcmp(repository->params.sa_repository.server_ip, server_ip)) {
				*state = MA_REPOSITORY_STATE_ALTERED;
				return MA_OK;
			}
			if((repository->params.sa_repository.server_name) && server_name && strcmp(repository->params.sa_repository.server_name, server_name)) {
				*state = MA_REPOSITORY_STATE_ALTERED;
				return MA_OK;
			}
		}
		else if(MA_REPOSITORY_URL_TYPE_SPIPE == repository->url_type){
			if( ((repository->params.spipe_repository.server_fqdn) && (!server_fqdn)) || ((!repository->params.spipe_repository.server_fqdn) && (server_fqdn)) || 
				((repository->params.spipe_repository.server_ip) && (!server_ip)) || ((!repository->params.spipe_repository.server_ip) && (server_ip)) ||
				((repository->params.spipe_repository.server_name) && (!server_name)) || ((!repository->params.spipe_repository.server_name) && (server_name)) ) {
					*state = MA_REPOSITORY_STATE_ALTERED;
					return MA_OK;
			}
			if((repository->params.spipe_repository.server_fqdn) && server_fqdn && strcmp(repository->params.spipe_repository.server_fqdn, server_fqdn)) {
				*state = MA_REPOSITORY_STATE_ALTERED;
				return MA_OK;
			}
			if((repository->params.spipe_repository.server_ip) && server_ip && strcmp(repository->params.spipe_repository.server_ip, server_ip)) {
				*state = MA_REPOSITORY_STATE_ALTERED;
				return MA_OK;
			}
			if((repository->params.spipe_repository.server_name) && server_name && strcmp(repository->params.spipe_repository.server_name, server_name)) {
				*state = MA_REPOSITORY_STATE_ALTERED;
				return MA_OK;
			}
		}
		else if(MA_REPOSITORY_URL_TYPE_HTTP == repository->url_type){
			if( ((repository->params.http_repository.server_fqdn) && (!server_fqdn)) || ((!repository->params.http_repository.server_fqdn) && (server_fqdn)) ) {
				*state = MA_REPOSITORY_STATE_ALTERED;
				return MA_OK;
			}
			if((repository->params.http_repository.server_fqdn) && server_fqdn && strcmp(repository->params.http_repository.server_fqdn, server_fqdn)) {
				*state = MA_REPOSITORY_STATE_ALTERED;
				return MA_OK;
			}
		}
		else if(MA_REPOSITORY_URL_TYPE_FTP == repository->url_type){
			if( ((repository->params.ftp_repository.server_fqdn) && (!server_fqdn)) || ((!repository->params.ftp_repository.server_fqdn) && (server_fqdn)) ) {
				*state = MA_REPOSITORY_STATE_ALTERED;
				return MA_OK;
			}
			if((repository->params.ftp_repository.server_fqdn) && server_fqdn && strcmp(repository->params.ftp_repository.server_fqdn, server_fqdn)) {
				*state = MA_REPOSITORY_STATE_ALTERED;
				return MA_OK;
			}
		}
		else if(MA_REPOSITORY_URL_TYPE_UNC == repository->url_type){
			if( ((repository->params.ucn_repository.server_fqdn) && (!server_fqdn)) || ((!repository->params.ucn_repository.server_fqdn) && (server_fqdn)) ) {
				*state = MA_REPOSITORY_STATE_ALTERED;
				return MA_OK;
			}
			if((repository->params.ucn_repository.server_fqdn) && server_fqdn && strcmp(repository->params.ucn_repository.server_fqdn, server_fqdn)) {
				*state = MA_REPOSITORY_STATE_ALTERED;
				return MA_OK;
			}
		}
		return MA_OK;
	}
	return rc;
}

static ma_error_t   ma_repository_check_state(ma_db_t *db, ma_repository_t *repository, ma_repository_state_t *state){
	ma_error_t rc = MA_ERROR_INVALIDARG;
	if(db && repository){
		char *server_fqdn = NULL;
		char *server_ip = NULL;
		char *server_name = NULL;
		(void)ma_repository_db_get_repository_fqdn(db, repository->name, &server_fqdn);
		(void)ma_repository_db_get_repository_ip(db, repository->name, &server_ip);
		(void)ma_repository_db_get_repository_server(db, repository->name, &server_name);
		rc = ma_repository_check_state_internal(db, repository, server_fqdn, server_ip, server_name, state);

		if(server_fqdn)
			free(server_fqdn);
		if(server_ip)
			free(server_ip);
		if(server_name)
			free(server_name);
	}
	return rc;
}

ma_error_t   ma_repository_set_icmp_attributes(ma_db_t *db, ma_repository_rank_type_t rank_type, ma_uint32_t max_pingtime, ma_uint32_t subnetdistance, ma_repository_t *repository){
	ma_error_t rc = MA_ERROR_INVALIDARG;
	if(db && repository){
		ma_repository_state_t state;
		ma_int32_t ping_time = max_pingtime * 1000;
		ma_int32_t sub_dist = subnetdistance;
		(void)ma_repository_check_state(db, repository, &state);
		if(MA_REPOSITORY_STATE_DEFAULT == state) {
			(void)ma_repository_db_get_repository_ping_count(db, repository->name, &ping_time);
			(void)ma_repository_db_get_repository_subnet_distance(db, repository->name, &sub_dist);
		}

		(void)ma_repository_set_pingtime(repository, ping_time) ;
		(void)ma_repository_set_subnetdistance(repository, sub_dist) ;
		(void)ma_repository_set_state(repository, state) ;
		rc = MA_OK;
	}
	return rc;
}

#define CHECK_ANY_SPIPE_SITE_IS_DISABLED_SQL_STMT "SELECT COUNT(*) FROM AGENT_REPOSITORIES WHERE URL_TYPE = 4 AND ENABLED = 0"
ma_error_t ma_repository_db_get_epo_site_enable_state(ma_db_t *db_handle, ma_bool_t *enable_state) {
	ma_error_t rc = MA_OK ;
	ma_db_statement_t *db_stmt = NULL ;

	*enable_state = MA_TRUE ;

	if(MA_OK == (rc = ma_db_statement_create(db_handle, CHECK_ANY_SPIPE_SITE_IS_DISABLED_SQL_STMT, &db_stmt))) {
		ma_db_recordset_t  *db_record = NULL ;

		if(MA_OK == (rc = ma_db_statement_execute_query(db_stmt, &db_record))) {
			if(MA_ERROR_NO_MORE_ITEMS != ma_db_recordset_next(db_record)) {
				ma_int32_t int_value = 0 ;

				(void)ma_db_recordset_get_int(db_record, 1, &int_value) ;
				if(int_value)	*enable_state = MA_FALSE ;
			}
			(void)ma_db_recordset_release(db_record) ; db_record = NULL ;
		}
		(void)ma_db_statement_release(db_stmt) ; db_stmt = NULL ;
	}
	return rc ;

}


#define SET_AH_SITES_ENABLE_STATE_SQL_STMT	"UPDATE AGENT_REPOSITORIES SET ENABLED = ? WHERE URL_TYPE = 4"
ma_error_t ma_repository_db_set_ah_sites_enable_state(ma_db_t *db_handle,  ma_bool_t enable_state) {
	if(db_handle) {
		ma_error_t rc = MA_OK ;
		ma_db_statement_t *db_stmt = NULL ;

		if(MA_OK == (rc = ma_db_statement_create(db_handle, SET_AH_SITES_ENABLE_STATE_SQL_STMT, &db_stmt))) {
			(void)ma_db_statement_set_int(db_stmt, 1, enable_state) ;

			rc = ma_db_statement_execute(db_stmt) ;

			(void)ma_db_statement_release(db_stmt) ; db_stmt = NULL ;
		}
		return rc ; 
	}
	return MA_ERROR_INVALIDARG ;
}

#define IS_SITE_EXISTS_SQL_STMT	"SELECT COUNT(*) FROM AGENT_REPOSITORIES WHERE URL_TYPE = ? AND (SERVER_FQDN = ? COLLATE NOCASE OR SERVER_IP = ? COLLATE NOCASE OR SERVER_NAME = ? COLLATE NOCASE)"
ma_error_t ma_repository_db_is_site_exists(ma_db_t *db_handle, const char *name, ma_repository_url_type_t url_type, ma_bool_t *exists) {
	if(db_handle && name && exists) {
		ma_error_t rc = MA_OK ;
		ma_db_statement_t *db_stmt = NULL ;

		*exists = MA_FALSE ;

		if(MA_OK == (rc = ma_db_statement_create(db_handle, IS_SITE_EXISTS_SQL_STMT, &db_stmt))) {
			ma_db_recordset_t *db_record = NULL ;

			(void)ma_db_statement_set_int(db_stmt, 1, url_type) ;
			(void)ma_db_statement_set_string(db_stmt, 2, 1, name, strlen(name)) ;
			(void)ma_db_statement_set_string(db_stmt, 3, 1, name, strlen(name)) ;
			(void)ma_db_statement_set_string(db_stmt, 4, 1, name, strlen(name)) ;
		
			if(MA_OK == (rc = ma_db_statement_execute_query(db_stmt, &db_record))) {
				ma_int32_t int_value = 0 ;
				
				if(MA_ERROR_NO_MORE_ITEMS != ma_db_recordset_next(db_record)) {
					(void)ma_db_recordset_get_int(db_record, 1, &int_value) ;

					if(int_value)	*exists = MA_TRUE ;
				}
				(void)ma_db_recordset_release(db_record) ; db_record = NULL ;
			}
			(void)ma_db_statement_release(db_stmt) ;	db_stmt = NULL ;
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}
