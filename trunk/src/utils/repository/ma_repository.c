#include "ma/ma_variant.h"
#include "ma/repository/ma_repository.h"
#include "ma/internal/utils/repository/ma_repository_internal.h"
#include "ma/internal/utils/proxy/ma_proxy_internal.h"
#include "ma/ma_buffer.h"


#include <string.h>
#include <stdlib.h>


ma_error_t   ma_repository_create(ma_repository_t **repository){
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	if(repository){
		if(*repository = (ma_repository_t*)calloc(1, sizeof(ma_repository_t))) {
			(*repository)->ref_count = 1;
			rc = MA_OK;
		}
	}
	return rc;
}

ma_error_t   ma_repository_add_ref(ma_repository_t *repository) {
	if(repository) {
        MA_ATOMIC_INCREMENT(repository->ref_count);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t   ma_repository_release(ma_repository_t *repository){
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	if(repository){
		if(0 == MA_ATOMIC_DECREMENT(repository->ref_count)) {
			if(repository->name) free(repository->name);
			if(repository->url_type == MA_REPOSITORY_URL_TYPE_HTTP){
				if(repository->params.http_repository.server_fqdn) free(repository->params.http_repository.server_fqdn);
				if(repository->params.http_repository.path) free(repository->params.http_repository.path);
				if(repository->params.http_repository.auth_user) free(repository->params.http_repository.auth_user);
				if(repository->params.http_repository.auth_passwd) free(repository->params.http_repository.auth_passwd);
			}
			if(repository->url_type == MA_REPOSITORY_URL_TYPE_FTP){ 
				if(repository->params.ftp_repository.server_fqdn) free(repository->params.ftp_repository.server_fqdn);
				if(repository->params.ftp_repository.path) free(repository->params.ftp_repository.path);
				if(repository->params.ftp_repository.auth_user) free(repository->params.ftp_repository.auth_user);
				if(repository->params.ftp_repository.auth_passwd) free(repository->params.ftp_repository.auth_passwd);
			}
			if(repository->url_type == MA_REPOSITORY_URL_TYPE_SPIPE){ 
				if(repository->params.spipe_repository.server_fqdn) free(repository->params.spipe_repository.server_fqdn);
				if(repository->params.spipe_repository.path) free(repository->params.spipe_repository.path);
				if(repository->params.spipe_repository.auth_user) free(repository->params.spipe_repository.auth_user);
				if(repository->params.spipe_repository.auth_passwd) free(repository->params.spipe_repository.auth_passwd);
				if(repository->params.spipe_repository.server_ip) free(repository->params.spipe_repository.server_ip);
				if(repository->params.spipe_repository.server_name) free(repository->params.spipe_repository.server_name);
			}
			if(repository->url_type == MA_REPOSITORY_URL_TYPE_SA){ 
				if(repository->params.sa_repository.server_fqdn) free(repository->params.sa_repository.server_fqdn);
				if(repository->params.sa_repository.path) free(repository->params.sa_repository.path);
				if(repository->params.sa_repository.auth_user) free(repository->params.sa_repository.auth_user);
				if(repository->params.sa_repository.auth_passwd) free(repository->params.sa_repository.auth_passwd);
				if(repository->params.sa_repository.server_ip) free(repository->params.sa_repository.server_ip);
				if(repository->params.sa_repository.server_name) free(repository->params.sa_repository.server_name);
			}
			if(repository->url_type == MA_REPOSITORY_URL_TYPE_UNC){
				if(repository->params.ucn_repository.server_fqdn) free(repository->params.ucn_repository.server_fqdn);
				if(repository->params.ucn_repository.path) free(repository->params.ucn_repository.path);
				if(repository->params.ucn_repository.domain) free(repository->params.ucn_repository.domain);
				if(repository->params.ucn_repository.auth_user) free(repository->params.ucn_repository.auth_user);
				if(repository->params.ucn_repository.auth_passwd) free(repository->params.ucn_repository.auth_passwd);
			}
			if(repository->url_type == MA_REPOSITORY_URL_TYPE_LOCAL){
				if(repository->params.local_repository.path) free(repository->params.local_repository.path);
				if(repository->params.local_repository.domain) free(repository->params.local_repository.domain);
				if(repository->params.local_repository.auth_user) free(repository->params.local_repository.auth_user);
				if(repository->params.local_repository.auth_passwd) free(repository->params.local_repository.auth_passwd);
			}
			free(repository);
			repository = NULL;
		}
		return MA_OK;
	}
	return rc;
}

ma_error_t   ma_repository_set_server_fqdn(ma_repository_t *repository, const char *server_fqdn){
    if(repository && server_fqdn){
		ma_error_t rc = MA_OK ;
		if(repository->url_type == MA_REPOSITORY_URL_TYPE_HTTP){
			if(repository->params.http_repository.server_fqdn) free(repository->params.http_repository.server_fqdn);
			repository->params.http_repository.server_fqdn = strdup(server_fqdn) ;
		}
		else if(repository->url_type == MA_REPOSITORY_URL_TYPE_FTP){
			if(repository->params.ftp_repository.server_fqdn) free(repository->params.ftp_repository.server_fqdn);
			repository->params.ftp_repository.server_fqdn = strdup(server_fqdn ) ;
		}
		else if(repository->url_type == MA_REPOSITORY_URL_TYPE_UNC){
			if(repository->params.ucn_repository.server_fqdn) free(repository->params.ucn_repository.server_fqdn);
			repository->params.ucn_repository.server_fqdn = strdup(server_fqdn) ;
		}
		else if(repository->url_type == MA_REPOSITORY_URL_TYPE_SPIPE){
			if(repository->params.spipe_repository.server_fqdn) free(repository->params.spipe_repository.server_fqdn);
			repository->params.spipe_repository.server_fqdn = strdup(server_fqdn) ;
		}
		else if(repository->url_type == MA_REPOSITORY_URL_TYPE_SA){
			if(repository->params.sa_repository.server_fqdn) free(repository->params.sa_repository.server_fqdn);
			repository->params.sa_repository.server_fqdn = strdup(server_fqdn) ;
		}
		else 
			rc = MA_ERROR_INVALIDARG;

		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t   ma_repository_set_server_path(ma_repository_t *repository, const char *server_path){
    if(repository && server_path){
		ma_error_t rc = MA_OK ;
		if(repository->url_type == MA_REPOSITORY_URL_TYPE_HTTP){
			if(repository->params.http_repository.path) free(repository->params.http_repository.path);
			repository->params.http_repository.path = strdup(server_path) ;
		}
		else if(repository->url_type == MA_REPOSITORY_URL_TYPE_FTP){
			if(repository->params.ftp_repository.path) free(repository->params.ftp_repository.path);
			repository->params.ftp_repository.path = strdup(server_path ) ;
		}
		else if(repository->url_type == MA_REPOSITORY_URL_TYPE_UNC){
			if(repository->params.ucn_repository.path) free(repository->params.ucn_repository.path);
			repository->params.ucn_repository.path = strdup(server_path) ;
		}
		else if(repository->url_type == MA_REPOSITORY_URL_TYPE_LOCAL){
			if(repository->params.local_repository.path) free(repository->params.local_repository.path);
			repository->params.local_repository.path = strdup(server_path) ;
		}
		else if(repository->url_type == MA_REPOSITORY_URL_TYPE_SPIPE){
			if(repository->params.spipe_repository.path) free(repository->params.spipe_repository.path);
			repository->params.spipe_repository.path = strdup(server_path) ;
		}
		else if(repository->url_type == MA_REPOSITORY_URL_TYPE_SA){
			if(repository->params.sa_repository.path) free(repository->params.sa_repository.path);
			repository->params.sa_repository.path = strdup(server_path) ;
		}
		else
			rc = MA_ERROR_INVALIDARG;

        return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t   ma_repository_set_enabled(ma_repository_t *repository, ma_bool_t is_enabled){
	if(repository ){
		repository->enabled = is_enabled;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t   ma_repository_set_authentication(ma_repository_t *repository, ma_repository_auth_t type){
	if(repository ){
		repository->auth_type = type;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_repository_set_namespace(ma_repository_t *repository, ma_repository_namespace_t repo_namespace) {
	if(repository){
		repository->repo_namespace = repo_namespace;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_repository_set_state(ma_repository_t *repository, ma_repository_state_t state) {
	if(repository){
		repository->state = state;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t   ma_repository_set_port(ma_repository_t *repository, ma_uint32_t port){
    if(repository){
		ma_error_t rc = MA_OK ;
		if(repository->url_type == MA_REPOSITORY_URL_TYPE_HTTP){
			repository->params.http_repository.port = port ;
		}
		else if(repository->url_type == MA_REPOSITORY_URL_TYPE_FTP){
			repository->params.ftp_repository.port = port ;
		}
		else if(repository->url_type == MA_REPOSITORY_URL_TYPE_SPIPE){
			repository->params.spipe_repository.port = port ;
		}
		else if(repository->url_type == MA_REPOSITORY_URL_TYPE_SA){
			repository->params.sa_repository.port = port ;
		}
		else
			rc = MA_ERROR_INVALIDARG;

		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t   ma_repository_set_ssl_port(ma_repository_t *repository, ma_uint32_t port){
    if(repository){
		ma_error_t rc = MA_OK ;
		if(repository->url_type == MA_REPOSITORY_URL_TYPE_SPIPE){
			repository->params.spipe_repository.ssl_port = port ;
		}
		else if(repository->url_type == MA_REPOSITORY_URL_TYPE_SA){
			repository->params.sa_repository.ssl_port = port ;
		}
		else
			rc = MA_ERROR_INVALIDARG;

		return rc;
	}
	return MA_ERROR_INVALIDARG;
}


ma_error_t   ma_repository_set_user_name(ma_repository_t *repository, const char *user_name){
	if(repository && user_name){
		ma_error_t rc = MA_OK ;
		if(repository->url_type == MA_REPOSITORY_URL_TYPE_HTTP){
			if(repository->params.http_repository.auth_user) free(repository->params.http_repository.auth_user);
			repository->params.http_repository.auth_user = strdup(user_name);
		}
		else if(repository->url_type == MA_REPOSITORY_URL_TYPE_FTP){
			if(repository->params.ftp_repository.auth_user) free(repository->params.ftp_repository.auth_user);
			repository->params.ftp_repository.auth_user = strdup(user_name);
		}
		else if(repository->url_type == MA_REPOSITORY_URL_TYPE_UNC){
			if(repository->params.ucn_repository.auth_user) free(repository->params.ucn_repository.auth_user);
			repository->params.ucn_repository.auth_user = strdup(user_name);
		}
		else if(repository->url_type == MA_REPOSITORY_URL_TYPE_LOCAL){
			if(repository->params.local_repository.auth_user) free(repository->params.local_repository.auth_user);
			repository->params.local_repository.auth_user = strdup(user_name);
		}
		else if(repository->url_type == MA_REPOSITORY_URL_TYPE_SPIPE){
			if(repository->params.spipe_repository.auth_user) free(repository->params.spipe_repository.auth_user);
			repository->params.spipe_repository.auth_user = strdup(user_name);
		}
		else if(repository->url_type == MA_REPOSITORY_URL_TYPE_SA){
			if(repository->params.sa_repository.auth_user) free(repository->params.sa_repository.auth_user);
			repository->params.sa_repository.auth_user = strdup(user_name);
		}
		else
			rc = MA_ERROR_INVALIDARG;

		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t   ma_repository_set_domain_name(ma_repository_t *repository, const char *domain_name){
	if(repository && domain_name){
		ma_error_t rc = MA_OK ;
		if(repository->url_type == MA_REPOSITORY_URL_TYPE_UNC){
			if(repository->params.ucn_repository.domain) free(repository->params.ucn_repository.domain);
			repository->params.ucn_repository.domain = strdup(domain_name);
		}
		else if(repository->url_type == MA_REPOSITORY_URL_TYPE_LOCAL){
			if(repository->params.local_repository.domain) free(repository->params.local_repository.domain);
			repository->params.local_repository.domain = strdup(domain_name);
		}
		else
			rc = MA_ERROR_INVALIDARG;

		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t   ma_repository_set_password(ma_repository_t *repository, const char *password){
	if(repository){
		ma_error_t rc = MA_OK ;
		if(repository->url_type == MA_REPOSITORY_URL_TYPE_HTTP){
			if(repository->params.http_repository.auth_passwd) free(repository->params.http_repository.auth_passwd);
			repository->params.http_repository.auth_passwd = ((password != NULL) ? strdup(password) : NULL) ;
		}
		else if(repository->url_type == MA_REPOSITORY_URL_TYPE_SPIPE){
			if(repository->params.spipe_repository.auth_passwd) free(repository->params.spipe_repository.auth_passwd);
			repository->params.spipe_repository.auth_passwd = ((password != NULL) ? strdup(password) : NULL) ;
		}
		else if(repository->url_type == MA_REPOSITORY_URL_TYPE_SA){
			if(repository->params.sa_repository.auth_passwd) free(repository->params.sa_repository.auth_passwd);
			repository->params.sa_repository.auth_passwd = ((password != NULL) ? strdup(password) : NULL) ;
		}
		else if(repository->url_type == MA_REPOSITORY_URL_TYPE_FTP){
			if(repository->params.ftp_repository.auth_passwd) free(repository->params.ftp_repository.auth_passwd);
			repository->params.ftp_repository.auth_passwd = ((password != NULL) ? strdup(password) : NULL) ;
		}
		else if(repository->url_type == MA_REPOSITORY_URL_TYPE_UNC){
			if(repository->params.ucn_repository.auth_passwd) free(repository->params.ucn_repository.auth_passwd);
			repository->params.ucn_repository.auth_passwd = ((password != NULL) ? strdup(password) : NULL) ;
		}
		else if(repository->url_type == MA_REPOSITORY_URL_TYPE_LOCAL){
			if(repository->params.local_repository.auth_passwd) free(repository->params.local_repository.auth_passwd);
			repository->params.local_repository.auth_passwd = ((password != NULL) ? strdup(password) : NULL) ;
		}
		else
			rc = MA_ERROR_INVALIDARG;

		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t   ma_repository_set_pingtime(ma_repository_t *repository, ma_uint32_t pingtime){
	if(repository){
		repository->ping_time = pingtime;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t   ma_repository_set_subnetdistance(ma_repository_t *repository, ma_uint32_t subnetdistance){
	if(repository){
		repository->subnet_distance = subnetdistance;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t   ma_repository_set_siteorder(ma_repository_t *repository, ma_uint32_t siteorder){
	if(repository){
		repository->sitelist_order = siteorder;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t   ma_repository_set_type(ma_repository_t *repository, ma_repository_type_t repository_type){
	if(repository){
		repository->repo_type = repository_type;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t   ma_repository_set_url_type(ma_repository_t *repository, ma_repository_url_type_t url_type){
	if(repository){
		repository->url_type = url_type;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t   ma_repository_set_name(ma_repository_t *repository, const char *repository_name){
	if(repository && repository_name){
		if(repository->name) free(repository->name);
		repository->name = strdup(repository_name);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t   ma_repository_set_server_ip(ma_repository_t *repository, const char *server_ip){
	if(repository && server_ip){
		ma_error_t rc = MA_OK ;
		if(repository->url_type == MA_REPOSITORY_URL_TYPE_SPIPE){
			if(repository->params.spipe_repository.server_ip) free(repository->params.spipe_repository.server_ip);
			repository->params.spipe_repository.server_ip = strdup(server_ip);
			rc = MA_OK;
		}
		else if(repository->url_type == MA_REPOSITORY_URL_TYPE_SA){
			if(repository->params.sa_repository.server_ip) free(repository->params.sa_repository.server_ip);
			repository->params.sa_repository.server_ip = strdup(server_ip);
			rc = MA_OK;
		}
		else
			rc = MA_ERROR_INVALIDARG;

		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t   ma_repository_set_server_name(ma_repository_t *repository, const char *server_name){
	if(repository && server_name){
		ma_error_t rc = MA_OK ;
		if(repository->url_type == MA_REPOSITORY_URL_TYPE_SPIPE){
			if(repository->params.spipe_repository.server_name) free(repository->params.spipe_repository.server_name);
			repository->params.spipe_repository.server_name = strdup(server_name);
			rc = MA_OK;
		}
		else if(repository->url_type == MA_REPOSITORY_URL_TYPE_SA){
			if(repository->params.sa_repository.server_name) free(repository->params.sa_repository.server_name);
			repository->params.sa_repository.server_name = strdup(server_name);
			rc = MA_OK;
		}
		else
			rc = MA_ERROR_INVALIDARG;

		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t   ma_repository_set_proxy_usage_type(ma_repository_t *repository, ma_proxy_usage_type_t proxy_usage){
	if(repository){
		repository->proxy_usage = proxy_usage;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t   ma_repository_get_proxy_usage_type(ma_repository_t *repository, ma_proxy_usage_type_t *proxy_usage){
	if(repository && proxy_usage){
		*proxy_usage = repository->proxy_usage;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t   ma_repository_get_pingtime(ma_repository_t *repository, ma_uint32_t *pingtime){
	if(repository && pingtime){
		*pingtime = repository->ping_time;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t   ma_repository_get_siteorder(ma_repository_t *repository, ma_uint32_t *siteorder){
	if(repository && siteorder){
		*siteorder = repository->sitelist_order;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t   ma_repository_get_subnetdistance(ma_repository_t *repository, ma_uint32_t *subnetdistance){
	if(repository && subnetdistance){
		*subnetdistance = repository->subnet_distance;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t   ma_repository_get_name(ma_repository_t *repository, const char **repository_name){
	if(repository && repository_name){
		*repository_name = repository->name;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}


ma_error_t   ma_repository_get_type(ma_repository_t *repository, ma_repository_type_t *repository_type){
	if(repository && repository_type){
		*repository_type = repository->repo_type;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t   ma_repository_get_url_type(ma_repository_t *repository, ma_repository_url_type_t *url_type){
	if(repository && url_type){
		*url_type = repository->url_type;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t	ma_repository_get_server_ip(ma_repository_t *repository, const char **server_ip){
	if(repository && server_ip){
		ma_error_t rc = MA_OK ;
		if(repository->url_type == MA_REPOSITORY_URL_TYPE_SPIPE && repository->params.spipe_repository.server_ip){
			*server_ip = repository->params.spipe_repository.server_ip;
			rc = MA_OK;
		}
		else if(repository->url_type == MA_REPOSITORY_URL_TYPE_SA && repository->params.sa_repository.server_ip){
			*server_ip = repository->params.sa_repository.server_ip;
			rc = MA_OK;
		}
		else
			rc = MA_ERROR_INVALIDARG;

		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t   ma_repository_get_namespace(ma_repository_t *repository, ma_repository_namespace_t *repo_namespace){
	if(repository && repo_namespace){
		*repo_namespace = repository->repo_namespace;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t   ma_repository_get_state(ma_repository_t *repository, ma_repository_state_t *state){
	if(repository && state){
		*state = repository->state;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}


ma_error_t   ma_repository_get_authentication(ma_repository_t *repository, ma_repository_auth_t *type){
	if(repository && type){
		*type = repository->auth_type;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t   ma_repository_get_server_fqdn(ma_repository_t *repository, const char **server_fqdn){
    if(repository && server_fqdn){
		ma_error_t rc = MA_OK ;
		if((repository->url_type == MA_REPOSITORY_URL_TYPE_HTTP) && (repository->params.http_repository.server_fqdn)){
			*server_fqdn = repository->params.http_repository.server_fqdn;
            rc = MA_OK;
		}
		else if((repository->url_type == MA_REPOSITORY_URL_TYPE_SPIPE) && (repository->params.spipe_repository.server_fqdn)){
			*server_fqdn = repository->params.spipe_repository.server_fqdn;
            rc = MA_OK;
		}
		else if((repository->url_type == MA_REPOSITORY_URL_TYPE_SA) && (repository->params.sa_repository.server_fqdn)){
			*server_fqdn = repository->params.sa_repository.server_fqdn;
			rc = MA_OK;
		}
		else if((repository->url_type == MA_REPOSITORY_URL_TYPE_FTP) && (repository->params.ftp_repository.server_fqdn)){
			*server_fqdn = repository->params.ftp_repository.server_fqdn;
            rc = MA_OK;
		}
		else if((repository->url_type == MA_REPOSITORY_URL_TYPE_UNC) && (repository->params.ucn_repository.server_fqdn)){
			*server_fqdn = repository->params.ucn_repository.server_fqdn;
            rc = MA_OK;
		}
		else
			rc = MA_ERROR_INVALIDARG;

		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t   ma_repository_get_server_name(ma_repository_t *repository, const char **server_name){
    if(repository && server_name){
		ma_error_t rc = MA_OK ;
		if((repository->url_type == MA_REPOSITORY_URL_TYPE_SPIPE) && (repository->params.spipe_repository.server_name)){
			*server_name = repository->params.spipe_repository.server_name;
            rc = MA_OK;
		}
		else if((repository->url_type == MA_REPOSITORY_URL_TYPE_SA) && (repository->params.sa_repository.server_name)){
			*server_name = repository->params.sa_repository.server_name;
			rc = MA_OK;
		}
		else
			rc = MA_ERROR_INVALIDARG;

		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t   ma_repository_get_server_path(ma_repository_t *repository, const char **server_path){
    if(repository && server_path){
		ma_error_t rc = MA_OK ;
		if((repository->url_type == MA_REPOSITORY_URL_TYPE_HTTP) && (repository->params.http_repository.path)){
			*server_path = repository->params.http_repository.path;
            rc = MA_OK;
		}
		else if((repository->url_type == MA_REPOSITORY_URL_TYPE_SPIPE) && (repository->params.spipe_repository.path)){
			*server_path = repository->params.spipe_repository.path;
            rc = MA_OK;
		}
		else if((repository->url_type == MA_REPOSITORY_URL_TYPE_SA) && (repository->params.sa_repository.path)){
			*server_path = repository->params.sa_repository.path;
			rc = MA_OK;
		}
		else if((repository->url_type == MA_REPOSITORY_URL_TYPE_FTP) && (repository->params.ftp_repository.path)){
			*server_path = repository->params.ftp_repository.path;
            rc = MA_OK;
		}
		else if((repository->url_type == MA_REPOSITORY_URL_TYPE_UNC) && (repository->params.ucn_repository.path)){
			*server_path = repository->params.ucn_repository.path;
            rc = MA_OK;
		}
		else if((repository->url_type == MA_REPOSITORY_URL_TYPE_LOCAL) && (repository->params.local_repository.path)){
			*server_path = repository->params.local_repository.path;
            rc = MA_OK;
		}
		else
			rc = MA_ERROR_INVALIDARG;

		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t   ma_repository_get_port(ma_repository_t *repository, ma_uint32_t *port){
	if(repository && port ) {
		ma_error_t rc = MA_OK ;
        if(repository->url_type == MA_REPOSITORY_URL_TYPE_HTTP)
		    *port = repository->params.http_repository.port ;
        else if(repository->url_type == MA_REPOSITORY_URL_TYPE_FTP)
            *port = repository->params.ftp_repository.port ;
        else if(repository->url_type == MA_REPOSITORY_URL_TYPE_SPIPE)
            *port = repository->params.spipe_repository.port ;
		else if(repository->url_type == MA_REPOSITORY_URL_TYPE_SA)
			*port = repository->params.sa_repository.port ;
		else
			rc = MA_ERROR_INVALIDARG;

		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t   ma_repository_get_ssl_port(ma_repository_t *repository, ma_uint32_t *ssl_port){
	if(repository && ssl_port ) {
		ma_error_t rc = MA_OK ;
		if(repository->url_type == MA_REPOSITORY_URL_TYPE_SPIPE) {
            *ssl_port = repository->params.spipe_repository.ssl_port ;		
        }
		else if(repository->url_type == MA_REPOSITORY_URL_TYPE_SA) {
			*ssl_port = repository->params.sa_repository.ssl_port ;		
		}
		else
			rc = MA_ERROR_INVALIDARG;

		return rc;
	}
	return MA_ERROR_INVALIDARG;
}


ma_error_t   ma_repository_get_enabled(ma_repository_t *repository, ma_bool_t *is_enabled){
	if(repository && is_enabled){
		*is_enabled = repository->enabled;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t   ma_repository_get_user_name(ma_repository_t *repository, const char **user_name){
	if(repository && user_name){
		ma_error_t rc = MA_OK ;
		if((repository->url_type == MA_REPOSITORY_URL_TYPE_HTTP) && (repository->params.http_repository.auth_user)){
			*user_name = repository->params.http_repository.auth_user;
		}
		else if((repository->url_type == MA_REPOSITORY_URL_TYPE_SPIPE) && (repository->params.spipe_repository.auth_user)){
			*user_name = repository->params.spipe_repository.auth_user;
		}
		else if((repository->url_type == MA_REPOSITORY_URL_TYPE_SA) && (repository->params.sa_repository.auth_user)){
			*user_name = repository->params.sa_repository.auth_user;
		}
		else if((repository->url_type == MA_REPOSITORY_URL_TYPE_FTP) && (repository->params.ftp_repository.auth_user)){
			*user_name = repository->params.ftp_repository.auth_user;
		}
		else if((repository->url_type == MA_REPOSITORY_URL_TYPE_UNC) && (repository->params.ucn_repository.auth_user)){
			*user_name = repository->params.ucn_repository.auth_user;
		}
		else if((repository->url_type == MA_REPOSITORY_URL_TYPE_LOCAL) && (repository->params.local_repository.auth_user)){
			*user_name = repository->params.local_repository.auth_user;
		}
		else
			rc = MA_ERROR_INVALIDARG;

		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t   ma_repository_get_password(ma_repository_t *repository, const char **password){
	if(repository && password){
		ma_error_t rc = MA_OK ;
		if(repository->url_type == MA_REPOSITORY_URL_TYPE_HTTP && repository->params.http_repository.auth_passwd){
			*password = repository->params.http_repository.auth_passwd;
		}
		else if((repository->url_type == MA_REPOSITORY_URL_TYPE_SPIPE) && (repository->params.spipe_repository.auth_passwd)){
			*password = repository->params.spipe_repository.auth_passwd;
		}
		else if((repository->url_type == MA_REPOSITORY_URL_TYPE_SA) && (repository->params.sa_repository.auth_passwd)){
			*password = repository->params.sa_repository.auth_passwd;
		}
		else if((repository->url_type == MA_REPOSITORY_URL_TYPE_FTP) && (repository->params.ftp_repository.auth_passwd)){
			*password = repository->params.ftp_repository.auth_passwd;
		}
		else if((repository->url_type == MA_REPOSITORY_URL_TYPE_UNC) && (repository->params.ucn_repository.auth_passwd)){
			*password = repository->params.ucn_repository.auth_passwd;
		}
		else if((repository->url_type == MA_REPOSITORY_URL_TYPE_LOCAL) && (repository->params.local_repository.auth_passwd)){
			*password = repository->params.local_repository.auth_passwd;
		}
		else {
			*password = NULL;
			rc = MA_ERROR_INVALIDARG;
		}

		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t   ma_repository_get_domain_name(ma_repository_t *repository, const char **domain_name){
	if(repository && domain_name){
		ma_error_t rc = MA_OK ;
		if((repository->url_type == MA_REPOSITORY_URL_TYPE_UNC) && (repository->params.ucn_repository.domain)){
			*domain_name = repository->params.ucn_repository.domain;
		}
		else if((repository->url_type == MA_REPOSITORY_URL_TYPE_LOCAL) && (repository->params.local_repository.domain)){
			*domain_name = repository->params.local_repository.domain;
		}
		else
			rc = MA_ERROR_INVALIDARG;

		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t   ma_repository_validate(ma_repository_t *repository){
	if(repository){
		if((!repository->name) || (repository->url_type < 0) || (repository->url_type >= MA_REPOSITORY_URL_TYPE_END) || (repository->repo_type < 0) || (repository->repo_type >= MA_REPOSITORY_TYPE_END) ||
			(repository->auth_type < 0) || (repository->auth_type >= MA_REPOSITORY_AUTH_END) ||	(repository->repo_namespace < 0) || (repository->repo_namespace >= MA_REPOSITORY_NAMESPACE_END) || 
			(repository->proxy_usage < 0) || (repository->proxy_usage >= MA_PROXY_USAGE_TYPE_END)){
				return MA_ERROR_INVALIDARG;
		}
		if(repository->url_type == MA_REPOSITORY_URL_TYPE_HTTP){
			if(!repository->params.http_repository.server_fqdn) 
				return MA_ERROR_INVALIDARG;
			if(repository->auth_type == MA_REPOSITORY_AUTH_CREDENTIALS){
				if(!repository->params.http_repository.auth_user)
					return MA_ERROR_INVALIDARG;
			}
			return MA_OK;
		}
		if(repository->url_type == MA_REPOSITORY_URL_TYPE_SPIPE){
			if(!repository->params.spipe_repository.server_fqdn) 
				return MA_ERROR_INVALIDARG;
			if(repository->auth_type == MA_REPOSITORY_AUTH_CREDENTIALS){
				if(!repository->params.spipe_repository.auth_user)
					return MA_ERROR_INVALIDARG;
			}
			return MA_OK;
		}
		if(repository->url_type == MA_REPOSITORY_URL_TYPE_SA){
			if(!repository->params.sa_repository.server_fqdn) 
				return MA_ERROR_INVALIDARG;
			if(repository->auth_type == MA_REPOSITORY_AUTH_CREDENTIALS){
				if(!repository->params.sa_repository.auth_user)
					return MA_ERROR_INVALIDARG;
			}
			return MA_OK;
		}
		if(repository->url_type == MA_REPOSITORY_URL_TYPE_FTP){
			if(!repository->params.ftp_repository.server_fqdn) 
				return MA_ERROR_INVALIDARG;
			if(repository->auth_type == MA_REPOSITORY_AUTH_CREDENTIALS){
				if(!repository->params.ftp_repository.auth_user)
					return MA_ERROR_INVALIDARG;
			}
			return MA_OK;
		}
		if(repository->url_type == MA_REPOSITORY_URL_TYPE_UNC){
			if(!repository->params.ucn_repository.server_fqdn) 
				return MA_ERROR_INVALIDARG;
			if(repository->auth_type == MA_REPOSITORY_AUTH_CREDENTIALS){
				if((!repository->params.ucn_repository.auth_user) || (!repository->params.ucn_repository.domain))
					return MA_ERROR_INVALIDARG;
			}
			return MA_OK;
		}
		if(repository->url_type == MA_REPOSITORY_URL_TYPE_LOCAL){
			if(!repository->params.local_repository.path) 
				return MA_ERROR_INVALIDARG;
			if(repository->auth_type == MA_REPOSITORY_AUTH_CREDENTIALS){
				if((!repository->params.local_repository.auth_user) || (!repository->params.local_repository.domain))
					return MA_ERROR_INVALIDARG;
			}
			return MA_OK;
		}
	}
	return MA_ERROR_INVALIDARG;

}

/*TBD: Use regular expression, instead of strstr */
ma_error_t   ma_repository_excusion_check(char *repo_name, ma_proxy_config_t *configuration, ma_bool_t *is_excuded){
	if(repo_name && configuration && configuration->exclusion_list){
		if(strstr(configuration->exclusion_list, repo_name))
			*is_excuded = MA_TRUE;
		else
			*is_excuded = MA_FALSE;

		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t   ma_repository_as_variant(ma_repository_t *repository, ma_variant_t **repo_variant){
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	
	if(repository && repository->name){
		ma_variant_t *value = NULL;
		ma_table_t *repo_tb = NULL;
		if(MA_OK == (rc = ma_table_create(&repo_tb))) {
			if(MA_OK == (rc = ma_variant_create_from_string(repository->name, strlen(repository->name), &value))) {
				rc = ma_table_add_entry(repo_tb, "name", value);
				(void)ma_variant_release(value); value = NULL;
			}

			if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_int32(repository->repo_type, &value)))) {
				rc = ma_table_add_entry(repo_tb, "repo_type", value);
				(void)ma_variant_release(value); value = NULL;
			}

			if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_int32(repository->url_type, &value)))) {
				rc = ma_table_add_entry(repo_tb, "url_type", value);
				(void)ma_variant_release(value); value = NULL;
			}

			if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_int32(repository->auth_type, &value)))) {
				rc = ma_table_add_entry(repo_tb, "auth_type", value);
				(void)ma_variant_release(value); value = NULL;
			}

			if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_int32(repository->repo_namespace, &value)))) {
				rc = ma_table_add_entry(repo_tb, "repo_namespace", value);
				(void)ma_variant_release(value); value = NULL;
			}

			if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_bool(repository->enabled, &value)))) {
				rc = ma_table_add_entry(repo_tb, "enabled", value);
				(void)ma_variant_release(value); value = NULL;
			}

			if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_int32(repository->state, &value)))) {
				rc = ma_table_add_entry(repo_tb, "state", value);
				(void)ma_variant_release(value); value = NULL;
			}

			if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_int32(repository->ping_time, &value)))) {
				rc = ma_table_add_entry(repo_tb, "ping_time", value);
				(void)ma_variant_release(value); value = NULL;
			}

			if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_int32(repository->subnet_distance, &value)))) {
				rc = ma_table_add_entry(repo_tb, "subnet_distance", value);
				(void)ma_variant_release(value); value = NULL;
			}

			if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_int32(repository->sitelist_order, &value)))) {
				rc = ma_table_add_entry(repo_tb, "sitelist_order", value);
				(void)ma_variant_release(value); value = NULL;
			}

			if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_int32(repository->proxy_usage, &value)))) {
				rc = ma_table_add_entry(repo_tb, "proxy_usage", value);
				(void)ma_variant_release(value); value = NULL;
			}

			if(repository->url_type == MA_REPOSITORY_URL_TYPE_HTTP){
				if((MA_OK == rc) && (repository->params.http_repository.server_fqdn) && (MA_OK == (rc = ma_variant_create_from_string(repository->params.http_repository.server_fqdn, strlen(repository->params.http_repository.server_fqdn), &value)))) {
					rc = ma_table_add_entry(repo_tb, "server_fqdn", value);
					(void)ma_variant_release(value); value = NULL;
				}

				if((MA_OK == rc) && (repository->params.http_repository.path) && (MA_OK == (rc = ma_variant_create_from_string(repository->params.http_repository.path, strlen(repository->params.http_repository.path), &value)))) {
					rc = ma_table_add_entry(repo_tb, "path", value);
					(void)ma_variant_release(value); value = NULL;
				}

				if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_int32(repository->params.http_repository.port, &value)))) {
					rc = ma_table_add_entry(repo_tb, "port", value);
					(void)ma_variant_release(value); value = NULL;
				}

				if((MA_OK == rc) && (repository->params.http_repository.auth_user) && (MA_OK == (rc = ma_variant_create_from_string(repository->params.http_repository.auth_user, strlen(repository->params.http_repository.auth_user), &value)))) {
					rc = ma_table_add_entry(repo_tb, "auth_user", value);
					(void)ma_variant_release(value); value = NULL;
				}

				if((MA_OK == rc) && (repository->params.http_repository.auth_passwd) && (MA_OK == (rc = ma_variant_create_from_string(repository->params.http_repository.auth_passwd, strlen(repository->params.http_repository.auth_passwd), &value)))) {
					rc = ma_table_add_entry(repo_tb, "auth_passwd", value);
					(void)ma_variant_release(value); value = NULL;
				}
			}

			if(repository->url_type == MA_REPOSITORY_URL_TYPE_FTP){
				if((MA_OK == rc) && (repository->params.ftp_repository.server_fqdn) && (MA_OK == (rc = ma_variant_create_from_string(repository->params.ftp_repository.server_fqdn, strlen(repository->params.ftp_repository.server_fqdn), &value)))) {
					rc = ma_table_add_entry(repo_tb, "server_fqdn", value);
					(void)ma_variant_release(value); value = NULL;
				}

				if((MA_OK == rc) && (repository->params.ftp_repository.path) && (MA_OK == (rc = ma_variant_create_from_string(repository->params.ftp_repository.path, strlen(repository->params.ftp_repository.path), &value)))) {
					rc = ma_table_add_entry(repo_tb, "path", value);
					(void)ma_variant_release(value); value = NULL;
				}

				if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_int32(repository->params.ftp_repository.port, &value)))) {
					rc = ma_table_add_entry(repo_tb, "port", value);
					(void)ma_variant_release(value); value = NULL;
				}

				if((MA_OK == rc) && (repository->params.ftp_repository.auth_user) && (MA_OK == (rc = ma_variant_create_from_string(repository->params.ftp_repository.auth_user, strlen(repository->params.ftp_repository.auth_user), &value)))) {
					rc = ma_table_add_entry(repo_tb, "auth_user", value);
					(void)ma_variant_release(value); value = NULL;
				}

				if((MA_OK == rc) && (repository->params.ftp_repository.auth_passwd) && (MA_OK == (rc = ma_variant_create_from_string(repository->params.ftp_repository.auth_passwd, strlen(repository->params.ftp_repository.auth_passwd), &value)))) {
					rc = ma_table_add_entry(repo_tb, "auth_passwd", value);
					(void)ma_variant_release(value); value = NULL;
				}
			}

			if(repository->url_type == MA_REPOSITORY_URL_TYPE_SPIPE){
				if((MA_OK == rc) && (repository->params.spipe_repository.server_fqdn) && (MA_OK == (rc = ma_variant_create_from_string(repository->params.spipe_repository.server_fqdn, strlen(repository->params.spipe_repository.server_fqdn), &value)))) {
					rc = ma_table_add_entry(repo_tb, "server_fqdn", value);
					(void)ma_variant_release(value); value = NULL;
				}

				if((MA_OK == rc) && (repository->params.spipe_repository.server_ip) && (MA_OK == (rc = ma_variant_create_from_string(repository->params.spipe_repository.server_ip, strlen(repository->params.spipe_repository.server_ip), &value)))) {
					rc = ma_table_add_entry(repo_tb, "server_ip", value);
					(void)ma_variant_release(value); value = NULL;
				}

				if((MA_OK == rc) && (repository->params.spipe_repository.server_name) && (MA_OK == (rc = ma_variant_create_from_string(repository->params.spipe_repository.server_name, strlen(repository->params.spipe_repository.server_name), &value)))) {
					rc = ma_table_add_entry(repo_tb, "server_name", value);
					(void)ma_variant_release(value); value = NULL;
				}

				if((MA_OK == rc) && (repository->params.spipe_repository.path) && (MA_OK == (rc = ma_variant_create_from_string(repository->params.spipe_repository.path, strlen(repository->params.spipe_repository.path), &value)))) {
					rc = ma_table_add_entry(repo_tb, "path", value);
					(void)ma_variant_release(value); value = NULL;
				}

				if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_int32(repository->params.spipe_repository.port, &value)))) {
					rc = ma_table_add_entry(repo_tb, "port", value);
					(void)ma_variant_release(value); value = NULL;
				}

				if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_int32(repository->params.spipe_repository.ssl_port, &value)))) {
					rc = ma_table_add_entry(repo_tb, "ssl_port", value);
					(void)ma_variant_release(value); value = NULL;
				}

				if((MA_OK == rc) && (repository->params.spipe_repository.auth_user) && (MA_OK == (rc = ma_variant_create_from_string(repository->params.spipe_repository.auth_user, strlen(repository->params.spipe_repository.auth_user), &value)))) {
					rc = ma_table_add_entry(repo_tb, "auth_user", value);
					(void)ma_variant_release(value); value = NULL;
				}

				if((MA_OK == rc) && (repository->params.spipe_repository.auth_passwd) && (MA_OK == (rc = ma_variant_create_from_string(repository->params.spipe_repository.auth_passwd, strlen(repository->params.spipe_repository.auth_passwd), &value)))) {
					rc = ma_table_add_entry(repo_tb, "auth_passwd", value);
					(void)ma_variant_release(value); value = NULL;
				}
			}

			if(repository->url_type == MA_REPOSITORY_URL_TYPE_SA){
				if((MA_OK == rc) && (repository->params.sa_repository.server_fqdn) && (MA_OK == (rc = ma_variant_create_from_string(repository->params.sa_repository.server_fqdn, strlen(repository->params.sa_repository.server_fqdn), &value)))) {
					rc = ma_table_add_entry(repo_tb, "server_fqdn", value);
					(void)ma_variant_release(value); value = NULL;
				}

				if((MA_OK == rc) && (repository->params.sa_repository.server_ip) && (MA_OK == (rc = ma_variant_create_from_string(repository->params.sa_repository.server_ip, strlen(repository->params.sa_repository.server_ip), &value)))) {
					rc = ma_table_add_entry(repo_tb, "server_ip", value);
					(void)ma_variant_release(value); value = NULL;
				}

				if((MA_OK == rc) && (repository->params.sa_repository.server_name) && (MA_OK == (rc = ma_variant_create_from_string(repository->params.sa_repository.server_name, strlen(repository->params.sa_repository.server_name), &value)))) {
					rc = ma_table_add_entry(repo_tb, "server_name", value);
					(void)ma_variant_release(value); value = NULL;
				}

				if((MA_OK == rc) && (repository->params.sa_repository.path) && (MA_OK == (rc = ma_variant_create_from_string(repository->params.sa_repository.path, strlen(repository->params.sa_repository.path), &value)))) {
					rc = ma_table_add_entry(repo_tb, "path", value);
					(void)ma_variant_release(value); value = NULL;
				}

				if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_int32(repository->params.sa_repository.port, &value)))) {
					rc = ma_table_add_entry(repo_tb, "port", value);
					(void)ma_variant_release(value); value = NULL;
				}

				if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_int32(repository->params.sa_repository.ssl_port, &value)))) {
					rc = ma_table_add_entry(repo_tb, "ssl_port", value);
					(void)ma_variant_release(value); value = NULL;
				}

				if((MA_OK == rc) && (repository->params.sa_repository.auth_user) && (MA_OK == (rc = ma_variant_create_from_string(repository->params.sa_repository.auth_user, strlen(repository->params.sa_repository.auth_user), &value)))) {
					rc = ma_table_add_entry(repo_tb, "auth_user", value);
					(void)ma_variant_release(value); value = NULL;
				}

				if((MA_OK == rc) && (repository->params.sa_repository.auth_passwd) && (MA_OK == (rc = ma_variant_create_from_string(repository->params.sa_repository.auth_passwd, strlen(repository->params.sa_repository.auth_passwd), &value)))) {
					rc = ma_table_add_entry(repo_tb, "auth_passwd", value);
					(void)ma_variant_release(value); value = NULL;
				}
			}

			if(repository->url_type == MA_REPOSITORY_URL_TYPE_UNC){
				if((MA_OK == rc) && (repository->params.ucn_repository.server_fqdn) && (MA_OK == (rc = ma_variant_create_from_string(repository->params.ucn_repository.server_fqdn, strlen(repository->params.ucn_repository.server_fqdn), &value)))) {
					rc = ma_table_add_entry(repo_tb, "server_fqdn", value);
					(void)ma_variant_release(value); value = NULL;
				}

				if((MA_OK == rc) && (repository->params.ucn_repository.path) && (MA_OK == (rc = ma_variant_create_from_string(repository->params.ucn_repository.path, strlen(repository->params.ucn_repository.path), &value)))) {
					rc = ma_table_add_entry(repo_tb, "path", value);
					(void)ma_variant_release(value); value = NULL;
				}

				if((MA_OK == rc) && (repository->params.ucn_repository.domain) && (MA_OK == (rc = ma_variant_create_from_string(repository->params.ucn_repository.domain, strlen(repository->params.ucn_repository.domain), &value)))) {
					rc = ma_table_add_entry(repo_tb, "domain", value);
					(void)ma_variant_release(value); value = NULL;
				}

				if((MA_OK == rc) && (repository->params.ucn_repository.auth_user) && (MA_OK == (rc = ma_variant_create_from_string(repository->params.ucn_repository.auth_user, strlen(repository->params.ucn_repository.auth_user), &value)))) {
					rc = ma_table_add_entry(repo_tb, "auth_user", value);
					(void)ma_variant_release(value); value = NULL;
				}

				if((MA_OK == rc) && (repository->params.ucn_repository.auth_passwd) && (MA_OK == (rc = ma_variant_create_from_string(repository->params.ucn_repository.auth_passwd, strlen(repository->params.ucn_repository.auth_passwd), &value)))) {
					rc = ma_table_add_entry(repo_tb, "auth_passwd", value);
					(void)ma_variant_release(value); value = NULL;
				}
			}

			if(repository->url_type == MA_REPOSITORY_URL_TYPE_LOCAL){
				if((MA_OK == rc) && (repository->params.local_repository.path) && (MA_OK == (rc = ma_variant_create_from_string(repository->params.local_repository.path, strlen(repository->params.local_repository.path), &value)))) {
					rc = ma_table_add_entry(repo_tb, "path", value);
					(void)ma_variant_release(value); value = NULL;
				}

				if((MA_OK == rc) && (repository->params.local_repository.domain) && (MA_OK == (rc = ma_variant_create_from_string(repository->params.local_repository.domain, strlen(repository->params.local_repository.domain), &value)))) {
					rc = ma_table_add_entry(repo_tb, "domain", value);
					(void)ma_variant_release(value); value = NULL;
				}

				if((MA_OK == rc) && (repository->params.local_repository.auth_user) && (MA_OK == (rc = ma_variant_create_from_string(repository->params.local_repository.auth_user, strlen(repository->params.local_repository.auth_user), &value)))) {
					rc = ma_table_add_entry(repo_tb, "auth_user", value);
					(void)ma_variant_release(value); value = NULL;
				}

				if((MA_OK == rc) && (repository->params.local_repository.auth_passwd) && (MA_OK == (rc = ma_variant_create_from_string(repository->params.local_repository.auth_passwd, strlen(repository->params.local_repository.auth_passwd), &value)))) {
					rc = ma_table_add_entry(repo_tb, "auth_passwd", value);
					(void)ma_variant_release(value); value = NULL;
				}

			}

			if(MA_OK == rc) 
			    rc = ma_variant_create_from_table(repo_tb, repo_variant);
			
			(void)ma_table_release(repo_tb);
		}
	}
	return rc;
}

static ma_error_t  get_value_from_table(ma_table_t *repo_tb, const char *key, char **value){
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	if(repo_tb && key && value){
		const char *temp_str = NULL;
		size_t temp_str_len;
		ma_buffer_t *buff = NULL;
		ma_variant_t *variant = NULL;
		if(MA_OK == (rc = ma_table_get_value(repo_tb, key, &variant))){
			if(MA_OK == (rc = ma_variant_get_string_buffer(variant, &buff))) {
				if(MA_OK == (rc = ma_buffer_get_string(buff, &temp_str, &temp_str_len))) {
					*value = strdup(temp_str);						
				}
				ma_buffer_release(buff);
			}
			ma_variant_release(variant);
		}
	}
	return rc;
}

ma_error_t   ma_repository_from_variant(ma_variant_t *repo_variant, ma_repository_t **repository){
	ma_error_t rc = MA_ERROR_INVALIDARG ;

	if(repo_variant && repository){
		ma_variant_t *value = NULL;
		ma_table_t *repo_tb = NULL;

		if(MA_OK != ma_repository_create(repository)) return MA_ERROR_OUTOFMEMORY;
				
		if(MA_OK == (rc = ma_variant_get_table(repo_variant, &repo_tb))){
			get_value_from_table(repo_tb, "name", &(*repository)->name);
			
			if(MA_OK == (rc = ma_table_get_value(repo_tb, "repo_type", &value))){
				rc = ma_variant_get_int32(value, (ma_int32_t*)(&(*repository)->repo_type));
				ma_variant_release(value);
			}

			if(MA_OK == (rc = ma_table_get_value(repo_tb, "url_type", &value))){
				rc = ma_variant_get_int32(value, (ma_int32_t*)(&(*repository)->url_type));
				ma_variant_release(value);
			}

			if(MA_OK == (rc = ma_table_get_value(repo_tb, "auth_type", &value))){
				rc = ma_variant_get_int32(value, (ma_int32_t*)(&(*repository)->auth_type));
				ma_variant_release(value);
			}

			if(MA_OK == (rc = ma_table_get_value(repo_tb, "repo_namespace", &value))){				
				rc = ma_variant_get_int32(value, (ma_int32_t*)(&(*repository)->repo_namespace));
				ma_variant_release(value);
			}

			if(MA_OK == (rc = ma_table_get_value(repo_tb, "enabled", &value))){
				rc = ma_variant_get_bool(value, &(*repository)->enabled);
				ma_variant_release(value);
			}

			if(MA_OK == (rc = ma_table_get_value(repo_tb, "state", &value))){
				rc = ma_variant_get_int32(value, (ma_int32_t*)(&(*repository)->state));
				ma_variant_release(value);
			}

			if(MA_OK == (rc = ma_table_get_value(repo_tb, "ping_time", &value))){
				rc = ma_variant_get_int32(value, &((*repository)->ping_time));
				ma_variant_release(value);
			}

			if(MA_OK == (rc = ma_table_get_value(repo_tb, "subnet_distance", &value))){
				rc = ma_variant_get_int32(value, &((*repository)->subnet_distance));
				ma_variant_release(value);
			}

			if(MA_OK == (rc = ma_table_get_value(repo_tb, "sitelist_order", &value))){
				rc = ma_variant_get_int32(value, &((*repository)->sitelist_order));
				ma_variant_release(value);
			}

			if(MA_OK == (rc = ma_table_get_value(repo_tb, "proxy_usage", &value))){
				rc = ma_variant_get_int32(value, (ma_int32_t*)(&((*repository)->proxy_usage)));
				ma_variant_release(value);
			}

			if((*repository)->url_type == MA_REPOSITORY_URL_TYPE_HTTP){
				get_value_from_table(repo_tb, "server_fqdn", &(*repository)->params.http_repository.server_fqdn);
				get_value_from_table(repo_tb, "path", &(*repository)->params.http_repository.path);
				get_value_from_table(repo_tb, "auth_user", &(*repository)->params.http_repository.auth_user);
				get_value_from_table(repo_tb, "auth_passwd", &(*repository)->params.http_repository.auth_passwd);

				if(MA_OK == (rc = ma_table_get_value(repo_tb, "port", &value))){
					rc = ma_variant_get_int32(value, &((*repository)->params.http_repository.port));
					ma_variant_release(value);
				}
			}

			if((*repository)->url_type == MA_REPOSITORY_URL_TYPE_FTP){
				get_value_from_table(repo_tb, "server_fqdn", &(*repository)->params.ftp_repository.server_fqdn);
				get_value_from_table(repo_tb, "path", &(*repository)->params.ftp_repository.path);
				get_value_from_table(repo_tb, "auth_user", &(*repository)->params.ftp_repository.auth_user);
				get_value_from_table(repo_tb, "auth_passwd", &(*repository)->params.ftp_repository.auth_passwd);

				if(MA_OK == (rc = ma_table_get_value(repo_tb, "port", &value))){
					rc = ma_variant_get_int32(value, &((*repository)->params.ftp_repository.port));
					ma_variant_release(value);
				}
			}

			if((*repository)->url_type == MA_REPOSITORY_URL_TYPE_SPIPE){
				get_value_from_table(repo_tb, "server_fqdn", &(*repository)->params.spipe_repository.server_fqdn);
				get_value_from_table(repo_tb, "path", &(*repository)->params.spipe_repository.path);
				get_value_from_table(repo_tb, "server_ip", &(*repository)->params.spipe_repository.server_ip);
				get_value_from_table(repo_tb, "server_name", &(*repository)->params.spipe_repository.server_name);
				get_value_from_table(repo_tb, "auth_user", &(*repository)->params.spipe_repository.auth_user);
				get_value_from_table(repo_tb, "auth_passwd", &(*repository)->params.spipe_repository.auth_passwd);

				if(MA_OK == (rc = ma_table_get_value(repo_tb, "port", &value))){
					rc = ma_variant_get_int32(value, &((*repository)->params.spipe_repository.port));
					ma_variant_release(value);
				}
				if(MA_OK == (rc = ma_table_get_value(repo_tb, "ssl_port", &value))){
					rc = ma_variant_get_int32(value, &((*repository)->params.spipe_repository.ssl_port));
					ma_variant_release(value);
				}
			}

			if((*repository)->url_type == MA_REPOSITORY_URL_TYPE_SA){
				get_value_from_table(repo_tb, "server_fqdn", &(*repository)->params.sa_repository.server_fqdn);
				get_value_from_table(repo_tb, "path", &(*repository)->params.sa_repository.path);
				get_value_from_table(repo_tb, "server_ip", &(*repository)->params.sa_repository.server_ip);
				get_value_from_table(repo_tb, "server_name", &(*repository)->params.sa_repository.server_name);
				get_value_from_table(repo_tb, "auth_user", &(*repository)->params.sa_repository.auth_user);
				get_value_from_table(repo_tb, "auth_passwd", &(*repository)->params.sa_repository.auth_passwd);

				if(MA_OK == (rc = ma_table_get_value(repo_tb, "port", &value))){
					rc = ma_variant_get_int32(value, &((*repository)->params.sa_repository.port));
					ma_variant_release(value);
				}
				if(MA_OK == (rc = ma_table_get_value(repo_tb, "ssl_port", &value))){
					rc = ma_variant_get_int32(value, &((*repository)->params.sa_repository.ssl_port));
					ma_variant_release(value);
				}
			}

			if((*repository)->url_type == MA_REPOSITORY_URL_TYPE_UNC){
				get_value_from_table(repo_tb, "server_fqdn", &(*repository)->params.ucn_repository.server_fqdn);
				get_value_from_table(repo_tb, "path", &(*repository)->params.ucn_repository.path);
				get_value_from_table(repo_tb, "auth_passwd", &(*repository)->params.ucn_repository.auth_passwd);
				get_value_from_table(repo_tb, "auth_user", &(*repository)->params.ucn_repository.auth_user);
				get_value_from_table(repo_tb, "domain", &(*repository)->params.ucn_repository.domain);
			}

			if((*repository)->url_type == MA_REPOSITORY_URL_TYPE_LOCAL){
				get_value_from_table(repo_tb, "path", &(*repository)->params.local_repository.path);
				get_value_from_table(repo_tb, "auth_passwd", &(*repository)->params.local_repository.auth_passwd);
				get_value_from_table(repo_tb, "auth_user", &(*repository)->params.local_repository.auth_user);
				get_value_from_table(repo_tb, "domain", &(*repository)->params.local_repository.domain);
			}
			(void)ma_table_release(repo_tb);
		}
	}
	return rc;
}