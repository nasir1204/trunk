#include "ma/ma_variant.h"
#include "ma/internal/utils/repository/ma_repository_internal.h"
#include <stdlib.h>
#include <string.h>

typedef struct repo_list_for_each_s{
	ma_int32_t index;
	const char *name;
	ma_bool_t found;
}repo_list_for_each_t;

static void ma_repository_list_for_each_name(size_t index, ma_variant_t *value, void *cb_args, ma_bool_t *stop_loop);

ma_error_t   ma_repository_list_create(ma_repository_list_t **repository_list){
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	if(repository_list) {
		ma_repository_list_t *self = (ma_repository_list_t *)calloc(1,sizeof(ma_repository_list_t));
		if(!self)	
			return MA_ERROR_OUTOFMEMORY;

		if(MA_OK == (rc = ma_array_create(&self->repo))) {
			*repository_list = self;
			rc = MA_OK;
		}
		else
			free(self);
	}
	return rc;
}

ma_error_t   ma_repository_list_release(ma_repository_list_t *repository_list){
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	if(repository_list) {
        (void)ma_array_release(repository_list->repo) ;
        free(repository_list) ;
        repository_list = NULL ;
		rc = MA_OK;
	}
	return rc;
}

ma_error_t   ma_repository_list_add_repository(ma_repository_list_t *repository_list, ma_repository_t *repository){
	ma_error_t    rc = MA_ERROR_INVALIDARG ;
	if(repository_list && repository) {
		ma_variant_t *repo_variant_value = NULL;
		if((repository->name) && ( MA_OK == (rc = ma_repository_as_variant(repository, &repo_variant_value)))){
			rc = ma_array_push(repository_list->repo, repo_variant_value);
			(void)ma_variant_release(repo_variant_value);
		}
	}
	return rc;     
}

ma_error_t   ma_repository_list_remove_repository(ma_repository_list_t *repository_list, ma_repository_t *repository){
	ma_error_t    rc = MA_ERROR_INVALIDARG ;
	if(repository_list && repository && repository->name){
		repo_list_for_each_t foreach_data = {0};
		foreach_data.name = repository->name;
		if(MA_OK == (rc = ma_array_foreach(repository_list->repo, ma_repository_list_for_each_name, (void*)&foreach_data))){
			if((foreach_data.found == MA_TRUE)){
				ma_variant_t *entry = NULL;
				if(MA_OK == (rc = ma_array_get_element_at(repository_list->repo, (foreach_data.index + 1), &entry))){
					ma_table_t *repo_tb = NULL;
					if(MA_OK == (rc = ma_variant_get_table(entry, &repo_tb))){
						ma_variant_t *new_var = NULL;
						if(MA_OK == (rc = ma_table_remove_entry(repo_tb, "state"))) {
							if(MA_OK == (rc = ma_variant_create_from_int32(2, &new_var))) { /*2 for MA_REPOSITORY_STATE_REMOVED */
								rc = ma_table_add_entry(repo_tb, "state", new_var);
								(void)ma_variant_release(new_var);
							}
						}
                        (void)ma_table_release(repo_tb);
					}
				    (void)ma_variant_release(entry);
				}

			}
		}
	}
	return rc;
}

ma_error_t   ma_repository_list_get_repositories_count(ma_repository_list_t *repository_list, size_t *repositories_count){
	ma_error_t    rc = MA_ERROR_INVALIDARG ;
	if(repository_list){
		rc = ma_array_size(repository_list->repo, repositories_count);
	}
	return rc;
}

ma_error_t   ma_repository_list_get_repository_by_index(ma_repository_list_t *repository_list, ma_int32_t index, ma_repository_t **repository){
	ma_error_t    rc = MA_ERROR_INVALIDARG ;
	if(repository_list && repository){
		ma_variant_t *entry = NULL;
		/*ma_array is 1 index based */
		if(MA_OK == (rc = ma_array_get_element_at(repository_list->repo, index+1, &entry))){			
				rc = ma_repository_from_variant(entry, repository);
				(void)ma_variant_release(entry);
		}
	}
	return rc;
}

void ma_repository_list_for_each_name(size_t index, ma_variant_t *value, void *cb_args, ma_bool_t *stop_loop) {
    repo_list_for_each_t *foreach_data = (repo_list_for_each_t *)cb_args;
    ma_error_t rc = MA_OK;
	ma_repository_t *repository = NULL;
    if(MA_OK == (rc = ma_repository_from_variant(value, &repository))) {
        if(!strcmp(foreach_data->name,repository->name )){
			foreach_data->found = MA_TRUE;
			/* Making zero based index */
			foreach_data->index = (ma_int32_t)index - 1;
			*stop_loop = MA_TRUE;
			(void)ma_repository_release(repository);
			return;
		}
		(void)ma_repository_release(repository);
    }
        
    if(MA_OK != rc) {
        *stop_loop = MA_TRUE;
        foreach_data->found = MA_FALSE;
    }
}

ma_error_t   ma_repository_list_get_repository_by_name(ma_repository_list_t *repository_list, const char *name, ma_repository_t **repository){
	ma_error_t    rc = MA_ERROR_INVALIDARG ;
	if(repository_list && repository && name){
		repo_list_for_each_t foreach_data = {0};
		foreach_data.name = name;
		if(MA_OK == (rc = ma_array_foreach(repository_list->repo, ma_repository_list_for_each_name, (void*)&foreach_data))){
			if(foreach_data.found == MA_TRUE) {
				rc = ma_repository_list_get_repository_by_index(repository_list, foreach_data.index, repository);
			}
		}
	}
	return rc;
}


ma_error_t   ma_repository_list_from_variant(ma_variant_t *data, ma_repository_list_t **repository_list){	
	if(repository_list && data) {		
		ma_error_t rc = MA_ERROR_OUTOFMEMORY ;
        ma_repository_list_t *self = (ma_repository_list_t *)calloc(1, sizeof(ma_repository_list_t));
        if(self) {
			if(MA_OK == (rc = ma_variant_get_array(data, &self->repo))){
				*repository_list = self;
				return rc;
			}
			free(self);			
		}
        return rc;        
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t   ma_repository_list_as_variant(ma_repository_list_t *repository_list, ma_variant_t **variant){	
	if(repository_list && repository_list->repo && variant) {		
		return ma_variant_create_from_array(repository_list->repo, variant);
	}
	return MA_ERROR_INVALIDARG;
}


ma_error_t   ma_repository_list_copy(const ma_repository_list_t *repository_list_from, ma_repository_list_t **repository_list_to) {
    if(repository_list_from && repository_list_to) {		
		ma_repository_list_t *repo_list = (ma_repository_list_t *)calloc(1, sizeof(ma_repository_list_t));
        if(repo_list) {
            ma_array_add_ref(repo_list->repo = repository_list_from->repo);
            *repository_list_to = repo_list;
            return MA_OK;
        }
        return MA_ERROR_OUTOFMEMORY;
	}
	return MA_ERROR_INVALIDARG;

}
