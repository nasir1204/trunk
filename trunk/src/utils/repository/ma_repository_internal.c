#include "ma/internal/utils/repository/ma_repository_private.h"
#include "ma/internal/utils/context/ma_context.h"
#include "ma/internal/utils/database/ma_db.h"
#include "ma/internal/utils/macrypto/ma_crypto.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/utils/repository/ma_repository_db_manager.h"
#include "ma/internal/services/ma_policy_settings_bag.h"


ma_error_t   ma_repository_get_repositories(ma_context_t *context, const char *filter, ma_repository_list_t **repository_list) {

	if(context && repository_list) {
		ma_db_t *db_handle = ma_configurator_get_database(MA_CONTEXT_GET_CONFIGURATOR(context)) ;
		ma_crypto_t *crypto = MA_CONTEXT_GET_CRYPTO(context) ;
		ma_policy_settings_bag_t *pso_bag = MA_CONTEXT_GET_POLICY_SETTINGS_BAG(context) ;
		
		ma_repository_rank_type_t repository_rank = MA_REPOSITORY_RANK_BY_SITELIST_ORDER ;

		(void)ma_policy_settings_bag_get_int(pso_bag, MA_REPOSITORY_ADVANCED_SECTION_NAME_STR, MA_REPOSITORY_KEY_RANK_TYPE_INT, MA_TRUE ,(ma_int32_t *)&repository_rank, MA_REPOSITORY_RANK_BY_SITELIST_ORDER) ; 

		return ma_repository_db_get_repositories(db_handle, crypto, filter, repository_rank, repository_list) ;
	}
	return MA_ERROR_INVALIDARG ;
}
