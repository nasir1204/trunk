#include "ma/internal/utils/repository/ma_sitelist.h"
#include "ma/internal/utils/xml/ma_xml.h"
#include "ma/internal/utils/xml/ma_xml_node.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"

#include "ma/internal/utils/proxy/ma_proxy_db_manager.h"

#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/defs/ma_general_defs.h"
#include "ma/internal/utils/repository/ma_repository_db_manager.h"
#include "ma/internal/utils/repository/ma_repository_internal.h"
#include "ma/internal/utils/database/ma_db.h"
#include "ma/internal/utils/database/ma_db_recordset.h"
#include "ma/internal/utils/database/ma_db_statement.h"


#include "ma/internal/defs/ma_ah_defs.h"
#include "ma/ma_log.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/macrypto/ma_crypto.h"
#include "ma/internal/utils/filesystem/path.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef MA_WINDOWS
#include <windows.h>
#else
#include <unistd.h>
#include <limits.h>
#endif

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "Repository"

#define MA_REPOSITORY_ROOT                              "ns:SiteLists"
#define MA_REPOSITORY_SITES_PATH                        "ns:SiteLists/SiteList"
#define MA_REPOSITORY_SPIPE_SITES_PATH                  "ns:SiteLists/SiteList/SpipeSite"
#define MA_REPOSITORY_PROXY_CONFIG_PATH                 "ns:SiteLists/SiteList/ProxyConfigList"

#define MA_SITELISTS_CERTIFICATE_PATH					"ns:SiteLists/CaBundle/CaCertificate"
#define MA_SITELISTS_GLOBAL_VERSION						"GlobalVersion"
#define MA_SITELISTS_LOCAL_VERSION						"LocalVersion"
#define MA_SITELISTS_CERTIFICATE						"CaCertificate"
#define MA_SITELISTS_VERSION							"Version"

#define MA_REPOSITORY_HTTP_SITE                         "HttpSite"
#define MA_REPOSITORY_FTP_SITE                          "FTPSite"
#define MA_REPOSITORY_UNC_SITE                          "UNCSite"
#define MA_REPOSITORY_SPIPE_SITE                        "SpipeSite"
#define MA_REPOSITORY_SA_SITE							"SuperAgentSite"
#define MA_REPOSITORY_PROXY_CONFIG                      "ProxyConfigList"

#define MA_REPOSITORY_ATTR_TYPE_STR                     "Type"
#define MA_REPOSITORY_ATTR_NAME_STR                     "Name"

#define MA_REPOSITORY_ATTR_SERVER_FQDN_STR              "Server"
#define MA_REPOSITORY_ATTR_SERVERIP_STR                 "ServerIP"
#define MA_REPOSITORY_ATTR_SERVER_NAME_STR              "ServerName"
#define MA_REPOSITORY_ATTR_SSL_PORT_STR				    "SecurePort"

#define MA_REPOSITORY_ATTR_ENABLED_STR                  "Enabled"
#define MA_REPOSITORY_ATTR_ORDER_STR                    "Order"
#define MA_REPOSITORY_ATTR_NAMESPACE_STR                "Local"
#define MA_REPOSITORY_TAG_RELATIVEPATH_STR              "RelativePath"
#define MA_REPOSITORY_TAG_SHARENAME_STR                 "ShareName"
#define MA_REPOSITORY_TAG_USE_AUTH_STR                  "UseAuth"
#define MA_REPOSITORY_TAG_DOMAIN_STR                    "DomainName"
#define MA_REPOSITORY_TAG_USER_STR                      "UserName"
#define MA_REPOSITORY_TAG_PASSWD_STR                    "Password"
#define MA_REPOSITORY_ATTR_ENCRYPTED_STR                "Encrypted"
#define MA_REPOSITORY_TAG_USERLOGGEDONACNT_STR          "UseLoggedonUserAccount"

#define MA_SERVER_SITE_LIST_COMPAT_STR                  "ServerSiteList.xml"

#define MA_AGENT_COMPAT_SCRACTH_STR                     "scratch"



#define MA_INFO_EPO_SERVER_LIST_GET_STMT						"SELECT SERVER_IP, SERVER_NAME, SERVER_FQDN FROM AGENT_REPOSITORIES WHERE URL_TYPE = 4"  /*4: MA_REPOSITORY_URL_TYPE_SPIPE */
#define MA_INFO_EPO_PORT_LIST_GET_STMT							"SELECT SSL_PORT,PORT FROM AGENT_REPOSITORIES WHERE URL_TYPE = 4"  

								


static ma_error_t set_repository_attributes(ma_repository_t *repository, ma_crypto_t *crypto, ma_xml_node_t *repo_node, ma_bool_t is_spipe_request) ;

static void format_certificate(const char *cert, char *output) {
    int iter = 0 ;
    int token_size = 0 ;
    int out_iter = 0 ;
    while(iter < (int)strlen(cert)) 
    {
        if(cert[iter] == '\r' || cert[iter] == '\n')    
            iter ++ ;
        else {
            output[out_iter++] = cert[iter++] ;
            if(++token_size == 64) {
                token_size = 0 ;
                output[out_iter++] = '\n' ;
            }
        }
    }
    if( 0 < token_size && token_size < 64) 
        output[out_iter] = '\n' ;
}

static ma_error_t create_ca_bundle_file(ma_xml_node_t *sitelist_root, const char *cabundle_path) {
    if(sitelist_root && cabundle_path) {         
        ma_error_t rc = MA_OK;
        const char *cert_start_marker = "-----BEGIN CERTIFICATE-----";
		const char *cert_end_marker = "-----END CERTIFICATE-----";

        if(NULL != sitelist_root){
            ma_xml_node_t *node = NULL;
            /* TBD - Take back up of existing cabundle file with suffix bak before overwriting*/
#ifdef MA_WINDOWS            
            DeleteFileA(cabundle_path);
#else
            unlink(cabundle_path);
#endif
            node = ma_xml_node_search_by_path(sitelist_root, MA_SITELISTS_CERTIFICATE_PATH);

            if(node) {
                FILE *fp = fopen(cabundle_path , "w");
                if(fp) {
                    /* Next node exists and it is a XML_SITELISTS_CERTIFICATE */
                    while(node && !strncmp(ma_xml_node_get_name(node), MA_SITELISTS_CERTIFICATE, strlen(MA_SITELISTS_CERTIFICATE))) {
                        ma_xml_node_t *next_node = ma_xml_node_get_next_sibling(node);                   
                        const char *cert = ma_xml_node_get_data(node); 
                        if(cert) {
                            char *tmp_cert = (char*)calloc(1, strlen(cert)*2);
                            if(tmp_cert) {           
                                format_certificate(cert, tmp_cert);
                                fprintf(fp, "%s\n", cert_start_marker);
                                fprintf(fp, "%s", tmp_cert);                          
                                fprintf(fp, "%s\n", cert_end_marker);
                                free(tmp_cert);                            
                            }
                            else {
                                rc = MA_ERROR_OUTOFMEMORY;
                                break;
                            }
                        }
                        node = next_node;
                    } 
                    fclose(fp);
                }
                else
                    rc = MA_ERROR_FILE_OPEN_FAILED;
            }
            else
                rc = MA_ERROR_UNEXPECTED;
        }

        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

static void handle_sitelist_versions(ma_sitelist_handler_t *handler, ma_xml_node_t *sitelist_root) {
	if(handler && sitelist_root ) {
		ma_error_t rc = MA_OK ;
		ma_xml_node_t *node = ma_xml_node_search_by_path(sitelist_root, MA_REPOSITORY_ROOT);

		if(ma_xml_node_attribute_has(node, MA_SITELISTS_GLOBAL_VERSION))
			(void)ma_ds_set_str(handler->ma_ds, MA_AH_POLICIES_SITELIST_PATH_NAME_STR, MA_AH_KEY_SITELIST_GLOBAL_VERSION_STR , ma_xml_node_attribute_get(node, MA_SITELISTS_GLOBAL_VERSION), -1);
                        
		if(ma_xml_node_attribute_has(node, MA_SITELISTS_LOCAL_VERSION))
			(void)ma_ds_set_str(handler->ma_ds, MA_AH_POLICIES_SITELIST_PATH_NAME_STR, MA_AH_KEY_SITELIST_LOCAL_VERSION_STR , ma_xml_node_attribute_get(node, MA_SITELISTS_LOCAL_VERSION), -1);
            
		{
			char cabundle_file_name[MA_MAX_LEN] = {0};
			MA_MSC_SELECT(_snprintf, snprintf)(cabundle_file_name, MA_MAX_LEN -1,"%s%c%s", handler->data_path, MA_PATH_SEPARATOR, MA_AH_CA_BUNDLE_FILE_NAME_STR);
			if(MA_OK == (rc = create_ca_bundle_file(sitelist_root, cabundle_file_name)))
				rc = ma_ds_set_str(handler->ma_ds, MA_AH_POLICIES_SITELIST_PATH_NAME_STR, MA_AH_KEY_SITELIST_CA_FILE_STR , cabundle_file_name, -1);
		}
		if(MA_OK != rc) MA_LOG(handler->logger, MA_LOG_SEV_TRACE, "sitelist version/certificate are not present(%d)", rc) ;
	}   
	return ;
}

ma_error_t ma_sitelist_handler_init(ma_sitelist_handler_t *handler, ma_crypto_t *crypto, ma_ds_t *ds, ma_db_t *db, ma_logger_t *logger, const char *datapath) {
	if(handler && crypto && ds && db && datapath){
		handler->crypto = crypto;
		handler->ma_db = db;
		handler->ma_ds = ds;
		handler->logger = logger;
		handler->data_path = datapath;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

static ma_error_t reset_existing_attributes(ma_db_t *ma_db, ma_repository_t *repository, ma_xml_node_t *repo_node) {
	int enabled, ping_time, subnet, state;
	if(MA_OK == ma_repository_get_non_sitelist_xml_attr(ma_db, repository->name, &enabled, &ping_time, &subnet, &state)){
		repository->ping_time = ping_time;
		repository->subnet_distance = subnet;
		repository->state = (ma_repository_state_t)(state - 10) ;
		(void)ma_repository_set_enabled(repository, (ma_bool_t)enabled) ;

	}
	else{/*no entry in DB */
		repository->ping_time = (repository->repo_type == MA_REPOSITORY_TYPE_FALLBACK) ? INT_MAX : (30 * 1000) ;
		repository->subnet_distance = (repository->repo_type == MA_REPOSITORY_TYPE_FALLBACK) ? INT_MAX : 15 ;
		repository->state = MA_REPOSITORY_STATE_ADDED ;
		(void)ma_repository_set_enabled(repository, (ma_bool_t)atoi(ma_xml_node_attribute_get(repo_node, MA_REPOSITORY_ATTR_ENABLED_STR))) ;
	}
	return MA_OK;
}

/* sitelist Handler Methods */
static void save_sitelist_xml(ma_sitelist_handler_t *handler, ma_xml_t *sitelist_xml) {
    char server_site_list_xml_path[MA_MAX_LEN] = {0};

#if defined (MA_WINDOWS)
    MA_MSC_SELECT(_snprintf, snprintf)(server_site_list_xml_path, MA_MAX_LEN -1,"%s%c%s", handler->data_path, MA_PATH_SEPARATOR, MA_SERVER_SITE_LIST_COMPAT_STR);
#else
    MA_MSC_SELECT(_snprintf, snprintf)(server_site_list_xml_path, MA_MAX_LEN -1,"%s%c%s%c%s", handler->data_path, MA_PATH_SEPARATOR, MA_AGENT_COMPAT_SCRACTH_STR, MA_PATH_SEPARATOR, MA_SERVER_SITE_LIST_COMPAT_STR);
#endif
    ma_xml_save_to_file(sitelist_xml, server_site_list_xml_path);
}

static void handle_sitelist_epo_versions(ma_sitelist_handler_t *handler, ma_xml_node_t *spipe_node) {
	if(handler && spipe_node){
		if(ma_xml_node_attribute_has(spipe_node, MA_SITELISTS_VERSION))
			(void)ma_ds_set_str(handler->ma_ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_EPO_VERSION_STR , ma_xml_node_attribute_get(spipe_node, MA_SITELISTS_VERSION), -1);
	}
}
        
ma_error_t ma_sitelist_handler_import_xml(ma_sitelist_handler_t *handler, char *sitelist_buffer, size_t buffer_size, ma_bool_t is_spipe_request){
    if(handler && sitelist_buffer){
		ma_error_t rc = MA_OK ;
		ma_xml_t *sitelist_mxl = NULL ;
		
		if(MA_OK == (rc = ma_xml_create(&sitelist_mxl))) {
			if(MA_OK == (rc = ma_xml_load_from_buffer(sitelist_mxl, sitelist_buffer, buffer_size))) {
				ma_xml_node_t *sitelist_root, *repo_node = NULL ;
                save_sitelist_xml(handler, sitelist_mxl);
				if(NULL != (sitelist_root = ma_xml_get_node(sitelist_mxl))) {
					ma_repository_t *repository = NULL ;
                    ma_int32_t agent_mode = 0;
					ma_bool_t enable_state = MA_TRUE ;

                    /*Now check if we are in managed mode and this sitelist.xml doesn't contains sPIPE entries, reject this sitelist.xml */
                    if(MA_OK == (rc = ma_ds_get_int32(handler->ma_ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_MODE_INT, &agent_mode))) {
                        if((MA_AGENT_MODE_MANAGED_INT == agent_mode) && ( NULL == ma_xml_node_search_by_path(sitelist_root, MA_REPOSITORY_SPIPE_SITES_PATH))){
                            MA_LOG(handler->logger, MA_LOG_SEV_ERROR, "failed to import site list as agent in managed mode and no spipe entries found.");
							(void)ma_xml_release(sitelist_mxl);
                            return (rc = MA_ERROR_REPOSITORY_INVALID_SITELIST);
                        }

						handle_sitelist_versions(handler, sitelist_root) ;
						repo_node = ma_xml_node_get_first_child(ma_xml_node_search_by_path(sitelist_root, MA_REPOSITORY_SITES_PATH)) ;
						
						/*Remove all global repositories & proxies from DB */
											
						(void)ma_repository_db_get_epo_site_enable_state(handler->ma_db, &enable_state) ;

						(void)ma_db_transaction_begin(handler->ma_db);
						if( (MA_OK == (rc = ma_repository_db_mark_remove_all_global_repositories(handler->ma_db))) && (MA_OK == (rc = ma_proxy_db_remove_all_global_proxies(handler->ma_db)))) {
							while(NULL != repo_node) {
								if(MA_OK == (rc = ma_repository_create(&repository))) {
									const char *repo_type = ma_xml_node_get_name(repo_node) ;
									if(!strcmp(repo_type, MA_REPOSITORY_HTTP_SITE)) {
										(void)ma_repository_set_url_type(repository, MA_REPOSITORY_URL_TYPE_HTTP) ;
									}
									else if(!strcmp(repo_type, MA_REPOSITORY_FTP_SITE)) {
										(void)ma_repository_set_url_type(repository, MA_REPOSITORY_URL_TYPE_FTP) ;
									}
									else if(!strcmp(repo_type, MA_REPOSITORY_UNC_SITE)) {
										(void)ma_repository_set_url_type(repository, MA_REPOSITORY_URL_TYPE_UNC) ;
									}
									else if(!strcmp(repo_type, MA_REPOSITORY_SPIPE_SITE)) {
    									(void)ma_repository_set_url_type(repository, MA_REPOSITORY_URL_TYPE_SPIPE) ;
										handle_sitelist_epo_versions(handler, repo_node);
									}
									else if(!strcmp(repo_type, MA_REPOSITORY_SA_SITE)) {
										(void)ma_repository_set_url_type(repository, MA_REPOSITORY_URL_TYPE_SA) ;
									}
									else { 
										MA_LOG(handler->logger, MA_LOG_SEV_ERROR, "repository(%s) has unknown repo type(%s)", repository->name, repo_type) ;
										(void)ma_repository_release(repository) ;
										repo_node = ma_xml_node_get_next_sibling(repo_node) ;
										continue;
									}								
									
									/*rc = set_repository_attributes(repository, handler->crypto, repo_node, is_spipe_request) ;
									if(MA_OK != rc) {
										MA_LOG(handler->logger, MA_LOG_SEV_ERROR, "repository(%s) set repository attributes failed, %d", repository->name, rc) ;
										(void)ma_repository_release(repository) ;
										break;
									}*/

									(void)set_repository_attributes(repository, handler->crypto, repo_node, is_spipe_request) ;
									(void)reset_existing_attributes(handler->ma_db, repository, repo_node);

									if(MA_OK == (rc = ma_repository_validate(repository))) {
										if(MA_OK == (rc = ma_repository_db_add_repository(handler->ma_db, repository))){
											MA_LOG(handler->logger, MA_LOG_SEV_DEBUG, "repository(%s) persisted to db", repository->name) ;
										}
										else{
											MA_LOG(handler->logger, MA_LOG_SEV_WARNING, "repository(%s) persistence to db failed", repository->name) ;
											(void)ma_repository_release(repository) ;
                                            repo_node = ma_xml_node_get_next_sibling(repo_node) ;
											continue;
										}
									}
									else{
										MA_LOG(handler->logger, MA_LOG_SEV_WARNING, "repository(%s) validation failed, so not persisting to db", repository->name) ;
										(void)ma_repository_release(repository) ;
                                        repo_node = ma_xml_node_get_next_sibling(repo_node) ;
										continue;
									}
									(void)ma_repository_release(repository) ;
								}
								repo_node = ma_xml_node_get_next_sibling(repo_node) ;
							}
							rc = ma_repository_db_remove_all_global_repositories_by_state(handler->ma_db);
							
							(void)ma_repository_db_set_ah_sites_enable_state(handler->ma_db, enable_state) ;

							(MA_OK == rc) ? ma_db_transaction_end(handler->ma_db) : ma_db_transaction_cancel(handler->ma_db);
						}
					}
				}
			}
			(void)ma_xml_release(sitelist_mxl);
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG;
}


static ma_repository_type_t get_repository_type(const char *type) {
    if(!strcmp(type, "repository")) {
        return MA_REPOSITORY_TYPE_REPO ;
    }
    else if(!strcmp(type, "master")) {
        return MA_REPOSITORY_TYPE_MASTER ;
    }
    else if(!strcmp(type, "fallback")) {
        return MA_REPOSITORY_TYPE_FALLBACK ;
    }
    else {
        return MA_REPOSITORY_TYPE_REPO ;
    }
}
static ma_repository_namespace_t get_repository_namespace(const char *type) {
    if(!strcmp(type, "0")) {
        return MA_REPOSITORY_NAMESPACE_GLOBAL ;
    }
    else if(!strcmp(type, "1")) {
        return MA_REPOSITORY_NAMESPACE_LOCAL ;
    }
    else {
        return MA_REPOSITORY_NAMESPACE_GLOBAL ;
    }
}

static ma_repository_auth_t get_repository_auth(ma_repository_t *repository, ma_xml_node_t *repo_node) {
	ma_xml_node_t *tmp = NULL, *ftp_passwd = NULL, *ftp_user = NULL ;
	ma_repository_auth_t auth = MA_REPOSITORY_AUTH_NONE ;
	switch(repository->url_type)
    {
        case MA_REPOSITORY_URL_TYPE_HTTP :
        case MA_REPOSITORY_URL_TYPE_SPIPE :
		case MA_REPOSITORY_URL_TYPE_SA :
            tmp = ma_xml_node_get_child_by_name(repo_node, MA_REPOSITORY_TAG_USE_AUTH_STR) ;
            if(tmp && 1 == atoi(ma_xml_node_get_data(tmp))) {
                auth =  MA_REPOSITORY_AUTH_CREDENTIALS ;
            }
            break ;
        case MA_REPOSITORY_URL_TYPE_FTP :
			/*ePO do not set "UseAuth" for FTP */
			/*Till this issue get resolve, adding manually check of password presence & accordingly setting auth */
			ftp_passwd = ma_xml_node_get_child_by_name(repo_node, MA_REPOSITORY_TAG_PASSWD_STR) ;
			ftp_user = ma_xml_node_get_child_by_name(repo_node, MA_REPOSITORY_TAG_USER_STR) ;
            tmp = ma_xml_node_get_child_by_name(repo_node, MA_REPOSITORY_TAG_USE_AUTH_STR) ;
            if(tmp && 1 == atoi(ma_xml_node_get_data(tmp))) {
				if(!MA_WIN_SELECT(stricmp,strcasecmp)("anonymous",ma_xml_node_get_data(ma_xml_node_get_child_by_name(repo_node, MA_REPOSITORY_TAG_USER_STR))))
                    auth = MA_REPOSITORY_AUTH_ANONYMOUS ;
                else
                    auth =  MA_REPOSITORY_AUTH_CREDENTIALS ;
            }
			else if(ftp_passwd && ftp_user) {
				const char *data = ma_xml_node_get_data(ftp_passwd);
				/* BZ#1003327: Some time ePO send password string for "anonymous" user too */
				if(!MA_WIN_SELECT(stricmp,strcasecmp)("anonymous", ma_xml_node_get_data(ma_xml_node_get_child_by_name(repo_node, MA_REPOSITORY_TAG_USER_STR))))
					auth = MA_REPOSITORY_AUTH_ANONYMOUS ;
				else if(data && strlen(data)) /*ma_xml_node_get_data() retun empty string instead of NULL(in case of data absense), so need to check with strlen() */
					auth =  MA_REPOSITORY_AUTH_CREDENTIALS ;
			}
			else if(ftp_user) {
				if(!MA_WIN_SELECT(stricmp,strcasecmp)("anonymous", ma_xml_node_get_data(ma_xml_node_get_child_by_name(repo_node, MA_REPOSITORY_TAG_USER_STR))))
					auth = MA_REPOSITORY_AUTH_ANONYMOUS ;
			}
            break ;
        case MA_REPOSITORY_URL_TYPE_UNC :
            tmp = ma_xml_node_get_child_by_name(repo_node, MA_REPOSITORY_TAG_USERLOGGEDONACNT_STR) ;
            if(tmp) {
                if(1 == atoi(ma_xml_node_get_data(tmp)))
                    auth = MA_REPOSITORY_AUTH_LOGGEDON_USER ;
                else
                    auth =  MA_REPOSITORY_AUTH_CREDENTIALS ;
            }
            break ;
        default :
            auth = MA_REPOSITORY_AUTH_NONE ;
    }
    return auth ;
}

static void ma_extract_ip_and_port(char *server, char *ip, char *port) {
	if(server && ip && port) {
		size_t len = strlen(server);
		char *p = NULL;
		char *port_pos = NULL;

		if(strchr(server, '[') && (port_pos = strchr(server, ']'))) {
			if(p = strchr(port_pos, ':')) {
				*p = '\0';
				strcpy(ip, server);
				strcpy(port, p+1);
			}
			else {
				strcpy(ip, server);
			}
		}
		else {
			if(p = strrchr(server, ':')) {
				*p = '\0';
				strcpy(ip, server);
				strcpy(port, p+1);
			}
			else {
				strcpy(ip, server);
			}
		}
	}
}

ma_error_t set_repository_attributes(ma_repository_t *repository, ma_crypto_t *crypto, ma_xml_node_t *repo_node, ma_bool_t is_spipe_request) {
    char *p = NULL, *server = NULL ;
    char *relative_path = NULL, *share_name = NULL ;
    ma_error_t rc = MA_OK;

    (void)ma_repository_set_type(repository, get_repository_type(ma_xml_node_attribute_get(repo_node, MA_REPOSITORY_ATTR_TYPE_STR))) ;

    (void)ma_repository_set_name(repository, ma_xml_node_attribute_get(repo_node, MA_REPOSITORY_ATTR_NAME_STR)) ; 

    if(ma_xml_node_attribute_has(repo_node, MA_REPOSITORY_ATTR_NAMESPACE_STR))
        (void)ma_repository_set_namespace(repository, get_repository_namespace(ma_xml_node_attribute_get(repo_node, MA_REPOSITORY_ATTR_NAMESPACE_STR))) ;
    else
        (void)ma_repository_set_namespace(repository, MA_REPOSITORY_NAMESPACE_GLOBAL) ;
    
    if(ma_xml_node_attribute_has(repo_node, MA_REPOSITORY_ATTR_SERVER_FQDN_STR)) {
		char addr[MA_MAX_PATH_LEN] = {0};
		char port[MA_MAX_PATH_LEN] = {0};
        server = strdup((char *)ma_xml_node_attribute_get(repo_node, MA_REPOSITORY_ATTR_SERVER_FQDN_STR));
		ma_extract_ip_and_port(server, addr, port);
        (void)ma_repository_set_server_fqdn(repository, addr) ;
        (void)ma_repository_set_port(repository, (ma_int32_t)atoi(port)) ;
		free(server);
        server = NULL ; p = NULL ;
    }
  
    if(ma_xml_node_attribute_has(repo_node, MA_REPOSITORY_ATTR_SERVERIP_STR)) {
		char addr[MA_MAX_PATH_LEN] = {0};
		char port[MA_MAX_PATH_LEN] = {0};
		server = strdup((char *)ma_xml_node_attribute_get(repo_node, MA_REPOSITORY_ATTR_SERVERIP_STR));
		
		ma_extract_ip_and_port(server, addr, port);
        (void)ma_repository_set_server_ip(repository, addr) ;
        (void)ma_repository_set_port(repository, (ma_int32_t)atoi(port)) ;
		free(server);
        server = NULL ; p = NULL ;
    }

	if(ma_xml_node_attribute_has(repo_node, MA_REPOSITORY_ATTR_SERVER_NAME_STR)) {
        server = (char *)ma_xml_node_attribute_get(repo_node, MA_REPOSITORY_ATTR_SERVER_NAME_STR) ;
        p = strtok (server,":") ;
        (void)ma_repository_set_server_name(repository, p) ;
        server = NULL ; p = NULL ;
    }

	if(ma_xml_node_attribute_has(repo_node, MA_REPOSITORY_ATTR_SSL_PORT_STR)) {
		(void)ma_repository_set_ssl_port(repository, atoi(ma_xml_node_attribute_get(repo_node, MA_REPOSITORY_ATTR_SSL_PORT_STR))) ;
	}

    relative_path = (char *)ma_xml_node_get_data(ma_xml_node_get_child_by_name(repo_node, MA_REPOSITORY_TAG_RELATIVEPATH_STR)) ;

    if(repository->url_type == MA_REPOSITORY_URL_TYPE_UNC)
        share_name = (char *)ma_xml_node_get_data(ma_xml_node_get_child_by_name(repo_node, MA_REPOSITORY_TAG_SHARENAME_STR)) ;
    
    if(share_name && relative_path ) {
        ma_temp_buffer_t  path = MA_TEMP_BUFFER_INIT ;
        ma_temp_buffer_reserve(&path, strlen(share_name) + strlen(relative_path) + 1) ;
        MA_MSC_SELECT(_snprintf, snprintf)(p = (char*)ma_temp_buffer_get(&path), ma_temp_buffer_capacity(&path), "%s%c%s", share_name, '\\', relative_path) ; 
        (void)ma_repository_set_server_path(repository, (char*)ma_temp_buffer_get(&path)) ;
        ma_temp_buffer_uninit(&path) ;
    }
    else if(share_name && !relative_path) {
        (void)ma_repository_set_server_path(repository, share_name) ;
    }
    else if(!share_name && relative_path) {
        (void)ma_repository_set_server_path(repository, relative_path) ;
    }
    

    (void)ma_repository_set_authentication(repository, get_repository_auth(repository, repo_node)) ;

    if(ma_xml_node_has_data(ma_xml_node_get_child_by_name(repo_node, MA_REPOSITORY_TAG_USER_STR)))
        (void)ma_repository_set_user_name(repository, ma_xml_node_get_data(ma_xml_node_get_child_by_name(repo_node, MA_REPOSITORY_TAG_USER_STR))) ;
	
	if(ma_xml_node_has_data(ma_xml_node_get_child_by_name(repo_node, MA_REPOSITORY_TAG_DOMAIN_STR)))
		(void)ma_repository_set_domain_name(repository, ma_xml_node_get_data(ma_xml_node_get_child_by_name(repo_node, MA_REPOSITORY_TAG_DOMAIN_STR)));

    if(ma_xml_node_has_data(ma_xml_node_get_child_by_name(repo_node, MA_REPOSITORY_TAG_PASSWD_STR)))
    {
        char *passwd = (char *)ma_xml_node_get_data(ma_xml_node_get_child_by_name(repo_node, MA_REPOSITORY_TAG_PASSWD_STR)) ;
        ma_bytebuffer_t *decrypt_byte_buffer = NULL ;
        ma_bytebuffer_t *encrypt_byte_buffer = NULL ;
		unsigned char *passwd_str = NULL;
        ma_crypto_encryption_t *encrypt  = NULL;
        ma_crypto_decryption_t *decrypt = NULL;
        
        if(passwd && (MA_OK == (rc = ma_crypto_encrypt_create(crypto,MA_CRYPTO_ENCRYPTION_3DES, &encrypt)))) {
            if(MA_OK == (rc = is_spipe_request?ma_crypto_decrypt_create(crypto,MA_CRYPTO_DECRYPTION_PASSWORD_ASYM, &decrypt):ma_crypto_decrypt_create(crypto,MA_CRYPTO_DECRYPTION_PASSWORD_3DES, &decrypt))) {
                size_t pwd_len = strlen(passwd);
                if(MA_OK ==  (rc = ma_bytebuffer_create(pwd_len + 1, &decrypt_byte_buffer))) {
                    if(MA_OK == (rc = ma_crypto_decode_and_decrypt(decrypt, (unsigned char *)passwd, pwd_len, decrypt_byte_buffer))) {
                        /* Using original encrypted password length for creating the encrypt byte buffer as decrypted password might be empty also */
                        if(MA_OK == (rc = ma_bytebuffer_create(pwd_len, &encrypt_byte_buffer))) {
                            if(MA_OK == (rc = ma_crypto_encrypt_and_encode(encrypt, ma_bytebuffer_get_bytes(decrypt_byte_buffer), ma_bytebuffer_get_size(decrypt_byte_buffer), encrypt_byte_buffer))) {
						        passwd_str = (unsigned char*) calloc((ma_bytebuffer_get_size(encrypt_byte_buffer) + 1), sizeof(unsigned char) );
								if(passwd_str) {
									memcpy (passwd_str, ma_bytebuffer_get_bytes(encrypt_byte_buffer), ma_bytebuffer_get_size(encrypt_byte_buffer));
									*(passwd_str + ma_bytebuffer_get_size(encrypt_byte_buffer)) ='\0';
								}
								else
									rc = MA_ERROR_OUTOFMEMORY;
                            }
                            (void)ma_bytebuffer_release(encrypt_byte_buffer) ;
                        }
                    }
                    (void)ma_bytebuffer_release(decrypt_byte_buffer) ;
                }
		        (void)ma_repository_set_password(repository, (char *)passwd_str) ;
		        if(passwd_str){
			        free(passwd_str); passwd_str = NULL;
		        }
            }
            (void)ma_crypto_decrypt_release(decrypt);
        }
        (void)ma_crypto_encrypt_release(encrypt);
    }
    
    if(ma_xml_node_attribute_has(repo_node, MA_REPOSITORY_ATTR_ORDER_STR))
        repository->sitelist_order = atoi(ma_xml_node_attribute_get(repo_node, MA_REPOSITORY_ATTR_ORDER_STR)) ;
    else
		repository->sitelist_order = (repository->repo_type == MA_REPOSITORY_TYPE_FALLBACK) ? INT_MAX : 999 ;

    return rc ;
}




ma_error_t ma_sitelist_get_server_list(ma_db_t *db_handle, ma_server_list_filter_t filter, ma_temp_buffer_t *list) {
	ma_error_t rc = MA_ERROR_APIFAILED ;
	ma_db_statement_t *db_stmt = NULL ;
	
	if(MA_OK == (rc =  ma_db_statement_create(db_handle, MA_INFO_EPO_SERVER_LIST_GET_STMT, &db_stmt))){
		ma_db_recordset_t *db_record = NULL ;
		if(MA_OK == (rc = ma_db_statement_execute_query(db_stmt, &db_record))) {
			char *last = NULL;
			while((MA_ERROR_NO_MORE_ITEMS != ma_db_recordset_next(db_record))) {
				const char *epo_server = NULL, *epo_server_ip = NULL, *epo_server_fqdn = NULL;

				(void)ma_db_recordset_get_string(db_record, 1, &epo_server_ip);
				(void)ma_db_recordset_get_string(db_record, 2, &epo_server);
				(void)ma_db_recordset_get_string(db_record, 3, &epo_server_fqdn);

				if(filter & MA_SERVER_LIST_BY_IP) {
					if(epo_server_ip) {
						strncat((char *)ma_temp_buffer_get(list), epo_server_ip, strlen(epo_server_ip));
						strncat((char *)ma_temp_buffer_get(list), MA_EPO_SERVER_LIST_OR_PORT_SEPARATOR_STR, 1);
					}
				}

				if(filter & MA_SERVER_LIST_BY_SERVER) {
					if(epo_server) {
						strncat((char *)ma_temp_buffer_get(list), epo_server, strlen(epo_server));
						strncat((char *)ma_temp_buffer_get(list), MA_EPO_SERVER_LIST_OR_PORT_SEPARATOR_STR, 1);
					}
				}

				if(filter & MA_SERVER_LIST_BY_FQDN) {
					if(epo_server_fqdn) {
						strncat((char *)ma_temp_buffer_get(list), epo_server_fqdn, strlen(epo_server_fqdn));
						strncat((char *)ma_temp_buffer_get(list), MA_EPO_SERVER_LIST_OR_PORT_SEPARATOR_STR, 1);
					}
				}
			}
			if(last = strrchr((char *)ma_temp_buffer_get(list), MA_EPO_SERVER_LIST_OR_PORT_SEPARATOR_C))
				*last = '\0';
			(void)ma_db_recordset_release(db_record);
		}
		(void)ma_db_statement_release(db_stmt);	
	}
	return rc;
}

ma_error_t ma_sitelist_get_server_port_list_get(ma_db_t *db_handle, ma_temp_buffer_t *list){
	ma_error_t rc = MA_ERROR_APIFAILED ;
	ma_db_statement_t *db_stmt = NULL ;
	
	if(MA_OK == (rc =  ma_db_statement_create(db_handle, MA_INFO_EPO_PORT_LIST_GET_STMT, &db_stmt))){
		ma_db_recordset_t *db_record = NULL ;
		if(MA_OK == (rc = ma_db_statement_execute_query(db_stmt, &db_record))) {
			char *last = NULL;
			while((MA_ERROR_NO_MORE_ITEMS != ma_db_recordset_next(db_record))) {
				int ssl_port = 0, non_ssl_port = 0;
				char port[16] = {0}; 
				(void)ma_db_recordset_get_int(db_record, 1, &ssl_port);
				(void)ma_db_recordset_get_int(db_record, 2, &non_ssl_port);
				MA_MSC_SELECT(_snprintf, snprintf) (port, 16, "%d", ssl_port > 0  ? ssl_port : non_ssl_port);
				strncat((char *)ma_temp_buffer_get(list), port, strlen(port));
				strncat((char *)ma_temp_buffer_get(list), MA_EPO_SERVER_LIST_OR_PORT_SEPARATOR_STR, 1);
			}
			if(last = strrchr((char *)ma_temp_buffer_get(list), MA_EPO_SERVER_LIST_OR_PORT_SEPARATOR_C))
				*last = '\0';
			(void)ma_db_recordset_release(db_record);
		}
		(void)ma_db_statement_release(db_stmt);
	}
	return rc;
}