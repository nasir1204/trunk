#include "ma/internal/utils/acl/ma_acl.h"

#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pwd.h>
#include <unistd.h>

struct ma_acl_s {
    uid_t           uid;        /* user id */

    gid_t           gid;        /* group id */

    mode_t          mode;       /* permissions */
};


ma_error_t  ma_acl_create(ma_sid_t sid, const char *sid_name, ma_acl_t **acl) {
    if(acl) {
        ma_acl_t *self = (ma_acl_t *)calloc(1, sizeof(ma_acl_t));
        if(!self) return MA_ERROR_OUTOFMEMORY;

        if((MA_SID_ADMIN == sid) || (MA_SID_WORLD == sid) || (MA_SID_LOCAL == sid)) {
            if(sid_name) {
                struct passwd *p = NULL;
                if((p = getpwnam(sid_name))) {
                    self->gid = p->pw_gid;
                    self->uid = p->pw_uid;
                    *acl = self;
                    return MA_OK;
                }
            }
        }else if(MA_SID_SYSTEM == sid) {
            *acl = self;
            return MA_OK;
        }
        free(self);
     
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t  ma_acl_add(ma_acl_t *self, ma_bool_t allow, ma_int64_t permissions, ma_int64_t inheritance) {
    if(self) {
		/*mode_t user_perm =  S_IRWXU;
        self->mode = allow ? (user_perm & permissions) : (~user_perm & permissions);*/
		self->mode = permissions;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t  ma_acl_attach_object(ma_acl_t *self, const char *object_name, ma_acl_object_type_t object_type, ma_bool_t allow_inheritance) {
    if(self && object_name && (MA_ACL_OBJECT_FILE == object_type)) {
        if(0 == chown(object_name, self->uid, self->gid)) {
            if(0 == chmod(object_name, self->mode))
                return MA_OK;
        }
        return MA_ERROR_UNEXPECTED;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_acl_release(ma_acl_t *self) {
    if(self) {
        free(self);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

    
