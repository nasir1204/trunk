#include "ma/internal/utils/acl/ma_acl.h"
#include "ma/internal/ma_macros.h"

#include <stdlib.h>
#include <stdio.h>

#include <Windows.h>
#include <aclapi.h>

struct ma_acl_s {
    PSID            psid;

    PACL            acl;
};

static ma_error_t change_security_attributes_by_name(ma_acl_t *acl, const char *sid_name);

ma_error_t  ma_acl_create(ma_sid_t sid, const char *sid_name, ma_acl_t **acl) {
    if(acl) {
        DWORD nSubAuthority, nSubAuthority1 = 0 ;
        ma_acl_t *self = NULL;
        ma_error_t rc = MA_OK;
        
        switch(sid) {
            case MA_SID_ADMIN:
                nSubAuthority = SECURITY_BUILTIN_DOMAIN_RID;
                nSubAuthority1 = DOMAIN_ALIAS_RID_ADMINS;
                break;
            case MA_SID_LOCAL:
                nSubAuthority = SECURITY_LOCAL_SERVICE_RID;
                nSubAuthority1 = 1 ;
                break;
            case MA_SID_SYSTEM:
                nSubAuthority = SECURITY_LOCAL_SYSTEM_RID;
                break;
            case MA_SID_WORLD:
                nSubAuthority = SECURITY_WORLD_RID;
                break;
            default:
                return MA_ERROR_INVALIDARG;
        }

        if(self = (ma_acl_t *)calloc(1, sizeof(ma_acl_t))) {
            static SID_IDENTIFIER_AUTHORITY authority = SECURITY_NT_AUTHORITY;
            if(AllocateAndInitializeSid(&authority, nSubAuthority1 ? 1 : 0 , nSubAuthority, nSubAuthority1, 0, 0, 0, 0, 0, 0, &self->psid)) {
                if((MA_SID_LOCAL == sid) && sid_name)
                    rc = change_security_attributes_by_name(self, sid_name);
                if(MA_OK == rc) {
                    *acl = self;
                    return MA_OK;
                }
            }else {
                rc = MA_ERROR_UNEXPECTED;
            }
            free(self);
        }else {
            rc = MA_ERROR_OUTOFMEMORY;
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t  ma_acl_add(ma_acl_t *self, ma_bool_t allow, ma_int64_t permissions, ma_int64_t inheritance) {
    if(self) {
        PACL new_acl;
        DWORD error;
        /* Add a SID account to the Access Control List (ACL). The caller can specify:
 
            allow
                Whether to allow or deny access
            permissions
                Win32 bit values that control what the account can do
            inheritance
                Win32 bit values that control how access is inherited by child objects 
        */

        /* Put the values to add into this structure. */
        EXPLICIT_ACCESS access = {0};

        access.grfAccessPermissions = (DWORD)permissions;
        access.grfAccessMode = allow? GRANT_ACCESS: DENY_ACCESS;
        access.grfInheritance = (DWORD)inheritance;
        access.Trustee.TrusteeForm = TRUSTEE_IS_SID;
        access.Trustee.ptstrName = (LPWSTR)self->psid;

     /* You can't actually add a SID to an existing ACL. Instead, you have to make a new
        ACL, copy the old ACL's entries to it (plus the new one), then delete the old ACL.
        We use SetEntriesInAcl to do this. The one new entry is specified by the 'access'
        structure. */
        if(ERROR_SUCCESS != (error = SetEntriesInAcl (1, &access, self->acl, &new_acl))) 
            return MA_ERROR_UNEXPECTED;

        LocalFree(self->acl);
        self->acl = new_acl;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t  ma_acl_attach_object(ma_acl_t *self, const char *object_name, ma_acl_object_type_t object_type, ma_bool_t allow_inheritance) {
    if(self && object_name && (MA_ACL_OBJECT_FILE == object_type)) {
        SECURITY_INFORMATION information = DACL_SECURITY_INFORMATION;
        DWORD error ;
        information |= (MA_TRUE == allow_inheritance) ? UNPROTECTED_DACL_SECURITY_INFORMATION : PROTECTED_DACL_SECURITY_INFORMATION;

        if(ERROR_SUCCESS != (error = SetNamedSecurityInfoA( (char*)object_name, SE_FILE_OBJECT, information, NULL, NULL, self->acl,NULL)))
            return MA_ERROR_UNEXPECTED;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}
ma_error_t ma_acl_release(ma_acl_t *self) {
    if(self) {
        FreeSid(self->psid);
        LocalFree(self->acl);
        free(self);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t change_security_attributes_by_name(ma_acl_t *self, const char *sid_name) {
    OSVERSIONINFOEX Version = {0};
    ZeroMemory (&Version, sizeof (OSVERSIONINFOEX));
    Version.dwOSVersionInfoSize = sizeof (OSVERSIONINFOEX);
    if (GetVersionEx ((OSVERSIONINFO *)&Version)) {
        /* Just use LOCAL SERVICE account on XP/2K3 */
        if(Version.dwMajorVersion <6) {
            return MA_OK;
        }
        else {
            DWORD sidSize = 0;
            SID_NAME_USE use;
            DWORD domainSize = 0;
            char name[MA_MAX_PATH_LEN] = {0};
            char *domain = NULL;
            MA_MSC_SELECT(_snprintf, snprintf)(name, MA_MAX_PATH_LEN-1, "NT SERVICE\\%s", sid_name);
            FreeSid(self->psid);

            /* This will fail, but will return the buffer size for the SID. */
            LookupAccountNameA(NULL, name, NULL, &sidSize, NULL, &domainSize, &use);

            if(self->psid = (PSID)GlobalAlloc(GPTR, sidSize)) {
                if(domain = (char *)calloc(domainSize, sizeof(char))) {
                    /* This should really get the SID. */
                    if(LookupAccountNameA(NULL, name, self->psid, &sidSize, domain, &domainSize, &use)) {
                        free(domain), domain = NULL;
                        return MA_OK;
                    }
                    free(domain), domain = NULL;
                }
                GlobalFree(self->psid);
            }
        }
    }
    return MA_ERROR_UNEXPECTED;
}