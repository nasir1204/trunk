#include "ma/internal/utils/xml/ma_xml_node.h"
#include "mxml.h"

//TODO - without typecasting how to make ma_xml_node_t to mxml_node_t
typedef struct mxml_node_t ma_xml_node_s;


ma_error_t ma_xml_node_create(ma_xml_node_t *parent, const char *name, ma_xml_node_t **node) {
	if(parent && name && node) {
		mxml_node_t *temp_node = NULL;

		temp_node = mxmlNewElement((mxml_node_t*)parent, name);
		if(!temp_node) return MA_ERROR_XML_INTERNAL_ERROR;
        
		*node = (ma_xml_node_t *)temp_node;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_xml_node_release(ma_xml_node_t *self) {
	if(!self) return MA_ERROR_INVALIDARG;
	mxmlRelease((mxml_node_t *)self);
	return MA_OK;
}

ma_error_t ma_xml_node_delete(ma_xml_node_t *self) {
	if(!self) return MA_ERROR_INVALIDARG;
	mxmlRemove((mxml_node_t *)self);
	return MA_OK;
}


ma_xml_node_t *ma_xml_node_search(ma_xml_node_t *self, const char *name) {
	return self  && name ? (ma_xml_node_t *)mxmlFindElement((mxml_node_t*)self,(mxml_node_t*)self,name,NULL,NULL,MXML_DESCEND) : NULL;
}

ma_xml_node_t *ma_xml_node_search_by_path(ma_xml_node_t *self, const char *path) {
	if(self && path) {
		mxml_node_t *node = NULL ;
		node = mxmlFindPath((mxml_node_t *)self,path);
		
        if(node && (MXML_OPAQUE == mxmlGetType(node)))
			node = mxmlGetParent(node);

		return (ma_xml_node_t *)node;
	}
	return NULL;
}

//Set for xml node
ma_error_t ma_xml_node_set_name(ma_xml_node_t *self, const char *name) {
	if(self && name) 
		return (0 == mxmlSetElement((mxml_node_t*)self,name)) ? MA_OK : MA_ERROR_XML_INTERNAL_ERROR;
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_xml_node_set_data(ma_xml_node_t *self, const char *data) {
	if(self && data) {
		mxml_node_t *child = NULL;
		child = mxmlGetFirstChild((mxml_node_t *)self);
		if(child) {
            mxmlRemove(child);
			mxmlRelease(child);
        }
		return (NULL != mxmlNewOpaque((mxml_node_t*)self,data)) ? MA_OK :MA_ERROR_XML_INTERNAL_ERROR;
	}
	
	return MA_ERROR_INVALIDARG;
}

//Get for xml node
const char *ma_xml_node_get_name(ma_xml_node_t *self) {
	return self ? mxmlGetElement((mxml_node_t*)self) : NULL;
}

ma_bool_t ma_xml_node_has_data(ma_xml_node_t *self) {
	return (self && mxmlGetOpaque((mxml_node_t*)self)) ? MA_TRUE : MA_FALSE;
}

const char *ma_xml_node_get_data(ma_xml_node_t *self) {
    const char *value = NULL;
    /* Returns empty string if mxmlGetOpaque fails. it will fail if attribute value is "". What about other cases???? */
	return self ? ((value = mxmlGetOpaque((mxml_node_t*)self)) ? value : "") : NULL ;
}

//Set,get for xml node attributes
ma_error_t ma_xml_node_attribute_set(ma_xml_node_t *self, const char *name, const char *value) {
	if(!self || !name) return MA_ERROR_INVALIDARG;
	mxmlElementSetAttr((mxml_node_t*)self,name,value);
	return MA_OK;
}

ma_bool_t ma_xml_node_attribute_has(ma_xml_node_t *self, const char *name) {
	return (self && name && mxmlElementGetAttr((mxml_node_t*)self,name)) ? MA_TRUE: MA_FALSE;
}

const char *ma_xml_node_attribute_get(ma_xml_node_t *self, const char *name) {
	return (self && name) ? mxmlElementGetAttr((mxml_node_t*)self,name) : NULL;
}

ma_error_t ma_xml_node_attribute_del(ma_xml_node_t *self, const char *name) {
	if(self && name) {
		mxmlElementDeleteAttr((mxml_node_t*)self,name);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}


//iteration of nodes
//TODO - In iterative functions check for node is leaf node and return NULL ??? or anyway it will be taken care of
ma_xml_node_t *ma_xml_node_get_parent(ma_xml_node_t *self) {
	return self ? (ma_xml_node_t*)mxmlGetParent((mxml_node_t *)self) : NULL;
}

ma_xml_node_t *ma_xml_node_get_child(ma_xml_node_t *self) {
	return self ? (ma_xml_node_t*)mxmlGetFirstChild((mxml_node_t *)self) : NULL;
}

ma_xml_node_t *ma_xml_node_get_next(ma_xml_node_t *self) {
	if(self) {
		mxml_node_t *node = NULL ;
		node = mxmlWalkNext((mxml_node_t*)self,NULL,MXML_DESCEND);
		if(node && (MXML_OPAQUE == mxmlGetType(node)))
			node = mxmlWalkNext(node, NULL, MXML_DESCEND);
		return (ma_xml_node_t*)node;
	}
	return NULL;
	
}

ma_xml_node_t *ma_xml_node_get_prev(ma_xml_node_t *self) {
	if(self) {
		mxml_node_t *node = NULL ;
		node = mxmlWalkPrev((mxml_node_t*)self,NULL,MXML_DESCEND);
        if(node && (MXML_OPAQUE == mxmlGetType(node)))
			node = mxmlWalkPrev(node, NULL, MXML_DESCEND );
		return (ma_xml_node_t *)node;
	}
	return NULL;
}

ma_xml_node_t *ma_xml_node_get_next_sibling(ma_xml_node_t *self) {
	/*return self ? (ma_xml_node_t*)mxmlGetNextSibling((mxml_node_t*)self) : NULL;
    since OPAQUE callback is being used while loading, the call will return OPAQUE*/
    if(self) {
        mxml_node_t *node = NULL ;
        node = mxmlGetNextSibling((mxml_node_t*)self) ;
        if(node && (MXML_OPAQUE == mxmlGetType(node)))
            node = mxmlGetNextSibling(node) ;
        return (ma_xml_node_t *)node ;
    }
    return NULL ;
}

ma_xml_node_t *ma_xml_node_get_prev_sibling(ma_xml_node_t *self) {
	/*return self ? (ma_xml_node_t*)mxmlGetPrevSibling((mxml_node_t*)self) : NULL;
    since OPAQUE callback is being used while loading, the call will return OPAQUE */
    if(self) {
        mxml_node_t *node = NULL ;
        node = mxmlGetPrevSibling((mxml_node_t*)self) ;
        if(node && (MXML_OPAQUE == mxmlGetType(node)))
            node = mxmlGetPrevSibling(node) ;
        return (ma_xml_node_t *)node ;
    }
    return NULL ;
}


ma_xml_node_t *ma_xml_node_get_first_child(ma_xml_node_t *self) {
	/*return self ? (ma_xml_node_t*)mxmlGetFirstChild((mxml_node_t *)self) : NULL;
    since OPAQUE callback is being used while loading, the call will return OPAQUE*/
    if(self) {
        mxml_node_t *node = NULL ;
        node = mxmlGetFirstChild((mxml_node_t*)self) ;
        if(node && (MXML_OPAQUE == mxmlGetType(node)))
            node = mxmlGetNextSibling(node) ;
        return (ma_xml_node_t *)node ;
    }
    return NULL ;
}

ma_xml_node_t *ma_xml_node_get_last_child(ma_xml_node_t *self) {
	return self ? (ma_xml_node_t*)mxmlGetLastChild((mxml_node_t *)self) : NULL;
}

ma_int32_t ma_xml_node_get_child_count(ma_xml_node_t *self) {
	ma_xml_node_t *child = NULL;
	ma_int32_t  count = 0;
	
	if(self) {
		child = ma_xml_node_get_first_child(self);
		while(child != NULL) {
			count++;
			child = ma_xml_node_get_next_sibling(child);
		}
	}
	return count;
}

ma_xml_node_t *ma_xml_node_get_child_by_name(ma_xml_node_t *self, const char *name) {
	if(self && name) {
        ma_xml_node_t *child = NULL;
        child = ma_xml_node_get_first_child(self) ;
        while(child != NULL) {
            if(0 == strcmp(name, ma_xml_node_get_name(child)))
                break ;
            else
                child = ma_xml_node_get_next_sibling(child) ;
        }
        return child ;
    }
    return NULL ;
}
