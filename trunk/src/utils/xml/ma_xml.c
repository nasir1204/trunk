#include "ma/internal/utils/xml/ma_xml.h"

#include "mxml.h"

#include <stdio.h>
#include <stdlib.h>



//TODO - without typecasting how to make ma_xml_node_t to mxml_node_t
typedef struct mxml_node_t ma_xml_node_s;

struct ma_xml_s {
	ma_xml_node_t	*tree;
};


ma_error_t ma_xml_create(ma_xml_t **xml) {
	if(xml) {
		ma_xml_t *self = (ma_xml_t *)calloc(1,sizeof(ma_xml_t));

        /*Put the wrapping logic to 0*/
        mxmlSetWrapMargin(0);
		if(!self) return MA_ERROR_OUTOFMEMORY;
		
		*xml = self;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_xml_construct(ma_xml_t *self) {
	if(self) {
		self->tree = (ma_xml_node_t*)mxmlNewXML("1.0");
		return self->tree ? MA_OK: MA_ERROR_XML_INTERNAL_ERROR;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_xml_load_from_file(ma_xml_t *self, const char *path) {
	if(self && path) {
		FILE *fp = fopen(path,"r");

		if(!fp) return MA_ERROR_XML_FILE_NOT_FOUND;
        self->tree =  (ma_xml_node_t *)mxmlLoadFile(NULL, fp, MXML_OPAQUE_CALLBACK);
		fclose(fp);
		return self->tree ? MA_OK : MA_ERROR_XML_LOAD_FAILED; 
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_xml_load_from_buffer(ma_xml_t *self, const char *buffer, size_t len) {
	if(self && buffer && (len > 0)) {
        ma_error_t rc = MA_OK;
		ma_bool_t b_constructed = MA_FALSE;
        /* checking xml version information existance in received buffer, if there is no version, prepend the version in xml */
        if(memcmp(buffer, "<?xml", 5)) {
            rc = ma_xml_construct(self);
			b_constructed = MA_TRUE;
		}
        if(MA_OK == rc) {
			ma_xml_node_t *temp_node = NULL;
		    temp_node = (ma_xml_node_t *)mxmlLoadString((mxml_node_t*)self->tree, buffer, MXML_OPAQUE_CALLBACK);
			if(temp_node) {
				self->tree = temp_node;
			}
			else {
				if(b_constructed) {
					mxmlRelease((mxml_node_t *) self->tree);
					self->tree = NULL;
				}
			}
		    return temp_node ? MA_OK : MA_ERROR_XML_LOAD_FAILED; 
        }
        return rc;
	}
	return MA_ERROR_INVALIDARG;
}


ma_error_t ma_xml_save_to_file(ma_xml_t *self, const char *path) {
	if(self && path) {
		int result = 0;
		FILE *fp = fopen(path,"w");
		if(!fp) return MA_ERROR_XML_FILE_NOT_FOUND;

		result = mxmlSaveFile((mxml_node_t*)self->tree, fp, MXML_NO_CALLBACK);
		fclose(fp);
		return 0 == result? MA_OK:MA_ERROR_XML_SAVE_FAILED;
	}
	return MA_ERROR_INVALIDARG;
}

MA_XML_API ma_error_t ma_xml_save_to_buffer(ma_xml_t *self, ma_bytebuffer_t *buffer) {
	if(self && buffer) {
		char *ptr = NULL;
		ma_error_t rc = MA_OK;
		size_t len = 0;

        /*TODO - mxmlSaveAllocString internally makes 8192 size buffer, we need to reduce it to MACRO */
		if(NULL == (ptr = mxmlSaveAllocString((mxml_node_t *)self->tree,MXML_NO_CALLBACK))) 
			return MA_ERROR_XML_SAVE_FAILED;

		len = strlen(ptr);
		ma_bytebuffer_reserve(buffer,len+1);
		rc = ma_bytebuffer_set_bytes(buffer,0,(const unsigned char *)ptr,len);
		free(ptr);
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_xml_release(ma_xml_t *self) {
	if(self) {
		if(self->tree) mxmlRelease((mxml_node_t*)self->tree);		
		free(self);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_xml_node_t *ma_xml_get_node(ma_xml_t *self) {
	return (self && self->tree) ? self->tree : NULL;
}

