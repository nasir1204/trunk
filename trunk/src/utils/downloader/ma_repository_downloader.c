#include "ma/internal/utils/downloader/ma_repository_downloader.h"
#include "ma/internal/utils/repository/ma_repository_internal.h"
#include "ma/internal/utils/proxy/ma_proxy_internal.h"
#include "ma/internal/utils/network/ma_url_request.h"

#include "ma/internal/defs/ma_ah_defs.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal/ma_macros.h"
#include "ma/ma_client.h"
#include "ma/repository/ma_repository_client.h"

#include <stdio.h>

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "repository_downloader"

typedef enum ma_repo_spipe_failover_state_e {
	REPO_SPIPE_FQDN = 0,
	REPO_SPIPE_IP,
	REPO_SPIPE_SERVER,
	REPO_SPIPE_NONE
} ma_repo_spipe_failover_state_t;

typedef ma_error_t (*download_request_cb_t)(ma_repository_downloader_t *self);

struct ma_repository_downloader_s {
    ma_downloader_t                 base;

    ma_net_client_service_t         *net_client;

    ma_proxy_config_t               *proxy_config;

    ma_bool_t                       use_relay;

    ma_logger_t                     *logger;

	ma_ds_t							*ds;

	ma_context_t					*context;

    /* Repository info */
    ma_repository_t					*repository;
	
	ma_bool_t						is_async;

	char							url_path[MAX_PATH_LEN];     

	download_request_cb_t			download_req_cb;

    /* Download request info */
    char                            url[MAX_PATH_LEN]; /* TBD - Size */

    ma_stream_t                     *stream;

    size_t                          download_total;

    ma_on_download_progress_cb      download_cb;

    void                            *cb_data;

	/* URL request data */
	ma_url_request_t                *url_request;

	ma_proxy_list_t					*proxies;

    ma_url_request_io_t             io_streams;

    ma_url_request_progress_t       url_progress;

    ma_url_request_auth_t           auth_info;

	ma_url_request_ssl_info_t		ssl_info ;

	ma_repository_downloader_t		*downloader;

	/* custom header key value */
	char							*hdr_key;

	char							*hdr_value;

	ma_repo_spipe_failover_state_t	state;

	ma_bool_t						is_request_for_super_agent;
};

static ma_error_t ma_repository_downloader_sync_start(ma_downloader_t *downloader, const char *url, ma_stream_t *stream);
static ma_error_t ma_repository_downloader_start(ma_downloader_t *self, const char *url, ma_stream_t *stream, ma_on_download_progress_cb cb, void *cb_data);
static ma_error_t ma_repository_downloader_stop(ma_downloader_t *downloader);
static ma_error_t ma_repository_downloader_destroy(ma_downloader_t *downloader);

static const struct ma_downloader_methods_s downloader_methods = {
    &ma_repository_downloader_sync_start,
    &ma_repository_downloader_start,
    &ma_repository_downloader_stop,
    &ma_repository_downloader_destroy
};

static ma_error_t inner_download_start(ma_repository_downloader_t *self);
static ma_error_t get_auth_info(ma_repository_downloader_t *self, ma_url_request_auth_t *auth);
static ma_error_t get_ssl_info(ma_repository_downloader_t *self, ma_url_request_ssl_info_t *ssl_info);

ma_error_t ma_repository_downloader_create(ma_downloader_t **downloader) {
    if(downloader) {
        ma_repository_downloader_t *self = (ma_repository_downloader_t *)calloc(1, sizeof(ma_repository_downloader_t));
        if(self) {
            ma_downloader_init(&self->base, &downloader_methods, self);
            self->download_total = -1;
			MA_URL_SSL_INIT((&self->ssl_info));            
			MA_URL_AUTH_INIT((&self->auth_info));
            *downloader = (ma_downloader_t *)self;
            return MA_OK;
        }
        return MA_ERROR_OUTOFMEMORY;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_repository_downloader_destroy(ma_downloader_t *downloader) {
    ma_repository_downloader_t *self = (ma_repository_downloader_t *)downloader;
    if(self) {

        MA_LOG(self->logger, MA_LOG_SEV_TRACE, "Destroying <%s> repository downloader", self->url);
       
		MA_URL_SSL_DEINIT((&self->ssl_info));
		MA_URL_AUTH_DEINIT((&self->auth_info));

		if(self->hdr_key) free(self->hdr_key);
		if(self->hdr_value) free(self->hdr_value);
		
        if(self->url_request) 
			(void) ma_url_request_release(self->url_request);

        if(self->stream) (void)ma_stream_release(self->stream);
        if(self->proxy_config) (void)ma_proxy_config_release(self->proxy_config);
        if(self->repository) (void)ma_repository_release(self->repository);

        free(self);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_repository_downloader_set_net_client(ma_downloader_t *downloader, ma_net_client_service_t *net_client) {
    ma_repository_downloader_t *self = (ma_repository_downloader_t *)downloader;
    if(self && net_client) {
        self->net_client = net_client;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_repository_downloader_add_header(ma_downloader_t *downloader, const char *key, const char *value) {
	ma_repository_downloader_t *self = (ma_repository_downloader_t *)downloader;
    if(self && key && value) {
		self->hdr_key = strdup(key);
		self->hdr_value = strdup(value);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_repository_downloader_set_repository(ma_downloader_t *downloader, ma_repository_t *repository) {
    ma_repository_downloader_t *self = (ma_repository_downloader_t *)downloader;
    if(self && repository) {
        return ma_repository_add_ref(self->repository = repository);
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_repository_downloader_set_proxy_config(ma_downloader_t *downloader, ma_proxy_config_t *proxy_config) {
    ma_repository_downloader_t *self = (ma_repository_downloader_t *)downloader;
    if(self && proxy_config) {
        return ma_proxy_config_copy(proxy_config, &self->proxy_config);
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_repository_downloader_set_logger(ma_downloader_t *downloader, ma_logger_t *logger) {
    ma_repository_downloader_t *self = (ma_repository_downloader_t *)downloader;
    if(self && logger) {
        self->logger = logger;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_repository_downloader_set_ds(ma_downloader_t *downloader, ma_ds_t *ds) {
    ma_repository_downloader_t *self = (ma_repository_downloader_t *)downloader;
    if(self && ds) {
        self->ds = ds;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_repository_downloader_use_relay(ma_downloader_t *downloader, ma_bool_t use_relay) {
    ma_repository_downloader_t *self = (ma_repository_downloader_t *)downloader;
    if(self) {
        self->use_relay = use_relay;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_repository_downloader_set_context(ma_downloader_t *downloader, ma_context_t *context) {
    ma_repository_downloader_t *self = (ma_repository_downloader_t *)downloader;
    if(self && context) {
        self->context = context;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static void process_url(char *url) {
    char *p = url;
    while(*p) {
        if(('/' == *p) ||  ('\\' == *p))
            *p = '/';
        p++;
    }
}

static ma_error_t ma_repository_downloader_sync_start(ma_downloader_t *downloader, const char *url, ma_stream_t *stream) {
    ma_repository_downloader_t *self = (ma_repository_downloader_t *)downloader;
    if(self && url && 0 < strlen(url) && stream) {
        ma_error_t rc = MA_OK;

        MA_MSC_SELECT(_snprintf, snprintf)(self->url, MAX_PATH_LEN,"%s", url);
        ma_stream_add_ref(self->stream = stream);

        process_url(self->url);
        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "<%s> sync download starts", self->url);

		if(self->repository) {
			if((MA_OK == (rc = get_auth_info(self, &self->auth_info)) && ( MA_OK == (rc = get_ssl_info(self, &self->ssl_info))))) {
				ma_repository_url_type_t url_type = MA_REPOSITORY_URL_TYPE_END;
				(void)ma_repository_get_url_type(self->repository, &url_type);
				
				self->state = (MA_REPOSITORY_URL_TYPE_SPIPE == url_type || MA_REPOSITORY_URL_TYPE_SA == url_type) ? REPO_SPIPE_IP : REPO_SPIPE_FQDN;
				
				self->is_async = MA_FALSE;
				if(MA_OK != (rc = inner_download_start(self))) {
					if(MA_REPOSITORY_URL_TYPE_SPIPE == url_type || MA_REPOSITORY_URL_TYPE_SA == url_type) {
						self->state = REPO_SPIPE_FQDN;
						MA_LOG(self->logger, MA_LOG_SEV_TRACE, "Failover to FQDN for SPIPE/SA");
						if(MA_OK != (rc = inner_download_start(self))) {
							self->state = REPO_SPIPE_SERVER;
							MA_LOG(self->logger, MA_LOG_SEV_TRACE, "Failover to SERVER name for SPIPE/SA");
							rc = inner_download_start(self);
						}
					}
					
					if(MA_OK != rc) {
						const char *repo_name = NULL;
						(void) ma_repository_get_name(self->repository, &repo_name);
						MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to download file <%s> from repository <%s>, rc <%d>", url, (repo_name) ? repo_name : "", rc);
					}
				}
			}
			else
				MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to get suth info");
        }
        else {
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Repositories is not set");
            rc = MA_ERROR_UNEXPECTED; /* TBD - use new error code for this */
        }
     
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}


static ma_error_t ma_repository_downloader_start(ma_downloader_t *downloader, const char *url, ma_stream_t *stream, ma_on_download_progress_cb cb, void *cb_data) {
    ma_repository_downloader_t *self = (ma_repository_downloader_t *)downloader;
    if(self && url && cb) {
        ma_error_t rc = MA_OK;

		self->download_cb = cb;
        self->cb_data = cb_data;
        ma_stream_add_ref(self->stream = stream);
        MA_MSC_SELECT(_snprintf, snprintf)(self->url, MAX_PATH_LEN,"%s", url);

        process_url(self->url);
        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "<%s> async download starts", self->url);

		if(self->repository) {
			if((MA_OK == (rc = get_auth_info(self, &self->auth_info)) && ( MA_OK == (rc = get_ssl_info(self, &self->ssl_info))))) {
				ma_repository_url_type_t url_type = MA_REPOSITORY_URL_TYPE_END;
				(void)ma_repository_get_url_type(self->repository, &url_type);
				
				self->state = (MA_REPOSITORY_URL_TYPE_SPIPE == url_type || MA_REPOSITORY_URL_TYPE_SA == url_type) ? REPO_SPIPE_IP : REPO_SPIPE_FQDN;

				self->is_async = MA_TRUE;
				if(MA_OK != (rc = inner_download_start(self))) {
					const char *repo_name = NULL;
					(void) ma_repository_get_name(self->repository, &repo_name);
					MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to download file <%s> from repository <%s>, rc <%d>", url, (repo_name) ? repo_name : "", rc);
				}
			}
			else
				MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to get suth info");
        }
        else {
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Repositories is not set");
            rc = MA_ERROR_UNEXPECTED; /* TBD - use new error code for this */
        }

        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_repository_downloader_stop(ma_downloader_t *downloader) {
    ma_repository_downloader_t *self = (ma_repository_downloader_t *)downloader;
    if(self) {
		ma_error_t rc = MA_ERROR_INVALID_OPERATION;
        /* TBD - call user cb with state CANCLE */
        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "<%s> download stopping", self->url);
		if(self->url_request) {
			rc = ma_url_request_cancel(self->url_request);
		}
		return rc;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t set_url_path(ma_repository_downloader_t *self) {
    ma_error_t rc = MA_OK;
    ma_repository_url_type_t url_type = MA_REPOSITORY_URL_TYPE_END;
	ma_uint32_t ssl_port = 0;
	memset(&self->url_path, 0, MAX_PATH_LEN);
    if(MA_OK == (rc = ma_repository_get_url_type(self->repository, &url_type))) {
        switch(url_type) {    
		case MA_REPOSITORY_URL_TYPE_SPIPE:
			{	
				(void) ma_repository_get_ssl_port(self->repository, &ssl_port);
			}		
		case MA_REPOSITORY_URL_TYPE_SA:
			{
				self->is_request_for_super_agent = MA_TRUE;
			}
		case MA_REPOSITORY_URL_TYPE_HTTP:
            {
                const char *addr = NULL, *server_path = NULL;
                ma_uint32_t port = 80;
				if(REPO_SPIPE_FQDN == self->state)
					(void) ma_repository_get_server_fqdn(self->repository,&addr);
				else if(REPO_SPIPE_IP == self->state)
					(void) ma_repository_get_server_ip(self->repository,&addr);
				else if(REPO_SPIPE_SERVER == self->state)
					(void) ma_repository_get_server_name(self->repository,&addr);
				else {
					MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Unknown failover state");
					rc = MA_ERROR_UNEXPECTED;
					break;
				}
				
                (void) ma_repository_get_port(self->repository, &port);
                (void) ma_repository_get_server_path(self->repository, &server_path);
				
                if(addr && server_path)
                    MA_MSC_SELECT(_snprintf, snprintf)(self->url_path, MAX_PATH_LEN,"%s://%s:%d/%s/%s", ssl_port ? "https" : "http", addr, ssl_port? ssl_port:port, server_path, self->url);
                else
                    MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to get the server name and server path for url type <%d>", url_type);
            }
            break;
		
        case MA_REPOSITORY_URL_TYPE_FTP:
            {
                const char *server = NULL, *server_path = NULL;
                ma_uint32_t port = 80;
                (void)ma_repository_get_server_fqdn(self->repository,&server);
                (void)ma_repository_get_port(self->repository, &port);
                (void)ma_repository_get_server_path(self->repository, &server_path);
                if(server && server_path)
                    MA_MSC_SELECT(_snprintf, snprintf)(self->url_path, MAX_PATH_LEN,"ftp://%s:%d/%s/%s", server, port, server_path, self->url);
                else
                    MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to get the server name and server path for url type <%d>", url_type);
            }
            break;
        case MA_REPOSITORY_URL_TYPE_UNC:
            {
                const char *server = NULL, *server_path = NULL;
                (void)ma_repository_get_server_fqdn(self->repository,&server);
                (void)ma_repository_get_server_path(self->repository, &server_path);
                if(server && server_path)
                    MA_MSC_SELECT(_snprintf, snprintf)(self->url_path, MAX_PATH_LEN,"unc://%s/%s/%s", server, server_path, self->url);
                else
                    MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to get the server name and server path for url type <%d>", url_type);
            }
            break;
        case MA_REPOSITORY_URL_TYPE_LOCAL:
            {
                const char *server_path = NULL;
                (void)ma_repository_get_server_path(self->repository, &server_path);
                if(server_path)
                    MA_MSC_SELECT(_snprintf, snprintf)(self->url_path, MAX_PATH_LEN,"file://%s/%s", server_path, self->url);
                else
                    MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to get the server path for url type <%d>", url_type);
            }
            break;
        default:
            rc = MA_ERROR_UNEXPECTED;
            break;
        }
    }
    else
        MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to get url type, rc <%d>", rc);

    return rc;
}


static int download_progress_cb(double download_total, double download_now, double upload_total, double upload_now, void *user_data) {
	ma_repository_downloader_t *self = (ma_repository_downloader_t *)user_data;

    MA_LOG(self->logger, MA_LOG_SEV_TRACE, "File <%s> total download size <%f>, current downloaded size <%f>, remaining <%f> ", self->url, download_total, download_now, download_total-download_now);
    self->download_total = (size_t) download_total; /* TBD - possible loss of data. handle this */
	self->download_cb((ma_downloader_t *)self, MA_DOWNLOADER_DOWNLOAD_IN_PROGRESS, self->stream, self->download_total, self->cb_data);
    return 0;
}

//static ma_error_t download_connect_callback(void *user_data, ma_url_request_t *request) {
//    ma_repository_downloader_t *self = (ma_repository_downloader_t *)user_data;
//    ma_error_t rc = MA_OK;
//
//    MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "File <%s> download - Failed to connect repository, trying to connect next repository", self->url);
//
//    if(MA_OK != (rc = start_async_download(self)))
//        rc = self->cb((ma_downloader_t *)self, MA_DOWNLOADER_DOWNLOAD_FAILED, NULL, -1, self->cb_data);
//
//}

static void download_final_callback(ma_error_t status, long response_code, void *user_data, ma_url_request_t *request) {
    ma_error_t rc = MA_OK;
	ma_repository_downloader_t *self = (ma_repository_downloader_t *)user_data;
	ma_repository_url_type_t url_type = MA_REPOSITORY_URL_TYPE_END;

    MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "File <%s> download response code <%ld> status <%d>", self->url, response_code, status);

	if(self->proxies) 
		(void) ma_proxy_list_release(self->proxies), self->proxies = NULL;

    if(status == MA_ERROR_NETWORK_REQUEST_SUCCEEDED) {
		if(ma_url_request_get_result(request, response_code)) {
			rc = self->download_cb((ma_downloader_t *)self,  MA_DOWNLOADER_DOWNLOAD_DONE, self->stream, self->download_total, self->cb_data);
			return;
		}
    }
	
	(void)ma_repository_get_url_type(self->repository, &url_type);
	if(MA_REPOSITORY_URL_TYPE_SPIPE == url_type || MA_REPOSITORY_URL_TYPE_SA == url_type) {
		self->state = (REPO_SPIPE_IP == self->state) ? REPO_SPIPE_FQDN: ((REPO_SPIPE_FQDN == self->state) ? REPO_SPIPE_SERVER : REPO_SPIPE_NONE);
		if(REPO_SPIPE_NONE != self->state) {
			if(MA_OK != (rc = inner_download_start(self))) {
				const char *repo_name = NULL;
				(void) ma_repository_get_name(self->repository, &repo_name);
				MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to download file from repository <%s> in failover state <%d>, rc <%d>", (repo_name) ? repo_name : "", self->state, rc);
				rc = self->download_cb((ma_downloader_t *)self, MA_DOWNLOADER_DOWNLOAD_FAILED, NULL, -1, self->cb_data);
				return;
			}
		}
		else
			rc = self->download_cb((ma_downloader_t *)self, MA_DOWNLOADER_DOWNLOAD_FAILED, NULL, -1, self->cb_data);
	}
	else
		rc = self->download_cb((ma_downloader_t *)self, MA_DOWNLOADER_DOWNLOAD_FAILED, NULL, -1, self->cb_data);		
}

static ma_error_t get_ssl_info(ma_repository_downloader_t *self, ma_url_request_ssl_info_t *ssl_info) {
	ma_uint32_t ssl_port = 0;
	(void)ma_repository_get_ssl_port(self->repository, &ssl_port);
	if(ssl_port && self->ds) {
		ma_buffer_t *buffer = NULL;
		if((MA_OK == ma_ds_get_str(self->ds, MA_AH_POLICIES_SITELIST_PATH_NAME_STR, MA_AH_KEY_SITELIST_CA_FILE_STR , &buffer)  && buffer)) {
			const char *str = NULL;
			size_t size = 0;
			if((MA_OK == ma_buffer_get_string(buffer, &str, &size)) &&  str){
				MA_URL_AUTH_SET_SSL_INFO((&self->ssl_info), str);
			}
			(void)ma_buffer_release(buffer);
		}
		

	}
	return MA_OK;
}
static ma_error_t get_auth_info(ma_repository_downloader_t *self, ma_url_request_auth_t *auth) {
    ma_error_t rc = MA_OK;
    ma_repository_auth_t   auth_type = MA_REPOSITORY_AUTH_END;

    if(MA_OK == (rc = ma_repository_get_authentication(self->repository, &auth_type))) {
        switch(auth_type){
        case MA_REPOSITORY_AUTH_CREDENTIALS:
	        {
                ma_repository_url_type_t url_type = MA_REPOSITORY_URL_TYPE_END;
                const char *domain = NULL, *auth_user = NULL, *auth_passwd = NULL;
                if(MA_OK == (rc = ma_repository_get_url_type(self->repository, &url_type))) {
                    if(url_type == MA_REPOSITORY_URL_TYPE_UNC || url_type == MA_REPOSITORY_URL_TYPE_LOCAL){
				        if(MA_OK != (rc = ma_repository_get_domain_name(self->repository, &domain))){
                             MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to get domain name");
							 break;
						}
			        }

			        if(MA_OK == (rc = ma_repository_get_user_name(self->repository, &auth_user))) {
				        if(MA_OK == (rc = ma_repository_get_password(self->repository, &auth_passwd))) {
				            if(domain)
								auth->domainname = strdup(domain);
                            auth->user = strdup(auth_user);
			                auth->password = strdup(auth_passwd);
                        }
		            }
                }
	        }
	        break;
        case MA_REPOSITORY_AUTH_ANONYMOUS:
	        {
		        auth->user = strdup("anonymous");
		        auth->password = NULL;
	        }
	        break;

		case MA_REPOSITORY_AUTH_LOGGEDON_USER:
			{
				auth->use_loggedOn_user_credentials = MA_TRUE;
				auth->user = NULL;
				auth->password = NULL;
			}
			break;
        }
    }

    if(MA_OK != rc)
        MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to get auth info, rc <%d>", rc);

    return rc;
}

static ma_error_t proxy_get_cb(ma_error_t status, ma_client_t *ma_client, ma_proxy_list_t *system_proxy, void *cb_data) {
	ma_repository_downloader_t *self = (ma_repository_downloader_t *)cb_data;

	if(MA_OK == status) {
		MA_LOG(self->logger, MA_LOG_SEV_TRACE, "Got system proxy info");
		ma_proxy_list_add_ref(self->proxies = system_proxy);
	}
	else
		MA_LOG(self->logger, MA_LOG_SEV_TRACE, "No proxy info in system detected proxy, continue without proxy, status <%d>", status);

	self->download_req_cb(self);
	return MA_OK;
}

static ma_error_t read_proxy_info(ma_repository_downloader_t *self) {
    ma_error_t rc = MA_OK;
	ma_bool_t download_start_now = MA_TRUE;

    if(self->proxy_config) {
	    ma_proxy_usage_type_t usage = MA_PROXY_USAGE_TYPE_NONE;
	    if(MA_OK == (rc = ma_proxy_config_get_proxy_usage_type(self->proxy_config, &usage))) {
	        switch(usage){
		        case MA_PROXY_USAGE_TYPE_NONE:
			        break;
		        case MA_PROXY_USAGE_TYPE_SYSTEM_CONFIGURED:
					{
						MA_LOG(self->logger, MA_LOG_SEV_TRACE, "Requesting system proxy info");
						if(self->is_async) {							
							if(MA_OK == (rc = ma_repository_client_async_get_system_proxy(MA_CONTEXT_GET_CLIENT(self->context), self->url_path, proxy_get_cb, self)))
								download_start_now = MA_FALSE;
						}
						else {
							rc = ma_repository_client_get_system_proxy(MA_CONTEXT_GET_CLIENT(self->context), self->url_path, &self->proxies);
						}
					}
			        break;
		        case MA_PROXY_USAGE_TYPE_SERVER_CONFIGURED:
			        rc = ma_proxy_config_get_proxy_list(self->proxy_config, &self->proxies);
			        break;
	        }
        }

        if(MA_OK != rc)
            MA_LOG(self->logger, MA_LOG_SEV_TRACE, "No proxy info, continue without proxy, rc <%d>", rc);
    }

	if(download_start_now)
		self->download_req_cb(self);

    return MA_OK;
}

static ma_error_t download_file_request_cb(ma_repository_downloader_t *self) {
	ma_error_t rc = MA_OK;

	MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Downloading file <%s>", self->url_path);

	/* download can be invoked multiple times for IP, FQDN, short name so release the url request before creating the new one */
	/* TBD - can single url request object be used for all fallbacks ???? */
	if(self->url_request) 
			(void) ma_url_request_release(self->url_request), self->url_request = NULL;

	if(MA_OK == (rc = ma_url_request_create(self->net_client, self->url_path, &self->url_request))) {
		self->url_progress.progress_callback = download_progress_cb;
		MA_URL_REQUEST_IO_INIT((&self->io_streams));

		MA_URL_REQUEST_IO_SET_WRITE_STREAM((&self->io_streams), self->stream);
		(void)ma_url_request_set_option(self->url_request, MA_URL_REQUEST_OPTION_CONNECT_TIMEOUT, 60);
		(void)ma_url_request_set_option(self->url_request, MA_URL_REQUEST_OPTION_VERBOSE_LOG, 1);
		(void)ma_url_request_set_option(self->url_request, MA_URL_REQUEST_OPTION_TRANSFER_TIMEOUT, 30*60);
		(void)ma_url_request_set_option(self->url_request, MA_URL_REQUEST_OPTION_REQUEST_TYPE, MA_URL_REQUEST_TYPE_GET);
		(void)ma_url_request_set_io_callback(self->url_request, &self->io_streams);
		(void)ma_url_request_set_progress_callback(self->url_request, &self->url_progress);
		
		if(self->ssl_info.certificate_path) {
			(void)ma_url_request_set_ssl_info(self->url_request, &self->ssl_info);
		}
		//(void)ma_url_request_set_connect_callback(file_url_request->url_request, download_connect_callback);

		if(self->is_request_for_super_agent) /* This ensures that each request uses the fresh connection */
			(void)ma_url_request_set_option(self->url_request, MA_URL_REQUEST_OPTION_USE_FRESH_CONNECTION, 1);

		if(self->hdr_key && self->hdr_value)
			ma_url_request_set_custom_header(self->url_request, self->hdr_key, self->hdr_value);

		if(self->proxies)
			(void)ma_url_request_set_proxy_info(self->url_request, self->proxies);

		if(self->auth_info.user || self->auth_info.use_loggedOn_user_credentials)
			(void)ma_url_request_set_url_auth_info(self->url_request, &self->auth_info);

		rc = (self->is_async) ?  ma_url_request_send(self->url_request, download_final_callback, self) :ma_url_request_sync_send(self->url_request, self);

		MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "URL <%s> download request rc <%d>", self->url_path, rc);

		rc = (MA_ERROR_NETWORK_REQUEST_SUCCEEDED == rc) ? MA_OK : rc;
	}

	if(!self->is_async) {
		if(self->proxies) 
			(void) ma_proxy_list_release(self->proxies), self->proxies = NULL;
	}

	return rc;
}

static ma_error_t inner_download_start(ma_repository_downloader_t *self) {
    ma_error_t rc = MA_OK;
	
	if(MA_OK == (rc = set_url_path(self))) {
		self->download_req_cb = download_file_request_cb;

		/* see you in download_file_request_cb callback */
		(void)read_proxy_info(self);
	}

    return rc;
}
