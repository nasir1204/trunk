#include "ma_mue_process.h"
#include "ma/internal/defs/ma_mue_defs.h"

#include "ma/ma_msgbus.h"
#include "ma/internal/core/msgbus/ma_msgbus_private.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/filesystem/path.h"

#include "ma/ma_log.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#if !defined(MA_WINDOWS)
#include <unistd.h>
#endif
#include <uv.h>


#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "Updater"

struct ma_mue_process_s{
	ma_context_t		*context;

	uv_process_t		mue_process_handle;

	mue_exit_cb			cb;

	void				*user_data;

	ma_bool_t           is_spawned;

	int					ref_count;
};

ma_error_t ma_mue_process_create(ma_context_t *context, ma_mue_process_t **mue_process){	
	if(context && mue_process){
		ma_mue_process_t *self  = (ma_mue_process_t*)calloc(1, sizeof(ma_mue_process_t));
		if(self){
			self->context			 = context;
			self->is_spawned		 = MA_FALSE;
			self->ref_count			 = 1;
			*mue_process			 = self;
			return MA_OK;			
		}
		return MA_ERROR_INVALIDARG;
	}
	return MA_ERROR_INVALIDARG;

}

static void final_call(uv_handle_t* handle) {
	ma_mue_process_t *self = (ma_mue_process_t*)handle->data;
	MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Finally releasing the updater engine process handle.");
	free(self);
}

ma_error_t ma_mue_process_release(ma_mue_process_t *self){
	if(self){		
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Releasing the updater engine process handle, ref count = %d, is spawned <%d>.", self->ref_count, self->is_spawned);
		if(!(--self->ref_count)) {
            if(self->is_spawned) {
			    uv_close((uv_handle_t*)(&self->mue_process_handle), final_call); 					
            }
            else {
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Releasing the updater engine process.");
                free(self);
            } 
		    return MA_OK;
        }
	}
	return MA_ERROR_INVALIDARG;
}

#ifdef MA_WINDOWS
static ma_bool_t is_mue_running(ma_mue_process_t *self){	
	HANDLE handle = NULL;	
    ma_bool_t status = self->is_spawned;
	if(self->is_spawned){
		handle = OpenEventW(SYNCHRONIZE | EVENT_MODIFY_STATE, MA_FALSE, MA_MUE_SHUTDOWN_GLOBAL_EVENT_NAME_STR) ;    	
		if(handle){
			status = MA_TRUE;
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Updater engine process running.");
			CloseHandle(handle);			
		}
		else{
			status = MA_FALSE;
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Updater engine process is not running.");		
		}
	}
	return status;
}

static ma_bool_t signal_mue_shutdown(ma_mue_process_t *self){	
	HANDLE shutdown_handle = NULL;	
	shutdown_handle = OpenEventW(SYNCHRONIZE | EVENT_MODIFY_STATE, MA_FALSE, MA_MUE_SHUTDOWN_GLOBAL_EVENT_NAME_STR) ;    	
	if(shutdown_handle){
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Updater engine process running, firing shutdown event.");		
		SetEvent(shutdown_handle);
		CloseHandle(shutdown_handle);		
	}	
	return MA_TRUE;
}
#else
static ma_bool_t is_mue_running(ma_mue_process_t *self){
	return self->is_spawned;
}
static ma_bool_t signal_mue_shutdown(ma_mue_process_t *self){	
	ma_mue_process_terminate(self);
        return MA_TRUE;
}
#endif

ma_error_t ma_mue_process_status(ma_mue_process_t *self, ma_bool_t *is_running){
	if(self && is_running){
		*is_running = is_mue_running(self);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

static void mue_process_exit_cb(uv_process_t* process, int exit_status, int term_signal){
	ma_mue_process_t *self = (ma_mue_process_t*)process->data;
	if(self){ /*mue_process is not released so we will follow*/
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Updater engine process exit callback received");
		if(self->cb){ /*User interested in the exit callback.*/
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Calling the updater engine session callback.");
			self->cb(self, exit_status, term_signal, self->user_data);	
		}			
	}
	/*Process handle should be free always in the exit callback.*/
	ma_mue_process_release(self); 	
}

ma_error_t ma_mue_process_start(ma_mue_process_t *self, const char *exe_path, const char **args, void *user_data, mue_exit_cb exit_cb){
	if(self && exe_path && args && exit_cb){
		uv_process_options_t	process_options = {0};
		uv_stdio_container_t child_stdio[3] ;				
		ma_msgbus_t *bus	= MA_CONTEXT_GET_MSGBUS(self->context);
		uv_loop_t *uv_loop  = NULL;
		if(!bus)
			return MA_ERROR_INVALID_CONTEXT;

		uv_loop = ma_event_loop_get_uv_loop(ma_msgbus_get_event_loop(bus));

		process_options.file = exe_path;
		process_options.args = (char**)args;
		process_options.exit_cb = mue_process_exit_cb;
		process_options.stdio_count = 3 ;
		process_options.env = NULL;
		process_options.flags = 0;
		
		/*TODO - I just blindly copioed this but I am not comaortable with stdio flags */
		child_stdio[0].flags = child_stdio[1].flags = UV_IGNORE ;	        
		child_stdio[2].flags = UV_IGNORE ; 
		child_stdio[2].data.fd = 2 ;
		process_options.stdio = child_stdio ;
		
		self->mue_process_handle.data = self;			
		if(0 != uv_spawn(uv_loop, &self->mue_process_handle, process_options)) {                
   			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to spawn updater engine process,%s.",uv_strerror(uv_last_error(uv_loop)));			
   			return MA_ERROR_UPDATER_ENGINE_SPAWN_FAILED;
   		}
          
		/*increase reference count*/
		++self->ref_count;

		#ifdef MA_WINDOWS
		if(self->mue_process_handle.spawn_error.code != UV_OK )
		#else
		if(self->mue_process_handle.errorno)
		#endif                
		{			
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Updater engine spawn failed,%s.",uv_strerror(uv_last_error(uv_loop)));
   			return MA_ERROR_UPDATER_ENGINE_SPAWN_FAILED ;
		}			
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Successfully launched updater engine, pid <%d>", self->mue_process_handle.pid);

		/*set all the upper layer data.*/		  		
		self->user_data  = user_data;
		self->is_spawned = MA_TRUE;
		self->cb		 = exit_cb;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_mue_process_stop(ma_mue_process_t *self){
	if(self){		
		int i = 0;
		self->cb = NULL; /*Let it die its own, uppper layer don't interested in it.*/
		
		if(!is_mue_running(self))
			return MA_OK;

		signal_mue_shutdown(self);
		while(++i < 10){
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO, "Trying to stop Updater engine process.");
			MA_MSC_SELECT(Sleep,sleep)(MA_MSC_SELECT(1000, 1));
			if(!is_mue_running(self))
				return MA_OK ;	
		}		
		return MA_ERROR_UNEXPECTED;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_mue_process_terminate(ma_mue_process_t *self){
	if(self){
		if(is_mue_running(self) && self->mue_process_handle.pid > 0){
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Terminating Updater engine process, pid <%d>", self->mue_process_handle.pid);
			uv_process_kill(&self->mue_process_handle, SIGTERM) ;
			self->is_spawned = MA_FALSE;
		}
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

