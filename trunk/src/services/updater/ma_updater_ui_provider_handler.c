#include "ma_updater_ui_provider_handler.h"
#include "ma/internal/utils/datastructures/ma_slist.h"
#include "ma/internal/defs/ma_object_defs.h"

#include <string.h>
#include <stdlib.h>


typedef struct ma_updater_ui_provider_info_s ma_updater_ui_provider_info_t ;

struct ma_updater_ui_provider_info_s {
	/*next link*/
	MA_SLIST_NODE_DEFINE(ma_updater_ui_provider_info_t);

    /* service name */
    char *provider_name ;
} ;

struct ma_updater_ui_provider_handler_s {
    MA_SLIST_DEFINE(ui_providers, ma_updater_ui_provider_info_t) ;

    ma_context_t    *context ;
} ;

ma_error_t ma_updater_ui_provider_info_create(const char *provider_name, ma_updater_ui_provider_info_t **provider_info) ;
ma_error_t ma_updater_ui_provider_info_release(ma_updater_ui_provider_info_t *provider_info) ;




ma_error_t ma_updater_ui_provider_handler_create(ma_context_t *context, ma_updater_ui_provider_handler_t **ui_provider_handler) {
    if(ui_provider_handler) {
        ma_updater_ui_provider_handler_t *tmp =  (ma_updater_ui_provider_handler_t *)calloc(1, sizeof(ma_updater_ui_provider_handler_t)) ;
        if(tmp) {
            *ui_provider_handler = tmp ;
            return MA_OK ;
        }
        return MA_ERROR_OUTOFMEMORY ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_updater_ui_provider_handler_release(ma_updater_ui_provider_handler_t *self) {
    if(self) {
        MA_SLIST_CLEAR(self, ui_providers, ma_updater_ui_provider_info_t, ma_updater_ui_provider_info_release) ;
        free(self) ;
        self = NULL ;
    }
    return MA_OK ;
}

static ma_error_t ma_updater_ui_provider_handler_get_provider_info(ma_updater_ui_provider_handler_t *self, const char * provider_name, ma_updater_ui_provider_info_t **provider) {
    ma_updater_ui_provider_info_t *tmp = NULL ;
    if(self && provider_name) {
        MA_SLIST_FOREACH(self, ui_providers, tmp) {
            if(0 == strcmp(tmp->provider_name, provider_name)) {
				*provider = tmp ;
                break ;
			}
        }
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_updater_ui_provider_handler_get_provider(ma_updater_ui_provider_handler_t *self, char **provider_name) {
    if(self && provider_name) {
        ma_updater_ui_provider_info_t *tmp = MA_SLIST_GET_HEAD(self, ui_providers) ;
		if(tmp){
    		*provider_name = tmp->provider_name ;
			return MA_OK ;
		}
		return MA_ERROR_OBJECTNOTFOUND;
    }
    return MA_ERROR_INVALIDARG ;
}


ma_error_t ma_updater_ui_provider_handler_add_provider(ma_updater_ui_provider_handler_t *self, const char * provider_name) {
    if(self && provider_name) {
        ma_updater_ui_provider_info_t *provider_info = NULL ;
        ma_error_t rc = MA_OK ;
    	if(MA_OK == (rc = ma_updater_ui_provider_handler_get_provider_info(self, provider_name, &provider_info))) {
    		if((!provider_info) && MA_OK == (rc = ma_updater_ui_provider_info_create(provider_name, &provider_info)))
    			MA_SLIST_PUSH_BACK(self, ui_providers, provider_info) ;
    	}
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_updater_ui_provider_handler_remove_provider(ma_updater_ui_provider_handler_t *self, const char * provider_name) {
    if(self && provider_name) {
        ma_updater_ui_provider_info_t *provider_info = NULL ;
        MA_SLIST_FOREACH(self, ui_providers, provider_info) {
            if(0 == strcmp(provider_info->provider_name, provider_name)) {
                MA_SLIST_REMOVE_NODE(self, ui_providers, provider_info, ma_updater_ui_provider_info_t) ;
                ma_updater_ui_provider_info_release(provider_info) ;
                break ;
            }
        }
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}
ma_error_t ma_updater_ui_provider_handler_get_providers(ma_updater_ui_provider_handler_t *self, ma_array_t **providers) {
    if(self && providers) {
        ma_array_t *tmp = NULL ;
        ma_error_t rc = MA_OK ;
        if(MA_OK == (rc = ma_array_create(&tmp))) {
            ma_updater_ui_provider_info_t *provider_info = NULL ;
            ma_variant_t *var_provider = NULL ;

            MA_SLIST_FOREACH(self, ui_providers, provider_info) {
                ma_variant_create_from_string(provider_info->provider_name, strlen(provider_info->provider_name), &var_provider) ;
                ma_array_push(tmp, var_provider) ;
                ma_variant_release(var_provider) ; var_provider = NULL ;
            }
            *providers = tmp ;
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_updaer_ui_provider_handler_clear_providers(ma_updater_ui_provider_handler_t *self) {
    if(self){
        MA_SLIST_CLEAR(self, ui_providers, ma_updater_ui_provider_info_t, ma_updater_ui_provider_info_release) ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_updater_ui_provider_info_create(const char *provider_name, ma_updater_ui_provider_info_t **provider_info) {
    if(provider_name && provider_info) {
        ma_updater_ui_provider_info_t *provider_info_tmp = NULL ;
        if(NULL != ( provider_info_tmp = (ma_updater_ui_provider_info_t *)calloc(1, sizeof(ma_updater_ui_provider_info_t)))) {
            provider_info_tmp->provider_name = strdup(provider_name) ;
            *provider_info = provider_info_tmp ;
            return MA_OK ;
        }
        return MA_ERROR_OUTOFMEMORY ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_updater_ui_provider_info_release(ma_updater_ui_provider_info_t *provider_info) {
    if(provider_info) {
        if(provider_info->provider_name) free(provider_info->provider_name) ;
        free(provider_info) ;
        provider_info = NULL ;
    }
    return MA_OK ;
}