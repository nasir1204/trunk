#include "ma_updater_request.h"

#include "ma/scheduler/ma_task.h"
#include "ma/internal/services/scheduler/ma_scheduler_core.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"
#include "ma/internal/defs/ma_scheduler_defs.h"
#include "ma/internal/services/io/ma_dc_task_service.h"

#include "ma/ma_log.h"

#include <stdlib.h>
#include <string.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "Updater"

static const ma_updater_request_methods_t methods = {0, 0, 0, 0, 0};

extern ma_error_t ma_updater_request_dc_create(ma_context_t *context, ma_updater_request_t **request);
extern ma_error_t ma_updater_request_task_create(ma_context_t *context, ma_updater_request_t **request);
extern ma_error_t ma_updater_request_ondemand_create(ma_context_t *context, ma_updater_request_t **request);
ma_error_t ma_updater_update_stop_request_create(ma_context_t *context, ma_updater_request_param_t *params, ma_updater_request_t **request);

ma_error_t ma_updater_update_stop_request_create(ma_context_t *context, ma_updater_request_param_t *params, ma_updater_request_t **request){
	if(context && params && request){
		ma_updater_request_t *self = NULL;		        
        self = (ma_updater_request_t *)calloc(1, sizeof(ma_updater_request_t));
        if(!self) 
			return MA_ERROR_OUTOFMEMORY;
		ma_updater_request_init( (ma_updater_request_t*)self, &methods, self);		
		self->params.initiator_type = params->initiator_type;
		self->params.request_type = params->request_type;
		strncpy(self->params.task_id, params->task_id, MA_MAX_TASKID_LEN);				
		*request = (ma_updater_request_t*)self;		
        return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

static ma_bool_t is_dc_task(ma_context_t *context, ma_updater_request_param_t *params){
	ma_error_t rc = MA_OK;
	ma_task_t  *task = NULL;
    ma_bool_t ret_val = MA_FALSE;
	
	if(MA_OK == (rc = ma_scheduler_get_task_handle(MA_CONTEXT_GET_SCHEDULER(context), params->task_id, &task))){
		ma_variant_t *dc_task_setting_value = NULL;
		if(MA_OK == (rc = ma_task_get_setting(task, DC_SCHEDULED_TASK_SECTION_NAME_STR, DC_SCHEDULED_TASK_SETTING_NAME_STR, &dc_task_setting_value))){
			ma_variant_release(dc_task_setting_value);
			dc_task_setting_value = NULL;
			ret_val = MA_TRUE;
		}
        (void)ma_task_release(task);
	}	
	return ret_val;
}

static ma_bool_t is_ondemand_task(ma_context_t *context, ma_updater_request_param_t *params){
	return (params->initiator_type == MA_UPDATER_INITIATOR_TYPE_ONDEMAND_UPDATE);	
}


ma_error_t ma_updater_request_factory(ma_context_t *context, ma_updater_request_param_t *params, ma_updater_request_t **out_request){
    if(context && params && out_request){
		if(MA_UPDATER_REQUEST_TYPE_STOP == params->request_type)
			return ma_updater_update_stop_request_create(context, params, out_request);		
		else{
			ma_error_t rc = MA_OK;
			/*Create an appropriate request.*/
			if(is_ondemand_task(context, params)){
				ma_updater_request_t *req = NULL;
				MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_INFO, "Creating on Demand updater request, task id = %s.", params->task_id);					
				if(MA_OK == (rc = ma_updater_request_ondemand_create(context, &req))){
					if(MA_OK == (rc = ma_updater_request_set_param(req, params))){
						*out_request = req;
						return rc;
					}
					ma_updater_request_release(req);
					MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_ERROR, "Failed to create on Demand updater request, task id = %s.", params->task_id);														
				}
			}
			else if(is_dc_task(context, params)){
				ma_updater_request_t *req = NULL;
				MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_INFO, "Creating run now updater request, task id = %s.", params->task_id);					
				if(MA_OK == (rc = ma_updater_request_dc_create(context, &req))){
					if(MA_OK == (rc = ma_updater_request_set_param(req, params))){
						*out_request = req;
						return rc;
					}
					ma_updater_request_release(req);
					MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_ERROR, "Failed to create run now updater request, task id = %s.", params->task_id);														
				}
			}
			else{
				ma_updater_request_t *req = NULL;
				MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_INFO, "Creating scheduled task updater request, task id = %s.", params->task_id);					
				if(MA_OK == (rc = ma_updater_request_task_create(context, &req))){
					if(MA_OK == (rc = ma_updater_request_set_param(req, params))){
						*out_request = req;
						return rc;
					}
					ma_updater_request_release(req);
					MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_ERROR, "Failed to create scheduled task updater request, task id = %s.", params->task_id);														
				}
			}
			return rc;
		}		
    }
    return MA_ERROR_INVALIDARG;
}



