#ifndef MA_MUE_PROCESS_H_INCLUDED
#define MA_MUE_PROCESS_H_INCLUDED


#include "ma/ma_common.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/defs/ma_updater_defs.h"

MA_CPP(extern "C" {)

typedef struct ma_mue_process_s ma_mue_process_t, *ma_mue_process_h;
typedef void (*mue_exit_cb)(ma_mue_process_t *session, int exit_status, int term_signal, void *cb_data);

ma_error_t ma_mue_process_create(ma_context_t *context, ma_mue_process_t **session);

ma_error_t ma_mue_process_status(ma_mue_process_t *self, ma_bool_t *is_running);

ma_error_t ma_mue_process_start(ma_mue_process_t *self, const char *exe_path, const char **args, void *user_data, mue_exit_cb exit_cb);

ma_error_t ma_mue_process_stop(ma_mue_process_t *self);

ma_error_t ma_mue_process_terminate(ma_mue_process_t *self);

ma_error_t ma_mue_process_release(ma_mue_process_t *self);

MA_CPP(})

#endif

