#include "ma/internal/services/updater/ma_updater_utils.h"

#include "ma/ma_log.h"

#include <stdlib.h>
#include <string.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "Updater"

ma_error_t ma_updater_request_param_to_variant(ma_updater_request_param_t *self, ma_variant_t **variant) {
    if(self && variant) {
        ma_array_t *array = NULL;
        ma_error_t rc = MA_OK;

        if(MA_OK == (rc = ma_array_create(&array))) {
            ma_variant_t *temp_variant = NULL;
            if(MA_OK == (rc = ma_variant_create_from_int16(self->request_type, &temp_variant))) {
                (void)ma_array_push(array, temp_variant);
                (void)ma_variant_release(temp_variant);
            }
            if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_int16(self->initiator_type, &temp_variant)))) {
                (void)ma_array_push(array, temp_variant);
                (void)ma_variant_release(temp_variant);
            }
            if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(self->task_id, MA_MAX_TASKID_LEN, &temp_variant)))) {
                (void)ma_array_push(array, temp_variant);
                (void)ma_variant_release(temp_variant);
            }
            if(MA_OK == rc) 
                rc = ma_variant_create_from_array(array, variant);

            (void)ma_array_release(array);
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_updater_request_param_from_variant(ma_variant_t *variant, ma_updater_request_param_t **request) {
    if(variant && request) {
       ma_error_t rc = MA_OK;
       ma_array_t *array = NULL;
       ma_updater_request_param_t *self = NULL;

       self = (ma_updater_request_param_t *)calloc(1, sizeof(ma_updater_request_param_t));
       if(!self) return MA_ERROR_OUTOFMEMORY;

       if(MA_OK == (rc = ma_variant_get_array(variant, &array))) {
		   int index = 0;
           ma_variant_t *temp_variant = NULL;

           if(MA_OK == (rc = ma_array_get_element_at(array, ++index, &temp_variant))) {

			   ma_int16_t int16 = 0;
               if(MA_OK == (rc = ma_variant_get_int16(temp_variant, &int16)))
					self->request_type = (ma_updater_request_type_t)int16;			   
               (void)ma_variant_release(temp_variant);
           }
           if((MA_OK == rc) && (MA_OK == (rc = ma_array_get_element_at(array, ++index, &temp_variant)))) {

               ma_int16_t int16 = 0;
               if(MA_OK == (rc = ma_variant_get_int16(temp_variant, &int16)))
					self->initiator_type = (ma_updater_initiator_type_t)int16;			   
               (void)ma_variant_release(temp_variant);
           }

           if((MA_OK == rc) && (MA_OK == (rc = ma_array_get_element_at(array, ++index, &temp_variant)))) {

               ma_buffer_t *buffer = NULL;
               if(MA_OK == (rc = ma_variant_get_string_buffer(temp_variant, &buffer))) {
                   const char *value = NULL;
                   size_t size = 0;
                   if(MA_OK == (rc = ma_buffer_get_string(buffer, &value, &size))) {
                       strncpy(self->task_id,value, MA_MAX_TASKID_LEN);
                   }
                   (void)ma_buffer_release(buffer);
               }
               (void)ma_variant_release(temp_variant);
           }           
           (void)ma_array_release(array);
       }
	   if(MA_OK == rc) 
          *request = self;
		else 
		  free(self);
       return rc;
    }
    return MA_ERROR_INVALIDARG;
}

void ma_updater_request_param_release(ma_updater_request_param_t *self){
	if(self) free(self);	
}

