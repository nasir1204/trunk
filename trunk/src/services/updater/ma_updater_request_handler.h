#ifndef MA_UPDATER_REQUEST_HANDLER_H_INCLUDED
#define MA_UPDATER_REQUEST_HANDLER_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma_updater_request.h"

MA_CPP(extern "C" {)

typedef struct ma_updater_request_handler_s ma_updater_request_handler_t, *ma_updater_request_handler_h;

ma_error_t ma_updater_request_handler_create(ma_context_t *ma_context, ma_updater_request_handler_t **handler);

ma_error_t ma_updater_request_handler_release(ma_updater_request_handler_t *handler);

ma_error_t ma_updater_request_handler_start(ma_updater_request_handler_t *handler);

ma_error_t ma_updater_request_handler_stop(ma_updater_request_handler_t *handler);

ma_error_t ma_updater_request_handler_get_status(ma_updater_request_handler_t *handler, ma_bool_t *busy);

/*
1) Updater start request is accepted by the request handler then updater request handler owns the request and will free it.
2) Updater stop request is never owns by the request handler, so user has to free it.
*/
ma_error_t ma_updater_request_handler_submit_request(ma_updater_request_handler_t *handler, ma_updater_request_t *request);

ma_error_t ma_updater_request_handler_check_state(ma_updater_request_handler_t *handler, const char *session_id, ma_update_state_t *state);

MA_CPP(})

#endif  /* MA_UPDATER_REQUEST_HANDLER_H_INCLUDED */

