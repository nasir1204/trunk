#include "ma_mue_session.h"
#include "ma_mue_process.h"

#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/core/msgbus/ma_msgbus_private.h"

#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/filesystem/path.h"

#include "ma/ma_log.h"
#include "ma/internal/defs/ma_scheduler_defs.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/services/scheduler/ma_scheduler_core.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"
#include "ma/internal/utils/platform/ma_system_property.h"
#include "ma/internal/utils/filesystem/ma_fs_utils.h"

#include "ma/internal/services/ma_policy_settings_bag.h"

#include "ma/internal/ma_strdef.h"

#include "ma_updater_ui_provider_handler.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>

#ifndef MA_WINDOWS
#include <unistd.h>
#endif

#include <uv.h>


#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "Updater"


#ifdef MA_WINDOWS
#define MA_MUE_PROCESS_NAME                                         "Mue.exe"
/*Still use McScript name to get VSE 8.5 access protection.*/
#define MA_MUE_IN_USE_PROCESS_NAME                                  "McScript_InUse.exe"
#else
#define MA_MUE_PROCESS_NAME                                         "Mue"
/*Still use McScript name to get VSE 8.5 access protection.*/
#define MA_MUE_IN_USE_PROCESS_NAME                                  "Mue_InUse"
#endif

#ifdef MA_WINDOWS
#define MA_MUE_LOG_FILE_NAME_STR                                    "McScript"
#else
#define MA_MUE_LOG_FILE_NAME_STR                                    "McScript.log"
#endif

#define MA_MUE_UPDATE_SCRIPT_NAME_STR                               "UpdateMain.McS"

#define MA_MUE_DEPLOYMENT_SCRIPT_NAME_STR                           "InstallMain.McS"

/*Update engine timeout 60 minutes*/
#define MA_MUE_TIMEOUT_VALUE_INT                                    (2 * 60 * 60 * 1000)

#define    MA_UPDATE_DIRECTORY_NAME_STR								"update"

struct task_settings{
	char *name;
	char *creator;
	ma_int32_t locale;
	char *ui_provider;
};

struct ma_mue_session_s {
    ma_context_t                            *ma_context;

    const ma_updater_request_t				*updater_request;

	ma_mue_event_listener_t                 *listener;

    ma_bool_t                               is_mue_process_running;

    ma_mue_process_t			            *mue_process_handle;
    
    uv_timer_t                              mue_process_timer_handle;

    mue_session_exit_cb_t			        mue_session_exit_cb; 
		
    void                                    *cb_data;

	struct task_settings					settings;
};

ma_error_t ma_mue_session_create(ma_context_t *context, ma_mue_session_t **session){
    if(context  && session) {
        ma_mue_session_t *self = NULL;
        ma_error_t rc = MA_OK;
        
        self = (ma_mue_session_t *)calloc(1, sizeof(ma_mue_session_t));
        if(self){            			
			self->ma_context = context;		
			uv_timer_init(ma_event_loop_get_uv_loop(ma_msgbus_get_event_loop(MA_CONTEXT_GET_MSGBUS(self->ma_context))), &self->mue_process_timer_handle);				
			self->mue_process_timer_handle.data  = self;
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_DEBUG, "uv timer data(%p)\n", self);
			*session = self;
			return rc;
		}
		return MA_ERROR_OUTOFMEMORY;
        
    }
    return MA_ERROR_INVALIDARG;
}

static void timer_close_cb(uv_handle_t *handle) {
	ma_mue_session_t *self = (ma_mue_session_t *)handle->data;
	if(self->settings.name) free(self->settings.name);
	if(self->settings.creator) free(self->settings.creator);
	if(self->settings.ui_provider) free(self->settings.ui_provider);
	free(self);
}

ma_error_t ma_mue_session_release(ma_mue_session_t *self){
    if(self) {			
		uv_timer_stop(&self->mue_process_timer_handle);		
		if(self->mue_process_handle) ma_mue_process_release(self->mue_process_handle);
		if(self->listener) ma_mue_event_listener_release(self->listener);		
		uv_close((uv_handle_t *)&self->mue_process_timer_handle, timer_close_cb);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_mue_event_listener_t *ma_mue_session_get_event_listener(ma_mue_session_t *self){
	return self ? self->listener : NULL;
}

static void mue_process_timer_timeout_cb(uv_timer_t* handle, int status);
static ma_error_t start_mue_process(ma_mue_session_t *self);

static ma_error_t get_task_info(ma_mue_session_t *self){
	ma_task_t *task = NULL;
	ma_error_t rc = MA_OK;
	if(MA_OK == (rc = ma_scheduler_get_task_handle(MA_CONTEXT_GET_SCHEDULER(self->ma_context), self->updater_request->params.task_id, &task))){
		const char *str = NULL;
		size_t	size = 0;
		ma_variant_t *var = NULL;

		if(MA_OK == ma_task_get_name(task, &str)){
			self->settings.name = strdup(str);
			str = NULL;
		}

		if(MA_OK == ma_task_get_creator_id(task, &str)){
			self->settings.creator = strdup(str);
			str = NULL;		
		}

		if(MA_OK == ma_task_get_setting(task, MA_SECTION_UI_PROVIDER_NAME_STR, MA_KEY_UI_PROVIDER_NAME_STR, &var)){
			ma_buffer_t *buffer = NULL;
			if(MA_OK == ma_variant_get_string_buffer(var, &buffer)){
				if(MA_OK == ma_buffer_get_string(buffer, &str, &size)){
					self->settings.ui_provider = strdup(str);
				}
				ma_buffer_release(buffer);
			}
			ma_variant_release(var);
			var = NULL;
		}
		
		if(MA_OK == ma_task_get_setting(task, MA_SECTION_GENERAL_STR, MA_KEY_LOCALE_INT, &var)){
			(void)ma_variant_get_int32(var, &self->settings.locale);
			(void)ma_variant_release(var);
		}

        (void)ma_task_release(task);
	}
	return rc;
}

ma_error_t ma_mue_session_start(ma_mue_session_t *self, ma_updater_request_t *request, mue_session_exit_cb_t cb, void *cb_data){
    if(self && request && cb) {
        ma_error_t rc = MA_OK;

		if(self->is_mue_process_running)    
			return MA_ERROR_UPDATER_SESSION_IN_PROGRESS;
		
		self->updater_request	  = request;
		self->mue_session_exit_cb = cb;		
		self->cb_data			  = cb_data;
		if(MA_OK == (rc = get_task_info(self))){		
			if(MA_OK == (rc = start_mue_process(self))){            
				self->mue_process_timer_handle.data = self;
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_DEBUG, "uv timer data(%p)\n", self);
				uv_timer_start(&self->mue_process_timer_handle, mue_process_timer_timeout_cb, MA_MUE_TIMEOUT_VALUE_INT, 0);	
				
				/*Starting mue event listener.*/
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_INFO, "Creating mue event handler.");
				ma_mue_event_listener_create(self->ma_context, self->updater_request->params.task_id , self->settings.name ,self->settings.creator , self->updater_request->params.initiator_type, &self->listener);
				if(self->listener)
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_INFO, "Created mue event handler.");									
				else
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_INFO, "Failed to create the mue event handler.");

				self->is_mue_process_running = MA_TRUE;
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_INFO, "Launched updater engine successfully.");
			}
			else {
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_ERROR, "Failed to launch updater engine process, %d.", rc);            
			}
		}
		else{
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_ERROR, "Failed to get task info, %d.", rc);            
		}
        return rc;
    }
    return MA_ERROR_INVALIDARG;
    
}

ma_error_t ma_mue_session_stop(ma_mue_session_t *self){
    if(self) {        	
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_INFO, "Sending stop signal to updater engine.");
		uv_timer_stop(&self->mue_process_timer_handle);
		if(MA_OK != ma_mue_process_stop(self->mue_process_handle)) {
			ma_bool_t is_running = MA_FALSE;

			MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_INFO, "Failed to stop updater engine gracefully, terminating it.");
			ma_mue_process_terminate(self->mue_process_handle);		
			ma_mue_session_status(self, &is_running);
			if(is_running)
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_INFO, "Failed to terminate updater engine.");		
		}	
		else
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_INFO, "Updater engine is stopped gracefully.");		
		return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_mue_session_status(ma_mue_session_t *self, ma_bool_t *is_progress){
	if(self && is_progress && self->mue_process_handle)
		return ma_mue_process_status(self->mue_process_handle, is_progress); 	
	return MA_ERROR_INVALIDARG;
}

static ma_bool_t check_if_file_exist(uv_loop_t *uv_loop, const char *mue_in_use_path_name) {
  int r  = 0;
  uv_fs_t req = {0};  
  ma_bool_t is_file_exist = MA_FALSE;

  r = uv_fs_stat(uv_loop, &req, mue_in_use_path_name, NULL);
  is_file_exist = ( r == 0 && req.result == 0) ;
  uv_fs_req_cleanup(&req);
  return is_file_exist;
}

static ma_bool_t create_file(uv_loop_t *uv_loop, const char *path) {
	uv_fs_t req = {0}; 
	if(-1 != uv_fs_open(uv_loop, &req, path, O_CREAT | O_RDWR, 0644 , NULL)) {
		uv_file dst_file = req.result ;
		uv_fs_req_cleanup(&req) ;
		
		uv_fs_close(uv_loop, &req, dst_file, NULL) ;
		uv_fs_req_cleanup(&req) ;
		return MA_TRUE;
	}
	return MA_FALSE;
}

static ma_bool_t is_mue_copied(ma_mue_session_t *self, uv_loop_t *uv_loop, const char *mue, const char *mue_in_use) {
  int r1  = 0;
  int r2  = 0;
  uv_fs_t req1 = {0};
  uv_fs_t req2 = {0};
  ma_bool_t is_copied = MA_FALSE;

  r1 = uv_fs_stat(uv_loop, &req1, mue, NULL);
  r2 = uv_fs_stat(uv_loop, &req2, mue_in_use, NULL);

  if( (r1 == 0 && req1.result == 0) && (r2 == 0 && req2.result == 0) ){
	  if(req1.statbuf.st_mtime == req2.statbuf.st_mtime){
		is_copied = MA_TRUE;
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_DEBUG, "Using the existing %s.", mue_in_use);
	  }
  }
  uv_fs_req_cleanup(&req1);
  uv_fs_req_cleanup(&req2);
  return is_copied;
}

static ma_error_t copy_mue_to_mue_in_use(ma_mue_session_t *self, uv_loop_t *uv_loop, const char *mue_path_name, const char *mue_in_use_path_name) {
	
	if(check_if_file_exist(uv_loop, mue_in_use_path_name) && is_mue_copied(self, uv_loop, mue_path_name, mue_in_use_path_name) ) 
		return MA_OK;

	unlink(mue_in_use_path_name);

	MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_DEBUG, "Coping %s to %s.", mue_path_name, mue_in_use_path_name);
	if(ma_fs_copy_file_with_timestamp(uv_loop, (char *)mue_path_name, (char *)mue_in_use_path_name))
		return MA_OK;

	/*TODO - proper return MA_ERROR_UPDATER_ENGINE_EXE_COPY_FAILED */
    return MA_ERROR_UPDATER_ENGINE_EXE_COPY_FAILED;
}


static void mue_process_exit_cb(ma_mue_process_t *session, int exit_status, int term_signal, void *cb_data);

static long generate_scriptid(){
	long id;
    srand(time(NULL));    
    id = rand();
    return id;
}

#define LOCALE_ID_LENGTH 5
char *getAgentLocale(ma_mue_session_t *self, char locale[LOCALE_ID_LENGTH]){	
	ma_buffer_t *buffer = NULL;
	ma_bool_t   is_agent_lang_enabled = MA_FALSE;
	ma_configurator_t *configurator = MA_CONTEXT_GET_CONFIGURATOR(self->ma_context);
	ma_ds_t *ds = ma_configurator_get_datastore(configurator);
    
	if(self->settings.locale){		
		MA_MSC_SELECT(_snprintf, snprintf)(locale, LOCALE_ID_LENGTH, "%04x", self->settings.locale);
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_DEBUG, "Using the locale %s (%d).", locale, self->settings.locale);
		return locale;
	}

	/*Get the agent locale and set.*/
	if(MA_OK == ma_ds_get_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_LANGUAGE_STR, &buffer)){
		const char *lang = NULL;
		size_t len = 0;
		if(MA_OK == ma_buffer_get_string(buffer, &lang, &len)){
			if(lang && len && strcmp(lang, "N/A") && len <LOCALE_ID_LENGTH) {
				is_agent_lang_enabled = MA_TRUE;
				strncpy(locale, lang, LOCALE_ID_LENGTH);					
			}
		}
        (void)ma_buffer_release(buffer);
    }	
	if(!is_agent_lang_enabled){
		const ma_misc_system_property_t *misc_sys_prop = ma_system_property_misc_get(MA_CONTEXT_GET_SYSTEM_PROPERTY(self->ma_context), MA_FALSE);
		if(misc_sys_prop)
			strncpy(locale, misc_sys_prop->language_code, LOCALE_ID_LENGTH-1);
	}
	/*Get the locale from the task and set if any.*/
	return locale;
}

static char *get_updater_log_file(ma_mue_session_t *self, char path[MA_MAX_LEN]){	
	ma_policy_settings_bag_t *bag = MA_CONTEXT_GET_POLICY_SETTINGS_BAG(self->ma_context);
	ma_buffer_t *log_file = NULL;
	uv_loop_t *uv_loop = ma_event_loop_get_uv_loop(ma_msgbus_get_event_loop(MA_CONTEXT_GET_MSGBUS(self->ma_context)));

	if(MA_OK == ma_policy_settings_bag_get_buffer(bag, MA_UPDATER_SERVICE_SECTION_NAME_STR, MA_UPDATER_KEY_UPFATE_LOG_FILE_NAME_STR, MA_TRUE,&log_file,NULL)){
		const char *log_file_c = NULL;
		size_t size = 0;

		if(MA_OK == ma_buffer_get_string(log_file, &log_file_c, &size)){
			if(size){
				char log_file_path[MA_MAX_LEN] = {0};
				MA_MSC_SELECT(_snprintf, snprintf)(log_file_path, MA_MAX_LEN, "%s.log", log_file_c);
				
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_DEBUG, "Updater log file set in the policy %s.", log_file_path);
				if(check_if_file_exist(uv_loop, log_file_path) || create_file(uv_loop, log_file_path) ){
					 MA_MSC_SELECT(_snprintf, snprintf)(path, MA_MAX_LEN, "%s", log_file_c);
				}
				else
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_WARNING, "Updater log file set in the policy %s, can't be used. Invalid path.", log_file_c);
			}
		}
		ma_buffer_release(log_file);
	}
	return path;
}

static const char *get_ui_provider(ma_mue_session_t *self){	
	return self->settings.ui_provider;	
}

static ma_error_t start_mue_process(ma_mue_session_t *self) {
    const char *agent_install_path = ma_configurator_get_agent_install_path(MA_CONTEXT_GET_CONFIGURATOR(self->ma_context));
    const char *agent_data_path = ma_configurator_get_agent_data_path(MA_CONTEXT_GET_CONFIGURATOR(self->ma_context));
    uv_loop_t *uv_loop = ma_event_loop_get_uv_loop(ma_msgbus_get_event_loop(MA_CONTEXT_GET_MSGBUS(self->ma_context)));
    ma_error_t rc = MA_OK;

    if(!uv_loop || !agent_install_path || !agent_data_path)  {
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_ERROR, "Failed to get one of the prerequsites for launching updater engine.");
        return MA_ERROR_UNEXPECTED;
    }
    else {
        char mue_in_use_exepath[MA_MAX_LEN] = {0} , 
			mue_exepath[MA_MAX_LEN] = {0}, mue_log_path[MA_MAX_LEN] = {0}, script_path[MA_MAX_LEN] = {0}, agent_path[MA_MAX_LEN] = {0}, 			
			initiator_type[8] = {0},script_id[24] = {0};

#ifdef MA_WINDOWS
		MA_MSC_SELECT(_snprintf, snprintf)(agent_path, MA_MAX_LEN, "%s%c", agent_install_path, MA_PATH_SEPARATOR);		
		
		#ifdef MA_WINDOWS_X64		
		MA_MSC_SELECT(_snprintf, snprintf)(mue_exepath, MA_MAX_LEN, "%sx86%c%s", agent_install_path, MA_PATH_SEPARATOR, MA_MUE_PROCESS_NAME);
        MA_MSC_SELECT(_snprintf, snprintf)(mue_in_use_exepath, MA_MAX_LEN, "%sx86%c%s", agent_install_path, MA_PATH_SEPARATOR, MA_MUE_IN_USE_PROCESS_NAME);		
		#else
		MA_MSC_SELECT(_snprintf, snprintf)(mue_exepath, MA_MAX_LEN, "%s%s", agent_install_path, MA_MUE_PROCESS_NAME);
        MA_MSC_SELECT(_snprintf, snprintf)(mue_in_use_exepath, MA_MAX_LEN, "%s%s", agent_install_path, MA_MUE_IN_USE_PROCESS_NAME);		
		#endif

		MA_MSC_SELECT(_snprintf, snprintf)(mue_log_path, MA_MAX_LEN, "%s%s%c%s", agent_data_path, MA_CONFIGURATION_LOGS_DIR_NAME_STR, MA_PATH_SEPARATOR, MA_MUE_LOG_FILE_NAME_STR);
		MA_MSC_SELECT(_snprintf, snprintf)(script_path, MA_MAX_LEN, "%s%s%c%s", agent_data_path, MA_UPDATE_DIRECTORY_NAME_STR, MA_PATH_SEPARATOR, (self->updater_request->params.initiator_type == MA_UPDATER_INITIATOR_TYPE_TASK_DEPLOYMENT ? MA_MUE_DEPLOYMENT_SCRIPT_NAME_STR : MA_MUE_UPDATE_SCRIPT_NAME_STR));	
#else
		MA_MSC_SELECT(_snprintf, snprintf)(mue_exepath, MA_MAX_LEN, "%s%c%s%c%s", agent_install_path, MA_PATH_SEPARATOR, "bin", MA_PATH_SEPARATOR, MA_MUE_PROCESS_NAME);
        MA_MSC_SELECT(_snprintf, snprintf)(mue_in_use_exepath, MA_MAX_LEN, "%s%c%s%c%s", agent_install_path, MA_PATH_SEPARATOR, "bin", MA_PATH_SEPARATOR, MA_MUE_IN_USE_PROCESS_NAME);
		MA_MSC_SELECT(_snprintf, snprintf)(agent_path, MA_MAX_LEN, "%s%c%s%c", agent_install_path, MA_PATH_SEPARATOR, "lib", MA_PATH_SEPARATOR);
		MA_MSC_SELECT(_snprintf, snprintf)(mue_log_path, MA_MAX_LEN, "%s%c%s%c%s", agent_data_path, MA_PATH_SEPARATOR, MA_CONFIGURATION_LOGS_DIR_NAME_STR, MA_PATH_SEPARATOR, MA_MUE_LOG_FILE_NAME_STR);
		MA_MSC_SELECT(_snprintf, snprintf)(script_path, MA_MAX_LEN, "%s%c%s%c%s", agent_data_path, MA_PATH_SEPARATOR, MA_UPDATE_DIRECTORY_NAME_STR, MA_PATH_SEPARATOR, (self->updater_request->params.initiator_type == MA_UPDATER_INITIATOR_TYPE_TASK_DEPLOYMENT ? MA_MUE_DEPLOYMENT_SCRIPT_NAME_STR : MA_MUE_UPDATE_SCRIPT_NAME_STR));	
#endif
        
		MA_MSC_SELECT(_snprintf, snprintf)(initiator_type, 8, "%d",self->updater_request->params.initiator_type);
		MA_MSC_SELECT(_snprintf, snprintf)(script_id, 24, "%Ld",generate_scriptid());

        if(MA_OK != (rc = copy_mue_to_mue_in_use(self, uv_loop,mue_exepath, mue_in_use_exepath))) {
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_ERROR, "Failed to copy %s to %s for starting engine , %d.", mue_exepath, mue_in_use_exepath , rc);
            return rc;
        }
        else {
			char locale[LOCALE_ID_LENGTH] = "0409";
            char *args[24] = {0};			
			int argv_index = 0;
			char commad_line[MA_MAX_BUFFER_LEN] = {0};
			int i = 0;
			
			getAgentLocale(self, locale);
			ma_mue_event_listener_set_locale(self->listener, locale);

            args[argv_index++] = mue_in_use_exepath;
            args[argv_index++]  = "-script";       args[argv_index++] = script_path;
            args[argv_index++]  = "-id";           args[argv_index++] = script_id; //TODO - figure out the script id
		#ifdef MA_WINDOWS
            args[argv_index++]  = "-localeid";     args[argv_index++] = locale;
		    args[argv_index++]  = "-logfile";      args[argv_index++] = get_updater_log_file(self, mue_log_path);
        #else
			args[argv_index++]  = "-localeid";     args[argv_index++] = "0409";
		    args[argv_index++]  = "-logfile";      args[argv_index++] = mue_log_path;
        #endif
            args[argv_index++]  = "-parent";       args[argv_index++] = "FRAMEWORK";
            args[argv_index++] = "-initiator";    args[argv_index++] = initiator_type;
            {
                const char *ui_provider = get_ui_provider(self) ;
                if(ui_provider) {
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_INFO, "Using Ui provider %s.", ui_provider);
					args[argv_index++] = "-ipcid";        args[argv_index++] = (char*)ui_provider ;
				}
            }
			#ifdef MA_WINDOWS_X64
			{
			char agent_path_x86[MA_MAX_LEN] = {0};
			MA_MSC_SELECT(_snprintf, snprintf)(agent_path_x86, MA_MAX_LEN, "%sx86%c", agent_path, MA_PATH_SEPARATOR);
            args[argv_index++] = "-installdir";   args[argv_index++] = (char*)agent_path_x86;
			}
			#else
			args[argv_index++] = "-installdir";   args[argv_index++] = (char*)agent_path;
			#endif
			args[argv_index++] = "-taskid";       args[argv_index++] = (char*)self->updater_request->params.task_id ;
			args[argv_index++] = 0;


			for(i = 0; i<argv_index; i++){
				if(args[i]){
					MA_MSC_SELECT(_snprintf, snprintf)(commad_line + strlen(commad_line), MA_MAX_BUFFER_LEN - strlen(commad_line), "%s ", args[i]);
				}
			}
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_INFO, "Invoking mue as, [%s].",commad_line);

			if(MA_OK == (rc = ma_mue_process_create(self->ma_context, &self->mue_process_handle))){            
				if(MA_OK == (rc = ma_mue_process_start(self->mue_process_handle, mue_in_use_exepath,args, self, mue_process_exit_cb))){                
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_INFO, "Updater engine is spawned successfully.");
   					return rc;
				}
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_ERROR, "Failed to spawn updater engine.");				
			}
			else
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_ERROR, "Failed to create updater engine instance.");
   			return rc;			
        }
    }
    return MA_OK;
}


static void mue_process_timer_timeout_cb(uv_timer_t* handle, int status) {
    ma_mue_session_t *self = (ma_mue_session_t *)handle->data;	
	MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_INFO, "Triggering update engine stop due to timeout occured.");
    ma_mue_session_stop(self);		    
	self->mue_session_exit_cb(self, self->updater_request, 1, -1 , self->cb_data);	    
}

void mue_process_exit_cb(ma_mue_process_t *session, int exit_status, int term_signal, void *cb_data){
	ma_mue_session_t *self = (ma_mue_session_t *)cb_data;
	if(self){
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_INFO, "Updater engine exited with exit status as %d and  term signal %d.", exit_status, term_signal);
		uv_timer_stop(&self->mue_process_timer_handle);
   		self->mue_session_exit_cb(self, self->updater_request, exit_status, term_signal , self->cb_data);	    
	}
}
