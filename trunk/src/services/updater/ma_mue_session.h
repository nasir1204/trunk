#ifndef MA_MUE_SESSION_H_INCLUDED
#define MA_MUE_SESSION_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/defs/ma_updater_defs.h"
#include "ma/internal/services/scheduler/ma_scheduler_core.h"

#include "ma_mue_event_listener.h"
#include "ma_updater_request.h"

MA_CPP(extern "C" {)

typedef struct ma_mue_session_s ma_mue_session_t ;

typedef ma_error_t (*mue_session_exit_cb_t)(ma_mue_session_t *session, const ma_updater_request_t *updaterreq, int exit_status, int term_signal, void *cb_data);

ma_error_t ma_mue_session_create(ma_context_t *context, ma_mue_session_t **session);

ma_error_t ma_mue_session_release(ma_mue_session_t *session);

ma_error_t ma_mue_session_start(ma_mue_session_t *session, ma_updater_request_t *request, mue_session_exit_cb_t cb, void *cb_data);

ma_error_t ma_mue_session_stop(ma_mue_session_t *session);

ma_error_t ma_mue_session_status(ma_mue_session_t *session, ma_bool_t *is_progress);

ma_mue_event_listener_t *ma_mue_session_get_event_listener(ma_mue_session_t *session);

MA_CPP(})

#endif  /*MA_MUE_SESSION_H_INCLUDED*/

