#include "ma/ma_variant.h"
#include "ma/ma_client.h"
#include "ma_mue_event_listener.h"
#include "ma/internal/ma_macros.h"

#include "ma/internal/utils/datastructures/ma_stream.h"
#include "ma/internal/utils/datastructures/ma_bytebuffer.h"

#include "ma/ma_log.h"
#include "ma_mue_event.h"
#include "ma/ma_datastore.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/event/ma_event_bag.h"

#include "ma/internal/defs/ma_mue_defs.h"
#include "ma/internal/defs/ma_updater_defs.h"
#include "ma/internal/defs/ma_dc_task_service_defs.h"
#include "ma/internal/clients/ma/ma_client_event.h"

#include "ma/internal/utils/text/ma_ini_parser.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal/utils/conf/ma_conf.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "Updater"


#define MA_CONTEXT_GET_DATASTORE(context) (ma_configurator_get_datastore(MA_CONTEXT_GET_CONFIGURATOR(context)))

struct ma_mue_event_listener_s{
	ma_context_t						*context;
	
	char								*task_id;
	
	char								*task_name;

	char								*task_creator;	
	
	char								*locale;

	ma_updater_initiator_type_t         initiator_type;	  

	ma_mue_event_list_t					mue_events;

	int									mue_rc;

	ma_update_state_t					session_state;	

	ma_bool_t							is_reboot_required;
};

const char *buffer_str(ma_buffer_t *value){
	static const char *EMPTY_STRING = "";
	const char *str = NULL;
	size_t  str_len = 0;
	ma_buffer_get_string(value, &str, &str_len);
	return str ? str : EMPTY_STRING;
}


ma_error_t ma_mue_event_listener_create(ma_context_t *context, const char *task_id, const char *task_name, const char *task_creator, ma_updater_initiator_type_t initiator_type, ma_mue_event_listener_t **listener){
	if(context && task_id && task_name && task_creator && listener){		
		ma_error_t rc = MA_ERROR_OUTOFMEMORY;
		ma_mue_event_listener_t *self = (ma_mue_event_listener_t*) calloc(1, sizeof(ma_mue_event_listener_t));
		if(self){			
			ma_mue_event_list_init(&self->mue_events);
			self->context = context;
			self->task_id = strdup(task_id);
			self->task_name = strdup(task_name);
			self->task_creator = strdup(task_creator);
			self->session_state = MA_UPDATE_STATE_INIT;
			self->initiator_type = initiator_type;			
			*listener = self;
			return MA_OK;
		}
		return MA_ERROR_OUTOFMEMORY;
	}
	return MA_ERROR_INVALIDARG;
}
ma_error_t ma_mue_event_listener_set_locale(ma_mue_event_listener_t *self, const char *locale){
	if(self && locale){
		self->locale = strdup(locale);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

#define MA_SCRIPT_EVENT_MESSAGE_FORMAT_STR "Script Event msg iEventId \"%d\"" \
	    " iSeverity \"%d\"" \
		" iProductId \"%s\"" \
		" iLocale \"%s\"" \
		" iUpdateType \"%s\"" \
		" iUpdateState \"%d\"" \
		" iUpdateError \"%d\"" \
		" iNewVersion \"%s\"" \
		" iDateTime \"%s\"" 
	
#define MA_PRODUCT_INSTALL_SUCCESS_RESULT	 "Product \"%s\" installation succeeded to version \"%s\"."
#define MA_PRODUCT_INSTALL_FAILURE_RESULT	 "Product \"%s\" installation failed, reason \"%s\"."
#define MA_PRODUCT_UNINSTALL_SUCCESS_RESULT	 "Product \"%s\" uninstallation succeeded."
#define MA_PRODUCT_UNINSTALL_FAILURE_RESULT	 "Product \"%s\" uninstallation failed, reason \"%s\"."

#define MA_PRODUCT_UPDATE_SUCCESS_RESULT	 "Product \"%s\" update succeeded to version \"%s\"."
#define MA_PRODUCT_UPDATE_FAILURE_RESULT	 "Product \"%s\" update failed, reason \"%s\"."

#define MA_OVERALL_SUCCESS_RESULT			 "Deplyment/Update task is finished successfully."
#define MA_OVERALL_FAILURE_RESULT			 "Deplyment/Update task failed."
#define MA_OVERALL_FAILURE_RESULT_WITH_ERROR "Deplyment/Update task failed, reason \"%s\"."

#define MA_AGENT_UPDATER_LAST_USE_SITE_STR   "LastUsedSite"
#define MA_AGENT_UPDATER_HISTORY_INI_STR	 "UpdateHistory.ini"

static ma_bool_t get_update_message(ma_mue_event_listener_t *self, ma_mue_event_t *event, char *update_messgae, int size, ma_bool_t success){
	
	if(!strcmp(event->iProductId, MA_SOFTWAREID_GENERAL_STR) && !strcmp(event->iUpdateType, "N/A")){
		if(success)
			MA_MSC_SELECT(_snprintf, snprintf)(update_messgae, size, MA_OVERALL_SUCCESS_RESULT);		
		else		
			MA_MSC_SELECT(_snprintf, snprintf)(update_messgae, size, MA_OVERALL_FAILURE_RESULT_WITH_ERROR, update_msg((ma_update_error_t)(event->iUpdateError)));		
		return MA_TRUE;
	}

	if(0 == update_result_code( (ma_update_error_t)(event->iUpdateError))){
		if(MA_UPDATER_INITIATOR_TYPE_TASK_DEPLOYMENT == self->initiator_type){
			if(0 == strcmp(event->iUpdateType, "Install"))
				MA_MSC_SELECT(_snprintf, snprintf)(update_messgae, size, MA_PRODUCT_INSTALL_SUCCESS_RESULT, event->iProductId, ( 0 == strcmp(event->iNewVersion,"N/A") ? "LATEST" : event->iNewVersion) );	
			else
				MA_MSC_SELECT(_snprintf, snprintf)(update_messgae, size, MA_PRODUCT_UNINSTALL_SUCCESS_RESULT, event->iProductId);	
		}
		else
			MA_MSC_SELECT(_snprintf, snprintf)(update_messgae, size, MA_PRODUCT_UPDATE_SUCCESS_RESULT, event->iProductId, ( 0 == strcmp(event->iNewVersion,"N/A") ? "LATEST" : event->iNewVersion) );						
	}
	else{
		if(MA_UPDATER_INITIATOR_TYPE_TASK_DEPLOYMENT == self->initiator_type){
			if(0 == strcmp(event->iUpdateType, "Install"))
				MA_MSC_SELECT(_snprintf, snprintf)(update_messgae, size, MA_PRODUCT_INSTALL_FAILURE_RESULT, event->iProductId, update_msg((ma_update_error_t)(event->iUpdateError)));		
			else
				MA_MSC_SELECT(_snprintf, snprintf)(update_messgae, size, MA_PRODUCT_UNINSTALL_FAILURE_RESULT, event->iProductId, update_msg((ma_update_error_t)(event->iUpdateError)));		
		}
		else
			MA_MSC_SELECT(_snprintf, snprintf)(update_messgae, size, MA_PRODUCT_UPDATE_FAILURE_RESULT, event->iProductId, update_msg((ma_update_error_t)(event->iUpdateError)));						
	}
	return MA_TRUE;
}

static ma_bool_t get_update_type(ma_mue_event_listener_t *self, ma_mue_event_t *event, char *update_messgae, int size, ma_bool_t success){

	if(!strcmp(event->iUpdateType, "N/A"))
		MA_MSC_SELECT(_snprintf, snprintf)(update_messgae, size, "%s", (MA_UPDATER_INITIATOR_TYPE_TASK_DEPLOYMENT == self->initiator_type ? MA_UPDATER_TASK_TYPE_COMPAT_DEPLOYMENT_STR : MA_UPDATER_TASK_TYPE_COMPAT_UPDATE_STR));	
	else
		MA_MSC_SELECT(_snprintf, snprintf)(update_messgae, size, "%s", event->iUpdateType);		
	return MA_TRUE;
}

static int updater_option_ini_handler(const char *section, const char *key, const char *value, void *userdata){
	struct ma_event_parameter_s *params = (struct ma_event_parameter_s*)userdata;
	if(!strcmp(key, MA_AGENT_UPDATER_LAST_USE_SITE_STR) && value)
		strncpy(params->site_name, value, 1023);
	return 0;
}

static ma_error_t get_updater_site_name(struct ma_event_parameter_s *param) {
	ma_error_t rc;
	ma_temp_buffer_t temp_buf = MA_TEMP_BUFFER_INIT;   
	ma_temp_buffer_t path = MA_TEMP_BUFFER_INIT;   
    char sub_path[MA_MAX_PATH_LEN] = {0};
	if(MA_OK == (rc = ma_conf_get_data_path(&temp_buf))){
	#ifdef MA_WINDOWS
		MA_MSC_SELECT(_snprintf, snprintf)(sub_path, MA_MAX_PATH_LEN-1, "%s", MA_AGENT_UPDATER_HISTORY_INI_STR);
	#else
		MA_MSC_SELECT(_snprintf, snprintf)(sub_path, MA_MAX_PATH_LEN-1, "%s%c%s", MA_CONFIGURATION_UDDATE_DIR_NAME_STR, MA_PATH_SEPARATOR, MA_AGENT_UPDATER_HISTORY_INI_STR);
	#endif    
		if(MA_OK == (rc = form_path(&path, (char*)ma_temp_buffer_get(&temp_buf), sub_path , MA_FALSE))){
			FILE *fp = NULL;
			if((fp = fopen((char*)ma_temp_buffer_get(&path), "r"))) {
				/* obtain file size */
				char *updater_history_ini = NULL;	
				size_t file_size = 0;
				fseek (fp , 0 , SEEK_END);
				file_size = ftell (fp);
				rewind (fp);
				if(file_size && (updater_history_ini = (char*) calloc(1, file_size + 1))){
					fread(updater_history_ini, 1, file_size, fp);
					fclose(fp);
					ma_ini_parser_start((char*)updater_history_ini, file_size, updater_option_ini_handler, param);
					free(updater_history_ini); updater_history_ini = NULL;
					rc = MA_OK;
				}
			}
			else{
				rc = MA_ERROR_FILE_OPEN_FAILED;
			}
		}
	}
	ma_temp_buffer_uninit(&temp_buf);
	ma_temp_buffer_uninit(&path);
	return rc;
}

static void generate_updater_event(ma_mue_event_listener_t *self, ma_uint32_t eventid, ma_event_severity_t severity, ma_mue_event_t *event, ma_bool_t success){
	struct ma_event_parameter_s params = {0};	
	char type[256] = {0};	

	ma_event_parameter_init(&params);
	get_update_type(self, event, type, sizeof(type), success);

	params.eventid = eventid;
	params.severity = severity;	
	params.source_productid = event->iProductId;
	params.type = type;	
	params.new_version = event->iNewVersion;
	params.initiator_id = self->task_creator;
	switch(self->initiator_type){
	case MA_UPDATER_INITIATOR_TYPE_ONDEMAND_UPDATE:
		params.initiator_type = "OnDemand";
		break;
	case MA_UPDATER_INITIATOR_TYPE_TASK_UPDATE:
		params.initiator_type = "UpdateTask";
		break;
	case MA_UPDATER_INITIATOR_TYPE_GLOBAL_UPDATE:
		params.initiator_type = "Automatic";
		break;
	case MA_UPDATER_INITIATOR_TYPE_REMEDIATION_UPDATE:
		params.initiator_type = "Remediation";
		break;
	case MA_UPDATER_INITIATOR_TYPE_TASK_DEPLOYMENT:
		params.initiator_type = "DeploymentTask";
		break;
	case MA_UPDATER_INITIATOR_TYPE_TASK_MIRROR:
		params.initiator_type = "Mirror";
		break;
	case MA_UPDATER_INITIATOR_TYPE_ONDEMAND_ROLLBACK:
		params.initiator_type = "Rollback";
		break;
	default:
		params.initiator_type = "Unknown";
	}

	(void)get_updater_site_name(&params);

	if(self->locale)
		MA_MSC_SELECT(_snprintf, snprintf)(params.locale, sizeof(params.locale), "%s", self->locale);
		
	MA_MSC_SELECT(_snprintf, snprintf)(params.error, sizeof(params.error), "%d", event->iUpdateError);	

	if(MA_OK == ma_client_event_generate(MA_CONTEXT_GET_CLIENT(self->context), &params))
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Updater event is generated");
	else
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to generate event.");
}

static void handle_update_sucess_failure_event(ma_mue_event_listener_t *self, ma_mue_event_t *event, ma_bool_t success){

	ma_uint32_t event_id = 0;
	ma_event_severity_t severity = MA_EVENT_SEVERITY_INFORMATIONAL;

	if(0 == strlen(event->iUpdateType) || 0 == strcmp(event->iUpdateType, "N/A")) return;

	if(MA_UPDATER_INITIATOR_TYPE_TASK_DEPLOYMENT == self->initiator_type){
		event_id = success ? EVENT_DEPLOYMENT_SUCCESS : EVENT_DEPLOYMENT_FAIL;
		severity = success ?  MA_EVENT_SEVERITY_INFORMATIONAL : MA_EVENT_SEVERITY_CRITICAL;
		generate_updater_event(self, event_id, severity, event, success);
	}
	else if(MA_UPDATER_INITIATOR_TYPE_TASK_UPDATE == self->initiator_type || MA_UPDATER_INITIATOR_TYPE_ONDEMAND_UPDATE == self->initiator_type){
		event_id = success ? EVENT_COMMON_UPDATE_SUCCESS : EVENT_COMMON_UPDATE_FAIL;
		severity = success ?  MA_EVENT_SEVERITY_INFORMATIONAL : MA_EVENT_SEVERITY_CRITICAL;		
		generate_updater_event(self, event_id, severity, event, success);
	}
	else{
	}
}

static void handle_update_finish_event(ma_mue_event_listener_t *self, ma_mue_event_t *event, ma_bool_t success){

	ma_uint32_t event_id = 0;
	ma_event_severity_t severity = MA_EVENT_SEVERITY_INFORMATIONAL;

	if(0 == strcmp(event->iProductId, MA_SOFTWAREID_GENERAL_STR) && 0 == strcmp(event->iUpdateType, "N/A")){
		if(!success){

			/*update the error if not found in the current event*/
			if(!update_result_code( (ma_update_error_t)(event->iUpdateError))){

				if(self->mue_events.events_count){ 
					ma_mue_event_t *evt = NULL;			
		
					for(evt = self->mue_events.events_head; evt != NULL; evt = evt->p_link){
						if(evt){
							/*Even single failure event, is a failure, try to get the last error.*/
							if(update_result_code( (ma_update_error_t)(evt->iUpdateError)))
								event->iUpdateError = evt->iUpdateError;																				
						}			
					}
				}				
				/*still error not found then set it to unknown*/
				if(!update_result_code( (ma_update_error_t)(event->iUpdateError)))
					event->iUpdateError = -1;
			}					

			if(MA_UPDATER_INITIATOR_TYPE_TASK_DEPLOYMENT == self->initiator_type){
				event_id = success ? EVENT_DEPLOYMENT_SUCCESS : EVENT_DEPLOYMENT_FAIL;
				severity = success ?  MA_EVENT_SEVERITY_INFORMATIONAL : MA_EVENT_SEVERITY_CRITICAL;
				generate_updater_event(self, event_id, severity, event, success);
			}
			else if(MA_UPDATER_INITIATOR_TYPE_TASK_UPDATE == self->initiator_type || MA_UPDATER_INITIATOR_TYPE_ONDEMAND_UPDATE == self->initiator_type){
				event_id = success ? EVENT_COMMON_UPDATE_SUCCESS : EVENT_COMMON_UPDATE_FAIL;
				severity = success ?  MA_EVENT_SEVERITY_INFORMATIONAL : MA_EVENT_SEVERITY_CRITICAL;		
				generate_updater_event(self, event_id, severity, event, success);
			}
			else{
			}
		}
	}
}

static void handle_update_cancel_event(ma_mue_event_listener_t *self, ma_mue_event_t *event)
{

}

static void handle_update_postponse_event(ma_mue_event_listener_t *self, ma_mue_event_t *event)
{

}

static void handle_update_reboot_event(ma_mue_event_listener_t *self, ma_mue_event_t *event)
{
	self->is_reboot_required = MA_TRUE;
}

ma_error_t ma_mue_event_listener_is_reboot_required(ma_mue_event_listener_t *self, ma_bool_t *is_reboot_required){
	if(self && is_reboot_required){
		*is_reboot_required = self->is_reboot_required;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

void on_script_events(ma_mue_event_listener_t *self, 
	long iEventId, 
	int iSeverity, 
	const char *iProductId,
	const char *iLocale,
	const char *iUpdateType,
	int iUpdateState,
	int iUpdateError,
	const char *iNewVersion,
	const char *iDateTime)
{

	char script_state_msg[MA_MAX_BUFFER_LEN] = {0};
	ma_mue_event_t *event = mue_event_new(iEventId, iSeverity, iProductId, iLocale, iUpdateType, iUpdateState, iUpdateError, iNewVersion, iDateTime);
	if(!event){
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to create mue event object.");
		return;
	}

	ma_mue_event_list_push(&self->mue_events, event);
	self->session_state  = (ma_update_state_t)iUpdateState;

	MA_MSC_SELECT(_snprintf, snprintf)(script_state_msg, MA_MAX_BUFFER_LEN, MA_SCRIPT_EVENT_MESSAGE_FORMAT_STR,
		event->iEventId, event->iSeverity, event->iProductId, event->iLocale, event->iUpdateType, event->iUpdateState, event->iUpdateError, event->iNewVersion, event->iDateTime);
	MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO, "%s",script_state_msg);

	if(event->iProductId && event->iUpdateType && !strcmp(event->iProductId, MA_SOFTWAREID_GENERAL_STR) && !strcmp(event->iUpdateType, "N/A") && eUPDATE_ERROR_NO_REPO == event->iUpdateError) {
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Generate the updater failure event.");
		handle_update_finish_event(self, event, MA_FALSE);
		self->session_state = MA_UPDATE_STATE_FINISHED_FAIL;
	}

	switch ((ma_update_state_t)event->iUpdateState){
		case MA_UPDATE_STATE_INIT:
			break;
		case MA_UPDATE_STATE_SUCCEEDED:
			handle_update_sucess_failure_event(self, event, MA_TRUE);
			break;
		case MA_UPDATE_STATE_FINISHED_OK:			
			handle_update_finish_event(self, event, MA_TRUE);
			break;
		case MA_UPDATE_STATE_FAILED:					
			handle_update_sucess_failure_event(self, event, MA_FALSE);
			break;
		case MA_UPDATE_STATE_FINISHED_FAIL:
			handle_update_finish_event(self, event, MA_FALSE);
			break;
		case MA_UPDATE_STATE_FINISHED_WAITING_REBOOT:
		case MA_UPDATE_STATE_FINISHED_WAITING_REBOOT_RESTART:
			handle_update_reboot_event(self, event);
			break;
		case MA_UPDATE_STATE_FINISHED_CANCEL:
			handle_update_cancel_event(self, event);			        
			break;
		case MA_UPDATE_STATE_FINISHED_NOTFOUND:
		case MA_UPDATE_STATE_FINISHED_PARTIAL:					
			handle_update_postponse_event(self, event);
			break;
	}		
    return;
}

#define MA_SCRIPT_PROGRESS_MESSAGE_FORMAT_STR "Script progress muecProgressEvent \"%d\" Message \"%s\"" 
		
void on_script_progress_message(ma_mue_event_listener_t *self, enum muecProgressEvent iEventType, int iProgressStep, int iProgressMax, const char *message){
	if(self && message)
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), (pe_Error == iEventType ? MA_LOG_SEV_ERROR : MA_LOG_SEV_DEBUG), MA_SCRIPT_PROGRESS_MESSAGE_FORMAT_STR, iEventType, message);			
    return;
}

#define MA_UPDATE_DIALOG_MESSAGE_FORMAT_STR "Show dialog, UpdateDialogType=%s" \
		" title \"%s\"" \
	    " message \"%s\"" \
		" countdownMessage \"%s\"" \
		" countdownValue \"%ul\"" 

void on_script_show_update_dialog(ma_mue_event_listener_t *self,
							enum muecUpdateDialogType type,
                            const char *title,
                            const char *message,
                            const char *countdownMessage,
                            long countdownValue)
{
	const char *update_dialog_type[5] = {"Reboot", "Progress", "Postpone", "Unknown"};
	const char *msg_type = update_dialog_type[3];
	if(type <= dlg_postpone)
		msg_type = update_dialog_type[type];	
	
	if(self && title && message && countdownMessage)
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, MA_UPDATE_DIALOG_MESSAGE_FORMAT_STR, msg_type, title, message, countdownMessage, countdownValue);		
	
    return;
}

#define MA_UPDATE_END_DIALOG_MESSAGE_FORMAT_STR "End dialog, " \
		" title \"%s\"" \
	    " message \"%s\"" \
		" countdownMessage \"%s\"" \
		" countdownValue \"%ul\"" 

void on_script_end_update_dialog(ma_mue_event_listener_t *self,
					const char *title,
                    const char *message,
                    const char *countdownMessage,
                    long countdownValue)
{
	if(self && title && message && countdownMessage)
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO, MA_UPDATE_END_DIALOG_MESSAGE_FORMAT_STR, title, message, countdownMessage, countdownValue);			
    return;
}

static ma_bool_t get_l(ma_array_t *event, int index, ma_int64_t *value){
	ma_bool_t rc = MA_FALSE;
	ma_variant_t *var = NULL;
	if(MA_OK == ma_array_get_element_at(event, index, &var)){
		ma_int64_t tmp;
		if(MA_OK == ma_variant_get_int64(var, &tmp)){				
			*value = tmp;
			rc = MA_TRUE;
		}
		ma_variant_release(var);
	}
	return rc;
}

static ma_bool_t get_s(ma_array_t *event, int index, ma_buffer_t **value){
	ma_bool_t rc = MA_FALSE;
	ma_variant_t *var = NULL;
	if(MA_OK == ma_array_get_element_at(event,index,&var)){		
		if(MA_OK == ma_variant_get_string_buffer(var, value)){	
			rc = MA_TRUE;
		}
		ma_variant_release(var);
	}
	return rc;
}

ma_error_t ma_mue_event_listener_add_event(ma_mue_event_listener_t *self, int op_type, const char *session_id, ma_variant_t *event_data){
	ma_error_t rc = MA_ERROR_INVALIDARG;
	if(self && session_id && event_data){		
		ma_array_t *event = NULL;
		int index = 0;
		
		if(MA_OK != (rc = ma_variant_get_array(event_data, &event))){					
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to get data from mue event, rc <%d>.", rc);
			return rc;
		}

		switch (op_type)
		{
			case eOpProgress:
			{
				ma_int64_t iEventType = 0;
				ma_int64_t iProgressStep = 0;
				ma_int64_t iProgressMax = 0;
				ma_buffer_t *Message = NULL;
				
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Processing script progress event.");
				if(get_l(event, ++index, &iEventType) &&
				   get_l(event, ++index, &iProgressStep) &&
				   get_l(event, ++index, &iProgressMax) &&
				   get_s(event, ++index, &Message))
				{						
					on_script_progress_message(self, (enum muecProgressEvent)iEventType, (int)iProgressStep, (int)iProgressMax, buffer_str(Message));
				}
				ma_buffer_release(Message);				
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Processing script progress event done.");
			}
			break;
			case eOpEvent:
			{				
				ma_int64_t iEventId = 0;
				ma_int64_t iSeverity = 0;
				ma_buffer_t *strProductId = NULL;
				ma_buffer_t *strLocale = NULL;
				ma_buffer_t *strUpdateType = NULL;
				ma_int64_t iUpdateState = 0;
				ma_int64_t iUpdateError = 0;
				ma_buffer_t *strNewVersion = NULL;
				ma_buffer_t *strDateTime = NULL;
								
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Processing script event.");

				if(get_l(event, ++index, &iEventId) &&
				   get_l(event, ++index, &iSeverity) &&
				   get_s(event, ++index, &strProductId) &&
				   get_s(event, ++index, &strLocale) &&
				   get_s(event, ++index, &strUpdateType) &&
				   get_l(event, ++index, &iUpdateState) &&
				   get_l(event, ++index, &iUpdateError) &&
				   get_s(event, ++index, &strNewVersion)&&
				   get_s(event, ++index, &strDateTime))
				{		
					on_script_events(self, iEventId, iSeverity, 
						buffer_str(strProductId),
						buffer_str(strLocale),
						buffer_str(strUpdateType),
						iUpdateState,
						iUpdateError,
						buffer_str(strNewVersion),
						buffer_str(strDateTime));						
				}
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Processing script event done.");

				ma_buffer_release(strProductId);				
				ma_buffer_release(strLocale);				
				ma_buffer_release(strUpdateType);							
				ma_buffer_release(strNewVersion);				
				ma_buffer_release(strDateTime);				
			}
			break;
			case eOpShowUpdateDialog:
			{					
				ma_int64_t iType = 0;
				ma_buffer_t *strTitle = NULL;
				ma_buffer_t *strMessage = NULL;
				ma_buffer_t *strCountdownMessage = NULL;
				ma_int64_t iCountdownValue =0 ;

				MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Processing ShowUpdateDialog information event.");
				if(get_l(event, ++index, &iType) &&				   
				   get_s(event, ++index, &strTitle) &&
				   get_s(event, ++index, &strMessage) &&
				   get_s(event, ++index, &strCountdownMessage) &&
				   get_l(event, ++index, &iCountdownValue))				   
				{	
					on_script_show_update_dialog(self, (enum muecUpdateDialogType)iType, buffer_str(strTitle), buffer_str(strMessage), buffer_str(strCountdownMessage), iCountdownValue);
				}
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Processing ShowUpdateDialog information event done.");				
				
				ma_buffer_release(strTitle);							
				ma_buffer_release(strMessage);				
				ma_buffer_release(strCountdownMessage);
			}
			break;

			case eOpEndUpdateDialog:
			{
				ma_buffer_t *strTitle = NULL;
				ma_buffer_t *strMessage = NULL;
				ma_buffer_t *strCountdownMessage = NULL;
				ma_int64_t iCountdownValue =0 ;

				MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Processing EndUpdateDialog information event.");
				if(get_s(event, ++index, &strTitle) &&
				   get_s(event, ++index, &strMessage) &&
				   get_s(event, ++index, &strCountdownMessage) &&
				   get_l(event, ++index, &iCountdownValue))				   
				{	
					on_script_end_update_dialog(self, buffer_str(strTitle), buffer_str(strMessage), buffer_str(strCountdownMessage), iCountdownValue);
				}
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Processing EndUpdateDialog information event done.");				
				
				ma_buffer_release(strTitle);							
				ma_buffer_release(strMessage);				
				ma_buffer_release(strCountdownMessage);
			}
			break;
			default:
			break;
		}
		ma_array_release(event);
	}		
	return rc;
}

ma_error_t ma_mue_event_listener_set_mue_return_code(ma_mue_event_listener_t *self, int state){
	if(self){
		self->mue_rc = state;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}



#define MA_DC_TASK_SUMMARY_REPORT_STR        "<Status>" "<Code>%d</Code>" "<TaskName>%s</TaskName>" "<Detail>%s</Detail>" "<FailureReason>%s</FailureReason>" "</Status>"
#define MA_DC_TASK_SUMMARY_MSG_SIZE		     2000

ma_error_t ma_mue_event_listener_get_task_summary(ma_mue_event_listener_t *self, char **xml_report){
	if(self && xml_report){
		ma_error_t rc				= MA_ERROR_UPDATER_TASK_SUMMARY_GENERATE_FAILED;
		char		    *xml		= NULL;
		ma_stream_t     *message	= NULL;
		ma_bytebuffer_t *messgae_b  = NULL;			
		int final_result			=  0;			
				
		/*{
			int code = 0;
			while( code < sizeof(update_error_msgs)/sizeof(update_error_msgs[0]) ){
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Error code %d msg %s.", code, update_error_msgs[code]);						
				code++;
			}
		}*/

		/*it is only supported for the deployment/update task.*/
		if( !(
			MA_UPDATER_INITIATOR_TYPE_TASK_DEPLOYMENT == self->initiator_type  || 
			MA_UPDATER_INITIATOR_TYPE_TASK_UPDATE == self->initiator_type ||
			MA_UPDATER_INITIATOR_TYPE_ONDEMAND_UPDATE == self->initiator_type
			))
			return rc;
		

		/*Form the Update message.*/
		(void)ma_mstream_create(MA_DC_TASK_SUMMARY_MSG_SIZE, NULL, &message);

		/*If some events then generate message.*/		
		if(self->mue_events.events_count){ 
			ma_mue_event_t *event = NULL;			
			ma_bool_t add_line_br = MA_FALSE;
			final_result	  =  0;						
			
			for(event = self->mue_events.events_head; event != NULL; event = event->p_link){
				if(event){
					char update_messgae[1024]   = {0};
					size_t size = 0;

					if(message){
						if((MA_OK == ma_stream_get_size(message, &size)) && size < 2000){							

							if(!strcmp(event->iProductId, MA_SOFTWAREID_GENERAL_STR) && !strcmp(event->iUpdateType, "N/A")){
								if(update_result_code((ma_update_error_t)event->iUpdateError))
									get_update_message(self, event, update_messgae, 1024, MA_FALSE);									
							}
							else
								get_update_message(self, event, update_messgae, 1024, MA_TRUE);	

							if(strlen(update_messgae)){
								if(add_line_br)
									(void)ma_stream_write(message, (unsigned char*)"&lt;br/&gt;", strlen("&lt;br/&gt;"), &size);
								else
									add_line_br = MA_TRUE;
								(void)ma_stream_write(message, (unsigned char*)update_messgae, strlen(update_messgae), &size);
							}
						}
					}
					/*Even single failire event, is a failure.*/
					final_result |= update_result_code( (ma_update_error_t)(event->iUpdateError));					
				}								
			}			
		}
		else{ 
			size_t size = 0;
			(void)ma_stream_write(message, MA_OVERALL_FAILURE_RESULT, strlen(MA_OVERALL_FAILURE_RESULT), &size);
		}

		/*final status*/
		{
			ma_updater_request_state_t task_state = MA_UPDATER_REQUEST_STOPPED;
			ma_mue_event_listener_get_task_state(self, &task_state);

			final_result |= ((task_state == MA_UPDATER_REQUEST_SUCCESS) ? 0 : 1);
		}

		/*Generate xml.*/		
		(void)ma_stream_get_buffer(message, &messgae_b);				
		if(messgae_b){
			size_t xml_length = 0;						
			if(messgae_b){
				xml_length = strlen(MA_DC_TASK_SUMMARY_REPORT_STR) + ma_bytebuffer_get_size(messgae_b) + strlen(self->task_name) + 1;
				xml = (char*)calloc(xml_length, 1); 
				if(xml){
					MA_MSC_SELECT(_snprintf, snprintf)(xml, xml_length, MA_DC_TASK_SUMMARY_REPORT_STR, (self->is_reboot_required ? MA_RUNNOW_TASK_STATUS_STARTED: (!final_result ? 1 : -1)) , self->task_name, ma_bytebuffer_get_bytes(messgae_b), "");
					*xml_report = xml;					
					rc = MA_OK;
				}
			}
			(void)ma_bytebuffer_release(messgae_b);
		}
		(void)ma_stream_release(message);
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_mue_event_listener_get_update_state(ma_mue_event_listener_t *self, ma_update_state_t *curr_state){
	if(self && curr_state){		
		*curr_state = self->session_state;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_mue_event_listener_get_task_state(ma_mue_event_listener_t *self, ma_updater_request_state_t *final_state){
	if(self && final_state){				
		*final_state = MA_UPDATER_REQUEST_STOPPED;
		switch(self->session_state){
			case MA_UPDATE_STATE_INIT:
			case MA_UPDATE_STATE_SUCCEEDED:
			case MA_UPDATE_STATE_FINISHED_WAITING_REBOOT:
			case MA_UPDATE_STATE_FINISHED_WAITING_REBOOT_RESTART:
			case MA_UPDATE_STATE_FINISHED_OK:			
				*final_state = MA_UPDATER_REQUEST_SUCCESS;
				break;			
			case MA_UPDATE_STATE_FINISHED_CANCEL:
				*final_state = MA_UPDATER_REQUEST_STOPPED;
				break;
			case MA_UPDATE_STATE_FAILED:		
			case MA_UPDATE_STATE_FINISHED_FAIL:
			case MA_UPDATE_STATE_FINISHED_NOTFOUND:
			case MA_UPDATE_STATE_FINISHED_PARTIAL:	
				*final_state = MA_UPDATER_REQUEST_FAILED;
		}
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_mue_event_listener_get_final_update_result(ma_mue_event_listener_t *self, ma_updater_request_state_t *final_state){
	if(self && final_state){		
		if(self->mue_events.events_count){ 
			ma_mue_event_t *event = NULL;			
		
			for(event = self->mue_events.events_head; event != NULL; event = event->p_link){
				if(event){
					/*Even single failire event, is a failure.*/
					if(update_result_code( (ma_update_error_t)(event->iUpdateError))){
						*final_state = MA_UPDATER_REQUEST_FAILED;
						return MA_OK;
					}
				}								
			}			
		}
		return ma_mue_event_listener_get_task_state(self, final_state);
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_mue_event_listener_release(ma_mue_event_listener_t *self){
	if(self){		
		if(self->mue_events.events_count)
			ma_mue_event_list_deinit(&self->mue_events);		
		if(self->task_id) free(self->task_id);
		if(self->task_creator) free(self->task_creator);
		if(self->task_name) free(self->task_name);
		if(self->locale) free(self->locale);
		free(self);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

