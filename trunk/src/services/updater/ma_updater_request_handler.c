#include "ma_updater_request_handler.h"
#include "ma_mue_session.h"
#include "ma_updater_request_queue.h"

#include "ma/ma_msgbus.h"
#include "ma/ma_message.h"
#include "ma/internal/core/msgbus/ma_msgbus_private.h"
#include "ma/ma_log.h"
#include "ma/internal/defs/ma_updater_defs.h"
#include "ma/internal/defs/ma_mue_defs.h"
#include "ma/internal/defs/ma_scheduler_defs.h"
#include "ma/internal/services/scheduler/ma_scheduler_core.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/time/ma_time.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/services/io/ma_registry_updater.h"
#include <stdlib.h>

#include <uv.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "Updater"

/* If updater session couldnt run for task , retry after 3 minutes */
/*TOD - can we put this in database */
#define MA_UPDATER_TASK_RETRY_INTERVAL_INT                          (3 * 60 )

static ma_error_t mue_event_listener_sub_cb(const char *topic, ma_message_t *msg, void *cb_data);

struct ma_updater_request_handler_s {
    ma_context_t						*ma_context;

    ma_mue_session_t					*mue_session;        

	ma_updater_request_node_t			*current_req;

	ma_updater_request_queue_t			up_req_queue;

	ma_msgbus_subscriber_t              *mue_listener;

	uv_timer_t                          task_invoker;

	/*track the last task status.*/
	char last_session_id[MA_MAX_TASKID_LEN+1];
	ma_update_state_t  last_session_status;
};

ma_error_t ma_updater_request_handler_create(ma_context_t *ma_context, ma_updater_request_handler_t **handler) {
    if(ma_context && handler) {
		ma_error_t rc = MA_OK;
        ma_updater_request_handler_t *self = (ma_updater_request_handler_t *)calloc(1, sizeof(ma_updater_request_handler_t));

        if(!self) return MA_ERROR_OUTOFMEMORY;
		if(MA_OK == (rc = ma_msgbus_subscriber_create(MA_CONTEXT_GET_MSGBUS(ma_context), &self->mue_listener))) {				
			self->ma_context = ma_context;        					
			uv_timer_init(ma_event_loop_get_uv_loop(ma_msgbus_get_event_loop(MA_CONTEXT_GET_MSGBUS(self->ma_context))), &self->task_invoker);				
			self->task_invoker.data = self;
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_DEBUG, "uv timer data(%p)\n", self);
			(void) ma_msgbus_subscriber_setopt(self->mue_listener, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
			(void) ma_msgbus_subscriber_setopt(self->mue_listener, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);				
			*handler = self;
			return rc;
		}        
		free(self);
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_updater_request_handler_release(ma_updater_request_handler_t *self) {
    if(self ) {
		if(self->mue_listener) ma_msgbus_subscriber_release(self->mue_listener);
		free(self);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t mue_event_listener_sub_cb(const char *topic, ma_message_t *msg, void *cb_data){
	ma_updater_request_handler_t *self  = (ma_updater_request_handler_t*)cb_data;	
	if(self && topic && cb_data){
		const char *op_type = NULL;
		const char *session_id = NULL;
		ma_variant_t *payload = NULL;

		ma_message_get_property(msg, MA_MUEEVENT_PROP_OPERATION_TYPE, &op_type);			
		ma_message_get_property(msg, MA_MUEEVENT_PROP_SESSION_ID, &session_id);			
		ma_message_get_payload(msg, &payload);

		if(op_type && session_id && payload && self->mue_session)
			ma_mue_event_listener_add_event(ma_mue_session_get_event_listener(self->mue_session), atoi(op_type), session_id, payload);	
		ma_variant_release(payload);
	}
	return MA_OK;
}

ma_error_t ma_updater_request_handler_start(ma_updater_request_handler_t *self) {
    if(self) {
		return ma_msgbus_subscriber_register(self->mue_listener, MA_MUEEVENT_PUBLISH_TOPIC, mue_event_listener_sub_cb, self);        
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t handle_update_start_request(ma_updater_request_handler_t *self);
static void updater_task_request_invoker(uv_timer_t* handle, int status) {
    ma_updater_request_handler_t *self = (ma_updater_request_handler_t *)handle->data;	
	MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_DEBUG, "Starting updater engine if any updater task in the queue.");
	uv_timer_stop(&self->task_invoker);
	(void)handle_update_start_request(self);
}

static void updater_task_request_schedule(ma_updater_request_handler_t *self){
	uv_timer_stop(&self->task_invoker);	
	if(self->mue_session) return; 
	if(ma_updater_request_queue_is_empty(&self->up_req_queue)) return;

	MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_DEBUG, "Invoking the updater request handler.");
	uv_timer_start(&self->task_invoker, updater_task_request_invoker, 1, 0);	
};

static ma_error_t handle_update_stop_request(ma_updater_request_handler_t *self);
ma_error_t ma_updater_request_handler_stop(ma_updater_request_handler_t *self) {
    if(self) {        
		uv_timer_stop(&self->task_invoker);	
		uv_close((uv_handle_t *)&self->task_invoker, NULL);  

		if(self->mue_session) {		
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_DEBUG, "Stopping the currently running mue session.");
			handle_update_stop_request(self);
        }
		/*now remove all the request in the queue.*/
		ma_updater_request_queue_deinit(&self->up_req_queue);
		return ma_msgbus_subscriber_unregister(self->mue_listener);        
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_updater_request_handler_get_status(ma_updater_request_handler_t *self, ma_bool_t *busy){
	if(self && busy){
		*busy = (NULL != self->current_req || !ma_updater_request_queue_is_empty(&self->up_req_queue));
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_updater_request_handler_check_state(ma_updater_request_handler_t *self, const char *session_id, ma_update_state_t *state){
	if(self && session_id && state){
		*state = MA_UPDATE_STATE_FINISHED_NOTFOUND;
		if(self->mue_session && self->current_req && 0 == strcmp(self->current_req->updater_request->params.task_id, session_id) )
			ma_mue_event_listener_get_update_state(ma_mue_session_get_event_listener(self->mue_session), state);					
		else if(self->last_session_id[0] && 0 == strcmp(self->last_session_id,session_id))
			*state = self->last_session_status;		
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_updater_request_handler_submit_request(ma_updater_request_handler_t *self, ma_updater_request_t *request) {
	ma_error_t rc = MA_OK;
    if(self && request) {        
		switch(request->params.request_type) {
		case MA_UPDATER_REQUEST_TYPE_START:
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_INFO, "Adding the updater request taskid <%s> in the queue.", request->params.task_id);
				if(MA_OK == (rc = ma_updater_request_queue_push(&self->up_req_queue, request))){					
					ma_updater_request_notify(request, MA_UPDATER_REQUEST_NOT_STARTED);
					updater_task_request_schedule(self);									
				}
				else
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_ERROR, "Failed to add the updater request taskid <%s> in the queue.", request->params.task_id);				
				break;
            case MA_UPDATER_REQUEST_TYPE_STOP:
				if(self->current_req && !strcmp(self->current_req->updater_request->params.task_id, request->params.task_id)){
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_INFO, "Updater request taskid <%s> is in running state, stopping it.", request->params.task_id);
					rc = handle_update_stop_request(self);
				}
				else{
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_INFO, "Updater request taskid <%s> is in not running, removing it from the updater request queue if there.", request->params.task_id);
					ma_updater_request_queue_remove(&self->up_req_queue, request);
				}				
                break;
            default:
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_ERROR, "Invalid request to handle ,request type %d.", request->params.request_type);
                rc = MA_ERROR_UPDATER_INVALID_REQUEST;
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}


static void handle_session_in_progress(ma_updater_request_handler_t *self, ma_updater_request_t *request);
static ma_error_t ma_mue_session_on_exit_cb(ma_mue_session_t *session, const ma_updater_request_t *updaterreq, int exit_status, int term_signal, void *cb_data);

static ma_error_t handle_update_start_request(ma_updater_request_handler_t *self) {	
	ma_error_t rc = MA_OK;

	self->current_req = ma_updater_request_queue_pop(&self->up_req_queue);	
	if(self->current_req){
		if(MA_OK == (rc = ma_mue_session_create(self->ma_context, &self->mue_session))) {
			if(MA_OK == (rc = ma_mue_session_start(self->mue_session, self->current_req->updater_request, ma_mue_session_on_exit_cb, self))) {
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_INFO, "Updater session started for initiator type=%d, task id = <%s>.", self->current_req->updater_request->params.initiator_type, self->current_req->updater_request->params.task_id);                    					
				ma_updater_request_notify(self->current_req->updater_request, MA_UPDATER_REQUEST_STARTED);
			}
			else{
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_ERROR, "Failed to start mue session for initiator type = %d, task id = <%s>, rc=<%d>.", self->current_req->updater_request->params.initiator_type, self->current_req->updater_request->params.task_id, rc);                    					
				
				/*release mue session.*/
				ma_mue_session_release(self->mue_session);    
				self->mue_session = NULL;

				/*update task status*/
				ma_updater_request_notify(self->current_req->updater_request, MA_UPDATER_REQUEST_FAILED);
				ma_updater_request_node_free(self->current_req);				
				self->current_req = NULL;				
			}   
		}
		else
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_ERROR, "Failed to create mue session rc = < %d>.", rc);
	}
	else
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_ERROR, "No request in queue %d.", rc);
    return rc;
}

static ma_error_t handle_update_stop_request(ma_updater_request_handler_t *self) {
    MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_DEBUG, "Stopping the currently running mue session.");
	if(self->mue_session){
		/*stop the currently running mue session and release it.*/
		ma_mue_session_stop(self->mue_session);					
		ma_mue_session_release(self->mue_session);
		self->mue_session = NULL; 

		/*lets notify to updater request callback.*/
		if(self->current_req){			
			ma_updater_request_notify(self->current_req->updater_request, MA_UPDATER_REQUEST_STOPPED);
			ma_updater_request_node_free(self->current_req);	
			self->current_req = NULL;
		}				
	}
	return MA_OK;
}

static ma_error_t ma_mue_session_on_exit_cb(ma_mue_session_t *session, const ma_updater_request_t *updaterreq, int exit_status, int term_signal, void *cb_data){
    ma_updater_request_handler_t *self = (ma_updater_request_handler_t *)cb_data;    
	ma_updater_request_state_t updater_state = MA_UPDATER_REQUEST_STOPPED;	
	ma_mue_event_listener_t *event_listener = NULL; 
	ma_updater_initiator_type_t type = MA_UPDATER_INITIATOR_TYPE_UNKNOWN;

	/*add last task state*/
	strncpy(self->last_session_id, self->current_req->updater_request->params.task_id, MA_MAX_TASKID_LEN);
	 
	/*send the task summary*/	
	MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_DEBUG, "Mue session for initiator type %d finished, mue exit state %d.",updaterreq->params.initiator_type, exit_status);		
	event_listener = ma_mue_session_get_event_listener(self->mue_session);
	if(event_listener){
		(void)ma_mue_event_listener_set_mue_return_code(event_listener, exit_status);
		(void)ma_mue_event_listener_get_final_update_result(event_listener, &updater_state);	

		MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_DEBUG, "Update state via mue event <%d>, mue exit state <%d>.", updater_state, exit_status);						

		if(ma_updater_request_is_task_summary_needed(self->current_req->updater_request)){
			char *summary = NULL;
			ma_mue_event_listener_get_task_summary(event_listener, &summary);
		
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_DEBUG, "Sending task summary to task invoker.");						
			ma_updater_request_task_summary(self->current_req->updater_request, updater_state, summary);
			if(summary){
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_DEBUG, "Task summary xml : %s.", summary);						
				free(summary);		
			}
		}
		(void)ma_mue_event_listener_get_update_state(event_listener, &self->last_session_status);
	}
	else
		updater_state = (0 == exit_status ? MA_UPDATER_REQUEST_SUCCESS : MA_UPDATER_REQUEST_FAILED);

	/*Update the last successful update time to datastore*/
	type =  self->current_req->updater_request->params.initiator_type;
	if(MA_UPDATER_INITIATOR_TYPE_GLOBAL_UPDATE == type || MA_UPDATER_INITIATOR_TYPE_TASK_UPDATE == type || MA_UPDATER_INITIATOR_TYPE_ONDEMAND_UPDATE == type) {
		if (MA_UPDATER_REQUEST_SUCCESS == updater_state) {
			ma_configurator_t *configurator = MA_CONTEXT_GET_CONFIGURATOR(self->ma_context);
			ma_ds_t *ds = ma_configurator_get_datastore(configurator);
			ma_time_t time = {0};
			ma_message_t *message = NULL;
			char asc_time[17] = {0} ;
			ma_time_get_localtime(&time) ;
			MA_MSC_SELECT(_snprintf, snprintf)(asc_time, 16, "%04d%02d%02d%02d%02d%02d", time.year, time.month, time.day, time.hour, time.minute, time.second);
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_DEBUG, "Last Update time <%s> on successful update", asc_time);
			ma_ds_set_str(ds, MA_PROPERTY_DATA_PATH_NAME_STR, MA_AGENT_CONFIGURATION_LAST_UPDATE_STR, asc_time, strlen(asc_time)) ; 

			ma_update_registry_publisher(MA_CONTEXT_GET_MSGBUS(self->ma_context));
		}
	}

	/*update task status*/
	ma_updater_request_notify(self->current_req->updater_request, updater_state);
	ma_updater_request_node_free(self->current_req);
	self->current_req = NULL;

	/*free the mue session*/
	ma_mue_session_release(self->mue_session);
	self->mue_session = NULL;     

	/*schedule next task*/    
	updater_task_request_schedule(self);
	return MA_OK;
}
