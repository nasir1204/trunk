#ifndef MA_MUE_EVENT_H_INCLUDED
#define MA_MUE_EVENT_H_INCLUDED

#include "ma/internal/utils/datastructures/ma_slist.h"
#include "ma/internal/defs/ma_updater_defs.h"
#include "ma/internal/defs/ma_mue_defs.h"
#include "ma/updater/ma_updater_client_defs.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

MA_CPP(extern "C" {)

typedef struct ma_mue_event_s ma_mue_event_t;
struct ma_mue_event_s{
	long iEventId;
	int iSeverity;
	char *iProductId;
	char *iLocale;
	char *iUpdateType;
	int iUpdateState;
	int iUpdateError;
	char *iNewVersion;
	char *iDateTime;	
	MA_SLIST_NODE_DEFINE(ma_mue_event_t);
};

typedef struct ma_mue_event_list_s ma_mue_event_list_t;
struct ma_mue_event_list_s{	
	MA_SLIST_DEFINE(events, ma_mue_event_t);	
};

static ma_mue_event_t *mue_event_new(long iEventId, 
	int iSeverity, 
	const char *iProductId,
	const char *iLocale,
	const char *iUpdateType,
	int iUpdateState,
	int iUpdateError,
	const char *iNewVersion,
	const char *iDateTime)
{
	ma_mue_event_t *event = (ma_mue_event_t*)calloc(1, sizeof(ma_mue_event_t));;

	if(event){
		event->iEventId = iEventId;
		event->iSeverity = iSeverity;
		event->iProductId = strdup(iProductId);
		event->iLocale = strdup(iLocale);
		event->iUpdateType = strdup(iUpdateType);
		event->iUpdateState = iUpdateState;
		event->iUpdateError = iUpdateError;
		event->iNewVersion = strdup(iNewVersion);
		event->iDateTime = strdup(iDateTime);		
	}
	return event;
}

static void ma_mue_event_free(ma_mue_event_t *node){
	if(node){		
		if(node->iProductId) free(node->iProductId);
		if(node->iLocale) free(node->iLocale);
		if(node->iUpdateType) free(node->iUpdateType);
		if(node->iNewVersion) free(node->iNewVersion);
		if(node->iDateTime) free(node->iDateTime);
		free(node);
	}
}

static void ma_mue_event_list_init(ma_mue_event_list_t *self){	
	MA_SLIST_INIT(self, events);	
}

static void ma_mue_event_list_deinit(ma_mue_event_list_t *self){	
	MA_SLIST_CLEAR(self, events, ma_mue_event_t, ma_mue_event_free);
}

static ma_error_t ma_mue_event_list_push(ma_mue_event_list_t *self, ma_mue_event_t *event){
	MA_SLIST_PUSH_BACK(self, events, event);			
	return MA_OK;
}

#define MA_UPDATE_ERROR_MESSAGES \
	MA_UPDATE_MESSAGE(msgs, eUPDATE_ERROR_START_OK, "Update/deployment success") \
	MA_UPDATE_MESSAGE(msgs, eUPDATE_ERROR_LATEST, "Update, product running latest version") \
	MA_UPDATE_MESSAGE(msgs, eUPDATE_ERROR_FAILED_TO_START,  "Update not started") \
	MA_UPDATE_MESSAGE(msgs, eUPDATE_ERROR_CALLBACKINIT, "Callback component can not be loaded") \
	MA_UPDATE_MESSAGE(msgs, eUPDATE_ERROR_REJECT, "Point product rejected the update") \
	MA_UPDATE_MESSAGE(msgs, eUPDATE_ERROR_CANCEL, "User cancelled the update") \
	MA_UPDATE_MESSAGE(msgs, eUPDATE_ERROR_EOL, "End of life for product/scanengine") \
	MA_UPDATE_MESSAGE(msgs, eUPDATE_ERROR_LOADCONFIG, "Load config error") \
	MA_UPDATE_MESSAGE(msgs, eUPDATE_ERROR_DOWNLOAD, "Content download error") \
	MA_UPDATE_MESSAGE(msgs, eUPDATE_ERROR_POSTPONE, "User postpone update") \
	MA_UPDATE_MESSAGE(msgs, eUPDATE_ERROR_REPLICATE, "Script engine error") \
	MA_UPDATE_MESSAGE(msgs, eUPDATE_ERROR_SERVICE_STOP, "Script engine error") \
	MA_UPDATE_MESSAGE(msgs, eUPDATE_ERROR_SERVICE_START, "Script engine error") \
	MA_UPDATE_MESSAGE(msgs, eUPDATE_ERROR_INVALID, "Script engine error") \
	MA_UPDATE_MESSAGE(msgs, eUPDATE_ERROR_INVALID_DATS, "Invalid dats") \
	MA_UPDATE_MESSAGE(msgs, eUPDATE_ERROR_BACKUP, "Backup error") \
	MA_UPDATE_MESSAGE(msgs, eUPDATE_ERROR_ROLLBACK, "Rollback error") \
	MA_UPDATE_MESSAGE(msgs, eUPDATE_ERROR_COPY, "Script engine copy file error") \
	MA_UPDATE_MESSAGE(msgs, eUPDATE_ERROR_DISKSPACE, "Disk space error") \
	MA_UPDATE_MESSAGE(msgs, eUPDATE_ERROR_SETCONFIG, "Config error") \
	MA_UPDATE_MESSAGE(msgs, eUPDATE_ERROR_LICENSE_EXPIRED, "License expired") \
	MA_UPDATE_MESSAGE(msgs, eUPDATE_ERROR_REBOOT_PENDING, "Update/deployment failed because reboot pending") \
	MA_UPDATE_MESSAGE(msgs, eUPDATE_ERROR_UPDATEOPTIONS, "Script engine internal error") \
	MA_UPDATE_MESSAGE(msgs, eUPDATE_ERROR_SCRIP_ENGINE_TERMINATED_UNEXPECTEDLY, "Script engine terminated unexpectedly") \
	MA_UPDATE_MESSAGE(msgs, eUPDATE_ERROR_INVALID_PARAM, "Precondition doesn't meet") \
	MA_UPDATE_MESSAGE(msgs, eUPDATE_ERROR_UNZIP, "Unable to de-compress file") \
	MA_UPDATE_MESSAGE(msgs, eUPDATE_ERROR_NO_REPO, "Unable to find valid repository") \
    MA_UPDATE_MESSAGE(msgs, eUPDATE_ERROR_UPDPLUGIN, "Update callback plugin error") \
    MA_UPDATE_MESSAGE(msgs, eUPDATE_ERROR_NO_PRODUCTS, "No qualifying product for the update") \
    MA_UPDATE_MESSAGE(msgs, eUPDATE_ERROR_STOP_INITIATED, "Initiate update stop process") \
    MA_UPDATE_MESSAGE(msgs, eUPDATE_ERROR_MEMORY, "Memory allocation problem") \
	MA_UPDATE_MESSAGE(msgs, eUPDATE_ERROR_ANOTHER_SESSION_RUNNING, "Another Script engine running") \
	MA_UPDATE_MESSAGE(msgs, eUPDATE_ERROR_CREATE_DIR, "create directory error") \
    MA_UPDATE_MESSAGE(msgs, eUPDATE_ERROR_NETWORK, "network error") 

static const char *update_error_msgs[]= {
#define MA_UPDATE_MESSAGE(msgs, index, message) message,
MA_UPDATE_ERROR_MESSAGES
#undef MA_UPDATE_MESSAGE
};

static const char *update_msg(ma_update_error_t error_code){
	if(error_code < 0 )
		return "Unknown error";
	else if(error_code < sizeof(update_error_msgs)/sizeof(update_error_msgs[0]))
		return update_error_msgs[error_code];
	else
		return "Unknown error";
}

static const char *update_result(ma_update_error_t error_code){
	if(error_code == eUPDATE_ERROR_START_OK || error_code == eUPDATE_ERROR_LATEST )
		return "Success";
	return "Failure";
}

static int update_result_code(ma_update_error_t error_code){
	if(error_code == eUPDATE_ERROR_START_OK || error_code == eUPDATE_ERROR_LATEST )
		return 0;
	return 1;
}

MA_CPP(})

#endif //MA_SPIPE_ALERT_LIST_H_INCLUDED

