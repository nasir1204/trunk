#ifndef MA_UPDATER_REQUEST_QUEUE_H_INCLUDED
#define MA_UPDATER_REQUEST_QUEUE_H_INCLUDED

#include "ma/internal/utils/datastructures/ma_slist.h"
#include "ma_updater_request.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

MA_CPP(extern "C" {)

typedef struct ma_updater_request_node_s ma_updater_request_node_t;
struct ma_updater_request_node_s{
	ma_updater_request_t *updater_request;
	MA_SLIST_NODE_DEFINE(ma_updater_request_node_t);
};

typedef struct ma_updater_request_queue_s ma_updater_request_queue_t;
struct ma_updater_request_queue_s{	
	MA_SLIST_DEFINE(upd_req_q, ma_updater_request_node_t);	
};


static ma_updater_request_node_t *ma_updater_request_node_new(ma_updater_request_t *updater_request){
	ma_updater_request_node_t *node = NULL;
	if(updater_request){
		node = (ma_updater_request_node_t*) calloc(1, sizeof(ma_updater_request_node_t));	
		if(node)
			node->updater_request = updater_request;
	}
	return node;
}

static void ma_updater_request_node_free(ma_updater_request_node_t *node){
	if(node){
		if(node->updater_request)
			ma_updater_request_release(node->updater_request);
		free(node);
	}
}

static void ma_updater_request_node_stop_release(ma_updater_request_node_t *node){
	if(node){
		ma_updater_request_notify(node->updater_request, MA_UPDATER_REQUEST_STOPPED);
		ma_updater_request_node_free(node);
	}
}

static MA_INLINE void ma_updater_request_queue_init(ma_updater_request_queue_t *self){	
	MA_SLIST_INIT(self, upd_req_q);	
}

static MA_INLINE void ma_updater_request_queue_deinit(ma_updater_request_queue_t *self){	
	MA_SLIST_CLEAR(self, upd_req_q, ma_updater_request_node_t, ma_updater_request_node_stop_release);
}

static MA_INLINE ma_error_t ma_updater_request_queue_push(ma_updater_request_queue_t *self, ma_updater_request_t *updater_request){
	ma_updater_request_node_t *node= ma_updater_request_node_new(updater_request);
	if(node){				
		MA_SLIST_PUSH_BACK(self, upd_req_q, node);			
		return MA_OK;
	}
	return MA_ERROR_OUTOFMEMORY;
}

static MA_INLINE void ma_updater_request_queue_remove(ma_updater_request_queue_t *self, ma_updater_request_t *updater_request){
	ma_updater_request_node_t *current=NULL,*prev=NULL;	
	for ( prev=NULL, current = self->upd_req_q_head ; current != NULL ; prev=current,current=current->p_link){		
		if(!strcmp(current->updater_request->params.task_id, updater_request->params.task_id)){
			(prev) ? (prev->p_link=current->p_link) : (self->upd_req_q_head = current->p_link);			
			(void)ma_updater_request_node_stop_release(current);		
			if(!self->upd_req_q_head){
				ma_updater_request_queue_init(self);	
			}
			break;
		}
	}		
}

static MA_INLINE ma_updater_request_node_t *ma_updater_request_queue_pop(ma_updater_request_queue_t *self){
	ma_updater_request_node_t *node = NULL;	
	MA_SLIST_POP_FRONT(self, upd_req_q, node);	
	return node;
}

static ma_bool_t ma_updater_request_queue_is_empty(ma_updater_request_queue_t *self){
	return self->upd_req_q_count == 0;
}

MA_CPP(})

#endif //MA_UPDATER_REQUEST_QUEUE_H_INCLUDED


