#include "ma_updater_session.h"


#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/utils/configurator/ma_configurator.h"

#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/filesystem/path.h"

#include "ma/ma_log.h"
#include "ma/internal/defs/ma_scheduler_defs.h"
#include "ma/internal/services/scheduler/ma_scheduler_core.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <uv.h>


#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "Updater"


#ifdef MA_WINDOWS
#define MA_MUE_PROCESS_NAME                                         "Mue.exe"
/*Still use McScript name to get VSE 8.5 access protection.*/
#define MA_MUE_IN_USE_PROCESS_NAME                                  "McScript_InUse.exe"
#else
#define MA_MUE_PROCESS_NAME                                         "Mue"
/*Still use McScript name to get VSE 8.5 access protection.*/
#define MA_MUE_IN_USE_PROCESS_NAME                                  "Mue_InUse"
#endif

#define MA_MUE_LOG_FILE_NAME_STR                                    "McScript"

#define MA_MUE_UPDATE_SCRIPT_NAME_STR                               "UpdateMain.McS"

#define MA_MUE_DEPLOYMENT_SCRIPT_NAME_STR                           "InstallMain.McS"

/*Update engine timeout 60 minutes*/
#define MA_MUE_TIMEOUT_VALUE_INT                                    (2 * 60 * 60 * 1000)

#define    MA_DB_DIRECTORY_NAME_STR									"db"
#define    MA_LOG_DIRECTORY_NAME_STR								"logs"
#define    MA_UPDATE_DIRECTORY_NAME_STR								"update"

struct ma_updater_session_s {
    ma_context_t                            *ma_context;

    char                                    task_id[MA_MAX_TASKID_LEN];

    ma_int32_t                              updater_state;

    ma_updater_initiator_type_t             initiator_type;

    ma_bool_t                               is_mue_process_running;

    uv_process_t			                mue_process_handle;
    
    uv_timer_t                              mue_process_timer_handle;

    ma_udpater_session_on_exit_cb_t         cb; 

    void                                    *cb_data;

	int                                     ref_count;
	
};

ma_error_t ma_updater_session_create(ma_context_t *context,  ma_updater_session_t **session) {
    if(context  && session) {
        ma_updater_session_t *self = NULL;
        ma_error_t rc = MA_OK;
        
        self = (ma_updater_session_t *)calloc(1, sizeof(ma_updater_session_t));
        if(!self) return MA_ERROR_OUTOFMEMORY;
            
        self->ma_context = context;
		self->ref_count = 0;
		uv_timer_init(ma_event_loop_get_uv_loop(ma_msgbus_get_event_loop(MA_CONTEXT_GET_MSGBUS(self->ma_context))), &self->mue_process_timer_handle);				
		self->mue_process_timer_handle.data  = NULL;		

        *session = self;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static void final_call(uv_handle_t* handle) {
	ma_updater_session_t *self = (ma_updater_session_t*)handle->data;
	if(!(--self->ref_count)) free(self);
}

ma_error_t ma_updater_session_release(ma_updater_session_t *self) {
    if(self) {		
		self->is_mue_process_running = MA_FALSE;
		uv_timer_stop(&self->mue_process_timer_handle);
		uv_close((uv_handle_t *)(&self->mue_process_timer_handle), self->ref_count ? final_call : 0); 		
		uv_close((uv_handle_t*)(&self->mue_process_handle), self->ref_count ? final_call : 0);	            
		if(!self->ref_count) free(self);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_updater_session_set_task_id(ma_updater_session_t *self, const char *task_id) {
    if(self && task_id) {
        strncpy(self->task_id, task_id, MA_MAX_TASKID_LEN);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_updater_session_set_updater_state(ma_updater_session_t *self, ma_int32_t updater_state) {
    if(self) {
        self->updater_state = updater_state;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_updater_session_get_updater_state(ma_updater_session_t *self, ma_int32_t *updater_state) {
    if(self && updater_state) {
        *updater_state = self->updater_state ;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


static void mue_process_timer_timeout_cb(uv_timer_t* handle, int status);
static ma_error_t start_updater_engine_process(ma_updater_session_t *self);

ma_error_t ma_updater_session_start(ma_updater_session_t *self, ma_updater_initiator_type_t initiator_type, ma_udpater_session_on_exit_cb_t cb, void *cb_data) {
    if(self) {
        ma_error_t rc = MA_OK;
        if(self->is_mue_process_running)    return MA_ERROR_UPDATER_SESSION_IN_PROGRESS;
		        
        self->initiator_type = initiator_type;
        if(MA_OK == (rc = start_updater_engine_process(self))) {            
            self->mue_process_timer_handle.data = self;
			++self->ref_count;
            uv_timer_start(&self->mue_process_timer_handle, mue_process_timer_timeout_cb, MA_MUE_TIMEOUT_VALUE_INT, 0);	
            self->is_mue_process_running = MA_TRUE;
            self->cb = cb;
            self->cb_data = cb_data;
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_INFO, "Launched updater engine succesfully.");
        }
        else {
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_ERROR, "Failed to launch updater engine process, %d.", rc);            
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
    
}

static ma_error_t stop_updater_engine_process(ma_updater_session_t *self);
ma_error_t ma_updater_session_stop(ma_updater_session_t *self) {
    if(self) {
        ma_error_t rc = MA_OK;
        if(0 == uv_process_kill(&self->mue_process_handle, 0)){
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_DEBUG, "Triggering updater engine stop.");
            rc = stop_updater_engine_process(self);
			self->cb = NULL;
			self->cb_data = NULL;
        }
        else
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_DEBUG, "Updater engine is not running, nothing to stop.");
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_updater_session_is_progress(ma_updater_session_t *self, ma_bool_t *is_progress)
{
	if(self && is_progress){
		if(0 == uv_process_kill(&self->mue_process_handle, 0)){           
			*is_progress = MA_TRUE;
        }
        else{
			*is_progress = MA_FALSE;
		}
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

static ma_error_t copy_mue_to_mue_in_use(uv_loop_t *uv_loop, const char *mue_path_name, const char *mue_in_use_path_name) {
    ma_error_t rc = MA_OK;
    uv_fs_t fs_req = {0};
    
    /* TODO - some enhancements like dont copy if not changed */
	unlink(mue_in_use_path_name);
    if(0 == uv_fs_link(uv_loop, &fs_req, mue_path_name, mue_in_use_path_name, NULL)) {
        uv_fs_req_cleanup(&fs_req);
        return MA_OK;
    }
    /*TODO - proper return MA_ERROR_UPDATER_ENGINE_EXE_COPY_FAILED */
    return MA_ERROR_UPDATER_ENGINE_EXE_COPY_FAILED;
}


static void update_engine_process_exit_cb(uv_process_t* process, int exit_status, int term_signal);

static long generate_scriptid(){
	long id;
    srand(time(NULL));    
    id = rand();
    return id;
}

static ma_error_t start_updater_engine_process(ma_updater_session_t *self) {
    const char *agent_install_path = ma_configurator_get_agent_install_path(MA_CONTEXT_GET_CONFIGURATOR(self->ma_context));
    const char *agent_data_path = ma_configurator_get_agent_data_path(MA_CONTEXT_GET_CONFIGURATOR(self->ma_context));
    uv_loop_t *uv_loop = ma_event_loop_get_uv_loop(ma_msgbus_get_event_loop(MA_CONTEXT_GET_MSGBUS(self->ma_context)));
    ma_error_t rc = MA_OK;

    if(!uv_loop || !agent_install_path || !agent_data_path)  {
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_ERROR, "Failed to get one of the prerequsites for launching updater engine.");
        return MA_ERROR_UNEXPECTED;
    }
    else {
        char mue_in_use_exepath[MA_MAX_LEN] = {0} , 
			mue_exepath[MA_MAX_LEN] = {0}, mue_log_path[MA_MAX_LEN] = {0}, script_path[MA_MAX_LEN] = {0}, agent_path[MA_MAX_LEN] = {0}, 			
			initiator_type[8] = {0},script_id[24] = {0};

        MA_MSC_SELECT(_snprintf, snprintf)(mue_exepath, MA_MAX_LEN, "%s%s", agent_install_path, MA_MUE_PROCESS_NAME);
        MA_MSC_SELECT(_snprintf, snprintf)(mue_in_use_exepath, MA_MAX_LEN, "%s%s", agent_install_path, MA_MUE_IN_USE_PROCESS_NAME);
        MA_MSC_SELECT(_snprintf, snprintf)(mue_log_path, MA_MAX_LEN, "%s%s%c%s", agent_data_path, MA_LOG_DIRECTORY_NAME_STR, MA_PATH_SEPARATOR, MA_MUE_LOG_FILE_NAME_STR);
        MA_MSC_SELECT(_snprintf, snprintf)(initiator_type, 8, "%d",self->initiator_type);
		MA_MSC_SELECT(_snprintf, snprintf)(script_id, 24, "%Ld",generate_scriptid());
		MA_MSC_SELECT(_snprintf, snprintf)(script_path, MA_MAX_LEN, "%s%s%c%s", agent_data_path, MA_UPDATE_DIRECTORY_NAME_STR, MA_PATH_SEPARATOR, (self->initiator_type == MA_UPDATER_INITIATOR_TYPE_TASK_DEPLOYMENT ? MA_MUE_DEPLOYMENT_SCRIPT_NAME_STR : MA_MUE_UPDATE_SCRIPT_NAME_STR));
			
		MA_MSC_SELECT(_snprintf, snprintf)(agent_path, MA_MAX_LEN, "%s%c", agent_install_path, MA_PATH_SEPARATOR);

        if(MA_OK != (rc = copy_mue_to_mue_in_use(uv_loop,mue_exepath, mue_in_use_exepath))) {
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_ERROR, "Failed to copy %s to %s for starting engine , %d.", mue_exepath, mue_in_use_exepath , rc);
            return rc;
        }
        else {
            char *args[24] = {0};			
			int argv_index = 0;
			char commad_line[MA_MAX_PATH] = {0};
			int i = 0;

            uv_process_options_t	process_options = {0};
            uv_stdio_container_t child_stdio[3] ;
						
            args[argv_index++] = mue_in_use_exepath;
            args[argv_index++]  = "-script";       args[argv_index++] = script_path;
            args[argv_index++]  = "-id";           args[argv_index++] = script_id; //TODO - figure out the script id
            args[argv_index++]  = "-localeid";     args[argv_index++] = "0409"; //TODO - get the locale from somewhere.
            args[argv_index++]  = "-parent";       args[argv_index++] = "FRAMEWORK";
            args[argv_index++]  = "-logfile";      args[argv_index++] = mue_log_path;
            args[argv_index++] = "-initiator";    args[argv_index++] = initiator_type;
            args[argv_index++] = "-ipcid";        args[argv_index++] = MA_UPDATER_SERVICE_NAME_STR;
            args[argv_index++] = "-installdir";   args[argv_index++] = (char *)agent_path;
			if((self->initiator_type == MA_UPDATER_INITIATOR_TYPE_TASK_DEPLOYMENT) || (self->initiator_type == MA_UPDATER_INITIATOR_TYPE_TASK_UPDATE)){
				args[argv_index++] = "-taskid";       args[argv_index++] = self->task_id ;
			}			
			args[argv_index++] = 0;


			for(i = 0; i<argv_index; i++){
				MA_MSC_SELECT(_snprintf, snprintf)(commad_line + strlen(commad_line), MA_MAX_PATH - strlen(commad_line), "%s ", args[i]);
			}
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_INFO, "Invloking mue as, [%s].",commad_line);

            process_options.file = mue_in_use_exepath;
	        process_options.args = args;
	        process_options.exit_cb = update_engine_process_exit_cb;
            process_options.stdio_count = 3 ;
	        
            /*TODO - I just blindly copioed this but I am not comaortable with stdio flags */
            child_stdio[0].flags = child_stdio[1].flags = UV_IGNORE ;	        
            child_stdio[2].flags = UV_INHERIT_FD ; /*I dont think we need it */
	        child_stdio[2].data.fd = 2 ;
	        process_options.stdio = child_stdio ;
            
            //self->mue_process_handle = (uv_process_t *)calloc(1, sizeof(uv_process_t *));            
            //if(!self->mue_process_handle) return MA_ERROR_OUTOFMEMORY;
			self->mue_process_handle.data = self;			
	        if(0 != uv_spawn(uv_loop, &self->mue_process_handle, process_options)) {                
   		        MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_ERROR, "Failed to spawn updater engine process,%s.",uv_strerror(uv_last_error(uv_loop)));
   		        return MA_ERROR_UPDATER_ENGINE_SPAWN_FAILED;
   	        }
            
#ifdef MA_WINDOWS
            if(self->mue_process_handle.spawn_error.code != UV_OK ) {
#else
            if(self->mue_process_handle.errorno) {
#endif                
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_ERROR, "Updater engine spawen failed,%s.",uv_strerror(uv_last_error(uv_loop)));
   		        return MA_ERROR_UPDATER_ENGINE_SPAWN_FAILED ;
            }			
   	        
			++self->ref_count;
        }
    }
    return MA_OK;
}


static ma_error_t stop_updater_engine_process(ma_updater_session_t *self) {
    /*TODO - send a message or let the update engine handle the SIGTERM */
    MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_INFO, "Sending signal to updater engine for stop.");
    if(0 == uv_process_kill(&self->mue_process_handle, 0)) {
        uv_process_kill(&self->mue_process_handle, SIGTERM);				
    }	
    return MA_OK;
}


static void mue_process_timer_timeout_cb(uv_timer_t* handle, int status) {
    ma_updater_session_t *self = (ma_updater_session_t *)handle->data;	
    if(UV_OK == uv_process_kill(&self->mue_process_handle, 0)) {
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_INFO, "Triggering update engine stop due to timeout occured.");
        stop_updater_engine_process(self);
    }
}

static void update_engine_process_exit_cb(uv_process_t* process, int exit_status, int term_signal) {
    if(process) {
	    ma_task_t *task = NULL;
        ma_updater_session_t *self = (ma_updater_session_t *)process->data;
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->ma_context), MA_LOG_SEV_INFO, "Updater engine exited with exit status as %d and  term signal %d.", exit_status, term_signal);
        uv_timer_stop(&self->mue_process_timer_handle);
   	    self->is_mue_process_running = MA_FALSE;

	    if(MA_OK == ma_scheduler_get_task_handle(MA_CONTEXT_GET_SCHEDULER(self->ma_context), self->task_id, &task)){
		    /*Task execuate will be handled as per the mue events.*/
		    (void)ma_task_set_execution_status(task, MA_TASK_EXEC_SUCCESS);
	    }

	    if(self->cb)/*If stopped by the handler the it will go into self suicide mode*/
		    self->cb(self, self->initiator_type, self->updater_state, self->cb_data);	
	    else
		    ma_updater_session_release(self);
    }

}
