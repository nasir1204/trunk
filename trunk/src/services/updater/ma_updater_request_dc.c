#include "ma_updater_request.h"

#include "ma/internal/services/io/ma_dc_task_service.h"
#include "ma/scheduler/ma_task.h"
#include "ma/internal/services/scheduler/ma_scheduler_core.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"
#include "ma/internal/defs/ma_scheduler_defs.h"
#include "ma/internal/defs/ma_dc_task_service_defs.h"

#include "ma/datachannel/ma_datachannel.h"
#include "ma/datachannel/ma_datachannel_item.h"

#include "ma/ma_log.h"
#include "ma/internal/ma_macros.h"

#include <stdlib.h>
#include <string.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "Updater"

typedef struct ma_updater_request_dc_s ma_updater_request_dc_t;

static ma_error_t updater_request_dc_set_param(ma_updater_request_t *self, ma_updater_request_param_t *task_param);		
static ma_bool_t updater_request_dc_is_task_summary_needed(ma_updater_request_t *self);
static void updater_request_dc_task_summary(ma_updater_request_t *self, int task_status, const char *task_summary);
static void updater_request_dc_notify(ma_updater_request_t *self, ma_updater_request_state_t state);	
static ma_error_t updater_request_dc_set_ui_provider(ma_updater_request_t *req, const char *ui_provider) ;
static void updater_request_dc_destroy(ma_updater_request_t *self);

static const ma_updater_request_methods_t methods = {
	&updater_request_dc_set_param,
	&updater_request_dc_is_task_summary_needed,
	&updater_request_dc_task_summary,
	&updater_request_dc_notify,
	&updater_request_dc_destroy,
};

struct ma_updater_request_dc_s {
    ma_updater_request_t     base;	
	ma_context_t             *context;
	ma_task_t                *task;
	char	                 *dc_name;
	ma_uint64_t               corelation_id;
};

ma_error_t ma_updater_request_dc_create(ma_context_t *context, ma_updater_request_t **request){
    if(context && request){
        ma_updater_request_dc_t *self = NULL;
		        
        self = (ma_updater_request_dc_t *)calloc(1, sizeof(ma_updater_request_dc_t));
        if(!self) return MA_ERROR_OUTOFMEMORY;
		ma_updater_request_init( (ma_updater_request_t*)self, &methods, self);
		self->context = context;
        *request = (ma_updater_request_t*)self;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

const char *get_str(ma_variant_t *value){
	ma_buffer_t *buffer = NULL;
	const char *str = NULL;
	size_t size = 0;

	(void)ma_variant_get_string_buffer(value, &buffer);
	if(buffer){
		(void)ma_buffer_get_string(buffer, &str, &size);
		(void)ma_buffer_release(buffer);
	}
	return str  ? str : "";
}

ma_uint64_t get_int(ma_variant_t *value){
	ma_uint64_t int_v = 0;
	(void)ma_variant_get_uint64(value, &int_v);
	return int_v;
}

static ma_error_t updater_request_dc_set_param(ma_updater_request_t *req, ma_updater_request_param_t *task_param){
	ma_updater_request_dc_t *self = (ma_updater_request_dc_t*)req;
	if(self && task_param){
		ma_error_t rc = MA_OK;
        ma_task_t *task = NULL;

		MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Configuring the dc task updater request.");
		if(MA_OK == (rc = ma_scheduler_get_task_handle(MA_CONTEXT_GET_SCHEDULER(self->context), task_param->task_id, &task))){
			ma_variant_t *dc_task_setting_value = NULL;
            self->task = task;

			if(MA_OK == (rc = ma_task_get_setting(self->task, DC_SCHEDULED_TASK_SECTION_NAME_STR, DC_SCHEDULED_TASK_SETTING_NAME_STR, &dc_task_setting_value))){
				self->dc_name = strdup(get_str(dc_task_setting_value));
				(void)ma_variant_release(dc_task_setting_value); dc_task_setting_value = NULL;

				if(MA_OK == (rc = ma_task_get_setting(self->task, DC_SCHEDULED_TASK_SECTION_NAME_STR, DC_SCHEDULED_TASK_SETTING_CORELATION_ID_INT, &dc_task_setting_value))){			
					self->corelation_id = get_int(dc_task_setting_value);
					ma_variant_release(dc_task_setting_value);dc_task_setting_value = NULL;

					self->base.params.initiator_type  = task_param->initiator_type;
					self->base.params.request_type    = task_param->request_type;
					strncpy(self->base.params.task_id, task_param->task_id, MA_MAX_TASKID_LEN);										
					return MA_OK;
				}
				else
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to get run now task corelation id settings = < %d>." ,rc);					
			}
			else
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to get run now task name settings = < %d>." ,rc);					
		}		
		else
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to get run now updater task handle = < %d>." ,rc);		
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

static ma_bool_t updater_request_dc_is_task_summary_needed(ma_updater_request_t *req){	
	ma_updater_request_dc_t *self = (ma_updater_request_dc_t*)req;
    ma_task_exec_status_t existing_status = MA_TASK_EXEC_RUNNING;

	MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_TRACE, "Checking if dc task updater request needed task summary.");    
    (void)ma_task_get_execution_status(self->task, &existing_status);
	MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "dc task updater request status(%d)", existing_status);
    if(MA_TASK_EXEC_NOT_STARTED == existing_status) return MA_FALSE;
	return MA_TRUE;
}

static void updater_request_dc_task_summary(ma_updater_request_t *req, int task_status, const char *task_summary){
	ma_updater_request_dc_t *self = (ma_updater_request_dc_t*)req;
	if(self && task_summary){
		ma_datachannel_item_t *dc_item = NULL;
		ma_error_t             rc = MA_OK;
		ma_variant_t		  *variant = NULL;
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Sending dc task updater request summary to epo.");				

		if(MA_OK == (rc = ma_variant_create_from_raw((unsigned char *)task_summary,strlen(task_summary), &variant))) {
			if(MA_OK == (rc = ma_datachannel_item_create(DC_TASK_RUN_NOW_RESULTS, variant, &dc_item))){
				ma_datachannel_item_set_option(dc_item, MA_DATACHANNEL_PERSIST, MA_TRUE);
				ma_datachannel_item_set_correlation_id(dc_item, self->corelation_id);		
				ma_datachannel_item_set_ttl(dc_item, 5*1000);
				if(MA_OK == (rc = ma_datachannel_async_send(MA_CONTEXT_GET_CLIENT(self->context), MA_SOFTWAREID_GENERAL_STR, dc_item))){
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Sending the run now task %s execution summary to epo.", self->dc_name);						
				}
				else
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to send run now task %s execution summary to epo, rc = <%d>.", self->dc_name, rc);						
				(void)ma_datachannel_item_release(dc_item);
			}
			else
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to create dc item %s, rc = <%d>.", self->dc_name, rc);		
			(void)ma_variant_release(variant);
		}
		else
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to create dc item variant, rc = <%d>.", rc);		
	}

}

#define MA_DC_TASK_SUMMARY_REPORT_STR  "<Status>" "<Code>%d</Code>" "<TaskName></TaskName>" "<Detail></Detail>" "<FailureReason></FailureReason>" "</Status>"
static void dc_task_set_status(ma_updater_request_t *req, int status){
	char task_summary[1024] = {0};
	
	MA_MSC_SELECT(_snprintf, snprintf)(task_summary, 1024, MA_DC_TASK_SUMMARY_REPORT_STR, status);													
	/*send task summary to epo.*/
    updater_request_dc_task_summary(req, status, task_summary);    
}

static void updater_request_dc_notify(ma_updater_request_t *req, ma_updater_request_state_t state){
	ma_updater_request_dc_t *self = (ma_updater_request_dc_t*)req;	
	if(self->task){
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Updating dc task updater request status");			
		ma_task_set_execution_status(self->task, (ma_task_exec_status_t)state);	
		if(MA_UPDATER_REQUEST_STARTED == state) dc_task_set_status(req, MA_RUNNOW_TASK_STATUS_STARTED);		
		else if(MA_UPDATER_REQUEST_STOPPED == state) dc_task_set_status(req, MA_RUNNOW_TASK_STATUS_STOPPED);		
	}
}

static void updater_request_dc_destroy(ma_updater_request_t *req){
	ma_updater_request_dc_t *self = (ma_updater_request_dc_t*)req;
	MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Destroying dc task updater request status");			
	if(self->task){
		ma_task_release(self->task); 
		self->task = NULL;
	}
	if(self->dc_name){
		free(self->dc_name);	
		self->dc_name = NULL;
	}
	free(self);	
}



