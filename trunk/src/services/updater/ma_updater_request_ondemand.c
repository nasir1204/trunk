#include "ma_updater_request.h"

#include "ma/scheduler/ma_task.h"
#include "ma/internal/services/scheduler/ma_scheduler_core.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"
#include "ma/internal/defs/ma_scheduler_defs.h"

#include "ma/ma_log.h"

#include <stdlib.h>
#include <string.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "Updater"

typedef struct ma_updater_request_ondemand_s ma_updater_request_ondemand_t;

static ma_error_t updater_request_ondemand_set_param(ma_updater_request_t *self, ma_updater_request_param_t *task_param);		
static ma_bool_t updater_request_ondemand_is_task_summary_needed(ma_updater_request_t *self);
static void updater_request_ondemand_task_summary(ma_updater_request_t *self, int task_status, const char *task_summary);
static void updater_request_ondemand_notify(ma_updater_request_t *self, ma_updater_request_state_t state);	
static ma_error_t updater_request_ondemand_set_ui_provider(ma_updater_request_t *req, const char *ui_provider) ;
static void updater_request_ondemand_destroy(ma_updater_request_t *self);

static const ma_updater_request_methods_t methods = {
	&updater_request_ondemand_set_param,
	&updater_request_ondemand_is_task_summary_needed,
	&updater_request_ondemand_task_summary,
	&updater_request_ondemand_notify,
	&updater_request_ondemand_destroy,
};

struct ma_updater_request_ondemand_s {
    ma_updater_request_t    base;	
	ma_context_t            *context;
	ma_task_t               *task;
};

ma_error_t ma_updater_request_ondemand_create(ma_context_t *context, ma_updater_request_t **request) {
    if(context && request){
        ma_updater_request_ondemand_t *self = NULL;
		        
        self = (ma_updater_request_ondemand_t *)calloc(1, sizeof(ma_updater_request_ondemand_t));
        if(!self) return MA_ERROR_OUTOFMEMORY;
		ma_updater_request_init( (ma_updater_request_t*)self, &methods, self);
		self->context = context;
        *request = (ma_updater_request_t*)self;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t updater_request_ondemand_set_param(ma_updater_request_t *req, ma_updater_request_param_t *task_param){
	ma_updater_request_ondemand_t *self = (ma_updater_request_ondemand_t*)req;
	if(self && task_param){
		ma_error_t rc = MA_OK;		
        ma_task_t *task = NULL;
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Configuring the on demand updater request.");	
		if(MA_OK == (rc = ma_scheduler_get_task_handle(MA_CONTEXT_GET_SCHEDULER(self->context), task_param->task_id, &task))){
            self->task = task;
			self->base.params.initiator_type = task_param->initiator_type;
			self->base.params.request_type   = task_param->request_type;
			strncpy(self->base.params.task_id, task_param->task_id, MA_MAX_TASKID_LEN);			
		}		
		else
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to get ondemand updater task handle = < %d>." ,rc);		
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

static ma_bool_t updater_request_ondemand_is_task_summary_needed(ma_updater_request_t *req){	
	ma_updater_request_ondemand_t *self = (ma_updater_request_ondemand_t*)req;	
	MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Checking if on demand task updater request needed task summary.");	
	return MA_FALSE;
}

static void updater_request_ondemand_task_summary(ma_updater_request_t *self, int task_status, const char *task_summary){}

static void updater_request_ondemand_notify(ma_updater_request_t *req, ma_updater_request_state_t state){
	ma_updater_request_ondemand_t *self = (ma_updater_request_ondemand_t *)req;	
	MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Updating on demand task updater request status");		
	if(self->task)
		ma_task_set_execution_status(self->task, (ma_task_exec_status_t)state);	
}

static void updater_request_ondemand_destroy(ma_updater_request_t *req){
	ma_updater_request_ondemand_t *self = (ma_updater_request_ondemand_t*)req;	
	/* TBD: Should we release the task here or client side. */
	MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Destroying on demand updater request.");
	if(self->task){
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Releasing on demand updater task.");
        (void) ma_task_release(self->task);
		self->task = NULL;
        
	}
	free(self);	
}



