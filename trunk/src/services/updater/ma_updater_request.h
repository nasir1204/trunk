#ifndef MA_UPDATER_REQUEST_H_INCLUDED
#define MA_UPDATER_REQUEST_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_variant.h"

#include "ma/internal/services/updater/ma_updater_request_param.h"

MA_CPP(extern "C" {)

typedef struct ma_updater_request_s ma_updater_request_t, *ma_updater_request_h;
typedef struct ma_updater_request_methods_s ma_updater_request_methods_t;

typedef enum ma_updater_request_state_e{
	MA_UPDATER_REQUEST_NOT_STARTED		= 1,
    MA_UPDATER_REQUEST_STARTED			= 2,   
	MA_UPDATER_REQUEST_FAILED			= 3,
    MA_UPDATER_REQUEST_SUCCESS			= 4,    
	MA_UPDATER_REQUEST_STOPING			= 5,
	MA_UPDATER_REQUEST_STOPPED			= 6
}ma_updater_request_state_t;

struct ma_updater_request_methods_s{
	ma_error_t (*set_param)(ma_updater_request_t *self, ma_updater_request_param_t *task_param);		
	ma_bool_t (*is_task_summary_needed)(ma_updater_request_t *self);
	void (*task_summary)(ma_updater_request_t *self, int task_status, const char *task_summary);
	void (*notify)(ma_updater_request_t *self, ma_updater_request_state_t state);    
	void (*destroy)(ma_updater_request_t *self);
};

struct ma_updater_request_s{
	ma_updater_request_param_t			 params;
	const ma_updater_request_methods_t  *methods;
    void								*userdata;
};

MA_STATIC_INLINE void ma_updater_request_init(ma_updater_request_t *self, const ma_updater_request_methods_t  *methods, void *userdata){
	self->methods  = methods;
	self->userdata = userdata;
}

MA_STATIC_INLINE ma_error_t ma_updater_request_set_param(ma_updater_request_t *self, ma_updater_request_param_t *param){
	if(self && param && self->methods->set_param)
		return self->methods->set_param(self, param);	
	return MA_ERROR_INVALIDARG;
}

MA_STATIC_INLINE ma_bool_t ma_updater_request_is_task_summary_needed(ma_updater_request_t *self){
	if(self && self->methods->is_task_summary_needed)
		return self->methods->is_task_summary_needed(self);	
	return MA_FALSE;
}

MA_STATIC_INLINE void  ma_updater_request_task_summary(ma_updater_request_t *self, int task_status, const char *task_summary){
	if(self && self->methods->task_summary) self->methods->task_summary(self, task_status, task_summary);	
}

MA_STATIC_INLINE void ma_updater_request_notify(ma_updater_request_t *self, ma_updater_request_state_t state){
	if(self && self->methods->notify) self->methods->notify(self, state);
}

MA_STATIC_INLINE void ma_updater_request_release(ma_updater_request_t *self){
	if(self && self->methods->destroy) self->methods->destroy(self);
}

ma_error_t ma_updater_request_factory(ma_context_t *context, ma_updater_request_param_t *params, ma_updater_request_t **request);

MA_CPP(})

#endif  /* MA_UPDATER_REQUEST_H_INCLUDED */

