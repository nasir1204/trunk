#include "ma/internal/services/updater/ma_updater_service.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/services/ma_service.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/services/ma_policy_settings_bag.h"

#include "ma/internal/ma_strdef.h"

#include "ma/internal/services/updater/ma_updater_utils.h"

#include "ma_updater_request_handler.h"

#include "ma/ma_msgbus.h"
#include "ma/ma_message.h"

#include "ma/ma_log.h"

#include <stdlib.h>
#include <string.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "Updater"

/* Back compat task types for updating */
#define MA_UPDATER_TASK_TYPE_UPDATE_COMPAT_STR                      "Update"
#define MA_UPDATER_TASK_TYPE_DEPLOYMENT_COMPAT_STR                  "Deployment"
#define MA_UPDATER_TASK_TYPE_DEPLOYMENT_SENSOR_COMPAT_STR                  "SensorDeployment"

typedef struct ma_updater_policy_s {
    ma_bool_t       enable_reboot_ui;

    ma_int32_t      reboot_timeout;

    ma_bool_t       enable_dat_downgrade;

    ma_bool_t       run_exe_on_update_success;

    ma_buffer_t     *run_exe_after_update;
}ma_updater_policy_t;


struct ma_updater_service_s {
    ma_service_t                    base_service;            /* base service object */
    
    ma_context_t                    *context;

    ma_msgbus_server_t              *updater_server;        /* message bus server */

    ma_msgbus_subscriber_t          *updater_subscriber;     /* msgbus subscriber */

    ma_context_t                    *ma_context;

	ma_updater_request_handler_t    *request_handler;       /* request handler */

    ma_updater_policy_t             policy;                 /* policies of updater */

    ma_logger_t                     *logger;
    
};

static ma_error_t ma_updater_service_configure(ma_updater_service_t *service, ma_context_t *context, unsigned hint);
static ma_error_t ma_updater_service_start(ma_updater_service_t *service);
static ma_error_t ma_updater_service_stop(ma_updater_service_t *service);
static void ma_updater_service_release(ma_updater_service_t *service);

static ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint) {
    return ma_updater_service_configure((ma_updater_service_t *)service, context, hint);
}

static ma_error_t service_start(ma_service_t *service) {
    return ma_updater_service_start((ma_updater_service_t *)service);
}

static ma_error_t service_stop(ma_service_t *service) {
    return ma_updater_service_stop((ma_updater_service_t *)service);
}

static void service_release(ma_service_t *service) {
    ma_updater_service_release((ma_updater_service_t *)service);
}

static const ma_service_methods_t service_methods = {
    &service_configure,
    &service_start,
    &service_stop,
    &service_release
};	

ma_error_t ma_updater_service_create(const char *service_name, ma_service_t **updater_service) {
    if(service_name && updater_service) {
        ma_error_t rc = MA_OK ;
        ma_updater_service_t *self = (ma_updater_service_t *)calloc(1, sizeof(ma_updater_service_t));
        if(self) {
            ma_service_init(&self->base_service, &service_methods, service_name, self);
            *updater_service = &self->base_service;
			return MA_OK;
        } 
        return MA_ERROR_OUTOFMEMORY;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_updater_service_configure(ma_updater_service_t *self, ma_context_t *context, unsigned hint) {
    if(self && context) {
        ma_error_t rc = MA_OK;
        ma_policy_settings_bag_t *bag = MA_CONTEXT_GET_POLICY_SETTINGS_BAG(context);

        if(MA_SERVICE_CONFIG_NEW == hint) {
            self->context = context;
            self->logger = MA_CONTEXT_GET_LOGGER(context);
            if(MA_OK == (rc = ma_updater_request_handler_create(context, &self->request_handler))) {
                if(MA_OK == (rc = ma_msgbus_server_create(MA_CONTEXT_GET_MSGBUS(context), MA_UPDATER_SERVICE_NAME_STR, &self->updater_server))) {
                    (void)ma_msgbus_server_setopt(self->updater_server, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);  
                    (void)ma_msgbus_server_setopt(self->updater_server, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
                    if(MA_OK == (rc = ma_msgbus_subscriber_create(MA_CONTEXT_GET_MSGBUS(context), &self->updater_subscriber))) {
                        (void)ma_msgbus_subscriber_setopt(self->updater_subscriber, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_INPROC);
                        (void)ma_msgbus_subscriber_setopt(self->updater_subscriber, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
                    }
                    else {
                        (void)ma_msgbus_server_release(self->updater_server);
						self->updater_server = NULL;
                        (void)ma_updater_request_handler_release(self->request_handler);                        
						self->request_handler = NULL;
                    }
                }                 
                else{
                    (void)ma_updater_request_handler_release(self->request_handler);
					self->request_handler = NULL;
				}
            }            
        }

        if(MA_OK == rc && bag) {
            (void)ma_policy_settings_bag_get_bool(bag, MA_UPDATER_SERVICE_SECTION_NAME_STR, MA_UPDATER_KEY_ENABLE_REBOOT_UI_INT, MA_FALSE,&self->policy.enable_reboot_ui, MA_FALSE);
            (void)ma_policy_settings_bag_get_int(bag, MA_UPDATER_SERVICE_SECTION_NAME_STR, MA_UPDATER_KEY_REBOOT_TIMEOUT_INT, MA_FALSE,&self->policy.reboot_timeout, -1);
            (void)ma_policy_settings_bag_get_bool(bag, MA_UPDATER_SERVICE_SECTION_NAME_STR, MA_UPDATER_KEY_ENABLE_DAT_DOWNGRADE_INT, MA_FALSE,&self->policy.enable_dat_downgrade, MA_FALSE);

            if(MA_OK == rc)
                rc = ma_policy_settings_bag_get_bool(bag, MA_UPDATER_SERVICE_SECTION_NAME_STR, MA_UPDATER_KEY_ENABLE_RUN_EXE_AFTER_UPDATE_INT, MA_FALSE,&self->policy.run_exe_on_update_success, MA_FALSE);

            if(MA_OK == rc && self->policy.run_exe_on_update_success) {                
                if(self->policy.run_exe_after_update) ma_buffer_release(self->policy.run_exe_after_update);        
                rc = ma_policy_settings_bag_get_buffer(bag, MA_UPDATER_SERVICE_SECTION_NAME_STR, MA_UPDATER_KEY_EXE_NAME_TO_RUN_AFTER_UPDATE_STR, MA_FALSE,&self->policy.run_exe_after_update, "");
            }            
        }        
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t updater_server_handler(ma_msgbus_server_t *server, ma_message_t *request, void *cb_data, ma_msgbus_client_request_t *c_request);
static ma_error_t updater_subscriber_handler(const char *topic, ma_message_t *message, void *cb_data);

ma_error_t ma_updater_service_start(ma_updater_service_t *self) {
    if(self) {
        ma_error_t rc = MA_OK;
        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Updater service starting.");

        if(MA_OK != (rc = ma_updater_request_handler_start(self->request_handler)))
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to start request handler rc = <%d>", rc);

        if((MA_OK == rc) &&(MA_OK != (rc = ma_msgbus_subscriber_register(self->updater_subscriber,  MA_GENERAL_SUBSCRIBER_TOPIC_STR, updater_subscriber_handler, self))))
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to subscribe to topic <%s> rc = <%d>", MA_GENERAL_SUBSCRIBER_TOPIC_STR, rc);

		
        if((MA_OK == rc) && (MA_OK != ma_msgbus_server_start(self->updater_server, updater_server_handler, self)))
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to start updater service rc = <%d>", rc);
     
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_updater_service_stop(ma_updater_service_t *self) {
    if(self) {
        ma_error_t rc = MA_OK;
        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Updater service stopping.");

        
        if(MA_OK != (rc = ma_msgbus_subscriber_unregister(self->updater_subscriber)))
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to unsubscribe rc = <%d>", rc);
        
        if(MA_OK != (rc = ma_msgbus_server_stop(self->updater_server)))
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to stop updater service rc = <%d>", rc);

        if(MA_OK != (rc = ma_updater_request_handler_stop(self->request_handler)))
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to stop request handler rc = %d", rc);

        /* TBD :: Do we need to clear the providers?? if we do , does we need to inform providers ??
        if(MA_OK != (rc = ma_updater_ui_provider_handler_clear_providers(self->ui_provider_handler)))
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to clear ui providers rc = %d", rc) ;
        */
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

void ma_updater_service_release(ma_updater_service_t *self) {
    if(self) {
        ma_error_t rc = MA_OK;

        if(MA_OK != (rc = ma_msgbus_server_release(self->updater_server)))
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "msgbus server release failed rc = <%d>", rc);

        if(MA_OK != (rc = ma_msgbus_subscriber_release(self->updater_subscriber)))
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "msgbus subscriber release failed rc = <%d>", rc);

        /* clean policeis */
        if(self->policy.run_exe_after_update) 
            ma_buffer_release(self->policy.run_exe_after_update);
        
        if(MA_OK != (rc = ma_updater_request_handler_release(self->request_handler)))
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "updater request handler release failed rc = <%d>", rc);

        free(self);
    }
}

static ma_error_t submit_update_stop_request(ma_updater_service_t *self, ma_updater_request_param_t *updater_request_param){
	if(self && updater_request_param){
		ma_error_t rc = MA_OK;
		ma_updater_request_t *upd_req = NULL;

		MA_LOG(self->logger, MA_LOG_SEV_INFO, "Received updater request initiator_type=%d request_type=%d taskid=%s.", updater_request_param->initiator_type, updater_request_param->request_type, updater_request_param->task_id);   

		if(MA_UPDATER_REQUEST_TYPE_STOP == updater_request_param->request_type){
			
			if(MA_OK == (rc = ma_updater_request_factory(self->context, updater_request_param, &upd_req))){
            
				if(MA_OK != (rc = ma_updater_request_handler_submit_request(self->request_handler, upd_req)))
					MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to process the update stop reqest.");   														
								
				ma_updater_request_release(upd_req);			
				upd_req = NULL;
			}		
		}
		return rc;
	}
	return MA_OK;
}

static ma_error_t submit_repository_ranking_request_message(ma_updater_service_t *self){
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	if(self) {
		ma_message_t *repository_ranking_msg = NULL;

		if(MA_OK == (rc = ma_message_create(&repository_ranking_msg))) {
			ma_msgbus_endpoint_t *endpoint = NULL ;
			ma_message_set_property(repository_ranking_msg, MA_REPOSITORY_MSG_KEY_TYPE_STR, MA_REPOSITORY_MSG_PROP_VAL_RQST_RANK_REPOSITORIES_STR) ;

			if(MA_OK == (rc = ma_msgbus_endpoint_create(MA_CONTEXT_GET_MSGBUS(self->context), MA_REPOSITORY_SERVICE_NAME_STR, NULL, &endpoint))) {
				ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);

				if(MA_OK == (rc = ma_msgbus_send_and_forget(endpoint, repository_ranking_msg))) {
					MA_LOG(self->logger, MA_LOG_SEV_TRACE, "successfully sent repositories ranking request."); 
				}
				else
					MA_LOG(self->logger, MA_LOG_SEV_INFO, "repositories ranking request send failed."); 
				ma_msgbus_endpoint_release(endpoint) ;
			}
			(void)ma_message_release(repository_ranking_msg) ;
		}
	}
	return rc ;
}


static ma_error_t submit_update_start_request(ma_updater_service_t *self, ma_updater_request_param_t *updater_request_param){
	if(self && updater_request_param){
		ma_error_t rc = MA_OK;
		ma_updater_request_t *upd_req = NULL;

		MA_LOG(self->logger, MA_LOG_SEV_INFO, "Received updater request initiator_type=%d request_type=%d taskid=%s.", updater_request_param->initiator_type, updater_request_param->request_type, updater_request_param->task_id);   
		(void)submit_repository_ranking_request_message(self);
		if(MA_UPDATER_REQUEST_TYPE_START == updater_request_param->request_type){
			if(MA_UPDATER_INITIATOR_TYPE_ONDEMAND_UPDATE == updater_request_param->initiator_type){
				ma_bool_t busy = MA_FALSE;
				if(MA_OK == ma_updater_request_handler_get_status(self->request_handler, &busy) && busy){
					MA_LOG(self->logger, MA_LOG_SEV_INFO, "Received on Demand updater request can't be completed");   
					return MA_ERROR_UPDATER_SESSION_IN_PROGRESS;
				}
			}
		}
		
		if(MA_OK == (rc = ma_updater_request_factory(self->context, updater_request_param, &upd_req))){

			if(MA_OK != (rc = ma_updater_request_handler_submit_request(self->request_handler, upd_req))){
				MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to process the update start reqest.");   														
				ma_updater_request_release(upd_req);			
				upd_req = NULL;
			}			
		}		
		return rc;
	}
	return MA_OK;
}

ma_error_t handle_update_request(ma_updater_service_t *self, ma_updater_request_param_t *updater_request_param){
	ma_error_t rc = MA_OK;
	switch(updater_request_param->request_type){
		case MA_UPDATER_REQUEST_TYPE_START:
			{
				MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Processing update start request.");   														
				rc = submit_update_start_request(self, updater_request_param);
			}
			break;
		case MA_UPDATER_REQUEST_TYPE_STOP:
			{
				MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Processing update stop request.");   														
				rc = submit_update_stop_request(self, updater_request_param);					
			}
			break;
	}
	return rc;
}
/*handle updater service message.*/
static ma_error_t handle_update_request_message(ma_updater_service_t *self, ma_message_t *request_msg, ma_variant_t **response_payload) {
    ma_error_t rc = MA_OK ;
    ma_variant_t *payload = NULL;

    if(MA_OK == (rc = ma_message_get_payload(request_msg, &payload))) {
        ma_updater_request_param_t *updater_request_param = NULL;

        if(MA_OK == (rc = ma_updater_request_param_from_variant(payload, &updater_request_param))) {
			switch(updater_request_param->request_type){

				case MA_UPDATER_REQUEST_TYPE_START:
				case MA_UPDATER_REQUEST_TYPE_STOP:
					rc = handle_update_request(self, updater_request_param);
					break;
				case MA_UPDATER_REQUEST_TYPE_STATUS_CHECK:
					{
						ma_update_state_t state = MA_UPDATE_STATE_UNKNOWN;
						(void)ma_updater_request_handler_check_state(self->request_handler, updater_request_param->task_id, &state);
						(void)ma_variant_create_from_int16(state, response_payload);
					}
					break;
				default:
					MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Invalid request type <%d>", updater_request_param->request_type);
			}			
			ma_updater_request_param_release(updater_request_param);
        }
        else {
            rc = MA_ERROR_UPDATER_INVALID_REQUEST;
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Malformed request  rc = <%d>", rc);
        }
        (void)ma_variant_release(payload);
    }
    return rc;
}

static ma_error_t updater_server_handler(ma_msgbus_server_t *server, ma_message_t *request_msg, void *cb_data, ma_msgbus_client_request_t *c_request) {
    if(server && request_msg && cb_data && c_request) {
        ma_error_t rc = MA_OK, status = MA_OK ;
        ma_updater_service_t *self	  = (ma_updater_service_t *)cb_data ;        
        ma_message_t *reply = NULL;
		ma_variant_t *reply_payload = NULL;
        char *msg_type = NULL ;

        status = handle_update_request_message(self, request_msg, &reply_payload);

        if(MA_OK == (rc = ma_message_create(&reply))) {
			if(reply_payload)
				(void)ma_message_set_payload(reply, reply_payload);
            if(MA_OK == ma_msgbus_server_client_request_post_result(c_request, status, reply))
				MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Reply status <%d> to updater clients", status); 			
			else
				MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to send the reply status and message");   
            (void)ma_message_release(reply);
        }
        else
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to create reply message");

		if(reply_payload) (void)ma_variant_release(reply_payload);
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}


static ma_error_t updater_subscriber_handler(const char *topic, ma_message_t *message, void *cb_data) {    
     if(topic && message && cb_data) {
        ma_error_t rc = MA_OK;
        ma_updater_service_t *self = (ma_updater_service_t *)cb_data;
        
		/*handle scheduled task.*/
        if( !strncmp(topic, MA_TASK_EXECUTE_PUBSUB_TOPIC, strlen(MA_TASK_EXECUTE_PUBSUB_TOPIC)) || 
            !strncmp(topic, MA_TASK_STOP_PUBSUB_TOPIC, strlen(MA_TASK_STOP_PUBSUB_TOPIC))) {
            const char *task_type = NULL , *task_owner_id = NULL , *task_id = NULL;	
            if( (MA_OK == (rc = ma_message_get_property(message, MA_TASK_TYPE, &task_type))) && task_type && 
                (MA_OK == (rc = ma_message_get_property(message, MA_TASK_PRODUCT_ID, &task_owner_id))) && task_owner_id && 
                (MA_OK == (rc = ma_message_get_property(message, MA_TASK_ID, &task_id))) && task_id) {
                
				if(
				   !strncmp(task_type, MA_UPDATER_TASK_TYPE_UPDATE_COMPAT_STR, strlen(MA_UPDATER_TASK_TYPE_UPDATE_COMPAT_STR)) ||
				   !strncmp(task_type, MA_UPDATER_TASK_TYPE_UPDATE_STR, strlen(MA_UPDATER_TASK_TYPE_UPDATE_STR)) || 
				   !strncmp(task_type, MA_UPDATER_TASK_TYPE_DEPLOYMENT_COMPAT_STR, strlen(MA_UPDATER_TASK_TYPE_DEPLOYMENT_COMPAT_STR)) || 
				   !strncmp(task_type, MA_UPDATER_TASK_TYPE_DEPLOYMENT_SENSOR_COMPAT_STR, strlen(MA_UPDATER_TASK_TYPE_DEPLOYMENT_SENSOR_COMPAT_STR)) || 
				   !strncmp(task_type, MA_UPDATER_TASK_TYPE_DEPLOYMENT_STR, strlen(MA_UPDATER_TASK_TYPE_DEPLOYMENT_STR)) 
				  ) 
				{
					if(!strncmp(task_owner_id, MA_SOFTWAREID_META_STR, strlen(MA_SOFTWAREID_META_STR)) || 
					   !strncmp(task_owner_id, MA_SOFTWAREID_GENERAL_STR, strlen(MA_SOFTWAREID_GENERAL_STR))) 
					{						
							ma_updater_request_param_t request_param;
							ma_updater_request_type_t request_type = (0 == strncmp(topic, MA_TASK_EXECUTE_PUBSUB_TOPIC, strlen(MA_TASK_EXECUTE_PUBSUB_TOPIC)))
																	 ? MA_UPDATER_REQUEST_TYPE_START : MA_UPDATER_REQUEST_TYPE_STOP;

							char *req_type[] = {"\"Updater Start\"", "\"Updater Stop\""};
							ma_updater_initiator_type_t initiator_type = 
								  ( !strncmp(task_type, MA_UPDATER_TASK_TYPE_UPDATE_COMPAT_STR, strlen(MA_UPDATER_TASK_TYPE_UPDATE_COMPAT_STR)) ||
									!strncmp(task_type, MA_UPDATER_TASK_TYPE_UPDATE_STR, strlen(MA_UPDATER_TASK_TYPE_UPDATE_STR)))
								  ? MA_UPDATER_INITIATOR_TYPE_TASK_UPDATE 
								  : MA_UPDATER_INITIATOR_TYPE_TASK_DEPLOYMENT;                        
                    							
							request_param.initiator_type = initiator_type;
							request_param.request_type   = request_type;
							strncpy(request_param.task_id, task_id, MA_MAX_TASKID_LEN);

							if(MA_OK == (rc = handle_update_request(self, &request_param))) 
								MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Processed %s request for initiator type %d to updater service.", req_type[request_type], initiator_type);
							else
								MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Processing %s request for initiator type %d to updater service failed, %d.", req_type[request_type], initiator_type, rc);
						
					}
					else
						MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Task owner id %s didn't match agent software id.", task_owner_id);
				}
				else
					MA_LOG(self->logger, MA_LOG_SEV_TRACE, "Task %s is not a updater task.", task_id);
					
            }
            else
                MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Message property get failed, %d.", rc);
        }
        return rc;
    }   
    return MA_OK;
}

