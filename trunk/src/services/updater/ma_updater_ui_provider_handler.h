#ifndef MA_UPDATER_UI_PROVIDER_HANDLER_H_INCLUDED
#define MA_UPDATER_UI_PROVIDER_HANDLER_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_variant.h"
#include "ma/internal/defs/ma_object_defs.h"

MA_CPP(extern "C" {)

typedef struct ma_updater_ui_provider_handler_s ma_updater_ui_provider_handler_t, *ma_updater_ui_provider_handler_h ;

ma_error_t ma_updater_ui_provider_handler_create(ma_context_t *context, ma_updater_ui_provider_handler_t **ui_provider_handler) ;

ma_error_t ma_updater_ui_provider_handler_release(ma_updater_ui_provider_handler_t *self) ;

ma_error_t ma_updater_ui_provider_handler_add_provider(ma_updater_ui_provider_handler_t *self, const char *provider_name) ;

ma_error_t ma_updater_ui_provider_handler_remove_provider(ma_updater_ui_provider_handler_t *self, const char *provider_name) ;

ma_error_t ma_updater_ui_provider_handler_get_provider(ma_updater_ui_provider_handler_t *self, char **provider_name) ;

ma_error_t ma_updater_ui_provider_handler_get_providers(ma_updater_ui_provider_handler_t *self, ma_array_t **providers) ;

ma_error_t ma_updater_ui_provider_handler_clear_providers(ma_updater_ui_provider_handler_t *self) ;

    
MA_CPP(})

#endif  /* MA_UPDATER_UI_PROVIDER_HANDLER_H_INCLUDED */