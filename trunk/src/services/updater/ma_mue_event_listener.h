#ifndef MA_MUE_EVENT_LISTENER_H_INCLUDED
#define MA_MUE_EVENT_LISTENER_H_INCLUDED


#include "ma/ma_common.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/defs/ma_updater_defs.h"
#include "ma_updater_request.h"

MA_CPP(extern "C" {)

typedef struct ma_mue_event_listener_s ma_mue_event_listener_t;

ma_error_t ma_mue_event_listener_create(ma_context_t *context, const char *task_id, const char *task_name, const char *task_creator, ma_updater_initiator_type_t initiator_type, ma_mue_event_listener_t **listener);

ma_error_t ma_mue_event_listener_set_mue_return_code(ma_mue_event_listener_t *self, int mue_rc);

ma_error_t ma_mue_event_listener_set_locale(ma_mue_event_listener_t *self, const char *locale);

ma_error_t ma_mue_event_listener_add_event(ma_mue_event_listener_t *self, int op_type, const char *session_id, ma_variant_t *event_data);

ma_error_t ma_mue_event_listener_get_task_summary(ma_mue_event_listener_t *self, char **xml_report);

ma_error_t ma_mue_event_listener_is_reboot_required(ma_mue_event_listener_t *self, ma_bool_t *is_reboot_required);

ma_error_t ma_mue_event_listener_get_task_state(ma_mue_event_listener_t *self, ma_updater_request_state_t *curr_state);

ma_error_t ma_mue_event_listener_get_update_state(ma_mue_event_listener_t *self, ma_update_state_t *curr_state);

ma_error_t ma_mue_event_listener_get_final_update_result(ma_mue_event_listener_t *self, ma_updater_request_state_t *final_state);

ma_error_t ma_mue_event_listener_release(ma_mue_event_listener_t *self);

MA_CPP(})

#endif

