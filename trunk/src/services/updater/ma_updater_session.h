#ifndef MA_UPDATER_SESSION_H_INCLUDED
#define MA_UPDATER_SESSION_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/defs/ma_updater_defs.h"
#include "ma/updater/ma_updater_client_defs.h"
MA_CPP(extern "C" {)

typedef struct ma_updater_session_s ma_updater_session_t, *ma_updater_session_h ;


ma_error_t ma_updater_session_create(ma_context_t *context, ma_updater_session_t **session);

ma_error_t ma_updater_session_release(ma_updater_session_t *session);

typedef ma_error_t (*ma_udpater_session_on_exit_cb_t)(ma_updater_session_t *session, ma_updater_initiator_type_t initiator_type, ma_int32_t updater_session_state, void *cb_data);

ma_error_t ma_updater_session_start(ma_updater_session_t *session, ma_updater_initiator_type_t initiator_type, ma_udpater_session_on_exit_cb_t cb, void *cb_data);

ma_error_t ma_updater_session_stop(ma_updater_session_t *session);

ma_error_t ma_updater_session_is_progress(ma_updater_session_t *session, ma_bool_t *is_progress);

ma_error_t ma_updater_session_set_task_id(ma_updater_session_t *session, const char *task_id);

ma_error_t ma_updater_session_get_task_id(ma_updater_session_t *session, const char **task_id);

ma_error_t ma_updater_session_set_updater_state(ma_updater_session_t *session, ma_int32_t updater_session_state);

ma_error_t ma_updater_session_get_updater_state(ma_updater_session_t *session, ma_int32_t *updater_session_state);

MA_CPP(})

#endif  /* MA_UPDATER_SESSION_H_INCLUDED */