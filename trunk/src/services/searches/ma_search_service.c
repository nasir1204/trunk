#include "ma/internal/services/search/ma_search_service.h"
#include "ma/internal/utils/context/ma_context.h"
#include "ma/internal/utils/search/ma_search.h"
#include "ma/internal/utils/search/ma_search_info.h"
#include "ma/internal/defs/ma_search_defs.h"
#include "ma/internal/services/ma_service.h"
#include "ma/internal/utils/season/ma_season_info.h"
#include "ma/internal/utils/season/ma_season.h"
#include "ma/ma_log.h"
#include "ma/ma_message.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/ma_variant.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/sensor/ma_sensor_msg.h"
#include "ma/sensor/ma_sensor_utils.h"
#include "ma/ma_message.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/ma_strdef.h"
#include "ma/scheduler/ma_task.h"
#include "ma/scheduler/ma_triggers.h"
#include "ma/scheduler/ma_enumerator.h"
#include "ma/scheduler/ma_triggerlist.h"
#include "ma/internal/services/scheduler/ma_scheduler_core.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"
#include "ma/internal/utils/json/ma_json_utils.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

const char base_path[] = "/search/";

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "search service"

MA_CPP(extern "C" {)
MA_CPP(})

struct ma_search_service_s {
    ma_service_t            service_base;
    ma_context_t            *context;
    ma_msgbus_server_t      *server;
    ma_logger_t             *logger;
    ma_msgbus_subscriber_t  *subscriber;
    ma_search_t           *search;
    ma_msgbus_t             *msgbus;
};


static ma_error_t search_subscriber_cb(const char *topic, ma_message_t *message, void *cb_data);
static ma_error_t ma_search_service_cb(ma_msgbus_server_t *server, ma_message_t *msg, void *cb_data, ma_msgbus_client_request_t *c_request);

static ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint);
static ma_error_t service_start(ma_service_t *service);
static ma_error_t service_stop(ma_service_t *service);
static void service_release(ma_service_t *service);

static const ma_service_methods_t service_methods = {
    &service_configure,
    &service_start,
    &service_stop,
    &service_release
};

ma_error_t ma_search_service_create(const char *service_name, ma_service_t **service) {
    if(service_name && service) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;

        if((*service = (ma_service_t*)calloc(1, sizeof(ma_search_service_t)))) {
            ma_service_init(*service, &service_methods, service_name, service);
            return MA_OK;
        }
        return err ;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_search_service_release(ma_search_service_t *self) {
    if(self) {
        ma_error_t err = MA_OK;
        if(self->server && (MA_OK != (err = ma_msgbus_server_release(self->server)))) {
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "msgbus server release failed");
        }
        (void)ma_msgbus_subscriber_release(self->subscriber);
        (void)ma_search_release(self->search);
        free(self) ;
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_bool_t check_context_validity(ma_context_t *context){	
    /*Will see if we need this*/
    return MA_TRUE;
}

ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint) {
    if(service && context) {
        ma_error_t rc = MA_OK;
        if(MA_SERVICE_CONFIG_NEW == hint && check_context_validity(context)) {
            ma_configurator_t *configurator = MA_CONTEXT_GET_CONFIGURATOR(context);
            ma_search_service_t *self = (ma_search_service_t *)service;
            ma_msgbus_t *msgbus = MA_CONTEXT_GET_MSGBUS(context);

            self->context = context;
            self->logger = MA_CONTEXT_GET_LOGGER(context);
            self->msgbus = msgbus;

            (void)ma_search_set_logger(self->logger);
            if(MA_OK == (rc = ma_search_create(msgbus, ma_configurator_get_search_datastore(configurator), MA_SEARCH_BASE_PATH_STR, &self->search))) {
                MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "search configured successfully");
                if(MA_OK == (rc = ma_msgbus_subscriber_create(MA_CONTEXT_GET_MSGBUS(context), &self->subscriber))) {
                    (void)ma_msgbus_subscriber_setopt(self->subscriber, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                    (void)ma_msgbus_subscriber_setopt(self->subscriber, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
                    if(MA_OK == (rc = ma_msgbus_server_create(msgbus, MA_SEARCH_SERVICE_NAME_STR, &self->server))) {                        
                        (void)ma_msgbus_server_setopt(self->server, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
                        if(MA_OK == (rc = ma_msgbus_server_setopt(self->server, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC))) {
                            /*TODO - No need to add the search object in the context if we can use the search client API's */
                            ma_context_add_object_info(context, MA_OBJECT_SEARCH_NAME_STR, (void *const *)&self->search);
                            MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_DEBUG, "search service configuration successful");
                            return MA_OK;
                        }
                        ma_msgbus_server_release(self->server);
                    }
                    ma_msgbus_subscriber_release(self->subscriber);
                }
            }
            else
                MA_LOG(self->logger, MA_LOG_SEV_ERROR, "search creation failed, last error(%d)", rc);
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t service_start(ma_service_t *service) {
    if(service) {
        ma_error_t err = MA_OK;
        ma_search_service_t *self = (ma_search_service_t *)service ;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);
        
        if(MA_OK == (err = ma_search_start(self->search))) {
            if(MA_OK == (err = ma_msgbus_server_start(((ma_search_service_t*)service)->server, ma_search_service_cb, service))) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "search service started successfully");
                if(MA_OK == (err = ma_msgbus_subscriber_register(((ma_search_service_t*)service)->subscriber,  "ma.task.*", search_subscriber_cb, service))) {
                    MA_LOG(((ma_search_service_t*)service)->logger, MA_LOG_SEV_DEBUG, "search subscriber registered successfully");
                    return MA_OK ;
                } 
                else 
                    MA_LOG(((ma_search_service_t*)service)->logger, MA_LOG_SEV_ERROR, "search service failed to subscribe topic, errocode(%d)", err);
                ma_msgbus_server_stop(((ma_search_service_t*)service)->server);
            }
            else
                MA_LOG(logger, MA_LOG_SEV_CRITICAL, "search service start failed, last error(%d)", err);
            (void)ma_search_stop(self->search);
        }
        else
            MA_LOG(logger, MA_LOG_SEV_CRITICAL, "search service start failed, last error(%d)", err);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_search_service_get_msgbus(ma_search_service_t *service, ma_msgbus_t **msgbus) {
    if(service && msgbus) {
        *msgbus = service->msgbus;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_search_service_get_version(ma_uint32_t *version) {
    if(version) {
        *version = 0x05020100; //TODO fetch version 
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_search_service_start(ma_search_service_t *service) {
    return service_start((ma_service_t*)service);
}

ma_error_t ma_search_service_stop(ma_search_service_t *service) {
    return service_stop((ma_service_t*)service);
}

ma_error_t service_stop(ma_service_t *service) {
    if(service) {
        ma_error_t err = MA_OK;
        ma_search_service_t *self = (ma_search_service_t *)service;

        if(MA_OK != (err = ma_msgbus_subscriber_unregister(((ma_search_service_t*)service)->subscriber))) {
            MA_LOG(((ma_search_service_t*)service)->logger, MA_LOG_SEV_ERROR, "search service unregistering subscriber failed, last error(%d)", err);
        }

        if(MA_OK == (err = ma_msgbus_server_stop(self->server))) {
            if(MA_OK != (err = ma_search_stop(self->search))) 
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_CRITICAL, "search stop failed, last error(%d)", err);
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO, "search service stopped successfully");
        }
        else
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_CRITICAL, "search service stop failed, last error(%d)", err);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

void service_release(ma_service_t *service) {
    if(service) {
        ma_search_service_release((ma_search_service_t *)service) ;
        return ;
    }
    return ;
}

static ma_error_t search_register_handler(ma_search_service_t *service, ma_message_t *search_req_msg, ma_message_t *search_rsp_msg) {
    if(service && search_req_msg && search_rsp_msg) {
        ma_error_t err = MA_OK;
        const char *search_id = NULL;
        char *search_reg_status = MA_TASK_REPLY_STATUS_FAILED;
        ma_search_info_t *info = NULL;
        ma_variant_t *search_variant = NULL;
        char error_code[MA_MAX_LEN] = {0};


        (void)ma_message_set_property(search_rsp_msg, MA_SEARCH_ID, search_id);
        if(MA_OK == (err = ma_message_get_property(search_req_msg, MA_SEARCH_ID, &search_id))) {
                search_reg_status = MA_SEARCH_ID_NOT_EXIST;
                if(MA_OK == (err = ma_search_get(service->search, search_id, &info))) {
                    search_reg_status = MA_SEARCH_ID_EXIST;
                    MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "search id(%s) already exist!", search_id);
                    (void)ma_search_info_release(info);
                } else {
                   if(MA_OK == ma_message_get_payload(search_req_msg, &search_variant)) {
                       if(MA_OK == (err = ma_search_add_variant(service->search, search_id, search_variant))) {
                           MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "search id(%s) written into DB successfully", search_id);
                           search_reg_status = MA_SEARCH_ID_WRITTEN;
                       }
                       (void)ma_variant_release(search_variant);
                   }
                }
        }
        if(MA_OK != err)
            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_CRITICAL, "search id register failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(search_rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(search_rsp_msg, MA_SEARCH_STATUS, search_reg_status);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t search_deregister_handler(ma_search_service_t *service, ma_message_t *search_req_msg, ma_message_t *search_rsp_msg) {
    if(service && search_req_msg && search_rsp_msg) {
        ma_error_t err = MA_OK;
        const char *search_id = NULL;
        char *search_reg_status = MA_SEARCH_ID_NOT_EXIST;
        ma_search_info_t *info = NULL;
        char error_code[MA_MAX_LEN] = {0};


        (void)ma_message_set_property(search_rsp_msg, MA_SEARCH_ID, search_id);
        if(MA_OK == (err = ma_message_get_property(search_req_msg, MA_SEARCH_ID, &search_id))) {
            if(MA_OK == (err = ma_search_get(service->search, search_id, &info))) {
                search_reg_status = MA_SEARCH_ID_EXIST;
                if(MA_OK == (err = ma_search_delete(service->search, search_id))) {
                    search_reg_status = MA_SEARCH_ID_DELETED;
                    MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "search id(%s) deleted successfully", search_id);
                }
                (void)ma_search_info_release(info);
            }
        }
        if(MA_OK != err)
            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_CRITICAL, "search id deregister failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(search_rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(search_rsp_msg, MA_SEARCH_STATUS, search_reg_status);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t search_get_handler(ma_search_service_t *service, ma_message_t *search_req_msg, ma_message_t *search_rsp_msg) {
    if(service && search_req_msg && search_rsp_msg) {
        ma_error_t err = MA_OK;
        const char *search_id = NULL;
        char *search_reg_status = MA_SEARCH_ID_NOT_EXIST;
        ma_search_info_t *info = NULL;
        ma_variant_t *search_variant = NULL;
        char error_code[MA_MAX_LEN] = {0};


        (void)ma_message_set_property(search_rsp_msg, MA_SEARCH_ID, search_id);
        if(MA_OK == (err = ma_message_get_property(search_req_msg, MA_SEARCH_ID, &search_id))) {
            char *tmp = strdup(search_id);
            char *p = strtok(tmp, " ");
            ma_json_table_t *jtable = NULL;

            if(MA_OK == (err = ma_json_table_create(&jtable))) {
                while(p) {
                    ma_json_array_t *array = NULL;

                    (void)ma_trim_string_by_char(p, ',');
                    (void)ma_trim_string_by_char(p, ' ');
                    if(MA_OK == (err = ma_search_get(service->search, p, &info))) {
                        search_reg_status = MA_SEARCH_ID_EXIST;
                        MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "search id(%s) get successfully", p);
                        if(MA_OK == (err = ma_search_info_convert_to_json(info, &array))) {
                            if(MA_OK == (err = ma_json_table_add_object(jtable, p, array))) {
                                MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "search key(%s) add to json successful", p);
                            } else
                                MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_ERROR, "search key(%s) add to json failed, last error(%d)", p, err);
                            (void)ma_json_array_release(array);
                        }
                        (void)ma_search_info_release(info);
                    }
                    p = strtok(NULL, " ");
                }
                {
                    ma_bytebuffer_t *buffer = NULL;

                    ma_bytebuffer_create(MA_MAX_BUFFER_LEN, &buffer);
                    if(MA_OK == (err = ma_json_table_get_data(jtable, buffer))) {
                        if(MA_OK == (err = ma_variant_create_from_string((const char*)ma_bytebuffer_get_bytes(buffer), ma_bytebuffer_get_size(buffer), &search_variant))) {
                            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "search json output(%s) len(%u)", ma_bytebuffer_get_bytes(buffer), ma_bytebuffer_get_size(buffer));
                            (void)ma_message_set_payload(search_rsp_msg, search_variant);
                            search_reg_status = MA_SEARCH_OP_SUCCESS;
                            (void)ma_variant_release(search_variant);
                        } else
                            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_ERROR, "search variant creation failed, last error(%d)", err);
                        
                    }
                    (void)ma_bytebuffer_release(buffer);
                }
                (void)ma_json_table_release(jtable);
            }
            if(tmp)free(tmp);
        }
        if(MA_OK != err)
            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_CRITICAL, "search id get failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(search_rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(search_rsp_msg, MA_SEARCH_STATUS, search_reg_status);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_search_service_cb(ma_msgbus_server_t *server, ma_message_t *msg, void *cb_data, ma_msgbus_client_request_t *c_request) {
    if(server && msg && cb_data) {
        ma_error_t err = MA_OK;
        const char *msg_type = NULL;
        ma_message_t *response = NULL;
        ma_search_service_t *service = (ma_search_service_t*)cb_data;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(service->context);

        if(MA_OK == (err = ma_message_create(&response))) {
            if(MA_OK == (err = ma_message_get_property(msg, MA_SEARCH_SERVICE_MSG_TYPE, &msg_type))) {
                if(!strcmp(msg_type, MA_SEARCH_SERVICE_MSG_REGISTER_TYPE)) {
                    if(MA_OK == (err = search_register_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client register id request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_SEARCH_SERVICE_MSG_UNREGISTER_TYPE)) {
                    if(MA_OK == (err = search_deregister_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client deregister request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_SEARCH_SERVICE_MSG_GET_TYPE)) {
                    if(MA_OK == (err = search_get_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client deregister request failed, last error(%d)", err);
                        }
                    }
                } else {
                    if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, MA_ERROR_SCHEDULER_INVALID_OP_TYPE, response))) {
                        MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client request failed, last error(%d)", err);
                    }
                }
            } else {
                if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err = MA_ERROR_SCHEDULER_TASK_TYPE_NOT_SET, response))) {
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client request failed, last error(%d)", err);
                }
            }
            if(MA_OK != ma_message_release(response)) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client request message release failed");
            }
        }
        return err;
    }
    return MA_OK;
}

static ma_error_t save_keywords(ma_search_service_t *service, const char *pin_id) {
    if(service && pin_id) {
        ma_context_t *context = service->context;
        ma_error_t err = MA_OK;
        ma_scheduler_t *scheduler = MA_CONTEXT_GET_SCHEDULER(context) ;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(context);
        ma_search_t *search = MA_CONTEXT_GET_SEARCH(context);
        ma_season_info_t *info = NULL;

        if(MA_OK == (err = ma_season_get(MA_CONTEXT_GET_SEASON(context), pin_id, &info))) {
            const char *board = NULL;
            const char *topic = NULL;
            const char *pid = NULL;

            (void)ma_season_info_get_board(info, &board);
            (void)ma_season_info_get_topic(info, &topic);
            (void)ma_season_info_get_id(info, &pid);
            if(board && topic && pid && strcmp(board,MA_EMPTY) && strcmp(topic, MA_EMPTY) && strcmp(pid, MA_EMPTY)) {
                ma_board_pin_t *pin = NULL;

                MA_LOG(logger, MA_LOG_SEV_TRACE, "saving pin(%s) under board(%s) topic(%s)", pid, board, topic);
                if(MA_OK == (err = ma_board_pin_create(&pin))) {
                    ma_search_info_t *o = NULL;
                    char *tmp = strdup(topic);
                    char *p = strtok(tmp, ",");

                    (void)ma_board_pin_set_id(pin, pid);
                    while(p) {
                        (void)ma_trim_string_by_char(p, ',');
                        (void)ma_trim_string_by_char(p, ' ');
                        (void)ma_board_pin_set_topic(pin, p);
                        if(MA_OK == (err = ma_search_get(search, p, &o))) {
                            ma_search_info_t *src = NULL;

                            if(MA_OK == (err = ma_search_info_create(&src))) {
                                (void)ma_search_info_set_id(src, p);
                                (void)ma_search_info_set_pin(src, pid, pin);
                                if(MA_OK == (err = ma_search_info_copy(o, src))) {
                                    if(MA_OK == (err = ma_search_add(MA_CONTEXT_GET_SEARCH(context), o))) {
                                        MA_LOG(logger, MA_LOG_SEV_DEBUG, "search topic(%s) add successful", p);
                                    } else {
                                        MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to set search topic(%s) last error(%d)", p, err);
                                    }
                                } else {
                                    MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed update search pin for topic(%s) last error(%d)", p, err);
                                }
                                (void)ma_search_info_release(src);
                            }
                            (void)ma_search_info_release(o);
                        } else {
                            if(MA_OK == (err = ma_search_info_create(&o))) {
                                (void)ma_search_info_set_id(o, p);
                                (void)ma_search_info_set_pin(o, pid, pin);
                                if(MA_OK == (err = ma_search_add(search, o))) {
                                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "search topic(%s) add successful", p);
                                } else {
                                    MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to set search topic(%s) last error(%d)", p, err);
                                }
                                (void)ma_search_info_release(o);
                            }
                        }
                        p = strtok(NULL, " ");
                    }
                    if(tmp)free(tmp);
                    (void)ma_board_pin_release(pin);
                }
            } else
                MA_LOG(logger, MA_LOG_SEV_ERROR, "board and topic are invalid"); 
            (void)ma_season_info_release(info);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}
ma_error_t search_subscriber_cb(const char *topic, ma_message_t *msg, void *cb_data) {
    if(topic && msg && cb_data) {
        ma_error_t err = MA_OK;
        ma_search_service_t *service = (ma_search_service_t*)cb_data;
        //ma_search_t *search = service->search;

        MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "search subscriber received sensor message(%s)", topic);
        if(!strcmp(topic, MA_TASK_EXECUTE_PUBSUB_TOPIC)) {
            const char *task_type = NULL , *task_owner_id = NULL , *task_id = NULL;

            MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "search service processing (%s) message", topic);
            if( (MA_OK == (err = ma_message_get_property(msg, MA_TASK_TYPE, &task_type))) && task_type &&
                (MA_OK == (err = ma_message_get_property(msg, MA_TASK_PRODUCT_ID, &task_owner_id))) && task_owner_id &&
                (MA_OK == (err = ma_message_get_property(msg, MA_TASK_ID, &task_id))) && task_id) {
                if(!strcmp(task_type, MA_SEASON_TASK_TYPE)) {
                    MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "season task id(%s) processing request received", task_id);
                    err = save_keywords(service, task_id);
                } else
                    MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "task id(%s) is not season task", task_id);
            }
        } else {
            /* do nothing */
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_search_service_get_search(ma_search_service_t *service, ma_search_t **search) {
    if(service && search) {
        *search = service->search;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


