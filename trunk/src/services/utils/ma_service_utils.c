#include "ma/internal/services/ma_service_utils.h"
#include "ma/internal/clients/policy/ma_policy_bag_internal.h"
#include "ma/internal/services/policy/ma_policy_service_internal.h"
#include "ma/internal/clients/policy/ma_policy_uri_internal.h"

#include <stdlib.h>
#include <string.h>


ma_error_t get_agent_policies(const ma_policy_uri_t *agent_uri, ma_db_t *policy_db, ma_policy_bag_t **policy_bag) {
    if(policy_bag) {
        ma_policy_bag_t *bag = NULL;
        ma_error_t rc = MA_OK;
        ma_variant_t *policy_data = NULL;
        ma_table_t *prop_table = NULL;
        size_t count = 0;
        if((MA_OK == (rc = ma_policy_uri_get_table(agent_uri, &prop_table))) && prop_table) {
            if((MA_OK == (rc = ma_policy_service_get_policies(prop_table, policy_db, &count, &policy_data, MA_TRUE))) && policy_data ) {
                if((MA_OK == (rc = ma_policy_bag_create_from_variant(policy_data, &bag))) && bag) {
                    *policy_bag = bag;
                }
                (void) ma_variant_release(policy_data);
            }
            (void) ma_table_release(prop_table);
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}
