#include "ma/internal/services/ma_service_event.h"
#include "ma/internal/defs/ma_general_defs.h"

#include "ma/internal/ma_macros.h"

#include <stdio.h>
#include <string.h>

static ma_error_t add_event_field_s(ma_event_t *event, const char *key, const char *value){
	ma_variant_t *var = NULL;
	ma_error_t rc = MA_OK;
	rc = ma_variant_create_from_string(value, strlen(value), &var);
	if (MA_OK == rc)
		rc = ma_event_add_common_field(event, key, var);			
	ma_variant_release(var);
	return rc;
}

static ma_error_t add_event_bag_ield_s(ma_event_bag_t *event, const char *key, const char *value){
	ma_variant_t *var = NULL;
	ma_error_t rc = MA_OK;
	rc = ma_variant_create_from_string(value, strlen(value), &var);
	if (MA_OK == rc)
		rc = ma_event_bag_add_common_field(event, key, var);			
	ma_variant_release(var);
	return rc;
}

static ma_error_t add_event_field_l(ma_event_t *event, const char *key, ma_int64_t value){
	char value_str[22] = {0};
	MA_MSC_SELECT(_snprintf, snprintf)(value_str, sizeof(value_str), "%d", value);
	return add_event_field_s(event, key, value_str);
}

ma_error_t ma_service_event_generate(ma_client_t *client, struct ma_event_parameter_s *param){

	if(client && param && param->eventid && param->product_version && param->SourceUserName && param->ThreatCategory && param->ThreatName && param->ThreatType){
		ma_error_t rc = MA_OK;
		ma_event_bag_t *event_bag = NULL;
	
		/*Create event bag.*/	
		rc = ma_event_bag_create(client, MA_SOFTWAREID_GENERAL_STR , &event_bag);
			
		if (MA_OK == rc)
			rc = ma_event_bag_set_product_version(event_bag, param->product_version);	
		
		if (MA_OK == rc)
			rc = ma_event_bag_set_product_family(event_bag, "Secure");	
				
		/**Set product common fileds.*/
		if (MA_OK == rc) 
			rc = add_event_bag_ield_s(event_bag, MA_EVENT_COMMON_FILED_KEY_Analyzer, MA_SOFTWAREID_GENERAL_STR);		
		
		if (MA_OK == rc) 
			rc = add_event_bag_ield_s(event_bag, MA_EVENT_COMMON_FILED_KEY_AnalyzerName, MA_MCAFEE_AGENT_SOFTWARE_STR);		

		if (MA_OK == rc) 
			rc = add_event_bag_ield_s(event_bag, MA_EVENT_COMMON_FILED_KEY_AnalyzerVersion, param->product_version);		

		/*Create and send event.*/
		if (MA_OK == rc){
			ma_event_t *event_obj = NULL;
		
			rc = ma_event_create(client, param->eventid, param->severity, &event_obj);
		
			if (MA_OK == rc)
				rc = add_event_field_s(event_obj, MA_EVENT_COMMON_FILED_KEY_SourceUserName, param->SourceUserName);	

			if (MA_OK == rc)
				rc = add_event_field_s(event_obj, MA_EVENT_COMMON_FILED_KEY_ThreatCategory, param->ThreatCategory);		

			if (MA_OK == rc)
				rc = add_event_field_s(event_obj, MA_EVENT_COMMON_FILED_KEY_ThreatType, param->ThreatType);						

			if (MA_OK == rc)
				rc = add_event_field_s(event_obj, MA_EVENT_COMMON_FILED_KEY_ThreatName, param->ThreatName);					
		

			if (MA_OK == rc)
				rc = ma_event_bag_add_event(event_bag, event_obj);		
			(void)ma_event_release(event_obj);
		}

		/*Send event bag.*/
		if (MA_OK == rc) 
			rc = ma_event_bag_send(event_bag);			
		
		(void)ma_event_bag_release(event_bag);
		/*retrun the event bag result.*/
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

