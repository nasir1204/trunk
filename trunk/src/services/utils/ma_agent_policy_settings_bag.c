#include "ma/internal/services/ma_agent_policy_settings_bag.h"


#include "ma/internal/services/ma_service_utils.h"

#include "ma/internal/ma_strdef.h"
#include "ma/internal/clients/policy/ma_policy_bag_internal.h"

#include <stdlib.h>
#include <string.h>

typedef struct ma_agent_policy_settings_bag_s {
    ma_policy_settings_bag_t    base;
    ma_policy_bag_t             *policy_bag;
    ma_policy_uri_t             *policy_uri;
    ma_db_t                     *policy_db;
}ma_agent_policy_settings_bag_t;

static ma_error_t get_bool(ma_policy_settings_bag_t *bag, const char *section, const char *key, ma_bool_t *value);
static ma_error_t get_int(ma_policy_settings_bag_t *bag, const char *section, const char *key, ma_int32_t *value);
static ma_error_t get_str(ma_policy_settings_bag_t *bag, const char *section, const char *key, const char **value);
static ma_error_t get_buffer(ma_policy_settings_bag_t *bag, const char *section, const char *key, ma_buffer_t **value);
static ma_error_t get_variant(ma_policy_settings_bag_t *self, const char *section, const char *key, ma_variant_t **value);;
static void destroy(ma_policy_settings_bag_t *bag); /*< virtual destructor */


static const ma_policy_settings_bag_methods_t methods = {
    &get_bool,
    &get_int,
    &get_str,
    &get_buffer,
    &get_variant,
    &destroy
};

ma_error_t ma_agent_policy_settings_bag_refresh(ma_policy_settings_bag_t *bag) {
    if(bag ) {
        ma_agent_policy_settings_bag_t *self = (ma_agent_policy_settings_bag_t *)bag;

        if(self->policy_bag) ma_policy_bag_release(self->policy_bag), self->policy_bag = NULL;

        return get_agent_policies(self->policy_uri, self->policy_db, &self->policy_bag);
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_agent_policy_settings_bag_create(ma_db_t *policy_db, ma_policy_settings_bag_t **bag) {
    if(bag && policy_db) {
        ma_agent_policy_settings_bag_t *self = (ma_agent_policy_settings_bag_t *)calloc(1, sizeof(ma_agent_policy_settings_bag_t));
        ma_error_t rc = MA_OK ;
        if(!self) return MA_ERROR_OUTOFMEMORY;

        self->policy_db = policy_db;

        if(MA_OK == (rc = ma_policy_uri_create(&self->policy_uri))) {
            ma_policy_uri_set_software_id(self->policy_uri, MA_SOFTWAREID_GENERAL_STR);
            if(MA_OK == (rc = ma_agent_policy_settings_bag_refresh((ma_policy_settings_bag_t*)self))) {
                ma_policy_settings_bag_init((ma_policy_settings_bag_t *)self, &methods, self);
                *bag = (ma_policy_settings_bag_t *)self ;
                return MA_OK;
            }
            ma_policy_uri_release(self->policy_uri);
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_policy_bag_t** ma_agent_policy_settings_bag_get_policy_bag(ma_policy_settings_bag_t *bag) {
    if(bag) {
		ma_agent_policy_settings_bag_t *self = (ma_agent_policy_settings_bag_t *)bag;
		if(self->policy_bag)
			return &(self->policy_bag);
    }
    return NULL ;
}

static void destroy(ma_policy_settings_bag_t *bag) {
    if(bag) {
        ma_agent_policy_settings_bag_t *self = (ma_agent_policy_settings_bag_t *)bag;
        if(self->policy_bag) ma_policy_bag_release(self->policy_bag);
        if(self->policy_uri) ma_policy_uri_release(self->policy_uri);
        free(self);
    }
}

ma_error_t ma_agent_policy_settings_bag_destroy(ma_policy_settings_bag_t *bag) {
	if(bag) {
		destroy(bag);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

static ma_error_t get_variant(ma_policy_settings_bag_t *bag, const char *section, const char *key, ma_variant_t **value) {
    ma_agent_policy_settings_bag_t *self = (ma_agent_policy_settings_bag_t *)bag;

    /* Assume that the section names are unique acroess MA policies. so uri is passing as a NULL */
    return ma_policy_bag_get_value_internal(self->policy_bag, NULL, section, key, value);
}


static ma_error_t get_buffer(ma_policy_settings_bag_t *bag, const char *section, const char *key, ma_buffer_t **value) {
    ma_variant_t *var_value = NULL;
    ma_error_t rc = MA_OK;

    if(MA_OK == (rc = get_variant(bag, section, key, &var_value))) {
        rc = ma_variant_get_string_buffer(var_value, value);
        (void) ma_variant_release(var_value);
    }
    return rc;
}

static ma_error_t get_str(ma_policy_settings_bag_t *bag, const char *section, const char *key, const char **value ) {
    ma_error_t rc = MA_OK;
    ma_buffer_t *dummy_buffer = NULL;
    if(MA_OK == (rc = get_buffer(bag, section, key, &dummy_buffer))) {
        const char *new_value = NULL;
        size_t size = 0;
        if((MA_OK == (rc = ma_buffer_get_string(dummy_buffer, &new_value, &size))) && new_value)
            *value = new_value;
        (void)ma_buffer_release(dummy_buffer);
    }
    return rc;
}

static ma_error_t get_int(ma_policy_settings_bag_t *bag, const char *section, const char *key, ma_int32_t *value) {
    ma_error_t rc = MA_OK;
    ma_buffer_t *buffer = NULL;
    if(MA_OK == (rc = get_buffer(bag, section, key, &buffer))) {
        const char *new_value = NULL;
        size_t size = 0;
        if((MA_OK == (rc = ma_buffer_get_string(buffer, &new_value, &size))) && new_value)
            *value = atoi(new_value);
        (void) ma_buffer_release(buffer);
    }
    return rc;
}

static ma_error_t get_bool(ma_policy_settings_bag_t *bag, const char *section, const char *key, ma_bool_t *value)  {
    ma_error_t rc = MA_OK;
    ma_int32_t dummy_value = 0;

    if(MA_OK == (rc = get_int(bag, section, key, &dummy_value)))
        *value = dummy_value ? MA_TRUE :MA_FALSE;

    return rc;
}


