#include "ma/internal/services/seasons/ma_season_service.h"
#include "ma/internal/utils/context/ma_context.h"
#include "ma/internal/utils/season/ma_season.h"
#include "ma/internal/utils/season/ma_season_info.h"
#include "ma/internal/defs/ma_season_defs.h"
#include "ma/internal/services/scheduler/ma_task_defs.h"
#include "ma/internal/services/scheduler/ma_task_internal.h"
#include "ma/internal/services/ma_service.h"
#include "ma/ma_log.h"
#include "ma/ma_message.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/utils/profiler/ma_profiler.h"
#include "ma/internal/utils/board/ma_board.h"
#include "ma/internal/utils/board/ma_board_info.h"
#include "ma/internal/utils/profiler/ma_profile_info.h"
#include "ma/internal/utils/recorder/ma_recorder_info.h"
#include "ma/internal/utils/recorder/ma_recorder.h"
#include "ma/internal/services/profile/ma_profile_service.h"
#include "ma/internal/services/mail/ma_mail_service.h"
#include "ma/internal/utils/json/ma_json_utils.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/defs/ma_mail_defs.h"
#include "ma/internal/utils/season/ma_season_info.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"
#include "ma/ma_variant.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/sensor/ma_sensor_msg.h"
#include "ma/sensor/ma_sensor_utils.h"
#include "ma/ma_message.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/services/scheduler/ma_scheduler_core.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MA_SEASON_TASK_REPEAT_INTERVAL 30 /* minutes */
#define MA_SEASON_TASK_REPEAT_DURATION 24 /* hours */
#define MA_SEASON_TASK_MAX_RUN_TIME_LIMIT 1 /* minutes */
#define MA_SEASON_TASK_START_DELAY  1 /* start */

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "season service"

const char base_path[] = "/season/";


MA_CPP(extern "C" {)

struct ma_season_service_s {
    ma_service_t            service_base;
    ma_context_t            *context;
    ma_msgbus_server_t      *server;
    ma_logger_t             *logger;
    ma_msgbus_subscriber_t  *subscriber;
    ma_season_t           *season;
    ma_msgbus_t             *msgbus;
};

typedef struct tmp_buf_s {
    ma_season_service_t *service;
    ma_json_array_t *array;
    int length;
    ma_profile_info_t *ptr;
}tmp_buf_t;

static ma_error_t season_subscriber_cb(const char *topic, ma_message_t *message, void *cb_data);
static ma_error_t ma_season_service_cb(ma_msgbus_server_t *server, ma_message_t *msg, void *cb_data, ma_msgbus_client_request_t *c_request);

static ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint);
static ma_error_t service_start(ma_service_t *service);
static ma_error_t service_stop(ma_service_t *service);
static void service_release(ma_service_t *service);

static const ma_service_methods_t service_methods = {
    &service_configure,
    &service_start,
    &service_stop,
    &service_release
};

ma_error_t ma_season_service_create(const char *service_name, ma_service_t **service) {
    if(service_name && service) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;

        if((*service = (ma_service_t*)calloc(1, sizeof(ma_season_service_t)))) {
            ma_service_init(*service, &service_methods, service_name, service);
            return MA_OK;
        }
        return err ;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_service_release(ma_season_service_t *self) {
    if(self) {
        ma_error_t err = MA_OK;
        if(self->server && (MA_OK != (err = ma_msgbus_server_release(self->server)))) {
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "msgbus server release failed");
        }
        (void)ma_msgbus_subscriber_release(self->subscriber);
        (void)ma_season_release(self->season);
        free(self) ;
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_bool_t check_context_validity(ma_context_t *context){	
    /*Will see if we need this*/
    return MA_TRUE;
}

ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint) {
    if(service && context) {
        ma_error_t rc = MA_OK;
        if(MA_SERVICE_CONFIG_NEW == hint && check_context_validity(context)) {
            ma_configurator_t *configurator = MA_CONTEXT_GET_CONFIGURATOR(context);
            ma_season_service_t *self = (ma_season_service_t *)service;
            ma_msgbus_t *msgbus = MA_CONTEXT_GET_MSGBUS(context);

            self->context = context;
            self->logger = MA_CONTEXT_GET_LOGGER(context);
            self->msgbus = msgbus;

            (void)ma_season_set_logger(self->logger);
            if(MA_OK == (rc = ma_season_create(msgbus, ma_configurator_get_season_datastore(configurator), MA_SEASON_BASE_PATH_STR, &self->season))) {
                MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "season configured successfully");
                if(MA_OK == (rc = ma_msgbus_subscriber_create(MA_CONTEXT_GET_MSGBUS(context), &self->subscriber))) {
                    (void)ma_msgbus_subscriber_setopt(self->subscriber, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                    (void)ma_msgbus_subscriber_setopt(self->subscriber, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
                    if(MA_OK == (rc = ma_msgbus_server_create(msgbus, MA_SEASON_SERVICE_NAME_STR, &self->server))) {                        
                        (void)ma_msgbus_server_setopt(self->server, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
                        if(MA_OK == (rc = ma_msgbus_server_setopt(self->server, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC))) {
                            /*TODO - No need to add the season object in the context if we can use the season client API's */
                            ma_context_add_object_info(context, MA_OBJECT_SEASON_NAME_STR, (void *const *)&self->season);
                            MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_DEBUG, "season service configuration successful");
                            return MA_OK;
                        }
                        ma_msgbus_server_release(self->server);
                    }
                    ma_msgbus_subscriber_release(self->subscriber);
                }
            }
            else
                MA_LOG(self->logger, MA_LOG_SEV_ERROR, "season creation failed, last error(%d)", rc);
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t service_start(ma_service_t *service) {
    if(service) {
        ma_error_t err = MA_OK;
        ma_season_service_t *self = (ma_season_service_t *)service ;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);
        
        if(MA_OK == (err = ma_season_start(self->season))) {
            if(MA_OK == (err = ma_msgbus_server_start(((ma_season_service_t*)service)->server, ma_season_service_cb, service))) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "season service started successfully");
                if(MA_OK == (err = ma_msgbus_subscriber_register(((ma_season_service_t*)service)->subscriber,  "ma.task.*", season_subscriber_cb, service))) {
                    MA_LOG(((ma_season_service_t*)service)->logger, MA_LOG_SEV_DEBUG, "season subscriber registered successfully");
                    return MA_OK ;
                } 
                else 
                    MA_LOG(((ma_season_service_t*)service)->logger, MA_LOG_SEV_ERROR, "season service failed to subscribe topic, errocode(%d)", err);
                ma_msgbus_server_stop(((ma_season_service_t*)service)->server);
            }
            else
                MA_LOG(logger, MA_LOG_SEV_CRITICAL, "season service start failed, last error(%d)", err);
            (void)ma_season_stop(self->season);
        }
        else
            MA_LOG(logger, MA_LOG_SEV_CRITICAL, "season service start failed, last error(%d)", err);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_service_get_msgbus(ma_season_service_t *service, ma_msgbus_t **msgbus) {
    if(service && msgbus) {
        *msgbus = service->msgbus;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_service_get_version(ma_uint32_t *version) {
    if(version) {
        *version = 0x05020100; //TODO fetch version 
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_season_service_start(ma_season_service_t *service) {
    return service_start((ma_service_t*)service);
}

ma_error_t ma_season_service_stop(ma_season_service_t *service) {
    return service_stop((ma_service_t*)service);
}

ma_error_t service_stop(ma_service_t *service) {
    if(service) {
        ma_error_t err = MA_OK;
        ma_season_service_t *self = (ma_season_service_t *)service;

        if(MA_OK != (err = ma_msgbus_subscriber_unregister(((ma_season_service_t*)service)->subscriber))) {
            MA_LOG(((ma_season_service_t*)service)->logger, MA_LOG_SEV_ERROR, "season service unregistering subscriber failed, last error(%d)", err);
        }

        if(MA_OK == (err = ma_msgbus_server_stop(self->server))) {
            if(MA_OK != (err = ma_season_stop(self->season))) 
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_CRITICAL, "season stop failed, last error(%d)", err);
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO, "season service stopped successfully");
        }
        else
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_CRITICAL, "season service stop failed, last error(%d)", err);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

void service_release(ma_service_t *service) {
    if(service) {
        ma_season_service_release((ma_season_service_t *)service) ;
        return ;
    }
    return ;
}

static ma_error_t ma_season_service_task_add(ma_season_service_t *self, const char *id, ma_variant_t *payload) {
    if(self && id) {
         ma_error_t err = MA_OK;
         ma_task_t *task = NULL;
         ma_trigger_t *trigger = NULL;
         ma_trigger_run_now_policy_t policy = {1};
         ma_scheduler_t *scheduler = MA_CONTEXT_GET_SCHEDULER(self->context);

         if(MA_OK == (err = ma_task_create(id, &task))) {
             (void)ma_task_set_software_id(task, MA_SOFTWAREID_GENERAL_STR);
             (void)ma_task_set_creator_id(task, MA_SEASON_SERVICE_SECTION_NAME_STR);
             (void)ma_task_set_type(task, MA_SEASON_TASK_TYPE);
             (void)ma_task_set_name(task, id);
             (void)ma_task_set_max_run_time_limit(task, MA_SEASON_TASK_MAX_RUN_TIME_LIMIT);
             if(MA_OK == (err = ma_trigger_create_run_now(&policy, &trigger))) {
                 if(MA_OK == (err = ma_task_add_trigger(task, trigger))) {
                     if(MA_OK == (err = ma_scheduler_add(scheduler, task))) {
                         MA_LOG(self->logger, MA_LOG_SEV_INFO, "Season task id(%s) added successfully into scheduler", id);
                         if(MA_OK == (err = ma_task_start(task))) {
                             MA_LOG(self->logger, MA_LOG_SEV_TRACE, "Season task id(%s) started", id);
                         } else {
                             MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Season task id(%s) failed, last error(%d)", id, err);
                         }
                     } else
                         MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Season task id(%s) added successfully into scheduler failed, last error(%d)", id, err);
                 }
                 (void)ma_trigger_release(trigger);
             }
             (void)ma_task_release(task);
         }
         return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_season_service_task_update(ma_season_service_t *self, const char *id, ma_variant_t *payload) {
    if(self && id) {
         ma_error_t err = MA_OK;
         ma_task_t *task = NULL;
         ma_trigger_t *trigger = NULL;
         ma_scheduler_t *scheduler = MA_CONTEXT_GET_SCHEDULER(self->context);

         if(MA_OK == (err = ma_scheduler_get_task_handle(scheduler, id, &task))) {
             size_t size = 0, i = 0;
             ma_trigger_list_t *tl = NULL;

             (void)ma_task_size(task, &size);
             (void)ma_task_get_trigger_list(task, &tl);
             (void)ma_task_set_app_payload(task, payload);
             for(i = 0; i < size; ++i) {
                 if(MA_OK == (err = ma_trigger_list_at(tl, i, &trigger))) {
                     ma_task_time_t begin_date = {0};

                     advance_current_date_by_seconds(MA_SEASON_TASK_START_DELAY, &begin_date);
                     (void)ma_trigger_set_begin_date(trigger, &begin_date);
                     (void)ma_trigger_release(trigger);
                 }
             }
             (void)ma_task_release(task);
             if(MA_OK == (err = ma_scheduler_modify_task(scheduler, task))) {
                 MA_LOG(self->logger, MA_LOG_SEV_INFO, "Season task id(%s) modified successfully into scheduler", id);
             } else
                 MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Season task id(%s) modified into scheduler failed, last error(%d)", id, err);
         }
         return err;
    }
    return MA_ERROR_INVALIDARG;
}
static ma_error_t ma_season_service_task_update_status(ma_season_service_t *self, const char *task_id, ma_task_exec_status_t status) {
    if(self && task_id) {
        ma_context_t *context = self->context;
        ma_task_t *task = NULL ;
        ma_error_t err = MA_OK ;
        ma_scheduler_t *scheduler = MA_CONTEXT_GET_SCHEDULER(context) ;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(context);

        MA_LOG(logger, MA_LOG_SEV_DEBUG, "Season task id(%s) status update handle begin", task_id);
        if(MA_OK == (err = ma_scheduler_get_task_handle(scheduler, task_id, &task))) {
            if(MA_OK == (err = ma_task_set_execution_status(task, status))) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "Season service task(%s) status set to %d", task_id, status) ;
            }
            (void)ma_task_release(task);
        }
        if(MA_OK != err)
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "Season service task(%s) set status(%d) failed, last error(%d)", task_id, status, err) ;

        MA_LOG(logger, MA_LOG_SEV_DEBUG, "Season task id(%s) status update handle end", task_id);
        return err ;
    }
    return MA_ERROR_INVALIDARG ;
}

static ma_error_t ma_season_service_task_start(ma_season_service_t *service, const char *task_id) {
    if(service && task_id) {
        ma_context_t *context = service->context;
        ma_task_t *task = NULL ;
        ma_error_t err = MA_OK ;
        ma_scheduler_t *scheduler = MA_CONTEXT_GET_SCHEDULER(context) ;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(context);

        if(MA_OK == (err = ma_scheduler_get_task_handle(scheduler, task_id, &task))) {
            if(MA_OK == (err = ma_task_start(task))) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "Season service task(%s) started", task_id) ;
            }
            (void)ma_task_release(task);
        }
        if(MA_OK != err)
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "Season service task(%s) start failed(%d)", task_id, err) ;

        return err ;
    }
    return MA_ERROR_INVALIDARG ;
}

static ma_error_t ma_season_service_task_stop(ma_season_service_t *service, const char *task_id) {
    if(service && task_id) {
        ma_context_t *context = service->context;
        ma_task_t *task = NULL ;
        ma_error_t err = MA_OK ;
        ma_scheduler_t *scheduler = MA_CONTEXT_GET_SCHEDULER(context) ;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(context);

        if(MA_OK == (err = ma_scheduler_get_task_handle(scheduler, task_id, &task))) {
            if(MA_OK == (err = ma_task_stop(task))) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "Season service task(%s) stopped", task_id) ;
            }
            (void)ma_task_release(task);
        }
        if(MA_OK != err)
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "Season service task(%s) stop failed(%d)", task_id, err) ;

        return err ;
    }
    return MA_ERROR_INVALIDARG ;
}

static ma_error_t ma_season_service_task_remove(ma_season_service_t *self, const char *task_id) {
    if(self && task_id) {
        ma_error_t err = MA_OK;
        ma_scheduler_t *scheduler = MA_CONTEXT_GET_SCHEDULER(self->context) ;
        ma_task_t *task = NULL ;

        if(MA_OK == (err = ma_scheduler_get_task_handle(scheduler, task_id, &task))) {
            (void)ma_task_release(task);
            if(MA_OK == (err = ma_scheduler_remove(scheduler, task_id))) {
                MA_LOG(self->logger, MA_LOG_SEV_INFO, "Season task removed from scheduler successfully");
            }
        }
        if(MA_OK != err)
            MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Season service task remove failed(%d)", err) ;

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t add_recorder_entry(ma_season_service_t *service) {
    if(service) {
        ma_error_t err = MA_OK;
        ma_task_time_t today = {0};
        char today_str[MA_MAX_LEN+1]= {0};
        ma_recorder_info_t *info = NULL;

        get_localtime(&today);
        today.hour = today.minute = today.secs = 0;
        MA_MSC_SELECT(_snprintf, snprintf)(today_str, MA_MAX_LEN, "%hu-%hu-%hu", today.month, today.day, today.year);
        if(MA_OK == (err = ma_recorder_info_create(&info))) {
            (void)ma_recorder_info_set_date(info, today_str);
            if(MA_OK == (err = ma_recorder_add(MA_CONTEXT_GET_RECORDER(service->context), info))) {
                MA_LOG(service->logger, MA_LOG_SEV_TRACE, "Added recorder entry(%s)", today_str);
            } else
                MA_LOG(service->logger, MA_LOG_SEV_ERROR, "Recorder entry(%s) add failed, last error(%d)", today_str, err);
            (void)ma_recorder_info_release(info);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t season_register_handler(ma_season_service_t *service, ma_message_t *season_req_msg, ma_message_t *season_rsp_msg) {
    if(service && season_req_msg && season_rsp_msg) {
        ma_error_t err = MA_OK;
        const char *season_id = NULL;
        char *season_reg_status = MA_TASK_REPLY_STATUS_FAILED;
        ma_season_info_t *info = NULL;
        ma_variant_t *season_variant = NULL;
        char error_code[MA_MAX_LEN] = {0};


        if(MA_OK == (err = ma_message_get_property(season_req_msg, MA_SEASON_ID, &season_id))) {
                (void)ma_message_set_property(season_rsp_msg, MA_SEASON_ID, season_id);
                season_reg_status = MA_SEASON_ID_NOT_EXIST;
                if(MA_OK == (err = ma_season_get(service->season, season_id, &info))) {
                    season_reg_status = MA_SEASON_ID_EXIST;
                    MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "season id(%s) already exist!", season_id);
                    (void)ma_season_info_release(info);
                } else {
                   if(MA_OK == ma_message_get_payload(season_req_msg, &season_variant)) {
                       if(MA_OK == (err = ma_season_add_variant(service->season, season_id, season_variant))) {
                           MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "season id(%s) written into DB successfully", season_id);
                           season_reg_status = MA_SEASON_ID_WRITTEN;
                           if(MA_OK !=(err = add_recorder_entry(service))) {
                               MA_LOG(service->logger, MA_LOG_SEV_ERROR, "season id(%s) recording failed, last error(%d)", season_id, err);
                           }
                           if(MA_OK == (err = ma_season_service_task_add(service, season_id, season_variant))) {
                               MA_LOG(service->logger, MA_LOG_SEV_TRACE, "season id(%s) task add successful", season_id);
                           } else
                               MA_LOG(service->logger, MA_LOG_SEV_ERROR, "season id(%s) task add failed, last error(%d)", season_id, err);
                       }
                       (void)ma_variant_release(season_variant);
                   }
                }
        }
        if(MA_OK != err)
            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_CRITICAL, "season id register failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(season_rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(season_rsp_msg, MA_SEASON_STATUS, season_reg_status);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t season_update_handler(ma_season_service_t *service, ma_message_t *season_req_msg, ma_message_t *season_rsp_msg) {
    if(service && season_req_msg && season_rsp_msg) {
        ma_error_t err = MA_OK;
        const char *season_id = NULL;
        char *season_reg_status = MA_TASK_REPLY_STATUS_FAILED;
        ma_variant_t *season_variant = NULL;
        char error_code[MA_MAX_LEN] = {0};


        if(MA_OK == (err = ma_message_get_property(season_req_msg, MA_SEASON_ID, &season_id))) {
            (void)ma_message_set_property(season_rsp_msg, MA_SEASON_ID, season_id);
            season_reg_status = MA_SEASON_ID_NOT_EXIST;
            if(MA_OK == ma_message_get_payload(season_req_msg, &season_variant)) {
                if(MA_OK == (err = ma_season_add_variant(service->season, season_id, season_variant))) {
                    MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "season id(%s) updated into DB successfully", season_id);
                    season_reg_status = MA_SEASON_ID_WRITTEN;
                    if(MA_OK == (err = ma_season_service_task_add(service, season_id, season_variant))) {
                        MA_LOG(service->logger, MA_LOG_SEV_TRACE, "season id(%s) task add successful", season_id);
                    } else
                        MA_LOG(service->logger, MA_LOG_SEV_ERROR, "season id(%s) task add failed, last error(%d)", season_id, err);
                }
                (void)ma_variant_release(season_variant);
            }
        }
        if(MA_OK != err)
            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_CRITICAL, "season id update failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(season_rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(season_rsp_msg, MA_SEASON_STATUS, season_reg_status);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t season_deregister_handler(ma_season_service_t *service, ma_message_t *season_req_msg, ma_message_t *season_rsp_msg) {
    if(service && season_req_msg && season_rsp_msg) {
        ma_error_t err = MA_OK;
        const char *season_id = NULL;
        char *season_reg_status = MA_SEASON_ID_NOT_EXIST;
        ma_season_info_t *info = NULL;
        char error_code[MA_MAX_LEN] = {0};


        (void)ma_message_set_property(season_rsp_msg, MA_SEASON_ID, season_id);
        if(MA_OK == (err = ma_message_get_property(season_req_msg, MA_SEASON_ID, &season_id))) {
            if(MA_OK == (err = ma_season_get(service->season, season_id, &info))) {
                season_reg_status = MA_SEASON_ID_EXIST;
                if(MA_OK == (err = ma_season_delete(service->season, season_id))) {
                    season_reg_status = MA_SEASON_ID_DELETED;
                    MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "season id(%s) deleted successfully", season_id);
                }
                (void)ma_season_info_release(info);
            }
        }
        if(MA_OK != err)
            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_CRITICAL, "season id deregister failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(season_rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(season_rsp_msg, MA_SEASON_STATUS, season_reg_status);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static void foreach_pin(const char *key, ma_variant_t *value, void *cb_data, ma_bool_t *stop_loop) {
    tmp_buf_t *tmp_buf = (tmp_buf_t*)cb_data;
    ma_error_t err = MA_OK;
    ma_recorder_info_t *rec_info = NULL;

    if(MA_OK == (err = ma_recorder_info_convert_from_variant(value, &rec_info))) {
        const char *entry = NULL;
        ma_season_info_t *season_info = NULL;

        (void)ma_recorder_info_get_date(rec_info, &entry);
        if(MA_OK == (err = ma_season_get(MA_CONTEXT_GET_SEASON(tmp_buf->service->context), entry, &season_info))) {
            const char* board = NULL;
            ma_bool_t found = MA_FALSE;
            int i = 0;
    
            (void)ma_season_info_get_board(season_info, &board);
            for(i = 0; i < tmp_buf->length; ++i) {
                const char *pboard = NULL;

                (void)ma_profile_info_board_at(tmp_buf->ptr, i, &pboard);
                if(!strcmp(board, pboard)) {
                    found = MA_TRUE; break;
                }
            }
            if(found == MA_TRUE) {
                ma_json_t *json = NULL;

                if(MA_OK == (err = ma_season_info_convert_to_json(season_info, &json))) {
                    (void)ma_json_array_add_object(tmp_buf->array, json);
                }
            }
            (void)ma_season_info_release(season_info);
        }
        (void)ma_recorder_info_release(rec_info);
    }
}

static ma_error_t season_get_by_day_handler(ma_season_service_t *service, ma_message_t *season_req_msg, ma_message_t *season_rsp_msg) {
    if(service && season_req_msg && season_rsp_msg) {
        ma_error_t err = MA_OK;
        const char *email = NULL;
        char *season_reg_status = MA_SEASON_OP_FAILED;
        ma_variant_t *season_variant = NULL;
        char error_code[MA_MAX_LEN] = {0};
        const char *pday = NULL;
        int which_day = 0;
        ma_task_time_t date = {0};
        char date_str[MA_MAX_LEN+1] = {0};

        season_reg_status = MA_SEASON_OP_SUCCESS;
#if 0
                    {
                        ma_variant_t *t = NULL;

                        if(MA_OK == (err = ma_recorder_get_all(MA_CONTEXT_GET_RECORDER(service->context), "3-3-2018:0:0:0", &t))) {
                            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "processing record operation for date %s", date_str);
                         }
                    }
#endif
        (void)ma_message_set_property(season_rsp_msg, MA_SEASON_ID, email);
        (void)ma_message_get_property(season_req_msg, MA_SEASON_WHICH_DAY, &pday);
        if(pday && strcmp(pday, "")) {
            int k = atoi(pday);
            which_day = k <= 0 ? 0 : k;
        }
        if(which_day) {
            advance_current_date_by_seconds(-(which_day * 86400), &date);
        } else {
            get_localtime(&date);
        }
        date.hour = date.minute = date.secs = 0;
        ma_strtime(date, date_str);
        if(MA_OK == (err = ma_message_get_property(season_req_msg, MA_SEASON_ID, &email))) {
            ma_profile_info_t *pinfo = NULL;

            if(MA_OK == (err = ma_profiler_get(MA_CONTEXT_GET_PROFILER(service->context), email, &pinfo))) {
                size_t no_of_boards = 0, i = 0;

                MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "processing season pin get by date operation");
                (void)ma_profile_info_board_size(pinfo, &no_of_boards);
                ma_variant_t *recorder_var = NULL;

                if(MA_OK == (err = ma_recorder_get_all(MA_CONTEXT_GET_RECORDER(service->context), date_str, &recorder_var))) {
                    ma_table_t *recorder_table = NULL;

                    MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "processing record operation for date %s", date_str);
                    if(MA_OK == (err = ma_variant_get_table(recorder_var, &recorder_table))) {
                        size_t size = 0;

                        (void)ma_table_size(recorder_table, &size);
                        if(size > 0) {
                            ma_json_array_t *mtable = NULL;

                            if(MA_OK == (err = ma_json_array_create(&mtable))) {
                                ma_bytebuffer_t *buffer = NULL;
                                tmp_buf_t tmp_buf;

                                tmp_buf.service = service; tmp_buf.array = mtable; tmp_buf.length = no_of_boards; tmp_buf.ptr = pinfo;
                                (void)ma_table_foreach(recorder_table, foreach_pin, &tmp_buf);
                                ma_bytebuffer_create(MA_MAX_BUFFER_LEN, &buffer);
                                if(MA_OK == (err = ma_json_array_get_data(mtable, buffer))) {
                                    if(MA_OK == (err = ma_variant_create_from_string((const char*)ma_bytebuffer_get_bytes(buffer), ma_bytebuffer_get_size(buffer), &season_variant))) {
                                        MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "board json output(%s) len(%u)", ma_bytebuffer_get_bytes(buffer), ma_bytebuffer_get_size(buffer));
                                        (void)ma_message_set_payload(season_rsp_msg, season_variant);
                                        (void)ma_variant_release(season_variant);
                                    } else
                                        MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_ERROR, "season pin variant creation failed, last error(%d)", err);
                    
                                }
                                (void)ma_bytebuffer_release(buffer);
                                (void)ma_json_array_release(mtable);
                            }
                        }
                        (void)ma_table_release(recorder_table);
                    }
                    (void)ma_variant_release(recorder_var);
                } 
                (void)ma_profile_info_release(pinfo);
            }
        }
        if(MA_OK != err)
            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_CRITICAL, "season id get failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(season_rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(season_rsp_msg, MA_SEASON_STATUS, season_reg_status);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t season_get_handler(ma_season_service_t *service, ma_message_t *season_req_msg, ma_message_t *season_rsp_msg) {
    if(service && season_req_msg && season_rsp_msg) {
        ma_error_t err = MA_OK;
        const char *email = NULL;
        char *season_reg_status = MA_SEASON_OP_FAILED;
        ma_variant_t *season_variant = NULL;
        char error_code[MA_MAX_LEN] = {0};


        (void)ma_message_set_property(season_rsp_msg, MA_SEASON_ID, email);
        if(MA_OK == (err = ma_message_get_property(season_req_msg, MA_SEASON_ID, &email))) {
            ma_profile_info_t *pinfo = NULL;

            if(MA_OK == (err = ma_profiler_get(MA_CONTEXT_GET_PROFILER(service->context), email, &pinfo))) {
                size_t no_of_boards = 0, i = 0;
                ma_board_t *board = MA_CONTEXT_GET_BOARD(service->context);
                ma_json_multi_table_t *mtable = NULL;

                MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "processing season pin get operation");
                (void)ma_profile_info_board_size(pinfo, &no_of_boards);
                if(MA_OK == (err = ma_json_multi_table_create(&mtable))) {
                    for(i = 0; i < no_of_boards; ++i) {
                        const char *boards = NULL;

                        if(MA_OK == (err = ma_profile_info_board_at(pinfo, i, &boards))) {
                            ma_board_info_t *info = NULL;

                            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "processing season pins in board(%s) ", boards);
                            if(MA_OK == (err = ma_board_get(board, boards, &info))) {
                                ma_json_table_t *table = NULL;

                                if(MA_OK == (err = ma_board_info_convert_to_json(info, &table))) {
                                    (void)ma_json_multi_table_add_object(mtable, boards, table);
                                    (void)ma_json_table_release(table);
                                }
                                (void)ma_board_info_release(info);
                            }
                        }
                    }
                    ma_bytebuffer_t *buffer = NULL;

                    ma_bytebuffer_create(MA_MAX_BUFFER_LEN, &buffer);
                    if(MA_OK == (err = ma_json_multi_table_get_data(mtable, buffer))) {
                        if(MA_OK == (err = ma_variant_create_from_string((const char*)ma_bytebuffer_get_bytes(buffer), ma_bytebuffer_get_size(buffer), &season_variant))) {
                            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "board json output(%s) len(%u)", ma_bytebuffer_get_bytes(buffer), ma_bytebuffer_get_size(buffer));
                            (void)ma_message_set_payload(season_rsp_msg, season_variant);
                            season_reg_status = MA_SEASON_OP_SUCCESS;
                            (void)ma_variant_release(season_variant);
                        } else
                            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_ERROR, "season pin variant creation failed, last error(%d)", err);
                        
                    }
                    (void)ma_bytebuffer_release(buffer);
                    (void)ma_json_multi_table_release(mtable);
                }
                (void)ma_profile_info_release(pinfo);
            }
        }
        if(MA_OK != err)
            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_CRITICAL, "season id get failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(season_rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(season_rsp_msg, MA_SEASON_STATUS, season_reg_status);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_service_cb(ma_msgbus_server_t *server, ma_message_t *msg, void *cb_data, ma_msgbus_client_request_t *c_request) {
    if(server && msg && cb_data) {
        ma_error_t err = MA_OK;
        const char *msg_type = NULL;
        ma_message_t *response = NULL;
        ma_season_service_t *service = (ma_season_service_t*)cb_data;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(service->context);

        if(MA_OK == (err = ma_message_create(&response))) {
            if(MA_OK == (err = ma_message_get_property(msg, MA_SEASON_SERVICE_MSG_TYPE, &msg_type))) {
                if(!strcmp(msg_type, MA_SEASON_SERVICE_MSG_REGISTER_TYPE)) {
                    if(MA_OK == (err = season_register_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client register id request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_SEASON_SERVICE_MSG_UNREGISTER_TYPE)) {
                    if(MA_OK == (err = season_deregister_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client deregister request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_SEASON_SERVICE_MSG_UPDATE_TYPE)) {
                    if(MA_OK == (err = season_update_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client update request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_SEASON_SERVICE_MSG_GET_TYPE)) {
                    if(MA_OK == (err = season_get_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client deregister request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_SEASON_SERVICE_MSG_GET_BY_DAY_TYPE)) {
                    if(MA_OK == (err = season_get_by_day_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client deregister request failed, last error(%d)", err);
                        }
                    }
                } else {
                    if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, MA_ERROR_SCHEDULER_INVALID_OP_TYPE, response))) {
                        MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client request failed, last error(%d)", err);
                    }
                }
            } else {
                if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err = MA_ERROR_SCHEDULER_TASK_TYPE_NOT_SET, response))) {
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client request failed, last error(%d)", err);
                }
            }
            if(MA_OK != ma_message_release(response)) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client request message release failed");
            }
        }
        return err;
    }
    return MA_OK;
}

ma_error_t season_subscriber_cb(const char *topic, ma_message_t *msg, void *cb_data) {
    if(topic && msg && cb_data) {
        ma_error_t err = MA_OK;
        ma_season_service_t *service = (ma_season_service_t*)cb_data;
        //ma_season_t *season = service->season;

        MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "season subscriber received task message(%s)", topic);
        if(!strcmp(topic, MA_TASK_EXECUTE_PUBSUB_TOPIC)) {
            const char *task_type = NULL , *task_owner_id = NULL , *task_id = NULL;

            MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "season processing (%s) message", topic);
            if( (MA_OK == (err = ma_message_get_property(msg, MA_TASK_TYPE, &task_type))) && task_type &&
                (MA_OK == (err = ma_message_get_property(msg, MA_TASK_PRODUCT_ID, &task_owner_id))) && task_owner_id &&
                (MA_OK == (err = ma_message_get_property(msg, MA_TASK_ID, &task_id))) && task_id) {
                if(!strcmp(task_type, MA_SEASON_TASK_TYPE)) {
                    MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "season task id(%s) processing request received", task_id);
                } else
                    MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "task id(%s) is not season task", task_id);
            }
        } else if(!strcmp(topic, MA_TASK_UPDATE_STATUS_PUBSUB_TOPIC)) {
            const char *task_type = NULL , *task_owner_id = NULL , *task_id = NULL;

            MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "season processing (%s) message", topic);
            if( (MA_OK == (err = ma_message_get_property(msg, MA_TASK_TYPE, &task_type))) && task_type &&
                (MA_OK == (err = ma_message_get_property(msg, MA_TASK_PRODUCT_ID, &task_owner_id))) && task_owner_id &&
                (MA_OK == (err = ma_message_get_property(msg, MA_TASK_ID, &task_id))) && task_id) {
                if(!strcmp(task_type, MA_SEASON_TASK_TYPE)) {
                    MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "season task id(%s) processing update request ", task_id);
                    //err = ma_season_service_task_update_status(service, task_id, MA_TASK_EXEC_SUCCESS);
                } else
                    MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "task id(%s) is not season task", task_id);
            }
        } else if(!strcmp(topic, MA_TASK_STOP_PUBSUB_TOPIC)) {
            const char *task_type = NULL , *task_owner_id = NULL , *task_id = NULL;

            MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "season processing (%s) message", topic);
            if( (MA_OK == (err = ma_message_get_property(msg, MA_TASK_TYPE, &task_type))) && task_type &&
                (MA_OK == (err = ma_message_get_property(msg, MA_TASK_PRODUCT_ID, &task_owner_id))) && task_owner_id &&
                (MA_OK == (err = ma_message_get_property(msg, MA_TASK_ID, &task_id))) && task_id) {
                if(!strcmp(task_type, MA_SEASON_TASK_TYPE)) {
                    MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "season task id(%s) processing stop request ", task_id);
                } else
                    MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "task id(%s) is not season task", task_id);
            }
        } else if(!strcmp(topic, MA_TASK_REMOVE_PUBSUB_TOPIC)) {
            const char *task_type = NULL , *task_owner_id = NULL , *task_id = NULL;

            MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "season processing (%s) message", topic);
            if( (MA_OK == (err = ma_message_get_property(msg, MA_TASK_TYPE, &task_type))) && task_type &&
                (MA_OK == (err = ma_message_get_property(msg, MA_TASK_PRODUCT_ID, &task_owner_id))) && task_owner_id &&
                (MA_OK == (err = ma_message_get_property(msg, MA_TASK_ID, &task_id))) && task_id) {
                if(!strcmp(task_type, MA_SEASON_TASK_TYPE)) {
                    MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "season task id(%s) processing remove request ", task_id);
                } else
                    MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "task id(%s) is not season task", task_id);
            }
        } else {
            /* do nothing */
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_season_service_get_season(ma_season_service_t *service, ma_season_t **season) {
    if(service && season) {
        *season = service->season;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


MA_CPP(})
