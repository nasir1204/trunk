#include "ma_p2p_service_internal.h"

#include "ma/internal/services/p2p/ma_p2p_db_manager.h"

#include "ma/ma_message.h"
#include "ma/internal/defs/ma_p2p_defs.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/defs/ma_common_services_defs.h"

#include "ma/internal/clients/p2p/ma_p2p_content_request_internal.h"
#include "ma/internal/services/p2p/ma_p2p_content.h"
#include "ma/internal/defs/ma_scheduler_defs.h"
#include "ma/scheduler/ma_scheduler.h"

#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/utils/time/ma_time.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal/utils/filesystem/ma_fs_utils.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h" 
#include "ma/internal/core/msgbus/ma_msgbus_private.h"

#include "ma/internal/utils/xml/ma_xml.h"
#include "ma/internal/utils/xml/ma_xml_node.h"

#include <stdlib.h>
#include <string.h>

#define MA_BOOTSTRAP_P2P_CONTENT_BASE_DIR 	  "InstallerFiles"	
#define SMART_INSTALLER_HASHES_XML  "hashes.xml"

static ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint) ;
static ma_error_t service_start(ma_service_t *service) ;
static ma_error_t service_stop(ma_service_t *service) ;
static void service_destroy(ma_service_t *service) ;

static const ma_service_methods_t methods = {
    &service_configure,
    &service_start,
    &service_stop,
    &service_destroy
} ;


ma_error_t ma_p2p_service_create(const char *service_name, ma_service_t **service) {
    if(service) {
        ma_p2p_service_t *self = (ma_p2p_service_t *)calloc(1, sizeof(ma_p2p_service_t)) ;

        if(!self)   return MA_ERROR_OUTOFMEMORY ;

        ma_service_init((ma_service_t *)self, &methods, service_name, self) ;

        *service = (ma_service_t *)self ;

        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

static ma_error_t ma_p2p_service_deinit(ma_p2p_service_t *self) ;
static ma_error_t ma_p2p_service_configure(ma_p2p_service_t *self, ma_context_t *context, unsigned hint) ;
static ma_error_t ma_p2p_service_start(ma_p2p_service_t *self) ;
static ma_error_t ma_p2p_service_stop(ma_p2p_service_t *self) ;
static void ma_p2p_service_destroy(ma_p2p_service_t *self) ;
static int add_installer_files_to_p2p_repo(ma_p2p_service_t *self);

ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint) {
    ma_p2p_service_t *self = (ma_p2p_service_t *)service;
    return  (self && context)? ma_p2p_service_configure(self, context, hint) : MA_ERROR_INVALIDARG ;
}

ma_error_t service_start(ma_service_t *service) {
    ma_p2p_service_t *self = (ma_p2p_service_t *)service ;
    return (self) ? ma_p2p_service_start(self) : MA_ERROR_INVALIDARG ;
}

ma_error_t service_stop(ma_service_t *service) {
    ma_p2p_service_t *self = (ma_p2p_service_t *)service ;
    return (self) ? ma_p2p_service_stop(self) : MA_ERROR_INVALIDARG ;
}

void service_destroy(ma_service_t *service) {
    ma_p2p_service_t *self = (ma_p2p_service_t *)service ;
    if(self)   ma_p2p_service_destroy(self) ;
    return ;
}

ma_error_t ma_p2p_service_deinit(ma_p2p_service_t *self) {
    if(self) {
        if(self->msgbus_server) ma_msgbus_server_release(self->msgbus_server) ;
        self->msgbus_server = NULL ;

        if(self->p2p_policy)    (void)ma_p2p_policy_release(self->p2p_policy) ;
        self->p2p_policy = NULL ;

        if(self->p2p_content_manger)    (void)ma_p2p_content_manager_release(self->p2p_content_manger) ;
        self->p2p_content_manger = NULL ;

        self->context = NULL ;

        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_p2p_service_configure(ma_p2p_service_t *self, ma_context_t *context, unsigned hint) {
    ma_error_t rc = MA_OK ;
    ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(context) ;

    self->context = context ;

    if(MA_SERVICE_CONFIG_NEW == hint) {	
        if(MA_OK == (rc = ma_p2p_db_create_schema(ma_configurator_get_msgbus_database(MA_CONTEXT_GET_CONFIGURATOR(context))))) {
			(void)ma_p2p_db_reset_state(ma_configurator_get_msgbus_database(MA_CONTEXT_GET_CONFIGURATOR(context))) ;
            if(MA_OK == (rc = (ma_p2p_policy_create(self, &self->p2p_policy)))) {
                if(MA_OK == (rc = (ma_p2p_content_manager_create(self, &self->p2p_content_manger)))) {
					if(MA_OK == (rc = ma_msgbus_server_create(MA_CONTEXT_GET_MSGBUS(self->context), MA_P2P_SERVICE_NAME_STR, &self->msgbus_server))) {
						(void)ma_msgbus_server_setopt(self->msgbus_server, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO) ;
						(void)ma_msgbus_server_setopt(self->msgbus_server, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC) ; 
						
						MA_LOG(logger, MA_LOG_SEV_INFO, "Successfully configured P2P service.") ;
					}
                }
            }
        }
        if(MA_OK != rc) {
            MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to configure the P2P service(%d).", rc) ;
			ma_p2p_service_deinit(self) ;				
		}
    }

    if(MA_SERVICE_CONFIG_MODIFIED == hint) {
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO, "Reconfiguring the P2P service.") ;
        if(MA_OK == (rc = ma_p2p_policy_update(self->p2p_policy))) {
			if(MA_OK == (rc = ma_p2p_content_manager_update(self->p2p_content_manger))) {
				const char *data_path = ma_configurator_get_agent_data_path(MA_CONTEXT_GET_CONFIGURATOR(self->context)) ;
				char bootstrap_hashfile[MAX_PATH_LEN] = {0} ;
				uv_loop_t *loop = ma_event_loop_get_uv_loop(ma_msgbus_get_event_loop(MA_CONTEXT_GET_MSGBUS(self->context))) ;

				MA_MSC_SELECT(_snprintf, snprintf)(bootstrap_hashfile, MAX_PATH_LEN, "%s%c%s%c%s%c%s", data_path, MA_PATH_SEPARATOR, MA_AGENT_DATA_DIR_STR, MA_PATH_SEPARATOR, MA_BOOTSTRAP_P2P_CONTENT_BASE_DIR, MA_PATH_SEPARATOR, SMART_INSTALLER_HASHES_XML ) ;

				if(self->p2p_policy && self->p2p_policy->enable_serving) {
                    MA_LOG(logger, MA_LOG_SEV_TRACE, "Check for McAfeeSmartInstall files and adding to p2p repo.") ;

                    if(ma_fs_is_file_exists(loop, bootstrap_hashfile)) {
    					(void)add_installer_files_to_p2p_repo(self);
                    }
                    else {
                        uv_err_t uv_err = uv_last_error(loop) ;
                        MA_LOG(logger, MA_LOG_SEV_TRACE, "Installer files doesn't exists (\'%s\' %s)", uv_strerror(uv_err),  uv_err_name(uv_err)) ;
                    }
				}
				return MA_OK ;
			}
        }
		MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to reconfigure P2P service(%d).", rc) ;
    }

    return rc ;
}

static ma_error_t ma_p2p_service_handler(ma_msgbus_server_t *server, ma_message_t *request, void *cb_data, ma_msgbus_client_request_t *c_request) ;

ma_error_t ma_p2p_service_start(ma_p2p_service_t *self) {
    ma_error_t rc = MA_OK ;
	ma_logger_t *logger =  MA_CONTEXT_GET_LOGGER(self->context) ;

    if(MA_OK == (rc = ma_msgbus_server_start(self->msgbus_server, ma_p2p_service_handler, (void *)self))) {
			MA_LOG(logger, MA_LOG_SEV_INFO, "P2P service started.") ;
	}
	else
		MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to start msgbus server, rc = %d.", rc) ;

	return rc ;
}

ma_error_t ma_p2p_service_stop(ma_p2p_service_t *self) {
	if(self) {
		(void)ma_msgbus_server_stop(self->msgbus_server) ;
		return MA_OK ;
	}
	return MA_ERROR_INVALIDARG ;
}

void ma_p2p_service_destroy(ma_p2p_service_t *self) {
    ma_p2p_service_deinit(self) ;
	free(self) ;

    return ;
}

static ma_error_t p2p_content_handle(ma_p2p_service_t *service, ma_bool_t add_remove, ma_variant_t *payload) ;

ma_error_t ma_p2p_service_handler(ma_msgbus_server_t *server, ma_message_t *request, void *cb_data, ma_msgbus_client_request_t *c_request) {
    if(server && request && c_request) {
        ma_error_t rc = MA_OK ;
        char *msg_type = NULL ;
        ma_variant_t *req_payload = NULL ;
        ma_message_t *response = NULL ;

        ma_p2p_service_t *service = (ma_p2p_service_t *)cb_data ;
		ma_db_t *db_handle = ma_configurator_get_msgbus_database(MA_CONTEXT_GET_CONFIGURATOR(service->context)) ;
		
		ma_bool_t purge_required = MA_FALSE ;

        if(MA_OK != (rc = ma_message_create(&response))) return rc;

        if(rc == ma_message_get_property(request, MA_P2P_MSG_KEY_TYPE_STR, &msg_type) && msg_type) {
            if(0 == strcmp(MA_P2P_MSG_PROP_VAL_RQST_ADD_CONTENT_STR, msg_type)) {
				ma_int64_t aggregated_size = 0 ;

                ma_message_get_payload(request, &req_payload) ;
                rc = p2p_content_handle(service, MA_TRUE, req_payload) ;
                (void)ma_message_set_property(response, MA_P2P_MSG_KEY_TYPE_STR, MA_P2P_MSG_PROP_VAL_RPLY_ADD_CONTENT_STR) ;
                (void)ma_variant_release(req_payload) ; req_payload = NULL ;
				
				(void)ma_p2p_db_get_aggregated_size(db_handle, &aggregated_size) ;
				if(aggregated_size > (service->p2p_policy->disk_quota * 1024 *1024) )	purge_required = MA_TRUE ;
            }
            else if(0 == strcmp(MA_P2P_MSG_PROP_VAL_RQST_REMOVE_CONTENT_STR, msg_type)) {
                (void)ma_message_get_payload(request, &req_payload) ;
                rc = p2p_content_handle(service, MA_FALSE, req_payload) ;
                (void)ma_message_set_property(response, MA_P2P_MSG_KEY_TYPE_STR, MA_P2P_MSG_PROP_VAL_RPLY_REMOVE_CONTENT_STR) ;
                (void)ma_variant_release(req_payload) ; req_payload = NULL ;
            }
			else if(0 == strcmp(MA_P2P_MSG_PROP_VAL_RQST_GET_CONTENT_DIR_STR, msg_type)) {
				(void)ma_message_set_property(response, MA_P2P_MSG_KEY_TYPE_STR, MA_P2P_MSG_PROP_VAL_RPLY_GET_CONTENT_DIR_STR) ;
				(void)ma_message_set_property(response, MA_P2P_MSG_KEY_CONTENT_DIR_STR, service->p2p_content_manger->p2p_content_dir) ;
			}
			else if(0 == strcmp(MA_P2P_MSG_PROP_VAL_RQST_CHECK_CONTENT_EXISTS_STR, msg_type)) {
				ma_bool_t is_exists = MA_FALSE ;
				char *hash = NULL ;
				
				(void)ma_message_get_property(request, MA_P2P_MSG_KEY_CONTENT_HASH_STR, &hash) ;
				
				if(hash) {
					(void)ma_p2p_db_is_content_exists(db_handle, hash, &is_exists) ;
				}

				(void)ma_message_set_property(response, MA_P2P_MSG_KEY_TYPE_STR, MA_P2P_MSG_PROP_VAL_RPLY_CHECK_CONTENT_EXISTS_STR) ;
				(void)ma_message_set_property(response, MA_P2P_MSG_KEY_CONTENT_EXISTS_STR, ((MA_TRUE == is_exists)? "1" : "0" )) ;
			}
			else if(0 == strcmp(MA_P2P_MSG_PROP_VAL_RQST_RUN_PURGE_TASK_STR, msg_type)) {
				rc = ma_p2p_content_manager_purge_content(service->p2p_content_manger, MA_TRUE) ;
				(void)ma_message_set_property(response, MA_P2P_MSG_KEY_TYPE_STR, MA_P2P_MSG_PROP_VAL_RPLY_RUN_PURGE_TASK_STR) ;
			}
            else {
                MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_ERROR, "Invalid message type(%s).", msg_type) ;
                rc = MA_ERROR_UNEXPECTED ;
            }
        }
        else {
            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_ERROR, "Invalid message type.") ;
            rc = MA_ERROR_UNEXPECTED ;
        }

        rc = ma_msgbus_server_client_request_post_result(c_request, rc, response) ;
        (void)ma_message_release(response) ; response = NULL ;

		if(purge_required)
			rc = ma_p2p_content_manager_purge_content(service->p2p_content_manger, MA_FALSE) ;

        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}


static char *get_string_from_variant(ma_variant_t *var) {
	if(var) {
		ma_buffer_t *buffer = NULL ;
		char *str_value = NULL ; size_t str_len = 0 ;

		(void)ma_variant_get_string_buffer(var, &buffer) ;
		(void)ma_buffer_get_string(buffer, &str_value, &str_len) ;
		(void)ma_buffer_release(buffer) ; buffer = NULL;
		return str_value ;
	}
	return NULL ;
}

static ma_error_t populate_content_object(ma_p2p_service_t *service, ma_variant_t *value, ma_p2p_content_t *p2p_content) {
    ma_error_t rc = MA_OK ;
	ma_array_t *arr_content = NULL ;
	ma_variant_t *data = NULL ;
	uv_loop_t *loop = ma_event_loop_get_uv_loop(ma_msgbus_get_event_loop(MA_CONTEXT_GET_MSGBUS(service->context))) ;

	(void)ma_variant_get_array(value, &arr_content) ;

	/* set content format and buffer */
	{
		ma_int32_t type = MA_P2P_CONTENT_FORMAT_FILE ;
		ma_buffer_t *buffer = NULL ;

		/*get type.*/
		(void)ma_array_get_element_at(arr_content, 5, &data) ;
		(void)ma_variant_get_int32(data, &type);
		(void)ma_variant_release(data) ; data = NULL ;

		/*get data.*/
		(void)ma_array_get_element_at(arr_content, 1, &data) ;

		if(MA_P2P_CONTENT_FORMAT_FILE == type) {
			(void)ma_p2p_content_set_content_format(p2p_content, MA_P2P_CONTENT_FORMAT_FILE) ;
			(void)ma_variant_get_string_buffer(data, &buffer) ;
		}
		else {
			(void)ma_p2p_content_set_content_format(p2p_content, MA_P2P_CONTENT_FORMAT_BUFFER) ;
			(void)ma_variant_get_raw_buffer(data, &buffer) ;
		}
		(void)ma_p2p_content_set_buffer(p2p_content, buffer) ;

		(void)ma_buffer_release(buffer) ; buffer = NULL ;
		(void)ma_variant_release(data) ; data = NULL ;
	}
	
	/* set urn */
	{
		(void)ma_array_get_element_at(arr_content, 2, &data) ;
		ma_p2p_content_set_urn(p2p_content, get_string_from_variant(data)) ;
		(void)ma_variant_release(data) ; data = NULL ;
	}

	/* set base dir */
	if(p2p_content->owned) {
		(void)ma_p2p_content_set_base_dir(p2p_content, service->p2p_content_manger->p2p_content_dir) ;
	}
	else {
		size_t str_size = 0 ;
		char *str_value = NULL ;

		(void)ma_buffer_get_string(p2p_content->buffer, &str_value, &str_size) ;
		(void)ma_p2p_content_set_base_dir(p2p_content, str_value) ;
	}

	/* set size */
	{
		size_t buffer_size = 0 ;
		char *str_value = NULL ;

		if(p2p_content->content_format == MA_P2P_CONTENT_FORMAT_FILE) {
			char file_name[MA_MAX_PATH_LEN] = {0} ;

			if(p2p_content->file_op == MA_P2P_CONTENT_END) {
				MA_MSC_SELECT(_snprintf, snprintf)(file_name, MA_MAX_PATH_LEN, "%s%c%s", service->p2p_content_manger->p2p_content_dir, MA_PATH_SEPARATOR, p2p_content->urn) ;
			}
			else {
				(void)ma_buffer_get_string(p2p_content->buffer, &str_value, &buffer_size) ;
				MA_MSC_SELECT(_snprintf, snprintf)(file_name, MA_MAX_PATH_LEN, "%s%c%s", str_value, MA_PATH_SEPARATOR, p2p_content->urn) ;
			}
			buffer_size = ma_fs_get_file_size(loop, file_name) ;
		}
		else {
			(void)ma_buffer_size(p2p_content->buffer, &buffer_size) ;
		}
		
		(void)ma_p2p_content_set_content_size(p2p_content, buffer_size) ;
	}

	/* set content type */
	{
		(void)ma_array_get_element_at(arr_content, 3, &data) ;
		(void)ma_p2p_content_set_content_type(p2p_content, get_string_from_variant(data)) ;
		(void)ma_variant_release(data) ; data = NULL ;
	}

	/* set created and last access time */
    {
        char time_created[21] ;
        ma_time_t cur_time = {0, 0, 0, 0, 0, 0, 0 } ;
        
        ma_time_get_localtime(&cur_time) ;
        MA_MSC_SELECT(_snprintf, snprintf)(time_created, 21, "%04d:%02d:%02d::%02d:%02d:%02d", cur_time.year, cur_time.month, cur_time.day, cur_time.hour, cur_time.minute, cur_time.second) ;
        (void)ma_p2p_content_set_time_created(p2p_content, time_created) ;
		(void)ma_p2p_content_set_time_last_access(p2p_content, time_created) ;
    }

	/* set hitcount */
	(void)ma_p2p_content_set_hit_count(p2p_content, 0) ;

    return rc ;
}

ma_error_t p2p_content_handle(ma_p2p_service_t *service, ma_bool_t add_remove, ma_variant_t *payload) {
	if(service && payload) {
		ma_error_t rc = MA_OK ;
		ma_p2p_content_request_t *request = NULL ;

		if(MA_OK == (rc = ma_p2p_content_request_from_variant(payload, &request))) {
			ma_array_t *hash_list = NULL ;
			
			if(MA_OK == (rc = ma_table_get_keys(request->content_table, &hash_list))) {
				size_t num_hash = 0 ;
				size_t iter = 0 ;

				(void)ma_array_size(hash_list, &num_hash) ;
				if(num_hash)
				{
					MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_INFO, "%d content found in the request.", num_hash) ;
					for(iter = 1 ; iter <= num_hash ; iter++) {
						ma_p2p_content_t *p2p_content = NULL ;

						if(MA_OK == (rc = ma_p2p_content_create(&p2p_content))) {
							ma_variant_t *var_key = NULL ;
							char *hash = NULL ;

							(void)ma_array_get_element_at(hash_list, iter, &var_key) ;
							hash = get_string_from_variant(var_key) ;

							if(hash) 
							{
								(void)ma_p2p_content_set_hash(p2p_content, hash) ;
								(void)ma_p2p_content_set_contributor(p2p_content, request->contributor) ;
								(void)ma_p2p_content_set_owned(p2p_content, request->format?MA_TRUE:(request->file_op?MA_TRUE:MA_FALSE)) ;
								(void)ma_p2p_content_set_file_op(p2p_content, (ma_p2p_content_add_type_t)request->file_op) ;

								if(add_remove) {/* add */
									ma_variant_t *content_data = NULL ;

									(void)ma_table_get_value(request->content_table, hash, &content_data) ;
									(void)populate_content_object(service, content_data, p2p_content) ;
									
									MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "Add request(hash = %s, urn = %s).",  p2p_content->hash, p2p_content->urn) ;

									if(MA_OK != (rc = ma_p2p_content_manager_add_content(service->p2p_content_manger, request->file_op, p2p_content)))
										MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_ERROR, "Add p2p content  with hash(%s) failed(%d).", hash, rc) ;

									(void)ma_variant_release(content_data) ; content_data = NULL ;
								}
								else {	/* remove */
									if(MA_OK != (rc = ma_p2p_content_manager_remove_content(service->p2p_content_manger, p2p_content)))
										MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_ERROR, "Remove p2p content with hash(%s) failed(%d).", hash, rc)  ;
								}
							}	/* if hash */
							(void)ma_variant_release(var_key) ; var_key = NULL ;
							(void)ma_p2p_content_release(p2p_content) ; p2p_content = NULL ;
						}
						else {
							MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_ERROR, "p2p content object create failed.") ;
						}
					}
				}
				else {
					MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_INFO, "No content found in the request.") ;
				}
				(void)ma_array_release(hash_list) ; hash_list = NULL ;
			}
			(void)ma_p2p_content_request_release(request) ; request = NULL ;
		}
		else {
			MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_ERROR, "Failed to create request object from payload.") ;
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}


static int add_installer_files_to_p2p_repo(ma_p2p_service_t *self)
{
    ma_error_t rc = MA_ERROR_PRECONDITION, rc1 = MA_OK ;
	ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context) ;

	char hashes_xml_path[MAX_PATH_LEN] = {0};
	char *p2p_base_dir = self->p2p_content_manger->p2p_content_dir ;
	char bootstrap_p2p_dir[MAX_PATH_LEN] = {0};
	uv_loop_t *loop = ma_event_loop_get_uv_loop(ma_msgbus_get_event_loop(MA_CONTEXT_GET_MSGBUS(self->context))) ;
	const char *data_path = ma_configurator_get_agent_data_path(MA_CONTEXT_GET_CONFIGURATOR(self->context)) ;
    
	if(data_path != NULL){
		MA_MSC_SELECT(_snprintf, snprintf)(bootstrap_p2p_dir,MAX_PATH_LEN, "%s%c%s%c%s",data_path,MA_PATH_SEPARATOR,MA_AGENT_DATA_DIR_STR,MA_PATH_SEPARATOR,MA_BOOTSTRAP_P2P_CONTENT_BASE_DIR );
		MA_MSC_SELECT(_snprintf, snprintf)(hashes_xml_path,MAX_PATH_LEN, "%s%c%s",bootstrap_p2p_dir,MA_PATH_SEPARATOR,SMART_INSTALLER_HASHES_XML);
	}
	else
		return 0;

	if(ma_fs_is_file_exists(loop,hashes_xml_path)){
		ma_xml_t *installer_hashes_xml = NULL;
		ma_xml_node_t *node = NULL;

		MA_LOG(logger, MA_LOG_SEV_DEBUG, "McAfeeSmartInstall files present in temporary folder") ;
		
		if(MA_OK == (rc = ma_xml_create(&installer_hashes_xml))){
			if(MA_OK == (rc = ma_xml_load_from_file(installer_hashes_xml, hashes_xml_path))){
				if(node = ma_xml_node_search(ma_xml_get_node(installer_hashes_xml),"sha256")) {
					ma_xml_node_t *child_node = ma_xml_node_get_child(node);
					ma_p2p_content_t *p2p_content = NULL ;
					ma_db_t *db_handle = ma_configurator_get_msgbus_database(MA_CONTEXT_GET_CONFIGURATOR(self->context)) ;

					MA_LOG(logger, MA_LOG_SEV_INFO, "Adding installer files to p2p repo") ;
					
					while(child_node){
						if(ma_xml_node_has_data(child_node))
						{
							
							const char *filename = NULL;
							const char *hash = NULL;
							   
							if((filename = ma_xml_node_get_name(child_node)) && (hash = ma_xml_node_get_data(child_node)))
							{
								char time_created[21] ;
								ma_time_t cur_time = {0, 0, 0, 0, 0, 0, 0 } ;
								char *urn = NULL ;
								
								char bootstrap_filepath[MAX_PATH_LEN] = {0};
								char p2p_filepath[MAX_PATH_LEN] = {0};

								ma_time_get_localtime(&cur_time) ;
								MA_MSC_SELECT(_snprintf, snprintf)(time_created, 21, "%04d:%02d:%02d::%02d:%02d:%02d", cur_time.year, cur_time.month, cur_time.day, cur_time.hour, cur_time.minute, cur_time.second) ;

								MA_MSC_SELECT(_snprintf, snprintf)(bootstrap_filepath,MAX_PATH_LEN, "%s%c%s_%s",bootstrap_p2p_dir ,MA_PATH_SEPARATOR,hash,filename);
								MA_MSC_SELECT(_snprintf, snprintf)(p2p_filepath,MAX_PATH_LEN, "%s%c%s_%s",p2p_base_dir ,MA_PATH_SEPARATOR,hash,filename);

								MA_LOG(logger, MA_LOG_SEV_DEBUG, "adding installer file<%s>.", bootstrap_filepath) ;

								if(ma_fs_is_file_exists(loop, bootstrap_filepath) && ma_fs_move_file_as_user(loop,bootstrap_filepath,p2p_filepath, MA_CMNSVC_SID_NAME)) {
									if(MA_OK == ma_p2p_content_create(&p2p_content)) {
										urn = p2p_filepath;
										urn = urn + strlen(p2p_base_dir);
							
										(void)ma_p2p_content_set_base_dir(p2p_content, p2p_base_dir) ;
										(void)ma_p2p_content_set_content_format(p2p_content, MA_P2P_CONTENT_FORMAT_FILE) ;
										(void)ma_p2p_content_set_content_size(p2p_content, ma_fs_get_file_size(loop, p2p_filepath)) ;
										(void)ma_p2p_content_set_content_type(p2p_content, "Default") ;
										(void)ma_p2p_content_set_contributor(p2p_content, MA_SOFTWAREID_GENERAL_STR) ;
										(void)ma_p2p_content_set_hash(p2p_content, hash) ;
										(void)ma_p2p_content_set_hit_count(p2p_content, 0) ;
										(void)ma_p2p_content_set_owned(p2p_content, MA_TRUE) ;
										(void)ma_p2p_content_set_time_created(p2p_content, time_created) ;
										(void)ma_p2p_content_set_time_last_access(p2p_content, time_created) ;
										(void)ma_p2p_content_set_urn(p2p_content, urn) ;

										if(MA_OK != (rc = ma_p2p_db_add_content(db_handle, p2p_content))) {
											MA_LOG(logger, MA_LOG_SEV_DEBUG, "Added installer file(%s) to p2p repo.") ;
										}
										else {
											rc1 = rc ;
											MA_LOG(logger, MA_LOG_SEV_WARNING, "adding installer file(%s) to p2p failed (rc = %d).", urn, rc) ;
										}
										(void)ma_p2p_content_release(p2p_content); p2p_content = NULL ;
									}
								}
								else {
									uv_err_t uv_err = uv_last_error(loop) ;

									MA_LOG(logger, MA_LOG_SEV_WARNING, "Remove file(hash = %s, urn = %s) failed - libuv err: (\'%s\' %s)", hash, urn, uv_strerror(uv_err),  uv_err_name(uv_err)) ;
									rc1 = MA_ERROR_FILE_MOVE_FAILED ;
									MA_LOG(logger, MA_LOG_SEV_WARNING, "adding installer file(%s) to p2p failed (rc = %d).", urn, rc1) ;
								}
							}
						}
						child_node = ma_xml_node_get_next_sibling(child_node);  
					}	
				}
			}
			(void)ma_xml_release(installer_hashes_xml) ;
		}

		if(rc1 == MA_OK)
		{
			ma_fs_remove_file(loop, hashes_xml_path) ;
			MA_LOG(logger, MA_LOG_SEV_DEBUG, "adding installer files to p2p succeded") ;
			ma_fs_remove_recursive_dir(loop, bootstrap_p2p_dir) ;
		}
		else {
			MA_LOG(logger, MA_LOG_SEV_DEBUG, "adding installer files to p2p repo failed, rc = <%d>.", rc1) ;
		}
	}
	return 1;
}