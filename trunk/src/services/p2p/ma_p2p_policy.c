#include "ma_p2p_service_internal.h"
#include "ma/internal/defs/ma_p2p_defs.h"

#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/services/ma_agent_policy_settings_bag.h" 
#include "ma/internal/utils/configurator/ma_configurator.h"

#include "ma/scheduler/ma_scheduler.h"
#include "ma/scheduler/ma_task.h"
#include "ma/scheduler/ma_triggers.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"

#include <stdlib.h>

ma_error_t ma_p2p_policy_create(ma_p2p_service_t *service, ma_p2p_policy_t **p2p_policy) {
    if(service && p2p_policy) {
        ma_p2p_policy_t *tmp = (ma_p2p_policy_t *)calloc(1, sizeof(ma_p2p_policy_t)) ;

        if(!tmp)    return MA_ERROR_OUTOFMEMORY ;

        tmp->data = (void *)service ;
		ma_p2p_policy_update(tmp) ;

        *p2p_policy = tmp ;

        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_p2p_policy_update(ma_p2p_policy_t *self) {
    if(self) {
		ma_error_t rc = MA_OK ;
		ma_p2p_service_t *service = (ma_p2p_service_t *)self->data ;
		ma_policy_settings_bag_t *pso_bag = MA_CONTEXT_GET_POLICY_SETTINGS_BAG(service->context) ;
		ma_configurator_t *configurator = MA_CONTEXT_GET_CONFIGURATOR(service->context) ;
        unsigned short agent_mode = ma_configurator_get_agent_mode(configurator);
        
		const char *str_value = NULL ;
		
		(void)ma_policy_settings_bag_get_int(pso_bag, MA_P2P_SERVICE_SECTION_NAME_STR, MA_P2P_KEY_HIT_COUNT_INT, MA_TRUE, (ma_int32_t *)&self->hit_count, 0) ;
		(void)ma_policy_settings_bag_get_int(pso_bag, MA_P2P_SERVICE_SECTION_NAME_STR, MA_P2P_KEY_PURGE_METHOD_INT, MA_TRUE,(ma_int32_t *)&self->purge_method, 0) ;
		(void)ma_policy_settings_bag_get_int(pso_bag, MA_P2P_SERVICE_SECTION_NAME_STR, MA_P2P_KEY_DISK_QUOTA_INT, MA_FALSE,(ma_int32_t *)&self->disk_quota, 512) ;
		(void)ma_policy_settings_bag_get_int(pso_bag, MA_P2P_SERVICE_SECTION_NAME_STR, MA_P2P_KEY_LONGEVITY_INT, MA_FALSE,(ma_int32_t *)&self->content_longevity, 1) ;
		(void)ma_policy_settings_bag_get_bool(pso_bag, MA_P2P_SERVICE_SECTION_NAME_STR, MA_P2P_KEY_ENABLE_CLIENT_INT, MA_FALSE, &self->enable_client, agent_mode ? MA_TRUE : MA_FALSE) ;
		(void)ma_policy_settings_bag_get_bool(pso_bag, MA_P2P_SERVICE_SECTION_NAME_STR, MA_P2P_KEY_ENABLE_SERVING_INT, MA_FALSE,&self->enable_serving, agent_mode ? MA_TRUE : MA_FALSE) ;
		(void)ma_policy_settings_bag_get_str(pso_bag, MA_P2P_SERVICE_SECTION_NAME_STR, MA_P2P_KEY_REPO_PATH_STR, MA_FALSE, &str_value, "DEFAULT") ;

		if(self->repo_path)	free(self->repo_path) ;
		self->repo_path = NULL ;

		if(!str_value || strlen(str_value) == 0 || 0 == strcmp(str_value, "DEFAULT")) {
			char path[MA_MAX_PATH_LEN] = {0} ;

			MA_MSC_SELECT(_snprintf, snprintf)(path, MA_MAX_PATH_LEN,"%s%c%s", ma_configurator_get_agent_data_path(configurator), MA_PATH_SEPARATOR, MA_AGENT_DATA_DIR_STR) ;
			self->repo_path = strdup(path) ;
		}
		else
			self->repo_path = strdup(str_value) ;

		return rc ;
	}
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_p2p_policy_release(ma_p2p_policy_t *self) {	
    if(self) {
		if(self->repo_path)	free(self->repo_path) ;
		free(self) ;
		return MA_OK  ;
	}
	return MA_ERROR_INVALIDARG;
}

