#include "ma/internal/services/p2p/ma_p2p_db_manager.h"
#include "ma_p2p_service_internal.h"
#include "ma/internal/utils/time/ma_time.h"
#include "ma/internal/ma_macros.h" 

#include <string.h>
#include <stdlib.h>


#define CREATE_P2P_CONTENT_TABLE_STMT   "CREATE TABLE IF NOT EXISTS P2P_CONTENT(HASH TEXT NOT NULL UNIQUE, CONTENT_TYPE TEXT NOT NULL, BASE_DIR TEXT NOT NULL, URN TEXT NOT NULL, CONTENT_SIZE INTEGER NOT NULL, CONTENT_CONTRIBUTOR TEXT NOT NULL, OWNED INTEGER NOT NULL, HIT_COUNT INTEGER NOT NULL, TIME_LAST_ACCESS TEXT NOT NULL, TIME_CREATED TEXT NOT NULL, STATE INTEGER NOT NULL, PRIMARY KEY (HASH) ON CONFLICT REPLACE)"

#define MA_P2P_TABLE_FIELD_BASE							0
#define MA_P2P_TABLE_FIELD_HASH							MA_P2P_TABLE_FIELD_BASE + 1
#define MA_P2P_TABLE_FIELD_CONTENT_TYPE					MA_P2P_TABLE_FIELD_BASE + 2
#define MA_P2P_TABLE_FIELD_BASE_DIR					    MA_P2P_TABLE_FIELD_BASE + 3
#define MA_P2P_TABLE_FIELD_URN							MA_P2P_TABLE_FIELD_BASE + 4
#define MA_P2P_TABLE_FIELD_CONTENT_SIZE					MA_P2P_TABLE_FIELD_BASE + 5
#define MA_P2P_TABLE_FIELD_CONTENT_CONTRIBUTOR			MA_P2P_TABLE_FIELD_BASE + 6
#define MA_P2P_TABLE_FIELD_OWNED						MA_P2P_TABLE_FIELD_BASE + 7
#define MA_P2P_TABLE_FIELD_HIT_COUNT					MA_P2P_TABLE_FIELD_BASE + 8
#define MA_P2P_TABLE_FIELD_TIME_LAST_ACCESS				MA_P2P_TABLE_FIELD_BASE + 9
#define MA_P2P_TABLE_FIELD_TIME_CREATED					MA_P2P_TABLE_FIELD_BASE + 10
#define MA_P2P_TABLE_FIELD_STATE						MA_P2P_TABLE_FIELD_BASE + 11

ma_error_t ma_p2p_db_create_schema(ma_db_t *db_handle) {
	if(db_handle) {
		ma_db_statement_t *db_stmt = NULL;
		ma_error_t rc = MA_OK ;

		if(MA_OK == (rc = ma_db_statement_create(db_handle, CREATE_P2P_CONTENT_TABLE_STMT, &db_stmt))) {
			rc = ma_db_statement_execute(db_stmt) ;
			
			(void)ma_db_statement_release(db_stmt) ; 
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

#define ADD_P2P_CONTENT_SQL_STMT    "INSERT INTO P2P_CONTENT VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
ma_error_t ma_p2p_db_add_content(ma_db_t *db_handle, ma_p2p_content_t *p2p_content) {
	if(db_handle && p2p_content) {
		ma_db_statement_t *db_stmt = NULL ;
		ma_error_t rc = MA_OK ;

		if(MA_OK == (rc = ma_db_statement_create(db_handle, ADD_P2P_CONTENT_SQL_STMT, &db_stmt))) {
			
			(void)ma_db_statement_set_string(db_stmt, MA_P2P_TABLE_FIELD_HASH, 1, p2p_content->hash, strlen(p2p_content->hash)) ;
			(void)ma_db_statement_set_string(db_stmt, MA_P2P_TABLE_FIELD_CONTENT_TYPE, 1, p2p_content->content_type, strlen(p2p_content->content_type)) ;
			(void)ma_db_statement_set_string(db_stmt, MA_P2P_TABLE_FIELD_BASE_DIR, 1, p2p_content->base_dir, strlen(p2p_content->base_dir)) ;
			(void)ma_db_statement_set_string(db_stmt, MA_P2P_TABLE_FIELD_URN, 1, p2p_content->urn, strlen(p2p_content->urn)) ;
			(void)ma_db_statement_set_long(db_stmt, MA_P2P_TABLE_FIELD_CONTENT_SIZE, (long)p2p_content->content_size) ;
			(void)ma_db_statement_set_string(db_stmt, MA_P2P_TABLE_FIELD_CONTENT_CONTRIBUTOR, 1, p2p_content->contributor, strlen(p2p_content->contributor)) ;
			(void)ma_db_statement_set_int(db_stmt, MA_P2P_TABLE_FIELD_OWNED, (int)p2p_content->owned) ;
			(void)ma_db_statement_set_int(db_stmt, MA_P2P_TABLE_FIELD_HIT_COUNT, (int)p2p_content->hit_count) ;
			(void)ma_db_statement_set_string(db_stmt, MA_P2P_TABLE_FIELD_TIME_LAST_ACCESS, 1, p2p_content->time_last_access, strlen(p2p_content->time_last_access)) ;
			(void)ma_db_statement_set_string(db_stmt, MA_P2P_TABLE_FIELD_TIME_CREATED, 1, p2p_content->time_created, strlen(p2p_content->time_created)) ;
			(void)ma_db_statement_set_int(db_stmt, MA_P2P_TABLE_FIELD_STATE, 0) ;

			rc = ma_db_statement_execute(db_stmt) ;
			
			(void)ma_db_statement_release(db_stmt) ; 
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

#define RESET_P2P_CONTENT_STATE_SQL_STMT	"UPDATE P2P_CONTENT SET STATE = 0"
ma_error_t ma_p2p_db_reset_state(ma_db_t *db_handle) {
	if(db_handle) {
		ma_db_statement_t *db_stmt = NULL;
		ma_error_t rc = MA_OK ;

		if(MA_OK == (rc = ma_db_statement_create(db_handle, RESET_P2P_CONTENT_STATE_SQL_STMT, &db_stmt))) {
			rc = ma_db_statement_execute(db_stmt) ;
			
			(void)ma_db_statement_release(db_stmt) ; 
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

#define ERASE_P2P_CONTENT_SQL_STMT    "DELETE FROM P2P_CONTENT WHERE OWNED = 1"
ma_error_t ma_p2p_db_erase_content(ma_db_t *db_handle) {
	if(db_handle) {
		ma_db_statement_t *db_stmt = NULL;
		ma_error_t rc = MA_OK ;

		if(MA_OK == (rc = ma_db_statement_create(db_handle, ERASE_P2P_CONTENT_SQL_STMT, &db_stmt))) {
			rc = ma_db_statement_execute(db_stmt) ;
			
			(void)ma_db_statement_release(db_stmt) ; 
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

#define ERASE_P2P_CONTENT_BY_CONTRIBUTOR_SQL_STMT    "DELETE FROM P2P_CONTENT WHERE CONTENT_CONTRIBUTOR = ?"
ma_error_t ma_p2p_db_erase_content_by_contributor(ma_db_t *db_handle, const char *contributor) {
	if(db_handle && contributor) {
		ma_db_statement_t *db_stmt = NULL;
		ma_error_t rc = MA_OK ;

		if(MA_OK == (rc = ma_db_statement_create(db_handle, ERASE_P2P_CONTENT_BY_CONTRIBUTOR_SQL_STMT, &db_stmt))) {
			(void)ma_db_statement_set_string(db_stmt, 1, 1, contributor, strlen(contributor)) ;

			rc = ma_db_statement_execute(db_stmt) ;
			
			(void)ma_db_statement_release(db_stmt) ; 
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

#define REMOVE_P2P_CONTENT_BY_HASH_SQL_STMT    "DELETE FROM P2P_CONTENT WHERE HASH = ?"
ma_error_t ma_p2p_db_remove_content_by_hash(ma_db_t *db_handle, const char *content_hash) {
	if(db_handle && content_hash) {
		ma_db_statement_t *db_stmt = NULL;
		ma_error_t rc = MA_OK ;

		if(MA_OK == (rc = ma_db_statement_create(db_handle, REMOVE_P2P_CONTENT_BY_HASH_SQL_STMT, &db_stmt))) {
			(void)ma_db_statement_set_string(db_stmt, 1, 1, content_hash, strlen(content_hash)) ;

			rc = ma_db_statement_execute(db_stmt) ;

			(void)ma_db_statement_release(db_stmt) ; 
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

#define REMOVE_P2P_CONTENT_BY_HIT_COUNT_SQL_STMT    "DELETE FROM P2P_CONTENT WHERE HIT_COUNT = ?"
ma_error_t ma_p2p_db_remove_content_by_hit_count(ma_db_t *db_handle, ma_uint32_t hit_count) {
	if(db_handle) {
		ma_db_statement_t *db_stmt = NULL;
		ma_error_t rc;

		if(MA_OK == (rc = ma_db_statement_create(db_handle, REMOVE_P2P_CONTENT_BY_HIT_COUNT_SQL_STMT, &db_stmt))) {
			(void)ma_db_statement_set_int(db_stmt, 1, (int)hit_count) ;
			
			rc = ma_db_statement_execute(db_stmt) ;
			
			(void)ma_db_statement_release(db_stmt) ; 
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

#define REMOVE_P2P_CONTENT_BY_TIME_CREATED_SQL_STMT    "DELETE FROM P2P_CONTENT WHERE TIME_CREATED = ?"
ma_error_t ma_p2p_db_remove_content_by_time_created(ma_db_t *db_handle, const char *time) {
	if(db_handle && time) {
		ma_db_statement_t *db_stmt = NULL;
		ma_error_t rc = MA_OK ;

		if(MA_OK == (rc = ma_db_statement_create(db_handle, REMOVE_P2P_CONTENT_BY_TIME_CREATED_SQL_STMT, &db_stmt))) {
			(void)ma_db_statement_set_string(db_stmt, 1, 1, time, strlen(time)) ;
			
			rc = ma_db_statement_execute(db_stmt) ;
			
			(void)ma_db_statement_release(db_stmt) ; 
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

#define REMOVE_P2P_CONTENT_BY_TIME_LAST_ACCESS_SQL_STMT    "DELETE FROM P2P_CONTENT WHERE TIME_LAST_ACCESS = ?"
ma_error_t ma_p2p_db_remove_content_by_time_lat_access(ma_db_t *db_handle, const char *time) {
	if(db_handle && time) {
		ma_db_statement_t *db_stmt = NULL;
		ma_error_t rc = MA_OK ;

		if(MA_OK == (rc = ma_db_statement_create(db_handle, REMOVE_P2P_CONTENT_BY_TIME_LAST_ACCESS_SQL_STMT, &db_stmt))) {
			(void)ma_db_statement_set_string(db_stmt, 1, 1, time, strlen(time)) ;
			
			rc = ma_db_statement_execute(db_stmt) ;
			
			(void)ma_db_statement_release(db_stmt) ; 
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

#define ESIXTS_P2P_CONTENT_SQL_STMT    "SELECT COUNT(HASH) FROM P2P_CONTENT WHERE HASH = ?"
ma_error_t ma_p2p_db_is_content_exists(ma_db_t *db_handle, const char *content_hash, ma_bool_t *is_exists)  {
	if(db_handle && content_hash && is_exists) {
		ma_db_statement_t *db_stmt = NULL;
		ma_error_t rc = MA_OK ;
		*is_exists = MA_FALSE ;

		if(MA_OK == (rc = ma_db_statement_create(db_handle, ESIXTS_P2P_CONTENT_SQL_STMT, &db_stmt))) {
			ma_db_recordset_t *db_record = NULL ;

			(void)ma_db_statement_set_string(db_stmt, 1, 1, content_hash, strlen(content_hash)) ;

			if(MA_OK == (rc = ma_db_statement_execute_query(db_stmt, &db_record))) {
				if(MA_ERROR_NO_MORE_ITEMS != (rc = ma_db_recordset_next(db_record))) {
					int int_value = 0 ;

					ma_db_recordset_get_int(db_record, 1, &int_value) ;
					if(int_value)	*is_exists = MA_TRUE ;
				}
				(void)ma_db_recordset_release(db_record) ;
			}
			(void)ma_db_statement_release(db_stmt) ;
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

#define GET_P2P_CONTENT_URN_SQL_STMT    "SELECT URN FROM P2P_CONTENT WHERE HASH = ?"
ma_error_t ma_p2p_db_get_urn_for_hash(ma_db_t *db_handle, const char *content_hash, char **urn)  {
	if(db_handle && content_hash && urn) {
		ma_db_statement_t *db_stmt = NULL;
		ma_error_t rc = MA_OK ;

		if(MA_OK == (rc = ma_db_statement_create(db_handle, GET_P2P_CONTENT_URN_SQL_STMT, &db_stmt))) {
			ma_db_recordset_t *db_record = NULL ;

			(void)ma_db_statement_set_string(db_stmt, 1, 1, content_hash, strlen(content_hash)) ;

			if(MA_OK == (rc = ma_db_statement_execute_query(db_stmt, &db_record))) {
				if(MA_ERROR_NO_MORE_ITEMS != (rc = ma_db_recordset_next(db_record))) {
					char *str_value = NULL ;

					(void)ma_db_recordset_get_string(db_record, 1, &str_value) ;
					*urn = strdup(str_value) ;
				}
				else
					rc = MA_ERROR_OBJECTNOTFOUND ;
				(void)ma_db_recordset_release(db_record) ;
			}
			(void)ma_db_statement_release(db_stmt) ;
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}


#define GET_P2P_CONTENT_BY_HASH_SQL_STMT    "SELECT * FROM P2P_CONTENT WHERE HASH = ?"
ma_error_t ma_p2p_db_get_content_by_hash(ma_db_t *db_handle, const char *content_hash, ma_p2p_content_t **p2p_content) {
	if(db_handle && content_hash && p2p_content) {
		ma_db_statement_t *db_stmt = NULL;
		ma_error_t rc = MA_OK ;

		if(MA_OK == (rc = ma_db_statement_create(db_handle, GET_P2P_CONTENT_BY_HASH_SQL_STMT, &db_stmt))) {
			ma_db_recordset_t *db_record = NULL ;

			(void)ma_db_statement_set_string(db_stmt, 1, 1, content_hash, strlen(content_hash)) ;

			if(MA_OK == (rc = ma_db_statement_execute_query(db_stmt, &db_record))) {
				if(MA_ERROR_NO_MORE_ITEMS != ma_db_recordset_next(db_record)) {
					
					if(MA_OK == (rc = ma_p2p_content_create(p2p_content))) {
						char *str_value = NULL ;
						int int_value = 0 ;
						ma_int64_t long_value = 0 ;

						(void)ma_db_recordset_get_string(db_record, MA_P2P_TABLE_FIELD_HASH, &str_value) ;
						(void)ma_p2p_content_set_hash(*p2p_content, str_value) ;
						str_value = NULL ;

						(void)ma_db_recordset_get_string(db_record, MA_P2P_TABLE_FIELD_CONTENT_TYPE, &str_value) ;
						(void)ma_p2p_content_set_content_type(*p2p_content, str_value) ;
						str_value = NULL ;

						(void)ma_db_recordset_get_string(db_record, MA_P2P_TABLE_FIELD_BASE_DIR, &str_value) ;
						(void)ma_p2p_content_set_base_dir(*p2p_content, str_value) ;
						str_value = NULL ;

						(void)ma_db_recordset_get_string(db_record, MA_P2P_TABLE_FIELD_URN, &str_value) ;
						(void)ma_p2p_content_set_urn(*p2p_content, str_value) ;
						str_value = NULL ;

						(void)ma_db_recordset_get_long(db_record, MA_P2P_TABLE_FIELD_CONTENT_SIZE, &long_value) ;
						(void)ma_p2p_content_set_content_size(*p2p_content, (size_t)int_value) ;

						(void)ma_db_recordset_get_string(db_record, MA_P2P_TABLE_FIELD_CONTENT_CONTRIBUTOR, &str_value) ;
						(void)ma_p2p_content_set_contributor(*p2p_content, str_value) ;
						str_value = NULL ;

						(void)ma_db_recordset_get_int(db_record, MA_P2P_TABLE_FIELD_OWNED, &int_value) ;
						(void)ma_p2p_content_set_owned(*p2p_content, (ma_bool_t)int_value) ;

						(void)ma_db_recordset_get_int(db_record, MA_P2P_TABLE_FIELD_HIT_COUNT, &int_value) ;
						(void)ma_p2p_content_set_hit_count(*p2p_content, (ma_uint32_t)int_value) ;

						(void)ma_db_recordset_get_string(db_record, MA_P2P_TABLE_FIELD_TIME_LAST_ACCESS, &str_value) ;
						(void)ma_p2p_content_set_time_last_access(*p2p_content, str_value) ;
						str_value = NULL ;

						(void)ma_db_recordset_get_string(db_record, MA_P2P_TABLE_FIELD_TIME_CREATED, &str_value) ;
						(void)ma_p2p_content_set_time_created(*p2p_content, str_value) ;
						str_value = NULL ;

					}
				}
				else
					rc = MA_ERROR_OBJECTNOTFOUND ;

				(void)ma_db_recordset_release(db_record) ;
			}
			(void)ma_db_statement_release(db_stmt) ; 
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

#define UPDATE_P2P_CONTENT_HIT_COUNT_AND_ACCESS_TIME_SQL_STMT    "UPDATE P2P_CONTENT SET HIT_COUNT = HIT_COUNT+1, TIME_LAST_ACCESS = ? WHERE HASH = ?"
ma_error_t ma_p2p_db_update_hit_count_and_access_time(ma_db_t *db_handle, const char *content_hash){
	if(db_handle && content_hash) {
		ma_db_statement_t *db_stmt = NULL;
		ma_error_t rc = MA_OK ;
		char access_time[21] = {0} ;
		ma_time_t cur_time = {0, 0, 0, 0, 0, 0, 0 } ;
										
		ma_time_get_localtime(&cur_time) ;
		MA_MSC_SELECT(_snprintf, snprintf)(access_time, 21, "%04d:%02d:%02d::%02d:%02d:%02d", cur_time.year, cur_time.month, cur_time.day, cur_time.hour, cur_time.minute, cur_time.second) ;

		if(MA_OK == (rc = ma_db_statement_create(db_handle, UPDATE_P2P_CONTENT_HIT_COUNT_AND_ACCESS_TIME_SQL_STMT, &db_stmt))) {
			(void)ma_db_statement_set_string(db_stmt, 1, 1, access_time, strlen(access_time)) ;
			(void)ma_db_statement_set_string(db_stmt, 2, 1, content_hash, strlen(content_hash)) ;
			
			rc = ma_db_statement_execute(db_stmt) ;
			
			(void)ma_db_statement_release(db_stmt) ; 
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

#ifdef MA_WINDOWS
#define GET_P2P_CONTENT_FILE_PATH_SQL_STMT    "SELECT BASE_DIR||'\\'||URN, STATE FROM P2P_CONTENT WHERE HASH = ?"
#else
#define GET_P2P_CONTENT_FILE_PATH_SQL_STMT    "SELECT BASE_DIR||'/'||URN, STATE FROM P2P_CONTENT WHERE HASH = ?"
#endif
ma_error_t ma_p2p_db_get_filepath_for_hash(ma_db_t *db_handle, const char *content_hash, ma_int32_t *state, char **file_path) {
	if(db_handle && content_hash && file_path) {
		ma_db_statement_t *db_stmt = NULL;
		ma_error_t rc = MA_OK ;

		if(MA_OK == (rc = ma_db_statement_create(db_handle, GET_P2P_CONTENT_FILE_PATH_SQL_STMT, &db_stmt))) {
			ma_db_recordset_t *db_record = NULL ;

			(void)ma_db_statement_set_string(db_stmt, 1, 1, content_hash, strlen(content_hash)) ;

			if(MA_OK == (rc = ma_db_statement_execute_query(db_stmt, &db_record))) {
				if(MA_ERROR_NO_MORE_ITEMS != (rc = ma_db_recordset_next(db_record))) {
					char *str_val = NULL ;

					(void)ma_db_recordset_get_string(db_record, 1, &str_val) ;
					(void)ma_db_recordset_get_int(db_record, 2, state) ;
					*file_path = strdup(str_val) ;

					rc = MA_OK ;
				}
				else
					rc = MA_ERROR_OBJECTNOTFOUND ;

				(void)ma_db_recordset_release(db_record) ;
			}
			(void)ma_db_statement_release(db_stmt) ; 
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

#define SET_P2P_CONTENT_TO_PURGE_SQL_STMT	"UPDATE P2P_CONTENT "\
											"SET STATE = -1 "\
											"WHERE HASH IN ( SELECT HASH FROM ( SELECT HASH, TIME_LAST_ACCESS AS TIME_ACCESS "\
																				"FROM P2P_CONTENT "\
																				"WHERE OWNED = 1 AND STATE = 0 "\
																					  "AND(	SELECT SUM(CONTENT_SIZE) "\
																							"FROM P2P_CONTENT  "\
																							"WHERE TIME_LAST_ACCESS <= TIME_ACCESS "\
																								  "AND  OWNED = 1 AND STATE = 0 "\
																							"ORDER BY TIME_LAST_ACCESS ASC, HIT_COUNT ASC"\
																						") <= ? + CONTENT_SIZE"\
																				")"\
															") "
ma_error_t ma_p2p_db_set_content_to_purge(ma_db_t *db_handle, ma_int64_t purge_size) {
	if(db_handle) {
		ma_db_statement_t *db_stmt = NULL;
		ma_error_t rc = MA_OK ;

		if(MA_OK == (rc = ma_db_statement_create(db_handle, SET_P2P_CONTENT_TO_PURGE_SQL_STMT, &db_stmt))) {

			(void)ma_db_statement_set_long(db_stmt, 1, purge_size) ;
			rc = ma_db_statement_execute(db_stmt) ;

			(void)ma_db_statement_release(db_stmt) ;	db_stmt = NULL ;
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

#define UNSET_P2P_CONTENT_TO_PURGE_SQL_STMT	"UPDATE P2P_CONTENT SET STATE = 0 WHERE STATE = -1 AND OWNED = 1"
ma_error_t ma_p2p_db_unset_content_to_purge(ma_db_t *db_handle) {
	if(db_handle) {
		ma_db_statement_t *db_stmt = NULL;
		ma_error_t rc = MA_OK ;

		if(MA_OK == (rc = ma_db_statement_create(db_handle, UNSET_P2P_CONTENT_TO_PURGE_SQL_STMT, &db_stmt))) {

			rc = ma_db_statement_execute(db_stmt) ;

			(void)ma_db_statement_release(db_stmt) ;	db_stmt = NULL ;
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

#define SET_P2P_CONTENT_TO_PURGE_BY_CORTIBUTOR_SQL_STMT	"UPDATE P2P_CONTENT "\
														"SET STATE = -1 "\
														"WHERE HASH IN ( SELECT HASH FROM ( SELECT HASH, TIME_LAST_ACCESS AS TIME_ACCESS "\
																							"FROM P2P_CONTENT "\
																							"WHERE CONTENT_CONTRIBUTOR = ? AND STATE = 0 "\
																								  "AND(	SELECT SUM(CONTENT_SIZE) "\
																										"FROM P2P_CONTENT  "\
																										"WHERE TIME_LAST_ACCESS <= TIME_ACCESS "\
																											  "AND  CONTENT_CONTRIBUTOR = ? AND STATE = 0 "\
																										"ORDER BY TIME_LAST_ACCESS ASC, HIT_COUNT ASC"\
																									") <= ? + CONTENT_SIZE"\
																							")"\
																		") "
ma_error_t ma_p2p_db_set_content_to_purge_by_contributor(ma_db_t *db_handle, ma_int64_t purge_size, const char *contributor) {
	if(db_handle && contributor) {
		ma_db_statement_t *db_stmt = NULL;
		ma_error_t rc = MA_OK ;

		if(MA_OK == (rc = ma_db_statement_create(db_handle, SET_P2P_CONTENT_TO_PURGE_BY_CORTIBUTOR_SQL_STMT, &db_stmt))) {

			(void)ma_db_statement_set_string(db_stmt, 1, 1, contributor, strlen(contributor)) ;
			(void)ma_db_statement_set_string(db_stmt, 2, 1, contributor, strlen(contributor)) ;
            (void)ma_db_statement_set_long(db_stmt, 3, purge_size) ;
			rc = ma_db_statement_execute(db_stmt) ;

			(void)ma_db_statement_release(db_stmt) ;	db_stmt = NULL ;
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

#define UNSET_P2P_CONTENT_TO_PURGE_BY_CORTIBUTOR_SQL_STMT	"UPDATE P2P_CONTENT SET STATE = 0 WHERE STATE = -1 AND CONTENT_CONTRIBUTOR = ?"
ma_error_t ma_p2p_db_unset_content_to_purge_by_contributor(ma_db_t *db_handle, const char *contributor) {
	if(db_handle && contributor) {
		ma_db_statement_t *db_stmt = NULL;
		ma_error_t rc = MA_OK ;

		if(MA_OK == (rc = ma_db_statement_create(db_handle, UNSET_P2P_CONTENT_TO_PURGE_BY_CORTIBUTOR_SQL_STMT, &db_stmt))) {

			(void)ma_db_statement_set_string(db_stmt, 1, 1, contributor, strlen(contributor)) ;

			rc = ma_db_statement_execute(db_stmt) ;

			(void)ma_db_statement_release(db_stmt) ;	db_stmt = NULL ;
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

#ifdef MA_WINDOWS
#define GET_P2P_CONTENT_TO_PURGE_SQL_STMT    "SELECT HASH, BASE_DIR||'\\'||URN, CONTENT_SIZE FROM P2P_CONTENT WHERE OWNED = 1 AND STATE = -1"
#else
#define GET_P2P_CONTENT_TO_PURGE_SQL_STMT    "SELECT HASH, BASE_DIR||'/'||URN, CONTENT_SIZE FROM P2P_CONTENT WHERE OWNED = 1 AND STATE = -1"
#endif
ma_error_t ma_p2p_db_get_content_to_purge(ma_db_t *db_handle, ma_db_recordset_t **content_set) {
	if(db_handle && content_set) {
		ma_db_statement_t *db_stmt = NULL;
		ma_error_t rc = MA_OK ;

		if(MA_OK == (rc = ma_db_statement_create(db_handle, GET_P2P_CONTENT_TO_PURGE_SQL_STMT, &db_stmt))) {
			if(MA_OK == (rc = ma_db_statement_execute_query(db_stmt, content_set))) {
				rc = MA_OK ;	
			}
			else
				rc = MA_ERROR_OBJECTNOTFOUND ;

			(void)ma_db_statement_release(db_stmt) ;	db_stmt = NULL ;
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

#ifdef MA_WINDOWS
#define GET_P2P_CONTENT_TO_PURGE_BY_CONTRIBUTOR_SQL_STMT    "SELECT HASH, BASE_DIR||'\\'||URN, CONTENT_SIZE FROM P2P_CONTENT WHERE CONTENT_CONTRIBUTOR = ?  AND STATE = -1"
#else
#define GET_P2P_CONTENT_TO_PURGE_BY_CONTRIBUTOR_SQL_STMT    "SELECT HASH, BASE_DIR||'/'||URN, CONTENT_SIZE FROM P2P_CONTENT WHERE CONTENT_CONTRIBUTOR = ?  AND STATE = -1"
#endif
ma_error_t ma_p2p_db_get_content_to_purge_by_contributor(ma_db_t *db_handle, const char *contributor, ma_db_recordset_t **content_set) {
	if(db_handle && contributor && content_set) {
		ma_db_statement_t *db_stmt = NULL;
		ma_error_t rc = MA_OK ;

		if(MA_OK == (rc = ma_db_statement_create(db_handle, GET_P2P_CONTENT_TO_PURGE_BY_CONTRIBUTOR_SQL_STMT, &db_stmt))) {

			(void)ma_db_statement_set_string(db_stmt, 1, 1, contributor, strlen(contributor)) ;
		
			if(MA_OK == (rc = ma_db_statement_execute_query(db_stmt, content_set))) {
				rc = MA_OK ;	
			}
			else
				rc = MA_ERROR_OBJECTNOTFOUND ;

			(void)ma_db_statement_release(db_stmt) ;	db_stmt = NULL ;
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}



#define GET_P2P_CONTENT_AGGREGATED_SIZE_SQL_STMT	"SELECT SUM(CONTENT_SIZE) FROM P2P_CONTENT WHERE OWNED = 1"
ma_error_t ma_p2p_db_get_aggregated_size(ma_db_t *db_handle, ma_int64_t *size) {
	if(db_handle && size) {
		ma_db_statement_t *db_stmt = NULL;
		ma_error_t rc = MA_OK ;
		ma_db_recordset_t *record_set = NULL ;
		*size = 0 ;

		if(MA_OK == (rc = ma_db_statement_create(db_handle, GET_P2P_CONTENT_AGGREGATED_SIZE_SQL_STMT, &db_stmt))) {
			if(MA_OK == (rc = ma_db_statement_execute_query(db_stmt, &record_set))) {
				if(MA_ERROR_NO_MORE_ITEMS != ma_db_recordset_next(record_set)) {
					(void)ma_db_recordset_get_long(record_set, 1 , size) ;
					rc = MA_OK ;
				}
				(void)ma_db_recordset_release(record_set) ;	record_set = NULL ;
			}
			(void)ma_db_statement_release(db_stmt) ;	db_stmt = NULL ;
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

#define GET_P2P_CONTENT_AGGREGATED_SIZE_BY_CONTRIBUTOR_SQL_STMT	"SELECT SUM(CONTENT_SIZE) FROM P2P_CONTENT WHERE CONTENT_CONTRIBUTOR = ?"
ma_error_t ma_p2p_db_get_aggregated_size_by_contributor(ma_db_t *db_handle, const char *contributor, ma_int64_t *size) {
	if(db_handle && contributor && size) {
		ma_db_statement_t *db_stmt = NULL;
		ma_error_t rc = MA_OK ;
		ma_db_recordset_t *record_set = NULL ;
		*size = 0 ;

		if(MA_OK == (rc = ma_db_statement_create(db_handle, GET_P2P_CONTENT_AGGREGATED_SIZE_BY_CONTRIBUTOR_SQL_STMT, &db_stmt))) {
			(void)ma_db_statement_set_string(db_stmt, 1, 1, contributor, strlen(contributor)) ;

			if(MA_OK == (rc = ma_db_statement_execute_query(db_stmt, &record_set))) {
				if(MA_ERROR_NO_MORE_ITEMS != ma_db_recordset_next(record_set)) {
					(void)ma_db_recordset_get_long(record_set, 1 , size) ;
					rc = MA_OK ;
				}
				(void)ma_db_recordset_release(record_set) ;	record_set = NULL ;
			}
			(void)ma_db_statement_release(db_stmt) ;	db_stmt = NULL ;
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

#define UPDATE_P2P_CONTENT_BASEDIR_BY_CONTRIBUTOR_SQL_STMT	"UPDATE P2P_CONTENT SET BASE_DIR = ? WHERE CONTENT_CONTRIBUTOR = ?"
ma_error_t ma_p2p_db_update_base_dir_by_contributor(ma_db_t *db_handle, const char *contributor, const char *base_dir) {
	if(db_handle && contributor && base_dir) {
		ma_db_statement_t *db_stmt = NULL;
		ma_error_t rc = MA_OK ;

		if(MA_OK == (rc = ma_db_statement_create(db_handle, UPDATE_P2P_CONTENT_BASEDIR_BY_CONTRIBUTOR_SQL_STMT, &db_stmt))) {
			(void)ma_db_statement_set_string(db_stmt, 1, 1, base_dir, strlen(base_dir)) ;
			(void)ma_db_statement_set_string(db_stmt, 2, 1, contributor, strlen(contributor)) ;
			
			rc = ma_db_statement_execute(db_stmt) ;
			
			(void)ma_db_statement_release(db_stmt) ; 
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

#define UPDATE_P2P_CONTENT_BASEDIR_SQL_STMT	"UPDATE P2P_CONTENT SET BASE_DIR = ? WHERE OWNED = 1"
ma_error_t ma_p2p_db_update_base_dir(ma_db_t *db_handle, const char *base_dir) {
	if(db_handle && base_dir) {
		ma_db_statement_t *db_stmt = NULL;
		ma_error_t rc = MA_OK ;

		if(MA_OK == (rc = ma_db_statement_create(db_handle, UPDATE_P2P_CONTENT_BASEDIR_SQL_STMT, &db_stmt))) {
			(void)ma_db_statement_set_string(db_stmt, 1, 1, base_dir, strlen(base_dir)) ;
			
			rc = ma_db_statement_execute(db_stmt) ;
			
			(void)ma_db_statement_release(db_stmt) ; 
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}


#define REMOVE_P2P_CONTENT_BY_URN_FILE_SQL_STMT	"DELETE FROM P2P_CONTENT WHERE CONTENT_CONTRIBUTOR = ? AND URN = ?"
#define REMOVE_P2P_CONTENT_BY_URN_DIR_SQL_STMT	"DELETE FROM P2P_CONTENT WHERE CONTENT_CONTRIBUTOR = ? AND URN LIKE ?" /* TODO need to append % */
ma_error_t ma_p2p_db_remove_content_by_urn(ma_db_t *db_handle, const char *contributor, const char *urn, ma_bool_t is_dir) {
	if(db_handle && contributor && urn) {
		ma_db_statement_t *db_stmt = NULL ;
		ma_error_t rc = MA_OK ;

		if(MA_OK == (rc = ma_db_statement_create(db_handle, is_dir?REMOVE_P2P_CONTENT_BY_URN_DIR_SQL_STMT:REMOVE_P2P_CONTENT_BY_URN_FILE_SQL_STMT, &db_stmt))) {
			(void)ma_db_statement_set_string(db_stmt, 1, 1, contributor, strlen(contributor)) ;
			(void)ma_db_statement_set_string(db_stmt, 2, 1, urn, strlen(urn)) ;
			
			rc = ma_db_statement_execute(db_stmt) ;
			
			(void)ma_db_statement_release(db_stmt) ; 
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

#define GET_P2P_CONTENT_STATE_SQL_STMT	"SELECT STATE FROM P2P_CONTENT WHERE HASH = ?"
ma_error_t ma_p2p_db_get_content_state(ma_db_t *db_handle, const char *hash, ma_int32_t *state) {
	if(db_handle && hash && state) {
		ma_db_statement_t *db_stmt = NULL;
		ma_error_t rc = MA_OK ;
		ma_db_recordset_t *record_set = NULL ;

		if(MA_OK == (rc = ma_db_statement_create(db_handle, GET_P2P_CONTENT_STATE_SQL_STMT, &db_stmt))) {
			(void)ma_db_statement_set_string(db_stmt, 1, 1, hash, strlen(hash)) ;

			if(MA_OK == (rc = ma_db_statement_execute_query(db_stmt, &record_set))) {
				if(MA_ERROR_NO_MORE_ITEMS != (rc = ma_db_recordset_next(record_set))) {
					(void)ma_db_recordset_get_int(record_set, 1 , state) ;
					rc = MA_OK ;
				}
				(void)ma_db_recordset_release(record_set) ;	record_set = NULL ;
			}
			(void)ma_db_statement_release(db_stmt) ;	db_stmt = NULL ;
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

#define SET_CONETENT_STATE_SERVING_SQL_STMT	"UPDATE P2P_CONTENT SET STATE = STATE+1 WHERE HASH = ?"
ma_error_t ma_p2p_db_set_content_state_serving(ma_db_t *db_handle, const char *hash) {
	if(db_handle && hash) {
		ma_db_statement_t *db_stmt = NULL;
		ma_error_t rc = MA_OK ;

		if(MA_OK == (rc = ma_db_statement_create(db_handle, SET_CONETENT_STATE_SERVING_SQL_STMT, &db_stmt))) {
			(void)ma_db_statement_set_string(db_stmt, 1, 1, hash, strlen(hash)) ;

			rc = ma_db_statement_execute(db_stmt) ;
			
			(void)ma_db_statement_release(db_stmt) ; db_stmt = NULL ;
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

#define UNSET_CONETENT_STATE_SERVING_SQL_STMT	"UPDATE P2P_CONTENT SET STATE = STATE-1 WHERE HASH = ?"
ma_error_t ma_p2p_db_unset_content_state_serving(ma_db_t *db_handle, const char *hash) {
	if(db_handle && hash) {
		ma_db_statement_t *db_stmt = NULL;
		ma_error_t rc = MA_OK ;

		if(MA_OK == (rc = ma_db_statement_create(db_handle, UNSET_CONETENT_STATE_SERVING_SQL_STMT, &db_stmt))) {
			(void)ma_db_statement_set_string(db_stmt, 1, 1, hash, strlen(hash)) ;

			rc = ma_db_statement_execute(db_stmt) ;
			
			(void)ma_db_statement_release(db_stmt) ; db_stmt = NULL ;
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

#define IS_CONETENT_MOVABLE_SQL_STMT	"SELECT COUNT(*) FROM P2P_CONTENT WHERE STATE != 0"
ma_error_t ma_p2p_db_is_content_movable(ma_db_t *db_handle, ma_bool_t *is_movable) {
	if(db_handle) {
		ma_db_statement_t *db_stmt = NULL;
		ma_error_t rc = MA_OK ;

		*is_movable = MA_TRUE ;

		if(MA_OK == (rc = ma_db_statement_create(db_handle, IS_CONETENT_MOVABLE_SQL_STMT, &db_stmt))) {
			ma_db_recordset_t *record_set = NULL ;

			if(MA_OK == (rc = ma_db_statement_execute_query(db_stmt, &record_set))) {
				if(MA_ERROR_NO_MORE_ITEMS != (rc = ma_db_recordset_next(record_set))) {
					ma_int32_t value = 0 ;
				
					(void)ma_db_recordset_get_int(record_set, 1 , &value) ;
					if(value)	*is_movable = MA_FALSE ;
					rc = MA_OK ;
				}
				(void)ma_db_recordset_release(record_set) ;	record_set = NULL ;
			}
			(void)ma_db_statement_release(db_stmt) ;	db_stmt = NULL ;
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}
