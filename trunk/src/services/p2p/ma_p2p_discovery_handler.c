#include "ma/internal/services/udp_server/ma_udp_msg_handler.h"
#include "ma/internal/services/udp_server/ma_udp_connection.h"

#include "ma/internal/defs/ma_p2p_defs.h"
#include "ma/internal/defs/ma_udp_defs.h"
#include "ma/internal/defs/ma_http_server_defs.h"
#include "ma/internal/services/p2p/ma_p2p_discovery_hanlder.h"
#include "ma/internal/services/p2p/ma_p2p_db_manager.h"
#include "ma/internal/core/msgbus/ma_msgbus_private.h"

#include "ma/internal/services/ma_agent_policy_settings_bag.h" 
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/defs/ma_http_server_defs.h"

#include "ma/internal/utils/threading/ma_atomic.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include "ma/internal/utils/platform/ma_system_property.h"
#include "ma/internal/utils/filesystem/ma_fs_utils.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h" 

#include "ma/ma_log.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "p2p"

static ma_bool_t on_match(ma_udp_msg_handler_t *handler, ma_udp_msg_t *c_msg) ;
static ma_error_t on_msg(ma_udp_msg_handler_t *handler, struct ma_udp_connection_s *c_request, ma_udp_msg_t *c_msg) ;
static ma_error_t add_ref(ma_udp_msg_handler_t *handler) ;
static ma_error_t on_release(ma_udp_msg_handler_t *handler) ;

static const ma_udp_msg_handler_methods_t p2p_discovery_handler_methods = {
	&on_match,
	&on_msg,
	&add_ref,
	&on_release
} ;

typedef struct ma_p2p_discovery_handler_s ma_p2p_discovery_handler_t;
struct ma_p2p_discovery_handler_s {
	ma_udp_msg_handler_t	base ;
	ma_atomic_counter_t		ref_count ;
	ma_context_t			*context ;
	ma_bool_t				is_serving_enabled ;
    void					*data ;
} ;

ma_error_t ma_p2p_discovery_handler_create(struct ma_context_s *context, ma_udp_msg_handler_t **discovery_handler) {
    if(discovery_handler) {
        ma_p2p_discovery_handler_t *tmp = (ma_p2p_discovery_handler_t *)calloc(1, sizeof(ma_p2p_discovery_handler_t)) ;

        if(!tmp)    return MA_ERROR_OUTOFMEMORY ;

		ma_udp_msg_handler_init(&tmp->base, &p2p_discovery_handler_methods, (void *)tmp) ;
		MA_ATOMIC_INCREMENT(tmp->ref_count) ;
		tmp->context = context ;
		*discovery_handler = (ma_udp_msg_handler_t*)tmp ;

        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_p2p_discovery_handler_release(ma_p2p_discovery_handler_t *self) {
    if(self) {

		if(0 == MA_ATOMIC_DECREMENT(self->ref_count)) {
			self->context = NULL ;
			self->data = NULL ;

			free(self) ;
		}
	}
    return MA_OK  ;
}

ma_error_t ma_p2p_discovery_handler_configure(ma_udp_msg_handler_t *handler, ma_context_t *context) {
	if(handler && context) {
		ma_error_t rc = MA_OK ;
		ma_bool_t is_enabled = MA_TRUE ;
		ma_p2p_discovery_handler_t *self = (ma_p2p_discovery_handler_t *)handler ;
		ma_policy_settings_bag_t *pso_bag = MA_CONTEXT_GET_POLICY_SETTINGS_BAG(context) ;

		if(MA_OK == ma_policy_settings_bag_get_bool(pso_bag, MA_P2P_SERVICE_SECTION_NAME_STR, MA_P2P_KEY_ENABLE_SERVING_INT, MA_FALSE,&is_enabled, 1)) {
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Configured p2p discovery handler(%d).", is_enabled ) ;
			self->is_serving_enabled = is_enabled ? MA_TRUE : MA_FALSE ;
		}
                return MA_OK;
	}
	return MA_ERROR_INVALIDARG ;
}

static ma_bool_t on_match(ma_udp_msg_handler_t *handler, ma_udp_msg_t *c_msg) {
	if(handler && c_msg) {
		ma_error_t rc = MA_OK ;
		ma_bool_t rc1 = MA_FALSE ;
		ma_message_t *msg = NULL ;
		ma_p2p_discovery_handler_t *self = (ma_p2p_discovery_handler_t *)handler ;

		if(MA_OK == (rc = ma_udp_msg_get_ma_message(c_msg, &msg))) {
			char *msg_type = NULL ;
			/* TODO : need to revisit as per the udp message specs */
			(void)ma_message_get_property(msg, MA_P2P_MSG_KEY_TYPE_STR, &msg_type) ;

			if(msg_type && 0 == strcmp(msg_type, MA_P2P_MSG_PROP_VAL_RQST_CONTENT_DISCOVERY_STR) && self->is_serving_enabled) {
				rc1 = MA_TRUE ;
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "udp message matched for p2p content discovery") ;
			}
			else if(msg_type && 0 == strcmp(msg_type, MA_P2P_MSG_PROP_VAL_RQST_SERVICE_DISCOVERY_STR) && self->is_serving_enabled) {
				rc1 = MA_TRUE ;
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "udp message matched for p2p service discovery") ;
			}

			ma_message_release(msg) ; msg = NULL ;
		}
		return rc1 ;
	}
	return MA_FALSE ;
}

static ma_error_t on_msg(ma_udp_msg_handler_t *handler, ma_udp_connection_t *c_request, ma_udp_msg_t *c_msg) {
	if(handler && c_request && c_msg) {
		ma_error_t rc = MA_OK ;
		char *msg_type = NULL ;
		ma_message_t *msg = NULL ;

		ma_udp_msg_get_ma_message(c_msg, &msg) ;

		if(msg)
			(void)ma_message_get_property(msg, MA_P2P_MSG_KEY_TYPE_STR, &msg_type) ;

		if(msg_type && 0 == strcmp(msg_type, MA_P2P_MSG_PROP_VAL_RQST_CONTENT_DISCOVERY_STR)) {		
			char *content_hash = NULL ;
	
			ma_p2p_discovery_handler_t *self = (ma_p2p_discovery_handler_t *)handler ;
			ma_db_t *db_handle = ma_configurator_get_msgbus_database(MA_CONTEXT_GET_CONFIGURATOR(self->context)) ;
			uv_loop_t *loop = ma_event_loop_get_uv_loop(ma_msgbus_get_event_loop(MA_CONTEXT_GET_MSGBUS(self->context))) ;
		
			ma_message_get_property(msg, MA_P2P_MSG_KEY_CONTENT_HASH_STR, &content_hash) ;

			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "processing p2p content  discovery for hash(%s).", content_hash) ;

			if(content_hash) {
				char *file_path = NULL ;
				ma_int32_t file_state = -1 ;
			
				(void)ma_p2p_db_get_filepath_for_hash(db_handle, content_hash, &file_state, &file_path) ;

				if(file_path && ma_fs_is_file_exists(loop, file_path) && file_state >= 0) {
					ma_temp_buffer_t temp_url = MA_TEMP_BUFFER_INIT ;
					char *p = NULL ;
					ma_int32_t http_port = MA_HTTP_SERVER_DEFAULT_TCP_PORT_INT ;	
					ma_policy_settings_bag_t *pso_bag = MA_CONTEXT_GET_POLICY_SETTINGS_BAG(self->context) ;
				
					ma_udp_msg_t *resp_udp_msg = NULL ;
					ma_message_t *resp_msg = NULL ;

					MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "P2p content found (hash = %s).", content_hash) ;

					ma_policy_settings_bag_get_int(pso_bag, MA_HTTP_SERVER_SERVICE_SECTION_NAME_STR, MA_HTTP_SERVER_KEY_PORT_INT, MA_FALSE, &http_port, http_port) ;

					ma_temp_buffer_reserve(&temp_url, sizeof(ma_uint32_t)+ 1) ;
					MA_MSC_SELECT(_snprintf, snprintf)(p = (char *)ma_temp_buffer_get(&temp_url), ma_temp_buffer_capacity(&temp_url) , "%d", http_port) ;

					if(MA_OK == (rc = ma_udp_msg_create(&resp_udp_msg)))
					{
						if(MA_OK == (rc = ma_message_create(&resp_msg))) {
							(void)ma_message_set_property(resp_msg, MA_P2P_MSG_KEY_TYPE_STR, MA_P2P_MSG_PROP_VAL_RPLY_CONTENT_DISCOVERY_STR) ;
							(void)ma_message_set_property(resp_msg, MA_P2P_MSG_KEY_CONTENT_HASH_STR, content_hash) ;
							(void)ma_message_set_property(resp_msg, MA_P2P_MSG_KEY_CONTENT_URL_PORT_STR, p = (char *)ma_temp_buffer_get(&temp_url)) ;

							(void)ma_udp_msg_set_ma_message(resp_udp_msg, resp_msg) ;
						
							(void)ma_udp_connection_set_delayed_response(c_request, MA_TRUE) ;
							(void)ma_udp_connection_set_response_delay_timeout(c_request, 300) ;	/* 300 ms delay added to randomize the response send */
						
							if(MA_OK == (rc = ma_udp_connection_post_response(c_request, resp_udp_msg)))
								MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "P2p content discovery(hash = %s), sent response", content_hash) ;
							else
								MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Falied(rc = %d) to send response for p2p content discovery(hash = %s).", rc , content_hash) ;

							(void)ma_message_release(resp_msg) ;	resp_msg = NULL ;
						}
						(void)ma_udp_msg_release(resp_udp_msg) ;	resp_udp_msg = NULL ;
					}			
					ma_temp_buffer_uninit(&temp_url) ;
				}
				else {
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "No p2p content found or content marked for purging(hash = %s).", content_hash) ;
				}
				if(file_path)	free(file_path), file_path = NULL ;
			}
			else {
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "No hash found in message.", content_hash) ;
			}
		}
		else if(msg_type && 0 == strcmp(msg_type, MA_P2P_MSG_PROP_VAL_RQST_SERVICE_DISCOVERY_STR)) {
			ma_udp_msg_t *resp_udp_msg = NULL ;
			ma_message_t *resp_msg = NULL ;
			
			ma_p2p_discovery_handler_t *self = (ma_p2p_discovery_handler_t *)handler ;		
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "processing p2p service discovery");
				
			if(MA_OK == (rc = ma_udp_msg_create(&resp_udp_msg)))
			{
				if(MA_OK == (rc = ma_message_create(&resp_msg))) {
					(void)ma_message_set_property(resp_msg, MA_P2P_MSG_KEY_TYPE_STR, MA_P2P_MSG_PROP_VAL_RPLY_SERVICE_DISCOVERY_STR) ;
					(void)ma_message_set_property(resp_msg, MA_P2P_MSG_KEY_CHECK_SERVICE_EXISTS_STR, "1") ;

					(void)ma_udp_msg_set_ma_message(resp_udp_msg, resp_msg) ;
												
					if(MA_OK == (rc = ma_udp_connection_post_response(c_request, resp_udp_msg)))
						MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "P2p service discovery response") ;
					else
						MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Falied(rc = %d) to send response for p2p service discovery");

					(void)ma_message_release(resp_msg) ;	resp_msg = NULL ;
				}
				(void)ma_udp_msg_release(resp_udp_msg) ;	resp_udp_msg = NULL ;
			}
		}
		
		(void)ma_message_release(msg) ;	msg = NULL ;

		return MA_OK ;
	}
	return MA_ERROR_INVALIDARG ;
}

static ma_error_t add_ref(ma_udp_msg_handler_t *handler) {
	if(handler) {
		ma_p2p_discovery_handler_t *self = (ma_p2p_discovery_handler_t *)handler ;
		MA_ATOMIC_INCREMENT(self->ref_count) ;
	}
	return MA_ERROR_INVALIDARG ;
}

static ma_error_t on_release(ma_udp_msg_handler_t *handler) {
	if(handler) {
		ma_p2p_discovery_handler_t *self = (ma_p2p_discovery_handler_t *)handler ;
		(void)ma_p2p_discovery_handler_release(self) ;
	}
	return MA_OK  ;
}
