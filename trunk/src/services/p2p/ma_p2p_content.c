#include "ma_p2p_service_internal.h"

#include <stdlib.h>
#include <string.h>

ma_error_t ma_p2p_content_create(ma_p2p_content_t **p2p_content) {
    if(p2p_content) {
        ma_p2p_content_t *tmp = (ma_p2p_content_t *)calloc(1, sizeof(ma_p2p_content_t)) ;

        if(!tmp)    return MA_ERROR_OUTOFMEMORY ;

		tmp->buffer = NULL ;
		tmp->hit_count = 0 ;
		tmp->time_created = NULL ;
		tmp->time_last_access = NULL ;
		tmp->owned = MA_FALSE ;
		tmp->content_size = 0 ;

        *p2p_content = tmp ;
        
		return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_p2p_content_release(ma_p2p_content_t *self) {
    if(self) {
        if(self->hash)   free(self->hash) ;
        self->hash = NULL ;

		if(self->content_type)   free(self->content_type) ;
        self->content_type = NULL ;

		if(self->base_dir)   free(self->base_dir) ;
        self->base_dir = NULL ;

		if(self->urn)   free(self->urn) ;
        self->urn = NULL ;

		if(self->contributor)   free(self->contributor) ;
        self->contributor = NULL ;

        if(self->buffer)   ma_buffer_release(self->buffer) ;
        self->buffer = NULL ;

        if(self->time_last_access)   free(self->time_last_access) ;
        self->time_last_access = NULL ;

        if(self->time_created)   free(self->time_created) ;
        self->time_created = NULL ;

        free(self) ;
		return MA_OK ;
    }
	return MA_ERROR_INVALIDARG;
    
}

ma_error_t ma_p2p_content_set_hash(ma_p2p_content_t *self, const char *hash) {
    if(self && hash) {
		if(self->hash)	free(self->hash) ;
        self->hash = strdup(hash) ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_p2p_content_set_content_type(ma_p2p_content_t *self, const char *type) {
	if(self && type) {
		if(self->content_type)	free(self->content_type) ;
        self->content_type = strdup(type) ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_p2p_content_set_base_dir(ma_p2p_content_t *self, const char *base_dir) {
	if(self && base_dir) {
		if(self->base_dir)	free(self->base_dir) ;
        self->base_dir = strdup(base_dir) ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_p2p_content_set_urn(ma_p2p_content_t *self, const char *urn) {
	if(self && urn) {
		if(self->urn)	free(self->urn) ;
        self->urn = strdup(urn) ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_p2p_content_set_content_size(ma_p2p_content_t *self, size_t size) {
	if(self) {
		self->content_size = size ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_p2p_content_set_contributor(ma_p2p_content_t *self, const char *contributor) {
	if(self && contributor) {
		if(self->contributor)	free(self->contributor) ;
        self->contributor = strdup(contributor) ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_p2p_content_set_owned(ma_p2p_content_t *self, ma_bool_t owned) {
	if(self) {
        self->owned = owned ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}


ma_error_t ma_p2p_content_set_content_format(ma_p2p_content_t *self, ma_p2p_content_format_t format) {
	if(self) {
        self->content_format = format ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_p2p_content_set_buffer(ma_p2p_content_t *self, ma_buffer_t *buffer) {
    if(self && buffer) {
		if(self->buffer)	ma_buffer_release(self->buffer) ;
        self->buffer = buffer ;
		ma_buffer_add_ref(buffer) ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_p2p_content_set_hit_count(ma_p2p_content_t *self, ma_uint32_t hit_count) {
    if(self) {
        self->hit_count = hit_count ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_p2p_content_set_time_last_access(ma_p2p_content_t *self, const char *time_last_access) {
    if(self && time_last_access) {
		if(self->time_last_access)	free(self->time_last_access) ;
        self->time_last_access = strdup(time_last_access) ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_p2p_content_set_time_created(ma_p2p_content_t *self, const char *time_created) {
    if(self && time_created) {
		if(self->time_created)	free(self->time_created) ;
        self->time_created = strdup(time_created) ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_p2p_content_set_file_op(ma_p2p_content_t *self, ma_p2p_content_add_type_t file_op) {
	if(self) {
		self->file_op = file_op ;
		return MA_OK ;		
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_p2p_content_get_hash(ma_p2p_content_t *self, const char **hash) {
    if(self && hash) {
        if(self->hash)
            *hash = self->hash ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_p2p_content_get_content_type(ma_p2p_content_t *self, const char **type) {
	if(self && type) {
       *type = self->content_type ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_p2p_content_get_base_dir(ma_p2p_content_t *self, const char **base_dir) {
	if(self && base_dir) {
       *base_dir = self->base_dir ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_p2p_content_get_urn(ma_p2p_content_t *self, const char **urn) {
    if(self && urn) {
        if(self->urn)
            *urn = self->urn ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_p2p_content_get_content_size(ma_p2p_content_t *self, size_t *size) {
    if(self && size) {
		*size = self->content_size ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_p2p_content_get_contributor(ma_p2p_content_t *self, const char **contributor) {
	if(self && contributor) {
       *contributor = self->contributor ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_p2p_content_get_owned(ma_p2p_content_t *self, ma_bool_t *owned) {
    if(self && owned) {
        *owned = self->owned ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_p2p_content_get_content_format(ma_p2p_content_t *self, ma_p2p_content_format_t *format) {
	if(self && format) {
       *format = self->content_format ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_p2p_content_get_buffer(ma_p2p_content_t *self, ma_buffer_t **buffer) {
    if(self && buffer) {
        if(self->buffer) {
            *buffer = self->buffer ;
			ma_buffer_add_ref(self->buffer) ;
		}
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_p2p_content_get_hit_count(ma_p2p_content_t *self, ma_uint32_t *hit_count) {
    if(self && hit_count) {
        *hit_count = self->hit_count ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_p2p_content_get_time_last_access(ma_p2p_content_t *self, const char **time_last_access) {
    if(self && time_last_access) {
        if(self->time_last_access) 
            *time_last_access = self->time_last_access ;
		return MA_OK ;
	}
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_p2p_content_get_time_created(ma_p2p_content_t *self, const char **time_created) {
    if(self && time_created) {
        if(self->time_created)
            *time_created = self->time_created ;
		return MA_OK ;		
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_p2p_content_get_file_op(ma_p2p_content_t *self, ma_p2p_content_add_type_t *file_op) {
	if(self && file_op) {
        if(self->file_op)
            *file_op = self->file_op ;
		return MA_OK ;		
    }
    return MA_ERROR_INVALIDARG ;
}