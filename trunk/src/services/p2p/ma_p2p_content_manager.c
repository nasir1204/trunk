#include "ma_p2p_service_internal.h"
#include "ma/internal/defs/ma_p2p_defs.h"
#include "ma/internal/services/p2p/ma_p2p_db_manager.h"
#include "ma/internal/core/msgbus/ma_msgbus_private.h"

#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal/utils/filesystem/ma_fs_utils.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h" 

#include "ma/internal/utils/time/ma_time.h"
#include "ma/internal/utils/acl/ma_acl.h"
#include "ma/internal/ma_strdef.h"

#include "ma/scheduler/ma_scheduler.h"
#include "ma/scheduler/ma_task.h"
#include "ma/scheduler/ma_triggers.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

ma_error_t ma_p2p_content_manager_create(ma_p2p_service_t *p2p_service, ma_p2p_content_manager_t **content_manager) {
    if(p2p_service && content_manager) {
        ma_p2p_content_manager_t *tmp = (ma_p2p_content_manager_t *)calloc(1, sizeof(ma_p2p_content_manager_t)) ;

        if(!tmp)    return MA_ERROR_OUTOFMEMORY ;

        tmp->data = (void *)p2p_service ;

		MA_MSC_SELECT(_snprintf, snprintf)(tmp->p2p_content_dir, MA_MAX_PATH_LEN,"%s%c%s", p2p_service->p2p_policy->repo_path, MA_PATH_SEPARATOR, MA_P2P_CONTENT_BASE_DIR_STR) ;

		*content_manager = tmp ;

        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}



static ma_error_t io_srvc_request_async_cb(ma_error_t status, ma_message_t *response, void *cb_data, ma_msgbus_request_t *request) {    
    ma_p2p_content_manager_t *self = (ma_p2p_content_manager_t *) cb_data;
    ma_p2p_service_t *service = (ma_p2p_service_t *)self->data ;
    ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(service->context) ;
    ma_db_t *db_handle = ma_configurator_get_msgbus_database(MA_CONTEXT_GET_CONFIGURATOR(service->context)) ;
    uv_loop_t *loop = ma_event_loop_get_uv_loop(ma_msgbus_get_event_loop(MA_CONTEXT_GET_MSGBUS(service->context))) ;
    char *path = NULL;
	ma_message_t *msg = NULL ;
	ma_error_t rc = MA_OK ;
 
    if(MA_OK == status && response && MA_OK == ma_message_get_property(response, MA_DESTINATION_FOLDER_NAME_STR, &path) && path && ma_fs_is_file_exists(loop, path)) {        
		(void)ma_p2p_db_update_base_dir(db_handle, path) ;
			
		MA_LOG(logger, MA_LOG_SEV_DEBUG, "Created p2p repo directory(%s).", path) ;

		memset(self->p2p_content_dir, 0, MA_MAX_PATH_LEN) ;
		MA_MSC_SELECT(_snprintf, snprintf)(self->p2p_content_dir, MA_MAX_PATH_LEN, "%s", path) ;
    }
    else {
        MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to create new p2p repo directory(%s), Keeping the content in %s", service->p2p_policy->repo_path, self->p2p_content_dir) ;
	}

    if((status == MA_OK) && response) ma_message_release(response) ;
	if(request) {
		ma_msgbus_endpoint_t * ep = NULL;
		(void) ma_msgbus_request_get_endpoint(request, &ep);		
		(void) ma_msgbus_request_release(request);
        if(ep) (void) ma_msgbus_endpoint_release(ep);
	}

	if(MA_OK == ma_message_create(&msg)) {
		ma_msgbus_endpoint_t *ep = NULL ;

		if(MA_OK == (rc = ma_msgbus_endpoint_create(MA_CONTEXT_GET_MSGBUS(service->context), MA_HTTP_SERVER_SERVICE_NAME_STR, NULL, &ep))) {
			(void) ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC) ;
            (void) ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO) ;

            (void) ma_message_set_property(msg, MA_HTTP_SERVER_MSG_KEY_TYPE_STR, MA_HTTP_SERVER_MSG_PROP_VAL_RQST_RESUME_P2P_SERVING_STR) ;

			if(MA_OK != ma_msgbus_send_and_forget(ep, msg))
				MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to send message to http server to resume the p2p serving <%d>.", rc ) ;

	        (void) ma_msgbus_endpoint_release(ep) ; ep = NULL ;
        }
		else
			MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to send message to http server to resume the p2p serving <%d>.", rc ) ;

		ma_message_release(msg) ;  msg = NULL ;
	}
	else MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to send message to http server to resume the p2p serving <%d>.", rc ) ;
		

    return MA_OK;
}


ma_error_t ma_p2p_content_manager_update(ma_p2p_content_manager_t *self) {
	if(self) {
		ma_error_t rc = MA_OK ;
		char path[MA_MAX_PATH_LEN] = {0} ;

		ma_p2p_service_t *service = (ma_p2p_service_t *)self->data ;
		ma_db_t *db_handle = ma_configurator_get_msgbus_database(MA_CONTEXT_GET_CONFIGURATOR(service->context)) ;
		uv_loop_t *loop = ma_event_loop_get_uv_loop(ma_msgbus_get_event_loop(MA_CONTEXT_GET_MSGBUS(service->context))) ;
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(service->context) ;

		MA_MSC_SELECT(_snprintf, snprintf)(path, MA_MAX_PATH_LEN,"%s%c%s", service->p2p_policy->repo_path, MA_PATH_SEPARATOR, MA_P2P_CONTENT_BASE_DIR_STR) ;

		if(service->p2p_policy->enable_serving) {
            if(!ma_fs_is_file_exists(loop, self->p2p_content_dir)) {
                ma_message_t *msg = NULL;
                if(MA_OK == (rc = ma_message_create(&msg))) {   
                    ma_table_t *acl_tb = NULL;
                    if(MA_OK == (rc = ma_table_create(&acl_tb))) {
                        ma_variant_t *value = NULL;
                        if(MA_OK == (rc = ma_variant_create_from_int8(MA_SID_LOCAL, &value))) {
                            ma_table_add_entry(acl_tb, MA_ACL_SID_INT, value);
                            ma_variant_release(value); value = NULL;
                        }

                        if((MA_OK ==rc) && (MA_OK == (rc = ma_variant_create_from_string(MA_CMNSVC_SID_NAME, sizeof(MA_CMNSVC_SID_NAME), &value)))) {
                            ma_table_add_entry(acl_tb, MA_ACL_SID_NAME_STR, value);
                            ma_variant_release(value); value = NULL;
                        }

                        if((MA_OK ==rc) && (MA_OK == (rc = ma_variant_create_from_bool(MA_TRUE, &value)))) {
                            ma_table_add_entry(acl_tb, MA_ACL_SID_ALLOW_BOOL, value);
                            ma_variant_release(value); value = NULL;
                        }

                        if((MA_OK ==rc) && (MA_OK == (rc = ma_variant_create_from_string(path, strlen(path), &value)))) {
                            ma_table_add_entry(acl_tb, MA_ACL_OBJECT_NAME_STR, value);
                            ma_variant_release(value); value = NULL;
                        }

                        if((MA_OK ==rc) && (MA_OK == (rc = ma_variant_create_from_int64(MA_CMNSVC_ACL_PERMISSIONS, &value)))) {
                            ma_table_add_entry(acl_tb, MA_ACL_OBJECT_PERMISSIONS_INT, value);
                            ma_variant_release(value); value = NULL;
                        }

                        if((MA_OK ==rc) && (MA_OK == (rc = ma_variant_create_from_int64(MA_CMNSVC_ACL_INHERITANCE, &value)))) {
                            ma_table_add_entry(acl_tb, MA_ACL_OBJECT_INHERITANCE_INT, value);
                            ma_variant_release(value); value = NULL;
                        }

                        if((MA_OK ==rc) && (MA_OK == (rc = ma_variant_create_from_bool(MA_TRUE, &value)))) {
                            ma_table_add_entry(acl_tb, MA_ACL_OBJECT_INHERITANCE_BOOL, value);
                            ma_variant_release(value); value = NULL;
                        }

                        if((MA_OK ==rc) && (MA_OK == (rc = ma_variant_create_from_bool(MA_TRUE, &value)))) {
                            ma_table_add_entry(acl_tb, MA_ACL_OBJECT_IS_DIRCTORY_BOOL, value);
                            ma_variant_release(value); value = NULL;
                        }

                        if(MA_OK == rc) {
                            ma_variant_t *payload = NULL;
                            if(MA_OK == (rc = ma_variant_create_from_table(acl_tb, &payload))) {
                                if(MA_OK == (rc = ma_message_set_payload(msg, payload))) {
                                    ma_msgbus_endpoint_t *ep = NULL;
                                    if(MA_OK == (rc = ma_message_set_property(msg, MA_PROP_KEY_REQUEST_TYPE_STR, MA_PROP_VALUE_ACL_REQUEST_STR))) {
                                        if(MA_OK == (rc = ma_msgbus_endpoint_create(MA_CONTEXT_GET_MSGBUS(service->context), MA_IO_SERVICE_NAME_STR, NULL, &ep))) {
                                            ma_msgbus_request_t *request = NULL;
			                                (void) ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                                            (void) ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
                                            if(MA_OK != (rc = ma_msgbus_async_send(ep, msg, io_srvc_request_async_cb, self, &request))) {				
				                                (void) ma_msgbus_endpoint_release(ep);
			                                }
											else {
												MA_LOG(logger, MA_LOG_SEV_DEBUG, "Request sent to create p2p repo", path) ;
											}
                                        }
                                    }
                                }
                                (void) ma_variant_release(payload);
                            }
                        }
                        (void) ma_table_release(acl_tb);
                    }
                    (void) ma_message_release(msg);
                }
            }
            else {
                if(strcmp(path, self->p2p_content_dir)) {
                    ma_message_t *msg = NULL;
					ma_msgbus_endpoint_t *ep = NULL;
					ma_bool_t	is_movable = MA_TRUE ;

					(void)ma_p2p_db_is_content_movable(db_handle, &is_movable) ;

					if(!is_movable) {
						MA_LOG(logger, MA_LOG_SEV_WARNING, "Content cannot be moved(being served or purged), will try in next enforcement.") ;
					}
					else {
						if(MA_OK == (rc = ma_message_create(&msg))) {   
							if(MA_OK == (rc = ma_msgbus_endpoint_create(MA_CONTEXT_GET_MSGBUS(service->context), MA_HTTP_SERVER_SERVICE_NAME_STR, NULL, &ep))) {
								ma_msgbus_request_t *request = NULL ;

								(void) ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC) ;
								(void) ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO) ;

								(void) ma_message_set_property(msg, MA_HTTP_SERVER_MSG_KEY_TYPE_STR, MA_HTTP_SERVER_MSG_PROP_VAL_RQST_SUSPEND_P2P_SERVING_STR) ;

								if(MA_OK == ma_msgbus_send_and_forget(ep, msg))
									MA_LOG(logger, MA_LOG_SEV_DEBUG, "Sent message to http server to suspend the p2p serving") ;
								else
									MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to send message to http server to suspend the p2p serving <%d>.", rc ) ;

								(void) ma_msgbus_endpoint_release(ep) ; ep = NULL ;
							}
							ma_message_release(msg) ;  msg = NULL ;
						}

						if(MA_OK == (rc = ma_message_create(&msg))) {   
							if(MA_OK == (rc = ma_msgbus_endpoint_create(MA_CONTEXT_GET_MSGBUS(service->context), MA_IO_SERVICE_NAME_STR, NULL, &ep))) {
								ma_msgbus_request_t *request = NULL;

								(void) ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
								(void) ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);

								(void) ma_message_set_property(msg, MA_PROP_KEY_REQUEST_TYPE_STR, MA_PROP_VALUE_FILE_MOVE_REQUEST_STR);
								(void) ma_message_set_property(msg, MA_FILE_MOVE_SOURCE_STR, self->p2p_content_dir);
								(void) ma_message_set_property(msg, MA_FILE_MOVE_DESTINATION_STR, path);
								(void) ma_message_set_property(msg, MA_FILE_MOVE_BASE_DIR_STR, service->p2p_policy->repo_path);       

								if(MA_OK != (rc = ma_msgbus_async_send(ep, msg, io_srvc_request_async_cb, self, &request))) {		
									MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to send message for p2p repo directory movement rc <%d>.", rc ) ; 
									(void) ma_msgbus_endpoint_release(ep);
								}
								else {
									MA_LOG(logger, MA_LOG_SEV_DEBUG, "Message sent to move p2p repo/content directory ") ;
								}
							}
							(void) ma_message_release(msg); msg = NULL ;
						}
					}
                }
            }
		}
		else {
			if(ma_fs_is_file_exists(loop, self->p2p_content_dir)) {
				ma_bool_t	is_movable = MA_TRUE ;

				MA_LOG(logger, MA_LOG_SEV_DEBUG, "Serving disabled, Any content will be erased.") ;

				(void)ma_p2p_db_is_content_movable(db_handle, &is_movable) ;

				if(!is_movable) {
					MA_LOG(logger, MA_LOG_SEV_WARNING, "Content cannot be erased(being served or purged), will try in next enforcement.") ;
					return MA_OK ;
				}
				else {
					if(!ma_fs_remove_recursive_dir(loop, self->p2p_content_dir)) {
						uv_err_t uv_err = uv_last_error(loop) ;
						MA_LOG(logger, MA_LOG_SEV_WARNING, "Failed to remove files - libuv err: (\'%s\' %s)", uv_strerror(uv_err),  uv_err_name(uv_err)) ;
					}
				}
			}
			(void)ma_p2p_db_erase_content(db_handle) ;
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_p2p_content_manager_add_content(ma_p2p_content_manager_t *self, ma_uint16_t file_op, ma_p2p_content_t *content) {
	if(self && content) {
		ma_error_t rc = MA_OK ;
		ma_int64_t aggregated_size = 0 ;

		char * pch = NULL ;
		char directory[MA_MAX_PATH_LEN] = {0} ;
		char file_name[MA_MAX_LEN] = {0} ;

		ma_p2p_content_t *existing_content = NULL ;
		ma_p2p_service_t *service = (ma_p2p_service_t *)self->data ;
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(service->context) ;
		uv_loop_t *loop = ma_event_loop_get_uv_loop(ma_msgbus_get_event_loop(MA_CONTEXT_GET_MSGBUS(service->context))) ;
		ma_db_t *db_handle = ma_configurator_get_msgbus_database(MA_CONTEXT_GET_CONFIGURATOR(service->context)) ;

		if(MA_OK == (rc = ma_p2p_db_get_content_by_hash(db_handle, content->hash, &existing_content))) {
			/* ignoring in the below scenarios, need to revisit */
			if( (existing_content->owned) || (!existing_content->owned && !content->owned) ) {
				MA_LOG(logger, MA_LOG_SEV_INFO, "Ignoring the request as content already in the p2p repo") ;
				(void)ma_p2p_content_release(existing_content) ;	existing_content = NULL ;
				return MA_OK ;
			}
			(void)ma_p2p_content_release(existing_content) ;	existing_content = NULL ;
		}
		else {
			rc = MA_OK ;
		}

		pch = strrchr(content->urn, MA_PATH_SEPARATOR) ;
		if(pch) {
			MA_MSC_SELECT(_snprintf, snprintf)(directory, pch-content->urn, "%s", content->urn) ;
			MA_MSC_SELECT(_snprintf, snprintf)(file_name, strlen(pch+1), "%s", pch+1) ;
			ma_fs_make_recursive_dir(loop, self->p2p_content_dir, directory) ;
		}
		else {
			MA_MSC_SELECT(_snprintf, snprintf)(file_name, strlen(content->urn), "%s", content->urn) ;
		}

		if(MA_OK == rc ) {
			char target_file[MA_MAX_PATH_LEN] = {0} ;
			char target_urn[MA_MAX_PATH_LEN] = {0} ;
			char source_urn[MA_MAX_PATH_LEN] = {0} ;

			MA_MSC_SELECT(_snprintf, snprintf)(target_file, MA_MAX_PATH_LEN, "%s%c%s%c%s_%s", self->p2p_content_dir, MA_PATH_SEPARATOR, directory, MA_PATH_SEPARATOR, content->hash, file_name) ;
			MA_MSC_SELECT(_snprintf, snprintf)(target_urn, MA_MAX_PATH_LEN, "%s%c%s_%s", directory, MA_PATH_SEPARATOR, content->hash, file_name) ;
			MA_MSC_SELECT(_snprintf, snprintf)(source_urn, MA_MAX_PATH_LEN, "%s", content->urn) ;
			(void)ma_p2p_content_set_urn(content, target_urn) ;
			
			if(content->content_size <= 0)
				(void)ma_p2p_content_set_content_size(content, ma_fs_get_file_size(loop, target_file)) ;

			if(MA_OK == (rc = ma_p2p_db_add_content(db_handle, content))) {
				if(content->content_format == MA_P2P_CONTENT_FORMAT_FILE) {
					char source_file[MA_MAX_PATH_LEN] = {0} ; 
					const char *str_value = NULL ; size_t str_len = 0 ;

					(void)ma_buffer_get_string(content->buffer, &str_value, &str_len) ;
					MA_MSC_SELECT(_snprintf, snprintf)(source_file, MA_MAX_PATH_LEN, "%s%c%s", str_value, MA_PATH_SEPARATOR, source_urn) ;

					if(file_op == MA_P2P_CONTENT_COPY) {
						if(!ma_fs_copy_file_as_user(loop, source_file, target_file, MA_CMNSVC_SID_NAME)) {
							uv_err_t uv_err = uv_last_error(loop) ;
							
							MA_LOG(logger, MA_LOG_SEV_WARNING, "Copy file(hash = %s, urn = %s) failed - libuv err: (\'%s\' %s)", content->hash, content->urn, uv_strerror(uv_err),  uv_err_name(uv_err)) ;

							rc = MA_ERROR_FILE_COPY_FAILED ;
						}
					}
					if(file_op == MA_P2P_CONTENT_MOVE) {
						if(! ma_fs_move_file_as_user(loop, source_file, target_file, MA_CMNSVC_SID_NAME)) {
							uv_err_t uv_err = uv_last_error(loop) ;
							
							MA_LOG(logger, MA_LOG_SEV_WARNING, "Move file(hash = %s, urn = %s) failed - libuv err: (\'%s\' %s)", content->hash, content->urn, uv_strerror(uv_err),  uv_err_name(uv_err)) ;

							rc = MA_ERROR_FILE_MOVE_FAILED ;
						}
					}
				}
				else {	/* buffer */
					unsigned char *buffer_data = NULL ; size_t data_len = 0 ;
					(void)ma_buffer_get_raw(content->buffer, (const unsigned char **)&buffer_data, &data_len) ;
					if(! ma_fs_write_data(loop, target_file, buffer_data, data_len)) {
						uv_err_t uv_err = uv_last_error(loop) ;
							
						MA_LOG(logger, MA_LOG_SEV_WARNING, "Write file(hash = %s, urn = %s) failed - libuv err: (\'%s\' %s)", content->hash, content->urn, uv_strerror(uv_err),  uv_err_name(uv_err)) ;

						rc = MA_ERROR_FILE_WRITE_FAILED ;
					}
				}
				/* if file copy/move or buffer write fails , delete the record from DB */
				if(MA_OK != rc) {
					MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to save content info in filesystem(%d).", rc) ;
					(void)ma_p2p_db_remove_content_by_hash(db_handle, content->hash) ;
				}
				else {
					MA_LOG(logger, MA_LOG_SEV_INFO, "content(hash = %s, size = %d) added to p2p repo.", content->hash, content->content_size) ;
				}
			}
			else {
				MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to save content info in DB(%d).", rc) ;
			}
		}

		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_p2p_content_manager_remove_content(ma_p2p_content_manager_t *self, ma_p2p_content_t *content) {
    if(self && content) {
		ma_error_t rc = MA_OK ;
		ma_p2p_content_t *existing_content = NULL ;
		ma_p2p_service_t *service = (ma_p2p_service_t *)self->data ;
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(service->context) ;
		uv_loop_t *loop = ma_event_loop_get_uv_loop(ma_msgbus_get_event_loop(MA_CONTEXT_GET_MSGBUS(service->context))) ;
		ma_db_t *db_handle = ma_configurator_get_msgbus_database(MA_CONTEXT_GET_CONFIGURATOR(service->context)) ;

		if(MA_OK == (rc = ma_p2p_db_get_content_by_hash(db_handle, content->hash, &existing_content))) {
			
			/* only referenced content will be removed matching the contributor */
			if(!existing_content->owned && (0 == strcmp(content->contributor, existing_content->contributor))) {
				char file_name[MA_MAX_PATH_LEN] = {0} ;

				MA_MSC_SELECT(_snprintf, snprintf)(file_name, MA_MAX_PATH_LEN, "%s%c%s", self->p2p_content_dir, MA_PATH_SEPARATOR, existing_content->urn) ;
				if(ma_fs_remove_file(loop, file_name)) {
					rc = ma_p2p_db_remove_content_by_hash(db_handle, content->hash) ;
				}
				else {
					uv_err_t uv_err = uv_last_error(loop) ;
					MA_LOG(logger, MA_LOG_SEV_WARNING, "Remove file(hash = %s, urn = %s) failed - libuv err: (\'%s\' %s)", existing_content->hash, existing_content->urn, uv_strerror(uv_err),  uv_err_name(uv_err)) ;
				}
			}
			else {
				MA_LOG(logger, MA_LOG_SEV_WARNING, "Only referenced and with same contributor can remove the content") ;
			}
			(void)ma_p2p_content_release(existing_content) ; existing_content = NULL ;
		}
		else {
			MA_LOG(logger, MA_LOG_SEV_ERROR, "No content (hash = %s) exists.", content->hash) ;
			rc = MA_OK ;
		}

		if(MA_OK != rc) {
			MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed(rc = %d) to delete content(hash = %s).", rc, content->hash) ;
		}
		
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_p2p_content_manager_purge_content(ma_p2p_content_manager_t *self, ma_bool_t purge_task) {
    if(self) {
		ma_error_t rc = MA_OK ;
		ma_int64_t aggregated_size = 0, purge_size = 0 ;
		ma_int64_t	disk_quota = 0 ;

		ma_p2p_service_t *service = (ma_p2p_service_t *)self->data ;
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(service->context) ;
		uv_loop_t *loop = ma_event_loop_get_uv_loop(ma_msgbus_get_event_loop(MA_CONTEXT_GET_MSGBUS(service->context))) ;
		ma_db_t *db_handle = ma_configurator_get_msgbus_database(MA_CONTEXT_GET_CONFIGURATOR(service->context)) ;

		ma_db_recordset_t *db_record = NULL ;

		(void)ma_p2p_db_get_aggregated_size(db_handle, &aggregated_size) ;
		disk_quota = (service->p2p_policy->disk_quota*1024LL*1024LL) ;

		/*TBD currently purging the half the size of disk quota set in case of task, do we need some logic?? */
		purge_size = aggregated_size - (purge_task? disk_quota/2 : disk_quota) ;
		
		MA_LOG(logger, MA_LOG_SEV_DEBUG, "aggregated_size = %lld, disk_quota = %lld, purge_size = %lld", aggregated_size, disk_quota, purge_size) ;

		if(purge_size > 0 && MA_OK == (rc = ma_p2p_db_set_content_to_purge(db_handle, purge_size)) && MA_OK == (rc = ma_p2p_db_get_content_to_purge(db_handle, &db_record))) {
			ma_int64_t purged_size = 0 ;
			while(MA_ERROR_NO_MORE_ITEMS != ma_db_recordset_next(db_record) && purged_size < purge_size) {
				char *hash = NULL ;
				char *file_path = NULL ;
				ma_int64_t content_size = 0 ;

				(void)ma_db_recordset_get_string(db_record, 1, &hash) ;
				(void)ma_db_recordset_get_string(db_record, 2, &file_path) ;
				(void)ma_db_recordset_get_long(db_record, 3, &content_size) ;

				if(ma_fs_remove_file(loop, file_path)) {
					if(MA_OK == ma_p2p_db_remove_content_by_hash(db_handle, hash)) {
						MA_LOG(logger, MA_LOG_SEV_DEBUG, "Purged content(hash = %s, size = %lld)", hash, content_size) ;
						purged_size += content_size ;
					}
				}
				else {
					uv_err_t uv_err = uv_last_error(loop) ;
					MA_LOG(logger, MA_LOG_SEV_WARNING, "Remove file(hash = %s, urn = %s) failed - libuv err: (\'%s\' %s)", hash, file_path, uv_strerror(uv_err),  uv_err_name(uv_err)) ;
				}
			}
			MA_LOG(logger, MA_LOG_SEV_DEBUG, "Total purged size %lld", purged_size) ;
			(void)ma_db_recordset_release(db_record) ;	db_record = NULL ;
			(void)ma_p2p_db_unset_content_to_purge(db_handle) ;
		}
		else {
			MA_LOG(logger, MA_LOG_SEV_DEBUG, "No content eligible/selected to purge, rc = <%d>.", rc) ;
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_p2p_content_manager_release(ma_p2p_content_manager_t *self) {
    if(self) {
		self->data = NULL ;

		free(self) ;
	}
    return MA_OK  ;
}


