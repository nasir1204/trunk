#ifndef MA_P2P_SERVICE_INTERNAL_H_INCLUDED
#define MA_P2P_SERVICE_INTERNAL_H_INCLUDED

#include "ma/internal/services/p2p/ma_p2p_service.h"
#include "ma/internal/services/p2p/ma_p2p_policy.h"
#include "ma/internal/services/p2p/ma_p2p_content_manager.h"
#include "ma/internal/services/p2p/ma_p2p_content.h"

#include "ma/internal/utils/threading/ma_atomic.h"
#include "ma/internal/services/ma_service.h"
#include "ma/ma_msgbus.h"
#include "ma/internal/ma_macros.h"

#include "ma/internal/services/udp_server/ma_udp_msg_handler.h"

#define LOG_FACILITY_NAME "p2p_service"

MA_CPP(extern "C" {)

struct ma_p2p_service_s {
    ma_service_t                    base ;
	ma_context_t                    *context ;

    ma_msgbus_server_t              *msgbus_server ;

    ma_p2p_policy_t                 *p2p_policy ;
    ma_p2p_content_manager_t        *p2p_content_manger ;
} ;

struct ma_p2p_policy_s {
	ma_bool_t		enable_serving ;
	ma_uint32_t		purge_method ;
	ma_uint32_t		hit_count ;
	ma_uint32_t		disk_quota ;
	ma_uint32_t		content_longevity ;
	ma_bool_t		enable_client ;
	char			*repo_path ;
    void			*data ;
} ;

struct ma_p2p_content_manager_s {
	char		p2p_content_dir[MA_MAX_PATH_LEN] ;		
	void		*data ;
} ;

struct ma_p2p_content_s {
	/* DB attributes */
    char *hash ;
	char *content_type ;
	char *base_dir ;
	char *urn ;
	size_t content_size ;
	char *contributor ;
	ma_bool_t owned ;

	/*user content format info */
    ma_p2p_content_format_t content_format ;
    ma_buffer_t *buffer ;
	
	ma_p2p_content_add_type_t  file_op ;
    /*internal*/
    ma_uint32_t hit_count ;
    char *time_last_access ;
    char *time_created ;
} ;

MA_CPP(})

#endif  /* MA_P2P_SERVICE_INTERNAL_H_INCLUDED */

