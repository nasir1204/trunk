#include "ma/internal/services/consignment/ma_consignment_service.h"
#include "ma/internal/services/booking/ma_booking_service.h"
#include "ma/internal/utils/context/ma_context.h"
#include "ma/internal/utils/consignor/ma_consignor.h"
#include "ma/internal/utils/consignor/ma_consign_info.h"
#include "ma/internal/utils/booker/ma_booking_info.h"
#include "ma/internal/services/scheduler/ma_task_defs.h"
#include "ma/internal/services/scheduler/ma_task_internal.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/defs/ma_consign_defs.h"
#include "ma/internal/services/ma_service.h"
#include "ma/ma_log.h"
#include "ma/ma_message.h"
#include "ma/internal/utils/booker/ma_booker.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/ma_variant.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/sensor/ma_sensor_msg.h"
#include "ma/sensor/ma_sensor_utils.h"
#include "ma/ma_message.h"
#include "ma/internal/ma_strdef.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

const char base_path[] = "/consignment/";

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "consignment service"

MA_CPP(extern "C" {)
MA_CPP(})

ma_logger_t *logger = NULL;

struct ma_consignment_service_s {
    ma_service_t            service_base;
    ma_context_t            *context;
    ma_msgbus_server_t      *server;
    ma_logger_t             *logger;
    ma_msgbus_subscriber_t  *subscriber;
    ma_consignor_t           *consignor;
    ma_msgbus_t             *msgbus;
};

static ma_consignment_service_t *gconsignment;

static ma_error_t consignment_subscriber_cb(const char *topic, ma_message_t *message, void *cb_data);
static ma_error_t ma_consignment_service_cb(ma_msgbus_server_t *server, ma_message_t *msg, void *cb_data, ma_msgbus_client_request_t *c_request);

static ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint);
static ma_error_t service_start(ma_service_t *service);
static ma_error_t service_stop(ma_service_t *service);
static void service_release(ma_service_t *service);

static const ma_service_methods_t service_methods = {
    &service_configure,
    &service_start,
    &service_stop,
    &service_release
};

ma_error_t ma_consignment_service_create(const char *service_name, ma_service_t **service) {
    if(service_name && service) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;

        if((*service = (ma_service_t*)calloc(1, sizeof(ma_consignment_service_t)))) {
            ma_service_init(*service, &service_methods, service_name, service);
            return MA_OK;
        }
        return err ;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_consignment_service_release(ma_consignment_service_t *self) {
    if(self) {
        ma_error_t err = MA_OK;
        if(self->server && (MA_OK != (err = ma_msgbus_server_release(self->server)))) {
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "msgbus server release failed");
        }
        (void)ma_msgbus_subscriber_release(self->subscriber);
        (void)ma_consignor_release(self->consignor);
        free(self) ;
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_bool_t check_context_validity(ma_context_t *context){	
    /*Will see if we need this*/
    return MA_TRUE;
}

ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint) {
    if(service && context) {
        ma_error_t rc = MA_OK;
        if(MA_SERVICE_CONFIG_NEW == hint && check_context_validity(context)) {
            ma_configurator_t *configurator = MA_CONTEXT_GET_CONFIGURATOR(context);
            ma_consignment_service_t *self = (ma_consignment_service_t *)service;
            ma_msgbus_t *msgbus = MA_CONTEXT_GET_MSGBUS(context);

            gconsignment = self;
            self->context = context;
            logger = self->logger = MA_CONTEXT_GET_LOGGER(context);
            self->msgbus = msgbus;

            (void)ma_consignor_set_logger(self->logger);
            if(MA_OK == (rc = ma_consignor_create(msgbus, ma_configurator_get_consignment_datastore(configurator), MA_CONSIGNOR_BASE_PATH_STR, &self->consignor))) {
                MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "consignor configured successfully");
                if(MA_OK == (rc = ma_msgbus_subscriber_create(MA_CONTEXT_GET_MSGBUS(context), &self->subscriber))) {
                    (void)ma_msgbus_subscriber_setopt(self->subscriber, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                    (void)ma_msgbus_subscriber_setopt(self->subscriber, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
                    if(MA_OK == (rc = ma_msgbus_server_create(msgbus, MA_CONSIGN_SERVICE_NAME_STR, &self->server))) {                        
                        (void)ma_msgbus_server_setopt(self->server, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
                        if(MA_OK == (rc = ma_msgbus_server_setopt(self->server, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC))) {
                            /*TODO - No need to add the consignment object in the context if we can use the consignment client API's */
                            ma_context_add_object_info(context, MA_OBJECT_CONSIGNOR_NAME_STR, (void *const *)&self->consignor);
                            MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_DEBUG, "consignment service configuration successful");
                            return MA_OK;
                        }
                        ma_msgbus_server_release(self->server);
                    }
                    ma_msgbus_subscriber_release(self->subscriber);
                }
            }
            else
                MA_LOG(self->logger, MA_LOG_SEV_ERROR, "consignor creation failed, last error(%d)", rc);
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t service_start(ma_service_t *service) {
    if(service) {
        ma_error_t err = MA_OK;
        ma_consignment_service_t *self = (ma_consignment_service_t *)service ;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);
        
        if(MA_OK == (err = ma_consignor_start(self->consignor))) {
            if(MA_OK == (err = ma_msgbus_server_start(((ma_consignment_service_t*)service)->server, ma_consignment_service_cb, service))) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "consignment service started successfully");
                //(void)ma_consignment_service_add_admin(self);
                if(MA_OK == (err = ma_msgbus_subscriber_register(((ma_consignment_service_t*)service)->subscriber,  "ma.task.*", consignment_subscriber_cb, service))) {
                    MA_LOG(((ma_consignment_service_t*)service)->logger, MA_LOG_SEV_DEBUG, "consignment subscriber registered successfully");
                    return MA_OK ;
                } 
                else 
                    MA_LOG(((ma_consignment_service_t*)service)->logger, MA_LOG_SEV_ERROR, "consignment service failed to subscribe topic, errocode(%d)", err);
                ma_msgbus_server_stop(((ma_consignment_service_t*)service)->server);
            }
            else
                MA_LOG(logger, MA_LOG_SEV_CRITICAL, "consignment service start failed, last error(%d)", err);
            (void)ma_consignor_stop(self->consignor);
        }
        else
            MA_LOG(logger, MA_LOG_SEV_CRITICAL, "consignment service start failed, last error(%d)", err);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_consignment_service_get_msgbus(ma_consignment_service_t *service, ma_msgbus_t **msgbus) {
    if(service && msgbus) {
        *msgbus = service->msgbus;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_consignment_service_get_version(ma_uint32_t *version) {
    if(version) {
        *version = 0x05020100; //TODO fetch version 
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_consignment_service_start(ma_consignment_service_t *service) {
    return service_start((ma_service_t*)service);
}

ma_error_t ma_consignment_service_stop(ma_consignment_service_t *service) {
    return service_stop((ma_service_t*)service);
}

ma_error_t service_stop(ma_service_t *service) {
    if(service) {
        ma_error_t err = MA_OK;
        ma_consignment_service_t *self = (ma_consignment_service_t *)service;

        if(MA_OK != (err = ma_msgbus_subscriber_unregister(((ma_consignment_service_t*)service)->subscriber))) {
            MA_LOG(((ma_consignment_service_t*)service)->logger, MA_LOG_SEV_ERROR, "consignment service unregistering subscriber failed, last error(%d)", err);
        }

        if(MA_OK == (err = ma_msgbus_server_stop(self->server))) {
            if(MA_OK != (err = ma_consignor_stop(self->consignor))) 
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_CRITICAL, "consignor stop failed, last error(%d)", err);
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO, "consignment service stopped successfully");
        }
        else
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_CRITICAL, "consignment service stop failed, last error(%d)", err);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

void service_release(ma_service_t *service) {
    if(service) {
        ma_consignment_service_release((ma_consignment_service_t *)service) ;
        return ;
    }
    return ;
}

static ma_error_t consignment_register_handler(ma_consignment_service_t *service, ma_message_t *consignment_req_msg, ma_message_t *consignment_rsp_msg) {
    if(service && consignment_req_msg && consignment_rsp_msg) {
        ma_error_t err = MA_OK;
        const char *consignment_id = NULL;
        char *consignment_reg_status = MA_TASK_REPLY_STATUS_FAILED;
        ma_consign_info_t *info = NULL;
        ma_variant_t *consignment_variant = NULL;
        char error_code[MA_MAX_LEN] = {0};


        (void)ma_message_set_property(consignment_rsp_msg, MA_CONSIGN_ID, consignment_id);
        if(MA_OK == (err = ma_message_get_property(consignment_req_msg, MA_CONSIGN_ID, &consignment_id))) {
                consignment_reg_status = MA_CONSIGN_ID_NOT_EXIST;
                if(MA_OK == (err = ma_consignor_get(service->consignor, consignment_id, &info))) {
                    consignment_reg_status = MA_CONSIGN_ID_EXIST;
                    MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "consignment id(%s) already exist!", consignment_id);
                    (void)ma_consign_info_release(info);
                } else {
                   if(MA_OK == ma_message_get_payload(consignment_req_msg, &consignment_variant)) {
                       if(MA_OK == (err = ma_consignor_add_variant(service->consignor, consignment_id, consignment_variant))) {
                           MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "consignment id(%s) written into DB successfully", consignment_id);
                           consignment_reg_status = MA_CONSIGN_ID_WRITTEN;
                       }
                       (void)ma_variant_release(consignment_variant);
                   }
                }
        }
        if(MA_OK != err)
            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_CRITICAL, "consignment id register failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(consignment_rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(consignment_rsp_msg, MA_CONSIGN_STATUS, consignment_reg_status);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static void foreach(const char *key, ma_variant_t *value, void *cb_data, ma_bool_t *stop_loop) {
    if(key && value && cb_data && stop_loop) {
        ma_uint64_t *dist = (ma_uint64_t*)cb_data;
        ma_error_t err = MA_OK;
        ma_booking_info_t *info = NULL;
        ma_booker_t *booker = MA_CONTEXT_GET_BOOKER(gconsignment->context);

        if(MA_OK == (err = ma_booker_get(booker, key, &info))) {
            ma_uint32_t ran = 0;

            (void)ma_booking_info_get_distance(info, &ran);
            *dist += ran;
            (void)ma_booking_info_release(info);
        }
    }
}

static ma_error_t consignment_ran_distance_handler(ma_consignment_service_t *service, ma_message_t *consignment_req_msg, ma_message_t *consignment_rsp_msg) {
    if(service && consignment_req_msg && consignment_rsp_msg) {
        ma_error_t err = MA_OK;
        const char *consignment_id = NULL;
        char *consignment_reg_status = MA_CONSIGN_ID_NOT_EXIST;
        ma_consign_info_t *info = NULL;
        ma_variant_t *consignment_variant = NULL;
        char error_code[MA_MAX_LEN] = {0};


        if(MA_OK == (err = ma_message_get_property(consignment_req_msg, MA_CONSIGN_ID, &consignment_id))) {
            ma_consign_info_t *pinfo = NULL;
            char buf[MA_MAX_LEN+1] = {0};
            ma_uint64_t dist = 0;

            (void)ma_message_set_property(consignment_rsp_msg, MA_CONSIGN_ID, consignment_id);
            if(MA_OK == (err = ma_consignor_get(service->consignor, consignment_id, &pinfo))) {
                consignment_reg_status = MA_CONSIGN_ID_EXIST;
                ma_table_t *completed_orders = NULL;

                if(MA_OK == (err = ma_consign_info_get_completed_orders_map(pinfo, &completed_orders))) {
                    (void)ma_table_foreach(completed_orders, &foreach, &dist);
                    (void)ma_table_release(completed_orders);
                }
                (void)ma_consign_info_release(pinfo);
            }
            MA_MSC_SELECT(_snprintf, snprintf)(buf, MA_MAX_LEN, "%lu", dist);
            (void)ma_message_set_property(consignment_rsp_msg, MA_CONSIGN_RAN_DISTANCE, buf);
        }
        if(MA_OK != err)
            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_CRITICAL, "consignment id update failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(consignment_rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(consignment_rsp_msg, MA_CONSIGN_STATUS, consignment_reg_status);
        return err = MA_OK; // force reset
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t consignment_update_handler(ma_consignment_service_t *service, ma_message_t *consignment_req_msg, ma_message_t *consignment_rsp_msg) {
    if(service && consignment_req_msg && consignment_rsp_msg) {
        ma_error_t err = MA_OK;
        const char *consignment_id = NULL;
        char *consignment_reg_status = MA_TASK_REPLY_STATUS_FAILED;
        ma_consign_info_t *info = NULL;
        ma_variant_t *consignment_variant = NULL;
        char error_code[MA_MAX_LEN] = {0};


        if(MA_OK == (err = ma_message_get_property(consignment_req_msg, MA_CONSIGN_ID, &consignment_id))) {
            (void)ma_message_set_property(consignment_rsp_msg, MA_CONSIGN_ID, consignment_id);
            consignment_reg_status = MA_CONSIGN_ID_NOT_EXIST;
            if(MA_OK == ma_message_get_payload(consignment_req_msg, &consignment_variant)) {
                if(MA_OK == (err = ma_consign_info_convert_from_variant(consignment_variant, &info))) {
                    ma_consign_info_t *pinfo = NULL;

                    if(MA_OK == (err = ma_consignor_get(service->consignor, consignment_id, &pinfo))) {
                        if(MA_OK == (err = ma_consignor_add(service->consignor, pinfo))) {
                            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "consignment id(%s) updated into DB successfully", consignment_id);
                            consignment_reg_status = MA_CONSIGN_ID_WRITTEN;
                        }
                        (void)ma_consign_info_release(pinfo);
                    }
                    (void)ma_consign_info_release(info);
                }
                (void)ma_variant_release(consignment_variant);
            }
        }
        if(MA_OK != err)
            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_CRITICAL, "consignment id update failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(consignment_rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(consignment_rsp_msg, MA_CONSIGN_STATUS, consignment_reg_status);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t consignment_deregister_handler(ma_consignment_service_t *service, ma_message_t *consignment_req_msg, ma_message_t *consignment_rsp_msg) {
    if(service && consignment_req_msg && consignment_rsp_msg) {
        ma_error_t err = MA_OK;
        const char *consignment_id = NULL;
        char *consignment_reg_status = MA_CONSIGN_ID_NOT_EXIST;
        ma_consign_info_t *info = NULL;
        char error_code[MA_MAX_LEN] = {0};


        (void)ma_message_set_property(consignment_rsp_msg, MA_CONSIGN_ID, consignment_id);
        if(MA_OK == (err = ma_message_get_property(consignment_req_msg, MA_CONSIGN_ID, &consignment_id))) {
            if(MA_OK == (err = ma_consignor_get(service->consignor, consignment_id, &info))) {
                consignment_reg_status = MA_CONSIGN_ID_EXIST;
                if(MA_OK == (err = ma_consignor_delete(service->consignor, consignment_id))) {
                    consignment_reg_status = MA_CONSIGN_ID_DELETED;
                    MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "consignment id(%s) deleted successfully", consignment_id);
                }
                (void)ma_consign_info_release(info);
            }
        }
        if(MA_OK != err)
            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_CRITICAL, "consignment id deregister failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(consignment_rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(consignment_rsp_msg, MA_CONSIGN_STATUS, consignment_reg_status);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t consignment_get_handler(ma_consignment_service_t *service, ma_message_t *consignment_req_msg, ma_message_t *consignment_rsp_msg) {
    if(service && consignment_req_msg && consignment_rsp_msg) {
        ma_error_t err = MA_OK;
        const char *consignment_id = NULL;
        char *consignment_reg_status = MA_CONSIGN_ID_NOT_EXIST;
        ma_consign_info_t *info = NULL;
        ma_variant_t *consignment_variant = NULL;
        char error_code[MA_MAX_LEN] = {0};


        (void)ma_message_set_property(consignment_rsp_msg, MA_CONSIGN_ID, consignment_id);
        if(MA_OK == (err = ma_message_get_property(consignment_req_msg, MA_CONSIGN_ID, &consignment_id))) {
            if(MA_OK == (err = ma_consignor_get(service->consignor, consignment_id, &info))) {
                MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "consignment id(%s) get successfully", consignment_id);
                if(MA_OK == (err = ma_consign_info_convert_to_variant(info, &consignment_variant))) {
                    MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "consignment id(%s) get successfull", consignment_id);
                    consignment_reg_status = MA_CONSIGN_ID_EXIST;
                    (void)ma_message_set_payload(consignment_rsp_msg, consignment_variant);
                    (void)ma_variant_release(consignment_variant);
                }
                (void)ma_consign_info_release(info);
            }
        }
        if(MA_OK != err)
            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_CRITICAL, "consignment id get failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(consignment_rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(consignment_rsp_msg, MA_CONSIGN_STATUS, consignment_reg_status);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t consignment_get_json_handler(ma_consignment_service_t *service, ma_message_t *consign_req_msg, ma_message_t *consign_rsp_msg) {
    if(service && consign_req_msg && consign_rsp_msg) {
        ma_error_t err = MA_OK;
        const char *consign_id = NULL;
        char *consign_reg_status = MA_CONSIGN_ID_NOT_EXIST;
        ma_consign_info_t *info = NULL;
        ma_variant_t *consign_variant = NULL;
        char error_code[MA_MAX_LEN] = {0};


        (void)ma_message_set_property(consign_rsp_msg, MA_CONSIGN_ID, consign_id);
        if(MA_OK == (err = ma_message_get_property(consign_req_msg, MA_CONSIGN_ID, &consign_id))) {
            if(MA_OK == (err = ma_consignor_get(service->consignor, consign_id, &info))) {
                ma_json_t *json = NULL;

                MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "consign id(%s) get successfully", consign_id);
                if(MA_OK == (err = ma_consign_info_convert_to_json(info, &json))) {
                    ma_bytebuffer_t *buffer = NULL;

                    ma_bytebuffer_create(MA_MAX_BUFFER_LEN, &buffer);
                    if(MA_OK == (err = ma_json_get_data(json, buffer))) {
                        if(MA_OK == (err = ma_variant_create_from_string((const char*)ma_bytebuffer_get_bytes(buffer), ma_bytebuffer_get_size(buffer), &consign_variant))) {
                            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "consign json output(%s) len(%u)", ma_bytebuffer_get_bytes(buffer), ma_bytebuffer_get_size(buffer));
                            consign_reg_status = MA_CONSIGN_ID_EXIST;
                            (void)ma_message_set_payload(consign_rsp_msg, consign_variant);
                            (void)ma_variant_release(consign_variant);
                        } else
                            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_ERROR, "consign variant creation failed, last error(%d)", err);
                    }
                    (void)ma_json_release(json);
                }
                (void)ma_consign_info_release(info);
            }
        }
        if(MA_OK != err)
            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_CRITICAL, "consign id get failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(consign_rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(consign_rsp_msg, MA_CONSIGN_STATUS, consign_reg_status);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_consignment_service_cb(ma_msgbus_server_t *server, ma_message_t *msg, void *cb_data, ma_msgbus_client_request_t *c_request) {
    if(server && msg && cb_data) {
        ma_error_t err = MA_OK;
        const char *msg_type = NULL;
        ma_message_t *response = NULL;
        ma_consignment_service_t *service = (ma_consignment_service_t*)cb_data;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(service->context);

        if(MA_OK == (err = ma_message_create(&response))) {
            if(MA_OK == (err = ma_message_get_property(msg, MA_CONSIGN_SERVICE_MSG_TYPE, &msg_type))) {
                if(!strcmp(msg_type, MA_CONSIGN_SERVICE_MSG_REGISTER_TYPE)) {
                    if(MA_OK == (err = consignment_register_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client register id request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_CONSIGN_SERVICE_MSG_UNREGISTER_TYPE)) {
                    if(MA_OK == (err = consignment_deregister_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client deregister request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_CONSIGN_SERVICE_MSG_GET_TYPE)) {
                    if(MA_OK == (err = consignment_get_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client deregister request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_CONSIGN_SERVICE_MSG_UPDATE_TYPE)) {
                    if(MA_OK == (err = consignment_update_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client update request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_CONSIGN_SERVICE_MSG_GET_RAN_DISTANCE_TYPE)) {
                    if(MA_OK == (err = consignment_ran_distance_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client get ran distance request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_CONSIGN_SERVICE_MSG_GET_JSON_TYPE)) {
                    if(MA_OK == (err = consignment_get_json_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client get json request failed, last error(%d)", err);
                        }
                    }
                } else {
                    if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, MA_ERROR_SCHEDULER_INVALID_OP_TYPE, response))) {
                        MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client request failed, last error(%d)", err);
                    }
                }
            } else {
                if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err = MA_ERROR_SCHEDULER_TASK_TYPE_NOT_SET, response))) {
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client request failed, last error(%d)", err);
                }
            }
            if(MA_OK != ma_message_release(response)) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client request message release failed");
            }
        }
        return err;
    }
    return MA_OK;
}

static ma_error_t add_pending_complete(ma_consign_info_t *consign, const char *user_contact, const char *id, ma_bool_t is_complete) {
    if(consign && user_contact && id) {
        ma_error_t err = MA_OK;

        if(!is_complete) {
            if(MA_OK == (err = ma_consign_info_add_pending_order(consign, user_contact, id))) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "consignment user(%s) booking id(%s) added in pending order map", user_contact, id);
            } else {
                MA_LOG(logger, MA_LOG_SEV_ERROR, "consignment user(%s) booking id(%s) added in pending order map failed last error(%d)", user_contact, id, err);
            }
        } else {
            if(MA_OK == (err = ma_consign_info_add_completed_order(consign, user_contact, id))) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "consignment user(%s) booking id(%s) added in completed order map", user_contact, id);
            } else {
                MA_LOG(logger, MA_LOG_SEV_ERROR, "consignment user(%s) booking id(%s) added in completed order map failed last error(%d)", user_contact, id, err);
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t remove_pending_complete(ma_consign_info_t *consign, const char *user_contact, const char *id, ma_bool_t is_complete) {
    if(consign && user_contact && id) {
        ma_error_t err = MA_OK;

        if(!is_complete) {
            if(MA_OK == (err = ma_consign_info_remove_pending_order(consign, user_contact, id))) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "consignment user(%s) booking id(%s) added in pending order map", user_contact, id);
            } else {
                MA_LOG(logger, MA_LOG_SEV_ERROR, "consignment user(%s) booking id(%s) added in pending order map failed last error(%d)", user_contact, id, err);
            }
        } else {
            if(MA_OK == (err = ma_consign_info_remove_completed_order(consign, user_contact, id))) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "consignment user(%s) booking id(%s) added in completed order map", user_contact, id);
            } else {
                MA_LOG(logger, MA_LOG_SEV_ERROR, "consignment user(%s) booking id(%s) added in completed order map failed last error(%d)", user_contact, id, err);
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t consignor_add_record(ma_consignor_t *consignor, const char *contact, const char *booking_id, ma_bool_t is_complete) {
    if(contact && booking_id) {
        ma_error_t err = MA_OK;
        ma_consign_info_t *consign = NULL;

        if(MA_OK == (err = ma_consignor_get(consignor, contact, &consign))) {
            (void)add_pending_complete(consign, contact, booking_id, is_complete);
            err = ma_consignor_add(consignor, consign);
            (void)ma_consign_info_release(consign);
        } else {
            if(MA_OK == (err = ma_consign_info_create(&consign))) {
                (void)ma_consign_info_set_contact(consign, contact);
                (void)add_pending_complete(consign, contact, booking_id, is_complete);
                err = ma_consignor_add(consignor, consign);
                (void)ma_consign_info_release(consign);
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t consignor_remove_record(ma_consignor_t *consignor, const char *contact, const char *booking_id, ma_bool_t is_complete) {
    if(contact && booking_id) {
        ma_error_t err = MA_OK;
        ma_consign_info_t *consign = NULL;

        if(MA_OK == (err = ma_consignor_get(consignor, contact, &consign))) {
            (void)remove_pending_complete(consign, contact, booking_id, is_complete);
            err = ma_consignor_add(consignor, consign);
            (void)ma_consign_info_release(consign);
        } else {
            if(MA_OK == (err = ma_consign_info_create(&consign))) {
                (void)ma_consign_info_set_contact(consign, contact);
                (void)remove_pending_complete(consign, contact, booking_id, is_complete);
                err = ma_consignor_add(consignor, consign);
                (void)ma_consign_info_release(consign);
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}
static ma_error_t consignment_save_handler(ma_consignment_service_t *service, const char *booking_id) {
    if(service && booking_id) {
        ma_error_t err = MA_OK;
        ma_context_t *context = service->context;
        ma_scheduler_t *scheduler = MA_CONTEXT_GET_SCHEDULER(context) ;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(context);
        ma_consignor_t *consignor = MA_CONTEXT_GET_CONSIGNOR(context);
        ma_booker_t *booker = MA_CONTEXT_GET_BOOKER(context);
        ma_task_t *task = NULL;
        const char *task_id = NULL;

        if(MA_OK == (err = ma_scheduler_get_task_handle(scheduler, booking_id, &task))) {
            ma_variant_t *payload = NULL;

            (void)ma_task_get_id(task, &task_id);
            MA_LOG(logger, MA_LOG_SEV_TRACE, "Booking Task id(%s) exist in scheduler", booking_id);
            if(MA_OK == (err = ma_task_get_app_payload(task, &payload)) && payload) {
                ma_booking_info_t *info = NULL;
                
                if(MA_OK == (err = ma_booker_get(booker, booking_id, &info))) {
                    char id[MA_MAX_LEN+1] = {0};
                    char picker_contact[MA_MAX_LEN+1] = {0};
                    ma_booking_info_status_t status = MA_BOOKING_INFO_STATUS_NOT_ASSIGNED;
                    char user_contact[MA_MAX_LEN+1] = {0};

                    (void)ma_booking_info_get_status(info, &status);
                    (void)ma_booking_info_get_contact(info, user_contact);
                    (void)ma_booking_info_get_id(info, id);
                    (void)ma_booking_info_get_picker_contact(info, picker_contact);
                    if(MA_BOOKING_INFO_STATUS_NOT_ASSIGNED == status) {
                       if(MA_OK == (err = consignor_add_record(consignor, user_contact, id, MA_FALSE))) {
                           MA_LOG(logger, MA_LOG_SEV_DEBUG, "consign add record contact(%s) id(%s)", user_contact, id);
                       } else {
                           MA_LOG(logger, MA_LOG_SEV_ERROR, "consign add record contact(%s) id(%s) failed last error(%d)", user_contact, id, err);
                       }
                    } else if(MA_BOOKING_INFO_STATUS_ASSIGNED == status) {
                       if(MA_OK == (err = consignor_add_record(consignor, user_contact, id, MA_FALSE))) {
                           MA_LOG(logger, MA_LOG_SEV_DEBUG, "consign add record contact(%s) id(%s)", user_contact, id);
                       } else {
                           MA_LOG(logger, MA_LOG_SEV_ERROR, "consign add record contact(%s) id(%s) failed last error(%d)", user_contact, id, err);
                       }
                       if(MA_OK == (err = consignor_add_record(consignor, picker_contact, id, MA_FALSE))) {
                           MA_LOG(logger, MA_LOG_SEV_DEBUG, "consign add record contact(%s) id(%s)", picker_contact, id);
                       } else {
                           MA_LOG(logger, MA_LOG_SEV_ERROR, "consign add record contact(%s) id(%s) failed last error(%d)", picker_contact, id, err);
                       }
                    } else if(MA_BOOKING_INFO_STATUS_DELIVERED == status ||
                              MA_BOOKING_INFO_STATUS_CANCELLED == status ||
                              MA_BOOKING_INFO_STATUS_NOT_DELIVERED == status) {
                        if(MA_OK == (err = consignor_remove_record(consignor, user_contact, id, MA_FALSE))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "consign add record contact(%s) id(%s)", user_contact, id);
                        } else {
                            MA_LOG(logger, MA_LOG_SEV_ERROR, "consign add record contact(%s) id(%s) failed last error(%d)", user_contact, id, err);
                        }
                        if(MA_OK == (err = consignor_add_record(consignor, user_contact, id, MA_TRUE))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "consign add record contact(%s) id(%s)", user_contact, id);
                        } else {
                            MA_LOG(logger, MA_LOG_SEV_ERROR, "consign add record contact(%s) id(%s) failed last error(%d)", user_contact, id, err);
                        }
                        if(MA_OK == (err = consignor_remove_record(consignor, picker_contact, id, MA_FALSE))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "consign add record contact(%s) id(%s)", picker_contact, id);
                        } else {
                            MA_LOG(logger, MA_LOG_SEV_ERROR, "consign add record contact(%s) id(%s) failed last error(%d)", picker_contact, id, err);
                        }
                        if(MA_OK == (err = consignor_add_record(consignor, picker_contact, id, MA_TRUE))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "consign add record contact(%s) id(%s)", picker_contact, id);
                        } else {
                            MA_LOG(logger, MA_LOG_SEV_ERROR, "consign add record contact(%s) id(%s) failed last error(%d)", picker_contact, id, err);
                        }
                        (void)ma_booking_info_set_active(info, MA_FALSE);
                        if(MA_OK == (err = ma_booker_add(booker, info))) {
                            ma_variant_t *v = NULL;

                            if(MA_OK == (err = ma_booking_info_convert_to_variant(info, &v))) {
                                (void)ma_task_set_app_payload(task, v);
                                (void)ma_variant_release(v);
                            }
                            MA_LOG(service->logger, MA_LOG_SEV_TRACE, "updating booking status(ASSIGNED)");
                        } else {
                            MA_LOG(service->logger, MA_LOG_SEV_TRACE, "updating booking status(ASSIGNED) failed last error(%d)", err);
                        }
                    } else if(MA_BOOKING_INFO_STATUS_PICKED == status) {
                        if(MA_OK == (err = consignor_add_record(consignor, picker_contact, id, MA_FALSE))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "consign add record contact(%s) id(%s)", picker_contact, id);
                        } else {
                            MA_LOG(logger, MA_LOG_SEV_ERROR, "consign add record contact(%s) id(%s) failed last error(%d)", picker_contact, id, err);
                        }
                    } else {
                    }
                    (void)ma_booking_info_release(info);
                }
                (void)ma_variant_release(payload);
            }
            (void)ma_task_release(task);
        }
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_bool_t check_booking_still_active(ma_booker_t *service, const char *booking_id) {
    if(service && booking_id) {
        ma_error_t err = MA_OK;
        ma_bool_t active = MA_FALSE;
        ma_booking_info_t *info = NULL;

        if(MA_OK == (err = ma_booker_get(service, booking_id, &info))) {
            (void)ma_booking_info_get_active(info, &active);
            (void)ma_booking_info_release(info);
        }
        return active;
    }
    return MA_FALSE;
}

ma_error_t consignment_subscriber_cb(const char *topic, ma_message_t *msg, void *cb_data) {
    if(topic && msg && cb_data) {
        ma_error_t err = MA_OK;
        ma_consignment_service_t *service = (ma_consignment_service_t*)cb_data;
        //ma_consignment_t *consignment = service->consignment;

        MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "consignment subscriber received sensor message(%s)", topic);
        if(!strcmp(topic, MA_TASK_EXECUTE_PUBSUB_TOPIC)) {
            const char *task_type = NULL , *task_owner_id = NULL , *task_id = NULL;

            MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "consignment service processing (%s) message", topic);
            if( (MA_OK == (err = ma_message_get_property(msg, MA_TASK_TYPE, &task_type))) && task_type &&
                (MA_OK == (err = ma_message_get_property(msg, MA_TASK_PRODUCT_ID, &task_owner_id))) && task_owner_id &&
                (MA_OK == (err = ma_message_get_property(msg, MA_TASK_ID, &task_id))) && task_id) {
                if(!strcmp(task_type, MA_BOOKING_TASK_TYPE)) {
                    ma_bool_t active = check_booking_still_active(MA_CONTEXT_GET_BOOKER(service->context), task_id);
                    if(active) {
                        MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "booking task id(%s) processing request received", task_id);
                        if(MA_OK == (err = consignment_save_handler(service, task_id))) {
                            MA_LOG(service->logger, MA_LOG_SEV_TRACE, "consignment service processing (%s) message", topic);
                        } else {
                            MA_LOG(service->logger, MA_LOG_SEV_ERROR, "consignment service processing (%s) message failed last error(%d)", topic, err);
                        }
                    } else {
                        MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "booking task id(%s) inactive ", task_id);
                    }
                } else
                    MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "task id(%s) is not booking task", task_id);
            }
        } else {
            /* do nothing */
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_consignment_service_get_consignor(ma_consignment_service_t *service, ma_consignor_t **consignor) {
    if(service && consignor) {
        *consignor = service->consignor;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


