#include "ma/internal/services/crypto/ma_crypto_service.h"
#include "ma/internal/services/ma_service.h"
#include "ma/internal/utils/macrypto/ma_crypto.h"
#include "ma/internal/utils/datastructures/ma_bytebuffer.h"
#include "ma/crypto/ma_crypto_defs.h"
#include "ma/ma_log.h"
#include "ma/ma_buffer.h"
#include "ma/ma_variant.h"

#include <stdlib.h>
#include <string.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "crypto service"

struct ma_crypto_service_s {
    ma_service_t        service_base;
    ma_msgbus_server_t  *server;
    ma_crypto_t         *crypto;
    ma_context_t        *context;
    ma_logger_t         *logger;
};

ma_logger_t *crypto_logger;

static ma_error_t ma_crypto_service_cb(ma_msgbus_server_t *server, ma_message_t *msg, void *cb_data, ma_msgbus_client_request_t *c_request);
static ma_error_t service_start(ma_service_t *service);
static ma_error_t service_stop(ma_service_t *service);
static ma_error_t service_release(ma_service_t *service);

/* crypto handler(s) */
static ma_error_t crypto_service_encrypt_handler(ma_crypto_service_t *service, ma_message_t *req, ma_message_t *rsp);
static ma_error_t crypto_service_decrypt_handler(ma_crypto_service_t *service, ma_message_t *req, ma_message_t *rsp);
static ma_error_t crypto_service_encode_handler(ma_crypto_service_t *service, ma_message_t *req, ma_message_t *rsp);
static ma_error_t crypto_service_decode_handler(ma_crypto_service_t *service, ma_message_t *req, ma_message_t *rsp);
static ma_error_t crypto_service_hash_handler(ma_crypto_service_t *service, ma_message_t *req, ma_message_t *rsp);
static ma_error_t crypto_service_encrypt_encode_handler(ma_crypto_service_t *service, ma_message_t *req, ma_message_t *rsp);
static ma_error_t crypto_service_decode_decrypt_handler(ma_crypto_service_t *service, ma_message_t *req, ma_message_t *rsp);

static const ma_service_methods_t service_methods = {
    &service_start,
    &service_stop,
    &service_release
};

ma_error_t ma_crypto_service_create(ma_context_t *context, ma_crypto_service_t **service) {
    if(context && service) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;

        if((*service = (ma_crypto_service_t*)calloc(1, sizeof(ma_crypto_service_t)))) {
            crypto_logger = (*service)->logger =  MA_CONTEXT_GET_LOGGER(context);
            if(MA_OK == (err = ma_msgbus_server_create(MA_CONTEXT_GET_MSGBUS(context), MA_CRYPTO_SERVICE_NAME_STR, &(*service)->server))) {
                if(MA_OK == (err = ma_msgbus_server_setopt((*service)->server, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC))) {
                    ma_service_init((ma_service_t*)*service, &service_methods, context, MA_CRYPTO_SERVICE_NAME_STR, service);
                    (*service)->crypto = MA_CONTEXT_GET_CRYPTO(context);
//                    (void)ma_crypto_set_logger((*service)->logger);
                    (*service)->context = context;
                    MA_LOG((*service)->logger, MA_LOG_SEV_DEBUG, "crypto service(%s) creation successful", MA_CRYPTO_SERVICE_NAME_STR);
                }
            }
        }
        if((MA_OK != err) && *service) {
            MA_LOG((*service)->logger, MA_LOG_SEV_DEBUG, "crypto service(%s) creation failed, errorcode(%d)", MA_CRYPTO_SERVICE_NAME_STR, err);
//            (void)ma_crypto_service_release(*service);
            *service = NULL;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_crypto_service_release(ma_crypto_service_t *service) {
    if(service) {
        ma_error_t err = MA_OK;

        if(MA_OK != (err = ma_service_release((ma_service_t*)service))) {
            MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "ma service(%s) release failed", MA_CRYPTO_SERVICE_NAME_STR);
        }
        if(service->server && (MA_OK != (err = ma_msgbus_server_release(service->server)))) {
            MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "msgbus server release failed");
        }
        if(MA_OK != err)MA_LOG(service->logger, MA_LOG_SEV_ERROR, "crypto service(%s) release failed, errorcode(%d)", MA_CRYPTO_SERVICE_NAME_STR, err);
        free(service);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t service_start(ma_service_t *service) {
    if(service) {
        ma_error_t err = MA_OK;

        if(MA_OK == (err = ma_msgbus_server_setopt( ((ma_crypto_service_t*)service)->server, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO))) {
            if(MA_OK == (err = ma_msgbus_server_start(((ma_crypto_service_t*)service)->server, ma_crypto_service_cb, service))) {
                MA_LOG(((ma_crypto_service_t*)service)->logger, MA_LOG_SEV_CRITICAL, "crypto service(%s) started successfully", MA_CRYPTO_SERVICE_NAME_STR);
            }
        }
        if(MA_OK != err) {
            MA_LOG(((ma_crypto_service_t*)service)->logger, MA_LOG_SEV_CRITICAL, "crypto service(%s) start failed, errorcode(%d)", MA_CRYPTO_SERVICE_NAME_STR, err);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_crypto_service_get_version(ma_uint32_t *version) {
    if(version) {
        *version = 0x05020100; //TODO fetch version 
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_crypto_service_start(ma_crypto_service_t *service) {
    return service_start((ma_service_t*)service);
}

ma_error_t ma_crypto_service_stop(ma_crypto_service_t *service) {
    return service_stop((ma_service_t*)service);
}

ma_error_t service_stop(ma_service_t *service) {
    if(service) {
        ma_error_t err = MA_OK;

        if(MA_OK == (err = ma_msgbus_server_stop(((ma_crypto_service_t*)service)->server))) {
            MA_LOG(((ma_crypto_service_t*)service)->logger, MA_LOG_SEV_CRITICAL, "crypto service(%s) stopped successfully", MA_CRYPTO_SERVICE_NAME_STR);
        }
        if(MA_OK != err) {
            MA_LOG(((ma_crypto_service_t*)service)->logger, MA_LOG_SEV_CRITICAL, "crypto service(%s) failed to stop, errorcode(%d)", MA_CRYPTO_SERVICE_NAME_STR, err);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t service_release(ma_service_t *service) {
    if(service) {
        if(service->service_name)free(service->service_name);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}



// crypto message handler(s)
ma_error_t crypto_service_encrypt_handler(ma_crypto_service_t *service, ma_message_t *req, ma_message_t *rsp) {
    if(service && req && rsp) {
        ma_error_t err = MA_OK;
        const char *status = MA_CRYPTO_REPLY_STATUS_FAILED;
        const char *algo = NULL;

        if(MA_OK == (err = ma_message_get_property(req, MA_CRYPTO_ATTR_ALGO_ID, &algo))) {
            int algo_id = atoi(algo);

            if(algo_id >= 0) {
                ma_variant_t *payload = NULL;

                if(MA_OK == (err = ma_message_get_payload(req, &payload))) {
                    ma_buffer_t *buf = NULL;

                    if(MA_OK == (err = ma_variant_get_raw_buffer(payload, &buf))) {
                        const unsigned char *raw = NULL;
                        size_t len = 0;

                        if(MA_OK == (err = ma_buffer_get_raw(buf, &raw, &len))) {
                            ma_bytebuffer_t *byte_buf = NULL;
                        }
                        (void)ma_buffer_release(buf);
                    }
                    (void)ma_variant_release(payload);
                }
            }
        }

        return ma_message_set_property(rsp, MA_CRYPTO_REPLY_STATUS, status);
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t crypto_service_decrypt_handler(ma_crypto_service_t *service, ma_message_t *req, ma_message_t *rsp) {
    if(service && req && rsp) {
        ma_error_t err = MA_OK;
        const char *status = MA_CRYPTO_REPLY_STATUS_FAILED;
        ma_variant_t *var = NULL;



        return ma_message_set_property(rsp, MA_CRYPTO_REPLY_STATUS, status);
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t crypto_service_encode_handler(ma_crypto_service_t *service, ma_message_t *req, ma_message_t *rsp) {
    if(service && req && rsp) {
        ma_error_t err = MA_OK;
        const char *status = MA_CRYPTO_REPLY_STATUS_FAILED;
        ma_variant_t *var = NULL;

        return ma_message_set_property(rsp, MA_CRYPTO_REPLY_STATUS, status);
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t crypto_service_decode_handler(ma_crypto_service_t *service, ma_message_t *req, ma_message_t *rsp) {
    if(service && req && rsp) {
        ma_error_t err = MA_OK;
        const char *status = MA_CRYPTO_REPLY_STATUS_FAILED;
        ma_variant_t *var = NULL;

        return ma_message_set_property(rsp, MA_CRYPTO_REPLY_STATUS, status);
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t crypto_service_hash_handler(ma_crypto_service_t *service, ma_message_t *req, ma_message_t *rsp) {
    if(service && req && rsp) {
        ma_error_t err = MA_OK;
        const char *status = MA_CRYPTO_REPLY_STATUS_FAILED;
        ma_variant_t *var = NULL;

        return ma_message_set_property(rsp, MA_CRYPTO_REPLY_STATUS, status);
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t crypto_service_encrypt_encode_handler(ma_crypto_service_t *service, ma_message_t *req, ma_message_t *rsp) {
    if(service && req && rsp) {
        ma_error_t err = MA_OK;
        const char *status = MA_CRYPTO_REPLY_STATUS_FAILED;
        ma_variant_t *var = NULL;

        return ma_message_set_property(rsp, MA_CRYPTO_REPLY_STATUS, status);
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t crypto_service_decode_decrypt_handler(ma_crypto_service_t *service, ma_message_t *req, ma_message_t *rsp) {
    if(service && req && rsp) {
        ma_error_t err = MA_OK;
        const char *status = MA_CRYPTO_REPLY_STATUS_FAILED;
        ma_variant_t *var = NULL;

        return ma_message_set_property(rsp, MA_CRYPTO_REPLY_STATUS, status);
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_crypto_service_cb(ma_msgbus_server_t *server, ma_message_t *msg, void *cb_data, ma_msgbus_client_request_t *c_request) {
    if(server && msg && cb_data) {
        ma_error_t err = MA_OK;
        const char *msg_type = NULL;
        ma_message_t *response = NULL;
        ma_crypto_service_t *service = (ma_crypto_service_t*)cb_data;

        if(MA_OK == (err = ma_message_create(&response))) {
            if(MA_OK == (err = ma_message_get_property(msg, MA_CRYPTO_SERVICE_MSG_TYPE, &msg_type))) {
                if(!strcmp(msg_type, MA_CRYPTO_MSG_TYPE_ENCRYPT)) {
                    if(MA_OK == (err = crypto_service_encrypt_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "msgbus posting client encrypt request failed, errorcode(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_CRYPTO_MSG_TYPE_DECRYPT)) {
                    if(MA_OK == (err = crypto_service_decrypt_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "msgbus posting client decrypt request failed, errorcode(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_CRYPTO_MSG_TYPE_ENCODE)) {
                    if(MA_OK == (err = crypto_service_encode_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "msgbus posting client encode request failed, errorcode(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_CRYPTO_MSG_TYPE_DECODE)) {
                    if(MA_OK == (err = crypto_service_decode_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "msgbus posting client decode request failed, errorcode(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_CRYPTO_MSG_TYPE_HASH)) {
                    if(MA_OK == (err = crypto_service_hash_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "msgbus posting client hash request failed, errorcode(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_CRYPTO_MSG_TYPE_ENCRYPT_AND_ENCODE)) {
                    if(MA_OK == (err = crypto_service_encrypt_encode_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "msgbus posting client encrypt encode request failed, errorcode(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_CRYPTO_MSG_TYPE_DECODE_AND_DECRYPT)) {
                    if(MA_OK == (err = crypto_service_decode_decrypt_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "msgbus posting client decode decrypt request failed, errorcode(%d)", err);
                        }
                    }
                } else {
                    if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, MA_ERROR_SCHEDULER_INVALID_OP_TYPE, response))) {
                        MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "msgbus posting client request failed, errorcode(%d)", err);
                    }
                }
            } else {
                if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err = MA_ERROR_SCHEDULER_TASK_TYPE_NOT_SET, response))) {
                    MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "msgbus posting client request failed, errorcode(%d)", err);
                }
            }
            if(MA_OK != ma_message_release(response)) {
                MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "msgbus posting client request message release failed");
            }
        }
        return err;
    }
    return MA_OK;
}


