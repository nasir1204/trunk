#include "ma/internal/services/scheduler/ma_scheduler_core.h"
#include "ma/internal/services/scheduler/ma_taskfolder.h"
#include "ma/scheduler/ma_enumerator.h"
#include "ma/internal/services/scheduler/ma_runningtask_collector.h"
#include "ma/datastore/ma_ds_database.h"
#include "ma/logger/ma_file_logger.h"
#include "ma/internal/ma_macros.h"
#include "ma/ma_message.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/scheduler/ma_triggers.h"
#include "ma/internal/services/scheduler/ma_trigger_core.h"
#include "ma/scheduler/ma_task.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"
#include "ma/internal/services/scheduler/ma_task_internal.h"
#include "ma/internal/services/scheduler/ma_triggers_internal.h"
#include "ma/internal/ma_macros.h"
#include "ma/logger/ma_file_logger.h"
#include "ma/sensor/ma_sensor_msg_utils.h"
#include "ma/sensor/ma_sensor_msg.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef MA_WINDOWS
#include<Windows.h>
#else
#include<unistd.h>
#endif

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "scheduler"

static const char *gzone_ptr;
extern ma_logger_t *logger;

MA_CPP(extern "C" {)
ma_error_t ma_scheduler_get_msgbus(ma_scheduler_t *scheduler, ma_msgbus_t **msgbus);
ma_error_t ma_scheduler_internal_release_task(ma_scheduler_t *scheduler, ma_task_t *task);
ma_error_t ma_task_get_missed_interval(ma_task_t *task, ma_uint16_t *interval);
ma_error_t ma_scheduler_get_running_task_collector_handle(ma_scheduler_t *scheduler, ma_running_task_collector_t **rt);
ma_error_t ma_task_get_scheduler(ma_task_t *task, ma_scheduler_t **scheduler);
ma_error_t ma_task_get_loop(ma_task_t *task, uv_loop_t **loop);
ma_error_t ma_scheduler_get_taskfolder(ma_scheduler_t *scheduler, ma_taskfolder_t **folder);
ma_error_t ma_taskfolder_expired_task_group_add(ma_taskfolder_t *folder, ma_task_t *task);
ma_error_t ma_taskfolder_expired_task_group_release(ma_taskfolder_t *folder);
MA_CPP(})


/* miss run method(s)*/
ma_bool_t core_check_missed_runs(ma_trigger_core_t *core) {
    if(core && !core->duration) {
        ma_bool_t run_missed = MA_FALSE;
        ma_bool_t miss_set = MA_FALSE;

        (void)ma_task_get_missed(core->task, &miss_set);
        if(miss_set) {
            ma_task_time_t last_run_time = {0};
            ma_task_time_t dummy = {0};
            ma_time_zone_t zone = MA_TIME_ZONE_LOCAL;

            run_missed = MA_TRUE;
            (void)ma_task_get_last_run_time(core->task, &last_run_time);
            (void)ma_task_get_time_zone(core->task, &zone);
            if(MA_TIME_ZONE_UTC == zone)
                convert_utc_to_local(last_run_time, &last_run_time);
            if(memcmp(&dummy, &last_run_time, sizeof(ma_task_time_t))) {
                ma_task_time_t today = {0};

                get_localtime(&today);
                run_missed = (memcmp(&core->last_next_fire_date, &today, sizeof(ma_task_time_t)) < 0 &&
                             memcmp(&last_run_time, &core->last_next_fire_date, sizeof(ma_task_time_t)) < 0) 
                             ? MA_TRUE : MA_FALSE;
            }
        }

        return run_missed;
    }
    return MA_FALSE;
}

ma_error_t core_get_next_missed_run_time(ma_trigger_core_t *core, ma_task_time_t *missed) {
    if(core && missed) {
        ma_error_t err = MA_ERROR_UNEXPECTED;
        ma_uint32_t conds = 0;
        const char *task_id  = NULL;
        ma_task_time_t next_run_time = {0};
        ma_task_time_t dummy = {0};
        ma_task_time_t *dp = NULL;
        ma_bool_t miss = MA_FALSE;

        (void)ma_task_get_id(core->task, &task_id);
        (void)ma_task_get_missed(core->task, &miss);
        memset(missed, 0, sizeof(ma_task_time_t));
        (void)ma_task_get_conditions(core->task, &conds);
        if(miss) {
            if(!core->duration) {
                ma_task_missed_policy_t policy = {{0}};
                ma_task_time_t now = {0};
                ma_int64_t secs = 0;

                MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) evaluating missed policy..", task_id ? task_id : MA_EMPTY);
                policy.missed_duration.hour = 0; policy.missed_duration.minute = 0;
                (void)ma_task_get_missed_policy(core->task, &policy);
                secs = (policy.missed_duration.hour || policy.missed_duration.minute) ? (policy.missed_duration.hour * 60 * 60) + (policy.missed_duration.minute * 60) : 3 /* immediate after 3 secs */;
                advance_current_date_by_seconds(secs, dp = missed);
                if(MA_OK == (err = ma_task_get_next_run_time(core->task, &next_run_time))) {
                    get_localtime(&now);
                    if(memcmp(&next_run_time, &now, sizeof(ma_task_time_t)) > 0) {
                        if(memcmp(&dummy, dp = &next_run_time, sizeof(ma_task_time_t))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) next run time(%hu/%hu/%hu %hu:%hu:%hu)", task_id ? task_id : MA_EMPTY, dp->year, dp->month, dp->day, dp->hour, dp->minute, dp->secs);
                            if(memcmp(&next_run_time, dp = missed, sizeof(ma_task_time_t)) < 0)
                                memcpy(dp = missed, &next_run_time, sizeof(ma_task_time_t));
                        }
                    }
                }
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) new missed time(%hu/%hu/%hu %hu:%hu:%hu)", task_id ? task_id : MA_EMPTY, dp->year, dp->month, dp->day, dp->hour, dp->minute, dp->secs);

            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_calculate_and_notify_timer_fire(ma_trigger_core_t *core) {
    if(core) {
        ma_error_t err = MA_ERROR_UNEXPECTED;
        ma_int64_t timeout = 0;
        const char *task_id = NULL;
        ma_time_zone_t zone = MA_TIME_ZONE_LOCAL;
        ma_task_time_t *dp = NULL;
        ma_task_time_t utc = {0};
        ma_uint32_t conds = 0;
        const char *task_name = NULL;
        ma_uint32_t events = 0;

        (void)ma_task_get_id(core->task, &task_id);
        (void)ma_task_get_time_zone(core->task, &zone);
        (void)ma_task_get_conditions(core->task, &conds);
        (void)ma_task_get_name(core->task, &task_name);
        (void)ma_task_get_events(core->task, &events);
        if(core->randomize && !core->interval) {
            if(core->random_interval) {
                srand((unsigned int)time(NULL));
                timeout += (rand() % core->random_interval) * 60;
                advance_date_by_seconds(timeout, core->next_fire_date, &core->next_fire_date);
            }
        }
        compute_timeout(&core->next_fire_date, &timeout);
        if(timeout < 0) {
            /* MLOS on agent start fails to mktime, trying again... */
            compute_timeout(&core->next_fire_date, &timeout);
        }
        if(timeout > 0) {
            if(core->randomize)core->randomize = MA_FALSE; /* resetting randomize flag */
            (void)ma_task_set_state(core->task, MA_TASK_STATE_NO_TRIGGER_TIME);
            dp = &core->next_fire_date;
            gzone_ptr = MA_LOCAL_ZONE_STR;
            if(core->start_lock) {
                char date_str[MA_MAX_PATH_LEN] = {0};

                MA_MSC_SELECT(_snprintf, snprintf)(date_str, MA_MAX_PATH_LEN-1, "%4hu/%2hu/%2hu %2hu:%2hu:%2hu", dp->year, dp->month, dp->day, dp->hour, dp->minute, dp->secs);
                MA_LOG(logger, MA_CHECK_BIT(conds, MA_TASK_COND_HIDDEN) ? MA_LOG_SEV_INFO  : MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "Next time(local) of task %s: %s", task_name ? task_name : task_id ? task_id : MA_EMPTY, date_str);
                uv_timer_set_repeat(&core->watcher.timer, (timeout * 1000));
                uv_timer_again(&core->watcher.timer);
            } else {
                if(MA_BYTE_CHECK_BIT(events, MA_TASK_EVENT_TYPE_SYSTEM_TIME_CHANGE)) {
                    uv_timer_set_repeat(&core->watcher.timer, (timeout * 1000));
                    uv_timer_again(&core->watcher.timer);
                } else {
                    uv_timer_init(core->loop, &core->watcher.timer);
                    uv_timer_start(&core->watcher.timer, &core_timer_state_machine_cb, (timeout * 1000), 0);
                }
                core->start_lock = MA_TRUE;
            }
            if(MA_TIME_ZONE_UTC == zone) {
                convert_local_to_utc(core->next_fire_date, dp = &utc);
                gzone_ptr = MA_UTC_ZONE_STR;
            }
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) trigger id(%s) next fire time(%hu/%hu/%hu %hu:%hu:%hu) zone(%s)", task_id ? task_id : MA_EMPTY, core->id, dp->year, dp->month, dp->day, dp->hour, dp->minute, dp->secs, gzone_ptr);
            if(MA_OK != (err = ma_task_set_next_run_time(core->task, core->next_fire_date))) {
                MA_LOG(logger, MA_LOG_SEV_ERROR, "task id(%s) trigger id(%s) failed to set next run time, last error(%d)", task_id ? task_id : MA_EMPTY, core->id, err);
                err = MA_OK; /* intentionally ignoring error on trigger start */
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_calculate_and_notify_timer_intervals(ma_trigger_core_t *core) {
    if(core) {
        ma_error_t err = MA_ERROR_UNEXPECTED;
        const char *task_id = NULL;
        const char *task_name = NULL;

        (void)ma_task_get_name(core->task, &task_name);
        (void)ma_task_get_id(core->task, &task_id);
        if(core->no_of_intervals) {
            while(core->no_of_intervals--) {
                advance_date_by_seconds(core->interval, core->next_fire_date, &core->next_fire_date);
                if(validate_date(((ma_trigger_core_t*)core)->next_fire_date)) {
                    core->randomize = MA_FALSE;
                    if(MA_OK != (err = core_calculate_and_notify_timer_fire(core))) {
                        (void)ma_trigger_core_close(core);
                    }
                    break;
                }
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_calculate_and_notify_timer_misfires(ma_trigger_core_t *core) {
    if(core) {
        ma_error_t err = MA_OK;
        ma_time_zone_t zone = MA_TIME_ZONE_LOCAL;
        ma_task_time_t utc = {0};
        ma_task_time_t *dp = NULL;
        ma_task_time_t missed = {0};
        
        (void)ma_task_get_time_zone(core->task, &zone);
        if(MA_OK != (err = core_calculate_and_notify_timer_intervals(core))) {
            if(MA_CHECK_BIT(core->flags, MA_TRIGGER_HAS_END_DATE)) {
                dp = &core->end_date;
                if(MA_TIME_ZONE_UTC == zone) {
                    convert_utc_to_local(core->end_date, &utc);
                    dp = &utc;
                }
                if(check_today_last_day(core->next_fire_date, *dp)) {
                    if(MA_OK == (err = core_get_next_missed_run_time(core, &missed))) {
                        memcpy(&core->next_fire_date, &missed, sizeof(ma_task_time_t));
                        if(MA_OK != (err = core_calculate_and_notify_timer_fire(core))) {
                            (void)ma_trigger_core_close(core);
                        }
                    } else {
                        (void)ma_trigger_core_close(core);
                    }
                    return err;
                }
            }
            if(core_check_missed_runs(core)) {
                if(MA_OK == (err = core_get_next_missed_run_time(core, &missed))) {
                    memcpy(&core->next_fire_date, &missed, sizeof(ma_task_time_t));
                    if(MA_OK == (err = core_calculate_and_notify_timer_fire(core))) {
                        return err;
                    }
                }
            }
            while(MA_OK == (err = ma_trigger_core_next_fire_date(core))) {
                if(MA_CHECK_BIT(core->flags, MA_TRIGGER_HAS_END_DATE)) {
                    dp = &core->end_date;
                    if(MA_TIME_ZONE_UTC == zone) {
                        convert_utc_to_local(core->end_date, &utc);
                        dp = &utc;
                    }
                    if(is_last_day_later(*dp, core->next_fire_date)) {
                        core_compute_trigger_interval_count(core);
                        if(MA_OK != (err =  core_calculate_and_notify_timer_fire(core))) {
                            if(MA_OK == (err = core_calculate_and_notify_timer_intervals(core))) {
                                break;
                            }
                        } else break;
                    } else {
                        if(MA_OK == (err = core_get_next_missed_run_time(core, &missed))) {
                            memcpy(&core->next_fire_date, &missed, sizeof(ma_task_time_t));
                            if(MA_OK != (err = core_calculate_and_notify_timer_fire(core))) {
                                (void)ma_trigger_core_close(core);
                            }
                        } else {
                            (void)ma_trigger_core_close(core);
                        }
                        break;
                    }
                } else {
                    core_compute_trigger_interval_count(core);
                    if(MA_OK != (err = core_calculate_and_notify_timer_fire(core))) {
                        err = core_calculate_and_notify_timer_intervals(core);
                    }
                    break;
                }
            } /* end of while */
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

/* core time notify timer fire intervals */
ma_error_t core_notify_timer_fire_intervals(ma_trigger_core_t *core) {
    if(core) {
        ma_error_t err = MA_ERROR_UNEXPECTED;
        ma_time_zone_t zone = MA_TIME_ZONE_LOCAL;
        ma_task_time_t *dp = NULL;
        const char *task_id = NULL;
        ma_task_time_t utc = {0};

        (void)ma_task_set_state(core->task, MA_TASK_STATE_NOT_SCHEDULED);
        (void)ma_task_get_time_zone(core->task, &zone);
        (void)ma_task_get_id(core->task, &task_id);
        if(MA_TRIGGER_STATE_BLOCKED != core->state)core->state = MA_TRIGGER_STATE_NOT_STARTED;
        while(core->no_of_intervals) {
            ma_int64_t timeout = 0;

            if(core->random_interval) {
                srand((unsigned int)time(NULL));
                timeout += (rand() % core->random_interval) * 60;
            }
            advance_date_by_seconds(core->interval + timeout, core->next_fire_date, &core->next_fire_date);
            --core->no_of_intervals;
            if(memcmp(&core->next_fire_date, &core->until_date, sizeof(ma_task_time_t)) <= 0) {
                if(MA_OK == (err = core_calculate_and_notify_timer_fire(core)))
                    break;
            } else {
                if(timeout) {
                    ma_task_time_t tmp = {0};

                    advance_date_by_seconds(-timeout, core->next_fire_date, &tmp);
                    if(memcmp(&tmp, &core->until_date, sizeof(ma_task_time_t)) <= 0) {
                        memcpy(&core->next_fire_date, &tmp, sizeof(ma_task_time_t));
                        if(MA_OK == (err = core_calculate_and_notify_timer_fire(core)))
                            break;
                    }
                }
                core->no_of_intervals = 0;
                break;
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

/* calendar trigger core timer starter method */
ma_error_t core_notify_timer_fire(ma_trigger_core_t *core) {
    if(core) {
        ma_error_t err = MA_OK;
        ma_time_zone_t zone = MA_TIME_ZONE_LOCAL;
        const char *task_id = NULL;
        const char *task_name = NULL;
        ma_task_time_t *dp = NULL;
        ma_task_time_t dummy = {0};
        ma_task_time_t now = {0};
        ma_task_time_t next_fire_date = {0};
        ma_bool_t check_next_fire_time = MA_FALSE;
        ma_uint32_t conds = 0;
        ma_uint32_t events = 0;

        (void)ma_task_get_id(core->task, &task_id);
        (void)ma_task_get_time_zone(core->task, &zone);
        (void)ma_task_get_conditions(core->task, &conds);
        (void)ma_task_get_name(core->task, &task_name);
        (void)ma_task_get_events(core->task, &events);
        get_localtime(&now);
        if(!core->start_lock) {
            if((MA_OK == (err = ma_task_get_loop(core->task, &core->loop))) && core->loop) {
                if((MA_TRIGGER_STATE_NOT_STARTED == core->state) || (MA_TRIGGER_STATE_BLOCKED == core->state)) {
                    (void)ma_task_set_state(core->task, MA_TASK_STATE_NO_TRIGGER);
                    if(memcmp(&core->next_fire_date, &dummy, sizeof(ma_task_time_t))) {
                        if((memcmp(&core->next_fire_date, dp = &core->begin_date, sizeof(ma_task_time_t)) > 0) && /* next fire time < trigger begin time */
                            (memcmp(&core->next_fire_date, &now, sizeof(ma_task_time_t)) > 0) /* next fire time < current time */
                          ) {
                            /* temporarily copy next fire date */
                            memcpy(&next_fire_date, &core->next_fire_date, sizeof(ma_task_time_t));
                            check_next_fire_time = MA_TRUE;
                        }
                    }
                    core->randomize = MA_BYTE_CHECK_BIT(events, MA_TASK_EVENT_TYPE_SYSTEM_TIME_CHANGE) ? MA_FALSE : MA_TRUE;
                    memcpy(dp = &core->next_fire_date, &core->begin_date, sizeof(ma_task_time_t));
                    gzone_ptr = MA_LOCAL_ZONE_STR;
                    if(MA_TIME_ZONE_UTC == zone) {
                        convert_utc_to_local(core->next_fire_date, dp = &core->next_fire_date);
                        gzone_ptr = MA_UTC_ZONE_STR;
                    }
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) trigger id(%s) begin time(%hu/%hu/%hu %hu:%hu:%hu) zone(%s)", task_id ? task_id : MA_EMPTY, core->id, dp->year, dp->month, dp->day, dp->hour, dp->minute, dp->secs, gzone_ptr);
                    core->watcher.timer.data = (ma_trigger_t*)core->core_data;
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) uv timer(%p) data(%p)\n", task_id, &core->watcher.timer, core->watcher.timer.data);
                    core_compute_trigger_interval_count(core);
                    if(MA_OK != (err = core_calculate_and_notify_timer_fire(core))) {
                        err = core_calculate_and_notify_timer_misfires(core);
                    }
                    if(MA_OK == err) {
                        char date_str[MA_MAX_PATH_LEN] = {0};

                        if(check_next_fire_time) {
                            if(memcmp(&next_fire_date, &core->next_fire_date, sizeof(ma_task_time_t)) < 0) {
                                memcpy(&core->next_fire_date, &next_fire_date, sizeof(ma_task_time_t));
                                MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) trigger id(%s) next fire time pre-empted ", task_id, core->id);
                                err = core_calculate_and_notify_timer_fire(core);
                            } else {
                                dp = &core->next_fire_date;
                                MA_MSC_SELECT(_snprintf, snprintf)(date_str, MA_MAX_PATH_LEN-1, "%4hu/%2hu/%2hu %2hu:%2hu:%2hu", dp->year, dp->month, dp->day, dp->hour, dp->minute, dp->secs);
                                MA_LOG(logger, MA_CHECK_BIT(conds, MA_TASK_COND_HIDDEN) ? MA_LOG_SEV_INFO  : MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "Next time(local) of task %s: %s", task_name ? task_name : task_id ? task_id : MA_EMPTY, date_str);
                            }
                        } else {
                            dp = &core->next_fire_date;
                            MA_MSC_SELECT(_snprintf, snprintf)(date_str, MA_MAX_PATH_LEN-1, "%4hu/%2hu/%2hu %2hu:%2hu:%2hu", dp->year, dp->month, dp->day, dp->hour, dp->minute, dp->secs);
                            MA_LOG(logger, MA_CHECK_BIT(conds, MA_TASK_COND_HIDDEN) ? MA_LOG_SEV_INFO  : MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "Next time(local) of task %s: %s", task_name ? task_name : task_id ? task_id : MA_EMPTY, date_str);
                        }
                        (void)ma_task_set_next_run_time(core->task, core->next_fire_date);
                        MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) trigger id(%s) started", task_id ? task_id : MA_EMPTY, core->id);
                        (void)ma_task_unset_event(core->task, MA_TASK_EVENT_TYPE_SYSTEM_TIME_CHANGE);
                    } else
                        MA_LOG(logger, MA_LOG_SEV_ERROR, "task failed to start, last error(%d)", err);
                }
            }
        } else {
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) trigger id(%s) enabled", task_id ? task_id : MA_EMPTY, core->id);
            if(MA_OK != (err = core_set_disable(core, MA_FALSE)))
                MA_LOG(logger, MA_LOG_SEV_ERROR, "task id(%s) trigger id(%s) failed to enable, last error(%d)", task_id ? task_id : MA_EMPTY, core->id, err);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}


/* core run once timer starter */
ma_error_t core_notify_run_once_fire(ma_trigger_core_t *core) {
    if(core) {
        ma_error_t err = MA_OK;
        const char *task_id = NULL;
        ma_time_zone_t zone = MA_TIME_ZONE_LOCAL;
        ma_task_time_t *dp = NULL;
        ma_task_time_t utc = {0};
        ma_task_time_t missed = {0};
        ma_task_time_t dummy = {0};
        ma_task_time_t now = {0};
        ma_uint32_t conds = 0;
        const char *task_name = NULL;
        ma_uint32_t events = 0;

        (void)ma_task_get_id(core->task, &task_id);
        (void)ma_task_get_time_zone(core->task, &zone);
        (void)ma_task_get_conditions(core->task, &conds);
        (void)ma_task_get_name(core->task, &task_name);
        (void)ma_task_get_events(core->task, &events);
        get_localtime(&now);
        if(!core->start_lock) {
            if((MA_OK == (err = ma_task_get_loop(core->task, &core->loop))) && core->loop) {
                if((MA_TRIGGER_STATE_NOT_STARTED == core->state) || (MA_TRIGGER_STATE_BLOCKED == core->state)) {
                    (void)ma_task_set_state(core->task, MA_TASK_STATE_NO_TRIGGER);
                    core->randomize = MA_FALSE;
                    if((memcmp(&core->next_fire_date, dp = &core->begin_date, sizeof(ma_task_time_t)) <= 0) || /* next fire time < trigger begin time */
                        !memcmp(&core->next_fire_date, &dummy, sizeof(ma_task_time_t)) || /* next fire time empty check */
                        (memcmp(&core->next_fire_date, &now, sizeof(ma_task_time_t)) < 0) /* next fire time < current time */
                      ) {
                        memcpy(dp = &core->next_fire_date, &core->begin_date, sizeof(ma_task_time_t));
                    }
                    core->randomize = MA_BYTE_CHECK_BIT(events, MA_TASK_EVENT_TYPE_SYSTEM_TIME_CHANGE) ? MA_FALSE : MA_TRUE;
                    gzone_ptr = MA_LOCAL_ZONE_STR;
                    if(MA_TIME_ZONE_UTC == zone) {
                        convert_utc_to_local(core->next_fire_date, &utc);
                        dp = &utc;
                        gzone_ptr = MA_UTC_ZONE_STR;
                    }
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) trigger id(%s) begin time(%u/%u/%u %u:%u:%u) zone(%s)", task_id ? task_id : MA_EMPTY, core->id, dp->year, dp->month, dp->day, dp->hour, dp->minute, dp->secs, gzone_ptr);
                    core->watcher.timer.data = core->core_data;
                    if(MA_OK != (err = core_calculate_and_notify_timer_fire(core))) {
                        if(core_check_missed_runs(core)) {
                            if(MA_OK == (err = core_get_next_missed_run_time(core, &missed))) {
                                memcpy(&core->next_fire_date, &missed, sizeof(ma_task_time_t));
                                if(MA_OK != (err = core_calculate_and_notify_timer_fire(core))) {
                                    MA_LOG(logger, MA_LOG_SEV_DEBUG,"task id(%s) trigger id(%s) expired", task_id ? task_id : MA_EMPTY, core->id);
                                    (void)ma_trigger_core_close(core); /* core is expired  proceeding to close */
                                }
                            } else {
                                MA_LOG(logger, MA_LOG_SEV_DEBUG,"task id(%s) trigger id(%s) expired", task_id ? task_id : MA_EMPTY, core->id);
                                (void)ma_trigger_core_close(core); /* core is expired  proceeding to close */
                            }
                        } else {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG,"task id(%s) trigger id(%s) expired", task_id ? task_id : MA_EMPTY, core->id);
                            (void)ma_trigger_core_close(core); /* core is expired  proceeding to close */
                        }
                    }
                    if(MA_OK == err) {
                        char date_str[MA_MAX_PATH_LEN] = {0};

                        dp = &core->next_fire_date;
                        MA_MSC_SELECT(_snprintf, snprintf)(date_str, MA_MAX_PATH_LEN-1, "%4hu/%2hu/%2hu %2hu:%2hu:%2hu", dp->year, dp->month, dp->day, dp->hour, dp->minute, dp->secs);
                        MA_LOG(logger, MA_CHECK_BIT(conds, MA_TASK_COND_HIDDEN) ? MA_LOG_SEV_INFO  : MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "Next time(local) of task %s: %s", task_name ? task_name : task_id ? task_id : MA_EMPTY, date_str);
                        (void)ma_task_unset_event(core->task, MA_TASK_EVENT_TYPE_SYSTEM_TIME_CHANGE);
                        (void)ma_task_set_next_run_time(core->task, core->next_fire_date);
                    }
                }
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

/* core run now timer starter */
ma_error_t core_notify_run_now_fire(ma_trigger_core_t *core) {
    if(core) {
        ma_error_t err = MA_OK;
        const char *task_id = NULL;
        ma_time_zone_t zone = MA_TIME_ZONE_LOCAL;
        ma_task_time_t *dp = NULL;
        ma_task_time_t utc = {0};
        ma_task_time_t dummy = {0};
        ma_task_time_t now = {0};
        ma_uint32_t conds = 0;
        const char *task_name = NULL;
        ma_uint32_t events = 0;

        (void)ma_task_get_id(core->task, &task_id);
        (void)ma_task_get_time_zone(core->task, &zone);
        (void)ma_task_get_conditions(core->task, &conds);
        (void)ma_task_get_name(core->task, &task_name);
        (void)ma_task_get_events(core->task, &events);
        get_localtime(&now);
        if(!core->start_lock) {
            if((MA_OK == (err = ma_task_get_loop(core->task, &core->loop))) && core->loop) {
                if((MA_TRIGGER_STATE_NOT_STARTED == core->state) || (MA_TRIGGER_STATE_BLOCKED == core->state)) {
                    (void)ma_task_set_state(core->task, MA_TASK_STATE_NO_TRIGGER);
                    if(!memcmp(&core->next_fire_date, &dummy, sizeof(ma_task_time_t))) {
                        ma_task_delay_policy_t policy = {0};
                        
                        memcpy(dp = &core->next_fire_date, &core->begin_date, sizeof(ma_task_time_t));
                        (void)ma_task_get_delay_policy(core->task, &policy);
                        if(policy.delay_time.hour || policy.delay_time.minute)
                            advance_current_date_by_seconds(((policy.delay_time.hour * 60 * 60) + (policy.delay_time.minute * 60)), dp = &core->next_fire_date);
                    }
                    core->randomize = MA_BYTE_CHECK_BIT(events, MA_TASK_EVENT_TYPE_SYSTEM_TIME_CHANGE) ? MA_FALSE : MA_TRUE;
                    gzone_ptr = MA_LOCAL_ZONE_STR;
                    if(MA_TIME_ZONE_UTC == zone) {
                        convert_utc_to_local(core->next_fire_date, dp = &utc);
                        gzone_ptr = MA_UTC_ZONE_STR;
                    }
                    core->watcher.timer.data = core->core_data;
                    if(MA_OK == (err = core_calculate_and_notify_timer_fire(core))) {
                        char date_str[MA_MAX_PATH_LEN] = {0};
                        
                        dp = &core->next_fire_date;
                        MA_MSC_SELECT(_snprintf, snprintf)(date_str, MA_MAX_PATH_LEN-1, "%4hu/%2hu/%2hu %2hu:%2hu:%2hu", dp->year, dp->month, dp->day, dp->hour, dp->minute, dp->secs);
                        MA_LOG(logger, MA_CHECK_BIT(conds, MA_TASK_COND_HIDDEN) ? MA_LOG_SEV_INFO  : MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "Next time(local) of task %s: %s", task_name ? task_name : task_id ? task_id : MA_EMPTY, date_str);
                    } else {
                        MA_LOG(logger, MA_LOG_SEV_DEBUG,"task id(%s) trigger id(%s) expired", task_id ? task_id : MA_EMPTY, core->id);
                        (void)ma_trigger_core_close(core); /* core is expired  proceeding to close */
                    }
                    if(MA_OK == err) {
                        (void)ma_task_set_next_run_time(core->task, core->next_fire_date);
                        (void)ma_task_unset_event(core->task, MA_TASK_EVENT_TYPE_SYSTEM_TIME_CHANGE);
                    }
                }
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

/* core event timer starter */
ma_error_t core_notify_event_fire(ma_trigger_core_t *core) {
    if(core) {
        ma_error_t err = MA_OK;
        const char *task_id = NULL;
        ma_task_time_t dummy = {0};
        ma_task_time_t now = {0};
        ma_task_time_t *dp = NULL;
        const char *task_name = NULL;
        ma_uint32_t conds = 0;

        (void)ma_task_get_name(core->task, &task_name);
        (void)ma_task_get_conditions(core->task, &conds);
        (void)ma_task_get_id(core->task, &task_id);
        if(!core->start_lock) {
            if((MA_OK == (err = ma_task_get_loop(core->task, &core->loop))) && core->loop) {
                if((MA_TRIGGER_STATE_NOT_STARTED == core->state) || (MA_TRIGGER_STATE_BLOCKED == core->state)) {
                    (void)ma_task_set_state(core->task, MA_TASK_STATE_NO_TRIGGER);
                    core->watcher.timer.data = core->core_data;
                    if(memcmp(&core->next_fire_date, &dummy, sizeof(ma_task_time_t))) {
                        if(memcmp(&core->next_fire_date, &now, sizeof(ma_task_time_t)) > 0) /* next fire time < current time */ {
                            if(MA_OK == (err = core_calculate_and_notify_timer_fire(core))) {                      
                                char date_str[MA_MAX_PATH_LEN] = {0};
                        
                                dp = &core->next_fire_date;
                                MA_MSC_SELECT(_snprintf, snprintf)(date_str, MA_MAX_PATH_LEN-1, "%4hu/%2hu/%2hu %2hu:%2hu:%2hu", dp->year, dp->month, dp->day, dp->hour, dp->minute, dp->secs);
                                MA_LOG(logger, MA_CHECK_BIT(conds, MA_TASK_COND_HIDDEN) ? MA_LOG_SEV_INFO  : MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "Next time(local) of task %s: %s", task_name ? task_name : task_id ? task_id : MA_EMPTY, date_str);
                            } 
                            else 
                                err = MA_OK;
                        }
                    }
                    if(!core->start_lock) {
                        uv_timer_init(core->loop, &core->watcher.timer);
                        core->start_lock = MA_TRUE;
                        core->state = MA_TRIGGER_STATE_BLOCKED;
                        uv_timer_start(&core->watcher.timer, &core_timer_state_machine_cb, 0, 0);
                    }
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) event trigger id(%s) started", task_id ? task_id : MA_EMPTY, core->id);
                }
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

/* core timer close call back */
void core_timer_close_cb(uv_handle_t *timer) {
    if(timer) {
        if(!uv_is_active(timer)) {
            ma_trigger_t *trigger = (ma_trigger_t*)timer->data;
            ma_trigger_core_t *core = NULL;
            uv_timer_t *self = (uv_timer_t*)timer;

            if(trigger && (core = (ma_trigger_core_t*)trigger->data)) {
                ma_task_t *task = core->task;
                const char *task_id = NULL;
                ma_taskfolder_t *folder = NULL;
                ma_scheduler_t *scheduler = NULL;
                ma_taskfolder_status_t status = MA_TASKFOLDER_STATUS_NOT_STARTED;

                (void)ma_task_get_id(task, &task_id);
                (void)ma_task_get_scheduler(task, &scheduler);
                core->state = MA_TRIGGER_STATE_EXPIRED;
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) trigger id(%s) expired", task_id ? task_id : MA_EMPTY, core->id);
                (void)ma_task_set_state(task, MA_TASK_STATE_EXPIRED);
                (void)ma_task_get_scheduler(task, &scheduler);
                (void)ma_scheduler_get_taskfolder(scheduler, &folder);
                (void)ma_taskfolder_get_status(folder, (int*)&status);
                if(MA_TASKFOLDER_STATUS_STARTED == status) {
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) trigger id(%s) uv timer data(%p)", task_id ? task_id : MA_EMPTY, core->id, self->data);
                    core_timer_destroy(core);
                } else {
                    if(MA_OK == ma_taskfolder_expired_task_group_add(folder, task)) {
                        MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) added to scheduler expired task group", task_id ? task_id : MA_EMPTY);
                    }
                }
            }
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "timer closed successfully");
        }
    }
}

/* core timer notify next fire */
ma_error_t core_notify_timer_next_fire(ma_trigger_core_t *core) {
    if(core) {
        ma_error_t err = MA_OK;
        ma_task_t *task = core->task;
        const char *task_id = NULL;
        ma_time_zone_t zone = MA_TIME_ZONE_LOCAL;
        ma_task_time_t *dp = NULL;
        ma_task_time_t utc = {0};
        ma_int64_t timeout = 0;

        (void)ma_task_get_time_zone(core->task, &zone);
        (void)ma_task_get_id(core->task, &task_id);
        core->curr_retry_count = 0; /* reset retry counter */
        if (MA_OK != (err =  core_notify_timer_fire_intervals(core))) {
            if(MA_TRIGGER_STATE_BLOCKED != core->state)core->state = MA_TRIGGER_STATE_COMPLETE;;
            (void)ma_task_set_state(core->task, MA_TASK_STATE_NO_MORE_RUN);
            if(MA_CHECK_BIT(core->flags, MA_TRIGGER_HAS_END_DATE)) {
                dp = &core->end_date;
                if(MA_TIME_ZONE_UTC == zone)
                    convert_utc_to_local(core->end_date, dp = &utc);
                if(check_today_last_day(core->next_fire_date, *dp)) {
                    MA_LOG(logger, MA_LOG_SEV_DEBUG,"task id(%s) trigger id(%s) closing end date hit", task_id ? task_id : MA_EMPTY, core->id);
                    (void)ma_trigger_core_close(core);
                    return MA_ERROR_UNEXPECTED;
                }
            }
            if(MA_OK == ma_trigger_core_next_fire_date(core)) {
                ma_task_time_t dummy = {0};

                (void)ma_task_set_state(task, MA_TASK_STATE_NOT_SCHEDULED);
                if(MA_TRIGGER_STATE_BLOCKED != core->state)core->state = MA_TRIGGER_STATE_NOT_STARTED;
                if(!memcmp(&dummy, &core->next_fire_date, sizeof(ma_task_time_t))) {
                    return ma_trigger_core_close(core);
                }
                (void)ma_task_set_next_run_time(task, core->next_fire_date);
                (void)compute_timeout(&core->next_fire_date, &timeout);
                if(MA_CHECK_BIT(core->flags, MA_TRIGGER_HAS_END_DATE)) {
                    dp = &core->end_date;
                    if(MA_TIME_ZONE_UTC == zone)
                        convert_utc_to_local(core->end_date, dp = &utc);
                    if(is_last_day_later(*dp, core->next_fire_date)) {
                        if(timeout > 0) {
                            core_compute_trigger_interval_count(core);
                            core->randomize = MA_TRUE;
                            if(MA_OK != (err = core_calculate_and_notify_timer_fire(core)))
                                (void)ma_trigger_core_close(core);
                        } else {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG,"task id(%s) trigger id(%s) timeout closing", task_id ? task_id : MA_EMPTY, core->id);
                            err = ma_trigger_stop((ma_trigger_t*)core);
                        }
                    } else {
                        MA_LOG(logger, MA_LOG_SEV_DEBUG,"task id(%s) trigger id(%s) end date hit, closing...", task_id ? task_id : MA_EMPTY, core->id);
                        (void)ma_trigger_core_close(core); err = MA_ERROR_UNEXPECTED;
                    }
                } else {
                    core_compute_trigger_interval_count(core);
                    if(timeout > 0) {
                        core->randomize = MA_TRUE;
                        if(MA_OK != (err = core_calculate_and_notify_timer_fire(core)))
                            (void)ma_trigger_core_close(core);
                    } else {
                        if(MA_OK == (err =  core_notify_timer_fire_intervals(core))) {
                            if(MA_OK != (err = core_calculate_and_notify_timer_fire(core)))
                                (void)ma_trigger_core_close(core);
                        } else {
                            (void)ma_trigger_core_close(core);
                        }
                    }
                }
            } else {
                (void)ma_trigger_core_close(core);
                err = MA_ERROR_UNEXPECTED; 
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

/* core run once next fire */
ma_error_t core_run_once_next_fire(ma_trigger_core_t *core) {
    if(core) {
        memset(&core->next_fire_date, 0, sizeof(ma_task_time_t)); // no more runs
        ma_task_set_next_run_time(core->task, core->next_fire_date);
        ma_trigger_stop((ma_trigger_t*)core->core_data);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

/* core event timer next fire */
ma_error_t core_notify_event_timer_next_fire(ma_trigger_core_t *core) {
    if(core) {
        if(core->start_lock) {
            if(MA_TRIGGER_STATE_BLOCKED != core->state)core->state = MA_TRIGGER_STATE_COMPLETE;;
            (void)ma_task_set_state(core->task, MA_TASK_STATE_NO_MORE_RUN);
            uv_timer_stop(&core->watcher.timer);
        }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

/* core system startup event timer next fire */
ma_error_t core_notify_system_startup_event_timer_next_fire(ma_trigger_core_t *core) {
    if(core) {
        if(core->start_lock) {
            ma_trigger_systemstart_policy_t policy = {0};
            
            if(MA_TRIGGER_STATE_BLOCKED != core->state) {
                core->state = MA_TRIGGER_STATE_COMPLETE;
                (void)ma_task_set_state(core->task, MA_TASK_STATE_NO_MORE_RUN);
                uv_timer_stop(&core->watcher.timer);
                (void)ma_trigger_get_policy((ma_trigger_t*)core->core_data, &policy);
                if(policy.run_just_once) {
                    (void)ma_trigger_stop((ma_trigger_t*)core->core_data);
                    (void)ma_trigger_close((ma_trigger_t*)core->core_data);
                }
            }
        }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


/* core timer notify force stop */
ma_error_t core_notify_force_stop(ma_trigger_core_t *core) {
    if(core) {
        if(MA_WATCHER_IDLE == core->watcher_type) {
            uv_idle_stop(&core->watcher.idler);
        } else if(MA_WATCHER_TIMER == core->watcher_type) {
            uv_timer_stop(&core->watcher.timer);
        } else {
            /* it should not come here */
        }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

/* core run once stop fire */
ma_error_t core_run_once_stop_fire(ma_trigger_core_t *core) {
    if(core) {
        core->state = MA_TRIGGER_STATE_COMPLETE;
        (void)ma_task_set_state(core->task, MA_TASK_STATE_NO_MORE_RUN);
        uv_timer_stop(&core->watcher.timer);
        memset(&core->next_fire_date, 0, sizeof(ma_task_time_t)); /* no more runs */
        ma_task_set_next_run_time(core->task, core->next_fire_date);
        return ma_trigger_core_close(core);
    }
    return MA_ERROR_INVALIDARG;
}

/* core daily stop fire */
ma_error_t core_daily_stop_fire(ma_trigger_core_t *core) {
    if(core) {
        ma_task_t *task = core->task;
        core->state = MA_TRIGGER_STATE_COMPLETE;
        core->flags |= MA_TRIGGER_DISABLED;
        uv_timer_stop(&core->watcher.timer);
        return ma_task_set_state(task, MA_TASK_STATE_NO_MORE_RUN);
    }
    return MA_ERROR_INVALIDARG;
}

/* core weekly stop fire */
ma_error_t core_weekly_stop_fire(ma_trigger_core_t *core) {
    if(core) {
        ma_task_t *task = core->task;
        core->state = MA_TRIGGER_STATE_COMPLETE;
        core->flags |= MA_TRIGGER_DISABLED;
        uv_timer_stop(&core->watcher.timer);
        return ma_task_set_state(task, MA_TASK_STATE_NO_MORE_RUN);
    }
    return MA_ERROR_INVALIDARG;
}

/* core montly stop fire */
ma_error_t core_monthly_date_stop_fire(ma_trigger_core_t *core) {
    if(core) {
        ma_task_t *task = core->task;
        core->state = MA_TRIGGER_STATE_COMPLETE;
        core->flags |= MA_TRIGGER_DISABLED;
        uv_timer_stop(&core->watcher.timer);
        return ma_task_set_state(task, MA_TASK_STATE_EXPIRED);
    }
    return MA_ERROR_INVALIDARG;
}

/* core monthly day of week stop fire */
ma_error_t core_monthly_dow_stop_fire(ma_trigger_core_t *core) {
    if(core) {
        ma_task_t *task = core->task;
        core->state = MA_TRIGGER_STATE_COMPLETE;
        core->flags |= MA_TRIGGER_DISABLED;
        uv_timer_stop(&core->watcher.timer);
        return ma_task_set_state(task, MA_TASK_STATE_EXPIRED);
    }
    return MA_ERROR_INVALIDARG;
}

/* core timer force reset */
ma_error_t core_notify_force_reset(ma_trigger_core_t *core, ma_int64_t timeout) {
    if(core) {
        uv_timer_stop(&core->watcher.timer);
        uv_timer_set_repeat(&core->watcher.timer, timeout);
        uv_timer_again(&core->watcher.timer);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_notify_event_force_reset(ma_trigger_core_t *core, ma_int64_t timeout) {
    if(core) {
        ma_error_t err = MA_OK;
        const char *task_id = NULL;
        ma_time_zone_t zone = MA_TIME_ZONE_LOCAL;
        ma_task_time_t *dp = NULL;
        ma_task_time_t utc = {0};
        const char *task_name = NULL;
        ma_uint32_t conds = 0;

        (void)ma_task_get_name(core->task, &task_name);
        (void)ma_task_get_conditions(core->task, &conds);
        (void)ma_task_get_id(core->task, &task_id);
        (void)ma_task_get_time_zone(core->task, &zone);
        uv_timer_stop(&core->watcher.timer);
        if(timeout)
            advance_current_date_by_seconds(timeout/1000, dp = &core->next_fire_date);
        else
            get_localtime(dp = &core->next_fire_date);
        gzone_ptr = MA_LOCAL_ZONE_STR;
        if(MA_TIME_ZONE_UTC == zone) {
            convert_local_to_utc(core->next_fire_date, dp = &utc); 
            gzone_ptr = MA_UTC_ZONE_STR;
        }
        if(MA_TRIGGER_STATE_NOT_STARTED == core->state) {                    
            char date_str[MA_MAX_PATH_LEN] = {0};
                        
            dp = &core->next_fire_date;
            MA_MSC_SELECT(_snprintf, snprintf)(date_str, MA_MAX_PATH_LEN-1, "%4hu/%2hu/%2hu %2hu:%2hu:%2hu", dp->year, dp->month, dp->day, dp->hour, dp->minute, dp->secs);
            MA_LOG(logger, MA_CHECK_BIT(conds, MA_TASK_COND_HIDDEN) ? MA_LOG_SEV_INFO  : MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "Next time(local) of task %s: %s", task_name ? task_name : task_id ? task_id : MA_EMPTY, date_str);
        }
        MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) trigger id(%s) next fire time(%hu/%hu/%hu %hu:%hu:%hu) zone(%s) ", task_id ? task_id : MA_EMPTY, core->id, dp->year, dp->month, dp->day, dp->hour, dp->minute, dp->secs, gzone_ptr);
        uv_timer_set_repeat(&core->watcher.timer, timeout);
        uv_timer_again(&core->watcher.timer);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}
/* core timer close */
ma_error_t core_notify_timer_fire_close(ma_trigger_core_t *core) {
    if(core) {
        if(core->start_lock) {
            if(uv_is_active((uv_handle_t*)&core->watcher.timer)) {
                uv_timer_stop(&core->watcher.timer);
            }
            uv_close((uv_handle_t*)&core->watcher.timer, &core_timer_close_cb);
            core->start_lock = MA_FALSE; /* reset */
        } else {
            ma_bool_t is_task_stopped = MA_FALSE;

            core->state = MA_TRIGGER_STATE_EXPIRED;
            if((MA_OK == ma_check_task_stopped(core->task, &is_task_stopped)) && is_task_stopped) {
                /* set state as "expired" */
                (void)ma_task_set_state(core->task, MA_TASK_STATE_EXPIRED);
                core_timer_destroy(core);
            }
        }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}



/* core timer state machine */
void core_timer_state_machine_cb(uv_timer_t *timer, int status) {
    if(timer) {
        ma_trigger_t *trigger = (ma_trigger_t*)timer->data;
        ma_task_time_t *dp = NULL;
        ma_task_time_t utc = {0};
        ma_uint32_t cond = 0;
        ma_error_t err = MA_OK;

        if(trigger) {
            ma_trigger_core_t *core = (ma_trigger_core_t*)trigger->data;

            if(core) {
                ma_task_t *task = NULL;
                const char *task_id = NULL;
                const char *task_name = NULL;
                ma_task_time_t now = {0};

                (void)ma_task_get_conditions(task = core->task, &cond);
                (void)ma_task_get_name(task, &task_name);
                (void)ma_task_get_id(task, &task_id);
                get_localtime(&now);
                if(core_check_task_runnable(core->task)) {
                    ma_scheduler_t *scheduler = NULL;
                    ma_running_task_collector_t *rt = NULL;
                    ma_task_exec_status_t exec_status = MA_TASK_EXEC_NOT_STARTED;
                    ma_time_zone_t zone = MA_TIME_ZONE_LOCAL;
            
                    (void)ma_task_get_scheduler(task, &scheduler);
                    (void)ma_scheduler_get_running_task_collector_handle(scheduler, &rt);
                    (void)ma_task_get_time_zone(core->task, &zone);
                    if(MA_TRIGGER_STATE_NOT_STARTED ==  core->state) {
                        MA_LOG(logger, MA_CHECK_BIT(cond, MA_TASK_COND_HIDDEN) ? MA_LOG_SEV_INFO : MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "Scheduler: Invoking task [%s]...", !strcmp(task_name, MA_EMPTY) ? task_id ? task_id : MA_EMPTY : task_name);
                        core_reset_task_exec_not_started_status(core->task);
                        if((MA_OK == ma_task_get_execution_status(task, &exec_status)) && (MA_TASK_EXEC_NOT_STARTED == exec_status)) {
                            ma_uint16_t max_run_time = 0;
                            ma_uint16_t max_run_time_limited = 0;
                            ma_uint32_t run_span = 0;
                            
                            memcpy(&core->last_fire_date, &now, sizeof(ma_task_time_t));
                            if(MA_CHECK_BIT(cond, MA_TASK_COND_NOTIFY_ON_EXEC_STATUS_CHANGE))
                                (void)ma_task_publish_topics(core->task, ma_task_get_execution_status_str(core->task, MA_TASK_EXEC_NOT_STARTED));
                            (void)ma_task_set_state(task, MA_TASK_STATE_RUNNING);
                            (void)ma_task_set_execution_status_internal(task, MA_TASK_EXEC_RUNNING);
                            core->state = MA_TRIGGER_STATE_RUNNING;
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) trigger id(%s) running ", task_id ? task_id : MA_EMPTY, core->id);
                            MA_LOG(logger, MA_CHECK_BIT(cond, MA_TASK_COND_HIDDEN) ? MA_LOG_SEV_INFO : MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "The task %s becomes active", !strcmp(task_name, MA_EMPTY) ? task_id ? task_id : MA_EMPTY : task_name);
                            if(MA_TIME_ZONE_UTC == zone) {
                                convert_local_to_utc(now, &now);
                            }
                            (void)ma_task_set_last_run_time(task, now);
                            if(MA_OK != ma_running_task_collector_add(rt, task))
                                MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) running task collector add failed", task_id ? task_id : MA_EMPTY);
                            if(MA_OK != (err = ma_task_publish_topics(core->task, MA_TASK_EXECUTE_PUBSUB_TOPIC)))
                                MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) trigger id(%s) publishing execute message failed", task_id, core->id, err);
                            if(MA_CHECK_BIT(cond, MA_TASK_COND_NOTIFY_ON_EXEC_STATUS_CHANGE))
                                (void)ma_task_publish_topics(core->task, ma_task_get_execution_status_str(core->task, MA_TASK_EXEC_RUNNING));
                            if((MA_OK == ma_task_get_max_run_time(task, &max_run_time)) && max_run_time) {
                                run_span = max_run_time;
                            }                        
                            if((MA_OK == ma_task_get_max_run_time_limit(task, &max_run_time_limited)) && max_run_time_limited) {
                                core->is_run_time_limited = MA_TRUE;
                            }
                            if(core->is_run_time_limited) {
                                run_span = max_run_time_limited;
                            }
                            core->signal_stop = MA_FALSE;
                            uv_timer_set_repeat(timer, (run_span * 60 * 1000 * 3)/4);
                            uv_timer_again(timer);
                        } else {
                            core->state = MA_TRIGGER_STATE_NOT_STARTED;
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) trigger id(%s) state = NOT STARTED ", task_id ? task_id : MA_EMPTY, core->id);
                            ma_trigger_core_next_fire(core);
                        }
                    } else if(MA_TRIGGER_STATE_BLOCKED == core->state) {
                        memcpy(&core->last_fire_date, &now, sizeof(ma_task_time_t));
                        MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) trigger id(%s) state = BLOCKED ", task_id ? task_id : MA_EMPTY, core->id);
                        (void)ma_trigger_core_next_fire(core);
                    } else if(MA_TRIGGER_STATE_RUNNING == core->state) {
                        if(MA_OK != ma_running_task_collector_remove(rt, task)) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) removing from running task collector failed", task_id ? task_id : MA_EMPTY);
                        }
                        if(MA_OK == ma_task_get_execution_status(task, &exec_status)) {
                            switch(exec_status) {
                                case MA_TASK_EXEC_STOP_INITIATED_BY_SCHEDULER: {
                                    if(core->signal_stop) {
                                        core->state = MA_TRIGGER_STATE_COMPLETE;
                                        (void)ma_task_set_execution_status_internal(task, MA_TASK_EXEC_FAILED);
                                        if(MA_CHECK_BIT(cond, MA_TASK_COND_NOTIFY_ON_EXEC_STATUS_CHANGE))
                                            (void)ma_task_publish_topics(core->task, ma_task_get_execution_status_str(core->task, MA_TASK_EXEC_FAILED));
                                        MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) trigger id(%s) state = STOP_INITIATED_BY_SCHEDULER ", task_id ? task_id : MA_EMPTY, core->id);
                                        MA_LOG(logger, MA_CHECK_BIT(cond, MA_TASK_COND_HIDDEN) ? MA_LOG_SEV_INFO : MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "The task %s has been stopped by Scheduler", !strcmp(task_name, MA_EMPTY) ? task_id ? task_id : MA_EMPTY : task_name);
                                        (void)ma_task_publish_topics(core->task, MA_TASK_STOP_PUBSUB_TOPIC);
                                        (void)ma_trigger_core_next_fire(core);
                                    }
                                    break;
                                }
                                case MA_TASK_EXEC_STOPPED: {
                                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) trigger id(%s) state = STOPPED ", task_id ? task_id : MA_EMPTY, core->id);
                                        if(MA_CHECK_BIT(cond, MA_TASK_COND_NOTIFY_ON_EXEC_STATUS_CHANGE))
                                            (void)ma_task_publish_topics(core->task, ma_task_get_execution_status_str(core->task, MA_TASK_EXEC_STOPPED));
                                    (void)ma_trigger_core_next_fire(core);
                                    break;
                                }
                                case MA_TASK_EXEC_RUNNING: {
                                    ma_uint16_t run_span = 0;
                                
                                    MA_LOG(logger, MA_CHECK_BIT(cond, MA_TASK_COND_HIDDEN) ? MA_LOG_SEV_INFO : MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "The task %s is still running", !strcmp(task_name, MA_EMPTY) ? task_id ? task_id : MA_EMPTY : task_name);
                                    if(core->is_run_time_limited)
                                        (void)ma_task_get_max_run_time_limit(task, &run_span);
                                    else
                                        (void)ma_task_get_max_run_time(task, &run_span);
                                    if(run_span) {
                                        MA_LOG(logger, MA_CHECK_BIT(cond, MA_TASK_COND_HIDDEN) ? MA_LOG_SEV_INFO : MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "Scheduler: Task [%s] exceeded duration limits. Task stopped", !strcmp(task_name, MA_EMPTY) ? task_id ? task_id : MA_EMPTY : task_name);
                                        MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) trigger id(%s) state = RUNNING ", task_id ? task_id : MA_EMPTY, core->id);
                                        (void)ma_task_publish_topics(core->task, MA_TASK_UPDATE_STATUS_PUBSUB_TOPIC);
                                        core->signal_stop = MA_TRUE;
                                        MA_LOG(logger, MA_CHECK_BIT(cond, MA_TASK_COND_HIDDEN) ? MA_LOG_SEV_INFO : MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "The status of the task %s  is unknown", !strcmp(task_name, MA_EMPTY) ? task_id ? task_id : MA_EMPTY : task_name);
                                        (void)ma_task_set_execution_status_internal(task, MA_TASK_EXEC_STOP_INITIATED_BY_SCHEDULER);
                                        if(MA_CHECK_BIT(cond, MA_TASK_COND_NOTIFY_ON_EXEC_STATUS_CHANGE))
                                            (void)ma_task_publish_topics(core->task, ma_task_get_execution_status_str(core->task, MA_TASK_EXEC_STOP_INITIATED_BY_SCHEDULER));
                                        uv_timer_set_repeat(timer, (run_span * 60 * 1000)/4);
                                        uv_timer_again(timer);
                                    } else {
                                        core->state = MA_TRIGGER_STATE_COMPLETE;
                                        (void)ma_task_set_execution_status_internal(task, MA_TASK_EXEC_FAILED);
                                        MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) trigger id(%s) state = HANGING ", task_id ? task_id : MA_EMPTY, core->id);
                                        MA_LOG(logger, MA_CHECK_BIT(cond, MA_TASK_COND_HIDDEN) ? MA_LOG_SEV_INFO : MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "Scheduler: Task [%s] exceeded duration limits. Task stopped", !strcmp(task_name, MA_EMPTY) ? task_id ? task_id : MA_EMPTY : task_name);
                                        (void)ma_task_publish_topics(core->task, MA_TASK_STOP_PUBSUB_TOPIC);
                                        if(MA_CHECK_BIT(cond, MA_TASK_COND_NOTIFY_ON_EXEC_STATUS_CHANGE))
                                            (void)ma_task_publish_topics(core->task, ma_task_get_execution_status_str(core->task, MA_TASK_EXEC_FAILED));
                                        (void)ma_trigger_core_next_fire(core);
                                        core->signal_stop = MA_FALSE;
                                    }
                                    break;
                                }
                                case MA_TASK_EXEC_SUCCESS: {
                                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) trigger id(%s) state = SUCCESS ", task_id ? task_id : MA_EMPTY, core->id);
                                    core->state = MA_TRIGGER_STATE_COMPLETE;
                                    MA_LOG(logger, MA_CHECK_BIT(cond, MA_TASK_COND_HIDDEN) ? MA_LOG_SEV_INFO : MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "The task %s is successful", !strcmp(task_name, MA_EMPTY) ? task_id ? task_id : MA_EMPTY : task_name);
                                    if(MA_CHECK_BIT(cond, MA_TASK_COND_NOTIFY_ON_EXEC_STATUS_CHANGE))
                                        (void)ma_task_publish_topics(core->task, ma_task_get_execution_status_str(core->task, MA_TASK_EXEC_SUCCESS));
                                    (void)ma_trigger_core_next_fire(core);
                                    (void)ma_task_set_state(task, MA_TASK_STATE_NO_TRIGGER_TIME);
                                    break;
                                }
                                case MA_TASK_EXEC_FAILED: {
                                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) trigger id(%s) state = FAILED ", task_id ? task_id : MA_EMPTY, core->id);
                                    MA_LOG(logger, MA_CHECK_BIT(cond, MA_TASK_COND_HIDDEN) ? MA_LOG_SEV_INFO : MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "The task  %s has failed", !strcmp(task_name, MA_EMPTY) ? task_id ? task_id : MA_EMPTY : task_name);
                                    core->state = MA_TRIGGER_STATE_COMPLETE;
                                    if(MA_CHECK_BIT(cond, MA_TASK_COND_NOTIFY_ON_EXEC_STATUS_CHANGE))
                                        (void)ma_task_publish_topics(core->task, ma_task_get_execution_status_str(core->task, MA_TASK_EXEC_FAILED));
                                    if(core->is_run_time_limited) {
                                        ma_task_retry_policy_t policy = {0};
                                    
                                        if((MA_OK == ma_task_get_retry_policy(task, &policy)) && policy.retry_count) {
                                            if(core->curr_retry_count < policy.retry_count) {
                                                ma_uint64_t retry_secs = ((policy.retry_interval.hour * 60)+policy.retry_interval.minute)*60;
                                            
                                                ++core->curr_retry_count;
                                                (void)ma_task_set_state(task, MA_TASK_STATE_NO_TRIGGER_TIME);
                                                core->state = MA_TRIGGER_STATE_NOT_STARTED;
                                                MA_LOG(logger, MA_LOG_SEV_DEBUG, "task_id(%s) trigger id(%s) retry count(%d)", task_id ? task_id : MA_EMPTY, core->id, core->curr_retry_count);
                                                gzone_ptr = MA_LOCAL_ZONE_STR;
                                                if(MA_TIME_ZONE_UTC == zone) {
                                                    convert_local_to_utc(core->next_fire_date, &utc); 
                                                    dp = &utc; 
                                                    gzone_ptr = MA_UTC_ZONE_STR;
                                                }
                                                uv_timer_set_repeat(timer, retry_secs * 1000);
                                                uv_timer_again(timer);
                                            } else {
                                                (void)ma_task_set_state(task, MA_TASK_STATE_NO_MORE_RUN);
                                                ma_trigger_core_next_fire(core);
                                            }
                                        } else {
                                            (void)ma_task_set_state(task, MA_TASK_STATE_NO_MORE_RUN);
                                            ma_trigger_core_next_fire(core);
                                        }
                                    } else {
                                        ma_trigger_core_next_fire(core);
                                    }
                                    break;
                                }
                            }
                        }
                    } else if(MA_TRIGGER_STATE_COMPLETE == core->state) {
                        (void)ma_task_set_state(task, MA_TASK_STATE_NO_MORE_RUN);
                        ma_trigger_core_next_fire(core);
                    }

                } else {
                    MA_LOG(logger, MA_LOG_SEV_DEBUG,"task id(%s) trigger id(%s) not runnable", task_id ? task_id : MA_EMPTY, core? core->id : NULL);
                    memcpy(&core->last_fire_date, &now, sizeof(ma_task_time_t));
                    (void)ma_trigger_core_next_fire(core ? core : NULL);
                }
            }
        } else {
            uv_timer_stop(timer);
            MA_LOG(logger, MA_LOG_SEV_DEBUG,"task invalid");
            uv_close((uv_handle_t*)timer, &core_timer_close_cb);
        }
    }
}



/* setter(s) */
ma_error_t core_set_begin_date(ma_trigger_core_t *core, ma_task_time_t *start_date) {
    if(core) {        
        ma_error_t err = MA_ERROR_SCHEDULER_INVALID_DATE;
        ma_task_time_t *dp = NULL;

        if(start_date) {
            if(!validate_date(*start_date))
                MA_LOG(logger, MA_LOG_SEV_DEBUG,"trigger begin date(%hu/%hu/%hu %hu:%hu:%hu) older",core->begin_date.day, core->begin_date.month, core->begin_date.year,core->begin_date.hour, core->begin_date.minute, core->begin_date.secs); 
            memcpy(dp = &core->begin_date, start_date, sizeof(ma_task_time_t));
            err = MA_OK; 
            MA_LOG(logger, MA_LOG_SEV_DEBUG,"trigger begin date(%hu/%hu/%hu %hu:%hu:%hu)",dp->day, dp->month, dp->year,dp->hour, dp->minute, dp->secs); 
        } else {
            get_localtime(dp = (ma_task_time_t*)&core->begin_date);
            err = MA_OK;
            MA_LOG(logger, MA_LOG_SEV_DEBUG,"trigger begin date(%hu/%hu/%hu %hu:%hu:%hu)",dp->day, dp->month, dp->year,dp->hour, dp->minute, dp->secs); 
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_set_end_date(ma_trigger_core_t *core, ma_task_time_t *end_date) {
    if(core && end_date) {
        ma_error_t err = MA_OK;
        ma_task_time_t today = {0};
        ma_task_time_t *dp = NULL;
        
        if(memcmp(&today, end_date, sizeof(ma_task_time_t))) {
            err = MA_ERROR_SCHEDULER_INVALID_DATE;
            memcpy(dp = &core->end_date, end_date, sizeof(ma_task_time_t));
            MA_LOG(logger, MA_LOG_SEV_DEBUG,"trigger end date(%hu/%hu/%hu %hu:%hu:%hu)", dp->year, dp->month, dp->day, dp->hour, dp->minute, dp->secs);
            core->flags |= MA_TRIGGER_HAS_END_DATE;
            if(validate_end_date(*end_date)) {
                get_localtime(&today);
                if(is_last_day_later(*end_date, today))
                    err = MA_OK;
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_set_state(ma_trigger_core_t *core, ma_trigger_state_t state) {
    if(core) {
        core->state = state;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_set_until_date(ma_trigger_core_t *core, ma_task_time_t date) {
    return(core && memcpy(&core->until_date, &date, sizeof(ma_task_time_t)))?MA_OK:MA_ERROR_INVALIDARG;
}

ma_error_t core_set_task_handle(ma_trigger_core_t *core, ma_task_t *task) {
    if(core) {
        core->task = task;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_set_duration(ma_trigger_core_t *core, ma_uint32_t duration) {
    if(core) {
        core->duration = duration;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_set_interval(ma_trigger_core_t *core, ma_uint32_t interval) {
    if(core) {
        core->interval = interval;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_set_random_interval(ma_trigger_core_t *core, ma_uint16_t interval) {
    if(core) {
        core->random_interval = interval;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_set_disable(ma_trigger_core_t *core, ma_bool_t disable) {
    if(core) {
        disable ? MA_SET_BIT(core->flags, MA_TRIGGER_DISABLED) : MA_CLEAR_BIT(core->flags, MA_TRIGGER_DISABLED);
        core->state = MA_CHECK_BIT(core->flags, MA_TRIGGER_DISABLED) ? MA_TRIGGER_STATE_BLOCKED : MA_TRIGGER_STATE_NOT_STARTED;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_set_id(ma_trigger_core_t *core, const char *id) {
    if(core && id) {
        memset(core->id, 0, MA_MAX_TRIGGERID_LEN + 1);
        strncpy(core->id, id, MA_MAX_TRIGGERID_LEN);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_set_loop(ma_trigger_core_t *core, uv_loop_t *loop) {
    return(core && loop && (core->loop = loop))?MA_OK:MA_ERROR_INVALIDARG;
}

ma_error_t core_set_curr_retry_count(ma_trigger_core_t *core, int count) {
    if(core) {
        core->curr_retry_count = count;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_set_next_fire_date(ma_trigger_core_t *core, ma_task_time_t next_fire_date) {
    if(core) {
        ma_error_t err = MA_ERROR_INVALIDARG;
        ma_task_time_t dummy = {0};

        if(memcmp(&dummy, &next_fire_date, sizeof(ma_task_time_t))) {
            memcpy(&core->last_next_fire_date, &next_fire_date, sizeof(ma_task_time_t));
            err = MA_OK;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_set_last_fire_date(ma_trigger_core_t *core, ma_task_time_t date) {
    return (core && memcpy(&core->last_fire_date, &date, sizeof(ma_task_time_t)))?MA_OK:MA_ERROR_INVALIDARG;
}

ma_error_t core_set_next_until_date(ma_trigger_core_t *core) {
    if(core) {
        ma_error_t err = MA_OK;

        if(core->duration) {
            const char *task_id = NULL;
            ma_time_zone_t zone = MA_TIME_ZONE_LOCAL;
            ma_task_time_t tmp = {0};
            ma_task_time_t utc = {0};
            ma_task_time_t *dp = NULL;

            (void)ma_task_get_id(core->task, &task_id);
            advance_date_by_seconds(core->duration, core->next_fire_date, &core->until_date);
            memcpy(dp = &tmp, &core->until_date, sizeof(ma_task_time_t));
            if(MA_OK == (err = ma_task_get_time_zone(core->task, &zone))) {
                gzone_ptr = MA_LOCAL_ZONE_STR;
                if(MA_TIME_ZONE_UTC == zone) {
                    convert_local_to_utc(tmp, &utc);
                    dp = &utc;
                    gzone_ptr = MA_UTC_ZONE_STR;
                }
            }
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) trigger id(%s) until date(%hu/%hu/%hu %hu:%hu:%hu) zone(%s)", task_id ? task_id : MA_EMPTY, core->id, dp->year, dp->month, dp->day, dp->hour, dp->minute, dp->secs, gzone_ptr);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}



/* core getter(s) */
ma_error_t core_get_next_fire_date(ma_trigger_core_t *core, ma_task_time_t *date) {
    return(core && date && memcpy(date, &core->next_fire_date, sizeof(ma_task_time_t)))?MA_OK:MA_ERROR_INVALIDARG;
}

ma_error_t core_get_last_fire_date(ma_trigger_core_t *core, ma_task_time_t *date) {
    return (core && date && memcpy(date, &core->last_fire_date, sizeof(ma_task_time_t)))?MA_OK:MA_ERROR_INVALIDARG;
}

ma_error_t core_get_start_lock_status(ma_trigger_core_t *core, ma_bool_t *status) {
    if(core && status) {
        *status = core->start_lock;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_get_type(ma_trigger_core_t *core, ma_trigger_type_t *type) {
    if(core && type) {
        *type = core->type;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_get_state(ma_trigger_core_t *core, ma_trigger_state_t *state) {
    if(core && state) {
        *state = core->state;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_get_id(ma_trigger_core_t *core, const char **id) {
    if(core && id) {
        *id = core->id;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_get_begin_date(ma_trigger_core_t *core, ma_task_time_t *date) {
    if(core && date) {
        memcpy(date, &core->begin_date, sizeof(ma_task_time_t)); 
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_get_end_date(ma_trigger_core_t *core, ma_task_time_t *date) {
    if(core && date) {
        memcpy(date, &core->end_date, sizeof(ma_task_time_t));
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_get_next_run_time(ma_trigger_core_t *core, ma_task_time_t *date) {
    return(core && date && memcpy(date, &core->next_fire_date, sizeof(ma_task_time_t)))?MA_OK:MA_ERROR_INVALIDARG;
}

ma_error_t core_get_task_handle(ma_trigger_core_t *core, ma_task_t **task) {
    if(core && task) {
        *task = core->task;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_get_loop(ma_trigger_core_t *core, uv_loop_t **loop) {
    if(core && loop) {
        *loop = core->loop;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_get_duration(ma_trigger_core_t *core, ma_uint32_t *duration) {
    if(core && duration) {
        *duration = core->duration;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_get_interval(ma_trigger_core_t *core, ma_uint32_t *intervals) {
    if(core && intervals) {
        *intervals = core->interval;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_get_random_interval(ma_trigger_core_t *core, ma_uint16_t *interval) {
    if(core && interval) {
        *interval = core->random_interval;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_get_curr_retry_count(ma_trigger_core_t *core, int *count) {
    if(core && count) {
        *count = core->curr_retry_count;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t core_set_task(ma_trigger_core_t *core, ma_task_t *task) {
    if(core) {
        core->task = task;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_reset_start_lock(ma_trigger_core_t *core) {
    if(core) {
        core->start_lock = MA_FALSE;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_release(ma_trigger_core_t *core) {
    if(core) {
        ma_error_t err = MA_OK;
        const char *task_id = NULL;

        (void)ma_task_get_id(core->task, &task_id);
        if(!MA_ATOMIC_DECREMENT(core->ref_count)) {
            if(strcmp(core->id, "")) {
                if(MA_TRIGGER_STATE_EXPIRED == core->state) {
                    MA_LOG(logger, MA_LOG_SEV_TRACE, "task id(%s) trigger id(%s) released successfully", task_id ? task_id : MA_EMPTY, core->id);
                    if(core->core_data)
                        free(core->core_data); /* trigger released */
                } else {
                    if(MA_OK != (err = ma_trigger_core_close(core))) {
                        MA_LOG(logger, MA_LOG_SEV_ERROR, "task id(%s) trigger id(%s) close failed, last error(%d)", task_id ? task_id : MA_EMPTY, core->id, err);
                    }
                    core->ref_count = 1; /* bonus */
                }
            } else {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) trigger id(%s) released successfully", task_id ? task_id : MA_EMPTY, core->id);
                if(core->core_data)
                    free(core->core_data); /* trigger released */
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

void core_reset_task_exec_not_started_status(ma_task_t *task) {
    if(task) {
        ma_task_exec_status_t exec_status = MA_TASK_EXEC_NOT_STARTED;

        if(MA_OK == ma_task_get_execution_status(task, &exec_status)) {
            if((MA_TASK_EXEC_SUCCESS == exec_status) || (MA_TASK_EXEC_FAILED == exec_status) || 
                (MA_TASK_EXEC_STOPPED == exec_status) || (MA_TASK_EXEC_STOP_INITIATED_BY_SCHEDULER == exec_status)) {
                (void)ma_task_set_execution_status_internal(task, MA_TASK_EXEC_NOT_STARTED);
            }
        }
    }
}

ma_bool_t core_check_task_runnable(ma_task_t *task) {
    if(task) {
        ma_bool_t runnable = MA_TRUE;
        ma_uint32_t conds = 0;
        const char *task_id = NULL;

        (void)ma_task_get_id(task, &task_id);
        (void)ma_task_get_conditions(task, &conds);
        if(MA_CHECK_BIT(conds, MA_TASK_COND_DISABLED)) {
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) is disabled", task_id ? task_id : MA_EMPTY);
            runnable = MA_FALSE;
        } else if(MA_CHECK_BIT(conds, MA_TASK_COND_DONT_START_IF_ON_BATTERIES) || MA_CHECK_BIT(conds,MA_TASK_COND_ENFORCE_POWER_POLICY)) {
            ma_task_power_policy_t policy = {0};
            ma_power_sensor_msg_t *msg = NULL;

            (void)ma_task_get_power_policy(task, &policy);
            if(policy.on_battery) {
                if(MA_OK == ma_sensor_get_power_sensor_msg(&msg)) {
                    size_t size = 0;

                    if(MA_OK == ma_power_sensor_msg_get_size(msg, &size)) {
                        if(size) {
                            ma_power_status_t p_status = MA_POWER_STATUS_OFFLINE;

                            if(MA_OK == ma_power_sensor_msg_get_power_status(msg, size -1, &p_status)) {
                                if(MA_POWER_STATUS_OFFLINE == p_status) {
                                    unsigned long life_time_secs = 0; /* in secs */

                                    (void)ma_power_sensor_msg_get_battery_lifetime(msg, size - 1, &life_time_secs);
                                    if(life_time_secs) {
                                        unsigned long life_time_minutes = life_time_secs / 60; /* in minutes */

                                        runnable = policy.battery_life_time < life_time_minutes ? MA_FALSE : MA_TRUE;
                                    }
                                }
                            }
                        }
                    }
                    (void)ma_power_sensor_msg_release(msg);
                }
            }
        } else if(MA_CHECK_BIT(conds, MA_TASK_COND_RUN_IF_CONNECTED_TO_INTERNET)) {
            ma_network_sensor_msg_t *msg = NULL;

            if(MA_OK == ma_sensor_get_network_sensor_msg(&msg)) {
                ma_network_connectivity_status_t status = MA_NETWORK_CONNECTIVITY_STATUS_DISCONNECTED;
                
                (void)ma_network_sensor_msg_get_connectivity_status(msg, &status);
                runnable = MA_NETWORK_CONNECTIVITY_STATUS_DISCONNECTED == status ? MA_FALSE : MA_TRUE;

                (void)ma_network_sensor_msg_release(msg);
            }
        } else {
            /* do nothing */
        }

        return runnable;
    }
    return MA_FALSE;
}

void core_compute_trigger_interval_count(ma_trigger_core_t *trigger) {
    if(trigger) {
        if((trigger->interval > 0) && (trigger->interval < trigger->duration)) {
            trigger->no_of_intervals = trigger->duration/trigger->interval;
        }
    }
}

void core_timer_destroy(ma_trigger_core_t *core) {
    if(core) {
        ma_task_t *task = core->task;
        ma_scheduler_t *scheduler = NULL;

        if((MA_OK == ma_task_get_scheduler(task, &scheduler)) && scheduler) {
            ma_running_task_collector_t *rt = NULL;

            if(MA_OK == ma_scheduler_get_running_task_collector_handle(scheduler, &rt))
                (void)ma_running_task_collector_remove(rt, task);
            (void)ma_scheduler_internal_release_task(scheduler, task);
        } else {
            ma_bool_t is_task_stopped = MA_FALSE;
                        
            if((MA_OK == ma_check_task_stopped(task, &is_task_stopped)) && is_task_stopped)
                (void)ma_task_release(task);
        }
    }
}

ma_error_t core_add_ref(ma_trigger_core_t *core) {
    if(core) {
        MA_ATOMIC_INCREMENT(core->ref_count);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

/* trigger polymorphic method(s) */
void ma_trigger_core_init(ma_trigger_core_t *core, const ma_trigger_core_methods_t *methods, ma_trigger_type_t type, ma_watcher_type_t watcher_type, void *data) {
    if(core) {
        core->core_methods = methods;
        core->watcher_type = watcher_type;
        core->type = type;
        core->state = MA_TRIGGER_STATE_NOT_STARTED;
        core->core_data = data;
    }
}


