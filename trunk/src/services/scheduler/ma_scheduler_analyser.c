#include "ma/internal/services/scheduler/ma_scheduler_base.h"
#include "ma/internal/services/scheduler/ma_scheduler_analyser.h"
#include "ma/internal/services/scheduler/ma_scheduler_task.h"
#include "ma/ma_scheduler_message.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MA_MAX_TASK_SLOTS 4096


static ma_error_t ma_scheduler_analyser_on_message(ma_scheduler_base_t *base, ma_scheduler_message_t *msg) {
    if(base && msg) {
        ma_error_t err = MA_OK;
        ma_scheduler_analyser_t *a = (ma_scheduler_analyser_t*)base;
        ma_bool_t is_task_for_today = MA_FALSE;
        ma_scheduler_task_time_t local_time = {0};

        ma_scheduler_get_localtime(&local_time);
        if((MA_OK == (err = ma_scheduler_task_for_today(&msg->schedule_time, &local_time, &is_task_for_today))) && is_task_for_today){
            size_t slotid = 0;
            if((MA_OK == (err = ma_scheduler_get_slotid(a, &slotid))) && slotid) {
                ma_scheduler_task_t *task = NULL;
                if(MA_OK == (err = ma_scheduler_task_create(&task))) {
                    if(MA_OK == (err = ma_scheduler_task_set_attributes(task, msg, slotid))) {

                        a->slots[slotid] = task;                        
                    }
                }
            }
        } else {
            //TODO :: drop into db
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static void ma_scheduler_analyser_destroy(ma_scheduler_base_t *base) {
    if(base) {
        ma_scheduler_analyser_t *a = (ma_scheduler_analyser_t*)base;
        ma_scheduler_task_slots_deinit(a);
    }
}

static const ma_scheduler_base_methods_t analyser_methods = {
    &ma_scheduler_analyser_on_message,
    &ma_scheduler_analyser_destroy
};

void ma_scheduler_analyser_init(ma_scheduler_analyser_t *a, uv_loop_t *loop, void *data) {
    ma_scheduler_base_init((ma_scheduler_base_t*)a, &analyser_methods, data);
    a->loop = loop;
}

ma_error_t ma_scheduler_task_slots_init(ma_scheduler_analyser_t *a) {
    if(a && a->slots) {
        if(a->slots = (ma_scheduler_task_t**)calloc(MA_MAX_TASK_SLOTS, sizeof(ma_scheduler_task_t*))) {
            a->capacity = MA_MAX_TASK_SLOTS;
            return MA_OK;
        }
        return MA_ERROR_OUTOFMEMORY;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_scheduler_task_slots_reserve(ma_scheduler_analyser_t *a, size_t size) {
    if(a && a->slots && size) {
        realloc(a->slots, a->capacity * sizeof(ma_scheduler_task_t*));
        if(!a->slots) {
            return MA_ERROR_OUTOFMEMORY;
        }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

void ma_scheduler_task_slots_deinit(ma_scheduler_analyser_t *a) {
    if(a && a->slots && a->capacity) {
        size_t itr = 0;
        for(itr = 0; itr < a->size; ++itr) {
            ma_scheduler_task_release(a->slots[itr]);
        }
        free(a->slots);
    }
}

ma_error_t ma_scheduler_get_slotid(ma_scheduler_analyser_t *a, size_t *slotid) {
    if(a && slotid) {
        ma_error_t err = MA_OK;

        *slotid = 0; //reset
        if(a->slots && !*a->slots ) {
            err = ma_scheduler_task_slots_init(a);
        } 
        if(a->size+1 > a->capacity) {
            err = ma_scheduler_task_slots_reserve(a, (a->capacity += a->capacity));
        }
        ++a->size;
        *slotid = a->size;

        return err;
    }
    return MA_ERROR_INVALIDARG;
}