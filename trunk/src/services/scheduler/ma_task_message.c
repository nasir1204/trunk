#include "ma/scheduler/ma_task_message.h"
#include "ma/internal/ma_macros.h"
#include "ma/scheduler/ma_scheduler_defs.h"
#include "ma/ma_variant.h"

#include <string.h>
#include <stdlib.h>

struct ma_task_message_s {
    char id[MA_MAX_TASKID_LEN];
    ma_uint32_t cond;
    ma_trigger_message_t *trigger_message_list;
    ma_uint16_t retry_count; // error retry_count
    ma_uint16_t retry_interval;
    ma_variant_t *app_pay_load;
    ma_uint16_t max_run_time;//minutes
    ma_uint16_t max_run_time_limit;//minutes
    ma_uint16_t missed_interval;
    ma_task_time_t last_run_time;
    ma_task_time_t next_run_time;
    ma_task_priority_t priority;
    char *creator_id;
};

struct ma_trigger_message_s {
	char id[MA_MAX_TRIGGERID_LEN];
    ma_task_time_t begin_date;
    ma_task_time_t end_date;
    ma_uint16_t random_interval;
    size_t no_of_intervals;
    ma_uint16_t duration;
    ma_uint16_t interval;
    ma_trigger_type_t type;
};

ma_error_t ma_task_message_create(ma_task_message_t **msg) {
	ma_error_t res = MA_ERROR_INVALIDARG;

	if(msg) {
		res = MA_ERROR_OUTOFMEMORY;
		if(*msg = ((ma_task_message_t *) calloc(1, sizeof(ma_task_message_t)))) {
			res = MA_OK;
		}
	}
	return res;
}
