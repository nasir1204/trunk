#include "ma/scheduler/ma_triggers.h"
#include "ma/internal/services/scheduler/ma_trigger_core.h"
#include "ma/scheduler/ma_task.h"
#include "ma/logger/ma_file_logger.h"
#include "ma/internal/services/scheduler/ma_triggers_internal.h"
#include "ma/internal/services/scheduler/ma_task_internal.h"

#include <string.h>
#include <stdlib.h>

extern ma_logger_t *logger;


#define MA_TRIGGER_CORE_ARG_EXPAND(ret_type, function, signature, arg, cargs)\
    ret_type trigger_##function signature {\
        return trigger ? core_##function cargs : MA_ERROR_INVALIDARG;\
    }
    MA_TRIGGER_CORE_ACTUAL_ARG_EXPANDER
#undef MA_TRIGGER_CORE_ARG_EXPAND

        
#define MA_TRIGGER_EXTN_CORE_ARG_EXPAND(ret_type, function, signature, arg, cargs)\
    ret_type ma_trigger_##function signature {\
        return trigger ? core_##function cargs : MA_ERROR_INVALIDARG;\
    }
    MA_TRIGGER_EXTN_CORE_ACTUAL_ARG_EXPANDER
#undef MA_TRIGGER_EXTN_CORE_ARG_EXPAND



/* trigger op(s) api */

ma_error_t ma_trigger_start(ma_trigger_t *trigger) {
    return(trigger && trigger->data)?ma_trigger_core_fire((ma_trigger_core_t*)trigger->data):MA_ERROR_INVALIDARG;
}


ma_error_t ma_trigger_stop(ma_trigger_t *trigger) {
    return trigger?ma_trigger_core_stop_fire((ma_trigger_core_t*)trigger->data):MA_ERROR_INVALIDARG;
}

ma_error_t ma_trigger_close(ma_trigger_t *trigger) {
    return trigger?ma_trigger_core_close((ma_trigger_core_t*)trigger->data):MA_ERROR_INVALIDARG;
}



