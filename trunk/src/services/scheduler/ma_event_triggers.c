#include "ma/internal/services/scheduler/ma_scheduler_utils.h"
#include "ma/internal/ma_macros.h"
#include "ma/logger/ma_file_logger.h"
#include "ma/internal/services/scheduler/ma_runningtask_collector.h"
#include "ma/internal/services/scheduler/ma_task_internal.h"
#include "ma/internal/services/scheduler/ma_triggers_internal.h"
#include "ma/internal/services/scheduler/ma_scheduler_core.h"
#include "ma/internal/services/scheduler/ma_trigger_core.h"

#include <string.h>
#include <stdlib.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "scheduler"

extern ma_logger_t *logger;

MA_CPP(extern "C" {)
ma_error_t event_trigger_get_policy(ma_trigger_t *trigger, void *policy);

struct ma_trigger_system_idle_s {
    ma_trigger_t trigger_impl;
    ma_trigger_core_t core;
    ma_uint16_t idle_wait_minutes;
};
struct ma_trigger_powersave_s {
    ma_trigger_t trigger_impl;
    ma_trigger_core_t core;
    ma_uint16_t powersave_minutes;
    ma_uint16_t powersave_deadline_minutes;
};
struct ma_trigger_dialup_s {
    ma_trigger_t trigger_impl;
    ma_trigger_core_t core;
    ma_bool_t daily_once;
};
struct ma_trigger_system_start_s {
    ma_trigger_t trigger_impl;
    ma_trigger_core_t core;
    ma_bool_t daily_once;
    ma_bool_t run_once;
};
struct ma_trigger_logon_s {
    ma_trigger_t base;
    ma_trigger_core_t core;
    ma_bool_t daily_once;
};



static const ma_trigger_methods_t trigger_methods = {
    &trigger_set_begin_date,
    &trigger_set_end_date,
    &trigger_get_type,
    &trigger_get_state,
    &trigger_get_id,
    &trigger_get_begin_date,
    &trigger_get_end_date,
    &event_trigger_get_policy,
    &trigger_set_state,
    &trigger_release,
    &trigger_add_ref,
    &trigger_set_next_fire_date,
    &trigger_get_next_fire_date,
    &trigger_set_last_fire_date,
    &trigger_get_last_fire_date,
};


ma_error_t event_trigger_get_policy(ma_trigger_t *trigger, void *policy) {
     if(trigger && policy) {
         ma_error_t err = MA_OK;

         switch(((ma_trigger_core_t*)trigger->data)->type) {
            case MA_TRIGGER_ON_SYSTEM_IDLE: {
                ((ma_trigger_system_idle_policy_t*)policy)->idle_wait_minutes = ((ma_trigger_system_idle_t*)trigger)->idle_wait_minutes;
                break;
            }
            case MA_TRIGGER_AT_SYSTEM_START: {
                ((ma_trigger_systemstart_policy_t*)policy)->daily_once = ((ma_trigger_system_start_t*)trigger)->daily_once;
                ((ma_trigger_systemstart_policy_t*)policy)->run_just_once = ((ma_trigger_system_start_t*)trigger)->run_once;
                break;
            }
            case MA_TRIGGER_AT_LOGON: {
                ((ma_trigger_logon_policy_t*)policy)->daily_once = ((ma_trigger_system_start_t*)trigger)->daily_once;
                break;
            }
            default: {
                err = MA_ERROR_NOT_SUPPORTED;
                break;
            }
         }

         return err;
     }
     return MA_ERROR_INVALIDARG;
}

static const ma_trigger_core_methods_t system_idle_core_methods = {
    &core_notify_event_fire,
    &core_notify_event_timer_next_fire,
    0,
    &core_daily_stop_fire,
    &core_notify_event_force_reset,
    &core_notify_force_stop,
    &core_notify_timer_fire_close
};

ma_error_t ma_trigger_create_system_idle(ma_trigger_system_idle_policy_t *policy, ma_trigger_t **trigger) {
    if(policy && trigger) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;
        ma_trigger_system_idle_t *idle = NULL;

        *trigger = NULL;
        if((idle = (ma_trigger_system_idle_t*)calloc(1, sizeof(ma_trigger_system_idle_t)))) {
            idle->idle_wait_minutes = policy->idle_wait_minutes;
            ma_trigger_init((*trigger = (ma_trigger_t*)idle), &trigger_methods, &idle->core);
            ma_trigger_core_init((ma_trigger_core_t*)&idle->core, &system_idle_core_methods, MA_TRIGGER_ON_SYSTEM_IDLE, MA_WATCHER_TIMER, idle);
            advance_current_date_by_seconds(30, &idle->core.begin_date);
            if(MA_OK == (err = ma_trigger_set_begin_date(*trigger, &idle->core.begin_date))) {
                err = ma_trigger_add_ref(*trigger);
            }
        }
        if(MA_OK == err) {
            MA_LOG(logger, MA_LOG_SEV_DEBUG,"scheduler system idle trigger created successfully");
        } else {
            MA_LOG(logger, MA_LOG_SEV_DEBUG,"scheduler system idle trigger creation failed, last error(%d)", err);
            free(*trigger);
            *trigger = NULL;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_trigger_create_power_save(ma_trigger_powersave_policy_t *policy, ma_trigger_t **powersave) {
    if(policy && powersave) {
        ma_error_t err = MA_OK;
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_trigger_create_dialup(ma_trigger_dialup_policy_t *policy, ma_trigger_t **dailup) {
    if(policy && dailup) {
        ma_error_t err = MA_OK;
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static const ma_trigger_core_methods_t system_start_core_methods = {
    &core_notify_event_fire,
    &core_notify_system_startup_event_timer_next_fire,
    0,
    &core_daily_stop_fire,
    &core_notify_event_force_reset,
    &core_notify_force_stop,
    &core_notify_timer_fire_close
};


ma_error_t ma_trigger_create_system_start(ma_trigger_systemstart_policy_t *policy, ma_trigger_t **trigger) {
    if(policy && trigger) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;
        ma_trigger_system_start_t *sys_start = NULL;

        *trigger = NULL;
        if((sys_start = (ma_trigger_system_start_t*)calloc(1, sizeof(ma_trigger_system_start_t)))) {
            sys_start->daily_once = policy->daily_once;
            sys_start->run_once =  policy->run_just_once;
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "system start trigger policy daily once(%d) run once(%d) settings", policy->daily_once, policy->run_just_once);
            ma_trigger_init((*trigger = (ma_trigger_t*)sys_start), &trigger_methods, &sys_start->core);
            ma_trigger_core_init((ma_trigger_core_t*)&sys_start->core, &system_start_core_methods, MA_TRIGGER_AT_SYSTEM_START, MA_WATCHER_TIMER, sys_start);
            advance_current_date_by_seconds(30, &sys_start->core.begin_date);
            if(MA_OK == (err = ma_trigger_set_begin_date(*trigger, &sys_start->core.begin_date))) {
                err = ma_trigger_add_ref(*trigger);
            }
        }
        if(MA_OK == err) {
            MA_LOG(logger, MA_LOG_SEV_DEBUG,"scheduler system start trigger created successfully");
        } else {
            MA_LOG(logger, MA_LOG_SEV_DEBUG,"scheduler system start trigger creation failed, last error(%d)", err);
            free(*trigger);
            *trigger = NULL;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static const ma_trigger_core_methods_t logon_core_methods = {
    &core_notify_event_fire,
    &core_notify_event_timer_next_fire,
    0,
    &core_daily_stop_fire,
    &core_notify_event_force_reset,
    &core_notify_force_stop,
    &core_notify_timer_fire_close
};

ma_error_t ma_trigger_create_logon(ma_trigger_logon_policy_t *policy, ma_trigger_t **trigger) {
    if(policy && trigger) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;
        ma_trigger_logon_t *logon = NULL;

        *trigger = NULL;
        if((logon = (ma_trigger_logon_t*)calloc(1, sizeof(ma_trigger_logon_t)))) {
            logon->daily_once = policy->daily_once;
            ma_trigger_init((*trigger = (ma_trigger_t*)logon), &trigger_methods, &logon->core);
            ma_trigger_core_init((ma_trigger_core_t*)&logon->core, &logon_core_methods, MA_TRIGGER_AT_LOGON, MA_WATCHER_TIMER, logon);
            advance_current_date_by_seconds(30, &logon->core.begin_date);
            if(MA_OK == (err = ma_trigger_set_begin_date(*trigger, &logon->core.begin_date))) {
                err = ma_trigger_add_ref(*trigger);
            }
        }
        if(MA_OK == err) {
            MA_LOG(logger, MA_LOG_SEV_DEBUG,"scheduler logon trigger created successfully");
        } else {
            MA_LOG(logger, MA_LOG_SEV_DEBUG,"scheduler logon trigger creation failed, last error(%d)", err);
            free(*trigger);
            *trigger = NULL;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}


MA_CPP(})
