#include "ma/internal/services/scheduler/ma_taskfolder.h"
#include "ma/internal/utils/threading/ma_atomic.h"
#include "ma/scheduler/ma_task.h"
#include "ma/logger/ma_file_logger.h"
#include "ma/scheduler/ma_triggerlist.h"
#include "ma/internal/defs/ma_scheduler_defs.h"
#include "ma/ma_datastore.h"
#include "uv-private/tree.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include "ma/internal/services/scheduler/ma_triggers_internal.h"
#include "ma/internal/services/scheduler/ma_task_internal.h"
#include "ma/internal/services/scheduler/ma_scheduler_datastore.h"
#include "ma/internal/services/scheduler/ma_trigger_core.h"
#include "ma/ma_message.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/ma_macros.h"
#include "ma/scheduler/ma_enumerator.h"

#include <string.h>
#include <stdlib.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "scheduler"

extern ma_logger_t *logger;

MA_CPP(extern "C" {)
ma_error_t ma_task_get_scheduler(ma_task_t *task, ma_scheduler_t **scheduler);
ma_error_t ma_scheduler_get_msgbus(ma_scheduler_t *scheduler, ma_msgbus_t **msgbus);
ma_error_t ma_trigger_start(ma_trigger_t *trigger);
ma_error_t ma_trigger_stop(ma_trigger_t *trigger);
ma_error_t ma_taskfolder_expired_task_group_add(ma_taskfolder_t *folder, ma_task_t *task);
ma_error_t ma_taskfolder_expired_task_group_release(ma_taskfolder_t *folder);
MA_CPP(})

typedef struct ma_tasktree_s ma_tasktree_t;
typedef struct ma_tasknode_t ma_tasknode_t;

struct ma_tasknode_t {
    RB_ENTRY(ma_tasknode_t) tree_entry;
    ma_task_t *task_;
};

typedef struct data_s {
    ma_task_t **task_array;
    size_t curr_index;
}data_t;


RB_HEAD (ma_tasktree_s, ma_tasknode_t);
typedef ma_error_t (*MA_TASK_NODE_FOR_EACH_CALLBACK) (ma_tasknode_t **node, void *data);

struct ma_taskfolder_s {
    ma_tasktree_t taskmap;
    ma_task_t **exp_task_group;
    size_t no_of_exp_tasks;
    size_t exp_task_group_capacity;
    size_t taskmap_size;
    ma_taskfolder_status_t status;
    uv_loop_t *loop;
    ma_ds_t* datastore;
    ma_scheduler_t *scheduler;
    char ds_path[MA_MAX_LEN];
};

static int task_comparator(const ma_tasknode_t *left, const ma_tasknode_t *right);
static ma_error_t notfiy_event_foreach_task(ma_tasknode_t **task, void *data);
static ma_error_t task_tree_node_restart(ma_tasknode_t **node, void *data);
static ma_error_t schedule_system_startup_task(ma_task_t *task, ma_trigger_t *trigger);
static ma_tasknode_t *find_task(ma_tasktree_t *taskmap, ma_task_t * task);
static ma_error_t task_tree_node_destroy(ma_tasknode_t **node, void *data);
static ma_error_t task_tree_node_start(ma_tasknode_t **node, void *data);
static ma_error_t task_tree_node_stop(ma_tasknode_t **node, void *data);
static ma_error_t persist_node(ma_tasknode_t **node, void *data);
static ma_error_t destroy_node(ma_tasknode_t **node, void *data);
static ma_error_t task_tree_node_dispose(ma_tasknode_t **node, void *data);
static ma_error_t task_tree_node_filldata(ma_tasknode_t **node, void *data);
static ma_error_t MA_TASK_TREE_ITERATE_012(ma_tasknode_t **node, MA_TASK_NODE_FOR_EACH_CALLBACK fp, void *data);
static ma_error_t enumerate_task_no_match(ma_taskfolder_t *folder, ma_task_t ***task_set, size_t *no_of_tasks);
static ma_error_t MA_TASK_TREE_ITERATE_021(ma_tasknode_t **node, MA_TASK_NODE_FOR_EACH_CALLBACK fp, void *data);
static ma_error_t MA_TASK_TREE_ITERATE_102(ma_tasknode_t **node, MA_TASK_NODE_FOR_EACH_CALLBACK fp, void *data);
static ma_error_t MA_TASK_TREE_ITERATE_201(ma_tasknode_t **node, MA_TASK_NODE_FOR_EACH_CALLBACK fp, void *data);
static ma_error_t MA_TASK_TREE_ITERATE_120(ma_tasknode_t **node, MA_TASK_NODE_FOR_EACH_CALLBACK fp, void *data);
static ma_error_t MA_TASK_TREE_ITERATE_210(ma_tasknode_t **node, MA_TASK_NODE_FOR_EACH_CALLBACK fp, void *data);
static ma_error_t schedule_logon_task(ma_task_t *task, ma_trigger_t *trigger);
static ma_error_t schedule_task_on_conditions(ma_task_t *task);
RB_GENERATE_STATIC(ma_tasktree_s, ma_tasknode_t, tree_entry, task_comparator);

int task_comparator(const ma_tasknode_t *left, const ma_tasknode_t *right) {
    if(left && right) {
        ma_error_t err = MA_OK;
        const char *left_id = NULL;
        const char *right_id = NULL;

        if(MA_OK != (err = ma_task_get_id(left->task_, &left_id))) {
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "left task id extraction failed, last error(%d)", err);
        }
        if(MA_OK != (err = ma_task_get_id(right->task_, &right_id))) {
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "right task id extraction failed, last error(%d)", err);
        } 

        return(left_id && right_id) ? strcmp(left_id, right_id) : 0;
    }
    return 0; /* default left branch */
}  

ma_tasknode_t *find_task(ma_tasktree_t *taskmap, ma_task_t *task) {
    if(taskmap && task) {
        ma_tasknode_t search_node = {{0}};
        ma_tasknode_t *ret_node = NULL;

        search_node.task_ = task;
        ret_node = RB_FIND(ma_tasktree_s, taskmap, &search_node);
        return ret_node;
    }
    return NULL;
}


ma_error_t task_tree_node_destroy(ma_tasknode_t **node, void *data) {
    if(node && *node) {
        ma_error_t err = MA_OK;
        const char *task_id = NULL;

        (void)ma_task_get_id((*node)->task_, &task_id);
        MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) destroyed", task_id ? task_id : MA_EMPTY);
        if(MA_OK == (err = ma_task_release((*node)->task_))) {
            free(*node);
            *node = NULL;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t task_tree_node_dispose(ma_tasknode_t **node, void *data) {
    if(node && *node) {
        ma_error_t err = MA_OK;
        ma_trigger_list_t *tl = NULL;
        ma_trigger_t *trigger = NULL;
        const char *task_id = NULL;

        (void)ma_task_get_id((*node)->task_, &task_id);
        (void)ma_task_get_trigger_list((*node)->task_, &tl);
        (void)ma_enumerator_reset((ma_enumerator_t*)tl);
        while(MA_OK == ma_enumerator_next((ma_enumerator_t*)tl, &trigger)) {
            ma_trigger_set_state(trigger, MA_TRIGGER_STATE_EXPIRED);
        }
        if(MA_OK == (err = ma_task_release((*node)->task_))) {
            free(*node);
            *node = NULL;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t task_tree_node_start(ma_tasknode_t **node, void *data) {
    if(node && *node) {
        const char *task_id = NULL;
        const char *task_name = NULL;
        ma_uint32_t cond = 0;

        (void)ma_task_get_conditions((*node)->task_, &cond);
        (void)ma_task_get_id((*node)->task_, &task_id);
        (void)ma_task_get_name((*node)->task_, &task_name);
        //MA_LOG(logger, MA_CHECK_BIT(cond, MA_TASK_COND_HIDDEN) ? MA_LOG_SEV_INFO : MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "The task %s has been invoked by Scheduler", !strcmp(task_name, MA_EMPTY) ? task_id ? task_id : MA_EMPTY : task_name);

        return ma_task_start((*node)->task_);
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t task_tree_node_stop(ma_tasknode_t **node, void *data) {
    if(node && *node) {
        const char *task_id = NULL;
        ma_trigger_list_t *tl = NULL;
        ma_trigger_t *trigger = NULL;
        ma_bool_t publish_stop_topic = MA_FALSE;

        (void)ma_task_get_id((*node)->task_, &task_id);
        (void)ma_task_get_trigger_list((*node)->task_, &tl);
        ma_enumerator_reset((ma_enumerator_t*)tl);
        while(MA_OK == ma_enumerator_next((ma_enumerator_t*)tl, &trigger)) {
            ma_trigger_state_t state = MA_TRIGGER_STATE_NOT_STARTED;

            (void)ma_trigger_get_state(trigger, &state);
            if(MA_TRIGGER_STATE_RUNNING == state) {
                publish_stop_topic = MA_TRUE;
            }
        }
        if(publish_stop_topic)
            (void)ma_task_publish_topics((*node)->task_, MA_TASK_STOP_PUBSUB_TOPIC);        
        MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) stopping...", task_id ? task_id : MA_EMPTY);
        return ma_task_stop((*node)->task_);
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t task_tree_node_filldata(ma_tasknode_t **node, void *data) {
    data_t *filldata = (data_t *) data;

    (void)ma_task_add_ref(filldata->task_array[filldata->curr_index++] = (*node)->task_);
    return MA_OK;
}

static ma_error_t persist_node(ma_tasknode_t **node, void *data) {
    if(node && *node && data) {
        ma_error_t err = MA_OK;
        ma_taskfolder_t *folder = (ma_taskfolder_t*)data;
		ma_task_t *task = (*node)->task_;
		const char *task_id = NULL;

		(void)ma_task_get_id(task, &task_id);

        if(MA_OK != (err = ma_scheduler_datastore_write(folder->scheduler, task, folder->datastore, folder->ds_path)))
			MA_LOG(logger, MA_LOG_SEV_WARNING, "task id(%s) persistence failed, last error(%d)", task_id, err);

        return err;
    }
    return  MA_ERROR_INVALIDARG;
}

static ma_error_t destroy_node(ma_tasknode_t **node, void *data) {
    if(node && *node && data) {
        ma_error_t err = MA_OK;
        ma_task_t *task = (*node)->task_;
        size_t no_of_triggers = 0;
        const char *task_id = NULL;
        ma_trigger_list_t *tl = NULL;

        (void)ma_task_get_id(task, &task_id);
        (void)ma_task_get_trigger_list(task, &tl);

        MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) releasing...", task_id);

        (void)ma_task_set_condition(task, MA_TASK_COND_DELETE_WHEN_DONE);
        if(MA_OK == (err = ma_task_size(task, &no_of_triggers))) {
            if(no_of_triggers) {
				size_t itr = 0;
                for(itr = 0; itr < no_of_triggers; ++itr) {
                    const char *tid = NULL;
                    ma_trigger_t *trigger = NULL;

                    if(MA_OK == (err = ma_trigger_list_at(tl, itr, &trigger))) {
                        (void)ma_trigger_release(trigger); /* intentional */
                        (void)ma_trigger_get_id(trigger, &tid);
                        if(MA_OK != (err = ma_task_remove_trigger(task, tid))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) trigger id(%s) remove failed, last error(%d)", task_id, tid, err);
                        }
                    }
                }
            } else
                err = ma_task_release((*node)->task_);
        }
        return err;
    }
    return  MA_ERROR_INVALIDARG;
}


/* pre-order traversal */
ma_error_t MA_TASK_TREE_ITERATE_012(ma_tasknode_t **node, MA_TASK_NODE_FOR_EACH_CALLBACK fp, void *data) {
    if(node && fp && *fp) {
        if(*node) {
            (*fp)(node, data);
            MA_TASK_TREE_ITERATE_012(&(*node)->tree_entry.rbe_left, fp, data);
            MA_TASK_TREE_ITERATE_012(&(*node)->tree_entry.rbe_right, fp, data);
        }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t MA_TASK_TREE_ITERATE_021(ma_tasknode_t **node, MA_TASK_NODE_FOR_EACH_CALLBACK fp, void *data) {
    if(node && fp && *fp) {
        if(*node) {
            (*fp)(node, data);
            MA_TASK_TREE_ITERATE_021(&(*node)->tree_entry.rbe_right, fp, data);
            MA_TASK_TREE_ITERATE_021(&(*node)->tree_entry.rbe_left, fp, data);
        }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

/* In-order traversal */
ma_error_t MA_TASK_TREE_ITERATE_102(ma_tasknode_t **node, MA_TASK_NODE_FOR_EACH_CALLBACK fp, void *data) {
    if(node && fp && *fp) {
        if(*node) {
            MA_TASK_TREE_ITERATE_102(&(*node)->tree_entry.rbe_left, fp, data);
            (*fp)(node, data);
            MA_TASK_TREE_ITERATE_102(&(*node)->tree_entry.rbe_right, fp, data);
        }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t MA_TASK_TREE_ITERATE_201(ma_tasknode_t **node, MA_TASK_NODE_FOR_EACH_CALLBACK fp, void *data) {
    if(node && fp && *fp) {
        if(*node) {
            MA_TASK_TREE_ITERATE_201(&(*node)->tree_entry.rbe_right, fp, data);
            (*fp)(node, data);
            MA_TASK_TREE_ITERATE_201(&(*node)->tree_entry.rbe_left, fp, data);
        }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

/* Post-order traversal */
ma_error_t MA_TASK_TREE_ITERATE_120(ma_tasknode_t **node, MA_TASK_NODE_FOR_EACH_CALLBACK fp, void *data) {
    if(node && fp && *fp) {
        if(*node) {
            MA_TASK_TREE_ITERATE_120(&(*node)->tree_entry.rbe_left, fp, data);
            MA_TASK_TREE_ITERATE_120(&(*node)->tree_entry.rbe_right, fp, data);
            (*fp)(node, data);
        }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t MA_TASK_TREE_ITERATE_210(ma_tasknode_t **node, MA_TASK_NODE_FOR_EACH_CALLBACK fp, void *data) {
    if(node && fp && *fp) {
        if(*node) {
            MA_TASK_TREE_ITERATE_210(&(*node)->tree_entry.rbe_right, fp, data);
            MA_TASK_TREE_ITERATE_210(&(*node)->tree_entry.rbe_left, fp, data);
            (*fp)(node, data);
        }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

#define MA_TASK_TREE_TRAVERSE(traversal_order, node, fp, data) MA_TASK_TREE_ITERATE_##traversal_order(node, fp, data)


ma_error_t ma_taskfolder_create(ma_scheduler_t *scheduler, uv_loop_t *loop, ma_ds_t *ds, const char *base_path, ma_taskfolder_t **folder) {
    if(loop && ds && base_path && folder) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;

        if((*folder = (ma_taskfolder_t*) calloc(1, sizeof(ma_taskfolder_t)))) {
            (*folder)->datastore = ds;
            strncpy((*folder)->ds_path, base_path, MA_MAX_LEN - 1);
            (*folder)->loop = loop;
            (*folder)->status = MA_TASKFOLDER_STATUS_NOT_STARTED;
            (*folder)->scheduler = scheduler;
            (*folder)->taskmap_size = 0U; err = MA_OK;
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler task folder created successfully");
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_taskfolder_release(ma_taskfolder_t *folder) {
    if(folder) {
        ma_error_t err = MA_OK;

        if(MA_TASKFOLDER_STATUS_STARTED == folder->status) {
            if(MA_OK != (err = ma_taskfolder_stop(folder)))
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler task folder stop failed, last error(%d)", err);
        } else if(MA_TASKFOLDER_STATUS_NOT_STARTED == folder->status) {			
            if(MA_OK != (err = MA_TASK_TREE_TRAVERSE(120, &folder->taskmap.rbh_root, task_tree_node_dispose, folder))) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler folder disposed failed, last error(%d)", err);
            }
        } else {
            /* do nothing */
        }
        (void)ma_taskfolder_expired_task_group_release(folder);
        if(folder->exp_task_group)free(folder->exp_task_group);
        free(folder);
        MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler task folder released successfully");

        return MA_OK; /* intentional */
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_taskfolder_start(ma_taskfolder_t *folder) {
    if(folder) {
        ma_error_t err = MA_OK;

        MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler task folder starting...", folder->status);
        if((MA_TASKFOLDER_STATUS_NOT_STARTED == folder->status) || (MA_TASKFOLDER_STATUS_STOPPED == folder->status)) {
            folder->status = MA_TASKFOLDER_STATUS_STARTING;
            if(MA_OK == (err = ma_scheduler_datastore_read(folder->scheduler, folder, folder->datastore, folder->ds_path))) {
                (void)MA_TASK_TREE_TRAVERSE(120, &folder->taskmap.rbh_root, task_tree_node_start, NULL);
                (void)ma_taskfolder_expired_task_group_release(folder);
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler task folder started successfully");
            } else
                MA_LOG(logger, MA_LOG_SEV_ERROR, "scheduler db path(%s) read failed, last error(%d)", folder->ds_path, err);
            folder->status = MA_TASKFOLDER_STATUS_STARTED;
        }
        if(MA_OK != err)
            MA_LOG(logger, MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "Failed to start task scheduler");

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_taskfolder_stop(ma_taskfolder_t *folder) {
    if(folder) {
        ma_error_t err = MA_OK;

		/*NOTE: we should not propagte failures as we need to stop */

        if(MA_TASKFOLDER_STATUS_STARTED == folder->status) {
            folder->status = MA_TASKFOLDER_STATUS_STOPPING;
            MA_TASK_TREE_TRAVERSE(201, &folder->taskmap.rbh_root, task_tree_node_stop, NULL);
            
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler datastore path(%s), total tasks (%d)", folder->ds_path, folder->taskmap_size);

			if(MA_OK == (err = ma_ds_database_transaction_begin((ma_ds_database_t *)folder->datastore))) {
				/* clear scheduler datastore before persisting new one*/
				err = ma_scheduler_datastore_clear(folder->datastore, folder->ds_path);
				/* first time access to datastore may not have ds_path. So, ignore MA_ERROR_DS_PATH_NOT_FOUND error*/
				if((MA_OK == err) || (MA_ERROR_DS_PATH_NOT_FOUND == err)) {
					(void)MA_TASK_TREE_TRAVERSE(120, &folder->taskmap.rbh_root, persist_node, folder);
					(void)ma_ds_database_transaction_end((ma_ds_database_t *)folder->datastore);
				}else {
					MA_LOG(logger, MA_LOG_SEV_ERROR, "failed to cleanup scheduler datastore, last error(%d)", err);
					(void)ma_ds_database_transaction_cancel((ma_ds_database_t *)folder->datastore);
				}
			}else 
				MA_LOG(logger, MA_LOG_SEV_ERROR, "failed to create the transcation for persisting tasks, last error (%d)", err);

			(void)MA_TASK_TREE_TRAVERSE(120, &folder->taskmap.rbh_root, destroy_node, folder);

            folder->status = MA_TASKFOLDER_STATUS_STOPPED;
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler folder stopped successfully");
        }

        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_taskfolder_get_datastore(ma_taskfolder_t *folder, ma_ds_t **datastore) {
    if(folder && datastore) {
        *datastore = folder->datastore;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_taskfolder_get_datastore_basepath(ma_taskfolder_t *folder, const char **ds_path) {
    if(folder && ds_path) {
        *ds_path = folder->ds_path;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_taskfolder_add_task(ma_taskfolder_t *folder, ma_task_t *task) {
    if(folder && task) {
        ma_error_t err = MA_OK;
        const char *task_id = NULL;

        if(MA_OK == (err = ma_task_get_id(task, &task_id))) {
            if(!find_task(&folder->taskmap, task)) {
                ma_tasknode_t *task_node = NULL;

                err = MA_ERROR_OUTOFMEMORY;
                if((task_node = (ma_tasknode_t *) calloc(1, sizeof(ma_tasknode_t)))) {
                    if(MA_OK == (err = ma_task_add_ref(task_node->task_ = task))) {
                        (void)ma_task_set_loop(task, folder->loop);
                        RB_INSERT(ma_tasktree_s, &folder->taskmap, task_node);
                        ++folder->taskmap_size; err = MA_OK;
                        MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler task folder add task id(%s) successful", task_id ? task_id : MA_EMPTY);
                    }
                } else
                    MA_LOG(logger, MA_LOG_SEV_ERROR, "scheduler task folder add task id(%s) failed, last error(%d)", task_id ? task_id : MA_EMPTY, err = MA_ERROR_OUTOFMEMORY);
            } else
                MA_LOG(logger, MA_LOG_SEV_ERROR, "scheduler task folder add task id(%s) already exist, last error(%d)", task_id ? task_id : MA_EMPTY, err = MA_ERROR_SCHEDULER_TASK_ID_CONFLICT);
        } else
            MA_LOG(logger, MA_LOG_SEV_ERROR, "scheduler task folder add task failed, last error(%d)", task_id, err = MA_ERROR_SCHEDULER_INVALID_TASK_ID);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_taskfolder_get_task_handle(ma_taskfolder_t *folder, const char *task_id, ma_task_t **task) {
    if(folder && task_id && task) {
        ma_error_t err = MA_OK;
        ma_tasknode_t *node = NULL;
        ma_task_t *tmp_task = NULL;

        *task = NULL;
        if(MA_OK == (err = ma_task_create(task_id, &tmp_task))) {
            if((node = find_task(&folder->taskmap, tmp_task))) {
                err = ma_task_add_ref(*task = node->task_);
            } else{
                err = MA_ERROR_SCHEDULER_INVALID_TASK_ID;
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler task folder get task id(%s) does not exist,return codes(%d)", task_id, err = MA_ERROR_SCHEDULER_INVALID_TASK_ID);
            }
            (void)ma_task_release(tmp_task);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_taskfolder_get_all_task_handle(ma_taskfolder_t *folder, ma_task_t ***task_set, size_t *no_of_tasks) {
    if(folder && task_set && no_of_tasks) {
        ma_error_t err = MA_OK;
        ma_task_t **task_array = NULL;

        *task_set = NULL;
        *no_of_tasks = folder->taskmap_size;
        if(0 < *no_of_tasks){
            err = MA_ERROR_OUTOFMEMORY;
            if((task_array = (ma_task_t **) calloc(*no_of_tasks, sizeof(ma_task_t*)))) {
                data_t data;
                data.task_array = task_array;
                data.curr_index = 0;

                err = MA_OK;
                (void)MA_TASK_TREE_TRAVERSE(102, &folder->taskmap.rbh_root, task_tree_node_filldata, &data);
                *task_set = task_array;
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_taskfolder_internal_destroy_task(ma_taskfolder_t *folder, ma_task_t *task) {
    if(folder && task) {
        ma_error_t err = MA_OK;
        ma_tasknode_t *node = NULL;
        const char *task_id = NULL;
        char id_buf[MA_MAX_LEN] = {0};
        ma_bool_t is_task_stopped = MA_FALSE;
        ma_uint32_t conds = 0;
        const char *task_name = NULL;

        (void)ma_task_get_name(task, &task_name);
        (void)ma_task_get_id(task, &task_id);
        task_id = task_id ? strncpy(id_buf, task_id, strlen(task_id)) : NULL;        
        if((node = find_task(&folder->taskmap, task))) {
            if((MA_OK == (err = ma_check_task_stopped(task, &is_task_stopped))) && is_task_stopped) {
                (void)ma_task_get_conditions(task, &conds);
                if(MA_CHECK_BIT(conds, MA_TASK_COND_DELETE_WHEN_DONE) &&  !MA_CHECK_BIT(conds, MA_TASK_COND_RUN_AT_SYSTEM_STARTUP) && !MA_CHECK_BIT(conds, MA_TASK_COND_RUN_ON_LOGON)) {
                    if(MA_TASKFOLDER_STATUS_STOPPING != folder->status && MA_TASKFOLDER_STATUS_STOPPED != folder->status) {
                        MA_LOG(logger, MA_CHECK_BIT(conds, MA_TASK_COND_HIDDEN) ? MA_LOG_SEV_INFO : MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "Scheduler: Task [%s] is finished", !strcmp(task_name, MA_EMPTY) ? task_id ? task_id : MA_EMPTY : task_name);
                        if(MA_OK == (err = ma_task_publish_topics(task, MA_TASK_REMOVE_PUBSUB_TOPIC)))
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) publishing remove message", task_id);
                        else
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) failed to publish remove message, last error(%d)", task_id, err);
                        if(MA_OK == (err = ma_scheduler_datastore_remove_task(task, folder->datastore, folder->ds_path)))
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) removed from scheduler DB", task_id);
                        else
                            MA_LOG(logger, MA_LOG_SEV_ERROR, "task id(%s) removing from scheduler DB failed, last error(%d)", task_id ? task_id : MA_EMPTY, err);
                        MA_LOG(logger, MA_CHECK_BIT(conds, MA_TASK_COND_HIDDEN) ? MA_LOG_SEV_INFO : MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "The task %s is freed", !strcmp(task_name, MA_EMPTY) ? task_id ? task_id : MA_EMPTY : task_name);
                    }
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) removed from scheduler task folder", task_id ? task_id : MA_EMPTY);
                    (void)ma_task_set_scheduler(task, NULL);
                    if((MA_TASKFOLDER_STATUS_STARTING == folder->status) || (MA_TASKFOLDER_STATUS_STOPPING == folder->status) || (MA_TASKFOLDER_STATUS_RESTARTING == folder->status)) {
                        ma_taskfolder_expired_task_group_add(folder, task);
                    } else {
                        RB_REMOVE(ma_tasktree_s, &folder->taskmap, node);
                        --folder->taskmap_size;
                        if(MA_OK != (err = task_tree_node_destroy(&node, NULL)))
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) removing from folder failed, last error(%d)", task_id, err);
                    }
                } else {
                    (void)ma_task_unset_event(task, MA_TASK_EVENT_TYPE_SYSTEM_TIME_CHANGE);
                    //MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) deletion from scheduler task folder not possible!!!", task_id);
                }
            } else
                MA_LOG(logger, MA_LOG_SEV_TRACE, "task id(%s) still running cannot be stopped!", task_id);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_taskfolder_destroy_task(ma_taskfolder_t *folder, ma_task_t *task) {
    if(folder && task) {
        ma_error_t err = MA_OK;
        const char *task_id = NULL;
        char id_buf[MA_MAX_LEN] = {0};
        
        (void)ma_task_get_id(task, &task_id);
        task_id = task_id ? strncpy(id_buf, task_id, strlen(task_id)) : NULL;
        if(find_task(&folder->taskmap, task)) {
            ma_trigger_list_t *tl = NULL;
            size_t itr = 0;
            size_t size = 0;
            ma_trigger_t *trigger = NULL;
            const char *trigger_id = NULL;
            
            //(void)ma_task_set_condition(task, MA_TASK_COND_DISABLED);
            (void)ma_task_set_condition(task, MA_TASK_COND_DELETE_WHEN_DONE);
            (void)ma_task_get_trigger_list(task, &tl);
            (void)ma_trigger_list_size(tl, &size);
            if(size) {
                for(itr = 0; itr < size; ++itr) {
                    if(MA_OK == (err = ma_trigger_list_at(tl, itr, &trigger))) {
                        (void)ma_trigger_release(trigger); /* intentional */
                        (void)ma_trigger_get_id(trigger, &trigger_id);
                        MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) trigger id(%s) closing", task_id, trigger_id);
                        if(MA_OK != (err = ma_trigger_core_close((ma_trigger_core_t*)trigger->data)))
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) trigger close failed, last error(%d)", task_id, err);
                    }
                } /* end of for loop */
            } else {
                /* empty task */
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) trigger list empty releasing...", task_id);
                err = ma_taskfolder_internal_destroy_task(folder, task);
            }
        } else
            MA_LOG(logger, MA_LOG_SEV_ERROR, "task id(%s) does not exist, last error(%d)", task_id, err = MA_ERROR_SCHEDULER_INVALID_TASK_ID);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_taskfolder_modify_task(ma_taskfolder_t *folder, ma_task_t *new_task) {
    if(folder && new_task) {
        const char *new_id = NULL;
        const char *task_name = NULL;
        const char *task_type = NULL;
        ma_error_t err = MA_OK;
        ma_uint32_t cond = 0;
        ma_bool_t publish_stop_topic = MA_FALSE;

        
        if(MA_OK == (err = ma_task_get_id(new_task, &new_id)) &&
            MA_OK == (err = ma_task_get_name(new_task, &task_name)) &&
            MA_OK == (err = ma_task_get_type(new_task, &task_type))) {
            ma_task_t *old_task = NULL;
            ma_tasknode_t *task_node = NULL;

            if(MA_OK == (err = ma_taskfolder_get_task_handle(folder, new_id, &old_task))) {
                ma_bool_t task_attr_modified = MA_FALSE;

                (void)ma_task_attributes_modified(old_task, new_task, &task_attr_modified);
                if(task_attr_modified) {
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) copying task attributes", new_id);
                    if(MA_OK != (err = ma_task_attributes_copy_only(new_task, old_task)))
                        MA_LOG(logger, MA_LOG_SEV_ERROR, "task id(%s) failed to clone task attributes", new_id);
                    if(MA_OK == (err = ma_task_start(old_task))) {
                        (void)ma_task_get_name(old_task, &task_name);
                        MA_LOG(logger, MA_CHECK_BIT(cond, MA_TASK_COND_HIDDEN) ? MA_LOG_SEV_INFO : MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "The task %s is modified", !strcmp(task_name, MA_EMPTY) ? new_id : task_name);
                    }
                    else
                        MA_LOG(logger, MA_LOG_SEV_ERROR, "modifying task failed, last error(%d)", err);

                } else {
                    if(old_task) {
                        ma_trigger_list_t *tl = NULL;
                                        
                        (void)ma_task_release(old_task); /* intentional */
                        if(MA_OK == (err = ma_task_get_trigger_list(old_task, &tl))) {
                            size_t itr = 0, size = 0;
                            if(MA_OK == (err = ma_trigger_list_size(tl, &size))) {
                                for(itr = 0; itr < size; ++itr) {
                                    ma_trigger_t *trigger = NULL;

                                    if((MA_OK == (err = ma_trigger_list_at(tl, itr, &trigger))) && trigger) {
                                        ma_bool_t start_lock = MA_FALSE;

                                        (void)ma_trigger_release(trigger);
                                        if((MA_OK == (err = core_get_start_lock_status((ma_trigger_core_t*)trigger->data, &start_lock))) && start_lock) {
                                            ma_trigger_state_t state = MA_TRIGGER_STATE_NOT_STARTED;
                                            ma_task_time_t begin_date = {0};

                                            if(MA_OK == (err = ma_trigger_get_state(trigger, &state))) {
                                                if(MA_TRIGGER_STATE_EXPIRED != state) {
                                                    if(MA_TRIGGER_STATE_RUNNING == state) {
                                                       // publish_stop_topic = MA_TRUE; /* commenting check before we publish stop topic, if task is already running while modification */
                                                    }
                                                    if(MA_OK != (err = ma_trigger_core_force_stop((ma_trigger_core_t*)trigger->data))) {
                                                        const char *trigger_id = NULL;

                                                        (void)ma_trigger_get_id(trigger, &trigger_id);
                                                        MA_LOG(logger, MA_LOG_SEV_DEBUG, "trigger id(%s) of task id(%s) failed to stop, last error(%d)", new_id, trigger_id, err);
                                                    }
                                                }
                                                (void)ma_trigger_set_state(trigger, MA_TRIGGER_STATE_NOT_STARTED);
                                            }
                                            (void)core_reset_start_lock((ma_trigger_core_t*)trigger->data);
                                            (void)ma_trigger_get_begin_date(trigger, &begin_date);
                                            (void)ma_trigger_set_next_fire_date(trigger, begin_date);
                                        }
                                    }
                                } /* end of for loop */
                            }
                        }

                        if(publish_stop_topic) 
                            ma_task_publish_topics(old_task, MA_TASK_STOP_PUBSUB_TOPIC);

                        if(old_task == new_task) {
                            if(MA_OK != (err = ma_scheduler_datastore_remove_task(new_task = old_task, folder->datastore, folder->ds_path)))
                                MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) removed from DB failed. last error(%d)", new_id, err);
                        } else {
                            if((task_node = find_task(&folder->taskmap, old_task))) {
                                const char *old_task_id = NULL;

                                (void)ma_task_get_id(old_task, &old_task_id);
                                if((MA_OK == (err = ma_task_set_scheduler(old_task, NULL))) &&
                                   (MA_OK == (err = ma_task_set_condition(old_task, MA_TASK_COND_DELETE_WHEN_DONE)))) {
                                       /* proceed to remove old task from scheduler db */
                                    if(MA_OK != (err = ma_scheduler_datastore_remove_task(old_task, folder->datastore, folder->ds_path)))
                                        MA_LOG(logger, MA_LOG_SEV_DEBUG, "old task id(%s) removed from DB failed, last error(%d)", old_task_id, err);
                                    task_node->task_ = new_task;
                                    (void)ma_task_set_execution_status_internal(new_task, MA_TASK_EXEC_NOT_STARTED); /* reset task exec status not started */
                                    if(MA_OK == (err = ma_task_get_trigger_list(task_node->task_, &tl))) {
                                        size_t itr = 0, size = 0;

                                        if(MA_OK == (err = ma_trigger_list_size(tl, &size))) {
                                            for(itr = 0; itr < size; ++itr) {
                                                ma_trigger_t *trigger = NULL;

                                                if((MA_OK == (err = ma_trigger_list_at(tl, itr, &trigger))) && trigger) {
                                                    ma_task_time_t begin_date = {0};

                                                    (void)ma_trigger_release(trigger); /* intentional */
                                                    if(MA_OK == ma_trigger_get_begin_date(trigger, &begin_date)) {
                                                        (void)ma_trigger_set_next_fire_date(trigger, begin_date);
                                                    }
                                                    (void)ma_trigger_set_state(trigger, MA_TRIGGER_STATE_NOT_STARTED);
                                                    (void)core_reset_start_lock((ma_trigger_core_t*)trigger->data);
                                                }
                                            } /* end of for loop */
                                        }
                                    }
                                }
                            }
                        }
                        if(MA_OK == (err = ma_task_set_execution_status_internal(new_task, MA_TASK_EXEC_NOT_STARTED))) {
                            if(MA_OK == (err = ma_task_set_loop(new_task, folder->loop))) {
                                if(MA_OK == (err = ma_task_start(new_task))) {
                                    ma_task_time_t dummy = {0};
                                    const char *task_name = NULL;

                                    memset(&dummy, 0, sizeof(ma_task_time_t));
                                    (void)ma_task_get_conditions(new_task, &cond);
                                    (void)ma_task_get_name(new_task, &task_name);
                                    /* reset last run time */
                                    (void)ma_task_set_last_run_time(new_task, dummy);
                                    MA_LOG(logger, MA_CHECK_BIT(cond, MA_TASK_COND_HIDDEN) ? MA_LOG_SEV_INFO : MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "The task %s is modified", !strcmp(task_name, MA_EMPTY) ? new_id : task_name);

                                } else
                                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "modified task failed to start, last error(%d)",  err);
                            }
                        }
                    } else {
                        if(MA_OK == (err = ma_taskfolder_add_task(folder, new_task))) {
                            if(MA_OK == (err = ma_task_unset_condition(new_task, MA_TASK_COND_DISABLED))) {
                                if(MA_OK == (err = ma_task_set_execution_status(new_task, MA_TASK_EXEC_NOT_STARTED))) {
                                    if(MA_OK == (err = ma_task_start(new_task))) {
                                        ma_task_time_t dummy = {0};
                                        const char *task_name = NULL;

                                        (void)ma_task_get_name(new_task, &task_name);
                                        (void)ma_task_get_conditions(new_task, &cond);
                                        /* reset last run time */
                                        (void)ma_task_set_last_run_time(new_task, dummy);
                                        MA_LOG(logger, MA_CHECK_BIT(cond, MA_TASK_COND_HIDDEN) ? MA_LOG_SEV_INFO : MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "The task %s is modified", !strcmp(task_name, MA_EMPTY) ? new_id : task_name);
                                        if(MA_OK != (err = ma_scheduler_datastore_write(folder->scheduler, new_task, folder->datastore, folder->ds_path))) {
                                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "modified task id(%s) DB write failed, last error(%d) ", new_id, err);
                                        }
                                    } else
                                        MA_LOG(logger, MA_LOG_SEV_DEBUG, "modified task failed to start, last error(%d)", err);
                                }
                            }
                        }
                    }
                }
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_taskfolder_run_task_on_demand(ma_taskfolder_t *folder, ma_task_t *task) {
    if(folder && task) {
        ma_error_t err = MA_OK;
        const char *task_id = NULL;

        if((MA_OK == (err = ma_task_get_id(task, &task_id))) && task_id) {
            ma_trigger_list_t *tl = NULL;
            if(MA_OK == (err = ma_task_get_trigger_list(task, &tl))) {
                size_t itr = 0, size = 0;
                if(MA_OK == (err = ma_trigger_list_size(tl, &size))) {
                    for(itr = 0; itr < size; ++itr) {
                        ma_trigger_t *trigger = NULL;

                        if((MA_OK == (err = ma_trigger_list_at(tl, itr, &trigger))) && trigger) {
                            ma_bool_t start_lock = MA_FALSE;

                            if((MA_OK == (err = core_get_start_lock_status((ma_trigger_core_t*)trigger->data, &start_lock))) && start_lock) {
                                ma_trigger_state_t state = MA_TRIGGER_STATE_NOT_STARTED;

                                if(MA_OK == (err = ma_trigger_get_state(trigger, &state))) {
                                    if((MA_TRIGGER_STATE_EXPIRED != state) && (MA_TRIGGER_STATE_RUNNING != state)) {
                                        const char *trigger_id = NULL;

                                        (void)ma_trigger_get_id(trigger, &trigger_id);
                                        if(MA_OK == (err = ma_trigger_core_reset((ma_trigger_core_t*)trigger->data, 1000))) { /* immediate */
                                            (void)ma_trigger_set_state(trigger, MA_TRIGGER_STATE_NOT_STARTED);
                                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) trigger id(%s) successfully reset, last error(%d)", task_id, trigger_id, err);
                                        } else
                                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) trigger id(%s) failed to reset, last error(%d)", task_id, trigger_id, err);
                                    }
                                }
                            }
                        }
                    } /* end of for loop */
                }
            }
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "task running on demand status. last error(%d)", err);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t enumerate_task_no_match(ma_taskfolder_t *folder, ma_task_t ***task_set, size_t *no_of_tasks) {
    if(folder && task_set && no_of_tasks) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;
        ma_tasknode_t *entry = NULL;
        size_t task_map_size = folder->taskmap_size;

        *no_of_tasks = 0;
        if((*task_set = (ma_task_t**)calloc(task_map_size, sizeof(ma_task_t*)))) {
            err = MA_OK;
            RB_FOREACH(entry, ma_tasktree_s, &folder->taskmap) {
                ma_uint32_t conds = 0;

                (void)ma_task_get_conditions(entry->task_, &conds);
                if(!MA_BYTE_CHECK_BIT(conds, MA_TASK_COND_HIDDEN)) {
                    const char *task_id = NULL;

                    (void)ma_task_get_id(entry->task_, &task_id);
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler enumerating task id(%s) count(%d)", task_id, *no_of_tasks);
                    (*task_set)[*no_of_tasks] = entry->task_;
                    ++(*no_of_tasks);
                }
                --task_map_size;
                if(!task_map_size)break;
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t enumerate_task_match_type(ma_taskfolder_t *folder, const char *task_type, ma_task_t ***task_set, size_t *no_of_tasks) {
    if(folder && task_type && task_set && no_of_tasks) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;
        ma_tasknode_t *entry = NULL;
        size_t task_map_size = folder->taskmap_size;

        *no_of_tasks = 0;
        if((*task_set = (ma_task_t**)calloc(task_map_size, sizeof(ma_task_t*)))) {
            err = MA_OK;
            RB_FOREACH(entry, ma_tasktree_s, &folder->taskmap) {
                const char *type = NULL;

                if(MA_OK == (err = ma_task_get_type(entry->task_, &type))) {
                    if(!strcmp(task_type, type)) {
                        ma_uint32_t conds = 0;

                        (void)ma_task_get_conditions(entry->task_, &conds);
                        if(!MA_BYTE_CHECK_BIT(conds, MA_TASK_COND_HIDDEN)) {
                            (*task_set)[*no_of_tasks] = entry->task_;
                            ++(*no_of_tasks);
                        }
                    }
                }
                --task_map_size;
                if(!task_map_size)break;
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t enumerator_task_match_creator(ma_taskfolder_t *folder, const char *creator_id, ma_task_t ***task_set, size_t *no_of_tasks) {
    if(folder && creator_id && task_set && no_of_tasks) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;
        ma_tasknode_t *entry = NULL;
        size_t task_map_size = folder->taskmap_size;

        *no_of_tasks = 0;
        if((*task_set = (ma_task_t**)calloc(task_map_size, sizeof(ma_task_t*)))) {
            err = MA_OK;
            RB_FOREACH(entry, ma_tasktree_s, &folder->taskmap) {
                const char *creator = NULL;

                if(MA_OK == (err = ma_task_get_creator_id(entry->task_, &creator))) {
                    if(!strcmp(creator_id, creator)) {
                        ma_uint32_t conds = 0;

                        (void)ma_task_get_conditions(entry->task_, &conds);
                        if(!MA_BYTE_CHECK_BIT(conds, MA_TASK_COND_HIDDEN)) {
                            (*task_set)[*no_of_tasks] = entry->task_;
                            ++(*no_of_tasks);
                        }
                    }
                }
                --task_map_size;
                if(!task_map_size)break;
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t enumerate_task_match_creator_type(ma_taskfolder_t *folder, const char *creator_id, const char *task_type, ma_task_t ***task_set, size_t *no_of_tasks) {
    if(folder && creator_id && task_type && task_set && no_of_tasks) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;
        ma_tasknode_t *entry = NULL;
        size_t task_map_size = folder->taskmap_size;

        *no_of_tasks = 0;
        if((*task_set = (ma_task_t**)calloc(task_map_size, sizeof(ma_task_t*)))) {
            err = MA_OK;
            RB_FOREACH(entry, ma_tasktree_s, &folder->taskmap) {
                const char *creator = NULL;
                const char *type = NULL;

                if((MA_OK == (err = ma_task_get_creator_id(entry->task_, &creator))) && 
                   (MA_OK == (err = ma_task_get_type(entry->task_, &type)))) {
                    if(type && creator) {
                        if(!strcmp(creator_id, creator) && !strcmp(task_type, type)) {
                            ma_uint32_t conds = 0;

                            (void)ma_task_get_conditions(entry->task_, &conds);
                            if(!MA_BYTE_CHECK_BIT(conds, MA_TASK_COND_HIDDEN)) {
                                (*task_set)[*no_of_tasks] = entry->task_;
                                ++(*no_of_tasks);
                            }
                        }
                    }
                }
                --task_map_size;
                if(!task_map_size)break;
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t enumerate_task_match_product(ma_taskfolder_t *folder, const char *product_id, ma_task_t ***task_set, size_t *no_of_tasks) {
    if(folder && product_id && task_set && no_of_tasks) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;
        ma_tasknode_t *entry = NULL;
        size_t task_map_size = folder->taskmap_size;

        *no_of_tasks = 0;
        if((*task_set = (ma_task_t**)calloc(task_map_size, sizeof(ma_task_t*)))) {
            err = MA_OK;
            RB_FOREACH(entry, ma_tasktree_s, &folder->taskmap) {
                const char *product = NULL;

                if(MA_OK == (err = ma_task_get_software_id(entry->task_, &product))) {
                    if(!strcmp(product_id, product)) {
                        ma_uint32_t conds = 0;

                        (void)ma_task_get_conditions(entry->task_, &conds);
                        if(!MA_BYTE_CHECK_BIT(conds, MA_TASK_COND_HIDDEN)) {
                            (*task_set)[*no_of_tasks] = entry->task_;
                            ++(*no_of_tasks);
                        }
                    }
                }
                --task_map_size;
                if(!task_map_size)break;
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t enumerate_task_match_product_type(ma_taskfolder_t *folder, const char *product_id, const char *task_type, ma_task_t ***task_set, size_t *no_of_tasks) {
    if(folder && product_id && task_type && task_set && no_of_tasks) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;
        ma_tasknode_t *entry = NULL;
        size_t task_map_size = folder->taskmap_size;

        *no_of_tasks = 0;
        if((*task_set = (ma_task_t**)calloc(task_map_size, sizeof(ma_task_t*)))) {
            err = MA_OK;
            RB_FOREACH(entry, ma_tasktree_s, &folder->taskmap) {
                const char *product = NULL;
                const char *type = NULL;

                if((MA_OK == (err = ma_task_get_software_id(entry->task_, &product))) && 
                   (MA_OK == (err = ma_task_get_type(entry->task_, &type)))) {
                    if(type && product) {
                        if(!strcmp(product_id, product) && !strcmp(task_type, type)) {
                            ma_uint32_t conds = 0;

                            (void)ma_task_get_conditions(entry->task_, &conds);
                            if(!MA_BYTE_CHECK_BIT(conds, MA_TASK_COND_HIDDEN)) {
                                (*task_set)[*no_of_tasks] = entry->task_;
                                ++(*no_of_tasks);
                            }
                        }
                    }
                }
                --task_map_size;
                if(!task_map_size)break;
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t enumerate_task_match_product_creator(ma_taskfolder_t *folder, const char *product_id, const char *creator_id, ma_task_t ***task_set, size_t *no_of_tasks) {
    if(folder && product_id && creator_id && task_set && no_of_tasks) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;
        ma_tasknode_t *entry = NULL;
        size_t task_map_size = folder->taskmap_size;

        *no_of_tasks = 0;
        if((*task_set = (ma_task_t**)calloc(task_map_size, sizeof(ma_task_t*)))) {
            err = MA_OK;
            RB_FOREACH(entry, ma_tasktree_s, &folder->taskmap) {
                const char *product = NULL;
                const char *creator = NULL;

                if((MA_OK == (err = ma_task_get_software_id(entry->task_, &product))) && 
                   (MA_OK == (err = ma_task_get_creator_id(entry->task_, &creator)))) {
                    if(creator && product) {
                        if(!strcmp(product_id, product) && !strcmp(creator_id, creator)) {
                            ma_uint32_t conds = 0;

                            (void)ma_task_get_conditions(entry->task_, &conds);
                            if(!MA_BYTE_CHECK_BIT(conds, MA_TASK_COND_HIDDEN)) {
                                (*task_set)[*no_of_tasks] = entry->task_;
                                ++(*no_of_tasks);
                            }
                        }
                    }
                }
                --task_map_size;
                if(!task_map_size)break;
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t enumerate_task_match_product_creator_type(ma_taskfolder_t *folder, const char *product_id, const char *creator_id, const char *task_type, ma_task_t ***task_set, size_t *no_of_tasks) {
    if(folder && product_id && creator_id && task_type && task_set && no_of_tasks) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;
        ma_tasknode_t *entry = NULL;
        size_t task_map_size = folder->taskmap_size;

        *no_of_tasks = 0;
        if((*task_set = (ma_task_t**)calloc(task_map_size, sizeof(ma_task_t*)))) {
            err = MA_OK;
            RB_FOREACH(entry, ma_tasktree_s, &folder->taskmap) {
                const char *product = NULL;
                const char *creator = NULL;
                const char *type = NULL;

                if((MA_OK == (err = ma_task_get_software_id(entry->task_, &product))) && 
                   (MA_OK == (err = ma_task_get_creator_id(entry->task_, &creator))) &&
                   (MA_OK == (err = ma_task_get_type(entry->task_, &type))) ) {
                    if(creator && product && type) {
                        if(!strcmp(product_id, product) && !strcmp(creator_id, creator) && !strcmp(task_type, type)) {
                            ma_uint32_t conds = 0;

                            (void)ma_task_get_conditions(entry->task_, &conds);
                            if(!MA_BYTE_CHECK_BIT(conds, MA_TASK_COND_HIDDEN)) {
                                (*task_set)[*no_of_tasks] = entry->task_;
                                ++(*no_of_tasks);
                            }
                        }
                    }
                }
                --task_map_size;
                if(!task_map_size)break;
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_taskfolder_enumerate_task(ma_taskfolder_t *folder, const char *product_id, const char *creator_id, const char *task_type, ma_task_t ***task_set, size_t *no_of_tasks) {
    if(folder && task_set && no_of_tasks) {
        ma_error_t err = MA_OK;

        if(!strcmp(product_id, MA_EMPTY) && !strcmp(creator_id, MA_EMPTY) && !strcmp(task_type, MA_EMPTY)) { /* 000 */
            err = enumerate_task_no_match(folder, task_set, no_of_tasks);
        } else if(!strcmp(product_id, MA_EMPTY) && !strcmp(creator_id, MA_EMPTY) && strcmp(task_type, MA_EMPTY)) { /* 001 */
            err = enumerate_task_match_type(folder, task_type, task_set, no_of_tasks);
        } else if(!strcmp(product_id, MA_EMPTY) && strcmp(creator_id, MA_EMPTY) && !strcmp(task_type, MA_EMPTY)) { /* 010 */
            err = enumerator_task_match_creator(folder, creator_id, task_set, no_of_tasks);
        } else if(!strcmp(product_id, MA_EMPTY) && strcmp(creator_id, MA_EMPTY) && strcmp(task_type, MA_EMPTY)) { /* 011 */
            err = enumerate_task_match_creator_type(folder, creator_id, task_type, task_set, no_of_tasks);
        }  else if(strcmp(product_id, MA_EMPTY) && !strcmp(creator_id, MA_EMPTY) && !strcmp(task_type, MA_EMPTY)) { /* 100 */
            err = enumerate_task_match_product(folder, product_id, task_set, no_of_tasks);
        } else if(strcmp(product_id, MA_EMPTY) && !strcmp(creator_id, MA_EMPTY) && strcmp(task_type, MA_EMPTY)) { /* 101 */
            err = enumerate_task_match_product_type(folder, product_id, task_type, task_set, no_of_tasks);
        } else if(strcmp(product_id, MA_EMPTY) && strcmp(creator_id, MA_EMPTY) && !strcmp(task_type, MA_EMPTY)) { /* 110 */
            err = enumerate_task_match_product_creator(folder, product_id, creator_id, task_set, no_of_tasks);
        } else if(strcmp(product_id, MA_EMPTY) && strcmp(creator_id, MA_EMPTY) && strcmp(task_type, MA_EMPTY)) { /* 111 */
            err = enumerate_task_match_product_creator_type(folder, product_id, creator_id, task_type, task_set, no_of_tasks);
        } else {
            //do nothing
            err = MA_ERROR_UNEXPECTED;
        }
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

/* system startup task handler */
ma_error_t schedule_system_startup_task(ma_task_t *task, ma_trigger_t *trigger) {
    if(task && trigger) {
        ma_error_t err = MA_OK;
        const char *task_id = NULL;
        ma_trigger_systemstart_policy_t policy = {0};
        ma_bool_t can_run = MA_TRUE;
        const char *trigger_id = NULL;
        ma_trigger_state_t state = MA_TRIGGER_STATE_NOT_STARTED;

        (void)ma_trigger_get_id(trigger, &trigger_id);
        (void)ma_trigger_get_policy(trigger, &policy);
        (void)ma_task_get_id(task, &task_id);
        MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) trigger id(%s) system event policy daily_once(%d) run_once(%d)", task_id, trigger_id, policy.daily_once, policy.run_just_once);
        if(policy.daily_once) {
            ma_task_time_t last_run_time = {0};
            ma_task_time_t dummy = {0};
            ma_task_time_t now = {0};
            ma_task_time_t *dp = NULL;

            get_localtime(&now);
            memset(&dummy, 0, sizeof(ma_task_time_t));
            if(MA_OK == (err = ma_task_get_last_run_time(task, dp = &last_run_time))) {
                if(memcmp(&dummy, &last_run_time, sizeof(ma_task_time_t))) 
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) last run time(%hu/%hu/%hu %hu:%hu:%hu)", task_id, dp->year, dp->month, dp->day, dp->hour, dp->minute, dp->secs);
            }
            can_run = (last_run_time.day == now.day) ? MA_FALSE : MA_TRUE;
            MA_LOG(logger, MA_LOG_SEV_ERROR, "task id(%s) system event trigger daily once(%d)", task_id, policy.daily_once);
        }
        if(can_run) {
            ma_task_delay_policy_t policy = {{0}};
            ma_int64_t delay_def = 1; /* default 5 secs */
            ma_bool_t start_lock = MA_FALSE;
            ma_int64_t delay = 0;

            if(MA_OK == (err = ma_task_get_delay_policy(task, &policy))) {
                if(policy.delay_time.hour || policy.delay_time.minute) {
                    delay = (policy.delay_time.hour * 60 * 60) + (policy.delay_time.minute * 60);
                }
            }
            if(!delay)
                delay = delay_def;

            if(MA_OK == (err = core_get_start_lock_status((ma_trigger_core_t*)trigger->data, &start_lock))) {
                if(!start_lock) {
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) trigger id(%s) starting", task_id, trigger_id);
                    if(MA_OK != (err = ma_trigger_start(trigger)))
                        MA_LOG(logger, MA_LOG_SEV_ERROR, "task id(%s) trigger id(%s) start failed, last error(%d)", task_id, trigger_id, err);
                }
                (void)ma_trigger_get_state(trigger, &state);
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) trigger id(%s) state(%d)", task_id, trigger_id, state);
                if((MA_TRIGGER_STATE_EXPIRED != state) && (MA_TRIGGER_STATE_RUNNING != state)) {
                    if(MA_OK == (err = ma_trigger_core_reset((ma_trigger_core_t*)trigger->data, delay * 1000))) {
                        (void)ma_trigger_set_state(trigger, MA_TRIGGER_STATE_NOT_STARTED);
                        MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) trigger id(%s) successfully reset", task_id, trigger_id);
                    } else
                        MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) trigger id(%s) failed to reset, last error(%d)", task_id, trigger_id, err);
                }
            }

        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

/* logon task handler */
ma_error_t schedule_logon_task(ma_task_t *task, ma_trigger_t *trigger) {
    if(task && trigger) {
        ma_error_t err = MA_OK;
        const char *task_id = NULL;
        ma_trigger_logon_policy_t policy = {0};
        ma_bool_t can_run = MA_TRUE;

        (void)ma_trigger_get_policy(trigger, &policy);
        (void)ma_task_get_id(task, &task_id);
        if(policy.daily_once) {
            ma_task_time_t last_run_time = {0};
            ma_task_time_t dummy = {0};
            ma_task_time_t now = {0};
            ma_task_time_t *dp = NULL;

            get_localtime(&now);
            memset(&dummy, 0, sizeof(ma_task_time_t));
            if(MA_OK == (err = ma_task_get_last_run_time(task, dp = &last_run_time))) {
                if(memcmp(&dummy, &last_run_time, sizeof(ma_task_time_t))) 
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) last run time(%hu/%hu/%hu %hu:%hu:%hu)", task_id, dp->year, dp->month, dp->day, dp->hour, dp->minute, dp->secs);
            }
            if(last_run_time.day == now.day)
                can_run = MA_FALSE;
        }
        if(can_run) {
            ma_task_delay_policy_t policy = {{0}};
            ma_int64_t delay_def = 1; /* default one second */
            ma_bool_t start_lock = MA_FALSE;
            ma_int64_t delay = 0;

            if(MA_OK == (err = ma_task_get_delay_policy(task, &policy))) {
                if(policy.delay_time.hour || policy.delay_time.minute) {
                    delay = (policy.delay_time.hour * 60 * 60) + (policy.delay_time.minute * 60);
                }
            }
            if(!delay) delay = delay_def;
            if((MA_OK == (err = core_get_start_lock_status((ma_trigger_core_t*)trigger->data, &start_lock))) && start_lock) {
                ma_trigger_state_t state = MA_TRIGGER_STATE_NOT_STARTED;

                if(MA_OK == (err = ma_trigger_get_state(trigger, &state))) {
                    if((MA_TRIGGER_STATE_EXPIRED != state) && (MA_TRIGGER_STATE_RUNNING != state)) {
                        const char *trigger_id = NULL;

                        (void)ma_trigger_get_id(trigger, &trigger_id);
                        (void)ma_trigger_set_state(trigger, MA_TRIGGER_STATE_NOT_STARTED);
                        if(MA_OK == (err = ma_trigger_core_reset((ma_trigger_core_t*)trigger->data, delay * 1000)))
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) trigger id(%s) successfully reset", task_id, trigger_id);
                        else
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) trigger id(%s) failed to reset, last error(%d)", task_id, trigger_id, err);
                    }
                }
            }

        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t schedule_task_on_conditions(ma_task_t *task) {
    if(task) {
        ma_error_t err = MA_OK;
        ma_task_exec_status_t status = MA_TASK_EXEC_RUNNING;
        const char *task_id = NULL;
        
        (void)ma_task_get_id(task, &task_id);
        if(MA_OK == (err = ma_task_get_execution_status(task, &status))) {
            if(MA_TASK_EXEC_NOT_STARTED == status) {
                ma_trigger_list_t *tl = NULL;
                ma_trigger_t *trigger = NULL;

                (void)ma_task_get_trigger_list(task, &tl);
                (void)ma_enumerator_reset((ma_enumerator_t*)tl);
                /* pick any trigger and run */
                while(MA_OK == ma_enumerator_next((ma_enumerator_t*)tl, &trigger)) {
                    ma_bool_t start_lock = MA_FALSE;

                    if((MA_OK == (err = core_get_start_lock_status((ma_trigger_core_t*)trigger->data, &start_lock))) && start_lock) {
                        ma_trigger_state_t state = MA_TRIGGER_STATE_NOT_STARTED;

                        if(MA_OK == (err = ma_trigger_get_state(trigger, &state))) {
                            if((MA_TRIGGER_STATE_EXPIRED != state) && (MA_TRIGGER_STATE_RUNNING != state) && (MA_TRIGGER_STATE_BLOCKED != state)) {
                                const char *trigger_id = NULL;
                                ma_int64_t delay = 0;
                                ma_task_delay_policy_t policy = {{0}};
                                ma_int64_t delay_def = 1; /* default one second */
                                
                                (void)ma_trigger_get_id(trigger, &trigger_id);
                                if(MA_OK == (err = ma_task_get_delay_policy(task, &policy))) {
                                    if(policy.delay_time.hour || policy.delay_time.minute) {
                                        delay = (policy.delay_time.hour * 60 * 60) + (policy.delay_time.minute * 60);
                                    }
                                }
                                if(!delay)
                                    delay = delay_def;
                                if(MA_OK == (err = ma_trigger_core_reset((ma_trigger_core_t*)trigger->data, delay * 1000))) {
                                    (void)ma_trigger_set_state(trigger, MA_TRIGGER_STATE_NOT_STARTED);
                                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) trigger id(%s) successfully reset", task_id, trigger_id);
                                    break;
                                } else {
                                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) trigger id(%s) failed to reset, last error(%d)", task_id, trigger_id, err);
                                }
                            }
                        }
                    }
                    
                }
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t notfiy_event_foreach_task(ma_tasknode_t **node, void *data) {
    if(node && *node && data) {
        ma_task_event_type_t *type = (ma_task_event_type_t*)data;
        ma_uint32_t events = 0;
        ma_error_t err = MA_OK;
        const char *task_id = NULL;
        ma_bool_t scheduled = MA_FALSE;
        ma_uint32_t conds = 0;
        ma_trigger_list_t *tl = NULL;
        ma_trigger_t *trigger = NULL;

        (void)ma_task_get_id((*node)->task_, &task_id);
        (void)ma_task_get_trigger_list((*node)->task_, &tl);
        if(MA_OK == (err = ma_task_get_events((*node)->task_, &events))) {
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler notifying event(%d)", *type);
            if(MA_BYTE_CHECK_BIT(events, *type)) {
                ma_enumerator_reset((ma_enumerator_t*)tl);
                while(MA_OK == ma_enumerator_next((ma_enumerator_t*)tl, &trigger)) {
                    ma_trigger_type_t trigger_type = MA_TRIGGER_NOW;

                    (void)ma_trigger_get_type(trigger, &trigger_type);
                    if(MA_TRIGGER_AT_SYSTEM_START == trigger_type) {
                        if(MA_TASK_EVENT_TYPE_SYSTEM_STARTUP == *type) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) received system startup event notification", task_id);
                            if(MA_OK == (err = schedule_system_startup_task((*node)->task_, trigger))) {
                                scheduled = MA_FALSE;
                            }
                        }
                    } else if(MA_TRIGGER_AT_LOGON == trigger_type) {
                        if(MA_TASK_EVENT_TYPE_LOGON == *type) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) received logon event notification", task_id);
                            if(MA_OK == (err = schedule_logon_task((*node)->task_, trigger))) {
                                scheduled = MA_FALSE;
                            }
                        }
                    } else {
                        /* do nothing */
                    }
                }/* end of while */
            }
        }
        
        if(MA_TASK_EVENT_TYPE_SYSTEM_TIME_CHANGE == *type) {
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) received system time change event notification", task_id);
            (void)ma_task_set_event((*node)->task_, MA_TASK_EVENT_TYPE_SYSTEM_TIME_CHANGE);
        }
        (void)ma_task_get_conditions((*node)->task_, &conds);
        if(conds) {
            if(!scheduled) {
                if(MA_BYTE_CHECK_BIT(conds, MA_TASK_COND_RUN_ON_LOGON)) {
                    /* run task on any user logon */
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) re-scheduled on logon condition", task_id);
                    if(MA_OK != (err = schedule_task_on_conditions((*node)->task_)))
                        MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) re-scheduled on logon condition failed, last error(%d)", task_id, err);
                } else if(MA_BYTE_CHECK_BIT(conds, MA_TASK_COND_RUN_AT_SYSTEM_STARTUP)) {
                    /* run task at system startup */
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) re-scheduled at system startup condition", task_id);
                    if(MA_OK != (err = schedule_task_on_conditions((*node)->task_)))
                        MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) re-scheduled at logsystem startupon condition failed, last error(%d)", task_id, err);
                } else {
                }
            }
            if(MA_BYTE_CHECK_BIT(conds, MA_TASK_COND_DONT_START_IF_ON_BATTERIES)) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) power status = battery disabling starter ", task_id ? task_id : MA_EMPTY);
                err = ma_task_set_condition((*node)->task_, MA_TASK_COND_DISABLED);
            }
            if(MA_BYTE_CHECK_BIT(conds, MA_TASK_COND_STOP_IF_GOING_ON_BATTERIES)) {
                ma_trigger_state_t state = MA_TRIGGER_STATE_NOT_STARTED;
                ma_bool_t task_running = MA_FALSE;

                ma_enumerator_reset((ma_enumerator_t*)tl);
                while(MA_OK == ma_enumerator_next((ma_enumerator_t*)tl, &trigger)) {
                    (void)ma_trigger_get_state(trigger, &state);
                    if(MA_TRIGGER_STATE_RUNNING == state) {
                        task_running = MA_TRUE;
                    }
                }
                if(task_running) {
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler initiated task id(%s) force stop power status = battery", task_id ? task_id : MA_EMPTY);
                    (void)ma_task_set_execution_status((*node)->task_, MA_TASK_EXEC_STOPPED);
                }
            }
            if(MA_BYTE_CHECK_BIT(conds, MA_TASK_COND_ADJUST_BEGIN_DATE_ON_SYSTEM_TIME_CHANGE)) {
                ma_task_time_t now = {0};

                get_localtime(&now);
                ma_enumerator_reset((ma_enumerator_t*)tl);
                while(MA_OK == ma_enumerator_next((ma_enumerator_t*)tl, &trigger)) {
                    ma_task_time_t begin_date = {0};

                    (void)ma_trigger_get_begin_date(trigger, &begin_date);
                    if(memcmp(&begin_date, &now, sizeof(ma_task_time_t)) > 0)
                        (void)ma_trigger_set_begin_date(trigger, &now);
                }
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_taskfolder_notify_event(ma_taskfolder_t *folder, int event) {
    return folder?MA_TASK_TREE_TRAVERSE(102, &folder->taskmap.rbh_root, notfiy_event_foreach_task, &event):MA_ERROR_INVALIDARG;
}


ma_error_t task_tree_node_restart(ma_tasknode_t **node, void *data) {
    if(node && *node) {
        ma_error_t err = MA_OK;
        const char *task_id = NULL;

        (void)ma_task_get_id((*node)->task_, &task_id);
        MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) restarting..", task_id ? task_id : MA_EMPTY);
        if(MA_OK == (err = ma_task_stop((*node)->task_))) {
            ma_trigger_list_t *tl = NULL;
            ma_trigger_t *t = NULL;

            (void)ma_task_get_trigger_list((*node)->task_, &tl);
            ma_enumerator_reset((ma_enumerator_t*)tl);
            while(MA_OK == ma_enumerator_next((ma_enumerator_t*)tl, &t)) {
                if(MA_OK == (err = ma_trigger_core_force_stop((ma_trigger_core_t*)t->data))) {
                    if(MA_OK == (err = core_reset_start_lock((ma_trigger_core_t*)t->data)))
                        (void)ma_trigger_set_state(t, MA_TRIGGER_STATE_NOT_STARTED);
                }
            }
            (void)ma_task_set_execution_status_internal((*node)->task_, MA_TASK_EXEC_NOT_STARTED);
            err = ma_task_start((*node)->task_);
        }

		return (MA_OK == err) ? ma_task_unset_event((*node)->task_, MA_TASK_EVENT_TYPE_SYSTEM_TIME_CHANGE) : err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_taskfolder_restart(ma_taskfolder_t *folder) {
    if(folder) {
        if(MA_TASKFOLDER_STATUS_STARTED == folder->status) {
            folder->status = MA_TASKFOLDER_STATUS_RESTARTING;
            MA_TASK_TREE_TRAVERSE(210, &folder->taskmap.rbh_root, task_tree_node_restart, NULL);
            ma_taskfolder_expired_task_group_release(folder);
            folder->status = MA_TASKFOLDER_STATUS_STARTED;
        }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

/* expired task group utils */
ma_error_t ma_taskfolder_expired_task_group_find(ma_taskfolder_t *folder, ma_task_t *task, ma_bool_t *is_exist) {
    if(folder && task && is_exist) {
        ma_error_t err = MA_OK;

        *is_exist = MA_FALSE;
        if(folder->exp_task_group) {
            size_t itr = 0;

            for(itr = 0; itr < folder->no_of_exp_tasks; ++itr) {
                if(folder->exp_task_group[itr] == task) {
                    *is_exist = MA_TRUE;
                    break;
                }
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_taskfolder_expired_task_group_add(ma_taskfolder_t *folder, ma_task_t *task) {
    if(folder && task) {
        ma_error_t err = MA_OK;
        ma_bool_t task_exist = MA_FALSE;

        (void)ma_taskfolder_expired_task_group_find(folder, task, &task_exist);
        if(!task_exist) {
            err = MA_ERROR_OUTOFMEMORY;
            if(!folder->exp_task_group) {
                if((folder->exp_task_group = (ma_task_t**)calloc(1, sizeof(ma_task_t*))))
                    err = MA_OK;
            } else {
                if(folder->exp_task_group_capacity) {
                    --folder->exp_task_group_capacity;
                    err = MA_OK;
                } else {
                    if((folder->exp_task_group = (ma_task_t**)realloc(folder->exp_task_group, sizeof(ma_task_t*) * (folder->no_of_exp_tasks + 1))))
                        err = MA_OK;
                }
            }
            if(MA_OK == err) {
                folder->exp_task_group[folder->no_of_exp_tasks] = task;
                ++folder->no_of_exp_tasks;
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_taskfolder_expired_task_group_release(ma_taskfolder_t *folder) {
    if(folder && folder->exp_task_group && folder->no_of_exp_tasks) {
        size_t itr = 0;

        for(itr = folder->no_of_exp_tasks; itr > 0 ; --itr) {
            if(folder->exp_task_group[itr - 1]) {
                ma_tasknode_t *node = NULL;

                if(node = find_task(&folder->taskmap, folder->exp_task_group[itr - 1])) {
                    --folder->no_of_exp_tasks;
                    ++folder->exp_task_group_capacity;
                    RB_REMOVE(ma_tasktree_s, &folder->taskmap, node);
                    (void)ma_task_release(node->task_);
                }
            }            
        }

        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_taskfolder_get_status(ma_taskfolder_t *folder, int *status) {
    if(folder && status) {
        *status = folder->status;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

