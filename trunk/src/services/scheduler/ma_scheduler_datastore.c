#include "ma/internal/services/scheduler/ma_scheduler_datastore.h"
#include "ma/internal/services/scheduler/ma_scheduler_core.h"
#include "ma/internal/services/scheduler/ma_taskfolder.h"
#include "ma/scheduler/ma_task.h"
#include "ma/scheduler/ma_triggerlist.h"
#include "ma/internal/services/scheduler/ma_triggers_internal.h"
#include "ma/internal/services/scheduler/ma_task_internal.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"
#include "ma/internal/defs/ma_scheduler_defs.h"
#include "ma/scheduler/ma_enumerator.h"
#include "ma_scheduler_private.h"

#include <string.h>
#include <stdlib.h>

extern ma_logger_t *logger;

#define TASK_DATA MA_DATASTORE_PATH_SEPARATOR "task_data"
#define TASK_ID MA_DATASTORE_PATH_SEPARATOR "task_id"

#define TASK_DATA_PATH TASK_DATA TASK_ID MA_DATASTORE_PATH_SEPARATOR/* \"task_data"\"task_id"\ */


ma_error_t ma_scheduler_datastore_read(ma_scheduler_t *scheduler, ma_taskfolder_t * folder, ma_ds_t *datastore, const char *ds_path) {
    if(scheduler && folder && datastore && ds_path) {
        ma_error_t err = MA_OK;
        ma_ds_iterator_t *task_iterator = NULL;
        ma_buffer_t *task_id_buf = NULL;
        char task_data_base_path[MA_MAX_PATH_LEN] = {0};

        MA_MSC_SELECT(_snprintf, snprintf)(task_data_base_path, MA_MAX_PATH_LEN, "%s%s", ds_path, TASK_DATA_PATH);
        MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler data store path(%s) reading...", task_data_base_path);
        if(MA_OK == (err = ma_ds_iterator_create(datastore, 0, task_data_base_path, &task_iterator))) {
            while(MA_ERROR_NO_MORE_ITEMS != ma_ds_iterator_get_next(task_iterator, &task_id_buf) && task_id_buf) {//create necessary paths
                const char *task_id = NULL;
                size_t size = 0;
                
                if(MA_OK == (err = ma_buffer_get_string(task_id_buf, &task_id, &size))) {
                    char task_data_path[MA_MAX_PATH_LEN] = {0};
                    ma_variant_t *task_var = NULL;
                    MA_MSC_SELECT(_snprintf, snprintf)(task_data_path, MA_MAX_PATH_LEN, "%s", task_data_base_path);/* base_path\"task_data"\"task_id"\4097 */
                    if(MA_OK == (err = ma_ds_get_variant(datastore, task_data_path, task_id, MA_VARTYPE_TABLE, &task_var))) {
                        ma_task_t *task = NULL;

                        if(MA_OK == (err = ma_task_create(task_id, &task))) {
                            if(MA_OK == (err = convert_variant_to_task(task_var, task))) {
                                if(MA_OK == (err = ma_taskfolder_add_task(folder, task))) {
                                    (void)ma_task_set_execution_status(task, MA_TASK_EXEC_NOT_STARTED);
                                    err = ma_task_set_scheduler(task, scheduler);
                                }
                            } else {
                                MA_LOG(logger, MA_LOG_SEV_ERROR, "scheduler data store - task id(%s) creation failed, last error(%d)", task_id, err);
                                (void)ma_scheduler_datastore_remove_task(task, datastore, ds_path);
                            }
                            (void)ma_task_release(task);
                        }
                        (void)ma_variant_release(task_var);
                    }
                }
                (void)ma_buffer_release(task_id_buf);
            }
            (void)ma_ds_iterator_release(task_iterator);
        }

        return ((MA_ERROR_DS_PATH_NOT_FOUND == err) || (MA_ERROR_SCHEDULER_TASK_ID_CONFLICT == err)) ? MA_OK : err;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_scheduler_datastore_write(ma_scheduler_t *scheduler, ma_task_t *task, ma_ds_t *datastore, const char *ds_path) {
    if(scheduler && task && datastore && ds_path) {
        char task_data_path[MA_MAX_PATH_LEN] = {0};
        const char *task_id = NULL;
        ma_error_t err = MA_OK;
        ma_variant_t *var = NULL;

        (void)ma_task_get_id(task, &task_id);
        MA_LOG(logger, MA_LOG_SEV_DEBUG, "persisting task id(%s)", task_id);
        MA_MSC_SELECT(_snprintf, snprintf)(task_data_path, MA_MAX_PATH_LEN, "%s%s", ds_path, TASK_DATA_PATH);
        if(MA_OK == (err = convert_task_to_variant(task, &var))) {
            if(MA_OK != ma_ds_set_variant(datastore, task_data_path, task_id, var)) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler datastore write task id(%s) failed", task_id);
            }
            (void)ma_variant_release(var);
        } else
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler datastore write task id(%s) conversion task to variant failed, last error(%d)", task_id, err);

        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_scheduler_datastore_remove_task(ma_task_t *task, ma_ds_t *datastore, const char *ds_path) {
    if(task && datastore && ds_path) {
        char task_path[MA_MAX_PATH_LEN] = {0};
        const char *task_id = NULL;
        
        (void)ma_task_get_id(task, &task_id);
        MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler removing task id(%s) from datastore", task_id);
        MA_MSC_SELECT(_snprintf, snprintf)(task_path, MA_MAX_PATH_LEN, "%s%s",ds_path, TASK_DATA_PATH );/* base_path\"task_data"\4097 */
        return ma_ds_rem(datastore, task_path, task_id, 1);
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_scheduler_datastore_clear(ma_ds_t *datastore, const char *ds_path) {
    return(datastore && ds_path) ? ma_ds_rem(datastore, ds_path, NULL, 1) : MA_ERROR_INVALIDARG;
}

