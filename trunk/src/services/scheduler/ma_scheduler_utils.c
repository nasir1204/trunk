#include "ma/internal/services/scheduler/ma_scheduler_utils.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/services/scheduler/ma_task_defs.h"
#include "ma/scheduler/ma_triggerlist.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include "ma/scheduler/ma_enumerator.h"
#include "ma/scheduler/ma_task.h"
#include "ma/scheduler/ma_triggers.h"
#include "ma/ma_variant.h"
#include "ma/ma_buffer.h"

#include <stdlib.h>

#ifdef MA_WINDOWS
#include <Windows.h>
#else
#include <sys/time.h>
#endif

#include <time.h>

MA_CPP(extern "C" {)
ma_error_t ma_task_get_trigger_list(ma_task_t *task, ma_trigger_list_t **tl);
ma_error_t ma_task_set_name(ma_task_t *task, const char *name);
ma_error_t ma_task_set_type(ma_task_t *task, const char *type);
MA_CPP(})

ma_error_t convert_seconds_to_hour_and_minutes(ma_uint64_t seconds, ma_uint16_t *hour, ma_uint16_t *minutes) {
    if(hour && minutes && (seconds >= 0)) {
        ma_uint64_t t = 0;

        t = seconds/3600;
        *hour = (ma_uint16_t)t;
        t = (ma_uint64_t) (seconds%3600)/60;
        *minutes = (ma_uint16_t)t;
        //sec = t%60;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

void get_localtime(ma_task_time_t *time) {
    if(time) {
#ifdef MA_WINDOWS
    SYSTEMTIME systime;

    GetLocalTime(&systime);
    time->year = systime.wYear;
    time->month = systime.wMonth;
    time->day   = systime.wDay;

    time->hour  = systime.wHour;
    time->minute = systime.wMinute;
    time->secs = systime.wSecond;

    //time->milliseconds = systime.wMilliseconds;
#else
    struct tm *tm_time;
    struct timeval tv;

    if ((0==gettimeofday(&tv, 0)) && NULL != (tm_time = localtime(&tv.tv_sec))) {
        time->year          = 1900 + tm_time->tm_year;
        time->month         = 1 + tm_time->tm_mon;
        time->day           = tm_time->tm_mday;

        time->hour          = tm_time->tm_hour;
        time->minute        = tm_time->tm_min;
        time->secs        = tm_time->tm_sec;

        //time->milliseconds  = tv.tv_usec / 1000;
    }
#endif
    }
}

void advance_current_date_by_seconds(ma_int64_t seconds, ma_task_time_t *new_date) {
    if(new_date) {
        ma_task_time_t tdate = {0};
        struct tm task_time = {0};
        struct tm *next_date = NULL;
        time_t tt = 0;

        get_localtime(&tdate);
        task_time.tm_hour = tdate.hour;
        task_time.tm_year = tdate.year-1900;
        task_time.tm_mon = tdate.month-1;
        task_time.tm_mday = tdate.day;
        task_time.tm_min = tdate.minute;
        task_time.tm_sec = tdate.secs;
        task_time.tm_isdst = -1;
        tt = mktime(&task_time) + seconds;

        if(tt && (next_date = localtime(&tt))) {
            new_date->year = next_date->tm_year+1900;
            new_date->month = next_date->tm_mon + 1;
            new_date->day = next_date->tm_mday;
            new_date->minute = next_date->tm_min;
            new_date->hour = next_date->tm_hour;
            new_date->secs = next_date->tm_sec;
        }
    }
}

void advance_date_by_seconds(ma_int64_t seconds, ma_task_time_t tdate, ma_task_time_t *new_date) {
    if(new_date) {
        struct tm task_time = {0};
        time_t tt = 0;
        struct tm *next_date = NULL;
            
        memset(new_date, 0, sizeof(ma_task_time_t));
        task_time.tm_hour = tdate.hour;
        task_time.tm_year = tdate.year-1900;
        task_time.tm_mon = tdate.month-1;
        task_time.tm_mday = tdate.day;
        task_time.tm_min = tdate.minute;
        task_time.tm_sec = tdate.secs;
        task_time.tm_isdst = -1;
        tt = mktime(&task_time) + seconds;

        if(tt && (next_date = localtime(&tt))) {
            new_date->year = next_date->tm_year+1900;
            new_date->month = next_date->tm_mon + 1;
            new_date->day = next_date->tm_mday;
            new_date->minute = next_date->tm_min;
            new_date->hour = next_date->tm_hour;
            new_date->secs = next_date->tm_sec;
        }
    }
}

ma_bool_t validate_date(ma_task_time_t date) {
    ma_task_time_t now = {0};
    int rc = 0;

    if(!memcmp(&date, &now, sizeof(ma_task_time_t)))return MA_FALSE;
    get_localtime(&now);
    return ((rc = memcmp(&date, &now, sizeof(ma_task_time_t))) >= 0)?MA_TRUE:MA_FALSE;
}

ma_bool_t validate_end_date(ma_task_time_t date) {
    ma_task_time_t now = {0};
    int rc = 0;

    if(!memcmp(&date, &now, sizeof(ma_task_time_t)))return MA_FALSE;
    get_localtime(&now);
    now.hour = 0; now.minute = 0; now.secs = 0;
    return ((rc = memcmp(&date, &now, sizeof(ma_task_time_t))) >= 0)?MA_TRUE:MA_FALSE;
}

void get_next_date(ma_task_time_t *next_day) {
    if(next_day) {
        time_t tt = time(NULL) + 86400; // tomorrow
        struct tm *tomorrow = NULL;

        if((tomorrow = localtime (&tt))) {
            next_day->year = 1900 + tomorrow->tm_year;
            next_day->month = 1 + tomorrow->tm_mon;
            next_day->day = tomorrow->tm_mday;
            next_day->hour = tomorrow->tm_hour;
            next_day->minute = tomorrow->tm_min;
            next_day->secs = tomorrow->tm_sec;
        }
    }
}

void compute_timeout(ma_task_time_t *tdate, ma_int64_t *timeout) {
    if(tdate && timeout) {
        ma_task_time_t now = {0};

        get_localtime(&now);
        compute_date_diff_seconds(*tdate, now, timeout);
    }
}

void compute_date_diff_seconds(ma_task_time_t end_date, ma_task_time_t begin_date, ma_int64_t *diff) {
    if(diff) {
        struct tm begin_time = {0};
        struct tm end_time = {0};
        double seconds = 0;
        
        begin_time.tm_hour = begin_date.hour;
        begin_time.tm_year = begin_date.year-1900;
        begin_time.tm_mon = begin_date.month-1;
        begin_time.tm_mday = begin_date.day;
        begin_time.tm_min = begin_date.minute;
        begin_time.tm_sec = begin_date.secs;
        begin_time.tm_isdst = -1;

        end_time.tm_hour = end_date.hour;
        end_time.tm_year = end_date.year-1900;
        end_time.tm_mon = end_date.month-1;
        end_time.tm_mday = end_date.day;
        end_time.tm_min = end_date.minute;
        end_time.tm_sec = end_date.secs;
        end_time.tm_isdst = -1;
        
        seconds = difftime(mktime(&end_time), mktime(&begin_time));
        *diff = seconds;
    }
}

void get_week_day(ma_task_time_t *tdate, ma_day_of_week_t *week_day) {
    if(tdate && week_day) {
        time_t now = {0};
        struct tm task_time = {0};

        time(&now);
        task_time.tm_hour = tdate->hour;
        task_time.tm_year = tdate->year-1900;
        task_time.tm_mon = tdate->month-1;
        task_time.tm_mday = tdate->day;
        task_time.tm_min = tdate->minute;
        task_time.tm_sec = tdate->secs;
        task_time.tm_isdst = -1;
        mktime(&task_time);
        switch(task_time.tm_wday) {
            case 0: *week_day = SUNDAY; break;
            case 1: *week_day = MONDAY; break;
            case 2: *week_day = TUESDAY; break;
            case 3: *week_day = WEDNESDAY; break;
            case 4: *week_day = THURSDAY; break;
            case 5: *week_day = FRIDAY; break;
            case 6: *week_day = SATURDAY; break;
        }
    }
}

void get_next_week_date_after(ma_task_time_t start_date, ma_uint32_t days_of_week, ma_uint16_t weeks_interval, ma_task_time_t *new_date) {
    if(new_date) {
        memset(new_date, 0, sizeof(ma_task_time_t));
        if(days_of_week) {
            time_t tt = 0;
            struct tm tmp_date = {0};
            struct tm *next_date = NULL;
            int no_of_days = 0;
            ma_uint32_t today = 0;
            ma_bool_t just_this_week = weeks_interval?0:1;

            tmp_date.tm_hour = start_date.hour;
            tmp_date.tm_year = start_date.year-1900;
            tmp_date.tm_mon = start_date.month-1;
            tmp_date.tm_mday = start_date.day;
            tmp_date.tm_min = start_date.minute;
            tmp_date.tm_sec = start_date.secs;
            tmp_date.tm_isdst = -1;
            mktime(&tmp_date);
            no_of_days = today = tmp_date.tm_wday;
            while(++no_of_days) {
                if(just_this_week) {
                    if(no_of_days < MA_NO_OF_DAYS_IN_WEEK) {
                        if(MA_CHECK_BIT(days_of_week, (1 << no_of_days))) {
                            tt = mktime(&tmp_date) + (86400 * (no_of_days - today));
                            break;
                        }
                    } else break;
                } else {
                    if(MA_CHECK_BIT(days_of_week, (1 << (no_of_days%MA_NO_OF_DAYS_IN_WEEK)))) {
                        if(no_of_days > MA_NO_OF_DAYS_IN_WEEK) {
                            if(no_of_days / (weeks_interval * MA_NO_OF_DAYS_IN_WEEK)) {
                                no_of_days -= MA_NO_OF_DAYS_IN_WEEK;
                            }
                            if((((no_of_days - today) + (weeks_interval * MA_NO_OF_DAYS_IN_WEEK)) / MA_NO_OF_DAYS_IN_WEEK) > weeks_interval) {
                                weeks_interval--;
                            }
                            tt = mktime(&tmp_date) + (86400 * ((no_of_days - today) + (weeks_interval * MA_NO_OF_DAYS_IN_WEEK)));
                            break;
                        } else {
                            tt = mktime(&tmp_date) + (86400 * ((no_of_days - today) + ((weeks_interval - 1) * MA_NO_OF_DAYS_IN_WEEK)));
                            break;
                        }
                    }
                }
            }
            if(tt && (next_date = localtime(&tt))) {
                new_date->year = next_date->tm_year+1900;
                new_date->month = next_date->tm_mon + 1;
                new_date->day = next_date->tm_mday;
                new_date->minute = next_date->tm_min;
                new_date->hour = next_date->tm_hour;
                new_date->secs = next_date->tm_sec;
            }
        }
    }
}

void get_next_date_after(ma_task_time_t start_date, ma_task_time_t *new_date, int no_of_days) {
    if(new_date) {
        time_t tt = 0;
        struct tm tmp_date = {0};
        struct tm *next_date = NULL;

        tmp_date.tm_hour = start_date.hour;
        tmp_date.tm_year = start_date.year-1900;
        tmp_date.tm_mon = start_date.month - 1;
        tmp_date.tm_mday = start_date.day;
        tmp_date.tm_min = start_date.minute;
        tmp_date.tm_sec = start_date.secs;
        tmp_date.tm_isdst = -1;
        tt = mktime(&tmp_date) + (86400 * no_of_days);
        if((next_date = localtime(&tt))) {
            new_date->year = next_date->tm_year+1900;
            new_date->month = next_date->tm_mon + 1;
            new_date->day = next_date->tm_mday;
            new_date->minute = next_date->tm_min;
            new_date->hour = next_date->tm_hour;
                new_date->secs = next_date->tm_sec;
        }
    }
}

ma_bool_t check_today_last_day(ma_task_time_t today, ma_task_time_t end_date) {
    return((end_date.year == today.year) && (end_date.month == today.month) && (end_date.day == today.day))?MA_TRUE:MA_FALSE;
}

ma_bool_t is_last_day_later(ma_task_time_t end_date, ma_task_time_t next_date) {
    ma_task_time_t dummy = {0};
    struct tm end_date_task_time = {0};
    struct tm next_date_task_time = {0};

    if(memcmp(&dummy, &end_date, sizeof(ma_task_time_t))) {

        end_date_task_time.tm_hour = end_date.hour;
        end_date_task_time.tm_year = end_date.year-1900;
        end_date_task_time.tm_mon = end_date.month-1;
        end_date_task_time.tm_mday = end_date.day;
        end_date_task_time.tm_min = 0;
        end_date_task_time.tm_sec = 0;

        next_date_task_time.tm_year = next_date.year-1900;
        next_date_task_time.tm_mon = next_date.month-1;
        next_date_task_time.tm_mday = next_date.day;
        next_date_task_time.tm_min = 0;
        next_date_task_time.tm_hour = 0;
        next_date_task_time.tm_sec = 0;
        next_date_task_time.tm_isdst = -1;

        return ((int)mktime(&next_date_task_time) <= (int)mktime(&end_date_task_time))?MA_TRUE:MA_FALSE;
    }

    return MA_TRUE;
}

void get_current_month_date(ma_task_time_t start_date,  ma_uint32_t months, ma_uint16_t day, ma_task_time_t *new_date) {
    if(new_date) {
        memset(new_date, 0, sizeof(ma_task_time_t));
        if(months) {
            time_t tt = 0;
            struct tm tmp_date = {0};
            struct tm *next_date = NULL;
            ma_uint32_t curr_month = 0;
            int no_of_months = 0;
            int default_day = 1; /* first of every month */

            tmp_date.tm_hour = start_date.hour;
            tmp_date.tm_year = start_date.year-1900;
            tmp_date.tm_mon = start_date.month-1;
            tmp_date.tm_mday = default_day;
            tmp_date.tm_min = start_date.minute;
            tmp_date.tm_sec = start_date.secs;
            tmp_date.tm_isdst = -1;
            mktime(&tmp_date);
            no_of_months = curr_month = tmp_date.tm_mon;
            if(no_of_months < 12) {
                if(MA_CHECK_BIT(months, (1 << no_of_months))) {
                    tmp_date.tm_mon = no_of_months;
                    tmp_date.tm_isdst = -1;
                    tt = mktime(&tmp_date);
                    if(tt && (next_date = localtime(&tt))) {
                        new_date->year = next_date->tm_year+1900;
                        new_date->month = next_date->tm_mon + 1;
                        new_date->day = next_date->tm_mday;// != day ? default_day : next_date->tm_mday;
                        new_date->minute = next_date->tm_min;
                        new_date->hour = next_date->tm_hour;
                        new_date->secs = next_date->tm_sec;
                        if(ma_date_validator(day, new_date->month, new_date->year))
                            new_date->day = day;
                        else
                            get_next_month_date_after(*new_date, months, day, new_date);
                    }
                }
            }
        }
    }
}

void get_next_month_date_after(ma_task_time_t start_date,  ma_uint32_t months, ma_uint16_t day, ma_task_time_t *new_date) {
    if(new_date) {
        memset(new_date, 0, sizeof(ma_task_time_t));
        if(months) {
            time_t tt = 0;
            struct tm tmp_date = {0};
            struct tm *next_date = NULL;
            ma_uint32_t curr_month = 0;
            int no_of_months = 0;
            int default_day = 1; /* first of every month */

            tmp_date.tm_hour = start_date.hour;
            tmp_date.tm_year = start_date.year-1900;
            tmp_date.tm_mon = start_date.month-1;
            tmp_date.tm_mday = default_day;
            tmp_date.tm_min = start_date.minute;
            tmp_date.tm_sec = start_date.secs;
            tmp_date.tm_isdst = -1;
            mktime(&tmp_date);
            no_of_months = curr_month = tmp_date.tm_mon;
            while(++no_of_months) {
                if(no_of_months < 12) {
                    if(MA_CHECK_BIT(months, (1 << no_of_months))) {
                        tmp_date.tm_mon = no_of_months;
                        tmp_date.tm_isdst = -1;
                        tt = mktime(&tmp_date);
                        if(tt && (next_date = localtime(&tt))) {
                            new_date->year = next_date->tm_year+1900;
                            new_date->month = next_date->tm_mon + 1;
                            new_date->day = next_date->tm_mday; //!= day ? default_day : next_date->tm_mday;
                            new_date->minute = next_date->tm_min;
                            new_date->hour = next_date->tm_hour;
                            new_date->secs = next_date->tm_sec;
                        }
                        if(ma_date_validator(day, new_date->month, new_date->year)) {
                            new_date->day = day;
                            break;
                        } else
                            tmp_date.tm_mon = curr_month;

                    }
                } else {
                    if(MA_CHECK_BIT(months, (1 << (no_of_months%12)))) {
                        tmp_date.tm_mon += (no_of_months - curr_month);
                        tmp_date.tm_isdst = -1;
                        tt = mktime(&tmp_date);
                        if(tt && (next_date = localtime(&tt))) {
                            new_date->year = next_date->tm_year+1900;
                            new_date->month = next_date->tm_mon + 1;
                            new_date->day = next_date->tm_mday; //!= day ? default_day : next_date->tm_mday;
                            new_date->minute = next_date->tm_min;
                            new_date->hour = next_date->tm_hour;
                            new_date->secs = next_date->tm_sec;
                        }
                        if(ma_date_validator(day, new_date->month, new_date->year)) {
                            new_date->day = day;
                            break;
                        } else
                            tmp_date.tm_mon -= (no_of_months - curr_month);
                    }
                }
            }
        }
    }
}

void get_current_month_dow(ma_task_time_t start_date, ma_uint32_t months, ma_uint16_t which_week, ma_uint32_t days_of_week, ma_task_time_t *new_date) {
    if(new_date) {
        memset(new_date, 0, sizeof(ma_task_time_t));
        if(months) {
            time_t tt = 0;
            struct tm tmp_date = {0};
            struct tm *next_date = NULL;
            int curr_month = 0;
            int no_of_months = 0;
            ma_uint32_t today = 0;
            int weeks_interval = 0;
            int no_of_days = 0;

            tmp_date.tm_hour = start_date.hour;
            tmp_date.tm_year = start_date.year-1900;
            tmp_date.tm_mon = start_date.month-1;
            tmp_date.tm_mday = 1;
            tmp_date.tm_min = start_date.minute;
            tmp_date.tm_sec = start_date.secs;
            tmp_date.tm_isdst = -1;
            mktime(&tmp_date);
            no_of_months = curr_month = tmp_date.tm_mon;
            if(no_of_months < 12) {
                if(MA_CHECK_BIT(months, (1 << no_of_months))) {
                    tmp_date.tm_mon = no_of_months;
                    tmp_date.tm_isdst = -1;
                    tt = mktime(&tmp_date);
                    weeks_interval = which_week;
                    no_of_days = today = tmp_date.tm_wday;
                    curr_month = tmp_date.tm_mon;
                    while(++no_of_days) {
                        if(MA_CHECK_BIT(days_of_week, (1 << (no_of_days%MA_NO_OF_DAYS_IN_WEEK)))) {
                            int interleaved = (no_of_days - today) == MA_NO_OF_DAYS_IN_WEEK ? 0 : (no_of_days - today);

                            tmp_date.tm_sec += 86400 * (interleaved + ((weeks_interval - 1) * MA_NO_OF_DAYS_IN_WEEK));
                            tt = mktime(&tmp_date);
                            break;
                        }
                    }
                    if(tmp_date.tm_mon != curr_month) {
                        tmp_date.tm_mday -= MA_NO_OF_DAYS_IN_WEEK;
                        tmp_date.tm_isdst = -1;
                        tt = mktime(&tmp_date);
                    }
                    if(tt && (next_date = localtime(&tt))) {
                        new_date->year = next_date->tm_year+1900;
                        new_date->month = next_date->tm_mon + 1;
                        new_date->day = next_date->tm_mday;
                        new_date->minute = next_date->tm_min;
                        new_date->hour = next_date->tm_hour;
                        new_date->secs = next_date->tm_sec;
                    }
                }
            }
        }
    }
}

void get_next_month_dow_date_after(ma_task_time_t start_date, ma_uint32_t months, ma_uint16_t which_week, ma_uint32_t days_of_week, ma_task_time_t *new_date) {
    if(new_date) {
        memset(new_date, 0, sizeof(ma_task_time_t));
        if(months) {
            time_t tt = 0;
            struct tm tmp_date = {0};
            struct tm *next_date = NULL;
            int curr_month = 0;
            int no_of_months = 0;
            ma_uint32_t today = 0;
            int weeks_interval = 0;
            int no_of_days = 0;

            tmp_date.tm_hour = start_date.hour;
            tmp_date.tm_year = start_date.year-1900;
            tmp_date.tm_mon = start_date.month-1;
            tmp_date.tm_mday = 1;
            tmp_date.tm_min = start_date.minute;
            tmp_date.tm_sec = start_date.secs;
            tmp_date.tm_isdst = -1;
            mktime(&tmp_date);
            no_of_months = curr_month = tmp_date.tm_mon;
            while(++no_of_months) {
                if(no_of_months < 12) {
                    if(MA_CHECK_BIT(months, (1 << no_of_months))) {
                        tmp_date.tm_mon = no_of_months;
                        tmp_date.tm_isdst = -1;
                        tt = mktime(&tmp_date);
                        break;
                    }
                } else {
                    if(MA_CHECK_BIT(months, (1 << (no_of_months%12)))) {
                        tmp_date.tm_mon += (no_of_months - curr_month);
                        tmp_date.tm_isdst = -1;
                        tt = mktime(&tmp_date);
                        break;
                    }
                }
            }
            weeks_interval = which_week;
            no_of_days = today = tmp_date.tm_wday;
            curr_month = tmp_date.tm_mon;
            while(++no_of_days) {
                if(MA_CHECK_BIT(days_of_week, (1 << (no_of_days%MA_NO_OF_DAYS_IN_WEEK)))) {
                    int interleaved = (no_of_days - today) == MA_NO_OF_DAYS_IN_WEEK ? 0 : (no_of_days - today);

                    tmp_date.tm_sec += 86400 * (interleaved + ((weeks_interval - 1) * MA_NO_OF_DAYS_IN_WEEK));
                    tt = mktime(&tmp_date);
                    break;
                }
            }
            if(tmp_date.tm_mon != curr_month) {
                tmp_date.tm_mday -= MA_NO_OF_DAYS_IN_WEEK;
                tmp_date.tm_isdst = -1;
                tt = mktime(&tmp_date);
            }
            if(tt && (next_date = localtime(&tt))) {
                new_date->year = next_date->tm_year+1900;
                new_date->month = next_date->tm_mon + 1;
                new_date->day = next_date->tm_mday;
                new_date->minute = next_date->tm_min;
                new_date->hour = next_date->tm_hour;
                new_date->secs = next_date->tm_sec;
            }
        }
    }
}

ma_error_t convert_task_to_variant(ma_task_t *task, ma_variant_t **task_variant) {
    if(task && task_variant) {
        ma_error_t err = MA_OK;
        ma_table_t *task_props = NULL;

        if(MA_OK == (err = ma_table_create(&task_props))) {

            {
                ma_trigger_list_t *list = NULL;
                if((MA_OK == err) && (MA_OK == (err = ma_task_get_trigger_list(task, &list))) && list) {
                    ma_array_t *trigger_set = NULL;
                    if(MA_OK == (err = ma_array_create(&trigger_set))) {
                        ma_trigger_t *trigger = NULL;
                        (void)ma_enumerator_reset((ma_enumerator_t*)list);
                        while(MA_OK == ma_enumerator_next((ma_enumerator_t*)list, &trigger)) {
                            ma_table_t *trigger_props = NULL;

                            if(MA_OK == (err = ma_table_create(&trigger_props))) {

                                {
                                    const char *id = NULL;
                                    if((MA_OK == err) &&  (MA_OK == (err = ma_trigger_get_id(trigger, &id)))) {
                                        ma_variant_t *id_variant = NULL;
                                        if(MA_OK == (err = ma_variant_create_from_string(id, strlen(id), &id_variant))) {
                                            err = ma_table_add_entry(trigger_props, MA_TRIGGER_ATTR_ID, id_variant);
                                            (void)ma_variant_release(id_variant);
                                        }
                                    }
                                }

                                {
                                    ma_trigger_type_t type = MA_TRIGGER_ONCE;
                                    if((MA_OK == err) && (MA_OK == (err = ma_trigger_get_type(trigger, &type)))) {
                                        ma_variant_t *trigger_type_variant = NULL;
                                        if(MA_OK == (err = ma_variant_create_from_uint32(type, &trigger_type_variant))) {
                                            err = ma_table_add_entry(trigger_props, MA_TRIGGER_ATTR_TYPE, trigger_type_variant);
                                            (void)ma_variant_release(trigger_type_variant);
                                        }
                                    }
                                }

                                {
                                    ma_trigger_type_t type = MA_TRIGGER_ONCE;
                                    if((MA_OK == err) && (MA_OK == (err = ma_trigger_get_type(trigger, &type)))) {
                                        ma_temp_buffer_t policy_buf = {0};

                                        ma_temp_buffer_init(&policy_buf);
                                        if(MA_OK == (err = ma_trigger_get_policy(trigger, ma_temp_buffer_get(&policy_buf)))) {
                                            size_t size = 0;
                                            ma_variant_t *policy_variant = NULL;

                                            switch(type) {
                                                case MA_TRIGGER_NOW:size = sizeof(ma_trigger_run_now_policy_t);break;
                                                case MA_TRIGGER_ONCE:size = sizeof(ma_trigger_run_once_policy_t);break;
                                                case MA_TRIGGER_DAILY:size = sizeof(ma_trigger_daily_policy_t);break;
                                                case MA_TRIGGER_WEEKLY:size = sizeof(ma_trigger_weekly_policy_t);break;
                                                case MA_TRIGGER_MONTHLY_DATE:size = sizeof(ma_trigger_monthly_date_policy_t);break;
                                                case MA_TRIGGER_MONTHLY_DOW:size = sizeof(ma_trigger_monthly_dow_policy_t);break;
                                                case MA_TRIGGER_ON_SYSTEM_IDLE:size = sizeof(ma_trigger_system_idle_policy_t);break;
                                                case MA_TRIGGER_ON_DIALUP:size = sizeof(ma_trigger_dialup_policy_t);break;
                                                case MA_TRIGGER_AT_SYSTEM_START:size = sizeof(ma_trigger_systemstart_policy_t);break;
                                                case MA_TRIGGER_AT_LOGON:size = sizeof(ma_trigger_logon_policy_t);break;
                                            } //end of switch
                                            if(MA_OK == (err = ma_variant_create_from_raw(ma_temp_buffer_get(&policy_buf), size, &policy_variant))) {
                                                err = ma_table_add_entry(trigger_props, MA_TRIGGER_ATTR_POLICY, policy_variant);
                                                (void)ma_variant_release(policy_variant);
                                            }
                                        }
                                        ma_temp_buffer_uninit(&policy_buf);
                                    }
                                }

                                {
                                    ma_task_time_t begin_date = {0};
                                    if((MA_OK == err) && (MA_OK == (err =  ma_trigger_get_begin_date(trigger, &begin_date)))) {
                                        ma_variant_t *begin_date_variant = NULL;
                                        if(MA_OK == (err =  ma_variant_create_from_raw(&begin_date, sizeof(ma_task_time_t), &begin_date_variant))) {
                                            err = ma_table_add_entry(trigger_props, MA_TRIGGER_ATTR_BEGIN_DATE, begin_date_variant);
                                            (void)ma_variant_release(begin_date_variant);
                                        }
                                    }
                                }

                                {
                                    ma_trigger_state_t state = MA_TRIGGER_STATE_NOT_STARTED;
                                    if((MA_OK == err) && (MA_OK == (err = ma_trigger_get_state(trigger, &state)))) {
                                        ma_variant_t *state_variant = NULL;
                                        if(MA_OK == (err = ma_variant_create_from_uint32(state, &state_variant))) {
                                            err = ma_table_add_entry(trigger_props, MA_TRIGGER_ATTR_STATE, state_variant);
                                            (void)ma_variant_release(state_variant);
                                        }
                                    }
                                }

                                {
                                    ma_task_time_t end_date = {0};
                                    if((MA_OK == err) && (MA_OK == (err =  ma_trigger_get_end_date(trigger, &end_date)))) {
                                        ma_variant_t *end_date_variant = NULL;
                                        if(MA_OK == (err =  ma_variant_create_from_raw(&end_date, sizeof(ma_task_time_t), &end_date_variant))) {
                                            err = ma_table_add_entry(trigger_props, MA_TRIGGER_ATTR_END_DATE, end_date_variant);
                                            (void)ma_variant_release(end_date_variant);
                                        }
                                    }
                                }

                                {
                                    ma_task_time_t next_fire_date = {0};
                                    if((MA_OK == err) && (MA_OK == (err =  ma_trigger_get_next_fire_date(trigger, &next_fire_date)))) {
                                        ma_variant_t *next_fire_date_variant = NULL;

                                        if(MA_OK == (err =  ma_variant_create_from_raw(&next_fire_date, sizeof(ma_task_time_t), &next_fire_date_variant))) {
                                            err = ma_table_add_entry(trigger_props, MA_TRIGGER_ATTR_NEXT_FIRE_DATE, next_fire_date_variant);
                                            (void)ma_variant_release(next_fire_date_variant);
                                        }
                                    }
                                }

                                {
                                    ma_task_time_t last_fire_date = {0};
                                    if((MA_OK == err) && (MA_OK == (err =  ma_trigger_get_last_fire_date(trigger, &last_fire_date)))) {
                                        ma_variant_t *last_fire_date_variant = NULL;

                                        if(MA_OK == (err =  ma_variant_create_from_raw(&last_fire_date, sizeof(ma_task_time_t), &last_fire_date_variant))) {
                                            err = ma_table_add_entry(trigger_props, MA_TRIGGER_ATTR_LAST_FIRE_DATE, last_fire_date_variant);
                                            (void)ma_variant_release(last_fire_date_variant);
                                        }
                                    }
                                }

                                {
                                    ma_variant_t *trigger_variant = NULL;
                                    if((MA_OK == err) && (MA_OK == (err = ma_variant_create_from_table(trigger_props, &trigger_variant)))) {
                                        err = ma_array_push(trigger_set, trigger_variant);
                                        (void)ma_variant_release(trigger_variant);
                                    }
                                }

                                (void)ma_table_release(trigger_props);
                            } // end of trigger

                            trigger = NULL;
                        } // end of while

                        {
                            ma_variant_t *trigger_list_variant = NULL;
                            if((MA_OK == err) && (MA_OK == (err = ma_variant_create_from_array(trigger_set, &trigger_list_variant)))) {
                                err = ma_table_add_entry(task_props, MA_TASK_ATTR_TRIGGER_LIST, trigger_list_variant);
                                (void)ma_variant_release(trigger_list_variant);
                            }
                        }

                        (void)ma_array_release(trigger_set);
                    }
                }
            }
            {
                ma_variant_t *app_payload = NULL;
                if((MA_OK == err) && (MA_OK == ma_task_get_app_payload(task, &app_payload) && app_payload)) {
                    err = ma_table_add_entry(task_props, MA_TASK_ATTR_APP_PAYLOAD, app_payload);
                    (void)ma_variant_release(app_payload);
                }
            }
            {
                const char *task_id = NULL;
                if((MA_OK == err) && (MA_OK == (err = ma_task_get_id(task, &task_id)))) {
                    ma_variant_t *task_id_variant = NULL;

                    if(MA_OK == (err =  ma_variant_create_from_string(task_id, strlen(task_id), &task_id_variant))) {
                        err = ma_table_add_entry(task_props, MA_TASK_ATTR_ID, task_id_variant);
                        (void)ma_variant_release(task_id_variant);
                    }
                }
            }
            {
                const char *task_name = NULL;
                if((MA_OK == err) && (MA_OK == (err = ma_task_get_name(task, &task_name)))) {
                    ma_variant_t *task_name_variant = NULL;

                    if(MA_OK == (err =  ma_variant_create_from_string(task_name, strlen(task_name), &task_name_variant))) {
                        err = ma_table_add_entry(task_props, MA_TASK_ATTR_NAME, task_name_variant);
                        (void)ma_variant_release(task_name_variant);
                    }
                }
            }

            {
                ma_time_zone_t zone = MA_TIME_ZONE_LOCAL;
                if((MA_OK == err) && (MA_OK == (err = ma_task_get_time_zone(task, &zone)))) {
                    ma_variant_t *zone_var = NULL;

                    if(MA_OK == (err = ma_variant_create_from_uint32((ma_uint32_t)zone, &zone_var))) {
                        err = ma_table_add_entry(task_props, MA_TASK_ATTR_TIME_ZONE, zone_var);
                        (void)ma_variant_release(zone_var);
                    }
                }
            }

            {
                ma_bool_t missed = MA_FALSE;
                if((MA_OK == err) && (MA_OK == (err = ma_task_get_missed(task, &missed)))) {
                    ma_variant_t *missed_var = NULL;

                    if(MA_OK == (err = ma_variant_create_from_uint32((ma_uint32_t)missed, &missed_var))) {
                        err = ma_table_add_entry(task_props, MA_TASK_ATTR_MISSED, missed_var);
                        (void)ma_variant_release(missed_var);
                    }
                }
            }

            {
                const char *software_id = NULL;
                if((MA_OK == err) && (MA_OK == ma_task_get_software_id(task, &software_id)) && software_id) {
                    ma_variant_t *software_id_variant = NULL;

                    if(MA_OK == (err =  ma_variant_create_from_string(software_id, strlen(software_id), &software_id_variant))) {
                        err = ma_table_add_entry(task_props, MA_TASK_ATTR_SOFTWARE_ID, software_id_variant);
                        (void)ma_variant_release(software_id_variant);
                    }
                }
            }

            {
                const char *task_type = NULL;
                if((MA_OK == err) && (MA_OK == (err = ma_task_get_type(task, &task_type)))) {
                    ma_variant_t *task_type_variant = NULL;

                    if(MA_OK == (err =  ma_variant_create_from_string(task_type, strlen(task_type), &task_type_variant))) {
                        err = ma_table_add_entry(task_props, MA_TASK_ATTR_TYPE, task_type_variant);
                        (void)ma_variant_release(task_type_variant);
                    }
                }
            }

            {
                ma_uint32_t conds = 0;
                if((MA_OK == err) && (MA_OK == (err = ma_task_get_conditions(task, &conds)))) {
                    ma_variant_t *cond_variant = NULL;

                    if(MA_OK == (err = ma_variant_create_from_uint32(conds, &cond_variant))) {
                        err = ma_table_add_entry(task_props, MA_TASK_ATTR_CONDITIONS, cond_variant);
                        (void)ma_variant_release(cond_variant);
                    }
                }
            }

            {
                ma_task_retry_policy_t policy = {0};
                if((MA_OK == err) && (MA_OK == (err = ma_task_get_retry_policy(task, &policy)))) {
                    ma_variant_t *retry_variant = NULL;

                    if(MA_OK == (err = ma_variant_create_from_raw(&policy, sizeof(ma_task_retry_policy_t), &retry_variant))) {
                        err = ma_table_add_entry(task_props, MA_TASK_ATTR_RETRY_POLICY, retry_variant);
                        (void)ma_variant_release(retry_variant);
                    }
                }
            }

            {
                ma_task_power_policy_t policy = {0};
                if((MA_OK == err) && (MA_OK == (err = ma_task_get_power_policy(task, &policy)))) {
                    ma_variant_t *power_variant = NULL;

                    if(MA_OK == (err = ma_variant_create_from_raw(&policy, sizeof(ma_task_power_policy_t), &power_variant))) {
                        err = ma_table_add_entry(task_props, MA_TASK_ATTR_POWER_POLICY, power_variant);
                        (void)ma_variant_release(power_variant);
                    }
                }
            }

            {
                ma_task_randomize_policy_t policy = {{0}};
                if((MA_OK == err) && (MA_OK == ma_task_get_randomize_policy(task, &policy))) {
                    ma_variant_t *randomize_var = NULL;
                    if(MA_OK == (err = ma_variant_create_from_raw(&policy, sizeof(ma_task_randomize_policy_t), &randomize_var))) {
                        err = ma_table_add_entry(task_props, MA_TASK_ATTR_RANDOMIZE_POLICY, randomize_var);
                        (void)ma_variant_release(randomize_var);
                    }
                }
            }

            {
                ma_task_delay_policy_t delay = {{0}};
                if((MA_OK == err) && (MA_OK == ma_task_get_delay_policy(task, &delay))) {
                    ma_variant_t *delay_var = NULL;
                    if(MA_OK == (err = ma_variant_create_from_raw(&delay, sizeof(ma_task_delay_policy_t), &delay_var))) {
                        err = ma_table_add_entry(task_props, MA_TASK_ATTR_DELAY_POLICY, delay_var);
                        (void)ma_variant_release(delay_var);
                    }
                }
            }

            {
                ma_task_repeat_policy_t policy = {{0}};
                if((MA_OK == err) && (MA_OK == (err = ma_task_get_repeat_policy(task, &policy)))) {
                    ma_variant_t *repeat_variant = NULL;

                    if(MA_OK == (err = ma_variant_create_from_raw(&policy, sizeof(ma_task_repeat_policy_t), &repeat_variant))) {
                        err = ma_table_add_entry(task_props, MA_TASK_ATTR_REPEAT_POLICY, repeat_variant);
                        (void)ma_variant_release(repeat_variant);
                    }
                }
            }

            {
                ma_uint16_t max_run_time = 0;
                if((MA_OK == err) && (MA_OK == (err = ma_task_get_max_run_time(task, &max_run_time)))) {
                    ma_variant_t *max_run_time_variant = NULL;
                    if(MA_OK == (err = ma_variant_create_from_uint16(max_run_time, &max_run_time_variant))) {
                        err = ma_table_add_entry(task_props, MA_TASK_ATTR_MAX_RUN_TIME, max_run_time_variant);
                        (void)ma_variant_release(max_run_time_variant);
                    }
                }
            }

            {
                ma_uint16_t max_run_time_limit = 0;
                if((MA_OK == err) && (MA_OK == (err = ma_task_get_max_run_time_limit(task, &max_run_time_limit)))) {
                    ma_variant_t *max_run_time_limit_variant = NULL;
                    if(MA_OK == (err = ma_variant_create_from_uint16(max_run_time_limit, &max_run_time_limit_variant))) {
                        err = ma_table_add_entry(task_props, MA_TASK_ATTR_MAX_RUN_TIME_LIMIT, max_run_time_limit_variant);
                        (void)ma_variant_release(max_run_time_limit_variant);
                    }
                }
            }

            {
                ma_task_missed_policy_t policy = {{0}};
                if((MA_OK == err) && (MA_OK == (err = ma_task_get_missed_policy(task, &policy)))) {
                    ma_variant_t *missed_variant = NULL;

                    if(MA_OK == (err = ma_variant_create_from_raw(&policy, sizeof(ma_task_missed_policy_t), &missed_variant))) {
                        err = ma_table_add_entry(task_props, MA_TASK_ATTR_MISSED_POLICY, missed_variant);
                        (void)ma_variant_release(missed_variant);
                    }
                }
            }

            {
                ma_task_priority_t priority = MA_TASK_PRIORITY_LOW;
                if((MA_OK == err) && (MA_OK == (err = ma_task_get_priority(task, &priority)))) {
                    ma_variant_t *priority_variant = NULL;
                    if(MA_OK == (err = ma_variant_create_from_uint32(priority, &priority_variant))) {
                        err = ma_table_add_entry(task_props, MA_TASK_ATTR_PRIORITY, priority_variant);
                        (void)ma_variant_release(priority_variant);
                    }
                }
            }

            {
                ma_task_state_t state = MA_TASK_STATE_NOT_SCHEDULED;
                if((MA_OK == err) && (MA_OK == (err = ma_task_get_state(task, &state)))) {
                    ma_variant_t *state_variant = NULL;
                    if(MA_OK == (err = ma_variant_create_from_uint32(state, &state_variant))) {
                        err = ma_table_add_entry(task_props, MA_TASK_ATTR_STATE, state_variant);
                        (void)ma_variant_release(state_variant);
                    }
                }
            }

            {
                ma_task_exec_status_t status = MA_TASK_EXEC_NOT_STARTED;
                if((MA_OK == err) && (MA_OK == (err = ma_task_get_execution_status(task, &status)))) {
                    ma_variant_t *status_variant = NULL;
                    if(MA_OK == (err = ma_variant_create_from_uint32(status, &status_variant))) {
                        err = ma_table_add_entry(task_props, MA_TASK_ATTR_EXECUTION_STATUS, status_variant);
                        (void)ma_variant_release(status_variant);
                    }
                }
            }

            {
                ma_task_time_t last_run_time = {0};
                if((MA_OK == err) && (MA_OK == (err = ma_task_get_last_run_time(task, &last_run_time)))) {
                    ma_variant_t *last_run_time_var = NULL;
                    if(MA_OK == (err = ma_variant_create_from_raw(&last_run_time, sizeof(ma_task_time_t), &last_run_time_var))) {
                        err = ma_table_add_entry(task_props, MA_TASK_ATTR_LAST_RUN_TIME, last_run_time_var);
                        (void)ma_variant_release(last_run_time_var);
                    }
                }
            }

            {
                ma_task_time_t next_run_time = {0};
                if((MA_OK == err) && (MA_OK == (err = ma_task_get_next_run_time(task, &next_run_time)))) {
                    ma_variant_t *next_run_time_var = NULL;
                    if(MA_OK == (err =  ma_variant_create_from_raw(&next_run_time, sizeof(ma_task_time_t), &next_run_time_var))) {
                        err = ma_table_add_entry(task_props, MA_TASK_ATTR_NEXT_RUN_TIME, next_run_time_var);
                        (void)ma_variant_release(next_run_time_var);
                    }
                }
            }

            {
                const char *creator_id = NULL;
                if((MA_OK == err) && (MA_OK == ma_task_get_creator_id(task, &creator_id))) {
                    ma_variant_t *creator_id_variant = NULL;

                    if(MA_OK == (err =  ma_variant_create_from_string(creator_id, strlen(creator_id), &creator_id_variant))) {
                        err = ma_table_add_entry(task_props, MA_TASK_ATTR_CREATOR, creator_id_variant);
                        (void)ma_variant_release(creator_id_variant);
                    }
                }
            }


            if(MA_OK == err) {
                err = ma_variant_create_from_table(task_props, task_variant);
            }

            (void)ma_table_release(task_props);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t convert_variant_to_task(ma_variant_t *task_details, ma_task_t *task) {
    if(task_details && task) {
        ma_error_t err = MA_OK;
        ma_vartype_t variant_type = MA_VARTYPE_NULL;

        if(MA_OK == (err = ma_variant_get_type(task_details, &variant_type))) {
            err = MA_ERROR_PRECONDITION;
			if(MA_OK == (err = ma_variant_is_type(task_details, MA_VARTYPE_TABLE))) {
                ma_table_t *task_prop = NULL;
                if(MA_OK == (err = ma_variant_get_table(task_details, &task_prop))) {


                    {
                        ma_variant_t *trigger_list_variant = NULL;
                        if((MA_OK == err) && (MA_OK == (err = ma_table_get_value(task_prop, MA_TASK_ATTR_TRIGGER_LIST, &trigger_list_variant)))) {
                            ma_array_t *trigger_set = NULL;
                            if(MA_OK == (err = ma_variant_get_array(trigger_list_variant, &trigger_set))) {
                                size_t trigger_set_size = 0;
                                if(MA_OK == (err = ma_array_size(trigger_set, &trigger_set_size))) {
                                    //err = MA_ERROR_PRECONDITION;
                                    if(trigger_set_size) {
                                        size_t itr = 0;
                                        err = MA_OK;
                                        for(itr = 1; ((MA_OK == err) && (itr <= trigger_set_size)); ++itr) {
                                            ma_variant_t *t_ele = NULL;
                                            ma_trigger_t *trigger = NULL;
                                            if(MA_OK == (err = ma_array_get_element_at(trigger_set, itr, &t_ele))) {
                                                ma_vartype_t type = MA_VARTYPE_NULL;
                                                if(MA_OK == (err = ma_variant_get_type(t_ele, &type))) {
                                                    if(MA_OK == (err = ma_variant_is_type(t_ele, MA_VARTYPE_TABLE))) {
                                                        ma_table_t *trigger_prop = NULL;
                                                        if(MA_OK == (err = ma_variant_get_table(t_ele, &trigger_prop))) {
                                                            ma_uint32_t trigger_type = 0;

                                                            {
                                                                ma_variant_t *type_variant = NULL;
                                                                if(MA_OK == (err = ma_table_get_value(trigger_prop, MA_TRIGGER_ATTR_TYPE, &type_variant))) {
                                                                    err = ma_variant_get_uint32(type_variant, &trigger_type);
                                                                    (void)ma_variant_release(type_variant);
                                                                }
                                                            }

                                                            {
                                                                ma_variant_t *policy_variant = NULL;
                                                                if((MA_OK == err) && (MA_OK == (err = ma_table_get_value(trigger_prop, MA_TRIGGER_ATTR_POLICY, &policy_variant)))) {
                                                                    ma_buffer_t *buffer = NULL;

                                                                    if(MA_OK == (err = ma_variant_get_raw_buffer(policy_variant, &buffer))) {
                                                                        const unsigned char *policy_buf = NULL;
                                                                        size_t size = 0;

                                                                        if(MA_OK == (err = ma_buffer_get_raw(buffer, &policy_buf, &size))) {
                                                                            switch((ma_trigger_type_t)trigger_type) {
                                                                                case MA_TRIGGER_NOW: {
                                                                                    ma_trigger_run_now_policy_t policy = {0};
                                                                                    if(size == sizeof(ma_trigger_run_now_policy_t)) {
                                                                                        memcpy(&policy, policy_buf, sizeof(ma_trigger_run_now_policy_t));
                                                                                        err = ma_trigger_create_run_now(&policy, &trigger);
                                                                                    }
                                                                                    break;
                                                                                }
                                                                                case MA_TRIGGER_ONCE: {
                                                                                    ma_trigger_run_once_policy_t policy = {0};
                                                                                    if(size == sizeof(ma_trigger_run_once_policy_t)) {
                                                                                        memcpy(&policy, policy_buf, sizeof(ma_trigger_run_once_policy_t));
                                                                                        err = ma_trigger_create_run_once(&policy, &trigger);
                                                                                    }
                                                                                    break;
                                                                                }
                                                                                case MA_TRIGGER_DAILY: {
                                                                                    ma_trigger_daily_policy_t policy = {0};
                                                                                    if(size == sizeof(ma_trigger_daily_policy_t)) {
                                                                                        memcpy(&policy, policy_buf, sizeof(ma_trigger_daily_policy_t));
                                                                                        err = ma_trigger_create_daily(&policy, &trigger);
                                                                                    }
                                                                                    break;
                                                                                }
                                                                                case MA_TRIGGER_WEEKLY: {
                                                                                    ma_trigger_weekly_policy_t policy = {0};
                                                                                    if(size == sizeof(ma_trigger_weekly_policy_t)) {
                                                                                        memcpy(&policy, policy_buf, sizeof(ma_trigger_weekly_policy_t));
                                                                                        err = ma_trigger_create_weekly(&policy, &trigger);
                                                                                    }
                                                                                    break;
                                                                                }
                                                                                case MA_TRIGGER_MONTHLY_DATE: {
                                                                                    ma_trigger_monthly_date_policy_t policy = {0};
                                                                                    if(size == sizeof(ma_trigger_monthly_date_policy_t)) {
                                                                                        memcpy(&policy, policy_buf, sizeof(ma_trigger_monthly_date_policy_t));
                                                                                        err = ma_trigger_create_monthly_date(&policy, &trigger);
                                                                                    }
                                                                                    break;
                                                                                }
                                                                                case MA_TRIGGER_MONTHLY_DOW: {
                                                                                    ma_trigger_monthly_dow_policy_t policy = {FIRST_WEEK,SUNDAY,0};
                                                                                    if(size == sizeof(ma_trigger_monthly_dow_policy_t)) {
                                                                                        memcpy(&policy, policy_buf, sizeof(ma_trigger_monthly_dow_policy_t));
                                                                                        err = ma_trigger_create_monthly_dow(&policy, &trigger);
                                                                                    }
                                                                                    break;
                                                                                }
                                                                                case MA_TRIGGER_ON_SYSTEM_IDLE: {
                                                                                    ma_trigger_system_idle_policy_t policy = {0};
                                                                                    if(size == sizeof(ma_trigger_system_idle_policy_t)) {
                                                                                        memcpy(&policy, policy_buf, sizeof(ma_trigger_system_idle_policy_t));
                                                                                        err = ma_trigger_create_system_idle(&policy, &trigger);
                                                                                    }
                                                                                    break;
                                                                                }
                                                                                case MA_TRIGGER_ON_DIALUP: {
                                                                                    ma_trigger_dialup_policy_t policy = {0};
                                                                                    if(size == sizeof(ma_trigger_dialup_policy_t)) {
                                                                                        memcpy(&policy, policy_buf, sizeof(ma_trigger_dialup_policy_t));
                                                                                        err = ma_trigger_create_dialup(&policy, &trigger);
                                                                                    }
                                                                                    break;
                                                                                }
                                                                                case MA_TRIGGER_AT_SYSTEM_START: {
                                                                                    ma_trigger_systemstart_policy_t policy = {0};
                                                                                    if(size == sizeof(ma_trigger_systemstart_policy_t)) {
                                                                                        memcpy(&policy, policy_buf, sizeof(ma_trigger_systemstart_policy_t));
                                                                                        err = ma_trigger_create_system_start(&policy, &trigger);
                                                                                    }
                                                                                    break;
                                                                                }
                                                                                case MA_TRIGGER_AT_LOGON: {
                                                                                    ma_trigger_logon_policy_t policy = {0};
                                                                                    if(size == sizeof(ma_trigger_logon_policy_t)) {
                                                                                        memcpy(&policy, policy_buf, sizeof(ma_trigger_logon_policy_t));
                                                                                        err = ma_trigger_create_logon(&policy, &trigger);
                                                                                    }
                                                                                    break;
                                                                                }
                                                                            } //end of switch
                                                                        }
                                                                        (void)ma_buffer_release(buffer);
                                                                    }
                                                                    (void)ma_variant_release(policy_variant), policy_variant = NULL;
                                                                }
                                                            }

                                                            {
                                                                ma_variant_t *begin_date_variant = NULL;
                                                                if((MA_OK == err) && MA_OK == (err = ma_table_get_value(trigger_prop, MA_TRIGGER_ATTR_BEGIN_DATE, &begin_date_variant))) {
                                                                    ma_buffer_t *buffer = NULL;

                                                                    if(MA_OK == (err = ma_variant_get_raw_buffer(begin_date_variant, &buffer))) {
                                                                        const unsigned char *date_buf = NULL;
                                                                        size_t size = 0;

                                                                        if(MA_OK == (err = ma_buffer_get_raw(buffer, &date_buf, &size))) {
                                                                            if(size == sizeof(ma_task_time_t)) {
                                                                                ma_task_time_t begin_date = {0};
                                                                                memcpy(&begin_date, date_buf, sizeof(ma_task_time_t));
                                                                                (void)ma_trigger_set_begin_date(trigger, &begin_date);
                                                                            }
                                                                        }
                                                                        (void)ma_buffer_release(buffer);
                                                                    }

                                                                    (void)ma_variant_release(begin_date_variant), begin_date_variant = NULL;
                                                                }
                                                            }

                                                            {
                                                                ma_variant_t *end_date_variant = NULL;
                                                                if((MA_OK == err) && MA_OK == (err = ma_table_get_value(trigger_prop, MA_TRIGGER_ATTR_END_DATE, &end_date_variant))) {
                                                                    ma_buffer_t *buffer = NULL;

                                                                    if(MA_OK == (err = ma_variant_get_raw_buffer(end_date_variant, &buffer))) {
                                                                        const unsigned char *date_buf = NULL;
                                                                        size_t size = 0;

                                                                        if(MA_OK == (err = ma_buffer_get_raw(buffer, &date_buf, &size))) {
                                                                            if(size == sizeof(ma_task_time_t)) {
                                                                                ma_task_time_t end_date = {0};
                                                                                memcpy(&end_date, date_buf, sizeof(ma_task_time_t));
                                                                                (void)ma_trigger_set_end_date(trigger, &end_date);
                                                                            }
                                                                        }
                                                                        (void)ma_buffer_release(buffer);
                                                                    }

                                                                    (void)ma_variant_release(end_date_variant), end_date_variant = NULL;
                                                                }
                                                            }

                                                            {
                                                                ma_variant_t *next_fire_date_variant = NULL;
                                                                if((MA_OK == err) && MA_OK == ma_table_get_value(trigger_prop, MA_TRIGGER_ATTR_NEXT_FIRE_DATE, &next_fire_date_variant)) {
                                                                    ma_buffer_t *buffer = NULL;

                                                                    if(MA_OK == (err = ma_variant_get_raw_buffer(next_fire_date_variant, &buffer))) {
                                                                        const unsigned char *date_buf = NULL;
                                                                        size_t size = 0;

                                                                        if(MA_OK == (err = ma_buffer_get_raw(buffer, &date_buf, &size))) {
                                                                            if(size == sizeof(ma_task_time_t)) {
                                                                                ma_task_time_t nex_fire_date = {0};

                                                                                memcpy(&nex_fire_date, date_buf, sizeof(ma_task_time_t));
                                                                                (void)ma_trigger_set_next_fire_date(trigger, nex_fire_date);
                                                                            }
                                                                        }
                                                                        (void)ma_buffer_release(buffer);
                                                                    }

                                                                    (void)ma_variant_release(next_fire_date_variant);
                                                                }
                                                            }

                                                            {
                                                                ma_variant_t *last_fire_date_variant = NULL;
                                                                if((MA_OK == err) && MA_OK == ma_table_get_value(trigger_prop, MA_TRIGGER_ATTR_LAST_FIRE_DATE, &last_fire_date_variant)) {
                                                                    ma_buffer_t *buffer = NULL;

                                                                    if(MA_OK == (err = ma_variant_get_raw_buffer(last_fire_date_variant, &buffer))) {
                                                                        const unsigned char *date_buf = NULL;
                                                                        size_t size = 0;

                                                                        if(MA_OK == (err = ma_buffer_get_raw(buffer, &date_buf, &size))) {
                                                                            if(size == sizeof(ma_task_time_t)) {
                                                                                ma_task_time_t last_fire_date = {0};

                                                                                memcpy(&last_fire_date, date_buf, sizeof(ma_task_time_t));
                                                                                (void)ma_trigger_set_last_fire_date(trigger, last_fire_date);
                                                                            }
                                                                        }
                                                                        (void)ma_buffer_release(buffer);
                                                                    }

                                                                    (void)ma_variant_release(last_fire_date_variant);
                                                                }
                                                            }

                                                            {
                                                                ma_variant_t *state_variant = NULL;
                                                                if((MA_OK == err) && MA_OK == (err = ma_table_get_value(trigger_prop, MA_TRIGGER_ATTR_STATE, &state_variant))) {
                                                                    ma_uint32_t state_value = 0;

                                                                    if(MA_OK == (err = ma_variant_get_uint32(state_variant, &state_value))) {
                                                                        err = ma_trigger_set_state(trigger, (ma_trigger_state_t)state_value);
                                                                    }
                                                                    (void)ma_variant_release(state_variant);
                                                                }
                                                            }

                                                            (void)ma_table_release(trigger_prop);
                                                        }
                                                    } else {
                                                        if(MA_OK == err) {
                                                            err = MA_ERROR_PRECONDITION;
                                                        }
                                                        break;
                                                    }
                                                }
                                                (void)ma_variant_release(t_ele), t_ele = NULL;
                                            }
                                            if(MA_OK == err) {
                                                if(MA_OK == (err = ma_task_add_trigger(task, trigger))) {
                                                    (void)ma_trigger_release(trigger);
                                                }
                                            }
                                            trigger = NULL;
                                        } //end of for
                                    }
                                }
                                (void)ma_array_release(trigger_set);
                            }
                            (void)ma_variant_release(trigger_list_variant); trigger_list_variant = NULL;
                        }
                    } //end of trigger list

                    {
                        ma_variant_t *app_variant = NULL;
                        if((MA_OK == err) && MA_OK == ma_table_get_value(task_prop, MA_TASK_ATTR_APP_PAYLOAD, &app_variant)) {
                            err = ma_task_set_app_payload(task, app_variant);
                            (void)ma_variant_release(app_variant); app_variant = NULL;
                        }
                    }

                    {
                        ma_variant_t *id_var = NULL;
                        if((MA_OK == err) && (MA_OK == (err = ma_table_get_value(task_prop, MA_TASK_ATTR_ID, &id_var)))) {
                            ma_buffer_t *id_buffer = NULL;
                            if(MA_OK == (err = ma_variant_get_string_buffer(id_var, &id_buffer))) {
                                const char *id = NULL;
                                size_t size = 0;
                                if(MA_OK == (err = ma_buffer_get_string(id_buffer, &id, &size))) {
                                    err = ma_task_set_id(task, id);
                                }
                                (void)ma_buffer_release(id_buffer);
                            }
                            (void)ma_variant_release(id_var);
                        }
                    }

                    {
                        ma_variant_t *zone_var = NULL;
                        if((MA_OK == err) && (MA_OK == (err = ma_table_get_value(task_prop, MA_TASK_ATTR_TIME_ZONE, &zone_var)))) {
                            ma_time_zone_t zone = MA_TIME_ZONE_LOCAL;
                            if(MA_OK == (err = ma_variant_get_uint32(zone_var, (ma_uint32_t*)&zone))) {
                                err = ma_task_set_time_zone(task, zone);
                            }
                            (void)ma_variant_release(zone_var);
                        }
                    }

                    {
                        ma_variant_t *missed_var = NULL;
                        if((MA_OK == err) && (MA_OK == ma_table_get_value(task_prop, MA_TASK_ATTR_MISSED, &missed_var))) {
                            ma_uint32_t missed = MA_FALSE;

                            if(MA_OK == (err = ma_variant_get_uint32(missed_var, &missed))) {
                                err = ma_task_set_missed(task, (ma_bool_t)missed);
                            }
                            (void)ma_variant_release(missed_var);
                        }
                    }

                    {
                        ma_variant_t *name_variant = NULL;
                        if((MA_OK == err) && (MA_OK == (err = ma_table_get_value(task_prop, MA_TASK_ATTR_NAME, &name_variant)))) {
                            ma_buffer_t *name_buffer = NULL;
                            if(MA_OK == (err = ma_variant_get_string_buffer(name_variant, &name_buffer))) {
                                const char *name = NULL;
                                size_t size = 0;
                                if(MA_OK == (err = ma_buffer_get_string(name_buffer, &name, &size))) {
                                    err = ma_task_set_name(task, name);
                                }
                                (void)ma_buffer_release(name_buffer);
                            }
                            (void)ma_variant_release(name_variant); name_variant = NULL;
                        }
                    }

                    {
                        ma_variant_t *type_variant = NULL;
                        if((MA_OK == err) && (MA_OK == (err = ma_table_get_value(task_prop, MA_TASK_ATTR_TYPE, &type_variant)))) {
                            ma_buffer_t *type_buffer = NULL;
                            if(MA_OK == (err = ma_variant_get_string_buffer(type_variant, &type_buffer))) {
                                const char *type = NULL;
                                size_t size = 0;
                                if(MA_OK == (err = ma_buffer_get_string(type_buffer, &type, &size))) {
                                    err = ma_task_set_type(task, type);
                                }
                                (void)ma_buffer_release(type_buffer);
                            }
                            (void)ma_variant_release(type_variant); type_variant = NULL;
                        }
                    }

                    {
                        ma_variant_t *cond_variant = NULL;
                        if((MA_OK == err) && (MA_OK == (err = ma_table_get_value(task_prop, MA_TASK_ATTR_CONDITIONS, &cond_variant)))) {
                            ma_uint32_t conditions = 0;
                            if(MA_OK == (err = ma_variant_get_uint32(cond_variant, &conditions))) {
                                err = ma_task_set_conditions(task, conditions);
                            }
                            (void)ma_variant_release(cond_variant); cond_variant = NULL;
                        }
                    }

                    {
                        ma_variant_t *retry_variant = NULL;
                        if((MA_OK == err) && (MA_OK == (err = ma_table_get_value(task_prop, MA_TASK_ATTR_RETRY_POLICY, &retry_variant)))) {
                            ma_buffer_t *buffer = NULL;
                            if(MA_OK == (err = ma_variant_get_raw_buffer(retry_variant, &buffer))) {
                                const unsigned char *policy_buf = NULL;
                                size_t size = 0;
                                if(MA_OK == (err = ma_buffer_get_raw(buffer, &policy_buf, &size))) {
                                    err = MA_ERROR_PRECONDITION;
                                    if(size == sizeof(ma_task_retry_policy_t)) {
                                        ma_task_retry_policy_t policy = {0};
                                        memcpy(&policy, policy_buf, sizeof(ma_task_retry_policy_t));
                                        err = ma_task_set_retry_policy(task, policy);
                                    }
                                }
                                (void)ma_buffer_release(buffer);
                            }
                            (void)ma_variant_release(retry_variant); retry_variant = NULL;
                        }
                    }

                    {
                        ma_variant_t *power_variant = NULL;
                        if((MA_OK == err) && (MA_OK == ma_table_get_value(task_prop, MA_TASK_ATTR_POWER_POLICY, &power_variant))) {
                            ma_buffer_t *buffer = NULL;

                            if(MA_OK == (err = ma_variant_get_raw_buffer(power_variant, &buffer))) {
                                const unsigned char *policy_buf = NULL;
                                size_t size = 0;
                                if(MA_OK == (err = ma_buffer_get_raw(buffer, &policy_buf, &size))) {
                                    if(size == sizeof(ma_task_power_policy_t)) {
                                        ma_task_power_policy_t policy = {0};

                                        memcpy(&policy, policy_buf, sizeof(ma_task_power_policy_t));
                                        err = ma_task_set_power_policy(task, policy);
                                    }
                                }
                                (void)ma_buffer_release(buffer);
                            }
                            (void)ma_variant_release(power_variant);
                        }
                    }

                    {
                        ma_variant_t *max_run_time_variant = NULL;
                        if((MA_OK == err) && (MA_OK == (err = ma_table_get_value(task_prop, MA_TASK_ATTR_MAX_RUN_TIME, &max_run_time_variant)))) {
                            ma_uint16_t max_run_time = 0;
                            if(MA_OK == (err = ma_variant_get_uint16(max_run_time_variant, &max_run_time))) {
                                err = ma_task_set_max_run_time(task, max_run_time);
                            }
                            (void)ma_variant_release(max_run_time_variant); max_run_time_variant = NULL;
                        }
                    }

                    {
                        ma_variant_t *missed_variant = NULL;
                        if((MA_OK == err) && (MA_OK == (err = ma_table_get_value(task_prop, MA_TASK_ATTR_MISSED_POLICY, &missed_variant)))) {
                            ma_buffer_t *buffer = NULL;
                            if(MA_OK == (err = ma_variant_get_raw_buffer(missed_variant, &buffer))) {
                                const unsigned char *policy_buf = NULL;
                                size_t size = 0;
                                if(MA_OK == (err = ma_buffer_get_raw(buffer, &policy_buf, &size))) {
                                    err = MA_ERROR_PRECONDITION;
                                    if(size == sizeof(ma_task_missed_policy_t)) {
                                        ma_task_missed_policy_t policy = {{0}};
                                        memcpy(&policy, policy_buf, sizeof(ma_task_missed_policy_t));
                                        err = ma_task_set_missed_policy(task, policy);
                                    }
                                }
                                (void)ma_buffer_release(buffer);
                            }
                            (void)ma_variant_release(missed_variant);
                        }
                    }

                    {
                        ma_variant_t *randomize_var = NULL;
                        if((MA_OK == err) && (MA_OK == ma_table_get_value(task_prop, MA_TASK_ATTR_RANDOMIZE_POLICY, &randomize_var))) {
                            ma_buffer_t *buffer = NULL;
                            if(MA_OK == ma_variant_get_raw_buffer(randomize_var, &buffer)) {
                                const unsigned char *policy_buf = NULL;
                                size_t size = 0;
                                if(MA_OK == ma_buffer_get_raw(buffer, &policy_buf, &size)) {
                                    if(size == sizeof(ma_task_randomize_policy_t)) {
                                        ma_task_randomize_policy_t policy = {{0}};
                                        memcpy(&policy, policy_buf, sizeof(ma_task_missed_policy_t));
                                        (void)ma_task_set_randomize_policy(task, policy);
                                    }
                                }
                                (void)ma_buffer_release(buffer);
                            }
                            (void)ma_variant_release(randomize_var);
                        }
                    }

                    {
                        ma_variant_t *delay_var = NULL;
                        if((MA_OK == err) && (MA_OK == ma_table_get_value(task_prop, MA_TASK_ATTR_DELAY_POLICY, &delay_var))) {
                            ma_buffer_t *buffer = NULL;
                            if(MA_OK == ma_variant_get_raw_buffer(delay_var, &buffer)) {
                                const unsigned char *policy_buf = NULL;
                                size_t size = 0;
                                if(MA_OK == ma_buffer_get_raw(buffer, &policy_buf, &size)) {
                                    if(size == sizeof(ma_task_randomize_policy_t)) {
                                        ma_task_delay_policy_t policy = {{0}};
                                        memcpy(&policy, policy_buf, sizeof(ma_task_delay_policy_t));
                                        (void)ma_task_set_delay_policy(task, policy);
                                    }
                                }
                                (void)ma_buffer_release(buffer);
                            }
                            (void)ma_variant_release(delay_var);
                        }
                    }

                    {
                        ma_variant_t *repeat_variant = NULL;
                        if((MA_OK == err) && (MA_OK == (err = ma_table_get_value(task_prop, MA_TASK_ATTR_REPEAT_POLICY, &repeat_variant)))) {
                            ma_buffer_t *buffer = NULL;
                            if(MA_OK == (err = ma_variant_get_raw_buffer(repeat_variant, &buffer))) {
                                const unsigned char *policy_buf = NULL;
                                size_t size = 0;
                                if(MA_OK == (err = ma_buffer_get_raw(buffer, &policy_buf, &size))) {
                                    if(size == sizeof(ma_task_repeat_policy_t)) {
                                        ma_task_repeat_policy_t policy = {{0}};
                                        memcpy(&policy, policy_buf, sizeof(ma_task_repeat_policy_t));
                                        err = ma_task_set_repeat_policy(task, policy);
                                    }
                                }
                                (void)ma_buffer_release(buffer);
                            }
                            (void)ma_variant_release(repeat_variant); repeat_variant = NULL;
                        }
                    }

                    {
                        ma_variant_t *next_run_time_var = NULL;
                        if((MA_OK == err) && (MA_OK == (err = ma_table_get_value(task_prop, MA_TASK_ATTR_NEXT_RUN_TIME, &next_run_time_var)))) {
                            ma_buffer_t *buffer = NULL;
                            if(MA_OK == (err = ma_variant_get_raw_buffer(next_run_time_var, &buffer))) {
                                const unsigned char *time_buf = NULL;
                                ma_task_time_t next_run_time = {0};
                                size_t size = 0;
                                if(MA_OK == (err = ma_buffer_get_raw(buffer, &time_buf, &size))) {
                                    memcpy(&next_run_time, time_buf, sizeof(ma_task_time_t));
                                    ma_task_set_next_run_time(task, next_run_time);
                                }
                                (void)ma_buffer_release(buffer);
                            }
                            (void)ma_variant_release(next_run_time_var);
                        }
                    }

                    {
                        ma_variant_t *last_run_time_var = NULL;
                        if((MA_OK == err) && (MA_OK == (err = ma_table_get_value(task_prop, MA_TASK_ATTR_LAST_RUN_TIME, &last_run_time_var)))) {
                            ma_buffer_t *buffer = NULL;
                            if(MA_OK == (err = ma_variant_get_raw_buffer(last_run_time_var, &buffer))) {
                                const unsigned char *time_buf = NULL;
                                ma_task_time_t last_run_time = {0};
                                size_t size = 0;
                                if(MA_OK == (err = ma_buffer_get_raw(buffer, &time_buf, &size))) {
                                    memcpy(&last_run_time, time_buf, sizeof(ma_task_time_t));
                                    ma_task_set_last_run_time(task, last_run_time);
                                }
                                (void)ma_buffer_release(buffer);
                            }
                            (void)ma_variant_release(last_run_time_var);
                        }
                    }

                    {
                        ma_variant_t *priority_variant = NULL;
                        if((MA_OK == err) && (MA_OK == (err = ma_table_get_value(task_prop, MA_TASK_ATTR_PRIORITY, &priority_variant)))) {
                            ma_uint32_t priority = 0;
                            if(MA_OK == (err = ma_variant_get_uint32(priority_variant, &priority))) {
                                err = ma_task_set_priority(task, (ma_task_priority_t)priority);
                            }
                            (void)ma_variant_release(priority_variant); priority_variant = NULL;
                        }
                    }

                    {
                        ma_variant_t *max_run_time_limit_variant = NULL;
                        if((MA_OK == err) && (MA_OK == (err = ma_table_get_value(task_prop, MA_TASK_ATTR_MAX_RUN_TIME_LIMIT, &max_run_time_limit_variant)))) {
                            ma_uint16_t max_run_time_limit = 0;
                            if((MA_OK == (err = ma_variant_get_uint16(max_run_time_limit_variant, &max_run_time_limit))) && max_run_time_limit) {
                                err = ma_task_set_max_run_time_limit(task, max_run_time_limit);
                            }
                            (void)ma_variant_release(max_run_time_limit_variant); max_run_time_limit_variant = NULL;
                        }
                    }

                    {
                        ma_variant_t *state_variant = NULL;
                        if((MA_OK == err) && (MA_OK == (err = ma_table_get_value(task_prop, MA_TASK_ATTR_STATE, &state_variant)))) {
                            ma_task_state_t state = MA_TASK_STATE_NOT_SCHEDULED;
                            if(MA_OK == (err = ma_variant_get_uint32(state_variant, (ma_uint32_t*)&state))) {
                                err = ma_task_set_state(task, state);
                            }
                            (void)ma_variant_release(state_variant);
                        }
                    }

                    {
                        ma_variant_t *status_variant = NULL;
                        if((MA_OK == err) && (MA_OK == (err = ma_table_get_value(task_prop, MA_TASK_ATTR_EXECUTION_STATUS, &status_variant)))) {
                            ma_task_exec_status_t status = MA_TASK_EXEC_NOT_STARTED;
                            if(MA_OK == (err = ma_variant_get_uint32(status_variant, (ma_uint32_t*)&status))) {
                                err = ma_task_set_execution_status(task, status);
                            }
                            (void)ma_variant_release(status_variant);
                        }
                    }

                    {
                        ma_variant_t *creator_id_var = NULL;
                        if((MA_OK == err) && (MA_OK == ma_table_get_value(task_prop, MA_TASK_ATTR_CREATOR, &creator_id_var))) {
                            ma_buffer_t *creator_buffer = NULL;
                            if(MA_OK == (err = ma_variant_get_string_buffer(creator_id_var, &creator_buffer))) {
                                const char *creator = NULL;
                                size_t size = 0;
                                if(MA_OK == (err = ma_buffer_get_string(creator_buffer, &creator, &size))) {
                                    err = ma_task_set_creator_id(task, creator);
                                }
                                (void)ma_buffer_release(creator_buffer);
                            }
                            (void)ma_variant_release(creator_id_var);
                        }
                    }

                    {
                        ma_variant_t *software_id_variant = NULL;
                        if((MA_OK == err) && (MA_OK == ma_table_get_value(task_prop, MA_TASK_ATTR_SOFTWARE_ID, &software_id_variant))) {
                            ma_buffer_t *software_id_buffer = NULL;
                            if(MA_OK == (err = ma_variant_get_string_buffer(software_id_variant, &software_id_buffer))) {
                                const char *software_id = NULL;
                                size_t size = 0;
                                if(MA_OK == (err = ma_buffer_get_string(software_id_buffer, &software_id, &size))) {
                                    err = ma_task_set_software_id(task, software_id);
                                }
                                (void)ma_buffer_release(software_id_buffer);
                            }
                            (void)ma_variant_release(software_id_variant);
                        }
                    }

                    ma_table_release(task_prop);
                }
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t set_settings(const char *section, const char *name, ma_variant_t *value, ma_variant_t **ret_variant) {
    if(section && name && value && ret_variant) {
        ma_error_t err = MA_OK;

        if(*ret_variant) {
            ma_table_t *sec_table = NULL;
            if(MA_OK == (err = ma_variant_get_table(*ret_variant, &sec_table))) {
                ma_variant_t *sec_var = NULL;
                if(MA_OK == (err = ma_table_get_value(sec_table, section, &sec_var))) {
                    ma_table_t *sections = NULL;
                    if(MA_OK == (err = ma_variant_get_table(sec_var, &sections))) {
                        err = ma_table_add_entry(sections, name, value);
                        (void)ma_table_release(sections);
                    }
                    (void)ma_variant_release(sec_var);
                } else {
                    ma_table_t *sections = NULL;
                    if(MA_OK == (err = ma_table_create(&sections))) {
                        if(MA_OK == (err = ma_table_add_entry(sections, name, value))) {
                            ma_variant_t *sec_var = NULL;
                            if(MA_OK == (err = ma_variant_create_from_table(sections, &sec_var))) {
                                if(MA_OK == (err = ma_table_add_entry(sec_table, section, sec_var))) {
                                    (void)ma_variant_release(sec_var);
                                }
                                (void)ma_table_release(sections);
                            }
                        }
                    }
                }
                (void)ma_table_release(sec_table);
            }
        } else {
            ma_table_t *sections = NULL;
            if(MA_OK == (err = ma_table_create(&sections))) {
                if(MA_OK == (err = ma_table_add_entry(sections, name, value))) {
                    ma_variant_t *sec_var = NULL;
                    if(MA_OK == (err = ma_variant_create_from_table(sections, &sec_var))) {
                        ma_table_t *sec_table = NULL;
                        if(MA_OK == (err = ma_table_create(&sec_table))) {
                            if(MA_OK == (err = ma_table_add_entry(sec_table, section, sec_var))) {
                                err = ma_variant_create_from_table(sec_table, ret_variant);
                                (void)ma_variant_release(sec_var);
                            }
                            (void)ma_table_release(sec_table);
                        }
                        (void)ma_table_release(sections);
                    }
                }
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t get_settings(ma_variant_t *collection, const char *section, const char *name, ma_variant_t **value) {
    if(section && name && value) {
        ma_error_t err = MA_OK;
        ma_table_t *sec_table =NULL;

        if(MA_OK == (err = ma_variant_get_table(collection, &sec_table))) {
            ma_variant_t *sec_var = NULL;
            if(MA_OK == (err = ma_table_get_value(sec_table, section, &sec_var))) {
                ma_table_t *sections = NULL;
                if(MA_OK == (err = ma_variant_get_table(sec_var, &sections))) {
                    err = ma_table_get_value(sections, name, value);
                    (void)ma_table_release(sections);
                }
                (void)ma_variant_release(sec_var);
            }
            (void)ma_table_release(sec_table);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

void convert_local_to_utc(ma_task_time_t local, ma_task_time_t *gm) {
    if(gm) {
        struct tm *gmcal = NULL;
        struct tm cal = {0};
        time_t tt = 0;
        ma_task_time_t now = {0};
        time_t t = time(NULL);
        struct tm *gtm = gmtime(&t);
        ma_task_time_t utc_now = {0};
        int64_t off_secs = 0;

        memset(gm, 0, sizeof(ma_task_time_t));
        utc_now.year = gtm->tm_year + 1900;
        utc_now.month = gtm->tm_mon + 1;
        utc_now.day = gtm->tm_mday;
        utc_now.hour = gtm->tm_hour;
        utc_now.minute = gtm->tm_min;
        utc_now.secs = gtm->tm_sec;
        get_localtime(&now);        
        compute_date_diff_seconds(now, utc_now, &off_secs);
        cal.tm_hour = local.hour;
        cal.tm_year = local.year-1900;
        cal.tm_mon = local.month-1;
        cal.tm_mday = local.day;
        cal.tm_min = local.minute;
        cal.tm_sec = local.secs;
        cal.tm_isdst = -1;
        tt = mktime(&cal) - off_secs;
        if((gmcal = localtime(&tt))) {
            gm->year = gmcal->tm_year + 1900;
            gm->month = gmcal->tm_mon + 1;
            gm->day = gmcal->tm_mday;
            gm->hour = gmcal->tm_hour;
            gm->minute = gmcal->tm_min;
            gm->secs = gmcal->tm_sec;
        }
    }
}

static time_t compute_gm_offset(struct tm *tm) {
    time_t utc = mktime(tm);
    time_t offset = mktime(gmtime(&utc));
    return (time_t)difftime(utc, offset);
}

void convert_utc_to_local(ma_task_time_t gm, ma_task_time_t *local) {
    if(local) {
        ma_task_time_t now = {0};
        time_t t = time(NULL);
        struct tm *gtm = gmtime(&t);
        ma_task_time_t utc_now = {0};
        int64_t off_secs = 0;
        
        utc_now.year = gtm->tm_year + 1900;
        utc_now.month = gtm->tm_mon + 1;
        utc_now.day = gtm->tm_mday;
        utc_now.hour = gtm->tm_hour;
        utc_now.minute = gtm->tm_min;
        utc_now.secs = gtm->tm_sec;
        get_localtime(&now);
        compute_date_diff_seconds(gm, utc_now, &off_secs);
        advance_date_by_seconds(off_secs, now, local);
    }
}

void adjust_date_to_utc_zone(ma_task_time_t *date) {
    if(date) {
        ma_task_time_t tmp = {0};
        ma_task_time_t utc = {0};
        ma_int64_t secs = 0;

        memcpy(&tmp, date, sizeof(ma_task_time_t));
        convert_local_to_utc(tmp, &utc);
        compute_timeout(&utc, &secs);
        if(secs < 0) {
            secs = -secs;
        }
        advance_date_by_seconds(secs, tmp, date);
    }
}

ma_int64_t ma_get_time_in_seconds(ma_task_interval_t time) {
    return (time.hour * MA_NO_OF_MINUTES_IN_HOUR * MA_NO_OF_SECONDS_IN_MINUTE) + (time.minute * MA_NO_OF_SECONDS_IN_MINUTE);
}

void ma_compute_until_duration(ma_task_time_t start_date, ma_task_interval_t until_time, ma_task_interval_t *duration) {
    if(duration) {
        ma_int64_t s_time_in_seconds = 0;
        ma_int64_t e_time_in_seconds = 0;
        ma_int64_t duration_ = 0;

        duration->hour = start_date.hour;
        duration->minute = start_date.minute;
        s_time_in_seconds = ma_get_time_in_seconds(*duration);
        e_time_in_seconds = ma_get_time_in_seconds(until_time);
        if(e_time_in_seconds < s_time_in_seconds) {
            // the time slots are split across dates
            duration_ = e_time_in_seconds + ( MA_MAX_SECONDS_IN_DAY - s_time_in_seconds);
        }
        else if(e_time_in_seconds > s_time_in_seconds){
            duration_ = e_time_in_seconds - s_time_in_seconds;
            // the time slot is running within the same day
        }
        else {
            duration_ = MA_MAX_SECONDS_IN_DAY;
        }	    
        duration_ = duration_ / MA_NO_OF_SECONDS_IN_MINUTE;
        duration->minute = duration_ % MA_NO_OF_MINUTES_IN_HOUR;
        duration_ = duration_ / MA_NO_OF_MINUTES_IN_HOUR;
        duration->hour = duration_;
    }
}


ma_bool_t ma_date_validator(int day, int month, int year) {
    if(month && day && year && (day >= 1 && day <= 31) && (month >= 1 && month <= 12)) {
        ma_bool_t validation = MA_TRUE;

        switch(month) {
            case 2: {
                if(ma_check_leap_year(year)) {/* We only care about leap years in February */
                    if(day > 29)
                        validation = MA_FALSE;
                } else {
                    if(day > 28)
                        validation = MA_FALSE;
                }
                break;
            }
            case 1: 
            case 3: 
            case 5: 
            case 7: 
            case 8: 
            case 10: 
            case 12: {
                if(day > 31)
                    validation = MA_FALSE;
                break;
            }
            case 4: 
            case 6: 
            case 9:
            case 11: {
                if(day > 30)
                    validation = MA_FALSE;
                break;
            }
            default: {/* the month is not between 1 and 12 */
                validation = MA_FALSE;
                break;
            }
        }

        return validation;
    }
    return MA_FALSE;
}

ma_bool_t ma_time_validator(int hour, int minute, int seconds) {
    return ((hour >= 0 && hour <= 24) && (minute >= 0 && minute < 60) && (seconds >= 0 && seconds < 60)) ? MA_TRUE : MA_FALSE;
}

int ma_month_of_year(int month) {
    switch(month) {
        case JANUARY:return 1;
        case FEBRUARY:return 2;
        case MARCH:return 3;
        case APRIL:return 4;
        case MAY:return 5;
        case JUNE:return 6;
        case JULY:return 7;
        case AUGUST:return 8;
        case SEPTEMBER:return 9;
        case OCTOBER:return 10;
        case NOVEMBER:return 11;
        case DECEMBER:return 12;
        default:return -1;
    }
    return -1;
}

ma_bool_t ma_check_leap_year(int year) {
    return year ? ((year % 100 != 0 && year % 4 == 0) || (year % 400 == 0)) ? MA_TRUE : MA_FALSE : MA_FALSE;
}

void ma_mktime(ma_task_time_t tdate, time_t *tt) {
    struct tm task_time = {0};

    task_time.tm_hour = tdate.hour;
    task_time.tm_year = tdate.year-1900;
    task_time.tm_mon = tdate.month-1;
    task_time.tm_mday = tdate.day;
    task_time.tm_min = tdate.minute;
    task_time.tm_sec = tdate.secs;
    task_time.tm_isdst = -1;
    *tt = mktime(&task_time);
}

void ma_convert_time_t_to_task_time(time_t tt, ma_task_time_t *new_date) {
    struct tm *next_date = NULL;
    
    memset(new_date, 0, sizeof(ma_task_time_t));
    if(tt && (next_date = localtime(&tt))) {
        new_date->year = next_date->tm_year+1900;
        new_date->month = next_date->tm_mon + 1;
        new_date->day = next_date->tm_mday; //!= day ? default_day : next_date->tm_mday;
        new_date->minute = next_date->tm_min;
        new_date->hour = next_date->tm_hour;
        new_date->secs = next_date->tm_sec;
    }
}
void ma_strtime(ma_task_time_t date, char *pdate) {
    if(pdate) {
        MA_MSC_SELECT(_snprintf, snprintf)(pdate, MA_MAX_LEN, "%hu-%hu-%hu:%hu:%hu:%hu", date.month, date.day, date.year, date.hour, date.minute, date.secs);
    }
}

void ma_strtime_to_task_time(const char *pdate, ma_task_time_t *date) {
    if(pdate && date) {
        memset(date, 0, sizeof(ma_task_time_t));
        sscanf(pdate, "%hu-%hu-%hu", &date->month, &date->day, &date->year); 
    }
}
