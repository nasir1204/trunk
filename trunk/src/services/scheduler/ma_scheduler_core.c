#include "ma/internal/services/scheduler/ma_scheduler_core.h"
#include "ma/internal/services/scheduler/ma_taskfolder.h"
#include "ma/scheduler/ma_enumerator.h"
#include "ma/internal/services/scheduler/ma_runningtask_collector.h"
#include "ma/datastore/ma_ds_database.h"
#include "ma/logger/ma_file_logger.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/defs/ma_scheduler_defs.h"
#include "ma/ma_msgbus.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef MA_WINDOWS
#include<Windows.h>
#else
#include<unistd.h>
#endif

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "scheduler"

ma_logger_t *logger = NULL;

MA_CPP(extern "C" {)
ma_error_t ma_task_set_scheduler(ma_task_t *task, ma_scheduler_t *scheduler);
ma_error_t ma_taskfolder_modify_task(ma_taskfolder_t *folder, ma_task_t *new_task);
ma_error_t ma_taskfolder_internal_destroy_task(ma_taskfolder_t *folder, ma_task_t *task);
ma_error_t ma_taskfolder_run_task_on_demand(ma_taskfolder_t *folder, ma_task_t *task);
ma_event_loop_t *ma_msgbus_get_event_loop(ma_msgbus_t *self);
MA_CPP(})

struct ma_scheduler_s {
    ma_scheduler_state_t state;
    ma_taskfolder_t *folder;
    ma_running_task_collector_t *rt;
    ma_msgbus_t *msgbus;
    uv_loop_t *loop;
};

ma_error_t ma_scheduler_create(ma_msgbus_t *msgbus, ma_ds_t *scheduler_datastore, const char *base_path, ma_scheduler_t **scheduler) {
    if(msgbus && scheduler_datastore && base_path && scheduler) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;

        if((*scheduler = (ma_scheduler_t*) calloc(1, sizeof(ma_scheduler_t)))) {
            ma_event_loop_t *event_loop = NULL;
            if((event_loop = ma_msgbus_get_event_loop(msgbus))) {
                if((MA_OK == (err = ma_event_loop_get_loop_impl(event_loop, &((*scheduler)->loop)))) && (*scheduler)->loop) {
                    if(MA_OK == (err = ma_taskfolder_create(*scheduler, (*scheduler)->loop, scheduler_datastore, base_path, &(*scheduler)->folder))) {
                        if(MA_OK == (err = ma_running_task_collector_create(&(*scheduler)->rt))) {
                            (*scheduler)->state = MA_SCHEDULER_STATE_NOTSTARTED;
                            (*scheduler)->msgbus = msgbus;
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler create successful");
                            MA_LOG(logger, MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "Scheduler");
                        }
                    }
                }
            }
        }
        if((MA_OK != err) && *scheduler) {
            MA_LOG(logger, MA_LOG_SEV_CRITICAL, "scheduler create task failed, last error(%d)", err);
            (void)ma_taskfolder_release((*scheduler)->folder);
            (void)ma_running_task_collector_release((*scheduler)->rt);
            free(*scheduler);
            *scheduler = NULL;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_scheduler_get_msgbus(ma_scheduler_t *scheduler, ma_msgbus_t **msgbus) {
    return(scheduler && msgbus && (*msgbus = scheduler->msgbus))?MA_OK:MA_ERROR_INVALIDARG;
}

ma_error_t ma_scheduler_set_logger(ma_logger_t *ma_logger) {
    return(ma_logger && (logger = ma_logger))?MA_OK:MA_ERROR_INVALIDARG;
}

ma_error_t ma_scheduler_release(ma_scheduler_t *scheduler) {
    if(scheduler) {
        ma_error_t err = MA_OK;
        if(MA_OK != (err = ma_taskfolder_release(scheduler->folder))) {
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler task folder release failed, last error(%d)", err);
        }
        if(MA_OK != (err = ma_running_task_collector_release(scheduler->rt))) {
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler running task collector release failed, last error(%d)", err);
        }
        MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler released");
        free(scheduler);

        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_scheduler_get_runnning_task_set(ma_scheduler_t *scheduler, ma_task_t ***set, size_t *no_of_tasks) {
    if(scheduler && set && no_of_tasks) {
        ma_error_t err = MA_OK;

        if((MA_OK == (err = ma_running_task_collector_size(scheduler->rt, no_of_tasks))) && *no_of_tasks) {
            ma_task_t **task_array = NULL;

            err = MA_ERROR_OUTOFMEMORY;
            if((task_array = (ma_task_t **)calloc(*no_of_tasks, sizeof(ma_task_t *)))) {
                ma_task_t *task = NULL;
                size_t i = 0;

                ma_enumerator_reset((ma_enumerator_t*)scheduler->rt);
                for(i = 0; ((MA_OK == (err = ma_enumerator_next((ma_enumerator_t*)scheduler->rt, &task))) && i < *no_of_tasks); ++i) {
                    task_array[i] =  task;
                }
                *set = task_array;
            }
        }
        if(MA_OK != err) {
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler getting running task set failed, errorcode(%d)", err);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_scheduler_get_task_set(ma_scheduler_t *scheduler, ma_task_t ***set, size_t *no_of_tasks) {
    return(scheduler && set && no_of_tasks)?ma_taskfolder_get_all_task_handle(scheduler->folder, set, no_of_tasks):MA_ERROR_INVALIDARG;
}

ma_error_t ma_scheduler_release_runnning_task_set(ma_task_t **set) {
    return ma_scheduler_release_task_set(set);
}

ma_error_t ma_scheduler_release_task_set(ma_task_t **set) {
    if(set) {
        free(set);
        MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler task set release successful");
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_scheduler_start(ma_scheduler_t *scheduler) {
    if(scheduler) {
        ma_error_t err = MA_OK;

        if((MA_SCHEDULER_STATE_NOTSTARTED == scheduler->state) || (MA_SCHEDULER_STATE_STOPPED == scheduler->state)) {
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler starting...");
            if(MA_OK == (err = ma_taskfolder_start(scheduler->folder))) {
                scheduler->state = MA_SCHEDULER_STATE_RUNNING;
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler start successful");
            } else
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler start failed, last error(%d)", err);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_scheduler_get_taskfolder(ma_scheduler_t *scheduler, ma_taskfolder_t **folder) {
    if(scheduler && folder) {
        *folder = scheduler->folder;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_scheduler_get_state(ma_scheduler_t *scheduler, ma_scheduler_state_t *state) {
    return (scheduler && state && (*state = scheduler->state))?MA_OK:MA_ERROR_INVALIDARG;
}

ma_error_t ma_scheduler_stop(ma_scheduler_t *scheduler) {
    if(scheduler) {
        ma_error_t err = MA_OK;

        if(MA_SCHEDULER_STATE_RUNNING == scheduler->state) {
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler stopping...");
            if(MA_OK == (err = ma_taskfolder_stop(scheduler->folder))) {
                scheduler->state = MA_SCHEDULER_STATE_STOPPED;
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler stop successful");
            } else {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler stop failed, last error(%d)", err);
            }
            MA_LOG(logger, MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "The Scheduler is shutting down");
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_scheduler_create_task(ma_scheduler_t *scheduler, const char *creator, const char *task_id, const char *task_name, const char *task_type, ma_task_t **task) {
    if(scheduler && creator && task_id && task_name && task_type && task) {
        ma_error_t err = MA_OK;

        if(MA_OK == (err = ma_task_create(task_id, task))) {
            if(MA_OK == (err = ma_taskfolder_add_task(scheduler->folder, *task))) {
                (void)ma_task_set_creator_id(*task, creator);
                (void)ma_task_set_name(*task, task_name);
                (void)ma_task_set_type(*task, task_type);
                (void)ma_task_set_scheduler(*task, scheduler);
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler task id(%s) added successfully", task_id);
            }
            (void)ma_task_release(*task);
        }
        if(MA_OK != err) {
            *task = NULL;
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler create task failed, last error(%d)", err);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_scheduler_release_task(ma_scheduler_t *scheduler, ma_task_t *task) {
    return(scheduler && task)?ma_taskfolder_destroy_task(scheduler->folder, task):MA_ERROR_INVALIDARG;
}

ma_error_t ma_scheduler_internal_release_task(ma_scheduler_t *scheduler, ma_task_t *task) {
    return(scheduler && task)?ma_taskfolder_internal_destroy_task(scheduler->folder, task):MA_ERROR_INVALIDARG;
}

ma_error_t ma_scheduler_get_running_task_collector_handle(ma_scheduler_t *scheduler, ma_running_task_collector_t **rt) {
    return(scheduler && rt && (*rt = scheduler->rt))?MA_OK:MA_ERROR_INVALIDARG;
}

/*changed API name form ma_scheduler_enumerate_task to ma_scheduler_enumerate_task_server to solve mac build failure, this is a temporary fix*/
ma_error_t ma_scheduler_enumerate_task_server(ma_scheduler_t *scheduler, const char *product_id, const char *creator_id, const char *task_type, ma_task_t ***task_set, size_t *no_of_tasks) {
    return(scheduler && task_set && no_of_tasks)?ma_taskfolder_enumerate_task(scheduler->folder, product_id, creator_id, task_type, task_set, no_of_tasks):MA_ERROR_INVALIDARG;
}

ma_error_t ma_scheduler_get_task_folder(ma_scheduler_t *scheduler, ma_taskfolder_t **folder) {
    if(scheduler && folder) {
        *folder = scheduler->folder;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}



ma_error_t ma_scheduler_add(ma_scheduler_t *scheduler, ma_task_t *task) {
    if(scheduler && task) {
        ma_error_t err = MA_OK;
        const char *task_id = NULL;

        (void)ma_task_get_id(task, &task_id);
        if(MA_OK == (err = ma_taskfolder_add_task(scheduler->folder, task))) {
            if(MA_OK == (err = ma_task_set_scheduler(task, scheduler))) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler add task id(%s) successful", task_id);
            }
        }
        if(MA_OK != err)
            MA_LOG(logger, MA_LOG_SEV_ERROR, "scheduler add task id(%s) failed, last error", task_id, err);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_scheduler_remove(ma_scheduler_t *scheduler, const char *task_id) {
    if(scheduler && task_id) {
        ma_error_t err = MA_OK;
        ma_task_t *task = NULL;

        if(MA_OK == (err = ma_scheduler_get_task_handle(scheduler, task_id, &task))) {
            (void)ma_task_release(task);
            if(MA_OK == (err = ma_taskfolder_destroy_task(scheduler->folder, task))) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler remove task id(%s) successful", task_id);
            }
        }
        if(MA_OK != err)
            MA_LOG(logger, MA_LOG_SEV_ERROR, "scheduler removing task id(%s) failed, last error", task_id, err);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

#define MA_SCHEDULER_CORE_ARG_EXPANDER(ret_type, function, signature, arg, cargs)\
    ret_type ma_scheduler_##function signature {\
        return scheduler ? ma_taskfolder_##function cargs : MA_ERROR_INVALIDARG;\
    }
    MA_SCHEDULER_CORE_ACTUAL_ARG_EXPANDER
#undef MA_SCHEDULER_CORE_ARG_EXPANDER
