#include "ma/internal/services/scheduler/ma_scheduler_service.h"
#include "ma/internal/utils/context/ma_context.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"
#include "ma/internal/services/ma_service.h"
#include "ma/ma_log.h"
#include "ma/ma_message.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/services/scheduler/ma_task_defs.h"
#include "ma/ma_variant.h"
#include "ma/scheduler/ma_task.h"
#include "ma/scheduler/ma_triggers.h"
#include "ma/internal/services/scheduler/ma_scheduler_core.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/services/scheduler/ma_task_internal.h"
#include "ma/sensor/ma_sensor_msg.h"
#include "ma/sensor/ma_sensor_utils.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

const char base_path[] = "/scheduler/";

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "scheduler service"

MA_CPP(extern "C" {)
ma_error_t ma_task_set_id(ma_task_t *task, const char *task_id);
ma_error_t ma_task_set_execution_status(ma_task_t *task, ma_task_exec_status_t status);
ma_error_t ma_task_start(ma_task_t *task);
ma_error_t ma_scheduler_run_task_on_demand(ma_scheduler_t *scheduler, ma_task_t *task);
ma_error_t ma_scheduler_internal_release_task(ma_scheduler_t *scheduler, ma_task_t *task);
MA_CPP(})

struct ma_scheduler_service_s {
    ma_service_t            service_base;
    ma_context_t            *context;
    ma_scheduler_t          *scheduler;
    ma_msgbus_server_t      *server;
     ma_logger_t *logger;
    ma_msgbus_subscriber_t  *subscriber;
};


static ma_error_t scheduler_subscriber_cb(const char *topic, ma_message_t *message, void *cb_data);
static ma_error_t ma_scheduler_service_cb(ma_msgbus_server_t *server, ma_message_t *msg, void *cb_data, ma_msgbus_client_request_t *c_request);

static ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint);
static ma_error_t service_start(ma_service_t *service);
static ma_error_t service_stop(ma_service_t *service);
static void service_release(ma_service_t *service);

static const ma_service_methods_t service_methods = {
    &service_configure,
    &service_start,
    &service_stop,
    &service_release
};

ma_error_t ma_scheduler_service_create(const char *service_name, ma_service_t **service) {
    if(service_name && service) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;

        if((*service = (ma_service_t*)calloc(1, sizeof(ma_scheduler_service_t)))) {
            ma_service_init(*service, &service_methods, service_name, service);
            return MA_OK;
        }
        return err ;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_scheduler_service_release(ma_scheduler_service_t *self) {
    if(self) {
        ma_error_t err = MA_OK;
        if(self->server && (MA_OK != (err = ma_msgbus_server_release(self->server)))) {
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "msgbus server release failed");
        }
        (void)ma_msgbus_subscriber_release(self->subscriber);
        (void)ma_scheduler_release(self->scheduler);
        free(self) ;
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_bool_t check_context_validity(ma_context_t *context){	
    /*Will see if we need this*/
    return MA_TRUE;
}

ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint) {
    if(service && context) {
        ma_error_t rc = MA_OK;
        if(MA_SERVICE_CONFIG_NEW == hint && check_context_validity(context)) {
            ma_configurator_t *configurator = MA_CONTEXT_GET_CONFIGURATOR(context);
            ma_scheduler_service_t *self = (ma_scheduler_service_t *)service;
            ma_msgbus_t *msgbus = MA_CONTEXT_GET_MSGBUS(context);

            self->context = context;
            self->logger = MA_CONTEXT_GET_LOGGER(context);

            if(MA_OK == (rc = ma_scheduler_create(msgbus, ma_configurator_get_scheduler_datastore(configurator), MA_SCHEDULER_BASE_PATH_STR, &self->scheduler))) {
                (void) ma_scheduler_set_logger(self->logger);
                if(MA_OK == (rc = ma_msgbus_subscriber_create(MA_CONTEXT_GET_MSGBUS(context), &self->subscriber))) {
                    (void)ma_msgbus_subscriber_setopt(self->subscriber, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                    (void)ma_msgbus_subscriber_setopt(self->subscriber, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
                    if(MA_OK == (rc = ma_msgbus_server_create(msgbus, MA_SCHEDULER_SERVICE_NAME_STR, &self->server))) {                        
                        (void)ma_msgbus_server_setopt(self->server, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
                        if(MA_OK == (rc = ma_msgbus_server_setopt(self->server, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC))) {
                            /*TODO - No need to add the scheduler object in the context if we can use the scheduler client API's */
                            ma_context_add_object_info(context, MA_OBJECT_SCHEDULER_NAME_STR, &self->scheduler);
                            MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_DEBUG, "scheduler service configuration successful");
                            return MA_OK;
                        }
                        ma_msgbus_server_release(self->server);
                    }
                    ma_msgbus_subscriber_release(self->subscriber);
                }
                ma_scheduler_release(self->scheduler);
            }         
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t service_start(ma_service_t *service) {
    if(service) {
        ma_error_t err = MA_OK;
        ma_scheduler_service_t *self = (ma_scheduler_service_t *)service ;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);
        
        if(MA_OK == (err = ma_scheduler_start(self->scheduler))) {
            if(MA_OK == (err = ma_msgbus_server_start(((ma_scheduler_service_t*)service)->server, ma_scheduler_service_cb, service))) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler service started successfully");
                if(MA_OK == (err = ma_msgbus_subscriber_register(((ma_scheduler_service_t*)service)->subscriber,  "ma.sensor.*", scheduler_subscriber_cb, service))) {
                    MA_LOG(((ma_scheduler_service_t*)service)->logger, MA_LOG_SEV_DEBUG, "scheduler subscriber registered successfully");
                    return MA_OK ;
                } 
                else 
                    MA_LOG(((ma_scheduler_service_t*)service)->logger, MA_LOG_SEV_ERROR, "scheduler service failed to subscribe topic, errocode(%d)", err);
                ma_msgbus_server_stop(((ma_scheduler_service_t*)service)->server);
            }
            else
                MA_LOG(logger, MA_LOG_SEV_CRITICAL, "scheduler service start failed, last error(%d)", err);
            ma_scheduler_stop(self->scheduler);
        }
        else
            MA_LOG(logger, MA_LOG_SEV_CRITICAL, "scheduler service start failed, last error(%d)", err);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_scheduler_service_get_version(ma_uint32_t *version) {
    if(version) {
        *version = 0x05020100; //TODO fetch version 
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_scheduler_service_start(ma_scheduler_service_t *service) {
    return service_start((ma_service_t*)service);
}

ma_error_t ma_scheduler_service_stop(ma_scheduler_service_t *service) {
    return service_stop((ma_service_t*)service);
}

ma_error_t service_stop(ma_service_t *service) {
    if(service) {
        ma_error_t err = MA_OK;
        ma_scheduler_service_t *self = (ma_scheduler_service_t *)service;

        if(MA_OK != (err = ma_msgbus_subscriber_unregister(((ma_scheduler_service_t*)service)->subscriber))) {
            MA_LOG(((ma_scheduler_service_t*)service)->logger, MA_LOG_SEV_ERROR, "scheduler service unregistering subscriber failed, last error(%d)", err);
        }

        if(MA_OK == (err = ma_msgbus_server_stop(self->server))) {
            if(MA_OK != (err = ma_scheduler_stop(self->scheduler))) 
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_CRITICAL, "scheduler core stop failed, last error(%d)", err);
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO, "scheduler service stopped successfully");
        }
        else
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_CRITICAL, "scheduler service stop failed, last error(%d)", err);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

void service_release(ma_service_t *service) {
    if(service) {
        ma_scheduler_service_release((ma_scheduler_service_t *)service) ;
        return ;
    }
    return ;
}



static ma_error_t task_add_handler(ma_scheduler_service_t *service, ma_message_t *task_req_msg, ma_message_t *task_rsp_msg) {
    if(service && task_req_msg && task_rsp_msg) {
        ma_error_t err = MA_OK;
        char *task_add_status = MA_TASK_REPLY_STATUS_FAILED;
        ma_variant_t *task_details = NULL;
        const char *task_id = NULL;
        const char *task_name = NULL;
        const char *task_type = NULL;
        ma_task_t *task = NULL;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(service->context);
        char error_code[MA_MAX_LEN] = {0};

        if(MA_OK == (err = ma_message_get_property(task_req_msg, MA_TASK_ID, &task_id)) && 
            MA_OK == (err = ma_message_get_property(task_req_msg, MA_TASK_NAME, &task_name)) && 
            MA_OK == (err = ma_message_get_property(task_req_msg, MA_TASK_TYPE, &task_type))) {
                if(MA_OK == (err = ma_message_set_property(task_rsp_msg, MA_TASK_ID, task_id))) {
                    if(MA_OK == (err = ma_message_get_payload(task_req_msg, &task_details))) {
                        ma_table_t *task_prop = NULL;
                        if(MA_OK == (err = ma_variant_get_table(task_details, &task_prop))) {
                            ma_variant_t *creator_id_variant = NULL;
                            if(MA_OK == (err = ma_table_get_value(task_prop, MA_TASK_ATTR_CREATOR, &creator_id_variant))) {
                                ma_buffer_t *creator_buffer = NULL;
                                if(MA_OK == (err = ma_variant_get_string_buffer(creator_id_variant, &creator_buffer))) {
                                    const char *creator = NULL;
                                    size_t size = 0; 
                                    if(MA_OK == (err = ma_buffer_get_string(creator_buffer, &creator, &size))) {
                                        if(MA_OK == (err = ma_scheduler_create_task(service->scheduler, creator, task_id, task_name, task_type, &task))) {
                                            if(MA_OK == (err = convert_variant_to_task(task_details, task))) {
                                                if(MA_OK == (err = ma_task_start(task))) {
                                                    task_add_status = MA_TASK_REPLY_STATUS_SUCCESS;
                                                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler add task id(%s) successful", task_id);
                                                } else {
                                                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler service add task id(%s): task start failed, last error(%d)", task_id, err);
                                                    if(MA_OK == (err = ma_scheduler_get_task_handle(service->scheduler, task_id, &task))) {
                                                        (void)ma_task_release(task);
                                                        (void)ma_task_set_condition(task, MA_TASK_COND_DISABLED);
                                                        /*
                                                        (void)ma_task_set_condition(task, MA_TASK_COND_DELETE_WHEN_DONE);
                                                        if(MA_OK != (err = ma_scheduler_release_task(service->scheduler, task))) {
                                                            MA_LOG(logger, MA_LOG_SEV_ERROR, "scheduler failed to remove task(%s), last error(%d)", task_id, err);
                                                        }*/
                                                    }
                                                }
                                            } else {
                                                MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler service add task id(%s): task conversion failed, last error(%d)", task_id, err);
                                                (void)ma_task_set_condition(task, MA_TASK_COND_DELETE_WHEN_DONE);
                                                (void)ma_task_set_condition(task, MA_TASK_COND_DISABLED);
                                                if(MA_OK != (err = ma_scheduler_release_task(service->scheduler, task))) {
                                                    MA_LOG(logger, MA_LOG_SEV_ERROR, "scheduler failed to remove task(%s), last error(%d)", task_id, err);
                                                }
                                            }
                                        }
                                    }
                                    (void)ma_buffer_release(creator_buffer);
                                }
                                (void)ma_variant_release(creator_id_variant);
                            }
                            (void)ma_table_release(task_prop);
                        }
                    }
                }
        }
        if(MA_OK != err)
            MA_LOG(logger, MA_LOG_SEV_CRITICAL, "scheduler add task failed, last error(%d)", err);
        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(task_rsp_msg, MA_ERROR_CODE, error_code);

        return ma_message_set_property(task_rsp_msg, MA_TASK_STATUS, task_add_status);
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t task_delete_handler(ma_scheduler_service_t *service, ma_message_t *task_req_msg, ma_message_t *task_rsp_msg) {
    if(service && task_req_msg && task_rsp_msg) {
        ma_error_t err = MA_OK;
        const char *task_id = NULL;
        const char *task_name = NULL;
        const char *task_type = NULL;
        char *task_deletion_status = MA_TASK_REPLY_STATUS_FAILED;
        ma_task_t *task = NULL;
        ma_variant_t *task_variant = NULL;
        char error_code[MA_MAX_LEN] = {0};


        if(MA_OK == (err = ma_message_get_property(task_req_msg, MA_TASK_ID, &task_id))) {
                task_deletion_status = MA_TASK_INVALID_ID;
                if(MA_OK == (err = ma_scheduler_get_task_handle(service->scheduler, task_id, &task))) {
                    const char *software_id = NULL;

                    if(MA_OK == (err = ma_task_get_software_id(task, &software_id))) {
                        (void)ma_task_get_name(task, &task_name);
                        (void)ma_task_get_type(task, &task_type);
                        if((MA_OK == (err = ma_message_set_property(task_rsp_msg, MA_TASK_ID, task_id))) &&
                            (MA_OK == (err = ma_message_set_property(task_rsp_msg, MA_TASK_NAME, task_name))) && 
                            (MA_OK == (err = ma_message_set_property(task_rsp_msg, MA_TASK_TYPE, task_type))) &&
                            (MA_OK == (err = convert_task_to_variant(task, &task_variant)))) {
                                if(task_variant) {
                                    (void)ma_message_set_payload(task_rsp_msg, task_variant);
                                    (void)ma_variant_release(task_variant);
                                }
                                task_deletion_status = MA_TASK_REPLY_STATUS_FAILED;
                                if(MA_OK == (err = ma_scheduler_release_task(service->scheduler, task))) {
                                    task_deletion_status = MA_TASK_REPLY_STATUS_SUCCESS;
                                    err = ma_message_set_payload(task_rsp_msg, task_variant);
                                }
                        }
                    }
                    (void)ma_task_release(task);
                }
        }
        if(MA_OK != err)
            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_CRITICAL, "scheduler delete task failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(task_rsp_msg, MA_ERROR_CODE, error_code);

        return ma_message_set_property(task_rsp_msg, MA_TASK_STATUS, task_deletion_status);
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t task_get_status_handler(ma_scheduler_service_t *service, ma_message_t *task_req_msg, ma_message_t *task_rsp_msg) {
    if(service && task_req_msg && task_rsp_msg) {
        ma_error_t err = MA_OK;
        const char *task_id = NULL;
        char *task_get_status = MA_TASK_INVALID_ID;
        ma_task_t *task = NULL;
        char error_code[MA_MAX_LEN] = {0};

        if(MA_OK == (err = ma_message_get_property(task_req_msg, MA_TASK_ID, &task_id))) {
                task_get_status = MA_TASK_INVALID_ID;
                if(MA_OK == (err = ma_message_set_property(task_rsp_msg, MA_TASK_ID, task_id))) {
                    if(MA_OK == (err = ma_scheduler_get_task_handle(service->scheduler, task_id, &task))) {
                        ma_task_exec_status_t status = MA_TASK_EXEC_NOT_STARTED;

                        task_get_status = MA_TASK_REPLY_STATUS_FAILED;
                        if(MA_OK == (err = ma_task_get_execution_status(task, &status))) {
                            task_get_status = (char*)ma_task_get_execution_status_str(task, status);
                            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "query task id(%s) status (%s) successful ", task_id, task_get_status);
                        }
                        (void)ma_task_release(task);
                    }
                }
        }
        if(MA_OK != err)
            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_CRITICAL, "scheduler task get status failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(task_rsp_msg, MA_ERROR_CODE, error_code);

        return ma_message_set_property(task_rsp_msg, MA_TASK_STATUS, task_get_status);
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t task_update_status_handler(ma_scheduler_service_t *service, ma_message_t *task_req_msg, ma_message_t *task_rsp_msg) {
    if(service && task_req_msg && task_rsp_msg) {
        ma_error_t err = MA_OK;
        const char *task_id = NULL;
        char *task_update_status = MA_TASK_INVALID_ID;
        const char *task_exec_status = NULL;
        ma_task_t *task = NULL;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(service->context);
        char error_code[MA_MAX_LEN] = {0};

        if(MA_OK == (err = ma_message_get_property(task_req_msg, MA_TASK_ID, &task_id))) {
                task_update_status = MA_TASK_INVALID_ID;
                if(MA_OK == (err = ma_message_set_property(task_rsp_msg, MA_TASK_ID, task_id))) {
                    if(MA_OK == (err = ma_scheduler_get_task_handle(service->scheduler, task_id, &task))) {
                        task_update_status = MA_TASK_REPLY_STATUS_FAILED;
                        if((MA_OK == (err = ma_message_get_property(task_req_msg, MA_TASK_STATUS, &task_exec_status))) && task_exec_status) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler task id(%s) status(%s) updating...", task_id, task_exec_status);
                            if(!strcmp(task_exec_status, MA_TASK_EXEC_FAILED_TOPIC)) {
                                MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler task id(%s) status(%s) updating...", task_id, task_exec_status);
                                if(MA_OK == (err = ma_task_set_execution_status(task,MA_TASK_EXEC_FAILED))) {
                                    task_update_status = MA_TASK_REPLY_STATUS_SUCCESS;
                                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler task id(%s) updated successfully (%s)", task_id, task_update_status);
                                } else {
                                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler task id(%s) status updation failed. last error(%d)", task_id, err);
                                }
                            } else if(!strcmp(task_exec_status, MA_TASK_EXEC_SUCCESS_TOPIC)) {
                                MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler task id(%s) status(%s) updating...", task_id, task_exec_status);
                                if(MA_OK == (err = ma_task_set_execution_status(task, MA_TASK_EXEC_SUCCESS))) {
                                    task_update_status = MA_TASK_REPLY_STATUS_SUCCESS;
                                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler task id(%s) updated successfully (%s)", task_id, task_update_status);
                                } else {
                                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler task id(%s) status updation failed. last error(%d)", task_id, err);
                                }
                            }  else if(!strcmp(task_exec_status, MA_TASK_EXEC_RUNNING_TOPIC)) {
                                MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler task id(%s) status(%s) updating...", task_id, task_exec_status);
                                if(MA_OK == (err = ma_task_set_execution_status(task, MA_TASK_EXEC_RUNNING))) {
                                    task_update_status = MA_TASK_REPLY_STATUS_SUCCESS;
                                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler task id(%s) updated successfully (%s)", task_id, task_update_status);
                                } else {
                                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler task id(%s) status updation failed. last error(%d)", task_id, err);
                                }
                            } else if(!strcmp(task_exec_status, MA_TASK_EXEC_NOT_STARTED_TOPIC)) {
                                MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler task id(%s) status(%s) updating...", task_id, task_exec_status);
                                if(MA_OK == (err = ma_task_set_execution_status(task, MA_TASK_EXEC_NOT_STARTED))) {
                                    task_update_status = MA_TASK_REPLY_STATUS_SUCCESS;
                                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler task id(%s) updated successfully (%s)", task_id, task_update_status);
                                } else {
                                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler task id(%s) status updation failed. last error(%d)", task_id, err);
                                }
                            } else if(!strcmp(task_exec_status, MA_TASK_EXEC_STOPPED_TOPIC)) {
                                MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler task id(%s) status(%s) updating...", task_id, task_exec_status);
                                if(MA_OK == (err = ma_task_set_execution_status(task, MA_TASK_EXEC_STOPPED))) {
                                    task_update_status = MA_TASK_REPLY_STATUS_SUCCESS;
                                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler task id(%s) updated successfully (%s)", task_id, task_update_status);
                                } else {
                                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler task id(%s) status updation failed. last error(%d)", task_id, err);
                                }
                            } else {
                                MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler task id(%s) status(%s) updating...", task_id, task_exec_status);
                                task_update_status = MA_TASK_REPLY_STATUS_FAILED;
                                MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler task id(%s) updated failed (%s)", task_id, task_update_status);
                            }
                        }
                        (void)ma_task_release(task);
                    }
                }
        }
        if(MA_OK != err)
            MA_LOG(logger, MA_LOG_SEV_CRITICAL, "scheduler update task status failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(task_rsp_msg, MA_ERROR_CODE, error_code);

        MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler task id(%s) reply status(%s)", task_id, task_update_status);
        return ma_message_set_property(task_rsp_msg, MA_TASK_STATUS, task_update_status);

    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t task_modify_handler(ma_scheduler_service_t *service, ma_message_t *task_req_msg, ma_message_t *task_rsp_msg) {
    if(service && task_req_msg && task_rsp_msg) {
        ma_error_t err = MA_OK;
        const char *task_id = NULL;
        const char *task_name = NULL;
        const char *task_type = NULL;
        char *task_modify_status = MA_TASK_INVALID_ID;
        ma_variant_t *task_details = NULL;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(service->context);
        char error_code[MA_MAX_LEN] = {0};

        if(MA_OK == (err = ma_message_get_property(task_req_msg, MA_TASK_ID, &task_id)) && 
            MA_OK == (err = ma_message_get_property(task_req_msg, MA_TASK_NAME, &task_name)) && 
            MA_OK == (err = ma_message_get_property(task_req_msg, MA_TASK_TYPE, &task_type))) {
                if(MA_OK == (err = ma_message_set_property(task_rsp_msg, MA_TASK_ID, task_id))) {
                    if(MA_OK == (err = ma_message_get_payload(task_req_msg, &task_details))) {
                        ma_table_t *task_prop = NULL;
                        if(MA_OK == (err = ma_variant_get_table(task_details, &task_prop))) {
                            ma_variant_t *creator_id_variant = NULL;
                            if(MA_OK == (err = ma_table_get_value(task_prop, MA_TASK_ATTR_CREATOR, &creator_id_variant))) {
                                ma_buffer_t *creator_buffer = NULL;
                                if(MA_OK == (err = ma_variant_get_string_buffer(creator_id_variant, &creator_buffer))) {
                                    const char *creator = NULL;
                                    size_t size = 0; 
                                    if(MA_OK == (err = ma_buffer_get_string(creator_buffer, &creator, &size))) {
                                        ma_task_t *task = NULL;
                                        if(MA_OK != (err = ma_scheduler_create_task(service->scheduler, creator, task_id, task_name, task_type, &task))) {
                                            ma_task_t *orig = NULL;
                                            if(MA_OK == (err = ma_scheduler_get_task_handle(service->scheduler, task_id, &orig))) {
                                                if(MA_OK == (err = ma_task_copy(orig, &task))) {
                                                    size_t size = 0;
                                                    size_t itr = 0;
                                                    ma_trigger_list_t *ptl = NULL;
                                                    ma_trigger_t *ptrigger = NULL;
                                                    const char *tid = NULL;

                                                    (void)ma_task_get_trigger_list(task, &ptl);
                                                    (void)ma_task_size(task, &size);
                                                    for(itr = 0; itr < size; ++itr) {
                                                        if(MA_OK == ma_trigger_list_at(ptl, itr, &ptrigger)) {
                                                            (void)ma_trigger_get_id(ptrigger, &tid);
                                                            ma_trigger_list_remove(ptl, tid);
                                                            (void)ma_trigger_release(ptrigger);
                                                        }
                                                    }
                                                } else
                                                    MA_LOG(logger, MA_LOG_SEV_ERROR, "task id(%s) copy failed. error(%d)", task_id, err);
                                                (void)ma_task_release(orig);
                                            }
                                        }
                                        if(MA_OK == err) {
                                            if(MA_OK == (err = convert_variant_to_task(task_details, task))) {
                                                if(MA_OK == (err = ma_scheduler_modify_task(service->scheduler, task))) {
                                                    task_modify_status = MA_TASK_REPLY_STATUS_SUCCESS;
                                                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) modified successfully", task_id);
                                                } else {
                                                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler service modify task(%s) request failed, last error(%d)", task_id, err);
                                                    if(MA_OK == (err = ma_scheduler_get_task_handle(service->scheduler, task_id, &task))) {
                                                        (void)ma_task_release(task);
                                                        (void)ma_task_set_condition(task, MA_TASK_COND_DISABLED);
                                                        /*(void)ma_task_set_condition(task, MA_TASK_COND_DELETE_WHEN_DONE);
                                                        if(MA_OK != (err = ma_scheduler_release_task(service->scheduler, task))) {
                                                            MA_LOG(logger, MA_LOG_SEV_ERROR, "scheduler service failed to modify task, last error(%d)", task_id, err);
                                                        }*/
                                                    }
                                                }
                                            } else {
                                                MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler service modify task id(%s): task conversion failed, last error(%d)", task_id, err);
                                                (void)ma_task_set_condition(task, MA_TASK_COND_DISABLED);
                                                /*
                                                (void)ma_task_set_condition(task, MA_TASK_COND_DELETE_WHEN_DONE);
                                                if(MA_OK != (err = ma_scheduler_release_task(service->scheduler, task))) {
                                                    MA_LOG(logger, MA_LOG_SEV_ERROR, "scheduler service failed to modify task(%s), last error(%d)", task_id, err);
                                                }*/
                                            }
                                        }
                                    }
                                    (void)ma_buffer_release(creator_buffer);
                                }
                                (void)ma_variant_release(creator_id_variant);
                            }
                            (void)ma_table_release(task_prop);
                        }
                    }
                }
        }
        if(MA_OK != err)
            MA_LOG(logger, MA_LOG_SEV_CRITICAL, "scheduler modify task request failed, last error(%d)", err);
        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(task_rsp_msg, MA_ERROR_CODE, error_code);

        return ma_message_set_property(task_rsp_msg, MA_TASK_STATUS, task_modify_status);
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t task_get_handler(ma_scheduler_service_t *service, ma_message_t *task_req_msg, ma_message_t *task_rsp_msg) {
    if(service && task_req_msg && task_rsp_msg) {
        ma_error_t err = MA_OK;
        const char *task_id = NULL;
        const char *task_get_status = MA_TASK_INVALID_ID;
        ma_task_t *task = NULL;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(service->context);
        char error_code[MA_MAX_LEN] = {0};

        if(MA_OK == (err = ma_message_get_property(task_req_msg, MA_TASK_ID, &task_id))) {
            if(MA_OK == (err = ma_message_set_property(task_rsp_msg, MA_TASK_ID, task_id))) {
                if(MA_OK == (err = ma_scheduler_get_task_handle(service->scheduler, task_id, &task))) {
                    ma_variant_t *task_variant = NULL;

                    if(MA_OK == (err = convert_task_to_variant(task, &task_variant))) {
                        if(MA_OK == (err = ma_message_set_payload(task_rsp_msg, task_variant))) {
                            task_get_status = MA_TASK_REPLY_STATUS_SUCCESS;
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler get task id(%s) successful", task_id);
                        }
                        (void)ma_variant_release(task_variant);
                    }
                    (void)ma_task_release(task);
                }
            }
        }
        if(MA_OK != err)
            MA_LOG(logger, MA_LOG_SEV_CRITICAL, "scheduler get task request failed, last error(%d)", err);
        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(task_rsp_msg, MA_ERROR_CODE, error_code);

        return ma_message_set_property(task_rsp_msg, MA_TASK_STATUS, task_get_status);;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t task_run_on_demand_handler(ma_scheduler_service_t *service, ma_message_t *task_req_msg, ma_message_t *task_rsp_msg) {
    if(service && task_req_msg && task_rsp_msg) {
        ma_error_t err = MA_OK;
        const char *task_id = NULL;
        const char *on_demand_run_status = MA_TASK_INVALID_ID;
        ma_task_t *task = NULL;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(service->context);
        char error_code[MA_MAX_LEN] = {0};

        if(MA_OK == (err = ma_message_get_property(task_req_msg, MA_TASK_ID, &task_id))) {
                if(MA_OK == (err = ma_message_set_property(task_rsp_msg, MA_TASK_ID, task_id))) {
                    if(MA_OK == (err = ma_scheduler_get_task_handle(service->scheduler, task_id, &task))) {
                        ma_variant_t *task_variant = NULL;

                        if(MA_OK == (err = convert_task_to_variant(task, &task_variant))) {
                            if(MA_OK == (err = ma_message_set_payload(task_rsp_msg, task_variant))) {
                                on_demand_run_status = MA_TASK_REPLY_STATUS_FAILED;
                                if(MA_OK == (err = ma_scheduler_run_task_on_demand(service->scheduler, task))) {
                                    on_demand_run_status = MA_TASK_REPLY_STATUS_SUCCESS;
                                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler task id(%s) run on demand successful", task_id);
                                }
                            }
                            (void)ma_variant_release(task_variant);
                        }
                        (void)ma_task_release(task);
                    }
                }
        }
        if(MA_OK != err)
            MA_LOG(logger, MA_LOG_SEV_CRITICAL, "scheduler run on demand task failed, last error(%d)", err);
        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(task_rsp_msg, MA_ERROR_CODE, error_code);

        return ma_message_set_property(task_rsp_msg, MA_TASK_STATUS, on_demand_run_status);;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t task_enumerate_handler(ma_scheduler_service_t *service, ma_message_t *task_req_msg, ma_message_t *task_rsp_msg) {
    if(service && task_req_msg && task_rsp_msg) {
        ma_error_t err = MA_OK;
        const char *product_id = NULL;
        const char *creator_id = NULL;
        const char *task_type = NULL;
        const char *enum_task_status = MA_TASK_INVALID_ID;
        ma_task_t **task_set = NULL;
        size_t no_of_tasks = 0;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(service->context);
        char error_code[MA_MAX_LEN] = {0};

        if((MA_OK == (err = ma_message_get_property(task_req_msg, MA_TASK_CREATOR_ID, &creator_id))) && 
           (MA_OK == (err = ma_message_get_property(task_req_msg, MA_TASK_PRODUCT_ID, &product_id))) && 
           (MA_OK == (err = ma_message_get_property(task_req_msg, MA_TASK_TYPE, &task_type)))) {
                enum_task_status = MA_TASK_REPLY_STATUS_FAILED;
                /*changed API name form ma_scheduler_enumerate_task to ma_scheduler_enumerate_task_server to solve mac build failure, this is a temporary fix*/
                if(MA_OK == (err = ma_scheduler_enumerate_task_server(service->scheduler, product_id, creator_id, task_type, &task_set, &no_of_tasks))) {
                    if(task_set) {
                        if(no_of_tasks) {
                            size_t itr = 0;
                            ma_array_t *task_array = NULL;
                            ma_variant_t *payload = NULL;

                            if(MA_OK == (err = ma_array_create(&task_array))) {
                                for(itr = 0; itr < no_of_tasks; ++itr) {
                                    ma_variant_t *task_variant = NULL;
                                    if(MA_OK == (err = convert_task_to_variant(task_set[itr], &task_variant))) {
                                        if(MA_OK != (err = ma_array_push(task_array, task_variant)))
                                            MA_LOG(logger, MA_LOG_SEV_ERROR, "scheduler failed to add task into task array, last error(%d)", err);
                                        (void)ma_variant_release(task_variant);
                                    }
                                }
                                if(MA_OK == (err = ma_variant_create_from_array(task_array, &payload))) {
                                    if(MA_OK == (err = ma_message_set_payload(task_rsp_msg, payload))) {
                                        enum_task_status = MA_TASK_REPLY_STATUS_SUCCESS;
                                        MA_LOG(logger, MA_LOG_SEV_DEBUG, "scheduler enumerate task(s) successful");
                                    }
                                    (void)ma_variant_release(payload);
                                }
                                (void)ma_array_release(task_array);
                            }
                        }
                        free(task_set);
                    }
                }
        }
        if(MA_OK != err)
            MA_LOG(logger, MA_LOG_SEV_CRITICAL, "scheduler enumerate task(s) failed) last error(%d)", err);
        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(task_rsp_msg, MA_ERROR_CODE, error_code);

        return ma_message_set_property(task_rsp_msg, MA_TASK_STATUS, enum_task_status);
    }
    return MA_ERROR_INVALIDARG;
}
static ma_error_t task_postpone_handler(ma_scheduler_service_t *service, ma_message_t *task_req_msg, ma_message_t *task_rsp_msg) {
    if(service && task_req_msg && task_rsp_msg) {
        ma_error_t err = MA_OK;
        const char *task_id = NULL;
        char *task_postpone_status = MA_TASK_INVALID_ID;
        const char *post_pone_buf = NULL;
        ma_task_t *task = NULL;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(service->context);
        char error_code[MA_MAX_LEN] = {0};

        if(MA_OK == (err = ma_message_get_property(task_req_msg, MA_TASK_ID, &task_id))) {
            task_postpone_status = MA_TASK_INVALID_ID;
            if(MA_OK == (err = ma_message_set_property(task_rsp_msg, MA_TASK_ID, task_id))) {
                if(MA_OK == (err = ma_scheduler_get_task_handle(service->scheduler, task_id, &task))) {
                    task_postpone_status = MA_TASK_REPLY_STATUS_FAILED;
                    if((MA_OK == (err = ma_message_get_property(task_req_msg, MA_TASK_POSTPONE_INTERVALS, &post_pone_buf))) && post_pone_buf) {
                        ma_task_interval_t interval = {0};

                        interval.minute = atoi(post_pone_buf);
                        MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) processing postpone intervals(%d)...", task_id, interval.minute);
                        task_postpone_status = MA_TASK_REPLY_STATUS_SUCCESS;
                        if(MA_OK != (err = ma_task_run_after(task, interval))) {
                            task_postpone_status = MA_TASK_REPLY_STATUS_FAILED;
                            MA_LOG(logger, MA_LOG_SEV_ERROR, "task id(%s) processing postpone intervals(%d) failed, last error(%d)", task_id, interval.minute, err);
                        }
                    }
                    (void)ma_task_release(task);
                }
            }
        }
        if(MA_OK != err) {
            MA_LOG(logger, MA_LOG_SEV_CRITICAL, "scheduler update task status failed, last error(%d)", err);
        }
        MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) reply status(%s)", task_id, task_postpone_status);
        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(task_rsp_msg, MA_ERROR_CODE, error_code);
        return ma_message_set_property(task_rsp_msg, MA_TASK_STATUS, task_postpone_status);

    }
    return MA_ERROR_INVALIDARG;
}
ma_error_t ma_scheduler_service_cb(ma_msgbus_server_t *server, ma_message_t *msg, void *cb_data, ma_msgbus_client_request_t *c_request) {
    if(server && msg && cb_data) {
        ma_error_t err = MA_OK;
        const char *msg_type = NULL;
        ma_message_t *response = NULL;
        ma_scheduler_service_t *service = (ma_scheduler_service_t*)cb_data;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(service->context);

        if(MA_OK == (err = ma_message_create(&response))) {
            if(MA_OK == (err = ma_message_get_property(msg, MA_SCHEDULER_SERVICE_MSG_TYPE, &msg_type))) {
                if(!strcmp(msg_type, MA_TASK_MSG_TYPE_ADD)) {
                    if(MA_OK == (err = task_add_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client add task request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_TASK_MSG_TYPE_DELETE)) {
                    if(MA_OK == (err = task_delete_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client delete task request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_TASK_MSG_TYPE_QUERY_STATUS)) {
                    if(MA_OK == (err = task_get_status_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client get status task request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_TASK_MSG_TYPE_MODIFY)) {
                    if(MA_OK == (err = task_modify_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client modify task request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_TASK_MSG_TYPE_UPDATE)) {
                    if(MA_OK == (err = task_update_status_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client update task request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_TASK_MSG_TYPE_GET)) {
                    if(MA_OK == (err = task_get_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client get task request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_TASK_MSG_TYPE_ON_DEMAND_RUN)) {
                    if(MA_OK == (err = task_run_on_demand_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client run task on demand request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_TASK_ENUMERATE)) {
                    if(MA_OK == (err = task_enumerate_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client run task on demand request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_TASK_MSG_TYPE_POSTPONE)) {
                    if(MA_OK == (err = task_postpone_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client task postpone request failed, last error(%d)", err);
                        }
                    }
                } else {
                    if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, MA_ERROR_SCHEDULER_INVALID_OP_TYPE, response))) {
                        MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client request failed, last error(%d)", err);
                    }
                }
            } else {
                if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err = MA_ERROR_SCHEDULER_TASK_TYPE_NOT_SET, response))) {
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client request failed, last error(%d)", err);
                }
            }
            if(MA_OK != ma_message_release(response)) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client request message release failed");
            }
        }
        return err;
    }
    return MA_OK;
}

ma_error_t scheduler_subscriber_cb(const char *topic, ma_message_t *msg, void *cb_data) {
    if(topic && msg && cb_data) {
        ma_error_t err = MA_OK;
        ma_scheduler_service_t *service = (ma_scheduler_service_t*)cb_data;
        ma_scheduler_t *scheduler = service->scheduler;

        MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "scheduler subscriber received sensor message(%s)", topic);
        if(!strcmp(topic, MA_SYSTEM_SENSOR_SYSTEM_STARTUP_PUB_TOPIC_STR)) {
            MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "scheduler processing (%s) message", topic);
            err = ma_scheduler_notify_event(service->scheduler, MA_TASK_EVENT_TYPE_SYSTEM_STARTUP);
        } else if(!strcmp(topic, MA_SENSOR_USER_CHANGED_PUB_TOPIC_STR)) {
            ma_variant_t *usr_msg_var = NULL;

            if(MA_OK == (err = ma_message_get_payload(msg, &usr_msg_var))) {
                ma_user_sensor_msg_t *usr_msg = NULL;

                if(MA_OK == (err = convert_variant_to_user_sensor_msg(usr_msg_var, &usr_msg))) {
                    ma_user_logged_state_t state = MA_USER_LOGGED_STATE_OFF;

                    (void)ma_user_sensor_msg_get_logon_state(usr_msg, &state);
                    if(MA_USER_LOGGED_STATE_ON == state) {
                        ma_bool_t startup_flag = MA_FALSE;

                        if(MA_OK == (err = ma_user_sensor_msg_get_startup_flag(usr_msg, &startup_flag))) {
                            if(!startup_flag) {
                                MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "scheduler processing (%s) message", topic);
                                err = ma_scheduler_notify_event(service->scheduler, MA_TASK_EVENT_TYPE_LOGON);
                            }
                        }
                    }
                    (void)ma_user_sensor_msg_release(usr_msg);
                }
                (void)ma_variant_release(usr_msg_var);
            }
        } else if(!strcmp(topic, MA_SYSTEM_SENSOR_TIME_CHANGE_EVENT_PUB_TOPIC_STR)) {
            MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "scheduler processing (%s) message", topic);
            (void)ma_scheduler_notify_event(scheduler, MA_TASK_EVENT_TYPE_SYSTEM_TIME_CHANGE);
            err = ma_scheduler_restart(scheduler);
        } else if(!strcmp(topic, MA_POWER_SENSOR_STATUS_CHANGE_PUB_TOPIC_STR) || !strcmp(topic, MA_POWER_SENSOR_BATTERY_LOW_PUB_TOPIC_STR)) {
            ma_variant_t *power_msg_var = NULL;
            
            MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "scheduler processing (%s) message", topic);
            if(MA_OK == (err = ma_message_get_payload(msg, &power_msg_var))) {
                ma_power_sensor_msg_t *power_msg = NULL;

                if(MA_OK == (err = convert_variant_to_power_sensor_msg(power_msg_var, &power_msg))) {
                    size_t size = 0;
                    
                    (void)ma_power_sensor_msg_get_size(power_msg, &size);
                    if(size) {
                        ma_power_status_t power_status = MA_POWER_STATUS_UNKNOWN;

                        if(MA_OK == (err = ma_power_sensor_msg_get_power_status(power_msg, size - 1, &power_status))) {
                            if(MA_POWER_STATUS_OFFLINE == power_status) {
                                unsigned long secs = 0;

                                (void)ma_power_sensor_msg_get_battery_lifetime(power_msg, size - 1, &secs);
                                if(secs) {
                                    ma_battery_charge_status_t status = MA_BATTERY_CHARGE_STATUS_CRITICAL;

                                    if(MA_OK == (err = ma_power_sensor_msg_get_batter_charge_status(power_msg, size -1, &status))) {
                                        if((MA_BATTERY_CHARGE_STATUS_CRITICAL == status) || (MA_BATTERY_CHARGE_STATUS_LOW == status)) {
                                            ma_scheduler_notify_event(service->scheduler, MA_TASK_COND_DONT_START_IF_ON_BATTERIES);
                                            ma_scheduler_notify_event(service->scheduler, MA_TASK_COND_STOP_IF_GOING_ON_BATTERIES);
                                        }
                                    }
                                }
                            } else if(MA_POWER_STATUS_ONLINE == power_status) {
                                /* TODO resume all power aware task(s) to enable state */
                            } else {
                                /* do nothing */
                            }
                        }
                    }
                    (void)ma_power_sensor_msg_release(power_msg);
                }
                (void)ma_variant_release(power_msg_var);
            }
        } else {
            /* do nothing */
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}


