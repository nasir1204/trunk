#include "ma/scheduler/ma_task.h"
#include "ma/internal/defs/ma_scheduler_defs.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"
#include "ma/scheduler/ma_triggerlist.h"
#include "ma/internal/ma_macros.h"
#include "ma/logger/ma_file_logger.h"
#include "ma/internal/services/scheduler/ma_task_defs.h"
#include "ma/internal/services/scheduler/ma_task_internal.h"
#include "ma/internal/services/scheduler/ma_triggers_internal.h"
#include "ma/internal/services/scheduler/ma_scheduler_core.h"
#include "ma/internal/services/scheduler/ma_trigger_core.h"
#include "ma/scheduler/ma_enumerator.h"
#include "ma/internal/services/scheduler/ma_scheduler_datastore.h"
#include "ma_scheduler_private.h"
#include "ma/internal/utils/threading/ma_atomic.h"
#include "ma/ma_message.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"

#include <string.h>
#include <stdlib.h>

extern ma_logger_t *logger;
static const char *gzone_ptr;

static const char *gtask_exec_status_str[] =  {
#define MA_TASK_EXEC_EXPANDER(status, index, status_str) status_str,
    MA_TASK_EXEC_STATUS_EXPANDER
#undef MA_TASK_EXEC_EXPANDER
};

MA_CPP(extern "C" {)
typedef struct ma_task_core_s ma_task_core_t;
//trigger op method(s)
ma_error_t ma_trigger_start(ma_trigger_t *trigger);
ma_error_t ma_trigger_stop(ma_trigger_t *trigger);
ma_error_t ma_trigger_set_state(ma_trigger_t *trigger, ma_trigger_state_t state);
ma_error_t ma_trigger_set_task_handle(ma_trigger_t *trigger, ma_task_t *task);
ma_error_t ma_trigger_get_next_run_time(ma_trigger_t *trigger, ma_task_time_t *next_run_time);
ma_error_t ma_trigger_set_until_date(ma_trigger_t *trigger, ma_task_time_t date);
ma_error_t ma_scheduler_get_taskfolder(ma_scheduler_t *scheduler, ma_taskfolder_t **folder);
ma_error_t ma_scheduler_internal_release_task(ma_scheduler_t *scheduler, ma_task_t *task);
ma_error_t ma_taskfolder_get_datastore(ma_taskfolder_t *folder, ma_ds_t **datastore);
ma_error_t ma_taskfolder_get_datastore_basepath(ma_taskfolder_t *folder, const char **ds_path);
ma_error_t ma_trigger_list_create(ma_trigger_list_t **tl);
ma_error_t ma_trigger_list_release(ma_trigger_list_t *tl);
ma_error_t ma_trigger_list_set_task(ma_trigger_list_t *tl, ma_task_t *task);
ma_error_t ma_trigger_set_id(ma_trigger_t *trigger, const char *id);
ma_error_t ma_task_get_scheduler(ma_task_t *task, ma_scheduler_t **scheduler);
ma_error_t ma_scheduler_get_msgbus(ma_scheduler_t *scheduler, ma_msgbus_t **msgbus);
MA_CPP(})

ma_error_t ma_task_get_scheduler(ma_task_t *task, ma_scheduler_t **scheduler);

static ma_bool_t is_all_triggers_state_same(ma_enumerator_t *enumerator, ma_trigger_state_t state);

static ma_error_t validate_run_after_time(ma_task_t *task, ma_task_interval_t intervals);
static ma_error_t core_task_apply_policies(ma_task_t *task);
static ma_error_t core_task_disable_events_on_trigger_dispose(ma_task_t *task, ma_trigger_t *trigger);
static ma_error_t core_task_destroy(ma_task_t *task);

static ma_error_t core_task_release(ma_task_t *task);
static ma_error_t core_task_set_settings(ma_task_t *task, const char *path , const char *name, ma_variant_t *value);
static ma_error_t core_task_set_retry_policy(ma_task_t *task, ma_task_retry_policy_t policy);
static ma_error_t core_task_set_app_payload(ma_task_t *task, ma_variant_t *app_payload);
static ma_error_t core_task_set_repeat_policy(ma_task_t *task, ma_task_repeat_policy_t policy);
static ma_error_t core_task_set_max_run_time(ma_task_t *task, ma_uint16_t max_run_time);
static ma_error_t core_task_set_max_run_time_limit(ma_task_t *task, ma_uint16_t max_run_time_limit);
static ma_error_t core_task_set_randomize_policy(ma_task_t *task, ma_task_randomize_policy_t policy);
static ma_error_t core_task_set_priority(ma_task_t *task, ma_task_priority_t priority);
static ma_error_t core_task_set_conditions(ma_task_t *task, ma_uint32_t conds);
static ma_error_t core_task_set_execution_status(ma_task_t *task, ma_task_exec_status_t status);
static ma_error_t core_task_set_delay_policy(ma_task_t *task, ma_task_delay_policy_t policy);
static ma_error_t core_task_set_missed_policy(ma_task_t *task, ma_task_missed_policy_t policy);
static ma_error_t core_task_set_creator_id(ma_task_t *task, const char *id);
static ma_error_t core_task_get_state(ma_task_t *task, ma_task_state_t *state);
static ma_error_t core_task_get_last_run_time(ma_task_t *task, ma_task_time_t *last_run_time);
static ma_error_t core_task_get_next_run_time(ma_task_t *task, ma_task_time_t *next_run_time);
static ma_error_t core_task_get_app_payload(ma_task_t *task, ma_variant_t **app_payload);
static ma_error_t core_task_get_missed_policy(ma_task_t *task, ma_task_missed_policy_t *policy);
static ma_error_t core_task_get_repeat_policy(ma_task_t *task, ma_task_repeat_policy_t *policy);
static ma_error_t core_task_get_priority(ma_task_t *task, ma_task_priority_t *priority);
static ma_error_t core_task_get_conditions(ma_task_t *task, ma_uint32_t *conds);
static ma_error_t core_task_get_id(ma_task_t *task, const char **task_id);
static ma_error_t core_task_get_name(ma_task_t *task, const char **task_name);
static ma_error_t core_task_get_type(ma_task_t *task, const char **task_type);
static ma_error_t core_task_get_creator_id(ma_task_t *task, const char **creator_id);
static ma_error_t core_task_get_retry_policy(ma_task_t *task, ma_task_retry_policy_t *retry_policy);
static ma_error_t core_task_get_max_run_time(ma_task_t *task, ma_uint16_t *max_run_time);
static ma_error_t core_task_get_max_run_time_limit(ma_task_t *task, ma_uint16_t *max_run_time_limit);
static ma_error_t core_task_get_execution_status(ma_task_t *task, ma_task_exec_status_t *status);
static ma_error_t core_task_add_trigger(ma_task_t *task, ma_trigger_t *trigger);
static ma_error_t core_task_remove_trigger(ma_task_t *task, const char *trigger_id);
static ma_error_t core_task_find_trigger(ma_task_t *task, const char *trigger_id, ma_trigger_t **trigger);
static ma_error_t core_task_set_name(ma_task_t *task, const char *name);
static ma_error_t core_task_set_type(ma_task_t *task, const char *type);
static ma_error_t core_task_set_software_id(ma_task_t *task, const char *id);
static ma_error_t core_task_get_software_id(ma_task_t *task, const char **id);
static ma_error_t core_task_get_settings(ma_task_t *task, const char *path , const char *name, ma_variant_t **value);
static ma_error_t core_task_set_state(ma_task_t *task, ma_task_state_t state);
static ma_error_t core_task_clear_setting(ma_task_t *task);
static ma_error_t core_task_set_next_run_time(ma_task_t *task, ma_task_time_t next_run_time);
static ma_error_t core_task_set_last_run_time(ma_task_t *task, ma_task_time_t last_run_time);
static ma_error_t core_task_set_time_zone(ma_task_t *task, ma_time_zone_t zone);
static ma_error_t core_task_get_time_zone(ma_task_t *task, ma_time_zone_t *zone);
static ma_error_t core_task_copy(const ma_task_t *task, ma_task_t **copy);
static ma_error_t core_task_set_id(ma_task_t *task, const char *task_id);
static ma_error_t core_task_get_trigger_list(ma_task_t *task, ma_trigger_list_t **tl);
static ma_error_t core_task_get_randomize_policy(ma_task_t *task, ma_task_randomize_policy_t *policy);
static ma_error_t core_task_get_delay_policy(ma_task_t *task, ma_task_delay_policy_t *policy);
static ma_error_t core_task_dispose_trigger(ma_task_t *task, ma_trigger_t *trigger);
static ma_error_t core_task_size(ma_task_t *task, size_t *no_of_triggers);
static ma_error_t core_task_add_ref(ma_task_t *task);
static ma_error_t core_task_unset_condition(ma_task_t *task, ma_task_cond_t cond);
static ma_error_t core_task_set_condition(ma_task_t *task, ma_task_cond_t cond);
static ma_error_t core_task_set_power_policy(ma_task_t *task, ma_task_power_policy_t policy);
static ma_error_t core_task_get_power_policy(ma_task_t *task, ma_task_power_policy_t *policy);
static ma_error_t core_task_set_missed(ma_task_t *task, ma_bool_t missed);
static ma_error_t core_task_get_missed(ma_task_t *task, ma_bool_t *missed);

const static ma_task_methods_t methods = {
    &core_task_release,
    &core_task_set_settings,
    &core_task_set_retry_policy,
    &core_task_set_app_payload,
    &core_task_set_repeat_policy,
    &core_task_set_max_run_time,
    &core_task_set_max_run_time_limit,
    &core_task_set_randomize_policy,
    &core_task_set_priority,
    &core_task_set_conditions,
    &core_task_set_execution_status,
    &core_task_set_delay_policy,
    &core_task_set_missed_policy,
    &core_task_set_creator_id,
    &core_task_get_state,
    &core_task_get_last_run_time,
    &core_task_get_next_run_time,
    &core_task_get_app_payload,
    &core_task_get_missed_policy,
    &core_task_get_repeat_policy,
    &core_task_get_priority,
    &core_task_get_conditions,
    &core_task_get_id,
    &core_task_get_name,
    &core_task_get_type,
    &core_task_get_creator_id,
    &core_task_get_retry_policy,
    &core_task_get_max_run_time,
    &core_task_get_max_run_time_limit,
    &core_task_get_execution_status,
    &core_task_add_trigger,
    &core_task_remove_trigger,
    &core_task_find_trigger,
    &core_task_set_name,
    &core_task_set_type,
    &core_task_set_software_id,
    &core_task_get_software_id,
    &core_task_get_settings,
    &core_task_set_state,
    &core_task_clear_setting,
    &core_task_set_next_run_time,
    &core_task_set_last_run_time,
    &core_task_set_time_zone,
    &core_task_get_time_zone,
    &core_task_copy,
    &core_task_set_id,
    &core_task_get_trigger_list,
    &core_task_get_randomize_policy,
    &core_task_get_delay_policy,
    &core_task_dispose_trigger,
    &core_task_size,
    &core_task_add_ref,
    &core_task_unset_condition,
    &core_task_set_condition,
    &core_task_set_power_policy,
    &core_task_get_power_policy,
    &core_task_set_missed,
    &core_task_get_missed,
};

struct ma_task_core_s {
    ma_task_t task_base;
    char id[MA_MAX_TASKID_LEN + 1];
    char name[MA_MAX_TASKID_LEN + 1];
    char type[MA_MAX_TASKID_LEN + 1];
    char creator_id[MA_MAX_CREATOR_ID_LEN_INT + 1];
    char software_id[MA_MAX_SOFTWARE_ID_LEN_INT + 1];
    uv_loop_t *loop;
    ma_task_state_t state;
    ma_uint32_t cond;
    ma_uint32_t events;
    ma_trigger_list_t *trigger_list;
    ma_trigger_list_t *dispose_list;
    ma_uint16_t retry_count; // error retry_count
    ma_uint16_t retry_interval;
    ma_task_exec_status_t exec_status; //task executor will update this state
    ma_task_repeat_policy_t repeat;
    ma_uint16_t max_run_time;//minutes
    ma_uint16_t max_run_time_limit;//minutes
    ma_uint16_t missed_interval;
    ma_task_time_t last_run_time;
    ma_task_time_t next_run_time;
    ma_task_priority_t priority;
    ma_variant_t *app_payload;
    ma_scheduler_t *scheduler;
    ma_time_zone_t zone;
    ma_task_delay_policy_t delay;
    ma_atomic_counter_t ref_count;
    ma_task_power_policy_t power;
    ma_uint32_t missed:1;
    void *reserved;
};

/* task ctor */
ma_error_t ma_task_create(const char *id, ma_task_t **task) {
    if(id && task && strlen(id) < MA_MAX_TASKID_LEN) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;
        ma_task_core_t *core_task = NULL;
                
        *task = NULL;
        if((core_task = (ma_task_core_t*)calloc(1, sizeof(ma_task_core_t)))) {
            MA_SET_BIT(core_task->cond,MA_TASK_COND_DELETE_WHEN_DONE);
            MA_SET_BIT(core_task->cond,MA_TASK_COND_INTERACTIVE);
            core_task->priority = MA_TASK_PRIORITY_LOW;
            core_task->max_run_time = MA_DEFAULT_MAX_RUN_TIME;
            core_task->max_run_time_limit = MA_DEFAULT_MAX_RUN_TIME_LIMIT;
            core_task->exec_status = MA_TASK_EXEC_NOT_STARTED;
            core_task->state = MA_TASK_STATE_NO_TRIGGER;
            core_task->zone = MA_TIME_ZONE_LOCAL;
            strncpy(core_task->id, id, strlen(id));
            ma_task_init(*task = (ma_task_t*)core_task, &methods, task);
            if(MA_OK == (err = ma_trigger_list_create(&core_task->trigger_list))) {
                (void)ma_trigger_list_set_task(core_task->trigger_list, *task);
                if(MA_OK == (err = ma_trigger_list_create(&core_task->dispose_list))) {
                    (void)ma_trigger_list_set_task(core_task->dispose_list, *task);
                    (void)ma_task_add_ref(*task);
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) created successfully", id);
                }
            }
            if(MA_OK != err) {
                MA_LOG(logger, MA_LOG_SEV_ERROR, "task id(%s) creation failed, last error(%d)", id, err); 
                free(core_task);
            }

        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}


/* task dtor */
ma_error_t core_task_release(ma_task_t *task) {
    if(task) {
        ma_error_t err = MA_OK;
        ma_task_core_t *core_task = (ma_task_core_t*)task;
        
        MA_LOG(logger, MA_LOG_SEV_TRACE, "task id(%s) refcount(%d) before task release", core_task->id, core_task->ref_count);
        if(!MA_ATOMIC_DECREMENT(core_task->ref_count)) {
            ma_bool_t is_task_stopped = MA_FALSE;

            if((MA_OK == (err = ma_check_task_stopped(task, &is_task_stopped))) && is_task_stopped) {
                if(MA_CHECK_BIT(core_task->cond, MA_TASK_COND_DELETE_WHEN_DONE)) {
                    if(core_task->scheduler) {
                        /* bonus: keeping task alive */
                        core_task->ref_count = 1;
                        /* task should be removed from scheduler DB */
                        MA_LOG(logger, MA_LOG_SEV_TRACE, "task id(%s) exist in scheduler DB should be removed before task release", core_task->id);
                        if(MA_OK != (err = ma_scheduler_internal_release_task(core_task->scheduler, task)))
                            core_task_destroy(task);
                    } else {
                        core_task_destroy(task);
                    }
                } else {
                    MA_LOG(logger, MA_LOG_SEV_TRACE, "task id(%s) deletion not possible!!!", core_task->id);
                    /* bonus: keeping task alive */
                    core_task->ref_count = 1;
                }
            } else {
                core_task->ref_count = 1;
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_task_set_settings(ma_task_t *task, const char *path , const char *name, ma_variant_t *value) {
    return(task && path && name && value)?set_settings(path, name, value, &((ma_task_core_t*)task)->app_payload):MA_ERROR_INVALIDARG;
}

ma_error_t core_task_set_retry_policy(ma_task_t *task, ma_task_retry_policy_t policy) {
    if(task) {
        ma_task_core_t *core_task = (ma_task_core_t*)task;
        ma_error_t err = MA_OK;
        ma_task_retry_policy_t dummy = {0};

        memset(&dummy, 0, sizeof(ma_task_retry_policy_t));
        core_task->retry_count = 0; core_task->retry_interval = 0;
        if(memcmp(&dummy, &policy, sizeof(ma_task_retry_policy_t))) {
            core_task->retry_count = policy.retry_count;       
            if(policy.retry_interval.hour || policy.retry_interval.minute) {
                core_task->retry_interval = ((policy.retry_interval.hour * 60) + policy.retry_interval.minute)*60;
            } else {
                core_task->retry_interval = core_task->retry_interval * 60; /* in secs */
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_task_set_app_payload(ma_task_t *task, ma_variant_t *payload) {
    if(task && payload) {
        ma_error_t err = MA_OK;

        if(((ma_task_core_t*)task)->app_payload) {
            if(((ma_task_core_t*)task)->app_payload != payload) {
                (void)ma_variant_release(((ma_task_core_t*)task)->app_payload);
                err = ma_variant_add_ref(((ma_task_core_t*)task)->app_payload = payload);
            }
        } else
            err = ma_variant_add_ref(((ma_task_core_t*)task)->app_payload = payload);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_task_set_repeat_policy(ma_task_t *task, ma_task_repeat_policy_t policy) {
    if(task) {
        ma_error_t err = MA_OK;
        ma_task_repeat_policy_t dummy = {{0}, {0}};
        ma_task_core_t *core_task = (ma_task_core_t*)task;
        ma_time_zone_t zone = MA_TIME_ZONE_LOCAL;
        const char *task_id = NULL;
        ma_trigger_t *trigger = NULL;

        (void)ma_task_get_time_zone(task, &zone);
        (void)ma_task_get_id(task, &task_id);
        memset(&dummy, 0, sizeof(ma_task_repeat_policy_t));
        memcpy(&core_task->repeat, &policy, sizeof(ma_task_repeat_policy_t));
        if(!memcmp(&dummy, &policy, sizeof(ma_task_repeat_policy_t))) {
            ma_enumerator_reset((ma_enumerator_t*)core_task->trigger_list);
            while(MA_OK == ma_enumerator_next((ma_enumerator_t*)core_task->trigger_list, &trigger)) {                
                (void)ma_trigger_set_duration(trigger, 0); /* reset repeat duration */
                (void)ma_trigger_set_interval(trigger, 0); /* reset repeat interval */
            }
        } else {
            err = MA_ERROR_INVALIDARG;
            if(((policy.repeat_interval.hour <= MA_TOTAL_NO_OF_HOURS_IN_DAY) && (policy.repeat_interval.minute <= MA_TOTAL_NO_OF_MINUTES_IN_HOUR)) || 
                ((policy.repeat_until.hour <=  MA_TOTAL_NO_OF_HOURS_IN_DAY) && (policy.repeat_until.minute <= MA_TOTAL_NO_OF_MINUTES_IN_HOUR))) {
                ma_uint32_t duration = ((policy.repeat_until.hour * 60) + (policy.repeat_until.minute)) * 60; /* in secs */
                ma_uint16_t interval = ((policy.repeat_interval.hour * 60) + (policy.repeat_interval.minute)) * 60; /* in secs */
                
                if (duration <= MA_MAX_SECONDS_IN_DAY) {
                    ma_enumerator_reset((ma_enumerator_t*)core_task->trigger_list); err = MA_OK;
                    while((MA_OK == err) && (MA_OK == ma_enumerator_next((ma_enumerator_t*)core_task->trigger_list, &trigger))) {
                        ma_task_time_t until_date = {0};
                        ma_task_time_t now = {0};
                        ma_task_time_t begin_date = {0};
                        ma_task_time_t *dp = NULL;
                        ma_task_time_t utc = {0};
                        const char *tid = NULL;

                        if(MA_OK == (err = ma_trigger_get_begin_date(trigger, dp = &begin_date))) {
                            gzone_ptr = MA_LOCAL_ZONE_STR;
                            get_localtime(&now);
                            if(MA_TIME_ZONE_UTC == zone) {
                                convert_utc_to_local(begin_date, dp = &utc);
                                gzone_ptr = MA_UTC_ZONE_STR;
                            }
                            now.hour = dp->hour; 
                            now.minute = dp->minute; 
                            now.secs = dp->secs;
                            advance_date_by_seconds(duration, now, dp = &until_date);
                            if(MA_OK == (err = ma_trigger_set_until_date(trigger, until_date))) {
                                (void)ma_trigger_get_id(trigger, &tid);
                                MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) trigger id(%s) until date(%hu/%hu/%hu %hu:%hu:%hu) zone(%s)", task_id, tid, dp->year, dp->month, dp->day, dp->hour, dp->minute, dp->secs, gzone_ptr);
                                (void)ma_trigger_set_duration(trigger, duration);
                                (void)ma_trigger_set_interval(trigger, interval);
                            }
                        }
                        trigger = NULL;
                    } /* end of while */
                    memcpy(&core_task->repeat, &policy, sizeof(ma_task_repeat_policy_t));
                }
            } else {
                memcpy(&core_task->repeat, &dummy, sizeof(ma_task_repeat_policy_t));
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_task_set_max_run_time(ma_task_t *task, ma_uint16_t max_run_time) {
    if(task) {
        ma_uint16_t run_time = MA_DEFAULT_MAX_RUN_TIME_LIMIT;

        if(max_run_time) {
            run_time = max_run_time > run_time ? run_time : max_run_time;
        }
        ((ma_task_core_t*)task)->max_run_time = run_time;

        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_task_set_max_run_time_limit(ma_task_t *task, ma_uint16_t max_run_time_limit) {
    if(task) {
        ma_uint16_t limit = MA_DEFAULT_MAX_RUN_TIME_LIMIT;

        if(max_run_time_limit) {
            limit = max_run_time_limit > limit ? limit : max_run_time_limit;
            limit = limit > ((ma_task_core_t*)task)->max_run_time ? ((ma_task_core_t*)task)->max_run_time : limit;
        }
        ((ma_task_core_t*)task)->max_run_time_limit = limit;

        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_task_set_randomize_policy(ma_task_t *task, ma_task_randomize_policy_t policy) {
    if(task) {
        ma_error_t err = MA_OK;
        ma_task_core_t *core_task = (ma_task_core_t*)task;
        ma_uint16_t random_interval = ((policy.random_interval.hour * 60) + (policy.random_interval.minute)); //minutes
        ma_trigger_t *trigger = NULL;
        const char *tid = NULL;

        ma_enumerator_reset((ma_enumerator_t*)core_task->trigger_list);
        while(MA_OK == ma_enumerator_next((ma_enumerator_t*)core_task->trigger_list, &trigger)) {
            (void)ma_trigger_get_id(trigger, &tid);
            if(MA_OK != (err = ma_trigger_set_random_interval(trigger, random_interval))) {
                MA_LOG(logger, MA_LOG_SEV_ERROR,"task id(%s) trigger id(%s) setting randomize policy failed, last error(%d)", core_task->id, tid, err); 
                break;
            }
            trigger = NULL;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_task_set_priority(ma_task_t *task, ma_task_priority_t priority) {
    if(task && ((priority >= MA_TASK_PRIORITY_LOW) && (priority <= MA_TASK_PRIORITY_HIGH))) {
        ((ma_task_core_t*)task)->priority = priority;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_task_set_conditions(ma_task_t *task, ma_uint32_t conds) {
    if(task) {
        ((ma_task_core_t*)task)->cond = conds;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_task_set_execution_status(ma_task_t *task, ma_task_exec_status_t status) {
    if(task && status) {
        ma_error_t err = MA_OK;

        if(MA_TASK_EXEC_STOP_INITIATED_BY_SCHEDULER != status) {
            size_t size = 0;

            (void)ma_task_size(task, &size);
            if(size) {
                ma_trigger_list_t *tl = ((ma_task_core_t*)task)->trigger_list;
                ma_trigger_t *trigger = NULL;

                ma_enumerator_reset((ma_enumerator_t*)tl);
                while(MA_OK == ma_enumerator_next((ma_enumerator_t*)tl, &trigger)) {
                    ma_trigger_state_t state = MA_TRIGGER_STATE_NOT_STARTED;

                    if(MA_OK == ma_trigger_get_state(trigger, &state)) {
                        if(MA_TRIGGER_STATE_RUNNING == state) {
                            if((MA_TASK_EXEC_RUNNING != status) && (MA_TASK_EXEC_NOT_STARTED != status)) {
                                err = ma_trigger_core_reset((ma_trigger_core_t*)trigger->data, 1000); /* 1 second delay */
                                ((ma_task_core_t*)task)->exec_status = status;
                            }
                            break;
                        }
                    }
                    trigger = NULL;
                }
            } else {
                if(MA_TASK_EXEC_SUCCESS == status || MA_TASK_EXEC_FAILED == status || MA_TASK_EXEC_STOPPED == status) {
                    (void)ma_task_release(task);
                }
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_task_set_delay_policy(ma_task_t *task, ma_task_delay_policy_t policy) {
    if(task) {
        memcpy(&((ma_task_core_t*)task)->delay, &policy, sizeof(ma_task_delay_policy_t));
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_task_set_missed_policy(ma_task_t *task, ma_task_missed_policy_t policy) {
    if(task) {
        ((ma_task_core_t*)task)->missed_interval = ((policy.missed_duration.hour * 60) + policy.missed_duration.minute) * 60;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_task_set_creator_id(ma_task_t *task, const char *id) {
    memset(((ma_task_core_t*)task)->creator_id, 0, MA_MAX_CREATOR_ID_LEN_INT);
    return(task && id && strncpy(((ma_task_core_t*)task)->creator_id, id, strlen(id) < MA_MAX_CREATOR_ID_LEN_INT ? strlen(id) : MA_MAX_CREATOR_ID_LEN_INT))? MA_OK:MA_ERROR_INVALIDARG;
}

ma_error_t core_task_set_power_policy(ma_task_t *task, ma_task_power_policy_t policy) {
    return(task && memcpy(&((ma_task_core_t*)task)->power, &policy, sizeof(ma_task_power_policy_t)))?MA_OK:MA_ERROR_INVALIDARG;
}

ma_error_t core_task_get_power_policy(ma_task_t *task, ma_task_power_policy_t *policy) {
    return(task && memcpy(policy, &((ma_task_core_t*)task)->power, sizeof(ma_task_power_policy_t)))?MA_OK:MA_ERROR_INVALIDARG;
}

ma_error_t core_task_get_state(ma_task_t *task, ma_task_state_t *state) {
    if(task && state) {
        *state = ((ma_task_core_t*)task)->state;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_task_get_last_run_time(ma_task_t *task, ma_task_time_t *last_run_time) {
    if(task && last_run_time) {
        ma_time_zone_t zone = MA_TIME_ZONE_LOCAL;
        
        (void)ma_task_get_time_zone(task, &zone);
        memcpy(last_run_time, &((ma_task_core_t*)task)->last_run_time, sizeof(ma_task_time_t));
        if(MA_TIME_ZONE_UTC == zone) {
            convert_local_to_utc(((ma_task_core_t*)task)->last_run_time, last_run_time);
        }

        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_task_get_next_run_time(ma_task_t *task, ma_task_time_t *next_run_time) {
    if(task && next_run_time) {
        ma_time_zone_t zone = MA_TIME_ZONE_LOCAL;
        
        memcpy(next_run_time, &((ma_task_core_t*)task)->next_run_time, sizeof(ma_task_time_t));
        (void)ma_task_get_time_zone(task, &zone);
        if(MA_TIME_ZONE_UTC == zone) {
            convert_local_to_utc(((ma_task_core_t*)task)->next_run_time, next_run_time);
        }

        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_task_get_app_payload(ma_task_t *task, ma_variant_t **app_payload) {
    if(task && app_payload) {
        *app_payload = NULL;
        if(((ma_task_core_t*)task)->app_payload)(void)ma_variant_add_ref(*app_payload = ((ma_task_core_t*)task)->app_payload);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_task_get_missed_policy(ma_task_t *task, ma_task_missed_policy_t *policy) {
    if(task && policy) {
        return convert_seconds_to_hour_and_minutes(((ma_task_core_t*)task)->missed_interval, &policy->missed_duration.hour, &policy->missed_duration.minute);
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_task_get_repeat_policy(ma_task_t *task, ma_task_repeat_policy_t *policy) {
    if(task && policy) {
        memcpy(policy, &((ma_task_core_t*)task)->repeat, sizeof(ma_task_repeat_policy_t));
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_task_get_priority(ma_task_t *task, ma_task_priority_t *priority) {
    if(task && priority) {
        *priority = ((ma_task_core_t*)task)->priority;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_task_get_conditions(ma_task_t *task, ma_uint32_t *conds) {
    if(task && conds) {
        *conds = ((ma_task_core_t*)task)->cond;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_task_get_id(ma_task_t *task, const char **task_id) {
    if(task && task_id) {
        *task_id = ((ma_task_core_t*)task)->id;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_task_get_name(ma_task_t *task, const char **task_name) {
    if(task && task_name) {
        *task_name = ((ma_task_core_t*)task)->name;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_task_get_type(ma_task_t *task, const char **task_type) {
    if(task && task_type) {
        *task_type = ((ma_task_core_t*)task)->type;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_task_get_creator_id(ma_task_t *task, const char **creator_id) {
    if(task && creator_id) {
        *creator_id = ((ma_task_core_t*)task)->creator_id;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_task_get_retry_policy(ma_task_t *task, ma_task_retry_policy_t *retry_policy) {
    if(task && retry_policy) {
        retry_policy->retry_count = ((ma_task_core_t*)task)->retry_count;
        return convert_seconds_to_hour_and_minutes(((ma_task_core_t*)task)->retry_interval, &retry_policy->retry_interval.hour, &retry_policy->retry_interval.minute);
    }
    return MA_ERROR_INVALIDARG;    
}

ma_error_t core_task_get_max_run_time(ma_task_t *task, ma_uint16_t *max_run_time) {
    if(task && max_run_time) {
        *max_run_time = ((ma_task_core_t*)task)->max_run_time;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_task_get_max_run_time_limit(ma_task_t *task, ma_uint16_t *max_run_time_limit) {
    if(task && max_run_time_limit) {
        *max_run_time_limit = ((ma_task_core_t*)task)->max_run_time_limit;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;    
}

ma_error_t core_task_get_execution_status(ma_task_t *task, ma_task_exec_status_t *status) {
    if(task && status) {
        *status = ((ma_task_core_t*)task)->exec_status;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;    
}

ma_error_t core_task_add_trigger(ma_task_t *task, ma_trigger_t *trigger) {
    if(task && trigger) {
        ma_error_t err = MA_OK;
        const char *trigger_id = NULL;

        MA_LOG(logger, MA_LOG_SEV_TRACE, "task id(%s) trigger adding...", ((ma_task_core_t*)task)->id);
        if(MA_OK == (err = ma_trigger_list_add(((ma_task_core_t*)task)->trigger_list, trigger))) {
            ma_trigger_type_t type = MA_TRIGGER_NOW;

            (void)ma_trigger_get_id(trigger, &trigger_id);
            if(MA_OK == (err = ma_trigger_set_task_handle(trigger, task))) {
                if(MA_TASK_STATE_NO_TRIGGER == ((ma_task_core_t*)task)->state) {
                    err = ma_task_set_state(task, MA_TASK_STATE_NOT_SCHEDULED);
                }
            }
            (void)ma_trigger_get_type(trigger, &type);
            if(MA_TRIGGER_ON_SYSTEM_IDLE == type) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) trigger id(%s) system idle event type", ((ma_task_core_t*)task)->id, trigger_id);
                (void)ma_task_set_event(task, MA_TASK_EVENT_TYPE_SYSTEM_IDLE);
            } else if(MA_TRIGGER_AT_SYSTEM_START == type) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) trigger id(%s) system start event type", ((ma_task_core_t*)task)->id, trigger_id);
                (void)ma_task_set_event(task, MA_TASK_EVENT_TYPE_SYSTEM_STARTUP);
            } else if(MA_TRIGGER_AT_LOGON == type) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) trigger id(%s) logon event type", ((ma_task_core_t*)task)->id, trigger_id);
                (void)ma_task_set_event(task, MA_TASK_EVENT_TYPE_LOGON);
            } else if(MA_TRIGGER_ON_DIALUP == type) {
                (void)ma_task_set_event(task, MA_TASK_EVENT_TYPE_DIALUP);
            } else {
                /* do nothing */
            }
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) trigger id(%s) added successfully", ((ma_task_core_t*)task)->id, trigger_id);
        } else 
            MA_LOG(logger, MA_LOG_SEV_ERROR,"task id(%s) trigger add failed, last error(%d)", ((ma_task_core_t*)task)->id, err);

        return err;
    }
    return MA_ERROR_INVALIDARG; 
}

ma_error_t core_task_remove_trigger(ma_task_t *task, const char *trigger_id) {
    if(task && trigger_id) {
        ma_trigger_t *trigger = NULL;
        ma_error_t err = MA_OK;

        if(MA_OK == (err = ma_trigger_list_find(((ma_task_core_t*)task)->trigger_list, trigger_id, &trigger))) {
            err = core_task_dispose_trigger(task, trigger);
            (void)ma_trigger_release(trigger);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG; 
}

ma_error_t core_task_find_trigger(ma_task_t *task, const char *trigger_id, ma_trigger_t **trigger) {
    if(task && trigger_id && trigger) {
        return ma_trigger_list_find(((ma_task_core_t*)task)->trigger_list, trigger_id, trigger);
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_task_set_name(ma_task_t *task, const char *name) {
    return (task && name && strncpy(((ma_task_core_t*)task)->name, name, MA_MAX_TASKID_LEN))?MA_OK:MA_ERROR_INVALIDARG;
}

ma_error_t core_task_set_type(ma_task_t *task, const char *type) {
    return (task && type && strncpy(((ma_task_core_t*)task)->type, type, MA_MAX_TASKID_LEN))?MA_OK:MA_ERROR_INVALIDARG;
}

ma_error_t core_task_set_software_id(ma_task_t *task, const char *id) {
    return(task && id && strncpy(((ma_task_core_t*)task)->software_id, id, MA_MAX_SOFTWARE_ID_LEN_INT)) ? MA_OK:MA_ERROR_INVALIDARG;
}

ma_error_t core_task_get_software_id(ma_task_t *task, const char **id) {
    if(task && id) {
        *id = ((ma_task_core_t*)task)->software_id;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_task_get_settings(ma_task_t *task, const char *path , const char *name, ma_variant_t **value) {
    return(task && path && name && value)?get_settings(((ma_task_core_t*)task)->app_payload, path, name, value):MA_ERROR_INVALIDARG;
}

ma_error_t core_task_set_state(ma_task_t *task, ma_task_state_t state) {
    if(task) {
        ma_error_t err = MA_OK;

        if((MA_TASK_STATE_EXPIRED == state) || (MA_TASK_STATE_NO_MORE_RUN == state)) {
            if(is_all_triggers_state_same((ma_enumerator_t*)((ma_task_core_t*)task)->trigger_list, MA_TRIGGER_STATE_EXPIRED)) {
                ((ma_task_core_t*)task)->state = state;
            }
        } else if(MA_TASK_STATE_NEVER_RUN == state) {
            if(is_all_triggers_state_same((ma_enumerator_t*)((ma_task_core_t*)task)->trigger_list, MA_TRIGGER_STATE_NOT_STARTED)) {
                ((ma_task_core_t*)task)->state = state;
            }
        } else {
            ((ma_task_core_t*)task)->state = state;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_task_clear_setting(ma_task_t *task) {
    if(task) {
        return(((ma_task_core_t*)task)->app_payload)?ma_variant_release(((ma_task_core_t*)task)->app_payload):MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_task_set_last_run_time(ma_task_t *task, ma_task_time_t last_run_time) {
    if(task) {
        ma_task_core_t *core_task = (ma_task_core_t*)task;
        memcpy(&core_task->last_run_time, &last_run_time, sizeof(ma_task_time_t));
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_task_set_next_run_time(ma_task_t *task, ma_task_time_t next_run_time) {
    if(task) {
        ma_error_t err = MA_OK;
        ma_trigger_t *trigger = NULL;
        ma_task_time_t nearest_run_time = {0};
        ma_bool_t copy_once = MA_FALSE;

        memcpy(&nearest_run_time, &next_run_time, sizeof(ma_task_time_t));
        ma_enumerator_reset((ma_enumerator_t*)((ma_task_core_t*)task)->trigger_list);
        while(MA_OK == ma_enumerator_next((ma_enumerator_t*)((ma_task_core_t*)task)->trigger_list, &trigger)){
            ma_task_time_t trigger_next_run_time = {0};
            if(MA_OK == (err = ma_trigger_get_next_run_time(trigger, &trigger_next_run_time))) {
                ma_task_time_t dummy = {0};
                if(memcmp(&dummy, &trigger_next_run_time, sizeof(ma_task_time_t))) {
                    ma_task_time_t now = {0};
                    
                    get_localtime(&now);
                    if(MA_TIME_ZONE_UTC == ((ma_task_core_t*)task)->zone) {
                        convert_local_to_utc(now, &now);
                    }
                    if(memcmp(&trigger_next_run_time, &now, sizeof(ma_task_time_t)) > 0) {
                        if(!copy_once) {
                            //first copy
                            memcpy(&nearest_run_time, &trigger_next_run_time, sizeof(ma_task_time_t));
                            copy_once = MA_TRUE;
                        } else {
                            if((memcmp(&nearest_run_time, &trigger_next_run_time, sizeof(ma_task_time_t))) > 0) {
                                memcpy(&nearest_run_time, &trigger_next_run_time, sizeof(ma_task_time_t));
                            }
                        }
                    }
                }
            }
        }
        memcpy(&((ma_task_core_t*)task)->next_run_time, &nearest_run_time, sizeof(ma_task_time_t));

        return err;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t core_task_set_time_zone(ma_task_t *task, ma_time_zone_t zone) {
    if(task) {
        ((ma_task_core_t*)task)->zone = zone;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_task_get_time_zone(ma_task_t *task, ma_time_zone_t *zone) {
    if(task && zone) {
        *zone = ((ma_task_core_t*)task)->zone;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_task_copy(const ma_task_t *task, ma_task_t **copy) {
    if(task && copy) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;

        if((*copy = (ma_task_t*)calloc(1, sizeof(ma_task_core_t)))) {
            ma_task_init((ma_task_t*)*copy, task->methods, copy);
            strncpy(((ma_task_core_t*)*copy)->name, ((ma_task_core_t*)task)->name, strlen(((ma_task_core_t*)task)->name) < MA_MAX_TASKID_LEN ? strlen(((ma_task_core_t*)task)->name):MA_MAX_TASKID_LEN);
            strncpy(((ma_task_core_t*)*copy)->type, ((ma_task_core_t*)task)->type, strlen(((ma_task_core_t*)task)->type) < MA_MAX_TASKID_LEN ? strlen(((ma_task_core_t*)task)->type):MA_MAX_TASKID_LEN);
            strncpy(((ma_task_core_t*)*copy)->id, ((ma_task_core_t*)task)->id, strlen(((ma_task_core_t*)task)->id) < MA_MAX_TASKID_LEN ? strlen(((ma_task_core_t*)task)->id) : MA_MAX_TASKID_LEN);
            strncpy(((ma_task_core_t*)*copy)->software_id, ((ma_task_core_t*)task)->software_id, strlen(((ma_task_core_t*)task)->software_id) < MA_MAX_SOFTWARE_ID_LEN_INT ? strlen(((ma_task_core_t*)task)->software_id) : MA_MAX_SOFTWARE_ID_LEN_INT);
            strncpy(((ma_task_core_t*)*copy)->creator_id, ((ma_task_core_t*)task)->creator_id, strlen(((ma_task_core_t*)task)->creator_id) < MA_MAX_CREATOR_ID_LEN_INT ? strlen(((ma_task_core_t*)task)->creator_id) : MA_MAX_CREATOR_ID_LEN_INT);
            ((ma_task_core_t*)*copy)->zone = ((ma_task_core_t*)task)->zone;
            ((ma_task_core_t*)*copy)->state = MA_TASK_STATE_NO_TRIGGER;
            ((ma_task_core_t*)*copy)->cond = ((ma_task_core_t*)task)->cond;
            ((ma_task_core_t*)*copy)->loop = ((ma_task_core_t*)task)->loop;
            ((ma_task_core_t*)*copy)->events = ((ma_task_core_t*)task)->events;
            ((ma_task_core_t*)*copy)->scheduler = ((ma_task_core_t*)task)->scheduler;
            if(((ma_task_core_t*)copy)->app_payload)
                (void)ma_variant_add_ref(((ma_task_core_t*)copy)->app_payload = ((ma_task_core_t*)task)->app_payload);

            {
                if(MA_OK == (err = ma_trigger_list_create(&((ma_task_core_t*)*copy)->trigger_list))) {
                    ma_trigger_t *trigger = NULL;

                    ma_enumerator_reset((ma_enumerator_t*)((ma_task_core_t*)task));
                    while(MA_OK == ma_enumerator_next((ma_enumerator_t*)((ma_task_core_t*)task)->trigger_list, &trigger)) {
                        ma_trigger_type_t type = MA_TRIGGER_ONCE;
                        if(MA_OK == (err = ma_trigger_get_type(trigger, &type))) {
                            ma_trigger_t *t = NULL;
                            switch(type) {
                                case MA_TRIGGER_DAILY: {
                                    ma_trigger_daily_policy_t policy = {0};
                                    if(MA_OK == (err = ma_trigger_get_policy(trigger, &policy))) {
                                        err = ma_trigger_create_daily(&policy, &t);
                                    }
                                    break;
                                }
                                case MA_TRIGGER_WEEKLY:  {
                                    ma_trigger_weekly_policy_t policy = {0};
                                    if(MA_OK == (err = ma_trigger_get_policy(trigger, &policy))) {
                                        err = ma_trigger_create_weekly(&policy, &t);
                                    }
                                    break;
                                }
                                case MA_TRIGGER_MONTHLY_DATE:  {
                                    ma_trigger_monthly_date_policy_t policy = {0};
                                    if(MA_OK == (err = ma_trigger_get_policy(trigger, &policy))) {
                                        err = ma_trigger_create_monthly_date(&policy, &t);
                                    }
                                    break;
                                }
                                case MA_TRIGGER_ONCE:  {
                                    ma_trigger_run_once_policy_t policy = {0};
                                    if(MA_OK == (err = ma_trigger_get_policy(trigger, &policy))) {
                                        err = ma_trigger_create_run_once(&policy, &t);
                                    }
                                    break;
                                }
                                case MA_TRIGGER_AT_SYSTEM_START:  {
                                    ma_trigger_systemstart_policy_t policy = {0};
                                    if(MA_OK == (err = ma_trigger_get_policy(trigger, &policy))) {
                                        err = ma_trigger_create_system_start(&policy, &t);
                                    }
                                    break;
                                }
                                case MA_TRIGGER_AT_LOGON:  {
                                    ma_trigger_logon_policy_t policy = {0};
                                    if(MA_OK == (err =  ma_trigger_get_policy(trigger, &policy))) {
                                        err = ma_trigger_create_logon(&policy, &t);
                                    }
                                    break;
                                }
                                case MA_TRIGGER_ON_SYSTEM_IDLE:  {
                                    ma_trigger_system_idle_policy_t policy = {0};
                                    if(MA_OK == (err = ma_trigger_get_policy(trigger, &policy))) {
                                        err = ma_trigger_create_system_idle(&policy, &t);
                                    }
                                    break;
                                }
                                case MA_TRIGGER_NOW:  {
                                    ma_trigger_run_now_policy_t policy = {0};
                                    if(MA_OK == (err = ma_trigger_get_policy(trigger, &policy))) {
                                        err = ma_trigger_create_run_now(&policy, &t);
                                    }
                                    break;
                                }
                                case MA_TRIGGER_ON_DIALUP:  {
                                    ma_trigger_dialup_policy_t policy = {0};
                                    if(MA_OK == (err = ma_trigger_get_policy(trigger, &policy))) {
                                        err = ma_trigger_create_dialup(&policy, &t);
                                    }
                                    break;
                                }
                                case MA_TRIGGER_MONTHLY_DOW:  {
                                    ma_trigger_monthly_dow_policy_t policy = {0};
                                    if(MA_OK == (err = ma_trigger_get_policy(trigger, &policy))) {
                                        err = ma_trigger_create_monthly_dow(&policy, &t);
                                    }
                                    break;
                                }
                            }

                            {
                                ma_task_time_t begin_date = {0};
                                if((MA_OK == err) && (MA_OK == (err = ma_trigger_get_begin_date(trigger, &begin_date)))) {
                                    (void)ma_trigger_set_begin_date(t, &begin_date);
                                }
                            }

                            {
                                ma_task_time_t end_date = {0};
                                if((MA_OK == err) && (MA_OK == (err = ma_trigger_get_end_date(trigger, &end_date)))) {
                                    (void)ma_trigger_set_end_date(t, &end_date);
                                }
                            }

                            if((MA_OK == err) && (MA_OK == (err = ma_task_add_trigger((ma_task_t*)*copy, t)))) {
                                t = NULL;
                            }

                        }
                    }
                }
            }

            {
                ma_task_retry_policy_t policy = {0};
                if(MA_OK == (err = ma_task_get_retry_policy((ma_task_t*)task, &policy))) {
                    err = ma_task_set_retry_policy((ma_task_t*)*copy, policy);
                }
            }

            {
                ma_task_repeat_policy_t policy = {{0}};
                if((MA_OK == err) && (MA_OK == (err = ma_task_get_repeat_policy((ma_task_t*)task, &policy)))) {
                    (void)ma_task_set_repeat_policy((ma_task_t*)*copy, policy);
                }
            }

            {
                ma_task_missed_policy_t policy = {{0}};
                if((MA_OK == err) && (MA_OK == (err = ma_task_get_missed_policy((ma_task_t*)task, &policy)))) {
                    (void)ma_task_set_missed_policy((ma_task_t*)*copy, policy);
                }
            }

            {
                ma_task_delay_policy_t policy = {0};
                if(MA_OK == ma_task_get_delay_policy((ma_task_t*)task, &policy)) {
                    (void)ma_task_set_delay_policy((ma_task_t*)*copy, policy);
                }
            }

            {
                ma_uint16_t max_run_time = 0;
                if((MA_OK == err) && (MA_OK == (err = ma_task_get_max_run_time((ma_task_t*)task, &max_run_time)))) {
                    (void)ma_task_set_max_run_time((ma_task_t*)*copy, max_run_time);
                }
            }

            {
                ma_uint16_t max_run_time_limit = 0;
                if((MA_OK == err) && (MA_OK == (err = ma_task_get_max_run_time_limit((ma_task_t*)task, &max_run_time_limit)))) {
                    err = ma_task_set_max_run_time_limit((ma_task_t*)*copy, max_run_time_limit);
                }
            }

            {
                ma_task_priority_t priority = MA_TASK_PRIORITY_LOW;
                if((MA_OK == err) && (MA_OK == (err = ma_task_get_priority((ma_task_t*)task, &priority)))) {
                    (void)ma_task_set_priority((ma_task_t*)*copy, priority);
                }
            }

            {
                ma_task_exec_status_t exec_status = MA_TASK_EXEC_NOT_STARTED;
                if((MA_OK == err) && (MA_OK == (err = ma_task_get_execution_status((ma_task_t*)task, &exec_status)))) {
                    (void)ma_task_set_execution_status((ma_task_t*)*copy, exec_status);
                }
            }

            if(MA_OK == err) {
                err = ma_task_add_ref((ma_task_t*)*copy);
            }
        }

        return err == MA_ERROR_NO_MORE_ITEMS ? MA_OK : err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_task_set_id(ma_task_t *task, const char *task_id) {
    if(task && task_id && (strlen(task_id) < MA_MAX_TASKID_LEN)) {
        strncpy(((ma_task_core_t*)task)->id, task_id, MA_MAX_TASKID_LEN);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_task_get_trigger_list(ma_task_t *task, ma_trigger_list_t **tl) {
    if(task && tl) {
        *tl = ((ma_task_core_t*)task)->trigger_list;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_task_get_randomize_policy(ma_task_t *task, ma_task_randomize_policy_t *policy) {
    if(task && policy) {
        ma_error_t err = MA_OK;
        ma_task_core_t *core_task = (ma_task_core_t*)task;
        ma_uint16_t random_interval = 0; //minutes
        ma_trigger_t *trigger = NULL;

        memset(policy, 0, sizeof(ma_task_randomize_policy_t));
        ma_enumerator_reset((ma_enumerator_t*)core_task->trigger_list);
        if(MA_OK == ma_enumerator_current((ma_enumerator_t*)core_task->trigger_list, &trigger)) {
            if(MA_OK == (err = ma_trigger_get_random_interval(trigger, &random_interval))) {
                convert_seconds_to_hour_and_minutes(random_interval*60, &policy->random_interval.hour, &policy->random_interval.minute);
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_task_get_delay_policy(ma_task_t *task, ma_task_delay_policy_t *policy) {
    return(task && policy && memcpy(policy, &((ma_task_core_t*)task)->delay, sizeof(ma_task_delay_policy_t)))?MA_OK:MA_ERROR_INVALIDARG;
}

ma_error_t core_task_dispose_trigger(ma_task_t *task, ma_trigger_t *trigger) {
    if(task && trigger) {
        ma_error_t err = MA_OK;
        const char *trigger_id = NULL;
        char tbuf[MA_MAX_LEN] = {0};

        (void)ma_trigger_get_id(trigger, &trigger_id);
        strncpy(tbuf, trigger_id, strlen(trigger_id));
        (void)ma_trigger_set_id(trigger, ""); /* allows trigger to get added into dispose list */
        if(MA_OK != (err = ma_trigger_list_add(((ma_task_core_t*)task)->dispose_list, trigger))) {
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) trigger id(%s) adding into task dispose list failed, last error(%d)", ((ma_task_core_t*)task)->id, trigger_id);
        }
        (void)ma_trigger_set_id(trigger, tbuf);
        if(MA_OK == (err = ma_trigger_list_remove(((ma_task_core_t*)task)->trigger_list, trigger_id))) {
            ma_bool_t start_lock = MA_FALSE;
            ma_uint32_t events = 0;

            if((MA_OK == (err = core_get_start_lock_status((ma_trigger_core_t*)trigger->data, &start_lock))) && start_lock) {
                if(MA_OK != (err = ma_trigger_close(trigger))) {
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) trigger id(%s) close failed, last error(%d)", ((ma_task_core_t*)task)->id, trigger_id, err);
                }
                (void)core_reset_start_lock((ma_trigger_core_t*)trigger->data);
            } else {
                (void)ma_trigger_set_state(trigger, MA_TRIGGER_STATE_EXPIRED);
            }
            MA_LOG(logger, MA_LOG_SEV_TRACE, "task id(%s) task ptr(%p) trigger id(%s) adding into task dispose list", ((ma_task_core_t*)task)->id, task, trigger_id);
            (void)ma_task_get_events(task, &events);
            if(events) {
                /* unset event from task */
                (void)core_task_disable_events_on_trigger_dispose(task, trigger);
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_task_size(ma_task_t *task, size_t *no_of_triggers) {
    return(task && no_of_triggers)?ma_trigger_list_size(((ma_task_core_t*)task)->trigger_list, no_of_triggers):MA_ERROR_INVALIDARG;
}





ma_error_t ma_task_set_loop(ma_task_t *task, uv_loop_t *loop) {
    if(task && loop) {
        ((ma_task_core_t*)task)->loop = loop;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_task_get_next_runtime_after(ma_task_t *task, ma_task_time_t *next_run_time) {
    if(task && next_run_time) {
        //TODO
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_task_get_missed_interval(ma_task_t *task, ma_uint16_t *interval) {
    if(task && interval) {
        *interval = ((ma_task_core_t*)task)->missed_interval;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_task_apply_policies(ma_task_t *task) {
    if(task) {
        ma_error_t err = MA_OK;
        ma_task_repeat_policy_t repeat = {{0}};
        ma_task_missed_policy_t missed = {{0}};
        ma_task_retry_policy_t retry = {0};

        if(MA_OK == (err = ma_task_get_repeat_policy(task, &repeat))) {
            (void)ma_task_set_repeat_policy(task, repeat);
        }
        if(MA_OK == (err = ma_task_get_missed_policy(task, &missed))) {
            (void)ma_task_set_missed_policy(task, missed);
        }
        if(MA_OK == (err = ma_task_get_retry_policy(task, &retry))) {
            (void)ma_task_set_retry_policy(task, retry);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_task_disable_events_on_trigger_dispose(ma_task_t *task, ma_trigger_t *trigger) {
    if(task && trigger) {
        ma_error_t err = MA_OK;
        ma_trigger_type_t type = MA_TRIGGER_NOW;
        const char *id = NULL;
        ma_bool_t clear_event = MA_TRUE;

        (void)ma_trigger_get_type(trigger, &type);
        (void)ma_trigger_get_id(trigger, &id);
        if((MA_TRIGGER_AT_SYSTEM_START == type) || (MA_TRIGGER_AT_LOGON == type) || (MA_TRIGGER_ON_SYSTEM_IDLE == type)) {
               ma_trigger_list_t *tl = NULL;
               ma_trigger_t *t = NULL;
               const char *tid = NULL;
               ma_trigger_type_t ttype = MA_TRIGGER_NOW;

               (void)ma_task_get_trigger_list(task, &tl);
               /* walk thru active trigger list */
               ma_enumerator_reset((ma_enumerator_t*)tl);
               while(MA_OK == ma_enumerator_next((ma_enumerator_t*)tl, &t)) {
                   (void)ma_trigger_get_type(t, &ttype);
                   if(ttype == type) {
                       (void)ma_trigger_get_id(t, &tid);
                       if(!strcmp(tid, id)) {
                           continue;
                       } else {
                           ma_trigger_state_t state = MA_TRIGGER_STATE_EXPIRED;
                           (void)ma_trigger_get_state(t, &state);

                           if(MA_TRIGGER_STATE_EXPIRED == state)continue;
                           else clear_event = MA_FALSE;break;
                       }
                   }
               }
               if(clear_event) {
                   /* walk thru dispose list */
                   ma_enumerator_reset((ma_enumerator_t*)((ma_task_core_t*)task)->dispose_list);
                   while(MA_OK == ma_enumerator_next((ma_enumerator_t*)((ma_task_core_t*)task)->dispose_list, &t)) {
                       (void)ma_trigger_get_type(t, &ttype);
                       if(ttype == type) {
                           (void)ma_trigger_get_id(t, &tid);
                           if(!strcmp(tid, id)) {
                               continue;
                           } else {
                               ma_trigger_state_t state = MA_TRIGGER_STATE_EXPIRED;
                               (void)ma_trigger_get_state(t, &state);

                               if(MA_TRIGGER_STATE_EXPIRED == state)continue;
                               else clear_event = MA_FALSE;break;
                           }
                       }
                   }
               }
               if(clear_event) {
                    if(MA_TRIGGER_ON_SYSTEM_IDLE == type) {
                        (void)ma_task_unset_event(task, MA_TASK_EVENT_TYPE_SYSTEM_IDLE);
                    } else if(MA_TRIGGER_AT_SYSTEM_START == type) {
                        (void)ma_task_unset_event(task, MA_TASK_EVENT_TYPE_SYSTEM_STARTUP);
                    } else if(MA_TRIGGER_AT_LOGON == type) {
                        (void)ma_task_unset_event(task, MA_TASK_EVENT_TYPE_LOGON);
                    } else if(MA_TRIGGER_ON_DIALUP == type) {
                        (void)ma_task_unset_event(task, MA_TASK_EVENT_TYPE_DIALUP);
                    } else {
                        /* do nothing */
                    }
               }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_task_add_ref(ma_task_t *task) {
    if(task) {
        MA_ATOMIC_INCREMENT(((ma_task_core_t*)task)->ref_count);
        MA_LOG(logger, MA_LOG_SEV_TRACE, "task id(%s) refcount(%d)", ((ma_task_core_t*)task)->id, ((ma_task_core_t*)task)->ref_count);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_task_set_condition(ma_task_t *task, ma_task_cond_t cond) {
    if(task) {
        MA_SET_BIT(((ma_task_core_t*)task)->cond, cond);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t core_task_set_missed(ma_task_t *task, ma_bool_t missed) {
    if(task) {
        ((ma_task_core_t*)task)->missed = missed;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_task_get_missed(ma_task_t *task, ma_bool_t *missed) {
    if(task) {
        *missed = ((ma_task_core_t*)task)->missed;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

/* control task execution */
ma_error_t ma_task_start(ma_task_t *task) {
    if(task) {
        ma_error_t err = MA_OK;
        const char *ds_path = NULL;
        const char *task_id = NULL;
        const char *trigger_id = NULL;
        size_t no_of_triggers = 0;
        size_t itr = 0;
        ma_trigger_t *trigger = NULL;
        ma_bool_t started = MA_FALSE;
        ma_taskfolder_t *folder = NULL;
        ma_ds_t *datastore = NULL;
        ma_scheduler_t *scheduler = NULL;
        ma_trigger_state_t  state = MA_TRIGGER_STATE_NOT_STARTED;
        ma_time_zone_t zone = MA_TIME_ZONE_LOCAL;
        char task_id_buf[MA_MAX_TASKID_LEN] = {0};
        char task_name_buf[MA_MAX_LEN] = {0};
        ma_uint32_t cond = 0;
        const char *task_name = NULL;
        
        (void)ma_task_get_conditions(task, &cond);
        (void)ma_task_get_id(task, &task_id);
        (void)ma_task_get_name(task, &task_name);
        strncpy(task_id_buf, task_id, strlen(task_id));
        strncpy(task_name_buf, task_name, MA_MAX_LEN-1);
        if(MA_OK == (err = ma_trigger_list_size(((ma_task_core_t*)task)->trigger_list, &no_of_triggers))) {
            for(itr = 0;  ((itr <  no_of_triggers) && (MA_OK == err)); ++itr) {
                if(MA_OK == (err = ma_trigger_list_at(((ma_task_core_t*)task)->trigger_list, itr, &trigger))) {
                    if(MA_OK == (err = ma_trigger_get_state(trigger, &state))) {
                        if(MA_TRIGGER_STATE_BLOCKED == state) {
                            (void)ma_trigger_set_disable(trigger, MA_FALSE);
                        } else if(MA_TRIGGER_STATE_NOT_STARTED != state) {
                            started = MA_TRUE; break;
                        } else {
                            /* do nothing */
                        }
                    }
                    (void)ma_trigger_release(trigger);
                }
            }


            if(!started) {
                (void)ma_task_get_time_zone(task, &zone);
                if(MA_OK == (err = ma_task_get_scheduler(task, &scheduler))) {
                    if(MA_OK == (err = ma_scheduler_get_taskfolder(scheduler, &folder))) {
                        if(MA_OK == (err = ma_taskfolder_get_datastore(folder, &datastore))) {
                            if(MA_OK == (err = ma_taskfolder_get_datastore_basepath(folder, &ds_path))) {
                                if(MA_OK != (err = ma_scheduler_datastore_write(scheduler, task, datastore, ds_path))) {
                                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) writting into DB failed. err(%d)", task_id, err);
                                }
                            }
                        }
                    }
                }

                if(MA_TIME_ZONE_UTC == zone) {
                    gzone_ptr = MA_UTC_ZONE_STR;
                    for(itr = 0; itr < no_of_triggers; ++itr) {
                        if(MA_OK == (err = ma_trigger_list_at(((ma_task_core_t*)task)->trigger_list, itr, &trigger))) {
                            ma_trigger_type_t type = MA_TRIGGER_DAILY;
                            ma_task_time_t begin_date = {0};
                            ma_task_time_t utc = {0};
                            
                            (void)ma_trigger_get_id(trigger, &trigger_id);
                            (void)ma_trigger_get_type(trigger, &type);
                            if(MA_TRIGGER_NOW != type) { /* zone should not be considered. if it's trigger type NOW ..default localtime zone*/
                                if(MA_OK == (err = ma_trigger_get_begin_date(trigger, &begin_date))) {
                                    memcpy(&utc, &begin_date, sizeof(ma_task_time_t));
                                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) adjusting trigger id(%s) begin date to utc time zone", task_id, trigger_id);
                                    convert_utc_to_local(begin_date, &utc);
                                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) trigger id(%s) utc to local time(%hu/%hu/%hu %hu:%hu:%hu)", task_id, trigger_id, utc.year, utc.month, utc.day, utc.hour, utc.minute, utc.secs);
                                    (void)ma_trigger_set_next_fire_date(trigger, utc);
                                }
                            }
                            (void)ma_trigger_release(trigger);
                        }
                    }
                }
                
                if(MA_OK == core_task_apply_policies(task)) {
                    if(task_id)MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) applied latest policies", task_id);
                }

                for(itr = 0;  itr <  no_of_triggers; ++itr) {
                    if(MA_OK == (err = ma_trigger_list_at(((ma_task_core_t*)task)->trigger_list, itr, &trigger))) {
                        (void)ma_trigger_get_id(trigger, &trigger_id);
                        (void)ma_trigger_release(trigger); /* intentional task might get release, if it is already expired */
                        if(MA_OK == (err = ma_trigger_start(trigger))) {
                            if(task_id && trigger_id)
                                MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) trigger id(%s) started.", task_id, trigger_id);
                        } else
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "task failed to start, last error(%d)", err);
                    }
                }
            }
        }

       // if(MA_OK != err)
           // MA_LOG(logger, MA_CHECK_BIT(cond, MA_TASK_COND_HIDDEN) ? MA_LOG_SEV_INFO : MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "The task  %s has failed", !strcmp(task_name_buf, MA_EMPTY) ? task_id_buf : task_name_buf);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_task_stop(ma_task_t *task) {
    if(task) {
        ma_error_t err = MA_OK;
        size_t no_of_triggers = 0;
        ma_trigger_t *trigger = NULL;

        if(MA_OK == (err = ma_trigger_list_size(((ma_task_core_t*)task)->trigger_list, &no_of_triggers))) {
            size_t itr = 0;

            for(itr = 0;  ((itr <  no_of_triggers) && (MA_OK == err)); ++itr) {
                trigger= NULL;
                if(MA_OK == (err = ma_trigger_list_at(((ma_task_core_t*)task)->trigger_list, itr, &trigger))) {
                    err = ma_trigger_set_disable(trigger, MA_TRUE);
                    (void)ma_trigger_release(trigger);
                }
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_task_get_loop(ma_task_t *task, uv_loop_t **loop) {
    if(task && loop) {
        *loop = ((ma_task_core_t*)task)->loop;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_task_set_scheduler(ma_task_t *task, ma_scheduler_t *scheduler) {
    if(task) {
        ((ma_task_core_t*)task)->scheduler = scheduler;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_task_get_scheduler(ma_task_t *task, ma_scheduler_t **scheduler) {
    if(task && scheduler) {
        *scheduler = ((ma_task_core_t*)task)->scheduler;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t core_task_unset_condition(ma_task_t *task, ma_task_cond_t cond) {
    if(task) {
        MA_CLEAR_BIT(((ma_task_core_t*)task)->cond, cond);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_check_task_stopped(ma_task_t *task, ma_bool_t *stopped) {
    if(task) {
        ma_error_t err = MA_OK;
        size_t no_of_triggers = 0;
        ma_enumerator_t *enumerator = NULL;
        ma_trigger_t *trigger = NULL;

        if(MA_OK == (err = ma_task_size(task, &no_of_triggers))) {
            if(no_of_triggers) {
                enumerator = (ma_enumerator_t*)((ma_task_core_t*)task)->trigger_list;
                *stopped = MA_TRUE;
                ma_enumerator_reset(enumerator);
                while((MA_OK == err) && (MA_OK == ma_enumerator_next(enumerator, &trigger))) {
                    ma_trigger_state_t state = MA_TRIGGER_STATE_EXPIRED;

                    if(MA_OK == (err = ma_trigger_get_state(trigger, &state))) {
                        if(MA_TRIGGER_STATE_NOT_STARTED == state) {
                            ma_bool_t start_lock = MA_FALSE;

                            (void)core_get_start_lock_status((ma_trigger_core_t*)trigger->data, &start_lock);
                            if(!start_lock) {
                                state = MA_TRIGGER_STATE_EXPIRED;
                                (void)ma_trigger_set_state(trigger, state);
                            }
                        } 
                        if(MA_TRIGGER_STATE_EXPIRED != state) {
                            *stopped = MA_FALSE; break;
                        }
                    }
                }
            } else {
                /* empty list */
                *stopped = MA_TRUE;
            }
        }
        if(*stopped) {
            enumerator =(ma_enumerator_t*)((ma_task_core_t*)task)->dispose_list;
            ma_enumerator_reset(enumerator);
            err = MA_OK;
            while((MA_OK == err) && (MA_OK == ma_enumerator_next(enumerator, &trigger))) {
                ma_trigger_state_t state = MA_TRIGGER_STATE_EXPIRED;

                if(MA_OK == (err = ma_trigger_get_state(trigger, &state))) {
                    if(MA_TRIGGER_STATE_EXPIRED != state) {
                        *stopped = MA_FALSE; 
                        break;
                    }
                }
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}



ma_bool_t is_all_triggers_state_same(ma_enumerator_t *enumerator, ma_trigger_state_t state) {
    if(enumerator) {
        ma_trigger_state_t trigger_state = MA_TRIGGER_STATE_NOT_STARTED;
        ma_error_t err = MA_OK;
        ma_trigger_t *trigger = NULL;

        ma_enumerator_reset(enumerator);
        while(MA_OK == ma_enumerator_next(enumerator, &trigger)){
            if(MA_OK == (err = ma_trigger_get_state(trigger, &trigger_state))) {
                if(state != trigger_state) {
                    return MA_FALSE;
                }
            }
            trigger = NULL;
        }

        return MA_TRUE;
    }
    return MA_FALSE;
}

ma_error_t ma_task_set_event(ma_task_t *task, ma_task_event_type_t type) {
    if(task) {
        MA_BYTE_SET_BIT(((ma_task_core_t*)task)->events, type);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_task_unset_event(ma_task_t *task, ma_task_event_type_t type) {
    if(task) {
        MA_BYTE_CLEAR_BIT(((ma_task_core_t*)task)->events, type);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_task_get_events(ma_task_t *task, ma_uint32_t *events) {
    if(task) {
        *events = ((ma_task_core_t*)task)->events;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_task_set_execution_status_internal(ma_task_t *task, ma_task_exec_status_t status) {
    if(task) {
        ((ma_task_core_t*)task)->exec_status = status;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


const char *ma_task_get_execution_status_str(ma_task_t *task, ma_task_exec_status_t status) {
    return(status-1 < (sizeof(gtask_exec_status_str)/sizeof(gtask_exec_status_str[0]))) ? gtask_exec_status_str[status-1] : NULL;
}

ma_error_t ma_task_run_after(ma_task_t *task, ma_task_interval_t intervals) {
    if(task) {
        ma_error_t err = MA_OK;
        const char *task_id = NULL;

        (void)ma_task_get_id(task, &task_id);
        if(intervals.hour || intervals.minute) {
            ma_task_exec_status_t exec_status = MA_TASK_EXEC_FAILED;

            (void)ma_task_get_execution_status(task, &exec_status);
            if(MA_TASK_EXEC_RUNNING == exec_status) {
                ma_int64_t interval_secs = (intervals.hour * 60 * 60) + (intervals.minute * 60); /* in secs */

                if(interval_secs) {
                    ma_trigger_list_t *tl = NULL;
                    ma_trigger_t *trigger = NULL;

                    (void)ma_task_get_trigger_list(task, &tl);
                    ma_enumerator_reset((ma_enumerator_t*)tl);
                    while(MA_OK == ma_enumerator_next((ma_enumerator_t*)tl, &trigger)) {
                        ma_trigger_state_t state = MA_TRIGGER_STATE_NOT_STARTED;
                        const char *trigger_id = NULL;

                        (void)ma_trigger_get_state(trigger, &state);
                        (void)ma_trigger_get_id(trigger, &trigger_id);
                        if((MA_TRIGGER_STATE_RUNNING == state) || (MA_TRIGGER_STATE_EXPIRED != state)) {
                            /* reset trigger state and task execution status */
                            (void)ma_task_set_execution_status_internal(task, MA_TASK_EXEC_NOT_STARTED);
                            (void)ma_trigger_set_state(trigger, MA_TRIGGER_STATE_NOT_STARTED);
                            if(MA_OK == (err = ma_trigger_core_reset((ma_trigger_core_t*)trigger->data, interval_secs * 1000))) {
                                MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) trigger id(%s) successfully rescheduled", task_id, trigger_id);
                                break;
                            } else
                                MA_LOG(logger, MA_LOG_SEV_ERROR, "task id(%s) trigger id(%s) failed, last error(%d)", task_id, trigger_id, err);
                        }
                    }
                }
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_task_publish_topics(ma_task_t *task, const char *topic) {
    if(task && topic) {
        ma_error_t err = MA_OK;
        const char *task_id = NULL;
        const char *task_name = NULL;
        const char *task_type = NULL;
        const char *product_id = NULL;
        ma_scheduler_t *scheduler = NULL;
        ma_message_t *msg = NULL;
        ma_msgbus_t *msgbus = NULL;
        ma_variant_t *task_variant = NULL;
        ma_uint32_t cond = 0;

        (void)ma_task_get_conditions(task, &cond);
        if(!MA_CHECK_BIT(cond, MA_TASK_COND_DISABLED)) {
            if((MA_OK == (err = ma_task_get_scheduler(task, &scheduler))) && (MA_OK == (err = ma_scheduler_get_msgbus(scheduler, &msgbus)))) {
                if(msgbus) {
                    if((MA_OK == (err = ma_task_get_id(task, &task_id))) && 
                       (MA_OK == (err = ma_task_get_name(task, &task_name))) &&
                       (MA_OK == (err = ma_task_get_type(task, &task_type))) &&
                       (MA_OK == (err = ma_task_get_software_id(task, &product_id)))) {
                            if(MA_OK == (err = ma_message_create(&msg))) {
                                if((MA_OK == (err = ma_message_set_property(msg, MA_TASK_ID, task_id))) &&
                                   (MA_OK == (err = ma_message_set_property(msg, MA_TASK_NAME, task_name))) &&
                                   (MA_OK == (err = ma_message_set_property(msg, MA_TASK_TYPE, task_type))) && 
                                   (MA_OK == (err = ma_message_set_property(msg, MA_TASK_PRODUCT_ID, product_id))) &&
                                   (MA_OK == (err = convert_task_to_variant(task, &task_variant))) && 
                                   (MA_OK == (err = ma_message_set_payload(msg, task_variant)))) {
                                        if(MA_OK == (err = ma_msgbus_publish(msgbus, topic, MSGBUS_CONSUMER_REACH_OUTPROC, msg))) {
                                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) publishing (%s) topic", task_id ? task_id : MA_EMPTY, topic);
                                        }
                                }
                                (void)ma_message_release(msg);
                            }
							if(task_variant)	(void)ma_variant_release(task_variant) ;	
							task_variant = NULL ;
                    }
                }
            }
        }

        return err;
    }
    MA_LOG(logger, MA_LOG_SEV_DEBUG,"publishing topic (%s) message last error (%d)", topic, MA_ERROR_INVALIDARG);
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_task_attributes_modified(ma_task_t *task1, ma_task_t *task2, ma_bool_t *result) {
    if(task1 && task2 && result) {
        ma_error_t err = MA_OK;

        *result = MA_FALSE;
        if(task1 != task2) {
            size_t task1_size = 0;
            size_t task2_size = 0;

            *result = MA_TRUE;
            (void)ma_task_size(task1, &task1_size);
            (void)ma_task_size(task2, &task2_size);
            if(task1_size == task2_size) {
                size_t itr = 0;
                ma_trigger_list_t *task1_tl = NULL;
                ma_trigger_list_t *task2_tl = NULL;

                (void)ma_task_get_trigger_list(task1, &task1_tl);
                (void)ma_task_get_trigger_list(task2, &task2_tl);
                for(itr = 0; itr < task1_size; ++itr) {
                    ma_trigger_t *trigger1 = NULL;
                    ma_trigger_t *trigger2 = NULL;

                    if(MA_OK == (err = ma_trigger_list_at(task1_tl, itr, &trigger1))) {
                        (void)ma_trigger_release(trigger1); /* intentional */
                        if(MA_OK == (err = ma_trigger_list_at(task2_tl, itr, &trigger2))) {
                            ma_trigger_type_t type1 = MA_TRIGGER_NOW;
                            ma_trigger_type_t type2 = MA_TRIGGER_NOW;

                            (void)ma_trigger_release(trigger2); /* intentional */
                            (void)ma_trigger_get_type(trigger1, &type1);
                            (void)ma_trigger_get_type(trigger2, &type2);
                            if(type1 == type2) {
                                ma_task_time_t trigger1_begin_date = {0};
                                ma_task_time_t trigger2_begin_date = {0};

                                (void)ma_trigger_get_begin_date(trigger1, &trigger1_begin_date);
                                (void)ma_trigger_get_begin_date(trigger2, &trigger2_begin_date);
                                if(!memcmp(&trigger1_begin_date, &trigger2_begin_date, sizeof(ma_task_time_t))) {
                                    ma_task_time_t trigger1_end_date = {0};
                                    ma_task_time_t trigger2_end_date = {0};

                                    (void)ma_trigger_get_end_date(trigger1, &trigger1_end_date);
                                    (void)ma_trigger_get_end_date(trigger2, &trigger2_end_date);

                                    if(memcmp(&trigger1_end_date, &trigger2_end_date, sizeof(ma_task_time_t))) {
                                        *result = MA_FALSE;
                                        break;
                                    }
                                } else {
                                    *result = MA_FALSE;
                                    break;
                                }
                            } else {
                                *result = MA_FALSE;
                                break;
                            }
                        } else {
                            *result = MA_FALSE;
                            break;
                        }
                    } else {
                        *result = MA_FALSE;
                        break;
                    }
                }
            } else {
                *result = MA_FALSE;
            }

            if(*result) {
                ma_uint16_t task1_max_run_time_limit = 0;
                ma_uint16_t task2_max_run_time_limit = 0;

                (void)ma_task_get_max_run_time_limit(task1, &task1_max_run_time_limit);
                (void)ma_task_get_max_run_time_limit(task2, &task2_max_run_time_limit);
                *result = task1_max_run_time_limit == task2_max_run_time_limit ? MA_TRUE : MA_FALSE;
            }

            if(*result) {
                ma_uint16_t task1_max_run_time = 0;
                ma_uint16_t task2_max_run_time = 0;

                (void)ma_task_get_max_run_time(task1, &task1_max_run_time);
                (void)ma_task_get_max_run_time(task2, &task2_max_run_time);
                *result = task1_max_run_time == task2_max_run_time ? MA_TRUE : MA_FALSE;
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_task_attributes_copy_only(ma_task_t *src, ma_task_t *dest) {
    if(src && dest) {
        ma_error_t err = MA_OK;

        {
            ma_task_repeat_policy_t policy = {0};

            if(MA_OK == (err = ma_task_get_repeat_policy(src, &policy))) {
                (void)ma_task_set_repeat_policy(dest, policy);
            }
        }

        {
            ma_task_delay_policy_t policy = {0};
            
            if(MA_OK == (err = ma_task_get_delay_policy(src, &policy))) {
                (void)ma_task_set_delay_policy(dest, policy);
            }
        }

        {
            ma_task_missed_policy_t policy = {0};

            if(MA_OK == (err = ma_task_get_missed_policy(src, &policy))) {
                (void)ma_task_set_missed_policy(dest, policy);
            }
        }

        {
            ma_task_retry_policy_t policy = {0};

            if(MA_OK == (err = ma_task_get_retry_policy(src, &policy))) {
                (void)ma_task_set_retry_policy(dest, policy);
            }
        }

        {
            ma_uint16_t max_run_time_limit = {0};

            if(MA_OK == (err = ma_task_get_max_run_time_limit(src, &max_run_time_limit))) {
                (void)ma_task_set_max_run_time_limit(dest, max_run_time_limit);
            }
        }

        {
            ma_uint16_t max_run_time = {0};

            if(MA_OK == (err = ma_task_get_max_run_time(src, &max_run_time))) {
                (void)ma_task_set_max_run_time(dest, max_run_time);
            }
        }

        {
            ma_variant_t *payload = {0};

            if(MA_OK == (err = ma_task_get_app_payload(src, &payload))) {
                if(payload) {
                    (void)ma_task_set_app_payload(dest, payload);
                    (void)ma_variant_release(payload);
                }
            }
        }

        {
            ma_time_zone_t zone = MA_TIME_ZONE_LOCAL;

            if(MA_OK == (err = ma_task_get_time_zone(src, &zone))) {
                (void)ma_task_set_time_zone(dest, zone);
            }
        }
        
        {
            ma_task_priority_t priority = MA_TASK_PRIORITY_LOW;

            if(MA_OK == (err = ma_task_get_priority(src, &priority))) {
                (void)ma_task_set_priority(dest, priority);
            }
        }

        {
            ma_task_randomize_policy_t policy = {0};

            if(MA_OK == (err = ma_task_get_randomize_policy(src, &policy))) {
                (void)ma_task_set_randomize_policy(dest, policy);
            }
        }

        {
            ma_uint32_t conds = {0};

            if(MA_OK == (err = ma_task_get_conditions(src, &conds))) {
                (void)ma_task_set_conditions(dest, conds);
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t core_task_destroy(ma_task_t *task) {
    if(task) {
        ma_error_t err = MA_OK;
        ma_task_core_t *core_task = (ma_task_core_t*)task;

        if(MA_OK == (err = ma_trigger_list_release(core_task->trigger_list))) {
            if(MA_OK == (err = ma_trigger_list_release(core_task->dispose_list))) {
                (void)core_task_clear_setting(task);
                core_task->dispose_list = NULL;
            }
            core_task->trigger_list = NULL;
        }
        free(task);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}
