#include "ma/internal/services/scheduler/ma_runningtask_collector.h"
#include "ma/scheduler/ma_task.h"
#include "ma/scheduler/ma_enumerator.h"
#include "ma/scheduler/ma_triggerlist.h"
#include "ma/internal/ma_macros.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct ma_running_task_collector_s {
    ma_enumerator_t enumerator;
    ma_task_t **collection;
    size_t no_of_tasks_visited;
    size_t no_of_tasks;
    size_t capacity;
};

MA_CPP(extern "C" {)
ma_error_t ma_task_get_trigger_list(ma_task_t *task, ma_trigger_list_t **tl);
ma_error_t ma_trigger_start(ma_trigger_t *trigger, void *data);
ma_error_t ma_trigger_stop(ma_trigger_t *trigger);
ma_error_t ma_task_stop(ma_task_t *task);
MA_CPP(})

static ma_error_t current(ma_enumerator_t *enumerator, void *curr);
static ma_error_t move_next(ma_enumerator_t *enumerator, void *next);
static void reset(ma_enumerator_t *enumerator);

static const ma_enumerator_methods_t methods = {
    &current,
    &move_next,
    &reset
};

ma_error_t ma_running_task_collector_create(ma_running_task_collector_t **collector) {
    if(collector) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;

        if((*collector = (ma_running_task_collector_t*)calloc(1, sizeof(ma_running_task_collector_t)))) {
            ma_enumerator_init((ma_enumerator_t*)*collector, &methods, collector);
            err = MA_OK;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_running_task_collector_find(ma_running_task_collector_t *collector, ma_task_t *td, ma_bool_t *found) {
    if(collector && td && found) {
        ma_error_t err = MA_OK;
        size_t itr = 0;

        *found = MA_FALSE;
        for(itr = 0; itr < collector->no_of_tasks; ++itr) {
            if(collector->collection[itr] == td) {
                *found = MA_TRUE; break;
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_running_task_collector_add(ma_running_task_collector_t *collector, ma_task_t *td) {
    if(collector && td) {
        ma_error_t err  = MA_OK;
        ma_bool_t is_task_exist = MA_FALSE;

        if(MA_OK == (err = ma_running_task_collector_find(collector, td, &is_task_exist))) {
            if(!is_task_exist) {
                if(collector->capacity) {
                    --collector->capacity;
                } else {
                    if(!(collector->collection = (ma_task_t**)realloc(collector->collection, sizeof(ma_task_t*) * (collector->no_of_tasks + 1)))) {
                        ma_enumerator_reset((ma_enumerator_t*)collector);
                        collector->no_of_tasks = 0;
                        return MA_ERROR_OUTOFMEMORY;
                    }
                }
                collector->collection[collector->no_of_tasks] = td;
                collector->no_of_tasks++;
            }
        }
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_running_task_collector_remove(ma_running_task_collector_t *collector, ma_task_t *td) {
    if(collector && td) {
        ma_error_t err = MA_OK;
        size_t itr = 0;

        for(itr = 0; itr < collector->no_of_tasks; ++itr) {
            if(td == collector->collection[itr]) {
                if(collector->no_of_tasks != (itr+1)) {
                    memcpy(collector->collection+itr, collector->collection+(itr+1), (sizeof(ma_task_t*)*(collector->no_of_tasks - (itr+1))));
                }
                --collector->no_of_tasks;
                break;
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_running_task_collector_stop(ma_running_task_collector_t *collector, const char *task_id) {
    if(collector && task_id) {
        ma_error_t err = MA_OK;
        ma_task_t *task = NULL;
        const char *id = NULL;

        (void)ma_enumerator_reset((ma_enumerator_t*)collector);
        while(MA_OK == (err = ma_enumerator_next((ma_enumerator_t*)collector, &task))) {
            if(MA_OK == (err = ma_task_get_id(task, &id))) {
                if(!strcmp(id, (const char*)task_id)) {
                    err = ma_task_stop(task); break;
                }
            }
            task = NULL; //reset
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_running_task_collector_size(ma_running_task_collector_t *collector, size_t *task_count) {
     if(collector && task_count) {
        *task_count = collector->no_of_tasks;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_running_task_collector_release(ma_running_task_collector_t *collector) {
    if(collector) {
        if(collector->collection)free(collector->collection);
        free(collector);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t current(ma_enumerator_t *enumerator, void *curr) {
    if(enumerator && curr) {
        ma_error_t err = MA_ERROR_OUTOFBOUNDS;
        ma_running_task_collector_t *collector = (ma_running_task_collector_t*)enumerator;
        ma_task_t **td = (ma_task_t**)curr;

        *td = NULL;
        if((collector->no_of_tasks_visited < collector->no_of_tasks) && collector->no_of_tasks) {
            *td = collector->collection[collector->no_of_tasks_visited]; err = MA_OK;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t move_next(ma_enumerator_t *enumerator, void *next) {
    if(enumerator && next) {
        ma_error_t err = MA_ERROR_OUTOFBOUNDS;
        ma_running_task_collector_t *collector = (ma_running_task_collector_t*)enumerator;
        ma_task_t **td = (ma_task_t**)next;

        *td = NULL;
        if((collector->no_of_tasks_visited < collector->no_of_tasks) && collector->no_of_tasks) {
            *td = collector->collection[collector->no_of_tasks_visited];
            ++collector->no_of_tasks_visited; err = MA_OK;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

void reset(ma_enumerator_t *enumerator) {
    if(enumerator) {
        ((ma_running_task_collector_t*)enumerator)->no_of_tasks_visited = 0;
    }
}
