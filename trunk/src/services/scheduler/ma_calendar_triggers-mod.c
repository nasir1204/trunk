#include "ma/internal/services/scheduler/ma_scheduler_utils.h"
#include "ma/internal/ma_macros.h"
#include "ma/logger/ma_file_logger.h"
#include "ma/internal/services/scheduler/ma_runningtask_collector.h"
#include "ma/internal/services/scheduler/ma_task_internal.h"
#include "ma/internal/services/scheduler/ma_triggers_internal.h"
#include "ma/internal/services/scheduler/ma_scheduler_core.h"
#include "ma/internal/services/scheduler/ma_trigger_core.h"
#include "ma_scheduler_private.h"

#include <string.h>
#include <stdlib.h>

extern ma_logger_t *logger;
MA_CPP(extern "C" {)
ma_error_t calendar_trigger_get_policy(ma_trigger_t *trigger, void *policy);
MA_CPP(})

static ma_bool_t check_any_runs_pending(ma_trigger_core_t *core, ma_task_time_t yesterday);

struct ma_trigger_run_now_s {
    ma_trigger_t trigger_impl;
    ma_trigger_core_t core;
    ma_bool_t now;
};
struct ma_trigger_run_once_s {
    ma_trigger_t trigger_impl;
    ma_trigger_core_t core;
    ma_bool_t once;
};
struct ma_trigger_daily_s {
    ma_trigger_t trigger_impl;
    ma_trigger_core_t core;
    ma_uint16_t days_interval;
};
struct ma_trigger_weekly_s {
    ma_trigger_t trigger_impl;
    ma_trigger_core_t core;
    ma_uint16_t weeks_interval;
    ma_uint16_t days_of_the_week;
};
struct ma_trigger_monthly_date_s {
    ma_trigger_t trigger_impl;
    ma_trigger_core_t core;
    ma_uint16_t date;
    ma_uint16_t months;
};
struct ma_trigger_monthly_dow_s {
    ma_trigger_t trigger_impl;
    ma_trigger_core_t core;
    ma_which_week_t which_week;
    ma_day_of_week_t day_of_the_week;
    ma_uint16_t months;
};
struct ma_trigger_missed_s {
    ma_trigger_t trigger_impl;
    ma_trigger_core_t core;
    ma_uint16_t hour;
    ma_uint16_t minute;
};
struct ma_trigger_app_idle_s {
    ma_trigger_t trigger_impl;
    ma_trigger_core_t core;
    ma_bool_t recurr;
};

static const ma_trigger_methods_t calendar_methods = {
    &trigger_set_begin_date,
    &trigger_set_end_date,
    &trigger_get_type,
    &trigger_get_state,
    &trigger_get_id,
    &trigger_get_begin_date,
    &trigger_get_end_date,
    &calendar_trigger_get_policy,
    &trigger_set_state,
    &trigger_release,
    &trigger_add_ref,
    &trigger_set_next_fire_date,
    &trigger_get_next_fire_date,
	&calender_trigger_validate_policy
};

ma_error_t calendar_trigger_get_policy(ma_trigger_t *trigger, void *policy) {
     if(trigger && policy) {
         ma_error_t err = MA_ERROR_UNEXPECTED;

         if(((ma_trigger_core_t*)trigger->data)) {
            err = MA_OK;
            if(MA_TRIGGER_NOW == ((ma_trigger_core_t*)trigger->data)->type) {
                ((ma_trigger_run_now_policy_t*)policy)->now = ((ma_trigger_run_now_t*)trigger)->now;
            } else if(MA_TRIGGER_ONCE == ((ma_trigger_core_t*)trigger->data)->type) {
                ((ma_trigger_run_once_policy_t*)policy)->once = ((ma_trigger_run_once_t*)trigger)->once;
            } else if(MA_TRIGGER_DAILY == ((ma_trigger_core_t*)trigger->data)->type) {
                ((ma_trigger_daily_policy_t*)policy)->days_interval = ((ma_trigger_daily_t*)trigger)->days_interval;
            } else if(MA_TRIGGER_WEEKLY == ((ma_trigger_core_t*)trigger->data)->type) {
                ((ma_trigger_weekly_policy_t*)policy)->days_of_the_week = ((ma_trigger_weekly_t*)trigger)->days_of_the_week;
                ((ma_trigger_weekly_policy_t*)policy)->weeks_interval = ((ma_trigger_weekly_t*)trigger)->weeks_interval;
            } else if(MA_TRIGGER_MONTHLY_DATE == ((ma_trigger_core_t*)trigger->data)->type) {
                ((ma_trigger_monthly_date_policy_t*)policy)->date = ((ma_trigger_monthly_date_t*)trigger)->date;
                ((ma_trigger_monthly_date_policy_t*)policy)->months = ((ma_trigger_monthly_date_t*)trigger)->months;
            } else if(MA_TRIGGER_MONTHLY_DOW == ((ma_trigger_core_t*)trigger->data)->type) {
                ((ma_trigger_monthly_dow_policy_t*)policy)->day_of_the_week = ((ma_trigger_monthly_dow_t*)trigger)->day_of_the_week;
                ((ma_trigger_monthly_dow_policy_t*)policy)->months = ((ma_trigger_monthly_dow_t*)trigger)->months;
                ((ma_trigger_monthly_dow_policy_t*)policy)->which_week = ((ma_trigger_monthly_dow_t*)trigger)->which_week;
            } else if(MA_TRIGGER_ON_APP_IDLE == ((ma_trigger_core_t*)trigger->data)->type) {
                ((ma_trigger_app_idle_policy_t*)policy)->recurr = ((ma_trigger_app_idle_t*)trigger)->recurr;
            }
         }

         return err;
     }
     return MA_ERROR_INVALIDARG;
}

ma_error_t calender_trigger_validate_policy(ma_trigger_t *trigger) {
	if(trigger) {
		ma_error_t err = MA_OK;
		
		switch(((ma_trigger_core_t*)trigger->data)->type) {
            case MA_TRIGGER_NOW: 
            case MA_TRIGGER_ONCE:
            case MA_TRIGGER_DAILY:
            case MA_TRIGGER_WEEKLY:
            case MA_TRIGGER_MONTHLY_DOW:
            case MA_TRIGGER_ON_APP_IDLE:
            case MA_TRIGGER_ON_SYSTEM_IDLE:
            case MA_TRIGGER_AT_SYSTEM_START:
            case MA_TRIGGER_AT_LOGON:
            case MA_TRIGGER_AT_POWER_SAVE: {
				break;
			}
            case MA_TRIGGER_MONTHLY_DATE: {
				ma_bool_t result = MA_FALSE;

				if(((ma_trigger_monthly_date_t*)trigger)->date > 0 && ((ma_trigger_monthly_date_t*)trigger)->date <= 31) {
					ma_uint16_t months = ((ma_trigger_monthly_date_t*)trigger)->months;
					ma_uint16_t date = ((ma_trigger_monthly_date_t*)trigger)->date;
					int month_of_year = -1;
					ma_task_time_t today = {0};

					get_localtime(&today);
					if(-1 == (month_of_year = ma_month_of_year(months))) {
						if(MA_CHECK_BIT(months, FEBRUARY) || MA_CHECK_BIT(months, APRIL) || MA_CHECK_BIT(months, JUNE) || 
						   MA_CHECK_BIT(months, SEPTEMBER) || MA_CHECK_BIT(months, NOVEMBER)) {
							result = date < 31 ? MA_TRUE : MA_FALSE;
						}
						if(MA_CHECK_BIT(months, JANUARY) || MA_CHECK_BIT(months, MARCH) || MA_CHECK_BIT(months, MAY) || MA_CHECK_BIT(months, JULY) ||
							MA_CHECK_BIT(months, AUGUST) || MA_CHECK_BIT(months, OCTOBER) || MA_CHECK_BIT(months, DECEMBER)) {
							result = date <= 31 ? MA_TRUE : MA_FALSE;
						}
					} else
						result = FEBRUARY == month_of_year ? date < 31 ? MA_TRUE : MA_FALSE : ma_date_validator(date, month_of_year, today.year);

					err = result ? MA_OK : MA_ERROR_SCHEDULER_INVALID_TRIGGER_POLICY;
				}
                break;
            }
            default: {
                err = MA_ERROR_NOT_SUPPORTED;
                break;
            }
        }

		return err;
	}
	return MA_ERROR_INVALIDARG;
}

static const ma_trigger_core_methods_t runonce_core_methods = {
    &core_notify_run_once_fire,
    &core_run_once_next_fire,
    0,
    &core_run_once_stop_fire,
    &core_notify_force_reset,
    &core_notify_force_stop,
    &core_notify_timer_fire_close
};

ma_error_t ma_trigger_create_run_once(ma_trigger_run_once_policy_t *policy, ma_trigger_t **trigger) {
    if(policy && trigger) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;
        ma_trigger_run_once_t *run_once = NULL;

        *trigger = NULL;
        if((run_once = (ma_trigger_run_once_t*)calloc(1, sizeof(ma_trigger_run_once_t)))) {
            ma_trigger_init(*trigger = (ma_trigger_t*)run_once, &calendar_methods, &run_once->core);
            ma_trigger_core_init((ma_trigger_core_t*)&run_once->core, &runonce_core_methods, MA_TRIGGER_ONCE, MA_WATCHER_TIMER, run_once);
            if(MA_OK ==(err = ma_trigger_set_begin_date(*trigger, NULL))) {
                (void)ma_trigger_add_ref(*trigger);
            }
        }
        if((MA_OK != err) && *trigger) {
            free(*trigger);
            *trigger = NULL;
            MA_LOG(logger, MA_LOG_SEV_DEBUG,"run once trigger creation failed with error(%d)", err);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t daily_next_fire_date(ma_trigger_core_t *core) {
    if(core) {
        ma_error_t err = MA_ERROR_SCHEDULER_NO_MORE_RUN;
        ma_trigger_daily_t *daily = (ma_trigger_daily_t*)core->core_data;
        ma_time_zone_t zone = MA_TIME_ZONE_LOCAL;

        (void)ma_task_get_time_zone(core->task, &zone);
        if(daily->days_interval) {
            ma_task_time_t tmp = {0};
            ma_task_time_t today = {0};
            ma_task_time_t prev_day = {0};
            ma_task_time_t next_day = {0};
            ma_task_time_t dummy = {0};
            ma_task_time_t utc = {0};

            get_localtime(&today);
            memcpy(&tmp, &core->begin_date, sizeof(ma_task_time_t));
            today.hour = core->begin_date.hour;
            today.minute = core->begin_date.minute;
            today.secs = core->begin_date.secs;
            if(MA_TIME_ZONE_UTC == zone) {
                convert_utc_to_local(tmp, &tmp);
                today.hour = tmp.hour;
                today.minute = tmp.minute;
                today.secs = tmp.secs;
            }
            while(memcmp(&tmp, &today, sizeof(ma_task_time_t)) < 0) {
                memcpy(&prev_day, &tmp, sizeof(ma_task_time_t)); /* previous day */
                get_next_date_after(tmp, &tmp, daily->days_interval); /* today */
            }                    
            get_next_date_after(tmp, &next_day, daily->days_interval); /* next day */
            if(check_any_runs_pending(core, prev_day)) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "previous days runs are still pending...");
                memcpy(&core->next_fire_date, &prev_day, sizeof(ma_task_time_t));
            } else if(check_any_runs_pending(core, tmp)) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "current days runs are still pending...");
                memcpy(&core->next_fire_date, &tmp, sizeof(ma_task_time_t));
            } else {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "moving on next day...");
                memcpy(&core->next_fire_date, &next_day, sizeof(ma_task_time_t));
            }
            err = core_set_next_until_date(core);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static const ma_trigger_core_methods_t daily_core_methods = {
    &core_notify_timer_fire,
    &core_notify_timer_next_fire,
    &daily_next_fire_date,
    &core_daily_stop_fire,
    &core_notify_force_reset,
    &core_notify_force_stop,
    &core_notify_timer_fire_close
};

ma_error_t ma_trigger_create_daily(ma_trigger_daily_policy_t *policy, ma_trigger_t **trigger) {
    if(policy && trigger) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;
        ma_trigger_daily_t *daily = NULL;

        *trigger = NULL;
        if((daily = (ma_trigger_daily_t*)calloc(1, sizeof(ma_trigger_daily_t)))) {
            daily->days_interval = policy->days_interval;
            ma_trigger_init(*trigger = (ma_trigger_t*)daily, &calendar_methods, &daily->core);
            ma_trigger_core_init((ma_trigger_core_t*)&daily->core, &daily_core_methods, MA_TRIGGER_DAILY, MA_WATCHER_TIMER, daily);
            if(MA_OK == (err = ma_trigger_set_begin_date(*trigger, NULL))) {
                err = ma_trigger_add_ref(*trigger);
            }
        }
        if((MA_OK != err) && *trigger) {
            free(*trigger);
            *trigger = NULL;
            MA_LOG(logger, MA_LOG_SEV_DEBUG,"daily trigger creation failed with error(%d)", err);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t weekly_next_fire_date(ma_trigger_core_t *core) {
    if(core) {
        ma_trigger_weekly_t *weekly = (ma_trigger_weekly_t*)core->core_data;
        ma_error_t err = MA_ERROR_SCHEDULER_NO_MORE_RUN;
        ma_time_zone_t zone = MA_TIME_ZONE_LOCAL;

        (void)ma_task_get_time_zone(core->task, &zone);
        if(weekly->days_of_the_week) {
            ma_task_time_t tmp = {0};
            ma_task_time_t today = {0};
            ma_task_time_t prev_day = {0};
            ma_task_time_t next_day = {0};
            ma_task_time_t dummy = {0};
            ma_task_time_t utc = {0};

            get_localtime(&today);
            memcpy(&tmp, &core->begin_date, sizeof(ma_task_time_t));
            today.hour = core->begin_date.hour;
            today.minute = core->begin_date.minute;
            today.secs = core->begin_date.secs;
            if(MA_TIME_ZONE_UTC == zone) {
                convert_utc_to_local(tmp, &tmp);
                today.hour = tmp.hour;
                today.minute = tmp.minute;
                today.secs = tmp.secs;
            }
            while(memcmp(&tmp, &today, sizeof(ma_task_time_t)) < 0) {
                memcpy(&prev_day, &tmp, sizeof(ma_task_time_t)); /* previous day */
                get_next_week_date_after(tmp, weekly->days_of_the_week, weekly->weeks_interval, &tmp);/* today */
            }
            get_next_week_date_after(tmp, weekly->days_of_the_week, weekly->weeks_interval, &next_day); /* next day */
            if(check_any_runs_pending(core, prev_day)) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "previous week days runs are still pending...");
                memcpy(&core->next_fire_date, &prev_day, sizeof(ma_task_time_t));
            } else if(check_any_runs_pending(core, tmp)) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "current week days runs are still pending...");
                memcpy(&core->next_fire_date, &tmp, sizeof(ma_task_time_t));
            } else {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "moving on next week day...");
                memcpy(&core->next_fire_date, &next_day, sizeof(ma_task_time_t));
            }
            err = core_set_next_until_date(core);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}


static const ma_trigger_core_methods_t weekly_core_methods = {
    &core_notify_timer_fire,
    &core_notify_timer_next_fire,
    &weekly_next_fire_date,
    &core_weekly_stop_fire,
    &core_notify_force_reset,
    &core_notify_force_stop,
    &core_notify_timer_fire_close
};

ma_error_t ma_trigger_create_weekly(ma_trigger_weekly_policy_t *policy, ma_trigger_t **trigger) {
    if(policy && trigger) {
        ma_error_t err = MA_ERROR_INVALIDARG;
        ma_trigger_weekly_t *weekly = NULL;
        
        *trigger = NULL;
        if(policy->days_of_the_week) {
            err = MA_ERROR_OUTOFMEMORY;
            if((weekly = (ma_trigger_weekly_t*)calloc(1, sizeof(ma_trigger_weekly_t)))) {
                weekly->days_of_the_week = policy->days_of_the_week;
                weekly->weeks_interval = policy->weeks_interval;
                ma_trigger_init((*trigger = (ma_trigger_t*)weekly), &calendar_methods, &weekly->core);
                ma_trigger_core_init((ma_trigger_core_t*)&weekly->core, &weekly_core_methods, MA_TRIGGER_WEEKLY, MA_WATCHER_TIMER, weekly);
                if(MA_OK == (err = ma_trigger_set_begin_date(*trigger, NULL))) {
                    err = ma_trigger_add_ref(*trigger);
                }
            }
            if((MA_OK != err) && *trigger) {
                free(*trigger);
                *trigger = NULL;
                MA_LOG(logger, MA_LOG_SEV_DEBUG,"weekly trigger creation failed with error(%d)", err);
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t monthly_date_next_fire_date(ma_trigger_core_t *core) {
    if(core) {
        ma_trigger_monthly_date_t *monthly_date = (ma_trigger_monthly_date_t*)core->core_data;
        ma_error_t err = MA_ERROR_SCHEDULER_NO_MORE_RUN;
        const char *task_id = NULL;
        ma_time_zone_t zone = MA_TIME_ZONE_LOCAL;

        (void)ma_task_get_time_zone(core->task, &zone);
        (void)ma_task_get_id(core->task, &task_id);
        if(monthly_date->months && ((monthly_date->date >= 1) && (monthly_date->date <= 31))) {
            ma_task_time_t tmp = {0};
            ma_task_time_t today = {0};
            ma_task_time_t cur_mon = {0};
            ma_task_time_t dummy = {0};
            ma_task_time_t next_mon = {0};
            ma_task_time_t utc = {0};

            memset(&dummy, 0, sizeof(ma_task_time_t));
            get_localtime(&today);
            memcpy(&tmp, &core->begin_date, sizeof(ma_task_time_t));
            today.hour = core->begin_date.hour;
            today.minute = core->begin_date.minute;
            today.secs = core->begin_date.secs;
            if(MA_TIME_ZONE_UTC == zone) {
                convert_utc_to_local(tmp, &tmp);
                today.hour = tmp.hour;
                today.minute = tmp.minute;
                today.secs = tmp.secs;
            }
            do {
                get_next_month_date_after(tmp, monthly_date->months, monthly_date->date, &tmp); /* next month */
            }while(memcmp(&tmp, &today, sizeof(ma_task_time_t)) < 0);

			while(tmp.day != monthly_date->date) {
				if(ma_date_validator(monthly_date->date, tmp.month, tmp.year)) {
					tmp.day = monthly_date->date;
					break;
				} else {
					get_next_month_date_after(tmp, monthly_date->months, monthly_date->date, &tmp); /* next month */
				}
			}

            get_current_month_date(today,  monthly_date->months, monthly_date->date, &cur_mon); /* curr month */
            if(check_any_runs_pending(core, cur_mon)) {
				if(cur_mon.day == monthly_date->date) {
					/* current month run pending */
					MA_LOG(logger, MA_LOG_SEV_DEBUG, "current month run still pending...");
					memcpy(&core->next_fire_date, &cur_mon, sizeof(ma_task_time_t));
				} else {
					/* next month run pending */
					MA_LOG(logger, MA_LOG_SEV_DEBUG, "next month run still pending...");
					memcpy(&core->next_fire_date, &tmp, sizeof(ma_task_time_t));
				}
            } else if(check_any_runs_pending(core, tmp)) {
                /* next month run pending */
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "next month run still pending...");
                memcpy(&core->next_fire_date, &tmp, sizeof(ma_task_time_t));
            } else {
                /* next to next month run */
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "next to next month run still pending...");
                get_next_month_date_after(tmp, monthly_date->months, monthly_date->date, &next_mon); /* next - next month */
                memcpy(&core->next_fire_date, &next_mon, sizeof(ma_task_time_t));
            }
            err = core_set_next_until_date(core);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}


static const ma_trigger_core_methods_t monthly_date_core_methods = {
    &core_notify_timer_fire,
    &core_notify_timer_next_fire,
    &monthly_date_next_fire_date,
    &core_monthly_date_stop_fire,
    &core_notify_force_reset,
    &core_notify_force_stop,
    &core_notify_timer_fire_close
};

ma_error_t ma_trigger_create_monthly_date(ma_trigger_monthly_date_policy_t *policy, ma_trigger_t **trigger) {
    if(policy && trigger) {
        ma_error_t err = MA_ERROR_INVALIDARG;
        ma_trigger_monthly_date_t *monthly_date = NULL;

        if(policy->months && ((policy->date >= 1) && (policy->date <= 31))) {
            err = MA_ERROR_OUTOFMEMORY;
            *trigger = NULL;
            if((monthly_date = (ma_trigger_monthly_date_t*)calloc(1, sizeof(ma_trigger_monthly_date_t)))) {
                monthly_date->date = policy->date;
                monthly_date->months = policy->months;
                ma_trigger_init((*trigger = (ma_trigger_t*)monthly_date), &calendar_methods, &monthly_date->core);
                ma_trigger_core_init((ma_trigger_core_t*)&monthly_date->core, &monthly_date_core_methods, MA_TRIGGER_MONTHLY_DATE, MA_WATCHER_TIMER, monthly_date);
				if(MA_OK == (err = ma_trigger_validate_policy(*trigger))) {
					if(MA_OK == (err = ma_trigger_set_begin_date(*trigger, NULL))) {
						err = ma_trigger_add_ref(*trigger);
					}
				}
            }
            if((MA_OK != err) && *trigger) {
                free(*trigger);
                *trigger = NULL;
                MA_LOG(logger, MA_LOG_SEV_DEBUG,"monthly date trigger failed with error(%d)", err);
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t monthly_dow_next_fire_date(ma_trigger_core_t *core) {
    if(core) {
        ma_error_t err = MA_ERROR_SCHEDULER_NO_MORE_RUN;
        ma_trigger_monthly_dow_t *dow = (ma_trigger_monthly_dow_t*)core->core_data;
        ma_time_zone_t zone = MA_TIME_ZONE_LOCAL;

        (void)ma_task_get_time_zone(core->task, &zone);
        if(dow->months && ((dow->which_week >= FIRST_WEEK) && (dow->which_week <= LAST_WEEK)) && ((dow->day_of_the_week >= SUNDAY) && (dow->day_of_the_week <= SATURDAY))) {
            ma_task_time_t tmp = {0};
            ma_task_time_t today = {0};
            ma_task_time_t cur_mon = {0};
            ma_task_time_t dummy = {0};
            ma_task_time_t next_mon = {0};
            ma_task_time_t utc = {0};

            memset(&dummy, 0, sizeof(ma_task_time_t));
            get_localtime(&today);
            memcpy(&tmp, &core->begin_date, sizeof(ma_task_time_t));
            today.hour = core->begin_date.hour;
            today.minute = core->begin_date.minute;
            today.secs = core->begin_date.secs;
            if(MA_TIME_ZONE_UTC == zone) {
                convert_utc_to_local(tmp, &tmp);
                today.hour = tmp.hour;
                today.minute = tmp.minute;
                today.secs = tmp.secs;
            }
            do {
                get_next_month_dow_date_after(tmp, dow->months, dow->which_week, dow->day_of_the_week, &tmp);/* next month */
            }while(memcmp(&tmp, &today, sizeof(ma_task_time_t)) < 0);
            
            get_current_month_dow(today, dow->months, dow->which_week, dow->day_of_the_week, &cur_mon);  /* curr month */
            if(check_any_runs_pending(core, cur_mon)) {
                /* current month run pending */
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "current month run still pending...");
                memcpy(&core->next_fire_date, &cur_mon, sizeof(ma_task_time_t));
            } else if(check_any_runs_pending(core, tmp)) {
                /* next month run pending */
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "next month run still pending...");
                memcpy(&core->next_fire_date, &tmp, sizeof(ma_task_time_t));
            } else {
                /* next to next month run */
                get_next_month_dow_date_after(tmp, dow->months, dow->which_week, dow->day_of_the_week, &next_mon);/* next to next month */
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "next to next month run still pending...");
                memcpy(&core->next_fire_date, &next_mon, sizeof(ma_task_time_t));
            }
            err = core_set_next_until_date(core);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}


static const ma_trigger_core_methods_t monthly_dow_core_methods = {
    &core_notify_timer_fire,
    &core_notify_timer_next_fire,
    &monthly_dow_next_fire_date,
    &core_monthly_dow_stop_fire,
    &core_notify_force_reset,
    &core_notify_force_stop,
    &core_notify_timer_fire_close
};


ma_error_t ma_trigger_create_monthly_dow(ma_trigger_monthly_dow_policy_t *policy, ma_trigger_t **trigger) {
    if(policy && trigger) {
        ma_error_t err = MA_ERROR_INVALIDARG;
        ma_trigger_monthly_dow_t *dow = NULL;

        if(policy->months && ((policy->which_week >= FIRST_WEEK) && (policy->which_week <= LAST_WEEK)) && ((policy->day_of_the_week >= SUNDAY) && (policy->day_of_the_week <= SATURDAY))) {
            err = MA_ERROR_OUTOFMEMORY;
            *trigger = NULL;
            if((dow = (ma_trigger_monthly_dow_t*)calloc(1, sizeof(ma_trigger_monthly_dow_t)))) {
                dow->day_of_the_week = policy->day_of_the_week;
                dow->months = policy->months;
                dow->which_week = policy->which_week;
                ma_trigger_init((*trigger = (ma_trigger_t*)dow), &calendar_methods, &dow->core);
                ma_trigger_core_init((ma_trigger_core_t*)&dow->core, &monthly_dow_core_methods, MA_TRIGGER_MONTHLY_DOW, MA_WATCHER_TIMER, dow);
                if(MA_OK == (err = ma_trigger_set_begin_date(*trigger, NULL))) {
                    err = ma_trigger_add_ref(*trigger);
                }
            }
            if((MA_OK != err) && *trigger) {
                free(*trigger);
                *trigger = NULL;
                MA_LOG(logger, MA_LOG_SEV_DEBUG,"monthly DOW trigger creation failed with error(%d)", err);
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}


static const ma_trigger_core_methods_t app_idle_core_methods = {
    &core_notify_idle_fire,
    &core_notify_idle_next_fire,
    0,
    &core_idle_stop_fire,
    0,
    &core_notify_force_stop,
    &core_notify_idle_fire_close
};

ma_error_t ma_trigger_create_app_idle(ma_trigger_app_idle_policy_t *policy, ma_trigger_t **trigger) {
    if(policy && trigger) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;
        ma_trigger_app_idle_t *app_idle =  NULL;

        *trigger = NULL;
        if((app_idle = (ma_trigger_app_idle_t*)calloc(1, sizeof(ma_trigger_app_idle_t)))) {
            app_idle->recurr = policy->recurr;
            ma_trigger_init((*trigger = (ma_trigger_t*)app_idle), &calendar_methods, &app_idle->core);
            ma_trigger_core_init((ma_trigger_core_t*)&app_idle->core, &app_idle_core_methods, MA_TRIGGER_ON_APP_IDLE, MA_WATCHER_IDLE, app_idle);
            if(MA_OK == (err = ma_trigger_set_begin_date(*trigger, NULL))) {
                err = ma_trigger_add_ref(*trigger);
            }
        }
        if((MA_OK != err) && *trigger) {
            free(*trigger);
            *trigger = NULL;
            MA_LOG(logger, MA_LOG_SEV_DEBUG,"app idle trigger creation failed with error(%d)", err);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static const ma_trigger_core_methods_t run_now_core_methods = {
    &core_notify_run_now_fire,
    &core_run_once_next_fire,
    0,
    &core_run_once_stop_fire,
    &core_notify_force_reset,
    &core_notify_force_stop,
    &core_notify_timer_fire_close
};

ma_error_t ma_trigger_create_run_now(ma_trigger_run_now_policy_t const *policy, ma_trigger_t **trigger) {
    if(policy && trigger) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;
        ma_trigger_run_now_t *run_now = NULL;

        *trigger = NULL;
        if((run_now = (ma_trigger_run_now_t*)calloc(1, sizeof(ma_trigger_run_now_t)))) {
            run_now->now = policy->now;
            ma_trigger_init((*trigger = (ma_trigger_t*)run_now), &calendar_methods, &run_now->core);
            ma_trigger_core_init((ma_trigger_core_t*)&run_now->core, &run_now_core_methods, MA_TRIGGER_NOW, MA_WATCHER_TIMER, run_now);
            advance_current_date_by_seconds(3, &run_now->core.begin_date); //trigger after 3secs
            if(MA_OK == (err = ma_trigger_set_begin_date(*trigger, &run_now->core.begin_date))) {
                err = ma_trigger_add_ref(*trigger);
            }
        }
        if((MA_OK != err) && *trigger) {
            free(*trigger);
            *trigger = NULL;
            MA_LOG(logger, MA_LOG_SEV_DEBUG,"run immediately trigger creation failed, last error(%d)", err);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}



/* helper methods */
ma_bool_t check_any_runs_pending(ma_trigger_core_t *core, ma_task_time_t date) {
    if(core) {
        ma_task_time_t now = {0};

        get_localtime(&now);
        if(core->duration)
            advance_date_by_seconds(core->duration - core->interval, date, &date);
        return (memcmp(&date, &now, sizeof(ma_task_time_t)) > 0)?MA_TRUE:MA_FALSE;
    }
    return MA_FALSE;
}


