#include "ma/internal/services/udp_server/ma_udp_msg_handler.h"
#include "ma/internal/services/udp_server/ma_udp_connection.h"
#include "ma/internal/defs/ma_udp_defs.h"
#include "ma/internal/services/udp_server/ma_udp_global_update_handler.h"

#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/utils/threading/ma_atomic.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include "ma/internal/utils/context/ma_context.h"
#include "ma/internal/services/ma_agent_policy_settings_bag.h" 
#include "ma/internal/utils/macrypto/ma_crypto.h"
#include "ma/internal/utils/spipe/ma_spipe_package.h"
#include "ma/internal/utils/repository/ma_repository_db_manager.h"

#include "ma/ma_log.h"
#include "ma/ma_client.h"
#include "ma/updater/ma_updater.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#ifdef LOG_FACILITY_NAME 
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "udp_global_update"

static ma_bool_t on_match(ma_udp_msg_handler_t *handler, ma_udp_msg_t *c_msg) ;
static ma_error_t on_msg(ma_udp_msg_handler_t *handler, struct ma_udp_connection_s *c_request, ma_udp_msg_t *c_msg) ;
static ma_error_t add_ref(ma_udp_msg_handler_t *handler) ;
static ma_error_t on_release(ma_udp_msg_handler_t *handler) ;

static const ma_udp_msg_handler_methods_t global_update_handler_methods = {
	&on_match,
	&on_msg,
	&add_ref,
	&on_release
} ;

typedef struct ma_udp_global_update_handler_s {
	ma_udp_msg_handler_t	base ;
	ma_atomic_counter_t		ref_count ;
	ma_context_t			*context ;
    void					*data ;
	char					*product_list ;
	char					*catalog_version ;
	char					*server ;
	char					*timestamp ;
} ma_udp_global_update_handler_t;

ma_error_t ma_udp_global_update_handler_create(ma_context_t *context, ma_udp_msg_handler_t **discovery_handler) {
    if(discovery_handler) {
        ma_udp_global_update_handler_t *tmp = (ma_udp_global_update_handler_t *)calloc(1, sizeof(ma_udp_global_update_handler_t)) ;

        if(!tmp)    return MA_ERROR_OUTOFMEMORY ;

		ma_udp_msg_handler_init(&tmp->base, &global_update_handler_methods, (void *)tmp) ;
		MA_ATOMIC_INCREMENT(tmp->ref_count) ;
		tmp->context = context ;
		*discovery_handler = (ma_udp_msg_handler_t*)tmp;

        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_udp_global_update_handler_release(ma_udp_global_update_handler_t *self) {
    if(self) {

		if(0 == MA_ATOMIC_DECREMENT(self->ref_count)) {
			self->context = NULL ;
			self->data = NULL ;

			free(self) ;
		}
	}
    return MA_OK  ;
}

static ma_error_t ma_udp_global_update_handler_set_product_list(ma_udp_global_update_handler_t *self, const char *product_list) {
	if(self) {
		if(self->product_list)	free(self->product_list) ;
		if(product_list) self->product_list = strdup(product_list) ;
		else	self->product_list = NULL ;
	}
	return MA_OK ;
}

static ma_error_t ma_udp_global_update_handler_set_catalog_version(ma_udp_global_update_handler_t *self, const char *catalog_version) {
	if(self) {
		if(self->catalog_version)	free(self->catalog_version) ;
		if(catalog_version) self->catalog_version = strdup(catalog_version) ;
		else	self->catalog_version = NULL ;
	}
	return MA_OK ;
}

ma_error_t ma_udp_global_update_handler_set_server(ma_udp_global_update_handler_t *self, const char *server_name) {
	if(self) {
		if(self->server)	free(self->server) ;
		self->server = NULL ;
		if(server_name)	self->server = strdup(server_name) ;
		return MA_OK ;
	}
	return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_udp_global_update_handler_set_timestamp(ma_udp_global_update_handler_t *self, const char *server_timestamp) {
	if(self) {
		if(self->timestamp)	free(self->timestamp) ;
		self->timestamp = NULL ;
		if(server_timestamp)	self->timestamp = strdup(server_timestamp) ;
		return MA_OK ;
	}
	return MA_ERROR_INVALIDARG ;
}

static ma_bool_t need_processing(ma_udp_global_update_handler_t *self, const char *server_name, const char *server_timestamp) {
	if(self && server_name && server_timestamp) {
		ma_bool_t rc = MA_FALSE, is_exists = MA_FALSE ;
		ma_db_t *db_handle = ma_configurator_get_database(MA_CONTEXT_GET_CONFIGURATOR(self->context)) ;

		if(MA_OK == ma_repository_db_is_site_exists(db_handle, server_name, MA_REPOSITORY_URL_TYPE_SPIPE, &is_exists) && is_exists) {
			if(self->server && self->timestamp) {
				if(strcmp(self->server, server_name) || strcmp(self->timestamp, server_timestamp)) {
					rc = MA_TRUE ;
				}
			}
			else rc = MA_TRUE ;
		}
		return rc ;
	}
	return MA_FALSE ;
}

static size_t get_data_size(const unsigned char *buffer, size_t size) {
	size_t data_size = 0 ;
	char sizeptr[8] = {0} ;
	char *p = NULL ;

	if(p = strstr((char*)buffer, ">")) {
		ma_uint32_t i = 0 ;

		while(p[0] != '=') --p ;
		p = p+2 ;
		
		while(p[i] != '"') {
			sizeptr[i] = p[i] ;
			++i ;
		}
		data_size = strtoul(sizeptr, NULL, 0 ) ;
	}
	return data_size ;
}

static ma_bool_t is_compact_global_update_message(ma_udp_global_update_handler_t *self, ma_udp_msg_t *c_msg) {
	ma_buffer_t *raw_buffer = NULL ;
	ma_error_t rc = MA_OK ;
	ma_bool_t rc1 = MA_FALSE ;

	ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context) ;

	if(MA_OK == (rc = ma_udp_msg_get_raw_message(c_msg, &raw_buffer) )) {
		const char *buffer = NULL ;
		size_t size = 0 ;
		
		(void)ma_buffer_get_string(raw_buffer, &buffer, &size) ;
		
		if(buffer && (size > strlen(MA_COMPAT_SA_WAKEUP_MESSAGE_HEADER_STR))
			&& (!strncmp((char*)buffer, MA_COMPAT_SA_WAKEUP_MESSAGE_HEADER_STR, strlen(MA_COMPAT_SA_WAKEUP_MESSAGE_HEADER_STR))) ) 
		{
			size_t spipe_buffer_len = get_data_size((const unsigned char *)buffer, size) ;
			char *dataptr = NULL ;

			if(dataptr = strstr((char*)buffer, ">"))	++dataptr ;

			if(spipe_buffer_len && dataptr) {
				ma_stream_t *spipe_stream = NULL ;
				
				if(MA_OK == (rc = ma_mstream_create(spipe_buffer_len+1, NULL, &spipe_stream))) {
					size_t written_bytes = 0 ;

					if(MA_OK == (rc = ma_stream_write(spipe_stream, (const unsigned char *)dataptr, spipe_buffer_len, &written_bytes)) && written_bytes == spipe_buffer_len) {
						ma_crypto_t *crypto = MA_CONTEXT_GET_CRYPTO(self->context) ;
						ma_spipe_package_t *spipe_pkg = NULL ;

						(void)ma_stream_seek(spipe_stream, 0) ;

						if(MA_OK == (rc = ma_spipe_package_create(crypto, &spipe_pkg))) {		
							(void)ma_spipe_package_set_logger(spipe_pkg, logger);

							if(MA_OK == (rc = ma_spipe_package_deserialize(spipe_pkg, spipe_stream))) {
								char *spipe_pkg_type = NULL, *product_list = NULL, *catalog_ver = NULL, *server_timestamp = NULL, *server_name = NULL ;
								size_t spipe_info_size = 0 ;

								(void)ma_spipe_package_get_info(spipe_pkg, STR_PACKAGE_TYPE, (const unsigned char **)&spipe_pkg_type, &spipe_info_size) ;
								(void)ma_spipe_package_get_info(spipe_pkg, STR_AGENT_PING_GLOBAL_UPDATE_PRODUCT_LIST, (const unsigned char **)&product_list, &spipe_info_size) ;
								(void)ma_spipe_package_get_info(spipe_pkg, STR_AGENT_PING_CATALOG_VERSION, (const unsigned char **)&catalog_ver, &spipe_info_size) ;
								(void)ma_spipe_package_get_info(spipe_pkg, STR_PKGINFO_SERVER_NAME,  (const unsigned char **)&server_name, &spipe_info_size) ;
								(void)ma_spipe_package_get_info(spipe_pkg, STR_PKGINFO_SERVER_TIME_STAMP,  (const unsigned char **)&server_timestamp, &spipe_info_size) ;

								if(spipe_pkg_type && !strcmp(STR_PKGTYPE_SUPER_AGENT_PING, (const char *)spipe_pkg_type) && catalog_ver) {
									if(need_processing(self, server_name, server_timestamp)) {
										(void)ma_udp_global_update_handler_set_catalog_version(self, (const char *)catalog_ver) ;
										(void)ma_udp_global_update_handler_set_product_list(self, (const char *)product_list) ;
										(void)ma_udp_global_update_handler_set_server(self, (const char *)server_name) ;
										(void)ma_udp_global_update_handler_set_timestamp(self, (const char *)server_timestamp) ;

										MA_LOG(logger, MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "Agent global update call received") ;

										rc1 = MA_TRUE ;
									}
								}
							}
							else {
								MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to deserialize spipe pkg, <rc = %d>.", rc) ;
							}
							(void)ma_spipe_package_release(spipe_pkg) ;	spipe_pkg = NULL ;
						}
						else {
							MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to create spipe pkg, <rc = %d>.", rc) ;
						}
					}
					else {
						MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to write spipe buffer to stream, <rc = %d>.", rc) ;
					}
					(void)ma_stream_release(spipe_stream) ; spipe_stream = NULL ;
				}
				else {
					MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to create to stream, <rc = %d>.", rc) ;
				}
			}
			else {
				MA_LOG(logger, MA_LOG_SEV_ERROR, "Invalid Agent wakeup compat message.") ;
			}
		}
		else {
			MA_LOG(logger, MA_LOG_SEV_ERROR, "Invalid Agent wakeup compat message.") ;
		}

		(void)ma_buffer_release(raw_buffer) ;	raw_buffer = NULL ;
	}
	else {
		MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to get udp raw message, <rc = %d>.", rc) ;
	}
	return rc1 ;
}

static ma_bool_t on_match(ma_udp_msg_handler_t *handler, ma_udp_msg_t *c_msg) {
	if(handler && c_msg) {
		ma_error_t rc = MA_OK ;
		ma_bool_t rc1 = MA_FALSE ;
		
		ma_udp_global_update_handler_t *self = (ma_udp_global_update_handler_t*)handler ;
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context) ;

		ma_udp_msg_type_t type = MA_UDP_MESSAGE_TYPE_MSGBUS ;

		(void)ma_udp_msg_get_type(c_msg, &type) ;

		if(type == MA_UDP_MESSAGE_TYPE_RAW) {
			if(rc1 = is_compact_global_update_message(self, c_msg))
				MA_LOG(logger, MA_LOG_SEV_DEBUG, "Received compat Agent global update message.") ;
		}
		else if(type == MA_UDP_MESSAGE_TYPE_MSGBUS) {
			ma_message_t *msg = NULL ;

			if(MA_OK == (rc = ma_udp_msg_get_ma_message(c_msg, &msg))) {
				char *msg_type = NULL;

				(void)ma_message_get_property(msg, MA_UDP_MSG_KEY_TYPE_STR, &msg_type) ;
            
				if(msg_type && 0 == strcmp(msg_type, MA_UDP_MSG_PROP_VAL_GLOBAL_UPDATE_STR)){
					char *product_list = NULL, *catalog_ver = NULL, *server_name = NULL, *server_timestamp = NULL ;

					(void)ma_message_get_property(msg, MA_UDP_MSG_KEY_GLOBAL_UPDATE_PRODUCT_LIST_STR, &product_list) ;
					(void)ma_message_get_property(msg, MA_UDP_MSG_KEY_GLOBAL_UPDATE_CATALOG_VERSION_STR, &catalog_ver) ;
					(void)ma_message_get_property(msg, MA_UDP_MSG_KEY_GLOBAL_UPDATE_SERVER_NAME_STR, &server_name) ;
					(void)ma_message_get_property(msg, MA_UDP_MSG_KEY_GLOBAL_UPDATE_SERVER_TIMESTAMP_STR, &server_timestamp) ;

					if(need_processing(self, server_name, server_timestamp)) {
						(void)ma_udp_global_update_handler_set_catalog_version(self, (const char *)catalog_ver) ;
						(void)ma_udp_global_update_handler_set_product_list(self, (const char *)product_list) ;
						(void)ma_udp_global_update_handler_set_server(self, (const char *)server_name) ;
						(void)ma_udp_global_update_handler_set_timestamp(self, (const char *)server_timestamp) ;
					
						MA_LOG(logger, MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "Agent global update call received") ;
						rc1 = MA_TRUE ;
					}
				}
				(void)ma_message_release(msg) ; msg = NULL ;
			}
			else {
				MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to get message from udp message") ;
			}
		}
		else {
			MA_LOG(logger, MA_LOG_SEV_ERROR, "Invalid udp message type.") ;
		}
		return rc1 ;
	}
	return MA_FALSE ;
}

static ma_error_t on_msg(ma_udp_msg_handler_t *handler, ma_udp_connection_t *c_request, ma_udp_msg_t *c_msg) {
	if(handler && c_request && c_msg) {
		ma_error_t rc = MA_OK ;
		ma_udp_global_update_handler_t *self = (ma_udp_global_update_handler_t *)handler ;
		ma_message_t *request = NULL ;
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context) ;
		
		MA_LOG(logger, MA_LOG_SEV_DEBUG, "Processing Global update...") ;               

		if(MA_OK == (rc = ma_message_create(&request))) {
			(void)ma_message_set_property(request, MA_UDP_MSG_KEY_GLOBAL_UPDATE_PRODUCT_LIST_STR, self->product_list?self->product_list:"0") ;
			(void)ma_message_set_property(request, MA_UDP_MSG_KEY_GLOBAL_UPDATE_CATALOG_VERSION_STR, self->catalog_version) ;

			if(MA_OK == (rc = ma_msgbus_publish(MA_CONTEXT_GET_MSGBUS(self->context), MA_MSG_AGENT_GLOBAL_UPDATE_TOPIC_STR, MSGBUS_CONSUMER_REACH_OUTPROC, request))) {
				MA_LOG(logger, MA_LOG_SEV_DEBUG, "Global update call processed.") ;															
			}
			else {
				MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to process Global Update, <rc = %d>.", rc) ;							
			}
			(void)ma_message_release(request) ;	request = NULL ;
		}
		(void)ma_udp_global_update_handler_set_catalog_version(self, NULL) ;
		(void)ma_udp_global_update_handler_set_product_list(self, NULL) ;
		return rc;
	}
	return MA_ERROR_INVALIDARG ;
}

static ma_error_t add_ref(ma_udp_msg_handler_t *handler) {
	if(handler) {
		ma_udp_global_update_handler_t *self = (ma_udp_global_update_handler_t *)handler ;
		MA_ATOMIC_INCREMENT(self->ref_count) ;
	}
	return MA_ERROR_INVALIDARG ;
}

static ma_error_t on_release(ma_udp_msg_handler_t *handler) {
	if(handler) {
		ma_udp_global_update_handler_t *self = (ma_udp_global_update_handler_t *)handler ;
		(void)ma_udp_global_update_handler_release(self) ;
	}
	return MA_OK  ;
}
