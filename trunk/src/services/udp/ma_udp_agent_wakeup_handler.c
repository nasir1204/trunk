#include "ma/internal/services/udp_server/ma_udp_msg_handler.h"
#include "ma/internal/services/udp_server/ma_udp_connection.h"
#include "ma/internal/defs/ma_udp_defs.h"
#include "ma_udp_agent_wakeup_handler.h"

#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/ma_strdef.h"

#include "ma/internal/utils/threading/ma_atomic.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include "ma/internal/utils/context/ma_context.h"
#include "ma/internal/services/ma_agent_policy_settings_bag.h" 
#include "ma/internal/utils/macrypto/ma_crypto.h"
#include "ma/internal/utils/spipe/ma_spipe_package.h"
#include "ma/internal/utils/repository/ma_repository_db_manager.h"

#include "ma/ma_log.h"
#include "ma/ma_client.h"
#include "ma/updater/ma_updater.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#ifdef LOG_FACILITY_NAME 
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "udp_agent_wakeup"

static ma_bool_t on_match(ma_udp_msg_handler_t *handler, ma_udp_msg_t *c_msg) ;
static ma_error_t on_msg(ma_udp_msg_handler_t *handler, struct ma_udp_connection_s *c_request, ma_udp_msg_t *c_msg) ;
static ma_error_t add_ref(ma_udp_msg_handler_t *handler) ;
static ma_error_t on_release(ma_udp_msg_handler_t *handler) ;

static const ma_udp_msg_handler_methods_t wakeup_handler_methods = {
	&on_match,
	&on_msg,
	&add_ref,
	&on_release
} ;

typedef struct ma_udp_agent_wakeup_handler_s {
	ma_udp_msg_handler_t	base ;
	ma_atomic_counter_t		ref_count ;
	ma_context_t			*context ;
    void					*data ;
    ma_bool_t               need_full_props;
	char					*server ;
	char					*timestamp ;
    char                    *random_interval ;
} ma_udp_agent_wakeup_handler_t ;

ma_error_t ma_udp_agent_wakeup_handler_create(ma_context_t *context, ma_udp_msg_handler_t **discovery_handler) {
    if(discovery_handler) {
        ma_udp_agent_wakeup_handler_t *tmp = (ma_udp_agent_wakeup_handler_t *)calloc(1, sizeof(ma_udp_agent_wakeup_handler_t)) ;

        if(!tmp)    return MA_ERROR_OUTOFMEMORY ;

		ma_udp_msg_handler_init(&tmp->base, &wakeup_handler_methods, (void *)tmp) ;
		MA_ATOMIC_INCREMENT(tmp->ref_count) ;
		tmp->context = context ;
		*discovery_handler = (ma_udp_msg_handler_t*)tmp ;

        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_udp_agent_wakeup_handler_set_server(ma_udp_agent_wakeup_handler_t *self, const char *server_name) {
	if(self) {
		if(self->server)	free(self->server) ;
		self->server = NULL ;
		if(server_name)	self->server = strdup(server_name) ;
		return MA_OK ;
	}
	return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_udp_agent_wakeup_handler_set_timestamp(ma_udp_agent_wakeup_handler_t *self, const char *server_timestamp) {
	if(self) {
		if(self->timestamp)	free(self->timestamp) ;
		self->timestamp = NULL ;
		if(server_timestamp)	self->timestamp = strdup(server_timestamp) ;
		return MA_OK ;
	}
	return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_udp_agent_wakeup_handler_set_randominterval(ma_udp_agent_wakeup_handler_t *self, const char *randominterval) {
	if(self) {
		if(self->random_interval)	free(self->random_interval) ;
		self->random_interval = NULL ;
		if(randominterval)	self->timestamp = strdup(randominterval) ;
		return MA_OK ;
	}
	return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_udp_agent_wakeup_handler_release(ma_udp_agent_wakeup_handler_t *self) {
    if(self) {

		if(0 == MA_ATOMIC_DECREMENT(self->ref_count)) {
            if(self->server)	free(self->server) ;
            if(self->timestamp)	free(self->timestamp) ;
            if(self->random_interval)	free(self->random_interval) ;

			self->context = NULL ;
			self->data = NULL ;

			free(self) ;
		}
	}
    return MA_OK  ;
}

static ma_bool_t need_processing(ma_udp_agent_wakeup_handler_t *self, const char *server_name, const char *server_timestamp) {
	if(self && server_name && server_timestamp) {
		ma_bool_t rc = MA_FALSE, is_exists = MA_FALSE ;
		ma_db_t *db_handle = ma_configurator_get_database(MA_CONTEXT_GET_CONFIGURATOR(self->context)) ;

		if(MA_OK == ma_repository_db_is_site_exists(db_handle, server_name, MA_REPOSITORY_URL_TYPE_SPIPE, &is_exists) && is_exists) {
			if(self->server && self->timestamp) {
				if(strcmp(self->server, server_name) || strcmp(self->timestamp, server_timestamp)) {
					rc = MA_TRUE ;
				}
			}
			else rc = MA_TRUE ;
		}
		return rc ;
	}
	return MA_FALSE ;
}

static size_t get_data_size(const unsigned char *buffer, size_t size) {
	size_t data_size = 0 ;
	char sizeptr[8] = {0} ;
	char *p = NULL ;

	if(p = strstr((char*)buffer, ">")) {
		ma_uint32_t i = 0 ;

		while(p[0] != '=') --p ;
		p = p+2 ;
		
		while(p[i] != '"') {
			sizeptr[i] = p[i] ;
			++i ;
		}
		data_size = strtoul(sizeptr, NULL, 0 ) ;
	}
	return data_size ;
}

static ma_bool_t is_compact_sa_wakeup_message(ma_udp_agent_wakeup_handler_t *self, ma_udp_msg_t *c_msg) {
	ma_buffer_t *raw_buffer = NULL ;
	ma_error_t rc = MA_OK ;
	ma_bool_t rc1 = MA_FALSE ;

	ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context) ;

	if(MA_OK == (rc = ma_udp_msg_get_raw_message(c_msg, &raw_buffer) )) {
		const char *buffer = NULL ;
		size_t size = 0 ;
		
		(void)ma_buffer_get_string(raw_buffer, &buffer, &size) ;
		
		if(buffer && (size > strlen(MA_COMPAT_SA_WAKEUP_MESSAGE_HEADER_STR))
			&& (!strncmp((char*)buffer, MA_COMPAT_SA_WAKEUP_MESSAGE_HEADER_STR, strlen(MA_COMPAT_SA_WAKEUP_MESSAGE_HEADER_STR))) ) 
		{
			size_t spipe_buffer_len = get_data_size((const unsigned char *)buffer, size) ;
			char *dataptr = NULL ;

			if(dataptr = strstr((char*)buffer, ">"))	++dataptr ;

			if(spipe_buffer_len && dataptr) {
				ma_stream_t *spipe_stream = NULL ;
				
				if(MA_OK == (rc = ma_mstream_create(spipe_buffer_len+1, NULL, &spipe_stream))) {
					size_t written_bytes = 0 ;

					if(MA_OK == (rc = ma_stream_write(spipe_stream, (const unsigned char *)dataptr, spipe_buffer_len, &written_bytes)) && written_bytes == spipe_buffer_len) {
						ma_crypto_t *crypto = MA_CONTEXT_GET_CRYPTO(self->context) ;
						ma_spipe_package_t *spipe_pkg = NULL ;

						(void)ma_stream_seek(spipe_stream, 0) ;

						if(MA_OK == (rc = ma_spipe_package_create(crypto, &spipe_pkg))) {		
							(void)ma_spipe_package_set_logger(spipe_pkg, logger);

							if(MA_OK == (rc = ma_spipe_package_deserialize(spipe_pkg, spipe_stream))) {
								const char *spipe_pkg_type = NULL, *b_need_full_props = NULL, *server_timestamp = NULL, *server_name = NULL, *randomization = NULL ;
								size_t spipe_info_size = 0 ;

								(void)ma_spipe_package_get_info(spipe_pkg, STR_PACKAGE_TYPE, (const unsigned char **)&spipe_pkg_type, &spipe_info_size) ;
								(void)ma_spipe_package_get_info(spipe_pkg, STR_AGENT_PING_SEND_FULL_PROPS, (const unsigned char **)&b_need_full_props, &spipe_info_size) ;
								(void)ma_spipe_package_get_info(spipe_pkg, STR_PKGINFO_SERVER_NAME,  (const unsigned char **)&server_name, &spipe_info_size) ;
								(void)ma_spipe_package_get_info(spipe_pkg, STR_PKGINFO_SERVER_TIME_STAMP,  (const unsigned char **)&server_timestamp, &spipe_info_size) ;
                                (void)ma_spipe_package_get_info(spipe_pkg, STR_AGENT_PING_RAND_INTERVAL, (const unsigned char **)&randomization, &spipe_info_size) ;

								if(spipe_pkg_type && !strcmp(STR_PKGTYPE_SUPER_AGENT_PING, (const char *)spipe_pkg_type) && b_need_full_props && server_name && server_timestamp) {
									if(need_processing(self, server_name, server_timestamp)) {
										MA_LOG(logger, MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "Agent wakeup call received") ;

										self->need_full_props = (b_need_full_props) ? !strcmp(b_need_full_props, "1") : MA_FALSE ;
										(void)ma_udp_agent_wakeup_handler_set_server(self, server_name) ;
										(void)ma_udp_agent_wakeup_handler_set_timestamp(self, server_timestamp) ;
                                        (void)ma_udp_agent_wakeup_handler_set_randominterval(self, randomization) ;
										
										rc1 = MA_TRUE ;
									}
								}
							}
							else {
								MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to deserialize spipe pkg, <rc = %d>.", rc) ;
							}
							(void)ma_spipe_package_release(spipe_pkg) ;	spipe_pkg = NULL ;
						}
						else {
							MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to create spipe pkg, <rc = %d>.", rc) ;
						}
					}
					else {
						MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to write spipe buffer to stream, <rc = %d>.", rc) ;
					}
					(void)ma_stream_release(spipe_stream) ; spipe_stream = NULL ;
				}
				else {
					MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to create to stream, <rc = %d>.", rc) ;
				}
			}
			else {
				MA_LOG(logger, MA_LOG_SEV_ERROR, "Invalid Agent wakeup compat message.") ;
			}
		}
		else {
			MA_LOG(logger, MA_LOG_SEV_ERROR, "Invalid Agent wakeup compat message.") ;
		}

		(void)ma_buffer_release(raw_buffer) ;	raw_buffer = NULL ;
	}
	else {
		MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to get udp raw message, <rc = %d>.", rc) ;
	}
	return rc1 ;
}

static ma_bool_t on_match(ma_udp_msg_handler_t *handler, ma_udp_msg_t *c_msg) {
	if(handler && c_msg) {
		ma_error_t rc = MA_OK ;
		ma_udp_agent_wakeup_handler_t *self = (ma_udp_agent_wakeup_handler_t*)handler;
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context) ;

		ma_bool_t rc1 = MA_FALSE ;
		ma_udp_msg_type_t type = MA_UDP_MESSAGE_TYPE_MSGBUS ;

		(void)ma_udp_msg_get_type(c_msg, &type) ;

		if(type == MA_UDP_MESSAGE_TYPE_RAW) {
			if(rc1 = is_compact_sa_wakeup_message(self, c_msg))
				MA_LOG(logger, MA_LOG_SEV_DEBUG, "Received compat Agent wakeup message.") ;
		}
		else if(type == MA_UDP_MESSAGE_TYPE_MSGBUS) {
			ma_message_t *msg = NULL ;

			if(MA_OK == (rc = ma_udp_msg_get_ma_message(c_msg, &msg))) {
				char *msg_type = NULL;
				/* TODO : need to revisit as per the udp message specs */
				(void)ma_message_get_property(msg, MA_UDP_MSG_KEY_TYPE_STR, &msg_type) ;
            
				if(msg_type && 0 == strcmp(msg_type, MA_UDP_MSG_PROP_VAL_AGENT_WAKEUP_STR)) {
					char *b_need_full_props = NULL, *server_timestamp = NULL, *server_name = NULL, *randomization = NULL ;

					(void)ma_message_get_property(msg, MA_UDP_MSG_KEY_AGENT_WAKEUP_NEED_FULL_PROPS_STR, &b_need_full_props) ;
					(void)ma_message_get_property(msg, MA_UDP_MSG_KEY_AGENT_WAKEUP_SERVER_NAME_STR, &server_name) ;
					(void)ma_message_get_property(msg, MA_UDP_MSG_KEY_AGENT_WAKEUP_SERVER_TIMESTAMP_STR, &server_timestamp) ;
                    (void)ma_message_get_property(msg, MA_UDP_MSG_KEY_AGENT_WAKEUP_RANDOM_INTERVAL_STR, &randomization) ;

					if(need_processing(self, server_name, server_timestamp)) {
						MA_LOG(logger, MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "Agent wakeup call received") ;

						self->need_full_props = (b_need_full_props) ? !strcmp(b_need_full_props, "1") : MA_FALSE ;
						(void)ma_udp_agent_wakeup_handler_set_server(self, server_name) ;
						(void)ma_udp_agent_wakeup_handler_set_timestamp(self, server_timestamp) ;
                        (void)ma_udp_agent_wakeup_handler_set_randominterval(self, randomization) ;

						rc1 = MA_TRUE ;
					}
				}
				(void)ma_message_release(msg) ; msg = NULL ;
			}
			else {
				MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to get message from udp message") ;
			}
		}
		else {
				MA_LOG(logger, MA_LOG_SEV_ERROR, "Invalid udp message type.") ;
		}
		return rc1 ;
	}
	return MA_FALSE ;
}

static ma_error_t on_msg(ma_udp_msg_handler_t *handler, ma_udp_connection_t *c_request, ma_udp_msg_t *c_msg) {
	if(handler && c_request && c_msg) {
		ma_error_t rc = MA_OK;
		ma_msgbus_endpoint_t *endpoint = NULL;
		ma_message_t *request = NULL;	
		ma_udp_agent_wakeup_handler_t *self = (ma_udp_agent_wakeup_handler_t*)handler;
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context) ;

		MA_LOG(logger, MA_LOG_SEV_DEBUG, "Processing AgentWakeUp...");               

		if(MA_OK == (rc = ma_message_create(&request))) {
			(void)ma_message_set_property(request, MA_PROPERTY_MSG_KEY_TYPE_STR, MA_PROPERTY_MSG_TYPE_COLLECT_STR);
			(void)ma_message_set_property(request, MA_PROPERTY_MSG_KEY_PROPS_TYPE_STR, (self->need_full_props) 
						? MA_PROPERTY_MSG_VALUE_PROPS_TYPE_FULL_STR : MA_PROPERTY_MSG_VALUE_PROPS_TYPE_VERSION_STR);
            (void) ma_message_set_property(request, MA_PROPERTY_MSG_KEY_RANDOM_INTERVALS_STR, self->random_interval) ;
            (void) ma_message_set_property(request, MA_PROPERTY_MSG_KEY_ENFORCE_POLICY_STR, "1") ;  /* set force enforce policy flag only for wakeup calls*/

			if(self->need_full_props)
				MA_LOG(logger, MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE , "Agent wakeup call for FULL PROPS received");

			if(MA_OK == (rc = ma_msgbus_endpoint_create(MA_CONTEXT_GET_MSGBUS(self->context), MA_PROPERTY_SERVICE_NAME_STR, NULL, &endpoint))) {
				if(MA_OK == (rc = ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC))) {							
					if(MA_OK == (rc = ma_msgbus_send_and_forget(endpoint, request))) {
						MA_LOG(logger, MA_LOG_SEV_DEBUG, "AgentWakeUp call processed.");															
					}
					else
						MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to process AgentWakeUp, <rc = %d>.", rc);							
				}
				(void)ma_msgbus_endpoint_release(endpoint);
			}
			(void)ma_message_release(request);
		}
		return rc;
	}
	return MA_ERROR_INVALIDARG ;
}

static ma_error_t add_ref(ma_udp_msg_handler_t *handler) {
	if(handler) {
		ma_udp_agent_wakeup_handler_t *self = (ma_udp_agent_wakeup_handler_t *)handler ;
		MA_ATOMIC_INCREMENT(self->ref_count) ;
	}
	return MA_ERROR_INVALIDARG ;
}

static ma_error_t on_release(ma_udp_msg_handler_t *handler) {
	if(handler) {
		ma_udp_agent_wakeup_handler_t *self = (ma_udp_agent_wakeup_handler_t *)handler ;
		(void)ma_udp_agent_wakeup_handler_release(self) ;
	}
	return MA_OK  ;
}
