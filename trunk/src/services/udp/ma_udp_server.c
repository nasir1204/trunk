#include "ma/internal/services/udp_server/ma_udp_server.h"
#include "ma/internal/utils/platform/ma_system_property.h"
#include "ma/internal/utils/network/ma_net_interface.h"
#include "ma/internal/utils/platform/ma_net_interface_list_internal.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/utils/event_loop/ma_uv_helpers.h"
#include "ma/internal/utils/datastructures/ma_queue.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/platform/ma_sys_apis.h"
#include "ma/internal/utils/threading/ma_atomic.h"
#include "uv.h"

#include "ma_udp_utils.h"
#include "ma_udp_msg_handler_list.h"
#include "ma_udp_connection_manager.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#ifndef MA_WINDOWS
#include <errno.h>
#endif

#ifndef IPV6_V6ONLY
#define IPV6_V6ONLY 0x27
#endif

#include "ma/ma_log.h"

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "udp_server"

#define MA_UDP_MAX_BUFFER_SIZE	(2*1024)

static ma_error_t set_mulitcast_options(ma_udp_server_t *self, ma_bool_t set_unset);
static ma_net_interface_list_t *get_net_interface_list(ma_udp_server_t *self) ;

static void ma_udp_server_policy_init(ma_udp_server_policy_t *policy, ma_bool_t  is_enabled, char	*multicast_addr, ma_uint16_t port);
static void ma_udp_server_policy_deinit(ma_udp_server_policy_t *policy);
static void ma_udp_server_policy_copy(ma_udp_server_policy_t *dest, ma_udp_server_policy_t *src);

extern ma_error_t ma_setsocketoption(uv_udp_t *handle, int level, int optname, const char* optval, int optlen, ma_logger_t *logger);

typedef enum ma_udp_server_state_e ma_udp_server_state_t;
enum ma_udp_server_state_e {
	MA_UDP_SERVER_STATE_STOPPED = 0,
	MA_UDP_SERVER_STATE_STARTED	   
};

struct ma_udp_server_s{

	uv_udp_t						v4_handle;

	uv_udp_t						v6_handle;
		
	ma_udp_server_policy_t			policy;

	uv_loop_t						*uv_loop;
	
	ma_logger_t						*logger;
		
	ma_udp_server_state_t			state;

	ma_udp_connection_manager_t		*connection_mgr;

	ma_udp_msg_handler_list_t		msg_handlers;

	ma_atomic_counter_t				ref_count;

	char							buffer[MA_UDP_MAX_BUFFER_SIZE];
};


ma_error_t ma_udp_server_create(ma_udp_server_t **self){
	if(self){
		ma_udp_server_t *server = (ma_udp_server_t*) calloc(1, sizeof(ma_udp_server_t));
		if(server){
			if(MA_OK == ma_udp_connection_manager_create(&server->connection_mgr)){
				ma_udp_msg_handler_list_init(&server->msg_handlers);
				server->state = MA_UDP_SERVER_STATE_STOPPED;
				MA_ATOMIC_INCREMENT(server->ref_count);
				*self = server;							
				return MA_OK;
			}
			free(server);
		}
		return MA_ERROR_OUTOFMEMORY;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_udp_server_release(ma_udp_server_t *self){
	if(self){
		if(0 == MA_ATOMIC_DECREMENT(self->ref_count)){
			MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "destroying the udp server instance.");
			ma_udp_msg_handler_list_deinit(&self->msg_handlers);
			ma_udp_connection_manager_release(self->connection_mgr);
			ma_udp_server_policy_deinit(&self->policy);
			free(self);		
		}
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_udp_server_set_policy(ma_udp_server_t *self, ma_udp_server_policy_t *policy){
	if(self && policy){
		ma_udp_server_policy_copy(&self->policy, policy);
		return MA_OK;							
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_udp_server_get_policy(ma_udp_server_t *self, const ma_udp_server_policy_t **policy){
	if(self && policy){
		*policy = &self->policy;		
		return MA_OK;							
	}
	return MA_ERROR_INVALIDARG;

}

ma_error_t ma_udp_server_set_event_loop(ma_udp_server_t *self, struct ma_event_loop_s *loop){
	if(self && loop){
		self->uv_loop = ma_event_loop_get_uv_loop(loop);		
		(void)ma_udp_connection_manager_set_loop(self->connection_mgr, loop);
		return MA_OK;
	}
	return MA_ERROR_OUTOFMEMORY;
}

ma_error_t ma_udp_server_set_logger(ma_udp_server_t *self, struct ma_logger_s *logger){
	if(self && logger){
		self->logger = logger;
		(void)ma_udp_connection_manager_set_logger(self->connection_mgr, logger);
		return MA_OK;
	}
	return MA_OK;
}

static uv_buf_t alloc_cb(uv_handle_t* handle, size_t suggested_size) {
	if(handle) {
		ma_udp_server_t *self = (ma_udp_server_t *)handle->data;						
		return uv_buf_init(self->buffer, sizeof(self->buffer));
	}
	return uv_buf_init(NULL, 0);
}

void static udp_server_handle_connection(ma_udp_server_t *self, uv_udp_t* handle, uv_buf_t buf, ssize_t nread, struct sockaddr* addr){
	ma_udp_connection_t *udp_c = NULL;
		
	if(MA_OK == ma_udp_connection_manager_manage_connection(self->connection_mgr, handle, addr, buf, nread, &udp_c)){
		ma_queue_t *udp_handler = NULL;
		ma_udp_msg_t *request = NULL;

		if(MA_OK == ma_udp_connection_get_request_msg(udp_c, &request)){

			MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Passing the connection and request msg to udp message handlers.");

			ma_queue_foreach(udp_handler, &self->msg_handlers.qhead){			
				ma_udp_msg_handler_t *current_handler = ((ma_udp_msg_handler_info_t*)udp_handler)->msg_handler;

				if(ma_udp_msg_handler_on_match(current_handler, request)){
					(void)ma_udp_msg_handler_on_msg(current_handler, udp_c, request);
					(void)ma_udp_connection_manager_process_connection(self->connection_mgr, udp_c);
					(void)ma_udp_msg_release(request);
					return;
				}
			}

			MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "No udp message handlers can handle the message, cleaning up.");
			(void)ma_udp_msg_release(request);
		}
		(void)ma_udp_connection_manager_remove_connection(self->connection_mgr, udp_c);
	}	
	
}

static void recv_cb(uv_udp_t* handle, ssize_t nread, uv_buf_t buf, struct sockaddr* addr, unsigned flags) {
	ma_udp_server_t *self = (ma_udp_server_t *)handle->data;
	if(self){
		MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "udp message received.");

		if(nread < 0) {	
			MA_LOG(self->logger, MA_LOG_SEV_ERROR, "udp message size is invalid, unexpected error.");
			return; 	
		}
		if(nread == 0) { /*ASSERT(addr == NULL);*/
			MA_LOG(self->logger, MA_LOG_SEV_ERROR, "udp message size is zero, returning unused buffer.");						
			return;
		}

		if(handle && handle->data && 0 == flags && addr) {
		
			ma_queue_t *item = NULL;
			struct sockaddr_storage sa;
			int namelen = sizeof(sa);
			char peer_addr[INET6_ADDRSTRLEN] = {0};
			ma_uint16_t port = 0;

			if (AF_INET6 == addr->sa_family) { 
				struct sockaddr_in6 *sa6 = (struct sockaddr_in6 *) addr;
				uv_ip6_name(sa6, peer_addr, MA_COUNTOF(peer_addr));
				port = ntohs(sa6->sin6_port);

			} else {
				struct sockaddr_in *sa4 = (struct sockaddr_in *) addr;
				uv_ip4_name( sa4, peer_addr, MA_COUNTOF(peer_addr));
				port = ntohs(sa4->sin_port);
			}
			MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Accepting udp connection from %s:%u", peer_addr, (unsigned) port);
			udp_server_handle_connection(self, handle, buf, nread, addr);				
		}		
	}	
}

static void ipv4_udp_server_start(ma_udp_server_t *self) {
    const unsigned int so_enabled = 1;
	struct sockaddr_in sa = uv_ip4_addr("0.0.0.0", self->policy.port);
	MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Starting ipv4 udp server on port %d...", (int) self->policy.port);

	uv_udp_init(self->uv_loop, &self->v4_handle);
	self->v4_handle.data = self;
	MA_ATOMIC_INCREMENT(self->ref_count);

#ifdef MA_WINDOWS 
    /* prevent anyone else from hijacking the socket. SO_EXCLUSIVEADDRUSE requires admin on XP and will fail gracefully there  */
    do {
        /* Note, we must create the socket ourselves in order to be able to set socket options before libuv binds it */
        const unsigned int so_enabled = 1, so_disabled = 0;
        SOCKET s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
        (void) setsockopt(s, SOL_SOCKET, SO_REUSEADDR, (const char *) &so_disabled, sizeof(so_disabled));
        (void) setsockopt(s, SOL_SOCKET, SO_EXCLUSIVEADDRUSE, (const char *) &so_enabled, sizeof(so_enabled));
        uv_udp_open(&self->v4_handle, s);
    } while (0);
#endif
	if(UV_OK ==  uv_udp_bind(&self->v4_handle, sa, 0)){				
		if(UV_OK == uv_udp_recv_start(&self->v4_handle, alloc_cb, recv_cb)) {
			self->state = MA_UDP_SERVER_STATE_STARTED;	
			MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Started ipv4 udp server.");
			uv_udp_set_broadcast(&self->v4_handle, MA_TRUE);						
		}
		else{
			uv_err_t uv_err = uv_last_error(self->uv_loop);
			MA_LOG(self->logger, MA_LOG_SEV_WARNING, "ipv4 uv_udp_recv_start - libuv err: (\'%s\' %s)", uv_strerror(uv_err), uv_err_name(uv_err));				
		}
	}
	else{
		uv_err_t uv_err = uv_last_error(self->uv_loop);
		MA_LOG(self->logger, MA_LOG_SEV_WARNING, "Unable to start ipv4 udp server - libuv err: (\'%s\' %s)", uv_strerror(uv_err), uv_err_name(uv_err));				
	}
}

static void udp_server_start(ma_udp_server_t *self) {
	struct sockaddr_in6 sa = uv_ip6_addr("::", self->policy.port);
	MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Starting dual-stack udp server on port %d", (int) self->policy.port);
		
	uv_udp_init(self->uv_loop, &self->v6_handle);			
	self->v6_handle.data = self;
	MA_ATOMIC_INCREMENT(self->ref_count);

#ifdef MA_WINDOWS /* prevent anyone else from hijacking the socket. SO_EXCLUSIVEADDRUSE requires admin on XP and will fail gracefully there  */
    do {
        const unsigned int so_enabled = 1, so_disabled = 0;
        SOCKET s = socket(AF_INET6, SOCK_DGRAM, IPPROTO_UDP);
        (void) setsockopt(s, SOL_SOCKET, SO_REUSEADDR, (const char *) &so_disabled, sizeof(so_disabled));
        (void) setsockopt(s, SOL_SOCKET, SO_EXCLUSIVEADDRUSE, (const char *) &so_enabled, sizeof(so_enabled));
        uv_udp_open(&self->v6_handle, s);
    } while (0);
#endif

	if(UV_OK ==  uv_udp_bind6(&self->v6_handle, sa, 0)){							
		if(UV_OK == uv_udp_recv_start(&self->v6_handle, alloc_cb, recv_cb)){		
			int ipv6_only = 0;
			int opt_len = sizeof(ipv6_only);
			self->state = MA_UDP_SERVER_STATE_STARTED;
			set_mulitcast_options(self, MA_TRUE);					
			if((0 == getsockopt(MA_WIN_SELECT(self->v6_handle.socket,self->v6_handle.io_watcher.fd), IPPROTO_IPV6, IPV6_V6ONLY, (char *) &ipv6_only, &opt_len) && ipv6_only) 
				|| MA_WIN_SELECT(WSAENOPROTOOPT == WSAGetLastError(), ENOPROTOOPT == errno )) {
					MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "dual-stack unsupported, Started ipv6 udp server, trying to start separate ipv4 udp server.");
					/* Listen on ipv4 separately if dual stack is not supported */
					ipv4_udp_server_start(self);

			 } 
			else
				MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Started dual-stack udp server.");				
		}
		else{
			uv_err_t uv_err = uv_last_error(self->uv_loop);						
			MA_LOG(self->logger, MA_LOG_SEV_WARNING, "ipv6 uv_udp_recv_start - libuv err: (\'%s\' %s)", uv_strerror(uv_err), uv_err_name(uv_err));							
			ipv4_udp_server_start(self);
		}
	}
	else{
		uv_err_t uv_err = uv_last_error(self->uv_loop);
		MA_LOG(self->logger, MA_LOG_SEV_WARNING, "Unable to start dual-stack udp server - libuv err: (\'%s\' %s)", uv_strerror(uv_err), uv_err_name(uv_err));				
		ipv4_udp_server_start(self);
	}
}


ma_error_t ma_udp_server_start(ma_udp_server_t *self){
	if(self){
		if(MA_UDP_SERVER_STATE_STARTED == self->state) return MA_OK;
		
		if(self->policy.is_enabled){
			if(self->policy.port){			
		
				MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Starting udp server on port %d", self->policy.port);
				udp_server_start(self);

				if(MA_UDP_SERVER_STATE_STARTED == self->state){
					ma_udp_connection_manager_start(self->connection_mgr);
					MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "udp server started on port %d", self->policy.port);
					return MA_OK;
				}
				else{
					MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to start udp server on the port %d", self->policy.port);
					return MA_ERROR_UNEXPECTED;
				}
			}
			else{
				MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Port si not set for udp server to start.");
				return MA_ERROR_PRECONDITION;
			}
		}
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

static void udp_handle_close_cb(uv_handle_t* handle){
	ma_udp_server_t *self = (ma_udp_server_t*) handle->data;	
	MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "udp server handle <%p> closed.", handle);	
	handle->type = UV_UNKNOWN_HANDLE;
	(void)ma_udp_server_release(self);
}

ma_error_t ma_udp_server_stop(ma_udp_server_t *self){
	if(self){
		MA_LOG(self->logger, MA_LOG_SEV_INFO, "Stopping the udp server");
		if(UV_UDP == self->v4_handle.type) uv_close((uv_handle_t*) &self->v4_handle, udp_handle_close_cb);

		if(UV_UDP == self->v6_handle.type) {
			MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Dropping the udp server multicast membership.");
			set_mulitcast_options(self, MA_FALSE);
			uv_close((uv_handle_t*) &self->v6_handle, udp_handle_close_cb);
		}
		
		MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Stopping the udp server connection manager.");
		(void)ma_udp_connection_manager_stop(self->connection_mgr);

		self->state  = MA_UDP_SERVER_STATE_STOPPED;
		return MA_OK;		
	}
	return MA_ERROR_INVALIDARG;
}


ma_error_t ma_udp_server_register_msg_handler(ma_udp_server_t *self, ma_udp_msg_handler_t *handler){
	if(self && handler){
		return ma_udp_msg_handler_list_add(&self->msg_handlers, handler);
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_udp_server_unregister_msg_handler(ma_udp_server_t *self, ma_udp_msg_handler_t *handler){
	if(self && handler){
		return ma_udp_msg_handler_list_remove(&self->msg_handlers, handler);
	}
	return MA_ERROR_INVALIDARG;
}


static ma_error_t set_mulitcast_options(ma_udp_server_t *self, ma_bool_t add){
	ma_error_t rc = MA_OK;
	ma_net_interface_list_t *list = get_net_interface_list(self);
	
    if(list) {
	    if(list->interfaces_count) {
		    ma_net_interface_t *it_if = NULL;
		    MA_SLIST_FOREACH(list, interfaces, it_if){
			    /*create a multicaster only for ipv6 and dual nic*/			
			    if(it_if->family == MA_IPFAMILY_IPV6 || it_if->family == MA_IPFAMILY_DUAL){
				    ma_net_address_info_t  *addr = NULL;

				    MA_SLIST_FOREACH(it_if, addresses, addr){
					    /*look for the link locak address*/
					    if(!strncmp(addr->ip_addr, "fe80:", strlen("fe80:")) || !strncmp(addr->ip_addr, "FE80:", strlen("FE80:"))){
						    MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "%s udp multicast socket option at nic %s with scope id %u.", add ? "Setting" : "Droping",  it_if->interface_name, addr->ipv6_scope_id);
						    set_v6_server_multicast_membership(&self->v6_handle, self->policy.multicast_addr, addr->ipv6_scope_id, add, self->logger);
						    break;
					    }
				    }
			    }
		    }		
	    }
	    (void) ma_net_interface_list_release(list);
    }
    else
        MA_LOG(self->logger, MA_LOG_SEV_WARNING, "Failed to set multicast option");

	return rc;
}

static ma_net_interface_list_t *get_net_interface_list(ma_udp_server_t *self) {
	if(self) {
		ma_net_interface_list_t *list = NULL;
		if(MA_OK == ma_net_interface_list_create(&list)) {
			if(MA_OK == ma_net_interface_list_scan(list, MA_FALSE)) {
				return list;
			}
			(void)ma_net_interface_list_release(list);
			list = NULL;
		}
	}
	return NULL;
}

void ma_udp_server_policy_init(ma_udp_server_policy_t *policy, ma_bool_t  is_enabled, char	*multicast_addr, ma_uint16_t port){
	policy->is_enabled = is_enabled;
	policy->multicast_addr = multicast_addr ? strdup(multicast_addr) : NULL;
	policy->port = port;
}

void ma_udp_server_policy_deinit(ma_udp_server_policy_t *policy){
	policy->is_enabled = MA_FALSE;
	policy->port = 0;
	if(policy->multicast_addr) free(policy->multicast_addr);
	policy->multicast_addr = NULL;
}

void ma_udp_server_policy_copy(ma_udp_server_policy_t *dest, ma_udp_server_policy_t *src){
	dest->is_enabled = src->is_enabled;
	dest->multicast_addr = src->multicast_addr ? strdup(src->multicast_addr) : NULL;
	dest->port = src->port;
}
