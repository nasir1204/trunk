#include "ma/ma_common.h"

#include <stdio.h>
#ifndef MA_WINDOWS
#include <sys/types.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <errno.h>
#else
#include <ws2tcpip.h>
#endif

#ifdef MA_WINDOWS

//include headers
#include <winsock2.h>
#include <windows.h>
#include <ws2tcpip.h>
#include <string.h>

//inetaddress length
#define INET_ADDRSTRLEN                       22
#define INET6_ADDRSTRLEN                      65

#else
#include <netinet/in.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <stdio.h>
#define INVALID_SOCKET -1
#define SOCKET_ERROR   -1
#ifndef NULL
#define NULL 0
#endif
#endif

#include "ma_udp_utils.h"
#include "ma/internal/ma_macros.h"

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "udp_server"

#ifndef IPV6_ADD_MEMBERSHIP
#define IPV6_ADD_MEMBERSHIP  IPV6_JOIN_GROUP
#endif
#ifndef IPV6_DROP_MEMBERSHIP
#define IPV6_DROP_MEMBERSHIP IPV6_LEAVE_GROUP
#endif

#ifdef USE_IP4_MREQ_DEFINITION
struct ip_mreq 
{
      struct in_addr imr_multiaddr;
      struct in_addr imr_interface;
};
#endif

/* For HPUX 11.23 the following definition is enabled in netinet/in.h only when _XOPEN_SOURCE_EXTENDED macro is not defined.
    But this macro is already in Generated compiler predefines */
#if defined(HPUX) && defined(HPUX_IP_MREQ_HACK)
struct ip_mreq {
	struct in_addr	imr_multiaddr;	/* IP multicast address of group */
	struct in_addr	imr_interface;	/* local IP address of interface */
};
#endif /* defined(HPUX) && defined(HPUX_IP_MREQ_HACK) */

union ip_mreq_u{
	struct ip_mreq mreq;
	struct ipv6_mreq mreq6;
};

static ma_error_t get_multicast_req(ma_bool_t is_ipv6, struct ip_mreq *mreq, struct ipv6_mreq *mreq6, const char *multicast_adddr, unsigned long interface_scopeid){
	struct addrinfo hints , *res;
		
	memset(&hints, 0, sizeof(struct addrinfo));		
	hints.ai_family   = AF_UNSPEC;
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_flags = AI_PASSIVE;

	if(0 == getaddrinfo(multicast_adddr, NULL , &hints , &res)){
		if(is_ipv6){
			mreq6->ipv6mr_interface = interface_scopeid;			
			memcpy(&mreq6->ipv6mr_multiaddr, &((struct sockaddr_in6*)(res->ai_addr))->sin6_addr,sizeof(mreq6->ipv6mr_multiaddr));
		}
		else{
			mreq->imr_interface.s_addr = htonl(INADDR_ANY);			
			memcpy(&mreq->imr_multiaddr, &((struct sockaddr_in*)(res->ai_addr))->sin_addr,sizeof(mreq->imr_multiaddr));			
		}		
		freeaddrinfo(res);
		return MA_OK;

	}
	return MA_ERROR_APIFAILED;				
}

ma_error_t ma_setsocketoption(uv_udp_t *handle, int level, int optname, const char* optval, int optlen, ma_logger_t *logger){
	int ret=0;
	ret = setsockopt(MA_WIN_SELECT(handle->socket,handle->io_watcher.fd), level, optname, optval, optlen);
	if(0 != ret){
		#ifdef WIN32
		MA_LOG(logger, MA_LOG_SEV_WARNING, "setsockopt(,%d,%d,) returned=%d WSAGetLastError=%d", level, optname, ret, WSAGetLastError());;
		#else
		MA_LOG(logger, MA_LOG_SEV_WARNING, "setsockopt(,%d,%d,) returned=%d ", level, optname, errno);;
		#endif		
		return MA_ERROR_APIFAILED;
	}
	return MA_OK;	
}

ma_error_t set_v6_server_multicast_membership(uv_udp_t *handle, const char *multicast_address, unsigned long interface_scopeid, ma_bool_t add, ma_logger_t *logger)
{
	ma_error_t rc = MA_OK;		
	union ip_mreq_u mreq;

	if(INVALID_SOCKET == MA_WIN_SELECT(handle->socket,handle->io_watcher.fd)) return MA_ERROR_INVALIDARG;
	if(multicast_address){
		memset(&mreq, 0, sizeof(mreq));
		if(MA_OK == (rc = get_multicast_req(MA_TRUE, &mreq.mreq, &mreq.mreq6, multicast_address, interface_scopeid)))
			return ma_setsocketoption(handle,IPPROTO_IPV6, add ? IPV6_ADD_MEMBERSHIP : IPV6_DROP_MEMBERSHIP, (char*)&mreq.mreq6, sizeof(mreq.mreq6), logger);	
	}
	return rc;
}


