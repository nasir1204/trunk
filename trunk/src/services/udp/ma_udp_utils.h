#ifndef MA_UDP_UTILS_H
#define MA_UDP_UTILS_H

#include "ma/ma_common.h"
#include "ma/ma_errors.h"
#include "ma/ma_log.h"

#include "uv.h"

//typedef unsigned long  ulong_t;

MA_CPP(extern "C" {)

ma_error_t set_v6_server_multicast_membership(uv_udp_t *handle, const char *multicast_address, unsigned long interface_scopeid, ma_bool_t add, ma_logger_t *logger);

MA_CPP(})

#endif 