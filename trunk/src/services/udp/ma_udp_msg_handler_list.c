#include "ma_udp_msg_handler_list.h"

#include <stdio.h>
#include <stdlib.h>


ma_error_t ma_udp_msg_handler_list_init(ma_udp_msg_handler_list_t *self){
	if(self){
		ma_queue_init(&self->qhead);	
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_udp_msg_handler_list_add(ma_udp_msg_handler_list_t *self, ma_udp_msg_handler_t *handler){
	if(self && handler){
		ma_udp_msg_handler_info_t *info = (ma_udp_msg_handler_info_t*) calloc(1, sizeof(ma_udp_msg_handler_info_t));
		if(info){
			ma_udp_msg_handler_add_ref(info->msg_handler = handler);
			ma_queue_insert_tail(&self->qhead, &info->qnode);
			return MA_OK;
		}
		return MA_ERROR_OUTOFMEMORY;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_udp_msg_handler_list_remove(ma_udp_msg_handler_list_t *self, ma_udp_msg_handler_t *handler){
	if(self && handler){
		ma_queue_t *info = NULL;
		ma_queue_foreach(info, &self->qhead){			
			if(((ma_udp_msg_handler_info_t*)info)->msg_handler ==  handler){
				ma_queue_remove(info);				
				ma_udp_msg_handler_release(handler);
				free(info);
				break;
			}
		}
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_udp_msg_handler_list_deinit(ma_udp_msg_handler_list_t *self){
	if(self){
		while(!ma_queue_empty(&self->qhead)) {
			ma_udp_msg_handler_t *handler = NULL;
			ma_queue_t *info = ma_queue_last(&self->qhead);		
			ma_queue_remove(info);	
			handler = ((ma_udp_msg_handler_info_t*)info)->msg_handler;
			ma_udp_msg_handler_release(handler);					
			free(info); info = NULL;
		}
		ma_queue_init(&self->qhead);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;

}