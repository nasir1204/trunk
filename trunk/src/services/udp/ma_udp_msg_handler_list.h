#ifndef MA_UDP_MSG_HANDLER_LIST_H_INCLUDED
#define MA_UDP_MSG_HANDLER_LIST_H_INCLUDED

#include "ma/internal/services/udp_server/ma_udp_msg_handler.h"

#include "ma/internal/utils/datastructures/ma_queue.h"

MA_CPP(extern "C" {)

typedef struct ma_udp_msg_handler_list_s ma_udp_msg_handler_list_t;
typedef struct ma_udp_msg_handler_info_s ma_udp_msg_handler_info_t;

struct ma_udp_msg_handler_info_s{
	/*make it q compatible*/
	ma_queue_t					 qnode;

	/*udp msg handler*/
	ma_udp_msg_handler_t		*msg_handler;		
};

struct ma_udp_msg_handler_list_s{
	/*handler list head node.*/
	ma_queue_t				qhead;	
};

ma_error_t ma_udp_msg_handler_list_init(ma_udp_msg_handler_list_t *qhandlers);

ma_error_t ma_udp_msg_handler_list_add(ma_udp_msg_handler_list_t *qhandlers, ma_udp_msg_handler_t *handler);

ma_error_t ma_udp_msg_handler_list_remove(ma_udp_msg_handler_list_t *qhandlers, ma_udp_msg_handler_t *handler);

ma_error_t ma_udp_msg_handler_list_deinit(ma_udp_msg_handler_list_t *qhandlers);

MA_CPP(})

#endif