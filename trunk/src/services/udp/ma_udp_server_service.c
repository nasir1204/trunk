#include "ma/internal/services/udp_server/ma_udp_server_service.h"
#include "ma/internal/services/udp_server/ma_udp_server.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/services/ma_agent_policy_settings_bag.h" 
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/defs/ma_relay_defs.h"

#include "ma/internal/services/udp_server/ma_udp_global_update_handler.h"
#include "ma/internal/services/relay/ma_relay_discovery_handler.h"
#include "ma/internal/services/relay/ma_compat_relay_discovery_handler.h"
#include "ma/internal/services/p2p/ma_p2p_discovery_hanlder.h"

#include "ma_udp_agent_wakeup_handler.h"

#include "ma/ma_msgbus.h"
#include "ma/internal/core/msgbus/ma_msgbus_private.h"
#include "ma/ma_log.h"

#include <stdio.h>
#include <stdlib.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "udp_server"

typedef struct ma_udp_server_service_s {

    ma_service_t                        base;

	ma_context_t                        *context;

	ma_logger_t                         *logger;
	
	ma_msgbus_t                         *msgbus;

	ma_udp_server_t                     *udp_server;
		
	/*comapt relay discovery server*/
	ma_service_t						*relay_compat;

	/*relay handlers.*/
	ma_udp_msg_handler_t				*relay_handler;

	ma_udp_msg_handler_t				*relay_compat_handler;

	ma_udp_msg_handler_t				*p2p_handler;

	ma_udp_msg_handler_t				*wakeup_handler;

	ma_udp_msg_handler_t				*global_update_handler;

} ma_udp_server_service_t;

static ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint);

static ma_error_t service_start(ma_service_t *service);

static ma_error_t service_stop(ma_service_t *service);

static void service_destroy(ma_service_t *service);

static const ma_service_methods_t methods = {
    &service_configure,
    &service_start,
    &service_stop,
    &service_destroy
};	

ma_error_t ma_udp_server_service_create(const char *service_name, ma_service_t **udp_service){
	if(service_name && udp_service){

		ma_udp_server_service_t *udp_s = (ma_udp_server_service_t*) calloc(1, sizeof(ma_udp_server_service_t));
		if(udp_s){
			if(MA_OK == ma_compat_relay_server_service_create(MA_RELAY_SERVICE_NAME_STR, &udp_s->relay_compat)){
				ma_service_init(&udp_s->base, &methods, service_name, (void *)udp_s) ;
				*udp_service = (ma_service_t*)udp_s;
				return MA_OK;
			}
			free(udp_s);
		}
		return MA_ERROR_OUTOFMEMORY;
	}
	return MA_ERROR_INVALIDARG;	
}

static ma_error_t setup_udp_server(ma_udp_server_service_t *self, ma_udp_server_policy_t *policy){
	ma_error_t rc = MA_OK;
	MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Setting up udp discovery server at port <%d> multicast address <%s>.", policy->port, policy->multicast_addr);		
	if(MA_OK == (rc = ma_udp_server_create(&self->udp_server))){
		ma_udp_server_set_logger(self->udp_server, self->logger);	
		ma_udp_server_set_event_loop(self->udp_server, ma_msgbus_get_event_loop(self->msgbus));													
		ma_udp_server_set_policy(self->udp_server, policy);						

		if(MA_OK == ma_relay_discovery_handler_create(&self->relay_handler)){
			(void)ma_relay_discovery_handler_configure(self->relay_handler, self->context);
			(void)ma_udp_server_register_msg_handler(self->udp_server, self->relay_handler);					
		}
		else{
			MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to configure the relay discovery handler.");						
			self->relay_handler = NULL;
		}
					
		if(MA_OK == ma_compat_relay_discovery_handler_create(&self->relay_compat_handler)){
			(void)ma_compat_relay_discovery_handler_configure(self->relay_compat_handler, self->context);
			(void)ma_udp_server_register_msg_handler(self->udp_server, self->relay_compat_handler);					
		}
		else{
			MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to create the compat relay discovery handler.");	
			self->relay_compat_handler = NULL;
		}

		if(MA_OK == ma_p2p_discovery_handler_create(self->context, &self->p2p_handler)){
			(void)ma_p2p_discovery_handler_configure(self->p2p_handler, self->context) ;
			(void)ma_udp_server_register_msg_handler(self->udp_server, self->p2p_handler);					
		}
		else{
			MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to create p2p discovery handler.") ;														
			self->p2p_handler = NULL;
		}
				
		if(MA_OK == ma_udp_agent_wakeup_handler_create(self->context, &self->wakeup_handler)){
			(void)ma_udp_server_register_msg_handler(self->udp_server, self->wakeup_handler);					
		}
		else{
			MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to create the agent wakeup handler.");														
			self->wakeup_handler = NULL;
		}

		if(MA_OK == ma_udp_global_update_handler_create(self->context, &self->global_update_handler)){
			(void)ma_udp_server_register_msg_handler(self->udp_server, self->global_update_handler);					
		}
		else{
			MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to create the global update handler.");														
			self->global_update_handler = NULL;
		}

	}	
	else
		MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to create udp server.");							
	return rc;	
}

static ma_error_t cleanup_udp_server(ma_udp_server_service_t *self){
	if(self->udp_server){
		MA_LOG(self->logger, MA_LOG_SEV_INFO, "Stopping udp discovery server.");		
		if(self->relay_handler){		
			(void)ma_udp_server_unregister_msg_handler(self->udp_server, self->relay_handler);			
			ma_udp_msg_handler_release(self->relay_handler);
			self->relay_handler = NULL;
		}

		if(self->relay_compat_handler){		
			(void)ma_udp_server_unregister_msg_handler(self->udp_server, self->relay_compat_handler);			
			ma_udp_msg_handler_release(self->relay_compat_handler);
			self->relay_compat_handler = NULL;
		}

		if(self->p2p_handler){		
			(void)ma_udp_server_unregister_msg_handler(self->udp_server, self->p2p_handler);			
			ma_udp_msg_handler_release(self->p2p_handler);
			self->p2p_handler = NULL;
		}

		if(self->wakeup_handler){		
			(void)ma_udp_server_unregister_msg_handler(self->udp_server, self->wakeup_handler);			
			ma_udp_msg_handler_release(self->wakeup_handler);
			self->wakeup_handler = NULL;
		}

		if(self->global_update_handler){		
			(void)ma_udp_server_unregister_msg_handler(self->udp_server, self->global_update_handler);			
			ma_udp_msg_handler_release(self->global_update_handler);
			self->global_update_handler = NULL;
		}
		
		(void)ma_udp_server_stop(self->udp_server);
		(void)ma_udp_server_release(self->udp_server);
		self->udp_server = NULL;
	}
	return MA_OK;
}

void get_udp_server_policy(ma_policy_settings_bag_t *pso_bag, ma_udp_server_policy_t *new_policy){
	ma_int32_t is_enabled = 0;	
	ma_int32_t port = MA_UDP_DEFAULT_PORT_INT;

	ma_policy_settings_bag_get_int(pso_bag, MA_UDP_SERVICE_SECTION_NAME_STR, MA_UDP_KEY_IS_ENABLED_INT, MA_FALSE,&is_enabled, 1) ;
	new_policy->is_enabled = is_enabled ? MA_TRUE : MA_FALSE;	
	ma_policy_settings_bag_get_int(pso_bag, MA_UDP_SERVICE_SECTION_NAME_STR, MA_UDP_KEY_BROADCAST_PORT_INT, MA_FALSE, &port, port) ;
	new_policy->port = port;
	ma_policy_settings_bag_get_str(pso_bag, MA_UDP_SERVICE_SECTION_NAME_STR, MA_UDP_KEY_MULTICAST_ADDRESS_STR, MA_FALSE,&new_policy->multicast_addr, MA_UDP_MULTICAST_ADDR_STR) ;		
}

static ma_error_t configure_udp_server(ma_udp_server_service_t *self, ma_policy_settings_bag_t *pso_bag){
	ma_error_t rc = MA_OK;
	ma_udp_server_policy_t udp_policy = {0};
	get_udp_server_policy(pso_bag, &udp_policy);
	if(udp_policy.is_enabled) /*if enabled.*/
		rc = setup_udp_server(self, &udp_policy);	
	return rc;
}

static ma_bool_t is_policy_change(ma_udp_server_service_t *self, ma_udp_server_policy_t *new_policy){
	if(self->udp_server){
		ma_udp_server_policy_t *old_policy = NULL;
		ma_udp_server_get_policy(self->udp_server, &old_policy);
		if(old_policy->is_enabled == new_policy->is_enabled && old_policy->port == new_policy->port && (0 == strcmp(new_policy->multicast_addr ? new_policy->multicast_addr : "", old_policy->multicast_addr ? old_policy->multicast_addr : "")))
			return MA_FALSE;		
	}
	return MA_TRUE;
}

static ma_error_t update_firewall_rule(ma_udp_server_service_t *self) {
	ma_msgbus_endpoint_t *ep = NULL;
	ma_error_t rc = MA_OK;
	if(MA_OK == (rc = ma_msgbus_endpoint_create(self->msgbus, MA_IO_SERVICE_NAME_STR, NULL, &ep))) {
		ma_message_t *req = NULL;
		(void) ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
		(void)ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
		if(MA_OK == (rc = ma_message_create(&req))) {
			(void) ma_message_set_property(req, MA_IO_SERVICE_MSG_TYPE, MA_IO_MSG_TYPE_FIREWALL_RULE_CHANGED_STR);
			rc = ma_msgbus_send_and_forget(ep, req);
			(void) ma_message_release(req);
		}
		(void) ma_msgbus_endpoint_release(ep);
	}
	return rc;
}

static ma_error_t reconfigure_udp_server(ma_udp_server_service_t *self, ma_policy_settings_bag_t *pso_bag){
	ma_error_t rc = MA_OK;
	if(self->udp_server){		
		ma_udp_server_policy_t udp_policy = {0};	
		get_udp_server_policy(pso_bag, &udp_policy);

		if(is_policy_change(self, &udp_policy)){
			(void)cleanup_udp_server(self);
			if(udp_policy.is_enabled){
				if(MA_OK == setup_udp_server(self, &udp_policy)){
					if(MA_OK == (rc = ma_udp_server_start(self->udp_server)))
						if(MA_OK != (rc = update_firewall_rule(self)))
							MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to update firewall rule on udp policy change, rc = %d", rc);
					return rc;
				}
			}		
		}		
	}
	else{		
		if(MA_OK == (rc = configure_udp_server(self, pso_bag))){
			if(self->udp_server)
				rc = ma_udp_server_start(self->udp_server);
		}				
	}
	return rc;
}


static ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint) {
    if(service && context){
		ma_error_t rc = MA_OK;
		ma_udp_server_service_t *self = (ma_udp_server_service_t*)service;
		ma_policy_settings_bag_t *pso_bag = MA_CONTEXT_GET_POLICY_SETTINGS_BAG(context) ;

		if(MA_SERVICE_CONFIG_NEW == hint){
			self->msgbus = MA_CONTEXT_GET_MSGBUS(context);
			self->logger = MA_CONTEXT_GET_LOGGER(context);
			self->context = context;
			if(self->msgbus && self->logger){				
				if(MA_OK == (rc = ma_service_configure(self->relay_compat, context, hint))){
					if(MA_OK != (rc = configure_udp_server(self, pso_bag))){
						MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to configure udp server.");											
					}				
				}				
				return rc;
			}
			return MA_ERROR_PRECONDITION;
		}
		else{			
			if(MA_OK == (rc = ma_service_configure(self->relay_compat, context, hint))){
				if(MA_OK != (rc = reconfigure_udp_server(self, pso_bag))){
					MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to reconfigure udp server.");											
				}
			}
			if(self->relay_handler) 
				(void)ma_relay_discovery_handler_configure(self->relay_handler, self->context);
			
			if(self->relay_compat_handler) 
				(void)ma_relay_discovery_handler_configure(self->relay_compat_handler, self->context);
			
			if(self->p2p_handler) 
				(void)ma_p2p_discovery_handler_configure(self->p2p_handler, self->context) ;
		}
		return rc ;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t service_start(ma_service_t *service) {
    if(service){
		ma_error_t rc = MA_OK;
		ma_udp_server_service_t *self = (ma_udp_server_service_t*)service;     
		
		if(MA_OK == (rc = ma_service_start(self->relay_compat))){
			if(self->udp_server){			
				if(MA_OK == (rc = ma_udp_server_start(self->udp_server))){				
					MA_LOG(self->logger, MA_LOG_SEV_INFO, "udp server started.");											
				}
				else
					MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to start udp server.");			
			}
		}
		return rc;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t service_stop(ma_service_t *service) {
    if(service){
		ma_error_t rc = MA_OK;
		ma_udp_server_service_t *self = (ma_udp_server_service_t*)service;     
				
		if(self->udp_server){			
			if(MA_OK == (rc = ma_udp_server_stop(self->udp_server))){												
				MA_LOG(self->logger, MA_LOG_SEV_INFO, "udp server stopped.");							
			}
			else
				MA_LOG(self->logger, MA_LOG_SEV_ERROR, "udp server stop failed.");			
		}
		if(MA_OK == rc)
			ma_service_stop(self->relay_compat);
		return rc;
    }
    return MA_ERROR_INVALIDARG;
}

static void service_destroy(ma_service_t *service) {
    if(service){
		ma_udp_server_service_t *self = (ma_udp_server_service_t*)service;  		
		(void)cleanup_udp_server(self);		
		ma_service_release(self->relay_compat);
		free(self);		
    }
}

