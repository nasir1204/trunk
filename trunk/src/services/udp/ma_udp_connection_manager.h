#ifndef MA_UDP_CONNECTION_MANAGER_H_INCLUDED
#define MA_UDP_CONNECTION_MANAGER_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/services/udp_server/ma_udp_connection.h"

#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/ma_log.h"
#include "uv.h"

MA_CPP(extern "C" {)

typedef struct ma_udp_connection_manager_s ma_udp_connection_manager_t;

ma_error_t ma_udp_connection_manager_create(ma_udp_connection_manager_t **self);

ma_error_t ma_udp_connection_manager_set_loop(ma_udp_connection_manager_t *self, struct ma_event_loop_s *loop);

ma_error_t ma_udp_connection_manager_set_logger(ma_udp_connection_manager_t *self, struct ma_logger_s *logger);

ma_error_t ma_udp_connection_manager_start(ma_udp_connection_manager_t *self);

ma_error_t ma_udp_connection_manager_stop(ma_udp_connection_manager_t *self);

ma_error_t ma_udp_connection_manager_manage_connection(ma_udp_connection_manager_t *self, uv_udp_t* receiving_handle, struct sockaddr* addr, uv_buf_t received_buf, size_t nread, ma_udp_connection_t **connection);

ma_error_t ma_udp_connection_manager_process_connection(ma_udp_connection_manager_t *self, ma_udp_connection_t *connection);

ma_error_t ma_udp_connection_manager_remove_connection(ma_udp_connection_manager_t *self, ma_udp_connection_t *connection);

ma_error_t ma_udp_connection_manager_release(ma_udp_connection_manager_t *self);

MA_CPP(})

#endif