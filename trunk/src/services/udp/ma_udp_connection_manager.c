#include "ma_udp_connection_manager.h"

#include "ma/internal/utils/datastructures/ma_queue.h"
#include "ma/internal/utils/platform/ma_sys_apis.h"

#include "ma/internal/ma_macros.h"

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "udp_server"

struct ma_udp_connection_manager_s{
	ma_queue_t       connections;

	uv_loop_t		*uv_loop;
		
	ma_logger_t     *logger; 
};

struct ma_udp_connection_s{
	ma_queue_t						connection_node;

	ma_udp_msg_t					*request_msg;
	
	ma_udp_msg_t					*response_msg;
	
	ma_bytebuffer_t                 *serialized_response;

	ma_bool_t						delay_response;

	ma_uint64_t						delay_timeout_ms;
		
	uv_timer_t						delay_timer;

	uv_udp_t						*receiving_handle;

	struct sockaddr_storage			peer_addr;

	uv_udp_send_t					response_handle;

	ma_udp_connection_manager_t		*connection_manager;
};

ma_error_t ma_udp_connection_create(ma_udp_connection_manager_t	*connection_manager, ma_udp_connection_t **self){
	if(connection_manager && self){
		ma_udp_connection_t *tmp = (ma_udp_connection_t*) calloc(1, sizeof(ma_udp_connection_t));
		if(tmp){			
			tmp->connection_manager		= connection_manager;
			tmp->delay_timer.data		= tmp;
			tmp->response_handle.data	= tmp;
            
            MA_LOG(connection_manager->logger, MA_LOG_SEV_DEBUG, "uv timer data(%p)\n", tmp);
			uv_timer_init(tmp->connection_manager->uv_loop, &tmp->delay_timer);			
			*self = tmp;
			return MA_OK;
		}
		return MA_ERROR_OUTOFMEMORY;
	}
	return MA_ERROR_INVALIDARG;
}

static void timer_close_cb(uv_handle_t* handle){
	ma_udp_connection_t *self = (ma_udp_connection_t*) handle->data;	
	MA_LOG(self->connection_manager->logger, MA_LOG_SEV_DEBUG, "Destroying the udp connection <%p>.", self);			
	free(self);	
}

ma_error_t ma_udp_connection_release(ma_udp_connection_t *self){
	if(self){
		MA_LOG(self->connection_manager->logger, MA_LOG_SEV_DEBUG, "Releasing the udp connection <%p> resources.", self);			
		uv_timer_stop(&self->delay_timer);		
		if(self->request_msg) ma_udp_msg_release(self->request_msg);
		if(self->response_msg) ma_udp_msg_release(self->response_msg);
		if(self->serialized_response) ma_bytebuffer_release(self->serialized_response);		
		uv_close((uv_handle_t*)&self->delay_timer, timer_close_cb);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_udp_connection_set_request_msg(ma_udp_connection_t *self, uv_udp_t* receiving_handle, struct sockaddr* peer_addr, const unsigned char *msg, size_t size){
	if(self && receiving_handle && msg && size){
		struct sockaddr_storage	sa;
		int namelen	= sizeof(sa);		
		
		MA_LOG(self->connection_manager->logger, MA_LOG_SEV_DEBUG, "Extracting the udp connection <%p> request message.", self);			

		if(UV_OK == uv_udp_getsockname(receiving_handle, (struct sockaddr *) &sa, &namelen)){			

			/*set the peer address*/
			if(AF_INET6 == sa.ss_family)
				memcpy(&self->peer_addr, peer_addr, sizeof(struct sockaddr_in6));
			else
				memcpy(&self->peer_addr, peer_addr, sizeof(struct sockaddr_in));			

			/*form the request message.*/
			if(MA_OK == ma_udp_msg_deserialize(msg, size, &self->request_msg)){
				self->receiving_handle = receiving_handle;
				MA_LOG(self->connection_manager->logger, MA_LOG_SEV_DEBUG, "Deserialize the received udp message.");			
				return MA_OK;
			}
			MA_LOG(self->connection_manager->logger, MA_LOG_SEV_ERROR, "Failed to deserialize the udp message, malformed message.");			
		}
		else {
			uv_err_t uv_err = uv_last_error(self->connection_manager->uv_loop);
			MA_LOG(self->connection_manager->logger, MA_LOG_SEV_WARNING, "uv_udp_getsockname error - libuv err: (\'%s\' %s)", uv_strerror(uv_err), uv_err_name(uv_err));
		}
		return MA_ERROR_UNEXPECTED;
	}
	return MA_ERROR_INVALIDARG;
}


ma_error_t ma_udp_connection_get_request_msg(ma_udp_connection_t *self, ma_udp_msg_t **request_msg){
	if(self && request_msg){
		if(self->request_msg){
			ma_udp_msg_add_ref( *request_msg = self->request_msg);
			return MA_OK;
		}
		return MA_ERROR_UNEXPECTED;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_udp_connection_set_delayed_response(ma_udp_connection_t *self, ma_bool_t delayed_response){
	if(self){
		self->delay_response = delayed_response;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_udp_connection_set_response_delay_timeout(ma_udp_connection_t *self, ma_uint64_t delay_time_ms){
	if(self){
		self->delay_timeout_ms = delay_time_ms;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

static void post_response_cb(uv_udp_send_t *response_handler, int status) {
	if(response_handler && 0 == status && response_handler->handle && response_handler->data) {
		ma_udp_connection_t *self = (ma_udp_connection_t *)response_handler->data;
		ma_udp_connection_manager_remove_connection(self->connection_manager, self);
	}
}

static void post_response(uv_timer_t* handle, int status) {
	if(handle && 0 == status && handle->data) {
		ma_udp_connection_t *self = (ma_udp_connection_t *)handle->data;

		MA_LOG(self->connection_manager->logger, MA_LOG_SEV_DEBUG, "Sending response for the udp connection <%p>.", self);

		if(self->response_msg){
		
			if(MA_OK == ma_udp_msg_serialize(self->response_msg, &self->serialized_response) && self->serialized_response){
				uv_buf_t reply_buf			= {0};
				int ret						= 0;		

				reply_buf					= uv_buf_init((char*)ma_bytebuffer_get_bytes(self->serialized_response), ma_bytebuffer_get_size(self->serialized_response));

				if(AF_INET6 == self->peer_addr.ss_family)
					ret = uv_udp_send6(&self->response_handle, self->receiving_handle, &reply_buf, 1, *(struct sockaddr_in6*)&self->peer_addr, post_response_cb);
				else
					ret = uv_udp_send(&self->response_handle, self->receiving_handle, &reply_buf, 1, *(struct sockaddr_in*)&self->peer_addr, post_response_cb);

				if(UV_OK == ret) {
					MA_LOG(self->connection_manager->logger, MA_LOG_SEV_DEBUG, "Posted the response successfully.");
					return;
				}
				else{
					uv_err_t uv_err = uv_last_error(self->connection_manager->uv_loop);
					MA_LOG(self->connection_manager->logger, MA_LOG_SEV_WARNING, "Error post response - libuv err: (\'%s\' %s)", uv_strerror(uv_err), uv_err_name(uv_err));
				}				
			}
			else{
				MA_LOG(self->connection_manager->logger, MA_LOG_SEV_WARNING, "Failed to serialize udp response message.");
			}				
		}
		/*if we reach here then we will remove the connection from the connection manager.*/
		ma_udp_connection_manager_remove_connection(self->connection_manager, self);
	}
}

ma_error_t ma_udp_connection_post_response(ma_udp_connection_t *self, ma_udp_msg_t *response){
	if(self){
		ma_udp_msg_add_ref( self->response_msg = response);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}


ma_error_t ma_udp_connection_manager_create(ma_udp_connection_manager_t **self){
	if(self){
		ma_udp_connection_manager_t *mgr = (ma_udp_connection_manager_t*) calloc(1, sizeof(ma_udp_connection_manager_t));
		if(mgr){
			ma_queue_init(&mgr->connections);
			*self = mgr;
			return MA_OK;
		}
		return MA_ERROR_OUTOFMEMORY;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_udp_connection_manager_set_loop(ma_udp_connection_manager_t *self, struct ma_event_loop_s *loop){
	if(self && loop){
		self->uv_loop = ma_event_loop_get_uv_loop(loop);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_udp_connection_manager_set_logger(ma_udp_connection_manager_t *self, struct ma_logger_s *logger){
	if(self && logger){
		self->logger = logger;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_udp_connection_manager_start(ma_udp_connection_manager_t *self){
	if(self){
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_udp_connection_manager_stop(ma_udp_connection_manager_t *self){
	if(self){
		MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Stopping udp connection manager.");
		while(!ma_queue_empty(&self->connections)){
			ma_queue_t *connection = ma_queue_last(&self->connections);		
			MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Removing udp connection <%p>.", connection);
			ma_queue_remove(connection);	
			ma_udp_connection_release( (ma_udp_connection_t*)connection );												
		}
		ma_queue_init(&self->connections);
		MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Stopped udp connection manager.");
		return MA_OK;
	}
	return MA_ERROR_OUTOFMEMORY;
}

ma_error_t ma_udp_connection_manager_manage_connection(ma_udp_connection_manager_t *self, uv_udp_t *receiving_handle, struct sockaddr* peer_addr, uv_buf_t received_buf, size_t nread, ma_udp_connection_t **connection){
	if(self && receiving_handle && peer_addr && received_buf.base && received_buf.len && connection){
		ma_error_t rc = MA_OK;
		ma_udp_connection_t *udp_c = NULL;

		MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Forming udp connection and adding it to connection manager.");
		
		if(MA_OK == (rc = ma_udp_connection_create(self, &udp_c))){
			if(MA_OK == (rc = ma_udp_connection_set_request_msg(udp_c, receiving_handle, peer_addr, (unsigned char*)received_buf.base, nread))){
				MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Adding udp connection <%p> to connection manager.", udp_c);
				ma_queue_insert_tail(&self->connections, (ma_queue_t*)udp_c);
				*connection = udp_c;
				return MA_OK;
			}
			ma_udp_connection_release(udp_c);
		}		
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_udp_connection_manager_process_connection(ma_udp_connection_manager_t *self, ma_udp_connection_t *connection){
	if(self && connection){
		if(connection->response_msg)
			uv_timer_start(&connection->delay_timer, post_response, (connection->delay_timeout_ms ? (ma_sys_rand() % (connection->delay_timeout_ms)) : 0), 0);
		else
			ma_udp_connection_manager_remove_connection(self, connection);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_udp_connection_manager_remove_connection(ma_udp_connection_manager_t *self, ma_udp_connection_t *connection){
	if(self && connection){
		MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Stopping and removing udp connection <%p>.", connection);
		if(!ma_queue_empty(&self->connections)){
			ma_queue_t *udp_c = NULL;		
			ma_queue_foreach(udp_c, &self->connections){			
				if((ma_queue_t*)connection == udp_c){
					MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Removing udp connection <%p>.", udp_c);
					ma_queue_remove(udp_c);	
					ma_udp_connection_release( (ma_udp_connection_t*)udp_c );					
					return MA_OK;
				}
			}			
		}
		MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Udp connection not found <%p>.", connection);
		return MA_OK;
	}
	return MA_ERROR_OUTOFMEMORY;
}

ma_error_t ma_udp_connection_manager_release(ma_udp_connection_manager_t *self){
	if(self){
		free(self);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}
