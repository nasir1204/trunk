#include "ma/internal/services/booking/ma_booking_service.h"
#include "ma/internal/utils/inventory/ma_inventory.h"
#include "ma/internal/utils/inventory/ma_inventory_info.h"
#include "ma/internal/utils/profiler/ma_profiler.h"
#include "ma/internal/utils/profiler/ma_profile_info.h"
#include "ma/internal/utils/context/ma_context.h"
#include "ma/internal/defs/ma_booking_defs.h"
#include "ma/internal/defs/ma_general_defs.h"
#include "ma/internal/services/scheduler/ma_task_defs.h"
#include "ma/internal/services/scheduler/ma_task_internal.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/utils/recorder/ma_recorder_info.h"
#include "ma/internal/utils/recorder/ma_recorder.h"
#include "ma/internal/utils/location/ma_location_info.h"
#include "ma/internal/utils/location/ma_location.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/utils/sms/ma_sms.h"
#include "ma/internal/services/ma_service.h"
#include "ma/ma_log.h"
#include "ma/ma_message.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/ma_variant.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/sensor/ma_sensor_msg.h"
#include "ma/sensor/ma_sensor_utils.h"
#include "ma/internal/utils/booker/ma_booker.h"
#include "ma/internal/utils/consignor/ma_consignor.h"
#include "ma/internal/utils/consignor/ma_consign_info.h"
#include "ma/scheduler/ma_task.h"
#include "ma/scheduler/ma_triggers.h"
#include "ma/scheduler/ma_enumerator.h"
#include "ma/scheduler/ma_triggerlist.h"
#include "ma/internal/services/scheduler/ma_scheduler_core.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MA_BOOKING_TASK_REPEAT_INTERVAL 30 /* minutes */
#define MA_BOOKING_TASK_REPEAT_DURATION 24 /* hours */
#define MA_BOOKING_TASK_MAX_RUN_TIME_LIMIT 2 /* minutes */
#define MA_BOOKING_TASK_START_DELAY  2 /* start */

const char base_path[] = "/booking/";

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "booking service"

MA_CPP(extern "C" {)
ma_error_t ma_mail_send(const char *from, const char *to, const char *sub, const char *body);
MA_CPP(})

struct ma_booking_service_s {
    ma_service_t            service_base;
    ma_context_t            *context;
    ma_msgbus_server_t      *server;
    ma_logger_t             *logger;
    ma_msgbus_subscriber_t  *subscriber;
    ma_booker_t             *booker;
};


static ma_bool_t check_booking_still_active(ma_booker_t *service, const char *booking_id);
static ma_error_t booking_subscriber_cb(const char *topic, ma_message_t *message, void *cb_data);
static ma_error_t ma_booking_service_cb(ma_msgbus_server_t *server, ma_message_t *msg, void *cb_data, ma_msgbus_client_request_t *c_request);
static ma_error_t consign_remove_complete(ma_consignor_t *consignor, const char *contact, const char *id);
static ma_error_t consign_remove_pending(ma_consignor_t *consignor, const char *contact, const char *id);

static ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint);
static ma_error_t service_start(ma_service_t *service);
static ma_error_t service_stop(ma_service_t *service);
static void service_release(ma_service_t *service);

static ma_booking_service_t *booking_service = NULL;

static const ma_service_methods_t service_methods = {
    &service_configure,
    &service_start,
    &service_stop,
    &service_release
};

ma_error_t ma_booking_service_create(const char *service_name, ma_service_t **service) {
    if(service_name && service) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;

        if((*service = (ma_service_t*)calloc(1, sizeof(ma_booking_service_t)))) {
            ma_service_init(*service, &service_methods, service_name, service);
            return MA_OK;
        }
        return err ;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_service_release(ma_booking_service_t *self) {
    if(self) {
        ma_error_t err = MA_OK;
        if(self->server && (MA_OK != (err = ma_msgbus_server_release(self->server)))) {
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "msgbus server release failed");
        }
        (void)ma_msgbus_subscriber_release(self->subscriber);
        free(self) ;
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_bool_t check_context_validity(ma_context_t *context){	
    /*Will see if we need this*/
    return MA_TRUE;
}

ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint) {
    if(service && context) {
        ma_error_t rc = MA_OK;
        if(MA_SERVICE_CONFIG_NEW == hint && check_context_validity(context)) {
            ma_configurator_t *configurator = MA_CONTEXT_GET_CONFIGURATOR(context);
            ma_booking_service_t *self = (ma_booking_service_t *)service;
            ma_msgbus_t *msgbus = MA_CONTEXT_GET_MSGBUS(context);

            booking_service = self;
            self->context = context;
            self->logger = MA_CONTEXT_GET_LOGGER(context);

            if(MA_OK == (rc = ma_booker_create(msgbus, ma_configurator_get_booking_datastore(configurator), MA_BOOKING_BASE_PATH_STR, &self->booker))) {
                (void)ma_booker_set_logger(self->logger);
                if(MA_OK == (rc = ma_msgbus_subscriber_create(MA_CONTEXT_GET_MSGBUS(context), &self->subscriber))) {
                    (void)ma_msgbus_subscriber_setopt(self->subscriber, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                    (void)ma_msgbus_subscriber_setopt(self->subscriber, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
                    if(MA_OK == (rc = ma_msgbus_server_create(msgbus, MA_BOOKING_SERVICE_NAME_STR, &self->server))) {                        
                        (void)ma_msgbus_server_setopt(self->server, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
                        if(MA_OK == (rc = ma_msgbus_server_setopt(self->server, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC))) {
                            ma_context_add_object_info(context, MA_OBJECT_BOOKER_NAME_STR, (void *const *)&self->booker);
                            MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_DEBUG, "booking service configuration successful");
                            return MA_OK;
                        }
                        ma_msgbus_server_release(self->server);
                    }
                    ma_msgbus_subscriber_release(self->subscriber);
                }
            }
            else
                MA_LOG(self->logger, MA_LOG_SEV_ERROR, "booker creation failed, last error(%d)", rc);
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t service_start(ma_service_t *service) {
    if(service) {
        ma_error_t err = MA_OK;
        ma_booking_service_t *self = (ma_booking_service_t *)service ;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);
        
        if(MA_OK == (err = ma_booker_start(self->booker))) {
            if(MA_OK == (err = ma_msgbus_server_start(((ma_booking_service_t*)service)->server, ma_booking_service_cb, service))) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "booking service started successfully");
                if(MA_OK == (err = ma_msgbus_subscriber_register(((ma_booking_service_t*)service)->subscriber,  "ma.task.*", booking_subscriber_cb, service))) {
                    MA_LOG(((ma_booking_service_t*)service)->logger, MA_LOG_SEV_DEBUG, "booking subscriber registered successfully");
                    return MA_OK ;
                } 
                else 
                    MA_LOG(((ma_booking_service_t*)service)->logger, MA_LOG_SEV_ERROR, "booking service failed to subscribe topic, errocode(%d)", err);
                ma_msgbus_server_stop(((ma_booking_service_t*)service)->server);
            }
            else
                MA_LOG(logger, MA_LOG_SEV_CRITICAL, "booking service start failed, last error(%d)", err);
            ma_booker_stop(self->booker);
        }
        else
            MA_LOG(logger, MA_LOG_SEV_CRITICAL, "booking service start failed, last error(%d)", err);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_service_get_version(ma_uint32_t *version) {
    if(version) {
        *version = 0x05020100; //TODO fetch version 
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_service_get_context(ma_booking_service_t *service, ma_context_t **context) {
    if(service && context) {
        *context = service->context;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_booking_service_start(ma_booking_service_t *service) {
    return service_start((ma_service_t*)service);
}

ma_error_t ma_booking_service_stop(ma_booking_service_t *service) {
    return service_stop((ma_service_t*)service);
}

ma_error_t service_stop(ma_service_t *service) {
    if(service) {
        ma_error_t err = MA_OK;
        ma_booking_service_t *self = (ma_booking_service_t *)service;

        if(MA_OK != (err = ma_msgbus_subscriber_unregister(((ma_booking_service_t*)service)->subscriber))) {
            MA_LOG(((ma_booking_service_t*)service)->logger, MA_LOG_SEV_ERROR, "booking service unregistering subscriber failed, last error(%d)", err);
        }

        if(MA_OK == (err = ma_msgbus_server_stop(self->server))) {
            if(MA_OK != (err = ma_booker_stop(self->booker))) 
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_CRITICAL, "booker stop failed, last error(%d)", err);
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO, "booking service stopped successfully");
        }
        else
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_CRITICAL, "booking service stop failed, last error(%d)", err);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

void service_release(ma_service_t *service) {
    if(service) {
        ma_booking_service_release((ma_booking_service_t *)service) ;
        return ;
    }
    return ;
}

ma_error_t ma_booking_service_get_booker(ma_booking_service_t *booking, ma_booker_t **booker) {
    if(booking && booker) {
        *booker = booking->booker;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_booking_service_task_add(ma_booking_service_t *booking, const char *booking_id, ma_variant_t *payload) {
    if(booking && booking_id) {
         ma_error_t err = MA_OK;
         ma_task_t *task = NULL;
         ma_trigger_t *trigger = NULL;
         ma_trigger_daily_policy_t policy = {1};
         ma_scheduler_t *scheduler = MA_CONTEXT_GET_SCHEDULER(booking->context);

         if(MA_OK == (err = ma_task_create(booking_id, &task))) {
             ma_task_repeat_policy_t repeat = {{0}};

             repeat.repeat_interval.hour = 0;
             repeat.repeat_interval.minute = MA_BOOKING_TASK_REPEAT_INTERVAL;
             repeat.repeat_until.hour = MA_BOOKING_TASK_REPEAT_DURATION;
             repeat.repeat_until.minute = 0;
             (void)ma_task_set_software_id(task, MA_SOFTWAREID_GENERAL_STR);
             (void)ma_task_set_creator_id(task, MA_BOOKING_SERVICE_SECTION_NAME_STR);
             (void)ma_task_set_type(task, MA_BOOKING_TASK_TYPE);
             (void)ma_task_set_name(task, booking_id);
             if(!payload)
                  MA_LOG(booking->logger, MA_LOG_SEV_ERROR, "Booking task id(%s) payload is NULL", booking_id);
             (void)ma_task_set_app_payload(task, payload); 
             (void)ma_task_set_repeat_policy(task, repeat);
             (void)ma_task_set_max_run_time_limit(task, MA_BOOKING_TASK_MAX_RUN_TIME_LIMIT);
             if(MA_OK == (err = ma_trigger_create_daily(&policy, &trigger))) {
                 ma_task_time_t begin_date = {0};

                 advance_current_date_by_seconds(MA_BOOKING_TASK_START_DELAY, &begin_date);
                 (void)ma_trigger_set_begin_date(trigger, &begin_date);
                 if(MA_OK == (err = ma_task_add_trigger(task, trigger))) {
                     if(MA_OK == (err = ma_scheduler_add(scheduler, task))) {
                         MA_LOG(booking->logger, MA_LOG_SEV_INFO, "Booking task id(%s) added successfully into scheduler", booking_id);
                     } else
                         MA_LOG(booking->logger, MA_LOG_SEV_ERROR, "Booking task id(%s) added successfully into scheduler failed, last error(%d)", booking_id, err);
                 }
                 (void)ma_trigger_release(trigger);
             }
             (void)ma_task_release(task);
         }
         return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_booking_service_task_update(ma_booking_service_t *booking, const char *booking_id, ma_variant_t *payload) {
    if(booking && booking_id) {
         ma_error_t err = MA_OK;
         ma_task_t *task = NULL;
         ma_trigger_t *trigger = NULL;
         ma_scheduler_t *scheduler = MA_CONTEXT_GET_SCHEDULER(booking->context);

         if(MA_OK == (err = ma_scheduler_get_task_handle(scheduler, booking_id, &task))) {
             size_t size = 0, i = 0;
             ma_trigger_list_t *tl = NULL;

             (void)ma_task_size(task, &size);
             (void)ma_task_get_trigger_list(task, &tl);
             (void)ma_task_set_app_payload(task, payload);
             for(i = 0; i < size; ++i) {
                 if(MA_OK == (err = ma_trigger_list_at(tl, i, &trigger))) {
                     ma_task_time_t begin_date = {0};

                     advance_current_date_by_seconds(MA_BOOKING_TASK_START_DELAY, &begin_date);
                     (void)ma_trigger_set_begin_date(trigger, &begin_date);
                     (void)ma_trigger_release(trigger);
                 }
             }
             if(MA_OK == (err = ma_scheduler_modify_task(scheduler, task))) {
                 MA_LOG(booking->logger, MA_LOG_SEV_INFO, "Booking task id(%s) modified successfully into scheduler", booking_id);
             } else
                 MA_LOG(booking->logger, MA_LOG_SEV_ERROR, "Booking task id(%s) modified into scheduler failed, last error(%d)", booking_id, err);
             (void)ma_task_release(task);
         }
         return err;
    }
    return MA_ERROR_INVALIDARG;
}
static ma_error_t ma_booking_service_task_update_status(ma_booking_service_t *booking, const char *task_id, ma_task_exec_status_t status) {
    if(booking && task_id) {
        ma_context_t *context = booking->context;
        ma_task_t *task = NULL ;
        ma_error_t err = MA_OK ;
        ma_scheduler_t *scheduler = MA_CONTEXT_GET_SCHEDULER(context) ;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(context);

        MA_LOG(logger, MA_LOG_SEV_DEBUG, "Booking task id(%s) status update handle begin", task_id);
        if(MA_OK == (err = ma_scheduler_get_task_handle(scheduler, task_id, &task))) {
            if(MA_OK == (err = ma_task_set_execution_status(task, status))) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "Booking service task(%s) status set to %d", task_id, status) ;
            }
            (void)ma_task_release(task);
        }
        if(MA_OK != err)
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "Booking service task(%s) set status(%d) failed, last error(%d)", task_id, status, err) ;

        MA_LOG(logger, MA_LOG_SEV_DEBUG, "Booking task id(%s) status update handle end", task_id);
        return err ;
    }
    return MA_ERROR_INVALIDARG ;
}

static ma_error_t ma_booking_service_task_start(ma_booking_service_t *service, const char *task_id) {
    if(service && task_id) {
        ma_context_t *context = service->context;
        ma_task_t *task = NULL ;
        ma_error_t err = MA_OK ;
        ma_scheduler_t *scheduler = MA_CONTEXT_GET_SCHEDULER(context) ;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(context);

        if(MA_OK == (err = ma_scheduler_get_task_handle(scheduler, task_id, &task))) {
            if(MA_OK == (err = ma_task_start(task))) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "Booking service task(%s) started", task_id) ;
            }
            (void)ma_task_release(task);
        }
        if(MA_OK != err)
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "Booking service task(%s) start failed(%d)", task_id, err) ;

        return err ;
    }
    return MA_ERROR_INVALIDARG ;
}

static ma_error_t ma_booking_service_task_stop(ma_booking_service_t *service, const char *task_id) {
    if(service && task_id) {
        ma_context_t *context = service->context;
        ma_task_t *task = NULL ;
        ma_error_t err = MA_OK ;
        ma_scheduler_t *scheduler = MA_CONTEXT_GET_SCHEDULER(context) ;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(context);

        if(MA_OK == (err = ma_scheduler_get_task_handle(scheduler, task_id, &task))) {
            if(MA_OK == (err = ma_task_stop(task))) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "Booking service task(%s) stopped", task_id) ;
            }
            (void)ma_task_release(task);
        }
        if(MA_OK != err)
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "Booking service task(%s) stop failed(%d)", task_id, err) ;

        return err ;
    }
    return MA_ERROR_INVALIDARG ;
}

static ma_error_t ma_booking_service_task_remove(ma_booking_service_t *booking, const char *task_id) {
    if(booking && task_id) {
        ma_error_t err = MA_OK;
        ma_scheduler_t *scheduler = MA_CONTEXT_GET_SCHEDULER(booking->context) ;
        ma_task_t *task = NULL ;

        if(MA_OK == (err = ma_scheduler_get_task_handle(scheduler, task_id, &task))) {
            (void)ma_task_release(task);
            if(MA_OK == (err = ma_scheduler_remove(scheduler, task_id))) {
                MA_LOG(booking->logger, MA_LOG_SEV_INFO, "Booking task removed from scheduler successfully");
            }
        }
        if(MA_OK != err)
            MA_LOG(booking->logger, MA_LOG_SEV_DEBUG, "Booking service task remove failed(%d)", err) ;

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t order_create_handler(ma_booking_service_t *service, ma_message_t *req_msg, ma_message_t *rsp_msg) {
    if(service && req_msg && rsp_msg) {
        ma_error_t err = MA_OK;
        const char *booking_id = NULL;
        char *status = MA_BOOKING_ID_NOT_EXIST;
        ma_variant_t *payload = NULL;
        char error_code[MA_MAX_LEN] = {0};
        ma_booking_info_t *info = NULL;

        (void)ma_sms_set_logger(service->logger);
        if(MA_OK == (err = ma_message_get_property(req_msg, MA_BOOKING_ID, &booking_id))) {
            (void)ma_message_set_property(rsp_msg, MA_BOOKING_ID, booking_id);
            MA_LOG(service->logger, MA_LOG_SEV_TRACE, "booking id(%s) create order processing", booking_id);
            if(MA_OK == (err = ma_message_get_payload(req_msg, &payload))) {
                if(MA_OK == (err = ma_booking_info_convert_from_variant(payload, &info))) {
                    char id[MA_MAX_NAME+1] = {0};

                    (void)ma_booking_info_get_id(info, id);
                    if(MA_OK == (err = ma_booker_add_variant(service->booker, booking_id, payload))) {
                        status = MA_BOOKING_ID_WRITTEN;
                        MA_LOG(service->logger, MA_LOG_SEV_TRACE, "booking_id(%s) crn(%s) written into DB successfully", booking_id, id);
                        if(MA_OK == (err = ma_booking_service_task_add(service, id, payload))) {
                            char user_contact[MA_MAX_LEN+1] = {0};
                            char msg[MA_MAX_BUFFER_LEN+1] = {0};

                            (void)ma_booking_info_get_contact(info, user_contact);
                            MA_MSC_SELECT(_snprintf, snprintf)(msg, MA_MAX_BUFFER_LEN, "Your order %s is confirmed! Thank you for placing the order in mileaccess.com", id);
                            (void)ma_sms_send(MA_HTTP_NON_SECURE_PROTOCOL, MA_MILEACCESS_HOST, MA_MILEACCESS_PINCODE_MAPPER_PORT, MA_SMS_SEND_PATH, user_contact, msg); 
                            MA_LOG(service->logger, MA_LOG_SEV_TRACE, "booking id(%s) task add successful", id);
                            err = ma_booking_service_task_start(service, id);
                        } else
                            MA_LOG(service->logger, MA_LOG_SEV_ERROR, "booking id(%s) task add failed, last error(%d)", id, err);
                    }
                    (void)ma_booking_info_release(info);
                }
                (void)ma_variant_release(payload);
            }
        }
        if(MA_OK != err)
            MA_LOG(service->logger, MA_LOG_SEV_CRITICAL, "booking order creation failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(rsp_msg, MA_BOOKING_STATUS, status);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t order_modify_handler(ma_booking_service_t *service, ma_message_t *req_msg, ma_message_t *rsp_msg) {
    if(service && req_msg && rsp_msg) {
        ma_error_t err = MA_OK;
        const char *booking_id = NULL;
        char *status = MA_BOOKING_ID_NOT_EXIST;
        ma_variant_t *payload = NULL;
        char error_code[MA_MAX_LEN] = {0};
        ma_booking_info_t *info = NULL;

        if(MA_OK == (err = ma_message_get_property(req_msg, MA_BOOKING_ID, &booking_id))) {
            (void)ma_message_set_property(rsp_msg, MA_BOOKING_ID, booking_id);
            MA_LOG(service->logger, MA_LOG_SEV_TRACE, "booking id(%s) order modify request  processing", booking_id);
            if(MA_OK == (err = ma_message_get_payload(req_msg, &payload))) {
                if(MA_OK == (err = ma_booking_info_convert_from_variant(payload, &info))) {
                    char id[MA_MAX_NAME+1] = {0};

                    (void)ma_booking_info_get_id(info, id);
                    if(MA_OK == (err = ma_booker_add_variant(service->booker, booking_id, payload))) {
                        status = MA_BOOKING_ID_WRITTEN;
                        MA_LOG(service->logger, MA_LOG_SEV_TRACE, "booking_id(%s) crn(%s) written into DB successfully", booking_id, id);

                    }
                    (void)ma_booking_info_release(info);
                }
                (void)ma_variant_release(payload);
            }
        }
        if(MA_OK != err)
            MA_LOG(service->logger, MA_LOG_SEV_CRITICAL, "booking order modify failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(rsp_msg, MA_BOOKING_STATUS, status);
        return err = MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t order_delete_handler(ma_booking_service_t *service, ma_message_t *req_msg, ma_message_t *rsp_msg) {
    if(service && req_msg && rsp_msg) {
        ma_error_t err = MA_OK;
        const char *booking_id = NULL;
        char *status = MA_BOOKING_ID_NOT_EXIST;
        char error_code[MA_MAX_LEN] = {0};

        if(MA_OK == (err = ma_message_get_property(req_msg, MA_BOOKING_ID, &booking_id))){
            ma_booking_info_t *binfo = NULL;
            char contact[MA_MAX_LEN+1] = {0};
            char picker_contact[MA_MAX_LEN+1] = {0};

            if(MA_OK == (err = ma_booker_get(service->booker, booking_id, &binfo))) {
                (void)ma_booking_info_get_contact(binfo, contact);
                (void)ma_booking_info_get_picker_contact(binfo, picker_contact);
                (void)ma_booking_info_release(binfo);
            }
            (void)ma_message_set_property(rsp_msg, MA_BOOKING_ID, booking_id);
            MA_LOG(service->logger, MA_LOG_SEV_TRACE, "booking id(%s) order delete request processing", booking_id);
            if(MA_OK == (err = ma_booker_delete(service->booker, booking_id))) {
                status = MA_BOOKING_ID_DELETED;
                if(strcmp(MA_EMPTY, contact)) {
                    (void)consign_remove_pending(MA_CONTEXT_GET_CONSIGNOR(service->context), contact, booking_id); 
                    (void)consign_remove_complete(MA_CONTEXT_GET_CONSIGNOR(service->context), contact, booking_id); 
                }
                if(strcmp(MA_EMPTY, picker_contact)) {
                    (void)consign_remove_pending(MA_CONTEXT_GET_CONSIGNOR(service->context), picker_contact, booking_id); 
                    (void)consign_remove_complete(MA_CONTEXT_GET_CONSIGNOR(service->context), picker_contact, booking_id); 
                }
                MA_LOG(service->logger, MA_LOG_SEV_TRACE, "booking id(%s) deleted successfully", booking_id);
                if(MA_OK == ma_booking_service_task_remove(service, booking_id)) {
                    MA_LOG(service->logger, MA_LOG_SEV_ERROR, "booking id(%s) task remove successfull", booking_id );
                } else
                    MA_LOG(service->logger, MA_LOG_SEV_ERROR, "booking id(%s)  task remove failed, last error(%d)", booking_id,  err);
            } else {
                MA_LOG(service->logger, MA_LOG_SEV_ERROR, "booking id(%s) order delete failed, last error(%d)", booking_id, err);
                status = MA_BOOKING_ID_NOT_EXIST;
            }
        }
        if(MA_OK != err)
            MA_LOG(service->logger, MA_LOG_SEV_CRITICAL, "booking order delete failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(rsp_msg, MA_BOOKING_STATUS, status);
        return err = MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static void copy_trans_each(const char *key, ma_variant_t *value, void *cb_data, ma_bool_t *stop_loop) {
    if(key && value && cb_data && stop_loop) {
        ma_table_t *info = (ma_table_t*)cb_data;
        (void)ma_table_add_entry(info, key, value);
    }
}
static ma_error_t order_update_status_handler(ma_booking_service_t *service, ma_message_t *req_msg, ma_message_t *rsp_msg) {
    if(service && req_msg && rsp_msg) {
        ma_error_t err = MA_OK;
        const char *booking_id = NULL;
        char *status = MA_BOOKING_ID_NOT_EXIST;
        char error_code[MA_MAX_LEN] = {0};

        if(MA_OK == (err = ma_message_get_property(req_msg, MA_BOOKING_ID, &booking_id))) {
            ma_booking_info_t *info = NULL;

            (void)ma_message_set_property(rsp_msg, MA_BOOKING_ID, booking_id);
            MA_LOG(service->logger, MA_LOG_SEV_TRACE, "booking id(%s) order update request processing", booking_id);
            if(MA_OK == (err = ma_booker_get(service->booker, booking_id, &info))) {
                ma_variant_t *payload = NULL;

                status = MA_BOOKING_ID_EXIST;
                MA_LOG(service->logger, MA_LOG_SEV_TRACE, "booking id(%s) order update request processing -- EXIST", booking_id);
                if(MA_OK == (err = ma_message_get_payload(req_msg, &payload))) {
                    ma_booking_info_t *req_info = NULL;

                    MA_LOG(service->logger, MA_LOG_SEV_TRACE, "booking id(%s) order update request processing -- MESSAGE PAYLOAD EXIST", booking_id);
                    if(MA_OK == (err = ma_booking_info_convert_from_variant(payload, &req_info))) {
                        char transit[MA_MAX_LEN+1] = {0};
                        ma_booking_info_status_t state = MA_BOOKING_INFO_STATUS_TRANSIT;
                        int proceed_update = 0;

                        MA_LOG(service->logger, MA_LOG_SEV_TRACE, "booking id(%s) order update request processing -- CONVERSION FROM VARIANT", booking_id);
                        (void)ma_booking_info_get_status(req_info, &state);
                        {
                           ma_booking_info_payment_status_t cur_status = MA_BOOKING_INFO_PAYMENT_STATUS_UNPAID;

                           (void)ma_booking_info_get_payment_status(info, &cur_status);
                           if(cur_status != MA_BOOKING_INFO_PAYMENT_STATUS_PAID) {
                               ma_booking_info_payment_status_t req_status = MA_BOOKING_INFO_PAYMENT_STATUS_UNPAID;

                               (void)ma_booking_info_get_payment_status(req_info, &req_status);
                               MA_LOG(service->logger, MA_LOG_SEV_TRACE, "booking id(%s) order update request processing -- request payment state(%d)", booking_id, req_status);
                               if(MA_BOOKING_INFO_PAYMENT_STATUS_PAID == req_status) {
                                   MA_LOG(service->logger, MA_LOG_SEV_TRACE, "booking id(%s) PAID successfully ", booking_id);
                                   (void)ma_booking_info_set_payment_status(info, req_status);
                                   proceed_update = MA_TRUE;
                               }
                           }
                        }
                        if(state == MA_BOOKING_INFO_STATUS_DELIVERED || 
                           state == MA_BOOKING_INFO_STATUS_CANCELLED ||
                           state == MA_BOOKING_INFO_STATUS_TRANSIT || 
                           state == MA_BOOKING_INFO_STATUS_PICKED || 
                           state == MA_BOOKING_INFO_STATUS_NOT_DELIVERED) {
                           ma_table_t *rec_trans =  NULL;
                           char recv_package_url[MA_MAX_BUFFER_LEN+1] = {0};

                           (void)ma_booking_info_set_status(info, state);
                           (void)ma_booking_info_get_transit(req_info, transit);
                           if(strcmp(transit, MA_EMPTY)) {
                               (void)ma_booking_info_set_transit(info, transit);
                           }
                           (void)ma_booking_info_get_package_image_url(req_info, recv_package_url);
                           if(strcmp(recv_package_url, MA_EMPTY)) {
                               (void)ma_booking_info_set_package_image_url(info, recv_package_url);
                           }
                           if(MA_OK == (err = ma_booking_info_get_transitions(req_info, &rec_trans))) {
                               ma_table_t *info_trans = NULL;
                               if(MA_OK == (err = ma_booking_info_get_transitions(info, &info_trans))) {
                                   (void)ma_table_foreach(rec_trans, &copy_trans_each, info_trans);
                                   (void)ma_table_release(info_trans);
                               }
                               (void)ma_table_release(rec_trans);
                           }
                           proceed_update = MA_TRUE;
                        }

                        if(proceed_update) {
                            MA_LOG(service->logger, MA_LOG_SEV_TRACE, "booking id(%s) order update request processing -- INITIATED", booking_id);
                            if(MA_OK == (err = ma_booker_add(service->booker, info))) { 
                                ma_variant_t *new_payload = NULL;
                                if(MA_OK == (err = ma_booking_info_convert_to_variant(info, &new_payload))) {
                                    status = MA_BOOKING_ID_UPDATED;
                                    if(MA_OK == (err = ma_booking_service_task_update(service, booking_id, new_payload))) {
                                        MA_LOG(service->logger, MA_LOG_SEV_TRACE, "booking id(%s) updated successfully", booking_id);
                                    } else
                                        MA_LOG(service->logger, MA_LOG_SEV_ERROR, "booking id(%s) update failed, last error(%d", booking_id, err);
                                    (void)ma_variant_release(new_payload);
                                }
                            } else {
                                MA_LOG(service->logger, MA_LOG_SEV_TRACE, "booking id(%s) order update request processing -- FAILED, error (%d)", booking_id, err);
                            }
                        }
                        (void)ma_booking_info_release(req_info);
                    }
                    (void)ma_variant_release(payload);
                }                
                (void)ma_booking_info_release(info);
            } else {
                MA_LOG(service->logger, MA_LOG_SEV_ERROR, "booking id(%s) order update failed, last error(%d)", booking_id, err);
                status = MA_BOOKING_ID_NOT_EXIST;
            }
        }
        if(MA_OK != err)
            MA_LOG(service->logger, MA_LOG_SEV_CRITICAL, "booking order update failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(rsp_msg, MA_BOOKING_STATUS, status);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t booking_get_json_handler(ma_booking_service_t *service, ma_message_t *req_msg, ma_message_t *rsp_msg) {
    if(service && req_msg && rsp_msg) {
        ma_error_t err = MA_OK;
        const char *booking_id = NULL;
        char *status = MA_BOOKING_ID_NOT_EXIST;
        char error_code[MA_MAX_LEN] = {0};

        if(MA_OK == (err = ma_message_get_property(req_msg, MA_BOOKING_ID, &booking_id))) {
            ma_booking_info_t *info = NULL;

            (void)ma_message_set_property(rsp_msg, MA_BOOKING_ID, booking_id);
            MA_LOG(service->logger, MA_LOG_SEV_TRACE, "booking id(%s) get json request processing", booking_id);
            if(MA_OK == (err = ma_booker_get(service->booker, booking_id, &info))) {
                ma_json_t *json = NULL;

                if(MA_OK == (err = ma_booking_info_convert_to_json(info, &json))) {
                    ma_bytebuffer_t *buffer = NULL;

                    ma_bytebuffer_create(MA_MAX_BUFFER_LEN, &buffer);
                    if(MA_OK == (err = ma_json_get_data(json, buffer))) {
                        ma_variant_t *v = NULL;

                        if(MA_OK == (err = ma_variant_create_from_string((const char*)ma_bytebuffer_get_bytes(buffer), ma_bytebuffer_get_size(buffer), &v))) {
                            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "booking json output(%s) len(%u)", ma_bytebuffer_get_bytes(buffer), ma_bytebuffer_get_size(buffer));
                            status = MA_BOOKING_ID_EXIST;
                            (void)ma_message_set_payload(rsp_msg, v);
                            (void)ma_variant_release(v);
                        } else
                            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_ERROR, "booking id(%s) variant creation failed, last error(%d)", booking_id,err);
                    }
                } 
                (void)ma_booking_info_release(info);
            }
        }
        if(MA_OK != err)
            MA_LOG(service->logger, MA_LOG_SEV_CRITICAL, "booking json get failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(rsp_msg, MA_BOOKING_STATUS, status);
        return err = MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


static ma_error_t booking_delete_handler(ma_booking_service_t *service, ma_message_t *req_msg, ma_message_t *rsp_msg) {
    if(service && req_msg && rsp_msg) {
        ma_error_t err = MA_OK;
        const char *booking_id = NULL;
        char *status = MA_BOOKING_ID_NOT_EXIST;
        char error_code[MA_MAX_LEN] = {0};

        if(MA_OK == (err = ma_message_get_property(req_msg, MA_BOOKING_ID, &booking_id))) {
            ma_variant_t *booking_var = NULL;

            (void)ma_message_set_property(rsp_msg, MA_BOOKING_ID, booking_id);
            MA_LOG(service->logger, MA_LOG_SEV_TRACE, "booking id(%s) delete request processing", booking_id);
            if(MA_OK == (err = ma_booker_get_all(service->booker, booking_id, &booking_var))) {
                ma_table_t *book_entry = NULL;

                if(MA_OK == (err = ma_variant_get_table(booking_var, &book_entry))) {
                    ma_array_t *arr = NULL;

                    if(MA_OK == (err = ma_table_get_keys(book_entry, &arr))) {
                        size_t arr_size = 0, i = 0;

                        (void)ma_array_size(arr, &arr_size);
                        for(i = 1; i <= arr_size; ++i) {
                            ma_variant_t *keyv = NULL;

                           if(MA_OK == (err = ma_array_get_element_at(arr, i, &keyv))) {
                               ma_buffer_t *buf = NULL;

                               if(MA_OK == (err = ma_variant_get_string_buffer(keyv, &buf))) {
                                   const char *key = NULL; size_t key_len = 0;

                                   if(MA_OK == (err = ma_buffer_get_string(buf, &key, &key_len))) {
                                       if(MA_OK == (err = ma_booking_service_task_remove(service, key))) {
                                           MA_LOG(service->logger, MA_LOG_SEV_INFO, "booking id(%s)  task removed successfully", booking_id);
                                       } else
                                           MA_LOG(service->logger, MA_LOG_SEV_ERROR, "booking id(%s) task remove failed, last error(%d)", booking_id, err);
                                   }
                                   (void)ma_buffer_release(buf);
                               }
                               (void)ma_variant_release(keyv);
                            }
                        }
                        (void)ma_array_release(arr);
                    }
                    (void)ma_table_release(book_entry);
                }
                (void)ma_variant_release(booking_var);
            }
            if(MA_OK == (err = ma_booker_delete(service->booker, booking_id))) {
                status = MA_BOOKING_ID_DELETED;
                MA_LOG(service->logger, MA_LOG_SEV_TRACE, "booking id(%s) deleted successfully", booking_id);
            } else {
                MA_LOG(service->logger, MA_LOG_SEV_ERROR, "booking id(%s) order delete failed, last error(%d)", booking_id, err);
                status = MA_BOOKING_ID_NOT_EXIST;
            }
        }
        if(MA_OK != err)
            MA_LOG(service->logger, MA_LOG_SEV_CRITICAL, "booking order delete failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(rsp_msg, MA_BOOKING_STATUS, status);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_booking_service_cb(ma_msgbus_server_t *server, ma_message_t *msg, void *cb_data, ma_msgbus_client_request_t *c_request) {
    if(server && msg && cb_data) {
        ma_error_t err = MA_OK;
        const char *msg_type = NULL;
        ma_message_t *response = NULL;
        ma_booking_service_t *service = (ma_booking_service_t*)cb_data;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(service->context);

        if(MA_OK == (err = ma_message_create(&response))) {
            if(MA_OK == (err = ma_message_get_property(msg, MA_BOOKING_SERVICE_MSG_TYPE, &msg_type))) {
                if(!strcmp(msg_type, MA_BOOKING_SERVICE_MSG_ORDER_CREATE_TYPE)) {
                    if(MA_OK == (err = order_create_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client order create request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_BOOKING_SERVICE_MSG_ORDER_MODIFY_TYPE)) {
                    if(MA_OK == (err = order_modify_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client order modify request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_BOOKING_SERVICE_MSG_ORDER_DELETE_TYPE)) {
                    if(MA_OK == (err = order_delete_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client order delete request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_BOOKING_SERVICE_MSG_ORDER_UPDATE_STATUS_TYPE)) {
                    if(MA_OK == (err = order_update_status_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client order update request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_BOOKING_SERVICE_MSG_BOOKING_DELETE_TYPE)) {
                    if(MA_OK == (err = booking_delete_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client order update request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_BOOKING_SERVICE_MSG_GET_JSON)) {
                    if(MA_OK == (err = booking_get_json_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client order update request failed, last error(%d)", err);
                        }
                    }
                } else {
                    if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, MA_ERROR_SCHEDULER_INVALID_OP_TYPE, response))) {
                        MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client request failed, last error(%d)", err);
                    }
                }
            } else {
                if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err = MA_ERROR_SCHEDULER_TASK_TYPE_NOT_SET, response))) {
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client request failed, last error(%d)", err);
                }
            }
            if(MA_OK != ma_message_release(response)) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client request message release failed");
            }
        }
        return err;
    }
    return MA_OK;
}

static ma_bool_t check_profile_active(const char *contact) {
    if(contact) {
        ma_profile_info_t *info = NULL;
        ma_bool_t status = MA_FALSE;

        if(MA_OK == ma_profiler_get(MA_CONTEXT_GET_PROFILER(booking_service->context), contact, &info)) {
            ma_profile_info_status_t active = MA_PROFILE_INFO_STATUS_NOT_ACTIVE;

            (void)ma_profile_info_get_status(info, &active);
            status = MA_PROFILE_INFO_STATUS_ACTIVE == active ? MA_TRUE : MA_FALSE;
            (void)ma_profile_info_release(info);
        }
        return status;
    }
    return MA_FALSE;
}

/* check for the load */
static ma_error_t shortlist_on_orders_load(ma_table_t *pincode_map, char *shortlisted) {
   if(pincode_map && shortlisted) {
       ma_error_t err = MA_OK;
       ma_array_t *contacts = NULL;
       size_t size = 0, i = 0, lowest_load = 0;
       int found = 0;
       ma_consignor_t *consignor = MA_CONTEXT_GET_CONSIGNOR(booking_service->context);

       if(MA_OK == (err = ma_table_get_keys(pincode_map, &contacts))) {
           (void)ma_array_size(contacts, &size);
           for(i = 1; i <= size && !found; ++i) {
              ma_variant_t *contact = NULL;

              if(MA_OK == (err = ma_array_get_element_at(contacts, i, &contact))) {
                  ma_buffer_t *buf = NULL;

                  if(MA_OK == (err = ma_variant_get_string_buffer(contact, &buf))) {
                      const char *pstr = NULL;
                      size_t len = 0;

                      if(MA_OK == (err = ma_buffer_get_string(buf, &pstr, &len))) {
                          ma_consign_info_t *consign = NULL;

                          if(MA_OK == (err = ma_consignor_get(consignor, pstr, &consign))) {
                              size_t cur_load = 0; 

                              (void)ma_consign_info_pending_order_list_size(consign, pstr, &cur_load);
                              MA_LOG(booking_service->logger, MA_LOG_SEV_DEBUG, "contact(%s) current pending load(%u)", pstr, cur_load);
                              if(0 == lowest_load) {
                                  lowest_load = cur_load;
                                  if(check_profile_active(pstr)) {
                                      strncpy(shortlisted, pstr, MA_MAX_LEN);
                                  }
                              } else if(cur_load < lowest_load) {
                                  lowest_load = cur_load;
                                  if(check_profile_active(pstr)) {
                                      strncpy(shortlisted, pstr, MA_MAX_LEN);
                                  }
                              } else {
                              }
                              (void)ma_consign_info_release(consign);                      
                          } else {
                              //default choice 
                              err = MA_OK;
                              if(check_profile_active(pstr)) {
                                  strncpy(shortlisted, pstr, MA_MAX_LEN);
                              }
                              found = !found;
                          }
                      }
                      (void)ma_buffer_release(buf);
                  }
                  (void)ma_variant_release(contact);
              }
           }
           (void)ma_array_release(contacts);
       }
       
       if(strcmp(shortlisted, MA_EMPTY)) {
           MA_LOG(booking_service->logger, MA_LOG_SEV_TRACE, "shortlisted contact(%s)", shortlisted);
       } else {
           MA_LOG(booking_service->logger, MA_LOG_SEV_TRACE, "no contacts shortlisted");
       }
       return err;
   } 
   return MA_ERROR_INVALIDARG;
}

static ma_error_t consign_add_pending(ma_consignor_t *consignor, const char *contact, const char *id) {
   if(consignor && contact) {
       ma_error_t err = MA_OK;
       ma_consign_info_t *consign = NULL;

       if(MA_OK == (err = ma_consignor_get(consignor, contact, &consign))) {
           if(MA_OK == (err = ma_consign_info_add_pending_order(consign, contact, id))) {
               if(MA_OK == (err = ma_consignor_add(consignor, consign))) {
                   MA_LOG(booking_service->logger, MA_LOG_SEV_TRACE, "consignment id(%s) added pending booking id(%s)", contact, id);
               } else {
                   MA_LOG(booking_service->logger, MA_LOG_SEV_TRACE, "consignment id(%s) added pending booking id(%s) failed last error(%d)", contact, id, err);
               }
           }
           (void)ma_consign_info_release(consign);
       }

       return err;
   }
   return MA_ERROR_INVALIDARG;
}

static ma_error_t recorder_add_pending(ma_recorder_t *recorder, const char *contact, const char *id) {
   if(recorder && contact) {
       ma_error_t err = MA_OK;
       ma_recorder_info_t *record = NULL;

       if(MA_OK == (err = ma_recorder_get(recorder, contact, &record))) {
           if(MA_OK == (err = ma_recorder_info_add_pending_order(record, contact, id))) {
               if(MA_OK == (err = ma_recorder_add(recorder, record))) {
                   MA_LOG(booking_service->logger, MA_LOG_SEV_TRACE, "recorder id(%s) added pending booking id(%s)", contact, id);
               } else {
                   MA_LOG(booking_service->logger, MA_LOG_SEV_TRACE, "recorder id(%s) added pending booking id(%s) failed last error(%d)", contact, id, err);
               }
           }
           (void)ma_recorder_info_release(record);
       }

       return err;
   }
   return MA_ERROR_INVALIDARG;
}
static ma_error_t consign_remove_pending(ma_consignor_t *consignor, const char *contact, const char *id) {
   if(consignor && contact) {
       ma_error_t err = MA_OK;
       ma_consign_info_t *consign = NULL;

       if(MA_OK == (err = ma_consignor_get(consignor, contact, &consign))) {
           if(MA_OK == (err = ma_consign_info_remove_pending_order(consign, contact, id))) {
               if(MA_OK == (err = ma_consignor_add(consignor, consign))) {
                   MA_LOG(booking_service->logger, MA_LOG_SEV_TRACE, "consignment id(%s) added pending booking id(%s)", contact, id);
               } else {
                   MA_LOG(booking_service->logger, MA_LOG_SEV_TRACE, "consignment id(%s) added pending booking id(%s) failed last error(%d)", contact, id, err);
               }
           }
           (void)ma_consign_info_release(consign);
       }

       return err;
   }
   return MA_ERROR_INVALIDARG;
}

static ma_error_t consign_remove_complete(ma_consignor_t *consignor, const char *contact, const char *id) {
   if(consignor && contact) {
       ma_error_t err = MA_OK;
       ma_consign_info_t *consign = NULL;

       if(MA_OK == (err = ma_consignor_get(consignor, contact, &consign))) {
           if(MA_OK == (err = ma_consign_info_remove_completed_order(consign, contact, id))) {
               if(MA_OK == (err = ma_consignor_add(consignor, consign))) {
                   MA_LOG(booking_service->logger, MA_LOG_SEV_TRACE, "consignment id(%s) removed from completed booking id(%s)", contact, id);
               } else {
                   MA_LOG(booking_service->logger, MA_LOG_SEV_TRACE, "consignment id(%s) removed from the booking id(%s) failed last error(%d)", contact, id, err);
               }
           }
           (void)ma_consign_info_release(consign);
       }

       return err;
   }
   return MA_ERROR_INVALIDARG;
}
static ma_error_t recorder_remove_pending(ma_recorder_t *recorder, const char *contact, const char *id) {
   if(recorder && contact) {
       ma_error_t err = MA_OK;
       ma_recorder_info_t *record = NULL;

       if(MA_OK == (err = ma_recorder_get(recorder, contact, &record))) {
           if(MA_OK == (err = ma_recorder_info_remove_pending_order(record, contact, id))) {
               if(MA_OK == (err = ma_recorder_add(recorder, record))) {
                   MA_LOG(booking_service->logger, MA_LOG_SEV_TRACE, "recorder id(%s) added pending booking id(%s)", contact, id);
               } else {
                   MA_LOG(booking_service->logger, MA_LOG_SEV_TRACE, "recorder id(%s) added pending booking id(%s) failed last error(%d)", contact, id, err);
               }
           }
           (void)ma_recorder_info_release(record);
       }

       return err;
   }
   return MA_ERROR_INVALIDARG;
}
static ma_error_t consign_add_complete(ma_consignor_t *consignor, const char *contact, const char *id) {
   if(consignor && contact) {
       ma_error_t err = MA_OK;
       ma_consign_info_t *consign = NULL;

       if(MA_OK == (err = ma_consignor_get(consignor, contact, &consign))) {
           if(MA_OK == (err = ma_consign_info_add_completed_order(consign, contact, id))) {
               if(MA_OK == (err = ma_consignor_add(consignor, consign))) {
                   MA_LOG(booking_service->logger, MA_LOG_SEV_TRACE, "consignment id(%s) added pending booking id(%s)", contact, id);
               } else {
                   MA_LOG(booking_service->logger, MA_LOG_SEV_TRACE, "consignment id(%s) added pending booking id(%s) failed last error(%d)", contact, id, err);
               }
           }
           (void)ma_consign_info_release(consign);
       }

       return err;
   }
   return MA_ERROR_INVALIDARG;
}

static ma_error_t recorder_add_complete(ma_recorder_t *recorder, const char *contact, const char *id) {
   if(recorder && contact) {
       ma_error_t err = MA_OK;
       ma_recorder_info_t *record = NULL;

       if(MA_OK == (err = ma_recorder_get(recorder, contact, &record))) {
           if(MA_OK == (err = ma_recorder_info_add_completed_order(record, contact, id))) {
               if(MA_OK == (err = ma_recorder_add(recorder, record))) {
                   MA_LOG(booking_service->logger, MA_LOG_SEV_TRACE, "record id(%s) added pending booking id(%s)", contact, id);
               } else {
                   MA_LOG(booking_service->logger, MA_LOG_SEV_TRACE, "record id(%s) added pending booking id(%s) failed last error(%d)", contact, id, err);
               }
           }
           (void)ma_recorder_info_release(record);
       }

       return err;
   }
   return MA_ERROR_INVALIDARG;
}
static ma_error_t order_allocator_handler(ma_booking_info_t *info, ma_location_info_t *linfo, const char *pincode) {
    if(info && linfo) {
        ma_error_t err = MA_OK;
        ma_consignor_t *consignor = MA_CONTEXT_GET_CONSIGNOR(booking_service->context);
        ma_table_t *pincodes = NULL; 

        if(MA_OK == (err = ma_location_info_get_contact_list(linfo, &pincodes))) {
            ma_variant_t *v = NULL;

            if(MA_OK == (err = ma_table_get_value(pincodes, pincode, &v))) {
                ma_table_t *contacts = NULL;

                if(MA_OK == (err = ma_variant_get_table(v, &contacts))) {
                    char shortlisted[MA_MAX_LEN+1] = {0};

                    if(MA_OK == (err = shortlist_on_orders_load(contacts, shortlisted))) {
                        char id[MA_MAX_LEN+1] = {0};
                        char user_contact[MA_MAX_LEN+1] = {0};

                        MA_LOG(booking_service->logger, MA_LOG_SEV_TRACE, "shortlisted contact(%s)", shortlisted);
                        (void)ma_booking_info_get_id(info, id);                    
                        (void)consign_add_pending(consignor, shortlisted, id);
                        (void)ma_booking_info_set_picker_contact(info, shortlisted);
                        (void)ma_booking_info_get_contact(info, user_contact);
                        (void)consign_add_pending(consignor, user_contact, id);
                    } else {
                        MA_LOG(booking_service->logger, MA_LOG_SEV_ERROR, "contacts shortlisting failed last error(%d)", err);
                    }
                    (void)ma_table_release(contacts);
                }
                (void)ma_variant_release(v);
            } else {
                err =  MA_ERROR_NO_MORE_ITEMS;
                MA_LOG(booking_service->logger, MA_LOG_SEV_ERROR, "currently no operation for this pincode");
            }
            (void)ma_table_release(pincodes);
        }
        
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static void current_date_str(char *today_str) {
    ma_task_time_t today = {0};
    get_localtime(&today);
    today.hour = today.minute = today.secs = 0;
    MA_MSC_SELECT(_snprintf, snprintf)(today_str, MA_MAX_LEN, "%hu-%hu-%hu", today.month, today.day, today.year);
}

static ma_error_t order_router_handler(ma_booking_service_t *service, const char *booking_id) {
    if(service && booking_id) {
        ma_error_t err = MA_OK;
        ma_context_t *context = service->context;
        ma_scheduler_t *scheduler = MA_CONTEXT_GET_SCHEDULER(context) ;
        ma_consignor_t *consignor = MA_CONTEXT_GET_CONSIGNOR(context);
        ma_location_t *locator = MA_CONTEXT_GET_LOCATION(context);
        ma_recorder_t *recorder = MA_CONTEXT_GET_RECORDER(context);
        ma_task_t *task = NULL;
        const char *task_id = NULL;

        if(MA_OK == (err = ma_scheduler_get_task_handle(scheduler, booking_id, &task))) {
            ma_variant_t *payload = NULL;

            (void)ma_task_get_id(task, &task_id);
            MA_LOG(service->logger, MA_LOG_SEV_TRACE, "Booking Task id(%s) exist in scheduler", booking_id);
            if(MA_OK == (err = ma_task_get_app_payload(task, &payload)) && payload) {
                ma_booking_info_t *info = NULL;
                
                if(MA_OK == (err = ma_booking_info_convert_from_variant(payload, &info))) {
                    char id[MA_MAX_LEN+1] = {0};
                    ma_booking_info_status_t status = MA_BOOKING_INFO_STATUS_NOT_ASSIGNED;
                    char user_contact[MA_MAX_LEN+1] = {0};

                    (void)ma_booking_info_get_status(info, &status);
                    (void)ma_booking_info_get_contact(info, user_contact);
                    (void)ma_booking_info_get_id(info, id);
                    if(MA_BOOKING_INFO_STATUS_NOT_ASSIGNED == status) {
                        ma_location_info_t *linfo = NULL;
                        char pincode[MA_MAX_LEN+1] = {0};
                        char today_str[MA_MAX_LEN+1]= {0};

                        (void)current_date_str(today_str);
                        (void)recorder_add_pending(recorder, today_str, id);

                        MA_LOG(service->logger, MA_LOG_SEV_TRACE, "Booking Task id(%s) status (NOT ASSIGNED)", id);
                        (void)ma_booking_info_get_source_pincode(info, pincode);
                        if(MA_OK == (err = ma_location_get(locator, pincode, &linfo))) {
                            if(MA_OK == (err = order_allocator_handler(info, linfo, pincode))) {
                                char picker_contact[MA_MAX_LEN+1] = {0};
                                char msg[MA_MAX_BUFFER_LEN+1] = {0};

                                (void)ma_booking_info_set_status(info, MA_BOOKING_INFO_STATUS_ASSIGNED);
                                (void)ma_booking_info_get_picker_contact(info, picker_contact);
                                MA_MSC_SELECT(_snprintf, snprintf)(msg, MA_MAX_BUFFER_LEN, "Your order %s has been updated and assigned to our delivery executive contact %s.", id, picker_contact);
                                (void)ma_sms_send(MA_HTTP_NON_SECURE_PROTOCOL, MA_MILEACCESS_HOST, MA_MILEACCESS_PINCODE_MAPPER_PORT, MA_SMS_SEND_PATH, user_contact, msg); 
                                MA_MSC_SELECT(_snprintf, snprintf)(msg, MA_MAX_BUFFER_LEN, "New Order %s has been assigned to you. Please schedule a pickiup. Thank you again for parterning with mileaccess.com.", id);
                                (void)ma_sms_send(MA_HTTP_NON_SECURE_PROTOCOL, MA_MILEACCESS_HOST, MA_MILEACCESS_PINCODE_MAPPER_PORT, MA_SMS_SEND_PATH, picker_contact, msg); 
                                if(MA_OK == (err = ma_booker_add(service->booker, info))) {
                                    ma_variant_t *v = NULL;

                                    if(MA_OK == (err = ma_booking_info_convert_to_variant(info, &v))) {
                                        (void)ma_task_set_app_payload(task, v);
                                        (void)ma_variant_release(v);
                                    }
                                    MA_LOG(service->logger, MA_LOG_SEV_TRACE, "updating booking status(ASSIGNED)");
                                } else {
                                    MA_LOG(service->logger, MA_LOG_SEV_TRACE, "updating booking status(ASSIGNED) failed last error(%d)", err);
                                }
                            }
                            (void)ma_location_info_release(linfo);
                        } else {
                            MA_LOG(service->logger, MA_LOG_SEV_TRACE, "Booking Task id(%s) cannot be assigned as no executives exist in the pincode", id);
                        }
                    } else if(MA_BOOKING_INFO_STATUS_TRANSIT == status) {
                        ma_location_info_t *linfo = NULL;
                        char pincode[MA_MAX_LEN+1] = {0};
                        char picker_contact[MA_MAX_LEN+1] = {0};

                        (void)ma_booking_info_get_picker_contact(info, picker_contact);
                        MA_LOG(service->logger, MA_LOG_SEV_TRACE, "Booking Task id(%s) status (TRANSIT)", id);
                        (void)ma_booking_info_get_transit(info, pincode);
                        if(strcmp(pincode, MA_EMPTY) && MA_OK == (err = ma_location_get(locator, pincode, &linfo))) {
                            if(MA_OK == (err = order_allocator_handler(info, linfo, pincode))) {
                                char new_picker_contact[MA_MAX_LEN+1] = {0};

                                (void)ma_booking_info_get_picker_contact(info, new_picker_contact);
                                (void)consign_add_complete(consignor, picker_contact, id);
                                (void)consign_remove_pending(consignor, picker_contact, id);
                                (void)consign_add_pending(consignor, new_picker_contact, id);
                                (void)ma_booking_info_set_status(info, MA_BOOKING_INFO_STATUS_ASSIGNED);
                                if(MA_OK == (err = ma_booker_add(service->booker, info))) {
                                    ma_variant_t *v = NULL;

                                    if(MA_OK == (err = ma_booking_info_convert_to_variant(info, &v))) {
                                        (void)ma_task_set_app_payload(task, v);
                                        (void)ma_variant_release(v);
                                    }
                                    MA_LOG(service->logger, MA_LOG_SEV_TRACE, "updating booking status(ASSIGNED)");
                                } else {
                                    MA_LOG(service->logger, MA_LOG_SEV_TRACE, "updating booking status(ASSIGNED) failed last error(%d)", err);
                                }
                            }
                            (void)ma_location_info_release(linfo);
                        } else {
                            MA_LOG(service->logger, MA_LOG_SEV_TRACE, "Booking Task id(%s) cannot be assigned as no executives exist in the pincode(%s)", id, pincode);
                        }
                    } else if(MA_BOOKING_INFO_STATUS_ASSIGNED == status) {
                        MA_LOG(service->logger, MA_LOG_SEV_TRACE, "Booking Task id(%s) status (ASSIGNED)", id);
                        if(MA_OK == (err = ma_booking_service_task_update_status(service, task_id, MA_TASK_EXEC_SUCCESS)))
                            MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "task id(%s) status updated successfully", task_id);
                        else
                            MA_LOG(service->logger, MA_LOG_SEV_ERROR, "task id(%s) status update failed, last error(%d)", task_id, err);
                    } else if(MA_BOOKING_INFO_STATUS_DELIVERED == status) {
                        char picker_contact[MA_MAX_LEN+1] = {0};
                        char today_str[MA_MAX_LEN+1]= {0};

                        (void)current_date_str(today_str);
                        (void)ma_booking_info_get_picker_contact(info, picker_contact);
                        (void)recorder_remove_pending(recorder, today_str, id);
                        (void)recorder_add_complete(recorder, today_str, id);
                        MA_LOG(service->logger, MA_LOG_SEV_TRACE, "Booking Task id(%s) status (DELIVERED)", id);
                        if(MA_OK == (err = ma_booking_service_task_update_status(service, task_id, MA_TASK_EXEC_SUCCESS)))
                            MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "task id(%s) status updated successfully", task_id);
                        else
                            MA_LOG(service->logger, MA_LOG_SEV_ERROR, "task id(%s) status update failed, last error(%d)", task_id, err);
                        (void)ma_booking_info_set_status(info, MA_BOOKING_INFO_STATUS_DELIVERED);
                        //(void)ma_booking_info_set_active(info, MA_FALSE);
                        if(MA_OK == (err = ma_booker_add(service->booker, info))) {
                            ma_variant_t *v = NULL;
                            char msg[MA_MAX_BUFFER_LEN+1] = {0};

                            MA_MSC_SELECT(_snprintf, snprintf)(msg, MA_MAX_BUFFER_LEN, "Your order %s has been DELIVERED. Thank you again for placing order with mileaccess.com ", id);
                            (void)ma_sms_send(MA_HTTP_NON_SECURE_PROTOCOL, MA_MILEACCESS_HOST, MA_MILEACCESS_PINCODE_MAPPER_PORT, MA_SMS_SEND_PATH, user_contact, msg); 
                            if(MA_OK == (err = ma_booking_info_convert_to_variant(info, &v))) {
                                (void)ma_task_set_app_payload(task, v);
                                (void)ma_variant_release(v);
                            }
                            MA_LOG(service->logger, MA_LOG_SEV_TRACE, "updating booking status(DELIVERED)");
                        } else {
                            MA_LOG(service->logger, MA_LOG_SEV_TRACE, "updating booking status(DELIVERED) failed last error(%d)", err);
                        }
                        (void)ma_booking_service_task_stop(service, booking_id);
                        (void)ma_booking_service_task_remove(service, booking_id);
                    } else if(MA_BOOKING_INFO_STATUS_CANCELLED == status) {
                        char picker_contact[MA_MAX_LEN+1] = {0};
                        char today_str[MA_MAX_LEN+1]= {0};

                        (void)current_date_str(today_str);
                        (void)recorder_remove_pending(recorder, today_str, id);
                        (void)recorder_add_complete(recorder, today_str, id);
                        (void)ma_booking_info_get_picker_contact(info, picker_contact);
                        (void)consign_remove_pending(consignor, user_contact, id);
                        (void)consign_add_complete(consignor, user_contact, id);
                        (void)consign_remove_pending(consignor, picker_contact, id);
                        (void)consign_add_complete(consignor, picker_contact, id);
                        MA_LOG(service->logger, MA_LOG_SEV_TRACE, "Booking Task id(%s) status (CANCELLED)", id);
                        if(MA_OK == (err = ma_booking_service_task_update_status(service, task_id, MA_TASK_EXEC_SUCCESS)))
                            MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "task id(%s) status updated successfully", task_id);
                        else
                            MA_LOG(service->logger, MA_LOG_SEV_ERROR, "task id(%s) status update failed, last error(%d)", task_id, err);
                        (void)ma_booking_info_set_status(info, MA_BOOKING_INFO_STATUS_CANCELLED);
                        //(void)ma_booking_info_set_active(info, MA_FALSE);
                        if(MA_OK == (err = ma_booker_add(service->booker, info))) {
                            ma_variant_t *v = NULL;

                            if(MA_OK == (err = ma_booking_info_convert_to_variant(info, &v))) {
                                 char msg[MA_MAX_BUFFER_LEN+1] = {0};

                                 MA_MSC_SELECT(_snprintf, snprintf)(msg, MA_MAX_BUFFER_LEN, "Your order %s has been CANCELLED. Thank you again for placing order with mileaccess.com ", id);
                                 (void)ma_sms_send(MA_HTTP_NON_SECURE_PROTOCOL, MA_MILEACCESS_HOST, MA_MILEACCESS_PINCODE_MAPPER_PORT, MA_SMS_SEND_PATH, user_contact, msg); 
                                (void)ma_task_set_app_payload(task, v);
                                (void)ma_variant_release(v);
                            }
                            MA_LOG(service->logger, MA_LOG_SEV_TRACE, "updating booking status(ASSIGNED)");
                        } else {
                            MA_LOG(service->logger, MA_LOG_SEV_TRACE, "updating booking status(ASSIGNED) failed last error(%d)", err);
                        }
                        (void)ma_booking_service_task_stop(service, booking_id);
                        (void)ma_booking_service_task_remove(service, booking_id);
                    } else if(MA_BOOKING_INFO_STATUS_NOT_DELIVERED == status) {
                        char today_str[MA_MAX_LEN+1]= {0};

                        (void)current_date_str(today_str);
                        (void)recorder_remove_pending(recorder, today_str, id);
                        (void)recorder_add_complete(recorder, today_str, id);
                        MA_LOG(service->logger, MA_LOG_SEV_TRACE, "Booking Task id(%s) status (NOT DELIVERED)", id);
                        if(MA_OK == (err = ma_booking_service_task_update_status(service, task_id, MA_TASK_EXEC_SUCCESS)))
                            MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "task id(%s) status updated successfully", task_id);
                        else
                            MA_LOG(service->logger, MA_LOG_SEV_ERROR, "task id(%s) status update failed, last error(%d)", task_id, err);
                        (void)ma_booking_info_set_status(info, MA_BOOKING_INFO_STATUS_NOT_DELIVERED);
                        //(void)ma_booking_info_set_active(info, MA_FALSE);
                        if(MA_OK == (err = ma_booker_add(service->booker, info))) {
                            ma_variant_t *v = NULL;

                            if(MA_OK == (err = ma_booking_info_convert_to_variant(info, &v))) {
                                char msg[MA_MAX_BUFFER_LEN+1] = {0};

                                MA_MSC_SELECT(_snprintf, snprintf)(msg, MA_MAX_BUFFER_LEN, "Unfortunately, we regret your order %s could not DELIVERED. Thank you again for placing order with mileaccess.com ", id);
                                (void)ma_sms_send(MA_HTTP_NON_SECURE_PROTOCOL, MA_MILEACCESS_HOST, MA_MILEACCESS_PINCODE_MAPPER_PORT, MA_SMS_SEND_PATH, user_contact, msg); 
                                (void)ma_task_set_app_payload(task, v);
                                (void)ma_variant_release(v);
                            }
                            MA_LOG(service->logger, MA_LOG_SEV_TRACE, "updating booking status(ASSIGNED)");
                        } else {
                            MA_LOG(service->logger, MA_LOG_SEV_TRACE, "updating booking status(ASSIGNED) failed last error(%d)", err);
                        }
                    } else if(MA_BOOKING_INFO_STATUS_PICKED == status) {
                        MA_LOG(service->logger, MA_LOG_SEV_TRACE, "Booking Task id(%s) status (PICKED)", id);
                        if(MA_OK == (err = ma_booking_service_task_update_status(service, task_id, MA_TASK_EXEC_SUCCESS)))
                            MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "task id(%s) status updated successfully", task_id);
                        else
                            MA_LOG(service->logger, MA_LOG_SEV_ERROR, "task id(%s) status update failed, last error(%d)", task_id, err);
                        (void)ma_booking_info_set_status(info, MA_BOOKING_INFO_STATUS_PICKED);
                        if(MA_OK == (err = ma_booker_add(service->booker, info))) {
                            ma_variant_t *v = NULL;

                            if(MA_OK == (err = ma_booking_info_convert_to_variant(info, &v))) {
                                (void)ma_task_set_app_payload(task, v);
                                (void)ma_variant_release(v);
                            }
                            MA_LOG(service->logger, MA_LOG_SEV_TRACE, "updating booking status(ASSIGNED)");
                        } else {
                            MA_LOG(service->logger, MA_LOG_SEV_TRACE, "updating booking status(ASSIGNED) failed last error(%d)", err);
                        }
                    } else {
                        /* do nothing */
                    }
                    (void)ma_booking_info_release(info);
                }
                (void)ma_variant_release(payload);
            }
            (void)ma_task_release(task);
        }
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_bool_t check_booking_still_active(ma_booker_t *service, const char *booking_id) {
    if(service && booking_id) {
        ma_error_t err = MA_OK;
        ma_bool_t active = MA_FALSE;
        ma_booking_info_t *info = NULL;

        if(MA_OK == (err = ma_booker_get(service, booking_id, &info))) {
            (void)ma_booking_info_get_active(info, &active);
            (void)ma_booking_info_release(info);
        }
        return active;
    }
    return MA_FALSE;
}

ma_error_t booking_subscriber_cb(const char *topic, ma_message_t *message, void *cb_data) {
    if(topic && message && cb_data) {
        ma_error_t err = MA_OK;
        ma_booking_service_t *service = (ma_booking_service_t*)cb_data;

        MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "booking subscriber received task message(%s)", topic);
        if(!strcmp(topic, MA_TASK_EXECUTE_PUBSUB_TOPIC) || !strcmp(topic, MA_TASK_STOP_PUBSUB_TOPIC) || !strcmp(MA_TASK_REMOVE_PUBSUB_TOPIC, topic) || !strcmp(topic, MA_TASK_UPDATE_STATUS_PUBSUB_TOPIC)) {
            const char *task_type = NULL , *task_owner_id = NULL , *task_id = NULL;

            MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "booking processing (%s) message", topic);
            if( (MA_OK == (err = ma_message_get_property(message, MA_TASK_TYPE, &task_type))) && task_type &&
                (MA_OK == (err = ma_message_get_property(message, MA_TASK_PRODUCT_ID, &task_owner_id))) && task_owner_id &&
                (MA_OK == (err = ma_message_get_property(message, MA_TASK_ID, &task_id))) && task_id) {
                if(!strcmp(task_type, MA_BOOKING_TASK_TYPE)) {
                    ma_bool_t active = check_booking_still_active(MA_CONTEXT_GET_BOOKER(service->context), task_id);
                    if(active) {
                        MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "booking task id(%s) processing request received", task_id);
                        (void)order_router_handler(service, task_id);
                    } else {
                        MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "booking task id(%s) inactive ", task_id);
                    }
                } else
                    MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "task id(%s) is not booking task", task_id);
            }
        } else {
            /* do nothing */
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}


