#include "stdafx.h"

#include "ma/internal/utils/aac_defs/ma_aac_defs.h"
#include "ma/ma_selfprotect.h"

#include "ma/logger/ma_outputdebugstring_logger.h"

#include <atlsecurity.h>

static ma_logger_t *logger = NULL;


ma_error_t ma_aac_enable_disable_rules(LPCWSTR which, ma_bool_t enable) {
    
    bool consider_all = 0==StrCmpICW(L"all", which);
    bool consider_ma = 0==StrCmpICW(L"ma", which);

    const ma_uint32_t products = (consider_ma ? MA_SELFPROTECT_PRODUCT_MA : 0) | (consider_all ? 0xff : 0);
    
    ma_uint32_t tmp;
    ma_error_t rc = ma_selfprotect_query_status(&tmp);
    MA_LOG(logger, MA_LOG_SEV_INFO, "products before: %u", tmp);

    ma_selfprotect_enable(products, enable);

    rc = ma_selfprotect_query_status(&tmp),  
    MA_LOG(logger, MA_LOG_SEV_INFO, "products after: %u", tmp);

    return rc;
;

    /*

    int rc; 
    if (ERROR_SUCCESS == (rc = AacControlCreate(&aac_control, MA_AAC_PRODUCT_GUID))) {
        AacActivePolicyList *policy_list = nullptr;
        if (ERROR_SUCCESS == (rc = aac_control->GetActivePolicyList(&policy_list, &MA_AAC_PRODUCT_GUID))) {
            std::size_t count = policy_list->GetPolicyCount();
            LPCGUID policy_guids = policy_list->GetPolicyList();
            for (std::size_t i=0; i!= count; ++i) {
                const GUID &policy_guid = policy_guids[i];

                if (MA_SYSCORE_POLICY_GUID == policy_guid) {
                    if (!consider_all) continue;
                } else if (MA_SYSCORE_AAC_POLICY_GUID == policy_guid) {
                    if (!consider_all) continue;
                } else {
                    if (!consider_all && !consider_ma) continue;
                }

                AacPolicy *policy = nullptr;
                if (ERROR_SUCCESS == policy_list->GetPolicy(&policy, policy_guid)) {
                    AacRule *rule = nullptr;
                    while (rule = policy->GetNextRule(rule)) {
                        if (AAC_REACTION_BLOCK != rule->GetReaction()) {
                            // skip anything but blocking rules
                            continue;
                        }
                        const GUID &rule_guid = rule->GetUid();
                        LPCWSTR description = rule->GetDescription();
                        rc = aac_control->EnableDisableRule(policy_guid, rule_guid, MA_FALSE != enable);

                        if (rc) {
                            MA_LOG(logger, MA_LOG_SEV_WARNING, "Failed %sabling rule <%ls>", enable ? "en" : "dis", description);
                        } else {
                            MA_LOG(logger, MA_LOG_SEV_INFO, "Successfully %sabled rule <%ls>", enable ? "en" : "dis", description);
                        }

                    }
                    policy->Release();
                }
            }
            policy_list->Release();
        }
        aac_control->Release();
        AacDllUnload();
    }
    MA_LOG(logger, MA_LOG_SEV_INFO, "AAC operations completed with a result of %d", rc);

    return ma_error_t(rc);
    */
}


/* This can be used even when the service isn't running but probably shouldn't be released to the public */

extern "C" ma_error_t CALLBACK ma_aac_ctrlW(HWND hwnd, HINSTANCE hinst, LPWSTR lpszCmdLine, int nCmdShow) {

    ma_outputdebugstring_logger_create(&logger);

    bool enable = false, disable = false;

    MA_LOG(logger, MA_LOG_SEV_INFO, "ma_aac_ctrl(,,\"%ls\",)",lpszCmdLine);

    if (0 == StrCmpNICW(L"enable",lpszCmdLine,6)) {
        lpszCmdLine += 6;
        enable = true;
    } else if (0 == StrCmpNICW(L"disable",lpszCmdLine,7)) {
        lpszCmdLine += 7;
        disable = true;
    }
    if (!enable && !disable) return MA_OK;

    if ('=' == *lpszCmdLine) ++lpszCmdLine;

    ma_error_t rc = ma_aac_enable_disable_rules(*lpszCmdLine ? lpszCmdLine : L"all", enable);

    ma_logger_release(logger);

    return rc;
}