#include "stdafx.h"

#include "ma/internal/services/aac_service/aac_service.h"
#include "ma/internal/core/msgbus/ma_msgbus_private.h"
#include "ma/internal/utils/aac_defs/ma_aac_defs.h"
#include "ma/internal/services/ma_policy_settings_bag.h"
#include "ma/ma_log.h"
#include "ma/logger/ma_logger.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/defs/ma_general_defs.h"
#include "ma/internal/utils/context/ma_context.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/utils/conf/ma_conf.h"
#include "ma/internal/utils/database/ma_db.h"
#include "ma/internal/utils/database/ma_db_statement.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/core/msgbus/ma_msgbus_defs.h"
#include "ma/internal/utils/xml/ma_xml.h"
#include "ma/internal/utils/macrypto/ma_crypto.h"
#include "ma/internal/core/msgbus/ma_msgbus_private.h"
#include "ma/msgbus/msgbus.hxx"
#include "resource.h"
#include "ma/internal/buildinfo.h"

#include "aacapi.h"
#include "mmsapi.h"
#include "mfevtpa.h"

#include <atlconv.h>

#include <Shlwapi.h>
#include <strsafe.h>
#include <io.h>
#include <fcntl.h>

//from "sysprocesshook.h" :
#define PROCESS_ALL_ACCESS_LEGACY	(STANDARD_RIGHTS_REQUIRED | SYNCHRONIZE | 0xFFF)
#define THREAD_ALL_ACCESS_LEGACY	(STANDARD_RIGHTS_REQUIRED | SYNCHRONIZE | 0x3FF)

#define DB_TABLE_NAME "auth_process_info"

#include <functional>
#include <new>
#include <string>

#undef LOG_FACILITY_NAME // someone is stealing my log facility name, take it back...
#define LOG_FACILITY_NAME "aac_service"
// We will do this in C++ since VSCore's API is C++ and it seems counterproductive to create a C shim just for this

enum RULE_LOCATION {
    RULE_LOCATION_BUILTIN,
    RULE_LOCATION_DISK,
};

// various hints passed to check_and_apply_rules 
typedef unsigned state_hints_t;

enum STATE_BITS {
    STATE_BIT_RUNNING = 0,
    STATE_BIT_SP_POLICY_CONFIGURED,
    STATE_BIT_SP_POLICY_ENABLED,
};



struct aac_service : ma_service_s, AacEventReporter, mileaccess::ma::msgbus_server::handler {

    aac_service();

    // redirected ma_service methods
    ma_error_t configure(ma_context_t *context, unsigned hint);

    ma_error_t start();

    ma_error_t stop();

    void destroy() {
        delete this;
    }

    ::ma_logger_h get_logger() const {
        return this->logger;
    }

private:
    // AacEventReporter implementation
    virtual void ReportEvent(AacEvent *aac_event);

    virtual void AskPointProduct(AacEvent *aacEvent, AacReactionType *aacReaction, bool *report);

    // msgbus_server::handler implementation
    virtual ma_error_t on_request(mileaccess::ma::msgbus_server &server, mileaccess::ma::message const &request_msg, ma_msgbus_client_request_t *c_request);

    // Handler for AAC_REPORT_TYPE_UPGRADE_IN_PROGRESS
    void ScheduleAacReload();

    AacControl *get_aac_control();

    bool set_state(STATE_BITS which, bool on) { // returns true if state was changed as a result
        const unsigned which_pattern = (1 << which);
        const bool was_on = !!(state & which_pattern);
        if (on && !was_on) {
            state |= which_pattern;
        } else if (!on && was_on) {
            state &= ~which_pattern;
        } else return false;

        check_and_apply_rules(which);
        return true;
    }

    bool check_state(STATE_BITS which) const {
        return !! (this->state & (1 << which));
    }

    void aac_up();

    void aac_down();

    void aac_apply();

    void aac_reload();

    void check_and_apply_rules(state_hints_t hints);

    ma_error_t apply_some_policy_xml(char const *policy_bytes);

    static void timer_cb(uv_timer_t* handle, int status) {
        aac_service *self = (aac_service *) handle->data;
        if (self->timer_handler) {
            (self->*(self->timer_handler))();
        }
    }
    unsigned timeout_value;
    unsigned next_timeout_ms() {
        unsigned result = timeout_value;
        timeout_value = min(2*timeout_value,30*60*1000);
        return result;
    }

    void reset_timeout_value() {
        timeout_value = 15000;
    }

    bool IsTrustedViaAuthenticode(unsigned pid, LPCWSTR pathname);

    bool IsTrustedViaCatalog(unsigned pid, LPCWSTR pathname);

	ma_error_t calculate_hash(int &exe_fd, ma_bytebuffer_t *hash_buffer);

	bool verify_binary_from_catalog(int &exe_fd, const char *manifest);

    void save_process_attributes(unsigned pid, LPCWSTR pathname, LPCWSTR subject_name, LPCWSTR signer_name);

private:
    ~aac_service();

    ma_logger_h                 logger, aac_event_log;

    ma_crypto_h                 crypto;

    ma_db_h                     db_msgbus;

    ma_msgbus_h                 msgbus;

    ma_event_loop_h             event_loop;

    struct uv_loop_s            *uv_loop;

    ma_loop_worker_h            loop_worker;

    uv_timer_t                  uv_timer;

    mileaccess::ma::msgbus_server   aac_msgbus_server;

    AacControl                  *aac_control; // please access via get_aac_control()

    unsigned                    state; // combination of 1 << STATE_BITS

    bool                        enforce_ma_policy_onto_syscore;

    void (aac_service::*timer_handler) (); 

    struct process_attributes {
        aac_service *parent;
        unsigned pid;
        LPCSTR pathname, subject_name, signer_name;
    };

    static void aac_service::save_process_info_worker(process_attributes *pa);
};

template <typename FOID, typename P>
ma_error_t DEFUSE(FOID foid, P *p) {
    try {
        return foid(p);
    } catch (mileaccess::ma::exception const &e) {
        MA_LOG(p->get_logger(), MA_LOG_SEV_WARNING, "Unhandled exception from %s : %ld", e.what(), (long) e.err());
        return e.err();
    }
    catch (std::exception const &e) {
        e.what();
    }
    catch (...) {

    }
    
    MA_LOG(p->get_logger(), MA_LOG_SEV_WARNING, "Unhandled and unknown exception !");

    return MA_ERROR_UNEXPECTED;
}

static ma_error_t aac_service_configure(ma_service_t *service, ma_context_t *context, unsigned hint) {
    return DEFUSE(std::bind( std::mem_fn(&aac_service::configure), std::placeholders::_1, context, hint), (static_cast<aac_service*>(service)));
}

static ma_error_t aac_service_start(ma_service_t *service) {
    return DEFUSE(std::mem_fn(&aac_service::start), (static_cast<aac_service*>(service)));
}

static ma_error_t aac_service_stop(ma_service_t *service) {
    return DEFUSE(std::mem_fn(&aac_service::stop), (static_cast<aac_service*>(service)));
}

static void aac_service_destroy(ma_service_t *service) {
    std::mem_fn(&aac_service::destroy), (static_cast<aac_service*>(service));
}


static const ma_service_methods_t aac_service_methods = {
    &aac_service_configure,
    &aac_service_start,
    &aac_service_stop,
    &aac_service_destroy
};


static const WCHAR access_digits[] = L"CWRDXE"; // each letter corresponds to the bit position of the AacBitmaskType flag in aac_am

struct ma_aac_text_formatter {
    WCHAR aac_mask_text[_countof(access_digits)];

    ma_aac_text_formatter(AacBitmaskType aac_am) {
        unsigned mask = 1;
        WCHAR *p = aac_mask_text;
        for (int i=0; i < _countof(access_digits); ++i, mask *= 2) {
            if (aac_am & mask) {
                *p++ = access_digits[i];
            }
        }
        *p = 0;
    }

    LPCWSTR get_buffer() const {
        return aac_mask_text; 
    }
};

MA_AAC_SERVICE_API ma_error_t ma_aac_service_create(char const *service_name, ma_service_t **service) {
    if (aac_service *svc = new (std::nothrow) aac_service) {
        ma_service_init(svc, &aac_service_methods, "aac_service", svc);
        *service = svc;
        return MA_OK;
    }
    return MA_ERROR_OUTOFMEMORY;
}



aac_service::aac_service() : logger(nullptr), aac_event_log(nullptr), crypto(nullptr), db_msgbus(nullptr), msgbus(nullptr), event_loop(nullptr), aac_msgbus_server(MA_AAC_SERVICE_NAME), state(0), aac_control(nullptr), enforce_ma_policy_onto_syscore(true), timer_handler(&aac_service::aac_apply) {
    reset_timeout_value();
}

aac_service::~aac_service() {
    ma_logger_release(aac_event_log);
    ma_logger_release(logger);
}


ma_error_t aac_service::configure(ma_context_t *context, unsigned hint) {
    
    ma_logger_release(logger);
    ma_logger_add_ref(logger = MA_CONTEXT_GET_LOGGER(context));

    // TODO: Use a dedicated access protection log
    ma_logger_release(aac_event_log);
    ma_logger_add_ref(aac_event_log = MA_CONTEXT_GET_LOGGER(context));

    if (!msgbus) {
        msgbus = MA_CONTEXT_GET_MSGBUS(context);
        aac_msgbus_server.create(msgbus);
        aac_msgbus_server.set_option(MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
        event_loop = ma_msgbus_get_event_loop(msgbus);

        ma_event_loop_get_loop_impl(event_loop,&uv_loop);

        uv_timer_init(uv_loop, &uv_timer);
        uv_timer.data = this;
        MA_LOG(logger, MA_LOG_SEV_DEBUG, "uv timer data(%p)", this);

        loop_worker = ma_msgbus_get_loop_worker(msgbus);
    }

    if (!crypto) {
        crypto = MA_CONTEXT_GET_CRYPTO(context);
    }

    if (!db_msgbus) {
        if (ma_db_h db = ma_configurator_get_msgbus_database(MA_CONTEXT_GET_CONFIGURATOR(context))) {

            // create database tables for authentication info 
            ma_error_t rc = ma_db_transaction_begin(db);

            if (MA_SUCCEEDED(rc)) {
                ma_db_statement_t *db_stmt = NULL;
            
                MA_SUCCEEDED ( rc = ma_db_statement_create(db, "DROP TABLE IF EXISTS " DB_TABLE_NAME, &db_stmt)) &&
                MA_SUCCEEDED ( rc = ma_db_statement_execute(db_stmt));

                ma_db_statement_release(db_stmt), db_stmt = NULL;

                MA_SUCCEEDED ( rc ) &&
                MA_SUCCEEDED ( rc = ma_db_statement_create(db, "CREATE TABLE " DB_TABLE_NAME " (pid INTEGER PRIMARY KEY ON CONFLICT REPLACE, exe_path TEXT, subject_name TEXT, signer_name TEXT)", &db_stmt)) &&
                MA_SUCCEEDED ( rc = ma_db_statement_execute(db_stmt));


                ma_db_statement_release(db_stmt);

                if (ma_error_t e2 = ma_db_transaction_end(db)) {
                    rc = e2;
                }
            }

            if (MA_SUCCEEDED(rc)) {
                db_msgbus = db;
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "Successfully created database table " DB_TABLE_NAME " for msgbus authentication data");
            } else {
                MA_LOG(logger, MA_LOG_SEV_ERROR, "Unable to create database table for msgbus authentication data, error: <%ld>", (long) rc);
            }
        }
    }

    ma_bool_t enabled = MA_TRUE;
    if (MA_OK == ma_policy_settings_bag_get_bool(MA_CONTEXT_GET_POLICY_SETTINGS_BAG(context), MA_AGENT_GENERAL_SECTION_NAME_STR, MA_AGENT_SELF_PROTECTION_ENABLED_STR, MA_FALSE,&enabled, MA_TRUE)){
        MA_LOG(logger, MA_LOG_SEV_INFO, "Self Protection is %sabled in policy", enabled ? "en" : "dis");
        set_state(STATE_BIT_SP_POLICY_ENABLED, MA_FALSE != enabled);
        set_state(STATE_BIT_SP_POLICY_CONFIGURED, true);
    }
    
    return MA_OK;
}

ma_error_t aac_service::start() {
    
    // hook up to receive msgbus messages
    if (msgbus) {
        this->aac_msgbus_server.start(*this);
    }

    /* Set some environment variables referenced by the AAC rule xml */
    CRegKey hklms_agent;
    if (ERROR_SUCCESS == hklms_agent.Open(HKEY_LOCAL_MACHINE, _T(MA_CONFIGURATION_REGISTRY_AGENT_FULL_PATH_STR), KEY_QUERY_VALUE|KEY_WOW64_32KEY)) {
        TCHAR agent_program_path[MAX_PATH];
        ULONG nChars = _countof(agent_program_path);
        if (ERROR_SUCCESS == hklms_agent.QueryStringValue(_T(MA_CONFIGURATION_AGENT_KEY_INSTALL_PATH_STR), agent_program_path, &nChars)) {
            SetEnvironmentVariable(_T("AGENT_PROGRAM_PATH"), agent_program_path);
        }
        nChars = _countof(agent_program_path);
        if (ERROR_SUCCESS == hklms_agent.QueryStringValue(_T(MA_CONFIGURATION_AGENT_KEY_DATA_PATH_STR), agent_program_path, &nChars)) {
            SetEnvironmentVariable(_T("AGENT_DATA_PATH"), agent_program_path);
        }
    }

    set_state(STATE_BIT_RUNNING, true);
    return MA_OK;
}

ma_error_t aac_service::stop() {
    this->aac_msgbus_server.stop();

    set_state(STATE_BIT_RUNNING, false);
    uv_timer_stop(&uv_timer);
    uv_close((uv_handle_t *)&uv_timer, 0);    
    return MA_OK;
}

void aac_service::aac_up() {
    uv_timer_stop(&uv_timer);
    if (nullptr == get_aac_control()) {
        unsigned timeout_ms = next_timeout_ms();
        MA_LOG(logger, MA_LOG_SEV_DEBUG, "Aac not up, retrying in %u secs...", timeout_ms/1000);
        uv_timer_start(&uv_timer, aac_service::timer_cb, timeout_ms, 0);
    } else {
        reset_timeout_value();
    }
}

void aac_service::aac_reload() {
    MA_LOG(logger, MA_LOG_SEV_INFO, "performing Aac reload");
    aac_down();
    aac_up();
}

void aac_service::aac_down() {
    if (aac_control) {
        MA_LOG(logger, MA_LOG_SEV_DEBUG, "Unregistering with and releasing Aac...");
        aac_control->StopReportEvents();
        aac_control->Release();
        aac_control = nullptr;
        AacDllUnload();
    }
}

void aac_service::aac_apply() {

    aac_up();
    
    if (!aac_control) return;

    if (false) {
        //load from a file
    } else {
        if (HRSRC res = FindResource(aac_module, MAKEINTRESOURCE(IDR_AAC_POLICY), RT_RCDATA)) {
            if (HGLOBAL hGlobal = LoadResource(aac_module, res)) {

                const char *p = static_cast<char*>(LockResource(hGlobal));
                DWORD res_size = SizeofResource(aac_module, res);

                // AacControl::AddPolicyFromXml requires null terminated string, hence making a copy to ensure null termination!
                std::string xml(p,res_size);
                MA_LOG(logger, MA_LOG_SEV_INFO, "Applying embedded Aac policies..."); 
                if (ma_error_t err = apply_some_policy_xml(xml.c_str())) {
                    MA_LOG(logger, MA_LOG_SEV_WARNING, "Failed applying embedded Aac polices, err = <0x%08X>", (long) err);
                    return;
                } else {
                    MA_LOG(logger, MA_LOG_SEV_INFO, "Successfully applied embedded Aac policies"); 
                }
            }
        }
    }
    
    const bool sp_enabled = check_state(STATE_BIT_SP_POLICY_ENABLED);
    if (sp_enabled) {
        MA_LOG(logger, MA_LOG_SEV_INFO, "Now enabling " MA_AAC_SELF_PROTECTION_GROUP " group"); 
        aac_control->EnableDisableGroup(MA_AAC_SELF_PROTECTION_POLICY_GUID, _CRT_WIDE(MA_AAC_SELF_PROTECTION_GROUP), true);
        // Below is an 'ask point product' rule
        aac_control->EnableDisableGroup(MA_AAC_MSGBUS_AUTH_POLICY_GUID, _CRT_WIDE(MA_AAC_SELF_PROTECTION_GROUP), true);
    } else {
        MA_LOG(logger, MA_LOG_SEV_INFO, "NOT enabling MA Self Protection"); 
    }

    if (this->enforce_ma_policy_onto_syscore) {
        MA_LOG(logger, MA_LOG_SEV_INFO, "%sabling SystemCore Self Protection", sp_enabled ? "En" : "Dis" ); 
        aac_control->EnableDisableGroup(MA_SYSCORE_POLICY_GUID, _CRT_WIDE(MA_SYSCORE_GROUP_NAME), sp_enabled);
        aac_control->EnableDisableGroup(MA_SYSCORE_AAC_POLICY_GUID, _CRT_WIDE(MA_SYSCORE_GROUP_NAME), sp_enabled);
    }

}

void aac_service::ScheduleAacReload() {
    // Again, this may come in on any thread
    // just create a message and send it to ourself (or could use loop worker)
    MA_LOG(logger, MA_LOG_SEV_INFO, "Scheduling Aac reload...");

    try {
        mileaccess::ma::msgbus_endpoint ep(msgbus, MA_AAC_SERVICE_NAME);

        mileaccess::ma::table t;
        t.set_entry("method", mileaccess::ma::variant(MA_AAC_DRIVER_RELOAD));

        mileaccess::ma::message msg;
        msg.set_payload(mileaccess::ma::variant(t));
        ep.send_and_forget(msg);

    } catch (std::exception const &e) {
        MA_LOG(logger, MA_LOG_SEV_WARNING, "Unable to schedule Aac reload message, not reloading Aac! Error = <%s>", e.what());
    }
}

// msgbus server handler
ma_error_t aac_service::on_request(mileaccess::ma::msgbus_server &server, mileaccess::ma::message const &request_msg, ma_msgbus_client_request_t *c_request) {
    MA_LOG(logger, MA_LOG_SEV_INFO, "Got a msgbus request, checking client ident...");
    /*TBD handle the auth info here */
    
    // there's just one message type...

    aac_reload();
    return MA_OK;
}


static bool is_vtp_up() {
    bool up = false;
    MmsControl *mms_ctrl;
    if (0 == MmsControlCreate(&mms_ctrl)) {
        if (MSManager *mms_mgr =  mms_ctrl->OpenMSManager()) {
            if (MmsService *mfemms = mms_mgr->OpenService(L"mfevtp", 0)) {
                DWORD status = 0;
                mfemms->QueryServiceStatus(&status);
                up = SERVICE_RUNNING == status;
                mfemms->Release();
            }
            mms_mgr->Release();
        }
        mms_ctrl->Release();
        MmsDllUnload();
    }
    return up;
}

AacControl *aac_service::get_aac_control() {
    if (!this->aac_control) {
        if (!is_vtp_up()) {
            MA_LOG(logger, MA_LOG_SEV_WARNING, "mfevtp is not up");
            return nullptr;
        }

        int rc = ::AacControlCreate(&this->aac_control, MA_AAC_PRODUCT_GUID, _CRT_WIDE(MA_VERSION_PRODUCTNAME));
        if (rc) {
            MA_LOG(logger, MA_LOG_SEV_WARNING, "Unable to create AacControl interface, error = <%d>", rc);
        } else {
            MA_LOG(logger, MA_LOG_SEV_INFO, "Using Aac version <%S>", (const wchar_t *) this->aac_control->GetVersion() );

            MA_LOG(logger, MA_LOG_SEV_DEBUG, "Registering for Aac events...");

            TCHAR my_process_path[MAX_PATH];
            GetModuleFileName(NULL, my_process_path, _countof(my_process_path));
            DWORD my_pid = GetCurrentProcessId();
            // Validate self (most importantly insert information about self into db)
            IsTrustedViaAuthenticode(my_pid, my_process_path) || IsTrustedViaCatalog(my_pid, my_process_path);

            // at the same time start listening for events
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "Registering for Aac events...");
            int rc = aac_control->StartReportEvents(this, AAC_REPORT_ALL_OBJECTS, AAC_DEFAULT_REPORT_FIELDS, AAC_REPORT_FIELD_RULE_DESCRIPTION);
            if (rc) {
                MA_LOG(logger, MA_LOG_SEV_WARNING, "Unable to listen for Aac events, rc=<0x%X>", rc);
            } else {
                MA_LOG(logger, MA_LOG_SEV_INFO, "Successfully registered for Aac events");
            }
        }
    }
    return this->aac_control;
}

void aac_service::check_and_apply_rules(unsigned hints) {
    MA_LOG(logger, MA_LOG_SEV_DEBUG, "check_and_apply_rules state is now %x", state);
    
    if (!check_state(STATE_BIT_RUNNING)) {
        aac_down();
        return;
    }
    
    if (!check_state(STATE_BIT_SP_POLICY_CONFIGURED)) return; // don't do anything without a policy; wait for policy to become present
    
    aac_up();

    if (!this->aac_control) return;

    aac_apply();
}

ma_error_t aac_service::apply_some_policy_xml(char const *policy_bytes) {
    if (AacControl *aac_control = get_aac_control()) {
        // Consider using CryptCreateHash(), CryptGetHashParam 
        return (ma_error_t) aac_control->AddPolicyFromXml(policy_bytes);
    }
    return MA_ERROR_PRECONDITION;
}


bool  aac_service::IsTrustedViaAuthenticode(unsigned pid, LPCWSTR pathname) {
    VtpValidationControl *vtpValidationControl = nullptr;
    bool trusted = false;
    if (ERROR_SUCCESS == VtpValidationControlCreate(&vtpValidationControl) && vtpValidationControl) {
        VtpBaseReportData *vtpReportData; 
        if (ERROR_SUCCESS == vtpValidationControl->ValidateModuleEx(pathname, 0, &vtpReportData)) {
            if (vtpReportData->vtpTrustResult.privileges & PRIVILEGE_IOCTL) {
                // TODO: parse vtpReportData->moduleSigner properly.
                // TOOD: list of approved CNs must be configurable
                if (!!wcsstr(vtpReportData->moduleSigner,L"CN=McAfee") || !!wcsstr(vtpReportData->moduleSigner,L"CN=\"McAfee") ) { // this also matches McAfee Test if Test Certs are configured 
                    trusted = true;
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "executable <%ls> is msgbus trusted. Signer: <%ls>", vtpReportData->moduleName, vtpReportData->moduleSigner);
                    save_process_attributes(pid, pathname, vtpReportData->moduleSigner, vtpReportData->moduleSigner);
                } else {
                    MA_LOG(logger, MA_LOG_SEV_INFO, "executable <%ls> is vtp trusted, but not msgbus trusted. Signer: <%ls>", vtpReportData->moduleName, vtpReportData->moduleSigner);
                }
            } else {
                MA_LOG(logger, MA_LOG_SEV_INFO, "executable <%ls> is not vtp trusted, hence not msgbus trusted", vtpReportData->moduleName);
            }
            vtpReportData->Release();
        }
        vtpValidationControl->Release();
        MfeVtpaDllUnload();
    }
    return trusted;
}

/*NOTE TBD - This whole code is getting duplicated at msgbus and here, we need to consolidate it at one place 
	Windows doesnt do anything other than executable validation 
*/
ma_error_t aac_service::calculate_hash(int &exe_fd, ma_bytebuffer_t *hash_buffer) {
	ma_error_t rc = MA_OK;
	ma_crypto_hash_t *hash_object = NULL;
	if(MA_OK == (rc = ma_crypto_hash_create(crypto, MA_CRYPTO_HASH_SHA256, &hash_object))) {
		unsigned char buffer[2048]= {0};
		int bytes_read = 0;
		unsigned short is_all_good = 1;
		while((bytes_read = read(exe_fd,buffer, 2048)) > 0) {
			if(MA_OK != (rc = ma_crypto_hash_data_update(hash_object, buffer, bytes_read)))
				break;
		}
		if(MA_OK == rc) 
			rc = ma_crypto_hash_data_final(hash_object, hash_buffer);
		else
			MA_LOG(logger, MA_LOG_SEV_ERROR, "failed to  calculate hash  for %d, %d", exe_fd, rc);

		(void)ma_crypto_hash_release(hash_object);
	}else
		MA_LOG(logger, MA_LOG_SEV_ERROR, "failed to create hash object for %d, %d", exe_fd, rc);
	return rc;
}



bool aac_service::verify_binary_from_catalog(int &exe_fd, const char *manifest) {
	int r = -1;
	ma_error_t rc = MA_OK;
	bool bRet = false;
	ma_bytebuffer_t *hash_buffer = NULL;
	if(MA_OK == (rc = ma_bytebuffer_create(MA_MSGBUS_AUTH_EXE_SHA256_DIGEST_LENGTH, &hash_buffer))) {
		if(MA_OK == (rc = calculate_hash(exe_fd, hash_buffer))) {
			ma_xml_t *xml = NULL;
			if(MA_OK == (rc = ma_xml_create(&xml))) {
				if(MA_OK == (rc = ma_xml_load_from_buffer(xml, manifest, strlen(manifest)))) {
					ma_xml_node_t *root = NULL;
					if((root = ma_xml_get_node(xml))) {
						ma_xml_node_t *node = NULL;
						if((node = ma_xml_node_search_by_path(root, MA_MSGBUS_AUTH_NODE_XML_PATH_STR))) {
							do {
								const char *name = ma_xml_node_attribute_get(node, MA_MSGBUS_KEY_AUTH_SELF_NAME_STR);
                                const char *sha256 = ma_xml_node_attribute_get(node, MA_MSGBUS_KEY_AUTH_SELF_HASH_STR);
								unsigned char decoded_hash[MA_MSGBUS_AUTH_EXE_SHA256_DIGEST_LENGTH] = {0};
								ma_crypto_hex_decode((const unsigned char *)sha256, strlen(sha256), decoded_hash, MA_MSGBUS_AUTH_EXE_SHA256_DIGEST_LENGTH);
								if(0 == memcmp(decoded_hash, ma_bytebuffer_get_bytes(hash_buffer), MA_MSGBUS_AUTH_EXE_SHA256_DIGEST_LENGTH)) {
									bRet = true;
									break;
								}
							}while(node = ma_xml_node_get_next_sibling(node));
						}else
							MA_LOG(logger, MA_LOG_SEV_ERROR, "no entries found in manifest");
					}else
						MA_LOG(logger, MA_LOG_SEV_ERROR, "failed to get root node in manifest");
				}else
					MA_LOG(logger, MA_LOG_SEV_ERROR, "failed to load manifest xml, %d", rc);
				(void)ma_xml_release(xml);
			}else
				MA_LOG(logger, MA_LOG_SEV_ERROR, "failed to create manifest xml, %d", rc);
		}else
			MA_LOG(logger, MA_LOG_SEV_ERROR, "failed to get hash of executable hash, %d", rc);
		(void)ma_bytebuffer_release(hash_buffer);
	}else
		MA_LOG(logger, MA_LOG_SEV_ERROR, "failed to create buffer, %d", rc);
    return bRet;
}



bool aac_service::IsTrustedViaCatalog(unsigned pid, LPCWSTR pathname) {
    WCHAR catalog_path[MAX_PATH], *file_part = PathFindFileNameW(pathname);
    if (!file_part) {
        MA_LOG(logger, MA_LOG_SEV_WARNING, "Unable to get file name portion of %ls", pathname);
        return false;
    }

    HRESULT hr = StringCchPrintfW(catalog_path, _countof(catalog_path), L"%.*s" _CRT_WIDE(MA_MSGBUS_AUTH_SIG_NAME_STR), file_part-pathname, pathname);
    if (S_OK != hr) {
        MA_LOG(logger, MA_LOG_SEV_WARNING, "Unable to construct sig file path for authentication, string building failed with error %X", hr);
        return false;
    }

    MA_LOG(logger, MA_LOG_SEV_DEBUG, "Using catalog path <%ls> for <%ls>", catalog_path, pathname);

    int cat_fd = -1, exe_fd = -1;
    bool trusted = false;

    if (0 == _wsopen_s(&cat_fd, catalog_path, _O_BINARY | _O_RDONLY, _SH_DENYNO, _S_IREAD) && 
        0 == _wsopen_s(&exe_fd, pathname, _O_BINARY | _O_RDONLY, _SH_DENYNO, _S_IREAD)) {
            ma_crypto_cms_t *cms = NULL;
            ma_error_t rc = MA_ERROR_MSGBUS_AUTH_FAILED;
            if(MA_OK == (rc = ma_crypto_cms_create(crypto, &cms))) {
                int is_verified = 0;
                if((MA_OK == (rc = ma_crypto_cms_verify(cms, cat_fd, &is_verified)) && is_verified)) {
					const char *manifest = NULL;
					if(MA_OK == (rc = ma_crypto_cms_get_auth_manifest(cms, &manifest))) {
						if(verify_binary_from_catalog(exe_fd, manifest)) {
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "%ls is %strusted according to catalog", pathname, is_verified ? "" : "UN" );
                    trusted = !!is_verified;
						}else
							MA_LOG(logger, MA_LOG_SEV_ERROR, "failed to verify for with catalog file");
							
					}else
						MA_LOG(logger, MA_LOG_SEV_ERROR, "failed to get manifest for catalog file,%d", rc);
                }else 
					MA_LOG(logger, MA_LOG_SEV_ERROR, "failed to verify signature within catalog file");
                ma_crypto_cms_release(cms);
            } else {
                MA_LOG(logger, MA_LOG_SEV_ERROR, "Unable to create crypto cms object, cannot authenticate!");
            }
    } else {
        // one or both files could not be opened
        MA_LOG(logger, MA_LOG_SEV_INFO, "Could not open both [catalog(fd=%d),executable(fd=%d)] files, status is %d (%s)", cat_fd, exe_fd, (int)errno, strerror(errno));
    }

    cat_fd < 0 || _close(cat_fd);
    exe_fd < 0 || _close(exe_fd);
    
    return trusted;
}


/*static*/
void aac_service::save_process_info_worker(process_attributes *pa) {
    ma_db_statement_t *db_stmt = NULL;
    ma_error_t rc;

    if (!ma_event_loop_is_loop_thread(pa->parent->event_loop)) {
        MA_LOG(pa->parent->logger, MA_LOG_SEV_ERROR, "Attempt to save process info from non i/o thread!");
    }

    MA_SUCCEEDED(rc = ma_db_statement_create(pa->parent->db_msgbus, "INSERT OR REPLACE INTO " DB_TABLE_NAME " VALUES(?,?,?,?)", &db_stmt)) &&
    MA_SUCCEEDED(rc = ma_db_statement_set_int(db_stmt, 1, pa->pid)) && 
    MA_SUCCEEDED(rc = ma_db_statement_set_string(db_stmt, 2, 1, pa->pathname, -1 )) && 
    MA_SUCCEEDED(rc = ma_db_statement_set_string(db_stmt, 3, 1, pa->subject_name, -1)) &&                 
    MA_SUCCEEDED(rc = ma_db_statement_set_string(db_stmt, 4, 1, pa->signer_name, -1)) &&
    MA_SUCCEEDED(rc = ma_db_statement_execute(db_stmt));

    if (MA_FAILED(rc)) {
        MA_LOG(pa->parent->logger, MA_LOG_SEV_ERROR, "Unable to save auth process attributes for pid = <%u>, rc = <%ld>", pa->pid, (long) rc);
    } else {
        MA_LOG(pa->parent->logger, MA_LOG_SEV_DEBUG, "Successfully saved auth process attributes for pid = <%u>", pa->pid);
    }

    (void) ma_db_statement_release(db_stmt);
}

// ANYTHREAD
void aac_service::save_process_attributes(unsigned pid, LPCWSTR pathname, LPCWSTR subject_name, LPCWSTR signer_name) {

    if (db_msgbus) {
        ATL::CW2A utf8_pathname(pathname, CP_UTF8);
        ATL::CW2A utf8_subject_name(subject_name, CP_UTF8);
        ATL::CW2A utf8_signer_name(signer_name, CP_UTF8);

        process_attributes pa;
        pa.parent = this;
        pa.pid = pid;
        pa.pathname = utf8_pathname;
        pa.subject_name = utf8_subject_name;
        pa.signer_name =utf8_signer_name;

        /* Note, only when saved via aac_service::configure will the pid match current process  
           and the process information saved directly from the calling thread
        */
        if (GetCurrentProcessId() == pid) {
            save_process_info_worker(&pa);
        } else {
            /* marshall onto worker thread to comply with threading requirements for sqlite connection */
            ma_loop_worker_submit_work(loop_worker, (ma_work_function) save_process_info_worker, (void *) &pa, true);
        }
    }
}


// callback from AAC for any AAC_REACTION_ASK_PP rules. Note, this may be invoked on any thread
void aac_service::AskPointProduct(AacEvent *aacEvent, AacReactionType *aacReaction, bool *report) {
    if (MA_AAC_MSGBUS_RULE_GUID == aacEvent->GetRuleUid()) {
        *report = true;
        *aacReaction = AAC_REACTION_BLOCK;

        // target
        LPCWSTR target_object_name = aacEvent->GetObjectName();

        // initiator
        LPCWSTR initiator_path = aacEvent->GetProcessName();
        unsigned initiator_pid = (unsigned) aacEvent->GetProcessId();

        AacBitmaskType mask = aacEvent->GetAccessMask();

        MA_LOG(logger, MA_LOG_SEV_DEBUG, "Process <%ls(%u)> trying to access(\'%ls\') pipe <%ls>, authenticating...", initiator_path, initiator_pid, ma_aac_text_formatter(mask).get_buffer(), target_object_name);

        if (GetCurrentProcessId() == initiator_pid) {
            /* Unconditionally allow this process access, information has already been inserted into db */
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "Allowing <%ls(%u)> onto msgbus based on pid", initiator_path, initiator_pid);
            *aacReaction = AAC_REACTION_EXCEPTIONAL_ALLOW; // or use AAC_REACTION_EXCEPTIONAL_ALLOW if there are other blocking rules in effect
            *report = false;
        } else if (IsTrustedViaAuthenticode(initiator_pid, initiator_path)) {
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "Allowing <%ls(%u)> onto msgbus based on Authenticode signature", initiator_path, initiator_pid);
            *aacReaction = AAC_REACTION_EXCEPTIONAL_ALLOW; // or use AAC_REACTION_EXCEPTIONAL_ALLOW if there are other blocking rules in effect
            *report = false;
        } else if (IsTrustedViaCatalog(initiator_pid, initiator_path)) {
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "Allowing <%ls(%u)> onto msgbus based on Catalog signature", initiator_path, initiator_pid);
            *aacReaction = AAC_REACTION_EXCEPTIONAL_ALLOW; // or use AAC_REACTION_EXCEPTIONAL_ALLOW if there are other blocking rules in effect
            *report = false;
        } else {
            MA_LOG(logger, MA_LOG_SEV_WARNING, "Blocking <%ls(%u)> from accessing msgbus pipe <%ls>", initiator_path, initiator_pid, target_object_name);
        }
    }
}

void aac_service::ReportEvent(AacEvent *aac_event) {
    if (!aac_event) return;
    /* Note that we are called on an arbitrary thread, not the i/o thread for sure */

    AacReportType report_type = aac_event->GetReportType();
    const char *report_type_text = AacReportTypeToString(report_type);

    switch (report_type) {

    case AAC_REPORT_TYPE_REACTION: {
        AacReactionType rt = aac_event->GetReaction();
        AacObjectType ot = aac_event->GetObjectType();
        UINT32 am = aac_event->GetWinntAccessMask();
        AacBitmaskType aac_am = aac_event->GetAccessMask();


        
        // ignore some events
        if (AAC_REACTION_ALLOW == rt || AAC_REACTION_EXCEPTIONAL_ALLOW == rt) {
            return;
        }

        if (AAC_OBJECT_PROCESS == ot) {
            if (PROCESS_ALL_ACCESS_LEGACY == (PROCESS_ALL_ACCESS_LEGACY & am))
                return;
            if (!(am & PROCESS_TERMINATE)) {
                return;
            } 
        }

        if (AAC_OBJECT_THREAD == ot) {
            if (THREAD_ALL_ACCESS_LEGACY == (THREAD_ALL_ACCESS_LEGACY & am))
                return;
            if (!(am & THREAD_TERMINATE)) {
                return;
            } 
        }

        const char *reaction_text = AacReactionTypeToString(rt);

        const char *ot_text = AacObjectTypeToString(ot);

        LPCWSTR object_name = aac_event->GetObjectName();

        LPCWSTR process_name = aac_event->GetProcessName();

        LPCWSTR rule_description = aac_event->GetRuleDescription();

        HANDLE pid = aac_event->GetProcessId();

        MA_WLOG(aac_event_log, MA_LOG_SEV_INFO, L"The process <%s>(%u) was blocked from accessing(\'%s\' (%x)) <%S:%s> via the rule <%s>", 
            process_name,
            (unsigned) pid,
            ma_aac_text_formatter(aac_am).get_buffer(),
            (unsigned) aac_am,
            ot_text,
            object_name,
            rule_description);

        break;
        }

    case AAC_REPORT_TYPE_POLICY_UPDATED:
        break;

    case AAC_REPORT_TYPE_POLICY_ADDED:
        break;

    case AAC_REPORT_TYPE_POLICY_DELETED:
        break;

    case AAC_REPORT_TYPE_PP_REGISTERED: 
        // fall through

    case AAC_REPORT_TYPE_PP_UNREGISTERED: {
        /* Not even sure how interesting this information is, there tends to be a lot of it in the logs with some point products */
        OLECHAR guid_text[40] = L"";
        StringFromGUID2(aac_event->GetPointProductUid(), guid_text, _countof(guid_text));
        MA_WLOG(aac_event_log, MA_LOG_SEV_DEBUG, 
            AAC_REPORT_TYPE_PP_REGISTERED == report_type 
                ? L"AAC_REPORT_TYPE_PP_REGISTERED(%s)"
                : L"AAC_REPORT_TYPE_PP_UNREGISTERED(%s)"
            , guid_text); 
        break;
        }
    
    case AAC_REPORT_TYPE_UPGRADE_IN_PROGRESS:
        ScheduleAacReload();
        break;

    default:
        break;
    }
}
