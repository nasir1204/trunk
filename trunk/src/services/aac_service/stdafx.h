// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>

#include <atlbase.h>

#include "aacapi.h"


#define LOG_FACILITY_NAME "aac_service"

extern HMODULE aac_module;

// TODO: reference additional headers your program requires here
