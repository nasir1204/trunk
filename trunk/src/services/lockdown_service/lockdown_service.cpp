// lockdown_service.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "ma/internal/services/ma_service.h"

#include "ma/ma_log.h"

#include "ma/internal/utils/datastructures/ma_queue.h"
#include "ma/internal/defs/ma_object_defs.h"

#include "ma/internal/utils/aac_defs/ma_lockdown_helper.hxx"

#include <process.h>
#include <new>
#include <algorithm>

#include "ma/internal/defs/ma_general_defs.h"

#define SERVICE_NAME "lockdown_service"
#define LOG_FACILITY_NAME "lockdown"


typedef struct ma_event_watcher_s ma_event_watcher_t, *ma_event_watcher_h;

typedef struct ma_event_watcher_item_s ma_event_watcher_item_t, *ma_event_watcher_item_h;

typedef struct ma_event_watcher_item_methods_s ma_event_watcher_item_methods_t, *ma_event_watcher_item_methods_h;

typedef void *ma_event_watcher_event_type;

struct ma_event_watcher_item_methods_s {

    /**
     * init function, executed on the event thread once per registration
     */
    ma_error_t (*init)(ma_event_watcher_item_t *item);

    /**
     * must return the event to be monitored
     * this is called on every loop iteration
     */
    ma_error_t (*get_event)(ma_event_watcher_item_t *item, ma_event_watcher_event_type *event);

    /**
     * invoked when the specified event is raised
     */
    ma_error_t (*signal)(ma_event_watcher_item_t *item); 

    /**
     * invoked on event thread when the item is removed or when 
     */
    ma_error_t (*uninit)(ma_event_watcher_item_t *item); 

};

struct ma_event_watcher_item_s {
    ma_queue_t  qitem;

    void        *event;

    void        *data;



    ma_event_watcher_item_methods_t const *methods;
};

void ma_event_watcher_item_init(ma_event_watcher_item_t *item, ma_event_watcher_item_methods_s const *methods, void *data);


struct ma_lockdown_service : ma_service_s {

    ma_lockdown_service(char const *name) 
        : logger(nullptr)
        , state(MA_SERVICE_STOPPED)
        , thread_signal(FALSE, TRUE) /* initial set*/ 
    {
        ma_service_init(this,&_methods, name, this);
    }

    ~ma_lockdown_service() {
        MA_LOG(logger, MA_LOG_SEV_INFO, "goodbye from " SERVICE_NAME );
    }

    ma_error_t configure(struct ma_context_s *context, unsigned hint);

    ma_error_t start();

    ma_error_t stop();

private :

    ma_logger_t *logger;

    LONG volatile state;

    ATL::CHandle hTread;

    ATL::CEvent thread_signal;

    unsigned run();

    // ma_service_methods :
    static ma_error_t configure(ma_service_t *service, struct ma_context_s *context, unsigned hint) {
        return static_cast<ma_lockdown_service*>(service)->configure(context, hint);
    }

    static ma_error_t start(ma_service_t *service) {
        return static_cast<ma_lockdown_service*>(service)->start();
    }

    static ma_error_t stop(ma_service_t *service) {
        return static_cast<ma_lockdown_service*>(service)->stop();
    }

    static void destroy(ma_service_t *service) {
        delete static_cast<ma_lockdown_service*>(service);
    }

    const static ma_service_methods_t _methods;

    static unsigned __stdcall thread_thunk( void *data) {
        ma_lockdown_service *self = reinterpret_cast<ma_lockdown_service*>(data);
        return self->run();
    }
};

const ma_service_methods_t ma_lockdown_service::_methods = {
    &ma_lockdown_service::configure,
    &ma_lockdown_service::start,
    &ma_lockdown_service::stop,
    &ma_lockdown_service::destroy
};


extern "C" ma_error_t ma_lockdown_service_create(char const *service_name, ma_service_t **service) {
    if (!service) return MA_ERROR_INVALIDARG;

    if (ma_lockdown_service *self = new (std::nothrow) ma_lockdown_service(SERVICE_NAME)) {
        *service = self;
        return MA_OK;
    }

    return MA_ERROR_OUTOFMEMORY;
}


ma_error_t ma_lockdown_service::configure(struct ma_context_s *context, unsigned hint) {

    logger = MA_CONTEXT_GET_LOGGER(context);
    return MA_OK;
}

ma_error_t ma_lockdown_service::start() {

    if (MA_SERVICE_STOPPED != state) {
        return MA_ERROR_UNEXPECTED;
    }

    state = MA_SERVICE_STARTING;
    MA_LOG(logger, MA_LOG_SEV_INFO, SERVICE_NAME " starting...");

    unsigned thrdaddr;
    hTread = ATL::CHandle( (HANDLE) _beginthreadex(NULL, 0, &ma_lockdown_service::thread_thunk, this, 0, &thrdaddr));

    // not sure if this is technically required, just need a barrier on the 'state' variable to ensure proper reading on the thread side
    InterlockedExchange(&state, MA_SERVICE_STARTED);

    MA_LOG(logger, MA_LOG_SEV_INFO, SERVICE_NAME " started");
    return MA_OK;
}

ma_error_t ma_lockdown_service::stop() {
    if (MA_SERVICE_STARTED != state)  {
        return MA_ERROR_UNEXPECTED;
    }

    MA_LOG(logger, MA_LOG_SEV_INFO, SERVICE_NAME " stopping...");

    // not sure if this is technically required, just need a barrier on the 'state' variable to ensure proper reading on the thread side
    InterlockedExchange(&state, MA_SERVICE_STOPPING);

    thread_signal.Set();
    WaitForSingleObject(hTread, INFINITE);

    hTread.Close();

    state = MA_SERVICE_STOPPED;

    MA_LOG(logger, MA_LOG_SEV_INFO, SERVICE_NAME " stopped");
    return MA_OK;
}


unsigned ma_lockdown_service::run() {

    ATL::CRegKey key;

    DWORD timeout = INFINITE;

    while (true) {

        switch (DWORD result = WaitForSingleObject(thread_signal, timeout)) {

        case WAIT_OBJECT_0: {
            if (MA_SERVICE_STOPPING == state) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "Lockdown thread now exiting...");
                return EXIT_SUCCESS;
            }
            
            timeout = 5 * 60 * 1000; // try again in 5 mins unless we get a good handle to monitor (see below)

            break;
            }

        case WAIT_TIMEOUT :
            // try again
            break;

        default:
            return result;
        }
    }
}
