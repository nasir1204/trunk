#ifndef XML_GENERATOR_UTILITIES_H_INCLUDED
#define XML_GENERATOR_UTILITIES_H_INCLUDED

#include "ma/internal/utils/filesystem/path.h"

/*Variant utilities*/
ma_bool_t validate_variant_type(ma_variant_t *variant, ma_vartype_t type);

/*Table utilities*/

ma_bool_t find_product_in_table(ma_table_t *product_table, const char *product_id, ma_table_t **value);
ma_bool_t find_section_in_table(ma_table_t *section_table, const char *section_name, ma_table_t **value);
ma_bool_t find_setting_in_table(ma_table_t *setting_table, const char *setting_name, char **value);


ma_error_t create_new_table_with_id(ma_table_t *parent_table, const char *id, ma_table_t **table);
	
ma_error_t add_string_to_table(ma_table_t *parent_table, const char *id, const char *value, size_t size);
ma_error_t merge_property_to_full_props(ma_table_t *full_prop, const char *product_id, const char *section_name, const char *setting_name, const char *value);

/*Datstore utilities*/


ma_error_t iterate_missing_products_in_db(ma_property_xml_generator_t *xml_generator, ma_bool_t b_manipulate_db, ma_xml_t *xml);

ma_error_t iterate_missing_sections_in_db(ma_property_xml_generator_t *xml_generator, ma_bool_t b_manipulate_db, const char* product_id, ma_table_t *section_table_from_full_prop, ma_xml_node_t *section_root);

ma_error_t iterate_missing_settings_in_db(ma_property_xml_generator_t *xml_generator, ma_bool_t b_manipulate_db, const char *product_id, const char *section_name, ma_table_t *section_table_from_full_prop,ma_xml_node_t *settings_root) ;

ma_error_t remove_path_from_db(ma_ds_t *datastore, const char *path);

/*XML utilities*/

ma_bool_t find_property_xml_setting_node( ma_xml_node_t *parent_node, const char *setting_name, ma_bool_t b_is_product_property, ma_bool_t b_delete, ma_xml_node_t **child);
ma_error_t create_property_xml_setting_node( ma_xml_node_t *parent_node, const char *setting_name, ma_bool_t b_is_product_property,char *value, ma_bool_t b_delete, ma_xml_node_t **node);
ma_error_t find_or_create_property_xml_setting_node( ma_xml_node_t *parent_node, const char *setting_name, ma_bool_t b_is_product_property,char *value, ma_bool_t b_delete, ma_xml_node_t **node);

ma_bool_t find_xml_product_section( ma_xml_node_t *parent, const char *section_name, ma_xml_node_t **node);
ma_error_t create_xml_product_section( ma_xml_node_t *parent, const char *section_name, ma_xml_node_t **node);
ma_error_t find_or_create_xml_product_section( ma_xml_node_t *parent, const char *section_name, ma_xml_node_t **node);

ma_bool_t find_xml_product( ma_xml_node_t *parent, ma_bool_t is_product, const char *product_id, ma_xml_node_t **node);
ma_error_t create_xml_product( ma_xml_node_t *parent, const char *product_id, ma_bool_t b_delete, ma_xml_node_t **node); 
ma_error_t find_or_create_xml_product( ma_xml_node_t *parent, const char *product_id, ma_bool_t b_delete, ma_xml_node_t **node);

ma_bool_t find_xml_property_root( ma_xml_t *root_xml, ma_xml_node_t **node);
ma_error_t create_xml_property_root( ma_xml_t *root_xml, ma_property_xml_generator_t *xml_generator,ma_bool_t b_full_prop,ma_xml_node_t **node);
ma_error_t find_or_create_xml_property_root( ma_xml_t *root_xml,ma_property_xml_generator_t *xml_generator, ma_bool_t b_full_prop, ma_xml_node_t **node);

ma_error_t mark_deleted_products(ma_xml_node_t *product_root, const char *product_id) ;
ma_error_t mark_deleted_product_settings(ma_xml_node_t *setting_root, const char *product_id, const char *section_name, const char *setting_name) ;

/* Property Bag table iterator and Utilties*/
void ma_prop_bag_section_iterator(const char *section_name, ma_variant_t *value, void *cb_args, ma_bool_t *b_stop);
void ma_prop_bag_setting_iterator(const char *setting_name, ma_variant_t *value, void *cb_args, ma_bool_t *b_stop);


/*General utilities*/
ma_bool_t is_product(const char *product_id);
#endif

