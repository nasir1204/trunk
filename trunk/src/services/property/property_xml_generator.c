#include "ma/ma_common.h"
#include "ma/ma_log.h"
#include "ma/internal/utils/datastructures/ma_slist.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal/ma_macros.h"
#include "ma/datastore/ma_ds_database.h"
#include "ma/internal/services/property/property_xml_generator.h"
#include "xml_generator_utilities.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "xml_generator"

typedef struct ma_props_ignore_list_node_s ma_props_ignore_list_node_t, *ma_props_ignore_list_node_h;

struct ma_props_ignore_list_node_s {
	char *product_id;
	MA_SLIST_NODE_DEFINE(ma_props_ignore_list_node_t);
};

ma_error_t ma_props_ignore_list_node_create(ma_props_ignore_list_node_t **node) {
	if(node) {
		*node = (ma_props_ignore_list_node_t *) calloc(1, sizeof(ma_props_ignore_list_node_t));
		if(!*node)
			return MA_ERROR_INVALIDARG;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_props_ignore_list_node_release(ma_props_ignore_list_node_t *list) {
	free(list->product_id);
	free(list);
	return MA_OK;
}

ma_error_t ma_props_ignore_list_node_set(
		ma_props_ignore_list_node_t *node,
		const char *product_id) {
	if(node){
		if(node->product_id) {
			free(node->product_id);
			node->product_id = NULL;
		}

		if(product_id) 
			node->product_id = strdup(product_id);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

typedef struct ma_props_table_iterator_s ma_props_table_iterator_t, *ma_props_table_iterator_h;
struct ma_props_table_iterator_s {
	ma_bool_t b_product_property;
	ma_bool_t b_full_prop_iteration;
	ma_bool_t b_save_to_datastore;
	char *product_id;
	char *section_name;
	char *path;

	ma_xml_node_t *p_xml_parent;

	ma_property_xml_generator_t *xml_generator;	
};

ma_error_t ma_props_table_iterator_create( ma_props_table_iterator_t **data) {
	if(data) {
		*data = (ma_props_table_iterator_t *)calloc(1,sizeof(ma_props_table_iterator_t));
		if(! *data)
			return MA_ERROR_OUTOFMEMORY;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}
ma_error_t ma_props_table_iterator_release(ma_props_table_iterator_t *data) {
	if(data) {
		if(data->product_id)
			free(data->product_id);
		if(data->section_name)
			free(data->section_name);
		if(data->path)
			free(data->path);
		free(data);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_props_table_iterator_set(
		ma_props_table_iterator_t *data,
		ma_bool_t b_full_prop_iter,
		ma_bool_t b_save_to_datastore,
		const char *path,
		const char *product_id,
		const char *section_name,
		ma_xml_node_t *xml_parent,
		ma_property_xml_generator_t *xml_generator) {
	if( !path || !data || !xml_generator)
		return MA_ERROR_INVALIDARG;
	data->b_full_prop_iteration = b_full_prop_iter;
	data->b_save_to_datastore = b_save_to_datastore;
	data->path = strdup(path);
	if(product_id) {
		data->b_product_property = is_product(product_id);
		data->product_id = strdup(product_id);
	}
	data->section_name = section_name? strdup(section_name): NULL;
	data->p_xml_parent = xml_parent;
	data->xml_generator = xml_generator;
	return MA_OK;
}
struct ma_property_xml_generator_s {
	char *machine_name;
	char *machine_guid;
	char *tenantid;
	char *prop_version;

	ma_ds_t *datastore;
	char *props_ds_root_path;

	ma_xml_t *incr_prop_xml;
	ma_table_t *full_props;
	MA_SLIST_DEFINE(invalid_settings, ma_props_ignore_list_node_t);
	MA_SLIST_DEFINE(prod_not_running, ma_props_ignore_list_node_t);

	ma_logger_t *logger;
};


ma_error_t ma_property_xml_generator_create(
		ma_property_xml_generator_t **xml_generator,
		ma_ds_t *datastore,
		const char *props_ds_root_path,
		const char *machine_name,
		const char *machine_guid, const char *tenantid, const char *prop_version) {
	ma_error_t error = MA_OK;
	if(	xml_generator && datastore &&
		(props_ds_root_path && props_ds_root_path[0] != '\0') &&
		(machine_name && machine_name[0] !='\0') &&
		(machine_guid && machine_guid[0] != '\0') &&
		(prop_version && prop_version[0] != '\0') ) {
		*xml_generator = (ma_property_xml_generator_t *) calloc(1, sizeof(ma_property_xml_generator_t));
		if(!*xml_generator)
			return MA_ERROR_OUTOFMEMORY;
		if(MA_OK != (error = ma_table_create(&((*xml_generator)->full_props)))) {
			free(*xml_generator);
			*xml_generator = NULL;
			return error;
		}
		(*xml_generator)->datastore = datastore;
		(*xml_generator)->props_ds_root_path = strdup(props_ds_root_path);
		(*xml_generator)->machine_name = strdup(machine_name);
		(*xml_generator)->machine_guid = strdup(machine_guid);
		if(tenantid && tenantid[0] != '\0') (*xml_generator)->tenantid = strdup(tenantid);
		(*xml_generator)->prop_version = strdup(prop_version);
		
		MA_SLIST_INIT(*xml_generator, invalid_settings);
		MA_SLIST_INIT(*xml_generator, prod_not_running);

		if(MA_OK != ma_xml_create(&(*xml_generator)->incr_prop_xml) )
			return MA_ERROR_APIFAILED;
		if(MA_OK !=	ma_xml_construct((*xml_generator)->incr_prop_xml))
			return MA_ERROR_APIFAILED;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_property_xml_generator_release(
		ma_property_xml_generator_t *xml_generator) {
	if(!xml_generator) 
		return MA_ERROR_INVALIDARG;
	xml_generator->datastore = NULL;
	free(xml_generator->props_ds_root_path);
	free(xml_generator->machine_name);
	free(xml_generator->machine_guid);
	if(xml_generator->tenantid) free(xml_generator->tenantid);
	free(xml_generator->prop_version);
	ma_table_release(xml_generator->full_props);
	ma_xml_release(xml_generator->incr_prop_xml);
	MA_SLIST_CLEAR(xml_generator,invalid_settings, ma_props_ignore_list_node_t, ma_props_ignore_list_node_release);
	MA_SLIST_CLEAR(xml_generator,prod_not_running, ma_props_ignore_list_node_t, ma_props_ignore_list_node_release);
	free(xml_generator);
	return MA_OK;
}

ma_error_t ma_property_xml_generator_set_logger(ma_property_xml_generator_t *xml_generator, ma_logger_t *logger) {
	if(xml_generator) {
		xml_generator->logger = logger;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}
/*
 * Datastore contains the last props that was reported to the server
 * the product_properties is collected per product and incremental xml calculation for the product is done then and there
 * the xml_generator->incr_prop_xml will contain the incremental properties based on current product property and last props
 */
ma_error_t ma_property_xml_generator_append_prop(
		ma_property_xml_generator_t *xml_generator,
		const char *product_id,
		ma_table_t *product_properties) {
	if(xml_generator && product_id && product_properties) {
		ma_props_table_iterator_t *data = NULL;
		ma_xml_node_t *product_root = NULL;
		ma_xml_node_t *prop_root = NULL;
		ma_temp_buffer_t product_path;
		ma_temp_buffer_init(&product_path);
		form_path(&product_path,xml_generator->props_ds_root_path,product_id,MA_TRUE);

		MA_LOG(xml_generator->logger, MA_LOG_SEV_TRACE, "Appending properties for %s product", product_id);
		if(MA_OK == find_or_create_xml_property_root(xml_generator->incr_prop_xml,xml_generator,MA_FALSE,&prop_root)){
			if(MA_OK == find_or_create_xml_product(prop_root,product_id,MA_FALSE,&product_root)) {
				size_t table_size = 0;
				if(MA_OK == ma_table_size(product_properties, &table_size)) {
					MA_LOG(xml_generator->logger, MA_LOG_SEV_TRACE, "Property Bag contains %d sections", table_size);
					if(0 < table_size) {
						if(MA_OK == ma_props_table_iterator_create(&data)) {
							ma_props_table_iterator_set(data, MA_FALSE, MA_FALSE, (char*)ma_temp_buffer_get(&product_path),product_id,NULL,product_root,xml_generator);
							ma_table_foreach(product_properties,ma_prop_bag_section_iterator,  data);
							ma_props_table_iterator_release(data);
						}
					}
					else {
						MA_LOG(xml_generator->logger, MA_LOG_SEV_TRACE, "No Sections in this property bag");
						/*ma_variant_t *variant = NULL;
						if(MA_OK == ma_variant_create_from_table(product_properties, &variant)) {
							ma_table_add_entry(xml_generator->full_props, product_id, variant);
							ma_variant_release(variant);
						}*/
						ma_property_xml_generator_set_product_not_running(xml_generator, product_id);
					}
				}
			}
		}

		
		ma_temp_buffer_uninit(&product_path);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}


ma_error_t ma_property_xml_generator_get_incremental_props( ma_property_xml_generator_t *xml_generator, unsigned char **incr_buffer, size_t *buf_size) {
	ma_bytebuffer_t *buffer = NULL;
	if(xml_generator && incr_buffer && buf_size) {
		size_t size = 0;
		ma_xml_node_t *root = NULL;
		find_or_create_xml_property_root(xml_generator->incr_prop_xml, xml_generator, MA_FALSE, &root);
		iterate_missing_products_in_db(xml_generator, MA_FALSE, xml_generator->incr_prop_xml);
		if(MA_OK == ma_bytebuffer_create(4096,&buffer)) {
			unsigned char *p_temp_buf = NULL;
			ma_xml_save_to_buffer(xml_generator->incr_prop_xml, buffer);
			p_temp_buf = ma_bytebuffer_get_bytes(buffer);
			size = ma_bytebuffer_get_size(buffer);
			*incr_buffer = (unsigned char *) calloc(size + 1 ,sizeof(unsigned char));
			if(*incr_buffer) {
				strncpy((char*) *incr_buffer,(char*) p_temp_buf, size);
				(*incr_buffer)[size] = '\0';
				*buf_size = size;
			}
			MA_LOG(xml_generator->logger, MA_LOG_SEV_TRACE, "%s", *incr_buffer);
			ma_bytebuffer_release(buffer);
		}
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

void ma_prop_table_product_iterator(const char *product_id, ma_variant_t *section_variant, void *cb_args, ma_bool_t *b_stop) {
	if(cb_args) {
		ma_props_table_iterator_t *data = (ma_props_table_iterator_t *) cb_args;
		ma_xml_node_t *prop_root = data->p_xml_parent;
		ma_xml_node_t *product_node = NULL;
		if(data->b_save_to_datastore || MA_OK == find_or_create_xml_product(prop_root, product_id, MA_FALSE, &product_node)){
			ma_props_table_iterator_t *new_data = NULL;
			ma_table_t *section_table = NULL;
			if(MA_TRUE == find_product_in_table(data->xml_generator->full_props, product_id, &section_table)){
				if(MA_OK == ma_props_table_iterator_create(&new_data)){
					ma_temp_buffer_t product_path;
					ma_temp_buffer_init(&product_path);
					form_path(&product_path, data->path, product_id,MA_TRUE);
					if(MA_OK == ma_props_table_iterator_set(new_data, data->b_full_prop_iteration, data->b_save_to_datastore, (char*)ma_temp_buffer_get(&product_path), product_id, NULL, product_node, data->xml_generator)){
						ma_table_foreach(section_table, ma_prop_bag_section_iterator, new_data);
					}
					ma_props_table_iterator_release(new_data);
					ma_temp_buffer_uninit(&product_path);
				}
				ma_table_release(section_table);
			}
		}
	}
}

ma_error_t ma_property_xml_generator_get_full_props( ma_property_xml_generator_t *xml_generator, unsigned char **full_buffer, size_t *buf_size) {
	ma_error_t error = MA_ERROR_INVALIDARG;
	if(xml_generator && full_buffer) {
		ma_xml_node_t *prop_root = NULL;
		ma_xml_t *full_props_xml = NULL;
		if(MA_OK == (error = ma_xml_create(&full_props_xml))) {
			if(MA_OK == (error = ma_xml_construct(full_props_xml))) {
				ma_bytebuffer_t *buffer = NULL;
				if(MA_OK == (error = find_or_create_xml_property_root(full_props_xml, xml_generator, MA_TRUE, &prop_root))) {
					ma_props_table_iterator_t *data = NULL;
					ma_table_t *full_prop_table = xml_generator->full_props;
					if(MA_OK == (error = ma_props_table_iterator_create(&data))) {
						if(MA_OK == (error = ma_props_table_iterator_set(data, MA_TRUE, MA_FALSE, xml_generator->props_ds_root_path, NULL, NULL, prop_root, xml_generator))) {
							ma_table_foreach(full_prop_table, ma_prop_table_product_iterator, data);
						}
						ma_props_table_iterator_release(data);
					}
				}
				iterate_missing_products_in_db(xml_generator, MA_FALSE, full_props_xml);
				if(MA_OK == ma_bytebuffer_create(4096,&buffer)){
					size_t size = 0;
					unsigned char *p_temp_buf = NULL;
					ma_xml_save_to_buffer(full_props_xml, buffer);
					size = ma_bytebuffer_get_size(buffer);
					p_temp_buf = ma_bytebuffer_get_bytes(buffer);
					*full_buffer = (unsigned char *) calloc(size + 1 ,sizeof(unsigned char));
					if(*full_buffer) {
						strncpy((char*) *full_buffer,(char*) p_temp_buf, size);
						(*full_buffer)[size] = '\0';
						*buf_size = size;
					}
					MA_LOG(xml_generator->logger, MA_LOG_SEV_TRACE, "%s", *full_buffer);
					ma_bytebuffer_release(buffer);
				}
			}
			ma_xml_release(full_props_xml);
		}
	}
	return error;
}


ma_error_t ma_property_xml_generator_set_product_not_running(ma_property_xml_generator_t *xml_generator, const char *product_id) {
	ma_error_t error = MA_ERROR_INVALIDARG;
	ma_props_ignore_list_node_t *node = NULL;
	if(xml_generator && product_id) {
		if(MA_FALSE == ma_property_xml_generator_is_product_not_running(xml_generator,product_id)){
			if(MA_OK == ma_props_ignore_list_node_create(&node)){
				if(MA_OK == ma_props_ignore_list_node_set(node, product_id)) {
					MA_SLIST_PUSH_BACK(xml_generator, prod_not_running, node);
					return MA_OK;
				}
				ma_props_ignore_list_node_release(node);
				node = NULL;
			}
		}
	}
	return error;
}

ma_bool_t ma_property_xml_generator_is_product_not_running(ma_property_xml_generator_t *xml_generator, const char *product_id) {
	ma_props_ignore_list_node_t *node = NULL;
	MA_SLIST_FOREACH(xml_generator, prod_not_running, node){
			if(0 == strcmp(node->product_id, product_id))
				return MA_TRUE;
	}
	return MA_FALSE;
}

ma_bool_t validate_variant_type(ma_variant_t *variant, ma_vartype_t type) {
	ma_bool_t ret = MA_FALSE;
	ma_vartype_t val_type = MA_VARTYPE_NULL;
	if(variant){
		if(MA_OK == ma_variant_is_type(variant, type)){
			ret = MA_TRUE;
		}
	}
	return ret;
}

ma_bool_t extract_table_from_table(ma_table_t *parent_table, const char * id, ma_table_t **value) {
	ma_bool_t b_ret = MA_FALSE;
	if(parent_table && id && value) {
		ma_variant_t *variant = NULL;
		if(MA_OK == ma_table_get_value(parent_table, id, &variant) ){
			if(MA_TRUE == validate_variant_type(variant, MA_VARTYPE_TABLE)) {
				if(MA_OK == ma_variant_get_table(variant, value))
					b_ret = MA_TRUE;
			}
			ma_variant_release(variant);
		}
	}
	return b_ret;
}

ma_bool_t find_product_in_table(ma_table_t *product_table, const char * product_id, ma_table_t **value) {
	return extract_table_from_table(product_table, product_id, value);
}

ma_bool_t find_section_in_table(ma_table_t *section_table, const char *section_name, ma_table_t **value) {
	return extract_table_from_table(section_table, section_name, value);
}

ma_bool_t find_setting_in_table(ma_table_t *section_table, const char *setting_name, char **value) {
	ma_bool_t ret = MA_FALSE;
	ma_variant_t *variant = NULL;
	ma_buffer_t *buffer = NULL;
	const char *temp = NULL;
	size_t size = 0;
	if(MA_OK == ma_table_get_value(section_table, setting_name, &variant)){
		if(MA_TRUE == validate_variant_type(variant, MA_VARTYPE_STRING)) {
			if(MA_OK == ma_variant_get_string_buffer(variant, &buffer)) {
				if(MA_OK == ma_buffer_get_string(buffer, &temp, &size)) {
					*value = strdup(temp);
					ret = MA_TRUE;
				}
				ma_buffer_release(buffer);
			}
		}
		ma_variant_release(variant);
	}
	return ret;
}

ma_bool_t is_product(const char *product_id){
	return (0 == strcmp(product_id, MA_SYSTEM_PROP_PROVIDER_ID) )? MA_FALSE :MA_TRUE;
}

ma_error_t create_new_table_with_id(ma_table_t *parent_table, const char *id, ma_table_t **table) {
	if(parent_table && id && table) {
		ma_variant_t *variant = NULL;
		if(MA_OK == ma_table_create(table)) {
			if(MA_OK == ma_variant_create_from_table(*table, &variant)) {
				ma_table_add_entry(parent_table, id, variant);
				ma_variant_release(variant);
			}
		}
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t add_string_to_table(ma_table_t *parent_table, const char *id, const char *value, size_t size) {
	if(parent_table && id && value) {
		ma_variant_t *variant = NULL;
		if(MA_OK == ma_variant_create_from_string(value, size, &variant)) {
			ma_table_add_entry(parent_table,id, variant);
			ma_variant_release(variant);
		}
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}


ma_error_t merge_property_to_full_props(ma_table_t *full_prop, const char *product_id, const char *section_name, const char *setting_name, const char *value) {
	ma_bool_t b_found = MA_FALSE;
	if(full_prop && product_id && setting_name && value) {
		ma_table_t *section_table = NULL;
		char *setting_value = NULL;
		if( MA_FALSE == (b_found = find_product_in_table(full_prop,product_id,&section_table)))
			(void)create_new_table_with_id(full_prop, product_id, &section_table);
		if(section_name ) {
			ma_table_t *setting_table = NULL;
			if(MA_FALSE == find_section_in_table(section_table,section_name, &setting_table))
				(void)create_new_table_with_id(section_table,section_name, &setting_table);
				
			if(MA_FALSE == find_setting_in_table(setting_table, setting_name, &setting_value))
				(void)add_string_to_table(setting_table, setting_name, value, strlen(value));
			else {
				free(setting_value);
				setting_value = NULL;
			}
			(void)ma_table_release(setting_table);
		}
		(void)ma_table_release(section_table);		
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

void ma_prop_bag_section_iterator(const char *section_name, ma_variant_t *value, void *cb_args, ma_bool_t *b_stop) {
	ma_table_t *setting_table = NULL;
	ma_props_table_iterator_t *data = (ma_props_table_iterator_t *) cb_args;
	ma_property_xml_generator_t *xml_generator = data->xml_generator;
	MA_LOG(xml_generator->logger, MA_LOG_SEV_TRACE, "Section Name : %s", section_name);

	if(MA_TRUE == validate_variant_type(value, MA_VARTYPE_TABLE)) {
		if(MA_OK == ma_variant_get_table(value, &setting_table)) {
			ma_props_table_iterator_t *new_data = NULL;
			ma_xml_node_t *section_xml_node = NULL;
			ma_temp_buffer_t section_path;
			ma_temp_buffer_init(&section_path);
			//char section_path[MAX_PATH_LEN] = {0};

			form_path(&section_path,data->path,section_name,MA_TRUE);
			if(MA_OK == ma_props_table_iterator_create(&new_data)) {
				if(data->b_product_property) {
					if(data->b_save_to_datastore || MA_OK == find_or_create_xml_product_section(data->p_xml_parent,section_name,&section_xml_node)) {
						(void)ma_props_table_iterator_set(new_data, data->b_full_prop_iteration, data->b_save_to_datastore, (char*)ma_temp_buffer_get(&section_path),data->product_id,section_name,section_xml_node,data->xml_generator);
					}
					else{
						(void)ma_props_table_iterator_release(new_data);
						(void)ma_table_release(setting_table);
						return;
					}
				}
				else
					(void)ma_props_table_iterator_set(new_data, data->b_full_prop_iteration, data->b_save_to_datastore,  (char*)ma_temp_buffer_get(&section_path),data->product_id,section_name,data->p_xml_parent,data->xml_generator);

				ma_table_foreach(setting_table, ma_prop_bag_setting_iterator, new_data);
				if((!data->b_save_to_datastore) && data->b_product_property) {
					int count = 0;
					if((count = ma_xml_node_get_child_count(section_xml_node)) == 0){
						(void)ma_xml_node_delete(section_xml_node);
						(void)ma_xml_node_release(section_xml_node);
					}
				}
				(void)ma_props_table_iterator_release(new_data);
			}
			(void)ma_table_release(setting_table);
			ma_temp_buffer_uninit(&section_path);
		}
	}
}

ma_error_t extract_string_from_variant(ma_variant_t *variant, char **value, size_t *size){
	ma_buffer_t *buffer = NULL;
	const char *temp = NULL;
	if(variant && value && size) {
		if(MA_OK == ma_variant_get_string_buffer(variant, &buffer) ){
			if(MA_OK == ma_buffer_get_string(buffer, &temp, size)) {
				if(*size == 0) {
					*value = strdup("");
				}
				else {
					*value = strdup(temp);
				}
			}
			(void)ma_buffer_release(buffer);
		}
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t lookup_in_datastore(ma_ds_t *datastore, const char *path, const char *key, char **value,size_t *size) {
	ma_buffer_t *buffer;
	const char *temp = NULL;
	ma_error_t error = MA_OK;
	if(MA_OK != (error = ma_ds_get_str(datastore, path, key, &buffer))) 
		return error;
	if(MA_OK == ma_buffer_get_string(buffer,&temp,size)) 
		*value = strdup(temp);
	(void)ma_buffer_release(buffer);
	return MA_OK;
}

void ma_prop_bag_setting_iterator(const char *setting_name, ma_variant_t *value, void *cb_args, ma_bool_t *b_stop) {
	char *setting_value = NULL;
	size_t size = 0;
	char *ds_setting_value = NULL;
	ma_props_table_iterator_t *data = (ma_props_table_iterator_t *) cb_args;
	ma_xml_node_t *node = NULL;
	ma_bool_t b_add_settting = MA_FALSE;
	ma_property_xml_generator_t *xml_generator = data->xml_generator;

	if(MA_FALSE == validate_variant_type(value, MA_VARTYPE_STRING))
		return;
	(void)extract_string_from_variant(value,&setting_value, &size);
	/*add the setting to the current full props*/
	MA_LOG(xml_generator->logger, MA_LOG_SEV_TRACE, "Setting Name : %s, value : %s", setting_name, setting_value);
	if((MA_FALSE == data->b_full_prop_iteration) && (MA_FALSE == data->b_save_to_datastore)){
		(void)merge_property_to_full_props(
				data->xml_generator->full_props,
				data->product_id, 
				data->section_name,
				setting_name,
				setting_value);
	}
	if(!data->b_save_to_datastore) {
	/*lookup in datastore*/
		if(MA_OK == lookup_in_datastore(data->xml_generator->datastore, data->path, setting_name, &ds_setting_value,&size)) {
			/*setting found. compare the values to see if different
			*If yes, add it to incremental properties xml
			*If equal, don't add to incremental properties xml
			*/
			if(0 != strcmp(setting_value,ds_setting_value)) {
				b_add_settting = MA_TRUE;
			}
			free(ds_setting_value);
		}
		else {
				b_add_settting = MA_TRUE;
		}
		if(b_add_settting || data->b_full_prop_iteration)
			find_or_create_property_xml_setting_node(data->p_xml_parent, setting_name, data->b_product_property, setting_value, MA_FALSE,&node);
	}
	else {
		(void)ma_ds_set_str(data->xml_generator->datastore, data->path, setting_name,setting_value, size);
	}
	free(setting_value);
}

ma_error_t create_property_xml_setting_node( ma_xml_node_t *parent_node, const char *setting_name, ma_bool_t b_is_product_property,char *value, ma_bool_t b_delete, ma_xml_node_t **node) {
	ma_error_t error = MA_OK;
	if(!node || !parent_node || !setting_name)
		return MA_ERROR_INVALIDARG;

	if(MA_TRUE == b_is_product_property) {
		if(MA_OK != (error = ma_xml_node_create(parent_node,MA_STR_PROD_PROPERTY_SETTING, node)))
			return error;	
		(void)ma_xml_node_attribute_set(*node, MA_STR_PROD_PROPERTY_ATTR_NAME,setting_name);
		
		if(!b_delete)
			(void)ma_xml_node_set_data(*node, value == NULL? "":value);
	}
	else {
		if(MA_OK != (error = ma_xml_node_create(parent_node,setting_name,node)))
			return error;
		if(!b_delete)
			(void)ma_xml_node_set_data(*node, value == NULL? "":value);
	}
	if(b_delete) 
		(void)ma_xml_node_attribute_set(*node, MA_STR_PROPERTY_ATTR_DELETE,MA_STR_TRUE);

	return MA_OK;
}

ma_bool_t find_property_xml_setting_node( ma_xml_node_t *parent_node, const char *setting_name, ma_bool_t b_is_product_property, ma_bool_t b_delete, ma_xml_node_t **child){
	if(parent_node && setting_name && child) {
		*child = ma_xml_node_get_first_child(parent_node);
		while(*child != NULL) {
			if(b_is_product_property) {
				if(0 == strcmp(MA_STR_PROD_PROPERTY_SETTING,ma_xml_node_get_name(*child))) {
					if(0 == strcmp(setting_name,ma_xml_node_attribute_get(*child,MA_STR_PROD_PROPERTY_ATTR_NAME)))
						return MA_TRUE;
				}
			}
			else{
				if( 0 == strcmp(setting_name, ma_xml_node_get_name(*child)))
					return MA_TRUE;
			}
			*child = ma_xml_node_get_next_sibling(*child);
		}
		*child = NULL;
	}
	return MA_FALSE;
}

ma_error_t find_or_create_property_xml_setting_node( ma_xml_node_t *parent_node, const char *setting_name, ma_bool_t b_is_product_property,char *value, ma_bool_t b_delete, ma_xml_node_t **node) {
	if(!parent_node || !setting_name || !node)
		return MA_ERROR_INVALIDARG;

	if(MA_FALSE == find_property_xml_setting_node(parent_node,setting_name,b_is_product_property,b_delete,node))
		(void)create_property_xml_setting_node(parent_node, setting_name, b_is_product_property, value, b_delete,node);
	else
		(void)ma_xml_node_set_data(*node,value);
	return MA_OK;
}

ma_bool_t find_xml_product_section( ma_xml_node_t *parent, const char *section_name, ma_xml_node_t **node){
	if(parent && section_name && node) {
		*node = ma_xml_node_get_first_child(parent);
		while(*node != NULL) {
			if(0 == strcmp(MA_STR_PROD_PROPERTY_SECTION, ma_xml_node_get_name(*node))) {
				if(0 == strcmp(section_name, ma_xml_node_attribute_get(*node,MA_STR_PROD_PROPERTY_ATTR_NAME))) {
					return MA_TRUE;
				}
			}
			*node = ma_xml_node_get_next_sibling(*node);
		}
		*node = NULL;
	}
	return MA_FALSE;
}

ma_error_t create_xml_product_section( ma_xml_node_t *parent, const char *section_name, ma_xml_node_t **node) {
	ma_error_t error = MA_OK;
	if(!parent || !section_name || !node)
		return MA_ERROR_INVALIDARG;
	if(MA_OK != (error = ma_xml_node_create(parent, MA_STR_PROD_PROPERTY_SECTION, node))){
		return error;
	}
	(void)ma_xml_node_attribute_set(*node, MA_STR_PROD_PROPERTY_ATTR_NAME, section_name);
	return MA_OK;
}

ma_error_t find_or_create_xml_product_section( ma_xml_node_t *parent, const char *section_name, ma_xml_node_t **node) {
	ma_error_t error = MA_OK;
	if(!parent || !section_name || !node)
		return MA_ERROR_INVALIDARG;
	if(MA_FALSE == find_xml_product_section(parent,section_name,node))
		error = create_xml_product_section(parent,section_name,node);
	return error ;
}

ma_bool_t find_xml_product( ma_xml_node_t *parent, ma_bool_t b_is_product, const char *product_id, ma_xml_node_t **node) {
	if(parent && product_id && node) {
		*node = ma_xml_node_get_first_child(parent);
		while(*node != NULL) {
			if(b_is_product) {
				if( 0 == strcmp(MA_STR_PROD_PROPERTY, ma_xml_node_get_name(*node)) ) {
					if(0 == strcmp(product_id,ma_xml_node_attribute_get(*node,MA_STR_PROD_PROPERTY_ATTR_SOFTWARE_ID)))
						return MA_TRUE;
				}
			}
			else {
				if( 0 == strcmp(MA_STR_SYSTEM_PROPERTY, ma_xml_node_get_name(*node)))
					return MA_TRUE;
			}
			*node = ma_xml_node_get_next_sibling(*node);
		}
		*node = NULL;
	}
	return MA_FALSE;
}
ma_error_t create_xml_product( ma_xml_node_t *parent, const char *product_id, ma_bool_t b_delete, ma_xml_node_t **node) {
	ma_error_t error = MA_OK;
	if(!parent || !product_id || !node)
		return MA_ERROR_INVALIDARG;

	if(MA_TRUE == is_product(product_id)) {
		if(MA_OK != (error = ma_xml_node_create(parent,MA_STR_PROD_PROPERTY,node)))
			return error;
		(void)ma_xml_node_attribute_set(*node, MA_STR_PROD_PROPERTY_ATTR_SOFTWARE_ID, product_id);
		(void)ma_xml_node_attribute_set(*node, MA_STR_PROPERTY_ATTR_DELETE, (MA_TRUE == b_delete)?MA_STR_TRUE:MA_STR_FALSE);
	}
	else {
		if(MA_OK != (error = ma_xml_node_create(parent,MA_STR_SYSTEM_PROPERTY,node)))
			return error;
	}
	return MA_OK;
}

ma_error_t find_or_create_xml_product( ma_xml_node_t *parent, const char *product_id, ma_bool_t b_delete, ma_xml_node_t **node) {
	if(!parent || !product_id || !node)
		return MA_ERROR_INVALIDARG;
	if(MA_FALSE == find_xml_product(parent, is_product(product_id),product_id,node))
		(void)create_xml_product(parent,product_id, b_delete, node);
	return MA_OK;
}

ma_bool_t find_xml_property_root( ma_xml_t *root_xml, ma_xml_node_t **node) {
	if(root_xml && node) {
		*node = ma_xml_get_node(root_xml);
		if(*node) {
			*node = ma_xml_node_get_first_child(*node);
			if(*node) {
				if(0 == strcmp(MA_PROPS_ROOT_NODE_NAME, ma_xml_node_get_name(*node))) 
					return MA_TRUE;
			}
		}	
		*node = NULL;
	}
	return MA_FALSE;
}
ma_error_t create_xml_property_root( ma_xml_t *root_xml, ma_property_xml_generator_t *xml_generator, ma_bool_t b_full_prop, ma_xml_node_t **node) {
	ma_xml_node_t *root_node = NULL;
	if(!root_xml || !xml_generator || !node)
		return MA_ERROR_INVALIDARG;

	root_node = ma_xml_get_node(root_xml);
	if(MA_OK  != ma_xml_node_create(root_node, MA_PROPS_ROOT_NODE_NAME,node))
		return MA_ERROR_APIFAILED;
	(void)ma_xml_node_attribute_set(*node, MA_PROPS_XML_ATTRIBUTE_MACHINE_NAME, xml_generator->machine_name);
	(void)ma_xml_node_attribute_set(*node, MA_PROPS_XML_ATTRIBUTE_MACHINE_ID, xml_generator->machine_guid);
	if(xml_generator->tenantid && xml_generator->tenantid[0] != '\0') (void)ma_xml_node_attribute_set(*node, MA_PROPS_XML_ATTRIBUTE_TENANT_ID, xml_generator->tenantid);
	(void)ma_xml_node_attribute_set(*node, MA_PROPS_XML_ATTRIBUTE_PROPSVERSION, xml_generator->prop_version);
	(void)ma_xml_node_attribute_set(*node, MA_PROPS_XML_ATTRIBUTE_PROPS_TYPE, MA_TRUE == b_full_prop?MA_STR_TRUE:MA_STR_FALSE);
	(void)ma_xml_node_attribute_set(*node, MA_PROPS_XML_ATTRIBUTE_XML_NAMESPACE, MA_PROPS_XML_ATTRIBUTE_XML_NAMESPACE_VAL);
	return MA_OK;
}
ma_error_t find_or_create_xml_property_root( ma_xml_t *root_xml,ma_property_xml_generator_t *xml_generator, ma_bool_t b_full_prop, ma_xml_node_t **node) {
	if(!root_xml || !xml_generator || !node)
		return MA_ERROR_INVALIDARG;
	if(MA_FALSE == find_xml_property_root(root_xml,node))
		(void)create_xml_property_root(root_xml,xml_generator,b_full_prop,node);

	return MA_OK;
}


ma_error_t iterate_missing_products_in_db(ma_property_xml_generator_t *xml_generator, ma_bool_t b_manipulate_db, ma_xml_t *xml) {
	ma_error_t error = MA_ERROR_INVALIDARG;
	ma_buffer_t *buffer = NULL;
	ma_ds_iterator_t *path_iterator = NULL;
	const char *product_id = NULL;
	size_t size = 0;
	ma_xml_node_t *prop_root = NULL;

	if(xml_generator) {
		ma_ds_t *datastore = xml_generator->datastore;
		if(b_manipulate_db || (MA_TRUE == (find_xml_property_root(xml, &prop_root)))) {
			ma_temp_buffer_t root_path;
			ma_temp_buffer_init(&root_path);
//			char root_path[MAX_PATH_LEN] = {0};
			form_path(&root_path,xml_generator->props_ds_root_path,"",MA_TRUE);
			if(MA_OK == (error = ma_ds_iterator_create(datastore, MA_TRUE,(char*)ma_temp_buffer_get(&root_path), &path_iterator))){
				while(MA_OK == ma_ds_iterator_get_next(path_iterator, &buffer)) {
					if(MA_OK == ma_buffer_get_string(buffer, &product_id, &size)){
						ma_table_t *section_table = NULL;
						ma_xml_node_t *product_root = NULL;
						/*
						 * Find the product in the full prop table 
						 * If found, then find or create the product xml node (delete = false)
						 * if not found, lookup the not running prod list, find or create the product in xml(delete = false)
						 * if not found in the "not running list", mark the product for deletion
						 */
						if(MA_TRUE == find_product_in_table(xml_generator->full_props, product_id, &section_table)){
							if(b_manipulate_db || MA_OK == (error = find_or_create_xml_product(prop_root,product_id, MA_FALSE, &product_root))) {
								(void)iterate_missing_sections_in_db(xml_generator, b_manipulate_db, product_id, section_table, product_root);
							}
							(void)ma_table_release(section_table);
						}
						else {
							if((MA_TRUE == ma_property_xml_generator_is_product_not_running(xml_generator, product_id))) {
								if((!b_manipulate_db)) {
									error = find_or_create_xml_product(prop_root,product_id, MA_FALSE, &product_root);
								}
							}
							else{
								if(!b_manipulate_db) {
									MA_LOG(xml_generator->logger, MA_LOG_SEV_TRACE, "Product missing %s ", product_id);
									error = mark_deleted_products(prop_root, product_id);
								}
								else {
									ma_temp_buffer_t product_path;
									ma_temp_buffer_init(&product_path);
									//char product_path[MAX_PATH_LEN] = {0};
									form_path(&product_path, (char*)ma_temp_buffer_get(&root_path), product_id,MA_TRUE);
									(void)remove_path_from_db(xml_generator->datastore, (char*)ma_temp_buffer_get(&product_path));
									ma_temp_buffer_uninit(&product_path);
								}
							}
						}
					}		
					(void)ma_buffer_release(buffer);
				}
				(void)ma_ds_iterator_release(path_iterator);
			}
			ma_temp_buffer_uninit(&root_path);
		}
		return MA_OK;
	}
	return error;
}

ma_error_t iterate_missing_sections_in_db(ma_property_xml_generator_t *xml_generator, ma_bool_t b_manipulate_db, const char* product_id, ma_table_t *section_table_from_full_prop, ma_xml_node_t *section_root) {
	ma_error_t error = MA_ERROR_INVALIDARG;
	const char *section_name = NULL;
	size_t size = 0;
	ma_buffer_t *buffer = NULL;
	if(xml_generator && product_id && section_table_from_full_prop){
		ma_temp_buffer_t product_path;
		ma_ds_iterator_t *section_iterator = NULL;
		ma_temp_buffer_init(&product_path);
		form_path(&product_path,xml_generator->props_ds_root_path,product_id,MA_TRUE);	
		if(MA_OK == (error == ma_ds_iterator_create(xml_generator->datastore, MA_TRUE, (char*)ma_temp_buffer_get(&product_path), &section_iterator))) {
			while(MA_OK == ma_ds_iterator_get_next(section_iterator, &buffer)){
				if(MA_OK == ma_buffer_get_string(buffer, &section_name, &size)) {
					ma_table_t *setting_table = NULL;
					if(find_section_in_table(section_table_from_full_prop, section_name, &setting_table)) {
						ma_xml_node_t *setting_root = NULL;
						if(!b_manipulate_db) {
							if(MA_TRUE == is_product(product_id))
								(void)find_or_create_xml_product_section(section_root, section_name, &setting_root);
							else 
								setting_root = section_root;
						}
						(void)iterate_missing_settings_in_db(xml_generator, b_manipulate_db , product_id, section_name,setting_table,setting_root);
						
						if(!b_manipulate_db) {
							if(ma_xml_node_get_child_count(setting_root) == 0) {
								(void)ma_xml_node_delete(setting_root);
								(void)ma_xml_node_release(setting_root);
							}
						}
						(void)ma_table_release(setting_table);
					}
					else {
						/*
						 *	An entire section was missing. 
						 *	So create a dummy table for iterating the missing settings so that the logic would mark them deleted.
						 */
						ma_xml_node_t *setting_root = NULL;
						ma_table_t *table = NULL;
						MA_LOG(xml_generator->logger, MA_LOG_SEV_TRACE,"Missing section : %s", section_name);
						if(MA_OK == ma_table_create(&table)){
							if(b_manipulate_db || MA_OK == find_or_create_xml_product_section(section_root, section_name, &setting_root)) {
								iterate_missing_settings_in_db(xml_generator, b_manipulate_db, product_id, section_name, table, setting_root);
								if(b_manipulate_db) {
									ma_temp_buffer_t section_path;
									ma_temp_buffer_init(&section_path);
									form_path(&section_path, (char*)ma_temp_buffer_get(&product_path), section_name,MA_TRUE);
									(void)ma_ds_rem(xml_generator->datastore, (char*) ma_temp_buffer_get(&section_path), NULL, MA_TRUE);
									ma_temp_buffer_uninit(&section_path);
								}
							}
							(void)ma_table_release(table);
							table = NULL;
						}
					}
				}
				(void)ma_buffer_release(buffer);
			}
			(void)ma_ds_iterator_release(section_iterator);
		}
		return MA_OK;
	}
	return error;
}

ma_error_t iterate_missing_settings_in_db(ma_property_xml_generator_t *xml_generator, ma_bool_t b_manipulate_db, const char *product_id, const char *section_name, ma_table_t *section_table_from_full_prop,ma_xml_node_t *settings_root) {
	ma_error_t error = MA_ERROR_INVALIDARG;
	const char *setting_name = NULL;
	size_t size = 0;
	if(xml_generator && product_id && section_name && section_table_from_full_prop) {
		ma_ds_iterator_t *settings_iterator = NULL;
		ma_buffer_t *buffer = NULL;
		ma_temp_buffer_t settings_path;
		ma_temp_buffer_init(&settings_path);
		form_path(&settings_path, xml_generator->props_ds_root_path, product_id,MA_TRUE);
		form_path(&settings_path, (char*)ma_temp_buffer_get(&settings_path), section_name,MA_TRUE);
		if(MA_OK == (error = ma_ds_iterator_create(xml_generator->datastore, MA_FALSE, (char*)ma_temp_buffer_get(&settings_path), &settings_iterator))) {
			while(MA_OK == ma_ds_iterator_get_next(settings_iterator, &buffer)) {
				if(MA_OK == ma_buffer_get_string(buffer, &setting_name,&size)){
					char *setting_value = NULL;
					if(MA_TRUE == find_setting_in_table(section_table_from_full_prop, setting_name, &setting_value)) {
						free(setting_value);
						setting_value = NULL;
					}
					else {
						if(!b_manipulate_db) {
							MA_LOG(xml_generator->logger, MA_LOG_SEV_TRACE,"Missing setting name : %s", setting_name);
							(void)mark_deleted_product_settings(settings_root, product_id, section_name, setting_name);
						}
						else
							(void)ma_ds_rem(xml_generator->datastore, (char*)ma_temp_buffer_get(&settings_path), setting_name, MA_FALSE);
					}
				}
				(void)ma_buffer_release(buffer);
			}
			(void)ma_ds_iterator_release(settings_iterator); 
			ma_temp_buffer_uninit(&settings_path);
		}
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t mark_deleted_products(ma_xml_node_t *product_root, const char *product_id) {
	if(product_root && product_id){
		ma_xml_node_t *node = NULL;
		(void)find_or_create_xml_product(product_root, product_id, MA_TRUE, &node);
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t mark_deleted_product_settings(ma_xml_node_t *setting_root, const char *product_id, const char *section_name, const char *setting_name) {
	if(setting_root && product_id && section_name && setting_name){
		ma_xml_node_t *node = NULL;
		if(MA_OK == find_or_create_property_xml_setting_node(setting_root, setting_name, is_product(product_id), NULL, MA_FALSE,&node)) {
			(void)ma_xml_node_attribute_set(node, MA_STR_PROPERTY_ATTR_DELETE, MA_STR_TRUE);
		}

		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t remove_path_from_db(ma_ds_t *datastore, const char *path){
	if(datastore && path) {
		ma_ds_iterator_t *key_iterator = NULL;
		ma_ds_iterator_t *path_iterator = NULL;

		if(MA_OK == ma_ds_iterator_create(datastore, MA_FALSE, path, &key_iterator)){
			ma_buffer_t *buffer = NULL;
			while(MA_OK == ma_ds_iterator_get_next(key_iterator, &buffer)){
				const char *key = NULL;
				size_t size = 0;
				if(MA_OK == ma_buffer_get_string(buffer, &key, &size)){
					(void)ma_ds_rem(datastore, path, key, MA_FALSE);
				}
				(void)ma_buffer_release(buffer);
			}
			(void)ma_ds_iterator_release(key_iterator);
		}

		if(MA_OK == ma_ds_iterator_create( datastore, MA_TRUE, path, &path_iterator)) {
			ma_buffer_t *buffer = NULL;
			while(MA_OK == ma_ds_iterator_get_next(path_iterator, &buffer)) {
				const char *sub_path = NULL;
				size_t size = 0;
				if(MA_OK == ma_buffer_get_string(buffer, &sub_path, &size)){
					ma_temp_buffer_t new_path;
					ma_temp_buffer_init(&new_path);
					form_path(&new_path,path, sub_path,MA_TRUE);
					remove_path_from_db(datastore, (char*) ma_temp_buffer_get(&new_path));
					ma_temp_buffer_uninit(&new_path);
				}
				(void)ma_buffer_release(buffer);
			}
			(void)ma_ds_iterator_release(path_iterator);
		}
		(void)ma_ds_rem(datastore, path, NULL, MA_TRUE);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}


ma_error_t ma_property_xml_generator_save_props_to_datastore(ma_property_xml_generator_t *xml_generator) {
	if(xml_generator) {
		ma_table_t *full_prop_table = xml_generator->full_props;
		ma_props_table_iterator_t *data = NULL;
		ma_error_t rc = MA_OK;
		if(MA_OK == (rc = ma_ds_database_transaction_begin((ma_ds_database_t *)xml_generator->datastore))) {
			if(MA_OK == (rc = iterate_missing_products_in_db(xml_generator, MA_TRUE, NULL))) {
				if(MA_OK == (rc =  ma_props_table_iterator_create(&data))) {
					if(MA_OK == (rc = ma_props_table_iterator_set(data, MA_TRUE, MA_TRUE, xml_generator->props_ds_root_path, NULL, NULL, NULL, xml_generator))) {
						ma_table_foreach(full_prop_table, ma_prop_table_product_iterator, data);
					}
					(void)ma_props_table_iterator_release(data);
				}
			}
			rc = (MA_OK == rc ) ? ma_ds_database_transaction_end((ma_ds_database_t *)xml_generator->datastore) : ma_ds_database_transaction_cancel((ma_ds_database_t *)xml_generator->datastore);
		}
		/*doesnt matter the return code */
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_property_xml_generator_get_last_full_props(ma_property_xml_generator_t *xml_generator, unsigned char **full_buffer, size_t *buffer_size) {
	if(xml_generator && full_buffer && buffer_size) {
		ma_error_t error = MA_OK;
		ma_ds_t *ds = xml_generator->datastore;
		ma_ds_iterator_t *product_iterator = NULL;
		ma_xml_node_t *prop_root = NULL;
		ma_xml_t *full_props_xml = NULL;
		ma_buffer_t *product_id_buffer = NULL;
		ma_bool_t b_iterated = MA_FALSE;
		if(MA_OK == (error = ma_xml_create(&full_props_xml))) {
			if(MA_OK == (error = ma_xml_construct(full_props_xml))) {
				ma_temp_buffer_t root_path;
				ma_temp_buffer_init(&root_path);
				form_path(&root_path,xml_generator->props_ds_root_path,"",MA_TRUE);
				if(MA_OK == ma_ds_iterator_create(ds, MA_TRUE, (char*)ma_temp_buffer_get(&root_path), &product_iterator)) {
					if(MA_OK == (error = find_or_create_xml_property_root(full_props_xml, xml_generator, MA_TRUE, &prop_root))) {
						const char *product_id = NULL;
						size_t size = 0;
						while(MA_OK == ma_ds_iterator_get_next(product_iterator, &product_id_buffer)) {
							b_iterated = MA_TRUE;
							if(MA_OK == ma_buffer_get_string(product_id_buffer, &product_id, &size)){
								ma_xml_node_t *product_node = NULL;
								ma_temp_buffer_t product_root_path = MA_TEMP_BUFFER_INIT;
								form_path(&product_root_path,xml_generator->props_ds_root_path,product_id,MA_TRUE);
								if(MA_OK == find_or_create_xml_product(prop_root, product_id, MA_FALSE, &product_node)) {
									ma_bool_t b_product= is_product(product_id);
									ma_ds_iterator_t *section_iterator = NULL;
									ma_buffer_t *section_name_buffer = NULL;
									if(MA_OK == ma_ds_iterator_create(ds, MA_TRUE, (char*)ma_temp_buffer_get(&product_root_path), &section_iterator)) {
										while(MA_OK == ma_ds_iterator_get_next(section_iterator, &section_name_buffer)) {
											const char *section_name = NULL;
											size_t section_size = 0; 
											ma_xml_node_t *section_node = NULL;
											if(MA_OK == ma_buffer_get_string(section_name_buffer, &section_name, &section_size)) {
												if(b_product) {
													(void)find_or_create_xml_product_section(product_node, section_name, &section_node);
												}
												else {
													section_node = product_node;
												}

												{
													ma_ds_iterator_t *settings_iterator = NULL;
													ma_temp_buffer_t settings_key_path = MA_TEMP_BUFFER_INIT;
													form_path(&settings_key_path, (const char *)ma_temp_buffer_get(&product_root_path), section_name, MA_TRUE);
													if(MA_OK == ma_ds_iterator_create(ds, MA_FALSE, (const char *) ma_temp_buffer_get(&settings_key_path), &settings_iterator)) {
														ma_buffer_t *settings_name_buffer = NULL;
														ma_buffer_t *settings_value_buffer = NULL;
														const char *settings_key = NULL;
														const char *settings_value = NULL;
														size_t settings_key_size =0;
														size_t settings_size =0;
														while(MA_OK == ma_ds_iterator_get_next(settings_iterator, &settings_name_buffer)) {
															if(MA_OK == ma_buffer_get_string(settings_name_buffer, &settings_key, &settings_key_size)) {
																if(MA_OK == ma_ds_get_str(ds, (const char *) ma_temp_buffer_get(&settings_key_path), settings_key, &settings_value_buffer)) {
																	if(MA_OK == ma_buffer_get_string(settings_value_buffer, &settings_value, &settings_size)) {
																		ma_xml_node_t *settings_node = NULL;
																		(void)find_or_create_property_xml_setting_node(section_node, settings_key, b_product, (char *)settings_value, MA_FALSE, &settings_node);
																	}
																	(void)ma_buffer_release(settings_value_buffer);
																}
															}
															(void)ma_buffer_release(settings_name_buffer);
														}
														(void)ma_ds_iterator_release(settings_iterator);
													}
													ma_temp_buffer_uninit(&settings_key_path);
												}
											}
										}
										(void)ma_ds_iterator_release(section_iterator);
									}
								}
								ma_temp_buffer_uninit(&product_root_path);
							}
							(void)ma_buffer_release(product_id_buffer);
							product_id_buffer = NULL;
						}
					}
					(void)ma_ds_iterator_release(product_iterator);
				}
				ma_temp_buffer_uninit(&root_path);	
			}
		}
		
		if(full_props_xml) {
			if(b_iterated) {
				ma_bytebuffer_t *bytebuffer = NULL;
				if(MA_OK == (error = ma_bytebuffer_create(1024, &bytebuffer))) {
					size_t _size = 0;
					unsigned char *p_temp_buf = NULL;
					ma_xml_save_to_buffer(full_props_xml, bytebuffer);
					_size = ma_bytebuffer_get_size(bytebuffer);
					p_temp_buf = ma_bytebuffer_get_bytes(bytebuffer);
					*full_buffer = (unsigned char *) calloc(_size + 1 ,sizeof(unsigned char));
					if(*full_buffer) {
						strncpy((char*) *full_buffer,(char*) p_temp_buf, _size);
						(*full_buffer)[_size] = '\0';
						*buffer_size = _size;
					}
					MA_LOG(xml_generator->logger, MA_LOG_SEV_TRACE, "%s", *full_buffer);
					(void)ma_bytebuffer_release(bytebuffer); bytebuffer = NULL;
				}
			}
			else {
				error = MA_ERROR_NOT_YET_REGISTERED;
			}
			(void)ma_xml_release(full_props_xml);
			full_props_xml = NULL;
		}
		return error ;
 	}
	return MA_ERROR_INVALIDARG;
}
