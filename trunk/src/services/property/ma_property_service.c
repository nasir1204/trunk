#include "ma_property_manager.h"
#include "ma_property_service_internal.h"

#include "ma/internal/ma_macros.h"

#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/core/msgbus/ma_msgbus_private.h"
#include "ma/internal/utils/platform/ma_system_property.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/utils/platform/ma_sys_apis.h"
#include "ma/internal/utils/time/ma_time.h"
#include "ma/internal/defs/ma_property_defs.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/services/ahclient/ma_ah_client_service.h"
#include "ma/internal/services/ma_agent_policy_settings_bag.h"
#include "ma/internal/clients/ma/ma_client_event.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "property"

#define MA_CONTEXT_GET_DATASTORE(context) (ma_configurator_get_datastore(MA_CONTEXT_GET_CONFIGURATOR(context)))

//#define MA_PROPERTY_LAST_PROP_COLLECTION_STATUS	"PROPERTY_COLLECTION_STATUS_LAST"

ma_error_t ma_property_provider_info_create(const char *provider_id, ma_property_provider_info_t **pp_info) ;
ma_error_t ma_property_provider_info_release(ma_property_provider_info_t *pp_info) ;

static ma_error_t ma_property_collect_session_create(ma_property_collect_session_t **pcs) ;
static ma_error_t ma_property_collect_session_release(ma_property_collect_session_t *pcs) ;
static ma_error_t ma_property_collect_session_add_provider(ma_property_collect_session_t *self, const char *provider_id, ma_uint64_t clientflags) ;
static ma_error_t ma_property_collect_session_remove_provider(ma_property_collect_session_t *self, const char *provider_id) ;
static ma_error_t ma_property_collect_session_get_provider(ma_property_collect_session_t *self, const char *provider_id, ma_property_provider_info_t **pp_info) ;
static ma_error_t ma_property_collect_session_clear_providers(ma_property_collect_session_t *self) ;

static ma_error_t ma_property_service_cb(ma_msgbus_server_t *server, ma_message_t *msg, void *cb_data, ma_msgbus_client_request_t *c_request) ;

static ma_error_t ma_property_raise_alert(ma_property_service_t *service) ;

static ma_error_t ma_property_service_configure(ma_property_service_t *self, ma_context_t *context, unsigned int hint) ;
static ma_error_t ma_property_service_start(ma_property_service_t *self) ;
static ma_error_t ma_property_service_stop(ma_property_service_t *self) ;
static void ma_property_service_release(ma_property_service_t *self) ;
static void timer_close(uv_handle_t *handle);

static ma_service_methods_t base_methods = {
    &ma_property_service_configure,
    &ma_property_service_start,
    &ma_property_service_stop,
    &ma_property_service_release
} ;

ma_error_t ma_property_service_create(const char *service_name, ma_service_t **service) {
	if(service_name && service ) {
		ma_property_service_t *self = (ma_property_service_t *)calloc(1,sizeof(ma_property_service_t));

        if(self) {
			(void)ma_registry_store_create(&self->app_store);
			if(self->app_store){
				ma_service_init(&self->base, &base_methods, service_name, (void *)self) ;
				*service = &self->base ;
				return MA_OK ;
			}
			free(self);
        }
        return MA_ERROR_OUTOFMEMORY ;
	}
	return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_property_service_configure(ma_property_service_t *self, ma_context_t *context, unsigned int hint) {
    if(context && self) {
        self->context = context ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_property_service_start(ma_property_service_t *self) {
	if(self) {
        ma_error_t rc = MA_OK ;

        if(MA_OK == (rc = ma_msgbus_server_create(MA_CONTEXT_GET_MSGBUS(self->context), MA_PROPERTY_SERVICE_NAME_STR, &self->server))
            && MA_OK == (rc = ma_property_manager_create(self, &self->ppm))
            && MA_OK == (rc = ma_property_collect_session_create(&self->sesion))    
           ) 
        {
            (void)ma_msgbus_server_setopt(self->server, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC) ;
            (void)ma_msgbus_server_setopt(self->server, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO) ;
            if(MA_OK == (rc = ma_msgbus_server_start(self->server, ma_property_service_cb, self))) {				
				ma_event_loop_t *maloop = ma_msgbus_get_event_loop(MA_CONTEXT_GET_MSGBUS(self->context));				
                uv_timer_init(ma_event_loop_get_uv_loop(maloop), &self->sesion->session_timer) ;
                self->session_state = PROPERTY_SESSION_STATE_INITIAL ;
            }
        }

        if(MA_OK != rc) {
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "PropertyService start failed(%d)", rc) ;
            if(self->ppm) ma_property_collect_session_release(self->sesion) ;
            if(self->ppm) ma_property_manager_release(self->ppm) ;
            if(self->server) ma_msgbus_server_release(self->server) ;
        }
		else {
			if(MA_OK != (rc = ma_ah_client_service_register_spipe_decorator(MA_CONTEXT_GET_AH_CLIENT(self->context), ma_property_manager_get_decorator(self->ppm))))
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to register property spipe decorator - rc <%d>", rc);

			if(MA_OK != (rc = ma_ah_client_service_register_spipe_handler(MA_CONTEXT_GET_AH_CLIENT(self->context), ma_property_manager_get_handler(self->ppm))))
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to register property spipe handler - rc <%d>", rc);
		}
        return rc ;
	}

	return MA_ERROR_INVALIDARG;
}

static void property_service_stop_cb(uv_handle_t *handle) {
    ma_property_collect_session_t *session = (ma_property_collect_session_t *)handle->data ;
    (void)ma_property_collect_session_release(session) ;
    return ;
}

ma_error_t ma_property_service_stop(ma_property_service_t *self) {
	if(self) {
        ma_error_t rc = MA_OK;

        uv_timer_stop(&self->sesion->session_timer) ;
        self->sesion->session_timer.data = self->sesion ;
        uv_close((uv_handle_t *)&self->sesion->session_timer, property_service_stop_cb) ;

        if(self->timer) {
            uv_timer_stop(self->timer);
            uv_close((uv_handle_t*)self->timer, timer_close);
            if(self->reserved)(void)ma_message_release((ma_message_t*)self->reserved);
        }
        if(MA_OK != (rc = ma_ah_client_service_unregister_spipe_decorator(MA_CONTEXT_GET_AH_CLIENT(self->context), ma_property_manager_get_decorator(self->ppm))))
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to unregister property spipe decorator - rc <%d>", rc);

        if(MA_OK != (rc = ma_ah_client_service_unregister_spipe_handler(MA_CONTEXT_GET_AH_CLIENT(self->context), ma_property_manager_get_handler(self->ppm))))
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to unregister property spipe handler - rc <%d>", rc);

		if(MA_OK != (rc = ma_msgbus_server_stop(self->server)))
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to stop property service - rc <%d>", rc);
      
        return rc;
	}
	return MA_ERROR_INVALIDARG;
}

void ma_property_service_release(ma_property_service_t *self) {
	if(self) {
        (void)ma_property_manager_release(self->ppm) ;
        (void)ma_msgbus_server_release(self->server) ;
		(void)ma_registry_store_release(self->app_store);
        free(self) ;
        self = NULL ;
	}
	return ;
}

static void ma_property_collection_last_status_update(ma_property_service_t *self, const char *status){
	ma_bool_t is_exists = 0 ;
    if(MA_OK != ma_ds_is_path_exists(MA_CONTEXT_GET_DATASTORE(self->context), MA_PROPERTY_PROPERTIES_PATH_NAME_STR, &is_exists))
        return ;
	if(is_exists){
		 ma_ds_set_str(MA_CONTEXT_GET_DATASTORE(self->context), MA_PROPERTY_PROPERTIES_PATH_NAME_STR, MA_PROPERTY_PROPERTIES_KEY_LAST_COLLECTION_STATUS, status, 1) ;
    } 
}

static char *get_agent_locale(ma_property_service_t *self, char *locale, int size){	
	ma_buffer_t *buffer = NULL;
	ma_bool_t   is_agent_lang_enabled = MA_FALSE;
	ma_configurator_t *configurator = MA_CONTEXT_GET_CONFIGURATOR(self->context);
	ma_ds_t *ds = ma_configurator_get_datastore(configurator);
    
	/*Get the agent locale and set.*/
	if(MA_OK == ma_ds_get_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_LANGUAGE_STR, &buffer)){
		const char *lang = NULL;
		size_t len = 0;
		if(MA_OK == ma_buffer_get_string(buffer, &lang, &len)){
			if(lang && len && strcmp(lang, "N/A") && len < size) {
				is_agent_lang_enabled = MA_TRUE;
				MA_MSC_SELECT(_snprintf, snprintf)(locale, size, "%s", lang);				
			}
		}
        (void)ma_buffer_release(buffer);
    }	
	if(!is_agent_lang_enabled){
		ma_system_property_t *props = MA_CONTEXT_GET_SYSTEM_PROPERTY(self->context);
		const ma_misc_system_property_t *misc = ma_system_property_misc_get(props, MA_FALSE);
		if(misc)
			MA_MSC_SELECT(_snprintf, snprintf)(locale, size, "%s", misc->language_code);		
	}
	/*Get the locale from the task and set if any.*/
	return locale;
}

static ma_bool_t check_if_product_property_already_reported(ma_property_service_t *self, char *software_id){
	return MA_TRUE;

	/* this was added so that we don't generate events for the pp which doesn't report props at all.

	ma_error_t rc = MA_OK;
	ma_bool_t ret = MA_FALSE;
	ma_error_t error = MA_ERROR_INVALIDARG;
	ma_buffer_t *buffer = NULL;
	ma_ds_iterator_t *path_iterator = NULL;
	ma_ds_t *ds = ma_configurator_get_datastore(MA_CONTEXT_GET_CONFIGURATOR(self->context));

	if(MA_OK == (error = ma_ds_iterator_create(ds, MA_TRUE,MA_PROPERTY_DATA_PATH_NAME_STR, &path_iterator))){

		while(MA_OK == ma_ds_iterator_get_next(path_iterator, &buffer)) {
			const char *product_id = NULL;
			size_t size = 0;
			if(MA_OK == ma_buffer_get_string(buffer, &product_id, &size)){
				if(!strcmp(	product_id, software_id)){
					ret = MA_TRUE;
					ma_buffer_release(buffer); buffer = NULL;
					break;
				}
			}
			ma_buffer_release(buffer);
		}
		ma_ds_iterator_release(path_iterator);
	}
	return ret;*/
}

static void generate_property_collection_failure_event(ma_property_service_t *self, char *software_id){
	struct ma_event_parameter_s param = {0};
	MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_WARNING, "Property collection failure event for %s.", software_id) ;
	ma_event_parameter_init(&param);
	param.eventid = MA_PROPERTY_COLLECT_FAILURE_EVENT_ID;
	param.severity = MA_EVENT_SEVERITY_CRITICAL;
	param.source_productid = software_id;
	param.type = MA_PROPERTY_EVENT_THREAT_TYPE_STR;
	param.initiator_id = MA_SOFTWAREID_GENERAL_STR;
	get_agent_locale(self, param.locale, sizeof(param.locale));
	MA_MSC_SELECT(_snprintf, snprintf)(param.error, sizeof(param.error), "%d", MA_PROPERTY_COLLECT_FAILURE_ERROR_ID);	

	ma_client_event_generate(MA_CONTEXT_GET_CLIENT(self->context), &param);
}

static void property_session_timer_cb(uv_timer_t *ps_timer, int status) {
    ma_property_service_t *self = (ma_property_service_t *)ps_timer->data ;
	ma_bool_t any_last_status_update_fail = MA_FALSE;
	ma_error_t rc = MA_OK ;

    if(PROPERTY_SESSION_STATE_IN_PROGRESS == self->session_state) {
        ma_property_provider_info_t *pp_info = NULL ;

        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO, "Property collection session timedout.") ;
        
		self->session_state = PROPERTY_SESSION_STATE_TIMEDOUT ;
        uv_timer_stop(&self->sesion->session_timer) ;
        
		MA_SLIST_FOREACH(self->sesion, providers, pp_info) {
            if(pp_info->is_property_provided == MA_FALSE) {
				ma_variant_t *props = NULL ;
				ma_table_t *props_table = NULL ;
				
				if(MA_OK == (rc = ma_ds_get_variant(MA_CONTEXT_GET_DATASTORE(self->context), MA_PROPERTY_DATA_PRODUCT_PROPERTIES_PATH_STR, pp_info->provider_id, MA_VARTYPE_TABLE, &props))) {
					if(MA_OK == (rc = ma_variant_get_table(props, &props_table))) {	/* add proprs to xml generator */
						MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Sending properties for %s from previous session.", pp_info->provider_id) ;
						rc = ma_property_xml_generator_append_prop(self->sesion->pxml, pp_info->provider_id, props_table);							
						(void)ma_table_release(props_table) ;
					}
					(void)ma_variant_release(props);
				}
				else if(pp_info->service_required_flags & MA_SERVICE_PROPERTY) {
					if(!pp_info->is_last_props_collection_failed) {
						MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Generating property collection failure event for product %s due to prop collect session timeout", pp_info->provider_id) ;
						(void) generate_property_collection_failure_event(self, pp_info->provider_id);
					}
					pp_info->is_last_props_collection_failed = MA_TRUE;
				}

				if(MA_OK != rc ) {
					any_last_status_update_fail = MA_TRUE;
					(void)ma_property_xml_generator_set_product_not_running(self->sesion->pxml, pp_info->provider_id) ;
				}
			}
			(void)ma_ds_remove(MA_CONTEXT_GET_DATASTORE(self->context), MA_PROPERTY_DATA_PRODUCT_PROPERTIES_PATH_STR, pp_info->provider_id) ;
        }

		if(any_last_status_update_fail)
			ma_property_collection_last_status_update(self, "0");

		if(MA_OK != ma_property_raise_alert(self)) {
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Raised spipe alert failed, <rc= %d>", rc) ;
			self->session_state = PROPERTY_SESSION_STATE_INITIAL ;						
			(void)ma_property_xml_generator_release(self->sesion->pxml) ;  self->sesion->pxml = NULL ;
		}
    }
    else {
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_WARNING, "Property collection session timedout, but session not in progress") ;
    }
    return ;
}

static void ma_property_get_new_version(char *version, size_t size) {
	ma_time_t time = {0, 0, 0, 0, 0, 0, 0 }; 
	ma_time_get_localtime(&time) ;
	MA_MSC_SELECT(_snprintf, snprintf)(version, size, "%04d%02d%02d%02d%02d%02d", time.year, time.month, time.day, time.hour, time.minute, time.second) ;
}

static ma_error_t collection_handler(ma_property_service_t *self, ma_message_t *msg) {
	ma_ds_t *ds_handle = MA_CONTEXT_GET_DATASTORE(self->context) ;
	ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context) ;

       
	MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "Collecting Properties");
		
	if(MA_OK == ma_registry_store_scan(self->app_store)) /* doing a scan , if any products are registered after last poprty collection TBD-shud we do it here??*/
    {
        ma_error_t rc = MA_OK ;
        ma_buffer_t *buffer = NULL ;
        const ma_application_t *app = NULL ;
        const char *provider = NULL ;
        ma_uint64_t rs = 0 ;

		ma_property_provider_info_t *pp_info = NULL ;
		MA_SLIST_FOREACH(self->sesion, providers, pp_info) {
			pp_info->mark_for_deletion = MA_TRUE;				
		}		

        MA_REGISTRY_STORE_FOR_EACH_APPLICATION(self->app_store, app) {
            provider = ma_application_get_software_id(app) ;

			rs = ma_application_get_required_services_flag(app) ;
            if(strncmp(provider, "CMNUPD__3000", strlen("CMNUPD__3000")) && strncmp(provider, "PCR_____1000", strlen("PCR_____1000"))
				&& strncmp(provider, "EPOAGENT3000META", strlen("EPOAGENT3000META")) )
			{
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "Adding property provider %s with required service flags 0x%08x", provider, rs);
				(void)ma_property_collect_session_add_provider(self->sesion, provider, rs) ;
			}
        }

		(void)ma_property_collect_session_clear_providers(self->sesion) ;

        if(0 < MA_SLIST_GET_COUNT(self->sesion, providers)) 
        {
			const char *guid = NULL ;
			const char *tenantid = NULL;
			ma_buffer_t *tenantid_buffer = NULL ;
			size_t buf_size = 0 ;				
			ma_int32_t property_collect_session_timeout = 1;
                
			const ma_misc_system_property_t *misc_sys_prop = ma_system_property_misc_get(MA_CONTEXT_GET_SYSTEM_PROPERTY(self->context), MA_FALSE) ;
		    ma_policy_settings_bag_t *bag = MA_CONTEXT_GET_POLICY_SETTINGS_BAG(self->context) ;
				
			ma_bool_t collect_full_props = MA_FALSE ;
			const char *props_type = NULL ;
            ma_message_t *publish_collect_props_msg = NULL;


			MA_MSC_SELECT(_snprintf, snprintf)(self->sesion->session_id, 4, "%d", ma_sys_rand()) ; /* is this ok or do we need to have the sessionid date/time dependent ??? */
			self->sesion->session_timer.data = self ;                
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "uv timer data(%p)\n", self);
            if(MA_OK == (rc = ma_message_get_property(msg, MA_PROPERTY_MSG_KEY_PROPS_TYPE_STR, &props_type))) {
                if(props_type && ((0 == strncmp(props_type, MA_PROPERTY_MSG_VALUE_PROPS_TYPE_FULL_STR, 1)) 
					|| (0 == strncmp(props_type, MA_PROPERTY_MSG_VALUE_PROPS_TYPE_INCR_STR, 1))
					|| (0 == strncmp(props_type, MA_PROPERTY_MSG_VALUE_PROPS_TYPE_VERSION_STR, 1)))) {
					const char *props_version = NULL ;
					size_t props_version_size = 0 ;

		            if(MA_OK == (rc = ma_message_clone(msg,&publish_collect_props_msg))) {
				        /* Reading property collect full props policy */

						if(0 == strncmp(props_type, MA_PROPERTY_MSG_VALUE_PROPS_TYPE_FULL_STR, 1)) {
							self->sesion->props_type = PROPS_TYPE_FULL ;
							collect_full_props = MA_TRUE ;
						}else if (0 == strncmp(props_type, MA_PROPERTY_MSG_VALUE_PROPS_TYPE_VERSION_STR, 1)) {
							self->sesion->props_type = PROPS_TYPE_VERSION;
							/* this flag is for the client whether to collect all the sections of only General */
							rc = ma_policy_settings_bag_get_bool(bag, MA_PROPERTY_SERVICE_SECTION_NAME_STR, MA_PROPERTY_KEY_COLLECT_FULL_PROPS_INT, MA_FALSE, &collect_full_props, MA_FALSE) ;
						}
						else {
							self->sesion->props_type = PROPS_TYPE_INCR ;
							/* this flag is for the client whether to collect all the sections of only General */
							rc = ma_policy_settings_bag_get_bool(bag, MA_PROPERTY_SERVICE_SECTION_NAME_STR, MA_PROPERTY_KEY_COLLECT_FULL_PROPS_INT, MA_FALSE, &collect_full_props, MA_FALSE) ;
						}

				        (void)ma_message_set_property(publish_collect_props_msg, MA_PROPERTY_MSG_KEY_SESSION_ID_INT, self->sesion->session_id) ;
                        (void)ma_message_set_property(publish_collect_props_msg, MA_PROPERTY_MSG_KEY_MINIMAL_PROPS_INT, collect_full_props? "0" : "1") ;

				        if(MA_OK == ma_ds_get_str(ds_handle, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_GUID_STR, &buffer)) {
					        (void)ma_buffer_get_string(buffer, &guid, &buf_size) ;
				        }

				        if(MA_OK == ma_ds_get_str(ds_handle, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_TENANT_ID_STR, &tenantid_buffer)) {
					        (void)ma_buffer_get_string(tenantid_buffer, &tenantid, &buf_size) ;
				        }

				        (void)ma_property_get_new_version(self->sesion->props_version, 18) ;

				        if(MA_OK == (rc = ma_property_xml_generator_create(&self->sesion->pxml, ds_handle, MA_PROPERTY_DATA_PATH_NAME_STR, misc_sys_prop->computername, guid, tenantid, self->sesion->props_version) ))
			            {
							ma_property_xml_generator_set_logger(self->sesion->pxml, MA_CONTEXT_GET_LOGGER(self->context));
					        if(MA_OK == (rc = ma_msgbus_publish(MA_CONTEXT_GET_MSGBUS(self->context), MA_PROPERTY_PUBSUB_TOPIC_COLLECT_STR, MSGBUS_CONSUMER_REACH_OUTPROC, publish_collect_props_msg))) 
				            {
                                char *enforce_policy = NULL ;

                                (void)ma_message_get_property(msg, MA_PROPERTY_MSG_KEY_ENFORCE_POLICY_STR, &enforce_policy) ;
                                self->sesion->enforce_policy = enforce_policy? MA_TRUE : MA_FALSE ;

					            self->session_state = PROPERTY_SESSION_STATE_IN_PROGRESS ;

					            //ma_ds_get_int32(ds_handle, MA_PROPERTY_POLICIES_PATH_NAME_STR, MA_PROPERTY_KEY_COLLECT_SESSION_TIMEOUT_INT, &property_collect_session_timeout) ;
								ma_policy_settings_bag_get_int(bag, MA_PROPERTY_SERVICE_SECTION_NAME_STR, MA_PROPERTY_KEY_COLLECT_SESSION_TIMEOUT_INT, MA_FALSE, &property_collect_session_timeout, 2) ;

                                property_collect_session_timeout = property_collect_session_timeout * MA_SLIST_GET_COUNT(self->sesion, providers) + 30 ;
                        
					            uv_timer_start(&self->sesion->session_timer, property_session_timer_cb, (property_collect_session_timeout *1000) , 0) ;

								MA_LOG(logger, MA_LOG_SEV_INFO, "Property collection session initiated for %s with session id %s.", (self->sesion->props_type == PROPS_TYPE_FULL)?"FullProps":((self->sesion->props_type == PROPS_TYPE_INCR)?"IncrProps":"PropsVersion"), self->sesion->session_id ) ;
				            }
				            else {
					            (void)ma_property_xml_generator_release(self->sesion->pxml) ;
								MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to publish property collect message to providers <rc = %d>", rc) ;
							}
			            }
				        else {
					        MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to create property xml generator, rc = %d.", rc) ;
				        }
				        if(buffer) (void)ma_buffer_release(buffer) ;
				        if(tenantid_buffer) (void)ma_buffer_release(tenantid_buffer) ;
                        (void)ma_message_release(publish_collect_props_msg) ;
                    }
					else {
						MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to clone the message <rc = %d>", rc ) ;
					}
                }else {
                    rc = MA_ERROR_INVALIDARG;
                    MA_LOG(logger, MA_LOG_SEV_ERROR, "invalid request recived as props type is invalid ");
                }
            }
			else {
				MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to get the props type from message <rc = %d>", rc) ;
			}
            return rc ;
        }
        else {
            MA_LOG(logger, MA_LOG_SEV_INFO, "No property providers are registered.") ;
        }
		return rc ;
	}
    else {
        MA_LOG(logger, MA_LOG_SEV_ERROR, "Registory store scan failed, not initiating the property collection.") ;
		return MA_ERROR_UNEXPECTED ;
    }


}

void timer_close(uv_handle_t *handle) {
    if(handle) {
        free(handle);
    }
}

static void timer_cb(uv_timer_t *timer, int status) {
    if(timer) {
        ma_property_service_t *self = (ma_property_service_t*)timer->data;
        ma_message_t *msg = (ma_message_t*)self->reserved;
	    ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context) ;

        uv_close((uv_handle_t*)timer, &timer_close);
        self->timer = NULL;
        if(PROPERTY_SESSION_STATE_INITIAL != self->session_state) {
		    MA_LOG(logger, MA_LOG_SEV_DEBUG | MA_LOG_FLAG_LOCALIZE, "Collecting Properties");
		    MA_LOG(logger, MA_LOG_SEV_INFO, "Poperty Collect session in progress, ignoring the request.");
        } else
            if(MA_OK != collection_handler(self, msg))  self->sesion->enforce_policy = MA_FALSE ;
        (void)ma_message_release(msg);
        self->reserved = NULL;
    }
}

static ma_error_t property_collect_handler(ma_property_service_t *self, ma_message_t *msg) {
    if(self && msg) {
	    ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context) ;
        const char *random_str = NULL;
        size_t random_intervals = 0;
        ma_error_t rc = MA_OK ;

        (void)ma_message_get_property(msg, MA_PROPERTY_MSG_KEY_RANDOM_INTERVALS_STR, &random_str);
        if(random_intervals = random_str ? atoi(random_str) : 0) {
            if(!self->timer) {
                if((self->timer = (uv_timer_t*)calloc(1, sizeof(uv_timer_t)))) {
                    uv_timer_init(ma_event_loop_get_uv_loop(ma_msgbus_get_event_loop(MA_CONTEXT_GET_MSGBUS(self->context))), self->timer);
                    self->timer->data = self;
                    MA_LOG(logger, MA_LOG_SEV_TRACE, "uv timer data(%p)", self);
                    (void)ma_message_add_ref((ma_message_t*)(self->reserved = msg));
                    srand((unsigned int)time(NULL));
                    random_intervals = (rand() % (random_intervals/60)) * 60;
                    random_intervals = random_intervals ? random_intervals : 1/* 1 default secs */;
		            MA_LOG(logger, MA_LOG_SEV_INFO, "randomizing property collection with intervals(%d) in secs", random_intervals);  
                    uv_timer_start(self->timer, timer_cb, random_intervals * 1000, 0);
                }
            }
        } else {
	        if(PROPERTY_SESSION_STATE_INITIAL != self->session_state) {
		        MA_LOG(logger, MA_LOG_SEV_DEBUG | MA_LOG_FLAG_LOCALIZE, "Collecting Properties");
		        MA_LOG(logger, MA_LOG_SEV_INFO, "Poperty Collect session in progress, ignoring the request.");
		        return MA_OK ;
            }
            if(MA_OK != (rc = collection_handler(self, msg)))   self->sesion->enforce_policy = MA_FALSE ;
        }

        return rc ;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t property_collected_handler(ma_property_service_t *self, ma_message_t *msg) {
	ma_error_t rc = MA_OK ;        
	const char *session_id = NULL ;
    const char *provider = NULL ;
    
    ma_variant_t *pp_msg_payload = NULL ;
    ma_table_t *props_table = NULL ;

	ma_ds_t *ds_handle = MA_CONTEXT_GET_DATASTORE(self->context) ;
	ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context) ;

	(void)ma_message_get_property(msg, MA_PROPERTY_MSG_KEY_SESSION_ID_INT, &session_id) ;
    (void)ma_message_get_property(msg, MA_PROPERTY_MSG_KEY_PROVIDER_ID_STR, &provider) ;
	(void)ma_message_get_payload(msg, &pp_msg_payload) ;
	(void)ma_variant_get_table(pp_msg_payload, &props_table) ;

	if(session_id && provider && pp_msg_payload && props_table) {
		if(PROPERTY_SESSION_STATE_IN_PROGRESS == self->session_state) {
			ma_property_provider_info_t *pp_info = NULL ;

			(void)ma_property_collect_session_get_provider(self->sesion, provider, &pp_info) ;

			if(session_id && !strcmp(self->sesion->session_id, session_id)) {
				if(pp_info) {
					int p_count = MA_SLIST_GET_COUNT(self->sesion, providers) ;
					pp_info->is_property_provided = MA_TRUE ;
					pp_info->is_last_props_collection_failed = MA_FALSE;
					rc = ma_property_xml_generator_append_prop(self->sesion->pxml, provider, props_table) ;
					MA_LOG(logger, MA_LOG_SEV_INFO, "Properties received from %s provider", provider) ;
					
					MA_SLIST_FOREACH(self->sesion, providers, pp_info) {
						if(pp_info->is_property_provided == MA_TRUE)
							--p_count ;
					}

					if(p_count == 0) {
						MA_LOG(logger, MA_LOG_SEV_INFO, "Finished Collecting Properties") ;

						uv_timer_stop(&self->sesion->session_timer) ;
						ma_property_collection_last_status_update(self, "1") ;
						
						if(MA_OK == (rc = ma_property_raise_alert(self)))
							MA_LOG(logger, MA_LOG_SEV_DEBUG, "Raised spipe alert") ;
						else {
							self->session_state = PROPERTY_SESSION_STATE_INITIAL ;
							MA_LOG(logger, MA_LOG_SEV_ERROR, "Raised spipe alert failed, <rc= %d>", rc) ;
							(void)ma_property_xml_generator_release(self->sesion->pxml) ; self->sesion->pxml = NULL ;
						}
					}			
				}
				else {
					MA_LOG(logger, MA_LOG_SEV_ERROR, "Property received from unregistered provider") ;
					rc = ma_property_xml_generator_append_prop(self->sesion->pxml, provider, props_table) ;
					MA_LOG(logger, MA_LOG_SEV_DEBUG, "PropertyService, properties received from %s (unregistered provider)", provider) ;				
				}
			}
			else {
				MA_LOG(logger, MA_LOG_SEV_ERROR, "Property received with invalid session, saving it for further use.") ;
				(void)ma_ds_set_variant(ds_handle, MA_PROPERTY_DATA_PRODUCT_PROPERTIES_PATH_STR, provider, pp_msg_payload) ;
				rc = MA_ERROR_PROPERTY_SESSION_NOT_VALID ;  //should we return error ???
			}			
		}
		else {
			MA_LOG(logger, MA_LOG_SEV_DEBUG, "Property recevied for %s, but session not in progress, saving it for further use.", provider) ;
			(void)ma_ds_set_variant(ds_handle, MA_PROPERTY_DATA_PRODUCT_PROPERTIES_PATH_STR, provider, pp_msg_payload) ;
		}
	}
	if(props_table)	(void)ma_table_release(props_table) ;
	props_table = NULL ;
	
	if(pp_msg_payload)	(void)ma_variant_release(pp_msg_payload) ;
	pp_msg_payload = NULL ;
	
    return MA_OK ;
}

static ma_error_t property_collect_failure_handler(ma_property_service_t *self, ma_message_t *msg) {
	ma_error_t rc = MA_OK ;        
	const char *session_id = NULL ;
    const char *provider = NULL ;

	(void)ma_message_get_property(msg, MA_PROPERTY_MSG_KEY_SESSION_ID_INT, &session_id) ;
    (void)ma_message_get_property(msg, MA_PROPERTY_MSG_KEY_PROVIDER_ID_STR, &provider) ;
	
	if(session_id && provider) {
		if(PROPERTY_SESSION_STATE_IN_PROGRESS == self->session_state) {
			if(!strcmp(self->sesion->session_id, session_id)) {
				ma_property_provider_info_t *pp_info = NULL;
				(void)ma_property_collect_session_get_provider(self->sesion, provider, &pp_info);	

				if(!pp_info->is_property_provided && check_if_product_property_already_reported(self, pp_info->provider_id)) {					
					if(!pp_info->is_last_props_collection_failed) {
						MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Generating property collection failure event for product %s due to prop collect failure", provider) ;
						(void) generate_property_collection_failure_event(self, pp_info->provider_id);
					}
					pp_info->is_last_props_collection_failed = MA_TRUE;
				}
			}			
		}		
	}
    return MA_OK ;
}
 
static ma_error_t ma_property_service_cb(ma_msgbus_server_t *server, ma_message_t *msg, void *cb_data, ma_msgbus_client_request_t *c_request) {
	if(server && msg && cb_data) {
		const char *msg_type = NULL ;
		ma_error_t rc = MA_OK ;
        ma_message_t *response = NULL ;
        ma_property_service_t *service = (ma_property_service_t *)cb_data ;
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(service->context) ;

		(void)ma_message_get_property(msg, MA_PROPERTY_MSG_KEY_TYPE_STR, &msg_type);
        (void)ma_message_create(&response) ;

		if(msg_type && !strcmp(msg_type, MA_PROPERTY_MSG_TYPE_COLLECT_STR)) {
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "Property collect request received.") ;
            (void)ma_message_add_ref(msg) ; /*incr ref as we are posting result before processing the messaage */
			rc = ma_msgbus_server_client_request_post_result(c_request, MA_OK, response ) ;
			rc = property_collect_handler((ma_property_service_t *)cb_data, msg) ;
            (void)ma_message_release(msg) ;
		}
		else if(msg_type && !strcmp(msg_type, MA_PROPERTY_MSG_TYPE_COLLECTED_STR)) {
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "Property collected request received.") ;
            (void)ma_message_add_ref(msg) ; /*incr ref as we are posting result before processing the messaage */
            rc = ma_msgbus_server_client_request_post_result(c_request, MA_OK, response ) ;            
			rc = property_collected_handler((ma_property_service_t *)cb_data, msg) ;
            (void)ma_message_release(msg) ;
		}
		else if(msg_type && !strcmp(msg_type, MA_PROPERTY_MSG_TYPE_COLLECT_FAILED_STR)) {
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "Property collect failure request received.") ;
            (void)ma_message_add_ref(msg) ; /*incr ref as we are posting result before processing the messaage */
            rc = ma_msgbus_server_client_request_post_result(c_request, MA_OK, response ) ;            
			rc = property_collect_failure_handler((ma_property_service_t *)cb_data, msg) ;
            (void)ma_message_release(msg) ;
		}
		else {
            MA_LOG(logger, MA_LOG_SEV_ERROR, "Invalid request received.") ;
            rc = ma_msgbus_server_client_request_post_result(c_request, MA_ERROR_UNEXPECTED, response ) ;            
			rc = MA_ERROR_UNEXPECTED;
		}
        (void)ma_message_release(response) ;
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_property_collect_session_create(ma_property_collect_session_t **property_collect_session) {
    if(property_collect_session) {
        ma_property_collect_session_t *session = (ma_property_collect_session_t *)calloc(1,sizeof(ma_property_collect_session_t));
        if(session) {
            MA_SLIST_INIT(session, providers) ;
            session->props_type = PROPS_TYPE_INCR ;
            *property_collect_session = session ;
            return MA_OK ;
        }
        return MA_ERROR_OUTOFMEMORY ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_property_collect_session_release(ma_property_collect_session_t *self) {
    if(self){
        MA_SLIST_CLEAR(self, providers, ma_property_provider_info_t, ma_property_provider_info_release) ;
        free(self) ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_property_collect_session_add_provider(ma_property_collect_session_t *self, const char *provider_id, ma_uint64_t clientflags) {
    if(self && provider_id) {
        ma_property_provider_info_t *pp_info = NULL ;
        ma_error_t rc = MA_OK ;
		if(strcmp(provider_id, MA_UPDATER_SOFTWAREID_STR)){ /*if it is not updater id.*/
			if(MA_OK == (rc = ma_property_collect_session_get_provider(self, provider_id, &pp_info))) {
				if(!pp_info) {
					if(MA_OK == (rc = ma_property_provider_info_create(provider_id, &pp_info))){
						MA_SLIST_PUSH_BACK(self, providers, pp_info) ;
						pp_info->service_required_flags = clientflags;
					}
				}
				else {
					pp_info->service_required_flags = clientflags;
					pp_info->mark_for_deletion = MA_FALSE;	
					pp_info->is_property_provided = MA_FALSE ;
				}
			}
		}
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_property_collect_session_remove_provider(ma_property_collect_session_t *self, const char *provider_id) {
    /* MA_SLIST_REMOVE_NODE */
    ma_property_provider_info_t *pp_info = NULL ;
    MA_SLIST_FOREACH(self, providers, pp_info) {
        if(0 == strcmp(pp_info->provider_id, provider_id)) {
            MA_SLIST_REMOVE_NODE(self, providers, pp_info, ma_property_provider_info_t) ;
            break ;
        }
    }
    return MA_OK ;
}

ma_error_t ma_property_collect_session_clear_providers(ma_property_collect_session_t *self) {
    if(self){
		if(MA_SLIST_GET_COUNT(self, providers) > 0) {
			ma_property_provider_info_t *pp_info = MA_SLIST_GET_HEAD(self, providers); 
			while(pp_info != NULL) {
				ma_property_provider_info_t *next_pp_info = MA_SLIST_GET_NEXT_NODE(pp_info);
				if(pp_info->mark_for_deletion) {
					MA_SLIST_REMOVE_NODE(self, providers, pp_info, ma_property_provider_info_t) ;
					(void)ma_property_provider_info_release(pp_info);
				}
				pp_info = next_pp_info;
			}
		}
	    return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_property_collect_session_get_provider(ma_property_collect_session_t *self, const char *provider_id, ma_property_provider_info_t **pp_info) {
    ma_property_provider_info_t *pp = NULL ;
    if(self && provider_id) {
        MA_SLIST_FOREACH(self, providers, pp) {
            if(0 == strcmp(pp->provider_id, provider_id)) {
				*pp_info = pp ;
                break ;
			}
        }
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_property_provider_info_create(const char *provider_id, ma_property_provider_info_t **pp_info) {
    if(provider_id && pp_info) {
        ma_property_provider_info_t *pp_info_tmp ;
        if(NULL != ( pp_info_tmp = (ma_property_provider_info_t *)calloc(1, sizeof(ma_property_provider_info_t)))) {
            pp_info_tmp->provider_id = strdup(provider_id) ;
            pp_info_tmp->is_property_provided = MA_FALSE ;
			pp_info_tmp->is_last_props_collection_failed = MA_FALSE;
            *pp_info = pp_info_tmp ;
            return MA_OK ;
        }
        return MA_ERROR_OUTOFMEMORY ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_property_provider_info_release(ma_property_provider_info_t *pp_info) {
    if(pp_info) {
        if(pp_info->provider_id) free(pp_info->provider_id) ;
        free(pp_info) ;
        pp_info = NULL ;
    }
    return MA_OK ;
}

ma_error_t ma_property_collect_message_create(ma_props_type_t props_type, ma_message_t **msg) {
    if(msg) {
        ma_message_t *msg_tmp = NULL ;
        ma_error_t rc = MA_OK ;

        if( MA_OK == (rc = ma_message_create(&msg_tmp))) {
            switch(props_type) {
                case PROPS_TYPE_FULL :
                    /* MA_LOG(props_logger, MA_LOG_SEV_DEBUG, "Created full props request message") ; */
					(void)ma_message_set_property(msg_tmp, MA_PROPERTY_MSG_KEY_PROPS_TYPE_STR, MA_PROPERTY_MSG_VALUE_PROPS_TYPE_FULL_STR) ;   //full props
                    break ;
                case PROPS_TYPE_INCR :
                    /* MA_LOG(props_logger, MA_LOG_SEV_DEBUG, "Created incr props request message") ; */
                    (void)ma_message_set_property(msg_tmp, MA_PROPERTY_MSG_KEY_PROPS_TYPE_STR, MA_PROPERTY_MSG_VALUE_PROPS_TYPE_INCR_STR) ;   //incremental props
                    break ;
                case PROPS_TYPE_VERSION :
                default :
                    /* MA_LOG(props_logger, MA_LOG_SEV_DEBUG, "Created props version request message") ; */
                    ma_message_set_property(msg_tmp,MA_PROPERTY_MSG_KEY_PROPS_TYPE_STR, MA_PROPERTY_MSG_VALUE_PROPS_TYPE_VERSION_STR) ;   //props version package
            }
			(void)ma_message_set_property(msg_tmp, MA_PROPERTY_MSG_KEY_TYPE_STR, MA_PROPERTY_MSG_TYPE_COLLECT_STR) ;
            //TBD : need any payload data ???
            *msg = msg_tmp ;
        }
        return rc ;    
    }
    /* MA_LOG(props_logger, MA_LOG_SEV_ERROR, "Invalid Argument") ; */
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_property_raise_alert(ma_property_service_t *self) {
    if(self) {		
        self->session_state = PROPERTY_SESSION_STATE_DATA_READY ;
        
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "Agent started performing ASCI") ;
		switch(self->sesion->props_type) {
			case PROPS_TYPE_INCR :
				return ma_property_raise_spipe_alert(self->ppm, "IncProps") ;
			case PROPS_TYPE_FULL :
				return ma_property_raise_spipe_alert(self->ppm, "FullProps") ;
			case PROPS_TYPE_VERSION :
			default :
				return ma_property_raise_spipe_alert(self->ppm, "PropsVersion") ;
		}
    }
    return MA_ERROR_INVALIDARG ;
}
