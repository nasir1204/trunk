#ifndef MA_PROPERTY_MANAGER_H_INCLUDED
#define MA_PROPERTY_MANAGER_H_INCLUDED

#include "ma_property_service_internal.h"
#include "ma/internal/services/ahclient/ma_spipe_decorator.h"
#include "ma/internal/services/ahclient/ma_spipe_handler.h"


MA_CPP(extern "C" {)

ma_error_t ma_property_manager_create(ma_property_service_t *service, ma_property_manager_t **property_manager) ;
ma_error_t ma_property_manager_release(ma_property_manager_t *property_manager) ;
ma_spipe_decorator_t * ma_property_manager_get_decorator(ma_property_manager_t *property_manager) ;
ma_spipe_handler_t * ma_property_manager_get_handler(ma_property_manager_t *property_manager) ;
ma_error_t ma_property_raise_spipe_alert(ma_property_manager_t *property_manager, const char *pkg_type) ;

MA_CPP(})

#endif  /* MA_PROPERTY_MANAGER_H_INCLUDED */

