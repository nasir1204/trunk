#include "ma_property_manager.h" 
#include "ma/internal/services/property/ma_property_service.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/utils/spipe/ma_spipe_package_defs.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/services/ahclient/ma_ah_client_service.h"
#include "ma/internal/defs/ma_property_defs.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/utils/time/ma_time.h"
#include "ma/internal/utils/platform/ma_system_property.h"
#include "ma/internal/defs/ma_info_defs.h"
#include "ma/info/ma_info.h"
#include "ma/internal/utils/configurator/ma_configurator_utils.h"

#include <stdlib.h>
#include <string.h>

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "property"

#define MA_CONTEXT_GET_DATASTORE(context) (ma_configurator_get_datastore(MA_CONTEXT_GET_CONFIGURATOR(context)))

struct ma_property_manager_s {
    ma_spipe_decorator_t        spipe_property_decorator ;
    ma_spipe_handler_t          spipe_property_handler ;
    ma_spipe_priority_t         spipe_property_priority ;
    void                        *data ;
} ;

static void property_manager_destroy(ma_property_manager_t *pm);

static ma_error_t property_collect_send_status(ma_property_service_t *ps, ma_bool_t status) ;

static ma_error_t spipe_decorate_property(ma_spipe_decorator_t *decorator, ma_spipe_priority_t priority, ma_spipe_package_t *spipe_pkg) {
    ma_property_manager_t *pm = (ma_property_manager_t *)decorator->data ;
    ma_property_service_t *ps = (ma_property_service_t *)pm->data ;

	ma_ds_t *ds_handle = MA_CONTEXT_GET_DATASTORE(ps->context) ;
	ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(ps->context) ;

    const char *value = NULL ;
    size_t val_size = 0 ;
    ma_error_t rc = MA_OK ;

    ma_spipe_package_get_info(spipe_pkg,  STR_PACKAGE_TYPE, (const unsigned char **)&value, &val_size) ;
    MA_LOG(logger, MA_LOG_SEV_DEBUG, "Spipe decorator for package type %s", value) ;

    if( (PROPERTY_SESSION_STATE_DATA_READY == ps->session_state || PROPERTY_SESSION_STATE_TIMEDOUT == ps->session_state)
        && (0 == strncmp(value, STR_PKGTYPE_AGENT_PUB_KEY, val_size) || 0 == strncmp(value, STR_PKGTYPE_INC_PROPS, val_size) 
            || 0 == strncmp(value, STR_PKGTYPE_FULL_PROPS, val_size) || 0 == strncmp(value, STR_PKGTYPE_PROPS_VERSION, val_size)
            || 0 == strncmp(value, STR_PKGTYPE_POLICY_MANIFEST_REQUEST, val_size))  )
    {
        char *props_data = NULL ;
        ma_buffer_t *buffer = NULL ;
        size_t props_data_size = 0;
		ma_bool_t is_first_asc = MA_FALSE;
		const char *log_message[3] = {
								"Agent is sending INC PROPS package to ePO server", 
								"Agent is sending FULL PROPS package to ePO server",
								"Agent is sending PROPS VERSION package to ePO server"};
        
		(void)ma_spipe_package_add_info(spipe_pkg, STR_PKGINFO_PROPS_VERSION, (unsigned char *)ps->sesion->props_version, strlen(ps->sesion->props_version)) ;

        if(MA_OK == (rc = ma_ds_get_str(ds_handle, MA_PROPERTY_DATA_PATH_NAME_STR, MA_PROPERTY_DATA_KEY_PROPERTY_VERSION_STR, &buffer))) {
			const char *prev_props_version = NULL ;
			size_t prev_props_version_size = 0 ;
            if(MA_OK == (rc = ma_buffer_get_string(buffer, &prev_props_version, &prev_props_version_size))) {
                rc = ma_spipe_package_add_info(spipe_pkg, STR_PKGINFO_PREV_PROPS_VERSION, (unsigned char *)prev_props_version, prev_props_version_size) ;
				if(0 == strcmp(prev_props_version, "0"))
					is_first_asc = MA_TRUE;
			}
            ma_buffer_release(buffer) ;
        }

        if(	   ((0 == strncmp(value, STR_PKGTYPE_PROPS_VERSION, val_size)) && is_first_asc)
			|| (0 == strncmp(value, STR_PKGTYPE_AGENT_PUB_KEY, val_size))
			|| (0 == strncmp(value, STR_PKGTYPE_FULL_PROPS, val_size))) {
            rc = ma_property_xml_generator_get_full_props(ps->sesion->pxml, (unsigned char **)&props_data,&props_data_size) ;
			MA_LOG(logger, MA_LOG_SEV_DEBUG, "Agent is attaching full props with package");
        }
        else {
            rc = ma_property_xml_generator_get_incremental_props(ps->sesion->pxml, (unsigned char **)&props_data, &props_data_size) ;
			MA_LOG(logger, MA_LOG_SEV_DEBUG, "Agent is attaching incremental props with package");
        }

		MA_LOG(logger, MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, log_message[ps->sesion->props_type]);
        
        if(MA_OK == rc) {
            if(MA_OK == (rc = ma_spipe_package_add_data(spipe_pkg, STR_MANAGEMENT_PROPS_SEND, props_data, props_data_size))) {
				MA_LOG(logger, MA_LOG_SEV_DEBUG, "Spipe decorator added props.xml") ;
				ps->session_state = PROPERTY_SESSION_STATE_SENT ;
			}
        }
        else
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "Failed to generate props xml (%d)", rc ) ;

		free(props_data) ;	props_data = NULL ;
    }
	else if(0 == strncmp(value, STR_PKGTYPE_AGENT_PUB_KEY, val_size)) {
		ma_time_t time = {0, 0, 0, 0, 0, 0, 0 }; 
		char props_version[25] = {0} ;
		const ma_misc_system_property_t *misc_sys_prop = ma_system_property_misc_get(MA_CONTEXT_GET_SYSTEM_PROPERTY(ps->context), MA_FALSE) ;
		ma_buffer_t *buffer = NULL, *guid_buffer = NULL, *tenantid_buffer = NULL ;
		const char *prev_props_version = NULL, *guid = NULL, *tenantid = NULL ;
		size_t buf_size = 0 ;
		ma_property_xml_generator_t *prop_xml_gen = NULL ;
		char *props_data = NULL ;

		ma_time_get_localtime(&time) ;

		MA_LOG(logger, MA_LOG_SEV_DEBUG, "Agent Pub Key request, no active Property collect session, will attach last collected props" ) ;

		MA_MSC_SELECT(_snprintf, snprintf)(props_version, 24, "%04d%02d%02d%02d%02d%02d", time.year, time.month, time.day, time.hour, time.minute, time.second) ;

		ma_spipe_package_add_info(spipe_pkg, STR_PKGINFO_PROPS_VERSION, (unsigned char *)props_version, strlen(props_version)) ;
		
		if(MA_OK == (rc = ma_ds_get_str(ds_handle, MA_PROPERTY_DATA_PATH_NAME_STR, MA_PROPERTY_DATA_KEY_PROPERTY_VERSION_STR, &buffer))) {
            if(MA_OK == (rc = ma_buffer_get_string(buffer, &prev_props_version, &buf_size)))
                rc = ma_spipe_package_add_info(spipe_pkg, STR_PKGINFO_PREV_PROPS_VERSION, (unsigned char *)prev_props_version, buf_size) ;
            ma_buffer_release(buffer) ; buffer = NULL ;
        }

		if(MA_OK == ma_ds_get_str(ds_handle, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_GUID_STR, &guid_buffer)) {
			ma_buffer_get_string(guid_buffer, &guid, &buf_size) ;
		}

		if(MA_OK == ma_ds_get_str(ds_handle, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_TENANT_ID_STR, &tenantid_buffer)) {
			ma_buffer_get_string(tenantid_buffer, &tenantid, &buf_size) ;
		}

		if(MA_OK == (rc = ma_property_xml_generator_create(&prop_xml_gen, ds_handle, MA_PROPERTY_DATA_PATH_NAME_STR, misc_sys_prop->computername, guid, tenantid, props_version))) {
			ma_property_xml_generator_set_logger(prop_xml_gen, logger);
			if(MA_OK == (rc = ma_property_xml_generator_get_last_full_props(prop_xml_gen, (unsigned char **)&props_data, &buf_size))) {
				(void)ma_spipe_package_add_data(spipe_pkg, STR_MANAGEMENT_PROPS_SEND, props_data, buf_size) ;
				MA_LOG(logger, MA_LOG_SEV_DEBUG, "Spipe decorator added props.xml") ;
				free(props_data) ;	props_data = NULL ;
			}
			(void)ma_property_xml_generator_release(prop_xml_gen) ;	prop_xml_gen = NULL ;
		}
		else
			MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to create props xml generator(%d)", rc ) ;

         if(guid_buffer) (void)ma_buffer_release(guid_buffer);
	     if(tenantid_buffer) (void)ma_buffer_release(tenantid_buffer) ;	
	}
    else {
        /* get the version from datastore */
        ma_buffer_t *buffer = NULL ;
        const char *props_version = NULL ;
        size_t props_version_size = 0 ;
        MA_LOG(logger, MA_LOG_SEV_DEBUG, "Spipe decorator will not decorate for package type %s and only adds propsversion", value) ;
        if(MA_OK == (rc = ma_ds_get_str(ds_handle, MA_PROPERTY_DATA_PATH_NAME_STR, MA_PROPERTY_DATA_KEY_PROPERTY_VERSION_STR, &buffer))) {
            if(MA_OK == (rc = ma_buffer_get_string(buffer, &props_version, &props_version_size)))
                rc = ma_spipe_package_add_info(spipe_pkg, STR_PKGTYPE_PROPS_VERSION, (const unsigned char *)props_version, props_version_size) ;
            ma_buffer_release(buffer) ;
        }
    }
    return rc ;
}

static ma_error_t get_spipe_property_priority(ma_spipe_decorator_t *decorator, ma_spipe_priority_t *priority) {
    *priority =  MA_SPIPE_PRIORITY_IMMEDIATE ;
    return MA_OK ;
}

static ma_error_t property_decorator_destroy(ma_spipe_decorator_t *decorator) {
    ma_property_manager_t *pm = (ma_property_manager_t *)decorator->data;
    if (0 == pm->spipe_property_handler.ref_count) {
        property_manager_destroy(pm);
    }
    return MA_OK ;
}

static ma_spipe_decorator_methods_t spipe_property_decorator_methods = {    
    &spipe_decorate_property,
    &get_spipe_property_priority,
    &property_decorator_destroy
} ;

static ma_error_t regnerate_guid(ma_property_service_t *ps) {
	char guid[UUID_LEN_TXT] = {0};
	ma_error_t rc = MA_OK;
	const ma_network_system_property_t *net = NULL;
	ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(ps->context) ;

	if(NULL != (net = ma_system_property_network_get(MA_CONTEXT_GET_SYSTEM_PROPERTY(ps->context), MA_FALSE))) {
		rc = ma_uuid_generate(1, (unsigned char*)net->mac_address, guid);
	} 
	else {
		MA_LOG(logger, MA_LOG_SEV_TRACE, "generating guid without mac address");
		rc = ma_uuid_generate(0, NULL, guid);
	}

	if(MA_OK == rc) {
		ma_ds_t *ds = NULL;
		ma_configurator_t *configurator = MA_CONTEXT_GET_CONFIGURATOR(ps->context);
		rc = MA_ERROR_UNEXPECTED;
		if(configurator) {
			if(NULL != (ds = ma_configurator_get_datastore(configurator))) {
				rc = ma_ds_set_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_GUID_STR, guid, -1);
				/* Ignore return code here as there is no impact with reset sequence number failure */
				(void) ma_ds_set_int(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_SPIPE_SEQUENCE_NUMBER_STR, 1);
				(void)ma_configurator_set_guid(configurator, guid);
			}
		}
	}
	return rc;
}

static ma_error_t spipe_property_handler(ma_spipe_handler_t *handler, const ma_spipe_response_t *spipe_response) {
    if(handler && spipe_response) {
        ma_property_manager_t *pm = (ma_property_manager_t *)handler->data ;
		ma_property_service_t *ps = (ma_property_service_t *)pm->data ;
        ma_error_t rc = MA_OK ;

		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(ps->context) ;

        if(spipe_response->net_service_error_code == MA_ERROR_NETWORK_REQUEST_SUCCEEDED && (spipe_response->http_response_code == 200 || spipe_response->http_response_code == 202 || spipe_response->http_response_code == 506 ))
        {
			if(PROPERTY_SESSION_STATE_SENT == ps->session_state) {
				MA_LOG(logger, MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "Package uploaded to ePO Server successfully") ;
			}
            if(spipe_response->respond_spipe_pkg) 
            {
                ma_spipe_package_t *response_pkg = spipe_response->respond_spipe_pkg ;
                const char *value = NULL ;
                size_t val_size = 0 ;
        
                ma_spipe_package_get_info(response_pkg,  STR_PACKAGE_TYPE, (const unsigned char **)&value, &val_size) ;
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "Spipe handler with package type %s", value) ;
                /* if props request from server */
                if(0 == strncmp(value, STR_PKGTYPE_REQUEST_PROPS, val_size)) 
                {
                    ma_props_type_t props_type = PROPS_TYPE_INCR ;
                    ma_message_t *pcm = NULL ;

					MA_LOG(logger, MA_LOG_SEV_INFO|MA_LOG_FLAG_LOCALIZE, "Agent received REQUEST PROPS package from ePO server") ;

					ma_spipe_package_get_info(response_pkg,  STR_PKGTYPE_FULL_PROPS, (unsigned char **)&value, &val_size) ;
                    if(0 == strncmp("1", value , 1))    props_type = PROPS_TYPE_FULL ;
                    if(PROPERTY_SESSION_STATE_INITIAL == ps->session_state ) {
                        ma_msgbus_endpoint_t *ep = NULL ;
                        if(MA_OK == (rc = ma_property_collect_message_create(props_type, &pcm))) {
                            if(MA_OK == (rc = ma_msgbus_endpoint_create(MA_CONTEXT_GET_MSGBUS(ps->context), MA_PROPERTY_SERVICE_NAME_STR, NULL, &ep))) {
								(void)ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
								(void)ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_INPROC);						
								
                                rc = ma_msgbus_send_and_forget(ep, pcm) ;
                                ma_msgbus_endpoint_release(ep) ;
                            }
                            ma_message_release(pcm) ;
                        }
                    }
                    else {
                        ps->sesion->props_type = props_type ;
						ps->session_state = PROPERTY_SESSION_STATE_DATA_READY;
						MA_LOG(logger, MA_LOG_SEV_DEBUG, "server is asking for full properties");
						if(MA_OK != ma_ah_client_service_raise_spipe_alert(MA_CONTEXT_GET_AH_CLIENT(ps->context), "FullProps"))
							property_collect_send_status(ps, MA_FALSE) ;
					}
                }
                else if(0 == strncmp(value, STR_PKGTYPE_PROPS_RESPONSE, val_size)) { /* props response */
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "Spipe handler received PropsResponse, closing property collection session") ;
                    rc = property_collect_send_status(ps, MA_TRUE) ;
                }
			    else if(0 == strncmp(value, STR_PKGTYPE_REQUEST_PUB_KEY, val_size)) {  /* Public key request */
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "Spipe handler received Public key request, check for props data to be attached") ;
				    if(PROPERTY_SESSION_STATE_SENT == ps->session_state) {
					    ps->session_state = PROPERTY_SESSION_STATE_DATA_READY ;
                    }
			    }
                else { /* response not interested by property handler */
                    if(PROPERTY_SESSION_STATE_SENT == ps->session_state) {
                         MA_LOG(logger, MA_LOG_SEV_DEBUG, "Spipe handler closing property collection session") ;
                        rc = property_collect_send_status(ps, MA_TRUE) ;
                    }
                }
            }
            else {  /* no response package but http code is success */  
				/* 506 means regenerate GUID again */
				if(506 == spipe_response->http_response_code) {
					if(MA_OK == (rc = regnerate_guid(ps))) {
						MA_LOG(logger, MA_LOG_SEV_DEBUG, "GUID was moved to duplicate list. So regenerating GUID") ;
						ps->session_state = PROPERTY_SESSION_STATE_DATA_READY ;
						if(MA_OK != ma_ah_client_service_raise_spipe_alert(MA_CONTEXT_GET_AH_CLIENT(ps->context), "IncProps"))
							property_collect_send_status(ps, MA_FALSE) ;
					}
					else {
						MA_LOG(logger, MA_LOG_SEV_ERROR, "GUID was moved to duplicate list. And failed to regenerate GUID") ;
						property_collect_send_status(ps, MA_FALSE) ;
					}
				}
				else {
					if(PROPERTY_SESSION_STATE_SENT == ps->session_state) {
						MA_LOG(logger, MA_LOG_SEV_DEBUG, "Spipe handler, No response package, assuming props send success") ;
						rc = property_collect_send_status(ps, MA_TRUE) ;
					}
				}
            }
            return rc ;
        }
        else { /* error, if props data is sent, send status else ignore  */
            if(PROPERTY_SESSION_STATE_SENT == ps->session_state) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "Spipe handler, props send failure(response %d)", spipe_response->http_response_code) ;
                rc = property_collect_send_status(ps, MA_FALSE) ;
            }
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

static ma_error_t spipe_property_handler_destroy(ma_spipe_handler_t *handler) {
    ma_property_manager_t *pm = (ma_property_manager_t *)handler->data;
    if (0 == pm->spipe_property_decorator.ref_count) {
        property_manager_destroy(pm);
    }
    return MA_OK ;
}

static ma_spipe_handler_methods_t spipe_property_handler_methods = {    
    &spipe_property_handler,
    &spipe_property_handler_destroy
};

static void property_manager_destroy(ma_property_manager_t *pm) {
    free(pm);
}

ma_error_t ma_property_manager_create(ma_property_service_t *service, ma_property_manager_t **property_manager) {
    if(service && property_manager) {
        ma_property_manager_t *self = (ma_property_manager_t *)calloc(1, sizeof(ma_property_manager_t)) ;
        if(self){
            self->spipe_property_priority = MA_SPIPE_PRIORITY_IMMEDIATE ;
            ma_spipe_decorator_init(&self->spipe_property_decorator, &spipe_property_decorator_methods, self);
            ma_spipe_handler_init(&self->spipe_property_handler, &spipe_property_handler_methods, self);
            /* ref count will be 1 + 1 now  */
            self->data = service ;
			*property_manager = self;
            return MA_OK ;
        }
        return MA_ERROR_OUTOFMEMORY ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_property_manager_release(ma_property_manager_t *pm) {
    if (!pm) return MA_ERROR_INVALIDARG;
    /* use the contained handler/decorator for ref counting */
    ma_spipe_handler_release(&pm->spipe_property_handler);
    ma_spipe_decorator_release(&pm->spipe_property_decorator);
    return MA_OK ;
}

ma_spipe_decorator_t * ma_property_manager_get_decorator(ma_property_manager_t *property_manager) {
    return (property_manager)?&property_manager->spipe_property_decorator:NULL ;
}

ma_spipe_handler_t * ma_property_manager_get_handler(ma_property_manager_t *property_manager) {
    return (property_manager)?&property_manager->spipe_property_handler:NULL ;
}

ma_error_t ma_property_raise_spipe_alert(ma_property_manager_t *self, const char *pkg_type) {
    if(self && pkg_type) {
        return ma_ah_client_service_raise_spipe_alert(MA_CONTEXT_GET_AH_CLIENT(((ma_property_service_t *)self->data)->context), pkg_type) ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t property_collect_send_status(ma_property_service_t *ps, ma_bool_t status) {
    ma_error_t rc = MA_OK ;
	ma_message_t *msg = NULL ;

	ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(ps->context) ;

    ps->session_state = PROPERTY_SESSION_STATE_INITIAL ;

	if(status) {
		(void)ma_ds_set_str(MA_CONTEXT_GET_DATASTORE(ps->context), MA_PROPERTY_DATA_PATH_NAME_STR, MA_PROPERTY_DATA_KEY_PROPERTY_VERSION_STR, ps->sesion->props_version, strlen(ps->sesion->props_version)) ;

		if(ps->sesion->pxml) {
			if(MA_OK != (rc = ma_property_xml_generator_save_props_to_datastore(ps->sesion->pxml)))
				MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to add sent properties to DS, rc = %d", rc) ;
		}

		if(MA_OK == (rc = ma_message_create(&msg))) {
			ma_msgbus_endpoint_t *ep = NULL ;

			(void)ma_message_set_property(msg, MA_PROP_KEY_REQUEST_TYPE_STR, MA_PROP_VALUE_AGENT_INFO_COLLECT) ;
			(void)ma_message_set_property(msg, MA_INFO_MSG_KEY_EVENT_TYPE_STR, MA_INFO_EVENT_PROPERTY_SENT_STR) ;

			if(MA_OK == (rc = ma_msgbus_endpoint_create(MA_CONTEXT_GET_MSGBUS(ps->context), MA_IO_SERVICE_NAME_STR, NULL, &ep))) {
				(void)ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
			    (void)ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_INPROC);						
				
				if(MA_OK != (rc = ma_msgbus_send_and_forget(ep, msg)))
					MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to send ma info message, rc = <%d>.", rc) ;

				(void)ma_msgbus_endpoint_release(ep) ;	ep = NULL ;
			}
			(void)ma_message_release(msg) ; msg = NULL ;
		}
	}

	if(MA_OK == (rc = ma_message_create(&msg))) {
		(void)ma_message_set_property(msg, MA_PROPERTY_MSG_KEY_TYPE_STR, MA_PROPERTY_MSG_TYPE_COLLECT_SEND_STATUS_STR) ;
			
		status? (void)ma_message_set_property(msg, MA_PROPERTY_MSG_KEY_COLLECT_STATUS_INT, "1") : (void)ma_message_set_property(msg, MA_PROPERTY_MSG_KEY_COLLECT_STATUS_INT, "0") ;
        if(ps->sesion->enforce_policy) (void)ma_message_set_property(msg, MA_PROPERTY_MSG_KEY_ENFORCE_POLICY_STR, "1") ;
            
		if(MA_OK == (rc = ma_msgbus_publish(MA_CONTEXT_GET_MSGBUS(ps->context), MA_PROPERTY_PUBSUB_TOPIC_COLLECT_STATUS_STR, MSGBUS_CONSUMER_REACH_INPROC, msg)))
			MA_LOG(logger, MA_LOG_SEV_INFO, "Published property collect and send status message") ;

		(void)ma_message_release(msg) ;
	}

	(void)ma_property_xml_generator_release(ps->sesion->pxml) ;   ps->sesion->pxml = NULL ;

	return rc ;
}
