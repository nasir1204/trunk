#ifndef MA_PROPERTY_SERVICE_INTERNAL_H_INCLUDED
#define MA_PROPERTY_SERVICE_INTERNAL_H_INCLUDED

#include "ma/internal/services/ma_service.h"
#include "ma/internal/utils/datastructures/ma_slist.h"
#include "ma/internal/utils/app_info/ma_registry_store.h"
#include "ma/internal/services/property/property_xml_generator.h"
#include "ma/internal/services/property/ma_property_service.h"

#include "ma/ma_log.h"

#include "uv.h"
MA_CPP(extern "C" {)

typedef struct ma_property_manager_s ma_property_manager_t ;

#define PROPERTY_PROVIDER_WAIT_TIMEOUT    500     //miliseconds

typedef struct ma_property_provider_info_s ma_property_provider_info_t;
struct ma_property_provider_info_s {
	/*next link*/
	MA_SLIST_NODE_DEFINE(ma_property_provider_info_t);

	/*id of the provider */
	char		*provider_id ;

	/*property for this provider is collected or not*/
	unsigned short int 	is_property_provided;

	ma_uint64_t			service_required_flags;

	ma_bool_t			mark_for_deletion;

	ma_bool_t			is_last_props_collection_failed;
};

typedef struct ma_property_collect_session_s ma_property_collect_session_t ;
struct ma_property_collect_session_s {
	/* list of the providers for this session */
	MA_SLIST_DEFINE(providers,ma_property_provider_info_t);

	/*session id for this session*/
	char session_id[5];//4 bytes random number

    //session timer
    uv_timer_t session_timer ;
    char props_version[18] ;
    ma_props_type_t props_type ;        /* INCR or FULL props */
    ma_property_xml_generator_t *pxml ;     /* property xml generator */

    /* force enforce policy for this session */
    ma_bool_t   enforce_policy ;
};

typedef enum ma_property_session_state_e {
    PROPERTY_SESSION_STATE_INITIAL = 0,
    PROPERTY_SESSION_STATE_IN_PROGRESS,
    PROPERTY_SESSION_STATE_TIMEDOUT,
    PROPERTY_SESSION_STATE_COMPLETED,
    PROPERTY_SESSION_STATE_DATA_READY,
    PROPERTY_SESSION_STATE_SENT
}ma_property_session_state_t ;

//TODO - do I need to make logger and loop reference increment and decrement
struct ma_property_service_s {
    ma_service_t base ;     /*base service object */

     ma_msgbus_server_t *server ;    //message bus server
    
    /* is session is already in progress*/
    ma_property_session_state_t session_state;
    
    /*property collection session*/
    ma_property_collect_session_t *sesion;
    
    ma_property_manager_t *ppm ; /* property manager holds spipe decorator and handler */

	ma_registry_store_t   *app_store;

    ma_context_t *context ;

    uv_timer_t *timer;

    void *reserved;
};

#define PROPRS_DS_ROOT_PATH         "AGENT\\SERVICES\\PROPETRY\\EPO"
#define PROPRS_VER_DS_PATH          "AGENT\\SERVICES\\PROPETRY\\EPO"
#define PROPRS_VER_DS_KEY           "AGENT\\SERVICES\\PROPETRY\\PROPERTIES\\VERSION"
#define PROPRS_PROVIDERS_DS_PATH    "AGENT\\SERVICES\\PROPETRY\\PROVIDERS"
#define PROPRS_SYSTEM_DS_PATH       "AGENT\\SERVICES\\PROPETRY\\SYTEM_PROPS"

MA_CPP(})

#endif  /* MA_PROPERTY_SERVICE_INTERNAL_H_INCLUDED */

