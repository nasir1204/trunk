#include "ma/sensor/ma_sensor.h"
#include "ma/internal/defs/ma_sensor_defs.h"
#include "ma/ma_log.h"
#include "ma/sensor/ma_system_sensor.h"
#include "ma/internal/ma_macros.h"
#include "ma_system_sensor_internal.h"
#include "ma/sensor/ma_sensor_msg.h"
#include "ma/sensor/ma_sensor_utils.h"
#include "ma/internal/utils/time/ma_time.h"


#include <stdlib.h>
#include <string.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "sensor"

extern ma_logger_t *sensor_logger;
extern int g_repeater_interval;

static ma_error_t detect_system_time_change(ma_sensor_t *sensor);
static void ma_advance_time_by_seconds(ma_int64_t seconds, ma_time_t tdate, ma_time_t *new_date);
static ma_error_t prepare_and_post_system_sensor_msg(ma_system_sensor_t *sys_sensor, ma_sensor_event_type_t event);

static const ma_sensor_methods_t system_methods = {
    &system_sensor_on_msg,
    &system_sensor_get_type,
    &system_sensor_get_id,
    &system_sensor_start,
    &system_sensor_stop,
    &system_sensor_release,
    &system_sensor_detect
};


ma_error_t ma_system_sensor_create(ma_msgbus_t *msgbus, ma_system_sensor_t **sensor) {
    if(msgbus && sensor) {
        ma_system_sensor_t *system = NULL;
        ma_error_t err = MA_ERROR_OUTOFMEMORY;

        if((system = (ma_system_sensor_t*)calloc(1, sizeof(ma_system_sensor_t)))) {
            if(MA_OK == (err = ma_sensor_inc_ref((ma_sensor_t*)(*sensor = system)))) {
                ma_sensor_init((ma_sensor_t*)*sensor, &system_methods, system);
                strncpy(system->name, MA_SENSOR_TYPE_SYSTEM_STR, strlen(MA_SENSOR_TYPE_SYSTEM_STR));
                system->type = MA_SENSOR_TYPE_SYSTEM;
                system->state = MA_SENSOR_STATE_NOT_STARTED;
                system->msgbus = msgbus;
                MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) created successfully", system->name);
            } else {
                MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) creation failed, last error(%d)", system->name, err);
                free(system); 
                *sensor = NULL;
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_system_sensor_release(ma_system_sensor_t *sensor) {
    if(sensor) {
        ma_error_t err = MA_OK;

        if(!((ma_sensor_t*)sensor)->ref_count) {
            MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) releasing...", sensor->name);
            free(sensor);
        } else {
            if(MA_OK != (err = ma_sensor_dec_ref((ma_sensor_t*)sensor)))
                MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) dec ref failed, last error(%d)", ma_sensor_type_to_str(MA_SENSOR_TYPE_SYSTEM), err);
        }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}



ma_error_t system_sensor_on_msg(ma_sensor_t *sensor, ma_sensor_msg_t *msg) {
    if(sensor && msg) {
        ma_error_t err = MA_OK;
        ma_system_sensor_t *sys_sensor = (ma_system_sensor_t*)sensor;

        if(MA_SENSOR_STATE_RUNNING == sys_sensor->state) {
            ma_system_sensor_msg_t *sys_msg = NULL;
            ma_sensor_event_type_t event_type = MA_SYSTEM_SENSOR_EVENT_STOP;
            ma_message_t *pub_msg = NULL;

            if(MA_OK == (err = ma_sensor_msg_get_system_sensor_msg(msg, &sys_msg))) {
                MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) received message", sys_sensor->name);
                (void)ma_system_sensor_msg_get_event_type(sys_msg, &event_type);
                if(MA_OK == (err = ma_message_create(&pub_msg))) {
                    if(MA_OK != (err = ma_sensor_publish_msg(sys_sensor->msgbus, sys_sensor->name, ma_sensor_event_type_to_str(event_type), pub_msg))) {
                        MA_LOG(sensor_logger, MA_LOG_SEV_ERROR, "sensor(%s) publishing event(%s) failed, last error(%d)", sys_sensor->name, ma_sensor_event_type_to_str(event_type), err);
                    }
                    (void)ma_message_release(pub_msg);
                }
                if(MA_SYSTEM_SENSOR_EVENT_STARTUP == event_type) {
                    /* check for system startup */
                    if(detect_system_startup((ma_sensor_t*)sys_sensor)) {
                        if(MA_OK != (err = prepare_and_post_system_sensor_msg(sys_sensor, MA_SYSTEM_SENSOR_EVENT_SYSTEM_STARTUP)))
                            MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) publishing event(%s) failed, last error(%d)", sys_sensor->name, ma_sensor_event_type_to_str(MA_SYSTEM_SENSOR_EVENT_SYSTEM_STARTUP), err);
                    } else 
                        MA_LOG(sensor_logger, MA_LOG_SEV_NOTICE, "sensor(%s) failed to detect system startup event", sys_sensor->name);
                }
                (void)ma_system_sensor_msg_release(sys_msg);
            }
        } else
            MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) is stopped", sys_sensor->name);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t system_sensor_get_type(ma_sensor_t *sensor, ma_sensor_type_t *type) {
    if(sensor && type) {
        ma_system_sensor_t *sys_sensor = (ma_system_sensor_t*)sensor;

        *type = sys_sensor->type;

        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t system_sensor_get_id(ma_sensor_t *sensor, const char **id) {
    if(sensor && id) {
        ma_system_sensor_t *sys_sensor = (ma_system_sensor_t*)sensor;

        *id = sys_sensor->name;

        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t system_sensor_start(ma_sensor_t *sensor) {
    if(sensor) {
        ma_system_sensor_t *sys_sensor = (ma_system_sensor_t*)sensor;

        if((MA_SENSOR_STATE_STOPPED == sys_sensor->state) || (MA_SENSOR_STATE_NOT_STARTED == sys_sensor->state)){
            sys_sensor->state = MA_SENSOR_STATE_RUNNING;
        }

        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t system_sensor_stop(ma_sensor_t *sensor) {
    if(sensor) {
        ma_system_sensor_t *sys_sensor = (ma_system_sensor_t*)sensor;

        if(MA_SENSOR_STATE_RUNNING == sys_sensor->state) {
            sys_sensor->state = MA_SENSOR_STATE_STOPPED;
        }

        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t system_sensor_release(ma_sensor_t *sensor) {
    if(sensor) {
        ma_error_t err = MA_OK;
        ma_system_sensor_t *sys_sensor = (ma_system_sensor_t*)sensor;
                
        if(MA_SENSOR_STATE_RUNNING == sys_sensor->state) {
            err = ma_sensor_stop(sensor);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t system_sensor_detect(ma_sensor_t *sensor) {
    if(sensor) {
        ma_error_t err = MA_OK;
        ma_system_sensor_t *sys_sensor = (ma_system_sensor_t*)sensor;

        if(MA_OK != (err = detect_system_time_change(sensor))) {
            MA_LOG(sensor_logger, MA_LOG_SEV_ERROR, "sensor(%s) failed to detect system time change", sys_sensor->name);
        }
        
        /* adjust repeater */
        g_repeater_interval = (g_repeater_interval > MA_SYSTEM_SENSOR_REPEATER_INTERVAL) 
                                ? MA_SYSTEM_SENSOR_REPEATER_INTERVAL 
                                : g_repeater_interval;

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

void ma_advance_time_by_seconds(ma_int64_t seconds, ma_time_t tdate, ma_time_t *new_date) {
    if(new_date) {
        memset(new_date, 0, sizeof(ma_time_t));
        if(seconds) {
            struct tm task_time = {0};
            time_t tt = 0;
            struct tm *next_date = NULL;

            task_time.tm_hour = tdate.hour;
            task_time.tm_year = tdate.year-1900;
            task_time.tm_mon = tdate.month-1;
            task_time.tm_mday = tdate.day;
            task_time.tm_min = tdate.minute;
            task_time.tm_sec = tdate.second;
            task_time.tm_isdst = -1;
            tt = mktime(&task_time) + seconds;

            if(tt && (next_date = localtime(&tt))) {
                new_date->year = next_date->tm_year+1900;
                new_date->month = next_date->tm_mon + 1;
                new_date->day = next_date->tm_mday;
                new_date->minute = next_date->tm_min;
                new_date->hour = next_date->tm_hour;
                new_date->second = next_date->tm_sec;
            }
        }
    }
}

ma_error_t prepare_and_post_system_sensor_msg(ma_system_sensor_t *sys_sensor, ma_sensor_event_type_t event) {
    if(sys_sensor) {
        ma_system_sensor_msg_t *sys_msg = NULL;
        ma_error_t err = MA_OK;

        if(MA_OK == (err = ma_system_sensor_msg_create(&sys_msg, 1))) {
            if(MA_OK == (err = ma_system_sensor_msg_set_event_type(sys_msg, event))) {
                ma_sensor_msg_t *msg = NULL;

                if(MA_OK == (err = ma_sensor_msg_create_from_system_sensor_msg(sys_msg, &msg))) {
                    MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) publishing(%s)", sys_sensor->name, ma_sensor_event_type_to_str(event));
                    if(MA_OK != (err = ma_sensor_on_msg((ma_sensor_t*)sys_sensor, msg))) {
                        MA_LOG(sensor_logger, MA_LOG_SEV_ERROR, "sensor(%s) publishing idle time message failed, last error(%d)", sys_sensor->name, err);
                    }
                    (void)ma_sensor_msg_release(msg);
                }
            }
            (void)ma_system_sensor_msg_release(sys_msg);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t detect_system_time_change(ma_sensor_t *sensor) {
    if(sensor) {
        ma_system_sensor_t *sys_sensor = (ma_system_sensor_t*)sensor;
        ma_error_t err = MA_OK;
        static ma_time_t time_prev = {0};
        static ma_bool_t once = MA_FALSE;
        ma_time_t time_curr = {0};

        ma_time_get_localtime(&time_curr);
        if(!once) {
            memcpy(&time_prev, &time_curr, sizeof(ma_time_t));
            once = MA_TRUE;
        } else {
            ma_time_t elapse_time_aftr = {0};
            ma_time_t elapse_time_befr = {0};

            memcpy(&elapse_time_aftr, &time_prev, sizeof(ma_time_t));
            memcpy(&elapse_time_befr, &time_prev, sizeof(ma_time_t));
            ma_advance_time_by_seconds(g_repeater_interval + MA_SYSTEM_TIME_CHANGE_MAX_THRESHOLD, elapse_time_aftr, &elapse_time_aftr);
            ma_advance_time_by_seconds(-(g_repeater_interval + MA_SYSTEM_TIME_CHANGE_MIN_THRESHOLD), elapse_time_befr, &elapse_time_befr);
            //MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "current time(%d/%d/%d %d:%d:%d:%d)", time_curr.year, time_curr.month, time_curr.day, time_curr.hour, time_curr.minute, time_curr.second, time_curr.milliseconds);
            //MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "elapse before time(%d/%d/%d %d:%d:%d:%d)", elapse_time_befr.year, elapse_time_befr.month, elapse_time_befr.day, elapse_time_befr.hour, elapse_time_befr.minute, elapse_time_befr.second, elapse_time_befr.milliseconds);
            //MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "elapse after time(%d/%d/%d %d:%d:%d:%d)", elapse_time_aftr.year, elapse_time_aftr.month, elapse_time_aftr.day, elapse_time_aftr.hour, elapse_time_aftr.minute, elapse_time_aftr.second, elapse_time_aftr.milliseconds);
            if((memcmp(&time_curr, &elapse_time_aftr, sizeof(ma_time_t)) > 0) || (memcmp(&time_curr, &elapse_time_befr, sizeof(ma_time_t)) < 0)) {
                /* publish system time change topic */
                if(MA_OK != (err = prepare_and_post_system_sensor_msg(sys_sensor, MA_SYSTEM_SENSOR_TIME_CHANGE_EVENT)))
                    MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) publishing event(%s) failed, last error(%d)", sys_sensor->name, ma_sensor_event_type_to_str(MA_SYSTEM_SENSOR_TIME_CHANGE_EVENT), err);
            }
            memcpy(&time_prev, &time_curr, sizeof(ma_time_t));
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}


