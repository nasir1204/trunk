#include "ma/sensor/ma_sensor_utils.h"
#include "ma/internal/defs/ma_sensor_defs.h"
#include "ma/ma_log.h"
#include "ma/sensor/ma_sensor_msg.h"
#include "ma/internal/services/sensor/ma_sensor_msg_defs.h"
#include "ma/internal/ma_macros.h"

#include <stdlib.h>
#include <string.h>


#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "sensor"

extern ma_logger_t *sensor_logger;

ma_error_t ma_sensor_publish_msg(ma_msgbus_t *msgbus, const char *sensor, const char *topic, ma_message_t *msg) {
    return(msgbus && sensor && topic && msg)?ma_msgbus_publish(msgbus, topic, MSGBUS_CONSUMER_REACH_OUTPROC, msg):MA_ERROR_INVALIDARG;
}

ma_error_t convert_variant_to_user_sensor_msg(ma_variant_t *variant, ma_user_sensor_msg_t **msg) {
    if(variant && msg) {
        ma_error_t err = MA_OK;
        ma_table_t *msg_table = NULL;

        if(MA_OK == (err = ma_variant_get_table(variant, &msg_table))) {
            size_t size = 0;
            {
                ma_variant_t *size_var = NULL;
                if(MA_OK == (err = ma_table_get_value(msg_table, MA_USER_SENSOR_MSG_ATTR_SIZE, &size_var))) {
                    if(MA_OK == (err = ma_variant_get_uint32(size_var, (ma_uint32_t*)&size))) {
                        err = ma_user_sensor_msg_create(msg, size);
                    }
                    (void)ma_variant_release(size_var);
                }
            }

            {
                ma_variant_t *logon_state_var = NULL;
                if((MA_OK == err) && (MA_OK == (err = ma_table_get_value(msg_table, MA_USER_SENSOR_MSG_ATTR_LOG_ON_STATE, &logon_state_var)))) {
                    ma_user_logged_state_t state = MA_USER_LOGGED_STATE_ON;
                    if(MA_OK == (err = ma_variant_get_uint32(logon_state_var, (ma_uint32_t*)&state))) {
                        err = ma_user_sensor_msg_set_logon_state(*msg, state);
                    }
                    (void)ma_variant_release(logon_state_var);
                }
            }

            {
                ma_variant_t *startup_flag_var = NULL;
                if((MA_OK == err) && (MA_OK == (err = ma_table_get_value(msg_table, MA_USER_SENSOR_MSG_ATTR_STARTUP_FLAG, &startup_flag_var)))) {
                    ma_bool_t flag = MA_FALSE;
                    if(MA_OK == (err = ma_variant_get_bool(startup_flag_var, &flag))) {
                        err = ma_user_sensor_msg_set_startup_flag(*msg, flag);
                    }
                    (void)ma_variant_release(startup_flag_var);
                }
            }

            {
                ma_variant_t *event_type_var = NULL;
                if((MA_OK == err) && (MA_OK == (err = ma_table_get_value(msg_table, MA_USER_SENSOR_MSG_ATTR_EVENT_TYPE, &event_type_var)))) {
                    ma_uint32_t type = 0;
                    if(MA_OK == (err = ma_variant_get_uint32(event_type_var, &type))) {
                        err = ma_user_sensor_msg_set_event_type(*msg, (ma_sensor_event_type_t)type);
                    }
                    (void)ma_variant_release(event_type_var);
                }
            }

            {
                ma_variant_t *info_var = NULL;
                if((MA_OK == err) && (MA_OK == (err = ma_table_get_value(msg_table, MA_USER_SENSOR_MSG_ATTR_INFO_LIST, &info_var)))) {
                    ma_array_t *info = NULL;
                    if(MA_OK == (err = ma_variant_get_array(info_var, &info))) {
                        size_t itr = 0;
                        for(itr = 0; itr < size; ++itr) {
                            ma_variant_t *info_entry = NULL;
                            if(MA_OK == (err = ma_array_get_element_at(info, itr+1, &info_entry))) {
                                ma_table_t *info_table = NULL;
                                if(MA_OK == (err = ma_variant_get_table(info_entry, &info_table))) {
                                    ma_uint32_t is_user_wide_char = MA_FALSE;
                                    ma_uint32_t is_domain_wide_char = MA_FALSE;
                                    {
                                        ma_variant_t *session_id_var = NULL;
                                        if(MA_OK == (err = ma_table_get_value(info_table, MA_USER_SENSOR_MSG_ATTR_SESSION_ID, &session_id_var))) {
                                            ma_uint32_t session_id = 0;
                                            if(MA_OK == (err = ma_variant_get_uint32(session_id_var, &session_id))) {
                                                err = ma_user_sensor_msg_set_session_id(*msg, itr, session_id);
                                            }
                                            (void)ma_variant_release(session_id_var);
                                        }
                                    }

                                    {
                                        ma_variant_t *user_wide_char_var = NULL;
                                        if((MA_OK == err) && (MA_OK == (err = ma_table_get_value(info_table, MA_USER_SENSOR_MSG_ATTR_IS_USER_WCHAR, &user_wide_char_var)))) {
                                            err = ma_variant_get_uint32(user_wide_char_var, &is_user_wide_char);
                                            (void)ma_variant_release(user_wide_char_var);
                                        }
                                    }

                                    {
                                        ma_variant_t *domain_wide_char_var = NULL;
                                        if((MA_OK == err) && (MA_OK == (err = ma_table_get_value(info_table, MA_USER_SENSOR_MSG_ATTR_IS_DOMAIN_WCHAR, &domain_wide_char_var)))) {
                                            err = ma_variant_get_uint32(domain_wide_char_var, &is_domain_wide_char);
                                            (void)ma_variant_release(domain_wide_char_var);
                                        }
                                    }

                                    {
                                        if(MA_OK == err) {
                                            ma_variant_t *user_var = NULL;
                                            size_t len = 0;

                                            if(MA_ASCII_STRING == is_user_wide_char) {
                                                if(MA_OK == (err = ma_table_get_value(info_table, MA_USER_SENSOR_MSG_ATTR_USER, &user_var))) {
                                                    ma_buffer_t *user_buf = NULL;
                                                    if(MA_OK == (ma_variant_get_string_buffer(user_var, &user_buf))) {
                                                        const char *user = NULL;
                                                        if(MA_OK == (err = ma_buffer_get_string(user_buf, &user, &len))) {
                                                            err = ma_user_sensor_msg_set_user(*msg, itr, user);
                                                        }
                                                        (void)ma_buffer_release(user_buf);
                                                    }
                                                    (void)ma_variant_release(user_var);
                                                }
                                            } else {
                                                if(MA_OK == (err = ma_table_get_value(info_table, MA_USER_SENSOR_MSG_ATTR_USER, &user_var))) {
                                                    ma_wbuffer_t *wuser_buf = NULL;
                                                    if(MA_OK == (ma_variant_get_wstring_buffer(user_var, &wuser_buf))) {
                                                        const wchar_t *user = NULL;
                                                        if(MA_OK == (err = ma_wbuffer_get_string(wuser_buf, &user, &len))) {
                                                            err = ma_user_sensor_msg_set_wuser(*msg, itr, user);
                                                        }
                                                        (void)ma_wbuffer_release(wuser_buf);
                                                    }
                                                    (void)ma_variant_release(user_var);
                                                }
                                            }
                                        }
                                    }

                                    {
                                        if(MA_OK == err) {
                                            ma_variant_t *domain_var = NULL;
                                            size_t len = 0;

                                            if(MA_ASCII_STRING == is_user_wide_char) {
                                                if(MA_OK == (err = ma_table_get_value(info_table, MA_USER_SENSOR_MSG_ATTR_DOMAIN, &domain_var))) {
                                                    ma_buffer_t *domain_buf = NULL;
                                                    if(MA_OK == (ma_variant_get_string_buffer(domain_var, &domain_buf))) {
                                                        const char *domain = NULL;
                                                        if(MA_OK == (err = ma_buffer_get_string(domain_buf, &domain, &len))) {
                                                            err = ma_user_sensor_msg_set_domain(*msg, itr, domain);
                                                        }
                                                        (void)ma_buffer_release(domain_buf);
                                                    }
                                                    (void)ma_variant_release(domain_var);
                                                }
                                            } else {
                                                if(MA_OK == (err = ma_table_get_value(info_table, MA_USER_SENSOR_MSG_ATTR_DOMAIN, &domain_var))) {
                                                    ma_wbuffer_t *wdomain_buf = NULL;
                                                    if(MA_OK == (ma_variant_get_wstring_buffer(domain_var, &wdomain_buf))) {
                                                        const wchar_t *domain = NULL;
                                                        if(MA_OK == (err = ma_wbuffer_get_string(wdomain_buf, &domain, &len))) {
                                                            err = ma_user_sensor_msg_set_wdomain(*msg, itr, domain);
                                                        }
                                                        (void)ma_wbuffer_release(wdomain_buf);
                                                    }
                                                    (void)ma_variant_release(domain_var);
                                                }
                                            }
                                        }
                                    }
                                    (void)ma_table_release(info_table);
                                }
                                (void)ma_variant_release(info_entry);
                            }
                        }
                        (void)ma_array_release(info);
                    }
                    (void)ma_variant_release(info_var);
                }
            }

            (void)ma_table_release(msg_table);
        } /* end of_ table */

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t convert_user_sensor_msg_to_variant(ma_user_sensor_msg_t *msg, ma_variant_t **variant) {
    if(msg && variant) {
        ma_error_t err = MA_OK;
        ma_table_t *msg_table = NULL;

        if(MA_OK == (err = ma_table_create(&msg_table))) {
            {
                ma_sensor_event_type_t event_type = MA_SYSTEM_SENSOR_EVENT_STOP;
                if((MA_OK == err) && (MA_OK == (err = ma_user_sensor_msg_get_event_type(msg, &event_type)))) {
                    ma_variant_t *event_type_var = NULL;
                    if(MA_OK == (err = ma_variant_create_from_uint32(event_type, &event_type_var))) {
                        err = ma_table_add_entry(msg_table, MA_USER_SENSOR_MSG_ATTR_EVENT_TYPE, event_type_var);
                        (void)ma_variant_release(event_type_var);
                    }
                }
            }
            {
                ma_user_logged_state_t status = MA_USER_LOGGED_STATE_ON;
                if((MA_OK == err) && (MA_OK == (err = ma_user_sensor_msg_get_logon_state(msg, &status)))) {
                    ma_variant_t *status_var = NULL;
                    if(MA_OK == (err = ma_variant_create_from_uint32(status, &status_var))) {
                        err = ma_table_add_entry(msg_table, MA_USER_SENSOR_MSG_ATTR_LOG_ON_STATE, status_var);
                        (void)ma_variant_release(status_var);
                    }
                }
            }
            {
                ma_bool_t startup_flag = MA_FALSE;
                if((MA_OK == err) && (MA_OK == (err = ma_user_sensor_msg_get_startup_flag(msg, &startup_flag)))) {
                    ma_variant_t *startup_flag_var = NULL;
                    if(MA_OK == (err = ma_variant_create_from_bool(startup_flag, &startup_flag_var))) {
                        err = ma_table_add_entry(msg_table, MA_USER_SENSOR_MSG_ATTR_STARTUP_FLAG, startup_flag_var);
                        (void)ma_variant_release(startup_flag_var);
                    }
                }
            }
            {
                size_t size = 0;
                if(MA_OK == (err = ma_user_sensor_msg_get_size(msg, &size))) {
                    ma_variant_t *size_var = NULL;
                    if(MA_OK == (err = ma_variant_create_from_uint32((ma_uint32_t)size, &size_var))) {
                        err = ma_table_add_entry(msg_table, MA_USER_SENSOR_MSG_ATTR_SIZE, size_var);
                        (void)ma_variant_release(size_var);
                    }
                }
                if(size) {
                    ma_array_t *info = NULL;
                    if(MA_OK == (err = ma_array_create(&info))) {
                        size_t itr = 0;
                        ma_variant_t *info_var = NULL;

                        for(itr = 0; itr < size; ++itr) {
                            ma_uint32_t is_user_wide_char = MA_FALSE;
                            ma_uint32_t is_domain_wide_char = MA_FALSE;
                            ma_table_t *info_table = NULL;

                            if(MA_OK == (err = ma_table_create(&info_table))) {
                                ma_variant_t *info_entry = NULL;
                                {
                                    ma_uint32_t session_id = 0;
                                    if(MA_OK == (err = ma_user_sensor_msg_get_session_id(msg, itr, &session_id))) {
                                        ma_variant_t *session_id_var = NULL;
                                        if(MA_OK == (err = ma_variant_create_from_uint32(session_id, &session_id_var))) {
                                            err = ma_table_add_entry(info_table, MA_USER_SENSOR_MSG_ATTR_SESSION_ID, session_id_var);
                                            (void)ma_variant_release(session_id_var);
                                        }
                                    }
                                }

                                {
                                    if((MA_OK == err) && (MA_OK == (err = ma_user_sensor_msg_is_user_wide_char(msg, itr, &is_user_wide_char)))) {
                                        ma_variant_t *user_wide_char_var = NULL;
                                        if(MA_OK == (err = ma_variant_create_from_uint32(is_user_wide_char, &user_wide_char_var))) {
                                            err = ma_table_add_entry(info_table, MA_USER_SENSOR_MSG_ATTR_IS_USER_WCHAR, user_wide_char_var);
                                            (void)ma_variant_release(user_wide_char_var);
                                        }
                                    }
                                }

                                {
                                    if((MA_OK == err) && (MA_OK == (err = ma_user_sensor_msg_is_domain_wide_char(msg, itr, &is_domain_wide_char)))) {
                                        ma_variant_t *domain_wide_char_var = NULL;
                                        if(MA_OK == (err = ma_variant_create_from_uint32(is_domain_wide_char, &domain_wide_char_var))) {
                                            err = ma_table_add_entry(info_table, MA_USER_SENSOR_MSG_ATTR_IS_DOMAIN_WCHAR, domain_wide_char_var);
                                            (void)ma_variant_release(domain_wide_char_var);
                                        }
                                    }
                                }

                                {
                                    if(MA_OK == err) {
                                        ma_variant_t *user_var = NULL;
                                        size_t len = 0;
                                        if(MA_ASCII_STRING == is_user_wide_char) {
                                            ma_buffer_t *user_buf = NULL;
                                            if(MA_OK == (err = ma_user_sensor_msg_get_user(msg, itr, &user_buf))) {
                                                const char *user = NULL;
                                                if(MA_OK == (err = ma_buffer_get_string(user_buf, &user, &len))) {
                                                    if(MA_OK == (err = ma_variant_create_from_string(user, len, &user_var))) {
                                                        err = ma_table_add_entry(info_table, MA_USER_SENSOR_MSG_ATTR_USER, user_var);
                                                        (void)ma_variant_release(user_var);
                                                    }
                                                }
                                                (void)ma_buffer_release(user_buf);
                                            }
                                        } else {
                                            ma_wbuffer_t *wuser_buf = NULL;
                                            if(MA_OK == (err = ma_user_sensor_msg_get_wuser(msg, itr, &wuser_buf))) {
                                                const wchar_t *user = NULL;
                                                if(MA_OK == (err = ma_wbuffer_get_string(wuser_buf, &user, &len))) {
                                                    if(MA_OK == (err = ma_variant_create_from_wstring(user, len, &user_var))) {
                                                        err = ma_table_add_entry(info_table, MA_USER_SENSOR_MSG_ATTR_USER, user_var);
                                                        (void)ma_variant_release(user_var);
                                                    }
                                                }
                                                (void)ma_wbuffer_release(wuser_buf);
                                            }
                                        }
                                    }
                                }

                                {
                                    if(MA_OK == err) {
                                        ma_variant_t *domain_var = NULL;
                                        size_t len = 0;
                                        if(MA_ASCII_STRING == is_domain_wide_char) {
                                            ma_buffer_t *domain_buf = NULL;
                                            if(MA_OK == (err = ma_user_sensor_msg_get_domain(msg, itr, &domain_buf))) {
                                                const char *domain = NULL;
                                                if(MA_OK == (err = ma_buffer_get_string(domain_buf, &domain, &len))) {
                                                    if(MA_OK == (err = ma_variant_create_from_string(domain, len, &domain_var))) {
                                                        err = ma_table_add_entry(info_table, MA_USER_SENSOR_MSG_ATTR_DOMAIN, domain_var);
                                                        (void)ma_variant_release(domain_var);
                                                    }
                                                }
                                                (void)ma_buffer_release(domain_buf);
                                            }
                                        } else {
                                            ma_wbuffer_t *wdomain_buf = NULL;
                                            if(MA_OK == (err = ma_user_sensor_msg_get_wdomain(msg, itr, &wdomain_buf))) {
                                                const wchar_t *domain = NULL;
                                                if(MA_OK == (err = ma_wbuffer_get_string(wdomain_buf, &domain, &len))) {
                                                    if(MA_OK == (err = ma_variant_create_from_wstring(domain, len, &domain_var))) {
                                                        err = ma_table_add_entry(info_table, MA_USER_SENSOR_MSG_ATTR_DOMAIN, domain_var);
                                                        (void)ma_variant_release(domain_var);
                                                    }
                                                }
                                                (void)ma_wbuffer_release(wdomain_buf);
                                            }
                                        }
                                    }
                                }

                                if((MA_OK == err) && (MA_OK == (err = ma_variant_create_from_table(info_table, &info_entry)))) {
                                    err = ma_array_push(info, info_entry);
                                    (void)ma_variant_release(info_entry);
                                }
                                (void)ma_table_release(info_table);
                            } /* end of info table */
                        } /* end of for */

                        if((MA_OK == err) && (MA_OK == (err = ma_variant_create_from_array(info, &info_var)))) {
                            err = ma_table_add_entry(msg_table, MA_USER_SENSOR_MSG_ATTR_INFO_LIST, info_var);
                            (void)ma_variant_release(info_var);
                        }
                        (void)ma_array_release(info);
                    }
                }
            }

            if(MA_OK == err) {
                err = ma_variant_create_from_table(msg_table, variant);
            }
            (void)ma_table_release(msg_table);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t convert_variant_to_power_sensor_msg(ma_variant_t *variant, ma_power_sensor_msg_t **msg) {
    if(variant && msg) {
        ma_error_t err = MA_OK;
        ma_table_t *msg_table = NULL;

        if(MA_OK == (err = ma_variant_get_table(variant, &msg_table))) {
            size_t size = 0;
            {
                ma_variant_t *size_var = NULL;
                if(MA_OK == (err = ma_table_get_value(msg_table, MA_POWER_SENSOR_MSG_ATTR_SIZE, &size_var))) {
                    if(MA_OK == (err = ma_variant_get_uint32(size_var, (ma_uint32_t*)&size))) {
                        err = ma_power_sensor_msg_create(msg, size);
                    }
                    (void)ma_variant_release(size_var);
                }
            }

            {
                ma_variant_t *event_type_var = NULL;
                if((MA_OK == err) && (MA_OK == (err = ma_table_get_value(msg_table, MA_POWER_SENSOR_MSG_ATTR_EVENT_TYPE, &event_type_var)))) {
                    ma_uint32_t type = 0;
                    if(MA_OK == (err = ma_variant_get_uint32(event_type_var, &type))) {
                        err = ma_power_sensor_msg_set_event_type(*msg, (ma_sensor_event_type_t)type);
                    }
                    (void)ma_variant_release(event_type_var);
                }
            }

            {
                ma_variant_t *info_var = NULL;
                if((MA_OK == err) && (MA_OK == (err = ma_table_get_value(msg_table, MA_POWER_SENSOR_MSG_ATTR_SETTING_LIST, &info_var)))) {
                    ma_array_t *info = NULL;
                    if(MA_OK == (err = ma_variant_get_array(info_var, &info))) {
                        size_t itr = 0;
                        for(itr = 0; itr < size; ++itr) {
                            ma_variant_t *info_entry = NULL;
                            if(MA_OK == (err = ma_array_get_element_at(info, itr+1, &info_entry))) {
                                ma_table_t *info_table = NULL;
                                if(MA_OK == (err = ma_variant_get_table(info_entry, &info_table))) {
                                    {
                                        ma_variant_t *status_var = NULL;
                                        if(MA_OK == (err = ma_table_get_value(info_table, MA_POWER_SENSOR_MSG_ATTR_POWER_STATUS, &status_var))) {
                                            ma_power_status_t status = MA_POWER_STATUS_OFFLINE;
                                            if(MA_OK == (err = ma_variant_get_uint32(status_var, (ma_uint32_t*)&status))) {
                                                err = ma_power_sensor_msg_set_power_status(*msg, itr, status);
                                            }
                                            (void)ma_variant_release(status_var);
                                        }
                                    }

                                    {
                                        ma_variant_t *status_var = NULL;
                                        if(MA_OK == (err = ma_table_get_value(info_table, MA_POWER_SENSOR_MSG_ATTR_BATTERY_CHARGE_STATUS, &status_var))) {
                                            ma_battery_charge_status_t status = MA_BATTERY_CHARGE_STATUS_HIGH;
                                            if(MA_OK == (err = ma_variant_get_uint32(status_var, (ma_uint32_t*)&status))) {
                                                err = ma_power_sensor_msg_set_battery_charge_status(*msg, itr, status);
                                            }
                                            (void)ma_variant_release(status_var);
                                        }
                                    }

                                    {
                                        ma_variant_t *percent_var = NULL;
                                        if(MA_OK == (err = ma_table_get_value(info_table, MA_POWER_SENSOR_MSG_ATTR_BATTERY_PERCENTAGE, &percent_var))) {
                                            ma_uint32_t percent = 0;
                                            if(MA_OK == (err = ma_variant_get_uint32(percent_var, &percent))) {
                                                err = ma_power_sensor_msg_set_battery_percent(*msg, itr, percent);
                                            }
                                            (void)ma_variant_release(percent_var);
                                        }
                                    }

                                    {
                                        ma_variant_t *battery_life_var = NULL;
                                        if(MA_OK == (err = ma_table_get_value(info_table, MA_POWER_SENSOR_MSG_ATTR_BATTERY_LIFE, &battery_life_var))) {
                                            ma_uint32_t life = 0;
                                            if(MA_OK == (err = ma_variant_get_uint32(battery_life_var, &life))) {
                                                err = ma_power_sensor_msg_set_battery_lifetime(*msg, itr, life);
                                            }
                                            (void)ma_variant_release(battery_life_var);
                                        }
                                    }

                                    (void)ma_table_release(info_table);
                                }
                                (void)ma_variant_release(info_entry);
                            }
                        }
                        (void)ma_array_release(info);
                    }
                    (void)ma_variant_release(info_var);
                }
            }

            (void)ma_table_release(msg_table);
        } /* end of_ table */

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t convert_power_sensor_msg_to_variant(ma_power_sensor_msg_t *msg, ma_variant_t **variant) {
    if(msg && variant) {
        ma_error_t err = MA_OK;
        ma_table_t *msg_table = NULL;

        if(MA_OK == (err = ma_table_create(&msg_table))) {
            {
                ma_sensor_event_type_t event_type = MA_SYSTEM_SENSOR_EVENT_STOP;
                if(MA_OK == (err = ma_power_sensor_msg_get_event_type(msg, &event_type))) {
                    ma_variant_t *event_type_var = NULL;
                    if(MA_OK == (err = ma_variant_create_from_uint32(event_type, &event_type_var))) {
                        err = ma_table_add_entry(msg_table, MA_POWER_SENSOR_MSG_ATTR_EVENT_TYPE, event_type_var);
                        (void)ma_variant_release(event_type_var);
                    }
                }
            }
            {
                size_t size = 0;
                if((MA_OK == err) && (MA_OK == (err = ma_power_sensor_msg_get_size(msg, &size)))) {
                    ma_variant_t *size_var = NULL;
                    if(MA_OK == (err = ma_variant_create_from_uint32((ma_uint32_t)size, &size_var))) {
                        err = ma_table_add_entry(msg_table, MA_POWER_SENSOR_MSG_ATTR_SIZE, size_var);
                        (void)ma_variant_release(size_var);
                    }
                }
                if(size) {
                    ma_array_t *info = NULL;
                    if((MA_OK == err) && (MA_OK == (err = ma_array_create(&info)))) {
                        size_t itr = 0;
                        ma_variant_t *info_var = NULL;

                        for(itr = 0; itr < size; ++itr) {

							ma_table_t *info_table = NULL;

                            if(MA_OK == (err = ma_table_create(&info_table))) {
                                ma_variant_t *info_entry = NULL;
                                {
                                    ma_power_status_t status = MA_POWER_STATUS_OFFLINE;
                                    if(MA_OK == (err = ma_power_sensor_msg_get_power_status(msg, itr, &status))) {
                                        ma_variant_t *status_var = NULL;
                                        if(MA_OK == (err = ma_variant_create_from_uint32(status, &status_var))) {
                                            err = ma_table_add_entry(info_table, MA_POWER_SENSOR_MSG_ATTR_POWER_STATUS, status_var);
                                            (void)ma_variant_release(status_var);
                                        }
                                    }
                                }
                                {
                                    ma_battery_charge_status_t status = MA_BATTERY_CHARGE_STATUS_HIGH;
                                    if((MA_OK == err) && (MA_OK == (err = ma_power_sensor_msg_get_batter_charge_status(msg, itr, &status)))) {
                                        ma_variant_t *status_var = NULL;
                                        if(MA_OK == (err = ma_variant_create_from_uint32(status, &status_var))) {
                                            err = ma_table_add_entry(info_table, MA_POWER_SENSOR_MSG_ATTR_BATTERY_CHARGE_STATUS, status_var);
                                            (void)ma_variant_release(status_var);
                                        }
                                    }
                                }
                                {
                                    ma_int32_t percent = 0;
                                    if((MA_OK == err) && (MA_OK == (err = ma_power_sensor_msg_get_battery_percent(msg, itr, &percent)))) {
                                        ma_variant_t *percent_var = NULL;
                                        if(MA_OK == (err = ma_variant_create_from_uint32(percent, &percent_var))) {
                                            err = ma_table_add_entry(info_table, MA_POWER_SENSOR_MSG_ATTR_BATTERY_PERCENTAGE, percent_var);
                                            (void)ma_variant_release(percent_var);
                                        }
                                    }
                                }
                                {
                                    unsigned long battery_life = 0;
                                    if((MA_OK == err) && (MA_OK == (err = ma_power_sensor_msg_get_battery_lifetime(msg, itr, &battery_life)))) {
                                        ma_variant_t *life_var = NULL;
                                        if(MA_OK == (err = ma_variant_create_from_uint32(battery_life, &life_var))) {
                                            err = ma_table_add_entry(info_table, MA_POWER_SENSOR_MSG_ATTR_BATTERY_LIFE, life_var);
                                            (void)ma_variant_release(life_var);
                                        }
                                    }
                                }

                                if((MA_OK == err) && (MA_OK == (err = ma_variant_create_from_table(info_table, &info_entry)))) {
                                    err = ma_array_push(info, info_entry);
                                    (void)ma_variant_release(info_entry);
                                }
                                (void)ma_table_release(info_table);
                            } /* end of info table */
                        } /* end of for */

                        if((MA_OK == err) && (MA_OK == (err = ma_variant_create_from_array(info, &info_var)))) {
                            err = ma_table_add_entry(msg_table, MA_POWER_SENSOR_MSG_ATTR_SETTING_LIST, info_var);
                            (void)ma_variant_release(info_var);
                        }
                        (void)ma_array_release(info);
                    }
                }
            }

            if(MA_OK == err) {
                err = ma_variant_create_from_table(msg_table, variant);
            }
            (void)ma_table_release(msg_table);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t convert_variant_to_network_sensor_msg(ma_variant_t *variant, ma_network_sensor_msg_t **msg) {
    if(variant && msg) {
        ma_error_t err = MA_OK;
        ma_table_t *msg_table = NULL;

        if(MA_OK == (err = ma_variant_get_table(variant, &msg_table))) {
            size_t size = 0;
            {
                ma_variant_t *size_var = NULL;
                if(MA_OK == (err = ma_table_get_value(msg_table, MA_NETWORK_SENSOR_MSG_ATTR_SIZE, &size_var))) {
                    if(MA_OK == (err = ma_variant_get_uint32(size_var, (ma_uint32_t*)&size))) {
                        err = ma_network_sensor_msg_create(msg, size);
                    }
                    (void)ma_variant_release(size_var);
                }
            }

            {
                ma_variant_t *event_type_var = NULL;
                if((MA_OK == err) && (MA_OK == (err = ma_table_get_value(msg_table, MA_NETWORK_SENSOR_MSG_ATTR_EVENT_TYPE, &event_type_var)))) {
                    ma_uint32_t type = 0;
                    if(MA_OK == (err = ma_variant_get_uint32(event_type_var, &type))) {
                        err = ma_network_sensor_msg_set_event_type(*msg, (ma_sensor_event_type_t)type);
                    }
                    (void)ma_variant_release(event_type_var);
                }
            }

            {
                ma_variant_t *status_var = NULL;
                if((MA_OK == err) && (MA_OK == (err = ma_table_get_value(msg_table, MA_NETWORK_SENSOR_MSG_ATTR_CONNECTIVITY_STATUS, &status_var)))) {
                    ma_network_connectivity_status_t status = MA_NETWORK_CONNECTIVITY_STATUS_CONNECTED;
                    if(MA_OK == (err = ma_variant_get_uint32(status_var, (ma_uint32_t*)&status))) {
                        err = ma_network_sensor_msg_set_connectivity_status(*msg, status);
                    }
                    (void)ma_variant_release(status_var);
                }
            }

            {
                ma_variant_t *info_var = NULL;
                if((MA_OK == err) && (MA_OK == (err = ma_table_get_value(msg_table, MA_NETWORK_SENSOR_MSG_ATTR_SETTING_LIST, &info_var)))) {
                    ma_array_t *info = NULL;
                    if(MA_OK == (err = ma_variant_get_array(info_var, &info))) {
                        size_t itr = 0;
                        for(itr = 0; itr < size; ++itr) {
                            ma_variant_t *info_entry = NULL;
                            if(MA_OK == (err = ma_array_get_element_at(info, itr+1, &info_entry))) {
                                ma_table_t *info_table = NULL;
                                if(MA_OK == (err = ma_variant_get_table(info_entry, &info_table))) {
//TODO

                                    (void)ma_table_release(info_table);
                                }
                                (void)ma_variant_release(info_entry);
                            }
                        }
                        (void)ma_array_release(info);
                    }
                    (void)ma_variant_release(info_var);
                }
            }

            (void)ma_table_release(msg_table);
        } /* end of_ table */

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t convert_network_sensor_msg_to_variant(ma_network_sensor_msg_t *msg, ma_variant_t **variant) {
    if(msg && variant) {
        ma_error_t err = MA_OK;
        ma_table_t *msg_table = NULL;

        if(MA_OK == (err = ma_table_create(&msg_table))) {
            {
                ma_sensor_event_type_t event_type = MA_SYSTEM_SENSOR_EVENT_STOP;
                if((MA_OK == err) && (MA_OK == (err = ma_network_sensor_msg_get_event_type(msg, &event_type)))) {
                    ma_variant_t *event_type_var = NULL;
                    if(MA_OK == (err = ma_variant_create_from_uint32(event_type, &event_type_var))) {
                        err = ma_table_add_entry(msg_table, MA_NETWORK_SENSOR_MSG_ATTR_EVENT_TYPE, event_type_var);
                        (void)ma_variant_release(event_type_var);
                    }
                }
            }
            {
                ma_network_connectivity_status_t status = MA_NETWORK_CONNECTIVITY_STATUS_CONNECTED;
                if((MA_OK == err) && (MA_OK == (err = ma_network_sensor_msg_get_connectivity_status(msg, &status)))) {
                    ma_variant_t *status_var = NULL;
                    if(MA_OK == (err = ma_variant_create_from_uint32(status, &status_var))) {
                        err = ma_table_add_entry(msg_table, MA_NETWORK_SENSOR_MSG_ATTR_CONNECTIVITY_STATUS, status_var);
                        (void)ma_variant_release(status_var);
                    }
                }
            }
            {
                size_t size = 0;
                if(MA_OK == (err = ma_network_sensor_msg_get_size(msg, &size))) {
                    ma_variant_t *size_var = NULL;
                    if(MA_OK == (err = ma_variant_create_from_uint32((ma_uint32_t)size, &size_var))) {
                        err = ma_table_add_entry(msg_table, MA_NETWORK_SENSOR_MSG_ATTR_SIZE, size_var);
                        (void)ma_variant_release(size_var);
                    }
                }
                if(size) {
                    ma_array_t *info = NULL;
                    if(MA_OK == (err = ma_array_create(&info))) {
                        size_t itr = 0;
                        ma_variant_t *info_var = NULL;

                        for(itr = 0; itr < size; ++itr) {

                            ma_table_t *info_table = NULL;

                            if(MA_OK == (err = ma_table_create(&info_table))) {
                                ma_variant_t *info_entry = NULL;

                                if((MA_OK == err) && (MA_OK == (err = ma_variant_create_from_table(info_table, &info_entry)))) {
                                    err = ma_array_push(info, info_entry);
                                    (void)ma_variant_release(info_entry);
                                }
                                (void)ma_table_release(info_table);
                            } /* end of info table */
                        } /* end of for */

                        if((MA_OK == err) && (MA_OK == (err = ma_variant_create_from_array(info, &info_var)))) {
                            err = ma_table_add_entry(msg_table, MA_NETWORK_SENSOR_MSG_ATTR_SETTING_LIST, info_var);
                            (void)ma_variant_release(info_var);
                        }
                        (void)ma_array_release(info);
                    }
                }
            }

            if(MA_OK == err) {
                err = ma_variant_create_from_table(msg_table, variant);
            }
            (void)ma_table_release(msg_table);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t convert_system_sensor_msg_to_variant(ma_system_sensor_msg_t *msg, ma_variant_t **variant) {
    if(msg && variant) {
        ma_error_t err = MA_OK;
        ma_table_t *msg_table = NULL;

        if(MA_OK == (err = ma_table_create(&msg_table))) {
            {
                ma_sensor_event_type_t event_type = MA_SYSTEM_SENSOR_EVENT_STOP;
                if((MA_OK == err) && (MA_OK == (err = ma_system_sensor_msg_get_event_type(msg, &event_type)))) {
                    ma_variant_t *event_type_var = NULL;
                    if(MA_OK == (err = ma_variant_create_from_uint32(event_type, &event_type_var))) {
                        err = ma_table_add_entry(msg_table, MA_SYSTEM_SENSOR_MSG_ATTR_EVENT_TYPE, event_type_var);
                        (void)ma_variant_release(event_type_var);
                    }
                }
            }
            {
                size_t size = 0;
                if(MA_OK == (err = ma_system_sensor_msg_get_size(msg, &size))) {
                    ma_variant_t *size_var = NULL;
                    if(MA_OK == (err = ma_variant_create_from_uint32((ma_uint32_t)size, &size_var))) {
                        err = ma_table_add_entry(msg_table, MA_SYSTEM_SENSOR_MSG_ATTR_SIZE, size_var);
                        (void)ma_variant_release(size_var);
                    }
                }
                if(size) {
                    ma_array_t *info = NULL;
                    if(MA_OK == (err = ma_array_create(&info))) {
                        size_t itr = 0;
                        ma_variant_t *info_var = NULL;

                        for(itr = 0; itr < size; ++itr) {

                            ma_table_t *info_table = NULL;

                            if(MA_OK == (err = ma_table_create(&info_table))) {
                                ma_variant_t *info_entry = NULL;

                                if((MA_OK == err) && (MA_OK == (err = ma_variant_create_from_table(info_table, &info_entry)))) {
                                    err = ma_array_push(info, info_entry);
                                    (void)ma_variant_release(info_entry);
                                }
                                (void)ma_table_release(info_table);
                            } /* end of info table */
                        } /* end of for */

                        if((MA_OK == err) && (MA_OK == (err = ma_variant_create_from_array(info, &info_var)))) {
                            err = ma_table_add_entry(msg_table, MA_SYSTEM_SENSOR_MSG_ATTR_SETTING_LIST, info_var);
                            (void)ma_variant_release(info_var);
                        }
                        (void)ma_array_release(info);
                    }
                }
            }

            if(MA_OK == err) {
                err = ma_variant_create_from_table(msg_table, variant);
            }
            (void)ma_table_release(msg_table);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t convert_variant_to_system_sensor_msg(ma_variant_t *variant, ma_system_sensor_msg_t **msg) {
    if(variant && msg) {
        ma_error_t err = MA_OK;
        ma_table_t *msg_table = NULL;

        if(MA_OK == (err = ma_variant_get_table(variant, &msg_table))) {
            size_t size = 0;
            {
                ma_variant_t *size_var = NULL;
                if(MA_OK == (err = ma_table_get_value(msg_table, MA_SYSTEM_SENSOR_MSG_ATTR_SIZE, &size_var))) {
                    if(MA_OK == (err = ma_variant_get_uint32(size_var, (ma_uint32_t*)&size))) {
                        err = ma_system_sensor_msg_create(msg, size);
                    }
                    (void)ma_variant_release(size_var);
                }
            }

            {
                ma_variant_t *event_type_var = NULL;
                if((MA_OK == err) && (MA_OK == (err = ma_table_get_value(msg_table, MA_SYSTEM_SENSOR_MSG_ATTR_EVENT_TYPE, &event_type_var)))) {
                    ma_uint32_t type = 0;
                    if(MA_OK == (err = ma_variant_get_uint32(event_type_var, &type))) {
                        err = ma_system_sensor_msg_set_event_type(*msg, (ma_sensor_event_type_t)type);
                    }
                    (void)ma_variant_release(event_type_var);
                }
            }

            {
                ma_variant_t *info_var = NULL;
                if((MA_OK == err) && (MA_OK == (err = ma_table_get_value(msg_table, MA_SYSTEM_SENSOR_MSG_ATTR_SETTING_LIST, &info_var)))) {
                    ma_array_t *info = NULL;
                    if(MA_OK == (err = ma_variant_get_array(info_var, &info))) {
                        size_t itr = 0;
                        for(itr = 0; itr < size; ++itr) {
                            ma_variant_t *info_entry = NULL;
                            if(MA_OK == (err = ma_array_get_element_at(info, itr+1, &info_entry))) {
                                ma_table_t *info_table = NULL;
                                if(MA_OK == (err = ma_variant_get_table(info_entry, &info_table))) {
//TODO

                                    (void)ma_table_release(info_table);
                                }
                                (void)ma_variant_release(info_entry);
                            }
                        }
                        (void)ma_array_release(info);
                    }
                    (void)ma_variant_release(info_var);
                }
            }

            (void)ma_table_release(msg_table);
        } /* end of_ table */

        return err;
    }
    return MA_ERROR_INVALIDARG;
}


/* idle message to variant converter */
ma_error_t convert_idle_sensor_msg_to_variant(ma_idle_sensor_msg_t *msg, ma_variant_t **variant) {
    if(msg && variant) {
        ma_error_t err = MA_OK;
        ma_table_t *msg_table = NULL;

        if(MA_OK == (err = ma_table_create(&msg_table))) {
            {
                ma_sensor_event_type_t event_type = MA_IDLE_SENSOR_IDLE_TIME_CHANGE_EVENT;

                if((MA_OK == err) && (MA_OK == (err = ma_idle_sensor_msg_get_event_type(msg, &event_type)))) {
                    ma_variant_t *event_type_var = NULL;
                    if(MA_OK == (err = ma_variant_create_from_uint32(event_type, &event_type_var))) {
                        err = ma_table_add_entry(msg_table, MA_IDLE_SENSOR_MSG_ATTR_EVENT_TYPE, event_type_var);
                        (void)ma_variant_release(event_type_var);
                    }
                }
            }
            {
                size_t size = 0;

                if(MA_OK == (err = ma_idle_sensor_msg_get_size(msg, &size))) {
                    ma_variant_t *size_var = NULL;
                    if(MA_OK == (err = ma_variant_create_from_uint32((ma_uint32_t)size, &size_var))) {
                        err = ma_table_add_entry(msg_table, MA_IDLE_SENSOR_MSG_ATTR_SIZE, size_var);
                        (void)ma_variant_release(size_var);
                    }
                }
                if(size) {
                    ma_array_t *info = NULL;
                    if(MA_OK == (err = ma_array_create(&info))) {
                        size_t itr = 0;
                        ma_variant_t *info_var = NULL;

                        for(itr = 0; itr < size; ++itr) {
                            ma_table_t *info_table = NULL;

                            if(MA_OK == (err = ma_table_create(&info_table))) {
                                ma_variant_t *info_entry = NULL;
                                
                                {
                                    int idle_wait = 0;
                                    if(MA_OK == (err = ma_idle_sensor_msg_get_idle_wait(msg, itr, &idle_wait))) {
                                        ma_variant_t *idle_wait_var = NULL;
                                        if(MA_OK == (err = ma_variant_create_from_int32(idle_wait, &idle_wait_var))) {
                                            err = ma_table_add_entry(info_table, MA_IDLE_SENSOR_MSG_ATTR_IDLE_WAIT, idle_wait_var);
                                            (void)ma_variant_release(idle_wait_var);
                                        }
                                    }
                                }

                                if((MA_OK == err) && (MA_OK == (err = ma_variant_create_from_table(info_table, &info_entry)))) {
                                    err = ma_array_push(info, info_entry);
                                    (void)ma_variant_release(info_entry);
                                }
                                (void)ma_table_release(info_table);
                            } /* end of info table */
                        } /* end of for */

                        if((MA_OK == err) && (MA_OK == (err = ma_variant_create_from_array(info, &info_var)))) {
                            err = ma_table_add_entry(msg_table, MA_IDLE_SENSOR_MSG_ATTR_SETTING_LIST, info_var);
                            (void)ma_variant_release(info_var);
                        }
                        (void)ma_array_release(info);
                    }
                }
            }

            if(MA_OK == err) {
                err = ma_variant_create_from_table(msg_table, variant);
            }
            (void)ma_table_release(msg_table);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}


/* variant to idle message converter */
ma_error_t convert_variant_to_idle_sensor_msg(ma_variant_t *variant, ma_idle_sensor_msg_t **msg) {
    if(variant && msg) {
        ma_error_t err = MA_OK;
        ma_table_t *msg_table = NULL;

        if(MA_OK == (err = ma_variant_get_table(variant, &msg_table))) {
            size_t size = 0;
            {
                ma_variant_t *size_var = NULL;
                if(MA_OK == (err = ma_table_get_value(msg_table, MA_IDLE_SENSOR_MSG_ATTR_SIZE, &size_var))) {
                    if(MA_OK == (err = ma_variant_get_uint32(size_var, (ma_uint32_t*)&size))) {
                        err = ma_idle_sensor_msg_create(msg, size);
                    }
                    (void)ma_variant_release(size_var);
                }
            }

            {
                ma_variant_t *event_type_var = NULL;
                if((MA_OK == err) && (MA_OK == (err = ma_table_get_value(msg_table, MA_IDLE_SENSOR_MSG_ATTR_EVENT_TYPE, &event_type_var)))) {
                    ma_uint32_t type = 0;
                    if(MA_OK == (err = ma_variant_get_uint32(event_type_var, &type))) {
                        err = ma_idle_sensor_msg_set_event_type(*msg, (ma_sensor_event_type_t)type);
                    }
                    (void)ma_variant_release(event_type_var);
                }
            }

            {
                ma_variant_t *info_var = NULL;
                if((MA_OK == err) && (MA_OK == (err = ma_table_get_value(msg_table, MA_IDLE_SENSOR_MSG_ATTR_SETTING_LIST, &info_var)))) {
                    ma_array_t *info = NULL;
                    if(MA_OK == (err = ma_variant_get_array(info_var, &info))) {
                        size_t itr = 0;
                        for(itr = 0; itr < size; ++itr) {
                            ma_variant_t *info_entry = NULL;
                            if(MA_OK == (err = ma_array_get_element_at(info, itr+1, &info_entry))) {
                                ma_table_t *info_table = NULL;
                                if(MA_OK == (err = ma_variant_get_table(info_entry, &info_table))) {
                                    {
                                        ma_variant_t *idle_var = NULL;
                                        if(MA_OK == (err = ma_table_get_value(info_table, MA_IDLE_SENSOR_MSG_ATTR_IDLE_WAIT, &idle_var))) {
                                            int idle_wait = 0;

                                            if(MA_OK == (err = ma_variant_get_int32(idle_var, &idle_wait))) {
                                                err = ma_idle_sensor_msg_set_idle_wait(*msg, itr, idle_wait);
                                            }
                                            (void)ma_variant_release(idle_var);
                                        }
                                    }
                                    (void)ma_table_release(info_table);
                                }
                                (void)ma_variant_release(info_entry);
                            }
                        }
                        (void)ma_array_release(info);
                    }
                    (void)ma_variant_release(info_var);
                }
            }

            (void)ma_table_release(msg_table);
        } /* end of_ table */

        return err;
    }
    return MA_ERROR_INVALIDARG;
}
