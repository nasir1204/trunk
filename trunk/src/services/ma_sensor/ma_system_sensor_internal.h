#ifndef MA_SYSTEM_SENSOR_INTERNAL_H_INCLUDED
#define MA_SYSTEM_SENSOR_INTERNAL_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/threading/ma_atomic.h"
#include "ma/internal/ma_macros.h"
#include "ma/sensor/ma_sensor.h"

#define MA_SYSTEM_TIME_CHANGE_MAX_THRESHOLD  (5 * 60) /* in secs */
#define MA_SYSTEM_TIME_CHANGE_MIN_THRESHOLD  30 /* in secs */
#define MA_SYSTEM_SENSOR_REPEATER_INTERVAL 30 /* in secs */


MA_CPP(extern "C" {)
struct ma_system_sensor_s {
    ma_sensor_t sensor;
    ma_msgbus_t *msgbus;
    ma_sensor_type_t type;
    ma_sensor_state_t state;
    char  name[MA_MAX_NAME];
};

ma_error_t system_sensor_on_msg(ma_sensor_t *sensor, ma_sensor_msg_t *msg);
ma_error_t system_sensor_get_type(ma_sensor_t *sensor, ma_sensor_type_t *type);
ma_error_t system_sensor_get_id(ma_sensor_t *sensor, const char **id);
ma_error_t system_sensor_start(ma_sensor_t *sensor);
ma_error_t system_sensor_stop(ma_sensor_t *sensor);
ma_error_t system_sensor_release(ma_sensor_t *sensor);
ma_error_t system_sensor_detect(ma_sensor_t *sensor);


ma_bool_t detect_system_startup(ma_sensor_t *sensor);

MA_CPP(})

#endif /* MA_SYSTEM_SENSOR_INTERNAL_H_INCLUDED */


