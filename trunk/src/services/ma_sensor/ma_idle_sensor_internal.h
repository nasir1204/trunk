#ifndef MA_IDLE_SENSOR_INTERNAL_H_INCLUDED
#define MA_IDLE_SENSOR_INTERNAL_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/ma_macros.h"
#include "ma/sensor/ma_sensor.h"
#include "uv.h"


#define MA_IDLE_SENSOR_REPEATER_INTERVAL 60 /* 1 minute */

MA_CPP(extern "C" {)
struct ma_idle_sensor_s {
    ma_sensor_t sensor;
    ma_msgbus_t *msgbus;
    ma_sensor_type_t type;
    ma_sensor_state_t state;
    char  name[MA_MAX_NAME];
    uv_loop_t *loop;
    uv_timer_t idler;
};

/* idle sensor virtual methods decleration(s) */
ma_error_t idle_sensor_on_msg(ma_sensor_t *sensor, ma_sensor_msg_t *msg);
ma_error_t idle_sensor_get_type(ma_sensor_t *sensor, ma_sensor_type_t *type);
ma_error_t idle_sensor_get_id(ma_sensor_t *sensor, const char **id);
ma_error_t idle_sensor_start(ma_sensor_t *sensor);
ma_error_t idle_sensor_stop(ma_sensor_t *sensor);
ma_error_t idle_sensor_release(ma_sensor_t *sensor);
ma_error_t idle_sensor_detect(ma_sensor_t *sensor);


/* gets system inactivity elapsed time in minutes*/
ma_error_t idle_sensor_get_idle_time(ma_idle_sensor_t *idle, int *idle_mins);

MA_CPP(})

#endif /* MA_IDLE_SENSOR_INTERNAL_H_INCLUDED */


