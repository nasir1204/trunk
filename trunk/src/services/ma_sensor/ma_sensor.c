#include "ma/sensor/ma_sensor.h"
#include "ma/sensor/ma_idle_sensor.h"
#include "ma/sensor/ma_user_sensor.h"
#include "ma/sensor/ma_system_sensor.h"
#include "ma/sensor/ma_power_sensor.h"
#include "ma/sensor/ma_network_sensor.h"
#include "ma/sensor/ma_sensor_tray.h"
#include "ma/internal/utils/threading/ma_atomic.h"
#include "ma/scheduler/ma_enumerator.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/defs/ma_sensor_defs.h"
#include "ma/ma_log.h"

#include <stdlib.h>
#include <string.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "sensor"

ma_logger_t *sensor_logger;

static const char *sensor_type_str[] = {
#define MA_SENSOR_EXPANDER(type, index, type_str) type_str,
    MA_SENSOR_TYPE_EXPANDER
#undef MA_SENSOR_EXPANDER
};

static const char *sensor_event_type_str[] = {
#define MA_SENSOR_EVENT_EXPANDER(type, str)  str,
    MA_SENSOR_EVENT_TYPE_EXPANDER
#undef MA_SENSOR_EVENT_EXPANDER
};

ma_error_t ma_sensor_inc_ref(ma_sensor_t *sensor) {
    if (sensor) {
        MA_ATOMIC_INCREMENT(sensor->ref_count);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_sensor_dec_ref(ma_sensor_t *sensor) {
    if (sensor && (0 <= sensor->ref_count)) {

        if (0 == MA_ATOMIC_DECREMENT(sensor->ref_count)) {
            ma_sensor_type_t type = MA_SENSOR_TYPE_USER;

            (void)ma_sensor_release(sensor);
            (void)ma_sensor_get_type(sensor, &type);
            switch(type) {
                case MA_SENSOR_TYPE_USER: {
                    (void)ma_user_sensor_release((ma_user_sensor_t*)sensor); break;
                }
                case MA_SENSOR_TYPE_IDLE: {
                    (void)ma_idle_sensor_release((ma_idle_sensor_t*)sensor); break;
                }
                case MA_SENSOR_TYPE_SYSTEM: {
                    (void)ma_system_sensor_release((ma_system_sensor_t*)sensor); break;
                }
                case MA_SENSOR_TYPE_POWER: {
                    (void)ma_power_sensor_release((ma_power_sensor_t*)sensor); break;
                }
                case MA_SENSOR_TYPE_NETWORK: {
                    (void)ma_network_sensor_release((ma_network_sensor_t*)sensor); break;
                }
                case MA_SENSOR_TYPE_TRAY: {
                    (void)ma_sensor_tray_release((ma_sensor_tray_t*)sensor); break;
                }
            }
        }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

const char* ma_sensor_type_to_str(ma_sensor_type_t type) {
    return(type < (sizeof(sensor_type_str)/sizeof(sensor_type_str[0]))) ? sensor_type_str[type] : NULL;
}

const char* ma_sensor_event_type_to_str(ma_sensor_event_type_t type) {
    return(type < (sizeof(sensor_event_type_str)/sizeof(sensor_event_type_str[0]))) ? sensor_event_type_str[type] : NULL;
}

ma_error_t ma_sensor_set_logger(ma_logger_t *logger) {
    if(logger) {
        sensor_logger = logger;
        return ma_sensor_msg_set_logger(logger);
    }
    return MA_ERROR_INVALIDARG;
}
