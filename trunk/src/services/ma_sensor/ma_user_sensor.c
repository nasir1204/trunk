#include "ma/sensor/ma_sensor.h"
#include "ma/internal/defs/ma_sensor_defs.h"
#include "ma/ma_log.h"
#include "ma/sensor/ma_user_sensor.h"
#include "ma_user_sensor_internal.h"
#include "ma/internal/ma_macros.h"
#include "uv-private/tree.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include "ma/internal/utils/text/ma_utf8.h"

#include <stdlib.h>
#include <string.h>
#include <wchar.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "sensor"

extern ma_logger_t *sensor_logger;

static ma_error_t display_users(ma_user_node_t **node, void *data);


static const ma_sensor_methods_t user_methods = {
    &user_sensor_on_msg,
    &user_sensor_get_type,
    &user_sensor_get_id,
    &user_sensor_start,
    &user_sensor_stop,
    &user_sensor_release,
    &user_sensor_detect
};


ma_error_t ma_user_sensor_create(ma_msgbus_t *msgbus, ma_user_sensor_t **sensor) {
    if(msgbus && sensor) {
        ma_user_sensor_t *user = NULL;
        ma_error_t err = MA_ERROR_OUTOFMEMORY;

        if((user = (ma_user_sensor_t*)calloc(1, sizeof(ma_user_sensor_t)))) {
            if(MA_OK == (err = ma_sensor_inc_ref((ma_sensor_t*)(*sensor = user)))) {
                ma_sensor_init((ma_sensor_t*)*sensor, &user_methods, *sensor);
                strncpy(user->name, MA_SENSOR_TYPE_USER_STR, strlen(MA_SENSOR_TYPE_USER_STR));
                user->type = MA_SENSOR_TYPE_USER;
                user->state = MA_SENSOR_STATE_NOT_STARTED;
                user->msgbus = msgbus;
                MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) created successfully", user->name);
            } else {
                MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) creation failed, last error(%d)", ma_sensor_type_to_str(MA_SENSOR_TYPE_USER), err);
                free(user); 
                *sensor = NULL;
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_user_sensor_release(ma_user_sensor_t *sensor) {
    if(sensor) {
        ma_error_t err = MA_OK;

        if(0 == ((ma_sensor_t*)sensor)->ref_count) {
            MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) releasing...", sensor->name);
            (void)map_clear(sensor->info_map.rbh_root);
            free(sensor);
            MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) released successfully", ma_sensor_type_to_str(MA_SENSOR_TYPE_USER));
        } else {
            if(MA_OK != (err = ma_sensor_dec_ref((ma_sensor_t*)sensor)))
                MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) dec ref failed, last error(%d)", ma_sensor_type_to_str(MA_SENSOR_TYPE_USER), err);
        }

        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

 

ma_user_node_t* map_find(ma_user_tree_t *map, ma_user_t *user) {
    if(map && user) {
        ma_user_node_t search_node = {{0}};

        memset(&search_node, 0, sizeof(ma_user_node_t));
        memcpy(&search_node.user, user, sizeof(ma_user_t));
        return RB_FIND(ma_user_tree_s, map, &search_node);
    }
    return NULL;
}

ma_error_t map_insert(ma_user_tree_t *map, ma_user_t *user) {
    if(map && user) {
        ma_error_t err = MA_ERROR_OBJECTEXIST;
        ma_user_node_t *node = NULL;

        if(!map_find(map, user)) {
            err = MA_ERROR_OUTOFMEMORY;
            if((node = (ma_user_node_t*) calloc(1, sizeof(ma_user_node_t)))) {
                memcpy(&node->user, user, sizeof(ma_user_t));
                RB_INSERT(ma_user_tree_s, map, node);err = MA_OK;
            }
        }
        //map_for_each(&map->rbh_root, &display_users, NULL);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t map_for_each(ma_user_node_t **node, MAP_FOR_EACH_CALLBACK cb, void *data) {
    ma_error_t err = MA_OK;

    if (!(*node))return err;
    if(MA_OK == err) err = map_for_each(&(*node)->tree_entry.rbe_right, cb, data);
    if(MA_OK == err) err = map_for_each(&(*node)->tree_entry.rbe_left, cb, data);
    if(MA_OK == err) err = (*cb)(node, data);

    return err;
}

ma_error_t map_destroy_node(ma_user_node_t **node, void *data) {
    if(node) {
        free(*node);*node = NULL;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t map_clear(ma_user_node_t *node) {
    if(node) {
        (void)map_clear(node->tree_entry.rbe_left);
        (void)map_clear(node->tree_entry.rbe_right);
        return map_destroy_node(&node, NULL);
    }
    return MA_OK; //intentional
}

ma_error_t map_erase(ma_user_tree_t *map, ma_user_t *user) {
    if(map && user) {
        ma_error_t err = MA_ERROR_OBJECTNOTFOUND;
        ma_user_node_t *node = NULL;

        if((node = map_find(map, user))) {
            RB_REMOVE(ma_user_tree_s, map, node);
            err = map_destroy_node(&node, NULL);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_user_set_attributes_from_msg(ma_user_sensor_msg_t *msg, size_t index, ma_user_t *u) {
    if(msg && u) {
        ma_error_t err = MA_OK;
        ma_uint32_t is_user_wide_char = MA_ASCII_STRING;
        ma_uint32_t is_domain_wide_char = MA_ASCII_STRING;
        size_t len = 0;
        
        (void)ma_user_sensor_msg_get_session_id(msg, index, &u->sid);
        (void)ma_user_sensor_msg_get_logon_state(msg, &u->state);
        (void)ma_user_sensor_msg_is_user_wide_char(msg, index, &is_user_wide_char);
        if(MA_WCHAR_STRING == is_user_wide_char) {
            const wchar_t *user = NULL;
            ma_wbuffer_t *user_buf = NULL;

            if(MA_OK == (err = ma_user_sensor_msg_get_wuser(msg, index, &user_buf))) {
                if(MA_OK == (err = ma_wbuffer_get_string(user_buf, &user, &len))) {
                    if(len < MA_MAX_NAME) {
                        wcscpy(u->u.wuser, user);
                    }
                    u->is_user_wide_str = MA_WCHAR_STRING;
                }
                (void)ma_wbuffer_release(user_buf);
            }

        } else {
            const char *user = NULL;
            ma_buffer_t *user_buf = NULL;

            if(MA_OK == (err = ma_user_sensor_msg_get_user(msg, index, &user_buf))) {
                if(MA_OK == (err = ma_buffer_get_string(user_buf, &user, &len))) {
                    if(len < MA_MAX_NAME) {
                        strncpy(u->u.user, user, MA_MAX_NAME-1);
                    }
                    u->is_user_wide_str = MA_ASCII_STRING;
                }
                (void)ma_buffer_release(user_buf);
            }
        }
        
        (void)ma_user_sensor_msg_is_user_wide_char(msg, index, &is_domain_wide_char);
        if(MA_WCHAR_STRING == is_domain_wide_char) {
            const wchar_t *domain = NULL;
            ma_wbuffer_t *domain_buf = NULL;

            if(MA_OK == (err = ma_user_sensor_msg_get_wdomain(msg, index, &domain_buf))) {
                if(MA_OK == (err = ma_wbuffer_get_string(domain_buf, &domain, &len))) {
                    if(len < MA_MAX_NAME) {
                        wcscpy(u->d.wdomain, domain);
                    }
                    u->is_domain_wide_str = MA_WCHAR_STRING;
                }
                (void)ma_wbuffer_release(domain_buf);
            }

        } else {
            const char *domain = NULL;
            ma_buffer_t *domain_buf = NULL;

            if(MA_OK == (err = ma_user_sensor_msg_get_domain(msg, index, &domain_buf))) {
                if(MA_OK == (err = ma_buffer_get_string(domain_buf, &domain, &len))) {
                    if(len < MA_MAX_NAME) {
                        strcpy(u->d.domain, domain);
                    }
                    u->is_domain_wide_str = MA_ASCII_STRING;
                }
                (void)ma_buffer_release(domain_buf);
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_user_set_attributes_to_msg(ma_user_t *user, size_t index, ma_user_sensor_msg_t *msg) {
    if(user && msg) {
        ma_error_t err = MA_OK;
        
        MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "setting user session id(%d)", user->sid);
        if(MA_OK == (err = (MA_WCHAR_STRING == user->is_user_wide_str) ? ma_user_sensor_msg_set_wuser(msg, index, user->u.wuser)
                                                          : ma_user_sensor_msg_set_user(msg, index, user->u.user))) {
            if(MA_OK == (err = (MA_WCHAR_STRING == user->is_domain_wide_str) ? ma_user_sensor_msg_set_wdomain(msg, index, user->d.wdomain)
                                                                             : ma_user_sensor_msg_set_domain(msg, index, user->d.domain))) {
                if(MA_OK == (err = ma_user_sensor_msg_set_logon_state(msg, user->state))) {
                    if(MA_OK == (err = ma_user_sensor_msg_set_event_type(msg, user->type))) {
                        if(MA_OK == (err = ma_user_sensor_msg_set_session_id(msg, index, user->sid))) {
                            MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) setting user attributes to message completed", ma_sensor_type_to_str(MA_SENSOR_TYPE_USER));
                        }
                    }
                }
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_user_sensor_get_all_logged_on_users(ma_user_sensor_t *user, ma_user_sensor_msg_t **msg) {
    if(user && msg) {
        ma_error_t err = MA_OK;
        ma_user_node_t *entry = NULL;
        size_t itr = 0;

        MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) total number of logged on users(%d)", user->name, user->info_map_size);
        if(MA_OK == (err = ma_user_sensor_msg_create(msg, user->info_map_size))) {
            RB_FOREACH(entry, ma_user_tree_s, &user->info_map) {
                if(MA_OK == (err = ma_user_set_attributes_to_msg(&entry->user, itr, *msg)))
                    ++itr;
                else
                    break;
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t display_users(ma_user_node_t **node, void *data) {
    if(node && *node) {
        ma_error_t err = MA_OK;
        ma_user_t *u = &(*node)->user;

        MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "displaying user details");
        MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "user session id(%d)", u->sid);
        MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "logon state(%d)", u->state);
        if(u->is_user_wide_str)
            MA_WLOG(sensor_logger, MA_LOG_SEV_DEBUG, L"user (%ls)", u->u.wuser);
        else
            MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "user (%s)", u->u.user);
        if(u->is_domain_wide_str)
            MA_WLOG(sensor_logger, MA_LOG_SEV_DEBUG, L"domain (%ls)", u->d.wdomain);
        else
            MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "domain (%s)", u->d.domain);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

