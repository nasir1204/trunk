#ifndef MA_NETWORK_SENSOR_INTERNAL_H_INCLUDED
#define MA_NETWORK_SENSOR_INTERNAL_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/ma_macros.h"
#include "ma/sensor/ma_sensor.h"

#define MA_NETWORK_SENSOR_REPEATER_INTERVAL 5 /* in secs */

MA_CPP(extern "C" {)

struct ma_network_sensor_s {
    ma_sensor_t sensor;
    ma_sensor_type_t type;
    ma_msgbus_t *msgbus;
    ma_sensor_state_t state;
    char  name[MA_MAX_NAME];
    void *data;
    ma_bool_t connect_status;
};

/* network sensor virtual methods decleration*/
ma_error_t network_sensor_on_msg(ma_sensor_t *sensor, ma_sensor_msg_t *msg);
ma_error_t network_sensor_get_type(ma_sensor_t *sensor, ma_sensor_type_t *type);
ma_error_t network_sensor_get_id(ma_sensor_t *sensor, const char **id);
ma_error_t network_sensor_start(ma_sensor_t *sensor);
ma_error_t network_sensor_stop(ma_sensor_t *sensor);
ma_error_t network_sensor_release(ma_sensor_t *sensor);
ma_error_t network_sensor_detect(ma_sensor_t *sensor);

ma_bool_t get_network_connectivity_status();
MA_CPP(})

#endif /* MA_NETWORK_SENSOR_INTERNAL_H_INCLUDED */


