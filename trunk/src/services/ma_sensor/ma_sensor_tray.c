#include "ma/sensor/ma_sensor.h"
#include "ma/sensor/ma_sensor_tray.h"
#include "ma/sensor/ma_idle_sensor.h"
#include "ma/sensor/ma_user_sensor.h"
#include "ma/sensor/ma_sensor_msg.h"
#include "ma/scheduler/ma_enumerator.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/defs/ma_sensor_defs.h"
#include "ma/ma_log.h"
#include "uv.h"
#include "ma/ma_msgbus.h"
#include "ma/internal/core/msgbus/ma_msgbus_private.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/sensor/ma_sensor_utils.h"

#include <stdlib.h>
#include <string.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "sensor"

extern ma_logger_t *sensor_logger;

int g_repeater_interval;


struct ma_sensor_tray_s {
    ma_sensor_t sensor;
    ma_enumerator_t enumerator;
    char name[MA_MAX_NAME];
    ma_sensor_state_t state;
    ma_msgbus_t *msgbus;
    ma_sensor_type_t type;
    ma_sensor_t **group;
    size_t no_of_sensors;
    size_t visited;
    size_t capacity;
    uv_loop_t *loop;
    uv_timer_t *detector;
};

static void detector_cb(uv_timer_t *detector, int status);
static void detector_close_cb(uv_handle_t *detector);

static ma_error_t sensor_tray_on_msg(ma_sensor_t *sensor, ma_sensor_msg_t *msg);
static ma_error_t sensor_tray_get_type(ma_sensor_t *sensor, ma_sensor_type_t *type);
static ma_error_t sensor_tray_get_id(ma_sensor_t *sensor, const char **id);
static ma_error_t sensor_tray_start(ma_sensor_t *sensor);
static ma_error_t sensor_tray_stop(ma_sensor_t *sensor);
static ma_error_t sensor_tray_release(ma_sensor_t *sensor);
static ma_error_t sensor_tray_detect(ma_sensor_t *sensor);

static const ma_sensor_methods_t sensor_tray_methods = {
    &sensor_tray_on_msg,
    &sensor_tray_get_type,
    &sensor_tray_get_id,
    &sensor_tray_start,
    &sensor_tray_stop,
    &sensor_tray_release,
    &sensor_tray_detect
};

static ma_error_t add_sensor(ma_sensor_tray_t *tray, ma_sensor_t *sensor);
static ma_error_t current(ma_enumerator_t *enumerator, void *cur);
static ma_error_t move_next(ma_enumerator_t *enumerator, void *next);
static void reset(ma_enumerator_t *enumerator);

const static ma_enumerator_methods_t methods = {
    &current,
    &move_next,
    &reset
};

ma_error_t ma_sensor_tray_create(ma_msgbus_t *msgbus, ma_sensor_tray_t **tray) {
    if(msgbus && tray) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;
        ma_event_loop_t *event_loop = NULL;

        if((*tray = (ma_sensor_tray_t*)calloc(1, sizeof(ma_sensor_tray_t)))) {
            if((event_loop = ma_msgbus_get_event_loop(msgbus))) {
                if((MA_OK == (err = ma_event_loop_get_loop_impl(event_loop, &(*tray)->loop))) && (*tray)->loop) {
                    if(MA_OK == (err = ma_sensor_inc_ref((ma_sensor_t*)(*tray)))) {
                        ma_sensor_init((ma_sensor_t*)(*tray), &sensor_tray_methods, *tray);
                        ma_enumerator_init((ma_enumerator_t*)&(*tray)->enumerator, &methods, *tray);
                        strncpy((*tray)->name, MA_SENSOR_TRAY_STR, strlen(MA_SENSOR_TRAY_STR));
                        (*tray)->state = MA_SENSOR_STATE_NOT_STARTED; 
                        (*tray)->type = MA_SENSOR_TYPE_TRAY;
                        (*tray)->msgbus = msgbus;
                        MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) created successfully", (*tray)->name);
                    }
                }
            }
        }
        if(MA_OK != err) {
            MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) creation failed, last error(%d)",MA_SENSOR_TRAY_STR, err );
            if(*tray) {
                free(*tray); 
                *tray = NULL;
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_sensor_tray_find(ma_sensor_tray_t *tray, ma_sensor_t *sensor, ma_bool_t *is_exist) {
    if(tray && sensor && is_exist) {
        ma_error_t err = MA_OK;

        *is_exist = MA_FALSE;
        if(tray->group) {
            const char *sid = NULL;
            size_t itr = 0;

            (void)ma_sensor_get_id(sensor, &sid);
            MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) searching sensor(%s)..", tray->name, sid);
            for(itr = 0; itr < tray->no_of_sensors; ++itr) {
                if(tray->group[itr] == sensor) {
                    MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) found sensor(%s)", tray->name, sid);
                    *is_exist = MA_TRUE; break;
                }
            }
        } else MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) empty", tray->name);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_sensor_tray_add(ma_sensor_tray_t *tray, ma_sensor_t *sensor) {
    if(tray && sensor) {
        ma_error_t err = MA_ERROR_INVALIDARG;
        const char *sid = NULL;
        ma_sensor_type_t type = MA_SENSOR_TYPE_TRAY;
        ma_bool_t is_exist = MA_FALSE;

        (void)ma_sensor_get_id(sensor, &sid);
        (void)ma_sensor_get_type((ma_sensor_t*)tray, &type);
        if(MA_SENSOR_TYPE_TRAY == type) {
            if((ma_sensor_t*)tray != sensor) {
                if(MA_OK == (err = ma_sensor_tray_find(tray, sensor, &is_exist))) {
                    err = MA_ERROR_OBJECTEXIST;
                    if(MA_FALSE == is_exist) {
                        MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) sensor(%s) adding...", tray->name, sid);
                        if(MA_OK == (err = add_sensor(tray, sensor))) {
                            if(sid)MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) sensor(%s) added successfully", tray->name, sid);
                        } else {
                            if(sid)MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) sensor(%s) add failed, last error(%d)", tray->name, sid, err);
                        }
                    }
                }
            } else
                if(sid)MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) and sensor(%s) are identical cannot be added", tray->name, sid);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_sensor_tray_remove(ma_sensor_tray_t *tray, ma_sensor_t *sensor) {
    if(tray && sensor) {
        ma_error_t  err = MA_OK;
        size_t      itr = 0;
        const char *sid = NULL;
        ma_bool_t found = MA_FALSE;

        (void)ma_sensor_get_id(sensor, &sid);
        if((ma_sensor_t*)tray != sensor) {
            if(sid)MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) sensor(%s) removing...", tray->name, sid);
            for(itr = 0; itr < tray->no_of_sensors; ++itr) {
                if(tray->group[itr] == sensor) {
                    if(MA_OK == (err = ma_sensor_dec_ref(tray->group[itr]))) {
                        found = MA_TRUE;
                        MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "removed sensor from sensor(%s) successfully", tray->name);
                        if(tray->no_of_sensors != (itr+1)) {
                            memcpy(tray->group+itr, tray->group+(itr+1), (sizeof(ma_sensor_t*)*(tray->no_of_sensors - (itr+1))));
                        }
                        --tray->no_of_sensors; ++tray->capacity;
                    }
                    break;
                }
            }
            if(!found){
                if(sid)MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) sensor(%s) cannot be removed", tray->name, sid);
            }
        } else
            if(sid)MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) and sensor(%s) are identical cannot be removed", tray->name, sid);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_sensor_tray_release(ma_sensor_tray_t *tray) {
    if(tray) {
        ma_error_t err = MA_OK;

        if(!((ma_sensor_t*)tray)->ref_count) {
            MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) release completed", tray->name);
            free(tray->group);
            free(tray);
        } else {
            MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) ref count(%d)", tray->name, ((ma_sensor_t*)tray)->ref_count);
            if(MA_OK != (err = ma_sensor_dec_ref((ma_sensor_t*)tray))) {
                MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) dec ref failed, last error(%d)", tray->name, err);
            }
        }

        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t add_sensor(ma_sensor_tray_t *tray, ma_sensor_t *sensor) {
    if(tray && sensor) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;

        if(!tray->group) {
            if((tray->group = (ma_sensor_t**)calloc(1, sizeof(ma_sensor_t*))))
                err = MA_OK;
        } else {
            if(tray->capacity) {
                --tray->capacity;
                err = MA_OK;
            } else {
                if((tray->group = (ma_sensor_t**)realloc(tray->group, sizeof(ma_sensor_t*)* (tray->no_of_sensors + 1))))
                    err = MA_OK;
            }
        }
        if(MA_OK == err) {
            if(MA_OK == (err = ma_sensor_inc_ref(tray->group[tray->no_of_sensors] = sensor))) {
                ++tray->no_of_sensors;
                MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) count(%d)", tray->name, tray->no_of_sensors);
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}


// sensor methods
ma_error_t sensor_tray_on_msg(ma_sensor_t *sensor, ma_sensor_msg_t *msg) {
    if(sensor && msg) {
        ma_error_t err = MA_OK;
        ma_sensor_tray_t *tray = (ma_sensor_tray_t*)sensor;

        if(tray->no_of_sensors) {
            ma_enumerator_t *enumerator = (ma_enumerator_t*)&tray->enumerator;
            ma_sensor_t *item = NULL;
            ma_sensor_type_t item_type = MA_SENSOR_TYPE_NETWORK;
            ma_sensor_type_t msg_type = MA_SENSOR_TYPE_NETWORK;

            (void)ma_sensor_msg_get_type(msg, &msg_type);
            ma_enumerator_reset(enumerator);
            while(MA_OK == ma_enumerator_next(enumerator, &item)) {
                if(MA_OK == (err = ma_sensor_get_type(item, &item_type))) {
                    if(item_type == msg_type) {
                        err = ma_sensor_on_msg(item, msg);
                        break;
                    }
                }
            }
        } else
            MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) empty", tray->name);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t sensor_tray_get_type(ma_sensor_t *sensor, ma_sensor_type_t *type) {
    if(sensor && type) {
        ma_sensor_tray_t *tray = (ma_sensor_tray_t*)sensor;
        *type = tray->type;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t sensor_tray_get_id(ma_sensor_t *sensor, const char **id) {
    if(sensor && id) {
        ma_sensor_tray_t *tray = (ma_sensor_tray_t*)sensor;

        *id = tray->name;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t sensor_tray_start(ma_sensor_t *sensor) {
    if(sensor) {
        ma_sensor_tray_t *tray = (ma_sensor_tray_t*)sensor;
        ma_error_t err = MA_OK;

        if((MA_SENSOR_STATE_NOT_STARTED == tray->state) || (MA_SENSOR_STATE_STOPPED == tray->state)) {
            ma_enumerator_t *enumerator = (ma_enumerator_t*)&tray->enumerator;
            ma_sensor_t *s = NULL;
            const char *sid = NULL;

            ma_enumerator_reset(enumerator);
            MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) starting...count(%d)", tray->name, tray->no_of_sensors);
            while(MA_OK == ma_enumerator_next(enumerator, &s)) {
                (void)ma_sensor_get_id(s, &sid);
                if(MA_OK == (err = ma_sensor_start(s)))
                    (void)ma_sensor_detect(s);
                else
                    MA_LOG(sensor_logger, MA_LOG_SEV_ERROR, "sensor(%s) start failed, last error(%d)", sid ? sid : NULL, err);
            }
            if((tray->detector = (uv_timer_t*)calloc(1, sizeof(uv_timer_t)))) {
                /* initialize detector */
                uv_timer_init(tray->loop, tray->detector);/* smart detector */
                tray->detector->data = tray;
                /* start detector timer */
                uv_timer_start(tray->detector, &detector_cb, g_repeater_interval = MA_DETECTOR_DEFAULT_REPEAT_INTERVAL, 0);
            }
            tray->state = MA_SENSOR_STATE_RUNNING;
            MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) started successfully", tray->name);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t sensor_tray_stop(ma_sensor_t *sensor) {
    if(sensor) {
        ma_sensor_tray_t *tray = (ma_sensor_tray_t*)sensor;
        ma_error_t err = MA_OK;

        if(MA_SENSOR_STATE_RUNNING == tray->state) {
            ma_enumerator_t *enumerator = (ma_enumerator_t*)&tray->enumerator;
            ma_sensor_t *s = NULL;
            const char *sid = NULL;

            ma_enumerator_reset(enumerator);
            MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) stopping...count(%d)", tray->name, tray->no_of_sensors);
            while(MA_OK == ma_enumerator_next(enumerator, &s)) {
                (void)ma_sensor_get_id(s, &sid);
                if(MA_OK != (err = ma_sensor_stop(s)))
                    if(sid)MA_LOG(sensor_logger, MA_LOG_SEV_ERROR, "sensor(%s) stop failed, last error(%d)", sid, err);
            }
            if(tray->detector) {
                if(uv_is_active((uv_handle_t*)tray->detector)) {
                    uv_timer_stop(tray->detector);
                }
                uv_close((uv_handle_t*)tray->detector, &detector_close_cb);
            }
            tray->state = MA_SENSOR_STATE_STOPPED;
            MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) stopped successfully", tray->name);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_sensor_tray_find_by_type(ma_sensor_tray_t *tray, ma_sensor_type_t type, ma_sensor_t **sensor) {
    if(tray && sensor) {
        ma_error_t err = MA_OK;
        size_t itr = 0;
        ma_sensor_type_t stype = MA_SENSOR_TYPE_USER;

        *sensor = NULL;
        for(itr = 0; itr < tray->no_of_sensors; ++itr) {
            if(MA_OK == (err = ma_sensor_get_type(tray->group[itr], &stype))) {
                if(type == stype) {
                    err = ma_sensor_inc_ref((*sensor = tray->group[itr])); break;
                }
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t sensor_tray_release(ma_sensor_t *sensor) {
    if(sensor) {
        ma_sensor_tray_t *tray = (ma_sensor_tray_t*)sensor;
        ma_error_t err = MA_OK;

        if(!((ma_sensor_t*)tray)->ref_count) {
            ma_enumerator_t *enumerator = (ma_enumerator_t*)&tray->enumerator;
            ma_sensor_t *s = NULL;

            ma_enumerator_reset(enumerator);
            MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) releasing...count(%d)", tray->name, tray->no_of_sensors);
            while(MA_OK == ma_enumerator_next(enumerator, &s)) {
                if(MA_OK != (err = ma_sensor_dec_ref(s))) {
                    MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) items release failed, last error(%d)", ma_sensor_type_to_str(MA_SENSOR_TYPE_TRAY), err);
                }
            }
        } else {
            err = ma_sensor_dec_ref((ma_sensor_t*)tray);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t sensor_tray_detect(ma_sensor_t *sensor) {
    if(sensor) {
        ma_sensor_tray_t *tray = (ma_sensor_tray_t*)sensor;
        ma_error_t err = MA_OK;

        if(MA_SENSOR_STATE_RUNNING == tray->state) {
            ma_sensor_t *sensor = NULL;

            ma_enumerator_reset((ma_enumerator_t*)&tray->enumerator);
            while(MA_OK == ma_enumerator_next((ma_enumerator_t*)&tray->enumerator, &sensor)) {
                if(MA_OK == (err = ma_sensor_detect(sensor))) {

                }
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}


// enumerator methods
ma_error_t current(ma_enumerator_t *enumerator, void *cur) {
    if(enumerator && cur) {
        ma_error_t err = MA_ERROR_OUTOFBOUNDS;
        ma_sensor_tray_t *tray = (ma_sensor_tray_t*)enumerator->data;
        ma_sensor_t **sensor = (ma_sensor_t**)cur;
        //const char *id = NULL;

        *sensor = NULL;
        if((tray->no_of_sensors > 0) && (tray->visited < tray->no_of_sensors)) {
            *sensor = tray->group[tray->visited];
            //(void)ma_sensor_get_id(*sensor, &id); err = MA_OK;
            //if(id)MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "current sensor(%s)", id);
        }

        return err;
    }
    //MA_LOG(sensor_logger, MA_LOG_SEV_ERROR, "last error(%d)", MA_ERROR_INVALIDARG);
    return MA_ERROR_INVALIDARG;
}

ma_error_t move_next(ma_enumerator_t *enumerator, void *next) {
    if(enumerator && next) {
        ma_error_t err = MA_OK;
        ma_sensor_tray_t *tray = (ma_sensor_tray_t*)enumerator->data;
        ma_sensor_t **sensor = (ma_sensor_t**)next;
        //const char *id = NULL;
        
        *sensor = NULL;
        if((0 == tray->no_of_sensors) || (tray->visited == tray->no_of_sensors)) {
            err = MA_ERROR_NO_MORE_ITEMS;
            //MA_LOG(sensor_logger, MA_LOG_SEV_ERROR, "sensors exhaust, last error(%d)", err);
        } else if(tray->visited > tray->no_of_sensors) {
            err = MA_ERROR_OUTOFBOUNDS;
            //MA_LOG(sensor_logger, MA_LOG_SEV_ERROR, "sensor out of bounds, last error(%d)", err);
        } else {
            *sensor = tray->group[tray->visited];
            //(void)ma_sensor_get_id(*sensor, &id);
            //if(id)MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "next sensor(%s)", id);
            ++tray->visited;
        }

        return err;
    }
    //MA_LOG(sensor_logger, MA_LOG_SEV_ERROR, "last error(%d)", MA_ERROR_INVALIDARG);
    return MA_ERROR_INVALIDARG;
}

void reset(ma_enumerator_t *enumerator) {
    if(enumerator) {
        ((ma_sensor_tray_t*)(enumerator->data))->visited = 0; //reset
        //MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor iterator reset");
    }
}

/* smart detector callbacks */
void detector_cb(uv_timer_t *detector, int status) {
    if(detector) {
        ma_sensor_tray_t *tray = (ma_sensor_tray_t*)detector->data;

        if(tray) {
            (void)ma_sensor_detect((ma_sensor_t*)tray);
            //MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) next detect intervals(%d)", tray->name, g_repeater_interval);
            uv_timer_set_repeat(tray->detector, g_repeater_interval * 1000);
            uv_timer_again(tray->detector);
        }
    }
}

void detector_close_cb(uv_handle_t *detector) {
    if(detector) {
        MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor tray detector is closed successfully");
        free(detector);
    }
}
