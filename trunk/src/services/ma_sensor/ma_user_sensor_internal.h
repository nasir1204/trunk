#ifndef MA_USER_SENSOR_INTERNAL_H_INCLUDED
#define MA_USER_SENSOR_INTERNAL_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/ma_macros.h"
#include "ma/sensor/ma_sensor.h"
#include "ma/sensor/ma_sensor_msg.h"
#include "ma/scheduler/ma_enumerator.h"
#include "uv-private/tree.h"

MA_CPP(extern "C" {)

typedef struct ma_user_tree_s ma_user_tree_t;
typedef struct ma_user_node_t ma_user_node_t;
typedef struct ma_user_s ma_user_t;

struct ma_user_s {
    union {
        char user[MA_MAX_NAME];
        wchar_t wuser[MA_MAX_NAME];
    }u;
    union {
        char domain[MA_MAX_NAME];
        wchar_t wdomain[MA_MAX_NAME];
    }d;
    ma_uint32_t is_user_wide_str:1;
    ma_uint32_t is_domain_wide_str:1;
    ma_uint32_t sid;
    ma_user_logged_state_t state;
    ma_sensor_event_type_t type;
};

struct ma_user_node_t {
    RB_ENTRY(ma_user_node_t) tree_entry;
    ma_user_t user;
};

typedef ma_error_t (*MAP_FOR_EACH_CALLBACK)(ma_user_node_t **node, void *data);
RB_HEAD (ma_user_tree_s, ma_user_node_t);
int map_comparator(ma_user_node_t *left, ma_user_node_t *right);
ma_error_t map_insert(ma_user_tree_t *map, ma_user_t *user);
ma_user_node_t* map_find(ma_user_tree_t *info_map, ma_user_t * user);
ma_error_t map_for_each(ma_user_node_t **node, MAP_FOR_EACH_CALLBACK cb, void *data);
ma_error_t map_clear(ma_user_node_t *node);
ma_error_t map_erase(ma_user_tree_t *map, ma_user_t *user);
ma_error_t ma_user_set_attributes_from_msg(ma_user_sensor_msg_t *msg, size_t index, ma_user_t *u);
ma_error_t ma_user_set_attributes_to_msg(ma_user_t *user, size_t index, ma_user_sensor_msg_t *msg);

struct ma_user_sensor_s {
    ma_sensor_t sensor;
    ma_user_tree_t info_map;
    size_t info_map_size;
    ma_sensor_type_t type;
    ma_msgbus_t *msgbus;
    ma_sensor_state_t state;
    char  name[MA_MAX_NAME];
    void *data;
};

ma_error_t user_sensor_on_msg(ma_sensor_t *sensor, ma_sensor_msg_t *msg);
ma_error_t user_sensor_get_type(ma_sensor_t *sensor, ma_sensor_type_t *type);
ma_error_t user_sensor_get_id(ma_sensor_t *sensor, const char **id);
ma_error_t user_sensor_start(ma_sensor_t *sensor);
ma_error_t user_sensor_stop(ma_sensor_t *sensor);
ma_error_t user_sensor_release(ma_sensor_t *sensor);
ma_error_t user_sensor_detect(ma_sensor_t *sensor);

RB_GENERATE_STATIC(ma_user_tree_s, ma_user_node_t, tree_entry, map_comparator);
MA_CPP(})

#endif /* MA_USER_SENSOR_INTERNAL_H_INCLUDED */


