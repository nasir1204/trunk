#include "ma/internal/services/sensor/ma_sensor_service.h"
#include "ma/internal/services/ma_service.h"

#include "ma/ma_msgbus.h"
#include "ma/ma_message.h"
#include "ma/internal/utils/context/ma_context.h"
#include "ma/internal/ma_strdef.h"

#include "ma/sensor/ma_sensor.h"
#include "ma/sensor/ma_sensor_tray.h"
#include "ma/sensor/ma_sensor_msg.h"
#include "ma/ma_log.h"

#include "ma/sensor/ma_user_sensor.h"
#include "ma/sensor/ma_sensor_tray.h"
#include "ma/sensor/ma_idle_sensor.h"
#include "ma/sensor/ma_system_sensor.h"
#include "ma/sensor/ma_power_sensor.h"
#include "ma/sensor/ma_network_sensor.h"


#include <stdlib.h>
#include <string.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "sensor service"

struct ma_sensor_service_s {
    ma_service_t                        base;

    ma_context_t                        *context;

    ma_msgbus_server_t                  *server;

    /* sensor tray */
    ma_sensor_tray_t                     *sensor_tray;

    
};

static ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint);
static ma_error_t service_start(ma_service_t *service);
static ma_error_t service_stop(ma_service_t *service);
static void service_destroy(ma_service_t *service);


static const ma_service_methods_t methods = {
    &service_configure,
    &service_start,
    &service_stop,
    &service_destroy
};	

static ma_error_t ma_sensor_service_cb(ma_msgbus_server_t *server, ma_message_t *msg, void *cb_data, ma_msgbus_client_request_t *c_request);

/* sensor handler(s) */
static ma_error_t user_sensor_handler(ma_sensor_service_t *service, ma_message_t *req, ma_message_t *resp);
static ma_error_t power_sensor_handler(ma_sensor_service_t *service, ma_message_t *req, ma_message_t *resp);
static ma_error_t idle_sensor_handler(ma_sensor_service_t *service, ma_message_t *req, ma_message_t *resp);
static ma_error_t network_sensor_handler(ma_sensor_service_t *service, ma_message_t *req, ma_message_t *resp);
static ma_error_t system_sensor_handler(ma_sensor_service_t *service, ma_message_t *req, ma_message_t *resp);
static ma_error_t misc_sensor_handler(ma_sensor_service_t *service, ma_message_t *req, ma_message_t *resp);


ma_error_t ma_sensor_service_create(const char *service_name, ma_service_t **service) {
    if(service_name && service) {
        ma_sensor_service_t *self = (ma_sensor_service_t *)calloc(1, sizeof(ma_sensor_service_t));
        if(!self) return MA_ERROR_OUTOFMEMORY;

        ma_service_init((ma_service_t*)self, &methods, service_name, self);
        *service = &self->base;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_bool_t check_context_validity(ma_context_t *context){	
	/*Will see if we need this*/
	return MA_TRUE;
}

static ma_error_t initialize_sensors(ma_sensor_service_t *self);
ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint){
    if(service && context) {
        ma_error_t err = MA_OK;
        ma_sensor_service_t *self = (ma_sensor_service_t *)service;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(context);
        ma_msgbus_t *msgbus = MA_CONTEXT_GET_MSGBUS(context);

        self->context = context;

        if(MA_SERVICE_CONFIG_NEW == hint && check_context_validity(context)) {
             if(MA_OK == (err = ma_sensor_tray_create(msgbus, &self->sensor_tray))) {
                (void)ma_sensor_set_logger(logger);

                if(MA_OK != (err = initialize_sensors(self))) 
                    MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to initialize sensors %d, continuing without sensors.",err);

                if(MA_OK == (err = ma_msgbus_server_create(MA_CONTEXT_GET_MSGBUS(context), MA_SENSOR_SERVICE_NAME_STR, &self->server))) {
                    if(MA_OK == (err = ma_msgbus_server_setopt(self->server, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC))) {
                        ma_context_add_object_info(self->context, MA_OBJECT_SENSOR_NAME_STR,(void *const*)&self->sensor_tray);
                        MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_DEBUG, "sensor service(%s) configuration successful", MA_SENSOR_SERVICE_NAME_STR);
                        return MA_OK;
                    }
                    (void)ma_msgbus_server_release(self->server);
                }
                else
                    MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_ERROR, "sensor service(%s) configuration failed, %d.", MA_SENSOR_SERVICE_NAME_STR, err);
                (void)ma_sensor_tray_release(self->sensor_tray);
             }
            return err;
        }
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

void service_destroy(ma_service_t *service) {
    if(service) {
        ma_error_t err = MA_OK;
        ma_sensor_service_t *self = (ma_sensor_service_t *)service;

        if(self->sensor_tray) ma_sensor_tray_release(self->sensor_tray);
        if(self->server && (MA_OK != (err = ma_msgbus_server_release(self->server)))) {
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "msgbus server release failed");
        }
        free(service);
    }
}

static ma_error_t post_sensor_agent_start_event(ma_sensor_service_t *self);
ma_error_t service_start(ma_service_t *service) {
    if(service) {
        ma_error_t err = MA_OK;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(((ma_sensor_service_t*)service)->context);

        if(MA_OK == (err = ma_msgbus_server_setopt( ((ma_sensor_service_t*)service)->server, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO))) {
            if(MA_OK == (err = ma_msgbus_server_start(((ma_sensor_service_t*)service)->server, ma_sensor_service_cb, service))) {
                if(MA_OK == (err = ma_sensor_start((ma_sensor_t *)((ma_sensor_service_t*)service)->sensor_tray))) {
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "posting agent start event");
                    post_sensor_agent_start_event((ma_sensor_service_t*)service);
                    MA_LOG(logger, MA_LOG_SEV_INFO, "sensor service(%s) started successfully", MA_SENSOR_SERVICE_NAME_STR);
                }
            }
        }
        if(MA_OK != err) {
            MA_LOG(logger, MA_LOG_SEV_CRITICAL, "sensor service(%s) start failed, last error(%d)", MA_SENSOR_SERVICE_NAME_STR, err);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_sensor_service_get_version(ma_uint32_t *version) {
    if(version) {
        *version = 0x05020100; //TODO fetch version 
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t service_stop(ma_service_t *service) {
    if(service) {
        ma_error_t err = MA_OK;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(((ma_sensor_service_t*)service)->context);

        if(MA_OK == (err = ma_sensor_stop((ma_sensor_t *)((ma_sensor_service_t*)service)->sensor_tray))) {
            if(MA_OK == (err = ma_msgbus_server_stop(((ma_sensor_service_t*)service)->server))) {
                MA_LOG(logger, MA_LOG_SEV_INFO, "sensor service(%s) stopped successfully", MA_SENSOR_SERVICE_NAME_STR);
            }
        }
        if(MA_OK != err) {
            MA_LOG(logger, MA_LOG_SEV_CRITICAL, "sensor service(%s) failed to stop, last error(%d)", MA_SENSOR_SERVICE_NAME_STR, err);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}


// sensor message handler(s)
ma_error_t user_sensor_handler(ma_sensor_service_t *service, ma_message_t *req, ma_message_t *resp) {
    if(service && req && resp) {
        ma_user_sensor_msg_t *user_msg = NULL;
        ma_sensor_msg_t *sensor_msg = NULL;
        ma_error_t err = MA_OK;
        const char *reply_status = MA_SENSOR_REPLY_STATUS_FAILED;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(((ma_sensor_service_t*)service)->context);
        ma_sensor_t *sensor = (ma_sensor_t *)((ma_sensor_service_t*)service)->sensor_tray;

        MA_LOG(logger, MA_LOG_SEV_DEBUG, "sensor service posting user sensor message");
        if(MA_OK == (err = ma_user_sensor_msg_create(&user_msg, 1))) {
            //TODO:: parse req message and get payload
            if(MA_OK == (err = ma_sensor_msg_create_from_user_sensor_msg(user_msg, &sensor_msg))) {
                if(MA_OK == (err = ma_sensor_on_msg(sensor, sensor_msg))) {
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "sensor service posted user sensor message successfully");
                    reply_status = MA_SENSOR_REPLY_STATUS_SUCCESS;
                } else
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "sensor service posted user sensor message failed, last error(%d)", err);
                (void)ma_sensor_msg_release(sensor_msg);
            }
            (void)ma_user_sensor_msg_release(user_msg);
        }

        return ma_message_set_property(resp, MA_SENSOR_REPLY_STATUS_STR, reply_status);;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t power_sensor_handler(ma_sensor_service_t *service, ma_message_t *req, ma_message_t *resp) {
    if(service && req && resp) {
        ma_power_sensor_msg_t *power_msg = NULL;
        ma_sensor_msg_t *sensor_msg = NULL;
        ma_error_t err = MA_OK;
        const char *reply_status = MA_SENSOR_REPLY_STATUS_FAILED;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(((ma_sensor_service_t*)service)->context);
        ma_sensor_t *sensor = (ma_sensor_t *)((ma_sensor_service_t*)service)->sensor_tray;

        MA_LOG(logger, MA_LOG_SEV_DEBUG, "sensor service posting power sensor message");
        if(MA_OK == (err = ma_power_sensor_msg_create(&power_msg, 1))) {
            //TODO:: parse req message and get payload
            if(MA_OK == (err = ma_sensor_msg_create_from_power_sensor_msg(power_msg, &sensor_msg))) {
                if(MA_OK == (err = ma_sensor_on_msg(sensor, sensor_msg))) {
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "sensor service posted power sensor message successfully");
                    reply_status = MA_SENSOR_REPLY_STATUS_SUCCESS;
                } else
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "sensor service posted power sensor message failed, last error(%d)", err);
                (void)ma_sensor_msg_release(sensor_msg);
            }
            (void)ma_power_sensor_msg_release(power_msg);
        }

        return ma_message_set_property(resp, MA_SENSOR_REPLY_STATUS_STR, reply_status);;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t idle_sensor_handler(ma_sensor_service_t *service, ma_message_t *req, ma_message_t *resp) {
    if(service && req && resp) {
        ma_idle_sensor_msg_t *idle_msg = NULL;
        ma_sensor_msg_t *sensor_msg = NULL;
        ma_error_t err = MA_OK;
        const char *reply_status = MA_SENSOR_REPLY_STATUS_FAILED;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(((ma_sensor_service_t*)service)->context);
        ma_sensor_t *sensor = (ma_sensor_t *)((ma_sensor_service_t*)service)->sensor_tray;

        MA_LOG(logger, MA_LOG_SEV_DEBUG, "sensor service posting idle sensor message");
        if(MA_OK == (err = ma_idle_sensor_msg_create(&idle_msg, 1))) {
            //TODO:: parse req message and get payload
            if(MA_OK == (err = ma_sensor_msg_create_from_idle_sensor_msg(idle_msg, &sensor_msg))) {
                if(MA_OK == (err = ma_sensor_on_msg(sensor, sensor_msg))) {
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "sensor service posted idle sensor message successfully");
                    reply_status = MA_SENSOR_REPLY_STATUS_SUCCESS;
                } else
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "sensor service posted idle sensor message failed, last error(%d)", err);
                (void)ma_sensor_msg_release(sensor_msg);
            }
            (void)ma_idle_sensor_msg_release(idle_msg);
        }

        return ma_message_set_property(resp, MA_SENSOR_REPLY_STATUS_STR, reply_status);;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t network_sensor_handler(ma_sensor_service_t *service, ma_message_t *req, ma_message_t *resp) {
    if(service && req && resp) {
        ma_network_sensor_msg_t *network_msg = NULL;
        ma_sensor_msg_t *sensor_msg = NULL;
        ma_error_t err = MA_OK;
        const char *reply_status = MA_SENSOR_REPLY_STATUS_FAILED;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(((ma_sensor_service_t*)service)->context);
        ma_sensor_t *sensor = (ma_sensor_t *)((ma_sensor_service_t*)service)->sensor_tray;

        MA_LOG(logger, MA_LOG_SEV_DEBUG, "sensor service posting network sensor message");
        if(MA_OK == (err = ma_network_sensor_msg_create(&network_msg, 1))) {
            //TODO:: parse req message and get payload
            if(MA_OK == (err = ma_sensor_msg_create_from_network_sensor_msg(network_msg, &sensor_msg))) {
                if(MA_OK == (err = ma_sensor_on_msg(sensor, sensor_msg))) {
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "sensor service posted network sensor message successfully");
                    reply_status = MA_SENSOR_REPLY_STATUS_SUCCESS;
                } else
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "sensor service posted network sensor message failed, last error(%d)", err);
                (void)ma_sensor_msg_release(sensor_msg);
            }
            (void)ma_network_sensor_msg_release(network_msg);
        }

        return ma_message_set_property(resp, MA_SENSOR_REPLY_STATUS_STR, reply_status);;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t system_sensor_handler(ma_sensor_service_t *service, ma_message_t *req, ma_message_t *resp) {
    if(service && req && resp) {
        ma_system_sensor_msg_t *system_msg = NULL;
        ma_sensor_msg_t *sensor_msg = NULL;
        ma_error_t err = MA_OK;
        const char *reply_status = MA_SENSOR_REPLY_STATUS_FAILED;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(((ma_sensor_service_t*)service)->context);
        ma_sensor_t *sensor = (ma_sensor_t *)((ma_sensor_service_t*)service)->sensor_tray;

        MA_LOG(logger, MA_LOG_SEV_DEBUG, "sensor service posting system sensor message");
        if(MA_OK == (err = ma_system_sensor_msg_create(&system_msg, 1))) {
            //TODO:: parse req message and get payload
            if(MA_OK == (err = ma_sensor_msg_create_from_system_sensor_msg(system_msg, &sensor_msg))) {
                if(MA_OK == (err = ma_sensor_on_msg(sensor, sensor_msg))) {
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "sensor service posted system sensor message successfully");
                    reply_status = MA_SENSOR_REPLY_STATUS_SUCCESS;
                } else
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "sensor service posted system sensor message failed, last error(%d)", err);
                (void)ma_sensor_msg_release(sensor_msg);
            }
            (void)ma_system_sensor_msg_release(system_msg);
        }

        return ma_message_set_property(resp, MA_SENSOR_REPLY_STATUS_STR, reply_status);;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t misc_sensor_handler(ma_sensor_service_t *service, ma_message_t *req, ma_message_t *resp) {
    if(service && req && resp) {
        ma_sensor_msg_t *sensor_msg = NULL;
        ma_error_t err = MA_OK;
        const char *reply_status = MA_SENSOR_REPLY_STATUS_FAILED;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(((ma_sensor_service_t*)service)->context);
        ma_sensor_t *sensor = (ma_sensor_t *)((ma_sensor_service_t*)service)->sensor_tray;
        
        MA_LOG(logger, MA_LOG_SEV_DEBUG, "sensor service posting sensor tray message");
        if(MA_OK == (err = ma_sensor_msg_create(&sensor_msg))) {
            //TODO:: parse req message and get payload
            if(MA_OK == (err = ma_sensor_on_msg(sensor, sensor_msg))) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "sensor service posted sensor tray message successfully");
                reply_status = MA_SENSOR_REPLY_STATUS_SUCCESS;
            } else
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "sensor service posted sensor tray message failed, last error(%d)", err);
            (void)ma_sensor_msg_release(sensor_msg);
        }

        return ma_message_set_property(resp, MA_SENSOR_REPLY_STATUS_STR, reply_status);;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_sensor_service_cb(ma_msgbus_server_t *server, ma_message_t *msg, void *cb_data, ma_msgbus_client_request_t *c_request) {
    if(server && msg && cb_data) {
        ma_error_t err = MA_OK;
        const char *msg_type = NULL;
        ma_message_t *response = NULL;
        ma_sensor_service_t *service = (ma_sensor_service_t*)cb_data;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(((ma_sensor_service_t*)service)->context);
        
        if(MA_OK == (err = ma_message_get_property(msg, MA_SENSOR_SERVICE_MSG_TYPE, &msg_type))) {
            if(MA_OK == (err = ma_message_create(&response))) {
                if(!strcmp(msg_type, MA_SENSOR_TYPE_USER_STR)) {
                    if(MA_OK == (err = user_sensor_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client user notification request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_SENSOR_TYPE_POWER_STR)) {
                    if(MA_OK == (err = power_sensor_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client power notification request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_SENSOR_TYPE_IDLE_STR)) {
                    if(MA_OK == (err = idle_sensor_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client idle notification request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_SENSOR_TYPE_NETWORK_STR)) {
                    if(MA_OK == (err = network_sensor_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client network notification request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_SENSOR_TYPE_SYSTEM_STR)) {
                    if(MA_OK == (err = system_sensor_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client system notification request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_SENSOR_TYPE_TRAY_STR)) {
                    if(MA_OK == (err = misc_sensor_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client misc notification request failed, last error(%d)", err);
                        }
                    }
                } else {
                }

                if(MA_OK != ma_message_release(response)) {
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client request message release failed");
                }
            }
        }
        return err;
    }
    return MA_OK;
}


static ma_error_t initialize_sensors(ma_sensor_service_t *self) {
    if(self) {
        ma_error_t err = MA_OK;
        ma_idle_sensor_t *idle = NULL;
        ma_user_sensor_t *user = NULL;
        ma_system_sensor_t *system = NULL;
        ma_power_sensor_t *power = NULL;
        ma_network_sensor_t *network = NULL;
        ma_sensor_t *sensor = NULL;

        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);
        ma_msgbus_t *msgbus = MA_CONTEXT_GET_MSGBUS(self->context);

        /* idle sensor creation */
        if(MA_OK == (err = ma_idle_sensor_create(msgbus, &idle))) {
            sensor = (ma_sensor_t*)idle;
            if(MA_OK == (err = ma_sensor_tray_add(self->sensor_tray, sensor))) {
                (void)ma_sensor_dec_ref(sensor);
            } else
                MA_LOG(logger, MA_LOG_SEV_ERROR, "failed to add idle sensor to sensor tray, errorcode<%d>", err);
        } else
            MA_LOG(logger, MA_LOG_SEV_ERROR, "failed to create idle sensor from service context, errorcode<%d>", err);

        /* user sensor creation */
        if(MA_OK == (err = ma_user_sensor_create(msgbus, &user))) {
            sensor = (ma_sensor_t*)user;
            if(MA_OK == (err = ma_sensor_tray_add(self->sensor_tray, sensor))) {
                (void)ma_sensor_dec_ref(sensor);
            } else
                MA_LOG(logger, MA_LOG_SEV_ERROR, "failed to add user sensor to sensor tray, errorcode<%d>", err);
        } else
            MA_LOG(logger, MA_LOG_SEV_ERROR, "failed to create user sensor from service context, errorcode<%d>", err);

        /* system sensor creation */
        if(MA_OK == (err = ma_system_sensor_create(msgbus, &system))) {
            sensor = (ma_sensor_t*)system;
            if(MA_OK == (err = ma_sensor_tray_add(self->sensor_tray, sensor))) {
                (void)ma_sensor_dec_ref(sensor);
            } else
                MA_LOG(logger, MA_LOG_SEV_ERROR, "failed to add system sensor to sensor tray, errorcode<%d>", err);
        } else
            MA_LOG(logger, MA_LOG_SEV_ERROR, "failed to create system sensor from service context, errorcode<%d>", err);

        /* power sensor creation */
        if(MA_OK == (err = ma_power_sensor_create(msgbus, &power))) {
            sensor = (ma_sensor_t*)power;
            if(MA_OK == (err = ma_sensor_tray_add(self->sensor_tray, sensor))) {
                (void)ma_sensor_dec_ref(sensor);
            } else
                MA_LOG(logger, MA_LOG_SEV_ERROR, "failed to add power sensor to sensor tray, errorcode<%d>", err);
        } else
            MA_LOG(logger, MA_LOG_SEV_ERROR, "failed to create power sensor from service context, errorcode<%d>", err);

        /* network sensor creation */
        if(MA_OK == (err = ma_network_sensor_create(msgbus, &network))) {
            sensor = (ma_sensor_t*)network;
            if(MA_OK == (err = ma_sensor_tray_add(self->sensor_tray, sensor))) {
                (void)ma_sensor_dec_ref(sensor);
            } else
                MA_LOG(logger, MA_LOG_SEV_ERROR, "failed to add network sensor to sensor tray, errorcode<%d>", err);
        } else
            MA_LOG(logger, MA_LOG_SEV_ERROR, "failed to create network sensor from service context, errorcode<%d>", err);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t  ma_sensor_service_post_sensor_msg(ma_sensor_service_t *self, ma_sensor_msg_t *msg) {
    if(self && msg) {
        ma_error_t err = MA_OK;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);
        MA_LOG(logger, MA_LOG_SEV_DEBUG, "posting sensor message..");
        if(MA_OK != (err = ma_sensor_on_msg((ma_sensor_t *)self->sensor_tray, msg))) {
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "posting sensor message failed, errorcode<%d>", err);
        }
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t post_sensor_agent_start_event(ma_sensor_service_t *self) {
    if(self) {
        ma_error_t err = MA_OK;
        ma_system_sensor_msg_t *sys_msg = NULL;
        ma_sensor_msg_t *msg = NULL;

        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);

        if(MA_OK == (err = ma_system_sensor_msg_create(&sys_msg, 1))) {
            (void)ma_system_sensor_msg_set_event_type(sys_msg, MA_SYSTEM_SENSOR_EVENT_STARTUP);
            if(MA_OK == (err = ma_sensor_msg_create_from_system_sensor_msg(sys_msg, &msg))) {
                if(MA_OK != (err = ma_sensor_service_post_sensor_msg(self, msg))) {
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "posting sensor startup event message failed, errorcode<%d>", err);
                }
                (void)ma_sensor_msg_release(msg);
            }
            (void)ma_system_sensor_msg_release(sys_msg);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}
