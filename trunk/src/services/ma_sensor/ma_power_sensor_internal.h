#ifndef MA_POWER_SENSOR_INTERNAL_H_INCLUDED
#define MA_POWER_SENSOR_INTERNAL_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/ma_macros.h"
#include "ma/sensor/ma_sensor.h"

MA_CPP(extern "C" {)
struct ma_power_sensor_s {
    ma_sensor_t sensor;
    ma_sensor_type_t type;
    ma_msgbus_t *msgbus;
    ma_sensor_state_t state;
    char  name[MA_MAX_NAME];
};

/* power sensor virtual methods decleration(s) */
ma_error_t power_sensor_on_msg(ma_sensor_t *sensor, ma_sensor_msg_t *msg);
ma_error_t power_sensor_get_type(ma_sensor_t *sensor, ma_sensor_type_t *type);
ma_error_t power_sensor_get_id(ma_sensor_t *sensor, const char **id);
ma_error_t power_sensor_start(ma_sensor_t *sensor);
ma_error_t power_sensor_stop(ma_sensor_t *sensor);
ma_error_t power_sensor_release(ma_sensor_t *sensor);
MA_CPP(})

#endif /* MA_POWER_SENSOR_INTERNAL_H_INCLUDED */


