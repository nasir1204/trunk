#include "ma/sensor/ma_sensor.h"
#include "ma/internal/defs/ma_sensor_defs.h"
#include "ma/ma_log.h"
#include "ma/sensor/ma_user_sensor.h"
#include "../ma_user_sensor_internal.h"
#include "ma/internal/ma_macros.h"
#include "ma/sensor/ma_sensor_msg.h"
#include "ma/sensor/ma_sensor_utils.h"
#include "ma_monitor_users.h"

#include <stdlib.h>
#include <string.h>

#include <windows.h>
#include <tchar.h>
#include <ntsecapi.h>
#include <WtsApi32.h>
#include <lm.h>
#pragma comment(lib, "WtsApi32.lib")

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "sensor"

extern ma_logger_t *sensor_logger;

static ma_error_t ma_get_logged_on_users(ma_user_sensor_t *sensor, ma_user_sensor_msg_t **msg, size_t *no_of_users);

ma_error_t user_sensor_on_msg(ma_sensor_t *sensor, ma_sensor_msg_t *msg) {
    if(sensor && msg) {
        ma_error_t err = MA_OK;
        ma_user_sensor_t *user = (ma_user_sensor_t*)sensor;
        ma_user_sensor_msg_t *user_msg = NULL;

        if(MA_SENSOR_STATE_RUNNING == user->state) {
            ma_sensor_event_type_t event_type = MA_SYSTEM_SENSOR_EVENT_STOP;
            const char *event_str = NULL;
            ma_message_t *pub_msg = NULL;
            ma_bool_t exist = MA_FALSE;
            ma_variant_t *msg_var = NULL;
            ma_user_sensor_msg_t *add_on_msg = NULL;
            ma_user_sensor_msg_t *exist_msg = NULL;
            ma_user_node_t *node = NULL;
            ma_user_t u = {0};
            size_t no_of_msgs = 0;
            size_t index = 0;

            if(MA_OK == (err = ma_sensor_msg_get_user_sensor_msg(msg, &user_msg))) {
                (void)ma_user_sensor_msg_get_event_type(user_msg, &event_type);
                (void)ma_user_sensor_msg_get_size(user_msg, &no_of_msgs);
                MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) received message(%s) event_type(%d)", user->name, ma_sensor_event_type_to_str(event_type), event_type);
                if(MA_USER_SENSOR_LOG_OFF_EVENT == event_type) {
                    ma_bool_t logoff_user_exist = MA_FALSE;
                    ma_user_node_t *node = NULL;

                    for(index = 0; index < no_of_msgs; ++index) {
                        (void)ma_user_sensor_msg_get_session_id(user_msg, index, &u.sid);
                        if((node = map_find(&user->info_map, &u))) {
                            node->user.is_user_wide_str ? (void)ma_user_sensor_msg_set_wuser(user_msg, index, node->user.u.wuser)
                                                        : (void)ma_user_sensor_msg_set_user(user_msg, index, node->user.u.user);
                            node->user.is_domain_wide_str ? (void)ma_user_sensor_msg_set_wdomain(user_msg, index, node->user.d.wdomain)
                                                            : (void)ma_user_sensor_msg_set_domain(user_msg, index, node->user.d.domain);
                            logoff_user_exist = MA_TRUE;
                        }
                    }
                    if(!logoff_user_exist)
                        return ma_user_sensor_msg_release(user_msg);
                }
                if(MA_OK == (err = ma_message_create(&pub_msg))) {
                    if(MA_OK == (err = convert_user_sensor_msg_to_variant(user_msg, &msg_var))) {
                        if(MA_OK == (err = ma_message_set_payload(pub_msg, msg_var))) {
                            MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) publishing event(%s)..", user->name, ma_sensor_event_type_to_str(event_type));
                            if(MA_OK != (err = ma_sensor_publish_msg(user->msgbus, user->name, ma_sensor_event_type_to_str(event_type), pub_msg))) {
                                MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) publishing event(%s) failed last error(%d)", user->name, ma_sensor_event_type_to_str(event_type), err);
                            }
                        } else MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) publishing event(%s) failed to set payload", user->name, ma_sensor_event_type_to_str(event_type));
                        (void)ma_variant_release(msg_var);
                    } else
                        MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) received message(%s) converting message to variant failed last error(%d)", user->name, ma_sensor_event_type_to_str(event_type), err);
                    (void)ma_message_release(pub_msg);
                }
                for(index = 0; index < no_of_msgs; ++index) {
                    if(MA_USER_SENSOR_LOG_ON_EVENT == event_type) {
                        MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) adding user to map", user->name);
                        if(MA_OK == (err = ma_user_set_attributes_from_msg(user_msg, index, &u))) {
                            if(!map_find(&user->info_map, &u)) {
                                ma_user_t *user_details = NULL;

                                if((user_details = (ma_user_t*)calloc(1, sizeof(ma_user_t)))) {
                                    (void)ma_user_set_attributes_from_msg(user_msg, index, user_details);
                                    user_details->type = event_type;
                                    if(MA_OK == (err = map_insert(&user->info_map, user_details))) {
                                        ++user->info_map_size;
                                    }
                                }
                            }
                        }
                    } else if(MA_USER_SENSOR_LOG_OFF_EVENT == event_type) {
                        MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) removing user from map", user->name);
                        if(MA_OK == (err = ma_user_set_attributes_from_msg(user_msg, index, &u))) {
                            if(map_find(&user->info_map, &u)) {
                                u.type = event_type;
                                if(MA_OK == (err = map_erase(&user->info_map, &u))) {
                                    MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) removed user successfully", user->name);
                                    --user->info_map_size;
                                }
                            } else
                                MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) user not exist in map", user->name);
                        }
                    } else {
                        /* ignore other than log on/off event */
                    }
                } /* end of for loop */
                (void)ma_user_sensor_msg_release(user_msg);
            }
        } else
            MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) is stopped", user->name);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t user_sensor_get_type(ma_sensor_t *sensor, ma_sensor_type_t *type) {
    if(sensor && type) {
        ma_user_sensor_t *user = (ma_user_sensor_t*)sensor;
        *type = user->type;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t user_sensor_get_id(ma_sensor_t *sensor, const char **id) {
    if(sensor && id) {
        ma_user_sensor_t *user = (ma_user_sensor_t*)sensor;
        *id = user->name;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t user_sensor_start(ma_sensor_t *sensor) {
    if(sensor) {
        ma_user_sensor_t *user = (ma_user_sensor_t*)sensor;
        ma_error_t err = MA_OK;

        if((MA_SENSOR_STATE_STOPPED == user->state) || (MA_SENSOR_STATE_NOT_STARTED == user->state)) {
            ma_user_sensor_msg_t *msg = NULL;
            size_t no_of_users = 0;
            ma_monitor_users_t *monitor = NULL;

            //if(MA_OK == (err = ma_monitor_users_create(user, &monitor))) {
            //    ma_monitor_users_init_sense(monitor);
            //}
            MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) checking for already logged on users...", user->name);
            if(MA_OK == (err = ma_get_logged_on_users(user, &msg, &no_of_users))) {
                MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) number of logged on users(%d) found", user->name, no_of_users);
                if(msg) {
                    /* enumerate logged on users once on startup */
                    (void)ma_user_sensor_msg_set_startup_flag(msg, MA_TRUE);
                    if(no_of_users) {
                        ma_message_t *pub_msg = NULL;
                        ma_variant_t *msg_var = NULL;

                        if(MA_OK == (err = ma_message_create(&pub_msg))) {
                            if(MA_OK == (err = convert_user_sensor_msg_to_variant(msg, &msg_var))) {
                                if(MA_OK == (err = ma_message_set_payload(pub_msg, msg_var))) {
                                    MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) publishing event(%s)..", user->name, ma_sensor_event_type_to_str(MA_USER_SENSOR_LOG_ON_EVENT));
                                    if(MA_OK != (err = ma_sensor_publish_msg(user->msgbus, user->name, ma_sensor_event_type_to_str(MA_USER_SENSOR_LOG_ON_EVENT), pub_msg))) {
                                        MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) publishing event(%s) failed last error(%d)", user->name, ma_sensor_event_type_to_str(MA_USER_SENSOR_LOG_ON_EVENT), err);
                                    }
                                }
                                (void)ma_variant_release(msg_var);
                            }
                            (void)ma_message_release(pub_msg);
                        }
                    }
                    (void)ma_user_sensor_msg_release(msg);
                }
            } else {
                if(msg)ma_user_sensor_msg_release(msg);
            }
            MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) started successfully", user->name);
            user->state = MA_SENSOR_STATE_RUNNING;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t user_sensor_stop(ma_sensor_t *sensor) {
    if(sensor) {
        ma_user_sensor_t *user = (ma_user_sensor_t*)sensor;
        ma_error_t err = MA_OK;

        if(MA_SENSOR_STATE_RUNNING == user->state) {
            //if(MA_OK == (err = ma_monitor_users_deinit_sense((ma_monitor_users_t*)user->data))) {
            //    if(user->data) {
            //        (void)ma_monitor_users_release((ma_monitor_users_t*)user->data);
            //    }
            //}
            user->state = MA_SENSOR_STATE_STOPPED;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t user_sensor_release(ma_sensor_t *sensor) {
    if(sensor) {
        ma_error_t err = MA_OK;

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t user_sensor_detect(ma_sensor_t *sensor) {
    if(sensor) {
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_get_logged_on_users(ma_user_sensor_t *sensor, ma_user_sensor_msg_t **user_msg, size_t *cnt) {
    if(sensor) {
        PLUID sessions = {0};
        ULONG sessionCount = {0};
        NTSTATUS rc = 0;
        ma_error_t err = MA_OK;
        ma_user_sensor_msg_t *msg = NULL;
        size_t no_of_users = 0;
        ma_user_node_t *node = NULL;

        if(!(rc = LsaEnumerateLogonSessions(&sessionCount, &sessions))) {
            if(sessionCount) {
                if(MA_OK == (err = ma_user_sensor_msg_create(&msg, sessionCount))) {
                    size_t itr = 0;
                    
                    (void)ma_user_sensor_msg_set_logon_state(msg, MA_USER_LOGGED_STATE_ON);
                    for(itr = 0; itr < sessionCount; ++itr) {
                        SECURITY_LOGON_SESSION_DATA *sessionData = NULL;

                        if(!(rc = LsaGetLogonSessionData(&sessions[itr], &sessionData))) {
                            if(sessionData->LogonType == Interactive || sessionData->LogonType == RemoteInteractive || sessionData->LogonType == CachedInteractive || sessionData->LogonType == CachedRemoteInteractive) {
                                //if(!sessionData->Session) 
                                {
                                    ma_user_t *user = NULL;
                                    
                                    if((user = (ma_user_t*)calloc(1, sizeof(ma_user_t)))) {
                                        wcsncpy(user->d.wdomain, sessionData->LogonDomain.Buffer, wcslen(sessionData->LogonDomain.Buffer));
                                        user->is_domain_wide_str = MA_WCHAR_STRING;
                                        wcsncpy(user->u.wuser, sessionData->UserName.Buffer, wcslen(sessionData->UserName.Buffer));
                                        user->is_user_wide_str = MA_WCHAR_STRING;
                                        user->type = MA_USER_SENSOR_LOG_ON_EVENT;
                                        user->state = MA_USER_LOGGED_STATE_ON;
                                        user->sid = sessionData->Session;

                                        if(!map_find(&sensor->info_map, user)) {
                                            if(MA_OK == (err = map_insert(&sensor->info_map, user))) {
                                                ++sensor->info_map_size;
                                                if(MA_OK == (err = ma_user_set_attributes_to_msg(user, no_of_users, msg))) {
                                                    MA_WLOG(sensor_logger, MA_LOG_SEV_DEBUG, L"sensor(ma.user->sensor) logged on user(%ls) domain(%ls) session(%d) orig sessionId(%d)", user->u.wuser, user->d.wdomain, user->sid, sessionData->Session);
                                                    ++no_of_users;
                                                }
                                            }
                                        } else {
                                            free(user);
                                            user = NULL;
                                        }
                                    }
                                }
                            }
                            (void)LsaFreeReturnBuffer(sessionData);
                        }
                    }
                    (void)ma_user_sensor_msg_set_size(*user_msg = msg, (*cnt = no_of_users));
                }
            }
            (void)LsaFreeReturnBuffer(sessions);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}


int map_comparator(ma_user_node_t *l, ma_user_node_t *r) {
    return (l && r) ? memcmp(&l->user.sid, &r->user.sid, sizeof(ma_uint32_t)) : 0;
}
