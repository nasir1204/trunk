#include "ma/sensor/ma_sensor_msg_utils.h"

#include <stdlib.h>
#include <string.h>
#include <Sensapi.h>

ma_error_t ma_sensor_get_power_sensor_msg(ma_power_sensor_msg_t **msg){
    if(msg){
        ma_error_t err = MA_OK;
        ma_power_sensor_msg_t *pwr_msg = NULL;
    
        if(MA_OK == (err = ma_power_sensor_msg_create(&pwr_msg, 1))){            
            SYSTEM_POWER_STATUS power_status = {0};
            err = MA_ERROR_APIFAILED;
            if(GetSystemPowerStatus(&power_status)){
                ma_power_status_t p_status = MA_POWER_STATUS_OFFLINE;
                ma_battery_charge_status_t b_status = MA_BATTERY_CHARGE_STATUS_HIGH;

                switch(power_status.ACLineStatus) {
                    case 0x00: p_status = MA_POWER_STATUS_OFFLINE; break;
                    case 0x01: p_status = MA_POWER_STATUS_ONLINE; break;
                    case 0xff: p_status = MA_POWER_STATUS_UNKNOWN; break;
                }
            
                switch(power_status.BatteryFlag) {
                    case 0x01: b_status = MA_BATTERY_CHARGE_STATUS_HIGH; break;
                    case 0x02: b_status = MA_BATTERY_CHARGE_STATUS_LOW; break;
                    case 0x04: b_status = MA_BATTERY_CHARGE_STATUS_CRITICAL; break;
                    case 0x08: b_status = MA_BATTERY_CHARGE_STATUS_CHARGING; break;
                    case 0x80: b_status = MA_BATTERY_CHARGE_STATUS_NO_BATTERY; break;
                    case 0xff: b_status = MA_BATTERY_CHARGE_STATUS_UNKNOWN; break;
                }

                (void)ma_power_sensor_msg_set_power_status(pwr_msg, 0, p_status);
                (void)ma_power_sensor_msg_set_battery_charge_status(pwr_msg, 0, b_status);            
                (void)ma_power_sensor_msg_set_battery_lifetime(pwr_msg, 0, power_status.BatteryLifeTime);
                (void)ma_power_sensor_msg_set_battery_percent(pwr_msg, 0, power_status.BatteryLifePercent);
                *msg = pwr_msg;
                return MA_OK;
            }
        }
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_bool_t get_network_connectivity_status() {
    ma_bool_t alive = 0;
    DWORD network = 0;

    alive = IsNetworkAlive(&network);
    return (GetLastError() ==0) ? alive : 0;
}

ma_error_t ma_sensor_get_network_sensor_msg(ma_network_sensor_msg_t **msg){
    if(msg){
        ma_error_t err = MA_OK;
        ma_network_sensor_msg_t *nw_msg = NULL;
    
        if(MA_OK == (err = ma_network_sensor_msg_create(&nw_msg, 1))){            
            (void)ma_network_sensor_msg_set_connectivity_status(nw_msg, get_network_connectivity_status() ? MA_NETWORK_CONNECTIVITY_STATUS_CONNECTED : MA_NETWORK_CONNECTIVITY_STATUS_DISCONNECTED);                        
            *msg = nw_msg;
        }
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

