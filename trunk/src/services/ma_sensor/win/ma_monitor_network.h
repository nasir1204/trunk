#ifndef MA_MONITOR_NETWORK_H_INCLUDED
#define MA_MONITOR_NETWORK_H_INCLUDED

#if _MSC_VER < 1000
#error Required MSC version > 1000
#endif // _MSC_VER > 1000

#undef STRICT
#define STRICT
#ifndef _WIN32_WINNT
#define _WIN32_WINNT 0x05010000
#endif

#include "ma/sensor/ma_sensor.h"
#include "ma/sensor/ma_network_sensor.h"
#include "ma/ma_log.h"

#pragma comment(lib, "iphlpapi.lib")
#include <eventsys.h>
#include <Sensevts.h>

#define MA_MCAFEE_NETWORK_STATUS_EVENT_STR     TEXT("MCAFEE_NETWORK_STATUS_EVENT")
extern ma_logger_t *sensor_logger;


MA_CPP(extern "C" {)

typedef struct ma_monitor_network_s ma_monitor_network_t;

ma_error_t ma_monitor_network_create(ma_network_sensor_t *sensor, ma_monitor_network_t **monitor);
ma_error_t ma_monitor_network_release(ma_monitor_network_t *monitor);
ma_error_t ma_monitor_network_set_sensor(ma_monitor_network_t *m, ma_network_sensor_t *sensor);
ma_error_t ma_monitor_network_get_sensor(ma_monitor_network_t *m, ma_network_sensor_t **sensor);
ma_error_t ma_monitor_network_init_sense(ma_monitor_network_t *pnetwork);
ma_error_t ma_monitor_network_deinit_sense(ma_monitor_network_t *pnetwork);

MA_CPP(})



#endif
