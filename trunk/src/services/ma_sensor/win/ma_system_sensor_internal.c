#include "ma/sensor/ma_sensor.h"
#include "ma/internal/defs/ma_sensor_defs.h"
#include "ma/ma_log.h"
#include "ma/sensor/ma_system_sensor.h"
#include "../ma_system_sensor_internal.h"
#include "ma/internal/ma_macros.h"
#include "ma/sensor/ma_sensor_msg.h"
#include "ma/sensor/ma_sensor_utils.h"
#include "ma/internal/utils/time/ma_time.h"

#include <stdlib.h>
#include <string.h>
#include <Windows.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "sensor"

extern ma_logger_t *sensor_logger;


MA_CPP(extern "C" {)


ma_bool_t detect_system_startup(ma_sensor_t *sensor) {
    if(sensor) {
        ma_system_sensor_t *sys_sensor = (ma_system_sensor_t*)sensor;
        DWORD uptime = GetTickCount();
        DWORD ma_max_uptime = MA_MAX_SYSTEM_STARTUP_TIME * 60000; /* 5 mintues */
        ma_error_t err = MA_OK;

        return (uptime && (uptime <= ma_max_uptime)) ? MA_TRUE : MA_FALSE;
    }
    return MA_FALSE;
}


MA_CPP(})