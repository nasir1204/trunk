#ifndef MA_MONITOR_USERS_H_INCLUDED
#define MA_MONITOR_USERS_H_INCLUDED

#if _MSC_VER < 1000
#error Required MSC version > 1000
#endif // _MSC_VER > 1000

#undef STRICT
#define STRICT
#ifndef _WIN32_WINNT
#define _WIN32_WINNT 0x05010000
#endif

#include "ma/sensor/ma_sensor.h"
#include "ma/sensor/ma_user_sensor.h"
#include "ma/ma_log.h"

#pragma comment(lib, "iphlpapi.lib")
#include <eventsys.h>
#include <Sensevts.h>

extern ma_logger_t *sensor_logger;

MA_CPP(extern "C" {)

typedef struct ma_monitor_users_s ma_monitor_users_t;

ma_error_t ma_monitor_users_create(ma_user_sensor_t *sensor, ma_monitor_users_t **monitor);
ma_error_t ma_monitor_users_release(ma_monitor_users_t *monitor);
ma_error_t ma_monitor_users_set_sensor(ma_monitor_users_t *monitor, ma_user_sensor_t *sensor);
ma_error_t ma_monitor_users_get_sensor(ma_monitor_users_t *monitor, ma_user_sensor_t **sensor);
ma_error_t ma_monitor_users_init_sense(ma_monitor_users_t *monitor);
ma_error_t ma_monitor_users_deinit_sense(ma_monitor_users_t *monitor);

MA_CPP(})



#endif
