#include "ma/sensor/ma_sensor.h"
#include "ma/internal/defs/ma_sensor_defs.h"
#include "ma/ma_log.h"
#include "ma/sensor/ma_idle_sensor.h"
#include "../ma_idle_sensor_internal.h"
#include "ma/internal/ma_macros.h"
#include "ma/sensor/ma_sensor_msg.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_message.h"
#include "ma/sensor/ma_sensor_utils.h"

#include <stdlib.h>
#include <string.h>
#include <Windows.h>
#include <Winuser.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "sensor"

extern ma_logger_t *sensor_logger;
extern int g_repeater_interval;

typedef struct ma_idle_check_data_s ma_idle_check_data_t;

struct ma_idle_check_data_s {
    POINT ptCursorPrev;
    POINT ptCursorCurr;
    SHORT keyStatePrev[256];
    SHORT keyStateCurr[256];
};



MA_CPP(extern "C" {)

ma_error_t idle_sensor_on_msg(ma_sensor_t *sensor, ma_sensor_msg_t *msg) {
    if(sensor && msg) {
        ma_error_t err = MA_OK;
        ma_idle_sensor_t *idle_sensor = (ma_idle_sensor_t*)sensor;
        ma_idle_sensor_msg_t *idle_msg = NULL;

        if(MA_SENSOR_STATE_RUNNING == idle_sensor->state) {
            ma_sensor_event_type_t event_type = MA_SYSTEM_SENSOR_EVENT_STOP;
            const char *event_str = NULL;
            ma_message_t *pub_msg = NULL;
            size_t no_of_msgs = 0;
            size_t index = 0;
            ma_variant_t *msg_var = NULL;

            if(MA_OK == (err = ma_sensor_msg_get_idle_sensor_msg(msg, &idle_msg))) {
                (void)ma_idle_sensor_msg_get_event_type(idle_msg, &event_type);
                (void)ma_idle_sensor_msg_get_size(idle_msg, &no_of_msgs);
                MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) received message(%s) event_type(%d)", idle_sensor->name, ma_sensor_event_type_to_str(event_type), event_type);
                if(MA_OK == (err = ma_message_create(&pub_msg))) {
                    if(MA_OK == (err = convert_idle_sensor_msg_to_variant(idle_msg, &msg_var))) {
                        if(MA_OK == (err = ma_message_set_payload(pub_msg, msg_var))) {
                            MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) publishing event(%s)..", idle_sensor->name, ma_sensor_event_type_to_str(event_type));
                            if(MA_OK != (err = ma_sensor_publish_msg(idle_sensor->msgbus, idle_sensor->name, ma_sensor_event_type_to_str(event_type), pub_msg))) {
                                MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) publishing event(%s) failed last error(%d)", idle_sensor->name, ma_sensor_event_type_to_str(event_type), err);
                            }
                        } else MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) publishing event(%s) failed to set payload, last error(%d)", idle_sensor->name, ma_sensor_event_type_to_str(event_type), err);
                        (void)ma_variant_release(msg_var);
                    } else
                        MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) received message(%s) converting message to variant failed, last error(%d)", idle_sensor->name, ma_sensor_event_type_to_str(event_type), err);
                    (void)ma_message_release(pub_msg);
                }
                (void)ma_idle_sensor_msg_release(idle_msg);
            }
        }

		return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t idle_sensor_get_type(ma_sensor_t *sensor, ma_sensor_type_t *type) {
    if(sensor && type) {
        ma_idle_sensor_t *idle = (ma_idle_sensor_t*)sensor;
        *type = idle->type;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t idle_sensor_get_id(ma_sensor_t *sensor, const char **id) {
    if(sensor && id) {
        ma_idle_sensor_t *idle = (ma_idle_sensor_t*)sensor;
        *id = idle->name;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t idle_sensor_start(ma_sensor_t *sensor) {
    if(sensor) {
        ma_idle_sensor_t *idle = (ma_idle_sensor_t*)sensor;

        if((MA_SENSOR_STATE_STOPPED == idle->state) || (MA_SENSOR_STATE_NOT_STARTED == idle->state)) {
            idle->state = MA_SENSOR_STATE_RUNNING;
            MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) started successfully", idle->name);
        }

        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t idle_sensor_stop(ma_sensor_t *sensor) {
    if(sensor) {
        ma_idle_sensor_t *idle = (ma_idle_sensor_t*)sensor;

        if(MA_SENSOR_STATE_RUNNING == idle->state) {
            idle->state = MA_SENSOR_STATE_STOPPED;
            MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) stopped successfully", idle->name);
        }

        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t idle_sensor_release(ma_sensor_t *sensor) {
    if(sensor) {
        ma_error_t err = MA_OK;
        ma_idle_sensor_t *idle = (ma_idle_sensor_t*)sensor;

        if(MA_SENSOR_STATE_RUNNING == idle->state) {
            if(MA_OK != (err = ma_sensor_stop(sensor))) {
                MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) stop failed, last error(%d)", idle->name, err);
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t idle_sensor_detect(ma_sensor_t *sensor) {
    if(sensor) {
        ma_idle_sensor_t *idle_sensor = (ma_idle_sensor_t*)sensor;
        ma_error_t err = MA_OK;
        int idle_wait = 0;

        if(MA_OK == (err = idle_sensor_get_idle_time(idle_sensor, &idle_wait))) {
            if(idle_wait > 0) {
                ma_idle_sensor_msg_t *idle_msg = NULL;

                if(MA_OK == (err = ma_idle_sensor_msg_create(&idle_msg, 1))) {
                    (void)ma_idle_sensor_msg_set_event_type(idle_msg, MA_IDLE_SENSOR_IDLE_TIME_CHANGE_EVENT);
                    if(MA_OK == (err = ma_idle_sensor_msg_set_idle_wait(idle_msg, 0, idle_wait))) {
                        ma_sensor_msg_t *msg = NULL;

                        if(MA_OK == (err = ma_sensor_msg_create_from_idle_sensor_msg(idle_msg, &msg))) {
                            MA_LOG(sensor_logger, MA_LOG_SEV_ERROR, "sensor(%s) publishing idle time(%d)", idle_sensor->name, idle_wait);
                            if(MA_OK != (err = ma_sensor_on_msg((ma_sensor_t*)idle_sensor, msg))) {
                                MA_LOG(sensor_logger, MA_LOG_SEV_ERROR, "sensor(%s) publishing idle time message failed, last error(%d)", idle_sensor->name, err);
                            }
                            (void)ma_sensor_msg_release(msg);
                        }
                    }
                    (void)ma_idle_sensor_msg_release(idle_msg);
                }
            }
        }
        /* adjust repeater */
        g_repeater_interval = (g_repeater_interval > MA_IDLE_SENSOR_REPEATER_INTERVAL) ? MA_IDLE_SENSOR_REPEATER_INTERVAL : g_repeater_interval;


        return err;
    }
    return MA_ERROR_INVALIDARG;
}

/* gets system inactive elapsed time */
ma_error_t idle_sensor_get_idle_time(ma_idle_sensor_t *idle, int *idle_mins) {
    if(idle && idle_mins) {
        *idle_mins = 0;
#if 0
        ma_bool_t key_active =  MA_FALSE;
        ma_bool_t mouse_active = MA_FALSE;
        static ma_idle_check_data_t    idle_check_data;
        static time_t           time_prev = 0;
        time_t                  time_curr;
        static int sys_idle_time = 0;
        int i = 0;

        if(!GetCursorPos(&idle_check_data.ptCursorCurr)) {
            MA_LOG(sensor_logger, MA_LOG_SEV_ERROR, "getcursorpos api failed, last error(%d)", GetLastError());
        }
        mouse_active = memcmp(&idle_check_data.ptCursorPrev, &idle_check_data.ptCursorCurr, sizeof(POINT)) != 0;
        for (i = (VK_RBUTTON + 1); i < 256; i++) {
            idle_check_data.keyStateCurr[i] = GetAsyncKeyState(i);
            if (idle_check_data.keyStateCurr[i] != idle_check_data.keyStatePrev[i]) {
                key_active = TRUE;
            }
        }
        time(&time_curr);
        if (!mouse_active && !key_active) {
            // system is idle, nothing change
            if (time_prev == 0) {
                time_prev = time_curr;
            }
            sys_idle_time += (time_curr - time_prev);
        } else {
            // something changed
            sys_idle_time = 0;
            memcpy(&idle_check_data.ptCursorPrev, &idle_check_data.ptCursorCurr, sizeof(POINT));
            for (i = 0; i < 256; i++) {
                idle_check_data.keyStatePrev[i] = idle_check_data.keyStateCurr[i];
            }
        }
        time_prev = time_curr;
        *idle_mins = sys_idle_time/60;
        MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) idle time(%d)", idle->name, *idle_mins);
#endif
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

MA_CPP(})
