#include "ma_monitor_network.h"
#include "ma/internal/ma_macros.h"
#include "ma/sensor/ma_sensor_msg.h"
#include "../ma_network_sensor_internal.h"

#include <Windows.h>
#include <stdlib.h>
#include <string.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "sensor"

static STDMETHODIMP QueryInterface(__RPC__in ISensNetwork * This, REFIID iid, void ** ppv);
static ULONG STDMETHODCALLTYPE AddRef(__RPC__in ISensNetwork * This);
static ULONG STDMETHODCALLTYPE Release(__RPC__in ISensNetwork * This);
static HRESULT STDMETHODCALLTYPE GetTypeInfoCount(__RPC__in ISensNetwork * This, unsigned int FAR* pctinfo);
static HRESULT STDMETHODCALLTYPE GetTypeInfo(__RPC__in ISensNetwork * This, unsigned int iTInfo, LCID  lcid, ITypeInfo FAR* FAR*  ppTInfo);
static HRESULT STDMETHODCALLTYPE GetIDsOfNames(__RPC__in ISensNetwork * This, REFIID riid, OLECHAR FAR* FAR* rgszNames, unsigned int cNames, LCID lcid, DISPID FAR* rgDispId);
static HRESULT STDMETHODCALLTYPE Invoke(__RPC__in ISensNetwork * This, DISPID dispIdMember, REFIID riid, LCID lcid, WORD wFlags, DISPPARAMS FAR* pDispParams, VARIANT FAR* parResult, EXCEPINFO FAR* pExcepInfo, unsigned int FAR* puArgErr);
static HRESULT STDMETHODCALLTYPE ConnectionMade(__RPC__in ISensNetwork * This, BSTR bstrConnection,ULONG ulType, LPSENS_QOCINFO lpQOCInfo);
static HRESULT STDMETHODCALLTYPE ConnectionMadeNoQOCInfo(__RPC__in ISensNetwork * This,BSTR bstrConnection,ULONG ulType);
static HRESULT STDMETHODCALLTYPE ConnectionLost(__RPC__in ISensNetwork *This, BSTR bstrConnection, ULONG ulType);
static void ma_monitor_network_change_status(ma_monitor_network_t *m, int bConnected);
static ma_error_t ma_monitor_network_init(ma_monitor_network_t *m);

static ISensNetworkVtbl methods= {
    &QueryInterface,
    &AddRef,
    &Release,
    &GetTypeInfoCount,
    &GetTypeInfo,
    &GetIDsOfNames,
    &Invoke,
    &ConnectionMade,
    &ConnectionMadeNoQOCInfo,
    &ConnectionLost,
    0,
    0
};

struct ma_monitor_network_s {
    ISensNetwork base;
    long ref_count;
    HANDLE m_hMcAfeeNetworkEvent;
    ma_network_sensor_t *sensor;
    IEventSystem *m_pIEventSystem;
};

ma_error_t ma_monitor_network_create(ma_network_sensor_t *sensor, ma_monitor_network_t **monitor) {
    if(sensor && monitor) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;

        if((*monitor = (ma_monitor_network_t*)calloc(1, sizeof(ma_monitor_network_t)))) {
            ((ISensNetwork*)(*monitor))->lpVtbl = &methods;
            (void)ma_sensor_inc_ref((ma_sensor_t*)((*monitor)->sensor = sensor));
            (void)ma_monitor_network_init(*monitor); err = MA_OK;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_monitor_network_destroy(ma_monitor_network_t *monitor) {
    if(monitor) {
        if(monitor->m_hMcAfeeNetworkEvent) CloseHandle(monitor->m_hMcAfeeNetworkEvent);
        if(monitor->sensor)(void)ma_sensor_dec_ref((ma_sensor_t*)monitor->sensor);
        free(monitor);
        return MA_OK;
     }
     return MA_ERROR_INVALIDARG;
}

ma_error_t ma_monitor_network_release(ma_monitor_network_t *monitor) {
    if(monitor) {
        Release(&monitor->base);
        return MA_OK ;
    }  
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_monitor_network_init(ma_monitor_network_t *m) {
    if(m) {
        m->m_hMcAfeeNetworkEvent = CreateEvent(NULL, TRUE, FALSE, MA_MCAFEE_NETWORK_STATUS_EVENT_STR);
        ma_monitor_network_change_status(m,(m->sensor)->connect_status = get_network_connectivity_status());
        m->ref_count = 1;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

void ma_monitor_network_change_status(ma_monitor_network_t *m, int connect_status) {
    ma_network_sensor_msg_t *network_msg = NULL;
    ma_error_t err = MA_OK;
    
    m->sensor->connect_status = connect_status;
    if(MA_OK == (err = ma_network_sensor_msg_create(&network_msg, 1))) {
        ma_sensor_msg_t *msg = NULL;
        if(MA_OK == (err = ma_sensor_msg_create_from_network_sensor_msg(network_msg, &msg))) {
            (void)ma_network_sensor_msg_set_event_type(network_msg, MA_NETWORK_SENSOR_CONNECTIVITY_STATUS_CHANGE_EVENT);
            if(connect_status) {
                SetEvent(m->m_hMcAfeeNetworkEvent);
                (void)ma_network_sensor_msg_set_connectivity_status(network_msg, MA_NETWORK_CONNECTIVITY_STATUS_CONNECTED);
                MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) network connected", ma_sensor_type_to_str(MA_SENSOR_TYPE_NETWORK));
            }
            else
            {
                ResetEvent(m->m_hMcAfeeNetworkEvent);
                (void)ma_network_sensor_msg_set_connectivity_status(network_msg, MA_NETWORK_CONNECTIVITY_STATUS_DISCONNECTED);
                MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) network disconnected", ma_sensor_type_to_str(MA_SENSOR_TYPE_NETWORK));
            }
            if(MA_OK != (err = network_sensor_on_msg((ma_sensor_t*)m->sensor, msg)))
                MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) posting message failed, last error(%d)", ma_sensor_type_to_str(MA_SENSOR_TYPE_NETWORK), err);
            (void)ma_sensor_msg_release(msg);
        }
        (void)ma_network_sensor_msg_release(network_msg);
    }
}

STDMETHODIMP QueryInterface(__RPC__in ISensNetwork * This, REFIID iid, void ** ppv) {
    if (IsEqualIID(iid, &IID_IUnknown) || IsEqualIID(iid, &IID_IDispatch) || IsEqualIID(iid, &IID_ISensNetwork)) {
        ma_monitor_network_t *m = (ma_monitor_network_t*)This;
        *ppv = m;
        AddRef(This);
        return S_OK;
    }
    *ppv = NULL;
    return E_NOINTERFACE;
}

ULONG STDMETHODCALLTYPE AddRef(__RPC__in ISensNetwork * This) {
    if(This) {
        ma_monitor_network_t *m = (ma_monitor_network_t*)This;
        return InterlockedIncrement(&m->ref_count);
    }
    return 0;
}

ULONG STDMETHODCALLTYPE Release(__RPC__in ISensNetwork * This) {
    if(This) {
        ma_monitor_network_t *m = (ma_monitor_network_t*)This;
        ULONG result = InterlockedDecrement(&m->ref_count);
        if(!result) ma_monitor_network_destroy(m);
        return result;
    }
    return 0;
}

HRESULT STDMETHODCALLTYPE GetTypeInfoCount(__RPC__in ISensNetwork * This, unsigned int FAR* pctinfo) { 
    return E_NOTIMPL;
}

HRESULT STDMETHODCALLTYPE GetTypeInfo(__RPC__in ISensNetwork * This, unsigned int iTInfo, LCID  lcid, ITypeInfo FAR* FAR*  ppTInfo) {
    return E_NOTIMPL;
}

HRESULT STDMETHODCALLTYPE GetIDsOfNames(__RPC__in ISensNetwork * This, REFIID riid, OLECHAR FAR* FAR* rgszNames, unsigned int cNames, LCID lcid, DISPID FAR* rgDispId) { 
    return E_NOTIMPL; 
}

HRESULT STDMETHODCALLTYPE Invoke(__RPC__in ISensNetwork * This, DISPID dispIdMember, REFIID riid, LCID lcid, WORD wFlags, DISPPARAMS FAR* pDispParams, VARIANT FAR* parResult, EXCEPINFO FAR* pExcepInfo, unsigned int FAR* puArgErr) {
    return E_NOTIMPL; 
}

// ISensNetwork methods:
HRESULT STDMETHODCALLTYPE ConnectionMade(__RPC__in ISensNetwork * This, BSTR bstrConnection,ULONG ulType, LPSENS_QOCINFO lpQOCInfo) {
    ma_monitor_network_change_status((ma_monitor_network_t*)This, 1);
    return S_OK;
}

HRESULT STDMETHODCALLTYPE ConnectionMadeNoQOCInfo(__RPC__in ISensNetwork * This,BSTR bstrConnection,ULONG ulType) {
    return S_OK;
}

HRESULT STDMETHODCALLTYPE ConnectionLost(__RPC__in ISensNetwork *This, BSTR bstrConnection, ULONG ulType) {
    ma_monitor_network_change_status((ma_monitor_network_t*)This, 0);
    return S_OK;
}


ma_error_t ma_monitor_network_init_sense(ma_monitor_network_t *m_network) {
    if(m_network) {
        ma_error_t err = MA_OK;
        IEventSystem *pIEventSystem = NULL;
        IEventSubscription *pIEventSubscription = NULL;
        HRESULT res = S_OK;
        int i =0;
        wchar_t* methods[] = {L"ConnectionMade",L"ConnectionMadeNoQOCInfo",L"ConnectionLost"};
        wchar_t* names[]   = {L"Connection Made 1",L"Connection Made 2",L"ConnectionLost"};
        wchar_t* subids[]  = {L"{cd1dcbd6-a14d-4823-a0d2-8473afde360f}",L"{a82f0e80-1305-400c-ba56-375ae04264a1}",
                              L"{45233130-b6c3-44fb-a6af-487c47cee611}"};
        
        //if(S_OK == (res = CoInitializeEx(NULL,COINIT_MULTITHREADED))) {
            if((S_OK == (res = CoCreateInstance(&CLSID_CEventSystem, 0,CLSCTX_SERVER, &IID_IEventSystem,(void**)&pIEventSystem))) && pIEventSystem) {
                m_network->m_pIEventSystem = pIEventSystem;
                for(i=0; i<3; i++) {
                    if(SUCCEEDED(res = CoCreateInstance(&CLSID_CEventSubscription,0, CLSCTX_SERVER, &IID_IEventSubscription,(LPVOID*)&pIEventSubscription))) {
                        MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(ma.network.sensor) com objected created successfully");
                        if(S_OK != (res = pIEventSubscription->lpVtbl->put_EventClassID(pIEventSubscription, OLESTR("{D5978620-5B9F-11D1-8DD2-00AA004ABD5E}"))))
                            MA_LOG(sensor_logger, MA_LOG_SEV_ERROR, "sensor(ma.network.sensor) setting event class ID failed, last error(%d)", res);
                        if(S_OK != (res = pIEventSubscription->lpVtbl->put_SubscriberInterface(pIEventSubscription, (IUnknown*) m_network)))
                            MA_LOG(sensor_logger, MA_LOG_SEV_ERROR, "sensor(ma.network.sensor) setting subscriber interface failed, last error(%d)", res);
                        if(S_OK != (res = pIEventSubscription->lpVtbl->put_MethodName(pIEventSubscription, methods[i])))
                            MA_LOG(sensor_logger, MA_LOG_SEV_ERROR, "sensor(ma.network.sensor) setting method name failed, last error(%d)", res);
                        if(S_OK != (res = pIEventSubscription->lpVtbl->put_SubscriptionName(pIEventSubscription, names[i])))
                            MA_LOG(sensor_logger, MA_LOG_SEV_ERROR, "sensor(ma.network.sensor) setting subscription name failed, last error(%d)", res);
                        if(S_OK != (res = pIEventSubscription->lpVtbl->put_SubscriptionID(pIEventSubscription, subids[i])))
                            MA_LOG(sensor_logger, MA_LOG_SEV_ERROR, "sensor(ma.network.sensor) setting subscription ID failed, last error(%d)", res);
                        if(S_OK != (res = pIEventSystem->lpVtbl->Store(pIEventSystem, PROGID_EventSubscription,(IUnknown*)pIEventSubscription)))
                            MA_LOG(sensor_logger, MA_LOG_SEV_ERROR, "sensor(ma.network.sensor) setting store failed, last error(%d)", res);
                        pIEventSubscription->lpVtbl->Release(pIEventSubscription);
                        pIEventSubscription=0;
                    }
                    else {
                        MA_LOG(sensor_logger, MA_LOG_SEV_ERROR, "sensor(ma.network.sensor) com object creation failed, last error(%d)", res);
                        err = MA_ERROR_APIFAILED;
                    }
                }
            } else {
                MA_LOG(sensor_logger, MA_LOG_SEV_ERROR, "sensor(ma.network.sensor) com eventsubscription object creation failed, last error(%d)", res); err = MA_ERROR_APIFAILED;
            }
        /*} else {
            MA_LOG(sensor_logger, MA_LOG_SEV_ERROR, "sensor(ma.network.sensor) com object init failed, last error(%d)", res);
            err = MA_ERROR_APIFAILED;
        }*/

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_monitor_network_deinit_sense(ma_monitor_network_t *m_network) {
    if(m_network && m_network->m_pIEventSystem) {
        int errorIndex = 0, i =0;
        HRESULT res = S_OK;
        wchar_t* subids[]={L"{cd1dcbd6-a14d-4823-a0d2-8473afde360f}",L"{a82f0e80-1305-400c-ba56-375ae04264a1}",
                           L"{45233130-b6c3-44fb-a6af-487c47cee611}"};

        for (i = 0; i < 3; i++) {
            wchar_t strSubID[MA_MAX_BUFFER_LEN] = {0};

            wcsncpy(strSubID, L"SubscriptionID=", wcslen(L"SubscriptionID="));
            wcscat(strSubID, subids[i]);
            if(S_OK != (res = m_network->m_pIEventSystem->lpVtbl->Remove(m_network->m_pIEventSystem, PROGID_EventSubscription, (BSTR)strSubID,&errorIndex)))
                MA_LOG(sensor_logger, MA_LOG_SEV_ERROR, "sensor(ma.sensor.network) event system remove failed, last error(%d)", res);
        }
        m_network->m_pIEventSystem->lpVtbl->Release(m_network->m_pIEventSystem);
        //CoUninitialize();

        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_monitor_network_set_sensor(ma_monitor_network_t *m, ma_network_sensor_t *sensor) {
    if(m && sensor) {
        if(m->sensor) {
            (void)ma_sensor_dec_ref((ma_sensor_t*)m->sensor);
        }
        return ma_sensor_inc_ref((ma_sensor_t*)(m->sensor = sensor));
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_monitor_network_get_sensor(ma_monitor_network_t *m, ma_network_sensor_t **sensor) {
    if(m && sensor) {
        return ma_sensor_inc_ref((ma_sensor_t*)(*sensor = m->sensor));
    }
    return MA_ERROR_INVALIDARG;
}



