#include "ma_monitor_users.h"
#include "ma/internal/ma_macros.h"
#include "ma/sensor/ma_sensor_msg.h"
#include "../ma_user_sensor_internal.h"

#include <Windows.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "sensor"

extern ma_logger_t *sensor_logger;

static HRESULT STDMETHODCALLTYPE QueryInterface(__RPC__in ISensLogon2 * self,__RPC__in REFIID riid,__RPC__deref_out  void **ppvObject);
static ULONG STDMETHODCALLTYPE AddRef(__RPC__in ISensLogon2 * self);
static ULONG STDMETHODCALLTYPE Release(__RPC__in ISensLogon2 * self);
static HRESULT STDMETHODCALLTYPE GetTypeInfoCount(__RPC__in ISensLogon2 * self, __RPC__out UINT *pctinfo);
static HRESULT STDMETHODCALLTYPE GetTypeInfo(__RPC__in ISensLogon2 * self, UINT iTInfo, LCID lcid,__RPC__deref_out_opt ITypeInfo **ppTInfo);
static HRESULT STDMETHODCALLTYPE GetIDsOfNames(__RPC__in ISensLogon2 * self,__RPC__in REFIID riid,__RPC__in_ecount_full(cNames) LPOLESTR *rgszNames, __RPC__in_range(0,16384) UINT cNames,LCID lcid,__RPC__out_ecount_full(cNames) DISPID *rgDispId);
static HRESULT STDMETHODCALLTYPE Invoke( ISensLogon2 * self,DISPID dispIdMember,REFIID riid,LCID lcid,WORD wFlags,DISPPARAMS *pDispParams,VARIANT *pVarResult,EXCEPINFO *pExcepInfo,UINT *puArgErr);
static HRESULT STDMETHODCALLTYPE Logon(__RPC__in ISensLogon2 * self,__RPC__in BSTR bstrUserName,DWORD dwSessionId);
static HRESULT STDMETHODCALLTYPE Logoff(__RPC__in ISensLogon2 * self,__RPC__in BSTR bstrUserName,DWORD dwSessionId);
static HRESULT STDMETHODCALLTYPE SessionDisconnect(__RPC__in ISensLogon2 * self,__RPC__in BSTR bstrUserName, DWORD dwSessionId);
static HRESULT STDMETHODCALLTYPE SessionReconnect(__RPC__in ISensLogon2 * self,__RPC__in BSTR bstrUserName,DWORD dwSessionId);
static HRESULT STDMETHODCALLTYPE PostShell(__RPC__in ISensLogon2 * self,__RPC__in BSTR bstrUserName,DWORD dwSessionId);
static void split_user_domain_name(wchar_t *full_name, wchar_t **user, wchar_t **domain);


static ISensLogon2Vtbl methods= {
    &QueryInterface,
    &AddRef,
    &Release,
    &GetTypeInfoCount,
    &GetTypeInfo,
    &GetIDsOfNames,
    &Invoke,
    &Logon,
    &Logoff,
    &SessionDisconnect,
    0,
    0
};

struct ma_monitor_users_s {
    ISensLogon2 base;
    long ref_count;
    ma_user_sensor_t *sensor;
    IEventSystem *m_pIEventSystem;
    wchar_t *m_subscriptionId;
};

ma_error_t ma_monitor_users_create(ma_user_sensor_t *sensor, ma_monitor_users_t **monitor) {
    if(sensor && monitor) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;

        if((*monitor = (ma_monitor_users_t*)calloc(1, sizeof(ma_monitor_users_t)))) {
            ((ISensLogon2*)(*monitor))->lpVtbl = &methods;
            (void)ma_sensor_inc_ref((ma_sensor_t*)((*monitor)->sensor = sensor));
            err = MA_OK;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_monitor_users_release(ma_monitor_users_t *monitor) {
    if(monitor) {
        if(monitor->sensor)(void)ma_sensor_dec_ref((ma_sensor_t*)monitor->sensor);
        free(monitor);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


HRESULT STDMETHODCALLTYPE QueryInterface(__RPC__in ISensLogon2 * self,__RPC__in REFIID iid,__RPC__deref_out  void **ppvObject) {
    if (IsEqualIID(iid, &IID_IUnknown) || IsEqualIID(iid, &IID_IDispatch) || IsEqualIID(iid, &IID_ISensLogon2)) {
        ma_monitor_users_t *user = (ma_monitor_users_t*)self;
        *ppvObject = user;
        AddRef(self);
        return S_OK;
    }
    *ppvObject = NULL;
    return E_NOINTERFACE;
}

ULONG STDMETHODCALLTYPE AddRef(__RPC__in ISensLogon2 * self) {
    if(self) {
        ma_monitor_users_t *m = (ma_monitor_users_t*)self;
        return InterlockedIncrement(&m->ref_count);
    }
    return 0;
}

ULONG STDMETHODCALLTYPE Release(__RPC__in ISensLogon2 * self) {
    if(self) {
        ma_monitor_users_t *m = (ma_monitor_users_t*)self;
        return InterlockedDecrement(&m->ref_count);
    }
    return 0;
}

HRESULT STDMETHODCALLTYPE GetTypeInfoCount(__RPC__in ISensLogon2 * self, unsigned int FAR* pctinfo) { 
    return E_NOTIMPL;
}

HRESULT STDMETHODCALLTYPE GetTypeInfo(__RPC__in ISensLogon2 * self, unsigned int iTInfo, LCID  lcid, ITypeInfo FAR* FAR*  ppTInfo) {
    return E_NOTIMPL;
}

HRESULT STDMETHODCALLTYPE GetIDsOfNames(__RPC__in ISensLogon2 * self, REFIID riid, OLECHAR FAR* FAR* rgszNames, unsigned int cNames, LCID lcid, DISPID FAR* rgDispId) { 
    return E_NOTIMPL; 
}

HRESULT STDMETHODCALLTYPE Invoke(__RPC__in ISensLogon2 * self, DISPID dispIdMember, REFIID riid, LCID lcid, WORD wFlags, DISPPARAMS FAR* pDispParams, VARIANT FAR* parResult, EXCEPINFO FAR* pExcepInfo, unsigned int FAR* puArgErr) {
    return E_NOTIMPL; 
}

HRESULT STDMETHODCALLTYPE Logon(__RPC__in ISensLogon2 * self,__RPC__in BSTR bstrUserName,DWORD dwSessionId) {
    if(self) {
        ma_user_t u = {0};
        ma_monitor_users_t *monitor = (ma_monitor_users_t*)self;

        u.sid = dwSessionId;
        if(!map_find(&monitor->sensor->info_map, &u)) {
            wchar_t *pfull_name = NULL;
        
            MA_WLOG(sensor_logger, MA_LOG_SEV_DEBUG, L"isense logged on user(%ls) session id(%d)", bstrUserName, dwSessionId);
            if((pfull_name = (wchar_t*)calloc(1, ((wcslen(bstrUserName) + 1) * sizeof(wchar_t))))) {
                wchar_t *user = NULL;
                wchar_t *domain = NULL;
                ma_user_sensor_msg_t *user_msg = NULL;
                ma_error_t err = MA_OK;
                ma_sensor_msg_t *msg = NULL;
                size_t index = 0;

                wcsncpy(pfull_name, bstrUserName, wcslen(bstrUserName));
                split_user_domain_name(pfull_name, &user, &domain);
                MA_WLOG(sensor_logger, MA_LOG_SEV_DEBUG, L"isense user(%ls) domain(%ls)", user, domain);
                if(MA_OK == (err = ma_user_sensor_msg_create(&user_msg, 1))) {
                    (void)ma_user_sensor_msg_set_event_type(user_msg, MA_USER_SENSOR_LOG_ON_EVENT);
                    MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "posting user sensor event<%d>...", MA_USER_SENSOR_LOG_ON_EVENT);
                    (void)ma_user_sensor_msg_set_wuser(user_msg, index, user);
                    (void)ma_user_sensor_msg_set_wdomain(user_msg, index, domain);
                    (void)ma_user_sensor_msg_set_logon_state(user_msg, MA_USER_LOGGED_STATE_ON);
                    MA_WLOG(sensor_logger, MA_LOG_SEV_DEBUG, L"user(%ls) domain(%ls) on_off(%d)", user, domain, MA_USER_LOGGED_STATE_ON); 
                    if(MA_OK == (err = ma_sensor_msg_create_from_user_sensor_msg(user_msg, &msg))) {
                        MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "posting user sensor event<%d>...", MA_USER_SENSOR_LOG_ON_EVENT);
                        if(MA_OK != (err = ma_sensor_on_msg((ma_sensor_t*)monitor->sensor, msg))) {
                            MA_LOG(sensor_logger, MA_LOG_SEV_ERROR, "posting user sensor event<%d> message failed, last error<%d>", MA_USER_SENSOR_LOG_ON_EVENT, err);
                        }
                        (void)ma_sensor_msg_release(msg);
                    }
                    (void)ma_user_sensor_msg_release(user_msg);
                }

                free(pfull_name);
            }
        }
    }
    return S_OK;
}

HRESULT STDMETHODCALLTYPE Logoff(__RPC__in ISensLogon2 * self,__RPC__in BSTR bstrUserName,DWORD dwSessionId) {
    if(self) {
        ma_user_t u = {0};
        ma_monitor_users_t *monitor = (ma_monitor_users_t*)self;
        
        u.sid = dwSessionId;
        if(map_find(&monitor->sensor->info_map, &u)) {
            wchar_t *pfull_name = NULL;

            MA_WLOG(sensor_logger, MA_LOG_SEV_DEBUG, L"isense logged off user(%ls) session id(%d)", bstrUserName, dwSessionId);
            if((pfull_name = (wchar_t*)calloc(1, ((wcslen(bstrUserName) + 1) * sizeof(wchar_t))))) {
                wchar_t *user = NULL;
                wchar_t *domain = NULL;
                ma_user_sensor_msg_t *user_msg = NULL;
                ma_error_t err = MA_OK;
                ma_sensor_msg_t *msg = NULL;
                size_t index = 0;

                wcsncpy(pfull_name, bstrUserName, wcslen(bstrUserName));
                split_user_domain_name(pfull_name, &user, &domain);
                MA_WLOG(sensor_logger, MA_LOG_SEV_DEBUG, L"isense full name(%ls) user(%ls) domain(%ls)", pfull_name, user, domain);
                if(MA_OK == (err = ma_user_sensor_msg_create(&user_msg, 1))) {
                    (void)ma_user_sensor_msg_set_event_type(user_msg, MA_USER_SENSOR_LOG_OFF_EVENT);
                    MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "posting user sensor event<%d>...", MA_USER_SENSOR_LOG_OFF_EVENT);
                    (void)ma_user_sensor_msg_set_wuser(user_msg, index, user);
                    (void)ma_user_sensor_msg_set_wdomain(user_msg, index, domain);
                    (void)ma_user_sensor_msg_set_logon_state(user_msg, MA_USER_LOGGED_STATE_OFF);
                    MA_WLOG(sensor_logger, MA_LOG_SEV_DEBUG, L"user(%ls) domain(%ls) on_off(%d)", user, domain, MA_USER_LOGGED_STATE_OFF); 
                    if(MA_OK == (err = ma_sensor_msg_create_from_user_sensor_msg(user_msg, &msg))) {
                        MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "posting user sensor event<%d>...", MA_USER_SENSOR_LOG_ON_EVENT);
                        if(MA_OK != (err = ma_sensor_on_msg((ma_sensor_t*)monitor->sensor, msg))) {
                            MA_LOG(sensor_logger, MA_LOG_SEV_ERROR, "posting user sensor event<%d> message failed, last error<%d>", MA_USER_SENSOR_LOG_OFF_EVENT, err);
                        }
                        (void)ma_sensor_msg_release(msg);
                    }
                    (void)ma_user_sensor_msg_release(user_msg);
                }

                free(pfull_name);
            }
        }
    }
    return S_OK;
}

HRESULT STDMETHODCALLTYPE SessionDisconnect(__RPC__in ISensLogon2 * self,__RPC__in BSTR bstrUserName, DWORD dwSessionId) {
    return S_OK;
}

ma_error_t ma_monitor_users_init_sense(ma_monitor_users_t *monitor) {
    if(monitor) {
        ma_error_t err = MA_OK;
        IEventSystem *m_spEventSystem = NULL;
        IEventSubscription *spEventSubscription = NULL;
        HRESULT res = S_OK;
        if(S_OK == (res = CoInitializeEx(NULL,COINIT_MULTITHREADED))) {
            HRESULT hr = CoCreateInstance(&CLSID_CEventSystem, NULL, CLSCTX_SERVER, &IID_IEventSystem, (void**)(&m_spEventSystem));
            if(SUCCEEDED(hr)) {
                monitor->m_pIEventSystem = m_spEventSystem;
                hr = CoCreateInstance(&CLSID_CEventSubscription, NULL, CLSCTX_SERVER, &IID_IEventSubscription, (void**)(&spEventSubscription));
            }
            if(SUCCEEDED(hr)) {
                hr = spEventSubscription->lpVtbl->get_SubscriptionID(spEventSubscription, &monitor->m_subscriptionId);
            }
            if(SUCCEEDED(hr)) {
                wchar_t subscriptionName[] = L"ISensLogon2";
                hr = spEventSubscription->lpVtbl->put_SubscriptionName(spEventSubscription, subscriptionName);
            }
            if(SUCCEEDED(hr)) {
                wchar_t eventClass[] = L"{D5978650-5B9F-11D1-8DD2-00AA004ABD5E}";
                hr = spEventSubscription->lpVtbl->put_EventClassID(spEventSubscription, eventClass);
            }
            if(SUCCEEDED(hr)) {
                hr = spEventSubscription->lpVtbl->put_SubscriberInterface(spEventSubscription, (IUnknown*)monitor);
            }
            if(SUCCEEDED(hr)) {
                wchar_t eventSubscriptionProgramId[] = PROGID_EventSubscription;
                hr = m_spEventSystem->lpVtbl->Store(m_spEventSystem, eventSubscriptionProgramId, (IUnknown*)spEventSubscription);
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_monitor_users_deinit_sense(ma_monitor_users_t *monitor) {
    if(monitor && monitor->m_pIEventSystem) {
        wchar_t queryInfo[MA_MAX_PATH] = {0};
        wchar_t *eventSubscriptionProgramId = PROGID_EventSubscription;
        HRESULT hr = 0;
        int errorIndex = 0;

        MA_MSC_SELECT(_snwprintf, snwprintf)(queryInfo, MA_MAX_PATH, L"%ls%ls",L"EventClassID={D5978650-5B9F-11D1-8DD2-00AA004ABD5E} & SubscriptionID=", monitor->m_subscriptionId);
        monitor->m_pIEventSystem->lpVtbl->Remove(monitor->m_pIEventSystem, eventSubscriptionProgramId, (BSTR)queryInfo, &errorIndex);
        monitor->m_pIEventSystem->lpVtbl->Release(monitor->m_pIEventSystem);
        CoUninitialize();

        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_monitor_users_set_sensor(ma_monitor_users_t *m, ma_user_sensor_t *sensor) {
    if(m && sensor) {
        if(m->sensor) {
            (void)ma_sensor_dec_ref((ma_sensor_t*)m->sensor);
        }
        return ma_sensor_inc_ref((ma_sensor_t*)(m->sensor = sensor));
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_monitor_users_get_sensor(ma_monitor_users_t *m, ma_user_sensor_t **sensor) {
    if(m && sensor) {
        return ma_sensor_inc_ref((ma_sensor_t*)(*sensor = m->sensor));
    }
    return MA_ERROR_INVALIDARG;
}

void split_user_domain_name(wchar_t *full_name, wchar_t **user, wchar_t **domain) {
    if(full_name && user && domain) {
        wchar_t spliter = L'\\';
        wchar_t *sep = NULL;

        if(sep = wcschr(full_name, spliter)) {
            *domain = full_name;
            *user = sep +1;
            *sep = L'\0';
        }
    }
}

