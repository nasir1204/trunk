#include "ma/sensor/ma_sensor.h"
#include "ma/internal/defs/ma_sensor_defs.h"
#include "ma/ma_log.h"
#include "ma/sensor/ma_network_sensor.h"
#include "../ma_network_sensor_internal.h"
#include "ma/internal/ma_macros.h"
#include "ma/sensor/ma_sensor_msg.h"
#include "ma_monitor_network.h"
#include "ma/sensor/ma_sensor_utils.h"

#include <stdlib.h>
#include <string.h>
#include <Sensapi.h>
#include <Wininet.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "sensor"

extern ma_logger_t *sensor_logger;
extern int g_repeater_interval;

ma_error_t network_sensor_on_msg(ma_sensor_t *sensor, ma_sensor_msg_t *msg) {
    if(sensor && msg) {
        ma_error_t err = MA_OK;
        ma_network_sensor_t *network = (ma_network_sensor_t*)sensor;
        ma_network_sensor_msg_t *network_msg = NULL;

        if(MA_OK == (err = ma_sensor_msg_get_network_sensor_msg(msg, &network_msg))) {
            ma_sensor_event_type_t type = MA_NETWORK_SENSOR_CONNECTIVITY_STATUS_CHANGE_EVENT;
            ma_message_t *pub_msg = NULL;
            
            MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) received message", network->name);
            (void)ma_network_sensor_msg_get_event_type(network_msg, &type);
            if(MA_OK == (err = ma_message_create(&pub_msg))) {
                ma_variant_t *msg_var = NULL;

                if(MA_OK == (err = convert_network_sensor_msg_to_variant(network_msg, &msg_var))) {
                    (void)ma_message_set_payload(pub_msg, msg_var);
                    if(MA_OK != (err = ma_sensor_publish_msg(network->msgbus, network->name, ma_sensor_event_type_to_str(type), pub_msg))) {
                        MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) publishing event(%s) failed last error(%d)", network->name, ma_sensor_event_type_to_str(type), err);
                    }
                    (void)ma_variant_release(msg_var);
                } else MA_LOG(sensor_logger, MA_LOG_SEV_ERROR, "sensor(%s) converting network sensor message to variant failed, last error(%d)", network->name, err);
                (void)ma_message_release(pub_msg);
            }
            (void)ma_network_sensor_msg_release(network_msg);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t network_sensor_get_type(ma_sensor_t *sensor, ma_sensor_type_t *type) {
    if(sensor && type) {
        ma_network_sensor_t *network = (ma_network_sensor_t*)sensor;
        *type = network->type;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t network_sensor_get_id(ma_sensor_t *sensor, const char **id) {
    if(sensor && id) {
        ma_network_sensor_t *network = (ma_network_sensor_t*)sensor;
        *id = network->name;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t network_sensor_start(ma_sensor_t *sensor) {
    if(sensor) {
        ma_error_t err = MA_OK;
        ma_network_sensor_t *network = (ma_network_sensor_t*)sensor;

        if((MA_SENSOR_STATE_STOPPED == network->state) || (MA_SENSOR_STATE_NOT_STARTED == network->state)) {
            ma_monitor_network_t *monitor = NULL;

            if(MA_OK == (err = ma_monitor_network_create(network, &monitor))) {
                if(MA_OK == (err = ma_monitor_network_init_sense(monitor))) {
                    network->state = MA_SENSOR_STATE_RUNNING;
                    network->data = monitor;
                    MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) started successfully", network->name);
                } else
                    MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) start failed, last error(%d)", network->name, err);
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t network_sensor_stop(ma_sensor_t *sensor) {
    if(sensor) {
        ma_error_t err = MA_OK;
        ma_network_sensor_t *network = (ma_network_sensor_t*)sensor;

        if(MA_SENSOR_STATE_RUNNING == network->state) {
            if(MA_OK == (err = ma_monitor_network_deinit_sense((ma_monitor_network_t*)network->data))) {
                if(network->data) {
                    (void)ma_monitor_network_release((ma_monitor_network_t*)network->data);
                }
                MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) stopped successfully", network->name);
            } else
                MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) stop failed, last error(%d)", network->name, err);
            network->state = MA_SENSOR_STATE_STOPPED;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t network_sensor_release(ma_sensor_t *sensor) {
    if(sensor) {
        ma_network_sensor_t *network = (ma_network_sensor_t*)sensor;

        MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) network monitor released successfully", network->name);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t network_sensor_detect(ma_sensor_t *sensor) {
    if(sensor) {
        ma_error_t err = MA_OK;
        ma_network_sensor_t *network = (ma_network_sensor_t*)sensor;
        ma_bool_t status = get_network_connectivity_status();

        if(status != network->connect_status) {
            ma_network_sensor_msg_t *network_msg = NULL;
            
            /* network status is modified */
            network->connect_status = status;
            if(MA_OK == (err = ma_network_sensor_msg_create(&network_msg, 1))) {
                ma_sensor_msg_t *msg = NULL;
                if(MA_OK == (err = ma_sensor_msg_create_from_network_sensor_msg(network_msg, &msg))) {
                    (void)ma_network_sensor_msg_set_event_type(network_msg, MA_NETWORK_SENSOR_CONNECTIVITY_STATUS_CHANGE_EVENT);
                    if(status) {
                        (void)ma_network_sensor_msg_set_connectivity_status(network_msg, MA_NETWORK_CONNECTIVITY_STATUS_CONNECTED);
                        MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) network connected", ma_sensor_type_to_str(MA_SENSOR_TYPE_NETWORK));
                    } else {
                        (void)ma_network_sensor_msg_set_connectivity_status(network_msg, MA_NETWORK_CONNECTIVITY_STATUS_DISCONNECTED);
                        MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) network disconnected", ma_sensor_type_to_str(MA_SENSOR_TYPE_NETWORK));
                    }
                    if(MA_OK != (err = network_sensor_on_msg(sensor, msg)))
                        MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) posting message failed, last error(%d)", ma_sensor_type_to_str(MA_SENSOR_TYPE_NETWORK), err);
                    (void)ma_sensor_msg_release(msg);
                }
                (void)ma_network_sensor_msg_release(network_msg);
            }
        }
        
        /* adjust repeater */
        g_repeater_interval = (g_repeater_interval > MA_NETWORK_SENSOR_REPEATER_INTERVAL) 
                                ? MA_NETWORK_SENSOR_REPEATER_INTERVAL 
                                : g_repeater_interval;

        return err;
    }
    return MA_ERROR_INVALIDARG;
}


static ma_bool_t check_network() {
    DWORD alive = 0;

    return InternetGetConnectedState(&alive,0) ? MA_TRUE : MA_FALSE;
    //return 0 == WSAGetLastError() ? alive & INTERNET_CONNECTION_LAN ||  alive & INTERNET_CONNECTION_MODEM ? MA_TRUE :  MA_FALSE
    //    : MA_FALSE;
}

ma_bool_t get_network_connectivity_status() {
    ma_bool_t alive = 0;
    DWORD network = 0;

    alive = IsNetworkAlive(&network);
    if(0 == GetLastError())
        alive = alive ? alive : check_network();

    MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor get network connectivity status returns (%d)", alive);
    return alive;
}


