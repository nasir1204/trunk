#include "ma/sensor/ma_sensor.h"
#include "ma/internal/defs/ma_sensor_defs.h"
#include "ma/ma_log.h"
#include "ma/sensor/ma_power_sensor.h"
#include "../ma_power_sensor_internal.h"
#include "ma/internal/ma_macros.h"
#include "ma/sensor/ma_sensor_msg.h"
#include "ma/sensor/ma_sensor_utils.h"

#include <stdlib.h>
#include <string.h>
#include <Windows.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "sensor"

extern ma_logger_t *sensor_logger;

ma_error_t power_sensor_on_msg(ma_sensor_t *sensor, ma_sensor_msg_t *msg) {
    if(sensor && msg) {
        ma_error_t err = MA_OK;
        ma_power_sensor_t *power = (ma_power_sensor_t*)sensor;
        ma_power_sensor_msg_t *power_msg = NULL;
        ma_message_t *pub_msg = NULL;

        if(MA_OK == (err = ma_sensor_msg_get_power_sensor_msg(msg, &power_msg))) {
            ma_sensor_event_type_t type = MA_POWER_SENSOR_BATTERY_LOW_EVENT;

            MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) received message", power->name);
            (void)ma_power_sensor_msg_get_event_type(power_msg, &type);
            if(MA_OK == (err = ma_message_create(&pub_msg))) {
                ma_variant_t *msg_var = NULL;

                if(MA_OK == (err = convert_power_sensor_msg_to_variant(power_msg, &msg_var))) {
                    (void)ma_message_set_payload(pub_msg, msg_var);
                    if(MA_OK != (err = ma_sensor_publish_msg(power->msgbus, power->name, ma_sensor_event_type_to_str(type), pub_msg))) {
                        MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) publishing event(%s) failed last error(%d)", power->name, ma_sensor_event_type_to_str(type), err);
                    }
                    (void)ma_variant_release(msg_var);
                } else MA_LOG(sensor_logger, MA_LOG_SEV_ERROR, "sensor(%s) converting power sensor message to variant failed, last error(%d)", power->name, err);
                (void)ma_message_release(pub_msg);
            }
            (void)ma_power_sensor_msg_release(power_msg);
        }
		return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t power_sensor_get_type(ma_sensor_t *sensor, ma_sensor_type_t *type) {
    if(sensor && type) {
        ma_power_sensor_t *power = (ma_power_sensor_t*)sensor;
        *type = power->type;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t power_sensor_get_id(ma_sensor_t *sensor, const char **id) {
    if(sensor && id) {
        ma_power_sensor_t *power = (ma_power_sensor_t*)sensor;
        *id = power->name;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t power_sensor_start(ma_sensor_t *sensor) {
    if(sensor) {
        ma_power_sensor_t *power = (ma_power_sensor_t*)sensor;
        ma_error_t err = MA_OK;

        if((MA_SENSOR_STATE_STOPPED == power->state) || (MA_SENSOR_STATE_NOT_STARTED == power->state)){
            ma_power_sensor_msg_t *pwr_msg = NULL;
            ma_sensor_msg_t *msg = NULL;
            size_t index = 0;

            if(MA_OK == (err = ma_power_sensor_msg_create(&pwr_msg, 1))) {            
                SYSTEM_POWER_STATUS power_status = {0};

			    if(GetSystemPowerStatus(&power_status)) {
                    {
                        ma_power_status_t status = MA_POWER_STATUS_OFFLINE;

                        switch(power_status.ACLineStatus) {
                            case 0x00: status = MA_POWER_STATUS_OFFLINE; break;
                            case 0x01: status = MA_POWER_STATUS_ONLINE; break;
                            case 0xff: status = MA_POWER_STATUS_UNKNOWN; break;
                        }
                        if(MA_OK != (err = ma_power_sensor_msg_set_power_status(pwr_msg, index, status))) {
                            MA_LOG(sensor_logger,MA_LOG_SEV_ERROR,"sensor(%s) failed to set power status, last error(%d)", ma_sensor_type_to_str(MA_SENSOR_TYPE_POWER), err);
                        }
                    }
                    {
                        ma_battery_charge_status_t status = MA_BATTERY_CHARGE_STATUS_HIGH;

                        switch(power_status.BatteryFlag) {
                            case 0x01: status = MA_BATTERY_CHARGE_STATUS_HIGH; break;
                            case 0x02: status = MA_BATTERY_CHARGE_STATUS_LOW; break;
                            case 0x04: status = MA_BATTERY_CHARGE_STATUS_CRITICAL; break;
                            case 0x08: status = MA_BATTERY_CHARGE_STATUS_CHARGING; break;
                            case 0x80: status = MA_BATTERY_CHARGE_STATUS_NO_BATTERY; break;
                            case 0xff: status = MA_BATTERY_CHARGE_STATUS_UNKNOWN; break;
                        }
                        if(MA_OK != (err = ma_power_sensor_msg_set_battery_charge_status(pwr_msg, index, status))) {
                            MA_LOG(sensor_logger,MA_LOG_SEV_ERROR,"sensor(%s) failed to set battery charge status, last error(%d)", ma_sensor_type_to_str(MA_SENSOR_TYPE_POWER), err);
                        }
                    }
                    if(MA_OK != (err = ma_power_sensor_msg_set_battery_lifetime(pwr_msg, index, power_status.BatteryLifeTime))) {
                        MA_LOG(sensor_logger,MA_LOG_SEV_ERROR,"sensor(%s) failed to set battery life time, last error(%d)",ma_sensor_type_to_str(MA_SENSOR_TYPE_POWER), err);
                    }
                    if(MA_OK != (err = ma_power_sensor_msg_set_battery_percent(pwr_msg, index, power_status.BatteryLifePercent))) {
                        MA_LOG(sensor_logger,MA_LOG_SEV_ERROR,"sensor(%s) failed to set battery life time, last error(%d)",ma_sensor_type_to_str(MA_SENSOR_TYPE_POWER), err);
                    }
                }
                (void)ma_power_sensor_msg_set_event_type(pwr_msg, MA_POWER_SENSOR_STATUS_CHANGE_EVENT);
                if(MA_OK == (err = ma_sensor_msg_create_from_power_sensor_msg(pwr_msg, &msg))) {
                    ma_message_t *pub_msg = NULL;
                    if(MA_OK == (err = ma_message_create(&pub_msg))) {
                        ma_variant_t *msg_var = NULL;
                        if(MA_OK == (err = convert_power_sensor_msg_to_variant(pwr_msg, &msg_var))) {
                            (void)ma_message_set_payload(pub_msg, msg_var);
                            MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) publishing power event(%s)...", ma_sensor_type_to_str(MA_SENSOR_TYPE_POWER), ma_sensor_event_type_to_str(MA_POWER_SENSOR_STATUS_CHANGE_EVENT));
                            if(MA_OK != (err = ma_sensor_publish_msg(power->msgbus, ma_sensor_type_to_str(MA_SENSOR_TYPE_POWER), ma_sensor_event_type_to_str(MA_POWER_SENSOR_STATUS_CHANGE_EVENT), pub_msg))) {
                                MA_LOG(sensor_logger, MA_LOG_SEV_ERROR, "sensor(%s) publishing power event(%s) failed, last error(%d)", ma_sensor_type_to_str(MA_SENSOR_TYPE_POWER), ma_sensor_event_type_to_str(MA_POWER_SENSOR_STATUS_CHANGE_EVENT), err);
                            }
                            (void)ma_variant_release(msg_var);
                        } else MA_LOG(sensor_logger, MA_LOG_SEV_ERROR, "sensor(%s) converting power sensor message to variant failed, last error(%d)", power->name, err);
                        (void)ma_message_release(pub_msg);
                    }
                    (void)ma_sensor_msg_release(msg);
                }
                (void)ma_power_sensor_msg_release(pwr_msg);
            }
            power->state = MA_SENSOR_STATE_RUNNING;
        }

        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t power_sensor_stop(ma_sensor_t *sensor) {
    if(sensor) {
        ma_power_sensor_t *power = (ma_power_sensor_t*)sensor;

        if(MA_SENSOR_STATE_RUNNING == power->state) {
            power->state = MA_SENSOR_STATE_STOPPED;
        }

        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t power_sensor_release(ma_sensor_t *sensor) {
    if(sensor) {
        ma_error_t err = MA_OK;

        return err;
    }
    return MA_ERROR_INVALIDARG;
}
