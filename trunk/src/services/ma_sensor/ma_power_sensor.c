#include "ma/sensor/ma_sensor.h"
#include "ma/internal/defs/ma_sensor_defs.h"
#include "ma/ma_log.h"
#include "ma/sensor/ma_power_sensor.h"
#include "ma/internal/ma_macros.h"
#include "ma_power_sensor_internal.h"

#include <stdlib.h>
#include <string.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "sensor"

extern ma_logger_t *sensor_logger;

static const ma_sensor_methods_t power_methods = {
    &power_sensor_on_msg,
    &power_sensor_get_type,
    &power_sensor_get_id,
    &power_sensor_start,
    &power_sensor_stop,
    &power_sensor_release
};


ma_error_t ma_power_sensor_create(ma_msgbus_t *msgbus, ma_power_sensor_t **sensor) {
    if(msgbus && sensor) {
        ma_power_sensor_t *power = NULL;
        ma_error_t err = MA_ERROR_OUTOFMEMORY;

        if((power = (ma_power_sensor_t*)calloc(1, sizeof(ma_power_sensor_t)))) {
            ma_sensor_init((ma_sensor_t*)(*sensor = power), &power_methods, power);
            strncpy(power->name, MA_SENSOR_TYPE_POWER_STR, strlen(MA_SENSOR_TYPE_POWER_STR));
            power->type = MA_SENSOR_TYPE_POWER; power->state = MA_SENSOR_STATE_NOT_STARTED;
            power->msgbus = msgbus;
            if(MA_OK == (err = ma_sensor_inc_ref((ma_sensor_t*)(*sensor)))) {
                MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "power sensor(%s) created successfully", power->name);
            } else {
                MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "power sensor(%s) creation failed, last error(%d)", power->name, err);
                free(power); *sensor = NULL;
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_power_sensor_release(ma_power_sensor_t *sensor) {
    if(sensor) {
        ma_error_t err = MA_OK;

        if(!((ma_sensor_t*)sensor)->ref_count) {
            MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "power sensor(%s) releasing...", sensor->name);
            free(sensor);
        } else {
            if(MA_OK != (err = ma_sensor_dec_ref((ma_sensor_t*)sensor))) {
                MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "power sensor(%s) dec ref failed, last error(%d)", ma_sensor_type_to_str(MA_SENSOR_TYPE_POWER), err);
            }
        }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}



