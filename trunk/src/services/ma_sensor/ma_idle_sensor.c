#include "ma/sensor/ma_sensor.h"
#include "ma/internal/defs/ma_sensor_defs.h"
#include "ma/ma_log.h"
#include "ma/sensor/ma_idle_sensor.h"
#include "ma_idle_sensor_internal.h"
#include "ma/internal/ma_macros.h"
#include "ma/ma_msgbus.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/sensor/ma_sensor_utils.h"
#include "ma/sensor/ma_sensor_msg.h"

#include <stdlib.h>
#include <string.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "sensor"

extern ma_logger_t *sensor_logger;

static const ma_sensor_methods_t idle_methods = {
    &idle_sensor_on_msg,
    &idle_sensor_get_type,
    &idle_sensor_get_id,
    &idle_sensor_start,
    &idle_sensor_stop,
    &idle_sensor_release,
    &idle_sensor_detect
};


ma_error_t ma_idle_sensor_create(ma_msgbus_t *msgbus, ma_idle_sensor_t **sensor) {
    if(msgbus && sensor) {
        ma_idle_sensor_t *idle_sensor = NULL;
        ma_error_t err = MA_ERROR_OUTOFMEMORY;

        if((idle_sensor = (ma_idle_sensor_t*)calloc(1, sizeof(ma_idle_sensor_t)))) {
            if(MA_OK == (err = ma_sensor_inc_ref((ma_sensor_t*)(*sensor = idle_sensor)))) {
                idle_sensor->type = MA_SENSOR_TYPE_IDLE; 
                idle_sensor->state = MA_SENSOR_STATE_NOT_STARTED;
                idle_sensor->msgbus = msgbus;
                ma_sensor_init((ma_sensor_t*)(*sensor = idle_sensor), &idle_methods, idle_sensor);
                strncpy(idle_sensor->name, MA_SENSOR_TYPE_IDLE_STR, strlen(MA_SENSOR_TYPE_IDLE_STR));
                MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) created successfully", idle_sensor->name);
            }
        }
        if(MA_OK != err)  {
            MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) create failed, last error(%d)", idle_sensor->name, err);
            free(idle_sensor); *sensor = NULL;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_idle_sensor_release(ma_idle_sensor_t *sensor) {
    if(sensor) {
        ma_error_t err = MA_OK;

        if(!((ma_sensor_t*)sensor)->ref_count) {
            MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) releasing...", sensor->name);
            free(sensor);
        } else {
            if(MA_OK != (err = ma_sensor_dec_ref((ma_sensor_t*)sensor))) {
                MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) dec ref failed, last error(%d)", ma_sensor_type_to_str(MA_SENSOR_TYPE_IDLE), err);
            }
        }

        return MA_OK; /* intentional */
    }
    return MA_ERROR_INVALIDARG;
}



