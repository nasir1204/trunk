#include "ma/sensor/ma_sensor.h"
#include "ma/internal/defs/ma_sensor_defs.h"
#include "ma/ma_log.h"
#include "ma/sensor/ma_system_sensor.h"
#include "../ma_system_sensor_internal.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/defs/ma_sensor_defs.h"

#include <stdlib.h>
#include <string.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "sensor"

extern ma_logger_t *sensor_logger;

#if  defined(MACX)
#include <time.h>
#include <errno.h>
#include <sys/sysctl.h>

static int get_uptime() {
    struct timeval boottime;
    size_t len = sizeof(boottime);
    int mib[2] = { CTL_KERN, KERN_BOOTTIME };

    if(sysctl(mib, 2, &boottime, &len, NULL, 0) < 0)
        return -1;
    time_t bsec = boottime.tv_sec, csec = time(NULL);

    return difftime(csec, bsec);
}

#elif defined(LINUX)
#include <sys/sysinfo.h>

static int get_uptime() {
    struct sysinfo info = {0};

    sysinfo(&info);
    return info.uptime;
}
#endif

MA_CPP(extern "C" {)

ma_bool_t detect_system_startup(ma_sensor_t *sensor) {
    if(sensor) {
#if defined(MACX) || defined(LINUX)
        return get_uptime() < (MA_MAX_SYSTEM_STARTUP_TIME * 60) ? MA_TRUE : MA_FALSE;
#else
        /* TBD system startup detection */
        return MA_FALSE;
#endif
    }
    return MA_FALSE;
}

MA_CPP(})

