#include "ma/sensor/ma_sensor.h"
#include "ma/internal/defs/ma_sensor_defs.h"
#include "ma/ma_log.h"
#include "ma/sensor/ma_user_sensor.h"
#include "../ma_user_sensor_internal.h"
#include "ma/internal/ma_macros.h"
#include "ma/sensor/ma_sensor_utils.h"
#include "ma/sensor/ma_sensor_msg.h"
#include "uv.h"
#include "uv-private/tree.h"

#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#ifdef MACX
#include "ma/internal/core/msgbus/ma_msgbus_private.h"
#include <sys/types.h>
#include <wchar.h>
#include <sys/stat.h>
#include <stdio.h>
#include <ctype.h>
#include <pwd.h>
#include <grp.h>
#include <utmpx.h>
#include <fcntl.h>

static void utmp_file_watcher_cb(uv_fs_event_t* handle, const char* filename, int events, int status);
static void utmp_file_watcher_close_cb(uv_handle_t* handle);
static void collect_logged_on_users(ma_user_sensor_t *sensor);
static ma_bool_t find_domain_name_via_dscl(const char *user_name, const char *recordname, char *domain_name);
static ma_bool_t find_domain_name(const char *user_name,  char *domain_name);

static ma_error_t utmp_file_watcher_init(ma_user_sensor_t *sensor);
static ma_error_t utmp_file_watcher_deinit(ma_user_sensor_t *sensor);
#endif


#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "sensor"

extern ma_logger_t *sensor_logger;

ma_error_t user_sensor_on_msg(ma_sensor_t *sensor,  ma_sensor_msg_t *msg) {
	if(sensor && msg) {
		ma_error_t err = MA_OK;
		ma_user_sensor_t *user = (ma_user_sensor_t*)sensor;
		ma_user_sensor_msg_t *usr_msg = NULL;

		if(MA_OK == (err = ma_sensor_msg_get_user_sensor_msg(msg, &usr_msg))) {
			ma_message_t *pub_msg = NULL;
			ma_variant_t *msg_var = NULL;

			if(MA_OK == (err = ma_message_create(&pub_msg))) {
				if(MA_OK == (err = convert_user_sensor_msg_to_variant(usr_msg, &msg_var))) {
					if(MA_OK == (err = ma_message_set_payload(pub_msg, msg_var))) {
						MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) publishing event(%s)..", user->name, ma_sensor_event_type_to_str(MA_USER_SENSOR_LOG_ON_EVENT));
						if(MA_OK != (err = ma_sensor_publish_msg(user->msgbus, user->name, ma_sensor_event_type_to_str(MA_USER_SENSOR_LOG_ON_EVENT), pub_msg))) {
							MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) publishing event(%s) failed last error(%d)", user->name, ma_sensor_event_type_to_str(MA_USER_SENSOR_LOG_ON_EVENT), err);
						}
					}
					(void)ma_variant_release(msg_var);
				}
				(void)ma_message_release(pub_msg);
			}
			(void)ma_user_sensor_msg_release(usr_msg);
		}

		return err;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t user_sensor_get_type(ma_sensor_t *sensor, ma_sensor_type_t *type) {
	if(sensor && type) {
		ma_user_sensor_t *user = (ma_user_sensor_t*)sensor;
		*type = user->type;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t user_sensor_get_id(ma_sensor_t *sensor, const char **id) {
	if(sensor && id) {
		ma_user_sensor_t *user = (ma_user_sensor_t*)sensor;
		*id = user->name;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t user_sensor_start(ma_sensor_t *sensor) {
	if(sensor) {
		ma_user_sensor_t *user = (ma_user_sensor_t*)sensor;
		ma_error_t err = MA_OK;

		if(MA_SENSOR_STATE_NOT_STARTED == user->state || MA_SENSOR_STATE_STOPPED == user->state) {
#ifdef MACX
			ma_sensor_msg_t *msg = NULL;
			ma_user_sensor_msg_t *usr_msg = NULL;

			if(MA_OK != (err = utmp_file_watcher_init(user)))
				MA_LOG(sensor_logger, MA_LOG_SEV_ERROR, "sensor(%s) utmp file watcher initialize failed, last error(%d)", user->name, err);
			collect_logged_on_users(user);
			if(MA_OK == (err = ma_user_sensor_get_all_logged_on_users(user, &usr_msg))) {
				/* enumerate logged on users once on startup */
				(void)ma_user_sensor_msg_set_startup_flag(usr_msg, MA_TRUE);
				if(MA_OK == (err = ma_sensor_msg_create_from_user_sensor_msg(usr_msg, &msg))) {
					(void)ma_sensor_msg_release(msg);
				}
				(void)ma_user_sensor_msg_release(usr_msg);
			}
#endif
			user->state = MA_SENSOR_STATE_RUNNING;
		}

		return err;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t user_sensor_stop(ma_sensor_t *sensor) {
	if(sensor) {
		ma_user_sensor_t *user = (ma_user_sensor_t*)sensor;

		if(MA_SENSOR_STATE_RUNNING == user->state) {
#ifdef MACX
			(void)utmp_file_watcher_deinit(user);
#endif
			user->state = MA_SENSOR_STATE_STOPPED;
		}
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t user_sensor_release(ma_sensor_t *sensor) {
	if(sensor) {
		ma_error_t err = MA_OK;

		return err;
	}
	return MA_ERROR_INVALIDARG;
}


ma_error_t user_sensor_detect(ma_sensor_t *sensor) {
	if(sensor) {
		ma_error_t err = MA_OK;

		return err;
	}
	return MA_ERROR_INVALIDARG;
}

int map_comparator(ma_user_node_t *l, ma_user_node_t *r) {
	if(l && r) {
		ma_user_t *lu = &l->user;
		ma_user_t *ru = &r->user;
		int result = 0;

		if(lu && ru) {
			if(lu->is_user_wide_str == ru->is_user_wide_str) {
				if(MA_ASCII_STRING == lu->is_user_wide_str) {
					result = strcmp(lu->u.user, ru->u.user);	
				} else if(MA_WCHAR_STRING == lu->is_user_wide_str) {
					result = wcscmp(lu->u.wuser, ru->u.wuser);
				} else {
					/* do nothing */
				}
			} else {
				/* TODO conversion check */
			}
		}

		return result;
	}
	return 0; /* left branch */
}

#ifdef MACX

#define MA_STR_TO_LOWER(p) for ( ; *p; ++p) *p = tolower(*p);
#define DSCL_CMD_LEN    256
#define UPN_RECORD      1024
#define RECORD1         "OriginalAuthenticationAuthority"
#define RECORD2         "userPrincipalName"
static size_t utmpx_file_size;

static void ma_str_to_lower(char *str) {
	int i = 0;
	for(i = 0; str[i]; i++){
		str[i] = tolower(str[i]);
	}
}

static void trimmer(char *str) {
	char *end = str + strlen(str) - 1;

	/* Trim leading non-letters */
	while(!isalnum(*str)) 
		strcpy(str, str + 1);

	/* Trim trailing non-letters */
	while(end > str && !isalnum(*end)) end--;
	*end = '\0';
}

ma_bool_t find_domain_name_via_dscl(const char *user_name, const char *recordname, char *domain_name) {
	char dscl_cmd[DSCL_CMD_LEN] = {0};
	char output[UPN_RECORD] = {0};
	FILE* fp = NULL;

	char format[DSCL_CMD_LEN]= "/usr/bin/dscl /Search read /Users/%s  |  /usr/bin/grep %s";
	snprintf(dscl_cmd,DSCL_CMD_LEN,format,user_name,recordname);

	fp = popen(dscl_cmd,"r");
	if(fp) {
		fread(output,1,UPN_RECORD,fp);
		pclose(fp);
	}
	else
		return MA_FALSE;

	{
		char *pch = NULL;

		/* local users */
		if(strlen(output))return MA_FALSE;
		strncpy(domain_name, "", strlen(""));
		ma_str_to_lower(output);
		if((pch = strstr(output, "@"))) {
			if(!strcmp(RECORD1,recordname)) {
				char *pend = NULL;
				size_t itr = 0;

				if((pend = strstr(pch+1, ";"))) {
					pch++;
					while(pch != pend) {
						domain_name[itr++] = *pch;
					}
					domain_name[itr] = '\0';
				}
			} else if(!strcmp(RECORD2,recordname)) {
				strcpy(domain_name, pch+1);
			}
			if(strlen(domain_name)) {
				trimmer(domain_name);               
				return MA_TRUE;
			}

		}

	}
	return MA_FALSE;
}


#define COMMAND_LEN 16384
ma_bool_t find_domain_name(const char *user_name,  char *domain_name) {	
	ma_bool_t bFound=MA_FALSE;
	char buffer[COMMAND_LEN]={0};
	int  bufflen = sizeof(buffer);	
	//get the user details.
	struct passwd* temp_pwd=NULL;
	struct passwd  pwd;	
	struct group *temp_gr=NULL;
	struct group grp;
	char *pch = NULL;
	//passwd = getpwnam(user_name.c_str());

	int s=getpwnam_r(user_name,&pwd,buffer,bufflen,&temp_pwd);

	if( (NULL == temp_pwd && s == 0) || (NULL == temp_pwd && s != 0) )
		return bFound;

	//now get the group.
	s = getgrgid_r(pwd.pw_gid,&grp,buffer,bufflen,&temp_gr);		
	if( (NULL == temp_gr && s == 0) || (NULL == temp_gr && s != 0) )
		return bFound;

	// gr will contain full path like "DOMAIN_NAME\domain users"
	if(grp.gr_name) {
		strcpy(domain_name, grp.gr_name);
		ma_str_to_lower(domain_name);

		// Check if this line is for domain user.
		if((pch = strstr(domain_name, "\\domain users"))) {
			*pch = '\0';
			bFound = MA_TRUE;
		}
	}
	return bFound;
}

void collect_logged_on_users(ma_user_sensor_t *sensor) {
	struct utmpx* utx=NULL;

	while(1) {
		utx=NULL;		
		utx=getutxent();		
		if(NULL == utx) {			
			break;
		} else {
			if(USER_PROCESS == utx->ut_type) {
				const char *puser = utx->ut_user;
				char dname[MA_MAX_BUFFER_LEN] = {0};

				if(find_domain_name(puser,dname) || find_domain_name_via_dscl(puser,RECORD2,dname) || find_domain_name_via_dscl(puser,RECORD1,dname) ) {
					ma_user_t *user = NULL;

					if((user = (ma_user_t*)calloc(1, sizeof(ma_user_t)))) {
						strncpy(user->d.domain, dname, strlen(dname));
						MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "collected user (%s) domain(%s)", puser,  dname);
						user->is_domain_wide_str = MA_ASCII_STRING;
						strncpy(user->u.user, puser, strlen(puser));
						user->is_user_wide_str = MA_ASCII_STRING;
						user->type = MA_USER_SENSOR_LOG_ON_EVENT;
						user->state = MA_USER_LOGGED_STATE_ON;

						if(!map_find(&sensor->info_map, user)) {
							if(MA_OK == map_insert(&sensor->info_map, user)) {
								++sensor->info_map_size;
							}
						} else
							free(user);
					}
				}
			}
		}
	}
	setutxent();
	return;
}

void utmp_file_watcher_cb(uv_fs_event_t* handle, const char* filename, int events, int status) {
	if(handle) {
		ma_user_sensor_t *sensor = (ma_user_sensor_t*)handle->data;
		ma_error_t err = MA_OK;

		/* log off */
		ma_user_t **user_vector = NULL;
		ma_user_node_t *entry = NULL;
		size_t itr = 0;
		size_t old_info_map_size = sensor->info_map_size;

		MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "ubp file modified ....");
		if((user_vector = (ma_user_t**)calloc(sensor->info_map_size, sizeof(ma_user_t*)))) {
			RB_FOREACH(entry, ma_user_tree_s, &sensor->info_map) {
				if((user_vector[itr] = (ma_user_t*)calloc(1, sizeof(ma_user_t)))) {
					memcpy(user_vector[itr++], &entry->user, sizeof(ma_user_t));
				}
			}

			for(itr = 0; itr < old_info_map_size; ++itr) {
				map_erase(&sensor->info_map, user_vector[itr]);
				--sensor->info_map_size;
			}
			collect_logged_on_users(sensor);
			if(old_info_map_size >= sensor->info_map_size) {
				for(itr = 0; itr < old_info_map_size; ++itr) {
					if(!map_find(&sensor->info_map, user_vector[itr])) {
						ma_user_sensor_msg_t *usr_msg = NULL;

						if(MA_OK == (err = ma_user_sensor_msg_create(&usr_msg, 1))) {
							if(MA_OK == ma_user_set_attributes_to_msg(user_vector[itr], 0, usr_msg)) {
								ma_sensor_msg_t *msg = NULL;

								(void)ma_user_sensor_msg_set_event_type(usr_msg, MA_USER_SENSOR_LOG_OFF_EVENT);
								(void)ma_user_sensor_msg_set_logon_state(usr_msg, MA_USER_LOGGED_STATE_OFF);
								MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) logged off user(%s) domain(%s) ", sensor->name, user_vector[itr]->u.user, user_vector[itr]->d.domain);
								if(MA_OK == (err = ma_sensor_msg_create_from_user_sensor_msg(usr_msg, &msg))) {
									ma_sensor_on_msg((ma_sensor_t*)sensor, msg);
									(void)ma_sensor_msg_release(msg);
								}
							}
							(void)ma_user_sensor_msg_release(usr_msg);
						}
					}		
				}
			} else {

				collect_logged_on_users(sensor);
				for(itr = 0; itr < old_info_map_size; ++itr) {
					map_erase(&sensor->info_map, user_vector[itr]);
					--sensor->info_map_size;
				}
				RB_FOREACH(entry, ma_user_tree_s, &sensor->info_map) {
					ma_user_sensor_msg_t *usr_msg = NULL;
					if(MA_OK == (err = ma_user_sensor_msg_create(&usr_msg, 1))) {
						if(MA_OK == ma_user_set_attributes_to_msg(&entry->user, 0, usr_msg)) {
							ma_sensor_msg_t *msg = NULL;

							(void)ma_user_sensor_msg_set_event_type(usr_msg, MA_USER_SENSOR_LOG_ON_EVENT);
							(void)ma_user_sensor_msg_set_logon_state(usr_msg, MA_USER_LOGGED_STATE_ON);
							MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) logged on user(%s) domain(%s) ", sensor->name, entry->user.u.user, entry->user.d.domain);
							if(MA_OK == (err = ma_sensor_msg_create_from_user_sensor_msg(usr_msg, &msg))) {
								ma_sensor_on_msg((ma_sensor_t*)sensor, msg);
								(void)ma_sensor_msg_release(msg);
							}
						}
						(void)ma_user_sensor_msg_release(usr_msg);
					}
				}
				collect_logged_on_users(sensor);
			}
			for(itr = 0; itr < old_info_map_size; ++itr) {
				free(user_vector[itr]);
			}
			free(user_vector);
		}
		uv_fs_event_init(handle->loop, handle, filename, utmp_file_watcher_cb, 0);
	}
}

void utmp_file_watcher_close_cb(uv_handle_t *handle) {
	if(handle) {
		MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "user sensor closing file watcher");
		free(handle);
	}
}

ma_error_t utmp_file_watcher_init(ma_user_sensor_t *sensor) {
	if(sensor) {
		ma_error_t err = MA_OK;
		ma_event_loop_t *event_loop = ma_msgbus_get_event_loop(sensor->msgbus);

		if(event_loop) {
			uv_loop_t *loop = NULL;

			(void)ma_event_loop_get_loop_impl(event_loop, &loop);
			if(loop) {
				uv_fs_event_t *utmp_file_watcher = NULL;

				err = MA_ERROR_OUTOFMEMORY;
				if((utmp_file_watcher = (uv_fs_event_t*)calloc(1, sizeof(uv_fs_event_t)))) {
					struct stat sb = {0};

					stat(UTMPX_FILE, &sb);
					utmpx_file_size = sb.st_size;					
					utmp_file_watcher->data = sensor;
					sensor->data = utmp_file_watcher;
					MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "utmpx file(%s)", UTMPX_FILE);
					uv_fs_event_init(loop, utmp_file_watcher, UTMPX_FILE, utmp_file_watcher_cb, 0);

					err = MA_OK;
				}
			}
		}

		return err;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t utmp_file_watcher_deinit(ma_user_sensor_t *sensor) {
	if(sensor) {
		if(sensor->data) {
			uv_close((uv_handle_t*)sensor->data, utmp_file_watcher_close_cb);
		}
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

#endif

