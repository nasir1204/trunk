#include "ma/sensor/ma_sensor.h"
#include "ma/internal/defs/ma_sensor_defs.h"
#include "ma/ma_log.h"
#include "ma/sensor/ma_power_sensor.h"
#include "../ma_power_sensor_internal.h"
#include "ma/internal/ma_macros.h"

#include <stdlib.h>
#include <string.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "sensor"

extern ma_logger_t *sensor_logger;

ma_error_t power_sensor_on_msg(ma_sensor_t *sensor, ma_sensor_msg_t *msg) {
    if(sensor && msg) {
        ma_error_t err = MA_OK;

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t power_sensor_get_type(ma_sensor_t *sensor, ma_sensor_type_t *type) {
    if(sensor && type) {
        ma_power_sensor_t *user = (ma_power_sensor_t*)sensor;
        *type = user->type;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t power_sensor_get_id(ma_sensor_t *sensor, const char **id) {
    if(sensor && id) {
        ma_power_sensor_t *user = (ma_power_sensor_t*)sensor;
        *id = user->name;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t power_sensor_start(ma_sensor_t *sensor) {
    if(sensor) {
        ma_power_sensor_t *user = (ma_power_sensor_t*)sensor;
        user->state = MA_SENSOR_STATE_RUNNING;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t power_sensor_stop(ma_sensor_t *sensor) {
    if(sensor) {
        ma_power_sensor_t *user = (ma_power_sensor_t*)sensor;
        user->state = MA_SENSOR_STATE_STOPPED;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t power_sensor_release(ma_sensor_t *sensor) {
    if(sensor) {
        ma_error_t err = MA_OK;

        return err;
    }
    return MA_ERROR_INVALIDARG;
}
