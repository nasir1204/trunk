#include "ma/sensor/ma_sensor.h"
#include "ma/internal/defs/ma_sensor_defs.h"
#include "ma/ma_log.h"
#include "ma/sensor/ma_network_sensor.h"
#include "../ma_network_sensor_internal.h"
#include "ma/internal/ma_macros.h"
#include "ma/sensor/ma_sensor_msg.h"
#include "ma/sensor/ma_sensor_utils.h"
#include "ma/sensor/ma_sensor_msg_utils.h"

#include <stdlib.h>
#include <string.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "sensor"

extern ma_logger_t *sensor_logger;

ma_error_t network_sensor_on_msg(ma_sensor_t *sensor, ma_sensor_msg_t *msg) {
    if(sensor && msg) {
        ma_error_t err = MA_OK;
        ma_network_sensor_t *network = (ma_network_sensor_t*)sensor;
        ma_network_sensor_msg_t *network_msg = NULL;

        if(MA_OK == (err = ma_sensor_msg_get_network_sensor_msg(msg, &network_msg))) {
            ma_sensor_event_type_t type = MA_NETWORK_SENSOR_CONNECTIVITY_STATUS_CHANGE_EVENT;
            ma_message_t *pub_msg = NULL;
            
            (void)ma_network_sensor_msg_get_event_type(network_msg, &type);
            MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) received message(%s)", network->name,  ma_sensor_event_type_to_str(type));
            if(MA_OK == (err = ma_message_create(&pub_msg))) {
                ma_variant_t *msg_var = NULL;

                if(MA_OK == (err = convert_network_sensor_msg_to_variant(network_msg, &msg_var))) {
                    (void)ma_message_set_payload(pub_msg, msg_var);
                    if(MA_OK != (err = ma_sensor_publish_msg(network->msgbus, network->name, ma_sensor_event_type_to_str(type), pub_msg))) {
                        MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) publishing event(%s) failed last error(%d)", network->name, ma_sensor_event_type_to_str(type), err);
                    }
                    (void)ma_variant_release(msg_var);
                } else 
                    MA_LOG(sensor_logger, MA_LOG_SEV_ERROR, "sensor(%s) converting network sensor message to variant failed, last error(%d)", network->name, err);
                (void)ma_message_release(pub_msg);
            }
            (void)ma_network_sensor_msg_release(network_msg);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t network_sensor_get_type(ma_sensor_t *sensor, ma_sensor_type_t *type) {
    if(sensor && type) {
        ma_network_sensor_t *network = (ma_network_sensor_t*)sensor;
        *type = network->type;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t network_sensor_get_id(ma_sensor_t *sensor, const char **id) {
    if(sensor && id) {
        ma_network_sensor_t *network = (ma_network_sensor_t*)sensor;
        *id = network->name;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t network_sensor_start(ma_sensor_t *sensor) {
    if(sensor) {
        ma_network_sensor_t *network = (ma_network_sensor_t*)sensor;
        ma_error_t err = MA_OK;

        if(MA_SENSOR_STATE_NOT_STARTED == network->state || MA_SENSOR_STATE_RUNNING == network->state) {
            ma_network_sensor_msg_t *nw_msg = NULL;

            MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) starting", network->name);
            network->state = MA_SENSOR_STATE_RUNNING;
            if(MA_OK == (err = ma_sensor_get_network_sensor_msg(&nw_msg))) {
                ma_sensor_msg_t *msg = NULL;

                MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) posting current network status", network->name);
                (void)ma_network_sensor_msg_set_event_type(nw_msg, MA_NETWORK_SENSOR_CONNECTIVITY_STATUS_CHANGE_EVENT);
                if(MA_OK == (err = ma_sensor_msg_create_from_network_sensor_msg(nw_msg, &msg))) {
                    if(MA_OK != (err = ma_sensor_on_msg((ma_sensor_t*)network, msg)))
                        MA_LOG(sensor_logger, MA_LOG_SEV_ERROR, "failed to send network sensor msg, last error(%d)", err);
                    (void)ma_sensor_msg_release(msg);
                }
                (void)ma_network_sensor_msg_release(nw_msg);
            }
        }

        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t network_sensor_stop(ma_sensor_t *sensor) {
    if(sensor) {
        ma_network_sensor_t *network = (ma_network_sensor_t*)sensor;

        if(MA_SENSOR_STATE_RUNNING == network->state) {
            network->state = MA_SENSOR_STATE_STOPPED;
        }

        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t network_sensor_release(ma_sensor_t *sensor) {
    if(sensor) {
        ma_error_t err = MA_OK;

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t network_sensor_detect(ma_sensor_t *sensor) {
    if(sensor) {
        ma_error_t err = MA_OK;

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

