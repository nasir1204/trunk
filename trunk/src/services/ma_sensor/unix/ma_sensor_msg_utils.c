#include "ma/sensor/ma_sensor_msg_utils.h"
#include "ma/ma_log.h"

extern ma_logger_t *sensor_logger;

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "sensor"

#ifdef MACX
#include <CoreFoundation/CoreFoundation.h>
#include <SystemConfiguration/SystemConfiguration.h>
#endif


MA_SENSOR_API ma_error_t ma_sensor_get_power_sensor_msg(ma_power_sensor_msg_t **msg){
	if(msg){
		ma_error_t err = MA_OK;
		return err;
	}
	return MA_ERROR_INVALIDARG;
}
#ifdef MACX
static ma_bool_t get_link_status(SCDynamicStoreRef store, CFArrayRef changedKeys, void  *info) {
    CFIndex count = CFArrayGetCount(changedKeys);
    CFIndex i = 0;
    ma_bool_t status = MA_FALSE;

    for(i=0; i<count; ++i) {
        CFStringRef key = CFArrayGetValueAtIndex(changedKeys, i);
        CFDictionaryRef newValue = SCDynamicStoreCopyValue(store, key);
        if(newValue) {
            CFRelease(newValue);
            status = MA_TRUE;
            break;
        }
    }
    return  status;
}


#endif

static ma_bool_t get_network_connectivity_status() {
#ifdef MACX
        SCDynamicStoreRef dynStore;
        ma_bool_t status = 0;

        dynStore = SCDynamicStoreCreate(kCFAllocatorDefault,
                                  CFBundleGetIdentifier(CFBundleGetMainBundle()),
                                  NULL,
                                  NULL);

        CFArrayRef refKeys = SCDynamicStoreCopyKeyList(dynStore, CFSTR("State:/Network/Interface/en.*/IPv.*"));
            
        status = get_link_status(dynStore, refKeys, NULL);
		
	if(refKeys) CFRelease(refKeys);

        if(dynStore) CFRelease(dynStore);
        return status;
#else
    return MA_TRUE;
#endif
}

MA_SENSOR_API ma_error_t ma_sensor_get_network_sensor_msg(ma_network_sensor_msg_t **msg){
	if(msg){
		ma_error_t err = MA_OK;
		ma_network_sensor_msg_t *nw_msg = NULL;

		if(MA_OK == (err = ma_network_sensor_msg_create(&nw_msg, 1))){
                        (void)ma_network_sensor_msg_set_event_type(nw_msg, MA_NETWORK_SENSOR_CONNECTIVITY_STATUS_CHANGE_EVENT);
			(void)ma_network_sensor_msg_set_connectivity_status(nw_msg, get_network_connectivity_status() ? MA_NETWORK_CONNECTIVITY_STATUS_CONNECTED : MA_NETWORK_CONNECTIVITY_STATUS_DISCONNECTED);
			*msg = nw_msg;
		}
		return err;

	}
	return MA_ERROR_INVALIDARG;
}

