#include "ma/sensor/ma_sensor.h"
#include "ma/internal/defs/ma_sensor_defs.h"
#include "ma/ma_log.h"
#include "ma/sensor/ma_network_sensor.h"
#include "ma/internal/ma_macros.h"
#include "ma_network_sensor_internal.h"

#include <stdlib.h>
#include <string.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "sensor"

extern ma_logger_t *sensor_logger;

static const ma_sensor_methods_t network_methods = {
    &network_sensor_on_msg,
    &network_sensor_get_type,
    &network_sensor_get_id,
    &network_sensor_start,
    &network_sensor_stop,
    &network_sensor_release,
};


ma_error_t ma_network_sensor_create(ma_msgbus_t *msgbus, ma_network_sensor_t **sensor) {
    if(msgbus && sensor) {
        ma_network_sensor_t *network = NULL;
        ma_error_t err = MA_ERROR_OUTOFMEMORY;

        if((network = (ma_network_sensor_t*)calloc(1, sizeof(ma_network_sensor_t)))) {
            if(MA_OK == (err = ma_sensor_inc_ref((ma_sensor_t*)(*sensor = network)))) {
                ma_sensor_init((ma_sensor_t*)*sensor, &network_methods, network);
                strncpy(network->name, MA_SENSOR_TYPE_NETWORK_STR, strlen(MA_SENSOR_TYPE_NETWORK_STR));
                network->type = MA_SENSOR_TYPE_NETWORK;
                network->state = MA_SENSOR_STATE_NOT_STARTED;
                network->msgbus = msgbus;
                MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) created successfully", network->name);
            } else {
                MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) creation failed, last error(%d)", ma_sensor_type_to_str(MA_SENSOR_TYPE_NETWORK), err);
                free(network); *sensor = NULL;
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_network_sensor_release(ma_network_sensor_t *sensor) {
    if(sensor) {
        ma_error_t err = MA_OK;

        if(!((ma_sensor_t*)sensor)->ref_count) {
            MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) releasing...", sensor->name);
            free(sensor);
        } else {
            if(MA_OK != (err = ma_sensor_dec_ref((ma_sensor_t*)sensor)))
                MA_LOG(sensor_logger, MA_LOG_SEV_DEBUG, "sensor(%s) dec ref failed, last error(%d)", ma_sensor_type_to_str(MA_SENSOR_TYPE_NETWORK), err);
        }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}



