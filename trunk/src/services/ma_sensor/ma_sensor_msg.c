#include "ma/sensor/ma_sensor.h"
#include "ma/internal/defs/ma_sensor_defs.h"
#include "ma/ma_log.h"
#include "ma/sensor/ma_sensor_msg.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/threading/ma_atomic.h"
#include "ma/internal/utils/text/ma_utf8.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"

#include <stdlib.h>
#include <string.h>
#include <wchar.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "sensor message"

static ma_logger_t *msg_logger = NULL;
static ma_error_t ma_sensor_msg_destroy(ma_sensor_msg_t *msg);

struct ma_power_info_s {
    ma_power_status_t  ac_line_status;
    ma_battery_charge_status_t  battery_flag;
    int battery_life_percent;
    unsigned long battery_life_time;
};

struct ma_network_info_s {
    int dummy;
};

struct ma_idle_info_s {
    int idle_wait; /* idle wait in minutes */
};

struct ma_system_info_s {
    int dummy;
};

struct ma_power_sensor_msg_s {
    ma_sensor_event_type_t type;
    ma_atomic_counter_t ref_count;
    size_t size;
    ma_power_info_t settings[1];
};

struct ma_idle_sensor_msg_s {
    ma_sensor_event_type_t type;
    ma_atomic_counter_t ref_count;
    size_t size;
    ma_idle_info_t settings[1];
};

struct ma_network_sensor_msg_s {
    ma_network_connectivity_status_t status;
    ma_sensor_event_type_t type;
    ma_atomic_counter_t ref_count;
    size_t size;
    ma_network_info_t settings[1];
};

struct ma_system_sensor_msg_s {
    ma_sensor_event_type_t type;
    ma_atomic_counter_t ref_count;
    size_t size;
    ma_system_info_t settings[1];
};

struct ma_user_info_s {
    union {
        ma_buffer_t *user;
        ma_wbuffer_t *wuser;
    }u;
    union {
        ma_buffer_t *domain;
        ma_wbuffer_t *wdomain;
    }d;
    ma_uint32_t is_user_wide_str:1;
    ma_uint32_t is_domain_wide_str:1;
    ma_uint32_t sid;
};

struct ma_user_sensor_msg_s {
    ma_user_logged_state_t state;
    ma_sensor_event_type_t type;
    ma_atomic_counter_t ref_count;
    ma_bool_t startup_flag; /* enumerating users on agent start up */
    size_t size;
    ma_user_info_t settings[1];
};

struct ma_sensor_msg_s {
    union {
        ma_system_sensor_msg_t *obj_system;
        ma_power_sensor_msg_t *obj_power;
        ma_idle_sensor_msg_t *obj_idle;
        ma_network_sensor_msg_t *obj_network;
        ma_user_sensor_msg_t *obj_user;
    }sensor_object;
    ma_sensor_type_t type;
    ma_atomic_counter_t ref_count;
};


ma_error_t ma_sensor_msg_set_logger(ma_logger_t *logger) {
    if(logger) {
        msg_logger = logger;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_sensor_msg_destroy(ma_sensor_msg_t *msg) {
    if(msg) {
        ma_error_t err = MA_OK;

        switch(msg->type) {
            case MA_SENSOR_TYPE_USER: { 
                err = ma_user_sensor_msg_release(msg->sensor_object.obj_user);
                break;
            }
            case MA_SENSOR_TYPE_IDLE: {
                err = ma_idle_sensor_msg_release(msg->sensor_object.obj_idle);
                break;
            }
            case MA_SENSOR_TYPE_NETWORK: {
                err = ma_network_sensor_msg_release(msg->sensor_object.obj_network);
                break;
            }
            case MA_SENSOR_TYPE_POWER: {
                err = ma_power_sensor_msg_release(msg->sensor_object.obj_power);
                break;
            }
            case MA_SENSOR_TYPE_SYSTEM: {
                err = ma_system_sensor_msg_release(msg->sensor_object.obj_system);
                break;
            }
            case MA_SENSOR_TYPE_TRAY: {
                break;
            }
        }
        free(msg);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_sensor_msg_create(ma_sensor_msg_t **msg) {
    if(msg) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;

        if((*msg = (ma_sensor_msg_t*)calloc(1, sizeof(ma_sensor_msg_t)))) {
            if(MA_OK == (err = ma_sensor_msg_add_ref(*msg))) {
                (*msg)->type = MA_SENSOR_TYPE_TRAY;
            } else {
                free(*msg); *msg = NULL;
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_sensor_msg_add_ref(ma_sensor_msg_t *msg) {
    if(msg) {
        MA_ATOMIC_INCREMENT(msg->ref_count);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_sensor_msg_release(ma_sensor_msg_t *msg) {
    if(msg) {
        ma_error_t err = MA_OK;
        if(0 == MA_ATOMIC_DECREMENT(msg->ref_count)) {
            err = ma_sensor_msg_destroy(msg);
        }
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_sensor_msg_get_type(ma_sensor_msg_t *msg, ma_sensor_type_t *type) {
    if(msg && type) {
        *type = msg->type;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_sensor_msg_set_type(ma_sensor_msg_t *msg, ma_sensor_type_t type) {
    if(msg) {
        msg->type = (ma_sensor_type_t)type;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_user_sensor_msg_destroy(ma_user_sensor_msg_t *msg) {
    if(msg) {
        size_t itr = 0;
        for(itr = 0; itr < msg->size; ++itr) {
            if(MA_WCHAR_STRING == msg->settings[itr].is_user_wide_str) {
                if(msg->settings[itr].u.wuser)(void)ma_wbuffer_release(msg->settings[itr].u.wuser);
            } else {
                if(msg->settings[itr].u.user)(void)ma_buffer_release(msg->settings[itr].u.user);
            }
            if(MA_WCHAR_STRING == msg->settings[itr].is_domain_wide_str) {
                if(msg->settings[itr].d.wdomain)(void)ma_wbuffer_release(msg->settings[itr].d.wdomain);
            } else {
                if(msg->settings[itr].d.domain)(void)ma_buffer_release(msg->settings[itr].d.domain);
            }
        }
        free(msg);
        MA_LOG(msg_logger, MA_LOG_SEV_DEBUG, "user sensor message released successfully");
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_system_sensor_msg_destroy(ma_system_sensor_msg_t *msg) {
    if(msg) {
        MA_LOG(msg_logger, MA_LOG_SEV_DEBUG, "system sensor message refcount(%d)", msg->ref_count);
        free(msg);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_idle_sensor_msg_destroy(ma_idle_sensor_msg_t *msg) {
    if(msg) {
        MA_LOG(msg_logger, MA_LOG_SEV_DEBUG, "system idle sensor message destroyed");
        free(msg);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_network_sensor_msg_destroy(ma_network_sensor_msg_t *msg) {
    if(msg) {
        MA_LOG(msg_logger, MA_LOG_SEV_DEBUG, "network sensor destroyed");
        free(msg);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_power_sensor_msg_destroy(ma_power_sensor_msg_t *msg) {
    if(msg) {
        MA_LOG(msg_logger, MA_LOG_SEV_DEBUG, "power sensor message destroyed");
        free(msg);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

#define MA_USER_NETWORK_POWER_IDLE_SYSTEM_SENSOR_MSG_GENERATOR(sensor_type, SENSOR_TYPE) \
    ma_error_t ma_##sensor_type##_sensor_msg_create(ma_##sensor_type##_sensor_msg_t **msg, size_t cnt) {\
        if(msg) {\
            ma_error_t err = MA_ERROR_OUTOFMEMORY;\
            \
            if((*msg = (ma_##sensor_type##_sensor_msg_t*)calloc(1, sizeof(ma_##sensor_type##_sensor_msg_t) + (sizeof(ma_##sensor_type##_info_t) * (!cnt ? !cnt : cnt))))) {\
                (*msg)->size = !cnt ? !cnt : cnt;\
                if(MA_OK != (err = ma_##sensor_type##_sensor_msg_add_ref(*msg))) {\
                    free(*msg); *msg = NULL;\
                }\
            }\
            \
            return err;\
        }\
        return MA_ERROR_INVALIDARG;\
    }\
    ma_error_t ma_##sensor_type##_sensor_msg_release(ma_##sensor_type##_sensor_msg_t *msg) {\
        if(msg) {\
            ma_error_t err = MA_OK;\
            \
            MA_ATOMIC_DECREMENT(msg->ref_count);\
            if(!msg->ref_count) {\
                ma_##sensor_type##_sensor_msg_destroy(msg);\
            }\
            \
            return err;\
        }\
        return MA_ERROR_INVALIDARG;\
    }\
    ma_error_t ma_##sensor_type##_sensor_msg_add_ref(ma_##sensor_type##_sensor_msg_t *msg) {\
        if(msg) {\
            MA_ATOMIC_INCREMENT(msg->ref_count);\
            return MA_OK;\
        }\
        return MA_ERROR_INVALIDARG;\
    }\
    ma_error_t ma_sensor_msg_create_from_##sensor_type##_sensor_msg(ma_##sensor_type##_sensor_msg_t *msg, ma_sensor_msg_t **sensor_msg) {\
        if(msg && sensor_msg) {\
            ma_error_t err = MA_OK; \
            \
            if(MA_OK == (err = ma_sensor_msg_create(sensor_msg))) {\
                if(MA_OK != (err = ma_##sensor_type##_sensor_msg_add_ref(msg))) {\
                    (void)ma_sensor_msg_release(*sensor_msg); \
                    *sensor_msg = NULL; \
                    return err; \
                }\
                (*sensor_msg)->type = MA_SENSOR_TYPE_##SENSOR_TYPE; \
                (*sensor_msg)->sensor_object.obj_##sensor_type = msg; \
            } \
            \
            return err; \
        }\
        return MA_ERROR_INVALIDARG;\
    } \
    ma_error_t ma_sensor_msg_get_##sensor_type##_sensor_msg(ma_sensor_msg_t *sensor_msg, ma_##sensor_type##_sensor_msg_t **msg) {\
        if(sensor_msg && msg) {\
            ma_error_t err = MA_ERROR_PRECONDITION; \
            \
            if(sensor_msg->type == MA_SENSOR_TYPE_##SENSOR_TYPE) {\
                *msg = sensor_msg->sensor_object.obj_##sensor_type; \
                err = ma_##sensor_type##_sensor_msg_add_ref(*msg);\
            }\
            \
            return err; \
        }\
        return MA_ERROR_INVALIDARG;\
    }\
    ma_error_t ma_##sensor_type##_sensor_msg_set_event_type(ma_##sensor_type##_sensor_msg_t *msg, ma_sensor_event_type_t type) {\
        if(msg) {\
            msg->type = type;\
            return MA_OK;\
        }\
        return MA_ERROR_INVALIDARG;\
    }\
    ma_error_t ma_##sensor_type##_sensor_msg_get_event_type(ma_##sensor_type##_sensor_msg_t *msg, ma_sensor_event_type_t *type) {\
        if(msg && type) {\
            *type = msg->type;\
            return MA_OK;\
        }\
        return MA_ERROR_INVALIDARG;\
    }\
    ma_error_t ma_##sensor_type##_sensor_msg_get_size(ma_##sensor_type##_sensor_msg_t *msg, size_t *no_of_msgs) {\
        if(msg && no_of_msgs) {\
            *no_of_msgs = msg->size;\
            return MA_OK;\
        }\
        return MA_ERROR_INVALIDARG;\
    }\
    ma_error_t ma_##sensor_type##_sensor_msg_set_size(ma_##sensor_type##_sensor_msg_t *msg, size_t no_of_msgs) {\
        if(msg) {\
            msg->size = no_of_msgs? no_of_msgs : 1;\
            return MA_OK;\
        }\
        return MA_ERROR_INVALIDARG;\
    }

MA_USER_NETWORK_POWER_IDLE_SYSTEM_SENSOR_MSG_GENERATOR(power, POWER)
MA_USER_NETWORK_POWER_IDLE_SYSTEM_SENSOR_MSG_GENERATOR(idle, IDLE)
MA_USER_NETWORK_POWER_IDLE_SYSTEM_SENSOR_MSG_GENERATOR(system, SYSTEM)
MA_USER_NETWORK_POWER_IDLE_SYSTEM_SENSOR_MSG_GENERATOR(network, NETWORK)
MA_USER_NETWORK_POWER_IDLE_SYSTEM_SENSOR_MSG_GENERATOR(user, USER)

ma_error_t ma_user_sensor_msg_set_user(ma_user_sensor_msg_t *msg, size_t index, const char *user) {
    if(msg && user && (strlen(user) < MA_MAX_NAME)) {
        ma_error_t err = MA_ERROR_OUTOFBOUNDS;

        if(index < msg->size) {
            if(msg->settings[index].u.user)(void)ma_buffer_release(msg->settings[index].u.user);
            if(MA_OK == (err = ma_buffer_create(&msg->settings[index].u.user, strlen(user)))) {
                if(MA_OK == (err = ma_buffer_set(msg->settings[index].u.user, user, strlen(user)))) {
                    msg->settings[index].is_user_wide_str = MA_ASCII_STRING;
                } else {
                    (void)ma_buffer_release(msg->settings[index].u.user);
                }
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_user_sensor_msg_get_user(ma_user_sensor_msg_t *msg, size_t index, ma_buffer_t **user) {
    if(msg && user) {
        ma_error_t err = MA_ERROR_OUTOFBOUNDS;

        if(index < msg->size) {
            size_t len = 0;

            if(MA_ASCII_STRING == msg->settings[index].is_user_wide_str) {
                err = ma_buffer_add_ref(*user = msg->settings[index].u.user);
            } else {
                ma_temp_buffer_t tmp = MA_TEMP_BUFFER_INIT;
                const wchar_t *wbuf = NULL;

                if(MA_OK == (err = ma_wbuffer_get_string(msg->settings[index].u.wuser, &wbuf, &len))) {
                    ma_temp_buffer_reserve(&tmp, (len + 1) * sizeof(wchar_t));
                    if(MA_OK == (err = ma_convert_wide_char_to_utf8(wbuf, &tmp))) {
                        if(MA_OK == (err = ma_buffer_create(user, len))) {
                            if(MA_OK != (err = ma_buffer_set(*user, (char*)ma_temp_buffer_get(&tmp), len))) {
                                (void)ma_buffer_release(*user); *user = NULL;
                            }
                        }
                    }
                    ma_temp_buffer_uninit(&tmp);
                }
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_user_sensor_msg_set_domain(ma_user_sensor_msg_t *msg, size_t index, const char *domain) {
    if(msg && domain && (strlen(domain) < MA_MAX_NAME)) {
        ma_error_t err = MA_ERROR_OUTOFBOUNDS;

        if(index < msg->size) {
            if(msg->settings[index].d.domain)(void)ma_buffer_release(msg->settings[index].d.domain);
            if(MA_OK == (err = ma_buffer_create(&msg->settings[index].d.domain, strlen(domain)))) {
                if(MA_OK == (err = ma_buffer_set(msg->settings[index].d.domain, domain, strlen(domain)))) {
                    msg->settings[index].is_domain_wide_str = MA_ASCII_STRING;
                } else {
                    (void)ma_buffer_release(msg->settings[index].d.domain);
                }
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_user_sensor_msg_get_domain(ma_user_sensor_msg_t *msg, size_t index, ma_buffer_t **domain) {
    if(msg && domain) {
        ma_error_t err = MA_ERROR_OUTOFBOUNDS;

        if(index < msg->size) {
            size_t len = 0;

            if(MA_ASCII_STRING == msg->settings[index].is_domain_wide_str) {
                err = ma_buffer_add_ref(*domain = msg->settings[index].d.domain);
            } else {
                ma_temp_buffer_t tmp = MA_TEMP_BUFFER_INIT;
                const wchar_t *wbuf = NULL;

                if(MA_OK == (err = ma_wbuffer_get_string(msg->settings[index].d.wdomain, &wbuf, &len))) {
                    ma_temp_buffer_reserve(&tmp, (len + 1) * sizeof(wchar_t));
                    if(MA_OK == (err = ma_convert_wide_char_to_utf8(wbuf, &tmp))) {
                        if(MA_OK == (err = ma_buffer_create(domain, len))) {
                            if(MA_OK != (err = ma_buffer_set(*domain, (char*)ma_temp_buffer_get(&tmp), len))) {
                                (void)ma_buffer_release(*domain); *domain = NULL;
                            }
                        }
                    }
                    ma_temp_buffer_uninit(&tmp);
                }
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_user_sensor_msg_set_session_id(ma_user_sensor_msg_t *msg, size_t index, ma_uint32_t sid) {
    if(msg) {
        ma_error_t err = MA_ERROR_OUTOFBOUNDS;

        if(index < msg->size) {
            msg->settings[index].sid = sid; err = MA_OK;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_user_sensor_msg_get_session_id(ma_user_sensor_msg_t *msg, size_t index, ma_uint32_t *sid) {
    if(msg && sid) {
        ma_error_t err = MA_ERROR_OUTOFBOUNDS;

        if(index < msg->size) {
            *sid = msg->settings[index].sid; err = MA_OK;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_user_sensor_msg_set_logon_state(ma_user_sensor_msg_t *msg, ma_user_logged_state_t state) {
    if(msg) {
        msg->state = state;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_user_sensor_msg_get_logon_state(ma_user_sensor_msg_t *msg, ma_user_logged_state_t *state) {
    if(msg && state) {
        *state = msg->state;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_user_sensor_msg_set_startup_flag(ma_user_sensor_msg_t *msg, ma_bool_t flag) {
    if(msg) {
        msg->startup_flag = flag;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_user_sensor_msg_get_startup_flag(ma_user_sensor_msg_t *msg, ma_bool_t *flag) {
    if(msg && flag) {
        *flag = msg->startup_flag;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


#if MA_HAS_WCHAR_T
ma_error_t ma_user_sensor_msg_set_wuser(ma_user_sensor_msg_t *msg, size_t index, const wchar_t *user) {
    if(msg && user && (wcslen(user) < MA_MAX_NAME)) {
        ma_error_t err = MA_ERROR_OUTOFBOUNDS;

        if(index < msg->size) {
            if(msg->settings[index].u.wuser)(void)ma_wbuffer_release(msg->settings[index].u.wuser);
            if(MA_OK == (err = ma_wbuffer_create(&msg->settings[index].u.wuser, wcslen(user)))) {
                if(MA_OK == (err = ma_wbuffer_set(msg->settings[index].u.wuser, user, wcslen(user)))) {
                    msg->settings[index].is_user_wide_str = MA_WCHAR_STRING;
                } else {
                    (void)ma_wbuffer_release(msg->settings[index].u.wuser);
                }
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_user_sensor_msg_get_wuser(ma_user_sensor_msg_t *msg, size_t index, ma_wbuffer_t **user) {
    if(msg && user) {
        ma_error_t err = MA_ERROR_OUTOFBOUNDS;

        if(index < msg->size) {
            size_t len = 0;

            if(MA_WCHAR_STRING == msg->settings[index].is_user_wide_str) {
                err = ma_wbuffer_add_ref(*user = msg->settings[index].u.wuser);
            } else {
                ma_temp_buffer_t tmp = MA_TEMP_BUFFER_INIT;
                const char *buf = NULL;

                if(MA_OK == (err = ma_buffer_get_string(msg->settings[index].u.user, &buf, &len))) {
                    ma_temp_buffer_reserve(&tmp, (len + 1) * sizeof(char));
                    if(MA_OK == (err = ma_convert_utf8_to_wide_char(buf, &tmp))) {
                        if(MA_OK == (err = ma_wbuffer_create(user, len))) {
                            if(MA_OK != (err = ma_wbuffer_set(*user, (wchar_t*)ma_temp_buffer_get(&tmp), len))) {
                                (void)ma_wbuffer_release(*user); *user = NULL;
                            }
                        }
                    }
                    ma_temp_buffer_uninit(&tmp);
                }
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_user_sensor_msg_set_wdomain(ma_user_sensor_msg_t *msg, size_t index, const wchar_t *domain) {
    if(msg && domain && (wcslen(domain) < MA_MAX_NAME)) {
        ma_error_t err = MA_ERROR_OUTOFBOUNDS;

        if(index < msg->size) {
            if(msg->settings[index].d.wdomain)(void)ma_wbuffer_release(msg->settings[index].d.wdomain);
            if(MA_OK == (err = ma_wbuffer_create(&msg->settings[index].d.wdomain, wcslen(domain)))) {
                if(MA_OK == (err = ma_wbuffer_set(msg->settings[index].d.wdomain, domain, wcslen(domain)))) {
                    msg->settings[index].is_domain_wide_str = MA_WCHAR_STRING;
                } else {
                    (void)ma_wbuffer_release(msg->settings[index].d.wdomain);
                }
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_user_sensor_msg_get_wdomain(ma_user_sensor_msg_t *msg, size_t index, ma_wbuffer_t **domain) {
    if(msg && domain) {
        ma_error_t err = MA_ERROR_OUTOFBOUNDS;

        if(index < msg->size) {
            size_t len = 0;

            if(MA_WCHAR_STRING == msg->settings[index].is_domain_wide_str) {
                err = ma_wbuffer_add_ref(*domain = msg->settings[index].d.wdomain);
            } else {
                ma_temp_buffer_t tmp = MA_TEMP_BUFFER_INIT;
                const char *buf = NULL;

                if(MA_OK == (err = ma_buffer_get_string(msg->settings[index].d.domain, &buf, &len))) {
                    ma_temp_buffer_reserve(&tmp, (len + 1) * sizeof(char));
                    if(MA_OK == (err = ma_convert_utf8_to_wide_char(buf, &tmp))) {
                        if(MA_OK == (err = ma_wbuffer_create(domain, len))) {
                            if(MA_OK != (err = ma_wbuffer_set(*domain, (wchar_t*)ma_temp_buffer_get(&tmp), len))) {
                                (void)ma_wbuffer_release(*domain); *domain = NULL;
                            }
                        }
                    }
                    ma_temp_buffer_uninit(&tmp);
                }
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}
#endif

ma_error_t ma_user_sensor_msg_is_user_wide_char(ma_user_sensor_msg_t *msg, size_t index, ma_uint32_t *is_wide_char) {
    if(msg && is_wide_char) {
        ma_error_t err = MA_ERROR_OUTOFBOUNDS;

        if(index < msg->size) {
            *is_wide_char = msg->settings[index].is_user_wide_str; err = MA_OK;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_user_sensor_msg_is_domain_wide_char(ma_user_sensor_msg_t *msg, size_t index, ma_uint32_t *is_wide_char) {
    if(msg && is_wide_char) {
        ma_error_t err = MA_ERROR_OUTOFBOUNDS;

        if(index < msg->size) {
            *is_wide_char = msg->settings[index].is_domain_wide_str; err = MA_OK;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}



ma_error_t ma_power_sensor_msg_set_power_status(ma_power_sensor_msg_t *msg, size_t index, ma_power_status_t status) {
    if(msg) {
        ma_error_t err = MA_ERROR_OUTOFBOUNDS;

        if(index < msg->size) {
            msg->settings[index].ac_line_status = status; err = MA_OK;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_power_sensor_msg_get_power_status(ma_power_sensor_msg_t *msg, size_t index, ma_power_status_t *status) {
    if(msg && status) {
        ma_error_t err = MA_ERROR_OUTOFBOUNDS;

        if(index < msg->size) {
            *status = msg->settings[index].ac_line_status; err = MA_OK;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_power_sensor_msg_set_battery_charge_status(ma_power_sensor_msg_t *msg, size_t index, ma_battery_charge_status_t status) {
    if(msg) {
        ma_error_t err = MA_ERROR_OUTOFBOUNDS;

        if(index < msg->size) {
            msg->settings[index].battery_flag = status; err = MA_OK;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_power_sensor_msg_get_batter_charge_status(ma_power_sensor_msg_t *msg, size_t index, ma_battery_charge_status_t *status) {
    if(msg && status) {
        ma_error_t err = MA_ERROR_OUTOFBOUNDS;

        if(index < msg->size) {
            *status = msg->settings[index].battery_flag; err = MA_OK;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_power_sensor_msg_set_battery_lifetime(ma_power_sensor_msg_t *msg, size_t index, unsigned long secs) {
    if(msg) {
        ma_error_t err = MA_ERROR_OUTOFBOUNDS;

        if(index < msg->size) {
            msg->settings[index].battery_life_time = secs; err = MA_OK;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_power_sensor_msg_get_battery_lifetime(ma_power_sensor_msg_t *msg, size_t index, unsigned long *secs) {
    if(msg && secs) {
        ma_error_t err = MA_ERROR_OUTOFBOUNDS;

        if(index < msg->size) {
            *secs = msg->settings[index].battery_life_time; err = MA_OK;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_power_sensor_msg_set_battery_percent(ma_power_sensor_msg_t *msg, size_t index, int percentage) {
    if(msg) {
        ma_error_t err = MA_ERROR_OUTOFBOUNDS;

        if(index < msg->size) {
            msg->settings[index].battery_life_percent = percentage; err = MA_OK;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_power_sensor_msg_get_battery_percent(ma_power_sensor_msg_t *msg, size_t index, int *percentage) {
    if(msg && percentage) {
        ma_error_t err = MA_ERROR_OUTOFBOUNDS;

        if(index < msg->size) {
            *percentage = msg->settings[index].battery_life_percent; err = MA_OK;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

/* network sensor msg(s) */
ma_error_t ma_network_sensor_msg_set_connectivity_status(ma_network_sensor_msg_t *msg, ma_network_connectivity_status_t status) {
    if(msg) {
        msg->status = status;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_network_sensor_msg_get_connectivity_status(ma_network_sensor_msg_t *msg, ma_network_connectivity_status_t *status) {
    if(msg && status) {
        *status = msg->status;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

/* idle sensor msg(s) */
ma_error_t ma_idle_sensor_msg_set_idle_wait(ma_idle_sensor_msg_t *msg, size_t index, int idle_wait) {
    if(msg) {
        ma_error_t err = MA_ERROR_OUTOFBOUNDS;

        if(index < msg->size) {
            msg->settings[index].idle_wait = idle_wait; err = MA_OK;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_idle_sensor_msg_get_idle_wait(ma_idle_sensor_msg_t *msg, size_t index, int *idle_wait) {
    if(msg && idle_wait) {
        ma_error_t err = MA_ERROR_OUTOFBOUNDS;

        if(index < msg->size) {
            *idle_wait = msg->settings[index].idle_wait; err = MA_OK;
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}


