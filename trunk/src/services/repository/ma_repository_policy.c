#include "ma/internal/ma_macros.h"
#include "ma/internal/ma_strdef.h"

#include "ma/internal/utils/configurator/ma_configurator.h"

#include "ma/internal/utils/repository/ma_repository_internal.h"
#include "ma/internal/utils/proxy/ma_proxy_db_manager.h"
#include "ma/internal/utils/repository/ma_repository_db_manager.h"
#include "ma_repository_service_internal.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include "ma/internal/utils/proxy/ma_proxy_internal.h"
#include "ma/internal/utils/proxy/ma_proxy_config_internal.h"
#include "ma/internal/utils/macrypto/ma_crypto.h"

#include "ma/ma_log.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "Repository"

#define SAFE_FREE(x) {if(x){free(x);x= NULL;}}

#define MA_CONTEXT_GET_DATABASE(context) (ma_configurator_get_database(MA_CONTEXT_GET_CONFIGURATOR(context)))
#define MA_CONTEXT_GET_DATASTORE(context) (ma_configurator_get_datastore(MA_CONTEXT_GET_CONFIGURATOR(context)))


ma_error_t ma_repository_policy_create(ma_repository_service_t *service, ma_repository_policy_t **repository_policy, int hints) {
    if(service && repository_policy) {
        ma_error_t rc = MA_OK ;
        ma_repository_policy_t *self = (ma_repository_policy_t *)calloc(1, sizeof(ma_repository_policy_t)) ;
        if(self) {
            self->data = (void *)service ;
            if(MA_OK == (rc = ma_proxy_config_create(&self->proxy_config))) {
                (void)ma_repository_policy_update(self, hints) ;
                *repository_policy = self ;
                rc = MA_OK ;
            }
            else{
                (void)ma_repository_policy_release(self) ; self = NULL;
            }
            return rc ;
        }
        return MA_ERROR_OUTOFMEMORY ;
    }
    return MA_ERROR_INVALIDARG ;   	
}

ma_error_t ma_repository_policy_release(ma_repository_policy_t *self) {
    if(self) {
        if(self->proxy_config) {
            (void)ma_proxy_config_release(self->proxy_config) ;
            self->proxy_config = NULL ;
        }
        free(self) ; 
        self = NULL ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_repository_policy_set_proxy_config(ma_repository_policy_t *self, ma_proxy_config_t *proxy_config) {
	if(self && proxy_config) {
		if(self->proxy_config)	ma_proxy_config_release(self->proxy_config) ;
		self->proxy_config =  NULL ;

		self->proxy_config = proxy_config ;
		ma_proxy_config_add_ref(proxy_config) ;
	}
	return MA_ERROR_INVALIDARG ;
}

static ma_error_t ma_repository_crypto_decipher(ma_crypto_t *crypto, const unsigned char *data, unsigned int data_size, ma_bytebuffer_t *decrypted_data){
	ma_error_t rc = MA_ERROR_INVALIDARG;
	if(!data_size){
		ma_bytebuffer_set_bytes(decrypted_data, 0, "", 1);
		return MA_OK;
	}
	if(crypto && data_size > 0 && data && decrypted_data){
		if (data_size >=10 && memcmp(data,"EPOAES128:",10) == 0) {
            ma_crypto_decryption_t *decrypt = NULL;
            if(MA_OK == (rc = ma_crypto_decrypt_create(crypto , MA_CRYPTO_DECRYPTION_POLICY_SYM, &decrypt))) {
			    rc = ma_crypto_decrypt(decrypt, (unsigned char *)data, data_size, decrypted_data);
                (void)ma_crypto_decrypt_release(decrypt);
            }
        }		
	}
	return rc;
}

static ma_error_t adjust_spipe_site_order(ma_repository_service_t *service, ma_int32_t *site_order) {
    ma_error_t rc = MA_OK;
    ma_ah_sitelist_t *sitelist = NULL;
    ma_db_t *repo_db = MA_CONTEXT_GET_DATABASE(service->context) ;
    ma_ah_client_service_t *ah_client = MA_CONTEXT_GET_AH_CLIENT(service->context);
    ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(service->context) ;

    if(MA_OK == (rc = ma_ah_client_service_get_sitelist(ah_client, &sitelist))) {
        ma_uint32_t count = ma_ah_sitelist_get_count(sitelist);
        ma_uint32_t i = 0;
        for(i = 0; i < count; i++) {
            ma_ah_site_t *site = ma_ah_sitelist_get_site(i,sitelist);
            if(site) {
                const char *name = ma_ah_site_get_name(site);
                if(name) {
                    if(MA_OK == ma_repository_db_set_repository_sitelistorder(repo_db, name, *site_order)) {
                        MA_LOG(logger, MA_LOG_SEV_DEBUG, "spipe repository(%s) site order updated(%d).", name, *site_order);
                        *site_order += 1;
                    }
                }
                else
                    MA_LOG(logger, MA_LOG_SEV_WARNING, "Failed to get spipe site name");
            }
            else
                MA_LOG(logger, MA_LOG_SEV_WARNING, "Failed to get spipe site");
        }
    }  
    else
        MA_LOG(logger, MA_LOG_SEV_WARNING, "Failed to get spipe site list");

    return rc;
}

static ma_error_t apply_advance_section(ma_repository_policy_t *self) ;
static ma_error_t apply_proxysettings_section(ma_repository_policy_t *self) ;
static ma_error_t apply_inetmanager_section(ma_repository_policy_t *self) ;
static ma_error_t apply_sitelist_section(ma_repository_policy_t *self) ;

ma_error_t ma_repository_policy_update(ma_repository_policy_t *self, int hints) {
    if(self) {
        ma_error_t rc = MA_OK ;
		ma_bool_t cur_overwrite_client_sites_policy = self->overwrite_client_sites;
        ma_repository_service_t *service = (ma_repository_service_t *)self->data ;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(service->context) ;
		ma_db_t *repo_db = MA_CONTEXT_GET_DATABASE(service->context) ;


        MA_LOG(logger, MA_LOG_SEV_DEBUG, "configuring repository policy") ;
		
        /* Advanced section*/
		(void)apply_advance_section(self) ;
        
        /* ProxySettings section*/
		/*In case of service start OR  overwrite_client_sites is true, then only over ride proxy config from policy (as per 4.8) */
		if(self->overwrite_client_sites) {
			(void)apply_proxysettings_section(self) ;
			(void)ma_proxy_db_remove_proxy_config(repo_db);
			/*In case of system proxy, do not remove alternate proxies */
			if(self->proxy_config->proxy_usage_type == MA_PROXY_USAGE_TYPE_SYSTEM_CONFIGURED)
				(void)ma_proxy_db_remove_all_global_proxies(repo_db);
			else
				(void)ma_proxy_db_remove_all_proxies(repo_db);
			(void)ma_proxy_db_add_proxy_config(repo_db, self->proxy_config) ;
		}
		else {
			ma_proxy_config_t *proxy_config_tmp = NULL ;

			if(MA_ERROR_OBJECTNOTFOUND == ma_proxy_db_get_proxy_config(repo_db, &proxy_config_tmp)) {
				(void)apply_proxysettings_section(self) ;
				(void)ma_proxy_db_add_proxy_config(repo_db, self->proxy_config) ;
			}
			else {
				(void)ma_proxy_config_release(self->proxy_config) ;
				self->proxy_config = proxy_config_tmp ;
			}
		}

        /* site list/repositories section*/
		/*In case of service start OR  overwrite_client_sites is false, then do not delete local repo */
		if(MA_SERVICE_CONFIG_MODIFIED == hints && self->overwrite_client_sites)
			(void)ma_repository_db_set_local_repositories(repo_db);
            
		/*if(MA_SERVICE_CONFIG_MODIFIED == hints && self->overwrite_client_sites)
			(void)ma_repository_db_remove_all_local_repositories(repo_db) ;*/
		if(self->overwrite_client_sites)
			(void)apply_sitelist_section(self) ;
		else {
			if(cur_overwrite_client_sites_policy != self->overwrite_client_sites)
				(void)apply_sitelist_section(self) ;
		}
		
		if(MA_SERVICE_CONFIG_MODIFIED == hints && self->overwrite_client_sites)
			(void)ma_repository_db_remove_repositories_by_state(repo_db);

        /*InetManager section */
		(void)apply_inetmanager_section(self) ;

		return MA_OK ;
    }    
	return MA_ERROR_INVALIDARG;
}


ma_error_t apply_advance_section(ma_repository_policy_t *self) {
	ma_error_t rc = MA_OK ;
    ma_repository_service_t *service = (ma_repository_service_t *)self->data ;
	ma_policy_settings_bag_t *pso_bag = MA_CONTEXT_GET_POLICY_SETTINGS_BAG(service->context) ;

	if(MA_OK != ma_policy_settings_bag_get_bool(pso_bag, MA_REPOSITORY_ADVANCED_SECTION_NAME_STR, MA_REPOSITORY_KEY_OVERWRITE_CLIENT_REPOS_INT, MA_TRUE,&self->overwrite_client_sites, MA_FALSE))
        self->overwrite_client_sites = MA_FALSE ;

    if(MA_OK != ma_policy_settings_bag_get_int(pso_bag, MA_REPOSITORY_ADVANCED_SECTION_NAME_STR, MA_REPOSITORY_KEY_RANK_TYPE_INT,MA_TRUE ,(ma_int32_t *)&self->repository_ranking, MA_REPOSITORY_RANK_BY_SITELIST_ORDER))
        self->repository_ranking = MA_REPOSITORY_RANK_BY_SITELIST_ORDER ;

    if(MA_OK != ma_policy_settings_bag_get_int(pso_bag, MA_REPOSITORY_ADVANCED_SECTION_NAME_STR, MA_REPOSITORY_KEY_HOP_LIMIT_INT, MA_TRUE, &self->max_subnet_distance, 30))
        self->max_subnet_distance = 30 ;

    if(MA_OK != ma_policy_settings_bag_get_int(pso_bag, MA_REPOSITORY_ADVANCED_SECTION_NAME_STR, MA_REPOSITORY_KEY_PING_TIMEOUT_INT, MA_TRUE,&self->max_ping_time, 60))
        self->max_ping_time = 60 ;

	return MA_OK ;
}

ma_error_t apply_proxysettings_section(ma_repository_policy_t *self) {
	ma_error_t rc = MA_OK ;
        
    ma_repository_service_t *service = (ma_repository_service_t *)self->data ;
    ma_policy_settings_bag_t *pso_bag = MA_CONTEXT_GET_POLICY_SETTINGS_BAG(service->context) ;
    ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(service->context) ;
    ma_crypto_t *crypto = MA_CONTEXT_GET_CRYPTO(service->context);

    ma_crypto_encryption_t *encrypt = NULL ;        
    ma_bytebuffer_t *decrypt_byte_buffer = NULL ;
    ma_bytebuffer_t *encrypt_byte_buffer = NULL ;

	if(MA_OK != ma_policy_settings_bag_get_int(pso_bag, MA_REPOSITORY_PROXY_SECTION_NAME_STR, MA_REPOSITORY_PROXY_KEY_PROXY_USAGE_INT, MA_TRUE,(ma_int32_t *)&self->proxy_config->proxy_usage_type, MA_PROXY_USAGE_TYPE_NONE))
		self->proxy_config->proxy_usage_type = MA_PROXY_USAGE_TYPE_NONE ;

	if(MA_OK != ma_policy_settings_bag_get_bool(pso_bag, MA_REPOSITORY_PROXY_SECTION_NAME_STR, MA_REPOSITORY_PROXY_KEY_ENABLE_BYPASS_LOCAL_INT, MA_TRUE,&self->proxy_config->bypass_local_address, MA_FALSE))
		self->proxy_config->bypass_local_address = MA_FALSE ;

	if(MA_OK != ma_policy_settings_bag_get_bool(pso_bag, MA_REPOSITORY_PROXY_SECTION_NAME_STR, MA_REPOSITORY_PROXY_KEY_ENABLE_USER_CONFIG_INT, MA_TRUE,&self->proxy_config->allow_user_config, MA_FALSE))
		self->proxy_config->allow_user_config = MA_FALSE ;

	/* exclusion list */
	{
		ma_int32_t num_exceptions = 0 ;

		ma_policy_settings_bag_get_int(pso_bag, MA_REPOSITORY_PROXY_SECTION_NAME_STR, MA_REPOSITORY_PROXY_KEY_NUM_OF_EXCEPTIONS_INT, MA_TRUE,&num_exceptions, num_exceptions) ;

		if(num_exceptions) {
			int iter = 0 ;
			const char *exclusion_address = NULL ;
			ma_temp_buffer_t buffer = MA_TEMP_BUFFER_INIT, tmp_buffer = MA_TEMP_BUFFER_INIT ;

			for(iter = 0; iter < num_exceptions ; iter++) {
				char *p = NULL ;
				ma_temp_buffer_t exception_buffer = MA_TEMP_BUFFER_INIT ;

				ma_temp_buffer_reserve(&buffer, strlen(MA_REPOSITORY_PROXY_KEY_EXCEPTION_LIST_STR) + 10) ;
				MA_MSC_SELECT(_snprintf, snprintf)(p = (char*)ma_temp_buffer_get(&buffer), ma_temp_buffer_capacity(&buffer), "%s%d", MA_REPOSITORY_PROXY_KEY_EXCEPTION_LIST_STR, iter) ;
                    
				if(MA_OK == ma_policy_settings_bag_get_str(pso_bag, MA_REPOSITORY_PROXY_SECTION_NAME_STR, (const char*)ma_temp_buffer_get(&buffer), MA_TRUE,&exclusion_address,NULL)) {
					ma_temp_buffer_reserve(&exception_buffer, ma_temp_buffer_capacity(&tmp_buffer) + strlen(exclusion_address) + 2) ;
					MA_MSC_SELECT(_snprintf, snprintf)(p = (char*)ma_temp_buffer_get(&exception_buffer), ma_temp_buffer_capacity(&exception_buffer), "%s%c%s", (char*)ma_temp_buffer_get(&tmp_buffer), ';', exclusion_address) ;
					ma_temp_buffer_copy(&tmp_buffer, &exception_buffer) ;
					ma_temp_buffer_uninit(&exception_buffer) ;
				} 
			}
			(void)ma_proxy_config_set_exclusion_urls(self->proxy_config, (char*)ma_temp_buffer_get(&tmp_buffer)) ;
			ma_temp_buffer_uninit(&buffer) ;
			ma_temp_buffer_uninit(&tmp_buffer) ;
		}
		else
			self->proxy_config->exclusion_list = NULL ;
	}

	(void)ma_proxy_list_release(self->proxy_config->proxy_list) ;	self->proxy_config->proxy_list = NULL ;
	ma_proxy_list_create(&self->proxy_config->proxy_list) ;

	/* if system configured proxy, set the auth info to be used if specified */
	if(MA_PROXY_USAGE_TYPE_SYSTEM_CONFIGURED == self->proxy_config->proxy_usage_type) {
		const char *user = NULL, *passwd = NULL ;
		ma_bool_t is_auth_rqrd = MA_FALSE ;
		ma_bool_t use_single_proxy = MA_TRUE ;

		(void)ma_policy_settings_bag_get_bool(pso_bag, MA_REPOSITORY_PROXY_SECTION_NAME_STR, MA_REPOSITORY_PROXY_KEY_USE_HTTP_AUTH_INT, MA_TRUE, &is_auth_rqrd, is_auth_rqrd) ;
		(void)ma_policy_settings_bag_get_bool(pso_bag, MA_REPOSITORY_PROXY_SECTION_NAME_STR, MA_REPOSITORY_PROXY_KEY_USE_SINGLE_PROXY_INT, MA_TRUE,&use_single_proxy,use_single_proxy) ;

		if(MA_TRUE == is_auth_rqrd) {
			(void)ma_policy_settings_bag_get_str(pso_bag, MA_REPOSITORY_PROXY_SECTION_NAME_STR, MA_REPOSITORY_PROXY_KEY_HTTP_PROXY_USER_STR, MA_TRUE, &user,NULL) ;
			(void)ma_policy_settings_bag_get_str(pso_bag, MA_REPOSITORY_PROXY_SECTION_NAME_STR, MA_REPOSITORY_PROXY_KEY_HTTP_PROXY_FIPS_PASSWD_STR, MA_TRUE, &passwd, NULL) ;

			if(user && passwd &&  (MA_OK == (rc = ma_crypto_encrypt_create(crypto,MA_CRYPTO_ENCRYPTION_3DES, &encrypt)))) {					
				if(MA_OK == (rc = ma_bytebuffer_create(strlen(passwd)+1, &decrypt_byte_buffer))) {
					if(MA_OK ==(rc = ma_repository_crypto_decipher(crypto, (const unsigned char *)passwd, strlen(passwd),decrypt_byte_buffer))) {
						if(MA_OK == (rc = ma_bytebuffer_create(ma_bytebuffer_get_size(decrypt_byte_buffer), &encrypt_byte_buffer))) {
							if(MA_OK == (rc =ma_crypto_encrypt_and_encode(encrypt, ma_bytebuffer_get_bytes(decrypt_byte_buffer), ma_bytebuffer_get_size(decrypt_byte_buffer), encrypt_byte_buffer))) {
								unsigned char *passwd_str = (unsigned char*) calloc((ma_bytebuffer_get_size(encrypt_byte_buffer) + 1), sizeof(unsigned char) );
								memcpy (passwd_str, ma_bytebuffer_get_bytes(encrypt_byte_buffer), ma_bytebuffer_get_size(encrypt_byte_buffer));
								*(passwd_str + ma_bytebuffer_get_size(encrypt_byte_buffer)) ='\0';
								rc = ma_proxy_config_set_system_proxy_authentication(self->proxy_config, MA_PROXY_TYPE_HTTP, is_auth_rqrd, user, (const char *)passwd_str) ;
								if(MA_TRUE == use_single_proxy)
									rc = ma_proxy_config_set_system_proxy_authentication(self->proxy_config, MA_PROXY_TYPE_FTP, is_auth_rqrd, user, (const char *)passwd_str) ;
								free(passwd_str); passwd_str = NULL;
							}
							(void)ma_bytebuffer_release(encrypt_byte_buffer) ; encrypt_byte_buffer = NULL;
						}
					}
					(void)ma_bytebuffer_release(decrypt_byte_buffer) ; decrypt_byte_buffer = NULL;
				}
				(void)ma_crypto_encrypt_release(encrypt) ;	encrypt = NULL ;
			}
			if(MA_OK != rc) 
				MA_LOG(logger, MA_LOG_SEV_ERROR, "Crypto operation failed for http proxy password, rc = %d.", rc) ;				
		}
		else{
			ma_proxy_config_set_system_proxy_authentication(self->proxy_config, MA_PROXY_TYPE_HTTP, is_auth_rqrd, NULL, NULL) ;
			if(MA_TRUE == use_single_proxy)
				ma_proxy_config_set_system_proxy_authentication(self->proxy_config, MA_PROXY_TYPE_FTP, is_auth_rqrd, NULL, NULL) ;
		}
		if(!use_single_proxy) {
			is_auth_rqrd = MA_FALSE ;
			(void)ma_policy_settings_bag_get_bool(pso_bag, MA_REPOSITORY_PROXY_SECTION_NAME_STR, MA_REPOSITORY_PROXY_KEY_USE_FTP_AUTH_INT, MA_TRUE, &is_auth_rqrd, is_auth_rqrd) ;
			if(MA_TRUE == is_auth_rqrd) {
				const char *user = NULL, *passwd = NULL ;
				(void) ma_policy_settings_bag_get_str(pso_bag, MA_REPOSITORY_PROXY_SECTION_NAME_STR, MA_REPOSITORY_PROXY_KEY_FTP_PROXY_USER_STR,MA_TRUE, &user, NULL) ;
				(void)ma_policy_settings_bag_get_str(pso_bag, MA_REPOSITORY_PROXY_SECTION_NAME_STR, MA_REPOSITORY_PROXY_KEY_FTP_PROXY_FIPS_PASSWD_STR,MA_TRUE, &passwd,NULL) ;

						if(user && passwd && (MA_OK == (rc = ma_crypto_encrypt_create(crypto,MA_CRYPTO_ENCRYPTION_3DES, &encrypt)))) {
							if(MA_OK == (rc = ma_bytebuffer_create(strlen(passwd)+1, &decrypt_byte_buffer))) {
								if(MA_OK ==(rc = ma_repository_crypto_decipher(crypto, (const unsigned char *)passwd, strlen(passwd), decrypt_byte_buffer))) {
									if(MA_OK == (rc = ma_bytebuffer_create(ma_bytebuffer_get_size(decrypt_byte_buffer), &encrypt_byte_buffer))) {
										if(MA_OK == (rc = ma_crypto_encrypt_and_encode(encrypt, ma_bytebuffer_get_bytes(decrypt_byte_buffer), ma_bytebuffer_get_size(decrypt_byte_buffer), encrypt_byte_buffer))) {
											unsigned char *passwd_str = (unsigned char*) calloc((ma_bytebuffer_get_size(encrypt_byte_buffer) + 1), sizeof(unsigned char) );
											memcpy (passwd_str, ma_bytebuffer_get_bytes(encrypt_byte_buffer), ma_bytebuffer_get_size(encrypt_byte_buffer));
											*(passwd_str + ma_bytebuffer_get_size(encrypt_byte_buffer)) ='\0';
											rc = ma_proxy_config_set_system_proxy_authentication(self->proxy_config, MA_PROXY_TYPE_FTP, is_auth_rqrd, user, (const char *)passwd_str) ;
											free(passwd_str); passwd_str = NULL;
										}
										(void)ma_bytebuffer_release(encrypt_byte_buffer) ; encrypt_byte_buffer = NULL;
									}
								}
								(void)ma_bytebuffer_release(decrypt_byte_buffer) ; decrypt_byte_buffer = NULL;
							}
							ma_crypto_encrypt_release(encrypt) ;	encrypt = NULL ;
						}

				if(MA_OK != rc) {
					MA_LOG(logger, MA_LOG_SEV_ERROR, "Crypto operation failed for ftp proxy password, rc = %d.", rc) ;
				}
			}
			else
				ma_proxy_config_set_system_proxy_authentication(self->proxy_config, MA_PROXY_TYPE_FTP, is_auth_rqrd, NULL, NULL) ;
		}
	}
	else if(MA_PROXY_USAGE_TYPE_SERVER_CONFIGURED == self->proxy_config->proxy_usage_type) {
		ma_bool_t use_single_proxy = MA_TRUE, is_auth_rqrd = MA_FALSE ;
		const char *server = NULL, *user = NULL, *passwd = NULL ;
		ma_int32_t port = 0 ;
		ma_proxy_t *proxy = NULL ;

		(void)ma_policy_settings_bag_get_bool(pso_bag, MA_REPOSITORY_PROXY_SECTION_NAME_STR, MA_REPOSITORY_PROXY_KEY_USE_SINGLE_PROXY_INT, MA_TRUE, &use_single_proxy, use_single_proxy) ;

		/* HTTP proxy */
		(void)ma_policy_settings_bag_get_str(pso_bag, MA_REPOSITORY_PROXY_SECTION_NAME_STR, MA_REPOSITORY_PROXY_KEY_HTTP_PROXY_SERVER_URL_STR, MA_TRUE, &server, NULL) ;
		(void)ma_policy_settings_bag_get_int(pso_bag, MA_REPOSITORY_PROXY_SECTION_NAME_STR, MA_REPOSITORY_PROXY_KEY_HTTP_PROXY_PORT_INT, MA_TRUE, &port, 0) ;
		(void)ma_policy_settings_bag_get_bool(pso_bag, MA_REPOSITORY_PROXY_SECTION_NAME_STR, MA_REPOSITORY_PROXY_KEY_USE_HTTP_AUTH_INT, MA_TRUE,&is_auth_rqrd, is_auth_rqrd) ;
            
		if(MA_OK == ma_proxy_create(&proxy)) {
			(void)ma_proxy_set_address(proxy, server) ;
			(void)ma_proxy_set_port(proxy, port) ;
			(void)ma_proxy_set_protocol_type(proxy, MA_PROXY_TYPE_HTTP) ;

			if(is_auth_rqrd) {
				(void)ma_policy_settings_bag_get_str(pso_bag, MA_REPOSITORY_PROXY_SECTION_NAME_STR, MA_REPOSITORY_PROXY_KEY_HTTP_PROXY_USER_STR, MA_TRUE, &user, NULL) ;
				(void)ma_policy_settings_bag_get_str(pso_bag, MA_REPOSITORY_PROXY_SECTION_NAME_STR, MA_REPOSITORY_PROXY_KEY_HTTP_PROXY_FIPS_PASSWD_STR, MA_TRUE, &passwd, NULL) ;
 
				if(user && passwd && (MA_OK == (rc = ma_crypto_encrypt_create(crypto,MA_CRYPTO_ENCRYPTION_3DES, &encrypt)))) {
					if(MA_OK == (rc = ma_bytebuffer_create(strlen(passwd)+1, &decrypt_byte_buffer))) {
						if(MA_OK ==(rc = ma_repository_crypto_decipher(crypto, (const unsigned char *)passwd, strlen(passwd),  decrypt_byte_buffer))) {
							if(MA_OK == (rc = ma_bytebuffer_create(ma_bytebuffer_get_size(decrypt_byte_buffer), &encrypt_byte_buffer))) {
								if(MA_OK == (rc = ma_crypto_encrypt_and_encode(encrypt, ma_bytebuffer_get_bytes(decrypt_byte_buffer), ma_bytebuffer_get_size(decrypt_byte_buffer), encrypt_byte_buffer))) {
									unsigned char *passwd_str = (unsigned char*) calloc((ma_bytebuffer_get_size(encrypt_byte_buffer) + 1), sizeof(unsigned char) );
									memcpy (passwd_str, ma_bytebuffer_get_bytes(encrypt_byte_buffer), ma_bytebuffer_get_size(encrypt_byte_buffer));
									*(passwd_str + ma_bytebuffer_get_size(encrypt_byte_buffer)) ='\0';
									rc = ma_proxy_set_authentication(proxy, is_auth_rqrd, user, (const char *)passwd_str) ;
									free(passwd_str); passwd_str = NULL;
								}
								(void)ma_bytebuffer_release(encrypt_byte_buffer) ; encrypt_byte_buffer = NULL;
							}
						}
						(void)ma_bytebuffer_release(decrypt_byte_buffer) ; decrypt_byte_buffer = NULL;
					}
					(void)ma_crypto_encrypt_release(encrypt) ; encrypt = NULL ;
				}

				if(MA_OK != rc) {
					MA_LOG(logger, MA_LOG_SEV_ERROR, "Crypto operation failed for proxy password, rc = %d.", rc) ;
				}
			}else
				ma_proxy_set_authentication(proxy, is_auth_rqrd, NULL, NULL) ;
                
			(void)ma_proxy_set_flag(proxy, (ma_int32_t)MA_PROXY_FLAG_SERVER_CONFIGURED) ;
			//(void)ma_proxy_db_add_proxy(proxy_db, proxy) ;
			(void)ma_proxy_list_add_proxy(self->proxy_config->proxy_list, proxy) ;
			if(use_single_proxy) {
					(void)ma_proxy_set_protocol_type(proxy, MA_PROXY_TYPE_FTP) ;
					//(void)ma_proxy_db_add_proxy(proxy_db, proxy) ;
					(void)ma_proxy_list_add_proxy(self->proxy_config->proxy_list, proxy) ;
			}
			(void)ma_proxy_release(proxy) ; proxy = NULL ;
		}
		/* FTP proxy */
		if(!use_single_proxy) {
			(void)ma_policy_settings_bag_get_str(pso_bag, MA_REPOSITORY_PROXY_SECTION_NAME_STR, MA_REPOSITORY_PROXY_KEY_FTP_PROXY_SERVER_URL_STR, MA_TRUE, &server, NULL) ;
			(void)ma_policy_settings_bag_get_int(pso_bag, MA_REPOSITORY_PROXY_SECTION_NAME_STR, MA_REPOSITORY_PROXY_KEY_FTP_PROXY_PORT_INT, MA_TRUE, &port, 0) ;
			(void)ma_policy_settings_bag_get_bool(pso_bag, MA_REPOSITORY_PROXY_SECTION_NAME_STR, MA_REPOSITORY_PROXY_KEY_USE_FTP_AUTH_INT, MA_TRUE, &is_auth_rqrd, MA_FALSE) ;
                
			if(MA_OK == ma_proxy_create(&proxy)) {
				(void)ma_proxy_set_address(proxy, server) ;
				(void)ma_proxy_set_port(proxy, port) ;
				(void)ma_proxy_set_protocol_type(proxy, MA_PROXY_TYPE_FTP) ;

				if(is_auth_rqrd) {
					user = NULL; passwd = NULL;
					(void)ma_policy_settings_bag_get_str(pso_bag, MA_REPOSITORY_PROXY_SECTION_NAME_STR, MA_REPOSITORY_PROXY_KEY_FTP_PROXY_USER_STR, MA_TRUE, &user, NULL) ;
					(void)ma_policy_settings_bag_get_str(pso_bag, MA_REPOSITORY_PROXY_SECTION_NAME_STR, MA_REPOSITORY_PROXY_KEY_FTP_PROXY_FIPS_PASSWD_STR, MA_TRUE,&passwd, NULL) ;
						
						
					if(user && passwd && (MA_OK == (rc = ma_crypto_encrypt_create(crypto,MA_CRYPTO_ENCRYPTION_3DES, &encrypt)))) {
						if(MA_OK == (rc = ma_bytebuffer_create(strlen(passwd), &decrypt_byte_buffer))) {
							if(MA_OK ==(rc = ma_repository_crypto_decipher(crypto, (unsigned char *)passwd, strlen(passwd), decrypt_byte_buffer))) {
								if(MA_OK == (rc = ma_bytebuffer_create(ma_bytebuffer_get_size(decrypt_byte_buffer), &encrypt_byte_buffer))) {
									if(MA_OK == (rc = ma_crypto_encrypt_and_encode(encrypt, ma_bytebuffer_get_bytes(decrypt_byte_buffer), ma_bytebuffer_get_size(decrypt_byte_buffer), encrypt_byte_buffer))) {
										unsigned char *passwd_str = (unsigned char*) calloc((ma_bytebuffer_get_size(encrypt_byte_buffer) + 1), sizeof(unsigned char) );
										memcpy (passwd_str, ma_bytebuffer_get_bytes(encrypt_byte_buffer), ma_bytebuffer_get_size(encrypt_byte_buffer));
										*(passwd_str + ma_bytebuffer_get_size(encrypt_byte_buffer)) ='\0';
										rc = ma_proxy_set_authentication(proxy, is_auth_rqrd, user, (const char *)passwd_str) ;
										free(passwd_str); passwd_str = NULL;
									}
									(void)ma_bytebuffer_release(encrypt_byte_buffer) ;  encrypt_byte_buffer = NULL;
								}
							}
							(void)ma_bytebuffer_release(decrypt_byte_buffer) ;    decrypt_byte_buffer = NULL;
						}
						(void)ma_crypto_encrypt_release(encrypt) ;	encrypt = NULL ;
					}

					if(MA_OK != rc) {
						MA_LOG(logger, MA_LOG_SEV_ERROR, "Crypto operation failed for proxy password, rc = %d.", rc) ;
					}
				}else
					ma_proxy_set_authentication(proxy, is_auth_rqrd, NULL, NULL) ;

				(void)ma_proxy_set_flag(proxy, (ma_int32_t)MA_PROXY_FLAG_SERVER_CONFIGURED) ;
				//(void)ma_proxy_db_add_proxy(proxy_db, proxy) ;
				(void)ma_proxy_list_add_proxy(self->proxy_config->proxy_list, proxy) ;
				(void)ma_proxy_release(proxy) ;   proxy = NULL;
			}
		}
	}
	return rc ;
}

ma_error_t apply_sitelist_section(ma_repository_policy_t *self) {
	ma_error_t rc = MA_OK ;
        
    ma_repository_service_t *service = (ma_repository_service_t *)self->data ;
    ma_policy_settings_bag_t *pso_bag = MA_CONTEXT_GET_POLICY_SETTINGS_BAG(service->context) ;
    ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(service->context) ;
    ma_crypto_t *crypto = MA_CONTEXT_GET_CRYPTO(service->context);
	ma_db_t *repo_db = MA_CONTEXT_GET_DATABASE(service->context) ;

    ma_crypto_encryption_t *encrypt = NULL ;        
    ma_bytebuffer_t *decrypt_byte_buffer = NULL ;
    ma_bytebuffer_t *encrypt_byte_buffer = NULL ;

	ma_int32_t num_repos = 0 ;
    
	(void)ma_policy_settings_bag_get_int(pso_bag, MA_REPOSITORY_SITELIST_SECTION_NAME_STR, MA_REPOSITORY_SITELIST_KEY_NUM_OF_SITES_INT, MA_TRUE, &num_repos, num_repos) ;
	if(num_repos) {
        ma_int32_t iter = 0 ;
        ma_temp_buffer_t tmp_buffer = MA_TEMP_BUFFER_INIT ;

        const char *str_value = NULL;
		char *p = NULL ;
        ma_int32_t int_value =  0 ;
               
        for(iter = 0; iter < num_repos; iter++) {
            ma_repository_t *repository = NULL ;

			if(MA_OK == (rc = ma_repository_create(&repository)))
			{
				/* repo type */
				(void)ma_repository_set_type(repository, MA_REPOSITORY_TYPE_REPO) ;
				/* namespace */
				(void)ma_repository_set_namespace(repository, MA_REPOSITORY_NAMESPACE_LOCAL) ;        
				/* proxy usage */
				(void)ma_repository_set_proxy_usage_type(repository, MA_PROXY_USAGE_TYPE_NONE) ;

				/* repository  name*/
				ma_temp_buffer_reserve(&tmp_buffer, strlen(MA_REPOSITORY_SITELIST_KEY_NAME_STR) + 10) ;
				MA_MSC_SELECT(_snprintf, snprintf)(p = (char*)ma_temp_buffer_get(&tmp_buffer), ma_temp_buffer_capacity(&tmp_buffer), "%s%d", MA_REPOSITORY_SITELIST_KEY_NAME_STR, iter) ;
				(void)ma_policy_settings_bag_get_str(pso_bag, MA_REPOSITORY_SITELIST_SECTION_NAME_STR, p, MA_TRUE, &str_value, NULL) ;
				(void)ma_repository_set_name(repository, str_value) ; str_value = NULL ;

				/* url type */
				ma_temp_buffer_reserve(&tmp_buffer, strlen(MA_REPOSITORY_SITELIST_KEY_SITE_TYPE_INT) + 10) ;
				MA_MSC_SELECT(_snprintf, snprintf)(p = (char*)ma_temp_buffer_get(&tmp_buffer), ma_temp_buffer_capacity(&tmp_buffer), "%s%d", MA_REPOSITORY_SITELIST_KEY_SITE_TYPE_INT, iter) ;
				(void)ma_policy_settings_bag_get_int(pso_bag, MA_REPOSITORY_SITELIST_SECTION_NAME_STR, p, MA_TRUE, &int_value, 0) ;
				(void)ma_repository_set_url_type(repository, (ma_repository_url_type_t)int_value) ;

				/* enabled */
				ma_temp_buffer_reserve(&tmp_buffer, strlen(MA_REPOSITORY_SITELIST_KEY_IS_ENABLED_INT) + 10) ;
				MA_MSC_SELECT(_snprintf, snprintf)(p = (char*)ma_temp_buffer_get(&tmp_buffer), ma_temp_buffer_capacity(&tmp_buffer), "%s%d", MA_REPOSITORY_SITELIST_KEY_IS_ENABLED_INT, iter) ;
				(void)ma_policy_settings_bag_get_int(pso_bag, MA_REPOSITORY_SITELIST_SECTION_NAME_STR, p, MA_TRUE, &int_value, 0) ;
				(void)ma_repository_set_enabled(repository, (int_value == 0)?MA_FALSE:MA_TRUE) ;
                    
				/* server */
				ma_temp_buffer_reserve(&tmp_buffer, strlen(MA_REPOSITORY_SITELIST_KEY_SERVER_URL_STR) + 10) ;
				MA_MSC_SELECT(_snprintf, snprintf)(p = (char*)ma_temp_buffer_get(&tmp_buffer), ma_temp_buffer_capacity(&tmp_buffer), "%s%d", MA_REPOSITORY_SITELIST_KEY_SERVER_URL_STR, iter) ;
				(void)ma_policy_settings_bag_get_str(pso_bag, MA_REPOSITORY_SITELIST_SECTION_NAME_STR, p, MA_TRUE, &str_value, NULL) ;
				(void)ma_repository_set_server_fqdn(repository, str_value) ; str_value = NULL ;

				/* port */
				ma_temp_buffer_reserve(&tmp_buffer, strlen(MA_REPOSITORY_SITELIST_KEY_SERVER_PORT_INT) + 10) ;
				MA_MSC_SELECT(_snprintf, snprintf)(p = (char*)ma_temp_buffer_get(&tmp_buffer), ma_temp_buffer_capacity(&tmp_buffer), "%s%d", MA_REPOSITORY_SITELIST_KEY_SERVER_PORT_INT, iter) ;
				(void)ma_policy_settings_bag_get_int(pso_bag, MA_REPOSITORY_SITELIST_SECTION_NAME_STR, p, MA_TRUE, &int_value, 0) ;
				(void)ma_repository_set_port(repository, int_value) ;
                    
				/* path */
				if(repository->url_type == MA_REPOSITORY_URL_TYPE_UNC) {
					ma_temp_buffer_reserve(&tmp_buffer, strlen(MA_REPOSITORY_SITELIST_KEY_SHARE_NAME_STR) + 10) ;
					MA_MSC_SELECT(_snprintf, snprintf)(p = (char*)ma_temp_buffer_get(&tmp_buffer), ma_temp_buffer_capacity(&tmp_buffer), "%s%d", MA_REPOSITORY_SITELIST_KEY_SHARE_NAME_STR, iter) ;
				}
				else {
					ma_temp_buffer_reserve(&tmp_buffer, strlen(MA_REPOSITORY_SITELIST_KEY_RELATIVE_PATH_STR) + 10) ;
					MA_MSC_SELECT(_snprintf, snprintf)(p = (char*)ma_temp_buffer_get(&tmp_buffer), ma_temp_buffer_capacity(&tmp_buffer), "%s%d", MA_REPOSITORY_SITELIST_KEY_RELATIVE_PATH_STR, iter) ;
				}
				(void)ma_policy_settings_bag_get_str(pso_bag, MA_REPOSITORY_SITELIST_SECTION_NAME_STR, p, MA_TRUE, &str_value, NULL) ;
				(void)ma_repository_set_server_path(repository, str_value) ; str_value = NULL ;

				/* auth type */
				ma_temp_buffer_reserve(&tmp_buffer, strlen(MA_REPOSITORY_SITELIST_KEY_USE_AUTH_INT) + 10) ;
				MA_MSC_SELECT(_snprintf, snprintf)(p = (char*)ma_temp_buffer_get(&tmp_buffer), ma_temp_buffer_capacity(&tmp_buffer), "%s%d", MA_REPOSITORY_SITELIST_KEY_USE_AUTH_INT, iter) ;
				(void)ma_policy_settings_bag_get_int(pso_bag, MA_REPOSITORY_SITELIST_SECTION_NAME_STR, p, MA_TRUE, &int_value, 0) ;
				switch(repository->url_type) {
					case MA_REPOSITORY_URL_TYPE_HTTP :
						(void)ma_repository_set_authentication(repository, int_value?MA_REPOSITORY_AUTH_CREDENTIALS:MA_REPOSITORY_AUTH_NONE) ;
						break ;
					case MA_REPOSITORY_URL_TYPE_FTP :
						(void)ma_repository_set_authentication(repository, int_value?MA_REPOSITORY_AUTH_ANONYMOUS:MA_REPOSITORY_AUTH_CREDENTIALS) ;
						break ;
					case MA_REPOSITORY_URL_TYPE_UNC :
					case MA_REPOSITORY_URL_TYPE_LOCAL :
						(void)ma_repository_set_authentication(repository, int_value?MA_REPOSITORY_AUTH_LOGGEDON_USER:MA_REPOSITORY_AUTH_CREDENTIALS) ;
						break ;
					default :
						(void)ma_repository_set_authentication(repository, MA_REPOSITORY_AUTH_NONE ) ;
						break ;
				}
                    
				if(repository->auth_type == MA_REPOSITORY_AUTH_ANONYMOUS)
					(void)ma_repository_set_user_name(repository, "anonymous") ;
				else if(repository->auth_type == MA_REPOSITORY_AUTH_CREDENTIALS)
				{
					/* domain */
					unsigned char *passwd_str = NULL;
					ma_temp_buffer_reserve(&tmp_buffer, strlen(MA_REPOSITORY_SITELIST_KEY_DOMAIN_NAME_STR) + 10) ;
					MA_MSC_SELECT(_snprintf, snprintf)(p = (char*)ma_temp_buffer_get(&tmp_buffer), ma_temp_buffer_capacity(&tmp_buffer), "%s%d", MA_REPOSITORY_SITELIST_KEY_DOMAIN_NAME_STR, iter) ;
					(void)ma_policy_settings_bag_get_str(pso_bag, MA_REPOSITORY_SITELIST_SECTION_NAME_STR, p, MA_TRUE,&str_value, NULL) ;
					(void)ma_repository_set_domain_name(repository, str_value) ; str_value = NULL ;
                    
					/* user */
					ma_temp_buffer_reserve(&tmp_buffer, strlen(MA_REPOSITORY_SITELIST_KEY_USER_NAME_STR) + 10) ;
					MA_MSC_SELECT(_snprintf, snprintf)(p = (char*)ma_temp_buffer_get(&tmp_buffer), ma_temp_buffer_capacity(&tmp_buffer), "%s%d", MA_REPOSITORY_SITELIST_KEY_USER_NAME_STR, iter) ;
					(void)ma_policy_settings_bag_get_str(pso_bag, MA_REPOSITORY_SITELIST_SECTION_NAME_STR, p, MA_TRUE, &str_value, NULL) ; 
					(void)ma_repository_set_user_name(repository, str_value) ; str_value = NULL ;

					/* password */
					ma_temp_buffer_reserve(&tmp_buffer, strlen(MA_REPOSITORY_SITELIST_KEY_FIPS_PASSWD_STR) + 10) ;
					MA_MSC_SELECT(_snprintf, snprintf)(p = (char*)ma_temp_buffer_get(&tmp_buffer), ma_temp_buffer_capacity(&tmp_buffer), "%s%d", MA_REPOSITORY_SITELIST_KEY_FIPS_PASSWD_STR, iter) ;
					(void)ma_policy_settings_bag_get_str(pso_bag, MA_REPOSITORY_SITELIST_SECTION_NAME_STR, p, MA_TRUE, &str_value, NULL) ;

					if(str_value && (MA_OK == (rc = ma_crypto_encrypt_create(crypto,MA_CRYPTO_ENCRYPTION_3DES, &encrypt)))) {
						if(MA_OK == (rc = ma_bytebuffer_create(strlen(str_value)+1, &decrypt_byte_buffer))) {
							if(MA_OK ==(rc = ma_repository_crypto_decipher(crypto, (unsigned char *)str_value, strlen(str_value), decrypt_byte_buffer))) {
								if(MA_OK == (rc = ma_bytebuffer_create(ma_bytebuffer_get_size(decrypt_byte_buffer), &encrypt_byte_buffer))) {
									if(MA_OK ==  (rc = ma_crypto_encrypt_and_encode(encrypt, ma_bytebuffer_get_bytes(decrypt_byte_buffer), ma_bytebuffer_get_size(decrypt_byte_buffer), encrypt_byte_buffer))) {
										passwd_str = (unsigned char*) calloc((ma_bytebuffer_get_size(encrypt_byte_buffer) + 1), sizeof(unsigned char) );
										memcpy (passwd_str, ma_bytebuffer_get_bytes(encrypt_byte_buffer), ma_bytebuffer_get_size(encrypt_byte_buffer));
										*(passwd_str + ma_bytebuffer_get_size(encrypt_byte_buffer)) ='\0';
									}
									(void)ma_bytebuffer_release(encrypt_byte_buffer) ; encrypt_byte_buffer = NULL;
								}
							}
							(void)ma_bytebuffer_release(decrypt_byte_buffer) ;  decrypt_byte_buffer = NULL;
						}
						ma_crypto_encrypt_release(encrypt) ;	encrypt = NULL ;
					}
						
					(void)ma_repository_set_password(repository, (const char*)passwd_str) ;
					if(passwd_str) {
						free(passwd_str); passwd_str = NULL;
					}
					else {
						MA_LOG(logger, MA_LOG_SEV_ERROR, "Crypto operation failed for repository password, rc = %d.", rc) ;
					}
				}

				/* site list order */
				ma_temp_buffer_reserve(&tmp_buffer, strlen(MA_REPOSITORY_SITELIST_KEY_SITE_ORDER_INT) + 10) ;
				MA_MSC_SELECT(_snprintf, snprintf)(p = (char*)ma_temp_buffer_get(&tmp_buffer), ma_temp_buffer_capacity(&tmp_buffer), "%s%d", MA_REPOSITORY_SITELIST_KEY_SITE_ORDER_INT, iter) ;
				(void)ma_policy_settings_bag_get_int(pso_bag, MA_REPOSITORY_SITELIST_SECTION_NAME_STR, p, MA_TRUE,&int_value, 0) ;
				(void)ma_repository_set_siteorder(repository, int_value) ;
                    
				/* ping time, subnet distance, state */
				ma_repository_set_icmp_attributes(repo_db, self->repository_ranking, self->max_ping_time, self->max_subnet_distance, repository);

				if(MA_OK == ma_repository_validate(repository))
					if(MA_OK == ma_repository_db_add_repository(repo_db, repository))
						MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "added local repository %s", repository->name) ;
					else
						MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_ERROR, "failed to add local repository %s (%d)", repository->name, rc) ;
				else
					MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_ERROR, "repository(%s) validation failed(%d)", repository->name, rc) ;					
                   
				(void)ma_repository_release(repository); repository = NULL;
            }
			else {
				MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_ERROR, "repository create failed (%d)", rc) ;
			}
		}
    }
	return rc ;
}

ma_error_t apply_inetmanager_section(ma_repository_policy_t *self) {
	ma_repository_service_t *service = (ma_repository_service_t *)self->data ;
    ma_error_t rc = MA_OK ;

	ma_policy_settings_bag_t *pso_bag = MA_CONTEXT_GET_POLICY_SETTINGS_BAG(service->context) ;
    ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(service->context) ;
	ma_db_t *repo_db = MA_CONTEXT_GET_DATABASE(service->context) ;

    ma_int32_t num_disabled_repos = 0, num_sitelistorder = 0 ;
    ma_int32_t iter = 0, site_order = 1;
    ma_temp_buffer_t tmp_buffer = MA_TEMP_BUFFER_INIT ;
    const char *str_value = NULL;
	char *p = NULL ;
	
	if(self->overwrite_client_sites) {
		(void)ma_policy_settings_bag_get_int(pso_bag, MA_REPOSITORY_INETMANAGER_SECTION_NAME_STR, MA_REPOSITORY_INETMANAGER_KEY_DISABLED_SITES_NUM_INT, MA_TRUE, &num_disabled_repos, num_disabled_repos) ;
		/* new to update repositories to enabled state and then updated as per the policies */
		(void)ma_repository_db_set_all_repositories_enabled(repo_db, MA_TRUE) ;
		for(iter = 0; iter < num_disabled_repos; iter++) {
			ma_temp_buffer_reserve(&tmp_buffer, strlen(MA_REPOSITORY_INETMANAGER_KEY_DISABLED_SITES_STR) + 10) ;
			MA_MSC_SELECT(_snprintf, snprintf)(p = (char*)ma_temp_buffer_get(&tmp_buffer), ma_temp_buffer_capacity(&tmp_buffer), "%s%d", MA_REPOSITORY_INETMANAGER_KEY_DISABLED_SITES_STR, iter) ;
			(void)ma_policy_settings_bag_get_str(pso_bag, MA_REPOSITORY_INETMANAGER_SECTION_NAME_STR, p, MA_TRUE,&str_value, NULL) ;
			if(MA_OK == ma_repository_db_set_repository_enabled(repo_db, str_value, MA_FALSE))
				MA_LOG(logger, MA_LOG_SEV_DEBUG, "repository(%s) disabled.", str_value) ; str_value = NULL ;
		}
	}

	/* check and update agent handlers enable state based on epo */
	/* there is no way to differentiate between ah and epo sites, so if any spipe site is diabled, disable all spipe sites*/
	{
		ma_bool_t enable_state = MA_TRUE ;
		(void)ma_repository_db_get_epo_site_enable_state(repo_db, &enable_state) ;
		(void)ma_repository_db_set_ah_sites_enable_state(repo_db, enable_state) ;
	}
	
	(void)ma_policy_settings_bag_get_int(pso_bag, MA_REPOSITORY_INETMANAGER_SECTION_NAME_STR, MA_REPOSITORY_INETMANAGER_KEY_SITELIST_ORDER_NUM_INT, MA_TRUE,&num_sitelistorder, 0) ;

    for(iter = 0; iter < num_sitelistorder; iter++) {
        ma_repository_url_type_t url_type = MA_REPOSITORY_URL_TYPE_END;
        ma_temp_buffer_reserve(&tmp_buffer, strlen(MA_REPOSITORY_INETMANAGER_KEY_SITELIST_ORDER_STR) + 10) ;
        MA_MSC_SELECT(_snprintf, snprintf)(p = (char*)ma_temp_buffer_get(&tmp_buffer), ma_temp_buffer_capacity(&tmp_buffer), "%s%d", MA_REPOSITORY_INETMANAGER_KEY_SITELIST_ORDER_STR, iter) ;
        (void)ma_policy_settings_bag_get_str(pso_bag, MA_REPOSITORY_INETMANAGER_SECTION_NAME_STR, p, MA_TRUE, &str_value, NULL) ;
                
        if(MA_OK == (rc = ma_repository_db_get_url_type_by_name(repo_db, str_value, &url_type))) {
            if(MA_REPOSITORY_URL_TYPE_SPIPE == url_type) {
                if(MA_OK != (rc = adjust_spipe_site_order(service, &site_order)))
                    MA_LOG(logger, MA_LOG_SEV_WARNING, "Failed to adjust spipe site order, rc (%d).", rc);
            }
            else {
                if(MA_OK == ma_repository_db_set_repository_sitelistorder(repo_db, str_value, site_order++))
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "repository(%s) site order updated(%d).", str_value, site_order) ; str_value = NULL ;
            }
        }
        else
            MA_LOG(logger, MA_LOG_SEV_WARNING, "Failed to get url type for repo (%s), rc (%d).", str_value, rc);
    }
    ma_temp_buffer_uninit(&tmp_buffer) ;

	return rc ;
}