#ifndef MA_REPOSITORY_SERVICE_REPOKEYS_H_INCLUDED
#define MA_REPOSITORY_SERVICE_REPOKEYS_H_INCLUDED
#include "ma/ma_variant.h"

MA_CPP(extern "C" {)

#define MA_REPOKEYS_REPOKEYHASHLIST_SECTION					"RepoKeyHashList"
#define MA_REPOKEYS_KEYDATA_SECTION							"KeyData"
#define MA_REPOKEYS_VERSION_KEY_STR							"Version"

ma_error_t ma_repository_keys_process(ma_ds_t *ds, char *repo_keys, size_t repo_keys_size);
ma_error_t ma_repository_keys_get(ma_ds_t *ds, ma_variant_t **variant);

MA_CPP(})
#endif  /* MA_REPOSITORY_SERVICE_REPOKEYS_H_INCLUDED */

