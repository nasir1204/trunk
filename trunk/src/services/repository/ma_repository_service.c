#include "ma/internal/ma_macros.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"

#include "ma/ma_msgbus.h"
#include "ma/ma_message.h"

#include "ma_repository_service_internal.h"
#include "ma/internal/services/repository/ma_repository_spipe_manager.h"
#include "ma/internal/services/repository/ma_repository_rank.h"
#include "ma/internal/services/repository/ma_repository_policy.h"
#include "ma/internal/utils/repository/ma_repository_db_manager.h"
#include "ma/internal/utils/repository/ma_repository_internal.h"
#include "ma/internal/utils/proxy/ma_proxy_db_manager.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/services/ma_agent_policy_settings_bag.h"
#include "ma/internal/utils/repository/ma_sitelist.h"
#include "ma_repository_repokeys_handler.h"
#include "ma/internal/utils/proxy/ma_proxy_internal.h"
#include "ma/ma_log.h"

#include "ma/internal/services/scheduler/ma_scheduler_core.h"

#include "ma/internal/services/repository/ma_mirror_request_param.h"
#include "ma/repository/ma_repository_client_defs.h"
#include "ma/repository/ma_repository.h"
#include "ma/internal/utils/configurator/ma_configurator_utils.h"
#include "ma/../../src/utils/configurator/ma_configurator_internal.h"
#include "ma/internal/utils/repository/ma_repository_private.h"
#include "ma/internal/utils/proxy/ma_proxy_config_internal.h"
/* #include "ma/internal/utils/firewall_exceptions/ma_firewall_exceptions.h" */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "Repository"

#define MA_CONTEXT_GET_DATABASE(context) (ma_configurator_get_database(MA_CONTEXT_GET_CONFIGURATOR(context)))
#define MA_CONTEXT_GET_DATASTORE(context) (ma_configurator_get_datastore(MA_CONTEXT_GET_CONFIGURATOR(context)))

/*Utils function to handle different messages.*/
static ma_error_t ma_repository_get_repositories_request_handle(ma_repository_service_t *service, const char *filter, ma_variant_t **repositories) ;
static ma_error_t ma_repository_update_repositories_request_handle(ma_repository_service_t *service, ma_variant_t *repositories) ;
static ma_error_t ma_repository_import_repositories_request_handle(ma_repository_service_t *service, ma_variant_t *repositories) ;
static ma_error_t ma_repository_get_proxies_request_handle(ma_repository_service_t *service, ma_variant_t **proxies) ;
static ma_error_t ma_repository_get_system_proxies_request_handle(ma_repository_service_t *service, char *url, ma_variant_t **sys_proxies_var);
static ma_error_t ma_repository_update_proxies_request_handle(ma_repository_service_t *service, ma_variant_t *proxies) ;
static ma_error_t ma_repository_mirror_task_request_handle(ma_repository_service_t *service, ma_variant_t *req_payload, ma_variant_t **response_payload);

static ma_error_t ma_repository_subscriber_handler(const char *topic, ma_message_t *message, void *cb_data);

static ma_error_t ma_repository_service_configure(ma_repository_service_t *self, ma_context_t *context, int hints);
static ma_error_t ma_repository_service_start(ma_repository_service_t *self);
static ma_error_t ma_repository_service_stop(ma_repository_service_t *self);
static ma_error_t ma_repository_service_destroy(ma_repository_service_t *self);

static ma_service_methods_t base_methods = {
    &ma_repository_service_configure,
    &ma_repository_service_start,
    &ma_repository_service_stop,
    &ma_repository_service_destroy
} ;

ma_error_t ma_repository_service_create(const char *service_name, ma_service_t **service) {
    if(service_name && service) {
        ma_error_t rc = MA_OK ;
        ma_repository_service_t *self = (ma_repository_service_t *)calloc(1, sizeof(ma_repository_service_t)) ;

        if(!self) return MA_ERROR_OUTOFMEMORY ;
        ma_service_init(&self->base, &base_methods, service_name, self) ;        
        *service = &self->base;
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

static ma_error_t ma_repository_service_deinit(ma_repository_service_t *self) {
    if(self) {
        if(self->repo_policy) ma_repository_policy_release(self->repo_policy) ;
		self->repo_policy  = NULL;

        if(self->repo_rank) {
			/*if(MA_TRUE == ma_repository_rank_get_state(self->repo_rank)){
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Ranking of repository stopping") ;
                    (void *)ma_repository_rank_stop(self->repo_rank) ;
			}*/
			(void)ma_repository_rank_release(self->repo_rank) ;
			self->repo_rank = NULL;
		}

        if(self->repo_spipe_manager)  (void)ma_repository_spipe_manager_release(self->repo_spipe_manager) ;
		self->repo_spipe_manager = NULL;

		if(self->subscriber)  (void)ma_msgbus_subscriber_release(self->subscriber) ;
		self->subscriber = NULL;

		if(self->server)  (void)ma_msgbus_server_release(self->server) ;
		self->server = NULL;

		self->context = NULL;

        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_repository_service_configure(ma_repository_service_t *self, ma_context_t *context, int hints) {
    if(context && self) {
		ma_error_t rc = MA_OK;

		self->context = context ;

		if(MA_SERVICE_CONFIG_NEW == hints){			
			self->logger = MA_CONTEXT_GET_LOGGER(context);

			if(MA_OK == (rc = ma_msgbus_server_create(MA_CONTEXT_GET_MSGBUS(self->context), MA_REPOSITORY_SERVICE_NAME_STR, &self->server))) {
				if(MA_OK == (rc = ma_msgbus_subscriber_create(MA_CONTEXT_GET_MSGBUS(self->context), &self->subscriber))) {
					if(MA_OK == (rc = ma_repository_spipe_manager_create(self, &self->repo_spipe_manager))) {
						if(MA_OK == (rc = ma_repository_db_create_schema( MA_CONTEXT_GET_DATABASE(self->context) ))) {
							if(MA_OK == (rc = ma_repository_rank_create(self, &self->repo_rank))) {
								if(MA_OK == (rc = ma_proxy_db_create_schema( MA_CONTEXT_GET_DATABASE(self->context)  ))) {	
									if(MA_OK == (rc = ma_repository_policy_create(self, &self->repo_policy, hints))) {
										MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO, "Successfully configured the repository service.") ;
									}
								}
							}
						}
					}
				}
			}
			if(MA_OK != rc){
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to configure the repository service.") ;
				ma_repository_service_deinit(self);				
			}			
		}		
		if(MA_SERVICE_CONFIG_MODIFIED == hints && self->context){
			ma_repository_rank_type_t old_repository_ranking =  self->repo_policy->repository_ranking;
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Reconfiguring the repository service.") ;
            if(MA_OK == (rc = ma_repository_policy_update(self->repo_policy, hints))) {
				if(old_repository_ranking != self->repo_policy->repository_ranking) { /* policy changed, mark repo as altered so that icmp module can pick up those repo */
					ma_repository_db_set_all_repository_state(MA_CONTEXT_GET_DATABASE(self->context), MA_REPOSITORY_STATE_ALTERED);
				}
				if( (self->repo_policy->repository_ranking == MA_REPOSITORY_RANK_BY_PING_TIME) || (self->repo_policy->repository_ranking == MA_REPOSITORY_RANK_BY_SUBNET_DISTANCE) ) {
					if(MA_TRUE == ma_repository_rank_get_state(self->repo_rank)){
						MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Ranking of repository stopping") ;
						(void *)ma_repository_rank_stop(self->repo_rank) ;
					}
				
					ma_repository_rank_start(self->repo_rank, self->repo_policy->repository_ranking);
				}
            }
            else
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Reconfiguring the repository policy failed.") ;
		}
		return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

static ma_error_t ma_repository_service_handler(ma_msgbus_server_t *server, ma_message_t *request, void *cb_data, ma_msgbus_client_request_t *c_request) ;

ma_error_t ma_repository_service_start(ma_repository_service_t *self) {
    if(self) {
        ma_error_t rc = MA_ERROR_PRECONDITION ;
		if(self->context){
			(void)ma_msgbus_server_setopt(self->server, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO) ;
			(void)ma_msgbus_server_setopt(self->server, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC) ; 
			if(MA_OK == (rc = ma_msgbus_server_start(self->server, ma_repository_service_handler, (void *)self))){
				(void)ma_msgbus_subscriber_setopt(self->subscriber, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO) ;
				(void)ma_msgbus_subscriber_setopt(self->subscriber, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC) ; 
				/* TBD - Will change the general subscription to specific ma.task.* topic */
				if(MA_OK == (rc = ma_msgbus_subscriber_register(self->subscriber, MA_GENERAL_SUBSCRIBER_TOPIC_STR, ma_repository_subscriber_handler, (void *)self))) {
					(void)ma_ah_client_service_register_spipe_decorator( MA_CONTEXT_GET_AH_CLIENT(self->context), ma_repository_spipe_manager_get_decorator(self->repo_spipe_manager));
					(void)ma_ah_client_service_register_spipe_handler( MA_CONTEXT_GET_AH_CLIENT(self->context), ma_repository_spipe_manager_get_handler(self->repo_spipe_manager));
				}
			}
		}		
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_repository_service_stop(ma_repository_service_t *self) {
    if(self) {
		ma_error_t rc = MA_ERROR_PRECONDITION;
		if(self->context){
            if(self->mirror_request) {
				if(MA_OK == (rc = ma_mirror_request_stop(self->mirror_request))) {
					rc = ma_mirror_request_release(self->mirror_request);
					self->mirror_request = NULL;
                }
		    }
			if(MA_TRUE == ma_repository_rank_get_state(self->repo_rank))
				(void)ma_repository_rank_stop(self->repo_rank) ;
			(void)ma_ah_client_service_unregister_spipe_decorator( MA_CONTEXT_GET_AH_CLIENT(self->context), ma_repository_spipe_manager_get_decorator(self->repo_spipe_manager));
			(void)ma_ah_client_service_unregister_spipe_handler( MA_CONTEXT_GET_AH_CLIENT(self->context), ma_repository_spipe_manager_get_handler(self->repo_spipe_manager));				
			(void)ma_msgbus_subscriber_unregister(self->subscriber);
			return ma_msgbus_server_stop(self->server) ;
		}
		return rc;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_repository_service_destroy(ma_repository_service_t *self) {
    if(self) {
		ma_repository_service_deinit(self);
        free(self) ; self = NULL ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

static ma_error_t ma_update_repositories_in_db(ma_repository_t *, void *) ;

ma_error_t ma_repository_service_handler(ma_msgbus_server_t *server, ma_message_t *request, void *cb_data, ma_msgbus_client_request_t *c_request) {
    if(server && request && c_request) {
        ma_error_t rc = MA_OK ;
        const char *msg_type = NULL ;
        ma_repository_service_t *service = (ma_repository_service_t *)cb_data ;

        (void)ma_message_get_property(request, MA_REPOSITORY_MSG_KEY_TYPE_STR , &msg_type) ;
        if(msg_type) 
        {
            ma_message_t *response = NULL ;
			ma_icmp_context_t *icmp_context = NULL;
            
		    (void)ma_message_create(&response) ;

            /* Get repositories request */
            if(!strcmp(MA_REPOSITORY_MSG_PROP_VAL_RQST_GET_REPOSITORIES_STR, msg_type)) 
            {
                ma_variant_t *response_payload = NULL ;
				char *filter = NULL ;

                MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "Received get repositories request") ;
                
				(void)ma_message_get_property(request, MA_REPOSITORY_MSG_PROP_KEY_FILTER_STR, &filter) ;

                rc = ma_repository_get_repositories_request_handle(service, filter, &response_payload) ;
                
                MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "posting result for get repositories request(%d)", rc) ;
                
				(void)ma_message_set_property(response, MA_REPOSITORY_MSG_KEY_TYPE_STR, MA_REPOSITORY_MSG_PROP_VAL_RPLY_GET_REPOSITORIES_STR) ;
				(void)ma_message_set_payload(response, response_payload) ;

                rc = ma_msgbus_server_client_request_post_result(c_request, rc, response) ;
				
				(void)ma_variant_release(response_payload) ;
            }
            /* Get repokeys request */
			else if(!strcmp(MA_REPOSITORY_MSG_PROP_VAL_RQST_GET_REPOKEYS_STR, msg_type)) 
            {
				ma_variant_t *response_payload = NULL ;

				MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "Received get repokeys request") ;
                (void)ma_message_set_property(response, MA_REPOSITORY_MSG_KEY_TYPE_STR, MA_REPOSITORY_MSG_PROP_VAL_RPLY_GET_REPOKEYS_STR) ;
                if(MA_OK == (rc = ma_repository_keys_get(MA_CONTEXT_GET_DATASTORE(service->context), &response_payload))) {
					(void)ma_message_set_payload(response, response_payload) ;
					(void)ma_variant_release(response_payload) ;
				}
                MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "posting result for get repo keys request(%d)", rc) ;
                rc = ma_msgbus_server_client_request_post_result(c_request, rc, response) ;                
            }
            /* Update repositories request */
            else if (!strcmp(MA_REPOSITORY_MSG_PROP_VAL_RQST_UPDATE_REPOSITORIES_STR, msg_type)) 
            {
                ma_variant_t *req_payload = NULL ;

                MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "Received update repositories request") ;
                
                (void)ma_message_set_property(response, MA_REPOSITORY_MSG_KEY_TYPE_STR, MA_REPOSITORY_MSG_PROP_VAL_RPLY_UPDATE_REPOSITORIES_STR) ;
                
                MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "posting result for update repositories request") ;

                (void)ma_message_get_payload(request, &req_payload) ;
              
                if(req_payload) 
                {
                    rc = ma_repository_update_repositories_request_handle(service, req_payload) ;
                    (void)ma_variant_release(req_payload) ;
                }
                else
                    MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "No payload in update repositories request") ;

				if(MA_OK != ma_msgbus_server_client_request_post_result(c_request, rc, response))
                    MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "posting result for update repositories request, failed") ;

                if(MA_TRUE == ma_repository_rank_get_state(service->repo_rank)){
					MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "Ranking of repository stopping") ;
					(void *)ma_repository_rank_stop(service->repo_rank) ;
				}
                if((service->repo_policy->repository_ranking == MA_REPOSITORY_RANK_BY_PING_TIME || service->repo_policy->repository_ranking == MA_REPOSITORY_RANK_BY_SUBNET_DISTANCE) ) {
                    if(MA_OK == ma_repository_rank_start(service->repo_rank, service->repo_policy->repository_ranking)) { 
                        MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "Ranking of repository starting with policy [%d]", service->repo_policy->repository_ranking) ;
                    }
                }
            }
			/* Import repositories request */
            else if (!strcmp(MA_REPOSITORY_MSG_PROP_VAL_RQST_IMPORT_REPOSITORIES_STR, msg_type)) 
            {
                ma_variant_t *req_payload = NULL ;

                MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "Received import repositories request") ;
                
                (void)ma_message_set_property(response, MA_REPOSITORY_MSG_KEY_TYPE_STR, MA_REPOSITORY_MSG_PROP_VAL_RPLY_IMPORT_REPOSITORIES_STR) ;
                
                MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "posting result for import repositories request") ;

                (void)ma_message_get_payload(request, &req_payload) ;
                
                if(req_payload) 
                {
                    rc = ma_repository_import_repositories_request_handle(service, req_payload) ;
                    (void)ma_variant_release(req_payload) ;
                }
                else
                    MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "No payload in update repositories request") ;

				if(MA_OK != ma_msgbus_server_client_request_post_result(c_request, rc, response))
                    MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "posting result for import repositories request, failed") ;

				if(MA_TRUE == ma_repository_rank_get_state(service->repo_rank)){
					MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "Ranking of repository stopping") ;
					(void *)ma_repository_rank_stop(service->repo_rank) ;
				}
				if((service->repo_policy->repository_ranking == MA_REPOSITORY_RANK_BY_PING_TIME || service->repo_policy->repository_ranking == MA_REPOSITORY_RANK_BY_SUBNET_DISTANCE) ) {
					if(MA_OK == ma_repository_rank_start(service->repo_rank, service->repo_policy->repository_ranking)) { 
						MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "Ranking of repository starting with policy [%d]", service->repo_policy->repository_ranking) ;
					}
				}
            }
            /* Get proxies request */
            else if (!strcmp(MA_REPOSITORY_MSG_PROP_VAL_RQST_GET_PROXIES_STR, msg_type)) 
            {
                ma_variant_t *response_payload = NULL ;

                MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "Received get proxies request") ;
                (void)ma_message_set_property(response, MA_REPOSITORY_MSG_KEY_TYPE_STR, MA_REPOSITORY_MSG_PROP_VAL_RPLY_GET_PROXIES_STR) ;

                if(MA_OK == (rc = ma_repository_get_proxies_request_handle(service, &response_payload))) {
                    ma_message_set_payload(response, response_payload) ;
					(void)ma_variant_release(response_payload) ;
                }

                MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "posting result for get proxy request(%d)", rc) ;
                rc = ma_msgbus_server_client_request_post_result(c_request, rc, response) ;                
            }
			/* Get system proxies request */
            else if (!strcmp(MA_REPOSITORY_MSG_PROP_VAL_RQST_GET_SYS_PROXIES_STR, msg_type)) 
            {
                ma_variant_t *response_payload = NULL ;
				char *url = NULL;
				(void)ma_message_get_property(request, MA_REPOSITORY_MSG_PROP_VAL_SYS_PROXY_URL_STR , &url) ;
                MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "Received get system proxy request") ;
                (void)ma_message_set_property(response, MA_REPOSITORY_MSG_KEY_TYPE_STR, MA_REPOSITORY_MSG_PROP_VAL_RPLY_GET_SYS_PROXIES_STR) ;

                if(MA_OK == (rc = ma_repository_get_system_proxies_request_handle(service, url, &response_payload))) {
                    ma_message_set_payload(response, response_payload) ;
					(void)ma_variant_release(response_payload) ;
                }

                MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "posting result for get system proxy request(%d)", rc) ;
                rc = ma_msgbus_server_client_request_post_result(c_request, rc, response) ;                
            }
            /* Update proxies request */
            else if (!strcmp(MA_REPOSITORY_MSG_PROP_VAL_RQST_UPDATE_PROXIES_STR, msg_type)) 
            {
                ma_variant_t *req_payload = NULL ;

                MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "Received update proxies request") ;
                
                (void)ma_message_set_property(response, MA_REPOSITORY_MSG_KEY_TYPE_STR, MA_REPOSITORY_MSG_PROP_VAL_RPLY_UPDATE_PROXIES_STR) ;
                (void)ma_message_get_payload(request, &req_payload) ;

                MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "posting result for update repositories request(%d)", rc) ;
                rc = ma_msgbus_server_client_request_post_result(c_request, rc, response) ;

                if(MA_OK != (rc = ma_repository_update_proxies_request_handle(service, req_payload))) {
                    MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "Failed to update the proxies from client(%d)", rc) ;
                }
                (void)ma_variant_release(req_payload) ;
            }
			else if (!strcmp(MA_REPOSITORY_MSG_PROP_VAL_RQST_MIRROR_TASK_STR, msg_type)) 
			{
				ma_variant_t *req_payload = NULL ;
				ma_variant_t *res_payload = NULL;

                MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "Received mirror task request") ;
                
                (void)ma_message_set_property(response, MA_REPOSITORY_MSG_KEY_TYPE_STR, MA_REPOSITORY_MSG_PROP_VAL_RPLY_MIRROR_TASK_STR) ;
                (void)ma_message_get_payload(request, &req_payload) ;

				if(MA_OK != (rc = ma_repository_mirror_task_request_handle(service, req_payload, &res_payload))) {
                    MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "Failed to handle mirror task request from client(%d)", rc) ;
                }
				else
					(void)ma_message_set_payload(response, res_payload);

                MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "posting result for mirror task request(%d)", rc) ;
                rc = ma_msgbus_server_client_request_post_result(c_request, rc, response) ;

				(void)ma_variant_release(res_payload) ;				               
                (void)ma_variant_release(req_payload) ;
			}
			/* repositories ranking request */
			else if (!strcmp(MA_REPOSITORY_MSG_PROP_VAL_RQST_RANK_REPOSITORIES_STR, msg_type)) 
			{
				MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "Received repositories rank request") ;

				/*(void)ma_message_set_property(response, MA_REPOSITORY_MSG_KEY_TYPE_STR, MA_REPOSITORY_MSG_PROP_VAL_RPLY_RANK_REPOSITORIES_STR) ;

				if(MA_OK != ma_msgbus_server_client_request_post_result(c_request, rc, response))
					MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "posting result for import repositories request, failed") ;*/

				if(MA_TRUE == ma_repository_rank_get_state(service->repo_rank)){
					MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "Ranking of repository stopping") ;
					(void *)ma_repository_rank_stop(service->repo_rank) ;
				}
				if((service->repo_policy->repository_ranking == MA_REPOSITORY_RANK_BY_PING_TIME || service->repo_policy->repository_ranking == MA_REPOSITORY_RANK_BY_SUBNET_DISTANCE) ) {
					if(MA_OK == ma_repository_rank_start(service->repo_rank, service->repo_policy->repository_ranking)) { 
						MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "Ranking of repository starting with policy [%d]", service->repo_policy->repository_ranking) ;
					}
				}
			}
            /* Message not interested */
            else 
            {
                MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "Invalid message type %s", msg_type) ;
                rc = MA_ERROR_UNEXPECTED ;
            }
            (void)ma_message_release(response) ;
        }
        else 
        {
            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "Invalid message, ignored") ;
            rc = MA_ERROR_UNEXPECTED ;
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_repository_get_repositories_request_handle(ma_repository_service_t *service, const char *filter, ma_variant_t **repositories) {
    ma_error_t rc = MA_OK ;
    ma_repository_list_t *repo_list = NULL ;

	if(MA_OK == (rc = ma_repository_get_repositories(service->context, filter?filter:MA_REPOSITORY_GET_REPOSITORIES_FILTER_NONE, &repo_list))) {
        if(MA_OK != (rc = ma_repository_list_as_variant(repo_list, repositories))) {
            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "Failed to create variant from repository list(%d)", rc) ;                
        }
		(void)ma_repository_list_release(repo_list);
    }

    return rc ;
}

ma_error_t ma_repository_update_repositories_request_handle(ma_repository_service_t *service, ma_variant_t *repositories) {
    ma_error_t rc = MA_OK ;
    ma_repository_list_t *repo_list_tmp = NULL ;

    if(MA_OK == (rc = ma_repository_list_from_variant(repositories, &repo_list_tmp))) 
    {
        ma_uint32_t index = 0 ;
		size_t num_repos = 0;

		ma_crypto_encryption_t *encrypt = NULL ;

        (void)ma_repository_list_get_repositories_count(repo_list_tmp, &num_repos) ;
		(void)ma_repository_db_set_local_repositories(MA_CONTEXT_GET_DATABASE(service->context));

        for(index = 0 ; index < num_repos ; index++) 
        {
            const char *passwd = NULL ;
			ma_repository_t *repo_tmp = NULL ;
            (void)ma_repository_list_get_repository_by_index(repo_list_tmp, index, &repo_tmp) ;
            (void)ma_repository_get_password(repo_tmp, &passwd) ;
            if(passwd) {
				unsigned char *passwd_str = NULL ;
				if(MA_OK ==(rc = ma_crypto_encrypt_create(MA_CONTEXT_GET_CRYPTO(service->context), MA_CRYPTO_ENCRYPTION_3DES, &encrypt))) {
					ma_bytebuffer_t *encrypt_buffer = NULL ;
					if(MA_OK == ma_bytebuffer_create(strlen(passwd) + 1, &encrypt_buffer)) {
						if(MA_OK == ma_crypto_encrypt_and_encode(encrypt, (unsigned char *)passwd, strlen(passwd), encrypt_buffer)) {
							passwd_str = (unsigned char*) calloc((ma_bytebuffer_get_size(encrypt_buffer) + 1), sizeof(unsigned char) );
							memcpy (passwd_str, ma_bytebuffer_get_bytes(encrypt_buffer), ma_bytebuffer_get_size(encrypt_buffer));
							*(passwd_str + ma_bytebuffer_get_size(encrypt_buffer)) ='\0';
						}
						(void)ma_bytebuffer_release(encrypt_buffer) ;
					}
					(void)ma_crypto_encrypt_release(encrypt) ;	encrypt = NULL ;
				}
				(void)ma_repository_set_password(repo_tmp, (char *)passwd_str) ;

				if(passwd_str) {
					free(passwd_str) ;
					passwd_str = NULL ;
				}
				else {
					MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_ERROR, "crypto operation failed for repository <%s> password", repo_tmp->name) ;
				}
            }

            if(MA_OK == ma_repository_validate(repo_tmp)){

				(void)ma_repository_set_icmp_attributes(MA_CONTEXT_GET_DATABASE(service->context), service->repo_policy->repository_ranking, service->repo_policy->max_ping_time, service->repo_policy->max_subnet_distance, repo_tmp);
               
                if(MA_OK != ma_update_repositories_in_db(repo_tmp, service))
                    MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "failed to persist repository(%s) in db", repo_tmp->name) ; 
            }
            else
                MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "validation failed for repository(%s) update", repo_tmp->name) ; 
			
			(void)ma_repository_release(repo_tmp); repo_tmp = NULL;
        }
		(void)ma_repository_db_remove_repositories_by_state(MA_CONTEXT_GET_DATABASE(service->context));
		
		(void)ma_repository_list_release(repo_list_tmp);
    }
    return rc ;
}

/* static ma_error_t update_firewall_rules(ma_repository_service_t *service) {
	ma_error_t rc = MA_OK;
	ma_msgbus_endpoint_t *ep = NULL;
	
	if(MA_OK == (rc = ma_msgbus_endpoint_create(MA_CONTEXT_GET_MSGBUS(service->context), MA_IO_SERVICE_NAME_STR, NULL, &ep))) {
		ma_message_t *req = NULL;
		(void) ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_INPROC);
		(void) ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);	
		if(MA_OK == (rc = ma_message_create(&req))) {
			(void) ma_message_set_property(req, MA_IO_SERVICE_MSG_TYPE, MA_IO_MSG_TYPE_FIREWALL_RULE_CHANGED_STR);
			rc = ma_msgbus_send_and_forget(ep, req);
			(void) ma_message_release(req);
		}
		(void) ma_msgbus_endpoint_release(ep);
	}

	if(MA_OK == rc)
		MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "Successfully updated firewall rules");
	else
		MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_ERROR, "Failed to update firewall rule");

	return rc;
} */

ma_error_t ma_repository_import_repositories_request_handle(ma_repository_service_t *service, ma_variant_t *repo_variant) {
    ma_error_t rc = MA_ERROR_INVALIDARG ;
	if(service && repo_variant){
		ma_buffer_t *repo_buffer = NULL;
		if(MA_OK == (rc = ma_variant_get_string_buffer(repo_variant, &repo_buffer))){
			char *repositories = NULL;
			size_t repositories_size;
			if(MA_OK == (rc = ma_buffer_get_string(repo_buffer, &repositories, &repositories_size))) 
			{
				ma_sitelist_handler_t handler;
				if(MA_OK == (rc = ma_sitelist_handler_init(&handler, MA_CONTEXT_GET_CRYPTO(service->context), MA_CONTEXT_GET_DATASTORE(service->context),MA_CONTEXT_GET_DATABASE(service->context), MA_CONTEXT_GET_LOGGER(service->context), ma_configurator_get_agent_data_path(MA_CONTEXT_GET_CONFIGURATOR(service->context)))))
				{
					if(MA_OK != (rc = ma_sitelist_handler_import_xml(&handler, repositories, repositories_size, MA_FALSE)))
						MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_ERROR, "Failed to import sitelist, error = (%d).", rc);													
					else{
						ma_ah_client_service_t *ah_client = MA_CONTEXT_GET_AH_CLIENT(service->context);
						MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_INFO, "Successfully imported the repositories.");													
						if(ah_client){
							MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_INFO, "Notifying ah client service to reload ah sitelist.");													
							(void) ma_ah_client_service_reload_sitelist(ah_client);						
							/* (void) update_firewall_rules(service); */
						}
					}
				}	
			}
			(void)ma_buffer_release(repo_buffer);
		}
	}
    return rc ;
}

ma_error_t ma_update_repositories_in_db(ma_repository_t *repository, void *cb_data) {
    ma_repository_service_t *service = (ma_repository_service_t *)cb_data ;

    switch(repository->state )
    {
        case MA_REPOSITORY_STATE_ADDED :
        case MA_REPOSITORY_STATE_ALTERED :
		case MA_REPOSITORY_STATE_DEFAULT: /*State is representing only icmp calculation status. Other attributes can be change */
            if(MA_OK == ma_repository_validate(repository)){
                if(MA_OK != ma_repository_db_add_repository(MA_CONTEXT_GET_DATABASE(service->context), repository))
                    MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "failed to update repository(%s) in db", repository->name) ; 
            }
            else
                MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "validation failed for repository(%s) update", repository->name) ;
            break ;
        case MA_REPOSITORY_STATE_REMOVED :
            if(MA_OK != ma_repository_db_remove_repository_by_name(MA_CONTEXT_GET_DATABASE(service->context), repository->name))
                    MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "failed to remove repository(%s) from db", repository->name) ;
            break ;
    }
    return MA_OK ;
}

//#define MA_PROXY_TABLE_FIELD_BASE					  0
//#define MA_PROXY_TABLE_FIELD_NAME					  MA_PROXY_TABLE_FIELD_BASE + 1
//#define MA_PROXY_TABLE_FIELD_PORT					  MA_PROXY_TABLE_FIELD_BASE + 2
//#define MA_PROXY_TABLE_FIELD_PROTOCOL_TYPE	      MA_PROXY_TABLE_FIELD_BASE + 3
//#define MA_PROXY_TABLE_FIELD_FLAG					  MA_PROXY_TABLE_FIELD_BASE + 4
//#define MA_PROXY_TABLE_FIELD_USE_AUTH				  MA_PROXY_TABLE_FIELD_BASE + 5
//#define MA_PROXY_TABLE_FIELD_USER_NAME		      MA_PROXY_TABLE_FIELD_BASE + 6
//#define MA_PROXY_TABLE_FIELD_PASSWORD				  MA_PROXY_TABLE_FIELD_BASE + 7
ma_error_t ma_repository_get_proxies_request_handle(ma_repository_service_t *service, ma_variant_t **proxies) {
    ma_error_t rc = MA_OK ;
	ma_proxy_config_t *proxy_config = NULL ;

	if(MA_OK == (rc = ma_proxy_get_config(service->context, &proxy_config))) {
		if(MA_OK != (rc = ma_proxy_config_as_variant(proxy_config, proxies))) {
			MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_ERROR, "Failed to convert proxy config to variant <rc = %d>.", rc) ;
		}
		(void)ma_proxy_config_release(proxy_config) ;	
		proxy_config = NULL ;
	}
	else
		MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_ERROR, "Failed to get proxies from db <rc = %d>.", rc) ;

    return rc ;
}

ma_error_t ma_repository_get_system_proxies_request_handle(ma_repository_service_t *service, char *url, ma_variant_t **sys_proxies_var) {
    ma_error_t rc = MA_OK ;
	ma_proxy_list_t *ma_proxy_list = NULL ;
	ma_proxy_config_t *proxy_config = NULL ;
	if(MA_OK == (rc = ma_proxy_get_config(service->context, &proxy_config))) {
		if(MA_OK == (rc = ma_proxy_config_detect_system_proxy(proxy_config, MA_CONTEXT_GET_LOGGER(service->context), url, &ma_proxy_list))){
			if(MA_OK == (rc = ma_proxy_list_to_variant(ma_proxy_list, sys_proxies_var))) {
				MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "system proxy request handle succeded") ;
			}else
				MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_ERROR, "Failed to convert ma_proxy_list_t to variant <rc = %d>.", rc) ;
			(void)ma_proxy_list_release(ma_proxy_list); ma_proxy_list = NULL;
		}else
			MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_ERROR, "detect system proxy failed <rc = %d>.", rc) ;
		(void)ma_proxy_config_release(proxy_config) ;	proxy_config = NULL ;
	}else
		MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_ERROR, "Failed to get proxies from db <rc = %d>.", rc) ;

    return rc ;
}

static ma_error_t encrypt_proxy_passwords(ma_context_t *context, ma_proxy_config_t *proxy_config) {
	size_t num_proxies = 0, iter = 0 ;
	ma_error_t rc = MA_OK ;
	ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(context) ;
	ma_crypto_t *crypto = MA_CONTEXT_GET_CRYPTO(context) ;
	ma_system_proxy_auth_info_t *auth_info =  proxy_config->system_proxy_auth ;

	(void)ma_proxy_list_size(proxy_config->proxy_list, &num_proxies) ;
	
	if(num_proxies) {
		ma_proxy_list_t *p_list = NULL ;
		if(MA_OK == (rc = ma_proxy_list_create(&p_list) )) {
			for(iter = 0 ; iter < num_proxies ; iter++) {
				ma_proxy_t *proxy = NULL ;
				if(MA_OK == ma_proxy_list_get_proxy(proxy_config->proxy_list, iter, &proxy)) {
					if(proxy->user_name && strlen(proxy->user_name) && proxy->user_passwd) {
						ma_crypto_encryption_t *encrypt = NULL ;
						if(MA_OK == (rc = ma_crypto_encrypt_create(crypto, MA_CRYPTO_ENCRYPTION_3DES, &encrypt))) {
							ma_bytebuffer_t *encrypt_buffer = NULL ;				
							(void)ma_bytebuffer_create(strlen(proxy->user_passwd)+1, &encrypt_buffer) ;							
							if(MA_OK == (rc = ma_crypto_encrypt_and_encode(encrypt, (const unsigned char *)proxy->user_passwd, strlen(proxy->user_passwd), encrypt_buffer))) {
								char *username_str = NULL;
								if(username_str = strdup(proxy->user_name)){
									unsigned char *passwd_str = (unsigned char*) calloc((ma_bytebuffer_get_size(encrypt_buffer) + 1), sizeof(unsigned char) );
									if(passwd_str){
										memcpy (passwd_str, ma_bytebuffer_get_bytes(encrypt_buffer), ma_bytebuffer_get_size(encrypt_buffer));
										*(passwd_str + ma_bytebuffer_get_size(encrypt_buffer)) ='\0';
										(void)ma_proxy_set_authentication(proxy, proxy->is_auth_rqued,  username_str, (const char*)passwd_str) ;
										free(passwd_str);
									}
									free(username_str);
								}
							}
							else {
								MA_LOG(logger, MA_LOG_SEV_DEBUG, "Failed to encrypt password for proxy, %s <rc = %d>.", proxy->server, rc) ;
							}
							(void)ma_bytebuffer_release(encrypt_buffer) ;
							(void)ma_crypto_encrypt_release(encrypt) ;	encrypt = NULL ;
						}
						else
							MA_LOG(logger, MA_LOG_SEV_DEBUG, "Failed to create crypto encrypt object, %d.", rc) ;
					}
					(void)ma_proxy_list_add_proxy(p_list, proxy) ;
				}
			}
			(void)ma_proxy_config_set_proxy_list(proxy_config, p_list) ;
		}
	}

	if((auth_info->http_user_name) && strlen(auth_info->http_user_name) && (auth_info->http_user_passwd)) {
		unsigned char *passwd_str = NULL ;
		char *user_str = NULL ;
		ma_crypto_encryption_t *encrypt = NULL ;

		if(MA_OK == (rc = ma_crypto_encrypt_create(crypto, MA_CRYPTO_ENCRYPTION_3DES, &encrypt))) {
			ma_bytebuffer_t *encrypt_buffer = NULL ;
						
			(void)ma_bytebuffer_create(strlen(auth_info->http_user_passwd)+1, &encrypt_buffer) ;
							
			if(MA_OK == (rc = ma_crypto_encrypt_and_encode(encrypt, (const unsigned char *)auth_info->http_user_passwd, strlen(auth_info->http_user_passwd), encrypt_buffer))) {
				if(user_str = strdup(auth_info->http_user_name)){
					if(passwd_str = (unsigned char*) calloc((ma_bytebuffer_get_size(encrypt_buffer) + 1), sizeof(unsigned char))){
						memcpy (passwd_str, ma_bytebuffer_get_bytes(encrypt_buffer), ma_bytebuffer_get_size(encrypt_buffer));
						*(passwd_str + ma_bytebuffer_get_size(encrypt_buffer)) ='\0';
						(void)ma_proxy_config_set_system_proxy_authentication(proxy_config, MA_PROXY_TYPE_HTTP, auth_info->http_auth_rqrd, (const char*)user_str, (const char*)passwd_str) ;
						free(passwd_str);
					}
					free(user_str);
				}
			}
			else {
				MA_LOG(logger, MA_LOG_SEV_DEBUG, "Failed to encrypt password for http system proxy, <rc = %d>.",  rc) ;
			}
			(void)ma_bytebuffer_release(encrypt_buffer) ;
			(void)ma_crypto_encrypt_release(encrypt) ;	encrypt = NULL ;
		}
		else
			MA_LOG(logger, MA_LOG_SEV_DEBUG, "Failed to create crypto encrypt object, %d.", rc) ;
	}
	if(auth_info->ftp_user_name && strlen(auth_info->ftp_user_name) && auth_info->ftp_user_passwd) {
		unsigned char *passwd_str = NULL ;
		char *user_str = NULL ;
		ma_crypto_encryption_t *encrypt = NULL ;

		if(MA_OK == (rc = ma_crypto_encrypt_create(crypto, MA_CRYPTO_ENCRYPTION_3DES, &encrypt))) {
			ma_bytebuffer_t *encrypt_buffer = NULL ;

			if(MA_OK == ma_bytebuffer_create(strlen(auth_info->ftp_user_passwd) + 1 , &encrypt_buffer)) {
				if(MA_OK == ma_crypto_encrypt_and_encode(encrypt, (const unsigned char *)auth_info->ftp_user_passwd, strlen(auth_info->ftp_user_passwd), encrypt_buffer)){
					if(user_str = strdup(auth_info->ftp_user_name)){
						if(passwd_str = (unsigned char*) calloc((ma_bytebuffer_get_size(encrypt_buffer) + 1), sizeof(unsigned char))){
							memcpy (passwd_str, ma_bytebuffer_get_bytes(encrypt_buffer), ma_bytebuffer_get_size(encrypt_buffer));
							*(passwd_str + ma_bytebuffer_get_size(encrypt_buffer)) ='\0';
							(void)ma_proxy_config_set_system_proxy_authentication(proxy_config, MA_PROXY_TYPE_FTP, auth_info->ftp_auth_rqrd, (const char *)user_str, (const char *)passwd_str) ;
							free(passwd_str);
						}
						free(user_str);
					}
				}
				else {
					MA_LOG(logger, MA_LOG_SEV_DEBUG, "Failed to encrypt password for http system proxy, <rc = %d>.",  rc) ;
				}
				(void)ma_bytebuffer_release(encrypt_buffer) ;
				(void)ma_crypto_encrypt_release(encrypt) ;
			}
		}
		else
			MA_LOG(logger, MA_LOG_SEV_DEBUG, "Failed to create crypto encrypt object, %d.", rc) ;
	}
	return rc ;
}

ma_error_t ma_repository_update_proxies_request_handle(ma_repository_service_t *service, ma_variant_t *proxies) {
    ma_error_t rc = MA_OK ;
    ma_proxy_config_t *proxy_config = NULL ;
	ma_db_t *db_handle = MA_CONTEXT_GET_DATABASE(service->context) ;

	if(MA_OK == (rc = ma_proxy_config_from_variant(proxies, &proxy_config))) {
		if(MA_OK == (rc = encrypt_proxy_passwords(service->context, proxy_config))) {
			(void)ma_repository_policy_set_proxy_config(service->repo_policy, proxy_config) ;
			(void)ma_proxy_db_remove_proxy_config(db_handle) ;
			(void)ma_proxy_db_remove_all_proxies(db_handle);
			(void)ma_proxy_db_add_proxy_config(db_handle, proxy_config) ;
			/*ask AH to update proxy list.*/
			(void)ma_ah_client_service_reload_proxy(MA_CONTEXT_GET_AH_CLIENT(service->context));
		}
		(void)ma_proxy_config_release(proxy_config) ;	proxy_config = NULL ;
	}
    return rc ;
}

static ma_error_t submit_mirror_task_stop_request(ma_repository_service_t *self, ma_mirror_request_param_t *mirror_request_param){
	if(self && mirror_request_param){
		ma_error_t rc = MA_OK;

		MA_LOG(self->logger, MA_LOG_SEV_INFO, "Received mirror task: request initiator_type <%d> request_type <%d> taskid <%s>.", mirror_request_param->initiator_type, mirror_request_param->request_type, mirror_request_param->task_id);   

		if(MA_MIRROR_REQUEST_TYPE_STOP == mirror_request_param->request_type) {	
			if(self->mirror_request) {
				if(MA_OK == (rc = ma_mirror_request_stop(self->mirror_request))) {
                    self->extra.last_mirror_session_info.state = MA_REPOSITORY_MIRROR_STATE_CANCELED;
					rc = ma_mirror_request_release(self->mirror_request);
					self->mirror_request = NULL;
				}
				if(MA_OK != rc)
					MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to process the mirror stop request.");   														
			}					
		}
		return rc;
	}
	return MA_OK;
}

static ma_error_t mirror_request_complete_cb(ma_mirror_request_t *request, ma_repository_mirror_state_t state, void *cb_data) {
	ma_error_t rc = MA_OK;
	ma_repository_service_t *self = (ma_repository_service_t *)cb_data;	
    ma_message_t *msg = NULL;
	rc = ma_mirror_request_release(self->mirror_request);
	self->mirror_request = NULL;	
    self->extra.last_mirror_session_info.state = state;

    if(MA_REPOSITORY_MIRROR_STATE_FINISHED_SUCCEEDED == state)
        MA_LOG(self->logger, MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "Mirror task completed");

    if(MA_REPOSITORY_MIRROR_STATE_FINISHED_FAILED == state || MA_REPOSITORY_MIRROR_STATE_CANCELED == state || MA_REPOSITORY_MIRROR_STATE_TIMEOUT == state)
        MA_LOG(self->logger, MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "Mirror task was not completed");
	
    /*publish this message */
    if(MA_OK == (rc = ma_message_create(&msg))) {
        if(MA_OK == (rc = ma_message_set_property(msg, MA_REPOSITORY_MSG_PROP_KEY_MIRROR_SESSION_ID_STR,  self->extra.last_mirror_session_info.session_id))) {
            ma_variant_t *payload = NULL;
            if(MA_OK == (rc = ma_variant_create_from_int16(state,&payload))) {
                if(MA_OK == (rc = ma_message_set_payload(msg, payload))) {
                    rc = ma_msgbus_publish(MA_CONTEXT_GET_MSGBUS(self->context), MA_REPOSITORY_PUBSUB_TOPIC_MIRROR_STATE_STR,  MSGBUS_CONSUMER_REACH_OUTPROC, msg);
                }
                (void)ma_variant_release(payload);
            }
        }
        (void)ma_message_release(msg);
    }

    if(MA_OK != rc) {
        MA_LOG(self->logger, MA_LOG_SEV_WARNING, "failed to publish mirror state");  
        rc = MA_OK; /*dont stop anything in failure */
    }
    return rc;
}

static ma_error_t submit_mirror_task_start_request(ma_repository_service_t *self, ma_mirror_request_param_t *mirror_request_param){
	if(self && mirror_request_param){
		ma_error_t rc = MA_OK;
		
		MA_LOG(self->logger, MA_LOG_SEV_INFO, "Received mirror task request: initiator_type <%d> request_type <%d> taskid <%s>", mirror_request_param->initiator_type, mirror_request_param->request_type, mirror_request_param->task_id);   
		if(MA_MIRROR_REQUEST_TYPE_START == mirror_request_param->request_type) {
			if(self->mirror_request) {
				MA_LOG(self->logger, MA_LOG_SEV_INFO, "Another mirror task is already in progress, so can't be continued");   
				return MA_ERROR_MIRROR_SESSION_IN_PROGRESS;				
			}
		}
		
		if(MA_OK == (rc = ma_mirror_request_create(self->context, mirror_request_param, &self->mirror_request))) {
			if(MA_OK != (rc = ma_mirror_request_start(self->mirror_request, mirror_request_complete_cb, self))) {
				MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to start the mirror task request rc <%d>.", rc);   														
				(void)ma_mirror_request_release(self->mirror_request); self->mirror_request = NULL;				
			}			
		}
		else
			MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to create the mirror task request rc <%d>.", rc);

        if(MA_OK == rc) {
            self->extra.last_mirror_session_info.session_id = mirror_request_param->task_id;
            self->extra.last_mirror_session_info.state = MA_REPOSITORY_MIRROR_STATE_IN_PROGRESS;
        }
       
		return rc;
	}
	return MA_OK;
}

ma_error_t handle_mirror_task_request(ma_repository_service_t *self, ma_mirror_request_param_t *mirror_request_param) {
	ma_error_t rc = MA_OK;
	switch(mirror_request_param->request_type) {
		case MA_MIRROR_REQUEST_TYPE_START:
			{
				MA_LOG(self->logger, MA_LOG_SEV_INFO, "Processing mirror task start request.");   														
				rc = submit_mirror_task_start_request(self, mirror_request_param);
			}
			break;
		case MA_MIRROR_REQUEST_TYPE_STOP:
			{
				MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Processing mirror task stop request.");   														
				rc = submit_mirror_task_stop_request(self, mirror_request_param);					
			}
			break;
	}
	return rc;
}

static ma_error_t ma_repository_subscriber_handler(const char *topic, ma_message_t *message, void *cb_data) {
	if(topic && message && cb_data) {
        ma_error_t rc = MA_OK;
        ma_repository_service_t *self = (ma_repository_service_t *)cb_data;

		/*handle scheduled task.*/
        if( !strncmp(topic, MA_TASK_EXECUTE_PUBSUB_TOPIC, strlen(MA_TASK_EXECUTE_PUBSUB_TOPIC)) || 
            !strncmp(topic, MA_TASK_EXEC_STOP_INITIATED_BY_SCHEDULER_TOPIC, strlen(MA_TASK_EXEC_STOP_INITIATED_BY_SCHEDULER_TOPIC)) ||             
            !strncmp(topic, MA_TASK_STOP_PUBSUB_TOPIC, strlen(MA_TASK_STOP_PUBSUB_TOPIC))) {
            const char *task_type = NULL , *task_owner_id = NULL , *task_id = NULL;	
            if( (MA_OK == (rc = ma_message_get_property(message, MA_TASK_TYPE, &task_type))) && task_type && 
                (MA_OK == (rc = ma_message_get_property(message, MA_TASK_PRODUCT_ID, &task_owner_id))) && task_owner_id && 
                (MA_OK == (rc = ma_message_get_property(message, MA_TASK_ID, &task_id))) && task_id) {
                
				if(!strcmp(task_type, MA_MIRROR_TASK_TYPE_COMPAT_MIRROR_STR) ||
				   !strcmp(task_type, MA_MIRROR_TASK_TYPE_MIRROR_STR) 
				  ) 
				{
					if(!strncmp(task_owner_id, MA_SOFTWAREID_META_STR, strlen(MA_SOFTWAREID_META_STR)) || 
					   !strncmp(task_owner_id, MA_SOFTWAREID_GENERAL_STR, strlen(MA_SOFTWAREID_GENERAL_STR))) 
					{			
                        ma_task_t *task = NULL;
						ma_mirror_request_param_t request_param;
						char *req_type[] = {"\"Mirror task Start\"", "\"Mirror task Stop\""};
						ma_mirror_request_type_t request_type = (0 == strncmp(topic, MA_TASK_EXECUTE_PUBSUB_TOPIC, strlen(MA_TASK_EXECUTE_PUBSUB_TOPIC)))
																	 ? MA_MIRROR_REQUEST_TYPE_START : MA_MIRROR_REQUEST_TYPE_STOP;

						ma_repository_mirror_initiator_type_t initiator_type = MA_REPOSITORY_MIRROR_INITIATOR_TYPE_TASK_MIRROR;								  
                    							
						request_param.initiator_type = initiator_type;
						request_param.request_type   = request_type;
						strncpy(request_param.task_id, task_id, MA_MAX_TASKID_LEN);

                        if(MA_OK == (rc = ma_scheduler_get_task_handle(MA_CONTEXT_GET_SCHEDULER(self->context), task_id, &task))) {
						    if(MA_OK == (rc = handle_mirror_task_request(self, &request_param))) {
							    MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Processed %s request for task id <%s>, initiator type %d to mirror task.", req_type[request_type], task_id, initiator_type);
                                ma_task_set_condition(task, MA_TASK_COND_NOTIFY_ON_EXEC_STATUS_CHANGE); /* To get notifications for task status change e.g stop */
                                (void)ma_task_set_execution_status(task, MA_TASK_EXEC_RUNNING);
                            }
						    else {
							    MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Processing %s request for task id <%s>, initiator type %d for mirror task failed, rc = %d.", req_type[request_type], task_id, initiator_type, rc);
                               (void)ma_task_set_execution_status(task, MA_TASK_EXEC_FAILED);
                            }
                            (void)ma_task_release(task); task = NULL;
                        }						
					}
					else
						MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Mirror task owner id %s didn't match agent software id.", task_owner_id);
				}
				else
					MA_LOG(self->logger, MA_LOG_SEV_TRACE, "Task %s is not a mirror task.", task_id);					
            }
            else
                MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Message property get failed, %d.", rc);
        }
        return rc;
    }   
    return MA_OK;
}


static ma_error_t ma_repository_mirror_task_request_handle(ma_repository_service_t *self, ma_variant_t *req_payload, ma_variant_t **response_payload)  {
	ma_mirror_request_param_t mirror_request_param;
	ma_error_t rc = MA_OK;

    if(MA_OK == (rc = ma_mirror_request_param_from_variant(req_payload, &mirror_request_param))) {
		switch(mirror_request_param.request_type){

			case MA_MIRROR_REQUEST_TYPE_START:
			case MA_MIRROR_REQUEST_TYPE_STOP:
				rc = handle_mirror_task_request(self, &mirror_request_param);
				break;
			case MA_MIRROR_REQUEST_TYPE_STATUS_CHECK:
				{
					ma_repository_mirror_state_t state = MA_REPOSITORY_MIRROR_STATE_UNKNOWN;
					if(self->mirror_request) {
                        state = MA_REPOSITORY_MIRROR_STATE_IN_PROGRESS;
					}
                    else {
                        if(mirror_request_param.task_id == self->extra.last_mirror_session_info.session_id)
                            state = self->extra.last_mirror_session_info.state;
                    }
					(void)ma_variant_create_from_int16(state, response_payload);
				}
				break;
			default:
				MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Invalid request type <%d>", mirror_request_param.request_type);
		}			
    }
    else {
        rc = MA_ERROR_MIRROR_INVALID_REQUEST;
        MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Malformed request  rc = <%d>", rc);
    }
   
    return rc;
}

/* function to configure repository service on sitelist change */
ma_error_t ma_repository_configure_on_sitelist_change(ma_repository_service_t *self) {
	return ma_repository_service_configure(self, self->context, MA_SERVICE_CONFIG_MODIFIED) ;
}