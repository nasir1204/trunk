#include "ma/internal/services/repository/ma_mirror_request.h"
#include "ma/internal/services/repository/ma_mirror_process.h"
#include "ma/internal/ma_strdef.h"

#include "ma/scheduler/ma_task.h"
#include "ma/internal/services/scheduler/ma_scheduler_core.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/services/ma_agent_policy_settings_bag.h"
#include "ma/internal/services/io/ma_dc_task_service.h"

#include "uv.h"

#include <string.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "repomirror"

#define MA_MAX_MIRROR_PROCESS_ARGS			12

#define MA_MIRROR_EXE_NAME_STR				"marepomirror"

#ifdef MA_WINDOWS
#define MA_MIRROR_EXE_SUFFIX_STR			".exe"
#else
#define MA_MIRROR_EXE_SUFFIX_STR			""
#endif

#define MA_MIRROR_LOG_FILE_NAME_STR			"marepomirror"

/* TBD - this time-out must be reviewed */
#define MA_MIRROR_TIMEOUT_VALUE_INT         (2 * 60 * 60 * 1000)

struct ma_mirror_request_s {
	ma_context_t					*context;

	ma_mirror_request_param_t		request_param;
	
	ma_mirror_process_t				*mirror_process;

	ma_task_t						*task;

	ma_mirror_request_complete_cb_t cb;

	void							*cb_data;

	uv_timer_t                      mirror_process_timeout_handle;
};

ma_error_t ma_mirror_request_create(ma_context_t *context, ma_mirror_request_param_t *param, ma_mirror_request_t **request) {
	if(param && request) {
		ma_mirror_request_t *self = (ma_mirror_request_t *)calloc(1, sizeof(ma_mirror_request_t));
		if(self) {			
			ma_error_t rc = MA_OK;
			if(MA_OK == (rc = ma_mirror_process_create(context, &self->mirror_process))) {
				if(MA_OK == (rc = ma_scheduler_get_task_handle(MA_CONTEXT_GET_SCHEDULER(context), param->task_id, &self->task))) {
					uv_loop_t *uv_loop = ma_event_loop_get_uv_loop(ma_msgbus_get_event_loop(MA_CONTEXT_GET_MSGBUS(context)));
					self->request_param.request_type = param->request_type;
					self->request_param.initiator_type = param->initiator_type;
					strncpy(self->request_param.task_id, param->task_id, MA_MAX_TASKID_LEN);
					self->context = context;
					uv_timer_init(uv_loop, &self->mirror_process_timeout_handle);                   
					*request = self;
					return MA_OK;
				}
			}
			free(self);
			return rc;
		}
		return MA_ERROR_OUTOFMEMORY;
	}
	return MA_ERROR_INVALIDARG;
}

static void mirror_process_exit_cb(ma_mirror_process_t *mirror_process, int exit_status, int term_signal, void *cb_data) {
	ma_mirror_request_t *self = (ma_mirror_request_t *)cb_data;
	uv_timer_stop(&self->mirror_process_timeout_handle);
    ma_task_set_execution_status(self->task, (!exit_status) ? MA_TASK_EXEC_SUCCESS : MA_TASK_EXEC_FAILED);
	self->cb(self, (!exit_status) ? MA_REPOSITORY_MIRROR_STATE_FINISHED_SUCCEEDED: MA_REPOSITORY_MIRROR_STATE_FINISHED_FAILED, self->cb_data);
}

static ma_error_t get_destination_directory(ma_task_t *task, char *dest_dir) {	
	ma_variant_t *task_data = NULL;
	ma_error_t rc = MA_OK;
	if(MA_OK == (rc = ma_task_get_app_payload(task, &task_data))) {	
		ma_variant_t *value = NULL;
		if(MA_OK == (rc = get_settings(task_data, MA_MIRROR_TASK_OPTIONS_SECTION_STR, MA_MIRROR_TASK_PATH_KEY_STR, &value))) {		
			ma_buffer_t *buffer = NULL;
			if(MA_OK == (rc = ma_variant_get_string_buffer(value, &buffer))) {			
				const char *str = NULL;
				size_t size = 0;
				if(MA_OK == (rc = ma_buffer_get_string(buffer, &str, &size))) {
					strncpy(dest_dir, str, size);
				}
				(void) ma_buffer_release(buffer);
			}
			(void) ma_variant_release(value);
		}
		(void) ma_variant_release(task_data);
	}
	return rc;
}

static void mirror_process_timeout_cb(uv_timer_t* handle, int status) {
    ma_mirror_request_t *self = (ma_mirror_request_t *)handle->data;	
	MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Triggering mirror task stop due to timeout occured.");
	ma_mirror_request_stop(self);
    ma_task_set_execution_status(self->task, MA_TASK_EXEC_FAILED);
	self->cb(self, MA_REPOSITORY_MIRROR_STATE_TIMEOUT, self->cb_data);	    
}

ma_error_t ma_mirror_request_start(ma_mirror_request_t *self, ma_mirror_request_complete_cb_t cb, void *cb_data) {
	if(self && cb) {
		ma_error_t rc = MA_OK;
		char dest_dir[MA_MAX_LEN] = {0};
		/* TBD - should we validate the return values ?? */
		const char *agent_install_path = ma_configurator_get_agent_install_path(MA_CONTEXT_GET_CONFIGURATOR(self->context));
		const char *agent_data_path = ma_configurator_get_agent_data_path(MA_CONTEXT_GET_CONFIGURATOR(self->context));			
		self->cb = cb;
		self->cb_data = cb_data;
		if(MA_OK == (rc = get_destination_directory(self->task, dest_dir))) {
			char commad_line[MA_MAX_BUFFER_LEN] = {0};
			char *args[MA_MAX_MIRROR_PROCESS_ARGS] = {0};			
			int argv_index = 0;
            char mirror_exe_path[MA_MAX_PATH_LEN] = {0}, mirror_log_path[MA_MAX_PATH_LEN] = {0}, catalog_version[MA_MAX_PATH_LEN] = {0}, license_key[MA_MAX_PATH_LEN] = {0};
            ma_int32_t agent_mode = ma_configurator_get_agent_mode(MA_CONTEXT_GET_CONFIGURATOR(self->context));
			int i = 0;
            const char *value = NULL;
			ma_policy_settings_bag_t *policy_bag = MA_CONTEXT_GET_POLICY_SETTINGS_BAG(self->context) ;
			
			#ifdef MA_WINDOWS_X64
			MA_MSC_SELECT(_snprintf, snprintf)(mirror_exe_path, MA_MAX_PATH_LEN-1, "%s%cx86%c%s%s", agent_install_path, MA_PATH_SEPARATOR, MA_PATH_SEPARATOR, MA_MIRROR_EXE_NAME_STR, MA_MIRROR_EXE_SUFFIX_STR);
			#else
			MA_MSC_SELECT(_snprintf, snprintf)(mirror_exe_path, MA_MAX_PATH_LEN-1, "%s%c%s%s", agent_install_path, MA_PATH_SEPARATOR, MA_MIRROR_EXE_NAME_STR, MA_MIRROR_EXE_SUFFIX_STR);
			#endif

			/* TBD - log file is hardcoded now, can be recieved from task request or from configuration*/
            MA_MSC_SELECT(_snprintf, snprintf)(mirror_log_path, MA_MAX_PATH_LEN-1, "%s%c%s%c%s", agent_data_path, MA_PATH_SEPARATOR, MA_CONFIGURATION_LOGS_DIR_NAME_STR, MA_PATH_SEPARATOR, MA_MIRROR_LOG_FILE_NAME_STR);
	

			if(MA_OK == ma_policy_settings_bag_get_str(policy_bag, MA_UPDATER_SERVICE_SECTION_NAME_STR, MA_UPDATER_KEY_CATALOG_VERSION_STR, MA_TRUE, &value,NULL)) {
                MA_MSC_SELECT(_snprintf, snprintf)(catalog_version, MA_MAX_PATH_LEN-1, "%s", value);
                value = NULL;
            }            

			if(MA_OK == ma_policy_settings_bag_get_str(policy_bag, MA_REPOSITORY_LICENSE_SECTION_NAME_STR, MA_REPOSITORY_KEY_LICENSE_STR, MA_TRUE,&value, NULL)) {
                MA_MSC_SELECT(_snprintf, snprintf)(license_key, MA_MAX_PATH_LEN-1, "%s", value);
                value = NULL;
            }
                       
			args[argv_index++] = mirror_exe_path;
			args[argv_index++] = "-d";		args[argv_index++] = dest_dir;
			args[argv_index++] = "-l";		args[argv_index++] = mirror_log_path;
            args[argv_index++] = "-m";		args[argv_index++] = (MA_AGENT_MODE_MANAGED_INT == agent_mode) ? "1" : "0";
			args[argv_index++] = "-v";		args[argv_index++] = strlen(catalog_version) ? catalog_version : "0";
			args[argv_index++] = "-k";		args[argv_index++] = strlen(license_key) ? license_key : "0";
			args[argv_index++] = 0;

			for(i = 0; i<argv_index; i++){
				MA_MSC_SELECT(_snprintf, snprintf)(commad_line + strlen(commad_line), MA_MAX_BUFFER_LEN - strlen(commad_line), "%s ", args[i]);
			}
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Invoking mirror as, [%s].",commad_line);
			
			if(MA_OK == (rc = ma_mirror_process_start(self->mirror_process, mirror_exe_path, args, mirror_process_exit_cb, self))) {
				self->mirror_process_timeout_handle.data  = self;
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "uv timer data(%p)\n", self);
				uv_timer_start(&self->mirror_process_timeout_handle, mirror_process_timeout_cb, MA_MIRROR_TIMEOUT_VALUE_INT, 0);	
			}
			else {
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to start mirror process rc <%d>", rc);
			}
		}
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_mirror_request_stop(ma_mirror_request_t *self) {
	if(self) {
		ma_error_t rc = MA_OK;
		uv_timer_stop(&self->mirror_process_timeout_handle);
		if(MA_OK == (rc = ma_mirror_process_stop(self->mirror_process))) {
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Mirror process is stopped gracefully.");
            ma_task_set_execution_status(self->task, MA_TASK_EXEC_STOPPED);
		}		
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

static void timer_close_cb(uv_handle_t *handle) {
	ma_mirror_request_t *self = (ma_mirror_request_t *)handle->data;
    MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Mirror request closed completely.");
	free(self);
}

ma_error_t ma_mirror_request_release(ma_mirror_request_t *self) {
	if(self) {		
        if(self->task) (void) ma_task_release(self->task);
		
		self->task = NULL;
		if(self->mirror_process) ma_mirror_process_release(self->mirror_process);
		uv_close((uv_handle_t *)&self->mirror_process_timeout_handle, timer_close_cb);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_mirror_request_get_status(ma_mirror_request_t *self, ma_repository_mirror_state_t *status) {
	if(self && status) {
		ma_bool_t is_process_running = MA_FALSE;
		ma_mirror_process_status(self->mirror_process, &is_process_running);		
		*status = (is_process_running) ? MA_REPOSITORY_MIRROR_STATE_FINISHED_SUCCEEDED : MA_REPOSITORY_MIRROR_STATE_FINISHED_FAILED;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

