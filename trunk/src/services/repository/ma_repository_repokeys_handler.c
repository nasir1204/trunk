#include "ma/internal/services/repository/ma_repository_service.h"

#include "ma/internal/utils/configurator/ma_configurator.h"

#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/utils/text/ma_ini_parser.h"
#include "ma/internal/utils/database/ma_db_statement.h"

#include "ma_repository_service_internal.h"

#include "ma_repository_repokeys_handler.h"
#include "ma/internal/ma_strdef.h"

#include "ma/ma_datastore.h"

#include <string.h>

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "Repository"



#define MA_CONTEXT_GET_DATABASE(context) (ma_configurator_get_database(MA_CONTEXT_GET_CONFIGURATOR(context)))
#define MA_CONTEXT_GET_DATASTORE(context) (ma_configurator_get_datastore(MA_CONTEXT_GET_CONFIGURATOR(context)))

static int repo_key_ini_handler(const char *section, const char *key, const char *value, void *userdata){
	ma_error_t rc = MA_OK ;
	ma_table_t *repo_key_tb = (ma_table_t*)userdata;
	ma_variant_t *variant = NULL;
	if(!strcmp(section, MA_REPOKEYS_REPOKEYHASHLIST_SECTION)){
		if(MA_OK == (rc = ma_table_get_value(repo_key_tb, MA_REPOKEYS_REPOKEYHASHLIST_SECTION, &variant))){
			ma_table_t *ma_table_t_tb = NULL;			
			if(MA_OK == (rc = ma_variant_get_table(variant, &ma_table_t_tb))){
				ma_variant_t *value_variant = NULL;
				if((MA_OK == (rc = ma_variant_create_from_string(value, strlen(value), &value_variant)))) {
					rc = ma_table_add_entry(ma_table_t_tb, key, value_variant);
					(void)ma_variant_release(value_variant); value_variant = NULL;	
				}
				(void)ma_table_release(ma_table_t_tb);
			}
			(void)ma_variant_release(variant); variant = NULL;
		}
	}
	else if(!strcmp(section, MA_REPOKEYS_KEYDATA_SECTION)){
		if(MA_OK == (rc = ma_table_get_value(repo_key_tb, MA_REPOKEYS_KEYDATA_SECTION, &variant))){
			ma_table_t *ma_table_t_tb = NULL;			
			if(MA_OK == (rc = ma_variant_get_table(variant, &ma_table_t_tb))){
				ma_variant_t *value_variant = NULL;
				if((MA_OK == (rc = ma_variant_create_from_string(value, strlen(value), &value_variant)))) {
					rc = ma_table_add_entry(ma_table_t_tb, key, value_variant);
					(void)ma_variant_release(value_variant); value_variant = NULL;	
				}
				(void)ma_table_release(ma_table_t_tb);
			}
			(void)ma_variant_release(variant); variant = NULL;
		}
	}
	return rc;
}

ma_error_t ma_repository_keys_get(ma_ds_t *ds, ma_variant_t **variant){
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	if(ds && variant){
		rc = ma_ds_get_variant(ds, MA_REPOSITORY_DATA_PATH_NAME_STR, MA_REPOSITORY_DATA_KEY_REPO_KEY_STR, MA_VARTYPE_TABLE, variant);
	}
	return rc ;
}

static ma_error_t repo_key_version_from_table_get(ma_table_t *repo_key_tb, char **value){
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	ma_variant_t *variant = NULL;
    if(MA_OK == (rc = ma_table_get_value(repo_key_tb, MA_REPOKEYS_REPOKEYHASHLIST_SECTION, &variant))){
			ma_table_t *ma_table_t_tb = NULL;  
			if(MA_OK == (rc = ma_variant_get_table(variant, &ma_table_t_tb))){
				ma_variant_t *value_variant = NULL;
				 if(MA_OK == (rc = ma_table_get_value(ma_table_t_tb, MA_REPOKEYS_VERSION_KEY_STR, &value_variant))){
					 ma_buffer_t *buffer = NULL;
					 size_t size;
                     if(MA_OK == ma_variant_get_string_buffer(value_variant, &buffer)){
						 if(MA_OK == ma_buffer_get_string(buffer, value, &size) && value){
							 rc = MA_OK;
						 }
						 (void)ma_buffer_release(buffer);
					 }
					 (void)ma_variant_release(value_variant);
				 }
				 (void)ma_table_release(ma_table_t_tb);
			}
			(void)ma_variant_release(variant);
	}
	return rc;
}

ma_error_t ma_repository_keys_process(ma_ds_t *ds, char *repo_keys, size_t repo_keys_size) {
	ma_error_t rc = MA_OK ;
	ma_table_t *repo_key_tb = NULL;

	if(MA_OK == (rc = ma_table_create(&repo_key_tb))) {
		ma_table_t *repokeyhashlist_tb = NULL;
		ma_table_t *keydata_tb = NULL;
		ma_variant_t *value = NULL;

		if(MA_OK == (rc = ma_table_create(&repokeyhashlist_tb))) {
			if((MA_OK == (rc = ma_variant_create_from_table(repokeyhashlist_tb, &value)))){
				rc = ma_table_add_entry(repo_key_tb, MA_REPOKEYS_REPOKEYHASHLIST_SECTION, value);
				(void)ma_variant_release(value); value = NULL;
			}
			(void)ma_table_release(repokeyhashlist_tb);
		}

		if(MA_OK == (rc = ma_table_create(&keydata_tb))) {
			if((MA_OK == rc ) && (MA_OK == (rc = ma_variant_create_from_table(keydata_tb, &value)))){
				rc = ma_table_add_entry(repo_key_tb, MA_REPOKEYS_KEYDATA_SECTION, value);
				(void)ma_variant_release(value); value = NULL;
			}
			(void)ma_table_release(keydata_tb);
		}
		{
			char *repokey_version = NULL;
			if((MA_OK == rc ) && (MA_OK ==(rc = ma_ini_parser_start(repo_keys, repo_keys_size, repo_key_ini_handler, (void*)repo_key_tb)))){
				if(MA_OK == (rc = ma_variant_create_from_table(repo_key_tb, &value))){
					rc = ma_ds_set_variant(ds, MA_REPOSITORY_DATA_PATH_NAME_STR, MA_REPOSITORY_DATA_KEY_REPO_KEY_STR, value);
					(void)ma_variant_release(value);
				}				
			}
			if(MA_OK == (rc = repo_key_version_from_table_get(repo_key_tb, &repokey_version))){
				ma_ds_set_str(ds, MA_REPOSITORY_DATA_PATH_NAME_STR, MA_REPOSITORY_DATA_KEY_REPO_VERSION_STR, repokey_version, strlen(repokey_version));
			}
			(void)ma_table_release(repo_key_tb);
		}
	}
	return rc;
}



