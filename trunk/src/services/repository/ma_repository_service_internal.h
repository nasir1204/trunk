#ifndef MA_REPOSITORY_SERVICE_INTERNAL_H_INCLUDED
#define MA_REPOSITORY_SERVICE_INTERNAL_H_INCLUDED

#include "ma/ma_msgbus.h"
#include "ma/proxy/ma_proxy_config.h"

#include "ma/internal/services/repository/ma_repository_service.h"
#include "ma/internal/services/repository/ma_repository_spipe_manager.h"
#include "ma/internal/services/repository/ma_repository_policy.h"
#include "ma/internal/services/repository/ma_repository_rank.h"

#include "ma/internal/services/ma_service.h"
#include "ma/internal/services/repository/ma_mirror_request.h"

#include "ma/internal/defs/ma_object_defs.h"

MA_CPP(extern "C" {)

typedef enum ma_repository_service_state_e {
    REPOSITORY_SERVICE_STATE_INITIAL = 0,
    REPOSITORY_SERVICE_STATE_IN_PROGRESS,
    REPOSITORY_SERVICE_STATE_RANKING_INPROGRESS,
    REPOSITORY_SERVICE_STATE_COMPLETED,
}ma_repository_service_state_t ;

struct ma_repository_service_s {
    ma_service_t					base ;          /*base service object */

	ma_context_t					*context;

    ma_msgbus_server_t				*server ;		/* message bus server */

	ma_msgbus_subscriber_t			*subscriber ;   /* message bus subscriber */

    ma_repository_policy_t			*repo_policy ;  /* repository policies */

    ma_repository_rank_t			*repo_rank ;

    ma_repository_spipe_manager_t	*repo_spipe_manager ; /* repository manager holds spipe decorator and handler */

	ma_logger_t						*logger;

    ma_mirror_request_t		        *mirror_request;

    union {
        struct {         
            void                            *session_id;
            ma_repository_mirror_state_t    state;
        } last_mirror_session_info;
    } extra;
} ;

struct ma_repository_spipe_manager_s {
    ma_spipe_decorator_t        repository_spipe_decorator ;
    ma_spipe_handler_t          repository_spipe_handler ;
    ma_spipe_priority_t         repository_spipe_priority ;
    void                        *data ;
} ;

struct ma_repository_policy_s {
    ma_repository_rank_type_t   repository_ranking ;
    ma_int32_t                  max_ping_time ;
    ma_int32_t                  max_subnet_distance ;
    ma_bool_t                   overwrite_client_sites ;
    ma_proxy_config_t           *proxy_config ;
    void                        *data ;
} ;

/* function to configure repository service on sitelist change */
ma_error_t ma_repository_configure_on_sitelist_change(ma_repository_service_t *self) ;

MA_CPP(})
#endif  /* MA_REPOSITORY_SERVICE_INTERNAL_H_INCLUDED */
