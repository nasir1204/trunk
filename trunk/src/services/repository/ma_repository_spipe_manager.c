#include "ma/internal/utils/xml/ma_xml.h"
#include "ma/internal/utils/xml/ma_xml_node.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include "ma/internal/utils/repository/ma_repository_db_manager.h"
#include "ma/internal/utils/proxy/ma_proxy_internal.h"
#include "ma/internal/utils/repository/ma_repository_internal.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma_repository_service_internal.h"
#include "ma_repository_repokeys_handler.h"
#include "ma/internal/utils/spipe/ma_spipe_package_defs.h"
#include "ma/ma_log.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/macrypto/ma_crypto.h"


#include "ma/internal/ma_strdef.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "Repository"

#define MA_CONTEXT_GET_DATABASE(context) (ma_configurator_get_database(MA_CONTEXT_GET_CONFIGURATOR(context)))
#define MA_CONTEXT_GET_DATASTORE(context) (ma_configurator_get_datastore(MA_CONTEXT_GET_CONFIGURATOR(context)))

static ma_error_t get_repository_spipe_priority(ma_spipe_decorator_t *decorator, ma_spipe_priority_t *priority) ;
static ma_error_t spipe_repository_decorator_destroy(ma_spipe_decorator_t *decorator) ;
static ma_error_t spipe_repository_decorate(ma_spipe_decorator_t *decorator, ma_spipe_priority_t priority, ma_spipe_package_t *spipe_pkg) ;

static ma_spipe_decorator_methods_t spipe_property_decorator_methods = {    
    &spipe_repository_decorate,
    &get_repository_spipe_priority,
    &spipe_repository_decorator_destroy
} ;

static ma_error_t spipe_repository_handler(ma_spipe_handler_t *handler, const ma_spipe_response_t *spipe_response) ;
static ma_error_t spipe_repository_handler_destroy(ma_spipe_handler_t *handler) ;

static ma_spipe_handler_methods_t spipe_property_handler_methods = {    
    &spipe_repository_handler,
    &spipe_repository_handler_destroy
} ;

ma_error_t ma_repository_spipe_manager_create(ma_repository_service_t *service, ma_repository_spipe_manager_t **repository_spipe_manager) {
    if(service && repository_spipe_manager) {
        ma_repository_spipe_manager_t *self = (ma_repository_spipe_manager_t *)calloc(1, sizeof(ma_repository_spipe_manager_t)) ;
        if(self){
            self->repository_spipe_priority = MA_SPIPE_PRIORITY_NORMAL ;
            ma_spipe_decorator_init((ma_spipe_decorator_t *)&self->repository_spipe_decorator, &spipe_property_decorator_methods, (void *)self) ;
            ma_spipe_handler_init((ma_spipe_handler_t *)&self->repository_spipe_handler, &spipe_property_handler_methods, (void *)self) ;
            self->data = service ;
			*repository_spipe_manager = self ;

            return MA_OK ;
        }
        return MA_ERROR_OUTOFMEMORY ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_repository_spipe_manager_release(ma_repository_spipe_manager_t *self) {
    if(self) {
        free(self) ;
        self = NULL ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_spipe_decorator_t * ma_repository_spipe_manager_get_decorator(ma_repository_spipe_manager_t *self) {
    return &self->repository_spipe_decorator ;
}

ma_spipe_handler_t * ma_repository_spipe_manager_get_handler(ma_repository_spipe_manager_t *self) {
    return &self->repository_spipe_handler ;
}

ma_error_t ma_repository_spipe_manager_raise_spipe_alert(ma_repository_spipe_manager_t *self) {
    return MA_OK ;
}

ma_error_t spipe_repository_decorate(ma_spipe_decorator_t *decorator, ma_spipe_priority_t priority, ma_spipe_package_t *spipe_pkg) {
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	if(spipe_pkg && decorator){
		ma_buffer_t *buffer = NULL;
		ma_bool_t added = MA_FALSE;
        ma_repository_service_t *repo_service = (ma_repository_service_t*)((ma_repository_spipe_manager_t*)decorator->data)->data ;
		
        if(MA_OK == (rc = ma_ds_get_str(MA_CONTEXT_GET_DATASTORE(repo_service->context), MA_REPOSITORY_DATA_PATH_NAME_STR, MA_REPOSITORY_DATA_KEY_REPO_VERSION_STR, &buffer))){
			const char *version_str = "0";
			size_t len = 1;	
			if(MA_OK == (rc = ma_buffer_get_string(buffer, &version_str, &len))) {
				rc = ma_spipe_package_add_info(spipe_pkg, STR_REPO_KEY_HASH_VERSION, (const unsigned char*)version_str, len);                
				added = MA_TRUE;
            }
			(void)ma_buffer_release(buffer); buffer = NULL;
		}
		if(!added)
			rc = ma_spipe_package_add_info(spipe_pkg, STR_REPO_KEY_HASH_VERSION, "0", strlen("0"));
		
	}
	return rc ;
}

ma_error_t get_repository_spipe_priority(ma_spipe_decorator_t *decorator, ma_spipe_priority_t *priority) {
    *priority =  MA_SPIPE_PRIORITY_NORMAL ;
    return MA_OK ;
}

ma_error_t spipe_repository_decorator_destroy(ma_spipe_decorator_t *decorator) {
    MA_ATOMIC_DECREMENT(decorator->ref_count);
    return MA_OK ;
}


ma_error_t spipe_repository_handler(ma_spipe_handler_t *handler, const ma_spipe_response_t *spipe_response) {
    if(handler && spipe_response) {
        ma_error_t rc = MA_OK ;
		ma_repository_service_t *repo_service = (ma_repository_service_t*)((ma_repository_spipe_manager_t*)handler->data)->data ;

        if(spipe_response->net_service_error_code == MA_ERROR_NETWORK_REQUEST_SUCCEEDED && (spipe_response->http_response_code == 200 || spipe_response->http_response_code == 202))
        {
            if(spipe_response->respond_spipe_pkg) 
            {
                ma_spipe_package_t *response_pkg = spipe_response->respond_spipe_pkg ;
                const char *value = NULL ;
                size_t val_size = 0 ;
        
                (void)ma_spipe_package_get_info(response_pkg,  STR_PACKAGE_TYPE, (const unsigned char **)&value, &val_size) ;
                
                /* if props response from server */
                /*if(0 == strncmp(value, STR_PKGTYPE_PROPS_RESPONSE, val_size)) 
                {
                    char *sitelist_xml = NULL ;
                    size_t sitelist_xml_size = 0 ;
                    MA_LOG(MA_CONTEXT_GET_LOGGER(repo_service->context), MA_LOG_SEV_DEBUG, "spipe handler with package type %s", value) ;
                    ma_spipe_package_get_data(response_pkg, STR_SITELIST_NAME, (void **)&sitelist_xml, &sitelist_xml_size) ;
                    if(sitelist_xml && sitelist_xml_size > 0 )
                    {
                        rc = ma_repository_process_sitelist(repo_service->base.context, sitelist_xml, sitelist_xml_size) ;
                    }
                }*/
                if(0 == strncmp(value, STR_PKGTYPE_PROPS_RESPONSE, val_size))
				{
					char *repo_keys = NULL, *sitelist = NULL ;
					size_t repo_keys_size = 0 ;
					
					MA_LOG(MA_CONTEXT_GET_LOGGER(repo_service->context), MA_LOG_SEV_DEBUG, "spipe handler with package type %s", value) ;
					
					(void)ma_spipe_package_get_data(response_pkg, MA_STR_SPIPEPKG_DATA_REPOKEYS_INI, (void **)&repo_keys, &repo_keys_size) ;
					if(repo_keys && repo_keys_size > 0 )
					{
						rc = ma_repository_keys_process(MA_CONTEXT_GET_DATASTORE(repo_service->context), repo_keys, repo_keys_size) ;
					}

					/* reconfigure repository policy as sitelist is changed */
					(void)ma_spipe_package_get_data(response_pkg, STR_SITELIST_NAME, (void **)&sitelist, &repo_keys_size) ;
					if(sitelist && repo_keys_size > 0) {
						ma_repository_configure_on_sitelist_change(repo_service) ;
					}
				}
            }
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t spipe_repository_handler_destroy(ma_spipe_handler_t *handler) {
    MA_ATOMIC_DECREMENT(handler->ref_count);
    return MA_OK ;
}

