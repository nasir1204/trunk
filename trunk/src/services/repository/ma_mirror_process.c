#include "ma/internal/services/repository/ma_mirror_process.h"
#include "ma/internal/defs/ma_mue_defs.h"

#include "ma/ma_msgbus.h"
#include "ma/internal/core/msgbus/ma_msgbus_private.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal//defs/ma_object_defs.h"

#include "ma/ma_log.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#if !defined(MA_WINDOWS)
#include <unistd.h>
#endif
#include <uv.h>


#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "Mirror"

struct ma_mirror_process_s{
	ma_context_t			*context;

	uv_process_t			process_handle;

	ma_mirror_exit_cb_t		cb;

	void					*cb_data;

    ma_bool_t               is_spawned;
		
	int						ref_count;
};

ma_error_t ma_mirror_process_create(ma_context_t *context, ma_mirror_process_t **mirror_process){	
	if(context && mirror_process){
		ma_mirror_process_t *self  = (ma_mirror_process_t *)calloc(1, sizeof(ma_mirror_process_t));
		if(self){
			self->context			 = context;
            self->is_spawned		 = MA_FALSE;
			self->ref_count			 = 1;
			*mirror_process			 = self;
			return MA_OK;			
		}
		return MA_ERROR_OUTOFMEMORY;
	}
	return MA_ERROR_INVALIDARG;

}

static void close_callback(uv_handle_t* handle) {
	ma_mirror_process_t *self = (ma_mirror_process_t *)handle->data;
	MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Finally releasing the mirror process handle.");
	free(self);
}

ma_error_t ma_mirror_process_release(ma_mirror_process_t *self){
	if(self){		
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Releasing the mirror process handle, ref count = %d.", self->ref_count);
		if(!(--self->ref_count)) {
            if(self->is_spawned) {
			    uv_close((uv_handle_t*)(&self->process_handle), close_callback);
            }
            else {
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Releasing the mirror engine process.");
                free(self);
            }            
		    return MA_OK;
        }
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_mirror_process_status(ma_mirror_process_t *self, ma_bool_t *is_running){
	if(self && is_running){
		*is_running = MA_TRUE; /* TBD */
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

static void mirror_process_exit_cb(uv_process_t *process, int exit_status, int term_signal){
	ma_mirror_process_t *self = (ma_mirror_process_t *)process->data;
	if(self){ /* mirror_process is not released so we will follow*/
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Mirror process exit callback received");
		if(self->cb){ /*User interested in the exit callback.*/
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Calling the mirror task request callback.");
			self->cb(self, exit_status, term_signal, self->cb_data);
		}			
	}
	/*Process handle should be free always in the exit callback.*/
	ma_mirror_process_release(self); 	
}

ma_error_t ma_mirror_process_start(ma_mirror_process_t *self, const char *exe_path, const char **args, ma_mirror_exit_cb_t exit_cb, void *cb_data){
	if(self && exe_path && args && exit_cb){
		uv_process_options_t	process_options = {0};
		uv_stdio_container_t	child_stdio[3] ;				
		ma_msgbus_t *msgbus		= MA_CONTEXT_GET_MSGBUS(self->context);
		uv_loop_t *uv_loop		= NULL;

		if(!msgbus)
			return MA_ERROR_INVALID_CONTEXT;

		uv_loop = ma_event_loop_get_uv_loop(ma_msgbus_get_event_loop(msgbus));

		process_options.file = exe_path;
		process_options.args = (char**)args;
		process_options.exit_cb = mirror_process_exit_cb;
		process_options.stdio_count = 3 ;
	      
		/*TODO - I just blindly copied this but I am not comfortable with stdio flags */
		child_stdio[0].flags = child_stdio[1].flags = UV_IGNORE ;	        
		child_stdio[2].flags = UV_INHERIT_FD ; /*I dont think we need it */
		child_stdio[2].data.fd = 2 ;
		process_options.stdio = child_stdio ;
    
		self->process_handle.data = self;			
		if(0 != uv_spawn(uv_loop, &self->process_handle, process_options)) {                
   			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to spawn mirror process,%s.",uv_strerror(uv_last_error(uv_loop)));			
   			return MA_ERROR_MIRROR_PROCESS_SPAWN_FAILED;
   		}
          
		/*increase reference count*/
		++self->ref_count;

#ifdef MA_WINDOWS
		if(self->process_handle.spawn_error.code != UV_OK )
#else
		if(self->process_handle.errorno)
#endif                
		{			
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Mirror process spawn failed, error: <%s>.",uv_strerror(uv_last_error(uv_loop)));
   			return MA_ERROR_MIRROR_PROCESS_SPAWN_FAILED ;
		}			
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Successfully launched mirror process, pid <%d>", self->process_handle.pid);

		/*set all the upper layer data.*/		  		
		self->cb_data		= cb_data;
        self->is_spawned    = MA_TRUE;
		self->cb			= exit_cb;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_mirror_process_stop(ma_mirror_process_t *self) {
	if(self) {
		if(self->process_handle.pid > 0){
			self->cb = NULL; /*Let it die its own, uppper layer don't interested in it.*/
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Stopping mirror process, pid <%d>", self->process_handle.pid);
			uv_process_kill(&self->process_handle, SIGTERM) ;			
		}
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

