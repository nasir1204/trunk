#include "ma/internal/services/repository/ma_mirror_request_param.h"

#include <string.h>

ma_error_t ma_mirror_request_param_from_variant(ma_variant_t *variant, ma_mirror_request_param_t *request_param) {
	if(variant && request_param) {
       ma_error_t rc = MA_OK;
       ma_array_t *array = NULL;

       if(MA_OK == (rc = ma_variant_get_array(variant, &array))) {
		   int index = 0;
           ma_variant_t *temp_variant = NULL;

           if(MA_OK == (rc = ma_array_get_element_at(array, ++index, &temp_variant))) {

			   ma_int16_t int16 = 0;
               if(MA_OK == (rc = ma_variant_get_int16(temp_variant, &int16)))
					request_param->request_type = (ma_mirror_request_type_t)int16;			   
               (void) ma_variant_release(temp_variant);
           }
           if((MA_OK == rc) && (MA_OK == (rc = ma_array_get_element_at(array, ++index, &temp_variant)))) {

               ma_int16_t int16 = 0;
               if(MA_OK == (rc = ma_variant_get_int16(temp_variant, &int16)))
					request_param->initiator_type = (ma_repository_mirror_initiator_type_t)int16;			   
               (void) ma_variant_release(temp_variant);
           }

           if((MA_OK == rc) && (MA_OK == (rc = ma_array_get_element_at(array, ++index, &temp_variant)))) {

               ma_buffer_t *buffer = NULL;
               if(MA_OK == (rc = ma_variant_get_string_buffer(temp_variant, &buffer))) {
                   const char *value = NULL;
                   size_t size = 0;
                   if(MA_OK == (rc = ma_buffer_get_string(buffer, &value, &size))) {
                       strncpy(request_param->task_id, value, MA_MAX_TASKID_LEN);
                   }
                   (void) ma_buffer_release(buffer);
               }
               (void) ma_variant_release(temp_variant);
           }           
           (void) ma_array_release(array);
       }
	   
       return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_mirror_request_param_to_variant(ma_mirror_request_param_t request_param, ma_variant_t **variant) {
	 if(variant) {
        ma_array_t *array = NULL;
        ma_error_t rc = MA_OK;

        if(MA_OK == (rc = ma_array_create(&array))) {
            ma_variant_t *temp_variant = NULL;
            if(MA_OK == (rc = ma_variant_create_from_int16(request_param.request_type, &temp_variant))) {
                (void) ma_array_push(array, temp_variant);
                (void) ma_variant_release(temp_variant);
            }
			if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_int16(request_param.initiator_type, &temp_variant)))) {
                (void) ma_array_push(array, temp_variant);
                (void) ma_variant_release(temp_variant);
            }
			if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(request_param.task_id, MA_MAX_TASKID_LEN, &temp_variant)))) {
                (void) ma_array_push(array, temp_variant);
                (void) ma_variant_release(temp_variant);
            }
            if(MA_OK == rc) 
                rc = ma_variant_create_from_array(array, variant);

            (void) ma_array_release(array);
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}


