#include "ma/internal/utils/repository/ma_repository_internal.h"
#include "ma/internal/services/repository/ma_repository_rank.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/core/msgbus/ma_msgbus_private.h"
#include "ma_repository_service_internal.h"

#include "ma/internal/utils/repository/ma_repository_private.h"
#include "ma/internal/utils/repository/ma_repository_db_manager.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/utils/platform/ma_system_property.h"
#include "ma/internal/utils/icmp/ma_icmp.h"
#include "ma/ma_log.h"
#include "ma/repository/ma_repository_client_defs.h"
#include "uv.h"
#include <stdlib.h>
#include <string.h>

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "reporank"

#define MA_CONTEXT_GET_DATABASE(context) (ma_configurator_get_database(MA_CONTEXT_GET_CONFIGURATOR(context)))
#define MA_CONTEXT_GET_DATASTORE(context) (ma_configurator_get_datastore(MA_CONTEXT_GET_CONFIGURATOR(context)))
#define SERVER_FQDN 8
#define SERVER_IP 9
#define SERVER_NAME 10
#define PING_TIME_COL 18
#define SUBNET_DIST_COL 19

struct ma_repository_rank_s {
    ma_bool_t           status ;    /* MA_TRUE - if running/in progress*/
    void                *data ;     /*holds repository service object*/
	ma_icmp_context_t	*icmp_context;
} ;

ma_error_t  ma_repository_rank_create(ma_repository_service_t *repository_service, ma_repository_rank_t **respository_rank){
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	if(repository_service && respository_rank){
		*respository_rank = (ma_repository_rank_t*)calloc(1, sizeof(ma_repository_rank_t));
        (*respository_rank)->status = MA_FALSE;
		(*respository_rank)->data = repository_service;
        rc = MA_OK ;
	}
    return rc ;
}

ma_error_t ma_repository_rank_sitelist_get(void *data, ma_icmp_repo_node_list_t **icmp_url_head){
	if(data && icmp_url_head){
		ma_error_t rc = MA_OK ;
		
		ma_repository_list_t *repo_list = NULL;
		ma_icmp_context_t *icmp_context = (ma_icmp_context_t*)data;
		ma_repository_rank_t *repo_ser = (ma_repository_rank_t*)ma_icmp_context_get_data(icmp_context);

		if((MA_OK == (rc = ma_repository_get_repositories(((ma_repository_service_t*)(repo_ser)->data)->context, MA_REPOSITORY_GET_REPOSITORIES_FILTER_FOR_RANKING,  &repo_list)) )) {
			size_t num_repos = 0, iter = 0 ;

			(void)ma_repository_list_get_repositories_count(repo_list, &num_repos) ;

			for(iter = 0 ; iter < num_repos ; iter ++) {
				ma_repository_t *repo = NULL ;

				if(MA_OK == ma_repository_list_get_repository_by_index(repo_list, iter, &repo) && repo) {
					ma_icmp_repo_node_list_t *icmp_url_list ;

					if(MA_OK == (rc = ma_icmp_repo_node_list_create(&icmp_url_list))) {
						char *url = NULL ;
						ma_bool_t numeric_url = MA_FALSE ;

						if( (MA_OK == ma_repository_get_server_ip(repo, &url) && url && (numeric_url = MA_TRUE) )
							|| (MA_OK == ma_repository_get_server_fqdn(repo, &url) && url)
							|| (MA_OK == ma_repository_get_server_name(repo, &url) && url)	)
						{
							ma_icmp_repo_node_list_init(icmp_url_list, repo->name, url, numeric_url);
							ma_queue_insert_tail(&(*icmp_url_head)->qlink, &(icmp_url_list)->qlink);
						}
					}
					(void)ma_repository_release(repo) ;	repo = NULL ;
				}
			}
			(void)ma_repository_list_release(repo_list);	repo_list = NULL ;
		}
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_repository_rank_sitelist_update(void *data){
	if(data){
		ma_icmp_callback_t *callback_data = (ma_icmp_callback_t*)data;
		ma_icmp_context_t *icmp_context = (ma_icmp_context_t*)(callback_data)->icmp_context;
		ma_repository_rank_t *repo_rank = (ma_repository_rank_t*)ma_icmp_context_get_data(icmp_context);
		if(repo_rank){
			ma_error_t rc = MA_OK ;
			ma_icmp_policy_t	icmp_policy ;
			ma_repository_service_t *service = (ma_repository_service_t*)repo_rank->data;
			if(service){
				ma_icmp_context_get_policy((icmp_context), &icmp_policy);
				if(icmp_policy.rank_type == MA_REPOSITORY_RANK_BY_PING_TIME){
					MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "updating ping time for repository <%s> with <%d>." , callback_data->name, callback_data->ping_time);
					rc = ma_repository_db_set_repository_ping_count(MA_CONTEXT_GET_DATABASE(service->context), callback_data->name, callback_data->ping_time);
				}
				else if(icmp_policy.rank_type == MA_REPOSITORY_RANK_BY_SUBNET_DISTANCE){
					MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "updating subnet distance for repository <%s> with <%d>." , callback_data->name, callback_data->subnet_distance);
					rc = ma_repository_db_set_repository_subnet_distance(MA_CONTEXT_GET_DATABASE(service->context), callback_data->name, callback_data->subnet_distance);
				}

				if(callback_data->computation_finishied){
					repo_rank->status = MA_FALSE; 
				}
				return rc;
			}
		}
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t  ma_repository_rank_stop(void *self) {
	if(self){
		ma_repository_rank_t *repo_rank = (ma_repository_rank_t*)self;
		if(repo_rank){
			repo_rank->status = MA_FALSE ;
			ma_icmp_computation_stop(&(repo_rank->icmp_context)) ; 
			return MA_OK;
		}
	}
	return MA_ERROR_INVALIDARG ;
}

/*If agent ip change from earlier rank computation, then re-compute all repo */
static ma_error_t  ma_repository_rank_ip_check(ma_repository_service_t *self){
	ma_error_t rc = MA_ERROR_INVALIDARG;
	if(self){
		ma_ds_t *ds = MA_CONTEXT_GET_DATASTORE(self->context);
		ma_db_t *db = MA_CONTEXT_GET_DATABASE(self->context);
		ma_system_property_t *sysp = MA_CONTEXT_GET_SYSTEM_PROPERTY(self->context) ;
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_TRACE, "ip change check");
		if(sysp && ds && db){
			const ma_network_system_property_t *net = ma_system_property_network_get(sysp, MA_TRUE) ;
			if(net) {
				if(strlen(net->ip_address_formated)){
					ma_buffer_t *buffer = NULL;
					/*get last used ip */
					if(MA_OK == ma_ds_get_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_AGENT_REPO_RANK_IP_STR, &buffer) ) {
						const char *value = NULL;
						size_t size = 0;
						if( (MA_OK == (rc = ma_buffer_get_string(buffer, &value, &size))) && value){
							if(strcmp(net->ip_address_formated, value)){
								MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_TRACE, "ip changed from <%s> to <%s>", net->ip_address_formated, value);
								ma_repository_db_reset_all_repository_icmp_attr(db, self->repo_policy->max_ping_time, self->repo_policy->max_subnet_distance);
								ma_ds_set_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_AGENT_REPO_RANK_IP_STR, net->ip_address_formated, -1);
							}
							ma_buffer_release(buffer);
						}
					}else{
						ma_ds_set_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_AGENT_REPO_RANK_IP_STR, net->ip_address_formated, -1);
						rc = MA_OK;
					}
				}
				else{
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_TRACE, "network is down or failed to get current ip");
				}
			}
		}
	}
	return rc;
}

/*If ping/subnet policy value change from earlier rank computation, then re-compute all repo */
static ma_error_t  ping_subnet_policy_change_check(ma_repository_service_t *self, ma_repository_rank_type_t rank_type){
	ma_error_t rc = MA_ERROR_INVALIDARG;
	if(self){
		ma_ds_t *ds = MA_CONTEXT_GET_DATASTORE(self->context);
		ma_db_t *db = MA_CONTEXT_GET_DATABASE(self->context);
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_TRACE, "repo ranking policy change check");
		if(ds && db){
			if(MA_REPOSITORY_RANK_BY_PING_TIME == rank_type){
				ma_int32_t ping_time = 30000;
				if(MA_OK == ma_ds_get_int32(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_AGENT_REPO_RANK_MAX_PING_TIME_STR, &ping_time) ) {
					if(ping_time != self->repo_policy->max_ping_time){
						MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_TRACE, "repo ranking policy changed from ping time <%d> to <%d>", ping_time, self->repo_policy->max_ping_time);
						ma_repository_db_reset_all_repository_ping_attr(db, (self->repo_policy->max_ping_time * 1000));
						ma_ds_set_int(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_AGENT_REPO_RANK_MAX_PING_TIME_STR, self->repo_policy->max_ping_time);
					}
				}
				else{
					/*first time */
					ma_repository_db_reset_all_repository_ping_attr(db, (self->repo_policy->max_ping_time * 1000));
					ma_ds_set_int(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_AGENT_REPO_RANK_MAX_PING_TIME_STR, self->repo_policy->max_ping_time);
				}
				rc = MA_OK;
			}
			else if(MA_REPOSITORY_RANK_BY_SUBNET_DISTANCE == rank_type){
				ma_int32_t subnet_dist = 15;
				if(MA_OK == ma_ds_get_int32(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_AGENT_REPO_RANK_MAX_SUBNET_DIST_STR, &subnet_dist) ) {
					if(subnet_dist != self->repo_policy->max_subnet_distance){
						MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_TRACE, "repo ranking policy changed from subnet dist <%d> to <%d>", subnet_dist, self->repo_policy->max_subnet_distance);
						ma_repository_db_reset_all_repository_subnet_dist_attr(db, self->repo_policy->max_subnet_distance);
						ma_ds_set_int(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_AGENT_REPO_RANK_MAX_SUBNET_DIST_STR, self->repo_policy->max_subnet_distance);
					}
				}
				else{
					/*first time */
					ma_repository_db_reset_all_repository_subnet_dist_attr(db, self->repo_policy->max_subnet_distance);
					ma_ds_set_int(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_AGENT_REPO_RANK_MAX_SUBNET_DIST_STR, self->repo_policy->max_subnet_distance);
				}
				rc = MA_OK;
			}
		}
	}
	return rc;
}

ma_error_t  ma_repository_rank_start(ma_repository_rank_t *self, ma_repository_rank_type_t rank_type){
	if(self){
		ma_repository_service_t *repository_service = (ma_repository_service_t*)self->data;
		(void)ping_subnet_policy_change_check(repository_service, rank_type);
		if(repository_service && (MA_OK == ma_repository_rank_ip_check(repository_service))) {
			ma_icmp_policy_t icmp_policy;
			ma_error_t rc = MA_OK;
			ma_icmp_context_t *icmp_context = NULL;
			ma_icmp_methods_t *icmp_methods = (ma_icmp_methods_t*)calloc(1, sizeof(ma_icmp_methods_t));
			ma_icmp_context_create(&icmp_context, NULL);
			MA_LOG(MA_CONTEXT_GET_LOGGER(repository_service->context), MA_LOG_SEV_DEBUG, "repository ranking started with policy <%d>", rank_type);
			icmp_methods->get = ma_repository_rank_sitelist_get;
			icmp_methods->update = ma_repository_rank_sitelist_update;
			icmp_methods->stop = ma_repository_rank_stop;
			ma_icmp_context_set_method(icmp_context, icmp_methods);
			ma_icmp_policy_create(&icmp_policy, ((ma_repository_service_t *)self->data)->repo_policy->max_ping_time, ((ma_repository_service_t *)self->data)->repo_policy->max_subnet_distance, rank_type);
			ma_icmp_context_set_logger(icmp_context, MA_CONTEXT_GET_LOGGER( ((ma_repository_service_t*)self->data)->context));
			ma_icmp_context_set_policy(icmp_context, icmp_policy);
			ma_icmp_context_set_data(icmp_context, (void*)self);
			ma_icmp_context_set_uv_loop(icmp_context, ma_event_loop_get_uv_loop(ma_msgbus_get_event_loop(MA_CONTEXT_GET_MSGBUS(repository_service->context))));
			self->status = MA_TRUE;
			self->icmp_context = icmp_context;
			if(MA_OK != (rc = ma_icmp_computation_start(icmp_context))){
				MA_LOG(MA_CONTEXT_GET_LOGGER(repository_service->context), MA_LOG_SEV_DEBUG, "repository ranking either failed or no qualified repositories to sort, return code <%d>", rc);
				ma_repository_rank_stop(self);
			}
			return rc;
		}
	}
	return MA_ERROR_INVALIDARG;
}

ma_bool_t ma_repository_rank_get_state(ma_repository_rank_t *self) {
    if(self) return self->status ;
    else return MA_FALSE ;
}

ma_error_t  ma_repository_rank_release(ma_repository_rank_t *self) {
	if(!self) 
		return MA_ERROR_INVALIDARG ;
	free(self);
	self = NULL;
	return MA_OK ;
}

