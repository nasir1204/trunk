#include "ma/internal/services/profile/ma_profile_service.h"
#include "ma/internal/utils/context/ma_context.h"
#include "ma/internal/utils/profiler/ma_profiler.h"
#include "ma/internal/utils/profiler/ma_profile_info.h"
#include "ma/internal/defs/ma_profile_defs.h"
#include "ma/internal/services/ma_service.h"
#include "ma/ma_log.h"
#include "ma/ma_message.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/ma_variant.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/sensor/ma_sensor_msg.h"
#include "ma/sensor/ma_sensor_utils.h"
#include "ma/ma_message.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/utils/sms/ma_sms.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

const char base_path[] = "/profile/";

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "profile service"

MA_CPP(extern "C" {)
MA_CPP(})

struct ma_profile_service_s {
    ma_service_t            service_base;
    ma_context_t            *context;
    ma_msgbus_server_t      *server;
    ma_logger_t             *logger;
    ma_msgbus_subscriber_t  *subscriber;
    ma_profiler_t           *profiler;
    ma_msgbus_t             *msgbus;
};


static ma_error_t ma_profile_service_add_admin(ma_profile_service_t *service);
static ma_error_t profile_subscriber_cb(const char *topic, ma_message_t *message, void *cb_data);
static ma_error_t ma_profile_service_cb(ma_msgbus_server_t *server, ma_message_t *msg, void *cb_data, ma_msgbus_client_request_t *c_request);

static ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint);
static ma_error_t service_start(ma_service_t *service);
static ma_error_t service_stop(ma_service_t *service);
static void service_release(ma_service_t *service);

static const ma_service_methods_t service_methods = {
    &service_configure,
    &service_start,
    &service_stop,
    &service_release
};

ma_error_t ma_profile_service_create(const char *service_name, ma_service_t **service) {
    if(service_name && service) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;

        if((*service = (ma_service_t*)calloc(1, sizeof(ma_profile_service_t)))) {
            ma_service_init(*service, &service_methods, service_name, service);
            return MA_OK;
        }
        return err ;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_service_release(ma_profile_service_t *self) {
    if(self) {
        ma_error_t err = MA_OK;
        if(self->server && (MA_OK != (err = ma_msgbus_server_release(self->server)))) {
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "msgbus server release failed");
        }
        (void)ma_msgbus_subscriber_release(self->subscriber);
        (void)ma_profiler_release(self->profiler);
        free(self) ;
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_bool_t check_context_validity(ma_context_t *context){	
    /*Will see if we need this*/
    return MA_TRUE;
}

ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint) {
    if(service && context) {
        ma_error_t rc = MA_OK;
        if(MA_SERVICE_CONFIG_NEW == hint && check_context_validity(context)) {
            ma_configurator_t *configurator = MA_CONTEXT_GET_CONFIGURATOR(context);
            ma_profile_service_t *self = (ma_profile_service_t *)service;
            ma_msgbus_t *msgbus = MA_CONTEXT_GET_MSGBUS(context);

            self->context = context;
            self->logger = MA_CONTEXT_GET_LOGGER(context);
            self->msgbus = msgbus;

            (void)ma_profiler_set_logger(self->logger);
            if(MA_OK == (rc = ma_profiler_create(msgbus, ma_configurator_get_profile_datastore(configurator), MA_PROFILER_BASE_PATH_STR, &self->profiler))) {
                MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "profiler configured successfully");
                if(MA_OK == (rc = ma_msgbus_subscriber_create(MA_CONTEXT_GET_MSGBUS(context), &self->subscriber))) {
                    (void)ma_msgbus_subscriber_setopt(self->subscriber, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                    (void)ma_msgbus_subscriber_setopt(self->subscriber, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
                    if(MA_OK == (rc = ma_msgbus_server_create(msgbus, MA_PROFILE_SERVICE_NAME_STR, &self->server))) {                        
                        (void)ma_msgbus_server_setopt(self->server, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
                        if(MA_OK == (rc = ma_msgbus_server_setopt(self->server, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC))) {
                            /*TODO - No need to add the profile object in the context if we can use the profile client API's */
                            ma_context_add_object_info(context, MA_OBJECT_PROFILER_NAME_STR, (void *const *)&self->profiler);
                            MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_DEBUG, "profile service configuration successful");
                            return MA_OK;
                        }
                        ma_msgbus_server_release(self->server);
                    }
                    ma_msgbus_subscriber_release(self->subscriber);
                }
            }
            else
                MA_LOG(self->logger, MA_LOG_SEV_ERROR, "profiler creation failed, last error(%d)", rc);
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t service_start(ma_service_t *service) {
    if(service) {
        ma_error_t err = MA_OK;
        ma_profile_service_t *self = (ma_profile_service_t *)service ;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);
        
        if(MA_OK == (err = ma_profiler_start(self->profiler))) {
            if(MA_OK == (err = ma_msgbus_server_start(((ma_profile_service_t*)service)->server, ma_profile_service_cb, service))) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "profile service started successfully");
                //(void)ma_profile_service_add_admin(self);
                if(MA_OK == (err = ma_msgbus_subscriber_register(((ma_profile_service_t*)service)->subscriber,  "ma.task.*", profile_subscriber_cb, service))) {
                    MA_LOG(((ma_profile_service_t*)service)->logger, MA_LOG_SEV_DEBUG, "profile subscriber registered successfully");
                    return MA_OK ;
                } 
                else 
                    MA_LOG(((ma_profile_service_t*)service)->logger, MA_LOG_SEV_ERROR, "profile service failed to subscribe topic, errocode(%d)", err);
                ma_msgbus_server_stop(((ma_profile_service_t*)service)->server);
            }
            else
                MA_LOG(logger, MA_LOG_SEV_CRITICAL, "profile service start failed, last error(%d)", err);
            (void)ma_profiler_stop(self->profiler);
        }
        else
            MA_LOG(logger, MA_LOG_SEV_CRITICAL, "profile service start failed, last error(%d)", err);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_service_get_msgbus(ma_profile_service_t *service, ma_msgbus_t **msgbus) {
    if(service && msgbus) {
        *msgbus = service->msgbus;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_service_get_version(ma_uint32_t *version) {
    if(version) {
        *version = 0x05020100; //TODO fetch version 
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_profile_service_start(ma_profile_service_t *service) {
    return service_start((ma_service_t*)service);
}

ma_error_t ma_profile_service_stop(ma_profile_service_t *service) {
    return service_stop((ma_service_t*)service);
}

ma_error_t service_stop(ma_service_t *service) {
    if(service) {
        ma_error_t err = MA_OK;
        ma_profile_service_t *self = (ma_profile_service_t *)service;

        if(MA_OK != (err = ma_msgbus_subscriber_unregister(((ma_profile_service_t*)service)->subscriber))) {
            MA_LOG(((ma_profile_service_t*)service)->logger, MA_LOG_SEV_ERROR, "profile service unregistering subscriber failed, last error(%d)", err);
        }

        if(MA_OK == (err = ma_msgbus_server_stop(self->server))) {
            if(MA_OK != (err = ma_profiler_stop(self->profiler))) 
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_CRITICAL, "profiler stop failed, last error(%d)", err);
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO, "profile service stopped successfully");
        }
        else
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_CRITICAL, "profile service stop failed, last error(%d)", err);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

void service_release(ma_service_t *service) {
    if(service) {
        ma_profile_service_release((ma_profile_service_t *)service) ;
        return ;
    }
    return ;
}

static ma_error_t profile_register_handler(ma_profile_service_t *service, ma_message_t *profile_req_msg, ma_message_t *profile_rsp_msg) {
    if(service && profile_req_msg && profile_rsp_msg) {
        ma_error_t err = MA_OK;
        const char *profile_id = NULL;
        char *profile_reg_status = MA_TASK_REPLY_STATUS_FAILED;
        ma_profile_info_t *info = NULL;
        ma_variant_t *profile_variant = NULL;
        char error_code[MA_MAX_LEN] = {0};


        (void)ma_message_set_property(profile_rsp_msg, MA_PROFILE_ID, profile_id);
        if(MA_OK == (err = ma_message_get_property(profile_req_msg, MA_PROFILE_ID, &profile_id))) {
                profile_reg_status = MA_PROFILE_ID_NOT_EXIST;
                if(MA_OK == (err = ma_profiler_get(service->profiler, profile_id, &info))) {
                    profile_reg_status = MA_PROFILE_ID_EXIST;
                    MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "profile id(%s) already exist!", profile_id);
                    (void)ma_profile_info_release(info);
                } else {
                   if(MA_OK == ma_message_get_payload(profile_req_msg, &profile_variant)) {
                       if(MA_OK == (err = ma_profiler_add_variant(service->profiler, profile_id, profile_variant))) {
                           profile_reg_status = MA_PROFILE_ID_WRITTEN;
                       }
                       (void)ma_variant_release(profile_variant);
                   }
                }
        }
        if(MA_OK != err)
            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_CRITICAL, "profile id register failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(profile_rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(profile_rsp_msg, MA_PROFILE_STATUS, profile_reg_status);
        return err = MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t profile_update_handler(ma_profile_service_t *service, ma_message_t *profile_req_msg, ma_message_t *profile_rsp_msg) {
    if(service && profile_req_msg && profile_rsp_msg) {
        ma_error_t err = MA_OK;
        const char *profile_id = NULL;
        char *profile_reg_status = MA_TASK_REPLY_STATUS_FAILED;
        ma_profile_info_t *info = NULL;
        ma_variant_t *profile_variant = NULL;
        char error_code[MA_MAX_LEN] = {0};


        if(MA_OK == (err = ma_message_get_property(profile_req_msg, MA_PROFILE_ID, &profile_id))) {
            (void)ma_message_set_property(profile_rsp_msg, MA_PROFILE_ID, profile_id);
            profile_reg_status = MA_PROFILE_ID_NOT_EXIST;
            if(MA_OK == ma_message_get_payload(profile_req_msg, &profile_variant)) {
                if(MA_OK == (err = ma_profile_info_convert_from_variant(profile_variant, &info))) {
                    ma_profile_info_t *pinfo = NULL;

                    if(MA_OK == (err = ma_profiler_get(service->profiler, profile_id, &pinfo))) {
                        {
                            const char *image_url = NULL;

                            (void)ma_profile_info_get_profile_url(info, &image_url);
                            if(strcmp(image_url, MA_EMPTY)) {
                                (void)ma_profile_info_set_profile_url(pinfo, image_url);
                            }
                        }
                        {
                            char passwd[MA_MAX_LEN+1] = {0};

                            (void)ma_profile_info_get_password(info, passwd);
                            if(strcmp(passwd, MA_EMPTY)) {
                                (void)ma_profile_info_set_password(pinfo, passwd);
                            }
                        }
                        {
                            char pincode[MA_MAX_LEN+1] = {0};

                            (void)ma_profile_info_get_source_pincode(info, pincode);
                            if(strcmp(pincode, MA_EMPTY)) {
                                (void)ma_profile_info_set_source_pincode(pinfo, pincode);
                            }
                        }
                        {
                            char contact[MA_MAX_LEN+1] = {0};

                            (void)ma_profile_info_get_contact(info, contact);
                            if(strcmp(contact, MA_EMPTY)) {
                                (void)ma_profile_info_set_contact(pinfo, contact);
                            }
                        }
                        {
                            char from_address[MA_MAX_BUFFER_LEN+1] = {0};

                            (void)ma_profile_info_get_from_address(info, from_address);
                            if(strcmp(from_address, MA_EMPTY)) {
                                (void)ma_profile_info_set_from_address(pinfo, from_address);
                            }
                        }
                        {
                            char from_city[MA_MAX_LEN+1] = {0};

                            (void)ma_profile_info_get_from_city(info, from_city);
                            if(strcmp(from_city, MA_EMPTY)) {
                                (void)ma_profile_info_set_from_city(pinfo, from_city);
                            }
                        }
                        {
                            char user[MA_MAX_NAME+1] = {0};

                            (void)ma_profile_info_get_user(info, user);
                            if(strcmp(user, MA_EMPTY)) {
                                MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "user (%s) ", user);
                                (void)ma_profile_info_set_user(pinfo, user);
                            }
                        }
                        {
                            ma_profile_info_status_t status = MA_PROFILE_INFO_STATUS_ACTIVE;
                            char status_str[MA_MAX_LEN+1] = {0};

                            (void)ma_profile_info_get_status(info, &status);
                            (void)ma_profile_info_get_status_str(info, status_str);
                            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "profile id(%s) status", status_str);
                            (void)ma_profile_info_set_status(pinfo, status);
                        }
                        {
                            size_t size = 0, i = 0;

                            (void)ma_profile_info_board_size(info, &size);
                            for(i = 0; i < size; ++i) {
                                const char *board = NULL;

                                (void)ma_profile_info_board_at(info, i, &board);
                                if(board) {
                                    size_t psize = 0, k = 0;

                                    (void)ma_profile_info_board_size(pinfo, &psize);
                                    for(k = 0; k < psize; ++k) {
                                        const char *pboard = NULL;

                                        (void)ma_profile_info_board_at(pinfo, k, &pboard);
                                        if(!strcmp(pboard, board))break;
                                    }
                                    if(psize == k) {
                                        (void)ma_profile_info_add_board(pinfo, board);
                                    }
                                }
                            }
                        }
                        if(MA_OK == (err = ma_profiler_add(service->profiler, pinfo))) {
                           char msg[MA_MAX_BUFFER_LEN+1] = {0};
                           char user[MA_MAX_NAME+1] = {0};

                           (void)ma_profile_info_get_user(info, user);
                           MA_MSC_SELECT(_snprintf, snprintf)(msg, MA_MAX_BUFFER_LEN, "Dear %s,Your profile has been updated successfully with mileaccess.com", user);
                           MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "sending user registeration message to user contact api %s", msg);
                           (void)ma_sms_send(MA_HTTP_NON_SECURE_PROTOCOL, MA_MILEACCESS_HOST, MA_MILEACCESS_PINCODE_MAPPER_PORT, MA_SMS_SEND_PATH, profile_id, msg); 
                           MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "profile id(%s) updated into DB successfully", profile_id);
                           profile_reg_status = MA_PROFILE_ID_WRITTEN;
                        }
                        (void)ma_profile_info_release(pinfo);
                    }
                    (void)ma_profile_info_release(info);
                }
                (void)ma_variant_release(profile_variant);
            }
        }
        if(MA_OK != err)
            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_CRITICAL, "profile id update failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(profile_rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(profile_rsp_msg, MA_PROFILE_STATUS, profile_reg_status);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t profile_deregister_handler(ma_profile_service_t *service, ma_message_t *profile_req_msg, ma_message_t *profile_rsp_msg) {
    if(service && profile_req_msg && profile_rsp_msg) {
        ma_error_t err = MA_OK;
        const char *profile_id = NULL;
        char *profile_reg_status = MA_PROFILE_ID_NOT_EXIST;
        ma_profile_info_t *info = NULL;
        char error_code[MA_MAX_LEN] = {0};


        (void)ma_message_set_property(profile_rsp_msg, MA_PROFILE_ID, profile_id);
        if(MA_OK == (err = ma_message_get_property(profile_req_msg, MA_PROFILE_ID, &profile_id))) {
            if(MA_OK == (err = ma_profiler_get(service->profiler, profile_id, &info))) {
                char user[MA_MAX_NAME+1] = {0};

                (void)ma_profile_info_get_user(info, user);
                profile_reg_status = MA_PROFILE_ID_EXIST;
                if(MA_OK == (err = ma_profiler_delete(service->profiler, profile_id))) {
                    char msg[MA_MAX_BUFFER_LEN+1] = {0};

                    MA_MSC_SELECT(_snprintf, snprintf)(msg, MA_MAX_BUFFER_LEN, "Dear %s,You have been successfully unregistered with mileaccess.com.Looking forward you again!", user);
                    MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "sending user deregisteration message to user contact api %s", msg);
                    (void)ma_sms_send(MA_HTTP_NON_SECURE_PROTOCOL, MA_MILEACCESS_HOST, MA_MILEACCESS_PINCODE_MAPPER_PORT, MA_SMS_SEND_PATH, profile_id, msg); 
                    profile_reg_status = MA_PROFILE_ID_DELETED;
                    MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "profile id(%s) deleted successfully", profile_id);
                }
                (void)ma_profile_info_release(info);
            }
        }
        if(MA_OK != err)
            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_CRITICAL, "profile id deregister failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(profile_rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(profile_rsp_msg, MA_PROFILE_STATUS, profile_reg_status);
        return err = MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t profile_get_json_handler(ma_profile_service_t *service, ma_message_t *profile_req_msg, ma_message_t *profile_rsp_msg) {
    if(service && profile_req_msg && profile_rsp_msg) {
        ma_error_t err = MA_OK;
        const char *profile_id = NULL;
        char *profile_reg_status = MA_PROFILE_ID_NOT_EXIST;
        ma_profile_info_t *info = NULL;
        ma_variant_t *profile_variant = NULL;
        char error_code[MA_MAX_LEN] = {0};


        (void)ma_message_set_property(profile_rsp_msg, MA_PROFILE_ID, profile_id);
        if(MA_OK == (err = ma_message_get_property(profile_req_msg, MA_PROFILE_ID, &profile_id))) {
            if(MA_OK == (err = ma_profiler_get(service->profiler, profile_id, &info))) {
                ma_json_t *json = NULL;

                MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "profile id(%s) get successfully", profile_id);
                if(MA_OK == (err = ma_profile_info_convert_to_json(info, &json))) {
                    ma_bytebuffer_t *buffer = NULL;

                    ma_bytebuffer_create(MA_MAX_BUFFER_LEN, &buffer);
                    if(MA_OK == (err = ma_json_get_data(json, buffer))) {
                        if(MA_OK == (err = ma_variant_create_from_string((const char*)ma_bytebuffer_get_bytes(buffer), ma_bytebuffer_get_size(buffer), &profile_variant))) {
                            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "profile json output(%s) len(%u)", ma_bytebuffer_get_bytes(buffer), ma_bytebuffer_get_size(buffer));
                            profile_reg_status = MA_PROFILE_ID_EXIST;
                            (void)ma_message_set_payload(profile_rsp_msg, profile_variant);
                            (void)ma_variant_release(profile_variant);
                        } else
                            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_ERROR, "profile variant creation failed, last error(%d)", err);
                    }
                }
                (void)ma_profile_info_release(info);
            }
        }
        if(MA_OK != err)
            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_CRITICAL, "profile id get failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(profile_rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(profile_rsp_msg, MA_PROFILE_STATUS, profile_reg_status);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t profile_get_handler(ma_profile_service_t *service, ma_message_t *profile_req_msg, ma_message_t *profile_rsp_msg) {
    if(service && profile_req_msg && profile_rsp_msg) {
        ma_error_t err = MA_OK;
        const char *profile_id = NULL;
        char *profile_reg_status = MA_PROFILE_ID_NOT_EXIST;
        ma_profile_info_t *info = NULL;
        ma_variant_t *profile_variant = NULL;
        char error_code[MA_MAX_LEN] = {0};


        (void)ma_message_set_property(profile_rsp_msg, MA_PROFILE_ID, profile_id);
        if(MA_OK == (err = ma_message_get_property(profile_req_msg, MA_PROFILE_ID, &profile_id))) {
            if(MA_OK == (err = ma_profiler_get(service->profiler, profile_id, &info))) {
                MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "profile id(%s) get successfully", profile_id);
                if(MA_OK == (err = ma_profile_info_convert_to_variant(info, &profile_variant))) {
                    MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "profile id(%s) get successfull", profile_id);
                    profile_reg_status = MA_PROFILE_ID_EXIST;
                    (void)ma_message_set_payload(profile_rsp_msg, profile_variant);
                    (void)ma_variant_release(profile_variant);
                }
                (void)ma_profile_info_release(info);
            }
        }
        if(MA_OK != err)
            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_CRITICAL, "profile id get failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(profile_rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(profile_rsp_msg, MA_PROFILE_STATUS, profile_reg_status);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_service_cb(ma_msgbus_server_t *server, ma_message_t *msg, void *cb_data, ma_msgbus_client_request_t *c_request) {
    if(server && msg && cb_data) {
        ma_error_t err = MA_OK;
        const char *msg_type = NULL;
        ma_message_t *response = NULL;
        ma_profile_service_t *service = (ma_profile_service_t*)cb_data;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(service->context);

        if(MA_OK == (err = ma_message_create(&response))) {
            if(MA_OK == (err = ma_message_get_property(msg, MA_PROFILE_SERVICE_MSG_TYPE, &msg_type))) {
                if(!strcmp(msg_type, MA_PROFILE_SERVICE_MSG_REGISTER_TYPE)) {
                    if(MA_OK == (err = profile_register_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client register id request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_PROFILE_SERVICE_MSG_UNREGISTER_TYPE)) {
                    if(MA_OK == (err = profile_deregister_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client deregister request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_PROFILE_SERVICE_MSG_GET_JSON_TYPE)) {
                    if(MA_OK == (err = profile_get_json_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client deregister request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_PROFILE_SERVICE_MSG_GET_TYPE)) {
                    if(MA_OK == (err = profile_get_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client deregister request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_PROFILE_SERVICE_MSG_UPDATE_TYPE)) {
                    if(MA_OK == (err = profile_update_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client update request failed, last error(%d)", err);
                        }
                    }
                } else {
                    if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, MA_ERROR_SCHEDULER_INVALID_OP_TYPE, response))) {
                        MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client request failed, last error(%d)", err);
                    }
                }
            } else {
                if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err = MA_ERROR_SCHEDULER_TASK_TYPE_NOT_SET, response))) {
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client request failed, last error(%d)", err);
                }
            }
            if(MA_OK != ma_message_release(response)) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client request message release failed");
            }
        }
        return err;
    }
    return MA_OK;
}

static ma_error_t update_profile(ma_profile_service_t *service, const char *pin_id) {
    if(service && pin_id) {
        ma_context_t *context = service->context;
        ma_error_t err = MA_OK;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(context);
        ma_season_info_t *info = NULL;

        MA_LOG(logger, MA_LOG_SEV_DEBUG, "%s starts", __FUNCTION__);
        if(MA_OK == (err = ma_season_get(MA_CONTEXT_GET_SEASON(context), pin_id, &info))) {
            const char *board = NULL;

            (void)ma_season_info_get_board(info, &board);
            if(board && strcmp(board, MA_EMPTY)) {
                const char *email = NULL;

                //(void)ma_season_info_get_email_id(info, &email);
                (void)ma_season_info_get_contact(info, &email);
                if(email && strcmp(email, MA_EMPTY)) {
                    ma_profile_info_t *pinfo = NULL;

                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "season contact id(%s)", email);
                    if(MA_OK == (err = ma_profiler_get(service->profiler, email, &pinfo))) {
                        if(MA_OK == (err = ma_profile_info_add_board(pinfo, board))) {
                            if(MA_OK == (err = ma_profiler_add(service->profiler, pinfo))) {
                                MA_LOG(logger, MA_LOG_SEV_TRACE, "profile(%s) update with board(%s) successfully", email, board);
                            } else {
                                MA_LOG(logger, MA_LOG_SEV_TRACE, "profile(%s) update with board(%s) failed, last error(%d)", email, board, err);
                            }
                        }
                        (void)ma_profile_info_release(pinfo);
                    } else 
                        MA_LOG(logger, MA_LOG_SEV_ERROR, "profile (%s) does not exist, last error(%d)", email, err);
                } else
                    MA_LOG(logger, MA_LOG_SEV_ERROR, "profile does not exist");
            } else
                MA_LOG(logger, MA_LOG_SEV_ERROR, "Board is invalid..skipping update");
            (void)ma_season_info_release(info);
        } else {
            MA_LOG(logger, MA_LOG_SEV_ERROR, "season info convert from variant fialed, last error(%d)", err);
        }

        MA_LOG(logger, MA_LOG_SEV_DEBUG, "%s end error(%d)", __FUNCTION__, err);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t profile_subscriber_cb(const char *topic, ma_message_t *msg, void *cb_data) {
    if(topic && msg && cb_data) {
        ma_error_t err = MA_OK;
        ma_profile_service_t *service = (ma_profile_service_t*)cb_data;
        //ma_profile_t *profile = service->profile;

        MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "profile subscriber received sensor message(%s)", topic);
        if(!strcmp(topic, MA_TASK_EXECUTE_PUBSUB_TOPIC)) {
            const char *task_type = NULL , *task_owner_id = NULL , *task_id = NULL;

            MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "profile service processing (%s) message", topic);
            if( (MA_OK == (err = ma_message_get_property(msg, MA_TASK_TYPE, &task_type))) && task_type &&
                (MA_OK == (err = ma_message_get_property(msg, MA_TASK_PRODUCT_ID, &task_owner_id))) && task_owner_id &&
                (MA_OK == (err = ma_message_get_property(msg, MA_TASK_ID, &task_id))) && task_id) {
                if(!strcmp(task_type, MA_SEASON_TASK_TYPE)) {
                    err = update_profile(service, task_id);
                } else
                    MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "task id(%s) is not season task", task_id);
            }
        } else {
            /* do nothing */
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_service_get_profiler(ma_profile_service_t *service, ma_profiler_t **profiler) {
    if(service && profiler) {
        *profiler = service->profiler;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_profile_service_add_admin(ma_profile_service_t *service) {
    if(service) {
        ma_error_t err = MA_OK;
        ma_profile_info_t *info = NULL;

        if(MA_OK == (err = ma_profiler_get(service->profiler, MA_PROFILE_INFO_ADMIN, &info))) {
            MA_LOG(service->logger, MA_LOG_SEV_TRACE, "admin user already exists");
            (void)ma_profile_info_release(info);
        } else {
            if(MA_OK == (err = ma_profile_info_create(&info, MA_PROFILE_INFO_USER_TYPE_ADMIN))) {
                (void)ma_profile_info_set_email_id(info, MA_PROFILE_INFO_ADMIN_MAIL_ID);
                (void)ma_profile_info_set_password(info, MA_PROFILE_INFO_ADMIN_PASSWORD);
                (void)ma_profile_info_set_user(info, MA_PROFILE_INFO_ADMIN);
                (void)ma_profile_info_set_privilege(info, MA_PROFILE_INFO_PRIVILEGE_RDWR);
                (void)ma_profile_info_set_status(info, MA_PROFILE_INFO_STATUS_ACTIVE);
                if(MA_OK == (err = ma_profiler_add(service->profiler, info))) {
                    MA_LOG(service->logger, MA_LOG_SEV_TRACE, "admin user created successfully");
                } else
                    MA_LOG(service->logger, MA_LOG_SEV_ERROR, "admin user creation failed, last error(%d)", err);
                (void)ma_profile_info_release(info);
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

