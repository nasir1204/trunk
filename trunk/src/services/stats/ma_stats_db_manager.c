#include "ma/internal/services/stats/ma_stats_db_manager.h"
#include "ma_stats_service_internal.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"

#include "ma/internal/ma_macros.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#define CREATE_AGENT_STATS_TABLE_STMT   "CREATE TABLE IF NOT EXISTS AGENT_STATS(OWNER TEXT NOT NULL, ATTRIBUTE TEXT NOT NULL, LAST_SENT TEXT NOT NULL, CURRENT TEXT NOT NULL, PRIMARY KEY (OWNER, ATTRIBUTE) ON CONFLICT REPLACE)"

ma_error_t ma_stats_db_schema_create(ma_db_t *db_handle) {
	if(db_handle) {
		ma_db_statement_t *db_stmt = NULL;
		ma_error_t rc = MA_OK ;

		if(MA_OK == (rc = ma_db_statement_create(db_handle, CREATE_AGENT_STATS_TABLE_STMT, &db_stmt))) {
			rc = ma_db_statement_execute(db_stmt) ;

			(void)ma_db_statement_release(db_stmt) ;
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

#define CHECK_DEFAULT_STATS_SQL_STMT	"SELECT COUNT(1) FROM AGENT_STATS"
ma_error_t ma_stats_db_check_default(ma_db_t *db_handle) {
	if(db_handle) {
		ma_db_statement_t *db_stmt = NULL;
		ma_error_t rc = MA_OK ;

		if(MA_OK == (rc = ma_db_statement_create(db_handle, CHECK_DEFAULT_STATS_SQL_STMT, &db_stmt))) {
			ma_db_recordset_t *db_record = NULL ;

			if(MA_OK == (rc = ma_db_statement_execute_query(db_stmt, &db_record))) {
				if(MA_ERROR_NO_MORE_ITEMS != (rc = ma_db_recordset_next(db_record))) {
					int int_value = 0 ;

					(void)ma_db_recordset_get_int(db_record, 1, &int_value) ;
					rc = (int_value)? MA_OK : MA_ERROR_NO_MORE_ITEMS ;
				}
				(void)ma_db_recordset_release(db_record);
			}
			(void)ma_db_statement_release(db_stmt) ;
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

#define ADD_STAT_SQL_STMT	"INSERT INTO AGENT_STATS VALUES(?, ?, ?, ?)"
ma_error_t ma_stats_db_add_stat(ma_db_t *db_handle, const char *owner, const char *attribute, ma_double_t last_sent, ma_double_t current) {
	if(db_handle && owner && attribute) {
		ma_db_statement_t *db_stmt = NULL;
		ma_error_t rc = MA_OK ;

		if(MA_OK == (rc = ma_db_statement_create(db_handle, ADD_STAT_SQL_STMT, &db_stmt))) {
			ma_temp_buffer_t buf = MA_TEMP_BUFFER_INIT ;
			char *p = NULL ;

			(void)ma_db_statement_set_string(db_stmt, MA_AGENT_STATS_FIELD_OWNER, 1, owner, strlen(owner)) ;
			(void)ma_db_statement_set_string(db_stmt, MA_AGENT_STATS_FIELD_ATTRIBUTE, 1, attribute, strlen(attribute)) ;

			ma_temp_buffer_reserve(&buf, MA_MAX_LEN) ;
			MA_MSC_SELECT(_snprintf, snprintf)(p = (char *)ma_temp_buffer_get(&buf), ma_temp_buffer_capacity(&buf), "%.7lf", last_sent) ;
			(void)ma_db_statement_set_string(db_stmt, MA_AGENT_STATS_FIELD_LAST_SENT, 1, p, strlen(p)) ;

			ma_temp_buffer_reserve(&buf, MA_MAX_LEN) ;
			MA_MSC_SELECT(_snprintf, snprintf)(p = (char *)ma_temp_buffer_get(&buf), ma_temp_buffer_capacity(&buf), "%.7lf", current) ;
			(void)ma_db_statement_set_string(db_stmt, MA_AGENT_STATS_FIELD_CURRENT, 1, p, strlen(p)) ;

			rc = ma_db_statement_execute(db_stmt) ;

			(void)ma_db_statement_release(db_stmt) ;	db_stmt = NULL ;
			ma_temp_buffer_uninit(&buf) ;
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

#define UPDATE_LAST_SENT_STATS_SQL_STMT	"UPDATE AGENT_STATS SET LAST_SENT = ? WHERE OWNER = ? AND ATTRIBUTE = ?"
#define GET_LAST_SENT_STATS_SQL_STMT	"SELECT LAST_SENT FROM AGENT_STATS WHERE OWNER = ? AND ATTRIBUTE = ?"
ma_error_t ma_stats_db_update_last_sent(ma_db_t *db_handle, const char*owner, const char *attribute, ma_double_t value) {
	if(db_handle ) {
		ma_db_statement_t *db_stmt = NULL;
		ma_error_t rc = MA_OK ;
		ma_double_t last_sent = 0 ;

		if(MA_OK == (rc = ma_db_statement_create(db_handle, GET_LAST_SENT_STATS_SQL_STMT, &db_stmt))) {
			ma_db_recordset_t *db_record = NULL ;

			(void)ma_db_statement_set_string(db_stmt, 1, 1, owner, strlen(owner)) ;
			(void)ma_db_statement_set_string(db_stmt, 2, 1, attribute, strlen(attribute)) ;

			if(MA_OK == (rc = ma_db_statement_execute_query(db_stmt, &db_record))) {
				if(MA_ERROR_NO_MORE_ITEMS != (rc = ma_db_recordset_next(db_record))) {
					char *val = NULL ;
					char *tmp = NULL ;

					(void)ma_db_recordset_get_string(db_record, 1, &val) ;
					last_sent = strtod(val, &tmp) ;
				}
				(void)ma_db_recordset_release(db_record) ;	db_record = NULL ;
			}
			(void)ma_db_statement_release(db_stmt) ;	db_stmt = NULL ;
		}

		last_sent += value ;

		if(MA_OK == rc && MA_OK == (rc = ma_db_statement_create(db_handle, UPDATE_LAST_SENT_STATS_SQL_STMT, &db_stmt))) {
			char str_value[MA_MAX_LEN] = {0} ;
			
			MA_MSC_SELECT(_snprintf, snprintf)(str_value, MA_MAX_LEN, "%.7lf", last_sent) ;

			(void)ma_db_statement_set_string(db_stmt, 2, 1, owner, strlen(owner)) ;
			(void)ma_db_statement_set_string(db_stmt, 3, 1, attribute, strlen(attribute)) ;
			(void)ma_db_statement_set_string(db_stmt, 1, 1, str_value, strlen(str_value)) ;
			
			rc = ma_db_statement_execute(db_stmt) ;

			(void)ma_db_statement_release(db_stmt) ; db_stmt = NULL ;
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

#define GET_STATS_BY_OWNER_SQL_STMT	"SELECT ATTRIBUTE, CURRENT, LAST_SENT FROM AGENT_STATS WHERE OWNER = ?"
ma_error_t ma_stats_db_get_stats_by_owner(ma_db_t *db_handle, const char*owner, ma_db_recordset_t **db_record) {
	if(db_handle && owner && db_record) {
		ma_db_statement_t *db_stmt = NULL;
		ma_error_t rc = MA_OK ;

		if(MA_OK == (rc = ma_db_statement_create(db_handle, GET_STATS_BY_OWNER_SQL_STMT, &db_stmt))) {

			(void)ma_db_statement_set_string(db_stmt, MA_AGENT_STATS_FIELD_OWNER, 1, owner, strlen(owner)) ;

			if(MA_OK == (rc = ma_db_statement_execute_query(db_stmt, db_record))) {
				rc = MA_OK ;
			}
			else
				rc = MA_ERROR_OBJECTNOTFOUND ;

			(void)ma_db_statement_release(db_stmt) ;	db_stmt = NULL ;

		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

#define GET_CURRENT_FOR_OWNER_AND_ATTRIBUTE_SQL_STMT "SELECT CURRENT FROM AGENT_STATS WHERE OWNER = ? AND ATTRIBUTE = ?"
#define UPDATE_CURRENT_FOR_OWNER_AND_ATTRIBUTE_SQL_STMT	"UPDATE AGENT_STATS SET CURRENT = ? WHERE OWNER = ? AND ATTRIBUTE = ?"
ma_error_t ma_stats_db_update_current(ma_db_t *db_handle, const char *owner, const char *attribute, ma_double_t current) {
	if(db_handle && owner && attribute && current > 0) {
		ma_db_statement_t *db_stmt = NULL;
		ma_error_t rc = MA_OK ;

		ma_double_t  current_value = 0.0 ;

		if(MA_OK == (rc = ma_db_statement_create(db_handle, GET_CURRENT_FOR_OWNER_AND_ATTRIBUTE_SQL_STMT, &db_stmt))) {
			ma_db_recordset_t *db_record = NULL ;

			(void)ma_db_statement_set_string(db_stmt, 1, 1, owner, strlen(owner)) ;
			(void)ma_db_statement_set_string(db_stmt, 2, 1, attribute, strlen(attribute)) ;

			if(MA_OK == (rc = ma_db_statement_execute_query(db_stmt, &db_record))) {
				if(MA_ERROR_NO_MORE_ITEMS != (rc = ma_db_recordset_next(db_record))) {
					char *value = NULL ;
					char *tmp = NULL ;

					(void)ma_db_recordset_get_string(db_record, 1, &value) ;
					current_value = strtod(value, &tmp) ;
				}
				ma_db_recordset_release(db_record) ;	db_record = NULL ;
			}
			(void)ma_db_statement_release(db_stmt) ;	db_stmt = NULL ;
		}

		current_value += current ;

		if(MA_OK == rc) {
			if(MA_OK == (rc = ma_db_statement_create(db_handle, UPDATE_CURRENT_FOR_OWNER_AND_ATTRIBUTE_SQL_STMT, &db_stmt))) {
				ma_temp_buffer_t buf = MA_TEMP_BUFFER_INIT ;
				char *p = NULL ;

				ma_temp_buffer_reserve(&buf, MA_MAX_LEN) ;
				MA_MSC_SELECT(_snprintf, snprintf)(p = (char *)ma_temp_buffer_get(&buf), ma_temp_buffer_capacity(&buf), "%.7lf", current_value) ;

				(void)ma_db_statement_set_string(db_stmt, 1, 1, p, strlen(p)) ;
				(void)ma_db_statement_set_string(db_stmt, 2, 1, owner, strlen(owner)) ;
				(void)ma_db_statement_set_string(db_stmt, 3, 1, attribute, strlen(attribute)) ;

				rc = ma_db_statement_execute(db_stmt) ;

				(void)ma_db_statement_release(db_stmt) ;	db_stmt = NULL ;
				(void)ma_temp_buffer_uninit(&buf) ;
			}
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}