#include "ma_stats_service_internal.h"
#include "ma/internal/services/stats/ma_stats_db_manager.h"

#include "ma/internal/core/msgbus/ma_msgbus_private.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/utils/configurator/ma_configurator_utils.h"
#include "ma/internal/utils/platform/ma_system_property.h"

#include "ma/internal/utils/xml/ma_xml.h"
#include "ma/internal/utils/xml/ma_xml_node.h"
#include "ma/internal/ma_macros.h"

#include "ma/internal/services/ma_agent_policy_settings_bag.h" 
#include "ma/internal/utils/time/ma_time.h"
#include "ma/datachannel/ma_datachannel.h"
#include "ma/datachannel/ma_datachannel_item.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "stats_service"

/*
<?xml version="1.0" encoding="UTF-8"?>
<StatisticsReport>
	<MachineInfo>
		<MachineName>BALAJIVISTA</MachineName>
		<AgentGUID>{2E1E6FA5-F130-4F2D-89C9-A45304A2493A}</AgentGUID>
		<IPAddress>192.168.71.129</IPAddress>
		<RawMACAddress>000c297aa1e7</RawMACAddress>
		<OSName>WVST</OSName>
		<UserName>SYSTEM</UserName>
		<TimeZoneBias>-330</TimeZoneBias>
		<GMTTime>2013-02-07 12:21:11 GMT</GMTTime>
		<ProductID>EPOAGENT3000</ProductID>
	</MachineInfo>
	<RelayServerStats>
		<FailedConnections>0</FailedConnections>
		<NumberOfTimesMaxed>0</NumberOfTimesMaxed>
	</RelayServerStats>
	<SAHUStats>
		<SAHUFilesServed>644<SAHUFilesServed>
		<SAHUBandwidthsaved>34638</SAHUBandwidthsaved>
	</SAHUStats>
	<P2PStats>
		<P2PFilesServed>644<P2PFilesServed>
		<P2PBandwidthsaved>34638</P2PBandwidthsaved>
	</P2PStats>
</StatisticsReport>
*/

typedef enum ma_stats_sent_e {
	MA_STATS_SENT_NONE = 0x00,
	MA_STATS_SENT_P2P = 0x01,
	MA_STATS_SENT_RELAY = 0x02,
	MA_STATS_SENT_SAHU = 0x04,
	MA_STATS_SENT_ALL = 0x08
} ma_stats_sent_t ;

static ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint) ;

static ma_error_t service_start(ma_service_t *service) ;

static ma_error_t service_stop(ma_service_t *service) ;

static void service_destroy(ma_service_t *service) ;

static const ma_service_methods_t methods = {
    &service_configure,
    &service_start,
    &service_stop,
    &service_destroy
} ;

ma_error_t ma_stats_service_create(const char *service_name, ma_service_t **service) {
    if(service) {
        ma_stats_service_t *self = (ma_stats_service_t *)calloc(1, sizeof(ma_stats_service_t)) ;

        if(!self)   return MA_ERROR_OUTOFMEMORY ;

        ma_service_init((ma_service_t *)self, &methods, service_name, self) ;

        *service = (ma_service_t *)self ;

        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

static ma_error_t ma_stats_service_deinit(ma_stats_service_t *self) {
    if(self) {
        if(self->stats_server) ma_msgbus_server_release(self->stats_server) ;
        self->stats_server = NULL ;

        self->context = NULL ;

        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

static ma_error_t ma_stats_add_default(ma_stats_service_t *self) ;

static ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint) {
    if(service && context) {
        ma_error_t rc = MA_OK;
        ma_stats_service_t *self = (ma_stats_service_t *)service;

		self->context = context ;

        if(MA_SERVICE_CONFIG_NEW == hint) {
			if(MA_OK == (rc = ma_stats_db_schema_create(ma_configurator_get_msgbus_database(MA_CONTEXT_GET_CONFIGURATOR(context))))) {
				if(MA_OK == (rc = ma_msgbus_server_create(MA_CONTEXT_GET_MSGBUS(self->context), MA_STATS_SERVICE_NAME_STR, &self->stats_server))) {
					ma_msgbus_server_setopt(self->stats_server, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO) ;
					ma_msgbus_server_setopt(self->stats_server, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC) ; 
					(void)ma_stats_add_default(self) ;	
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Successfully configured stats service.") ;
				}
			}
			if(MA_OK != rc) {
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to configure the stats service(%d).", rc) ;
				ma_stats_service_deinit(self) ;				
			}
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

static ma_error_t ma_stats_service_handler(ma_msgbus_server_t *server, ma_message_t *request, void *cb_data, ma_msgbus_client_request_t *c_request) ;

static ma_error_t service_start(ma_service_t *service) {
    ma_stats_service_t *self = (ma_stats_service_t *)service ;
	if(self) {
		ma_error_t rc = MA_OK ;
		ma_logger_t *logger =  MA_CONTEXT_GET_LOGGER(self->context) ;

		if(MA_OK == (rc = ma_msgbus_server_start(self->stats_server, ma_stats_service_handler, (void *)self))) {
				MA_LOG(logger, MA_LOG_SEV_INFO, "stats service started.") ;
		}
		else
			MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to start msgbus server, rc = %d.", rc) ;

		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

static ma_error_t service_stop(ma_service_t *service) {
    ma_stats_service_t *self = (ma_stats_service_t *)service;
    if(self) {
		(void)ma_msgbus_server_stop(self->stats_server) ;
		return MA_OK ;
	}
	return MA_ERROR_INVALIDARG ;
}

static void service_destroy(ma_service_t *service) {
	ma_stats_service_t *self = (ma_stats_service_t *)service;
	if(self) {
		(void)ma_stats_service_deinit(self) ;
		free(self) ;
	}
	return ;
}

static ma_error_t ma_stats_add_default(ma_stats_service_t *self) {
	ma_error_t rc = MA_OK ;
	ma_db_t *db_handle = ma_configurator_get_msgbus_database(MA_CONTEXT_GET_CONFIGURATOR(self->context)) ;

	if(MA_OK != ma_stats_db_check_default(db_handle)) {

		if(MA_OK == (rc = ma_stats_db_add_stat(db_handle, MA_STAT_P2P, MA_STAT_ATTRIBUTE_FILES_SERVED, 0.0, 0.0))
			&& MA_OK == (rc = ma_stats_db_add_stat(db_handle, MA_STAT_P2P, MA_STAT_ATTRIBUTE_HITS, 0.0, 0.0))
			&& MA_OK == (rc = ma_stats_db_add_stat(db_handle, MA_STAT_P2P, MA_STAT_ATTRIBUTE_BANDWITH_SAVED, 0.0, 0.0))) 
		{
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Default stats for p2p added.") ;
		}
		else {
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Default stats for p2p add failed(%d).", rc) ;
		}
		
		if(MA_OK == (rc = ma_stats_db_add_stat(db_handle, MA_STAT_RELAY_SERVER, MA_STAT_ATTRIBUTE_SUCCEEDED_CONNECTIONS, 0.0, 0.0))
			&& MA_OK == (rc = ma_stats_db_add_stat(db_handle, MA_STAT_RELAY_SERVER, MA_STAT_ATTRIBUTE_FAILED_CONNECTIONS, 0.0, 0.0))
			&& MA_OK == (rc = ma_stats_db_add_stat(db_handle, MA_STAT_RELAY_SERVER, MA_STAT_ATTRIBUTE_SURGED_CONNECTIONS, 0.0, 0.0))) 
		{
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Default stats for Relay added.") ;
		}
		else {
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Default stats for Relay add failed(%d).", rc) ;
		}
		
		if(MA_OK == (rc = ma_stats_db_add_stat(db_handle, MA_STAT_SAHU, MA_STAT_ATTRIBUTE_FILES_SERVED, 0.0, 0.0))
			&& MA_OK == (rc = ma_stats_db_add_stat(db_handle, MA_STAT_SAHU, MA_STAT_ATTRIBUTE_HITS, 0.0, 0.0))
			&& MA_OK == (rc = ma_stats_db_add_stat(db_handle, MA_STAT_SAHU, MA_STAT_ATTRIBUTE_BANDWITH_SAVED, 0.0, 0.0))) 
		{
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Default stats for SAHU added.") ;
		}
		else {
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Default stats for SAHU add failed(%d).", rc) ;
		}
	}
	return MA_OK ;
}

static ma_error_t ma_stats_collect_and_send_agent_statistics(ma_stats_service_t *self) ;

static ma_error_t ma_stats_service_handler(ma_msgbus_server_t *server, ma_message_t *request, void *cb_data, ma_msgbus_client_request_t *c_request) {
	if(server && request && c_request) {
        ma_error_t rc = MA_OK ;
        char *msg_type = NULL ;
        ma_variant_t *req_payload = NULL ;
        ma_message_t *response = NULL ;

        ma_stats_service_t *service = (ma_stats_service_t *)cb_data ;
		ma_logger_t *logger =  MA_CONTEXT_GET_LOGGER(service->context) ;

		if(MA_OK != (rc = ma_message_create(&response))) return MA_ERROR_OUTOFMEMORY;

        if(rc == ma_message_get_property(request, MA_STATS_MSG_KEY_TYPE_STR, &msg_type) && msg_type) {
            if(0 == strcmp(MA_STATS_MSG_PROP_VAL_RQST_COLLECT_AND_SEND_STR, msg_type)) {
				if(MA_OK == (rc = ma_stats_collect_and_send_agent_statistics(service))) {
					MA_LOG(logger, MA_LOG_SEV_DEBUG, "Agent statistics collected and sent.") ;
				}
				else {
					MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to collect and send stats(%d).", rc) ;
				}
             }
            else {
                MA_LOG(logger, MA_LOG_SEV_ERROR, "Invalid message type(%s).", msg_type) ;
                rc = MA_ERROR_UNEXPECTED ;
            }
        }
        else {
            MA_LOG(logger, MA_LOG_SEV_ERROR, "Invalid message type.") ;
            rc = MA_ERROR_UNEXPECTED ;
        }

        rc = ma_msgbus_server_client_request_post_result(c_request, rc, response) ;
        ma_message_release(response) ; response = NULL ;

        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

static ma_error_t ma_stats_xml_create(ma_xml_t **stats_xml) ;
static ma_error_t ma_stats_xml_add_machine_info(ma_xml_t *stats_xml, ma_context_t *context) ;
static ma_error_t ma_stats_xml_add_relay_stats(ma_xml_t *stats_xml, ma_relay_stats_t *relay_stats) ;
static ma_error_t ma_stats_xml_add_sahu_stats(ma_xml_t *stats_xml, ma_sa_repo_stats_t *sa_repo_stats) ;
static ma_error_t ma_stats_xml_add_p2p_stats(ma_xml_t *stats_xml, ma_p2p_stats_t *p2p_stats) ;
static void ma_stats_xml_destroy(ma_xml_t *stats_xml) ;

static ma_error_t ma_stats_send_dc_stats_report(ma_stats_service_t *self, ma_xml_t *stats_xml) ;
static ma_error_t get_p2p_stats_from_db(ma_stats_service_t *self, ma_p2p_stats_t **stats)  ;
static ma_error_t get_relay_stats_from_db(ma_stats_service_t *self, ma_relay_stats_t **stats)  ;
static ma_error_t get_sa_stats_from_db(ma_stats_service_t *self, ma_sa_repo_stats_t **stats)  ;

static ma_error_t  ma_stats_save_sent_statistics(ma_stats_service_t *self, ma_uint16_t stats_to_send) ;

static ma_error_t ma_stats_collect_and_send_agent_statistics(ma_stats_service_t *self) {
	ma_error_t rc = MA_OK ;

	ma_logger_t *logger =  MA_CONTEXT_GET_LOGGER(self->context) ;

	ma_xml_t *stats_xml = NULL ;

	if(MA_OK == (rc = ma_stats_xml_create(&stats_xml))) {
		ma_bool_t is_enabled = MA_FALSE ;
		ma_policy_settings_bag_t *pso_bag = MA_CONTEXT_GET_POLICY_SETTINGS_BAG(self->context) ;
		ma_uint16_t stats_to_send = MA_STATS_SENT_NONE ;

		rc = ma_stats_xml_add_machine_info(stats_xml, self->context) ;

		/* TODO DO we need to check for the policies?? */
		if(MA_OK == rc && MA_OK == ma_policy_settings_bag_get_bool(pso_bag, MA_RELAY_SERVICE_SECTION_NAME_STR, MA_RELAY_KEY_IS_ENABLED_INT, MA_FALSE, &is_enabled, 0) 
			&& is_enabled) 
		{
			if(MA_OK== (rc = get_relay_stats_from_db(self, &self->relay_stats)) && MA_OK == (rc = ma_stats_xml_add_relay_stats(stats_xml, self->relay_stats))) {
				stats_to_send = stats_to_send | MA_STATS_SENT_RELAY ;
			}
			else
				MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to append relay stats to report(%d).", rc) ;
		}

		if(MA_OK == rc && MA_OK == ma_policy_settings_bag_get_bool(pso_bag, MA_P2P_SERVICE_SECTION_NAME_STR, MA_P2P_KEY_ENABLE_SERVING_INT, MA_FALSE,&is_enabled, 0) 
			&& is_enabled) 
		{
			if(MA_OK== (rc = get_p2p_stats_from_db(self, &self->p2p_stats)) && MA_OK == (rc = ma_stats_xml_add_p2p_stats(stats_xml, self->p2p_stats))) {
				stats_to_send = stats_to_send | MA_STATS_SENT_P2P ;
			}
			else
				MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to append p2p stats to report(%d).", rc) ;
		}

		if(MA_OK == rc && MA_OK == ma_policy_settings_bag_get_bool(pso_bag, MA_HTTP_SERVER_SERVICE_SECTION_NAME_STR, MA_HTTP_SERVER_KEY_ENABLE_SUPER_AGENT_INT, MA_FALSE,&is_enabled, MA_FALSE) 
			&& is_enabled &&MA_OK == ma_policy_settings_bag_get_bool(pso_bag, MA_HTTP_SERVER_SERVICE_SECTION_NAME_STR, MA_HTTP_SERVER_KEY_ENABLE_SUPER_AGENT_REPOSITORY_INT,MA_FALSE, &is_enabled, MA_FALSE)
			&& is_enabled	) 
		{
			if(MA_OK== (rc = get_sa_stats_from_db(self, &self->sahu_stats)) && MA_OK == (rc = ma_stats_xml_add_sahu_stats(stats_xml, self->sahu_stats))) {
				stats_to_send = stats_to_send | MA_STATS_SENT_SAHU ;
			}
			else
				MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to append sahu stats to report(%d).", rc) ;
		}

		if(MA_OK == rc) {
			if(stats_to_send == MA_STATS_SENT_NONE) {
				MA_LOG(logger, MA_LOG_SEV_INFO, "No policy enabled for stats collection, No report will be sent.") ;
			}
			else {
				if(MA_OK == (rc = ma_stats_send_dc_stats_report(self, stats_xml))) {
					if(MA_OK != ma_stats_save_sent_statistics(self, stats_to_send)) {
						MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to persist stats sent in db(%d).", rc) ;
					}
				}
			}
		}
		else {
			MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to form stats report(%d).", rc) ;
		}

		(void)ma_stats_xml_destroy(stats_xml) ;	stats_xml = NULL ;
	}
	else
		MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to create stats report xml(%d).", rc) ;
					
	if(self->p2p_stats)	free(self->p2p_stats) ;
	self->p2p_stats = NULL ;

	if(self->relay_stats)	free(self->relay_stats) ;
	self->relay_stats = NULL ;

	if(self->sahu_stats)	free(self->sahu_stats) ;
	self->sahu_stats = NULL ;

	return rc ;
}

static ma_error_t ma_stats_xml_create(ma_xml_t **stats_xml) {
	if(stats_xml) {
		ma_xml_t *tmp_stats_xml = NULL ;
		ma_error_t rc = MA_OK ;
		ma_xml_node_t *root_node = NULL ;

		if(MA_OK == (rc = ma_xml_create(&tmp_stats_xml)) 
			&&	MA_OK == (rc = ma_xml_construct(tmp_stats_xml))) 
		{
				rc = ma_xml_node_create(ma_xml_get_node(tmp_stats_xml), MA_STATS_XML_NODE_ROOT, &root_node) ;
		}

		if(MA_OK != rc && tmp_stats_xml) { 
			ma_stats_xml_destroy(tmp_stats_xml) ;
			tmp_stats_xml = NULL ;
		}

		*stats_xml = tmp_stats_xml ;
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

static void get_stytem_time(char system_time[], size_t length){
	ma_time_t ma_time = {0};
	ma_time_get_gmttime(&ma_time);
		MA_MSC_SELECT(_snprintf, snprintf)(system_time, length-1, "%02d/%02d/%04d %02d:%02d:%02d", 
        ma_time.month,
        ma_time.day,
        ma_time.year,
        ma_time.hour,
        ma_time.minute,
        ma_time.second);
}


static ma_error_t ma_stats_xml_add_machine_info(ma_xml_t *stats_xml, ma_context_t *context) {
	if(stats_xml && context) {
		ma_error_t rc = MA_OK ;
		ma_xml_node_t *machine_info_node = NULL ;
		char sys_time[64] = {0};
		ma_configurator_t *configurator = MA_CONTEXT_GET_CONFIGURATOR(context) ;
		ma_system_property_t *sys_prop = MA_CONTEXT_GET_SYSTEM_PROPERTY(context) ;

		const ma_network_system_property_t *net = ma_system_property_network_get(sys_prop, MA_TRUE) ;
		const ma_os_system_property_t *os = ma_system_property_os_get(sys_prop, MA_FALSE) ;

		if(MA_OK == (rc = ma_xml_node_create(ma_xml_node_search(ma_xml_get_node(stats_xml),MA_STATS_XML_NODE_ROOT) , MA_STATS_XML_NODE_MACHINE_INFO, &machine_info_node))) {
			ma_xml_node_t *child_node = NULL ;

			ma_xml_node_create(machine_info_node, MA_STATS_XML_TAG_MACHINE_NAME, &child_node) ;
			ma_xml_node_set_data(child_node, "NA") ;
			child_node = NULL ;

			ma_xml_node_create(machine_info_node, MA_STATS_XML_TAG_AGENT_GUID, &child_node) ;
			{
				char *guid = NULL ;
				
				if(NULL == (guid = (char *)ma_configurator_get_agent_guid(configurator)) || ('\0' == guid[0])) {
					ma_buffer_t *buffer = NULL ;
					size_t len = 0 ;

					if(MA_OK == ma_ds_get_str(ma_configurator_get_datastore(configurator), MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_GUID_STR, &buffer)) {
						(void)ma_buffer_get_string(buffer, &guid, &len) ;
						(void)ma_configurator_set_guid(configurator, guid) ;
						(void)ma_buffer_release(buffer) ; buffer = NULL ;
					}
				}
				ma_xml_node_set_data(child_node, ma_configurator_get_agent_guid(configurator)) ;
				child_node = NULL ;
			}

			ma_xml_node_create(machine_info_node, MA_STATS_XML_TAG_IP_ADDRESS, &child_node) ;
			ma_xml_node_set_data(child_node, net->ip_address_formated) ;
			child_node = NULL ;

			ma_xml_node_create(machine_info_node, MA_STATS_XML_TAG_MAC_ADDRESS, &child_node) ;
			ma_xml_node_set_data(child_node, net->mac_address_formated) ;
			child_node = NULL ;

			ma_xml_node_create(machine_info_node, MA_STATS_XML_TAG_OS_NAME, &child_node) ;
			ma_xml_node_set_data(child_node, os->type) ;
			child_node = NULL ;

			ma_xml_node_create(machine_info_node, MA_STATS_XML_TAG_USER_NAME, &child_node) ;
			ma_xml_node_set_data(child_node, "mfe") ;
			child_node = NULL ;

			ma_xml_node_create(machine_info_node, MA_STATS_XML_TAG_TIMEZONE_BIAS, &child_node) ;
			ma_xml_node_set_data(child_node, "0") ;
			child_node = NULL ;
			get_stytem_time(sys_time, 64);
			ma_xml_node_create(machine_info_node, MA_STATS_XML_TAG_GMT_TIME, &child_node) ;
			ma_xml_node_set_data(child_node, sys_time) ;
			child_node = NULL ;

			ma_xml_node_create(machine_info_node, MA_STATS_XML_TAG_PRODUCT_ID, &child_node) ;
			ma_xml_node_set_data(child_node, ma_configurator_get_agent_software_id(configurator)) ;
			child_node = NULL ;
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

static ma_error_t ma_stats_xml_add_relay_stats(ma_xml_t *stats_xml, ma_relay_stats_t *relay_stats) {
	if(stats_xml && relay_stats) {
		ma_error_t rc = MA_OK ;
		ma_xml_node_t *relay_stats_node = NULL ;
		

		if(MA_OK == (rc = ma_xml_node_create(ma_xml_node_search(ma_xml_get_node(stats_xml),MA_STATS_XML_NODE_ROOT), MA_STATS_XML_NODE_RELAY_STATS, &relay_stats_node))) {
			ma_xml_node_t *child_node = NULL ;
			char value[MA_MAX_LEN] = {0} ;

			/*ma_xml_node_create(relay_stats_node, MA_STATS_XML_TAG_SUCCEEDED_CONNECTIONS, &child_node) ;
			MA_MSC_SELECT(_snprintf, snprintf)(value, MA_MAX_LEN, "%lu", relay_stats->succeeded_connections) ;
			ma_xml_node_set_data(child_node, value) ;
			memset(value, 0, MA_MAX_LEN) ;
			child_node = NULL ;*/

			ma_xml_node_create(relay_stats_node, MA_STATS_XML_TAG_FAILED_CONNECRIONS, &child_node) ;
			MA_MSC_SELECT(_snprintf, snprintf)(value, MA_MAX_LEN, "%lu", relay_stats->failed_connections) ;
			ma_xml_node_set_data(child_node, value) ;
			memset(value, 0, MA_MAX_LEN) ;
			child_node = NULL ;

			ma_xml_node_create(relay_stats_node, MA_STATS_XML_TAG_SURGED_CONNECTIONS, &child_node) ;
			MA_MSC_SELECT(_snprintf, snprintf)(value, MA_MAX_LEN, "%lu", relay_stats->surged_connections) ;
			ma_xml_node_set_data(child_node, value) ;
			memset(value, 0, MA_MAX_LEN) ;
			child_node = NULL ;
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

static ma_error_t ma_stats_xml_add_sahu_stats(ma_xml_t *stats_xml, ma_sa_repo_stats_t *sa_repo_stats) {
	if(stats_xml && sa_repo_stats) {
		ma_error_t rc = MA_OK ;
		ma_xml_node_t *sahu_stats_node = NULL ;

		if(MA_OK == (rc = ma_xml_node_create(ma_xml_node_search(ma_xml_get_node(stats_xml),MA_STATS_XML_NODE_ROOT), MA_STATS_XML_NODE_SAHU_STATS, &sahu_stats_node))) {
			ma_xml_node_t *child_node = NULL ;
			char value[MA_MAX_LEN] = {0} ;

			ma_xml_node_create(sahu_stats_node, MA_STATS_XML_TAG_SAHU_FILES_SERVED, &child_node) ;
			MA_MSC_SELECT(_snprintf, snprintf)(value, MA_MAX_LEN, "%lu", sa_repo_stats->files_served) ;
			ma_xml_node_set_data(child_node, value) ;
			memset(value, 0, MA_MAX_LEN) ;
			child_node = NULL ;

			/*ma_xml_node_create(sahu_stats_node, MA_STATS_XML_TAG_SAHU_HITS, &child_node) ;
			MA_MSC_SELECT(_snprintf, snprintf)(value, MA_MAX_LEN, "%lu", sa_repo_stats->num_hits) ;
			ma_xml_node_set_data(child_node, value) ;
			memset(value, 0, MA_MAX_LEN) ;
			child_node = NULL ;*/

			ma_xml_node_create(sahu_stats_node, MA_STATS_XML_TAG_SAHU_BANDWIDTH_SAVED, &child_node) ;
			MA_MSC_SELECT(_snprintf, snprintf)(value, MA_MAX_LEN, "%lf MB", sa_repo_stats->bandwidth_saved) ;
			ma_xml_node_set_data(child_node, value) ;
			memset(value, 0, MA_MAX_LEN) ;
			child_node = NULL ;
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

static ma_error_t ma_stats_xml_add_p2p_stats(ma_xml_t *stats_xml, ma_p2p_stats_t *p2p_stats) {
	if(stats_xml && p2p_stats) {
		ma_error_t rc = MA_OK ;
		ma_xml_node_t *p2p_stats_node = NULL ;

		if(MA_OK == (rc = ma_xml_node_create(ma_xml_node_search(ma_xml_get_node(stats_xml),MA_STATS_XML_NODE_ROOT), MA_STATS_XML_NODE_P2P_STATS, &p2p_stats_node))) {
			ma_xml_node_t *child_node = NULL ;
			char value[MA_MAX_LEN] = {0} ;

			ma_xml_node_create(p2p_stats_node, MA_STATS_XML_TAG_P2P_FILES_SERVED, &child_node) ;
			MA_MSC_SELECT(_snprintf, snprintf)(value, MA_MAX_LEN, "%lu", p2p_stats->files_served) ;
			ma_xml_node_set_data(child_node, value) ;
			memset(value, 0, MA_MAX_LEN) ;
			child_node = NULL ;

			/*ma_xml_node_create(p2p_stats_node, MA_STATS_XML_TAG_P2P_HITS, &child_node) ;
			MA_MSC_SELECT(_snprintf, snprintf)(value, MA_MAX_LEN, "%lu", p2p_stats->num_hits) ;
			ma_xml_node_set_data(child_node, value) ;
			memset(value, 0, MA_MAX_LEN) ;
			child_node = NULL ;*/

			ma_xml_node_create(p2p_stats_node, MA_STATS_XML_TAG_P2P_BANDWIDTH_SAVED, &child_node) ;
			MA_MSC_SELECT(_snprintf, snprintf)(value, MA_MAX_LEN, "%lf MB", p2p_stats->bandwidth_saved) ;
			ma_xml_node_set_data(child_node, value) ;
			memset(value, 0, MA_MAX_LEN) ;
			child_node = NULL ;
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

static void ma_stats_xml_destroy(ma_xml_t *stats_xml) {
	if(stats_xml) {
		(void)ma_xml_release(stats_xml) ;
	}
	return ;
}

static ma_error_t ma_stats_send_dc_stats_report(ma_stats_service_t *self, ma_xml_t *stats_xml) {
	if(self && stats_xml) {
		ma_error_t rc = MA_OK ;
		ma_bytebuffer_t *report_buffer = NULL ;
		ma_client_t *client = MA_CONTEXT_GET_CLIENT(self->context) ;
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context) ;

		if(MA_OK == (rc = ma_bytebuffer_create(4096, &report_buffer))) {	/* TODO optimize 4096*/
			if(MA_OK == (rc = ma_xml_save_to_buffer(stats_xml, report_buffer))) {
				ma_variant_t *dc_payload = NULL ;

				MA_LOG(logger, MA_LOG_SEV_DEBUG, "Stats report to be sent %s", (char *)ma_bytebuffer_get_bytes(report_buffer)) ;

				if(MA_OK == (rc = ma_variant_create_from_string((char *)ma_bytebuffer_get_bytes(report_buffer), ma_bytebuffer_get_size(report_buffer), &dc_payload))) {
					ma_datachannel_item_t *stats_dc_item = NULL ;

					if(MA_OK == (rc = ma_datachannel_item_create("EPOAGENT3000_Statistics", dc_payload, &stats_dc_item))) {
						ma_datachannel_item_set_option(stats_dc_item, MA_DATACHANNEL_PERSIST, MA_TRUE);
						ma_datachannel_item_set_correlation_id(stats_dc_item, 0) ;
						ma_datachannel_item_set_ttl(stats_dc_item, 10*1000) ;
						ma_datachannel_item_set_origin(stats_dc_item, MA_SOFTWAREID_GENERAL_STR) ;
						ma_datachannel_item_set_payload(stats_dc_item, dc_payload) ;

						rc = ma_datachannel_async_send(client, MA_SOFTWAREID_GENERAL_STR, stats_dc_item) ;
						
						(void)ma_datachannel_item_release(stats_dc_item) ;
					}
					else
						MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to create dc item(%d)", rc) ;
					(void)ma_variant_release(dc_payload) ;
				}
				else
					MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to create dc payload(%d)", rc) ;
			}
			else
				MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to get buffer from xml(%d)", rc) ;
		
		    (void)ma_bytebuffer_release(report_buffer);
		}
		else
			MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to get bytebuffer buffer(%d)", rc) ;

		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

static ma_error_t get_p2p_stats_from_db(ma_stats_service_t *self, ma_p2p_stats_t **stats) {
	if(self && stats) {
		ma_error_t rc = MA_OK ;
		ma_db_t *db_handle = ma_configurator_get_msgbus_database(MA_CONTEXT_GET_CONFIGURATOR(self->context)) ;
		ma_db_recordset_t *db_record= NULL ;

		ma_p2p_stats_t *p2p_stat = (ma_p2p_stats_t *)calloc(1, sizeof(ma_p2p_stats_t)) ;

		if(!p2p_stat)	return MA_ERROR_OUTOFMEMORY ;

		if(MA_OK == (rc = ma_stats_db_get_stats_by_owner(db_handle, MA_STAT_P2P, &db_record))) {
			while(MA_ERROR_NO_MORE_ITEMS != ma_db_recordset_next(db_record)) {
				char *attribute = NULL, *tmp_ptr = NULL, *value = NULL ;
				ma_double_t last_sent = 0.0, current = 0.0 ;

				(void)ma_db_recordset_get_string(db_record, 1, &attribute) ;
				/* TODO add other stats files served , hits */
				if(!strcmp(attribute, MA_STAT_ATTRIBUTE_BANDWITH_SAVED)) {
					p2p_stat->bandwidth_saved = 0 ;
					
					(void)ma_db_recordset_get_string(db_record, 2, &value) ;
					current = strtod(value, &tmp_ptr) ;
					tmp_ptr = NULL ;	value = NULL ;

					(void)ma_db_recordset_get_string(db_record, 3, &value) ;
					last_sent = strtod(value, &tmp_ptr) ;
					tmp_ptr = NULL ;	value = NULL ;

					p2p_stat->bandwidth_saved = current - last_sent ;
				}
				if(!strcmp(attribute, MA_STAT_ATTRIBUTE_FILES_SERVED)) {
					p2p_stat->files_served = 0 ;
					
					(void)ma_db_recordset_get_string(db_record, 2, &value) ;
					current = strtod(value, &tmp_ptr) ;
					tmp_ptr = NULL ;	value = NULL ;

					(void)ma_db_recordset_get_string(db_record, 3, &value) ;
					last_sent = strtod(value, &tmp_ptr) ;
					tmp_ptr = NULL ;	value = NULL ;

					p2p_stat->files_served = (ma_uint64_t)(current - last_sent) ;
				}
			}
			*stats = p2p_stat ;
			(void)ma_db_recordset_release(db_record) ; db_record = NULL ;
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

static ma_error_t get_relay_stats_from_db(ma_stats_service_t *self, ma_relay_stats_t **stats) {
	if(self && stats) {
		ma_error_t rc = MA_OK ;
		ma_db_t *db_handle = ma_configurator_get_msgbus_database(MA_CONTEXT_GET_CONFIGURATOR(self->context)) ;
		ma_db_recordset_t *db_record= NULL ;

		ma_relay_stats_t *relay_stat = (ma_relay_stats_t *)calloc(1, sizeof(ma_relay_stats_t)) ;

		if(!relay_stat)	return MA_ERROR_OUTOFMEMORY ;

		if(MA_OK == (rc = ma_stats_db_get_stats_by_owner(db_handle, MA_STAT_RELAY_SERVER, &db_record))) {
			while(MA_ERROR_NO_MORE_ITEMS != ma_db_recordset_next(db_record)) {
				char *attribute = NULL, *tmp_ptr = NULL, *value = NULL ;
				ma_uint64_t last = 0, current = 0 ;

				(void)ma_db_recordset_get_string(db_record, 1, &attribute) ;

				/* TODO add other stats files succeeded connections */
				if(!strcmp(attribute, MA_STAT_ATTRIBUTE_FAILED_CONNECTIONS)) {
					relay_stat->failed_connections = 0 ;

					(void)ma_db_recordset_get_string(db_record, 2, &value) ;
					current = MA_MSC_SELECT(_strtoui64, strtoull)(value, &tmp_ptr, 0) ;
					tmp_ptr = NULL, value = NULL ;

					(void)ma_db_recordset_get_string(db_record, 3, &value) ;
					last = MA_MSC_SELECT(_strtoui64, strtoull)(value, &tmp_ptr, 0) ;
					tmp_ptr = NULL, value = NULL ;

					relay_stat->failed_connections = current - last ;
				}
				if(!strcmp(attribute, MA_STAT_ATTRIBUTE_SURGED_CONNECTIONS)) {
					relay_stat->surged_connections = 0 ;

					(void)ma_db_recordset_get_string(db_record, 2, &value) ;
					current = MA_MSC_SELECT(_strtoui64, strtoull)(value, &tmp_ptr, 0) ;
					tmp_ptr = NULL, value = NULL ;

					(void)ma_db_recordset_get_string(db_record, 3, &value) ;
					last = MA_MSC_SELECT(_strtoui64, strtoull)(value, &tmp_ptr, 0) ;
					tmp_ptr = NULL, value = NULL ;

					relay_stat->surged_connections = current - last ;
				}
			}
			*stats = relay_stat ;
			(void)ma_db_recordset_release(db_record) ; db_record = NULL ;
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

static ma_error_t get_sa_stats_from_db(ma_stats_service_t *self, ma_sa_repo_stats_t **stats) {
	if(self && stats) {
		ma_error_t rc = MA_OK ;
		ma_db_t *db_handle = ma_configurator_get_msgbus_database(MA_CONTEXT_GET_CONFIGURATOR(self->context)) ;
		ma_db_recordset_t *db_record= NULL ;

		ma_sa_repo_stats_t *sa_stat = (ma_sa_repo_stats_t *)calloc(1, sizeof(ma_sa_repo_stats_t)) ;

		if(!sa_stat)	return MA_ERROR_OUTOFMEMORY ;

		if(MA_OK == (rc = ma_stats_db_get_stats_by_owner(db_handle, MA_STAT_SAHU, &db_record))) {
			while(MA_ERROR_NO_MORE_ITEMS != ma_db_recordset_next(db_record)) {
				char *attribute = NULL, *tmp_ptr = NULL, *value = NULL ;
				ma_double_t last_sent = 0.0, current = 0.0 ;

				(void)ma_db_recordset_get_string(db_record, 1, &attribute) ;
				/* TODO add other stats files served , hits */
				if(!strcmp(attribute, MA_STAT_ATTRIBUTE_BANDWITH_SAVED)) {
					sa_stat->bandwidth_saved = 0 ;
					
					(void)ma_db_recordset_get_string(db_record, 2, &value) ;
					current = strtod(value, &tmp_ptr) ;
					tmp_ptr = NULL ;	value = NULL ;

					(void)ma_db_recordset_get_string(db_record, 3, &value) ;
					last_sent = strtod(value, &tmp_ptr) ;
					tmp_ptr = NULL ;	value = NULL ;

					sa_stat->bandwidth_saved = current - last_sent ;
				}
				if(!strcmp(attribute, MA_STAT_ATTRIBUTE_FILES_SERVED)) {
					sa_stat->files_served = 0 ;
					
					(void)ma_db_recordset_get_string(db_record, 2, &value) ;
					current = strtod(value, &tmp_ptr) ;
					tmp_ptr = NULL ;	value = NULL ;

					(void)ma_db_recordset_get_string(db_record, 3, &value) ;
					last_sent = strtod(value, &tmp_ptr) ;
					tmp_ptr = NULL ;	value = NULL ;

					sa_stat->files_served = (ma_uint64_t)(current - last_sent) ;
				}
			}
			*stats = sa_stat ;
			(void)ma_db_recordset_release(db_record) ; db_record = NULL ;
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}

static ma_error_t  ma_stats_save_sent_statistics(ma_stats_service_t *self, ma_uint16_t stats_to_send) {
	if(self) {
		ma_error_t rc = MA_OK ;
		ma_db_t *db_handle = ma_configurator_get_msgbus_database(MA_CONTEXT_GET_CONFIGURATOR(self->context)) ;
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context) ;

		if(MA_STATS_SENT_P2P == (stats_to_send & MA_STATS_SENT_P2P)) {
			if(MA_OK == ma_stats_db_update_last_sent(db_handle, MA_STAT_P2P, MA_STAT_ATTRIBUTE_FILES_SERVED, (ma_double_t)self->p2p_stats->files_served)) {
				if(MA_OK != ma_stats_db_update_last_sent(db_handle, MA_STAT_P2P, MA_STAT_ATTRIBUTE_BANDWITH_SAVED, self->p2p_stats->bandwidth_saved)) {
					MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to save p2p stats <rc = %d>", rc) ;
				}
			}
			else
				MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to save p2p stats <rc = %d>", rc) ;
		}
		if(MA_STATS_SENT_RELAY == (stats_to_send & MA_STATS_SENT_RELAY)) {
			if(MA_OK == ma_stats_db_update_last_sent(db_handle, MA_STAT_RELAY_SERVER, MA_STAT_ATTRIBUTE_FAILED_CONNECTIONS, (ma_double_t)self->relay_stats->failed_connections)) {
				if(MA_OK != ma_stats_db_update_last_sent(db_handle, MA_STAT_RELAY_SERVER, MA_STAT_ATTRIBUTE_SURGED_CONNECTIONS, (ma_double_t)self->relay_stats->surged_connections)) {
					MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to save relay stats <rc = %d>", rc) ;
				}
			}
			else
				MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to save p2p stats <rc = %d>", rc) ;
		}
		if(MA_STATS_SENT_SAHU == (stats_to_send & MA_STATS_SENT_SAHU)) {
			if(MA_OK == ma_stats_db_update_last_sent(db_handle, MA_STAT_SAHU, MA_STAT_ATTRIBUTE_FILES_SERVED, (ma_double_t)self->sahu_stats->files_served)) {
				if(MA_OK != ma_stats_db_update_last_sent(db_handle, MA_STAT_SAHU, MA_STAT_ATTRIBUTE_BANDWITH_SAVED, self->sahu_stats->bandwidth_saved)) {
					MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to save sahu stats <rc = %d>", rc) ;
				}
			}
			else
				MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to save sahu stats <rc = %d>", rc) ;
		}
		return rc ;
	}
	return MA_ERROR_INVALIDARG ;
}