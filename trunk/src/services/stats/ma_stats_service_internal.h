#ifndef MA_STATS_SERVICE_INTERNAL_H_INCLUDED
#define MA_STATS_SERVICE_INTERNAL_H_INCLUDED

#include "ma/internal/apps/macmnsvc/ma_stats_service.h"
#include "ma/ma_msgbus.h"

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "stats_service"

MA_CPP(extern "C" {)

typedef struct ma_p2p_stats_s {
    ma_uint64_t		files_served ;
	ma_uint64_t		num_hits ;
	ma_double_t		bandwidth_saved ;	/* in MB */
} ma_p2p_stats_t ;

typedef struct ma_relay_stats_s {
	ma_uint64_t		total_connections ;
	ma_uint64_t		failed_connections ;
	ma_uint64_t		succeeded_connections ;
	ma_uint64_t		surged_connections ;
} ma_relay_stats_t ;

typedef struct ma_sa_repo_stats_s {
    ma_uint64_t		files_served ;
	ma_uint64_t		num_hits ;
	ma_double_t		bandwidth_saved ;	/* in MB */
} ma_sa_repo_stats_t ;

struct ma_stats_service_s {
    ma_service_t			base ;
	ma_context_t			*context ;
	ma_msgbus_server_t		*stats_server ;
	ma_p2p_stats_t			*p2p_stats ;
	ma_relay_stats_t		*relay_stats ;
	ma_sa_repo_stats_t		*sahu_stats ;
} ;

MA_CPP(})

#endif  /* MA_STATS_SERVICE_INTERNAL_H_INCLUDED */
