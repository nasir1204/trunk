#ifndef MA_DATACHANNEL_SERVICE_UTILS_H_INCLUDED
#define MA_DATACHANNEL_SERVICE_UTILS_H_INCLUDED

#include "ma/ma_variant.h"
#include "ma/internal/utils/datastructures/ma_map.h"

MA_CPP(extern "C" {)


MA_MAP_DECLARE(ma_dc_spipe_counter, char *, ma_int64_t);

ma_error_t validate_datachannel_msgbus_msgupload_message(ma_datachannel_service_t *self, ma_message_t *request_payload, ma_message_t *response);

ma_error_t validate_datachannel_msgbus_subscription_message(ma_datachannel_service_t *self, ma_message_t *request_payload, ma_message_t *response);

ma_error_t validate_datachannel_msgbus_compat_message(ma_datachannel_service_t *self, ma_message_t *request_payload, ma_message_t *response);

ma_error_t ma_datachannel_utils_validate_msgbus_message(ma_datachannel_service_t *self, ma_message_t *request_payload, ma_message_t *response, ma_datachannel_messages_type_t *request_type);

ma_error_t ma_datachannel_database_convert_payload_to_raw(ma_datachannel_service_t *self, ma_variant_t *unencrypted_payload, ma_variant_t **processed_variant);

ma_error_t ma_datachannel_notify_delivery_confirmation(ma_datachannel_service_t *self, char *ids);

ma_error_t ma_datachannel_notify_unsaved_item_purge(ma_datachannel_service_t *self, ma_error_t rc, ma_message_t *unsaved_item);

ma_error_t ma_datachannel_notify_purge(ma_datachannel_service_t *self, ma_db_recordset_t *recordset, ma_error_t reason);

ma_error_t ma_datachannel_manager_spipe_msg_upload_decorator(ma_datachannel_service_t *service, ma_spipe_decorator_t *decorator, ma_spipe_priority_t priority, ma_spipe_package_t *spipe_pkg);

ma_error_t ma_datachannel_manager_spipe_msg_request_decorator(ma_datachannel_service_t *service, ma_spipe_decorator_t *decorator, ma_spipe_priority_t priority, ma_spipe_package_t *spipe_pkg);

ma_error_t ma_datachannel_manager_spipe_msg_available_handler(ma_datachannel_service_t *service, ma_spipe_handler_t *handler, const ma_spipe_response_t *spipe_response);

ma_error_t ma_datachannel_manager_spipe_msg_response_handler(ma_datachannel_service_t *service, ma_spipe_handler_t *handler, const ma_spipe_response_t *spipe_response);

ma_error_t ma_datachannel_utils_msg_upload_populate_spipe_package(ma_datachannel_service_t *serivce , const char *guid, ma_db_recordset_t *recordset, MA_MAP_T(ma_dc_spipe_counter) *map, ma_spipe_package_t *spike_pkg, ma_int64_t *row_id);

ma_error_t ma_datachannel_utils_msg_response_process_message_id(ma_datachannel_service_t *service, ma_spipe_package_t *package, const char *message_id, ma_int64_t count);

ma_error_t ma_datachannel_utils_process_datachannel_item_routing(ma_datachannel_service_t *self, ma_db_recordset_t *db_recordset);

MA_CPP(})

#endif

