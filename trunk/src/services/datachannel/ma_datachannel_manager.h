#ifndef MA_DATACHANNEL_MANAGER_H_INCLUDED
#define MA_DATACHANNEL_MANAGER_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/services/ahclient/ma_spipe_decorator.h"
#include "ma/internal/services/ahclient/ma_spipe_handler.h"

MA_CPP(extern "C" { )
	

ma_error_t ma_datachannel_manager_create(ma_datachannel_service_t *service, ma_datachannel_manager_t **dc_manager);

ma_error_t ma_datachannel_manager_release(ma_datachannel_manager_t *dc_manager);

ma_error_t ma_datachannel_manager_start(ma_datachannel_manager_t *dc_manager);

ma_error_t ma_datachannel_manager_stop(ma_datachannel_manager_t *dc_manager);

ma_spipe_decorator_t *ma_datachannel_manager_get_spipe_decorator(ma_datachannel_manager_t *dc_manager);

ma_spipe_handler_t *ma_datachannel_manager_get_spipe_handler(ma_datachannel_manager_t *dc_manager);

ma_error_t ma_datachannel_manager_raise_spipe_alert(ma_datachannel_manager_t *dc_manager, const char *pkg_type);

ma_error_t ma_datachannel_manager_handle_primary_subscription_request(ma_datachannel_manager_t *self, char *message_id);

MA_CPP(})

#endif /*MA_DATACHANNEL_MANAGER_H_INCLUDED*/
