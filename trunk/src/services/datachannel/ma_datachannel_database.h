#ifndef MA_DATACHANNEL_DATABASE_H_INCLUDED
#define MA_DATACHANNEL_DATABASE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/database/ma_db.h"
#include "ma/internal/utils/database/ma_db_recordset.h"
#include "ma/internal/utils/database/ma_db_statement.h"

MA_CPP( extern "C" {)

ma_error_t ma_datachannel_database_add_instance(ma_datachannel_service_t *self);

ma_error_t ma_datachannel_database_remove_instance(ma_datachannel_service_t *self);

ma_error_t ma_datachannel_database_add_message(ma_datachannel_service_t *self, ma_message_t *dc_message, ma_int32_t from_server, ma_int64_t *inserted_row_id);

ma_error_t ma_datachannel_database_remove_message(ma_datachannel_service_t *self, ma_bool_t notify, ma_error_t reason, ma_uint64_t row_id);

ma_error_t ma_datachannel_database_remove_messages(ma_datachannel_service_t *self, char *csv_row_ids);

ma_error_t ma_datachannel_database_get_expired_messages(ma_datachannel_service_t *self, time_t current_time, ma_int32_t from_epo, ma_db_recordset_t **recordset);

ma_error_t ma_datachannel_database_get_epo_upload_batch(ma_datachannel_service_t *self, const char *guid, ma_db_recordset_t **recordset);

ma_error_t ma_datachannel_database_get_sent_messages_notification_data(ma_datachannel_service_t *self, char *ids, ma_db_recordset_t **recordset);

ma_error_t ma_datachannel_database_get_message_notification_data(ma_datachannel_service_t *self, ma_int64_t row_id, ma_db_recordset_t **recordset);

ma_error_t ma_datachannel_database_get_undelivered_messages(ma_datachannel_service_t *self, ma_db_recordset_t **recordset);

ma_error_t ma_datachannel_database_get_undelivered_messages_by_id(ma_datachannel_service_t *self, const char *item_id, ma_db_recordset_t **recordset);

ma_error_t ma_datachannel_database_get_messages_count(ma_datachannel_service_t *self, ma_int64_t *to_server_rows_count, ma_int64_t *from_server_rows_count);

ma_error_t ma_datachannel_database_remove_non_persistent_messages_from_ids(ma_datachannel_service_t *self, char *ids);

MA_CPP(})


#endif
