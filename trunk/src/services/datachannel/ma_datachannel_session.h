#ifndef MA_DATACHANNEL_SESSION_H_INCLUDED
#define MA_DATACHANNEL_SESSION_H_INCLUDED

MA_CPP( extern "C" {) 


ma_error_t ma_datachannel_session_create(struct ma_datachannel_manager_s *dc_manager, ma_datachannel_session_t **session);

ma_error_t ma_datachannel_session_release(ma_datachannel_session_t *session);

ma_error_t ma_datachannel_session_add_to_cookie(ma_datachannel_session_t *session, ma_uint64_t row_id);

ma_error_t ma_datachannel_session_get_cookie(ma_datachannel_session_t *session, char **cookie, size_t *size);

ma_error_t ma_datachannel_session_get_state(ma_datachannel_session_t *session, ma_datachannel_session_state_t *state);

ma_error_t ma_datachannel_session_begin(ma_datachannel_session_t *session);

ma_error_t ma_datachannel_session_finish(ma_datachannel_session_t *session);

MA_CPP(});
#endif /*MA_DATACHANNEL_SESSION_H_INCLUDED*/

