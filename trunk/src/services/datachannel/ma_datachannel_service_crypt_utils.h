#ifndef MA_DATACHANNEL_SERVICE_CRYPT_UTILS_H_INCLUDED
#define MA_DATACHANNEL_SERVICE_CRYPT_UTILS_H_INCLUDED


#include "ma/ma_common.h"
#include "ma/ma_variant.h"
#include "ma/internal/services/datachannel/ma_datachannel_service.h"

MA_CPP(extern "C" {)

ma_error_t ma_datachannel_service_encrypt_data(ma_datachannel_service_t *self, ma_variant_t *plain_data, ma_variant_t **encrypted);
ma_error_t ma_datachannel_service_decrypt_data(ma_datachannel_service_t *self, ma_variant_t *encrypted_data, ma_variant_t **plain_data);

MA_CPP(})
#endif

