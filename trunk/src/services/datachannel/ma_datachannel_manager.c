#include "ma_datachannel_service_internal.h"
#include "ma/datastore/ma_ds.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/core/msgbus/ma_msgbus_private.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/services/ahclient/ma_ah_client_service.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/datachannel/ma_datachannel_item.h"
#include "ma/internal/defs/ma_info_defs.h"
#include "ma/info/ma_info.h"

#include <time.h>
#include <stdio.h>

#ifdef LOG_FACILITY_NAME
	#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME	"DataChannel.Manager"

MA_MAP_DEFINE(ma_dc_spipe_counter, char *, strdup, free, ma_int64_t, , , strcmp);

static ma_error_t dc_spipe_decorate(ma_spipe_decorator_t *decorator, ma_spipe_priority_t priority, ma_spipe_package_t *spipe_pkg);
static ma_error_t dc_get_spipe_priority(ma_spipe_decorator_t *decorator, ma_spipe_priority_t *priority);
static ma_error_t dc_decorator_destroy(ma_spipe_decorator_t *decorator);


static ma_error_t dc_spipe_handler(ma_spipe_handler_t *handler, const ma_spipe_response_t *spipe_response);
static ma_error_t dc_spipe_handler_destroy(ma_spipe_handler_t *handler);

static void ma_datachannel_manager_retry_cb(uv_timer_t *timer_handle,int status);
static void ma_datachannel_manager_timer_check(ma_datachannel_manager_t *self, ma_bool_t b_startup);
static void ma_datachannel_manager_upload_timer_cb(uv_timer_t *timer_handle, int status);
static void ma_datachannel_manager_routing_timer_cb(uv_timer_t *timer_handle, int status);

static ma_spipe_decorator_methods_t dc_spipe_decorator_methods = {    
    &dc_spipe_decorate,
    &dc_get_spipe_priority,
    &dc_decorator_destroy
} ;


/*SPIPE Handler Methods*/

static ma_spipe_handler_methods_t dc_spipe_handler_methods = {    
    &dc_spipe_handler,
    &dc_spipe_handler_destroy
};

ma_error_t ma_datachannel_manager_create(ma_datachannel_service_t *service, ma_datachannel_manager_t **dc_manager) {
	if(service && dc_manager) {
		ma_error_t rc = MA_ERROR_OUTOFMEMORY;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(service->context);
        ma_msgbus_t *msgbus = MA_CONTEXT_GET_MSGBUS(service->context);
		ma_datachannel_manager_t *self = (ma_datachannel_manager_t *) calloc(1, sizeof(ma_datachannel_manager_t));
		if(self) {
			self->spipe_decorator	= (ma_spipe_decorator_t *) calloc(1, sizeof(ma_spipe_decorator_t));
			if(self->spipe_decorator) {
				self->spipe_handler		= (ma_spipe_handler_t *) calloc(1, sizeof(ma_spipe_handler_t));
				if(self->spipe_handler) {
					if(MA_OK == (rc = ma_service_manager_create(msgbus, &self->service_manager))) {
						if(MA_OK == (rc = ma_datachannel_session_create(self, &self->upload_session))){
							if(MA_OK == (rc = ma_datachannel_session_create(self, &self->request_session))){
								if(MA_OK == (rc = ma_msgbus_subscriber_create(msgbus, &self->network_subscriber))) {
									self->service = service;
									self->spipe_decorator->data = self;
									self->spipe_handler->data = self;
									self->nw_conn_status = MA_NETWORK_CONNECTIVITY_STATUS_CONNECTED;
									ma_spipe_decorator_init((ma_spipe_decorator_t *)self->spipe_decorator, &dc_spipe_decorator_methods, (void *)self) ;
									ma_spipe_handler_init((ma_spipe_handler_t *)self->spipe_handler, &dc_spipe_handler_methods, self) ;
									*dc_manager = self;
									MA_LOG(logger, MA_LOG_SEV_INFO, "DataChannel Service started successfully"); 
									return MA_OK;
								}
								ma_datachannel_session_release(self->request_session);
							}
							ma_datachannel_session_release(self->upload_session);
						}
						ma_service_manager_release(self->service_manager);
						self->service_manager = NULL;
					}
					free(self->spipe_handler);
				}
				free(self->spipe_decorator);
			}
			free(self);
		}
		MA_LOG(logger, MA_LOG_SEV_CRITICAL, "Failed to create DataChannel service manager - rc <%d>", rc);
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_datachannel_manager_release(ma_datachannel_manager_t *self) {
	if(self) {
		(void)ma_datachannel_session_release(self->upload_session);
		(void)ma_datachannel_session_release(self->request_session);
		(void)ma_service_manager_release(self->service_manager);
		(void)ma_spipe_decorator_release(self->spipe_decorator);
		(void)ma_spipe_handler_release(self->spipe_handler);
		(void)ma_msgbus_subscriber_release(self->network_subscriber);
		free(self);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

void ma_datachannel_manager_notify_and_remove_expired_items(ma_datachannel_manager_t * self, ma_bool_t from_server, ma_bool_t b_notify)
{
	/*Remove any expired items*/
	ma_error_t rc = MA_OK;
	time_t current_time = time(NULL);
	ma_db_recordset_t *recordset = NULL;
	ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->service->context);

	if(MA_OK == (rc = ma_datachannel_database_get_expired_messages(self->service, current_time, from_server, &recordset))) {
		while(MA_OK == ma_db_recordset_next(recordset)) {
			ma_int64_t row_id = 0;
			if(MA_OK == (rc = ma_db_recordset_get_long(recordset, 1, &row_id))) {
				if(MA_OK == (rc = ma_datachannel_database_remove_message(self->service, b_notify, MA_ERROR_DATACHANNEL_ITEM_EXPIRED, row_id))) {
					MA_LOG(logger, MA_LOG_SEV_DEBUG, "Successfully removed Expired DataChannel item at %lld",row_id);
				}
			}
		}
		ma_db_recordset_release(recordset);
		recordset = NULL;
	}
}

static ma_error_t ma_datachannel_manager_handle_network_sensor_wakeup_cb(const char *topic, ma_message_t *message, void *cb_data) {
	ma_datachannel_manager_t *self = (ma_datachannel_manager_t *) cb_data;
	if(self && topic && message) {
		ma_error_t rc = MA_ERROR_INVALIDARG;
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->service->context);
		MA_LOG(logger, MA_LOG_SEV_DEBUG, "received sensor message in datachannel service.");
		if(!strncmp(topic, MA_NETWORK_SENSOR_CONNECTIVITY_STATUS_CHANGE_PUB_TOPIC_STR, strlen(MA_NETWORK_SENSOR_CONNECTIVITY_STATUS_CHANGE_PUB_TOPIC_STR))) {
			ma_variant_t *payload = NULL;
			ma_network_sensor_msg_t *nw_sensor_msg = NULL;

			ma_message_get_payload(message, &payload);
			convert_variant_to_network_sensor_msg(payload, &nw_sensor_msg);
			if(MA_OK != (rc = ma_network_sensor_msg_get_connectivity_status(nw_sensor_msg, &self->nw_conn_status))) {
				MA_LOG(logger, MA_LOG_SEV_ERROR, "error getting the network change status");
			}
			
			MA_LOG(logger, MA_LOG_SEV_TRACE, "Network status = %d", self->nw_conn_status);

			if(MA_NETWORK_CONNECTIVITY_STATUS_CONNECTED == self->nw_conn_status) {
				if(MA_DATACHANNEL_MANAGER_NOSESSION == self->upload_session->state) {
					ma_datachannel_manager_notify_and_remove_expired_items(self, MA_FALSE, MA_TRUE);
					ma_datachannel_manager_timer_check(self, MA_FALSE);
				}
				else
				MA_LOG(logger, MA_LOG_SEV_ERROR, "Session state = %d", self->upload_session->state);
			}
			ma_network_sensor_msg_release(nw_sensor_msg);
			ma_variant_release(payload);
		}
	}
	return MA_OK;
}

ma_error_t ma_datachannel_manager_start(ma_datachannel_manager_t *self) {
	if(self) {
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->service->context);
		uv_loop_t *uv_loop = NULL;
		ma_error_t rc = MA_ERROR_APIFAILED;
		ma_event_loop_t *event_loop = ma_msgbus_get_event_loop(MA_CONTEXT_GET_MSGBUS(self->service->context));
		if(MA_OK == (rc = ma_event_loop_get_loop_impl(event_loop, &uv_loop))) {
			ma_msgbus_subscriber_setopt(self->network_subscriber, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
			if(MA_OK == (rc = ma_msgbus_subscriber_register(self->network_subscriber, MA_NETWORK_SENSOR_CONNECTIVITY_STATUS_CHANGE_PUB_TOPIC_STR, ma_datachannel_manager_handle_network_sensor_wakeup_cb, self))) {
				self->b_retry_timer = self->b_routing_timer = self->b_upload_timer = MA_FALSE;
				uv_timer_init(uv_loop,&self->upload_timer);
				uv_timer_init(uv_loop,&self->routing_timer);
				uv_timer_init(uv_loop, &self->retry_timer);
				self->retry_timer.data = self;
				self->routing_timer.data = self;
				self->upload_timer.data = self;
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "uv timer data(%p)\n", self);
				self->retry_interval = EPO_DATACHANNEL_SPIPE_RETRY_TIMEOUT;
				self->nw_conn_status = MA_NETWORK_CONNECTIVITY_STATUS_CONNECTED;
				ma_datachannel_session_finish(self->upload_session);
			}
		}
		ma_datachannel_manager_timer_check(self,MA_TRUE);
		MA_LOG(logger, MA_LOG_SEV_DEBUG, "Started DataChannel manager successfully");
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_datachannel_manager_stop(ma_datachannel_manager_t *self) {
	if(self) {
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->service->context);
		if(self->b_upload_timer) {
			uv_timer_stop(&self->upload_timer);
			self->b_upload_timer = MA_FALSE;
		}
		if(self->b_routing_timer) {
			uv_timer_stop(&self->routing_timer);
			self->b_routing_timer = MA_FALSE;
		}
		if(self->b_retry_timer) {
			uv_timer_stop(&self->retry_timer);
			self->b_retry_timer = MA_FALSE;
		}
		uv_close((uv_handle_t *)&self->upload_timer, NULL);
		uv_close((uv_handle_t *)&self->routing_timer, NULL);
		uv_close((uv_handle_t *)&self->retry_timer, NULL);
		self->nw_conn_status = MA_NETWORK_CONNECTIVITY_STATUS_DISCONNECTED;
		MA_LOG(logger, MA_LOG_SEV_DEBUG, "Stopped DataChannel manager successfully");
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_spipe_decorator_t *ma_datachannel_manager_get_spipe_decorator(ma_datachannel_manager_t *self) {
	if(self) {
		return self->spipe_decorator? self->spipe_decorator: NULL;
	}
	return NULL;
}

ma_spipe_handler_t *ma_datachannel_manager_get_spipe_handler(ma_datachannel_manager_t *self) {
	if(self) {
		return self->spipe_handler? self->spipe_handler: NULL;
	}
	return NULL;
}



static ma_error_t dc_spipe_decorate(ma_spipe_decorator_t *decorator, ma_spipe_priority_t priority, ma_spipe_package_t *spipe_pkg) {
	if(decorator && spipe_pkg) {
		const unsigned char *package_type = NULL;
		size_t size = 0;
		ma_logger_t *logger = NULL;
		ma_datachannel_manager_t *self = (ma_datachannel_manager_t *) decorator->data;
		ma_error_t rc = MA_OK;

		if(self && self->service) {
			logger = MA_CONTEXT_GET_LOGGER(self->service->context);
			ma_spipe_package_get_info(spipe_pkg, STR_PACKAGE_TYPE, &package_type, &size);
			if(0 == strcmp(STR_PKGTYPE_MSG_UPLOAD,(const char *)package_type)) {
				MA_LOG(logger, MA_LOG_SEV_INFO, "DataChannel Service decorating SPIPE package for : { %s }",STR_PKGTYPE_MSG_UPLOAD);
				if(MA_OK != (rc = ma_datachannel_manager_spipe_msg_upload_decorator(self->service, decorator, priority, spipe_pkg))) {
					MA_LOG(logger, MA_LOG_SEV_ERROR, "");
				}
			}
			else if ( 0 == strcmp(STR_PKGTYPE_MSG_REQUEST, (const char *) package_type)) {
				MA_LOG(logger, MA_LOG_SEV_INFO, "DataChannel Service decorating SPIPE package for : { %s }",STR_PKGTYPE_MSG_REQUEST);
				if(MA_OK != (rc = ma_datachannel_manager_spipe_msg_request_decorator(self->service, decorator, priority, spipe_pkg))) {
					MA_LOG(logger, MA_LOG_SEV_ERROR, "");
				}
			}
			else {
				MA_LOG(logger, MA_LOG_SEV_INFO, "DataChannel Service ignoring decoration of SPIPE package for : { %s }",package_type);
			}
			return MA_OK;
		}
	}
	return MA_ERROR_INVALIDARG;
}

static ma_error_t dc_get_spipe_priority(ma_spipe_decorator_t *decorator, ma_spipe_priority_t *priority) {
	if(priority) {
        *priority =  MA_SPIPE_PRIORITY_IMMEDIATE ;
		return MA_OK ;
	}
	return MA_ERROR_INVALIDARG;
}

static ma_error_t dc_decorator_destroy(ma_spipe_decorator_t *decorator) {
	if(decorator) {
		MA_ATOMIC_DECREMENT(decorator->ref_count);
		return MA_OK ;
	}
	return MA_ERROR_INVALIDARG;
}

static void send_ma_info_message(ma_datachannel_service_t *service, const char *info_event) {
	if(service && info_event) {
		ma_message_t *info_message = NULL ;
		ma_error_t rc = MA_OK ;

		if(MA_OK == (rc = ma_message_create(&info_message))) {
			ma_msgbus_endpoint_t *ep = NULL ;
					
			(void)ma_message_set_property(info_message, MA_PROP_KEY_REQUEST_TYPE_STR, MA_PROP_VALUE_AGENT_INFO_COLLECT) ;
			(void)ma_message_set_property(info_message, MA_INFO_MSG_KEY_EVENT_TYPE_STR, info_event) ;

			if(MA_OK == (rc = ma_msgbus_endpoint_create(MA_CONTEXT_GET_MSGBUS(service->context), MA_IO_SERVICE_NAME_STR, NULL, &ep))) {
				(void)ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_INPROC) ;
						
				if(MA_OK != (rc = ma_msgbus_send_and_forget(ep, info_message)))
					MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_ERROR, "Failed to send ma info message, rc = <%d>.", rc) ;
						
				(void)ma_msgbus_endpoint_release(ep) ;	ep = NULL ;
			}
			(void)ma_message_release(info_message) ; info_message = NULL ;
		}
	}
	return ;
}

static ma_error_t dc_spipe_handler(ma_spipe_handler_t *handler, const ma_spipe_response_t *spipe_response) {
	if(handler && spipe_response) {
		ma_datachannel_manager_t *self = (ma_datachannel_manager_t *) handler->data;
		ma_datachannel_service_t *service = self->service;
		ma_error_t rc = MA_OK;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(service->context);
		
		ma_datachannel_session_state_t upload_state = self->upload_session->state;
		ma_datachannel_session_state_t request_state = self->request_session->state;

		if(	spipe_response->net_service_error_code == MA_ERROR_NETWORK_REQUEST_SUCCEEDED &&
			(spipe_response->http_response_code == 200 || spipe_response->http_response_code == 202)) {
				
			/**/
			self->retry_interval = EPO_DATACHANNEL_SPIPE_RETRY_TIMEOUT;
			if(MA_DATACHANNEL_MANAGER_SESSION_DECORATED_MSGUPLOAD == upload_state) {
				MA_LOG(logger, MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "Package uploaded to ePO Server successfully");
				{
					char *p = NULL;
					size_t size;
					if(MA_OK == ma_datachannel_session_get_cookie(self->upload_session, &p, &size)) {
						ma_datachannel_notify_delivery_confirmation(service, p);
						free(p);
					}
				}
				
				ma_datachannel_session_finish(self->upload_session);
				(void)send_ma_info_message(service, MA_INFO_EVENT_DC_MSG_UPLOADED_STR) ;
			}
			else if(MA_DATACHANNEL_MANAGER_SESSION_DECORATED_MSGREQUEST == request_state) {
				MA_LOG(logger, MA_LOG_SEV_DEBUG, "MsgRequest SPIPE alert successfully completed");
				ma_datachannel_session_finish(self->request_session);
			}

			if(spipe_response->respond_spipe_pkg) {
				ma_spipe_package_t *package = spipe_response->respond_spipe_pkg;
				char *value = NULL;
				char *pkg_type_value = NULL;
				size_t val_size = 0;
				ma_spipe_package_get_info(package, STR_PKGTYPE_MSG_AVAILABLE, (unsigned char **) &value, &val_size);
				ma_spipe_package_get_info(package,  STR_PACKAGE_TYPE, (const unsigned char **)&pkg_type_value, &val_size) ;
				MA_LOG(logger, MA_LOG_SEV_DEBUG, "{ %s }", pkg_type_value);
				if(value) {
					if( 0 != strcmp(value, "0") ) {
						MA_LOG(logger, MA_LOG_SEV_DEBUG, "Found { %s } in { %s } package.", STR_PKGTYPE_MSG_AVAILABLE, pkg_type_value);
						rc = ma_datachannel_manager_spipe_msg_available_handler(service, handler, spipe_response);
					}
				}

				if(0 == strcmp(pkg_type_value, STR_PKGTYPE_MSG_AVAILABLE)) {
					MA_LOG(logger, MA_LOG_SEV_DEBUG, "DataChannel service is handling { %s } package", pkg_type_value);
					rc = ma_datachannel_manager_spipe_msg_available_handler(service, handler, spipe_response);
				}
				else if(0 == strcmp(pkg_type_value, STR_PKGTYPE_MSG_RESPONSE)) {
					MA_LOG(logger, MA_LOG_SEV_DEBUG, "DataChannel service is handling { %s } package", pkg_type_value);
					rc = ma_datachannel_manager_spipe_msg_response_handler(service, handler, spipe_response);
					(void)send_ma_info_message(service, MA_INFO_EVENT_DC_MSG_AVAILABLE_STR) ;
				}
				else {
					MA_LOG(logger, MA_LOG_SEV_DEBUG, "DataChannel service is ignoring { %s } package", pkg_type_value);
				}
			}
			/* Do the check for DataChannel messages here so that, if there was a message available in the package.
			 * MsgRequest could get scheduled before MsgUpload if any present in the DB
			 */
			ma_datachannel_manager_timer_check(self,MA_FALSE);
		}
		else {
			/*
			Goto retry mode if we had tried to upload DataChannel MsgUpload.
			*/
			
			if(MA_DATACHANNEL_MANAGER_SESSION_DECORATED_MSGUPLOAD == upload_state) {
				MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to upload DataChannel items - Network<%d>, HTTP Response<%d>", spipe_response->net_service_error_code, spipe_response->http_response_code);
				MA_LOG(logger, MA_LOG_SEV_ERROR | MA_LOG_FLAG_LOCALIZE, "Failed to upload package to ePO Server");
				{
					char *p = NULL;
					size_t size = 0;
					if(MA_OK == ma_datachannel_session_get_cookie(self->upload_session, &p, &size)) {
						ma_datachannel_database_remove_non_persistent_messages_from_ids(service, p);
						free(p);
					}
				}
				self->b_retry_msg_upload = MA_TRUE;
				ma_datachannel_session_finish(self->upload_session);
			}
			else if(MA_DATACHANNEL_MANAGER_SESSION_DECORATED_MSGREQUEST == request_state) {
				MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to check for new DataChannel items - Network <%d>, HTTP Response<%d>", spipe_response->net_service_error_code, spipe_response->http_response_code);
				self->b_retry_msg_request = MA_TRUE;
				ma_datachannel_session_finish(self->request_session);
			}
			
			if(!self->b_retry_timer) {
				uv_timer_start(&self->retry_timer, ma_datachannel_manager_retry_cb, self->retry_interval, 0);
				self->retry_interval = self->retry_interval * 2;
				if(self->retry_interval >= MA_DATACHANNEL_MAX_RETRY_TIMEOUT) 
					self->retry_interval = EPO_DATACHANNEL_SPIPE_RETRY_TIMEOUT;
				self->b_retry_timer = MA_TRUE;
			}
		}
	}
	return MA_ERROR_INVALIDARG;
}

static ma_error_t dc_spipe_handler_destroy(ma_spipe_handler_t *handler) {
	if(handler) {
		MA_ATOMIC_DECREMENT(handler->ref_count);
		return MA_OK ;
	}
	return MA_ERROR_INVALIDARG;
}

void ma_datachannel_manager_remove_expired_items(ma_datachannel_manager_t * self) {
	ma_error_t rc = MA_OK;
	time_t current_time = time(NULL);
	ma_db_recordset_t *recordset = NULL;
	ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->service->context);
	if(MA_OK == (rc = ma_datachannel_database_get_expired_messages(self->service, current_time, MA_FALSE, &recordset))) {
		while(MA_OK == ma_db_recordset_next(recordset)) {
			ma_int64_t row_id = 0;
			if(MA_OK == (rc = ma_db_recordset_get_long(recordset, 1, &row_id))) {
				if(MA_OK == (rc = ma_datachannel_database_remove_message(self->service, MA_FALSE, MA_OK, row_id))) {
					MA_LOG(logger, MA_LOG_SEV_DEBUG, "Successfully removed Expired DataChannel item at %lld",row_id);
				}
			}
		}
        (void)ma_db_recordset_release(recordset);
	}
}

ma_error_t ma_datachannel_manager_raise_spipe_alert(ma_datachannel_manager_t *self, const char *pkg_type) {
	if(self && pkg_type) {
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->service->context);
		ma_ah_client_service_t *ah_client = MA_CONTEXT_GET_AH_CLIENT(self->service->context);
		ma_configurator_t *configurator = MA_CONTEXT_GET_CONFIGURATOR(self->service->context);
		ma_ds_t *ds_handle = ma_configurator_get_datastore(configurator);
		ma_error_t rc = MA_ERROR_APIFAILED;
		ma_bool_t b_raise_alert = MA_FALSE;

		if(self->b_upload_timer) {
			self->b_upload_timer = MA_FALSE;
			uv_timer_stop(&self->upload_timer);
		}

		if(self->b_retry_timer) {
			if(0 == strcmp(pkg_type, STR_PKGTYPE_MSG_REQUEST))
				self->b_retry_msg_request = MA_TRUE;
			else if(0 == strcmp(pkg_type, STR_PKGTYPE_MSG_UPLOAD))
				self->b_retry_msg_upload = MA_TRUE;

			MA_LOG(logger, MA_LOG_SEV_DEBUG, "A retry session is already in progress. Will raise alert for %s when retrying", pkg_type);
			return MA_OK;
		}

		if(0 == strcmp(pkg_type, STR_PKGTYPE_MSG_UPLOAD)) {
			if(self->upload_session) {
				if(MA_DATACHANNEL_MANAGER_NOSESSION == self->upload_session->state) {
					ma_datachannel_session_begin(self->upload_session);
					b_raise_alert = MA_TRUE;
				}
				else {
					MA_LOG(logger, MA_LOG_SEV_DEBUG, "A { %s } SPIPE connection from DataChannel service is already in progress.", STR_PKGTYPE_MSG_UPLOAD);
				}
			}
		}
		else if(0 == strcmp(pkg_type, STR_PKGTYPE_MSG_REQUEST)) {
			if(self->request_session) {
				if(MA_DATACHANNEL_MANAGER_NOSESSION == self->request_session->state) {
					ma_datachannel_session_begin(self->request_session);
					b_raise_alert = MA_TRUE;
				}
				else {
					MA_LOG(logger, MA_LOG_SEV_DEBUG, "A { %s } SPIPE connection from DataChannel service is already in progress.", STR_PKGTYPE_MSG_REQUEST);
				}
			}
		}
		if(!self->service->b_is_running) {
			b_raise_alert = MA_FALSE;
			MA_LOG(logger, MA_LOG_SEV_WARNING, "Ignoring raise spipe alert because service is in shutdown state.");
		}
		if(b_raise_alert) {
			ma_bool_t is_first_asc = MA_FALSE;
			ma_buffer_t *buffer = NULL;
			if(MA_OK == (rc = ma_ds_get_str(ds_handle, MA_PROPERTY_DATA_PATH_NAME_STR, MA_PROPERTY_DATA_KEY_PROPERTY_VERSION_STR, &buffer))) {
				const char *prev_props_version = NULL ;
				size_t prev_props_version_size = 0 ;
				if(MA_OK == (rc = ma_buffer_get_string(buffer, &prev_props_version, &prev_props_version_size))) {
					if(0 == strcmp(prev_props_version, "0"))
						is_first_asc = MA_TRUE;
				}
				ma_buffer_release(buffer) ;
			}
			if(is_first_asc) {
				rc = MA_ERROR_NOT_YET_REGISTERED;
				MA_LOG(logger, MA_LOG_SEV_INFO, "A { %s } spipe alert could not be raise because the first ASC is not yet initiated.", pkg_type);
			}
			else
				rc = ma_ah_client_service_raise_spipe_alert(ah_client, pkg_type);

			if(MA_OK == rc) {
				MA_LOG(logger, MA_LOG_SEV_INFO, "A { %s } spipe alert has been raised successfully in ah_client.", pkg_type);
			}
			else if( MA_ERROR_AH_NO_NETWORK == rc) {
				MA_LOG(logger, MA_LOG_SEV_INFO, "A { %s } spipe alert could not be raised due to no network connectivity. rc - <%d>", pkg_type, rc);
				self->nw_conn_status = MA_NETWORK_CONNECTIVITY_STATUS_DISCONNECTED;
				if(0 == strcmp(pkg_type, STR_PKGTYPE_MSG_UPLOAD)) {
					self->b_retry_msg_upload = MA_TRUE;
					ma_datachannel_session_finish(self->upload_session);
				}
				else if(0 == strcmp(pkg_type, STR_PKGTYPE_MSG_REQUEST)) {
					self->b_retry_msg_request = MA_TRUE;
					ma_datachannel_session_finish(self->request_session);
				}
			}
			else {
				/*There is no network*/
				MA_LOG(logger, MA_LOG_SEV_DEBUG, "A { %s } spipe alert could not be raised. rc - <%d>", pkg_type, rc);
				if(0 == strcmp(pkg_type, STR_PKGTYPE_MSG_UPLOAD)) {
					self->b_retry_msg_upload = MA_TRUE;
					ma_datachannel_session_finish(self->upload_session);
				}
				else if(0 == strcmp(pkg_type, STR_PKGTYPE_MSG_REQUEST)) {
					self->b_retry_msg_request = MA_TRUE;
					ma_datachannel_session_finish(self->request_session);
				}

				if(!self->b_retry_timer) {
					uv_timer_start(&self->retry_timer, ma_datachannel_manager_retry_cb, self->retry_interval, 0);
					self->retry_interval = self->retry_interval * 2;
					if(self->retry_interval >= MA_DATACHANNEL_MAX_RETRY_TIMEOUT) 
						self->retry_interval = EPO_DATACHANNEL_SPIPE_RETRY_TIMEOUT;
					self->b_retry_timer = MA_TRUE;
				}
			}
		}
	}
	return MA_ERROR_INVALIDARG;
}

static void ma_datachannel_manager_retry_cb(uv_timer_t *timer_handle,int status) {
	if(timer_handle) {
		ma_datachannel_manager_t *self = (ma_datachannel_manager_t *)timer_handle->data;
		if(self) {
			if(self->b_retry_timer) {
				uv_timer_stop(&self->retry_timer);
				self->b_retry_timer = MA_FALSE;
			}
			if(self->b_retry_msg_request) {
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->service->context), MA_LOG_SEV_DEBUG, "Retrying to raise SPIPE { %s } package alert now.", STR_PKGTYPE_MSG_REQUEST);
				self->b_retry_msg_request = MA_FALSE;
				ma_datachannel_manager_raise_spipe_alert(self, STR_PKGTYPE_MSG_REQUEST);
			}
			if(self->b_retry_msg_upload) {
				ma_int64_t to_epo = 0;
				ma_int64_t from_epo = 0;
				self->b_retry_msg_upload = MA_FALSE;
				ma_datachannel_manager_notify_and_remove_expired_items(self, MA_FALSE, MA_TRUE);
				if(MA_OK == ma_datachannel_database_get_messages_count(self->service, &to_epo, &from_epo)) {
					if(to_epo > 0){
						MA_LOG(MA_CONTEXT_GET_LOGGER(self->service->context), MA_LOG_SEV_DEBUG, "Retrying to raise SPIPE { %s } package alert now.", STR_PKGTYPE_MSG_UPLOAD);
						ma_datachannel_manager_raise_spipe_alert(self, STR_PKGTYPE_MSG_UPLOAD);
					}
					else {
						MA_LOG(MA_CONTEXT_GET_LOGGER(self->service->context), MA_LOG_SEV_DEBUG, "No DataChannel message to upload.");
					}
				}
			}
		}
	}
}

ma_error_t ma_datachannel_manager_spipe_msg_upload_decorator(ma_datachannel_service_t *self, ma_spipe_decorator_t *decorator, ma_spipe_priority_t priority, ma_spipe_package_t *spipe_pkg) {
	if(self && decorator && spipe_pkg) {
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);
		ma_db_recordset_t *recordset = NULL;
		ma_ds_t *ds_handle = ma_configurator_get_datastore(MA_CONTEXT_GET_CONFIGURATOR(self->context));
		ma_error_t rc = MA_OK;
		ma_buffer_t *buffer = NULL;
		const char *p_agent_guid = NULL;
		char *agent_guid = NULL;
		/*Get the agent GUID*/
		if(MA_OK == ma_ds_get_str(ds_handle, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_GUID_STR, &buffer) ) {
			size_t size = 0;
			if(MA_OK == ma_buffer_get_string(buffer, &p_agent_guid, &size)) {
				agent_guid = strdup(p_agent_guid);
			}
			ma_buffer_release(buffer);
		}
		/*Create the batch to ePO.*/
		if(MA_OK == (rc = ma_datachannel_database_get_epo_upload_batch(self, agent_guid, &recordset))) {
			MA_MAP_T(ma_dc_spipe_counter) *map = NULL;
			if(MA_OK == MA_MAP_CREATE(ma_dc_spipe_counter, map)) {
				while(MA_OK == (rc = ma_db_recordset_next(recordset))) {
					ma_int64_t row_id = 0;
					if(MA_OK != (rc = ma_datachannel_utils_msg_upload_populate_spipe_package(self, agent_guid, recordset, map, spipe_pkg, &row_id))) {
						MA_LOG(logger, MA_LOG_SEV_ERROR, "An error occurred while adding an item to Spipe Package. Removing from DB");
						if(row_id)
							ma_datachannel_database_remove_message(self, MA_TRUE, rc, row_id);
					}
				}
				{
					ma_temp_buffer_t types = MA_TEMP_BUFFER_INIT;
					size_t types_len = 0;
					const unsigned char * p = NULL;
					ma_int64_t total_dc_count = 0;
					MA_MAP_FOREACH(ma_dc_spipe_counter, map, iter) {
						char items_count[MA_DATACHANNEL_MAX_ID_LEN] = {0};
						char temp_buf[MA_DATACHANNEL_MAX_ID_LEN] = {0};
						size_t len = strlen((const char *) ma_temp_buffer_get(&types));
						
						ma_temp_buffer_reserve(&types, len + strlen(iter.first) + 1);
						if(total_dc_count != 0)
							strncat((char*) ma_temp_buffer_get(&types), ",", strlen(","));
						strncat((char*) ma_temp_buffer_get(&types), iter.first, strlen(iter.first));
						

						MA_MSC_SELECT(_snprintf,snprintf) (temp_buf, MA_DATACHANNEL_MAX_ID_LEN, "%s.COUNT",iter.first);
						MA_MSC_SELECT(_snprintf,snprintf)((char *)items_count, MA_DATACHANNEL_MAX_ID_LEN, "%lld", iter.second);

						ma_spipe_package_add_info(spipe_pkg, temp_buf, (const unsigned char *)items_count, strlen(items_count));
						total_dc_count += iter.second;
						MA_LOG(logger, MA_LOG_SEV_TRACE, "%s.COUNT=%s", temp_buf, items_count);
					}
					p = (const unsigned char *) ma_temp_buffer_get(&types);
					types_len = strlen((const char *)p);
					ma_spipe_package_add_info(spipe_pkg, STR_PKGINFO_MSG_UPLOAD_NAMES, p, types_len);
					MA_LOG(logger, MA_LOG_SEV_TRACE, "Types= %s", p);
					MA_LOG(logger, MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "Sending the next batch of %ld data channel items", total_dc_count);
					ma_temp_buffer_uninit(&types);
				}
				MA_MAP_RELEASE(ma_dc_spipe_counter, map);
			}
			ma_db_recordset_release(recordset);
		}
		else {
			MA_LOG(logger, MA_LOG_SEV_CRITICAL, "Query execution failed");
		}
		if(agent_guid) {
			free(agent_guid);
			agent_guid = NULL;
		}
		self->datachannel_manager->upload_session->state = MA_DATACHANNEL_MANAGER_SESSION_DECORATED_MSGUPLOAD;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_datachannel_manager_spipe_msg_request_decorator(ma_datachannel_service_t *self, ma_spipe_decorator_t *decorator, ma_spipe_priority_t priority, ma_spipe_package_t *spipe_pkg) {
	if(self && decorator && spipe_pkg) {
		self->datachannel_manager->request_session->state = MA_DATACHANNEL_MANAGER_SESSION_DECORATED_MSGREQUEST;		
		/*No info or data to be added in the decorator. Just keeping this function for a future reference if any needs to be added*/
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_datachannel_manager_spipe_msg_available_handler(ma_datachannel_service_t *self, ma_spipe_handler_t *handler, const ma_spipe_response_t *spipe_response) {
	if(self && handler && spipe_response) {
		ma_datachannel_manager_raise_spipe_alert(self->datachannel_manager, STR_PKGTYPE_MSG_REQUEST);
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_datachannel_manager_spipe_msg_response_handler(ma_datachannel_service_t *self, ma_spipe_handler_t *handler, const ma_spipe_response_t *spipe_response) {
	if(self && handler && spipe_response) {
		//ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);
		ma_error_t rc = MA_ERROR_DATACHANNEL_INTERNAL;
		if(spipe_response && spipe_response->respond_spipe_pkg) {
			ma_spipe_package_t *pkg = spipe_response->respond_spipe_pkg;
			const unsigned char *dc_types = NULL;
			size_t size = 0;
			if(MA_OK == ma_spipe_package_get_info(pkg, STR_PKGINFO_MSG_UPLOAD_NAMES, &dc_types, &size)) {
				char *message_id = NULL;
				message_id = strtok((char *) dc_types, ",");
				while (NULL != message_id) {
					const unsigned char *count_str = 0;
					size_t size = 0;
					ma_int64_t messages_count = 0;
					char temp_buf[MA_DATACHANNEL_MAX_ID_LEN] = {0};
					MA_MSC_SELECT(_snprintf, snprintf)(temp_buf, MA_DATACHANNEL_MAX_ID_LEN, "%s.COUNT", message_id);
					if(MA_OK == ma_spipe_package_get_info(pkg, temp_buf, &count_str, &size)) {
						messages_count = atol((const char *)count_str);
						rc = ma_datachannel_utils_msg_response_process_message_id(self, pkg, message_id, messages_count);
					}
					message_id = strtok(NULL, ",");
				}

			}
		}
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

static void ma_datachannel_manager_timer_check(ma_datachannel_manager_t *self, ma_bool_t b_startup) {
	if(self) {
		ma_int64_t to_epo = 0;
		ma_int64_t from_epo = 0;
		ma_error_t rc = MA_OK;
		if(MA_OK == (rc = ma_datachannel_database_get_messages_count(self->service, &to_epo, &from_epo))) {
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->service->context), MA_LOG_SEV_DEBUG, "Status : to_epo < %lld > from_epo < %lld>", to_epo, from_epo);
			if(b_startup) {
					if(to_epo > 0 ) {
						self->upload_timer.data = self;
                        MA_LOG(MA_CONTEXT_GET_LOGGER(self->service->context), MA_LOG_SEV_DEBUG, "uv timer data(%p)\n", self);
						uv_timer_start(&self->upload_timer, ma_datachannel_manager_upload_timer_cb, EPO_DATACHANNEL_STARTUP_UPLOAD_TIMEOUT,0); 
						self->b_upload_timer = MA_TRUE;
					}
					if(from_epo > 0){
						if(from_epo > 0){
							if(!self->b_routing_timer) {
								self->routing_timer.data = self;
								self->b_routing_timer = MA_TRUE;
								uv_timer_start(&self->routing_timer, ma_datachannel_manager_routing_timer_cb, EPO_DATACHANNEL_MAINTANANCE_TIMEOUT, 0);
							}
						}
					}
			}
			else {
				if(!self->b_upload_timer) {
					if(to_epo > 0)
						ma_datachannel_manager_raise_spipe_alert(self, STR_PKGTYPE_MSG_UPLOAD);

					if(from_epo > 0){
						if(!self->b_routing_timer) {
							self->routing_timer.data = self;
							self->b_routing_timer = MA_TRUE;
							uv_timer_start(&self->routing_timer, ma_datachannel_manager_routing_timer_cb, EPO_DATACHANNEL_IMMEDIATE_ROUTING_TIMEOUT, 0);
						}
					}
				}
			}
		}
	}
}
typedef struct ma_dc_service_mgr_async_data_s {
	ma_datachannel_service_t *service;
	char *message_id;
}ma_dc_service_mgr_async_data_t;

static ma_error_t ma_service_manager_get_subscribes_cb(ma_error_t status, ma_service_manager_t *service_manager, const char *topic_filter, const ma_service_manager_subscriber_list_t *subscribers_list, size_t count, void *cb_data) {
	ma_error_t error = MA_OK;
	ma_db_recordset_t *recordset = NULL;
	ma_dc_service_mgr_async_data_t *async_data = (ma_dc_service_mgr_async_data_t *) cb_data;

	if(async_data) {
		char *message_id = async_data->message_id;
		ma_datachannel_service_t *self = async_data->service;

		if(count > 0) {
			if(MA_OK == ma_datachannel_database_get_undelivered_messages_by_id(self, message_id, &recordset)) {
				while(MA_OK == (error = ma_db_recordset_next(recordset))) {
					ma_datachannel_utils_process_datachannel_item_routing(self, recordset);
				}

				ma_db_recordset_release(recordset);
			}
		}
		else {
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "No subscribers for %s to push the data.", message_id);
			if(MA_OK == ma_datachannel_database_get_undelivered_messages_by_id(self, message_id, &recordset)) {
				while(MA_OK == (error = ma_db_recordset_next(recordset))) {
					ma_int64_t row_id = 0;
					ma_int32_t flags = 0;
					(void)ma_db_recordset_get_long(recordset, 1, &row_id);
					(void)ma_db_recordset_get_int(recordset, 4, &flags);
					if(MA_DATACHANNEL_PERSIST != (flags & MA_DATACHANNEL_PERSIST)) {
						if(MA_OK == ma_datachannel_database_remove_message(self, MA_FALSE, MA_OK, row_id)) {
							MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Removing non-Queueable DataChannel item from ePO : { %d }",row_id);
						}
					}
				}
				ma_db_recordset_release(recordset);
			}
			ma_datachannel_manager_notify_and_remove_expired_items(self->datachannel_manager, MA_TRUE, MA_FALSE);
			ma_datachannel_manager_timer_check(self->datachannel_manager, MA_FALSE);
		}
		free(async_data->message_id);
		free(async_data);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

static void ma_datachannel_manager_upload_timer_cb(uv_timer_t *timer_handle, int status) {
	if(timer_handle) {
		ma_datachannel_manager_t * self = (ma_datachannel_manager_t *)timer_handle->data;
		uv_timer_stop(&self->upload_timer);
		self->b_upload_timer = MA_FALSE;
		ma_datachannel_manager_raise_spipe_alert(self, STR_PKGTYPE_MSG_UPLOAD);
	}
}

static void ma_datachannel_manager_routing_timer_cb(uv_timer_t *timer_handle, int status) {
	if(timer_handle) {
		ma_db_recordset_t *db_recordset = NULL;
		ma_datachannel_manager_t *self = (ma_datachannel_manager_t *)timer_handle->data;
		self->b_routing_timer = MA_FALSE;
		uv_timer_stop(&self->routing_timer);
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->service->context), MA_LOG_SEV_DEBUG, "Routing saved messages.");

		if(MA_OK == ma_datachannel_database_get_undelivered_messages(self->service, &db_recordset)){
			while(MA_OK == ma_db_recordset_next(db_recordset)) {
				const char *message_id = NULL;
				if(MA_OK == ma_db_recordset_get_string(db_recordset, 1, &message_id)) {
					ma_dc_service_mgr_async_data_t *async_data = (ma_dc_service_mgr_async_data_t *) calloc(1,sizeof(ma_dc_service_mgr_async_data_t));
					if(async_data) {
						async_data->service = self->service;
						async_data->message_id = strdup(message_id);
						ma_service_manager_get_subscribers_async(self->service_manager, message_id, ma_service_manager_get_subscribes_cb, async_data);
					}
				}
			}
			ma_db_recordset_release(db_recordset);
		}
	}
}

ma_error_t ma_datachannel_manager_handle_primary_subscription_request(ma_datachannel_manager_t *self, char *message_id) {
	if(self && message_id) {

		ma_dc_service_mgr_async_data_t *async_data = (ma_dc_service_mgr_async_data_t *) calloc(1,sizeof(ma_dc_service_mgr_async_data_t));
		if(async_data) {
			async_data->service = self->service;
			async_data->message_id = strdup(message_id);
			ma_service_manager_get_subscribers_async(self->service_manager, message_id, ma_service_manager_get_subscribes_cb, async_data);
		}
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

