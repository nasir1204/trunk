#include "ma_datachannel_service_internal.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma_datachannel_manager.h"
#include "ma_datachannel_handler.h"
#include "ma/internal/services/datachannel/ma_datachannel_service.h"
#include "ma/ma_message.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/ma_log.h"
#include "ma/datachannel/ma_datachannel_item.h"
#include "ma_datachannel_service_crypt_utils.h"
#include <stdio.h>

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME	"DataChannelHandler"

ma_error_t ma_datachannel_msgbus_server_handler_cb(ma_msgbus_server_t *server, ma_message_t *request_payload, void *cb_data, ma_msgbus_client_request_t *c_request) {
	if(server && request_payload && c_request) {
		ma_datachannel_service_t *self = (ma_datachannel_service_t *) cb_data;
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);

		ma_error_t rc = MA_OK;
		ma_error_t error = MA_OK;
		ma_datachannel_messages_type_t request_type = EPO_DATACHANNEL_MESSAGES_TYPE_NONE;
		ma_message_t *response = NULL;
		
		if(self->b_is_running) {
			MA_LOG(logger, MA_LOG_SEV_DEBUG, "Received a msgbus request in DataChannel service");

			/*Post the response whether the validation has succeeded or not*/
			if(MA_OK == (error = ma_message_create(&response))) {
				rc = ma_datachannel_utils_validate_msgbus_message(self, request_payload, response, &request_type);
				/*
				 *	DataChannel msgbus response message format
				 *  property - status 
				 *  property - reason
				 *  payload  - NULL
				 */
				ma_msgbus_server_client_request_post_result(c_request, rc, response);
				ma_message_release(response);
			}
			else 
				MA_LOG(logger, MA_LOG_SEV_CRITICAL, "Failed to create a response for incoming request - rc <%d>", error);
			if(MA_OK == rc) {
				switch(request_type) {
					case EPO_DATACHANNEL_MESSAGES_TYPE_MSGUPLOAD:
						{
							rc = ma_datachannel_handler_msgupload_handler(self, request_payload);
						}
						break;
					case EPO_DATACHANNEL_MESSAGES_TYPE_MSGAVAILABLE: 
						{
							rc = ma_datachannel_handler_msgavailable_handler(self, request_payload);
						}
						break;
					case EPO_DATACHANNEL_MESSAGES_TYPE_SUBSCRIPTION:
						{
							rc = ma_datachannel_handler_primary_subscriber_handler(self, request_payload);
						}
						break;
					case EPO_DATACHANNEL_MESSAGES_TYPE_COMPAT:
						{
							rc = ma_datachannel_handler_compat_requeue_handler(self, request_payload);
						}
						break;
					default:
						break;
				}
			}
			return MA_OK;
		}
		else {
			MA_LOG(logger, MA_LOG_SEV_WARNING, "Received a  msgbus request in DataChannel Service, but state is in shutdown. Ignoring request");
		}
	}
	return MA_ERROR_INVALIDARG;
}


ma_error_t ma_datachannel_handler_msgupload_handler(ma_datachannel_service_t *self, ma_message_t *request) {
	if(self && request) {
		ma_error_t rc = MA_ERROR_APIFAILED;
		ma_int64_t row_id = 0;
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);
		const char *origin = NULL;
		(void)ma_message_get_property(request, EPO_DATACHANNEL_ITEM_PROP_ORIGIN_STR, &origin);
		if(origin) {
			MA_LOG(logger, MA_LOG_SEV_INFO, "Received %s from client : %s", EPO_DATACHANNEL_MESSAGES_TYPE_MSGUPLOAD_STR, origin);
			rc = ma_datachannel_database_add_message(self, request, MA_FALSE, &row_id);
			if(MA_OK != rc) {
				/*Error condition. Should be a pass on most cases.*/
				const char *flags_str = NULL;
				MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to save received DataChannel message into database - rc <%d>", rc);
				(void)ma_message_get_property(request, EPO_DATACHANNEL_ITEM_PROP_FLAGS_STR, &flags_str);
				if(flags_str) {
					ma_int32_t flags = atol(flags_str);
					if(MA_DATACHANNEL_PURGED_NOTIFICATION == (flags & MA_DATACHANNEL_PURGED_NOTIFICATION)) {
						ma_datachannel_notify_unsaved_item_purge(self, MA_ERROR_DATACHANNEL_INTERNAL, request);
					}
				}
			}
			else {
				MA_LOG(logger, MA_LOG_SEV_DEBUG, "Saved DataChannel item to database with record id %lld", row_id);
				ma_datachannel_manager_raise_spipe_alert(self->datachannel_manager, STR_PKGTYPE_MSG_UPLOAD);
			}
			return MA_OK;
		}
		return MA_ERROR_DATACHANNEL_BAD_REQUEST;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_datachannel_handler_msgavailable_handler(ma_datachannel_service_t *self, ma_message_t *request) {
	if(self) {
		ma_datachannel_manager_raise_spipe_alert(self->datachannel_manager, STR_PKGTYPE_MSG_REQUEST);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_datachannel_handler_compat_requeue_handler(ma_datachannel_service_t *self, ma_message_t *request) {
	if(self) {
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);

		ma_error_t rc = MA_ERROR_DATACHANNEL_BAD_REQUEST;
		ma_int64_t row_id = 0;
		MA_LOG(logger, MA_LOG_SEV_DEBUG, "Re-Queuing message since it was not delivered to LPC product");
		rc = ma_datachannel_database_add_message(self, request, MA_TRUE, &row_id);
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_datachannel_handler_primary_subscriber_handler(ma_datachannel_service_t *self, ma_message_t *request) {
	if(self) {
		char *message_id = NULL;

		if(MA_OK == ma_message_get_property(request, EPO_DATACHANNEL_ITEM_PROP_MESSAGE_TYPE_STR, &message_id)) {
			if(message_id) {
				if(!self->datachannel_manager->b_routing_timer) {
					ma_datachannel_manager_handle_primary_subscription_request(self->datachannel_manager, message_id);
				}
				return MA_OK;
			}
		}
		return MA_ERROR_APIFAILED;
	}
	return MA_ERROR_INVALIDARG;
}
