#ifndef MA_DATACHANNEL_DB_QUERIES_H_INCLUDED
#define MA_DATACHANNEL_DB_QUERIES_H_INCLUDED

#include "ma/internal/utils/database/ma_db_recordset.h"
#include "ma/internal/utils/database/ma_db_statement.h"

#define MA_DATACHANNEL_DB_ADD_INSTANCE								"CREATE TABLE IF NOT EXISTS MA_DATACHANNEL_MESSAGES ("\
																		"AUTO_ID INTEGER, "\
																		"DIRECTION INTEGER, "\
																		"MESSAGE_ID TEXT, "\
																		"ORIGIN TEXT, "\
																		"CORRELATION_ID INTEGER, "\
																		"RELIABILITYFLAGS INTEGER, "\
																		"TIMECREATED INTEGER, "\
																		"TTL INTEGER, "\
																		"PAYLOAD_TYPE INTEGER, "\
																		"PAYLOAD BLOB, "\
																	"PRIMARY KEY(AUTO_ID) "\
																	"ON CONFLICT REPLACE)"


#define MA_DATACHANNEL_DB_REMOVE_INSTANCE							"DROP TABLE MA_DATACHANNEL_MESSAGES"


#define MA_DATACHANNEL_DB_ADD_MESSAGE_STR							"INSERT INTO MA_DATACHANNEL_MESSAGES( "\
																		"DIRECTION, MESSAGE_ID, "\
																		"ORIGIN, CORRELATION_ID, "\
																		"RELIABILITYFLAGS, TIMECREATED, "\
																		"TTL, PAYLOAD_TYPE, PAYLOAD) "\
																	"VALUES ("\
																		"?, ?, "\
																		"?, ?, "\
																		"?, ?, "\
																		"?, ?, ?)"


#define MA_DATACHANNEL_DB_GET_LAST_INSERTED_ROW_ID_STR				"SELECT LAST_INSERT_ROWID()"


#define MA_DATACHANNEL_DB_REMOVE_MESSAGE_STR						"DELETE FROM MA_DATACHANNEL_MESSAGES WHERE AUTO_ID=?"


#define MA_DATACHANNEL_DB_REMOVE_MESSAGES_STR						"DELETE FROM MA_DATACHANNEL_MESSAGES WHERE AUTO_ID IN (%s)"



#define MA_DATACHANNEL_DB_REMOVE_EXPIRED_MESSAGES_STR				"SELECT AUTO_ID, MESSAGE_ID, ORIGIN, CORRELATION_ID "\
																	"FROM MA_DATACHANNEL_MESSAGES "\
																	"WHERE ((? - TIMECREATED) - TTL) >  0 "\
																	"AND DIRECTION = ?"


#define MA_DATACHANNEL_DB_REMOVE_NON_PERSISTENT_MESSAGE_STR			"SELECT AUTO_ID "\
																	"FROM MA_DATACHANNEL_MESSAGES "\
																	"WHERE AUTO_ID IN (%s) AND "\
																	"DIRECTION = 0 AND "\
																	"(RELIABILITYFLAGS & ?) != ?"


#define MA_DATACHANNEL_DB_GET_EPO_UPLOAD_BATCH_STR					"SELECT AUTO_ID AS RECORD_ID, MESSAGE_ID, "\
																			"ORIGIN, CORRELATION_ID, RELIABILITYFLAGS,  "\
																			"TIMECREATED, TTL, PAYLOAD_TYPE, PAYLOAD AS DATA, DIRECTION AS TOSERVER "\
																	"FROM MA_DATACHANNEL_MESSAGES "\
																	"WHERE ("\
																		"SELECT SUM(LENGTH(PAYLOAD)) "\
																		"FROM MA_DATACHANNEL_MESSAGES "\
																		"WHERE TOSERVER = ? AND AUTO_ID <= RECORD_ID) <= ? "\
																	"ORDER BY MESSAGE_ID LIMIT ?"


#define MA_DATACHANNEL_DB_GET_SENT_ITEM_NOTIFICATION_DATA_STR		"SELECT MESSAGE_ID, ORIGIN, RELIABILITYFLAGS, CORRELATION_ID "\
																	"FROM MA_DATACHANNEL_MESSAGES "\
																	"WHERE AUTO_ID IN (%s) AND "\
																	"DIRECTION = 0 AND "\
																	"(RELIABILITYFLAGS & ?) = ?"

#define MA_DATACHANNEL_DB_GET_PURGE_NOTIFICATION_DATA				"SELECT MESSAGE_ID, ORIGIN, RELIABILITYFLAGS, CORRELATION_ID "\
																	"FROM MA_DATACHANNEL_MESSAGES "\
																	"WHERE AUTO_ID=? AND "\
																	"(RELIABILITYFLAGS & ?) = ?"

#define MA_DATACHANNEL_DB_GET_ITEMS_BY_ID							"SELECT AUTO_ID, MESSAGE_ID, ORIGIN, RELIABILITYFLAGS, "\
																		"CORRELATION_ID, TIMECREATED, TTL, PAYLOAD_TYPE, PAYLOAD "\
																	"FROM MA_DATACHANNEL_MESSAGES "\
																	"WHERE DIRECTION = ? "\
																	"AND MESSAGE_ID = ?"


#define MA_DATACHANNEL_DB_GET_MESSAGES_COUNT_STR					"SELECT COUNT(*) , DIRECTION  "\
																	"FROM MA_DATACHANNEL_MESSAGES  "\
																	"GROUP BY DIRECTION"

#define MA_DATACHANNEL_DB_GET_UNDELIVERED_MESSAGES_STR				"SELECT DISTINCT MESSAGE_ID"\
																	" FROM MA_DATACHANNEL_MESSAGES"


#endif /*MA_DATACHANNEL_DB_QUERIES_H_INCLUDED*/
