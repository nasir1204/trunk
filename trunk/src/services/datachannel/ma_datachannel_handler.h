#ifndef MA_DATACHANNEL_HANDLER_H_
#define MA_DATACHANNEL_HANDLER_H_

#include "ma/ma_common.h"

MA_CPP(extern "C" {)

ma_error_t ma_datachannel_msgbus_server_handler_cb(ma_msgbus_server_t *server, ma_message_t *request_payload, void *cb_data, ma_msgbus_client_request_t *c_request);

ma_error_t ma_datachannel_handler_msgupload_handler(ma_datachannel_service_t *self, ma_message_t *request);

ma_error_t ma_datachannel_handler_msgavailable_handler(ma_datachannel_service_t *self, ma_message_t *request);

ma_error_t ma_datachannel_handler_compat_requeue_handler(ma_datachannel_service_t *self, ma_message_t *request);

ma_error_t ma_datachannel_handler_primary_subscriber_handler(ma_datachannel_service_t *self, ma_message_t *request);

MA_CPP(})

#endif /*MA_DATACHANNEL_HANDLER_H_*/

