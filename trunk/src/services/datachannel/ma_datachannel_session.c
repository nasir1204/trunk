#include "ma_datachannel_service_internal.h"
#include "ma_datachannel_session.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include "ma/internal/ma_macros.h"
#include <stdio.h>
#include "ma/internal/defs/ma_object_defs.h"
#define  LOG_FACILITY_NAME "DataChannel.Manager.Session"

ma_error_t ma_datachannel_session_create(struct ma_datachannel_manager_s *dc_manager, ma_datachannel_session_t **session) {
	if(dc_manager && session) {
		ma_datachannel_session_t *self = (ma_datachannel_session_t *) calloc(1,sizeof(ma_datachannel_session_t));
		if(self) {
			if(MA_OK == ma_bytestream_create(MA_LITTLE_ENDIAN, MA_DATACHANNEL_MAX_ID_LEN, &self->stream)) {
				self->dc_manager = dc_manager;
				self->state = MA_DATACHANNEL_MANAGER_NOSESSION;
				*session = self;
				return MA_OK;
			}
			ma_bytestream_release(self->stream);
		}
		return MA_ERROR_OUTOFMEMORY;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_datachannel_session_release(ma_datachannel_session_t *self) {
	if(self) {
		ma_bytestream_release(self->stream);
		free(self);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_datachannel_session_add_to_cookie(ma_datachannel_session_t *self, ma_uint64_t row_id) {
	if(self) {
		char buf[1024] = {0};
		size_t written =0;
		size_t size = ma_bytestream_get_size(self->stream);
		MA_MSC_SELECT(_snprintf, snprintf)(buf, 1024, size == 0?"%lld":",%lld", row_id);
		ma_bytestream_write_buffer(self->stream, (const unsigned char *) buf, strlen(buf), &written);
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->dc_manager->service->context), MA_LOG_SEV_DEBUG, "Tracking record { %d } for session", row_id );
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_datachannel_session_get_state(ma_datachannel_session_t *self, ma_datachannel_session_state_t *state) {
	if(self && state) {
		*state = self->state;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_datachannel_session_begin(ma_datachannel_session_t *self) {
	if(self) {
		self->state = MA_DATACHANNEL_MANAGER_SESSION_STARTED;
		ma_bytestream_seek(self->stream, 0);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_datachannel_session_finish(ma_datachannel_session_t *self) {
	if(self) {
		self->state = MA_DATACHANNEL_MANAGER_NOSESSION;
		ma_bytestream_release(self->stream);
		return ma_bytestream_create(MA_LITTLE_ENDIAN, MA_DATACHANNEL_MAX_ID_LEN, &self->stream);
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_datachannel_session_get_cookie(ma_datachannel_session_t *self, char **cookie, size_t *size) {
	if(self && cookie && size) {
		ma_error_t rc = MA_OK;
		ma_bytebuffer_t *buffer = ma_bytestream_get_buffer(self->stream);
		if(buffer) {
			char *p = (char *) ma_bytebuffer_get_bytes(buffer);
			*size = ma_bytebuffer_get_size(buffer);
			*cookie = (char *) calloc((*size) + 1, sizeof(char));
			if(*cookie) {
				memcpy(*cookie, p, *size);
				rc= MA_OK;
			}
			else
				rc = MA_ERROR_OUTOFMEMORY;
			ma_bytebuffer_release(buffer);
		}
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}
