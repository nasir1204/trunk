#ifndef MA_DATACHANNEL_SERVICE_INTERNAL_H_INCLUDED
#define MA_DATACHANNEL_SERVICE_INTERNAL_H_INCLUDED

#include "uv.h"
#include "ma/ma_common.h"
#include "ma/ma_log.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_service_manager.h"
#include "ma/internal/defs/ma_datachannel_defs.h"
#include "ma/internal/services/ma_service.h"
#include "ma/internal/services/ahclient/ma_spipe_decorator.h"
#include "ma/internal/services/ahclient/ma_spipe_handler.h"
#include "ma/internal/utils/datastructures/ma_bytebuffer.h"
#include "ma/internal/utils/datastructures/ma_bytestream.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include "ma/sensor/ma_sensor_utils.h"

MA_CPP(extern "C" {)

/*Datachannel service internal header. Contains definition of all the service related objects*/

typedef enum ma_datachannel_messages_type_e {
	EPO_DATACHANNEL_MESSAGES_TYPE_NONE = 0,
	EPO_DATACHANNEL_MESSAGES_TYPE_COMPAT,
	EPO_DATACHANNEL_MESSAGES_TYPE_SUBSCRIPTION,
	EPO_DATACHANNEL_MESSAGES_TYPE_MSGUPLOAD,
	EPO_DATACHANNEL_MESSAGES_TYPE_MSGAVAILABLE,
	EPO_DATACHANNEL_MESSAGES_TYPE_MSGRESPONSE,
	EPO_DATACHANNEL_MESSAGES_TYPE_NOTIFICATION
} ma_datachannel_messages_type_t;

typedef struct ma_datachannel_service_s ma_datachannel_service_t, *ma_datachannel_service_h;
typedef struct ma_datachannel_manager_s ma_datachannel_manager_t, *ma_datachannel_manager_h;
typedef struct ma_datachannel_session_s ma_datachannel_session_t, *ma_datachannel_session_h;

typedef enum ma_datachannel_session_state_e {
	MA_DATACHANNEL_MANAGER_NOSESSION,
	MA_DATACHANNEL_MANAGER_SESSION_STARTED,
	MA_DATACHANNEL_MANAGER_SESSION_DECORATED_MSGUPLOAD,
	MA_DATACHANNEL_MANAGER_SESSION_DECORATED_MSGREQUEST
}ma_datachannel_session_state_t;

struct ma_datachannel_session_s {
	ma_bytestream_t *stream;
	ma_datachannel_session_state_t state;
	ma_datachannel_manager_t *dc_manager;
};


struct ma_datachannel_manager_s {
	ma_datachannel_service_t				*service;
	ma_datachannel_session_t				*upload_session;
	ma_datachannel_session_t				*request_session;

	ma_msgbus_subscriber_t					*network_subscriber;

	ma_spipe_decorator_t					*spipe_decorator;
	ma_spipe_handler_t						*spipe_handler;
	
	ma_service_manager_t 					*service_manager;

	ma_network_connectivity_status_t		nw_conn_status;

	ma_bool_t								b_upload_timer;
	uv_timer_t								upload_timer;

	ma_bool_t								b_routing_timer;
	uv_timer_t								routing_timer;

	ma_bool_t								b_retry_timer;
	uv_timer_t								retry_timer;

	ma_bool_t								b_retry_msg_upload;
	ma_bool_t								b_retry_msg_request;

	ma_int32_t								retry_interval;
};


struct ma_datachannel_service_s {
	ma_service_t				base;
	ma_datachannel_manager_t	*datachannel_manager;
	ma_msgbus_server_t			*datachannel_message_listener;
    ma_context_t				*context;
	ma_int32_t					max_items;
	ma_bool_t					b_is_running;
};


#include "ma_datachannel_db_queries.h"
#include "ma_datachannel_service_utils.h"
#include "ma_datachannel_manager.h"
#include "ma_datachannel_database.h"
#include "ma_datachannel_session.h"
#include "ma_datachannel_service_crypt_utils.h"

MA_CPP(})
#endif

