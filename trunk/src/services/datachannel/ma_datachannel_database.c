#include "ma_datachannel_service_internal.h"
#include "ma/ma_message.h"
#include "ma_datachannel_db_queries.h"
#include "ma_datachannel_database.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/defs/ma_ah_defs.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/datachannel/ma_datachannel_item.h"
#include "ma_datachannel_service_crypt_utils.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/platform/ma_sys_apis.h"
#include <stdio.h>

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "DataChannel.Database"

ma_error_t ma_datachannel_database_add_instance(ma_datachannel_service_t *self) {
	if(self) {
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);
		ma_db_t *db_handle = ma_configurator_get_database(MA_CONTEXT_GET_CONFIGURATOR(self->context));
		ma_error_t rc = MA_ERROR_IO_INTERNAL;

		if(db_handle) {
			ma_db_statement_t *db_statement = NULL;
			if(MA_OK == (rc = ma_db_transaction_begin(db_handle))) {
				if(MA_OK == (rc = ma_db_statement_create(db_handle, MA_DATACHANNEL_DB_ADD_INSTANCE, &db_statement))) {
					if(MA_OK == (rc = ma_db_statement_execute(db_statement))) {
						ma_db_transaction_end(db_handle);
						rc = MA_OK;
						MA_LOG(logger, MA_LOG_SEV_DEBUG, "DataChannel  database instance added successfully.");
					}
					else {
						ma_db_transaction_cancel(db_handle);
						MA_LOG(logger, MA_LOG_SEV_ERROR, "DataChannel  database statement execution failed - rc <%d>.", rc);
					}
					ma_db_statement_release(db_statement);
				}
			}
		}
		else
			MA_LOG(logger, MA_LOG_SEV_CRITICAL, "Could not find database handle.");

		if(rc != MA_OK)
			MA_LOG(logger, MA_LOG_SEV_ERROR, "DataChannel  database add instance failed - rc <%d>", rc);
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_datachannel_database_remove_instance(ma_datachannel_service_t *self) {
	if(self) {
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);
		ma_db_t *db_handle = ma_configurator_get_database(MA_CONTEXT_GET_CONFIGURATOR(self->context));
		ma_error_t rc = MA_ERROR_IO_INTERNAL;

		if(db_handle) {
			ma_db_statement_t *db_statement = NULL;
			if(MA_OK == (rc = ma_db_transaction_begin(db_handle))) {
				if(MA_OK == (rc = ma_db_statement_create(db_handle, MA_DATACHANNEL_DB_REMOVE_INSTANCE, &db_statement))) {
					if(MA_OK == (rc = ma_db_statement_execute(db_statement))) {
						ma_db_transaction_end(db_handle);
						rc = MA_OK;
						MA_LOG(logger, MA_LOG_SEV_DEBUG, "DataChannel  database instance removed successfully.");
					}
					else {
						ma_db_transaction_cancel(db_handle);
						MA_LOG(logger, MA_LOG_SEV_ERROR, "DataChannel  database statement execution failed - rc <%d>.", rc);
					}
					ma_db_statement_release(db_statement);
				}
			}
		}
		else
			MA_LOG(logger, MA_LOG_SEV_CRITICAL, "Could not find database handle");

		if(rc != MA_OK)
			MA_LOG(logger, MA_LOG_SEV_ERROR, "DataChannel  database remove instance failed - rc <%d>", rc);
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_datachannel_database_add_message(ma_datachannel_service_t *self, ma_message_t *dc_message, ma_int32_t from_server, ma_int64_t *inserted_row_id) {
	if(self) {
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);
		ma_db_t *db_handle = ma_configurator_get_database(MA_CONTEXT_GET_CONFIGURATOR(self->context));
		ma_error_t rc = MA_ERROR_IO_INTERNAL;

		if(db_handle) {
			/* The dc_message param should be validate in the layer which is calling this API.
			 * This API assumes the message is properly formated and proceeds
			 */

			ma_variant_t *unencrypted_payload = NULL, *processed_payload = NULL;
			const char *message_id = NULL, *origin = NULL, *str_flags = NULL, *str_ttl = NULL, *str_cid = NULL, *str_tc = NULL;
			ma_int32_t flags = 0, ttl_in_seconds = 0;
			ma_int64_t correlation_id = 0;
			time_t time_created = time(NULL);

			(rc = ma_message_get_property(dc_message, EPO_DATACHANNEL_ITEM_PROP_MESSAGE_TYPE_STR, &message_id));
			(rc = ma_message_get_property(dc_message, EPO_DATACHANNEL_ITEM_PROP_ORIGIN_STR, &origin));
			(rc = ma_message_get_property(dc_message, EPO_DATACHANNEL_ITEM_PROP_FLAGS_STR, &str_flags)); 
			(rc = ma_message_get_property(dc_message, EPO_DATACHANNEL_ITEM_PROP_TTL_STR, &str_ttl));
			(rc = ma_message_get_property(dc_message, EPO_DATACHANNEL_ITEM_PROP_CORRELATION_ID_STR, &str_cid));
			(rc = ma_message_get_property(dc_message, EPO_DATACHANNEL_ITEM_PROP_TIMECREATED_STR, &str_tc));
			(rc = ma_message_get_payload(dc_message, &unencrypted_payload));


			if((message_id && *message_id) && (origin && *origin) && unencrypted_payload) {
				flags = str_flags? atoi(str_flags):0;
				ttl_in_seconds = str_ttl?ma_atoint64(str_ttl):0;
				correlation_id = str_cid ? ma_atoint64(str_cid) : 0;

				/*Perform the basic routine checks for each parameter and set it to default if found invalid*/

				if(0 >= ttl_in_seconds || MA_DATACHANNEL_MAX_TTL < ttl_in_seconds) {
					MA_LOG(logger, MA_LOG_SEV_DEBUG, "Resetting ttl to max TTL since TTL <%d> > MAX TTL<%d>", ttl_in_seconds, MA_DATACHANNEL_MAX_TTL);
					ttl_in_seconds = MA_DATACHANNEL_MAX_TTL;
				}

				if(str_tc) 
					time_created = ma_atoint64(str_tc);


				/*Check for encryption flags to encrypt the payload otherwise directly use it as processed_payload*/
				if(MA_OK == (rc = ma_datachannel_database_convert_payload_to_raw(self, unencrypted_payload, &processed_payload))) {
					ma_variant_release(unencrypted_payload);
					/*Swap the pointers for processing for encryption*/
					unencrypted_payload = processed_payload;
					processed_payload = NULL;
					if(MA_DATACHANNEL_ENCRYPT == (flags & MA_DATACHANNEL_ENCRYPT)) {
						if(MA_OK != (rc = ma_datachannel_service_encrypt_data(self, unencrypted_payload, &processed_payload))) {
							MA_LOG(logger, MA_LOG_SEV_CRITICAL, "Failed to encrypt data - rc <%d>", rc);
							ma_variant_release(unencrypted_payload);
							return rc;
						}
						else {
							MA_LOG(logger, MA_LOG_SEV_DEBUG, "The payload to be processed is encrypted securely.");
							ma_variant_release(unencrypted_payload);
						}
					}
					else {
						processed_payload = unencrypted_payload;
					}
				}
				else {
					ma_variant_release(unencrypted_payload);
					MA_LOG(logger, MA_LOG_SEV_ERROR, "The payload could not be converted to raw variant type - rc <%d>", rc);
					return rc;
				}
				{
					ma_vartype_t vartype = MA_VARTYPE_NULL;
					ma_db_statement_t *db_statement = NULL;
					if(processed_payload) {
						ma_variant_get_type(processed_payload, &vartype);
						if(MA_OK == (rc = ma_db_statement_create(db_handle, MA_DATACHANNEL_DB_ADD_MESSAGE_STR, &db_statement))) {
							if( MA_OK == (rc = ma_db_statement_set_int(db_statement, 1, from_server)) &&
							MA_OK == (rc = ma_db_statement_set_string(db_statement, 2, 1, message_id, strlen(message_id))) &&
							MA_OK == (rc = ma_db_statement_set_string(db_statement, 3, 1, origin, strlen(origin))) &&
							MA_OK == (rc = ma_db_statement_set_long(db_statement, 4, correlation_id)) &&
							MA_OK == (rc = ma_db_statement_set_int(db_statement, 5, flags)) &&
							MA_OK == (rc = ma_db_statement_set_long(db_statement, 6, time_created)) &&
							MA_OK == (rc = ma_db_statement_set_long(db_statement, 7, ttl_in_seconds)) &&
							MA_OK == (rc = ma_db_statement_set_int(db_statement, 8, vartype)) &&
							MA_OK == (rc = ma_db_statement_set_variant(db_statement, 9, processed_payload))) {
								if(MA_OK == (rc = ma_db_statement_execute(db_statement))) {
									ma_db_statement_t *stmt = NULL;
									ma_db_recordset_t *db_recordset = NULL;
									rc = MA_OK;
									if(MA_OK == (ma_db_statement_create(db_handle, MA_DATACHANNEL_DB_GET_LAST_INSERTED_ROW_ID_STR, &stmt))) {
										if(MA_OK == ma_db_statement_execute_query(stmt,&db_recordset)) {
											while(MA_OK == (ma_db_recordset_next(db_recordset))) {
												if(MA_OK == (ma_db_recordset_get_long(db_recordset, 1, inserted_row_id))){
													rc = MA_OK;
													break;
												}
											}
											ma_db_recordset_release(db_recordset);
										}
										else
											MA_LOG(logger, MA_LOG_SEV_ERROR, "failed to execute query to fetch last inserted record id - rc<%d>", rc);
										ma_db_statement_release(stmt);
									}
									else
										MA_LOG(logger, MA_LOG_SEV_ERROR, "failed to create statement - rc<%d>", rc);
								}
								else 
									MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to execute statement - rc <%d>",rc);
							}
							ma_db_statement_release(db_statement);
						}
						ma_variant_release(processed_payload);
					}
					else
						return MA_ERROR_DATACHANNEL_BAD_REQUEST;
				}
			}
			else {
				MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to add invalid format message into database.");
				rc = MA_ERROR_DATACHANNEL_BAD_REQUEST;
			}
		}
		else 
			MA_LOG(logger, MA_LOG_SEV_CRITICAL, "Could not find database handle");
		
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_datachannel_database_remove_message(ma_datachannel_service_t *self, ma_bool_t notify, ma_error_t reason, ma_uint64_t row_id)
{
	if(self) {
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);
		ma_db_t *db_handle = ma_configurator_get_database(MA_CONTEXT_GET_CONFIGURATOR(self->context));
		ma_error_t rc = MA_ERROR_IO_INTERNAL;

		if(db_handle) {
			ma_db_statement_t *db_statement = NULL;
			if(notify) {
				ma_db_recordset_t *db_recordset = NULL;
				if(MA_OK == ma_datachannel_database_get_message_notification_data(self, row_id, &db_recordset)) {
					while(MA_OK == ma_db_recordset_next(db_recordset)) {
						ma_datachannel_notify_purge(self, db_recordset, reason);
					}
					ma_db_recordset_release(db_recordset);
				}
			}
			if(MA_OK == (rc = ma_db_statement_create(db_handle, MA_DATACHANNEL_DB_REMOVE_MESSAGE_STR, &db_statement))) {
				if(MA_OK == (rc = ma_db_statement_set_long(db_statement, 1, row_id))) {
					if(MA_OK == (rc = ma_db_transaction_begin(db_handle))) {
						if(MA_OK == (rc = ma_db_statement_execute(db_statement))) {
							ma_db_transaction_end(db_handle);
							rc = MA_OK;
						}
						else {
							ma_db_transaction_cancel(db_handle);
						}
					}
				}
				ma_db_statement_release(db_statement);
			}
		}
		else 
			MA_LOG(logger, MA_LOG_SEV_CRITICAL, "Could not find database handle");
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_datachannel_database_remove_messages(ma_datachannel_service_t *self, char *csv_row_ids) {
	if(self) {
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);
		ma_db_t *db_handle = ma_configurator_get_database(MA_CONTEXT_GET_CONFIGURATOR(self->context));
		ma_error_t rc = MA_ERROR_IO_INTERNAL;
	
		if(db_handle) {
			ma_db_statement_t *db_statement = NULL;
			size_t size = strlen(MA_DATACHANNEL_DB_REMOVE_MESSAGES_STR) + strlen(csv_row_ids) + 1;
			char *buf = (char *) calloc(size, sizeof(char));
			if(buf) {
				MA_LOG(logger, MA_LOG_SEV_DEBUG, "Removing items {%s} from database",csv_row_ids);
				MA_MSC_SELECT(_snprintf, snprintf)(buf, size, MA_DATACHANNEL_DB_REMOVE_MESSAGES_STR, csv_row_ids);
				if(MA_OK == (rc = ma_db_statement_create(db_handle, buf, &db_statement))) {
					if(MA_OK == (rc = ma_db_transaction_begin(db_handle))) {
						if(MA_OK == (rc = ma_db_statement_execute(db_statement))) {
							MA_LOG(logger, MA_LOG_SEV_DEBUG, "Removed items {%s} from database",csv_row_ids);
							ma_db_transaction_end(db_handle);
							rc = MA_OK;
						}
						else {
							ma_db_transaction_cancel(db_handle);
						}
					}
					ma_db_statement_release(db_statement);
				}
				free(buf);
			}
		}
		else 
			MA_LOG(logger, MA_LOG_SEV_CRITICAL, "Could not find database handle");
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_datachannel_database_get_expired_messages(ma_datachannel_service_t *self, time_t current_time, ma_int32_t from_epo, ma_db_recordset_t **recordset)
{
	if(self && recordset) {
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);
		ma_db_t *db_handle = ma_configurator_get_database(MA_CONTEXT_GET_CONFIGURATOR(self->context));
		ma_error_t rc = MA_ERROR_IO_INTERNAL;
	
		if(db_handle) {
			ma_db_statement_t *db_statement = NULL;

			if(MA_OK == (rc = ma_db_statement_create(db_handle, MA_DATACHANNEL_DB_REMOVE_EXPIRED_MESSAGES_STR, &db_statement))) {
				if(MA_OK == (rc = ma_db_statement_set_long(db_statement,1, current_time))) {
					if(MA_OK == (rc = ma_db_statement_set_long(db_statement,2, from_epo))) {
						if(MA_OK == (rc = ma_db_statement_execute_query(db_statement, recordset))) {
								rc = MA_OK;
						}
					}
				}
				ma_db_statement_release(db_statement);
			}
		}
		else 
			MA_LOG(logger, MA_LOG_SEV_CRITICAL, "Could not find database handle");
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_datachannel_database_get_epo_upload_batch(ma_datachannel_service_t *self, const char *guid, ma_db_recordset_t **recordset) {
	if(self && recordset) {
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);
		ma_db_t *db_handle = ma_configurator_get_database(MA_CONTEXT_GET_CONFIGURATOR(self->context));
		ma_error_t rc = MA_ERROR_IO_INTERNAL;

		if(db_handle) {
			ma_db_statement_t *db_statement = NULL;

			if(MA_OK == (rc = ma_db_statement_create(db_handle, MA_DATACHANNEL_DB_GET_EPO_UPLOAD_BATCH_STR, &db_statement))) {
				if(MA_OK == (rc = ma_db_statement_set_int(db_statement, 1, 0))) {
					if(MA_OK == (rc = ma_db_statement_set_long(db_statement, 2, MA_MAX_SPIPE_SIZE))) {
						if(MA_OK == (rc = ma_db_statement_set_int(db_statement, 3, self->max_items?self->max_items:MA_DATACHANNEL_MAX_ITEMS))) {
							if(MA_OK == (rc = ma_db_statement_execute_query(db_statement, recordset))) {
								rc = MA_OK;
							}
						}
					}
				}
				ma_db_statement_release(db_statement);
			}
		}
		else 
			MA_LOG(logger, MA_LOG_SEV_CRITICAL, "Could not find database handle");
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_datachannel_database_get_sent_messages_notification_data(ma_datachannel_service_t *self, char *ids, ma_db_recordset_t **recordset) {
	if(self && ids && recordset) {
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);
		ma_db_t *db_handle = ma_configurator_get_database(MA_CONTEXT_GET_CONFIGURATOR(self->context));
		ma_error_t rc = MA_ERROR_IO_INTERNAL;

		if(db_handle) {
			ma_db_statement_t *db_statement = NULL;
			char *buf = NULL;
			size_t len = strlen(ids) + strlen(MA_DATACHANNEL_DB_GET_SENT_ITEM_NOTIFICATION_DATA_STR);
			buf = (char *) calloc(len + 1, sizeof(char));
			if(buf) {
				MA_MSC_SELECT(_snprintf, snprintf)(buf, len, MA_DATACHANNEL_DB_GET_SENT_ITEM_NOTIFICATION_DATA_STR, ids);
				if(MA_OK == (rc = ma_db_statement_create(db_handle, buf, &db_statement))) {
					if(MA_OK == (rc = ma_db_statement_set_int(db_statement,1, MA_DATACHANNEL_DELIVERY_NOTIFICATION))) {
						if(MA_OK == (rc = ma_db_statement_set_int(db_statement,2, MA_DATACHANNEL_DELIVERY_NOTIFICATION))) {
							if(MA_OK == (rc = ma_db_statement_execute_query(db_statement, recordset))) {
								rc = MA_OK;
							}
						}
					}
					ma_db_statement_release(db_statement);
				}
				free(buf);
			}
		}
		else 
			MA_LOG(logger, MA_LOG_SEV_CRITICAL, "Could not find database handle");
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_datachannel_database_get_undelivered_messages(ma_datachannel_service_t *self, ma_db_recordset_t **recordset) {
	if(self && recordset) {
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);
		ma_db_t *db_handle = ma_configurator_get_database(MA_CONTEXT_GET_CONFIGURATOR(self->context));
		ma_error_t rc = MA_ERROR_IO_INTERNAL;

		if(db_handle) {
			ma_db_statement_t *db_statement = NULL;

			if(MA_OK == (rc = ma_db_statement_create(db_handle, MA_DATACHANNEL_DB_GET_UNDELIVERED_MESSAGES_STR, &db_statement))) {
				if(MA_OK == (rc = ma_db_statement_execute_query(db_statement, recordset))) {
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Obtained the unique message id's to be routed");
					rc = MA_OK;
				}
				ma_db_statement_release(db_statement);
			}
		}
		else 
			MA_LOG(logger, MA_LOG_SEV_CRITICAL, "Could not find database handle");
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_datachannel_database_get_undelivered_messages_by_id(ma_datachannel_service_t *self, const char *message_id, ma_db_recordset_t **recordset) {
	if(self && message_id && recordset) {
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);
		ma_db_t *db_handle = ma_configurator_get_database(MA_CONTEXT_GET_CONFIGURATOR(self->context));
		ma_error_t rc = MA_ERROR_IO_INTERNAL;

		if(db_handle) {
			ma_db_statement_t *db_statement = NULL;
			MA_LOG(logger, MA_LOG_SEV_DEBUG, "Routing DataChannel items for ID : { %s }", message_id);
			if(MA_OK == (rc = ma_db_statement_create(db_handle, MA_DATACHANNEL_DB_GET_ITEMS_BY_ID, &db_statement))) {
				if(MA_OK == (rc = ma_db_statement_set_int(db_statement,1, 1))) {
					if(MA_OK == (rc = ma_db_statement_set_string(db_statement,2, 1, message_id, strlen(message_id)))) {
						if(MA_OK == (rc = ma_db_statement_execute_query(db_statement, recordset))) {
							MA_LOG(logger, MA_LOG_SEV_DEBUG, "Found items for {%s}", message_id);							
							rc = MA_OK;
						}
					}
				}
				ma_db_statement_release(db_statement);
			}
		}
		else 
			MA_LOG(logger, MA_LOG_SEV_CRITICAL, "Could not find database handle");
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_datachannel_database_get_messages_count(ma_datachannel_service_t *self, ma_int64_t *to_server_rows_count, ma_int64_t *from_server_rows_count) {
	if(self && to_server_rows_count && from_server_rows_count) {
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);
		ma_db_t *db_handle = ma_configurator_get_database(MA_CONTEXT_GET_CONFIGURATOR(self->context));
		ma_error_t rc = MA_ERROR_IO_INTERNAL;
		if(db_handle) {
			ma_db_statement_t *db_statement = NULL;
			ma_db_recordset_t *recordset = NULL;
			if(MA_OK == (rc = ma_db_statement_create(db_handle, MA_DATACHANNEL_DB_GET_MESSAGES_COUNT_STR, &db_statement))) {
				if(MA_OK == (rc = ma_db_statement_execute_query(db_statement, &recordset))) {
					ma_int32_t direction = 0;
					ma_int64_t count = 0;
					while(MA_OK == rc && MA_ERROR_NO_MORE_ITEMS != (rc = ma_db_recordset_next(recordset))) {
						if(MA_OK == (rc = ma_db_recordset_get_long(recordset, 1, &count))) {
							if(MA_OK == (rc = ma_db_recordset_get_int(recordset, 2, &direction))) {
								if(0 == direction) {
									*to_server_rows_count  = count;
								}
								else if ( 1 == direction ) {
									*from_server_rows_count = count;
								}
								else {
									rc = MA_ERROR_UNEXPECTED;
								}
							}
						}
					}
					ma_db_recordset_release(recordset);
				}
				ma_db_statement_release(db_statement);
			}
			if(rc == MA_ERROR_NO_MORE_ITEMS) 
				rc = MA_OK;
			return rc;
		}
		else 
			MA_LOG(logger, MA_LOG_SEV_CRITICAL, "Could not find database handle");
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_datachannel_database_get_message_notification_data(ma_datachannel_service_t *self, ma_int64_t row_id, ma_db_recordset_t **recordset) {
	if(self && recordset && (row_id > 0)) {
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);
		ma_db_t *db_handle = ma_configurator_get_database(MA_CONTEXT_GET_CONFIGURATOR(self->context));
		ma_error_t rc = MA_ERROR_IO_INTERNAL;

		if(db_handle) {
			ma_db_statement_t *db_statement = NULL;
			if(MA_OK == (rc = ma_db_statement_create(db_handle, MA_DATACHANNEL_DB_GET_PURGE_NOTIFICATION_DATA, &db_statement))) {
				if(MA_OK == (rc = ma_db_statement_set_long(db_statement,1, row_id))) {
					if(MA_OK == (rc = ma_db_statement_set_int(db_statement, 2, MA_DATACHANNEL_PURGED_NOTIFICATION))) {
						if(MA_OK == (rc = ma_db_statement_set_int(db_statement, 3, MA_DATACHANNEL_PURGED_NOTIFICATION))) {
							if(MA_OK == (rc = ma_db_statement_execute_query(db_statement, recordset))) {
								rc = MA_OK;
							}
						}
					}
				}
				ma_db_statement_release(db_statement);
			}
		}
		else 
			MA_LOG(logger, MA_LOG_SEV_CRITICAL, "Could not find database handle");
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_datachannel_database_remove_non_persistent_messages_from_ids(ma_datachannel_service_t *self, char *ids)
{
	if(self && ids && *ids) {
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);
		ma_db_t *db_handle = ma_configurator_get_database(MA_CONTEXT_GET_CONFIGURATOR(self->context));
		ma_error_t rc = MA_ERROR_IO_INTERNAL;
		ma_db_recordset_t *recordset = NULL;

		if(db_handle) {
			ma_db_statement_t *db_statement = NULL;
			char temp_statement[MA_MAX_BUFFER_LEN] = {0};
			MA_MSC_SELECT(_snprintf, snprintf)(temp_statement, MA_MAX_BUFFER_LEN, MA_DATACHANNEL_DB_REMOVE_NON_PERSISTENT_MESSAGE_STR, ids);
			if(MA_OK == (rc= ma_db_statement_create(db_handle, temp_statement, &db_statement))) {
				if(MA_OK == (rc = ma_db_statement_set_int(db_statement, 1, MA_DATACHANNEL_PERSIST))) {
					if(MA_OK == (rc = ma_db_statement_set_int(db_statement, 2, MA_DATACHANNEL_PERSIST))) {
						if(MA_OK == (rc = ma_db_statement_execute_query(db_statement, &recordset))) {
							while(MA_OK == ma_db_recordset_next(recordset)) {
								ma_int64_t row_id = 0;
								if(MA_OK == ma_db_recordset_get_long(recordset, 1, &row_id)) {
									rc = ma_datachannel_database_remove_message(self, MA_TRUE, MA_ERROR_DATACHANNEL_EPO_CONNECT_FAILED, row_id);
								}
							}
							rc = MA_OK;
							ma_db_recordset_release(recordset);
							recordset = NULL;
						}
					}
				}
				ma_db_statement_release(db_statement);
				db_statement = NULL;
			}
		}
		else 
			MA_LOG(logger, MA_LOG_SEV_CRITICAL, "Could not find database handle");
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}
