#include "ma_datachannel_service_internal.h"
#include "ma/ma_message.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/defs/ma_ah_defs.h"
#include "ma/internal/defs/ma_datachannel_defs.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/ma_log.h"
#include "ma/datachannel/ma_datachannel.h"
#include "ma/datachannel/ma_datachannel_item.h"
#include "ma_datachannel_service_crypt_utils.h"
#include "ma/internal/utils/datastructures/ma_bytestream.h"
#include "ma/internal/utils/filesystem/path.h"

#include <stdio.h>
#include <string.h>
#ifdef LOG_FACILITY_NAME 
#undef LOG_FACILITY_NAME 
#endif
#define LOG_FACILITY_NAME "datachannel_utils"


static void ma_datachannel_utils_set_msgbus_response_status(ma_message_t *response, ma_logger_t *logger, ma_error_t status, const char *reason) {
	if(response && reason) {
		char buf[MA_MAX_LEN] = {0};
		MA_MSC_SELECT(_snprintf, snprintf)(buf, MA_MAX_LEN, "%d", status);
		ma_message_set_property(response, EPO_DATACHANNEL_MESSAGE_BUS_PROP_STATUS,buf);
		ma_message_set_property(response, EPO_DATACHANNEL_MESSAGE_BUS_PROP_REASON,reason);
		MA_LOG(logger, MA_LOG_SEV_DEBUG, "Status : %s, Reason %s", buf, reason);
	}
}

ma_error_t ma_datachannel_utils_validate_msgbus_message(	
	ma_datachannel_service_t *self, 
	ma_message_t *request_payload, 
	ma_message_t *response,
	ma_datachannel_messages_type_t *request_type) {
	ma_error_t error = MA_OK;
	
	ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);
	if(self && request_payload && response && request_type) {
		const char *type_value = NULL;
		MA_LOG(logger, MA_LOG_SEV_DEBUG, "Validating DataChannel msgbus request format");
		if(MA_OK == (error = ma_message_get_property(request_payload, EPO_DATACHANNEL_MESSAGES_TYPE_STR, &type_value))) {
			MA_LOG(logger, MA_LOG_SEV_DEBUG, "Received message type : %s",type_value);
			if(type_value && *type_value) {
				ma_error_t rc = MA_ERROR_DATACHANNEL_BASE;
				if( 0 == strcmp(type_value, EPO_DATACHANNEL_MESSAGES_TYPE_MSGUPLOAD_STR)) {
					*request_type = EPO_DATACHANNEL_MESSAGES_TYPE_MSGUPLOAD;
					rc = validate_datachannel_msgbus_msgupload_message(self, request_payload, response);
				}
				else if (0 == strcmp(type_value, EPO_DATACHANNEL_MESSAGES_TYPE_SUBSCRIPTION_STR)) {
					*request_type = EPO_DATACHANNEL_MESSAGES_TYPE_SUBSCRIPTION;
					rc = validate_datachannel_msgbus_subscription_message(self, request_payload, response);
				}
				else if (0 == strcmp(type_value, EPO_DATACHANNEL_MESSAGES_TYPE_COMPAT_STR)) {
					*request_type = EPO_DATACHANNEL_MESSAGES_TYPE_COMPAT;
					rc = validate_datachannel_msgbus_compat_message(self, request_payload, response);
				}
				else if (0 == strcmp(type_value, EPO_DATACHANNEL_MESSAGES_TYPE_MSGAVAILABLE_STR)) {
					*request_type = EPO_DATACHANNEL_MESSAGES_TYPE_MSGAVAILABLE;
					MA_LOG(logger, MA_LOG_SEV_DEBUG, "");
					rc = MA_OK;
				}
				else {
					*request_type = EPO_DATACHANNEL_MESSAGES_TYPE_NONE;
					MA_LOG(logger, MA_LOG_SEV_DEBUG, "Unknown format type");
					rc = MA_ERROR_DATACHANNEL_BAD_REQUEST;
				}
				return rc;
			}
			else {
				ma_datachannel_utils_set_msgbus_response_status(response, logger, MA_ERROR_DATACHANNEL_BAD_REQUEST, "Invalid format. Missing or Empty format type.");
				return MA_ERROR_DATACHANNEL_BAD_REQUEST;
			}
		}
	}
	MA_LOG(logger, MA_LOG_SEV_ERROR, "Internal Service Error - rc <%d>", error);
	ma_datachannel_utils_set_msgbus_response_status(response, logger, MA_ERROR_MSGBUS_INTERNAL, "Internal service error");
	return MA_ERROR_INVALIDARG;
}

ma_error_t validate_datachannel_msgbus_msgupload_message(ma_datachannel_service_t *self, ma_message_t *request_payload, ma_message_t *response) {
	if(self && request_payload) {
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);
		ma_error_t rc = MA_OK;
		ma_variant_t *payload = NULL;
		ma_vartype_t var_type = MA_VARTYPE_NULL;
		const char *message_id = NULL;
		const char *origin = NULL;
		const char *flags_str = NULL;
		ma_int32_t flags = 0;
		size_t size = 0;

		(rc = ma_message_get_property(request_payload, EPO_DATACHANNEL_ITEM_PROP_MESSAGE_TYPE_STR, &message_id));
		(rc = ma_message_get_property(request_payload, EPO_DATACHANNEL_ITEM_PROP_ORIGIN_STR, &origin));
		(rc = ma_message_get_property(request_payload, EPO_DATACHANNEL_ITEM_PROP_FLAGS_STR, &flags_str));
		(rc = ma_message_get_payload(request_payload, &payload));

		rc = MA_ERROR_DATACHANNEL_BAD_REQUEST;

		if(payload) {
			(void)ma_variant_get_type(payload, &var_type);

			switch(var_type) {
			case MA_VARTYPE_STRING:
				{
					ma_buffer_t *buffer = NULL;
					const char *p = NULL;
					if(MA_OK == ma_variant_get_string_buffer(payload, &buffer)) {
						if(MA_OK == ma_buffer_get_string(buffer, &p, &size)) {
							MA_LOG(logger, MA_LOG_SEV_TRACE, "Received a string payload to be uploaded.");
						}
						ma_buffer_release(buffer);
					}
				}
				break;
			case MA_VARTYPE_RAW:
				{
					ma_buffer_t *buffer = NULL;
					const unsigned char *p = NULL;
					if(MA_OK == ma_variant_get_raw_buffer(payload, &buffer)) {
						if(MA_OK == ma_buffer_get_raw(buffer, &p, &size)) {
							MA_LOG(logger, MA_LOG_SEV_TRACE, "Received a raw payload to be uploaded.");
						}
						ma_buffer_release(buffer);
					}
				}
				break;
			default:
				ma_datachannel_utils_set_msgbus_response_status(response, logger, rc, payload?"no payload was attached to the message.":"the variant type was invalid for this request");
				break;
			}
			ma_variant_release(payload);
		}
		if(size > 0 ) {
			if(size <= MA_MAX_SPIPE_SIZE) {								
				if(message_id && *message_id) {
					if(origin && *origin) {
						if(flags_str) {
							flags = atoi(flags_str);
						}

						if( (MA_DATACHANNEL_PERSIST != (flags & MA_DATACHANNEL_PERSIST))  && (MA_NETWORK_CONNECTIVITY_STATUS_DISCONNECTED ==  self->datachannel_manager->nw_conn_status) ) {
							rc = MA_ERROR_DATACHANNEL_EPO_CONNECT_FAILED;
							ma_datachannel_utils_set_msgbus_response_status(response, logger, rc, "There is no network and the item is not to be persisted.");
						}
						else {
							rc = MA_OK;
							ma_datachannel_utils_set_msgbus_response_status(response, logger, rc, "Valid format.");
						}
						return rc;
					}
					else									
						ma_datachannel_utils_set_msgbus_response_status(response, logger, rc, "Empty or missing origin.");
				}
				else
					ma_datachannel_utils_set_msgbus_response_status(response, logger, rc, "Empty of missing messageType.");
			}
			else {
				rc = MA_ERROR_DATACHANNEL_MESSAGE_TOO_LARGE;
				ma_datachannel_utils_set_msgbus_response_status(response, logger, rc , "The size of the message being uploaded is larger than what SPIPE protocol can support.");
			}
		}
		else {
			rc = MA_ERROR_DATACHANNEL_BAD_REQUEST;
			ma_datachannel_utils_set_msgbus_response_status(response, logger, rc , "The size of the payload is zero. Cannot upload empty messages");
		}
		return rc;
	}
	
	ma_datachannel_utils_set_msgbus_response_status(response, NULL, MA_ERROR_MSGBUS_INTERNAL, "Internal service error.");
	return MA_ERROR_INVALIDARG;
}

ma_error_t validate_datachannel_msgbus_subscription_message(ma_datachannel_service_t *self, ma_message_t *request_payload, ma_message_t *response) {
	if(self && request_payload) {
		const char *item_id = NULL;
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);
		ma_error_t rc= MA_ERROR_DATACHANNEL_BAD_REQUEST;
		ma_message_get_property(request_payload, EPO_DATACHANNEL_ITEM_PROP_MESSAGE_TYPE_STR, &item_id);
		if( !( item_id && *item_id)) {
			ma_datachannel_utils_set_msgbus_response_status(response, logger, rc, "Empty of missing messageType.");
			return MA_ERROR_DATACHANNEL_BAD_REQUEST;
		}
		ma_datachannel_utils_set_msgbus_response_status(response, logger, MA_OK, "Valid primary subscription message format.");
		return MA_OK;
	}
	ma_datachannel_utils_set_msgbus_response_status(response, NULL, MA_ERROR_MSGBUS_INTERNAL, "Internal service error.");
	return MA_ERROR_INVALIDARG;
}

ma_error_t validate_datachannel_msgbus_compat_message(ma_datachannel_service_t *self, ma_message_t *request_payload, ma_message_t *response) {
	if(self && request_payload) {
		ma_error_t rc = MA_ERROR_DATACHANNEL_BAD_REQUEST;
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);
		const char *time_str = NULL;
		rc = validate_datachannel_msgbus_msgupload_message(self, request_payload, response);
		if(MA_OK == rc) {
			ma_message_get_property(request_payload, EPO_DATACHANNEL_ITEM_PROP_TIMECREATED_STR, &time_str);
			if(time_str && *time_str) {
				ma_datachannel_utils_set_msgbus_response_status(response, logger, MA_OK, "Valid re-queue message format.");
				return MA_OK;
			}
			else  {
				ma_datachannel_utils_set_msgbus_response_status(response, logger, rc, "Invalid format missing time stamp.");
				return rc;
			}
		}
		return rc;
	}
	ma_datachannel_utils_set_msgbus_response_status(response, NULL, MA_ERROR_MSGBUS_INTERNAL, "Internal service error.");
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_datachannel_database_convert_payload_to_raw(ma_datachannel_service_t *self, ma_variant_t *unencrypted_payload, ma_variant_t **processed_variant) {
	if(self && unencrypted_payload && processed_variant) {
		ma_error_t rc = MA_ERROR_APIFAILED;
		ma_variant_t *processed_payload = NULL;
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);
		ma_vartype_t vartype = MA_VARTYPE_NULL;

		ma_variant_get_type(unencrypted_payload, &vartype);
		if(MA_VARTYPE_STRING == vartype) {
			/*Convert this to raw payload*/
			const char *string = NULL;
			size_t size = 0;
			ma_buffer_t *buffer = NULL;

			if(MA_OK == (rc = ma_variant_get_string_buffer(unencrypted_payload, &buffer))) {
				if(MA_OK == (rc = ma_buffer_get_string(buffer, &string, &size))) {
					if(MA_OK == ma_variant_create_from_raw(string, size, &processed_payload)) {
						rc = MA_OK;
					}
				}
				ma_buffer_release(buffer);
			}

			if(MA_OK != rc) {
				MA_LOG(logger , MA_LOG_SEV_ERROR, "Failed to convert string to raw");
				return rc;
			}
		}
		else if(MA_VARTYPE_RAW == vartype) {
			processed_payload = unencrypted_payload;
			ma_variant_add_ref(unencrypted_payload);
			rc = MA_OK;
		}

		if(MA_OK == rc)
			*processed_variant = processed_payload;
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}


ma_error_t ma_datachannel_notify_delivery_confirmation(ma_datachannel_service_t *self, char *ids) {
	if(self && ids && *ids) {
		ma_db_recordset_t *recordset = NULL;
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);
		ma_error_t rc = MA_OK;
		MA_LOG(logger, MA_LOG_SEV_DEBUG,"Notifying sender for delivery confirmation : %s ", ids);
		if (MA_OK == ma_datachannel_database_get_sent_messages_notification_data(self, ids, &recordset)) {
			const char *message_id = NULL;
			const char *origin = NULL;
			ma_int32_t flags = 0;
			ma_int64_t correlation_id = 0;
			while(MA_OK == ma_db_recordset_next(recordset)) {
				if( MA_OK == ma_db_recordset_get_string(recordset, 1, &message_id) &&
					MA_OK == ma_db_recordset_get_string(recordset, 2, &origin) &&
					MA_OK == ma_db_recordset_get_int(recordset, 3, &flags) &&
					MA_OK == ma_db_recordset_get_long(recordset, 4, &correlation_id)) {
						ma_message_t *message = NULL;
						if(MA_OK == ma_message_create(&message)) {
							char temp_buf[MA_DATACHANNEL_MAX_ID_LEN] = {0};
							ma_message_set_property(message, EPO_DATACHANNEL_MESSAGES_TYPE_STR, EPO_DATACHANNEL_MESSAGES_TYPE_NOTIFICATION_STR);
							ma_message_set_property(message, EPO_DATACHANNEL_ITEM_PROP_MESSAGE_TYPE_STR, message_id);
							ma_message_set_property(message, EPO_DATACHANNEL_ITEM_PROP_ORIGIN_STR, origin);
							MA_MSC_SELECT(_snprintf, snprintf) (temp_buf, MA_DATACHANNEL_MAX_ID_LEN, "%d", flags);
							ma_message_set_property(message, EPO_DATACHANNEL_ITEM_PROP_FLAGS_STR, temp_buf);

							memset(temp_buf, 0, MA_DATACHANNEL_MAX_ID_LEN);
							MA_MSC_SELECT(_snprintf, snprintf) (temp_buf, MA_DATACHANNEL_MAX_ID_LEN, "%lld", correlation_id);
							ma_message_set_property(message, EPO_DATACHANNEL_ITEM_PROP_CORRELATION_ID_STR, temp_buf);

							memset(temp_buf, 0, MA_DATACHANNEL_MAX_ID_LEN);
							MA_MSC_SELECT(_snprintf, snprintf)(temp_buf, MA_DATACHANNEL_MAX_ID_LEN, "%d", MA_DC_NOTIFICATION_DELIVERED);
							ma_message_set_property(message, EPO_DATACHANNEL_ITEM_PROP_STATUS_STR, temp_buf);

							memset(temp_buf, 0, MA_DATACHANNEL_MAX_ID_LEN);
							MA_MSC_SELECT(_snprintf, snprintf)(temp_buf, MA_DATACHANNEL_MAX_ID_LEN, "%d", MA_OK);
							ma_message_set_property(message, EPO_DATACHANNEL_ITEM_PROP_REASON_STR, temp_buf);

							MA_LOG(logger, MA_LOG_SEV_DEBUG, "Notifying the sender {%s} that the DataChannel item { %s } with correlation id { %lld } is delivered to ePO.", origin, message_id, correlation_id);
							if(MA_OK != (rc = ma_msgbus_publish(MA_CONTEXT_GET_MSGBUS(self->context), EPO_DATACHANNEL_NOTIFICATION_PUBLISHER_TOPIC_STR, MSGBUS_CONSUMER_REACH_OUTPROC, message))) {
								MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to send notification.");
							}
							ma_message_release(message);
						}
				}
			}
			ma_db_recordset_release(recordset);
		}
		ma_datachannel_database_remove_messages(self, ids);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_datachannel_notify_purge(ma_datachannel_service_t *self, ma_db_recordset_t *recordset, ma_error_t reason) {
	if(self && recordset) {
		ma_error_t rc = MA_OK;
		const char *message_id = NULL;
		const char *origin = NULL;
		ma_int32_t flags = 0;
		ma_int64_t correlation_id = 0;
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);
		if( MA_OK == ma_db_recordset_get_string(recordset, 1, &message_id) &&
			MA_OK == ma_db_recordset_get_string(recordset, 2, &origin) &&
			MA_OK == ma_db_recordset_get_int(recordset, 3, &flags) &&
			MA_OK == ma_db_recordset_get_long(recordset, 4, &correlation_id)) {
			rc = MA_OK;
			if(MA_DATACHANNEL_PURGED_NOTIFICATION == (flags & MA_DATACHANNEL_PURGED_NOTIFICATION)) {
				ma_message_t *message = NULL;
				if(MA_OK == ma_message_create(&message)) {
					char temp_buf[MA_DATACHANNEL_MAX_ID_LEN] = {0};
					ma_message_set_property(message, EPO_DATACHANNEL_MESSAGES_TYPE_STR, EPO_DATACHANNEL_MESSAGES_TYPE_NOTIFICATION_STR);
					ma_message_set_property(message, EPO_DATACHANNEL_ITEM_PROP_MESSAGE_TYPE_STR, message_id);
					ma_message_set_property(message, EPO_DATACHANNEL_ITEM_PROP_ORIGIN_STR, origin);
					MA_MSC_SELECT(_snprintf, snprintf) (temp_buf, MA_DATACHANNEL_MAX_ID_LEN, "%d", flags);
					ma_message_set_property(message, EPO_DATACHANNEL_ITEM_PROP_FLAGS_STR, temp_buf);

					memset(temp_buf, 0, MA_DATACHANNEL_MAX_ID_LEN);
					MA_MSC_SELECT(_snprintf, snprintf) (temp_buf, MA_DATACHANNEL_MAX_ID_LEN, "%lld", correlation_id);
					ma_message_set_property(message, EPO_DATACHANNEL_ITEM_PROP_CORRELATION_ID_STR, temp_buf);

					memset(temp_buf, 0, MA_DATACHANNEL_MAX_ID_LEN);
					MA_MSC_SELECT(_snprintf, snprintf)(temp_buf, MA_DATACHANNEL_MAX_ID_LEN, "%d", MA_DC_NOTIFICATION_PURGED);
					ma_message_set_property(message, EPO_DATACHANNEL_ITEM_PROP_STATUS_STR, temp_buf);

					memset(temp_buf, 0, MA_DATACHANNEL_MAX_ID_LEN);
					MA_MSC_SELECT(_snprintf, snprintf)(temp_buf, MA_DATACHANNEL_MAX_ID_LEN, "%d", reason);
					ma_message_set_property(message, EPO_DATACHANNEL_ITEM_PROP_REASON_STR, temp_buf);

					MA_LOG(logger, MA_LOG_SEV_DEBUG, "Notifying the sender {%s} that the DataChannel item { %s } with correlation id { %lld } is purged.", origin, message_id, correlation_id);
					if(MA_OK != (rc = ma_msgbus_publish(MA_CONTEXT_GET_MSGBUS(self->context), EPO_DATACHANNEL_NOTIFICATION_PUBLISHER_TOPIC_STR, MSGBUS_CONSUMER_REACH_OUTPROC, message))) {
						MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to send notification.");
					}
					ma_message_release(message);
				}
			}
		}
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}
ma_error_t ma_datachannel_notify_unsaved_item_purge(ma_datachannel_service_t *self, ma_error_t rc, ma_message_t *unsaved_item) {
	ma_error_t error = MA_OK;
	if(self && unsaved_item) {
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);
		ma_message_t *response = NULL;
		if(MA_OK == (error = ma_message_create(&response))) {
			char temp_buf[MA_DATACHANNEL_MAX_ID_LEN] = {0};
			const char *correlation_id = NULL;
			const char *origin = NULL;
			const char *message_type = NULL;
			const char *flags = NULL;
			ma_message_get_property(unsaved_item, EPO_DATACHANNEL_ITEM_PROP_CORRELATION_ID_STR, &correlation_id);
			ma_message_get_property(unsaved_item, EPO_DATACHANNEL_ITEM_PROP_FLAGS_STR, &flags);
			ma_message_get_property(unsaved_item, EPO_DATACHANNEL_ITEM_PROP_MESSAGE_TYPE_STR, &message_type);
			ma_message_get_property(unsaved_item, EPO_DATACHANNEL_ITEM_PROP_ORIGIN_STR, &origin);
			/*Validate params - message_type & origin are mandatory. if they are not set it won't reach this code point*/
			correlation_id = correlation_id?correlation_id:"0";
			flags = flags?flags:"0";
			/*Start filling up the response for notification*/
			ma_message_set_property(response, EPO_DATACHANNEL_MESSAGES_TYPE_STR, EPO_DATACHANNEL_MESSAGES_TYPE_NOTIFICATION_STR);
			ma_message_set_property(response, EPO_DATACHANNEL_ITEM_PROP_MESSAGE_TYPE_STR, message_type);
			ma_message_set_property(response, EPO_DATACHANNEL_ITEM_PROP_ORIGIN_STR, origin);
			ma_message_set_property(response, EPO_DATACHANNEL_ITEM_PROP_FLAGS_STR, flags);
			ma_message_set_property(response, EPO_DATACHANNEL_ITEM_PROP_CORRELATION_ID_STR, correlation_id);

			MA_MSC_SELECT(_snprintf, snprintf)(temp_buf, MA_DATACHANNEL_MAX_ID_LEN, "%d", MA_DC_NOTIFICATION_PURGED);
			ma_message_set_property(response, EPO_DATACHANNEL_ITEM_PROP_STATUS_STR, temp_buf);

			memset(temp_buf, 0, MA_DATACHANNEL_MAX_ID_LEN);
			MA_MSC_SELECT(_snprintf, snprintf)(temp_buf, MA_DATACHANNEL_MAX_ID_LEN, "%d", rc);
			ma_message_set_property(response, EPO_DATACHANNEL_ITEM_PROP_REASON_STR, temp_buf);

			MA_LOG(logger, MA_LOG_SEV_DEBUG, "Notifying the sender { %s } that the DataChannel item { %s } with correlation id { %s } is purged.", origin, message_type, correlation_id);
			if(MA_OK != (error = ma_msgbus_publish(MA_CONTEXT_GET_MSGBUS(self->context), EPO_DATACHANNEL_NOTIFICATION_PUBLISHER_TOPIC_STR, MSGBUS_CONSUMER_REACH_OUTPROC, response))) {
				MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to send notification.");
			}
			ma_message_release(response);
		}
		return error;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_datachannel_utils_msg_upload_populate_spipe_package(ma_datachannel_service_t *self , const char *guid, ma_db_recordset_t *recordset, MA_MAP_T(ma_dc_spipe_counter) *map, ma_spipe_package_t *spike_pkg, ma_int64_t *row_id)
{
	if(self && recordset && guid && map && spike_pkg && row_id) {
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);
		ma_error_t rc = MA_OK;
		const char *message_id = NULL;
		const char *origin = NULL;
		ma_uint64_t correlation_id = 0;
		ma_uint32_t flags = 0;
		ma_uint64_t time_created = 0;
		ma_uint64_t ttl = 0;
		ma_variant_t *decrypted_payload = NULL;
		ma_variant_t *db_payload = NULL;
		ma_vartype_t var_type = MA_VARTYPE_NULL;

		if(	MA_OK == (rc = ma_db_recordset_get_long(recordset, 1, row_id)) &&
			MA_OK == (rc = ma_db_recordset_get_string(recordset, 2, &message_id)) && 
			MA_OK == (rc = ma_db_recordset_get_string(recordset, 3, &origin)) &&
			MA_OK == (rc = ma_db_recordset_get_long(recordset, 4, (ma_int64_t *)&correlation_id)) &&
			MA_OK == (rc = ma_db_recordset_get_int(recordset, 5, (ma_int32_t *)&flags)) &&
			MA_OK == (rc = ma_db_recordset_get_long(recordset, 6, (ma_int64_t *)&time_created)) &&
			MA_OK == (rc = ma_db_recordset_get_long(recordset, 7, (ma_int64_t *)&ttl)) &&
			MA_OK == (rc = ma_db_recordset_get_int(recordset, 8, (ma_int32_t *) &var_type)) &&
			MA_OK == (rc = ma_db_recordset_get_variant(recordset, 9, var_type, &db_payload))
			) {
			
			if(MA_DATACHANNEL_ENCRYPT == (flags & MA_DATACHANNEL_ENCRYPT)) {
				if(MA_OK == (rc = ma_datachannel_service_decrypt_data(self, db_payload, &decrypted_payload))) 
					ma_variant_release(db_payload);
				else {
					MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to decrypt data from database at %lld - rc<%d>. Removing the message", *row_id, rc);
					ma_variant_release(db_payload);
					return rc;
					/*TODO: Notify item to be purged and send notification.*/
				}
			}
			else {
				decrypted_payload = db_payload;
			}

			{
				ma_int64_t count = 0;
				if(MA_OK == MA_MAP_GET_VALUE(ma_dc_spipe_counter, map, message_id,count)) {
					count++;
					MA_MAP_ADD_ENTRY(ma_dc_spipe_counter, map, message_id, count);
				}
				else {
					count = 1;
					MA_MAP_ADD_ENTRY(ma_dc_spipe_counter, map, message_id, count);
				}
				{
					ma_bytestream_t *stream = NULL;
					ma_int16_t header_size = 20;
					ma_buffer_t *buffer = NULL;
					const unsigned char *payload_buffer = NULL;
					size_t size = 0;
					unsigned char message_id_size = (unsigned char ) (strlen(message_id) <=255?strlen(message_id):255);
					unsigned char origin_size = (unsigned char) (strlen(origin) <=255?strlen(origin):255);
					unsigned char guid_size = (unsigned char) (strlen(guid) <=255?strlen(guid):255);
					header_size += origin_size;
					header_size += message_id_size;
					header_size += guid_size;
					if(MA_OK == ma_variant_get_raw_buffer(decrypted_payload, &buffer)) {
						if(MA_OK == ma_buffer_get_raw(buffer, &payload_buffer, &size)) {
							if(MA_OK == ma_bytestream_create(MA_LITTLE_ENDIAN, header_size + size, &stream)) {
								ma_bytebuffer_t *spipe_data_buffer = NULL;
								ma_uint8_t flags_temp = 0;
								size_t size_written = 0;
								/*Header size(short)*/
								ma_bytestream_write_uint16(stream,header_size);
								/*Header Version(short)*/
								ma_bytestream_write_uint16(stream,1);
								/*TTL(long)*/
								ma_bytestream_write_uint32(stream, (ma_uint32_t) ttl);
								/*Correlation_ID(long)*/
								ma_bytestream_write_uint32(stream, (ma_uint32_t) correlation_id);
								/*Bit Flags (char)*/
								ma_bytestream_write_buffer(stream, (unsigned char *) &flags_temp, 1, &size_written);
								/*Origin Size(char)*/
								ma_bytestream_write_buffer(stream, &origin_size,1, &size_written);
								/*origin(string)*/
								ma_bytestream_write_buffer(stream, (unsigned char *) origin, origin_size, &size_written);
								/*Message_id size (char)*/
								ma_bytestream_write_buffer(stream, &message_id_size,1, &size_written);
								/*Message_id (string)*/
								ma_bytestream_write_buffer(stream, (unsigned char *) message_id, message_id_size, &size_written);
								/*guid size (char)*/
								ma_bytestream_write_buffer(stream, &guid_size,1, &size_written);
								/*guid (string)*/
								ma_bytestream_write_buffer(stream, (unsigned char *) guid, guid_size, &size_written);
								/*data size (long)*/
								ma_bytestream_write_uint32(stream, size);
								/*data*/
								ma_bytestream_write_buffer(stream, (unsigned char *) payload_buffer, size, &size_written);

								spipe_data_buffer = ma_bytestream_get_buffer(stream);
								if(spipe_data_buffer) {
									char temp_buf [MAX_PATH_LEN] = {0};
									MA_MSC_SELECT(_snprintf,snprintf)((char *)temp_buf, MAX_PATH_LEN, "%s.%012.12lld", message_id, count);
									if(MA_OK == ma_spipe_package_add_data(spike_pkg, temp_buf, ma_bytebuffer_get_bytes(spipe_data_buffer), ma_bytebuffer_get_size(spipe_data_buffer))) {
										ma_datachannel_session_add_to_cookie(self->datachannel_manager->upload_session, *row_id);
										MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Successfully added %s to spipe package.", temp_buf);
										rc = MA_OK;
									}
									else {
										MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to add item %s to package", temp_buf);
										rc = MA_ERROR_DATACHANNEL_INTERNAL;
									}
									ma_bytebuffer_release(spipe_data_buffer);
								}
								ma_bytestream_release(stream);
							}
						}
						ma_buffer_release(buffer);
					}
					else {
						MA_LOG(logger, MA_LOG_SEV_CRITICAL, "Failed to get the raw buffer from the database record. Deleting malicious record.");
						rc = MA_ERROR_DATACHANNEL_INTERNAL;
					}
				}
			}
			if(decrypted_payload)
				ma_variant_release(decrypted_payload);
		}
		else {
			MA_LOG(logger, MA_LOG_SEV_ERROR, "The recordset contains missing fields");
			if(db_payload) {
				ma_variant_release(db_payload);
				db_payload = NULL;
			}
			return MA_ERROR_INVALID_OPERATION;
		}
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}


ma_error_t ma_datachannel_utils_msg_response_process_message_id(ma_datachannel_service_t *self, ma_spipe_package_t *package, const char *message_id, ma_int64_t count) {
	if(self && package) {
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);
		if(count) {
			ma_int64_t i = 0;
			for(i = 0 ; i < count ; i++) {
				char temp_buf[MA_DATACHANNEL_MAX_ID_LEN] = {0};
				unsigned char *dc_blob = NULL;
				size_t dc_size = 0;
				MA_MSC_SELECT(_snprintf, snprintf)(temp_buf, MA_DATACHANNEL_MAX_ID_LEN, "%s.%lld", message_id, i+1);
				if(MA_OK == ma_spipe_package_get_data(package, temp_buf, (void **)&dc_blob, &dc_size)) {
					ma_bytestream_t *stream = NULL;
					MA_LOG(logger, MA_LOG_SEV_DEBUG, "DC ItemID: {%s}, BlobSize = {%d}", temp_buf, dc_size);
					if(MA_OK == ma_bytestream_create(MA_LITTLE_ENDIAN, dc_size, &stream)) {
						size_t written = 0;
						if(MA_OK == ma_bytestream_write_buffer(stream, dc_blob, dc_size, &written)) {
							if(MA_OK == ma_bytestream_seek(stream, 0)) {
								ma_uint16_t header_size = 0;
								ma_uint16_t header_version = 0;
								ma_int64_t ttl = 0;
								ma_uint32_t correlation_id = 0;
								size_t read = 0;
								char flags = 0;
								ma_uint32_t dc_body_len = 0; 
								char ttl_str[MA_DATACHANNEL_MAX_ID_LEN] = {0};
								char cid[MA_DATACHANNEL_MAX_ID_LEN] = {0};
								char flags_str[MA_DATACHANNEL_MAX_ID_LEN] = {0};
								char origin[MA_DATACHANNEL_MAX_ID_LEN] = {0};
								char item_id[MA_DATACHANNEL_MAX_ID_LEN] = {0};
								char agent_guid[MA_DATACHANNEL_MAX_ID_LEN] = {0};
								char *dc_blob_data = NULL;

								ma_bytestream_read_uint16(stream, &header_size);
								if(header_size > dc_size) /*Sanity check*/ {
                                    ma_bytestream_release(stream);
									continue;
								}
								
								ma_bytestream_read_uint16(stream, &header_version);
								if(header_version == 1) {
									{
										/*TTL*/
										ma_bytestream_read_uint32(stream, (ma_uint32_t *)&ttl);
										MA_MSC_SELECT(_snprintf, snprintf)(ttl_str, MA_DATACHANNEL_MAX_ID_LEN, "%lld", ttl);
									}
									{
										/*Correlation ID*/
										ma_bytestream_read_uint32(stream, (ma_uint32_t *) &correlation_id);
										MA_MSC_SELECT(_snprintf, snprintf)(cid, MA_DATACHANNEL_MAX_ID_LEN, "%d", correlation_id);
									}
									{
										/*Flags*/
										ma_int32_t b_flags = 0;
										char flags_temp = 0;
										ma_bytestream_read_buffer(stream, (unsigned char *) &flags, 1, &read);
										flags_temp = flags;
										b_flags |= ((flags_temp)		& (char) 1)?MA_DATACHANNEL_PERSIST:0;
										b_flags |= ((flags_temp >> 1)	& (char) 1)?MA_DATACHANNEL_PURGED_NOTIFICATION:0;
										b_flags |= ((flags_temp >> 2)	& (char) 1)?MA_DATACHANNEL_DELIVERY_NOTIFICATION:0;
										b_flags |= ((flags_temp >> 3)	& (char) 1)?MA_DATACHANNEL_ENCRYPT:0;
										MA_MSC_SELECT(_snprintf, snprintf) (flags_str, MA_DATACHANNEL_MAX_ID_LEN, "%d", b_flags);
									}
									/*Origin*/
									{
										char string_len = 0;
										ma_bytestream_read_buffer(stream, (unsigned char *) &string_len, 1, &read);
										if(string_len > 0)
											ma_bytestream_read_buffer(stream, (unsigned char *) origin, string_len, &read);
									}
									/*Item ID*/
									{
										char string_len = 0;
										ma_bytestream_read_buffer(stream, (unsigned char *) &string_len, 1, &read);
										if(string_len > 0)
											ma_bytestream_read_buffer(stream, (unsigned char *) item_id, string_len, &read);
									}
									/*GUID*/
									{
										char string_len = 0;
										ma_bytestream_read_buffer(stream, (unsigned char *) &string_len, 1, &read);
										if(string_len > 0)
											ma_bytestream_read_buffer(stream, (unsigned char *) agent_guid, string_len, &read);
									}
									{
										ma_bytestream_read_uint32(stream, &dc_body_len);
										MA_LOG(logger, MA_LOG_SEV_DEBUG, "Data Blob body size : %d",dc_body_len);
										if(dc_body_len) {
											dc_blob_data = (char *) calloc(dc_body_len, sizeof(char));
											if(dc_blob_data) {
												ma_bytestream_read_buffer(stream, (unsigned char *) dc_blob_data, dc_body_len, &read);
											}
											else {
												MA_LOG(logger, MA_LOG_SEV_CRITICAL, "Out of memory. rc = %d", MA_ERROR_OUTOFMEMORY);
												ma_bytestream_release(stream);
												return MA_ERROR_OUTOFMEMORY;
											}
										}
									}
								}
								/*TODO: Send the message to available subscribers*/
								if(dc_blob_data) {
									ma_error_t error = MA_OK;
									ma_message_t *message = NULL;
									if (MA_OK == (error = ma_message_create(&message))) {
										ma_int64_t time_created = time(NULL);
										char time_created_str[MA_DATACHANNEL_MAX_ID_LEN] = {0};
										ma_variant_t *payload = NULL;
										MA_MSC_SELECT(_snprintf, snprintf)(time_created_str,MA_DATACHANNEL_MAX_ID_LEN, "%lld",time_created);
										(void)ma_message_set_property(message, EPO_DATACHANNEL_ITEM_PROP_MESSAGE_TYPE_STR, item_id);
										(void)ma_message_set_property(message, EPO_DATACHANNEL_ITEM_PROP_FLAGS_STR, flags_str);
										(void)ma_message_set_property(message, EPO_DATACHANNEL_ITEM_PROP_ORIGIN_STR, origin);
										(void)ma_message_set_property(message, EPO_DATACHANNEL_ITEM_PROP_CORRELATION_ID_STR, cid);
										(void)ma_message_set_property(message, EPO_DATACHANNEL_ITEM_PROP_TIMECREATED_STR, time_created_str);
										(void)ma_message_set_property(message, EPO_DATACHANNEL_ITEM_PROP_TTL_STR, ttl_str);
										if(MA_OK == (error = ma_variant_create_from_raw(dc_blob_data, dc_body_len, &payload))) {
											/*Save to database*/
											ma_int64_t row_id = 0;
											ma_error_t rc = MA_OK;
											(void)ma_message_set_payload(message, payload);
											if(MA_OK == (rc = ma_datachannel_database_add_message(self, message, MA_TRUE, &row_id))) {
												MA_LOG(logger, MA_LOG_SEV_DEBUG, "Saved item {%s} with correlation Id {%s} from ePO at row - <%lld>", item_id, cid, row_id);
											}
											else {
												MA_LOG(logger, MA_LOG_SEV_CRITICAL, "Failed to save message from ePO to database - rc <%d>",rc);
											}
											ma_variant_release(payload);
										}
										else {
											MA_LOG(logger, MA_LOG_SEV_CRITICAL, "Failed to save DataChannel item from ePO - rc <%d> %d %s", error, dc_body_len, dc_blob_data);
										}
										ma_message_release(message);
									}
									else {
										MA_LOG(logger, MA_LOG_SEV_CRITICAL, "Failed to save DataChannel item from ePO - rc <%d>", error);
									}
								}
								else {
									MA_LOG(logger, MA_LOG_SEV_ERROR, "Received a DataChannel message with empty body.");
								}
								if(dc_blob_data)
									free(dc_blob_data);
							}
						}
						ma_bytestream_release(stream);
					}
				}
			}
		}
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}


ma_error_t ma_datachannel_utils_process_datachannel_item_routing(ma_datachannel_service_t *self, ma_db_recordset_t *db_recordset) {
	if(self && db_recordset) {
		ma_error_t rc =  MA_OK;
		const char *message_id = NULL;
		ma_int64_t row_id = 0;
		const char *origin = NULL;
		ma_int32_t flags = 0;
		ma_int64_t time_created = 0;
		ma_int64_t ttl = 0;
		ma_variant_t *decrypted_payload = NULL;
		ma_variant_t *encrypted_payload = NULL;
		ma_message_t *message = NULL;
		ma_int64_t correlation_id = 0;
		ma_vartype_t var_type = MA_VARTYPE_NULL;
		(void)ma_db_recordset_get_long(db_recordset, 1, &row_id);
		(void)ma_db_recordset_get_string(db_recordset, 2, &message_id);
		(void)ma_db_recordset_get_string(db_recordset, 3, &origin);
		(void)ma_db_recordset_get_int(db_recordset, 4, &flags);
		(void)ma_db_recordset_get_long(db_recordset, 5, &correlation_id);
		(void)ma_db_recordset_get_long(db_recordset, 6, &time_created);
		(void)ma_db_recordset_get_long(db_recordset, 7, &ttl);
		(void)ma_db_recordset_get_int(db_recordset, 8, (ma_int32_t *) &var_type);
		(void)ma_db_recordset_get_variant(db_recordset, 9, var_type, &encrypted_payload);

		if(MA_DATACHANNEL_ENCRYPT == (flags & MA_DATACHANNEL_ENCRYPT)) {
			if(MA_OK == (rc = ma_datachannel_service_decrypt_data(self, encrypted_payload, &decrypted_payload)))
				ma_variant_release(encrypted_payload);
			else {
				rc = MA_OK;
				ma_variant_release(encrypted_payload);
			}
		} else{
			decrypted_payload = encrypted_payload;
		}

		if(MA_OK == ma_message_create(&message)) {
			char buf[1024] = {0};
			ma_message_set_property(message, EPO_DATACHANNEL_MESSAGES_TYPE_STR, EPO_DATACHANNEL_MESSAGES_TYPE_MSGRESPONSE_STR);
			ma_message_set_property(message, EPO_DATACHANNEL_ITEM_PROP_ORIGIN_STR, origin);
			ma_message_set_property(message, EPO_DATACHANNEL_ITEM_PROP_MESSAGE_TYPE_STR, message_id);

			MA_MSC_SELECT(_snprintf, snprintf)(buf,1024, "%d",flags);
			ma_message_set_property(message, EPO_DATACHANNEL_ITEM_PROP_FLAGS_STR, buf);

			memset(buf,0,1024);
			MA_MSC_SELECT(_snprintf, snprintf)(buf,1024, "%lld",ttl);
			ma_message_set_property(message, EPO_DATACHANNEL_ITEM_PROP_TTL_STR, buf);

			memset(buf,0,1024);
			MA_MSC_SELECT(_snprintf, snprintf)(buf,1024, "%lld",time_created);
			ma_message_set_property(message, EPO_DATACHANNEL_ITEM_PROP_TIMECREATED_STR, buf);

			memset(buf,0,1024);
			MA_MSC_SELECT(_snprintf, snprintf)(buf,1024, "%lld",correlation_id);
			ma_message_set_property(message, EPO_DATACHANNEL_ITEM_PROP_CORRELATION_ID_STR, buf);

			ma_message_set_payload(message, decrypted_payload);


			if(MA_OK == ma_msgbus_publish(MA_CONTEXT_GET_MSGBUS(self->context), message_id, MSGBUS_CONSUMER_REACH_OUTPROC, message)) {
				if(MA_OK == ma_datachannel_database_remove_message(self, MA_FALSE, MA_OK, row_id)) {
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "delivered item {%s} with correlation id {%lld}", message_id, correlation_id);
				}
			}
			
			ma_message_release(message);
		}
		(void)ma_variant_release(decrypted_payload) ;	decrypted_payload = NULL ;
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}
