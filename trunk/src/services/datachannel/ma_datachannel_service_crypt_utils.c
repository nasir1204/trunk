#include "ma_datachannel_service_internal.h"

#include "ma/ma_buffer.h"
#include "ma/ma_client.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/utils/macrypto/ma_crypto.h"


#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "DataChannel.Crypto"

static ma_error_t encrypt_data(ma_datachannel_service_t *self, const unsigned char *data, size_t size, ma_variant_t **encrypted) {
	if(self && data && size && encrypted) {
		ma_error_t rc = MA_ERROR_APIFAILED;
		ma_crypto_t *crypto = NULL;
		ma_crypto_encryption_t *encryption_obj = NULL;
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);
		crypto = MA_CONTEXT_GET_CRYPTO(self->context);

		if(MA_OK == (rc = ma_crypto_encrypt_create(crypto, MA_CRYPTO_ENCRYPTION_3DES, &encryption_obj))) {
			ma_bytebuffer_t *buffer = NULL;
			if(MA_OK == ma_bytebuffer_create(size+1, &buffer)) {
				if(MA_OK == (rc = ma_crypto_encrypt(encryption_obj, data, size, buffer))) {
					size_t encrypted_buffer_size = ma_bytebuffer_get_size(buffer);
					unsigned char *encrpyted_buffer = ma_bytebuffer_get_bytes(buffer);
					if(MA_OK == (rc = ma_variant_create_from_raw(encrpyted_buffer, encrypted_buffer_size, encrypted))){
						rc = MA_OK;
					}else 
						MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to create encrypted raw variant - rc <%d>",rc);
				}
				else
					MA_LOG(logger, MA_LOG_SEV_CRITICAL, "Failed to encrypt data - rc <%d>",rc);

				(void)ma_bytebuffer_release(buffer);
			}
			(void)ma_crypto_encrypt_release(encryption_obj);
		}
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_datachannel_service_encrypt_data(ma_datachannel_service_t *self, ma_variant_t *plain_data, ma_variant_t **encrypted) {
	if(plain_data && encrypted) {
		ma_buffer_t *buffer = NULL;
		ma_vartype_t var_type = MA_VARTYPE_NULL;
		ma_logger_t *logger = NULL;
		ma_error_t rc = MA_ERROR_INVALID_OPTION;
		logger = MA_CONTEXT_GET_LOGGER(self->context);
		(void)ma_variant_get_type(plain_data, &var_type);
		switch(var_type) {
		case MA_VARTYPE_RAW:
			{
				const unsigned char *plain_buffer = NULL;
				size_t size = 0;
				if(MA_OK == (rc = ma_variant_get_raw_buffer(plain_data, &buffer))) {
					if(MA_OK == (rc = ma_buffer_get_raw(buffer, &plain_buffer, &size))) {
						rc = encrypt_data(self, plain_buffer, size, encrypted);
					}
					(void)ma_buffer_release(buffer);
				}
			}
			break;
		default:
			MA_LOG(logger, MA_LOG_SEV_DEBUG, "The variant type <%d> is not supported for encryption. convert to raw.", var_type);
			rc = MA_ERROR_NOT_SUPPORTED;
			break;
		}
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

static ma_error_t decrypt_data(ma_datachannel_service_t *self, const unsigned char *data, size_t size, ma_variant_t **decrypted) {
	if(self && data && size && decrypted) {
		ma_error_t rc = MA_OK;
		ma_crypto_t *crypto = NULL;
		ma_crypto_decryption_t *decryption_obj = NULL;
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);

		crypto = MA_CONTEXT_GET_CRYPTO(self->context);
		if(MA_OK == (rc = ma_crypto_decrypt_create(crypto, MA_CRYPTO_DECRYPTION_3DES, &decryption_obj))) {
			ma_bytebuffer_t *buffer = NULL;
			if(MA_OK == ma_bytebuffer_create(size+1, &buffer)) {
				if(MA_OK == (rc = ma_crypto_decrypt(decryption_obj, data, size, buffer))) {
					size_t decrypted_buffer_size = ma_bytebuffer_get_size(buffer);
					unsigned char *decrpyted_buffer = ma_bytebuffer_get_bytes(buffer);
					if(MA_OK == (rc = ma_variant_create_from_raw(decrpyted_buffer, decrypted_buffer_size, decrypted))){
						rc = MA_OK;
					}
					else
						MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to create variant - rc <%d>",rc);
				}
				else
					MA_LOG(logger, MA_LOG_SEV_CRITICAL, "Failed to decrypt data - rc <%d>",rc);
				(void)ma_bytebuffer_release(buffer);
			}
			(void)ma_crypto_decrypt_release(decryption_obj);
		}
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}
ma_error_t ma_datachannel_service_decrypt_data(ma_datachannel_service_t *self, ma_variant_t *encrypted_data, ma_variant_t **plain_data) {
	if(plain_data && encrypted_data) {
		ma_buffer_t *buffer = NULL;
		ma_vartype_t var_type = MA_VARTYPE_NULL;
		ma_error_t rc = MA_ERROR_INVALID_OPTION;
		(void)ma_variant_get_type(encrypted_data, &var_type);

		switch(var_type) {
		case MA_VARTYPE_RAW:
			{
				const unsigned char *plain_buffer = NULL;
				size_t size = 0;
				if(MA_OK == (rc = ma_variant_get_raw_buffer(encrypted_data, &buffer))) {
					if(MA_OK == (rc = ma_buffer_get_raw(buffer, &plain_buffer, &size))) {
						rc = decrypt_data(self, plain_buffer, size, plain_data);
					}
					(void)ma_buffer_release(buffer);
				}
			}
			break;
		default:
			break;
		}
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

