#include "ma_datachannel_service_internal.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma_datachannel_manager.h"
#include "ma/ma_message.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/services/ahclient/ma_ah_client_service.h"
#include "ma/internal/services/datachannel/ma_datachannel_service.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma_datachannel_handler.h"
#include "ma/internal/services/ma_policy_settings_bag.h"


#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#ifdef LOG_FACILITY_NAME
	#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME	"DataChannel.Service"


static ma_error_t ma_datachannel_service_configure(ma_service_t *service, ma_context_t *context, unsigned hint);
static ma_error_t ma_datachannel_service_start(ma_service_t *service);
static ma_error_t ma_datachannel_service_stop(ma_service_t *service);
static void ma_datachannel_service_release(ma_service_t *service);

static ma_service_methods_t base_methods = {
    &ma_datachannel_service_configure,
    &ma_datachannel_service_start,
    &ma_datachannel_service_stop,
    &ma_datachannel_service_release
};

ma_error_t ma_datachannel_service_create(char const *service_name, ma_service_t **service) {
    if(service) {
        ma_datachannel_service_t *self = (ma_datachannel_service_t *)calloc(1, sizeof(ma_datachannel_service_t));        
        if(self) {            
            ma_service_init((ma_service_t *)self, &base_methods, service_name, self);            
			self->max_items = MA_DATACHANNEL_MAX_ITEMS;
            *service = (ma_service_t *)self;
            return MA_OK;
        }
        return MA_ERROR_OUTOFMEMORY;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_datachannel_service_configure(ma_service_t *service, ma_context_t *context, unsigned hint) {
    ma_datachannel_service_t *self = (ma_datachannel_service_t *) service;    
    if(self && context) {
        ma_error_t rc = MA_OK;
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(context);
        if(MA_SERVICE_CONFIG_NEW == hint) {
            ma_msgbus_t *msgbus = MA_CONTEXT_GET_MSGBUS(context);
			ma_policy_settings_bag_t *policy_bag =  MA_CONTEXT_GET_POLICY_SETTINGS_BAG(context);
			self->context = context;
			if(MA_OK == (rc = ma_msgbus_server_create(msgbus, MA_DATACHANNEL_SERVICE_NAME_STR, &self->datachannel_message_listener))) {
				MA_LOG(logger, MA_LOG_SEV_DEBUG, "Created DataChannel message bus service successfully");
				(void)ma_msgbus_server_setopt(self->datachannel_message_listener, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
				(void)ma_msgbus_server_setopt(self->datachannel_message_listener, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
				/*create the DataChannel manager here*/
				ma_datachannel_database_add_instance(self);
				if(MA_OK == (rc = ma_datachannel_manager_create(self, &self->datachannel_manager))) {
					MA_LOG(logger, MA_LOG_SEV_DEBUG, "Max items to be uploaded to ePO: %d", self->max_items);
					return rc;
				}
				MA_LOG(logger, MA_LOG_SEV_CRITICAL, "Failed to create DataChannel manager - rc <%d>", rc);
				ma_msgbus_server_release(self->datachannel_message_listener);
			}

			if(policy_bag) {
				ma_policy_settings_bag_get_int(policy_bag, MA_DATACHANNEL_SERVICE_SECTION_NAME_STR, MA_DATACHANNEL_KEY_MAX_ITEMS, MA_FALSE,&self->max_items, (ma_int32_t)MA_DATACHANNEL_MAX_ITEMS);
			}
			else {
				self->max_items = MA_DATACHANNEL_MAX_ITEMS;
			}
        }
		else if(MA_SERVICE_CONFIG_MODIFIED == hint) {
			ma_policy_settings_bag_t *policy_bag =  MA_CONTEXT_GET_POLICY_SETTINGS_BAG(context);
			if(policy_bag) {
				ma_policy_settings_bag_get_int(policy_bag, MA_DATACHANNEL_SERVICE_SECTION_NAME_STR, MA_DATACHANNEL_KEY_MAX_ITEMS, MA_FALSE, &self->max_items, (ma_int32_t)MA_DATACHANNEL_MAX_ITEMS);
			}
			else {
				self->max_items = MA_DATACHANNEL_MAX_ITEMS;
			}
		}
		MA_LOG(logger, MA_LOG_SEV_DEBUG, "Max items to be uploaded to ePO: %d", self->max_items);
        return rc;
    }
    return MA_ERROR_OUTOFMEMORY;
}


static ma_error_t ma_datachannel_service_start(ma_service_t *service) {
    ma_datachannel_service_t *self = (ma_datachannel_service_t *)service;
	if(self) {
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);
		ma_ah_client_service_t *ah_client = MA_CONTEXT_GET_AH_CLIENT(self->context);
		ma_error_t rc = MA_OK;
		if(MA_OK == (rc = ma_msgbus_server_start(self->datachannel_message_listener, ma_datachannel_msgbus_server_handler_cb, self))) {
			if(MA_OK == (rc = ma_datachannel_manager_start(self->datachannel_manager))) {
				ma_spipe_decorator_t *decorator = ma_datachannel_manager_get_spipe_decorator(self->datachannel_manager);
				ma_spipe_handler_t *handler = ma_datachannel_manager_get_spipe_handler(self->datachannel_manager);
				if(MA_OK == (rc = ma_ah_client_service_register_spipe_decorator(ah_client, decorator))) {
					if(MA_OK == (rc = ma_ah_client_service_register_spipe_handler(ah_client, handler))) {
						MA_LOG(logger, MA_LOG_SEV_INFO,"Started DataChannel Service successfully");
						self->b_is_running = MA_TRUE;
						return rc;
					}
					else
						MA_LOG(logger, MA_LOG_SEV_CRITICAL, "Failed to register DataChannel spipe handler - rc <%d>", rc);
				}
				else
					MA_LOG(logger, MA_LOG_SEV_CRITICAL, "Failed to register DataChannel spipe decorator - rc <%d>", rc);
				ma_datachannel_manager_stop(self->datachannel_manager);
			}
			else
				MA_LOG(logger, MA_LOG_SEV_CRITICAL, "Failed to start DataChannel manager - rc <%d>", rc);
			ma_msgbus_server_stop(self->datachannel_message_listener);
		}
		else 
			MA_LOG(logger, MA_LOG_SEV_CRITICAL, "Failed to start DataChannel message bus service - rc <%d>", rc);
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_datachannel_service_stop(ma_service_t *service) {
    ma_datachannel_service_t *self = (ma_datachannel_service_t *)service;
	if(self) {
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);
		ma_ah_client_service_t *ah_client = MA_CONTEXT_GET_AH_CLIENT(self->context);
		ma_error_t rc = MA_OK;
		ma_spipe_decorator_t *decorator = ma_datachannel_manager_get_spipe_decorator(self->datachannel_manager);
		ma_spipe_handler_t *handler = ma_datachannel_manager_get_spipe_handler(self->datachannel_manager);
		if(MA_OK == (rc = ma_ah_client_service_unregister_spipe_decorator(ah_client, decorator))) {
			if(MA_OK == (rc = ma_ah_client_service_unregister_spipe_handler(ah_client, handler))) {
				if(MA_OK == (rc = ma_datachannel_manager_stop(self->datachannel_manager))) {
					if(MA_OK == (rc = ma_msgbus_server_stop(self->datachannel_message_listener))) {
						self->b_is_running = MA_FALSE;
						return MA_OK;
					}
					else
						MA_LOG(logger, MA_LOG_SEV_CRITICAL, "Failed to stop DataChannel message bus service - rc <%d>", rc);
					ma_datachannel_manager_start(self->datachannel_manager);
				}
				else 
					MA_LOG(logger, MA_LOG_SEV_CRITICAL, "Failed to stop DataChannel manager - rc <%d>", rc);
				ma_ah_client_service_register_spipe_handler(ah_client, handler);
			}
			else
				MA_LOG(logger, MA_LOG_SEV_CRITICAL, "Failed to unregister DataChannel spipe handler - rc <%d>", rc);
			ma_ah_client_service_register_spipe_decorator(ah_client, decorator);
		}
		else
			MA_LOG(logger, MA_LOG_SEV_CRITICAL, "Failed to unregister DataChannel spipe decorator - rc <%d>", rc);
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

static void ma_datachannel_service_release(ma_service_t *service) {
	if(service) {
		ma_error_t error = MA_ERROR_APIFAILED;
		ma_datachannel_service_t *self = (ma_datachannel_service_t *)service;
		error = ma_msgbus_server_release(self->datachannel_message_listener);
		error = ma_datachannel_manager_release(self->datachannel_manager);
	}
}

