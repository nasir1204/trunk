#include "ma/internal/services/event/ma_event_service.h"
#include "ma/internal/defs/ma_event_defs.h"
#include "ma/internal/defs/ma_object_defs.h"

#include "ma/ma_msgbus.h"
#include "ma/ma_message.h"
#include "ma/internal/services/ahclient/ma_ah_client_service.h"
#include "ma/internal/services/ma_policy_settings_bag.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/utils/platform/ma_system_property.h"
#include "ma/internal/utils/platform/ma_sys_apis.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/configurator/ma_configurator.h"

#include "ma_event_persist.h"
#include "ma_event_spipe_manager.h"
#include "ma_event_xml_generator.h"
#include "ma/internal/defs/ma_general_defs.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>

#ifdef MA_WINDOWS
#include <Windows.h>
#endif


#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "event"

MA_CPP(extern "C" {)
ma_error_t ma_event_persist_add_event_by_name(ma_event_persist_t *self, const char *event_xml_name, ma_event_priority_t priority, const unsigned char *event_xml_buffer, size_t size);
MA_CPP(})


typedef struct ma_event_service_s ma_event_service_t;

ma_error_t ma_event_service_configure(ma_event_service_t *service, ma_context_t *context, unsigned hint);
ma_error_t ma_event_service_start(ma_event_service_t *self);
ma_error_t ma_event_service_stop(ma_event_service_t *self);
ma_error_t ma_event_service_destroy(ma_event_service_t *self);

static struct ma_service_methods_s methods = {
    &ma_event_service_configure,
    &ma_event_service_start,
    &ma_event_service_stop,
    &ma_event_service_destroy
};

static ma_error_t event_message_received_cb(ma_msgbus_server_t *listener, ma_message_t *msg, void *cb_data, ma_msgbus_client_request_t *c_request);
static ma_error_t event_service_subscriber_cb(const char *topic, ma_message_t *payload, void *cb_data);
static ma_error_t read_legacy_event_from_disk(ma_event_service_t *service);

struct ma_event_service_s{
    /*base class*/
    ma_service_t				base; 	

    /*context objects.*/
    ma_context_t				*context;

    /*event datastore*/
    ma_event_persist_t			*event_ds;

    /*event spipe decorator/handler*/
    ma_event_spipe_manager_t	*event_spipe_mgr;	

    /*event message listener*/
    ma_msgbus_server_t			*event_msg_listener;

    /*is running.*/
    ma_bool_t                   is_running;
};

static MA_INLINE void event_service_deinit(ma_event_service_t *service){
    if(service){
        if(service->event_ds)
            ma_event_persist_release(service->event_ds);	
        service->event_ds = NULL;

        if(service->event_spipe_mgr)
            ma_event_spipe_manager_release(service->event_spipe_mgr);					
        service->event_spipe_mgr = NULL;

        if(service->event_msg_listener)
            ma_msgbus_server_release(service->event_msg_listener);					
        service->event_msg_listener = NULL;
        
        service->context = NULL;
    }
}

ma_error_t ma_event_service_create(const char *service_name, ma_service_t **event_service){
    if(service_name && event_service){
        ma_event_service_t *service = NULL;
        service = (ma_event_service_t*) calloc(1, sizeof(ma_event_service_t) );
        if(service){	
            ma_service_init((ma_service_t*)service, &methods, service_name, service);
            *event_service = (ma_service_t*)service;
            return MA_OK;
        }
        return MA_ERROR_OUTOFMEMORY;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_bool_t check_context_validity(ma_context_t *context){	
    /*Will see if we need this*/
    return MA_TRUE;
}

ma_error_t ma_event_service_configure(ma_event_service_t *self, ma_context_t *context, unsigned hint){
    if(self && context){
        if(MA_SERVICE_CONFIG_NEW == hint){
            if(check_context_validity(context)){
                ma_error_t rc = MA_OK;
                self->context = context;
                if ( MA_OK == ( rc = ma_event_persist_create(self->context, &self->event_ds) ) &&
                     MA_OK == ( rc = ma_event_spipe_manager_create(self->context, self->event_ds, &self->event_spipe_mgr) ) &&
                     MA_OK == ( rc = ma_msgbus_server_create(MA_CONTEXT_GET_MSGBUS(self->context), self->base.service_name, &self->event_msg_listener))){
                        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO, "Successfully configured the event service.");									
                }							
                else{
                    MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_ERROR, "Failed to create event service instance, last error = %d", rc);	
                    event_service_deinit(self);
                    return rc;
                }			
            }
        }

        if(self->context){
            /*retrive the agent policies and apply them.*/
            ma_priority_event_forwarding_policy_t policy = {MA_FALSE, 0, 0, MA_EVENT_SEVERITY_CRITICAL};
            ma_int32_t data = 0;

            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO, "Enforcing event service policy.");			
            if (MA_OK == ma_policy_settings_bag_get_int(MA_CONTEXT_GET_POLICY_SETTINGS_BAG(self->context), MA_EVENT_SERVICE_SECTION_NAME_STR, MA_EVENT_KEY_IS_ENABLED_PRIORITY_FORWARD_INT, &data) ){			
                policy.is_enabled = (data ? MA_TRUE : MA_FALSE);//#define SZ_TVD_SHARED_FRAMEWORK_A                 REG_6432_BASE_PATH_A "Network Associates\\TVD\\Shared Components\\Framework"
                if(policy.is_enabled){
                    /*Set policy to event service.*/
                    if (MA_OK == ma_policy_settings_bag_get_int(MA_CONTEXT_GET_POLICY_SETTINGS_BAG(self->context), MA_EVENT_SERVICE_SECTION_NAME_STR, MA_EVENT_KEY_UPLOAD_TIMEOUT_INT, &data) ){
                        policy.trigger_interval = data;
                        if (MA_OK == ma_policy_settings_bag_get_int(MA_CONTEXT_GET_POLICY_SETTINGS_BAG(self->context), MA_EVENT_SERVICE_SECTION_NAME_STR, MA_EVENT_KEY_UPLOAD_THRESHOLD_INT, &data) ){						
                            policy.event_batch_size = data;
                            if (MA_OK == ma_policy_settings_bag_get_int(MA_CONTEXT_GET_POLICY_SETTINGS_BAG(self->context), MA_EVENT_SERVICE_SECTION_NAME_STR, MA_EVENT_KEY_PRIORITY_LEVEL_INT, &data) ){
                                if(data <= MA_EVENT_SEVERITY_CRITICAL)
                                    policy.event_trigger_threshold = (ma_event_severity_t)data;
                            }
                        }
                    }
                }
#ifdef MA_WINDOWS
                {
                    HKEY hKey = NULL;

                    if (ERROR_SUCCESS == RegOpenKeyEx(HKEY_LOCAL_MACHINE, SZ_COMPAT_TVD_FRAMEWORK_REG_KEY, 0, KEY_READ|KEY_WRITE, &hKey)) {
                        char szBuffer[MAX_PATH] = {0};

                        MA_MSC_SELECT(_snprintf, snprintf)(szBuffer, MAX_PATH, "%d", policy.is_enabled);
                        (void)RegSetValueEx(hKey, SZ_COMPAT_AGENT_ENABLE_EVENT_TRIGGER, 0, REG_SZ, (const BYTE *)szBuffer, (DWORD)(strlen(szBuffer) + 1));
                        
                        MA_MSC_SELECT(_snprintf, snprintf)(szBuffer, MAX_PATH, "%d", policy.event_trigger_threshold ? policy.event_trigger_threshold : MA_EVENT_SEVERITY_CRITICAL);
                        (void)RegSetValueEx(hKey, SZ_COMPAT_AGENT_ENABLE_EVENT_TRIGGER_THRESHOLD, 0, REG_SZ, (const BYTE *)szBuffer, (DWORD)(strlen(szBuffer) + 1));
                        RegCloseKey(hKey);
                    }
                }
#endif
            }
            MA_LOG( MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Enforcing priority event send policy done (Policy:Enabled=<%d>:timeout=<%d>:batch=<%d>:priority threshold<%d>).", policy.is_enabled, policy.trigger_interval, policy.event_batch_size, policy.event_trigger_threshold);
            ma_event_spipe_manager_set_priority_event_forwarding_policy(self->event_spipe_mgr, &policy);
        }		
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_event_service_start(ma_event_service_t *self){
    if(self){
        if(self->context){
            ma_error_t rc = MA_OK;
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO, "Starting event service.");	
            /*start listener*/			
            ma_msgbus_server_setopt(self->event_msg_listener, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);	
            ma_msgbus_server_setopt(self->event_msg_listener, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);			
            if(MA_OK == (rc = ma_msgbus_server_start(self->event_msg_listener, event_message_received_cb, self))){
                ma_ah_client_service_register_spipe_decorator(MA_CONTEXT_GET_AH_CLIENT(self->context), ma_event_spipe_manager_get_spipe_decorator(self->event_spipe_mgr)); 
                ma_ah_client_service_register_spipe_handler(MA_CONTEXT_GET_AH_CLIENT(self->context), ma_event_spipe_manager_get_spipe_handler(self->event_spipe_mgr)); 
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO, "Successfully started event service.");	
            }else
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to starting event service.");					
            return rc;
        }
        else
            return MA_ERROR_PRECONDITION;		
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_event_service_stop(ma_event_service_t *self){
    if(self){
        ma_error_t rc = MA_OK;
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO, "Stopping event service.");	
        ma_ah_client_service_unregister_spipe_decorator(MA_CONTEXT_GET_AH_CLIENT(self->context), ma_event_spipe_manager_get_spipe_decorator(self->event_spipe_mgr)); 
        ma_ah_client_service_unregister_spipe_handler(MA_CONTEXT_GET_AH_CLIENT(self->context), ma_event_spipe_manager_get_spipe_handler(self->event_spipe_mgr)); 
        rc = ma_msgbus_server_stop(self->event_msg_listener);
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO, "Stopped event service.");	
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_event_service_destroy(ma_event_service_t *self){
    if(self){
        event_service_deinit(self);
        free(self);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t form_event_filter_message(ma_context_t *context, ma_bool_t is_publish, ma_event_persist_t *event_ds, ma_message_t **msg){	
    ma_error_t rc = MA_OK;
    MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_DEBUG, "Forming event filter message.");	
    if (MA_OK == (rc = ma_message_create(msg) ) ){
        /* Event filter infos*/
        char *filter_version = NULL;
        size_t filter_version_len = 10 ;
        ma_variant_t *disabled_events = NULL;	

        /* Get event filter infos*/		
        ma_event_persist_get_event_filter_version(event_ds, &filter_version, &filter_version_len);
        ma_event_persist_get_disabled_event_list(event_ds, &disabled_events);
                        
        /* If we got event filter infos, then set them in payload*/		
        if(filter_version && disabled_events){
            ma_table_t *payload_tbl = NULL;
            ma_variant_t *payload = NULL;
            ma_message_set_property(*msg, MA_EVENT_MSG_PROP_KEY_EVENT_FILTER_VERSION, filter_version);			
            if( MA_OK == ma_table_create(&payload_tbl) && 
                MA_OK == ma_variant_create_from_table(payload_tbl, &payload) ) {				
                ma_table_add_entry(payload_tbl, MA_EVENT_MSG_PAYLOAD_KEY_DISABLED_EVENTS, disabled_events);			
                ma_message_set_payload(*msg, payload);

            }
            ma_table_release(payload_tbl);
            ma_variant_release(payload);
        }			
        else{
            MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_WARNING, "Failed to get the event filter version or diabled events.");	
        }
        ma_variant_release(disabled_events);
    }
    return rc;
}
static ma_error_t event_message_handler_request_event_fiter(ma_event_service_t *self, ma_message_t *msg, ma_msgbus_client_request_t *c_request){
    ma_message_t *response = NULL;
    ma_error_t rc = MA_OK;
    MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Event filter request received.");	
    if (MA_OK == (rc = form_event_filter_message(self->context, MA_FALSE, self->event_ds, &response) ) ){				
        /* Send the response back.*/				
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Sending event filter message.");	
        ma_msgbus_server_client_request_post_result(c_request, rc, response ) ;	
        ma_message_release(response) ;	
    }
    return rc;
}

static ma_error_t form_event_xml_and_save(ma_event_service_t *self, ma_variant_t *event_payload){
    if(self && event_payload){
        ma_error_t rc = MA_OK;		
        ma_bytebuffer_t  *event_xml_buffer = NULL;
        ma_event_severity_t highest_severity = MA_EVENT_SEVERITY_INFORMATIONAL;
        if( MA_OK == (rc = ma_bytebuffer_create(1024, &event_xml_buffer) ) &&
            MA_OK == (rc = ma_event_xml_make(self->context, event_payload, event_xml_buffer, &highest_severity) ) ){
                ma_event_priority_t priority = MA_EVENT_PRIORITY_NORMAL;
                const ma_priority_event_forwarding_policy_t *event_policy = ma_event_spipe_manager_get_priority_event_forwarding_policy(self->event_spipe_mgr);
                if(event_policy && highest_severity >= event_policy->event_trigger_threshold)
                    priority = MA_EVENT_PRIORITY_IMMEDIATE;
                rc = ma_event_persist_add_event(self->event_ds, priority, ma_bytebuffer_get_bytes(event_xml_buffer), ma_bytebuffer_get_size(event_xml_buffer) );
        }
        else{
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to generate event xml, error = %d", rc);	
        }
        if(event_xml_buffer)
            ma_bytebuffer_release(event_xml_buffer);		
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t event_message_handler_event_recived(ma_event_service_t *self, ma_message_t *msg, ma_msgbus_client_request_t *c_request){
    ma_error_t rc = MA_OK;	
    ma_error_t status = MA_ERROR_EVENT_INVALID_EVENT_FORMAT;
    ma_message_t *response = NULL;
    ma_variant_t *payload = NULL;

    MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Received an event.");	
    ma_message_get_payload(msg, &payload);
    if(payload){
        if( MA_OK == (rc = form_event_xml_and_save(self,  payload) ) )
            status = MA_OK;					
        ma_variant_release(payload);
    }
    else{
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Event message doesn't contain payload, ignoring it.");	
    }
    if (MA_OK == (rc = ma_message_create(&response) ) ){
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Sending back the event message response.");			
        ma_msgbus_server_client_request_post_result(c_request, status, response ) ;	
        ma_message_release(response) ;
    }
    return rc;
}

ma_error_t read_legacy_event_from_disk(ma_event_service_t *service) {
    if(service) {
        ma_error_t err = MA_OK;
        const char *data_path = NULL;
        ma_configurator_t *configurator = MA_CONTEXT_GET_CONFIGURATOR(service->context);

        if((data_path = ma_configurator_get_agent_data_path(configurator))) {
            char path[MA_MAX_PATH] = {0};
            size_t size = 0;

            MA_MSC_SELECT(_snprintf, snprintf)(path, MA_MAX_PATH,"%s%c%s", data_path, MA_PATH_SEPARATOR, "AgentEvents");
            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "reading legacy events from disk path(%s)", path);
            (void)ma_filesystem_iterator_count(path, MA_FILESYSTEM_ITERATE_FILES, &size);
            if(size) {
                ma_filesystem_iterator_t *iterator = NULL;
                ma_temp_buffer_t buf = {0};

                if(MA_OK == (err = ma_filesystem_iterator_create(path, &iterator))) {
                    (void)ma_filesystem_iterator_set_mode(iterator, MA_FILESYSTEM_ITERATE_FILES);
                    while(MA_OK == ma_filesystem_iterator_get_next(iterator, &buf)) {
                        char file_path[MA_MAX_PATH] = {0};
                        FILE *fp = NULL;
                        struct stat st = {0};

                        MA_MSC_SELECT(_snprintf, snprintf)(file_path, MA_MAX_PATH,"%s%c%s", path, MA_PATH_SEPARATOR, (const char*)ma_temp_buffer_get(&buf));
                        (void)stat(file_path, &st);
                        if((fp = fopen(file_path, "r"))) {
                            char unsigned *data =NULL;
                            char event_file[MA_MAX_PATH] = {0};

                            if((data = (char unsigned*)calloc(st.st_size, sizeof(char)))) {
                                fread(data, 1, st.st_size, fp);
                                /* event file name starts with mc_xxx */
                                MA_MSC_SELECT(_snprintf, snprintf)(event_file, MA_MAX_PATH, "mc_%s", (const char*)ma_temp_buffer_get(&buf));
                                MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_ERROR, "event(%s) persisting...", event_file);
                                if(MA_OK != (err = ma_event_persist_add_event_by_name(service->event_ds, event_file, strstr(file_path, ".txml") ? MA_EVENT_PRIORITY_IMMEDIATE : MA_EVENT_PRIORITY_NORMAL, data, st.st_size))) {
                                    MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_ERROR, "event(%s) persisting failed, last error(%d)", event_file, err);
                                }
                                free(data); data = NULL;
                            } else {
                                err = MA_ERROR_OUTOFMEMORY;
                                MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_ERROR, "event(%s) reading from disk failed, last error(%d)", file_path, err);
                                fclose(fp);
                                break;
                            }
                            fclose(fp);
                            unlink(file_path);
                        }
                        ma_temp_buffer_uninit(&buf);
                    }
                    (void)ma_filesystem_iterator_release(iterator);                        
                } else
                    MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_ERROR, "reading legacy events from disk path(%s) failed, last error(%d)", path, err);
            } else
                MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "legacy event folder(%s) empty ", path);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t event_message_handler_send_event_to_epo(ma_event_service_t *self, ma_message_t *msg, ma_msgbus_client_request_t *c_request){
    ma_error_t rc = MA_OK;	
    ma_message_t *response = NULL;
    const char *priority = NULL;
    ma_event_priority_t ipriority = MA_EVENT_PRIORITY_NORMAL;

<<<<<<< .mine
	/*check the message type.*/
	ma_message_get_property(msg, MA_EVENT_MSG_PROP_KEY_EVENT_PRIORITY, &priority);	
	if(priority)
		ipriority = (ma_event_priority_t)(atoi(priority));
	
    /* reading legacy events from disk */
    (void)read_legacy_event_from_disk(self);

	MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "\"Send events to epo\", message received.");	
	ma_event_spipe_manager_raise_alert(self->event_spipe_mgr, ipriority);
=======
    /*check the message type.*/
    ma_message_get_property(msg, MA_EVENT_MSG_PROP_KEY_EVENT_PRIORITY, &priority);	
    if(priority)
        ipriority = (ma_event_priority_t)(atoi(priority));
    
    MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "\"Send events to epo\", message received.");	
    ma_event_spipe_manager_raise_alert(self->event_spipe_mgr, ipriority);
>>>>>>> .r6109

    /*Send response back.*/
    if (MA_OK == (rc = ma_message_create(&response) ) ){
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Sending the response back.");			
        ma_msgbus_server_client_request_post_result(c_request, MA_OK, response ) ;	
        ma_message_release(response) ;
    }
    return rc;
}

static ma_error_t event_message_handler_handle_unknown_message(ma_event_service_t *self, ma_message_t *msg, ma_msgbus_client_request_t *c_request){
    ma_message_t *response = NULL;
    ma_error_t rc = MA_OK;
    MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Unknown message received, ignoring it.");	
    if (MA_OK == (rc = ma_message_create(&response) ) ){		
        ma_msgbus_server_client_request_post_result(c_request, MA_ERROR_EVENT_MSG_UNKNOWN, response ) ;	
        ma_message_release(response) ;
    }
    return rc;
}

static ma_error_t event_message_received_cb(ma_msgbus_server_t *listener, ma_message_t *msg, void *cb_data, ma_msgbus_client_request_t *c_request) {	
    if(listener && msg && cb_data) {		
        char *msg_type = NULL;
        ma_error_t rc = MA_OK;
        ma_event_service_t *self = (ma_event_service_t*)cb_data;
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Event service received a message, processing it.");	
        ma_message_get_property(msg, MA_EVENT_MSG_PROP_KEY_MSG_TYPE, &msg_type);
        if(msg_type){
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Event service received a message type = <%s>.", msg_type);	

            if(!strcmp(msg_type, MA_EVENT_MSG_PROP_VALUE_REQ_POST_EVENT)) {
                rc = event_message_handler_event_recived(self, msg, c_request) ;
            }
            else if(!strcmp(msg_type, MA_EVENT_MSG_PROP_VALUE_REQ_UPLOAD_EVENT)) {
                rc = event_message_handler_send_event_to_epo(self, msg, c_request);
            }
            else if(!strcmp(msg_type, MA_EVENT_MSG_PROP_VALUE_REQ_GET_EVENT_FILTER)) {
                rc = event_message_handler_request_event_fiter(self, msg, c_request) ;
            }
            else {
                rc = event_message_handler_handle_unknown_message(self, msg, c_request);
            }
        }
        else {
            rc = event_message_handler_handle_unknown_message(self, msg, c_request);
        }	
        return rc;
    }
    return MA_OK;
}


static ma_error_t event_service_subscriber_cb(const char *topic, ma_message_t *payload, void *cb_data){

}