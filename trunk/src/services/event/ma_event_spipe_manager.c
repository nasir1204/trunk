#include "ma/ma_message.h"
#include "ma/ma_log.h"
#include "ma/internal/defs/ma_event_defs.h"
#include "ma/internal/services/ahclient/ma_ah_client_service.h"
#include "ma/internal/services/event/ma_event_spipe_manager.h"
#include "ma_event_xml_list.h"
#include "ma/internal/utils/text/ma_ini_parser.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal/defs/ma_general_defs.h"

#include "ma/internal/defs/ma_info_defs.h"
#include "ma/info/ma_info.h"
#include "ma/internal/defs/ma_io_defs.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define MA_SPIPE_ALERT_NONE								0x0000
#define MA_SPIPE_ALERT_IMMEDIATE						0x0001
#define MA_SPIPE_ALERT_NORMAL							0x0010
#define MA_SPIPE_ALERT_ON								0x0100

#define SET_SPIPE_IMMEDIATE_ALERT(alert)				(alert |= MA_SPIPE_ALERT_IMMEDIATE)
#define SET_SPIPE_NORMAL_ALERT(alert)					(alert |= MA_SPIPE_ALERT_NORMAL)
#define SET_SPIPE_ALERT_ON(alert)						(alert |= MA_SPIPE_ALERT_ON)

#define IS_SPIPE_IMMEDIATE_ALERT(alert)					(alert & MA_SPIPE_ALERT_IMMEDIATE)
#define IS_SPIPE_NORMAL_ALERT(alert)					(alert & MA_SPIPE_ALERT_NORMAL)
#define IS_SPIPE_ANY_ALERT(alert)						(alert & MA_SPIPE_ALERT_NORMAL || alert & MA_SPIPE_ALERT_IMMEDIATE)
#define IS_SPIPE_ALERT_NONE(alert)						(MA_SPIPE_ALERT_NONE == alert)

#define IS_SPIPE_ALERT_ON(alert)						(alert & MA_SPIPE_ALERT_ON)

#define SPIPE_ALERT_RESET(alert)						(alert = 0) 

#define MA_DEFAULT_IMMEDIATE_EVENT_BATCH_SIZE			5
#define MA_DEFAULT_NORMAL_EVENT_BATCH_SIZE				10*1000

#define MA_EVENT_FILTER_VERSION_KEY						"Version"
#define MA_EVENT_FILTER_EVENT_LIST_KEY					"DisabledEvent"

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "event"

/*local helper functions*/
static void publish_disabled_events(ma_event_spipe_manager_t *self);

/* Event spipe decorator*/
static ma_error_t event_spipe_decorator_decorate(ma_spipe_decorator_t *decorator, ma_spipe_priority_t, ma_spipe_package_t *pkg);
static ma_error_t event_spipe_decorator_get_spipe_priority(ma_spipe_decorator_t *decorator, ma_spipe_priority_t *priority);
static ma_error_t event_spipe_decorator_destroy(ma_spipe_decorator_t *decorator);

static struct ma_spipe_decorator_methods_s decorator_methods = {
	&event_spipe_decorator_decorate,
	&event_spipe_decorator_get_spipe_priority,
	&event_spipe_decorator_destroy
};

/* Event spipe handler*/
static ma_error_t event_spipe_handler_on_spipe_response(ma_spipe_handler_t *handler, const ma_spipe_response_t *spipe_response);
static ma_error_t event_spipe_handler_destroy(ma_spipe_handler_t *handler);

static struct ma_spipe_handler_methods_s handler_methods = {
	&event_spipe_handler_on_spipe_response,
	&event_spipe_handler_destroy	
};

struct ma_event_spipe_manager_s{
    /*ma context instance*/
	ma_context_t *context;

	/*event service decorator*/
	ma_spipe_decorator_t *event_decorator;

	/*event service handler*/
	ma_spipe_handler_t *event_handler;

	/*event persist*/
	ma_event_persist_t *evt_persist;

	/*current event xml list*/
	ma_event_xml_list_t *event_xml_list;

	/*for internal use, indication that if last time we have sent something while spipe decoration*/
	ma_bool_t is_decoration_done;

	/*current alerts*/
	int spipe_alert;
	
	/*event spipe priority*/
	ma_spipe_priority_t event_spipe_priority;

	/*priority event forwarding policy*/	
	ma_priority_event_forwarding_policy_t event_service_policy;

};

ma_error_t ma_event_spipe_manager_create(ma_context_t *context, ma_event_persist_t *evt_persist, ma_event_spipe_manager_t **evt_spipe_manager){
	if(context && evt_persist && evt_spipe_manager){
		ma_event_spipe_manager_t *mgr = (ma_event_spipe_manager_t*) calloc (1, sizeof(ma_event_spipe_manager_t) );
		if(mgr){
			mgr->event_decorator = (ma_spipe_decorator_t*) calloc(1, sizeof(ma_spipe_decorator_t) );
			if(mgr->event_decorator){
				mgr->event_handler = (ma_spipe_handler_t*) calloc(1, sizeof(ma_spipe_handler_t) );
				if(mgr->event_handler){
					ma_spipe_decorator_init(mgr->event_decorator, &decorator_methods, mgr);
					ma_spipe_handler_init(mgr->event_handler, &handler_methods, mgr);
					mgr->context = context;                    
					mgr->evt_persist = evt_persist;		
					*evt_spipe_manager = mgr;
					return MA_OK;
				}
				free(mgr->event_decorator);
			}
			free(mgr);
		}
		MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_ERROR, "Failed to create event spipe manager.");				
		return MA_ERROR_OUTOFMEMORY;
	}
	return MA_ERROR_INVALIDARG;
}

ma_spipe_decorator_t *ma_event_spipe_manager_get_spipe_decorator(ma_event_spipe_manager_t *self){
	return self ? self->event_decorator : NULL;
}

ma_spipe_handler_t *ma_event_spipe_manager_get_spipe_handler(ma_event_spipe_manager_t *self){
	return self ? self->event_handler : NULL;	
}
void ma_event_spipe_manager_set_priority_event_forwarding_policy(ma_event_spipe_manager_t *self, const ma_priority_event_forwarding_policy_t *ev_policy){
	if(self && ev_policy){
		self->event_service_policy.is_enabled = ev_policy->is_enabled;
		if(self->event_service_policy.is_enabled){
			self->event_service_policy.event_batch_size = ev_policy->event_batch_size;	
			self->event_service_policy.event_trigger_threshold = ev_policy->event_trigger_threshold;
			self->event_service_policy.trigger_interval = ev_policy->trigger_interval;
		}
		ma_event_persist_refresh_event_filter_version(self->evt_persist);
		publish_disabled_events(self);
	}
}

const ma_priority_event_forwarding_policy_t *ma_event_spipe_manager_get_priority_event_forwarding_policy(ma_event_spipe_manager_t *self){
	return self->event_service_policy.is_enabled ? &self->event_service_policy : NULL;
} 

ma_bool_t ma_event_spipe_manager_raise_alert(ma_event_spipe_manager_t *self, ma_event_priority_t new_priority){
	if(self){		
		ma_error_t error = MA_OK;
		ma_bool_t check_normal_events = MA_FALSE;			
		ma_bool_t check_immediate_events = MA_FALSE;			
		if(!self->event_service_policy.is_enabled && MA_EVENT_PRIORITY_IMMEDIATE == new_priority) 
			return MA_FALSE;
				
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "Agent is looking for events to upload" );
		
		switch(new_priority){				
			case MA_EVENT_PRIORITY_UNDECIDED:					
				break;
			case MA_EVENT_PRIORITY_NORMAL:
				ma_event_persist_is_event_exist(self->evt_persist, MA_EVENT_PRIORITY_NORMAL, &check_normal_events);
			case MA_EVENT_PRIORITY_IMMEDIATE:
				ma_event_persist_is_event_exist(self->evt_persist, MA_EVENT_PRIORITY_IMMEDIATE, &check_immediate_events);				
		}		

		switch(new_priority){				
			case MA_EVENT_PRIORITY_UNDECIDED:					
				break;
			case MA_EVENT_PRIORITY_NORMAL:
				if(check_normal_events || check_immediate_events)
					SET_SPIPE_NORMAL_ALERT(self->spipe_alert);
				break;
			case MA_EVENT_PRIORITY_IMMEDIATE:
				if(check_immediate_events)					
					SET_SPIPE_IMMEDIATE_ALERT(self->spipe_alert);
				break;
		}
		/*if there are some events to send and no event spipe alert then we can raise the alert.*/
		if(IS_SPIPE_ANY_ALERT(self->spipe_alert) && !IS_SPIPE_ALERT_ON(self->spipe_alert)){
			self->event_spipe_priority = MA_SPIPE_PRIORITY_IMMEDIATE;
			if (MA_OK == (error = ma_ah_client_service_raise_spipe_alert(MA_CONTEXT_GET_AH_CLIENT(self->context), STR_PKGTYPE_EVENT))) {				
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Event service raised spipe alert.");			
				SET_SPIPE_ALERT_ON(self->spipe_alert);
			}			
		}
		if(!IS_SPIPE_ALERT_ON(self->spipe_alert)) {
			if(MA_EVENT_PRIORITY_NORMAL == new_priority && !self->is_decoration_done) {
				if(!(check_normal_events || check_immediate_events))
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "Agent did not find any events to upload");			
			}
		}		
		return (check_normal_events || check_immediate_events);
	}
	return MA_FALSE;
}

void ma_event_spipe_manager_release(ma_event_spipe_manager_t *self){
	if(self){
		ma_spipe_decorator_release(self->event_decorator);
		ma_spipe_handler_release(self->event_handler);
		if(self->event_xml_list)
			ma_event_xml_list_release(self->event_xml_list);		
		free(self);
	}	
}

static MA_INLINE ma_bool_t should_decorate_event_filter_version(const char *pkg_type){
	/*TBD: add the appropriate logic, should we add the event filter version.*/
	return MA_TRUE;
}
static MA_INLINE ma_bool_t should_decorate_event(const char *pkg_type){
	if(pkg_type){
		if( !strcmp(pkg_type, STR_PKGTYPE_EVENT) )
			return MA_TRUE;
	}
	return MA_FALSE;
}

/*Implement event spipe decorator*/
ma_error_t event_spipe_decorator_decorate(ma_spipe_decorator_t *decorator, ma_spipe_priority_t priority, ma_spipe_package_t *pkg){
	ma_event_spipe_manager_t *self = (ma_event_spipe_manager_t*) decorator->data;
	size_t size = 0;
	const char *package_type = NULL;
	if(self->event_xml_list){
		ma_event_xml_list_release(self->event_xml_list);
		self->event_xml_list = NULL;
	}	
	
	/*Should we decorate event filter as per the package type.*/
	ma_spipe_package_get_info(pkg, STR_PACKAGE_TYPE, (const unsigned char**)&package_type, &size);
	if( should_decorate_event_filter_version(package_type) ){
		size_t size = 10;
		char *event_filter_version = NULL;		
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Adding event filter version to spipe package.");
		ma_event_persist_get_event_filter_version(self->evt_persist, &event_filter_version, &size);
		ma_spipe_package_add_info(pkg, MA_STR_SPIPEPKG_INFO_EVENT_FILTER_VERSION, event_filter_version ? (const unsigned char*)event_filter_version : "0", event_filter_version ? strlen(event_filter_version) : 1);
	}
	
	/*Should we decorate package.*/
	if( should_decorate_event(package_type) ){
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "Sending Events..." );

		self->event_xml_list = ma_event_xml_list_create();
		self->is_decoration_done = MA_TRUE;
		if(self->event_xml_list){
			if(IS_SPIPE_NORMAL_ALERT(self->spipe_alert) || (ma_configurator_get_vdimode(MA_CONTEXT_GET_CONFIGURATOR(self->context)))){
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Sending event package to ePO.");
				ma_event_persist_get_event_xml_list(self->evt_persist, MA_EVENT_PRIORITY_UNDECIDED, MA_DEFAULT_NORMAL_EVENT_BATCH_SIZE, self->event_xml_list);
			}
			else{
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Sending priority event package to ePO.");
				ma_event_persist_get_event_xml_list(self->evt_persist, MA_EVENT_PRIORITY_IMMEDIATE, self->event_service_policy.event_batch_size, self->event_xml_list);
				ma_event_persist_get_event_xml_list(self->evt_persist, MA_EVENT_PRIORITY_NORMAL, (MA_DEFAULT_NORMAL_EVENT_BATCH_SIZE - self->event_service_policy.event_batch_size), self->event_xml_list);
			}
			/*We have some events to send.*/
			if(self->event_xml_list->event_xml_count){		
				ma_event_xml_info_node_t *node = NULL;								
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Adding event xmls to spipe package.");

				MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "Agent is sending EVENT package to ePO server");
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "Agent uploading %d events to ePO Server", self->event_xml_list->event_xml_count);

				MA_SLIST_FOREACH(self->event_xml_list, event_xml, node){
					char *key = NULL;					
					unsigned char *value = NULL;
					size_t size = 0;
					ma_buffer_get_string(node->name, &key, &size);
					ma_buffer_get_raw(node->xml, &value, &size);
					if(key && value){
						if (MA_OK != ma_spipe_package_add_data(pkg, key, value, size ) )
							MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to add event xml %s to spipe package.", key);						
					}
					ma_buffer_release(node->xml);
					node->xml = NULL;
				}								
			}
		}
		self->event_spipe_priority = MA_SPIPE_PRIORITY_NONE;
	}
	return MA_OK;
}

ma_error_t event_spipe_decorator_get_spipe_priority(ma_spipe_decorator_t *decorator, ma_spipe_priority_t *priority){
	ma_event_spipe_manager_t *self = (ma_event_spipe_manager_t*) decorator->data;
	*priority = self->event_spipe_priority;
	return MA_OK;
}

ma_error_t event_spipe_decorator_destroy(ma_spipe_decorator_t *decorator){	
	free(decorator);
	return MA_OK;	
}

extern ma_error_t form_event_filter_message(ma_context_t *context, ma_bool_t is_publish, ma_event_persist_t *event_ds, ma_message_t **msg);
static void publish_disabled_events(ma_event_spipe_manager_t *self){
	ma_message_t *event_filter_message = NULL;
	MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO, "Publishing event filter.");
	form_event_filter_message(self->context, MA_TRUE, self->evt_persist, &event_filter_message);
	if(event_filter_message){
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Created event filter message.");
		if(MA_OK != ma_msgbus_publish(MA_CONTEXT_GET_MSGBUS(self->context), MA_EVENT_MSG_TOPIC_PUBSUB_EVENT_FILTER, MSGBUS_CONSUMER_REACH_OUTPROC,  event_filter_message) ){
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to published event fllter.");
		}
		ma_message_release(event_filter_message);
	}
}

struct ma_event_filter_ini_content_s { 		
	char *version ;	
	char *disabled_events;	
};

static int event_filter_ini_handler(const char *section, const char *key, const char *value, void *userdata){
	struct ma_event_filter_ini_content_s *event_filter = (struct ma_event_filter_ini_content_s*)userdata;
	if(!strcmp(key, MA_EVENT_FILTER_VERSION_KEY))
		event_filter->version = (char*)value;
	else if(!strcmp(key, MA_EVENT_FILTER_EVENT_LIST_KEY))
		event_filter->disabled_events = (char*)value;
	return 0;
}

/* Implement event spipe handler */
ma_error_t event_spipe_handler_on_spipe_response(ma_spipe_handler_t *handler, const ma_spipe_response_t *spipe_response){
	ma_event_spipe_manager_t *self = (ma_event_spipe_manager_t*) handler->data;
	
	/* Check if we have got the event filter data then extract and set to persist also publish them.*/
	if(spipe_response->respond_spipe_pkg){
		size_t size = 0;
		const unsigned char *event_ini = NULL;		
		
		/*get EvtFiltr.ini file from the pkg*/
		ma_spipe_package_get_data(spipe_response->respond_spipe_pkg, MA_STR_SPIPEPKG_DATA_EVENT_FILTER_INI, (void**)&event_ini, &size );
		if(event_ini){		
			struct ma_event_filter_ini_content_s event_filter = {NULL, NULL};
            const char *data_path = NULL;
			char disbaled_events[] = {"0;"};

            ma_configurator_t *configurator = MA_CONTEXT_GET_CONFIGURATOR(self->context);

            if((data_path = ma_configurator_get_agent_data_path(configurator))) {
                FILE *event_fp = NULL;
                char path[MA_MAX_PATH_LEN] = {0};

                MA_MSC_SELECT(_snprintf, snprintf)(path, MA_MAX_PATH_LEN,"%s%c%s%c%s", data_path, MA_PATH_SEPARATOR, MA_AGENT_DATA_DIR_STR, MA_PATH_SEPARATOR, MA_STR_SPIPEPKG_DATA_EVENT_FILTER_INI);
                if((event_fp = fopen(path, "w"))) {
                    MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO, "EvtFiltr.ini writing into disk path(%s)", path);
                    fwrite(event_ini, 1, size, event_fp);
                    fclose(event_fp);
                }
            }

			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO, "EvtFiltr.ini received from epo, extract the details.");

			/*extract  Version & DisabledEvent EvtFiltr.ini file from the pkg*/
			ma_ini_parser_start((char*)event_ini, size, event_filter_ini_handler, &event_filter);			

			/*Check if we got both  Version & DisabledEvent in EvtFiltr.ini file*/
			if(event_filter.version){				
				/*Save in persist*/
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO, "Persisting the event filter version and disabled events.");
				ma_event_persist_set_event_filter_version(self->evt_persist, event_filter.version);
				ma_event_persist_set_disabled_event_list(self->evt_persist, event_filter.disabled_events ? event_filter.disabled_events : disbaled_events);									
				/*Publish it*/
				publish_disabled_events(self);												
			}
			else{
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Malformed EvtFiltr.ini received from epo, EvtFiltr.ini = %s.", event_ini);
			}
		}
	}	

	/* Check if we did the decoration, and succeeded then deletes the event xmls from persist.*/
	if(self->is_decoration_done){

		int prev_alert = self->spipe_alert;
		SPIPE_ALERT_RESET(self->spipe_alert);
		/*Check if we have events*/
		if(self->event_xml_list){
			size_t size = 0;
			const char *package_type = NULL ;
			(void)ma_spipe_package_get_info(spipe_response->respond_spipe_pkg, STR_PACKAGE_TYPE, (const unsigned char**)&package_type, &size) ;

			/*Check if last spipe connection succeeded then remove events from persist.*/
			if(IS_SPIPE_SEND_SUCCEEDED(spipe_response->http_response_code)){
				/*no package from epo or no request public key then events are accepted by epo.*/
				if(!package_type || strcmp(package_type, STR_PKGTYPE_REQUEST_PUB_KEY)) {
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "Package uploaded to ePO Server successfully") ;
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO, "Removing the last sent events from persist.");
					ma_event_persist_remove_event_xml(self->evt_persist, self->event_xml_list); 				
				
					/* send ma info message */
					{
						ma_message_t *info_message = NULL ;
						ma_error_t rc = MA_OK ;
						if(MA_OK == (rc = ma_message_create(&info_message))) {
							ma_msgbus_endpoint_t *ep = NULL ;
					
							(void)ma_message_set_property(info_message, MA_PROP_KEY_REQUEST_TYPE_STR, MA_PROP_VALUE_AGENT_INFO_COLLECT) ;
							(void)ma_message_set_property(info_message, MA_INFO_MSG_KEY_EVENT_TYPE_STR, MA_INFO_EVENT_EVENTS_UPLOADED_STR) ;

							if(MA_OK == (rc = ma_msgbus_endpoint_create(MA_CONTEXT_GET_MSGBUS(self->context), MA_IO_SERVICE_NAME_STR, NULL, &ep))) {
								(void)ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_INPROC) ;
								(void)ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
		
						
								if(MA_OK != (rc = ma_msgbus_send_and_forget(ep, info_message)))
									MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to send ma info message, rc = <%d>.", rc) ;
						
								(void)ma_msgbus_endpoint_release(ep) ;	ep = NULL ;
							}
							(void)ma_message_release(info_message) ; info_message = NULL ;
						}
					}
				}

				if(IS_SPIPE_NORMAL_ALERT(prev_alert) && (202 == spipe_response->http_response_code))
					ma_event_spipe_manager_raise_alert(self, MA_EVENT_PRIORITY_NORMAL);
			}
			else{
				if(self->event_xml_list->event_xml_count)
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR | MA_LOG_FLAG_LOCALIZE, "Agent failed to send events");				
			}
			ma_event_xml_list_release(self->event_xml_list);
			self->event_xml_list = NULL;
		}	
		self->is_decoration_done = MA_FALSE;		
	}	
	return MA_OK;
}

ma_error_t event_spipe_handler_destroy(ma_spipe_handler_t *handler){	
	free(handler);
	return MA_OK;	
}


