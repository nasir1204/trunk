#ifndef MA_EVENT_XML_LIST_H_INCLUDED
#define MA_EVENT_XML_LIST_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_buffer.h"
#include "ma/internal/utils/datastructures/ma_slist.h"

#include <stdlib.h>

#define MAX_BUFFER_SIZE (9 * 1024 * 1024)

MA_CPP(extern "C" {)

/* In memeory representation of event xml data*/
struct ma_event_xml_info_node_s{
	ma_buffer_t *name;
	ma_buffer_t *xml;
	ma_buffer_t *path;
	ma_event_priority_t priority;
	MA_SLIST_NODE_DEFINE(struct ma_event_xml_info_node_s);
};

/* In memeory representation of event xml list*/
struct ma_event_xml_list_s{
	MA_SLIST_DEFINE(event_xml,struct ma_event_xml_info_node_s);
	int max_buffer_size;
};

static void ma_event_xml_info_node_free(struct ma_event_xml_info_node_s *node){
	ma_buffer_release(node->path);
	ma_buffer_release(node->name);
	ma_buffer_release(node->xml);
	free(node);
}

static MA_INLINE ma_event_xml_list_t *ma_event_xml_list_create(){
	ma_event_xml_list_t *list = (ma_event_xml_list_t*) calloc(1, sizeof(ma_event_xml_list_t) );
	list->max_buffer_size = MAX_BUFFER_SIZE;
	return list;
}

static MA_INLINE void ma_event_xml_list_add(ma_event_xml_list_t *xml_list, ma_event_xml_info_node_t *node){
	MA_SLIST_PUSH_BACK(xml_list, event_xml, node);
}

static MA_INLINE void ma_event_xml_list_clear(ma_event_xml_list_t *xml_list){
	MA_SLIST_CLEAR(xml_list, event_xml, ma_event_xml_info_node_t, ma_event_xml_info_node_free);
}

static MA_INLINE void ma_event_xml_list_release(ma_event_xml_list_t *xml_list){
	ma_event_xml_list_clear(xml_list);
	free(xml_list);
}


MA_CPP(})

#endif

