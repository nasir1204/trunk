#include "ma/internal/services/event/ma_event_service.h"
#include "ma/internal/defs/ma_event_defs.h"
#include "ma/internal/defs/ma_object_defs.h"

#include "ma/ma_msgbus.h"
#include "ma/ma_message.h"
#include "ma/internal/core/msgbus/ma_msgbus_private.h"
#include "ma/internal/services/ahclient/ma_ah_client_service.h"
#include "ma/internal/services/ma_policy_settings_bag.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/utils/platform/ma_system_property.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/platform/ma_sys_apis.h"
#include "ma/internal/utils/xml/ma_xml.h"
#include "ma/internal/utils/xml/ma_xml_node.h"
#include "ma/internal/ma_strdef.h"

#include "ma/internal/services/event/ma_event_persist.h"
#include "ma/internal/services/event/ma_event_spipe_manager.h"
#include "ma_event_xml_generator.h"
#include "ma/internal/defs/ma_general_defs.h"
#include "ma/datastore/ma_ds_registry.h"

#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>

#include "uv.h"

#ifdef MA_WINDOWS
#include <Windows.h>
#else
#include <sys/statvfs.h>
#endif


#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "event"


#define MA_EVENT_DISK_SPACE_THRESHOLD_IN_MB			500
#define MA_EVENT_DISK_SPACE_THRESHOLD_PERCENT		5
#define MA_EVENT_CHECK_DISK_SPACE_AFTER_EVENTS		10

typedef struct ma_event_service_s ma_event_service_t;

ma_error_t ma_event_service_configure(ma_event_service_t *service, ma_context_t *context, unsigned hint);
ma_error_t ma_event_service_start(ma_event_service_t *self);
ma_error_t ma_event_service_stop(ma_event_service_t *self);
ma_error_t ma_event_service_destroy(ma_event_service_t *self);

static struct ma_service_methods_s methods = {
    &ma_event_service_configure,
    &ma_event_service_start,
    &ma_event_service_stop,
    &ma_event_service_destroy
};

static ma_error_t event_message_received_cb(ma_msgbus_server_t *listener, ma_message_t *msg, void *cb_data, ma_msgbus_client_request_t *c_request);
static ma_error_t event_service_subscriber_cb(const char *topic, ma_message_t *payload, void *cb_data);
static ma_error_t read_legacy_event_from_disk(ma_event_service_t *service);
ma_error_t machine_node_add_machine_info(ma_context_t *context, ma_xml_node_t *machine_info_node);

struct ma_event_service_s {
    /*base class*/
    ma_service_t				base;

    /*context objects.*/
    ma_context_t				*context;

    /*event datastore*/
    ma_event_persist_t			*event_ds;

    /*event spipe decorator/handler*/
    ma_event_spipe_manager_t	*event_spipe_mgr;

    /*event message listener*/
    ma_msgbus_server_t			*event_msg_listener;

    /*is running.*/
    ma_bool_t                   is_running;

    /*disk space threshold percentage*/
    ma_int32_t					disk_threshold_percentage;

    ma_int32_t					min_disk_space_threshold;

	/*disk space after events*/
	ma_int32_t					check_after_events;

	ma_bool_t					check_disk_on_each_event;
};

static MA_INLINE void event_service_deinit(ma_event_service_t *service) {
    if (service) {
        if (service->event_ds) {
            ma_event_persist_release(service->event_ds);
        }
        service->event_ds = NULL;

        if (service->event_spipe_mgr) {
            ma_event_spipe_manager_release(service->event_spipe_mgr);
        }
        service->event_spipe_mgr = NULL;

        if (service->event_msg_listener) {
            ma_msgbus_server_release(service->event_msg_listener);
        }
        service->event_msg_listener = NULL;

        service->context = NULL;
    }
}

ma_error_t ma_event_service_create(const char *service_name, ma_service_t **event_service) {
    if (service_name && event_service) {
        ma_event_service_t *service = NULL;
        service = (ma_event_service_t*) calloc(1, sizeof(ma_event_service_t));
        if (service) {
            ma_service_init((ma_service_t*)service, &methods, service_name, service);
            service->disk_threshold_percentage = MA_EVENT_DISK_SPACE_THRESHOLD_PERCENT;
            service->min_disk_space_threshold = MA_EVENT_DISK_SPACE_THRESHOLD_IN_MB;
			service->check_after_events = MA_EVENT_CHECK_DISK_SPACE_AFTER_EVENTS;
			service->check_disk_on_each_event = MA_TRUE;
            *event_service = (ma_service_t*)service;
            return MA_OK;
        }
        return MA_ERROR_OUTOFMEMORY;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_bool_t check_context_validity(ma_context_t *context) {
    /*Will see if we need this*/
    return MA_TRUE;
}

static void add_compat_event_policy(ma_event_service_t *self, ma_context_t *context){
		
#ifndef MA_WINDOWS
	const ma_network_system_property_t *net				= ma_system_property_network_get(MA_CONTEXT_GET_SYSTEM_PROPERTY(context), MA_FALSE);	
	const ma_misc_system_property_t *misc				= ma_system_property_misc_get(MA_CONTEXT_GET_SYSTEM_PROPERTY(context), MA_FALSE);	
	const ma_os_system_property_t *os					= ma_system_property_os_get(MA_CONTEXT_GET_SYSTEM_PROPERTY(context), MA_FALSE);		
    ma_configurator_t *configurator						= MA_CONTEXT_GET_CONFIGURATOR(self->context);
	const char *guid									= ma_configurator_get_agent_guid(configurator);    
	ma_ds_t *ds											= ma_configurator_get_datastore(configurator);
	const ma_priority_event_forwarding_policy_t *policy = ma_event_spipe_manager_get_priority_event_forwarding_policy(self->event_spipe_mgr);
	const char *data_path								= NULL;
	
    if((data_path = ma_configurator_get_agent_data_path(configurator))) {
        FILE *event_fp = NULL;
        char buffer[MA_MAX_PATH_LEN] = {0};

        MA_MSC_SELECT(_snprintf, snprintf)(buffer, MA_MAX_PATH_LEN,"%s%c%s%c%s", data_path, MA_PATH_SEPARATOR, MA_AGENT_DATA_DIR_STR, MA_PATH_SEPARATOR, key_event_policy_ini);
        if((event_fp = fopen(buffer, "w"))) {
			ma_buffer_t *buff = NULL;

            MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_INFO, "%s writing into disk path(%s)", key_event_policy_ini, buffer);

			/*write section*/
			MA_MSC_SELECT(_snprintf, snprintf)(buffer, MA_MAX_PATH_LEN,"[%s]\n", key_str_event_section);
			fwrite(buffer, 1, strlen(buffer), event_fp);

			/*write event directory path.*/
			MA_MSC_SELECT(_snprintf, snprintf)(buffer, MA_MAX_PATH_LEN,"%s=%s%c%s\n", key_str_events_EventDirectory, data_path, MA_PATH_SEPARATOR, MA_AGENT_EVENTS_PATH_STR);
            fwrite(buffer, 1, strlen(buffer), event_fp);

			/*write event filter*/
			if(MA_OK == ma_ds_get_str(ds, MA_EVENT_POLICIES_PATH_NAME_STR, MA_EVENT_KEY_FILTERED_LIST_STR , &buff)){
				const char *evt_filter = NULL;
				size_t size = 0;

				if(MA_OK == ma_buffer_get_string(buff, &evt_filter, &size)){
					MA_MSC_SELECT(_snprintf, snprintf)(buffer, MA_MAX_PATH_LEN,"%s=%s\n", key_str_events_EventFilterString, evt_filter);
					fwrite(buffer, 1, strlen(buffer), event_fp);
				}
				ma_buffer_release(buff);
				buff = NULL;
			}            

			/*write agent guid*/
			if(guid){
				MA_MSC_SELECT(_snprintf, snprintf)(buffer, MA_MAX_PATH_LEN,"%s=%s\n", key_str_events_EventAgentGuid, guid);
				fwrite(buffer, 1, strlen(buffer), event_fp);
			}

			/*add event policy.*/
			if(policy){
				MA_MSC_SELECT(_snprintf, snprintf)(buffer, MA_MAX_PATH_LEN,"%s=%d\n", key_str_events_EnableEventTrigger, policy->is_enabled);
				fwrite(buffer, 1, strlen(buffer), event_fp);

				MA_MSC_SELECT(_snprintf, snprintf)(buffer, MA_MAX_PATH_LEN,"%s=%d\n", key_str_events_EventTriggerThreshold, policy->event_batch_size);
				fwrite(buffer, 1, strlen(buffer), event_fp);
			}

			if(misc){
				MA_MSC_SELECT(_snprintf, snprintf)(buffer, MA_MAX_PATH_LEN,"%s=%s\n", key_str_events_EventComputerName, misc->computername);
				fwrite(buffer, 1, strlen(buffer), event_fp);

				MA_MSC_SELECT(_snprintf, snprintf)(buffer, MA_MAX_PATH_LEN,"%s=%s\n", key_str_events_EventUser, misc->username);
				fwrite(buffer, 1, strlen(buffer), event_fp);
			}

			if(os){
				MA_MSC_SELECT(_snprintf, snprintf)(buffer, MA_MAX_PATH_LEN,"%s=%s\n", key_str_events_EventOS, os->type);
				fwrite(buffer, 1, strlen(buffer), event_fp);
			}

			if(net){
				MA_MSC_SELECT(_snprintf, snprintf)(buffer, MA_MAX_PATH_LEN,"%s=%s\n", key_str_events_EventMacAddress, net->mac_address_formated);
				fwrite(buffer, 1, strlen(buffer), event_fp);

				MA_MSC_SELECT(_snprintf, snprintf)(buffer, MA_MAX_PATH_LEN,"%s=%s\n", key_str_events_EventIPAddress, net->ip_address_formated);
				fwrite(buffer, 1, strlen(buffer), event_fp);
			}
			fclose(event_fp);
		}
    }
#endif
}
ma_error_t ma_event_service_configure(ma_event_service_t *self, ma_context_t *context, unsigned hint) {
    if (self && context) {
        if (MA_SERVICE_CONFIG_NEW == hint) {
            if (check_context_validity(context)) {
                ma_error_t rc = MA_OK;
                self->context = context;
                if (MA_OK == (rc = ma_event_persist_create(self->context, &self->event_ds)) &&
                        MA_OK == (rc = ma_event_spipe_manager_create(self->context, self->event_ds, &self->event_spipe_mgr)) &&
                        MA_OK == (rc = ma_msgbus_server_create(MA_CONTEXT_GET_MSGBUS(self->context), MA_EVENT_SERVICE_NAME_STR, &self->event_msg_listener))) {
                    MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO, "Successfully configured the event service.");
                } else {
                    MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_ERROR, "Failed to create event service instance, last error = %d", rc);
                    event_service_deinit(self);
                    return rc;
                }
            }
        }

        if (self->context) {
            /*retrive the agent policies and apply them.*/
            ma_priority_event_forwarding_policy_t policy = {MA_FALSE, 0, 0, MA_EVENT_SEVERITY_CRITICAL};
            ma_int32_t data = 0;

            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO, "Enforcing event service policy.");
            ma_policy_settings_bag_get_int(MA_CONTEXT_GET_POLICY_SETTINGS_BAG(self->context), MA_EVENT_SERVICE_SECTION_NAME_STR, MA_EVENT_DISK_SPACE_THRESHOLD_PERCENTAGE_INT, MA_FALSE, &data, MA_EVENT_DISK_SPACE_THRESHOLD_PERCENT);

            ma_policy_settings_bag_get_int(MA_CONTEXT_GET_POLICY_SETTINGS_BAG(self->context), MA_EVENT_SERVICE_SECTION_NAME_STR, MA_EVENT_DISK_SPACE_THRESHOLD_MIN_INT, MA_FALSE, &data, MA_EVENT_DISK_SPACE_THRESHOLD_IN_MB);
            
			self->check_after_events = MA_EVENT_CHECK_DISK_SPACE_AFTER_EVENTS;
			self->check_disk_on_each_event = MA_TRUE;

            if (MA_OK == ma_policy_settings_bag_get_int(MA_CONTEXT_GET_POLICY_SETTINGS_BAG(self->context), MA_EVENT_SERVICE_SECTION_NAME_STR, MA_EVENT_KEY_IS_ENABLED_PRIORITY_FORWARD_INT, MA_FALSE, &data, 1)) {
                policy.is_enabled = (data ? MA_TRUE : MA_FALSE);//#define SZ_TVD_SHARED_FRAMEWORK_A                 REG_6432_BASE_PATH_A "Network Associates\\TVD\\Shared Components\\Framework"
                if (policy.is_enabled) {
                    /*Set policy to event service.*/
                    if (MA_OK == ma_policy_settings_bag_get_int(MA_CONTEXT_GET_POLICY_SETTINGS_BAG(self->context), MA_EVENT_SERVICE_SECTION_NAME_STR, MA_EVENT_KEY_UPLOAD_TIMEOUT_INT, MA_FALSE, &data, 5)) {
                        policy.trigger_interval = data;
                        if (MA_OK == ma_policy_settings_bag_get_int(MA_CONTEXT_GET_POLICY_SETTINGS_BAG(self->context), MA_EVENT_SERVICE_SECTION_NAME_STR, MA_EVENT_KEY_UPLOAD_THRESHOLD_INT, MA_FALSE, &data, 10)) {
                            policy.event_batch_size = data;
                            if (MA_OK == ma_policy_settings_bag_get_int(MA_CONTEXT_GET_POLICY_SETTINGS_BAG(self->context), MA_EVENT_SERVICE_SECTION_NAME_STR, MA_EVENT_KEY_PRIORITY_LEVEL_INT, MA_FALSE, &data, 3)) {
                                if (data <= MA_EVENT_SEVERITY_CRITICAL) {
                                    policy.event_trigger_threshold = (ma_event_severity_t)data;
                                }
                            }
                        }
                    }
                }
            }
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Enforcing priority event send policy done (Policy:Enabled=<%d>:timeout=<%d>:batch=<%d>:priority threshold<%d>).", policy.is_enabled, policy.trigger_interval, policy.event_batch_size, policy.event_trigger_threshold);
            ma_event_spipe_manager_set_priority_event_forwarding_policy(self->event_spipe_mgr, &policy);
			add_compat_event_policy(self, self->context);
        }		
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_event_service_start(ma_event_service_t *self) {
    if (self) {
        if (self->context) {
            ma_error_t rc = MA_OK;
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO, "Starting event service.");
            /*start listener*/
            ma_msgbus_server_setopt(self->event_msg_listener, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
            ma_msgbus_server_setopt(self->event_msg_listener, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
            if (MA_OK == (rc = ma_msgbus_server_start(self->event_msg_listener, event_message_received_cb, self))) {
                ma_ah_client_service_register_spipe_decorator(MA_CONTEXT_GET_AH_CLIENT(self->context), ma_event_spipe_manager_get_spipe_decorator(self->event_spipe_mgr));
                ma_ah_client_service_register_spipe_handler(MA_CONTEXT_GET_AH_CLIENT(self->context), ma_event_spipe_manager_get_spipe_handler(self->event_spipe_mgr));
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO, "Successfully started event service.");
            } else {
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to starting event service.");
            }
            return rc;
        } else {
            return MA_ERROR_PRECONDITION;
        }
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_event_service_stop(ma_event_service_t *self) {
    if (self) {
        ma_error_t rc = MA_OK;
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO, "Stopping event service.");
        ma_ah_client_service_unregister_spipe_decorator(MA_CONTEXT_GET_AH_CLIENT(self->context), ma_event_spipe_manager_get_spipe_decorator(self->event_spipe_mgr));
        ma_ah_client_service_unregister_spipe_handler(MA_CONTEXT_GET_AH_CLIENT(self->context), ma_event_spipe_manager_get_spipe_handler(self->event_spipe_mgr));
        rc = ma_msgbus_server_stop(self->event_msg_listener);
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO, "Stopped event service.");
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_event_service_destroy(ma_event_service_t *self) {
    if (self) {
        event_service_deinit(self);
        free(self);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t form_event_filter_message(ma_context_t *context, ma_bool_t is_publish, ma_event_persist_t *event_ds, ma_message_t **msg) {
    ma_error_t rc = MA_OK;
    MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_DEBUG, "Forming event filter message.");
    if (MA_OK == (rc = ma_message_create(msg))) {
        /* Event filter infos*/
        char *filter_version = NULL;
        size_t filter_version_len = 10 ;
        ma_variant_t *disabled_events = NULL;

        /* Get event filter infos*/
        ma_event_persist_get_event_filter_version(event_ds, &filter_version, &filter_version_len);
        ma_event_persist_get_disabled_event_list(event_ds, &disabled_events);

        /* If we got event filter infos, then set them in payload*/
        if (filter_version && disabled_events) {
            ma_table_t *payload_tbl = NULL;
            ma_variant_t *payload = NULL;
            ma_message_set_property(*msg, MA_EVENT_MSG_PROP_KEY_EVENT_FILTER_VERSION, filter_version);
            if (MA_OK == ma_table_create(&payload_tbl) &&
                    MA_OK == ma_variant_create_from_table(payload_tbl, &payload)) {
                ma_table_add_entry(payload_tbl, MA_EVENT_MSG_PAYLOAD_KEY_DISABLED_EVENTS, disabled_events);
                ma_message_set_payload(*msg, payload);

            }
            ma_table_release(payload_tbl);
            ma_variant_release(payload);
        } else {
            MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_DEBUG, "event filter version or diabled events list not found.");
        }
        ma_variant_release(disabled_events);
    }
    return rc;
}
static ma_error_t event_message_handler_request_event_fiter(ma_event_service_t *self, ma_message_t *msg, ma_msgbus_client_request_t *c_request) {
    ma_message_t *response = NULL;
    ma_error_t rc = MA_OK;
    MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Event filter request received.");
    if (MA_OK == (rc = form_event_filter_message(self->context, MA_FALSE, self->event_ds, &response))) {
        /* Send the response back.*/
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Sending event filter message.");
        ma_msgbus_server_client_request_post_result(c_request, rc, response) ;
        ma_message_release(response) ;
    }
    return rc;
}

static ma_error_t form_event_xml_and_save(ma_event_service_t *self, ma_variant_t *event_payload) {
    if (self && event_payload) {
        ma_error_t rc = MA_OK;
        ma_bytebuffer_t  *event_xml_buffer = NULL;
        ma_event_severity_t highest_severity = MA_EVENT_SEVERITY_INFORMATIONAL;
        if (MA_OK == (rc = ma_bytebuffer_create(1024, &event_xml_buffer)) &&
                MA_OK == (rc = ma_event_xml_make(self->context, event_payload, event_xml_buffer, &highest_severity))) {
            ma_event_priority_t priority = MA_EVENT_PRIORITY_NORMAL;
            const ma_priority_event_forwarding_policy_t *event_policy = ma_event_spipe_manager_get_priority_event_forwarding_policy(self->event_spipe_mgr);
            if (event_policy && highest_severity >= event_policy->event_trigger_threshold) {
                priority = MA_EVENT_PRIORITY_IMMEDIATE;
            }
            rc = ma_event_persist_add_event(self->event_ds, priority, ma_bytebuffer_get_bytes(event_xml_buffer), ma_bytebuffer_get_size(event_xml_buffer));
        } else {
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to generate event xml, error = %d", rc);
        }
        if (event_xml_buffer) {
            ma_bytebuffer_release(event_xml_buffer);
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_custom_event_severity(ma_xml_t *custom_event, ma_event_severity_t *highest_severity) {
    if (custom_event && highest_severity) {
        ma_xml_node_t *node = NULL;
        if (node = ma_xml_node_search(ma_xml_get_node(custom_event), "Severity")) {
            const char *severity = ma_xml_node_get_data(node);
            *highest_severity = (ma_event_severity_t)atoi(severity);
            return MA_OK;
        }
    }
    return MA_ERROR_INVALIDARG;
}


static ma_error_t form_custom_event_xml_and_save(ma_event_service_t *self, ma_xml_t *custom_event) {
    if (self && custom_event) {
        ma_error_t rc = MA_OK;
        ma_bytebuffer_t  *event_xml_buffer = NULL;
        ma_event_severity_t highest_severity = MA_EVENT_SEVERITY_INFORMATIONAL;
        if ((MA_OK == (rc = ma_bytebuffer_create(1024, &event_xml_buffer))) &&
                (MA_OK == (rc = ma_custom_event_severity(custom_event, &highest_severity))) &&
                (MA_OK == (rc = ma_xml_save_to_buffer(custom_event, event_xml_buffer)))) {
            ma_event_priority_t priority = MA_EVENT_PRIORITY_NORMAL;
            const ma_priority_event_forwarding_policy_t *event_policy = ma_event_spipe_manager_get_priority_event_forwarding_policy(self->event_spipe_mgr);
            if (event_policy && highest_severity >= event_policy->event_trigger_threshold) {
                priority = MA_EVENT_PRIORITY_IMMEDIATE;
            }
            rc = ma_event_persist_add_event(self->event_ds, priority, ma_bytebuffer_get_bytes(event_xml_buffer), ma_bytebuffer_get_size(event_xml_buffer));
        } else {
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to save custom event xml, error = %d", rc);
        }

        if (event_xml_buffer) {
            ma_bytebuffer_release(event_xml_buffer);
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

static void publish_latest_event_filter(ma_event_service_t *self, ma_message_t *event_msg){
	char *filter_version = NULL;
    size_t filter_version_len = 10 ;
	const char *ms_filter_version = NULL;
    
    /* Get event filter infos*/
    ma_event_persist_get_event_filter_version(self->event_ds, &filter_version, &filter_version_len);
		
	if(MA_OK == ma_message_get_property(event_msg, MA_EVENT_MSG_PROP_KEY_EVENT_FILTER_VERSION, &ms_filter_version)){
		if(!ms_filter_version || !filter_version || 0 != strcmp(ms_filter_version, filter_version)){
			if(filter_version && 0 != strcmp(filter_version, "0")){
				ma_message_t *event_filter_message = NULL;
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO, "Publishing event filter, as clients don't have latest event filter version.");
				form_event_filter_message(self->context, MA_TRUE, self->event_ds, &event_filter_message);
				if(event_filter_message){
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Created event filter message.");
					if(MA_OK != ma_msgbus_publish(MA_CONTEXT_GET_MSGBUS(self->context), MA_EVENT_MSG_TOPIC_PUBSUB_EVENT_FILTER, MSGBUS_CONSUMER_REACH_OUTPROC,  event_filter_message) ){
						MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to published event fllter.");
					}
					ma_message_release(event_filter_message);
				}
			}
		}		
	}
}

static ma_error_t event_message_handler_event_recived(ma_event_service_t *self, ma_message_t *msg, ma_msgbus_client_request_t *c_request) {
    ma_error_t rc = MA_OK;
    ma_error_t status = MA_ERROR_EVENT_INVALID_EVENT_FORMAT;
    ma_message_t *response = NULL;
    ma_variant_t *payload = NULL;

    MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Received an event.");
    ma_message_get_payload(msg, &payload);
    if (payload) {
        if (MA_OK == (rc = form_event_xml_and_save(self,  payload))) {
            status = MA_OK;
        }
        ma_variant_release(payload);
    } else {
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Event message doesn't contain payload, ignoring it.");
    }
    if (MA_OK == (rc = ma_message_create(&response))) {
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Sending back the event message response.");
        ma_msgbus_server_client_request_post_result(c_request, status, response) ;
        ma_message_release(response) ;
    }
	publish_latest_event_filter(self, msg);
    return rc;
}

static ma_error_t custome_event_message_handler_event_recived(ma_event_service_t *self, ma_message_t *msg, ma_msgbus_client_request_t *c_request) {
    ma_error_t rc = MA_OK;
    ma_error_t status = MA_ERROR_EVENT_INVALID_EVENT_FORMAT;
    ma_message_t *response = NULL;
	ma_variant_t *variant = NULL;
	ma_buffer_t *buffer = NULL;
    MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Received an custom event.");
	if(MA_OK == ma_message_get_payload(msg,&variant)) {
		if(MA_OK == ma_variant_get_string_buffer(variant,&buffer)) {
			const char *event_str = NULL;
			size_t event_str_size = 0;
			if(MA_OK == ma_buffer_get_string(buffer, &event_str, &event_str_size)) {
				ma_xml_t *custom_event_xml = NULL;
				if (MA_OK == (rc = ma_xml_create(&custom_event_xml))) {
					if (MA_OK == (rc = ma_xml_load_from_buffer(custom_event_xml, event_str, strlen(event_str)))) {
						ma_xml_node_t *node = NULL;
						if (node = ma_xml_node_search(ma_xml_get_node(custom_event_xml), MA_CUSTOM_EVENT_MACHINEINFO_NODE)) {
							if (MA_OK == (rc = machine_node_add_machine_info(self->context, node))) {
								if (MA_OK == (rc = form_custom_event_xml_and_save(self,  custom_event_xml))) {
									MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Successfully added custom events.");
									status = MA_OK;
								}
							} else 
								MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "failed to add system info in custom event , ignoring it.");
						} else
							MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "failed to get MachineInfo node from custom event, ignoring it.");
					} else
						MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "failed to load xml from custom event buffer, ignoring it.");
					ma_xml_release(custom_event_xml);
				} else
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "failed to create xml object, ignoring it.");
			}else
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "failed to get string from buffer, ignoring it.");
		(void)ma_buffer_release(buffer);
		} else
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "failed to get buffer from variant, ignoring it.");
		(void)ma_variant_release(variant);
    } else {
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "custom event message doesn't contain payload, ignoring it.");
    }

    if (MA_OK == (rc = ma_message_create(&response))) {
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Sending back the custom event message response.");
        ma_msgbus_server_client_request_post_result(c_request, status, response) ;
        (void)ma_message_release(response) ;
    }
	publish_latest_event_filter(self, msg);
    return rc;
}

static ma_bool_t fs_move_file(ma_event_loop_t *loop, const char *src, const char *dst) {
    ma_bool_t b_rc = MA_FALSE;
    if(loop && src && dst) {
        uv_fs_t req;
        if(-1 != uv_fs_rename(ma_event_loop_get_uv_loop(loop), &req, src, dst, NULL)) {
            b_rc = MA_TRUE;
            uv_fs_req_cleanup(&req);
        }
    }
    return b_rc;
}

ma_error_t read_legacy_event_from_disk(ma_event_service_t *service) {
    if (service) {
        ma_error_t err = MA_OK;
        const char *data_path = NULL;
        ma_configurator_t *configurator = MA_CONTEXT_GET_CONFIGURATOR(service->context);

        if ((data_path = ma_configurator_get_agent_data_path(configurator))) {
            char path[MA_MAX_PATH_LEN] = {0};
            size_t size = 0;

            MA_MSC_SELECT(_snprintf, snprintf)(path, MA_MAX_PATH_LEN,"%s%c%s", data_path, MA_PATH_SEPARATOR, MA_AGENT_EVENTS_PATH_STR);
            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "reading legacy events from disk path(%s)", path);
            (void)ma_filesystem_iterator_count(path, MA_FILESYSTEM_ITERATE_FILES, &size);
            if (size) {
                ma_db_t *db_handle = ma_configurator_get_database(configurator);

				if(MA_OK == (err = ma_db_transaction_begin(db_handle))) {
					ma_filesystem_iterator_t *iterator = NULL;					
				
					if (MA_OK == (err = ma_filesystem_iterator_create(path, &iterator))) {
						
						ma_temp_buffer_t buf = {0};
						(void)ma_filesystem_iterator_set_mode(iterator, MA_FILESYSTEM_ITERATE_FILES);
						while (MA_OK == ma_filesystem_iterator_get_next(iterator, &buf)) {
							char file_path[MA_MAX_PATH_LEN] = {0};
							char dst_file_path[MA_MAX_PATH_LEN] = {0};
							char event_file[MA_MAX_PATH_LEN] = {0};

							MA_MSC_SELECT(_snprintf, snprintf)(file_path, MA_MAX_PATH_LEN,"%s%c%s", path, MA_PATH_SEPARATOR, (const char*)ma_temp_buffer_get(&buf));														
							MA_MSC_SELECT(_snprintf, snprintf)(event_file, MA_MAX_PATH_LEN, "mc_%s", (const char*)ma_temp_buffer_get(&buf));
							MA_MSC_SELECT(_snprintf, snprintf)(dst_file_path, MA_MAX_PATH_LEN,"%s%c%s%c%s", path, MA_PATH_SEPARATOR, MA_AGENT_EVENTS_UPLOAD_PATH_STR, MA_PATH_SEPARATOR, event_file);														

							MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "event(%s) persisting...", event_file);

							if (MA_OK == (err = ma_event_persist_add_event_by_name(service->event_ds, event_file, strstr(file_path, ".txml") ? MA_EVENT_PRIORITY_IMMEDIATE : MA_EVENT_PRIORITY_NORMAL, dst_file_path, strlen(dst_file_path)))) {
								if(!fs_move_file(ma_msgbus_get_event_loop(MA_CONTEXT_GET_MSGBUS(service->context)), file_path, dst_file_path))
									MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_ERROR, "Failed to move file from %s to %s", file_path, dst_file_path);
							}
							else{
								MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_ERROR, "event(%s) persisting failed, last error(%d)", event_file, err);
							}
							ma_temp_buffer_uninit(&buf);
						}						
						(void)ma_filesystem_iterator_release(iterator);
					} else {
						MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_ERROR, "reading legacy events from disk path(%s) failed, last error(%d)", path, err);
					}
					ma_db_transaction_end(db_handle);
				}
				else{
					MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_ERROR, "Failed in db transaction begin, last error(%d)", path, err);
				}

            } else {
                MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "legacy event folder(%s) empty ", path);
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t event_message_handler_send_event_to_epo(ma_event_service_t *self, ma_message_t *msg, ma_msgbus_client_request_t *c_request) {
    ma_error_t rc = MA_OK;
    ma_message_t *response = NULL;
    const char *priority = NULL;
    ma_event_priority_t ipriority = MA_EVENT_PRIORITY_NORMAL;

    /*check the message type.*/
    ma_message_get_property(msg, MA_EVENT_MSG_PROP_KEY_EVENT_PRIORITY, &priority);
    if (priority) {
        ipriority = (ma_event_priority_t)(atoi(priority));
    }

    /* reading legacy events from disk */
    (void)read_legacy_event_from_disk(self);

    MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "\"Send events to epo\", message received.");

    if(ma_event_spipe_manager_raise_alert(self->event_spipe_mgr, ipriority)){
		self->check_after_events = MA_EVENT_CHECK_DISK_SPACE_AFTER_EVENTS;
		self->check_disk_on_each_event = MA_TRUE;
	}
	
    /*Send response back.*/
    if (MA_OK == (rc = ma_message_create(&response))) {
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Sending the response back.");
        ma_msgbus_server_client_request_post_result(c_request, MA_OK, response) ;
        ma_message_release(response) ;
    }
    return rc;
}

static ma_error_t send_event_response(ma_event_service_t *self, ma_message_t *msg, ma_msgbus_client_request_t *c_request, ma_error_t ret_code) {
    ma_message_t *response = NULL;
    ma_error_t rc = MA_OK;
    if (MA_OK == (rc = ma_message_create(&response))) {
        ma_msgbus_server_client_request_post_result(c_request, ret_code, response) ;
        ma_message_release(response) ;
    }
    return rc;
}

static ma_bool_t get_disk_space(const char *path, ma_uint64_t *totalbytes, ma_uint64_t *freebytes) {
#ifdef MA_WINDOWS
    {
        char drv_string[4] = {0};
        LONGLONG free_bytes_tocaller, total_bytes, free_bytes;

        MA_MSC_SELECT(_snprintf, snprintf)(drv_string, 4,"%c:\\", path[0]);
        if (GetDiskFreeSpaceExA(drv_string, (PULARGE_INTEGER)&free_bytes_tocaller,(PULARGE_INTEGER)&total_bytes,(PULARGE_INTEGER)&free_bytes)) {
            *totalbytes = total_bytes;
            *freebytes = free_bytes;
            return MA_TRUE;
        }
        return MA_FALSE;
    }
#else
    {
        struct statvfs fs;
        if (0 == statvfs(path, &fs)) {
            *totalbytes =(ma_uint64_t)((ma_uint64_t) fs.f_blocks * (ma_uint64_t)fs.f_frsize) ;
            *freebytes =(ma_uint64_t)((ma_uint64_t)fs.f_bfree * (ma_uint64_t)fs.f_frsize);
            return MA_TRUE;
        }
        return MA_FALSE;
    }
#endif
}

static ma_uint64_t get_disk_space_threshold_mb(ma_event_service_t *self, ma_uint64_t totalbytes) {
    ma_uint64_t threshold_disk_value = (self->disk_threshold_percentage * totalbytes) / 100;
    threshold_disk_value = threshold_disk_value / (1024 * 1024);
    return (threshold_disk_value < self->min_disk_space_threshold ? threshold_disk_value : self->min_disk_space_threshold);
}

static ma_error_t is_sufficient_agent_disk_space(ma_event_service_t *self) {
    ma_configurator_t *configurator = MA_CONTEXT_GET_CONFIGURATOR(self->context);
    ma_uint64_t totalbytes = MA_EVENT_DISK_SPACE_THRESHOLD_IN_MB * 1024 * 1024;
    ma_uint64_t freebytes  = MA_EVENT_DISK_SPACE_THRESHOLD_IN_MB * 1024 * 1024;

	/* how about it checking after event 10 events. and check every time if closer to disk_threshold (freebytes_mb < threshold_mb*2)*/

	if(self->check_disk_on_each_event || 0 == (++self->check_after_events%MA_EVENT_CHECK_DISK_SPACE_AFTER_EVENTS)){
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Checking disk space in agent data path");

		if (get_disk_space(ma_configurator_get_agent_data_path(configurator), &totalbytes, &freebytes)) {
			ma_uint64_t freebytes_mb = freebytes / (1024 * 1024);
			ma_uint64_t threshold_mb = get_disk_space_threshold_mb(self, totalbytes);
			if (freebytes_mb < threshold_mb) {
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Low disk space in agent data path. Minimum free disk space required = %lu MB, free disk space available = %lu MB.", (unsigned long)threshold_mb, (unsigned long)freebytes_mb);
				return MA_ERROR_SYSTEM_INSUFFICIENT_DISK_SPACE;
			}
			if(freebytes_mb < threshold_mb*2){
				self->check_disk_on_each_event = MA_TRUE;
				self->check_after_events = MA_EVENT_CHECK_DISK_SPACE_AFTER_EVENTS;
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Reaching to low disk space in agent data path. Minimum free disk space required = %lu MB, free disk space available = %lu MB.", (unsigned long)threshold_mb, (unsigned long)freebytes_mb);				
			}
			else
				self->check_disk_on_each_event = MA_FALSE;							
		}
	}
    return MA_OK;
}

static ma_error_t event_message_received_cb(ma_msgbus_server_t *listener, ma_message_t *msg, void *cb_data, ma_msgbus_client_request_t *c_request) {
    if (listener && msg && cb_data) {
        const char *msg_type = NULL;
        ma_error_t rc = MA_OK;
        ma_event_service_t *self = (ma_event_service_t*)cb_data;
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Event service received a message, processing it.");
        (void)ma_message_get_property(msg, MA_EVENT_MSG_PROP_KEY_MSG_TYPE, &msg_type);
        if (msg_type) {
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Event service received a message type = <%s>.", msg_type);

            if (!strcmp(msg_type, MA_EVENT_MSG_PROP_VALUE_REQ_POST_EVENT)) {
                if (MA_OK == (rc = is_sufficient_agent_disk_space(self))) {
                    rc = event_message_handler_event_recived(self, msg, c_request) ;
                } else {
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Event can't be saved, err = <%d>.", rc);
					rc = send_event_response(self, msg, c_request, rc);
                }
            } else if (!strcmp(msg_type, MA_EVENT_MSG_PROP_VALUE_REQ_POST_CUSTOM_EVENT)) {
                if (MA_OK == (rc = is_sufficient_agent_disk_space(self))) {
                    rc = custome_event_message_handler_event_recived(self, msg, c_request);
                } else {
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Custom event can't be saved, err = <%d>.", rc);
					rc = send_event_response(self, msg, c_request, rc);
                }
            } else if (!strcmp(msg_type, MA_EVENT_MSG_PROP_VALUE_REQ_UPLOAD_EVENT)) {
                const char *props_ver = NULL ;
                size_t size = 0 ;
                ma_buffer_t *buffer = NULL ;
                ma_ds_t *ds = ma_configurator_get_datastore(MA_CONTEXT_GET_CONFIGURATOR(self->context)) ;

                if (MA_OK == (rc = ma_ds_get_str(ds, MA_PROPERTY_DATA_PATH_NAME_STR, MA_PROPERTY_DATA_KEY_PROPERTY_VERSION_STR, &buffer))) {
                    (void)ma_buffer_get_string(buffer, &props_ver, &size) ;
                    if (props_ver && (0 == strcmp(props_ver, "0"))) {
                        rc = MA_ERROR_ASC_NOT_PERFORMED ;
                    } else {
                        rc = event_message_handler_send_event_to_epo(
                            self, msg, c_request);
                    }
                    (void)ma_buffer_release(buffer) ;
                    buffer = NULL ;
                }
            } else if (!strcmp(msg_type, MA_EVENT_MSG_PROP_VALUE_REQ_GET_EVENT_FILTER)) {
                rc = event_message_handler_request_event_fiter(self, msg, c_request) ;
            } else {
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Unknown message received, ignoring it.");    
				rc = send_event_response(self, msg, c_request, MA_ERROR_EVENT_MSG_UNKNOWN);
            }
        } else {
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Unknown message received, ignoring it.");    
            rc = send_event_response(self, msg, c_request, MA_ERROR_EVENT_MSG_UNKNOWN);
        }
        return rc;
    }
    return MA_OK;
}


static ma_error_t event_service_subscriber_cb(const char *topic, ma_message_t *payload, void *cb_data) {
    return MA_OK;
}
