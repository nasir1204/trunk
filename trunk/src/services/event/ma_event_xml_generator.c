#include "ma/ma_common.h"
#include "ma/ma_errors.h"
#include "ma/internal/ma_macros.h"
#include "ma/event/ma_event_payload_defs.h"
#include "ma/internal/utils/datastructures/ma_bytebuffer.h"
#include "ma/internal/utils/xml/ma_xml.h"
#include "ma/internal/utils/xml/ma_xml_node.h"
#include "ma_event_xml_generator.h"

#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/ma_datastore.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/utils/platform/ma_system_property.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"

#include "ma/internal/services/ma_policy_settings_bag.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "event"

#define MA_CEF_XML_TAG_ROOT_NODE_NAME						"EPOEvent"
#define MA_CEF_XML_TAG_MACHINE_INFO_NODE_NAME				"MachineInfo"
#define MA_CEF_XML_TAG_SOFTWARE_INFO_NODE_NAME				"SoftwareInfo"
#define MA_CEF_XML_TAG_EVENT_NODE_NAME						"Event"
#define MA_CEF_XML_TAG_AGENT_GUID_NODE_NAME					"AgentGUID"
#define MA_CEF_XML_TAG_MACHINE_NAME_NODE_NAME				"MachineName"
#define MA_CEF_XML_TAG_MAC_ADDRESS_NODE_NAME				"RawMACAddress"
#define MA_CEF_XML_TAG_IP_ADDRESS_NODE_NAME					"IPAddress"
#define MA_CEF_XML_TAG_AGENT_VERION_NODE_NAME				"AgentVersion"
#define MA_CEF_XML_TAG_OS_NAME_NODE_NAME					"OSName"
#define MA_CEF_XML_TAG_TIME_ZONE_BIAS_NODE_NAME				"TimeZoneBias"
#define MA_CEF_XML_TAG_USER_NAME_NODE_NAME					"UserName"


static ma_uint32_t severity_or_map[] = {0x00, 0x01, 0x03, 0x07, 0x0f};

/*static ma_error_t get_setting_from_ds(ma_context_t *context, const char *path, const char *key, ma_vartype_t type, ma_temp_buffer_t *buffer){
	union{
	ma_int64_t int_value;
	ma_buffer_t *str;
	}local_buffer = {0};
	ma_error_t rc = MA_OK;	
	
	switch(type){
		case MA_VARTYPE_INT64:
		{
			rc = ma_ds_get_int64(MA_CONTEXT_GET_DATASTORE(context), path, key, &local_buffer.int_value);
			if(MA_OK == rc){
				ma_temp_buffer_reserve(buffer, 22);						
				MA_MSC_SELECT(_snprintf, snprintf)((char*)ma_temp_buffer_get(buffer), ma_temp_buffer_capacity(buffer)-1, "%ld", local_buffer.int_value);
			}
		}
		break;
		case MA_VARTYPE_STRING:
		{
			rc = ma_ds_get_str(MA_CONTEXT_GET_DATASTORE(context), path, key, &local_buffer.str);
			if(MA_OK == rc){				
				const char *value = NULL;
				size_t size = 0;
				ma_buffer_get_string(local_buffer.str, &value, &size);
				ma_temp_buffer_reserve(buffer, size+1);
				if(value && size)					
					strcpy((char*)ma_temp_buffer_get(buffer), value);				
			}			
		}
		break;
	}	
	ma_buffer_release(local_buffer.str);
	return rc;
}*/



static ma_event_severity_t get_overall_severity(ma_uint32_t events_severity_or_value){
	switch(events_severity_or_value){
		case 0x00: return MA_EVENT_SEVERITY_INFORMATIONAL;
		case 0x01: return MA_EVENT_SEVERITY_WARNING;
		case 0x03: return MA_EVENT_SEVERITY_MINOR;
		case 0x07: return MA_EVENT_SEVERITY_MAJOR;
		default: return MA_EVENT_SEVERITY_CRITICAL;
	}
}

static int xml_node_add_attribute(char const *key, ma_variant_t *value, ma_xml_node_t *node){
	const char *value_buffer = NULL;
	size_t valuesize = 0;	
	ma_buffer_t *buffer = NULL;		
	(void)ma_variant_get_string_buffer(value, &buffer);
	(void)ma_buffer_get_string(buffer, &value_buffer, &valuesize);
	if(value_buffer)
		(void)ma_xml_node_attribute_set(node, key, value_buffer);	
	(void)ma_buffer_release(buffer);
	return 0;
}

static void xml_node_add_child(char const *key, ma_variant_t *value, void *cb_args, ma_bool_t *stop){
	ma_buffer_t *buffer = NULL;	
	ma_xml_node_t *parent = (ma_xml_node_t*)cb_args;
	ma_variant_get_string_buffer(value, &buffer);
	if(buffer){
		const char *value_buffer = NULL;
		size_t valuesize = 0;	
		ma_xml_node_t *child = NULL;
		(void)ma_buffer_get_string(buffer, &value_buffer, &valuesize);
		if(value_buffer){
			(void)ma_xml_node_create(parent, key, &child);
			(void)ma_xml_node_set_data(child, value_buffer);
		}
		(void)ma_buffer_release(buffer);	
	}
	else{
		ma_uint32_t int_value = 0;
		char buffer[10] = {0};		
		if(MA_OK == ma_variant_get_uint32(value, &int_value)){
			ma_xml_node_t *child = NULL;
			MA_MSC_SELECT(_snprintf, snprintf)(buffer, 10, "%u", int_value);
			(void)ma_xml_node_create(parent, key, &child);
			(void)ma_xml_node_set_data(child, buffer);
		}
	}
}

static void xml_node_add_common_fields(ma_xml_node_t *parent, ma_table_t *table){
	ma_variant_t *var = NULL;
	if( MA_OK == ma_table_get_value(table, MA_EVENT_BAG_COMMON_FIELDS_KEY, &var) ){				
		ma_table_t *common_fields_tbl = NULL;
		ma_variant_get_table(var, &common_fields_tbl);
		if(common_fields_tbl){
			ma_xml_node_t *node_created = NULL;
			ma_xml_node_create(parent, MA_EVENT_BAG_COMMON_FIELDS_KEY, &node_created);			
			if(node_created)
				ma_table_foreach(common_fields_tbl, xml_node_add_child, node_created);
			(void)ma_table_release(common_fields_tbl);
			
		}		
		(void)ma_variant_release(var);
	}
}

static void xml_node_add_custom_fields(ma_xml_node_t *parent, ma_table_t *table){
	ma_variant_t *var = NULL;
	if( MA_OK == ma_table_get_value(table, MA_EVENT_BAG_CUSTOM_FIELDS_KEY, &var) ){		
		ma_variant_t *custom_target = NULL;
		if( MA_OK == ma_table_get_value(table, MA_EVENT_BAG_CUSTOM_TARGET_KEY, &custom_target) ){
			ma_table_t *custom_fields_tbl = NULL;
			(void)ma_variant_get_table(var, &custom_fields_tbl);
			if(custom_fields_tbl){
				ma_xml_node_t *custom_filelds_node = NULL;
				(void)ma_xml_node_create(parent, MA_EVENT_BAG_CUSTOM_FIELDS_KEY, &custom_filelds_node);
				if(custom_filelds_node){
					xml_node_add_attribute(MA_EVENT_BAG_CUSTOM_TARGET_KEY, custom_target, custom_filelds_node);
					ma_table_foreach(custom_fields_tbl, xml_node_add_child, custom_filelds_node);
				}
				(void)ma_table_release(custom_fields_tbl);
			}		
			(void)ma_variant_release(custom_target);
		}		
		(void)ma_variant_release(var);
	}
}

static void event_node_add_event_info(char const *key, ma_variant_t *value, void *cb_args, ma_bool_t *stop){	
	if(strcmp(key, MA_EVENT_BAG_CUSTOM_FIELDS_KEY) && strcmp(key, MA_EVENT_BAG_COMMON_FIELDS_KEY) && strcmp(key, MA_EVENT_BAG_CUSTOM_TARGET_KEY))
		xml_node_add_child(key, value, cb_args, stop);
}

struct ma_xml_node_holder_s{
	ma_xml_node_t *software_info_node;
	ma_uint32_t events_severity_or_value;
};

static void software_node_add_event(size_t index, ma_variant_t *value, void *cb_args, ma_bool_t *stop){
	ma_table_t *event_info_tbl = NULL;	
	struct ma_xml_node_holder_s *software_info = (struct ma_xml_node_holder_s*)cb_args;
	ma_variant_get_table(value, &event_info_tbl);
	if(event_info_tbl){
		/*create event node first*/
		ma_xml_node_t *event_node = NULL;
		(void)ma_xml_node_create(software_info->software_info_node, MA_CEF_XML_TAG_EVENT_NODE_NAME, &event_node);		

		if(event_node){
			ma_variant_t *var = NULL;

			/*get the event id*/
			if( MA_OK == ma_table_get_value(event_info_tbl, MA_EVENT_EVENT_ID_KEY, &var) ){
				xml_node_add_child(MA_EVENT_EVENT_ID_KEY, var, event_node, stop);
				(void)ma_variant_release(var);
				var = NULL;
			}

			/*get the event severty*/
			if( MA_OK == ma_table_get_value(event_info_tbl, MA_EVENT_EVENT_SEVERITY_KEY, &var) ){
				ma_uint32_t severity = MA_EVENT_SEVERITY_INFORMATIONAL;
				(void)ma_variant_get_uint32(var, &severity);
				if(severity < 5)
					software_info->events_severity_or_value |= severity_or_map[severity];
				xml_node_add_child(MA_EVENT_EVENT_SEVERITY_KEY, var, event_node, stop);
				(void)ma_variant_release(var);
				var = NULL;
			}	

			if( MA_OK == ma_table_get_value(event_info_tbl, MA_EVENT_EVENT_GMTTIME_KEY, &var) ){
				xml_node_add_child(MA_EVENT_EVENT_GMTTIME_KEY, var, event_node, stop);
				(void)ma_variant_release(var);
				var = NULL;
			}

			if( MA_OK == ma_table_get_value(event_info_tbl, MA_EVENT_EVENT_LOCALTIME_KEY, &var) ){
				xml_node_add_child(MA_EVENT_EVENT_LOCALTIME_KEY, var, event_node, stop);
				(void)ma_variant_release(var);
				var = NULL;
			}

			/*Add event common fields to event node*/
			xml_node_add_common_fields(event_node, event_info_tbl);

			/*Add event custom fields to event node*/
			xml_node_add_custom_fields(event_node, event_info_tbl);

			(void)ma_table_release(event_info_tbl);		
		}
	}	
}

static ma_error_t software_node_add_software_info(struct ma_xml_node_holder_s *software_info, ma_table_t *software_info_tbl){
	ma_variant_t *var = NULL;

	/*add the product name*/
	if( MA_OK == ma_table_get_value(software_info_tbl, MA_EVENT_BAG_PRODUCT_NAME_KEY, &var) ){
		xml_node_add_attribute(MA_EVENT_BAG_PRODUCT_NAME_KEY, var, software_info->software_info_node);
		(void)ma_variant_release(var);
	}
	var = NULL;
	/*add the product version*/
	if( MA_OK == ma_table_get_value(software_info_tbl, MA_EVENT_BAG_PRODUCT_VERSION_KEY, &var) ){
		xml_node_add_attribute(MA_EVENT_BAG_PRODUCT_VERSION_KEY, var, software_info->software_info_node);
		(void)ma_variant_release(var);
	}
	var = NULL;
	/*add the product family*/
	if( MA_OK == ma_table_get_value(software_info_tbl, MA_EVENT_BAG_PRODUCT_FAMILY_KEY, &var) ){
		xml_node_add_attribute(MA_EVENT_BAG_PRODUCT_FAMILY_KEY, var, software_info->software_info_node);
		(void)ma_variant_release(var);
	}
	
	/*add the product info common fields*/
	xml_node_add_common_fields(software_info->software_info_node, software_info_tbl);

	/*add the product info custom fields*/
	xml_node_add_custom_fields(software_info->software_info_node, software_info_tbl);
	
	var = NULL;
	/*add all the product events*/
	if( MA_OK == ma_table_get_value(software_info_tbl, MA_EVENT_BAG_EVENTS_KEY, &var) ){
		ma_array_t *events = NULL;
		(void)ma_variant_get_array(var, &events);
		if(events){
			(void)ma_array_foreach(events, software_node_add_event, software_info);
			(void)ma_array_release(events);
		}
		(void)ma_variant_release(var);
	}	
	return MA_OK;
}

ma_error_t machine_node_add_machine_info(ma_context_t *context, ma_xml_node_t *machine_info_node){	
	ma_error_t rc = MA_ERROR_PRECONDITION;
	ma_temp_buffer_t buffer;
	const ma_network_system_property_t *net;	
	const char *value = NULL;
	const ma_misc_system_property_t *misc	= ma_system_property_misc_get(MA_CONTEXT_GET_SYSTEM_PROPERTY(context), MA_FALSE);
	ma_temp_buffer_init(&buffer);

    if(NULL != (value = ma_configurator_get_agent_guid(MA_CONTEXT_GET_CONFIGURATOR(context)))){
		ma_xml_node_t *child = NULL;	
		(void)ma_xml_node_create(machine_info_node, MA_CEF_XML_TAG_AGENT_GUID_NODE_NAME, &child);
		(void)ma_xml_node_set_data(child, (char*)value);
		if(misc){
			child = NULL;
			(void)ma_xml_node_create(machine_info_node, MA_CEF_XML_TAG_MACHINE_NAME_NODE_NAME, &child);
			(void)ma_xml_node_set_data(child, misc->computername);
            rc = MA_OK;
		}
		else
			rc = MA_ERROR_PRECONDITION;
	}
	value = NULL;

	if(MA_OK == rc){
		const ma_os_system_property_t *os = ma_system_property_os_get(MA_CONTEXT_GET_SYSTEM_PROPERTY(context), MA_FALSE);
		net = ma_system_property_network_get(MA_CONTEXT_GET_SYSTEM_PROPERTY(context), MA_FALSE);		
		if(net){
			ma_xml_node_t *child = NULL;             
            (void)ma_xml_node_create(machine_info_node, MA_CEF_XML_TAG_MAC_ADDRESS_NODE_NAME, &child);
            (void)ma_xml_node_set_data(child, net->mac_address_formated);              

            child = NULL;        
            (void)ma_xml_node_create(machine_info_node, MA_CEF_XML_TAG_IP_ADDRESS_NODE_NAME, &child);
            (void)ma_xml_node_set_data(child, net->ip_address_formated);			
		}
		value = NULL;
		if(MA_OK == ma_policy_settings_bag_get_str(MA_CONTEXT_GET_POLICY_SETTINGS_BAG(context), MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_VERSION_STR, MA_FALSE, &value, "5.0.0")){
			ma_xml_node_t *child = NULL;		
			(void)ma_xml_node_create(machine_info_node, MA_CEF_XML_TAG_AGENT_VERION_NODE_NAME, &child);
			(void)ma_xml_node_set_data(child, (char*)value);		
		}

		if(os){
			ma_xml_node_t *child = NULL;		
			(void)ma_xml_node_create(machine_info_node, MA_CEF_XML_TAG_OS_NAME_NODE_NAME, &child);
			(void)ma_xml_node_set_data(child, os->type);		
		}
		
		if(misc){
			ma_xml_node_t *child = NULL;		
			(void)ma_xml_node_create(machine_info_node, MA_CEF_XML_TAG_TIME_ZONE_BIAS_NODE_NAME, &child);
			(void)ma_xml_node_set_data(child, misc->timezonebios);		
			child = NULL;		
			(void)ma_xml_node_create(machine_info_node, MA_CEF_XML_TAG_USER_NAME_NODE_NAME, &child);
			(void)ma_xml_node_set_data(child, misc->username);
		}


	}	
	ma_temp_buffer_uninit(&buffer);
	return rc;
}

ma_error_t ma_event_xml_make(ma_context_t *context, ma_variant_t *payload, ma_bytebuffer_t *xml, ma_event_severity_t *highest_severity_event){
	if(context && payload && xml){
		ma_error_t rc = MA_ERROR_EVENT_MSG_SOFTWARE_INFO_NOTFOUND;				
		ma_table_t *software_info_table = NULL;		
		if( MA_OK == ma_variant_get_table(payload, &software_info_table) )
		{				
			ma_variant_t *events_var = NULL;
			rc = MA_ERROR_EVENT_MSG_EVENTS_NOTFOUND;
			if( MA_OK == ma_table_get_value(software_info_table, MA_EVENT_BAG_EVENTS_KEY, &events_var) )
			{
				ma_xml_t *event_xml = NULL;
				ma_xml_node_t *root_node = NULL;
				ma_xml_node_t *machine_info_node = NULL;
				ma_xml_node_t *software_info_node = NULL;
				
				ma_variant_release(events_var);
				rc = MA_ERROR_EVENT_XML_FORMATION_FAILED;
				if(MA_OK == ma_xml_create(&event_xml) &&
				   MA_OK == ma_xml_construct(event_xml) &&
				   MA_OK == ma_xml_node_create(ma_xml_get_node(event_xml), MA_CEF_XML_TAG_ROOT_NODE_NAME, &root_node) &&
				   MA_OK == ma_xml_node_create(root_node, MA_CEF_XML_TAG_MACHINE_INFO_NODE_NAME, &machine_info_node) &&
				   MA_OK == ma_xml_node_create(root_node, MA_CEF_XML_TAG_SOFTWARE_INFO_NODE_NAME, &software_info_node) )
				{
				   rc = MA_ERROR_EVENT_GET_MACHINE_INFO_FAILED;
				   if(MA_OK == machine_node_add_machine_info(context, machine_info_node) )
				   {  
					    struct ma_xml_node_holder_s holder;
						holder.software_info_node = software_info_node;
						holder.events_severity_or_value = 0;
						if(MA_OK == software_node_add_software_info(&holder, software_info_table) )
						{	
							if(	MA_OK == ma_xml_save_to_buffer(event_xml, xml) ) 
							{
								*highest_severity_event = get_overall_severity(holder.events_severity_or_value);
								rc = MA_OK;
							}
						}
				   }
				}
				(void)ma_xml_release(event_xml);			
			}			
			(void)ma_table_release(software_info_table);
		}		
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}


