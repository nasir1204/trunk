#include "ma/internal/ma_macros.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/utils/time/ma_time.h"
#include "ma/internal/utils/platform/ma_sys_apis.h"
#include "ma/internal/utils/threading/ma_thread.h"
#include "ma/internal/utils/datastructures/ma_bytebuffer.h"

#include "ma/datastore/ma_ds_mapper.h"
#include "ma_event_xml_generator.h"
#include "ma/internal/services/event/ma_event_persist.h"
#include "ma_event_xml_list.h"
#include "ma/ma_log.h"
#include "ma/internal/utils/configurator/ma_configurator.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>


#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "event"

#define MA_EVENT_DS_PATH				 "Events"
#define MA_IMMEDIATE_EVENT_DS_PATH		 "Events\\Immediate"

#define GET_EVENT_PATH(priority)		(MA_EVENT_PRIORITY_IMMEDIATE == priority ? MA_IMMEDIATE_EVENT_DS_PATH : MA_EVENT_DS_PATH)
 
#define MAX_RAND						10000

#define MA_CONTEXT_GET_EVENT_DATASTORE(context) (ma_configurator_get_datastore(MA_CONTEXT_GET_CONFIGURATOR(context)))

static void generate_event_xml_name(ma_event_priority_t priority, char *event_name, size_t size){
	ma_time_t time = {0, 0, 0, 0, 0, 0, 0}; 
	ma_thread_id_t thread_id;
	ma_time_get_localtime(&time);
	thread_id = ma_gettid();
	MA_MSC_SELECT(_snprintf, snprintf)
		(	event_name, size, "mc_%04d%02d%02d%02d%02d%02d%03d%04u%08X%s", 
			time.year, time.month, time.day, time.hour, time.minute, time.second, time.milliseconds, 
			(unsigned int)(ma_sys_rand()%MAX_RAND) + 1, thread_id,	
			MA_EVENT_PRIORITY_IMMEDIATE == priority ? ".txml" : ".xml"
		);
}

struct ma_event_persist_s{
	/* ma context */
	ma_context_t *context;

	/* Event Filter Version */
	char *evt_filter_version;
};

ma_error_t ma_event_persist_create(ma_context_t *context, ma_event_persist_t **event_ds){
	if(context && event_ds){
		*event_ds = (ma_event_persist_t*) calloc(1, sizeof(ma_event_persist_t) );
		if(*event_ds){
			(*event_ds)->context = context;
			return MA_OK;
		}
		return MA_ERROR_OUTOFMEMORY;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_event_persist_add_event_by_name(ma_event_persist_t *self, const char *event_xml_name, ma_event_priority_t priority, const char *event_xml_buffer, size_t size) {
    return (self && event_xml_name && event_xml_buffer && size) 
            ? ma_ds_set_blob( MA_CONTEXT_GET_EVENT_DATASTORE(self->context) , GET_EVENT_PATH(priority), event_xml_name, event_xml_buffer, size ) 
            : MA_ERROR_INVALIDARG;
}

ma_error_t ma_event_persist_add_event(ma_event_persist_t *self, ma_event_priority_t priority, const unsigned char *event_xml_buffer, size_t size){
	if(self && event_xml_buffer && size){
		ma_error_t rc = MA_OK;		
		char event_xml_name[101] = {0};
		generate_event_xml_name(priority, event_xml_name, 100);				
		if( MA_OK != ( rc = ma_ds_set_blob( MA_CONTEXT_GET_EVENT_DATASTORE(self->context) , GET_EVENT_PATH(priority), event_xml_name, event_xml_buffer, size ) ) )
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to persist event, error = %d", rc);				
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_event_persist_remove_event(ma_event_persist_t *self, ma_event_priority_t priority, char const *event_xmlname){
	if(self && event_xmlname){
		ma_error_t rc = MA_OK;		
		if ( MA_OK != (rc = ma_ds_remove(MA_CONTEXT_GET_EVENT_DATASTORE(self->context), GET_EVENT_PATH(priority), event_xmlname) ) )
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to delete event %s from datastore, error = %d", event_xmlname, rc);			
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}


ma_error_t ma_event_persist_set_event_filter_version(ma_event_persist_t *self, const char *evt_filter_version){
	if(self && evt_filter_version){
		ma_error_t rc = MA_OK;		
		if(MA_OK == (rc = ma_ds_set_str(MA_CONTEXT_GET_EVENT_DATASTORE(self->context), MA_EVENT_POLICIES_PATH_NAME_STR, MA_EVENT_KEY_FILTER_VERSION_STR, evt_filter_version, strlen(evt_filter_version) ) ) ){
			self->evt_filter_version = (char*)realloc(self->evt_filter_version, strlen(evt_filter_version) + 1 );
			if(self->evt_filter_version)
				MA_MSC_SELECT(_snprintf, snprintf)(self->evt_filter_version, strlen(evt_filter_version) + 1, "%s", evt_filter_version);		
		}
		else {
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to persist event filter version = %s, error = %d", evt_filter_version, rc);	
		}
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_event_persist_refresh_event_filter_version(ma_event_persist_t *self){
	if(self){
		ma_error_t rc = MA_OK;
		ma_buffer_t *event_filter = NULL;
		if(self->evt_filter_version){
			free(self->evt_filter_version);
			self->evt_filter_version = NULL;
		}
		if( MA_OK == (rc = ma_ds_get_str(MA_CONTEXT_GET_EVENT_DATASTORE(self->context), MA_EVENT_POLICIES_PATH_NAME_STR, MA_EVENT_KEY_FILTER_VERSION_STR, &event_filter))){
			size_t version_size = 0;
			const char *filter_version = NULL; 				
			if(MA_OK == (rc = ma_buffer_get_string(event_filter, &filter_version, &version_size))){
				self->evt_filter_version = (char*) calloc(strlen(filter_version) + 1, 1);
				if(self->evt_filter_version)
					MA_MSC_SELECT(_snprintf, snprintf)(self->evt_filter_version, strlen(filter_version) + 1, "%s", filter_version);											
			}
		}
		else{
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "No event filter version in datastore[1],%d", rc);	
		}
		(void)ma_buffer_release(event_filter);	
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_event_persist_get_event_filter_version(ma_event_persist_t *self, char **evt_filter_version, size_t *size){
	if(self && evt_filter_version && size){
		ma_error_t rc = MA_OK;
		*evt_filter_version = NULL; *size = 0;		
		if(!self->evt_filter_version)
			rc = ma_event_persist_refresh_event_filter_version(self);
		if(self->evt_filter_version){
			*evt_filter_version = self->evt_filter_version;
			*size = strlen(self->evt_filter_version);
		}
		else{
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "No event filter version in datastore.");	
		}
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_event_persist_set_disabled_event_list(ma_event_persist_t *self, const char *disabled_events){
	if(self && disabled_events){
		ma_error_t rc = MA_OK;		
		if ( MA_OK != (rc = ma_ds_set_str(MA_CONTEXT_GET_EVENT_DATASTORE(self->context), MA_EVENT_POLICIES_PATH_NAME_STR, MA_EVENT_KEY_FILTERED_LIST_STR , disabled_events, strlen(disabled_events) )))
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to persist disabled events, error = %d", rc);			
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}


static void coma_sep_events_to_array(ma_event_persist_t *self, char *coma_sep_events, ma_variant_t **disabled_events){	
	ma_array_t *disabled_events_arr = NULL;
	ma_variant_t *disabled_events_var = NULL;
	ma_array_create(&disabled_events_arr);
	ma_variant_create_from_array(disabled_events_arr, &disabled_events_var);
	if(disabled_events_arr && disabled_events_var){
		char *start_event_id = NULL;
		char *tmp = coma_sep_events;				
		while(*tmp)	{
			start_event_id = tmp;
			/*get one event id*/
			while(*tmp && ';' != *tmp) tmp++;						
			/*we found ';'*/
			if(*tmp){
				ma_uint32_t event_id_int = 0;
				ma_variant_t *event_id = NULL;
				*(tmp) = '\0'; tmp++;			
				event_id_int = atoi(start_event_id);
				(void)ma_variant_create_from_uint32(event_id_int, &event_id);
				if(event_id){					
					(void)ma_array_push(disabled_events_arr, event_id);
					(void)ma_variant_release(event_id);
				}
				else{
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to create event id variant.");						
				}

			} /*we didnt find ";" it so break.*/
			else{
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Malformed disabled event list.");						
				break;
			}
		}
	}
	else{		
		(void)ma_variant_release(disabled_events_var);									
		disabled_events_var = NULL;		
	}				
	(void)ma_array_release(disabled_events_arr);
	*disabled_events = disabled_events_var;
}

ma_error_t ma_event_persist_get_disabled_event_list(ma_event_persist_t *self, ma_variant_t **event_filter_list){
	if(self && event_filter_list){
		ma_error_t rc = MA_OK;		
		ma_buffer_t *disabled_events_buffer = NULL;			
		if ( MA_OK == (rc = ma_ds_get_str(MA_CONTEXT_GET_EVENT_DATASTORE(self->context), MA_EVENT_POLICIES_PATH_NAME_STR, MA_EVENT_KEY_FILTERED_LIST_STR, &disabled_events_buffer))){
			size_t size = 0;
			char *disabled_events = NULL; 				
			if(MA_OK == (rc = ma_buffer_get_string(disabled_events_buffer, &disabled_events, &size))){
				coma_sep_events_to_array(self, disabled_events, event_filter_list);
			}
			(void)ma_buffer_release(disabled_events_buffer);
		}
		else
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "No disabled event list in persist, error = %d", rc);					
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_event_persist_is_event_exist(ma_event_persist_t *self, ma_event_priority_t priority, ma_bool_t *is_exist){
	if(self && is_exist){
		ma_error_t rc = MA_OK;
		ma_ds_iterator_t *ds_iter = NULL;
		*is_exist = MA_FALSE;
		if ( MA_OK == (rc = ma_ds_iterator_create(MA_CONTEXT_GET_EVENT_DATASTORE(self->context), 0, GET_EVENT_PATH(priority), &ds_iter) ) ){
			ma_buffer_t *key = NULL;
			if (MA_OK == ma_ds_iterator_get_next(ds_iter, &key) ){
				*is_exist = MA_TRUE;
				(void)ma_buffer_release(key);
			}
			(void)ma_ds_iterator_release(ds_iter);
		}
		else{			
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "No events found, rc = <%d>", rc);			
		}
		return rc;
	}
	return MA_ERROR_INVALIDARG;	
}

#define XML_START_TAG "<?xml"

static ma_error_t read_event_buffer(ma_ds_t *datastore, ma_event_priority_t priority, const char *key, ma_buffer_t **xml_buffer){
	ma_error_t rc = MA_ERROR_APIFAILED;
	ma_buffer_t *value = NULL;
	if(MA_OK == ma_ds_get_blob(datastore, GET_EVENT_PATH(priority), key, &value)){
		const char *event_xml = NULL;
		size_t event_xml_size = 0 ;

		if(value && MA_OK == ma_buffer_get_string(value, &event_xml, &event_xml_size) && event_xml_size){
			if(0 == strncmp(event_xml, XML_START_TAG, strlen(XML_START_TAG))){
				/*event in ds.*/
				rc = MA_OK;
				ma_buffer_add_ref(*xml_buffer = value);				
			}
		}	
		(void)ma_buffer_release(value);
	}
	return rc;
}

static ma_error_t read_legacy_event_buffer(ma_ds_t *datastore, ma_event_priority_t priority, const char *key, ma_buffer_t **xml_path, ma_buffer_t **xml_buffer){
	struct stat st = {0};
	ma_error_t rc = MA_ERROR_APIFAILED;
	ma_buffer_t *value = NULL;
	if(MA_OK == ma_ds_get_blob(datastore, GET_EVENT_PATH(priority), key, &value)){
		const char *event_path = NULL;
		size_t event_xml_path_size = 0 ;
		if(value && MA_OK == ma_buffer_get_string(value, &event_path, &event_xml_path_size) && event_xml_path_size){
			if(0 == stat(event_path, &st)){
				FILE *fp = NULL;
				if((fp = fopen(event_path, "r"))) {
					char *data =NULL;
					
					if((data = (char*)calloc(st.st_size+1, sizeof(char)))) {

						size_t read_till_now = 0;
						size_t total = st.st_size;
						while(read_till_now < total){
							size_t read_now = fread((data + read_till_now), sizeof(char), (total - read_till_now), fp);
							read_till_now += read_now;
						}

						if(read_till_now == total && MA_OK == ma_buffer_create(xml_buffer, st.st_size)){
							rc = MA_OK;
							(void)ma_buffer_add_ref(*xml_path = value);
							(void)ma_buffer_set(*xml_buffer, data, st.st_size);
						}				
						free(data);
					} 
					fclose(fp);					
				}					
			}			
		}	
		(void)ma_buffer_release(value);
	}
	return rc;
}

static ma_error_t event_persist_get_event_xml_list(ma_event_persist_t *self, ma_event_priority_t priority, size_t *batch_size, ma_event_xml_list_t *xml_list){
	ma_error_t rc = MA_OK;
	ma_ds_iterator_t *ds_iter = NULL;
	if ( MA_OK == (rc = ma_ds_iterator_create(MA_CONTEXT_GET_EVENT_DATASTORE(self->context), 0, GET_EVENT_PATH(priority), &ds_iter) ) )
	{
		ma_buffer_t *key = NULL;
		while (*batch_size && MA_OK == ma_ds_iterator_get_next(ds_iter, &key)) 
		{
            const char *event_name = NULL ;
            size_t len = 0 ;
            (void)ma_buffer_get_string(key, &event_name, &len) ;
            if(event_name)
			{
				ma_buffer_t *xml_buffer = NULL;
				ma_buffer_t *xml_path = NULL;

				if(MA_OK == read_event_buffer(MA_CONTEXT_GET_EVENT_DATASTORE(self->context), priority, event_name, &xml_buffer) || MA_OK == read_legacy_event_buffer(MA_CONTEXT_GET_EVENT_DATASTORE(self->context), priority, event_name, &xml_path, &xml_buffer))
				{
					const char *event_xml = NULL;
					size_t event_xml_size = 0 ;
				
					if(xml_buffer && MA_OK == ma_buffer_get_string(xml_buffer, &event_xml, &event_xml_size))
					{					
						if((int)(xml_list->max_buffer_size - event_xml_size) <= 0)
						{
							MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Enough xml added for the current spipe package.");	
							*batch_size = 0;						
						}
						if(*batch_size)
						{
							ma_event_xml_info_node_t *node = (ma_event_xml_info_node_t*) calloc(1, sizeof(ma_event_xml_info_node_t) ) ;						
							if(node)
							{
								ma_buffer_add_ref(node->name = key);								
								ma_buffer_add_ref(node->xml = xml_buffer);
								ma_buffer_add_ref(node->path = xml_path);								

								node->priority = priority;
								ma_event_xml_list_add(xml_list, node);		

								xml_list->max_buffer_size -= event_xml_size;
								*batch_size -= 1;
							}
							else
							{
								MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to create event list node.");	
							}
						}												
					}
					else
					{
						MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Invalid event %s xml.", event_name);	
					}
					(void)ma_buffer_release(xml_buffer);
					(void)ma_buffer_release(xml_path);
				}
				else
				{
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_CRITICAL, "Failed to get event = %s xml from persist.", event_name);	
				}
			}
			(void)ma_buffer_release(key) ;
        }
        (void)ma_ds_iterator_release(ds_iter);
	}
	else
	{
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to create ds iterator, error = %d", rc);			
	}
	return rc;
}


ma_error_t ma_event_persist_get_event_xml_list(ma_event_persist_t *self, ma_event_priority_t priority, size_t batch_size, ma_event_xml_list_t *xml_list){
	if(self && xml_list){
		ma_error_t rc = MA_OK;
		size_t xmls_count = batch_size;
		switch(priority){			
			case MA_EVENT_PRIORITY_NORMAL:
				rc = event_persist_get_event_xml_list(self, MA_EVENT_PRIORITY_NORMAL, &xmls_count, xml_list);			
				break;
			case MA_EVENT_PRIORITY_IMMEDIATE:
				rc = event_persist_get_event_xml_list(self, MA_EVENT_PRIORITY_IMMEDIATE, &xmls_count, xml_list);								
				break;
			default:
				rc = event_persist_get_event_xml_list(self, MA_EVENT_PRIORITY_IMMEDIATE, &xmls_count, xml_list);								
				rc = event_persist_get_event_xml_list(self, MA_EVENT_PRIORITY_NORMAL, &xmls_count, xml_list);			
		}

		if(MA_OK == rc) {
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Successfully retrieved the event xmls from datastore");			
		}
		else {
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to get the event xmls from datastore, error = %d", rc);					
		}
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_event_persist_remove_event_xml(ma_event_persist_t *self, ma_event_xml_list_t *xml_list){
	if(self && xml_list){
		/*remove db entries*/
		ma_error_t rc = MA_OK;
		ma_event_xml_info_node_t *node = NULL;
		if(MA_OK == (rc = ma_db_transaction_begin(ma_configurator_get_database(MA_CONTEXT_GET_CONFIGURATOR(self->context))))){
			
			MA_SLIST_FOREACH(xml_list, event_xml, node){
				const char *event_name = NULL;
				size_t event_name_size = 0;
				ma_buffer_get_string(node->name, &event_name, &event_name_size);
				if(event_name)
					ma_event_persist_remove_event(self, node->priority, event_name);						
			}
			ma_db_transaction_end(ma_configurator_get_database(MA_CONTEXT_GET_CONFIGURATOR(self->context)));
		}
		else {
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_CRITICAL, "Failed in ma_db_transaction_begin, error = %d", rc);					
		}

		node = NULL;
		MA_SLIST_FOREACH(xml_list, event_xml, node){
			if(node->path){
				const char *event_path = NULL;
				size_t event_path_size = 0;
				(void)ma_buffer_get_string(node->path, &event_path, &event_path_size);
				if(event_path)
					unlink(event_path);						
			}			
		}
		
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_event_persist_release(ma_event_persist_t *self){
	if(self){
		if(self->evt_filter_version)
			free(self->evt_filter_version);
		free(self);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}



