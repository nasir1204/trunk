#ifndef MA_EVENT_XML_GENERATOR_H_INCLUDED
#define MA_EVENT_XML_GENERATOR_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_variant.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/defs/ma_event_defs.h"
#include "ma/event/ma_event.h"

MA_CPP(extern "C" {)

ma_error_t ma_event_xml_make(ma_context_t *context, ma_variant_t *payload, ma_bytebuffer_t *xml, ma_event_severity_t *highest_severity_event);


MA_CPP(})

#endif
