#include "ma/internal/services/recorder/ma_recorder_service.h"
#include "ma/internal/utils/context/ma_context.h"
#include "ma/internal/defs/ma_recorder_defs.h"
#include "ma/internal/services/ma_service.h"
#include "ma/ma_log.h"
#include "ma/ma_message.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/ma_variant.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/sensor/ma_sensor_msg.h"
#include "ma/sensor/ma_sensor_utils.h"
#include "ma/internal/utils/recorder/ma_recorder.h"
#include "ma/internal/services/scheduler/ma_task_defs.h"
#include "ma/internal/services/scheduler/ma_task_internal.h"
#include "ma/internal/utils/context/ma_context.h"
#include "ma/internal/utils/booker/ma_booker.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

const char base_path[] = "/recorder/";

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "recorder service"

MA_CPP(extern "C" {)
MA_CPP(})

ma_logger_t *logger = NULL;

struct ma_recorder_service_s {
    ma_service_t            service_base;
    ma_context_t            *context;
    ma_msgbus_server_t      *server;
    ma_logger_t             *logger;
    ma_msgbus_subscriber_t  *subscriber;
    ma_recorder_t             *recorder;
};


static ma_error_t recorder_subscriber_cb(const char *topic, ma_message_t *message, void *cb_data);
static ma_error_t ma_recorder_service_cb(ma_msgbus_server_t *server, ma_message_t *msg, void *cb_data, ma_msgbus_client_request_t *c_request);

static ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint);
static ma_error_t service_start(ma_service_t *service);
static ma_error_t service_stop(ma_service_t *service);
static void service_release(ma_service_t *service);

static const ma_service_methods_t service_methods = {
    &service_configure,
    &service_start,
    &service_stop,
    &service_release
};

ma_error_t ma_recorder_service_create(const char *service_name, ma_service_t **service) {
    if(service_name && service) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;

        if((*service = (ma_service_t*)calloc(1, sizeof(ma_recorder_service_t)))) {
            ma_service_init(*service, &service_methods, service_name, service);
            return MA_OK;
        }
        return err ;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_recorder_service_release(ma_recorder_service_t *self) {
    if(self) {
        ma_error_t err = MA_OK;
        if(self->server && (MA_OK != (err = ma_msgbus_server_release(self->server)))) {
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "msgbus server release failed");
        }
        (void)ma_msgbus_subscriber_release(self->subscriber);
        free(self) ;
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_bool_t check_context_validity(ma_context_t *context){	
    /*Will see if we need this*/
    return MA_TRUE;
}

ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint) {
    if(service && context) {
        ma_error_t rc = MA_OK;
        if(MA_SERVICE_CONFIG_NEW == hint && check_context_validity(context)) {
            ma_configurator_t *configurator = MA_CONTEXT_GET_CONFIGURATOR(context);
            ma_recorder_service_t *self = (ma_recorder_service_t *)service;
            ma_msgbus_t *msgbus = MA_CONTEXT_GET_MSGBUS(context);

            self->context = context;
            logger = self->logger = MA_CONTEXT_GET_LOGGER(context);

            if(!ma_configurator_get_recorder_datastore(configurator))
                 MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_INFO, "recorder datastore does not exist");
            if(MA_OK == (rc = ma_recorder_create(msgbus, ma_configurator_get_recorder_datastore(configurator), MA_RECORDER_BASE_PATH_STR, &self->recorder))) {
                (void)ma_recorder_set_logger(self->logger);
                if(MA_OK == (rc = ma_msgbus_subscriber_create(MA_CONTEXT_GET_MSGBUS(context), &self->subscriber))) {
                    (void)ma_msgbus_subscriber_setopt(self->subscriber, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                    (void)ma_msgbus_subscriber_setopt(self->subscriber, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
                    if(MA_OK == (rc = ma_msgbus_server_create(msgbus, MA_RECORDER_SERVICE_NAME_STR, &self->server))) {                        
                        (void)ma_msgbus_server_setopt(self->server, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
                        if(MA_OK == (rc = ma_msgbus_server_setopt(self->server, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC))) {
                            /*TODO - No need to add the recorder object in the context if we can use the recorder client API's */
                            ma_context_add_object_info(context, MA_OBJECT_RECORDER_NAME_STR, (void *const *)&self->recorder);
                            MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_DEBUG, "recorder service configuration successful");
                            return MA_OK;
                        }
                        ma_msgbus_server_release(self->server);
                    }
                    ma_msgbus_subscriber_release(self->subscriber);
                }
            }
            else
                MA_LOG(self->logger, MA_LOG_SEV_ERROR, "recorder creation failed, last error(%d)", rc);
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t service_start(ma_service_t *service) {
    if(service) {
        ma_error_t err = MA_OK;
        ma_recorder_service_t *self = (ma_recorder_service_t *)service ;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);
        
        if(MA_OK == (err = ma_recorder_start(self->recorder))) {
            if(MA_OK == (err = ma_msgbus_server_start(((ma_recorder_service_t*)service)->server, ma_recorder_service_cb, service))) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "recorder service started successfully");
                if(MA_OK == (err = ma_msgbus_subscriber_register(((ma_recorder_service_t*)service)->subscriber,  "ma.task.*", recorder_subscriber_cb, service))) {
                    MA_LOG(((ma_recorder_service_t*)service)->logger, MA_LOG_SEV_DEBUG, "recorder subscriber registered successfully");
                    return MA_OK ;
                } 
                else 
                    MA_LOG(((ma_recorder_service_t*)service)->logger, MA_LOG_SEV_ERROR, "recorder service failed to subscribe topic, errocode(%d)", err);
                ma_msgbus_server_stop(((ma_recorder_service_t*)service)->server);
            }
            else
                MA_LOG(logger, MA_LOG_SEV_CRITICAL, "recorder service start failed, last error(%d)", err);
            ma_recorder_stop(self->recorder);
        }
        else
            MA_LOG(logger, MA_LOG_SEV_CRITICAL, "recorder service start failed, last error(%d)", err);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_recorder_service_get_version(ma_uint32_t *version) {
    if(version) {
        *version = 0x05020100; //TODO fetch version 
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_recorder_service_start(ma_recorder_service_t *service) {
    return service_start((ma_service_t*)service);
}

ma_error_t ma_recorder_service_stop(ma_recorder_service_t *service) {
    return service_stop((ma_service_t*)service);
}

ma_error_t service_stop(ma_service_t *service) {
    if(service) {
        ma_error_t err = MA_OK;
        ma_recorder_service_t *self = (ma_recorder_service_t *)service;

        if(MA_OK != (err = ma_msgbus_subscriber_unregister(((ma_recorder_service_t*)service)->subscriber))) {
            MA_LOG(((ma_recorder_service_t*)service)->logger, MA_LOG_SEV_ERROR, "recorder service unregistering subscriber failed, last error(%d)", err);
        }

        if(MA_OK == (err = ma_msgbus_server_stop(self->server))) {
            if(MA_OK != (err = ma_recorder_stop(self->recorder))) 
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_CRITICAL, "recorder stop failed, last error(%d)", err);
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO, "recorder service stopped successfully");
        }
        else
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_CRITICAL, "recorder service stop failed, last error(%d)", err);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

void service_release(ma_service_t *service) {
    if(service) {
        ma_recorder_service_release((ma_recorder_service_t *)service) ;
        return ;
    }
    return ;
}

ma_error_t ma_recorder_service_get_recorder(ma_recorder_service_t *service, ma_recorder_t **recorder) {
    if(service && recorder) {
        *recorder = service->recorder;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t order_create_handler(ma_recorder_service_t *service, ma_message_t *req_msg, ma_message_t *rsp_msg) {
    if(service && req_msg && rsp_msg) {
        ma_error_t err = MA_OK;
        const char *recorder_id = NULL;
        char *status = MA_RECORDER_ID_NOT_EXIST;
        ma_variant_t *payload = NULL;
        char error_code[MA_MAX_LEN] = {0};
        ma_recorder_info_t *info = NULL;

        if(MA_OK == (err = ma_message_get_property(req_msg, MA_RECORDER_ID, &recorder_id))) {
            (void)ma_message_set_property(rsp_msg, MA_RECORDER_ID, recorder_id);
            MA_LOG(service->logger, MA_LOG_SEV_TRACE, "recorder id(%s) create order processing", recorder_id);
            if(MA_OK == (err = ma_message_get_payload(req_msg, &payload))) {
                if(MA_OK == (err = ma_recorder_info_convert_from_variant(payload, &info))) {
                    if(MA_OK == (err = ma_recorder_add_variant(service->recorder, recorder_id, payload))) {
                        status = MA_RECORDER_ID_WRITTEN;
                        MA_LOG(service->logger, MA_LOG_SEV_TRACE, "recorder_id(%s) written into DB successfully", recorder_id);

                    }
                    (void)ma_recorder_info_release(info);
                }
                (void)ma_variant_release(payload);
            }
        }
        if(MA_OK != err)
            MA_LOG(service->logger, MA_LOG_SEV_CRITICAL, "recorder order creation failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(rsp_msg, MA_RECORDER_STATUS, status);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t order_modify_handler(ma_recorder_service_t *service, ma_message_t *req_msg, ma_message_t *rsp_msg) {
    if(service && req_msg && rsp_msg) {
        ma_error_t err = MA_OK;
        const char *recorder_id = NULL;
        char *status = MA_RECORDER_ID_NOT_EXIST;
        ma_variant_t *payload = NULL;
        char error_code[MA_MAX_LEN] = {0};
        ma_recorder_info_t *info = NULL;

        if(MA_OK == (err = ma_message_get_property(req_msg, MA_RECORDER_ID, &recorder_id))) {
            (void)ma_message_set_property(rsp_msg, MA_RECORDER_ID, recorder_id);
            MA_LOG(service->logger, MA_LOG_SEV_TRACE, "recorder id(%s) order modify request  processing", recorder_id);
            if(MA_OK == (err = ma_message_get_payload(req_msg, &payload))) {
                if(MA_OK == (err = ma_recorder_info_convert_from_variant(payload, &info))) {
                    if(MA_OK == (err = ma_recorder_add_variant(service->recorder, recorder_id, payload))) {
                        status = MA_RECORDER_ID_WRITTEN;
                        MA_LOG(service->logger, MA_LOG_SEV_TRACE, "recorder_id(%s) written into DB successfully", recorder_id);

                    }
                    (void)ma_recorder_info_release(info);
                }
                (void)ma_variant_release(payload);
            }
        }
        if(MA_OK != err)
            MA_LOG(service->logger, MA_LOG_SEV_CRITICAL, "recorder order modify failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(rsp_msg, MA_RECORDER_STATUS, status);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t order_delete_handler(ma_recorder_service_t *service, ma_message_t *req_msg, ma_message_t *rsp_msg) {
    if(service && req_msg && rsp_msg) {
        ma_error_t err = MA_OK;
        const char *recorder_id = NULL;
        char *status = MA_RECORDER_ID_NOT_EXIST;
        char error_code[MA_MAX_LEN] = {0};

        if(MA_OK == (err = ma_message_get_property(req_msg, MA_RECORDER_ID, &recorder_id))) {
            (void)ma_message_set_property(rsp_msg, MA_RECORDER_ID, recorder_id);
            MA_LOG(service->logger, MA_LOG_SEV_TRACE, "recorder id(%s) order delete request processing", recorder_id);
            if(MA_OK == (err = ma_recorder_delete(service->recorder, recorder_id))) {
                status = MA_RECORDER_ID_DELETED;
                MA_LOG(service->logger, MA_LOG_SEV_TRACE, "recorder id(%s) deleted successfully", recorder_id);
            } else {
                MA_LOG(service->logger, MA_LOG_SEV_ERROR, "recorder id(%s) order delete failed, last error(%d)", recorder_id, err);
                status = MA_RECORDER_ID_NOT_EXIST;
            }
        }
        if(MA_OK != err)
            MA_LOG(service->logger, MA_LOG_SEV_CRITICAL, "recorder order delete failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(rsp_msg, MA_RECORDER_STATUS, status);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t order_update_status_handler(ma_recorder_service_t *service, ma_message_t *req_msg, ma_message_t *rsp_msg) {
    if(service && req_msg && rsp_msg) {
        ma_error_t err = MA_OK;
        const char *recorder_id = NULL;
        char *status = MA_RECORDER_ID_NOT_EXIST;
        char error_code[MA_MAX_LEN] = {0};
        const char *recorder_status = NULL;

        if(MA_OK == (err = ma_message_get_property(req_msg, MA_RECORDER_ID, &recorder_id)) &&
           MA_OK == (err = ma_message_get_property(req_msg, MA_RECORDER_STATUS, &recorder_status))) {
            ma_recorder_info_t *info = NULL;

            (void)ma_message_set_property(rsp_msg, MA_RECORDER_ID, recorder_id);
            MA_LOG(service->logger, MA_LOG_SEV_TRACE, "recorder id(%s) order update request processing", recorder_id);
            if(MA_OK == (err = ma_recorder_get(service->recorder, recorder_id, &info))) {
                status = MA_RECORDER_ID_EXIST;
                //TODO : update status
                MA_LOG(service->logger, MA_LOG_SEV_TRACE, "recorder id(%s) deleted successfully", recorder_id);
                (void)ma_recorder_info_release(info);
            } else {
                MA_LOG(service->logger, MA_LOG_SEV_ERROR, "recorder id(%s) order delete failed, last error(%d)", recorder_id, err);
                status = MA_RECORDER_ID_NOT_EXIST;
            }
        }
        if(MA_OK != err)
            MA_LOG(service->logger, MA_LOG_SEV_CRITICAL, "recorder order delete failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(rsp_msg, MA_RECORDER_STATUS, status);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t recorder_delete_handler(ma_recorder_service_t *service, ma_message_t *req_msg, ma_message_t *rsp_msg) {
    if(service && req_msg && rsp_msg) {
        ma_error_t err = MA_OK;
        const char *recorder_id = NULL;
        char *status = MA_RECORDER_ID_NOT_EXIST;
        char error_code[MA_MAX_LEN] = {0};

        if(MA_OK == (err = ma_message_get_property(req_msg, MA_RECORDER_ID, &recorder_id))) {
            (void)ma_message_set_property(rsp_msg, MA_RECORDER_ID, recorder_id);
            MA_LOG(service->logger, MA_LOG_SEV_TRACE, "recorder id(%s) delete request processing", recorder_id);
            if(MA_OK == (err = ma_recorder_delete(service->recorder, recorder_id))) {
                status = MA_RECORDER_ID_DELETED;
                MA_LOG(service->logger, MA_LOG_SEV_TRACE, "recorder id(%s) deleted successfully", recorder_id);
            } else {
                MA_LOG(service->logger, MA_LOG_SEV_ERROR, "recorder id(%s) order delete failed, last error(%d)", recorder_id, err);
                status = MA_RECORDER_ID_NOT_EXIST;
            }
        }
        if(MA_OK != err)
            MA_LOG(service->logger, MA_LOG_SEV_CRITICAL, "recorder order delete failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(rsp_msg, MA_RECORDER_STATUS, status);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t recorder_register_handler(ma_recorder_service_t *service, ma_message_t *recorder_req_msg, ma_message_t *recorder_rsp_msg) {
    if(service && recorder_req_msg && recorder_rsp_msg) {
        ma_error_t err = MA_OK;
        const char *recorder_id = NULL;
        char *recorder_reg_status = MA_TASK_REPLY_STATUS_FAILED;
        ma_variant_t *recorder_variant = NULL;
        char error_code[MA_MAX_LEN] = {0};


        (void)ma_message_set_property(recorder_rsp_msg, MA_RECORDER_ID, recorder_id);
        if(MA_OK == (err = ma_message_get_property(recorder_req_msg, MA_RECORDER_ID, &recorder_id))) {
            recorder_reg_status = MA_RECORDER_ID_NOT_EXIST;
            if(MA_OK == ma_message_get_payload(recorder_req_msg, &recorder_variant)) {
                if(MA_OK == (err = ma_recorder_add_variant(service->recorder, recorder_id, recorder_variant))) {
                    MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "recorder id(%s) written into DB successfully", recorder_id);
                    recorder_reg_status = MA_RECORDER_ID_WRITTEN;
                }
                (void)ma_variant_release(recorder_variant);
            }
        }
        if(MA_OK != err)
            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_CRITICAL, "recorder id register failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(recorder_rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(recorder_rsp_msg, MA_RECORDER_STATUS, recorder_reg_status);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t recorder_deregister_handler(ma_recorder_service_t *service, ma_message_t *recorder_req_msg, ma_message_t *recorder_rsp_msg) {
    if(service && recorder_req_msg && recorder_rsp_msg) {
        ma_error_t err = MA_OK;
        const char *recorder_id = NULL;
        char *recorder_reg_status = MA_RECORDER_ID_NOT_EXIST;
        char error_code[MA_MAX_LEN] = {0};


        (void)ma_message_set_property(recorder_rsp_msg, MA_RECORDER_ID, recorder_id);
        if(MA_OK == (err = ma_message_get_property(recorder_req_msg, MA_RECORDER_ID, &recorder_id))) {
            if(MA_OK == (err = ma_recorder_delete(service->recorder, recorder_id))) {
                recorder_reg_status = MA_RECORDER_ID_DELETED;
                MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "recorder id(%s) deleted successfully", recorder_id);
            }
        }
        if(MA_OK != err)
            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_CRITICAL, "recorder id deregister failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(recorder_rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(recorder_rsp_msg, MA_RECORDER_STATUS, recorder_reg_status);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t recorder_get_json_handler(ma_recorder_service_t *service, ma_message_t *recorder_req_msg, ma_message_t *recorder_rsp_msg) {
    if(service && recorder_req_msg && recorder_rsp_msg) {
        ma_error_t err = MA_OK;
        const char *recorder_id = NULL;
        char *recorder_reg_status = MA_RECORDER_ID_NOT_EXIST;
        ma_recorder_info_t *info = NULL;
        ma_variant_t *recorder_variant = NULL;
        char error_code[MA_MAX_LEN] = {0};


        (void)ma_message_set_property(recorder_rsp_msg, MA_RECORDER_ID, recorder_id);
        if(MA_OK == (err = ma_message_get_property(recorder_req_msg, MA_RECORDER_ID, &recorder_id))) {
            if(MA_OK == (err = ma_recorder_get(service->recorder, recorder_id, &info))) {
                ma_json_t *json = NULL;

                MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "recorder id(%s) get successfully", recorder_id);
                if(MA_OK == (err = ma_recorder_info_convert_to_json(info, &json))) {
                    ma_bytebuffer_t *buffer = NULL;

                    ma_bytebuffer_create(MA_MAX_BUFFER_LEN, &buffer);
                    if(MA_OK == (err = ma_json_get_data(json, buffer))) {
                        if(MA_OK == (err = ma_variant_create_from_string((const char*)ma_bytebuffer_get_bytes(buffer), ma_bytebuffer_get_size(buffer), &recorder_variant))) {
                            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "recorder json output(%s) len(%u)", ma_bytebuffer_get_bytes(buffer), ma_bytebuffer_get_size(buffer));
                            recorder_reg_status = MA_RECORDER_ID_EXIST;
                            (void)ma_message_set_payload(recorder_rsp_msg, recorder_variant);
                            (void)ma_variant_release(recorder_variant);
                        } else
                            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_ERROR, "recorder variant creation failed, last error(%d)", err);
                    }
                }
                (void)ma_recorder_info_release(info);
            }
        }
        if(MA_OK != err)
            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_CRITICAL, "recorder id get failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(recorder_rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(recorder_rsp_msg, MA_RECORDER_STATUS, recorder_reg_status);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_recorder_service_cb(ma_msgbus_server_t *server, ma_message_t *msg, void *cb_data, ma_msgbus_client_request_t *c_request) {
    if(server && msg && cb_data) {
        ma_error_t err = MA_OK;
        const char *msg_type = NULL;
        ma_message_t *response = NULL;
        ma_recorder_service_t *service = (ma_recorder_service_t*)cb_data;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(service->context);

        MA_LOG(logger, MA_LOG_SEV_DEBUG, "Recorder service callback invoked");
        if(MA_OK == (err = ma_message_create(&response))) {
            if(MA_OK == (err = ma_message_get_property(msg, MA_RECORDER_SERVICE_MSG_TYPE, &msg_type))) {
                if(!strcmp(msg_type, MA_RECORDER_SERVICE_MSG_ORDER_CREATE_TYPE)) {
                    if(MA_OK == (err = order_create_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client order create request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_RECORDER_SERVICE_MSG_ORDER_MODIFY_TYPE)) {
                    if(MA_OK == (err = order_modify_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client order modify request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_RECORDER_SERVICE_MSG_REGISTER)) {
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "Recorder register request received");
                    if(MA_OK == (err = recorder_register_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client order modify request failed, last error(%d)", err);
                        }
                    } else
                        MA_LOG(logger, MA_LOG_SEV_ERROR, "recorder register request failed, last error(%d)", err);
                } else if(!strcmp(msg_type, MA_RECORDER_SERVICE_MSG_DEREGISTER)) {
                    if(MA_OK == (err = recorder_deregister_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client order modify request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_RECORDER_SERVICE_MSG_ORDER_DELETE_TYPE)) {
                    if(MA_OK == (err = order_delete_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client order delete request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_RECORDER_SERVICE_MSG_ORDER_UPDATE_STATUS_TYPE)) {
                    if(MA_OK == (err = order_update_status_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client order update request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_RECORDER_SERVICE_MSG_RECORDER_DELETE_TYPE)) {
                    if(MA_OK == (err = recorder_delete_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client order update request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_RECORDER_SERVICE_MSG_GET_JSON_TYPE)) {
                    if(MA_OK == (err = recorder_get_json_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client get json request failed, last error(%d)", err);
                        }
                    }
                } else {
                    if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, MA_ERROR_SCHEDULER_INVALID_OP_TYPE, response))) {
                        MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client request failed, last error(%d)", err);
                    }
                }
            } else {
                if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err = MA_ERROR_SCHEDULER_TASK_TYPE_NOT_SET, response))) {
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client request failed, last error(%d)", err);
                }
            }
            if(MA_OK != ma_message_release(response)) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client request message release failed");
            }
        }
        return err;
    }
    return MA_OK;
}

static ma_error_t add_pending_complete(ma_recorder_info_t *recorder, const char *user_contact, const char *id, ma_bool_t is_complete) {
    if(recorder && user_contact && id) {
        ma_error_t err = MA_OK;

        if(!is_complete) {
            if(MA_OK == (err = ma_recorder_info_add_pending_order(recorder, user_contact, id))) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "recorder user(%s) booking id(%s) added in pending order map", user_contact, id);
            } else {
                MA_LOG(logger, MA_LOG_SEV_ERROR, "recorder user(%s) booking id(%s) added in pending order map failed last error(%d)", user_contact, id, err);
            }
        } else {
            if(MA_OK == (err = ma_recorder_info_add_completed_order(recorder, user_contact, id))) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "recorder user(%s) booking id(%s) added in completed order map", user_contact, id);
            } else {
                MA_LOG(logger, MA_LOG_SEV_ERROR, "recorder user(%s) booking id(%s) added in completed order map failed last error(%d)", user_contact, id, err);
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t remove_pending_complete(ma_recorder_info_t *recorder, const char *user_contact, const char *id, ma_bool_t is_complete) {
    if(recorder && user_contact && id) {
        ma_error_t err = MA_OK;

        if(!is_complete) {
            if(MA_OK == (err = ma_recorder_info_remove_pending_order(recorder, user_contact, id))) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "recorder user(%s) booking id(%s) added in pending order map", user_contact, id);
            } else {
                MA_LOG(logger, MA_LOG_SEV_ERROR, "recorder user(%s) booking id(%s) added in pending order map failed last error(%d)", user_contact, id, err);
            }
        } else {
            if(MA_OK == (err = ma_recorder_info_remove_completed_order(recorder, user_contact, id))) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "recorder user(%s) booking id(%s) added in completed order map", user_contact, id);
            } else {
                MA_LOG(logger, MA_LOG_SEV_ERROR, "recorder user(%s) booking id(%s) added in completed order map failed last error(%d)", user_contact, id, err);
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t recorder_add_record(ma_recorder_t *recorder, const char *contact, const char *booking_id, ma_bool_t is_complete) {
    if(contact && booking_id) {
        ma_error_t err = MA_OK;
        ma_recorder_info_t *record = NULL;

        if(MA_OK == (err = ma_recorder_get(recorder, contact, &record))) {
            (void)add_pending_complete(record, contact, booking_id, is_complete);
            err = ma_recorder_add(recorder, record);
            (void)ma_recorder_info_release(record);
        } else {
            if(MA_OK == (err = ma_recorder_info_create(&record))) {
                (void)ma_recorder_info_set_date(record, contact);
                (void)add_pending_complete(record, contact, booking_id, is_complete);
                err = ma_recorder_add(recorder, record);
                (void)ma_recorder_info_release(record);
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t recorder_remove_record(ma_recorder_t *recorder, const char *contact, const char *booking_id, ma_bool_t is_complete) {
    if(contact && booking_id) {
        ma_error_t err = MA_OK;
        ma_recorder_info_t *record = NULL;

        if(MA_OK == (err = ma_recorder_get(recorder, contact, &record))) {
            (void)remove_pending_complete(record, contact, booking_id, is_complete);
            err = ma_recorder_add(recorder, record);
            (void)ma_recorder_info_release(record);
        } else {
            if(MA_OK == (err = ma_recorder_info_create(&record))) {
                (void)ma_recorder_info_set_date(record, contact);
                (void)remove_pending_complete(record, contact, booking_id, is_complete);
                err = ma_recorder_add(recorder, record);
                (void)ma_recorder_info_release(record);
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}
static void current_date_str(char *today_str) {
    ma_task_time_t today = {0};
    get_localtime(&today);
    today.hour = today.minute = today.secs = 0;
    MA_MSC_SELECT(_snprintf, snprintf)(today_str, MA_MAX_LEN, "%hu-%hu-%hu", today.month, today.day, today.year);
}
static ma_error_t recorder_save_handler(ma_recorder_service_t *service, const char *booking_id) {
    if(service && booking_id) {
        ma_error_t err = MA_OK;
        ma_context_t *context = service->context;
        ma_scheduler_t *scheduler = MA_CONTEXT_GET_SCHEDULER(context) ;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(context);
        ma_recorder_t *recorder = MA_CONTEXT_GET_RECORDER(context);
        ma_booker_t *booker = MA_CONTEXT_GET_BOOKER(context);
        ma_task_t *task = NULL;
        const char *task_id = NULL;

        if(MA_OK == (err = ma_scheduler_get_task_handle(scheduler, booking_id, &task))) {
            ma_variant_t *payload = NULL;

            (void)ma_task_get_id(task, &task_id);
            MA_LOG(logger, MA_LOG_SEV_TRACE, "Booking Task id(%s) exist in scheduler", booking_id);
            if(MA_OK == (err = ma_task_get_app_payload(task, &payload)) && payload) {
                ma_booking_info_t *info = NULL;
                
                if(MA_OK == (err = ma_booker_get(booker, booking_id, &info))) {
                    char id[MA_MAX_LEN+1] = {0};
                    char picker_contact[MA_MAX_LEN+1] = {0};
                    ma_booking_info_status_t status = MA_BOOKING_INFO_STATUS_NOT_ASSIGNED;
                    char user_contact[MA_MAX_LEN+1] = {0};

                    (void)ma_booking_info_get_status(info, &status);
                    (void)ma_booking_info_get_contact(info, user_contact);
                    (void)ma_booking_info_get_id(info, id);
                    (void)ma_booking_info_get_picker_contact(info, picker_contact);
                    if(MA_BOOKING_INFO_STATUS_NOT_ASSIGNED == status) {
                       char today_str[MA_MAX_LEN+1]= {0};

                       (void)current_date_str(today_str);
                       if(MA_OK == (err = recorder_add_record(recorder, today_str, id, MA_FALSE))) {
                           MA_LOG(logger, MA_LOG_SEV_DEBUG, "record add record today_str(%s) id(%s)", today_str, id);
                       } else {
                           MA_LOG(logger, MA_LOG_SEV_ERROR, "record add record today_str(%s) id(%s) failed last error(%d)", today_str, id, err);
                       }
                    } else if(MA_BOOKING_INFO_STATUS_ASSIGNED == status) {
                       char today_str[MA_MAX_LEN+1]= {0};

                       (void)current_date_str(today_str);
                       if(MA_OK == (err = recorder_add_record(recorder, today_str, id, MA_FALSE))) {
                           MA_LOG(logger, MA_LOG_SEV_DEBUG, "record add record today_str(%s) id(%s)", today_str, id);
                       } else {
                           MA_LOG(logger, MA_LOG_SEV_ERROR, "record add record today_str(%s) id(%s) failed last error(%d)", today_str, id, err);
                       }
                       if(MA_OK == (err = recorder_add_record(recorder, today_str, id, MA_FALSE))) {
                           MA_LOG(logger, MA_LOG_SEV_DEBUG, "record add record today_str(%s) id(%s)", today_str, id);
                       } else {
                           MA_LOG(logger, MA_LOG_SEV_ERROR, "record add record today_str(%s) id(%s) failed last error(%d)", today_str, id, err);
                       }
                    } else if(MA_BOOKING_INFO_STATUS_DELIVERED == status ||
                              MA_BOOKING_INFO_STATUS_CANCELLED == status ||
                              MA_BOOKING_INFO_STATUS_NOT_DELIVERED == status) {
                        char today_str[MA_MAX_LEN+1]= {0};

                        (void)current_date_str(today_str);
                        if(MA_OK == (err = recorder_remove_record(recorder, today_str, id, MA_FALSE))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "record add record today_str(%s) id(%s)", today_str, id);
                        } else {
                            MA_LOG(logger, MA_LOG_SEV_ERROR, "record add record today_str(%s) id(%s) failed last error(%d)", today_str, id, err);
                        }
                        if(MA_OK == (err = recorder_add_record(recorder, today_str, id, MA_TRUE))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "record add record today_str(%s) id(%s)", today_str, id);
                        } else {
                            MA_LOG(logger, MA_LOG_SEV_ERROR, "record add record today_str(%s) id(%s) failed last error(%d)", today_str, id, err);
                        }
                    } else if(MA_BOOKING_INFO_STATUS_PICKED == status) {
                    } else {
                    }
                    (void)ma_booking_info_release(info);
                }
                (void)ma_variant_release(payload);
            }
            (void)ma_task_release(task);
        }
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_bool_t check_booking_still_active(ma_booker_t *service, const char *booking_id) {
    if(service && booking_id) {
        ma_error_t err = MA_OK;
        ma_bool_t active = MA_FALSE;
        ma_booking_info_t *info = NULL;

        if(MA_OK == (err = ma_booker_get(service, booking_id, &info))) {
            (void)ma_booking_info_get_active(info, &active);
            (void)ma_booking_info_release(info);
        }
        return active;
    }
    return MA_FALSE;
}

ma_error_t recorder_subscriber_cb(const char *topic, ma_message_t *message, void *cb_data) {
    if(topic && message && cb_data) {
        ma_error_t err = MA_OK;
        ma_recorder_service_t *service = (ma_recorder_service_t*)cb_data;
        //ma_recorder_t *recorder = service->recorder;

        MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "recorder subscriber received sensor message(%s)", topic);
        if(!strcmp(topic, MA_TASK_EXECUTE_PUBSUB_TOPIC) || !strcmp(topic, MA_TASK_STOP_PUBSUB_TOPIC) || !strcmp(MA_TASK_REMOVE_PUBSUB_TOPIC, topic) || !strcmp(topic, MA_TASK_UPDATE_STATUS_PUBSUB_TOPIC)) {
            const char *task_type = NULL , *task_owner_id = NULL , *task_id = NULL;

            MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "recorder processing (%s) message", topic);
            if( (MA_OK == (err = ma_message_get_property(message, MA_TASK_TYPE, &task_type))) && task_type &&
                (MA_OK == (err = ma_message_get_property(message, MA_TASK_PRODUCT_ID, &task_owner_id))) && task_owner_id &&
                (MA_OK == (err = ma_message_get_property(message, MA_TASK_ID, &task_id))) && task_id) {
                if(!strcmp(task_type, MA_BOOKING_TASK_TYPE)) {
                    ma_bool_t active = check_booking_still_active(MA_CONTEXT_GET_BOOKER(service->context), task_id);
                    if(active) {
                        MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "booking task id(%s) processing request received", task_id);
                        if(MA_OK == (err = recorder_save_handler(service, task_id))) {
                            MA_LOG(service->logger, MA_LOG_SEV_TRACE, "recorder service processing (%s) message", topic);
                        } else {
                            MA_LOG(service->logger, MA_LOG_SEV_ERROR, "recorder service processing (%s) message failed last error(%d)", topic, err);
                        }
                    } else {
                        MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "booking task id(%s) inactive ", task_id);
                    }
                    MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "recorder task id(%s) processing request received", task_id);
                } else
                    MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "task id(%s) is not recorder task", task_id);
            }
        } else {
            /* do nothing */
        }


        return err;
    }
    return MA_ERROR_INVALIDARG;
}


