#include "ma/internal/services/policy/ma_policy_handler.h"
#include "ma/ma_log.h"
#include "ma/ma_variant.h"
#include "ma/internal/utils/threading/ma_atomic.h"

#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/defs/ma_policy_defs.h"
#include "ma/internal/defs/ma_io_defs.h"
#include "ma/internal/defs/ma_info_defs.h"
#include "ma/info/ma_info.h"

#include "ma/internal/utils/database/ma_db_recordset.h"

#include "ma/internal/utils/xml/ma_xml.h"
#include "ma/internal/utils/text/ma_byteorder.h"

#include "ma/internal/services/policy/ma_policy_manifest.h"
#include "ma/internal/services/policy/ma_policy_db.h"
#include "ma/internal/ma_macros.h"

#include "ma/ma_msgbus.h"
#include "ma/ma_message.h"

#include "ma/internal/services/policy/ma_policy_common.h"
#include "ma/internal/utils/datastructures/ma_vector.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "policy"

#define MA_CONTEXT_GET_POLICY_DATABASE(context) (ma_configurator_get_policies_database(MA_CONTEXT_GET_CONFIGURATOR(context)))

struct ma_policy_handler_s {
    ma_policy_manager_t     *policy_manager;
    ma_uint32_t             download_pso_index;
    ma_uint32_t             download_po_index;
    ma_error_t              status;
    ma_array_t              *notified_products;     /* This array contains products list */ 
    char                    *version; 
    ma_policy_manifest_t    *manifest;
    ma_atomic_counter_t     ref_cnt;
    ma_bool_t               policy_changed;
	ma_bool_t				policy_download_in_progress;
	ma_policy_url_request_t	*policy_url_request ;
    ma_spipe_package_t      *spipe_pkg;

    /* dynamic array of PSOs to be downloaded */
    ma_vector_define(download_pso_vec, policy_setting_object_t*);

    /* dynamic array of POs objects to be downloaded */
    ma_vector_define(download_po_vec, policy_object_t*);
};

static ma_error_t download_policy(ma_policy_handler_t *self, const char *po_id, const char *pso_id, const char *digest);
static ma_error_t load_hooked_policies_from_spipe(ma_policy_handler_t *self);
static ma_error_t convert_from_wire_form_to_db(ma_policy_handler_t *self, const char *po_id, unsigned char *po_xml, size_t xml_size);

ma_error_t ma_policy_handler_create(ma_policy_manager_t *policy_manager, ma_policy_handler_t **policy_handler) {
    if(policy_manager && policy_handler) {
        ma_policy_handler_t *self = (ma_policy_handler_t *)calloc(1, sizeof(ma_policy_handler_t));
        if(self) {
            self->policy_manager = policy_manager;
            MA_ATOMIC_INCREMENT(self->ref_cnt);
            self->policy_changed = MA_FALSE;
			self->policy_download_in_progress = MA_FALSE;
            *policy_handler = self;
            return MA_OK;
        }
        return MA_ERROR_OUTOFMEMORY;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_policy_handler_set_spipe_package(ma_policy_handler_t *self, ma_spipe_package_t *spipe_pkg) {
    return ma_spipe_package_add_ref(self->spipe_pkg = spipe_pkg);
}

ma_error_t ma_policy_handler_add_ref(ma_policy_handler_t *self) {
    if(self) {
        MA_ATOMIC_INCREMENT(self->ref_cnt);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t get_notified_product_list(ma_policy_handler_t *self) {
    /* querying for products which are changed in latest policies */
    ma_error_t rc = MA_OK;
    ma_db_recordset_t *record_set = NULL;
    if(self->version) {
        if(MA_OK != (rc = ma_array_create(&self->notified_products))) {
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to create array to save the notified products list");
            return MA_ERROR_POLICY_INTERNAL;
        }

        if(MA_OK != (rc = db_get_notified_product_list(MA_CONTEXT_GET_POLICY_DATABASE(self->policy_manager->context), self->version, &record_set))) {
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to get the products list  from Policy DB for policy version <%s> - rc <%d>", self->version, rc);
            return MA_ERROR_POLICY_INTERNAL;
        }
       
        while((MA_OK == rc) && (MA_ERROR_NO_MORE_ITEMS != (rc = ma_db_recordset_next(record_set)))) {
            char *product = NULL;        
            if(MA_OK != rc)
                break;

            (void)ma_db_recordset_get_string(record_set, 1, &product);              
            if(product) {
                ma_variant_t *product_var = NULL;                                            
                if(MA_OK == (rc = ma_variant_create_from_string(product, strlen(product), &product_var))) {
                    if(MA_OK != (rc = ma_array_push(self->notified_products, product_var)))
                        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to push product in notified products array");
                    (void)ma_variant_release(product_var);
                }
            }
        }
        
        if(MA_ERROR_NO_MORE_ITEMS == rc)
            rc = MA_OK;

        if(record_set) (void)ma_db_recordset_release(record_set);
    }
    else {
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "policy version is not available");
        rc = MA_ERROR_POLICY_INTERNAL;
    }
    return rc;
}

/* send policy changed msg to the policy service*/
static ma_error_t send_policy_change_msg(ma_policy_handler_t *self) {
    ma_error_t rc = MA_OK;
    ma_msgbus_endpoint_t *policy_ep = NULL;
    if(MA_OK == (rc = ma_msgbus_endpoint_create(MA_CONTEXT_GET_MSGBUS(self->policy_manager->context), MA_POLICY_SERVICE_NAME_STR, MA_POLICY_SERVICE_HOSTNAME_STR, &policy_ep))) {
        ma_message_t *policy_change_message = NULL;
        (void)ma_msgbus_endpoint_setopt(policy_ep, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
		(void)ma_msgbus_endpoint_setopt(policy_ep, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_INPROC);						
        if(MA_OK == (rc = ma_message_create(&policy_change_message))) {
            ma_variant_t *payload = NULL;
            ma_message_set_property(policy_change_message , MA_PROP_KEY_POLICY_REQUEST_TYPE_STR , MA_PROP_VALUE_ENFORCE_POLICIES_REQUEST_STR);
            ma_message_set_property(policy_change_message , MA_PROP_KEY_POLICY_ENFORCE_REASON_STR , MA_PROP_VALUE_POLICY_CHANGED_STR);
            ma_message_set_property(policy_change_message, MA_PROP_KEY_POLICY_VERSION, self->version);   
            if(MA_OK == (rc = get_notified_product_list(self))) {
                if(MA_OK == (rc = ma_variant_create_from_array(self->notified_products, &payload))) {                   
                    ma_message_set_payload(policy_change_message, payload);                                
                    if(MA_OK != (rc = ma_msgbus_send_and_forget(policy_ep, policy_change_message)))
                        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "Failed to send the policy change request to policy service = <%d>", rc);
                    (void)ma_variant_release(payload);
               }                
            }
            (void)ma_message_release(policy_change_message);
        }
        else            
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to create message rc = <%d>", rc);
        
        (void)ma_msgbus_endpoint_release(policy_ep);
    }        
    else
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to create endpoint for policy service rc = <%d>", rc);

    return rc;
}

/* send force policy enforcement msg to the policy service*/
static ma_error_t send_force_policy_enforcement_msg(ma_policy_handler_t *self) {
    ma_error_t rc = MA_OK ;
    ma_msgbus_endpoint_t *policy_ep = NULL ;

    if(MA_OK == (rc = ma_msgbus_endpoint_create(MA_CONTEXT_GET_MSGBUS(self->policy_manager->context), MA_POLICY_SERVICE_NAME_STR, MA_POLICY_SERVICE_HOSTNAME_STR, &policy_ep))) {
        ma_message_t *policy_enforce_message = NULL ;

        (void)ma_msgbus_endpoint_setopt(policy_ep, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO) ;
		(void)ma_msgbus_endpoint_setopt(policy_ep, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_INPROC) ;						
        if(MA_OK == (rc = ma_message_create(&policy_enforce_message))) {
            ma_variant_t *payload = NULL ;

            (void)ma_message_set_property(policy_enforce_message , MA_PROP_KEY_POLICY_REQUEST_TYPE_STR , MA_PROP_VALUE_ENFORCE_POLICIES_REQUEST_STR) ;
            (void)ma_message_set_property(policy_enforce_message , MA_PROP_KEY_POLICY_ENFORCE_REASON_STR , MA_PROP_VALUE_POLICY_FORCED_ENFORCEMENT_STR) ;
            (void)ma_message_set_property(policy_enforce_message, MA_PROP_KEY_POLICY_VERSION, self->version) ;   

            if(MA_OK == (rc = get_notified_product_list(self))) {
                if(MA_OK == (rc = ma_variant_create_from_array(self->notified_products, &payload))) {                   
                    (void)ma_message_set_payload(policy_enforce_message, payload) ;

                    if(MA_OK != (rc = ma_msgbus_send_and_forget(policy_ep, policy_enforce_message)))
                        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "Failed to send the force policy enforcement request to policy service = <%d>", rc);
                    (void)ma_variant_release(payload);
               }                
            }
            (void)ma_message_release(policy_enforce_message) ; policy_enforce_message = NULL ;
        }
        else            
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to create message rc = <%d>", rc);
        
        (void)ma_msgbus_endpoint_release(policy_ep);
    }        
    else
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to create endpoint for policy service rc = <%d>", rc);

    return rc;
}

static void publish_policy_update_status(ma_policy_handler_t *self, const char *status_topic) {
    ma_message_t *msg = NULL;
    ma_error_t rc = MA_OK;
    if(MA_OK == (rc = ma_message_create(&msg))) {
        if(!strcmp(status_topic, MA_POLICY_UPDATE_SUCCESS_TOPIC_STR) && self->manifest) {
            ma_table_t *server_settings = NULL;
            if(MA_OK == (rc = ma_policy_manifest_get_server_settings(self->manifest, &server_settings))) {
                ma_variant_t *var_data = NULL;
                if(MA_OK == (rc = ma_variant_create_from_table(server_settings, &var_data))) {
                    rc = ma_message_set_payload(msg, var_data);
                    (void) ma_variant_release(var_data);
                }  
                (void) ma_table_release(server_settings);
            }

			if(MA_OK != rc) 
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to attach the server setting to the publish message <%s>", status_topic);
    
			/* send ma info message */
			{
				ma_message_t *info_message = NULL ;
				if(MA_OK == (rc = ma_message_create(&info_message))) {
					ma_msgbus_endpoint_t *ep = NULL ;
					
					(void)ma_message_set_property(info_message, MA_PROP_KEY_REQUEST_TYPE_STR, MA_PROP_VALUE_AGENT_INFO_COLLECT) ;
					(void)ma_message_set_property(info_message, MA_INFO_MSG_KEY_EVENT_TYPE_STR, MA_INFO_EVENT_POLICY_DOWNLOADED_STR) ;

					if(MA_OK == (rc = ma_msgbus_endpoint_create(MA_CONTEXT_GET_MSGBUS(self->policy_manager->context), MA_IO_SERVICE_NAME_STR, NULL, &ep))) {
						(void)ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_INPROC) ;
						(void)ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);		
						
						if(MA_OK != (rc = ma_msgbus_send_and_forget(ep, info_message)))
							MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to send ma info message, rc = <%d>.", rc) ;
						
						(void)ma_msgbus_endpoint_release(ep) ;	ep = NULL ;
					}
					(void)ma_message_release(info_message) ; info_message = NULL ;
				}
		
				if(MA_OK != rc) 
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to send policy downlaod info message <%d>", rc);    
			}
        }
        
		    
		if(self->policy_download_in_progress)
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "Enforcing newly downloaded policies");
        
		rc = ma_msgbus_publish(MA_CONTEXT_GET_MSGBUS(self->policy_manager->context), status_topic, MSGBUS_CONSUMER_REACH_INPROC, msg);
        ma_message_release(msg);
    }
    
    if(MA_OK != rc) {
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to publish policies update topic <%s>", status_topic);
    }
    else {
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "Successfully publish Policies update topic <%s>", status_topic);
    }
}

static void release(void *p) {

}

void ma_policy_handler_stop(ma_policy_handler_t *self, ma_error_t status) {    
    MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "Policy handler stop - received status - <%d>", status);
    if(MA_OK == status) {
        if(MA_OK == ma_db_transaction_end(MA_CONTEXT_GET_POLICY_DATABASE(self->policy_manager->context)))
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "Policy db transaction end");

        if(self->policy_changed) {
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "Policies are changed");
            (void)send_policy_change_msg(self);
        }
        else if(self->policy_manager->force_policy_enforcement) {
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "Forcing policy enforment.");
            (void)send_force_policy_enforcement_msg(self) ;
        }

		(void)publish_policy_update_status(self, MA_POLICY_UPDATE_SUCCESS_TOPIC_STR);
    }
    else {
        if(MA_OK == ma_db_transaction_cancel(MA_CONTEXT_GET_POLICY_DATABASE(self->policy_manager->context))) {
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "Policy db transaction canceled");
            (void)publish_policy_update_status(self, MA_POLICY_UPDATE_FAILURE_TOPIC_STR);
        }
    }

	if(!ma_vector_empty(self, download_pso_vec) || !ma_vector_empty(self, download_po_vec)) {
		if(self->policy_url_request)	ma_policy_url_request_cancel(self->policy_url_request), self->policy_url_request = NULL ;
		if(!ma_vector_empty(self, download_pso_vec)) ma_vector_clear(self, policy_setting_object_t*, download_pso_vec, release) ;
        if(!ma_vector_empty(self, download_po_vec)) ma_vector_clear(self, policy_object_t*, download_po_vec, release) ;
	}

    if(self->manifest)         (void)ma_policy_manifest_release(self->manifest), self->manifest = NULL;
    if(self->spipe_pkg)        (void)ma_spipe_package_release(self->spipe_pkg), self->spipe_pkg = NULL;

    self->policy_manager->policy_request_status = MA_POLICY_MANIFEST_REQUEST_COMPLETED;

    ma_policy_handler_release(self);
}

static ma_error_t download_connect_callback(void *user_data, ma_url_request_t *request) {  
    ma_policy_handler_t *self = (ma_policy_handler_t *)user_data;
    return MA_OK;
}

static void download_final_callback(ma_error_t status, long response_code, void *user_data, ma_url_request_t *request) {
    ma_error_t rc = MA_OK; 
    ma_policy_handler_t *self = (ma_policy_handler_t *)user_data;
    ma_bool_t is_po_download = self->policy_url_request->is_po_download;

    MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "download http response status <%d>, response code <%ld>.", status, response_code);

    if(200 == response_code) {            
        ma_bytebuffer_t *byte_buffer = NULL;
        ma_stream_t *mstream = (ma_stream_t *)self->policy_url_request->io_streams.write_stream;
            
        if(MA_OK == (rc = ma_stream_get_buffer(mstream, &byte_buffer))) {
            unsigned char *xml_data = ma_bytebuffer_get_bytes(byte_buffer);
            size_t xml_len = ma_bytebuffer_get_size(byte_buffer);            
            rc = convert_from_wire_form_to_db(self, self->policy_url_request->id, xml_data, xml_len);
            ma_bytebuffer_release(byte_buffer);           
        }
        else
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "failed to get bytebuffer from mstream rc <%d>", rc);
    }	
    else {
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to download po/pso <%s>, network return status <%d>, response code <%ld>", self->policy_url_request->id, status, response_code);  
		
		/* Policy download stopped on network error from any policy download, anyhow it cancels the DB transaciton */
        if(MA_ERROR_NETWORK_REQUEST_SUCCEEDED != status) 
            rc = MA_ERROR_POLICY_INTERNAL; 

		/* Set PO digest value to 0 if response from server is other than 200 e.g 404 response, It allows recovery of latest policies in next time policy download */
		if(MA_OK == rc) {
			const char *po_id = NULL;
			if(is_po_download) {
				po_id = self->policy_url_request->id;
			}
			else {
				/* Retrive PO id for PSO */
				policy_setting_object_t *pso = NULL;            
				pso = ma_vector_at(self, self->download_pso_index, policy_setting_object_t *, download_pso_vec);			
				po_id = (pso) ? pso->po_id : NULL;
			}
		
			if(po_id) {
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "setting digest value to 0 for PO id <%s>", po_id);
				(void) db_update_po_digest(MA_CONTEXT_GET_POLICY_DATABASE(self->policy_manager->context), po_id, "0");
			}
			else
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "failed to retrive po Id");
		}
    }

    if(MA_OK != rc) {
        if(MA_ERROR_NETWORK_REQUEST_ABORTED != status) {
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Due to network error, calling policy handler stop rc - <%d>", rc);
            (void)ma_policy_url_request_release(self->policy_url_request);
            self->policy_url_request = NULL;
			ma_policy_handler_stop(self, rc);
		}
        return;
    }

    if(is_po_download) {
        if( (self->download_po_index +1 ) == ma_vector_get_size(self, download_po_vec) ) {		  
            (void)ma_policy_url_request_release(self->policy_url_request);
            self->policy_url_request = NULL;
            ma_policy_handler_stop(self, rc);
            return;
        } 
        else {
            policy_object_t *po = NULL;        
            ++self->download_po_index;
            po = ma_vector_at(self, self->download_po_index, policy_object_t *, download_po_vec);
			if(po) {
				if(po->is_hooked_policy) {
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "Copying hooked policy from spipe, po index <%d>", self->download_po_index);                
					if(MA_OK != (rc = load_hooked_policies_from_spipe(self)))
						MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "failed to load hooked policies from spipe rc - <%d>", rc);
					if(self->policy_url_request) (void)ma_policy_url_request_release(self->policy_url_request), self->policy_url_request = NULL;
					ma_policy_handler_stop(self, rc);
				}
				else {
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "download po from network, po index <%d>", self->download_po_index);                
					if(MA_OK != (rc =  download_policy(self, po->po_id, NULL, po->digest))) {                
						MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "failed to start download po, rc - <%d>", rc);
						(void)ma_policy_url_request_release(self->policy_url_request), self->policy_url_request = NULL;
						ma_policy_handler_stop(self, rc);
					}
				}
			}
			else {
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_CRITICAL, "Failed to get po object from vector for index %d", self->download_po_index);
				(void)ma_policy_url_request_release(self->policy_url_request);
				self->policy_url_request = NULL;
				ma_policy_handler_stop(self, MA_ERROR_UNEXPECTED);
				return;
			}
        }
    }
    else {
        if((self->download_pso_index +1 )  == ma_vector_get_size(self, download_pso_vec)) {
	        if(!ma_vector_empty(self,download_po_vec)) {   
                policy_object_t *po = NULL;
                self->download_po_index = 0;
                po = ma_vector_at(self, self->download_po_index, policy_object_t *, download_po_vec);
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "Total POs to be downloaded <%d>", ma_vector_get_size(self, download_po_vec));            
                
                if(po->is_hooked_policy) {
                    if(MA_OK != (rc = load_hooked_policies_from_spipe(self)))
                        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "failed to load hooked policies from spipe rc - <%d>", rc);
                    if(self->policy_url_request) (void)ma_policy_url_request_release(self->policy_url_request), self->policy_url_request = NULL;
                    ma_policy_handler_stop(self, rc);
                }
                else {        
                    /* start PO downloading one by one */
                    if(MA_OK != (rc = download_policy(self, po->po_id, NULL, po->digest))) {
                        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to start po downloads- rc <%d>", rc);
                        if(self->policy_url_request) (void)ma_policy_url_request_release(self->policy_url_request), self->policy_url_request = NULL;
                        ma_policy_handler_stop(self, rc);
                    }
                }
            }
            else {
                if(self->policy_url_request) (void)ma_policy_url_request_release(self->policy_url_request), self->policy_url_request = NULL;
                ma_policy_handler_stop(self, rc);
            }
            return;
        } 
        else {
            policy_setting_object_t *pso = NULL;
            ++self->download_pso_index;
            pso = ma_vector_at(self, self->download_pso_index, policy_setting_object_t *, download_pso_vec);
			if(pso) {
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "download pso, index <%d>", self->download_pso_index);
				if(MA_OK != (rc = download_policy(self, pso->po_id, pso->pso_id, pso->digest))) {
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "failed to start pso download rc - <%d>", rc);
					if(self->policy_url_request) (void)ma_policy_url_request_release(self->policy_url_request), self->policy_url_request = NULL;
					ma_policy_handler_stop(self, rc);
				}
			}
			else {
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_CRITICAL, "failed to get pso from vector index %d", self->download_pso_index);
				if(self->policy_url_request) (void)ma_policy_url_request_release(self->policy_url_request), self->policy_url_request = NULL;
					ma_policy_handler_stop(self, MA_ERROR_UNEXPECTED);
				}
			}
        }
	}

static ma_error_t copy_pso_xml_to_db(ma_policy_handler_t *self, const char *po_id, unsigned char *xml_payload, size_t xml_len) {
    ma_error_t rc = MA_OK;
    ma_xml_t *pol_xml = NULL;
    if((MA_OK == rc) && MA_OK == (rc = ma_xml_create(&pol_xml))) {
        if(MA_OK == (rc = ma_xml_load_from_buffer(pol_xml, (char*)xml_payload, xml_len))) {
            ma_xml_node_t *policy_root = NULL;
            ma_xml_node_t *node =  NULL;
            if((NULL != (policy_root = ma_xml_get_node(pol_xml)))
                &&(NULL != (node = ma_xml_node_search_by_path(policy_root, XML_POLICY_SETTING_OBJECT_ROOT_STR)))) {                
                ma_table_t *sec_table = NULL;
                ma_variant_t *pso_data_var = NULL;              

                const char *param_int = (char *)ma_xml_node_attribute_get(node, XML_PARM_INT_STR);
                const char *param_str = (char *)ma_xml_node_attribute_get(node, XML_PARAM_STR_STR);
                const char *type = (char *)ma_xml_node_attribute_get(node, XML_TYPE_STR);
                const char *category = (char *)ma_xml_node_attribute_get(node, XML_CATEGORY_STR);
                const char *feature = (char *)ma_xml_node_attribute_get(node, XML_FEATURE_STR);
                const char *name = (char *)ma_xml_node_attribute_get(node, XML_NAME_STR);
                const char *pso_id = (char *)ma_xml_node_attribute_get(node, XML_ID_STR);
                
                if(!pso_id || !name || !feature || !category || !type || !param_str || !param_int) {
                    MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to get pso details from manifest");
                    rc = MA_ERROR_POLICY_INTERNAL;            
                }

                if((MA_OK == rc) && (MA_OK == (rc = ma_table_create(&sec_table)))) {
                    ma_xml_node_t *sec_node = ma_xml_node_get_child(node);
                    while((MA_OK == rc) && sec_node) {
                        const char *sec_name = (char *)ma_xml_node_attribute_get(sec_node, "name");
                        ma_xml_node_t *set_node = ma_xml_node_get_child(sec_node);
                        ma_variant_t *set_var = NULL;
                        ma_table_t *set_table = NULL;
                        rc = ma_table_create(&set_table);
                        while((MA_OK == rc ) && set_node) {  
                            const char *key = (char *)ma_xml_node_attribute_get(set_node, "name");
                            const char *value = ma_xml_node_get_data(set_node);
                            if(key && value) {
                                ma_variant_t *val_var = NULL;
                                if(MA_OK == (rc = ma_variant_create_from_string(value, strlen(value), &val_var))) {
                                    rc = ma_table_add_entry(set_table, key, val_var);
                                    (void)ma_variant_release(val_var);                           
                                }
                            }
                            set_node = ma_xml_node_get_next_sibling(set_node);
                        }

                        if(MA_OK == (rc = ma_variant_create_from_table(set_table, &set_var))) {
                            rc = ma_table_add_entry(sec_table, sec_name, set_var);
                            (void)ma_variant_release(set_var);
                        }                                    
                        (void)ma_table_release(set_table);

                        sec_node = ma_xml_node_get_next_sibling(sec_node);
                    }
                
                    if(MA_OK == (rc = ma_variant_create_from_table(sec_table, &pso_data_var))) {                           
                        policy_setting_object_t *pso = NULL;
                        if(MA_OK == (rc = ma_policy_manifest_get_pso(self->manifest, po_id, pso_id, &pso))) {
                            if(MA_OK != (rc = db_add_policy_settings_object(MA_CONTEXT_GET_POLICY_DATABASE(self->policy_manager->context), po_id, pso_id, name, pso->digest, pso_data_var, param_int, param_str, (char*)self->version)))
                                MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "failed to add pso data into pso table");
                        }
                        else
                            MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "failed to get pso from manifest for po id <%s> pso id <%s>, rc <%d>", po_id, pso_id, rc);
                        (void)ma_variant_release(pso_data_var);
                    }
                    (void)ma_table_release(sec_table);                                
                }
                else
                    MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "failed to create table");

            }
            else {
                rc = MA_ERROR_POLICY_INTERNAL;
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "failed to get pso node from xml");
            }
        }
        else
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "failed to load xml from buffer rc <%d>", rc);
                    
        (void)ma_xml_release(pol_xml);
    }
    else
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "failed to create maxml rc <%d>", rc);

    return rc;
}

static ma_error_t convert_from_wire_form_to_db(ma_policy_handler_t *self, const char *po_id, unsigned char *po_xml, size_t xml_size) {
    ma_error_t rc = MA_OK;

    ma_uint16_t pkg_type = 0;
    ma_uint32_t pkg_len = 0;
    ma_uint64_t obj_id = 0;
    ma_bool_t is_pso_type = MA_TRUE;
    ma_uint16_t pso_count = 0;
    ma_uint16_t xml_len = 0;

    unsigned char *p = po_xml;     

    /* start (EPO) */       
    p += sizeof(ma_uint32_t);     

    /* package type  */    
	memcpy(&pkg_type, p, sizeof(ma_uint16_t));
    pkg_type = ma_byteorder_host_to_be_uint16(pkg_type);
    p += sizeof(ma_uint16_t);        

    /* 0x0001 - Policy Setting Object Type, 0x0002 - Policy Object Type */
    is_pso_type = (pkg_type == 0x0001) ? MA_TRUE : MA_FALSE;

    /* package length */
	memcpy(&pkg_len, p, sizeof(ma_uint32_t));
    pkg_len = ma_byteorder_host_to_be_uint32(pkg_len);
    p += sizeof(ma_uint32_t);
        
    /* object id */
	memcpy(&obj_id, p, sizeof(ma_uint64_t));
    obj_id = ma_byteorder_host_to_be_uint64(obj_id);
    p += sizeof(ma_uint64_t);
    
    
    if(is_pso_type) {        
        ma_uint32_t xml_len = 0;
		
        /* xml length */
		memcpy(&xml_len, p, sizeof(ma_uint32_t));
        xml_len = ma_byteorder_host_to_be_uint32(xml_len);        
        p += sizeof(ma_uint32_t);

        rc = copy_pso_xml_to_db(self, po_id, p, xml_len);

        p += xml_len + sizeof(ma_uint32_t); /* length + END */
    }
    else {
        /* PSO count */
		memcpy(&pso_count, p, sizeof(ma_uint16_t));
        pso_count = ma_byteorder_host_to_be_uint16(pso_count);        
        p += sizeof(ma_uint16_t);

        while(pso_count) {
            ma_uint32_t xml_len = 0;            
            unsigned char *xml_payload = NULL;
            /* start (EPO) + pacgkage type + package length + object id + xml length */
            size_t hdr_len = sizeof(ma_uint32_t) + sizeof(ma_uint16_t) + sizeof(ma_uint32_t) + sizeof(ma_uint64_t); 

            p += hdr_len;
            /* xml length */
			memcpy(&xml_len, p, sizeof(ma_uint32_t));
            xml_len = ma_byteorder_host_to_be_uint32(xml_len);        
            p += sizeof(ma_uint32_t);            

            rc = copy_pso_xml_to_db(self, po_id, p, xml_len);

            p += xml_len + sizeof(ma_uint32_t); /* length + END */

            --pso_count;
        }
    }

    return rc;
}

static ma_error_t load_hooked_policies_from_spipe(ma_policy_handler_t *self) {
    ma_error_t rc = MA_OK;

    while(self->download_po_index != ma_vector_get_size(self, download_po_vec)) {        
        char po_file_name[MA_MAX_LEN] = {0};
        const unsigned char *po_xml = NULL;
        size_t xml_size = 0;
        policy_object_t *po = ma_vector_at(self, self->download_po_index, policy_object_t *, download_po_vec);

        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "copying po from spipe pkg, po index <%d>", self->download_po_index);

        MA_MSC_SELECT(_snprintf, snprintf)(po_file_name, MA_MAX_LEN, "%s.po", po->digest);
        /* Checking for hooked policies from spipe package */
        if(MA_OK == ma_spipe_package_get_data(self->spipe_pkg, po_file_name, (void **)&po_xml, &xml_size)) {
            rc = convert_from_wire_form_to_db(self, po->po_id, (unsigned char *)po_xml, xml_size);
        }
        
        if(MA_OK != rc) break;

        ++self->download_po_index;
    }

    return rc;
}

static ma_error_t download_policy(ma_policy_handler_t *self, const char *po_id, const char *pso_id, const char *digest) {
    ma_error_t rc = MA_OK;
    char url[MA_POLICY_TASK_MAX_URL_LENGTH_INT] = {0};
    self->policy_download_in_progress = MA_TRUE;

	MA_MSC_SELECT(_snprintf, snprintf)(url, MA_POLICY_TASK_MAX_URL_LENGTH_INT, "%s://%s:%d/policy/%s.%s.%s", 
			self->policy_manager->last_connection_from_ah->is_secure_port ? "https" : "http", self->policy_manager->last_connection_from_ah->address, self->policy_manager->last_connection_from_ah->port, (pso_id) ? "pso" : "po", (pso_id) ? pso_id: po_id , digest);	
    
    MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "Download Policy URL <%s>", url);
    if(MA_OK == (rc = ma_policy_url_request_reset_stream(self->policy_url_request))) {
        strncpy(self->policy_url_request->id, po_id, strlen(po_id));
        self->policy_url_request->is_po_download = (!pso_id) ? MA_TRUE : MA_FALSE;
        if(MA_OK != (rc = ma_policy_url_request_send(self->policy_url_request, url, download_connect_callback, download_final_callback, self))) {
		    MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Policy url request send failed for task object id <%s>", po_id);
            (void)ma_policy_url_request_release(self->policy_url_request);
            self->policy_url_request = NULL;
		}
    }
    else
	    MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to reset policy url request, rc <%d>", rc);

    return rc;
}

static int po_order_compare(const void *p1, const void *p2) {
    policy_object_t **po1 = (policy_object_t **)p1;
    policy_object_t **po2 = (policy_object_t **)p2;        
    return ((*po1)->is_hooked_policy - (*po2)->is_hooked_policy);
}

static ma_error_t update_pso_table(ma_policy_handler_t *self) {    
    ma_error_t rc = MA_OK;
    ma_array_t *po_id_arr = NULL;
    if(MA_OK == (rc = ma_policy_manifest_get_po_ids(self->manifest, &po_id_arr))) {        
        ma_variant_t *id_var = NULL;
        size_t po_id_size = 0;
        
        (void)ma_array_size(po_id_arr, &po_id_size);    

        if(po_id_size > 0) {
            ma_vector_init(self, po_id_size, download_po_vec, policy_object_t *);    
            ma_vector_init(self, po_id_size, download_pso_vec, policy_setting_object_t *);
        }

        while((MA_OK == rc) && po_id_size) {
            if(MA_OK == (rc = ma_array_get_element_at(po_id_arr, po_id_size, &id_var))) {
                ma_buffer_t *buff = NULL;
                if(MA_OK == (rc = ma_variant_get_string_buffer(id_var, &buff))) {
                    char *po_id = NULL;
                    size_t id_len = 0;
                    if(MA_OK == (rc = ma_buffer_get_string(buff, &po_id, &id_len))) {
                        ma_array_t *pso_id_arr = NULL;
                        if(MA_OK == (rc = ma_policy_manifest_get_pso_ids(self->manifest, po_id, &pso_id_arr))) { 
                            size_t pso_id_size = 0;        
                            (void)ma_array_size(pso_id_arr, &pso_id_size);   

                            if(pso_id_size > 0) {
                                policy_object_t *po = NULL;
                                if(MA_OK == (rc = ma_policy_manifest_get_po(self->manifest, po_id, &po))) {
                                    char po_file_name[MA_MAX_LEN] = {0};
                                    const char *policy_xml = NULL;
                                    size_t xml_size = 0;
                                    MA_MSC_SELECT(_snprintf, snprintf)(po_file_name, MA_MAX_LEN, "%s.po", po->digest);
                                    
                                    /* Checking for hooked policies from spipe package */
                                    if(MA_OK == ma_spipe_package_get_data(self->spipe_pkg, po_file_name, (void **)&policy_xml, &xml_size)) {                                        
                                        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_TRACE, "pushed hooked PO ID <%s> into PO downloading vector list.", po->po_id);
                                        po->is_hooked_policy = MA_TRUE;
                                        ma_vector_push_back(self, po, policy_object_t*, download_po_vec);
                                    }                                   
                                    else if((pso_id_size == po->pso_total) || pso_id_size > po->pso_total/2) {
                                        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_TRACE, "pushed PO ID <%s> into PO downloading vector list.", po->po_id);
                                        ma_vector_push_back(self, po, policy_object_t*, download_po_vec);
                                    }  /* If changed PSOs are less than half of total PSOs, download PSOs instead of PO */
                                    else {
                                        while((MA_OK == rc) && pso_id_size) {
                                            ma_variant_t *pso_id_var = NULL;
                                            if(MA_OK == (rc = ma_array_get_element_at(pso_id_arr, pso_id_size, &pso_id_var))) {
                                                ma_buffer_t *buff = NULL;
                                                if(MA_OK == (rc = ma_variant_get_string_buffer(pso_id_var, &buff))) {
                                                    char *pso_id = NULL;
                                                    size_t pso_id_len = 0;
                                                    if(MA_OK == (rc = ma_buffer_get_string(buff, &pso_id, &pso_id_len))) {
                                                        policy_setting_object_t *pso = NULL;
                                                        if(po_id && (MA_OK == (rc = ma_policy_manifest_get_pso(self->manifest, po_id, pso_id, &pso))) && pso) {
                                                            ma_vector_push_back(self, pso, policy_setting_object_t*, download_pso_vec);
                                                            MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_TRACE, "pushed PSO ID <%s> into PSO downloading vector list.", pso_id);
                                                        }
                                                        else
                                                            MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to get PSO data fro pso id <%s> (po id <%s>) ", pso_id, po_id);
                                                    }
                                                    else
                                                        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to get string from buffer - rc <%d>", rc);
                                                    (void)ma_buffer_release(buff);
                                                }
                                                else
                                                    MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to get buffer from variant - rc <%d>", rc);
                                                
                                                (void)ma_variant_release(pso_id_var);
                                            }
                                            else
                                                MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to get the element <%d> from PSO array ", pso_id_size);

                                            pso_id_size--;
                                        }                                       
                                    }
                                }
                                else
                                    MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to get PO from manifest for PO ID <%s>", po_id);
                            }
                            else
                                MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "NO changed PSOs for PO ID <%s>", po_id);

                            (void)ma_array_release(pso_id_arr);
                        }
                        else
                            MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to get PSO IDs from manifest for PO ID <%s>", po_id);                    
                    }
                    else
                        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to get string from buffer - rc <%d>", rc);
                    (void)ma_buffer_release(buff);
                }
                else
                    MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to get buffer from variant - rc <%d>", rc);
                (void)ma_variant_release(id_var);
            }            
            else
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to get the element <%d> from PO array ", po_id_size);

            po_id_size--;
        }
        (void)ma_array_release(po_id_arr);
    }
    else
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to get PO ids array from manifest rc <%d>", rc);

    if(MA_OK == rc) {
        /* pop-up all POs to be downloaded from network */
        ma_vector_sort(self, policy_object_t*, download_po_vec, po_order_compare);
        /* start download first PSO from network */ 
        if(!ma_vector_empty(self,download_pso_vec)) {
            policy_setting_object_t *pso = NULL;
            self->download_pso_index = 0;
            if(MA_OK == (rc = ma_policy_url_request_create(self->policy_manager, &self->policy_url_request))) {
                if(self->policy_manager->last_connection_from_ah->proxy_list)
                    ma_policy_url_request_set_proxy(self->policy_url_request, self->policy_manager->last_connection_from_ah->proxy_list);
                pso = ma_vector_at(self, self->download_pso_index, policy_setting_object_t *, download_pso_vec);
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "Total PSOs to be downloaded <%d>", ma_vector_get_size(self, download_pso_vec));
                /* start PSO downloading one by one */
                if(MA_OK != (rc = download_policy(self, pso->po_id, pso->pso_id, pso->digest))) {
                    MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to start pso downloads- rc <%d>", rc);
                }else {
                    ma_policy_handler_add_ref(self);
                }
            }
            else
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to create policy url request object rc <%d>", rc);
        }       
        else if(!ma_vector_empty(self,download_po_vec)) {
            policy_object_t *po = NULL;
            self->download_po_index = 0;            
            po = ma_vector_at(self, self->download_po_index, policy_object_t *, download_po_vec);
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "Total POs to be downloaded <%d>", ma_vector_get_size(self, download_po_vec));
            /* start PO downloading one by one */
            if(po->is_hooked_policy) {
                if(MA_OK != (rc = load_hooked_policies_from_spipe(self)))
                    MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "failed to load hooked policies from spipe rc - <%d>", rc);
            }
            else {
                if(MA_OK == (rc = ma_policy_url_request_create(self->policy_manager, &self->policy_url_request))) {
                    if(self->policy_manager->last_connection_from_ah->proxy_list)
                        ma_policy_url_request_set_proxy(self->policy_url_request, self->policy_manager->last_connection_from_ah->proxy_list);
                    if(MA_OK != (rc = download_policy(self, po->po_id, NULL, po->digest))) {
                        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to start po downloads- rc <%d>", rc);
                    }else {
                        ma_policy_handler_add_ref(self);
                    }
                }
                else
                    MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to create policy url request object rc <%d>", rc);
            }
        }
    }

    return rc;
}

static ma_error_t update_policy_settings(ma_policy_handler_t *self, const char *po_id) {
    ma_error_t rc = MA_OK;
    ma_db_recordset_t *record_set = NULL;

    if(MA_OK != (rc = db_get_policy_settings_objects(MA_CONTEXT_GET_POLICY_DATABASE(self->policy_manager->context), po_id, NULL, &record_set))) 
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to get the PSO records from PSO table for PO ID <%s> - rc <%d>", po_id, rc);
    
    while((MA_OK == rc) && (MA_ERROR_NO_MORE_ITEMS != (rc = ma_db_recordset_next(record_set)))) {
        char *pso_id = NULL, *digest = NULL;
        
        if(MA_OK != rc)
            break;

        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "Got new record from PSO table");

        (void)ma_db_recordset_get_string(record_set, 2, &pso_id);  /* 2nd column is PSO ID */
        (void)ma_db_recordset_get_string(record_set, 3, &digest); /* 3rd column is PSO Digest */

        if(pso_id && digest) {
            policy_setting_object_t *pso = NULL;
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "Updating policy setting object id <%s> (policy object <%s>)", pso_id, po_id);
            if(MA_OK == (rc = ma_policy_manifest_get_pso(self->manifest, po_id, pso_id, &pso)) && pso) {                
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "Checking digest of PSO - pso id <%s> (po id <%s>)", pso_id, po_id);
                if(!strncmp(pso->digest, digest, strlen(digest))) { /* If digest is not changed, delete pso from manifest object */
                    rc = ma_policy_manifest_delete_pso(self->manifest, po_id, pso_id); /* delete the policy setting object from manifest object as no change in PSO */  
                }
            }
            else if(MA_ERROR_OBJECTNOTFOUND == rc) {
                if(MA_OK == (rc = db_remove_policy_settings_object(MA_CONTEXT_GET_POLICY_DATABASE(self->policy_manager->context), po_id, pso_id))) { /*  This deletes records from pso table */
                    self->policy_changed = MA_TRUE; /* delete pso from pso table can be treated as a policy change */
                }
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "Deleting PSO Id <%s> and PO ID <%s> from PSO table, Not found in manifest", pso_id, po_id);                                
            }
			else {
				rc = MA_ERROR_POLICY_INTERNAL;
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to get the pso id <%s> info from manifest, rc <%d>", pso_id, rc);
			}
        }
        else {
            rc = MA_ERROR_POLICY_INTERNAL;
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to get the pso id and digest from pso table");
        }
    }
     
    if(MA_ERROR_NO_MORE_ITEMS == rc)
        rc = MA_OK;
    if(record_set) (void)ma_db_recordset_release(record_set);

    return rc ;
}

static ma_ah_site_t *get_agent_handler_site(ma_policy_handler_t *self, const char *server) {
    size_t i=0;
    ma_ah_sitelist_t *ah_sitelist = NULL;
    ma_ah_client_service_get_sitelist(MA_CONTEXT_GET_AH_CLIENT(self->policy_manager->context), &ah_sitelist);
    if(ah_sitelist){
        size_t count = ma_ah_sitelist_get_count(ah_sitelist);    
        for(i = 0; i < count ; i++) {
            ma_ah_site_t *site = ma_ah_sitelist_get_site(i, ah_sitelist);
            if(site) {
                const char *ah_server = ma_ah_site_get_short_name(site);
                if(ah_server && !strcmp(ah_server, server))
                    return site;
            }
        }
    }
    return NULL;
}

static ma_error_t update_po_table(ma_policy_handler_t *self) {    
    ma_error_t rc = MA_OK;
    ma_array_t *po_id_arr = NULL;
    if(MA_OK == (rc = ma_policy_manifest_get_po_ids(self->manifest, &po_id_arr))) {        
        ma_variant_t *id_var = NULL;
        size_t size = 0;
        
        const char *server_name = ma_policy_manifest_get_handler(self->manifest) ;  
        const char *created_time = ma_policy_manifest_get_created_time(self->manifest) ;

        (void)ma_array_size(po_id_arr, &size);
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "Number of new/modified Policy objects in manifest <%d>", size);

        if(size) {
            if(!created_time || !server_name) {
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to get the created time/ah name from manifest file");            
                rc = MA_ERROR_POLICY_INTERNAL;
            }
            else {
                /*if(NULL == (self->ah_site = get_agent_handler_site(self, server_name))) {
                    MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to get ah site of agent handler <%s>", server_name);
                    (void)ma_array_release(po_id_arr);
                    return MA_ERROR_POLICY_INTERNAL;
                }*/
                self->policy_changed = MA_TRUE;
                self->version = strdup(created_time); /* This value will be inserted in PSO table for new PSO and also same will be send in policy change message */            
            } 
        }
        else if(self->policy_manager->force_policy_enforcement) {
            self->version = strdup(created_time) ;
        }

        while((MA_OK == rc) && size) {
            if(MA_OK == (rc = ma_array_get_element_at(po_id_arr, size, &id_var))) {
                ma_buffer_t *buff = NULL;
                if(MA_OK == (rc = ma_variant_get_string_buffer(id_var, &buff))) {
                    char *po_id = NULL;
                    size_t id_len = 0;
                    if(MA_OK == (rc = ma_buffer_get_string(buff, &po_id, &id_len))) {
                        policy_object_t *po = NULL;
                        if(po_id && (MA_OK == (rc = ma_policy_manifest_get_po(self->manifest, po_id, &po))) && po) {
                            //ma_variant_t *product_var = NULL;
                            MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "Checking PSO digests of the PO ID <%s>", po_id);
                            /* To be reviewed - The feature and product values are same in manifest. so we can consider the feature as product */
                            /* Feature is added into notified_products array for policy change notifications */                            
                            /*if(MA_OK == (rc = ma_variant_create_from_string(po->feature, strlen(po->feature), &product_var))) {
                                if(MA_OK != (rc = ma_array_push(self->notified_products, product_var)))
                                    MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to push product in notified products array");
                                (void)ma_variant_release(product_var);
                            }*/

                            if((MA_OK == rc) && (MA_OK == (rc = update_policy_settings(self, po_id)))) { 
                                MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "Adding PO ID <%s> data into PO table", po_id);
                                if(MA_OK != (rc = db_add_policy_object(MA_CONTEXT_GET_POLICY_DATABASE(self->policy_manager->context), po->po_id, po->name, po->feature, po->category, po->type, po->digest, po->legacy)))
                                    MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to Add PO ID <%s> data into PO table", po_id);
                            }
                            else
                                MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to update policy setting objects in mnaifest table for PO ID <%s>", po_id);
                        }
                        else
                            MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to get PO from manifest for PO ID <%s>", po_id);
                    }
                    else
                        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to get string from buffer - rc <%d>", rc);
                    (void)ma_buffer_release(buff);
                }
                else
                    MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to get buffer from variant - rc <%d>", rc);
                (void)ma_variant_release(id_var);
            }            
            else
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to get the element <%d> from PO array ", size);

            size--;
        }
        (void)ma_array_release(po_id_arr);
    }
    else
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to get PO ids array from manifest rc <%d>", rc);

     return (MA_OK == rc) ? update_pso_table(self) : rc ;
}

static ma_error_t update_policies(ma_policy_handler_t *self) {
    ma_error_t rc = MA_OK;
    ma_db_recordset_t *record_set = NULL;

    if(MA_OK != (rc = db_get_policy_objects(MA_CONTEXT_GET_POLICY_DATABASE(self->policy_manager->context), NULL, &record_set))) 
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to get the all policy object records from Policy table - rc <%d>", rc);
        
    while((MA_OK == rc) && (MA_ERROR_NO_MORE_ITEMS != (rc = ma_db_recordset_next(record_set)))) {
        char *po_id = NULL, *digest = NULL;
        
        if(MA_OK != rc)
            break;

        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "Got new record from PO table");

        (void)ma_db_recordset_get_string(record_set, 1, &po_id);  /* first column is Policy Object ID */
        (void)ma_db_recordset_get_string(record_set, 2, &digest); /* third column is Policy Digest */

        if(po_id && digest) {
            policy_object_t *po = NULL;
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "Updating policy object id <%s>", po_id);
            if(MA_OK == (rc = ma_policy_manifest_get_po(self->manifest, po_id, &po)) && po) {
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "Checking digest of PO - po id <%s>",po_id);
                if(!strncmp(po->digest, digest, strlen(digest)))                    
                    rc = ma_policy_manifest_delete_po(self->manifest, po_id); /* delete the policy object from manifest object as no change in PO */                
            }
            else if(MA_ERROR_OBJECTNOTFOUND == rc) { 
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "Deleting policy object id <%s> from the PO table", po_id);                
                if(MA_OK == (rc = db_remove_policy_object(MA_CONTEXT_GET_POLICY_DATABASE(self->policy_manager->context), po_id))) { /*  This deletes records from all the tables corresponding to the PO id */
                    /* self->policy_changed = MA_TRUE; */ /* delete po from table can be treated as a policy change ???*/
                    MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "Deleted policy object id <%s> from policy DB", po_id);
                }
            }
			else {
				rc = MA_ERROR_POLICY_INTERNAL;
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to get the po id <%s> info from manifest, rc <%d>", po_id, rc);
			}
        }
        else {
            rc = MA_ERROR_POLICY_INTERNAL;
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to get the Policy object id and digest from PO table");
        }        
    }
    
    if(MA_ERROR_NO_MORE_ITEMS == rc)
        rc = MA_OK;

    if(record_set) (void)ma_db_recordset_release(record_set);

    return (MA_OK == rc) ? update_po_table(self) : rc ;
}

static void assign_key_array_foreach(size_t index, ma_variant_t *assign_key_var,  void *cb_args, ma_bool_t *stop_loop) {
    ma_buffer_t *buff = NULL;
    ma_error_t rc = MA_OK;
    ma_policy_handler_t *self = (ma_policy_handler_t *)cb_args;
    if(MA_OK == (rc = ma_variant_get_string_buffer(assign_key_var, &buff))) {
        char *key = NULL;
        size_t size = 0;
        if(MA_OK == (rc = ma_buffer_get_string(buff, &key, &size)) && key) {
            const assignments_object_t *po_assign = NULL;            
            if((MA_OK == (rc = ma_policy_manifest_get_assignment(self->manifest, key, &po_assign))) && po_assign) {
                /* method and param parameters are optional, so these values can be NULL */
                if(MA_OK == (rc = db_add_policy_product_assignment(MA_CONTEXT_GET_POLICY_DATABASE(self->policy_manager->context), po_assign->obj_id , po_assign->product, (po_assign->method) ? po_assign->method: NULL, (po_assign->param) ? po_assign->param : NULL))) {
                   /* ma_variant_t *product_var = NULL;
                    if(MA_OK == (rc = ma_variant_create_from_string(po_assign->product, strlen(po_assign->product), &product_var))) {
                        if(MA_OK != (rc = ma_array_push(self->notified_products, product_var)))
                            MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to push product in notified products array");
                        (void)ma_variant_release(product_var);
                    }*/
                    MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_INFO, "Added policy assignment po id <%s> product id <%s> in policy DB", po_assign->obj_id, po_assign->obj_id);
                }
                else
                    MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to insert data into policy assignments table - rc <%d>", rc);
            }
            else
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to get the assignment data from assignment table for key <%s>", key);
        }
        else
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to get the string data from buffer - rc <%d>", rc);

        (void) ma_buffer_release(buff);
    }
    else
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to get the string buffer from variant - rc <%d>", rc);

    /* If insertion operation failed for any item, loop stops, the same error propagate to the foreach caller */
    if(MA_OK != rc) {
        self->status = rc;
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "assignment data insertion operation failed - rc <%d>", rc);
        *stop_loop = MA_TRUE;
    }
}

static ma_error_t update_policy_assignments(ma_policy_handler_t *self) {
    ma_db_recordset_t *record_set = NULL;
    ma_error_t rc = MA_OK;

    if(MA_OK != (rc = db_get_policy_product_assignments(MA_CONTEXT_GET_POLICY_DATABASE(self->policy_manager->context), NULL, NULL, NULL, NULL, &record_set)))
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to get the policy assignments from DB - rc <%d>", rc);

    while((MA_OK == rc) && (MA_ERROR_NO_MORE_ITEMS != (rc = ma_db_recordset_next(record_set)))) {
        char *po_id = NULL, *product = NULL, *method = NULL, *param = NULL;

        if(MA_OK != rc)
            break;

        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "Got New record from policy assignment table");

        (void)ma_db_recordset_get_string(record_set, 1, &po_id);    /* 1st column is PO ID */
        (void)ma_db_recordset_get_string(record_set, 2, &product);  /* 2nd column is Product ID */                    
        (void)ma_db_recordset_get_string(record_set, 3, &method);   /* 3rd column is method */
        (void)ma_db_recordset_get_string(record_set, 4, &param);    /* 4th column is param */

        if(po_id && product) {
            char key[MA_MAX_LEN] = {0};
            MA_MSC_SELECT(_snprintf, snprintf)(key, MA_MAX_LEN, "%s|%s|%s|%s", po_id, product, method, param);                        
            
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "Updating policy assignment for <%s>", key);

            if(ma_policy_manifest_find_assignment(self->manifest, key))
                rc = ma_policy_manifest_delete_assignment(self->manifest, key); /* No change in existing assignment */
            else {
                if(strcmp(method, "0") && strcmp(param, "0")) {
                    /* remove data from all tables here because user policies might be assigned to another user without changing policy data */
                    if(MA_OK == (rc = db_remove_policy_object(MA_CONTEXT_GET_POLICY_DATABASE(self->policy_manager->context), po_id))) 
                        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "Deleted policies for <%s> from policy DB", key);
                    else
                        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to remove policy objects from policy DB for method <%s> and param <%s>, rc = <%d>", method, param, rc);                    
                }
                else {
                    /* delete po from po table will delete all the references from assignment and pso, so we can igonore this ??? */                    
                    if(MA_OK == (rc = db_remove_policy_product_assignment(MA_CONTEXT_GET_POLICY_DATABASE(self->policy_manager->context), po_id, product, (method)?method:NULL, (param)?param:NULL)))
                        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "Deleted product assignment <%s> from policy DB", key);                    
                    else
                        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to remove product assignment from policy DB rc = <%d>", rc);
                }
            }
        }
        else {
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to get the PO ID and product from Policy assignment table");
            rc = MA_ERROR_POLICY_INTERNAL;
        }
    }
    
    if(MA_ERROR_NO_MORE_ITEMS == rc)
        rc = MA_OK;

    if(record_set) (void)ma_db_recordset_release(record_set);

    if(MA_OK == rc) {
        ma_array_t  *key_arr = NULL;
        if(MA_OK == (rc = ma_policy_manifest_get_assigment_keys(self->manifest, &key_arr))) {
            size_t size = 0;
            (void)ma_array_size(key_arr, &size);
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "Number of new/modified policy assignments in manifest <%d>", size);
            if(size > 0) {
                self->policy_changed = MA_TRUE; /* new assignment can be treated as a policies change */
                self->status = MA_OK;
                if(MA_OK == (rc = ma_array_foreach(key_arr, assign_key_array_foreach, self)))
                    rc = self->status;
                self->status = MA_OK;
            }
            (void)ma_array_release(key_arr);
        }
    }
    return rc;
}

ma_error_t ma_policy_handler_start(ma_policy_handler_t *self, unsigned char *manifest, size_t manifest_size) {
    ma_error_t rc = MA_OK; 
	
	ma_policy_handler_add_ref(self);
    if(MA_OK == (rc = db_add_policy_instance(MA_CONTEXT_GET_POLICY_DATABASE(self->policy_manager->context)))) {
        if(MA_OK == (rc = ma_db_transaction_begin(MA_CONTEXT_GET_POLICY_DATABASE(self->policy_manager->context)))) { 
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "Succeeded DB transaction begin");
        }
        else
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed DB transaction begin");
    }
    else
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to create Policy DB instance"); 
    
   /* if(MA_OK != (rc = ma_array_create(&self->notified_products)))
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to create notified products array object");*/

    if((MA_OK == rc ) && (MA_OK == (rc = ma_policy_manifest_create(&self->manifest)))) {
        if(MA_OK == (rc = ma_policy_manifest_load(self->manifest, manifest, manifest_size, MA_TRUE))) {
            if(MA_OK == (rc = update_policy_assignments(self))) {
                if(MA_OK == (rc = update_policies(self))) {
                    MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "update policies Succeeded");
                }
                else
                    MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "update policies Failed - rc <%d>", rc);
            }
            else
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "update policy assignments Failed - rc <%d>", rc);
        }
        else
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to load manifest - rc <%d>", rc);
    }
    else
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to create manifest instance - rc <%d>", rc);

    
    if(MA_OK == rc)  {
		if(!self->policy_download_in_progress)  /* policy download is not in progress */
            (void)ma_policy_handler_stop(self, rc);
    }
    else{
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "policiy handler stop is calling due to the failure - rc <%d>", rc);
        (void)ma_policy_handler_stop(self, rc);
    }
    
    return rc;
}


static ma_error_t ma_policy_handler_destroy(ma_policy_handler_t *self) {
    if(self) {
        if(self->version) free(self->version), self->version = NULL;
        if(self->notified_products) (void)ma_array_release(self->notified_products), self->notified_products;
        free(self);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_policy_handler_release(ma_policy_handler_t *self) {
    if(self) {
        if(0 == MA_ATOMIC_DECREMENT(self->ref_cnt)) {
            (void)ma_policy_handler_destroy(self);
        }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

