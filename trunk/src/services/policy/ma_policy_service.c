#include "ma/internal/services/policy/ma_policy_service.h"
#include "ma/internal/defs/ma_policy_defs.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/services/policy/ma_policy_manager.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/utils/database/ma_db_recordset.h"
#include "ma/logger/ma_logger.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/filesystem/path.h"

#include "ma/internal/ma_strdef.h"
#include "ma/sensor/ma_sensor_msg.h"
#include "ma/sensor/ma_sensor_utils.h"

#include "ma/internal/defs/ma_info_defs.h"
#include "ma/info/ma_info.h"

#include "ma/internal/services/ma_service.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_message.h"

#include "ma/internal/services/policy/ma_policy_service_internal.h"
#include "ma/internal/services/policy/ma_policy_db.h"
#include "ma/internal/services/policy/ma_policy_user_info.h"
#include "ma/internal/utils/platform/ma_sys_apis.h"
#include "ma/internal/core/msgbus/ma_msgbus_private.h"
#include "uv.h"

#include <stdio.h>
#include <stdlib.h>


#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "policy"


#define MA_CONTEXT_GET_POLICY_DATABASE(context) (ma_configurator_get_policies_database(MA_CONTEXT_GET_CONFIGURATOR(context)))


struct ma_policy_service_s {
    ma_service_t            base_service;

    ma_context_t            *context;

    ma_msgbus_server_t      *policy_server;

    ma_policy_manager_t     *policy_manager;

    ma_msgbus_subscriber_t  *sensor_subscriber;

    ma_msgbus_subscriber_t  *policy_subscriber; /* Inproc subscriber for policy ypdate success or failure */

    ma_policy_user_info_t   *user_info;

	uv_timer_t				*enforce_on_startup_timer ;	/* timer to randomize the enforcement on service start */

    union {
        struct {
            ma_bool_t       is_policy_refreshed_for_user;
            ma_bool_t       is_policy_raise_alert_pending;
            char            *user;
            ma_uint32_t     session_id;
            ma_user_logged_state_t user_logged_state;
        } extra;
    };
} ;

static ma_error_t ma_policy_service_configure(ma_policy_service_t *service, ma_context_t *context, unsigned hint);
static ma_error_t ma_policy_service_start(ma_policy_service_t *service);
static ma_error_t ma_policy_service_stop(ma_policy_service_t *service);
static void ma_policy_service_destory(ma_policy_service_t *service);

static ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint) {
    return ma_policy_service_configure((ma_policy_service_t *)service, context, hint);
}

static ma_error_t service_start(ma_service_t *service) {
    return ma_policy_service_start((ma_policy_service_t *)service);
}

static ma_error_t service_stop(ma_service_t *service) {
    return ma_policy_service_stop((ma_policy_service_t *)service);
}

static void service_destory(ma_service_t *service) {
    ma_policy_service_destory((ma_policy_service_t *)service);
}

static const ma_service_methods_t service_methods = {
    &service_configure,
    &service_start,
    &service_stop,
    &service_destory
};

ma_error_t ma_policy_service_create(const char *service_name, ma_service_t **policy_service) {
    if (service_name && policy_service) {
        ma_policy_service_t *self = (ma_policy_service_t *)calloc(1, sizeof(ma_policy_service_t));
        if (self) {
            ma_service_init(&self->base_service, &service_methods, service_name, self);
            *policy_service = &self->base_service;
            return MA_OK;
        }
        return MA_ERROR_OUTOFMEMORY;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_bool_t check_context_validity(ma_context_t *context) {
    /*Will see if we need this*/
    return MA_TRUE;
}

ma_error_t ma_policy_service_configure(ma_policy_service_t *self, ma_context_t *context, unsigned hint) {
    if (self && context) {
        ma_error_t rc = MA_OK;
        if (MA_SERVICE_CONFIG_NEW == hint && check_context_validity(context)) {
            self->context = context;
            if (MA_OK == (rc = ma_policy_user_info_create(context, &self->user_info))) {
                (void) ma_policy_user_info_refresh(self->user_info, MA_FALSE);
                if (MA_OK == (rc = ma_policy_manager_create(context, &self->policy_manager))) {
                    (void) ma_policy_manager_set_user_info(self->policy_manager, self->user_info);
                    if (MA_OK == (rc = ma_msgbus_server_create(MA_CONTEXT_GET_MSGBUS(context), MA_POLICY_SERVICE_NAME_STR, &self->policy_server))) {
                        ma_msgbus_server_setopt(self->policy_server, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                        ma_msgbus_server_setopt(self->policy_server, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
                        if (MA_OK == (rc = ma_msgbus_subscriber_create(MA_CONTEXT_GET_MSGBUS(context), &self->sensor_subscriber))) {
                            ma_msgbus_subscriber_setopt(self->sensor_subscriber, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                            ma_msgbus_subscriber_setopt(self->sensor_subscriber, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
                            if (MA_OK == (rc = ma_msgbus_subscriber_create(MA_CONTEXT_GET_MSGBUS(context), &self->policy_subscriber))) {
                                ma_msgbus_subscriber_setopt(self->policy_subscriber, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_INPROC);
                                ma_msgbus_subscriber_setopt(self->policy_subscriber, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
                                return MA_OK;
                            }
                            (void) ma_msgbus_subscriber_release(self->sensor_subscriber);
                        }
                        (void) ma_msgbus_server_release(self->policy_server);
                        self->policy_server = NULL;
                    }
                    (void) ma_policy_manager_release(self->policy_manager);
                    self->policy_manager = NULL;
                }
                (void) ma_policy_user_info_release(self->user_info);
                self->user_info = NULL;
                self->context = NULL;
            }
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t refresh_policies_handle(ma_policy_service_t *self, ma_message_t *request_msg, ma_msgbus_client_request_t *c_request);
static ma_error_t enforce_policies_handle(ma_policy_service_t *self, ma_message_t *request_msg, ma_msgbus_client_request_t *c_request);
static ma_error_t get_policies_handle(ma_policy_service_t *self, ma_message_t *request_msg, ma_msgbus_client_request_t *c_request, ma_bool_t need_full_policies);
static ma_error_t get_policies_by_uri(ma_policy_service_t *self, ma_message_t *request_msg, ma_msgbus_client_request_t *c_request);
static ma_error_t get_policy_sections_by_uri(ma_policy_service_t *self, ma_message_t *request_msg, ma_msgbus_client_request_t *c_request);
static ma_error_t get_policies_by_section(ma_policy_service_t *self, ma_message_t *request_msg, ma_msgbus_client_request_t *c_request);
static ma_error_t set_policies_handle(ma_policy_service_t *self, ma_message_t *request_msg, ma_msgbus_client_request_t *c_request);
static ma_error_t reply_to_clients(ma_policy_service_t *self, ma_error_t status, ma_message_t *reply_msg, ma_msgbus_client_request_t *c_request);

static ma_error_t create_user_change_publish_message(const char *user, ma_user_logged_state_t user_logged_state, ma_uint32_t session_id, ma_bool_t is_user_log_on_first_time, ma_variant_t **publish_var);

static ma_bool_t is_first_asci_done(ma_context_t *context) {
    ma_bool_t b_result = MA_FALSE;
    if (context) {
        ma_buffer_t *buffer = NULL;
        ma_ds_t *ds = ma_configurator_get_datastore(MA_CONTEXT_GET_CONFIGURATOR(context));

        if (MA_OK == ma_ds_get_str(ds, MA_PROPERTY_DATA_PATH_NAME_STR, MA_PROPERTY_DATA_KEY_ASC_TIME_STR, &buffer)) {
            const char *value = NULL;
            size_t size = 0;
            if (MA_OK == ma_buffer_get_string(buffer, &value, &size)) {
                MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_DEBUG, "Policy - last ASCI time <%s>", value);
                b_result = strcmp(value, "0");
            }
            (void) ma_buffer_release(buffer);
        }
    }
    return b_result;
}

/*  MA_PROP_VALUE_REFRESH_POLICIES_REQUEST_STR   --> From policy controller or policy client.
MA_PROP_VALUE_ENFORCE_POLICIES_REQUEST_STR   --> From policy controller(timeout) or policy handler(policy change)
MA_PROP_VALUE_GET_POLICIES_REQUEST_STR       --> From policy client.
*/
static ma_error_t policy_server_handler(ma_msgbus_server_t *server, ma_message_t *request_msg, void *cb_data, ma_msgbus_client_request_t *c_request) {
    ma_policy_service_t *self = (ma_policy_service_t *)cb_data;
    ma_error_t rc = MA_OK;
    const char *request_type = NULL;
    if (self && request_msg && c_request) {
        if (MA_OK == (rc = ma_message_get_property(request_msg, MA_PROP_KEY_POLICY_REQUEST_TYPE_STR, &request_type))) {

            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Received policy request type <%s>", request_type);

            if (!strcmp(request_type, MA_PROP_VALUE_REFRESH_POLICIES_REQUEST_STR)) {
                rc = (is_first_asci_done(self->context)) ? refresh_policies_handle(self, request_msg, c_request) : MA_ERROR_ASC_NOT_PERFORMED ;
            } else if (!strcmp(request_type, MA_PROP_VALUE_ENFORCE_POLICIES_REQUEST_STR)) {
                rc = enforce_policies_handle(self, request_msg, c_request);
            } else if (!strcmp(request_type, MA_PROP_VALUE_GET_POLICIES_REQUEST_STR)) {
                rc = get_policies_handle(self, request_msg, c_request, MA_TRUE);
			} else if (!strcmp(request_type, MA_PROP_VALUE_GET_POLICY_URI_LIST_REQUEST_STR)) { 
				rc = get_policies_handle(self, request_msg, c_request, MA_FALSE);
			} else if (!strcmp(request_type, MA_PROP_VALUE_GET_POLICIES_BY_URI_REQUEST_STR)) { 
				rc = get_policies_by_uri(self, request_msg, c_request);            
			} else if (!strcmp(request_type, MA_PROP_VALUE_GET_SECTIONS_BY_URI_REQUEST_STR)) { 
				rc = get_policy_sections_by_uri(self, request_msg, c_request);            
			} else if (!strcmp(request_type, MA_PROP_VALUE_GET_SECTTINGS_BY_SECITON_REQUEST_STR)) { 				          
				rc = get_policies_by_section(self, request_msg, c_request);
            } else if (!strcmp(request_type, MA_PROP_VALUE_SET_POLICIES_REQUEST_STR)) {
                rc = set_policies_handle(self, request_msg, c_request);
            } else {
                rc = MA_ERROR_POLICY_INVALID_REQUEST;
            }
        } else {
            rc = MA_ERROR_POLICY_INVALID_REQUEST;
        }
    }

    if ((MA_ERROR_POLICY_INVALID_REQUEST == rc) || (MA_ERROR_ASC_NOT_PERFORMED == rc)) {
        ma_error_t status = rc;
        ma_message_t *reply = NULL;
        if (MA_OK == (rc = ma_message_create(&reply))) {
            rc = reply_to_clients(self, status, reply, c_request);
            (void)ma_message_release(reply);
        } else {
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to create reply message");
        }
    }

    return rc;
}

ma_error_t reply_to_clients(ma_policy_service_t *self, ma_error_t status, ma_message_t *reply_msg, ma_msgbus_client_request_t *c_request) {
    ma_error_t rc = MA_OK;
    if (MA_OK == (rc = ma_msgbus_server_client_request_post_result(c_request, status, reply_msg))) {
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Reply status <%d> to policy clients", status);
    } else {
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to send the reply status and message");
    }
    return rc;
}

static ma_error_t publish_policy_notification(ma_policy_service_t *self, const char *notification_type, ma_variant_t *payload) {
    ma_message_t *publish_msg = NULL;
    ma_error_t rc = MA_OK;
    if (MA_OK == (rc = ma_message_create(&publish_msg))) {
        (void)ma_message_set_payload(publish_msg, payload);
        ma_message_set_property(publish_msg , MA_PROP_KEY_POLICY_NOTIFICATION_STR , notification_type);
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "Enforcing policies");
        if (MA_OK == (rc = ma_msgbus_publish(MA_CONTEXT_GET_MSGBUS(self->context), MA_POLICY_NOTIFICATION_TOPIC_STR, MSGBUS_CONSUMER_REACH_OUTBOX, publish_msg))) {
			ma_db_recordset_t *recordset = NULL;
			ma_int32_t agent_mode = ma_configurator_get_agent_mode(MA_CONTEXT_GET_CONFIGURATOR(self->context));
			if(MA_OK == db_get_all_products(MA_CONTEXT_GET_POLICY_DATABASE(self->context), agent_mode, &recordset)) {
				while(MA_OK == ma_db_recordset_next(recordset)) {
					const char *product_id = NULL;
					(void)ma_db_recordset_get_string(recordset, 1, &product_id);
					if(product_id) {
						MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "Enforcing Policies for %s", product_id);
					}
				}
				ma_db_recordset_release(recordset);
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "Agent finished Enforcing policies");
				/* send ma info message */
				{
					ma_message_t *info_message = NULL ;
					if(MA_OK == (rc = ma_message_create(&info_message))) {
						ma_msgbus_endpoint_t *ep = NULL ;
					
						(void)ma_message_set_property(info_message, MA_PROP_KEY_REQUEST_TYPE_STR, MA_PROP_VALUE_AGENT_INFO_COLLECT) ;
						(void)ma_message_set_property(info_message, MA_INFO_MSG_KEY_EVENT_TYPE_STR, MA_INFO_EVENT_POLICY_ENFORCED_STR) ;

						if(MA_OK == (rc = ma_msgbus_endpoint_create(MA_CONTEXT_GET_MSGBUS(self->context), MA_IO_SERVICE_NAME_STR, NULL, &ep))) {
							(void)ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_INPROC) ;
							(void)ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);

							if(MA_OK != (rc = ma_msgbus_send_and_forget(ep, info_message)))
								MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to send ma info message, rc = <%d>.", rc) ;
							
							(void)ma_msgbus_endpoint_release(ep) ;	ep = NULL ;
						}
						(void)ma_message_release(info_message) ; info_message = NULL ;
					}
				}
			}
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Succeeded to publish policy notification type <%s> to subscribers", notification_type);
        }
        ma_message_release(publish_msg);
    } else {
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to create the message");
    }
    return rc;
}

static void reset_policy_user_data(ma_policy_service_t *self) {
    if (self->extra.user) {
        free(self->extra.user);
        self->extra.user = NULL;
    }
    self->extra.is_policy_refreshed_for_user = MA_FALSE;
    self->extra.is_policy_raise_alert_pending = MA_FALSE;
    self->extra.session_id = 0;
    self->extra.user_logged_state = 0;
}

static ma_error_t policy_status_subscription_callback(const char *topic, ma_message_t *message, void *cb_data) {
    ma_policy_service_t *self = (ma_policy_service_t*)cb_data;
    if (self && topic && message) {
        ma_error_t rc = MA_OK;
        /* publish only when the policy manager decorator has already added the users into spipe pkg and policy for those users are available as of now */
        if ((!strcmp(topic, MA_POLICY_UPDATE_SUCCESS_TOPIC_STR) || !strcmp(topic, MA_TASK_UPDATE_SUCCESS_TOPIC_STR)) && ma_policy_manager_raise_alert_request_done(self->policy_manager)) {
            if (self->extra.is_policy_refreshed_for_user) {
                ma_variant_t *publish_var = NULL;
                if (MA_OK == (rc = create_user_change_publish_message(self->extra.user, self->extra.user_logged_state, self->extra.session_id, MA_TRUE, &publish_var))) {
                    MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Publishing user change notification after policy update success");
                    rc = publish_policy_notification(self, MA_PROP_VALUE_NOTIFICATION_USER_CHANGED_STR, publish_var);
                    (void) ma_variant_release(publish_var);
                    reset_policy_user_data(self);
                }
                return rc;
            }

            if (self->extra.is_policy_raise_alert_pending) {
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Raise an alert which is pending due to user change notification,");
                if ((MA_OK == (rc = ma_policy_manager_raise_alert(self->policy_manager, MA_FALSE))) || (rc == MA_ERROR_POLICY_SESSION_IN_PROGRESS)) {
                    if (rc == MA_OK) {
                        self->extra.is_policy_refreshed_for_user = MA_TRUE;
                        (void) ma_policy_user_info_add(self->user_info, self->extra.user);
                        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_TRACE, "Raised alert after current policy update session for user <%s> notificaiton", self->extra.user);
                    } else {
                        self->extra.is_policy_raise_alert_pending = MA_TRUE;
                        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_TRACE, "Raise alert pending again for user <%s> notificaiton", self->extra.user);
                    }
                } else {
                    MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to refresh the policies after policy update success for user <%s>, rc <%d>", self->extra.user, rc);
                    reset_policy_user_data(self);
                }
                return rc;
            }
        } else if ((!strcmp(topic, MA_POLICY_UPDATE_FAILURE_TOPIC_STR) || !strcmp(topic, MA_TASK_UPDATE_FAILURE_TOPIC_STR)) && ma_policy_manager_raise_alert_request_done(self->policy_manager)) {
            if (self->extra.is_policy_refreshed_for_user && self->extra.is_policy_raise_alert_pending) {
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_WARNING, "Current policy update failed, So ignoring user change notification");
                reset_policy_user_data(self);
            }
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

/* TBD - This same code is there in policy user info, will be moved to common place */
static ma_bool_t is_local_user(ma_context_t *context, const char *domain_name) {
    ma_bool_t b_result = MA_TRUE;
    if (context && domain_name) {
        ma_buffer_t *buffer = NULL;
        ma_ds_t *ds = ma_configurator_get_datastore(MA_CONTEXT_GET_CONFIGURATOR(context));
        if (MA_OK == ma_ds_get_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_COMPUTER_NAME_STR, &buffer)) {
            const char *value = NULL;
            size_t size = 0;
            ma_buffer_get_string(buffer, &value, &size);
            b_result = !MA_WIN_SELECT(stricmp,strcasecmp)(value, domain_name);
            (void) ma_buffer_release(buffer);
        }

        if (!b_result) {
            b_result = !MA_WIN_SELECT(stricmp,strcasecmp)("WORKGROUP", domain_name);
        }
    }
    return b_result;
}

static ma_error_t create_user_change_publish_message(const char *user, ma_user_logged_state_t user_logged_state, ma_uint32_t session_id, ma_bool_t is_user_log_on_first_time, ma_variant_t **publish_var) {
    if (user && publish_var) {
        ma_table_t  *info_table = NULL;
        ma_error_t rc = MA_OK;

        if (MA_OK == (rc = ma_table_create(&info_table))) {
            ma_variant_t *value = NULL;
            char tmp_str[8] = {0};

            /* TBD - Sensor is sending only two states either LOG ON or LOG OFF, what about other states ?? */
            if (MA_OK == (rc = ma_variant_create_from_string((user_logged_state == MA_USER_LOGGED_STATE_ON) ? LOG_ON_STR : LOG_OFF_STR, (user_logged_state == MA_USER_LOGGED_STATE_ON) ? strlen(LOG_ON_STR) : strlen(LOG_OFF_STR), &value))) {
                (void) ma_table_add_entry(info_table, LOGON_STATE_STR, value);
                ma_variant_release(value);
                value = NULL;
            }

            MA_MSC_SELECT(_snprintf, snprintf)(tmp_str, 8, "%d", session_id);
            if (MA_OK == (rc = ma_variant_create_from_string(tmp_str, strlen(tmp_str), &value))) {
                (void) ma_table_add_entry(info_table, SESSION_ID_STR, value);
                ma_variant_release(value);
                value = NULL;
            }

            if (MA_OK == (rc = ma_variant_create_from_string(user, strlen(user), &value))) {
                (void) ma_table_add_entry(info_table, USER_ID_STR, value);
                ma_variant_release(value);
                value = NULL;
            }

			if (MA_OK == (rc = ma_variant_create_from_string(is_user_log_on_first_time? "1" :"0", 1, & value))) {
                (void) ma_table_add_entry(info_table, IS_USER_LOGON_FIRST_TIME, value);
                ma_variant_release(value);
                value = NULL;
            }

            rc = ma_variant_create_from_table(info_table, publish_var);
            (void) ma_table_release(info_table);
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t policy_sensor_subscription_callback(const char *topic, ma_message_t *message, void *cb_data) {
    ma_policy_service_t *self = (ma_policy_service_t*)cb_data;
    if (self && topic && message) {
        ma_error_t rc = MA_OK;

        if (!strcmp(topic, MA_SENSOR_USER_CHANGED_SUB_TOPIC_STR)) {
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Policy - sensor subscriber received topic <%s>", topic);
            /* To be reviewed - do we need check of first asci here ??? */
            if (is_first_asci_done(self->context)) {
                ma_variant_t *sensor_payload = NULL;
                ma_user_sensor_msg_t *user_sensor_msg = NULL;
                size_t n_users = 0;

                if (MA_OK == (rc = ma_message_get_payload(message, &sensor_payload))) {
                    rc = convert_variant_to_user_sensor_msg(sensor_payload, &user_sensor_msg);
                }

                if (user_sensor_msg && MA_OK == (rc = ma_user_sensor_msg_get_size(user_sensor_msg, &n_users))) {
                    MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Total number of users=%u", n_users);
                    /* TBD - Sensor can add one property in sensor msg to identify whether it is start-up message or not */
                    if (n_users == 1) {
                        ma_user_logged_state_t user_logged_state;
                        if (MA_OK == (rc = ma_user_sensor_msg_get_logon_state(user_sensor_msg, &user_logged_state))) {
                            ma_buffer_t *user_buf = NULL;
                            ma_buffer_t *domain_buf = NULL;
                            ma_uint32_t session_id;
                            if (MA_OK == (rc = ma_user_sensor_msg_get_user(user_sensor_msg, 0, &user_buf))) {
                                if (MA_OK == (rc = ma_user_sensor_msg_get_domain(user_sensor_msg, 0, &domain_buf))) {
                                    if (MA_OK == (rc = ma_user_sensor_msg_get_session_id(user_sensor_msg, 0, &session_id))) {
                                        const char *user = NULL, *domain = NULL;
                                        size_t size = 0;
                                        (void)ma_buffer_get_string(user_buf, &user, &size);
                                        (void)ma_buffer_get_string(domain_buf, &domain, &size);
                                        if (domain && user) {
                                            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "User <%s\\%s> logon state <%s> and session id <%d>", domain, user, user_logged_state == MA_USER_LOGGED_STATE_ON ? "ON" : "OFF", session_id);
                                            if (!is_local_user(self->context, domain)) {
                                                char user_domain[MA_MAX_LEN] = {0}; /* domain/User lenght will be more than 256 ?? */

                                                /* TBD - Assume the path separator must be same for all platforms in thsi case */
                                                MA_MSC_SELECT(_snprintf, snprintf)(user_domain, MA_MAX_LEN, "%s\\%s", domain, user);

                                                if (ma_policy_user_info_search(self->user_info, user_domain)) {
                                                    ma_variant_t *publish_var = NULL;
                                                    if (MA_OK == (rc = (create_user_change_publish_message(user_domain, user_logged_state, session_id, MA_FALSE, &publish_var)))) {
                                                        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Publishing <%s> user change notification", user_domain);
                                                        rc = publish_policy_notification(self, MA_PROP_VALUE_NOTIFICATION_USER_CHANGED_STR, publish_var);
                                                        (void) ma_variant_release(publish_var);
                                                    }
                                                } else {
                                                    if ((MA_OK == (rc = ma_policy_manager_raise_alert(self->policy_manager, MA_FALSE))) || (rc == MA_ERROR_POLICY_SESSION_IN_PROGRESS)) {
                                                        self->extra.session_id = session_id;
                                                        self->extra.user_logged_state = user_logged_state;
                                                        self->extra.user = strdup(user_domain);

                                                        if (rc == MA_OK) {
                                                            self->extra.is_policy_refreshed_for_user = MA_TRUE;
                                                            (void) ma_policy_user_info_add(self->user_info, user_domain);
                                                            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Raised alert for user <%s> notificaiton", user_domain);
                                                        } else {
                                                            self->extra.is_policy_raise_alert_pending = MA_TRUE;
                                                            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Raise alert pending for user <%s> notificaiton", user_domain);
                                                        }
                                                    } else {
                                                        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to refresh the policies for user <%s>, rc <%d>", user_domain, rc);
                                                    }
                                                }
                                            } else {
                                                MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "user <%s\\%s> is local user, so ignoring", domain, user);
                                            }
                                        } else {
                                            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to get domain and user name from sensor message");
                                        }                                        
                                    }
                                    (void) ma_buffer_release(domain_buf);
                                }
								(void) ma_buffer_release(user_buf);
                            }
                        }
                    } else if (n_users > 1) { /* This scenario may occur only on process start-up, Sensor service is sending all users in same publish event */
                        rc = ma_policy_user_info_refresh(self->user_info, MA_TRUE);
                    } else {
                        /* Ignore this case. This case will never hit */
                    }

                    (void) ma_user_sensor_msg_release(user_sensor_msg);
                }
            } else {
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Ignoring user changed message as first ASCI is not completed");
            }
        }
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_policy_service_start(ma_policy_service_t *self) {
    if (self && self->context) {
        ma_error_t rc = MA_OK;
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Policy Service Starts");

        if (MA_OK != (rc = ma_msgbus_subscriber_register(self->policy_subscriber, MA_POLICY_STATUS_SUBSCRIBE_TOPIC_STR, policy_status_subscription_callback, self))) {
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to subscribe to topic filter <%s> - rc <%d>", MA_POLICY_STATUS_SUBSCRIBE_TOPIC_STR, rc);
        }

        if (MA_OK != (rc = ma_msgbus_subscriber_register(self->sensor_subscriber,  MA_SENSOR_USER_CHANGED_SUB_TOPIC_STR, policy_sensor_subscription_callback, self))) {
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to subscribe to topic <%s> - rc <%d>", MA_SENSOR_USER_CHANGED_SUB_TOPIC_STR, rc);
        }

        if (MA_OK != ma_msgbus_server_start(self->policy_server, policy_server_handler, self)) {
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to start policy service - rc <%d>", rc);
        }

        if (MA_CONTEXT_GET_AH_CLIENT(self->context)) {
            if (MA_OK != (rc = ma_ah_client_service_register_spipe_decorator(MA_CONTEXT_GET_AH_CLIENT(self->context), ma_policy_manager_get_decorator(self->policy_manager)))) {
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to register policy spipe decorator - rc <%d>", rc);
            }

            if (MA_OK != (rc = ma_ah_client_service_register_spipe_handler(MA_CONTEXT_GET_AH_CLIENT(self->context), ma_policy_manager_get_handler(self->policy_manager)))) {
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to register policy spipe handler - rc <%d>", rc);
            }
        } else {
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "AH client is not available, not registering with AH client");
        }

        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

static void timer_close_cb(uv_handle_t *timer) {
	free(timer) ;
}

ma_error_t ma_policy_service_stop(ma_policy_service_t *self) {
    if (self && self->context) {
        ma_error_t rc = MA_OK;
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO, "Policy Service Stops");
		
		if(self->enforce_on_startup_timer) {
			uv_timer_stop(self->enforce_on_startup_timer) ;
			uv_close((uv_handle_t *)self->enforce_on_startup_timer, timer_close_cb) ;
			self->enforce_on_startup_timer = NULL ;
		}

        (void) ma_policy_manager_abort(self->policy_manager);

        if (MA_OK != (rc = ma_ah_client_service_unregister_spipe_decorator(MA_CONTEXT_GET_AH_CLIENT(self->context), ma_policy_manager_get_decorator(self->policy_manager)))) {
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to unregister policy spipe decorator - rc <%d>", rc);
        }

        if (MA_OK != (rc = ma_ah_client_service_unregister_spipe_handler(MA_CONTEXT_GET_AH_CLIENT(self->context), ma_policy_manager_get_handler(self->policy_manager)))) {
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to unregister policy spipe handler - rc <%d>", rc);
        }

        if (MA_OK != (rc = ma_msgbus_subscriber_unregister(self->policy_subscriber))) {
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to unsubscribe policy status subscriber - rc <%d>", rc);
        }

        if (MA_OK != (rc = ma_msgbus_subscriber_unregister(self->sensor_subscriber))) {
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to unsubscribe sensor subscriber - rc <%d>", rc);
        }

        if (MA_OK != (rc = ma_msgbus_server_stop(self->policy_server))) {
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to stop policy service - rc <%d>", rc);
        }

        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

void ma_policy_service_destory(ma_policy_service_t *self) {
    if (self) {
        ma_error_t rc = MA_OK;

        if (MA_OK != (rc = ma_policy_user_info_release(self->user_info))) {
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Policy user info release failed rc = <%d>", rc);
        }

        if (MA_OK != (rc = ma_msgbus_subscriber_release(self->policy_subscriber))) {
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "msgbus subscriber(policy_subscriber) release failed rc = <%d>", rc);
        }

        if (MA_OK != (rc = ma_msgbus_subscriber_release(self->sensor_subscriber))) {
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "msgbus subscriber(sensor_subscriber) release failed rc = <%d>", rc);
        }


        if (MA_OK != (rc = ma_msgbus_server_release(self->policy_server))) {
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "msgbus server release failed rc = <%d>", rc);
        }

        if (MA_OK != (rc = ma_policy_manager_release(self->policy_manager))) {
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Policy manager release failed rc = <%d>", rc);
        }

    }
    return;
}

static ma_error_t create_policy_change_payload(ma_message_t *request_msg, ma_variant_t **send_payload) {
    /* Request message has changed policy version property  and array of products which are being impacted on this policy change */
    /* creating two key value pairs in table for notificaiton msg for clients, one is version and another is product list */
    /* request message contains poduct list array which is coming from policy handler */
    const char *version = NULL;
    ma_error_t rc = MA_OK;
    if (MA_OK == (rc = ma_message_get_property(request_msg, MA_PROP_KEY_POLICY_VERSION, &version)) && version) {
        ma_variant_t *recv_payload = NULL;
        if (MA_OK == (rc = ma_message_get_payload(request_msg, &recv_payload))) {
            ma_table_t *prop_table = NULL;
            if (MA_OK == (rc = ma_table_create(&prop_table))) {
                ma_variant_t *ver_var = NULL;
                if (MA_OK == (rc = ma_variant_create_from_string(version, strlen(version), &ver_var))) {
                    if (MA_OK == (rc = ma_table_add_entry(prop_table, VERSION_STR, ver_var))) {
                        if (MA_OK == (rc = ma_table_add_entry(prop_table, PRODUCT_LIST_STR, recv_payload))) {
                            rc = ma_variant_create_from_table(prop_table, send_payload);
                        }
                    }
                    (void)ma_variant_release(ver_var);
                }
                (void)ma_table_release(prop_table);
            }
            (void)ma_variant_release(recv_payload);
        }
    }

    return rc;
}

/* Sending list of products available in assignment table in policy timeout notification message */
static ma_error_t create_policy_timeout_payload(ma_policy_service_t *self, ma_variant_t **send_payload) {
    ma_error_t rc = MA_OK;
    ma_db_recordset_t *record_set = NULL;
    ma_array_t *product_array = NULL;

    if (MA_OK == (rc = ma_array_create(&product_array))) {
		ma_int32_t agent_mode = ma_configurator_get_agent_mode(MA_CONTEXT_GET_CONFIGURATOR(self->context));
        if (MA_OK == (rc = db_get_all_products(MA_CONTEXT_GET_POLICY_DATABASE(self->context), agent_mode, &record_set))) {
            ma_table_t *prop_table = NULL;
            while ((MA_OK == rc) && record_set && (MA_ERROR_NO_MORE_ITEMS != (rc = ma_db_recordset_next(record_set)))) {
                const char *product_id = NULL;

                if (MA_OK != rc) {
                    break;
                }

                (void)ma_db_recordset_get_string(record_set, 1, &product_id);  /* product ID */

                if (product_id) {
                    ma_variant_t *product_var = NULL;
                    if (MA_OK == (rc = ma_variant_create_from_string(product_id, strlen(product_id), &product_var))) {
                        (void)ma_array_push(product_array, product_var);
                        (void)ma_variant_release(product_var);
                        product_var = NULL;
                    }
                } else {
                    rc = MA_ERROR_POLICY_INTERNAL;
                }
            }

            if (MA_ERROR_NO_MORE_ITEMS == rc) {
                rc = MA_OK;
            }

            if (record_set) {
                (void)ma_db_recordset_release(record_set);
            }

            if (MA_OK == (rc = ma_table_create(&prop_table))) {
                ma_variant_t *products_var = NULL;
                if (MA_OK == (rc = ma_variant_create_from_array(product_array, &products_var))) {
                    if (MA_OK == (rc = ma_table_add_entry(prop_table, PRODUCT_LIST_STR, products_var))) {
                        rc = ma_variant_create_from_table(prop_table, send_payload);
                    }
                    (void)ma_variant_release(products_var);
                }
                (void)ma_table_release(prop_table);
            }
        }
        (void)ma_array_release(product_array);
    }
    return rc;
}

static void enforce_policy_on_service_startup_cb(uv_timer_t *timer , int status) {
	ma_variant_t *send_payload = NULL ;
	ma_policy_service_t *self = (ma_policy_service_t *)timer->data ;
	ma_error_t rc = MA_OK ;

	if (MA_OK == (rc = create_policy_timeout_payload(self, &send_payload))) { /* using the timeout payload .. */
        rc = publish_policy_notification(self, MA_PROP_VALUE_NOTIFICATION_SERVICE_STARTUP_STR, send_payload);
        (void)ma_variant_release(send_payload);
    }
	uv_close((uv_handle_t *)timer, timer_close_cb) ;
	self->enforce_on_startup_timer = NULL ;
}

static void enforce_policy_on_service_startup(ma_policy_service_t *self) {
	ma_uint32_t random_interval = ma_sys_rand() % 30 + 30 ;
	if(self->enforce_on_startup_timer = (uv_timer_t *)calloc(1, sizeof(uv_timer_t))) {
		uv_timer_init(ma_event_loop_get_uv_loop(ma_msgbus_get_event_loop(MA_CONTEXT_GET_MSGBUS(self->context))), self->enforce_on_startup_timer) ;
		self->enforce_on_startup_timer->data = self ;
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "uv timer data(%p)\n", self);
		uv_timer_start(self->enforce_on_startup_timer, enforce_policy_on_service_startup_cb, random_interval*1000, 0) ;
	}else{
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "memory allocation failed");
	}
}

static ma_error_t enforce_policies_handle(ma_policy_service_t *self, ma_message_t *request_msg,ma_msgbus_client_request_t *c_request) {
    ma_error_t rc = MA_OK, request_status = MA_OK;
    const char *reason = NULL;
    if (MA_OK == ma_message_get_property(request_msg, MA_PROP_KEY_POLICY_ENFORCE_REASON_STR, &reason)) {
        ma_message_t *reply = NULL;
        ma_variant_t *send_payload = NULL;

        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Reason for policy Enforcement <%s>", reason);

        if (!strncmp(reason, MA_PROP_VALUE_POLICY_TIMEOUT_STR, strlen(MA_PROP_VALUE_POLICY_TIMEOUT_STR))) {
            if (MA_OK == (rc = create_policy_timeout_payload(self, &send_payload))) {
                rc = publish_policy_notification(self, MA_PROP_VALUE_NOTIFICATION_POLICY_TIMEOUT_STR, send_payload);
                (void)ma_variant_release(send_payload);
            }
        } else if (!strncmp(reason, MA_PROP_VALUE_POLICY_CHANGED_STR, strlen(MA_PROP_VALUE_POLICY_CHANGED_STR))) {
            if (MA_OK == (rc = create_policy_change_payload(request_msg, &send_payload))) {
                rc = publish_policy_notification(self, MA_PROP_VALUE_NOTIFICATION_POLICY_CHANGED_STR, send_payload);
                (void)ma_variant_release(send_payload);
            }
        } else if (!strncmp(reason, MA_PROP_VALUE_SERVICE_STARTUP_STR, strlen(MA_PROP_VALUE_SERVICE_STARTUP_STR))) {
			(void)enforce_policy_on_service_startup(self) ;
        } else if (!strncmp(reason, MA_PROP_VALUE_POLICY_FORCED_ENFORCEMENT_STR, strlen(MA_PROP_VALUE_POLICY_FORCED_ENFORCEMENT_STR))) {
			if (MA_OK == (rc = create_policy_timeout_payload(self, &send_payload))) {
                rc = publish_policy_notification(self, MA_PROP_VALUE_NOTIFICATION_FORCED_ENFORCEMENT_STR, send_payload);
                (void)ma_variant_release(send_payload) ;
            }
		}else {
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Sorry! I am not handling the enforement reason <%s>", reason);
            rc = MA_ERROR_POLICY_INTERNAL;
        }

        request_status = (MA_OK == rc) ? MA_OK : MA_ERROR_POLICY_INTERNAL;

        /* Reply status to clients */
        if (MA_OK == (rc = ma_message_create(&reply))) {
            rc = reply_to_clients(self, request_status, reply, c_request);
            (void)ma_message_release(reply);
        } else {
           MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to create reply message");
        }
    }

    return rc;
}

void array_foreach_cb(size_t index, ma_variant_t *value, void *cb_args, ma_bool_t *stop_loop) {
    ma_policy_service_t *self = (ma_policy_service_t *)cb_args;
    ma_buffer_t *buffer = NULL;
    if (MA_OK == ma_variant_get_string_buffer(value, &buffer)) {
        const char *user = NULL;
        size_t size = 0;
        (void)ma_buffer_get_string(buffer, &user, &size);
        if (user) {
            ma_policy_user_info_add(self->user_info, user);
        }
        (void) ma_buffer_release(buffer);
    }
}

static ma_error_t refresh_policies_handle(ma_policy_service_t *self, ma_message_t *request_msg, ma_msgbus_client_request_t *c_request) {
    ma_error_t rc = MA_OK, request_status = MA_OK;
    ma_message_t *reply = NULL;
    ma_variant_t *recv_payload = NULL;
    const char *force_policy_enforcement = NULL ;

    /* SoftwareID may also be part of the recieved payload, ignoring the software ID details */

    if (MA_OK == (rc = ma_message_get_payload(request_msg, &recv_payload))) {
        ma_table_t  *prop_table = NULL;
        if (MA_OK == (rc = ma_variant_get_table(recv_payload, &prop_table))) {
            ma_variant_t *value = NULL;
            if ((MA_OK == (rc = ma_table_get_value(prop_table, USER_LIST_STR, &value))) && value) {
                ma_array_t *user_list = NULL;
                (void)ma_variant_get_array(value, &user_list);
                if (user_list) {
                    rc = ma_array_foreach(user_list, array_foreach_cb, self);
                    (void) ma_array_release(user_list);
                }
                (void) ma_variant_release(value);
            }
            (void) ma_table_release(prop_table);
        }
        (void) ma_variant_release(recv_payload);
    }

    (void)ma_message_get_property(request_msg, MA_PROP_KEY_FORCE_POLICY_ENFORCEMENT_STR, &force_policy_enforcement) ;
    /* The received message dosen't contain any information. so ignoring the message properties and payload here */
    request_status = ma_policy_manager_raise_alert(self->policy_manager, force_policy_enforcement? MA_TRUE : MA_FALSE);

    /* Reply status to clients */
    if (MA_OK == (rc = ma_message_create(&reply))) {
        rc = reply_to_clients(self, request_status, reply, c_request);
        (void)ma_message_release(reply);
    } else {
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to create reply message");
    }

    return rc;
}

static ma_error_t get_policies_handle(ma_policy_service_t *self, ma_message_t *request_msg, ma_msgbus_client_request_t *c_request, ma_bool_t need_full_policies) {
    ma_error_t rc = MA_OK, request_status = MA_OK;
    ma_message_t *reply = NULL;
    ma_variant_t *recv_payload = NULL;
    ma_variant_t *policy_data = NULL;

    MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Get Policies Handle");

    if (MA_OK == (rc = ma_message_get_payload(request_msg, &recv_payload))) {
        ma_table_t  *prop_table = NULL;
        if (MA_OK == (rc = ma_variant_get_table(recv_payload, &prop_table))) {
            size_t policies_count = 0;
            if ((MA_OK == (rc = ma_policy_service_get_policies(prop_table, MA_CONTEXT_GET_POLICY_DATABASE(self->context), &policies_count, &policy_data, need_full_policies)))) {
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Policies count <%d>", policies_count);
                if (policies_count <= 0) {
                    request_status = MA_ERROR_POLICY_NO_POLICIES;
                }
            }
            (void) ma_table_release(prop_table);
        }
        (void) ma_variant_release(recv_payload);
    }


    if (MA_OK != rc) {
        request_status = MA_ERROR_POLICY_GET_OPERATION_FAILED;
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Policy get operation failed rc = <%d>", rc);
    }

    MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_TRACE, "Preparing the reply message with request status <%d>", request_status);

    /* create payload here using the record set */
    if (MA_OK == (rc = ma_message_create(&reply))) {
        if (policy_data) {
            (void)ma_message_set_payload(reply, policy_data);
        }
        rc = reply_to_clients(self, request_status, reply, c_request);
        (void)ma_message_release(reply);
    } else {
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to create reply message");
    }

    if (policy_data) {
        (void)ma_variant_release(policy_data);
    }

    return rc;
}

ma_error_t set_policies_handle(ma_policy_service_t *self, ma_message_t *request_msg, ma_msgbus_client_request_t *c_request) {
    ma_error_t rc = MA_OK, request_status = MA_OK;
    ma_message_t *reply = NULL;
    ma_variant_t *recv_payload = NULL;

    MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Set Policies Handle");

    if (MA_OK == (rc = ma_message_get_payload(request_msg, &recv_payload))) {
        ma_array_t *payload_arr = NULL;
        if (MA_OK == (rc = ma_variant_get_array(recv_payload, &payload_arr))) {
            ma_variant_t *info_table_var = NULL, *policy_data = NULL;
            ma_table_t  *info_table = NULL;
            (void)ma_array_get_element_at(payload_arr, 1, &info_table_var);
            (void)ma_array_get_element_at(payload_arr, 2, &policy_data);
            if (MA_OK == (rc = ma_variant_get_table(info_table_var, &info_table))) {
                rc = ma_policy_service_set_policies(info_table, MA_CONTEXT_GET_POLICY_DATABASE(self->context), policy_data);
                (void) ma_table_release(info_table);
            }
                      if(info_table_var) (void) ma_variant_release(info_table_var);
                      if(policy_data) (void) ma_variant_release(policy_data);
                      (void) ma_array_release(payload_arr);
        }
        (void) ma_variant_release(recv_payload);
    }

    if (MA_OK != rc) {
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Policy set operation failed rc = <%d>", rc);
    }

    request_status = rc;

    /* create payload here using the record set */
    if (MA_OK == (rc = ma_message_create(&reply))) {
        rc = reply_to_clients(self, request_status, reply, c_request);
        (void)ma_message_release(reply);
    } else {
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to create reply message");
    }

    return rc;
}

static ma_error_t ma_policy_service_get_policies_by_uri(ma_policy_service_t *self, const char *product_id, ma_table_t *uri_table, ma_variant_t **policy_data) {
	if(self && uri_table && policy_data) {
        ma_error_t rc = MA_OK;
        ma_buffer_t *po_feature_buf = NULL, *po_category_buf = NULL, *po_type_buf = NULL, *po_name_buf = NULL, *pso_name_buf = NULL, *pso_param_int_buf = NULL, *pso_param_str_buf = NULL;
        const char *feature = NULL, *cetagory = NULL, *type = NULL, *name = NULL, *pso_name = NULL, *pso_param_int = NULL, *pso_param_str = NULL;
        ma_variant_t *value = NULL;
        size_t size = 0;
        ma_db_recordset_t *record_set = NULL;

        if(MA_OK == (rc = ma_table_get_value(uri_table, PO_FEATURE_STR, &value))) {
            if(MA_OK == (rc = ma_variant_get_string_buffer(value, &po_feature_buf)))
                rc = ma_buffer_get_string(po_feature_buf, &feature, &size);                
            (void)ma_variant_release(value); value = NULL; 
		}
                                
        if(MA_OK == ma_table_get_value(uri_table, PO_CATEGORY_STR, &value)) { 
            if(MA_OK == ma_variant_get_string_buffer(value, &po_category_buf))
                (void)ma_buffer_get_string(po_category_buf, &cetagory, &size);
            (void)ma_variant_release(value); value = NULL;
        }
                
        if(MA_OK == ma_table_get_value(uri_table, PO_TYPE_STR, &value)) {
            if(MA_OK == ma_variant_get_string_buffer(value, &po_type_buf))
                (void)ma_buffer_get_string(po_type_buf, &type, &size);
            (void)ma_variant_release(value); value = NULL;
        }
          
        if(MA_OK == ma_table_get_value(uri_table, PO_NAME_STR, &value)) { 
            if(MA_OK == ma_variant_get_string_buffer(value, &po_name_buf))
                (void)ma_buffer_get_string(po_name_buf, &name, &size);
            (void)ma_variant_release(value); value = NULL;                    
        }

		if(MA_OK == ma_table_get_value(uri_table, PSO_NAME_STR, &value)) { 
            if(MA_OK == ma_variant_get_string_buffer(value, &pso_name_buf))
                (void)ma_buffer_get_string(pso_name_buf, &pso_name, &size);
            (void)ma_variant_release(value); value = NULL;                    
        }

		if(MA_OK == ma_table_get_value(uri_table, PSO_PARAM_INT_STR, &value)) { 
            if(MA_OK == ma_variant_get_string_buffer(value, &pso_param_int_buf))
                (void)ma_buffer_get_string(pso_param_int_buf, &pso_param_int, &size);
            (void)ma_variant_release(value); value = NULL;                    
        }

		if(MA_OK == ma_table_get_value(uri_table, PSO_PARAM_STR_STR, &value)) { 
            if(MA_OK == ma_variant_get_string_buffer(value, &pso_param_str_buf))
                (void)ma_buffer_get_string(pso_param_str_buf, &pso_param_str, &size);
            (void)ma_variant_release(value); value = NULL;                    
        }

        if((MA_OK == rc) && feature && cetagory && type && name) {
			rc = db_get_product_policies_by_uri(MA_CONTEXT_GET_POLICY_DATABASE(self->context), product_id, feature, cetagory, type, name, pso_name, pso_param_int, pso_param_str, &record_set);
        }
        else
            rc = MA_ERROR_UNEXPECTED;
                
        if(po_feature_buf) (void)ma_buffer_release(po_feature_buf);
        if(po_category_buf) (void)ma_buffer_release(po_category_buf);
        if(po_type_buf) (void)ma_buffer_release(po_type_buf);
        if(po_name_buf) (void)ma_buffer_release(po_name_buf);
		if(pso_name_buf) (void)ma_buffer_release(pso_name_buf);
		if(pso_param_int_buf) (void)ma_buffer_release(pso_param_int_buf);
		if(pso_param_str_buf) (void)ma_buffer_release(pso_param_str_buf);


        if(MA_OK == rc) {        
			/* Only one record is returned for this query */ 
            if(MA_OK == (rc = ma_db_recordset_next(record_set))) {
				rc = ma_db_recordset_get_variant(record_set, 1, MA_VARTYPE_TABLE, policy_data);
            }
			if(record_set) (void)ma_db_recordset_release(record_set);
        }

        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t get_policies_by_uri(ma_policy_service_t *self, ma_message_t *request_msg, ma_msgbus_client_request_t *c_request) {
	ma_error_t rc = MA_OK, request_status = MA_OK;
    ma_message_t *reply = NULL;
    ma_variant_t *recv_payload = NULL;
    ma_variant_t *policy_data = NULL;

    MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Get Policies by URI Handle");

    if (MA_OK == (rc = ma_message_get_payload(request_msg, &recv_payload))) {
        ma_table_t  *prop_table = NULL;
        if (MA_OK == (rc = ma_variant_get_table(recv_payload, &prop_table))) {
			const char *product_id = NULL;
			ma_message_get_property(request_msg, MA_PROP_KEY_PRODUCT_ID_STR, &product_id);
			if(product_id) {
				rc = ma_policy_service_get_policies_by_uri(self, product_id, prop_table, &policy_data);
				if(MA_ERROR_NO_MORE_ITEMS == rc) {
					request_status = MA_ERROR_POLICY_NO_POLICIES;
					rc = MA_OK;
				}
				(void) ma_table_release(prop_table);
			}
			else
				rc = MA_ERROR_UNEXPECTED;
        }
        (void) ma_variant_release(recv_payload);
    }

    if (MA_OK != rc) {
        request_status = MA_ERROR_POLICY_GET_OPERATION_FAILED;
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Policy get operation failed rc = <%d>", rc);
    }

    MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_TRACE, "Preparing the reply message with request status <%d>", request_status);

    /* create payload here using the record set */
    if (MA_OK == (rc = ma_message_create(&reply))) {
        if (policy_data) {
            (void)ma_message_set_payload(reply, policy_data);
        }
        rc = reply_to_clients(self, request_status, reply, c_request);
        (void)ma_message_release(reply);
    } else {
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to create reply message");
    }

    if (policy_data) {
        (void)ma_variant_release(policy_data);
    }

    return rc;
}

static ma_error_t get_policy_sections_by_uri(ma_policy_service_t *self, ma_message_t *request_msg, ma_msgbus_client_request_t *c_request) {
	ma_error_t rc = MA_OK, request_status = MA_OK;
    ma_message_t *reply = NULL;
    ma_variant_t *recv_payload = NULL;
    ma_variant_t *policy_data = NULL;

    MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Get Policy sections by URI Handle");

    if (MA_OK == (rc = ma_message_get_payload(request_msg, &recv_payload))) {
        ma_table_t  *prop_table = NULL;
        if (MA_OK == (rc = ma_variant_get_table(recv_payload, &prop_table))) {  
			ma_variant_t *sections_var = NULL;
			const char *product_id = NULL;
			ma_message_get_property(request_msg, MA_PROP_KEY_PRODUCT_ID_STR, &product_id);
			if(product_id) {
				rc = ma_policy_service_get_policies_by_uri(self, product_id, prop_table, &sections_var);
				if(MA_ERROR_NO_MORE_ITEMS == rc) {
					request_status = MA_ERROR_POLICY_NO_POLICIES;
					rc = MA_OK;
				}
				else if(MA_OK == rc) {
					ma_table_t *sections_tb = NULL;
					if(MA_OK == (rc = ma_variant_get_table(sections_var, &sections_tb))){
						ma_array_t *sections_array = NULL;
						if(MA_OK == (rc = ma_table_get_keys(sections_tb, &sections_array))) {
							ma_variant_create_from_array(sections_array, &policy_data);
							(void) ma_array_release(sections_array);
						}
						ma_table_release(sections_tb);
					}
					(void) ma_variant_release(sections_var);
				}
				(void) ma_table_release(prop_table);
			}
			else
				rc = MA_ERROR_UNEXPECTED;
        }
        (void) ma_variant_release(recv_payload);
    }

    if (MA_OK != rc) {
        request_status = MA_ERROR_POLICY_GET_OPERATION_FAILED;
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Policy get operation failed rc = <%d>", rc);
    }

    MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_TRACE, "Preparing the reply message with request status <%d>", request_status);

    /* create payload here using the record set */
    if (MA_OK == (rc = ma_message_create(&reply))) {
        if (policy_data) {
            (void)ma_message_set_payload(reply, policy_data);
        }
        rc = reply_to_clients(self, request_status, reply, c_request);
        (void)ma_message_release(reply);
    } else {
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to create reply message");
    }

    if (policy_data) {
        (void)ma_variant_release(policy_data);
    }

    return rc;
}

static ma_error_t get_policies_by_section(ma_policy_service_t *self, ma_message_t *request_msg, ma_msgbus_client_request_t *c_request) {
	ma_error_t rc = MA_OK, request_status = MA_OK;
    ma_message_t *reply = NULL;
    ma_variant_t *recv_payload = NULL;
    ma_variant_t *policy_data = NULL;

    MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Get Policies by section and URI Handle");

    if (MA_OK == (rc = ma_message_get_payload(request_msg, &recv_payload))) {
        ma_table_t  *prop_table = NULL;
        if (MA_OK == (rc = ma_variant_get_table(recv_payload, &prop_table))) {  
			ma_variant_t *sections_var = NULL;
			const char *product_id = NULL, *section_name = NULL;
			ma_message_get_property(request_msg, MA_PROP_KEY_PRODUCT_ID_STR, &product_id);
			ma_message_get_property(request_msg, MA_PROP_KEY_SECTION_NAME_STR, &section_name);
			 
			if(product_id && section_name){ 
				rc = ma_policy_service_get_policies_by_uri(self, product_id, prop_table, &sections_var);
				if(MA_ERROR_NO_MORE_ITEMS == rc) {
					request_status = MA_ERROR_POLICY_NO_POLICIES;
					rc = MA_OK;
				}
				else if(MA_OK == rc) {
					ma_table_t *sections_tb = NULL;
					if(MA_OK == (rc = ma_variant_get_table(sections_var, &sections_tb))){
						rc = ma_table_get_value(sections_tb, section_name, &policy_data);
						ma_table_release(sections_tb);
					}
					(void) ma_variant_release(sections_var);
				}
				(void) ma_table_release(prop_table);
			}
			else
				rc = MA_ERROR_UNEXPECTED;
        }
        (void) ma_variant_release(recv_payload);
    }

    if (MA_OK != rc) {
        request_status = MA_ERROR_POLICY_GET_OPERATION_FAILED;
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Policy get operation failed rc = <%d>", rc);
    }

    MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_TRACE, "Preparing the reply message with request status <%d>", request_status);

    /* create payload here using the record set */
    if (MA_OK == (rc = ma_message_create(&reply))) {
        if (policy_data) {
            (void)ma_message_set_payload(reply, policy_data);
        }
        rc = reply_to_clients(self, request_status, reply, c_request);
        (void)ma_message_release(reply);
    } else {
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to create reply message");
    }

    if (policy_data) {
        (void)ma_variant_release(policy_data);
    }

    return rc;
}
