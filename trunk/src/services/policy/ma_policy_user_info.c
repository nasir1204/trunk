#include "ma/internal/services/policy/ma_policy_user_info.h"
#include "ma/internal/services/policy/ma_policy_db.h"
#include "ma/sensor/ma_user_sensor.h"
#include "ma/internal/utils/datastructures/ma_queue.h"
#include "ma/sensor/ma_sensor_tray.h"
#include "ma/sensor/ma_sensor.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/configurator/ma_configurator.h"

#include "ma/internal/ma_strdef.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef MA_WINDOWS
#include <strings.h> /*strcasecmp*/
#endif


#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "policy"

typedef struct ma_policy_user_s {
    ma_queue_t          qlink;

    char                *user;
} ma_policy_user_t;

struct ma_policy_user_info_s {
    ma_context_t        *context;

    ma_queue_t          qhead;
};

ma_error_t ma_policy_user_info_create(ma_context_t *context, ma_policy_user_info_t **user_info) {
    if(context && user_info) {
        ma_policy_user_info_t *self = (ma_policy_user_info_t *)calloc(1, sizeof(ma_policy_user_info_t));
        if(self) {
            self->context = context;
            ma_queue_init(&self->qhead);
            *user_info = self;
            return MA_OK;
        }
        return MA_ERROR_OUTOFMEMORY;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_policy_user_info_release(ma_policy_user_info_t *self) {
    if(self) {
        free(self);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_bool_t ma_policy_user_info_search(ma_policy_user_info_t *self, const char *user) {
    if(self && user) {
        ma_queue_t *item;
        ma_queue_foreach(item, &self->qhead) {
            ma_policy_user_t *entry = ma_queue_data(item, ma_policy_user_t, qlink);
            if(entry && !MA_WIN_SELECT(stricmp,strcasecmp)(entry->user, user)) {
                return MA_TRUE;                
            }
        }
    }
    return MA_FALSE;
}

ma_bool_t ma_policy_user_info_add(ma_policy_user_info_t *self, const char *user) {
    if(self && user) {
        /* Ignore if user is already in list */
        if(!ma_policy_user_info_search(self, user)) {
            ma_policy_user_t *entry = (ma_policy_user_t *)calloc(1, sizeof(ma_policy_user_t));
            if(entry) {
                entry->user = strdup(user);
                ma_queue_insert_tail(&self->qhead, &entry->qlink);
                return MA_OK;
            }
            return MA_ERROR_OUTOFMEMORY;
        }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_bool_t ma_policy_user_info_remove(ma_policy_user_info_t *self, const char *user) {
    if(self && user) {
        ma_queue_t *item;
        ma_queue_foreach(item, &self->qhead) {
            ma_policy_user_t *entry = ma_queue_data(item, ma_policy_user_t, qlink);
            if(entry && !MA_WIN_SELECT(stricmp,strcasecmp)(entry->user, user)) {
                ma_queue_remove(&entry->qlink);
                free(entry->user);
                break;             
            }
        }
        return MA_OK;    
    }
    return MA_ERROR_INVALIDARG;
}

/* TBD - This same code is there in policy service, will be moved to common place */
static ma_bool_t is_local_user(ma_context_t *context, const char *domain_name){
    ma_bool_t b_result = MA_TRUE;
	if(context && domain_name){
        ma_buffer_t *buffer = NULL;				
        ma_ds_t *ds = ma_configurator_get_datastore(MA_CONTEXT_GET_CONFIGURATOR(context));
         if(MA_OK == ma_ds_get_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_COMPUTER_NAME_STR, &buffer) ) {
            const char *value = NULL;
            size_t size = 0;
            ma_buffer_get_string(buffer, &value, &size);
            b_result = !MA_WIN_SELECT(stricmp,strcasecmp)(value, domain_name);
            (void) ma_buffer_release(buffer);
        }
        
         if(!b_result) b_result = !MA_WIN_SELECT(stricmp,strcasecmp)("WORKGROUP", domain_name);       
	}
	return b_result;
}

static ma_error_t refresh_from_policy_db(ma_policy_user_info_t *self) {
    ma_db_recordset_t *record_set = NULL;
    ma_configurator_t *configurator = MA_CONTEXT_GET_CONFIGURATOR(self->context);
    ma_db_t *policy_db = ma_configurator_get_policies_database(configurator);
    ma_error_t rc = MA_OK;

    if(MA_OK == (rc = db_get_assignment_params(policy_db, "user", &record_set))) {
        while((MA_OK == rc) && (MA_ERROR_NO_MORE_ITEMS != (rc = ma_db_recordset_next(record_set)))) {
            const char *user_name = NULL;
            if(MA_OK != rc) continue;
            (void) ma_db_recordset_get_string(record_set, 1, &user_name);
            if(user_name)
                (void) ma_policy_user_info_add(self, user_name);
        }
        (void) ma_db_recordset_release(record_set);
    }
    return rc;
}

static ma_error_t refresh_from_sensor(ma_policy_user_info_t *self) {
    ma_error_t rc = MA_OK;
    ma_user_sensor_t *user_sensor = NULL;

    if(MA_OK == (rc = ma_sensor_tray_find_by_type((ma_sensor_tray_t*)MA_CONTEXT_GET_SENSOR(self->context), MA_SENSOR_TYPE_USER, (ma_sensor_t**)&user_sensor))) {
        ma_user_sensor_msg_t *user_sensor_msg = NULL;
	    if(MA_OK == (rc = ma_user_sensor_get_all_logged_on_users(user_sensor, &user_sensor_msg))) {
            size_t i = 0, n_users = 0;
            (void) ma_user_sensor_msg_get_size(user_sensor_msg, &n_users);
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "number of logged on users = %d", n_users);
            for(i = 0; i < n_users; i++) {                
                ma_buffer_t *domain_buf = NULL, *user_buf = NULL;				
                if(MA_OK == (rc = ma_user_sensor_msg_get_domain(user_sensor_msg, i, &domain_buf))) {
                    if(MA_OK == (rc = ma_user_sensor_msg_get_user(user_sensor_msg, i, &user_buf))) {
                        const char *domain = NULL, *user = NULL;				
				        size_t size = 0;                        
			            (void) ma_buffer_get_string(domain_buf, &domain, &size);
                        (void) ma_buffer_get_string(user_buf, &user, &size);
                        if(domain && user) {
                            char user_name[MA_MAX_LEN] = {0};
                            MA_MSC_SELECT(_snprintf, snprintf)(user_name, MA_MAX_LEN, "%s\\%s", domain, user);                                 
                            if(!is_local_user(self->context, domain)) {
                                (void) ma_policy_user_info_add(self, user_name);
                            }
                            else
                                MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "<%s> is local user", user_name);
                        }
                        (void) ma_buffer_release(user_buf);
                    }
                    (void) ma_buffer_release(domain_buf);
                }
            }
            (void) ma_user_sensor_msg_release(user_sensor_msg);
        }
        (void) ma_sensor_release((ma_sensor_t*)user_sensor);
    }
    return rc;
}

ma_error_t ma_policy_user_info_refresh(ma_policy_user_info_t *self, ma_bool_t is_refresh_from_sensor) {
    if(self) {
        return (is_refresh_from_sensor) ? refresh_from_sensor(self) :  refresh_from_policy_db(self);
    }
    return MA_ERROR_INVALIDARG;

}

ma_error_t ma_policy_user_info_foreach(ma_policy_user_info_t *self, ma_policy_user_info_foreach_cb_t cb, void *cb_data) {
    if(self && cb) {
        ma_error_t rc = MA_OK;
        ma_queue_t *item;
        ma_queue_foreach(item, &self->qhead) {
            ma_policy_user_t *entry = ma_queue_data(item, ma_policy_user_t, qlink);
            if(entry && entry->user) {
                rc = cb(entry->user, cb_data);
            }
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}
