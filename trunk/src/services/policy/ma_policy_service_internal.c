#include "ma/internal/services/policy/ma_policy_service_internal.h"
#include "ma/internal/services/policy/ma_policy_db.h"
#include "ma/internal/defs/ma_policy_defs.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal/utils/platform/ma_sys_apis.h"

#include <stdio.h>
#include <stdlib.h>

ma_error_t ma_policy_service_get_policies(ma_table_t *uri_table, ma_db_t *policy_db, size_t *policies_count, ma_variant_t **policy_data, ma_bool_t need_full_policies) {
    if(uri_table && policy_db && policies_count && policy_data) {
        ma_error_t rc = MA_OK;
        ma_buffer_t *sw_id_buf = NULL, *ver_buf = NULL, *user_buf = NULL, *loc_buf = NULL;
        const char *sw_id = NULL, *version = NULL, *user = NULL, *location = NULL;
        ma_variant_t *value = NULL;
        size_t size = 0;
        ma_db_recordset_t *record_set = NULL;

        if(MA_OK == (rc = ma_table_get_value(uri_table, SOFTWARE_ID_STR, &value))) {  /* For all policies */
            if(MA_OK == (rc = ma_variant_get_string_buffer(value, &sw_id_buf)))
                rc = ma_buffer_get_string(sw_id_buf, &sw_id, &size);
                
            (void)ma_variant_release(value); value = NULL; 
                                
            /* TBD - version is always NULL now, as it is net set on client side. it is a placeholder for policies based on version (in policy change notification scenarios)*/
            if(MA_OK == ma_table_get_value(uri_table, VERSION_STR, &value)) {  /* For Policy change */
                if(MA_OK == ma_variant_get_string_buffer(value, &ver_buf))
                    (void)ma_buffer_get_string(ver_buf, &version, &size);
                (void)ma_variant_release(value); value = NULL;
            }
                
            if(MA_OK == ma_table_get_value(uri_table, USER_ID_STR, &value)) {  /* For User based policies */ 
                if(MA_OK == ma_variant_get_string_buffer(value, &user_buf))
                    (void)ma_buffer_get_string(user_buf, &user, &size);
                (void)ma_variant_release(value); value = NULL;
            }
          
            if(MA_OK == ma_table_get_value(uri_table, LOCATION_ID_STR, &value)) {   /* For location based policies */
                if(MA_OK == ma_variant_get_string_buffer(value, &loc_buf))
                    (void)ma_buffer_get_string(loc_buf, &location, &size);
                (void)ma_variant_release(value); value = NULL;                    
            }

            if((MA_OK == rc) && sw_id) {
                if(user) {
					rc = db_get_product_policies(policy_db, sw_id, version, "user", user, &record_set);
                }
                else if(location) {
                    /* TBD - there is no support on location based policies. it is just a place holder for that */
                    //rc = db_get_product_policies(self->db, sw_id, version, "location", location, &record_set) ;  
                }
                else
                    rc = db_get_product_policies(policy_db, sw_id, version, NULL, NULL, &record_set);
            }
            else
                rc = MA_ERROR_UNEXPECTED;
                
            if(sw_id_buf) (void)ma_buffer_release(sw_id_buf);
            if(ver_buf) (void)ma_buffer_release(ver_buf);
            if(user_buf) (void)ma_buffer_release(user_buf);
            if(loc_buf) (void)ma_buffer_release(loc_buf);                
        }

        if(MA_OK == rc) {
            ma_array_t *pol_array = NULL;
        
            if(MA_OK == (rc = ma_array_create(&pol_array))) {        
                while((MA_OK == rc) && record_set && (MA_ERROR_NO_MORE_ITEMS != (rc = ma_db_recordset_next(record_set)))) {
                    ma_array_t *arr = NULL;
                    ma_variant_t *value = NULL, *prop_var = NULL, *sec_var = NULL , *arr_entry = NULL;
                    const char *feature = NULL, *category = NULL, *type = NULL, *name = NULL, *pso_name = NULL, *pso_param_int = NULL, *pso_param_str = NULL;
                    ma_table_t *prop_tb = NULL;

                    if(MA_OK != rc)
                        break;

                    if(MA_OK == (rc = ma_array_create(&arr))) {
                        if(MA_OK == (rc = ma_table_create(&prop_tb))) {
                            if((MA_OK == (rc =	ma_db_recordset_get_string(record_set, 1, &feature))) &&
                               (MA_OK == (rc = ma_db_recordset_get_string(record_set, 2, &category))) &&
                               (MA_OK == (rc = ma_db_recordset_get_string(record_set, 3, &type))) &&
                               (MA_OK == (rc = ma_db_recordset_get_string(record_set, 4, &name))) &&
							   (MA_OK == (rc = ma_db_recordset_get_string(record_set, 5, &pso_name))) &&
							   (MA_OK == (rc = ma_db_recordset_get_variant(record_set, 8, MA_VARTYPE_TABLE, &sec_var)))) {
							
							   (void) ma_db_recordset_get_string(record_set, 6, &pso_param_int);
							   (void) ma_db_recordset_get_string(record_set, 7, &pso_param_str);                               
                        
                                if(MA_OK == (rc = ma_variant_create_from_string(feature, strlen(feature), &value))) {
                                    rc = ma_table_add_entry(prop_tb, PO_FEATURE_STR, value);
                                    (void)ma_variant_release(value); value = NULL;
                                }
                  
                                if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(category, strlen(category), &value)))) {
                                    rc = ma_table_add_entry(prop_tb, PO_CATEGORY_STR, value);
                                    (void)ma_variant_release(value); value = NULL;
                                }
                  
                                if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(type, strlen(type), &value)))) {
                                    rc = ma_table_add_entry(prop_tb, PO_TYPE_STR, value);
                                    (void)ma_variant_release(value); value = NULL;
                                }
                  
                                if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(name, strlen(name), &value)))) {
                                    rc = ma_table_add_entry(prop_tb, PO_NAME_STR, value);
                                    (void)ma_variant_release(value); value = NULL;
                                }
                  
								if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(pso_name, strlen(pso_name), &value)))) {
                                    rc = ma_table_add_entry(prop_tb, PSO_NAME_STR, value);
                                    (void)ma_variant_release(value); value = NULL;
                                }

								if((MA_OK == rc) &&  pso_param_int && (MA_OK == (rc = ma_variant_create_from_string(pso_param_int, strlen(pso_param_int), &value)))) {
                                    rc = ma_table_add_entry(prop_tb, PSO_PARAM_INT_STR, value);
                                    (void)ma_variant_release(value); value = NULL;
                                }

								if((MA_OK == rc) && pso_param_str && (MA_OK == (rc = ma_variant_create_from_string(pso_param_str, strlen(pso_param_str), &value)))) {
                                    rc = ma_table_add_entry(prop_tb, PSO_PARAM_STR_STR, value);
                                    (void)ma_variant_release(value); value = NULL;
                                }

								/* If full policies aren ot required, set empty data */
								if(!need_full_policies) {
									ma_table_t *empty_sec_tb = NULL;
									(void)ma_variant_release(sec_var), sec_var = NULL;
									if(MA_OK == (rc = ma_table_create(&empty_sec_tb))) {
										rc = ma_variant_create_from_table(empty_sec_tb, &sec_var);
										(void) ma_table_release(empty_sec_tb);
									}
								}

                                if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_table(prop_tb, &prop_var)))) {                            
                                    if(MA_OK == (rc = ma_array_push(arr, prop_var))) {
                                        if(MA_OK == (rc = ma_array_push(arr, sec_var))) {                                    
                                            if(MA_OK == (rc = ma_variant_create_from_array(arr ,&arr_entry))) {
                                                rc = ma_array_push(pol_array, arr_entry);
                                                (void)ma_variant_release(arr_entry);
                                            }
                                        }                        
                                    }
                                    (void)ma_variant_release(prop_var);
                                }
                                (void)ma_variant_release(sec_var);
                            }
                            (void)ma_table_release(prop_tb);
                        }
                        (void)ma_array_release(arr);
                    }            
                }   
			    if(MA_ERROR_NO_MORE_ITEMS == rc)
				    rc = MA_OK;
            }
            
            if(MA_OK == rc) {
                (void)ma_array_size(pol_array, policies_count);            
                if(*policies_count > 0) 
                    rc = ma_variant_create_from_array(pol_array, policy_data);            
            }    
            if(pol_array) (void)ma_array_release(pol_array);
			if(record_set) (void)ma_db_recordset_release(record_set);
        }

        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

/*NOTE: This is used for updating local policies */
ma_error_t ma_policy_service_set_policies(ma_table_t *uri_table, ma_db_t *policy_db, ma_variant_t *policy_data) {
 if(uri_table && policy_db && policy_data) {
        ma_error_t rc = MA_OK;
        size_t policies_count = 0, policy_index = 1;
        ma_array_t *policy_arr = NULL;

        (void)ma_variant_get_array(policy_data, &policy_arr);
        (void)ma_array_size(policy_arr, &policies_count);

        while((MA_OK == rc) && (policies_count >= policy_index)) {
            ma_variant_t *policy_var = NULL;
            (void) ma_array_get_element_at(policy_arr, policy_index, &policy_var);
            if(policy_var) {
                ma_array_t *table_array = NULL;
                /* Expecting array of two tables, first one is pso properties table and another is section data table */
                if(MA_OK == (rc = ma_variant_get_array(policy_var, &table_array))) {
                    size_t size = 0;
                    (void) ma_array_size(table_array, &size);
                    if(2 == size) {   
                        ma_variant_t *props_var=NULL, *sections_var=NULL;
                        (void) ma_array_get_element_at(table_array, 1, &props_var);
                        (void) ma_array_get_element_at(table_array, 2, &sections_var);
                        if(props_var && sections_var) { 
                            ma_table_t *props_table=NULL;
                            ma_table_t *sections_table = NULL;
                            (void) ma_variant_get_table(props_var, &props_table);
                            (void) ma_variant_get_table(sections_var, &sections_table);
                            if(props_table && sections_table) {
                                ma_variant_t *value = NULL;
                                ma_buffer_t *sw_id_buf = NULL, *ver_buf = NULL, *user_buf = NULL, *loc_buf = NULL;
                                const char *sw_id = NULL, *version = NULL, *user = NULL, *location = NULL;
                                ma_buffer_t *feature_buf = NULL, *category_buf = NULL, *type_buf = NULL, *name_buf = NULL, *pso_name_buf = NULL, *pso_param_int_buf = NULL, *pso_param_str_buf = NULL;
                                const char *feature = NULL, *category = NULL, *type = NULL, *name = NULL, *pso_name = NULL, *pso_param_int = NULL, *pso_param_str = NULL;

                                if(MA_OK == (rc = ma_table_get_value(uri_table, SOFTWARE_ID_STR, &value))) {  /* For all policies */
                                    if(MA_OK == (rc = ma_variant_get_string_buffer(value, &sw_id_buf)))
                                        rc = ma_buffer_get_string(sw_id_buf, &sw_id, &size);                
                                    (void)ma_variant_release(value); value = NULL; 
                                
                                    if(MA_OK == ma_table_get_value(uri_table, VERSION_STR, &value)) {  /* For Policy change */
                                        if(MA_OK == ma_variant_get_string_buffer(value, &ver_buf))
                                            (void)ma_buffer_get_string(ver_buf, &version, &size);
                                        (void)ma_variant_release(value); value = NULL;
                                    }
                
                                    if(MA_OK == ma_table_get_value(uri_table, USER_ID_STR, &value)) {  /* For User based policies */ 
                                        if(MA_OK == ma_variant_get_string_buffer(value, &user_buf))
                                            (void)ma_buffer_get_string(user_buf, &user, &size);
                                        (void)ma_variant_release(value); value = NULL;
                                    }                                   
                
                                    if(MA_OK == ma_table_get_value(uri_table, LOCATION_ID_STR, &value)) {   /* For location based policies */
                                        if(MA_OK == ma_variant_get_string_buffer(value, &loc_buf))
                                            (void)ma_buffer_get_string(loc_buf, &location, &size);
                                        (void)ma_variant_release(value); value = NULL;                    
                                    }

                                    if(MA_OK == ma_table_get_value(props_table, PO_FEATURE_STR, &value)) {  
                                        if(MA_OK == ma_variant_get_string_buffer(value, &feature_buf))
                                            (void)ma_buffer_get_string(feature_buf, &feature, &size);
                                        (void)ma_variant_release(value); value = NULL;
                                    }
                
                                    if(MA_OK == ma_table_get_value(props_table, PO_CATEGORY_STR, &value)) {   
                                        if(MA_OK == ma_variant_get_string_buffer(value, &category_buf))
                                            (void)ma_buffer_get_string(category_buf, &category, &size);
                                        (void)ma_variant_release(value); value = NULL;
                                    }

                                    if(MA_OK == ma_table_get_value(props_table, PO_TYPE_STR, &value)) {  
                                        if(MA_OK == ma_variant_get_string_buffer(value, &type_buf))
                                            (void)ma_buffer_get_string(type_buf, &type, &size);
                                        (void)ma_variant_release(value); value = NULL;                                                
                                    }
                
                                    if(MA_OK == ma_table_get_value(props_table, PO_NAME_STR, &value)) {   
                                        if(MA_OK == ma_variant_get_string_buffer(value, &name_buf))
                                            (void)ma_buffer_get_string(name_buf, &name, &size);
                                        (void)ma_variant_release(value); value = NULL;                    
                                    }

									if(MA_OK == ma_table_get_value(props_table, PSO_NAME_STR, &value)) {   
                                        if(MA_OK == ma_variant_get_string_buffer(value, &pso_name_buf))
                                            (void)ma_buffer_get_string(pso_name_buf, &pso_name, &size);
                                        (void)ma_variant_release(value); value = NULL;                    
                                    }

									if(MA_OK == ma_table_get_value(props_table, PSO_PARAM_INT_STR, &value)) {   
                                        if(MA_OK == ma_variant_get_string_buffer(value, &pso_param_int_buf))
                                            (void)ma_buffer_get_string(pso_param_int_buf, &pso_param_int, &size);
                                        (void)ma_variant_release(value); value = NULL;                    
                                    }

									if(MA_OK == ma_table_get_value(props_table, PSO_PARAM_STR_STR, &value)) {   
                                        if(MA_OK == ma_variant_get_string_buffer(value, &pso_param_str_buf))
                                            (void)ma_buffer_get_string(pso_param_str_buf, &pso_param_str, &size);
                                        (void)ma_variant_release(value); value = NULL;                    
                                    }
									
                                    if((MA_OK == rc) && sw_id && feature && category && type) {
                                        if(MA_OK == (rc = db_add_policy_instance(policy_db))) {
                                            if(MA_OK == (rc = ma_db_transaction_begin(policy_db))) {                         
                                                char method[MA_MAX_LEN] = {0};
                                                char param[MA_MAX_LEN] = {0};
                                                if(user) { 
                                                    strcpy(method, "user");
                                                    strcpy(param, user); 
                                                }
                                                else if(location) {
                                                    /* TBD - there is no support on location based policies. it is just a place holder for that */
                                                }                        

                                                if(MA_OK == rc) {
                                                    ma_db_recordset_t *records = NULL;                                                    
                                                    char pso_id[MA_MAX_LEN] = {0};                                                   
                                                    ma_bool_t  is_policy_object_avaiable = MA_FALSE;                                                   
                                                  
                                                    if(MA_OK == (rc = db_get_policy_setting_object_id(policy_db, sw_id, feature, category, type, name, pso_name, pso_param_int, pso_param_str, &records))) {
                                                        /* expecting only one record for this query */
                                                        if(MA_ERROR_NO_MORE_ITEMS != (rc = ma_db_recordset_next(records))) {
                                                            const char *pso_id_tmp = NULL;

                                                            if(MA_OK == (rc = ma_db_recordset_get_string(records, 1, &pso_id_tmp))){   
                                                                    strncpy(pso_id, pso_id_tmp, MA_MAX_LEN-1);                                                                                                                                      
                                                                    is_policy_object_avaiable = MA_TRUE;
                                                            }
                                                        }

                                                        if(MA_ERROR_NO_MORE_ITEMS == rc) rc = MA_OK;                                                                                                            
                                                        if(records) (void)ma_db_recordset_release(records);
                                                    }

                                                   
                                                    if(MA_OK == rc) {
                                                        if(is_policy_object_avaiable) {
                                                            rc = db_update_policy_setting_object_data(policy_db, sections_var, pso_id);
                                                        }
                                                        else {
                                                            char po_id[MA_MAX_LEN] = {0};
                                                            MA_MSC_SELECT(_snprintf, snprintf)(po_id, MA_MAX_LEN, "local_%lu", ma_sys_rand() );
                                                            MA_MSC_SELECT(_snprintf, snprintf)(pso_id, MA_MAX_LEN, "local_%lu", ma_sys_rand());
                                                            /* digest , is_legacy and version are "0" s for locally import policies */
                                                            if(MA_OK == (rc = db_add_policy_object(policy_db, po_id, name, feature, category, type, "0", "0"))) {
                                                                if(MA_OK == (rc = db_add_policy_settings_object(policy_db, po_id, pso_id, pso_name ? pso_name : name , "0", sections_var, pso_param_int? pso_param_int : "0", pso_param_str ? pso_param_str : "", (version) ? version : "0"))) {
                                                                    rc = db_add_policy_product_assignment(policy_db, po_id, sw_id, (user || location) ? method : NULL, (user || location) ? param : NULL );
                                                                }
                                                            }
                                                        }
                                                    }                                                   
                                                }
                                                rc = (MA_OK == rc) ? ma_db_transaction_end(policy_db) : ma_db_transaction_cancel(policy_db) ;
                                            }
                                        }
                                    }
                                    else
                                        rc = MA_ERROR_UNEXPECTED;
                
                                    if(sw_id_buf) (void)ma_buffer_release(sw_id_buf);
                                    if(ver_buf) (void)ma_buffer_release(ver_buf);
                                    if(user_buf) (void)ma_buffer_release(user_buf);
                                    if(loc_buf) (void)ma_buffer_release(loc_buf);   
                                    if(feature_buf) (void)ma_buffer_release(feature_buf);   
                                    if(category_buf) (void)ma_buffer_release(category_buf);   
                                    if(type_buf) (void)ma_buffer_release(type_buf);   
                                    if(name_buf) (void)ma_buffer_release(name_buf);
									if(pso_name_buf) (void)ma_buffer_release(pso_name_buf);
									if(pso_param_int_buf) (void)ma_buffer_release(pso_param_int_buf);
									if(pso_param_str_buf) (void)ma_buffer_release(pso_param_str_buf);
                                }                                
                                ma_table_release(sections_table);
								ma_table_release(props_table);    
                            }
                            ma_variant_release(sections_var); 
							ma_variant_release(props_var);       
                        }
                    }
                    (void)ma_array_release(table_array);
                }
                (void)ma_variant_release(policy_var);
            }
            policy_index++;
        }

		if(policy_arr) ma_array_release(policy_arr);
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

