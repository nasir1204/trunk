#include "ma/ma_variant.h"
#include "ma/internal/services/policy/ma_policy_manifest.h"
#include "ma/internal/utils/xml/ma_xml.h"

#include "ma/internal/ma_macros.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Manifest XML macros */
#define MA_MANIFEST_ROOT_STR                     "Manifest"
#define MA_MANIFEST_HANDLER                      "handler"
#define MA_MANIFEST_KEYHASH                      "keyhash"
#define MA_MANIFEST_CREATED                      "created"
#define MA_MANIFEST_POLICY_OBJECT_PATH_STR       "Manifest/PolicyRoot/Policies/Policy"
#define MA_MANIFEST_POLICY_ASSIGNMENT_PATH_STR   "Manifest/PolicyRoot/Assignments/Assignment"
#define MA_MANIFEST_ID_STR                       "id"
#define MA_MANIFEST_DIGEST_STR                   "digest"
#define MA_MANIFEST_PRODUCT_STR                  "product"
#define MA_MANIFEST_METHOD_STR                   "method"
#define MA_MANIFEST_PARAM_STR                    "param"
#define MA_MANIFEST_FEATURE_STR                  "feature"
#define MA_MANIFEST_CATEGORY_STR                 "category"
#define MA_MANIFEST_TYPE_STR                     "type"
#define MA_MANIFEST_NAME_STR                     "name"
#define MA_MANIFEST_LEGACY_STR                   "legacy"

#define MA_MANIFEST_TASK_ASSIGNMENT_PATH_STR     "Manifest/TaskRoot/Assignments/Assignment"
#define MA_MANIFEST_TASK_OBJECT_PATH_STR         "Manifest/TaskRoot/Tasks/Task"
#define MA_MANIFEST_AGENT_META_DATA_PATH_STR     "Manifest/AgentMeta"

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "policy"

/* TBD - Add logs and proper error codes */

struct ma_policy_manifest_s {   
    ma_xml_t                    *manifest_xml;
    char                        *handler;
    char                        *keyhash;
    char                        *created_time;
    ma_bool_t                   is_policy_data;
    ma_table_t                  *assign_table;      /* key  is combination of all fields of the assignment and value is assignments_object_t */
    ma_table_t                  *po_table;          /* key  is po id and value is policy_object_t */
    ma_table_t                  *task_table;        /* key  is task id and value is task_object_t */  /* TBD - we can remove one table object and use only table based on type */
    ma_table_t                  *server_settings_table; /* Agent meta section */
} ;

ma_error_t ma_policy_manifest_create(ma_policy_manifest_t **policy_manifest) {
    if(policy_manifest){
        ma_policy_manifest_t *self = (ma_policy_manifest_t *)calloc(1, sizeof(ma_policy_manifest_t));
        if(self) {
            ma_error_t rc = MA_OK;
            if(MA_OK == (rc = ma_xml_create(&self->manifest_xml))) {               
                if(MA_OK == rc) {                    
                    self->is_policy_data = MA_TRUE;
                    *policy_manifest = self;
                    return MA_OK;
                }            
            }
            (void)ma_policy_manifest_release(self);
            return rc;
        }
        return MA_ERROR_OUTOFMEMORY;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_bool_t is_assignment_valid(ma_policy_manifest_t *self, const char *obj_id) {
    ma_bool_t rc = MA_FALSE;
    ma_variant_t *value = NULL;
    /* Object not found and any non MA_OK is treated as a assignment invalid */
    if(MA_OK == ma_table_get_value(self->is_policy_data?self->po_table:self->task_table, obj_id, &value)) {
        (void)ma_variant_release(value);
        rc = MA_TRUE;
    }
    return rc;
}

static ma_error_t load_server_settings(ma_policy_manifest_t *self, ma_xml_node_t *manifest_root) {
    ma_error_t rc = MA_OK;    

    if(MA_OK == (rc = ma_table_create(&self->server_settings_table))) {        
        ma_xml_node_t *agent_meta_node = ma_xml_node_search_by_path(manifest_root, MA_MANIFEST_AGENT_META_DATA_PATH_STR);   
        rc = MA_ERROR_POLICY_MANIFEST_INTERNAL;
        if(agent_meta_node) {
            ma_xml_node_t *node = ma_xml_node_get_child(agent_meta_node);
            if(node) {
                do { 
                    const char *key = ma_xml_node_get_name(node);
                    const char *value = ma_xml_node_get_data(node);
                    if(key && value) {
                        ma_variant_t *value_var = NULL;
                        if(MA_OK == (rc = ma_variant_create_from_string(value, strlen(value), &value_var))) {
                            rc = ma_table_add_entry(self->server_settings_table, key, value_var);
                            (void)ma_variant_release(value_var);
                        }
                    }                
                } while((MA_OK == rc) && (node = ma_xml_node_get_next_sibling(node)));
            }
        }
    }
    return rc;
}

static ma_error_t load_assignments(ma_policy_manifest_t *self, ma_xml_node_t *manifest_root) {
    ma_error_t rc = MA_OK;    
    ma_xml_node_t *node = ma_xml_node_search_by_path(manifest_root, (self->is_policy_data) ? MA_MANIFEST_POLICY_ASSIGNMENT_PATH_STR : MA_MANIFEST_TASK_ASSIGNMENT_PATH_STR); 
    while((MA_OK == rc) && node) {              
        assignments_object_t assign_data;
        ma_variant_t *assign_var = NULL;        

        memset(&assign_data, 0 ,sizeof(assignments_object_t));
        assign_data.obj_id = (char *) ma_xml_node_attribute_get(node, MA_MANIFEST_ID_STR);
        assign_data.product = (char *) ma_xml_node_attribute_get(node, MA_MANIFEST_PRODUCT_STR);
        assign_data.method = (char *) ma_xml_node_attribute_get(node, MA_MANIFEST_METHOD_STR);
        assign_data.param = (char *) ma_xml_node_attribute_get(node, MA_MANIFEST_PARAM_STR);

        /* checking object id existance in the manifest file, if the object id is not available in objects list, the received file might be curropted */
        if(is_assignment_valid(self, assign_data.obj_id)) {
            if(assign_data.obj_id && assign_data.product) {
                char key[MA_MAX_LEN] = {0};
                MA_MSC_SELECT(_snprintf, snprintf)(key, MA_MAX_LEN, "%s|%s|%s|%s", assign_data.obj_id, assign_data.product, (assign_data.method)?assign_data.method:"0", (assign_data.param)?assign_data.param:"0");                        

                if(MA_OK == (rc = ma_variant_create_from_raw(&assign_data, sizeof(assignments_object_t), &assign_var))) {
                    rc = ma_table_add_entry(self->assign_table, key, assign_var);
                    (void)ma_variant_release(assign_var);
                }
            }
            else
                rc = MA_ERROR_POLICY_MANIFEST_INTERNAL;
        }
        else
            rc = MA_ERROR_POLICY_MANIFEST_INTERNAL;

        node = ma_xml_node_get_next_sibling(node);
    }
    return rc;
}

static ma_error_t load_policy_data(ma_policy_manifest_t *self, ma_xml_node_t *manifest_root) {
    ma_error_t rc = MA_OK;
    ma_xml_node_t *po_node = ma_xml_node_search_by_path(manifest_root, MA_MANIFEST_POLICY_OBJECT_PATH_STR); 
    while((MA_OK == rc) && po_node) {              
        policy_object_t po_data ;
        ma_xml_node_t *pso_node = NULL;
        ma_variant_t *po_data_var = NULL;

        memset(&po_data, 0 ,sizeof(policy_object_t));
        po_data.po_id = (char *)ma_xml_node_attribute_get(po_node, MA_MANIFEST_ID_STR);
        po_data.type = (char *)ma_xml_node_attribute_get(po_node, MA_MANIFEST_TYPE_STR);
        po_data.category = (char *)ma_xml_node_attribute_get(po_node, MA_MANIFEST_CATEGORY_STR);
        po_data.feature = (char *)ma_xml_node_attribute_get(po_node, MA_MANIFEST_FEATURE_STR);
        po_data.name = (char *)ma_xml_node_attribute_get(po_node, MA_MANIFEST_NAME_STR);
        po_data.digest = (char *)ma_xml_node_attribute_get(po_node, MA_MANIFEST_DIGEST_STR);
		po_data.legacy = (char *)ma_xml_node_attribute_get(po_node, MA_MANIFEST_LEGACY_STR);

		/* Not checking legacy filed existance in policies as it supports only from ePO 5.1 onwards 
		   To support ePO5.0, assigning "0" (i.e, object based policies) to "legacy" field for all policy objects */ 
        if(!po_data.po_id || !po_data.type || !po_data.category || !po_data.feature || !po_data.name || !po_data.digest)
            rc = MA_ERROR_POLICY_MANIFEST_INTERNAL;
        else
            pso_node = ma_xml_node_get_child(po_node);

		if(!po_data.legacy) po_data.legacy = "0";

        if((MA_OK == rc) && (MA_OK != (rc = ma_table_create(&po_data.pso_table))))
            rc = MA_ERROR_POLICY_MANIFEST_INTERNAL;

        while((MA_OK == rc) && pso_node) { 
            ma_variant_t *pso_data_var = NULL;
            policy_setting_object_t pso_data;
            
            memset(&pso_data, 0 ,sizeof(policy_setting_object_t));
            pso_data.pso_id = (char *)ma_xml_node_attribute_get(pso_node, MA_MANIFEST_ID_STR);                    
            pso_data.digest = (char *)ma_xml_node_attribute_get(pso_node, MA_MANIFEST_DIGEST_STR);
            pso_data.po_id = po_data.po_id;
            if(pso_data.pso_id && pso_data.digest) {
                if(MA_OK == (rc = ma_variant_create_from_raw(&pso_data, sizeof(policy_setting_object_t), &pso_data_var))) {
                    po_data.pso_total++;
                    rc = ma_table_add_entry(po_data.pso_table, pso_data.pso_id, pso_data_var);                        
                    (void)ma_variant_release(pso_data_var);
                }
            }
            else
                rc = MA_ERROR_POLICY_MANIFEST_INTERNAL;

            pso_node = ma_xml_node_get_next_sibling(pso_node);
        }
        
        if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_raw(&po_data, sizeof(policy_object_t), &po_data_var)))) {
            rc = ma_table_add_entry(self->po_table, po_data.po_id, po_data_var);
            (void)ma_variant_release(po_data_var);
        }
  
        po_node = ma_xml_node_get_next_sibling(po_node);
    }
    return rc;
}

static ma_error_t load_task_data(ma_policy_manifest_t *self, ma_xml_node_t *manifest_root) {
    ma_error_t rc = MA_OK;    
    ma_xml_node_t *node = ma_xml_node_search_by_path(manifest_root, MA_MANIFEST_TASK_OBJECT_PATH_STR); 
    while((MA_OK == rc) && node) {              
        task_object_t task_data;
        ma_variant_t *task_var = NULL;

        memset(&task_data, 0 ,sizeof(task_object_t));
        task_data.task_obj_id = (char *) ma_xml_node_attribute_get(node, MA_MANIFEST_ID_STR);
        task_data.name = (char *) ma_xml_node_attribute_get(node, MA_MANIFEST_NAME_STR);
        task_data.digest = (char *) ma_xml_node_attribute_get(node, MA_MANIFEST_DIGEST_STR);
        
        if(task_data.task_obj_id && task_data.name && task_data.digest) {
            if(MA_OK == (rc = ma_variant_create_from_raw(&task_data, sizeof(task_object_t), &task_var))) {
                rc = ma_table_add_entry(self->task_table, task_data.task_obj_id, task_var);
                (void)ma_variant_release(task_var);
            }
        }
        else
            rc = MA_ERROR_POLICY_MANIFEST_INTERNAL;

        node = ma_xml_node_get_next_sibling(node);
    }
    return rc;
}

ma_error_t ma_policy_manifest_load(ma_policy_manifest_t *self, unsigned char *manifest_data, size_t manifest_size, ma_bool_t is_policy_data) {
    if(self && manifest_data && manifest_size > 0) {        
        ma_xml_node_t *manifest_root = NULL;    
        ma_error_t rc = MA_OK;
        self->is_policy_data = is_policy_data;
        if(MA_OK == (rc = ma_xml_load_from_buffer(self->manifest_xml, (char *)manifest_data, manifest_size))) {
            /* Creates the tables only when xml loads successfully */
            if(MA_OK == (rc = ma_table_create(&self->assign_table))) {
                if(self->is_policy_data) {
                    rc = ma_table_create(&self->po_table);                        
                }
                else
                     rc = ma_table_create(&self->task_table);
            }

            if((MA_OK == rc) && (NULL != (manifest_root = ma_xml_get_node(self->manifest_xml)))) {
                ma_xml_node_t *node = ma_xml_node_search_by_path(manifest_root, MA_MANIFEST_ROOT_STR);
                if(node) {
                    self->handler = (char *)ma_xml_node_attribute_get(node, MA_MANIFEST_HANDLER);
                    self->keyhash = (char *)ma_xml_node_attribute_get(node, MA_MANIFEST_KEYHASH);
                    self->created_time = (char *)ma_xml_node_attribute_get(node, MA_MANIFEST_CREATED);
                }
                
                if(self->is_policy_data) 
                    rc = load_policy_data(self, manifest_root);
                else
                    rc = load_task_data(self, manifest_root);

                if(MA_OK == rc)
                    rc = load_assignments(self, manifest_root); 

                if(MA_OK == rc && self->is_policy_data)
                    rc = load_server_settings(self, manifest_root);
            }
        }
        return rc;    
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_policy_manifest_get_po(ma_policy_manifest_t *self, const char *po_id, const policy_object_t **po) {
    if(self && po_id && po) {
        ma_error_t rc = MA_OK;
        ma_variant_t *po_data_var = NULL;        
        if(MA_OK == (rc = ma_table_get_value(self->po_table, po_id, &po_data_var))) {
            ma_buffer_t *buff = NULL;
            if(MA_OK == (rc = ma_variant_get_raw_buffer(po_data_var, &buff))) {
                const unsigned char *data = NULL;
                size_t size = 0;
                if(MA_OK == (rc = ma_buffer_get_raw(buff, &data, &size)) && data) {
                    *po = (policy_object_t *)data;                    
                }
                (void) ma_buffer_release(buff);
            }
            (void)ma_variant_release(po_data_var);
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_policy_manifest_delete_po(ma_policy_manifest_t *self, const char *po_id) {
    if(self && po_id) { 
        ma_error_t rc = MA_OK;
        ma_variant_t *po_data_var = NULL;        
        if(MA_OK == (rc = ma_table_get_value(self->po_table, po_id, &po_data_var))) {
            ma_buffer_t *buff = NULL;
            if(MA_OK == (rc = ma_variant_get_raw_buffer(po_data_var, &buff))) {
                const unsigned char *data = NULL;
                size_t size = 0;
                if(MA_OK == (rc = ma_buffer_get_raw(buff, &data, &size)) && data) {
                    policy_object_t *po_data = (policy_object_t *)data; 
                    rc = ma_table_release(po_data->pso_table);
                }
                (void)ma_buffer_release(buff);
            }
            (void)ma_variant_release(po_data_var);
        }

        if(MA_OK == rc)
            rc =  ma_table_remove_entry(self->po_table, po_id);        

        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_policy_manifest_get_po_ids(ma_policy_manifest_t *self, ma_array_t **id_array) {
    if(self && id_array) {
        return ma_table_get_keys(self->po_table,  id_array);    
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_policy_manifest_get_pso(ma_policy_manifest_t *self, const char *po_id, const char *pso_id, const policy_setting_object_t **pso) {
    if(self && po_id && pso_id && pso) {
        ma_error_t rc = MA_OK;                
        ma_variant_t *po_data_var = NULL;        
        if(MA_OK == (rc = ma_table_get_value(self->po_table, po_id, &po_data_var))) {
            ma_buffer_t *buff = NULL;
            if(MA_OK == (rc = ma_variant_get_raw_buffer(po_data_var, &buff))) {
                const unsigned char *data = NULL;
                size_t size = 0;
                if(MA_OK == (rc = ma_buffer_get_raw(buff, &data, &size)) && data) {
                    policy_object_t *po_data = (policy_object_t *)data; 
                    ma_variant_t *pso_data_var = NULL;
                    if(MA_OK == (rc = ma_table_get_value(po_data->pso_table, pso_id, &pso_data_var))) {
                         ma_buffer_t *pso_buff = NULL;
                        if(MA_OK == (rc = ma_variant_get_raw_buffer(pso_data_var, &pso_buff))) {
                            const unsigned char *data = NULL; size_t size = 0;
                            if(MA_OK == (rc = ma_buffer_get_raw(pso_buff, &data, &size)) && data) {
                                *pso = (policy_setting_object_t *)data; 
                            }
                            (void) ma_buffer_release(pso_buff);
                        }                    
                        (void)ma_variant_release(pso_data_var);
                    }                    
                }
                (void) ma_buffer_release(buff);
            }
            (void)ma_variant_release(po_data_var);
        }
        return rc;        
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_policy_manifest_delete_pso(ma_policy_manifest_t *self, const char *po_id, const char *pso_id) {
    if(self && po_id && pso_id) {
        ma_error_t rc = MA_OK;                
        ma_variant_t *po_data_var = NULL;        
        if(MA_OK == (rc = ma_table_get_value(self->po_table, po_id, &po_data_var))) {
            ma_buffer_t *buff = NULL;
            if(MA_OK == (rc = ma_variant_get_raw_buffer(po_data_var, &buff))) {
                const unsigned char *data = NULL;
                size_t size = 0;
                if(MA_OK == (rc = ma_buffer_get_raw(buff, &data, &size)) && data) {
                    policy_object_t *po_data = (policy_object_t *)data; 
                    ma_variant_t *pso_data_var = NULL;
                    if(MA_OK == (rc = ma_table_get_value(po_data->pso_table, pso_id, &pso_data_var))) {
                         ma_buffer_t *pso_buff = NULL;
                        if(MA_OK == (rc = ma_variant_get_raw_buffer(pso_data_var, &pso_buff))) {
                            const unsigned char *data = NULL; size_t size = 0;
                            if(MA_OK == (rc = ma_buffer_get_raw(pso_buff, &data, &size)) && data) {
                                rc = ma_table_remove_entry(po_data->pso_table, pso_id);
                            }
                            (void) ma_buffer_release(pso_buff);
                        }                    
                        (void)ma_variant_release(pso_data_var);
                    }                    
                }
                (void) ma_buffer_release(buff);
            }
            (void)ma_variant_release(po_data_var);
        }
        return rc;        
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_policy_manifest_get_pso_ids(ma_policy_manifest_t *self, const char *po_id, ma_array_t **pso_id_array) {
    if(self && pso_id_array) {
        ma_error_t rc = MA_OK;
        ma_variant_t *po_data_var = NULL;        
        if(MA_OK == (rc = ma_table_get_value(self->po_table, po_id, &po_data_var))) {
            ma_buffer_t *buff = NULL;
            if(MA_OK == (rc = ma_variant_get_raw_buffer(po_data_var, &buff))) {
                const unsigned char *data = NULL;
                size_t size = 0;
                if(MA_OK == (rc = ma_buffer_get_raw(buff, &data, &size)) && data) {
                    policy_object_t *po_data = (policy_object_t *)data;
                    rc = ma_table_get_keys(po_data->pso_table, pso_id_array);  
                }
                (void)ma_buffer_release(buff);
            }
            (void)ma_variant_release(po_data_var);
        }
        return rc;                 
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_policy_manifest_get_task_object(ma_policy_manifest_t *self, const char *task_obj_id, const task_object_t **task_object) {
    if(self && task_obj_id && task_object) {
        ma_error_t rc = MA_OK;
        ma_variant_t *to_data_var = NULL;        
        if(MA_OK == (rc = ma_table_get_value(self->task_table, task_obj_id, &to_data_var))) {
            ma_buffer_t *buff = NULL;
            if(MA_OK == (rc = ma_variant_get_raw_buffer(to_data_var, &buff))) {
                const unsigned char *data = NULL;
                size_t size = 0;
                if(MA_OK == (rc = ma_buffer_get_raw(buff, &data, &size)) && data) {
                    *task_object = (task_object_t *)data;                    
                }
                (void) ma_buffer_release(buff);
            }
            (void)ma_variant_release(to_data_var);
        }
        return rc;        
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_policy_manifest_delete_task_object(ma_policy_manifest_t *self, const char *task_obj_id) {
    if(self && task_obj_id) {
        return ma_table_remove_entry(self->task_table, task_obj_id);  
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_policy_manifest_get_task_ids(ma_policy_manifest_t *self, ma_array_t **id_array) {
    if(self && id_array) {
        return ma_table_get_keys(self->task_table,  id_array);    
    }
    return MA_ERROR_INVALIDARG;
}

ma_bool_t ma_policy_manifest_find_assignment(ma_policy_manifest_t *self, const char *assign_key) {
    if(self && assign_key) {
        ma_variant_t *assign_var = NULL;        
        if(((MA_OK == ma_table_get_value(self->assign_table, assign_key, &assign_var))) && assign_var) {
            (void)ma_variant_release(assign_var);
            return MA_TRUE;
        }
    }
    return MA_FALSE;
}

ma_error_t ma_policy_manifest_get_assignment(ma_policy_manifest_t *self, const char *assign_key, const assignments_object_t **assign_data) {
    if(self && assign_key && assign_data) {
        ma_error_t rc = MA_OK;
        ma_variant_t *assign_var = NULL;        
        if(MA_OK == (rc = ma_table_get_value(self->assign_table, assign_key, &assign_var))) {
            ma_buffer_t *buff = NULL;
            if(MA_OK == (rc = ma_variant_get_raw_buffer(assign_var, &buff))) {
                const unsigned char *data = NULL;
                size_t size = 0;
                if(MA_OK == (rc = ma_buffer_get_raw(buff, &data, &size)) && data) {
                    *assign_data = (assignments_object_t *)data;                    
                }
                (void) ma_buffer_release(buff);
            }
            (void)ma_variant_release(assign_var);
        }
        return rc;        
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_policy_manifest_delete_assignment(ma_policy_manifest_t *self, const char *assign_key) {
    if(self && assign_key) {
        return ma_table_remove_entry(self->assign_table, assign_key);  
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_policy_manifest_get_assigment_keys(ma_policy_manifest_t *self, ma_array_t **assign_key_array) {
    if(self && assign_key_array) {
        return ma_table_get_keys(self->assign_table, assign_key_array);    
    }
    return MA_ERROR_INVALIDARG;
}

const char *ma_policy_manifest_get_handler(ma_policy_manifest_t *self) {    
    return (self) ? self->handler : NULL;    
}

const char *ma_policy_manifest_get_key_hash(ma_policy_manifest_t *self) {
    return (self) ? self->keyhash : NULL;
}

const char *ma_policy_manifest_get_created_time(ma_policy_manifest_t *self) {
    return (self) ? self->created_time : NULL;
}


static void foreach_pso_release(char const *table_key, ma_variant_t *table_value, void *cb_args, ma_bool_t *stop_loop) {
    ma_buffer_t *buff = NULL;    
    if(MA_OK == ma_variant_get_raw_buffer(table_value, &buff)) {
        const unsigned char *data = NULL;
        size_t size = 0;
        if((MA_OK == ma_buffer_get_raw(buff, &data, &size)) && data) {
            policy_object_t *po_data = (policy_object_t *)data;             
            (void)ma_table_release(po_data->pso_table);
        }
        (void) ma_buffer_release(buff);
    }
}

ma_error_t ma_policy_manifest_release(ma_policy_manifest_t *self) {
    if(self) {
        (void)ma_xml_release(self->manifest_xml);
        
        if(self->server_settings_table)
            (void)ma_table_release(self->server_settings_table);

        if(self->assign_table)
            (void)ma_table_release(self->assign_table);
        
        if(self->is_policy_data) {
            if(self->po_table) {                
                ma_table_foreach(self->po_table, foreach_pso_release, NULL);
                (void)ma_table_release(self->po_table);
            }
        }
        else {
            if(self->task_table)
                (void)ma_table_release(self->task_table);
        }
        
        free(self);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_policy_manifest_get_server_settings(ma_policy_manifest_t *self, ma_table_t **server_settings_table) {
    if(self && server_settings_table) {
        ma_error_t rc = MA_OK;
        ma_table_add_ref(*server_settings_table = self->server_settings_table);
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

