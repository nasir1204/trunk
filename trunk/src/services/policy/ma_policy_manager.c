#include "ma/internal/services/policy/ma_policy_manifest.h"
#include "ma/internal/services/policy/ma_policy_common.h"

#include "ma/internal/services/policy/ma_task_handler.h"
#include "ma/internal/services/policy/ma_policy_handler.h"

#include "ma/internal/services/ahclient/ma_ah_client_service.h"
#include "ma/internal/utils/database/ma_db.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal/utils/configurator/ma_configurator.h"

#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/defs/ma_policy_defs.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include "ma/internal/utils/text/ma_utf8.h"
#include "ma/internal/utils/network/ma_url_request.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "policy"

typedef struct user_info_for_each_data_s {
    ma_spipe_package_t   *spipepackage;
    size_t               user_count;
} user_info_for_each_data_t;

static ma_error_t policy_user_info_foreach(const char *user, void *cb_data) {
    user_info_for_each_data_t *data = (user_info_for_each_data_t *) cb_data;
    char key[MA_MAX_LEN]  = {0};
    MA_MSC_SELECT(_snprintf, snprintf)(key, MA_MAX_LEN, STR_PKGINFO_USER"%d", ++data->user_count); 
    return ma_spipe_package_add_info(data->spipepackage, key, (const unsigned char *)user, strlen(user));
}

static void save_manifest_file(ma_context_t *context, unsigned char *manifest, size_t manifest_size) ;

/* Policy Decorator methods */
static ma_error_t decorate_policy_package(ma_spipe_decorator_t *decorator, ma_spipe_priority_t priority, ma_spipe_package_t *spipepackage) {
	ma_error_t rc = MA_ERROR_INVALIDARG;
	if(decorator && spipepackage) {
		ma_policy_manager_t *self = (ma_policy_manager_t *)decorator->data;
		/* Check the type of the package if it is STR_PKGTYPE_POLICY_MANIFEST_REQUEST package then decorate it."*/             
		const unsigned char *value=NULL;
		size_t size=0;
		rc = ma_spipe_package_get_info(spipepackage, STR_PACKAGE_TYPE, &value, &size);
		if((MA_OK == rc) && value ) {
			if(!strncmp((char*)value, STR_PKGTYPE_POLICY_MANIFEST_REQUEST, strlen(STR_PKGTYPE_POLICY_MANIFEST_REQUEST))) {
				self->policy_request_status = MA_POLICY_MANIFEST_REQUEST_DECORATED;
				self->task_request_status = MA_POLICY_MANIFEST_REQUEST_DECORATED;
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Decorated <%s>", STR_PKGTYPE_POLICY_MANIFEST_REQUEST);
			}
		}

		/* TBD - Attaching Policy version & Task Version to satisfy the ePO, why ePO expects this version??? */
		if((MA_OK == rc) && MA_OK == (rc = ma_spipe_package_add_info(spipepackage, STR_PKGINFO_POLICY_VERSION, (const unsigned char *)"0", 1))) {
			if(MA_OK == (rc = ma_spipe_package_add_info(spipepackage, STR_PKGINFO_TASK_VERSION, (const unsigned char *)"0", 1))) {				
				/* Adding "SupportsManifestContent" info in SPIPE. it indicates that the Agent supports manifest features */
				if(MA_OK == rc) {
                    user_info_for_each_data_t data;
                    ma_spipe_package_add_info(spipepackage, STR_PKGINFO_SUPPORTS_MANIFEST_CONTENT, (const unsigned char *)"1", 1);

                    /* Update user list */
                    (void) ma_policy_user_info_refresh(self->user_info, MA_TRUE);
                    
                    data.spipepackage = spipepackage;
                    data.user_count = 0;
                    if(MA_OK == (rc = ma_policy_user_info_foreach(self->user_info, policy_user_info_foreach, &data))) {
                        char value[MA_MAX_LEN]  = {0};
                        MA_MSC_SELECT(_snprintf, snprintf)(value, MA_MAX_LEN, "%d", data.user_count); 
                        ma_spipe_package_add_info(spipepackage, STR_USER_ASSIGNMENT_COUNT, (const unsigned char *)value, strlen(value));
                    }    
                }
			}
		}
	}

	return rc;
}

static ma_error_t get_spipe_priority(ma_spipe_decorator_t *decorator, ma_spipe_priority_t *priority) {
	if(decorator && priority) {
		ma_policy_manager_t *self = (ma_policy_manager_t *)decorator->data;
		if(self) {
			*priority = self->priority;
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_TRACE, "Priority set for policy decorator <%d>", *priority);
		}
		else
			*priority = MA_SPIPE_PRIORITY_NONE;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

static void policy_manager_destroy(ma_policy_manager_t *self) {
	free(self);
}

static ma_error_t policy_decorator_destroy(ma_spipe_decorator_t *decorator) {
	ma_policy_manager_t *self = (ma_policy_manager_t *)decorator->data;
	/* let go only if handler's ref count is also 0 */
	if (0 == self->handler_impl.ref_count) {
		policy_manager_destroy(self);
	}
	return MA_OK;
}

static ma_spipe_decorator_methods_t policy_decorator_methods = {    
	&decorate_policy_package,
	&get_spipe_priority,
	&policy_decorator_destroy
};

/* Policy Handler Methods */
static ma_error_t policy_handler(ma_spipe_handler_t *handler, const ma_spipe_response_t *spipe_response) {
	if(handler && handler->data && spipe_response) {
		ma_error_t rc = MA_OK;
		ma_policy_manager_t *self = (ma_policy_manager_t*)handler->data;

		if(spipe_response->http_response_code == 200) {
			/* It will handle the response messages if package type is "PolicyManifest"*/             
			const unsigned char *value=NULL;
			size_t size=0;
			rc = ma_spipe_package_get_info(spipe_response->respond_spipe_pkg, STR_PACKAGE_TYPE, &value, &size);             
			if((MA_OK == rc) && value) {

				if(!strncmp((char*)value, STR_PKGTYPE_AGENT_PUB_KEY, strlen(STR_PKGTYPE_AGENT_PUB_KEY))) {
					/* reset the state if decorated */
					if(self->policy_request_status == MA_POLICY_MANIFEST_REQUEST_DECORATED)
						self->policy_request_status = MA_POLICY_MANIFEST_REQUEST_COMPLETED ;

					if(self->task_request_status == MA_POLICY_MANIFEST_REQUEST_DECORATED)
						self->task_request_status = MA_POLICY_MANIFEST_REQUEST_COMPLETED ;
					
					return MA_OK ;
				}

				/* STR_PKGTYPE_POLICY_MANIFEST */
				if(!strncmp((char*)value, STR_PKGTYPE_POLICY_MANIFEST, strlen(STR_PKGTYPE_POLICY_MANIFEST))) {
					unsigned char *manifest = NULL;
					size_t manifest_size = 0;                    
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "Agent received POLICY package from ePO server");
					if(MA_OK == (rc = ma_spipe_package_get_data(spipe_response->respond_spipe_pkg, STR_MANAGEMENT_POLICY_MANIFEST, (void **)&manifest, &manifest_size))) { 
						const ma_url_request_t *url_request = NULL;

                        /* TBD - Copying policy manifest file on disk for debugging purpose. This code will be disabled later */
                        save_manifest_file(self->context, manifest, manifest_size);

						if(MA_OK == (rc = ma_ah_client_service_get_last_connection(MA_CONTEXT_GET_AH_CLIENT(self->context), &url_request))) {
							if(MA_OK == (rc = ma_url_request_get_connection_info((ma_url_request_t *)url_request, &self->last_connection_from_ah))) {
								/* TBD - Not checking procx data validation, it will be validated when policy uses proxy info */
								if(self->last_connection_from_ah->address && self->last_connection_from_ah->port > 0) {
									MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Last succeeded connection info addr <%s> port <%d>", self->last_connection_from_ah->address, self->last_connection_from_ah->port);
									/* Handle Policies from manifest */                        
									if(MA_OK == (rc = ma_policy_handler_create(self, &self->policy_handler))) {
										MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Policy handler create succeeded");
                                        (void) ma_policy_handler_set_spipe_package(self->policy_handler, spipe_response->respond_spipe_pkg);
										if( MA_OK == (rc = ma_policy_handler_start(self->policy_handler, manifest, manifest_size))) {
											MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Policy handler start succeeded");
										}
										else
											MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to start policy handler - rc <%d>", rc);
										(void)ma_policy_handler_release(self->policy_handler);
									}
									else {
										MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to create policy handler - rc <%d>", rc);
										self->policy_request_status = MA_POLICY_MANIFEST_REQUEST_COMPLETED;
									}

									/* Handle Tasks from manifest */                        
									if((MA_OK == rc) && (MA_OK == (rc = ma_task_handler_create(self, &self->task_handler)))) {
										MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Task handler create succeeded");
										(void) ma_task_handler_set_spipe_package(self->task_handler, spipe_response->respond_spipe_pkg);
										if(MA_OK == (rc = ma_task_handler_start(self->task_handler, manifest, manifest_size))) {
											MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Task handler start succeeded");
										}
										else
											MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to start Task handler - rc <%d>", rc);
										(void)ma_task_handler_release(self->task_handler);
									}
									else {
										MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to create task handler - rc <%d>", rc);
										self->task_request_status = MA_POLICY_MANIFEST_REQUEST_COMPLETED;
									}
								}
								else
									rc = MA_ERROR_POLICY_INTERNAL;
							}
						}
						if(MA_OK != rc) {
							MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to get last succeeded connection info - rc <%d>", rc);
							self->task_request_status = MA_POLICY_MANIFEST_REQUEST_COMPLETED;
							self->policy_request_status = MA_POLICY_MANIFEST_REQUEST_COMPLETED;
						}
					}
					else {
						MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to get the manifest.xml file from spipe package rc <%d>", rc);
						self->policy_request_status = MA_POLICY_MANIFEST_REQUEST_COMPLETED;
						self->task_request_status = MA_POLICY_MANIFEST_REQUEST_COMPLETED;
					}

					self->priority = MA_SPIPE_PRIORITY_NONE;
				}                
			}
			else {
				if(MA_POLICY_MANIFEST_REQUEST_DECORATED == self->policy_request_status)
					self->policy_request_status = MA_POLICY_MANIFEST_REQUEST_COMPLETED;
				if(MA_POLICY_MANIFEST_REQUEST_DECORATED == self->task_request_status)
					self->task_request_status = MA_POLICY_MANIFEST_REQUEST_COMPLETED;
			}
		}
		else {
			/* If the http response is other than 200 for policymanifest request, status is changed to completed */
			if(MA_POLICY_MANIFEST_REQUEST_DECORATED == self->policy_request_status)
				self->policy_request_status = MA_POLICY_MANIFEST_REQUEST_COMPLETED;
			if(MA_POLICY_MANIFEST_REQUEST_DECORATED == self->task_request_status)
				self->task_request_status = MA_POLICY_MANIFEST_REQUEST_COMPLETED;
		}
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

static ma_error_t policy_handler_destroy(ma_spipe_handler_t *handler) {
	ma_policy_manager_t *self = (ma_policy_manager_t *)handler->data;
	/* let go only if decorator's ref count is also 0 */
	if (0 == self->decorator_impl.ref_count) {
		policy_manager_destroy(self);
	}
	return MA_OK;

}

static ma_spipe_handler_methods_t policy_handler_methods = {    
	&policy_handler,
	&policy_handler_destroy
};


ma_error_t ma_policy_manager_create(ma_context_t *context, ma_policy_manager_t **policy_manager) {
	if(context && policy_manager) {
		ma_policy_manager_t *self = (ma_policy_manager_t*)calloc(1, sizeof(ma_policy_manager_t));
		if(self) {
			ma_spipe_decorator_init((ma_spipe_decorator_t *)&self->decorator_impl, &policy_decorator_methods, self);            
			ma_spipe_handler_init((ma_spipe_handler_t *)&self->handler_impl, &policy_handler_methods, self);   
			self->context = context;
			self->priority = MA_SPIPE_PRIORITY_NONE;
			self->policy_request_status = MA_POLICY_MANIFEST_REQUEST_COMPLETED;  
			self->task_request_status = MA_POLICY_MANIFEST_REQUEST_COMPLETED;
            self->force_policy_enforcement = MA_FALSE ;
			*policy_manager = self;
			return MA_OK;
		}
		return MA_ERROR_OUTOFMEMORY;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_policy_manager_set_user_info(ma_policy_manager_t *self, ma_policy_user_info_t *user_info) {
    return (self && (self->user_info = user_info)) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_spipe_decorator_t * ma_policy_manager_get_decorator(ma_policy_manager_t *self) {
	return (self) ? &self->decorator_impl : NULL ;
}

ma_spipe_handler_t * ma_policy_manager_get_handler(ma_policy_manager_t *self) {
	return (self) ? &self->handler_impl : NULL ;
}

ma_error_t ma_policy_manager_raise_alert(ma_policy_manager_t *self, ma_bool_t force_policy_enforcement) {
	if(self) {
		if((MA_POLICY_MANIFEST_REQUEST_COMPLETED == self->policy_request_status) &&
			(MA_POLICY_MANIFEST_REQUEST_COMPLETED == self->task_request_status)) {
				self->policy_request_status = MA_POLICY_MANIFEST_REQUEST_RAISED; 
				self->task_request_status = MA_POLICY_MANIFEST_REQUEST_RAISED;
				self->priority = MA_SPIPE_PRIORITY_IMMEDIATE;
                self->policy_handler = NULL;
                self->task_handler = NULL;
                self->force_policy_enforcement = force_policy_enforcement ;
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Policy service raised spipe alert.");

				if(MA_OK != ma_ah_client_service_raise_spipe_alert(MA_CONTEXT_GET_AH_CLIENT(self->context), STR_PKGTYPE_POLICY_MANIFEST_REQUEST)){
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to raise a spipe alert");
					// To make sure we perform the operation on next raise alert.
					self->policy_request_status = self->task_request_status = MA_POLICY_MANIFEST_REQUEST_COMPLETED;
				}
				return MA_OK;
		}
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Update policies session is in progress");
		return MA_ERROR_POLICY_SESSION_IN_PROGRESS;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_policy_manager_abort(ma_policy_manager_t *self) {
	if(!self) return MA_ERROR_INVALIDARG;
	if(self->policy_request_status != MA_POLICY_MANIFEST_REQUEST_COMPLETED && self->policy_handler)  ma_policy_handler_stop(self->policy_handler, MA_ERROR_POLICY_INTERNAL);
	if(self->task_request_status != MA_POLICY_MANIFEST_REQUEST_COMPLETED && self->task_handler)	ma_task_handler_stop(self->task_handler, MA_ERROR_POLICY_INTERNAL);
	return MA_OK;
}

ma_error_t ma_policy_manager_release(ma_policy_manager_t *self) {
	if (!self) return MA_ERROR_INVALIDARG;
	policy_handler_destroy(&self->handler_impl);
	policy_decorator_destroy(&self->decorator_impl);
	if(self->last_connection_from_ah) ma_url_request_release_connection_info(self->last_connection_from_ah);	
	return MA_OK;
}

ma_bool_t ma_policy_manager_raise_alert_request_done(ma_policy_manager_t *self) {
    return (self) ? (self->policy_request_status == MA_POLICY_MANIFEST_REQUEST_COMPLETED && self->task_request_status == MA_POLICY_MANIFEST_REQUEST_COMPLETED) : MA_FALSE;
}

static void save_manifest_file(ma_context_t *context, unsigned char *manifest, size_t manifest_size) {
    if(context && manifest && manifest_size > 0) {
        char file_path[MAX_PATH_LEN] = {0};
        const char *data_path = ma_configurator_get_agent_data_path(MA_CONTEXT_GET_CONFIGURATOR(context));
        if(data_path) {
            FILE *fp = NULL;
            MA_MSC_SELECT(_snprintf, snprintf)(file_path, MAX_PATH_LEN, "%s%c%s", data_path, MA_PATH_SEPARATOR, "manifest.xml");
            fp = fopen(file_path, "w+");
            if(fp) {
                fwrite(manifest, sizeof(char), manifest_size, fp);
                fclose(fp);
            }
        }
    }
}