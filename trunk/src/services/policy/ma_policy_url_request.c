#include "ma/internal/services/policy/ma_policy_common.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/proxy/ma_proxy.h"

#include <stdlib.h>

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "policy"

/* TBD - Will move all common functionality of url request here from policy handler and task handler */
ma_error_t ma_policy_url_request_create(ma_policy_manager_t *manager, ma_policy_url_request_t **policy_url_request) {
    if(policy_url_request) {
        ma_policy_url_request_t *self = (ma_policy_url_request_t *)calloc(1, sizeof(ma_policy_url_request_t));
        if(self) {
            ma_error_t rc = MA_OK; 
            ma_ah_sitelist_t *ah_sitelist = NULL;
           (void) ma_ah_client_service_get_sitelist(MA_CONTEXT_GET_AH_CLIENT(manager->context), &ah_sitelist);
	
            if(!ah_sitelist){
                MA_LOG(MA_CONTEXT_GET_LOGGER(manager->context), MA_LOG_SEV_ERROR, "Failed to get sitelist received from the ah client service.");
                rc = MA_ERROR_PRECONDITION;
            }
            else {
                const char *certificate = (char *)ma_ah_sitelist_get_ca_file(ah_sitelist);   
                if(!certificate) {
                    MA_LOG(MA_CONTEXT_GET_LOGGER(manager->context), MA_LOG_SEV_ERROR, "server ca certificate is not available");
                    rc = MA_ERROR_POLICY_INTERNAL;
                }
                else {
                    if(MA_OK == (rc = ma_url_request_create(MA_CONTEXT_GET_NET_CLIENT(manager->context), "https://127.0.0.1", &self->url_request))) {
                        (void)ma_url_request_set_option(self->url_request, MA_URL_REQUEST_OPTION_REQUEST_TYPE, (unsigned long)MA_URL_REQUEST_TYPE_GET);
                        (void)ma_url_request_set_option(self->url_request, MA_URL_REQUEST_OPTION_CONNECT_TIMEOUT, 10);
                        (void)ma_url_request_set_option(self->url_request, MA_URL_REQUEST_OPTION_TRANSFER_TIMEOUT, 60);	
                        
                        MA_URL_SSL_INIT((&self->ssl_info));                            
                        MA_URL_AUTH_SET_SSL_INFO((&self->ssl_info), certificate);
                        (void)ma_url_request_set_ssl_info(self->url_request, &self->ssl_info);
                        *policy_url_request = self;
                        return MA_OK;
                    }
                }
            }
            free(self);
            return rc;
        }
        return MA_ERROR_OUTOFMEMORY;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_policy_url_request_set_proxy(ma_policy_url_request_t *self, ma_proxy_list_t *proxy_list) {
    return ma_url_request_set_proxy_info(self->url_request, proxy_list);
}

ma_error_t ma_policy_url_request_send(ma_policy_url_request_t *self, const char *url, ma_url_request_connect_callback_t connect_cb, ma_url_request_finalize_callback_t final_cb, void *cb_data) {
    if(self && url && final_cb) {
        if(connect_cb) ma_url_request_set_connect_callback(self->url_request, connect_cb);
        (void)ma_url_request_set_url(self->url_request, url);
        return ma_url_request_send(self->url_request, final_cb, cb_data);
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_policy_url_request_cancel(ma_policy_url_request_t *self) {
    return ma_url_request_cancel(self->url_request);
}

ma_error_t ma_policy_url_request_reset_stream(ma_policy_url_request_t *self) {
    if(self) {
        ma_error_t rc = MA_OK;
        ma_stream_t *downstream = NULL;
        if(self->io_streams.write_stream) 
            (void)ma_stream_release(self->io_streams.write_stream);
        if(MA_OK == (rc = ma_mstream_create(MAX_MSTREAM_SIZE, NULL, &downstream))) {  
            MA_URL_REQUEST_IO_INIT((&self->io_streams));
            MA_URL_REQUEST_IO_SET_WRITE_STREAM((&self->io_streams), downstream);
            (void)ma_url_request_set_io_callback(self->url_request, &self->io_streams);
            memset(self->id, 0, MA_MAX_LEN);
            self->is_po_download = MA_FALSE;
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_policy_url_request_release(ma_policy_url_request_t *self) {
    if(self) {
        MA_URL_SSL_DEINIT((&self->ssl_info));  
        (void)ma_stream_release(self->io_streams.write_stream);
        (void)ma_url_request_release(self->url_request);
        free(self);
        return MA_OK;       
    }
    return MA_ERROR_INVALIDARG;
}

