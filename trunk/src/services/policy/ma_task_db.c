#include "ma/internal/services/policy/ma_task_db.h"

#include <string.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "policy"

ma_error_t db_add_task_instance(ma_db_t *db_handle) {
    if(db_handle){
        ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;

        if(MA_OK == (rc = ma_db_transaction_begin(db_handle))) 
        {
            if( MA_OK == (rc = ma_db_statement_create(db_handle, CREATE_MET_TABLE_STMT, &db_stmt)) )
            {
                rc = ma_db_statement_execute(db_stmt) ;
                ma_db_statement_release(db_stmt); db_stmt = NULL ;
                if(MA_OK == rc &&  MA_OK == (rc = ma_db_statement_create(db_handle, CREATE_META_TABLE_STMT, &db_stmt)))
                {
                    if(MA_OK == (rc = ma_db_statement_execute(db_stmt)))
                        rc = ma_db_transaction_end(db_handle) ;
                    ma_db_statement_release(db_stmt); db_stmt = NULL ;
                }
            }
            if(MA_OK != rc)
                ma_db_transaction_cancel(db_handle) ;
        }
         return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

#define UPDATE_TASK_DATA_STMT "UPDATE MA_EPO_TASK SET TIMESTAMP = ? WHERE TASK_OBJECT_ID = ?"
ma_error_t db_update_epo_task(ma_db_t *db_handle, const char *task_object_id, const char *time_stamp) {
    if(db_handle && task_object_id && time_stamp) {
        ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;

		if( MA_OK == (rc = ma_db_statement_create(db_handle, UPDATE_TASK_DATA_STMT, &db_stmt))) {
            if( MA_OK == (rc = ma_db_statement_set_string(db_stmt, 2, 1, task_object_id, strlen(task_object_id)))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 1, 1, time_stamp, strlen(time_stamp)))) 
            { 
                rc = ma_db_statement_execute(db_stmt) ;
            }
            ma_db_statement_release(db_stmt) ;
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

#define ADD_TASK_DATA_STMT "INSERT INTO MA_EPO_TASK VALUES(?, ?, ?, ?, ?, ?, ?, ?)"
ma_error_t db_add_epo_task(ma_db_t *db_handle, const char *task_object_id, const char *task_id, const char *task_name, const char *task_digest, 
                            const char *task_type, const char *task_priority, ma_variant_t *task_data, const char *time_stamp) 
{
    if(db_handle && task_object_id && task_id && task_name && task_digest && task_type && task_priority && task_data && time_stamp) {
        ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;

       if( MA_OK == (rc = ma_db_statement_create(db_handle, ADD_TASK_DATA_STMT, &db_stmt))) 
        {
            if( MA_OK == (rc = ma_db_statement_set_string(db_stmt, 1, 1, task_object_id, strlen(task_object_id)))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 2, 1, task_id, strlen(task_id)))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 3, 1, task_name, strlen(task_name)))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 4, 1, task_digest, strlen(task_digest)))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 5, 1, task_type, strlen(task_type)))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 6, 1, task_priority, strlen(task_priority)))
                && MA_OK == (rc = ma_db_statement_set_variant(db_stmt, 7, task_data))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 8, 1, time_stamp, strlen(time_stamp)))  )
            {
                rc = ma_db_statement_execute(db_stmt) ;
            }
            ma_db_statement_release(db_stmt) ;
        }
       return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

#define ADD_TASK_ASSIGNMENT_DATA_STMT   "INSERT INTO MA_EPO_TASK_ASSIGNMENT VALUES (?, ?, ?, ?)"
ma_error_t db_add_epo_task_assignment(ma_db_t *db_handle, const char *task_object_id, const char *product, const char *method, const char *param )  {
    if(db_handle && task_object_id && product) {
        ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;

       if( MA_OK == (rc = ma_db_statement_create(db_handle, ADD_TASK_ASSIGNMENT_DATA_STMT, &db_stmt))) 
        {
            if( MA_OK == (rc = ma_db_statement_set_string(db_stmt, 1, 1, task_object_id, strlen(task_object_id)))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 2, 1, product, strlen(product)))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 3, 1, method, (method)?strlen(method):0))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 4, 1, param, (param)?strlen(param):0))  )
            {
                rc = ma_db_statement_execute(db_stmt) ;
            }
            ma_db_statement_release(db_stmt) ;
        }
       return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

#define REMOVE_EPO_TASK_STMT    "DELETE FROM MA_EPO_TASK WHERE TASK_OBJECT_ID = ?"
#define REMOVE_EPO_TASK_ASSIGNMENT_FOR_TASK_STMT    "DELETE FROM MA_EPO_TASK_ASSIGNMENT WHERE TASK_OBJECT_ID = ?"
ma_error_t db_remove_epo_task (ma_db_t *db_handle, const char *task_object_id) {
    if(db_handle && task_object_id) {
        ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;
        //*******TBD should we start the transaction or caller will ????
        if( MA_OK == (rc = ma_db_statement_create(db_handle, REMOVE_EPO_TASK_ASSIGNMENT_FOR_TASK_STMT, &db_stmt))   
            && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 1, 1, task_object_id, strlen(task_object_id)))   
            && MA_OK == (rc = ma_db_statement_execute(db_stmt))  
            && MA_OK == (rc = ma_db_statement_release(db_stmt)) )
        {
            if( MA_OK == (rc = ma_db_statement_create(db_handle, REMOVE_EPO_TASK_STMT, &db_stmt))   
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 1, 1, task_object_id, strlen(task_object_id)))   
                && MA_OK == (rc = ma_db_statement_execute(db_stmt))  )
           {
               ma_db_statement_release(db_stmt) ;
           }
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

#define REMOVE_DATA_EPO_TASK_ASSIGNMENT_STMT    "DELETE FROM MA_EPO_TASK_ASSIGNMENT WHERE TASK_OBJECT_ID = ? AND META_PRODUCT = ? AND ( (? = 0 AND META_METHOD IS NULL AND META_PARAM IS NULL) OR (? = 1 AND META_METHOD = ? AND META_PARAM IS NULL) OR (? = 2 AND META_METHOD = ? AND META_PARAM = ?))"
ma_error_t db_remove_epo_task_assignment (ma_db_t *db_handle, const char *task_object_id, const char *product, const char *method, const char *param ) {
    if(db_handle && task_object_id && product) {
        ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;

        if( MA_OK == (rc = ma_db_statement_create(db_handle, REMOVE_DATA_EPO_TASK_ASSIGNMENT_STMT, &db_stmt)))
        {
            if( MA_OK == (rc = ma_db_statement_set_string(db_stmt, 1, 1, task_object_id, strlen(task_object_id)))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 2, 1, product, strlen(product)))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 5, 1, method, (method) ? strlen(method) : 0)) 
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 7, 1, method, (method) ? strlen(method) : 0))  
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 8, 1, param, (param) ? strlen(param) : 0))  )
            {
                int value = 0 ;
                if(method && param)
                    value = 2 ;
                else if(method)
                    value = 1 ;
                
                ma_db_statement_set_int(db_stmt, 3, value) ;
                ma_db_statement_set_int(db_stmt, 4, value) ;
                ma_db_statement_set_int(db_stmt, 6, value) ;

                rc = ma_db_statement_execute(db_stmt) ;
            }
            ma_db_statement_release(db_stmt) ;
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

#define GET_EPO_TASK_DATA_STMT  "SELECT TASK_OBJECT_ID, TASK_DIGEST FROM MA_EPO_TASK WHERE ( (? = 0 ) OR ( ? = 1 AND TASK_OBJECT_ID = ?) )"
ma_error_t db_get_epo_task(ma_db_t *db_handle, const char *task_object_id, ma_db_recordset_t **db_record) {
    if(db_handle && db_record) {
        ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;

        if( MA_OK == (rc = ma_db_statement_create(db_handle, GET_EPO_TASK_DATA_STMT, &db_stmt)) ) 
        {
            int value = 0 ;
            if(task_object_id) value = 1 ;
            if( MA_OK == (rc = ma_db_statement_set_int(db_stmt, 1, value))
                && MA_OK == (rc = ma_db_statement_set_int(db_stmt, 2, value))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 3, 1, task_object_id, (task_object_id) ? strlen(task_object_id) : 0)) )
            {
                rc = ma_db_statement_execute_query(db_stmt, db_record) ;
            }
            ma_db_statement_release(db_stmt) ;
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}


#define GET_EPO_TASK_DATA_BY_OBJECT_ID_STMT  "SELECT TASK_OBJECT_ID, TASK_DATA FROM MA_EPO_TASK"
ma_error_t db_get_epo_task_data(ma_db_t *db_handle, const char *task_object_id, ma_db_recordset_t **db_record) {
    if(db_handle && db_record) {
        ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;

        if( MA_OK == (rc = ma_db_statement_create(db_handle, GET_EPO_TASK_DATA_BY_OBJECT_ID_STMT, &db_stmt)) ) {
            rc = ma_db_statement_execute_query(db_stmt, db_record) ;
            ma_db_statement_release(db_stmt) ;
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

#define GET_EPO_TASK_ASSIGNMENT_DATA_STMT "SELECT DISTINCT * FROM MA_EPO_TASK_ASSIGNMENT WHERE (? = 0 ) OR (? = 1 AND TASK_OBJECT_ID = ?) OR (? = 2 AND TASK_OBJECT_ID = ? AND META_PRODUCT = ? AND META_METHOD IS NULL AND META_PARAM IS NULL) OR (? = 3 AND TASK_OBJECT_ID = ? AND META_PRODUCT = ? AND META_METHOD = ? AND META_PARAM IS NULL) OR (? = 4 AND TASK_OBJECT_ID = ? AND META_PRODUCT = ? AND META_METHOD = ? AND META_PARAM = ?)"
ma_error_t db_get_epo_task_assignments(ma_db_t *db_handle, const char *task_object_id, const char *product, const char *method, const char *param, ma_db_recordset_t **db_record) {
    if(db_handle) {
        ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;
        int filter= 0 ; //All assignments

        if(task_object_id) { // assignments for a PO 
            filter = 1 ;
            if(product) {
                filter = 2 ;
                if(method && param) filter = 4 ;
                else if(method) filter = 3 ;
            }
        }
        if( MA_OK == (rc = ma_db_statement_create(db_handle, GET_EPO_TASK_ASSIGNMENT_DATA_STMT, &db_stmt)) ) 
        {
            if( MA_OK == (rc = ma_db_statement_set_int(db_stmt, 1, filter))
                && MA_OK == (rc = ma_db_statement_set_int(db_stmt, 2, filter))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 3, 1, task_object_id, (task_object_id)?strlen(task_object_id):0))
                && MA_OK == (rc = ma_db_statement_set_int(db_stmt, 4, filter))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 5, 1, task_object_id, (task_object_id)?strlen(task_object_id):0))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 6, 1, product, (product)?strlen(product):0))
                && MA_OK == (rc = ma_db_statement_set_int(db_stmt, 7, filter))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 8, 1, task_object_id, (task_object_id)?strlen(task_object_id):0))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 9, 1, product, (product)?strlen(product):0))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 10, 1, method, (method)?strlen(method):0))
                && MA_OK == (rc = ma_db_statement_set_int(db_stmt, 11, filter))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 12, 1, task_object_id, (task_object_id)?strlen(task_object_id):0))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 13, 1, product, (product)?strlen(product):0))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 14, 1, method, (method)?strlen(method):0))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 15, 1, param, (param)?strlen(param):0)) )
            {
                rc = ma_db_statement_execute_query(db_stmt, db_record) ;
            }
            ma_db_statement_release(db_stmt) ;
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

#define GET_ALL_TASKS_STMT "SELECT DISTINCT META.META_PRODUCT, MET.TASK_OBJECT_ID, MET.TASK_ID, MET.TASK_NAME, MET.TASK_TYPE, MET.TASK_PRIORITY, MET.TIMESTAMP, MET.TASK_DATA FROM MA_EPO_TASK MET, MA_EPO_TASK_ASSIGNMENT META WHERE MET.TASK_OBJECT_ID=META.TASK_OBJECT_ID AND ((? = 0 ) OR (? = 1 AND MET.TIMESTAMP = ?))"
ma_error_t db_get_all_epo_tasks(ma_db_t *db_handle, const char *timestamp, ma_db_recordset_t **db_record)  {
    if(db_handle) {
        ma_db_statement_t *db_stmt = NULL;
        ma_error_t rc = MA_OK;
        int tvalue= 0;

        if(timestamp) 
            tvalue = 1;
           
        if( MA_OK == (rc = ma_db_statement_create(db_handle, GET_ALL_TASKS_STMT, &db_stmt)) ) 
        {
            if( MA_OK == (rc = ma_db_statement_set_int(db_stmt, 1, tvalue))
                && MA_OK == (rc = ma_db_statement_set_int(db_stmt, 2, tvalue))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 3, 1, timestamp, (timestamp) ? strlen(timestamp) : 0)))
            {
                rc = ma_db_statement_execute_query(db_stmt, db_record) ;
            }
            ma_db_statement_release(db_stmt) ;
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}
