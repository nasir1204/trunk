#include "ma/internal/services/policy/ma_policy_db.h"
#include <string.h>

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "policy"

ma_error_t db_add_policy_instance(ma_db_t *db_handle) {
    if(db_handle){
        ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;

        if(MA_OK == (rc = ma_db_transaction_begin(db_handle))) 
        {
            if( MA_OK == (rc = ma_db_statement_create(db_handle, CREATE_MPO_TABLE_STMT, &db_stmt)))
            {
                rc = ma_db_statement_execute(db_stmt) ;
                ma_db_statement_release(db_stmt); db_stmt = NULL ;
                if( MA_OK == rc && MA_OK == (rc = ma_db_statement_create(db_handle, CREATE_MPSO_TABLE_STMT, &db_stmt)) )
                {
                    rc = ma_db_statement_execute(db_stmt) ;
                    ma_db_statement_release(db_stmt); db_stmt = NULL ;
                    if( MA_OK == rc && MA_OK == (rc = ma_db_statement_create(db_handle, CREATE_MPPA_TABLE_STMT, &db_stmt)))
                    {
                        if(MA_OK  == (rc = ma_db_statement_execute(db_stmt)))
                            rc = ma_db_transaction_end(db_handle) ;
                        ma_db_statement_release(db_stmt); db_stmt = NULL ;
                    }
                }
            }
            if(MA_OK != rc)
               ma_db_transaction_cancel(db_handle) ;
        }
         return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

#define ADD_DATA_MPA_STMT    "INSERT INTO MA_POLICY_OBJECT VALUES(?, ?, ?, ?, ?, ?, ?)"
ma_error_t db_add_policy_object(ma_db_t *db_handle, const char *po_id, const char *po_name, const char *po_feature, const char *po_category, const char *po_type, const char *po_digest, const char *po_legacy) {
    if(db_handle && po_id && po_name && po_feature && po_category && po_type && po_digest && po_legacy) {
        ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;

        if( MA_OK == (rc = ma_db_statement_create(db_handle, ADD_DATA_MPA_STMT, &db_stmt))) 
        {
            if( MA_OK == (rc = ma_db_statement_set_string(db_stmt, 1, 1, po_id, strlen(po_id)))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 2, 1, po_name, strlen(po_name)))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 3, 1, po_feature, strlen(po_feature)))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 4, 1, po_category, strlen(po_category)))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 5, 1, po_type, strlen(po_type)))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 6, 1, po_digest, strlen(po_digest)))
				&& MA_OK == (rc = ma_db_statement_set_string(db_stmt, 7, 1, po_legacy, strlen(po_legacy))) )
            {
                rc = ma_db_statement_execute(db_stmt) ;
            }
            ma_db_statement_release(db_stmt) ;
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

#define ADD_DATA_MPSO_STMT    "INSERT INTO MA_POLICY_SETTINGS_OBJECT VALUES(?, ?, ?, ?, ?, ?, ?, ?) "
ma_error_t db_add_policy_settings_object(ma_db_t *db_handle, const char *po_id, const char *pso_id, const char *pso_name, const char *pso_digest, ma_variant_t *pso_data, const char *pso_param_int, const char *pso_param_str, const char *time_stamp) {
    if(db_handle && po_id && pso_id && pso_name && pso_digest && pso_data && time_stamp) {
        ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;

       if( MA_OK == (rc = ma_db_statement_create(db_handle, ADD_DATA_MPSO_STMT, &db_stmt))) 
        {
            if( MA_OK == (rc = ma_db_statement_set_string(db_stmt, 1, 1, po_id, strlen(po_id)))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 2, 1, pso_id, strlen(pso_id)))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 3, 1, pso_name, strlen(pso_name)))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 4, 1, pso_digest, strlen(pso_digest)))
                && MA_OK == (rc = ma_db_statement_set_variant(db_stmt, 5, pso_data))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 6, 1, pso_param_int, (pso_param_int)?strlen(pso_param_int):0))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 7, 1, pso_param_str, (pso_param_str)?strlen(pso_param_str):0))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 8, 1, time_stamp, strlen(time_stamp)))  )
            {
                rc = ma_db_statement_execute(db_stmt) ;
            }
            ma_db_statement_release(db_stmt) ;
        }
       return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

#define ADD_DATA_MPPA_STMT    "INSERT INTO MA_POLICY_PRODUCT_ASSIGNMENT VALUES(?, ?, ?, ?)"
ma_error_t db_add_policy_product_assignment(ma_db_t *db_handle, const char *po_id, const char *product, const char *method, const char *param ) {
    if(db_handle && po_id && product) {
        ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;

        if( MA_OK == (rc = ma_db_statement_create(db_handle, ADD_DATA_MPPA_STMT, &db_stmt))) 
        {
            if( MA_OK == (rc = ma_db_statement_set_string(db_stmt, 1, 1, po_id, strlen(po_id)))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 2, 1, product, strlen(product)))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 3, 1, (method)?method:"0", (method)?strlen(method):1))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 4, 1, (param)?param:"0", (param)?strlen(param):1))  )
            {
                rc = ma_db_statement_execute(db_stmt) ;
            }
            ma_db_statement_release(db_stmt) ;
        }
       return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

#define REMOVE_DATA_MPO_STMT    "DELETE FROM MA_POLICY_OBJECT WHERE PO_ID = ?"
#define REMOVE_DATA_MPSO_FOR_PO_STMT    "DELETE FROM MA_POLICY_SETTINGS_OBJECT WHERE PO_ID = ?"
#define REMOVE_DATA_MPPA_FOR_PO_STMT    "DELETE FROM MA_POLICY_PRODUCT_ASSIGNMENT WHERE PO_ID = ?"

ma_error_t db_remove_policy_object(ma_db_t *db_handle, const char *po_id) {
    if(db_handle && po_id) {
        ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;
        //*******TBD should we start the transaction or caller will ????
        if( MA_OK == (rc = ma_db_statement_create(db_handle, REMOVE_DATA_MPPA_FOR_PO_STMT, &db_stmt))   
            && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 1, 1, po_id, strlen(po_id)))   
            && MA_OK == (rc = ma_db_statement_execute(db_stmt))  
            && MA_OK == (rc = ma_db_statement_release(db_stmt)) )
        {
            if( MA_OK == (rc = ma_db_statement_create(db_handle, REMOVE_DATA_MPSO_FOR_PO_STMT, &db_stmt))   
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 1, 1, po_id, strlen(po_id)))   
                && MA_OK == (rc = ma_db_statement_execute(db_stmt))  
                && MA_OK == (rc = ma_db_statement_release(db_stmt)) )
            {
                if( MA_OK == (rc = ma_db_statement_create(db_handle, REMOVE_DATA_MPO_STMT, &db_stmt))   
                    && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 1, 1, po_id, strlen(po_id)))   
                    && MA_OK == (rc = ma_db_statement_execute(db_stmt)) )
                {
                    ma_db_statement_release(db_stmt) ;
                }
            }
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

#define REMOVE_DATA_MPSO_STMT    "DELETE FROM MA_POLICY_SETTINGS_OBJECT WHERE PO_ID = ? AND PSO_ID = ?"
ma_error_t db_remove_policy_settings_object(ma_db_t *db_handle, const char *po_id, const char *pso_id) {
    if(db_handle && po_id && pso_id) {
        ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;

        if( MA_OK == (rc = ma_db_statement_create(db_handle, REMOVE_DATA_MPSO_STMT, &db_stmt)))
        {
            if( MA_OK == (rc = ma_db_statement_set_string(db_stmt, 1, 1, po_id, strlen(po_id)))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 2, 1, pso_id, strlen(pso_id))) )
            {
                rc = ma_db_statement_execute(db_stmt) ;
            }
            ma_db_statement_release(db_stmt) ;
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

#define UPDATE_PO_DIGEST_MPO_STMT    "UPDATE MA_POLICY_OBJECT SET PO_DIGEST = ? WHERE PO_ID = ?"
ma_error_t db_update_po_digest(ma_db_t *db_handle, const char *po_id, const char *digest) {
    if(db_handle && po_id && digest) {
        ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;

        if( MA_OK == (rc = ma_db_statement_create(db_handle, UPDATE_PO_DIGEST_MPO_STMT, &db_stmt)))
        {
            if( MA_OK == (rc = ma_db_statement_set_string(db_stmt, 1, 1, digest, strlen(digest)))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 2, 1, po_id, strlen(po_id))) )
            {
                rc = ma_db_statement_execute(db_stmt) ;
            }
            ma_db_statement_release(db_stmt) ;
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

#define REMOVE_DATA_MPPA_STMT    "DELETE FROM MA_POLICY_PRODUCT_ASSIGNMENT WHERE PO_ID = ? AND PPA_PRODUCT = ? AND PPA_METHOD = ? AND PPA_PARAM = ?"
ma_error_t db_remove_policy_product_assignment(ma_db_t *db_handle, const char *po_id, const char *product, const char *method, const char *param ) {
    if(db_handle && po_id && product) {
        ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;

        if( MA_OK == (rc = ma_db_statement_create(db_handle, REMOVE_DATA_MPPA_STMT, &db_stmt)))
        {
            if( MA_OK == (rc = ma_db_statement_set_string(db_stmt, 1, 1, po_id, strlen(po_id)))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 2, 1, product, strlen(product)))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 3, 1, (method)?method:"0", (method)?strlen(method):1)) 
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 4, 1, (param)?param:"0", (param)?strlen(param):1))  )
            {               
                rc = ma_db_statement_execute(db_stmt) ;
            }
            ma_db_statement_release(db_stmt) ;
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

#define GET_PO_DATA "SELECT PO_ID, PO_DIGEST FROM MA_POLICY_OBJECT WHERE ( (? = 0 ) OR ( ? = 1 AND PO_ID = ?) )"
ma_error_t db_get_policy_objects(ma_db_t *db_handle, const char *po_id, ma_db_recordset_t **db_record) {
    if(db_handle && db_record) {
        ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;

        if( MA_OK == (rc = ma_db_statement_create(db_handle, GET_PO_DATA, &db_stmt)) ) 
        {
            int value = 0 ;
            if(po_id) value = 1 ;
            if( MA_OK == (rc = ma_db_statement_set_int(db_stmt, 1, value))
                && MA_OK == (rc = ma_db_statement_set_int(db_stmt, 2, value))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 3, 1, po_id, (po_id) ? strlen(po_id) : 0)) )
            {
                rc = ma_db_statement_execute_query(db_stmt, db_record) ;
            }
            ma_db_statement_release(db_stmt) ;
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

#define GET_PSO_DATA_FOR_PO "SELECT PO_ID, PSO_ID, PSO_DIGEST FROM MA_POLICY_SETTINGS_OBJECT WHERE ( (? = 0) OR (? = 1 AND  PO_ID = ?) OR (? = 2 AND PO_ID = ? AND PSO_ID = ?) ) "
ma_error_t db_get_policy_settings_objects(ma_db_t *db_handle, const char *po_id, const char *pso_id, ma_db_recordset_t **db_record) {
    if(db_handle && db_record) {
        ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;
        int value = 0 ;

        if(po_id && pso_id) value = 2 ;
        else if(po_id) value = 1 ;

        if( MA_OK == (rc = ma_db_statement_create(db_handle, GET_PSO_DATA_FOR_PO, &db_stmt)) ) 
        {
            if( MA_OK == (rc = ma_db_statement_set_int(db_stmt, 1, value))
                && MA_OK == (rc = ma_db_statement_set_int(db_stmt, 2, value))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 3, 1, po_id, (po_id) ? strlen(po_id) : 0))
                && MA_OK == (rc = ma_db_statement_set_int(db_stmt, 4, value))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 5, 1, po_id, (po_id) ? strlen(po_id) : 0))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 6, 1, pso_id, (pso_id) ? strlen(pso_id) : 0))  )
            {
                rc = ma_db_statement_execute_query(db_stmt, db_record) ;
            }
            ma_db_statement_release(db_stmt) ;
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

#define GET_ASSIGNMENT_DATA_STMT "SELECT DISTINCT * FROM MA_POLICY_PRODUCT_ASSIGNMENT WHERE  (? = 0 ) OR (? = 0 AND PPA_METHOD = ? AND PPA_PARAM = ? ) OR (? = 1 AND PO_ID = ? AND PPA_METHOD = ? AND PPA_PARAM = ?) OR (? = 2 AND PO_ID = ? AND PPA_PRODUCT = ? AND PPA_METHOD = ? AND PPA_PARAM = ?)"
ma_error_t db_get_policy_product_assignments(ma_db_t *db_handle, const char *po_id, const char *product, const char *method, const char *param, ma_db_recordset_t **db_record) {
    if(db_handle) {
        ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;
        int filter= 0 ; //All assignments

        if(po_id) { // assignments for a PO 
            filter = 1 ;
            if(product) {
                filter = 2 ;     
            }
        }
        if( MA_OK == (rc = ma_db_statement_create(db_handle, GET_ASSIGNMENT_DATA_STMT, &db_stmt)) ) 
        {
            if( MA_OK == (rc = ma_db_statement_set_int(db_stmt, 1, filter))
                && MA_OK == (rc = ma_db_statement_set_int(db_stmt, 2, filter))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 3, 1, (method)?method:"0", (method)?strlen(method):1))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 4, 1, (param)?param:"0", (param)?strlen(param):1))
                && MA_OK == (rc = ma_db_statement_set_int(db_stmt, 5, filter))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 6, 1, po_id, (po_id) ? strlen(po_id) : 0))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 7, 1, (method)?method:"0", (method)?strlen(method):1))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 8, 1, (param)?param:"0", (param)?strlen(param):1))
                && MA_OK == (rc = ma_db_statement_set_int(db_stmt, 9, filter))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 10, 1, po_id, (po_id) ? strlen(po_id) : 0))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 11, 1, product, (product) ? strlen(product) : 0))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 12, 1, (method)?method:"0", (method)?strlen(method):1))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 13, 1, (param)?param:"0", (param)?strlen(param):1)) )
            {
                rc = ma_db_statement_execute_query(db_stmt, db_record) ;
            }
            ma_db_statement_release(db_stmt) ;
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

#define GET_ALL_PRODUCTS_STMT "SELECT DISTINCT PPA_PRODUCT FROM MA_POLICY_PRODUCT_ASSIGNMENT MPPA, MA_POLICY_OBJECT MPO WHERE MPPA.PO_ID=MPO.PO_ID AND ((? = 0) OR (? = 1 AND MPO.PO_NAME = ? ))"
ma_error_t db_get_all_products(ma_db_t *db_handle, ma_int32_t agent_mode, ma_db_recordset_t **db_record) {
    if(db_handle && db_record) {
        ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;
		int tvalue = (agent_mode) ? 1 : 0;

        if( MA_OK == (rc = ma_db_statement_create(db_handle, GET_ALL_PRODUCTS_STMT, &db_stmt)) ) 
        {
			if( MA_OK == (rc = ma_db_statement_set_int(db_stmt, 1, tvalue))
                && MA_OK == (rc = ma_db_statement_set_int(db_stmt, 2, tvalue))
				&& MA_OK == (rc = ma_db_statement_set_string(db_stmt, 3, 1, "__EPO_ENFORCE_YES__", strlen("__EPO_ENFORCE_YES__"))))
				rc = ma_db_statement_execute_query(db_stmt, db_record) ;            
            ma_db_statement_release(db_stmt) ;
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

#define GET_POLICIES_FOR_PRODUCT_STMT "SELECT DISTINCT MPO.PO_FEATURE, MPO.PO_CATEGORY, MPO.PO_TYPE, MPO.PO_NAME, MPSO.PSO_NAME, MPSO.PSO_PARAM_INT, MPSO.PSO_PARAM_STR, MPSO.PSO_DATA FROM MA_POLICY_OBJECT MPO, MA_POLICY_PRODUCT_ASSIGNMENT MPPA, MA_POLICY_SETTINGS_OBJECT MPSO WHERE MPO.PO_ID = MPSO.PO_ID AND MPO.PO_ID = MPPA.PO_ID AND MPPA.PPA_PRODUCT = ? AND ( ? = 0 OR (? = 1 AND MPSO.PSO_TIMESTAMP = ?)) AND ( ? = 0 OR ( ? = 1 AND MPPA.PPA_METHOD = ? AND MPPA.PPA_PARAM = ?)) AND MPO.PO_CATEGORY != ? COLLATE NOCASE"
ma_error_t db_get_product_policies(ma_db_t *db_handle, const char *product, const char *timestamp, const char *method, const char *param, ma_db_recordset_t **db_record) {
    if(db_handle && product && db_record) {
        ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;
        int tvalue = (timestamp) ? 1 : 0;
        int mvalue = (method && param) ? 1 : 0;			

        if( MA_OK == (rc = ma_db_statement_create(db_handle, GET_POLICIES_FOR_PRODUCT_STMT, &db_stmt)) ) 
        {
            if( MA_OK == (rc = ma_db_statement_set_string(db_stmt, 1, 1, product, strlen(product)))
                && MA_OK == (rc = ma_db_statement_set_int(db_stmt, 2, tvalue))
                && MA_OK == (rc = ma_db_statement_set_int(db_stmt, 3, tvalue))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 4, 1, timestamp, (timestamp) ? strlen(timestamp) : 0))
				&& MA_OK == (rc = ma_db_statement_set_int(db_stmt, 5, mvalue))
                && MA_OK == (rc = ma_db_statement_set_int(db_stmt, 6, mvalue))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 7, 1, (method)?method:"0", (method)?strlen(method):1))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 8, 1, (param)?param:"0", (param)?strlen(param):1))
				&& MA_OK == (rc = ma_db_statement_set_string(db_stmt, 9, 1, ".EPO_ENFORCE", strlen(".EPO_ENFORCE"))) )
            {
                rc = ma_db_statement_execute_query(db_stmt, db_record) ;
            }
            ma_db_statement_release(db_stmt) ;
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

#define GET_POLICIES_BY_URI_STMT "SELECT DISTINCT MPSO.PSO_DATA FROM MA_POLICY_OBJECT MPO, MA_POLICY_SETTINGS_OBJECT MPSO, MA_POLICY_PRODUCT_ASSIGNMENT MPPA WHERE MPO.PO_ID = MPPA.PO_ID AND MPO.PO_ID = MPSO.PO_ID AND MPPA.PPA_PRODUCT = ? AND MPO.PO_FEATURE = ? AND MPO.PO_CATEGORY = ? AND MPO.PO_TYPE = ? AND MPO.PO_NAME = ? AND ((? = 0 ) OR (? = 1 AND MPSO.PSO_NAME = ? AND MPSO.PSO_PARAM_INT = ? AND MPSO.PSO_PARAM_STR = ?))"
ma_error_t db_get_product_policies_by_uri(ma_db_t *db_handle, const char *product_id, const char *po_feature, const char *po_category, const char *po_type, const char *po_name, const char *pso_name, const char *pso_param_int, const char *pso_param_str, ma_db_recordset_t **db_record) {
    if(db_handle && product_id && po_feature && po_category && po_type && po_name && db_record) {
        ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;
		int filter= 0;

        if(pso_name && pso_param_int && pso_param_str)
            filter = 1 ; 

        if( MA_OK == (rc = ma_db_statement_create(db_handle, GET_POLICIES_BY_URI_STMT, &db_stmt))) 
        {
            if( MA_OK == (rc = ma_db_statement_set_string(db_stmt, 1, 1, product_id, strlen(product_id)))
				&& MA_OK == (rc = ma_db_statement_set_string(db_stmt, 2, 1, po_feature, strlen(po_feature)))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 3, 1, po_category, strlen(po_category)))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 4, 1, po_type, strlen(po_type)))
				&& MA_OK == (rc = ma_db_statement_set_string(db_stmt, 5, 1, po_name, strlen(po_name)))
				&& MA_OK == (rc = ma_db_statement_set_int(db_stmt, 6, filter))
				&& MA_OK == (rc = ma_db_statement_set_int(db_stmt, 7, filter))				
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 8, 1, pso_name, pso_name ? strlen(pso_name): 0))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 9, 1, pso_param_int, pso_param_int ? strlen(pso_param_int):0))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 10, 1, pso_param_str, pso_param_str ? strlen(pso_param_str):0))
			)
            {
                rc = ma_db_statement_execute_query(db_stmt, db_record) ;
            }
            ma_db_statement_release(db_stmt) ;
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

#define GET_NOTIFIED_PRODUCTS_STMT "SELECT DISTINCT MPPA.PPA_PRODUCT FROM MA_POLICY_OBJECT MPO, MA_POLICY_PRODUCT_ASSIGNMENT MPPA, MA_POLICY_SETTINGS_OBJECT MPSO WHERE MPO.PO_ID = MPSO.PO_ID AND MPO.PO_ID = MPPA.PO_ID AND MPSO.PSO_TIMESTAMP = ? AND MPPA.PPA_PRODUCT IN (SELECT DISTINCT PPA_PRODUCT FROM MA_POLICY_PRODUCT_ASSIGNMENT MPPA1, MA_POLICY_OBJECT MPO1 WHERE MPPA1.PO_ID=MPO1.PO_ID AND MPO1.PO_NAME = ?)"
ma_error_t db_get_notified_product_list(ma_db_t *db_handle, const char *timestamp, ma_db_recordset_t **db_record) {
    if(db_handle && timestamp && db_record) {
        ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;
                   
        if( MA_OK == (rc = ma_db_statement_create(db_handle, GET_NOTIFIED_PRODUCTS_STMT, &db_stmt)) ) 
        {
            if(MA_OK == (rc = ma_db_statement_set_string(db_stmt, 1, 1, timestamp, strlen(timestamp)))
				&& MA_OK == (rc = ma_db_statement_set_string(db_stmt, 2, 1, "__EPO_ENFORCE_YES__", strlen("__EPO_ENFORCE_YES__")))) {
                rc = ma_db_statement_execute_query(db_stmt, db_record) ;
            }
            ma_db_statement_release(db_stmt) ;
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

#define GET_PSO_ID_STMT "SELECT DISTINCT MPSO.PSO_ID FROM MA_POLICY_OBJECT MPO, MA_POLICY_PRODUCT_ASSIGNMENT MPPA, MA_POLICY_SETTINGS_OBJECT MPSO WHERE MPO.PO_ID = MPSO.PO_ID AND MPO.PO_ID = MPPA.PO_ID AND MPPA.PPA_PRODUCT = ? AND MPO.PO_FEATURE = ? AND MPO.PO_CATEGORY = ? AND MPO.PO_TYPE = ? AND ((? = 0 ) OR (? = 1 AND MPO.PO_NAME = ? AND MPSO.PSO_NAME = ? AND MPSO.PSO_PARAM_INT = ? AND MPSO.PSO_PARAM_STR = ?))"
ma_error_t db_get_policy_setting_object_id(ma_db_t *db_handle, const char *product, const char *po_feature, const char *po_category, const char *po_type, const char *po_name, const char *pso_name, const char *pso_param_int, const char *pso_param_str, ma_db_recordset_t **db_record) {
    if(db_handle && product && po_feature && po_category && po_type && db_record) {
        ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;
		int filter= 0;

        if(po_name && pso_name && pso_param_int && pso_param_str)
            filter = 1 ;        

        if( MA_OK == (rc = ma_db_statement_create(db_handle, GET_PSO_ID_STMT, &db_stmt))) 
        {
            if( MA_OK == (rc = ma_db_statement_set_string(db_stmt, 1, 1, product, strlen(product)))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 2, 1, po_feature, strlen(po_feature)))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 3, 1, po_category, strlen(po_category)))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 4, 1, po_type, strlen(po_type)))
				&& MA_OK == (rc = ma_db_statement_set_int(db_stmt, 5, filter))
				&& MA_OK == (rc = ma_db_statement_set_int(db_stmt, 6, filter))
				&& MA_OK == (rc = ma_db_statement_set_string(db_stmt, 7, 1, po_name, po_name ? strlen(po_name): 0))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 8, 1, pso_name, pso_name ? strlen(pso_name): 0))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 9, 1, pso_param_int, pso_param_int ? strlen(pso_param_int):0))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 10, 1, pso_param_str, pso_param_str ? strlen(pso_param_str):0))
               )
            {
                rc = ma_db_statement_execute_query(db_stmt, db_record) ;
            }
            ma_db_statement_release(db_stmt) ;
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

#define UPDATE_PSO_DATA_STMT "UPDATE MA_POLICY_SETTINGS_OBJECT SET PSO_DATA = ? WHERE PSO_ID = ?"
ma_error_t db_update_policy_setting_object_data(ma_db_t *db_handle, ma_variant_t *pso_data, const char *pso_id) {
    if(db_handle && pso_data && pso_id) {
        ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;

        if( MA_OK == (rc = ma_db_statement_create(db_handle, UPDATE_PSO_DATA_STMT, &db_stmt))) 
        {
            if( MA_OK == (rc = ma_db_statement_set_variant(db_stmt, 1, pso_data))
                && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 2, 1, pso_id, strlen(pso_id)))               
               )
            {
                rc = ma_db_statement_execute(db_stmt) ;
            }
            ma_db_statement_release(db_stmt) ;
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}


#define GET_POLICY_ASSIGNMENT_PARAMS_STMT "SELECT DISTINCT PPA_PARAM FROM MA_POLICY_PRODUCT_ASSIGNMENT WHERE PPA_METHOD = ?"
ma_error_t db_get_assignment_params(ma_db_t *db_handle, const char *method, ma_db_recordset_t **db_record) {
    if(db_handle && method && db_record) {
        ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;

        if( MA_OK == (rc = ma_db_statement_create(db_handle, GET_POLICY_ASSIGNMENT_PARAMS_STMT, &db_stmt)) ) 
        {
            if( MA_OK == (rc = ma_db_statement_set_string(db_stmt, 1, 1, method, strlen(method))))              
            {
                rc = ma_db_statement_execute_query(db_stmt, db_record) ;
            }
            ma_db_statement_release(db_stmt) ;
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}
