#include "ma/internal/services/policy/ma_task_handler.h"
#include "ma/internal/services/policy/ma_policy_manifest.h"
#include "ma/internal/services/policy/ma_task_db.h"

#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/utils/network/ma_url_request.h"

#include "ma/internal/utils/xml/ma_xml.h"

#include "ma/ma_msgbus.h" 
#include "ma/ma_message.h" 
#include "ma/internal/ma_macros.h"

#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/defs/ma_policy_defs.h"
#include "ma/internal/defs/ma_io_defs.h"

#include "ma/internal/services/policy/ma_policy_common.h"
#include "ma/internal/utils/datastructures/ma_vector.h"
#include "ma/internal/utils/text/ma_byteorder.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "policy"

#define MA_CONTEXT_GET_TASK_DATABASE(context) (ma_configurator_get_task_database(MA_CONTEXT_GET_CONFIGURATOR(context)))

struct ma_task_handler_s {
    ma_policy_manager_t     *policy_manager;
    ma_uint32_t             download_task_index;
    ma_error_t              status;
    char                    *version;
    ma_policy_manifest_t    *manifest;
    ma_atomic_counter_t     ref_cnt;
    ma_bool_t               tasks_changed;
	ma_policy_url_request_t *task_url_request ;    
	ma_spipe_package_t      *spipe_pkg;

    /* dynamic array of site objects */
    ma_vector_define(download_task_vec, task_object_t*);
};

static ma_error_t download_tasks(ma_task_handler_t *self, const char *task_obj_id, const char *digest);
static ma_error_t load_hooked_task_from_spipe(ma_task_handler_t *self);
static ma_error_t convert_from_wire_form_to_db(ma_task_handler_t *self, const char *task_obj_id, unsigned char *task_xml, size_t xml_size);

ma_error_t ma_task_handler_create(ma_policy_manager_t *policy_manager, ma_task_handler_t **task_handler) {
    if(policy_manager && task_handler) {
        ma_task_handler_t *self = (ma_task_handler_t *)calloc(1, sizeof(ma_task_handler_t));
        if(self) {
            self->policy_manager = policy_manager;
            MA_ATOMIC_INCREMENT(self->ref_cnt);
            self->tasks_changed = MA_FALSE;
            *task_handler = self;
            return MA_OK;
        }
        return MA_ERROR_OUTOFMEMORY;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_task_handler_set_spipe_package(ma_task_handler_t *self, ma_spipe_package_t *spipe_pkg) {
    return ma_spipe_package_add_ref(self->spipe_pkg = spipe_pkg);
}

ma_error_t ma_task_handler_add_ref(ma_task_handler_t *self) {
    if(self) {
        MA_ATOMIC_INCREMENT(self->ref_cnt);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

static ma_error_t publish_task_change_msg(ma_task_handler_t *self) {
    ma_error_t rc = MA_OK;
    ma_message_t *task_change_msg = NULL;
    if(MA_OK == (rc = ma_message_create(&task_change_msg))) {                
        if(self->version) ma_message_set_property(task_change_msg , MA_PROP_KEY_TASK_CHANGE_VERSION_STR , self->version); /* sets time stamp to fetch new tasks */
        if(MA_OK != (rc = ma_msgbus_publish(MA_CONTEXT_GET_MSGBUS(self->policy_manager->context), MA_TASK_CHANGED_TOPIC_STR, MSGBUS_CONSUMER_REACH_INPROC, task_change_msg))){
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to publish task change message = <%d>", rc);
        }
        (void)ma_message_release(task_change_msg);
    }
    else
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to create message rc = <%d>", rc);    
  
    return rc;
}

static void publish_task_update_status(ma_task_handler_t *self, const char *status_topic) {
    ma_message_t *msg = NULL;
    ma_error_t rc = MA_OK;
    if(MA_OK == ma_message_create(&msg)) {
        (void)ma_msgbus_publish(MA_CONTEXT_GET_MSGBUS(self->policy_manager->context), status_topic, MSGBUS_CONSUMER_REACH_INPROC, msg);
        (void)ma_message_release(msg);
    }

    if(MA_OK != rc) {
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to publish task update topic <%s>", status_topic);
    }
    else
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "Successfully publish task update topic <%s>", status_topic);
}

static void release(void *p) {

}

void ma_task_handler_stop(ma_task_handler_t *self, ma_error_t status) {
    MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "update tasks end - received status - <%d>", status);
    if(MA_OK == status) {
        if(MA_OK == ma_db_transaction_end(MA_CONTEXT_GET_TASK_DATABASE(self->policy_manager->context)))
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "Task db transaction end");

        if(self->tasks_changed) {
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "Tasks are changed");
            (void)publish_task_change_msg(self);            
        }
        /*  TBD - should we send the status code also in published message ?? */
        (void)publish_task_update_status(self, MA_TASK_UPDATE_SUCCESS_TOPIC_STR);
    }
    else {        
        if(MA_OK == ma_db_transaction_cancel(MA_CONTEXT_GET_TASK_DATABASE(self->policy_manager->context))) {
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "Task db transaction canceled");
            (void)publish_task_update_status(self, MA_TASK_UPDATE_FAILURE_TOPIC_STR);
        }
    }

	if(!ma_vector_empty(self, download_task_vec)) {
        if(self->task_url_request)	ma_policy_url_request_cancel(self->task_url_request), self->task_url_request = NULL ;
		ma_vector_clear(self, task_object_t*, download_task_vec, release) ;
	}

    if(self->manifest)         (void)ma_policy_manifest_release(self->manifest);
    if(self->spipe_pkg)        (void)ma_spipe_package_release(self->spipe_pkg), self->spipe_pkg = NULL;
   
	self->policy_manager->task_request_status = MA_POLICY_MANIFEST_REQUEST_COMPLETED;

    ma_task_handler_release(self);
}

static ma_error_t download_connect_callback(void *user_data, ma_url_request_t *request) {  
    ma_task_handler_t *self = (ma_task_handler_t *)user_data;
    return MA_OK;
}

static void download_final_callback(ma_error_t status, long response_code, void *user_data, ma_url_request_t *request) {
    ma_error_t rc = MA_OK; 
    ma_task_handler_t *self = (ma_task_handler_t *)user_data;
    
    MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "download http response status <%d>, response code <%ld>.", status, response_code);
    if(200 == response_code) {   
		ma_bytebuffer_t *byte_buffer = NULL;
        ma_stream_t *mstream = (ma_stream_t *)self->task_url_request->io_streams.write_stream;
            
        if(MA_OK == (rc = ma_stream_get_buffer(mstream, &byte_buffer))) {
            unsigned char *xml_data = ma_bytebuffer_get_bytes(byte_buffer);
            size_t xml_len = ma_bytebuffer_get_size(byte_buffer);            
            rc = convert_from_wire_form_to_db(self, self->task_url_request->id, xml_data, xml_len);
            ma_bytebuffer_release(byte_buffer);           
        }
        else
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "failed to get bytebuffer from mstream rc <%d>", rc);
    }
    else {
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to download task <%s>, network return status <%d>, response code <%ld>", self->task_url_request->id, status, response_code);
        if(MA_ERROR_NETWORK_REQUEST_SUCCEEDED != status) 
            rc = MA_ERROR_POLICY_INTERNAL; 
    }

	if(MA_OK != rc) {
        if(MA_ERROR_NETWORK_REQUEST_ABORTED != status) {
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "calling task handler stop rc - <%d>", rc);
            (void)ma_policy_url_request_release(self->task_url_request);
            self->task_url_request = NULL;
			ma_task_handler_stop(self, rc);
		}
        return;
    }

    if( (self->download_task_index +1 ) == ma_vector_get_size(self, download_task_vec)) {
        (void)ma_policy_url_request_release(self->task_url_request);
        self->task_url_request = NULL;
        ma_task_handler_stop(self, rc);
        return;
    } 
    else {
        task_object_t *task_obj = NULL;
        ++self->download_task_index;
        task_obj = ma_vector_at(self, self->download_task_index, task_object_t *, download_task_vec);

		if(task_obj) {
			if(task_obj->is_hooked_task) {
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "Copying hooked task from spipe, task index <%d>", self->download_task_index);                
				if(MA_OK != (rc = load_hooked_task_from_spipe(self)))
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "failed to load hooked task from spipe rc - <%d>", rc);
				if(self->task_url_request) (void)ma_policy_url_request_release(self->task_url_request), self->task_url_request = NULL;
				ma_task_handler_stop(self, rc);
			}
			else {
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "download task from network, task index <%d>", self->download_task_index);                
				if(MA_OK != (rc =  download_tasks(self, task_obj->task_obj_id, task_obj->digest)))  {
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "failed to start download task, rc - <%d>", rc);
					(void)ma_policy_url_request_release(self->task_url_request), self->task_url_request = NULL;
					ma_task_handler_stop(self, rc);
				}
			}
		}
		else {
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_CRITICAL, "Failed to get task object from vector for index %d", self->download_task_index);
			(void)ma_policy_url_request_release(self->task_url_request), self->task_url_request = NULL;
			ma_task_handler_stop(self, MA_ERROR_UNEXPECTED);
			return;
		}
    }
}

static ma_error_t copy_task_xml_to_db(ma_task_handler_t *self, const char *taks_obj_id, unsigned char *xml_payload, size_t xml_len) {
    ma_error_t rc = MA_OK;
    ma_xml_t *task_xml = NULL;
    if(MA_OK == (rc = ma_xml_create(&task_xml))) {
        if(MA_OK == (rc = ma_xml_load_from_buffer(task_xml, (char*)xml_payload, xml_len))) {
            ma_xml_node_t *task_root = NULL;
            ma_xml_node_t *node =  NULL;
            if((NULL != (task_root = ma_xml_get_node(task_xml)))
                &&(NULL != (node = ma_xml_node_search_by_path(task_root, XML_TASK_OBJECT_ROOT_STR)))) {                
                ma_table_t *sec_table = NULL;
                ma_variant_t *task_data_var = NULL;              
                const char *task_id = (char *)ma_xml_node_attribute_get(node, XML_ID_STR);
                const char *type = (char *)ma_xml_node_attribute_get(node, XML_TYPE_STR);
                const char *priority = (char *)ma_xml_node_attribute_get(node, XML_PRIORITY_STR);
                const char *name = (char *)ma_xml_node_attribute_get(node, XML_NAME_STR);
                
                if(!task_id || !type || !priority || !name) {
                    MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "failed to get task details from manifest");
                    rc = MA_ERROR_POLICY_INTERNAL;            
                }

                if((MA_OK == rc) && (MA_OK == (rc = ma_table_create(&sec_table)))) {
                    ma_xml_node_t *sec_node = ma_xml_node_get_child(node);
                    while((MA_OK == rc) && sec_node) {
                        const char *sec_name = (char *)ma_xml_node_attribute_get(sec_node, "name");
                        ma_xml_node_t *set_node = ma_xml_node_get_child(sec_node);
                        ma_variant_t *set_var = NULL;
                        ma_table_t *set_table = NULL;
                        rc = ma_table_create(&set_table);
                        while((MA_OK == rc ) && set_node) {  
                            const char *key = (char *)ma_xml_node_attribute_get(set_node, "name");
                            const char *value = ma_xml_node_get_data(set_node);
                            if(key && value) {
                                ma_variant_t *val_var = NULL;
                                if(MA_OK == (rc = ma_variant_create_from_string(value, strlen(value), &val_var))) {
                                    rc = ma_table_add_entry(set_table, key, val_var);
                                    (void)ma_variant_release(val_var);                           
                                }
                            }
                            set_node = ma_xml_node_get_next_sibling(set_node);
                        }

                        if(MA_OK == (rc = ma_variant_create_from_table(set_table, &set_var))) {
                            rc = ma_table_add_entry(sec_table, sec_name, set_var);
                            (void)ma_variant_release(set_var);
                        }                                    
                        (void)ma_table_release(set_table);

                        sec_node = ma_xml_node_get_next_sibling(sec_node);
                    }
                
                    if(MA_OK == (rc = ma_variant_create_from_table(sec_table, &task_data_var))) {             
                        task_object_t *task_obj = NULL;
                        if(MA_OK == (rc = ma_policy_manifest_get_task_object(self->manifest, taks_obj_id, &task_obj))) {
                            if(MA_OK != (rc = db_add_epo_task(MA_CONTEXT_GET_TASK_DATABASE(self->policy_manager->context), taks_obj_id, task_id, name, task_obj->digest, type, priority, task_data_var, self->version)))
                                MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "failed to add task data into task table rc = <%d>", rc);
                        }
                        else
                            MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "failed to get task obj from manifest for task obj id <%s>, rc <%d>", taks_obj_id, rc);
                        (void)ma_variant_release(task_data_var);
                    }
                    (void)ma_table_release(sec_table);                                
                }
                else
                    MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "failed to create table");

            }
            else {
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "failed to get node from xml");
                rc = MA_ERROR_POLICY_INTERNAL;
            }
        }
        else
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "failed to load xml from buffer rc <%d>", rc);
                    
        (void)ma_xml_release(task_xml);
    }
    else
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "failed to create maxml rc <%d>", rc);

    return rc;
}

static ma_error_t convert_from_wire_form_to_db(ma_task_handler_t *self, const char *task_obj_id, unsigned char *task_xml, size_t xml_size) {
    ma_error_t rc = MA_OK;        

    ma_uint16_t pkg_type = 0;
    ma_uint32_t pkg_len = 0;
    ma_uint64_t obj_id = 0;
    ma_uint16_t xml_len = 0;
	ma_uint32_t data_len = 0;

    unsigned char *p = task_xml;     

    /* start (EPO) */       
    p += sizeof(ma_uint32_t);     

    /* package type  */   
	memcpy(&pkg_type, p, sizeof(ma_uint16_t));
    pkg_type = ma_byteorder_host_to_be_uint16(pkg_type);
    p += sizeof(ma_uint16_t);
   
    /* package length */
	memcpy(&pkg_len, p, sizeof(ma_uint32_t));
    pkg_len = ma_byteorder_host_to_be_uint32(pkg_len);
    p += sizeof(ma_uint32_t);
        
    /* object id */
	memcpy(&obj_id, p, sizeof(ma_uint64_t));
    obj_id = ma_byteorder_host_to_be_uint64(obj_id);
    p += sizeof(ma_uint64_t);
    
    /* data length */
	memcpy(&data_len, p, sizeof(ma_uint32_t));
    data_len = ma_byteorder_host_to_be_uint32(data_len);        
    p += sizeof(ma_uint32_t);

    rc = copy_task_xml_to_db(self, task_obj_id, p, data_len);

    p += data_len + sizeof(ma_uint32_t); /* length + END */
  

    return rc;
}

static ma_error_t load_hooked_task_from_spipe(ma_task_handler_t *self) {
    ma_error_t rc = MA_OK;

    while(self->download_task_index != ma_vector_get_size(self, download_task_vec)) {        
        char task_file_name[MA_MAX_LEN] = {0};
        const unsigned char *task_xml = NULL;
        size_t xml_size = 0;
        task_object_t *task_obj = ma_vector_at(self, self->download_task_index, task_object_t *, download_task_vec);

        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "copying po from spipe pkg, po index <%d>", self->download_task_index);

        MA_MSC_SELECT(_snprintf, snprintf)(task_file_name, MA_MAX_LEN, "%s.task", task_obj->digest);
        /* Checking for hooked policies from spipe package */
        if(MA_OK == ma_spipe_package_get_data(self->spipe_pkg, task_file_name, (void **)&task_xml, &xml_size)) {
			rc = convert_from_wire_form_to_db(self, task_obj->task_obj_id, (unsigned char *)task_xml, xml_size);
        }
        
        if(MA_OK != rc) break;

        ++self->download_task_index;
    }

    return rc;
}

static ma_error_t download_tasks(ma_task_handler_t *self, const char *task_obj_id, const char *digest) {
    ma_error_t rc = MA_OK;
    char url[MA_POLICY_TASK_MAX_URL_LENGTH_INT] = {0};

	MA_MSC_SELECT(_snprintf, snprintf)(url, MA_POLICY_TASK_MAX_URL_LENGTH_INT, "%s://%s:%d/policy/task.%s.%s", self->policy_manager->last_connection_from_ah->is_secure_port ? "https" : "http", self->policy_manager->last_connection_from_ah->address, self->policy_manager->last_connection_from_ah->port, task_obj_id, digest);	
	
    MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "task object download URL <%s>", url);
    if(MA_OK == (rc = ma_policy_url_request_reset_stream(self->task_url_request))) {
        strncpy(self->task_url_request->id, task_obj_id, strlen(task_obj_id));
	    if(MA_OK != (rc = ma_policy_url_request_send(self->task_url_request, url, download_connect_callback, download_final_callback, self))) {
		    MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "task url request send failed for task object id <%s>", task_obj_id);
            (void)ma_policy_url_request_release(self->task_url_request);
            self->task_url_request = NULL;
		}
    }
    else
	    MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to reset task url request, rc <%d>", rc);

	return rc;
}

static ma_ah_site_t *get_agent_handler_site(ma_task_handler_t *self, const char *server) {
    size_t i=0;
    ma_ah_sitelist_t *ah_sitelist = NULL;
	(void)ma_ah_client_service_get_sitelist(MA_CONTEXT_GET_AH_CLIENT(self->policy_manager->context), &ah_sitelist);
	if(ah_sitelist){
		size_t count = ma_ah_sitelist_get_count(ah_sitelist);    
		for(i = 0; i < count ; i++) {
			ma_ah_site_t *site = ma_ah_sitelist_get_site(i, ah_sitelist);
			if(site) {
				const char *ah_server = ma_ah_site_get_short_name(site);
				if(ah_server && !strcmp(ah_server, server))
					return site;
			}
		}
	}
    return NULL;
}

static int task_order_compare(const void *t1, const void *t2) {
    task_object_t **task1 = (task_object_t **)t1;
    task_object_t **task2 = (task_object_t **)t2;        
    return ((*task1)->is_hooked_task - (*task2)->is_hooked_task);
}

static ma_error_t update_tasks(ma_task_handler_t *self) {
    ma_error_t rc = MA_OK;
    ma_array_t *task_id_arr = NULL;
    if(MA_OK == (rc = ma_policy_manifest_get_task_ids(self->manifest, &task_id_arr))) {
        ma_variant_t *id_var = NULL;
        size_t size =0;
        
        (void)ma_array_size(task_id_arr, &size); 
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "Number of tasks objects to be downloaded <%d>", size);

        if(size) {
            const char *server_name = ma_policy_manifest_get_handler(self->manifest);  
            const char *created_time = ma_policy_manifest_get_created_time(self->manifest);
            if(!created_time) {
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to get the created time from manifest file");            
                rc = MA_ERROR_POLICY_INTERNAL;
            }
            else { 
                /*if(NULL == (self->ah_site = get_agent_handler_site(self, server_name))) {
                    MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to get ah site of agent handler <%s>", server_name);
					(void)ma_array_release(task_id_arr);
                    return MA_ERROR_POLICY_INTERNAL;
                }*/
                self->tasks_changed = MA_TRUE;
                self->version = strdup(created_time); /* This value will be inserted in task table for new tasks and also same will be sending in task change message */            
            }
            
            ma_vector_init(self, size, download_task_vec, task_object_t *);    

        }else {
            /* This will happen only for delete task scenarios */
            if(self->tasks_changed)
                (void)ma_task_handler_stop(self, rc);
        }


        while((MA_OK == rc) && size) {
            if(MA_OK == (rc = ma_array_get_element_at(task_id_arr, size, &id_var))) {
                ma_buffer_t *buff = NULL;
                if(MA_OK == (rc = ma_variant_get_string_buffer(id_var, &buff))) {
                    const char *task_obj_id = NULL;
                    size_t len = 0;
                    if(MA_OK == (rc = ma_buffer_get_string(buff, &task_obj_id, &len))) {
                        task_object_t *task_data = NULL;
                        if(task_obj_id && (MA_OK == (rc = ma_policy_manifest_get_task_object(self->manifest, task_obj_id, &task_data)))) {
							char task_file_name[MA_MAX_LEN] = {0};
                            const char *task_xml = NULL;
                            size_t xml_size = 0;
                            MA_MSC_SELECT(_snprintf, snprintf)(task_file_name, MA_MAX_LEN, "%s.task", task_data->digest);
                                    
                            /* Checking for hooked tasks from spipe package */
                            if(MA_OK == ma_spipe_package_get_data(self->spipe_pkg, task_file_name, (void **)&task_xml, &xml_size)) {                                        
								MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_TRACE, "pushed hooked task ID <%s> into task downloading vector list.", task_data->task_obj_id);
								task_data->is_hooked_task = MA_TRUE;
							}
							else
								MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_TRACE, "pushed task obj ID <%s> into downloading vector list.", task_obj_id);

                            ma_vector_push_back(self, task_data, task_object_t*, download_task_vec);
                        }
                        else
                            MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to get task object from manifest for task object ID <%s>", task_obj_id);
                    }
                    else
                        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to get string from buffer - rc <%d>", rc);
                    (void)ma_buffer_release(buff);
                }
                else
                    MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to get buffer from variant - rc <%d>", rc);
                (void)ma_variant_release(id_var);
            }
            else
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to get the element <%d> from task array ", size);

            size--;
        }
        (void)ma_array_release(task_id_arr);
    } 
    else
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to get task ids array from manifest rc <%d>", rc);


    if(MA_OK == rc) {
		/* pop-up all POs to be downloaded from network */
        ma_vector_sort(self, task_object_t*, download_task_vec, task_order_compare);
		
		if(!ma_vector_empty(self,download_task_vec)) {
			task_object_t *task_obj = NULL;
			self->download_task_index = 0;

			task_obj = ma_vector_at(self, self->download_task_index, task_object_t *, download_task_vec);
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "Total task objects to be downloaded <%d>", ma_vector_get_size(self, download_task_vec));

			/* start downloading one by one */
			if(task_obj->is_hooked_task) {
				if(MA_OK != (rc = load_hooked_task_from_spipe(self)))
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "failed to load hooked task from spipe rc - <%d>", rc);
				(void) ma_task_handler_stop(self, rc);
			}
			else {
				if(MA_OK == (rc = ma_policy_url_request_create(self->policy_manager, &self->task_url_request))) {
					if(self->policy_manager->last_connection_from_ah->proxy_list)
						ma_policy_url_request_set_proxy(self->task_url_request, self->policy_manager->last_connection_from_ah->proxy_list);
					
					if(MA_OK != (rc = download_tasks(self, task_obj->task_obj_id, task_obj->digest))) {
						MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to start task downloads- rc <%d>", rc);
					}else {
						ma_task_handler_add_ref(self);
					}
				}
				else
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to create task url request object rc <%d>", rc);
			}
		}
	}
    return rc;    
}


static ma_error_t update_task_objects(ma_task_handler_t *self) {
    ma_error_t rc = MA_OK;
    ma_db_recordset_t *record_set = NULL;

    if(MA_OK != (rc = db_get_epo_task(MA_CONTEXT_GET_TASK_DATABASE(self->policy_manager->context), NULL, &record_set))) 
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to get the task records from task table - rc <%d>", rc);
        
    while((MA_OK == rc) && (MA_ERROR_NO_MORE_ITEMS != (rc = ma_db_recordset_next(record_set)))) {
        const char *task_obj_id = NULL, *digest = NULL;
        
        if(MA_OK != rc)
            break;

        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "Got new record from task table");

        (void)ma_db_recordset_get_string(record_set, 1, &task_obj_id);  /* first column is Task Object ID */
        (void)ma_db_recordset_get_string(record_set, 2, &digest); /* third column is Task Digest */

        if(task_obj_id && digest) {
            task_object_t *task = NULL;
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "Updating task object id <%s>", task_obj_id);
            if(MA_OK == (rc = ma_policy_manifest_get_task_object(self->manifest, task_obj_id, &task)) && task) {
                if(!strncmp(task->digest, digest, strlen(digest))) {
                    if(MA_OK != (rc = ma_policy_manifest_delete_task_object(self->manifest, task_obj_id))) {
                        rc = MA_ERROR_POLICY_INTERNAL;
                        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to delete task object id from manifest data rc = <%d>", rc);
                    }
                }
            }
            else if(MA_ERROR_OBJECTNOTFOUND == rc) { 
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "Task obj id <%s> is marked as a deleted task", task_obj_id);                
                /* Setting timestamp as "0" for deleted tasks. task service can identify the delete tasks based on "0" timestamp value */
                if(MA_OK == (rc = db_update_epo_task(MA_CONTEXT_GET_TASK_DATABASE(self->policy_manager->context), task_obj_id, "0"))) {
                    self->tasks_changed = MA_TRUE;
					ma_task_handler_add_ref(self);
				}
                else {
                    rc = MA_ERROR_POLICY_INTERNAL;
                    MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to update epo task in task DB rc = <%d>", rc);
                }
            }
            else {
                rc = MA_ERROR_POLICY_INTERNAL;
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to get task object id from manifest data rc = <%d>", rc);
            }
        }
        else {
            rc = MA_ERROR_POLICY_INTERNAL;
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to get the task object id and digest from task table");
        }        
    }
    
	if(MA_ERROR_NO_MORE_ITEMS == rc)
		rc = MA_OK;

    if(record_set) (void)ma_db_recordset_release(record_set);

    return (MA_OK == rc) ? update_tasks(self) : rc ;
}

static void assign_key_array_foreach(size_t index, ma_variant_t *assign_key_var,  void *cb_args, ma_bool_t *stop_loop) {
    ma_buffer_t *buff = NULL;
    ma_error_t rc = MA_OK;
    ma_task_handler_t *self = (ma_task_handler_t *)cb_args;
    if(MA_OK == (rc = ma_variant_get_string_buffer(assign_key_var, &buff))) {
        char *key = NULL;
        size_t size = 0;
        if(MA_OK == (rc = ma_buffer_get_string(buff, &key, &size)) && key) {
            const assignments_object_t *task_assign = NULL;            
            if((MA_OK == (rc = ma_policy_manifest_get_assignment(self->manifest, key, &task_assign))) && task_assign) {
                /* method and param parameters are optional, so these values can be NULL */
                if(MA_OK != (rc = db_add_epo_task_assignment(MA_CONTEXT_GET_TASK_DATABASE(self->policy_manager->context), task_assign->obj_id , task_assign->product, task_assign->method, task_assign->param)))
                    MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to insert data into task assignments table - rc <%d>", rc);
            }
            else
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to get the assignment data from assignment table for key <%s>", key);
        }
        else
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to get the string data from buffer - rc <%d>", rc);

        (void) ma_buffer_release(buff);
    }
    else
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to get the string buffer from variant - rc <%d>", rc);

    /* If insertion operation failed for any item, loop stops, the same error propagate to the foreach caller */
    if(MA_OK != rc) {
        self->status = rc;
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "assignment data insertion operation failed - rc <%d>", rc);
        *stop_loop = MA_TRUE;
    }
}

/* To Be Reviewed - comparison of task assignments list may not be required as a change in assignment list means not a change in tasks.
   If there is any change in the assignments list i.e., delete or addition, definitely there is a corresponding change in task object list. 
   if yes, we can directly overwrite the data in DB instead of comparision*/ 
static ma_error_t update_assignments(ma_task_handler_t *self) {
    ma_db_recordset_t *record_set = NULL;
    ma_error_t rc = MA_OK;

    if(MA_OK != (rc = db_get_epo_task_assignments(MA_CONTEXT_GET_TASK_DATABASE(self->policy_manager->context), NULL, NULL, NULL, NULL, &record_set)))
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to get the epo task assignments from DB - rc <%d>", rc);

    while((MA_OK == rc) && (MA_ERROR_NO_MORE_ITEMS != (rc = ma_db_recordset_next(record_set)))) {
        const char *task_obj_id = NULL, *product = NULL, *method = NULL, *param = NULL;

        if(MA_OK != rc)
            break;

        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "Got New record from task assginment table");

        (void)ma_db_recordset_get_string(record_set, 1, &task_obj_id);       /* 1st column is Task ID */
        (void)ma_db_recordset_get_string(record_set, 2, &product);  /* 2nd column is Product ID */                    
        (void)ma_db_recordset_get_string(record_set, 3, &method);   /* 3rd column is method */
        (void)ma_db_recordset_get_string(record_set, 4, &param);    /* 4th column is param */

        if(task_obj_id && product) {
            char key[MA_MAX_LEN] = {0};
            MA_MSC_SELECT(_snprintf, snprintf)(key, MA_MAX_LEN, "%s|%s|%s|%s", task_obj_id, product, (method)?method:"0", (param)?param:"0");                        
            
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "updating task assignment for <%s>", key);

            if(ma_policy_manifest_find_assignment(self->manifest, key))
                rc = ma_policy_manifest_delete_assignment(self->manifest, key);            
            else {
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "task assignment <%s> to be removed from task assignment table when task object deletes", key);                
                /* To Be Reviewed - Is it possible to exist task object without any assignment ??? */
            }
        }
        else {
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to get the task obj ID and product from task assignment table");
            rc = MA_ERROR_POLICY_INTERNAL;
        }
    }
    
	if(MA_ERROR_NO_MORE_ITEMS == rc)
		rc = MA_OK;

    if(record_set) (void)ma_db_recordset_release(record_set);

    if(MA_OK == rc) {
        ma_array_t  *key_arr = NULL;
        if(MA_OK == (rc = ma_policy_manifest_get_assigment_keys(self->manifest, &key_arr))) {
            size_t size = 0;
            (void)ma_array_size(key_arr, &size);
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "Number of new/modified task assignments in manifest <%d>", size);
            if(size > 0) {               
                /* To be reviewed - is Assignment addition a task change?? Assume that task and product mapping is 1:1 */
                self->status = MA_OK;
                if(MA_OK == (rc = ma_array_foreach(key_arr, assign_key_array_foreach, self)))
                    rc = self->status;
                self->status = MA_OK;
            }
			(void)ma_array_release(key_arr);
        }
    }

    return rc;
}

ma_error_t ma_task_handler_start(ma_task_handler_t *self, unsigned char *manifest, size_t manifest_size) {
    ma_error_t rc = MA_OK;
    
	ma_task_handler_add_ref(self);
    if(MA_OK == (rc = db_add_task_instance(MA_CONTEXT_GET_TASK_DATABASE(self->policy_manager->context)))) {
        if(MA_OK == (rc = ma_db_transaction_begin(MA_CONTEXT_GET_TASK_DATABASE(self->policy_manager->context)))) {/* transaction end will be executed in update_tasks_end */
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "Succeeded DB transaction begin");
	}
        else
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed DB transaction begin");
    }
    else
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to create task DB instance"); 
 
    if((MA_OK == rc ) && (MA_OK == (rc = ma_policy_manifest_create(&self->manifest)))) {
        if(MA_OK == (rc = ma_policy_manifest_load(self->manifest, manifest, manifest_size, MA_FALSE))) {
            if(MA_OK == (rc = update_assignments(self))) {
                if(MA_OK == (rc = update_task_objects(self))) {
                    MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_DEBUG, "Succeeded to update tasks");                
		        }       
                else
                    MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to update task objects - rc <%d>", rc);
            }
            else
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to update assignments - rc <%d>", rc);
        }
        else
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to load manifest - rc <%d>", rc);
    }
    else
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "Failed to create manifest instance - rc <%d>", rc);

    if(MA_OK == rc)  {
		if(!self->tasks_changed)  /* tasks are not changed */
            (void)ma_task_handler_stop(self, rc);
    }
    else{
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->policy_manager->context), MA_LOG_SEV_ERROR, "update tasks stop called due to the failure - rc <%d>", rc);
        ma_task_handler_stop(self, rc);
    }
   
    return rc;
}

static ma_error_t ma_task_handler_destroy(ma_task_handler_t *self) {
    if(self) {
        if(self->version) free(self->version);
		free(self);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_task_handler_release(ma_task_handler_t *self) {    
    if(self) {
        if(0 == MA_ATOMIC_DECREMENT(self->ref_cnt)) {
            (void)ma_task_handler_destroy(self);
        }
		return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}
