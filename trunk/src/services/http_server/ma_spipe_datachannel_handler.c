
#include "ma_spipe_handler_internal.h"
#include "ma/internal/defs/ma_datachannel_defs.h"
#include "ma/ma_message.h"

static ma_error_t datachannel_req_async_cb(ma_error_t status, ma_message_t *response, void *cb_data, ma_msgbus_request_t *request) {
	ma_spipe_handler_t *self = (ma_spipe_handler_t *) cb_data;

	ma_spipe_handler_send_response(self, status);

	if((status == MA_OK) && response) ma_message_release(response);
	if(request) {
		ma_msgbus_endpoint_t * ep = NULL;
		(void) ma_msgbus_request_get_endpoint(request, &ep);		
		(void) ma_msgbus_request_release(request);
        if(ep) (void) ma_msgbus_endpoint_release(ep);
	}

	return MA_OK;
}

ma_error_t ma_spipe_datachannel_handler(ma_spipe_handler_t *self) {
	ma_spipe_handler_factory_t *factory = (ma_spipe_handler_factory_t *)self->base.data ;
    ma_http_server_t *server = (ma_http_server_t *)factory->base.data;
	ma_error_t rc = MA_OK;
	ma_msgbus_endpoint_t *ep = NULL;
	ma_message_t *req_msg = NULL;	
	
	if(MA_OK == (rc = ma_msgbus_endpoint_create(server->msgbus, MA_DATACHANNEL_SERVICE_NAME_STR, NULL, &ep))) {
		(void) ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
        (void) ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
		if(MA_OK == (rc = ma_message_create(&req_msg))) {
			ma_msgbus_request_t *request = NULL;
			(void) ma_message_set_property(req_msg, EPO_DATACHANNEL_MESSAGES_TYPE_STR, EPO_DATACHANNEL_MESSAGES_TYPE_MSGAVAILABLE_STR);
			/* TBD - Does Data channel service expects product ID also ??? */
			if(MA_OK != (rc = ma_msgbus_async_send(ep, req_msg, datachannel_req_async_cb, self, &request))) {				
				(void) ma_msgbus_endpoint_release(ep);
			}	
			(void) ma_message_release(req_msg);
		}
		else
			(void) ma_msgbus_endpoint_release(ep);
	}

	if(MA_OK != rc) ma_spipe_handler_send_response(self, rc);
	return rc;

	return MA_OK;
}

