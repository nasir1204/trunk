#include "ma/internal/services/http_server/ma_url_cache.h"
#include "ma/internal/services/http_server/ma_url_cache_manager.h"
#include "ma/internal/services/http_server/ma_http_server_utils.h"
#include "ma/internal/utils/datastructures/ma_queue.h"
#include "ma/internal/utils/filesystem/ma_fs_utils.h"
#include "ma/internal/utils/downloader/ma_repository_downloader.h"
#include "ma/internal/ma_strdef.h"
#include "ma/datastore/ma_ds_ini.h"
#include "ma/internal/utils/filesystem/ma_fs_utils.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal/utils/platform/ma_sys_apis.h"

#include "ma_http_repository_handler_internal.h"

#include "ma/internal/utils/time/ma_time.h"
#include "ma/internal/services/p2p/ma_p2p_db_manager.h"
#include "ma/internal/utils/configurator/ma_configurator.h"

#include <stdio.h>
#include <fcntl.h>

struct ma_url_cache_s {
    ma_atomic_counter_t                     ref_count;

    ma_url_cache_state_t                    cache_state;    

    uv_loop_t                               *uv_loop;

    ma_logger_t                             *logger;

    char                                    *file;

	char									*file_hash ;

    char                                    temp_download_file[MA_HTTP_MAX_URL_PATH_LEN];

    ma_bool_t                               is_file_downloading_in_temp_file;

    size_t                                  file_size;

    size_t                                  write_file_offset;

    uv_file                                 fd;

    ma_queue_t                              qhead;  

    ma_ds_t                                 *ds;
    
    ma_http_repository_handler_factory_t    *factory;

    uv_async_t                              async;

	ma_repository_t							*valid_repository;

	char									*sahu_hdr_value;

    ma_bool_t                               honor_lazycache;
};

typedef struct ma_cache_reader_s {
    ma_queue_t                          qlink; 

    ma_on_cache_ready_cb_t              cb;

    void                                *cb_data;

    ma_url_cache_t                      *url_cache;

    size_t                              file_cur_pos;

    uv_async_t                          reader_async;

	ma_bool_t							is_served_p2p_content ;
} ma_cache_reader_t;


static ma_bool_t get_file_hash_from_replica_log(ma_url_cache_t *self, uv_buf_t *hash_buf, uv_buf_t *hash_buf256);
static void cache_async_cb(uv_async_t *handle, int status);

ma_error_t ma_url_cache_create(uv_loop_t *uv_loop, const char *file, ma_url_cache_t **url_cache) {
    if(file && url_cache) {
        ma_url_cache_t *self = (ma_url_cache_t *)calloc(1, sizeof(ma_url_cache_t));
        if(self) {            
            self->file = strdup(file);
            self->ref_count = 1;
            self->uv_loop = uv_loop;
            self->cache_state = MA_URL_CACHE_INITIALIZED;
            ma_queue_init(&self->qhead);  
            self->async.data = self;
            uv_async_init(self->uv_loop, &self->async, cache_async_cb);
            self->fd = -1;
            *url_cache = self;
            return MA_OK;            
        }
        return MA_ERROR_OUTOFMEMORY;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_url_cache_set_repository_factory(ma_url_cache_t *self, struct ma_http_repository_handler_factory_s *factory){
    if(self && factory) {
        self->factory = factory;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_url_cache_set_ds(ma_url_cache_t *self, struct ma_ds_s *ds) {
    if(self && ds) {
        self->ds = ds;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_url_cache_set_logger(ma_url_cache_t *self, struct ma_logger_s *logger) {
    if(self && logger) {
        self->logger = logger;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_url_cache_set_repository(ma_url_cache_t *self, ma_repository_t *repository) {
	if(self && repository) {
        ma_repository_add_ref(self->valid_repository = repository);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_url_cache_state_t ma_url_cache_get_state(ma_url_cache_t *self) {
    return (self) ? self->cache_state : MA_URL_CACHE_UNKNOWN;
}

char *ma_url_cache_get_file_hash(ma_url_cache_t *url_cache) {
	return (url_cache->file_hash) ? url_cache->file_hash : NULL ;
}

ma_error_t ma_url_cache_set_state(ma_url_cache_t *self, ma_url_cache_state_t state) {
    if(self) {
        self->cache_state = state;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_url_cache_add_ref(ma_url_cache_t *self) {
    if (self && (0 <= self->ref_count)) {
        MA_LOG(self->logger, MA_LOG_SEV_TRACE, "add ref to file cache <%s>", self->file); /* TBD -  This is added for debugging, will be removed */
        MA_ATOMIC_INCREMENT(self->ref_count);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t on_replica_log_cache_ready_cb(ma_url_cache_t *replica_log_cache, ma_url_cache_state_t state, size_t replica_log_file_size, void *cookie, void *cb_data) {
    ma_url_cache_t *self = (ma_url_cache_t *) cb_data;
    
    MA_LOG(self->logger, MA_LOG_SEV_TRACE, "File <%s> cache callback, state <%d> file size <%d>", replica_log_cache->file, state, replica_log_file_size); 
    if(state == MA_URL_CACHE_INITIALIZED || state == MA_URL_CACHE_VALIDATION_START || state == MA_URL_CACHE_DOWNLOAD_START || state == MA_URL_CACHE_DOWNLOADING) {
        // log a statement here, these states can be ignored in this case.    
        return MA_OK;
    }
    else if(state == MA_URL_CACHE_READY) {  /* relica log can be processed only after download completes */
        char local_file_path[MA_HTTP_MAX_URL_PATH_LEN] = {0};
        ma_http_server_t *server = (ma_http_server_t *)self->factory->base.data;
        if(get_local_file_path_for_file(self->file, server->policies.virtual_dir, local_file_path, MA_HTTP_MAX_URL_PATH_LEN)) {
            if(ma_fs_is_file_exists(self->uv_loop, local_file_path)) {
                uv_buf_t hash = uv_buf_init(0,0), hash256 = uv_buf_init(0,0);                    
                if(get_file_hash_from_replica_log(self, &hash, &hash256)) {
					if((hash.base && verify_file_hash(self->uv_loop, MA_CONTEXT_GET_CRYPTO(self->factory->context), local_file_path, MA_CRYPTO_HASH_SHA1, hash)) 
                        || (hash256.base && verify_file_hash(self->uv_loop, MA_CONTEXT_GET_CRYPTO(self->factory->context), local_file_path, MA_CRYPTO_HASH_SHA256, hash256))) {                                                                    
                        self->cache_state = MA_URL_CACHE_READY;
                        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "File <%s> HASH verification succeeded", self->file);
						self->file_hash = strdup(hash.base? hash.base: hash256.base) ; /* TODO set the proper hash */
                    } 
                    else {
                        self->cache_state = MA_URL_CACHE_DOWNLOAD_START;
                        MA_LOG(self->logger, MA_LOG_SEV_TRACE, "File <%s> HASH verification failed, so start download", self->file); 
                    }
                    if(hash.base) free(hash.base); 
                    if(hash256.base) free(hash256.base);
                }
                else
                    self->cache_state = MA_URL_CACHE_VALIDATION_FAILED;
            }
            else                 
                self->cache_state = MA_URL_CACHE_DOWNLOAD_START;            
        }
        else
            self->cache_state = MA_URL_CACHE_VALIDATION_FAILED;

        if(MA_OK == ma_url_cache_read_stop(replica_log_cache, cookie))
            (void) ma_url_cache_release(replica_log_cache);  

        uv_async_send(&self->async);
        return (self->cache_state == MA_URL_CACHE_DOWNLOAD_START || self->cache_state == MA_URL_CACHE_READY) ? MA_OK : MA_ERROR_HTTP_SERVER_INTERNAL;
    }
    else { /* MA_URL_CACHE_VALIDATION_FAILED, MA_URL_CACHE_DOWNLOAD_FAILED, MA_URL_CACHE_DEINITIALIZED, MA_URL_CACHE_ERROR, MA_URL_CACHE_UNKNOWN */
        self->cache_state = MA_URL_CACHE_VALIDATION_FAILED;
        if(MA_OK == ma_url_cache_read_stop(replica_log_cache, cookie))
            (void) ma_url_cache_release(replica_log_cache);       
        uv_async_send(&self->async);
        return MA_ERROR_HTTP_SERVER_INTERNAL;;
    }    
}

static ma_error_t start_validation(ma_url_cache_t *self) {
    ma_error_t rc = MA_ERROR_HTTP_SERVER_INTERNAL;
    char replica_log[MA_HTTP_MAX_URL_PATH_LEN] = {0};
    
    MA_LOG(self->logger, MA_LOG_SEV_TRACE, "File <%s> validation starts", self->file);

    if(get_replica_log_from_file(self->file, replica_log, MA_HTTP_MAX_URL_PATH_LEN)) {
        ma_url_cache_t *replica_log_url_cache = NULL;
        ma_http_server_t *server = (ma_http_server_t *)self->factory->base.data;
        if(MA_OK != (rc = ma_url_cache_manager_search(server->cache_manager, replica_log, &replica_log_url_cache))) { 
            MA_LOG(self->logger, MA_LOG_SEV_TRACE, "File <%s> not found in cache", replica_log);            
            if(MA_OK == (rc = ma_url_cache_create(self->uv_loop, replica_log, &replica_log_url_cache))) {
                (void) ma_url_cache_set_ds(replica_log_url_cache, self->ds);
                (void) ma_url_cache_set_repository_factory(replica_log_url_cache, self->factory);
				(void) ma_url_cache_set_repository(replica_log_url_cache, self->valid_repository);
                (void) ma_url_cache_set_logger(replica_log_url_cache, self->logger);

				if(self->sahu_hdr_value)
					ma_url_cache_set_sahu_header(replica_log_url_cache, self->sahu_hdr_value);

                rc = ma_url_cache_manager_add(server->cache_manager, replica_log, replica_log_url_cache);
            }    
        }

        if(MA_OK == rc) {
            MA_LOG(self->logger, MA_LOG_SEV_TRACE, "File <%s> cache read start", replica_log); 
            rc = ma_url_cache_read_start(replica_log_url_cache, self->honor_lazycache, on_replica_log_cache_ready_cb, self);        
        }
    }
    
    if(MA_OK != rc) {
        MA_LOG(self->logger, MA_LOG_SEV_TRACE, "File <%s> validation Failed, rc <%d>", self->file, rc);
        self->cache_state = MA_URL_CACHE_VALIDATION_FAILED;      
    }

    return rc;
}

static ma_error_t on_download_progress(ma_downloader_t *downloader, ma_download_progress_state_t state, ma_stream_t *stream, size_t download_total, void *cb_data) { 
    ma_url_cache_t *self = (ma_url_cache_t *)cb_data;
    ma_bool_t is_download_done = MA_FALSE;

    MA_LOG(self->logger, MA_LOG_SEV_TRACE, "File <%s> download callback, state <%d> and total download <%d>", self->file, state, download_total);
    
	if(MA_DOWNLOADER_DOWNLOAD_IN_PROGRESS == state) {
        self->file_size = download_total;  
        self->cache_state = MA_URL_CACHE_DOWNLOADING;  
    }
    else if(MA_DOWNLOADER_DOWNLOAD_DONE == state) {
        self->cache_state = MA_URL_CACHE_READY; 
        is_download_done = MA_TRUE;
		uv_async_send(&self->async);
    }
    else if(MA_DOWNLOADER_DOWNLOAD_CANCLED == state || MA_DOWNLOADER_DOWNLOAD_FAILED == state || MA_DOWNLOADER_CONNECT_FAILED == state) {
        self->cache_state = MA_URL_CACHE_ERROR;
        is_download_done = MA_TRUE;
        uv_async_send(&self->async);
    }    
    else {
        self->cache_state = MA_URL_CACHE_ERROR;
        /* Don't hit this case any scenario */
        is_download_done = MA_TRUE;   
        uv_async_send(&self->async);
    }
        
    if(is_download_done) {        
        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "File <%s> download done with status <%d>", self->file, state);
        (void) ma_downloader_release(downloader);
        (void) ma_stream_release(stream);   
    }      

    return MA_OK;
}

size_t netstream_callback(const unsigned char *data, size_t size, void *userdata) {
    ma_url_cache_t *self = (ma_url_cache_t *)userdata;
    size_t written_bytes = 0; 

    uv_fs_t fs_req;              

	if(-1 != self->fd) {
		if(-1 != uv_fs_write(self->uv_loop, &fs_req, self->fd, (void *)data, size,  self->write_file_offset, NULL)) {
			written_bytes = fs_req.result;
			self->write_file_offset += written_bytes;
			uv_fs_req_cleanup(&fs_req);
			uv_fs_fsync(self->uv_loop, &fs_req, self->fd, NULL);
			uv_async_send(&self->async);
		}
	}
	else
		MA_LOG(self->logger, MA_LOG_SEV_ERROR, "write file descriptor is  <%d>", self->fd);
   
    return written_bytes;
}

static ma_error_t start_file_downloading(ma_url_cache_t *self) {
    ma_error_t rc = MA_ERROR_HTTP_SERVER_INTERNAL;
    ma_stream_t *stream = NULL;
    ma_downloader_t  *downloader = NULL;

    MA_LOG(self->logger, MA_LOG_SEV_TRACE, "File <%s> download starts", self->file);       

    if(MA_OK == (rc = ma_repository_downloader_create(&downloader))) {        
        if(MA_OK == (rc = ma_netstream_create(netstream_callback, self, &stream))) {
			(void) ma_repository_downloader_set_net_client(downloader, MA_CONTEXT_GET_NET_CLIENT(self->factory->context));
			(void)ma_repository_downloader_set_ds(downloader, self->ds);
			/* The repositoy object must be available as we have alreayd identified the valid repository before arraving this statement */
			(void) ma_repository_downloader_set_repository(downloader, self->valid_repository);
            (void) ma_repository_downloader_set_proxy_config(downloader, self->factory->proxy_config);
            (void) ma_repository_downloader_set_logger(downloader, self->logger);
			(void) ma_repository_downloader_set_context(downloader, self->factory->context);
            (void) ma_repository_downloader_use_relay(downloader, MA_FALSE); /* will set to TRUE when you add this support in repository downloader */

			if(self->sahu_hdr_value)
				ma_repository_downloader_add_header(downloader, MA_HTTP_SAHU_HEADER_STR, self->sahu_hdr_value);

            rc = ma_downloader_start(downloader, self->file, stream, on_download_progress, self);
        }
    }        

    if(MA_OK != rc) {
        MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to start file <%s> download, rc <%d>", self->file, rc);
        if(stream) (void)ma_stream_release(stream);
        if(downloader) (void)ma_downloader_release(downloader);
        self->cache_state = MA_URL_CACHE_DOWNLOAD_FAILED;        
    }

    return rc;
}

static void ma_url_cache_state_manager(ma_url_cache_t *self) {
    ma_error_t rc = MA_OK;
    MA_LOG(self->logger, MA_LOG_SEV_TRACE, "Current state of file <%s> cache - <%d> ",self->file,  self->cache_state);
    switch(self->cache_state) {
    case MA_URL_CACHE_INITIALIZED:    
        if(is_request_for_replica_log(self->file) || is_request_for_sitestat_xml(self->file))
            self->cache_state = MA_URL_CACHE_DOWNLOAD_START;
        else
            self->cache_state = MA_URL_CACHE_VALIDATION_START;
        uv_async_send(&self->async);
        break;
    case MA_URL_CACHE_VALIDATION_START:
        rc = start_validation(self);
        break;           
    case MA_URL_CACHE_DOWNLOAD_START:
        rc = start_file_downloading(self);
        break;
    case MA_URL_CACHE_DOWNLOADING:
    case MA_URL_CACHE_READY:
        {   
            if(self->fd > 0) {
                ma_queue_t *qitem; 
                ma_queue_foreach(qitem, &self->qhead) {
                    ma_cache_reader_t *reader = ma_queue_data(qitem, ma_cache_reader_t, qlink);
                    if(reader) {
                        uv_async_send(&reader->reader_async);
                    }
                } 
            }
            break;
        }
    case MA_URL_CACHE_VALIDATION_FAILED:        
    case MA_URL_CACHE_DOWNLOAD_FAILED:
    case MA_URL_CACHE_DEINITIALIZED:
    case MA_URL_CACHE_ERROR:
    case MA_URL_CACHE_UNKNOWN:
	case MA_URL_CACHE_ABORTED:
        rc = MA_ERROR_HTTP_SERVER_INTERNAL;
        break;
    }

    if(MA_OK != rc) {
        ma_http_server_t *server = (ma_http_server_t *)self->factory->base.data;
        ma_queue_t *qitem; 
        ma_queue_foreach(qitem, &self->qhead) {
            ma_cache_reader_t *reader = ma_queue_data(qitem, ma_cache_reader_t, qlink);
            if(reader) {
                reader->cb((ma_url_cache_t *)self, self->cache_state, self->file_size, reader, reader->cb_data);                                   
            }        
        }
        /* Removing cache itself from cache manager when cache is in bad state */ 
        ma_url_cache_manager_remove(server->cache_manager, self->file, self);
    }
}

size_t ma_url_cache_get_data(void *cookie, unsigned char *buffer, size_t max_size) {
    ma_cache_reader_t *reader = (ma_cache_reader_t *)cookie;
    if(reader) {
        ma_url_cache_t *self = reader->url_cache;
        uv_fs_t fs_req;
        size_t read_bytes = 0;
        if(-1 != uv_fs_read(self->uv_loop, &fs_req, self->fd, buffer, max_size, reader->file_cur_pos, NULL)) {
            reader->file_cur_pos += fs_req.result;
            read_bytes = fs_req.result;
            uv_fs_req_cleanup(&fs_req);
            return read_bytes;
        }        
    }
    return -1;
}

static void reader_async_cb(uv_async_t *handle, int status) {
    ma_cache_reader_t *reader = (ma_cache_reader_t *)handle->data;
    ma_url_cache_t *self = reader->url_cache; 
    ma_error_t rc = MA_OK;
    if(MA_OK != (rc = reader->cb(self, self->cache_state, self->file_size, reader, reader->cb_data))) {       
        MA_LOG(self->logger, MA_LOG_SEV_ERROR, "File <%s> url cache reader callback failed, rc <%d>", self->file, rc);        
    }
}

static void cache_async_cb(uv_async_t *handle, int status) {
    ma_url_cache_t *self = (ma_url_cache_t *)handle->data;
    ma_url_cache_state_manager(self);
}

ma_bool_t create_directory(ma_url_cache_t *self) {
    ma_bool_t b_rc = MA_FALSE;
    char *p = NULL;
    if(p = strrchr(self->file, MA_PATH_SEPARATOR)) {
        *p = 0;
        if(strlen(self->file) > 1) {
            char path[MA_HTTP_MAX_URL_PATH_LEN] = {0};
            ma_http_server_t *server = (ma_http_server_t *)self->factory->base.data;
            ma_fs_make_recursive_dir(self->uv_loop, server->policies.virtual_dir, self->file);            
            MA_MSC_SELECT(_snprintf,snprintf)(path, MA_HTTP_MAX_URL_PATH_LEN, "%s%c%s", server->policies.virtual_dir, MA_PATH_SEPARATOR, self->file); 
            if(ma_fs_is_file_exists(self->uv_loop, path)) { 
                b_rc = MA_TRUE;
            }
        }
        else
            b_rc = MA_TRUE;
        *p = MA_PATH_SEPARATOR;        
    }
    return b_rc;
}

/* TBD - create stream on temp path, and move this to original path when cache obj ref count goes to zero */
ma_error_t ma_url_cache_read_start(ma_url_cache_t *self, ma_bool_t honor_lazy_cache, ma_on_cache_ready_cb_t cb, void *cb_data) {
    if(self && cb) { 
        /* If cache object in any failure state, we can reject read start immediately */
        MA_LOG(self->logger, MA_LOG_SEV_TRACE, "Cache read starts for file <%s>, current state of cache <%d>", self->file, self->cache_state);
        if(MA_URL_CACHE_INITIALIZED == self->cache_state ||
           MA_URL_CACHE_VALIDATION_START == self->cache_state ||
           MA_URL_CACHE_DOWNLOAD_START == self->cache_state ||
           MA_URL_CACHE_DOWNLOADING == self->cache_state ||
           MA_URL_CACHE_READY == self->cache_state) {
            ma_cache_reader_t *reader = NULL;
            self->honor_lazycache = honor_lazy_cache;

            /* creating directory structure, expecting atleast one directory */
            if(MA_URL_CACHE_INITIALIZED == self->cache_state) {
                if(!create_directory(self))
                    return MA_ERROR_HTTP_SERVER_INTERNAL;
            }

            reader = (ma_cache_reader_t *)calloc(1, sizeof(ma_cache_reader_t));
            if(!reader) 
                return MA_ERROR_OUTOFMEMORY;

            /* If readers list is empty, then open the file descriptor and get size if file exists on disk */
            if(ma_queue_empty(&self->qhead)) {
                if(-1 == self->fd) {                   
                    char local_file_path[MA_HTTP_MAX_URL_PATH_LEN] = {0};
                    ma_http_server_t *server = (ma_http_server_t *)self->factory->base.data;
                    if(get_local_file_path_for_file(self->file, server->policies.virtual_dir, local_file_path, MA_HTTP_MAX_URL_PATH_LEN)) {  
                        uv_fs_t fs_req;  
                        self->file_size = ma_fs_get_file_size(self->uv_loop, local_file_path);                        
                        if((-1 == self->file_size) || (0 == self->file_size)) {
							if(self->honor_lazycache) {
								if(MA_URL_CACHE_READY == self->cache_state) {
									/* File doesn't exist but file is in cache */                            
									self->cache_state = MA_URL_CACHE_INITIALIZED;
									self->write_file_offset = 0;
									create_directory(self); /* TBD - error check */ 
								}
                            
								if(MA_URL_CACHE_INITIALIZED == self->cache_state) {
									/* If there is no file, start download in temp download folder first */
									char temp_file_name[MA_MAX_LEN] = {0};
									char *file_name = strrchr(self->file, MA_PATH_SEPARATOR);
									if(file_name) {
										char download_folder[MA_HTTP_MAX_URL_PATH_LEN] = {0};
										MA_MSC_SELECT(_snprintf,snprintf)(download_folder, MA_HTTP_MAX_URL_PATH_LEN - 1, "%s%c%s", server->policies.virtual_dir, MA_PATH_SEPARATOR, MA_HTTP_SA_REPOSITORY_TEMP_DIR);

										if(!ma_fs_is_file_exists(self->uv_loop, download_folder))
											ma_fs_make_recursive_dir(self->uv_loop, server->policies.virtual_dir, MA_HTTP_SA_REPOSITORY_TEMP_DIR);

										MA_MSC_SELECT(_snprintf,snprintf)(temp_file_name, MA_MAX_LEN, "%s_%lu_%08X", file_name, ma_sys_rand(), self);
										memset(local_file_path, 0, MA_HTTP_MAX_URL_PATH_LEN);
										MA_MSC_SELECT(_snprintf,snprintf)(local_file_path, MA_HTTP_MAX_URL_PATH_LEN - 1, "%s%c%s", download_folder, MA_PATH_SEPARATOR, temp_file_name);
										self->is_file_downloading_in_temp_file = MA_TRUE;
										strcpy(self->temp_download_file, local_file_path);
									}
									else {
										free(reader);
										return MA_ERROR_HTTP_SERVER_INTERNAL;
									}
								}
							}
							else {
								free(reader) ;
								return MA_ERROR_HTTP_SERVER_INTERNAL;
							}
                        }						
                        self->fd = uv_fs_open(self->uv_loop, &fs_req, local_file_path, O_RDWR | O_CREAT, 00700, NULL);   /* TBD - check again open mode and options */                        

						if(-1 == self->fd) {
							MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to open file <%s>", local_file_path);
							free(reader);
							uv_fs_req_cleanup(&fs_req);
							return MA_ERROR_HTTP_SERVER_INTERNAL;
						}
						uv_fs_req_cleanup(&fs_req);
                    }  
					else {
						free(reader) ;
						return MA_ERROR_HTTP_SERVER_INTERNAL;
					}
                }
            }

			{
				ma_int32_t state = 0 ; 
				ma_db_t *db = ma_configurator_get_msgbus_database(MA_CONTEXT_GET_CONFIGURATOR(self->factory->context));
				if(self->is_file_downloading_in_temp_file == MA_FALSE && MA_OK == ma_p2p_db_get_content_state(db, self->file_hash, &state)) {
					if(state >= 0) {
						(void)ma_p2p_db_set_content_state_serving(db, self->file_hash) ;
						reader->is_served_p2p_content = MA_TRUE ;
					}
					else {
						free(reader) ;
						return MA_ERROR_P2P_CONTENT_MARKED_FOR_PURGE ;
					}
				}
			}
            reader->cb = cb;
            reader->cb_data = cb_data;           
            ma_queue_insert_tail(&self->qhead, &reader->qlink); 
            reader->url_cache = self;
            reader->reader_async.data = reader;
            uv_async_init(self->uv_loop, &reader->reader_async, reader_async_cb);
			MA_LOG(self->logger, MA_LOG_SEV_TRACE, "cache object async data <%p>", self);
			MA_LOG(self->logger, MA_LOG_SEV_TRACE, "cache reader async data <%p>", reader);

            if(MA_URL_CACHE_INITIALIZED == self->cache_state ||           
               MA_URL_CACHE_READY == self->cache_state) {
                uv_async_send(&self->async);
            }

            return MA_OK;             
        }
        return MA_ERROR_HTTP_SERVER_INTERNAL;
    }
    return MA_ERROR_INVALIDARG;
}

static void reader_async_close_cb(uv_handle_t *handle) {
    ma_cache_reader_t *reader = (ma_cache_reader_t *)handle->data;
    free(reader);
}

ma_error_t ma_url_cache_read_stop(ma_url_cache_t *self, void *cookie) {
    ma_error_t rc = MA_ERROR_OBJECTNOTFOUND;
    if(self && cookie) {
        ma_queue_t *qitem;
        ma_cache_reader_t *reader = (ma_cache_reader_t *)cookie;
		ma_db_t *db = ma_configurator_get_msgbus_database(MA_CONTEXT_GET_CONFIGURATOR(self->factory->context));

        MA_LOG(self->logger, MA_LOG_SEV_TRACE, "Cache read stops for file <%s>, current state of cache <%d>", self->file, self->cache_state);       

        ma_queue_foreach(qitem, &self->qhead) {
            ma_cache_reader_t *entry = ma_queue_data(qitem, ma_cache_reader_t, qlink);
            if(reader == entry) {
                ngx_queue_remove(qitem); 
                uv_close((uv_handle_t *)&reader->reader_async, reader_async_close_cb);
                rc = MA_OK;
                break;
            }
        } 

		if(reader->is_served_p2p_content) {
			(void)ma_p2p_db_unset_content_state_serving(db, self->file_hash) ;
		}

        /* If list is empty, we can close the fd */
        if(ma_queue_empty(&self->qhead)) {
            uv_fs_t fs_req;       
            MA_LOG(self->logger, MA_LOG_SEV_TRACE, "Closing file descriptor of file <%s>, There are no callback registrations for read", self->file);
            if((-1 != self->fd) && !uv_fs_close(self->uv_loop, &fs_req, self->fd, NULL)) {
                uv_fs_req_cleanup(&fs_req);               
                if(self->is_file_downloading_in_temp_file) {
                    if(MA_URL_CACHE_READY == self->cache_state) {
                        char local_file_path[MA_HTTP_MAX_URL_PATH_LEN] = {0};
                        ma_http_server_t *server = (ma_http_server_t *)self->factory->base.data;
                        if(get_local_file_path_for_file(self->file, server->policies.virtual_dir, local_file_path, MA_HTTP_MAX_URL_PATH_LEN)) {
                            if(ma_fs_is_file_exists(self->uv_loop, local_file_path)) {
                                uv_fs_unlink(self->uv_loop, &fs_req, local_file_path, NULL);
                                uv_fs_req_cleanup(&fs_req);
                            }
                            if(!ma_fs_move_file(self->uv_loop, self->temp_download_file, local_file_path))
                                MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to rename temp download file <%s> to <%s>", self->temp_download_file, local_file_path);
							else {
									/* add the file as reference */
								if(!(is_request_for_replica_log(self->file) || is_request_for_sitestat_xml(self->file))) {
									ma_p2p_content_t *p2p_content = NULL ;
									if(!self->file_hash) {
										uv_buf_t hash = uv_buf_init(0,0), hash256 = uv_buf_init(0,0);                    
                						if(get_file_hash_from_replica_log(self, &hash, &hash256)) {
											self->file_hash = strdup(hash.base? hash.base: hash256.base) ;
											if(hash.base) free(hash.base); 
											if(hash256.base) free(hash256.base);
										}
									}
									if(self->file_hash && (MA_OK == ma_p2p_content_create(&p2p_content))) {
										char time_created[21] ;
										ma_time_t cur_time = {0, 0, 0, 0, 0, 0, 0 } ;
										char *urn = NULL ;

										ma_time_get_localtime(&cur_time) ;
										MA_MSC_SELECT(_snprintf, snprintf)(time_created, 21, "%04d:%02d:%02d::%02d:%02d:%02d", cur_time.year, cur_time.month, cur_time.day, cur_time.hour, cur_time.minute, cur_time.second) ;

										urn = local_file_path ;
										urn = urn + strlen(server->policies.virtual_dir) ;

										(void)ma_p2p_content_set_base_dir(p2p_content, server->policies.virtual_dir) ;
										(void)ma_p2p_content_set_content_format(p2p_content, MA_P2P_CONTENT_FORMAT_FILE) ;
										(void)ma_p2p_content_set_content_size(p2p_content, ma_fs_get_file_size(self->uv_loop, local_file_path)) ;
										(void)ma_p2p_content_set_content_type(p2p_content, "Default") ;
										(void)ma_p2p_content_set_contributor(p2p_content, "LAZY_CACHE") ;
										(void)ma_p2p_content_set_hash(p2p_content, self->file_hash) ;
										(void)ma_p2p_content_set_hit_count(p2p_content, 0) ;
										(void)ma_p2p_content_set_owned(p2p_content, MA_FALSE) ;
										(void)ma_p2p_content_set_time_created(p2p_content, time_created) ;
										(void)ma_p2p_content_set_time_last_access(p2p_content, time_created) ;
										(void)ma_p2p_content_set_urn(p2p_content, urn) ;

										if(MA_OK != (rc = ma_p2p_db_add_content(db, p2p_content))) {
											MA_LOG(self->logger, MA_LOG_SEV_WARNING, "adding conetnt reference failed (rc = %d).", rc) ;
										}

										(void)ma_p2p_content_release(p2p_content);
									}
								}
							}
                            self->is_file_downloading_in_temp_file = MA_FALSE;
                        }
                    }
                    else {                        
                        uv_fs_unlink(self->uv_loop, &fs_req, self->temp_download_file, NULL);
                        uv_fs_req_cleanup(&fs_req);                        
                    }
                }               
            }
            else
                rc = MA_ERROR_HTTP_SERVER_INTERNAL;
            self->fd = -1;
        }

        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

static void async_close_cb(uv_handle_t *handle) {
    ma_url_cache_t *self = (ma_url_cache_t *)handle->data;
    while (!ma_queue_empty(&self->qhead)) {
        ma_queue_t *qitem = ma_queue_last(&self->qhead);
        ma_cache_reader_t *reader = ma_queue_data(qitem, ma_cache_reader_t, qlink);
        ngx_queue_remove(qitem);
        uv_close((uv_handle_t *)&reader->reader_async, reader_async_close_cb);
    }
    MA_LOG(self->logger, MA_LOG_SEV_TRACE, "Destroyed file <%s> cache", self->file);

    if(self->file) free(self->file);
	
	if(self->file_hash)	free(self->file_hash) ;
	self->file_hash = NULL ;
    
	free(self);
}

ma_error_t ma_url_cache_abort(ma_url_cache_t *self) {
	if(self) {
		ma_http_server_t *server = (ma_http_server_t *)self->factory->base.data;
		ma_queue_t *qitem; 

		self->cache_state = MA_URL_CACHE_ABORTED ;
		ma_queue_foreach(qitem, &self->qhead) {
			ma_cache_reader_t *reader = ma_queue_data(qitem, ma_cache_reader_t, qlink);
			if(reader) {
				reader->cb((ma_url_cache_t *)self, self->cache_state, self->file_size, reader, reader->cb_data);                                   
			}        
		}
		return MA_OK ;
	}
	return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_url_cache_release(ma_url_cache_t *self){
    if (self && (0 < self->ref_count)) {
        MA_LOG(self->logger, MA_LOG_SEV_TRACE, "Releasing file <%s> cache, current ref count <%d>", self->file, self->ref_count);
        if (0 == MA_ATOMIC_DECREMENT(self->ref_count)) {
            MA_LOG(self->logger, MA_LOG_SEV_TRACE, "Destroying file <%s> cache", self->file);
			
			if(self->sahu_hdr_value) free(self->sahu_hdr_value);

			if(self->valid_repository) 
				ma_repository_release(self->valid_repository);
            self->cache_state = MA_URL_CACHE_DEINITIALIZED;
            uv_close((uv_handle_t *)&self->async, async_close_cb);            
        }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

#define MA_HTTP_REPLICA_GENERAL_SECTION     "General"
#define MA_HTTP_REPLICA_NO_OF_ITEMS_KEY     "NumItems"
#define MA_HTTP_REPLICA_FILE_NAME_KEY       "Name"
#define MA_HTTP_REPLICA_FILE_HASH_KEY       "Hash"
#define MA_HTTP_REPLICA_FILE_HASH256_KEY    "Hash256"
#define MA_HTTP_REPLICA_FILE_TYPE_STR       "Type"

static ma_bool_t get_file_hash_from_replica_log(ma_url_cache_t *self, uv_buf_t *hash_buf, uv_buf_t *hash_buf256) {
    ma_ds_t *ini_ds = NULL;
    ma_bool_t b_rc = MA_FALSE;
   
    const char *file_name = strrchr(self->file, MA_PATH_SEPARATOR);
    if(file_name) {
        char replica_log[MA_HTTP_MAX_URL_PATH_LEN] = {0};    
        if(get_replica_log_from_file(self->file, replica_log, MA_HTTP_MAX_URL_PATH_LEN)) {
            char replica_log_local_path[MA_HTTP_MAX_URL_PATH_LEN] = {0};
            ma_http_server_t *server = (ma_http_server_t *)self->factory->base.data;
            if(get_local_file_path_for_file(replica_log, server->policies.virtual_dir, replica_log_local_path, MA_HTTP_MAX_URL_PATH_LEN)) {
                ma_error_t rc = MA_OK;
                if(MA_OK == (rc = ma_ds_ini_open(replica_log_local_path, 0, (ma_ds_ini_t **)&ini_ds))) {
                    ma_variant_t *value = NULL;                    
                    if(MA_OK == (rc = ma_ds_get(ini_ds, MA_HTTP_REPLICA_GENERAL_SECTION, MA_HTTP_REPLICA_NO_OF_ITEMS_KEY, MA_VARTYPE_UINT32, &value))) {
                        ma_int32_t no_of_items = 0 ;
                        ma_int32_t i = 0;
                        (void) ma_variant_get_int32(value, &no_of_items);
                        (void) ma_variant_release(value); value = NULL;

                        for(i = 1; i <= no_of_items; i++) { 
                            char section[MA_MAX_LEN] = {0};
                            ma_buffer_t *buffer = NULL;
                            MA_MSC_SELECT(_snprintf,snprintf)(section, MA_MAX_LEN, "ITEM_%d", i);
                            /* TBD - will check file Type alos */
                            if(MA_OK == ma_ds_get_str(ini_ds, section, MA_HTTP_REPLICA_FILE_NAME_KEY, &buffer)) {
                                const char *file = NULL;
                                size_t size = 0;
                                (void)ma_buffer_get_string(buffer, &file, &size);
                                /* If file name matches in any section, we can read HASH and stop the searching */
                                if(file && !strcmp(file, file_name+1)) {
                                    ma_buffer_t *buff = NULL;
                                    if(MA_OK == ma_ds_get_str(ini_ds, section, MA_HTTP_REPLICA_FILE_HASH_KEY, &buff)) {
                                        const char *str = NULL;
                                        size_t len = 0;
                                        (void)ma_buffer_get_string(buff, &str, &len);
                                        if(str) {
                                            hash_buf->base = strdup(str);
                                            hash_buf->len = len;
                                        }
                                        (void)ma_buffer_release(buff); buff = NULL;
                                    }

                                    if(MA_OK == ma_ds_get_str(ini_ds, section, MA_HTTP_REPLICA_FILE_HASH256_KEY, &buff)) {
                                        const char *str = NULL;
                                        size_t len = 0;
                                        (void)ma_buffer_get_string(buff, &str, &len);
                                        if(str) {
                                            hash_buf256->base = strdup(str);
                                            hash_buf256->len = len;
                                        }
                                        (void)ma_buffer_release(buff); buff = NULL;
                                    }

                                    b_rc = hash_buf->base || hash_buf256->base;                                
                                }
                                (void)ma_buffer_release(buffer); buffer = NULL;                        
                            }
                            if(b_rc) break;
                        }
                    }
                    (void)ma_ds_release(ini_ds);
                }
                else
                    MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to open replica log <%s> to read HASH, rc <%d>", replica_log_local_path, rc);
            }            
        }
    }

    if(!b_rc)
        MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to find file <%s> HASH from file replica log", self->file);
    
    return b_rc;
}

ma_error_t ma_url_cache_set_sahu_header(ma_url_cache_t *self, const char *sahu_hdr_value) {
	if(self && sahu_hdr_value) {
		self->sahu_hdr_value = strdup(sahu_hdr_value);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}