#include "ma_http_request_handler_factories.h"
#include <string.h>
#include <stdlib.h>

MA_MAP_DEFINE(ma_dns_resolver_map, char *, strdup, free, char *, strdup, free, strcmp);

void ma_http_request_handler_factories_init(ma_http_request_handler_factories_t *self) {
    if (self) {
        ma_queue_init(&self->qhead);
		MA_MAP_CREATE(ma_dns_resolver_map, self->dns_map);
    }
}

void ma_http_request_handler_factories_push_back(ma_http_request_handler_factories_t *self, ma_http_request_handler_factory_t *factory) {
    if (self && factory) {
        ma_queue_insert_tail(&self->qhead, &factory->qlink);
    }
}

void ma_http_request_handler_factories_uninit(ma_http_request_handler_factories_t *self) {
    if (self) {
        while (!ma_queue_empty(&self->qhead)) {
            ma_queue_t *qitem = ma_queue_last(&self->qhead);
            ma_http_request_handler_factory_t *factory = ma_queue_data(qitem, ma_http_request_handler_factory_t, qlink);
            ma_queue_remove(qitem);
            ma_http_request_handler_factory_release(factory);
        }

		MA_MAP_CLEAR(ma_dns_resolver_map, self->dns_map);
		MA_MAP_RELEASE(ma_dns_resolver_map, self->dns_map);
    }
}

void ma_http_request_handler_factories_start(ma_http_request_handler_factories_t *self) {
	if(self) {
		ma_queue_t *qitem ;
		ma_queue_foreach(qitem, &self->qhead) {
			ma_http_request_handler_factory_t *factory = ma_queue_data(qitem, ma_http_request_handler_factory_t, qlink);
			(void) ma_http_request_handler_factory_start(factory);
		}
	}
}

void ma_http_request_handler_factories_stop(ma_http_request_handler_factories_t *self) {
	if(self) {
		ma_queue_t *qitem ;
		ma_queue_foreach(qitem, &self->qhead) {
			ma_http_request_handler_factory_t *factory = ma_queue_data(qitem, ma_http_request_handler_factory_t, qlink);
			(void) ma_http_request_handler_factory_stop(factory);
		}
	}
}