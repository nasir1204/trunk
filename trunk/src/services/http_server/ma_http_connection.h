#ifndef MA_HTTP_CONNECTION_H_INCLUDED
#define MA_HTTP_CONNECTION_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/datastructures/ma_queue.h"

#include "http_parser.h"
#include "uv.h"

MA_CPP(extern "C" {)


struct ma_http_server_s;
struct ma_http_request_handler_s;

typedef struct ma_http_connection_s ma_http_connection_t, *ma_http_connection_h;

typedef enum ma_http_connection_state_e {
    MA_HTTP_CONNECTION_STATE_STOPPED = 0,
    MA_HTTP_CONNECTION_STATE_STARTED,
    MA_HTTP_CONNECTION_STATE_STOPPING
} ma_http_connection_state_t;


typedef enum ma_http_connection_type_e {
    MA_HTTP_CONNECTION_TYPE_SPIPE = 0,
	MA_HTTP_CONNECTION_TYPE_PROXY,
	MA_HTTP_CONNECTION_TYPE_SSL_PROXY,
    MA_HTTP_CONNECTION_TYPE_SAREPO,
    MA_HTTP_CONNECTION_TYPE_P2P
} ma_http_connection_type_t ;


/* stock response variants */
typedef enum ma_http_connection_response_status_e {
    MA_HTTP_REPLY_200_OK,
    MA_HTTP_REPLY_403_FORBIDDEN,
    MA_HTTP_REPLY_404_NOT_FOUND,
} ma_http_connection_response_status_t;

typedef struct ma_http_header_s {
    ma_queue_t      qlink; /**< so they can form a linked list */
    uv_buf_t        field, value;
} ma_http_header_t;

/** data structure to store http request information */
typedef struct ma_http_request_s {

    /**
     * 
     * Please keep as first member of this structure or change the parser callbacks
     */ 
    http_parser     parser;

    /**
     * request url, null terminated 
     */
    uv_buf_t        url;

    ma_queue_t      headers;

} ma_http_request_t;

#define MA_HTTP_CONNECTION_EXTRA_SEND_BUFFER_SIZE 256

/* a simple scratch buffer to store temporary response data */
typedef struct ma_http_buffer_s {
    char                        buffer[MA_HTTP_CONNECTION_EXTRA_SEND_BUFFER_SIZE];

    char                        *next;
} ma_http_buffer_t;

MA_STATIC_INLINE void ma_http_buffer_init(ma_http_buffer_t *buffer) {
    buffer->next = buffer->buffer;
}

MA_STATIC_INLINE void *ma_http_buffer_peek(ma_http_buffer_t *buffer) {
    return buffer->next;
}

MA_STATIC_INLINE void *ma_http_buffer_advance(ma_http_buffer_t *buffer, size_t offset) {
    void *result = buffer->next;
    buffer->next += offset;
    return result;
}

MA_STATIC_INLINE size_t ma_http_buffer_bytes_available(ma_http_buffer_t *buffer) {
    return sizeof(buffer->buffer) + buffer->buffer - buffer->next;
}



void ma_http_request_init(ma_http_request_t *request);

ma_bool_t ma_http_request_search_header(ma_http_request_t *request, const char *field, uv_buf_t *value);

void ma_http_request_clear(ma_http_request_t *request);


struct ma_http_connection_s {
    ma_queue_t                  qlink; /**< forms a linked list - used by connection_manager */

    uv_tcp_t                    c_socket; /**< tcp socket to the connected client */

    uv_timer_t                  timer; /**< to detect stale, idle connections and abort them */

    http_parser_settings        parser_callbacks; /* mutable copy allows handler objects to hijack one or more callbacks until next request comes in */

    ma_http_connection_state_t  state;

    struct ma_http_server_s     *server; /**< back pointer to server */

    ma_uint32_t                 readtimeout_ms; /**< cached value of read-timeout */

    ma_http_request_t           request;

    uv_write_t                  c_write_req; /**< for write requests back to client */

    uv_buf_t                    *cur_hdr_field, *cur_hdr_value; /* housekeeping during header parsing */

    uv_buf_t                    cur_total_headers; /* Total header data */

    ma_http_buffer_t            send_data; /* extra data maintained during write operations (instead of expensive heap memory) */

    unsigned                    close_after_write:1; /* set to 1 to force connection to close when response has been sent */
    unsigned                    ref_count:2; /* only need to count to 2 for now... */

    struct ma_http_request_handler_s *cur_req_handler; /* current request handler if any */

	unsigned					is_cur_req_handler_busy:1;
    
    char                        peer_addr[INET6_ADDRSTRLEN];

	ma_http_connection_type_t	connection_type ;
    
	struct sockaddr_storage		sa;
};

ma_error_t ma_http_connection_create(struct ma_http_server_s *server, ma_http_connection_t **connection);

void ma_http_connection_add_ref(ma_http_connection_t *connection);

void ma_http_connection_release(ma_http_connection_t *connection);

ma_error_t ma_http_connection_accept(ma_http_connection_t *connection, uv_stream_t *server);

ma_error_t ma_http_connection_start(ma_http_connection_t *connection);

ma_error_t ma_http_connection_stop(ma_http_connection_t *connection);

/* invoked by request handlers to tell the connection that it should prepare for another request */
void ma_http_connection_notify_request_complete(ma_http_connection_t *connection);

void ma_http_connection_idletimer_restart(ma_http_connection_t *self);

void ma_http_connection_send_stock_response(ma_http_connection_t *connection, char const *status_text);

MA_CPP(})

#endif /* MA_HTTP_CONNECTION_H_INCLUDED */
