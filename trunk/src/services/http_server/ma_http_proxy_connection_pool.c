#include "ma_http_proxy_handler_internal.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/threading/ma_atomic.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define MA_HTTP_PROXY_MAX_CONNECTIONS 1000

void ma_http_proxy_connection_pool_init(ma_http_proxy_connection_pool_t *pool, struct uv_loop_s *uv_loop) {
    ma_queue_init(&pool->qhead);
    pool->uv_loop = uv_loop;
    pool->size = 0;
}

void ma_http_proxy_connection_pool_remove(ma_http_proxy_connection_pool_t *pool, ma_http_proxy_upstream_connection_t *connection) {
    if(pool && connection) {
        ma_queue_t  *qitem;        
        ma_queue_foreach(qitem, &pool->qhead)  {
            ma_http_proxy_upstream_connection_t *qconnection = ma_queue_data(qitem, ma_http_proxy_upstream_connection_t, qlink);            
            if (qconnection == connection) {
                ngx_queue_remove(qitem);
                ma_http_proxy_upstream_connection_stop(connection);    
                MA_ATOMIC_DECREMENT(pool->size);
                break;
            }
        }
    }
}

void ma_http_proxy_connection_pool_clear(ma_http_proxy_connection_pool_t *pool) {
    while (!ma_queue_empty(&pool->qhead)) {
        ma_queue_t *qitem = ma_queue_head(&pool->qhead);
        ma_http_proxy_upstream_connection_t *connection = ma_queue_data(qitem, ma_http_proxy_upstream_connection_t, qlink);        
        ma_queue_remove(qitem);
        ma_http_proxy_upstream_connection_stop(connection);
        MA_ATOMIC_DECREMENT(pool->size);
    }    
}

ma_error_t ma_http_proxy_connection_pool_request_init(ma_http_proxy_connection_pool_request_t *self, ma_http_proxy_connection_pool_t *pool) {
    memset(self,0,sizeof(ma_http_proxy_connection_pool_request_t));
    
    self->pool = pool;
    self->cb = 0;

    return MA_OK;
}

static void pool_req_try_connect(ma_http_proxy_connection_pool_request_t *self);

static void pool_req_connect_cb(uv_connect_t *req, int status) {
    ma_http_proxy_connection_pool_request_t *self = ma_queue_data(req, ma_http_proxy_connection_pool_request_t, connect_req);

    if (status) {
        self->addr_info_iter = self->addr_info_iter->ai_next;
        /* try next ip address if any*/
        pool_req_try_connect(self);
    } else {
        ma_http_proxy_upstream_connection_t *connection;
        if (MA_OK == ma_http_proxy_upstream_connection_create(self->pool, &connection)) {
            connection->ps_socket = self->ps_socket;
            self->connection = connection;
            self->ps_socket = 0;
            connection->server_info = self->server_info;
            MA_ATOMIC_INCREMENT(self->pool->size);
            ma_queue_insert_head(&self->pool->qhead, &connection->qlink);
            MA_BYTE_SET_BIT(connection->flags, MA_HTTP_PROXY_UPSTREAM_CONNECTION_BUSY);
            self->cb(self, 0, connection);
        }


        /* TADA, connected. Tell client we are good to go */
    }
}


static void pool_req_give_up(ma_http_proxy_connection_pool_request_t *self) {
    /* Notify  */
    self->cb(self,-1, 0);
}


static void pool_req_try_connect(ma_http_proxy_connection_pool_request_t *self) {
    if (0 == self->addr_info_iter) {
        /* exhausted the list of resolved addresses */ 
        pool_req_give_up(self);
        return;
    }


    /* Note, we may come here multiple times as a result of a previous failed connection attempt when multiple addresses are returned from DNS */
    /* May have to close the socket first  */

    if (!self->ps_socket) {
        if (NULL == (self->ps_socket = calloc(1, sizeof(uv_tcp_t)))) {
            pool_req_give_up(self);
            return;
        }

        uv_tcp_init(self->pool->uv_loop,self->ps_socket);
    }

    if (AF_INET6 == self->addr_info_iter->ai_family) {
        uv_tcp_connect6(&self->connect_req, self->ps_socket, *(struct sockaddr_in6 *) self->addr_info_iter->ai_addr, &pool_req_connect_cb);
    } else if (AF_INET == self->addr_info_iter->ai_family) {
        uv_tcp_connect(&self->connect_req, self->ps_socket, *(struct sockaddr_in *) self->addr_info_iter->ai_addr, &pool_req_connect_cb);
    } else {
        /* try next address */
        self->addr_info_iter = self->addr_info_iter->ai_next;
        pool_req_try_connect(self); /* NOTE, this is a recursive call, consider unfolding it... */
    }
}


static void pool_req_getaddrinfo_cb(uv_getaddrinfo_t *req, int status, struct addrinfo *res) {
    ma_http_proxy_connection_pool_request_t *self = ma_queue_data(req, ma_http_proxy_connection_pool_request_t, addr_info_req);
    if (status) {
        /* some error */
        pool_req_give_up(self);
    } else {
        /* start enumerating the list of returned ip addresses until one of them works */
        assert(0 == self->addr_info_begin);
        self->addr_info_iter = self->addr_info_begin = res;
        pool_req_try_connect(self); /* */
    }
}

static void async_timer_cb(uv_timer_t *timer, int status) {
    ma_http_proxy_connection_pool_request_t *self = timer->data;
    
    if(!status)
        self->connection ? self->cb(self, 0, self->connection) : self->cb(self, -1, 0);    
    else {
        MA_BYTE_CLEAR_BIT(self->connection->flags, MA_HTTP_PROXY_UPSTREAM_CONNECTION_BUSY); /* This is just for fun :) */
        assert(!"Async timer failed");
    }

    self->is_timer_started = MA_FALSE;
    uv_close((uv_handle_t *)&self->timer, NULL);    
}

static ma_error_t notify_async_way(ma_http_proxy_connection_pool_request_t *self, ma_http_proxy_upstream_connection_t *connection) {
    uv_timer_init(self->pool->uv_loop, &self->timer);
    self->connection = connection;
    self->timer.data = self;
    uv_timer_start(&self->timer, async_timer_cb, 0, 0);
    self->is_timer_started = MA_TRUE;
    return MA_OK;
}

ma_error_t ma_http_proxy_connection_pool_request_start(ma_http_proxy_connection_pool_request_t *self, ma_http_server_info_t const *server_info, ma_http_proxy_connection_pool_request_cb cb) {
    char service[16];

    /* TODO, first look for any existing available connection and use that */
    ma_queue_t *qitem;
    
    self->cb = cb;
    self->server_info = *server_info;

    ma_queue_foreach(qitem, &self->pool->qhead) {
        ma_http_proxy_upstream_connection_t *connection = ma_queue_data(qitem, ma_http_proxy_upstream_connection_t, qlink);
        if(!strcmp(connection->server_info.hostname, server_info->hostname) && connection->server_info.port == server_info->port) {
            if(!MA_BYTE_CHECK_BIT(connection->flags,MA_HTTP_PROXY_UPSTREAM_CONNECTION_BUSY)) {
                MA_BYTE_SET_BIT(connection->flags, MA_HTTP_PROXY_UPSTREAM_CONNECTION_BUSY);
                return notify_async_way(self, connection);            
            }
        }
    }    

    /* TODO determine if it is ok to fire up a new connection to this server as per max connections and  max connections to this server */

    /* TODO MAX connection value must be retrivied from policy */
    if(self->pool->size < MA_HTTP_PROXY_MAX_CONNECTIONS) {
        MA_MSC_SELECT(_snprintf, snprintf)(service, MA_COUNTOF(service), "%d", server_info->port);

        if (UV_OK != uv_getaddrinfo(self->pool->uv_loop, &self->addr_info_req, &pool_req_getaddrinfo_cb, server_info->hostname, service, 0)) {
            return MA_ERROR_INVALIDARG;
        }
    }
    else {
        return notify_async_way(self, NULL);
    }
   
    return MA_OK;
}

ma_error_t ma_http_proxy_connection_pool_request_stop(ma_http_proxy_connection_pool_request_t *self) {  
    if(self->connection) {
        if(self->is_timer_started) {
            if(uv_is_active((uv_handle_t *)&self->timer))
                uv_timer_stop(&self->timer);
            uv_close((uv_handle_t *)&self->timer, NULL);
            self->is_timer_started = MA_FALSE;
        }
        if(self->addr_info_begin) 
            uv_freeaddrinfo(self->addr_info_begin);
        self->connection = NULL;
        self->cb = 0;
    }
    return MA_OK;    
}


/** ** ** CONNECTION IMPLEMENTATION ** ** ** */

ma_error_t ma_http_proxy_upstream_connection_create(ma_http_proxy_connection_pool_t *pool, ma_http_proxy_upstream_connection_t **connection) {
    if(pool && connection) {
        if (*connection = calloc(1,sizeof(ma_http_proxy_upstream_connection_t))) {
            (*connection)->pool = pool;
            return MA_OK;
        }
        return MA_ERROR_OUTOFMEMORY;
    }
    return MA_ERROR_INVALIDARG;
}

void ma_http_proxy_upstream_connection_set_owner(ma_http_proxy_upstream_connection_t *self, void *owner) {
    self->owner = owner;
}

ma_error_t ma_http_proxy_upstream_connection_stop(ma_http_proxy_upstream_connection_t *self) {
    if(self) {
        if(MA_BYTE_CHECK_BIT(self->flags,MA_HTTP_PROXY_UPSTREAM_CONNECTION_BUSY)) {
            MA_BYTE_CLEAR_BIT(self->flags,MA_HTTP_PROXY_UPSTREAM_CONNECTION_BUSY);
            self->owner = NULL;
        }
        else {
            uv_read_stop( (uv_stream_t *) self->ps_socket); /* is it ok to call if socket alrady closed  on other end ??? */
            uv_close( (uv_handle_t *) self->ps_socket, NULL);
            free(self);
        }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

