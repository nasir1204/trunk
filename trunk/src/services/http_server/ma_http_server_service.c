#include "ma/ma_datastore.h"
#include "ma/internal/utils/macrypto/ma_crypto.h"
#include "ma/internal/utils/network/ma_net_client_service.h"
#include "ma/ma_msgbus.h"
#include "ma/internal/core/msgbus/ma_msgbus_private.h"
#include "ma/internal/utils/configurator/ma_configurator.h"

#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/services/http_server/ma_http_server.h"
#include "ma/internal/services/ma_service.h"
#include "ma/internal/services/http_server/ma_ssl_proxy_handler.h"
#include "ma/internal/services/http_server/ma_http_proxy_handler.h"
#include "ma/internal/services/http_server/ma_spipe_handler.h"
#include "ma/internal/services/http_server/ma_http_log_file_handler.h"
#include "ma/internal/services/http_server/ma_http_repository_handler.h"
#include "ma/internal/services/http_server/ma_p2p_handler.h"

#include "ma/internal/services/ma_policy_settings_bag.h"
#include "ma/ma_client.h"

#include <stdlib.h>

static ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint);

static ma_error_t service_start(ma_service_t *service);

static ma_error_t service_stop(ma_service_t *service);

static void service_destroy(ma_service_t *service);

static const ma_service_methods_t methods = {
    &service_configure,
    &service_start,
    &service_stop,
    &service_destroy
};

/* generic creation api */
MA_HTTP_SERVER_API ma_error_t ma_http_server_service_create(char const *service_name, ma_service_t **service) {
    ma_http_server_t *server;
    ma_error_t err = ma_http_server_create(&server);
    if (MA_OK == err) {
        if (NULL != (*service = (ma_service_t*)calloc(1, sizeof(ma_service_t)))) {
            ma_service_init(*service, &methods, service_name, server);
            return MA_OK;
        } else {
            err = MA_ERROR_OUTOFMEMORY;
        }
        ma_http_server_release(server);
    }
    return err;
}

static ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint) {
    ma_http_server_t *server = (ma_http_server_t*)service->data;

    if(hint == MA_SERVICE_CONFIG_NEW) {
        /* for now, add the handlers unconditionally */
        ma_http_request_handler_factory_t *handler_factory;

        if (MA_OK == ma_ssl_proxy_handler_factory_create(server, &handler_factory)) {
			(void)ma_ssl_proxy_handler_factory_logger(handler_factory, MA_CONTEXT_GET_LOGGER(context)) ;
			(void)ma_ssl_proxy_handler_factory_set_msgbus_db(handler_factory, ma_configurator_get_msgbus_database(MA_CONTEXT_GET_CONFIGURATOR(context))) ;
            ma_http_server_add_request_handler_factory(server, handler_factory);
        }

        if (MA_OK == ma_http_proxy_handler_factory_create(server, &handler_factory)) {
			(void)ma_http_proxy_handler_factory_logger(handler_factory, MA_CONTEXT_GET_LOGGER(context)) ;
			(void)ma_http_proxy_handler_factory_set_msgbus_db(handler_factory, ma_configurator_get_msgbus_database(MA_CONTEXT_GET_CONFIGURATOR(context))) ;
            ma_http_proxy_handler_factory_set_event_loop(handler_factory, ma_msgbus_get_event_loop(MA_CONTEXT_GET_MSGBUS(context)));
            ma_http_server_add_request_handler_factory(server, handler_factory);
        }
        
        if (MA_OK == ma_http_repository_handler_factory_create(server, &handler_factory)) {
			(void) ma_http_repository_handler_factory_set_context(handler_factory, context);
            ma_http_server_add_request_handler_factory(server, handler_factory);
        }

        if (MA_OK == ma_spipe_handler_factory_create(server, &handler_factory)) {
            (void) ma_spipe_handler_factory_set_crypto(handler_factory, MA_CONTEXT_GET_CRYPTO(context));
			(void) ma_spipe_handler_factory_set_context(handler_factory, context) ;
            ma_http_server_add_request_handler_factory(server, handler_factory);
        }

		if(MA_OK == ma_p2p_handler_factory_create(server, &handler_factory)) {
			(void)ma_p2p_handler_factory_set_context(handler_factory, context) ;
			ma_http_server_add_request_handler_factory(server, handler_factory) ;
		}

        if (MA_OK == ma_log_file_handler_factory_create(server, &handler_factory)) {       
	         ma_http_server_add_request_handler_factory(server, handler_factory);
        }        
    }
	
    (void) ma_http_server_set_msgbus(server, MA_CONTEXT_GET_MSGBUS(context));
    (void) ma_http_server_set_datastore(server, ma_configurator_get_datastore(MA_CONTEXT_GET_CONFIGURATOR(context)));
    (void) ma_http_server_set_database(server, ma_configurator_get_database(MA_CONTEXT_GET_CONFIGURATOR(context)));
    (void)ma_http_server_set_msgbus_database(server, ma_configurator_get_msgbus_database(MA_CONTEXT_GET_CONFIGURATOR(context))) ;
    (void) ma_http_server_set_event_loop(server, ma_msgbus_get_event_loop(MA_CONTEXT_GET_MSGBUS(context)));
    (void) ma_http_server_set_logger(server, MA_CONTEXT_GET_LOGGER(context));
	(void) ma_http_server_set_context(server, context);
    return ma_http_server_config(server, MA_CONTEXT_GET_POLICY_SETTINGS_BAG(context), hint);    
}

static ma_error_t service_start(ma_service_t *service) {
    return ma_http_server_start((ma_http_server_t*)service->data);
}

static ma_error_t service_stop(ma_service_t *service) {
    return ma_http_server_stop((ma_http_server_t*)service->data);
}

static void service_destroy(ma_service_t *service) {
    ma_http_server_release((ma_http_server_t*)service->data);
    free(service);
}
