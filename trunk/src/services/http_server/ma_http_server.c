#include "ma_http_connection_manager.h"
#include "ma_http_connection.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/utils/event_loop/ma_uv_helpers.h"
#include "ma/internal/services/ma_service.h"

#include"ma/internal/utils/network/ma_net_interface.h"
#include"ma/internal/utils/platform/ma_net_interface_list_internal.h"
#include "ma/internal/services/ma_policy_settings_bag.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"

#include "ma/internal/ma_strdef.h"
#include "ma/logger/ma_logger.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/conf/ma_conf.h"
#include "ma/internal/utils/filesystem/ma_fs_utils.h"
#include "ma/internal/utils/acl/ma_acl.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_message.h"
#include "ma/internal/services/p2p/ma_p2p_db_manager.h"
#include "ma_http_server_internal.h"
#include "ma/internal/services/http_server/ma_http_server_utils.h"
#include "ma/internal/utils/xml/ma_xml.h"
#include "ma/internal/utils/platform/ma_sys_apis.h"

#include <stdlib.h>
#include <stdio.h>


#ifdef MA_WINDOWS
#else
#include <sys/socket.h>
#include <errno.h>
#endif

#ifndef IPV6_V6ONLY
#define IPV6_V6ONLY 0x27
#endif

static ma_error_t ma_http_server_enforce_policies(ma_http_server_t *self, const ma_policy_settings_bag_t *policy_bag, unsigned hint);
static void timer_close_cb(uv_handle_t *handle);

MA_HTTP_SERVER_API ma_error_t ma_http_server_create(ma_http_server_t **http_server) {
	ma_http_server_t *self = (ma_http_server_t *)calloc(1, sizeof(ma_http_server_t));
	if (self) {        
		ma_http_request_handler_factories_init(&self->handler_factories);
		ma_http_connection_manager_init(&self->connection_mgr);

		self->is_sitestat_enabled = MA_TRUE;
		self->is_server_running = MA_FALSE;

		*http_server = self;
		return MA_OK;
	}
	return MA_ERROR_OUTOFMEMORY;
}

MA_HTTP_SERVER_API ma_error_t ma_http_server_release(ma_http_server_t *self) {
	if (self) {

		ma_http_connection_manager_uninit(&self->connection_mgr);

		ma_http_request_handler_factories_uninit(&self->handler_factories);

		if(self->http_msgbus_server) (void)ma_msgbus_server_release(self->http_msgbus_server) ;

		if(self->last_used_repo_name) 
			free(self->last_used_repo_name), self->last_used_repo_name = NULL;

		if(self->policies.is_lazy_cache_enabled) 
			ma_url_cache_manager_release(self->cache_manager);

		ma_logger_release(self->logger);

		free(self);
	}
	return MA_OK;
}

ma_error_t ma_http_server_repo_cache_flush(ma_http_server_t *self) {
	if(self) {
		if(self->cache_manager)
			ma_url_cache_manager_clear(self->cache_manager);

		if(self->last_used_repo_name) 
			free(self->last_used_repo_name),  self->last_used_repo_name = NULL;

		memset(&self->catalog_version, 0, MA_MAX_LEN);

		self->is_sitestat_enabled = MA_TRUE;
	}
	return MA_OK;
}

MA_HTTP_SERVER_API ma_error_t ma_http_server_set_event_loop(ma_http_server_t *self, ma_event_loop_t *loop) {
	if (self) {
		self->uv_loop = ma_event_loop_get_uv_loop(loop);
		return MA_OK;
	}

	return MA_ERROR_INVALIDARG;
}

MA_HTTP_SERVER_API ma_error_t ma_http_server_set_datastore(ma_http_server_t *self, ma_ds_t *datastore) {
	if (self) {
		self->ds = datastore;
	}
	return MA_OK;
}

MA_HTTP_SERVER_API ma_error_t ma_http_server_set_database(ma_http_server_t *self, ma_db_t *database) {
	if (self) {
		self->db = database;
	}
	return MA_OK;
}

MA_HTTP_SERVER_API ma_error_t ma_http_server_set_msgbus_database(ma_http_server_t *self, ma_db_t *database) {
	if (self) {
		self->msgbus_db = database;
	}
	return MA_OK;
}

MA_HTTP_SERVER_API ma_error_t ma_http_server_set_logger(ma_http_server_t *self, ma_logger_t *logger) {
	if (self) {
		ma_logger_add_ref(logger);
		ma_logger_release(self->logger);
		self->logger = logger;
	}
	return MA_OK;
}

ma_error_t ma_http_server_set_msgbus(ma_http_server_t *self, ma_msgbus_t *msgbus) {
	if (self && msgbus) {
		self->msgbus = msgbus;
	}
	return MA_OK;
}

ma_error_t ma_http_server_set_context(ma_http_server_t *self, ma_context_t *context) {
	if (self && context) {
		self->context = context;
	}
	return MA_OK;
}


MA_HTTP_SERVER_API ma_error_t ma_http_server_add_request_handler_factory(ma_http_server_t *self, struct ma_http_request_handler_factory_s *factory) {
	if (self) {
		ma_http_request_handler_factories_push_back(&self->handler_factories, factory);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

static void ma_accept_cb(uv_stream_t *stream, int status) {
	ma_http_server_t *self = (ma_http_server_t *)stream->data;
	if (status) {

	} else {
		/* everything ok, create a connection and let it deal with it */
		ma_http_connection_t *connection;
		if (MA_OK == ma_http_connection_create(self, &connection)) {
			ma_http_connection_accept(connection, stream);
			ma_http_connection_manager_add(&self->connection_mgr, connection);
			ma_http_connection_start(connection);
		}
	}
}

static ma_error_t ma_http_repository_purge(ma_http_server_t *self) {
	ma_error_t rc = MA_OK ;

	if(self->policies.is_super_agent_repository_enabled) {
		ma_error_t rc = MA_OK ;
		ma_int64_t aggregated_size = 0, purge_size = 0 ;
		ma_int64_t	disk_quota = 0 ;

		ma_db_recordset_t *db_record = NULL ;

		rc = ma_p2p_db_get_aggregated_size_by_contributor(self->msgbus_db, "LAZY_CACHE", &aggregated_size) ;

		disk_quota = (self->policies.repository_disk_quota*1024LL*1024LL*1024LL)/2 ;

		purge_size = aggregated_size - disk_quota ;

		MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "aggregated_size = %lld, disk_quota = %lld, purge_size = %lld", aggregated_size, disk_quota, purge_size) ;

		if(purge_size > 0 && MA_OK == ma_p2p_db_set_content_to_purge_by_contributor(self->msgbus_db, purge_size, "LAZY_CACHE")
			&& MA_OK == (rc = ma_p2p_db_get_content_to_purge_by_contributor(self->msgbus_db, "LAZY_CACHE", &db_record)))
		{
			ma_int64_t purged_size = 0 ;
			while(MA_ERROR_NO_MORE_ITEMS != ma_db_recordset_next(db_record) && purged_size < purge_size) {
				const char *hash = NULL ;
				const char *file_path = NULL ;
				ma_int64_t content_size = 0 ;

				(void)ma_db_recordset_get_string(db_record, 1, &hash) ;
				(void)ma_db_recordset_get_string(db_record, 2, &file_path) ;
				(void)ma_db_recordset_get_long(db_record, 3, &content_size) ;

				if(ma_fs_remove_file(self->uv_loop, (char *)file_path)) {
					if(MA_OK == ma_p2p_db_remove_content_by_hash(self->msgbus_db, hash)) {
						MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Purged content(hash = %s, size = %ld)", hash, content_size) ;
						purged_size += content_size ;
					}
				}
				else {
					uv_err_t uv_err = uv_last_error(self->uv_loop) ;
					MA_LOG(self->logger, MA_LOG_SEV_WARNING, "Remove file(hash = %s, urn = %s) failed - libuv err: (\'%s\' %s)", hash, file_path, uv_strerror(uv_err),  uv_err_name(uv_err)) ;
				}
			}
			MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Total purged size %ld", purged_size) ;
			(void)ma_db_recordset_release(db_record) ;	db_record = NULL ;
			(void)ma_p2p_db_unset_content_to_purge_by_contributor(self->msgbus_db, "LAZY_CACHE") ;
		}
		else {
			MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "No content eligible/selected to purge, rc = <%d>.", rc) ;
		}
	}
	return rc ;
}

static ma_error_t ma_http_service_handler(ma_msgbus_server_t *server, ma_message_t *request, void *cb_data, ma_msgbus_client_request_t *c_request) {
	if(server && request && c_request) {
		ma_error_t rc = MA_OK ;
		const char *msg_type = NULL ;
		ma_message_t *response = NULL ;

		ma_http_server_t *self = (ma_http_server_t *)cb_data ;

		(void)ma_message_create(&response) ;

		if(MA_OK == ma_message_get_property(request, MA_HTTP_SERVER_MSG_KEY_TYPE_STR, &msg_type) && msg_type) {
			if(0 == strcmp(MA_HTTP_SERVER_MSG_PROP_VAL_RQST_RUN_PURGE_TASK_STR, msg_type)) {
				rc = ma_http_repository_purge(self) ;
				ma_message_set_property(response, MA_HTTP_SERVER_MSG_KEY_TYPE_STR, MA_HTTP_SERVER_MSG_PROP_VAL_RPLY_RUN_PURGE_TASK_STR) ;
			}	
			else if(0 == strcmp(MA_HTTP_SERVER_MSG_PROP_VAL_RQST_SUSPEND_P2P_SERVING_STR, msg_type)) {
				self->policies.is_p2p_serving_suspended = MA_TRUE ;

				MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "P2p serving suspended.") ;

				ma_http_connection_manager_abort_by_type(&self->connection_mgr, MA_HTTP_CONNECTION_TYPE_P2P) ;

				ma_message_set_property(response, MA_HTTP_SERVER_MSG_KEY_TYPE_STR, MA_HTTP_SERVER_MSG_PROP_VAL_RPLY_SUSPEND_P2P_SERVING_STR) ;
			}
			else if(0 == strcmp(MA_HTTP_SERVER_MSG_PROP_VAL_RQST_RESUME_P2P_SERVING_STR, msg_type)) {
				self->policies.is_p2p_serving_suspended = MA_FALSE ;
				MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "P2p serving resumed.") ;
				ma_message_set_property(response, MA_HTTP_SERVER_MSG_KEY_TYPE_STR, MA_HTTP_SERVER_MSG_PROP_VAL_RPLY_RESUME_P2P_SERVING_STR) ;
			}
			else {
				MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Invalid message type.") ;
				rc = MA_ERROR_UNEXPECTED ;
			}
		}
		else {
			MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Invalid message type.") ;
			rc = MA_ERROR_UNEXPECTED ;
		}
		rc = ma_msgbus_server_client_request_post_result(c_request, rc, response) ;
		(void)ma_message_release(response) ; response = NULL ;
	}
	return MA_ERROR_INVALIDARG ;
}

static ma_bool_t need_http_server_start(ma_http_server_policies_t *policy) {
	return  policy->is_agent_ping_enabled ||      
		policy->is_super_agent_enabled ||
		policy->is_datachannel_ping_enabled ||
		policy->is_remote_log_enabled ||
		policy->is_relay_enabled ||
		policy->is_content_serving_enabled ;
}

MA_HTTP_SERVER_API ma_error_t ma_http_server_start(ma_http_server_t *self) {
	if (self) {
		ma_error_t rc = MA_OK;

		if(need_http_server_start(&self->policies)) {
			struct sockaddr_in6 const sa6 = uv_ip6_addr("::", self->policies.tcp_port);
			struct sockaddr_in const sa4 = uv_ip4_addr("0.0.0.0", self->policies.tcp_port);
			rc = MA_ERROR_UNEXPECTED ;

			if(MA_OK != (rc = ma_msgbus_server_start(self->http_msgbus_server, ma_http_service_handler, (void *)self))) {
				MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to start http msgbus server, rc <%d>", rc) ;
				return rc ;
			}

			uv_tcp_init(self->uv_loop, &self->listen_socket);
			self->listen_socket.data = self;

			MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Http server starting");
			MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Http Server listening on port <%d>", self->policies.tcp_port);
			if (UV_OK == uv_tcp_bind6(&self->listen_socket, sa6) && UV_OK == uv_listen( (uv_stream_t *) &self->listen_socket, SOMAXCONN, &ma_accept_cb)) {
				int ipv6_only = 0;
				int opt_len = sizeof(ipv6_only);
				if ((0==getsockopt(MA_WIN_SELECT(self->listen_socket.socket,self->listen_socket.io_watcher.fd), IPPROTO_IPV6, IPV6_V6ONLY, (char *) &ipv6_only, &opt_len) && ipv6_only) || MA_WIN_SELECT(WSAENOPROTOOPT==WSAGetLastError(), ENOPROTOOPT==errno )) {
					/* listen on ipv4 separately if dual stack is not supported */
					uv_tcp_init(self->uv_loop, &self->alt_listen_socket);
					self->alt_listen_socket.data = self;
					if (UV_OK == uv_tcp_bind(&self->alt_listen_socket, sa4) && UV_OK == uv_listen( (uv_stream_t *) &self->alt_listen_socket, SOMAXCONN, &ma_accept_cb)) {
						MA_LOG(self->logger, MA_LOG_SEV_INFO, "listening on separate ipv6 and ipv4 sockets");
					} else {
						MA_LOG(self->logger, MA_LOG_SEV_INFO, "listening on ipv6 only socket");
					}
				} else {
					MA_LOG(self->logger, MA_LOG_SEV_INFO, "listening on dual-stack socket");
				}
				rc =  MA_OK;
			} else if (UV_OK == uv_tcp_bind(&self->listen_socket, sa4) && UV_OK == uv_listen( (uv_stream_t *) &self->listen_socket, SOMAXCONN, &ma_accept_cb)) {
				MA_LOG(self->logger, MA_LOG_SEV_INFO, "listening on ipv4 only socket");
				rc =  MA_OK;
			} else {
				uv_err_t err = uv_last_error(self->uv_loop);
				ma_http_server_log_uv_err(self,"Error starting http_server", MA_LOG_SEV_WARNING, err);
				rc =  MA_ERROR_FROM_UV_LAST_ERROR(self->uv_loop);
			}

			if(MA_OK == rc)				
				(void)ma_http_request_handler_factories_start(&self->handler_factories) ;			

			self->is_server_running = MA_TRUE;
		}

		return rc ;
	}
	return MA_ERROR_INVALIDARG;
}

/* <SiteStatus Status="Enabled" CatalogVersion="20140614190212"/> */
ma_error_t ma_http_server_update_sitestat_info(ma_http_server_t *self, const char *sitestat_data, size_t size) {
	if(self && sitestat_data && size > 0) {
		ma_xml_t *xml_data = NULL;

		if(MA_OK == ma_xml_create(&xml_data)) {
			if(MA_OK == ma_xml_load_from_buffer(xml_data, (char*)sitestat_data, size)) {
				ma_xml_node_t *root = ma_xml_get_node(xml_data);
				if(root) {
					ma_xml_node_t *node =  ma_xml_node_search_by_path(root, SITESTAT_ROOT_NODE_STR);
					if(node) {
						const char *p = NULL;
						memset(&self->catalog_version, 0, MA_MAX_LEN);
						p = (char *)ma_xml_node_attribute_get(node, SITESTAT_CATALOG_VERSION_STR);
						if(p) 
							MA_MSC_SELECT(_snprintf,snprintf)(self->catalog_version, MA_MAX_LEN - 1, "%s", p);

						p = (char *)ma_xml_node_attribute_get(node, SITESTAT_STATUS_ATTR_STR);

						if(p)
							self->is_sitestat_enabled = 0 == strcmp(p, SITESTAT_ENABLED_STR);

						MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Http server repository, sitestats status = %s catalog version = %s", (self->is_sitestat_enabled) ? "Enabled" : "Disabled", self->catalog_version);
					}
				}
			}
			(void)ma_xml_release(xml_data);
		}
	}

	return MA_OK;
}

static void read_sitestate_info(ma_http_server_t *self) {
	char sitestat_file[MA_MAX_PATH_LEN] = {0};
	ma_temp_buffer_t buffer = MA_TEMP_BUFFER_INIT;
	size_t buffer_size = 0;

	MA_MSC_SELECT(_snprintf,snprintf)(sitestat_file, MA_MAX_PATH_LEN - 1, "%s%c%s", self->policies.virtual_dir, MA_PATH_SEPARATOR, "SiteStat.xml");

	if(ma_fs_get_data(self->uv_loop, sitestat_file, NULL, &buffer_size)) {
		ma_temp_buffer_reserve(&buffer, buffer_size);
		if(ma_fs_get_data(self->uv_loop, sitestat_file, (unsigned char *)ma_temp_buffer_get(&buffer), &buffer_size)) {
			(void) ma_http_server_update_sitestat_info(self, (const char *)ma_temp_buffer_get(&buffer), buffer_size);
		}
	}

	ma_temp_buffer_uninit(&buffer);
}

static ma_error_t update_firewall_rule(ma_http_server_t *self) {
	ma_msgbus_endpoint_t *ep = NULL;
	ma_error_t rc = MA_OK;
	if(MA_OK == (rc = ma_msgbus_endpoint_create(self->msgbus, MA_IO_SERVICE_NAME_STR, NULL, &ep))) {
		ma_message_t *req = NULL;
		(void) ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
		(void)ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);	
		if(MA_OK == (rc = ma_message_create(&req))) {
			(void) ma_message_set_property(req, MA_IO_SERVICE_MSG_TYPE, MA_IO_MSG_TYPE_FIREWALL_RULE_CHANGED_STR);
			rc = ma_msgbus_send_and_forget(ep, req);
			(void) ma_message_release(req);
		}
		(void) ma_msgbus_endpoint_release(ep);
	}
	return rc;
}

ma_error_t ma_http_server_config(ma_http_server_t *self, const ma_policy_settings_bag_t *policy_bag, unsigned hint) {
	ma_int32_t tcp_port = MA_HTTP_SERVER_DEFAULT_TCP_PORT_INT;
	ma_queue_t *qitem;    
	ma_error_t rc = MA_OK;

	MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Http server configuring");

	if(MA_OK != (rc = ma_http_server_enforce_policies(self, policy_bag, hint)))
		MA_LOG(self->logger, MA_LOG_SEV_WARNING, "Failed to enforce http server policies, rc <%d>", rc);

	/* reset last used repo for each policy enforcement */
	if(self->policies.is_lazy_cache_enabled &&  self->last_used_repo_name)
		free(self->last_used_repo_name), self->last_used_repo_name = NULL;


	if(MA_OK != (rc = ma_policy_settings_bag_get_int((ma_policy_settings_bag_t *)policy_bag, 
		MA_HTTP_SERVER_SERVICE_SECTION_NAME_STR, 
		MA_HTTP_SERVER_KEY_PORT_INT, MA_FALSE,
		&tcp_port, tcp_port))) {
			MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to get http server port from policy bag, rc <%d>", rc);
			return rc;
	}

	if(hint == MA_SERVICE_CONFIG_NEW) {
		if(MA_OK != (rc = ma_msgbus_server_create(self->msgbus, MA_HTTP_SERVER_SERVICE_NAME_STR, &self->http_msgbus_server))) {
			MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to create http msgbus server, rc <%d>", rc) ;
			return rc ;
		}
		else {
			ma_msgbus_server_setopt(self->http_msgbus_server, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO) ;
			ma_msgbus_server_setopt(self->http_msgbus_server, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC) ;

			self->policies.tcp_port = tcp_port;        
		}

		if(self->policies.is_lazy_cache_enabled)
			(void) read_sitestate_info(self);
	}
	else {
		ma_bool_t is_restart_triggered = MA_FALSE;

		if(tcp_port != self->policies.tcp_port) {
			MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Http server port changed from <%d> to <%d>", self->policies.tcp_port, tcp_port);
			self->policies.tcp_port = tcp_port;

			if(MA_OK != (rc = ma_http_server_restart(self))) {
				MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to re-start http server on port chnage, rc <%d>", rc);
			}
			else {
				is_restart_triggered = MA_TRUE;
				if(MA_OK != (rc = update_firewall_rule(self)))
					MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to update firewall rule on tcp port change, rc = %d", rc);					
			}
		}

		if(!self->is_server_running && !is_restart_triggered) {
			if(MA_OK != (rc = ma_http_server_start(self)))
				MA_LOG(self->logger, MA_LOG_SEV_WARNING, "Failed to start http server, rc <%d>", rc);
		}
	}

	ma_queue_foreach(qitem, &self->handler_factories.qhead) {
		ma_http_request_handler_factory_t *factory = ma_queue_data(qitem, ma_http_request_handler_factory_t, qlink);
		(void)ma_http_request_handler_factory_configure(factory);
	}

	return MA_OK;
}

static void alt_listen_socket_close_cb(uv_handle_t *handle) {
	ma_http_server_t *self = (ma_http_server_t *)handle->data;
	ma_http_server_start(self);
}

static void listen_socket_close_cb(uv_handle_t *handle) {
	ma_http_server_t *self = (ma_http_server_t *)handle->data;
	if (UV_TCP == self->alt_listen_socket.type) 
		uv_close((uv_handle_t*) &self->alt_listen_socket, alt_listen_socket_close_cb);
	else   
		ma_http_server_start(self);
}

MA_HTTP_SERVER_API ma_error_t ma_http_server_restart(ma_http_server_t *self) {
	if(self->is_server_running) {
		uv_close((uv_handle_t*) &self->listen_socket, listen_socket_close_cb);
		ma_http_connection_manager_stop_all(&self->connection_mgr); /* TBR - is this stop all ok here */
		(void)ma_http_request_handler_factories_stop(&self->handler_factories) ;
	}
	return MA_OK;
}

MA_HTTP_SERVER_API ma_error_t ma_http_server_stop(ma_http_server_t *self) {
	if (self) {
		MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Http server stopping");

		if(self->agent_wakeup_timer) {
			if(uv_is_active((uv_handle_t *)self->agent_wakeup_timer))
				uv_timer_stop(self->agent_wakeup_timer);
			uv_close((uv_handle_t *)self->agent_wakeup_timer, timer_close_cb);
			self->agent_wakeup_timer = NULL ;
		}

		if(self->is_server_running) {
			if (UV_TCP == self->alt_listen_socket.type) uv_close((uv_handle_t*) &self->alt_listen_socket, 0);
			if (UV_TCP == self->listen_socket.type) uv_close((uv_handle_t*) &self->listen_socket, 0);

			(void)ma_msgbus_server_stop(self->http_msgbus_server) ;

			ma_http_connection_manager_stop_all(&self->connection_mgr);

			(void)ma_http_request_handler_factories_stop(&self->handler_factories) ; /* stop all factories before releasing them */
		}
	}

	return MA_OK;
}

void ma_http_server_on_connection_stopped(ma_http_server_t *self, ma_http_connection_t *connection) {
	ma_http_connection_manager_remove(&self->connection_mgr, connection);
}

void ma_http_server_log_uv_err(ma_http_server_t *self, char const *msg, ma_log_severity_t sev, uv_err_t uv_err) {
	MA_LOG(self->logger, sev, "%s - libuv err: (\'%s\' %s)", msg, uv_strerror(uv_err),  uv_err_name(uv_err));
}

static ma_error_t wakeup_req_cb(ma_error_t status, ma_message_t *response, void *cb_data, ma_msgbus_request_t *request) {
	ma_error_t rc = MA_OK;    
	ma_http_server_t *self = (ma_http_server_t *)cb_data;
	ma_msgbus_endpoint_t *ep = NULL;

	MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "status from agent wakeup request, status <%d>", status);

	if((status == MA_OK) && response) ma_message_release(response);		
	if(MA_OK == (rc = ma_msgbus_request_get_endpoint(request, &ep))) {
		(void)ma_msgbus_request_release(request);
		(void)ma_msgbus_endpoint_release(ep);    
	}
	return rc;
}

static void timer_close_cb(uv_handle_t *handle) {
	free(handle) ;
}

static void agent_wakeup_startup_cb(uv_timer_t *timer, int status) {
	ma_error_t rc = MA_OK;
	ma_message_t *req_msg = NULL;
	ma_http_server_t *self = (ma_http_server_t *) timer->data;
	if(MA_OK == (rc = ma_message_create(&req_msg))) {
		ma_msgbus_endpoint_t *ep = NULL;
		(void) ma_message_set_property(req_msg, MA_PROPERTY_MSG_KEY_TYPE_STR, MA_PROPERTY_MSG_TYPE_COLLECT_STR);
		(void) ma_message_set_property(req_msg, MA_PROPERTY_MSG_KEY_PROPS_TYPE_STR, MA_PROPERTY_MSG_VALUE_PROPS_TYPE_VERSION_STR);
		if(MA_OK == (rc = ma_msgbus_endpoint_create(self->msgbus, MA_PROPERTY_SERVICE_NAME_STR, NULL, &ep))) {
			ma_msgbus_request_t *request = NULL; 
			ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
			ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
			if(MA_OK != (rc = ma_msgbus_async_send(ep, req_msg, wakeup_req_cb, self, &request)))
				(void)ma_msgbus_endpoint_release(ep);
		}
		(void) ma_message_release(req_msg); req_msg = NULL;
	}
	uv_close((uv_handle_t *)timer, timer_close_cb);
	self->agent_wakeup_timer = NULL;
}

/* Deffer agent wake up minumum 1 sec otherwise props version is same and changed props are getting lost in ASCI */
static void wakeup_agent_on_sa_policy_change(ma_http_server_t *self) {
	ma_uint32_t random_interval = ma_sys_rand() % 2 + 1 ;
	MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Waking-up agent on SA repository policy change, randomization <%d>", random_interval);
	self->agent_wakeup_timer = (uv_timer_t *)calloc(1, sizeof(uv_timer_t));
	if(self->agent_wakeup_timer) {
		uv_timer_init(self->uv_loop, self->agent_wakeup_timer) ;
		self->agent_wakeup_timer->data = self ;
		MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "uv timer data(%p)\n", self);
		uv_timer_start(self->agent_wakeup_timer, agent_wakeup_startup_cb, random_interval*1000, 0);
	}
	else
		MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to wake-up the agent");
}

static ma_error_t io_srvc_request_async_cb(ma_error_t status, ma_message_t *response, void *cb_data, ma_msgbus_request_t *request) {
	ma_http_server_t *self = (ma_http_server_t *) cb_data;
	ma_bool_t b_rc = MA_FALSE;
	const char *sa_repo_virtual_dir = NULL;

	(void) ma_message_get_property(response, MA_DESTINATION_FOLDER_NAME_STR, &sa_repo_virtual_dir);    
	if(MA_OK == status && sa_repo_virtual_dir) {    
		char temp_download_folder[MA_MAX_PATH_LEN] = {0};
		MA_MSC_SELECT(_snprintf,snprintf)(temp_download_folder, MA_MAX_PATH_LEN - 1, "%s%c%s", sa_repo_virtual_dir, MA_PATH_SEPARATOR, MA_HTTP_SA_REPOSITORY_TEMP_DIR);

		ma_fs_make_recursive_dir(self->uv_loop, NULL, temp_download_folder);
		if(ma_fs_is_file_exists(self->uv_loop, temp_download_folder)){
			memset(&self->policies.virtual_dir, 0, MA_MAX_PATH_LEN);
			strncpy(self->policies.virtual_dir, sa_repo_virtual_dir, MA_MAX_PATH_LEN-1);
			b_rc = MA_TRUE;
		}

		if(b_rc) {
			MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Cleared cache manager as virtual directory policy changed");
			(void) ma_http_server_repo_cache_flush(self); /* clear cache if virtual dir policy changed */

			/* update the base dir for content referenced for LAZY CACHE */
			if(MA_OK != ma_p2p_db_update_base_dir_by_contributor(self->msgbus_db, "LAZY_CAHCE", self->policies.virtual_dir))
				MA_LOG(self->logger, MA_LOG_SEV_WARNING, "Failed to update base directory for referenced content") ;
		}
	}
	else 
		MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to create new virtual directory");

	if((status == MA_OK) && response) ma_message_release(response);
	if(request) {
		ma_msgbus_endpoint_t * ep = NULL;
		(void) ma_msgbus_request_get_endpoint(request, &ep);		
		(void) ma_msgbus_request_release(request);
		if(ep) (void) ma_msgbus_endpoint_release(ep);
	}

	return MA_OK;
}

static ma_error_t send_folder_create_request(ma_http_server_t *self, const char *folder_name) {
	ma_error_t rc = MA_OK;
	ma_message_t *msg = NULL;

	MA_LOG(self->logger, MA_LOG_SEV_TRACE, "Sending <%s> folder creation requesting", folder_name);

	if(MA_OK == (rc = ma_message_create(&msg))) {   
		ma_table_t *acl_tb = NULL;
		if(MA_OK == (rc = ma_table_create(&acl_tb))) {
			ma_variant_t *value = NULL;

			if(MA_OK == (rc = ma_variant_create_from_int8(MA_SID_LOCAL, &value))) {
				ma_table_add_entry(acl_tb, MA_ACL_SID_INT, value);
				ma_variant_release(value); value = NULL;
			}

			if((MA_OK ==rc) && (MA_OK == (rc = ma_variant_create_from_string(MA_CMNSVC_SID_NAME, sizeof(MA_CMNSVC_SID_NAME), &value)))) {
				ma_table_add_entry(acl_tb, MA_ACL_SID_NAME_STR, value);
				ma_variant_release(value); value = NULL;
			}

			if((MA_OK ==rc) && (MA_OK == (rc = ma_variant_create_from_bool(MA_TRUE, &value)))) {
				ma_table_add_entry(acl_tb, MA_ACL_SID_ALLOW_BOOL, value);
				ma_variant_release(value); value = NULL;
			}

			if((MA_OK ==rc) && (MA_OK == (rc = ma_variant_create_from_string(folder_name, strlen(folder_name), &value)))) {
				ma_table_add_entry(acl_tb, MA_ACL_OBJECT_NAME_STR, value);
				ma_variant_release(value); value = NULL;
			}

			if((MA_OK ==rc) && (MA_OK == (rc = ma_variant_create_from_int64(MA_CMNSVC_ACL_PERMISSIONS, &value)))) {
				ma_table_add_entry(acl_tb, MA_ACL_OBJECT_PERMISSIONS_INT, value);
				ma_variant_release(value); value = NULL;
			}

			if((MA_OK ==rc) && (MA_OK == (rc = ma_variant_create_from_int64(MA_CMNSVC_ACL_INHERITANCE, &value)))) {
				ma_table_add_entry(acl_tb, MA_ACL_OBJECT_INHERITANCE_INT, value);
				ma_variant_release(value); value = NULL;
			}

			if((MA_OK ==rc) && (MA_OK == (rc = ma_variant_create_from_bool(MA_TRUE, &value)))) {
				ma_table_add_entry(acl_tb, MA_ACL_OBJECT_INHERITANCE_BOOL, value);
				ma_variant_release(value); value = NULL;
			}

			if((MA_OK ==rc) && (MA_OK == (rc = ma_variant_create_from_bool(MA_TRUE, &value)))) {
				ma_table_add_entry(acl_tb, MA_ACL_OBJECT_IS_DIRCTORY_BOOL, value);
				ma_variant_release(value); value = NULL;
			}

			if(MA_OK == rc) {
				ma_variant_t *payload = NULL;
				if(MA_OK == (rc = ma_variant_create_from_table(acl_tb, &payload))) {
					if(MA_OK == (rc = ma_message_set_payload(msg, payload))) {
						ma_msgbus_endpoint_t *ep = NULL;
						if(MA_OK == (rc = ma_message_set_property(msg, MA_PROP_KEY_REQUEST_TYPE_STR, MA_PROP_VALUE_ACL_REQUEST_STR))) {
							if(MA_OK == (rc = ma_msgbus_endpoint_create(self->msgbus, MA_IO_SERVICE_NAME_STR, NULL, &ep))) {
								ma_msgbus_request_t *request = NULL;
								(void) ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
								(void) ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
								if(MA_OK != (rc = ma_msgbus_async_send(ep, msg, io_srvc_request_async_cb, self, &request))) {				
									(void) ma_msgbus_endpoint_release(ep);
								}	
							}
						}
					}
					(void) ma_variant_release(payload);
				}
			}
			(void) ma_table_release(acl_tb);
		}
		(void) ma_message_release(msg);
	}

	return rc;
}

static ma_error_t send_folder_move_request(ma_http_server_t *self, const char *src_base_folder, const char *dest_folder) {
	ma_error_t rc = MA_OK;
	ma_message_t *msg = NULL;

	MA_LOG(self->logger, MA_LOG_SEV_TRACE, "Request to Move folder from <%s> to <%s> ", self->policies.virtual_dir, dest_folder);
	if(MA_OK == (rc = ma_message_create(&msg))) {   
		ma_msgbus_endpoint_t *ep = NULL;
		if(MA_OK == (rc = ma_msgbus_endpoint_create(self->msgbus, MA_IO_SERVICE_NAME_STR, NULL, &ep))) {
			ma_msgbus_request_t *request = NULL;
			(void) ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
			(void) ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);

			(void) ma_message_set_property(msg, MA_PROP_KEY_REQUEST_TYPE_STR, MA_PROP_VALUE_FILE_MOVE_REQUEST_STR);
			(void) ma_message_set_property(msg, MA_FILE_MOVE_SOURCE_STR, self->policies.virtual_dir);
			(void) ma_message_set_property(msg, MA_FILE_MOVE_DESTINATION_STR, dest_folder);
			(void) ma_message_set_property(msg, MA_FILE_MOVE_BASE_DIR_STR, src_base_folder);       

			if(MA_OK != (rc = ma_msgbus_async_send(ep, msg, io_srvc_request_async_cb, self, &request))) {				
				(void) ma_msgbus_endpoint_release(ep);
			}
		}
		(void) ma_message_release(msg);
	}

	return rc;
}
static ma_bool_t update_virtual_dir(ma_http_server_t *self, const char *virtual_dir) {
	ma_error_t rc = MA_OK;
	/* There is no chance to get the empty string from new policy as ePO allows only valid strings */

	char sa_repo_virtual_dir[MA_MAX_PATH_LEN] = {0};

	if(!virtual_dir) {
		MA_LOG(self->logger, MA_LOG_SEV_WARNING, "Received virtual directory as NULL, continue with currnt virtual directory.");
		return MA_FALSE;
	}

	if(!strcmp(virtual_dir, "DEFAULT")) {
		ma_temp_buffer_t data_path = MA_TEMP_BUFFER_INIT;
		const char *str = NULL;
		if(MA_OK != ma_conf_get_data_path(&data_path)) 
			return MA_FALSE;	
		str = (char *)ma_temp_buffer_get(&data_path);
		if(str[strlen(str)-1] != MA_PATH_SEPARATOR)
			MA_MSC_SELECT(_snprintf,snprintf)(sa_repo_virtual_dir, MA_MAX_PATH_LEN - 1, "%s%c%s%c%s", ma_temp_buffer_get(&data_path),MA_PATH_SEPARATOR, MA_AGENT_DATA_DIR_STR, MA_PATH_SEPARATOR, MA_HTTP_SA_REPOSITORY_DIR); 
		else
			MA_MSC_SELECT(_snprintf,snprintf)(sa_repo_virtual_dir, MA_MAX_PATH_LEN - 1, "%s%s%c%s", ma_temp_buffer_get(&data_path),MA_AGENT_DATA_DIR_STR, MA_PATH_SEPARATOR, MA_HTTP_SA_REPOSITORY_DIR); 

		ma_temp_buffer_uninit(&data_path);
	}
	else {
		if(virtual_dir[strlen(virtual_dir) -1] != MA_PATH_SEPARATOR)
			MA_MSC_SELECT(_snprintf,snprintf)(sa_repo_virtual_dir, MA_MAX_PATH_LEN - 1, "%s%c%s", virtual_dir, MA_PATH_SEPARATOR, MA_HTTP_SA_REPOSITORY_DIR);        
		else
			MA_MSC_SELECT(_snprintf,snprintf)(sa_repo_virtual_dir, MA_MAX_PATH_LEN - 1, "%s%s", virtual_dir, MA_HTTP_SA_REPOSITORY_DIR);
	}

	if(!strlen(self->policies.virtual_dir)) {
		/* 1. current policy is empty and received policy virtual folder is not available in file system */
		if(!ma_fs_is_file_exists(self->uv_loop, sa_repo_virtual_dir)) {
			rc = send_folder_create_request(self, sa_repo_virtual_dir);
		}
		else /* 2. current policy is empty and received policy virtual folder is available in file system */
			strncpy(self->policies.virtual_dir, sa_repo_virtual_dir, MA_MAX_PATH_LEN-1);
	}
	else {
		/* 3. current policy virtual folder is not available in file system, create new virtual folder */
		if(!ma_fs_is_file_exists(self->uv_loop, self->policies.virtual_dir)) {
			rc = send_folder_create_request(self, sa_repo_virtual_dir);
		}
		else {
			/* 4. if existing and received virtual folders are not same, move old folder to new path */
			if(strcmp(sa_repo_virtual_dir, self->policies.virtual_dir)) {
				rc = send_folder_move_request(self, virtual_dir, sa_repo_virtual_dir);
			}
		}
	}

	return (MA_OK == rc) ? MA_TRUE : MA_FALSE;
}

static ma_error_t ma_http_server_enforce_policies(ma_http_server_t *self, const ma_policy_settings_bag_t *policy_bag, unsigned hint) {
	ma_error_t rc = MA_OK;
	ma_bool_t is_sa_repo_enabled = MA_FALSE;

	(void) ma_policy_settings_bag_get_bool((ma_policy_settings_bag_t *)policy_bag, MA_HTTP_SERVER_SERVICE_SECTION_NAME_STR, MA_HTTP_SERVER_KEY_ENABLE_DATACHANNEL_PING_INT, MA_FALSE, &self->policies.is_datachannel_ping_enabled, MA_TRUE);
	(void) ma_policy_settings_bag_get_bool((ma_policy_settings_bag_t *)policy_bag, MA_HTTP_SERVER_SERVICE_SECTION_NAME_STR, MA_HTTP_SERVER_KEY_ENABLE_AGENT_PING_INT, MA_FALSE, &self->policies.is_agent_ping_enabled, MA_TRUE);
	(void) ma_policy_settings_bag_get_bool((ma_policy_settings_bag_t *)policy_bag, MA_HTTP_SERVER_SERVICE_SECTION_NAME_STR, MA_HTTP_SERVER_KEY_ENABLE_SUPER_AGENT_INT, MA_FALSE, &self->policies.is_super_agent_enabled, MA_FALSE);    
	(void) ma_policy_settings_bag_get_bool((ma_policy_settings_bag_t *)policy_bag, MA_HTTP_SERVER_SERVICE_SECTION_NAME_STR, MA_HTTP_SERVER_KEY_LISTEN_TO_EPO_SERVER_ONLY_INT, MA_FALSE,&self->policies.is_listen_to_epo_server_only, MA_FALSE);    
	(void) ma_policy_settings_bag_get_int((ma_policy_settings_bag_t *)policy_bag, MA_HTTP_SERVER_SERVICE_SECTION_NAME_STR, MA_HTTP_SERVER_KEY_SAREPO_CONCURRENT_CONNECTIONS_LIMIT_INT, MA_FALSE,(ma_int32_t *)&self->policies.sa_repo_concurrent_connections_limit, 1024) ;

	(void) ma_policy_settings_bag_get_bool((ma_policy_settings_bag_t *)policy_bag, MA_LOGGER_SERVICE_SECTION_NAME_STR, MA_LOGGER_KEY_IS_REMOTE_LOG_ENABLED_INT, MA_FALSE, &self->policies.is_remote_log_enabled, MA_FALSE);

	(void)ma_policy_settings_bag_get_bool((ma_policy_settings_bag_t *)policy_bag, MA_P2P_SERVICE_SECTION_NAME_STR, MA_P2P_KEY_ENABLE_SERVING_INT, MA_FALSE,&self->policies.is_content_serving_enabled, MA_TRUE) ;
	(void) ma_policy_settings_bag_get_int((ma_policy_settings_bag_t *)policy_bag, MA_P2P_SERVICE_SECTION_NAME_STR, MA_P2P_KEY_CONCURRENT_CONNECTIONS_LIMIT_INT, MA_FALSE, (ma_int32_t *)&self->policies.p2p_concurrent_connections_limit, 10) ;

	(void) ma_policy_settings_bag_get_bool((ma_policy_settings_bag_t *)policy_bag, MA_RELAY_SERVICE_SECTION_NAME_STR, MA_RELAY_KEY_IS_ENABLED_INT, MA_FALSE,&self->policies.is_relay_enabled, MA_FALSE);
	(void) ma_policy_settings_bag_get_int((ma_policy_settings_bag_t *)policy_bag, MA_RELAY_SERVICE_SECTION_NAME_STR, MA_RELAY_KEY_CONCURRENT_CONNECTIONS_LIMIT_INT, MA_FALSE,(ma_int32_t *)&self->policies.relay_concurrent_connections_limit, 1024) ;

	(void) ma_policy_settings_bag_get_bool((ma_policy_settings_bag_t *)policy_bag, MA_HTTP_SERVER_SERVICE_SECTION_NAME_STR, MA_HTTP_SERVER_KEY_ENABLE_SUPER_AGENT_REPOSITORY_INT, MA_FALSE,&is_sa_repo_enabled, MA_FALSE);

	/* Start ASCI when SA repository policy "IsSuperAgentRepositoryEnabled" changed from 0 -> 1 or 1 -> 0 */	
	if(self->policies.is_super_agent_repository_enabled != is_sa_repo_enabled) {
		self->policies.is_super_agent_repository_enabled = is_sa_repo_enabled;
		if(hint != MA_SERVICE_CONFIG_NEW) 
			(void)wakeup_agent_on_sa_policy_change(self);
	}

	if(self->policies.is_super_agent_repository_enabled) { /* No need to get the virtual directory & lazy cache policies if SA repository is disabled */
		const char *virtual_dir = "DEFAULT";
		ma_bool_t is_lazy_cache_enabled = MA_FALSE;

		(void) ma_policy_settings_bag_get_str((ma_policy_settings_bag_t *)policy_bag, MA_HTTP_SERVER_SERVICE_SECTION_NAME_STR, MA_HTTP_SERVER_KEY_VIRTUAL_DIRECTORY_STR, MA_FALSE,&virtual_dir, virtual_dir );
		(void) ma_policy_settings_bag_get_int((ma_policy_settings_bag_t *)policy_bag, MA_HTTP_SERVER_SERVICE_SECTION_NAME_STR, MA_HTTP_SERVER_KEY_REPOSITORY_SYNC_INTERVAL_INT, MA_FALSE,&self->policies.repository_sync_interval, 30);        
		(void) ma_policy_settings_bag_get_int((ma_policy_settings_bag_t *)policy_bag, MA_HTTP_SERVER_SERVICE_SECTION_NAME_STR, MA_HTTP_SERVER_KEY_REPOSITORY_DISK_QUOTA_INT, MA_FALSE,&self->policies.repository_disk_quota, 1) ;
		(void) ma_policy_settings_bag_get_bool((ma_policy_settings_bag_t *)policy_bag, MA_HTTP_SERVER_SERVICE_SECTION_NAME_STR, MA_HTTP_SERVER_KEY_ENABLE_LAZY_CACHING_INT, MA_FALSE,&is_lazy_cache_enabled, MA_FALSE);

		MA_LOG(self->logger, MA_LOG_SEV_TRACE, "Http handler polices is_super_agent_enabled <%d>, is_super_agent_repository_enabled <%d>, is_lazy_cache_enabled <%d>, virtual_dir <%s>", self->policies.is_super_agent_enabled, self->policies.is_super_agent_repository_enabled, is_lazy_cache_enabled, virtual_dir);

		if(is_lazy_cache_enabled != self->policies.is_lazy_cache_enabled) {
			self->policies.is_lazy_cache_enabled = is_lazy_cache_enabled;
			if(self->policies.is_lazy_cache_enabled) {
				ma_url_cache_manager_create(&self->cache_manager);
				self->last_time_check_for_new_content = ma_get_tick_count();
				MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Created cache manager as lazy cache policy enabled");
			}
			else {
				MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Cleared cache manager as lazy cache policy disabled");
				(void) ma_http_server_repo_cache_flush(self);                
			}
		}   

		if(!update_virtual_dir(self, virtual_dir)) {
			MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to update virtual directory on policy change");
			rc = MA_ERROR_HTTP_SERVER_INTERNAL;
		}        
	}
	else {	/* remove the repo content */
		MA_LOG(self->logger, MA_LOG_SEV_TRACE, "Clearing cache and removing virtual directory as SA Repo policy is disabled");
		(void) ma_http_server_repo_cache_flush(self);

		if(ma_fs_is_file_exists(self->uv_loop, self->policies.virtual_dir))
			ma_fs_remove_recursive_dir(self->uv_loop, self->policies.virtual_dir) ;

		(void)ma_p2p_db_erase_content_by_contributor(self->msgbus_db, "LAZY_CACHE") ;
	}

	self->policies.is_p2p_serving_suspended = self->policies.is_p2p_serving_suspended? self->policies.is_content_serving_enabled : self->policies.is_p2p_serving_suspended ;

	return rc;
}


static ma_bool_t ma_http_server_resolve_name(ma_http_server_t *self, struct sockaddr* addr, socklen_t length, char *buff, size_t buflen, char *peer_addr, MA_MAP_T(ma_dns_resolver_map) *map) {
	char *peer_name = NULL;
	if(0 == getnameinfo(addr, length, buff, NI_MAXHOST, NULL, 0 , NI_NUMERICSERV)) {
		size_t size= 0;
		char str_temp[NI_MAXHOST] = {0};
		ma_format_v6_addr(buff, str_temp, MA_FALSE);
		peer_name = strdup(str_temp);
		MA_LOG(self->logger, MA_LOG_SEV_INFO, "Peer address (%s) resolves to peer_name (%s)", peer_addr, buff);
		MA_MAP_SIZE(ma_dns_resolver_map, map, size);
		if(size >= MA_MAX_DNS_ENTRIES) 
			MA_MAP_CLEAR(ma_dns_resolver_map, map);
		MA_MAP_ADD_ENTRY(ma_dns_resolver_map, map, peer_addr, peer_name);
		free(peer_name);
		return MA_TRUE;
	}
	else {
		MA_LOG(self->logger, MA_LOG_SEV_TRACE, "Error occured when trying to get name information on peer addr");
	}
	return MA_FALSE;
}
ma_bool_t ma_http_server_request_validation_by_policy(ma_http_server_t *self, ma_http_request_t const *request) {
	ma_http_connection_t *connection = (ma_http_connection_t *)request->parser.data;
	ma_bool_t b_valid_request = !self->policies.is_listen_to_epo_server_only;

	ma_http_request_handler_factories_t *factories = &self->handler_factories;
	MA_MAP_T(ma_dns_resolver_map) *map = factories->dns_map;    

	if(!b_valid_request) 
		b_valid_request = is_request_from_valid_client(self->context, connection->peer_addr);

	if(!b_valid_request) {
		char *peer_name = NULL;
		char *temp = NULL;
		if(MA_OK == MA_MAP_GET_VALUE(ma_dns_resolver_map, map, connection->peer_addr, temp)) {
			peer_name = strdup(temp);
			MA_LOG(self->logger, MA_LOG_SEV_DEBUG,"peer_addr(%s) resolves to peer_name (%s)",connection->peer_addr, peer_name?peer_name:connection->peer_addr);
		}
		else {
			char buff[NI_MAXHOST]= {0};
			char str_peer_addr[NI_MAXHOST] = {0};
			struct addrinfo *res = NULL;
			ma_net_interface_list_t *list = NULL;
			/*This scanning will happen for every unique peer request. If it has already been resolved it is put in the cache.*/
			if(MA_OK == ma_net_interface_list_create(&list)) {
				if(MA_OK == ma_net_interface_list_scan(list, MA_FALSE)) {
					ma_bool_t b_linklocal = ma_format_v6_addr_with_scope(list, connection->peer_addr, str_peer_addr, MA_FALSE);

					/*The getadrinfo is a necessary blocking call here but it'll be performed only one time for a peer ip and it is cached.
					  getaddrinfo goes into a blocking state if a hostname is given to resolve the hostname into available addresses,
					  since ip is given it simply forms the structure and gives us an addrinfo structure.*/
					if(b_linklocal) {
						if(0 == ma_create_addrinfo_struct(list, str_peer_addr, "", &res, MA_FALSE)) {
							while(res) {
								if(ma_http_server_resolve_name(self, res->ai_addr, res->ai_addrlen, buff, NI_MAXHOST, connection->peer_addr, map)) 
									break;
								res = res->ai_next;
							}
							freeaddrinfo(res);
						}
					}
					else {
						struct sockaddr_storage *sa = &connection->sa;
						ma_http_server_resolve_name(self, (struct sockaddr *) sa, sizeof(struct sockaddr_storage) , buff, NI_MAXHOST, connection->peer_addr, map);
					}
				}
				ma_net_interface_list_release(list);
			}
		}

		b_valid_request = is_request_from_valid_client(self->context, peer_name);

		if(peer_name)
			free(peer_name);
	}

	return b_valid_request;
}