#include "ma/internal/services/http_server/ma_ssl_proxy_handler.h"

#include "ma/internal/utils/repository/ma_repository_db_manager.h"

#include "ma_http_connection.h"
#include "ma_http_server_internal.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/defs/ma_stats_defs.h"
#include "ma/internal/services/stats/ma_stats_db_manager.h"
#include "ma/internal/ma_macros.h"

#include <stdlib.h>
#include <assert.h>
#include <stdio.h>

typedef struct ma_ssl_proxy_handler_s {
    ma_http_request_handler_t base;

    /**
     * can't do much without a connection
     */
    ma_http_connection_t *connection;

    /**
     * server port to connect to, in host byte order
     */
    ma_uint16_t s_port;

    /**
     * for DNS lookup
     */
    uv_getaddrinfo_t addr_info_req;

    /**
     * Results of DNS lookup
     */
    struct addrinfo *addr_info_begin, *addr_info_iter;

    /**
     * socket to upstream server
     */
    uv_tcp_t    s_socket;

    /**
     *
     */
    uv_connect_t connect_req;

    uv_shutdown_t s_shutdown_req, c_shutdown_req;

    /**
     * uv write request handles 
     */
    uv_write_t  c_write_req, s_write_req;

    char upstream_buffer[MA_HTTP_REPONSE_DATA_SIZE], downstream_buffer[MA_HTTP_REPONSE_DATA_SIZE];

} ma_ssl_proxy_handler_t;

static ma_error_t ssl_proxy_start(ma_http_request_handler_t *handler, struct ma_http_connection_s *connection);

static ma_error_t ssl_proxy_stop(ma_http_request_handler_t *handler);

static void ssl_proxy_destroy(ma_http_request_handler_t *handler);

const static ma_http_request_handler_methods_t ssl_proxy_methods = {
    &ssl_proxy_start,
    &ssl_proxy_stop,
    &ssl_proxy_destroy
};

typedef struct ma_ssl_proxy_handler_factory_s {
    ma_http_request_handler_factory_t   base;

    ma_logger_t                         *logger;

	ma_db_t								*msgbus_db ;
} ma_ssl_proxy_handler_factory_t;

static ma_bool_t ssl_proxy_factory_matches(ma_http_request_handler_factory_t *self, ma_http_request_t const *request);

static ma_bool_t ssl_proxy_factory_authorize(ma_http_request_handler_factory_t *self, ma_http_request_t const *request);

static ma_error_t ssl_proxy_factory_configure(ma_http_request_handler_factory_t *self);

static ma_error_t ssl_proxy_factory_start(ma_http_request_handler_factory_t *self) ;

static ma_error_t ssl_proxy_factory_stop(ma_http_request_handler_factory_t *self) ;

static ma_error_t ssl_proxy_factory_create_instance(ma_http_request_handler_factory_t *self, ma_http_request_handler_t **handler);

static void ssl_proxy_factory_destroy(ma_http_request_handler_factory_t *self);

const static ma_http_request_handler_factory_methods_t ssl_proxy_factory_methods = {
    &ssl_proxy_factory_matches,
    &ssl_proxy_factory_authorize,
    &ssl_proxy_factory_configure,
    &ssl_proxy_factory_create_instance,
	&ssl_proxy_factory_start,
	&ssl_proxy_factory_stop,
    &ssl_proxy_factory_destroy
};

static void log_uv_err(ma_ssl_proxy_handler_t *self, char const *msg, ma_log_severity_t sev, uv_err_t uv_err);

static void ssl_proxy_try_connect(ma_ssl_proxy_handler_t *handler);

static ma_error_t ssl_proxy_give_up(ma_ssl_proxy_handler_t *handler);

static ma_error_t ssl_proxy_begin_teardown(ma_ssl_proxy_handler_t *handler);

static ma_bool_t is_it_valid_spipe_site(ma_db_t *db, uv_buf_t url);

/* uv callbacks */ 

static void c_write_cb(uv_write_t *req, int status);
static void c_read_cb(uv_stream_t *stream, ssize_t nread, uv_buf_t buf);
static void c_shutdown_cb(uv_shutdown_t *req, int status);


/* * * * F A C T O R Y * * */

/* accept any HTTP CONNECT requests */
static ma_bool_t ssl_proxy_factory_matches(ma_http_request_handler_factory_t *factory, struct ma_http_request_s const *request) {
    return HTTP_CONNECT == request->parser.method;
}

static ma_bool_t ssl_proxy_factory_authorize(ma_http_request_handler_factory_t *factory, ma_http_request_t const *request) {
    ma_ssl_proxy_handler_factory_t *self = (ma_ssl_proxy_handler_factory_t *)factory;
    ma_http_server_t *server = (ma_http_server_t *) self->base.data;

	if(server->policies.is_relay_enabled && is_it_valid_spipe_site(server->db, request->url)) {
		if(server->policies.relay_active_connections <= server->policies.relay_concurrent_connections_limit) {
			return MA_TRUE ;
		}
		else {
			(void)ma_stats_db_update_current(self->msgbus_db, MA_STAT_RELAY_SERVER, MA_STAT_ATTRIBUTE_SURGED_CONNECTIONS, (ma_double_t)1) ;
			MA_LOG(self->logger, MA_LOG_SEV_WARNING, "http proxy/relay exceeds max concurrent connections limit.") ;
		}
	}
	else 
		MA_LOG(self->logger, MA_LOG_SEV_WARNING, "http proxy/relay is disabled or not a valid spipe site.") ;

    return MA_FALSE ;
}

static ma_error_t ssl_proxy_factory_configure(ma_http_request_handler_factory_t *factory) {
    ma_ssl_proxy_handler_factory_t *self = (ma_ssl_proxy_handler_factory_t *)factory;    
    return MA_OK;
}

static ma_error_t ssl_proxy_factory_start(ma_http_request_handler_factory_t *factory) {
	ma_ssl_proxy_handler_factory_t *self = (ma_ssl_proxy_handler_factory_t *)factory ;

	if(self)
		MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "http sll proxy/relay handler started.") ;

	return MA_OK ;
}

static ma_error_t ssl_proxy_factory_stop(ma_http_request_handler_factory_t *factory) {
	ma_ssl_proxy_handler_factory_t *self = (ma_ssl_proxy_handler_factory_t *)factory ;

	if(self)
		MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "http sll proxy/relay handler stoped.") ;

	return MA_OK ;
}

/* this may have to be a method of an ssl_proxy_handler_factory class instead */
static ma_error_t ssl_proxy_factory_create_instance(ma_http_request_handler_factory_t *self, ma_http_request_handler_t **handler) {
    ma_ssl_proxy_handler_t *instance = calloc(1, sizeof(ma_ssl_proxy_handler_t));
    if (instance) {
		ma_ssl_proxy_handler_factory_t *factory = (ma_ssl_proxy_handler_factory_t *)self ;
		ma_http_server_t *server = (ma_http_server_t *) factory->base.data ;

        ma_http_request_handler_init(&instance->base, &ssl_proxy_methods, self);
        *handler = &instance->base;
		
		++server->policies.relay_active_connections ;

        return MA_OK;
    }
    return MA_ERROR_OUTOFMEMORY;
}

static void ssl_proxy_factory_destroy(ma_http_request_handler_factory_t *self) {
    free(self);
}

MA_HTTP_SERVER_API ma_error_t ma_ssl_proxy_handler_factory_create(ma_http_server_t *server, ma_http_request_handler_factory_t **ssl_proxy_factory) {
    ma_ssl_proxy_handler_factory_t *self = calloc(1, sizeof(ma_ssl_proxy_handler_factory_t));
    if (self) {
        ma_http_request_handler_factory_init(&self->base, &ssl_proxy_factory_methods, server);
        self->logger = server->logger;
        *ssl_proxy_factory = &self->base;
        return MA_OK;
    }

    return MA_ERROR_OUTOFMEMORY;
}

MA_HTTP_SERVER_API ma_error_t  ma_ssl_proxy_handler_factory_logger(ma_http_request_handler_factory_t *factory, ma_logger_t *logger) {
	ma_ssl_proxy_handler_factory_t *self = (ma_ssl_proxy_handler_factory_t *)factory;
    return (self && (self->logger = logger)) ? MA_OK : MA_ERROR_INVALIDARG;
}

MA_HTTP_SERVER_API ma_error_t ma_ssl_proxy_handler_factory_set_msgbus_db(ma_http_request_handler_factory_t *factory, ma_db_t *db) {
	ma_ssl_proxy_handler_factory_t *self = (ma_ssl_proxy_handler_factory_t *)factory;
    return (self && (self->msgbus_db = db)) ? MA_OK : MA_ERROR_INVALIDARG;
}

/* * * * * END F A C T O R Y * * * * */

/* allocate memory for data coming from server */
static uv_buf_t s_read_alloc_cb(uv_handle_t *handle, size_t suggested_size) {
    ma_ssl_proxy_handler_t *self = ma_queue_data(handle, ma_ssl_proxy_handler_t, s_socket);
    return uv_buf_init(self->downstream_buffer,sizeof(self->downstream_buffer));
}

static void s_read_cb(uv_stream_t *stream, ssize_t nread, uv_buf_t buf) {
    ma_ssl_proxy_handler_t *self = ma_queue_data(stream, ma_ssl_proxy_handler_t, s_socket);
    /* pass it on to client */
    if (nread<0) {
        uv_err_t uv_err = uv_last_error(stream->loop);
        log_uv_err(self, "in s_read_cb nread<0 ", MA_LOG_SEV_DEBUG, uv_err); 
        /* error reading from server (it probably closed the connection) */
        if (UV_EOF == uv_err.code) {
            /* graceful server close, forward that to client */
            MA_LOG(self->connection->server->logger, MA_LOG_SEV_DEBUG, "graceful close from server, now forwarding FIN to client...");
            uv_shutdown(&self->c_shutdown_req, (uv_stream_t *) &self->connection->c_socket, c_shutdown_cb);
        } else {
            ssl_proxy_begin_teardown(self);
        }
        /* in turn, */
    } else if (0<nread) {
        buf.len = nread;
        MA_LOG(self->connection->server->logger, MA_LOG_SEV_DEBUG, "ma_ssl_proxy_handler_t(%p) returning %ld bytes to client", self, (long)nread );
        uv_read_stop(stream); /* cannot read while data being sent downstream, re-enabled in c_write_cb */
        ma_http_connection_idletimer_restart(self->connection);
        uv_write(&self->c_write_req, (uv_stream_t *) &self->connection->c_socket, &buf, 1, &c_write_cb);
    } 
}

static void c_shutdown_cb(uv_shutdown_t *req, int status) {
    ma_ssl_proxy_handler_t *self = ma_queue_data(req, ma_ssl_proxy_handler_t, c_shutdown_req);
    MA_LOG(self->connection->server->logger, MA_LOG_SEV_DEBUG, "shutdown() of client completed with status %d", (int)status);
    uv_is_writable((uv_stream_t *) &self->s_socket) || ssl_proxy_begin_teardown(self);
}

/* invoked when completed sending to client */
static void c_write_cb(uv_write_t *req, int status) {
    ma_ssl_proxy_handler_t *self = ma_queue_data(req, ma_ssl_proxy_handler_t, c_write_req);
    if (status) {
        /* could not write to client */
        log_uv_err(self, "error writing to client", MA_LOG_SEV_WARNING, uv_last_error(self->s_socket.loop)); 
        ssl_proxy_begin_teardown(self);
    } else {
        /* downstream buffer is now available, start listening (again) for data from server */
        if (UV_OK != uv_read_start((uv_stream_t *)&self->s_socket, s_read_alloc_cb, s_read_cb)) {
            log_uv_err(self, "in c_write_cb() uv_read_start ", MA_LOG_SEV_WARNING, uv_last_error(self->s_socket.loop));
            ssl_proxy_begin_teardown(self);
        }
    }
}

static uv_buf_t c_read_alloc_cb(uv_handle_t *handle, size_t suggested_size) {
    ma_ssl_proxy_handler_t *self = handle->data;
    return uv_buf_init(self->upstream_buffer,sizeof(self->upstream_buffer));
}

static void s_write_cb(uv_write_t *req, int status) {
    ma_ssl_proxy_handler_t *self = ma_queue_data(req, ma_ssl_proxy_handler_t, s_write_req);
    if (status) {
        /* could not write to server */
        log_uv_err(self, "error writing to server", MA_LOG_SEV_WARNING, uv_last_error(self->s_socket.loop)); 
        ssl_proxy_begin_teardown(self);
    } else {
        /* wrote ok to server, upstream buffer available for client reads: */
        if (UV_OK != uv_read_start((uv_stream_t *) &self->connection->c_socket, c_read_alloc_cb, c_read_cb)) {
            log_uv_err(self, "in s_write_cb() uv_read_start ", MA_LOG_SEV_WARNING, uv_last_error(self->s_socket.loop));
            ssl_proxy_begin_teardown(self);
        }
    }
}

static void s_shutdown_cb(uv_shutdown_t *req, int status) {
    ma_ssl_proxy_handler_t *self = ma_queue_data(req, ma_ssl_proxy_handler_t, s_shutdown_req);
    MA_LOG(self->connection->server->logger, MA_LOG_SEV_DEBUG, "shutdown() of server completed with status %d", (int)status);

    uv_is_writable((uv_stream_t *) &self->connection->c_socket) || ssl_proxy_begin_teardown(self);
}

// there is data from client, pass it up to server
static void c_read_cb(uv_stream_t *stream, ssize_t nread, uv_buf_t buf) {
    ma_ssl_proxy_handler_t *self = stream->data;
    if (0<nread) {
        uv_read_stop(stream); /* stop reading while buffer is being sent upstream */
        ma_http_connection_idletimer_restart(self->connection);

        buf.len = nread;
        MA_LOG(self->connection->server->logger, MA_LOG_SEV_DEBUG, "ma_ssl_proxy_handler_t(%p) forwarding %ld bytes to server", self, (long)nread );
        if (MA_OK != uv_write(&self->s_write_req, (uv_stream_t *) &self->s_socket, &buf, 1, s_write_cb)) {
            log_uv_err(self, "error in c_read_cb() uv_read_start ", MA_LOG_SEV_WARNING, uv_last_error(self->s_socket.loop));
            ssl_proxy_begin_teardown(self);
        }
    } else if (nread<0) {
        uv_err_t uv_err = uv_last_error(stream->loop);
        log_uv_err(self, "in c_read_cb nread<0 ", MA_LOG_SEV_DEBUG, uv_err);
        /* error reading from server (it probably closed the connection) */
        if (UV_EOF == uv_err.code) {
            /* graceful client close: forward that to server */
            MA_LOG(self->connection->server->logger, MA_LOG_SEV_DEBUG, "graceful close from client, now forwarding FIN to server...");
            uv_shutdown(&self->s_shutdown_req, (uv_stream_t *) &self->s_socket, s_shutdown_cb);
        } else {
            ssl_proxy_begin_teardown(self);
        }
    }
}

void ssl_proxy_send_ack_response(ma_ssl_proxy_handler_t *self) {

    uv_buf_t write_data[] = {
        MA_UV_BUF_TEXT(MA_HTTP_RESPONSE_HTTP1_1), MA_UV_BUF_TEXT(MA_HTTP_RESPONSE_200_CONNECTION_ESTABLISHED), MA_UV_BUF_TEXT(MA_HTTP_CRLF),
        MA_UV_BUF_TEXT(MA_HTTP_CRLF),
    };

    if (UV_OK != uv_write(&self->c_write_req, (uv_stream_t *) &self->connection->c_socket, write_data, MA_COUNTOF(write_data), &c_write_cb)) {
        /* TODO Add more diagnostic to help troubleshoot failure */
        ssl_proxy_begin_teardown(self);
    }
}



static void ssl_proxy_connect_cb(uv_connect_t *req, int status) {
    ma_ssl_proxy_handler_t *self = ma_queue_data(req, ma_ssl_proxy_handler_t, connect_req);

    if (status) {
        self->addr_info_iter = self->addr_info_iter->ai_next;
        /* TODO may have to close the tcp handle first */
        ssl_proxy_try_connect(self);
    } else {
        /* TADA, connected. Tell client we are good to go */
        ssl_proxy_send_ack_response(self);
        /* also, start accepting more data from the client */
        self->connection->c_socket.data = self;
        if (MA_OK != uv_read_start((uv_stream_t *)&self->connection->c_socket, &c_read_alloc_cb, &c_read_cb) ) {
            uv_err_t uv_err = uv_last_error(req->handle->loop);
            log_uv_err(self, "unable to read from client, uv_read_start", MA_LOG_SEV_WARNING, uv_err);
            ssl_proxy_begin_teardown(self);
        }
    }
}


static void ssl_proxy_try_connect(ma_ssl_proxy_handler_t *self) {
    if (0 == self->addr_info_iter) {
        /* exhausted the list of resolved addresses */ 
        ssl_proxy_give_up(self);
        return;
    } 
    /* Note, we may come here multiple times as a result of a previous failed connection attempt when multiple addresses are returned from DNS */
    /* May have to close the socket first  */
    uv_tcp_init(self->connection->server->uv_loop, &self->s_socket);

    if (AF_INET6 == self->addr_info_iter->ai_family) {
        uv_tcp_connect6(&self->connect_req, &self->s_socket, *(struct sockaddr_in6 *) self->addr_info_iter->ai_addr, &ssl_proxy_connect_cb);
    } else if (AF_INET == self->addr_info_iter->ai_family) {
        uv_tcp_connect(&self->connect_req, &self->s_socket, *(struct sockaddr_in *) self->addr_info_iter->ai_addr, &ssl_proxy_connect_cb);
    } else {
        /* try next address */
        self->addr_info_iter = self->addr_info_iter->ai_next;
        ssl_proxy_try_connect(self); /* NOTE, this is a recursive call, consider unfolding it... */
    }
}

static void ma_uv_getaddrinfo_cb(uv_getaddrinfo_t *req, int status, struct addrinfo *res) {
    ma_ssl_proxy_handler_t *self = ma_queue_data(req, ma_ssl_proxy_handler_t, addr_info_req);
    if (status) {
        /* some error */
        MA_LOG(self->connection->server->logger, MA_LOG_SEV_ERROR, "uv getaddrinfo failed");
        ssl_proxy_give_up(self);
    } else {
        assert(0 == self->addr_info_begin);
        self->addr_info_iter = self->addr_info_begin = res;
        ssl_proxy_try_connect(self); /* */
    }
}

static ma_error_t ssl_proxy_start(ma_http_request_handler_t *handler, ma_http_connection_t *connection) {
    ma_ssl_proxy_handler_t *self = (ma_ssl_proxy_handler_t *) handler;
    char const *url_base = connection->request.url.base;
    char upstream_server[64], service[6] = "https";

    struct http_parser_url url_parts;
    self->connection = connection;
	self->connection->connection_type = MA_HTTP_CONNECTION_TYPE_SSL_PROXY ;

    MA_LOG(self->connection->server->logger, MA_LOG_SEV_DEBUG, "ssl proxy handler start");
    if (http_parser_parse_url(url_base, self->connection->request.url.len, 1, &url_parts) || !(url_parts.field_set & (1 << UF_HOST))) {
        /* */
        return MA_ERROR_INVALIDARG;
    }
    /* need null terminated string so make a copy (or we _could_ simply trash the source buffer and use that instead...) */
    MA_MSC_SELECT(_snprintf, snprintf)(upstream_server, MA_COUNTOF(upstream_server), "%.*s", (int) url_parts.field_data[UF_HOST].len, url_base + url_parts.field_data[UF_HOST].off);

    if (url_parts.field_set & (1 << UF_PORT)) {
        self->s_port = url_parts.port;
        MA_MSC_SELECT(_snprintf, snprintf)(service, MA_COUNTOF(service), "%.*s", (int) url_parts.field_data[UF_PORT].len, url_base + url_parts.field_data[UF_PORT].off);
    } else {
        self->s_port = 443;
    }

    if (UV_OK != uv_getaddrinfo(self->connection->server->uv_loop, &self->addr_info_req, &ma_uv_getaddrinfo_cb, upstream_server, service, 0)) {
        return MA_ERROR_INVALIDARG;
    }

    /* stop reading till we are ready  */
    uv_read_stop((uv_stream_t *) &self->connection->c_socket);

	
    return MA_OK;
}
 
static ma_error_t ssl_proxy_give_up(ma_ssl_proxy_handler_t *self) {
	ma_ssl_proxy_handler_factory_t *factory = (ma_ssl_proxy_handler_factory_t *)self->base.data ;
	
    self->connection->close_after_write = 1;
    MA_LOG(self->connection->server->logger, MA_LOG_SEV_ERROR, "ssl proxy responding with status <%s>", MA_HTTP_RESPONSE_503_SERVICE_UNAVAILABLE);
    ma_http_connection_send_stock_response(self->connection, MA_HTTP_RESPONSE_503_SERVICE_UNAVAILABLE);

	(void)ma_stats_db_update_current(factory->msgbus_db, MA_STAT_RELAY_SERVER, MA_STAT_ATTRIBUTE_FAILED_CONNECTIONS, (ma_double_t)1) ;

    return MA_OK;
}

static ma_error_t ssl_proxy_begin_teardown(ma_ssl_proxy_handler_t *self) {
	ma_ssl_proxy_handler_factory_t *factory = (ma_ssl_proxy_handler_factory_t *)self->base.data ;

    MA_LOG(self->connection->server->logger, MA_LOG_SEV_DEBUG, "initiating tear down");

	(void)ma_stats_db_update_current(factory->msgbus_db, MA_STAT_RELAY_SERVER, MA_STAT_ATTRIBUTE_FAILED_CONNECTIONS, (ma_double_t)1) ;

    return ma_http_connection_stop(self->connection);
}

static void s_socket_close_cb(uv_handle_t *handle) {
   ma_ssl_proxy_handler_t *self = ma_queue_data(handle, ma_ssl_proxy_handler_t, s_socket);
   ma_http_connection_release(self->connection);
}


static ma_error_t ssl_proxy_stop(ma_http_request_handler_t *handler) {
    ma_ssl_proxy_handler_t *self = (ma_ssl_proxy_handler_t *) handler;

    if (uv_is_active((uv_handle_t *) &self->s_socket)) uv_close((uv_handle_t *) &self->s_socket, s_socket_close_cb);
    MA_LOG(self->connection->server->logger, MA_LOG_SEV_DEBUG, "in ma_ssl_proxy_handler::stop(%p)", self);
    ma_http_connection_add_ref(self->connection); /* bump ref count so we don't get destroyed until after our s_socket_close_cb has been invoked */
    return MA_OK;
}

static void ssl_proxy_destroy(ma_http_request_handler_t *handler) {
    ma_ssl_proxy_handler_t *self = (ma_ssl_proxy_handler_t *) handler;
	
	ma_ssl_proxy_handler_factory_t *factory = (ma_ssl_proxy_handler_factory_t *)self->base.data ;
	ma_http_server_t *server = (ma_http_server_t *) factory->base.data ;
	
	--server->policies.relay_active_connections ;

    MA_LOG(self->connection->server->logger, MA_LOG_SEV_DEBUG, "freeing ma_ssl_proxy_handler(%p)", self);
    uv_freeaddrinfo(self->addr_info_begin);
    free(self);
}


static void log_uv_err(ma_ssl_proxy_handler_t *self, char const *msg, ma_log_severity_t sev, uv_err_t uv_err) {
    MA_LOG(self->connection->server->logger, sev, "%s - libuv err: (\'%s\' %s)", msg, uv_strerror(uv_err),  uv_err_name(uv_err));
}

static ma_bool_t is_it_valid_spipe_site(ma_db_t *db, uv_buf_t url) {
    ma_bool_t b_rc = MA_FALSE;
    if(db && url.base) {
        ma_error_t rc = MA_OK;
        ma_db_recordset_t *record_set = NULL;
        char upstream_server[MA_MAX_LEN] = {0};
        struct http_parser_url url_parts;
        char const *url_base = url.base;

        if (http_parser_parse_url(url_base, url.len, 1, &url_parts) || !(url_parts.field_set & (1 << UF_HOST)))
            return b_rc;
        
        /* need null terminated string so make a copy (or we _could_ simply trash the source buffer and use that instead...) */
        MA_MSC_SELECT(_snprintf, snprintf)(upstream_server, MA_COUNTOF(upstream_server), "%.*s", (int) url_parts.field_data[UF_HOST].len, url_base + url_parts.field_data[UF_HOST].off);
		if(strchr(upstream_server, ':')) {
			char *dup = strdup(upstream_server);
			memset(upstream_server, 0, MA_COUNTOF(upstream_server));
			MA_MSC_SELECT(_snprintf, snprintf)(upstream_server, MA_COUNTOF(upstream_server), "[%s]", dup);
			free(dup);
			dup = NULL;
		}
        if(MA_OK == (rc = ma_repository_db_get_url_type_by_server(db, upstream_server, &record_set))) {
             while(MA_ERROR_NO_MORE_ITEMS != ma_db_recordset_next(record_set)) {   
                int url_type = -1 ;
				ma_db_recordset_get_int(record_set, 1, &url_type);
                if(MA_REPOSITORY_URL_TYPE_SPIPE == url_type) {
                    b_rc = MA_TRUE;
                    break;
                }
            }
            (void) ma_db_recordset_release(record_set);
        }
    }
    return b_rc;
}
