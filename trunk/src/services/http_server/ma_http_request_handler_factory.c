#include "ma_http_request_handler_factory.h"
#include "ma_http_connection.h"

#include "ma_ssl_proxy_handler.h"

#include "ma/internal/ma_macros.h"

/* This is not exactly how I want it. 
 * There should most likely be a separate factory for each supported request type, each such factory can host contextual information that is (only) relevant for that type 
 */

static const struct handler_info {
    ma_bool_t (*matches)(struct ma_http_request_s const *request);

    ma_error_t (*create)(struct ma_http_connection_s *connection, ma_http_request_handler_t **handler);
} handlers[] = {
    {&ma_is_request_ssl_proxy, &ma_ssl_proxy_handler_create}
};

ma_error_t ma_http_request_handler_factory_create_instance(ma_http_request_handler_factory_t *factory, struct ma_http_connection_s *connection, ma_http_request_handler_t **handler) {
    int i;
    for (i=0; MA_COUNTOF(handlers) != i; ++i) {
        if (handlers[i].matches(&connection->request)) {
            if (MA_OK == handlers[i].create(connection, handler)) {
                return MA_OK;
            }
        }
    }

    return MA_ERROR_NOT_SUPPORTED;
}
