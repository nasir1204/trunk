
#include "ma_spipe_handler_internal.h"
#include "ma/internal/ma_macros.h"

#include "ma/internal/core/msgbus/ma_msgbus_private.h"
#include "ma/internal/clients/udp/ma_udp_request.h"
#include "ma/internal/defs/ma_udp_defs.h"
#include "ma/internal/defs/ma_object_defs.h"

#include "ma/internal/services/ma_agent_policy_settings_bag.h" 

#include <stdio.h>
#include <string.h>

#define MA_SA_WAKEUP_BRODACAST_TIMEOUT	5000
#define MA_SA_WAKEUP_MESSAGE_HEADER_FORMAT "<Type=\"AgentWakeup\" Version=\"1.0\" DataSize=\"%u\">"

void ma_sa_wakeup_request_finalize_cb(ma_error_t rc, ma_udp_request_t *request, void *user_data) {
    ma_spipe_handler_t *self = (ma_spipe_handler_t *)user_data;
  
    MA_LOG(self->connection->server->logger, MA_LOG_SEV_DEBUG, "SA wakeup broadcast request async cb response rc <%d>", rc);

    if(request) {
        ma_udp_client_t *udp_client = NULL;
        if(MA_OK == ma_udp_request_get_client(request, &udp_client)) {
            (void) ma_udp_request_release(request);
            (void) ma_udp_client_release(udp_client);
        }        
    }
    (void)ma_spipe_handler_send_response(self, MA_OK);
}

ma_error_t ma_spipe_super_agent_wakeup_handler(ma_spipe_handler_t *self) {
    ma_spipe_handler_factory_t *factory = (ma_spipe_handler_factory_t *)self->base.data;
    ma_error_t rc = MA_OK;

    char *b_need_full_props=NULL, *randomization = NULL, *server_name = NULL, *server_timestamp = NULL ;
    size_t size=0;
    
	ma_udp_client_t  *udp_client = NULL;
	ma_udp_request_t *udp_request = NULL;
	ma_udp_msg_t *udp_msg[2] = {0} ;
	ma_int32_t num_messages_set = 0 ;

    (void)ma_spipe_package_get_info(self->spipe, STR_AGENT_PING_SEND_FULL_PROPS, (const unsigned char **)&b_need_full_props, &size) ;
    (void)ma_spipe_package_get_info(self->spipe, STR_AGENT_PING_RAND_INTERVAL, (const unsigned char **)&randomization, &size) ;
	(void)ma_spipe_package_get_info(self->spipe, STR_PKGINFO_SERVER_NAME, (const unsigned char **)&server_name, &size) ;
	(void)ma_spipe_package_get_info(self->spipe, STR_PKGINFO_SERVER_TIME_STAMP, (const unsigned char **)&server_timestamp, &size) ;

    MA_LOG(self->connection->server->logger, MA_LOG_SEV_DEBUG, "Received SA wakeup call, broadcasting agent wakeup") ;

	if(b_need_full_props && server_name && server_timestamp) {
		if(MA_OK == (rc = ma_udp_client_create(&udp_client))) 
		{			
			(void)ma_udp_client_set_logger(udp_client, MA_CONTEXT_GET_LOGGER(factory->context)) ;
			(void)ma_udp_client_set_event_loop(udp_client, ma_msgbus_get_event_loop(MA_CONTEXT_GET_MSGBUS(factory->context))) ;
			(void)ma_udp_client_set_loop_worker(udp_client, ma_msgbus_get_loop_worker(MA_CONTEXT_GET_MSGBUS(factory->context))) ;

			if(MA_OK == (rc = ma_udp_request_create(udp_client, &udp_request))) 
			{
				const char *multicast_addr = MA_UDP_MULTICAST_ADDR_STR;
				ma_int32_t port = MA_UDP_DEFAULT_PORT_INT ;
				ma_policy_settings_bag_t *pso_bag = MA_CONTEXT_GET_POLICY_SETTINGS_BAG(factory->context) ;

				ma_policy_settings_bag_get_str(pso_bag, MA_UDP_SERVICE_SECTION_NAME_STR, MA_UDP_KEY_MULTICAST_ADDRESS_STR, MA_FALSE, &multicast_addr, multicast_addr) ;
				ma_policy_settings_bag_get_int(pso_bag, MA_UDP_SERVICE_SECTION_NAME_STR, MA_UDP_KEY_BROADCAST_PORT_INT, MA_FALSE,&port, port) ;

				(void)ma_udp_request_set_multicast_addr(udp_request, multicast_addr) ;
				(void)ma_udp_request_set_port(udp_request, port);
				(void)ma_udp_request_set_timeout(udp_request, MA_SA_WAKEUP_BRODACAST_TIMEOUT) ;
				(void)ma_udp_request_set_callbacks(udp_request, NULL, NULL, self) ;

				if(MA_OK == (rc = ma_udp_msg_create(&udp_msg[0]))) 
				{
					ma_message_t *msg = NULL ;

					if(MA_OK == (rc = ma_message_create(&msg))) {					
						(void)ma_message_set_property(msg, MA_UDP_MSG_KEY_TYPE_STR, MA_UDP_MSG_PROP_VAL_AGENT_WAKEUP_STR) ;
						(void)ma_message_set_property(msg, MA_UDP_MSG_KEY_AGENT_WAKEUP_NEED_FULL_PROPS_STR, b_need_full_props) ;
						(void)ma_message_set_property(msg, MA_UDP_MSG_KEY_AGENT_WAKEUP_SERVER_NAME_STR, server_name) ;
						(void)ma_message_set_property(msg, MA_UDP_MSG_KEY_AGENT_WAKEUP_SERVER_TIMESTAMP_STR, server_timestamp) ;
                        (void) ma_message_set_property(msg, MA_UDP_MSG_KEY_AGENT_WAKEUP_RANDOM_INTERVAL_STR, randomization) ;

						(void)ma_udp_msg_set_ma_message(udp_msg[0], msg) ;		++num_messages_set ;	
						(void)ma_message_release(msg);
					}
				}
				if(MA_OK == rc && MA_OK == (rc = ma_udp_msg_create(&udp_msg[1]))) 
				{
					if(self->spipe_buffer) {
						size_t spipe_buffer_len = ma_bytebuffer_get_size(self->spipe_buffer) ;
						char *header_buffer = NULL ;
						size_t header_buffer_len = strlen(MA_SA_WAKEUP_MESSAGE_HEADER_FORMAT) + 21 ;
						unsigned char *raw_buffer = NULL ;
						ma_buffer_t *buffer = NULL ;

						header_buffer = (char *)calloc( 1, sizeof(char)*header_buffer_len) ;
						MA_MSC_SELECT(_snprintf, snprintf)(header_buffer, header_buffer_len,  MA_SA_WAKEUP_MESSAGE_HEADER_FORMAT,  spipe_buffer_len) ;

						raw_buffer = (unsigned char *)calloc(1, sizeof(unsigned char)*(strlen(header_buffer)+spipe_buffer_len+1)) ;

						memcpy(raw_buffer, header_buffer, strlen(header_buffer)) ;
						memcpy(raw_buffer+strlen(header_buffer), ma_bytebuffer_get_bytes(self->spipe_buffer), spipe_buffer_len) ;

						if(MA_OK == ma_buffer_create(&buffer, strlen(header_buffer)+spipe_buffer_len)) {
							(void)ma_buffer_set(buffer, (const char *)raw_buffer , strlen(header_buffer)+spipe_buffer_len) ;
						
							(void)ma_udp_msg_set_raw_message(udp_msg[1], buffer) ;	++num_messages_set ;
						
							(void)ma_buffer_release(buffer) ;	buffer = NULL ;
						}
						free(header_buffer) ;	header_buffer = NULL ;
						free(raw_buffer) ;	raw_buffer = NULL ;
					}
				}

				if(MA_OK == rc && num_messages_set) {

					(void)ma_udp_request_set_message(udp_request, udp_msg, num_messages_set) ;
					
					if(MA_OK == (rc = ma_udp_request_async_send(udp_request, ma_sa_wakeup_request_finalize_cb))) {
						MA_LOG(self->connection->server->logger, MA_LOG_SEV_DEBUG, "Broadcasted agent wakeup request") ;		    
					}
					else {
						MA_LOG(self->connection->server->logger, MA_LOG_SEV_ERROR, "Failed send global update request, <rc = %d>.", rc) ;
					}
				}
				else {
					MA_LOG(self->connection->server->logger, MA_LOG_SEV_ERROR, "Failed to form udp messages for global update request, <rc = %d>.", rc) ;
				}

				(void)ma_udp_msg_release(udp_msg[0]) ;
				(void)ma_udp_msg_release(udp_msg[1]) ;
			}
			else {
				MA_LOG(self->connection->server->logger, MA_LOG_SEV_ERROR, "Failed to create udp request, <rc = %d>.", rc) ;
			}
		}
		else {
			MA_LOG(self->connection->server->logger, MA_LOG_SEV_ERROR, "Failed to create udp client, <rc = %d>.", rc) ;
		}
	}
	else {
		MA_LOG(self->connection->server->logger, MA_LOG_SEV_ERROR, "Invalid SA wakeup call.") ;
	}
    return rc;    
}

