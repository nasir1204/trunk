#include "ma_http_allocator.h"

#include <stdlib.h>
#include <assert.h>

struct ma_http_allocator_chunk_s {
    ma_http_allocator_chunk_t *next;          /* */
    
    char                      *begin, *end;   /* range of available space */
};

#define CHUNK_SPACE(c) ((c)->end - (c)->begin)

ma_http_allocator_t *ma_http_allocator_create(size_t pool_size) {
    /* allocate a chunk, add the pool header at the start of the chunk data */
    const size_t pool_hdr_size = sizeof(ma_http_allocator_t) + sizeof(ma_http_allocator_chunk_t);
    char *p = malloc( pool_size + pool_hdr_size );
    if (p) {
        /* chunk header at the front */
        ma_http_allocator_chunk_t *chunk = (void*) p;
        ma_http_allocator_t *a = (void*) (p + sizeof(ma_http_allocator_chunk_t));

        chunk->next = NULL;
        chunk->begin = p + pool_hdr_size;
        chunk->end =  chunk->begin + pool_size;
        
        /* followed by the allocator hdr (yes, really!), this is stored like any other data */
        /* caveat: the chunk at the end of the chain will be allocated a bit bigger then all other ones (to make room for the pool header itself) */
        a->pool_size = pool_size;
        a->chunk_head = chunk;
        a->bigblock_head = NULL;

        return a;
    }
    return NULL;
}

void ma_http_allocator_destroy(ma_http_allocator_t *allocator) {
    ma_http_allocator_chunk_t *chunk = allocator->chunk_head;
    while (chunk) {
        ma_http_allocator_chunk_t *next = chunk->next;
        free(chunk);
        chunk = next;
    }
}

void ma_http_allocator_reset(ma_http_allocator_t *allocator);

static void *ma_http_allocator_new_chunk(ma_http_allocator_t *allocator, size_t size);

void *ma_http_allocator_alloc(ma_http_allocator_t *allocator, size_t size) {
    ma_http_allocator_chunk_t *chunk;
    assert("for now, size must be no bigger than pool_size" && size<= allocator->pool_size);
    if (allocator->pool_size < size) return 0;
    for (chunk = allocator->chunk_head; chunk; chunk=chunk->next) {
        if (size <= CHUNK_SPACE(chunk)) {
            void *result = chunk->begin;
            chunk->begin += size;
            assert(chunk->begin <= chunk->end);
            return result;
        }
    }

    return ma_http_allocator_new_chunk(allocator, size);
}

void ma_http_allocator_reset(ma_http_allocator_t *allocator) {
    ma_http_allocator_chunk_t *chunk;
    for (chunk = allocator->chunk_head;chunk;chunk = chunk->next) {
        chunk->begin = (char *) chunk + sizeof(ma_http_allocator_chunk_t) + (chunk->next ? 0 : sizeof(ma_http_allocator_t));
    }
}


static void *ma_http_allocator_new_chunk(ma_http_allocator_t *allocator, size_t size) {
    /* allocate a new chunk, prepend to list */
    assert(size <= allocator->pool_size);
    if (size <= allocator->pool_size) {
        const size_t hdr_size = sizeof(ma_http_allocator_chunk_t);
        char *p = malloc(allocator->pool_size + hdr_size);
        if (p) {
        
            ma_http_allocator_chunk_t *chunk = (void *) p;
            chunk->begin = p + sizeof(ma_http_allocator_chunk_t);
            chunk->end = chunk->begin + allocator->pool_size;

            chunk->next = allocator->chunk_head;
            allocator->chunk_head = chunk;

            /* don't forget to reserve space for requested data */
            chunk->begin += size;
            return p + sizeof(ma_http_allocator_chunk_t);
        }
    }
    return 0;
}