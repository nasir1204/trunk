
#include "ma_spipe_handler_internal.h"
#include "ma/internal/ma_strdef.h"
#include "ma/ma_message.h"

static ma_error_t agent_wakeup_req_async_cb(ma_error_t status, ma_message_t *response, void *cb_data, ma_msgbus_request_t *request) {
	ma_spipe_handler_t *self = (ma_spipe_handler_t *) cb_data;

    MA_LOG(self->connection->server->logger, MA_LOG_SEV_DEBUG, "received status (%d) for agent property collection request", status);

	ma_spipe_handler_send_response(self, status);

	if((status == MA_OK) && response) ma_message_release(response);
	if(request) {
		ma_msgbus_endpoint_t * ep = NULL;
		(void) ma_msgbus_request_get_endpoint(request, &ep);		
		(void) ma_msgbus_request_release(request);
        if(ep) (void) ma_msgbus_endpoint_release(ep);
	}

	return MA_OK;
}

ma_error_t ma_spipe_agent_wakeup_handler(ma_spipe_handler_t *self) {
	ma_spipe_handler_factory_t *factory = (ma_spipe_handler_factory_t *)self->base.data ;
    ma_http_server_t *server = (ma_http_server_t *)factory->base.data;
	ma_error_t rc = MA_OK;
	ma_msgbus_endpoint_t *ep = NULL;
	ma_message_t *req_msg = NULL;	
	ma_bool_t b_needs_full_props = MA_FALSE;

    char *need_full_props=NULL, *randomization = NULL ;
    size_t size=0;
    ma_spipe_package_get_info(self->spipe, STR_AGENT_PING_SEND_FULL_PROPS, (const unsigned char **)&need_full_props, &size);
    ma_spipe_package_get_info(self->spipe, STR_AGENT_PING_RAND_INTERVAL, (const unsigned char **)&randomization, &size); /* Not using Randamization data */

    MA_LOG(self->connection->server->logger, MA_LOG_SEV_DEBUG, "Received wakeup call, sending agent property collection request");
    MA_LOG(self->connection->server->logger, MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE , "Agent wakeup call received");

    if(need_full_props && !strcmp(need_full_props, "1")){
		b_needs_full_props = MA_TRUE;
        MA_LOG(self->connection->server->logger, MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE , "Agent wakeup call for FULL PROPS received");
	}

	if(MA_OK == (rc = ma_msgbus_endpoint_create(server->msgbus, MA_PROPERTY_SERVICE_NAME_STR, NULL, &ep))) {
		(void) ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
        (void) ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
		if(MA_OK == (rc = ma_message_create(&req_msg))) {
			ma_msgbus_request_t *request = NULL;
			(void) ma_message_set_property(req_msg, MA_PROPERTY_MSG_KEY_TYPE_STR, MA_PROPERTY_MSG_TYPE_COLLECT_STR);
            (void) ma_message_set_property(req_msg, MA_PROPERTY_MSG_KEY_PROPS_TYPE_STR, b_needs_full_props 
							? MA_PROPERTY_MSG_VALUE_PROPS_TYPE_FULL_STR : MA_PROPERTY_MSG_VALUE_PROPS_TYPE_VERSION_STR);
            (void) ma_message_set_property(req_msg, MA_PROPERTY_MSG_KEY_RANDOM_INTERVALS_STR, randomization);
            (void) ma_message_set_property(req_msg, MA_PROPERTY_MSG_KEY_ENFORCE_POLICY_STR, "1") ;  /* set force enforce policy flag only for wakeup calls*/

			if(MA_OK != (rc = ma_msgbus_async_send(ep, req_msg, agent_wakeup_req_async_cb, self, &request))) {				
				(void) ma_msgbus_endpoint_release(ep);
			}	
			(void) ma_message_release(req_msg);
		}
		else
			(void) ma_msgbus_endpoint_release(ep);
	}

	if(MA_OK != rc) ma_spipe_handler_send_response(self, rc);
	return rc;
}

