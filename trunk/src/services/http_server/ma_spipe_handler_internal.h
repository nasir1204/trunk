#ifndef MA_SPIPE_HANDLER_INTERNAL_H_INCLUDED
#define MA_SPIPE_HANDLER_INTERNAL_H_INCLUDED

#include "ma/ma_common.h"

#include "ma_http_connection.h"
#include "ma/internal/services/http_server/ma_spipe_handler.h"


#include "ma/ma_msgbus.h"
#include "ma/internal/utils/macrypto/ma_crypto.h"
#include "ma/internal/utils/spipe/ma_spipe_package.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal/services/http_server/ma_http_server_utils.h"
#include "ma/internal/utils/context/ma_context.h"
#include "ma_http_server_internal.h"

MA_CPP(extern "C" {)

typedef struct ma_spipe_handler_factory_s ma_spipe_handler_factory_t, *ma_spipe_handler_factory_h;

typedef struct ma_spipe_handler_s ma_spipe_handler_t, *ma_spipe_handler_h;

struct ma_spipe_handler_factory_s {
	ma_http_request_handler_factory_t	base;

	ma_msgbus_t							*msgbus;

	ma_crypto_t							*crypto;

	ma_context_t						*context ;
};


struct ma_spipe_handler_s {
    ma_http_request_handler_t			base;

	ma_http_connection_t				*connection;

	ma_spipe_package_t					*spipe;   

    ma_stream_t                         *stream;

    size_t                              total_size;

    char                                local_full_file_path[MA_HTTP_MAX_URL_PATH_LEN];

    uv_file                             file;    

    ma_bool_t                           is_spipe_file_request; /* or else request is spipe pkg */

    ma_bool_t                           is_spipe_file_upload_request;  /* or else request for delte file */

	ma_bytebuffer_t						*spipe_buffer ;
} ;


/**
 * Individual spipe handler APIs.
 */
ma_error_t ma_spipe_agent_wakeup_handler(ma_spipe_handler_t *handler);

ma_error_t ma_spipe_super_agent_wakeup_handler(ma_spipe_handler_t *handler);

ma_error_t ma_spipe_datachannel_handler(ma_spipe_handler_t *handler);

ma_error_t ma_spipe_file_delete_handler(ma_spipe_handler_t *handler, const char *file_name);

ma_error_t ma_spipe_file_upload_handler(ma_spipe_handler_t *handler) ;

ma_error_t ma_spipe_global_update_handler(ma_spipe_handler_t *self) ;

/* Common utility functions */
void ma_spipe_handler_send_response(ma_spipe_handler_t *handler, ma_error_t status);

MA_CPP(})

#endif /* MA_SPIPE_HANDLER_INTERNAL_H_INCLUDED */
