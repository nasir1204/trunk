#ifndef MA_HTTP_REQUEST_HANDLER_FACTORIES_H_INCLUDED
#define MA_HTTP_REQUEST_HANDLER_FACTORIES_H_INCLUDED

/* represents an ordered list of request factories */

#include "ma/internal/services/http_server/ma_http_request_handler_factory.h"
#include "ma/internal/utils/datastructures/ma_map.h"
MA_CPP(extern "C" {)

#define MA_MAX_DNS_ENTRIES	128

typedef struct ma_http_request_handler_factories_s ma_http_request_handler_factories_t, *ma_http_request_handler_factories_h;

MA_MAP_DECLARE(ma_dns_resolver_map, char *, char *);
struct ma_http_request_handler_factories_s {

    ma_queue_t qhead;

	MA_MAP_T(ma_dns_resolver_map) *dns_map;
};

/**
 * initializes an already allocated factories list
 */
void ma_http_request_handler_factories_init(ma_http_request_handler_factories_t *factories);


/**
 * add a factory to the end of the list. This takes ownership in the sense that ma_http_request_handler_factories_uninit will release all such contained factories
 * do not add the same factory more than once (why would you do that anyways)!
 */
void ma_http_request_handler_factories_push_back(ma_http_request_handler_factories_t *factories, ma_http_request_handler_factory_t *factory);

/**
 * start all factories
 */
void ma_http_request_handler_factories_start(ma_http_request_handler_factories_t *factories);

/**
 * stop all factories
 */
void ma_http_request_handler_factories_stop(ma_http_request_handler_factories_t *factories);


/**
 * releases all factories
 */
void ma_http_request_handler_factories_uninit(ma_http_request_handler_factories_t *factories);



MA_CPP(})

#endif /* MA_HTTP_REQUEST_HANDLER_FACTORIES_H_INCLUDED */
