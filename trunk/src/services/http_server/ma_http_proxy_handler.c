#include "ma/internal/services/http_server/ma_http_proxy_handler.h"

#include "ma_http_connection.h"
#include "ma_http_proxy_handler_internal.h"
#include "ma_http_connection.h"

#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/utils/datastructures/ma_vector.h"
#include "ma/internal/buildinfo.h"
#include "ma/internal/utils/repository/ma_repository_db_manager.h"
#include "ma/internal/services/stats/ma_stats_db_manager.h"
#include "ma/internal/defs/ma_stats_defs.h"
#include "ma/internal/defs/ma_relay_defs.h"

#include "ma_http_server_internal.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <ctype.h>

/* from http://www.freesoft.org/CIE/RFC/2068/143.htm
hop to hop headers:
    connection
    keep-alive
    proxy-authenticate
    proxy-connection (non standard header)
    public
    transfer-encoding
    upgrade

interesting proxy headers
    via

    content-length
*/

/* HEADER_MAP will apply any user specifed macro for each below line */

#define HEADER_MAP(XX) \
    XX(0, CONNECTION, connection) \
    XX(1, KEEPALIVE, keep-alive) \
    XX(2, PROXYAUTHENTICATE, proxy-authenticate) \
    XX(3, PROXYCONNECTION, proxy-connection) \
    XX(4, PUBLIC, public) \
    XX(5, TRANSFERENCODING, transfer-encoding) \
    XX(6, UPGRADE, upgrade) \
    XX(7, VIA, via) \
    XX(8, CONTENTLENGTH, content-length)

typedef enum header_identification_e {
#define XX(x,y,z) HEADER_ID_ ## y = x,
    HEADER_MAP(XX)
#undef XX
} header_identification_t;

static struct abc {const char * name; size_t len;} header_literals[] = {
#define XX(x,y,z) {#z,sizeof(#z)-1},
    HEADER_MAP(XX)
    {0,0}
#undef XX
};

#define HEADER_ID_MASK 0x7f
#define HEADER_CAT_END2END 0x80 /* */
#define HEADER_CAT_HOP2HOP 0x90 /* Hop 2 hop header, do not forward */
#define HEADER_CAT_PROXY 0xa0 /* Designates the Via Header */

static unsigned get_header_identification(char const *header_field) {
    char const *p; 
    char c;
    int cat = 0;
    size_t index = 0;
    
    for (p=header_field; (c = tolower(*p)); ++p,++index) {
        if (0 == index) {
            switch (c) {
                case 'c' : cat = HEADER_ID_CONNECTION; break; /* or HEADER_ID_CONTENTLENGTH */
                case 'k' : cat = HEADER_ID_KEEPALIVE; break;
                case 'p' : cat = HEADER_ID_PROXYAUTHENTICATE; break; /* or HEADER_ID_PUBLIC or HEADER_ID_PROXYCONNECTION */
                case 't' : cat = HEADER_ID_TRANSFERENCODING; break;
                case 'u' : cat = HEADER_ID_UPGRADE; break;
                case 'v' : cat = HEADER_ID_VIA; break;
                default  : return HEADER_CAT_END2END; 
           }
        } else {
            char const *matcher = header_literals[cat].name;
            const size_t len = header_literals[cat].len;
            
            if (len <= index) return HEADER_CAT_END2END;

            if (matcher[index] == c) {
                ; /* Ok, continue */
            }
            else if (HEADER_ID_CONNECTION == cat) {
                if (3 == index && 't' == c) {
                    cat = HEADER_ID_CONTENTLENGTH;
                } else {
                    return HEADER_CAT_END2END;
                }
            } else if (HEADER_ID_PROXYAUTHENTICATE == cat) {
                if (1 == index && 'u' ==c) {
                    cat = HEADER_ID_PUBLIC;
                } else if (5 == index && 'c' == c) {
                    cat = HEADER_ID_PROXYCONNECTION;
                } else return HEADER_CAT_END2END;
            }
        }
    }
    return header_literals[cat].len == index 
        ? cat | ( HEADER_ID_VIA == cat ? HEADER_CAT_PROXY : HEADER_CAT_HOP2HOP) 
        : HEADER_CAT_END2END;
}



typedef struct ma_http_proxy_handler_factory_s {
    ma_http_request_handler_factory_t   base;

	ma_logger_t							*logger ;

	ma_db_t								*msgbus_db ;
    ma_http_proxy_connection_pool_t     connection_pool;
} ma_http_proxy_handler_factory_t;


static ma_bool_t http_proxy_factory_matches(ma_http_request_handler_factory_t *self, ma_http_request_t const *request);

static ma_bool_t http_proxy_factory_authorize(ma_http_request_handler_factory_t *self, ma_http_request_t const *request);

static ma_error_t http_proxy_factory_configure(ma_http_request_handler_factory_t *self);

static ma_error_t http_proxy_factory_start(ma_http_request_handler_factory_t *self) ;

static ma_error_t http_proxy_factory_stop(ma_http_request_handler_factory_t *self) ;

static ma_error_t http_proxy_factory_create_instance(ma_http_request_handler_factory_t *self, ma_http_request_handler_t **handler);

static void http_proxy_factory_destroy(ma_http_request_handler_factory_t *self);

static ma_bool_t is_it_valid_http_site(ma_db_t *db, uv_buf_t url);

static void log_uv_err(ma_http_proxy_handler_t *self, char const *msg, ma_log_severity_t sev, uv_err_t uv_err);

static ma_error_t http_proxy_give_up(ma_http_proxy_handler_t *self) ;

static ma_error_t http_proxy_begin_teardown(ma_http_proxy_handler_t *self) ;

const static ma_http_request_handler_factory_methods_t http_proxy_factory_methods = {
    &http_proxy_factory_matches,
    &http_proxy_factory_authorize,
    &http_proxy_factory_configure,
    &http_proxy_factory_create_instance,
	&http_proxy_factory_start,
	&http_proxy_factory_stop,
    &http_proxy_factory_destroy
};

/* proxy handler methods */
static ma_error_t http_proxy_start(ma_http_request_handler_t *handler, struct ma_http_connection_s *connection);

static ma_error_t http_proxy_stop(ma_http_request_handler_t *handler);

static void http_proxy_destroy(ma_http_request_handler_t *handler);

const static ma_http_request_handler_methods_t http_proxy_methods = {
    &http_proxy_start,
    &http_proxy_stop,
    &http_proxy_destroy
};

/* factory methods */
MA_HTTP_SERVER_API ma_error_t ma_http_proxy_handler_factory_create(ma_http_server_t *server, ma_http_request_handler_factory_t **http_proxy_factory) {
    ma_http_proxy_handler_factory_t *self = calloc(1, sizeof(ma_http_proxy_handler_factory_t));
    if (self) {
        ma_http_request_handler_factory_init(&self->base,&http_proxy_factory_methods,server);       
        self->logger = server->logger;
        *http_proxy_factory = &self->base;
        return MA_OK;
    }
    return MA_ERROR_OUTOFMEMORY;
}

MA_HTTP_SERVER_API ma_error_t ma_http_proxy_handler_factory_set_event_loop(ma_http_request_handler_factory_t *http_proxy_factory, struct ma_event_loop_s *ma_loop) {
    ma_http_proxy_handler_factory_t *self = (ma_http_proxy_handler_factory_t *)http_proxy_factory;
    if (self) {
        ma_http_proxy_connection_pool_init(&self->connection_pool, ma_event_loop_get_uv_loop(ma_loop) );
    }
    return MA_OK;
}

MA_HTTP_SERVER_API ma_error_t  ma_http_proxy_handler_factory_logger(ma_http_request_handler_factory_t *factory, ma_logger_t *logger) {
	ma_http_proxy_handler_factory_t *self = (ma_http_proxy_handler_factory_t *)factory;
    return (self && (self->logger = logger)) ? MA_OK : MA_ERROR_INVALIDARG;
}

MA_HTTP_SERVER_API ma_error_t ma_http_proxy_handler_factory_set_msgbus_db(ma_http_request_handler_factory_t *factory, ma_db_t *db) {
	ma_http_proxy_handler_factory_t *self = (ma_http_proxy_handler_factory_t *)factory;
    return (self && (self->msgbus_db = db)) ? MA_OK : MA_ERROR_INVALIDARG;
}

static ma_bool_t http_proxy_factory_matches(ma_http_request_handler_factory_t *factory, ma_http_request_t const *request) {
    /* proxy requests all have urls starting with http:// */
    return 0 == strncmp(request->url.base,"http://",7);
}

static ma_bool_t http_proxy_factory_authorize(ma_http_request_handler_factory_t *factory, ma_http_request_t const *request) {
    ma_http_proxy_handler_factory_t *self = (ma_http_proxy_handler_factory_t *) factory;
    ma_http_server_t *server = (ma_http_server_t *) self->base.data;

	if(server->policies.is_relay_enabled && is_it_valid_http_site(server->db, request->url)) {
		if(server->policies.relay_active_connections <= server->policies.relay_concurrent_connections_limit) {
			return MA_TRUE ;
		}
		else {
			(void)ma_stats_db_update_current(self->msgbus_db, MA_STAT_RELAY_SERVER, MA_STAT_ATTRIBUTE_SURGED_CONNECTIONS, (ma_double_t)1) ;
			MA_LOG(self->logger, MA_LOG_SEV_WARNING, "http proxy/relay exceeds max concurrent connections limit.") ;
		}
	}
	else 
		MA_LOG(self->logger, MA_LOG_SEV_WARNING, "http proxy/relay is disabled or not a valid http site.") ;

    return MA_FALSE ;
}

static ma_error_t http_proxy_factory_configure(ma_http_request_handler_factory_t *factory) {
    ma_http_proxy_handler_factory_t *self = (ma_http_proxy_handler_factory_t *) factory;   
    return MA_OK;
}

static ma_error_t http_proxy_factory_start(ma_http_request_handler_factory_t *factory) {
	ma_http_proxy_handler_factory_t *self = (ma_http_proxy_handler_factory_t *)factory ;

	if(self)
		MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "http proxy/relay handler started.") ;

	return MA_OK ;
}

static ma_error_t http_proxy_factory_stop(ma_http_request_handler_factory_t *factory) {
	ma_http_proxy_handler_factory_t *self = (ma_http_proxy_handler_factory_t *)factory ;

	if(self) {
		MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "http proxy/relay handler stoped.") ;
		ma_http_proxy_connection_pool_clear(&self->connection_pool);
	}

	return MA_OK ;
}

static ma_error_t http_proxy_factory_create_instance(ma_http_request_handler_factory_t *factory, ma_http_request_handler_t **handler) {
    ma_http_proxy_handler_factory_t *self = (ma_http_proxy_handler_factory_t *) factory;

    ma_http_proxy_handler_t *proxy_handler = (ma_http_proxy_handler_t *)calloc(1, sizeof(ma_http_proxy_handler_t));
    if (proxy_handler) {
        ma_http_server_t *server = (ma_http_server_t *) self->base.data ;

        ma_http_request_handler_init(&proxy_handler->base, &http_proxy_methods, self);
        proxy_handler->pool = &self->connection_pool;
        *handler = &proxy_handler->base;

        ++server->policies.relay_active_connections;

        return MA_OK;
    }
    return MA_ERROR_OUTOFMEMORY;
}

static void http_proxy_factory_destroy(ma_http_request_handler_factory_t *http_proxy_factory) {
    ma_http_proxy_handler_factory_t *self = (ma_http_proxy_handler_factory_t *)http_proxy_factory;    
    free(self);
}


/******** http_parser event handlers *******/
static int on_message_begin(http_parser*);
static int on_url(http_parser*, const char *at, size_t length);
static int on_status_complete(http_parser*);
static int on_header_field(http_parser*, const char *at, size_t length);
static int on_header_value(http_parser*, const char *at, size_t length);
static int on_headers_data(http_parser *parser, const char *at, size_t length);
static int on_headers_complete(http_parser*);
static int on_body(http_parser*, const char *at, size_t length);
static int on_message_complete(http_parser*);

static const http_parser_settings response_parser_methods = {
    &on_message_begin,
    &on_url,
    &on_status_complete,
    &on_header_field,
    &on_header_value,
    &on_headers_data,
    &on_headers_complete,
    &on_body,
    &on_message_complete,
};

static void start_send_if_ready(ma_http_proxy_handler_t *self);

static int parser_on_body(http_parser *parser, const char *at, size_t length) {
    //ma_http_proxy_handler_t *self = parser->data;

   /* TBD - if (!self->req_body) {
        if (MA_OK != ma_bytebuffer_create(length, &self->req_body)) {
            return -1;
        }
    } else {
        if (MA_OK != ma_bytebuffer_reserve(self->req_body, length + ma_bytebuffer_get_size(self->req_body))) {
            return -1;
        }
    }

    ma_bytebuffer_append_bytes(self->req_body, (const unsigned char*) at, length);

    */
    return 0;
}

static uv_buf_t ps_read_alloc_cb(uv_handle_t *handle, size_t suggested_size) {
    ma_http_proxy_upstream_connection_t *upstream_connection = handle->data;
    /* memset(&upstream_connection->upstream_buffer, 0, sizeof(upstream_connection->upstream_buffer));  TBD - Let me enable when needed */
    return uv_buf_init(upstream_connection->upstream_buffer,sizeof(upstream_connection->upstream_buffer));
}

/* This is almost the same code as ma_http_connection, can we consolidate? */
static void ps_read_cb(uv_stream_t *stream, ssize_t nread, uv_buf_t buf) {
    ma_http_proxy_upstream_connection_t *upstream_connection = stream->data;
    ma_http_proxy_handler_t *self = (ma_http_proxy_handler_t *)upstream_connection->owner;

    ma_bool_t should_parse = MA_FALSE;

    /* this is data coming back from server to be forwarded to client (after being filtered)*/
    if (0<nread) {
        should_parse = MA_TRUE;
    } else if (nread<0) {
        /* uv_err_t uv_err = uv_last_error(stream->loop); */
        /* error reading from server (it probably closed the connection) */
        if(self) {
            MA_LOG(self->downstream_connection->server->logger, MA_LOG_SEV_DEBUG, "Upstream connection terminated");
            (void)http_proxy_give_up(self) ;
        }

        ma_http_proxy_connection_pool_remove(upstream_connection->pool, upstream_connection);
    }

    if (should_parse && self) {        
        MA_LOG(self->downstream_connection->server->logger, MA_LOG_SEV_DEBUG, "Start http parse execute on data received from upstream server");
        http_parser_execute(&self->response.parser, &response_parser_methods, buf.base, 0<nread ? nread : 0);
    }
}


static void ps_write_cb(uv_write_t *req, int status) {
    ma_http_proxy_handler_t *self = req->data;
    MA_LOG(self->downstream_connection->server->logger, MA_LOG_SEV_DEBUG, "in ps_write_cb, status = %d", status);
    if (status) {
        (void)http_proxy_give_up(self) ;
    } else {
        self->upstream_connection->ps_socket->data = self->upstream_connection;
        http_parser_init(&self->response.parser, HTTP_RESPONSE);
        self->response.parser.data = self;
        uv_read_start((uv_stream_t *)self->upstream_connection->ps_socket, ps_read_alloc_cb, ps_read_cb);
    }
}

#define MA_HTTP_PROXY_VIA_IDENT ", 1.1 " "hostname" " (" MA_VERSION_PRODUCTNAME " Relay/" MA_VERSION_STRING ")"

typedef struct ma_uv_buffer_list_s {
    ma_vector_define(y,uv_buf_t);
} ma_uv_buffer_list_t;

/* empty function for vector clear API */
void buf_release(uv_buf_t buf) {
    
}

static void start_send_if_ready(ma_http_proxy_handler_t *self) {
    if (self->has_full_request && self->upstream_connection) {
        ma_queue_t *qitem;
        /* NOTE Below code is for first communication where the full request is sent to upstream server */ 

        /* start forward the request to server */
        /* somewhat kludgy code to forward request to server */
        const char *method = http_method_str(self->downstream_connection->request.parser.method);
        uv_buf_t method_buf = MA_UV_BUF_STRING(method), url_buf = MA_UV_BUF_STRING(self->url_path);
        ma_bool_t need_via_header = MA_TRUE;

        ma_uv_buffer_list_t uv_buffers;
        ma_vector_init(&uv_buffers, 24, y, uv_buf_t);

        /* POST <url> HTTP/1.1<CR><LF>*/
        ma_vector_push_back(&uv_buffers, uv_buf_init( (char *) method, strlen(method)), uv_buf_t, y);
        ma_vector_push_back(&uv_buffers, uv_buf_init(" ",1), uv_buf_t, y);
        ma_vector_push_back(&uv_buffers, uv_buf_init( (char *) self->url_path, strlen(self->url_path)), uv_buf_t, y);
        ma_vector_push_back(&uv_buffers, uv_buf_init(" ",1), uv_buf_t, y);
        ma_vector_push_back(&uv_buffers, uv_buf_init(MA_HTTP_RESPONSE_HTTP1_1, sizeof(MA_HTTP_RESPONSE_HTTP1_1)-2 /* -1 for trailing space, -1 for zero terminator */), uv_buf_t, y);
        ma_vector_push_back(&uv_buffers, uv_buf_init(MA_HTTP_CRLF,2), uv_buf_t, y);

        /* now transfer headers, filtering out any hop 2 hop headers */
        ma_queue_foreach(qitem,&self->downstream_connection->request.headers) {
            ma_http_header_t *hdr = ma_queue_data(qitem, ma_http_header_t, qlink);
            unsigned hdr_ident  = get_header_identification(hdr->field.base);
            if (hdr_ident & (HEADER_CAT_END2END|HEADER_CAT_PROXY)) {
                size_t elems_required = 6 + ma_vector_get_size(&uv_buffers, y);
                ma_vector_reserve(&uv_buffers, elems_required, y, uv_buf_t);

                ma_vector_push_back(&uv_buffers, hdr->field, uv_buf_t, y);
                ma_vector_push_back(&uv_buffers, uv_buf_init(": ",2), uv_buf_t, y);
                ma_vector_push_back(&uv_buffers, hdr->value, uv_buf_t, y);
                if (HEADER_ID_VIA == (hdr_ident & HEADER_ID_MASK)) {
                    // append to existing via header
                    ma_vector_push_back(&uv_buffers, uv_buf_init(MA_HTTP_PROXY_VIA_IDENT,sizeof(MA_HTTP_PROXY_VIA_IDENT)-1), uv_buf_t, y);
                    need_via_header = MA_FALSE;
                }
                ma_vector_push_back(&uv_buffers, uv_buf_init(MA_HTTP_CRLF,2), uv_buf_t, y);
            } 
        }

        if (need_via_header) {
            ma_vector_push_back(&uv_buffers, uv_buf_init(MA_HTTP_RESPONSE_HEADER_FIELD_VIA,sizeof(MA_HTTP_RESPONSE_HEADER_FIELD_VIA)-1), uv_buf_t, y);
            ma_vector_push_back(&uv_buffers, uv_buf_init((char *)&MA_HTTP_PROXY_VIA_IDENT[2],sizeof(MA_HTTP_PROXY_VIA_IDENT)-1-2), uv_buf_t, y);
            ma_vector_push_back(&uv_buffers, uv_buf_init(MA_HTTP_CRLF,2), uv_buf_t, y);
        }
        /* TODO Add content-length */
        //ma_vector_push_back(&uv_buffers, uv_buf_init(MA_HTTP_RESPONSE_HEADER_FIELD_CONTENT_LENGTH,sizeof(MA_HTTP_RESPONSE_HEADER_FIELD_CONTENT_LENGTH)-1), uv_buf_t, y);

        //ma_vector_push_back(&uv_buffers, uv_buf_init(MA_HTTP_CRLF,2), uv_buf_t, y);

        /* Add CRLF after all headers */
        ma_vector_push_back(&uv_buffers, uv_buf_init(MA_HTTP_CRLF,2), uv_buf_t, y);

        //ma_vector_push_back(&uv_buffers, uv_buf_init(ma_bytebuffer_get_bytes(self->req_body), ma_bytebuffer_get_size(self->req_body)), uv_buf_t, y);

        self->ps_write_req.data = self;
        if (UV_OK != uv_write(&self->ps_write_req, (uv_stream_t*) self->upstream_connection->ps_socket, ma_vector_get_ptr(&uv_buffers, y), ma_vector_get_size(&uv_buffers, y) , &ps_write_cb)) {
            (void)http_proxy_give_up(self) ;
        }

        ma_vector_clear(&self->response, uv_buf_t, response, buf_release);
    }
}

static int parser_on_message_complete (http_parser *parser) {
    ma_http_proxy_handler_t *self = parser->data; 
    MA_LOG(self->downstream_connection->server->logger, MA_LOG_SEV_DEBUG, "downstream data message complete");
    self->has_full_request = 1;  
    start_send_if_ready(self);
    return 0;
}


static void pool_req_cb(ma_http_proxy_connection_pool_request_t *pool_req, int status, ma_http_proxy_upstream_connection_t *connection) {
    ma_http_proxy_handler_t *self = pool_req->data; 
    MA_LOG(self->downstream_connection->server->logger, MA_LOG_SEV_DEBUG, "in pool_req_cb, status = %d", status);
    if (status) {
        /* could not get a connection, let's go tell the client */
        (void)http_proxy_give_up(self) ;
    } else {
        /* grab the connection and start communicating */
        assert(connection);
        ma_http_proxy_upstream_connection_set_owner(connection, self);
        self->upstream_connection = connection;
        start_send_if_ready(self);
    }
}

static ma_error_t http_proxy_start(ma_http_request_handler_t *handler, ma_http_connection_t *connection) {
    ma_http_proxy_handler_t *self = (ma_http_proxy_handler_t *) handler;
    char const *url_base = connection->request.url.base;
    ma_http_server_info_t si;
    struct http_parser_url url_parts;
	ma_error_t rc = MA_OK ;

    self->downstream_connection = connection;
	self->downstream_connection->connection_type = MA_HTTP_CONNECTION_TYPE_PROXY ;
	

    MA_LOG(connection->server->logger, MA_LOG_SEV_DEBUG, "in http_proxy_start::start(%p)", self);
    /* factory ensures we only get here for urls starting with http:// so no need to check that again */
    if (http_parser_parse_url(url_base, self->downstream_connection->request.url.len, 0, &url_parts) || !(url_parts.field_set & (1 << UF_HOST))) {
        /* */
        return MA_ERROR_INVALIDARG;
    }
    /* need null terminated string for hostname */
    MA_MSC_SELECT(_snprintf, snprintf)(si.hostname, MA_COUNTOF(si.hostname), "%.*s", (int) url_parts.field_data[UF_HOST].len, url_base + url_parts.field_data[UF_HOST].off);

    /* no need to copy remainder of url, it's already null terminated (and memory valid for the duration of this request) */
    self->url_path = (url_parts.field_set & (1 << UF_HOST)) ? url_base + url_parts.field_data[UF_PATH].off : "/";

    MA_LOG(connection->server->logger, MA_LOG_SEV_DEBUG, "http proxy received request for file <%s>, downstream client <%s>, upstream server <%s>", self->url_path, connection->peer_addr, si.hostname);

    si.port = (url_parts.field_set & (1 << UF_PORT)) ? url_parts.port : 80;
	
	ma_http_proxy_connection_pool_request_init(&self->pool_req, self->pool);
    self->pool_req.data = self;

	/* start a request, see you in pool_req_cb() */
	if(MA_OK == (rc = ma_http_proxy_connection_pool_request_start(&self->pool_req, &si, &pool_req_cb))) {

		/* take over request parser event handler so we can get the rest of the request  */

		self->downstream_connection->parser_callbacks.on_body = &parser_on_body;

		self->downstream_connection->request.parser.data = self;

		self->downstream_connection->parser_callbacks.on_message_complete = &parser_on_message_complete;

	}

	return rc;
}

static ma_error_t http_proxy_stop(ma_http_request_handler_t *handler) {
    ma_http_proxy_handler_t *self = (ma_http_proxy_handler_t *) handler;

    MA_LOG(self->downstream_connection->server->logger, MA_LOG_SEV_DEBUG, "in http_proxy_stop::stop(%p)", self);
    ma_http_proxy_connection_pool_request_stop(&self->pool_req);
    if(self->upstream_connection) ma_http_proxy_upstream_connection_stop(self->upstream_connection);
    return MA_OK;
}

static void http_proxy_destroy(ma_http_request_handler_t *handler) {
    ma_http_proxy_handler_t *self = (ma_http_proxy_handler_t *) handler;

	ma_http_proxy_handler_factory_t *factory = (ma_http_proxy_handler_factory_t *)self->base.data ;
	ma_http_server_t *server = (ma_http_server_t *) factory->base.data ;
	
	--server->policies.relay_active_connections ;

    MA_LOG(self->downstream_connection->server->logger, MA_LOG_SEV_DEBUG, "in http_proxy_destroy::detroy(%p)", self);
    free(self);
}

static const char *get_http_status_text(unsigned short status_code) {
    switch(status_code) {
    case(200):
        return MA_HTTP_RESPONSE_200_OK;
        break;
    case(202):
        return MA_HTTP_RESPONSE_202_OK;
        break;
    case(400):
        return MA_HTTP_RESPONSE_400_BAD_REQUEST;
        break;
    case(401):
        return MA_HTTP_RESPONSE_401_UNAUTHORIZED;
        break;
    case(403):
        return MA_HTTP_RESPONSE_403_FORBIDDEN;
        break;
    case(404):
        return MA_HTTP_RESPONSE_404_NOT_FOUND;
        break;
    case(405):
        return MA_HTTP_RESPONSE_405_METHOD_NOT_ALLOWED;
        break;
    case(500):
        return MA_HTTP_RESPONSE_500_INTERNAL_SERVICE;
        break;
    case(503):
        return MA_HTTP_RESPONSE_500_INTERNAL_SERVICE;
        break;
    default:     /* TBD - default is 503 if this API failed to find the status code ??? */
        return MA_HTTP_RESPONSE_503_SERVICE_UNAVAILABLE;
        break;
    }
}

/******** http_parser event handlers *******/
static int on_message_begin(http_parser *parser) {
    ma_http_proxy_handler_t *self = ma_queue_data(parser, ma_http_proxy_handler_t, response);  
    ma_vector_init(&self->response, 24, response, uv_buf_t);
    self->response.is_content_header = MA_FALSE;
    return 0;
}

static int on_url(http_parser *parser, const char *at, size_t length)  {
    return 0;
}

static int on_status_complete(http_parser *parser)  {    
    ma_http_proxy_handler_t *self = ma_queue_data(parser, ma_http_proxy_handler_t, response);
    const char *status_text = get_http_status_text(parser->status_code);    

    ma_vector_push_back(&self->response, uv_buf_init(MA_HTTP_RESPONSE_HTTP1_1, sizeof(MA_HTTP_RESPONSE_HTTP1_1)-1), uv_buf_t, response);
    ma_vector_push_back(&self->response, uv_buf_init((char *)status_text, strlen(status_text)), uv_buf_t, response);
    ma_vector_push_back(&self->response, uv_buf_init(MA_HTTP_CRLF,2), uv_buf_t, response);  

    return 0;
}

static int on_header_field(http_parser *parser, const char *at, size_t length)  {
    /*  header fields do not include the ':' delimiter */
    ma_http_proxy_handler_t *self = ma_queue_data(parser, ma_http_proxy_handler_t, response);
    ma_vector_push_back(&self->response, uv_buf_init((char *)at, length), uv_buf_t, response);
    ma_vector_push_back(&self->response, uv_buf_init(": ",2), uv_buf_t, response);    
    self->response.is_content_header = !strncmp(at, MA_HTTP_SERVER_CONTENT_LENGTH_HEADER_NAME_STR, sizeof(MA_HTTP_SERVER_CONTENT_LENGTH_HEADER_NAME_STR)-1);
    return 0;
}

static int on_header_value(http_parser *parser, const char *at, size_t length)  {
    ma_http_proxy_handler_t *self = ma_queue_data(parser, ma_http_proxy_handler_t, response);
    ma_vector_push_back(&self->response, uv_buf_init((char *)at, length), uv_buf_t, response);
    ma_vector_push_back(&self->response, uv_buf_init(MA_HTTP_CRLF,2), uv_buf_t, response);
    if(self->response.is_content_header) {
        char str[16] = {0};
        strncpy(str, at, length);
        self->response.content_length = atoi(str);
        MA_LOG(self->downstream_connection->server->logger, MA_LOG_SEV_DEBUG, "content length received from upstream server = %d", self->response.content_length);
    }
    return 0;
}

static int on_headers_data(http_parser *parser, const char *at, size_t length) {
    return 0;
}

static void client_write_cb(uv_write_t *req, int status) {
    ma_http_connection_t *conn = ma_queue_data(req, ma_http_connection_t, c_write_req);
    ma_http_proxy_handler_t *self = (ma_http_proxy_handler_t *)conn->cur_req_handler; 
    MA_LOG(self->downstream_connection->server->logger, MA_LOG_SEV_DEBUG, "client_write_cb, status = %d", status);

    if (UV_OK == status) {                
        if(self->response.content_length == self->response.received_data) {
            ma_http_proxy_upstream_connection_set_owner(self->upstream_connection, NULL);                     
            ma_http_connection_stop(conn);
        }
        else {
            ma_http_connection_idletimer_restart(conn);    
            self->upstream_connection->ps_socket->data = self->upstream_connection;
        }
        uv_read_start((uv_stream_t *)self->upstream_connection->ps_socket, ps_read_alloc_cb, ps_read_cb);

    } else {
        /* TODO Add diagnostic */
        (void)http_proxy_begin_teardown(self) ;
    }
}

static int on_headers_complete(http_parser *parser) {
    ma_http_proxy_handler_t *self = ma_queue_data(parser, ma_http_proxy_handler_t, response);  
    MA_LOG(self->downstream_connection->server->logger, MA_LOG_SEV_DEBUG, "headers complete from upstream socket");
    if(!ma_vector_empty(&self->response, response)) {
        ma_vector_push_back(&self->response, uv_buf_init(MA_HTTP_CRLF,2), uv_buf_t, response);      
        self->response.has_headers = MA_TRUE;
    }
      
    return 0;
}

static int on_body(http_parser *parser, const char *at, size_t length)  {
    ma_http_proxy_handler_t *self = ma_queue_data(parser, ma_http_proxy_handler_t, response);
    if(!self->response.has_headers) {
        ma_vector_init(&self->response, 1, response, uv_buf_t);
    }
    self->response.has_headers = MA_FALSE;
    ma_vector_push_back(&self->response, uv_buf_init((char *)at, length), uv_buf_t, response);

    self->response.received_data += length;
    MA_LOG(self->downstream_connection->server->logger, MA_LOG_SEV_DEBUG, "sending data to client, length = %d, remaining bytes = %d", length,  self->response.content_length - self->response.received_data);

    uv_read_stop((uv_stream_t *)self->upstream_connection->ps_socket);

    self->downstream_connection->c_write_req.data = self;
    MA_LOG(self->downstream_connection->server->logger, MA_LOG_SEV_DEBUG, "Writing data on downstream socket");
    if (UV_OK != uv_write(&self->downstream_connection->c_write_req, (uv_stream_t *) &self->downstream_connection->c_socket, ma_vector_get_ptr(&self->response, response), ma_vector_get_size(&self->response, response), &client_write_cb)) {
        uv_err_t uv_err = uv_last_error(self->downstream_connection->server->uv_loop);
        log_uv_err(self, "uv write failed on downstream data", MA_LOG_SEV_DEBUG, uv_err);
        (void)http_proxy_begin_teardown(self) ;
    }

    ma_vector_clear(&self->response, uv_buf_t, response, buf_release);
    return 0;
}

static int on_message_complete(http_parser *parser)  {
    ma_http_proxy_handler_t *self = ma_queue_data(parser, ma_http_proxy_handler_t, response);    
    MA_LOG(self->downstream_connection->server->logger, MA_LOG_SEV_DEBUG, "message complete from upstream socket");
    return 0;
}

static ma_bool_t is_it_valid_http_site(ma_db_t *db, uv_buf_t url) {
    ma_bool_t b_rc = MA_FALSE;
    if(db && url.base) {
        ma_error_t rc = MA_OK;
        char upstream_server[MA_MAX_LEN] = {0};
        ma_db_recordset_t *record_set = NULL;
        struct http_parser_url url_parts;
        char const *url_base = url.base;

        if(http_parser_parse_url(url_base, url.len, 0, &url_parts) || !(url_parts.field_set & (1 << UF_HOST)))
            return b_rc;
        
        MA_MSC_SELECT(_snprintf, snprintf)(upstream_server, MA_COUNTOF(upstream_server), "%.*s", (int) url_parts.field_data[UF_HOST].len, url_base + url_parts.field_data[UF_HOST].off);
		if(strchr(upstream_server, ':')) {
			char *dup = strdup(upstream_server);
			memset(upstream_server, 0, MA_COUNTOF(upstream_server));
			MA_MSC_SELECT(_snprintf, snprintf)(upstream_server, MA_COUNTOF(upstream_server), "[%s]", dup);
			free(dup);
			dup = NULL;
		}
        if(MA_OK == (rc = ma_repository_db_get_url_type_by_server(db, upstream_server, &record_set))) {
             while(MA_ERROR_NO_MORE_ITEMS != ma_db_recordset_next(record_set)) {   
                int url_type = -1 ;
				ma_db_recordset_get_int(record_set, 1, &url_type);
                if(MA_REPOSITORY_URL_TYPE_HTTP == url_type || MA_REPOSITORY_URL_TYPE_SPIPE == url_type || MA_REPOSITORY_URL_TYPE_SA == url_type) {
                    b_rc = MA_TRUE;
                    break;
                }
            }
            (void) ma_db_recordset_release(record_set);
        }
    }
    return b_rc;
}

static void log_uv_err(ma_http_proxy_handler_t *self, char const *msg, ma_log_severity_t sev, uv_err_t uv_err) {
    MA_LOG(self->downstream_connection->server->logger, sev, "%s - libuv err: (\'%s\' %s)", msg, uv_strerror(uv_err),  uv_err_name(uv_err));
}

static ma_error_t http_proxy_give_up(ma_http_proxy_handler_t *self) {
	ma_http_proxy_handler_factory_t *factory = (ma_http_proxy_handler_factory_t *)self->base.data ;

    self->downstream_connection->close_after_write = 1;
    MA_LOG(self->downstream_connection->server->logger, MA_LOG_SEV_ERROR, "http proxy responding with status <%s>", MA_HTTP_RESPONSE_503_SERVICE_UNAVAILABLE);
    ma_http_connection_send_stock_response(self->downstream_connection, MA_HTTP_RESPONSE_503_SERVICE_UNAVAILABLE);

	(void)ma_stats_db_update_current(factory->msgbus_db, MA_STAT_RELAY_SERVER, MA_STAT_ATTRIBUTE_FAILED_CONNECTIONS, (ma_double_t)1) ;
	
    return MA_OK;
}

static ma_error_t http_proxy_begin_teardown(ma_http_proxy_handler_t *self) {
	ma_http_proxy_handler_factory_t *factory = (ma_http_proxy_handler_factory_t *)self->base.data ;

    MA_LOG(self->downstream_connection->server->logger, MA_LOG_SEV_DEBUG, "initiating tear down");

	(void)ma_stats_db_update_current(factory->msgbus_db, MA_STAT_RELAY_SERVER, MA_STAT_ATTRIBUTE_FAILED_CONNECTIONS, (ma_double_t)1) ;

    return ma_http_connection_stop(self->downstream_connection);
}
