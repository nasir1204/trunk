#ifndef MA_HTTP_REPOSITORY_HANDLER_INTERNAL_H_INCLUDED
#define MA_HTTP_REPOSITORY_HANDLER_INTERNAL_H_INCLUDED

#include "ma/ma_common.h"

#include "ma_http_connection.h"
#include "ma_http_server_internal.h"

#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal/services/http_server/ma_http_server_utils.h"

#include "ma/repository/ma_repository_client.h"
#include "ma/internal/utils/context/ma_context.h"

MA_CPP(extern "C" {)

typedef struct ma_http_repository_handler_factory_s ma_http_repository_handler_factory_t, *ma_http_repository_handler_factory_h;

typedef struct ma_http_repository_handler_s ma_http_repository_handler_t, *ma_http_repository_handler_h;

/* Http repository handler Factory */
struct ma_http_repository_handler_factory_s {
	ma_http_request_handler_factory_t	    base;	

    uv_loop_t                               *uv_loop;

	ma_context_t							*context;	

    ma_logger_t                             *logger;

    ma_repository_list_t                    *repository_list;

    ma_proxy_config_t                       *proxy_config;

	ma_uint32_t								active_connections;
} ;


#define MA_HTTP_SAHU_SEPARATOR_STR          "|"
#define MA_HTTP_SAHU_HEADER_STR			    "SAHU"

MA_CPP(})

#endif /* MA_HTTP_REPOSITORY_HANDLER_INTERNAL_H_INCLUDED */


