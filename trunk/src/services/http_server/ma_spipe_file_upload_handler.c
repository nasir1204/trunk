
#include "ma_spipe_handler_internal.h"
#include "ma/internal/services/http_server/ma_http_server_utils.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal/utils/filesystem/ma_fs_utils.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/defs/ma_object_defs.h"

#include "ma/internal/utils/time/ma_time.h"
#include "ma/internal/services/p2p/ma_p2p_db_manager.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/utils/configurator/ma_configurator.h"

#include <stdio.h>
#include <fcntl.h>

static void get_sitestate_info(ma_spipe_handler_t *self) {
	ma_spipe_handler_factory_t *factory = (ma_spipe_handler_factory_t *)self->base.data ;
    ma_http_server_t *server = (ma_http_server_t *)factory->base.data;
	
	ma_temp_buffer_t buffer = MA_TEMP_BUFFER_INIT;
	size_t buffer_size = 0;

	if(ma_fs_get_data(server->uv_loop, self->local_full_file_path, NULL, &buffer_size)) {
		ma_temp_buffer_reserve(&buffer, buffer_size);
		if(ma_fs_get_data(server->uv_loop, self->local_full_file_path, (unsigned char *)ma_temp_buffer_get(&buffer), &buffer_size)) {
			(void) ma_http_server_update_sitestat_info(server, (const char *)ma_temp_buffer_get(&buffer), buffer_size);
		}
	}

	ma_temp_buffer_uninit(&buffer);
}

ma_error_t ma_spipe_file_upload_handler(ma_spipe_handler_t *self) {
	ma_spipe_handler_factory_t *factory = (ma_spipe_handler_factory_t *)self->base.data ;
    ma_http_server_t *server = (ma_http_server_t *)factory->base.data;
    ma_error_t rc = MA_OK;
	ma_bool_t is_hash_matched = MA_FALSE;    
    uv_buf_t hash_buf = uv_buf_init(0,0), hash256_buf = uv_buf_init(0,0);
    
    is_hash_matched = ma_http_request_search_header(&self->connection->request, MA_HTTP_SERVER_FILE_HASH_HEADER_NAME_STR, &hash_buf) &&
                      verify_file_hash(self->connection->server->uv_loop, factory->crypto, self->local_full_file_path, MA_CRYPTO_HASH_SHA1, hash_buf);
                
    if(!is_hash_matched)
        is_hash_matched = ma_http_request_search_header(&self->connection->request, MA_HTTP_SERVER_FILE_HASH256_HEADER_NAME_STR, &hash256_buf) &&
                            verify_file_hash(self->connection->server->uv_loop, factory->crypto, self->local_full_file_path, MA_CRYPTO_HASH_SHA256, hash256_buf);

    if(!is_hash_matched) {
        uv_fs_t req;
        uv_fs_unlink(self->connection->server->uv_loop, &req, self->local_full_file_path, NULL);
        uv_fs_req_cleanup(&req);
        MA_LOG(MA_CONTEXT_GET_LOGGER(factory->context), MA_LOG_SEV_WARNING, "Upload file <%s> Hash verification failed", self->local_full_file_path);
        ma_spipe_handler_send_response(self, MA_OK); /* TBD - Sending OK on HASH verification failure ??? */
    }
    else {
        if(is_request_for_sitestat_xml(self->local_full_file_path)) {
			ma_bool_t last_sitestat_status = server->is_sitestat_enabled;
			(void) get_sitestate_info(self);

			/* When sitestat state changed from disabled to enabled, flush the cache */
			if(server->policies.is_lazy_cache_enabled && (MA_FALSE == last_sitestat_status) && (last_sitestat_status != server->is_sitestat_enabled))
				(void) ma_http_server_repo_cache_flush(server);

            MA_LOG(MA_CONTEXT_GET_LOGGER(factory->context), MA_LOG_SEV_DEBUG, "Sitestat current status <%s>", (self->connection->server->is_sitestat_enabled) ? "Enabled" : "Disabled");
        }
        
		/* add the file as reference p2p content repo
			filter sitestat and relica.log 
		*/		
        if(!(is_request_for_replica_log(self->local_full_file_path) || is_request_for_sitestat_xml(self->local_full_file_path)))
		{
			ma_p2p_content_t *p2p_content = NULL ;
			ma_db_t *db_handle = ma_configurator_get_msgbus_database(MA_CONTEXT_GET_CONFIGURATOR(factory->context)) ;

			MA_LOG(MA_CONTEXT_GET_LOGGER(factory->context), MA_LOG_SEV_DEBUG, "adding conetnt reference (hash= %s, file = %s).", hash_buf.base, self->local_full_file_path) ;

			if(MA_OK == ma_p2p_content_create(&p2p_content)) {
				char time_created[21] ;
				ma_time_t cur_time = {0, 0, 0, 0, 0, 0, 0 } ;
				char *urn = NULL ;

				ma_time_get_localtime(&cur_time) ;
				MA_MSC_SELECT(_snprintf, snprintf)(time_created, 21, "%04d:%02d:%02d::%02d:%02d:%02d", cur_time.year, cur_time.month, cur_time.day, cur_time.hour, cur_time.minute, cur_time.second) ;

				urn = self->local_full_file_path ;
				urn = urn + strlen(server->policies.virtual_dir)+1 ;

				(void)ma_p2p_content_set_base_dir(p2p_content, server->policies.virtual_dir) ;
				(void)ma_p2p_content_set_content_format(p2p_content, MA_P2P_CONTENT_FORMAT_FILE) ;
				(void)ma_p2p_content_set_content_size(p2p_content, ma_fs_get_file_size(self->connection->server->uv_loop, self->local_full_file_path)) ;
				(void)ma_p2p_content_set_content_type(p2p_content, "Default") ;
				(void)ma_p2p_content_set_contributor(p2p_content, "LAZY_CACHE") ;
				(void)ma_p2p_content_set_hash(p2p_content, hash_buf.base) ;
				(void)ma_p2p_content_set_hit_count(p2p_content, 0) ;
				(void)ma_p2p_content_set_owned(p2p_content, MA_FALSE) ;
				(void)ma_p2p_content_set_time_created(p2p_content, time_created) ;
				(void)ma_p2p_content_set_time_last_access(p2p_content, time_created) ;
				(void)ma_p2p_content_set_urn(p2p_content, urn) ;

				if(MA_OK != (rc = ma_p2p_db_add_content(db_handle, p2p_content))) {
					MA_LOG(MA_CONTEXT_GET_LOGGER(factory->context), MA_LOG_SEV_WARNING, "adding conetnt reference failed (rc = %d).", rc) ;
				}

				(void)ma_p2p_content_release(p2p_content);
			}
		}
		
		ma_spipe_handler_send_response(self, MA_OK) ;
    }	

	return rc;
}

