#include "ma_spipe_handler_internal.h"

#include "ma_http_connection.h"

#include "ma/internal/ma_strdef.h"
#include "ma/internal/ma_macros.h"

#include "ma/internal/utils/filesystem/ma_fs_utils.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/core/msgbus/ma_msgbus_private.h"
#include "ma/internal/utils/platform/ma_sys_apis.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define MA_SPIPE_URL_TYPE_PKG_STR		"/spipe/pkg"
#define MA_SPIPE_URL_TYPE_FILE_STR		"/spipe/file"

#define MA_HTTP_REQUEST_COMMAND_QUERY_NAME_STR  "Command"
#define MA_HTTP_REQUEST_FILE_QUERY_NAME_STR     "URL"

/* Spipe handler */
static ma_error_t spipe_handler_start(ma_http_request_handler_t *handler, struct ma_http_connection_s *connection);
static ma_error_t spipe_handler_stop(ma_http_request_handler_t *handler);
static void spipe_handler_destroy(ma_http_request_handler_t *handler);

const static ma_http_request_handler_methods_t spipe_handler_methods = {
    &spipe_handler_start,
    &spipe_handler_stop,
    &spipe_handler_destroy
};

/* Spipe handler Factory */
static ma_bool_t spipe_handler_factory_matches(ma_http_request_handler_factory_t *self, ma_http_request_t const *request);
static ma_bool_t spipe_handler_factory_authorize(ma_http_request_handler_factory_t *self, ma_http_request_t const *request);
static ma_error_t spipe_handler_factory_configure(ma_http_request_handler_factory_t *self);
static ma_error_t spipe_handler_factory_create_instance(ma_http_request_handler_factory_t *self, ma_http_request_handler_t **handler);
static ma_error_t spipe_handler_factory_start(ma_http_request_handler_factory_t *self) ;
static ma_error_t spipe_handler_factory_stop(ma_http_request_handler_factory_t *self) ;
static void spipe_handler_factory_destroy(ma_http_request_handler_factory_t *self);

const static ma_http_request_handler_factory_methods_t spipe_handler_factory_methods = {
    &spipe_handler_factory_matches,
	&spipe_handler_factory_authorize,
    &spipe_handler_factory_configure,
    &spipe_handler_factory_create_instance,
	&spipe_handler_factory_start,
	&spipe_handler_factory_stop,
    &spipe_handler_factory_destroy
};


/* * * * F A C T O R Y * * */
static ma_bool_t spipe_handler_factory_matches(ma_http_request_handler_factory_t *self, struct ma_http_request_s const *request) {
	char url_path[MA_HTTP_MAX_URL_PATH_LEN] = {0};

    ma_bool_t is_spipe_request = get_url_path_from_url(&request->url, url_path, MA_HTTP_MAX_URL_PATH_LEN, 0) && (strstr(url_path, MA_SPIPE_URL_TYPE_PKG_STR) || strstr(url_path, MA_SPIPE_URL_TYPE_FILE_STR));
    
    return is_spipe_request && ((HTTP_POST == request->parser.method) || (HTTP_GET == request->parser.method));
}

static ma_bool_t verify_header_sign(ma_spipe_handler_factory_t *self, ma_http_request_t const *request, ma_bool_t is_spipe_file_upload_req) {
    ma_http_connection_t *connection = (ma_http_connection_t *)request->parser.data;
    ma_bool_t b_rc = MA_FALSE;   
    if(connection->cur_total_headers.base && connection->cur_total_headers.len > 0) {
        char *start = connection->cur_total_headers.base;
        char *end = strstr(start, MA_HTTP_SERVER_SIGNATURE_HEADER_NAME_STR);
        if(end && end < start + connection->cur_total_headers.len - 1) {
            uv_buf_t sig_buf = uv_buf_init(0, 0);
            size_t hdr_length = end - start;
            if(ma_http_request_search_header(request, MA_HTTP_SERVER_SIGNATURE_HEADER_NAME_STR, &sig_buf)) {
                ma_bytebuffer_t *sig_decode_buf = NULL;
                if(MA_OK == ma_bytebuffer_create(sig_buf.len, &sig_decode_buf)) { /* TBD - allocating buffer with size of sig data */
                    if(MA_OK == ma_crypto_decode((unsigned char *)sig_buf.base, sig_buf.len, sig_decode_buf, MA_TRUE)) {
                        ma_crypto_verify_t *crypto_verify = NULL;
                        if(MA_OK == ma_crypto_verify_create(self->crypto, MA_CRYPTO_VERIFY_SERVER_PUBLIC_KEY, &crypto_verify)) {
                            (void) ma_crypto_verify_buffer(crypto_verify, (unsigned char *)start, hdr_length, ma_bytebuffer_get_bytes(sig_decode_buf), ma_bytebuffer_get_size(sig_decode_buf), &b_rc);
                            (void) ma_crypto_verify_release(crypto_verify);
                        }
                    }
                    (void) ma_bytebuffer_release(sig_decode_buf);
                }
            }
        }
    }
    return b_rc;
}

static ma_bool_t spipe_handler_factory_authorize(ma_http_request_handler_factory_t *factory, ma_http_request_t const *request) {	
	ma_spipe_handler_factory_t *self = (ma_spipe_handler_factory_t *)factory;	
    ma_http_connection_t *connection = (ma_http_connection_t *)request->parser.data;
    ma_bool_t b_rc = MA_TRUE, is_header_signed = MA_FALSE, b_valid_request = MA_FALSE;
    ma_http_server_t *server = (ma_http_server_t *) self->base.data;

    char url_path[MA_HTTP_MAX_URL_PATH_LEN] = {0};
    ma_bool_t is_spipe_file_req = get_url_path_from_url(&connection->request.url, url_path, MA_HTTP_MAX_URL_PATH_LEN, 0) && !strcmp(url_path,MA_SPIPE_URL_TYPE_FILE_STR);    

    /* Header sign verification is needed only for spipe file upload and delete requests */
    if(is_spipe_file_req) {
        ma_bool_t is_spipe_file_upload_req =  is_spipe_file_req && (HTTP_POST == connection->request.parser.method);
        is_header_signed = verify_header_sign(self, request, is_spipe_file_upload_req);
    }
	b_valid_request = ma_http_server_request_validation_by_policy(server, request);

    b_rc = (!is_spipe_file_req || is_header_signed) && b_valid_request;

    MA_LOG(server->logger, MA_LOG_SEV_DEBUG, "ma_http_request_t(%p) authorization status (%d) in spipe handler factory", request, b_rc);

    return b_rc;
}

static ma_error_t spipe_handler_factory_configure(ma_http_request_handler_factory_t *factory) {
	ma_spipe_handler_factory_t *self = (ma_spipe_handler_factory_t *)factory;

    return MA_OK;
}

static ma_error_t spipe_handler_factory_create_instance(ma_http_request_handler_factory_t *factory, ma_http_request_handler_t **handler) {
    ma_spipe_handler_t *instance = (ma_spipe_handler_t *)calloc(1, sizeof(ma_spipe_handler_t));
    if (instance) {
        ma_http_request_handler_init(&instance->base, &spipe_handler_methods, factory);
        *handler = &instance->base;
        return MA_OK;
    }
    return MA_ERROR_OUTOFMEMORY;
}

static ma_error_t spipe_handler_factory_start(ma_http_request_handler_factory_t *factory) {
	ma_spipe_handler_factory_t *self = (ma_spipe_handler_factory_t *)factory ;

	if(self)
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "http spipe handler started") ;

	return MA_OK ;
}

static ma_error_t spipe_handler_factory_stop(ma_http_request_handler_factory_t *factory) {
	ma_spipe_handler_factory_t *self = (ma_spipe_handler_factory_t *)factory ;

	if(self)
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "http spipe handler stoped ") ;

	return MA_OK ;
}

static void spipe_handler_factory_destroy(ma_http_request_handler_factory_t *factory) {
	ma_spipe_handler_factory_t *self = (ma_spipe_handler_factory_t *)factory;
    free(self);
}

ma_error_t ma_spipe_handler_factory_create(ma_http_server_t *server, ma_http_request_handler_factory_t **spipe_handler_factory) {
    ma_spipe_handler_factory_t *self = (ma_spipe_handler_factory_t *)calloc(1, sizeof(ma_spipe_handler_factory_t));
    if (self) {
		ma_error_t rc = MA_OK;
		ma_http_request_handler_factory_init(&self->base, &spipe_handler_factory_methods, server);
        
       	*spipe_handler_factory = &self->base;
		return MA_OK;
    }
    return MA_ERROR_OUTOFMEMORY;
}

ma_error_t ma_spipe_handler_factory_set_crypto(ma_http_request_handler_factory_t *factory, ma_crypto_t *crypto) {
	ma_spipe_handler_factory_t *self = (ma_spipe_handler_factory_t *)factory;
	return (self && (self->crypto = crypto)) ? MA_OK : MA_ERROR_INVALIDARG;
}

ma_error_t ma_spipe_handler_factory_set_context(ma_http_request_handler_factory_t *factory, ma_context_t *context) {
	ma_spipe_handler_factory_t *self = (ma_spipe_handler_factory_t *)factory ;
	return (self && (self->context = context)) ? MA_OK : MA_ERROR_INVALIDARG ;
}
/* * * * * END F A C T O R Y * * * * */

void ma_spipe_handler_send_response(ma_spipe_handler_t *handler, ma_error_t status) {	
    MA_LOG(handler->connection->server->logger, MA_LOG_SEV_DEBUG, "ma_spipe_handler_t(%p) sending response for status (%d)", handler, status);
    switch(status) {
        case MA_OK:
            if(handler->is_spipe_file_upload_request) handler->connection->close_after_write = 1;
            ma_http_connection_send_stock_response(handler->connection, MA_HTTP_RESPONSE_200_OK);
            break;
        case MA_ERROR_HTTP_SERVER_REQUEST_NOT_FOUND:
            handler->connection->close_after_write = 1; /* It closes the connection after writing the response */
		    ma_http_connection_send_stock_response(handler->connection, MA_HTTP_RESPONSE_404_NOT_FOUND);	
            break;
        case MA_ERROR_HTTP_SERVER_REQUEST_FORBIDDEN:
            handler->connection->close_after_write = 1; /* It closes the connection after writing the response */
            ma_http_connection_send_stock_response(handler->connection, MA_HTTP_RESPONSE_403_FORBIDDEN);
            break;
        default:
            handler->connection->close_after_write = 1; /* It closes the connection after writing the response */
            ma_http_connection_send_stock_response(handler->connection, MA_HTTP_RESPONSE_500_INTERNAL_SERVICE);
            break;
    }
}

/* TBD - optimization - Can we calculate the hash and write data for each chunk ? so that we can avoid keeping whole data in-memory 
         This optimizaiton is needed only for file upload requests, not necessary for other requests */
static int on_spipe_body(http_parser *parser, const char *data, size_t length) {	
    ma_http_connection_t *connection = (ma_http_connection_t *)parser->data;
	ma_spipe_handler_t *handler = (ma_spipe_handler_t *) connection->cur_req_handler;
    ma_error_t rc = MA_OK;    
    size_t written_bytes = 0;    
    
    if(MA_OK == (rc = ma_stream_write(handler->stream, (const unsigned char *)data, length, &written_bytes)))
        handler->total_size += written_bytes;
        
    if(MA_OK == rc && handler->is_spipe_file_upload_request) {
        /* Restart the timer as connection is till active to download the files from ePO */
        ma_http_connection_idletimer_restart(connection);
    }

    if(MA_OK != rc) ma_spipe_handler_send_response(handler, rc);
	/* TBD - should we return -1 to stop further process on failures */
    return 0;
}

/* check whether the request is valid, this check was there in 4.8 */
ma_bool_t is_valid_request(ma_spipe_handler_t *handler) {
	ma_spipe_handler_factory_t *factory = (ma_spipe_handler_factory_t *)handler->base.data ;
	char *servername = NULL, *timestamp = NULL ;
	size_t size = 0 ;
	ma_bool_t rc = MA_FALSE ;

	(void)ma_spipe_package_get_info(handler->spipe, STR_PKGINFO_SERVER_NAME, (const unsigned char **)&servername, &size) ;
	(void)ma_spipe_package_get_info(handler->spipe, STR_PKGINFO_SERVER_TIME_STAMP, (const unsigned char **)&timestamp, &size) ;

	if(servername && timestamp) {
			rc = MA_TRUE ;
	}
	return rc ;
}

static int on_spipe_message_complete(http_parser *parser) {  
    ma_http_connection_t *connection = (ma_http_connection_t *)parser->data;
	ma_spipe_handler_t *handler = (ma_spipe_handler_t *) connection->cur_req_handler;
	ma_spipe_handler_factory_t *factory = (ma_spipe_handler_factory_t *)handler->base.data ;
    ma_http_server_t *server = (ma_http_server_t *) factory->base.data;

	ma_error_t rc =  MA_ERROR_HTTP_SERVER_INTERNAL;       

    MA_LOG(server->logger, MA_LOG_SEV_DEBUG, "ma_spipe_handler_t(%p) processing spipe request", handler);

    if(!handler->is_spipe_file_request) { /* /spipe/pkg */
        if(handler->total_size && handler->stream) {
			ma_bytebuffer_t *byte_buffer = NULL ;

            ma_stream_seek(handler->stream, 0);
			
			if(MA_OK == ma_stream_get_buffer(handler->stream, &byte_buffer)) {
				if(MA_OK == ma_bytebuffer_create(ma_bytebuffer_get_size(byte_buffer), &handler->spipe_buffer)) {
					(void)ma_bytebuffer_set_bytes(handler->spipe_buffer, 0, ma_bytebuffer_get_bytes(byte_buffer), ma_bytebuffer_get_size(byte_buffer)) ;
				}
				(void)ma_bytebuffer_release(byte_buffer) ;	byte_buffer = NULL ;
			}
	        
			if(MA_OK == (rc = ma_spipe_package_create(factory->crypto, &handler->spipe))) {		
		        (void)ma_spipe_package_set_logger(handler->spipe, connection->server->logger);
		        if(MA_OK == (rc = ma_spipe_package_deserialize(handler->spipe, handler->stream))) {
			        unsigned char const *value=NULL;
			        size_t size=0;				
			        if((MA_OK == (rc = ma_spipe_package_get_info(handler->spipe, STR_PACKAGE_TYPE, &value, &size))) && value) {
                         MA_LOG(connection->server->logger, MA_LOG_SEV_DEBUG, "ma_spipe_handler_t(%p) processing spipe pkg request type (%s)", handler, value);
				        if(!strcmp((char*)value, STR_PKGTYPE_AGENT_PING)) {
                            if(server->policies.is_agent_ping_enabled)
								rc = ma_spipe_agent_wakeup_handler(handler);
							else {
								rc = MA_ERROR_HTTP_SERVER_REQUEST_FORBIDDEN ;
                                MA_LOG(handler->connection->server->logger, MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE , "Agent wake up call is disabled through policy");
							}
				        }
				        else if(!strcmp((char*)value, STR_PKGTYPE_SUPER_AGENT_PING)) {
					        if(server->policies.is_super_agent_enabled) {
								if(is_valid_request(handler)) {
									char *catalog_version = NULL ;
									size_t size = 0 ;

									(void)ma_spipe_package_get_info(handler->spipe, STR_AGENT_PING_CATALOG_VERSION, (const unsigned char **)&catalog_version, &size) ;

									if(NULL == catalog_version) {
										rc = ma_spipe_super_agent_wakeup_handler(handler);
									}
									else {
										if(MA_OK == (rc = ma_spipe_global_update_handler(handler))) {
											/* Cache is flushed after receiving super agent ping. If global update notification is enabled in ePO, whenever new package is checked-in it sends notification to SA. 
											   Hence we perform cache flush, so that SA can serve latest content to the requesting agents
											*/
											if(server->policies.is_lazy_cache_enabled)
												(void) ma_http_server_repo_cache_flush(server);
										}
									}
								}
								else {
	                                MA_LOG(handler->connection->server->logger, MA_LOG_SEV_ERROR , "Invalid SA wake up call.");
								}
							}
                            else {
							    rc = MA_ERROR_HTTP_SERVER_REQUEST_FORBIDDEN ;
                                MA_LOG(handler->connection->server->logger, MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE , "Super Agent wake up call is disabled through policy");
                            }
                        }
				        else if(!strcmp((char*)value, STR_PKGTYPE_MSG_AVAILABLE)) {
                            rc = (server->policies.is_datachannel_ping_enabled) ? ma_spipe_datachannel_handler(handler) : MA_ERROR_HTTP_SERVER_REQUEST_FORBIDDEN;
				        }
				        else if(!strcmp((char*)value, STR_PKGTYPE_ACTION)) {
					        rc = MA_ERROR_NOT_SUPPORTED; /* TBD */
				        }			   
			        }				
		        }
		        (void)ma_spipe_package_release(handler->spipe); handler->spipe = NULL;
	        }
			(void)ma_bytebuffer_release(handler->spipe_buffer) ;	handler->spipe_buffer = NULL ;
            (void)ma_stream_release(handler->stream); handler->stream = NULL;
        }
    }
    else { /* /spipe/file */                        
        if(server->policies.is_super_agent_enabled && server->policies.is_super_agent_repository_enabled) {
            char file_path[MA_HTTP_MAX_URL_PATH_LEN] = {0};            
            rc = MA_ERROR_HTTP_SERVER_REQUEST_FORBIDDEN;
            if(get_query_value_from_url(&connection->request.url, MA_HTTP_REQUEST_FILE_QUERY_NAME_STR, file_path, MA_HTTP_MAX_URL_PATH_LEN, 0) && (is_request_for_repository(file_path))) {
                  MA_LOG(connection->server->logger, MA_LOG_SEV_DEBUG, "ma_spipe_handler_t(%p) processing spipe file request type (%s) for file <%s>", handler, (handler->is_spipe_file_upload_request) ? "upload file" : "delete file", file_path);
                if(handler->is_spipe_file_upload_request) {
                    if(handler->total_size && handler->stream) {
                        ma_stream_release(handler->stream); handler->stream = NULL;
                        rc = ma_spipe_file_upload_handler(handler);
                    }
                }
                else {
                    char value[MA_MAX_LEN] = {0};
                    if(get_query_value_from_url(&connection->request.url, MA_HTTP_REQUEST_COMMAND_QUERY_NAME_STR, value, MA_MAX_LEN, 0)) {
                        if(!strcmp(value, "DELETE")) {
                           char file_name[MAX_PATH_LEN] = {0};
                           if(get_file_from_url_path(file_path, file_name, MA_HTTP_MAX_URL_PATH_LEN))
                               rc =  ma_spipe_file_delete_handler(handler, file_name);
                        }   
                    }
                }
            }
        }   
        else {
            rc = MA_ERROR_HTTP_SERVER_REQUEST_FORBIDDEN ;
            MA_LOG(handler->connection->server->logger, MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE , "Super Agent repository disabled through policy");
        }
    }       


	/* Handlers are the owners for sending the response back to client when rc equals to MA_OK in this place */
	if(MA_OK != rc) ma_spipe_handler_send_response(handler, rc);
	
    return 0;
}

static ma_bool_t is_disk_space_available(const char *dir_name, size_t req_size) {
    uint64_t free_space = ma_get_disk_free_space(dir_name);
    return (free_space > req_size) ? MA_TRUE : MA_FALSE;
}

static ma_error_t spipe_handler_start(ma_http_request_handler_t *handler, ma_http_connection_t *connection) {
    ma_spipe_handler_t *spipe_handler = (ma_spipe_handler_t *) handler;    
	ma_error_t rc = MA_OK;
    ma_spipe_handler_factory_t *factory = (ma_spipe_handler_factory_t *)spipe_handler->base.data ;          
    ma_http_server_t *server = (ma_http_server_t *) factory->base.data;

    char url_path[MA_HTTP_MAX_URL_PATH_LEN] = {0};
    spipe_handler->is_spipe_file_request = get_url_path_from_url(&connection->request.url, url_path, MA_HTTP_MAX_URL_PATH_LEN, 0) && !strcmp(url_path,MA_SPIPE_URL_TYPE_FILE_STR);
    spipe_handler->is_spipe_file_upload_request =  spipe_handler->is_spipe_file_request && (HTTP_POST == connection->request.parser.method); 

    spipe_handler->connection = connection; 
	spipe_handler->connection->connection_type = MA_HTTP_CONNECTION_TYPE_SPIPE ;

    if(!spipe_handler->is_spipe_file_request) {
        rc = ma_mstream_create(1024, NULL, &spipe_handler->stream);  /* TBD - Initializing the stream with 1024, Can we initialize with some other value */       
    }
    else {
        if(spipe_handler->is_spipe_file_upload_request) {           
            uv_buf_t hdr_value = uv_buf_init(0,0);
            rc = MA_ERROR_HTTP_SERVER_INTERNAL;
            if(ma_http_request_search_header(&connection->request, MA_HTTP_SERVER_CONTENT_LENGTH_HEADER_NAME_STR, &hdr_value)) {
                size_t content_length = (hdr_value.base) ? atoi(hdr_value.base) : 0;
                 /* Check free disk space */             
                if(content_length > 0 && is_disk_space_available(server->policies.virtual_dir, content_length)) { 
                    char file_path[MA_HTTP_MAX_URL_PATH_LEN] = {0};            
                    if(get_query_value_from_url(&connection->request.url, MA_HTTP_REQUEST_FILE_QUERY_NAME_STR, file_path, MA_HTTP_MAX_URL_PATH_LEN, 0) && (is_request_for_repository(file_path))) {
                        char file_name[MAX_PATH_LEN] = {0};
                        if(get_file_from_url_path(file_path, file_name, MA_HTTP_MAX_URL_PATH_LEN)) {                          
                            if(get_local_file_path_for_file(file_name, server->policies.virtual_dir, spipe_handler->local_full_file_path, MAX_PATH_LEN)) {                               
                                /* Make directories */
                                char *p = NULL;
                                if(p = strrchr(file_name, MA_PATH_SEPARATOR)) {
                                    *p = 0;
                                    if(strlen(file_name) > 1)
                                        ma_fs_make_recursive_dir(connection->server->uv_loop, server->policies.virtual_dir, file_name);                                    
                                    *p = MA_PATH_SEPARATOR;
                                }
                                rc = ma_fstream_create(spipe_handler->local_full_file_path, MA_STREAM_ACCESS_FLAG_WRITE, NULL, &spipe_handler->stream);                               
                            }
                        }
                    }                    
                }
            }
        }
    }
  	
    if(MA_OK == rc) {        
	    /* Assigning on_spipe_body() and on_spipe_message_complete() to get the http body */
	    connection->parser_callbacks.on_body = &on_spipe_body;	
        connection->parser_callbacks.on_message_complete = &on_spipe_message_complete;	
        MA_LOG(connection->server->logger, MA_LOG_SEV_DEBUG, "ma_spipe_handler_t(%p) start", spipe_handler);
    }

    return rc;
}

static ma_error_t spipe_handler_stop(ma_http_request_handler_t *handler) {
    ma_spipe_handler_t *spipe_handler = (ma_spipe_handler_t *) handler;    
   
    MA_LOG(spipe_handler->connection->server->logger, MA_LOG_SEV_DEBUG, "ma_spipe_handler_t(%p) stop", spipe_handler);

    /* uv_fs_req_cleanup(&spipe_handler->fs_req); */

    return MA_OK;
}

static void spipe_handler_destroy(ma_http_request_handler_t *handler) {
    ma_spipe_handler_t *spipe_handler = (ma_spipe_handler_t *) handler;
    MA_LOG(spipe_handler->connection->server->logger, MA_LOG_SEV_DEBUG, "ma_spipe_handler_t(%p) destroy", spipe_handler);
    free(spipe_handler);
}

