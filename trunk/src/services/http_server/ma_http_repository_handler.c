#include "ma_http_repository_handler_internal.h"
#include "ma_http_connection.h"

#include "ma/internal/services/http_server/ma_http_server_utils.h"
#include "ma/internal/services/http_server/ma_url_cache.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/utils/filesystem/ma_fs_utils.h"
#include "ma/internal/utils/repository/ma_repository_private.h"
#include "ma/internal/utils/proxy/ma_proxy_config_internal.h"
#include "ma/internal/utils/configurator/ma_configurator.h"

#include "ma/internal/services/p2p/ma_p2p_db_manager.h"
#include "ma/internal/services/stats/ma_stats_db_manager.h"
#include "ma/internal/utils/conf/ma_conf.h"
#include "ma/internal/utils/downloader/ma_repository_downloader.h"
#include "ma/internal/utils/xml/ma_xml.h"
#include "ma/internal/utils/platform/ma_sys_apis.h"

#include <stdio.h>
#include <fcntl.h>

typedef ma_error_t (*after_validation_t)(ma_error_t status, ma_http_repository_handler_t *self);

struct ma_http_repository_handler_s {
    ma_http_request_handler_t			base;

	ma_http_connection_t				*connection;

    char                                file[MA_HTTP_MAX_URL_PATH_LEN];

    size_t                              bytes_sent;

    ma_url_cache_t                      *url_cache;

    char                                mime_type[MA_MAX_LEN];

	unsigned char						send_buffer[MA_HTTP_REPONSE_DATA_SIZE];

    void                                *cache_cookie;

    ma_bool_t                           is_response_header_sent;

    ma_bool_t                           is_file_reading_in_progress;

    size_t                              file_size;

    char                                *sahu_header_str;
	
	ma_bool_t							is_sitestat_downloading_from_server;

	ma_bool_t							need_repo_sync;

    ma_bool_t                           honor_lazycache;

	union {
        struct { 
			ma_repository_list_t                *cur_repository_list;

			ma_repository_t						*cur_repository;

			after_validation_t					validaiton_cb;

			ma_int32_t							cur_repo_index;

			size_t								repo_count;
			
        } repo_data;
    } extra;
} ;


/* Http repository handler */
static ma_error_t http_repository_handler_start(ma_http_request_handler_t *handler, struct ma_http_connection_s *connection);
static ma_error_t http_repository_handler_stop(ma_http_request_handler_t *handler);
static void http_repository_handler_destroy(ma_http_request_handler_t *handler);

static ma_error_t ma_repositories_get_cb(ma_error_t status, ma_client_t *ma_client, const ma_repository_list_t *repository_list, void *cb_data);
static ma_error_t ma_proxy_config_get_cb(ma_error_t status, ma_client_t *ma_client, const ma_proxy_config_t *proxy_config, void *cb_data);
static ma_error_t process_request(ma_http_repository_handler_t *handler);
static ma_error_t on_cache_data_cb(ma_url_cache_t *cache_obj, ma_url_cache_state_t state, size_t file_size, uv_file fd, void *cookie, void *cb_data);
static ma_error_t download_from_server(ma_http_repository_handler_t *self);
static ma_error_t download_from_disk(ma_http_repository_handler_t *self);

const static ma_http_request_handler_methods_t http_repository_handler_methods = {
    &http_repository_handler_start,
    &http_repository_handler_stop,
    &http_repository_handler_destroy
};

static ma_bool_t http_repository_handler_factory_matches(ma_http_request_handler_factory_t *self, ma_http_request_t const *request);
static ma_bool_t http_repository_handler_factory_authorize(ma_http_request_handler_factory_t *self, ma_http_request_t const *request);
static ma_error_t http_repository_handler_factory_configure(ma_http_request_handler_factory_t *self);
static ma_error_t http_repository_handler_factory_create_instance(ma_http_request_handler_factory_t *self, ma_http_request_handler_t **handler);
static ma_error_t http_repository_handler_factory_start(ma_http_request_handler_factory_t *self) ;
static ma_error_t http_repository_handler_factory_stop(ma_http_request_handler_factory_t *self) ;
static void http_repository_handler_factory_destroy(ma_http_request_handler_factory_t *self);

static ma_bool_t update_virtual_dir(ma_http_repository_handler_factory_t *self, const char *virtual_dir);

const static ma_http_request_handler_factory_methods_t http_repository_handler_factory_methods = {
    &http_repository_handler_factory_matches,
	&http_repository_handler_factory_authorize,
    &http_repository_handler_factory_configure,
    &http_repository_handler_factory_create_instance,
	&http_repository_handler_factory_start,
	&http_repository_handler_factory_stop,
    &http_repository_handler_factory_destroy
};

/* * * * F A C T O R Y * * */
static ma_bool_t http_repository_handler_factory_matches(ma_http_request_handler_factory_t *factory, struct ma_http_request_s const *request) {	
    char url_path[MA_HTTP_MAX_URL_PATH_LEN] = {0};
    return (HTTP_GET == request->parser.method) && get_url_path_from_url(&request->url, url_path, MA_HTTP_MAX_URL_PATH_LEN, 0) && is_request_for_repository(url_path);
}

static ma_bool_t http_repository_handler_factory_authorize(ma_http_request_handler_factory_t *factory, ma_http_request_t const *request) {
    ma_http_repository_handler_factory_t *self = (ma_http_repository_handler_factory_t *)factory;
    ma_http_connection_t *connection = (ma_http_connection_t *)request->parser.data;
    ma_http_server_t *server = (ma_http_server_t *) self->base.data;

	if(server->policies.is_super_agent_enabled && server->policies.is_super_agent_repository_enabled) {
		if(self->active_connections < server->policies.sa_repo_concurrent_connections_limit) {
			return MA_TRUE;
		}
		MA_LOG(connection->server->logger, MA_LOG_SEV_WARNING, "Super Agent repository handler reached max concurrent connections limit.") ;
	}
    else {
        MA_LOG(connection->server->logger, MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE , "Super Agent repository disabled through policy");
	}
	return MA_FALSE;
}

static ma_error_t http_repository_handler_factory_configure(ma_http_request_handler_factory_t *factory) {
	ma_http_repository_handler_factory_t *self = (ma_http_repository_handler_factory_t *)factory;
    ma_http_server_t *server = (ma_http_server_t *)self->base.data;
    ma_error_t rc = MA_OK;
    if(server->policies.is_lazy_cache_enabled) {
		ma_repository_list_t *repo_list = NULL;
		ma_proxy_config_t *proxy_cfg = NULL;
		if(MA_OK != ma_repository_get_repositories(self->context, MA_REPOSITORY_GET_REPOSITORIES_FILTER_ALL_ENABLED, &repo_list)) {
            MA_LOG(self->logger, MA_LOG_SEV_WARNING, "Failed to get repository list, continue with existing repository list, rc <%d>", rc);
        }
		else {
			if(self->repository_list) ma_repository_list_release(self->repository_list);
			self->repository_list = repo_list;
		}

		if(MA_OK != (rc = ma_proxy_get_config(self->context, &proxy_cfg))) {
            MA_LOG(self->logger, MA_LOG_SEV_WARNING, "Failed to get proxy config, continue with existing proxy config, rc <%d>", rc);
        }
		else {
			if(self->proxy_config) ma_proxy_config_release(self->proxy_config);
			self->proxy_config = proxy_cfg;
		}
    }
    return MA_OK;
}

static ma_error_t http_repository_handler_factory_create_instance(ma_http_request_handler_factory_t *factory, ma_http_request_handler_t **handler) {
    ma_http_repository_handler_t *instance = (ma_http_repository_handler_t *)calloc(1, sizeof(ma_http_repository_handler_t));
    if (instance) {
		ma_http_repository_handler_factory_t *self = (ma_http_repository_handler_factory_t *)factory ;

		++self->active_connections ;

        ma_http_request_handler_init(&instance->base, &http_repository_handler_methods, factory);
        *handler = &instance->base;
        return MA_OK;
    }
    return MA_ERROR_OUTOFMEMORY;
}

static ma_error_t http_repository_handler_factory_start(ma_http_request_handler_factory_t *factory) {
	ma_http_repository_handler_factory_t *self = (ma_http_repository_handler_factory_t *)factory ;
    ma_http_server_t *server = (ma_http_server_t *)self->base.data;
	ma_error_t rc = MA_OK ;
	
	return MA_OK ;
}

static ma_error_t http_repository_handler_factory_stop(ma_http_request_handler_factory_t *factory) {
	ma_http_repository_handler_factory_t *self = (ma_http_repository_handler_factory_t *)factory ;
	ma_http_server_t *server = (ma_http_server_t *)self->base.data;
	ma_error_t rc = MA_OK ;

	MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "http repository handler stoped");		

	if(server->policies.is_lazy_cache_enabled)
		(void) ma_http_server_repo_cache_flush(server);

	return rc ;
}

static void http_repository_handler_factory_destroy(ma_http_request_handler_factory_t *factory) {
	ma_http_repository_handler_factory_t *self = (ma_http_repository_handler_factory_t *)factory;  
	if(self->repository_list) ma_repository_list_release(self->repository_list);
	if(self->proxy_config) ma_proxy_config_release(self->proxy_config);
    free(self);
}

ma_error_t ma_http_repository_handler_factory_create(ma_http_server_t *server, ma_http_request_handler_factory_t **repository_handler_factory) {
    ma_http_repository_handler_factory_t *self = (ma_http_repository_handler_factory_t *)calloc(1, sizeof(ma_http_repository_handler_factory_t));
    if (self) {
		ma_error_t rc = MA_OK;
		ma_http_request_handler_factory_init(&self->base, &http_repository_handler_factory_methods, server);
        self->logger = server->logger;
        *repository_handler_factory = &self->base;
        return rc;
    }
    return MA_ERROR_OUTOFMEMORY;
}

ma_error_t ma_http_repository_handler_factory_set_context(ma_http_request_handler_factory_t *factory, ma_context_t *context) {
	ma_http_repository_handler_factory_t *self = (ma_http_repository_handler_factory_t *)factory;
	return (self && (self->context = context)) ? MA_OK : MA_ERROR_INVALIDARG;
}

/* * * * * END F A C T O R Y * * * * */

static int on_repository_message_body(http_parser *parser, const char *data, size_t length) {	
    /* No data for log file requests */ 
    return 0;
}

static int on_repository_message_complete(http_parser *parser) {
    ma_http_connection_t *connection = (ma_http_connection_t *)parser->data;
	ma_http_repository_handler_t *handler = (ma_http_repository_handler_t *) connection->cur_req_handler;

	ma_error_t rc =  MA_ERROR_HTTP_SERVER_INTERNAL;

    char url_path[MA_HTTP_MAX_URL_PATH_LEN] = {0};
 
    if(get_url_path_from_url(&connection->request.url, url_path, MA_HTTP_MAX_URL_PATH_LEN, 0)) {
        MA_LOG(connection->server->logger, MA_LOG_SEV_DEBUG, "ma_http_repository_handler_t(%p) processing repository file <%s> request", handler, url_path);        
        if(get_file_from_url_path(url_path, handler->file, MA_HTTP_MAX_URL_PATH_LEN)) {
            /* TBD - If file name is sitestate.xml , do cache flush based on flush interval */
            rc = process_request(handler);
        }
    }

    if(MA_OK != rc) ma_http_connection_send_stock_response(connection, (rc = MA_ERROR_HTTP_SERVER_REQUEST_NOT_FOUND) ? MA_HTTP_RESPONSE_404_NOT_FOUND : MA_HTTP_RESPONSE_403_FORBIDDEN);  
    return 0;
}

static ma_error_t process_sahu_headers(ma_http_repository_handler_t *self) {
    ma_error_t rc = MA_OK;    
    ma_buffer_t *buffer = NULL; 
    ma_bool_t is_request_from_sa = MA_FALSE;

    if(MA_OK == ma_ds_get_str(self->connection->server->ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_GUID_STR, &buffer) ) {
        const char *guid = NULL;
        size_t guid_len = 0;
        if(MA_OK == ma_buffer_get_string(buffer, &guid, &guid_len)) {  
            uv_buf_t hdr_value = uv_buf_init(0,0);
			size_t sahu_header_len = 0;
            /* If SAHU header present in headers, the request is coming from another SA */
            if(is_request_from_sa = ma_http_request_search_header(&self->connection->request, MA_HTTP_SAHU_HEADER_STR, &hdr_value)) {
                MA_LOG(self->connection->server->logger, MA_LOG_SEV_TRACE, "File request received from another SA, SAHU list <%s>", hdr_value.base);
                if(strstr(hdr_value.base, guid)) {    
                    MA_LOG(self->connection->server->logger, MA_LOG_SEV_ERROR, "Rejected file request, self SA GUID <%s> found in SAHU list<%s>", guid, hdr_value.base);
                    rc = MA_ERROR_HTTP_SERVER_REQUEST_FORBIDDEN;
                }  
                sahu_header_len = hdr_value.len;                
            }
            if(MA_OK == rc) {
                sahu_header_len += strlen(MA_HTTP_SAHU_SEPARATOR_STR) + guid_len;
                self->sahu_header_str = (char *)calloc(sahu_header_len + 1, sizeof(char));
                if(self->sahu_header_str) {
                    if(hdr_value.base) {
                        strncpy(self->sahu_header_str, hdr_value.base, hdr_value.len);
                        strcat(self->sahu_header_str, MA_HTTP_SAHU_SEPARATOR_STR);
                    }
                    else
                        strcpy(self->sahu_header_str, MA_HTTP_SAHU_SEPARATOR_STR);

                    strcat(self->sahu_header_str, guid);
                    MA_LOG(self->connection->server->logger, MA_LOG_SEV_TRACE, "SAHU header value <%s>", self->sahu_header_str);
                }
                else
                    rc = MA_ERROR_OUTOFMEMORY;
            }
        }          
        (void) ma_buffer_release(buffer);
    }

    if(MA_OK == rc && is_request_from_sa) {
        char url_path[MA_HTTP_MAX_URL_PATH_LEN] = {0};                    
        if(get_url_path_from_url(&self->connection->request.url, url_path, MA_HTTP_MAX_URL_PATH_LEN, 0)) {                             
            char file[MA_HTTP_MAX_URL_PATH_LEN] = {0};
            if(get_file_from_url_path(url_path, file, MA_HTTP_MAX_URL_PATH_LEN)) {
                ma_http_repository_handler_factory_t *factory = (ma_http_repository_handler_factory_t *)self->base.data;
                ma_url_cache_t *url_cache = NULL;
                if(MA_OK == ma_url_cache_manager_search(self->connection->server->cache_manager, file, &url_cache)) {
                    if(MA_URL_CACHE_READY != ma_url_cache_get_state(url_cache)) {
                        MA_LOG(self->connection->server->logger, MA_LOG_SEV_ERROR, "Rejecting file <%s> request, it is received from another SA and file is in progress of download", file);
                        rc = MA_ERROR_HTTP_SERVER_REQUEST_FORBIDDEN;                                    
                    }
                    (void) ma_url_cache_release(url_cache);
                }
            }
        }
    }     

    if(MA_OK != rc) {
        if(self->sahu_header_str) {
            free(self->sahu_header_str);
            self->sahu_header_str = NULL;
        }
    }

    return rc;
}


static ma_error_t http_repository_handler_start(ma_http_request_handler_t *handler, ma_http_connection_t *connection) {
    ma_http_repository_handler_t *self = (ma_http_repository_handler_t *) handler;  
    ma_http_repository_handler_factory_t *factory = (ma_http_repository_handler_factory_t *)self->base.data;
	ma_error_t rc = MA_OK;     
    ma_http_server_t *server = (ma_http_server_t *) factory->base.data;
    uv_buf_t user_agent_buf = uv_buf_init(0,0);
	ma_bool_t is_request_from_agent = MA_FALSE;

    self->connection = connection;
	self->connection->connection_type = MA_HTTP_CONNECTION_TYPE_SAREPO ;
  
    self->honor_lazycache = MA_FALSE;
    if(ma_http_request_search_header(&self->connection->request, MA_HTTP_SERVER_USER_AGENT_HEADER_NAME_STR, &user_agent_buf)) {
        /* I don't know why we allow empty strings also. following MA 4.8 code */
	    if(!strcmp(user_agent_buf.base, "") || !strcmp(user_agent_buf.base, MA_HTTP_SERVER_USER_AGENT_HEADER_VALUE_STR)) {  
            self->honor_lazycache = server->policies.is_lazy_cache_enabled;
			is_request_from_agent = MA_TRUE;
        }
    }
    else {
        /* MA_LOG(server->logger, MA_LOG_SEV_ERROR, "Request rejecting due to absence of user-agent header in request");	*/
        ma_http_connection_send_stock_response(connection, MA_HTTP_RESPONSE_403_FORBIDDEN); /* reject request if there is no User-Agent header in request */
        return MA_OK; 
    }

	if(is_request_from_agent && (MA_FALSE == connection->server->is_sitestat_enabled)) {
        ma_http_connection_send_stock_response(connection, MA_HTTP_RESPONSE_503_SERVICE_UNAVAILABLE);
        return MA_OK;
    }

	/* If lazy cache enabled, procress request headers to detect cyclic and dead lock scenarios */
	if(self->honor_lazycache) {
		if(MA_OK != (rc = process_sahu_headers(self))) {
			ma_http_connection_send_stock_response(self->connection, MA_HTTP_RESPONSE_403_FORBIDDEN);
			return MA_OK;
		}
	}

	connection->parser_callbacks.on_body = &on_repository_message_body;
	connection->parser_callbacks.on_message_complete = &on_repository_message_complete;		

    return rc;
}

static ma_error_t http_repository_handler_stop(ma_http_request_handler_t *handler) {
    ma_http_repository_handler_t *self = (ma_http_repository_handler_t *) handler;
	ma_http_repository_handler_factory_t *factory = (ma_http_repository_handler_factory_t *)self->base.data ;
    ma_error_t rc = MA_OK;
		   
	MA_LOG(self->connection->server->logger, MA_LOG_SEV_DEBUG, "ma_http_repository_handler_t(%p) stop", self);       
    
	return rc;
}

static void http_repository_handler_destroy(ma_http_request_handler_t *handler) {
    ma_http_repository_handler_t *self = (ma_http_repository_handler_t *) handler;
	ma_http_repository_handler_factory_t *factory = (ma_http_repository_handler_factory_t *)self->base.data ;

	--factory->active_connections ;

    MA_LOG(self->connection->server->logger, MA_LOG_SEV_DEBUG, "ma_http_repository_handler_t(%p)  release", self);
    if(self->sahu_header_str) free(self->sahu_header_str);
    free(self);
}

static ma_error_t send_response(ma_http_repository_handler_t *self, unsigned char *buffer, size_t buffer_len);

static ma_error_t cache_stop(ma_http_repository_handler_t *self, ma_bool_t need_stat_update) {
    ma_error_t rc = MA_OK;
    if(MA_OK == (rc = ma_url_cache_read_stop(self->url_cache, self->cache_cookie))) {
		if(need_stat_update) {
			ma_http_repository_handler_factory_t *factory = (ma_http_repository_handler_factory_t *)self->base.data ;
			ma_db_t *db = ma_configurator_get_msgbus_database(MA_CONTEXT_GET_CONFIGURATOR(factory->context));
			(void)ma_p2p_db_update_hit_count_and_access_time(db, ma_url_cache_get_file_hash(self->url_cache)) ;
			(void)ma_stats_db_update_current(db, MA_STAT_SAHU, MA_STAT_ATTRIBUTE_BANDWITH_SAVED, (ma_double_t)((ma_double_t)(self->file_size)/1024)/1024 ) ;
			(void)ma_stats_db_update_current(db, MA_STAT_SAHU, MA_STAT_ATTRIBUTE_FILES_SERVED, (ma_double_t)1) ;

			MA_LOG(self->connection->server->logger, MA_LOG_SEV_DEBUG, "Updated hit count from SA") ;
		}
    }
	rc = ma_url_cache_release(self->url_cache);
    self->url_cache = NULL; self->cache_cookie = NULL;
	self->file_size = 0;
    
	return rc;
}

static void ma_uv_write_cb(uv_write_t *req, int status) {
    ma_http_connection_t *conn = ma_queue_data(req, ma_http_connection_t, c_write_req);
    ma_http_repository_handler_t *self = (ma_http_repository_handler_t *)conn->cur_req_handler; 
	ma_http_repository_handler_factory_t *factory = (ma_http_repository_handler_factory_t *)self->base.data ;

    if (UV_OK == status) {
		/* added greater than check because sitestat.xml size received form server 62 (excluding \r\n) but file size is 64 when file reads from disk */
        if(self->bytes_sent >= self->file_size) { /* This check ensures that requested file transferred completely */
			MA_LOG(self->connection->server->logger, MA_LOG_SEV_TRACE, "File <%s> transferred completely.", self->file);
		    if (!conn->close_after_write) {

				(void) cache_stop(self, MA_TRUE);
                
				ma_http_connection_notify_request_complete(conn);  
                
				return; /* keep connection open  */
            }           
            (void) cache_stop(self, MA_FALSE);
            ma_http_connection_stop(conn);
        }
        else {
			size_t buffer_len;
			memset(&self->send_buffer, 0, MA_HTTP_REPONSE_DATA_SIZE);
			buffer_len = ma_url_cache_get_data(self->cache_cookie, self->send_buffer, MA_HTTP_REPONSE_DATA_SIZE);
			MA_LOG(self->connection->server->logger, MA_LOG_SEV_TRACE, "ma_uv_write_cb:File <%s> data serving size <%d> served size <%d>.", self->file, buffer_len, self->bytes_sent);
            if(((size_t)-1 == buffer_len) || (0 == buffer_len)) {
				MA_LOG(self->connection->server->logger, MA_LOG_SEV_TRACE, "ma_uv_write_cb:Waiting for download data, file <%s>", self->file);
                self->is_file_reading_in_progress = MA_FALSE;
            }
            else {
               self->bytes_sent += buffer_len;
                if(MA_OK != send_response(self, self->send_buffer, buffer_len)) { 
                    MA_LOG(self->connection->server->logger, MA_LOG_SEV_ERROR, "Failed to send response, file <%s>", self->file);
                }
            }
            /* Restart the timer as connection is till active to serve the files to caller */
            ma_http_connection_idletimer_restart(conn);
        }
    } else {
        uv_err_t uv_err = uv_last_error(self->connection->server->uv_loop);
        ma_http_server_log_uv_err(self->connection->server, "UV Write failed while serving data to clients in write callback", MA_LOG_SEV_ERROR, uv_err);
        (void) cache_stop(self, MA_FALSE);
        ma_http_connection_stop(conn);        
    }
}

/* TBD - below function looks ugly. will change this */
static ma_error_t send_response(ma_http_repository_handler_t *self, unsigned char *buffer, size_t buffer_len) { 
    ma_error_t rc = MA_OK;    

    if(!self->is_response_header_sent) {
        const size_t bytes_avail = ma_http_buffer_bytes_available(&self->connection->send_data);
        int n = MA_MSC_SELECT(_snprintf,snprintf)((char *)ma_http_buffer_peek(&self->connection->send_data), bytes_avail, "%u", self->file_size);        
        char *p = (0 < n && (size_t)n < bytes_avail) ? ma_http_buffer_advance(&self->connection->send_data, n) : 0;
        uv_buf_t write_headers[] = {
            MA_UV_BUF_TEXT(MA_HTTP_RESPONSE_HTTP1_1), MA_UV_BUF(MA_HTTP_RESPONSE_200_OK, strlen(MA_HTTP_RESPONSE_200_OK)), MA_UV_BUF_TEXT(MA_HTTP_CRLF),
            MA_UV_BUF_TEXT(MA_HTTP_RESPONSE_HEADER_FIELD_CONTENT_TYPE), MA_UV_BUF(self->mime_type, strlen(self->mime_type)), MA_UV_BUF_TEXT(MA_HTTP_CRLF),
            MA_UV_BUF_TEXT(MA_HTTP_RESPONSE_HEADER_FIELD_CONTENT_LENGTH), MA_UV_BUF(p, n), MA_UV_BUF_TEXT(MA_HTTP_CRLF),
            MA_UV_BUF_TEXT(MA_HTTP_CRLF),
            MA_UV_BUF(buffer, buffer_len),
        };
        
        if (UV_OK != uv_write(&self->connection->c_write_req, (uv_stream_t *) &self->connection->c_socket, write_headers, MA_COUNTOF(write_headers), &ma_uv_write_cb)) {
            uv_err_t uv_err = uv_last_error(self->connection->server->uv_loop);
            ma_http_server_log_uv_err(self->connection->server, "UV Write failed while serving data to clients", MA_LOG_SEV_ERROR, uv_err);
            (void) cache_stop(self, MA_FALSE);
            ma_http_connection_stop(self->connection);
            rc = MA_ERROR_HTTP_SERVER_INTERNAL;
        }
      
        self->is_response_header_sent = MA_TRUE;
    }
    else {
        uv_buf_t write_data[] = {
            MA_UV_BUF(buffer, buffer_len),
        };
     
        if (UV_OK != uv_write(&self->connection->c_write_req, (uv_stream_t *) &self->connection->c_socket, write_data, MA_COUNTOF(write_data), &ma_uv_write_cb)) {
            uv_err_t uv_err = uv_last_error(self->connection->server->uv_loop);
            ma_http_server_log_uv_err(self->connection->server, "UV Write failed while serving data to clients", MA_LOG_SEV_ERROR, uv_err);  
            (void) cache_stop(self, MA_FALSE);
            ma_http_connection_stop(self->connection);
            rc = MA_ERROR_HTTP_SERVER_INTERNAL;
        }      
    }
   
    return rc;
}

void get_catalog_verison(ma_http_repository_handler_t *self, const char *sitestat_data, size_t size, char *catalog_ver) {
	if(self && sitestat_data && size > 0 && catalog_ver) {
		ma_xml_t *xml_data = NULL;
		
		if(MA_OK == ma_xml_create(&xml_data)) {
			if(MA_OK == ma_xml_load_from_buffer(xml_data, (char*)sitestat_data, size)) {
				ma_xml_node_t *root = ma_xml_get_node(xml_data);
				if(root) {
					ma_xml_node_t *node =  ma_xml_node_search_by_path(root, SITESTAT_ROOT_NODE_STR);
					if(node) {
						const char *p = NULL;
						p = (char *)ma_xml_node_attribute_get(node, SITESTAT_CATALOG_VERSION_STR);
						if(p) 
							strncpy(catalog_ver, p, MA_MAX_LEN);
					}
				}
			}
			(void)ma_xml_release(xml_data);
		}
	}
}

static ma_error_t on_cache_ready_cb(ma_url_cache_t *cache_obj, ma_url_cache_state_t state, size_t file_size, void *cookie, void *cb_data) {
    ma_http_repository_handler_t *self = (ma_http_repository_handler_t *)cb_data;
    ma_error_t rc = MA_OK;             
	ma_http_repository_handler_factory_t *factory = (ma_http_repository_handler_factory_t *)self->base.data;
	ma_http_server_t *server = (ma_http_server_t *) factory->base.data;

    self->cache_cookie = cookie;
    self->file_size = file_size;
    MA_LOG(server->logger, MA_LOG_SEV_TRACE, "File <%s> cache callback received state <%d>", self->file, state);
    if(state == MA_URL_CACHE_INITIALIZED || state == MA_URL_CACHE_VALIDATION_START || state == MA_URL_CACHE_DOWNLOAD_START) {         
        // log statment here, these states can be ignored
         return MA_OK;
    }
    else if(state == MA_URL_CACHE_DOWNLOADING || state == MA_URL_CACHE_READY) { 
	 	if(file_size == -1) {
            MA_LOG(self->connection->server->logger, MA_LOG_SEV_ERROR, "File <%s> is not available on disk", self->file);
            return cache_stop(self, MA_FALSE);            
		}

        if(!self->is_file_reading_in_progress) { /* Ignore callback request if handler busy with reading data from file */
            size_t buffer_len;
			memset(&self->send_buffer, 0, MA_HTTP_REPONSE_DATA_SIZE);
			buffer_len = ma_url_cache_get_data(cookie, self->send_buffer, MA_HTTP_REPONSE_DATA_SIZE);
			MA_LOG(self->connection->server->logger, MA_LOG_SEV_TRACE, "on_cache_ready_cb:File <%s> data serving size <%d> served size <%d>.", self->file, buffer_len, self->bytes_sent);
			if(((size_t)-1 == buffer_len) || (0 == buffer_len)) {
				MA_LOG(self->connection->server->logger, MA_LOG_SEV_TRACE, "on_cache_ready_cb:Waiting for download data, file <%s>", self->file);
				return rc;
			}
			else {
				self->bytes_sent += buffer_len;			

				/* Assume that Sitestat data can be received in one response */
				if(self->honor_lazycache) {
					if(is_request_for_sitestat_xml(self->file)) {
						if(self->need_repo_sync) {
							char recv_catalog_ver[MA_MAX_LEN] = {0};
							int ret = 0;
							(void) get_catalog_verison(self, (const char *)self->send_buffer, buffer_len, recv_catalog_ver);

							ret = strcmp(recv_catalog_ver, server->catalog_version);
							MA_LOG(self->connection->server->logger, MA_LOG_SEV_TRACE, "Catalog version received <%s> and current catalog verison <%s>", recv_catalog_ver, server->catalog_version);
							
							/* Never get lower than our current verison as validate repository filter out those repositories
							   will serve the lower version to clients if received version is lower than that current version */
							if(0 < ret) { 
								server->last_time_check_for_new_content = ma_get_tick_count();
								ma_url_cache_manager_clear(server->cache_manager);
								ma_url_cache_set_state(self->url_cache, MA_URL_CACHE_READY);
								ma_url_cache_manager_add(server->cache_manager, self->file, self->url_cache);
							}
						}
						
						self->need_repo_sync = MA_FALSE;
						(void) ma_http_server_update_sitestat_info(server, (const char *)self->send_buffer, buffer_len);
					}
				}

				if(MA_OK == (rc = send_response(self, self->send_buffer, buffer_len)))
                    self->is_file_reading_in_progress = MA_TRUE;
            }
        }
        return rc;
    }
    else { /* MA_URL_CACHE_ABORTED, MA_URL_CACHE_VALIDATION_FAILED, MA_URL_CACHE_DOWNLOAD_FAILED, MA_URL_CACHE_DEINITIALIZED, MA_URL_CACHE_ERROR, MA_URL_CACHE_UNKNOWN */

		/* Skip sitestat cache object flush when flush happens due to repo sync based on flush timeout policy */
		if(is_request_for_sitestat_xml(self->file) && MA_URL_CACHE_ABORTED == state && self->need_repo_sync) {			
			return MA_OK;
		}

		(void) cache_stop(self, MA_FALSE);

		/* Server SiteStat.xml from disk if failed to download from server */
		if(self->honor_lazycache && is_request_for_sitestat_xml(self->file)) {
			if(self->is_sitestat_downloading_from_server) {
				self->is_sitestat_downloading_from_server = MA_FALSE;				
				/* sitestat data from disk */
				if(MA_OK == (rc = download_from_disk(self))) {
					(void) ma_url_cache_manager_add(server->cache_manager, self->file, self->url_cache);
					return rc;
				}
			}
		}
		
		if(self->connection) 
			(void) ma_http_connection_send_stock_response(self->connection, (MA_ERROR_HTTP_SERVER_REQUEST_NOT_FOUND == rc) ? MA_HTTP_RESPONSE_404_NOT_FOUND : MA_HTTP_RESPONSE_403_FORBIDDEN); 

        return MA_ERROR_HTTP_SERVER_INTERNAL;
    }
}

const char site_stat_file_name[25] = "/SiteStat.xml";

static ma_error_t on_sitestat_download_progress(ma_downloader_t *downloader, ma_download_progress_state_t state, ma_stream_t *stream, size_t download_total, void *cb_data) { 
    ma_http_repository_handler_t *self = (ma_http_repository_handler_t *)cb_data;
    ma_http_repository_handler_factory_t *factory = (ma_http_repository_handler_factory_t *)self->base.data;
	ma_http_server_t *server = (ma_http_server_t *) factory->base.data;

	ma_bool_t is_download_done = MA_FALSE;
	ma_error_t status = MA_ERROR_HTTP_SERVER_INTERNAL;
	
    MA_LOG(server->logger, MA_LOG_SEV_TRACE, "File <%s> download callback, state <%d> and total download <%d>", site_stat_file_name, state, download_total);
    
	if(MA_DOWNLOADER_DOWNLOAD_IN_PROGRESS == state) {
        self->file_size = download_total;  
    }
    else if(MA_DOWNLOADER_DOWNLOAD_DONE == state) {
		ma_bytebuffer_t *sitestat_buf = NULL;
		ma_stream_get_buffer(stream, &sitestat_buf);
		if(sitestat_buf) {
			unsigned char *sitestat_data = ma_bytebuffer_get_bytes(sitestat_buf);
            size_t len = ma_bytebuffer_get_size(sitestat_buf); 
			ma_xml_t *xml_data = NULL;	
			/* <SiteStatus Status="Enabled" CatalogVersion="20140614190212"/> */
			if(MA_OK == ma_xml_create(&xml_data)) {
				if(MA_OK == ma_xml_load_from_buffer(xml_data, (char*)sitestat_data, len)) {
					ma_xml_node_t *root = ma_xml_get_node(xml_data);
					if(root) {
						ma_xml_node_t *node =  ma_xml_node_search_by_path(root, SITESTAT_ROOT_NODE_STR);
						if(node) {
							const char *site_status = (char *)ma_xml_node_attribute_get(node, SITESTAT_STATUS_ATTR_STR);
							const char *catalog_version = (char *)ma_xml_node_attribute_get(node, SITESTAT_CATALOG_VERSION_STR);
							if(site_status && catalog_version && 0 <= strcmp(catalog_version, server->catalog_version)) {
								MA_LOG(server->logger, MA_LOG_SEV_TRACE, "SiteStat status <%s> catalog verison <%s>", site_status, catalog_version);
								if(!strcmp(site_status, SITESTAT_ENABLED_STR)) {
									const char *repo_name = NULL;
									ma_repository_get_name(self->extra.repo_data.cur_repository, &repo_name);
									if(repo_name) {
										server->last_used_repo_name = strdup(repo_name);
										status = MA_OK;		
									}
								}
							}
						}
					}
				}
				(void)ma_xml_release(xml_data);
			}			
			ma_bytebuffer_release(sitestat_buf);
		}		
        is_download_done = MA_TRUE;
    }
    else if(MA_DOWNLOADER_DOWNLOAD_CANCLED == state || MA_DOWNLOADER_DOWNLOAD_FAILED == state || MA_DOWNLOADER_CONNECT_FAILED == state) {
        is_download_done = MA_TRUE;
    }    
    else {
	    is_download_done = MA_TRUE;   
    }
        
    if(is_download_done) {        
        MA_LOG(server->logger, MA_LOG_SEV_DEBUG, "File <%s> download done with status <%d>", site_stat_file_name, state);
        (void) ma_downloader_release(downloader);
        (void) ma_stream_release(stream);  
		self->extra.repo_data.validaiton_cb(status, self);
    }    

    return MA_OK;
}

static ma_error_t sitestat_file_download(ma_http_repository_handler_t *self) {
	ma_error_t rc = MA_OK;
	ma_http_repository_handler_factory_t *factory = (ma_http_repository_handler_factory_t *)self->base.data;
	ma_http_server_t *server = (ma_http_server_t *) factory->base.data;
	
	if(MA_OK == (rc = ma_repository_list_get_repository_by_index(self->extra.repo_data.cur_repository_list, self->extra.repo_data.cur_repo_index++, &self->extra.repo_data.cur_repository))) {
		ma_downloader_t  *sitestat_downloader = NULL;
		ma_stream_t *stream = NULL;
		ma_repository_type_t repo_type = MA_REPOSITORY_TYPE_REPO; 
		ma_repository_url_type_t url_type = MA_REPOSITORY_URL_TYPE_END;

		(void) ma_repository_get_type(self->extra.repo_data.cur_repository, &repo_type);
		if(MA_REPOSITORY_TYPE_FALLBACK == repo_type)  {	
			MA_LOG(server->logger, MA_LOG_SEV_TRACE, "skipping fallback repository");
			self->extra.repo_data.validaiton_cb(MA_ERROR_REPOSITORY_INVALID, self);
			return rc;
		}

#ifndef MA_WINDOWS
		(void) ma_repository_get_url_type(self->extra.repo_data.cur_repository, &url_type);
		if(MA_REPOSITORY_URL_TYPE_UNC == url_type)  {			
			MA_LOG(server->logger, MA_LOG_SEV_TRACE, "skipping UNC repository access from *nix SA");
			self->extra.repo_data.validaiton_cb(MA_ERROR_REPOSITORY_INVALID, self);
			return rc;
		}
#endif
		if(MA_OK == (rc = ma_repository_downloader_create(&sitestat_downloader))) {						
			if(MA_OK == (rc = ma_mstream_create(1024, self, &stream))) {
				(void) ma_repository_downloader_set_net_client(sitestat_downloader, MA_CONTEXT_GET_NET_CLIENT(factory->context));
				(void) ma_repository_downloader_set_repository(sitestat_downloader, self->extra.repo_data.cur_repository);
				(void) ma_repository_downloader_set_ds(sitestat_downloader, self->connection->server->ds);
				(void) ma_repository_downloader_set_proxy_config(sitestat_downloader, factory->proxy_config);
				(void) ma_repository_downloader_set_logger(sitestat_downloader, server->logger);
				(void) ma_repository_downloader_set_context(sitestat_downloader, server->context);
				(void) ma_repository_downloader_use_relay(sitestat_downloader, MA_FALSE);	/* will set to TRUE when you add this support in repository downloader */

				if(self->sahu_header_str)
					ma_repository_downloader_add_header(sitestat_downloader, MA_HTTP_SAHU_HEADER_STR, self->sahu_header_str);

				rc = ma_downloader_start(sitestat_downloader, site_stat_file_name, stream, on_sitestat_download_progress, self);							
			}
		}    

		if(MA_OK != rc) {
			MA_LOG(server->logger, MA_LOG_SEV_ERROR, "Failed to start file <%s> download, rc <%d>", site_stat_file_name, rc);
			if(stream) (void)ma_stream_release(stream);
			if(sitestat_downloader) (void)ma_downloader_release(sitestat_downloader);
			(void)ma_repository_release(self->extra.repo_data.cur_repository),	self->extra.repo_data.cur_repository = NULL;
		}
	}
	else
		MA_LOG(server->logger, MA_LOG_SEV_WARNING, "Failed to get the next repository, rc <%d>", rc);

	return rc;
}

static ma_error_t after_finding_valid_repository(ma_error_t status, ma_http_repository_handler_t *self) {
	ma_http_repository_handler_factory_t *factory = (ma_http_repository_handler_factory_t *)self->base.data;
	ma_http_server_t *server = (ma_http_server_t *) factory->base.data;
	ma_error_t rc = MA_ERROR_HTTP_SERVER_INTERNAL;

	if(MA_OK == status) {
		if(self->url_cache) {
			if(is_request_for_sitestat_xml(self->file))
				self->is_sitestat_downloading_from_server = MA_TRUE;
			
			(void) ma_url_cache_set_repository(self->url_cache, self->extra.repo_data.cur_repository);
            rc = ma_url_cache_read_start(self->url_cache, self->honor_lazycache, on_cache_ready_cb, self);
		}

		(void)ma_repository_release(self->extra.repo_data.cur_repository),	self->extra.repo_data.cur_repository = NULL;
		ma_repository_list_release(self->extra.repo_data.cur_repository_list); self->extra.repo_data.cur_repository_list = NULL;
	}
	else {
		if(self->extra.repo_data.cur_repo_index < self->extra.repo_data.repo_count) {
			/* restart the timer, it may take time to find the valid repository */
			ma_http_connection_idletimer_restart(self->connection);
			/* release current repository as we are trying to download from another repository */
			(void)ma_repository_release(self->extra.repo_data.cur_repository),	self->extra.repo_data.cur_repository = NULL;
			rc = sitestat_file_download(self);
		}	
		else {
			(void)ma_repository_release(self->extra.repo_data.cur_repository),	self->extra.repo_data.cur_repository = NULL;
			ma_repository_list_release(self->extra.repo_data.cur_repository_list); self->extra.repo_data.cur_repository_list = NULL;
			if(self->url_cache) {
				/* Serve Sitestat.xml from disk if avialable on disk */
				ma_url_cache_set_state(self->url_cache, MA_URL_CACHE_READY);                        
                rc = ma_url_cache_read_start(self->url_cache, self->honor_lazycache, on_cache_ready_cb, self);
			}
		}
	}

	if(MA_OK != rc) {
		if(self->url_cache) {			
			ma_url_cache_manager_remove(server->cache_manager, self->file, self->url_cache);
			ma_url_cache_release(self->url_cache), self->url_cache = NULL;
		}
		MA_LOG(self->connection->server->logger, MA_LOG_SEV_ERROR, "Failed to find valid repository");
		(void) ma_http_connection_send_stock_response(self->connection, MA_HTTP_RESPONSE_404_NOT_FOUND);
	}

	return rc;
}

static ma_error_t find_valid_repository(ma_http_repository_handler_t *self) {
	ma_error_t rc = MA_OK;
	ma_http_repository_handler_factory_t *factory = (ma_http_repository_handler_factory_t *)self->base.data;
	ma_http_server_t *server = (ma_http_server_t *) factory->base.data;
	
	ma_repository_list_copy(factory->repository_list, &self->extra.repo_data.cur_repository_list);

	/* search last used repo name from repository list, if it finds use it. */
	if(server->last_used_repo_name) {
		size_t repo_count = 0;
		if(MA_OK == (rc = ma_repository_list_get_repositories_count(self->extra.repo_data.cur_repository_list, &repo_count))) {
			MA_LOG(server->logger, MA_LOG_SEV_TRACE, "Repositories count <%d>", repo_count);
			if(repo_count > 0) {
				ma_int32_t repo_index = 0;
				ma_bool_t has_valid_repo = MA_FALSE;
				while(repo_index < repo_count) {	
					if(MA_OK == (rc = ma_repository_list_get_repository_by_index(self->extra.repo_data.cur_repository_list, repo_index++, &self->extra.repo_data.cur_repository))) {						
						const char *repo_name = NULL;
						ma_bool_t is_enabled = MA_FALSE;
						(void) ma_repository_get_name(self->extra.repo_data.cur_repository, &repo_name);
						(void) ma_repository_get_enabled(self->extra.repo_data.cur_repository, &is_enabled);
						if(repo_name && !strcmp(server->last_used_repo_name, repo_name) && is_enabled) {							
							self->extra.repo_data.validaiton_cb(MA_OK, self);							
							has_valid_repo = MA_TRUE;
							break;
						}
						(void)ma_repository_release(self->extra.repo_data.cur_repository), self->extra.repo_data.cur_repository = NULL;
					}
					else
						MA_LOG(server->logger, MA_LOG_SEV_WARNING, "Failed to get the next repository, rc <%d>", rc);
				}

				if(!has_valid_repo) {
					(void) ma_http_server_repo_cache_flush(server);
					server->last_used_repo_name = NULL;
				}
			}
			else {
				MA_LOG(server->logger, MA_LOG_SEV_ERROR, "Repositories are not available");
			}
		}
		else
			MA_LOG(server->logger, MA_LOG_SEV_ERROR, "Failed to get repositories count, rc <%d>", rc);
	}

	if((MA_OK == rc) && !server->last_used_repo_name ) {
		if(MA_OK == (rc = ma_repository_list_get_repositories_count(self->extra.repo_data.cur_repository_list, &self->extra.repo_data.repo_count))) {
			MA_LOG(server->logger, MA_LOG_SEV_TRACE, "Repositories count <%d>", self->extra.repo_data.repo_count);
			if(self->extra.repo_data.repo_count > 0) {
				rc = sitestat_file_download(self);
			}
			else {
				MA_LOG(server->logger, MA_LOG_SEV_ERROR, "Repositories are not available");
			}
		}
		else
			MA_LOG(server->logger, MA_LOG_SEV_ERROR, "Failed to get repositories count, rc <%d>", rc);
	}

	if(MA_OK != rc) 
		ma_repository_list_release(self->extra.repo_data.cur_repository_list), self->extra.repo_data.cur_repository_list = NULL;

	return rc;
}

static ma_error_t download_from_disk(ma_http_repository_handler_t *self) {
	ma_http_repository_handler_factory_t *factory = (ma_http_repository_handler_factory_t *)self->base.data;
    ma_error_t rc = MA_ERROR_HTTP_SERVER_INTERNAL;
    ma_http_server_t *server = (ma_http_server_t *) factory->base.data;

	char local_file_path[MA_HTTP_MAX_URL_PATH_LEN] = {0};
	MA_LOG(self->connection->server->logger, MA_LOG_SEV_TRACE, "Serving file <%s> from local repository", self->file);
	if(get_local_file_path_for_file(self->file, server->policies.virtual_dir, local_file_path, MA_HTTP_MAX_URL_PATH_LEN)) {
		if(ma_fs_is_file_exists(server->uv_loop, local_file_path)) {			 
			if(MA_OK == (rc = ma_url_cache_create(server->uv_loop, self->file, &self->url_cache))) {
				(void) ma_url_cache_set_logger(self->url_cache, self->connection->server->logger);
				(void) ma_url_cache_set_repository_factory(self->url_cache, factory);
				ma_url_cache_set_state(self->url_cache, MA_URL_CACHE_READY);                        
                rc = ma_url_cache_read_start(self->url_cache, self->honor_lazycache, on_cache_ready_cb, self);
			}       
		}
		else {
			rc = MA_ERROR_HTTP_SERVER_REQUEST_NOT_FOUND;
			MA_LOG(server->logger, MA_LOG_SEV_DEBUG, "file <%s> not found on disk", self->file);
		}
	}
	return rc;
}

static ma_error_t download_from_server(ma_http_repository_handler_t *self) {
	ma_http_repository_handler_factory_t *factory = (ma_http_repository_handler_factory_t *)self->base.data;
    ma_error_t rc = MA_ERROR_HTTP_SERVER_INTERNAL;
    ma_http_server_t *server = (ma_http_server_t *) factory->base.data;
	
	if(MA_OK == (rc = ma_url_cache_create(self->connection->server->uv_loop, self->file, &self->url_cache))) {
		(void) ma_url_cache_set_ds(self->url_cache, self->connection->server->ds);
		(void) ma_url_cache_set_repository_factory(self->url_cache, factory);
		(void) ma_url_cache_set_logger(self->url_cache, self->connection->server->logger);

		if(self->sahu_header_str)
			ma_url_cache_set_sahu_header(self->url_cache, self->sahu_header_str);

		rc = ma_url_cache_manager_add(server->cache_manager, self->file, self->url_cache);			
		
		MA_LOG(self->connection->server->logger, MA_LOG_SEV_TRACE, "Reading file <%s>", self->file);
	}

	/* Set busy flag on this handler, it may take time to download the data from another server or find the valid repsotiroy scenarios */
	self->connection->is_cur_req_handler_busy = 1;
	self->extra.repo_data.validaiton_cb = after_finding_valid_repository;			
	if(MA_OK != (rc = find_valid_repository(self)))
		MA_LOG(server->logger, MA_LOG_SEV_ERROR, "Failed to find valid repository, rc <%d>", rc);
	
	return rc;
}


static ma_bool_t checking_for_new_content(ma_http_repository_handler_t *self) {
	ma_http_repository_handler_factory_t *factory = (ma_http_repository_handler_factory_t *)self->base.data;
    ma_http_server_t *server = (ma_http_server_t *) factory->base.data;
	char local_file_path[MA_HTTP_MAX_URL_PATH_LEN] = {0};
	
	unsigned long cur_tick_count = ma_get_tick_count();

	/* If Sitestat.xml doesn't exsit on disk even cache has entry for this */
	if(get_local_file_path_for_file(self->file, server->policies.virtual_dir, local_file_path, MA_HTTP_MAX_URL_PATH_LEN)) {
		if(!ma_fs_is_file_exists(server->uv_loop, local_file_path)) {
			MA_LOG(server->logger, MA_LOG_SEV_TRACE, "Sitestat.xml doesn't exist on disk trying to download from server");
			return MA_TRUE;
		}
	}

	/* After 52 days , GetTickCount will reset the value ...so if it is less than m_lLastTimeChecked
		..go and check the new contents 
		OR
		Check for the proper occurence of timeout
	*/

	if( (cur_tick_count < server->last_time_check_for_new_content ) || 
		((cur_tick_count - server->last_time_check_for_new_content)/1000  >= (server->policies.repository_sync_interval * 60) )) {
			MA_LOG(server->logger, MA_LOG_SEV_TRACE, "Timeout occurred to check new repository content in source");
			return MA_TRUE;
	}

	return MA_FALSE;
}

static ma_error_t process_request(ma_http_repository_handler_t *self) {
    ma_http_repository_handler_factory_t *factory = (ma_http_repository_handler_factory_t *)self->base.data;
    ma_error_t rc = MA_ERROR_HTTP_SERVER_INTERNAL;
    ma_http_server_t *server = (ma_http_server_t *) factory->base.data;
   
    const char *mime_type = get_mime_type(self->file);
    MA_MSC_SELECT(_snprintf,snprintf)(self->mime_type, MA_MAX_LEN, "%s", mime_type);
    
    MA_LOG(server->logger, MA_LOG_SEV_TRACE, "processing repository file <%s> request", self->file);	 

    MA_LOG(server->logger, MA_LOG_SEV_TRACE, "is_super_agent_repository_enabled <%d> is_lazy_cache_enabled <%d> honor_lazycache <%d>", server->policies.is_super_agent_repository_enabled, server->policies.is_lazy_cache_enabled, self->honor_lazycache);
    if(server->policies.is_super_agent_repository_enabled && self->honor_lazycache) {
		if(MA_OK == (rc = ma_url_cache_manager_search(server->cache_manager, self->file, &self->url_cache))) {			
			MA_LOG(server->logger, MA_LOG_SEV_TRACE, "file <%s> found in cache", self->file); 

			if(is_request_for_sitestat_xml(self->file)) {
				MA_LOG(server->logger, MA_LOG_SEV_TRACE, "processing Sitestat.xml request");
				self->need_repo_sync = checking_for_new_content(self);

				if(self->need_repo_sync) {					
					ma_url_cache_manager_remove(server->cache_manager, self->file, self->url_cache);
					ma_url_cache_release(self->url_cache), self->url_cache = NULL;
					if(MA_OK == (rc = download_from_server(self))) /* TBD - Should we fallback to some other in this failure */
						return rc;
				}
			}
			
			if(self->url_cache) {
				MA_LOG(self->connection->server->logger, MA_LOG_SEV_TRACE, "serving file <%s> from cache", self->file);
                rc = ma_url_cache_read_start(self->url_cache, self->honor_lazycache, on_cache_ready_cb, self);
			}
		}
		else {
			MA_LOG(self->connection->server->logger, MA_LOG_SEV_TRACE, "file <%s> not found in cache", self->file);
			rc = download_from_server(self);		
		}   		
    }
    else {
		rc = download_from_disk(self);
    }

    return rc;
}



