#ifndef MA_HTTP_ALLOCATOR_H_INCLUDED
#define MA_HTTP_ALLOCATOR_H_INCLUDED

#include <stdlib.h>

/* Single threaded memory allocator
 * keeps track of all allocated memory so you don't have to. All memory is freed whent the allocator is freed
 * works well for many small memory, for large memory blocks it falls back to malloc and friends, so not much slower (and still keeps track of those big blocks too)
 * 
 * caveat: 
 *  * won't free small blocks until allocator is freed
 *  * does not support realloc
 */

/* top level allocator */
 
typedef struct ma_http_allocator_s  ma_http_allocator_t;

/*!
 * brings a new allocator into existence, tada
 *
 */
ma_http_allocator_t *ma_http_allocator_create(size_t pool_size);

/*!
 * frees all contained data and the allocator itself, puf
 *
 */
void ma_http_allocator_destroy(ma_http_allocator_t *allocator);

/*!
 * frees all contained data without freeing the allocator itself, aah
 *
 */
void ma_http_allocator_reset(ma_http_allocator_t *allocator);

/*!
 * gives you some memory to play with, here you go
 */   
void *ma_http_allocator_alloc(ma_http_allocator_t *allocator, size_t size);

/*!
 * frees some memory, maybe (only frees the big blocks), phew
 */
void ma_http_allocator_free(ma_http_allocator_t *allocator, void *data);


typedef struct ma_http_allocator_chunk_s ma_http_allocator_chunk_t;
typedef struct ma_http_allocator_bigblock_s ma_http_allocator_bigblock_t;

struct ma_http_allocator_s {

    /*!
     * size that the user requested in ma_http_allocator_create()
     */
    size_t                          pool_size;

    /*!
     * linked list of chunks, these can be sub-allocated from
     */
    ma_http_allocator_chunk_t       *chunk_head;
    
    /*!
     * linked list of big blocks, no sub-allocation, but allocator still keeps track of memory. This memory also gets freed during ma_http_allocator_reset
     */
    ma_http_allocator_bigblock_t    *bigblock_head;

}; 

#endif /* MA_HTTP_ALLOCATOR_H_INCLUDED */
