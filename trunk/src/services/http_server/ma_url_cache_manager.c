#include "ma/internal/services/http_server/ma_url_cache_manager.h"
#include "ma/internal/utils/datastructures/ma_map.h"

#include <string.h>

MA_MAP_DECLARE(url_cache_map, char *, ma_url_cache_t *);

MA_MAP_DEFINE(url_cache_map, char *, strdup, free, ma_url_cache_t *, , ma_url_cache_release, strcmp);

/* Added wrapper on MAP, it might be need to ot add mutex on cache list */
struct ma_url_cache_manager_s {
       
    MA_MAP_T(url_cache_map)     *cache_map;

};

ma_error_t  ma_url_cache_manager_create(ma_url_cache_manager_t **url_cache_manager) {
    if(url_cache_manager) {
        ma_url_cache_manager_t *self = (ma_url_cache_manager_t *)calloc(1, sizeof(ma_url_cache_manager_t));
        if(self) {
            ma_error_t rc = MA_OK;
            if(MA_OK == (rc = MA_MAP_CREATE(url_cache_map, self->cache_map))) {
                *url_cache_manager = self;
                return MA_OK;
            }
            free(self);
            return rc;
        }
        return MA_ERROR_OUTOFMEMORY;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t  ma_url_cache_manager_add(ma_url_cache_manager_t *self, const char *url_name, ma_url_cache_t *cache_obj) {
    if(self && url_name && cache_obj) {
        ma_url_cache_add_ref(cache_obj);
        return MA_MAP_ADD_ENTRY(url_cache_map, self->cache_map, url_name, cache_obj);        
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t  ma_url_cache_manager_search(ma_url_cache_manager_t *self, const char *url_name, ma_url_cache_t **cache_obj) {
    if(self && url_name && cache_obj) {
        ma_error_t rc = MA_OK;
        if(MA_OK == (rc = MA_MAP_GET_VALUE(url_cache_map, self->cache_map, url_name, *cache_obj))) {
            ma_url_cache_add_ref(*cache_obj);
            return MA_OK;
        }
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t  ma_url_cache_manager_remove(ma_url_cache_manager_t *self, const char *url_name, ma_url_cache_t *cache_obj) {
    if(self && url_name && cache_obj) {      
		/* Notify all readers attached to the cache object */
		ma_url_cache_abort(cache_obj) ;
        return MA_MAP_REMOVE_ENTRY(url_cache_map, self->cache_map, url_name);
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t  ma_url_cache_manager_clear(ma_url_cache_manager_t *self) {
    if(self) {
		MA_MAP_FOREACH(url_cache_map, self->cache_map, iter) {
			ma_url_cache_abort((ma_url_cache_t *)iter.second) ;
		}
        return MA_MAP_CLEAR(url_cache_map, self->cache_map);       
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t  ma_url_cache_manager_release(ma_url_cache_manager_t *self) {
    if(self) {        
		MA_MAP_RELEASE(url_cache_map, self->cache_map);        
		free(self);
		return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}
