#include "ma_http_connection_manager.h"
#include "ma_http_connection.h"
#include "ma_http_server_internal.h"

void ma_http_connection_manager_init(ma_http_connection_manager_t *self) {
    if (self) {
        ma_queue_init(&self->qhead);
    }
}

void ma_http_connection_manager_uninit(ma_http_connection_manager_t *self) {
    if (self) {
		while(!ma_queue_empty(&self->qhead)) {
			ma_queue_t *item = ma_queue_last(&self->qhead);
            ma_http_connection_t *connection = ma_queue_data(item, ma_http_connection_t, qlink);
            ma_queue_remove(&connection->qlink);
            ma_http_connection_release(connection);
        }

        ma_queue_init(&self->qhead);
    }

}

ma_error_t ma_http_connection_manager_add(ma_http_connection_manager_t *self, struct ma_http_connection_s *connection) {
    if (self) {
        /*  */
        ma_queue_insert_head(&self->qhead, &connection->qlink);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_http_connection_manager_remove(ma_http_connection_manager_t *self, struct ma_http_connection_s *connection) {
    if (self) {
        ma_queue_remove(&connection->qlink);
        ma_http_connection_release(connection);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_http_connection_manager_stop_all(ma_http_connection_manager_t *self) {
    if (self) {
        ma_error_t result = MA_OK;
        ma_queue_t *item;
        ma_queue_foreach(item, &self->qhead) {
            ma_http_connection_t *connection = ma_queue_data(item, ma_http_connection_t, qlink);
            ma_error_t e = ma_http_connection_stop(connection);
            if (MA_OK != e) result = e; /* capture and return the first error */
        }
        return result;
    }

    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_http_connection_manager_abort_by_type(ma_http_connection_manager_t *self, ma_http_connection_type_t connection_type) {
    if (self) {
        ma_error_t result = MA_OK;
        ma_queue_t *item ;
        ma_queue_foreach(item, &self->qhead) {
            ma_http_connection_t *connection = ma_queue_data(item, ma_http_connection_t, qlink) ;
			if(connection->connection_type == connection_type) {
				connection->close_after_write = 1 ;
				ma_http_connection_send_stock_response(connection, MA_HTTP_RESPONSE_500_INTERNAL_SERVICE) ;
			}
        }
        return result ;
    }
    return MA_ERROR_INVALIDARG;
}