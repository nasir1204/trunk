#ifndef MA_HTTP_SERVER_INTERNAL_H_INCLUDEDs
#define MA_HTTP_SERVER_INTERNAL_H_INCLUDED

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "http_server"

#include "ma/ma_log.h"
#include "ma/ma_datastore.h"
#include "ma/internal/utils/database/ma_db.h"

#include "ma/internal/services/http_server/ma_http_server.h"
#include "ma_http_request_handler_factories.h"
#include "ma_http_connection_manager.h"
#include "ma/internal/ma_macros.h"
#include "ma/ma_msgbus.h"
#include "ma/internal/services/http_server/ma_url_cache_manager.h"
#include "ma/internal/defs/ma_http_server_defs.h"
#include "ma/internal/utils/context/ma_context.h"

#include "uv.h"

typedef struct ma_http_server_policies_s {
    ma_uint16_t                                 tcp_port;

    /* Spipe and repository Policies */
    ma_bool_t                                   is_agent_ping_enabled;

    ma_bool_t                                   is_super_agent_enabled;

    ma_bool_t                                   is_super_agent_repository_enabled;

    ma_bool_t                                   is_listen_to_epo_server_only;

    char                                        virtual_dir[MA_MAX_PATH_LEN];

    ma_bool_t                                   is_datachannel_ping_enabled; /* No policy from ePO as of now, so default it is MA_TRUE */

    ma_bool_t                                   is_lazy_cache_enabled;

    ma_int32_t                                  repository_sync_interval;

	ma_int32_t                                  repository_disk_quota ;

    /* Proxy Policies */
    ma_bool_t                                   is_relay_enabled;

    /* P2P policies */
    ma_bool_t							        is_content_serving_enabled ; 

	ma_bool_t									is_p2p_serving_suspended ;

    /* Policies for log file access requests */
    ma_bool_t                                   is_remote_log_enabled;

	/* Policies for connection limits */
	ma_uint32_t									p2p_concurrent_connections_limit ;

	ma_uint32_t									relay_concurrent_connections_limit ;

	ma_uint32_t									sa_repo_concurrent_connections_limit ;

	ma_uint32_t									relay_active_connections ;		/*kept here as we have different proxy factories and relay_concurrent_connections_limit apllies for both and cumulative */

} ma_http_server_policies_t;


struct ma_http_server_s {
    uv_loop_t                                   *uv_loop;

    uv_tcp_t                                    listen_socket;

    /* alternate socket in case dual stack is not supported (XP, W2K3,...) */
    uv_tcp_t                                    alt_listen_socket;

    ma_http_connection_manager_t                connection_mgr;

    ma_ds_t                                     *ds;

    ma_db_t                                     *db;

    ma_db_t                                     *msgbus_db ;

    ma_logger_t                                 *logger;

    ma_msgbus_t                                 *msgbus;

	ma_context_t								*context;
        
    ma_bool_t                                   is_server_running;

    ma_http_request_handler_factories_t         handler_factories;

    ma_http_server_policies_t                   policies;

    ma_url_cache_manager_t                      *cache_manager;

	ma_msgbus_server_t							*http_msgbus_server ;

	/* Repository Infor */
	char										*last_used_repo_name;

    ma_bool_t                                   is_sitestat_enabled;

	char										catalog_version[MA_MAX_LEN];

	unsigned long								last_time_check_for_new_content;

	uv_timer_t									*agent_wakeup_timer;					
};	

/* Reading 1024 size from file and sending back to clients */
#define MA_HTTP_REPONSE_DATA_SIZE       2048

#define MA_HTTP_SERVER_DEFAULT_READTIMEOUT_MS (120 * 1000)


#define MA_HTTP_CRLF "\xD\xA"

#define MA_HTTP_RESPONSE_HTTP1_1 "HTTP/1.1 "


#define MA_HTTP_RESPONSE_200_OK  "200 OK"

#define MA_HTTP_RESPONSE_202_OK  "202 OK"

#define MA_HTTP_RESPONSE_200_CONNECTION_ESTABLISHED  "200 Connection Established"

#define MA_HTTP_RESPONSE_400_BAD_REQUEST   "400 Bad Request"

#define MA_HTTP_RESPONSE_401_UNAUTHORIZED   "401 Unauthorized"

#define MA_HTTP_RESPONSE_403_FORBIDDEN   "403 Forbidden"

#define MA_HTTP_RESPONSE_404_NOT_FOUND  "404 Not Found"

#define MA_HTTP_RESPONSE_405_METHOD_NOT_ALLOWED    "405 Method not allowed"

#define MA_HTTP_RESPONSE_500_INTERNAL_SERVICE  "500 Internal Server Error"

#define MA_HTTP_RESPONSE_503_SERVICE_UNAVAILABLE  "503 Service Unavailable"

#define MA_HTTP_RESPONSE_HEADER_FIELD_CONTENT_LENGTH "Content-Length: "

#define MA_HTTP_RESPONSE_HEADER_FIELD_CONTENT_DISPOSITION  "Content-Disposition: "

#define MA_HTTP_RESPONSE_HEADER_FIELD_VIA "Via: "

#define MA_HTTP_RESPONSE_HEADER_FIELD_CONTENT_TYPE "Content-Type: "

#define MA_HTTP_RESPONSE_HEADER_FIELD_X_FRAME_OPTIONS "X-FRAME-OPTIONS: "

#define MA_HTTP_RESPONSE_HEADER_CONTENT_TYPE_TEXTPLAIN "text/plain"

#define MA_HTTP_RESPONSE_HEADER_CONTENT_TYPE_TEXTHTML "text/html"

#define MA_HTTP_RESPONSE_HEADER_VALUE_X_FRAME_OPTION	"SAMEORIGIN"

/* uv_buf is laid out differently depending on platform... */
#ifdef MA_WINDOWS
# define MA_UV_BUF(data, len) {(len), (char*) (data)}
#else
# define MA_UV_BUF(data, len) {(char *) (data), (len)}
#endif

#define MA_UV_BUF_TEXT(x) MA_UV_BUF((x), sizeof(x)-1)
#define MA_UV_BUF_STRING(x) MA_UV_BUF((x), strlen(x))

#define MA_HTTP_SA_REPOSITORY_DIR     "McAfeeHttp"

#define MA_HTTP_SA_REPOSITORY_TEMP_DIR    ".download"

/* private, for internal use only */
void ma_http_server_on_connection_stopped(ma_http_server_t *http_server, struct ma_http_connection_s *connection);

void ma_http_server_log_uv_err(ma_http_server_t *http_server, char const *msg, ma_log_severity_t sev, uv_err_t uv_err);

#define SITESTAT_ROOT_NODE_STR						 "SiteStatus"
#define SITESTAT_STATUS_ATTR_STR					 "Status"
#define SITESTAT_CATALOG_VERSION_STR				 "CatalogVersion"
#define SITESTAT_ENABLED_STR						 "Enabled"

ma_error_t ma_http_server_repo_cache_flush(ma_http_server_t *http_server);

ma_error_t ma_http_server_update_sitestat_info(ma_http_server_t *http_server, const char *sitestat_data, size_t size);

/* Forward delcleration */
struct ma_http_request_s;

ma_bool_t ma_http_server_request_validation_by_policy(ma_http_server_t *self, struct ma_http_request_s const *request);

#endif /* MA_HTTP_SERVER_INTERNAL_H_INCLUDED */
