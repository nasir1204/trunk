
#include "ma/internal/services/http_server/ma_http_server_utils.h"
#include "ma/internal/utils/repository/ma_repository_private.h"
#include "ma/internal/utils/network/ma_net_interface.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal/utils/datastructures/ma_stream.h"
#include "ma/internal/utils/filesystem/ma_fs_utils.h"
#include "ma/internal/utils/context/ma_context.h"
#include "ma/repository/ma_repository_client_defs.h"
#include "http_parser.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>

char *mime_list[][2] = {
		{ "log",        "text/plain"},
        { "html",       "text/html"},
        { "htm",        "text/html"},
		{ "xsl",        "text/xml"},
        { "xml",        "text/xml"},
        { "htc",        "text/x-component"},
		{ "js",			"application/x-javascript" },
        { "json",       "application/json" },
		{ "css",		"text/css" },
        { "txt",        "text/plain"},
        { "au",         "audio/basic"},
        { "snd",        "audio/basic"},
        { "mid",        "audio/midi"},
        { "midi",       "audio/midi"},
        { "aif",        "audio/x-aiff"},
        { "aiff",       "audio/x-aiff"},
        { "aifc",       "audio/x-aiff"},
        { "ra",         "audio/x-pn-realaudio"},
        { "rm",         "audio/x-pn-realaudio"},
        { "ram",        "audio/x-pn-realaudio"},
        { "wav",        "audio/x-wav"},
        { "qt",         "video/quicktime"},
        { "mov",        "video/quicktime"},
        { "mpeg",       "video/mpeg"},
        { "mpg",        "video/mpeg"},
        { "mpe",        "video/mpeg"},
        { "mws",        "application/maple-v-r4"},
        { "csm",        "application/cu-seeme"},
        { "cu",         "application/cu-seeme"},
        { "tsp",        "application/dsptype"},
        { "xls",        "application/excel"},
        { "hqx",        "application/mac-binhex40"},
        { "doc",        "application/msword"},
        { "dot",        "application/msword"},
        { "bin",        "application/octet-stream"},
		{ "msi",        "application/octet-stream"},
        { "oda",        "application/oda"},
        { "pdf",        "application/pdf"},
        { "pgp",        "application/pgp-signature"},
        { "ps",         "application/postscript"},
        { "ai",         "application/postscript"},
        { "eps",        "application/postscript"},
        { "ppt",        "application/powerpoint"},
        { "rtf",        "application/rtf"},
        { "wp5",        "application/wordperfect5.1"},
        { "wk",         "application/x-123"},
        { "wz",         "application/x-Wingz"},
        { "bcpio",      "application/x-bcpio"},
        { "cpio",       "application/x-cpio"},
        { "csh",        "application/x-csh"},
        { "deb",        "application/x-debian-package"},
        { "dvi",        "application/x-dvi"},
        { "pfa",        "application/x-font"},
        { "pfb",        "application/x-font"},
        { "gsf",        "application/x-font"},
        { "pcf",        "application/x-font"},
        { "gtar",       "application/x-gtar"},
        { "tgz",        "application/x-gtar"},
        { "hdf",        "application/x-hdf"},
        { "phtml",      "application/x-httpd-php"},
        { "pht",        "application/x-httpd-php"},
        { "php",        "application/x-httpd-php"},
        { "php3",       "application/x-httpd-php3"},
        { "phps",       "application/x-httpd-php3-source"},
        { "php3p",      "application/x-httpd-php3-preprocessed"},
        { "class",      "application/x-java"},
        { "latex",      "application/x-latex"},
        { "frm",        "application/x-maker"},
        { "maker",      "application/x-maker"},
        { "frame",      "application/x-maker"},
        { "fm",         "application/x-maker"},
        { "fb",         "application/x-maker"},
        { "book",       "application/x-maker"},
        { "fbdoc",      "application/x-maker"},
        { "mif",        "application/x-mif"},
        { "com",        "application/x-msdos-program"},
        { "exe",        "application/x-msdos-program"},
        { "bat",        "application/x-msdos-program"},
        { "nc",         "application/x-netcdf"},
        { "cdf",        "application/x-netcdf"},
        { "o",          "application/x-object"},
        { "pl",         "application/x-perl"},
        { "pm",         "application/x-perl"},
        { "sh",         "application/x-sh"},
        { "shar",       "application/x-shar"},
        { "sv4cpio",    "application/x-sv4cpio"},
        { "sv4crc",     "application/x-sv4crc"},
        { "tar",        "application/x-tar"},
        { "tcl",        "application/x-tcl"},
        { "tex",        "application/x-tex"},
        { "gf",         "application/x-tex-gf"},
        { "pk",         "application/x-tex-pk"},
        { "texinfo",    "application/x-texinfo"},
        { "texi",       "application/x-texinfo"},
        { "bak",        "application/x-trash"},
        { "old",        "application/x-trash"},
        { "sik",        "application/x-trash"},
        { "t",          "application/x-troff"},
        { "tr",         "application/x-troff"},
        { "roff",       "application/x-troff"},
        { "man",        "application/x-troff-man"},
        { "me",         "application/x-troff-me"},
        { "ms",         "application/x-troff-ms"},
        { "ustar",      "application/x-ustar"},
        { "src",        "application/x-wais-source"},
        { "zip",        "application/zip"},
        { "z",			"application/zip"},
		{ "gz",			"application/x-gunzip"},
        { "gif",        "image/gif"},
        { "ief",        "image/ief"},
        { "jpeg",       "image/jpeg"},
        { "jpg",        "image/jpeg"},
        { "jpe",        "image/jpeg"},
        { "png",        "image/png"},
        { "tiff",       "image/tiff"},
        { "tif",        "image/tiff"},
        { "ras",        "image/x-cmu-raster"},
        { "bmp",        "image/x-ms-bmp"},
        { "pnm",        "image/x-portable-anymap"},
        { "pbm",        "image/x-portable-bitmap"},
        { "pgm",        "image/x-portable-graymap"},
        { "ppm",        "image/x-portable-pixmap"},
        { "rgb",        "image/x-rgb"},
        { "xbm",        "image/x-xbitmap"},
        { "xpm",        "image/x-xpixmap"},
        { "xwd",        "image/x-xwindowdump"},
        { "csv",        "text/comma-separated-values"},
        { "rtx",        "text/richtext"},
        { "tsv",        "text/tab-separated-values"},
        { "hpp",        "text/x-c++hdr"},
        { "hxx",        "text/x-c++hdr"},
        { "hh",         "text/x-c++hdr"},
        { "cpp",        "text/x-c++src"},
        { "cxx",        "text/x-c++src"},
        { "cc",         "text/x-c++src"},
        { "h",          "text/x-chdr"},
        { "c",          "text/x-csrc"},
        { "java",       "text/x-java"},
        { "moc",        "text/x-moc"},
        { "p",          "text/x-pascal"},
        { "pas",        "text/x-pascal"},
        { "etx",        "text/x-setext"},
        { "tcl",        "text/x-tcl"},
        { "tk",         "text/x-tcl"},
        { "tex",        "text/x-tex"},
        { "ltx",        "text/x-tex"},
        { "sty",        "text/x-tex"},
        { "cls",        "text/x-tex"},
        { "vcs",        "text/x-vCalendar"},
        { "vcf",        "text/x-vCard"},
        { "dl",         "video/dl"},
        { "fli",        "video/fli"},
        { "gl",         "video/gl"},
        { "avi",        "video/x-msvideo"},
        { "movie",      "video/x-sgi-movie"},
        { "vrm",        "x-world/x-vrml"},
        { "vrml",       "x-world/x-vrml"},
        { "wrl",        "x-world/x-vrml"},
        { "nas",        "text/html"},
        { "ini",        "text/html"},
        { "pkg",        "text/html"},
        { "hlp",        "application/hlp"},		
        { NULL,        NULL }
};


ma_bool_t get_url_path_from_url(const uv_buf_t *url, char *url_path, size_t file_size, int is_connect) {
    if(url && url_path) {
        struct http_parser_url url_parts;        

        if(!http_parser_parse_url(url->base, url->len, is_connect, &url_parts) && (url_parts.field_set & (1 << UF_PATH))) {
            if(file_size > url_parts.field_data[UF_PATH].len) {
                strncpy(url_path, url->base + url_parts.field_data[UF_PATH].off, url_parts.field_data[UF_PATH].len);
                return MA_TRUE;
            }
        }        
    }
    return MA_FALSE;
}

ma_bool_t get_query_from_url(const uv_buf_t *url, char *query, size_t size, int is_connect) {
    if(url && query) {
        struct http_parser_url url_parts;

        if(!http_parser_parse_url(url->base, url->len, is_connect, &url_parts) && (url_parts.field_set & (1 << UF_QUERY))) {
            if(size > url_parts.field_data[UF_QUERY].len) {
                strncpy(query, url->base + url_parts.field_data[UF_QUERY].off, url_parts.field_data[UF_QUERY].len);
                return MA_TRUE;
            }
        }        
    }
    return MA_FALSE;
}

ma_bool_t get_query_value_from_url(const uv_buf_t *url, const char *name, char *query_value, size_t size, int is_connect) {
    ma_bool_t b_rc = MA_FALSE;
    if(url && name && query_value) {
        char query[MA_MAX_LEN] = {0};        
        if(get_query_from_url(url, query, MA_MAX_LEN, is_connect)) {                 
            char *token = strtok(query, "&");
            while(token) {
                if(!strncmp(token, name, strlen(name))) {               
                    char *p = token + strlen(name);
                    if(p && (*p++ == '=')) {                        
                        char *s = p;
                        size_t len = 0;
                        while(p && *p && (*p++ != '&'));                            

                        if(len = p - s){
                            if(size > len) {
                                strncpy(query_value, s, len);
                                b_rc = MA_TRUE;
                            }
                        }
                        break;
                    }
                }
                token = strtok(NULL, "&");
            }            
        }        
    }
    return b_rc;
}


#define HASH_SHA1_HEX_SIZE   40
#define HASH_SHA256_HEX_SIZE   64

static ma_bool_t  hex_encode(const unsigned char *raw,const unsigned int size,unsigned char *hex,unsigned int max_size) {
    if(raw && hex && max_size >= size*2) {
        unsigned int i, j;
        const char hex_char[16]={'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
        for(i=0,j=0;i<size;i++)
        {
	        char temp=*(raw+i);
	        char first=(temp>>4) & 0x0F;
	        char second=temp & 0x0F;

            *(hex+j)=hex_char[(int)first];j++;
	        *(hex+j)=hex_char[(int)second];j++;
        }
        return MA_TRUE;
    }
    return MA_FALSE;
}

ma_bool_t verify_file_hash(uv_loop_t *uv_loop, ma_crypto_t *crypto, const char *local_file_path, ma_crypto_hash_type_t hash_type, uv_buf_t hash_buf) {
    ma_bool_t is_hash_matched = MA_FALSE;    
	ma_crypto_hash_t *hash_crypto = NULL;
    ma_error_t rc = MA_OK;

    if(MA_OK == ma_crypto_hash_create(crypto, hash_type, &hash_crypto)) {
        size_t total_size = ma_fs_get_file_size(uv_loop, local_file_path);
        if(total_size != -1) {
            uv_fs_t fs_req;
            if(-1 != uv_fs_open(uv_loop, &fs_req, local_file_path, O_RDONLY, 0, NULL)) {
                uv_file fd = fs_req.result;
                size_t cur_pos = 0;
                uv_fs_req_cleanup(&fs_req);
                while(cur_pos != total_size) {
                    unsigned char buffer[MA_MAX_BUFFER_LEN] = {0};
                    if(-1 != uv_fs_read(uv_loop, &fs_req, fd, buffer, MA_MAX_BUFFER_LEN, cur_pos, NULL)) {
                        if(fs_req.result > 0) {
                            if(MA_OK != (rc = ma_crypto_hash_data_update(hash_crypto, buffer, fs_req.result)))
				                break;
                            cur_pos += fs_req.result;                            
                        }
                        uv_fs_req_cleanup(&fs_req);
                    }
	            }
		
                /* No hash for empty files ?? */
	            if(MA_OK == rc && total_size) {
                    ma_bytebuffer_t *byte_buffer = NULL;
		            if(MA_OK == ma_bytebuffer_create(MA_MAX_HASH_LEN, &byte_buffer)) {	
			            if(MA_OK == ma_crypto_hash_data_final(hash_crypto, byte_buffer)) { 
                            if(MA_CRYPTO_HASH_SHA1 == hash_type) {
                                unsigned char hex_hash[HASH_SHA1_HEX_SIZE] = {0};
                                hex_encode(ma_bytebuffer_get_bytes(byte_buffer), ma_bytebuffer_get_size(byte_buffer), hex_hash, HASH_SHA1_HEX_SIZE);
                                is_hash_matched = !memcmp(hex_hash, hash_buf.base, HASH_SHA1_HEX_SIZE);
                            }
                            else {
                                unsigned char hex_hash[HASH_SHA256_HEX_SIZE] = {0};
                                hex_encode(ma_bytebuffer_get_bytes(byte_buffer), ma_bytebuffer_get_size(byte_buffer), hex_hash, HASH_SHA256_HEX_SIZE);
                                is_hash_matched = !memcmp(hex_hash, hash_buf.base, HASH_SHA256_HEX_SIZE);
                            }                            
                        }
                        ma_bytebuffer_release(byte_buffer);
		            }
	            }
                uv_fs_close(uv_loop, &fs_req, fd, NULL);
                uv_fs_req_cleanup(&fs_req);
            }
        }
		(void) ma_crypto_hash_release(hash_crypto);
	}	
	return is_hash_matched;
}

 /* TBD - comparision should be case insensitive , convert to lower and compare */
ma_bool_t is_request_for_repository(const char *file_name) {
    return (file_name) && (strstr(file_name, "Software") || strstr(file_name, "software")) ;
}

ma_bool_t is_request_for_replica_log(const char *file_name){
    return (file_name) && (strstr(file_name, "Replica.log") || strstr(file_name, "replica.log")) ;
}

ma_bool_t is_request_for_sitestat_xml(const char *file_name){
    return (file_name) && (strstr(file_name, "SiteStat.xml") || strstr(file_name, "Sitestat.xml") || strstr(file_name, "sitestat.xml")) ;
}

ma_bool_t is_request_for_log_file(const char *file_name) {
    ma_bool_t b_rc = MA_FALSE;	    
    if(file_name) {
        const char *p = file_name;
        if(!strcmp(p, "/") || !strcmp(p, "\\")) {                    
            b_rc = MA_TRUE;
        }
        else {
            p++;
			b_rc = MA_TRUE;
			/* TBD  - will check the file name in list of files */
            //b_rc = (!MA_WIN_SELECT(stricmp,strcasecmp)(p, MA_HTTP_AGENT_LOG_HTML_FILE_STR) || !MA_WIN_SELECT(stricmp,strcasecmp)(p, MA_HTTP_AGENT_LOG_JS_FILE_STR) || !MA_WIN_SELECT(stricmp,strcasecmp)(p, MA_HTTP_AGENT_LOG_JSON_FILE_STR));
        }    
    }
    return b_rc;
}

/* file->   /Previous/VSCANDAT1000/DAT/0000/PkgCatalog.z - *nix
            \\Previous\\VSCANDAT100\\DAT\\0000\\PkgCatalog.z - win
   expected replica_log_url -> /Previous/VSCANDAT1000/DAT/0000/replica.log - *nix
                            -> \\Previous\\VSCANDAT100\\DAT\\0000\\replica.z - win
*/
ma_bool_t get_replica_log_from_file(const char *file, char *replica_log, size_t size) {
    ma_bool_t b_rc = MA_FALSE;
    if(file && replica_log && size > 0 ) {
        char *p = NULL;
        if(p = strrchr((char *)file, MA_PATH_SEPARATOR)) {
            *p = 0;
            MA_MSC_SELECT(_snprintf,snprintf)(replica_log, size, "%s%c%s", file, MA_PATH_SEPARATOR, "Replica.log");
            *p = MA_PATH_SEPARATOR;
            b_rc = MA_TRUE;
        }
    }
    return b_rc;
}

/* url_path -> /Software/Previous\VSCANDAT1000\DAT\0000\PkgCatalog.z
   expected filename -> \\Previous\\VSCANDAT1000\DAT\0000\PkgCatalog.z for Windows
                     -> /Previous/VSCANDAT1000/DAT/0000/PkgCatalog.z for *nix
*/
ma_bool_t get_file_from_url_path(char *url_path, char *file, size_t size) {    
    ma_bool_t b_rc = MA_FALSE;
    if(url_path && file && (size > strlen(url_path)) && *url_path == '/') {
        char *token = NULL;
        char *str = url_path;
        while(*str) {
            if(('/' == *str) ||  ('\\' == *str))
                *str = MA_PATH_SEPARATOR;
            str++;
        }    

        token = strtok(url_path, MA_PATH_SEPARATOR_STR);  
        
        /* First level directory is always "Software" */        
        if(token && (!MA_WIN_SELECT(stricmp,strcasecmp)(token, MA_HTTP_REPO_SOFTWARE_FOLDER_STR))) {
            if(token = strtok(NULL, MA_PATH_SEPARATOR_STR)) {
                strcpy(file, MA_PATH_SEPARATOR_STR);
                while(token) {                             
                        strcat(file, token);
                        token = strtok(NULL, MA_PATH_SEPARATOR_STR);
                        if(token) strcat(file, MA_PATH_SEPARATOR_STR);
                }             
                b_rc = MA_TRUE;
            }            
        }        
    }
    return b_rc;
}

/* file -> \\Previous\\VSCANDAT1000\DAT\0000\PkgCatalog.z for Windows
        -> /Previous/VSCANDAT1000/DAT/0000/PkgCatalog.z for *nix
   expected local_file_path -> <root_dir>\\Previous\\VSCANDAT1000\DAT\0000\PkgCatalog.z for windows
                            -> <root_dir>/Previous/VSCANDAT1000/DAT/0000/PkgCatalog.z for *nix
*/
ma_bool_t get_local_file_path_for_file(const char *file, const char *root_dir, char *local_file_path, size_t size) {
    ma_bool_t b_rc = MA_FALSE;
    if(file && root_dir && local_file_path && size > (strlen(file) + strlen(root_dir) + 1)) {
		size_t base_len = strlen(root_dir);
		size_t file_len = strlen(file);
		if((root_dir[base_len-1] != MA_PATH_SEPARATOR) && (file[0] != MA_PATH_SEPARATOR))
			MA_MSC_SELECT(_snprintf,snprintf)(local_file_path, size, "%s%c%s", root_dir, MA_PATH_SEPARATOR, file);
		else {
			if((root_dir[base_len - 1] == MA_PATH_SEPARATOR) && (file[0] == MA_PATH_SEPARATOR))
				MA_MSC_SELECT(_snprintf,snprintf)(local_file_path, size, "%s%s", root_dir, file+1);
			else
				MA_MSC_SELECT(_snprintf,snprintf)(local_file_path, size, "%s%s", root_dir, file);
		}
        b_rc = MA_TRUE;
    }
    return b_rc;
}

ma_bool_t is_request_from_valid_client(ma_context_t *context, const char *peer_name) {
    ma_bool_t b_rc = MA_FALSE;
    if(context && peer_name) {
        ma_repository_list_t *repo_list = NULL;
        /* TBD - Add Logs for failures */
        if(MA_OK == ma_repository_get_repositories(context, MA_REPOSITORY_GET_REPOSITORIES_FILTER_NONE, &repo_list)) {
			size_t	num_repos = 0, iter = 0 ;

			(void)ma_repository_list_get_repositories_count(repo_list, &num_repos) ;

			for(iter = 0 ; iter < num_repos ; iter ++) {
				ma_repository_t *repo = NULL ;

				ma_repository_list_get_repository_by_index(repo_list, iter, &repo) ;
				if( repo && (MA_REPOSITORY_URL_TYPE_SPIPE == repo->url_type) ) {
					char *server_ip = NULL, *server_name = NULL, *server_fqdn = NULL;

					(void)ma_repository_get_server_ip(repo, &server_ip) ;
					(void)ma_repository_get_server_name(repo, &server_name) ;
					(void)ma_repository_get_server_fqdn(repo, &server_fqdn);
					if( (server_ip && ma_compare_with_peer_address(server_ip, peer_name))|| (server_name && ma_compare_with_peer_address(server_name, peer_name)) || (server_fqdn && ma_compare_with_peer_address(server_fqdn, peer_name))) {
						b_rc = MA_TRUE;
						(void)ma_repository_release(repo) ;	repo = NULL ;
						break;
					}
				}
				(void)ma_repository_release(repo) ;	repo = NULL ;
			}
			(void)ma_repository_list_release(repo_list) ;
        }
    }
    return b_rc;
}

const char *get_mime_type(char *file_name) {
    if(file_name) {
        char *suffix = strrchr(file_name, '.');
        if(suffix) {
            ma_int32_t index = 0;
            char *extn = suffix+1;
            while(mime_list[index][0]) {
                if(0 == MA_WIN_SELECT(stricmp,strcasecmp)(extn, mime_list[index][0])) {
                    return mime_list[index][1];
                }
                index++;
            }
        }
    }
    return "text/html";  /* default mime type is text/html */
}


