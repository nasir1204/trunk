#ifndef MA_HTTP_PROXY_HANDLER_INTERNAL_H
#define MA_HTTP_PROXY_HANDLER_INTERNAL_H

#include "ma/internal/services/http_server/ma_http_request_handler.h"
#include "ma/internal/utils/datastructures/ma_vector.h"
#include "ma/internal/utils/datastructures/ma_bytebuffer.h"

#include "ma_http_connection.h"

#include "uv.h"

MA_CPP(extern "C" {)

typedef struct ma_http_proxy_handler_s ma_http_proxy_handler_t, *ma_http_proxy_handler_h;

/* container of upstream connection */
typedef struct ma_http_proxy_connection_pool_s ma_http_proxy_connection_pool_t, *ma_http_proxy_connection_pool_h;

typedef struct ma_http_proxy_upstream_connection_s ma_http_proxy_upstream_connection_t, *ma_http_proxy_upstream_connection_h;

#define MA_HTTP_MAX_HOSTNAME_LEN 254

typedef struct ma_http_server_info_s {
    char hostname[MA_HTTP_MAX_HOSTNAME_LEN];
    ma_uint16_t port;
} ma_http_server_info_t;

/*!
 * pool request object
 */
typedef struct ma_http_proxy_connection_pool_request_s ma_http_proxy_connection_pool_request_t, *ma_http_proxy_connection_pool_request_h;

/*!
 * request callback
 * @param pool_req   the request object for this operation
 * @param status 0   indicates success and should be followed by a valid connection objection up for grabs
 * @param connection the resulting connection object already connected to the requested server
 */
typedef void (*ma_http_proxy_connection_pool_request_cb)(ma_http_proxy_connection_pool_request_t *pool_req, int status, ma_http_proxy_upstream_connection_t *connection);

struct ma_http_proxy_connection_pool_request_s {

    ma_http_proxy_connection_pool_t             *pool;

    ma_http_proxy_connection_pool_request_cb    cb;

    /*!
     * server we are looking for
     */
    ma_http_server_info_t                       server_info;

    /*!
     * @private
     */
    uv_getaddrinfo_t                            addr_info_req;

    /*!
     * @private
     * Results of DNS lookup
     */
    struct addrinfo                             *addr_info_begin, *addr_info_iter;

    /*!
     * socket to upstream server
     * Note, we use a pointer here, so we can pass ownership of it to the connection
     */
    uv_tcp_t                                    *ps_socket;

    /*!
     * 
     */
    uv_connect_t                                connect_req;


    /*!
     * 
     */
     uv_timer_t                                 timer;

     ma_bool_t                                  is_timer_started;

    /*!
     *  extra pointer to be used in async notification
     */
    ma_http_proxy_upstream_connection_t         *connection;

    /*!
     * opaque, user data
     */
    void                                        *data;
};


/*!
 * Creates a new instance of http_proxy_handler
 * The returned object may be cast to the general ma_http_request_handler interface
 */ 
ma_error_t ma_http_proxy_handler_create(ma_http_proxy_handler_t **proxy_handler, ma_http_proxy_connection_pool_t *pool);

void ma_http_proxy_connection_pool_init(ma_http_proxy_connection_pool_t *pool, struct uv_loop_s *uv_loop);

void ma_http_proxy_connection_pool_clear(ma_http_proxy_connection_pool_t *pool);

void ma_http_proxy_connection_pool_remove(ma_http_proxy_connection_pool_t *pool, ma_http_proxy_upstream_connection_t *connection);

ma_error_t ma_http_proxy_connection_pool_request_init(ma_http_proxy_connection_pool_request_t *pool_req, ma_http_proxy_connection_pool_t *pool);

ma_error_t ma_http_proxy_connection_pool_request_start(ma_http_proxy_connection_pool_request_t *pool_req, ma_http_server_info_t const *server_info, ma_http_proxy_connection_pool_request_cb cb);

ma_error_t ma_http_proxy_connection_pool_request_stop(ma_http_proxy_connection_pool_request_t *pool_req);

/* TODO - will ocnsolidate request and response objects if possible */
typedef struct ma_http_response_s {    
    http_parser         parser;   

    ma_bool_t           is_content_header;

    ma_bool_t           has_headers;

    size_t              content_length;

    size_t              received_data;

    ma_vector_define(response, uv_buf_t);

} ma_http_response_t;


struct ma_http_proxy_handler_s {
    ma_http_request_handler_t               base;

    ma_http_proxy_connection_pool_request_t pool_req;    

    const char                              *url_path; /* this includes path, query, fragment etc. */

    unsigned                                has_full_request:1;

    /* ma_bytebuffer_t                         *req_body; */

    uv_write_t                              ps_write_req;       

    ma_http_response_t                      response;

    /* extant stuff */

    ma_http_proxy_connection_pool_t         *pool;

    ma_http_connection_t                    *downstream_connection;
    
    ma_http_proxy_upstream_connection_t     *upstream_connection;


};


struct ma_http_proxy_connection_pool_s {
    /*!
     * list of upstream_connections
     */
    ma_queue_t  qhead;

    size_t      size;

    uv_loop_t   *uv_loop;
};

typedef enum ma_http_proxy_upstream_connection_flags_e {

    MA_HTTP_PROXY_UPSTREAM_CONNECTION_BUSY = 0x1,

} ma_http_proxy_upstream_connection_flags_t;

struct ma_http_proxy_upstream_connection_s {

    /*!
     * server hostname and port for this connection
     */
    ma_http_server_info_t   server_info;

    /*!
     * proxy -> server socket (this is a uv_stream and uv_handle)
     */
    uv_tcp_t                *ps_socket;

    /*!
     * combination of various flags
     */
    unsigned                flags;

    void                    *owner;

    ma_http_proxy_connection_pool_t         *pool;

    char                    upstream_buffer[2048];
    
    /*!
     * linked list of upstream_connections
     */
    ma_queue_t  qlink;
};


ma_error_t ma_http_proxy_upstream_connection_create(ma_http_proxy_connection_pool_t *pool, ma_http_proxy_upstream_connection_t **connection);

void ma_http_proxy_upstream_connection_set_owner(ma_http_proxy_upstream_connection_t *connection, void *owner);

ma_error_t ma_http_proxy_upstream_connection_stop(ma_http_proxy_upstream_connection_t *connection);

MA_CPP(})

#endif /* MA_HTTP_PROXY_HANDLER_INTERNAL_H */
