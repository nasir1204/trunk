#ifndef MA_HTTP_CONNECTION_MANAGER_H_INCLUDED
#define MA_HTTP_CONNECTION_MANAGER_H_INCLUDED

#include "ma/ma_common.h"

#include "ma/internal/utils/datastructures/ma_queue.h"

MA_CPP(extern "C" {)

typedef struct ma_http_connection_manager_s ma_http_connection_manager_t, *ma_http_connection_manager_h;

struct ma_http_connection_s;
enum ma_http_connection_type_e ;

struct ma_http_connection_manager_s {
    ma_queue_t qhead; /**< linked list of connections */
    };

/**
 * initializes the members of an already created connection manager instance
 */
void ma_http_connection_manager_init(ma_http_connection_manager_t *con_mgr);

void ma_http_connection_manager_uninit(ma_http_connection_manager_t *con_mgr);

ma_error_t ma_http_connection_manager_add(ma_http_connection_manager_t *con_mgr, struct ma_http_connection_s *connection);

ma_error_t ma_http_connection_manager_remove(ma_http_connection_manager_t *con_mgr, struct ma_http_connection_s *connection);

ma_error_t ma_http_connection_manager_stop_all(ma_http_connection_manager_t *con_mgr);

ma_error_t ma_http_connection_manager_abort_by_type(ma_http_connection_manager_t *con_mgr, enum ma_http_connection_type_e connection_type) ;

#endif /* MA_HTTP_CONNECTION_MANAGER_H_INCLUDED */
