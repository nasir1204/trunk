#include "ma_http_server_internal.h"

#include "ma_http_connection.h"

#include "ma/internal/utils/event_loop/ma_uv_helpers.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/services/http_server/ma_http_server_utils.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#ifdef MA_WINDOWS
#else
#include <sys/socket.h>
#endif

/* TBD - Similar macro is already defined in spipe handler header. should be moved to one http common macro file */
#define MA_SPIPE_FILE_REQUEST_URL_STR		"/spipe/file"


/******* libuv callbacks *****/
static void ma_uv_timer_cb(uv_timer_t *handle, int status);

static uv_buf_t ma_uv_alloc_cb(uv_handle_t *handle, size_t suggested_size);

static void ma_uv_read_cb(uv_stream_t *stream, ssize_t nread, uv_buf_t buf);

static void ma_uv_close_cb(uv_handle_t* handle);

/******** http_parser event handlers *******/
static int on_message_begin(http_parser*);

static int on_url(http_parser*, const char *at, size_t length);

static int on_status_complete(http_parser*);

static int on_header_field(http_parser*, const char *at, size_t length);

static int on_header_value(http_parser*, const char *at, size_t length);

static int on_headers_data(http_parser *parser, const char *at, size_t length);

static int on_headers_complete(http_parser*);

static int on_body(http_parser*, const char *at, size_t length);

static int on_message_complete(http_parser*);

/* misc helper functions */

static void ma_http_connection_prepare_request(ma_http_connection_t *self);

static const http_parser_settings ma_parser_settings = {
    &on_message_begin,
    &on_url,
    &on_status_complete,
    &on_header_field,
    &on_header_value,
    &on_headers_data,
    &on_headers_complete,
    &on_body,
    &on_message_complete,
};




ma_error_t ma_http_connection_create(struct ma_http_server_s *server, ma_http_connection_t **connection) {
    if (server) {
        ma_http_connection_t *self = calloc(1, sizeof(ma_http_connection_t) );
        if (self) {
            ma_http_request_init(&self->request);

            self->ref_count = 1;

            uv_tcp_init(server->uv_loop, &self->c_socket);
            self->c_socket.data = self;
            self->server = server;
            uv_timer_init(server->uv_loop, &self->timer);

            self->readtimeout_ms = MA_HTTP_SERVER_DEFAULT_READTIMEOUT_MS;

            *connection = self;
            return MA_OK;
        }
        return MA_ERROR_OUTOFMEMORY;
    }
    return MA_ERROR_INVALIDARG;
}

static void http_connection_destroy(ma_http_connection_t *self) {
    assert("Connection must be STOPPED upon release" && MA_HTTP_CONNECTION_STATE_STOPPED == self->state);
    MA_LOG(self->server->logger, MA_LOG_SEV_DEBUG, "ma_http_connection_t(%p)::destroy()", self);
    ma_http_request_handler_release(self->cur_req_handler);
    ma_http_request_clear(&self->request);
   
    if(self->cur_total_headers.base) 
        free(self->cur_total_headers.base), self->cur_total_headers.base = NULL, self->cur_total_headers.len = 0;
   
    free(self);
}

void ma_http_connection_add_ref(ma_http_connection_t *self) {
    if (self) {
        ++self->ref_count;
    }
}

void ma_http_connection_release(ma_http_connection_t *self) {
    if (self && 0 == --self->ref_count) {
        http_connection_destroy(self);
    }
} 

ma_error_t ma_http_connection_accept(ma_http_connection_t *self, uv_stream_t *server) {
    if (self) {
        if (uv_accept(server, (uv_stream_t *) &self->c_socket)) {
            /* some error */
            /*uv_err_t uv_err = uv_last_error(self->server->uv_loop);*/
            return MA_ERROR_FROM_UV_LAST_ERROR(self->server->uv_loop);
        } else {
            int namelen = sizeof(self->sa);            
            ma_uint16_t port = 0;
			if (UV_OK == uv_tcp_getpeername(&self->c_socket, (struct sockaddr *) &self->sa, &namelen)) {
                if (AF_INET6 == self->sa.ss_family) { 
                    struct sockaddr_in6 *sa6 = (struct sockaddr_in6 *) &self->sa;
                    uv_ip6_name(sa6, self->peer_addr, MA_COUNTOF(self->peer_addr));
                    port = ntohs(sa6->sin6_port);

                } else {
                    struct sockaddr_in *sa4 = (struct sockaddr_in *) &self->sa;
                    uv_ip4_name( sa4, self->peer_addr, MA_COUNTOF(self->peer_addr));
                    port = ntohs(sa4->sin_port);
                }
                MA_LOG(self->server->logger, MA_LOG_SEV_INFO, "ma_http_connection_t(%p) accepting tcp connection from %s:%u", self, self->peer_addr, (unsigned) port);
            }

            return MA_OK;
        } 
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_http_connection_start(ma_http_connection_t *self) {
    if (self) {
        assert("connection can only be started from the STOPPED state" && MA_HTTP_CONNECTION_STATE_STOPPED == self->state );
        if (MA_HTTP_CONNECTION_STATE_STOPPED == self->state) {
            uv_read_start( (uv_stream_t *) &self->c_socket, &ma_uv_alloc_cb, &ma_uv_read_cb);
            ma_http_connection_idletimer_restart(self);
            self->state = MA_HTTP_CONNECTION_STATE_STARTED;
            ma_http_connection_prepare_request(self);
            return MA_OK;
        } else {
            return MA_ERROR_UNEXPECTED;
        }
    }
    return MA_ERROR_INVALIDARG;
}

static void timer_close_cb(uv_handle_t *handle) {
    ma_http_connection_t *self = ma_queue_data(handle, ma_http_connection_t, timer);

    MA_LOG(self->server->logger, MA_LOG_SEV_DEBUG, "Http connection(%p) timer close cb", self);
    /* this should be the last activity */

    /* Notify server that connection is closed */

    ma_http_server_on_connection_stopped(self->server, self);
} 

static void ma_uv_close_cb(uv_handle_t *handle) {
    ma_http_connection_t *self = ma_queue_data(handle, ma_http_connection_t, c_socket);

    MA_LOG(self->server->logger, MA_LOG_SEV_DEBUG, "Http connection(%p) close cb", self);

    self->state = MA_HTTP_CONNECTION_STATE_STOPPED;

    uv_close((uv_handle_t *) &self->timer, &timer_close_cb);

}


ma_error_t ma_http_connection_stop(ma_http_connection_t *self) {
    if (self) {
        MA_LOG(self->server->logger, MA_LOG_SEV_DEBUG, "Http connection(%p) stopping, current state (%d)", self, self->state);
        if (MA_HTTP_CONNECTION_STATE_STARTED == self->state) {
            ma_http_request_handler_stop(self->cur_req_handler);
            uv_timer_stop(&self->timer);
            uv_read_stop( (uv_stream_t *) &self->c_socket);
            /* uv_close will cancel any outstanding write requests */
            uv_close( (uv_handle_t *) &self->c_socket, ma_uv_close_cb);
            self->state = MA_HTTP_CONNECTION_STATE_STOPPING;
            return MA_OK;
        } else {
            /* wrong state */
            return MA_ERROR_UNEXPECTED;
        }
    }
    return MA_ERROR_INVALIDARG;
}

void ma_http_connection_notify_request_complete(ma_http_connection_t *self) {
    ma_http_connection_prepare_request(self);
}

void ma_http_connection_idletimer_restart(ma_http_connection_t *self) {
    (void) uv_timer_stop(&self->timer);
    if (self->readtimeout_ms) {
        uv_timer_start(&self->timer, ma_uv_timer_cb, self->readtimeout_ms, 0);
    }
}



/* tries to allocate as much as was asked for, if that fails, tries some smaller sizes
   memory is freed in the corresponding ma_uv_read_cb
*/
static uv_buf_t ma_uv_alloc_cb(uv_handle_t *handle, size_t suggested_size) {
    uv_buf_t result = {0};
    size_t size;
    for (size = suggested_size; size && NULL == (result.base = malloc(size)); size /= 2) {;}
    result.len = size;
    return result;
}

static void ma_uv_read_cb(uv_stream_t *stream, ssize_t nread, uv_buf_t buf) {
    ma_http_connection_t *self = ma_queue_data(stream, ma_http_connection_t, c_socket);
    ma_bool_t needs_parse = MA_FALSE;
    if (nread < 0) {
        /* some error occurred */
        uv_err_t uv_err = uv_last_error(stream->loop);
        if (UV_EOF == uv_err.code) {
            needs_parse = MA_TRUE; /* need to tell the parser there is no more data coming (to terminate a body without a content-length) */
            nread = 0;
            self->close_after_write = 1;
        } else {
            /* any other error, we kill the connection */
            ma_http_connection_stop(self);
        }

    } else if (0 < nread) {
        /* feed to parser */
        needs_parse = MA_TRUE;
    } else {

    }
    
    if (needs_parse) {
        size_t nparsed = http_parser_execute(&self->request.parser, &self->parser_callbacks, buf.base, nread);
        ma_http_connection_idletimer_restart(self);
        if (self->request.parser.upgrade) {
            /* handle new protocol */
        } else if (nparsed != nread) {
            enum http_errno http_err = HTTP_PARSER_ERRNO(&self->request.parser);
            MA_LOG(self->server->logger, MA_LOG_SEV_INFO, "error parsing http request: <%s>", http_errno_name(http_err));
            
            /* Handle error or a stop signal from one of the callbacks */
            if(HPE_CB_url == http_err) {
                /* Respond with 400 as this error will come when http server receives the invalid URLs */ 
                self->close_after_write = 1;                
                ma_http_connection_send_stock_response(self, MA_HTTP_RESPONSE_400_BAD_REQUEST);
            }
            else 
                ma_http_connection_stop(self);
        }
    }


    /* Note, we must ensure entire buffer has been consumed at this point */

    /* make sure to always free the buffer regardless of above */
    if (buf.base) free(buf.base);
}

static void ma_uv_timer_cb(uv_timer_t *handle, int status) {

    ma_http_connection_t *self = ma_queue_data(handle, ma_http_connection_t, timer);

	if(self->cur_req_handler && self->is_cur_req_handler_busy) {
		MA_LOG(self->server->logger, MA_LOG_SEV_TRACE, "ma_http_connection_t(%p) timer restart as current handler is still busy", self);
		ma_http_connection_idletimer_restart(self);
	}
	else {
		MA_LOG(self->server->logger, MA_LOG_SEV_DEBUG, "ma_http_connection_t(%p) timed out, stopping the fun...", self);
		ma_http_connection_stop(self);
	}
}

static int append_buf_data(uv_buf_t *buf, const char *at, size_t length) {
    if (length) {
        const size_t new_length = buf->len + length;
        char *p = realloc(buf->base, 1+new_length); /* allocate an extra char for null termination as a courtesy */ /* TODO: check realloc is ok to call with a NULL buf->base*/
        if (!p) return -1;
        memcpy(&p[buf->len], at, length);
        p[new_length] = 0; /* insert null termination for ease of debugging etc. */
        buf->base = p;
        buf->len = new_length;
    }
    return 0;
}

static void ma_http_connection_prepare_request(ma_http_connection_t *self) {
    MA_LOG(self->server->logger, MA_LOG_SEV_DEBUG, "ma_http_connection_t(%p) ma_http_connection_prepare_request", self);
    /* reset parser to point back to us */
    self->parser_callbacks = ma_parser_settings;
    http_parser_init(&self->request.parser, HTTP_REQUEST);
    self->request.parser.data = self;

    /* take back hijacked read callback */
    self->c_socket.read_cb = &ma_uv_read_cb;

    /* 'de-allocate' extra send data from previous request if any */
    ma_http_buffer_init(&self->send_data);

    ma_http_request_handler_release(self->cur_req_handler);
	self->is_cur_req_handler_busy = 0;
    self->cur_req_handler = 0;
}

/* any event handler that returns non 0 will abort the parsing */
static int on_message_begin(http_parser *parser) {
    ma_http_connection_t *self = ma_queue_data(parser, ma_http_connection_t, request);
    ma_http_request_clear(&self->request);
    if(self->cur_total_headers.base) 
        free(self->cur_total_headers.base), self->cur_total_headers.base = NULL, self->cur_total_headers.len = 0;
   
    return 0;
}

/* handling HTTP vulnerabilities :
   http://localhost:8081/../../../../../../windows/win.ini
*/
static int is_valid_url_path(ma_http_connection_t *self) {
    char url_path[MA_HTTP_MAX_URL_PATH_LEN] = {0};
    if(get_url_path_from_url(&self->request.url, url_path, MA_HTTP_MAX_URL_PATH_LEN, 0)) {
        return NULL != strstr(url_path, "..");
    }
    return -1;
}

static int on_url(http_parser *parser, const char *at, size_t length) {
    ma_http_connection_t *self = ma_queue_data(parser, ma_http_connection_t, request);
    if(!append_buf_data(&self->request.url, at, length))
        return ((HTTP_POST == parser->method) || (HTTP_GET == parser->method)) && is_valid_url_path(self);
    return -1;
}

static int on_status_complete(http_parser *parser) {
    return 0;
}

static int on_header_field(http_parser *parser, const char *at, size_t length) {
    /*  header fields do not include the ':' delimiter */
    ma_http_connection_t *self = ma_queue_data(parser, ma_http_connection_t, request);

    if (!self->cur_hdr_field) {
        ma_http_header_t *hdr = calloc(1, sizeof(ma_http_header_t));
        if (!hdr) return -1;
        ma_queue_insert_tail(&self->request.headers, &hdr->qlink);
        self->cur_hdr_field = &hdr->field;
        self->cur_hdr_value = &hdr->value;
    }
    return append_buf_data(self->cur_hdr_field, at, length);
}

static int on_header_value(http_parser *parser, const char *at, size_t length) {
    ma_http_connection_t *self = ma_queue_data(parser, ma_http_connection_t, request);
    self->cur_hdr_field = NULL; /* to ensure we create a new header on next on_header_field() */
    return append_buf_data(self->cur_hdr_value, at, length);
}

static int on_headers_data(http_parser *parser, const char *at, size_t length) {
    ma_http_connection_t *self = ma_queue_data(parser, ma_http_connection_t, request);
    char url_path[MA_HTTP_MAX_URL_PATH_LEN] = {0};    
    ma_bool_t is_spipe_file_req = get_url_path_from_url(&self->request.url, url_path, MA_HTTP_MAX_URL_PATH_LEN, 0) && !strcmp(url_path, MA_SPIPE_FILE_REQUEST_URL_STR);    
    
    /* Collecting total headers only for spipe file requests */
    if(((HTTP_POST == parser->method) || ( HTTP_GET == parser->method)) && is_spipe_file_req) {
        self->cur_total_headers = uv_buf_init(0, 0);
        return append_buf_data(&self->cur_total_headers, at, length);
    }

     return 0;
}

static int on_headers_complete(http_parser *parser) {
    ma_http_connection_t *self = ma_queue_data(parser, ma_http_connection_t, request);
    ma_queue_t *qitem;

    self->close_after_write = 0 == http_should_keep_alive(&self->request.parser);

	/*Note:  This should not happen still instead of assert we are rejecting the conneciton request */
    if(0 != self->cur_req_handler) {
		self->parser_callbacks.on_message_complete = 0;
		MA_LOG(self->server->logger, MA_LOG_SEV_DEBUG, "ma_http_connection_t(%p) is busy with request_handler(%p)", self, self->cur_req_handler);
		ma_http_connection_send_stock_response(self, MA_HTTP_RESPONSE_503_SERVICE_UNAVAILABLE);
		return 0;
	}

    /* Now search the factories for a request handler for this request */
    ma_queue_foreach(qitem,&self->server->handler_factories.qhead) {
        ma_http_request_handler_factory_t *factory = ma_queue_data(qitem, ma_http_request_handler_factory_t, qlink);
        if (ma_http_request_handler_factory_matches(factory, &self->request)) {
            self->parser_callbacks.on_message_complete = 0;
            if (ma_http_request_handler_factory_authorize(factory, &self->request)) {
                /* handler found and authorized; now create and start handler */
                ma_http_request_handler_t *handler = 0;
                if (MA_OK == ma_http_request_handler_factory_create_instance(factory, &handler) && MA_OK == ma_http_request_handler_start(handler, self)) {
                    self->cur_req_handler = handler;

                    MA_LOG(self->server->logger, MA_LOG_SEV_DEBUG, "ma_http_connection_t(%p) now associated with request_handler(%p)", self, handler);
                    return 0;
                }
                /* something went wrong, try undo everything (and continue searching) */
                self->close_after_write = 1;

                MA_LOG(self->server->logger, MA_LOG_SEV_WARNING, "ma_http_connection_t(%p) unable to start request_handler(%p)", self, handler);
                ma_http_request_handler_release(handler);   
                
                ma_http_connection_send_stock_response(self, MA_HTTP_RESPONSE_500_INTERNAL_SERVICE);        

                return 0;
            } else {
                MA_LOG(self->server->logger, MA_LOG_SEV_DEBUG, "ma_http_connection_t(%p) on_headers_complete, request forbidden", self);
                self->close_after_write = 1;
                /* found, but not authorized*/                
                ma_http_connection_send_stock_response(self, MA_HTTP_RESPONSE_403_FORBIDDEN);
                return 0;
            }
        }
    }
    
    self->parser_callbacks.on_message_complete = &on_message_complete;
    return 0;
}

static int on_body(http_parser *parser, const char *at, size_t length) {
    return 0;
}

static int on_message_complete(http_parser *parser) {
    /* getting here means there was nobody else to handle the request - just reply with a 404 Not Found message */
    ma_http_connection_t *self = ma_queue_data(parser, ma_http_connection_t, request);
    MA_LOG(self->server->logger, MA_LOG_SEV_DEBUG, "ma_http_connection_t(%p) on_message_complete", self);
    self->close_after_write = 1;
	/* if request method is HEAD, just send HTTP 200 OK to keep compaibility */
	ma_http_connection_send_stock_response(self, (HTTP_HEAD == parser->method) ? MA_HTTP_RESPONSE_200_OK:MA_HTTP_RESPONSE_404_NOT_FOUND);
    return 0;
}

static void ma_uv_write_cb(uv_write_t *req, int status) {
    ma_http_connection_t *self = ma_queue_data(req, ma_http_connection_t, c_write_req);

    MA_LOG(self->server->logger, MA_LOG_SEV_DEBUG, "ma_http_connection_t(%p) ma_uv_write_cb, status(%d)", self, status);
    if (UV_OK == status) {
        if (!self->close_after_write) {
            ma_http_connection_notify_request_complete(self);
            return; /* keep connection open  */
        }
    } else {
        /* TODO Add diagnostic */
    }
    ma_http_connection_stop(self);
}


void ma_http_connection_send_stock_response(ma_http_connection_t *self, char const *status_text) {
    const size_t bytes_avail = ma_http_buffer_bytes_available(&self->send_data);
    const size_t status_len = strlen(status_text);
    int n = MA_MSC_SELECT(_snprintf,snprintf)(ma_http_buffer_peek(&self->send_data), bytes_avail, "%u", (unsigned)status_len );
    char *p = (0 < n && (size_t)n < bytes_avail) ? ma_http_buffer_advance(&self->send_data, n) : 0;

    uv_buf_t write_data[] = {
        MA_UV_BUF_TEXT(MA_HTTP_RESPONSE_HTTP1_1), MA_UV_BUF(status_text, status_len), MA_UV_BUF_TEXT(MA_HTTP_CRLF),
        MA_UV_BUF_TEXT(MA_HTTP_RESPONSE_HEADER_FIELD_CONTENT_TYPE), MA_UV_BUF_TEXT(MA_HTTP_RESPONSE_HEADER_CONTENT_TYPE_TEXTPLAIN), MA_UV_BUF_TEXT(MA_HTTP_CRLF),
        MA_UV_BUF_TEXT(MA_HTTP_RESPONSE_HEADER_FIELD_CONTENT_LENGTH), MA_UV_BUF(p, n), MA_UV_BUF_TEXT(MA_HTTP_CRLF),
        MA_UV_BUF_TEXT(MA_HTTP_CRLF),
        MA_UV_BUF(status_text, status_len),
    };

    MA_LOG(self->server->logger, MA_LOG_SEV_DEBUG, "ma_http_connection_t(%p) ma_http_connection_send_stock_response, response(%s)", self, status_text);

    if (UV_OK != uv_write(&self->c_write_req, (uv_stream_t *) &self->c_socket, write_data, MA_COUNTOF(write_data), &ma_uv_write_cb)) {
        /* TODO Add more diagnostic to help troubleshoot failure */
        ma_http_connection_stop(self);
    }
}
