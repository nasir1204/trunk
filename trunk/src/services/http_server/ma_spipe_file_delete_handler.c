
#include "ma_spipe_handler_internal.h"
#include "ma/internal/services/http_server/ma_http_server_utils.h"

#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/utils/configurator/ma_configurator.h"

#include "ma/internal/services/p2p/ma_p2p_db_manager.h"
#include "ma/internal/utils/filesystem/ma_fs_utils.h"
/* TBD - Recursive deletion of directory if input directory has files and directories */
ma_error_t ma_spipe_file_delete_handler(ma_spipe_handler_t *self, const char *file_name) {
	ma_spipe_handler_factory_t *factory = (ma_spipe_handler_factory_t *)self->base.data;    
    ma_http_server_t *server = (ma_http_server_t *)factory->base.data;

    ma_error_t rc = MA_ERROR_HTTP_SERVER_INTERNAL;  
    if(get_local_file_path_for_file(file_name, server->policies.virtual_dir, self->local_full_file_path, MAX_PATH_LEN)) {
		char *urn = NULL ;
		ma_db_t *db_handle = ma_configurator_get_msgbus_database(MA_CONTEXT_GET_CONFIGURATOR(factory->context)) ;

        uv_fs_t fs_req;
        rc = MA_OK;

		urn = self->local_full_file_path ;
		urn = urn + strlen(server->policies.virtual_dir)+1 ;

        if(uv_fs_stat(self->connection->server->uv_loop, &fs_req, self->local_full_file_path, NULL))
            rc = MA_ERROR_HTTP_SERVER_REQUEST_NOT_FOUND;
        else {
            ma_bool_t is_dir = ma_fs_is_dir(self->connection->server->uv_loop, self->local_full_file_path);
            uv_fs_req_cleanup(&fs_req);
            if(is_dir) {
                /* This removes only empty directory */
                if(uv_fs_rmdir(self->connection->server->uv_loop, &fs_req, self->local_full_file_path, NULL))
                    rc = MA_ERROR_HTTP_SERVER_REQUEST_FORBIDDEN;
                else {
                    uv_fs_req_cleanup(&fs_req);
					/* Delete reference from p2p */
					(void)ma_p2p_db_remove_content_by_urn(db_handle, "LAZY_CACHE", urn, MA_TRUE) ;
				}
            }
            else { /* TBD - Treated as a regular file, Will check for other types too */
                if(uv_fs_unlink(self->connection->server->uv_loop, &fs_req, self->local_full_file_path, NULL))
                    rc = MA_ERROR_HTTP_SERVER_REQUEST_FORBIDDEN;
                else {
                    uv_fs_req_cleanup(&fs_req);
					/* Delete reference from p2p */
					(void)ma_p2p_db_remove_content_by_urn(db_handle, "LAZY_CACHE", urn, MA_FALSE) ;
				}
            }    
        }
    }
   
	if(MA_OK == rc) ma_spipe_handler_send_response(self, rc);
	return rc;
}

