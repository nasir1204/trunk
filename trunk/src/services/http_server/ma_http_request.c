#include "ma_http_connection.h"

#include <stdlib.h>
#include <string.h>

void ma_http_request_init(ma_http_request_t *self) {
    ma_queue_init(&self->headers);
    self->url = uv_buf_init(0,0);
}

ma_bool_t ma_http_request_search_header(ma_http_request_t *self, const char *field, uv_buf_t *value) {
    ma_bool_t b_rc = MA_FALSE;
    if(self && field) {
        ma_queue_t *qitem = NULL;
        ma_queue_foreach(qitem, &self->headers) {
            ma_http_header_t *hdr = ma_queue_data(qitem, ma_http_header_t, qlink);
            if(b_rc = !strcmp(hdr->field.base, field)) {
                value->base = hdr->value.base;
                value->len = hdr->value.len;
                break;
            }
        }
    }
    return b_rc;
}

void ma_http_request_clear(ma_http_request_t *self) {
    while (!ma_queue_empty(&self->headers)) {
        ma_queue_t *qitem = ma_queue_last(&self->headers);
        ma_http_header_t *hdr = ma_queue_data(qitem, ma_http_header_t, qlink);
        if (hdr->field.base) free(hdr->field.base);
        if (hdr->value.base) free(hdr->value.base);
        ma_queue_remove(qitem);
        free(hdr);
    }
    if (self->url.base) free(self->url.base);

    ma_http_request_init(self);
}
