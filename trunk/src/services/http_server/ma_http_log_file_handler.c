#include "ma_http_connection.h"

#include "ma/internal/services/ma_policy_settings_bag.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_message.h"

#include "ma/internal/services/http_server/ma_http_server_utils.h"
#include "ma/internal/services/io/ma_logger_db.h"
#include "ma/internal/utils/json/ma_json_utils.h"
#include "ma_http_server_internal.h"

#include <stdio.h>
#include <fcntl.h>


typedef struct ma_log_file_handler_s {
    ma_http_request_handler_t			base;

	ma_http_connection_t				*connection;

    char                                root_dir[MA_HTTP_MAX_URL_PATH_LEN];

	unsigned char						buffer[MA_HTTP_REPONSE_DATA_SIZE] ;

	ma_int64_t							cur_position;

	ma_int64_t							content_size ;	

	ma_bytebuffer_t						*send_json_buffer;

    uv_fs_t                             fs_open_req;

    uv_fs_t                             fs_read_req;

    uv_file                             file;

    char                                mime_type[MA_MAX_LEN];

    char                                content_disposition[MA_MAX_LEN];
} ma_log_file_handler_t;

/* Log file handler */
static ma_error_t log_file_handler_start(ma_http_request_handler_t *handler, struct ma_http_connection_s *connection);
static ma_error_t log_file_handler_stop(ma_http_request_handler_t *handler);
static void log_file_handler_destroy(ma_http_request_handler_t *handler);

static ma_error_t process_request(ma_log_file_handler_t *handler, const char *file_name);
static ma_error_t read_data(ma_log_file_handler_t *handler);

const static ma_http_request_handler_methods_t log_file_handler_methods = {
    &log_file_handler_start,
    &log_file_handler_stop,
    &log_file_handler_destroy
};

/* Log file handler Factory */

typedef struct ma_log_file_handler_factory_s {
	ma_http_request_handler_factory_t	base;	
    
	ma_logger_t							*logger ;
}ma_log_file_handler_factory_t;

static ma_bool_t log_file_handler_factory_matches(ma_http_request_handler_factory_t *self, ma_http_request_t const *request);
static ma_bool_t log_file_handler_factory_authorize(ma_http_request_handler_factory_t *self, ma_http_request_t const *request);
static ma_error_t log_file_handler_factory_configure(ma_http_request_handler_factory_t *self);
static ma_error_t log_file_handler_factory_create_instance(ma_http_request_handler_factory_t *self, ma_http_request_handler_t **handler);
static ma_error_t log_file_handler_factory_start(ma_http_request_handler_factory_t *self);
static ma_error_t log_file_handler_factory_stop(ma_http_request_handler_factory_t *self);
static void log_file_handler_factory_destroy(ma_http_request_handler_factory_t *self);

const static ma_http_request_handler_factory_methods_t log_file_handler_factory_methods = {
    &log_file_handler_factory_matches,
	&log_file_handler_factory_authorize,
    &log_file_handler_factory_configure,
    &log_file_handler_factory_create_instance,
	&log_file_handler_factory_start,
	&log_file_handler_factory_stop,
    &log_file_handler_factory_destroy
};


/* * * * F A C T O R Y * * */
static ma_bool_t log_file_handler_factory_matches(ma_http_request_handler_factory_t *factory, struct ma_http_request_s const *request) {
    char url_path[MA_HTTP_MAX_URL_PATH_LEN] = {0};
    return (HTTP_GET == request->parser.method) && get_url_path_from_url(&request->url, url_path, MA_HTTP_MAX_URL_PATH_LEN, 0) && is_request_for_log_file(url_path);
}

static ma_bool_t log_file_handler_factory_authorize(ma_http_request_handler_factory_t *factory, ma_http_request_t const *request) {
    ma_log_file_handler_factory_t *self = (ma_log_file_handler_factory_t *)factory;
    ma_http_connection_t *connection = (ma_http_connection_t *)request->parser.data;
    ma_http_server_t *server = (ma_http_server_t *) self->base.data;
	
    if(server->policies.is_remote_log_enabled)
        return ma_http_server_request_validation_by_policy(server,request) ;
    else {
        MA_LOG(server->logger, MA_LOG_SEV_TRACE, "Remote access to log is disabled through policy");        
		return MA_FALSE;
	}
}

static ma_error_t log_file_handler_factory_configure(ma_http_request_handler_factory_t *factory) {
	ma_log_file_handler_factory_t *self = (ma_log_file_handler_factory_t *)factory;
	ma_error_t rc = MA_OK;   
    
    return rc;
}

static ma_error_t log_file_handler_factory_create_instance(ma_http_request_handler_factory_t *factory, ma_http_request_handler_t **handler) {
    ma_log_file_handler_t *instance = (ma_log_file_handler_t *)calloc(1, sizeof(ma_log_file_handler_t));
    if (instance) {
        ma_http_request_handler_init(&instance->base, &log_file_handler_methods, factory);
        *handler = &instance->base;
        return MA_OK;
    }
    return MA_ERROR_OUTOFMEMORY;
}

static ma_error_t log_file_handler_factory_start(ma_http_request_handler_factory_t *factory) {
	ma_log_file_handler_factory_t *self = (ma_log_file_handler_factory_t *)factory ;

	if(self)
		MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "http logfile handler started") ;

	return MA_OK ;
}

static ma_error_t log_file_handler_factory_stop(ma_http_request_handler_factory_t *factory) {
	ma_log_file_handler_factory_t *self = (ma_log_file_handler_factory_t *)factory ;

	if(self)
		MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "http logfile handler stoped") ;

	return MA_OK ;
}

static void log_file_handler_factory_destroy(ma_http_request_handler_factory_t *factory) {
	ma_log_file_handler_factory_t *self = (ma_log_file_handler_factory_t *)factory;

    free(self);
}

ma_error_t ma_log_file_handler_factory_create(ma_http_server_t *server, ma_http_request_handler_factory_t **log_file_handler_factory) {
    ma_log_file_handler_factory_t *self = (ma_log_file_handler_factory_t *)calloc(1, sizeof(ma_log_file_handler_factory_t));
    if (self) {
		ma_http_request_handler_factory_init(&self->base, &log_file_handler_factory_methods, server);
        self->logger = server->logger;
		*log_file_handler_factory = &self->base;
		return MA_OK;
    }
    return MA_ERROR_OUTOFMEMORY;
}
/* * * * * END F A C T O R Y * * * * */

static int on_log_file_message_body(http_parser *parser, const char *data, size_t length) {
    /* No data for log file requests */
    return 0;
}

static int on_log_file_message_complete(http_parser *parser) {
    ma_http_connection_t *connection = (ma_http_connection_t *)parser->data;
	ma_log_file_handler_t *handler = (ma_log_file_handler_t *) connection->cur_req_handler;
    ma_error_t rc =  MA_ERROR_HTTP_SERVER_INTERNAL;
    char *file_name = NULL;
    char url_path[MA_HTTP_MAX_URL_PATH_LEN] = {0};

    if(get_url_path_from_url(&connection->request.url, url_path, MA_HTTP_MAX_URL_PATH_LEN, 0)) {
        MA_LOG(connection->server->logger, MA_LOG_SEV_DEBUG, "ma_log_file_handler_t(%p) processing log file <%s> request", handler, url_path);

        if(!strcmp(url_path, "/") || !strcmp(url_path, "\\"))
            file_name = MA_HTTP_AGENT_LOG_HTML_FILE_STR;
        else 
            file_name = url_path+1;
    
        rc = process_request(handler, file_name);
    } 
	else
		(void) ma_http_connection_send_stock_response(connection, MA_HTTP_RESPONSE_500_INTERNAL_SERVICE);
    
    return 0;
}

static ma_error_t log_file_handler_start(ma_http_request_handler_t *handler, ma_http_connection_t *connection) {
    ma_log_file_handler_t *log_file_handler = (ma_log_file_handler_t *) handler;    
	ma_error_t rc = MA_OK;	
    ma_buffer_t *buffer = NULL;

    if(MA_OK == (rc = ma_ds_get_str(connection->server->ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_DATA_PATH_STR, &buffer))) {
        const char *data_path = NULL;
        size_t size = 0;
        if(MA_OK == (rc = ma_buffer_get_string(buffer, &data_path, &size))) {            
            memset(log_file_handler->root_dir, 0, MA_HTTP_MAX_URL_PATH_LEN);
            MA_MSC_SELECT(_snprintf, snprintf)(log_file_handler->root_dir, MA_HTTP_MAX_URL_PATH_LEN,"%s%c%s%c%s", data_path, MA_PATH_SEPARATOR, MA_AGENT_DATA_DIR_STR, MA_PATH_SEPARATOR,MA_AGENT_LOGGING_DATA_DIR_STR);
        }
        (void) ma_buffer_release(buffer);
    }

	if(MA_OK == rc) {
		log_file_handler->connection = connection;  
		connection->parser_callbacks.on_body = &on_log_file_message_body;	
		connection->parser_callbacks.on_message_complete = &on_log_file_message_complete;	
	}

    return rc;
}

static ma_error_t log_file_handler_stop(ma_http_request_handler_t *handler) {
    ma_log_file_handler_t *log_file_handler = (ma_log_file_handler_t *) handler;    
	return MA_OK;
}

static void log_file_handler_destroy(ma_http_request_handler_t *handler) {
    ma_log_file_handler_t *log_file_handler = (ma_log_file_handler_t *) handler;        
    free(log_file_handler);
}

static void file_close(ma_log_file_handler_t *handler) {
    uv_fs_t fs_close_req;
    uv_fs_close(handler->connection->server->uv_loop, &fs_close_req, handler->file, NULL) ;
    uv_fs_req_cleanup(&fs_close_req);
}

static void file_data_write_cb(uv_write_t* req, int status) {
	ma_http_connection_t *conn = ma_queue_data(req, ma_http_connection_t, c_write_req) ;
    ma_log_file_handler_t *handler = (ma_log_file_handler_t *)conn->cur_req_handler ;

	if(UV_OK == status) {
		if(handler->cur_position >= handler->content_size) {	/* content serve complete */
			MA_LOG(conn->server->logger, MA_LOG_SEV_DEBUG, "Log file handler request completed...") ;
		
            file_close(handler);
			if (!conn->close_after_write) {                
                ma_http_connection_notify_request_complete(conn) ;
                return; /* keep connection open  */
            }           
            
            ma_http_connection_stop(conn);			
		}
		else {
			if(MA_OK != read_data(handler)) {	
                file_close(handler);   
                handler->connection->close_after_write = 1;
				ma_http_connection_send_stock_response(handler->connection, MA_HTTP_RESPONSE_500_INTERNAL_SERVICE) ;
			}
		}
	}
	else {
		uv_err_t uv_err = uv_last_error(handler->connection->server->uv_loop) ;
        file_close(handler);       
        ma_http_server_log_uv_err(handler->connection->server, "UV Write failed while serving data to clients in write callback", MA_LOG_SEV_ERROR, uv_err) ;
        ma_http_connection_stop(conn) ;
	}
	return ;
}

static ma_error_t send_file_data_response(ma_log_file_handler_t *self, size_t content_size) { 
    ma_error_t rc = MA_OK ;    
	if(!self->cur_position) {
		const size_t bytes_avail = ma_http_buffer_bytes_available(&self->connection->send_data) ;
		int n = MA_MSC_SELECT(_snprintf,snprintf)((char *)ma_http_buffer_peek(&self->connection->send_data), bytes_avail, "%lld", self->content_size) ;        
        char *p = (0 < n && (size_t)n < bytes_avail) ? ma_http_buffer_advance(&self->connection->send_data, n) : 0 ;
		uv_buf_t content_data_with_header[] = {
                MA_UV_BUF_TEXT(MA_HTTP_RESPONSE_HTTP1_1),
				MA_UV_BUF(MA_HTTP_RESPONSE_200_OK, strlen(MA_HTTP_RESPONSE_200_OK)),
				MA_UV_BUF_TEXT(MA_HTTP_CRLF),
                MA_UV_BUF_TEXT(MA_HTTP_RESPONSE_HEADER_FIELD_CONTENT_TYPE),
				MA_UV_BUF(self->mime_type, strlen(self->mime_type)),
				MA_UV_BUF_TEXT(MA_HTTP_CRLF),
                MA_UV_BUF_TEXT(MA_HTTP_RESPONSE_HEADER_FIELD_CONTENT_LENGTH),
				MA_UV_BUF(p, n),
				MA_UV_BUF_TEXT(MA_HTTP_CRLF),
				MA_UV_BUF_TEXT(MA_HTTP_RESPONSE_HEADER_FIELD_X_FRAME_OPTIONS), 
				MA_UV_BUF(MA_HTTP_RESPONSE_HEADER_VALUE_X_FRAME_OPTION, strlen(MA_HTTP_RESPONSE_HEADER_VALUE_X_FRAME_OPTION)),	
				MA_UV_BUF_TEXT(MA_HTTP_CRLF),
                MA_UV_BUF_TEXT(MA_HTTP_RESPONSE_HEADER_FIELD_CONTENT_DISPOSITION),
                MA_UV_BUF(self->content_disposition, strlen(self->content_disposition)),
				MA_UV_BUF_TEXT(MA_HTTP_CRLF),
                MA_UV_BUF_TEXT(MA_HTTP_CRLF),
				MA_UV_BUF(self->buffer, content_size),
            } ;
		
		self->cur_position += content_size;
		if(UV_OK != uv_write(&self->connection->c_write_req, (uv_stream_t *)&self->connection->c_socket, content_data_with_header, MA_COUNTOF(content_data_with_header), &file_data_write_cb)) {
            uv_err_t uv_err = uv_last_error(self->connection->server->uv_loop) ;

			file_close(self);
            ma_http_server_log_uv_err(self->connection->server, "UV Write failed while serving data to clients", MA_LOG_SEV_ERROR, uv_err) ;
            ma_http_connection_stop(self->connection) ;
            rc = MA_ERROR_HTTP_SERVER_INTERNAL ;
		}
	}
	else {
		uv_buf_t content_data[] = {
				MA_UV_BUF(self->buffer, content_size),
			} ;

		self->cur_position += content_size;
		if (UV_OK != uv_write(&self->connection->c_write_req, (uv_stream_t *) &self->connection->c_socket, content_data, MA_COUNTOF(content_data), &file_data_write_cb)) {
            uv_err_t uv_err = uv_last_error(self->connection->server->uv_loop) ;

			file_close(self);
            ma_http_server_log_uv_err(self->connection->server, "UV Write failed while serving data to clients", MA_LOG_SEV_ERROR, uv_err) ;  
            ma_http_connection_stop(self->connection) ;
            rc = MA_ERROR_HTTP_SERVER_INTERNAL ;
        }
	}

    return rc ;
}

static void file_read_cb(uv_fs_t* req) {

	ma_error_t rc = MA_ERROR_HTTP_SERVER_INTERNAL ;
	ma_log_file_handler_t *handler = (ma_log_file_handler_t *)req->data;

	if(req->result != -1) {        
        rc = send_file_data_response(handler, req->result);                
    }
    else {        
       file_close(handler);
	}

    uv_fs_req_cleanup(req) ;

    if(MA_OK != rc) {
        handler->connection->close_after_write = 1;
        ma_http_connection_send_stock_response(handler->connection, MA_HTTP_RESPONSE_500_INTERNAL_SERVICE) ;
    }
}

static ma_error_t read_data(ma_log_file_handler_t *handler) {
	ma_error_t rc = MA_ERROR_HTTP_SERVER_INTERNAL ;
	handler->fs_read_req.data = handler ;

	if(-1 != uv_fs_read(handler->connection->server->uv_loop, &handler->fs_read_req, handler->file, handler->buffer, MA_HTTP_REPONSE_DATA_SIZE, handler->cur_position, file_read_cb)) {
		rc = MA_OK ;
	}
       
	return rc ;
}

static void file_open_cb(uv_fs_t* req) {
    ma_error_t rc = MA_ERROR_HTTP_SERVER_INTERNAL;
    ma_log_file_handler_t *handler = (ma_log_file_handler_t *)req->data;
    if(req->result >= 0) {
        uv_fs_t stat_req;
        handler->file = req->result;        
        if(!uv_fs_fstat(handler->connection->server->uv_loop, &stat_req, handler->file, NULL)) {
            /* TBD - length field in uv_fs_t structure is not providing the size of file, so using internal structure data for file size */
            /* These structures might be changed in UV newer versions, so this code should be updated according to UV changes */
#ifdef MA_WINDOWS
            struct _stat64 s = stat_req.statbuf;
#else
            struct stat s = stat_req.statbuf;
#endif
			handler->content_size = s.st_size ;			
			if(handler->content_size > 0) {
				rc = read_data(handler);
			}
            uv_fs_req_cleanup(&stat_req);
        }
    }

    uv_fs_req_cleanup(&handler->fs_open_req);   
    
    if(MA_OK != rc) {
		handler->connection->close_after_write = 1;
		ma_http_connection_send_stock_response(handler->connection, MA_HTTP_RESPONSE_403_FORBIDDEN);
	}
}

static ma_error_t read_file_from_disk(ma_log_file_handler_t *handler, const char *file_name) { 
    ma_error_t rc = MA_OK;
    handler->fs_open_req.data = handler;
    if(-1 == uv_fs_open(handler->connection->server->uv_loop, &handler->fs_open_req, file_name, O_RDONLY, 0, file_open_cb))
        rc = MA_ERROR_HTTP_SERVER_INTERNAL;
    return rc;
}

static void json_data_write_cb(uv_write_t* req, int status) {
	ma_http_connection_t *self = ma_queue_data(req, ma_http_connection_t, c_write_req);
	ma_log_file_handler_t *handler = (ma_log_file_handler_t *) self->cur_req_handler;
    if (UV_OK == status) {
		ma_bytebuffer_release(handler->send_json_buffer), handler->send_json_buffer = NULL;
        if (!self->close_after_write) {
            ma_http_connection_notify_request_complete(self);
            return; /* keep connection open  */
        }
    } else {
        /* TODO Add diagnostic */
    }
    ma_http_connection_stop(self);
}

static ma_error_t send_json_data_response(ma_log_file_handler_t *handler) { 
    ma_error_t rc = MA_ERROR_HTTP_SERVER_INTERNAL;
	if(handler->send_json_buffer) {
        const size_t bytes_avail = ma_http_buffer_bytes_available(&handler->connection->send_data);
        int n = MA_MSC_SELECT(_snprintf,snprintf)((char *)ma_http_buffer_peek(&handler->connection->send_data), bytes_avail, "%u", ma_bytebuffer_get_size(handler->send_json_buffer));
        char *p = (0 < n && (size_t)n < bytes_avail) ? ma_http_buffer_advance(&handler->connection->send_data, n) : 0;
            uv_buf_t write_data[] = {
            MA_UV_BUF_TEXT(MA_HTTP_RESPONSE_HTTP1_1), MA_UV_BUF(MA_HTTP_RESPONSE_200_OK, strlen(MA_HTTP_RESPONSE_200_OK)), MA_UV_BUF_TEXT(MA_HTTP_CRLF),
            MA_UV_BUF_TEXT(MA_HTTP_RESPONSE_HEADER_FIELD_CONTENT_TYPE), MA_UV_BUF(handler->mime_type, strlen(handler->mime_type)), MA_UV_BUF_TEXT(MA_HTTP_CRLF),
            MA_UV_BUF_TEXT(MA_HTTP_RESPONSE_HEADER_FIELD_CONTENT_LENGTH), MA_UV_BUF(p, n), MA_UV_BUF_TEXT(MA_HTTP_CRLF),
			MA_UV_BUF_TEXT(MA_HTTP_RESPONSE_HEADER_FIELD_X_FRAME_OPTIONS), MA_UV_BUF(MA_HTTP_RESPONSE_HEADER_VALUE_X_FRAME_OPTION, strlen(MA_HTTP_RESPONSE_HEADER_VALUE_X_FRAME_OPTION)), MA_UV_BUF_TEXT(MA_HTTP_CRLF),
            MA_UV_BUF_TEXT(MA_HTTP_CRLF),
            MA_UV_BUF(ma_bytebuffer_get_bytes(handler->send_json_buffer), ma_bytebuffer_get_size(handler->send_json_buffer)),
        };

        if (UV_OK != uv_write(&handler->connection->c_write_req, (uv_stream_t *) &handler->connection->c_socket, write_data, MA_COUNTOF(write_data), &json_data_write_cb)) {
            /* TODO Add more diagnostic to help troubleshoot failure */
            ma_http_connection_stop(handler->connection);
        }        
        rc = MA_OK;
    }
    return rc;
}

static ma_error_t read_agent_logs_from_db(ma_log_file_handler_t *handler) {    
	ma_error_t rc =  MA_ERROR_HTTP_SERVER_INTERNAL;
    ma_db_recordset_t *db_record = NULL;
    ma_json_array_t *json_array = NULL;
    ma_json_t *json = NULL;
    ma_buffer_t *buffer = NULL;   

    if(MA_OK == (rc = ma_json_array_create(&json_array))) {
        if(MA_OK == (rc = ma_json_create(&json))) {       
            /* TBD - Version and Computer names can be retrived and stored in log_file_handler facotry to use in multiple requestds */
            if(MA_OK == ma_ds_get_str(handler->connection->server->ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_VERSION_STR, &buffer) ) {
                const char *value = "N/A";
                size_t size = 0;
                ma_buffer_get_string(buffer, &value, &size);
                (void) ma_json_add_element(json, "Version", value ? value: "");
                (void) ma_buffer_release(buffer);
            }
            else
                (void) ma_json_add_element(json, "Version", "");

            if(MA_OK == ma_ds_get_str(handler->connection->server->ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_COMPUTER_NAME_STR, &buffer) ) {
                const char *value = "N/A";
                size_t size = 0;
                ma_buffer_get_string(buffer, &value, &size);
                (void) ma_json_add_element(json, "AgentHostName", value ? value: "");
                (void) ma_buffer_release(buffer);
            }
            else
                (void) ma_json_add_element(json, "AgentHostName", "N/A");

            (void) ma_json_array_add_object(json_array, json);
            (void) ma_json_release(json); json = NULL;
        }

        if(MA_OK == (rc = ma_logger_db_get_logs(handler->connection->server->db, &db_record))) {
            while((MA_OK == rc) && (MA_ERROR_NO_MORE_ITEMS != (rc = ma_db_recordset_next(db_record)))) {            
                const char *date_time = NULL, *severity = NULL, *facility = NULL, *message = NULL;
                (void) ma_db_recordset_get_string(db_record, 1, &date_time);
                (void) ma_db_recordset_get_string(db_record, 2, &severity);
                (void) ma_db_recordset_get_string(db_record, 3, &facility);
                (void) ma_db_recordset_get_string(db_record, 4, &message);

                if(date_time && severity && facility && message) {                
                    if(MA_OK == (rc = ma_json_create(&json))) {
                        (void) ma_json_add_element(json, "Time", date_time);
                        (void) ma_json_add_element(json, "Severity", severity);
                        (void) ma_json_add_element(json, "Facility", facility);
                        (void) ma_json_add_element(json, "Message", message);
                        (void) ma_json_array_add_object(json_array, json);
                        (void) ma_json_release(json); json = NULL;
                    }
                }
            }
        }
        /* TBD - Do we need to send the response back to client even zero records in db ?? */
        if(MA_ERROR_NO_MORE_ITEMS == rc) 
            rc = MA_OK;
        (void) ma_db_recordset_release(db_record);        
    }
    
    if(MA_OK == rc) {
		if(!ma_json_array_is_empty(json_array)) {
			if(MA_OK == (rc = ma_bytebuffer_create(MA_MAX_LEN, &handler->send_json_buffer))) {
				ma_json_array_get_data(json_array, handler->send_json_buffer);
				rc = send_json_data_response(handler);                       
			}
		}                  
    }

    if(json_array) ma_json_array_release(json_array);

    return rc;
}


static void foreach_product(char const *key, ma_variant_t *value, void *cb_args, ma_bool_t *stop_loop) {    
	ma_json_array_t *json_array = (ma_json_array_t *)cb_args;
	ma_json_t *json = NULL;
	if(MA_OK == ma_json_create(&json)) {
		ma_buffer_t *buffer = NULL;
		if(MA_OK == ma_variant_get_string_buffer(value, &buffer)) {
			const char *product_name = NULL;
			size_t size = 0;
			ma_buffer_get_string(buffer, &product_name, &size);
			(void) ma_json_add_element(json, MA_PRODUCT_NAME_STR, product_name ? product_name: "");
			(void) ma_json_add_element(json, MA_PRODUCT_ID_STR, key ? key: "");
			(void) ma_buffer_release(buffer);
		}
		(void) ma_json_array_add_object(json_array, json);
		(void) ma_json_release(json); json = NULL;
	}
}

ma_error_t send_products_info(ma_log_file_handler_t *handler, ma_message_t *response) {
	ma_error_t rc = MA_OK;
	ma_json_array_t *json_array = NULL;

	if(MA_OK == (rc = ma_json_array_create(&json_array))) {
		ma_json_t *json = NULL;	
		ma_variant_t *payload = NULL;

		if(MA_OK == (rc = ma_json_create(&json))) {	
			ma_buffer_t *buffer = NULL;   
		    if(MA_OK == ma_ds_get_str(handler->connection->server->ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_VERSION_STR, &buffer) ) {
                const char *value = "N/A";
                size_t size = 0;
                ma_buffer_get_string(buffer, &value, &size);
                (void) ma_json_add_element(json, "Version", value ? value: "");
                (void) ma_buffer_release(buffer), buffer = NULL;
            }
            else
                (void) ma_json_add_element(json, "Version", "");

			if(MA_OK == ma_ds_get_str(handler->connection->server->ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_COMPUTER_NAME_STR, &buffer) ) {
				const char *value = "N/A";
				size_t size = 0;
				ma_buffer_get_string(buffer, &value, &size);
				(void) ma_json_add_element(json, "AgentHostName", value ? value: "");
				(void) ma_buffer_release(buffer), buffer = NULL;
			}
			else
				(void) ma_json_add_element(json, "AgentHostName", "N/A");

			(void) ma_json_array_add_object(json_array, json);
			(void) ma_json_release(json); json = NULL;
		}
		
		if(MA_OK == (rc = ma_message_get_payload(response, &payload))) {
			ma_table_t *product_tb = NULL;
			if(MA_OK == (rc = ma_variant_get_table(payload, &product_tb))) {
				ma_table_foreach(product_tb, foreach_product, json_array);
				(void) ma_table_release(product_tb);
			}
			(void) ma_variant_release(payload);
		}
	}
    
	if(MA_OK == rc) {
		if(!ma_json_array_is_empty(json_array)) {
			if(MA_OK == (rc = ma_bytebuffer_create(MA_MAX_LEN, &handler->send_json_buffer))) {
				ma_json_array_get_data(json_array, handler->send_json_buffer);
				rc = send_json_data_response(handler);                       
			}
		}                  
	}

	if(json_array) ma_json_array_release(json_array);

	return rc;
}

ma_error_t send_product_log_files_info(ma_log_file_handler_t *handler, ma_message_t *response) {
	ma_error_t rc = MA_OK;
	ma_json_array_t *json_array = NULL;	
	const char *product_id = NULL;
	
	(void) ma_message_get_property(response, MA_PRODUCT_ID_STR, &product_id);			
	if(MA_OK == (rc = ma_json_array_create(&json_array))) {
		ma_variant_t *payload = NULL;
		if(MA_OK == (rc = ma_message_get_payload(response, &payload))) {
			ma_array_t *log_file_array = NULL;
			if(MA_OK == (rc = ma_variant_get_array(payload, &log_file_array))) {
				size_t count = 0, i = 0;
				(void)ma_array_size(log_file_array, &count);
				for(i=0; i<count; i++) {
					ma_variant_t *value = NULL;
					if(MA_OK == (rc = ma_array_get_element_at(log_file_array, i+1, &value))) {					
						ma_buffer_t *buffer = NULL;
						if(MA_OK == (rc = ma_variant_get_string_buffer(value, &buffer))) {
							const char *file_name = NULL;
							size_t size = 0;
							ma_buffer_get_string(buffer, &file_name, &size);
							if(file_name) {
								ma_json_t *json = NULL;
								if(MA_OK == ma_json_create(&json)) {
									(void) ma_json_add_element(json, MA_PRODUCT_ID_STR, product_id);
									(void) ma_json_add_element(json, MA_LOG_FILE_NAME_STR, file_name);						
									(void) ma_json_array_add_object(json_array, json);
									(void) ma_json_release(json); json = NULL;
								}
							}
							ma_buffer_release(buffer), buffer = NULL;
						}
						ma_variant_release(value), value = NULL;
					}
				}
				(void) ma_array_release(log_file_array), log_file_array = NULL;
			}
			(void) ma_variant_release(payload);
		}
	}
    
	if(MA_OK == rc) {
		if(!ma_json_array_is_empty(json_array)) {
			if(MA_OK == (rc = ma_bytebuffer_create(MA_MAX_LEN, &handler->send_json_buffer))) {
				ma_json_array_get_data(json_array, handler->send_json_buffer);
				rc = send_json_data_response(handler);                       
			}
		}                  
	}

	if(json_array) ma_json_array_release(json_array);

	return rc;
}

static ma_error_t product_log_info_async_cb(ma_error_t status, ma_message_t *response, void *cb_data, ma_msgbus_request_t *request) {
	ma_log_file_handler_t *handler = (ma_log_file_handler_t *)cb_data;
	ma_error_t rc =  status;

	if(MA_OK == status && response) {		
		const char *response_type = NULL;
		(void) ma_message_get_property(response, MA_PROP_KEY_LOGGER_RESPONSE_TYPE_STR, &response_type);

		if(!strcmp(response_type, MA_PROP_VALUE_GET_PRODUCTS_STR)) {
			rc = send_products_info(handler, response);
		}
		else if(!strcmp(response_type, MA_PROP_VALUE_GET_PRODUCT_LOG_FILES_STR)) {
			rc = send_product_log_files_info(handler, response);
		}
		else if(!strcmp(response_type, MA_PROP_VALUE_GET_PRODUCT_LOG_PATH_STR)) {
			const char *log_full_path = NULL;
			(void) ma_message_get_property(response, MA_LOG_FILE_FULL_PATH_STR, &log_full_path);
			rc = read_file_from_disk(handler, log_full_path);
		}
		(void) ma_message_release(response);
	}
	
	if(request) {
		ma_msgbus_endpoint_t * ep = NULL;
		(void) ma_msgbus_request_get_endpoint(request, &ep);		
		(void) ma_msgbus_request_release(request);
        if(ep) (void) ma_msgbus_endpoint_release(ep);
	}

    if(MA_OK != rc) ma_http_connection_send_stock_response(handler->connection, MA_HTTP_RESPONSE_403_FORBIDDEN);

    return rc;
}

static ma_error_t get_products_info(ma_log_file_handler_t *handler) {
	ma_log_file_handler_factory_t *factory = (ma_log_file_handler_factory_t *)handler->base.data ;
    ma_http_server_t *server = (ma_http_server_t *)factory->base.data;
	ma_msgbus_endpoint_t *ep = NULL;
	ma_error_t rc = MA_OK;
	if(MA_OK == (rc = ma_msgbus_endpoint_create(server->msgbus, MA_LOGGER_SERVICE_NAME_STR, MA_LOGGER_SERVICE_HOSTNAME_STR, &ep))) {
		ma_message_t *msg = NULL;
		(void)ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);			
		(void)ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);						
		if(MA_OK == (rc = ma_message_create(&msg))) {
			ma_msgbus_request_t *request = NULL;
			(void) ma_message_set_property(msg, MA_PROP_KEY_LOGGER_REQUEST_TYPE_STR, MA_PROP_VALUE_GET_PRODUCTS_STR);
			if(MA_OK != (rc = ma_msgbus_async_send(ep, msg, product_log_info_async_cb, handler, &request)))
				(void) ma_msgbus_endpoint_release(ep);
			ma_message_release(msg); msg = NULL;
		}
		else
			(void) ma_msgbus_endpoint_release(ep);
	}
	return rc;
}

static ma_error_t get_product_log_files_info(ma_log_file_handler_t *handler, const char *product_id) {
	ma_log_file_handler_factory_t *factory = (ma_log_file_handler_factory_t *)handler->base.data ;
        ma_http_server_t *server = (ma_http_server_t *)factory->base.data;
	ma_msgbus_endpoint_t *ep = NULL;
	ma_error_t rc = MA_OK;
	if(MA_OK == (rc = ma_msgbus_endpoint_create(server->msgbus, MA_LOGGER_SERVICE_NAME_STR, MA_LOGGER_SERVICE_HOSTNAME_STR, &ep))) {
		ma_message_t *msg = NULL;
		(void)ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);			
		(void)ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);						
		if(MA_OK == (rc = ma_message_create(&msg))) {
			ma_msgbus_request_t *request = NULL;
			(void) ma_message_set_property(msg, MA_PROP_KEY_LOGGER_REQUEST_TYPE_STR, MA_PROP_VALUE_GET_PRODUCT_LOG_FILES_STR);
			(void) ma_message_set_property(msg, MA_PRODUCT_ID_STR, product_id);
			if(MA_OK != (rc = ma_msgbus_async_send(ep, msg, product_log_info_async_cb, handler, &request)))
				(void) ma_msgbus_endpoint_release(ep);
			ma_message_release(msg); msg = NULL;
		}
		else
			(void) ma_msgbus_endpoint_release(ep);
	}
	return rc;
}

static ma_error_t get_log_file_full_path(ma_log_file_handler_t *handler, const char *product_id, const char *log_file_name) {
	ma_log_file_handler_factory_t *factory = (ma_log_file_handler_factory_t *)handler->base.data ;
    ma_http_server_t *server = (ma_http_server_t *)factory->base.data;
	ma_msgbus_endpoint_t *ep = NULL;
	ma_error_t rc = MA_OK;
	if(MA_OK == (rc = ma_msgbus_endpoint_create(server->msgbus, MA_LOGGER_SERVICE_NAME_STR, MA_LOGGER_SERVICE_HOSTNAME_STR, &ep))) {
		ma_message_t *msg = NULL;
		(void)ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);			
		(void)ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);						
		if(MA_OK == (rc = ma_message_create(&msg))) {
			ma_msgbus_request_t *request = NULL;
			(void) ma_message_set_property(msg, MA_PROP_KEY_LOGGER_REQUEST_TYPE_STR, MA_PROP_VALUE_GET_PRODUCT_LOG_PATH_STR);
			(void) ma_message_set_property(msg, MA_PRODUCT_ID_STR, product_id);
			(void) ma_message_set_property(msg, MA_LOG_FILE_NAME_STR, log_file_name);
			if(MA_OK != (rc = ma_msgbus_async_send(ep, msg, product_log_info_async_cb, handler, &request)))
				(void) ma_msgbus_endpoint_release(ep);
			ma_message_release(msg); msg = NULL;
		}
		else
			(void) ma_msgbus_endpoint_release(ep);
	}
	return rc;
}

static ma_error_t process_request(ma_log_file_handler_t *handler, const char *file_name) {
    ma_error_t rc = MA_ERROR_HTTP_SERVER_INTERNAL;
    char local_file_path[MA_HTTP_MAX_URL_PATH_LEN] = {0};

    MA_MSC_SELECT(_snprintf, snprintf)(local_file_path, MA_HTTP_MAX_URL_PATH_LEN,"%s%c%s", handler->root_dir, MA_PATH_SEPARATOR, file_name); 
   
    if(!MA_WIN_SELECT(stricmp,strcasecmp)(file_name, MA_HTTP_AGENT_LOG_JSON_FILE_STR)) {
        MA_MSC_SELECT(_snprintf, snprintf)(handler->mime_type, MA_MAX_LEN,"%s", "application/json"); 
        rc = read_agent_logs_from_db(handler);
    }
	else if(!MA_WIN_SELECT(stricmp,strcasecmp)(file_name, MA_HTTP_LOAD_PRODUCT_NAMES_STR)) {
        MA_MSC_SELECT(_snprintf, snprintf)(handler->mime_type, MA_MAX_LEN,"%s", "application/json"); 
        rc = get_products_info(handler);
    }
	else if(!MA_WIN_SELECT(stricmp,strcasecmp)(file_name, MA_HTTP_LOAD_LOG_FILE_NAMES_STR)) {
		char product_id[MA_MAX_LEN] = {0};		
		if(get_query_value_from_url(&handler->connection->request.url, MA_PRODUCT_ID_STR, product_id, MA_MAX_LEN, 0)) {
            MA_MSC_SELECT(_snprintf, snprintf)(handler->mime_type, MA_MAX_LEN,"%s", "application/json"); 
			rc = get_product_log_files_info(handler, product_id);
		}		
    }
	else if(!MA_WIN_SELECT(stricmp,strcasecmp)(file_name, MA_HTTP_DISPLAY_LOG_FILE_STR) || !MA_WIN_SELECT(stricmp,strcasecmp)(file_name, MA_HTTP_DOWNLOAD_LOG_FILE_STR)) {
		char product_id[MA_MAX_LEN] = {0}, log_file_name[MA_MAX_LEN] = {0};		
		if(get_query_value_from_url(&handler->connection->request.url, MA_PRODUCT_ID_STR, product_id, MA_MAX_LEN, 0) &&
		   get_query_value_from_url(&handler->connection->request.url, MA_LOG_FILE_NAME_STR, log_file_name, MA_MAX_LEN, 0)) {
            MA_MSC_SELECT(_snprintf, snprintf)(handler->mime_type, MA_MAX_LEN,"%s", "application/octet-stream"); 
            MA_MSC_SELECT(_snprintf, snprintf)(handler->content_disposition, MA_MAX_LEN,"attachment; filename=%s", log_file_name); 
			rc = get_log_file_full_path(handler, product_id, log_file_name);
		}
    }
    else {
		const char *mime_type = get_mime_type((char *)file_name);
        MA_MSC_SELECT(_snprintf, snprintf)(handler->mime_type, MA_MAX_LEN,"%s", mime_type); 
        rc = read_file_from_disk(handler, local_file_path);
    }

    if(MA_OK != rc) ma_http_connection_send_stock_response(handler->connection, MA_HTTP_RESPONSE_403_FORBIDDEN);

    return rc;
}