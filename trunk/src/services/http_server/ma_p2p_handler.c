#include "ma/ma_common.h"

#include "ma_http_connection.h"
#include "ma/internal/services/http_server/ma_p2p_handler.h"
#include "ma/internal/services/p2p/ma_p2p_db_manager.h" 
#include "ma/internal/services/stats/ma_stats_db_manager.h"

#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal/services/http_server/ma_http_server_utils.h"
#include "ma/internal/utils/context/ma_context.h"
#include "ma/internal/utils/datastructures/ma_stream.h"

#include "ma/internal/ma_strdef.h"
#include "ma/internal/ma_macros.h"

#include "ma/internal/services/ma_policy_settings_bag.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/utils/time/ma_time.h"
#include "ma_http_server_internal.h"

#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <math.h>
#include <stdio.h>

#define MA_P2P_URL_TYPE_STR		"/p2p/"
#define CHUNK_SIZE				1024


typedef struct ma_p2p_handler_factory_s {
	ma_http_request_handler_factory_t	base ;

	ma_context_t						*context ;

	ma_uint32_t							active_connections ;
} ma_p2p_handler_factory_t ;


typedef struct ma_p2p_handler_s {
    ma_http_request_handler_t			base ;

	ma_http_connection_t				*connection ;

    uv_fs_t                             fs_open_req ;

    uv_fs_t                             fs_read_req ;

    uv_file                             file ;

	size_t								cur_position;

	size_t								content_size ;

	unsigned char						buffer[CHUNK_SIZE] ;

	char                                mime_type[MA_MAX_LEN] ;

	char								*hash ;
}ma_p2p_handler_t ;


/* p2p handler */
static ma_error_t p2p_handler_start(ma_http_request_handler_t *handler, ma_http_connection_t *connection) ;
static ma_error_t p2p_handler_stop(ma_http_request_handler_t *handler) ;
static void p2p_handler_destroy(ma_http_request_handler_t *handler) ;

const static ma_http_request_handler_methods_t spipe_handler_methods = {
    &p2p_handler_start,
    &p2p_handler_stop,
    &p2p_handler_destroy
} ;

/* p2p handler Factory */
static ma_bool_t p2p_handler_factory_matches(ma_http_request_handler_factory_t *factory, ma_http_request_t const *request) ;
static ma_bool_t p2p_handler_factory_authorize(ma_http_request_handler_factory_t *factory, ma_http_request_t const *request) ;
static ma_error_t p2p_handler_factory_configure(ma_http_request_handler_factory_t *factory);
static ma_error_t p2p_handler_factory_start(ma_http_request_handler_factory_t *factory) ;
static ma_error_t p2p_handler_factory_stop(ma_http_request_handler_factory_t *factory) ;
static ma_error_t p2p_handler_factory_create_instance(ma_http_request_handler_factory_t *factory, ma_http_request_handler_t **handler) ;
static void p2p_handler_factory_destroy(ma_http_request_handler_factory_t *factory) ;

const static ma_http_request_handler_factory_methods_t p2p_handler_factory_methods = {
    &p2p_handler_factory_matches,
	&p2p_handler_factory_authorize,
    &p2p_handler_factory_configure,
    &p2p_handler_factory_create_instance,
	&p2p_handler_factory_start,
	&p2p_handler_factory_stop,
    &p2p_handler_factory_destroy
} ;

ma_error_t ma_p2p_handler_factory_create(ma_http_server_t *server, ma_http_request_handler_factory_t **p2p_handler_factory) {
	 ma_p2p_handler_factory_t *self = (ma_p2p_handler_factory_t *)calloc(1, sizeof(ma_p2p_handler_factory_t)) ;
    if (self) {
		ma_http_request_handler_factory_init(&self->base, &p2p_handler_factory_methods, server) ;

		*p2p_handler_factory = &self->base ;
		return MA_OK ;
    }
    return MA_ERROR_OUTOFMEMORY ;
}

ma_error_t ma_p2p_handler_factory_set_context(ma_http_request_handler_factory_t *p2p_handler_factory, ma_context_t *context) {
	ma_p2p_handler_factory_t *self = (ma_p2p_handler_factory_t *)p2p_handler_factory ;
	self->context = context ;
	return MA_OK ;
}

static int add_installer_files_to_p2p_table(ma_p2p_handler_t *handler,ma_http_connection_t *connection);
static int on_p2p_message_body(http_parser *parser, const char *data, size_t length) ;
static int on_p2p_message_complete(http_parser *parser) ;
static void file_close(ma_p2p_handler_t *handler);

ma_error_t p2p_handler_start(ma_http_request_handler_t *handler, ma_http_connection_t *connection) {
	ma_p2p_handler_t *p2p_handler = (ma_p2p_handler_t *) handler ;
	ma_p2p_handler_factory_t *factory = (ma_p2p_handler_factory_t *)p2p_handler->base.data ;
	
	p2p_handler->connection = connection ;
	p2p_handler->connection->connection_type = MA_HTTP_CONNECTION_TYPE_P2P ;

	connection->parser_callbacks.on_body = &on_p2p_message_body ;	
	connection->parser_callbacks.on_message_complete = &on_p2p_message_complete ;	
	return MA_OK;
}

ma_error_t p2p_handler_stop(ma_http_request_handler_t *handler) {
	ma_p2p_handler_t *self = (ma_p2p_handler_t *) handler ;
	ma_p2p_handler_factory_t *factory = (ma_p2p_handler_factory_t *)self->base.data ;

	

	if(self->file)	file_close(self) ;

	return MA_OK ;
}

void p2p_handler_destroy(ma_http_request_handler_t *handler) {
	ma_p2p_handler_t *p2p_handler = (ma_p2p_handler_t *) handler ;
    ma_p2p_handler_factory_t *factory = (ma_p2p_handler_factory_t *)p2p_handler->base.data ;

	--factory->active_connections ;

	if(p2p_handler->hash)	free(p2p_handler->hash) ;
	
	p2p_handler->hash = NULL ;


	free(p2p_handler ) ;

	return ;
}

ma_bool_t p2p_handler_factory_matches(ma_http_request_handler_factory_t *factory, ma_http_request_t const *request) {
	char url_path[MA_HTTP_MAX_URL_PATH_LEN] = {0} ;

    ma_bool_t is_p2p_content_request = get_url_path_from_url(&request->url, url_path, MA_HTTP_MAX_URL_PATH_LEN, 0) && strstr(url_path, MA_P2P_URL_TYPE_STR) ;
    
    return is_p2p_content_request && (HTTP_GET == request->parser.method) ;
}

ma_bool_t p2p_handler_factory_authorize(ma_http_request_handler_factory_t *factory, ma_http_request_t const *request) {
	ma_p2p_handler_factory_t *self = (ma_p2p_handler_factory_t *)factory ;
    ma_http_server_t *server = (ma_http_server_t *) self->base.data;

	if(self->active_connections <= server->policies.p2p_concurrent_connections_limit) {
		if(server->policies.is_content_serving_enabled && !server->policies.is_p2p_serving_suspended) {
			uv_buf_t user_agent_buf = uv_buf_init(0,0) ;
			if(ma_http_request_search_header((ma_http_request_t *)request, MA_HTTP_SERVER_USER_AGENT_HEADER_NAME_STR, &user_agent_buf)) {
				if(0 == strcmp(user_agent_buf.base, MA_HTTP_SERVER_USER_AGENT_HEADER_VALUE_STR)) {
					return MA_TRUE ;
				}
			}
		}
		else
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_WARNING, "http p2p serving is disabled or suspended.") ;
	}
	else
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_WARNING, "http p2p handler has reached concurrent connections limit") ;

	return MA_FALSE ;
}

ma_error_t p2p_handler_factory_configure(ma_http_request_handler_factory_t *factory) {
	ma_p2p_handler_factory_t *self = (ma_p2p_handler_factory_t *)factory ;
	
	return MA_OK ;
}

ma_error_t p2p_handler_factory_start(ma_http_request_handler_factory_t *factory) {
	ma_p2p_handler_factory_t *self = (ma_p2p_handler_factory_t *)factory ;

	if(self)
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "http p2p handler started.") ;

	return MA_OK ;
}

ma_error_t p2p_handler_factory_stop(ma_http_request_handler_factory_t *factory) {
	ma_p2p_handler_factory_t *self = (ma_p2p_handler_factory_t *)factory ;

	if(self)
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "http p2p handler stoped.") ;

	return MA_OK ;
}

ma_error_t p2p_handler_factory_create_instance(ma_http_request_handler_factory_t *factory, ma_http_request_handler_t **handler) {
	ma_p2p_handler_t *self = (ma_p2p_handler_t *)calloc(1, sizeof(ma_p2p_handler_t)) ;
	if (self) {
		ma_p2p_handler_factory_t *p2p_factory = (ma_p2p_handler_factory_t *)factory ;

        ma_http_request_handler_init(&self->base, &spipe_handler_methods, factory) ;
        *handler = &self->base ;
		++p2p_factory->active_connections ;

        return MA_OK ;
    }
    return MA_ERROR_OUTOFMEMORY ;
}

void p2p_handler_factory_destroy(ma_http_request_handler_factory_t *factory) {
	ma_p2p_handler_factory_t *self = (ma_p2p_handler_factory_t *)factory ;
    free(self) ;
	return ;
}

int on_p2p_message_body(http_parser *parser, const char *data, size_t length) {
	return 0 ;
}

static ma_error_t serve_p2p_content(ma_p2p_handler_t *handler, const char *hash) ;

int on_p2p_message_complete(http_parser *parser) {
	ma_http_connection_t *connection = (ma_http_connection_t *)parser->data ;
	ma_p2p_handler_t *handler = (ma_p2p_handler_t *) connection->cur_req_handler ;
	ma_p2p_handler_factory_t *factory = (ma_p2p_handler_factory_t *)handler->base.data ;
    
    char url_path[MA_HTTP_MAX_URL_PATH_LEN] = {0} ;
	ma_db_t *db_handle = ma_configurator_get_msgbus_database(MA_CONTEXT_GET_CONFIGURATOR(factory->context)) ;

	if(get_url_path_from_url(&connection->request.url, url_path, MA_HTTP_MAX_URL_PATH_LEN, 0)) {
		ma_int32_t file_state = -1 ;
		char *content_file = NULL ;
		ma_error_t rc =  MA_ERROR_HTTP_SERVER_INTERNAL ;

        MA_LOG(connection->server->logger, MA_LOG_SEV_DEBUG, "p2p handler processing <%s> request", url_path) ;

		handler->hash = strdup(strrchr(url_path, '/')+1) ;

		if(MA_OK == (rc = ma_p2p_db_get_filepath_for_hash(db_handle, handler->hash, &file_state, &content_file))
			&&	file_state >= 0 && content_file) 
		{
			if(MA_OK == (rc = serve_p2p_content(handler, content_file)))
				(void)ma_p2p_db_set_content_state_serving(db_handle, handler->hash) ;
		}
		else {
            handler->connection->close_after_write = 1;
			ma_http_connection_send_stock_response(handler->connection, MA_HTTP_RESPONSE_404_NOT_FOUND) ;
		}
	}
	else
		(void) ma_http_connection_send_stock_response(connection, MA_HTTP_RESPONSE_503_SERVICE_UNAVAILABLE) ;

	return 0 ;
}

static void content_file_open_cb(uv_fs_t* req) ;
static void content_file_read_cb(uv_fs_t* req) ;
static ma_error_t read_data(ma_p2p_handler_t *self) ;
static void content_file_write_cb(uv_write_t* req, int status) ;
static ma_error_t send_response(ma_p2p_handler_t *self) ;

ma_error_t serve_p2p_content(ma_p2p_handler_t *handler, const char *content_file) {
	ma_error_t rc = MA_OK ;
	const char *mime_type = get_mime_type((char *)content_file);

    MA_MSC_SELECT(_snprintf, snprintf)(handler->mime_type, MA_MAX_LEN, "%s", mime_type) ; 
	handler->fs_open_req.data = handler ;

	if(-1 == uv_fs_open(handler->connection->server->uv_loop, &handler->fs_open_req, content_file, O_RDONLY, 0, content_file_open_cb))
        rc = MA_ERROR_HTTP_SERVER_INTERNAL ;

	return rc ;
}

void content_file_open_cb(uv_fs_t* req) {
	ma_error_t rc = MA_ERROR_HTTP_SERVER_INTERNAL ;
    ma_p2p_handler_t *handler = (ma_p2p_handler_t *)req->data ;
	ma_p2p_handler_factory_t *factory = (ma_p2p_handler_factory_t *)handler->base.data ;
	ma_int32_t content_state = -1 ;

	if(req->result >= 0) {
		uv_fs_t stat_req ;
		handler->file = req->result ;        
		if(!uv_fs_fstat(handler->connection->server->uv_loop, &stat_req, handler->file, NULL)) {
			/* TBD - length field in uv_fs_t structure is not providing the size of file, so using internal structure data for file size */
			/* These structures have been changed in UV newer versions, so this code might be changed according to UV changes */
#ifdef MA_WINDOWS
			struct _stat64 s = stat_req.statbuf ;
#else
			struct stat s = stat_req.statbuf;
#endif
			handler->content_size = s.st_size ;
			
			if(handler->content_size > 0) {
				rc = read_data(handler) ;
			}
            
			uv_fs_req_cleanup(&stat_req) ;
		}
	}   

    uv_fs_req_cleanup(req) ;

    if(MA_OK != rc) {
        file_close(handler);    
        handler->connection->close_after_write = 1;
        ma_http_connection_send_stock_response(handler->connection, MA_HTTP_RESPONSE_500_INTERNAL_SERVICE) ;
    }
}

static ma_error_t read_data(ma_p2p_handler_t *handler) {
	ma_error_t rc = MA_ERROR_HTTP_SERVER_INTERNAL ;
	handler->fs_read_req.data = handler ;

	if(-1 != uv_fs_read(handler->connection->server->uv_loop, &handler->fs_read_req, handler->file, handler->buffer, CHUNK_SIZE, handler->cur_position, content_file_read_cb)) {
		rc = MA_OK ;
	}
       
	return rc ;
}

void content_file_read_cb(uv_fs_t* req) {
	ma_error_t rc = MA_ERROR_HTTP_SERVER_INTERNAL ;
    ma_p2p_handler_t *handler = (ma_p2p_handler_t *)req->data ;

	if(req->result != -1) {
        rc = send_response(handler);                
    }
    else {        
       file_close(handler);
	}

    uv_fs_req_cleanup(req) ;

    if(MA_OK != rc) {
        handler->connection->close_after_write = 1;
        ma_http_connection_send_stock_response(handler->connection, MA_HTTP_RESPONSE_500_INTERNAL_SERVICE) ;
    }
}

static ma_error_t send_response(ma_p2p_handler_t *self) { 
    ma_error_t rc = MA_OK ;    

	if(!self->cur_position) {
		const size_t bytes_avail = ma_http_buffer_bytes_available(&self->connection->send_data) ;
		int n = MA_MSC_SELECT(_snprintf,snprintf)((char *)ma_http_buffer_peek(&self->connection->send_data), bytes_avail, "%u", self->content_size) ;        
        char *p = (0 < n && (size_t)n < bytes_avail) ? ma_http_buffer_advance(&self->connection->send_data, n) : 0 ;
		uv_buf_t content_data_with_header[] = {
                MA_UV_BUF_TEXT(MA_HTTP_RESPONSE_HTTP1_1),
				MA_UV_BUF(MA_HTTP_RESPONSE_200_OK, strlen(MA_HTTP_RESPONSE_200_OK)),
				MA_UV_BUF_TEXT(MA_HTTP_CRLF),
                MA_UV_BUF_TEXT(MA_HTTP_RESPONSE_HEADER_FIELD_CONTENT_TYPE),
				MA_UV_BUF(self->mime_type, strlen(self->mime_type)),
				MA_UV_BUF_TEXT(MA_HTTP_CRLF),
                MA_UV_BUF_TEXT(MA_HTTP_RESPONSE_HEADER_FIELD_CONTENT_LENGTH),
				MA_UV_BUF(p, n),
				MA_UV_BUF_TEXT(MA_HTTP_CRLF),
                MA_UV_BUF_TEXT(MA_HTTP_CRLF),
				MA_UV_BUF(self->buffer, self->fs_read_req.result),
            } ;
		
		self->cur_position += self->fs_read_req.result;
		if(UV_OK != uv_write(&self->connection->c_write_req, (uv_stream_t *)&self->connection->c_socket, content_data_with_header, MA_COUNTOF(content_data_with_header), &content_file_write_cb)) {
            uv_err_t uv_err = uv_last_error(self->connection->server->uv_loop) ;

			file_close(self);
            ma_http_server_log_uv_err(self->connection->server, "UV Write failed while serving data to clients", MA_LOG_SEV_ERROR, uv_err) ;
            ma_http_connection_stop(self->connection) ;
            rc = MA_ERROR_HTTP_SERVER_INTERNAL ;
		}
	}
	else {
		uv_buf_t content_data[] = {
				MA_UV_BUF(self->buffer, self->fs_read_req.result),
			} ;

		self->cur_position += self->fs_read_req.result;
		if (UV_OK != uv_write(&self->connection->c_write_req, (uv_stream_t *) &self->connection->c_socket, content_data, MA_COUNTOF(content_data), &content_file_write_cb)) {
            uv_err_t uv_err = uv_last_error(self->connection->server->uv_loop) ;

			file_close(self);
            ma_http_server_log_uv_err(self->connection->server, "UV Write failed while serving data to clients", MA_LOG_SEV_ERROR, uv_err) ;  
            ma_http_connection_stop(self->connection) ;
            rc = MA_ERROR_HTTP_SERVER_INTERNAL ;
        }
	}

	/*if(MA_OK == rc) {
		MA_LOG(self->connection->server->logger, MA_LOG_SEV_DEBUG, "Serving file data size(%d), current position (%d)", self->fs_read_req.result, self->cur_position) ;
	}*/

    return rc ;
}

void content_file_write_cb(uv_write_t* req, int status) {
	ma_http_connection_t *conn = ma_queue_data(req, ma_http_connection_t, c_write_req) ;
    ma_p2p_handler_t *handler = (ma_p2p_handler_t *)conn->cur_req_handler ;
	ma_p2p_handler_factory_t *factory = (ma_p2p_handler_factory_t *)handler->base.data ;

	if(UV_OK == status) {
		if(handler->cur_position == handler->content_size) {	/* content serve complete */
			ma_db_t *db_handle = ma_configurator_get_msgbus_database(MA_CONTEXT_GET_CONFIGURATOR(factory->context)) ;

			/*update hit count*/
			
			MA_LOG(conn->server->logger, MA_LOG_SEV_DEBUG, "P2P Content serving complete...") ;
			
			(void)ma_p2p_db_update_hit_count_and_access_time(db_handle, handler->hash) ;
			(void)ma_stats_db_update_current(db_handle, MA_STAT_P2P, MA_STAT_ATTRIBUTE_BANDWITH_SAVED, (ma_double_t)((ma_double_t)(handler->content_size)/1024)/1024 ) ;
			(void)ma_stats_db_update_current(db_handle, MA_STAT_P2P, MA_STAT_ATTRIBUTE_FILES_SERVED, (ma_double_t)1) ;

			MA_LOG(conn->server->logger, MA_LOG_SEV_DEBUG, "Updated hit count and access time") ;
			

            file_close(handler);
			if (!conn->close_after_write) {                
                ma_http_connection_notify_request_complete(conn) ;
				MA_LOG(conn->server->logger, MA_LOG_SEV_DEBUG, "Notified request completion") ;
                return; /* keep connection open  */
            }           
            
            ma_http_connection_stop(conn);			
		}
		else {
			/* MA_LOG(handler->connection->server->logger, MA_LOG_SEV_DEBUG, "Served data (%d)", handler->cur_position) ; */
			/* Restart the timer as connection is till active to serve the files to caller */
			(void)ma_http_connection_idletimer_restart(conn) ;

			if(MA_OK != read_data(handler)) {	
                file_close(handler);   
                handler->connection->close_after_write = 1;
				ma_http_connection_send_stock_response(handler->connection, MA_HTTP_RESPONSE_500_INTERNAL_SERVICE) ;
			}
		}
	}
	else {
		uv_err_t uv_err = uv_last_error(handler->connection->server->uv_loop) ;
        file_close(handler);       
        ma_http_server_log_uv_err(handler->connection->server, "UV Write failed while serving data to clients in write callback", MA_LOG_SEV_ERROR, uv_err) ;
        ma_http_connection_stop(conn) ;
	}
	return ;
}

static void file_close(ma_p2p_handler_t *handler) {
	ma_p2p_handler_factory_t *factory = (ma_p2p_handler_factory_t *)handler->base.data ;
	ma_db_t *db_handle = ma_configurator_get_msgbus_database(MA_CONTEXT_GET_CONFIGURATOR(factory->context)) ;

	uv_fs_t fs_close_req;
    
	uv_fs_close(handler->connection->server->uv_loop, &fs_close_req, handler->file, NULL) ;
	uv_fs_req_cleanup(&fs_close_req);

	(void)ma_p2p_db_unset_content_state_serving(db_handle, handler->hash) ;
}
