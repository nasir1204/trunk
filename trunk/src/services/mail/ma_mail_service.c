#include "ma/internal/services/mail/ma_mail_service.h"
#include "ma/internal/utils/context/ma_context.h"
#include "ma/internal/defs/ma_mail_defs.h"
#include "ma/internal/services/ma_service.h"
#include "ma/ma_log.h"
#include "ma/ma_message.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/ma_variant.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/sensor/ma_sensor_msg.h"
#include "ma/sensor/ma_sensor_utils.h"
#include "ma/internal/services/mail/ma_mail_utils.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "mail service"

ma_logger_t *mail_logger;

MA_CPP(extern "C" {)
MA_CPP(})

struct ma_mail_service_s {
    ma_service_t            service_base;
    ma_context_t            *context;
    ma_msgbus_server_t      *server;
    ma_logger_t             *logger;
    ma_msgbus_t             *msgbus;
    ma_msgbus_subscriber_t  *subscriber;
};


static ma_error_t mail_subscriber_cb(const char *topic, ma_message_t *message, void *cb_data);
static ma_error_t ma_mail_service_cb(ma_msgbus_server_t *server, ma_message_t *msg, void *cb_data, ma_msgbus_client_request_t *c_request);

static ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint);
static ma_error_t service_start(ma_service_t *service);
static ma_error_t service_stop(ma_service_t *service);
static void service_release(ma_service_t *service);

static const ma_service_methods_t service_methods = {
    &service_configure,
    &service_start,
    &service_stop,
    &service_release
};

ma_error_t ma_mail_service_create(const char *service_name, ma_service_t **service) {
    if(service_name && service) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;

        if((*service = (ma_service_t*)calloc(1, sizeof(ma_mail_service_t)))) {
            ma_service_init(*service, &service_methods, service_name, service);
            return MA_OK;
        }
        return err ;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_mail_service_release(ma_mail_service_t *self) {
    if(self) {
        ma_error_t err = MA_OK;
        if(self->server && (MA_OK != (err = ma_msgbus_server_release(self->server)))) {
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "msgbus server release failed");
        }
        (void)ma_msgbus_subscriber_release(self->subscriber);
        free(self) ;
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_bool_t check_context_validity(ma_context_t *context){	
    /*Will see if we need this*/
    return MA_TRUE;
}

ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint) {
    if(service && context) {
        ma_error_t rc = MA_OK;
        if(MA_SERVICE_CONFIG_NEW == hint && check_context_validity(context)) {
            ma_configurator_t *configurator = MA_CONTEXT_GET_CONFIGURATOR(context);
            ma_mail_service_t *self = (ma_mail_service_t *)service;
            ma_msgbus_t *msgbus = MA_CONTEXT_GET_MSGBUS(context);

            self->context = context;
            mail_logger = self->logger = MA_CONTEXT_GET_LOGGER(context);
            self->msgbus = msgbus;

            if(MA_OK == (rc = ma_msgbus_subscriber_create(MA_CONTEXT_GET_MSGBUS(context), &self->subscriber))) {
                (void)ma_msgbus_subscriber_setopt(self->subscriber, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                (void)ma_msgbus_subscriber_setopt(self->subscriber, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
                if(MA_OK == (rc = ma_msgbus_server_create(msgbus, MA_MAIL_SERVICE_NAME_STR, &self->server))) {                        
                    (void)ma_msgbus_server_setopt(self->server, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
                    if(MA_OK == (rc = ma_msgbus_server_setopt(self->server, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC))) {
                        /*TODO - No need to add the mail object in the context if we can use the mail client API's */
                        MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_DEBUG, "mail service configuration successful");
                        return MA_OK;
                    }
                    ma_msgbus_server_release(self->server);
                }
                ma_msgbus_subscriber_release(self->subscriber);
            }
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t service_start(ma_service_t *service) {
    if(service) {
        ma_error_t err = MA_OK;
        ma_mail_service_t *self = (ma_mail_service_t *)service ;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);
        
        if(MA_OK == (err = ma_msgbus_server_start(((ma_mail_service_t*)service)->server, ma_mail_service_cb, service))) {
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "mail service started successfully");
            if(MA_OK == (err = ma_msgbus_subscriber_register(((ma_mail_service_t*)service)->subscriber,  "ma.sensor.*", mail_subscriber_cb, service))) {
                MA_LOG(((ma_mail_service_t*)service)->logger, MA_LOG_SEV_DEBUG, "mail subscriber registered successfully");
                return MA_OK ;
            } 
            else 
                MA_LOG(((ma_mail_service_t*)service)->logger, MA_LOG_SEV_ERROR, "mail service failed to subscribe topic, errocode(%d)", err);
            ma_msgbus_server_stop(((ma_mail_service_t*)service)->server);
        }
        else
            MA_LOG(logger, MA_LOG_SEV_CRITICAL, "mail service start failed, last error(%d)", err);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_mail_service_get_version(ma_uint32_t *version) {
    if(version) {
        *version = 0x05020100; //TODO fetch version 
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_mail_service_start(ma_mail_service_t *service) {
    return service_start((ma_service_t*)service);
}

ma_error_t ma_mail_service_stop(ma_mail_service_t *service) {
    return service_stop((ma_service_t*)service);
}

ma_error_t service_stop(ma_service_t *service) {
    if(service) {
        ma_error_t err = MA_OK;
        ma_mail_service_t *self = (ma_mail_service_t *)service;

        if(MA_OK != (err = ma_msgbus_subscriber_unregister(((ma_mail_service_t*)service)->subscriber))) {
            MA_LOG(((ma_mail_service_t*)service)->logger, MA_LOG_SEV_ERROR, "mail service unregistering subscriber failed, last error(%d)", err);
        }

        if(MA_OK == (err = ma_msgbus_server_stop(self->server))) {
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO, "mail service stopped successfully");
        }
        else
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_CRITICAL, "mail service stop failed, last error(%d)", err);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

void service_release(ma_service_t *service) {
    if(service) {
        ma_mail_service_release((ma_mail_service_t *)service) ;
        return ;
    }
    return ;
}

ma_error_t ma_mail_service_get_msgbus(ma_mail_service_t *service, ma_msgbus_t **msgbus) {
    if(service && msgbus) {
        *msgbus = service->msgbus;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t mail_send_handler(ma_mail_service_t *service, ma_message_t *req_msg, ma_message_t *rsp_msg) {
    if(service && req_msg && rsp_msg) {
        ma_error_t err = MA_OK;
        const char *from = NULL;
        const char *to = NULL;
        const char *sub = NULL;
        char *status = MA_MAIL_SEND_FAILED;
        ma_variant_t *envelope = NULL;
        char error_code[MA_MAX_LEN] = {0};

        /* TODO: attachments need to handle */
        if(MA_OK == (err = ma_message_get_property(req_msg, MA_MAIL_ID_FROM, &from)) &&
           MA_OK == (err = ma_message_get_property(req_msg, MA_MAIL_ID_TO, &to)) &&
           MA_OK == (err = ma_message_get_property(req_msg, MA_MAIL_SUBJECT, &sub))) {
            MA_LOG(service->logger, MA_LOG_SEV_TRACE, "mail send from(%s) to(%s) subject(%s) request", from, to, sub);
            if(MA_OK == ma_message_get_payload(req_msg, &envelope)) {
                ma_buffer_t *env_buf = NULL;

                status = MA_MAIL_SEND_SUCCESS;
                if(MA_OK == (err = ma_variant_get_string_buffer(envelope, &env_buf))) {
                    const char *body = NULL;
                    size_t len = 0;

                    (void)ma_buffer_get_string(env_buf, &body, &len);
                    if(MA_OK == (err = ma_mail_send(from, to, sub, body)))
                        MA_LOG(service->logger, MA_LOG_SEV_TRACE, "mail send successfully");
                    else
                        MA_LOG(service->logger, MA_LOG_SEV_ERROR, "mail send failed, last error(%d)", err);
                    (void)ma_buffer_release(env_buf);
                }
                (void)ma_variant_release(envelope);
            }
        }
        if(MA_OK != err)
            MA_LOG(service->logger, MA_LOG_SEV_CRITICAL, "mail send failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(rsp_msg, MA_MAIL_STATUS, status);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_mail_service_cb(ma_msgbus_server_t *server, ma_message_t *msg, void *cb_data, ma_msgbus_client_request_t *c_request) {
    if(server && msg && cb_data) {
        ma_error_t err = MA_OK;
        const char *msg_type = NULL;
        ma_message_t *response = NULL;
        ma_mail_service_t *service = (ma_mail_service_t*)cb_data;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(service->context);

        if(MA_OK == (err = ma_message_create(&response))) {
            if(MA_OK == (err = ma_message_get_property(msg, MA_MAIL_SERVICE_MSG_TYPE, &msg_type))) {
                if(!strcmp(msg_type, MA_MAIL_SERVICE_MSG_SEND_TYPE)) {
                    if(MA_OK == (err = mail_send_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client mail send request failed, last error(%d)", err);
                        }
                    }
                } else {
                    if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, MA_ERROR_SCHEDULER_INVALID_OP_TYPE, response))) {
                        MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client request failed, last error(%d)", err);
                    }
                }
            } else {
                if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err = MA_ERROR_SCHEDULER_TASK_TYPE_NOT_SET, response))) {
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client request failed, last error(%d)", err);
                }
            }
            if(MA_OK != ma_message_release(response)) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client request message release failed");
            }
        }
        return err;
    }
    return MA_OK;
}

ma_error_t mail_subscriber_cb(const char *topic, ma_message_t *msg, void *cb_data) {
    if(topic && msg && cb_data) {
        ma_error_t err = MA_OK;
        ma_mail_service_t *service = (ma_mail_service_t*)cb_data;

        MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "mail subscriber received sensor message(%s)", topic);
        if(!strcmp(topic, MA_SYSTEM_SENSOR_SYSTEM_STARTUP_PUB_TOPIC_STR)) {
            MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "booking processing (%s) message", topic);
        } else {
            /* do nothing */
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}


