#include "ma/internal/services/mail/ma_mail_utils.h"
#include "ma/internal/ma_macros.h"
#include "ma/ma_log.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <stdio.h>


#include <string.h>

#include <curl/curl.h>

#define URLSIZE 256
#ifdef MA_WINDOWS
#include <tchar.h>
#else
#include <unistd.h>
#include <ctype.h>
#endif

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "masvc"

extern ma_logger_t *mail_logger;

MA_CPP(extern "C" {)
ma_error_t ma_mail_send(const char *from, const char *to, const char *sub, const char *body) {
    if(from && to) {
        ma_error_t err = MA_ERROR_UNEXPECTED;
        char temp[MA_MAX_BUFFER_LEN+1] = {0};

        MA_LOG(mail_logger, MA_LOG_SEV_TRACE, "mail send request received from(%s) to(%s) sub(%s) body(%s)", from, to, sub ? sub : "empty", body ? body : "empty");
#ifdef MA_WINDOWS
        const char *sendmail = "sendmail.exe";
        MA_MSC_SELECT(_snprint, snprintf)(temp, MA_MAX_BUFFER_LEN, "%s",tempnam("c:\\temp\\", "sendmail"));
#else
        const char *sendmail = "ssmtp";
        MA_MSC_SELECT(_snprint, snprintf)(temp, MA_MAX_BUFFER_LEN, "%s",tempnam("/tmp/", "sendmail"));
#endif
        {
            FILE *fp = fopen(temp, "w");
            if(fp) {
                char cmd[MA_MAX_BUFFER_LEN+1] = {0};
                int rc = 0;

                fprintf(fp, "From: %s;\n", from);
                fprintf(fp, "To: %s;\n", to);
                if(sub)fprintf(fp, "Subject: %s\n", sub);
                if(body)fprintf(fp, "%s\n", body);
                fclose(fp);
                MA_MSC_SELECT(_sprintf, sprintf)(cmd, "%s %s < %s", sendmail, to, temp);
                if(-1 != (rc = system(cmd))) {
                    err = MA_OK;
                    MA_LOG(mail_logger, MA_LOG_SEV_ERROR, "sendmail error(%d)", rc);
                }
                unlink(temp);
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

/* * Send SMS C/C++ example need curllib download from http://curl.haxx.se/ */


struct MemoryStruct {
        char *memory;
        size_t size;
};

/* Converts a hex character to its integer value */

char from_hex(char ch) {
        return isdigit(ch) ? ch - '0' : tolower(ch) - 'a' + 10;
}

/* Converts an integer value to its hex character*/

char to_hex(char code) {
        static char hex[] = "0123456789abcdef";
        return hex[code & 15];
}

/* Returns a url-encoded version of str */

char *url_encode(char *str) {
        char *pstr = str, *buf = (char *)malloc(strlen(str) * 3 + 1), *pbuf = buf;
        while (*pstr) {
                if (isalnum(*pstr) || *pstr == '-' || *pstr == '_' || *pstr == '.' || *pstr == '~')
                        *pbuf++ = *pstr;
                else if (*pstr == ' ')
                        *pbuf++ = '+';
                else
                        *pbuf++ = '%', *pbuf++ = to_hex(*pstr >> 4), *pbuf++ = to_hex(*pstr & 15);
                pstr++;
        }
        *pbuf = '\0';
        return buf;
}

/* CURL Callback write function */

static size_t WriteMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp) {
        size_t realsize = size * nmemb;
        struct MemoryStruct *mem = (struct MemoryStruct *)userp;
        mem->memory = (char *)realloc(mem->memory, mem->size + realsize + 1);
        if (mem->memory == NULL) {
                /* out of memory! */
                printf("not enough memory (realloc returned NULL)\n");
                exit(EXIT_FAILURE);
        }
        memcpy(&(mem->memory[mem->size]), contents, realsize);
        mem->size += realsize;
        mem->memory[mem->size] = 0;
        return realsize;
}

/* Send SMS */

char * sendSMS(const char *username, const char *password, const char *number, char *message, char *originator) {
        static char url[URLSIZE] = "http://api.textmarketer.co.uk/gateway/?option=xml";
        char *encoded;
        CURL *curl;
        CURLcode res;
        struct MemoryStruct chunk;
        chunk.memory = (char *)malloc(1);  /* will be grown as needed by the realloc above */
        chunk.size = 0;    /* no data at this point */
        curl = curl_easy_init();
        if(curl) {
                strcat(url,  "&username=");
                strcat(url,  username);
                strcat(url,  "&password=");
                strcat(url,  password);
                strcat(url,  "&number=");
                strcat(url,  number);
                strcat(url,  "&message=");
                encoded = url_encode(message);
                strcat(url,  encoded);
                free(encoded);
                encoded = url_encode(originator);
                strcat(url,  "&orig=");
                strcat(url,  encoded);
                free(encoded);
                curl_easy_setopt(curl, CURLOPT_URL, url);
                /* send all data to this function  */
                curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
                /* we pass our 'chunk' struct to the callback function */
                curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&chunk);
                if((res = curl_easy_perform(curl)) != CURLE_OK) {
                        return NULL;
                }
                curl_easy_cleanup(curl);
        }
        return chunk.memory;
}

#if 0
int main(void) {
        char *response;
        response = sendSMS("myuser", "mypass", "4477777777", "test test test", "me");
        if(response != NULL) {
                printf(response);
                free(response);
        }
        getchar();
        return 0;
}
#endif

MA_CPP(})

