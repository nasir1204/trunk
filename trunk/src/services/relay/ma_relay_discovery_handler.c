#include "ma/internal/services/relay/ma_relay_discovery_handler.h"
#include "ma/internal/services/udp_server/ma_udp_connection.h"
#include "ma/internal/services/ma_agent_policy_settings_bag.h" 

#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/defs/ma_http_server_defs.h"
#include "ma/internal/defs/ma_relay_defs.h"
#include "ma/internal/utils/threading/ma_atomic.h"

#include "ma/internal/ma_macros.h"

#include <stdlib.h>
#include <stdio.h>

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "relay_handler"

static ma_bool_t relay_discovery_handler_on_match(ma_udp_msg_handler_t *self, ma_udp_msg_t *c_msg);
static ma_error_t relay_discovery_handler_on_msg(ma_udp_msg_handler_t *self, struct ma_udp_connection_s *c_request, ma_udp_msg_t *c_msg);
static ma_error_t relay_discovery_handler_add_ref(ma_udp_msg_handler_t *self);
static ma_error_t relay_discovery_handler_release(ma_udp_msg_handler_t *self);

static ma_udp_msg_handler_methods_t methods = {
	&relay_discovery_handler_on_match,
	&relay_discovery_handler_on_msg,
	&relay_discovery_handler_add_ref,
	&relay_discovery_handler_release
};

typedef struct ma_relay_discovery_handler_s ma_relay_discovery_handler_t;
struct ma_relay_discovery_handler_s{
	ma_udp_msg_handler_t		base;

	ma_logger_t					*logger;
	
	ma_bool_t					relay_enabled;

	ma_uint16_t					relay_port;

	ma_atomic_counter_t			ref_count;
};

ma_error_t ma_relay_discovery_handler_create(ma_udp_msg_handler_t **self){
	ma_relay_discovery_handler_t *handler = (ma_relay_discovery_handler_t*) calloc(1, sizeof(ma_relay_discovery_handler_t));
	if(handler){
		MA_ATOMIC_INCREMENT(handler->ref_count);
		*self = (ma_udp_msg_handler_t*)handler;
		ma_udp_msg_handler_init((ma_udp_msg_handler_t*)handler, &methods, handler);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_relay_discovery_handler_configure(ma_udp_msg_handler_t *self, ma_context_t *context){
	ma_relay_discovery_handler_t *handler = (ma_relay_discovery_handler_t*)(self);	
	ma_error_t rc = MA_OK;
	ma_int32_t is_enabled = 0;
	
	ma_policy_settings_bag_t *pso_bag = MA_CONTEXT_GET_POLICY_SETTINGS_BAG(context) ;
	handler->logger = MA_CONTEXT_GET_LOGGER(context);
	
	handler->relay_enabled = MA_FALSE;
	handler->relay_port = 0;

	if(MA_OK == ma_policy_settings_bag_get_int(pso_bag, MA_RELAY_SERVICE_SECTION_NAME_STR, MA_RELAY_KEY_IS_ENABLED_INT, MA_FALSE, &is_enabled, MA_FALSE)){
		handler->relay_enabled = is_enabled ? MA_TRUE : MA_FALSE;
	}

	/* if udp enabled.*/
	if(handler->relay_enabled){
		ma_int32_t port = MA_HTTP_SERVER_DEFAULT_TCP_PORT_INT;
		ma_policy_settings_bag_get_int(pso_bag, MA_HTTP_SERVER_SERVICE_SECTION_NAME_STR, MA_HTTP_SERVER_KEY_PORT_INT, MA_FALSE, &port, port);		
		handler->relay_port = port;
		MA_LOG(handler->logger, MA_LOG_SEV_DEBUG, "relay service enabled, relay port %d.", port);
	}
	return MA_OK;
}

static ma_bool_t relay_discovery_handler_on_match(ma_udp_msg_handler_t *self, ma_udp_msg_t *c_msg){
	ma_message_t *msg = NULL;
	ma_bool_t match = MA_FALSE;
	ma_relay_discovery_handler_t *handler = (ma_relay_discovery_handler_t*)(self);
	if(MA_OK == ma_udp_msg_get_ma_message(c_msg, &msg)){
		const char *type = NULL;
		ma_message_get_property(msg, MA_RELAY_MSG_PROP_KEY_TYPE_STR, &type);
		if(type && 0 == strcmp(type, MA_RELAY_MSG_PROP_VAL_REQUEST_RELAY_DISCOVERY_STR)){
			MA_LOG(handler->logger, MA_LOG_SEV_DEBUG, "Relay discovery message received.");
			match = MA_TRUE;		
		}
		(void)ma_message_release(msg);
	}
	return match;
}


static ma_error_t relay_discovery_handler_on_msg(ma_udp_msg_handler_t *self, struct ma_udp_connection_s *c_request, ma_udp_msg_t *c_msg){
	ma_udp_msg_t *response = NULL;
	ma_relay_discovery_handler_t *handler = (ma_relay_discovery_handler_t*)(self);

	if(handler->relay_enabled && handler->relay_port){
		if(MA_OK == ma_udp_msg_create(&response)){
			ma_message_t *msg = NULL;
			if(MA_OK == ma_message_create(&msg)){
				char port[7] = {0};
				MA_MSC_SELECT(_snprintf, snprintf)(port, 7,"%d", handler->relay_port);

				/*form message*/
				(void)ma_message_set_property(msg, MA_RELAY_MSG_PROP_KEY_TYPE_STR, MA_RELAY_MSG_PROP_VAL_RESPONSE_RELAY_DISCOVERY_STR);				
				(void)ma_message_set_property(msg, MA_RELAY_KEY_SERVER_PORT_INT, port);
				(void)ma_udp_msg_set_ma_message(response, msg);
				(void)ma_message_release(msg);

				/*udp connection response*/
				MA_LOG(handler->logger, MA_LOG_SEV_DEBUG, "Posting relay discovery response.");
				(void)ma_udp_connection_set_delayed_response(c_request, MA_TRUE);
				(void)ma_udp_connection_set_response_delay_timeout(c_request, 500);
				(void)ma_udp_connection_post_response(c_request, response);
			}			
			ma_udp_msg_release(response);
		}
	}
	else
		MA_LOG(handler->logger, MA_LOG_SEV_DEBUG, "Relay service disabled.");
	return MA_OK;
}

static ma_error_t relay_discovery_handler_add_ref(ma_udp_msg_handler_t *self){
	ma_relay_discovery_handler_t *handler = (ma_relay_discovery_handler_t*)(self);
	MA_ATOMIC_INCREMENT(handler->ref_count);
	return MA_OK;
}

static ma_error_t relay_discovery_handler_release(ma_udp_msg_handler_t *self){
	ma_relay_discovery_handler_t *handler = (ma_relay_discovery_handler_t*)(self);
	if(0 == MA_ATOMIC_DECREMENT(handler->ref_count))
		free(handler);	
	return MA_OK;
}