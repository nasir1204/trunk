#include "ma/internal/services/udp_server/ma_udp_server_service.h"
#include "ma/internal/services/udp_server/ma_udp_server.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/services/ma_agent_policy_settings_bag.h" 
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/defs/ma_udp_defs.h"
#include "ma/internal/defs/ma_relay_defs.h"

#include "ma/internal/services/relay/ma_compat_relay_discovery_handler.h"

#include "ma/ma_msgbus.h"
#include "ma/internal/core/msgbus/ma_msgbus_private.h"
#include "ma/ma_log.h"

#include <stdio.h>
#include <stdlib.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "relay"

typedef struct ma_compat_relay_server_service_s {

    ma_service_t                        base;

	ma_context_t                        *context;

	ma_logger_t                         *logger;
	
	ma_msgbus_t                         *msgbus;

	ma_udp_server_t                     *relay_discovery_server;
		
	ma_udp_msg_handler_t				*relay_compat_handler;
		
} ma_compat_relay_server_service_t;

static ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint);

static ma_error_t service_start(ma_service_t *service);

static ma_error_t service_stop(ma_service_t *service);

static void service_destroy(ma_service_t *service);

static const ma_service_methods_t methods = {
    &service_configure,
    &service_start,
    &service_stop,
    &service_destroy
};	

ma_error_t ma_compat_relay_server_service_create(const char *service_name, ma_service_t **udp_service){
	if(service_name && udp_service){

		ma_compat_relay_server_service_t *udp_s = (ma_compat_relay_server_service_t*) calloc(1, sizeof(ma_compat_relay_server_service_t));
		if(udp_s){
			ma_service_init(&udp_s->base, &methods, service_name, (void *)udp_s) ;
			*udp_service = (ma_service_t*)udp_s;
			return MA_OK;
		}
		return MA_ERROR_OUTOFMEMORY;
	}
	return MA_ERROR_INVALIDARG;	
}

static ma_error_t setup_relay_discovery_server(ma_compat_relay_server_service_t *self, ma_udp_server_policy_t *policy){
	ma_error_t rc = MA_OK;
	MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Setting up compat relay discovery server at port <%d> multicast address <%s>.", policy->port, policy->multicast_addr);		
	if(MA_OK == (rc = ma_udp_server_create(&self->relay_discovery_server))){
		ma_udp_server_set_logger(self->relay_discovery_server, self->logger);			
		ma_udp_server_set_event_loop(self->relay_discovery_server, ma_msgbus_get_event_loop(self->msgbus));										
		ma_udp_server_set_policy(self->relay_discovery_server, policy);

		if(MA_OK == ma_compat_relay_discovery_handler_create(&self->relay_compat_handler)){
			(void)ma_compat_relay_discovery_handler_configure(self->relay_compat_handler, self->context);
			(void)ma_udp_server_register_msg_handler(self->relay_discovery_server, self->relay_compat_handler);					
		}
		else{
			MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to create the compat relay discovery handler.");		
			self->relay_compat_handler = NULL;
		}
	}	
	return rc;
}

static ma_error_t creanup_relay_discovery_server(ma_compat_relay_server_service_t *self){
	if(self->relay_discovery_server){		

		if(self->relay_compat_handler){		
			(void)ma_udp_server_unregister_msg_handler(self->relay_discovery_server, self->relay_compat_handler);			
			ma_udp_msg_handler_release(self->relay_compat_handler);
			self->relay_compat_handler = NULL;
		}
		(void)ma_udp_server_stop(self->relay_discovery_server);
		(void)ma_udp_server_release(self->relay_discovery_server);
		self->relay_discovery_server = NULL;
	}
	return MA_OK;
}

static ma_error_t get_relay_discovery_server_policy(ma_policy_settings_bag_t *pso_bag, ma_udp_server_policy_t *udp_policy){
	ma_error_t rc = MA_OK;
	ma_int32_t is_enabled = 0;
	ma_int32_t port = 0;
	ma_policy_settings_bag_get_int(pso_bag, MA_RELAY_SERVICE_SECTION_NAME_STR, MA_RELAY_KEY_IS_ENABLED_INT, MA_FALSE,&is_enabled, 0) ;

	udp_policy->is_enabled = is_enabled ? MA_TRUE : MA_FALSE;
	udp_policy->port = MA_RELAY_COMPAT_PORT;
	udp_policy->multicast_addr = MA_UDP_MULTICAST_ADDR_STR;
	
	ma_policy_settings_bag_get_int(pso_bag, MA_UDP_SERVICE_SECTION_NAME_STR, MA_UDP_KEY_BROADCAST_PORT_INT, MA_FALSE,&port, MA_RELAY_COMPAT_PORT) ;
	/*udp server port == RELAY COMPAT port the disable the compat service*/
	if(port == MA_RELAY_COMPAT_PORT) udp_policy->is_enabled = MA_FALSE;	

	return rc ;
}

static ma_error_t configure_relay_discovery_server(ma_compat_relay_server_service_t *self, ma_policy_settings_bag_t *pso_bag){
	ma_error_t rc = MA_OK;	
	ma_udp_server_policy_t udp_policy = {0};	
	get_relay_discovery_server_policy(pso_bag, &udp_policy);
	if(udp_policy.is_enabled)
		return setup_relay_discovery_server(self, &udp_policy);	
	else
		return creanup_relay_discovery_server(self);			
}

static ma_bool_t is_policy_change(ma_compat_relay_server_service_t *self, ma_udp_server_policy_t *new_policy){
	if(self->relay_discovery_server){
		ma_udp_server_policy_t *old_policy = NULL;
		ma_udp_server_get_policy(self->relay_discovery_server, &old_policy);
		if(old_policy->is_enabled == new_policy->is_enabled && old_policy->port == new_policy->port && (0 == strcmp(new_policy->multicast_addr ? new_policy->multicast_addr : "", old_policy->multicast_addr ? old_policy->multicast_addr : "")))
			return MA_FALSE;		
	}
	return MA_TRUE;
}

static ma_error_t reconfigure_relay_discovery_server(ma_compat_relay_server_service_t *self, ma_policy_settings_bag_t *pso_bag){
	ma_error_t rc = MA_OK;
	if(self->relay_discovery_server){
		ma_udp_server_policy_t udp_policy = {0};	
		get_relay_discovery_server_policy(pso_bag, &udp_policy);

		if(is_policy_change(self, &udp_policy)){
			(void)creanup_relay_discovery_server(self);
			if(udp_policy.is_enabled){
				if(MA_OK == (rc = setup_relay_discovery_server(self, &udp_policy))){
					rc = ma_udp_server_start(self->relay_discovery_server);
				}
			}		
		}		
	}
	else {
		if(MA_OK == (rc = configure_relay_discovery_server(self, pso_bag))){
			if(self->relay_discovery_server)
				rc = ma_udp_server_start(self->relay_discovery_server);			
		}
	}
	return rc;
}

static ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint) {
    if(service && context){
		ma_error_t rc = MA_OK;
		ma_compat_relay_server_service_t *self = (ma_compat_relay_server_service_t*)service;
		ma_policy_settings_bag_t *pso_bag = MA_CONTEXT_GET_POLICY_SETTINGS_BAG(context) ;

		if(MA_SERVICE_CONFIG_NEW == hint){
			self->msgbus = MA_CONTEXT_GET_MSGBUS(context);
			self->logger = MA_CONTEXT_GET_LOGGER(context);
			self->context = context;
			if(self->msgbus && self->logger){				
				if(MA_OK != (rc = configure_relay_discovery_server(self, pso_bag)))
					MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to configure the compat relay service.");							
				return rc;
			}
			return MA_ERROR_PRECONDITION;
		}
		else{			
			if(MA_OK != (rc = reconfigure_relay_discovery_server(self, pso_bag)))
				MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to reconfigure the compat relay service.");						
			return rc;			
		}
		return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t service_start(ma_service_t *service) {
    if(service){
		ma_error_t rc = MA_OK;
		ma_compat_relay_server_service_t *self = (ma_compat_relay_server_service_t*)service;     
				
		if(self->relay_discovery_server){
			if(MA_OK == (rc = ma_udp_server_start(self->relay_discovery_server)))				
				MA_LOG(self->logger, MA_LOG_SEV_INFO, "Compat relay discovery server started.");										
			else
				MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to start compat relay discovery server.");			
		}
		return rc;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t service_stop(ma_service_t *service) {
    if(service){
		ma_error_t rc = MA_OK;
		ma_compat_relay_server_service_t *self = (ma_compat_relay_server_service_t*)service;     
				
		if(self->relay_discovery_server){						
			if(MA_OK == (rc = ma_udp_server_stop(self->relay_discovery_server)))
				MA_LOG(self->logger, MA_LOG_SEV_INFO, "Compat relay discovery server stopped.");			
			else
				MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to stop compat relay discovery server.");			
		}
		return rc;
    }
    return MA_ERROR_INVALIDARG;
}

static void service_destroy(ma_service_t *service) {
    if(service){
		ma_compat_relay_server_service_t *self = (ma_compat_relay_server_service_t*)service;  
		creanup_relay_discovery_server(self);				
		free(self);		
    }
}

