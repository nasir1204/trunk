#include "ma/ma_common.h"
#include "ma/internal/utils/filesystem/ma_fs_utils.h"
#include "ma/datastore/ma_ds_registry.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/ma_macros.h"
#include <stdio.h>

void ma_language_change(const char *lang_id, ma_int32_t b_override) {
	#ifdef MA_WINDOWS
	{
		ma_ds_t *ds = NULL;
		char *endstring = NULL;
		char mctray_p_lang[MA_MAX_LEN] = {0};
		ma_bool_t b_delete_registry = MA_FALSE;
		unsigned long mctray_lang_id = strtoul(lang_id, NULL, 16);

		if(0 == strcmp(lang_id, "0000") || 0 == strcmp(lang_id, "N/A")) {
			b_delete_registry = MA_TRUE;
		}
		_snprintf(mctray_p_lang, MA_MAX_LEN, "%ld", mctray_lang_id);
		if(MA_OK == ma_ds_registry_open(HKEY_LOCAL_MACHINE, MA_LOCALMACHINE_REGKEY_ROOT,  MA_TRUE, MA_TRUE, KEY_ALL_ACCESS | KEY_WOW64_32KEY, (ma_ds_registry_t **) &ds)) {
			if(!b_override) 
				ma_ds_remove(ds, MA_UPDATER_UI_LOCALIZATION_PICKUP_PATH, MA_UPDATER_UI_LANGUAGE_OVERRIDE);
			else 
				ma_ds_set_str(ds, MA_UPDATER_UI_LOCALIZATION_PICKUP_PATH, MA_UPDATER_UI_LANGUAGE_OVERRIDE, "1", 1);

			if(b_delete_registry) {
				ma_ds_remove(ds,MA_UPDATER_UI_LOCALIZATION_PICKUP_PATH, MA_UPDATER_UI_USE_LANGUAGE);
				ma_ds_remove(ds,MA_UPDATER_UI_LOCALIZATION_MCTRAY_PICKUP_PATH, MA_UPDATER_UI_MCTRAY_LANG);
			}
			else{
				ma_ds_set_str(ds, MA_UPDATER_UI_LOCALIZATION_PICKUP_PATH, MA_UPDATER_UI_USE_LANGUAGE, lang_id,strlen(lang_id));
				ma_ds_set_str(ds, MA_UPDATER_UI_LOCALIZATION_MCTRAY_PICKUP_PATH, MA_UPDATER_UI_MCTRAY_LANG, mctray_p_lang,strlen(mctray_p_lang));
			}
			ma_ds_release(ds);
		}
	}
	#endif
}

#ifdef MA_WINDOWS
static  void ma_service_remove_reboot_entry(ma_ds_t *ds) {
	if(ds) {
		ma_bool_t key_entry_exists = MA_FALSE;
		if(MA_OK == ma_ds_entry_exists(ds, REBOOT_PENDING_REGKEY_PATH, REBOOT_PENDING_REGKEY, &key_entry_exists)) {
			if(key_entry_exists)
				ma_ds_remove(ds, REBOOT_PENDING_REGKEY_PATH, REBOOT_PENDING_REGKEY);
		}
	}
}
#endif

void ma_service_pre_check_sequence(uv_loop_t *loop) {
#ifdef MA_WINDOWS
	{
		ma_ds_t *ds = NULL;
		if(MA_OK == ma_ds_registry_open(HKEY_LOCAL_MACHINE, MA_LOCALMACHINE_REGKEY_ROOT, MA_TRUE, MA_FALSE, KEY_ALL_ACCESS | KEY_WOW64_32KEY, (ma_ds_registry_t**) &ds)) {
			ma_bool_t path_entry_exists = MA_FALSE;

			if(MA_OK == ma_ds_entry_exists(ds, REBOOT_PENDING_REGKEY_PATH, REBOOT_MARKED_FILE_PATH, &path_entry_exists)) {
				if(path_entry_exists) {
					ma_buffer_t *buffer = NULL;
					if(MA_OK == ma_ds_get_str(ds, REBOOT_PENDING_REGKEY_PATH, REBOOT_MARKED_FILE_PATH, &buffer)) {
						const char *reg_value = NULL;
						size_t size;
						if(MA_OK == ma_buffer_get_string(buffer, &reg_value, &size)) {
							if(!ma_fs_is_file_exists(loop, reg_value)) {
								ma_ds_remove(ds, REBOOT_PENDING_REGKEY_PATH, REBOOT_MARKED_FILE_PATH);
								ma_service_remove_reboot_entry(ds);
							}
							else {
								MoveFileExA(reg_value, NULL, MOVEFILE_DELAY_UNTIL_REBOOT);
							}
						}
					}
				}
				else {
					ma_service_remove_reboot_entry(ds);
				}
			}
			ma_ds_release(ds);
		}
	}
#endif 
}

