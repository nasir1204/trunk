#include "ma/info/ma_info.h"
#include <ma/internal/ma_macros.h>
#include "ma/ma_datastore.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/utils/database/ma_db.h"
#include "ma/internal/utils/database/ma_db_recordset.h"
#include "ma/internal/utils/database/ma_db_statement.h"
#include "ma/internal/utils/datastructures/ma_bytebuffer.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/services/ma_policy_settings_bag.h"
#include "ma/internal/defs/ma_info_defs.h"
#include "ma/internal/utils/platform/ma_system_property.h"
#include "ma/internal/utils/repository/ma_sitelist.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "ma_info_handler"

typedef struct ma_non_transient_info_s{
	char *version;
	char *guid;
	char *log_location;
	char *install_location;
	char *crypto_mode;
	char *data_location;
	char *tenantId;
}ma_non_transient_info_t;

#define MA_CONTEXT_GET_DATASTORE(context) (ma_configurator_get_datastore(MA_CONTEXT_GET_CONFIGURATOR(context)))
#define MA_CONTEXT_GET_DATABASE(context) (ma_configurator_get_database(MA_CONTEXT_GET_CONFIGURATOR(context)))
								
static ma_error_t agent_non_transient_info_collect_(ma_context_t *context, ma_non_transient_info_t **agent_info){
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	if(context && agent_info){
		ma_buffer_t *buffer = NULL;
		size_t buf_size = 0;
		const char *info = NULL;
		if(*agent_info = (ma_non_transient_info_t*)calloc(1, sizeof(ma_non_transient_info_t))){
			if(MA_OK == ma_ds_get_str(MA_CONTEXT_GET_DATASTORE(context), MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_VERSION_STR, &buffer) 
				&& MA_OK == ma_buffer_get_string(buffer, &info, &buf_size)) 
			{
				(*agent_info)->version = strdup(info);
				
			}
			else	(*agent_info)->version = strdup("N/A");

			if(buffer) (void)ma_buffer_release(buffer), buffer = NULL ;

			if(MA_OK == ma_ds_get_str(MA_CONTEXT_GET_DATASTORE(context), MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_GUID_STR, &buffer) 
				&& MA_OK == ma_buffer_get_string(buffer, &info, &buf_size)) 
			{
				(*agent_info)->guid = strdup(info);
			}
			else	(*agent_info)->guid = strdup("N/A");
			
            if(buffer) (void)ma_buffer_release(buffer), buffer = NULL ;

			if(MA_OK == ma_ds_get_str(MA_CONTEXT_GET_DATASTORE(context), MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_TENANT_ID_STR, &buffer) 
				&& MA_OK == ma_buffer_get_string(buffer, &info, &buf_size)) 
			{
				(*agent_info)->tenantId = strdup(info);
			}
			else	(*agent_info)->tenantId = strdup("N/A");
			
            if(buffer) (void)ma_buffer_release(buffer), buffer = NULL ;

			if(MA_OK == ma_ds_get_str(MA_CONTEXT_GET_DATASTORE(context), MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_LOGS_PATH_STR, &buffer) 
				&& MA_OK == ma_buffer_get_string(buffer, &info, &buf_size)) 
			{
				(*agent_info)->log_location = strdup(info);
			}
			else	(*agent_info)->log_location = strdup("N/A");

			if(buffer) (void)ma_buffer_release(buffer), buffer = NULL ;

			if(MA_OK == ma_ds_get_str(MA_CONTEXT_GET_DATASTORE(context), MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_INSTALL_PATH_STR, &buffer)
				&& MA_OK == ma_buffer_get_string(buffer, &info, &buf_size)) 
			{
				(*agent_info)->install_location = strdup(info);
			}
			else	(*agent_info)->install_location = strdup("N/A");

			if(buffer) (void)ma_buffer_release(buffer), buffer = NULL ;

			if(MA_OK == ma_ds_get_str(MA_CONTEXT_GET_DATASTORE(context), MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_DATA_PATH_STR, &buffer)
				&& MA_OK == ma_buffer_get_string(buffer, &info, &buf_size)) 
			{
				(*agent_info)->data_location = strdup(info);
			}
			else	(*agent_info)->data_location = strdup("N/A");

			if(buffer) (void)ma_buffer_release(buffer), buffer = NULL ;

			if(MA_OK == ma_ds_get_str(MA_CONTEXT_GET_DATASTORE(context), MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_CRYPTO_MODE_INT, &buffer) 
				&& MA_OK == ma_buffer_get_string(buffer, &info, &buf_size)) 
			{
				(*agent_info)->crypto_mode = strdup(info);
			}
			else	(*agent_info)->crypto_mode = strdup("N/A");

			if(buffer) (void)ma_buffer_release(buffer), buffer = NULL ;

			rc = MA_OK;
		}
		else
			rc = MA_ERROR_APIFAILED;
	}
	return rc;
}

static ma_error_t ma_info_epo_server_list_get(ma_context_t *context, char **list){
	ma_error_t rc = MA_ERROR_APIFAILED ;
	
	ma_db_t *db_handle = MA_CONTEXT_GET_DATABASE(context);
	char *tmp_list = NULL ;
	ma_temp_buffer_t buffer_list = MA_TEMP_BUFFER_INIT;

	if(MA_OK == (rc = ma_sitelist_get_server_list(db_handle, MA_SERVER_LIST_ALL, &buffer_list))) {
		tmp_list = strdup((char *)ma_temp_buffer_get(&buffer_list));		
	}

	ma_temp_buffer_uninit(&buffer_list);
	if(!tmp_list)	tmp_list = strdup("N/A") ;
	
	*list = tmp_list ;
	return rc;
}

static ma_error_t ma_info_epo_port_list_get(ma_context_t *context, char **list){
	ma_error_t rc = MA_ERROR_APIFAILED ;
	
	ma_db_t *db_handle = MA_CONTEXT_GET_DATABASE(context);
	char *tmp_list = NULL ;
	ma_temp_buffer_t buffer_list = MA_TEMP_BUFFER_INIT;

	if(MA_OK == (rc = ma_sitelist_get_server_port_list_get(db_handle, &buffer_list))) {
		tmp_list = strdup((char *)ma_temp_buffer_get(&buffer_list));		
	}

	ma_temp_buffer_uninit(&buffer_list);
	if(!tmp_list)	tmp_list = strdup("N/A") ;
	
	*list = tmp_list ;
	return rc;
}

static ma_error_t ma_info_asc_time_get(ma_context_t *context, char **value){
	if(context){
		ma_buffer_t *buffer = NULL;
		size_t buf_size = 0;
		const char *info = NULL;
		if(MA_OK == ma_ds_get_str(MA_CONTEXT_GET_DATASTORE(context), MA_PROPERTY_DATA_PATH_NAME_STR, MA_PROPERTY_DATA_KEY_ASC_TIME_STR, &buffer) ) {
			if(MA_OK == ma_buffer_get_string(buffer, &info, &buf_size)) {
				*value = strdup(info);
			}
			ma_buffer_release(buffer);
		}
		if(!(*value))
			*value = strdup("N/A");
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_info_custom_props_get(ma_context_t *context, ma_table_t *info_table){
	if(context){
		ma_ds_t *ds = MA_CONTEXT_GET_DATASTORE(context);
		if(ds){
			int i = 1; /* UserProperty1, ....., UserProperty4 */
			for(; i < 5; i++){
				ma_buffer_t *buffer = NULL;
				char cust_prop_key[64] = {0};
				MA_MSC_SELECT(_snprintf, snprintf)(cust_prop_key, 63, "%s%d", MA_PROPERTY_PROPERTIES_CUSTOM_STR, i);
				if(MA_OK == ma_ds_get_str(ds, MA_PROPERTY_PROPERTIES_PATH_NAME_STR, cust_prop_key, &buffer) ) {
					size_t buf_size = 0;
					const char *props = NULL;
					if(MA_OK == ma_buffer_get_string(buffer, &props, &buf_size) && props) {
						ma_variant_t	*var_info = NULL ;
						if(MA_OK == ma_variant_create_from_string(props, strlen(props), &var_info)) {
							(void)ma_table_add_entry(info_table, cust_prop_key, var_info) ;
							(void)ma_variant_release(var_info) ;	var_info = NULL ;
						}
					}
					ma_buffer_release(buffer);
				}
			}
			return MA_OK;
		}
	}
	return MA_ERROR_INVALIDARG;
}			

static ma_error_t ma_info_epo_version_get(ma_context_t *context, char **value){
	if(context){
		ma_buffer_t *buffer = NULL;
		size_t buf_size = 0;
		const char *info = NULL;
		if(MA_OK == ma_ds_get_str(MA_CONTEXT_GET_DATASTORE(context), MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_EPO_VERSION_STR, &buffer) ) {
			if(MA_OK == ma_buffer_get_string(buffer, &info, &buf_size)) {
				*value = strdup(info);
			}
			ma_buffer_release(buffer);
		}
		if(!(*value))
			*value = strdup("N/A");
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_info_agent_mode_get(ma_context_t *context, char **value){
	if(context){
		ma_int32_t is_cloud = 0;
		if(MA_OK == ma_policy_settings_bag_get_int(MA_CONTEXT_GET_POLICY_SETTINGS_BAG(context), MA_AGENT_GENERAL_SECTION_NAME_STR, MA_AGENT_KEY_IS_CLOUD_INT, MA_TRUE, &is_cloud, MA_FALSE) && is_cloud) {
			*value = strdup("2");
		} else {
			ma_buffer_t *buffer = NULL;
			size_t buf_size = 0;
			const char *info = NULL;
			if( (MA_OK == ma_ds_get_str(MA_CONTEXT_GET_DATASTORE(context), MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_MODE_INT, &buffer))
				&& (MA_OK == ma_buffer_get_string(buffer, &info, &buf_size)) && info ) {
				*value = strdup(info);
			}
			else
				*value = strdup("N/A");

			if(buffer) {
				(void)ma_buffer_release(buffer);
				buffer = NULL;
			}
			return MA_OK;
		}
	}
	return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_info_policy_update_time_get(ma_context_t *context, char **value){
	if(context){
		ma_buffer_t *buffer = NULL;
		size_t buf_size = 0;
		const char *info = NULL;
		if(MA_OK == ma_ds_get_str(MA_CONTEXT_GET_DATASTORE(context), MA_POLICY_DATA_PATH_NAME_STR, MA_POLICY_UPDATE_TIME_STR, &buffer) ) {
			if(MA_OK == ma_buffer_get_string(buffer, &info, &buf_size)) {
				*value = strdup(info);
			}
			ma_buffer_release(buffer);
		}
		if(!(*value))
			*value = strdup("N/A");
	}
	return MA_ERROR_INVALIDARG;
}

/*Used 4.8 logic, Need to modify once appropriate entry get addd in DB */
static ma_error_t ma_info_last_epo_server_used_get(ma_context_t *context, char **value){
	return ma_info_epo_server_list_get(context, value);
}

ma_error_t ma_info_collect(ma_context_t *context, ma_message_t *msg, ma_msgbus_client_request_t *c_request){

	static ma_non_transient_info_t *non_transient_agent_info = NULL;
	ma_error_t rc = MA_OK;
	const char *msg_type = NULL;
	ma_message_t *response = NULL ;
	ma_variant_t *response_payload = NULL ;
		
	MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_DEBUG, "Received get agent info request.") ;

	if(!non_transient_agent_info){ /*This aggent values remain same for process lifetime, no need to fill again */
		rc = agent_non_transient_info_collect_(context, &non_transient_agent_info) ;
	}

	if(MA_OK == rc){
		ma_table_t *info_table = NULL ;

		if(MA_OK == (rc = ma_table_create(&info_table))) {
			ma_variant_t	*var_info = NULL ;
			char *value = NULL ;
			ma_buffer_t *buffer = NULL ;
			size_t size = 0 ;			
			
			/* Default values */
			const char *properties_collection_timeout = "60";
			const char *policy_enforcement_timeout = "60";
			const char *event_upload_timeout = "5";

			ma_policy_settings_bag_t *policy_bag = MA_CONTEXT_GET_POLICY_SETTINGS_BAG(context) ;
			
			if(MA_OK == ma_variant_create_from_string(non_transient_agent_info->version, strlen(non_transient_agent_info->version), &var_info)) {
				(void)ma_table_add_entry(info_table, MA_INFO_VERSION_STR, var_info) ;
				(void)ma_variant_release(var_info) ;	var_info = NULL ;
			}
			if(MA_OK == ma_variant_create_from_string(non_transient_agent_info->guid, strlen(non_transient_agent_info->guid), &var_info)) {
				(void)ma_table_add_entry(info_table, MA_INFO_GUID_STR, var_info) ;
				(void)ma_variant_release(var_info) ;	var_info = NULL ;
			}
			if(MA_OK == ma_variant_create_from_string(non_transient_agent_info->tenantId, strlen(non_transient_agent_info->tenantId), &var_info)) {
				(void)ma_table_add_entry(info_table, MA_INFO_TENANTID_STR, var_info) ;
				(void)ma_variant_release(var_info) ;	var_info = NULL ;
			}
			if(MA_OK == ma_variant_create_from_string(non_transient_agent_info->log_location, strlen(non_transient_agent_info->log_location), &var_info)) {
				(void)ma_table_add_entry(info_table, MA_INFO_LOG_LOCATION_STR, var_info) ;
				(void)ma_variant_release(var_info) ;	var_info = NULL ;
			}
			if(MA_OK == ma_variant_create_from_string(non_transient_agent_info->install_location, strlen(non_transient_agent_info->install_location), &var_info)) {
				(void)ma_table_add_entry(info_table, MA_INFO_INSTALL_LOCATION_STR, var_info) ;
				(void)ma_variant_release(var_info) ;	var_info = NULL ;
			}
			if(MA_OK == ma_variant_create_from_string(non_transient_agent_info->crypto_mode, strlen(non_transient_agent_info->crypto_mode), &var_info)) {
				(void)ma_table_add_entry(info_table, MA_INFO_CRYPTO_MODE_STR, var_info) ;
				(void)ma_variant_release(var_info) ;	var_info = NULL ;
			}
			if(MA_OK == ma_variant_create_from_string(non_transient_agent_info->data_location, strlen(non_transient_agent_info->data_location), &var_info)) {
				(void)ma_table_add_entry(info_table, MA_INFO_DATA_LOCATION_STR, var_info) ;
				(void)ma_variant_release(var_info) ;	var_info = NULL ;
			}

			(void)ma_info_agent_mode_get(context, &value);
			if(MA_OK == ma_variant_create_from_string(value, strlen(value), &var_info)) {
				(void)ma_table_add_entry(info_table, MA_INFO_AGENT_MODE_STR, var_info) ;
				(void)ma_variant_release(var_info) ;	var_info = NULL ;
			}
			free(value) ; value = NULL ;

			(void)ma_info_epo_version_get(context, &value);
			if(MA_OK == ma_variant_create_from_string(value, strlen(value), &var_info)) {
				(void)ma_table_add_entry(info_table, MA_INFO_EPO_VERSION_STR, var_info) ;
				(void)ma_variant_release(var_info) ;	var_info = NULL ;
			}
			free(value) ; value = NULL ;
			
			(void)ma_info_epo_server_list_get(context, &value) ;
			if(MA_OK == ma_variant_create_from_string(value, strlen(value), &var_info)) {
				(void)ma_table_add_entry(info_table, MA_INFO_EPO_SERVER_LIST, var_info) ;
				(void)ma_variant_release(var_info) ;	var_info = NULL ;
			}
			free(value) ; value = NULL ;
				
			(void)ma_info_epo_port_list_get(context, &value) ;
			if(MA_OK == ma_variant_create_from_string(value, strlen(value), &var_info)) {
				(void)ma_table_add_entry(info_table, MA_INFO_EPO_PORT_LIST, var_info) ;
				(void)ma_variant_release(var_info) ;	var_info = NULL ;
			}
			free(value) ; value = NULL ;

			if(MA_OK == ma_ds_get_str(MA_CONTEXT_GET_DATASTORE(context), MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_LAST_COMMUNICATED_EPO_SERVER_STR, &buffer)) {
				(void)ma_buffer_get_string(buffer, &value, &size) ;
				if(MA_OK == ma_variant_create_from_string(value, strlen(value), &var_info)) {
					(void)ma_table_add_entry(info_table, MA_INFO_EPO_SERVER_LAST_USED_STR, var_info) ;
					(void)ma_variant_release(var_info) ;	var_info = NULL ;
				}
				ma_buffer_release(buffer) ; buffer = NULL ;
				value = NULL ;
			}

			{
				ma_int64_t epo_port;
				char epo_port_str[16] = {0};
				(void)ma_ds_get_int64(MA_CONTEXT_GET_DATASTORE(context), MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_LAST_COMMUNICATED_EPO_PORT_INT, &epo_port) ;
				MA_MSC_SELECT(_snprintf, snprintf)(epo_port_str, 15, "%d", epo_port);
				if(MA_OK == ma_variant_create_from_string(epo_port_str, strlen(epo_port_str), &var_info)) {
					(void)ma_table_add_entry(info_table, MA_INFO_EPO_PORT_LAST_USED_STR, var_info) ;
					(void)ma_variant_release(var_info) ;	var_info = NULL ;
				}
			}
			
			(void)ma_info_asc_time_get(context, &value) ;
			if(MA_OK == ma_variant_create_from_string(value, strlen(value), &var_info)) {
				(void)ma_table_add_entry(info_table, MA_INFO_LAST_ASC_TIME, var_info) ;
				(void)ma_variant_release(var_info) ;	var_info = NULL ;
			}
			free(value) ; value = NULL ;

			(void)ma_info_policy_update_time_get(context, &value) ;
			if(MA_OK == ma_variant_create_from_string(value, strlen(value), &var_info)) {
				(void)ma_table_add_entry(info_table, MA_INFO_LAST_POLICY_UPDATE_TIME, var_info) ;
				(void)ma_variant_release(var_info) ;	var_info = NULL ;
			}
			free(value) ; value = NULL ;
			
			(void)ma_policy_settings_bag_get_str(policy_bag, MA_PROPERTY_SERVICE_SECTION_NAME_STR, MA_PROPERTY_KEY_COLLECTION_TIMEOUT_INT, MA_FALSE, &value, properties_collection_timeout) ;
			if(MA_OK == ma_variant_create_from_string(value, strlen(value), &var_info)) {
				(void)ma_table_add_entry(info_table, MA_INFO_ASC_INTERVAL_STR, var_info) ;
				(void)ma_variant_release(var_info) ;	var_info = NULL ;
			}
			value = NULL ;

			(void)ma_policy_settings_bag_get_str(policy_bag, MA_POLICY_SERVICE_SECTION_NAME_STR, MA_POLICY_KEY_ENFORCEMENT_TIMEOUT_INT, MA_FALSE, &value, policy_enforcement_timeout) ;
			if(MA_OK == ma_variant_create_from_string(value, strlen(value), &var_info)) {
				(void)ma_table_add_entry(info_table, MA_INFO_POLICY_ENFORCE_INTERVAL_STR, var_info) ;
				(void)ma_variant_release(var_info) ;	var_info = NULL ;
			}
			value = NULL ;

			(void)ma_policy_settings_bag_get_str(policy_bag, MA_EVENT_SERVICE_SECTION_NAME_STR, MA_EVENT_KEY_UPLOAD_TIMEOUT_INT, MA_FALSE, &value, event_upload_timeout) ;
			if(MA_OK == ma_variant_create_from_string(value, strlen(value), &var_info)) {
				(void)ma_table_add_entry(info_table, MA_INFO_EVENTS_UPLOAD_INTERVAL_STR, var_info) ;
				(void)ma_variant_release(var_info) ;	var_info = NULL ;
			}
			value = NULL ;

			{
				ma_system_property_t *sys_props = MA_CONTEXT_GET_SYSTEM_PROPERTY(context) ;
				const ma_network_system_property_t *net = ma_system_property_network_get(sys_props, MA_FALSE) ;

				if(MA_OK == ma_variant_create_from_string(net->ip_address_formated, strlen(net->ip_address_formated), &var_info)) {
					(void)ma_table_add_entry(info_table, MA_INFO_IPADDRESS_STR, var_info) ;
					(void)ma_variant_release(var_info) ;	var_info = NULL ;
				}

				if(MA_OK == ma_variant_create_from_string(net->fqdn, strlen(net->fqdn), &var_info)) {
					(void)ma_table_add_entry(info_table, MA_INFO_HOSTNAME_STR, var_info) ;
					(void)ma_variant_release(var_info) ;	var_info = NULL ;
				}
			}

			(void)ma_info_custom_props_get(context, info_table) ;
			
			(void)ma_variant_create_from_table(info_table, &response_payload) ;
			(void)ma_table_release(info_table) ;	info_table = NULL ;
		}
	}

    if(MA_OK == ma_message_create(&response)) {
		char *info_event = NULL ;

		MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_DEBUG, "creating agent info response.") ;

		(void)ma_message_set_payload(response, response_payload) ;

		(void)ma_message_get_property(msg, MA_INFO_MSG_KEY_EVENT_TYPE_STR, &info_event) ;
		if(info_event) {
			if(MA_OK != (rc = ma_msgbus_publish(MA_CONTEXT_GET_MSGBUS(context), MA_INFO_EVENTS_PUBSUB_TOPIC_STR, MSGBUS_CONSUMER_REACH_OUTPROC, response)))
				MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_ERROR, "Failed to publish ma info message, rc = <%d>.", rc) ;
		}
		else {
			(void)ma_message_set_property(response, MA_AGENT_INFO_MSG_PROP_KEY_MSG_TYPE_STR, MA_AGENT_INFO_MSG_PROP_VAL_RPLY_GET_AGENT_INFO_STR) ;

			if(MA_OK != (rc = ma_msgbus_server_client_request_post_result(c_request, rc, response)))
				MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_ERROR, "Failed to post ma info response, rc = <%d>.", rc) ;
		}
		(void)ma_message_release(response);
	}
	(void)ma_variant_release(response_payload) ;	

	return rc;
}


