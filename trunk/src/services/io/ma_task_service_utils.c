#include "ma/internal/services/io/ma_task_service_utils.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/services/scheduler/ma_scheduler_core.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"
#include "ma/internal/services/scheduler/ma_task_internal.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/services/ma_policy_settings_bag.h"

#include "ma/internal/defs/ma_property_defs.h"
#include "ma/internal/defs/ma_updater_defs.h"
#include "ma/internal/defs/ma_io_defs.h"
#include "ma/internal/defs/ma_udp_defs.h"

#include "ma/internal/services/io/ma_task_service.h"
#include "ma/internal/utils/app_info/ma_registry_store.h"

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "io.service"

#define MA_TASK_MAX_RUN_TIME   4
#define MA_TASK_DAYS_INTERVAL  1

ma_error_t ma_task_service_task_create(ma_context_t *context, const char *task_id, const char *task_name, const char *task_type, const char *creator, ma_int16_t interval, ma_int64_t begin_after_secs, ma_bool_t enabled) {
    if(context && task_id && task_name && task_type && creator && interval > 0) {
        ma_scheduler_t *scheduler = MA_CONTEXT_GET_SCHEDULER(context);
        ma_task_t *task = NULL ;
        ma_trigger_t *trigger = NULL ;
        ma_error_t rc = MA_OK ;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(context);

        if(MA_ERROR_SCHEDULER_INVALID_TASK_ID == (rc = ma_scheduler_get_task_handle(scheduler, task_id, &task))) {
            if(MA_OK == (rc = ma_scheduler_create_task(scheduler, creator, task_id, task_name, task_type, &task))) {
                ma_task_time_t start_date = {0} ;
                ma_trigger_daily_policy_t daily_policy = {0} ;
                ma_task_repeat_policy_t repeat_policy = {0} ;
                
                ma_configurator_t *configurator = MA_CONTEXT_GET_CONFIGURATOR(context);

                /* Sets Agent product ID */
                (void)ma_task_set_software_id(task, ma_configurator_get_agent_software_id(configurator));
                (void)ma_task_set_max_run_time(task, MA_TASK_MAX_RUN_TIME) ;
                (void)ma_task_set_max_run_time_limit(task, MA_TASK_MAX_RUN_TIME) ;
				(void)ma_task_set_condition(task, MA_TASK_COND_HIDDEN) ;
                (void)ma_task_set_condition(task, MA_TASK_COND_ADJUST_BEGIN_DATE_ON_SYSTEM_TIME_CHANGE);
				if(!enabled) {
					MA_LOG(logger, MA_LOG_SEV_DEBUG, "Task %s is disabled", task_id) ;
					(void)ma_task_set_condition(task, MA_TASK_COND_DISABLED) ;
				}

				if(!strcmp(task_id, MA_P2P_CONTENT_PURGE_TASK_ID_STR) || !strcmp(task_id, MA_HTTP_SERVER_SUPERAGENT_REPO_PURGE_TASK_ID_STR)) {
                    daily_policy.days_interval = interval ;
                    /* interval * 24 hours * 60 minutes * 60 seconds */
                    advance_current_date_by_seconds(interval * 24 * 60 * 60, &start_date) ;
                }
                else {					
                    ma_task_repeat_policy_t repeat_policy = {0} ;

                    advance_current_date_by_seconds(begin_after_secs, &start_date) ;
                    daily_policy.days_interval = MA_TASK_DAYS_INTERVAL ;
                    repeat_policy.repeat_interval.hour = 0 ;
                    repeat_policy.repeat_interval.minute = (ma_int16_t)interval ;
                    repeat_policy.repeat_until.hour = 24 ; //24 hrs
                    repeat_policy.repeat_until.minute = 0 ;
                    (void)ma_task_set_repeat_policy(task, repeat_policy) ;
                }
                
                

                if(MA_OK == (rc = ma_trigger_create_daily(&daily_policy, &trigger))) {
                    if(MA_OK == (rc = ma_trigger_set_begin_date(trigger, &start_date))) {
                        if(MA_OK == (rc = ma_task_add_trigger(task, trigger))) {
                                MA_LOG(logger, MA_LOG_SEV_DEBUG, "Task service task(id = %s) created, repeat interval = %d. ", task_id, interval) ;
                                return rc ;
                            }
                        }
                    (void)ma_trigger_release(trigger);
                }
            }
            MA_LOG(logger, MA_LOG_SEV_ERROR, "Task service task(id = %s) create failed (%d)", task_id, rc) ;
            return rc ;
        }
        else if(MA_OK == rc ) {
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "Task service task(id = %s) already created ", task_id) ;
            (void)ma_task_release(task);
            return rc ;
        }
        else {
            MA_LOG(logger, MA_LOG_SEV_ERROR, "Task service task(id = %s) get failed (%d)", task_id, rc) ;
            return rc ;
        }
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_task_service_task_update(ma_context_t *context, const char *task_id, const char *task_name, const char *task_type, ma_int16_t interval, ma_bool_t enabled) {
    if(context && task_id && task_name && task_type && interval > 0) {
        ma_task_t *task = NULL ;
        ma_error_t rc = MA_OK ;
        ma_task_time_t start_date = {0} ;
                
        ma_scheduler_t *scheduler = MA_CONTEXT_GET_SCHEDULER(context) ;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(context);

        if(MA_OK == (rc = ma_scheduler_get_task_handle(scheduler, task_id, &task))) {
            ma_trigger_list_t *tl = NULL ;
            ma_trigger_t *old_trigger = NULL, *new_trigger = NULL ;		
            ma_trigger_daily_policy_t daily_policy = {0} ;

			/* unset all the conditions set previously */
			(void)ma_task_unset_condition(task, MA_TASK_COND_DISABLED) ;
			(void)ma_task_unset_condition(task, MA_TASK_COND_HIDDEN) ;

			if(!enabled) {
				(void)ma_task_set_condition(task, MA_TASK_COND_DISABLED) ;
				(void)ma_task_set_condition(task, MA_TASK_COND_HIDDEN) ;
				MA_LOG(logger, MA_LOG_SEV_DEBUG, "Disabling task %s", task_id) ;
			}
			else {
				MA_LOG(logger, MA_LOG_SEV_DEBUG, "Enabling task %s", task_id) ;
				(void)ma_task_set_condition(task, MA_TASK_COND_HIDDEN) ;
			}

			/*delete the old trigger */
            if(MA_OK == (rc = ma_task_get_trigger_list(task, &tl))) {             
                
                if(MA_OK == (rc = ma_trigger_list_at(tl, 0, &old_trigger))) {
                    const char *trigger_id = 0 ;
                    if(MA_OK == (rc = ma_trigger_get_id(old_trigger, &trigger_id))) {
                        (void)ma_task_remove_trigger(task, trigger_id) ;
                    }
                    (void)ma_trigger_release(old_trigger);
                }                
            }

            if(0 == strcmp(task_id, MA_P2P_CONTENT_PURGE_TASK_ID_STR) || !strcmp(task_id, MA_HTTP_SERVER_SUPERAGENT_REPO_PURGE_TASK_ID_STR)) {
                daily_policy.days_interval = interval ;
                /* interval * 24 hours * 60 minutes * 60 seconds */
                advance_current_date_by_seconds(interval * 24 * 60 * 60, &start_date) ;
            }
            else {
                ma_task_repeat_policy_t repeat_policy = {0} ;
                advance_current_date_by_seconds(interval * 60, &start_date) ;
                daily_policy.days_interval = MA_TASK_DAYS_INTERVAL;
                repeat_policy.repeat_interval.hour = 0 ;
                repeat_policy.repeat_interval.minute = (ma_int16_t)interval ;
                repeat_policy.repeat_until.hour = 24 ; //24 hrs
                repeat_policy.repeat_until.minute = 0 ;
                (void)ma_task_set_repeat_policy(task, repeat_policy) ;
            }

            if(MA_OK == (rc = ma_trigger_create_daily(&daily_policy, &new_trigger))) {
                if(MA_OK == (rc = ma_trigger_set_begin_date(new_trigger, &start_date))) {
                    if(MA_OK == (rc = ma_task_add_trigger(task, new_trigger))) {              
                        if(MA_OK == (rc = ma_scheduler_modify_task(scheduler, task))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "Task service Updated task(%s) with interval(%d)", task_id, interval) ;
                        }                
                    }
                }
                (void)ma_trigger_release(new_trigger);
            }
            (void)ma_task_release(task);
        }
        if(MA_OK != rc)
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "Task service Updated task(%s) failed", task_id) ;

        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_task_service_task_set_status(ma_context_t *context, const char *task_id, ma_task_exec_status_t status) {
    if(context && task_id) {
        ma_task_t *task = NULL ;
        ma_error_t rc = MA_OK ;
        ma_scheduler_t *scheduler = MA_CONTEXT_GET_SCHEDULER(context) ;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(context);

        if(MA_OK == (rc = ma_scheduler_get_task_handle(scheduler, task_id, &task))) {
            if(MA_OK == (rc = ma_task_set_execution_status(task, status))) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "Task service task(%s) status set to %d", task_id, status) ;
            }
            (void)ma_task_release(task);
        }
        if(MA_OK != rc)
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "Task service task(%s) set status(%d) failed(%d)", task_id, status, rc) ;
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_task_service_task_start(ma_context_t *context, const char *task_id, const char *task_name, const char *task_type) {
    if(context && task_id && task_name && task_type) {
        ma_task_t *task = NULL ;
        ma_error_t rc = MA_OK ;
        ma_scheduler_t *scheduler = MA_CONTEXT_GET_SCHEDULER(context) ;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(context);

        if(MA_OK == (rc = ma_scheduler_get_task_handle(scheduler, task_id, &task))) {
            if(MA_OK == (rc = ma_task_start(task))) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "Task service task(%s) started", task_id) ;
            }
            (void)ma_task_release(task);
        }
        if(MA_OK != rc)
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "Task service task(%s) start failed(%d)", task_id, rc) ;

        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_task_service_task_stop(ma_context_t *context, const char *task_id, const char *task_name, const char *task_type) {
    if(context && task_id && task_name && task_type) {
        ma_task_t *task = NULL ;
        ma_error_t rc = MA_OK ;
        ma_scheduler_t *scheduler = MA_CONTEXT_GET_SCHEDULER(context) ;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(context);

        if(MA_OK == (rc = ma_scheduler_get_task_handle(scheduler, task_id, &task))) {
            if(MA_OK == (rc = ma_task_stop(task))) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "Task service task(%s) stopped", task_id) ;
            }
            (void)ma_task_release(task);
        }
        if(MA_OK != rc)
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "Task service task(%s) stop failed(%d)", task_id, rc) ;

        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}


static ma_bool_t is_pip_installed(const char *application_name){
	ma_registry_store_t *reg_store  = NULL; 
	ma_bool_t app_on = MA_FALSE;
	if(MA_OK == ma_registry_store_create(&reg_store)){
		if(MA_OK == ma_registry_store_scan(reg_store)){
			if(ma_registry_store_get_application(reg_store, application_name)) 
				app_on = MA_TRUE;	
		}
		ma_registry_store_release(reg_store);
	}
	return app_on;
}

/*  TELEMTRY deployment task */
#define MA_TELEMTRY_SOFTWARE_ID_STR										"TELEMTRY1000"
#define MA_TELEMTRY_DEP_TASK_NAME_STR									"ma.task.deployment.TELEMTRY1000"
#define MA_TELEMTRY_DEP_TASK_ID_STR										"ma.task.id.deployment.TELEMTRY1000"

#define MA_POLICY_TELEMTRY_SECTION_NAME_STR								"Telemetry"
#define MA_POLICY_TELEMTRY_KEY_BRANCH_NAME_STR							"branch"
#define MA_POLICY_TELEMTRY_KEY_OPTIN_STR								"optIn"
#define MA_POLICY_TELEMTRY_KEY_SERVER_LEVEL_STR							"serverLevelInstallFlag"

#define MA_TELEMTRY_DEP_TASK_SETTING_SECTION_STR						"Install\\TELEMTRY1000"
#define MA_TELEMTRY_DEP_TASK_SETTING_PKG_PATH_STR						"PackagePathType"

void ma_telemtry_deployment_task_run(struct ma_context_s *context){
	#ifdef MA_WINDOWS
	if(context) {
		ma_error_t rc = MA_OK;		
		const char *branch_name = NULL;
		const char *opt_in = NULL;
		const char *server_setting = NULL;

		(void)ma_policy_settings_bag_get_str(MA_CONTEXT_GET_POLICY_SETTINGS_BAG(context), MA_POLICY_TELEMTRY_SECTION_NAME_STR, MA_POLICY_TELEMTRY_KEY_BRANCH_NAME_STR, MA_TRUE, &branch_name, NULL);
		(void)ma_policy_settings_bag_get_str(MA_CONTEXT_GET_POLICY_SETTINGS_BAG(context), MA_POLICY_TELEMTRY_SECTION_NAME_STR, MA_POLICY_TELEMTRY_KEY_OPTIN_STR, MA_TRUE, &opt_in, NULL);
		(void)ma_policy_settings_bag_get_str(MA_CONTEXT_GET_POLICY_SETTINGS_BAG(context), MA_POLICY_TELEMTRY_SECTION_NAME_STR, MA_POLICY_TELEMTRY_KEY_SERVER_LEVEL_STR, MA_TRUE,&server_setting, NULL);

		if(branch_name && opt_in && server_setting){
					
			ma_task_t *task = NULL ;
			ma_error_t rc = MA_OK ;
			ma_scheduler_t *scheduler = MA_CONTEXT_GET_SCHEDULER(context) ;
			ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(context);
			MA_LOG(logger, MA_LOG_SEV_DEBUG, "Product improvement program policy (%s:%s:%s).", branch_name, opt_in, server_setting) ;

			if(0 != strlen(branch_name) && !strcmp(opt_in, "true") && !strcmp(server_setting, "true")){

				if(is_pip_installed(MA_TELEMTRY_SOFTWARE_ID_STR)) return;

				if(MA_ERROR_SCHEDULER_INVALID_TASK_ID == (rc = ma_scheduler_get_task_handle(scheduler, MA_TELEMTRY_DEP_TASK_ID_STR, &task))) {

					if(MA_OK == (rc = ma_scheduler_create_task(scheduler, MA_SOFTWAREID_GENERAL_STR, MA_TELEMTRY_DEP_TASK_ID_STR, MA_TELEMTRY_DEP_TASK_NAME_STR, MA_UPDATER_TASK_TYPE_COMPAT_DEPLOYMENT_STR, &task))) {
						ma_variant_t *setting_value = NULL;
						char install_key[10] = {0};
						ma_trigger_run_now_policy_t run_now = {MA_TRUE};				
						ma_trigger_t *trigger = NULL ;
							
						MA_LOG(logger, MA_LOG_SEV_INFO, "Product improvement program deployment task (id = %s) created.", MA_TELEMTRY_DEP_TASK_ID_STR, rc) ;
						/* Sets Agent product ID */
						(void)ma_task_set_software_id(task, MA_SOFTWAREID_GENERAL_STR);				                
						(void)ma_task_set_condition(task, MA_TASK_COND_DELETE_WHEN_DONE);				                
						(void)ma_task_set_condition(task, MA_TASK_COND_HIDDEN);

						(void)ma_variant_create_from_string("1", strlen("1"), &setting_value);		
						(void)ma_task_set_setting(task, MA_SECTION_INSTALL_STR, MA_KEY_NUM_OF_INSTALLS_STR, setting_value);
						(void)ma_variant_release(setting_value); setting_value = NULL;

						MA_MSC_SELECT(_snprintf, snprintf)(install_key, 10, "%s_1", MA_SECTION_INSTALL_STR);													
						(void)ma_variant_create_from_string(MA_TELEMTRY_SOFTWARE_ID_STR, strlen(MA_TELEMTRY_SOFTWARE_ID_STR), &setting_value);		
						(void)ma_task_set_setting(task, MA_SECTION_INSTALL_STR, install_key, setting_value);
						(void)ma_variant_release(setting_value); setting_value = NULL;
					 
						(void)ma_variant_create_from_string(branch_name, strlen(branch_name), &setting_value);		
						(void)ma_task_set_setting(task, MA_TELEMTRY_DEP_TASK_SETTING_SECTION_STR, MA_TELEMTRY_DEP_TASK_SETTING_PKG_PATH_STR, setting_value);
						(void)ma_variant_release(setting_value); setting_value = NULL;

						if(MA_OK == (rc = ma_trigger_create_run_now(&run_now, &trigger))) {
							if(MA_OK == (rc = ma_task_add_trigger(task, trigger))) {
								if(MA_OK == (rc = ma_task_start(task))) {
									MA_LOG(logger, MA_LOG_SEV_INFO, "Product improvement program deployment task (%s) is started.", MA_TELEMTRY_DEP_TASK_ID_STR) ;
								}  							
							}						
							(void)ma_trigger_release(trigger);
						}  						
					}            
					else{
						MA_LOG(logger, MA_LOG_SEV_ERROR, "Product improvement program deployment task (id = %s) create failed (%d)", MA_TELEMTRY_DEP_TASK_ID_STR, rc) ;
					}
				}
				else if(MA_OK == rc ) {
					MA_LOG(logger, MA_LOG_SEV_DEBUG, "Product improvement program deployment task (id = %s) already created ", MA_TELEMTRY_DEP_TASK_ID_STR) ;														
					(void)ma_task_release(task);
				}
			}																
		}
	}			    
	return;
	#endif
}

static ma_error_t add_updater_task_settings(ma_task_t *self, ma_array_t *product_list) {
    if(self && product_list) {
        size_t l_number_of_updates = 0;
        ma_error_t rc = MA_OK;
		ma_variant_t *var = NULL;

        if(MA_OK == ma_variant_create_from_string("0", strlen("0"), &var)){
            ma_task_set_setting(self, MA_SECTION_UPDATE_OPTIONS_STR, MA_KEY_SHOW_UI_STR, var);
            (void)ma_variant_release(var);
        }

		if(MA_OK == (rc = ma_variant_create_from_string("1", strlen("1"), &var))) {
            (void)ma_task_set_setting(self, MA_SECTION_SELECTED_UPDATES_STR, MA_KEY_UPDATE_ALL_STR, var);
            ma_variant_release(var), var = NULL;
        }

        if(MA_OK == (rc = ma_array_size(product_list, &l_number_of_updates))) {

            if(l_number_of_updates > 0) {
                char s_number_of_updates[16] = {0};
                ma_variant_t *variant = NULL;

                MA_MSC_SELECT(_snprintf, snprintf)(s_number_of_updates, 16, "%d", l_number_of_updates);
                if(MA_OK == (rc = ma_variant_create_from_string(s_number_of_updates, strlen(s_number_of_updates), &variant))) {
                    int i = 0;
                    const char *update_all = "0";

                    (void)ma_task_set_setting(self, MA_SECTION_SELECTED_UPDATES_STR, MA_KEY_NUM_OF_UPDATES_STR, variant);
                    ma_variant_release(variant), variant = NULL;

                    for(i = l_number_of_updates; i > 0 ; i--) {
                        char selected_update_index[MA_MAX_LEN] = {0};
                        MA_MSC_SELECT(_snprintf, snprintf)(selected_update_index, MA_MAX_LEN -1, "%s_%d", MA_KEY_UPDATE_STR, i);

                        if(MA_OK == ma_array_get_element_at(product_list, i, &variant)) {
                            ma_buffer_t *buffer = NULL;
                            ma_task_set_setting(self, MA_SECTION_SELECTED_UPDATES_STR, selected_update_index, variant);
                            if(MA_OK == ma_variant_get_string_buffer(variant, &buffer)) {
                                const char *value = NULL;
                                size_t size = 0;
                                if(MA_OK == ma_buffer_get_string(buffer, &value, &size)) {
                                    char selected_update_section[MA_MAX_LEN] = {0};
                                    const char *update_enable = "1";
                                    ma_variant_t *update_enable_variant = NULL;
                                    if(MA_OK == ma_variant_create_from_string(update_enable, strlen(update_enable), &update_enable_variant)) {
                                        MA_MSC_SELECT(_snprintf, snprintf)(selected_update_section, MA_MAX_LEN -1, "%s\\%s", MA_SECTION_SELECTED_UPDATES_STR, value);
                                        ma_task_set_setting(self, selected_update_section, MA_KEY_UPDATE_STR, update_enable_variant);
                                        ma_variant_release(update_enable_variant);
                                    }
                                }
                                ma_buffer_release(buffer);
                            }
                            ma_variant_release(variant);
                        }
                    }
                    /* now set the update all to 0 */
                    if(MA_OK == (rc = ma_variant_create_from_string(update_all, strlen(update_all), &variant))) {
                        (void)ma_task_set_setting(self, MA_SECTION_SELECTED_UPDATES_STR, MA_KEY_UPDATE_ALL_STR, variant);
                        ma_variant_release(variant), variant = NULL;
                    }
                }
            }
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_task_schedule_global_update(ma_message_t *msg, struct ma_context_s *context){
	if(msg && context){
		ma_error_t rc = MA_OK;
		ma_task_t *task = NULL;
		ma_trigger_t *trigger = NULL;
		const char *product_list = NULL;
		const char *catalog_ver = NULL;
		const char *ge_catalog_ver = NULL;
		ma_array_t *arr_product_list = NULL;		
		ma_buffer_t *ge_catalog_ver_buff = NULL;

		(void)ma_message_get_property(msg, MA_UDP_MSG_KEY_GLOBAL_UPDATE_PRODUCT_LIST_STR, &product_list) ;
		(void)ma_message_get_property(msg, MA_UDP_MSG_KEY_GLOBAL_UPDATE_CATALOG_VERSION_STR, &catalog_ver) ;
		(void)ma_ds_get_str(ma_configurator_get_datastore(MA_CONTEXT_GET_CONFIGURATOR(context)), MA_UPDATER_DATA_PATH_NAME_STR, MA_UPDATER_KEY_LAST_GLOBAL_UPDATE_CATALOG_VERSION_STR, &ge_catalog_ver_buff);			       

		if(ge_catalog_ver_buff){
			size_t size = 0;
			ma_buffer_get_string(ge_catalog_ver_buff, &ge_catalog_ver, &size);
		}
		else
			ge_catalog_ver = "0";		

		MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_DEBUG, "Global updating settings ProductList (%s) CatalogVersion(%s) LastGlobalUpdateCatelogVersion(%s).", (product_list ? product_list : "0"), (catalog_ver ? catalog_ver : "0"), (ge_catalog_ver ? ge_catalog_ver: "0") );                            		

		if(product_list && catalog_ver && ge_catalog_ver && strcmp(catalog_ver, ge_catalog_ver) ){

			(void)ma_array_create(&arr_product_list);
				
			if(strcmp(product_list, "0")) {
				char *str = strdup(product_list);
				char *p = NULL ;

				p = strtok(str , "|") ;

				while(p != NULL){
					ma_variant_t *var = NULL;
					(void)ma_variant_create_from_string(p, strlen(p), &var);
					(void)ma_array_push(arr_product_list, var);
					(void)ma_variant_release(var);
					var = NULL;
					p = strtok(NULL, "|") ;
				}
				free(str);
			}

			if(MA_OK == (rc = ma_scheduler_create_task(MA_CONTEXT_GET_SCHEDULER(context), MA_SOFTWAREID_GENERAL_STR, MA_GLOBAL_UPDATING_TASK_ID_STR, MA_GLOBAL_UPDATING_TASK_NAME_STR, MA_UPDATER_TASK_TYPE_COMPAT_UPDATE_STR, &task))){ 	
				ma_trigger_run_now_policy_t run_now;
				ma_variant_t *var = NULL;
				run_now.now = MA_TRUE;		
				
				(void)ma_task_set_software_id(task, MA_SOFTWAREID_GENERAL_STR);				                
				(void)ma_task_set_condition(task, MA_TASK_COND_DELETE_WHEN_DONE);				                
				(void)ma_task_set_condition(task, MA_TASK_COND_HIDDEN);			

				if(MA_OK == ma_variant_create_from_string(catalog_ver, strlen(catalog_ver), &var)){					
					(void)ma_task_set_setting(task, MA_GLOBAL_UPDATE_TASK_SECTION_STR, MA_GLOBAL_UPDATE_CATALOG_VERSION_SETTINGS_STR, var);
					(void)ma_variant_release(var);
				}

				if(MA_OK == (rc = add_updater_task_settings(task, arr_product_list))){

					/*trigger run_now*/
					if(MA_OK == (rc = ma_trigger_create_run_now(&run_now, &trigger))){
						if(MA_OK == (rc = ma_task_add_trigger(task, trigger))){
							if(MA_OK == (rc = ma_task_start(task))){		
								(void)ma_ds_set_str(ma_configurator_get_datastore(MA_CONTEXT_GET_CONFIGURATOR(context)), MA_UPDATER_DATA_PATH_NAME_STR, MA_UPDATER_KEY_LAST_GLOBAL_UPDATE_CATALOG_VERSION_STR, catalog_ver, strlen(catalog_ver) );			       
								MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_INFO, "Scheduled the global update task");                            
							}
						}
						(void)ma_trigger_release(trigger);
					}
				}
				else
					MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_INFO, "Failed to add global update task settings %d", rc);                            

				if(MA_OK != rc){
					(void)ma_scheduler_release_task(MA_CONTEXT_GET_SCHEDULER(context), task);
					MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_ERROR, "Failed to schdule the global update task, error = <%d>", rc);                            
				}
			}
			else
				MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_DEBUG, "Not able to create the global update task,  error = <%d>", rc);                            		

			(void)ma_array_release(arr_product_list);
		}
		else
			MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_DEBUG, "Can't start the global updating for this request.");                            		

		(void)ma_buffer_release(ge_catalog_ver_buff);
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}




