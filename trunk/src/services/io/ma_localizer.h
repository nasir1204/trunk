#ifndef MA_LOCALIZER_H_INCLUDED
#define MA_LOCALIZER_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/context/ma_context.h"
#include "ma/ma_buffer.h"

MA_CPP(extern "C" {)

typedef struct ma_localizer_s ma_localizer_t, *ma_localizer_h;

ma_error_t ma_localizer_create(ma_context_t *context, ma_localizer_t **localizer);

ma_error_t ma_localizer_start(ma_localizer_t *localizer);

ma_error_t ma_localizer_stop(ma_localizer_t *localizer);

ma_error_t ma_localizer_get_localize_string(ma_localizer_t *localizer, const char *msg, ma_buffer_t **localized_buffer);

ma_error_t ma_localizer_get_language(ma_localizer_t *localizer, const char **lang);

ma_error_t ma_localizer_release(ma_localizer_t *localizer);

MA_CPP(})


#endif /* MA_LOCALIZER_H_INCLUDED */

