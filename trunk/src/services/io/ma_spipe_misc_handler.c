#include "ma_spipe_misc_handler.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/defs/ma_compat_defs.h"
#include "ma/internal/defs/ma_general_defs.h"
#include "ma/ma_log.h"
#include "ma/datastore/ma_ds_registry.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "ioservice"


struct ma_spipe_misc_handler_s{
	/*ma context*/
	ma_context_t            *context;
		
	/*default value is false, its true if uninstall package sent*/
	ma_bool_t uninstall_sent;

	/*common decorator*/
	ma_spipe_decorator_t	*common_decorator;

	/*common handler*/
	ma_spipe_handler_t		*common_handler;
};

/* common spipe decorator*/
static ma_error_t misc_spipe_decorator_decorate(ma_spipe_decorator_t *decorator, ma_spipe_priority_t, ma_spipe_package_t *pkg);
static ma_error_t misc_spipe_decorator_get_spipe_priority(ma_spipe_decorator_t *decorator, ma_spipe_priority_t *priority);
static ma_error_t misc_spipe_decorator_destroy(ma_spipe_decorator_t *decorator);

static struct ma_spipe_decorator_methods_s decorator_methods = {
	&misc_spipe_decorator_decorate,
	&misc_spipe_decorator_get_spipe_priority,
	&misc_spipe_decorator_destroy
};

ma_error_t misc_spipe_decorator_decorate(ma_spipe_decorator_t *decorator, ma_spipe_priority_t priority, ma_spipe_package_t *pkg){
	const char *value = NULL ;
    size_t val_size = 0 ;
    ma_error_t rc = MA_OK ;
	ma_spipe_misc_handler_t *misc_spipe_hdl = (ma_spipe_misc_handler_t*)(decorator->data);
    
    ma_spipe_package_get_info(pkg,  STR_PACKAGE_TYPE, (const unsigned char **)&value, &val_size) ;
	if(value){
		MA_LOG(MA_CONTEXT_GET_LOGGER(misc_spipe_hdl->context), MA_LOG_SEV_DEBUG, "misc spipe decorator for package type %s", value) ;
		if(0 == strcmp(value, STR_PKGTYPE_AGENT_UNINSTALL_RESPONSE)){
			misc_spipe_hdl->uninstall_sent = MA_TRUE;
		}
	}
	return rc;
}


ma_error_t misc_spipe_decorator_get_spipe_priority(ma_spipe_decorator_t *decorator, ma_spipe_priority_t *priority){
	*priority = MA_SPIPE_PRIORITY_NONE;
	return MA_OK;
}

ma_error_t misc_spipe_decorator_destroy(ma_spipe_decorator_t *decorator){
	free(decorator);
	return MA_OK;
}

static ma_error_t misc_spipe_handler_on_spipe_response(ma_spipe_handler_t *handler, const ma_spipe_response_t *spipe_response);
static ma_error_t misc_spipe_handler_destroy(ma_spipe_handler_t *handler);

static struct ma_spipe_handler_methods_s handler_methods = {
	&misc_spipe_handler_on_spipe_response,
	&misc_spipe_handler_destroy	
};

static ma_error_t uninstall_agent(ma_logger_t *logger) {
	#ifdef MA_WINDOWS
		#define MA_UNINSTALL_COMMAND            "Uninstall Command"
		#define MA_FRAMEINST_RUN_BY             "RunBy"
		#define MA_FRAMEINST_EPO_CONSOLE        "EpoConsole"

		ma_error_t		rc = MA_OK;
		char			unistall_cmd[1024] = {0};

		ma_buffer_t *buffer = NULL;
		ma_ds_t *ds = NULL;

		if(MA_OK != (rc = ma_ds_registry_open(HKEY_LOCAL_MACHINE, SZ_COMPAT_AGENT_UNINSTALL_CMD_REG_KEY,  MA_DS_REGISTRY_32BIT_VIEW, MA_FALSE, KEY_ALL_ACCESS, (ma_ds_registry_t **) &ds))) {
			rc = ma_ds_registry_open(HKEY_LOCAL_MACHINE, SZ_COMPAT_AGENT_UNINSTALL_CMD_REG_KEY,  MA_FALSE, MA_FALSE, KEY_ALL_ACCESS, (ma_ds_registry_t **) &ds);	
		}		

		if(MA_OK == rc && ds) {
			if(MA_OK == (rc = ma_ds_get_str(ds, MA_SOFTWAREID_GENERAL_STR, MA_UNINSTALL_COMMAND, &buffer))) {
				size_t size = 0;
				const char *path = NULL;
				if(MA_OK == ma_buffer_get_string(buffer, &path, &size)) {
					strncpy(unistall_cmd, path, 1023);
				}
				ma_buffer_release(buffer);
			}
			else{
				MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to get uninstall command from registry %s", SZ_COMPAT_AGENT_UNINSTALL_CMD_REG_KEY) ;
			}
			ma_ds_release(ds);
		}
		else{
			MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to open agent registry %s", SZ_COMPAT_AGENT_UNINSTALL_CMD_REG_KEY) ;
		}

		if(rc == MA_OK) {
			STARTUPINFOA si;
			PROCESS_INFORMATION pi;
			char final_unistall_cmd[1024] = {0};
			si.cb = sizeof(si);
			ZeroMemory(&si, sizeof(si));
			ZeroMemory(&pi, sizeof(pi));
			/*strcpy(unistall_cmd, "\"C:\\Program Files (x86)\\McAfee\\Agent\\frminst.exe\" /Remove=Agent /Silent");*/
			

			_snprintf((char*)final_unistall_cmd, 1023, "%s%s%s%c%s", unistall_cmd, " /", MA_FRAMEINST_RUN_BY, '=', MA_FRAMEINST_EPO_CONSOLE);

			MA_LOG(logger, MA_LOG_SEV_DEBUG, "Running agent uninstall command %s.", final_unistall_cmd) ;

			if(CreateProcessA(NULL,(LPSTR)final_unistall_cmd, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi)) {
				WaitForInputIdle(pi.hProcess, 1000);
				CloseHandle(pi.hThread);
				CloseHandle(pi.hProcess);
				rc= MA_OK;
			}			
			else{
				MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to run agent uninstall command %s.", final_unistall_cmd) ;
				rc= MA_ERROR_APIFAILED;
			}
		}
		return rc;
	#else
		return MA_ERROR_APIFAILED;
	#endif
}
ma_error_t misc_spipe_handler_on_spipe_response(ma_spipe_handler_t *handler, const ma_spipe_response_t *spipe_response){
	ma_error_t rc = MA_OK;
	const char *value = NULL ;
    size_t val_size = 0 ;
	ma_spipe_misc_handler_t *misc_spipe_hdl = (ma_spipe_misc_handler_t*)(handler->data);    
	ma_spipe_package_get_info(spipe_response->respond_spipe_pkg,  STR_PACKAGE_TYPE, (const unsigned char **)&value, &val_size) ;
    
	if(value && 0 == strcmp(value, STR_PKGTYPE_AGENT_UNINSTALL)){
		MA_LOG(MA_CONTEXT_GET_LOGGER(misc_spipe_hdl->context), MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "Agent received AGENT UNINSTALL package from ePO server");
		rc = uninstall_agent(MA_CONTEXT_GET_LOGGER(misc_spipe_hdl->context));
	}

	return rc;
}

ma_error_t misc_spipe_handler_destroy(ma_spipe_handler_t *handler){
	free(handler);
	return MA_OK;
}

ma_error_t ma_spipe_misc_handler_create(ma_context_t *context, ma_spipe_misc_handler_t **misc_spipe_hdl){
	if(context && misc_spipe_hdl){
		ma_spipe_misc_handler_t *self = (ma_spipe_misc_handler_t*) calloc(1, sizeof(ma_spipe_misc_handler_t));
		if(self){
			self->common_decorator = (ma_spipe_decorator_t*) calloc(1, sizeof(ma_spipe_decorator_t));
			if(self->common_decorator){
				self->common_handler = (ma_spipe_handler_t*) calloc(1, sizeof(ma_spipe_handler_t));
				if(self->common_handler){
					ma_spipe_decorator_init(self->common_decorator, &decorator_methods, self);
					ma_spipe_handler_init(self->common_handler, &handler_methods, self);
					self->context = context;
					*misc_spipe_hdl = self;
					return MA_OK;
				}
			}
		}
		ma_spipe_misc_handler_release(self);
		return MA_ERROR_OUTOFMEMORY;
	}
	return MA_ERROR_INVALIDARG;
}

ma_spipe_decorator_t *ma_spipe_misc_handler_get_decorator(ma_spipe_misc_handler_t *self){
	return self ? self->common_decorator : NULL;	
}

ma_spipe_handler_t *ma_spipe_misc_handler_get_handler(ma_spipe_misc_handler_t *self){
	return self ? self->common_handler : NULL;
}

ma_error_t ma_spipe_misc_handler_release(ma_spipe_misc_handler_t *self){
	if(self){
		if(self->common_decorator)
			ma_spipe_decorator_release(self->common_decorator);
		if(self->common_handler)
			ma_spipe_handler_release(self->common_handler);
		free(self);
	}
	return MA_ERROR_INVALIDARG;
}

