#ifndef MA_SPIPE_MISC_HANDLER_H_INCLUDED
#define MA_SPIPE_MISC_HANDLER_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/services/ahclient/ma_ah_client_service.h"
#include "ma/internal/utils/context/ma_context.h"

MA_CPP(extern "C" {)

typedef struct ma_spipe_misc_handler_s ma_spipe_misc_handler_t, *ma_spipe_misc_handler_h;

ma_error_t ma_spipe_misc_handler_create(ma_context_t *context, ma_spipe_misc_handler_t **self);

ma_spipe_decorator_t *ma_spipe_misc_handler_get_decorator(ma_spipe_misc_handler_t *self);

ma_spipe_handler_t *ma_spipe_misc_handler_get_handler(ma_spipe_misc_handler_t *self);

ma_error_t ma_spipe_misc_handler_release(ma_spipe_misc_handler_t *self);

MA_CPP(})

#endif

