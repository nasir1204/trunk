#ifndef MA_LOGGER_SERVICE_H_INCLUDED
#define MA_LOGGER_SERVICE_H_INCLUDED

#include "ma/ma_common.h"

MA_CPP(extern "C" {)

struct ma_service_s;

ma_error_t ma_logger_service_create(char const *service_name, struct ma_service_s **service);

MA_CPP(})

#endif /* MA_LOGGER_SERVICE_H_INCLUDED */
