#include "ma/ma_datastore.h"
#include "ma/ma_variant.h"
#include "ma/internal/ma_macros.h"

#include "ma_spipe_common_property_manager.h"
#include "ma/internal/utils/spipe/ma_spipe_package_defs.h"
#include "ma/internal/utils/platform/ma_system_property.h"

#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include "ma/internal/utils/uuid/ma_uuid.h"
#include "ma/internal/defs/ma_general_defs.h"

#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/defs/ma_policy_defs.h"
#include "ma/internal/defs/ma_io_defs.h"

#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/utils/configurator/ma_configurator_utils.h"

#include "ma/ma_log.h"

#include <stdlib.h>
#include <stdio.h>

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "ioservice"


struct ma_spipe_common_property_manager_s{
	/*ma context*/
	ma_context_t            *context;
		
	/*common decorator*/
	ma_spipe_decorator_t	*common_decorator;

	/*common handler*/
	ma_spipe_handler_t		*common_handler;
};

/* common spipe decorator*/
static ma_error_t common_spipe_decorator_decorate(ma_spipe_decorator_t *decorator, ma_spipe_priority_t, ma_spipe_package_t *pkg);
static ma_error_t common_spipe_decorator_get_spipe_priority(ma_spipe_decorator_t *decorator, ma_spipe_priority_t *priority);
static ma_error_t common_spipe_decorator_destroy(ma_spipe_decorator_t *decorator);

static struct ma_spipe_decorator_methods_s decorator_methods = {
	&common_spipe_decorator_decorate,
	&common_spipe_decorator_get_spipe_priority,
	&common_spipe_decorator_destroy
};

ma_error_t get_setting_from_ds(ma_spipe_common_property_manager_t *self, const char *path, const char *key, ma_vartype_t type, ma_temp_buffer_t *buffer){
	union{
	ma_int64_t int_value;
	ma_buffer_t *str;
	}local_buffer = {0};
	ma_error_t rc = MA_OK;	
    ma_configurator_t *configurator = MA_CONTEXT_GET_CONFIGURATOR(self->context);
    ma_ds_t *ds = ma_configurator_get_datastore(configurator);

	switch(type){
		case MA_VARTYPE_INT64:
		{
			rc = ma_ds_get_int64(ds, path, key, &local_buffer.int_value);
			if(MA_OK == rc){
				ma_temp_buffer_reserve(buffer, 22);						
				MA_MSC_SELECT(_snprintf, snprintf)((char*)ma_temp_buffer_get(buffer), ma_temp_buffer_capacity(buffer)-1, "%ld", local_buffer.int_value);
			}
		}
		break;
		case MA_VARTYPE_STRING:
		{
			rc = ma_ds_get_str(ds, path, key, &local_buffer.str);
			if(MA_OK == rc){				
				const char *value = NULL;
				size_t size = 0;
				(void)ma_buffer_get_string(local_buffer.str, &value, &size);
				ma_temp_buffer_reserve(buffer, size+1);
				if(value && size)					
					strcpy((char*)ma_temp_buffer_get(buffer), value);				
			}			
		}
		break;
	}	
	(void)ma_buffer_release(local_buffer.str);
	return rc;
}

static ma_error_t create_guid(ma_spipe_common_property_manager_t *self, ma_temp_buffer_t *mac_buffer, ma_temp_buffer_t *buffer){	
	ma_error_t rc = MA_OK;	
	ma_temp_buffer_reserve(buffer, UUID_LEN_TXT+1);		
	rc = ma_uuid_generate(1, (unsigned char*)ma_temp_buffer_get(mac_buffer), (char*)ma_temp_buffer_get(buffer));
	if(MA_OK != rc)		
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to create the guid, error code = %d", rc);		
	return rc;
}

static ma_error_t get_next_sequence_number(ma_spipe_common_property_manager_t *self, ma_temp_buffer_t *buffer){
	ma_error_t rc = MA_OK;
	ma_int64_t seq_no = 0;
	ma_configurator_t *configurator = MA_CONTEXT_GET_CONFIGURATOR(self->context);
    ma_ds_t *ds = ma_configurator_get_datastore(configurator);

	/*Get the sequence no.*/
	rc = ma_ds_get_int64(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_SPIPE_SEQUENCE_NUMBER_STR, &seq_no);
	if(MA_OK != rc){ /*If not found then insert 1.*/		
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_WARNING, "resetting sequence number, %d", rc);		
		rc = ma_ds_set_int(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_SPIPE_SEQUENCE_NUMBER_STR, 1);
		if(MA_OK != rc)	
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to set the first sequence number, error code = %d", rc);		
	}
	if(MA_OK == rc){ /*If found or inserted in ds, then set to temp buffer.*/
		ma_temp_buffer_reserve(buffer, 22);	
		MA_MSC_SELECT(_snprintf, snprintf)((char*)ma_temp_buffer_get(buffer), ma_temp_buffer_capacity(buffer)-1, "%ld", seq_no);

		/*Increment the seq number.*/
		if(MA_OK != (rc = ma_ds_set_int(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_SPIPE_SEQUENCE_NUMBER_STR, seq_no+1)))
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to set the next sequence number, error code = %d", rc);		
	}
	return rc;
}

static ma_error_t get_computer_name(ma_spipe_common_property_manager_t *self, ma_temp_buffer_t *buffer){	
	const ma_misc_system_property_t *sys_prop = ma_system_property_misc_get(MA_CONTEXT_GET_SYSTEM_PROPERTY(self->context), MA_FALSE);
	ma_temp_buffer_reserve(buffer, strlen(sys_prop->computername)+1);
	strcpy((char*)ma_temp_buffer_get(buffer), sys_prop->computername);	
	return MA_OK;
}


ma_error_t common_spipe_decorator_decorate(ma_spipe_decorator_t *decorator, ma_spipe_priority_t priority, ma_spipe_package_t *pkg){
	ma_temp_buffer_t agent_guid = MA_TEMP_BUFFER_INIT , mac_address = MA_TEMP_BUFFER_INIT, pkg_guid = MA_TEMP_BUFFER_INIT;
	ma_temp_buffer_t computer_name = MA_TEMP_BUFFER_INIT , seq_number = MA_TEMP_BUFFER_INIT, agent_version = MA_TEMP_BUFFER_INIT;
	ma_error_t rc = MA_OK;
	const ma_network_system_property_t *net ;
	ma_spipe_common_property_manager_t *cmn_prop_mgr = (ma_spipe_common_property_manager_t*)(decorator->data);
	
	if(MA_OK == (rc = get_setting_from_ds(cmn_prop_mgr, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_GUID_STR, MA_VARTYPE_STRING, &agent_guid))){
		
		ma_spipe_package_set_header_property(pkg, MA_SPIPE_PROPERTY_SENDER_GUID, (char*)ma_temp_buffer_get(&agent_guid) );			
		if(net = ma_system_property_network_get(MA_CONTEXT_GET_SYSTEM_PROPERTY(cmn_prop_mgr->context), MA_FALSE)){
            memcpy(ma_temp_buffer_get(&mac_address), net->mac_address, MA_MAC_ADDRESS_SIZE);
			if(MA_OK == (rc = create_guid(cmn_prop_mgr, &mac_address, &pkg_guid))){

				(void)ma_spipe_package_set_header_property(pkg, MA_SPIPE_PROPERTY_PACKAGE_GUID, (char*)ma_temp_buffer_get(&pkg_guid));    
				(void)ma_spipe_package_set_package_type(pkg, MA_SPIPE_PACKAGE_TYPE_DATA);

				if(MA_OK == (rc = get_computer_name(cmn_prop_mgr, &computer_name))){
					const char *tenantid_ =  ma_configurator_get_tenantid(MA_CONTEXT_GET_CONFIGURATOR(cmn_prop_mgr->context));
					unsigned short vdi_mode =  ma_configurator_get_vdimode(MA_CONTEXT_GET_CONFIGURATOR(cmn_prop_mgr->context));
					const ma_misc_system_property_t *sys_prop = ma_system_property_misc_get(MA_CONTEXT_GET_SYSTEM_PROPERTY(cmn_prop_mgr->context), MA_FALSE);

					(void)ma_spipe_package_set_header_property(pkg, MA_SPIPE_PROPERTY_COMPUTER_NAME, (char*)ma_temp_buffer_get(&computer_name));
									
					(void)ma_spipe_package_add_info(pkg, STR_DOMAIN_NAME, (unsigned char*)sys_prop->domainname, strlen(sys_prop->domainname));
					(void)ma_spipe_package_add_info(pkg, STR_COMPUTER_NAME, (unsigned char*)ma_temp_buffer_get(&computer_name), strlen(((char*)ma_temp_buffer_get(&computer_name))));    						
  				
  					if(tenantid_)
  						ma_spipe_package_add_info(pkg, STR_TENANT_ID, (unsigned char*)tenantid_, strlen(((char*)tenantid_)));    

					(void)ma_spipe_package_add_info(pkg, STR_PLATFORM_ID, (unsigned char *)ma_system_property_os_get(MA_CONTEXT_GET_SYSTEM_PROPERTY(cmn_prop_mgr->context), MA_FALSE)->platformid, strlen(ma_system_property_os_get(MA_CONTEXT_GET_SYSTEM_PROPERTY(cmn_prop_mgr->context), MA_FALSE)->platformid));					
					(void)ma_spipe_package_add_info(pkg, STR_GUID_GEN_SUPPORTED, "1", strlen("1"));
					(void)ma_spipe_package_add_info(pkg, STR_SUPPORTED_SPIPE_VERSION, "6.0", strlen("6.0"));
					(void)ma_spipe_package_add_info(pkg, STR_SUPPORT_HIGHER_KEYS, "1", strlen("1"));
					(void)ma_spipe_package_add_info(pkg, STR_VDI, (vdi_mode ? "1": "0"), 1);

					/*add mac address*/
					(void)ma_spipe_package_add_info(pkg, STR_NET_ADDRESS, (unsigned char*)net->mac_address_formated, strlen(net->mac_address_formated));    
				
					if(MA_OK == (rc = get_next_sequence_number(cmn_prop_mgr, &seq_number))){
						(void)ma_spipe_package_add_info(pkg, STR_SEQUENCE_NO, (unsigned char*)ma_temp_buffer_get(&seq_number), strlen(((char*)ma_temp_buffer_get(&seq_number))));    
					
						if(MA_OK == (rc = get_setting_from_ds(cmn_prop_mgr, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_VERSION_STR, MA_VARTYPE_STRING, &agent_version))){
							(void)ma_spipe_package_add_info(pkg, STR_AGENT_VERSION, (unsigned char*)ma_temp_buffer_get(&agent_version), strlen(((char*)ma_temp_buffer_get(&agent_version))));    				
							(void)ma_spipe_package_add_info(pkg, STR_FQDN_NAME, (unsigned char*)net->fqdn, strlen(net->fqdn));    
							if(strlen(net->spipe_src_ip_address_formatted)){
								(void)ma_spipe_package_add_info(pkg, STR_IP_ADDRESS, (unsigned char*)net->spipe_src_ip_address_formatted, strlen(net->spipe_src_ip_address_formatted));    
							}
						}
					}
				}
			}				
		}
	}
	if(MA_OK != rc)
		MA_LOG(MA_CONTEXT_GET_LOGGER(cmn_prop_mgr->context), MA_LOG_SEV_ERROR, "Failed to get common property, last error = %d", rc);
	ma_temp_buffer_uninit(&agent_guid); ma_temp_buffer_uninit(&mac_address); ma_temp_buffer_uninit(&pkg_guid);
	ma_temp_buffer_uninit(&computer_name); ma_temp_buffer_uninit(&seq_number); ma_temp_buffer_uninit(&agent_version);
	return rc;
}


ma_error_t common_spipe_decorator_get_spipe_priority(ma_spipe_decorator_t *decorator, ma_spipe_priority_t *priority){
	*priority = MA_SPIPE_PRIORITY_NONE;
	return MA_OK;
}

ma_error_t common_spipe_decorator_destroy(ma_spipe_decorator_t *decorator){
	free(decorator);
	return MA_OK;
}

/*Common spipe handler*/
static ma_error_t common_spipe_handler_on_spipe_response(ma_spipe_handler_t *handler, const ma_spipe_response_t *spipe_response);
static ma_error_t common_spipe_handler_destroy(ma_spipe_handler_t *handler);

static struct ma_spipe_handler_methods_s handler_methods = {
	&common_spipe_handler_on_spipe_response,
	&common_spipe_handler_destroy	
};

static ma_error_t enforce_agent_policies(ma_spipe_common_property_manager_t *self) {
	ma_error_t rc = MA_OK;
	ma_msgbus_endpoint_t *endpoint = NULL;
	ma_message_t *request = NULL;	
	
	MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO, "Enforcing policy as agent guid changed.");
	if(MA_OK == (rc = ma_message_create(&request))) {
        if(MA_OK == (rc = ma_message_set_property(request, MA_IO_SERVICE_MSG_TYPE, MA_IO_MSG_TYPE_RECONFIGURE_SERVICES_STR))) {
            if(MA_OK == (rc = ma_msgbus_endpoint_create(MA_CONTEXT_GET_MSGBUS(self->context), MA_IO_SERVICE_NAME_STR, NULL, &endpoint))) {
				(void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
			    if(MA_OK == (rc = ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_INPROC))) {
                        
				    if(MA_OK == (rc = ma_msgbus_send_and_forget(endpoint, request))) 
                        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Policy enforcement initiated as agent guid changed.");								                                                
					else
                        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Policy enforcement initiate failed, %d.", rc);
                }
                (void)ma_msgbus_endpoint_release(endpoint);
            }            
        }
        (void)ma_message_release(request);
    }
	return rc;
}

static void regenerate_agent_keys(ma_spipe_common_property_manager_t *self) {

}

ma_error_t common_spipe_handler_on_spipe_response(ma_spipe_handler_t *handler, const ma_spipe_response_t *spipe_response){
	ma_error_t rc = MA_OK;
	ma_spipe_common_property_manager_t *self = (ma_spipe_common_property_manager_t*)(handler->data);

	if(IS_SPIPE_SEND_SUCCEEDED(spipe_response->http_response_code)){
		
		size_t size = 0;
		const char *agent_guid = NULL;

		if(MA_OK == ma_spipe_package_get_info(spipe_response->respond_spipe_pkg, STR_PKGINFO_AGENT_GUID, (const unsigned char**)&agent_guid, &size)){

			if(agent_guid && size){
				ma_configurator_t *configurator = MA_CONTEXT_GET_CONFIGURATOR(self->context);
				ma_ds_t *ds = ma_configurator_get_datastore(configurator);
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO, "ePO has sent agent guid %s, using this guid for further use", agent_guid);
				if(MA_OK == (rc = ma_ds_set_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_GUID_STR, agent_guid, -1))){
					(void)ma_configurator_set_guid(configurator, (char*)agent_guid);
					(void)enforce_agent_policies(self);
					regenerate_agent_keys(self);
				}
			}
		}
	}
	return rc;
}

ma_error_t common_spipe_handler_destroy(ma_spipe_handler_t *handler){
	free(handler);
	return MA_OK;
}

ma_error_t ma_spipe_common_property_manager_create(ma_context_t *context, ma_spipe_common_property_manager_t **cmn_prop_mgr){
	if(context && cmn_prop_mgr){
		ma_spipe_common_property_manager_t *self = (ma_spipe_common_property_manager_t*) calloc(1, sizeof(ma_spipe_common_property_manager_t));
		if(self){
			self->common_decorator = (ma_spipe_decorator_t*) calloc(1, sizeof(ma_spipe_decorator_t));
			if(self->common_decorator){
				self->common_handler = (ma_spipe_handler_t*) calloc(1, sizeof(ma_spipe_handler_t));
				if(self->common_handler){
					ma_spipe_decorator_init(self->common_decorator, &decorator_methods, self);
					ma_spipe_handler_init(self->common_handler, &handler_methods, self);
					self->context = context;
					*cmn_prop_mgr = self;
					return MA_OK;
				}
			}
		}
		ma_spipe_common_property_manager_release(self);
		return MA_ERROR_OUTOFMEMORY;
	}
	return MA_ERROR_INVALIDARG;
}

ma_spipe_decorator_t *ma_spipe_common_property_manager_get_decorator(ma_spipe_common_property_manager_t *self){
	return self ? self->common_decorator : NULL;	
}

ma_spipe_handler_t *ma_spipe_common_property_manager_get_handler(ma_spipe_common_property_manager_t *self){
	return self ? self->common_handler : NULL;
}

ma_error_t ma_spipe_common_property_manager_release(ma_spipe_common_property_manager_t *self){
	if(self){
		if(self->common_decorator)
			ma_spipe_decorator_release(self->common_decorator);
		if(self->common_handler)
			ma_spipe_handler_release(self->common_handler);
		free(self);
	}
	return MA_ERROR_INVALIDARG;
}

