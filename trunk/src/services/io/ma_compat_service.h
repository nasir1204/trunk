#ifndef MA_COMPAT_SERVICE_H_INCLUDED
#define MA_COMPAT_SERVICE_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/services/ma_service.h"

MA_CPP(extern "C" {)
	
typedef struct ma_compat_service_s ma_compat_service_t, *ma_compat_service_h;

MA_COMPAT_SERVICE_API ma_error_t ma_compat_service_create(char const *service_name, ma_service_t **compat_service);

MA_CPP(})

#endif /*MA_COMPAT_SERVICE_H_INCLUDED*/
