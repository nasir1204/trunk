#include "ma/internal/services/io/ma_task_service.h"
#include "ma/internal/services/scheduler/ma_scheduler_core.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"
#include "ma/internal/services/scheduler/ma_task_internal.h"
#include "ma/internal/services/scheduler/ma_triggers_internal.h"
#include "ma_epo_task_defs.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/services/policy/ma_task_db.h"
#include "ma/internal/services/scheduler/ma_task_internal.h"

#include <stdlib.h>

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "io.service"

/* This function is using the get_settings() funtion which is defined in ma_scheduler_utils.c */
static ma_error_t get_task_setting_value_str(ma_variant_t *task_data_var, const char *section, const char *key, const char **value) {
    if(task_data_var && section && key && value) {
        ma_error_t rc = MA_OK;
        ma_variant_t *value_var = NULL;
        if(MA_OK == (rc = get_settings(task_data_var, section, key, &value_var))) { 
            ma_buffer_t *buffer = NULL;
            if(MA_OK == (rc = ma_variant_get_string_buffer(value_var, &buffer))) {
                const char *p = NULL; size_t size = 0;
                if(MA_OK == (rc = ma_buffer_get_string(buffer, &p, &size))) {
                    *value = (p) ? p : NULL;
                }
                (void)ma_buffer_release(buffer);
            }
            (void)ma_variant_release(value_var);
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t apply_settings_to_epo_task(ma_context_t *context, ma_task_t *task, ma_variant_t *task_data_var) {
    /* Apply the settings to task as per settings in schedule and settings sections */
    ma_error_t rc = MA_ERROR_EPO_TASK_SCHEDULE_FAILED;
    const char * value = NULL;
    ma_trigger_t *trigger = NULL;
    ma_bool_t is_start_stop_date_needed = MA_TRUE;
    const char *task_id = NULL;
    ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(context);
    ma_task_time_t start_date = {0};

    /* TBD - will handle schedule api return types */

    (void)ma_task_get_id(task, &task_id);
    if(MA_OK == (rc = get_task_setting_value_str(task_data_var, MA_TASK_SCHEDULE_SECTION_NAME_STR, MA_TASK_SCHEDULE_TYPE_STR, &value))) {
        ma_int32_t schedule_type = (value) ? atoi(value) : -1;       

        MA_LOG(logger, MA_LOG_SEV_DEBUG, "Task schedule type <%d>", schedule_type);

        /* TBD - Error handling */
        switch(schedule_type) {
            case MA_TRIGGER_DAILY:
                {
                    ma_trigger_daily_policy_t daily_policy = {0};
                    const char *repeat_days = NULL;
                    if((MA_OK == (rc = get_task_setting_value_str(task_data_var, MA_TASK_SCHEDULE_SECTION_NAME_STR, MA_TASK_SCHEDULE_REPEATDAYS_STR, &repeat_days))) && repeat_days) {
                        daily_policy.days_interval = atoi(repeat_days);
                        MA_LOG(logger, MA_LOG_SEV_DEBUG, "Daily trigger days interval <%d>", daily_policy.days_interval);
                    }

                    if((MA_OK == rc) && (MA_OK == (rc = ma_trigger_create_daily(&daily_policy, &trigger)))) {
                        rc = ma_task_add_trigger(task, trigger);
                        (void)ma_trigger_release(trigger);
                    }
                    break;
                }
            case MA_TRIGGER_WEEKLY:
                {
                    ma_trigger_weekly_policy_t weekly_policy = {0};
                    const char *repeat_weeks = NULL, *days_of_week = NULL;
                    if((MA_OK == (rc = get_task_setting_value_str(task_data_var, MA_TASK_SCHEDULE_SECTION_NAME_STR, MA_TASK_SCHEDULE_REPEATWEEKS_STR, &repeat_weeks))) && repeat_weeks)
                        weekly_policy.weeks_interval = atoi(repeat_weeks);

                    if((MA_OK == rc) && (MA_OK == (rc = get_task_setting_value_str(task_data_var, MA_TASK_SCHEDULE_SECTION_NAME_STR, MA_TASK_SCHEDULE_MASKDAYSOFWEEK_STR, &days_of_week))) && days_of_week)
                        weekly_policy.days_of_the_week = atoi(days_of_week);

                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "Weekly trigger week iterval <%d> day of the week <%d>", weekly_policy.weeks_interval, weekly_policy.days_of_the_week);

                    if((MA_OK == rc) && MA_OK == (rc = ma_trigger_create_weekly(&weekly_policy, &trigger))) {
                        rc = ma_task_add_trigger(task, trigger);
                        (void)ma_trigger_release(trigger);
                    }
                    break;
                }
            case MA_TRIGGER_MONTHLY_DATE:
                {                            
                    const char *month_option = NULL;

                    if((MA_OK == (rc = get_task_setting_value_str(task_data_var, MA_TASK_SCHEDULE_SECTION_NAME_STR, MA_TASK_SCHEDULE_MONTHOPTION_STR, &month_option))) && month_option) {
                        const char *day_num_of_month = NULL, *day_of_week = NULL, *week_num_of_month = NULL, *mask_months_of_year = NULL;
                        ma_trigger_monthly_date_policy_t monthly_policy = {0};
                        ma_trigger_monthly_dow_policy_t monthly_dow_policy = {0} ; 

                        memset(&monthly_dow_policy, 0, sizeof(ma_trigger_monthly_dow_policy_t));

                        if(atoi(month_option)) {  /* DayOfWeek */
                            if((MA_OK == (rc = get_task_setting_value_str(task_data_var, MA_TASK_SCHEDULE_SECTION_NAME_STR, MA_TASK_SCHEDULE_DAYOFWEEK_STR, &day_of_week))) && day_of_week)
                                monthly_dow_policy.day_of_the_week = 1 << (atoi(day_of_week) - 1);
                        
                            if((MA_OK == rc) && MA_OK == (rc = get_task_setting_value_str(task_data_var, MA_TASK_SCHEDULE_SECTION_NAME_STR, MA_TASK_SCHEDULE_WEEKNUMOFMONTH_STR, &week_num_of_month)))
                                monthly_dow_policy.which_week = atoi(week_num_of_month);

                            if((MA_OK == rc) && (MA_OK == (rc = get_task_setting_value_str(task_data_var, MA_TASK_SCHEDULE_SECTION_NAME_STR, MA_TASK_SCHEDULE_MASKMONTHSOFYEAR_STR, &mask_months_of_year))) && mask_months_of_year)
                                monthly_dow_policy.months = atoi(mask_months_of_year);

                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "Monthly DOW trigger days of week <%d> week num of month <%d> mask of months <%d>", monthly_dow_policy.day_of_the_week, monthly_dow_policy.which_week, monthly_dow_policy.months);

                             if((MA_OK == rc) && (MA_OK == (rc = ma_trigger_create_monthly_dow(&monthly_dow_policy, &trigger)))) {
                                rc = ma_task_add_trigger(task, trigger);
                                (void)ma_trigger_release(trigger);
                             }
                        }
                        else { /* DayNumOfMonth */
                            if((MA_OK == (rc = get_task_setting_value_str(task_data_var, MA_TASK_SCHEDULE_SECTION_NAME_STR, MA_TASK_SCHEDULE_DAYNUMOFMONTH_STR, &day_num_of_month))) && day_num_of_month)
                                monthly_policy.date = atoi(day_num_of_month);  

                            if((MA_OK == rc) && (MA_OK == (rc = get_task_setting_value_str(task_data_var, MA_TASK_SCHEDULE_SECTION_NAME_STR, MA_TASK_SCHEDULE_MASKMONTHSOFYEAR_STR, &mask_months_of_year))) && mask_months_of_year)
                                monthly_policy.months = atoi(mask_months_of_year);

                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "Monthly Date trigger day <%d> month <%d>", monthly_policy.date, monthly_dow_policy.months);

                            if((MA_OK == rc) && (MA_OK == (rc = ma_trigger_create_monthly_date(&monthly_policy, &trigger)))) {
                                rc = ma_task_add_trigger(task, trigger);
                                (void)ma_trigger_release(trigger);
                            }
                        }
                    }

                    break;
                }
            case MA_TRIGGER_ONCE:
                {
                    ma_trigger_run_once_policy_t run_once_policy = {0};
                
                    run_once_policy.once = MA_TRUE;  /* TBD - Sets true to run once, ePO is not sending any data for this type, Why do we need this ??*/

                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "Once trigger");

                    if(MA_OK == (rc = ma_trigger_create_run_once(&run_once_policy, &trigger))) {
                        rc = ma_task_add_trigger(task, trigger);
                        (void)ma_trigger_release(trigger);
                    }                                
                    break;
                }
            case MA_TRIGGER_AT_SYSTEM_START:
                {
                    ma_trigger_systemstart_policy_t sys_start_policy = {0};
                    const char *delay_time = NULL, *once_a_day = NULL;
                    ma_task_delay_policy_t delay = {0};
                
                    if((MA_OK == (rc = get_task_setting_value_str(task_data_var, MA_TASK_SCHEDULE_SECTION_NAME_STR, MA_TASK_SCHEDULE_ENABLEONCEADAY_STR, &once_a_day)) && once_a_day)) {
                        sys_start_policy.daily_once = atoi(once_a_day);
                    }

                    if((MA_OK == rc) && (MA_OK == (rc = get_task_setting_value_str(task_data_var, MA_TASK_SCHEDULE_SECTION_NAME_STR, MA_TASK_SCHEDULE_TASK_DELAY_MINUTES_STR, &delay_time))) && delay_time) {
                        delay.delay_time.minute = atoi(delay_time);
                    }
                    (void)ma_task_set_delay_policy(task, delay);

                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "System start trigger delay time <%d>", delay.delay_time.minute);

                    if((MA_OK == rc) && (MA_OK == (rc = ma_trigger_create_system_start(&sys_start_policy, &trigger)))) {
                        rc = ma_task_add_trigger(task, trigger);
                        (void)ma_trigger_release(trigger);
                    }
                    break;
                }
            case MA_TRIGGER_AT_LOGON:
                {
                    ma_trigger_logon_policy_t logon_policy = {0};
                    const char *delay_time = NULL, *once_a_day = NULL;
                    ma_task_delay_policy_t delay = {0};

                    if((MA_OK == (rc = get_task_setting_value_str(task_data_var, MA_TASK_SCHEDULE_SECTION_NAME_STR, MA_TASK_SCHEDULE_ENABLEONCEADAY_STR, &once_a_day)) && once_a_day)) {
                        logon_policy.daily_once = atoi(once_a_day);
                        /* TBD - where do we set this value, scheduler will provide thsi option */
                    }

                    if((MA_OK == rc) && (MA_OK == (rc = get_task_setting_value_str(task_data_var, MA_TASK_SCHEDULE_SECTION_NAME_STR, MA_TASK_SCHEDULE_TASK_DELAY_MINUTES_STR, &delay_time))) && delay_time)
                        delay.delay_time.minute = atoi(delay_time);

                    (void)ma_task_set_delay_policy(task, delay);
                
                    /* TBD - logon_policy.logon_deadline_minutes - no data from ePO about this , 0 value is ok for scheduler */
                     MA_LOG(logger, MA_LOG_SEV_DEBUG, "Logon trigger logon minutes <%d> ", delay.delay_time.minute);

                    if((MA_OK == rc) &&  (MA_OK == (rc = ma_trigger_create_logon(&logon_policy, &trigger)))) {
                        rc = ma_task_add_trigger(task, trigger);
                        (void)ma_trigger_release(trigger);
                    }
                    break;
                }
            case MA_TRIGGER_ON_SYSTEM_IDLE:
                {
                    rc = MA_ERROR_NOT_SUPPORTED;
/*                    ma_trigger_system_idle_policy_t idle_policy = {0};
                    const char *idle_time = NULL;
                              
                    if((MA_OK == (rc = get_task_setting_value_str(task_data_var, MA_TASK_SCHEDULE_SECTION_NAME_STR, MA_TASK_SCHEDULE_IDLEMINUTES_STR, &idle_time))) && idle_time) {
                        idle_policy.idle_wait_minutes = atoi(idle_time);
                    }

                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "Idle trigger idle wait minutes <%d>", idle_policy.idle_wait_minutes);

                    if((MA_OK == rc) && (MA_OK == (rc = ma_trigger_create_system_idle(&idle_policy, &trigger)))) {
                        rc = ma_task_add_trigger(task, trigger);
                        (void)ma_trigger_release(trigger);
                    }*/                
                    break;
                }
            case MA_TRIGGER_NOW:
                {
                    ma_trigger_run_now_policy_t runnow_policy = {0};
                    runnow_policy.now = MA_TRUE;  /* TBD - Sets true to run once, ePO is not sending any data for this type, Why do we need this ??*/

                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "Trigger Now");

                    if(MA_OK == (rc = ma_trigger_create_run_now(&runnow_policy, &trigger))) {
                        rc = ma_task_add_trigger(task, trigger);
                        (void)ma_trigger_release(trigger);
                    }
                
                    is_start_stop_date_needed = MA_FALSE; /* start data and stop is not requiired for this type */
                    break;
                }
            case MA_TRIGGER_ON_DIALUP:
                {
                    rc = MA_ERROR_NOT_SUPPORTED;
                    //ma_trigger_dialup_policy_t dailup_policy = {0};
                    //const char *once_a_day = NULL;
                
                    //if((MA_OK == (rc = get_task_setting_value_str(task_data_var, MA_TASK_SCHEDULE_SECTION_NAME_STR, MA_TASK_SCHEDULE_ENABLEONCEADAY_STR, &once_a_day)) && once_a_day)) {
                    //    /* TBD - where do we set this value, scheduler will provide thsi option */
                    //}

                    //MA_LOG(logger, MA_LOG_SEV_DEBUG, "Trigger Dialup");

                    //if((MA_OK == rc) && (MA_OK == (rc = ma_trigger_create_dialup(&dailup_policy, &trigger)))) {
                    //    rc = ma_task_add_trigger(task, trigger);
                    //    (void)ma_trigger_release(trigger);
                    //}
                    break;
                }
            case MA_TRIGGER_MONTHLY_DOW:
                {
                    /* This trigger is used in the part of MA_TRIGGER_MONTHLY_DATE for ePO tasks */
                    break;
                }
            default:
                rc = MA_ERROR_EPO_TASK_SCHEDULE_FAILED;
                break;
        }
        value = NULL;
    }     

    /* If trigger creates successfully, apply task settings */
    if((MA_OK == rc) && trigger) {
        /* Randamization */
        if((MA_OK == (rc = get_task_setting_value_str(task_data_var, MA_TASK_SCHEDULE_SECTION_NAME_STR, MA_TASK_SCHEDULE_ENABLERANDOM_STR, &value))) && value) {
            ma_task_randomize_policy_t randamization_policy = {0};

            memset(&randamization_policy, 0, sizeof(ma_task_randomize_policy_t));
            if(atoi(value)) {

                if((MA_OK == (rc = get_task_setting_value_str(task_data_var, MA_TASK_SCHEDULE_SECTION_NAME_STR, MA_TASK_SCHEDULE_RANDOMWNDMINS_STR, &value))) && value) {
                    ma_uint64_t tv = (ma_uint64_t)atol(value) * 60;
                    convert_seconds_to_hour_and_minutes(tv, &randamization_policy.random_interval.hour, &randamization_policy.random_interval.minute);
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "Randamization Hr <%d> Mints <%d>", randamization_policy.random_interval.hour, randamization_policy.random_interval.minute);
                }
            }
            value = NULL;
            if(MA_OK != (rc = ma_task_set_randomize_policy(task, randamization_policy))) {
                if(task_id)MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) random policy setting failed", task_id);
            }
        }

        /* time zone */
        if((MA_OK == rc) && (MA_OK == (rc = get_task_setting_value_str(task_data_var, MA_TASK_SCHEDULE_SECTION_NAME_STR, MA_TASK_SCHEDULE_GMTTIME_STR, &value))) && value) {
            ma_time_zone_t zone = MA_TIME_ZONE_LOCAL;
            const char *zone_str = MA_LOCAL_ZONE_STR;

            zone = atol(value) ? MA_TIME_ZONE_UTC : MA_TIME_ZONE_LOCAL;
            if(MA_TIME_ZONE_UTC == zone)zone_str = MA_UTC_ZONE_STR;
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "time zone<%ld> str<%s>", atol(value), zone_str);
            (void)ma_task_set_time_zone(task, zone);
        }

        /* Run If Missed */
        if((MA_OK == rc) && (MA_OK == (rc = get_task_setting_value_str(task_data_var, MA_TASK_SCHEDULE_SECTION_NAME_STR, MA_TASK_SCHEDULE_RUNIFMISSED_STR, &value))) && value) {
            ma_task_missed_policy_t missed_policy = {0};

            memset(&missed_policy, 0, sizeof(ma_task_missed_policy_t));
            (void)ma_task_set_missed(task, atoi(value));
            if(atoi(value)) {

                if((MA_OK == (rc = get_task_setting_value_str(task_data_var, MA_TASK_SCHEDULE_SECTION_NAME_STR, MA_TASK_SCHEDULE_RUNIFMISSED_DELAY_MINS_STR, &value))) && value) {
                    ma_uint64_t tv = (ma_uint64_t)atol(value) * 60;
                    convert_seconds_to_hour_and_minutes(tv, &missed_policy.missed_duration.hour, &missed_policy.missed_duration.minute);
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "Run missed Hr <%d> Mints <%d>", missed_policy.missed_duration.hour, missed_policy.missed_duration.minute);
                }
            }
            value = NULL;
            if(MA_OK != (rc = ma_task_set_missed_policy(task, missed_policy))) {
                if(task_id)MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) missed policy setting failed", task_id);
            }
        }
               
        if((MA_OK == rc) && is_start_stop_date_needed) {
            const char *date_str = NULL;

            if((MA_OK == (rc = get_task_setting_value_str(task_data_var, MA_TASK_SCHEDULE_SECTION_NAME_STR, MA_TASK_SCHEDULE_STARTDATETIME_STR, &date_str))) && date_str) {
                /* The max count is 16, YYYYMMDDHHMMSS */
                sscanf(date_str, "%4hu%2hu%2hu%2hu%2hu%2hu", &start_date.year, &start_date.month, &start_date.day, &start_date.hour, &start_date.minute, &start_date.secs);
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "Start date is <%s>", date_str);
                (void)ma_trigger_set_begin_date(trigger, &start_date);
                date_str = NULL;
            }

            if((MA_OK == (rc = get_task_setting_value_str(task_data_var, MA_TASK_SCHEDULE_SECTION_NAME_STR, MA_TASK_SCHEDULE_STOPDATEVALID_STR, &value))) && value) {
                if(atoi(value)) {
                    if((MA_OK == (rc = get_task_setting_value_str(task_data_var, MA_TASK_SCHEDULE_SECTION_NAME_STR, MA_TASK_SCHEDULE_STOPDATETIME_STR, &date_str))) && date_str) {
                        ma_task_time_t stop_date = {0};
                        /* The max count is 16, YYYYMMDDHHMMSS */
                        sscanf(date_str, "%4hu%2hu%2hu%2hu%2hu%2hu", &stop_date.year, &stop_date.month, &stop_date.day, &stop_date.hour, &stop_date.minute, &stop_date.secs);
                        MA_LOG(logger, MA_LOG_SEV_DEBUG, "Stop date is <%s>", date_str);
                        ma_trigger_set_end_date(trigger, &stop_date);                        
                    }
                    value = NULL;
                }
                else
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "No stop date");
            }  
        }
        /* Task Repeatable */
        if((MA_OK == rc) && (MA_OK == (rc = get_task_setting_value_str(task_data_var, MA_TASK_SCHEDULE_SECTION_NAME_STR, MA_TASK_SCHEDULE_TASKREPEATABEL_STR, &value))) && value) {
            ma_task_repeat_policy_t repeat_policy = {0};

            memset(&repeat_policy, 0, sizeof(ma_task_repeat_policy_t));
            if(atoi(value)) {
                const char *option = NULL;

                if((MA_OK == (rc = get_task_setting_value_str(task_data_var, MA_TASK_SCHEDULE_SECTION_NAME_STR, MA_TASK_SCHEDULE_REPEATOPTION_STR, &option))) && option) {
                    const char *interval = NULL;
                    if((MA_OK == (rc = get_task_setting_value_str(task_data_var, MA_TASK_SCHEDULE_SECTION_NAME_STR, MA_TASK_SCHEDULE_REPEARTINTERVAL_STR, &interval))) && interval) {
                        if(atoi(option))  { /* Minutes */
                            repeat_policy.repeat_interval.minute = atoi(interval);                     
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "Repeat policy interval in Mints <%d>", repeat_policy.repeat_interval.minute);
                        }
                        else { /* Hours */
                            repeat_policy.repeat_interval.hour = atoi(interval);
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "Repeat policy interval in Hrs <%d>", repeat_policy.repeat_interval.hour);
                        }
                        interval = NULL;
                    }
                    option = NULL;
                }

                if((MA_OK == (rc = get_task_setting_value_str(task_data_var, MA_TASK_SCHEDULE_SECTION_NAME_STR, MA_TASK_SCHEDULE_UNTILOPTION_STR, &option))) && option) { 
                    const char *duration = NULL, *time= NULL;
                    if(atoi(option)) {
                        if((MA_OK == (rc = get_task_setting_value_str(task_data_var, MA_TASK_SCHEDULE_SECTION_NAME_STR, MA_TASK_SCHEDULE_UNTILDURATION_STR, &duration))) && duration) {
                            /* Duration comes in minutes */
                            //ma_task_time_t end_time = {0};
                            ma_uint64_t secs = (ma_uint64_t)atoi(duration) * 60 ;

                            convert_seconds_to_hour_and_minutes(secs, &repeat_policy.repeat_until.hour, &repeat_policy.repeat_until.minute);
                            //repeat_policy.repeat_until.hour = end_time.hour;        /* TBD - Max value is 100 from ePO so date maight be changed to next day ????*/
                            //repeat_policy.repeat_until.minute = end_time.minute; 

                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "Repeat policy until duration <%d> mints", atoi(duration));
                        }
                    }
                    else {
                        if((MA_OK == (rc = get_task_setting_value_str(task_data_var, MA_TASK_SCHEDULE_SECTION_NAME_STR, MA_TASK_SCHEDULE_UNTILTIME_STR, &time))) && time) {
                            /* Time value could be 00021130140700 (2:07PM) */
                            if(strlen(time) >= 12)  {
                                ma_task_interval_t until_time = {0};
                                ma_task_interval_t duration = {0};
                               
                                sscanf(time+8,"%2hu%2hu", &repeat_policy.repeat_until.hour, &repeat_policy.repeat_until.minute);
                                until_time.hour = repeat_policy.repeat_until.hour; 
                                until_time.minute = repeat_policy.repeat_until.minute;
                                ma_compute_until_duration(start_date, until_time, &duration);
                                MA_LOG(logger, MA_LOG_SEV_DEBUG, "Repeat policy until time Hrs <%d> Mints <%d> time str<%s>", duration.hour, duration.minute, time);
                                repeat_policy.repeat_until.hour = duration.hour;
                                repeat_policy.repeat_until.minute = duration.minute;
                            }
                        }
                    }
                }
            }                    
            value = NULL;
            if(MA_OK != (rc = ma_task_set_repeat_policy(task, repeat_policy))) {
                if(task_id)MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id(%s) repeat policy setting failed", task_id);
            }
        }        

    }
        
    return rc;
}

ma_error_t handle_epo_tasks(ma_task_service_t *service, const char *task_version) {
    ma_error_t rc = MA_OK;
    ma_db_recordset_t *record_set = NULL;
    ma_configurator_t *configurator = MA_CONTEXT_GET_CONFIGURATOR(service->context);
    ma_db_t *task_db = ma_configurator_get_task_database(configurator);
    ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(service->context);
    ma_scheduler_t *scheduler = MA_CONTEXT_GET_SCHEDULER(service->context);

    if(task_db && (MA_OK != (rc = ma_db_transaction_begin(task_db)))) {
        MA_LOG(logger, MA_LOG_SEV_DEBUG, "Task DB transaction begin failed");
        return rc;
    }

    MA_LOG(logger, MA_LOG_SEV_DEBUG, "Task DB transaction begin succeeded");

    /* Fetch all tasks to be deleted. deleted tasks have the task version "0". it is marked in task handler */
    if(MA_OK == (rc = db_get_all_epo_tasks(task_db, "0", &record_set))) {
        while((MA_OK == rc) && (MA_ERROR_NO_MORE_ITEMS != (rc = ma_db_recordset_next(record_set)))) {
            const char *product_id = NULL, *task_obj_id = NULL, *task_id = NULL, *task_name = NULL, *task_type = NULL;

            if(MA_OK != rc)
                break;

            ma_db_recordset_get_string(record_set, 1, &product_id);
            ma_db_recordset_get_string(record_set, 2, &task_obj_id);
            ma_db_recordset_get_string(record_set, 3, &task_id);
            ma_db_recordset_get_string(record_set, 4, &task_name);
            ma_db_recordset_get_string(record_set, 5, &task_type);

            if(product_id && task_obj_id && task_name && task_type) {
                ma_task_t *task = NULL;
                rc = ma_scheduler_get_task_handle(scheduler, task_obj_id, &task);

                if(MA_OK == rc) {
                    /* Assume that Schedule task relase will take care about all assigned triggers release */
                    (void)ma_task_release(task);
                    if(MA_OK == (rc = ma_scheduler_release_task(scheduler, task))) {
                        task = NULL;
                        MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id <%s> is removed from scheduler",task_obj_id);
                    } else {
                        MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id <%s> is failed to removed from scheduler. rc = <%d>",task_obj_id, rc);
                    }
                }
                else if(MA_ERROR_SCHEDULER_INVALID_TASK_ID == rc) {
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id <%s> is not available",task_obj_id);
                }
                else {
                    MA_LOG(logger, MA_LOG_SEV_ERROR, "scheduler get task handler failed rc = <%d>",rc);
                    rc = MA_ERROR_POLICY_INTERNAL;
                    continue;
                }

                /* removes the task from task DB even task is not available in scheduler */                
                if(MA_OK == (rc = db_remove_epo_task(task_db, task_obj_id))) {
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "task id <%s> is removed from task DB", task_obj_id);
                }
                else {
                    MA_LOG(logger, MA_LOG_SEV_ERROR, "task id <%s> is failed to remove from task DB", task_obj_id);
                    rc = MA_ERROR_POLICY_INTERNAL;
                }                
            }
            else {
                MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to retrive task data from DB");
                rc = MA_ERROR_POLICY_INTERNAL;            
            }
        }

        if(MA_ERROR_NO_MORE_ITEMS == rc)
            rc = MA_OK;

        if(record_set) {
            (void)ma_db_recordset_release(record_set); 
            record_set = NULL;
        }
    }         

    if((MA_OK == rc) && task_version) { /* it means new or modifed tasks are available in task DB */
        /* Fetch all tasks based on task version and schedule */
        if(MA_OK == (rc = db_get_all_epo_tasks(task_db, task_version, &record_set))) {
            while(MA_ERROR_NO_MORE_ITEMS != (rc = ma_db_recordset_next(record_set))) {
                const char *product_id = NULL, *task_obj_id = NULL, *task_id = NULL, *task_name = NULL, *task_type = NULL, *task_priority = NULL, *task_timestamp = NULL;
                ma_variant_t *task_data_var = NULL;

                if(MA_OK != rc)
                    continue;

                ma_db_recordset_get_string(record_set, 1, &product_id);
                ma_db_recordset_get_string(record_set, 2, &task_obj_id);
                ma_db_recordset_get_string(record_set, 3, &task_id);
                ma_db_recordset_get_string(record_set, 4, &task_name);
                ma_db_recordset_get_string(record_set, 5, &task_type);
                ma_db_recordset_get_string(record_set, 6, &task_priority);
                ma_db_recordset_get_string(record_set, 7, &task_timestamp);
                ma_db_recordset_get_variant(record_set, 8, MA_VARTYPE_TABLE, &task_data_var);

                if(product_id && task_obj_id && task_id && task_name && task_type && task_priority && task_timestamp && task_data_var) {
                    ma_task_t *task_ori = NULL, *task = NULL;                
                    ma_bool_t is_new_task = MA_FALSE;

                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "Got Task id [%s]", task_obj_id);
                    if(MA_ERROR_SCHEDULER_INVALID_TASK_ID == (rc = ma_scheduler_get_task_handle(scheduler, task_obj_id, &task_ori))) {
                        MA_LOG(logger, MA_LOG_SEV_DEBUG, "Task id <%s> is not available, so creating new task", task_obj_id);
                        if(MA_OK != (rc = ma_scheduler_create_task(scheduler, ma_configurator_get_agent_software_id(configurator), 
                                                                    task_obj_id, 
                                                                    task_name,
                                                                    task_type,
                                                                    &task))) {
                            MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to create task with task id <%s> rc = <%d>", task_obj_id, rc);
                            if(task_data_var) (void)ma_variant_release(task_data_var);
                            continue;
                        }
                        is_new_task = MA_TRUE;
                    } else if (MA_OK == rc) {
                        ma_task_t *tmp = task_ori;
                        ma_trigger_list_t *tl = NULL;
                        size_t no_of_triggers = 0;
                        size_t itr = 0;
                        ma_bool_t publish_stop_topic = MA_FALSE;

                        task_ori = task; task = tmp;
                        (void)ma_task_get_trigger_list(task, &tl);
                        (void)ma_task_size(task, &no_of_triggers);
                        for(itr = 0; itr < no_of_triggers; ++itr) {
                            ma_trigger_t *trigger = NULL;

                            if(MA_OK == (rc = ma_trigger_list_at(tl, itr, &trigger))) {
                                ma_trigger_state_t state = MA_TRIGGER_STATE_NOT_STARTED;

                                (void)ma_trigger_get_state(trigger, &state);
                                if(MA_TRIGGER_STATE_RUNNING == state) {
                                    //publish_stop_topic = MA_TRUE; /* commenting check before we publish stop topic, if task is already running while modification */
                                }

                                (void)ma_trigger_release(trigger);
                            }
                        }

                        if(publish_stop_topic)
                            ma_task_publish_topics(task, MA_TASK_STOP_PUBSUB_TOPIC);

                        for(itr = 0; itr < no_of_triggers; ++itr) {
                            ma_trigger_t *trigger = NULL;

                            if(MA_OK == (rc = ma_trigger_list_at(tl, itr, &trigger))) {
                                const char *tid = NULL;

                                (void)ma_trigger_get_id(trigger, &tid);
                                if(MA_OK != (rc = ma_task_remove_trigger(task, tid))) {
                                    MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to remove trigger of task id <%s>, rc = <%d>", task_obj_id, rc);
                                }
                                (void)ma_trigger_release(trigger);
                            }
                        }
                        (void)ma_task_release(task);

                    } else {
                        MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to get/create task id <%s>, rc = <%d>", task_obj_id, rc);
                        if(task_data_var) (void)ma_variant_release(task_data_var);
                        continue;
                    }

                    if(task && MA_OK == (rc = apply_settings_to_epo_task(service->context, task, task_data_var))) {
                         const char *value = NULL;
                         ma_bool_t is_task_enabled = MA_FALSE;

                        (void)ma_task_set_creator_id(task, ma_configurator_get_agent_software_id(configurator));
                        (void)ma_task_set_software_id(task, product_id);
                        /* TBD - setting whole task data including scheduling data, should we remove the schedule and settings sections from this data ?? */    
                        (void)ma_task_set_app_payload(task, task_data_var); 
                        /* TBD - proiority will be set - need to finalize the mapping of epo task priority with scheduler task priority type */                            

                        if((MA_OK == (rc = get_task_setting_value_str(task_data_var, MA_TASK_SETTINGS_SECTION_NAME_STR, MA_TASK_SETTINGS_ENABLED_STR, &value))) && value) {
                            is_task_enabled = atoi(value) ? MA_TRUE : MA_FALSE;
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "ePO task status(%d)", is_task_enabled);
                            is_task_enabled ? ma_task_unset_condition(task, MA_TASK_COND_DISABLED) : ma_task_set_condition(task, MA_TASK_COND_DISABLED);
                            value = NULL;
                        }

                        if(MA_OK == (rc = get_task_setting_value_str(task_data_var, MA_TASK_SETTINGS_SECTION_NAME_STR, MA_TASK_SETTINGS_STOPAFTERMINUTES_STR, &value))) {
                            (void)ma_task_set_max_run_time_limit(task, atoi(value));
                            value = NULL;
                        }

                        if(MA_OK == rc) {
                            if(is_new_task) {
                                if(MA_OK == (rc = ma_task_start(task))) {
                                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "ePO task is started");
                                }
                                else 
                                    MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to start epo task - rc = <%d>",rc);
                            } else {
                                if(MA_OK == (rc = ma_scheduler_modify_task(scheduler, task))) {
                                    (void)ma_task_set_name(task, task_name);
                                    (void)ma_task_set_type(task, task_type);
                                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "ePo Task is modified and stopped");
                                } else 
                                    MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to start modified epo task - rc = <%d>",rc);
                            }
                            if((MA_OK == rc) && !is_task_enabled) {
                                if(MA_OK == (rc = ma_task_stop(task))) {
                                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "ePo Task is stopped");
                                }
                            }
                        }

                        if(MA_OK != rc) {
                            MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to create/schedule epo task id <%s>, rc = <%d>",task_obj_id, rc);
                            if(MA_OK == ma_scheduler_get_task_handle(scheduler, task_obj_id, &task)) {
                                (void)ma_task_release(task);
                                (void)ma_scheduler_release_task(scheduler, task);
                            }
                        }
                    } else {
                        MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to create/schedule epo task id <%s>, rc = <%d>",task_obj_id, rc);
                        if(MA_OK == ma_scheduler_get_task_handle(scheduler, task_obj_id, &task)) {
                            (void)ma_task_release(task);
                            (void)ma_task_set_condition(task, MA_TASK_COND_DISABLED);
                            (void)ma_scheduler_release_task(scheduler, task);
                        }
                    }
                }
                
                if(task_data_var) (void)ma_variant_release(task_data_var);
            }

            if(MA_ERROR_NO_MORE_ITEMS == rc)
                rc = MA_OK;

            if(record_set) {
                (void)ma_db_recordset_release(record_set); 
                record_set = NULL;
            }
        }
    }
    if(MA_OK != rc) {
        if(MA_OK == ma_db_transaction_cancel(task_db))
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "Task DB transaction cancelled");        
    }
    else {
        if(MA_OK == ma_db_transaction_end(task_db))
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "Task DB transaction End");        
    }

    return rc;
}

