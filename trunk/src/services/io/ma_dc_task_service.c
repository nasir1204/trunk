#include "ma/internal/services/io/ma_dc_task_service.h"
#include "ma_dc_task_handler.h"

#include "ma/internal/utils/uuid/ma_uuid.h"
#include "ma/datachannel/ma_datachannel.h"
#include "ma/datachannel/ma_datachannel_item.h"

#include "ma/ma_variant.h"
#include "ma/ma_message.h"
#include "ma/ma_msgbus.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/ma_strdef.h"

#include "ma/internal/services/scheduler/ma_task_internal.h"
#include "ma/internal/utils/xml/ma_xml.h"

#include "ma/ma_common.h"
#include "ma/scheduler/ma_task.h"
#include "ma/internal/services/scheduler/ma_scheduler_core.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"

#include "ma/internal/services/updater/ma_updater_request_param.h"

#include "ma/ma_client.h"

#include <stdlib.h>
#include <string.h>

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "runnow"



/*DC task cb*/
static void agent_datachannel_subscriber_cb(ma_client_t *client, const char *product_id, const char *message_id, ma_int64_t correlation_id, ma_datachannel_item_t *dc_message, void *cb_data);

typedef struct ma_dc_task_service_s {
    ma_service_t                        base;
	    
    ma_context_t                        *context;

	ma_dc_task_handler_t                *dc_task_handler;
} ma_dc_task_service_t;

static ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint);

static ma_error_t service_start(ma_service_t *service);

static ma_error_t service_stop(ma_service_t *service);

static void service_destroy(ma_service_t *service);

static const ma_service_methods_t methods = {
    &service_configure,
    &service_start,
    &service_stop,
    &service_destroy
};	


ma_error_t ma_dc_task_service_create(const char *service_name, ma_service_t **service) {
    if(service) {
        ma_dc_task_service_t *self = (ma_dc_task_service_t *)calloc(1, sizeof(ma_dc_task_service_t));        
        if(!self)   return MA_ERROR_OUTOFMEMORY;
        ma_service_init((ma_service_t *)self, &methods, service_name, self);
        *service = (ma_service_t *)self;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint) {
    if(service && context) {        
		ma_dc_task_service_t *self = (ma_dc_task_service_t *)service;
		if(MA_SERVICE_CONFIG_NEW == hint) {
		   ma_error_t rc = MA_OK;
		   if(MA_OK != (rc = ma_dc_task_handler_create(context, &self->dc_task_handler))){
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to create the subscriber in dc task service, rc = %d.", rc);
				return rc;
		   }
		   self->context = context;     				   
		   return rc;
		}
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t service_start(ma_service_t *service) {
	if(service){
		ma_error_t rc = MA_OK;
		ma_dc_task_service_t *self = (ma_dc_task_service_t *)service;

        if(MA_OK == (rc = ma_datachannel_register_message_handler_callback( MA_CONTEXT_GET_CLIENT(self->context) , MA_SOFTWAREID_GENERAL_STR, agent_datachannel_subscriber_cb, self))){
			if(MA_OK == (rc = ma_datachannel_subscribe(MA_CONTEXT_GET_CLIENT(self->context) , MA_SOFTWAREID_GENERAL_STR, DC_TASK_UPDATE_NOW, MA_DC_SUBSCRIBER_LISTENER))){
				if(MA_OK == (rc = ma_datachannel_subscribe(MA_CONTEXT_GET_CLIENT(self->context) , MA_SOFTWAREID_GENERAL_STR, DC_TASK_RUN_NOW, MA_DC_SUBSCRIBER_LISTENER))){					
					if(MA_OK == (rc = ma_dc_task_handler_start(self->dc_task_handler)))
						MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO, "Started dc task handler.");												
					else
						MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Dc task handler failed.");												
				}
				else
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to subscribe datachannel item %s, rc = <%d>", DC_TASK_RUN_NOW, rc);							
			}
			else
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to subscribe datachannel item %s, rc = <%d>", DC_TASK_UPDATE_NOW, rc);											
		}
		else
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to register datachannel callbacks = <%d>", rc);				

		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

static ma_error_t service_stop(ma_service_t *service) {
    if(service) {
        ma_dc_task_service_t *self = (ma_dc_task_service_t *)service;		
        if(MA_OK != ma_datachannel_unsubscribe(MA_CONTEXT_GET_CLIENT(self->context) , MA_SOFTWAREID_GENERAL_STR, DC_TASK_UPDATE_NOW))
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to unsubscribe datachannel item %s", DC_TASK_UPDATE_NOW);				

		if(MA_OK != ma_datachannel_unsubscribe(MA_CONTEXT_GET_CLIENT(self->context) , MA_SOFTWAREID_GENERAL_STR, DC_TASK_RUN_NOW))
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to unsubscribe datachannel item %s.", DC_TASK_RUN_NOW);				

		ma_datachannel_unregister_message_handler_callback(MA_CONTEXT_GET_CLIENT(self->context) , MA_SOFTWAREID_GENERAL_STR);		

		if(MA_OK != ma_dc_task_handler_stop(self->dc_task_handler))
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to stop dc task handler.");						

		MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO, "Stopped dc task service.");							
		return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static void service_destroy(ma_service_t *service) {
    if(service) {
        ma_dc_task_service_t *self = (ma_dc_task_service_t *)service;
		ma_dc_task_handler_release(self->dc_task_handler);
		free(self);
    }
}

#define XML_TASK_GROUP_ROOT								"AgentData/TaskRoot/TaskGroup"
#define XML_TASK_ROOT									"AgentData/TaskRoot/TaskGroup/Task"

#define XML_TASK_ID_STR									"TaskID"
#define XML_TASK_NAME_STR								"DisplayName"
#define XML_TASK_TYPE_STR								"TaskType"
#define XML_TASK_CREATOR_STR							"CreatorID"
#define XML_TASK_OWNER_STR								"SoftwareID"
#define XML_TASK_PRIORITY_STR							"TaskPriority"


static ma_error_t parse_task_xml(ma_dc_task_service_t *self, ma_dc_task_t *dc_task, const char *task_xml_payload, size_t task_xml_len){
	ma_xml_t *task_xml = NULL;
	ma_error_t rc = MA_OK;

    if(MA_OK == (rc = ma_xml_create(&task_xml))) {
        if(MA_OK == (rc = ma_xml_load_from_buffer(task_xml, (char*)task_xml_payload, task_xml_len))) {
            ma_xml_node_t *task_root = NULL;
            ma_xml_node_t *node =  NULL;
            if((NULL != (task_root = ma_xml_get_node(task_xml)))
                &&(NULL != (node = ma_xml_node_search_by_path(task_root, XML_TASK_GROUP_ROOT)))) {                
                ma_table_t *sec_table = NULL;
                ma_variant_t *task_data_var = NULL;              

				const char *name = NULL, *owner = NULL, *type = NULL, *task_id = NULL, *creator = NULL;

				name = (char *)ma_xml_node_attribute_get(node, XML_TASK_NAME_STR);
				owner = (char *)ma_xml_node_attribute_get(node, XML_TASK_OWNER_STR);
				type = (char *)ma_xml_node_attribute_get(node, XML_TASK_TYPE_STR);
				task_id = (char *)ma_xml_node_attribute_get(node, XML_TASK_ID_STR);
				creator = (char *)ma_xml_node_attribute_get(node, XML_TASK_CREATOR_STR);				

				if(name) dc_task->name = strdup(name);				
				if(owner) dc_task->owner = strdup(owner);
				if(type) dc_task->type = strdup(type);

				/*generate a unique task id for the update now task.*/
				{
					char guid[UUID_LEN_TXT] = {0};
					char uuid_taskid[MA_MAX_TASKID_LEN] = {0};
					ma_uuid_generate(0, NULL, guid);
					MA_MSC_SELECT(_snprintf, snprintf)(uuid_taskid, MA_MAX_TASKID_LEN,"%s_%s_%s", 
						(dc_task->dc_task_name ? dc_task->dc_task_name: "RunNow"), 
						(task_id ? task_id: ""), 
						guid); 
					dc_task->task_id = strdup(uuid_taskid);
				}								

				/*agent is the default creator for all the task.*/
				dc_task->creator = strdup(creator ? creator : MA_SOFTWAREID_GENERAL_STR);				

				

                if(!dc_task->task_id || !dc_task->type || !dc_task->name || !dc_task->creator || !dc_task->owner) {
                    MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to get task details from the xml.");
					rc = MA_ERROR_INVALIDARG;            
                }				

				MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO, "dc task name:%s owner:%s type:%s task_id:%s creator:%s",
dc_task->name, dc_task->owner, dc_task->type, dc_task->task_id, dc_task->creator);

                if((MA_OK == rc) && (MA_OK == (rc = ma_table_create(&sec_table)))) {
					/*extract the task sections*/
					ma_xml_node_t *sec_node = NULL;					
					node = ma_xml_node_get_first_child(node);					
                    sec_node = ma_xml_node_get_first_child(node);

                    while((MA_OK == rc) && sec_node) {
                        const char *sec_name = (char *)ma_xml_node_attribute_get(sec_node, "name");
                        ma_xml_node_t *set_node = ma_xml_node_get_first_child(sec_node);
                        ma_variant_t *set_var = NULL;
                        ma_table_t *set_table = NULL;
                        rc = ma_table_create(&set_table);
												
						/*extract the section's settings*/
                        while((MA_OK == rc ) && set_node) {  
                            const char *key = (char *)ma_xml_node_attribute_get(set_node, "name");
                            const char *value = ma_xml_node_get_data(set_node);
                            if(key && value){
                                ma_variant_t *val_var = NULL;
								MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Task Settings [%s]%s = %s.", sec_name, key, value);
                                if(MA_OK == (rc = ma_variant_create_from_string(value, strlen(value), &val_var))) {
                                    rc = ma_table_add_entry(set_table, key, val_var);
                                    (void)ma_variant_release(val_var);                           
                                }
                            }
                            set_node = ma_xml_node_get_next_sibling(set_node);
                        }

						/*add settings into section.*/
                        if(MA_OK == (rc = ma_variant_create_from_table(set_table, &set_var))) {
                            rc = ma_table_add_entry(sec_table, sec_name, set_var);
                            (void)ma_variant_release(set_var);
                        }                                    
                        (void)ma_table_release(set_table);

                        sec_node = ma_xml_node_get_next_sibling(sec_node);
                    }

					/*creating setting variant.*/
                    if(MA_OK == (rc = ma_variant_create_from_table(sec_table, &task_data_var))) {                               						
						dc_task->settings = task_data_var;                        
                    }
                    (void)ma_table_release(sec_table);                                
                }
                else
                    MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to create table");

            }
            else {
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to get node from xml");
                rc = MA_ERROR_POLICY_INTERNAL;
            }
        }
        else
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to load xml from buffer rc <%d>", rc);
                    
        (void)ma_xml_release(task_xml);
    }
    else
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to create maxml rc <%d>", rc);
	return rc;
}

static ma_error_t ma_dc_task_form(ma_dc_task_service_t *self, ma_dc_task_t *dc_task, const char *dc_task_name, const char *task_xml, size_t xml_len){
	if(dc_task && dc_task_name && task_xml && xml_len){
		ma_error_t rc = MA_OK;
		if(MA_OK == (rc = parse_task_xml(self, dc_task, task_xml, xml_len)))
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO, "Parse the dc task %s xml.", dc_task_name);                            		
		else
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to parse the dc task %s xml.", dc_task_name);                            
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

static void handle_dc_task(ma_dc_task_service_t *self, const char *product_id, const char *message_id, ma_int64_t corelation_id, ma_datachannel_item_t *dc_message){	
	ma_variant_t *variant = NULL;
	ma_buffer_t *buffer = NULL;
	ma_error_t rc = MA_OK;
	if(MA_OK == (rc = ma_datachannel_item_get_payload(dc_message, &variant))) {
		const char *dc_data = NULL;
		size_t    dc_data_len = 0;
		if(MA_OK == (rc = ma_variant_get_raw_buffer(variant, &buffer))) {
			if(MA_OK == (rc = ma_buffer_get_string(buffer, &dc_data, &dc_data_len))){
				ma_dc_task_t dc_task;
				ma_dc_task_init(&dc_task); 
				dc_task.corelation_id = corelation_id;
				dc_task.dc_task_name   = strdup(message_id);
				if(MA_OK == (rc = ma_dc_task_form(self, &dc_task, message_id, dc_data, dc_data_len))){
					if(MA_OK == (rc = ma_dc_task_handler_submit(self->dc_task_handler, &dc_task)))
						MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO, "Scheduled the task for the dc message <%s>.", message_id);                            												
					else
						MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to Schedule the dc task <%s>.", message_id);                            							
				}
				else
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to form the dc task <%s> from the xml.", message_id);                            							
				ma_dc_task_deinit(&dc_task); 
			}
			ma_buffer_release(buffer);
		}
		ma_variant_release(variant);
	}
	else
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to get the dc item payload for the dc task <%s>.", message_id);                            							
	return;
}

static void agent_datachannel_subscriber_cb(ma_client_t *client, const char *product_id, const char *message_id, ma_int64_t correlation_id, ma_datachannel_item_t *dc_message, void *cb_data){
	ma_dc_task_service_t *self = (ma_dc_task_service_t*)cb_data;
	if(message_id){
		if(!strcmp(message_id, DC_TASK_UPDATE_NOW) || !strcmp(message_id, DC_TASK_RUN_NOW)){
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO, "DC task received from epo <%s>.", message_id);                            
			handle_dc_task(self, product_id, message_id, correlation_id, dc_message);
		}
		else
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "DC task received <%s>, dc task service can't handle it.", message_id);                            		
	}	
}
