#ifndef MA_SPIPE_COMMON_PROPERTY_MANAGER_H_INCLUDED
#define MA_SPIPE_COMMON_PROPERTY_MANAGER_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/services/ahclient/ma_ah_client_service.h"
#include "ma/internal/utils/context/ma_context.h"

MA_CPP(extern "C" {)

typedef struct ma_spipe_common_property_manager_s ma_spipe_common_property_manager_t, *ma_spipe_common_property_manager_h;

ma_error_t ma_spipe_common_property_manager_create(ma_context_t *context, ma_spipe_common_property_manager_t **self);

ma_spipe_decorator_t *ma_spipe_common_property_manager_get_decorator(ma_spipe_common_property_manager_t *self);

ma_spipe_handler_t *ma_spipe_common_property_manager_get_handler(ma_spipe_common_property_manager_t *self);

ma_error_t ma_spipe_common_property_manager_release(ma_spipe_common_property_manager_t *self);

MA_CPP(})

#endif

