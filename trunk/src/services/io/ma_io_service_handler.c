#include "ma/internal/services/io/ma_io_service_internal.h"
#include "ma/internal/services/io/ma_io_service.h"
#include "ma/internal/services/io/ma_task_service.h"
#include "ma/internal/defs/ma_property_defs.h"
#include "ma/internal/defs/ma_policy_defs.h"
#include "ma/internal/defs/ma_event_defs.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/defs/ma_io_defs.h"

#include "ma/internal/utils/acl/ma_acl.h"
#include "ma/internal/utils/time/ma_time.h"
#include "ma/datastore/ma_ds.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/services/ma_service_manager.h"

#include "ma_localizer.h"
#include "ma/../../src/utils/configurator/ma_configurator_internal.h"
#include "ma/internal/clients/policy/ma_policy_uri_internal.h"
#include "ma/internal/clients/policy/ma_policy_bag_internal.h"
#include "ma/internal/services/policy/ma_policy_service_internal.h"
#include "ma/internal/services/ma_service_utils.h"
#include "ma/internal/services/io/ma_task_service_utils.h"
#include "ma/internal/core/msgbus/ma_msgbus_private.h"
#include "ma/internal/utils/filesystem/ma_fs_utils.h"
#include "ma/internal/services/ma_agent_policy_settings_bag.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include "ma/internal/utils/firewall_exceptions/ma_firewall_exceptions.h"
#include "ma/internal/utils/repository/ma_sitelist.h"
#include "ma/datastore/ma_ds_ini.h"

#include <string.h>

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "ioservice"

ma_error_t ma_info_collect(ma_context_t *context, ma_message_t *msg, ma_msgbus_client_request_t *c_request);
static ma_error_t language_change_handler(ma_io_service_t *service, ma_message_t *msg, ma_message_t *resp);
static ma_error_t language_reset_handler(ma_io_service_t *service, ma_message_t *task_req_msg, ma_message_t *task_rsp_msg);
static ma_error_t ma_config_custom_prop_handler(ma_io_service_t *service, ma_message_t *task_req_msg, ma_msgbus_client_request_t *c_request);

static ma_error_t ma_product_license_state_request_handler(ma_io_service_t *self, ma_message_t *msg, ma_msgbus_client_request_t *c_request);
static ma_error_t ma_license_key_set_handler(ma_io_service_t *self, ma_message_t *msg, ma_msgbus_client_request_t *c_request);
static ma_error_t ma_acl_request_handler(ma_io_service_t *self, ma_message_t *msg, ma_msgbus_client_request_t *c_request);
static ma_error_t ma_file_move_request_handler(ma_io_service_t *self, ma_message_t *msg, ma_msgbus_client_request_t *c_request);
static ma_error_t loglevel_change_handler(ma_io_service_t *service, ma_message_t *task_req_msg, ma_message_t *task_rsp_msg) ;
static ma_error_t ma_firewall_rules_handler(ma_io_service_t *service, ma_message_t *msg, ma_msgbus_client_request_t *c_request, ma_message_t *response);

static ma_error_t ma_send_collect_properties(ma_io_service_t *service) {
	ma_error_t rc = MA_ERROR_INVALIDARG;
	if(service){
		ma_msgbus_endpoint_t *endpoint = NULL;
		ma_message_t *request = NULL;	

		MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "sending collect props command.");
		if(MA_OK == (rc = ma_message_create(&request))) {
			if(MA_OK == (rc = ma_message_set_property(request, MA_PROPERTY_MSG_KEY_TYPE_STR, MA_PROPERTY_MSG_TYPE_COLLECT_STR))) {
				if(MA_OK == (rc = ma_message_set_property(request, MA_PROPERTY_MSG_KEY_PROPS_TYPE_STR, MA_PROPERTY_MSG_VALUE_PROPS_TYPE_VERSION_STR))) {
					if(MA_OK == (rc = ma_msgbus_endpoint_create(MA_CONTEXT_GET_MSGBUS(service->context), MA_PROPERTY_SERVICE_NAME_STR, NULL, &endpoint))) {
						(void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
						if(MA_OK == (rc = ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_INPROC))) {
							if(MA_OK == (rc = ma_msgbus_send_and_forget(endpoint, request))) {
								MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "Properties collect and send command sent.");							
							}
							else
								MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_ERROR, "Properties collect and send command failed, %d.", rc);
						}
						(void)ma_msgbus_endpoint_release(endpoint);
					}
				}
			}
			(void)ma_message_release(request);
		}
	}
	return rc;
}

static ma_error_t ma_spipe_source_ip_change_handler(ma_io_service_t *service, ma_message_t *msg) {
	ma_error_t err = MA_ERROR_INVALIDARG;
	if(service && msg) {
		const char *ip = NULL;
		const ma_network_system_property_t *nw_prop = NULL;
		if(nw_prop = ma_system_property_network_get(MA_CONTEXT_GET_SYSTEM_PROPERTY(service->context), MA_TRUE)){
			if(MA_OK == (err = ma_message_get_property(msg, MA_PROP_KEY_SPIPE_SOURCE_IP_STR, &ip)) && ip) {
				if(strcmp(nw_prop->spipe_src_ip_address_formatted, ip)){
					MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_DEBUG, "ePO communicating local ip changes from <%s> to <%s>", nw_prop->spipe_src_ip_address_formatted, ip);
					memset((void*)nw_prop->spipe_src_ip_address_formatted, 0, MA_IPV6_ADDR_LEN);
					strncpy((char*)nw_prop->spipe_src_ip_address_formatted, ip, MA_IPV6_ADDR_LEN);
					/*Send incremental property */
					err = ma_send_collect_properties(service);
				}
			}
		}
	}
	return err;
}

ma_error_t ma_io_service_server_handler(ma_msgbus_server_t *server, ma_message_t *msg, void *cb_data, ma_msgbus_client_request_t *c_request) {
	/* Handle key upgrade changes */
	/* Handle any other request like GUID regeneration etc */
	ma_io_service_t *self = (ma_io_service_t*)cb_data;
	const char *msg_type = NULL;
	ma_message_t *response = NULL;
	ma_error_t rc = MA_OK;

	MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "IO service received the message.");

	if( (MA_OK == ma_message_get_property(msg, MA_PROP_KEY_REQUEST_TYPE_STR, &msg_type)) && msg_type){
		if(!strcmp(msg_type, MA_PROP_VALUE_AGENT_INFO_COLLECT)){
			MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "IO service received agent info request.");
			rc = ma_info_collect(self->context, msg, c_request);
		}		
		else if(!strcmp(msg_type, MA_CONFIG_MSG_TYPE_CUSTOM_PROPS_STR)) {
			MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "IO service received custom prop setting request.");
			rc = ma_config_custom_prop_handler(self, msg, c_request);
		}
		else if(!strcmp(msg_type, MA_PROP_VALUE_GET_PRODUCT_LICENSE_STATE_STR)){	
			MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "IO service received PP license state info request.");
			rc = ma_product_license_state_request_handler(self, msg, c_request);
		}
		else if(!strcmp(msg_type, MA_CONFIG_MSG_TYPE_LICENSE_KEY_STR)){	
			MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "IO service received license key set request.");
			rc = ma_license_key_set_handler(self, msg, c_request);
		}
		else if(!strcmp(msg_type, MA_PROP_VALUE_ACL_REQUEST_STR)){	
			MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "IO service received acl request.");
			rc = ma_acl_request_handler(self, msg, c_request);
		}
		else if(!strcmp(msg_type, MA_PROP_VALUE_FILE_MOVE_REQUEST_STR)){	
			MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "IO service received file move request.");
			rc = ma_file_move_request_handler(self, msg, c_request);
		}
		else
			rc = MA_ERROR_INVALID_REQUEST;
	}
	else if(MA_OK == ma_message_get_property(msg, MA_IO_SERVICE_MSG_TYPE, &msg_type) && msg_type) {
		if(MA_OK == (rc = ma_message_create(&response))) {
			if(!strcmp(msg_type, MA_IO_MSG_TYPE_LANG_SETTING_CHANGED_STR)) {
				MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "IO service received language setting change request.");
				rc = language_change_handler(self, msg, response);
			} else if(!strcmp(msg_type, MA_IO_MSG_TYPE_LANG_SETTING_RESET_STR)) {
				MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "IO service received language resetting request.");
				rc = language_reset_handler(self, msg, response);
			} else if(!strcmp(msg_type, MA_IO_MSG_TYPE_LOGLEVEL_SETTING_CHANGED_STR)) {
				MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "IO service received loglevel setting request.");
				rc = loglevel_change_handler(self, msg, response);
			} else if(!strcmp(msg_type, MA_IO_MSG_TYPE_STATUS_CHECK)) {
				rc = MA_OK;
			} else if(!strcmp(msg_type, MA_IO_MSG_TYPE_RECONFIGURE_SERVICES_STR)) {
				rc = ma_service_manager_configure_all(&self->service_manager, self->context, MA_SERVICE_CONFIG_MODIFIED);				
			} else if(self->relay_handler && (!strcmp(msg_type, MA_IO_MSG_TYPE_GET_RELAY_INFO) || !strcmp(msg_type, MA_IO_MSG_TYPE_REFRESH_RELAY_INFO)) ) {
				MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "IO service received relay message.");
				(void)ma_relay_handler_handle_msg(self->relay_handler, msg_type, c_request, msg);
				(void)ma_message_release(response);
				return rc;
			}
			else if(!strcmp(msg_type, MA_IO_MSG_TYPE_FIREWALL_RULE_CHANGED_STR)){			
				MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "IO service received firewall rule change request.");
				rc = ma_firewall_rules_handler(self, msg, c_request, response);
			}else {
				rc = MA_ERROR_INVALID_REQUEST;
			}
			if(rc != MA_ERROR_INVALID_REQUEST){            
				if(MA_OK != (rc = ma_msgbus_server_client_request_post_result(c_request, rc, response))) {
					MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "msgbus posting client setting language change request failed, last error(%d)", rc);
				}
			}
			(void)ma_message_release(response);
		}        
	}
	else if(MA_OK == ma_message_get_property(msg, MA_PROP_KEY_SPIPE_SOURCE_IP_STR, &msg_type) && msg_type) {
		return (rc = ma_spipe_source_ip_change_handler(self, msg));
	}
	else 
		rc = MA_ERROR_INVALID_REQUEST;

	/* Reply with MA_ERROR_INVALID_REQUEST status to clients */
	if(MA_ERROR_INVALID_REQUEST == rc) {
		if(MA_OK == (rc = ma_message_create(&response))) {
			rc = ma_msgbus_server_client_request_post_result(c_request, rc, response);
			(void) ma_message_release(response);
		}
	}

	return rc;
}


static ma_error_t update_last_asc_time(ma_io_service_t *self) {    
	ma_time_t t = {0} ; 
	char asc_time[17] = {0} ;
	time_t backcompat_time ;
	ma_configurator_t *configurator = MA_CONTEXT_GET_CONFIGURATOR(self->context);
	ma_ds_t *ds = ma_configurator_get_datastore(configurator);

	ma_time_get_localtime(&t) ;
	backcompat_time  = time(NULL);
	MA_MSC_SELECT(_snprintf, snprintf)(asc_time, 16, "%04d%02d%02d%02d%02d%02d", t.year, t.month, t.day, t.hour, t.minute, t.second) ;
	MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Property ASC time <%s> on prop send request success", asc_time);
#if defined(MA_WINDOWS)
	{
		HKEY hKey;
		LSTATUS result = ERROR_SUCCESS;
		if(ERROR_SUCCESS == (result = RegOpenKeyExA(HKEY_LOCAL_MACHINE, SZ_COMPAT_ePO_AGENT_REG_PATH, 0, KEY_WRITE | KEY_WOW64_32KEY, &hKey))) {
			result = RegSetValueExA(hKey, MA_AGENT_LAST_ASCI_TIME_DWORD, 0, REG_DWORD, (unsigned char*)&backcompat_time, sizeof(DWORD));
			RegCloseKey(hKey);
		}
	}
#endif

	return ma_ds_set_str(ds, MA_PROPERTY_DATA_PATH_NAME_STR, MA_PROPERTY_DATA_KEY_ASC_TIME_STR, asc_time, strlen(asc_time)) ;    
}


static ma_error_t send_policy_refresh_message(ma_io_service_t *self, const char *enforce_policy) {
	ma_error_t rc = MA_OK;
	ma_message_t *policy_msg = NULL;
	if(MA_OK == (rc = ma_message_create(&policy_msg))) {
		ma_msgbus_endpoint_t *ep = NULL;     
		
        (void)ma_message_set_property(policy_msg, MA_PROP_KEY_POLICY_REQUEST_TYPE_STR, MA_PROP_VALUE_REFRESH_POLICIES_REQUEST_STR) ;
        if(enforce_policy) (void)ma_message_set_property(policy_msg, MA_PROP_KEY_FORCE_POLICY_ENFORCEMENT_STR, enforce_policy) ;

		if(MA_OK == (rc = ma_msgbus_endpoint_create(MA_CONTEXT_GET_MSGBUS(self->context), MA_POLICY_SERVICE_NAME_STR, MA_POLICY_SERVICE_HOSTNAME_STR, &ep))) { 
			(void)ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
			(void)ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_INPROC); 
			
            MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Policy refresh starts on prop send request success");
			
            rc = ma_msgbus_send_and_forget(ep, policy_msg);
			
            (void)ma_msgbus_endpoint_release(ep);       
		}
		(void)ma_message_release(policy_msg);
	}	
	return rc; 
}

static ma_error_t send_event_forward_message(ma_io_service_t *self){
	ma_error_t rc = MA_OK;	
	ma_message_t *message = NULL;
	if( MA_OK == (rc = ma_message_create(&message) ) ){
		ma_msgbus_endpoint_t *endpoint = NULL;
		ma_message_set_property(message, MA_EVENT_MSG_PROP_KEY_MSG_TYPE, MA_EVENT_MSG_PROP_VALUE_REQ_UPLOAD_EVENT);
		ma_message_set_property(message, MA_EVENT_MSG_PROP_KEY_EVENT_PRIORITY, "1");
		if( MA_OK == ( rc = ma_msgbus_endpoint_create(MA_CONTEXT_GET_MSGBUS(self->context), MA_EVENT_SERVICE_NAME_STR, NULL, &endpoint) ) ) {
			ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
			ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_INPROC);
			MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Event upload starts on prop send request success");
			rc = ma_msgbus_send_and_forget(endpoint, message);
			ma_msgbus_endpoint_release(endpoint);						
		}
		ma_message_release(message);
	}
	return rc;
}

/* subscribers are ma or ma_broker. No payload attached to this publish message, subscribers will read policies from Database */
static ma_error_t publish_server_settings_change_msg(ma_io_service_t *self) {
	ma_message_t *msg = NULL;
	ma_error_t rc = MA_OK;
	if(MA_OK == (rc = ma_message_create(&msg))) {      
		MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Publishing server settings change notification");
		rc = ma_msgbus_publish(MA_CONTEXT_GET_MSGBUS(self->context), MA_SERVER_SETTINGS_CHANGE_PUB_TOPIC_STR, MSGBUS_CONSUMER_REACH_OUTPROC, msg);
		ma_message_release(msg);
	}

	if(MA_OK != rc) {
		MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to publish server settings change notification <%s>", MA_SERVER_SETTINGS_CHANGE_PUB_TOPIC_STR);
	}
	else {
		MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Successfully publish server settings change notification <%s>", MA_SERVER_SETTINGS_CHANGE_PUB_TOPIC_STR);
	}
	return rc;
}

ma_error_t encrypt_and_encode_lic_key(ma_io_service_t *self, ma_variant_t *in_var, ma_variant_t **out_var) {
	ma_error_t rc = MA_OK;
	if(in_var && out_var) {
		ma_crypto_encryption_t *encrypto = NULL;
		if(MA_OK == (rc = ma_crypto_encrypt_create(MA_CONTEXT_GET_CRYPTO(self->context),  MA_CRYPTO_ENCRYPTION_3DES, &encrypto))){
			ma_buffer_t *buffer = NULL;
			(void) ma_variant_get_string_buffer(in_var, &buffer);
			if(buffer) {
				const char *str = NULL;
				size_t size = 0;
				(void) ma_buffer_get_string(buffer, &str, &size);
				if(str) {
					ma_bytebuffer_t *bytebuffer = NULL;
					/* allocate max buffer */
					if(MA_OK == (rc = ma_bytebuffer_create(1024, &bytebuffer))) {
						if(MA_OK == (rc = ma_crypto_encrypt_and_encode(encrypto, (unsigned char*)str, size, bytebuffer))){
							rc = ma_variant_create_from_string((const char *)ma_bytebuffer_get_bytes(bytebuffer), ma_bytebuffer_get_size(bytebuffer), out_var);
						}
						(void) ma_bytebuffer_release(bytebuffer);
					}
				}
				(void) ma_buffer_release(buffer);
			}
			(void) ma_crypto_encrypt_release(encrypto);
		}
	}

	if(MA_OK != rc)
		MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Unable to encrypt and encode license key, rc <%d>",rc);

	return rc;
}

static ma_error_t set_server_settings(ma_io_service_t *self, ma_policy_bag_t *policy_bag, ma_table_t *settings_tb) {
	ma_error_t rc = MA_OK;
	ma_policy_uri_t *general_uri = NULL, *repo_uri = NULL ;
	if(MA_OK == (rc = ma_policy_uri_create(&general_uri))) {
		ma_variant_t *value_var = NULL;                

		ma_policy_uri_set_feature(general_uri, MA_POLICY_FEATURE_ID);
		ma_policy_uri_set_category(general_uri, MA_POLICY_CATEGORY_ID);    
		ma_policy_uri_set_type(general_uri, MA_POLICY_TYPE_ID);   
		ma_policy_uri_set_name(general_uri, MA_POLICY_NAME_ID);					

		if(MA_OK == (rc = ma_table_get_value(settings_tb, MA_SERVER_SETTING_AGENT_BROADCAST_PORT_STR, &value_var))) {
			rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_UDP_SERVICE_SECTION_NAME_STR, MA_UDP_KEY_BROADCAST_PORT_INT, value_var); 				    
			(void) ma_variant_release(value_var); value_var = NULL;
		}

		if(MA_OK == (rc = ma_table_get_value(settings_tb, MA_SERVER_SETTING_AGENT_PING_PORT_STR, &value_var))) {
			rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_HTTP_SERVER_SERVICE_SECTION_NAME_STR, MA_HTTP_SERVER_KEY_PORT_INT, value_var); 				    
			(void) ma_variant_release(value_var); value_var = NULL;
		}

		if(MA_OK == (rc = ma_table_get_value(settings_tb, MA_SERVER_SETTING_CATALOG_VERSION_STR, &value_var))) {
			rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_UPDATER_SERVICE_SECTION_NAME_STR, MA_UPDATER_KEY_CATALOG_VERSION_STR, value_var); 				    
			(void) ma_variant_release(value_var); value_var = NULL;
		}

		if(MA_OK == (rc = ma_table_get_value(settings_tb, MA_SERVER_SETTING_ENABLE_AUTO_UPDATE_STR, &value_var))) {
			rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_UDP_SERVICE_SECTION_NAME_STR, MA_UDP_ENABLE_AUTO_UPDATE_STR, value_var); 				    
			(void) ma_variant_release(value_var); value_var = NULL;
		}

		if(MA_OK == (rc = ma_table_get_value(settings_tb, MA_SERVER_SETTING_ENFORCE_POLICY_STR, &value_var))) {
			rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_POLICY_SERVICE_SECTION_NAME_STR, MA_POLICY_KEY_ENFORCE_POLICY_STR, value_var); 				    
			(void) ma_variant_release(value_var); value_var = NULL;
		}

		if(MA_OK == (rc = ma_table_get_value(settings_tb, MA_SERVER_SETTING_SERVER_NAME_STR, &value_var))) {
			rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_AGENT_GENERAL_SECTION_NAME_STR, MA_AGENT_KEY_SERVER_NAME_STR,  value_var); 				    
			(void) ma_variant_release(value_var); value_var = NULL;
		}

		(void) ma_policy_uri_release(general_uri);
	}

	if( MA_OK == (rc = ma_policy_uri_create(&repo_uri)) ) {				
		ma_variant_t *value_var = NULL;

		ma_policy_uri_set_feature(repo_uri, MA_POLICY_FEATURE_ID);
		ma_policy_uri_set_category(repo_uri, MA_REPO_POLICY_CATEGORY_ID);    
		ma_policy_uri_set_type(repo_uri, MA_REPO_POLICY_TYPE_ID);   
		ma_policy_uri_set_name(repo_uri, MA_POLICY_NAME_ID);   	

		if(MA_OK == (rc = ma_table_get_value(settings_tb, MA_SERVER_SETTING_LICENSE_KEY_STR, &value_var))) {
			ma_variant_t *enc_lic_key_var = NULL;
			if(MA_OK == encrypt_and_encode_lic_key(self, value_var, &enc_lic_key_var)) {
				rc = ma_policy_bag_set_value(policy_bag, repo_uri, MA_REPOSITORY_LICENSE_SECTION_NAME_STR, MA_REPOSITORY_KEY_LICENSE_STR, enc_lic_key_var);
				ma_variant_release(enc_lic_key_var);
			}
			(void) ma_variant_release(value_var); value_var = NULL;
		}
		(void) ma_policy_uri_release(repo_uri);
	}

	if(MA_OK == rc) {
		ma_policy_uri_t *agent_uri = NULL;
		if( MA_OK == (rc = ma_policy_uri_create(&agent_uri)) ) {                            
			ma_configurator_ex_t *configurator = (ma_configurator_ex_t*)self->configurator;
			ma_policy_uri_set_software_id(agent_uri, MA_SOFTWAREID_GENERAL_STR) ;
			rc = ma_import_policies(configurator->db_policy, agent_uri, policy_bag) ;
			(void) ma_policy_uri_release(agent_uri);
		}
	}
	return rc;
}

static void update_hack_for_firewall_port(ma_io_service_t *self, ma_policy_bag_t *policy_bag) {
#ifndef MA_WINDOWS
	#if defined(__APPLE__)
		static const char *ini_name = "/Library/McAfee/cma/scratch/agent.ini";
	#else
		static const char *ini_name = "/opt/McAfee/cma/scratch/agent.ini";
	#endif
	ma_ds_t *ini = NULL;

	if(MA_OK == ma_ds_ini_open(ini_name, 0, (ma_ds_ini_t **) &ini)) {
		ma_variant_t *variant = NULL;
		if(MA_OK == ma_policy_bag_get_value_internal(policy_bag, NULL, MA_HTTP_SERVER_SERVICE_SECTION_NAME_STR, MA_HTTP_SERVER_KEY_PORT_INT, &variant)) {
			ma_buffer_t *buffer = NULL;
			if(MA_OK == ma_variant_get_string_buffer(variant, &buffer)) {
				const char *str = NULL;
				size_t size = 0;
				if(MA_OK == ma_buffer_get_string(buffer, &str, &size)) {
					(void)ma_ds_set_int(ini, "AgentListenServer", "bEnableAgentPing", 1);
					(void)ma_ds_set_str(ini, "AgentListenServer", "AgentPingPort", str, -1);
				}
				(void)ma_buffer_release(buffer);
			}
			(void)ma_variant_release(variant);
		}
		(void)ma_ds_release(ini);
	}
#endif
}


static ma_error_t update_server_settings(ma_io_service_t *self, ma_message_t *msg) {
	ma_error_t rc = MA_OK;
	ma_variant_t *settings_var = NULL;
	ma_policy_bag_t *policy_bag = NULL;
	ma_db_t *policy_db =  ma_configurator_get_policies_database(self->configurator);

	ma_policy_uri_t *policy_uri = NULL;

	if(MA_OK == (rc = ma_policy_uri_create(&policy_uri))) {
		(void)ma_policy_uri_set_software_id(policy_uri, MA_SOFTWAREID_GENERAL_STR);
		rc = get_agent_policies(policy_uri, policy_db, &policy_bag);
		(void)ma_policy_uri_release(policy_uri);
	} 

	if(policy_bag) {
		if(MA_OK == (rc = ma_message_get_payload(msg, &settings_var))) {
			ma_table_t *settings_tb = NULL;
			if(MA_OK == (rc = ma_variant_get_table(settings_var, &settings_tb))) {
				if(MA_OK == (rc = set_server_settings(self, policy_bag, settings_tb))) {
					ma_bool_t are_server_settings_changed = (!self->server_settings_tb) ? MA_TRUE : MA_FALSE;
					if(!are_server_settings_changed) {
						ma_bool_t b_rc = MA_FALSE;
						rc = ma_table_is_equal(self->server_settings_tb, settings_tb, &b_rc);
						are_server_settings_changed = !b_rc;
					}
					if((MA_OK == rc) && are_server_settings_changed) {
						if(self->server_settings_tb) ma_table_release(self->server_settings_tb), self->server_settings_tb = NULL;
						ma_table_add_ref(self->server_settings_tb = settings_tb);
						update_hack_for_firewall_port(self, policy_bag);
						rc = publish_server_settings_change_msg(self);
					}
				}
				(void) ma_table_release(settings_tb);
			}
			(void) ma_variant_release(settings_var);
		}        
		(void) ma_policy_bag_release(policy_bag);
	}
	return rc;
}

static ma_error_t validate_ma_policies(ma_io_service_t *self) {
	ma_error_t rc = MA_OK;
	ma_policy_bag_t *policy_bag = NULL;
	ma_db_t *policy_db =  ma_configurator_get_policies_database(self->configurator);
	ma_policy_uri_t *policy_uri = NULL;
	ma_bool_t is_validation_success = MA_FALSE;

	if(MA_OK == (rc = ma_policy_uri_create(&policy_uri))) {
		(void)ma_policy_uri_set_software_id(policy_uri, MA_SOFTWAREID_GENERAL_STR);        

		if((MA_OK == (rc = get_agent_policies(policy_uri, policy_db, &policy_bag))) && policy_bag) {          			
			ma_variant_t *value_var = NULL;

			/* If "ExtensionVersion" policy is not available, then policies recevied from ePO with old extension. so we can go head with existing MA policies */
			if(MA_OK == ma_policy_bag_get_value_internal(policy_bag, NULL, MA_AGENT_GENERAL_SECTION_NAME_STR, MA_AGENT_EXENSION_VERSION_STR, &value_var)) {  
				is_validation_success = MA_TRUE; 
				(void) ma_variant_release(value_var);
			}
			else {
				MA_LOG(self->logger, MA_LOG_SEV_WARNING, "Received MA policies doesn't contain the extension information, So MA continues with existing policies");                                       
			}

			(void) ma_policy_bag_release(policy_bag);
		}
		else {
			rc = MA_OK;
			MA_LOG(self->logger, MA_LOG_SEV_WARNING, "There are no MA policies in recevied policies, So MA continues with existing policies"); 
		}

		if((MA_OK == rc) && !is_validation_success) {
			ma_policy_bag_t *cur_policy_bag = MA_CONTEXT_GET_POLICY_BAG(self->context);
			rc = ma_import_policies(policy_db, policy_uri, cur_policy_bag) ;
		}

		(void)ma_policy_uri_release(policy_uri);
	}     

	return rc;
}

ma_error_t ma_io_service_subscriber_handler(const char *topic, ma_message_t *msg, void *cb_data) {
	ma_error_t rc = MA_OK;
	ma_io_service_t *self = (ma_io_service_t *)cb_data;

	if(topic && msg && self) {
		if(!strcmp(topic, MA_PROPERTY_PUBSUB_TOPIC_COLLECT_STATUS_STR)) {            
			const char *props_send_status = NULL, *enforce_policy = NULL ;
			
            MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "io service subscriber handler received topic <%s>", topic);
			
            (void)ma_message_get_property(msg, MA_PROPERTY_MSG_KEY_COLLECT_STATUS_KEY_NAME_STR, &props_send_status);
            (void)ma_message_get_property(msg, MA_PROPERTY_MSG_KEY_ENFORCE_POLICY_STR, &enforce_policy) ;

			if(!strcmp(props_send_status, "1")) {   /* if property send success */				
				rc = ((MA_OK == (rc = update_last_asc_time(self)))
					&& (MA_OK == (rc = send_policy_refresh_message(self, enforce_policy)))
					&& (MA_OK == (rc = send_event_forward_message(self)))) ? MA_OK : rc;
			}
		}else if(!strcmp(topic, MA_POLICY_UPDATE_SUCCESS_TOPIC_STR)) {            
			ma_time_t time = {0}; 
			char policy_updater_time[17] = {0} ;
			ma_configurator_t *configurator = MA_CONTEXT_GET_CONFIGURATOR(self->context);
			ma_ds_t *ds = ma_configurator_get_datastore(configurator);

			MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "io service subscriber handler received topic <%s>", topic);

			if(MA_OK != (rc = validate_ma_policies(self)))
				MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to validate the recevied MA, rc <%d>", rc);

			if(MA_OK != (rc = update_server_settings(self, msg)))
				MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to update server settings in DS, rc <%d>", rc);

			ma_time_get_localtime(&time) ;
			MA_MSC_SELECT(_snprintf, snprintf)(policy_updater_time, 16, "%04d%02d%02d%02d%02d%02d", time.year, time.month, time.day, time.hour, time.minute, time.second) ;
			MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "policy update time <%s> on policy update succes topic.", policy_updater_time);
			ma_ds_set_str(ma_configurator_get_datastore(MA_CONTEXT_GET_CONFIGURATOR(self->context)), MA_PROPERTY_PROPERTIES_PATH_NAME_STR, MA_PROPERTY_PROPERTIES_KEY_LAST_POLICY_ENFORCEMENT_STATUS, "1", 1) ;
			return ma_ds_set_str(ds, MA_POLICY_DATA_PATH_NAME_STR, MA_POLICY_UPDATE_TIME_STR, policy_updater_time, strlen(policy_updater_time)) ;    
		}else if( (!strcmp(topic, MA_POLICY_REFRESH_FAILURE_TOPIC_STR)) || (!strcmp(topic, MA_POLICY_UPDATE_FAILURE_TOPIC_STR))) {   
			ma_ds_set_str(ma_configurator_get_datastore(MA_CONTEXT_GET_CONFIGURATOR(self->context)), MA_PROPERTY_PROPERTIES_PATH_NAME_STR, MA_PROPERTY_PROPERTIES_KEY_LAST_POLICY_ENFORCEMENT_STATUS, "0", 1) ;
		}
		else if(!strcmp(topic, MA_SERVER_SETTINGS_CHANGE_PUB_TOPIC_STR)) {
			if(MA_OK == (rc = ma_agent_policy_settings_bag_refresh(self->policy_settings_bag))) 
				rc = ma_service_manager_configure_all(&self->service_manager, self->context, MA_SERVICE_CONFIG_MODIFIED);
		}
		else if(!strcmp(topic, MA_TASK_EXECUTE_PUBSUB_TOPIC)) {
			if(self->relay_handler){
				const char *task_type = NULL;
				(void)ma_message_get_property(msg, MA_TASK_TYPE, &task_type);
				if(task_type && !strcmp(task_type, MA_PROPERTY_COLLECT_TASK_TYPE_STR)) 
					(void)ma_relay_handler_refresh(self->relay_handler);				
			}
		}
		else if(!strcmp(topic, MA_IO_MSG_TYPE_UPDATE_REG_INFO)) {
			ma_service_t *compat_service = ma_service_manager_find_service(&self->service_manager, MA_COMPAT_SERVICE_NAME_STR);
			if(MA_OK == ma_service_configure(compat_service, self->context, MA_SERVICE_CONFIG_MODIFIED)) {
				rc = MA_OK;
			}
		}
	}
	return rc;
}

ma_error_t language_change_handler(ma_io_service_t *service, ma_message_t *task_req_msg, ma_message_t *task_rsp_msg) {
	if(service && task_req_msg && task_rsp_msg) {
		ma_error_t err = MA_OK;
		const char *lang = NULL;
		const char *reply_status = MA_IO_REPLY_STATUS_FAILED;
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(service->context);
		ma_configurator_t *configurator = MA_CONTEXT_GET_CONFIGURATOR(service->context);
		ma_ds_t *ds = ma_configurator_get_datastore(configurator);


		if(MA_OK == (err = ma_message_get_property(task_req_msg, MA_IO_CONFIGURED_LANGUAGE_STR, &lang)) && lang) {
			if(MA_OK == (err = ma_ds_set_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_USER_LANGUAGE_STR, lang, strlen(lang)))) {
				if(MA_OK == (err = ma_ds_set_int(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURAITON_KEY_LANG_OVERRIDE_INT, 1))) {
					ma_service_t *logger_service = ma_service_manager_find_service(&service->service_manager, MA_LOGGER_SERVICE_NAME_STR);

					if(MA_OK == (err = ma_service_configure(logger_service, service->context, MA_SERVICE_CONFIG_MODIFIED))) {
						reply_status = MA_IO_REPLY_STATUS_SUCCESS;
					}
				}
			}
		}
		if(MA_OK != err)
			MA_LOG(logger, MA_LOG_SEV_CRITICAL, "io service language setting update failed, last error(%d)", err);

		return ma_message_set_property(task_rsp_msg, MA_IO_REPLY_STATUS, reply_status);
	}
	return MA_ERROR_INVALIDARG;
}

static ma_error_t loglevel_change_handler(ma_io_service_t *service, ma_message_t *task_req_msg, ma_message_t *task_rsp_msg) {
	if(service && task_req_msg && task_rsp_msg) {
		ma_error_t err = MA_OK;
		const char *loglevel = NULL;
		const char *reply_status = MA_IO_REPLY_STATUS_FAILED;
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(service->context);
		ma_configurator_ex_t *configurator = (ma_configurator_ex_t*)service->configurator;
		if(configurator){
			if(MA_OK == (err = ma_message_get_property(task_req_msg, MA_IO_CONFIGURED_LOGLEVEL_STR, &loglevel)) && loglevel) {
				char *filter_pattern = NULL;
				char *is_app_log_enabled = NULL;
				if(!strcmp("0", loglevel)){
					MA_LOG(logger, MA_LOG_SEV_INFO, "Disabling logging");
					is_app_log_enabled = strdup("0");
				}else if(!strcmp("1", loglevel)){
					MA_LOG(logger, MA_LOG_SEV_INFO, "Setting loglevel to Info");
					is_app_log_enabled = strdup("1");
					filter_pattern = strdup("*.Info");
				}else if(!strcmp("2", loglevel)){
					MA_LOG(logger, MA_LOG_SEV_INFO, "Setting loglevel to Debug");
					is_app_log_enabled = strdup("1");
					filter_pattern = strdup("*.Debug");
				}else if(!strcmp("3", loglevel)){
					MA_LOG(logger, MA_LOG_SEV_INFO, "Setting loglevel to Trace");
					is_app_log_enabled = strdup("1");
					filter_pattern = strdup("*.Trace");
				}else{
					MA_LOG(logger, MA_LOG_SEV_ERROR, "io service recieved invalid loglevel <%s>", loglevel);
				}

				if(filter_pattern || is_app_log_enabled){
					ma_policy_uri_t *general_uri = NULL;              
					if(MA_OK == ma_policy_uri_create(&general_uri)) {
						ma_variant_t *value = NULL;
						ma_service_t *logger_service = ma_service_manager_find_service(&service->service_manager, MA_LOGGER_SERVICE_NAME_STR);
						ma_policy_bag_t *policy_bag = MA_CONTEXT_GET_POLICY_BAG(service->context);
						ma_policy_uri_set_feature(general_uri, MA_POLICY_FEATURE_ID); 
						ma_policy_uri_set_category(general_uri, MA_POLICY_CATEGORY_ID);    
						ma_policy_uri_set_type(general_uri, MA_POLICY_TYPE_ID);  
						ma_policy_uri_set_software_id(general_uri, MA_SOFTWAREID_GENERAL_STR);
						if(is_app_log_enabled){
							if(MA_OK == (err = ma_variant_create_from_string(is_app_log_enabled, strlen(is_app_log_enabled), &value))) {
								ma_policy_bag_set_value(policy_bag, general_uri, MA_LOGGER_SERVICE_SECTION_NAME_STR, MA_LOGGER_KEY_LOG_APP_LOGGING_ENABLED_INT, value);
								(void) ma_variant_release(value); value = NULL;
							}
							free(is_app_log_enabled);
						}
						if(filter_pattern){
							if(MA_OK == (err = ma_variant_create_from_string(filter_pattern, strlen(filter_pattern), &value))) {
								ma_policy_bag_set_value(policy_bag, general_uri, MA_LOGGER_SERVICE_SECTION_NAME_STR, MA_LOGGER_KEY_FILTER_PATTERN_STR, value);
								(void) ma_variant_release(value); value = NULL;
							}
							free(filter_pattern);
						}
					//	if(MA_OK == (err = ma_service_configure(logger_service, service->context, MA_SERVICE_CONFIG_MODIFIED))) {
							if(MA_OK != ma_import_policies(configurator->db_policy, general_uri, policy_bag)){
								MA_LOG(logger, MA_LOG_SEV_ERROR, "import policies failed");
							}
							reply_status = MA_IO_REPLY_STATUS_SUCCESS;
					//	}	
						(void)ma_policy_uri_release(general_uri);
					}
					else{
						if(is_app_log_enabled)
							free(is_app_log_enabled);
						if(filter_pattern)
							free(filter_pattern);
					}
				}
			}
		}

		if(MA_OK != err)
			MA_LOG(logger, MA_LOG_SEV_CRITICAL, "io service loglevel setting update failed, last error(%d)", err);

		return ma_message_set_property(task_rsp_msg, MA_IO_REPLY_STATUS, reply_status);
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t language_reset_handler(ma_io_service_t *service, ma_message_t *task_req_msg, ma_message_t *task_rsp_msg) {
	if(service && task_req_msg && task_rsp_msg) {
		ma_error_t err = MA_OK;
		const char *lang = NULL;
		const char *reply_status = MA_IO_REPLY_STATUS_FAILED;
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(service->context);
		ma_configurator_t *configurator = MA_CONTEXT_GET_CONFIGURATOR(service->context);
		ma_ds_t *ds = ma_configurator_get_datastore(configurator);
		const ma_misc_system_property_t *misc_prop = NULL;
		const char *default_lang_id = "0409";

		//(void)ma_system_property_scan(MA_CONTEXT_GET_SYSTEM_PROPERTY(service->context));
		misc_prop = ma_system_property_misc_get(MA_CONTEXT_GET_SYSTEM_PROPERTY(service->context), MA_FALSE);
		lang = strcmp(misc_prop->language_code, "") ? misc_prop->language_code : default_lang_id;
		MA_LOG(logger, MA_LOG_SEV_DEBUG, "io service resetting language(%s)", lang);
		if(MA_OK == (err = ma_ds_remove(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_USER_LANGUAGE_STR))) {
			ma_service_t *logger_service = ma_service_manager_find_service(&service->service_manager, MA_LOGGER_SERVICE_NAME_STR);    
			ma_ds_remove( ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURAITON_KEY_LANG_OVERRIDE_INT);
			if(MA_OK == (err = ma_service_configure(logger_service, service->context, MA_SERVICE_CONFIG_MODIFIED))) {
				reply_status = MA_IO_REPLY_STATUS_SUCCESS;
			}
		}
		if(MA_OK != err)
			MA_LOG(logger, MA_LOG_SEV_CRITICAL, "io service language resetting failed, last error(%d)", err);

		return ma_message_set_property(task_rsp_msg, MA_IO_REPLY_STATUS, reply_status);
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_config_custom_prop_handler(ma_io_service_t *service, ma_message_t *task_req_msg, ma_msgbus_client_request_t *c_request) {
	if(service && task_req_msg && c_request) {
		ma_error_t rc = MA_OK;
		const char *custom_prop = NULL;
		const char *reply_status = MA_IO_REPLY_STATUS_SUCCESS;
		ma_message_t *response = NULL ;
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(service->context);
		ma_configurator_t *configurator = MA_CONTEXT_GET_CONFIGURATOR(service->context);
		ma_ds_t *ds = ma_configurator_get_datastore(configurator);

		if( (MA_OK == ma_message_get_property(task_req_msg, MA_CONFIG_CUSTOM_PROP1_STR, &custom_prop)) && custom_prop)
			rc = (ma_error_t) (rc | ma_ds_set_str(ds, MA_PROPERTY_PROPERTIES_PATH_NAME_STR, MA_PROPERTY_PROPERTIES_CUSTOM1_STR, custom_prop, strlen(custom_prop)));
		if( (MA_OK == ma_message_get_property(task_req_msg, MA_CONFIG_CUSTOM_PROP2_STR, &custom_prop)) && custom_prop)
			rc = (ma_error_t) (rc | ma_ds_set_str(ds, MA_PROPERTY_PROPERTIES_PATH_NAME_STR, MA_PROPERTY_PROPERTIES_CUSTOM2_STR, custom_prop, strlen(custom_prop)));
		if( (MA_OK == ma_message_get_property(task_req_msg, MA_CONFIG_CUSTOM_PROP3_STR, &custom_prop)) && custom_prop)
			rc = (ma_error_t) (rc | ma_ds_set_str(ds, MA_PROPERTY_PROPERTIES_PATH_NAME_STR, MA_PROPERTY_PROPERTIES_CUSTOM3_STR, custom_prop, strlen(custom_prop)));
		if( (MA_OK == ma_message_get_property(task_req_msg, MA_CONFIG_CUSTOM_PROP4_STR, &custom_prop)) && custom_prop)
			rc = (ma_error_t) (rc | ma_ds_set_str(ds, MA_PROPERTY_PROPERTIES_PATH_NAME_STR, MA_PROPERTY_PROPERTIES_CUSTOM4_STR, custom_prop, strlen(custom_prop)));
		if(MA_OK != rc){
			reply_status = MA_IO_REPLY_STATUS_FAILED;
			MA_LOG(logger, MA_LOG_SEV_CRITICAL, "io service custom props setting update failed, last error(%d)", rc);
		}

		if(MA_OK == (rc = ma_message_create(&response))){
			ma_message_set_property(response, MA_IO_REPLY_STATUS, reply_status) ;
			rc = ma_msgbus_server_client_request_post_result(c_request, rc, response) ;
			ma_message_release(response);
		}
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_product_license_state_request_handler(ma_io_service_t *self, ma_message_t *msg, ma_msgbus_client_request_t *c_request) {
	if(self && msg && c_request) {
		ma_error_t rc = MA_OK;
		ma_message_t *response =  NULL;
		const char *product_id = NULL;        
		if(MA_OK == (rc = ma_message_create(&response))) {
			if((MA_OK == (rc = ma_message_get_property(msg, MA_PROP_KEY_PRODUCT_ID_STR, &product_id))) && product_id) {     
				ma_variant_t *value = NULL;
				if(MA_OK == (rc = ma_policy_settings_bag_get_variant(MA_CONTEXT_GET_POLICY_SETTINGS_BAG(self->context), MA_PP_LICENSE_POLICY_SECTION_NAME, product_id, MA_TRUE, &value, NULL))) {
					rc = ma_message_set_payload(response, value);
					(void) ma_variant_release(value);
				}
			}

			rc = ma_msgbus_server_client_request_post_result(c_request, rc, response);
			(void) ma_message_release(response);      
		}
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_license_key_set_handler(ma_io_service_t *self, ma_message_t *msg, ma_msgbus_client_request_t *c_request) {
	ma_error_t rc = MA_ERROR_INVALIDARG;
	if(self && msg && c_request) {
		ma_message_t *response =  NULL;        
		if(MA_OK == (rc = ma_message_create(&response))) {
			const char *license_key = NULL;
			if((MA_OK == (rc = ma_message_get_property(msg, MA_PROP_VALUE_LICENSE_KEY_STR, &license_key))) && license_key) {
				ma_policy_bag_t *policy_bag = NULL;
				rc = MA_ERROR_APIFAILED ;
				if(policy_bag = MA_CONTEXT_GET_POLICY_BAG(self->context)) {
					ma_policy_uri_t *repo_uri = NULL ;
					if( MA_OK == (rc = ma_policy_uri_create(&repo_uri)) ) {
						ma_variant_t *value = NULL ;
						(void)ma_policy_uri_set_feature(repo_uri, MA_POLICY_FEATURE_ID);
						(void)ma_policy_uri_set_category(repo_uri, MA_REPO_POLICY_CATEGORY_ID);    
						(void)ma_policy_uri_set_type(repo_uri, MA_REPO_POLICY_TYPE_ID);   
						(void)ma_policy_uri_set_name(repo_uri, MA_POLICY_NAME_ID);      

						if(MA_OK == (rc = ma_variant_create_from_string(license_key, strlen(license_key), &value))) {
							ma_variant_t *enc_lic_key_var = NULL;
							if(MA_OK == encrypt_and_encode_lic_key(self, value, &enc_lic_key_var)) {
								rc = ma_policy_bag_set_value(policy_bag, repo_uri, MA_REPOSITORY_LICENSE_SECTION_NAME_STR, MA_REPOSITORY_KEY_LICENSE_STR, enc_lic_key_var);
								ma_variant_release(enc_lic_key_var);
							}
							if(self->server_settings_tb) 
								ma_table_add_entry(self->server_settings_tb, MA_SERVER_SETTING_LICENSE_KEY_STR, value);
							(void) ma_variant_release(value); value = NULL;
						}
						if(MA_OK == rc) {
							ma_policy_uri_t *agent_uri = NULL;
							if( MA_OK == (rc = ma_policy_uri_create(&agent_uri)) ) {                            
								ma_configurator_ex_t *configurator = (ma_configurator_ex_t*)self->configurator;

								ma_policy_uri_set_software_id(agent_uri, MA_SOFTWAREID_GENERAL_STR) ;
								rc = ma_import_policies(configurator->db_policy, agent_uri, policy_bag) ;
								(void) ma_policy_uri_release(agent_uri);
							}
						}
						(void) ma_policy_uri_release(repo_uri);
					}				
				}
			}
			rc = ma_msgbus_server_client_request_post_result(c_request, rc, response);
			(void) ma_message_release(response);      
		}
	}
	return rc;
}

static ma_error_t ma_acl_request_handler(ma_io_service_t *self, ma_message_t *msg, ma_msgbus_client_request_t *c_request) {
	ma_error_t rc = MA_ERROR_INVALID_REQUEST;
	if(self && msg && c_request) {
		ma_error_t status = MA_OK;
		ma_message_t *response =  NULL;        
		ma_variant_t *payload = NULL;
		if(MA_OK == ma_message_get_payload(msg, &payload)) {
			ma_table_t *acl_tb = NULL;
			if(MA_OK == ma_variant_get_table(payload, &acl_tb)) {
				ma_variant_t *value = NULL;
				ma_sid_t sid = MA_SID_UNKNOWN;
				const char *obj_name = NULL, *sid_name = NULL;
				ma_int64_t perm = 0, inheritance = 0;
				ma_bool_t sid_allow = MA_TRUE, allow_inheritance = MA_TRUE, is_directory = MA_TRUE;

				if(MA_OK == (rc = ma_table_get_value(acl_tb, MA_ACL_SID_INT, &value))) {
					(void)ma_variant_get_int8(value, (ma_int8_t *)&sid);
					(void)ma_variant_release(value); value = NULL;
				}

				if(MA_OK == (rc = ma_table_get_value(acl_tb, MA_ACL_SID_NAME_STR, &value))) { 
					ma_buffer_t *buffer = NULL;
					if(MA_OK == (rc = ma_variant_get_string_buffer(value, &buffer))) {
						size_t size = 0;
						(void)ma_buffer_get_string(buffer, &sid_name, &size);
						(void) ma_buffer_release(buffer); buffer = NULL;
					}
					ma_variant_release(value); value = NULL;
				}

				if(MA_OK == (rc = ma_table_get_value(acl_tb, MA_ACL_SID_ALLOW_BOOL, &value))) {
					(void)ma_variant_get_bool(value, &sid_allow);
					(void)ma_variant_release(value); value = NULL;
				}

				if(MA_OK == (rc = ma_table_get_value(acl_tb, MA_ACL_OBJECT_NAME_STR, &value))) { 
					ma_buffer_t *buffer = NULL;
					if(MA_OK == (rc = ma_variant_get_string_buffer(value, &buffer))) {
						size_t size = 0;
						(void)ma_buffer_get_string(buffer, &obj_name, &size);
						(void)ma_buffer_release(buffer); buffer = NULL;
					}
					ma_variant_release(value); value = NULL;
				}

				if(MA_OK == (rc = ma_table_get_value(acl_tb, MA_ACL_OBJECT_PERMISSIONS_INT, &value))) {
					(void)ma_variant_get_int64(value, &perm);
					(void)ma_variant_release(value); value = NULL;
				}

				if(MA_OK == (rc = ma_table_get_value(acl_tb, MA_ACL_OBJECT_INHERITANCE_INT, &value))) {
					(void)ma_variant_get_int64(value, &inheritance);
					(void)ma_variant_release(value); value = NULL;
				}

				if(MA_OK == (rc = ma_table_get_value(acl_tb, MA_ACL_OBJECT_INHERITANCE_BOOL, &value))) {
					(void)ma_variant_get_bool(value, &allow_inheritance);
					(void)ma_variant_release(value); value = NULL;
				}

				if(MA_OK == (rc = ma_table_get_value(acl_tb, MA_ACL_OBJECT_IS_DIRCTORY_BOOL, &value))) {
					(void)ma_variant_get_bool(value, &is_directory);
					(void)ma_variant_release(value); value = NULL;
				}

				if(MA_SID_UNKNOWN != sid && sid_name && obj_name) {
					uv_loop_t *uv_loop = ma_event_loop_get_uv_loop(ma_msgbus_get_event_loop(self->msgbus));
					ma_acl_t *acl = NULL;
					if(MA_OK == (rc = ma_acl_create(sid, sid_name, &acl))) {
						if(is_directory) {
							if(ma_fs_remove_dir(uv_loop, (char *)obj_name))
								ma_fs_make_recursive_dir(uv_loop, NULL, (char *)obj_name);
						}
						else {
							if(ma_fs_remove_file(uv_loop, obj_name))
								ma_fs_create_file(uv_loop, obj_name);
						}

						if(ma_fs_is_file_exists(uv_loop, obj_name)) {
							if(MA_OK == (rc = ma_acl_add(acl, sid_allow, perm, inheritance))) 
								rc = ma_acl_attach_object(acl, obj_name, MA_ACL_OBJECT_FILE, allow_inheritance);
						}
						else
							rc = MA_ERROR_UNEXPECTED;

						(void) ma_acl_release(acl); acl = NULL;
					}

					if(MA_OK == ma_message_create(&response)) {
						(void) ma_message_set_property(response, MA_DESTINATION_FOLDER_NAME_STR, obj_name);
						(void) ma_msgbus_server_client_request_post_result(c_request, rc, response);
						(void) ma_message_release(response);      
					}
				}
				else
					rc = MA_ERROR_INVALID_REQUEST;

				(void) ma_table_release(acl_tb);
			}
			(void) ma_variant_release(payload);
		}
	}
	return rc;
}

static ma_error_t ma_file_move_request_handler(ma_io_service_t *self, ma_message_t *msg, ma_msgbus_client_request_t *c_request) {
	ma_error_t rc = MA_ERROR_INVALID_REQUEST;
	if(self && msg && c_request) {
		ma_error_t status = MA_OK;        
		const char *source = NULL, *dest = NULL, *base_dir = NULL;
		uv_loop_t *loop = ma_event_loop_get_uv_loop(ma_msgbus_get_event_loop(self->msgbus)) ;

		(void) ma_message_get_property(msg, MA_FILE_MOVE_SOURCE_STR, &source);
		(void) ma_message_get_property(msg, MA_FILE_MOVE_DESTINATION_STR, &dest);
		(void) ma_message_get_property(msg, MA_FILE_MOVE_BASE_DIR_STR, &base_dir);

		if(source && dest && base_dir) {
			ma_message_t *response =  NULL;
			ma_bool_t b_rc = MA_FALSE;
			uv_loop_t *uv_loop = ma_event_loop_get_uv_loop(ma_msgbus_get_event_loop(self->msgbus));            
			ma_fs_make_recursive_dir(uv_loop, NULL, (char *)base_dir);
			if(b_rc = ma_fs_is_file_exists(uv_loop, base_dir)) {
				if(MA_FALSE == (b_rc = ma_fs_move_file(uv_loop, source, dest))) {
					uv_err_t uv_err = uv_last_error(loop) ;
					MA_LOG(self->logger, MA_LOG_SEV_ERROR, "File move request failed, source(%s) to dest(%s) failed - libuv err: (\'%s\' %s)", source, dest, uv_strerror(uv_err),  uv_err_name(uv_err)) ;
				}
			}
			else {
				MA_LOG(self->logger, MA_LOG_SEV_ERROR, "File move request failed, base(%s) directory doesn't exists", base_dir) ;
			}

			if(MA_OK == ma_message_create(&response)) {         
				(void) ma_message_set_property(response, MA_DESTINATION_FOLDER_NAME_STR, dest);
				(void) ma_msgbus_server_client_request_post_result(c_request, (b_rc) ? MA_OK : MA_ERROR_UNEXPECTED, response);
				(void) ma_message_release(response);      
			}
			rc = MA_OK;
		}
	}
	return rc;
}

static ma_error_t ma_firewall_rules_handler(ma_io_service_t *self, ma_message_t *msg, ma_msgbus_client_request_t *c_request, ma_message_t *response) {
	if(self && msg && c_request) {
		ma_error_t rc = MA_OK;
		ma_db_t *policy_db =  ma_configurator_get_policies_database(self->configurator);
		/* 
		ma_temp_buffer_t server_ip_list = MA_TEMP_BUFFER_INIT, server_fqdn_list = MA_TEMP_BUFFER_INIT;	
		ma_db_t *db =  ma_configurator_get_database(self->configurator); 
		*/
		const char *tcp_port = "8081", *udp_port = "8082";			

		/*(void) ma_sitelist_get_server_list(db, MA_SERVER_LIST_BY_IP, &server_ip_list);
		(void) ma_sitelist_get_server_list(db, MA_SERVER_LIST_BY_FQDN, &server_fqdn_list);
		*/
			 
		(void) ma_policy_settings_bag_get_str(self->policy_settings_bag, 
                                              MA_HTTP_SERVER_SERVICE_SECTION_NAME_STR, 
                                              MA_HTTP_SERVER_KEY_PORT_INT, MA_FALSE,
                                              &tcp_port, tcp_port);
		(void) ma_policy_settings_bag_get_str(self->policy_settings_bag, MA_UDP_SERVICE_SECTION_NAME_STR, MA_UDP_KEY_BROADCAST_PORT_INT, MA_FALSE, &udp_port, udp_port) ;

		if(MA_OK != (rc = ma_firewall_exceptions_add_dynamic_rules(self->configurator, tcp_port, udp_port)))
			MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to add MA firewall dynamic rule rc <%d>", rc);

		/*ma_temp_buffer_uninit(&server_ip_list);
		ma_temp_buffer_uninit(&server_fqdn_list);	*/
		
		if(MA_OK == rc)
			(void) ma_msgbus_server_client_request_post_result(c_request, MA_OK, response);
		
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}
