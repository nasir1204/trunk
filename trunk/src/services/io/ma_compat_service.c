#include "ma_compat_service.h"
#include "ma/internal/services/ma_service.h"

#include "ma/ma_log.h"
#include "ma/internal/utils/context/ma_context.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/defs/ma_compat_defs.h"
#include "ma/internal/utils/service/ma_agent_service_helper.h"

#include "ma/ma_datastore.h"
#include "ma/internal/utils/time/ma_time.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include "ma/internal/services/ma_policy_settings_bag.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include "ma/internal/services/ma_policy_settings_bag.h"
#include "ma/repository/ma_repository_list.h"
#include "ma/repository/ma_repository.h"
#include "ma/repository/ma_repository_client_defs.h"
#include "ma/internal/utils/repository/ma_repository_private.h"
#include "ma/datastore/ma_ds_registry.h"
#include "ma/internal/services/event/ma_event_spipe_manager.h"
#include "ma/internal/utils/conf/ma_conf.h"
#include "ma/datastore/ma_ds.h"
#include "ma/datastore/ma_ds_ini.h"

#include "ma/internal/utils/xml/ma_xml.h"
#include "ma/internal/utils/xml/ma_xml_node.h"

/* TODO - it should be on demand , need to put watchers back */
#include <string.h>
#include <stdio.h>

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME   "compatservice"

#include <stdlib.h>

#ifndef MA_WINDOWS
#include <fcntl.h> 
#include <sys/stat.h>
#endif

struct ma_compat_service_s {
    ma_service_t                base ;

    ma_context_t                *context;

    
    char                        guid[UUID_LEN_TXT];

	char                        last_asci[MA_MAX_TIME_LEN];

	char                        last_update_check[MA_MAX_TIME_LEN];

    char                        last_policy_update[MA_MAX_TIME_LEN];

	char                        *epo_server_list;

	char                        lang_id[16];

	
	ma_bool_t                   b_super_agent_enabled;

	ma_bool_t                   b_relay_enabled;

	ma_bool_t                   b_p2p_enabled;

	ma_bool_t                   b_lang_override;

	ma_bool_t                   b_allow_update;

	ma_int32_t                  policy_enforcement_interval;

	ma_int32_t                  asci_interval;

	ma_int16_t                  agent_mode;

};

static ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint);

static ma_error_t service_start(ma_service_t *service);

static ma_error_t service_stop(ma_service_t *service);

static void service_destroy(ma_service_t *service);

static const ma_service_methods_t methods = {
    &service_configure,
    &service_start,
    &service_stop,
    &service_destroy
};	


ma_error_t ma_compat_service_create(char const *service_name, ma_service_t **compat_service) {
    if(service_name && compat_service) {
        ma_error_t rc = MA_OK;
        ma_compat_service_t *self = (ma_compat_service_t *)calloc(1, sizeof(ma_compat_service_t));
        
        if(!self)   return MA_ERROR_OUTOFMEMORY;
        	
        ma_service_init((ma_service_t *)self, &methods, service_name, self);

        *compat_service = &self->base;

        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


static ma_error_t ma_compat_registry_refresh_information(ma_compat_service_t *self, unsigned hint);
static ma_error_t ma_compat_agent_conf_file(ma_compat_service_t *self, unsigned hint);

ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint){
    if(service && context) {
        ma_ds_t *ds = NULL;
        ma_compat_service_t *self = (ma_compat_service_t *)service;
        self->context = context;       
        ma_compat_registry_refresh_information(self, hint);
		ma_compat_agent_conf_file(self, hint);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}		



ma_error_t service_start(ma_service_t *service) {
    if(service) {
        ma_compat_service_t *self = (ma_compat_service_t *)service;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);
        ma_error_t rc = MA_OK;
        ma_bool_t is_running = MA_FALSE;

#ifdef MA_WINDOWS
		if(MA_OK == (rc = ma_agent_service_is_running(MA_AGENT_COMPAT_SERVICE_NAME_STR, &is_running))) {
            if(MA_FALSE == is_running) {
                if(MA_OK == (rc = ma_agent_service_start(MA_AGENT_COMPAT_SERVICE_NAME_STR, MA_FALSE))) {
                    ma_ds_t *ds = NULL;
			        if(MA_OK == ma_ds_registry_open(HKEY_LOCAL_MACHINE, SZ_COMPAT_TVD_FRAMEWORK_REG_PATH, MA_DS_REGISTRY_32BIT_VIEW, MA_TRUE, KEY_ALL_ACCESS, (ma_ds_registry_t **) &ds)) {
				        ma_ds_set_str(ds, "", MA_AGENT_SERVICE_STATE, "1", strlen("1"));
				        ma_ds_release(ds);
			        }
		            MA_LOG(logger, MA_LOG_SEV_DEBUG, "Started agent compatibility service.") ;
                }
                else 
                    MA_LOG(logger, MA_LOG_SEV_CRITICAL, "Failed to start agent compatibility service %d, continuing without it.", rc) ;
            }
            else
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "Agent compatibility service is already in running state.");
        }
        else 
            MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to get compatibility service status,%d", rc);
#endif
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t service_stop(ma_service_t *service) {
    if(service) {
        ma_compat_service_t *self = (ma_compat_service_t *)service;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);
        ma_error_t rc = MA_OK;
        ma_bool_t is_running = MA_FALSE;

#ifdef MA_WINDOWS
        if(MA_OK == (rc = ma_agent_service_is_running(MA_AGENT_COMPAT_SERVICE_NAME_STR, &is_running))) {
            if(MA_TRUE == is_running) {
                if(MA_OK == (rc = ma_agent_service_stop(MA_AGENT_COMPAT_SERVICE_NAME_STR, MA_FALSE))) {
    		        ma_ds_t *ds = NULL;
			        if(MA_OK == ma_ds_registry_open(HKEY_LOCAL_MACHINE, SZ_COMPAT_TVD_FRAMEWORK_REG_PATH, MA_DS_REGISTRY_32BIT_VIEW, MA_TRUE, KEY_ALL_ACCESS, (ma_ds_registry_t **) &ds)) {
				        ma_ds_set_str(ds, "", MA_AGENT_SERVICE_STATE, "0", strlen("0"));
				        ma_ds_release(ds);
			        }
		            MA_LOG(logger, MA_LOG_SEV_DEBUG, "Stopped agent compatibility service.") ;
                }
                else 
                    MA_LOG(logger, MA_LOG_SEV_CRITICAL, "Failed to stop agent compatibility service %d.", rc) ;
            }
            else
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "Agent compatibility service is already in stopped state.");
        }
        else 
            MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to get compatibility service status,%d", rc);
#endif
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

void service_destroy(ma_service_t *service) {
    if(service) {
        ma_compat_service_t *self = (ma_compat_service_t *)service;		
		#ifdef MA_WINDOWS
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);
        ma_error_t rc = MA_OK;
        
		if(MA_OK == (rc = ma_agent_service_stop(MA_AGENT_COMPAT_SERVICE_NAME_STR, MA_TRUE))) {    		        
			MA_LOG(logger, MA_LOG_SEV_DEBUG, "Stopped agent compatibility service.") ;
		}
        else 
            MA_LOG(logger, MA_LOG_SEV_CRITICAL, "Failed to stop agent compatibility service %d.", rc) ;                    
		#endif
        free(self) ;
    }   
}



static void ma_compat_registry_append_epo_server_list_information(ma_temp_buffer_t *tmp, const char *string) {
	if(tmp && string) {
		size_t buffer_size = strlen((char*) ma_temp_buffer_get(tmp));
		size_t string_len = strlen(string);
		ma_temp_buffer_reserve(tmp, buffer_size + string_len +1);
		MA_MSC_SELECT(_snprintf, snprintf)((char*) ma_temp_buffer_get(tmp), buffer_size + string_len + 1, "%s%s", (char*) ma_temp_buffer_get(tmp), string);
	}
}

#ifdef MA_WINDOWS
static ma_error_t ma_compat_registry_service_update_epo_list_to_registry(ma_compat_service_t *self) {
	ma_ds_t *reg_ds = NULL;
	ma_error_t rc = MA_OK;
	
    if(MA_OK == (rc = ma_ds_registry_open(HKEY_LOCAL_MACHINE, SZ_COMPAT_TVD_FRAMEWORK_REG_PATH, MA_DS_REGISTRY_32BIT_VIEW, MA_TRUE, KEY_ALL_ACCESS, (ma_ds_registry_t **)&reg_ds))) {
		ma_ds_set_str(reg_ds, "", MA_AGENT_ABOUT_EPO_SERVER_LIST, self->epo_server_list, strlen(self->epo_server_list));
		ma_ds_release(reg_ds);  reg_ds = NULL ;
	}
    if(MA_OK == (rc = ma_ds_registry_open(HKEY_LOCAL_MACHINE, SZ_COMPAT_ePO_AGENT_REG_PATH, MA_DS_REGISTRY_32BIT_VIEW, MA_TRUE, KEY_ALL_ACCESS, (ma_ds_registry_t **)&reg_ds))) {
		ma_ds_set_str(reg_ds, "", MA_AGENT_ABOUT_EPO_SERVER_LIST, self->epo_server_list, strlen(self->epo_server_list));
		ma_ds_release(reg_ds); reg_ds = NULL ;
	}

	return rc;
}

static ma_error_t ma_compat_registry_update_epo_list(ma_compat_service_t *self) {
	ma_error_t rc = MA_OK;
	ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context) ;
	ma_repository_list_t *repository_list = NULL ;

	MA_LOG(logger, MA_LOG_SEV_DEBUG, "Getting repository information.") ;

	if(MA_OK == (rc = ma_repository_get_repositories(self->context, MA_REPOSITORY_GET_REPOSITORIES_FILTER_SPIPE, &repository_list))) {
		size_t repo_count = 0, iter = 0 ;
		ma_temp_buffer_t tmp = MA_TEMP_BUFFER_INIT ;

		(void)ma_repository_list_get_repositories_count(repository_list, &repo_count) ;

		for(iter = 0 ; iter < repo_count ; iter++) {
			ma_repository_t *repository = NULL ;
		
			if(MA_OK == ma_repository_list_get_repository_by_index(repository_list, iter, &repository)) {
				const char *dns_name = NULL ;
				const char *ip = NULL ;
				char port_str[16] = {0} ;
				ma_uint32_t ssl_port =0 , non_ssl_port = 0 ;

				(void)ma_repository_get_server_fqdn(repository, &dns_name) ;
				if(dns_name) {
					ma_compat_registry_append_epo_server_list_information(&tmp, dns_name);
				}
				(void)ma_compat_registry_append_epo_server_list_information(&tmp, "|");
				(void)ma_repository_get_server_ip(repository, &ip);
				if(ip) {
					ma_compat_registry_append_epo_server_list_information(&tmp, ip);
				}
				ma_compat_registry_append_epo_server_list_information(&tmp, "|");
				
                (void)ma_repository_get_port(repository, &non_ssl_port);
				(void)ma_repository_get_ssl_port(repository, &ssl_port);

				MA_MSC_SELECT(_snprintf, snprintf)(port_str,  16, "%ld", ssl_port > 0  ? ssl_port : non_ssl_port);

				(void)ma_compat_registry_append_epo_server_list_information(&tmp, port_str);
				
				(void)ma_compat_registry_append_epo_server_list_information(&tmp, ";");
				
				(void)ma_repository_release(repository); repository = NULL ;
			}
		}
		if(self->epo_server_list) free(self->epo_server_list) ; 
		self->epo_server_list = NULL;
		
		self->epo_server_list = strdup((char*) ma_temp_buffer_get(&tmp)) ;

		(void)ma_compat_registry_service_update_epo_list_to_registry(self) ;
		
		MA_LOG(logger, MA_LOG_SEV_DEBUG, "Updated repository information in registry.") ;

		(void)ma_temp_buffer_uninit(&tmp) ;
		(void)ma_repository_list_release(repository_list);
	}
	else {
		MA_LOG(logger, MA_LOG_SEV_WARNING, "Falied to get spipe repositories, <rc = %d>.", rc) ;
	}
	return rc ;
}

static ma_int64_t convert_time_into_backcompat(const char *value) {
    ma_time_t time = {0};
    wchar_t wtime[32] = {0};
    time_t t = -1;
	if(0 == strcmp(value, "0")) return 0;
    sscanf(value,"%04d%02d%02d%02d%02d%02d", &time.year, &time.month, &time.day, &time.hour, &time.minute, &time.second);
    return ma_time_to_time_t(time);
}

static void write_dword_into_registry(const char *path, const char *key, DWORD value) {
    HKEY hKey;
	LSTATUS result = ERROR_SUCCESS;
	if(ERROR_SUCCESS == (result = RegOpenKeyExA(HKEY_LOCAL_MACHINE, path, 0, KEY_WRITE | KEY_WOW64_32KEY, &hKey))) {
        result = RegSetValueExA(hKey, key, 0, REG_DWORD, (unsigned char*)&value, sizeof(DWORD));
        RegCloseKey(hKey);
    }
}
#endif



static ma_error_t ma_compat_registry_refresh_information(ma_compat_service_t *self, unsigned int hint) {
	ma_error_t rc = MA_OK;
    ma_buffer_t *buffer = NULL;
    ma_ds_t *ds = ma_configurator_get_datastore(MA_CONTEXT_GET_CONFIGURATOR(self->context));
    const char *p = NULL;
    size_t size = 0;
    ma_int32_t i = 0;
    ma_bool_t b = MA_FALSE;
	ma_int32_t check_network_message_interval = 0;
    ma_policy_settings_bag_t *policy_bag = MA_CONTEXT_GET_POLICY_SETTINGS_BAG(self->context);
	ma_priority_event_forwarding_policy_t policy = {MA_FALSE, 0, 0, MA_EVENT_SEVERITY_CRITICAL};
	ma_int32_t data = 0;
	ma_temp_buffer_t tmp = MA_TEMP_BUFFER_INIT;
#ifdef MA_WINDOWS
	ma_ds_t *reg_ds = NULL;

    /*Force open in 32 bit mode*/
	if(MA_OK == (rc = ma_ds_registry_open(HKEY_LOCAL_MACHINE, MA_CONFIGURATION_LOCAL_MACHINE_SOFTWARE, MA_TRUE, MA_TRUE, KEY_ALL_ACCESS | KEY_WOW64_32KEY, (ma_ds_registry_t **)&reg_ds))) {        
        if(MA_SERVICE_CONFIG_NEW == hint) {
            if(MA_OK == (rc = ma_ds_get_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_VERSION_STR, &buffer))) {
			    if(MA_OK == (rc = ma_buffer_get_string(buffer, &p, &size))) 
                    ma_ds_set_str(reg_ds, SZ_COMPAT_TVD_FRAMEWORK_REG_KEY, MA_AGENT_ABOUT_VERSION, p, size);
                ma_buffer_release(buffer),buffer = NULL;
            }

            if(MA_OK == (rc = ma_ds_get_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_COMPUTER_NAME_STR, &buffer))) {
			    if(MA_OK == (rc = ma_buffer_get_string(buffer, &p, &size))) 
                    ma_ds_set_str(reg_ds, SZ_COMPAT_TVD_FRAMEWORK_REG_KEY, MA_AGENT_ABOUT_COMPUTER_NAME, p, size);
                ma_buffer_release(buffer),buffer = NULL;
            }
        }
		    
        if(MA_OK == ma_ds_get_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_GUID_STR, &buffer) ) {
			if(MA_OK == (rc = ma_buffer_get_string(buffer, &p, &size))) {
                if(strcmp(p, self->guid)) {
				    strcpy(self->guid, p);
                    ma_ds_set_str(reg_ds, SZ_COMPAT_TVD_FRAMEWORK_REG_KEY, SZ_COMPAT_AGENT_GUID_A, self->guid, size);
                    ma_ds_set_str(reg_ds, SZ_COMPAT_AGENT_REG_KEY, SZ_COMPAT_AGENT_GUID_A, self->guid, size);
                }
            }
            ma_buffer_release(buffer),buffer = NULL;
        }
		
		if(MA_OK == (rc = ma_ds_get_str(ds, MA_PROPERTY_DATA_PATH_NAME_STR, MA_PROPERTY_DATA_KEY_ASC_TIME_STR, &buffer))) {
			if(MA_OK == (rc = ma_buffer_get_string(buffer, &p, &size))) {
                if(strcmp(p, self->last_asci)) {
				    strcpy(self->last_asci, p);
                    ma_ds_set_str(reg_ds, SZ_COMPAT_TVD_FRAMEWORK_REG_KEY, MA_AGENT_ABOUT_LAST_ASCI, self->last_asci, size);
                }
            }
            ma_buffer_release(buffer),buffer = NULL;
        }
        if(MA_OK != rc)  write_dword_into_registry(SZ_COMPAT_ePO_AGENT_REG_PATH, MA_AGENT_LAST_ASCI_TIME_DWORD, 0);

        if(MA_OK == (rc = ma_ds_get_str(ds, MA_POLICY_DATA_PATH_NAME_STR, MA_POLICY_UPDATE_TIME_STR, &buffer))) {
			if(MA_OK == (rc = ma_buffer_get_string(buffer, &p, &size))) {
                if(strcmp(p, self->last_policy_update)) {
				    strcpy(self->last_policy_update, p);
                    write_dword_into_registry(SZ_COMPAT_ePO_AGENT_REG_PATH, MA_AGENT_LAST_POLICY_UPDATE_TIME_DWORD, convert_time_into_backcompat(self->last_policy_update));
                }
            }
            ma_buffer_release(buffer),buffer = NULL;
        }

        if(MA_OK != rc)  write_dword_into_registry(SZ_COMPAT_ePO_AGENT_REG_PATH, MA_AGENT_LAST_POLICY_UPDATE_TIME_DWORD, 0);

		if(MA_OK == (rc = ma_ds_get_str(ds, MA_PROPERTY_DATA_PATH_NAME_STR, MA_AGENT_CONFIGURATION_LAST_UPDATE_STR, &buffer))) {
            if(MA_OK == (rc = ma_buffer_get_string(buffer, &p, &size))) {
                if(strcmp(p, self->last_update_check)) {
				    strcpy(self->last_update_check, p);
                    ma_ds_set_str(reg_ds, SZ_COMPAT_TVD_FRAMEWORK_REG_KEY, MA_AGENT_ABOUT_LAST_UPDATE_CHECK, self->last_update_check, size);
                }
            }
            ma_buffer_release(buffer),buffer = NULL;
        }

		if(MA_OK == (rc = ma_ds_get_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_LANGUAGE_STR, &buffer))) {
			if(MA_OK == (rc = ma_buffer_get_string(buffer, &p, &size))) {
                if(strcmp(p, self->lang_id)) {
					char mctray_p_lang[MA_MAX_LEN] = {0};
					unsigned long mctray_lang_id = strtoul(p, NULL, 16);
					_snprintf(mctray_p_lang, MA_MAX_LEN, "%ld", mctray_lang_id);
				    strcpy(self->lang_id, p);
                    ma_ds_set_str(reg_ds, SZ_COMPAT_TVD_FRAMEWORK_REG_KEY, MA_UPDATER_UI_USE_LANGUAGE, self->lang_id, size);
                    ma_ds_set_str(reg_ds, MA_UPDATER_UI_LOCALIZATION_MCTRAY_PICKUP_PATH, MA_UPDATER_UI_MCTRAY_LANG, mctray_p_lang, size);
                }
			}
			ma_buffer_release(buffer), buffer = NULL;
        }
		
		if(MA_OK == (rc = ma_ds_get_int32(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_MODE_INT, &i))) {
            self->agent_mode = i;
            write_dword_into_registry(SZ_COMPAT_TVD_FRAMEWORK_REG_PATH, MA_CONFIGURATION_KEY_AGENT_MODE_INT, self->agent_mode);
		}

        (void)ma_policy_settings_bag_get_bool(policy_bag, MA_HTTP_SERVER_SERVICE_SECTION_NAME_STR, MA_HTTP_SERVER_KEY_ENABLE_SUPER_AGENT_REPOSITORY_INT, MA_FALSE,  &b, MA_FALSE);
            self->b_super_agent_enabled = b ;
            ma_ds_set_int(reg_ds, SZ_COMPAT_TVD_FRAMEWORK_REG_KEY, MA_AGENT_ABOUT_POLICY_SUPER_AGENT, self->b_super_agent_enabled);

		(void)ma_policy_settings_bag_get_bool(policy_bag, MA_P2P_SERVICE_SECTION_NAME_STR, MA_P2P_KEY_ENABLE_SERVING_INT, MA_FALSE, &b, MA_FALSE);
            self->b_p2p_enabled = b ;
            ma_ds_set_int(reg_ds, SZ_COMPAT_TVD_FRAMEWORK_REG_KEY, MA_AGENT_ABOUT_POLICY_P2P_SERVER, self->b_p2p_enabled);

		(void)ma_policy_settings_bag_get_bool(policy_bag, MA_RELAY_SERVICE_SECTION_NAME_STR, MA_RELAY_KEY_IS_ENABLED_INT, MA_FALSE, &b, MA_FALSE);
            self->b_relay_enabled = b ;
            ma_ds_set_int(reg_ds, SZ_COMPAT_TVD_FRAMEWORK_REG_KEY, MA_AGENT_ABOUT_POLICY_RELAY_SERVICE, self->b_relay_enabled);

		(void)ma_policy_settings_bag_get_int(policy_bag, MA_POLICY_SERVICE_SECTION_NAME_STR, MA_POLICY_KEY_ENFORCEMENT_TIMEOUT_INT, MA_FALSE, &i, 60);
            self->policy_enforcement_interval = i;
            ma_ds_set_int(reg_ds, SZ_COMPAT_TVD_FRAMEWORK_REG_KEY, MA_AGENT_ABOUT_POLICY_ENFORCEMENT_INTERVAL, self->policy_enforcement_interval);

        (void)ma_policy_settings_bag_get_int(policy_bag, MA_PROPERTY_SERVICE_SECTION_NAME_STR, MA_PROPERTY_KEY_COLLECTION_TIMEOUT_INT, MA_FALSE, &i, 60);
		check_network_message_interval = i;
            self->asci_interval = i;
            ma_ds_set_int(reg_ds, SZ_COMPAT_TVD_FRAMEWORK_REG_KEY, MA_AGENT_ABOUT_POLICY_CHECK_NETWORK_MESSAGE_INTERVAL, self->asci_interval);
            
        (void)ma_policy_settings_bag_get_bool(policy_bag, MA_AGENT_GENERAL_SECTION_NAME_STR, MA_UPDATER_UI_PO_SETTING_ALLOW_UPDATE_SECURITY, MA_FALSE, &b, MA_TRUE);
            self->b_allow_update = b;
            ma_ds_set_int(reg_ds, SZ_COMPAT_TVD_FRAMEWORK_REG_KEY, MA_MCTRAY_POLICY_ALLOW_UPDATE, self->b_allow_update);
            
		{
			if (MA_OK == ma_policy_settings_bag_get_int(MA_CONTEXT_GET_POLICY_SETTINGS_BAG(self->context), MA_EVENT_SERVICE_SECTION_NAME_STR, MA_EVENT_KEY_IS_ENABLED_PRIORITY_FORWARD_INT, MA_FALSE, &data, 1)) {
				policy.is_enabled = (data ? MA_TRUE : MA_FALSE);//#define SZ_TVD_SHARED_FRAMEWORK_A                 REG_6432_BASE_PATH_A "Network Associates\\TVD\\Shared Components\\Framework"
				if (policy.is_enabled) {
					if (MA_OK == ma_policy_settings_bag_get_int(MA_CONTEXT_GET_POLICY_SETTINGS_BAG(self->context), MA_EVENT_SERVICE_SECTION_NAME_STR, MA_EVENT_KEY_PRIORITY_LEVEL_INT, MA_FALSE, &data, 3)) {
						if (data <= MA_EVENT_SEVERITY_CRITICAL) {
							policy.event_trigger_threshold = (ma_event_severity_t)data;
						}
					}
				}			
			}

			{
				char szBuffer[MAX_PATH] = {0};
	
				MA_MSC_SELECT(_snprintf, snprintf)(szBuffer, MAX_PATH, "%d", policy.is_enabled);
				ma_ds_set_str(reg_ds, SZ_COMPAT_TVD_FRAMEWORK_REG_KEY, SZ_COMPAT_AGENT_ENABLE_EVENT_TRIGGER, szBuffer, strlen(szBuffer) + 1);

				MA_MSC_SELECT(_snprintf, snprintf)(szBuffer, MAX_PATH, "%d", policy.event_trigger_threshold ? policy.event_trigger_threshold : MA_EVENT_SEVERITY_CRITICAL);
				ma_ds_set_str(reg_ds, SZ_COMPAT_TVD_FRAMEWORK_REG_KEY, SZ_COMPAT_AGENT_ENABLE_EVENT_TRIGGER_THRESHOLD, szBuffer, strlen(szBuffer) + 1);
			}
		}

		{
			const char *p_ip = NULL;
			size_t size = 0;
			ma_buffer_t *buffer = NULL;
			if(MA_OK == ma_ds_get_str(ds, MA_PROPERTY_PROPERTIES_PATH_NAME_STR, SZ_COMPAT_IPADDRESS_A, &buffer)) {
				if(MA_OK == ma_buffer_get_string(buffer, &p_ip, &size)) {
					ma_ds_set_str(reg_ds, SZ_COMPAT_TVD_FRAMEWORK_REG_KEY, SZ_COMPAT_IPADDRESS_A, p_ip, size);
				}
				ma_buffer_release(buffer), buffer = NULL;
			}
		}
		
		rc = ma_compat_registry_update_epo_list(self);
        ma_ds_release(reg_ds);
    }
#endif
	if(MA_OK == ma_conf_get_logs_path(&tmp)) {
		const char *data_path = (const char *)ma_temp_buffer_get(&tmp);
		char ini_name[MA_MAX_PATH_LEN] = {0};
		ma_ds_t *ini = NULL;
		MA_MSC_SELECT(_snprintf, snprintf)(ini_name, MA_MAX_PATH_LEN, "%s%c..%c%s",data_path, MA_PATH_SEPARATOR, MA_PATH_SEPARATOR, "Agent.ini");
		if(MA_OK == ma_ds_ini_open(ini_name, 0, (ma_ds_ini_t **) &ini)) {
			ma_ds_set_int(ini, MA_NETWORK_SECTION_NAME_STR, MA_CHECK_NETWORK_MESSAGE_INTERVAL_KEY_STR, check_network_message_interval*60);
			ma_ds_release(ini);
			ini = NULL;
		}
	}
    return MA_OK;
}

#ifndef MA_WINDOWS

#ifdef HPUX
#define RELOAD_PLUGIN_CMD  "/sbin/init.d/cma reload SOFTWAREID"
#define UNLOAD_PLUGIN_CMD  "/sbin/init.d/cma unload SOFTWAREID"
#else
#ifndef MACX
#define RELOAD_PLUGIN_CMD  "/etc/init.d/cma reload SOFTWAREID"
#define UNLOAD_PLUGIN_CMD  "/etc/init.d/cma unload SOFTWAREID"
#else
#define RELOAD_PLUGIN_CMD  "/Library/StartupItems/cma/cma.sh reload SOFTWAREID"
#define UNLOAD_PLUGIN_CMD  "/Library/StartupItems/cma/cma.sh unload SOFTWAREID"
#endif
#endif 

 /*<?xml version="1.0" encoding="UTF-8"?> 
 <Configuration>  
	   <AgentGUID>{248BC997-A8B6-E311-936F-000000000000}</AgentGUID>
       <CMAVersion>4.6.0</CMAVersion>
       <CMABuildNumber>1694</CMABuildNumber>
       <InstallDirectory>/opt/McAfee/cma</InstallDirectory>
       <PluginBaseDirectory>/etc/cma.d</PluginBaseDirectory>
       <UnloadPluginCommand>/etc/init.d/cma unload SOFTWAREID</UnloadPluginCommand>
       <ReloadPluginCommand>/etc/init.d/cma reload SOFTWAREID</ReloadPluginCommand>
       <IsManaged>1</IsManaged>
 </Configuration>*/

static ma_error_t add_configuration(ma_context_t *context, ma_xml_node_t *conf_node){	
	ma_error_t rc = MA_OK;
	ma_configurator_t *configurator = MA_CONTEXT_GET_CONFIGURATOR(context);
	ma_ds_t *ds = ma_configurator_get_datastore(configurator);
	int mode = ma_configurator_get_agent_mode(configurator);
	ma_xml_node_t *child = NULL;
	ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(context);

	(void)ma_xml_node_create(conf_node, "IsManaged", &child);
	(void)ma_xml_node_set_data(child, mode ? "1" : "0");
	child = NULL;

	(void)ma_xml_node_create(conf_node, "AgentGUID", &child);
	if(mode){
		const char *value = NULL;	
		if(NULL != (value = ma_configurator_get_agent_guid(configurator)))
			(void)ma_xml_node_set_data(child, (char*)value);							
	}
	child = NULL;

	{				
		ma_buffer_t *buffer = NULL;
		if(MA_OK == (rc = ma_ds_get_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_VERSION_STR, &buffer))) {
			const char *p = NULL;
			size_t size = 0;
		    if(MA_OK == (rc = ma_buffer_get_string(buffer, &p, &size))){
				char agent_build[20] = {0};
				char *build_number = NULL;
				MA_MSC_SELECT(_snprintf, snprintf)(agent_build, sizeof(agent_build), "%s", p);

				if(build_number = (char*)strrchr(agent_build, '.')) 
					*build_number = '\0';
				
				(void)ma_xml_node_create(conf_node, "CMAVersion", &child);
				(void)ma_xml_node_set_data(child, (char*)agent_build);					
				child = NULL;

				(void)ma_xml_node_create(conf_node, "CMABuildNumber", &child);
				if(build_number)
					(void)ma_xml_node_set_data(child, (char*)(build_number+1));									                
			}
			ma_buffer_release(buffer);
		}	
		else						
			MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to get agent version.") ;
	}


	(void)ma_xml_node_create(conf_node, "InstallDirectory", &child);
	(void)ma_xml_node_set_data(child, ma_configurator_get_agent_install_path(configurator));					
	child = NULL;

	(void)ma_xml_node_create(conf_node, "PluginBaseDirectory", &child);
	(void)ma_xml_node_set_data(child, "/etc/cma.d");					
	child = NULL;

	(void)ma_xml_node_create(conf_node, "UnloadPluginCommand", &child);
	(void)ma_xml_node_set_data(child, UNLOAD_PLUGIN_CMD);					
	child = NULL;

	(void)ma_xml_node_create(conf_node, "ReloadPluginCommand", &child);
	(void)ma_xml_node_set_data(child, RELOAD_PLUGIN_CMD);					
	child = NULL;

	return rc;
}
#endif

static ma_error_t ma_compat_agent_conf_file(ma_compat_service_t *self, unsigned hint){
#ifndef MA_WINDOWS
	if(self){
		ma_error_t rc = MA_OK;				
		ma_xml_t *conf_xml = NULL;
		ma_xml_node_t *root_node = NULL;				
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);
		ma_configurator_t *configurator = MA_CONTEXT_GET_CONFIGURATOR(self->context);
		const char *guid = ma_configurator_get_agent_guid(configurator);
		int mode = ma_configurator_get_agent_mode(configurator);

		if(MA_SERVICE_CONFIG_NEW == hint || (mode && strcmp(guid, self->guid))){
			strcpy(self->guid, guid);

			if(MA_OK == (rc = ma_xml_create(&conf_xml)) &&
				MA_OK == (rc = ma_xml_construct(conf_xml)) &&
				MA_OK == (rc = ma_xml_node_create(ma_xml_get_node(conf_xml), "Configuration", &root_node))){			

				if(MA_OK == (rc = add_configuration(self->context, root_node))){
					if(MA_OK == (rc = ma_xml_save_to_file(conf_xml, "/etc/cma.conf")))
						chmod("/etc/cma.conf", 0640);
					else						
						MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to save /etc/cma.conf.") ;
				}
				else
					MA_LOG(logger, MA_LOG_SEV_ERROR, "Failed to add agent configuration in /etc/cma.conf.") ;
			}
			(void)ma_xml_release(conf_xml);						
		}
		return rc;
	}
	return MA_ERROR_INVALIDARG;
#endif
	return MA_OK;
}

