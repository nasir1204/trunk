#include "ma/property/ma_property_bag.h"
#include "ma/internal/services/ma_policy_settings_bag.h"
#include "ma/policy/ma_policy_bag.h"

#include "ma/internal/utils/platform/ma_system_property.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"

#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/services/io/ma_registry_updater.h"

#include "ma/internal/clients/ma/ma_client_internal.h"
#include "ma/internal/defs/ma_general_defs.h"
#include "ma/datastore/ma_ds_registry.h"
#include "ma/ma_log.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef MA_WINDOWS
#include <windows.h>
#endif


#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "ioservice"


#define MA_SYS_PROPS_ROOT_PATH                         "ComputerProperties"
#define MA_SYS_PROPS_CPU_TYPE                          "CPUType"
#define MA_SYS_PROPS_CPU_COUNT                         "NumOfCPU"
#define MA_SYS_PROPS_CPU_SPEED                         "CPUSpeed"
#define MA_SYS_PROPS_CPU_SERIAL                        "CPUSerialNumber"
#define MA_SYS_PROPS_MEM_TOTAL                         "TotalPhysicalMemory"
#define MA_SYS_PROPS_MEM_FREE                          "FreeMemory"
#define MA_SYS_PROPS_OS_TYPE                           "OSType"
#define MA_SYS_PROPS_OS_PLATFORM                       "OSPlatform"
#define MA_SYS_PROPS_OS_VERSION                        "OSVersion"
#define MA_SYS_PROPS_OS_BUILD                          "OSBuildNum"
#define MA_SYS_PROPS_OS_CSDVERSION                     "OSCsdVersion"
#define MA_SYS_PROPS_OS_OEMID                          "OSOEMId"
#define MA_SYS_PROPS_OS_PLATFORM_ID                    "PlatformID"
#define MA_SYS_PROPS_OS_BIT_MODE                       "OSBitMode"
#define MA_SYS_PROPS_OS_SMBIOS_UUID                    "SMBiosUUID"
#define MA_SYS_PROPS_MISC_COMPUTER_NAME                "ComputerName"
#define MA_SYS_PROPS_MISC_COMPUTER_DESC                "ComputerDescription"
#define MA_SYS_PROPS_MISC_USER_NAME                    "UserName"
#define MA_SYS_PROPS_MISC_TIMEZONE                     "TimeZone"
#define MA_SYS_PROPS_MISC_LANGUAGE                     "DefaultLangID"
#define MA_SYS_PROPS_MISC_UPDATE_TIME                  "LastUpdate"
#define MA_SYS_PROPS_MISC_DOMAIN_NAME                  "DomainName"
#define MA_SYS_PROPS_MISC_IS_PORTABLE                  "IsPortable"
#define MA_SYS_PROPS_MISC_EMAIL                        "EmailAddress"
#define MA_SYS_PROPS_MISC_IP_HOST_NAME                 "IPHostName"
#define MA_SYS_PROPS_MISC_VID			                "Vdi"
#define MA_SYS_PROPS_DISK_HARD_DRIVE_COUNT             "NumOfHardDrives"
#define MA_SYS_PROPS_DISK_TOTAL_SPACE                  "TotalDiskSpace"
#define MA_SYS_PROPS_DISK_FREE_SPACE                   "FreeDiskSpace"
#define MA_SYS_PROPS_DISK_FS_TOTAL_SPACE               "Total_Space_of_Drive_"
#define MA_SYS_PROPS_DISK_FS_FREE_SPACE                "Free_Space_of_Drive_"
#define MA_SYS_PROPS_NETWORK_HOST_NAME                 "IPHostName"
#define MA_SYS_PROPS_NETWORK_IPX                       "IPXAddress"
#define MA_SYS_PROPS_NETWORK_IP                        "IPAddress"
#define MA_SYS_PROPS_NETWORK_SUBNET_ADDRESS            "SubnetAddress"
#define MA_SYS_PROPS_NETWORK_SUBNET_MASK               "SubnetMask"
#define MA_SYS_PROPS_NETWORK_MAC_ADDRESS               "NETAddress"
#define MA_SYS_PROPS_MANAGEMENT_TYPE                   "ManagementType"



#define MA_PROPERTY_SECTION_NAME_STR                    "General"
#define MA_PROPERTY_INSTALL_PATH                        "szInstallDir"
#define MA_PROPERTY_PLUGIN_VERSION                      "PluginVersion"
#define MA_PROPERTY_LANGUAGE                            "Language"
#define MA_PROPERTY_GUID                                "AgentGUID"
#define MA_PROPERTY_AGENT_VERSION                       "szProductVer"
#define MA_PROPERTY_ENABLE_SA                           "bEnableSuperAgent"
#define MA_PROPERTY_ENABLE_SA_REPO                      "bEnableSuperAgentRepository"
#define MA_PROPERTY_ENABLE_P2P							"bEnableP2P"
#define MA_PROPERTY_ENABLE_P2P_CLIENT					"bEnableP2PClient"
#define MA_PROPERTY_P2P_REPO_PATH						"P2PRepoPath"
#define MA_PROPERTY_VIRTUAL_DIRECTORY                   "VirtualDirectory"
#define MA_PROPERTY_ENABLE_PING                         "bEnableAgentPing"
#define MA_PROPERTY_BROADCAST_PINGPORT                  "AgentBroadcastPingPort"
#define MA_PROPERTY_PINGPORT                            "AgentPingPort"
#define MA_PROPERTY_AGENTUI                             "ShowAgentUI"
#define MA_PROPERTY_REBOOTUI                            "ShowRebootUI"
#define MA_PROPERTY_REBOOT_TIMEOUT                      "RebootTimeOut"
#define MA_PROPERTY_POLICY_ENFORCE_TIMEOUT              "PolicyEnforcementInterval"
#define MA_PROPERTY_NW_MESSAGE_INTERVAL                 "CheckNetworkMessageInterval"
#define MA_PROPERTY_SERVER_KEY_HASH_STR                 "ServerKeyHash"
#define MA_PROPERTY_LAST_PROP_COLLECTION_STATUS	        "PCStatus"
#define MA_PROPERTY_LAST_POLICY_ENFORCEMENT_STATUS	    "PEStatus"
#define MA_PROPERTY_IS_RUN_NOW_SUPPORTED			    "IsRunNowSupported"
#define MA_PROPERTY_ENABLE_RELAY_SERVICE			    "bEnableRelayService"

#define MA_PROPERTY_CLUSTER_NODES                       "ClusterNodes"
#define MA_PROPERTY_CLUSTER_NODE                        "ClusterNode"
#define MA_PROPERTY_CLUSTER_IP_ADDR                     "ClusterIPAddress"
#define MA_PROPERTY_CLUSTER_NAME		                "ClusterName"
#define MA_PROPERTY_CLUSTER_HOST                        "ClusterNodeActingAsHostToCluster"
#define MA_PROPERTY_CLUSTER_QUORUM_PATH                 "ClusterQuorumResourcePath"

#define MA_PROPERTY_PATH_SYSPROPS1000_STR				"AGENT\\SERVICES\\PROPERTY\\DATA\\SYSPROPS1000\\ComputerProperties"

static ma_error_t add_buffer_to_props_bag(ma_property_bag_t *bag, const char *path, const char *name, ma_buffer_t *buffer) {
    ma_error_t rc = MA_OK;
    const char *value = NULL;
    size_t size = 0;
    if(MA_OK == (rc = ma_buffer_get_string(buffer, &value, &size))) {
        ma_variant_t *variant = NULL;
        if(MA_OK == (rc = ma_variant_create_from_string(value, size, &variant))) {
            (void)ma_property_bag_add_property(bag, path, name, variant) ;
            (void)ma_variant_release(variant);
        }
    }
    return rc;
}

static ma_error_t add_string_to_props_bag(ma_property_bag_t *bag, const char *path, const char *name, const char *value) {
    ma_error_t rc = MA_OK;
    ma_variant_t *variant = NULL;
    if(MA_OK == ma_variant_create_from_string(value, strlen(value), &variant)) {
        (void)ma_property_bag_add_property(bag, path, name, variant);
        (void)ma_variant_release(variant);
    }
    return rc;
}

static ma_error_t create_repo_path_string(ma_ds_t *ds, const char *path_suffix, char *path, int path_size) {
	ma_error_t rc = MA_OK;
	if(ds && path_suffix && path && path_size){
		ma_buffer_t *buffer = NULL;
		if(MA_OK == (rc = ma_ds_get_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_DATA_PATH_STR, &buffer))) {
			const char *value = NULL;
			size_t size = 0;
			if(MA_OK == (rc = ma_buffer_get_string(buffer, &value, &size))){
				strncpy(path, value, (path_size - strlen(path_suffix) - 2) );
				strcat(path, path_suffix);
			}
			(void)ma_buffer_release(buffer);
		}
	}
	return rc;
}

ma_error_t agent_property_provider_cb(ma_client_t *client, const char *software_id, ma_property_bag_t *bag, void *cb_data) {
    ma_error_t rc = MA_OK;
    ma_context_t *context = (ma_context_t *)cb_data;
    ma_policy_settings_bag_t *policy_bag = MA_CONTEXT_GET_POLICY_SETTINGS_BAG(context);
    ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(context);
    ma_ds_t *ds = ma_configurator_get_datastore(MA_CONTEXT_GET_CONFIGURATOR(context));
    ma_system_property_t *sysp = MA_CONTEXT_GET_SYSTEM_PROPERTY(context) ;
    const ma_cluster_system_property_t *cluster = ma_system_property_cluster_get(sysp, MA_FALSE);
    ma_buffer_t *buffer = NULL;
	ma_int32_t is_cloud = 0;

    if(!policy_bag || !ds || !sysp) {
        MA_LOG(logger, MA_LOG_SEV_ERROR, "Precondition for collecting agent properties failed.") ;
        return MA_ERROR_PRECONDITION;
    }

    MA_LOG(logger, MA_LOG_SEV_DEBUG, "Adding agent properties to property bag") ;

    /* install path */
    if(MA_OK == (rc = ma_ds_get_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_INSTALL_PATH_STR, &buffer))) {
        rc = add_buffer_to_props_bag(bag, MA_PROPERTY_SECTION_NAME_STR, MA_PROPERTY_INSTALL_PATH, buffer);
        ma_buffer_release(buffer);
    }else{
		MA_LOG(logger, MA_LOG_SEV_ERROR, "Adding agent install path properties to property bag failed") ;
	}

    /* Language */  
    if((MA_OK == (rc = ma_ds_get_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_LANGUAGE_STR, &buffer)))) {
        rc = add_buffer_to_props_bag(bag, MA_PROPERTY_SECTION_NAME_STR, MA_PROPERTY_LANGUAGE, buffer);
        ma_buffer_release(buffer);
    }else{
		MA_LOG(logger, MA_LOG_SEV_ERROR, "Adding agent language properties to property bag failed") ;
	}

    /* guid */
    if((MA_OK == (rc = ma_ds_get_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_GUID_STR, &buffer)))) {
        add_buffer_to_props_bag(bag, MA_PROPERTY_SECTION_NAME_STR, MA_PROPERTY_GUID, buffer);
        ma_buffer_release(buffer);
    }else{
		MA_LOG(logger, MA_LOG_SEV_ERROR, "Adding agent guid properties to property bag failed") ;
	}

    /* agent & plug-in version */
    if((MA_OK == (rc = ma_ds_get_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_VERSION_STR, &buffer)))) {
        add_buffer_to_props_bag(bag, MA_PROPERTY_SECTION_NAME_STR, MA_PROPERTY_AGENT_VERSION, buffer);
        add_buffer_to_props_bag(bag, MA_PROPERTY_SECTION_NAME_STR, MA_PROPERTY_PLUGIN_VERSION, buffer);
        ma_buffer_release(buffer);
    }else{
		MA_LOG(logger, MA_LOG_SEV_ERROR, "Adding agent version and plug-in properties to property bag failed") ;
	}

    /* Server key hash */	
    if((MA_OK == (rc = ma_ds_get_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_SERVER_KEY_HASH_STR, &buffer)))) {
        add_buffer_to_props_bag(bag, MA_PROPERTY_SECTION_NAME_STR, MA_PROPERTY_SERVER_KEY_HASH_STR, buffer);
        ma_buffer_release(buffer);
    }else{
		MA_LOG(logger, MA_LOG_SEV_ERROR, "Adding agent key hash properties to property bag failed") ;
	}

	/* enable P2P Server*/
    if( (MA_OK == (rc = ma_policy_settings_bag_get_buffer(policy_bag, MA_P2P_SERVICE_SECTION_NAME_STR, MA_P2P_KEY_ENABLE_SERVING_INT, MA_TRUE, &buffer, NULL)))) {
        add_buffer_to_props_bag(bag, MA_PROPERTY_SECTION_NAME_STR, MA_PROPERTY_ENABLE_P2P, buffer);
        ma_buffer_release(buffer);
    }else{
		MA_LOG(logger, MA_LOG_SEV_DEBUG, "Adding enable p2p server properties to property bag failed") ;
	}

	/* enable P2P Client*/
	if( (MA_OK == (rc = ma_policy_settings_bag_get_buffer(policy_bag, MA_P2P_SERVICE_SECTION_NAME_STR, MA_P2P_KEY_ENABLE_CLIENT_INT, MA_TRUE, &buffer, NULL)))) {
        add_buffer_to_props_bag(bag, MA_PROPERTY_SECTION_NAME_STR, MA_PROPERTY_ENABLE_P2P_CLIENT, buffer);
        ma_buffer_release(buffer);
    }else{
		MA_LOG(logger, MA_LOG_SEV_DEBUG, "Adding enable p2p client properties to property bag failed") ;
	}

	/* P2P content directory */
    if((MA_OK == (rc = ma_policy_settings_bag_get_buffer(policy_bag, MA_P2P_SERVICE_SECTION_NAME_STR, MA_P2P_KEY_REPO_PATH_STR, MA_TRUE, &buffer, NULL)))) {
		const char *value = NULL;
		size_t size = 0;
		char path[1024] = {0};
		#ifdef MA_WINDOWS
			#define MA_P2P_CONTENT_BASE_DIR								   "data\\McAfeeP2P"
		#else
			#define MA_P2P_CONTENT_BASE_DIR								   "/data/McAfeeP2P"
		#endif
		if( (MA_OK == (rc = ma_buffer_get_string(buffer, &value, &size))) && (!strcmp(value, "DEFAULT")) &&
					(MA_OK == (rc = create_repo_path_string(ds, MA_P2P_CONTENT_BASE_DIR, path, 1024))) ) {
			add_string_to_props_bag(bag, MA_PROPERTY_SECTION_NAME_STR, MA_PROPERTY_P2P_REPO_PATH,path);
		}else{
			add_buffer_to_props_bag(bag, MA_PROPERTY_SECTION_NAME_STR, MA_PROPERTY_P2P_REPO_PATH, buffer);
		}
        ma_buffer_release(buffer);
    }else{
		MA_LOG(logger, MA_LOG_SEV_DEBUG, "Adding agent p2p repo path properties to property bag failed") ;
	}
    
#ifdef MA_WINDOWS
	/* Agent UI */
    if((MA_OK == (rc = ma_policy_settings_bag_get_buffer(policy_bag, MA_AGENT_GENERAL_SECTION_NAME_STR, MA_AGENT_KEY_SHOW_AGENT_UI_INT, MA_TRUE, &buffer, NULL)))) {
        add_buffer_to_props_bag(bag, MA_PROPERTY_SECTION_NAME_STR, MA_PROPERTY_AGENTUI, buffer);
        ma_buffer_release(buffer);
    }else{
		MA_LOG(logger, MA_LOG_SEV_DEBUG, "Adding agent UI to property bag failed") ;
	}

	/* Agent Reboot UI */
	if((MA_OK == (rc = ma_policy_settings_bag_get_buffer(policy_bag, MA_AGENT_GENERAL_SECTION_NAME_STR, MA_AGENT_KEY_SHOW_REBOOT_UI_INT, MA_TRUE, &buffer, NULL)))) {
        add_buffer_to_props_bag(bag, MA_PROPERTY_SECTION_NAME_STR, MA_PROPERTY_REBOOTUI, buffer);
        ma_buffer_release(buffer);
    }else{
		MA_LOG(logger, MA_LOG_SEV_DEBUG, "Adding agent reboot UI properties to property bag failed") ;
	}
#else
	/*Non-windows. It should disabled */
	/* Agent UI */
	if(MA_OK == ma_buffer_create(&buffer, 1)){
		ma_buffer_set(buffer, "N/A", 3);
		add_buffer_to_props_bag(bag, MA_PROPERTY_SECTION_NAME_STR, MA_PROPERTY_AGENTUI, buffer);
		ma_buffer_release(buffer);
	}else{
		MA_LOG(logger, MA_LOG_SEV_DEBUG, "Adding agent UI to property bag failed") ;
	}
	/* Agent Reboot UI */
	if(MA_OK == ma_buffer_create(&buffer, 1)){
		ma_buffer_set(buffer, "N/A", 3);
		add_buffer_to_props_bag(bag, MA_PROPERTY_SECTION_NAME_STR, MA_PROPERTY_REBOOTUI, buffer);
		ma_buffer_release(buffer);
	}else{
		MA_LOG(logger, MA_LOG_SEV_DEBUG, "Adding agent reboot UI properties to property bag failed") ;
	}
#endif

    /* Agent Reboot timeout */
    if((MA_OK == (rc = ma_policy_settings_bag_get_buffer(policy_bag, MA_AGENT_GENERAL_SECTION_NAME_STR, MA_AGENT_KEY_REBOOT_TIMEOUT_INT, MA_TRUE,&buffer, NULL)))) {
        add_buffer_to_props_bag(bag, MA_PROPERTY_SECTION_NAME_STR, MA_PROPERTY_REBOOT_TIMEOUT, buffer);
        ma_buffer_release(buffer);
    }else{
		MA_LOG(logger, MA_LOG_SEV_DEBUG, "Adding agent reboot timeout properties to property bag failed") ;
	}

    /* policy enforcement timeout */
    if((MA_OK == (rc = ma_policy_settings_bag_get_buffer(policy_bag, MA_POLICY_SERVICE_SECTION_NAME_STR, MA_POLICY_KEY_ENFORCEMENT_TIMEOUT_INT, MA_TRUE, &buffer, NULL)))) {
        add_buffer_to_props_bag(bag, MA_PROPERTY_SECTION_NAME_STR, MA_PROPERTY_POLICY_ENFORCE_TIMEOUT, buffer);
        ma_buffer_release(buffer);
    }else{
		MA_LOG(logger, MA_LOG_SEV_DEBUG, "Adding agent policy enforcement properties to property bag failed") ;
	}

    /* property collection interval/timeout */
    if((MA_OK == (rc = ma_policy_settings_bag_get_buffer(policy_bag, MA_PROPERTY_SERVICE_SECTION_NAME_STR, MA_PROPERTY_KEY_COLLECTION_TIMEOUT_INT, MA_TRUE, &buffer, NULL)))) {
        add_buffer_to_props_bag(bag, MA_PROPERTY_SECTION_NAME_STR, MA_PROPERTY_NW_MESSAGE_INTERVAL, buffer);
        ma_buffer_release(buffer);
    }else{
		MA_LOG(logger, MA_LOG_SEV_DEBUG, "Adding agent property collection timeout properties to property bag failed") ;
	}

    /* Last Property Collection status */
    if((MA_OK == (rc = ma_ds_get_str(ds, MA_PROPERTY_PROPERTIES_PATH_NAME_STR, MA_PROPERTY_PROPERTIES_KEY_LAST_COLLECTION_STATUS, &buffer)))) {
        add_buffer_to_props_bag(bag, MA_PROPERTY_SECTION_NAME_STR, MA_PROPERTY_LAST_PROP_COLLECTION_STATUS, buffer);
        ma_buffer_release(buffer);
    }else{
		MA_LOG(logger, MA_LOG_SEV_ERROR, "Adding agent last property collection properties to property bag failed") ;
	}

	/* Last Policy Enforcement status */
    if((MA_OK == (rc = ma_ds_get_str(ds, MA_PROPERTY_PROPERTIES_PATH_NAME_STR, MA_PROPERTY_PROPERTIES_KEY_LAST_POLICY_ENFORCEMENT_STATUS, &buffer)))) {
        add_buffer_to_props_bag(bag, MA_PROPERTY_SECTION_NAME_STR, MA_PROPERTY_LAST_POLICY_ENFORCEMENT_STATUS, buffer);
        ma_buffer_release(buffer);
    }else{
		MA_LOG(logger, MA_LOG_SEV_DEBUG, "Adding agent last policy enforcement properties to property bag failed") ;
	}

    /* SMBiosUUID */   
	if((MA_OK == (rc = ma_ds_get_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_SMBIOS_UUID_STR, &buffer)))) {
        add_buffer_to_props_bag(bag, MA_PROPERTY_SECTION_NAME_STR, MA_SYS_PROPS_OS_SMBIOS_UUID, buffer);
        ma_buffer_release(buffer);
    }else{
		MA_LOG(logger, MA_LOG_SEV_DEBUG, "Adding agent last policy enforcement properties to property bag failed") ;
	}

    /* Cluster Details */   
    if(cluster) {
        /* On the basis of these cluster properties (one off) we cannot mark the whole properties wrong */
        (void)add_string_to_props_bag(bag, MA_PROPERTY_SECTION_NAME_STR, MA_PROPERTY_CLUSTER_NODE, cluster->state);
        if(atoi(cluster->state) == 1){
            (void)add_string_to_props_bag(bag, MA_PROPERTY_SECTION_NAME_STR, MA_PROPERTY_CLUSTER_IP_ADDR, cluster->ipaddr);
            (void)add_string_to_props_bag(bag, MA_PROPERTY_SECTION_NAME_STR, MA_PROPERTY_CLUSTER_NAME, cluster->name);
            (void)add_string_to_props_bag(bag, MA_PROPERTY_SECTION_NAME_STR, MA_PROPERTY_CLUSTER_HOST, cluster->host);
            (void)add_string_to_props_bag(bag, MA_PROPERTY_SECTION_NAME_STR, MA_PROPERTY_CLUSTER_QUORUM_PATH, cluster->quorum_resource_path);
            (void)add_string_to_props_bag(bag, MA_PROPERTY_SECTION_NAME_STR, MA_PROPERTY_CLUSTER_NODES, cluster->nodes);
        }
    }

	/* Do not send these properties, if agent is reporting to cloud */
	if( ! (((MA_OK == ma_policy_settings_bag_get_int(policy_bag, MA_AGENT_GENERAL_SECTION_NAME_STR, MA_AGENT_KEY_IS_CLOUD_INT, MA_TRUE,&is_cloud,0)) && is_cloud))) {

		/* Agent ping port */
		if( (MA_OK == (rc = ma_policy_settings_bag_get_buffer(policy_bag, MA_HTTP_SERVER_SERVICE_SECTION_NAME_STR, MA_HTTP_SERVER_KEY_PORT_INT, MA_TRUE,&buffer,NULL)))) {
			add_buffer_to_props_bag(bag, MA_PROPERTY_SECTION_NAME_STR, MA_PROPERTY_PINGPORT, buffer);
			ma_buffer_release(buffer);
		}else{
			MA_LOG(logger, MA_LOG_SEV_DEBUG, "Adding agent ping port properties to property bag failed") ;
		}

		/* Agent broadcast port */
		if((MA_OK == (rc = ma_policy_settings_bag_get_buffer(policy_bag, MA_UDP_SERVICE_SECTION_NAME_STR, MA_UDP_KEY_BROADCAST_PORT_INT, MA_TRUE,&buffer,NULL)))) {
			add_buffer_to_props_bag(bag, MA_PROPERTY_SECTION_NAME_STR, MA_PROPERTY_BROADCAST_PINGPORT, buffer);
			ma_buffer_release(buffer);
		}else{
			MA_LOG(logger, MA_LOG_SEV_DEBUG, "Adding agent broadcast port properties to property bag failed") ;
		}

		/* enable super agent */
		if( (MA_OK == (rc = ma_policy_settings_bag_get_buffer(policy_bag, MA_HTTP_SERVER_SERVICE_SECTION_NAME_STR, MA_HTTP_SERVER_KEY_ENABLE_SUPER_AGENT_INT, MA_TRUE,&buffer, NULL)))) {
			add_buffer_to_props_bag(bag, MA_PROPERTY_SECTION_NAME_STR, MA_PROPERTY_ENABLE_SA, buffer);
			ma_buffer_release(buffer);
		}else{
			MA_LOG(logger, MA_LOG_SEV_DEBUG, "Adding enable super agent properties to property bag failed") ;
		}

		/* enable super agent repository*/
		if( (MA_OK == (rc = ma_policy_settings_bag_get_buffer(policy_bag, MA_HTTP_SERVER_SERVICE_SECTION_NAME_STR, MA_HTTP_SERVER_KEY_ENABLE_SUPER_AGENT_REPOSITORY_INT, MA_TRUE,&buffer,NULL)))) {
			add_buffer_to_props_bag(bag, MA_PROPERTY_SECTION_NAME_STR, MA_PROPERTY_ENABLE_SA_REPO, buffer);
			ma_buffer_release(buffer);
		}else{
			MA_LOG(logger, MA_LOG_SEV_DEBUG, "Adding enable super agent properties to property bag failed") ;
		}

		/* Lazy caching enabled */
		if((MA_OK == (rc = ma_policy_settings_bag_get_buffer(policy_bag, MA_HTTP_SERVER_SERVICE_SECTION_NAME_STR, MA_HTTP_SERVER_KEY_ENABLE_LAZY_CACHING_INT, MA_TRUE,&buffer,NULL)))) {
			add_buffer_to_props_bag(bag, MA_PROPERTY_SECTION_NAME_STR, MA_HTTP_SERVER_KEY_ENABLE_LAZY_CACHING_INT, buffer);
			ma_buffer_release(buffer);
		}else{
			MA_LOG(logger, MA_LOG_SEV_DEBUG, "Adding agent lazy catching to property bag failed") ;
		}

		/* virtual directory */
		if((MA_OK == (rc = ma_policy_settings_bag_get_buffer(policy_bag, MA_HTTP_SERVER_SERVICE_SECTION_NAME_STR, MA_HTTP_SERVER_KEY_VIRTUAL_DIRECTORY_STR, MA_TRUE,&buffer,NULL)))) {
			const char *value = NULL;
			size_t size = 0;
			char path[1024] = {0};
			#ifdef MA_WINDOWS
				#define MA_SA_CONTENT_BASE_DIR								   "data\\McAfeeHttp"
			#else
				#define MA_SA_CONTENT_BASE_DIR								   "/data/McAfeeHttp"
			#endif
			if( (MA_OK == (rc = ma_buffer_get_string(buffer, &value, &size))) && (!strcmp(value, "DEFAULT")) &&
				(MA_OK == (rc = create_repo_path_string(ds, MA_SA_CONTENT_BASE_DIR, path, 1024))) ) {
					add_string_to_props_bag(bag, MA_PROPERTY_SECTION_NAME_STR, MA_PROPERTY_VIRTUAL_DIRECTORY,path);
			}else{
				add_buffer_to_props_bag(bag, MA_PROPERTY_SECTION_NAME_STR, MA_PROPERTY_VIRTUAL_DIRECTORY, buffer);
			}
			ma_buffer_release(buffer);

		}else{
			MA_LOG(logger, MA_LOG_SEV_DEBUG, "Adding agent virtual directory properties to property bag failed") ;
		}

		/* Agent ping */
		if( (MA_OK == (rc = ma_policy_settings_bag_get_buffer(policy_bag, MA_HTTP_SERVER_SERVICE_SECTION_NAME_STR, MA_HTTP_SERVER_KEY_ENABLE_AGENT_PING_INT, MA_TRUE,&buffer,NULL)))) {
			add_buffer_to_props_bag(bag, MA_PROPERTY_SECTION_NAME_STR, MA_PROPERTY_ENABLE_PING, buffer);
			ma_buffer_release(buffer);
		}else{
			MA_LOG(logger, MA_LOG_SEV_DEBUG, "Adding agent ping properties to property bag failed") ;
		}

		/* Is run now supported */
		(void)add_string_to_props_bag(bag, MA_PROPERTY_SECTION_NAME_STR, MA_PROPERTY_IS_RUN_NOW_SUPPORTED, "1");
		
	}

    /* enable relay service */
    if((MA_OK == (rc = ma_policy_settings_bag_get_buffer(policy_bag, MA_RELAY_SERVICE_SECTION_NAME_STR, MA_RELAY_KEY_IS_ENABLED_INT, MA_TRUE, &buffer,NULL)))) {
		add_buffer_to_props_bag(bag, MA_PROPERTY_SECTION_NAME_STR, MA_PROPERTY_ENABLE_RELAY_SERVICE, buffer);
		ma_buffer_release(buffer);
	}else{
		MA_LOG(logger, MA_LOG_SEV_DEBUG, "Adding Relay server property to property bag failed") ;
	}

    return MA_OK;
}

static uint64_t get_memory_delta(uint64_t max, uint64_t intervals) {
	return (max / intervals);
}

/*In case of any n/w api failure or not applicable places, n/w library put value as "N/A".
Since ePO fail to parse such string, we double check it before sending*/
/*Make use of this function to add more check (wrt ePO parser)*/
static ma_bool_t network_address_format_check(const char *addr){
	if(addr){
		if(strstr(addr, "N/A"))
			return MA_FALSE;
		else
			return MA_TRUE;
	}
	return MA_FALSE;
}

#define MA_MINIMUM_MEMORY_DELTA 500
ma_error_t system_property_provider_cb(ma_client_t *client, const char *software_id, ma_property_bag_t *bag, void *cb_data) {
    ma_error_t rc = MA_OK;
    ma_context_t *context = (ma_context_t *)cb_data;
    ma_system_property_t *sysp = MA_CONTEXT_GET_SYSTEM_PROPERTY(context) ;
    ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(context);

    if(!sysp) {
        MA_LOG(logger, MA_LOG_SEV_ERROR, "AgentPropertyProvider : precondition for collecting system properties failed.") ;
        return MA_ERROR_PRECONDITION;
    }

	{
        ma_variant_t *value = NULL ;
        ma_buffer_t *buffer = NULL ;
        ma_ds_t *ds = ma_configurator_get_datastore(MA_CONTEXT_GET_CONFIGURATOR(context));
        int fs_count = 0 ;
        ma_temp_buffer_t key_buffer = {0} ;
        
        const ma_cpu_system_property_t *cpu = ma_system_property_cpu_get(sysp, MA_FALSE) ;
        const ma_memory_system_property_t *mem =  ma_system_property_memory_get(sysp, MA_TRUE) ;	
        const ma_os_system_property_t *os = ma_system_property_os_get(sysp, MA_FALSE) ;
        const ma_misc_system_property_t *misc =	ma_system_property_misc_get(sysp, MA_TRUE) ;
        const ma_disk_system_property_t	*disk =	ma_system_property_disk_get(sysp, MA_TRUE) ;
        const ma_network_system_property_t *net = ma_system_property_network_get(sysp, MA_TRUE) ;

        MA_LOG(logger, MA_LOG_SEV_DEBUG, "Adding system properties to property bag") ;

        /** add CPU info ***/
        if(cpu){
            /* cpu type */
            (void)add_string_to_props_bag(bag, MA_SYS_PROPS_ROOT_PATH, MA_SYS_PROPS_CPU_TYPE, cpu->type);
            /* cpu count */
            (void)add_string_to_props_bag(bag, MA_SYS_PROPS_ROOT_PATH, MA_SYS_PROPS_CPU_COUNT, cpu->numbers);
            /* cpu speed */
            (void)add_string_to_props_bag(bag, MA_SYS_PROPS_ROOT_PATH, MA_SYS_PROPS_CPU_SPEED, cpu->speed);
            /* cpu serial */
            (void)add_string_to_props_bag(bag, MA_SYS_PROPS_ROOT_PATH, MA_SYS_PROPS_CPU_SERIAL, cpu->serialnumber);
        }

        /** add memory info **/
        if(mem){	
            /* free memory */
			if( MA_OK == ma_ds_get_str(ds, MA_PROPERTY_PATH_SYSPROPS1000_STR, MA_SYS_PROPS_MEM_FREE, &buffer)) {
				char *str = NULL;
				size_t size = 0;
				if( (MA_OK == (ma_buffer_get_string(buffer, &str, &size))) && str){

					uint64_t mem_diff = abs(MA_MSC_SELECT(_strtoi64,strtoll)(str,NULL,0) - MA_MSC_SELECT(_strtoi64,strtoll)(mem->free_memory,NULL,0) );
					if( (mem_diff < MA_MINIMUM_MEMORY_DELTA) && (mem_diff < get_memory_delta((MA_MSC_SELECT(_strtoi64,strtoll)(mem->total_memory,NULL,0)), 32)) ){
						/* send old status */
						(void)add_string_to_props_bag(bag, MA_SYS_PROPS_ROOT_PATH, MA_SYS_PROPS_MEM_FREE, str);
					}
					else{
						(void)add_string_to_props_bag(bag, MA_SYS_PROPS_ROOT_PATH, MA_SYS_PROPS_MEM_FREE, mem->free_memory);
					}
				}
				else
					(void)add_string_to_props_bag(bag, MA_SYS_PROPS_ROOT_PATH, MA_SYS_PROPS_MEM_FREE, mem->free_memory);
				ma_buffer_release(buffer);
			}
			else
				(void)add_string_to_props_bag(bag, MA_SYS_PROPS_ROOT_PATH, MA_SYS_PROPS_MEM_FREE, mem->free_memory);  

			 /* total memory */
			(void)add_string_to_props_bag(bag, MA_SYS_PROPS_ROOT_PATH, MA_SYS_PROPS_MEM_TOTAL, mem->total_memory);
        }

        /** add OS info **/
        if(os){
            /* OS type */
            (void)add_string_to_props_bag(bag, MA_SYS_PROPS_ROOT_PATH, MA_SYS_PROPS_OS_TYPE, os->type);
            /* OS platform */
            (void)add_string_to_props_bag(bag, MA_SYS_PROPS_ROOT_PATH, MA_SYS_PROPS_OS_PLATFORM, os->platform);
            /* OS version */
            (void)add_string_to_props_bag(bag, MA_SYS_PROPS_ROOT_PATH, MA_SYS_PROPS_OS_VERSION, os->version);
            /* OS build */
            (void)add_string_to_props_bag(bag, MA_SYS_PROPS_ROOT_PATH, MA_SYS_PROPS_OS_BUILD, os->buildnumber);
            /* OS servicepack */
            (void)add_string_to_props_bag(bag, MA_SYS_PROPS_ROOT_PATH, MA_SYS_PROPS_OS_CSDVERSION, os->servicepack);
            /* OS oemid */
            (void)add_string_to_props_bag(bag, MA_SYS_PROPS_ROOT_PATH, MA_SYS_PROPS_OS_OEMID, os->oemid);
            /* OS platform id */
            (void)add_string_to_props_bag(bag, MA_SYS_PROPS_ROOT_PATH, MA_SYS_PROPS_OS_PLATFORM_ID, os->platformid);
            /* OS bit mode */ 
            (void)add_string_to_props_bag(bag, MA_SYS_PROPS_ROOT_PATH, MA_SYS_PROPS_OS_BIT_MODE, os->is64bit);
        }

        /** misc info **/
        if(misc){
            /* computer name */
            (void)add_string_to_props_bag(bag, MA_SYS_PROPS_ROOT_PATH, MA_SYS_PROPS_MISC_COMPUTER_NAME, misc->computername);
            /* computer desc */
            (void)add_string_to_props_bag(bag, MA_SYS_PROPS_ROOT_PATH, MA_SYS_PROPS_MISC_COMPUTER_DESC, misc->computerdesc);
            /* UserName */
            (void)add_string_to_props_bag(bag, MA_SYS_PROPS_ROOT_PATH, MA_SYS_PROPS_MISC_USER_NAME, misc->username);
            /* DomainName */
            (void)add_string_to_props_bag(bag, MA_SYS_PROPS_ROOT_PATH, MA_SYS_PROPS_MISC_DOMAIN_NAME, misc->domainname);
            /* TimeZone */
            (void)add_string_to_props_bag(bag, MA_SYS_PROPS_ROOT_PATH, MA_SYS_PROPS_MISC_TIMEZONE, misc->timezone);
            /* language */
            (void)add_string_to_props_bag(bag, MA_SYS_PROPS_ROOT_PATH, MA_SYS_PROPS_MISC_LANGUAGE, misc->language);
            /* datetime */
            (void)add_string_to_props_bag(bag, MA_SYS_PROPS_ROOT_PATH, MA_SYS_PROPS_MISC_UPDATE_TIME, misc->systemtime);
            /* portable */
            (void)add_string_to_props_bag(bag, MA_SYS_PROPS_ROOT_PATH, MA_SYS_PROPS_MISC_IS_PORTABLE, misc->isportable);
            /* Email */
            (void)add_string_to_props_bag(bag, MA_SYS_PROPS_ROOT_PATH, MA_SYS_PROPS_MISC_EMAIL, "N/A");
            /* ipx address */
            (void)add_string_to_props_bag(bag, MA_SYS_PROPS_ROOT_PATH, MA_SYS_PROPS_NETWORK_IPX, misc->ipx_addr);
        }
		/* vid */
		(void)add_string_to_props_bag(bag, MA_SYS_PROPS_ROOT_PATH, MA_SYS_PROPS_MISC_VID, (ma_configurator_get_vdimode(MA_CONTEXT_GET_CONFIGURATOR(context)) ? "1": "0"));
		
        /** FS **/
        if(disk){
            /* total disk space */
            (void)add_string_to_props_bag(bag, MA_SYS_PROPS_ROOT_PATH, MA_SYS_PROPS_DISK_TOTAL_SPACE, disk->total_diskspace);
            /* number of drives */
            (void)add_string_to_props_bag(bag, MA_SYS_PROPS_ROOT_PATH, MA_SYS_PROPS_DISK_HARD_DRIVE_COUNT, disk->total_filesystems);
            /* total free disk space */
			if( MA_OK == ma_ds_get_str(ds, MA_PROPERTY_PATH_SYSPROPS1000_STR, MA_SYS_PROPS_DISK_FREE_SPACE, &buffer)) {
				char *str = NULL;
				size_t size = 0;
				if( (MA_OK == (ma_buffer_get_string(buffer, &str, &size))) && str){
					uint64_t mem_diff = abs(MA_MSC_SELECT(_strtoi64,strtoll)(str,NULL,0) - MA_MSC_SELECT(_strtoi64,strtoll)(disk->free_diskspace,NULL,0));
				
					if( (mem_diff < MA_MINIMUM_MEMORY_DELTA) && (mem_diff < get_memory_delta((MA_MSC_SELECT(_strtoi64,strtoll)(disk->total_diskspace,NULL,0)), 32)) ){
						/* send old status */
						(void)add_string_to_props_bag(bag, MA_SYS_PROPS_ROOT_PATH, MA_SYS_PROPS_DISK_FREE_SPACE, str);
					}
					else{
						(void)add_string_to_props_bag(bag, MA_SYS_PROPS_ROOT_PATH, MA_SYS_PROPS_DISK_FREE_SPACE, disk->free_diskspace);
					}
				}
				else
					(void)add_string_to_props_bag(bag, MA_SYS_PROPS_ROOT_PATH, MA_SYS_PROPS_DISK_FREE_SPACE, disk->free_diskspace);
				ma_buffer_release(buffer);
			}
			else
				(void)add_string_to_props_bag(bag, MA_SYS_PROPS_ROOT_PATH, MA_SYS_PROPS_DISK_FREE_SPACE, disk->free_diskspace);  

            /* individual drive/fs space : for windows only */
            #if defined(MA_WINDOWS)
                ma_temp_buffer_init(&key_buffer) ;
                for(fs_count = 0; fs_count < atoi(disk->total_filesystems); fs_count++) {
                    char *p = NULL ;
                    ma_temp_buffer_reserve(&key_buffer, strlen(MA_SYS_PROPS_DISK_FS_TOTAL_SPACE) + strlen(disk->filesysteminfo[fs_count].drive_name)) ;
                    MA_MSC_SELECT(_snprintf, snprintf)(p = (char*)ma_temp_buffer_get(&key_buffer), ma_temp_buffer_capacity(&key_buffer), "%s%s", MA_SYS_PROPS_DISK_FS_TOTAL_SPACE, disk->filesysteminfo[fs_count].drive_name) ;
                    (void)add_string_to_props_bag(bag, MA_SYS_PROPS_ROOT_PATH, p, disk->filesysteminfo[fs_count].drive_totalspace);

                    ma_temp_buffer_reserve(&key_buffer, strlen(MA_SYS_PROPS_DISK_FS_FREE_SPACE) + strlen(disk->filesysteminfo[fs_count].drive_name)) ;
                    MA_MSC_SELECT(_snprintf, snprintf)(p = (char*)ma_temp_buffer_get(&key_buffer), ma_temp_buffer_capacity(&key_buffer), "%s%s", MA_SYS_PROPS_DISK_FS_FREE_SPACE, disk->filesysteminfo[fs_count].drive_name) ;

					if( MA_OK == ma_ds_get_str(ds, MA_PROPERTY_PATH_SYSPROPS1000_STR, p, &buffer)) {
						char *str = NULL;
						size_t size = 0;
						if( (MA_OK == (ma_buffer_get_string(buffer, &str, &size))) && str){
							uint64_t mem_diff = abs(MA_MSC_SELECT(_strtoi64,strtoll)(str,NULL,0) - MA_MSC_SELECT(_strtoi64,strtoll)(disk->filesysteminfo[fs_count].drive_freespace,NULL,0));
						
							if( (mem_diff < MA_MINIMUM_MEMORY_DELTA) && (mem_diff < get_memory_delta((MA_MSC_SELECT(_strtoi64,strtoll)(disk->filesysteminfo[fs_count].drive_totalspace,NULL,0)), 32)) ){
								/* send old status */
								(void)add_string_to_props_bag(bag, MA_SYS_PROPS_ROOT_PATH, p, str);
							}
							else{
								(void)add_string_to_props_bag(bag, MA_SYS_PROPS_ROOT_PATH, p, disk->filesysteminfo[fs_count].drive_freespace);
							}
						}
						else
							(void)add_string_to_props_bag(bag, MA_SYS_PROPS_ROOT_PATH, p, disk->filesysteminfo[fs_count].drive_freespace);
						ma_buffer_release(buffer);
					}
					else
						(void)add_string_to_props_bag(bag, MA_SYS_PROPS_ROOT_PATH, p, disk->filesysteminfo[fs_count].drive_freespace);
                }
                ma_temp_buffer_uninit(&key_buffer) ;
            #endif
        }
        /** network info **/
        if(net){
            /*Mac Address */
            /*TODO - it can create problem , need to change this */
			if(network_address_format_check(net->mac_address_formated))
				(void)add_string_to_props_bag(bag, MA_SYS_PROPS_ROOT_PATH, MA_SYS_PROPS_NETWORK_MAC_ADDRESS, net->mac_address_formated);
            /* ip address */
			if(network_address_format_check(net->spipe_src_ip_address_formatted))
				(void)add_string_to_props_bag(bag, MA_SYS_PROPS_ROOT_PATH, MA_SYS_PROPS_NETWORK_IP, net->spipe_src_ip_address_formatted);

			ma_ds_set_str(ds, MA_PROPERTY_PROPERTIES_PATH_NAME_STR, SZ_COMPAT_IPADDRESS_A, (const char *) net->spipe_src_ip_address_formatted, strlen(net->spipe_src_ip_address_formatted)+1);
			ma_update_registry_publisher(MA_CONTEXT_GET_MSGBUS(context));

			/* subnet mask */
			if(network_address_format_check(net->subnet_mask))
				(void)add_string_to_props_bag(bag, MA_SYS_PROPS_ROOT_PATH, MA_SYS_PROPS_NETWORK_SUBNET_MASK, net->subnet_mask);

			/*subnet addr is not applicable for ipv6 */
			if((!strstr(net->spipe_src_ip_address_formatted, ":")) && (network_address_format_check(net->subnet_address))){
				/* subnet address */
				(void)add_string_to_props_bag(bag, MA_SYS_PROPS_ROOT_PATH, MA_SYS_PROPS_NETWORK_SUBNET_ADDRESS, net->subnet_address);
			}
            /* ip hostname */
            (void)add_string_to_props_bag(bag, MA_SYS_PROPS_ROOT_PATH, MA_SYS_PROPS_MISC_IP_HOST_NAME, net->fqdn);
        }

        /*Custom Properties */
        if(MA_OK == ma_ds_get_str(ds, MA_PROPERTY_PROPERTIES_PATH_NAME_STR, MA_PROPERTY_PROPERTIES_CUSTOM1_STR, &buffer)) {
            (void)add_buffer_to_props_bag(bag, MA_SYS_PROPS_ROOT_PATH, MA_PROPERTY_PROPERTIES_CUSTOM1_STR, buffer);
			ma_buffer_release(buffer);
        }

        if(MA_OK == ma_ds_get_str(ds, MA_PROPERTY_PROPERTIES_PATH_NAME_STR, MA_PROPERTY_PROPERTIES_CUSTOM2_STR, &buffer)) {
            (void)add_buffer_to_props_bag(bag, MA_SYS_PROPS_ROOT_PATH, MA_PROPERTY_PROPERTIES_CUSTOM2_STR, buffer);
			ma_buffer_release(buffer);
		}

        if(MA_OK == ma_ds_get_str(ds, MA_PROPERTY_PROPERTIES_PATH_NAME_STR, MA_PROPERTY_PROPERTIES_CUSTOM3_STR, &buffer)) {
            (void)add_buffer_to_props_bag(bag, MA_SYS_PROPS_ROOT_PATH, MA_PROPERTY_PROPERTIES_CUSTOM3_STR, buffer);
    		ma_buffer_release(buffer);    
		}

        if(MA_OK == ma_ds_get_str(ds, MA_PROPERTY_PROPERTIES_PATH_NAME_STR, MA_PROPERTY_PROPERTIES_CUSTOM4_STR, &buffer)) {
            (void)add_buffer_to_props_bag(bag, MA_SYS_PROPS_ROOT_PATH, MA_PROPERTY_PROPERTIES_CUSTOM4_STR, buffer);
			ma_buffer_release(buffer);
		}

        /*Management property */
        (void)add_string_to_props_bag(bag, MA_SYS_PROPS_ROOT_PATH, MA_SYS_PROPS_MANAGEMENT_TYPE, MA_SOFTWAREID_META_STR);
    }
    
    return rc ;
}

