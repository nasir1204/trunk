#include "ma_dc_task_handler.h"

#include "ma/datachannel/ma_datachannel.h"
#include "ma/datachannel/ma_datachannel_item.h"

#include "ma/internal/services/io/ma_dc_task_service.h"
#include "ma/internal/utils/datastructures/ma_slist.h"
#include "ma/ma_message.h"
#include "ma/ma_msgbus.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/ma_strdef.h"

#include "ma/internal/services/scheduler/ma_task_internal.h"
#include "ma/internal/utils/xml/ma_xml.h"
#include "ma/internal/utils/app_info/ma_registry_store.h"

#include "ma/ma_common.h"
#include "ma/scheduler/ma_task.h"
#include "ma/internal/services/scheduler/ma_scheduler_core.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"
#include "ma/internal/services/updater/ma_updater_request_param.h"
#include "ma/internal/utils/configurator/ma_configurator.h"

#include "ma/ma_client.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "runnow"

#define MA_CONTEXT_GET_DATASTORE(context) (ma_configurator_get_datastore(MA_CONTEXT_GET_CONFIGURATOR(context)))

struct ma_dc_task_handler_s{

	ma_context_t				*context;

	ma_registry_store_t			*reg_store;

	ma_msgbus_subscriber_t		*task_status_listener;

};

ma_bool_t is_application_installed(ma_dc_task_handler_t *self, const char *application_name){
		ma_registry_store_scan(self->reg_store);
		if(ma_registry_store_get_application(self->reg_store, application_name)) return MA_TRUE;
		return MA_FALSE;
}

static void upload_dc_task_summary(ma_dc_task_handler_t *self, const char *name, const char *task_id, ma_int64_t corelation_id, const char *task_summary){
	if(task_summary){
		ma_datachannel_item_t *dc_item = NULL;
		ma_error_t             rc = MA_OK;
		ma_variant_t          *variant = NULL;
		(void)ma_variant_create_from_string(task_summary, strlen(task_summary), &variant);
		if(variant){
			if(MA_OK == (rc = ma_datachannel_item_create(DC_TASK_RUN_NOW_RESULTS, variant, &dc_item))){
				ma_datachannel_item_set_option(dc_item, MA_DATACHANNEL_PERSIST, MA_TRUE);
				ma_datachannel_item_set_correlation_id(dc_item, corelation_id);		
				if(MA_OK == (rc = ma_datachannel_async_send(MA_CONTEXT_GET_CLIENT(self->context), MA_SOFTWAREID_GENERAL_STR, dc_item))){
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Sending the dc task %s execution summary to epo.", task_id);						
				}
				else
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to send dc task %s execution summary to epo, rc = <%d>.", task_id, rc);		
				ma_datachannel_item_release(dc_item);
			}
			else
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to create dc task %s execution summary, rc = <%d>.", task_id, rc);		
			ma_variant_release(variant);
		}
		else
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to create dc item variant.");		
	}

}

/* Back compat task types for updating */
#define MA_UPDATER_TASK_TYPE_UPDATE_COMPAT_STR                      "Update"
#define MA_UPDATER_TASK_TYPE_DEPLOYMENT_COMPAT_STR                  "Deployment"

static ma_bool_t is_it_update_task(ma_dc_task_t *dc_task){

	return (
			  (
				 !strcmp(dc_task->owner, MA_SOFTWAREID_META_STR) || !strcmp(dc_task->owner, MA_SOFTWAREID_GENERAL_STR)
			  )
			 &&
			  (
				 !strcmp(dc_task->type, MA_UPDATER_TASK_TYPE_UPDATE_COMPAT_STR) ||
				 !strcmp(dc_task->type, MA_UPDATER_TASK_TYPE_UPDATE_STR) || 
				 !strcmp(dc_task->type, MA_UPDATER_TASK_TYPE_DEPLOYMENT_COMPAT_STR) || 
				 !strcmp(dc_task->type, MA_UPDATER_TASK_TYPE_DEPLOYMENT_STR)
			   )
			);
}
/* Mapping of scheduler task status string to int status  
#define MA_TASK_EXEC_NOT_STARTED_TOPIC "ma.task.exec.not.started"
#define MA_TASK_EXEC_RUNNING_TOPIC "ma.task.exec.running"
#define MA_TASK_EXEC_FAILED_TOPIC "ma.task.exec.failed"
#define MA_TASK_EXEC_SUCCESS_TOPIC "ma.task.exec.success"
#define MA_TASK_EXEC_STOP_INITIATED_BY_SCHEDULER_TOPIC "ma.task.exec.stop.initiated.by.scheduler"
#define MA_TASK_EXEC_STOPPED_TOPIC "ma.task.exec.stopped"
typedef enum ma_runnow_task_status_e {
	MA_RUNNOW_TASK_STATUS_SUCCESS			= 1,	
	MA_RUNNOW_TASK_STATUS_FAILED			= -1,		
	MA_RUNNOW_TASK_STATUS_RECEIVED			= 7,	
	MA_RUNNOW_TASK_STATUS_STARTED			= 8,
	MA_RUNNOW_TASK_STATUS_NO_PP				= 9,	
	MA_RUNNOW_TASK_STATUS_STOPPED			= 10,	
	MA_RUNNOW_TASK_STATUS_UNKNOWN			= 11,	
	MA_RUNNOW_TASK_STATUS_NO_FINAL_STATUS	= 12
}ma_runnow_task_status_t; */

/*ePO expects :  -1 on failure, 1 on success and 0 on running */

static int get_task_status(const char *status_str) {
    if(!strcmp(status_str, MA_TASK_EXEC_SUCCESS_TOPIC))
        return 1;
    else if(!strcmp(status_str, MA_TASK_EXEC_FAILED_TOPIC) || !strcmp(status_str, MA_TASK_EXEC_STOPPED_TOPIC))
        return -1;
    else if(!strcmp(status_str, MA_TASK_EXEC_RUNNING_TOPIC))
        return 0;

	return 2;
}

#define MA_DC_TASK_SUMMARY_REPORT_STR  "<Status>" "<Code>%d</Code>" "<TaskName>%s</TaskName>" "<Detail>%s</Detail>" "<FailureReason></FailureReason>" "</Status>"

static void dc_task_set_status(ma_dc_task_handler_t *self, const char *name, const char *task_id, ma_int64_t corelation_id, int status, const char *status_str){
	char task_summary[1024] = {0};
	
	MA_MSC_SELECT(_snprintf, snprintf)(task_summary, 1024, MA_DC_TASK_SUMMARY_REPORT_STR, status, name, status_str);													
	/*send task summary to epo.*/
    upload_dc_task_summary(self, name, task_id, corelation_id, task_summary);    
}

static ma_error_t dc_task_status_handler(const char *topic, ma_message_t *msg, void *cb_data) {
	ma_dc_task_handler_t *self = (ma_dc_task_handler_t *)cb_data;
	ma_error_t rc = MA_OK;
	const char *task_id = NULL, *task_name = NULL;	
    
	if(MA_OK == (rc = ma_message_get_property(msg, MA_TASK_ID, &task_id)) && task_id &&
	   MA_OK == (rc = ma_message_get_property(msg, MA_TASK_NAME, &task_name)) && task_name){		
		ma_int64_t corelation_id = 0;
		ma_bool_t is_task_done = MA_FALSE;

		if(!strncmp(task_id, DC_TASK_UPDATE_NOW, strlen(DC_TASK_UPDATE_NOW)) || !strncmp(task_id, DC_TASK_RUN_NOW, strlen(DC_TASK_RUN_NOW))){

			if(MA_OK == (rc = ma_ds_get_int64(MA_CONTEXT_GET_DATASTORE(self->context), MA_RUNNOW_SERVICE_SECTION_STR, task_id, &corelation_id))){							
				if(!strcmp(topic, MA_TASK_EXEC_SUCCESS_TOPIC)){
					dc_task_set_status(self, task_name, task_id, corelation_id, MA_RUNNOW_TASK_STATUS_SUCCESS, topic);
					is_task_done = MA_TRUE;
				}
				else if(!strcmp(topic, MA_TASK_EXEC_FAILED_TOPIC)){
					dc_task_set_status(self, task_name, task_id, corelation_id, MA_RUNNOW_TASK_STATUS_FAILED, topic);
					is_task_done = MA_TRUE;
				}
				else if(!strcmp(topic, MA_TASK_EXEC_STOPPED_TOPIC)){
					dc_task_set_status(self, task_name, task_id, corelation_id, MA_RUNNOW_TASK_STATUS_STOPPED, topic);
					is_task_done = MA_TRUE;
				}
				else if(!strcmp(topic, MA_TASK_EXEC_RUNNING_TOPIC)){
					dc_task_set_status(self, task_name, task_id, corelation_id, MA_RUNNOW_TASK_STATUS_STARTED, topic);				
				}

				if(is_task_done){
					if(MA_OK != ma_ds_remove(MA_CONTEXT_GET_DATASTORE(self->context), MA_RUNNOW_SERVICE_SECTION_STR, task_id))
						MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to remove the run now task id {%s} entry from the datastore.", task_id);						
				}
			}
		}
	}
	return MA_OK;
}

ma_error_t ma_dc_task_handler_create(ma_context_t *context, ma_dc_task_handler_t **self){
	if(context && self){
		ma_dc_task_handler_t *handler = (ma_dc_task_handler_t*) calloc(1, sizeof(ma_dc_task_handler_t));
		if(handler){
			ma_error_t rc = MA_OK;
			if(MA_OK != (rc = ma_msgbus_subscriber_create(MA_CONTEXT_GET_MSGBUS(context), &handler->task_status_listener))){				
				MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_ERROR, "Failed to create the subscriber in dc task service, rc = %d.", rc);
				free(handler);
			}
		    else{
				(void)ma_registry_store_create(&handler->reg_store);
				(void) ma_msgbus_subscriber_setopt(handler->task_status_listener, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
				(void) ma_msgbus_subscriber_setopt(handler->task_status_listener, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_INPROC);
				handler->context = context;
				*self = handler;
		    }			
			return rc;
		}		
		return MA_ERROR_OUTOFMEMORY;
	}
	return MA_ERROR_INVALIDARG;
}


ma_error_t ma_dc_task_handler_start(ma_dc_task_handler_t *self){
	if(self){
		ma_error_t rc = MA_OK;
		if(MA_OK == (rc = ma_msgbus_subscriber_register(self->task_status_listener, MA_TASK_STATUS_PUBSUB_TOPIC_STR, dc_task_status_handler, self)))
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO, "Started dc task service.");									
		else
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to start subscriber for the task status, rc = <%d>", rc);							
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_dc_task_handler_stop(ma_dc_task_handler_t *self){
	if(self){
		ma_error_t rc = MA_OK;
		if(MA_OK != (rc = ma_msgbus_subscriber_unregister(self->task_status_listener)))
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to unregister task status topic, rc = <%d>.", rc);								
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}


static ma_error_t dc_task_handler_schedule_dc_task(ma_dc_task_handler_t *self, ma_dc_task_t *dc_task){
	ma_error_t rc = MA_OK;
	ma_task_t *task = NULL;
	ma_trigger_t *trigger = NULL;
	
	if(MA_OK == (rc = ma_scheduler_create_task(MA_CONTEXT_GET_SCHEDULER(self->context), dc_task->creator, dc_task->task_id, dc_task->name, dc_task->type, &task))){ 	
		ma_variant_t    *dc_setting_value = NULL;
		ma_trigger_run_now_policy_t run_now;
		run_now.now = MA_TRUE;		
		
		(void)ma_task_set_software_id(task, dc_task->owner);
		(void)ma_task_set_app_payload(task, dc_task->settings); 				

		/*Add the dc related settings into the task setting.*/		
		(void)ma_variant_create_from_string(dc_task->dc_task_name, strlen(dc_task->dc_task_name), &dc_setting_value);		
		(void)ma_task_set_setting(task, DC_SCHEDULED_TASK_SECTION_NAME_STR, DC_SCHEDULED_TASK_SETTING_NAME_STR, dc_setting_value);
		(void)ma_variant_release(dc_setting_value);dc_setting_value = NULL;

		(void)ma_variant_create_from_string(dc_task->owner, strlen(dc_task->owner), &dc_setting_value);		
		(void)ma_task_set_setting(task, DC_SCHEDULED_TASK_SECTION_NAME_STR, DC_SCHEDULED_TASK_SETTING_OWNER_NAME_STR, dc_setting_value);
		(void)ma_variant_release(dc_setting_value); dc_setting_value = NULL;

		(void)ma_variant_create_from_uint64(dc_task->corelation_id, &dc_setting_value);		
		(void)ma_task_set_setting(task, DC_SCHEDULED_TASK_SECTION_NAME_STR, DC_SCHEDULED_TASK_SETTING_CORELATION_ID_INT, dc_setting_value);
		(void)ma_variant_release(dc_setting_value); dc_setting_value = NULL;
		
		(void)ma_variant_create_from_uint64(1, &dc_setting_value);		
		(void)ma_task_set_setting(task, DC_SCHEDULED_TASK_SECTION_NAME_STR, DC_SCHEDULED_TASK_SETTING_RUNNOW_STR, dc_setting_value);
		(void)ma_variant_release(dc_setting_value); dc_setting_value = NULL;

		(void)ma_variant_create_from_uint64(is_it_update_task(dc_task) ? 0 : 1, &dc_setting_value);		
		(void)ma_task_set_setting(task, DC_SCHEDULED_TASK_SECTION_NAME_STR, DC_SCHEDULED_TASK_SETTING_RUNNOW_STATUS_CHECK_STR, dc_setting_value);
		(void)ma_variant_release(dc_setting_value); dc_setting_value = NULL;

		/*trigger run_now*/
		if(MA_OK == (rc = ma_trigger_create_run_now(&run_now, &trigger))){
			if(MA_OK == (rc = ma_task_add_trigger(task, trigger))){
				if(MA_OK == (rc = ma_task_start(task))){					
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO, "Scheduled the dc task <%s> name <%s> rc = <%d>", dc_task->task_id, dc_task->name, rc);                            
				}
			}
            (void)ma_trigger_release(trigger);
		}
		if(MA_OK != rc){
			(void)ma_scheduler_release_task(MA_CONTEXT_GET_SCHEDULER(self->context), task);
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to create task with task id <%s> name <%s> rc = <%d>", dc_task->task_id, dc_task->name, rc);                            
		}
	}
	else{
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to create task with task id <%s> name <%s> rc = <%d>", dc_task->task_id, dc_task->name, rc);                            
	}	
	return rc;
}



static ma_error_t dc_task_handler_add(ma_dc_task_handler_t *self, ma_dc_task_t *dc_task){
	/*agent update task will be handled by the updater service itself, other run now task dc task handler will handle it.*/
	if(!is_it_update_task(dc_task)){		
        ma_task_t *sched_task = NULL;			
		ma_error_t rc = MA_OK;

        /* Set condition on task to get the execution status notifications */
        if(MA_OK == (rc = ma_scheduler_get_task_handle(MA_CONTEXT_GET_SCHEDULER(self->context), dc_task->task_id, &sched_task))) {
			ma_variant_t *var = NULL;

            ma_task_set_condition(sched_task, MA_TASK_COND_NOTIFY_ON_EXEC_STATUS_CHANGE);
            ma_task_release(sched_task);

			if(MA_OK == ma_variant_create_from_int64(dc_task->corelation_id, &var)){
				if(MA_OK == (rc = ma_ds_set(MA_CONTEXT_GET_DATASTORE(self->context), MA_RUNNOW_SERVICE_SECTION_STR, dc_task->task_id, var))){										
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Added the task {%s} info in datastore", dc_task->task_id);    					
				}
				else{
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to set the task {%s} info in datastore, rc = <%d> ", dc_task->task_id, rc);                            
				}			
				ma_variant_release(var);
			}
		}
		else{
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to get the task {%s} handle, rc = <%d> ", dc_task->task_id, rc);                            
		}			
	}
	return MA_OK;
}

ma_error_t ma_dc_task_handler_submit(ma_dc_task_handler_t *self, ma_dc_task_t *task){
	if(self && task){
		if(task->task_id &&
			task->type &&
			task->settings && 
			task->owner &&
			task->creator &&
			task->dc_task_name && 
			task->name){

			ma_error_t rc = MA_OK;
			if(is_application_installed(self, task->owner)){
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Product {%s} run now task {%s} received", task->owner, task->name);  

				dc_task_set_status(self, task->name, task->task_id, task->corelation_id, MA_RUNNOW_TASK_STATUS_RECEIVED, "");
				if(MA_OK == (rc = dc_task_handler_schedule_dc_task(self, task)))
					dc_task_handler_add(self, task);			
			}
			else{
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_WARNING, "Product {%s} run now task {%s} received, which is not installed, can't handle it.", task->owner, task->name);  
				dc_task_set_status(self, task->name, task->task_id, task->corelation_id, MA_RUNNOW_TASK_STATUS_NO_PP, "");
			}
			return rc;
		}
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_dc_task_handler_release(ma_dc_task_handler_t *self){
	if(self){		
		(void)ma_msgbus_subscriber_release(self->task_status_listener);
		(void)ma_registry_store_release(self->reg_store);
		free(self);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}
	

void ma_dc_task_init(ma_dc_task_t *dc_task){
	if(dc_task) {
		dc_task->dc_task_name = NULL;
		dc_task->owner = NULL;
		dc_task->creator = NULL;
		dc_task->task_id = NULL;
		dc_task->type = NULL;
		dc_task->name = NULL;
		dc_task->settings = NULL;		
		dc_task->corelation_id = 0;
	}
}

ma_error_t ma_dc_task_deinit(ma_dc_task_t *dc_task){
	if(dc_task){
		if(dc_task->dc_task_name) free(dc_task->dc_task_name);
		if(dc_task->creator) free(dc_task->creator);
		if(dc_task->owner) free(dc_task->owner);
		if(dc_task->name) free(dc_task->name);
		if(dc_task->task_id) free(dc_task->task_id);		
		if(dc_task->type) free(dc_task->type);
		if(dc_task->settings) ma_variant_release(dc_task->settings); 		
		ma_dc_task_init(dc_task);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}
