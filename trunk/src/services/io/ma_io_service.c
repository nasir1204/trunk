#include "ma/internal/services/io/ma_io_service_internal.h"
#include "ma/internal/services/io/ma_io_service.h"

#include "ma/internal/services/ma_agent_policy_settings_bag.h"

/* services external */
#include "ma_crypto_service.h"
#include "ma/internal/services/scheduler/ma_scheduler_service.h"
#include "ma/internal/services/property/ma_property_service.h"
#include "ma/internal/services/policy/ma_policy_service.h"
#include "ma/internal/services/event/ma_event_service.h"
#include "ma/internal/services/datachannel/ma_datachannel_service.h"
#include "ma/internal/services/updater/ma_updater_service.h"
#include "ma/internal/services/repository/ma_repository_service.h"
#include "ma/internal/services/booking/ma_booking_service.h"
#include "ma/internal/services/mail/ma_mail_service.h"
#include "ma/internal/services/profile/ma_profile_service.h"
#include "ma/internal/services/seasons/ma_season_service.h"
#include "ma/internal/services/search/ma_search_service.h"
#include "ma/internal/services/board/ma_board_service.h"
#include "ma/internal/services/comments/ma_comment_service.h"
#include "ma/internal/services/consignment/ma_consignment_service.h"
#include "ma/internal/services/inventory/ma_inventory_service.h"
#include "ma/internal/services/recorder/ma_recorder_service.h"
#include "ma/internal/services/location/ma_location_service.h"
#include "ma_compat_service.h"
#include "ma/internal/services/sensor/ma_sensor_service.h"
#include "ma/internal/utils/firewall_exceptions/ma_firewall_exceptions.h"
#include "ma/internal/utils/repository/ma_sitelist.h"

/* services internal */
#include "ma/internal/core/msgbus/ma_msgbus_private.h"
#include "ma_logger_service.h"
#include "ma/internal/utils/network/ma_network_service.h"
#include "ma/internal/services/io/ma_task_service.h"
#include "ma/internal/services/io/ma_dc_task_service.h"
#include "ma/internal/utils/filesystem/path.h"

#include "ma/internal/utils/time/ma_time.h"
#include "ma/internal/services/property/ma_property_service.h"


#include "ma_spipe_misc_handler.h"
#include "ma_relay_handler.h"
#include "ma/datastore/ma_ds_registry.h"

#include "ma/internal/defs/ma_info_defs.h"
#include "ma/info/ma_info.h"



#include <stdio.h>
#include <stdlib.h>

#ifndef MA_WINDOWS
#include <unistd.h>
#endif

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "ioservice"

void ma_service_pre_check_sequence(uv_loop_t *loop);

static ma_error_t policy_notification_cb(ma_client_t *client, const char *product_id, const ma_policy_uri_t *info_uri, void *cb_data);
static ma_error_t manage_services_as_per_policies(ma_io_service_t *self);
static ma_error_t create_managed_mode_items(ma_io_service_t *self);
static void release_managed_mode_items(ma_io_service_t *self);
static ma_error_t setup_agent_configuration(ma_io_service_t *self);
static ma_error_t ma_io_service_post_sensor_agent_start_event(ma_io_service_t *service);

static void publish_ma_info_event_message(ma_io_service_t *self, const char *info_event) ;

static ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint);
static ma_error_t service_start(ma_service_t *service);
static ma_error_t service_stop(ma_service_t *service);

static const ma_service_methods_t methods = {
    &service_configure,
    &service_start,
    &service_stop,
    NULL /* don't use service manager's memory management, lifetime is controlled through other means */
};  

ma_error_t ma_io_service_create(const char *service_name, ma_logger_t *logger, ma_io_service_t **io_service) {
    if(io_service) {
        ma_error_t rc = MA_OK;
        uv_loop_t *loop = NULL;
        ma_policy_bag_t** policy_bag = NULL;
        ma_datastore_configuration_request_t datastore_required = {{MA_TRUE,MA_DB_OPEN_READWRITE}, {MA_TRUE,MA_DB_OPEN_READWRITE}, {MA_TRUE,MA_DB_OPEN_READWRITE}, {MA_TRUE,MA_DB_OPEN_READWRITE}, {MA_FALSE,0}, {MA_TRUE, MA_DB_OPEN_READWRITE}, {MA_TRUE, MA_DB_OPEN_READWRITE}, {MA_TRUE, MA_DB_OPEN_READWRITE}, {MA_TRUE, MA_DB_OPEN_READWRITE}, {MA_TRUE, MA_DB_OPEN_READWRITE}, {MA_TRUE, MA_DB_OPEN_READWRITE}, {MA_TRUE, MA_DB_OPEN_READWRITE}, {MA_TRUE, MA_DB_OPEN_READWRITE}, {MA_TRUE, MA_DB_OPEN_READWRITE}, {MA_TRUE, MA_DB_OPEN_READWRITE}};
        ma_io_service_t *self = (ma_io_service_t *)calloc(1, sizeof(ma_io_service_t));
        
        if(!self)   return MA_ERROR_OUTOFMEMORY;

        ma_service_init((ma_service_t *)self, &methods, service_name, self);
        ma_service_manager_init(&self->service_manager);
        self->logger = logger;
        /* create mandatory objects */
        rc = (
            /* context */
            (MA_OK == (rc = ma_context_create(&self->context))) && 
            (MA_OK == (rc = ma_service_configurator_create(&self->configurator))) &&
            (MA_OK == (rc = ma_configurator_intialize_datastores(self->configurator, &datastore_required))) &&
            (MA_OK == (rc = ma_agent_policy_settings_bag_create(ma_configurator_get_policies_database(self->configurator), &self->policy_settings_bag))) &&            
            (MA_OK == (rc = ma_policy_settings_bag_set_logger((ma_policy_settings_bag_t *)self->policy_settings_bag, logger))) &&
            (MA_OK == (rc = ma_system_property_create(&self->system_property, self->logger))) &&           
            (MA_OK == (rc = ma_msgbus_create(MA_SOFTWAREID_GENERAL_STR, &self->msgbus))) &&
            (MA_OK == (rc = ma_msgbus_set_logger(self->msgbus, self->logger))) &&           
            (MA_OK == (rc = ma_client_create(MA_SOFTWAREID_GENERAL_STR, &self->client))) &&
            (MA_OK == (rc = ma_client_set_msgbus(self->client, self->msgbus))) &&
			(MA_OK == (rc = ma_client_set_logger(self->client, self->logger))) &&
            (MA_OK == (rc = ma_client_set_thread_option(self->client, MA_MSGBUS_CALLBACK_THREAD_IO)))
            ) ? MA_OK : rc;

        if(MA_OK != rc) {
            ma_io_service_release(self);
            return rc;
        }
        
        loop = ma_event_loop_get_uv_loop(ma_msgbus_get_event_loop(self->msgbus));

        ma_service_pre_check_sequence(loop);

        /* add objects into context */
        ma_context_add_object_info(self->context, MA_OBJECT_MA_CONFIGURATOR_NAME_STR, (void *const*)&self->configurator);
        ma_context_add_object_info(self->context, MA_OBJECT_CLIENT_NAME_STR, (void *const*)&self->client);
        ma_context_add_object_info(self->context, MA_OBJECT_POLICY_SETTINGS_BAG_STR, (void *const*)&self->policy_settings_bag);
        ma_context_add_object_info(self->context, MA_OBJECT_LOGGER_NAME_STR, (void *const*)&self->logger);
        ma_context_add_object_info(self->context, MA_OBJECT_SYSTEM_PROPERTY_STR, (void *const*)&self->system_property);        
        ma_context_add_object_info(self->context, MA_OBJECT_MSGBUS_NAME_STR, (void *const*)&self->msgbus);        
        if(policy_bag = ma_agent_policy_settings_bag_get_policy_bag(self->policy_settings_bag))
            ma_context_add_object_info(self->context, MA_OBJECT_POLICY_BAG_STR, (void*const*)policy_bag);

        ma_service_manager_set_logger(&self->service_manager, self->logger);
        /* add ourself as a service too */
        ma_service_manager_add_service(&self->service_manager, &self->base_service, service_name);

        /* manage services as per policies and add/remove with service manager */
        if(MA_OK != (rc = manage_services_as_per_policies(self))) {
            ma_io_service_release(self);
            return rc;
        }                     
        
        
        *io_service = self;
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

static void check_and_perform_asc_if_needed(ma_io_service_t *self) {
	ma_buffer_t *buffer = NULL ;
	ma_error_t rc  = MA_OK;
	ma_int32_t days = 0;
	ma_ds_t *ds = ma_configurator_get_datastore(MA_CONTEXT_GET_CONFIGURATOR(self->context)) ;

	if((MA_OK == (rc = ma_policy_settings_bag_get_int(self->policy_settings_bag, MA_PROPERTY_SERVICE_SECTION_NAME_STR, MA_PROPERTY_KEY_COLLECT_IF_DELAYS_BY_DAYS_INT, MA_FALSE, &days, 0)))
	   && (days > 0)){
		if(MA_OK == (rc = ma_ds_get_str(ds, MA_PROPERTY_DATA_PATH_NAME_STR, MA_PROPERTY_DATA_KEY_ASC_TIME_STR, &buffer))) {
			const char *last_asc_time = NULL;
			size_t size =  0;
			if(MA_OK == (rc = ma_buffer_get_string(buffer, &last_asc_time, &size))) {
				ma_time_t asc_time = {0};
				ma_time_t current_time = {0};
				double diff = 0;
				ma_int32_t year = 0, month = 0, day = 0, hour = 0, minute = 0;

				sscanf(last_asc_time,"%04d%02d%02d%02d%02d", &year, &month, &day, &hour, &minute);
				asc_time.year = (ma_int16_t)year; asc_time.month = (ma_int16_t)month; asc_time.day = (ma_int16_t)day;
				asc_time.hour = (ma_int16_t)hour; asc_time.minute = (ma_int16_t)minute;
				
				ma_time_get_localtime(&current_time);
				current_time.milliseconds = current_time.second = 0;
				diff = difftime(ma_time_to_time_t(current_time), ma_time_to_time_t(asc_time)) ;

				if((diff / (60 * 60)) > (days * 24)) {
					ma_message_t *props_collect_msg = NULL;
					MA_LOG(self->logger, MA_LOG_SEV_INFO, "Triggering ASC because of 24 hours check passed at system startup");
					if(MA_OK == (rc = ma_property_collect_message_create(PROPS_TYPE_VERSION, &props_collect_msg))) {
						ma_msgbus_endpoint_t *endpoint = NULL;
						if(MA_OK == (rc = ma_msgbus_endpoint_create(self->msgbus, MA_PROPERTY_SERVICE_NAME_STR, NULL, &endpoint))) {
							(void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_INPROC);
							(void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
							if(MA_OK == (rc = ma_msgbus_send_and_forget(endpoint, props_collect_msg))) {
								MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Triggered ASC because of 24 hours check at system startup");
							}
							(void)ma_msgbus_endpoint_release(endpoint);
						}
						(void)ma_message_release(props_collect_msg);
					}
				}
			}
			(void)ma_buffer_release(buffer);
		}
	}
}
 
static void enforce_policy_on_service_startup(ma_io_service_t *self) {
	ma_msgbus_endpoint_t *endpoint = NULL ;
	ma_message_t *request = NULL ;
	ma_error_t rc = MA_OK ;
	const char *props_ver = NULL ;
	size_t size = 0 ;
	ma_buffer_t *buffer = NULL ;
	ma_ds_t *ds = ma_configurator_get_datastore(MA_CONTEXT_GET_CONFIGURATOR(self->context)) ;

	/* First ASC has already happened */
	if (MA_OK == (rc = ma_ds_get_str(ds, MA_PROPERTY_DATA_PATH_NAME_STR, MA_PROPERTY_DATA_KEY_PROPERTY_VERSION_STR, &buffer))) {
        (void)ma_buffer_get_string(buffer, &props_ver, &size) ;
        if (props_ver && (0 == strcmp(props_ver, "0"))) {
			rc = MA_ERROR_ASC_NOT_PERFORMED ;
		}
		(void)ma_buffer_release(buffer) ;	buffer = NULL ;
	}

	if(MA_OK == rc && MA_OK == (rc = ma_message_create(&request))) {
        (void)ma_message_set_property(request, MA_PROP_KEY_POLICY_REQUEST_TYPE_STR, MA_PROP_VALUE_ENFORCE_POLICIES_REQUEST_STR) ;
        (void)ma_message_set_property(request, MA_PROP_KEY_POLICY_ENFORCE_REASON_STR, MA_PROP_VALUE_SERVICE_STARTUP_STR) ;
        
		if(MA_OK == (rc = ma_msgbus_endpoint_create(self->msgbus, MA_POLICY_SERVICE_NAME_STR, MA_POLICY_SERVICE_HOSTNAME_STR, &endpoint))) {
			(void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_INPROC) ;
			(void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);			
				
			if(MA_OK == (rc = ma_msgbus_send_and_forget(endpoint, request))) {
				MA_LOG(self->logger, MA_LOG_SEV_INFO, "Policy enforcement on service start up message sent.") ;								
            }
			else
                MA_LOG(self->logger, MA_LOG_SEV_WARNING, "Policy enforcement on service start up failed, %d.", rc) ;
            
            (void)ma_msgbus_endpoint_release(endpoint) ;	endpoint = NULL ;
        }
        (void)ma_message_release(request) ;	request = NULL ;
    }
}

static void io_service_prepare_cb(uv_prepare_t* handle, int status) {
    ma_io_service_t *self = (ma_io_service_t *)handle->data;
    if(!status) {
        ma_error_t rc = MA_OK;
        /* configure all of the services */
        if(MA_OK == (rc = ma_service_manager_configure_all(&self->service_manager, self->context, MA_SERVICE_CONFIG_NEW))) {
            (void)ma_msgbus_set_crypto(self->msgbus, MA_CONTEXT_GET_CRYPTO(self->context));
            if(MA_OK == (rc = ma_service_manager_start_all(&self->service_manager))) {
                MA_LOG(self->logger, MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "Agent service is running");     
				/* send policy enforcement message - fix for BZ# 968468*/
				(void)enforce_policy_on_service_startup(self) ;
				(void)check_and_perform_asc_if_needed(self);
            }
            else
                MA_LOG(self->logger, MA_LOG_SEV_ERROR, "One or more services did not start!, rc = %d", rc);
        }
        else
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "One or more services did not configure!, rc = %d", rc);
    }
    uv_prepare_stop(&self->prepare_event);
    uv_close((uv_handle_t *)&self->prepare_event, NULL);
}

ma_error_t ma_io_service_run(ma_io_service_t *self) {    
    if(self) {         
        if(!uv_prepare_init( ma_event_loop_get_uv_loop(ma_msgbus_get_event_loop(self->msgbus)), &self->prepare_event)) {
            self->prepare_event.data = self;
            if(!uv_prepare_start(&self->prepare_event, io_service_prepare_cb)) {           
                /* Below will block */
                return ma_msgbus_run(self->msgbus);
            }
            uv_close((uv_handle_t *)&self->prepare_event, NULL);
        }
    }
    return MA_ERROR_INVALIDARG;
}

static void loop_stop_handler_cb(ma_event_loop_t *loop, int status, void *cb_data) {
    ma_io_service_t *self = (ma_io_service_t *)cb_data;
    ma_error_t rc = MA_OK;
    if(self) {
        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "in ma_io_service_stop, stopping from worker thread...");
        rc = ma_service_manager_stop_all(&self->service_manager);
		publish_ma_info_event_message(self, MA_INFO_EVENT_AGENT_STOPED_STR) ;
		ma_msgbus_stop(self->msgbus, MA_FALSE);      
        MA_LOG(self->logger, MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "McAfee Agent service stopped");
    }
}
ma_error_t ma_io_service_stop(ma_io_service_t *self) {    
    if(self) { 
        ma_event_loop_t *event_loop = ma_msgbus_get_event_loop(MA_CONTEXT_GET_MSGBUS(self->context));
        ma_event_loop_register_stop_handler(event_loop, loop_stop_handler_cb, self);
		
        return ma_event_loop_stop(event_loop);
    }
    return MA_ERROR_INVALIDARG;
}

static void loop_shutdown_handler_cb(ma_event_loop_t *loop, int status, void *cb_data) {
    ma_io_service_t *self = (ma_io_service_t *)cb_data;
    ma_error_t rc = MA_OK;
    if(self) {        
		MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "in ma_io_service_shutdown, sending agent uninstall package to epo.");		
		ma_ah_client_service_send_uninstall_package(MA_CONTEXT_GET_AH_CLIENT(self->context));		
		loop_stop_handler_cb(loop, status, cb_data);	
    }
}

ma_error_t ma_io_service_shutdown(ma_io_service_t *self){
	if(self) { 
		unsigned short vdi_mode =  ma_configurator_get_vdimode(MA_CONTEXT_GET_CONFIGURATOR(self->context));
		MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "system shutdown, stopping io service...");		
		if(MA_AGENT_MODE_MANAGED_INT == self->agent_mode && vdi_mode){						
			ma_event_loop_t *event_loop = ma_msgbus_get_event_loop(MA_CONTEXT_GET_MSGBUS(self->context));
			MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Agent is in vdi mode, send uninstall package to epo.");
			
			ma_event_loop_register_stop_handler(event_loop, loop_shutdown_handler_cb, self);
			return ma_event_loop_stop(event_loop);
		}
		else{
			return ma_io_service_stop(self);
		}
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_io_service_release(ma_io_service_t *self) {
    if(self) {
        ma_service_manager_release_all(&self->service_manager);
		
		if(self->relay_handler) ma_relay_handler_release(self->relay_handler);
		
        if(self->server_settings_tb) ma_table_release(self->server_settings_tb);

        if(self->io_subscriber) ma_msgbus_subscriber_release(self->io_subscriber);

        if(self->io_server) ma_msgbus_server_release(self->io_server);

        if(self->client) ma_client_release(self->client);

        if(self->msgbus)    ma_msgbus_release(self->msgbus);

        if(self->system_property) ma_system_property_release(self->system_property);
        
        if(self->policy_settings_bag) ma_policy_settings_bag_release(self->policy_settings_bag);
                
        if(self->configurator)  ma_configurator_release(self->configurator);
				
        if(self->context) ma_context_release(self->context);

        free(self);
  
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

void stop_compat_eventing(ma_io_service_t *self){		
	#ifndef MA_WINDOWS
    ma_configurator_t *configurator	= MA_CONTEXT_GET_CONFIGURATOR(self->context);
	const char *data_path			= NULL;	
    if((data_path = ma_configurator_get_agent_data_path(configurator))) {
        char buffer[MA_MAX_PATH_LEN] = {0};
        MA_MSC_SELECT(_snprintf, snprintf)(buffer, MA_MAX_PATH_LEN,"%s%c%s%c%s", data_path, MA_PATH_SEPARATOR, MA_AGENT_DATA_DIR_STR, MA_PATH_SEPARATOR, key_event_policy_ini);
        unlink(buffer);
	}
	#endif
}

static ma_error_t update_firewall_exceptions_rules(ma_io_service_t *self) {
	ma_error_t rc = MA_OK;
	/* ma_db_t *db = ma_configurator_get_database(self->configurator); */

	if(MA_OK != (rc = ma_firewall_exceptions_add_constant_rules(self->configurator))) 
		MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to add MA firewall constant rules rc <%d>", rc);
	else {
		/*ma_temp_buffer_t server_ip_list = MA_TEMP_BUFFER_INIT, server_fqdn_list = MA_TEMP_BUFFER_INIT;
		
		if(MA_OK == (rc = ma_sitelist_get_server_list(db, MA_SERVER_LIST_BY_IP, &server_ip_list)) &&
			MA_OK == (rc = ma_sitelist_get_server_list(db, MA_SERVER_LIST_BY_FQDN, &server_fqdn_list))) {
		*/
			const char *tcp_port = "8081", *udp_port = "8082";			
			(void) ma_policy_settings_bag_get_str(self->policy_settings_bag, 
                                                  MA_HTTP_SERVER_SERVICE_SECTION_NAME_STR, 
                                                  MA_HTTP_SERVER_KEY_PORT_INT, MA_FALSE,
                                                  &tcp_port, tcp_port);
			(void) ma_policy_settings_bag_get_str(self->policy_settings_bag, MA_UDP_SERVICE_SECTION_NAME_STR, MA_UDP_KEY_BROADCAST_PORT_INT, MA_FALSE, &udp_port, udp_port) ;

			if(MA_OK != (rc = ma_firewall_exceptions_add_dynamic_rules(self->configurator, tcp_port, udp_port)))
				MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to add MA firewall dynamic rules, rc <%d>.", rc);		
		/*}
		ma_temp_buffer_uninit(&server_ip_list);
		ma_temp_buffer_uninit(&server_fqdn_list);
		*/
	}
		
	return rc;
}

static ma_error_t initialize_sensors(ma_io_service_t *self);
static ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint) {
    if(service) {
        ma_error_t rc = MA_OK;
        ma_io_service_t *self = (ma_io_service_t *)service;
 
        if(MA_SERVICE_CONFIG_NEW == hint) {
            ma_msgbus_t *msgbus = MA_CONTEXT_GET_MSGBUS(context);

			(void)setup_agent_configuration(self);

            if(MA_OK != (rc = ma_ds_get_int32(ma_configurator_get_datastore(self->configurator), MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_MODE_INT, &self->agent_mode)))
                return rc;

            /* Create the server */
            if(MA_OK != (rc = ma_msgbus_server_create(msgbus,MA_IO_SERVICE_NAME_STR, &self->io_server))) {          
                MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to create the service %s.", MA_IO_SERVICE_NAME_STR);
                return rc;
            }
            ma_msgbus_server_setopt(self->io_server, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
            ma_msgbus_server_setopt(self->io_server, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);

            /* Create the subscriber */
            if(MA_OK != (rc = ma_msgbus_subscriber_create(msgbus, &self->io_subscriber))) {
                ma_msgbus_server_release(self->io_server);
                MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to create the subscriber in i/o service, rc = %d.", rc);
                return rc;
            }
            (void) ma_msgbus_subscriber_setopt(self->io_subscriber, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
            (void) ma_msgbus_subscriber_setopt(self->io_subscriber, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_INPROC);		

			if(MA_AGENT_MODE_MANAGED_INT == self->agent_mode){
				if(MA_OK != (rc = ma_relay_handler_create(&self->relay_handler)))
					MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to create relay discovery handler, rc = %d", rc);								
			}
			
			/* update firewall exceptions rules always on service start */
			if(MA_OK != (rc = update_firewall_exceptions_rules(self)))
				MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to add MA filrewall rules, rc = %d", rc);
			else
				MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Successfully added MA firewall rules");
        }
		if(self->relay_handler){
			(void)ma_relay_handler_configure(self->relay_handler, context, hint);
		} 

		if(MA_AGENT_MODE_UNMANAGED_INT == self->agent_mode)
			stop_compat_eventing(self);

		/*	provision only ma and scheduler db.
			policy, task might be in transaction at this time
			provision them in their respective services
		*/
		(void)ma_db_provision(ma_configurator_get_database(self->configurator)) ;
		(void)ma_db_provision(ma_configurator_get_scheduler_database(self->configurator)) ;

        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t service_start(ma_service_t *service) {
    if(service) {
        ma_error_t rc = MA_OK;
        ma_io_service_t *self = (ma_io_service_t *)service;
        
        
        /* start client */
        if(MA_OK != (rc = ma_client_start(self->client))) {
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to start client manager, %d.", rc);
            return rc;
        }

		if(MA_OK != (rc = ma_policy_register_notification_callback(self->client, MA_SOFTWAREID_GENERAL_STR, MA_POLICY_CHANGED|MA_POLICY_TIMEOUT, policy_notification_cb, self))) {
			MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to register policy notification callback, %d.", rc);
			return rc;
		}

        /*managed mode register for property and spipe common property manager */
        if(MA_OK != (rc = create_managed_mode_items(self))) {
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to register for managed mode items, %d.", rc);
            return rc;
        }
						
        if(MA_OK != (rc = ma_msgbus_server_start(self->io_server,ma_io_service_server_handler,self))) {
            ma_client_stop(self->client);
            release_managed_mode_items(self);
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to start the service %s .", MA_IO_SERVICE_NAME_STR);
            return rc;
        }

        if(MA_OK != (rc = ma_msgbus_subscriber_register(self->io_subscriber, MA_GENERAL_SUBSCRIBER_TOPIC_STR, ma_io_service_subscriber_handler,self))) {
            ma_client_stop(self->client);
            ma_msgbus_server_stop(self->io_server);
            release_managed_mode_items(self);
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to register with subscriber for %s  , rc = %d.",MA_GENERAL_SUBSCRIBER_TOPIC_STR, rc);
            return rc;
        }
		
		if(self->relay_handler)
			(void)ma_relay_handler_start(self->relay_handler);		

		publish_ma_info_event_message(self, MA_INFO_EVENT_AGENT_STARTED_STR) ;

        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "%s service started.", MA_IO_SERVICE_NAME_STR);
        
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t service_stop(ma_service_t *service) {
    if(service) {
        ma_error_t rc = MA_OK;
        ma_io_service_t *self = (ma_io_service_t *)service;
        
		
        
        release_managed_mode_items(self);

        (void) ma_client_stop(self->client);

		if(self->relay_handler)
			(void)ma_relay_handler_stop(self->relay_handler);		

        if(MA_OK != (rc = ma_msgbus_subscriber_unregister(self->io_subscriber))) {
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to unregister with subscriber for %s , rc = %d.",MA_GENERAL_SUBSCRIBER_TOPIC_STR, rc);
            return rc;
        }

        if(MA_OK != (rc = ma_msgbus_server_stop(self->io_server))) {
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to stop the service %s .", MA_IO_SERVICE_NAME_STR);
            return rc;
        }

        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "%s service stoped.", MA_IO_SERVICE_NAME_STR);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}
         

static ma_error_t manage_services_as_per_policies(ma_io_service_t *self) {
    ma_error_t rc = MA_OK;
    ma_int32_t agent_mode = ma_configurator_get_agent_mode(self->configurator);
    ma_int32_t is_logger_on = 1, is_crypto_on = 0, is_network_on = 0, is_scheduler_on = 1, is_ah_on = agent_mode ? 1 : 0, is_property_on = agent_mode ? 1 : 0, is_policy_on = 1;
    ma_int32_t is_event_on = agent_mode ? 1 : 0, is_compat_on = 0, is_sensor_on = 1, is_dc_on = agent_mode ? 1 : 0 ,is_repository_on = 1, is_updater_on = 1 , is_task_on = 1;
    ma_int32_t is_compat_registry_on = 1;
    ma_int32_t is_booking_service_on = 1;
    ma_int32_t is_profile_service_on = 1;
    ma_int32_t is_mail_service_on = 1;
    ma_int32_t is_inventory_service_on = 1;
    ma_int32_t is_recorder_service_on = 1;
    ma_int32_t is_location_service_on = 1;
    ma_int32_t is_season_service_on = 1;
    ma_int32_t is_search_service_on = 1;
    ma_int32_t is_board_service_on = 1;
    ma_int32_t is_comment_service_on = 1;
    ma_int32_t is_consignment_service_on = 1;
    ma_policy_settings_bag_t *policy_bag = MA_CONTEXT_GET_POLICY_SETTINGS_BAG(self->context);
    
    if( 
           (MA_OK == (rc = ma_policy_settings_bag_get_int(policy_bag, MA_LOGGER_SERVICE_SECTION_NAME_STR, MA_LOGGER_KEY_IS_ENABLED_INT, MA_FALSE, &is_logger_on, is_logger_on)))
        && (MA_OK == (rc = ma_policy_settings_bag_get_int(policy_bag, MA_CRYPTO_SERVICE_SECTION_NAME_STR, MA_CRYPTO_KEY_IS_ENABLED_INT, MA_FALSE, &is_crypto_on,is_crypto_on)))
        && (MA_OK == (rc = ma_policy_settings_bag_get_int(policy_bag, MA_SCHEDULER_SERVICE_SECTION_NAME_STR, MA_SCHEDULER_KEY_IS_ENABLED_INT, MA_FALSE, &is_scheduler_on, is_scheduler_on )))
        && (MA_OK == (rc = ma_policy_settings_bag_get_int(policy_bag, MA_AH_SERVICE_SECTION_NAME_STR, MA_AH_KEY_IS_ENABLED_INT, MA_FALSE, &is_ah_on, is_ah_on)))
        && (MA_OK == (rc = ma_policy_settings_bag_get_int(policy_bag, MA_PROPERTY_SERVICE_SECTION_NAME_STR, MA_PROPERTY_KEY_IS_ENABLED_INT, MA_FALSE, &is_property_on, is_property_on)))
        && (MA_OK == (rc = ma_policy_settings_bag_get_int(policy_bag, MA_POLICY_SERVICE_SECTION_NAME_STR, MA_POLICY_KEY_IS_ENABLED_INT, MA_FALSE, &is_policy_on, is_policy_on)))
        && (MA_OK == (rc = ma_policy_settings_bag_get_int(policy_bag, MA_EVENT_SERVICE_SECTION_NAME_STR, MA_EVENT_KEY_IS_ENABLED_INT, MA_FALSE,&is_event_on,is_event_on )))
        && (MA_OK == (rc = ma_policy_settings_bag_get_int(policy_bag, MA_COMPAT_SERVICE_SECTION_NAME_STR, MA_COMPAT_KEY_IS_ENABLED_INT, MA_FALSE,&is_compat_on, is_compat_on)))
        && (MA_OK == (rc = ma_policy_settings_bag_get_int(policy_bag, MA_SENSOR_SERVICE_SECTION_NAME_STR, MA_SENSOR_KEY_IS_ENABLED_INT,MA_FALSE, &is_sensor_on, is_sensor_on)))
        && (MA_OK == (rc = ma_policy_settings_bag_get_int(policy_bag, MA_DATACHANNEL_SERVICE_SECTION_NAME_STR, MA_DATACHANNEL_KEY_IS_ENABLED_INT, MA_FALSE,&is_dc_on, is_dc_on)))
        && (MA_OK == (rc = ma_policy_settings_bag_get_int(policy_bag, MA_REPOSITORY_SERVICE_SECTION_NAME_STR, MA_REPOSITORY_KEY_IS_ENABLED_INT,MA_FALSE, &is_repository_on, is_repository_on)))
        && (MA_OK == (rc = ma_policy_settings_bag_get_int(policy_bag, MA_UPDATER_SERVICE_SECTION_NAME_STR, MA_UPDATER_KEY_IS_ENABLED_INT,MA_FALSE, &is_updater_on, is_updater_on)))
        ) {

            ma_service_manager_service_entry_t services[] = {
                {MA_SERVICE_ENABLE_IF(is_logger_on), MA_LOGGER_SERVICE_NAME_STR, &ma_logger_service_create},
                {MA_SERVICE_ENABLE_IF(is_crypto_on), MA_CRYPTO_SERVICE_NAME_STR, &ma_crypto_service_create},
                {MA_SERVICE_ENABLE_IF(is_network_on), MA_NETWORK_SERVICE_NAME_STR, &ma_network_service_create},
                {MA_SERVICE_ENABLE_IF(is_scheduler_on), MA_SCHEDULER_SERVICE_NAME_STR, &ma_scheduler_service_create},
                {MA_SERVICE_ENABLE_IF(is_ah_on), MA_AH_SERVICE_NAME_STR, &ma_ah_client_service_create},
                {MA_SERVICE_ENABLE_IF(is_task_on), "ma.service.task", &ma_task_service_create},
                {MA_SERVICE_ENABLE_IF(is_property_on), MA_PROPERTY_SERVICE_NAME_STR, &ma_property_service_create},
                {MA_SERVICE_ENABLE_IF(is_policy_on), MA_POLICY_SERVICE_NAME_STR, &ma_policy_service_create},
                {MA_SERVICE_ENABLE_IF(is_event_on), MA_EVENT_SERVICE_NAME_STR, &ma_event_service_create},
                {MA_SERVICE_ENABLE_IF(is_dc_on), MA_DC_TASK_SERVICE_NAME_STR, &ma_dc_task_service_create},
                {MA_SERVICE_ENABLE_IF(is_repository_on), MA_REPOSITORY_SERVICE_NAME_STR, &ma_repository_service_create},

                {MA_SERVICE_ENABLE_IF(is_updater_on), MA_UPDATER_SERVICE_NAME_STR, &ma_updater_service_create},
                {MA_SERVICE_ENABLE_IF(is_compat_on), MA_COMPAT_SERVICE_NAME_STR, &ma_compat_service_create},                
                {MA_SERVICE_ENABLE_IF(is_sensor_on), MA_SENSOR_SERVICE_NAME_STR, &ma_sensor_service_create},
//#ifdef MA_WINDOWS
//                {MA_SERVICE_OPTION_OPTIONAL|MA_SERVICE_OPTION_DYNLOAD, "lockdown_service", NULL, "ma_lockdown_service.dll", "ma_lockdown_service_create"},
//#endif
                {MA_SERVICE_ENABLE_IF(is_booking_service_on), MA_BOOKING_SERVICE_NAME_STR, &ma_booking_service_create},
                {MA_SERVICE_ENABLE_IF(is_profile_service_on), MA_PROFILE_SERVICE_NAME_STR, &ma_profile_service_create},
                {MA_SERVICE_ENABLE_IF(is_mail_service_on), MA_MAIL_SERVICE_NAME_STR, &ma_mail_service_create},
                {MA_SERVICE_ENABLE_IF(is_inventory_service_on), MA_INVENTORY_SERVICE_NAME_STR, &ma_inventory_service_create},
                {MA_SERVICE_ENABLE_IF(is_recorder_service_on), MA_RECORDER_SERVICE_NAME_STR, &ma_recorder_service_create},
                {MA_SERVICE_ENABLE_IF(is_location_service_on), MA_LOCATION_SERVICE_NAME_STR, &ma_location_service_create},
                {MA_SERVICE_ENABLE_IF(is_season_service_on), MA_SEASON_SERVICE_NAME_STR, &ma_season_service_create},
                {MA_SERVICE_ENABLE_IF(is_search_service_on), MA_SEARCH_SERVICE_NAME_STR, &ma_search_service_create},
                {MA_SERVICE_ENABLE_IF(is_board_service_on), MA_BOARD_SERVICE_NAME_STR, &ma_board_service_create},
                {MA_SERVICE_ENABLE_IF(is_comment_service_on), MA_COMMENT_SERVICE_NAME_STR, &ma_comment_service_create},
                {MA_SERVICE_ENABLE_IF(is_consignment_service_on), MA_CONSIGN_SERVICE_NAME_STR, &ma_consignment_service_create},
                {0, 0, 0}
            };

            //is_compat_on = 0;
            MA_LOG(self->logger, MA_LOG_SEV_INFO, "is_logger_on (%d)", is_logger_on);
            MA_LOG(self->logger, MA_LOG_SEV_INFO, "is_crypto_on (%d)", is_crypto_on);
            MA_LOG(self->logger, MA_LOG_SEV_INFO, "is_network_on (%d)", is_network_on);
            MA_LOG(self->logger, MA_LOG_SEV_INFO, "is_scheduler_on (%d)", is_scheduler_on);
            MA_LOG(self->logger, MA_LOG_SEV_INFO, "is_ah_on (%d)", is_ah_on);
            MA_LOG(self->logger, MA_LOG_SEV_INFO, "is_task_on (%d)", is_task_on);
            MA_LOG(self->logger, MA_LOG_SEV_INFO, "is_property_on (%d)", is_property_on);
            MA_LOG(self->logger, MA_LOG_SEV_INFO, "is_policy_on (%d)", is_policy_on);
            MA_LOG(self->logger, MA_LOG_SEV_INFO, "is_event_on (%d)", is_event_on);
            MA_LOG(self->logger, MA_LOG_SEV_INFO, "is_repository_on (%d)", is_repository_on);
            MA_LOG(self->logger, MA_LOG_SEV_INFO, "is_updater_on (%d)", is_updater_on);
            MA_LOG(self->logger, MA_LOG_SEV_INFO, "is_compat_on (%d)", is_compat_on);
            MA_LOG(self->logger, MA_LOG_SEV_INFO, "is_compat_registry_on (%d)", is_compat_registry_on);
            MA_LOG(self->logger, MA_LOG_SEV_INFO, "is_sensor_on (%d)", is_sensor_on);
            MA_LOG(self->logger, MA_LOG_SEV_INFO, "is_booking_on (%d)", is_booking_service_on);
            MA_LOG(self->logger, MA_LOG_SEV_INFO, "is_profile_on (%d)", is_profile_service_on);
            MA_LOG(self->logger, MA_LOG_SEV_INFO, "is_mail_on (%d)", is_mail_service_on);
            MA_LOG(self->logger, MA_LOG_SEV_INFO, "is_inventory_on (%d)", is_inventory_service_on);
            MA_LOG(self->logger, MA_LOG_SEV_INFO, "is_recorder_on (%d)", is_recorder_service_on);
            MA_LOG(self->logger, MA_LOG_SEV_INFO, "is_season_on (%d)", is_season_service_on);
            MA_LOG(self->logger, MA_LOG_SEV_INFO, "is_search_on (%d)", is_search_service_on);
            MA_LOG(self->logger, MA_LOG_SEV_INFO, "is_board_on (%d)", is_board_service_on);
            MA_LOG(self->logger, MA_LOG_SEV_INFO, "is_comment_on (%d)", is_comment_service_on);
            MA_LOG(self->logger, MA_LOG_SEV_INFO, "is_consignment_on (%d)", is_consignment_service_on);

            ma_service_manager_create_services(&self->service_manager, services);

    } else {
        MA_LOG(self->logger, MA_LOG_SEV_CRITICAL, "Unable to acquire service configuration for one or more services");
    }
    return rc;
}

static ma_error_t create_managed_mode_items(ma_io_service_t *self) {
    ma_error_t rc = MA_OK;
    if(MA_AGENT_MODE_MANAGED_INT == self->agent_mode) {
        /*managed mode register for property and spipe common property manager */
        if(MA_OK == (rc = ma_property_register_provider_callback(self->client, MA_SOFTWAREID_GENERAL_STR, agent_property_provider_cb, self->context))) {
            if(MA_OK == (rc = ma_property_register_provider_callback(self->client, MA_SYSPROPS_SOFTWAREID_STR, system_property_provider_cb, self->context))) {
                    if(MA_OK == (rc = ma_spipe_common_property_manager_create(self->context, &self->spipe_common_mgr))) {                                                       
                        if(MA_OK == (rc = ma_spipe_misc_handler_create(self->context, &self->spipe_misc_handler))) {
							ma_ah_client_service_register_spipe_decorator(MA_CONTEXT_GET_AH_CLIENT(self->context), ma_spipe_common_property_manager_get_decorator(self->spipe_common_mgr));
							ma_ah_client_service_register_spipe_handler(MA_CONTEXT_GET_AH_CLIENT(self->context), ma_spipe_common_property_manager_get_handler(self->spipe_common_mgr)); 
							ma_ah_client_service_register_spipe_decorator(MA_CONTEXT_GET_AH_CLIENT(self->context), ma_spipe_misc_handler_get_decorator(self->spipe_misc_handler));
							ma_ah_client_service_register_spipe_handler(MA_CONTEXT_GET_AH_CLIENT(self->context), ma_spipe_misc_handler_get_handler(self->spipe_misc_handler));                                                        
							return MA_OK;
						}
						ma_spipe_common_property_manager_release(self->spipe_common_mgr);
                    }
                    ma_policy_unregister_notification_callback(self->client, MA_SOFTWAREID_GENERAL_STR);
                ma_property_unregister_provider_callback(self->client, MA_SYSPROPS_SOFTWAREID_STR);
            }
            ma_property_unregister_provider_callback(self->client, MA_SOFTWAREID_GENERAL_STR);
        }
		/*create relay handler.*/		
    }
    return rc;
}

static void release_managed_mode_items(ma_io_service_t *self) {
    if(MA_AGENT_MODE_MANAGED_INT == self->agent_mode) {
        ma_ah_client_service_unregister_spipe_decorator(MA_CONTEXT_GET_AH_CLIENT(self->context), ma_spipe_common_property_manager_get_decorator(self->spipe_common_mgr));
        ma_ah_client_service_unregister_spipe_handler(MA_CONTEXT_GET_AH_CLIENT(self->context), ma_spipe_common_property_manager_get_handler(self->spipe_common_mgr));   
		(void)ma_spipe_common_property_manager_release(self->spipe_common_mgr);

		ma_ah_client_service_unregister_spipe_decorator(MA_CONTEXT_GET_AH_CLIENT(self->context), ma_spipe_misc_handler_get_decorator(self->spipe_misc_handler));
        ma_ah_client_service_unregister_spipe_handler(MA_CONTEXT_GET_AH_CLIENT(self->context), ma_spipe_misc_handler_get_handler(self->spipe_misc_handler)); 
        (void)ma_spipe_misc_handler_release(self->spipe_misc_handler);

        (void)ma_policy_unregister_notification_callback(self->client, MA_SOFTWAREID_GENERAL_STR);
        (void)ma_property_unregister_provider_callback(self->client, MA_SYSPROPS_SOFTWAREID_STR);
        (void)ma_property_unregister_provider_callback(self->client, MA_SOFTWAREID_GENERAL_STR);
    }
}



static ma_error_t policy_notification_cb(ma_client_t *client, const char *product_id, const ma_policy_uri_t *info_uri, void *cb_data) {
    if(client && product_id && info_uri && cb_data) {
        ma_io_service_t *self = (ma_io_service_t *)cb_data;
        ma_error_t rc = MA_OK;

        /*TODO - we need to see policies to see actually for any services enablement/disablement, objects reconfiguration(crypto) */
        /*TODO -service manager needs to be somehow abstracted out, loading of service dll , start stop etc */
        if(MA_OK == (rc = ma_agent_policy_settings_bag_refresh(self->policy_settings_bag))) 
            rc = ma_service_manager_configure_all(&self->service_manager, self->context, MA_SERVICE_CONFIG_MODIFIED);

        return rc;
    }
    return MA_ERROR_INVALIDARG;
}


static ma_error_t regnerate_guid(ma_io_service_t *self, ma_ds_t *ds, ma_configurator_t *configurator) {
	char guid[UUID_LEN_TXT] = {0};
	ma_error_t rc = MA_OK;
	const ma_network_system_property_t *net = NULL;

	if(NULL != (net = ma_system_property_network_get(self->system_property, MA_FALSE))) {
		rc = ma_uuid_generate(1, (unsigned char*)net->mac_address, guid);
	}else {
		MA_LOG(self->logger, MA_LOG_SEV_TRACE, "generating guid without mac address");
		rc = ma_uuid_generate(0, NULL, guid);
	}

	if(MA_OK == rc) {
		rc = ma_ds_set_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_GUID_STR, guid, -1);
		(void)ma_configurator_set_guid(configurator, guid);
	}
	return rc;
}

static ma_bool_t need_guid_generation(ma_io_service_t *self, ma_ds_t *ds) {
	ma_bool_t need_generation = MA_FALSE;
    ma_error_t rc = MA_OK;
    ma_int16_t agent_mode = MA_AGENT_MODE_UNMANAGED_INT;
    
    if((MA_OK == (rc = ma_ds_get_int16(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_MODE_INT, &agent_mode))) 
		&& (MA_AGENT_MODE_MANAGED_INT == agent_mode)) {
		ma_bool_t is_exists = MA_FALSE;
		if(MA_OK == (rc = ma_ds_is_key_exists(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_GUID_STR, &is_exists))) {
			const ma_os_system_property_t *os = NULL;
			if(MA_FALSE == is_exists) {
				need_generation = MA_TRUE;
				MA_LOG(self->logger, MA_LOG_SEV_TRACE, "need guid regeneration because guid doesnt exists");
			}
			
			#if defined (MA_WINDOWS)
			if(NULL != (os = ma_system_property_os_get(self->system_property, MA_FALSE))) {
				if(!( (0 == strcmp(os->smbios_uuid ,"00000000-0000-0000-0000-000000000000")) 
						|| (0 == strcmp(os->smbios_uuid ,"FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF")))) {
					ma_buffer_t *buffer = NULL;
					if(MA_OK == (rc = ma_ds_get_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_SMBIOS_UUID_STR, &buffer))) {
						const char *stored_smbios_uuid = NULL;
						size_t size = -1;
						if(MA_OK == (rc = ma_buffer_get_string(buffer, &stored_smbios_uuid, &size))) {
							if(strcmp(stored_smbios_uuid, os->smbios_uuid)) {
								need_generation = MA_TRUE;
								MA_LOG(self->logger, MA_LOG_SEV_INFO, "need guid regeneration because of smbios uuid changed");
								(void)ma_ds_set_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_SMBIOS_UUID_STR, os->smbios_uuid,-1);
							}
						}
						(void)ma_buffer_release(buffer);
					}else 
						(void)ma_ds_set_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_SMBIOS_UUID_STR, os->smbios_uuid,-1);
				}else 
					MA_LOG(self->logger, MA_LOG_SEV_ERROR , "retrieved incorect 0000/FFFF smbios uuid");
			}else 
				MA_LOG(self->logger, MA_LOG_SEV_ERROR , "failed to get os properties");
			#endif /*MA_WINDOWS*/
		}
	}
	return need_generation;
}

static ma_error_t setup_agent_configuration(ma_io_service_t *self) {
    ma_ds_t *ds = NULL;
    if(NULL != (ds = ma_configurator_get_datastore(self->configurator))) {
        const ma_misc_system_property_t *misc = NULL;
		ma_error_t rc = MA_OK;

		if(MA_TRUE == need_guid_generation(self,ds)) {
			if(MA_OK != (rc = regnerate_guid(self, ds, self->configurator)))
				MA_LOG(self->logger, MA_LOG_SEV_ERROR, "failed to generate agent guid, %d", rc);
		}

        if(NULL == (misc = ma_system_property_misc_get(self->system_property, MA_FALSE))) {
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to get the misc property interface.");
            rc = MA_ERROR_DS_OBJECT_NOT_FOUND;
        }
        else {                        
            if(MA_OK != (rc = ma_ds_set_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_COMPUTER_NAME_STR, misc->computername, -1))) 
                MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to set Agent computer name.");
        }

        return rc;
    }
    return MA_ERROR_DS_OBJECT_NOT_FOUND;
}

ma_error_t  ma_io_service_get_event_loop(ma_io_service_t *self, struct ma_event_loop_s **event_loop) {
    if(self && event_loop) {
        *event_loop = ma_msgbus_get_event_loop(self->msgbus);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t  ma_io_service_get_service(ma_io_service_t *self, const char *service_name, ma_service_t **service) {
    if(self && service_name && service) {
        *service = ma_service_manager_find_service(&self->service_manager, service_name);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static void publish_ma_info_event_message(ma_io_service_t *self, const char *info_event) {
	ma_message_t *msg = NULL ;
	ma_error_t rc = MA_OK ;

	if(MA_OK == (rc = ma_message_create(&msg))) {
		ma_msgbus_endpoint_t *ep = NULL ;

			(void)ma_message_set_property(msg, MA_PROP_KEY_REQUEST_TYPE_STR, MA_PROP_VALUE_AGENT_INFO_COLLECT) ;
			(void)ma_message_set_property(msg, MA_INFO_MSG_KEY_EVENT_TYPE_STR, info_event) ;

			if(MA_OK == (rc = ma_msgbus_endpoint_create(self->msgbus, MA_IO_SERVICE_NAME_STR, NULL, &ep))) {
				(void)ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_INPROC) ;
				(void)ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
		
				
				if(MA_OK != (rc = ma_msgbus_send_and_forget(ep, msg)))
					MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to send ma info message, rc = <%d>.", rc) ;

				(void)ma_msgbus_endpoint_release(ep) ;	ep = NULL ;
			}
			(void)ma_message_release(msg) ; msg = NULL ;
	}
	else
		MA_LOG(self->logger, MA_LOG_SEV_INFO, "Failed to create ma info event message, rc = <%d>.", rc) ;

	return ;
}
