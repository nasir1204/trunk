#ifndef MA_RELAY_HANDLER_H_INCLUDED
#define MA_RELAY_HANDLER_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_message.h"

#include "ma/internal/defs/ma_object_defs.h"

MA_CPP(extern "C" {)

typedef struct ma_relay_handler_s ma_relay_handler_t;

ma_error_t ma_relay_handler_create(ma_relay_handler_t **self);

ma_error_t ma_relay_handler_configure(ma_relay_handler_t *self, ma_context_t *context, unsigned hint);

ma_error_t ma_relay_handler_start(ma_relay_handler_t *self);

ma_error_t ma_relay_handler_refresh(ma_relay_handler_t *self);

ma_error_t ma_relay_handler_stop(ma_relay_handler_t *self);

ma_error_t ma_relay_handler_handle_msg(ma_relay_handler_t *self, const char *msg_type, ma_msgbus_client_request_t *c_request, ma_message_t *request);

ma_error_t ma_relay_handler_release(ma_relay_handler_t *self);

MA_CPP(})

#endif

