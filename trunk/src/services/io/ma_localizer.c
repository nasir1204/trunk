#include "ma_localizer.h"
#include "ma/datastore/ma_ds_ini.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal/utils/platform/ma_system_property.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/utils/datastructures/ma_hash.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/utils/configurator/ma_configurator.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static const char section[] = "STRINGTABLE";
const char res_ini_file[] = "AgentRes.ini";

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "io service"
#ifdef MA_WINDOWS
#include <Windows.h>
#endif

struct ma_localizer_s {
    ma_context_t *context;
    ma_ds_t *ds;
    char lang_id[MA_MAX_NAME];
    char path[MA_MAX_PATH_LEN];
};

static ma_error_t ma_localizer_load_ini(ma_localizer_t *localizer, const char *path);
static ma_error_t ma_localizer_unload_ini(ma_localizer_t *localizer);

ma_error_t ma_localizer_create(ma_context_t *context, ma_localizer_t **localizer) {
    if(localizer) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;

        if((*localizer = (ma_localizer_t*)calloc(1, sizeof(ma_localizer_t)))) {
            (*localizer)->context = context; err = MA_OK;
            if(MA_OK != (err = ma_localizer_start(*localizer))) {
                if((*localizer)->ds)ma_localizer_unload_ini(*localizer);
                *localizer = NULL;
            }
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_localizer_load_ini(ma_localizer_t *localizer, const char *path) {
    if(localizer && path) {
        ma_error_t err = MA_OK;
        char buf[MA_MAX_PATH_LEN] = {0};
        const char *lang_code = NULL;
        const char *lang = NULL;
        size_t len = 0;
        ma_configurator_t *configurator = MA_CONTEXT_GET_CONFIGURATOR(localizer->context);
        ma_ds_t *ds = ma_configurator_get_datastore(configurator);
        const ma_misc_system_property_t *misc_prop = NULL;
        ma_buffer_t *buffer = NULL;

        misc_prop = ma_system_property_misc_get(MA_CONTEXT_GET_SYSTEM_PROPERTY(localizer->context), MA_FALSE);
        if(misc_prop)strcpy(localizer->lang_id, lang_code = misc_prop->language_code);
        MA_LOG(MA_CONTEXT_GET_LOGGER(localizer->context), MA_LOG_SEV_DEBUG, "lang resource folder(%s)", lang_code);
        if(MA_OK == (err = ma_ds_get_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_LANGUAGE_STR, &buffer))) {
            if(MA_OK == (err = ma_buffer_get_string(buffer, &lang, &len))) {
                if(lang && len && strcmp(lang, "N/A")) {
                    strcpy(localizer->lang_id, lang_code = lang);
                    lang_code = lang;
                    MA_LOG(MA_CONTEXT_GET_LOGGER(localizer->context), MA_LOG_SEV_DEBUG, "lang ds resource folder(%s)", lang_code);
                }
            }
            (void)ma_buffer_release(buffer);
        }
        MA_MSC_SELECT(_snprintf, snprintf)(buf, MA_MAX_PATH_LEN - 1, "%s%s%c%s", path, localizer->lang_id, MA_PATH_SEPARATOR, res_ini_file);
        MA_LOG(MA_CONTEXT_GET_LOGGER(localizer->context), MA_LOG_SEV_DEBUG, "localizer resource file(%s) opening..", buf);
        strncpy(localizer->path, buf, strlen(buf));

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_localizer_unload_ini(ma_localizer_t *localizer) {
    if(localizer) {
        ma_error_t err = MA_OK;

        if(localizer->ds) {
            MA_LOG(MA_CONTEXT_GET_LOGGER(localizer->context), MA_LOG_SEV_DEBUG, "localizer db unloading..");
            if(MA_OK != (err = ma_ds_ini_release((ma_ds_ini_t*)localizer->ds))) {
                MA_LOG(MA_CONTEXT_GET_LOGGER(localizer->context), MA_LOG_SEV_DEBUG, "localizer db unloading failed, last error(%d)", err);
            }
            localizer->ds = 0;
        }

        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_localizer_start(ma_localizer_t *localizer) {
    if(localizer) {
        ma_error_t err = MA_OK;

        MA_LOG(MA_CONTEXT_GET_LOGGER(localizer->context), MA_LOG_SEV_DEBUG, "localizer db loading..");
        if(MA_OK != (err = ma_localizer_load_ini(localizer, ma_configurator_get_agent_install_path(MA_CONTEXT_GET_CONFIGURATOR(localizer->context))))) {
            MA_LOG(MA_CONTEXT_GET_LOGGER(localizer->context), MA_LOG_SEV_DEBUG, "localizer db loading failed, last error(%d)", err);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_localizer_stop(ma_localizer_t *localizer) {
    if(localizer) {
        return ma_localizer_unload_ini(localizer);
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_localizer_release(ma_localizer_t *localizer) {
    if(localizer) {
        (void)ma_localizer_stop(localizer);
        free(localizer);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_localizer_get_localize_string(ma_localizer_t *localizer, const char *msg, ma_buffer_t **localized_buffer) {
    if(localizer && msg && localized_buffer) {
        ma_error_t err = MA_OK;
        ma_uint32_t hash = 0;
#ifdef MA_WINDOWS
        DWORD ret = 0;
#endif
        if(hash = ma_hash_compute(msg, strlen(msg))) {
           // ma_wbuffer_t *wbuf = NULL;
            char hash_buf[MA_MAX_LEN] = {0};
            char str[MA_MAX_LEN] = {0};
            MA_MSC_SELECT(_snprintf, snprintf)(hash_buf, MA_MAX_LEN, "%u", hash);
           /* if(MA_OK == (err = ma_ds_get_wstr(localizer->ds, section, hash_buf, &wbuf))) {
                const wchar_t *wstr = NULL;
                size_t len = 0;
                if(MA_OK == (err = ma_wbuffer_get_string(wbuf, &wstr, &len))) {
                    ma_variant_t *variant = NULL;
                    if(MA_OK == ma_variant_create_from_wstring(wstr, len, &variant)) {
                        err = ma_variant_get_string_buffer(variant, localized_buffer);
                        ma_variant_release(variant);
                    }
                }
               
            } else
                MA_LOG(MA_CONTEXT_GET_LOGGER(localizer->context), MA_LOG_SEV_DEBUG, "localizing string(%s) failed, last error(%d)", hash_buf, err); (void)ma_wbuffer_release(wbuf);*/
#ifdef MA_WINDOWS
            if(0!= (ret = GetPrivateProfileStringA(section, hash_buf, "", str, MA_MAX_LEN, localizer->path))) {

                ma_variant_t *variant = NULL;
                if(MA_OK == ma_variant_create_from_string(str, strlen(str), &variant)) {
                    err = ma_variant_get_string_buffer(variant, localized_buffer);
                    ma_variant_release(variant);
                }
            } 
            else
				MA_LOG(MA_CONTEXT_GET_LOGGER(localizer->context), MA_LOG_SEV_DEBUG, "localizing string(%s) failed, GetPrivateProfileStringA returns (%d) (%d)", hash_buf, ret, GetLastError());
            
#else 
			{
				ma_variant_t *variant = NULL;
                if(MA_OK == ma_variant_create_from_string(msg, strlen(msg), &variant)) {
                    err = ma_variant_get_string_buffer(variant, localized_buffer);
                    ma_variant_release(variant);
                }
			}
#endif
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_localizer_get_language(ma_localizer_t *localizer, const char **lang) {
    if(localizer && lang) {
        *lang = localizer->lang_id;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

