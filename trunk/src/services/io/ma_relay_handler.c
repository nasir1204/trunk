#include "ma/ma_service.h"
#include "ma_relay_handler.h"
#include "ma/internal/clients/relay/ma_relay_discovery_request.h"

#include "ma/ma_log.h"
#include "ma/ma_msgbus.h"
#include "ma/internal/core/msgbus/ma_msgbus_private.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"

#include "ma/internal/defs/ma_udp_defs.h"
#include "ma/internal/defs/ma_relay_defs.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/defs/ma_io_defs.h"
#include "ma/internal/services/ma_agent_policy_settings_bag.h"
#include "ma/proxy/ma_proxy_config.h"
#include "ma/internal/utils/proxy/ma_proxy_internal.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME	"relay"

#define MA_REALY_DISCOVERY_MAX_RELAY_COUNT								6
#define MA_REALY_DISCOVERY_RFRESH_TIMEOUT_SECONDS						2
#define MA_RELAY_DISCOVERY_CLIENT_REQUEST_QUEUE_SIZE					2

static void send_relay_response(ma_relay_handler_t *self, ma_proxy_list_t *relay_list, ma_msgbus_client_request_t *c_request);
static void relay_discovery_response(void *user_data);

struct ma_relay_handler_s{

	ma_context_t					*context;

	ma_udp_client_t					*udp_client;

	ma_logger_t						*logger;

	ma_bool_t						relay_client_enabled;

	ma_bool_t						relay_server_enabled;

	ma_relay_discovery_policy_t		policy;

	ma_relay_discovery_request_t	*relay_discovery_req;		

	ma_msgbus_client_request_t		*c_request[MA_RELAY_DISCOVERY_CLIENT_REQUEST_QUEUE_SIZE];
	ma_uint16_t						c_req_size;
};

ma_error_t ma_relay_handler_create(ma_relay_handler_t **self){
	if(self){
		ma_relay_handler_t *handler = (ma_relay_handler_t*) calloc(1, sizeof(ma_relay_handler_t));
		if(handler){
			if(MA_OK == ma_udp_client_create(&handler->udp_client)){
				*self = handler;
				return MA_OK;
			}
			free(handler);
		}
		return MA_ERROR_OUTOFMEMORY;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_relay_handler_configure(ma_relay_handler_t *self, ma_context_t *context, unsigned hint){
	if(self && context){

		ma_int32_t int32 = 0;
		ma_policy_settings_bag_t *pso_bag = MA_CONTEXT_GET_POLICY_SETTINGS_BAG(context) ;
		
		if(MA_SERVICE_CONFIG_NEW == hint){
			self->context = context;
			self->logger = MA_CONTEXT_GET_LOGGER(context);
			(void)ma_udp_client_set_logger(self->udp_client, MA_CONTEXT_GET_LOGGER(context));
			(void)ma_udp_client_set_event_loop(self->udp_client, ma_msgbus_get_event_loop(MA_CONTEXT_GET_MSGBUS(context)));						
			(void)ma_udp_client_set_loop_worker(self->udp_client, ma_msgbus_get_loop_worker(MA_CONTEXT_GET_MSGBUS(context)));						
		}

		self->relay_server_enabled = self->relay_client_enabled = MA_FALSE;
		if(MA_OK == ma_policy_settings_bag_get_int(pso_bag, MA_RELAY_SERVICE_SECTION_NAME_STR, MA_RELAY_KEY_IS_ENABLED_INT, MA_FALSE, &int32, 0))
			self->relay_server_enabled = (int32 != 0);

		if(self->relay_server_enabled){
			/*relay server can't use relay discovery.*/
			self->relay_client_enabled = MA_FALSE;		
		}
		else{
			int32 = 0;
			if(MA_OK == ma_policy_settings_bag_get_int(pso_bag, MA_RELAY_SERVICE_SECTION_NAME_STR, MA_RELAY_KEY_ENABLE_CLIENT_INT, MA_FALSE, &int32, 0))
				self->relay_client_enabled = (int32 != 0);

			if(self->relay_client_enabled){
				const char *addr = MA_UDP_MULTICAST_ADDR_STR;
				int32 = MA_UDP_DEFAULT_PORT_INT;

				if(MA_OK == ma_policy_settings_bag_get_int(pso_bag, MA_UDP_SERVICE_SECTION_NAME_STR, MA_UDP_KEY_BROADCAST_PORT_INT, MA_FALSE, &int32, int32))
					self->policy.discovery_port = int32;
				if(MA_OK == ma_policy_settings_bag_get_str(pso_bag, MA_UDP_SERVICE_SECTION_NAME_STR, MA_UDP_KEY_MULTICAST_ADDRESS_STR, MA_FALSE, &addr, addr) && addr){
					if(self->policy.discovery_multicast_addr) free(self->policy.discovery_multicast_addr);
					self->policy.discovery_multicast_addr = strdup(addr);
				}

				self->policy.max_no_of_relays_to_discover = MA_REALY_DISCOVERY_MAX_RELAY_COUNT;
				self->policy.discovery_timeout_ms =	MA_REALY_DISCOVERY_RFRESH_TIMEOUT_SECONDS * 1000;
				self->policy.sync_discovery = MA_FALSE;		
			}
		}

		if(self->relay_client_enabled){
			if(!self->relay_discovery_req){
				ma_error_t rc = MA_OK;
				if(MA_OK == (rc = ma_relay_discovery_request_create(self->udp_client, &self->relay_discovery_req))){
					(void)ma_relay_discovery_request_set_final_cb(self->relay_discovery_req, &relay_discovery_response, self);
					if(MA_SERVICE_CONFIG_MODIFIED == hint){
						/*first time enabled so let discover.*/
						MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "relay usage is enabled, so start the discovery.");
						(void)ma_relay_handler_start(self);
					}
				}
				else
					MA_LOG(self->logger, MA_LOG_SEV_ERROR, "failed to create relay discovery request, rc = <%d>.", rc);								
			}
		}
		else{
			if(self->relay_discovery_req) ma_relay_discovery_request_release(self->relay_discovery_req);		
			self->relay_discovery_req = NULL;
		}
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

static ma_error_t do_relay_discovery(ma_relay_handler_t *self){
	if(self){
		if(self->relay_discovery_req){
			MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "running a fresh relay discovery.");
			if(MA_OK == ma_relay_discovery_request_set_policy(self->relay_discovery_req, &self->policy))
				(void)ma_relay_discovery_request_send(self->relay_discovery_req);						
		}
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_relay_handler_start(ma_relay_handler_t *self){
	if(self){		
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_relay_handler_stop(ma_relay_handler_t *self){
	if(self){
		if(self->relay_discovery_req){
			MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "stopping relay discovery.");
			(void)ma_relay_discovery_request_cancel(self->relay_discovery_req);					
		}
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_relay_handler_refresh(ma_relay_handler_t *self){
	if(self){				
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

static void send_relay_response(ma_relay_handler_t *self, ma_proxy_list_t *relay_list, ma_msgbus_client_request_t *c_request){
	ma_variant_t *var = NULL;
	ma_error_t rc = MA_OK;
	ma_message_t *response = NULL;

	(void)ma_message_create(&response);
	if(relay_list && MA_OK == ma_proxy_list_to_variant(relay_list, &var)){
		(void)ma_message_set_payload(response, var);						
		(void)ma_variant_release(var);
	}
	
	if(MA_OK != (rc = ma_msgbus_server_client_request_post_result(c_request, rc, response))) 
		MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "msgbus posting client setting language change request failed, last error(%d)", rc);

	(void)ma_message_release(response);
}

static void relay_discovery_response(void *user_data){
	ma_relay_handler_t *self = (ma_relay_handler_t*) user_data;
	MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "sending relay discovery response for pending requests <%d>", self->c_req_size);
	if(self->c_req_size){
		int i = 0;
		for(i = 0; i<self->c_req_size; i++){
			if(self->c_request[i]){
				ma_proxy_list_t *proxy_list = NULL;			
				(void)ma_relay_discovery_request_get_relays(self->relay_discovery_req, &proxy_list);
				send_relay_response(self, proxy_list, self->c_request[i]);
				(void)ma_msgbus_client_request_release(self->c_request[i]);
				self->c_request[i] = NULL;
			}
		}
	}
	self->c_req_size = 0;
}

ma_error_t ma_relay_handler_handle_msg(ma_relay_handler_t *self, const char *msg_type, ma_msgbus_client_request_t *c_request, ma_message_t *request){
	if(self && msg_type && c_request && request){
		if(self->relay_discovery_req){
			if(!strcmp(msg_type, MA_IO_MSG_TYPE_GET_RELAY_INFO)){

				if(MA_RELAY_DISCOVERY_CLIENT_REQUEST_QUEUE_SIZE == self->c_req_size){
					ma_proxy_list_t *proxy_list = NULL;
					if(MA_OK == ma_relay_discovery_request_get_relays(self->relay_discovery_req, &proxy_list) && proxy_list){ 
						MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "sending the discoverd relays.");
						send_relay_response(self, proxy_list, c_request);
					}
					else{ 
						MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "relay handler doesn't have any relay, can't process anymore request.");
						send_relay_response(self, NULL, c_request);
					}
				}
				else {
					MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "discover and send the relay.");
					ma_msgbus_client_request_add_ref(self->c_request[self->c_req_size] = c_request);
					++self->c_req_size;
					do_relay_discovery(self);
				}
			}
			else{				
				send_relay_response(self, NULL, c_request);
			}
		}
		else{
			MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "relay is disabled.");
			send_relay_response(self, NULL, c_request);
		}
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_relay_handler_release(ma_relay_handler_t *self){
	if(self){
		if(self->relay_discovery_req) ma_relay_discovery_request_release(self->relay_discovery_req);
		if(self->udp_client) ma_udp_client_release(self->udp_client);
		free(self);
	}
	return MA_ERROR_INVALIDARG;
}
