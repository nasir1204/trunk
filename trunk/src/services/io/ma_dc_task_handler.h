#ifndef MA_DC_TASK_HANDLER_H_INCLUDED
#define MA_DC_TASK_HANDLER_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_variant.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/defs/ma_dc_task_service_defs.h"

MA_CPP(extern "C" {)

typedef struct ma_dc_task_handler_s ma_dc_task_handler_t;

typedef struct ma_dc_task_s ma_dc_task_t;
struct ma_dc_task_s{	
	char         *dc_task_name;
	char         *owner;
	char         *creator;
	char		 *task_id;
	char		 *type;	
	char		 *name;		
	ma_variant_t *settings;	
	ma_int64_t	 corelation_id;
};

void ma_dc_task_init(ma_dc_task_t *dc_task);

ma_error_t ma_dc_task_deinit(ma_dc_task_t *dc_task);


ma_error_t ma_dc_task_handler_create(ma_context_t *context, ma_dc_task_handler_t **self);

ma_error_t ma_dc_task_handler_start(ma_dc_task_handler_t *self);

ma_error_t ma_dc_task_handler_stop(ma_dc_task_handler_t *self);

ma_error_t ma_dc_task_handler_submit(ma_dc_task_handler_t *self, ma_dc_task_t *task);

ma_error_t ma_dc_task_handler_release(ma_dc_task_handler_t *self);

MA_CPP(})

#endif

