#include "ma_crypto_service.h"
#include "ma/internal/utils/macrypto/ma_crypto.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/ma_strdef.h"

#include "ma/ma_msgbus.h"
#include "ma/ma_message.h"
#include "ma/crypto/ma_crypto.h"

#include <stdlib.h>
#include <string.h>

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "cryptoservice"

struct ma_crypto_service_s {
    ma_service_t                        base;
	
	ma_context_t                        *context;

    ma_crypto_t                         *crypto;

    ma_msgbus_server_t                  *server;

    ma_crypto_mode_t                    crypto_mode;
	
};

static ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint);

static ma_error_t service_start(ma_service_t *service);

static ma_error_t service_stop(ma_service_t *service);

static void service_destroy(ma_service_t *service);

static const ma_service_methods_t methods = {
    &service_configure,
    &service_start,
    &service_stop,
    &service_destroy
};	


ma_error_t ma_crypto_service_create(const char *service_name, ma_service_t **service) {
    if(service) {
        ma_error_t rc = MA_OK;
        ma_crypto_service_t *self = (ma_crypto_service_t *)calloc(1, sizeof(ma_crypto_service_t));
        
        if(!self)   return MA_ERROR_OUTOFMEMORY;

        if(MA_OK == (rc = ma_crypto_create(&self->crypto))) {
            ma_service_init((ma_service_t *)self, &methods, service_name, self);
            *service = (ma_service_t *)self;
            return MA_OK;
        }

        free(self);
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

/* TODO - register for prevent of loop run and then do the whole thing */
static ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint) {
    if(service && context) {
        ma_error_t rc = MA_OK;
        if(MA_SERVICE_CONFIG_NEW == hint) {
            ma_crypto_service_t *self = (ma_crypto_service_t *)service;
            ma_configurator_t *configurator = MA_CONTEXT_GET_CONFIGURATOR(context);
            ma_ds_t *ds = ma_configurator_get_datastore(configurator);

            if(!configurator || !ds) return MA_ERROR_PRECONDITION;
            else {
                ma_crypto_ctx_t *ctx = NULL;
                if(MA_OK == (rc = ma_msgbus_server_create(MA_CONTEXT_GET_MSGBUS(context), MA_CRYPTO_SERVICE_NAME_STR, &self->server))){
                    (void)ma_msgbus_server_setopt(self->server, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);  
                    (void)ma_msgbus_server_setopt(self->server, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
                    if(MA_OK == (rc = ma_crypto_ctx_create(&ctx))) {
                        ma_buffer_t *buffer = NULL;
                        const char *p = NULL;
                        size_t size = 0;
                        ma_crypto_agent_mode_t agent_mode = MA_CRYPTO_AGENT_UNMANAGED;
                        ma_int16_t int_16_value = 0;
						
                        self->crypto_mode = MA_CRYPTO_MODE_NON_FIPS;
                        (void)ma_crypto_ctx_set_logger(ctx,MA_CONTEXT_GET_LOGGER(context));
                        (void)ma_crypto_ctx_set_role(ctx, MA_CRYPTO_ROLE_USER);
                        (void)ma_crypto_ctx_set_crypto_lib_path(ctx, ma_configurator_get_agent_lib_path(configurator, MA_FALSE));

                        if(MA_OK == (rc = ma_ds_get_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_KEYSTORE_PATH_STR, &buffer))) {
                            if(MA_OK == (rc = ma_buffer_get_string(buffer, &p, &size))) 
                                ma_crypto_ctx_set_keystore(ctx, p);
                            ma_buffer_release(buffer);
                        }

                        if((MA_OK == rc) && (MA_OK == (rc = ma_ds_get_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_CERTSTORE_PATH_STR, &buffer)))) {
                            if(MA_OK == (rc = ma_buffer_get_string(buffer, &p, &size))) 
                                ma_crypto_ctx_set_certstore(ctx, p);
                            ma_buffer_release(buffer);
                        }

						int_16_value = 0;
                        if((MA_OK == rc) && (MA_OK == (rc = ma_ds_get_int16(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_MODE_INT, (ma_int16_t*)(&int_16_value))))) 
							agent_mode = int_16_value;
                            ma_crypto_ctx_set_agent_mode(ctx, agent_mode);
						
						int_16_value = 0;
                        if((MA_OK == rc) && (MA_OK == (rc = ma_ds_get_int16(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_CRYPTO_MODE_INT, (ma_int16_t*)(&int_16_value)))))
							self->crypto_mode = int_16_value;
                            ma_crypto_ctx_set_fips_mode(ctx, self->crypto_mode);
            
                        
                        if((MA_OK == rc) && (MA_OK == (rc = ma_crypto_initialize(ctx, self->crypto)))) {
                            (void) ma_crypto_ctx_release(ctx);
                            ma_context_add_object_info(context, MA_OBJECT_CRYPTO_NAME_STR, (void *const *)&self->crypto);   
                            self->context = context;
                            return MA_OK;
                        }
                        (void) ma_crypto_ctx_release(ctx);
                    }
                    (void)ma_msgbus_server_release(self->server);
                }
            }
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t crypto_server_handler(ma_msgbus_server_t *server, ma_message_t *request_msg, void *cb_data, ma_msgbus_client_request_t *c_request);
static ma_error_t service_start(ma_service_t *service) {
	ma_crypto_service_t *self = (ma_crypto_service_t*)service;
	if(self){
        ma_error_t rc = MA_OK ;
        if(MA_OK != (rc = ma_msgbus_server_start(self->server, crypto_server_handler, self)))
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_WARNING, "Failed to start crypto server for client, continuing without it, <%d>", rc);

		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

static ma_error_t service_stop(ma_service_t *service) {
	ma_crypto_service_t *self = (ma_crypto_service_t*)service;
	if(self){		
        (void) ma_msgbus_server_stop(self->server);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

static void service_destroy(ma_service_t *service) {
    if(service) {
        ma_crypto_service_t *self = (ma_crypto_service_t *)service;
        (void) ma_msgbus_server_release(self->server);
		(void) ma_crypto_deinitialize(self->crypto);
        (void) ma_crypto_release(self->crypto);
        free(self);
    }
}

static ma_error_t reply_to_clients(ma_crypto_service_t *self, ma_error_t status, ma_message_t *reply_msg, ma_msgbus_client_request_t *c_request) {
	ma_error_t rc = MA_OK;
	if(MA_OK == (rc = ma_msgbus_server_client_request_post_result(c_request, status, reply_msg))) {            
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Reply status <%d> to crypto clients", status); 
	}
	else
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to send the reply status and message");   
	return rc;
}

static ma_error_t add_bytebuffer_as_payload(ma_bytebuffer_t *buffer, ma_message_t *msg) {
    ma_error_t rc = MA_OK;
    ma_variant_t *variant = NULL;
    if(MA_OK == (rc = ma_variant_create_from_raw(ma_bytebuffer_get_bytes(buffer), ma_bytebuffer_get_size(buffer), &variant))) {
        rc = ma_message_set_payload(msg, variant);
        ma_variant_release(variant);
    }
    return rc;
}

static ma_error_t get_buffer_from_payload(ma_message_t *msg, const unsigned char **data, size_t *data_len) {
    ma_error_t rc = MA_OK;
    ma_variant_t *payload = NULL;
    if(MA_OK == (rc = ma_message_get_payload(msg, &payload))) {
        ma_buffer_t *buffer = NULL;
        if(MA_OK == (rc = ma_variant_get_raw_buffer(payload, &buffer))) {
            rc = ma_buffer_get_raw(buffer, data, data_len);
            ma_buffer_release(buffer);
        }
        ma_variant_release(payload);
    }
    return rc;
}


static ma_error_t encrypt_handle(ma_crypto_service_t *self, ma_message_t *request_msg, ma_message_t *response_msg, ma_msgbus_client_request_t *c_request) {
    const char *algo_id_str = NULL;
    ma_error_t rc = MA_OK;
    if(MA_OK == (rc = ma_message_get_property(request_msg, MA_PROP_KEY_ATTR_ALGO_ID_STR, &algo_id_str))) {
        ma_crypto_encryption_type_t enc_type = MA_CRYPTO_ENCRYPTION_3DES;
        if(!strcmp(algo_id_str, MA_CRYPTO_ALGO_TYPE_3DES_STR)) 
            enc_type = MA_CRYPTO_ENCRYPTION_3DES;
        else if(!strcmp(algo_id_str, MA_CRYPTO_ALGO_TYPE_AES128_STR)) 
            enc_type = MA_CRYPTO_ENCRYPTION_3DES;
        else 
            rc = MA_ERROR_CRYPTO_NOT_SUPPORTED;

        if(MA_OK == rc) {
            ma_crypto_encryption_t *pencryption = NULL;    
            if(MA_OK == (rc = ma_crypto_encrypt_create(self->crypto, enc_type, &pencryption))) {
                const unsigned char *data = NULL;
                size_t data_len = 0;
                if(MA_OK == (rc = get_buffer_from_payload(request_msg, &data, &data_len))) {
                    ma_bytebuffer_t *buffer = NULL;
                    if(MA_OK == (rc = ma_bytebuffer_create(data_len, &buffer))) {
		                if(MA_OK == (rc = ma_crypto_encrypt(pencryption, data, data_len, buffer))) {
                            rc = add_bytebuffer_as_payload(buffer, response_msg);
                        }
                        (void)ma_bytebuffer_release(buffer);
                    }
                }
                ma_crypto_encrypt_release(pencryption);
            }
        }
    }
    reply_to_clients(self, rc, response_msg, c_request);
	return rc;
}

static ma_error_t decrypt_handle(ma_crypto_service_t *self, ma_message_t *request_msg, ma_message_t *response_msg, ma_msgbus_client_request_t *c_request) {
    const char *algo_id_str = NULL;
    ma_error_t rc = MA_OK;
    if(MA_OK == (rc = ma_message_get_property(request_msg, MA_PROP_KEY_ATTR_ALGO_ID_STR, &algo_id_str))) {
        ma_crypto_decryption_type_t enc_type = MA_CRYPTO_DECRYPTION_3DES;
        if(!strcmp(algo_id_str, MA_CRYPTO_ALGO_TYPE_3DES_STR)) 
            enc_type = MA_CRYPTO_DECRYPTION_3DES;
        else if(!strcmp(algo_id_str, MA_CRYPTO_ALGO_TYPE_AES128_STR)) 
            enc_type = MA_CRYPTO_DECRYPTION_3DES;
        else if(!strcmp(algo_id_str, MA_CRYPTO_ALGO_TYPE_POLICY_AES128_STR))
            enc_type = MA_CRYPTO_DECRYPTION_POLICY_SYM;
        else 
            rc = MA_ERROR_CRYPTO_NOT_SUPPORTED;

        if(MA_OK == rc) {
            ma_crypto_decryption_t *pencryption = NULL;    
            if(MA_OK == (rc = ma_crypto_decrypt_create(self->crypto, enc_type, &pencryption))) {
                const unsigned char *data = NULL;
                size_t data_len = 0;
                if(MA_OK == (rc = get_buffer_from_payload(request_msg, &data, &data_len))) {
                    ma_bytebuffer_t *buffer = NULL;
                    if(MA_OK == (rc = ma_bytebuffer_create(data_len, &buffer))) {
                        if (MA_CRYPTO_DECRYPTION_POLICY_SYM == enc_type) {
                            if (data_len >= 10 && memcmp(data, "EPOAES128:", 10) == 0)
                                rc = ma_crypto_decrypt(pencryption, data, data_len, buffer);
                            else
                                rc = ma_crypto_decode(data, data_len, buffer, MA_FALSE);
                        }
                    }
                    else {
                        rc = ma_crypto_decrypt(pencryption, data, data_len, buffer);
                    }

                    if(MA_OK == rc) 
                        rc = add_bytebuffer_as_payload(buffer, response_msg);

                    (void)ma_bytebuffer_release(buffer);             
                }
                ma_crypto_decrypt_release(pencryption);
            }
        }
    }

    reply_to_clients(self, rc, response_msg, c_request);
    return rc;
}

static ma_error_t hash_handle(ma_crypto_service_t *self, ma_message_t *request_msg, ma_message_t *response_msg, ma_msgbus_client_request_t *c_request) {
    const char *hash_id_str = NULL;
    ma_error_t rc = MA_OK;
    if(MA_OK == (rc = ma_message_get_property(request_msg, MA_PROP_KEY_ATTR_ALGO_ID_STR, &hash_id_str))) {
        ma_crypto_hash_type_t hash_type = MA_CRYPTO_HASH_DEFAULT;
        if(!strcmp(hash_id_str, MA_CRYPTO_DIGEST_TYPE_SHA1_STR)) 
            hash_type = MA_CRYPTO_HASH_SHA1;
        else if(!strcmp(hash_id_str, MA_CRYPTO_DIGEST_TYPE_SHA256_STR)) 
            hash_type = MA_CRYPTO_HASH_SHA256;
        else 
            rc = MA_ERROR_CRYPTO_NOT_SUPPORTED;

        if(MA_OK == rc) {
            ma_crypto_hash_t *hash  = NULL;
            if(MA_OK == (rc = ma_crypto_hash_create(self->crypto, hash_type, &hash))) {
                const unsigned char *data = NULL;
                size_t data_len = 0;
                if(MA_OK == (rc = get_buffer_from_payload(request_msg, &data, &data_len))) {
                    ma_bytebuffer_t *buffer = NULL;
                    if(MA_OK == (rc = ma_bytebuffer_create(data_len, &buffer))) {
                        if(MA_OK == (rc = ma_crypto_hash_buffer(hash, data, data_len, buffer))) {
                            rc = add_bytebuffer_as_payload(buffer, response_msg);
                        }
                        (void)ma_bytebuffer_release(buffer);
                    }
                }
                (void)ma_crypto_hash_release(hash);
            }
        }
    }
    reply_to_clients(self, rc, response_msg, c_request);
	return rc;
}

static ma_error_t crypto_server_handler(ma_msgbus_server_t *server, ma_message_t *request_msg, void *cb_data, ma_msgbus_client_request_t *c_request) {
    ma_crypto_service_t *self = (ma_crypto_service_t *)cb_data;
	ma_error_t rc = MA_OK;
	
	if(self && request_msg && c_request) {
        ma_message_t *response_msg = NULL;
        const char *request_type = NULL;
		if(MA_OK == (rc = ma_message_get_property(request_msg, MA_PROP_KEY_CRYPTO_REQUEST_TYPE_STR, &request_type))) {
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Received crypto request type <%s>", request_type);

            if(MA_OK == (rc = ma_message_create(&response_msg))) {
			    if(!strcmp(request_type, MA_PROP_VALUE_ENCRYPT_REQUEST_STR)) 
				    rc = encrypt_handle(self, request_msg, response_msg, c_request);
			    else if(!strcmp(request_type, MA_PROP_VALUE_DECRYPT_REQUEST_STR)) 
				    rc = decrypt_handle(self, request_msg, response_msg, c_request);			   
                else if(!strcmp(request_type, MA_PROP_VALUE_HASH_REQUEST_STR))
				    rc = hash_handle(self, request_msg, response_msg, c_request);
			    else {   
				    rc = MA_ERROR_POLICY_INVALID_REQUEST;
			    }
                (void)ma_message_release(response_msg);
            }
        }else
			rc = MA_ERROR_POLICY_INVALID_REQUEST;		
    }
	return rc;
}

