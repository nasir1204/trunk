#ifndef MA_EPO_TASK_DEFS_H_INCLUDED
#define MA_EPO_TASK_DEFS_H_INCLUDED

#include "ma/ma_common.h"

MA_CPP(extern "C" {)

/* TBD - These defs should be reviewed */

/* Section Names */
#define MA_TASK_SETTINGS_SECTION_NAME_STR                  "Settings"
#define MA_TASK_SCHEDULE_SECTION_NAME_STR                  "Schedule"

/* Section - Settings */
#define MA_TASK_SETTINGS_STOPAFTERMINUTES_STR              "StopAfterMinutes"       /* Task  */
#define MA_TASK_SETTINGS_ENABLED_STR                       "Enabled"                /* Task  */ 
#define MA_TASK_SETTINGS_PENDING_STR                       "PendingTask"            /* No support */
#define MA_TASK_SETTINGS_HOLDING_STR                       "HoldingTask"            /* No support */
#define MA_TASK_SETTINGS_DUPLICATED_STR                    "Duplicated"             /* No support */
#define MA_TASK_SETTINGS_PARENT_GUID_STR                   "ParentGuid"             /* No support */
#define MA_TASK_SETTINGS_LAST_RUN_TIME_STR                 "LastRunTime"            /* Task - where will we use this? */
#define MA_TASK_SETTINGS_DIALUP_TRIGGER_ENABLED_STR        "DialupTriggerEnabled"   /* No support */
#define MA_TASK_SETTINGS_RUN_AT_ENFORCEMENT_ENABLED_STR    "RunAtEnforcementEnabled" /* No support */

/* Section - Schedule */
#define MA_TASK_SCHEDULE_TYPE_STR                         "Type"                    /* trigger type - map to trigger type enum */
#define MA_TASK_SCHEDULE_GMTTIME_STR                      "GMTTime"                 /* Trigger - No support - use_gmt() need api*/
#define MA_TASK_SCHEDULE_RUNIFMISSED_STR                  "RunIfMissed"             /* Task - create missed policy if enable - Not implemented*/
#define MA_TASK_SCHEDULE_RUNIFMISSED_DELAY_MINS_STR       "RunIfMissedDelayMins"    /* Task - */
#define MA_TASK_SCHEDULE_STARTDATETIME_STR                "StartDateTime"           /* Trigger */

#define MA_TASK_SCHEDULE_STOPDATEVALID_STR                "StopDateValid"           /* Trigger - based on this value set the stop data*/
#define MA_TASK_SCHEDULE_STOPDATETIME_STR                 "StopDateTime"            /* Trigger */   

#define MA_TASK_SCHEDULE_TASKREPEATABEL_STR               "TaskRepeatable"          /* Task Repeat Policy */
#define MA_TASK_SCHEDULE_REPEATOPTION_STR                 "RepeatOption"            /* Task Repeat Policy */
#define MA_TASK_SCHEDULE_REPEARTINTERVAL_STR              "RepeatInterval"          /* Task Repeat Policy */
#define MA_TASK_SCHEDULE_UNTILOPTION_STR                  "UntilOption"             /* Task Repeat Policy */
#define MA_TASK_SCHEDULE_UNTILDURATION_STR                "UntilDuration"           /* Task Repeat Policy */
#define MA_TASK_SCHEDULE_UNTILTIME_STR                    "UntilTime"               /* Task Repeat Policy */

#define MA_TASK_SCHEDULE_ENABLERANDOM_STR                 "RandomizationEnabled"    /* Task Policy */
#define MA_TASK_SCHEDULE_RANDOMWNDMINS_STR                "RandomizationWndMins"    /* Task Policy */

#define MA_TASK_SCHEDULE_ENABLEONCEADAY_STR               "EnableRunTaskOnceADay"   /* Trigger Policy */
        
#define MA_TASK_SCHEDULE_REPEATDAYS_STR                   "RepeatDays"              /* Trigger - for daily policy intrval*/

#define MA_TASK_SCHEDULE_REPEATWEEKS_STR                  "RepeatWeeks"             /* Trigger - for weekly policy */
#define MA_TASK_SCHEDULE_MASKDAYSOFWEEK_STR               "MaskDaysOfWeek"          /* Trigger - for weekly policy  */

#define MA_TASK_SCHEDULE_MONTHOPTION_STR                  "MonthOption"             /* Trigger */
#define MA_TASK_SCHEDULE_DAYNUMOFMONTH_STR                "DayNumOfMonth"           /* Trigger */
#define MA_TASK_SCHEDULE_WEEKNUMOFMONTH_STR               "WeekNumOfMonth"          /* Trigger */
#define MA_TASK_SCHEDULE_DAYOFWEEK_STR                    "DayOfWeek"               /* Trigger */
#define MA_TASK_SCHEDULE_MASKMONTHSOFYEAR_STR             "MaskMonthsOfYear"        /* Trigger */

#define MA_TASK_SCHEDULE_IDLEMINUTES_STR                  "IdleMinutes"             /* Idle policy - No support */
#define MA_TASK_SCHEDULE_ONLYSTARTWHENIDLE_STR            "OnlyStartWhenIdle"       /* Task - No support */

#define MA_TASK_SCHEDULE_TASK_DELAY_MINUTES_STR           "AtStartupDelayMinutes"   /* Task - no support*/

MA_CPP(})

#endif /* MA_EPO_TASK_DEFS_H_INCLUDED */

