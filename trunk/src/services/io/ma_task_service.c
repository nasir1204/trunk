#include "ma/internal/services/io/ma_task_service.h"
#include "ma/internal/services/ma_policy_settings_bag.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/services/scheduler/ma_scheduler_core.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"
#include "ma/internal/services/policy/ma_task_db.h"
#include "ma/internal/services/scheduler/ma_task_internal.h"
#include "ma/internal/services/io/ma_task_service_utils.h"

#include "ma/internal/services/ma_agent_policy_settings_bag.h"


#include "ma/internal/services/property/ma_property_service.h"

#include "ma/internal/ma_strdef.h"
#include "ma/internal/utils/platform/ma_sys_apis.h"

#include <stdlib.h>
#include <string.h>

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "io.service"

static ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint);

static ma_error_t service_start(ma_service_t *service);

static ma_error_t service_stop(ma_service_t *service);

static void service_destroy(ma_service_t *service);

static const ma_service_methods_t methods = {
    &service_configure,
    &service_start,
    &service_stop,
    &service_destroy
};	


ma_error_t ma_task_service_create(char const *service_name, ma_service_t **service) {
    if(service) {
        ma_task_service_t *self = (ma_task_service_t *)calloc(1, sizeof(ma_task_service_t));        
        if(self) {            
            ma_service_init((ma_service_t *)self, &methods, service_name, self);            
            *service = (ma_service_t *)self;
            return MA_OK;
        }
        return MA_ERROR_OUTOFMEMORY;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t task_service_subscriber_cb(const char *topic, ma_message_t *payload, void *cb_data);
static ma_error_t agent_wakeup_task_handler(ma_task_service_t *self, const char *task_id);

static ma_error_t service_start(ma_service_t *service) {
    ma_task_service_t *self = (ma_task_service_t *)service;

    if(self) {
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context) ;

        ma_error_t rc = ma_msgbus_subscriber_register(self->subscriber, MA_GENERAL_SUBSCRIBER_TOPIC_STR, task_service_subscriber_cb, self);

        if(self->is_agent_managed) {
			ma_uint64_t random_interval = ma_sys_rand() % 45 + 45 ;

			MA_LOG(logger, MA_LOG_SEV_INFO, "randomizing property task for %lld secs", random_interval) ;
			
            if((MA_OK == rc) && (MA_OK == (rc = ma_task_service_task_create(self->context, MA_PROPERTY_COLLECT_TASK_ID_STR, MA_PROPERTY_COLLECT_TASK_NAME_STR, MA_PROPERTY_COLLECT_TASK_TYPE_STR, MA_TASK_SERVICE_CREATOR_NAME_STR, self->property_collection_timeout, random_interval, MA_TRUE))))
                    rc = ma_task_service_task_start(self->context, MA_PROPERTY_COLLECT_TASK_ID_STR, MA_PROPERTY_COLLECT_TASK_NAME_STR, MA_PROPERTY_COLLECT_TASK_TYPE_STR);     
                 
            if((MA_OK == rc) && (MA_OK == (rc = ma_task_service_task_create(self->context, MA_PRIORITY_EVENT_TRIGGER_TASK_ID_STR, MA_PRIORITY_EVENT_TRIGGER_TASK_NAME_STR, MA_PRIORITY_EVENT_TRIGGER_TASK_TYPE_STR, MA_TASK_SERVICE_CREATOR_NAME_STR, self->event_trigger_interval, 90, MA_TRUE ))))
                    rc = ma_task_service_task_start(self->context, MA_PRIORITY_EVENT_TRIGGER_TASK_ID_STR, MA_PRIORITY_EVENT_TRIGGER_TASK_NAME_STR, MA_PRIORITY_EVENT_TRIGGER_TASK_TYPE_STR);
			
			if((MA_OK == rc) && (MA_OK == (rc = ma_task_service_task_create(self->context, MA_P2P_CONTENT_PURGE_TASK_ID_STR, MA_P2P_CONTENT_PURGE_TASK_NAME_STR, MA_P2P_CONTENT_PURGE_TASK_TYPE_STR, MA_TASK_SERVICE_CREATOR_NAME_STR, self->p2p_purge_interval, 60, self->is_p2p_serving_enabled )))) 
                rc = ma_task_service_task_start(self->context, MA_P2P_CONTENT_PURGE_TASK_ID_STR, MA_P2P_CONTENT_PURGE_TASK_NAME_STR, MA_P2P_CONTENT_PURGE_TASK_TYPE_STR) ;

			if((MA_OK == rc) && (MA_OK == (rc = ma_task_service_task_create(self->context, MA_HTTP_SERVER_SUPERAGENT_REPO_PURGE_TASK_ID_STR, MA_HTTP_SERVER_SUPERAGENT_REPO_PURGE_TASK_NAME_STR, MA_HTTP_SERVER_SUPERAGENT_REPO_PURGE_TASK_TYPE_STR, MA_TASK_SERVICE_CREATOR_NAME_STR, self->sa_repo_purge_interval, 60, self->is_sa_repo_task_enabled)))) 
                rc = ma_task_service_task_start(self->context, MA_HTTP_SERVER_SUPERAGENT_REPO_PURGE_TASK_ID_STR, MA_HTTP_SERVER_SUPERAGENT_REPO_PURGE_TASK_NAME_STR, MA_HTTP_SERVER_SUPERAGENT_REPO_PURGE_TASK_TYPE_STR) ;

			ma_telemtry_deployment_task_run(self->context);
        }
		else{
			 (void)ma_task_service_task_stop(self->context, MA_PROPERTY_COLLECT_TASK_ID_STR, MA_PROPERTY_COLLECT_TASK_NAME_STR, MA_PROPERTY_COLLECT_TASK_TYPE_STR);
             (void)ma_task_service_task_stop(self->context, MA_PRIORITY_EVENT_TRIGGER_TASK_ID_STR, MA_PRIORITY_EVENT_TRIGGER_TASK_NAME_STR, MA_PRIORITY_EVENT_TRIGGER_TASK_TYPE_STR);
			 (void)ma_task_service_task_stop(self->context, MA_P2P_CONTENT_PURGE_TASK_ID_STR, MA_P2P_CONTENT_PURGE_TASK_NAME_STR, MA_P2P_CONTENT_PURGE_TASK_TYPE_STR);
			 (void)ma_task_service_task_stop(self->context, MA_HTTP_SERVER_SUPERAGENT_REPO_PURGE_TASK_ID_STR, MA_HTTP_SERVER_SUPERAGENT_REPO_PURGE_TASK_NAME_STR, MA_HTTP_SERVER_SUPERAGENT_REPO_PURGE_TASK_TYPE_STR);
		}

        if((MA_OK == rc) && (MA_OK == (rc = ma_task_service_task_create(self->context, MA_POLICY_ENFORCE_TASK_ID_STR, MA_POLICY_ENFORCE_TASK_NAME_STR, MA_POLICY_ENFORCE_TASK_TYPE_STR, MA_TASK_SERVICE_CREATOR_NAME_STR, self->policy_enforce_timeout, 60, MA_TRUE )))) 
                rc = ma_task_service_task_start(self->context, MA_POLICY_ENFORCE_TASK_ID_STR, MA_POLICY_ENFORCE_TASK_NAME_STR, MA_POLICY_ENFORCE_TASK_TYPE_STR); 

        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t service_stop(ma_service_t *service) {
    ma_error_t rc = MA_OK;
    ma_task_service_t *self = (ma_task_service_t *)service;
	ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);

    if(MA_OK == (rc = ma_msgbus_subscriber_unregister(self->subscriber))) {
        if(self->is_agent_managed) {
            rc = (MA_OK == (rc = ma_task_service_task_stop(self->context, MA_PROPERTY_COLLECT_TASK_ID_STR, MA_PROPERTY_COLLECT_TASK_NAME_STR, MA_PROPERTY_COLLECT_TASK_TYPE_STR)))             
                 && (MA_OK == (rc = ma_task_service_task_stop(self->context, MA_PRIORITY_EVENT_TRIGGER_TASK_ID_STR, MA_PRIORITY_EVENT_TRIGGER_TASK_NAME_STR, MA_PRIORITY_EVENT_TRIGGER_TASK_TYPE_STR)))
				 && (MA_OK != (rc = ma_task_service_task_stop(self->context, MA_P2P_CONTENT_PURGE_TASK_ID_STR, MA_P2P_CONTENT_PURGE_TASK_NAME_STR, MA_P2P_CONTENT_PURGE_TASK_TYPE_STR)))
				 && (MA_OK != (rc = ma_task_service_task_stop(self->context, MA_HTTP_SERVER_SUPERAGENT_REPO_PURGE_TASK_ID_STR, MA_HTTP_SERVER_SUPERAGENT_REPO_PURGE_TASK_NAME_STR, MA_HTTP_SERVER_SUPERAGENT_REPO_PURGE_TASK_TYPE_STR)))
                 ? MA_OK : rc;

			if(MA_OK != rc )
				MA_LOG(logger, MA_LOG_SEV_DEBUG, "Failed to stop managed mode tasks, rc = %d", rc) ;
        }
        
		if(MA_OK != (rc = ma_task_service_task_stop(self->context, MA_POLICY_ENFORCE_TASK_ID_STR, MA_POLICY_ENFORCE_TASK_NAME_STR, MA_POLICY_ENFORCE_TASK_TYPE_STR)))
			MA_LOG(logger, MA_LOG_SEV_DEBUG, "Failed to stop policy enforcement task, rc = %d", rc) ;
    }
    return rc;
}

static void service_destroy(ma_service_t *service) {
    ma_task_service_t *self = (ma_task_service_t *)service;
    (void) ma_msgbus_subscriber_release(self->subscriber);
    free(self);
}

static ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint) {
    ma_task_service_t *self = (ma_task_service_t *)service;    
    ma_policy_settings_bag_t *bag = MA_CONTEXT_GET_POLICY_SETTINGS_BAG(context);
    ma_error_t rc = MA_OK;
    ma_int32_t new_value = -1;
	ma_bool_t enabled = MA_FALSE, sa_enabled = MA_FALSE, sa_repo_enabled = MA_FALSE;
    ma_configurator_t *configurator = MA_CONTEXT_GET_CONFIGURATOR(context);
    ma_bool_t is_property_task_policy_changed = MA_FALSE, is_policy_task_policy_changed = MA_FALSE,  is_event_task_policy_changed = MA_FALSE, is_p2p_task_policy_changed = MA_FALSE, is_sa_repo_task_policy_changed = MA_FALSE;

    self->is_agent_managed = ma_configurator_get_agent_mode(configurator);

    if(self->is_agent_managed) {
        if(MA_OK == (rc = ma_policy_settings_bag_get_int(bag, MA_PROPERTY_SERVICE_SECTION_NAME_STR, MA_PROPERTY_KEY_COLLECTION_TIMEOUT_INT, MA_FALSE, &new_value, 60)))
            self->property_collection_timeout =( (new_value != self->property_collection_timeout) && (is_property_task_policy_changed = MA_TRUE)) ? new_value : self->property_collection_timeout;      

        if(MA_OK == (rc = ma_policy_settings_bag_get_int(bag, MA_EVENT_SERVICE_SECTION_NAME_STR, MA_EVENT_KEY_UPLOAD_TIMEOUT_INT, MA_FALSE, &new_value, 5)))
            self->event_trigger_interval = ((new_value != self->event_trigger_interval) && (is_event_task_policy_changed = MA_TRUE)) ? new_value : self->event_trigger_interval;

		if(MA_OK == (rc = ma_policy_settings_bag_get_bool(bag, MA_P2P_SERVICE_SECTION_NAME_STR, MA_P2P_KEY_ENABLE_SERVING_INT, MA_FALSE, &enabled, MA_TRUE)))
			self->is_p2p_serving_enabled = ((enabled != self->is_p2p_serving_enabled) && (is_p2p_task_policy_changed = MA_TRUE)) ? enabled : self->is_p2p_serving_enabled ;
		if(MA_OK == (rc = ma_policy_settings_bag_get_int(bag, MA_P2P_SERVICE_SECTION_NAME_STR, MA_P2P_KEY_LONGEVITY_INT, MA_FALSE, &new_value, 1)))
			self->p2p_purge_interval = ((new_value != self->p2p_purge_interval) && (is_p2p_task_policy_changed = MA_TRUE)) ? new_value : self->p2p_purge_interval ;

		(void)ma_policy_settings_bag_get_bool(bag, MA_HTTP_SERVER_SERVICE_SECTION_NAME_STR, MA_HTTP_SERVER_KEY_ENABLE_SUPER_AGENT_INT, MA_FALSE, &sa_enabled, MA_FALSE) ;
		(void)ma_policy_settings_bag_get_bool(bag, MA_HTTP_SERVER_SERVICE_SECTION_NAME_STR, MA_HTTP_SERVER_KEY_ENABLE_SUPER_AGENT_REPOSITORY_INT, MA_FALSE, &sa_repo_enabled, MA_FALSE) ;
		(void)ma_policy_settings_bag_get_int(bag, MA_HTTP_SERVER_SERVICE_SECTION_NAME_STR, MA_HTTP_SERVER_KEY_REPOSITORY_PURGE_INTERVAL_INT, MA_FALSE, &new_value, 1) ;
		self->sa_repo_purge_interval = ((new_value != self->sa_repo_purge_interval) && (is_sa_repo_task_policy_changed = MA_TRUE)) ? new_value : self->sa_repo_purge_interval ;
		(void)ma_policy_settings_bag_get_int(bag, MA_HTTP_SERVER_SERVICE_SECTION_NAME_STR, MA_HTTP_SERVER_KEY_REPOSITORY_SYNC_INTERVAL_INT, MA_FALSE, &new_value, 30) ;

		if(sa_enabled != self->is_sa_enabled || sa_repo_enabled != self->is_sa_repo_task_enabled ) {
			is_sa_repo_task_policy_changed = MA_TRUE ;
		}
		self->is_sa_enabled = sa_enabled ;
		self->is_sa_repo_task_enabled = (sa_enabled) ? sa_repo_enabled : MA_FALSE ;
    }

    if(MA_OK == (rc = ma_policy_settings_bag_get_int(bag, MA_POLICY_SERVICE_SECTION_NAME_STR, MA_POLICY_KEY_ENFORCEMENT_TIMEOUT_INT, MA_FALSE, &new_value, 60)))
        self->policy_enforce_timeout = ((new_value != self->policy_enforce_timeout) && (is_policy_task_policy_changed = MA_TRUE)) ? new_value : self->policy_enforce_timeout;

	

    if(MA_SERVICE_CONFIG_NEW == hint) {
        ma_msgbus_t *msgbus = MA_CONTEXT_GET_MSGBUS(context);
        self->context = context;
        rc = ma_msgbus_subscriber_create(msgbus, &self->subscriber);      
        (void) ma_msgbus_subscriber_setopt(self->subscriber, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
        (void) ma_msgbus_subscriber_setopt(self->subscriber, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
        
        /* Here scheduling the tasks which are migrated while upgrading MA, task version set as "1" in migration code */
        /*TBD - till the time scheduler task migration is ready 
            handle_epo_tasks(self, "1");
        */
    }
    else {
        if((MA_OK == rc) && is_property_task_policy_changed)
            rc = ma_task_service_task_update(context,  MA_PROPERTY_COLLECT_TASK_ID_STR, MA_PROPERTY_COLLECT_TASK_NAME_STR, MA_PROPERTY_COLLECT_TASK_TYPE_STR, self->property_collection_timeout, MA_TRUE);
                  
        if((MA_OK == rc) && is_policy_task_policy_changed)
            rc = ma_task_service_task_update(context,  MA_POLICY_ENFORCE_TASK_ID_STR, MA_POLICY_ENFORCE_TASK_NAME_STR, MA_POLICY_ENFORCE_TASK_TYPE_STR, self->policy_enforce_timeout, MA_TRUE);

        if((MA_OK == rc) && is_event_task_policy_changed)
            rc = ma_task_service_task_update(context,  MA_PRIORITY_EVENT_TRIGGER_TASK_ID_STR, MA_PRIORITY_EVENT_TRIGGER_TASK_NAME_STR, MA_PRIORITY_EVENT_TRIGGER_TASK_TYPE_STR, self->event_trigger_interval, MA_TRUE);

		if((MA_OK == rc) && is_p2p_task_policy_changed)
			rc = ma_task_service_task_update(context,  MA_P2P_CONTENT_PURGE_TASK_ID_STR, MA_P2P_CONTENT_PURGE_TASK_NAME_STR, MA_P2P_CONTENT_PURGE_TASK_TYPE_STR, self->p2p_purge_interval, self->is_p2p_serving_enabled) ;

		if((MA_OK == rc) && is_sa_repo_task_policy_changed)
			rc = ma_task_service_task_update(context,  MA_HTTP_SERVER_SUPERAGENT_REPO_PURGE_TASK_ID_STR, MA_HTTP_SERVER_SUPERAGENT_REPO_PURGE_TASK_NAME_STR, MA_HTTP_SERVER_SUPERAGENT_REPO_PURGE_TASK_TYPE_STR, self->sa_repo_purge_interval, self->is_sa_repo_task_enabled) ;

		ma_telemtry_deployment_task_run(self->context);
    }
    return rc;            
}

typedef struct service_request_data_s {
    char                *task_id;
    ma_task_service_t   *self;
} service_request_data_t;

static ma_error_t service_response_callback(ma_error_t status, ma_message_t *response, void *cb_data, ma_msgbus_request_t *policy_msg_request) {
    ma_error_t rc = MA_OK;    
    service_request_data_t *data = (service_request_data_t *)cb_data;
    ma_msgbus_endpoint_t *policy_ep = NULL;    

    /* Generate the event on success or failure */  
	if(data && data->task_id) {
	    ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(data->self->context);
 
	    MA_LOG(logger, MA_LOG_SEV_DEBUG, "Task id <%s> execution status <%d>", data->task_id, status);
        ma_task_service_task_set_status(data->self->context, data->task_id, (MA_OK == status) ? MA_TASK_EXEC_SUCCESS: MA_TASK_EXEC_FAILED);
        free(data->task_id);
        free(data);
    }

	if((status == MA_OK) && response) ma_message_release(response);		
    if(MA_OK == (rc = ma_msgbus_request_get_endpoint(policy_msg_request, &policy_ep))) {
        (void)ma_msgbus_request_release(policy_msg_request);
        (void)ma_msgbus_endpoint_release(policy_ep);    
    }
    return rc;
}

static ma_error_t send_request_to_service(ma_task_service_t *self, const char *task_id, ma_message_t *msg, const char *service_name) {
    ma_error_t rc = MA_OK;
    ma_msgbus_endpoint_t *ep = NULL;     
    if(MA_OK == (rc = ma_msgbus_endpoint_create(MA_CONTEXT_GET_MSGBUS(self->context), service_name, NULL, &ep))) {               
        ma_msgbus_request_t *request = NULL; 
        service_request_data_t *data = (service_request_data_t *)calloc(1, sizeof(service_request_data_t));
        if(data) {
            data->task_id = strdup(task_id);
            data->self = self;
            ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
            ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);

            if(MA_OK != (rc = ma_msgbus_async_send(ep, msg, service_response_callback, data, &request)))
                (void)ma_msgbus_endpoint_release(ep);
        }
        else
            rc = MA_ERROR_OUTOFMEMORY;
    }	
    return rc; 
}

static ma_error_t send_and_forget_request_to_service(ma_task_service_t *self, const char *task_id, ma_message_t *msg, const char *service_name) {
    ma_error_t rc = MA_OK;
    ma_msgbus_endpoint_t *ep = NULL;     
    if(MA_OK == (rc = ma_msgbus_endpoint_create(MA_CONTEXT_GET_MSGBUS(self->context), service_name, NULL, &ep))) {               
        ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
        ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);

		rc = ma_msgbus_send_and_forget(ep, msg);          
	    (void)ma_msgbus_endpoint_release(ep);
    }	
	ma_task_service_task_set_status(self->context, task_id, (MA_OK == rc) ? MA_TASK_EXEC_SUCCESS: MA_TASK_EXEC_FAILED);
    return rc; 
}

ma_error_t task_service_subscriber_cb(const char *topic, ma_message_t *message, void *cb_data) {
    ma_error_t rc = MA_OK;
    ma_task_service_t *self = (ma_task_service_t *)cb_data;
    ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);

    if(topic) {
        if(!strcmp(topic, MA_TASK_CHANGED_TOPIC_STR)){
            const char *task_version = NULL;
            (void)ma_message_get_property(message, MA_PROP_KEY_TASK_CHANGE_VERSION_STR, &task_version);
            
			if(task_version)
				MA_LOG(logger, MA_LOG_SEV_DEBUG, "Task version <%s> of received ePO tasks", task_version);

            MA_LOG(logger, MA_LOG_SEV_DEBUG, "Scheduling ePO tasks");

            if(MA_OK == (rc = handle_epo_tasks(self, task_version))) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "ePO tasks schedule succeeded");					
            }
            else
                MA_LOG(logger, MA_LOG_SEV_ERROR, "ePO tasks schedule failed rc = <%d>", rc);            
        }
		else if(!strcmp(topic, MA_MSG_AGENT_GLOBAL_UPDATE_TOPIC_STR)){
			MA_LOG(logger, MA_LOG_SEV_INFO, "ma global update message received");
			rc = ma_task_schedule_global_update(message, self->context);
        }
        else if(!strncmp(topic, MA_TASK_EXECUTE_PUBSUB_TOPIC, strlen(MA_TASK_EXECUTE_PUBSUB_TOPIC))) {
			const char *task_id = NULL, *task_type = NULL;
			ma_policy_settings_bag_t *policy_bag = MA_CONTEXT_GET_POLICY_SETTINGS_BAG(self->context);
            if((MA_OK == (rc = ma_message_get_property(message, MA_TASK_TYPE, &task_type))) && task_type &&
			    (MA_OK == (rc = ma_message_get_property(message, MA_TASK_ID, &task_id))) && task_id) 
			{	
				if(!strcmp(task_type, MA_POLICY_ENFORCE_TASK_TYPE_STR)) {
					ma_message_t *msg = NULL;
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "Task type <%s> is running", task_type);
					if(MA_OK == (rc = ma_message_create(&msg))) {
						ma_int32_t value = 0;
						ma_message_set_property(msg, MA_PROP_KEY_POLICY_ENFORCE_REASON_STR, MA_PROP_VALUE_POLICY_TIMEOUT_STR);
						ma_message_set_property(msg, MA_PROP_KEY_POLICY_REQUEST_TYPE_STR, MA_PROP_VALUE_ENFORCE_POLICIES_REQUEST_STR);
						rc = send_request_to_service(self, task_id, msg, MA_POLICY_SERVICE_NAME_STR);
						ma_policy_settings_bag_get_int(policy_bag, MA_POLICY_SERVICE_SECTION_NAME_STR, MA_POLICY_KEY_ENFORCEMENT_TIMEOUT_INT, MA_FALSE, &value, 60);
						MA_LOG(logger, MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "Next policy enforcement in %d minutes", value);
						ma_message_release(msg);
					}
                    if(MA_OK != rc)(void)ma_task_service_task_set_status(self->context, task_id, MA_TASK_EXEC_FAILED);
				}
				else if(!strcmp(task_type, MA_PRIORITY_EVENT_TRIGGER_TASK_TYPE_STR)) {     
					ma_message_t *msg = NULL;
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "Task type <%s> is running", task_type);
					if(MA_OK == (rc = ma_message_create(&msg))) {
						ma_message_set_property(msg, MA_EVENT_MSG_PROP_KEY_MSG_TYPE, MA_EVENT_MSG_PROP_VALUE_REQ_UPLOAD_EVENT);
						ma_message_set_property(msg, MA_EVENT_MSG_PROP_KEY_EVENT_PRIORITY, "2");                      
						rc = send_and_forget_request_to_service(self, task_id, msg, MA_EVENT_SERVICE_NAME_STR);
						ma_message_release(msg);
					}
                    if(MA_OK != rc)(void)ma_task_service_task_set_status(self->context, task_id, MA_TASK_EXEC_FAILED);
				}
				else if(!strcmp(task_type, MA_PROPERTY_COLLECT_TASK_TYPE_STR)) {
					ma_message_t *props_collect_msg = NULL;
					MA_LOG(logger, MA_LOG_SEV_DEBUG, "Task type <%s> is running", task_type);
					
					if(MA_OK == (rc = ma_property_collect_message_create(PROPS_TYPE_VERSION, &props_collect_msg))) {
						rc = send_request_to_service(self, task_id, props_collect_msg, MA_PROPERTY_SERVICE_NAME_STR);
						ma_message_release(props_collect_msg);
					}
                    
                    if(MA_OK != rc)(void)ma_task_service_task_set_status(self->context, task_id, MA_TASK_EXEC_FAILED);
				}
                else if(!MA_WIN_SELECT(stricmp,strcasecmp)(task_type, MA_AGENT_WAKEUP_TASK_TYPE_STR)) {
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "Task type <%s> is running", task_type);
                    rc = agent_wakeup_task_handler(self, task_id);
                }
				else if(!strcmp(task_type, MA_P2P_CONTENT_PURGE_TASK_TYPE_STR)) {
					ma_message_t *msg = NULL ;
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "Task type <%s> is running", task_type);
					if(MA_OK == (rc = ma_message_create(&msg))) {
						ma_message_set_property(msg, MA_P2P_MSG_KEY_TYPE_STR, MA_P2P_MSG_PROP_VAL_RQST_RUN_PURGE_TASK_STR) ;                  
						rc = send_request_to_service(self, task_id, msg, MA_P2P_SERVICE_NAME_STR) ;
						ma_message_release(msg) ;
					}
                    if(MA_OK != rc)(void)ma_task_service_task_set_status(self->context, task_id, MA_TASK_EXEC_FAILED) ;
				}
				else if(!strcmp(task_type, MA_HTTP_SERVER_SUPERAGENT_REPO_PURGE_TASK_TYPE_STR)) {
					ma_message_t *msg = NULL ;
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "Task type <%s> is running", task_type);
					if(MA_OK == (rc = ma_message_create(&msg))) {

						ma_message_set_property(msg, MA_HTTP_SERVER_MSG_KEY_TYPE_STR, MA_HTTP_SERVER_MSG_PROP_VAL_RQST_RUN_PURGE_TASK_STR) ;
						rc = send_request_to_service(self, task_id, msg, MA_HTTP_SERVER_SERVICE_NAME_STR) ;
						ma_message_release(msg) ;
					}
                    if(MA_OK != rc)(void)ma_task_service_task_set_status(self->context, task_id, MA_TASK_EXEC_FAILED) ;
				}
				else if(!strcmp(task_type, MA_AGENT_STATS_COLLECT_TASK_TYPE_STR)) {
					ma_message_t *msg = NULL ;
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "Task type <%s> is running", task_type);
					if(MA_OK == (rc = ma_message_create(&msg))) {
						ma_message_set_property(msg, MA_STATS_MSG_KEY_TYPE_STR, MA_STATS_MSG_PROP_VAL_RQST_COLLECT_AND_SEND_STR) ;

						rc = send_request_to_service(self, task_id, msg, MA_STATS_SERVICE_NAME_STR) ;
						ma_message_release(msg) ;
					}
                    if(MA_OK != rc)(void)ma_task_service_task_set_status(self->context, task_id, MA_TASK_EXEC_FAILED) ;
				}
            }
        }
		else if(!strcmp(topic, MA_PROPERTY_PUBSUB_TOPIC_COLLECT_STATUS_STR)) {            
			ma_task_t *task = NULL ;
			ma_scheduler_t *scheduler = MA_CONTEXT_GET_SCHEDULER(self->context) ;
			ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);
			
			if(MA_OK == (rc = ma_scheduler_get_task_handle(scheduler, MA_PROPERTY_COLLECT_TASK_ID_STR, &task))) {
				ma_task_time_t fire_time;
				if(MA_OK == ma_task_get_next_run_time(task, &fire_time)){
					ma_int64_t minutes_after = 0;
					ma_int64_t seconds_after = 0;
					compute_timeout(&fire_time, &seconds_after);					
					minutes_after = seconds_after/60;
					seconds_after = seconds_after%60;
                    /* compute time out will result difference next run time into -ve as it is still ascii is in progress */
                    if(minutes_after > 0 || seconds_after > 0)
					    MA_LOG(logger, MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "Next collect and send properties in %d minutes and %d seconds.", minutes_after, seconds_after);	
				}
				ma_task_release(task);
			}			
        }
    }
    return rc;
}

/* Agent Wakeup Task functions */
   
static ma_error_t get_wakeup_task_settings(ma_task_t *task, ma_bool_t *use_custom_asci_setting, ma_bool_t *need_full_props) {	
	ma_variant_t *task_data = NULL;
	ma_error_t rc = MA_OK;
	if(MA_OK == (rc = ma_task_get_app_payload(task, &task_data))) {	
		ma_variant_t *value = NULL;
		if(MA_OK == (rc = get_settings(task_data, MA_AGENT_WAKEUP_TASK_SETTINGS_SECTION_STR, MA_AGENT_WAKEUP_TASK_USE_CUSTOM_ASCI_SETTING_STR, &value))) {		
			ma_buffer_t *buffer = NULL;
			if(MA_OK == (rc = ma_variant_get_string_buffer(value, &buffer))) {			
				const char *str = NULL;
				size_t size = 0;
				if(MA_OK == (rc = ma_buffer_get_string(buffer, &str, &size))) {
					*use_custom_asci_setting = atoi(str);
				}
				(void) ma_buffer_release(buffer);
			}
			(void) ma_variant_release(value); value = NULL;
		}

        if(*use_custom_asci_setting) {
            if(MA_OK == (rc = get_settings(task_data, MA_AGENT_WAKEUP_TASK_SETTINGS_SECTION_STR, MA_AGENT_WAKEUP_TASK_USE_CUSTOM_ASCI_SETTING_STR, &value))) {		
			    ma_buffer_t *buffer = NULL;
			    if(MA_OK == (rc = ma_variant_get_string_buffer(value, &buffer))) {			
				    const char *str = NULL;
				    size_t size = 0;
				    if(MA_OK == (rc = ma_buffer_get_string(buffer, &str, &size))) {
					    *need_full_props = atoi(str);
				    }
				    (void) ma_buffer_release(buffer);
			    }
			    (void) ma_variant_release(value);
		    }
        }
		(void) ma_variant_release(task_data);
	}
	return rc;
}

static ma_error_t agent_wakeup_task_handler(ma_task_service_t *self, const char *task_id) {
	ma_error_t rc = MA_OK;
	ma_bool_t use_custom_asci_setting = MA_FALSE, need_full_props = MA_FALSE;
    ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);
    ma_task_t *task = NULL;

    MA_LOG(logger, MA_LOG_SEV_DEBUG, "Received wakeup task, sending agent property collection request");

    if(MA_OK == (rc = ma_scheduler_get_task_handle(MA_CONTEXT_GET_SCHEDULER(self->context), task_id, &task))) {
        if(MA_OK == (rc = get_wakeup_task_settings(task, &use_custom_asci_setting, &need_full_props))) {           
            ma_message_t *msg = NULL;
            need_full_props = (use_custom_asci_setting) ? need_full_props: MA_FALSE;  
			if(MA_OK == (rc = ma_message_create(&msg))) {				
				(void) ma_message_set_property(msg, MA_PROPERTY_MSG_KEY_TYPE_STR, MA_PROPERTY_MSG_TYPE_COLLECT_STR);
                (void) ma_message_set_property(msg, MA_PROPERTY_MSG_KEY_PROPS_TYPE_STR, (need_full_props) 
											? MA_PROPERTY_MSG_VALUE_PROPS_TYPE_FULL_STR : MA_PROPERTY_MSG_VALUE_PROPS_TYPE_VERSION_STR);
				rc = send_request_to_service(self, task_id, msg, MA_PROPERTY_SERVICE_NAME_STR);				
				(void) ma_message_release(msg); msg = NULL;
			}
        }
        if(MA_OK != rc) ma_task_set_execution_status(task, MA_TASK_EXEC_FAILED);
    }
	return rc;
}
