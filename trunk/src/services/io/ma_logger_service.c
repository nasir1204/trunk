#include "ma/internal/utils/platform/ma_system_property.h"
#include "ma/internal/services/ma_service.h"
#include "ma/logger/ma_file_logger.h"
#include "ma/internal/utils/logger/ma_logger_internal.h"
#include "ma/logger/ma_log_filter.h"

#include "ma/internal/services/ma_policy_settings_bag.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include "ma_localizer.h"
#include "ma/ma_variant.h"
#include "ma/ma_message.h"
#include "ma/ma_msgbus.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/ma_macros.h"
#include "ma/datastore/ma_ds.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/services/io/ma_logger_db.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal/utils/conf/ma_conf_application.h"

#include "ma/internal/utils/platform/ma_sys_apis.h"
#include "ma/datastore/ma_ds_registry.h"
#include "ma/internal/ma_strdef.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#undef LOG_FACILITY_NAME 
#define LOG_FACILITY_NAME "io.service"

extern const char res_ini_file[];

typedef struct ma_logger_service_s {
    ma_service_t                        base;

    /* just a reference */
    ma_logger_t                         *logger;

    ma_file_logger_policy_t             file_logger_policy;

    char                                *filter_pattern;

    ma_log_filter_t                     *logger_filter;

    ma_localizer_t                      *localizer;

    ma_context_t                        *context;

    ma_bool_t                           is_log_recording_enabled;

    ma_int32_t                          log_records_size;

    char                                lang_id[MA_MAX_NAME];

    ma_msgbus_server_t                  *logger_server;

} ma_logger_service_t;

static ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint);

static ma_error_t service_start(ma_service_t *service);

static ma_error_t service_stop(ma_service_t *service);

static void service_destroy(ma_service_t *service);

static const ma_service_methods_t methods = {
    &service_configure,
    &service_start,
    &service_stop,
    &service_destroy
};	


ma_error_t ma_logger_service_create(const char *service_name, ma_service_t **service) {
    if(service) {
        ma_logger_service_t *self = (ma_logger_service_t *)calloc(1, sizeof(ma_logger_service_t));
        
        if(!self)   return MA_ERROR_OUTOFMEMORY;

        ma_service_init((ma_service_t *)self, &methods, service_name, self);

        /* Default policies */
        self->is_log_recording_enabled = MA_TRUE;
        self->log_records_size = 200;

        *service = (ma_service_t *)self;

        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


static void publish_log_message(const ma_log_msg_t *msg, void *data);
static ma_error_t localize_msg_cb(const char *msg, ma_buffer_t **localized_msg, void *cb_data);
static ma_error_t ma_logger_service_server_handler(ma_msgbus_server_t *server, ma_message_t *msg, void *cb_data, ma_msgbus_client_request_t *c_request);
static ma_error_t process_log_data(ma_logger_service_t *self, const char *time_str, const char *severity, const char *facility, const char *localize_str);
static ma_error_t handle_log_localize_message(ma_logger_service_t *self, ma_message_t *msg);
static ma_error_t handle_get_products(ma_logger_service_t *self, ma_msgbus_client_request_t *c_request);
static ma_error_t handle_get_product_log_files(ma_logger_service_t *self, const char *product_id, ma_msgbus_client_request_t *c_request);
static ma_error_t handle_get_product_log_file_path(ma_logger_service_t *self, const char *product_id, const char *file_name, ma_msgbus_client_request_t *c_request);

void ma_language_change(const char *lang_id,ma_int32_t b_override);


static ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint) {
    if(service && context) {
        ma_error_t rc = MA_OK;
        ma_logger_service_t *self = (ma_logger_service_t *)service;
        ma_policy_settings_bag_t *policy_bag = NULL;
        char const *p_filter_pattern = "*.Info", *p_format_pattern = "%d %+t %p(%P.%T) %f.%s: %m";
        ma_int32_t log_size_limit = 0, max_rollovers = 0;
        ma_bool_t rollover_enable = MA_FALSE;
		ma_bool_t app_logging_enable = MA_FALSE;
        const char *p_enable_agent_lang_selection = "1";
        ma_bool_t localizer_restart = MA_FALSE;
        ma_int32_t cur_log_records_size  = 0;
        ma_configurator_t *configurator = MA_CONTEXT_GET_CONFIGURATOR(context);
        ma_ds_t *ds = ma_configurator_get_datastore(configurator);
		ma_int32_t b_overide_lang = 0;
		ma_int32_t agent_mode = 0;
		char *p_user_lang = NULL;

        if( !(self->context = context) || !(policy_bag = MA_CONTEXT_GET_POLICY_SETTINGS_BAG(context)) || !(self->logger = MA_CONTEXT_GET_LOGGER(context)))
            return MA_ERROR_PRECONDITION;
    
        if(MA_SERVICE_CONFIG_NEW == hint) {

            if(MA_OK != (rc = ma_msgbus_server_create(MA_CONTEXT_GET_MSGBUS(context), MA_LOGGER_SERVICE_NAME_STR, &self->logger_server))) {          
                MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to create the service %s.", MA_LOGGER_SERVICE_NAME_STR);
                return rc;
            }
            ma_msgbus_server_setopt(self->logger_server, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
            ma_msgbus_server_setopt(self->logger_server, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);

            /* localizer creation */
            (void) ma_localizer_create(context, &self->localizer);
            
            ma_logger_inc_ref(self->logger);
            /* Register with localizer and publishing of the message */
            MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_DEBUG, "registering logger(%p) publish call back..", MA_CONTEXT_GET_LOGGER(context));
            if(MA_OK != (rc = ma_logger_register_visitor(MA_CONTEXT_GET_LOGGER(context), &publish_log_message, self))) {
                MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_DEBUG, "registering logger publish call back failed, last error(%d)", rc);
            }
            
            MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_DEBUG, "registering logger localize call back..");
            if(MA_OK != (rc = ma_logger_register_localizer(MA_CONTEXT_GET_LOGGER(context), &localize_msg_cb, self))) {
                MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_DEBUG, "registering logger localize call back failed, last error(%d)", rc);
            }
            
               
        } else if(MA_SERVICE_CONFIG_MODIFIED == hint) {
			
        }

		if(MA_OK == ma_ds_get_int32(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_MODE_INT, &agent_mode)) {
			if(MA_OK == ma_ds_get_int32(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURAITON_KEY_LANG_OVERRIDE_INT, &b_overide_lang)) {
				if(agent_mode) {
					if(b_overide_lang) 
						ma_ds_remove( ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURAITON_KEY_LANG_OVERRIDE_INT);
				}
			}
		}
		{
			ma_buffer_t *buffer = NULL;
			size_t size = 0;
			if(MA_OK == ma_ds_get_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_USER_LANGUAGE_STR, &buffer)) {
				char *p = NULL;
				if(MA_OK == ma_buffer_get_string(buffer, &p, &size)) {
					p_user_lang = strdup(p);
				}
				ma_buffer_release(buffer);
			}
		}
        cur_log_records_size  = self->log_records_size;
        
        /* Enforce the policies if changed */
        rc = ( (MA_OK == (rc = ma_policy_settings_bag_get_str(policy_bag, MA_LOGGER_SERVICE_SECTION_NAME_STR, MA_LOGGER_KEY_FILTER_PATTERN_STR, MA_FALSE, &p_filter_pattern, p_filter_pattern)))
            && (MA_OK == (rc = ma_policy_settings_bag_get_str(policy_bag, MA_LOGGER_SERVICE_SECTION_NAME_STR, MA_LOGGER_KEY_FORMAT_PATTERN_STR, MA_FALSE,&p_format_pattern, p_format_pattern)))
            && (MA_OK == (rc = ma_policy_settings_bag_get_int(policy_bag, MA_LOGGER_SERVICE_SECTION_NAME_STR, MA_LOGGER_KEY_LOG_SIZE_LIMIT_INT, MA_FALSE, &log_size_limit, 2)))
			&& (MA_OK == (rc = ma_policy_settings_bag_get_bool(policy_bag, MA_LOGGER_SERVICE_SECTION_NAME_STR, MA_LOGGER_KEY_LOG_APP_LOGGING_ENABLED_INT, MA_FALSE, &app_logging_enable, MA_TRUE)))
            && (MA_OK == (rc = ma_policy_settings_bag_get_bool(policy_bag, MA_LOGGER_SERVICE_SECTION_NAME_STR, MA_LOGGER_KEY_LOG_ENABLE_ROLLOVER_INT, MA_FALSE, &rollover_enable, MA_FALSE)))
            && (MA_OK == (rc = ma_policy_settings_bag_get_int(policy_bag, MA_LOGGER_SERVICE_SECTION_NAME_STR, MA_LOGGER_KEY_LOG_MAX_ROLLOVER_INT, MA_FALSE,&max_rollovers, 1)))
            && (MA_OK == (rc = ma_policy_settings_bag_get_bool(policy_bag, MA_LOGGER_SERVICE_SECTION_NAME_STR, MA_LOGGER_KEY_IS_LOG_RECORDING_ENABLED_INT, MA_FALSE, &self->is_log_recording_enabled, MA_FALSE)))       
            && (MA_OK == (rc = ma_policy_settings_bag_get_int(policy_bag, MA_LOGGER_SERVICE_SECTION_NAME_STR, MA_LOGGER_KEY_LOG_RECORDS_SIZE_INT, MA_FALSE, &self->log_records_size, 200))))
            ? MA_OK : rc;

        
        if(MA_OK == rc) {
            ma_bool_t is_policy_changed = MA_FALSE;

            if(cur_log_records_size != self->log_records_size) {
                if(MA_OK == (rc = ma_db_transaction_begin(ma_configurator_get_database(configurator)))) {                 
                    rc = ma_logger_db_adjust_logs(ma_configurator_get_database(configurator), self->log_records_size);                    
                    (MA_OK == rc) ? ma_db_transaction_end(ma_configurator_get_database(configurator)) : ma_db_transaction_cancel(ma_configurator_get_database(configurator));
                }
            }

            if(!self->filter_pattern || (p_filter_pattern && strcmp(p_filter_pattern, self->filter_pattern))) {
                if(self->filter_pattern) free(self->filter_pattern);
                ma_logger_remove_filter(self->logger);
                if(self->logger_filter) ma_log_filter_release(self->logger_filter);
                if(MA_OK == (rc = ma_generic_log_filter_create(p_filter_pattern, &self->logger_filter))) {
                    self->filter_pattern = strdup(p_filter_pattern);
                    ma_logger_add_filter(self->logger , self->logger_filter);
                }
            }

            if((MA_OK == rc ) && (!self->file_logger_policy.format_pattern || (p_format_pattern && !strcmp(p_format_pattern, self->file_logger_policy.format_pattern)))) {
                if(self->file_logger_policy.format_pattern) free(self->file_logger_policy.format_pattern);
                self->file_logger_policy.format_pattern = strdup(p_format_pattern);
                is_policy_changed = MA_TRUE;
            }

            if((MA_OK == rc) && (self->file_logger_policy.is_rollover_enabled != rollover_enable)) {
                self->file_logger_policy.is_rollover_enabled = rollover_enable;
                is_policy_changed = MA_TRUE;
            }

			if((MA_OK == rc) && (self->file_logger_policy.is_logging_enabled != app_logging_enable)) {
                self->file_logger_policy.is_logging_enabled = app_logging_enable;
                is_policy_changed = MA_TRUE;
            }

            if((MA_OK == rc) && (self->file_logger_policy.log_size_limit != log_size_limit)) {                
                self->file_logger_policy.log_size_limit = log_size_limit;
                MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_DEBUG, "logger size in bytes(%d)", self->file_logger_policy.log_size_limit);
                is_policy_changed = MA_TRUE;
            }

            if((MA_OK == rc) && (self->file_logger_policy.rollover_policy.max_number_of_rollovers != max_rollovers)) {
                self->file_logger_policy.rollover_policy.max_number_of_rollovers = max_rollovers;
                is_policy_changed = MA_TRUE;
            }
                                
            if(MA_TRUE == is_policy_changed) {
                rc = ma_file_logger_set_policy((ma_file_logger_t *)self->logger, &self->file_logger_policy);
            }

            
            if(MA_OK == ma_policy_settings_bag_get_str(policy_bag, MA_AGENT_LANGUAGE_OPTIONS_SECTION_NAME_STR, MA_AGENT_ENABLE_LANG_SELECTION_STR, MA_TRUE, &p_enable_agent_lang_selection, "0409")) {
                const char *p_lang = NULL;
                const char *default_lang_id = "0409";
				size_t size = 0;
                const ma_misc_system_property_t *misc_prop = NULL;
				ma_buffer_t *buffer = NULL;
				if(0 == strcmp(p_enable_agent_lang_selection,"0") && p_user_lang) {
					b_overide_lang = MA_TRUE;
				}

				if(b_overide_lang) {
					if(MA_OK == (rc = ma_ds_get_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_USER_LANGUAGE_STR, &buffer))) {
						if(MA_OK == (rc = ma_buffer_get_string(buffer, &p_lang, &size))) {
							MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_DEBUG, "Language override enforced, reading language from datastore.");
						}
					}
				}
				else {
					if(MA_OK == (rc = ma_policy_settings_bag_get_str(policy_bag, MA_AGENT_LANGUAGE_OPTIONS_SECTION_NAME_STR, MA_AGENT_LANGUAGE_SELECTION_STR, MA_FALSE, &p_lang, default_lang_id))) {
						MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_DEBUG, "Language being read from policy.");
					}
				}
				
				if(p_lang && 0 != strcmp(p_lang, self->lang_id)) {
					size = strlen(p_lang);
					ma_ds_set_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_LANGUAGE_STR, p_lang, size);
					MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_DEBUG, "Changing language to %s", p_lang);
					ma_language_change(p_lang,b_overide_lang);
					if(!strcmp(p_lang, MA_AGENT_DEFAULT_LANG_ID)) {
						misc_prop = ma_system_property_misc_get(MA_CONTEXT_GET_SYSTEM_PROPERTY(context), MA_FALSE);
						p_lang = strcmp(misc_prop->language_code, "") ? misc_prop->language_code : default_lang_id;
					}
					if(MA_OK == (rc = ma_ds_set_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_LANGUAGE_STR, p_lang, strlen(p_lang)))) {
						strcpy(self->lang_id, p_lang);
						localizer_restart = MA_TRUE;
					}
				}

				if(self->lang_id[0] != '\0') {
					char res_path[MA_MAX_PATH_LEN] = {0};
					char fallback_lang[MA_MAX_PATH_LEN] = "0409";
					const char *path = ma_configurator_get_agent_install_path(MA_CONTEXT_GET_CONFIGURATOR(self->context));
					if(path) {
						struct stat st;
						MA_MSC_SELECT(_snprintf, snprintf)(res_path, MA_MAX_PATH_LEN, "%s%c%s%c%s", path, MA_PATH_SEPARATOR, self->lang_id, MA_PATH_SEPARATOR, res_ini_file);
						if(-1 == stat(res_path, &st)) {
							if(MA_OK == (rc = ma_ds_set_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_LANGUAGE_STR, fallback_lang, strlen(fallback_lang)))) {
								strcpy(self->lang_id, fallback_lang);
								localizer_restart = MA_TRUE;
							}
						}
					}
				}

				if(localizer_restart) {
					(void)ma_localizer_stop(self->localizer);
					(void)ma_localizer_start(self->localizer);
				}
				if(buffer)
					(void)ma_buffer_release(buffer);
			}
        }
		if(p_user_lang)
			free(p_user_lang);
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}
 
static ma_error_t service_start(ma_service_t *service) {
    if(service) {
        ma_error_t rc = MA_OK;
        ma_logger_service_t *self = (ma_logger_service_t *)service;

        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "logger service starting...");
        if(MA_OK == ma_localizer_start(((ma_logger_service_t*)service)->localizer)) {
            const char *lang = NULL;

            if(MA_OK == (rc = ma_localizer_get_language(((ma_logger_service_t*)service)->localizer, &lang))) {
                strncpy(((ma_logger_service_t*)service)->lang_id, lang, MA_MAX_NAME-1);
            }
        }

        if(MA_OK != (rc = ma_msgbus_server_start(self->logger_server, ma_logger_service_server_handler,self))) {            
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to start the service %s .", MA_LOGGER_SERVICE_NAME_STR);
            return rc;
        }

        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t service_stop(ma_service_t *service) {
    if(service) {
        ma_logger_service_t *self = (ma_logger_service_t *)service;
        ma_error_t rc = MA_OK;

        MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "logger service stopping...");

        if(MA_OK != (rc = ma_msgbus_server_stop(self->logger_server))) {
            MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Failed to stop the service %s .", MA_LOGGER_SERVICE_NAME_STR);
            return rc;
        }

        (void)ma_logger_unregister_visitor(self->logger);
        (void)ma_logger_unregister_localizer(self->logger);

        return ma_localizer_stop(((ma_logger_service_t*)service)->localizer);
    }
    return MA_OK;
}

static void service_destroy(ma_service_t *service) {
    if(service) {
        ma_logger_service_t *self = (ma_logger_service_t *)service;

        if(self->logger_server) ma_msgbus_server_release(self->logger_server);

        if(self->logger_filter) {
            (void)ma_logger_remove_filter(self->logger);
            (void)ma_log_filter_release(self->logger_filter);
        }

        if(self->localizer)  (void)ma_localizer_release(self->localizer);
        if(self->filter_pattern) free(self->filter_pattern);
        if(self->file_logger_policy.format_pattern) free(self->file_logger_policy.format_pattern);

        (void)ma_logger_release(self->logger);
    }
}

static ma_error_t ma_logger_service_server_handler(ma_msgbus_server_t *server, ma_message_t *msg, void *cb_data, ma_msgbus_client_request_t *c_request) {
    ma_logger_service_t *self = (ma_logger_service_t *)cb_data;    
	const char *msg_type = NULL;
	ma_message_t *response = NULL;
	ma_error_t rc = MA_OK;
    
	if( msg && (MA_OK == ma_message_get_property(msg, MA_PROP_KEY_LOGGER_REQUEST_TYPE_STR, &msg_type)) && msg_type){
		MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Logger service received message type <%s>.", msg_type);
		if(!strcmp(msg_type, MA_PROP_VALUE_LOG_LOCALIZE_MESSAGE_STR)){
            rc = handle_log_localize_message(self, msg);
		}		
		else if(!strcmp(msg_type, MA_PROP_VALUE_GET_PRODUCTS_STR)){
            rc = handle_get_products(self, c_request);
		}
		else if(!strcmp(msg_type, MA_PROP_VALUE_GET_PRODUCT_LOG_FILES_STR)){
			const char *product_id = NULL;
			(void) ma_message_get_property(msg, MA_PRODUCT_ID_STR, &product_id);
            rc = (product_id) ? handle_get_product_log_files(self, product_id, c_request) : MA_ERROR_INVALID_REQUEST;
		}
		else if(!strcmp(msg_type, MA_PROP_VALUE_GET_PRODUCT_LOG_PATH_STR)){
			const char *file_name = NULL, *product_id = NULL;;
			(void) ma_message_get_property(msg, MA_LOG_FILE_NAME_STR, &file_name);
			(void) ma_message_get_property(msg, MA_PRODUCT_ID_STR, &product_id);
			rc = (file_name) ? handle_get_product_log_file_path(self, product_id, file_name, c_request) : MA_ERROR_INVALID_REQUEST;
		}
        else
            rc = MA_ERROR_INVALID_REQUEST;
	}
    else 
        rc = MA_ERROR_INVALID_REQUEST;

    /* Reply with MA_ERROR_INVALID_REQUEST status to clients */
    if(MA_ERROR_INVALID_REQUEST == rc) {
        if(MA_OK == (rc = ma_message_create(&response))) {
            rc = ma_msgbus_server_client_request_post_result(c_request, rc, response);
            (void) ma_message_release(response);
        }
    }
    return rc;
}

static ma_error_t handle_get_products(ma_logger_service_t *self, ma_msgbus_client_request_t *c_request) {
	ma_error_t rc = MA_OK;	
	ma_table_t *product_tb = NULL;
	ma_variant_t *payload = NULL;

	if(MA_OK == (rc = ma_table_create(&product_tb))) {
		ma_conf_application_list_t *product_list = NULL;
		if(MA_OK == (rc = ma_conf_applications_create(&product_list))) {
			if(MA_OK == (rc = ma_conf_applications_scan(product_list))) {
				size_t count = 0, i = 0;
				ma_conf_applications_get_size(product_list, &count);
				for(i = 0; i < count; i++) {
					const char *product_id = NULL;
					if(MA_OK == (rc = ma_conf_applications_get_product_id(product_list, i, &product_id))) {
						ma_temp_buffer_t temp_buf = MA_TEMP_BUFFER_INIT;
						if(MA_OK == ma_conf_application_get_property_str(product_id, REG_PRODUCT_DISPLAY_NAME_STR, &temp_buf)) {
							ma_variant_t *product_name_var = NULL;
							char *product_name = (char *)ma_temp_buffer_get(&temp_buf);
							if(MA_OK == (rc = ma_variant_create_from_string(product_name, strlen(product_name), &product_name_var))) {
								rc = ma_table_add_entry(product_tb, product_id, product_name_var);
								(void) ma_variant_release(product_name_var); product_name_var = NULL;
							}
						}
						ma_temp_buffer_uninit(&temp_buf);
					}
				}
			}
			(void) ma_conf_applications_release(product_list);
		}

		if((MA_OK == (rc = ma_variant_create_from_table(product_tb, &payload)))) {
			ma_message_t *response = NULL;
			if(MA_OK == (rc = ma_message_create(&response))) { 
				(void) ma_message_set_property(response, MA_PROP_KEY_LOGGER_RESPONSE_TYPE_STR, MA_PROP_VALUE_GET_PRODUCTS_STR);
				(void)ma_message_set_payload(response, payload);
				rc = ma_msgbus_server_client_request_post_result(c_request, rc, response);
				(void)ma_message_release(response); response = NULL;
			}
			(void)ma_variant_release(payload); payload = NULL;
		}
		(void)ma_table_release(product_tb), product_tb = NULL;
	}
	return rc;
}

static ma_error_t handle_get_product_log_files(ma_logger_service_t *self, const char *product_id, ma_msgbus_client_request_t *c_request) {
	ma_error_t rc = MA_OK;	
	ma_array_t *log_file_array = NULL;
	if(MA_OK == (rc = ma_array_create(&log_file_array))) {
		ma_variant_t *payload = NULL;
		ma_temp_buffer_t temp_path = MA_TEMP_BUFFER_INIT, temp_pattern = MA_TEMP_BUFFER_INIT;
		if(MA_OK == ma_conf_application_get_property_str(product_id, REG_PRODUCT_LOG_FILE_PATH, &temp_path) &&
		   MA_OK == ma_conf_application_get_property_str(product_id, REG_PRODUCT_LOG_FILE_PATTERN_PATH, &temp_pattern) ) {
			const char *log_file_path = (const char *)ma_temp_buffer_get(&temp_path) ;
			const char *log_pattern = (const char *)ma_temp_buffer_get(&temp_pattern) ;
			if(log_file_path) {
				ma_filesystem_iterator_t *fs_iter = NULL ;
				if(MA_OK == (rc = ma_filesystem_iterator_create(log_file_path, &fs_iter))) {
					ma_temp_buffer_t temp_file = MA_TEMP_BUFFER_INIT ;
					(void)ma_filesystem_iterator_set_mode(fs_iter, MA_FILESYSTEM_ITERATE_FILES);
					while (MA_OK == ma_filesystem_iterator_get_next(fs_iter, &temp_file)) {
						ma_variant_t *file_var = NULL;
						const char *file_name = (char *)ma_temp_buffer_get(&temp_file);
						const char *p = strrchr(file_name, '.');
						if(p && !strcmp(p, log_pattern+1)) { 
							if(MA_OK == (rc = ma_variant_create_from_string(file_name, strlen(file_name), &file_var))) {
								(void)ma_array_push(log_file_array, file_var);
								(void)ma_variant_release(file_var), file_var = NULL;
							}
						}
					}
					ma_temp_buffer_uninit(&temp_file) ;
					(void)ma_filesystem_iterator_release(fs_iter);
				}
			}
		}
		ma_temp_buffer_uninit(&temp_pattern);
		ma_temp_buffer_uninit(&temp_path);

		if((MA_OK == (rc = ma_variant_create_from_array(log_file_array, &payload)))) {
			ma_message_t *response = NULL;
			if(MA_OK == (rc = ma_message_create(&response))) { 
				(void) ma_message_set_property(response, MA_PROP_KEY_LOGGER_RESPONSE_TYPE_STR, MA_PROP_VALUE_GET_PRODUCT_LOG_FILES_STR);
				(void) ma_message_set_property(response, MA_PRODUCT_ID_STR, product_id);
				ma_message_set_payload(response, payload);
				rc = ma_msgbus_server_client_request_post_result(c_request, rc, response);
				(void)ma_message_release(response); response = NULL;
			}
			(void)ma_variant_release(payload); payload = NULL;
		}
		(void)ma_array_release(log_file_array); log_file_array = NULL;
	}
	return rc;
}

static ma_error_t handle_get_product_log_file_path(ma_logger_service_t *self, const char *product_id, const char *file_name, ma_msgbus_client_request_t *c_request) {
	ma_error_t rc = MA_OK;	
	ma_message_t *response = NULL;	
	char log_full_path[MA_MAX_PATH_LEN] = {0} ;

	ma_temp_buffer_t temp_path = MA_TEMP_BUFFER_INIT;
	if(MA_OK == ma_conf_application_get_property_str(product_id, REG_PRODUCT_LOG_FILE_PATH, &temp_path)) {
		const char *log_file_path = (const char *)ma_temp_buffer_get(&temp_path) ;
		if(log_file_path) {
			MA_MSC_SELECT(_snprintf,snprintf)(log_full_path, MA_MAX_PATH_LEN, "%s%c%s", log_file_path, MA_PATH_SEPARATOR, file_name);				
		}
	}
	ma_temp_buffer_uninit(&temp_path);

	if(MA_OK == (rc = ma_message_create(&response))) { 
		(void) ma_message_set_property(response, MA_PROP_KEY_LOGGER_RESPONSE_TYPE_STR, MA_PROP_VALUE_GET_PRODUCT_LOG_PATH_STR);
		(void) ma_message_set_property(response, MA_LOG_FILE_FULL_PATH_STR, log_full_path);					
		rc = ma_msgbus_server_client_request_post_result(c_request, rc, response);
		(void)ma_message_release(response); response = NULL;
	}

	return rc;
}

static ma_error_t handle_log_localize_message(ma_logger_service_t *self, ma_message_t *msg) {
	ma_variant_t *payload = NULL;
	ma_error_t rc = MA_OK;
    if(MA_OK == ma_message_get_payload(msg, &payload)) {
        ma_table_t *msg_tb = NULL;
        if(MA_OK == ma_variant_get_table(payload, &msg_tb)) {
            const char *time_str = NULL, *facility = NULL, *severity = NULL, *log_msg = NULL;
            ma_variant_t *value = NULL;
            ma_buffer_t *buffer = NULL, *localize_buffer = NULL;
            size_t size = 0;
            if(MA_OK == ma_table_get_value(msg_tb, MA_LOG_MSG_KEY_TIME_STR, &value)) {
                if(MA_OK == ma_variant_get_string_buffer(value, &buffer)) {
                    (void)ma_buffer_get_string(buffer, &time_str, &size);
                    (void)ma_buffer_release(buffer); buffer = NULL;
                }
                (void) ma_variant_release(value); value = NULL;
            }

            if(MA_OK == ma_table_get_value(msg_tb, MA_LOG_MSG_KEY_FACILITY_STR, &value)) {
                if(MA_OK == ma_variant_get_string_buffer(value, &buffer)) {
                    (void)ma_buffer_get_string(buffer, &facility, &size);
                    (void)ma_buffer_release(buffer); buffer = NULL;
                }
                (void) ma_variant_release(value); value = NULL;
            }

            if(MA_OK == ma_table_get_value(msg_tb, MA_LOG_MSG_KEY_SEVERITY_STR, &value)) {
                if(MA_OK == ma_variant_get_string_buffer(value, &buffer)) {
                    (void)ma_buffer_get_string(buffer, &severity, &size);
                    (void)ma_buffer_release(buffer); buffer = NULL;
                }
                (void) ma_variant_release(value); value = NULL;
            }

            if(MA_OK == ma_table_get_value(msg_tb, MA_LOG_MSG_KEY_MESSAGE_STR, &value)) {
                if(MA_OK == ma_variant_get_string_buffer(value, &buffer)) {
                    (void)ma_buffer_get_string(buffer, &log_msg, &size);
                    (void)ma_buffer_release(buffer); buffer = NULL;
                }
                (void)ma_variant_release(value); value = NULL;
            }
                    
            if(time_str && facility && severity && log_msg) {
                if(MA_OK == ma_localizer_get_localize_string(self->localizer, log_msg, &localize_buffer)) {
                    const char *localize_str = NULL;
                    ma_buffer_get_string(localize_buffer, &localize_str, &size);
                    if(localize_str) {
                        rc = process_log_data(self, time_str, severity, facility, localize_str);
                    }
                    (void) ma_buffer_release(localize_buffer);
                }
            }
            (void)ma_table_release(msg_tb);
        }
        (void) ma_variant_release(payload);
    }
	return rc;
}

static ma_error_t process_log_data(ma_logger_service_t *self, const char *time_str, const char *severity, const char *facility, const char *localize_str) {
    ma_error_t rc = MA_OK;
    ma_message_t *msg = NULL;

    if(self->is_log_recording_enabled) {            
        ma_configurator_t *configurator = MA_CONTEXT_GET_CONFIGURATOR(self->context);
        /* TBD - Will handle wide chars & add multiple logs in single operation */
        if(MA_OK == (rc = ma_db_transaction_begin(ma_configurator_get_database(configurator)))) {                        
            if(MA_OK == (rc = ma_logger_db_add_log(ma_configurator_get_database(configurator), time_str, (severity) ? severity : "", facility, localize_str)))
                rc = ma_logger_db_adjust_logs(ma_configurator_get_database(configurator), self->log_records_size);                        
            (MA_OK == rc) ? ma_db_transaction_end(ma_configurator_get_database(configurator)) : ma_db_transaction_cancel(ma_configurator_get_database(configurator));
        }
    }
    
    if(MA_OK == (rc = ma_message_create(&msg))) {                
        ma_table_t *log_msg_tb = NULL;
        if(MA_OK == (rc = ma_table_create(&log_msg_tb))) {
            ma_variant_t *var = NULL;
            if(MA_OK == (rc = ma_variant_create_from_string(time_str, strlen(time_str), &var))) {
                rc = ma_table_add_entry(log_msg_tb, MA_LOG_MSG_KEY_TIME_STR, var);
                (void) ma_variant_release(var); var = NULL;
            }

            if(severity && (MA_OK == (rc = ma_variant_create_from_string(severity, strlen(severity), &var)))) {
                rc = ma_table_add_entry(log_msg_tb, MA_LOG_MSG_KEY_SEVERITY_STR, var);
                (void) ma_variant_release(var); var = NULL;
            }

            if(facility && (MA_OK == (rc = ma_variant_create_from_string(facility, strlen(facility), &var)))) {
                rc = ma_table_add_entry(log_msg_tb, MA_LOG_MSG_KEY_FACILITY_STR, var);
                (void) ma_variant_release(var); var = NULL;
            }

            /* TBD - Will handle wide chars - rc = log_msg->msg_is_wide_char ? 
                ma_variant_create_from_wstring(log_msg->message.wchar_message, wcslen(log_msg->message.wchar_message), &payload)  :
                ma_variant_create_from_string(log_msg->message.utf8_message, strlen(log_msg->message.utf8_message), &payload);
            */

            if(localize_str && (MA_OK == (rc = ma_variant_create_from_string(localize_str, strlen(localize_str), &var)))) {
                rc = ma_table_add_entry(log_msg_tb, MA_LOG_MSG_KEY_MESSAGE_STR, var);
                (void) ma_variant_release(var); var = NULL;
            }

            if(MA_OK == (rc = ma_variant_create_from_table(log_msg_tb, &var))) {
                if(MA_OK == (rc = ma_message_set_payload(msg, var)))
                    rc = ma_msgbus_publish(MA_CONTEXT_GET_MSGBUS(self->context), MA_LOGGER_MSG_PUB_TOPIC, MSGBUS_CONSUMER_REACH_OUTPROC, msg);
                (void) ma_variant_release(var);
            }
            (void) ma_table_release(log_msg_tb);                    
        }
        (void)ma_message_release(msg);
    }
    
    return rc;
}

static void publish_log_message(const ma_log_msg_t *log_msg,  void *cb_data) {
    if(log_msg && cb_data) {
        ma_error_t rc = MA_OK;
        ma_logger_service_t *self = (ma_logger_service_t *)cb_data;
        
        /* Here we should decide what we want to do with messages, e.g. publish only localized messages or dump into db more than >Info or some random */
        if(log_msg->is_localized) {          
            const char *severity = ma_log_get_severity_label(log_msg->severity);
            char time_str[MA_MAX_LEN] = {0};

            MA_MSC_SELECT(_snprintf, snprintf)(time_str, MA_MAX_LEN, "%.4d-%.2d-%.2dT%.2d:%.2d:%.2d", log_msg->time.year, log_msg->time.month, log_msg->time.day, log_msg->time.hour, log_msg->time.minute, log_msg->time.second);

            rc = process_log_data(self, time_str, severity, log_msg->facility, log_msg->message.utf8_message);           
        }
    }
}

static ma_error_t localize_msg_cb(const char *msg, ma_buffer_t **localized_msg, void *cb_data) {
    if(msg && localized_msg && cb_data) {
        ma_logger_service_t *self = (ma_logger_service_t *)cb_data;
        return ma_localizer_get_localize_string(self->localizer, msg, localized_msg);
    }
    return MA_ERROR_INVALIDARG;
}

