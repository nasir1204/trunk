#include "ma/internal/services/io/ma_logger_db.h"

#include <string.h>

#define CREATE_LOGGER_TABLE_STMT   "CREATE TABLE IF NOT EXISTS AGENT_LOGS(DATE_TIME TEXT NOT NULL, SEVERITY TEXT NOT NULL, FACILITY TEXT NOT NULL, MESSAGE TEXT NOT NULL)"

//#define UPDATE_LOG_STATMENT_STME   "UPDATE AGENT_LOGGER SET ID = ? , DATE_TIME = ?, SEVERITY = ?, FACILITY = ?, MESSAGE = ? WHERE ID = (SELECT min(ID) FROM AGENT_LOGS)"

ma_error_t ma_logger_db_schema_create(ma_db_t *db_handle) {
    if(db_handle) {
        ma_db_statement_t *db_stmt = NULL;
        ma_error_t rc = MA_OK;

        if( MA_OK == (rc = ma_db_statement_create(db_handle, CREATE_LOGGER_TABLE_STMT, &db_stmt))) {
            rc = ma_db_statement_execute(db_stmt);
            ma_db_statement_release(db_stmt);
	    }
        return rc ;
    }
    return MA_ERROR_INVALIDARG;
}

#define ADD_LOG_STATMENT_STMT      "INSERT INTO AGENT_LOGS VALUES(?, ?, ?, ?)"
ma_error_t ma_logger_db_add_log(ma_db_t *db_handle, const char *datetime, const char *severity, const char *facility, const char *message) {
     if(db_handle && datetime && severity && facility && message) {
        ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;
        if( MA_OK == (rc = ma_db_statement_create(db_handle, ADD_LOG_STATMENT_STMT, &db_stmt))) {
                if( MA_OK == (rc = ma_db_statement_set_string(db_stmt, 1, 1, datetime, strlen(datetime)))
                    && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 2, 1, severity, strlen(severity)))
                    && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 3, 1, facility, strlen(facility)))
                    && MA_OK == (rc = ma_db_statement_set_string(db_stmt, 4, 1, message, strlen(message))) ) {
                    rc = ma_db_statement_execute(db_stmt) ;
                }
                (void) ma_db_statement_release(db_stmt) ;
        }        
        return rc ;
    }
    return MA_ERROR_INVALIDARG;
}

#define GET_LOG_STATMENTS_STMT     "SELECT * FROM AGENT_LOGS ORDER BY ROWID ASC, DATE_TIME ASC"
ma_error_t ma_logger_db_get_logs(ma_db_t *db_handle, ma_db_recordset_t **db_record) {
    if(db_handle && db_record) {
        ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;

        if( MA_OK == (rc = ma_db_statement_create(db_handle, GET_LOG_STATMENTS_STMT, &db_stmt)) ) {
            rc = ma_db_statement_execute_query(db_stmt, db_record) ;
            ma_db_statement_release(db_stmt) ;
        }
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}

#define ADJUST_LOG_STATMENTS_STMT  "DELETE FROM AGENT_LOGS WHERE ROWID NOT IN (SELECT ROWID FROM AGENT_LOGS ORDER BY DATE_TIME DESC LIMIT ?)"
ma_error_t ma_logger_db_adjust_logs(ma_db_t *db_handle, ma_int32_t records_size) {
     if(db_handle && records_size > 0) {
        ma_db_statement_t *db_stmt = NULL ;
        ma_error_t rc = MA_OK ;
        if( MA_OK == (rc = ma_db_statement_create(db_handle, ADJUST_LOG_STATMENTS_STMT, &db_stmt))) {
            if( MA_OK == (rc = ma_db_statement_set_int(db_stmt, 1, records_size)) ) {
                rc = ma_db_statement_execute(db_stmt) ;
            }
            (void) ma_db_statement_release(db_stmt) ;
        }
       return rc ;
    }
    return MA_ERROR_INVALIDARG;
}
