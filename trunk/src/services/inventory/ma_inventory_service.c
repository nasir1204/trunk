#include "ma/internal/services/inventory/ma_inventory_service.h"
#include "ma/internal/services/scheduler/ma_task_internal.h"
#include "ma/internal/utils/profiler/ma_profiler.h"
#include "ma/internal/utils/profiler/ma_profile_info.h"
#include "ma/internal/utils/booker/ma_booking_info.h"
#include "ma/internal/utils/booker/ma_booker.h"
#include "ma/internal/utils/context/ma_context.h"
#include "ma/internal/defs/ma_inventory_defs.h"
#include "ma/internal/utils/recorder/ma_recorder_info.h"
#include "ma/internal/utils/recorder/ma_recorder.h"
#include "ma/internal/services/ma_service.h"
#include "ma/ma_log.h"
#include "ma/ma_message.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/ma_variant.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/sensor/ma_sensor_msg.h"
#include "ma/sensor/ma_sensor_utils.h"
#include "ma/internal/utils/inventory/ma_inventory.h"
#include "ma/scheduler/ma_triggers.h"
#include "ma/scheduler/ma_task.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"
#include "ma/internal/services/scheduler/ma_scheduler_core.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

const char base_path[] = "/inventory/";

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "inventory service"

MA_CPP(extern "C" {)
ma_error_t ma_mail_send(const char *from, const char *to, const char *sub, const char *body);
MA_CPP(})

static ma_error_t ma_inventory_service_task_add(ma_inventory_service_t *inventory, const char *inventory_id);

struct ma_inventory_service_s {
    ma_service_t            service_base;
    ma_context_t            *context;
    ma_msgbus_server_t      *server;
    ma_logger_t             *logger;
    ma_msgbus_subscriber_t  *subscriber;
    ma_inventory_t             *inventory;
};


static ma_error_t inventory_subscriber_cb(const char *topic, ma_message_t *message, void *cb_data);
static ma_error_t ma_inventory_service_cb(ma_msgbus_server_t *server, ma_message_t *msg, void *cb_data, ma_msgbus_client_request_t *c_request);

static ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint);
static ma_error_t service_start(ma_service_t *service);
static ma_error_t service_stop(ma_service_t *service);
static void service_release(ma_service_t *service);

static const ma_service_methods_t service_methods = {
    &service_configure,
    &service_start,
    &service_stop,
    &service_release
};

ma_error_t ma_inventory_service_create(const char *service_name, ma_service_t **service) {
    if(service_name && service) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;

        if((*service = (ma_service_t*)calloc(1, sizeof(ma_inventory_service_t)))) {
            ma_service_init(*service, &service_methods, service_name, service);
            return MA_OK;
        }
        return err ;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_service_release(ma_inventory_service_t *self) {
    if(self) {
        ma_error_t err = MA_OK;
        if(self->server && (MA_OK != (err = ma_msgbus_server_release(self->server)))) {
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "msgbus server release failed");
        }
        (void)ma_msgbus_subscriber_release(self->subscriber);
        free(self) ;
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_bool_t check_context_validity(ma_context_t *context){	
    /*Will see if we need this*/
    return MA_TRUE;
}

ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint) {
    if(service && context) {
        ma_error_t rc = MA_OK;
        if(MA_SERVICE_CONFIG_NEW == hint && check_context_validity(context)) {
            ma_configurator_t *configurator = MA_CONTEXT_GET_CONFIGURATOR(context);
            ma_inventory_service_t *self = (ma_inventory_service_t *)service;
            ma_msgbus_t *msgbus = MA_CONTEXT_GET_MSGBUS(context);

            self->context = context;
            self->logger = MA_CONTEXT_GET_LOGGER(context);

            if(!ma_configurator_get_inventory_datastore(configurator))
                 MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_INFO, "inventory datastore does not exist");
            if(MA_OK == (rc = ma_inventory_create(msgbus, ma_configurator_get_inventory_datastore(configurator), MA_INVENTORY_BASE_PATH_STR, &self->inventory))) {
                (void)ma_inventory_set_logger(self->logger);
                if(MA_OK == (rc = ma_msgbus_subscriber_create(MA_CONTEXT_GET_MSGBUS(context), &self->subscriber))) {
                    (void)ma_msgbus_subscriber_setopt(self->subscriber, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                    (void)ma_msgbus_subscriber_setopt(self->subscriber, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
                    if(MA_OK == (rc = ma_msgbus_server_create(msgbus, MA_INVENTORY_SERVICE_NAME_STR, &self->server))) {                        
                        (void)ma_msgbus_server_setopt(self->server, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
                        if(MA_OK == (rc = ma_msgbus_server_setopt(self->server, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC))) {
                            /*TODO - No need to add the inventory object in the context if we can use the inventory client API's */
                            ma_context_add_object_info(context, MA_OBJECT_INVENTORY_NAME_STR, (void *const *)&self->inventory);
                            MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_DEBUG, "inventory service configuration successful");
                            return MA_OK;
                        }
                        ma_msgbus_server_release(self->server);
                    }
                    ma_msgbus_subscriber_release(self->subscriber);
                }
            }
            else
                MA_LOG(self->logger, MA_LOG_SEV_ERROR, "inventory creation failed, last error(%d)", rc);
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_inventory_service_task_start(ma_inventory_service_t *service, const char *task_id) {
    if(service && task_id) {
        ma_context_t *context = service->context;
        ma_task_t *task = NULL ;
        ma_error_t err = MA_OK ;
        ma_scheduler_t *scheduler = MA_CONTEXT_GET_SCHEDULER(context) ;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(context);

        if(MA_OK == (err = ma_scheduler_get_task_handle(scheduler, task_id, &task))) {
            if(MA_OK == (err = ma_task_start(task))) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "Inventory service task(%s) started", task_id) ;
            }
            (void)ma_task_release(task);
        }
        if(MA_OK != err)
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "Inventory service task(%s) start failed(%d)", task_id, err) ;

        return err ;
    }
    return MA_ERROR_INVALIDARG ;
}
ma_error_t service_start(ma_service_t *service) {
    if(service) {
        ma_error_t err = MA_OK;
        ma_inventory_service_t *self = (ma_inventory_service_t *)service ;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);
        
        if(MA_OK == (err = ma_inventory_start(self->inventory))) {
            if(MA_OK == (err = ma_msgbus_server_start(((ma_inventory_service_t*)service)->server, ma_inventory_service_cb, service))) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "inventory service started successfully");
                if(MA_OK == (err = ma_msgbus_subscriber_register(((ma_inventory_service_t*)service)->subscriber,  "ma.task.*", inventory_subscriber_cb, service))) {
                    ma_task_t *task = NULL;

                    MA_LOG(((ma_inventory_service_t*)service)->logger, MA_LOG_SEV_DEBUG, "inventory subscriber registered successfully");
                    if(MA_ERROR_SCHEDULER_INVALID_TASK_ID == (err = ma_scheduler_get_task_handle(MA_CONTEXT_GET_SCHEDULER(self->context), MA_INVENTORY_TASK_ID, &task))) {
                        if(MA_OK == (err = ma_inventory_service_task_add(self, MA_INVENTORY_TASK_ID))) {
                            MA_LOG(logger, MA_LOG_SEV_TRACE, "Inventory Task id(%s) added successfully", MA_INVENTORY_TASK_ID);
                            err = ma_inventory_service_task_start(self, MA_INVENTORY_TASK_ID);
                        } else
                            MA_LOG(logger, MA_LOG_SEV_ERROR, "Inventory Task id(%s) add failed, last error(%d)", MA_INVENTORY_TASK_ID, err);
                    } else {
                        MA_LOG(logger, MA_LOG_SEV_TRACE, "Inventory Task id(%s) exist", MA_INVENTORY_TASK_ID);
                    }
                    return MA_OK ;
                } 
                else 
                    MA_LOG(((ma_inventory_service_t*)service)->logger, MA_LOG_SEV_ERROR, "inventory service failed to subscribe topic, errocode(%d)", err);
                ma_msgbus_server_stop(((ma_inventory_service_t*)service)->server);
            }
            else
                MA_LOG(logger, MA_LOG_SEV_CRITICAL, "inventory service start failed, last error(%d)", err);
            ma_inventory_stop(self->inventory);
        }
        else
            MA_LOG(logger, MA_LOG_SEV_CRITICAL, "inventory service start failed, last error(%d)", err);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_service_get_version(ma_uint32_t *version) {
    if(version) {
        *version = 0x05020100; //TODO fetch version 
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_inventory_service_start(ma_inventory_service_t *service) {
    return service_start((ma_service_t*)service);
}

ma_error_t ma_inventory_service_stop(ma_inventory_service_t *service) {
    return service_stop((ma_service_t*)service);
}

ma_error_t service_stop(ma_service_t *service) {
    if(service) {
        ma_error_t err = MA_OK;
        ma_inventory_service_t *self = (ma_inventory_service_t *)service;

        if(MA_OK != (err = ma_msgbus_subscriber_unregister(((ma_inventory_service_t*)service)->subscriber))) {
            MA_LOG(((ma_inventory_service_t*)service)->logger, MA_LOG_SEV_ERROR, "inventory service unregistering subscriber failed, last error(%d)", err);
        }

        if(MA_OK == (err = ma_msgbus_server_stop(self->server))) {
            if(MA_OK != (err = ma_inventory_stop(self->inventory))) 
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_CRITICAL, "inventory stop failed, last error(%d)", err);
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO, "inventory service stopped successfully");
        }
        else
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_CRITICAL, "inventory service stop failed, last error(%d)", err);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

void service_release(ma_service_t *service) {
    if(service) {
        ma_inventory_service_release((ma_inventory_service_t *)service) ;
        return ;
    }
    return ;
}

ma_error_t ma_inventory_service_get_inventory(ma_inventory_service_t *service, ma_inventory_t **inventory) {
    if(service && inventory) {
        if(!service->inventory)
            MA_LOG(service->logger, MA_LOG_SEV_ERROR, "Inventory is empty!!!!!!!!!!!!!!!!!!!!!!");
        *inventory = service->inventory;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t order_create_handler(ma_inventory_service_t *service, ma_message_t *req_msg, ma_message_t *rsp_msg) {
    if(service && req_msg && rsp_msg) {
        ma_error_t err = MA_OK;
        const char *inventory_id = NULL;
        char *status = MA_INVENTORY_ID_NOT_EXIST;
        ma_variant_t *payload = NULL;
        char error_code[MA_MAX_LEN] = {0};
        ma_inventory_info_t *info = NULL;

        if(MA_OK == (err = ma_message_get_property(req_msg, MA_INVENTORY_ID, &inventory_id))) {
            (void)ma_message_set_property(rsp_msg, MA_INVENTORY_ID, inventory_id);
            MA_LOG(service->logger, MA_LOG_SEV_TRACE, "inventory id(%s) create order processing", inventory_id);
            if(MA_OK == (err = ma_message_get_payload(req_msg, &payload))) {
                if(MA_OK == (err = ma_inventory_info_convert_from_variant(payload, &info))) {
                    char id[MA_MAX_NAME+1] = {0};

                    //(void)ma_inventory_info_get_email_id(info, id);
                    (void)ma_inventory_info_get_contact(info, id);
                    if(MA_OK == (err = ma_inventory_add_variant(service->inventory, inventory_id, payload))) {
                        status = MA_INVENTORY_ID_WRITTEN;
                        MA_LOG(service->logger, MA_LOG_SEV_TRACE, "inventory_id(%s) crn(%s) written into DB successfully", inventory_id, id);

                    }
                    (void)ma_inventory_info_release(info);
                }
                (void)ma_variant_release(payload);
            }
        }
        if(MA_OK != err)
            MA_LOG(service->logger, MA_LOG_SEV_CRITICAL, "inventory order creation failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(rsp_msg, MA_INVENTORY_STATUS, status);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t order_modify_handler(ma_inventory_service_t *service, ma_message_t *req_msg, ma_message_t *rsp_msg) {
    if(service && req_msg && rsp_msg) {
        ma_error_t err = MA_OK;
        const char *inventory_id = NULL;
        char *status = MA_INVENTORY_ID_NOT_EXIST;
        ma_variant_t *payload = NULL;
        char error_code[MA_MAX_LEN] = {0};
        ma_inventory_info_t *info = NULL;

        if(MA_OK == (err = ma_message_get_property(req_msg, MA_INVENTORY_ID, &inventory_id))) {
            (void)ma_message_set_property(rsp_msg, MA_INVENTORY_ID, inventory_id);
            MA_LOG(service->logger, MA_LOG_SEV_TRACE, "inventory id(%s) order modify request  processing", inventory_id);
            if(MA_OK == (err = ma_message_get_payload(req_msg, &payload))) {
                if(MA_OK == (err = ma_inventory_info_convert_from_variant(payload, &info))) {
                    char id[MA_MAX_NAME+1] = {0};

                    //(void)ma_inventory_info_get_email_id(info, id);
                    (void)ma_inventory_info_get_contact(info, id);
                    if(MA_OK == (err = ma_inventory_add_variant(service->inventory, inventory_id, payload))) {
                        status = MA_INVENTORY_ID_WRITTEN;
                        MA_LOG(service->logger, MA_LOG_SEV_TRACE, "inventory_id(%s) crn(%s) written into DB successfully", inventory_id, id);

                    }
                    (void)ma_inventory_info_release(info);
                }
                (void)ma_variant_release(payload);
            }
        }
        if(MA_OK != err)
            MA_LOG(service->logger, MA_LOG_SEV_CRITICAL, "inventory order modify failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(rsp_msg, MA_INVENTORY_STATUS, status);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t order_delete_handler(ma_inventory_service_t *service, ma_message_t *req_msg, ma_message_t *rsp_msg) {
    if(service && req_msg && rsp_msg) {
        ma_error_t err = MA_OK;
        const char *inventory_id = NULL;
        const char *crn = NULL;
        char *status = MA_INVENTORY_ID_NOT_EXIST;
        char error_code[MA_MAX_LEN] = {0};

        if(MA_OK == (err = ma_message_get_property(req_msg, MA_INVENTORY_ID, &inventory_id)) &&
           MA_OK == (err = ma_message_get_property(req_msg, MA_INVENTORY_CRN, &crn))) {
            (void)ma_message_set_property(rsp_msg, MA_INVENTORY_ID, inventory_id);
            (void)ma_message_set_property(rsp_msg, MA_INVENTORY_CRN, crn);
            MA_LOG(service->logger, MA_LOG_SEV_TRACE, "inventory id(%s) crn(%s) order delete request processing", inventory_id, crn);
            if(MA_OK == (err = ma_inventory_delete_crn(service->inventory, inventory_id, crn))) {
                status = MA_INVENTORY_ID_DELETED;
                MA_LOG(service->logger, MA_LOG_SEV_TRACE, "inventory id(%s) crn (%s) deleted successfully", inventory_id, crn);
            } else {
                MA_LOG(service->logger, MA_LOG_SEV_ERROR, "inventory id(%s) crn(%s) order delete failed, last error(%d)", inventory_id, crn ,err);
                status = MA_INVENTORY_ID_NOT_EXIST;
            }
        }
        if(MA_OK != err)
            MA_LOG(service->logger, MA_LOG_SEV_CRITICAL, "inventory order delete failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(rsp_msg, MA_INVENTORY_STATUS, status);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t order_update_status_handler(ma_inventory_service_t *service, ma_message_t *req_msg, ma_message_t *rsp_msg) {
    if(service && req_msg && rsp_msg) {
        ma_error_t err = MA_OK;
        const char *inventory_id = NULL;
        const char *crn = NULL;
        char *status = MA_INVENTORY_ID_NOT_EXIST;
        char error_code[MA_MAX_LEN] = {0};
        const char *inventory_status = NULL;

        if(MA_OK == (err = ma_message_get_property(req_msg, MA_INVENTORY_ID, &inventory_id)) &&
           MA_OK == (err = ma_message_get_property(req_msg, MA_INVENTORY_CRN, &crn)) &&
           MA_OK == (err = ma_message_get_property(req_msg, MA_INVENTORY_STATUS, &inventory_status))) {
            ma_inventory_info_t *info = NULL;

            (void)ma_message_set_property(rsp_msg, MA_INVENTORY_ID, inventory_id);
            (void)ma_message_set_property(rsp_msg, MA_INVENTORY_CRN, crn);
            MA_LOG(service->logger, MA_LOG_SEV_TRACE, "inventory id(%s) crn(%s) order update request processing", inventory_id, crn);
            if(MA_OK == (err = ma_inventory_get(service->inventory, inventory_id, crn, &info))) {
                status = MA_INVENTORY_ID_EXIST;
                //TODO : update status
                MA_LOG(service->logger, MA_LOG_SEV_TRACE, "inventory id(%s) crn (%s) deleted successfully", inventory_id, crn);
                (void)ma_inventory_info_release(info);
            } else {
                MA_LOG(service->logger, MA_LOG_SEV_ERROR, "inventory id(%s) crn(%s) order delete failed, last error(%d)", inventory_id, crn ,err);
                status = MA_INVENTORY_ID_NOT_EXIST;
            }
        }
        if(MA_OK != err)
            MA_LOG(service->logger, MA_LOG_SEV_CRITICAL, "inventory order delete failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(rsp_msg, MA_INVENTORY_STATUS, status);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t inventory_delete_handler(ma_inventory_service_t *service, ma_message_t *req_msg, ma_message_t *rsp_msg) {
    if(service && req_msg && rsp_msg) {
        ma_error_t err = MA_OK;
        const char *inventory_id = NULL;
        char *status = MA_INVENTORY_ID_NOT_EXIST;
        char error_code[MA_MAX_LEN] = {0};

        if(MA_OK == (err = ma_message_get_property(req_msg, MA_INVENTORY_ID, &inventory_id))) {
            (void)ma_message_set_property(rsp_msg, MA_INVENTORY_ID, inventory_id);
            MA_LOG(service->logger, MA_LOG_SEV_TRACE, "inventory id(%s) delete request processing", inventory_id);
            if(MA_OK == (err = ma_inventory_delete(service->inventory, inventory_id))) {
                status = MA_INVENTORY_ID_DELETED;
                MA_LOG(service->logger, MA_LOG_SEV_TRACE, "inventory id(%s) deleted successfully", inventory_id);
            } else {
                MA_LOG(service->logger, MA_LOG_SEV_ERROR, "inventory id(%s) order delete failed, last error(%d)", inventory_id, err);
                status = MA_INVENTORY_ID_NOT_EXIST;
            }
        }
        if(MA_OK != err)
            MA_LOG(service->logger, MA_LOG_SEV_CRITICAL, "inventory order delete failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(rsp_msg, MA_INVENTORY_STATUS, status);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t inventory_register_handler(ma_inventory_service_t *service, ma_message_t *inventory_req_msg, ma_message_t *inventory_rsp_msg) {
    if(service && inventory_req_msg && inventory_rsp_msg) {
        ma_error_t err = MA_OK;
        const char *inventory_id = NULL;
        char *inventory_reg_status = MA_TASK_REPLY_STATUS_FAILED;
        ma_variant_t *inventory_variant = NULL;
        char error_code[MA_MAX_LEN] = {0};


        (void)ma_message_set_property(inventory_rsp_msg, MA_INVENTORY_ID, inventory_id);
        if(MA_OK == (err = ma_message_get_property(inventory_req_msg, MA_INVENTORY_ID, &inventory_id))) {
            inventory_reg_status = MA_INVENTORY_ID_NOT_EXIST;
            if(MA_OK == ma_message_get_payload(inventory_req_msg, &inventory_variant)) {
                if(MA_OK == (err = ma_inventory_add_variant(service->inventory, inventory_id, inventory_variant))) {
                    MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "inventory id(%s) written into DB successfully", inventory_id);
                    inventory_reg_status = MA_INVENTORY_ID_WRITTEN;
                } else
                    MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_ERROR, "inventory id(%s) registration failed, last error(%d)", inventory_id, err);
                (void)ma_variant_release(inventory_variant);
            }
        }
        if(MA_OK != err)
            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_CRITICAL, "inventory id register failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(inventory_rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(inventory_rsp_msg, MA_INVENTORY_STATUS, inventory_reg_status);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t inventory_deregister_handler(ma_inventory_service_t *service, ma_message_t *inventory_req_msg, ma_message_t *inventory_rsp_msg) {
    if(service && inventory_req_msg && inventory_rsp_msg) {
        ma_error_t err = MA_OK;
        const char *inventory_id = NULL;
        char *inventory_reg_status = MA_INVENTORY_ID_NOT_EXIST;
        char error_code[MA_MAX_LEN] = {0};


        (void)ma_message_set_property(inventory_rsp_msg, MA_INVENTORY_ID, inventory_id);
        if(MA_OK == (err = ma_message_get_property(inventory_req_msg, MA_INVENTORY_ID, &inventory_id))) {
            if(MA_OK == (err = ma_inventory_delete(service->inventory, inventory_id))) {
                inventory_reg_status = MA_INVENTORY_ID_DELETED;
                MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "inventory id(%s) deleted successfully", inventory_id);
            }
        }
        if(MA_OK != err)
            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_CRITICAL, "inventory id deregister failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(inventory_rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(inventory_rsp_msg, MA_INVENTORY_STATUS, inventory_reg_status);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_inventory_service_task_add(ma_inventory_service_t *inventory, const char *inventory_id) {
    if(inventory && inventory_id) {
         ma_error_t err = MA_OK;
         ma_task_t *task = NULL;
         ma_trigger_t *trigger = NULL;
         ma_trigger_daily_policy_t policy = {1};
         ma_scheduler_t *scheduler = MA_CONTEXT_GET_SCHEDULER(inventory->context);

         if(MA_OK == (err = ma_task_create(inventory_id, &task))) {
             (void)ma_task_set_software_id(task, MA_SOFTWAREID_GENERAL_STR);
             (void)ma_task_set_creator_id(task, MA_INVENTORY_SERVICE_SECTION_NAME_STR);
             (void)ma_task_set_type(task, MA_INVENTORY_TASK_TYPE);
             (void)ma_task_set_name(task, inventory_id);
             (void)ma_task_set_max_run_time_limit(task, MA_INVENTORY_TASK_MAX_RUNTIME);
             (void)ma_task_set_condition(task, MA_TASK_COND_HIDDEN);
             (void)ma_task_set_time_zone(task, MA_TIME_ZONE_UTC);
             if(MA_OK == (err = ma_trigger_create_daily(&policy, &trigger))) {
                 ma_task_time_t begin_date = {0};

                 get_localtime(&begin_date);
                 begin_date.hour=00; begin_date.minute=00; begin_date.secs=00;
                 MA_LOG(inventory->logger, MA_LOG_SEV_TRACE, "Iventory Task Begin Date(%hu-%hu-%hu:%hu:%hu:%hu)", begin_date.month, begin_date.day, begin_date.year, begin_date.hour, begin_date.minute, begin_date.secs);
                 (void)ma_trigger_set_begin_date(trigger, &begin_date);
                 if(MA_OK == (err = ma_task_add_trigger(task, trigger))) {
                     if(MA_OK == (err = ma_scheduler_add(scheduler, task))) {
                         MA_LOG(inventory->logger, MA_LOG_SEV_INFO, "Inventory task id(%s) added successfully into scheduler", inventory_id);
                     } else
                         MA_LOG(inventory->logger, MA_LOG_SEV_ERROR, "Inventory task id(%s) added successfully into scheduler failed, last error(%d)", inventory_id, err);
                 }
                 (void)ma_trigger_release(trigger);
             }
             (void)ma_task_release(task);
         }
         return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_inventory_service_cb(ma_msgbus_server_t *server, ma_message_t *msg, void *cb_data, ma_msgbus_client_request_t *c_request) {
    if(server && msg && cb_data) {
        ma_error_t err = MA_OK;
        const char *msg_type = NULL;
        ma_message_t *response = NULL;
        ma_inventory_service_t *service = (ma_inventory_service_t*)cb_data;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(service->context);

        MA_LOG(logger, MA_LOG_SEV_DEBUG, "Inventory service callback invoked");
        if(MA_OK == (err = ma_message_create(&response))) {
            if(MA_OK == (err = ma_message_get_property(msg, MA_INVENTORY_SERVICE_MSG_TYPE, &msg_type))) {
                if(!strcmp(msg_type, MA_INVENTORY_SERVICE_MSG_ORDER_CREATE_TYPE)) {
                    if(MA_OK == (err = order_create_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client order create request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_INVENTORY_SERVICE_MSG_ORDER_MODIFY_TYPE)) {
                    if(MA_OK == (err = order_modify_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client order modify request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_INVENTORY_SERVICE_MSG_REGISTER)) {
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "Inventory register request received");
                    if(MA_OK == (err = inventory_register_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client order modify request failed, last error(%d)", err);
                        }
                    } else
                        MA_LOG(logger, MA_LOG_SEV_ERROR, "inventory register request failed, last error(%d)", err);
                } else if(!strcmp(msg_type, MA_INVENTORY_SERVICE_MSG_DEREGISTER)) {
                    if(MA_OK == (err = inventory_deregister_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client order modify request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_INVENTORY_SERVICE_MSG_ORDER_DELETE_TYPE)) {
                    if(MA_OK == (err = order_delete_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client order delete request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_INVENTORY_SERVICE_MSG_ORDER_UPDATE_STATUS_TYPE)) {
                    if(MA_OK == (err = order_update_status_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client order update request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_INVENTORY_SERVICE_MSG_INVENTORY_DELETE_TYPE)) {
                    if(MA_OK == (err = inventory_delete_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client order update request failed, last error(%d)", err);
                        }
                    }
                } else {
                    if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, MA_ERROR_SCHEDULER_INVALID_OP_TYPE, response))) {
                        MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client request failed, last error(%d)", err);
                    }
                }
            } else {
                if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err = MA_ERROR_SCHEDULER_TASK_TYPE_NOT_SET, response))) {
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client request failed, last error(%d)", err);
                }
            }
            if(MA_OK != ma_message_release(response)) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client request message release failed");
            }
        }
        return err;
    }
    return MA_OK;
}

static ma_error_t ma_inventory_service_task_update_status(ma_inventory_service_t *inventory, const char *task_id, ma_task_exec_status_t status) {
    if(inventory && task_id) {
        ma_context_t *context = inventory->context;
        ma_task_t *task = NULL ;
        ma_error_t err = MA_OK ;
        ma_scheduler_t *scheduler = MA_CONTEXT_GET_SCHEDULER(context) ;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(context);

        MA_LOG(logger, MA_LOG_SEV_DEBUG, "Inventory task id(%s) status update handle begin", task_id);
        if(MA_OK == (err = ma_scheduler_get_task_handle(scheduler, task_id, &task))) {
            if(MA_OK == (err = ma_task_set_execution_status(task, status))) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "Inventory service task(%s) status set to %d", task_id, status) ;
            }
            (void)ma_task_release(task);
        }
        if(MA_OK != err)
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "Inventory service task(%s) set status(%d) failed, last error(%d)", task_id, status, err) ;

        MA_LOG(logger, MA_LOG_SEV_DEBUG, "Inventory task id(%s) status update handle end", task_id);
        return err ;
    }
    return MA_ERROR_INVALIDARG ;
}

static ma_error_t update_pending_orders(ma_context_t *context, ma_inventory_info_t *info) {
   if(context && info) {
       ma_error_t err = MA_OK;
       ma_scheduler_t *scheduler = MA_CONTEXT_GET_SCHEDULER(context);
       ma_task_t **task_array = NULL;
       size_t task_array_size = 0;
       char inventory_id[MA_MAX_LEN+1] = {0};
       size_t count = 0;

       //(void)ma_inventory_info_get_email_id(info, inventory_id);
       (void)ma_inventory_info_get_contact(info, inventory_id);
       if(MA_OK == (err = ma_scheduler_enumerate_task_server(scheduler, MA_SOFTWAREID_GENERAL_STR, MA_BOOKING_SERVICE_SECTION_NAME_STR, MA_BOOKING_TASK_TYPE, &task_array, &task_array_size))) {
           int i = 0;
           for(i = 0; i < task_array_size; ++i) {
               ma_variant_t *booking_payload = NULL;

               if(MA_OK == (err = ma_task_get_app_payload(task_array[i], &booking_payload))) {
                   ma_booking_info_t *book_info = NULL;

                   if(MA_OK == (err = ma_booking_info_convert_from_variant(booking_payload, &book_info))) {
                       char picker_id[MA_MAX_LEN+1] = {0};

                       (void)ma_booking_info_get_picker_email_id(book_info, picker_id);
                       if(!strcmp(picker_id, inventory_id)) {
                           ma_booking_info_status_t status = MA_BOOKING_INFO_STATUS_NOT_ASSIGNED;

                           (void)ma_booking_info_get_status(book_info, &status);
                           if(MA_BOOKING_INFO_STATUS_NOT_ASSIGNED != status) ++count;
                       }
                       (void)ma_booking_info_release(book_info);
                   }
                   (void)ma_variant_release(booking_payload);
               }
           }

           (void)ma_scheduler_release_task_set(task_array);
       }

       (void)ma_inventory_info_set_order_pending_count(info, count);
       return err;
   }
   return MA_ERROR_INVALIDARG;
}
static void inventory_for_each(size_t index, ma_variant_t *value, void *cb_data, ma_bool_t *stop_loop) {
    if(index && value && cb_data && stop_loop) {
        ma_buffer_t *buf = NULL;
        ma_inventory_service_t *service = (ma_inventory_service_t*)cb_data;
        ma_error_t err = MA_OK;

        if(MA_OK == (err = ma_variant_get_string_buffer(value, &buf))) {
            const char *inventory_id = NULL;
            size_t len = 0;

            if(MA_OK == (err = ma_buffer_get_string(buf, &inventory_id, &len))) {
                ma_profile_info_t *pinfo = NULL;

                if(MA_OK == (err = ma_profiler_get(MA_CONTEXT_GET_PROFILER(service->context), inventory_id, &pinfo))) {
                    ma_profile_info_user_type_t usr_type = MA_PROFILE_INFO_USER_TYPE_NORMAL;
                    char pincode[MA_MAX_LEN+1] = {0};

                    (void)ma_profile_info_get_user_type(pinfo, &usr_type);
                    (void)ma_profile_info_set_source_pincode(pinfo, pincode);
                    if(MA_PROFILE_INFO_USER_TYPE_OPERATION == usr_type) {
                        ma_inventory_info_t *iinfo = NULL;
                        ma_task_time_t today = {0};
                        char today_str[MA_MAX_LEN+1] = {0};

                        MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "Inventory id(%s) found", inventory_id);
                        get_localtime(&today);
                        today.hour = today.minute = today.secs = 0;
                        ma_strtime(today, today_str);
                        if(MA_OK == (err = ma_inventory_get(MA_CONTEXT_GET_INVENTORY(service->context), inventory_id, today_str, &iinfo))) {
                            (void)ma_inventory_info_release(iinfo);
                        } else {
                            if(MA_OK == (err = ma_inventory_info_create(&iinfo))) {
                                (void)ma_inventory_info_set_email_id(iinfo, inventory_id);
                                (void)ma_inventory_info_set_date(iinfo, today);
                                (void)ma_inventory_info_set_work_status(iinfo, MA_INVENTORY_INFO_WORK_STATUS_PRESENT);
                                (void)ma_inventory_info_set_pincode(iinfo, pincode);
                                (void)update_pending_orders(service->context, iinfo);
                                if(MA_OK == (err = ma_inventory_add(MA_CONTEXT_GET_INVENTORY(service->context), iinfo))) {
                                    char subject[MA_MAX_LEN+1] = {0};
                                    char body[MA_MAX_BUFFER_LEN+1] = {0};

                                    MA_MSC_SELECT(_snprintf, snprintf)(subject, MA_MAX_LEN, "Update work status(%s) for date(%s)", inventory_id, today_str);
                                    MA_MSC_SELECT(_snprintf, snprintf)(body, MA_MAX_BUFFER_LEN,"Dear(%s),\n\nPlease update work status.\n\nRegards,\ncs@mileaccess.com", inventory_id);
                                    if(MA_OK == (err = ma_mail_send(MA_EMAIL_ID, inventory_id, subject, body)))
                                        MA_LOG(service->logger, MA_LOG_SEV_TRACE, "Inventory id(%s) work status check mail sent", inventory_id);
                                    else
                                        MA_LOG(service->logger, MA_LOG_SEV_ERROR, "Inventory id(%s) work status check mail send failed, last error(%d)", inventory_id, err);
                                    MA_LOG(service->logger, MA_LOG_SEV_TRACE, "Inventory id(%s) Date(%s) record added", inventory_id, today_str);
                                } else
                                    MA_LOG(service->logger, MA_LOG_SEV_ERROR, "Inventory id(%s) Date(%s) record add failed, last error(%d)", inventory_id, today_str, err);
                                (void)ma_inventory_info_release(iinfo);
                            }
                        } 
                    }
                    (void)ma_profile_info_release(pinfo);
                }
            }
            (void)ma_buffer_release(buf);
        }
    }
}

static ma_error_t add_recorder_entry(ma_inventory_service_t *service) {
    if(service) {
        ma_error_t err = MA_OK;
        ma_task_time_t today = {0};
        char today_str[MA_MAX_LEN+1]= {0};
        ma_recorder_info_t *info = NULL;

        get_localtime(&today);
        today.hour = today.minute = today.secs = 0;
        ma_strtime(today, today_str);
        if(MA_OK == (err = ma_recorder_info_create(&info))) {
            (void)ma_recorder_info_set_date(info, today_str);

            if(MA_OK == (err = ma_recorder_add(MA_CONTEXT_GET_RECORDER(service->context), info))) {
                MA_LOG(service->logger, MA_LOG_SEV_TRACE, "Added recorder entry(%s)", today_str);
            } else
                MA_LOG(service->logger, MA_LOG_SEV_ERROR, "Recorder entry(%s) add failed, last error(%d)", today_str, err);
            (void)ma_recorder_info_release(info);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t enumerate_operation_query_work_status(ma_inventory_service_t *service) {
    if(service) {
        ma_error_t err = MA_OK;
        ma_variant_t *var = NULL;

        if(MA_OK == (err = ma_inventory_enumerate(service->inventory, &var))) {
            ma_array_t *array = NULL;

            if(MA_OK == (err = ma_variant_get_array(var, &array))) {
                ma_array_foreach(array, &inventory_for_each, service);
                (void)ma_array_release(array);
            }
            (void)ma_variant_release(var);
        }
        /* add new entry record in recorder */
        if(MA_OK != (err = add_recorder_entry(service))) {
            MA_LOG(service->logger, MA_LOG_SEV_ERROR, "Recording failed, last error(%d)", err);
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t inventory_subscriber_cb(const char *topic, ma_message_t *message, void *cb_data) {
    if(topic && message && cb_data) {
        ma_error_t err = MA_OK;
        ma_inventory_service_t *service = (ma_inventory_service_t*)cb_data;
        //ma_inventory_t *inventory = service->inventory;

        MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "inventory subscriber received task message(%s)", topic);
        if(!strcmp(topic, MA_TASK_EXECUTE_PUBSUB_TOPIC) || !strcmp(topic, MA_TASK_STOP_PUBSUB_TOPIC) || !strcmp(MA_TASK_REMOVE_PUBSUB_TOPIC, topic) || !strcmp(topic, MA_TASK_UPDATE_STATUS_PUBSUB_TOPIC)) {
            const char *task_type = NULL , *task_owner_id = NULL , *task_id = NULL;

            MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "inventory processing (%s) message", topic);
            if( (MA_OK == (err = ma_message_get_property(message, MA_TASK_TYPE, &task_type))) && task_type &&
                (MA_OK == (err = ma_message_get_property(message, MA_TASK_PRODUCT_ID, &task_owner_id))) && task_owner_id &&
                (MA_OK == (err = ma_message_get_property(message, MA_TASK_ID, &task_id))) && task_id) {
                if(!strcmp(task_type, MA_INVENTORY_TASK_TYPE)) {
                    MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "inventory task id(%s) processing request received", task_id);
                    (void)enumerate_operation_query_work_status(service);
                    if(MA_OK == (err = ma_inventory_service_task_update_status(service, task_id, MA_TASK_EXEC_SUCCESS)))
                        MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "task id(%s) status updated successfully", task_id);
                    else
                        MA_LOG(service->logger, MA_LOG_SEV_ERROR, "task id(%s) status update failed, last error(%d)", task_id, err);
                } else
                    MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "task id(%s) is not inventory task", task_id);
            }
        } else {
            /* do nothing */
        }


        return err;
    }
    return MA_ERROR_INVALIDARG;
}


