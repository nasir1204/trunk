#include "ma/internal/services/comments/ma_comment_service.h"
#include "ma/internal/utils/context/ma_context.h"
#include "ma/internal/utils/comment/ma_comment.h"
#include "ma/internal/utils/comment/ma_comment_info.h"
#include "ma/internal/defs/ma_comment_defs.h"
#include "ma/internal/services/scheduler/ma_task_defs.h"
#include "ma/internal/services/scheduler/ma_task_internal.h"
#include "ma/internal/services/ma_service.h"
#include "ma/ma_log.h"
#include "ma/ma_message.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/ma_variant.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/sensor/ma_sensor_msg.h"
#include "ma/sensor/ma_sensor_utils.h"
#include "ma/ma_message.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/services/scheduler/ma_scheduler_core.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MA_COMMENT_TASK_REPEAT_INTERVAL 30 /* minutes */
#define MA_COMMENT_TASK_REPEAT_DURATION 24 /* hours */
#define MA_COMMENT_TASK_MAX_RUN_TIME_LIMIT 2 /* minutes */
#define MA_COMMENT_TASK_START_DELAY  2 /* start */

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "comment service"

const char base_path[] = "/comment/";

MA_CPP(extern "C" {)
MA_CPP(})

struct ma_comment_service_s {
    ma_service_t            service_base;
    ma_context_t            *context;
    ma_msgbus_server_t      *server;
    ma_logger_t             *logger;
    ma_msgbus_subscriber_t  *subscriber;
    ma_comment_t           *comment;
    ma_msgbus_t             *msgbus;
};


static ma_error_t comment_subscriber_cb(const char *topic, ma_message_t *message, void *cb_data);
static ma_error_t ma_comment_service_cb(ma_msgbus_server_t *server, ma_message_t *msg, void *cb_data, ma_msgbus_client_request_t *c_request);

static ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint);
static ma_error_t service_start(ma_service_t *service);
static ma_error_t service_stop(ma_service_t *service);
static void service_release(ma_service_t *service);

static const ma_service_methods_t service_methods = {
    &service_configure,
    &service_start,
    &service_stop,
    &service_release
};

ma_error_t ma_comment_service_create(const char *service_name, ma_service_t **service) {
    if(service_name && service) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;

        if((*service = (ma_service_t*)calloc(1, sizeof(ma_comment_service_t)))) {
            ma_service_init(*service, &service_methods, service_name, service);
            return MA_OK;
        }
        return err ;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_comment_service_release(ma_comment_service_t *self) {
    if(self) {
        ma_error_t err = MA_OK;
        if(self->server && (MA_OK != (err = ma_msgbus_server_release(self->server)))) {
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "msgbus server release failed");
        }
        (void)ma_msgbus_subscriber_release(self->subscriber);
        (void)ma_comment_release(self->comment);
        free(self) ;
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_bool_t check_context_validity(ma_context_t *context){	
    /*Will see if we need this*/
    return MA_TRUE;
}

ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint) {
    if(service && context) {
        ma_error_t rc = MA_OK;
        if(MA_SERVICE_CONFIG_NEW == hint && check_context_validity(context)) {
            ma_configurator_t *configurator = MA_CONTEXT_GET_CONFIGURATOR(context);
            ma_comment_service_t *self = (ma_comment_service_t *)service;
            ma_msgbus_t *msgbus = MA_CONTEXT_GET_MSGBUS(context);

            self->context = context;
            self->logger = MA_CONTEXT_GET_LOGGER(context);
            self->msgbus = msgbus;

            (void)ma_comment_set_logger(self->logger);
            if(MA_OK == (rc = ma_comment_create(msgbus, ma_configurator_get_comment_datastore(configurator), MA_COMMENT_BASE_PATH_STR, &self->comment))) {
                MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "comment configured successfully");
                if(MA_OK == (rc = ma_msgbus_subscriber_create(MA_CONTEXT_GET_MSGBUS(context), &self->subscriber))) {
                    (void)ma_msgbus_subscriber_setopt(self->subscriber, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                    (void)ma_msgbus_subscriber_setopt(self->subscriber, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
                    if(MA_OK == (rc = ma_msgbus_server_create(msgbus, MA_COMMENT_SERVICE_NAME_STR, &self->server))) {                        
                        (void)ma_msgbus_server_setopt(self->server, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
                        if(MA_OK == (rc = ma_msgbus_server_setopt(self->server, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC))) {
                            /*TODO - No need to add the comment object in the context if we can use the comment client API's */
                            ma_context_add_object_info(context, MA_OBJECT_COMMENT_NAME_STR, (void *const *)&self->comment);
                            MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_DEBUG, "comment service configuration successful");
                            return MA_OK;
                        }
                        ma_msgbus_server_release(self->server);
                    }
                    ma_msgbus_subscriber_release(self->subscriber);
                }
            }
            else
                MA_LOG(self->logger, MA_LOG_SEV_ERROR, "comment creation failed, last error(%d)", rc);
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t service_start(ma_service_t *service) {
    if(service) {
        ma_error_t err = MA_OK;
        ma_comment_service_t *self = (ma_comment_service_t *)service ;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);
        
        if(MA_OK == (err = ma_comment_start(self->comment))) {
            if(MA_OK == (err = ma_msgbus_server_start(((ma_comment_service_t*)service)->server, ma_comment_service_cb, service))) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "comment service started successfully");
                if(MA_OK == (err = ma_msgbus_subscriber_register(((ma_comment_service_t*)service)->subscriber,  "ma.sensor.*", comment_subscriber_cb, service))) {
                    MA_LOG(((ma_comment_service_t*)service)->logger, MA_LOG_SEV_DEBUG, "comment subscriber registered successfully");
                    return MA_OK ;
                } 
                else 
                    MA_LOG(((ma_comment_service_t*)service)->logger, MA_LOG_SEV_ERROR, "comment service failed to subscribe topic, errocode(%d)", err);
                ma_msgbus_server_stop(((ma_comment_service_t*)service)->server);
            }
            else
                MA_LOG(logger, MA_LOG_SEV_CRITICAL, "comment service start failed, last error(%d)", err);
            (void)ma_comment_stop(self->comment);
        }
        else
            MA_LOG(logger, MA_LOG_SEV_CRITICAL, "comment service start failed, last error(%d)", err);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_comment_service_get_msgbus(ma_comment_service_t *service, ma_msgbus_t **msgbus) {
    if(service && msgbus) {
        *msgbus = service->msgbus;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_comment_service_get_version(ma_uint32_t *version) {
    if(version) {
        *version = 0x05020100; //TODO fetch version 
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_comment_service_start(ma_comment_service_t *service) {
    return service_start((ma_service_t*)service);
}

ma_error_t ma_comment_service_stop(ma_comment_service_t *service) {
    return service_stop((ma_service_t*)service);
}

ma_error_t service_stop(ma_service_t *service) {
    if(service) {
        ma_error_t err = MA_OK;
        ma_comment_service_t *self = (ma_comment_service_t *)service;

        if(MA_OK != (err = ma_msgbus_subscriber_unregister(((ma_comment_service_t*)service)->subscriber))) {
            MA_LOG(((ma_comment_service_t*)service)->logger, MA_LOG_SEV_ERROR, "comment service unregistering subscriber failed, last error(%d)", err);
        }

        if(MA_OK == (err = ma_msgbus_server_stop(self->server))) {
            if(MA_OK != (err = ma_comment_stop(self->comment))) 
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_CRITICAL, "comment stop failed, last error(%d)", err);
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO, "comment service stopped successfully");
        }
        else
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_CRITICAL, "comment service stop failed, last error(%d)", err);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

void service_release(ma_service_t *service) {
    if(service) {
        ma_comment_service_release((ma_comment_service_t *)service) ;
        return ;
    }
    return ;
}

static ma_error_t ma_comment_service_task_add(ma_comment_service_t *self, const char *id, ma_variant_t *payload) {
    if(self && id) {
         ma_error_t err = MA_OK;
         ma_task_t *task = NULL;
         ma_trigger_t *trigger = NULL;
         ma_trigger_daily_policy_t policy = {1};
         ma_scheduler_t *scheduler = MA_CONTEXT_GET_SCHEDULER(self->context);

         if(MA_OK == (err = ma_task_create(id, &task))) {
             ma_task_repeat_policy_t repeat = {{0}};

             repeat.repeat_interval.hour = 0;
             repeat.repeat_interval.minute = MA_COMMENT_TASK_REPEAT_INTERVAL;
             repeat.repeat_until.hour = MA_COMMENT_TASK_REPEAT_DURATION;
             repeat.repeat_until.minute = 0;
             (void)ma_task_set_software_id(task, MA_SOFTWAREID_GENERAL_STR);
             (void)ma_task_set_creator_id(task, MA_COMMENT_SERVICE_SECTION_NAME_STR);
             (void)ma_task_set_type(task, MA_COMMENT_TASK_TYPE);
             (void)ma_task_set_name(task, id);
             if(!payload)
                  MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Comment task id(%s) payload is NULL", id);
             (void)ma_task_set_app_payload(task, payload); 
             (void)ma_task_set_repeat_policy(task, repeat);
             (void)ma_task_set_max_run_time_limit(task, MA_COMMENT_TASK_MAX_RUN_TIME_LIMIT);
             if(MA_OK == (err = ma_trigger_create_daily(&policy, &trigger))) {
                 ma_task_time_t begin_date = {0};

                 advance_current_date_by_seconds(MA_COMMENT_TASK_START_DELAY, &begin_date);
                 (void)ma_trigger_set_begin_date(trigger, &begin_date);
                 if(MA_OK == (err = ma_task_add_trigger(task, trigger))) {
                     if(MA_OK == (err = ma_scheduler_add(scheduler, task))) {
                         MA_LOG(self->logger, MA_LOG_SEV_INFO, "Comment task id(%s) added successfully into scheduler", id);
                     } else
                         MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Comment task id(%s) added successfully into scheduler failed, last error(%d)", id, err);
                 }
                 (void)ma_trigger_release(trigger);
             }
             (void)ma_task_release(task);
         }
         return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_comment_service_task_update(ma_comment_service_t *self, const char *id, ma_variant_t *payload) {
    if(self && id) {
         ma_error_t err = MA_OK;
         ma_task_t *task = NULL;
         ma_trigger_t *trigger = NULL;
         ma_scheduler_t *scheduler = MA_CONTEXT_GET_SCHEDULER(self->context);

         if(MA_OK == (err = ma_scheduler_get_task_handle(scheduler, id, &task))) {
             size_t size = 0, i = 0;
             ma_trigger_list_t *tl = NULL;

             (void)ma_task_size(task, &size);
             (void)ma_task_get_trigger_list(task, &tl);
             (void)ma_task_set_app_payload(task, payload);
             for(i = 0; i < size; ++i) {
                 if(MA_OK == (err = ma_trigger_list_at(tl, i, &trigger))) {
                     ma_task_time_t begin_date = {0};

                     advance_current_date_by_seconds(MA_COMMENT_TASK_START_DELAY, &begin_date);
                     (void)ma_trigger_set_begin_date(trigger, &begin_date);
                     (void)ma_trigger_release(trigger);
                 }
             }
             (void)ma_task_release(task);
             if(MA_OK == (err = ma_scheduler_modify_task(scheduler, task))) {
                 MA_LOG(self->logger, MA_LOG_SEV_INFO, "Comment task id(%s) modified successfully into scheduler", id);
             } else
                 MA_LOG(self->logger, MA_LOG_SEV_ERROR, "Comment task id(%s) modified into scheduler failed, last error(%d)", id, err);
         }
         return err;
    }
    return MA_ERROR_INVALIDARG;
}
static ma_error_t ma_comment_service_task_update_status(ma_comment_service_t *self, const char *task_id, ma_task_exec_status_t status) {
    if(self && task_id) {
        ma_context_t *context = self->context;
        ma_task_t *task = NULL ;
        ma_error_t err = MA_OK ;
        ma_scheduler_t *scheduler = MA_CONTEXT_GET_SCHEDULER(context) ;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(context);

        MA_LOG(logger, MA_LOG_SEV_DEBUG, "Comment task id(%s) status update handle begin", task_id);
        if(MA_OK == (err = ma_scheduler_get_task_handle(scheduler, task_id, &task))) {
            if(MA_OK == (err = ma_task_set_execution_status(task, status))) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "Comment service task(%s) status set to %d", task_id, status) ;
            }
            (void)ma_task_release(task);
        }
        if(MA_OK != err)
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "Comment service task(%s) set status(%d) failed, last error(%d)", task_id, status, err) ;

        MA_LOG(logger, MA_LOG_SEV_DEBUG, "Comment task id(%s) status update handle end", task_id);
        return err ;
    }
    return MA_ERROR_INVALIDARG ;
}

static ma_error_t ma_comment_service_task_start(ma_comment_service_t *service, const char *task_id) {
    if(service && task_id) {
        ma_context_t *context = service->context;
        ma_task_t *task = NULL ;
        ma_error_t err = MA_OK ;
        ma_scheduler_t *scheduler = MA_CONTEXT_GET_SCHEDULER(context) ;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(context);

        if(MA_OK == (err = ma_scheduler_get_task_handle(scheduler, task_id, &task))) {
            if(MA_OK == (err = ma_task_start(task))) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "Comment service task(%s) started", task_id) ;
            }
            (void)ma_task_release(task);
        }
        if(MA_OK != err)
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "Comment service task(%s) start failed(%d)", task_id, err) ;

        return err ;
    }
    return MA_ERROR_INVALIDARG ;
}

static ma_error_t ma_comment_service_task_stop(ma_comment_service_t *service, const char *task_id) {
    if(service && task_id) {
        ma_context_t *context = service->context;
        ma_task_t *task = NULL ;
        ma_error_t err = MA_OK ;
        ma_scheduler_t *scheduler = MA_CONTEXT_GET_SCHEDULER(context) ;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(context);

        if(MA_OK == (err = ma_scheduler_get_task_handle(scheduler, task_id, &task))) {
            if(MA_OK == (err = ma_task_stop(task))) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "Comment service task(%s) stopped", task_id) ;
            }
            (void)ma_task_release(task);
        }
        if(MA_OK != err)
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "Comment service task(%s) stop failed(%d)", task_id, err) ;

        return err ;
    }
    return MA_ERROR_INVALIDARG ;
}

static ma_error_t ma_comment_service_task_remove(ma_comment_service_t *self, const char *task_id) {
    if(self && task_id) {
        ma_error_t err = MA_OK;
        ma_scheduler_t *scheduler = MA_CONTEXT_GET_SCHEDULER(self->context) ;
        ma_task_t *task = NULL ;

        if(MA_OK == (err = ma_scheduler_get_task_handle(scheduler, task_id, &task))) {
            (void)ma_task_release(task);
            if(MA_OK == (err = ma_scheduler_remove(scheduler, task_id))) {
                MA_LOG(self->logger, MA_LOG_SEV_INFO, "Comment task removed from scheduler successfully");
            }
        }
        if(MA_OK != err)
            MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "Comment service task remove failed(%d)", err) ;

        return err;
    }
    return MA_ERROR_INVALIDARG;
}
static ma_error_t comment_register_handler(ma_comment_service_t *service, ma_message_t *comment_req_msg, ma_message_t *comment_rsp_msg) {
    if(service && comment_req_msg && comment_rsp_msg) {
        ma_error_t err = MA_OK;
        const char *comment_id = NULL;
        char *comment_reg_status = MA_TASK_REPLY_STATUS_FAILED;
        ma_comment_info_t *info = NULL;
        ma_variant_t *comment_variant = NULL;
        char error_code[MA_MAX_LEN] = {0};


        (void)ma_message_set_property(comment_rsp_msg, MA_COMMENT_ID, comment_id);
        if(MA_OK == (err = ma_message_get_property(comment_req_msg, MA_COMMENT_ID, &comment_id))) {
                comment_reg_status = MA_COMMENT_ID_NOT_EXIST;
                if(MA_OK == (err = ma_comment_get(service->comment, comment_id, &info))) {
                    comment_reg_status = MA_COMMENT_ID_EXIST;
                    MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "comment id(%s) already exist!", comment_id);
                    (void)ma_comment_info_release(info);
                } else {
                   if(MA_OK == ma_message_get_payload(comment_req_msg, &comment_variant)) {
                       if(MA_OK == (err = ma_comment_add_variant(service->comment, comment_id, comment_variant))) {
                           MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "comment id(%s) written into DB successfully", comment_id);
                           comment_reg_status = MA_COMMENT_ID_WRITTEN;
                           if(MA_OK == (err = ma_comment_service_task_add(service, comment_id, comment_variant))) {
                               MA_LOG(service->logger, MA_LOG_SEV_TRACE, "comment id(%s) task add successful", comment_id);
                               err = ma_comment_service_task_start(service, comment_id);
                           } else
                               MA_LOG(service->logger, MA_LOG_SEV_ERROR, "comment id(%s) task add failed, last error(%d)", comment_id, err);
                       }
                       (void)ma_variant_release(comment_variant);
                   }
                }
        }
        if(MA_OK != err)
            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_CRITICAL, "comment id register failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(comment_rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(comment_rsp_msg, MA_COMMENT_STATUS, comment_reg_status);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t comment_update_handler(ma_comment_service_t *service, ma_message_t *comment_req_msg, ma_message_t *comment_rsp_msg) {
    if(service && comment_req_msg && comment_rsp_msg) {
        ma_error_t err = MA_OK;
        const char *comment_id = NULL;
        char *comment_reg_status = MA_TASK_REPLY_STATUS_FAILED;
        ma_variant_t *comment_variant = NULL;
        char error_code[MA_MAX_LEN] = {0};


        (void)ma_message_set_property(comment_rsp_msg, MA_COMMENT_ID, comment_id);
        if(MA_OK == (err = ma_message_get_property(comment_req_msg, MA_COMMENT_ID, &comment_id))) {
            comment_reg_status = MA_COMMENT_ID_NOT_EXIST;
            if(MA_OK == ma_message_get_payload(comment_req_msg, &comment_variant)) {
                if(MA_OK == (err = ma_comment_add_variant(service->comment, comment_id, comment_variant))) {
                    MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "comment id(%s) updated into DB successfully", comment_id);
                    comment_reg_status = MA_COMMENT_ID_WRITTEN;
                }
                (void)ma_variant_release(comment_variant);
            }
        }
        if(MA_OK != err)
            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_CRITICAL, "comment id update failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(comment_rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(comment_rsp_msg, MA_COMMENT_STATUS, comment_reg_status);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t comment_deregister_handler(ma_comment_service_t *service, ma_message_t *comment_req_msg, ma_message_t *comment_rsp_msg) {
    if(service && comment_req_msg && comment_rsp_msg) {
        ma_error_t err = MA_OK;
        const char *comment_id = NULL;
        char *comment_reg_status = MA_COMMENT_ID_NOT_EXIST;
        ma_comment_info_t *info = NULL;
        char error_code[MA_MAX_LEN] = {0};


        (void)ma_message_set_property(comment_rsp_msg, MA_COMMENT_ID, comment_id);
        if(MA_OK == (err = ma_message_get_property(comment_req_msg, MA_COMMENT_ID, &comment_id))) {
            if(MA_OK == (err = ma_comment_get(service->comment, comment_id, &info))) {
                comment_reg_status = MA_COMMENT_ID_EXIST;
                if(MA_OK == (err = ma_comment_delete(service->comment, comment_id))) {
                    comment_reg_status = MA_COMMENT_ID_DELETED;
                    MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "comment id(%s) deleted successfully", comment_id);
                }
                (void)ma_comment_info_release(info);
            }
        }
        if(MA_OK != err)
            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_CRITICAL, "comment id deregister failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(comment_rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(comment_rsp_msg, MA_COMMENT_STATUS, comment_reg_status);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t comment_get_handler(ma_comment_service_t *service, ma_message_t *comment_req_msg, ma_message_t *comment_rsp_msg) {
    if(service && comment_req_msg && comment_rsp_msg) {
        ma_error_t err = MA_OK;
        const char *comment_id = NULL;
        char *comment_reg_status = MA_COMMENT_ID_NOT_EXIST;
        ma_comment_info_t *info = NULL;
        ma_variant_t *comment_variant = NULL;
        char error_code[MA_MAX_LEN] = {0};


        (void)ma_message_set_property(comment_rsp_msg, MA_COMMENT_ID, comment_id);
        if(MA_OK == (err = ma_message_get_property(comment_req_msg, MA_COMMENT_ID, &comment_id))) {
            if(MA_OK == (err = ma_comment_get(service->comment, comment_id, &info))) {
                comment_reg_status = MA_COMMENT_ID_EXIST;
                MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "comment id(%s) get successfully", comment_id);
                if(MA_OK == (err = ma_comment_info_convert_to_variant(info, &comment_variant))) {
                    (void)ma_message_set_payload(comment_rsp_msg, comment_variant);
                    (void)ma_variant_release(comment_variant);
                }
                (void)ma_comment_info_release(info);
            }
        }
        if(MA_OK != err)
            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_CRITICAL, "comment id get failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(comment_rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(comment_rsp_msg, MA_COMMENT_STATUS, comment_reg_status);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_comment_service_cb(ma_msgbus_server_t *server, ma_message_t *msg, void *cb_data, ma_msgbus_client_request_t *c_request) {
    if(server && msg && cb_data) {
        ma_error_t err = MA_OK;
        const char *msg_type = NULL;
        ma_message_t *response = NULL;
        ma_comment_service_t *service = (ma_comment_service_t*)cb_data;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(service->context);

        if(MA_OK == (err = ma_message_create(&response))) {
            if(MA_OK == (err = ma_message_get_property(msg, MA_COMMENT_SERVICE_MSG_TYPE, &msg_type))) {
                if(!strcmp(msg_type, MA_COMMENT_SERVICE_MSG_REGISTER_TYPE)) {
                    if(MA_OK == (err = comment_register_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client register id request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_COMMENT_SERVICE_MSG_UNREGISTER_TYPE)) {
                    if(MA_OK == (err = comment_deregister_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client deregister request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_COMMENT_SERVICE_MSG_UPDATE_TYPE)) {
                    if(MA_OK == (err = comment_update_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client update request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_COMMENT_SERVICE_MSG_GET_TYPE)) {
                    if(MA_OK == (err = comment_get_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client deregister request failed, last error(%d)", err);
                        }
                    }
                } else {
                    if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, MA_ERROR_SCHEDULER_INVALID_OP_TYPE, response))) {
                        MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client request failed, last error(%d)", err);
                    }
                }
            } else {
                if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err = MA_ERROR_SCHEDULER_TASK_TYPE_NOT_SET, response))) {
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client request failed, last error(%d)", err);
                }
            }
            if(MA_OK != ma_message_release(response)) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client request message release failed");
            }
        }
        return err;
    }
    return MA_OK;
}

ma_error_t comment_subscriber_cb(const char *topic, ma_message_t *msg, void *cb_data) {
    if(topic && msg && cb_data) {
        ma_error_t err = MA_OK;
        ma_comment_service_t *service = (ma_comment_service_t*)cb_data;
        //ma_comment_t *comment = service->comment;

        MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "comment subscriber received task message(%s)", topic);
        if(!strcmp(topic, MA_TASK_EXECUTE_PUBSUB_TOPIC)) {
            const char *task_type = NULL , *task_owner_id = NULL , *task_id = NULL;

            MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "comment processing (%s) message", topic);
            if( (MA_OK == (err = ma_message_get_property(msg, MA_TASK_TYPE, &task_type))) && task_type &&
                (MA_OK == (err = ma_message_get_property(msg, MA_TASK_PRODUCT_ID, &task_owner_id))) && task_owner_id &&
                (MA_OK == (err = ma_message_get_property(msg, MA_TASK_ID, &task_id))) && task_id) {
                if(!strcmp(task_type, MA_COMMENT_TASK_TYPE)) {
                    MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "comment task id(%s) processing request received", task_id);
                } else
                    MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "task id(%s) is not comment task", task_id);
            }
        } else if(!strcmp(topic, MA_TASK_UPDATE_STATUS_PUBSUB_TOPIC)) {
            const char *task_type = NULL , *task_owner_id = NULL , *task_id = NULL;

            MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "comment processing (%s) message", topic);
            if( (MA_OK == (err = ma_message_get_property(msg, MA_TASK_TYPE, &task_type))) && task_type &&
                (MA_OK == (err = ma_message_get_property(msg, MA_TASK_PRODUCT_ID, &task_owner_id))) && task_owner_id &&
                (MA_OK == (err = ma_message_get_property(msg, MA_TASK_ID, &task_id))) && task_id) {
                if(!strcmp(task_type, MA_COMMENT_TASK_TYPE)) {
                    MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "comment task id(%s) processing update request ", task_id);
                    err = ma_comment_service_task_update_status(service, task_id, MA_TASK_EXEC_SUCCESS);
                } else
                    MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "task id(%s) is not comment task", task_id);
            }
        } else if(!strcmp(topic, MA_TASK_STOP_PUBSUB_TOPIC)) {
            const char *task_type = NULL , *task_owner_id = NULL , *task_id = NULL;

            MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "comment processing (%s) message", topic);
            if( (MA_OK == (err = ma_message_get_property(msg, MA_TASK_TYPE, &task_type))) && task_type &&
                (MA_OK == (err = ma_message_get_property(msg, MA_TASK_PRODUCT_ID, &task_owner_id))) && task_owner_id &&
                (MA_OK == (err = ma_message_get_property(msg, MA_TASK_ID, &task_id))) && task_id) {
                if(!strcmp(task_type, MA_COMMENT_TASK_TYPE)) {
                    MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "comment task id(%s) processing stop request ", task_id);
                } else
                    MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "task id(%s) is not comment task", task_id);
            }
        } else if(!strcmp(topic, MA_TASK_REMOVE_PUBSUB_TOPIC)) {
            const char *task_type = NULL , *task_owner_id = NULL , *task_id = NULL;

            MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "comment processing (%s) message", topic);
            if( (MA_OK == (err = ma_message_get_property(msg, MA_TASK_TYPE, &task_type))) && task_type &&
                (MA_OK == (err = ma_message_get_property(msg, MA_TASK_PRODUCT_ID, &task_owner_id))) && task_owner_id &&
                (MA_OK == (err = ma_message_get_property(msg, MA_TASK_ID, &task_id))) && task_id) {
                if(!strcmp(task_type, MA_COMMENT_TASK_TYPE)) {
                    MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "comment task id(%s) processing remove request ", task_id);
                } else
                    MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "task id(%s) is not comment task", task_id);
            }
        } else {
            /* do nothing */
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_comment_service_get_comment(ma_comment_service_t *service, ma_comment_t **comment) {
    if(service && comment) {
        *comment = service->comment;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


