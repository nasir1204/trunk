#ifndef MA_SPIPE_DEMUX_H_INCLUDED
#define MA_SPIPE_DEMUX_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>

#include "ma/internal/utils/spipe/ma_spipe_package.h"
#include "ma/internal/utils/macrypto/ma_crypto.h"
#include "ma/internal/services/ahclient/ma_spipe_handler.h"
#include "ma/internal/utils/datastructures/ma_slist.h"

MA_CPP(extern "C" {)

typedef struct ma_spipe_handler_node_s ma_spipe_handler_node_t;
struct ma_spipe_handler_node_s{
	ma_spipe_handler_t *handler;
	MA_SLIST_NODE_DEFINE(ma_spipe_handler_node_t);
};

typedef struct ma_spipe_demux_s{
	MA_SLIST_DEFINE(hlist,ma_spipe_handler_node_t);
}ma_spipe_demux_t;

void ma_spipe_demux_init(ma_spipe_demux_t *self);

void ma_spipe_demux_deinit(ma_spipe_demux_t *self);

ma_error_t ma_spipe_demux_add_spipe_handler(ma_spipe_demux_t *self, ma_spipe_handler_t *shandler);

void ma_spipe_demux_remove_spipe_handler(ma_spipe_demux_t *self, ma_spipe_handler_t *shandler);

ma_error_t ma_spipe_demux_deserialize_spipe_package(ma_spipe_demux_t *self, ma_crypto_t *crypto, ma_logger_t *logger, ma_stream_t *stream, ma_spipe_package_t **spipepkg);

ma_error_t ma_spipe_demux_start(ma_spipe_demux_t *self, ma_spipe_response_t *response);

MA_CPP(})

#endif //MA_SPIPE_DEMUX_H_INCLUDED

