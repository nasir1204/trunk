#include "ma_ah_relay_discoverer.h"
#include "ma/internal/clients/relay/ma_relay_discovery_request.h"

#include "ma/ma_log.h"
#include "ma/ma_msgbus.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/services/ma_agent_policy_settings_bag.h" 

#include "ma/internal/defs/ma_udp_defs.h"

#include <stdlib.h>
#include <stdio.h>

struct ma_ah_relay_discoverer_s{
	ma_context_t				*context;

	ma_udp_client_t				 *udp_client;

	ma_logger_t					 *logger;

	ma_relay_discovery_request_t *relay_discovery_req;

	relay_discovery_final_cb_t   cb;

	void						*user_data;
};

static void relay_discovery_cb(ma_relay_discovery_request_t *req, ma_relay_list_t *relay_list, void *user_data){
}

static void relay_discovery_final_cb(ma_relay_discovery_request_t *req, void *user_data){
	ma_ah_relay_discoverer_t *self = (ma_ah_relay_discoverer_t*)user_data;
	self->cb(self, self->user_data);
}

#ifdef MA_WINDOWS
ma_error_t ma_ah_relay_discoverer_create(ma_context_t *context, ma_ah_relay_discoverer_t **self){
	if(context && self){
		ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(context);
		ma_msgbus_t *msgbus = MA_CONTEXT_GET_MSGBUS(context);		

		ma_ah_relay_discoverer_t *tmp = (ma_ah_relay_discoverer_t*) calloc(1, sizeof(ma_ah_relay_discoverer_t));

		if(tmp){
			if(MA_OK == ma_udp_client_create(&tmp->udp_client)){
				(void)ma_udp_client_set_logger(tmp->udp_client, logger);
				(void)ma_udp_client_set_event_loop(tmp->udp_client, ma_msgbus_get_event_loop(msgbus));			
				(void)ma_udp_client_set_loop_worker(tmp->udp_client, ma_msgbus_get_loop_worker(msgbus));

				if(MA_OK == ma_relay_discovery_request_create(tmp->udp_client, &tmp->relay_discovery_req)){				
					ma_relay_discovery_callbacks_t cbs;
					cbs.relay_discovery_cb = relay_discovery_cb;
					cbs.relay_discovery_final_cb = relay_discovery_final_cb;				

					(void)ma_relay_discovery_request_set_logger(tmp->relay_discovery_req, logger);
					(void)ma_relay_discovery_request_set_callbacks(tmp->relay_discovery_req, &cbs, tmp);
					tmp->context = context;
					tmp->logger = logger;
					*self = tmp;
					return MA_OK;
				}			
			}
			free(tmp);
		}
		return MA_ERROR_OUTOFMEMORY;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_ah_relay_discoverer_start(ma_ah_relay_discoverer_t *self, relay_discovery_final_cb_t cb, void *data){
	if(self && cb){
		ma_error_t rc = MA_OK;
		ma_relay_discovery_policy_t policy;		
		ma_int32_t port = 0;
		ma_policy_settings_bag_t *pso_bag = MA_CONTEXT_GET_POLICY_SETTINGS_BAG(self->context) ;
		ma_policy_settings_bag_get_int(pso_bag, MA_UDP_SERVICE_SECTION_NAME_STR, MA_UDP_KEY_BROADCAST_PORT_INT, &port) ;
		policy.discovery_port = port;
		ma_policy_settings_bag_get_str(pso_bag, MA_UDP_SERVICE_SECTION_NAME_STR, MA_UDP_KEY_MULTICAST_ADDRESS_STR, &policy.discovery_multicast_addr) ;
		self->cb = cb;
		self->user_data = data;

		if(MA_OK == (rc = ma_relay_discovery_request_set_policy(self->relay_discovery_req, &policy))){
			ma_relay_discovery_settings_t settings;
			settings.max_no_of_relays_to_discover = 5;
			settings.discovery_timeout_ms = 2 * 1000;
			settings.sync_discovery = MA_FALSE;
			ma_relay_discovery_request_reset(self->relay_discovery_req);
			rc = ma_relay_discovery_request_perform(self->relay_discovery_req, &settings);
		}
		return rc;
	}	
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_ah_relay_discoverer_stop(ma_ah_relay_discoverer_t *self){
	if(self){
		ma_relay_discovery_request_cancel(self->relay_discovery_req);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_ah_relay_discoverer_get_relay_count(ma_ah_relay_discoverer_t *self, ma_uint16_t *count){
	if(self && count){
		ma_relay_list_t *r_list = NULL;
		*count = 0;
		if(MA_OK == ma_relay_discovery_request_get_relays(self->relay_discovery_req, &r_list)){			
			(void)ma_relay_list_get_count(r_list, count);			
			ma_relay_list_release(r_list);			
		}
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_ah_relay_discoverer_get_relays(ma_ah_relay_discoverer_t *self, ma_proxy_list_t **proxies){
	if(self && proxies){
		ma_relay_list_t *r_list = NULL;
		ma_error_t rc = MA_ERROR_RELAY_NONE;
		if(MA_OK == ma_relay_discovery_request_get_relays(self->relay_discovery_req, &r_list)){
			ma_uint16_t count = 0;
			(void)ma_relay_list_get_count(r_list, &count);
			if(count){
				ma_proxy_list_t *p_list = NULL;
				if(MA_OK == ma_proxy_list_create(&p_list)){
					int i = 1;
					for(i; i <= count; i++){
						ma_relay_t relay;
						if(MA_OK == ma_relay_list_get_relay(r_list, i, &relay)){
							ma_proxy_t *proxy = NULL;
							if(MA_OK == ma_proxy_create(&proxy)){
								ma_proxy_set_address(proxy, relay.relay_addr);
								ma_proxy_set_scopeid(proxy, relay.nic_scope_id);
								ma_proxy_set_port(proxy, relay.relay_port);								
								rc = MA_OK;
							}
						}
					}

					if(MA_OK == rc)
						*proxies = p_list;
					else
						ma_proxy_list_release(p_list);
				}
			}
			ma_relay_list_release(r_list);
		}
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_ah_relay_discoverer_release(ma_ah_relay_discoverer_t *self){
	if(self){
		(void)ma_relay_discovery_request_release(self->relay_discovery_req);
		(void)ma_udp_client_release(self->udp_client);
		free(self);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

#else
ma_error_t ma_ah_relay_discoverer_create(ma_context_t *context, ma_ah_relay_discoverer_t **self){return MA_OK;}

ma_error_t ma_ah_relay_discoverer_start(ma_ah_relay_discoverer_t *self, relay_discovery_final_cb_t cb, void *data){return MA_OK;}
ma_error_t ma_ah_relay_discoverer_stop(ma_ah_relay_discoverer_t *self){return MA_OK;}
ma_error_t ma_ah_relay_discoverer_reset(ma_ah_relay_discoverer_t *self){return MA_OK;}

ma_error_t ma_ah_relay_discoverer_get_relay_count(ma_ah_relay_discoverer_t *self, ma_uint16_t *count){return MA_OK;}

ma_error_t ma_ah_relay_discoverer_get_next_relay(ma_ah_relay_discoverer_t *self, ma_proxy_list_t **proxies){return MA_OK;}

ma_error_t ma_ah_relay_discoverer_get_relays(ma_ah_relay_discoverer_t *self, ma_proxy_list_t **proxies){return MA_OK;}

ma_error_t ma_ah_relay_discoverer_release(ma_ah_relay_discoverer_t *self){return MA_OK;}
#endif
