#include "ma/ma_common.h"
#include "ma_ah_site_url_provider.h"
#include "ma/internal/utils/network/ma_url_request.h"
#include "ma/internal/services/ahclient/ma_ah_sitelist.h"

MA_CPP(extern "C" {)

typedef struct ma_spipe_url_request_s ma_spipe_url_request_t;
typedef void (*on_spipe_response)(ma_error_t status, int response_code , void *userdata, ma_spipe_url_request_t *spipereq);
typedef ma_error_t (*on_spipe_site_connect)(void *userdata, ma_spipe_url_request_t *spipereq);

struct ma_spipe_url_request_s {
	uv_loop_t *loop ;
	uv_timer_t final_timer;
	ma_url_request_io_t io_streams;
	on_spipe_response final_cb;
	on_spipe_site_connect site_connect_cb;
	ma_error_t spipe_prepare_error;
	int response_code;
	ma_error_t error;
	void *user_data;
};

static MA_INLINE void ma_spipe_url_request_init(ma_spipe_url_request_t *self, ma_event_loop_t *ev_loop){
	if(self && ev_loop){
		ma_event_loop_get_loop_impl(ev_loop, &self->loop);
		uv_timer_init(self->loop, &self->final_timer);
	}
}

static MA_INLINE void ma_spipe_url_request_deinit(ma_spipe_url_request_t *self){
	(void)ma_stream_release(self->io_streams.read_stream);
	(void)ma_stream_release(self->io_streams.write_stream);
	self->io_streams.read_stream = NULL;
	self->io_streams.write_stream = NULL;
}

static MA_INLINE ma_error_t ma_spipe_url_request_cancel(ma_spipe_url_request_t *self){
	uv_timer_stop(&self->final_timer);
	return MA_OK;
}

static MA_INLINE void ma_spipe_url_request_set_upstream(ma_spipe_url_request_t *self, ma_stream_t *upstream){
	(void)ma_stream_release(self->io_streams.read_stream);		
	self->io_streams.read_stream = upstream;	
}

static MA_INLINE void ma_spipe_url_request_set_downstream(ma_spipe_url_request_t *self, ma_stream_t *downstream){
	(void)ma_stream_release(self->io_streams.write_stream);		
	self->io_streams.write_stream = downstream;			
}

static MA_INLINE void ma_spipe_url_request_set_connect_cb(ma_spipe_url_request_t *self, on_spipe_site_connect cb){
	self->site_connect_cb = cb;	
}

static MA_INLINE void ma_spipe_url_request_set_final_cb(ma_spipe_url_request_t *self, on_spipe_response cb){
	self->final_cb = cb;	
}

static void transfer_done(uv_timer_t *handle, int status){
	ma_spipe_url_request_t *self = (ma_spipe_url_request_t*)handle->data;
	printf("\n\n data transfer completed. \n\n");
	uv_timer_stop(&self->final_timer);
	self->final_cb(self->error, self->response_code, self->user_data, self);
}

static ma_error_t ma_spipe_url_request_send(ma_spipe_url_request_t *self, void *userdata){
	if(self && userdata){
		self->user_data = userdata;
		uv_timer_start(&self->final_timer, transfer_done, 10000, 10000);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}
