#include "ma_ah_site_common.h"

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "ahclient"

/* ma_ah_site APIs */
ma_error_t ma_ah_site_create(ma_ah_site_t **ah_site) {
    if(ah_site) {
        ma_ah_site_t *self = (ma_ah_site_t *)calloc(1, sizeof(ma_ah_site_t));
        if(self) {            
            *ah_site = self;
            return MA_OK;
        }
        return MA_ERROR_OUTOFMEMORY;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_ah_site_release(ma_ah_site_t *self) {
    if(self) {
        if(self->name) free(self->name);    
        if(self->version) free(self->version);
        if(self->fqdn_name) free(self->fqdn_name);
        if(self->ip_address) free(self->ip_address);
		if(self->short_name) free(self->short_name);
        free(self);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

const char *ma_ah_site_get_name(ma_ah_site_t *self) {
    return (self) ? self->name : NULL;
}

const char *ma_ah_site_get_version(ma_ah_site_t *self) {
    return (self) ? self->version : NULL;
}

const char *ma_ah_site_get_fqdn_name(ma_ah_site_t *self) {
    return (self) ? self->fqdn_name : NULL;
}

const char *ma_ah_site_get_short_name(ma_ah_site_t *self) {
    return (self) ? self->short_name : NULL;
}

const char *ma_ah_site_get_ip_address(ma_ah_site_t *self) {
    return (self) ? self->ip_address : NULL;
}

ma_uint32_t ma_ah_site_get_port(ma_ah_site_t *self) {
    return (self) ? self->port : -1;
}

ma_uint32_t ma_ah_site_get_ssl_port(ma_ah_site_t *self) {
    return (self) ? self->ssl_port : -1;
}

ma_uint32_t ma_ah_site_get_order(ma_ah_site_t *self) {
    return (self) ? self->site_order : -1;
}

ma_error_t ma_ah_site_set_name(ma_ah_site_t *self, const char *name) {
    if(self && name) {
        self->name = strdup(name);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_ah_site_set_version(ma_ah_site_t *self, const char *version) {
    if(self && version) {
        self->version = strdup(version);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_ah_site_set_fqdn_name(ma_ah_site_t *self, const char *fqdn_name) {
    if(self && fqdn_name) {
        self->fqdn_name = strdup(fqdn_name);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_ah_site_set_short_name(ma_ah_site_t *self, const char *short_name) {
    if(self && short_name) {
        self->short_name = strdup(short_name);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_ah_site_set_ip_address(ma_ah_site_t *self, const char *ip_address) {
    if(self && ip_address) {
        self->ip_address = strdup(ip_address);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_ah_site_set_port(ma_ah_site_t *self, ma_uint32_t port) {
    if(self) {
        self->port = port;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_ah_site_set_ssl_port(ma_ah_site_t *self, ma_uint32_t ssl_port) {
    if(self) {
        self->ssl_port = ssl_port;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_ah_site_set_order(ma_ah_site_t *self, ma_uint32_t order) {
    if(self) {
        self->site_order = order;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}
