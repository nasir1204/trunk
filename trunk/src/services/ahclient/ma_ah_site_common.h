#ifndef MA_AH_SITE_COMMON_H_INCLUDED
#define MA_AH_SITE_COMMON_H_INCLUDED

#include "ma/internal/services/ahclient/ma_ah_sitelist.h"
#include "ma/internal/utils/datastructures/ma_vector.h"

#include <stdlib.h>
#include <string.h>

struct ma_ah_site_s {
    /* Name of sitelist */
    char                            *name;

    /* version of sitelist i.e, ePO version*/
    char                            *version;

    /* Server FQDN name */
    char                            *fqdn_name;

    /* Server IP Address */
    char                            *ip_address;

    /* Server short name */
    char                            *short_name;

    /* port */
    ma_uint32_t                     port;

    /* secure port */
    ma_uint32_t                     ssl_port;

    /* Order of Site */
    ma_uint32_t                     site_order;
};

struct ma_ah_sitelist_s {
    /* ca certificates file*/
    char                *ca_file;

    /* sitelist local version */
    char                *local_version;

    /* sitelist global version */
    char                *global_version;

    /* dynamic array of site objects */
    ma_vector_define(site_vec, ma_ah_site_t*);
};


#endif /* MA_AH_SITE_COMMON_H_INCLUDED */

