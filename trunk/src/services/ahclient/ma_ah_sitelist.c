#include "ma_ah_site_common.h"
#include "ma/internal/utils/platform/ma_sys_apis.h"

/* MAX_SITES macro is used to create the array of sites in first time
   The array is extended dynamically in ma_vector*/

#define MAX_SITES   8

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "ahclient"

ma_error_t ma_ah_sitelist_create(ma_ah_sitelist_t **ah_sitelist) {
    if(ah_sitelist) {
        ma_ah_sitelist_t *self = (ma_ah_sitelist_t *)calloc(1, sizeof(ma_ah_sitelist_t));
        if(self) {
            ma_vector_init(self, MAX_SITES, site_vec, ma_ah_site_t*);            
            *ah_sitelist = self;
            return MA_OK;
        }
        return MA_ERROR_OUTOFMEMORY;
    }
    return MA_ERROR_INVALIDARG;
}

const char *ma_ah_sitelist_get_ca_file(ma_ah_sitelist_t *self) {
    return (self) ? self->ca_file : NULL;  
}

const char *ma_ah_sitelist_get_local_version(ma_ah_sitelist_t *self) {
    return (self) ? self->local_version : NULL;
}

const char *ma_ah_sitelist_get_global_version(ma_ah_sitelist_t *self) {
    return (self) ? self->global_version : NULL;
}

ma_error_t ma_ah_sitelist_set_ca_file(ma_ah_sitelist_t *self, const char *ca_file) {
    if(self && ca_file) {
        self->ca_file = strdup(ca_file);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_ah_sitelist_set_local_version(ma_ah_sitelist_t *self, const char *local_version) {
    if(self && local_version) {
        self->local_version = strdup(local_version);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_ah_sitelist_set_global_version(ma_ah_sitelist_t *self, const char *global_version) {
    if(self && global_version) {
        self->global_version = strdup(global_version);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_ah_sitelist_add_site(ma_ah_sitelist_t *self, ma_ah_site_t *ah_site) {
    if(self && ma_vector_get_ptr(self, site_vec) && ah_site) {
        ma_vector_push_back(self, ah_site, ma_ah_site_t*, site_vec);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

/* compare function for elements in ascending order */
static int site_order_compare(const void *s1, const void *s2) {
    ma_ah_site_t **site1 = (ma_ah_site_t **)s1;
    ma_ah_site_t **site2 = (ma_ah_site_t **)s2;        
    return ((*site1)->site_order - (*site2)->site_order);
}

/*  adjust site order in sitelist
    The sites are in groups like this:
        FIRST   SECOND
        1       <site A>
        1       <site B>

        2       <site C>
        2       <site D>
        2       <site E>
                    
        3       <site F>

    We want all the 1 groups to be shuffled together, then the 2 groups,
    etc. We do this in a simple way. Make a new order number with the group
    number (the first) in the top word (ex. 0x00010000) and a random
    number in the bottom word (ex. 0x00002607), for example:

        FIRST   SECOND      SORT ORDER
        1       <site B>    0x1E33A
        1       <site A>    0x12607
                    *
        2       <site E>    0x200AF
        2       <site D>    0x26BB1
        2       <site C>    0x2C902
                    
        3       <site F>    0x352D7 
*/
ma_error_t ma_ah_sitelist_adjust_site_order(ma_ah_sitelist_t *self) {      
    ma_uint32_t count = 0;
    ma_error_t rc = MA_OK;
    for(count = 0; count < ma_vector_get_size(self, site_vec); count++) {   
	    ma_uint32_t new_site_order = 0 ;
        ma_uint32_t rand_num = ma_sys_rand();
        ma_ah_site_t *site = ma_vector_at(self, count, ma_ah_site_t*, site_vec);
        if(!site) {
            rc = MA_ERROR_AH_INTERNAL;
            break;
        }
        new_site_order = (( site->site_order << 16) | (rand_num & 0xffff)) & 0xffffffff;
        site->site_order = new_site_order;        
    }

    if((0 != count) && (MA_OK == rc))
        ma_vector_sort(self, ma_ah_site_t*, site_vec, site_order_compare);

    return rc;
}

ma_error_t ma_ah_sitelist_remove_site(ma_ah_sitelist_t *self, ma_ah_site_t *ah_site) {
    if(self && ah_site) {
        ma_vector_remove(self, ah_site, ma_ah_site_t*, site_vec, ma_ah_site_release);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_uint32_t  ma_ah_sitelist_get_count(ma_ah_sitelist_t *self) {
    return ma_vector_get_size(self, site_vec);
}

ma_ah_site_t *ma_ah_sitelist_get_site(ma_uint32_t index, ma_ah_sitelist_t *self) {
    return (self) ? ma_vector_at(self, index, ma_ah_site_t, site_vec) : NULL;
}

ma_error_t ma_ah_sitelist_for_each(ma_ah_sitelist_t *self, for_each_callback cb, void *cb_data) {
    if(self && cb) {
        ma_error_t  rc = MA_OK;
        ma_uint32_t i = 0;
        for(i = 0; i < ma_ah_sitelist_get_count(self); i++ ) {
             if(MA_OK != (rc = cb(ma_ah_sitelist_get_site(i, self), cb_data)))
                return rc;
        }    
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_ah_sitelist_clear(ma_ah_sitelist_t *self) {
    if(self) { 
        ma_vector_clear(self, ma_ah_site_t*, site_vec, ma_ah_site_release);
        if(self->ca_file) { free(self->ca_file); self->ca_file = NULL; }
        if(self->global_version) { free(self->global_version); self->global_version = NULL; }
        if(self->local_version) { free(self->local_version); self->local_version = NULL; }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

//static ma_error_t assign_callback(ma_ah_site_t *ah_site, void *cb_data) {
//    ma_ah_sitelist_t *self = (ma_ah_sitelist_t*)cb_data;
//    
//    if(self) {
//        ma_error_t rc = MA_OK;
//        ma_ah_site_t *site = NULL;
//
//        if(MA_OK == (rc = ma_ah_site_create(&site))) {
//            ma_bool_t b_rc = MA_OK == ma_ah_site_set_name(site, ah_site->name) &&
//                             MA_OK == ma_ah_site_set_version(site, ah_site->version) &&
//                             MA_OK == ma_ah_site_set_fqdn_name(site, ah_site->fqdn_name) &&
//                             MA_OK == ma_ah_site_set_short_name(site, ah_site->short_name) &&
//                             MA_OK == ma_ah_site_set_ip_address(site, ah_site->ip_address) &&
//                             MA_OK == ma_ah_site_set_port(site, ah_site->port) &&
//                             MA_OK == ma_ah_site_set_ssl_port(site, ah_site->ssl_port) &&
//                             MA_OK == ma_ah_site_set_order(site, ah_site->site_order);
//            
//            if(b_rc)
//                rc = ma_ah_sitelist_add_site(self, site);
//            else
//                rc = MA_ERROR_AH_INTERNAL;
//
//        }
//        return rc;
//    }
//    return MA_ERROR_INVALIDARG;
//}

//ma_error_t ma_ah_sitelist_assign(ma_ah_sitelist_t *self, ma_ah_sitelist_t *source) {
//     if(self && source) {  
//        ma_error_t rc = MA_OK;                
//        if(MA_OK == (rc = ma_ah_sitelist_clear(self))) {            
//            if(source->ca_file) 
//                self->ca_file = strdup(source->ca_file);
//            
//            if(source->global_version)
//                self->global_version = strdup(source->global_version);
//
//            if(source->local_version)
//                self->local_version = strdup(source->local_version);
//            
//            ma_vector_init(self, MAX_SITES, site_vec, ma_ah_site_t*);
//
//            rc = ma_ah_sitelist_for_each(source, assign_callback, self);            
//        }
//        return rc;
//    }
//    return MA_ERROR_INVALIDARG;
//}

ma_error_t ma_ah_sitelist_release(ma_ah_sitelist_t *self) {
    if(self) {  
        ma_error_t rc = ma_ah_sitelist_clear(self);
        free(self); self= NULL;
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

