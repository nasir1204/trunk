#include "ma/internal/services/ahclient/ma_ah_client_service.h"
#include "ma_spipe_key_manager.h"
#include "ma/internal/defs/ma_io_defs.h"

#include <stdlib.h>
#include <string.h>

#include "ma/ma_msgbus.h"
#include "ma/ma_message.h"

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "cryptoservice"


struct ma_spipe_key_manager_s {
	ma_context_t                *context;
    ma_spipe_decorator_t        decorator_impl;
    ma_spipe_handler_t          handler_impl;    
    ma_spipe_priority_t         priority;
};

#ifndef AH_UNIT_TEST
static ma_error_t decorate_key_package(ma_spipe_decorator_t *decorator, ma_spipe_priority_t priority, ma_spipe_package_t *spipe_pkg);
static ma_error_t key_handler(ma_spipe_handler_t *handler, const ma_spipe_response_t *spipe_response);
#endif

/* Key Decorator methods */
ma_error_t decorate_key_package(ma_spipe_decorator_t *decorator, ma_spipe_priority_t priority, ma_spipe_package_t *spipe_pkg) {
    if(decorator && spipe_pkg) {
        ma_spipe_key_manager_t *self = (ma_spipe_key_manager_t *)decorator->data;
        ma_error_t rc = MA_OK;
		/* Check the type of the package if it is agent public key package then decorate the agent public key otherwise send server public key hash */    
		/* Server Key Hash */
        ma_bytebuffer_t *server_public_key_hash_buffer = NULL;
		if(MA_OK == (rc = ma_crypto_get_server_public_key_hash(MA_CONTEXT_GET_CRYPTO(self->context), &server_public_key_hash_buffer))) {                     
            if(MA_OK == (rc = ma_spipe_package_add_info(spipe_pkg, STR_PKGTYPE_SERVER_KEY_HASH, ma_bytebuffer_get_bytes(server_public_key_hash_buffer), ma_bytebuffer_get_size(server_public_key_hash_buffer)))) {
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Successfully added server public key Hash");
            }
            else
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to add server public key Hash to spipe package"); 
        }
        else
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to get server public key hash from crypto");

        if(MA_OK == rc) {
        	unsigned char *value=NULL;
		    size_t size=0;
            rc = ma_spipe_package_get_info(spipe_pkg, STR_PACKAGE_TYPE, &value, &size);            

            if((MA_OK == rc) && value && !strncmp((char*)value, STR_PKGTYPE_AGENT_PUB_KEY, strlen(STR_PKGTYPE_AGENT_PUB_KEY))) {
                ma_bytebuffer_t *agent_pub_key = NULL;
                if(MA_OK == (rc = ma_crypto_get_agent_public_key(MA_CONTEXT_GET_CRYPTO(self->context), &agent_pub_key))) {
                    /* Overwrite the SPIPE package type from default type to key . so that other decorators can ignore this spipe pkg in this turn*/
                    if(MA_OK == (rc = ma_spipe_package_set_package_type(spipe_pkg, MA_SPIPE_PACKAGE_TYPE_KEY))) {                    
                        /* Agent public key */
                        if(MA_OK == (rc = ma_spipe_package_add_data(spipe_pkg, STR_AGENT_PUB_KEY_FILE, ma_bytebuffer_get_bytes(agent_pub_key), ma_bytebuffer_get_size(agent_pub_key)))) {
                            /* Reset the priority after attaching the public key ans server pub key hash to the spipe package */
                            self->priority = MA_SPIPE_PRIORITY_NONE;
                            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Successfully added agent public key");                           
                        }
                        else
                            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to add agent public key to spipe package"); 
                    }
                    else
                        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to set spipe package type");
                }
                else
                    MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to get agent pub key from crypto");        
            }            
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t get_spipe_priority(ma_spipe_decorator_t *decorator, ma_spipe_priority_t *priority) {
    if(decorator && priority) {
		ma_spipe_key_manager_t *self = (ma_spipe_key_manager_t *)decorator->data;
		if(self) {
			*priority = self->priority;
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_TRACE, "Priority set for key decorator <%d>", *priority);
		}
		else
			*priority = MA_SPIPE_PRIORITY_NONE;

        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t key_decorator_destroy(ma_spipe_decorator_t *decorator) {
    ma_spipe_key_manager_t *self = (ma_spipe_key_manager_t *)decorator->data;
    if(self && 0 == self->handler_impl.ref_count) {
        free(self);        
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_spipe_decorator_methods_t key_decorator_methods = {    
    &decorate_key_package,
    &get_spipe_priority,
    &key_decorator_destroy
};

/* Key Handler Methods */
static ma_error_t key_update_async_cb(ma_error_t status, ma_message_t *response, void *cb_data, ma_msgbus_request_t *request) {
    ma_msgbus_endpoint_t *io_service_ep = NULL;
	ma_spipe_key_manager_t *self = (ma_spipe_key_manager_t*)cb_data;

    MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "response status for key update request = <%d>", status);

    if(status != MA_OK)
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to process the key update request by io service, status is <%d>", status);

	if((status == MA_OK) && response) ma_message_release(response);
    if(MA_OK == ma_msgbus_request_get_endpoint(request, &io_service_ep)) {        
        (void)ma_msgbus_request_release(request);
        (void)ma_msgbus_endpoint_release(io_service_ep);
    }
    return MA_OK;
}

static ma_error_t keyupdate_handle(ma_spipe_key_manager_t *self, const ma_spipe_response_t *spipe_response) {
    ma_error_t rc = MA_OK;
    unsigned char *server_pub_key = NULL, *server_sign_sec_key = NULL, *agent_fips_mode = NULL;
    size_t  server_pub_key_len = 0, server_sign_sec_key_len = 0, agent_fips_mode_len = 0;
    
    if(MA_OK == (rc = ma_spipe_package_get_data(spipe_response->respond_spipe_pkg, STR_SERVER_2048_PUB_KEY_FILE, (void **)&server_pub_key, &server_pub_key_len)) &&
       MA_OK == (rc = ma_spipe_package_get_data(spipe_response->respond_spipe_pkg, STR_SERVER_REQ_2048_SEC_KEY_FILE, (void **)&server_sign_sec_key, &server_sign_sec_key_len)) &&
       MA_OK == (rc = ma_spipe_package_get_data(spipe_response->respond_spipe_pkg, STR_AGENT_FIPS_MODE_FLAG_FILE, (void **)&agent_fips_mode, &agent_fips_mode_len))
       ) {
        /* sending key update message to i/o service to take the appropriate action i.e, restart the process or only speified services */                 
        ma_msgbus_endpoint_t *io_service_ep = NULL;
		if(MA_OK == (rc = ma_msgbus_endpoint_create(MA_CONTEXT_GET_MSGBUS(self->context), MA_IO_SERVICE_NAME_STR, NULL, &io_service_ep))) {
            /* Attache server keys and fips mode to this messsage - Array of raw type variants */                     
            ma_array_t *new_array = NULL;      
            (void)ma_msgbus_endpoint_setopt(io_service_ep, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
			(void)ma_msgbus_endpoint_setopt(io_service_ep, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_INPROC);						
				
            
            if(MA_OK == (rc = ma_array_create(&new_array))) {
                ma_variant_t *tmp_variant = NULL, *variant_array = NULL;
                if(MA_OK == (rc = ma_variant_create_from_raw(server_pub_key , server_pub_key_len , &tmp_variant))){
                    (void)ma_array_push(new_array, tmp_variant);
                    (void)ma_variant_release(tmp_variant); tmp_variant = NULL;
                }
                if(MA_OK == (rc = ma_variant_create_from_raw(server_sign_sec_key , server_sign_sec_key_len , &tmp_variant))) {
                    (void)ma_array_push(new_array, tmp_variant);
                    (void)ma_variant_release(tmp_variant); tmp_variant = NULL;
                }
                if(MA_OK == (rc = ma_variant_create_from_raw(agent_fips_mode , agent_fips_mode_len , &tmp_variant))) {
                    (void)ma_array_push(new_array, tmp_variant);
                    (void)ma_variant_release(tmp_variant); tmp_variant = NULL;
                }

                if(MA_OK == rc && (MA_OK == (rc = ma_variant_create_from_array(new_array, &variant_array)))) {
                    ma_message_t *key_update_msg = NULL;                            
                    ma_msgbus_request_t *request = NULL;
                    if(MA_OK == (rc = ma_message_create(&key_update_msg))) {
                        ma_message_set_property(key_update_msg , MA_PROP_KEY_REQUEST_TYPE_STR , MA_PROP_VALUE_KEY_UPDATE);
                        if(MA_OK == (rc = ma_message_set_payload(key_update_msg, variant_array))) {
                            if(MA_OK != (rc = ma_msgbus_async_send(io_service_ep, key_update_msg, key_update_async_cb, self, &request))){   
                                MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to send the key update msg to io service rc = <%d>", rc);
                            }
                        }
                        else
                            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to set the payload to message rc = <%d>", rc);
                        
                        (void)ma_message_release(key_update_msg);                        
                    }
                    else
                        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to create the message rc = <%d>", rc);                        
                    (void)ma_variant_release(variant_array); variant_array = NULL;
                }
                else
                    MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to construct the array of variants rc = <%d>", rc);
                (void)ma_array_release(new_array);
            }
            else
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to create variant array rc = <%d>", rc);
            
            if(MA_OK != rc)
                (void)ma_msgbus_endpoint_release(io_service_ep);
        }        
        else
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to create endpoint for io service rc = <%d>", rc); 
    }
    else
        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to get keyupdate data from spipe package");

    return rc;
}

ma_error_t key_handler(ma_spipe_handler_t *handler, const ma_spipe_response_t *spipe_response) {
    if(handler && spipe_response) {
        ma_error_t rc = MA_OK;
        ma_spipe_key_manager_t *self = (ma_spipe_key_manager_t*)handler->data;
        if(spipe_response->http_response_code == 200 || spipe_response->http_response_code == 403) {
            /* It will handle the response messages if package type is either "RequestPublicKey" or "KeyPackage"*/             
            unsigned char *value=NULL;
		    size_t size=0;
            rc = ma_spipe_package_get_info(spipe_response->respond_spipe_pkg, STR_PACKAGE_TYPE, &value, &size);            
            
            if((MA_OK == rc) && value) {
                /* STR_PKGTYPE_REQUEST_PUB_KEY */
                if(!strncmp((char*)value, STR_PKGTYPE_REQUEST_PUB_KEY, strlen(STR_PKGTYPE_REQUEST_PUB_KEY))){
                    MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Received spipe package type <%s>", STR_PKGTYPE_REQUEST_PUB_KEY);
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "Agent received REQUEST PUBLIC KEY package from ePO server");
                    /* The priority should be set when key handler receives "RequestPublicKey" message */
                    self->priority = MA_SPIPE_PRIORITY_IMMEDIATE;
                    /* Raise an alert to AH client service to send spipe package with Agent Pub key */
					rc = ma_ah_client_service_raise_spipe_alert(MA_CONTEXT_GET_AH_CLIENT(self->context), STR_PKGTYPE_AGENT_PUB_KEY);
                }
                /* STR_PKGTYPE_KEY_PACKAGE */
                else if(!strncmp((char*)value, STR_PKGTYPE_KEY_PACKAGE, strlen(STR_PKGTYPE_KEY_PACKAGE))){
                    MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Received spipe package type <%s>", STR_PKGTYPE_KEY_PACKAGE);
                    rc = keyupdate_handle(self, spipe_response);
                }
                else 
                    MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Received unknown spipe package type");
            }
            else
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Failed to get spipe package info");
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t key_handler_destroy(ma_spipe_handler_t *handler) {
    ma_spipe_key_manager_t *self = (ma_spipe_key_manager_t *)handler->data;
    if(self && 0 == self->decorator_impl.ref_count) {
        free(self);        
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_spipe_handler_methods_t key_handler_methods = {    
    &key_handler,
    &key_handler_destroy
};


ma_error_t ma_spipe_key_manager_create(ma_context_t *context, ma_spipe_key_manager_t **spipe_key_manager) {
     if(context && spipe_key_manager) {
        ma_spipe_key_manager_t *self = (ma_spipe_key_manager_t*)calloc(1, sizeof(ma_spipe_key_manager_t));
        if(self) {            
            ma_spipe_decorator_init((ma_spipe_decorator_t *)&self->decorator_impl, &key_decorator_methods, self);            
            ma_spipe_handler_init((ma_spipe_handler_t *)&self->handler_impl, &key_handler_methods, self);
            self->priority = MA_SPIPE_PRIORITY_NONE; /* TBD - what is the default priority for keys ??? */
			self->context = context;
            *spipe_key_manager = self;
            return MA_OK;
        }
        return MA_ERROR_OUTOFMEMORY;
    }
    return MA_ERROR_INVALIDARG;
}

ma_spipe_decorator_t *ma_spipe_key_manager_get_decorator(ma_spipe_key_manager_t *self) {
    return (self) ? &self->decorator_impl : NULL ;
}

ma_spipe_handler_t *ma_spipe_key_manager_get_handler(ma_spipe_key_manager_t *self) {
    return (self) ? &self->handler_impl : NULL ;
}

ma_error_t ma_spipe_key_manager_release(ma_spipe_key_manager_t *self) {
    /* note, the double release below matches the double ref count to both sides performed in the constructor */
    (void)ma_spipe_decorator_release(&self->decorator_impl);
    (void)ma_spipe_handler_release(&self->handler_impl);
    return MA_OK;
}
