#ifndef MA_SPIPE_URL_REQUEST_H_INCLUDED
#define MA_SPIPE_URL_REQUEST_H_INCLUDED

#include "ma/ma_common.h"
#include "ma_ah_site_url_provider.h"
#include "ma/internal/utils/network/ma_url_request.h"
#include "ma/internal/services/ahclient/ma_ah_sitelist.h"
#include "ma/internal/utils/proxy/ma_proxy_internal.h"
#include "ma/internal/defs/ma_object_defs.h"

MA_CPP(extern "C" {)

typedef struct ma_spipe_url_request_s ma_spipe_url_request_t;
typedef void (*on_spipe_response)(ma_error_t status, int response_code , void *userdata, ma_spipe_url_request_t *spipereq);
typedef ma_error_t (*on_spipe_site_connect)(void *userdata, ma_spipe_url_request_t *spipereq);

ma_error_t ma_spipe_url_request_create(ma_context_t *context, ma_spipe_url_request_t **self);

void ma_spipe_url_request_deinit(ma_spipe_url_request_t *self);

void ma_spipe_url_request_release(ma_spipe_url_request_t *self);

void ma_spipe_url_request_set_url_provider(ma_spipe_url_request_t *self, ma_ah_site_url_provider_t *spipe_site_url_provider);

ma_error_t ma_spipe_url_request_cancel(ma_spipe_url_request_t *self);

void ma_spipe_url_request_cleanup_streams(ma_spipe_url_request_t *self);

void ma_spipe_url_request_set_upstream(ma_spipe_url_request_t *self, ma_stream_t *upstream);

void ma_spipe_url_request_set_downstream(ma_spipe_url_request_t *self, ma_stream_t *downstream);

ma_bool_t ma_spipe_url_request_is_connected(ma_spipe_url_request_t *self);

ma_stream_t *ma_spipe_url_request_get_upstream(ma_spipe_url_request_t *self);

ma_stream_t *ma_spipe_url_request_get_downstream(ma_spipe_url_request_t *self);

void ma_spipe_url_request_set_connect_cb(ma_spipe_url_request_t *self, on_spipe_site_connect cb);

void ma_spipe_url_request_set_final_cb(ma_spipe_url_request_t *self, on_spipe_response cb);

void ma_spipe_url_request_set_proxy(ma_spipe_url_request_t *self, ma_proxy_list_t *proxy_list);

void ma_spipe_url_request_set_relay(ma_spipe_url_request_t *self, ma_proxy_list_t *proxy_list);

void ma_spipe_url_request_set_relay_enabled(ma_spipe_url_request_t *self, ma_bool_t relay_enabled);

ma_error_t ma_spipe_url_request_send(ma_spipe_url_request_t *self, void *userdata);

ma_error_t ma_spipe_url_request_sync_send(ma_spipe_url_request_t *self, void *userdata);

const ma_url_request_t *ma_spipe_url_request_get_url_request(ma_spipe_url_request_t *self);

void ma_spipe_url_request_set_proxy_config(ma_spipe_url_request_t *self, ma_proxy_config_t *config);

ma_bool_t ma_spipe_url_request_is_proxy_enabled(ma_spipe_url_request_t *self);

ma_bool_t ma_spipe_url_request_get_proxy_list(ma_spipe_url_request_t *self, const char *url);

MA_CPP(})

#endif
