#ifndef MA_AH_RELAY_DISCOVERER_H_INCLUDED
#define MA_AH_RELAY_DISCOVERER_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/proxy/ma_proxy.h"
#include "ma/internal/defs/ma_object_defs.h"

MA_CPP(extern "C" {)

typedef struct ma_ah_relay_discoverer_s ma_ah_relay_discoverer_t;

typedef void (*relay_discovery_final_cb_t)(ma_ah_relay_discoverer_t *self, void *data);

ma_error_t ma_ah_relay_discoverer_create(ma_context_t *context, ma_ah_relay_discoverer_t **self);

ma_error_t ma_ah_relay_discoverer_start(ma_ah_relay_discoverer_t *self, relay_discovery_final_cb_t cb, void *data);

ma_error_t ma_ah_relay_discoverer_stop(ma_ah_relay_discoverer_t *self);

ma_error_t ma_ah_relay_discoverer_get_relay_count(ma_ah_relay_discoverer_t *self, ma_uint16_t *count);

ma_error_t ma_ah_relay_discoverer_get_relays(ma_ah_relay_discoverer_t *self, ma_proxy_list_t **proxies);

ma_error_t ma_ah_relay_discoverer_release(ma_ah_relay_discoverer_t *self);

MA_CPP(})

#endif

