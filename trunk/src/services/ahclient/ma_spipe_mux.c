#include "ma_spipe_mux.h"

#include <string.h>

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "ahclient"


void ma_spipe_mux_init(ma_spipe_mux_t *self){
	MA_SLIST_INIT(self,dlist);	
}

void ma_spipe_mux_deinit(ma_spipe_mux_t *self){
	ma_spipe_decorator_node_t *node=NULL;	
	for ( node = self->dlist_head ; node != NULL ; ){
		ma_spipe_decorator_node_t *current=node;
		node=node->p_link;
		(void)ma_spipe_decorator_release(current->decorator);		
		free(current);
	}
	MA_SLIST_INIT(self,dlist);	
}

ma_error_t ma_spipe_mux_add_spipe_decorator(ma_spipe_mux_t *self, ma_spipe_decorator_t *sdecorator){
	ma_spipe_decorator_node_t *node=(ma_spipe_decorator_node_t*)calloc(1,sizeof(ma_spipe_decorator_node_t) );
	if(node){		
		node->decorator=sdecorator;
		(void)ma_spipe_decorator_add_ref(sdecorator);
		MA_SLIST_PUSH_BACK(self,dlist,node);	
		return MA_OK;
	}
	return MA_ERROR_OUTOFMEMORY;
}

void ma_spipe_mux_remove_spipe_decorator(ma_spipe_mux_t *self, ma_spipe_decorator_t *sdecorator){
	ma_spipe_decorator_node_t *current=NULL,*prev=NULL;	
	for ( prev=NULL, current = self->dlist_head ; current != NULL ; prev=current,current=current->p_link){		
		if(current->decorator == sdecorator){
			(prev) ? (prev->p_link=current->p_link) : (self->dlist_head=current->p_link);			
			(void)ma_spipe_decorator_release(current->decorator);		
			free(current);
			if(!self->dlist_head){
				MA_SLIST_INIT(self,dlist);	
			}
			break;
		}
	}				
}

ma_spipe_priority_t ma_spipe_mux_get_spipe_priority(ma_spipe_mux_t *self){
	int overall_priority=MA_SPIPE_PRIORITY_NONE;
	ma_spipe_decorator_node_t *node=NULL;		
	MA_SLIST_FOREACH(self,dlist,node){
		ma_spipe_priority_t priority=MA_SPIPE_PRIORITY_NONE;
		(void)ma_spipe_decorator_get_spipe_priority(node->decorator,&priority);										
		overall_priority |= (int)priority ;
	}
	return (ma_spipe_priority_t)overall_priority;	
}

ma_error_t check_package_validity(const char *pkg_type, ma_spipe_package_t *spipepkg){
	
	if(!strcmp(pkg_type, STR_PKGTYPE_EVENT)||
	   !strcmp(pkg_type, STR_PKGTYPE_MSG_UPLOAD)) {
		size_t size = 0;
		ma_spipe_package_get_data_element_count(spipepkg, &size);
		return size == 0 ? MA_ERROR_SPIPE_NO_DATA : MA_OK;
	}
	return MA_OK;
}

ma_error_t ma_spipe_mux_start(ma_spipe_mux_t *self, ma_spipe_priority_t priority, const char *pkg_type, ma_crypto_t *crypto, ma_logger_t *logger, ma_stream_t *stream){
	ma_error_t error=MA_OK;
	ma_spipe_package_t *spipepkg=NULL;
	if( MA_OK == (error = ma_spipe_package_create(crypto,&spipepkg) ) ) {		
		ma_spipe_decorator_node_t *node=NULL;		
		ma_spipe_package_set_logger(spipepkg, logger);
		if(MA_OK == (error = ma_spipe_package_add_info(spipepkg, STR_PACKAGE_TYPE, (unsigned char*)pkg_type, strlen(pkg_type) ) ) ){
			MA_SLIST_FOREACH(self,dlist,node){
				(void)ma_spipe_decorator_decorate(node->decorator,priority,spipepkg);								
			}
			if(MA_OK == (error = check_package_validity(pkg_type, spipepkg)))
				error=ma_spipe_package_serialize(spipepkg,stream);
		}
		(void)ma_spipe_package_release(spipepkg);				
	}
	return error;	
}

