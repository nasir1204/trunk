#include "ma_spipe_demux.h"

#define MA_SPIPE_HEADER_SIZE        234

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "ahclient"

void ma_spipe_demux_init(ma_spipe_demux_t *self){
	MA_SLIST_INIT(self,hlist);	
}

void ma_spipe_demux_deinit(ma_spipe_demux_t *self){
	ma_spipe_handler_node_t *node=NULL;	
	for ( node = self->hlist_head ; node != NULL ; ){
		ma_spipe_handler_node_t *current=node;
		node=node->p_link;
		(void)ma_spipe_handler_release(current->handler);		
		free(current);
	}
	MA_SLIST_INIT(self,hlist);	
}

ma_error_t ma_spipe_demux_add_spipe_handler(ma_spipe_demux_t *self, ma_spipe_handler_t *shandler){
	ma_spipe_handler_node_t *node=(ma_spipe_handler_node_t*)calloc(1,sizeof(ma_spipe_handler_node_t) );
	if(node){		
		node->handler=shandler;
		(void)ma_spipe_handler_add_ref(shandler);
		MA_SLIST_PUSH_BACK(self,hlist,node);		
		return MA_OK;
	}
	return MA_ERROR_OUTOFMEMORY;
}

void ma_spipe_demux_remove_spipe_handler(ma_spipe_demux_t *self, ma_spipe_handler_t *shandler){
	ma_spipe_handler_node_t *current=NULL,*prev=NULL;	
	for ( prev=NULL, current = self->hlist_head ; current != NULL ; prev=current,current=current->p_link){		
		if(current->handler == shandler){
			(prev) ? (prev->p_link=current->p_link) : (self->hlist_head=current->p_link);			
			(void)ma_spipe_handler_release(current->handler);		
			free(current);
			if(!self->hlist_head){
				MA_SLIST_INIT(self,hlist);	
			}
			break;
		}
	}							
}


ma_error_t ma_spipe_demux_deserialize_spipe_package(ma_spipe_demux_t *self, ma_crypto_t *crypto, ma_logger_t *logger, ma_stream_t *stream, ma_spipe_package_t **spipepkg){
	ma_error_t error=MA_OK;	
	size_t size=0;	
	*spipepkg=NULL;
	if( MA_OK == (error = ma_stream_get_size(stream,&size) ) && size > MA_SPIPE_HEADER_SIZE) {
		if( MA_OK == (error = ma_spipe_package_create(crypto,spipepkg) ) ) {
			ma_spipe_package_set_logger(*spipepkg, logger);
			if( MA_OK != (error=ma_spipe_package_deserialize(*spipepkg,stream) ) ){				
				(void)ma_spipe_package_release(*spipepkg);
				*spipepkg=NULL;
			}
		}
	}			
	return error;	
}

ma_error_t ma_spipe_demux_start(ma_spipe_demux_t *self, ma_spipe_response_t *response){
	ma_error_t error=MA_OK;
	ma_spipe_handler_node_t *node=NULL;
	MA_SLIST_FOREACH(self,hlist,node){
		(void)ma_spipe_handler_on_spipe_response(node->handler,response);					
	}	
	return error;	
}

