#ifndef MA_SPIPE_ALERT_LIST_H_INCLUDED
#define MA_SPIPE_ALERT_LIST_H_INCLUDED

#include "ma/internal/utils/datastructures/ma_slist.h"
#include "ma/internal/utils/threading/ma_thread.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

MA_CPP(extern "C" {)

typedef struct ma_spipe_alert_node_s ma_spipe_alert_node_t;
struct ma_spipe_alert_node_s{
	char *package_type;
	MA_SLIST_NODE_DEFINE(ma_spipe_alert_node_t);
};

typedef struct ma_spipe_alert_list_s ma_spipe_alert_list_t;
struct ma_spipe_alert_list_s{
	MA_SLIST_DEFINE(alertlist, ma_spipe_alert_node_t);
};


static void ma_spipe_alert_node_free(ma_spipe_alert_node_t *node){
	if(node){
		if(node->package_type)
			free(node->package_type);
		free(node);
	}
}

static MA_INLINE void ma_spipe_alert_list_init(ma_spipe_alert_list_t *self){
	MA_SLIST_INIT(self, alertlist);
}

static MA_INLINE void ma_spipe_alert_list_deinit(ma_spipe_alert_list_t *self){
	MA_SLIST_CLEAR(self, alertlist, ma_spipe_alert_node_t, ma_spipe_alert_node_free);
}

static MA_INLINE void ma_spipe_alert_list_clear(ma_spipe_alert_list_t *self){
	MA_SLIST_CLEAR(self, alertlist, ma_spipe_alert_node_t, ma_spipe_alert_node_free);
}

static MA_INLINE ma_error_t ma_spipe_alert_list_push(ma_spipe_alert_list_t *self, const char *package_type, ma_bool_t is_front){
	ma_spipe_alert_node_t *node=(ma_spipe_alert_node_t*)calloc(1,sizeof(ma_spipe_alert_node_t) );
	if(node){
		node->package_type = strdup(package_type);
		if(is_front)
			MA_SLIST_PUSH_FRONT(self, alertlist, node);
		else
			MA_SLIST_PUSH_BACK(self, alertlist, node);
		return MA_OK;
	}
	return MA_ERROR_OUTOFMEMORY;
}

static MA_INLINE ma_spipe_alert_node_t *ma_spipe_alert_list_pop(ma_spipe_alert_list_t *self){
	ma_spipe_alert_node_t *node = NULL;
	MA_SLIST_POP_FRONT(self, alertlist, node);
	return node;
}

MA_CPP(})

#endif //MA_SPIPE_ALERT_LIST_H_INCLUDED

