#include "ma_spipe_url_request_internal.h"
#include "ma_ah_site_url_provider.h"
#include "ma/internal/services/ahclient/ma_ah_client_service.h"
#include "ma_spipe_url_request_via_proxy.h"
#include "ma_spipe_url_request_via_relay.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_message.h"
#include "ma/internal/defs/ma_io_defs.h"
#include "ma/internal/utils/network/ma_net_interface.h"

#include <string.h>
#include <stdlib.h>

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "ahclient"

extern ma_error_t ma_spipe_url_request_via_relay(ma_spipe_url_request_t *request);
static ma_error_t epo_connected_local_ip_message_publish(ma_spipe_url_request_t *url_request, char *source_ip);

ma_error_t ma_spipe_url_request_create(ma_context_t *context, ma_spipe_url_request_t **url_req){
	if(context && url_req){
		ma_spipe_url_request_t *self = (ma_spipe_url_request_t*)calloc(1, sizeof(ma_spipe_url_request_t));
		if(self){
			self->g_context = context;			
			*url_req = self;
			return MA_OK;						
		}
		return MA_ERROR_OUTOFMEMORY;
	}
	return MA_ERROR_INVALIDARG;
}

void ma_spipe_url_request_deinit(ma_spipe_url_request_t *self){
	if(self){
		self->is_connected = MA_FALSE;

		MA_URL_SSL_DEINIT( (&self->ssl_info) );
		MA_URL_SSL_INIT( (&self->ssl_info) );

		(void)ma_url_request_release(self->url_request);		
		self->url_request = NULL;

		(void)ma_stream_release(self->io_streams.read_stream);
		self->io_streams.read_stream = NULL;

		(void)ma_stream_release(self->io_streams.write_stream);
		self->io_streams.write_stream = NULL;				
	}
}

void ma_spipe_url_request_cleanup_streams(ma_spipe_url_request_t *self){
	if(self){
		(void)ma_stream_release(self->io_streams.read_stream);
		self->io_streams.read_stream = NULL;

		(void)ma_stream_release(self->io_streams.write_stream);		
		self->io_streams.write_stream = NULL;
	}
}

void ma_spipe_url_request_release(ma_spipe_url_request_t *self){
	if(self){
		ma_spipe_url_request_deinit(self);
		ma_spipe_url_request_set_proxy(self, NULL);		
		ma_spipe_url_request_set_proxy_config(self, NULL);
		free(self);
	}
}

ma_error_t ma_spipe_url_request_cancel(ma_spipe_url_request_t *self){
	if(self){
		self->is_connected = MA_FALSE;
		self->is_cancelled = MA_TRUE;
		if(self->url_request)
			return ma_url_request_cancel(self->url_request);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

void ma_spipe_url_request_set_url_provider(ma_spipe_url_request_t *self, ma_ah_site_url_provider_t *spipe_site_url_provider){
	if(self)
		self->spipe_site_url_provider = spipe_site_url_provider;	
}

const ma_url_request_t *ma_spipe_url_request_get_url_request(ma_spipe_url_request_t *self){
	return self->url_request;
}

void ma_spipe_url_request_set_proxy(ma_spipe_url_request_t *self, ma_proxy_list_t *proxy_list){
	if(self){
		self->is_connected = MA_FALSE;		
		if(self->proxy_list) ma_proxy_list_release(self->proxy_list);		
		self->proxy_list = 	proxy_list;
		if(self->url_request) ma_url_request_set_proxy_info(self->url_request, self->proxy_list);
		self->method = self->proxy_list ? MA_CONNECTION_VIA_PROXY : MA_CONNECTION_DIRECT;
	}
}

void ma_spipe_url_request_set_relay(ma_spipe_url_request_t *self, ma_proxy_list_t *proxy_list){
	if(self){
		ma_spipe_url_request_set_proxy(self, proxy_list);
		self->method = self->proxy_list ? MA_CONNECTION_VIA_RELAY : MA_CONNECTION_DIRECT;
	}
}

void ma_spipe_url_request_set_relay_enabled(ma_spipe_url_request_t *self, ma_bool_t relay_enabled){
	if(self){
		self->relay_enabled = relay_enabled;
		if(!self->relay_enabled && MA_CONNECTION_VIA_RELAY == self->method)
			ma_spipe_url_request_set_relay(self, NULL);
	}
}

void ma_spipe_url_request_set_upstream(ma_spipe_url_request_t *self, ma_stream_t *upstream){
	if(self){
		(void)ma_stream_release(self->io_streams.read_stream);		
		self->io_streams.read_stream = NULL;
		self->io_streams.read_stream = upstream;	
	}
}

void ma_spipe_url_request_set_downstream(ma_spipe_url_request_t *self, ma_stream_t *downstream){
	if(self){
		(void)ma_stream_release(self->io_streams.write_stream);		
		self->io_streams.write_stream = NULL;
		self->io_streams.write_stream = downstream;			
	}
}

ma_bool_t ma_spipe_url_request_is_connected(ma_spipe_url_request_t *self){
	return (self->url_request && self->is_connected);
}

ma_stream_t *ma_spipe_url_request_get_upstream(ma_spipe_url_request_t *self){
	return self ? self->io_streams.read_stream : NULL;
}

ma_stream_t *ma_spipe_url_request_get_downstream(ma_spipe_url_request_t *self){
	return self ? self->io_streams.write_stream : NULL;
}

void ma_spipe_url_request_set_connect_cb(ma_spipe_url_request_t *self, on_spipe_site_connect cb){
	if(self)
		self->site_connect_cb = cb;	
}

void ma_spipe_url_request_set_final_cb(ma_spipe_url_request_t *self, on_spipe_response cb){
	if(self)
		self->final_cb = cb;	
}

ma_bool_t is_connection_to_next_site_needed(ma_spipe_url_request_t *request, ma_error_t status, int response_code, ma_bool_t *is_current_url_reachable){	
	MA_LOG(MA_CONTEXT_GET_LOGGER(request->g_context), MA_LOG_SEV_INFO, "Network library rc = <%d>, Agent handler reports response code <%d>.", status, response_code);
	if(status != MA_ERROR_NETWORK_REQUEST_SUCCEEDED){
		*is_current_url_reachable = MA_FALSE;
		return MA_TRUE;	
	}
	*is_current_url_reachable = MA_TRUE;
	switch(response_code){
	case 200:
		MA_LOG(MA_CONTEXT_GET_LOGGER(request->g_context), MA_LOG_SEV_INFO, "Agent handler reports spipe package received. response code %d.", response_code);
		return MA_FALSE;
	case 201:
	case 503:
		MA_LOG(MA_CONTEXT_GET_LOGGER(request->g_context), MA_LOG_SEV_INFO, "Agent handler reports server busy. response code %d.", response_code);
		return MA_FALSE;
	case 506:			
		MA_LOG(MA_CONTEXT_GET_LOGGER(request->g_context), MA_LOG_SEV_INFO, "Agent handler reports agent guid existing in ePO. response code %d.", response_code);
		return MA_FALSE;
	case 202:
		MA_LOG(MA_CONTEXT_GET_LOGGER(request->g_context), MA_LOG_SEV_INFO, "Agent handler doesn't have anything to send. response code %d.", response_code);
		return MA_FALSE;
	case 400:
		MA_LOG(MA_CONTEXT_GET_LOGGER(request->g_context), MA_LOG_SEV_INFO, "Agent handler reports invalid request. response code %d", response_code);
		return MA_FALSE;
	case 403:
		MA_LOG(MA_CONTEXT_GET_LOGGER(request->g_context), MA_LOG_SEV_INFO, "Agent handler reports access denied, agent key update needed. response code %d", response_code);
		return MA_FALSE;
	case 404:
		MA_LOG(MA_CONTEXT_GET_LOGGER(request->g_context), MA_LOG_SEV_INFO, "Agent handler reports file not found. response code %d", response_code);
		return MA_FALSE;
	case 401:
	case 407:
		MA_LOG(MA_CONTEXT_GET_LOGGER(request->g_context), MA_LOG_SEV_INFO, "Agent handler reports authentication failures. response code %d", response_code);
		return MA_TRUE;		
	default:
		MA_LOG(MA_CONTEXT_GET_LOGGER(request->g_context), MA_LOG_SEV_INFO, "Agent handler reports unknown response code. response code %d", response_code);
		return MA_TRUE;
	}
}

static ma_error_t spipe_url_request_connect_cb(void *userdata, ma_url_request_t *net_request ){
	ma_error_t err = MA_OK;
	ma_spipe_url_request_t *request = (ma_spipe_url_request_t*)userdata;
	MA_LOG(MA_CONTEXT_GET_LOGGER(request->g_context), MA_LOG_SEV_INFO, "connection initiated  to site %s.", request->current_url);
	if(MA_OK != (err = request->site_connect_cb(request->userdata, request))){
		MA_LOG(MA_CONTEXT_GET_LOGGER(request->g_context), MA_LOG_SEV_ERROR, "Agent handler service error, terminating the spipe connection.");
		request->is_cancelled = MA_TRUE;
	}
	return err;
}

void call_ah_final_cb(ma_error_t status, long response_code, ma_spipe_url_request_t *spipereq, ma_bool_t is_success){
	
	if(is_success){
		MA_LOG(MA_CONTEXT_GET_LOGGER(spipereq->g_context), MA_LOG_SEV_DEBUG, "Spipe connection to site %s succeeded.",spipereq->current_url);
		MA_LOG(MA_CONTEXT_GET_LOGGER(spipereq->g_context), MA_LOG_SEV_DEBUG, "Cache current site for further spipe connection.");		

		#ifndef AH_UNIT_TEST
		{
			char *source_ip = NULL;
			if( (MA_OK == ma_url_request_get_source_ip_info(spipereq->url_request, &source_ip)) && source_ip && strlen(source_ip) ){
				char formated_ip[MA_IPV6_ADDR_LEN] = {0};
				/* ipv6 addr need to be formated as per epo requirement, expand_ipv6_address() is harmless to ipv4 addr */
				expand_ipv6_address(formated_ip, source_ip);
				(void)epo_connected_local_ip_message_publish(spipereq, formated_ip);
                free(source_ip) ; source_ip = NULL ;
			}
			else
				MA_LOG(MA_CONTEXT_GET_LOGGER(spipereq->g_context), MA_LOG_SEV_DEBUG, "ePO communicating source ip retrieval failed.");		
		}
		#endif

		(void)ma_ah_site_url_provider_cache_current_site(spipereq->spipe_site_url_provider);	
		spipereq->is_connected = MA_TRUE;
		spipereq->final_cb(status, response_code, spipereq->userdata, spipereq);	
		MA_LOG(MA_CONTEXT_GET_LOGGER(spipereq->g_context), MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "Agent communication session closed");
	}
	else{
		MA_LOG(MA_CONTEXT_GET_LOGGER(spipereq->g_context), MA_LOG_SEV_ERROR | MA_LOG_FLAG_LOCALIZE, "Agent failed to communicate with ePO Server");
		
		(void)ma_ah_site_url_provider_reset_url(spipereq->spipe_site_url_provider);	
		spipereq->is_connected = MA_FALSE;
		spipereq->final_cb(status, response_code, spipereq->userdata, spipereq);	
	}
}

/*Publish local ip which is communicating to ePO */
static ma_error_t epo_connected_local_ip_message_publish(ma_spipe_url_request_t *url_request, char *source_ip) {
	ma_error_t rc = MA_OK;
	ma_msgbus_endpoint_t *endpoint = NULL;
	ma_message_t *request = NULL;	
	if(MA_OK == (rc = ma_message_create(&request))) {
		if(MA_OK == (rc = ma_message_set_property(request, MA_PROP_KEY_SPIPE_SOURCE_IP_STR, source_ip))) {
			if(MA_OK == (rc = ma_msgbus_endpoint_create(MA_CONTEXT_GET_MSGBUS(url_request->g_context), MA_IO_SERVICE_NAME_STR, NULL, &endpoint))) {
				(void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);				
				if(MA_OK == (rc = ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_INPROC))) {
					if(MA_OK == (rc = ma_msgbus_send_and_forget(endpoint, request))) {
						MA_LOG(MA_CONTEXT_GET_LOGGER(url_request->g_context), MA_LOG_SEV_DEBUG, "ePO communicating source ip<%s> msg sent", source_ip);							
					}
					else
						MA_LOG(MA_CONTEXT_GET_LOGGER(url_request->g_context), MA_LOG_SEV_DEBUG, "ePO communicating source ip<%s> msg send failed err<%d>", source_ip, rc);
				}
				(void)ma_msgbus_endpoint_release(endpoint);
			}
		}
		(void)ma_message_release(request);
	}
	return rc;
}

void spipe_url_request_final_cb(ma_error_t status, long response_code, void *userdata, ma_url_request_t *net_request){
	ma_bool_t is_current_url_reachable = MA_TRUE;
	ma_spipe_url_request_t *request = (ma_spipe_url_request_t*)userdata;

	if(request->is_cancelled){		
		MA_LOG(MA_CONTEXT_GET_LOGGER(request->g_context), MA_LOG_SEV_INFO, "Spipe connection to site %s cancelled.",request->current_url);
		call_ah_final_cb(status, response_code, request, MA_FALSE);
		return;
	}

	if(is_connection_to_next_site_needed(request, status, response_code, &is_current_url_reachable) ){		
		/*Failure*/
		if(MA_CONNECTION_DIRECT != request->method){
			MA_LOG(MA_CONTEXT_GET_LOGGER(request->g_context), MA_LOG_SEV_INFO, "Failed to connect via cached connection, trying direct connection"); 
			ma_spipe_url_request_set_proxy(request, NULL);
			ma_ah_site_url_provider_reset_url(request->spipe_site_url_provider);
		}

		while(NULL != (request->current_url = ma_ah_site_url_provider_get_next_url(request->spipe_site_url_provider, is_current_url_reachable) ) ){
			MA_LOG(MA_CONTEXT_GET_LOGGER(request->g_context), MA_LOG_SEV_INFO, "Initiating spipe connection to site %s.", request->current_url);

			(void)ma_url_request_set_proxy_info(request->url_request, request->proxy_list);
			(void)ma_url_request_set_url(request->url_request, request->current_url);
			if (MA_OK == ma_url_request_send(request->url_request, spipe_url_request_final_cb, request) )
				return;			
			else
				MA_LOG(MA_CONTEXT_GET_LOGGER(request->g_context), MA_LOG_SEV_ERROR, "Spipe connection to site %s failed.", request->current_url);			
		}
		spipe_url_request_send_via_proxy(request);	
	}
	else{			
		/*success*/
		call_ah_final_cb(status, response_code, request, MA_TRUE);
	}
}

static void spipe_url_request_resend_final_cb(ma_error_t status, long response_code, void *userdata, ma_url_request_t *net_request){	
	ma_spipe_url_request_t *request = (ma_spipe_url_request_t*)userdata;
	ma_bool_t is_current_url_reachable = MA_TRUE;

	if(is_connection_to_next_site_needed(request, status, response_code, &is_current_url_reachable)){
		/*reset the urls and relays*/
		(void)ma_spipe_url_request_set_proxy(request, NULL);
		call_ah_final_cb(status, response_code, request, MA_FALSE);
	}
	else
		call_ah_final_cb(status, response_code, request, MA_TRUE);
	return;	
}

ma_error_t ma_spipe_url_request_resend(ma_spipe_url_request_t *self){
	ma_error_t rc = MA_OK;
	if(self){
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_INFO, "Sending the spipe package over existing connection site %s.", self->current_url);				

		(void)ma_url_request_set_proxy_info(self->url_request, self->proxy_list);
		if (MA_OK == (rc = ma_url_request_send(self->url_request, spipe_url_request_resend_final_cb, self)))
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_INFO, "Sending the spipe package over existing connection to site %s.", self->current_url);						
		else{
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_ERROR | MA_LOG_FLAG_LOCALIZE, "Agent failed to communicate with ePO Server");
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_ERROR, "Sending the spipe package over existing connection to site %s failed.", self->current_url);				
		}
		return rc;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_spipe_url_request_send(ma_spipe_url_request_t *self, void *userdata){
	if(self && self->g_context && self->spipe_site_url_provider){
		ma_error_t err = MA_OK;	
		self->userdata = userdata;

		MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "Agent communication session started");
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "Agent is connecting to ePO server");

		/*if already connected.*/
		if(self->url_request && self->is_connected) 
			return ma_spipe_url_request_resend(self);

		self->is_connected = MA_FALSE;

		/*if not url request create a new one.*/
		if(!self->url_request){
			if ( MA_OK == (err = ma_url_request_create(MA_CONTEXT_GET_NET_CLIENT(self->g_context), "https://127.0.0.1/", &self->url_request) ) ){				
				(void)ma_url_request_set_option(self->url_request, MA_URL_REQUEST_OPTION_CONNECT_TIMEOUT, MA_SPIPE_CONNECT_TIMEOUT);
				(void)ma_url_request_set_option(self->url_request, MA_URL_REQUEST_OPTION_VERBOSE_LOG, 1);	
				(void)ma_url_request_set_option(self->url_request, MA_URL_REQUEST_OPTION_TRANSFER_TIMEOUT, MA_SPIPE_TRANSFER_TIMEOUT);				
				(void)ma_url_request_set_option(self->url_request, MA_URL_REQUEST_OPTION_REQUEST_TYPE, MA_URL_REQUEST_TYPE_POST);
				(void)ma_url_request_set_ssl_info(self->url_request, &self->ssl_info);
				(void)ma_url_request_set_io_callback(self->url_request, &self->io_streams);						
                (void)ma_url_request_set_connect_callback(self->url_request, spipe_url_request_connect_cb);
			}
		}

		/*Fill the dynamic values.*/
		if(self->url_request){						
			const char *ca_path = NULL;
			err = MA_ERROR_AH_SPIPE_REQUEST_SEND_FAILED;
			MA_URL_SSL_DEINIT( (&self->ssl_info) );
			ca_path = ma_ah_sitelist_get_ca_file(ma_ah_site_url_provider_get_sitelist(self->spipe_site_url_provider));
			if(ca_path)
				MA_URL_AUTH_SET_SSL_INFO( (&self->ssl_info), ca_path);						

			(void)ma_url_request_set_proxy_info(self->url_request, self->proxy_list);

			while(NULL != (self->current_url = ma_ah_site_url_provider_get_next_url(self->spipe_site_url_provider, MA_FALSE) ) ){
				(void)ma_url_request_set_url(self->url_request, self->current_url);
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_INFO, "Initiating spipe connection to site %s.", self->current_url);
				if (MA_OK == (err = ma_url_request_send(self->url_request, spipe_url_request_final_cb, self) ) )	
					break;				
				else
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_ERROR, "Spipe connection to site %s failed.", self->current_url);				
			}
		}
		if(MA_OK != err)
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_ERROR | MA_LOG_FLAG_LOCALIZE, "Agent failed to communicate with ePO Server");			
		
		return err;
	}
	return MA_ERROR_INVALIDARG;
}


ma_error_t ma_spipe_url_request_sync_send(ma_spipe_url_request_t *self, void *userdata){
	if(self && self->g_context && self->spipe_site_url_provider){
		ma_error_t err = MA_OK;	
		self->userdata = userdata;

		MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "Agent communication session started");
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_INFO | MA_LOG_FLAG_LOCALIZE, "Agent is connecting to ePO server");

		/*if not url request create a new one.*/
		if(!self->url_request){
			if ( MA_OK == (err = ma_url_request_create(MA_CONTEXT_GET_NET_CLIENT(self->g_context), "https://127.0.0.1/", &self->url_request) ) ){				
				(void)ma_url_request_set_option(self->url_request, MA_URL_REQUEST_OPTION_CONNECT_TIMEOUT, MA_SPIPE_CONNECT_TIMEOUT_ATUNINSTALL);
				(void)ma_url_request_set_option(self->url_request, MA_URL_REQUEST_OPTION_VERBOSE_LOG, 1);	
				(void)ma_url_request_set_option(self->url_request, MA_URL_REQUEST_OPTION_TRANSFER_TIMEOUT, MA_SPIPE_TRANSFER_TIMEOUT_ATUNINSTALL);				
				(void)ma_url_request_set_option(self->url_request, MA_URL_REQUEST_OPTION_REQUEST_TYPE, MA_URL_REQUEST_TYPE_POST);
				(void)ma_url_request_set_ssl_info(self->url_request, &self->ssl_info);
				(void)ma_url_request_set_io_callback(self->url_request, &self->io_streams);						                
			}
		}

		/*Fill the dynamic values.*/
		if(self->url_request){						
			const char *ca_path = NULL;
			err = MA_ERROR_AH_SPIPE_REQUEST_SEND_FAILED;
			MA_URL_SSL_DEINIT( (&self->ssl_info) );
			ca_path = ma_ah_sitelist_get_ca_file(ma_ah_site_url_provider_get_sitelist(self->spipe_site_url_provider));
			if(ca_path)
				MA_URL_AUTH_SET_SSL_INFO( (&self->ssl_info), ca_path);						

			(void)ma_url_request_set_proxy_info(self->url_request, self->proxy_list);

			while(NULL != (self->current_url = ma_ah_site_url_provider_get_next_url(self->spipe_site_url_provider, MA_FALSE) ) ){
				(void)ma_url_request_set_url(self->url_request, self->current_url);
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_INFO, "Initiating spipe connection to site %s.", self->current_url);
				if (MA_ERROR_NETWORK_REQUEST_SUCCEEDED == (err = ma_url_request_sync_send(self->url_request, self) ) ){	
					call_ah_final_cb(err, 202, self, MA_TRUE);
					return MA_OK;				
				}
				else
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_ERROR, "Spipe connection to site %s failed.", self->current_url);				
			}
		}
		call_ah_final_cb(err, -1, self, MA_FALSE);
		return err;
	}
	return MA_ERROR_INVALIDARG;
}
