#ifndef MA_SPIPE_SITELIST_MANAGER_H_INCLUDED
#define MA_SPIPE_SITELIST_MANAGER_H_INCLUDED

#include "ma/ma_datastore.h"
#include "ma/internal/services/ahclient/ma_ah_client_service.h"
#include "ma/internal/defs/ma_object_defs.h"

#include "ma_ah_site_url_provider.h"

/* This is derived from ma_spipe_decorator and ma_spipe_handler */
typedef struct ma_spipe_sitelist_manager_s ma_spipe_sitelist_manager_t, *ma_spipe_sitelist_manager_h;

ma_error_t ma_spipe_sitelist_manager_create(ma_context_t *context, ma_spipe_sitelist_manager_t **spipe_sitelist_manager);

ma_spipe_decorator_t *ma_spipe_sitelist_manager_get_decorator(ma_spipe_sitelist_manager_t *spipe_sitelist_manager);

ma_spipe_handler_t *ma_spipe_sitelist_manager_get_handler(ma_spipe_sitelist_manager_t *spipe_sitelist_manager);

ma_spipe_handler_t *ma_spipe_sitelist_manager_get_handler(ma_spipe_sitelist_manager_t *spipe_sitelist_manager);

ma_ah_site_url_provider_t *ma_spipe_sitelist_manager_get_ah_site_url_provider(ma_spipe_sitelist_manager_t *spipe_sitelist_manager);

ma_error_t ma_spipe_sitelist_manager_release(ma_spipe_sitelist_manager_t *spipe_sitelist_manager);

#endif  /* MA_SPIPE_SITELIST_MANAGER_H_INCLUDED */
