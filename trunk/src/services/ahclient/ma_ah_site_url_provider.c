#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ma_ah_site_url_provider.h"
#include "ma/internal/services/ma_policy_settings_bag.h"

#include "ma/internal/ma_strdef.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/utils/repository/ma_sitelist.h"
#include "ma/internal/utils/repository/ma_repository_db_manager.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MA_GUID_SIZE     64
#define MAX_PORT_LENGTH  6
#define URI_LENGTH		 64

#define MA_AH_URL_MAX_SIZE 2048

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "ahclient"


#define MA_CONTEXT_GET_DATABASE(context) (ma_configurator_get_database(MA_CONTEXT_GET_CONFIGURATOR(context)))
#define MA_CONTEXT_GET_DATASTORE(context) (ma_configurator_get_datastore(MA_CONTEXT_GET_CONFIGURATOR(context)))

typedef enum ma_ah_site_hostname_type_e {
	MA_AH_SITE_HOSTNAME_TYPE_UNKNOWN=0,
	MA_AH_SITE_HOSTNAME_TYPE_IPADDRESS,
	MA_AH_SITE_HOSTNAME_TYPE_FQDNAME,
	MA_AH_SITE_HOSTNAME_TYPE_NETBIOSNAME
}ma_ah_site_hostname_type_t;

struct ma_ah_site_url_provider_s {	
	ma_context_t                *g_context;
	ma_ah_sitelist_t			*ah_sitelist;	
	ma_ah_site_t				*last_site;		
	ma_ah_site_t				*current_site;
	ma_uint32_t					current_site_index;	
	ma_ah_site_hostname_type_t	name_type;
	char						*siteurl;
};

void ma_ah_site_url_provider_init(ma_ah_site_url_provider_t *self){
	self->g_context			 = NULL;
	self->ah_sitelist		 = NULL; 
	self->siteurl			 = NULL; 
	self->last_site			 = NULL; 
	self->current_site		 = NULL; 
	self->current_site_index = 0;	
	self->name_type			 = MA_AH_SITE_HOSTNAME_TYPE_UNKNOWN; 
}

void ma_ah_site_url_provider_deinit(ma_ah_site_url_provider_t *self){ 
	if(self->siteurl) free(self->siteurl); 
	self->ah_sitelist		 = NULL; 
	self->siteurl			 = NULL; 
	self->last_site			 = NULL;
	self->current_site		 = NULL; 
	self->current_site_index = 0;	
	self->name_type			 = MA_AH_SITE_HOSTNAME_TYPE_UNKNOWN; 
}

ma_error_t ma_ah_site_url_provider_create(ma_context_t *context, ma_ah_site_url_provider_t **self){	
	if(context && self){	
		ma_ah_site_url_provider_t *tmp = (ma_ah_site_url_provider_t*)calloc(1, sizeof(ma_ah_site_url_provider_t));
		if(tmp){			
			ma_ah_site_url_provider_init(tmp);
			tmp->siteurl = (char*)calloc(MA_AH_URL_MAX_SIZE, 1);
			if(tmp->siteurl){			
				tmp->g_context  = context;
				*self = tmp;
				return MA_OK;
			}
			free(tmp);
		}
		return MA_ERROR_OUTOFMEMORY;
	}
	return MA_ERROR_INVALIDARG;
}

void ma_ah_site_url_provider_release(ma_ah_site_url_provider_t *self){
	if(self){
		if(self->ah_sitelist) (void)ma_ah_sitelist_release(self->ah_sitelist);		
		self->ah_sitelist = NULL;
		if(self->siteurl) free(self->siteurl); 
		self->siteurl = NULL;
		free(self);
	}
}

static void get_sitelist_versions(ma_context_t *ctx, ma_ah_sitelist_t *sitelist) {
	if(ctx && sitelist){
		ma_buffer_t *str = NULL;

		(void)ma_ds_get_str(MA_CONTEXT_GET_DATASTORE(ctx), MA_AH_POLICIES_SITELIST_PATH_NAME_STR, MA_AH_KEY_SITELIST_GLOBAL_VERSION_STR , &str);
		if(str){
			const char *value = NULL;
			size_t size = 0;
			(void)ma_buffer_get_string(str, &value, &size);
			(void)ma_ah_sitelist_set_global_version(sitelist, value);
			(void)ma_buffer_release(str);
			str = NULL;
		}
        
		(void)ma_ds_get_str(MA_CONTEXT_GET_DATASTORE(ctx), MA_AH_POLICIES_SITELIST_PATH_NAME_STR, MA_AH_KEY_SITELIST_LOCAL_VERSION_STR , &str);
		if(str){
			const char *value = NULL;
			size_t size = 0;
			(void)ma_buffer_get_string(str, &value, &size);
			(void)ma_ah_sitelist_set_local_version(sitelist, value);
			(void)ma_buffer_release(str);
			str = NULL;
		}
        
		(void)ma_ds_get_str(MA_CONTEXT_GET_DATASTORE(ctx), MA_AH_POLICIES_SITELIST_PATH_NAME_STR, MA_AH_KEY_SITELIST_CA_FILE_STR , &str);
		if(str){
			const char *value = NULL;
			size_t size = 0;
			(void)ma_buffer_get_string(str, &value, &size);
			(void)ma_ah_sitelist_set_ca_file(sitelist, value);
			(void)ma_buffer_release(str);
			str = NULL;
		}
	}   
	return ;
}

ma_error_t repository_to_ah_sitelist(ma_ah_site_url_provider_t *self, ma_db_recordset_t *repo_records) {
    if(self && repo_records) {
        ma_error_t rc = MA_OK ;
        ma_ah_sitelist_t *sitelist = NULL ;        
        
        if(MA_OK == (rc = ma_ah_sitelist_create(&sitelist))){
            while(MA_ERROR_NO_MORE_ITEMS != ma_db_recordset_next(repo_records)){
				const char *str_value = NULL ;
                int int_value = 0 ;

				(void)ma_db_recordset_get_int(repo_records, MA_REPO_TABLE_FILED_URL_TYPE, &int_value) ;
                
				if(MA_REPOSITORY_URL_TYPE_SPIPE == int_value){					
                    ma_ah_site_t *site = NULL ;

					if(MA_OK == (rc = ma_ah_site_create(&site))) {                
						(void)ma_db_recordset_get_string(repo_records, MA_REPO_TABLE_FILED_NAME, &str_value) ;
						(void)ma_ah_site_set_name(site, str_value) ; str_value = NULL ;                    						

						(void)ma_db_recordset_get_string(repo_records, MA_REPO_TABLE_FILED_SERVER_FQDN, &str_value) ;
						(void)ma_ah_site_set_fqdn_name(site, str_value) ; str_value = NULL ;

						(void)ma_db_recordset_get_string(repo_records, MA_REPO_TABLE_FILED_SERVER_IP, &str_value) ;
						(void)ma_ah_site_set_ip_address(site, str_value) ; str_value = NULL ;
                    
						(void)ma_db_recordset_get_string(repo_records, MA_REPO_TABLE_FILED_SERVER_NAME, &str_value) ;
						(void)ma_ah_site_set_short_name(site, str_value) ; str_value = NULL ;

						(void)ma_db_recordset_get_int(repo_records, MA_REPO_TABLE_FILED_PORT, &int_value) ;
						(void)ma_ah_site_set_port(site, (ma_int32_t)int_value) ;
		            
						(void)ma_db_recordset_get_int(repo_records, MA_REPO_TABLE_FILED_SSL_PORT, &int_value) ;
						(void)ma_ah_site_set_ssl_port(site, int_value) ; int_value = 0 ;
												
						(void)ma_db_recordset_get_int(repo_records, MA_REPO_TABLE_FILED_SITELIST_ORDER, &int_value) ;
						(void)ma_ah_site_set_order(site, int_value) ; int_value = 0 ;

						(void)ma_ah_sitelist_add_site(sitelist, site) ;
					}
                }
            }            
        }
		get_sitelist_versions(self->g_context, sitelist);
		if(self->ah_sitelist){			
			(void)ma_ah_sitelist_release(self->ah_sitelist);
			self->ah_sitelist  = NULL;
		}
		self->ah_sitelist = sitelist;		
		(void)ma_ah_sitelist_adjust_site_order(self->ah_sitelist);		

		MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_DEBUG, "Successfully loaded new sitelist in the memory.") ;
        return rc ;
    }
    return MA_ERROR_INVALIDARG ;
}



static ma_error_t load_sitelist(ma_ah_site_url_provider_t *self){
	ma_error_t rc = MA_OK ;
	ma_db_recordset_t *repo_records = NULL ;
	MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_DEBUG, "Loading latest recived sitelist in the memory.") ;	
	if(MA_OK == (rc = ma_repository_db_get_spipe_repositories(MA_CONTEXT_GET_DATABASE(self->g_context), &repo_records))){
		if(MA_OK == (rc = repository_to_ah_sitelist(self, repo_records)))			
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_DEBUG, "Successfully loaded new sitelist.") ;		
		else
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_DEBUG, "Failed to loaded new sitelist(%d)", rc) ;		
		(void)ma_db_recordset_release(repo_records) ;
	}
	return rc ;
}

void ma_ah_site_url_provider_load_sitelist(ma_ah_site_url_provider_t *self){
	if(self){
		ma_ah_site_url_provider_reset_url(self); 				
		load_sitelist(self);
	}
}

ma_ah_sitelist_t *ma_ah_site_url_provider_get_sitelist(ma_ah_site_url_provider_t *self){
	if(self){
		if(!self->ah_sitelist) 
			ma_ah_site_url_provider_load_sitelist(self);
		return self->ah_sitelist;
	}
	return NULL;
}

void ma_ah_site_url_provider_cache_current_site(ma_ah_site_url_provider_t *self){
	if(self) {
		if(self->current_site) {
			/* add the last used epo site info to db - the same will be provided as part of MA INFO to clients */
			ma_ds_t *ds_handle = MA_CONTEXT_GET_DATASTORE(self->g_context) ;
			const char *eposerver = (self->name_type == MA_AH_SITE_HOSTNAME_TYPE_NETBIOSNAME) ? ma_ah_site_get_short_name(self->current_site) : ((self->name_type == MA_AH_SITE_HOSTNAME_TYPE_FQDNAME) ? ma_ah_site_get_fqdn_name(self->current_site): ma_ah_site_get_ip_address(self->current_site)) ;
			ma_uint32_t ssl_port = ma_ah_site_get_ssl_port(self->current_site);
			ma_uint32_t non_ssl_port = ma_ah_site_get_port(self->current_site);
			ma_bool_t is_secure_port_enabled = (ssl_port > 0 )? MA_TRUE : MA_FALSE;
			

			(void)ma_ds_set_str(ds_handle, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_LAST_COMMUNICATED_EPO_SERVER_STR, eposerver, strlen(eposerver)) ;
			(void)ma_ds_set_int(ds_handle, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_LAST_COMMUNICATED_EPO_PORT_INT, (ma_int64_t)is_secure_port_enabled ? ssl_port : non_ssl_port) ;

			self->last_site          = self->current_site; 
			self->current_site       = NULL; 
		}
		self->name_type			 = MA_AH_SITE_HOSTNAME_TYPE_UNKNOWN; 
		self->current_site_index = 0; 
	}
}

void ma_ah_site_url_provider_reset_url(ma_ah_site_url_provider_t *self) {
	if(self){
		self->last_site			 = NULL; 
		self->name_type			 = MA_AH_SITE_HOSTNAME_TYPE_UNKNOWN; 
		self->current_site_index = 0; 
		self->current_site		 = NULL; 
	}
}

/*Get the guid from agent policy*/
static ma_error_t get_agent_guid(ma_ah_site_url_provider_t *self, char *guid){		
	const char *agent_guid =  ma_configurator_get_agent_guid(MA_CONTEXT_GET_CONFIGURATOR(self->g_context));
	if(agent_guid){
		strncpy(guid, agent_guid, MA_GUID_SIZE);						
		return MA_OK;
	}
	return MA_ERROR_OBJECTNOTFOUND;
}

static ma_error_t get_tenantid(ma_ah_site_url_provider_t *self, char *tenantid){		
	const char *tenantid_ =  ma_configurator_get_tenantid(MA_CONTEXT_GET_CONFIGURATOR(self->g_context));
	if(tenantid_){
		strncpy(tenantid, tenantid_, MA_GUID_SIZE);						
		return MA_OK;
	}
	return MA_ERROR_OBJECTNOTFOUND;
}
/*
State machine to get the appropriate site for next spipe connection.
*/
static ma_ah_site_t *goto_next_ah_site(ma_ah_site_url_provider_t *self, ma_bool_t is_current_url_reachable){
	/* 
	if: current url reachable so no point of trying the same site with different name type, so move to next site. 
	Setting self->name_type = MA_AH_SITE_HOSTNAME_TYPE_NETBIOSNAME; forthcoming logic move us to next site.
	*/	
	if(is_current_url_reachable)
		self->name_type = MA_AH_SITE_HOSTNAME_TYPE_NETBIOSNAME;

	/* if: we are using last used site. */	
	if(self->last_site){ 
		/* if: we had tried all combination then reset last used site to NULL. */	
		if(MA_AH_SITE_HOSTNAME_TYPE_NETBIOSNAME == self->name_type){
			self->last_site = NULL;
			self->current_site = ma_ah_sitelist_get_site(++self->current_site_index, self->ah_sitelist); /* should get the next site */
			self->name_type = MA_AH_SITE_HOSTNAME_TYPE_IPADDRESS ;
		}
		/*else: move to same site with different name type. */	
		else {
			self->name_type = (ma_ah_site_hostname_type_t)(self->name_type+1);	
			return self->last_site;
		}
	}
	/* else: we are not using the last used site.*/	
	else {
		/* if: we are already trying some site and failed to connect.*/	
		if(self->current_site) { 
			/* if: we are already tried all name types the move to next site.*/	
			if (MA_AH_SITE_HOSTNAME_TYPE_NETBIOSNAME == self->name_type){
				self->current_site = ma_ah_sitelist_get_site(self->current_site_index++, self->ah_sitelist);
				self->name_type = MA_AH_SITE_HOSTNAME_TYPE_IPADDRESS;
			}
			/* else: move to same site with different name type.*/	
			else{
				self->name_type = (ma_ah_site_hostname_type_t)(self->name_type+1);
			}		
		}
		/*else: we are trying first time, so get the first site.*/	
		else{ 
			self->current_site = ma_ah_sitelist_get_site(self->current_site_index++, self->ah_sitelist);
			self->name_type = MA_AH_SITE_HOSTNAME_TYPE_IPADDRESS;
		}
	}
	/*If: we have tried all sites the reset the self->current_site_index = 0, so next time we will try from the first site.*/		
	if(!self->current_site)
		self->current_site_index = 0;
	return self->current_site;
}

const char *ma_ah_site_url_provider_get_next_url(ma_ah_site_url_provider_t *self, ma_bool_t is_current_url_reachable){
	if(self && self->ah_sitelist){
		ma_ah_site_t *site_to_use = NULL;
		const char *hostname = NULL;		
		char guid[MA_GUID_SIZE+1] = {0};
		char tenantid[MA_GUID_SIZE+1] = {0};
		
		while(!hostname){
			if ( NULL == (site_to_use = goto_next_ah_site(self, is_current_url_reachable) ) )
				return NULL;
			switch(self->name_type){
				case MA_AH_SITE_HOSTNAME_TYPE_IPADDRESS:
					hostname = ma_ah_site_get_ip_address(site_to_use);
					break;
				case MA_AH_SITE_HOSTNAME_TYPE_FQDNAME:
					hostname = ma_ah_site_get_fqdn_name(site_to_use);
					break;
				case MA_AH_SITE_HOSTNAME_TYPE_NETBIOSNAME:
					hostname = ma_ah_site_get_short_name(site_to_use);
					break;
				default:
					return NULL;
			};
		}		
		if(MA_OK == get_agent_guid(self, guid)){			
			ma_uint32_t ssl_port = ma_ah_site_get_ssl_port(site_to_use);
			ma_uint32_t non_ssl_port = ma_ah_site_get_port(site_to_use);
			ma_bool_t is_secure_port_enabled = (ssl_port > 0 )? MA_TRUE : MA_FALSE;
			get_tenantid(self, tenantid);				
			MA_MSC_SELECT(_snprintf, snprintf)(self->siteurl, MA_AH_URL_MAX_SIZE, "%s://%s:%u/spipe/pkg?AgentGuid=%s&Source=Agent_3.0.0", 
									is_secure_port_enabled ? "https" : "http", hostname, is_secure_port_enabled ? ssl_port : non_ssl_port, guid) ;
			if(tenantid[0] != '\0') {
				strncat(self->siteurl, "&TenantId=", strlen("&TenantId="));
				strncat(self->siteurl, tenantid, strlen(tenantid));
			}			
			return self->siteurl;
		}		
	}
	return NULL;
}



