#include "ma_spipe_url_request_via_relay.h"

#include "ma/internal/defs/ma_io_defs.h"
#include "ma/internal/utils/proxy/ma_proxy_internal.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_message.h"

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "ahclient"

extern ma_bool_t is_connection_to_next_site_needed(ma_spipe_url_request_t *request, ma_error_t status, int response_code, ma_bool_t *is_current_url_reachable);

void spipe_url_request_via_relay_final_cb(ma_error_t status, long response_code, void *userdata, ma_url_request_t *net_request){
	ma_bool_t is_current_url_reachable = MA_TRUE;
	ma_spipe_url_request_t *request = (ma_spipe_url_request_t*)userdata;

	if(request->is_cancelled){
		MA_LOG(MA_CONTEXT_GET_LOGGER(request->g_context), MA_LOG_SEV_INFO, "Spipe connection to site %s cancelled.",request->current_url);
		call_ah_final_cb(status, response_code, request, MA_FALSE);
		return;
	}

	if(is_connection_to_next_site_needed(request, status, response_code, &is_current_url_reachable) ){				
		/*failed try with next URL.*/
		while(NULL != (request->current_url = ma_ah_site_url_provider_get_next_url(request->spipe_site_url_provider, MA_FALSE))){
			ma_error_t err = MA_OK;
			(void)ma_url_request_set_proxy_info(request->url_request, request->proxy_list);
			(void)ma_url_request_set_url(request->url_request, request->current_url);
			MA_LOG(MA_CONTEXT_GET_LOGGER(request->g_context), MA_LOG_SEV_INFO, "Initiating spipe connection via relay to site %s.", request->current_url);

			if(MA_OK == (err = ma_url_request_send(request->url_request, spipe_url_request_via_relay_final_cb, request))){
				MA_LOG(MA_CONTEXT_GET_LOGGER(request->g_context), MA_LOG_SEV_INFO, "Initiating spipe connection via relay to site %s success.", request->current_url);
				return;				
			}
			else
				MA_LOG(MA_CONTEXT_GET_LOGGER(request->g_context), MA_LOG_SEV_ERROR, "Spipe connection to site vai relay %s failed.", request->current_url);				
		}	
		/*failed with all relays lets clean up so that in next run we will discover.*/
		(void)ma_spipe_url_request_set_relay(request, NULL);		
		call_ah_final_cb(status, response_code,request, MA_FALSE);		
	}
	else{ 
		/*successful connection*/
		ma_proxy_list_t *new_list = 0;	
		/*Set last used proxy to use later on.*/
		if(MA_OK == ma_proxy_list_create(&new_list)){
			ma_int32_t last_used_relay_index = 0;
			ma_proxy_t *last_used_proxy = NULL;

			(void)ma_proxy_list_get_last_proxy_index(request->proxy_list, &last_used_relay_index);
			if(MA_OK == ma_proxy_list_get_proxy(request->proxy_list, last_used_relay_index, &last_used_proxy)){
				(void)ma_proxy_list_add_proxy(new_list, last_used_proxy);
				(void)ma_proxy_release(last_used_proxy); last_used_proxy = NULL;
				(void)ma_spipe_url_request_set_relay(request, new_list);				
			}
			else
				(void)ma_proxy_list_release(new_list);
		}
		/*call success*/
		call_ah_final_cb(status, response_code, request, MA_TRUE);		
	}
}

/*relay functions*/
static void send_spipe_via_relay(ma_spipe_url_request_t *self, ma_proxy_list_t *proxy_list){
	if(self) {
		if(proxy_list){		
			
			(void)ma_ah_site_url_provider_reset_url(self->spipe_site_url_provider);
			(void)ma_spipe_url_request_set_relay(self, proxy_list);
				
			while(NULL != (self->current_url = ma_ah_site_url_provider_get_next_url(self->spipe_site_url_provider, MA_FALSE))){
				ma_error_t err = MA_OK;
				(void)ma_url_request_set_proxy_info(self->url_request, self->proxy_list);
				(void)ma_url_request_set_url(self->url_request, self->current_url);
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_INFO, "Initiating spipe connection via relay to site %s.", self->current_url);

				if(MA_OK == (err = ma_url_request_send(self->url_request, spipe_url_request_via_relay_final_cb, self))){
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_INFO, "Initiated spipe connection to site %s via relay.", self->current_url);
					return;				
				}
				else
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_ERROR, "Spipe connection to site %s vai relay failed.", self->current_url);				
			}		
		
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_DEBUG, "Spipe connection to spipe sites via relay failed.");	
			call_ah_final_cb(MA_ERROR_AH_SPIPE_REQUEST_SEND_FAILED, -1, self, MA_FALSE);
			return;
		}
		else{
			/*no relay found*/
			/*send_relay_refresh(self);*/
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_DEBUG, "No relay found in the network, calling final cb.");						
		}

		call_ah_final_cb(MA_ERROR_AH_SPIPE_REQUEST_SEND_FAILED, -1, self, MA_FALSE);
	}
}


static ma_error_t async_request_cb(ma_error_t status, ma_message_t *response, void *cb_data, ma_msgbus_request_t *request) {
    ma_spipe_url_request_t *self = (ma_spipe_url_request_t *) cb_data;
	ma_proxy_list_t *proxy_list = NULL;

    if(response){
        ma_variant_t *payload = NULL;
		if(MA_OK == ma_message_get_payload(response, &payload) && payload){			
			if(MA_OK == ma_proxy_list_from_variant(payload, &proxy_list))
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_DEBUG, "Get the relay list from relay handler.");							
			ma_variant_release(payload);					
		}
		ma_message_release(response);
    }

	if(request) {
		ma_msgbus_endpoint_t * ep = NULL;
		(void) ma_msgbus_request_get_endpoint(request, &ep);         
        (void) ma_msgbus_request_release(request);          
        if(ep) (void) ma_msgbus_endpoint_release(ep);
	}

	send_spipe_via_relay(self, proxy_list);
	return MA_OK;
}

void spipe_url_request_send_via_relay(ma_spipe_url_request_t *self){
	if(self->relay_enabled){
		ma_error_t rc = MA_OK;	
		ma_message_t *msg = NULL;
		(void)ma_spipe_url_request_set_relay(self, NULL);

		if(MA_OK == (rc = ma_message_create(&msg))){		
			ma_msgbus_endpoint_t *endpoint = NULL;

			(void)ma_message_set_property(msg, MA_IO_SERVICE_MSG_TYPE, MA_IO_MSG_TYPE_GET_RELAY_INFO);
			if(MA_OK == (rc = ma_msgbus_endpoint_create(MA_CONTEXT_GET_MSGBUS(self->g_context), MA_IO_SERVICE_NAME_STR, NULL, &endpoint))){
				ma_msgbus_request_t *request = NULL;
				(void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);			
				(void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_INPROC);						
				(void)ma_msgbus_endpoint_setopt(endpoint, MSGBUSOPT_TIMEOUT, 10*1000);

				if(MA_OK == (rc = ma_msgbus_async_send(endpoint, msg, async_request_cb, self, &request))){
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_DEBUG, "Sending relay discovery request to relay handler.");	
					ma_message_release(msg); msg = NULL;
					return;
				}	
				(void)ma_msgbus_endpoint_release(endpoint);
			}		
			(void)ma_message_release(msg);
		}
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_ERROR, "Failed to get relay info from relay handler, rc = <%d>.", rc);	
	}
	else
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_DEBUG, "Relay communication is disabled.");	

	call_ah_final_cb(MA_ERROR_AH_SPIPE_REQUEST_SEND_FAILED, -1, self, MA_FALSE);
	return;
}

