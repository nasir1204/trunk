#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/utils/repository/ma_sitelist.h"
#include "ma/internal/utils/repository/ma_repository_db_manager.h"
/* #include "ma/internal/utils/firewall_exceptions/ma_firewall_exceptions.h" */

#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/xml/ma_xml.h"
#include "ma/internal/ma_strdef.h"

#include "ma_spipe_sitelist_manager.h"
#include "ma_spipe_sitelist_common.h"
#include "ma_ah_site_url_provider.h"

#include "ma/ma_msgbus.h"
#include "ma/ma_message.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "ahclient"

#define MA_CONTEXT_GET_DATABASE(context)  (ma_configurator_get_database(MA_CONTEXT_GET_CONFIGURATOR(context)))
#define MA_CONTEXT_GET_DATASTORE(context) (ma_configurator_get_datastore(MA_CONTEXT_GET_CONFIGURATOR(context)))
#define MA_CONTEXT_GET_DATAPATH(context)  (ma_configurator_get_agent_data_path(MA_CONTEXT_GET_CONFIGURATOR(context)))

struct ma_spipe_sitelist_manager_s {
    ma_spipe_decorator_t        decorator_impl;
    ma_spipe_handler_t          handler_impl;
    ma_context_t                *context;	
    ma_spipe_priority_t         priority;
	ma_ah_site_url_provider_t   *url_provider;
};

static void ma_spipe_sitelist_manager_destroy(ma_spipe_sitelist_manager_t *self);
#ifndef AH_UNIT_TEST
static ma_error_t sitelist_handler(ma_spipe_handler_t *handler, const ma_spipe_response_t *spipe_response);
static ma_error_t decorate_sitelist_package(ma_spipe_decorator_t *decorator, ma_spipe_priority_t priority, ma_spipe_package_t *spipepackage);
#endif

/* Sitelist Decorator methods */
ma_error_t decorate_sitelist_package(ma_spipe_decorator_t *decorator, ma_spipe_priority_t priority, ma_spipe_package_t *spipepackage) {
    if(decorator && spipepackage) {
        ma_spipe_sitelist_manager_t *self = (ma_spipe_sitelist_manager_t *)decorator->data;
        ma_error_t rc = MA_OK;
		const char *global_version = ma_ah_sitelist_get_global_version(ma_ah_site_url_provider_get_sitelist(self->url_provider));
        if(global_version) {
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "sitelist version info <%s>", global_version);
            rc = ma_spipe_package_add_info(spipepackage, STR_SITEINFO_VERSION, (const unsigned char *)global_version, strlen(global_version));
		}
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t get_spipe_priority(ma_spipe_decorator_t *decorator, ma_spipe_priority_t *priority) {
    if(decorator && priority) {
		ma_spipe_sitelist_manager_t *self = (ma_spipe_sitelist_manager_t *)decorator->data;
		if(self) {
			*priority = self->priority;
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_TRACE, "Priority set for sitelist decorator <%d>", *priority);
		}
		else
			*priority = MA_SPIPE_PRIORITY_NONE;

        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t sitelist_decorator_destroy(ma_spipe_decorator_t *decorator) {
    ma_spipe_sitelist_manager_t *self = (ma_spipe_sitelist_manager_t *)decorator->data;
    if(self && 0 == self->handler_impl.ref_count) {
		ma_spipe_sitelist_manager_destroy(self);    
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_spipe_decorator_methods_t sitelist_decorator_methods = {
    &decorate_sitelist_package,
    &get_spipe_priority,
    &sitelist_decorator_destroy
};

/* static ma_error_t update_firewall_rules(ma_spipe_sitelist_manager_t *self) {
	ma_error_t rc = MA_OK;
	ma_msgbus_endpoint_t *ep = NULL;
	
	if(MA_OK == (rc = ma_msgbus_endpoint_create(MA_CONTEXT_GET_MSGBUS(self->context), MA_IO_SERVICE_NAME_STR, NULL, &ep))) {
		ma_message_t *req = NULL;
		(void) ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_INPROC);
		(void) ma_msgbus_endpoint_setopt(ep, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
		if(MA_OK == (rc = ma_message_create(&req))) {
			(void) ma_message_set_property(req, MA_IO_SERVICE_MSG_TYPE, MA_IO_MSG_TYPE_FIREWALL_RULE_CHANGED_STR);
			rc = ma_msgbus_send_and_forget(ep, req);
			(void) ma_message_release(req);
		}
		(void) ma_msgbus_endpoint_release(ep);
	}
	
	if(MA_OK == rc)
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_TRACE, "Successfully updated firewall rules");
	else
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to update firewall rule");

	return rc;
} */

ma_error_t sitelist_handler(ma_spipe_handler_t *handler, const ma_spipe_response_t *spipe_response) {
    if(handler && spipe_response) {
        ma_error_t rc = MA_OK;
        ma_spipe_sitelist_manager_t *self = (ma_spipe_sitelist_manager_t*)handler->data;
        if(spipe_response->http_response_code == 200) {
            /* It will handle the response messages if package type is "PeopsResponse" */
            const unsigned char *value=NULL;
		    size_t size=0;
            rc = ma_spipe_package_get_info(spipe_response->respond_spipe_pkg, STR_PACKAGE_TYPE, &value, &size);

            if((MA_OK == rc) && value) {
                /* STR_PKGTYPE_PROPS_RESPONSE */
                if(!memcmp(value, STR_PKGTYPE_PROPS_RESPONSE, strlen(STR_PKGTYPE_PROPS_RESPONSE))){
                    char *data = NULL;
                    size_t data_len = 0;
                    MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Received spipe package type <%s>", STR_PKGTYPE_PROPS_RESPONSE);
                    if(MA_OK == (rc = ma_spipe_package_get_data(spipe_response->respond_spipe_pkg, STR_SITELIST_NAME, (void **)&data, &data_len))) {
						
						ma_sitelist_handler_t handler = {0};
                        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Recived new sitelist.xml from epo.");

						if(MA_OK == ma_sitelist_handler_init(&handler, MA_CONTEXT_GET_CRYPTO(self->context), MA_CONTEXT_GET_DATASTORE(self->context), MA_CONTEXT_GET_DATABASE(self->context),  MA_CONTEXT_GET_LOGGER(self->context), MA_CONTEXT_GET_DATAPATH(self->context))){
							if(MA_OK == (rc = ma_sitelist_handler_import_xml(&handler, data, data_len, MA_TRUE))) {
								/* notifying to AH client about sitlist updates*/
								ma_ah_site_url_provider_load_sitelist(self->url_provider);	
								/* (void) update_firewall_rules(self); */
								MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Succeeded to copy sitelist data.");
							}
							else
								MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to copy sitelist data.");
						}
						else
							MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to initialize the sitelist handler.");
                    }
                    else
                        MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "No Sitelist.xml in received spipe package.");
                }
                else
                    MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "Received unknown spipe package type.");
            }
            else
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Failed to get spipe package info.");
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t sitelist_handler_destroy(ma_spipe_handler_t *handler) {
    ma_spipe_sitelist_manager_t *self = (ma_spipe_sitelist_manager_t *)handler->data;
    if(self && 0 == self->decorator_impl.ref_count) {
        ma_spipe_sitelist_manager_destroy(self);
    }
    return MA_ERROR_INVALIDARG;
}

static ma_spipe_handler_methods_t sitelist_handler_methods = {
    &sitelist_handler,
    &sitelist_handler_destroy
};

ma_error_t ma_spipe_sitelist_manager_create(ma_context_t *context, ma_spipe_sitelist_manager_t **spipe_sitelist_manager) {
     if(context && spipe_sitelist_manager) {
        ma_spipe_sitelist_manager_t *self = (ma_spipe_sitelist_manager_t*)calloc(1, sizeof(ma_spipe_sitelist_manager_t));
        if(self) {
			ma_ah_site_url_provider_create(context, &self->url_provider);
			if(self->url_provider){
				ma_spipe_decorator_init((ma_spipe_decorator_t *)&self->decorator_impl, &sitelist_decorator_methods, self);
				ma_spipe_handler_init((ma_spipe_handler_t *)&self->handler_impl, &sitelist_handler_methods, self);
				self->priority = MA_SPIPE_PRIORITY_NORMAL;
				self->context = context;
				ma_ah_site_url_provider_load_sitelist(self->url_provider);
				*spipe_sitelist_manager = self;				
				return MA_OK;
			}
			free(self);        
		}		
        return MA_ERROR_OUTOFMEMORY;
    }
    return MA_ERROR_INVALIDARG;
}


ma_spipe_decorator_t *ma_spipe_sitelist_manager_get_decorator(ma_spipe_sitelist_manager_t *self) {
    return (self) ? &self->decorator_impl : NULL ;
}

ma_spipe_handler_t *ma_spipe_sitelist_manager_get_handler(ma_spipe_sitelist_manager_t *self) {
    return (self) ? &self->handler_impl : NULL ;
}

ma_ah_site_url_provider_t *ma_spipe_sitelist_manager_get_ah_site_url_provider(ma_spipe_sitelist_manager_t *self){
	return (self) ? self->url_provider : NULL ;
}

void ma_spipe_sitelist_manager_destroy(ma_spipe_sitelist_manager_t *self){
	if(self){
		if(self->url_provider)
			ma_ah_site_url_provider_release(self->url_provider);
		free(self);     
	}
}

ma_error_t ma_spipe_sitelist_manager_release(ma_spipe_sitelist_manager_t *self) {
    /* note, the double release below matches the double ref count to both sides performed in the constructor */
    ma_spipe_decorator_release(&self->decorator_impl);
    ma_spipe_handler_release(&self->handler_impl);
    return MA_OK;
}

