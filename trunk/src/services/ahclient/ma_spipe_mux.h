#ifndef MA_SPIPE_MUX_H_INCLUDED
#define MA_SPIPE_MUX_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>

#include "ma/internal/utils/spipe/ma_spipe_package.h"
#include "ma/internal/services/ahclient/ma_spipe_decorator.h"
#include "ma/internal/utils/macrypto/ma_crypto.h"
#include "ma/internal/utils/datastructures/ma_slist.h"
#include "ma/internal/utils/threading/ma_thread.h"

MA_CPP(extern "C" {)

typedef struct ma_spipe_decorator_node_s ma_spipe_decorator_node_t;
struct ma_spipe_decorator_node_s{
	ma_spipe_decorator_t *decorator;
	MA_SLIST_NODE_DEFINE(ma_spipe_decorator_node_t);
};

typedef struct ma_spipe_mux_s ma_spipe_mux_t;
struct ma_spipe_mux_s{
	MA_SLIST_DEFINE(dlist,ma_spipe_decorator_node_t);
};

void ma_spipe_mux_init(ma_spipe_mux_t *self);

void ma_spipe_mux_deinit(ma_spipe_mux_t *self);

ma_error_t ma_spipe_mux_add_spipe_decorator(ma_spipe_mux_t *self, ma_spipe_decorator_t *sdecorator);

void ma_spipe_mux_remove_spipe_decorator(ma_spipe_mux_t *self, ma_spipe_decorator_t *sdecorator);

ma_spipe_priority_t ma_spipe_mux_get_spipe_priority(ma_spipe_mux_t *self);

ma_error_t ma_spipe_mux_start(ma_spipe_mux_t *self, ma_spipe_priority_t priority, const char *pkg_type, ma_crypto_t *crypto, ma_logger_t *logger, ma_stream_t *stream);

MA_CPP(})

#endif //MA_SPIPE_MUX_H_INCLUDED
