#ifndef MA_SPIPE_SITELIST_COMMON_H_INCLUDED
#define MA_SPIPE_SITELIST_COMMON_H_INCLUDED

#include "ma/ma_common.h"

MA_CPP(extern "C" {)

void split_host_and_port(const char *host_port, char *host, ma_uint32_t *port);

MA_CPP(})

#endif /* MA_SPIPE_SITELIST_COMMON_H_INCLUDED */
