#include "ma/ma_msgbus.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/services/ma_policy_settings_bag.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/core/msgbus/ma_msgbus_private.h"
#include "ma/internal/utils/platform/ma_sys_apis.h"
#include "ma/internal/services/ahclient/ma_ah_client_service.h"
#include "ma/internal/services/ma_service.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/repository/ma_repository_client.h"
#include "ma/internal/utils/proxy/ma_proxy_internal.h"
#include "ma_spipe_sitelist_manager.h"

#include "ma_spipe_mux.h"
#include "ma_spipe_demux.h"
#include "ma_spipe_url_request.h"
#include "ma_spipe_alert_list.h"
//#include "ma_spipe_url_request_via_relay.h"

#include "ma/sensor/ma_sensor_msg.h"
#include "ma/internal/defs/ma_sensor_defs.h"
#include "ma/internal/defs/ma_relay_defs.h"
#include "ma/sensor/ma_sensor_utils.h"


#include "ma_spipe_key_manager.h"


#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "ahclient"

#define MIN_IMMEDIATE_SPIPE_TRIGGER_TIMEOUT			1    /*in sec.*/
#define MAX_IMMEDIATE_SPIPE_TRIGGER_TIMEOUT			2    /*in sec*/
#define MIN_NORMAL_SPIPE_TRIGGER_TIMEOUT			4	 /*in sec.*/
#define MAX_NORMAL_SPIPE_TRIGGER_TIMEOUT			8	 /*in sec.*/
#define IMMEDIATE_SPIPE_TRIGGER_TIMEOUT				(0)
#define NORMAL_SPIPE_TRIGGER_TIMEOUT				(MIN_NORMAL_SPIPE_TRIGGER_TIMEOUT * 1000 + ma_sys_rand()%( (MAX_NORMAL_SPIPE_TRIGGER_TIMEOUT - MIN_NORMAL_SPIPE_TRIGGER_TIMEOUT) * 1000) )

typedef enum ma_ah_client_service_state_e{
	/*AH client stopped.*/
	MA_AH_CS_STATE_STOP = 0,

	/*AH client started.*/
	MA_AH_CS_STATE_START,

	/*AH client schduled, with normal priority*/
	MA_AH_CS_STATE_SPIPE_SCHEDULED,

	/*AH client schduled, with immediate priority*/
	MA_AH_CS_STATE_SPIPE_IMMEDIATE_SCHEDULED,

	/*AH client in process of spipe decoration*/
	MA_AH_CS_STATE_SPIPE_MUX,

	/*AH client in process of spipe upload*/
	MA_AH_CS_STATE_SPIPE_SEND,

	/*AH client in process of spipe response handling*/
	MA_AH_CS_STATE_SPIPE_DEMUX,
}ma_ah_client_service_state_t;

#define CHECK_ACTIVE_SPIPE(state) (state >= MA_AH_CS_STATE_SPIPE_MUX)
#define IS_AH_STOPPED(state)      (state == MA_AH_CS_STATE_STOP)
#define IS_AH_STARTED(state)      (state != MA_AH_CS_STATE_STOP)

struct ma_ah_client_service_s{
	ma_service_t					base;
	ma_context_t                    *g_context;

	uv_loop_t                       *uv_loop;
	ma_bool_t						cancel_spipe_connection;
	ma_ah_client_service_state_t	ah_service_state;

	uv_timer_t						spipe_mux_trigger;
	ma_spipe_mux_t					spipe_multiplexer;
	ma_spipe_demux_t				spipe_demultiplexer;
	ma_spipe_alert_list_t			spipe_alerts;

	/*sitelist manager.*/
	ma_spipe_sitelist_manager_t     *sitelist_manager;

	/*spipe url request.*/
	ma_spipe_priority_t				spipe_priority;
	ma_spipe_url_request_t			*spipe_url_request;
	ma_error_t                      spipe_error;

	/* subscribe for sensor msg */
	ma_msgbus_subscriber_t				*sensor_subscriber;
	ma_network_connectivity_status_t	nw_conn_status;

    /* spipe key manager */
    ma_spipe_key_manager_t              *key_manager;

	/*self address*/
	ma_ah_client_service_t			*self;
};

/*spipe connection utility function.*/
static void schedule_spipe_connection(ma_ah_client_service_t *self);
static void spipe_connection_trigger_callback(uv_timer_t *spipe_mux_trigger, int status);
static ma_error_t spipe_site_connect_callback(void *userdata, ma_spipe_url_request_t *spipereq);
static void spipe_connection_response_callback(ma_error_t status, int response_code, void *userdata, ma_spipe_url_request_t *spipereq);
static void prepare_spipe_package(ma_ah_client_service_t *self, ma_spipe_url_request_t *spipereq);
static ma_error_t		proxy_config_get_cb(ma_error_t status, ma_client_t *ma_client, const ma_proxy_config_t *proxy_config, void *cb_data);

/*intenal utility functions.*/
static ma_error_t ma_on_sensor_msg(const char *topic, ma_message_t *message, void *cb_data);

/*Ah client service base methods.*/
static ma_error_t ma_ah_client_service_configure(ma_ah_client_service_t *self, ma_context_t *context, unsigned hint);
static ma_error_t ma_ah_client_service_start(ma_ah_client_service_t *self);
static ma_error_t ma_ah_client_service_stop(ma_ah_client_service_t *self);
static void ma_ah_client_service_destroy(ma_ah_client_service_t *self);

static ma_service_methods_t ah_client_service_methods = {
	&ma_ah_client_service_configure,
	&ma_ah_client_service_start,
	&ma_ah_client_service_stop,
	&ma_ah_client_service_destroy
};

ma_error_t ma_ah_client_service_create(const char *service_name, ma_service_t **ah_service){
	if(service_name && ah_service){

		ma_ah_client_service_t *ah_instance = (ma_ah_client_service_t*)calloc(1, sizeof(ma_ah_client_service_t) );
		if (!ah_instance)
			return MA_ERROR_OUTOFMEMORY;

		/*Service init.*/
		ma_service_init((ma_service_t*)ah_instance, &ah_client_service_methods, service_name, ah_instance);
		ah_instance->ah_service_state = MA_AH_CS_STATE_STOP;

		/*spipe mux/demux*/
		ma_spipe_alert_list_init(&ah_instance->spipe_alerts);
		ma_spipe_mux_init(&ah_instance->spipe_multiplexer);
		ma_spipe_demux_init(&ah_instance->spipe_demultiplexer);

		ah_instance->self = (ma_ah_client_service_t*)ah_instance;
		*ah_service = (ma_service_t *)ah_instance;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

static ma_bool_t check_context_validity(ma_context_t *context){
	/*Will see if we need this*/
	return (context ?  MA_TRUE : MA_FALSE);
}

static void handle_relay_policy(ma_ah_client_service_t *self){
	ma_error_t rc = MA_OK;
	ma_policy_settings_bag_t *pso_bag = MA_CONTEXT_GET_POLICY_SETTINGS_BAG(self->g_context);		
	ma_int32_t relay_client_enabled = 1;

	if(MA_OK == (rc = ma_policy_settings_bag_get_int(pso_bag, MA_RELAY_SERVICE_SECTION_NAME_STR, MA_RELAY_KEY_ENABLE_CLIENT_INT, MA_FALSE, &relay_client_enabled, 1))){
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_DEBUG, "Relay client communication is \"%s\".", relay_client_enabled ? "Enabled": "Disabled");

		ma_spipe_url_request_set_relay_enabled(self->spipe_url_request, relay_client_enabled ? MA_TRUE : MA_FALSE );	
	}
	else
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_DEBUG, "Failed to get relay policy, rc = <%d>.", rc);
}

ma_error_t ma_ah_client_service_configure(ma_ah_client_service_t *self, ma_context_t *context, unsigned hint){
	if(self && context){
		if(MA_SERVICE_CONFIG_NEW == hint){
			if(check_context_validity(context)){
				ma_event_loop_t *maloop = NULL;
                ma_error_t rc = MA_OK;

                self->g_context	= context;
				if(MA_OK == (rc = ma_msgbus_subscriber_create(MA_CONTEXT_GET_MSGBUS(self->g_context), &self->sensor_subscriber))) {
				    (void)ma_msgbus_subscriber_setopt(self->sensor_subscriber, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
				    (void)ma_msgbus_subscriber_setopt(self->sensor_subscriber, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_INPROC);
				    self->nw_conn_status = MA_NETWORK_CONNECTIVITY_STATUS_CONNECTED;
				    maloop = ma_msgbus_get_event_loop(MA_CONTEXT_GET_MSGBUS(self->g_context));
				    if(MA_OK == (rc = ma_spipe_url_request_create(self->g_context, &self->spipe_url_request))) {
				        if(MA_OK == (rc = ma_spipe_sitelist_manager_create(self->g_context, &self->sitelist_manager))) {
                            if(MA_OK == (rc = ma_spipe_key_manager_create(context, &self->key_manager))) {
                                ma_bytebuffer_t *byte_buffer = NULL;
                                ma_event_loop_get_loop_impl(maloop, &self->uv_loop);
				                /*Setup the url request handler.*/
				                ma_spipe_url_request_set_url_provider(self->spipe_url_request, ma_spipe_sitelist_manager_get_ah_site_url_provider(self->sitelist_manager));
				                ma_spipe_url_request_set_connect_cb(self->spipe_url_request, spipe_site_connect_callback);
				                ma_spipe_url_request_set_final_cb(self->spipe_url_request, spipe_connection_response_callback);

                                /* Save server key hash in the datastore */
                                if(MA_OK == (rc = ma_crypto_get_server_public_key_hash(MA_CONTEXT_GET_CRYPTO(self->g_context), &byte_buffer))) {
                                    ma_configurator_t *configurator = MA_CONTEXT_GET_CONFIGURATOR(self->g_context);
                                    ma_ds_t *ds = ma_configurator_get_datastore(configurator);
                                    (void)ma_ds_set_str(ds, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_SERVER_KEY_HASH_STR, (char*)ma_bytebuffer_get_bytes(byte_buffer), ma_bytebuffer_get_size(byte_buffer));

                                    ma_context_add_object_info(self->g_context, MA_OBJECT_AH_CLIENT_NAME_STR, (void *)&self->self);
                                    MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_INFO, "Ah client service configured.");
                                    return MA_OK;
                                }
                                MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_ERROR, "Failed to get server public key hash, %d.", rc);
                                ma_spipe_key_manager_release(self->key_manager);
                            }
                            MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_ERROR, "Failed to get spipe key manager, %d.", rc);
                            ma_spipe_sitelist_manager_release(self->sitelist_manager);
                        }
                        MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_ERROR, "Failed to get create sitelist manager, %d.", rc);
                        ma_spipe_url_request_release(self->spipe_url_request);
                    }
                    MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_ERROR, "Failed to get msgbus sensor subscriber, %d.", rc);
                    ma_msgbus_subscriber_release(self->sensor_subscriber);
                }
                self->g_context	= NULL;
                MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_ERROR, "Failed to configure the ah client service, %d", rc);
				return rc;
			}
			else{
				MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_ERROR, "Failed to configure the ah client service, context missing some objects.");
				return MA_ERROR_PRECONDITION;
			}
		}
		else{
			ma_spipe_url_request_set_proxy_config(self->spipe_url_request, NULL);
			handle_relay_policy(self);
		}
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_ah_client_service_start(ma_ah_client_service_t *self){
	if(self){
		if(check_context_validity(self->g_context)){
			ma_error_t rc = MA_OK;
			uv_timer_init(self->uv_loop, &self->spipe_mux_trigger);
			self->spipe_mux_trigger.data = self;
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_DEBUG, "uv timer data(%p)\n", self);
			self->ah_service_state = MA_AH_CS_STATE_START;

            ma_ah_client_service_register_spipe_decorator(self, ma_spipe_key_manager_get_decorator(self->key_manager));
		    ma_ah_client_service_register_spipe_handler(self, ma_spipe_key_manager_get_handler(self->key_manager));
			ma_ah_client_service_register_spipe_decorator(self, ma_spipe_sitelist_manager_get_decorator(self->sitelist_manager));
			ma_ah_client_service_register_spipe_handler(self, ma_spipe_sitelist_manager_get_handler(self->sitelist_manager));
			if(MA_OK != (rc = ma_msgbus_subscriber_register(self->sensor_subscriber, MA_NETWORK_SENSOR_CONNECTIVITY_STATUS_CHANGE_PUB_TOPIC_STR, ma_on_sensor_msg, self))) {
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_INFO, "subscription to %s failed. error = %d", MA_NETWORK_SENSOR_CONNECTIVITY_STATUS_CHANGE_PUB_TOPIC_STR, rc);
			} 

			handle_relay_policy(self);
			return MA_OK;
		}
		return MA_ERROR_PRECONDITION;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_ah_client_service_stop(ma_ah_client_service_t *self){
	if(self){
		uv_timer_stop(&self->spipe_mux_trigger);
		uv_close((uv_handle_t *)&self->spipe_mux_trigger, NULL);

		ma_spipe_alert_list_deinit(&self->spipe_alerts);
		if(MA_AH_CS_STATE_SPIPE_SEND == self->ah_service_state){
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_DEBUG, "Cancelling active spipe connection.");
			self->cancel_spipe_connection = MA_TRUE;
			(void) ma_spipe_url_request_cancel(self->spipe_url_request); 	/*TBD: should we add the sync return.*/
		}

		ma_ah_client_service_unregister_spipe_decorator(self, ma_spipe_sitelist_manager_get_decorator(self->sitelist_manager));
		ma_ah_client_service_unregister_spipe_handler(self, ma_spipe_sitelist_manager_get_handler(self->sitelist_manager));
        ma_ah_client_service_unregister_spipe_decorator(self, ma_spipe_key_manager_get_decorator(self->key_manager));
		ma_ah_client_service_unregister_spipe_handler(self, ma_spipe_key_manager_get_handler(self->key_manager));
		ma_msgbus_subscriber_unregister(self->sensor_subscriber);

		self->ah_service_state = MA_AH_CS_STATE_STOP;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

void ma_ah_client_service_destroy(ma_ah_client_service_t *self){
	if(self){
		if(IS_AH_STOPPED(self->ah_service_state)){
			ma_spipe_alert_list_deinit(&self->spipe_alerts);
			ma_spipe_mux_deinit(&self->spipe_multiplexer);
			ma_spipe_demux_deinit(&self->spipe_demultiplexer);

            ma_spipe_key_manager_release(self->key_manager);
			ma_spipe_sitelist_manager_release(self->sitelist_manager);
			ma_spipe_url_request_release(self->spipe_url_request);
			if(self->sensor_subscriber) ma_msgbus_subscriber_release(self->sensor_subscriber);
			free(self);
			return;
		}
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_ERROR, "Ah client service isn't stopped, can't destroy it.");
		return;
	}
}

ma_error_t ma_ah_client_service_send_uninstall_package(ma_ah_client_service_t *self){
	/*lets send the uninstall package.*/
	if(self){
		uv_timer_stop(&self->spipe_mux_trigger);
		ma_spipe_alert_list_deinit(&self->spipe_alerts);

		if(MA_AH_CS_STATE_SPIPE_SEND == self->ah_service_state){
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_DEBUG, "Cancelling active spipe connection.");
			self->cancel_spipe_connection = MA_TRUE;
			(void) ma_spipe_url_request_cancel(self->spipe_url_request); 	/*TBD: should we add the sync return.*/
		}

		MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_DEBUG, "Sending agent uninstall package to ePO");			
		ma_spipe_alert_list_push(&self->spipe_alerts, STR_PKGTYPE_AGENT_UNINSTALL_RESPONSE, MA_TRUE);
		prepare_spipe_package(self, self->spipe_url_request);

		if(MA_OK == self->spipe_error){
			ma_error_t err = MA_OK;
			self->ah_service_state = MA_AH_CS_STATE_SPIPE_SEND;
			ma_spipe_url_request_sync_send(self->spipe_url_request, self);
		}
		return MA_OK;	
	}
	return MA_ERROR_INVALIDARG;
}

static void spipe_connection_response_callback(ma_error_t status, int response_code, void *userdata, ma_spipe_url_request_t *spipereq){
	ma_error_t err = MA_OK;
	ma_spipe_response_t spipe_response;
	ma_ah_client_service_t *self = (ma_ah_client_service_t*)userdata;

	/* demux the spipe response. */
	MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_INFO, "Spipe connection response received, network return code = %d, response code %d.", status, response_code);

	spipe_response.net_service_error_code = status;
	spipe_response.http_response_code = response_code;
	spipe_response.respond_spipe_pkg = NULL;
	
	/* Not interested on reponse package if response code is 506 */
	if(506 != response_code) {		
		if(MA_OK != (err = ma_spipe_demux_deserialize_spipe_package(&self->spipe_demultiplexer, MA_CONTEXT_GET_CRYPTO(self->g_context),  MA_CONTEXT_GET_LOGGER(self->g_context), ma_spipe_url_request_get_downstream(spipereq), &spipe_response.respond_spipe_pkg) ) )
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_ERROR, "Spipe package deserialiazation failed, error=%d.",err);
	}
	else
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_TRACE, "Spipe package deserialiazation skipping as received response code is 506.");

	/*
	TBD: Socket close was causing the crash, so delay the sipe url request release in the next connection request.
	Network library should have a proper fix for it. Temporary commenting the release here.
	ma_spipe_url_request_deinit(spipereq);
	*/
	(void)ma_spipe_url_request_cleanup_streams(self->spipe_url_request);

	if(!(IS_AH_STOPPED(self->ah_service_state)))
		self->ah_service_state = MA_AH_CS_STATE_SPIPE_DEMUX;

	/* Now start the demux where we can accept the further high priority spipe alert.*/
	if(MA_OK != (err = ma_spipe_demux_start(&self->spipe_demultiplexer, &spipe_response) ) )
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_ERROR, "Spipe connection response demultiplexing failed, error=%d.",err);

	if(spipe_response.respond_spipe_pkg)
		(void)ma_spipe_package_release(spipe_response.respond_spipe_pkg);

	if(self->cancel_spipe_connection || IS_AH_STOPPED(self->ah_service_state)){
		ma_spipe_url_request_deinit(self->spipe_url_request);		
		self->cancel_spipe_connection = MA_FALSE;
		return;
	}
	self->ah_service_state = MA_AH_CS_STATE_START;
	schedule_spipe_connection(self);
}

static void prepare_spipe_package(ma_ah_client_service_t *self, ma_spipe_url_request_t *spipereq){
	ma_stream_t *upstream = NULL, *downstream = NULL;
	self->spipe_error = MA_ERROR_OUTOFMEMORY;
	(void)ma_mstream_create(1024, spipereq, &upstream);
	(void)ma_mstream_create(1024, spipereq, &downstream);
	if(upstream && downstream){
		ma_spipe_alert_node_t *alert = NULL;
		self->spipe_error = MA_ERROR_AH_INTERNAL;

		/* create streams */
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_DEBUG, "Starting spipe package creation.");
		ma_spipe_url_request_set_upstream(spipereq, upstream);
		ma_spipe_url_request_set_downstream(spipereq, downstream);

		/* pop the alert from front and process it */
		alert = ma_spipe_alert_list_pop(&self->spipe_alerts);
		if(alert){
			/* Start spipe secoration. */
			if ( MA_OK == (self->spipe_error = ma_spipe_mux_start(&self->spipe_multiplexer, self->spipe_priority, alert->package_type, MA_CONTEXT_GET_CRYPTO(self->g_context), MA_CONTEXT_GET_LOGGER(self->g_context), ma_spipe_url_request_get_upstream(spipereq) ) ) ) {
				ma_stream_seek(ma_spipe_url_request_get_upstream(spipereq), 0);
				ma_stream_seek(ma_spipe_url_request_get_downstream(spipereq), 0);
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_DEBUG, "Successfully created spipe package.");
			}
			else
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_ERROR, "Failed to create spipe package, error=%d.", self->spipe_error);
			ma_spipe_alert_node_free(alert);
		}
	}
	else{
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_ERROR, "Failed to create io streams, error=%d.", self->spipe_error);
		(void)ma_stream_release(upstream);
		(void)ma_stream_release(downstream);
	}
}

static ma_error_t spipe_site_connect_callback(void *userdata, ma_spipe_url_request_t *spipereq){
	ma_ah_client_service_t *self = (ma_ah_client_service_t*)userdata;
	return self->spipe_error;
}

static void spipe_connection_trigger_callback(uv_timer_t *spipe_mux_trigger, int status){
	ma_error_t err = MA_OK;
	ma_ah_client_service_t *self = (ma_ah_client_service_t*)spipe_mux_trigger->data;
	MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_INFO, "Start processing spipe connection request.");
	if(CHECK_ACTIVE_SPIPE(self->ah_service_state))
		return;
	uv_timer_stop(&self->spipe_mux_trigger);

	/*prepare spipe package.*/
	self->ah_service_state = MA_AH_CS_STATE_SPIPE_MUX;
	self->spipe_error      = MA_OK;
	prepare_spipe_package(self, self->spipe_url_request);

	/*send spipe package.*/
	if(MA_OK == self->spipe_error){
		self->ah_service_state = MA_AH_CS_STATE_SPIPE_SEND;
		if( MA_OK == (err = ma_spipe_url_request_send(self->spipe_url_request, self) ) ){
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_DEBUG, "Started spipe connection request");
			return;
		}
		else{
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_ERROR, "Failed to process the spipe connection request, error=%d.", err);
		}
	}
	spipe_connection_response_callback(err, -1, self, self->spipe_url_request);
}

static void schedule_spipe_connection(ma_ah_client_service_t *self){
	if(CHECK_ACTIVE_SPIPE(self->ah_service_state))
		return;

	if(!self->spipe_alerts.alertlist_count){
		ma_spipe_url_request_deinit(self->spipe_url_request);
		return;
	}

	if(ma_spipe_url_request_is_connected(self->spipe_url_request))
		self->spipe_priority = MA_SPIPE_PRIORITY_IMMEDIATE;
	else
		self->spipe_priority = ma_spipe_mux_get_spipe_priority(&self->spipe_multiplexer);

	if(self->spipe_priority){
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_INFO, "Scheduling spipe connection with \"%s\" priority.", MA_SPIPE_PRIORITY_IMMEDIATE == self->spipe_priority ? "immediate" : "normal");
	}else{
		ma_spipe_alert_list_clear(&self->spipe_alerts);
	}

	if(MA_SPIPE_PRIORITY_IMMEDIATE == self->spipe_priority){
		switch(self->ah_service_state){
			case MA_AH_CS_STATE_SPIPE_SCHEDULED:
				uv_timer_stop(&self->spipe_mux_trigger);
			case MA_AH_CS_STATE_START:
				uv_timer_start(&self->spipe_mux_trigger, spipe_connection_trigger_callback, IMMEDIATE_SPIPE_TRIGGER_TIMEOUT, 0);
			case MA_AH_CS_STATE_SPIPE_IMMEDIATE_SCHEDULED:
				self->ah_service_state = MA_AH_CS_STATE_SPIPE_IMMEDIATE_SCHEDULED;
		}
	}
	else if(MA_SPIPE_PRIORITY_NORMAL == self->spipe_priority){
		switch(self->ah_service_state){
			case MA_AH_CS_STATE_SPIPE_IMMEDIATE_SCHEDULED :
				uv_timer_stop(&self->spipe_mux_trigger);
			case MA_AH_CS_STATE_START :
				uv_timer_start(&self->spipe_mux_trigger, spipe_connection_trigger_callback, NORMAL_SPIPE_TRIGGER_TIMEOUT, 0);
			case MA_AH_CS_STATE_SPIPE_SCHEDULED :
				self->ah_service_state = MA_AH_CS_STATE_SPIPE_SCHEDULED;
		}
	}
	else{
		switch(self->ah_service_state){
			case MA_AH_CS_STATE_SPIPE_IMMEDIATE_SCHEDULED :
			case MA_AH_CS_STATE_SPIPE_SCHEDULED :
				uv_timer_stop(&self->spipe_mux_trigger);
			case MA_AH_CS_STATE_START:
				self->ah_service_state = MA_AH_CS_STATE_START;
		}
	}
}

ma_error_t ma_ah_client_service_reload_sitelist(ma_ah_client_service_t *self){
	if(self){
		ma_ah_site_url_provider_load_sitelist(ma_spipe_sitelist_manager_get_ah_site_url_provider(self->sitelist_manager));
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_ah_client_service_reload_proxy(ma_ah_client_service_t *self){
	if(self){
		ma_spipe_url_request_set_proxy_config(self->spipe_url_request, NULL);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_ah_client_service_get_sitelist(ma_ah_client_service_t *self, const ma_ah_sitelist_t **sitelist){
	if(self && sitelist){
		*sitelist = ma_ah_site_url_provider_get_sitelist(ma_spipe_sitelist_manager_get_ah_site_url_provider(self->sitelist_manager));
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_ah_client_service_register_spipe_decorator(ma_ah_client_service_t *self, ma_spipe_decorator_t *sdecorator){
	if(self && sdecorator){
		return ma_spipe_mux_add_spipe_decorator(&self->spipe_multiplexer, sdecorator);
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_ah_client_service_unregister_spipe_decorator(ma_ah_client_service_t *self, ma_spipe_decorator_t *sdecorator){
	if(self && sdecorator){
		ma_spipe_mux_remove_spipe_decorator(&self->spipe_multiplexer, sdecorator);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_ah_client_service_register_spipe_handler(ma_ah_client_service_t *self, ma_spipe_handler_t *shandler){
	if(self && shandler){
		return ma_spipe_demux_add_spipe_handler(&self->spipe_demultiplexer, shandler);
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_ah_client_service_unregister_spipe_handler(ma_ah_client_service_t *self, ma_spipe_handler_t *shandler){
	if(self && shandler){
		ma_spipe_demux_remove_spipe_handler(&self->spipe_demultiplexer, shandler);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t ma_ah_client_service_raise_spipe_alert(ma_ah_client_service_t *self, char const *package_type){
	if(self && package_type){
		MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_DEBUG, "raise alert requested");
		if(self->nw_conn_status != MA_NETWORK_CONNECTIVITY_STATUS_CONNECTED) {
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_WARNING, "no network available. raise alert request is ignored...");
			return MA_ERROR_AH_NO_NETWORK;
		}
		if(IS_AH_STARTED(self->ah_service_state)){
			/* Alert as part of demux, so give it highest priority*/
			
			if( ((MA_AH_CS_STATE_SPIPE_DEMUX == self->ah_service_state) && !((0 == strcmp(package_type, STR_PKGTYPE_MSG_UPLOAD)) || (0 == strcmp(package_type, STR_PKGTYPE_MSG_REQUEST))))
				|| (0 == strcmp(package_type, STR_PKGTYPE_AGENT_UNINSTALL_RESPONSE)) ) {
				ma_spipe_alert_list_push(&self->spipe_alerts, package_type, MA_TRUE);
				return MA_OK;
			}
			ma_spipe_alert_list_push(&self->spipe_alerts, package_type, MA_FALSE);
			schedule_spipe_connection(self);
			return MA_OK;
		}
		return MA_ERROR_AH_INTERNAL;
	}
	return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_on_sensor_msg(const char *topic, ma_message_t *message, void *cb_data) {
	ma_ah_client_service_t * self = (ma_ah_client_service_t *)cb_data;
	ma_error_t rc = MA_ERROR_INVALIDARG;

	if(self && topic && message) {
		rc = MA_OK;

		MA_LOG(MA_CONTEXT_GET_LOGGER((self->g_context)), MA_LOG_SEV_DEBUG, "received sensor message");

		/* this check may not be required, after all we are subscribed only for this topic */
		if(!strncmp(topic, MA_NETWORK_SENSOR_CONNECTIVITY_STATUS_CHANGE_PUB_TOPIC_STR, strlen(MA_NETWORK_SENSOR_CONNECTIVITY_STATUS_CHANGE_PUB_TOPIC_STR))) {
			ma_variant_t *payload = NULL;
			ma_network_sensor_msg_t *nw_sensor_msg = NULL;

			ma_message_get_payload(message, &payload);
			convert_variant_to_network_sensor_msg(payload, &nw_sensor_msg);

			if(MA_OK != (rc = ma_network_sensor_msg_get_connectivity_status(nw_sensor_msg, &self->nw_conn_status))) {
				MA_LOG(MA_CONTEXT_GET_LOGGER((self->g_context)), MA_LOG_SEV_ERROR, "error getting the network change status");
			}
			MA_LOG(MA_CONTEXT_GET_LOGGER((self->g_context)), MA_LOG_SEV_DEBUG, "current status of network = %s", self->nw_conn_status == MA_NETWORK_CONNECTIVITY_STATUS_CONNECTED ? "UP" : "DOWN");
			ma_network_sensor_msg_release(nw_sensor_msg);
			ma_variant_release(payload);
		}
	}
	return rc;
}


ma_error_t ma_ah_client_service_get_last_connection(ma_ah_client_service_t *self,const ma_url_request_t **url_request){
	if(self && url_request){
		*url_request = ma_spipe_url_request_get_url_request(self->spipe_url_request);
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

