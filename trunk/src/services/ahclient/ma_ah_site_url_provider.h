#ifndef MA_AH_SITE_URL_PROVIDER_H_INCLUDED
#define MA_AH_SITE_URL_PROVIDER_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/ma_macros.h"
#include "ma/ma_datastore.h"
#include "ma/internal/services/ahclient/ma_ah_sitelist.h"
#include "ma/internal/utils/network/ma_net_client_service.h"
#include "ma/internal/utils/network/ma_url_request.h"
#include "ma/internal/defs/ma_object_defs.h"

MA_CPP(extern "C" {)

typedef struct ma_ah_site_url_provider_s ma_ah_site_url_provider_t;

ma_error_t ma_ah_site_url_provider_create(ma_context_t *context, ma_ah_site_url_provider_t **self);

void ma_ah_site_url_provider_release(ma_ah_site_url_provider_t *self);

void ma_ah_site_url_provider_load_sitelist(ma_ah_site_url_provider_t *self);

ma_ah_sitelist_t *ma_ah_site_url_provider_get_sitelist(ma_ah_site_url_provider_t *self);

void ma_ah_site_url_provider_cache_current_site(ma_ah_site_url_provider_t *self);

const char *ma_ah_site_url_provider_get_next_url(ma_ah_site_url_provider_t *self, ma_bool_t is_current_url_reachable);

void ma_ah_site_url_provider_reset_url(ma_ah_site_url_provider_t *self);

MA_CPP(})

#endif //end

