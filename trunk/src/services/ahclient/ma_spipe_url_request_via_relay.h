#ifndef MA_SPIPE_URL_REQUEST_VIA_RELAY_H_INCLUDED
#define MA_SPIPE_URL_REQUEST_VIA_RELAY_H_INCLUDED

#include "ma_spipe_url_request_internal.h"

void spipe_url_request_send_via_relay(ma_spipe_url_request_t *self);

#endif /*MA_SPIPE_URL_REQUEST_VIA_RELAY_H_INCLUDED*/
