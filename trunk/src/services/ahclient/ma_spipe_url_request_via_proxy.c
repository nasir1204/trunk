#include "ma/repository/ma_repository_client.h"
#include "ma_spipe_url_request_via_proxy.h"
#include "ma_spipe_url_request_via_relay.h"

#include "ma/internal/defs/ma_io_defs.h"
#include "ma/internal/utils/proxy/ma_proxy_internal.h"

#include "ma/ma_msgbus.h"
#include "ma/ma_message.h"

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "ahclient"

extern ma_bool_t is_connection_to_next_site_needed(ma_spipe_url_request_t *request, ma_error_t status, int response_code, ma_bool_t *is_current_url_reachable);

void spipe_url_request_via_proxy_final_cb(ma_error_t status, long response_code, void *userdata, ma_url_request_t *net_request){
	ma_bool_t is_current_url_reachable = MA_TRUE;
	ma_spipe_url_request_t *request = (ma_spipe_url_request_t*)userdata;

	if(request->is_cancelled){
		MA_LOG(MA_CONTEXT_GET_LOGGER(request->g_context), MA_LOG_SEV_INFO, "Spipe connection to site %s cancelled.",request->current_url);
		call_ah_final_cb(status, response_code, request, MA_FALSE);
		return;
	}

	if(is_connection_to_next_site_needed(request, status, response_code, &is_current_url_reachable) ){				
		/*failed try with next URL.*/
		while(NULL != (request->current_url = ma_ah_site_url_provider_get_next_url(request->spipe_site_url_provider, MA_FALSE))){

			if(ma_spipe_url_request_get_proxy_list(request, request->current_url)){
				ma_error_t err = MA_OK;
				(void)ma_url_request_set_proxy_info(request->url_request, request->proxy_list);
				(void)ma_url_request_set_url(request->url_request, request->current_url);
				MA_LOG(MA_CONTEXT_GET_LOGGER(request->g_context), MA_LOG_SEV_INFO, "Initiating spipe connection via proxy to site %s.", request->current_url);

				if(MA_OK == (err = ma_url_request_send(request->url_request, spipe_url_request_via_proxy_final_cb, request))){
					MA_LOG(MA_CONTEXT_GET_LOGGER(request->g_context), MA_LOG_SEV_INFO, "Initiating spipe connection via proxy to site %s success.", request->current_url);
					return;				
				}
				else
					MA_LOG(MA_CONTEXT_GET_LOGGER(request->g_context), MA_LOG_SEV_ERROR, "Spipe connection to site vai proxy %s failed.", request->current_url);
			}
			else
				MA_LOG(MA_CONTEXT_GET_LOGGER(request->g_context), MA_LOG_SEV_DEBUG, "No proxy found for site %s.", request->current_url);
		}	
		/*failed with proxies so lets try via relay.*/
		(void)ma_spipe_url_request_set_proxy(request, NULL);
		spipe_url_request_send_via_relay(request);		
	}
	else{ 
		/*successful connection*/
		ma_proxy_list_t *new_list = 0;	
		/*Set last used proxy to use later on.*/
		if(MA_OK == ma_proxy_list_create(&new_list)){
			ma_int32_t last_used_relay_index = 0;
			ma_proxy_t *last_used_proxy = NULL;

			(void)ma_proxy_list_get_last_proxy_index(request->proxy_list, &last_used_relay_index);
			if(MA_OK == ma_proxy_list_get_proxy(request->proxy_list, last_used_relay_index, &last_used_proxy)){
				(void)ma_proxy_list_add_proxy(new_list, last_used_proxy);
				(void)ma_proxy_release(last_used_proxy); last_used_proxy = NULL;
				(void)ma_spipe_url_request_set_proxy(request, new_list);				
			}
			else
				(void)ma_proxy_list_release(new_list);
		}
		/*call success*/
		call_ah_final_cb(status, response_code, request, MA_TRUE);		
	}
}

ma_bool_t ma_spipe_url_request_is_proxy_enabled(ma_spipe_url_request_t *self){
	ma_bool_t rc_b = MA_FALSE;
	if(self && self->proxy_config){
		ma_error_t rc = MA_OK;		
		ma_proxy_usage_type_t usage = MA_PROXY_USAGE_TYPE_NONE;
		(void)ma_proxy_config_get_proxy_usage_type(self->proxy_config, &usage);

		if(MA_PROXY_USAGE_TYPE_SYSTEM_CONFIGURED == usage){
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_DEBUG, "Proxy configuration setting \"Use System Proxy\"");
			rc_b = MA_TRUE;
		}				
		else if(MA_PROXY_USAGE_TYPE_SERVER_CONFIGURED == usage){
			ma_proxy_list_t *list = NULL;
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_DEBUG, "Proxy configuration setting \"Use policy configured proxy\"");
				
			(void)ma_proxy_config_get_proxy_list(self->proxy_config, &list);
			if(list){
				ma_spipe_url_request_set_proxy(self, list);
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_DEBUG, "Use the proxy set in policy");
				rc_b = MA_TRUE;
			}
		}
		else{
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_DEBUG, "No proxy usage.");
			rc_b = MA_FALSE;
		}
	}
	return rc_b;
}

ma_bool_t ma_spipe_url_request_get_proxy_list(ma_spipe_url_request_t *self, const char *url){
	if(self->proxy_config){
		ma_error_t rc = MA_OK;
		ma_proxy_usage_type_t usage = MA_PROXY_USAGE_TYPE_NONE;
		ma_proxy_config_get_proxy_usage_type(self->proxy_config, &usage);

		if(MA_PROXY_USAGE_TYPE_SYSTEM_CONFIGURED == usage){
			ma_proxy_list_t *list = NULL;
			if(MA_OK == (rc = ma_proxy_config_detect_system_proxy(self->proxy_config, MA_CONTEXT_GET_LOGGER(self->g_context), url, &list))){
				ma_spipe_url_request_set_proxy(self, list);
				return MA_TRUE;
			}
		}				
		else if(MA_PROXY_USAGE_TYPE_SERVER_CONFIGURED == usage){
			if(self->proxy_list)
				return MA_TRUE;
		}
		return MA_FALSE;
	}
	return MA_FALSE;
}

static void try_spipe_url_request_via_proxy(ma_spipe_url_request_t *self){
	if(self){
		if(self->proxy_config) {
			(void)ma_spipe_url_request_set_proxy(self, NULL);

			if(ma_spipe_url_request_is_proxy_enabled(self)){		
			
				(void)ma_ah_site_url_provider_reset_url(self->spipe_site_url_provider);
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_DEBUG, "Trying to make the spipe connection via proxy.");

				while(NULL != (self->current_url = ma_ah_site_url_provider_get_next_url(self->spipe_site_url_provider, MA_FALSE))){
					ma_error_t err = MA_OK;
					if(ma_spipe_url_request_get_proxy_list(self, self->current_url)){

						(void)ma_url_request_set_proxy_info(self->url_request, self->proxy_list);
						(void)ma_url_request_set_url(self->url_request, self->current_url);
						MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_INFO, "Initiating spipe connection to site %s via proxy.", self->current_url);

						if(MA_OK == (err = ma_url_request_send(self->url_request, spipe_url_request_via_proxy_final_cb, self))){
							MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_INFO, "Initiated spipe connection to site %s via proxy.", self->current_url);
							return;				
						}
						else
							MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_ERROR, "Spipe connection to site %s vai proxy failed.", self->current_url);	
					}
					else
						MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_DEBUG, "No proxy found for site %s.", self->current_url);		
				}					
				spipe_url_request_send_via_relay(self);
				return;
			}
			else{
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_DEBUG, "Proxy configuration can't be used for spipe connection, trying with relay.");		
				spipe_url_request_send_via_relay(self);
				return;
			}
		}
		spipe_url_request_send_via_relay(self);
	}
}

void ma_spipe_url_request_set_proxy_config(ma_spipe_url_request_t *self, ma_proxy_config_t *config){
	if(self){
		if(self->proxy_config)
			ma_proxy_config_release(self->proxy_config);
		if(MA_CONNECTION_VIA_PROXY == self->method) 
			ma_spipe_url_request_set_proxy(self, NULL);
		self->proxy_config = config;
	}
}

static ma_error_t proxy_config_get_cb(ma_error_t status, ma_client_t *ma_client, const ma_proxy_config_t *proxy_config, void *cb_data) {
	ma_spipe_url_request_t *self = (ma_spipe_url_request_t *)cb_data;
	if(MA_OK == status) {
		ma_proxy_config_t *config = NULL;
		if(MA_OK == ma_proxy_config_copy((ma_proxy_config_t *)proxy_config, &config)) {
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_INFO, "Using the new proxy config.");
			ma_spipe_url_request_set_proxy_config(self, config);
		}
	}
	try_spipe_url_request_via_proxy(self);
	return MA_OK;
}


void spipe_url_request_send_via_proxy(ma_spipe_url_request_t *self){
	if(self){
		if(self->proxy_config){
			try_spipe_url_request_via_proxy(self);
			return;
		}
		else {
			ma_error_t rc = MA_OK;
			MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_DEBUG, "Get the new proxy config from agent policy.");
			if(MA_OK != (rc = ma_repository_client_async_get_proxy_config(MA_CONTEXT_GET_CLIENT(self->g_context), proxy_config_get_cb, self)))         
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->g_context), MA_LOG_SEV_ERROR, "Failed to get proxy config from policies, rc <%d>", rc);
		}
	}
}