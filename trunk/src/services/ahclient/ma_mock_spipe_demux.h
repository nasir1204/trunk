#include <stdio.h>
#include <stdlib.h>

#include "ma/internal/utils/spipe/ma_spipe_package.h"
#include "ma/internal/utils/macrypto/ma_crypto.h"
#include "ma/internal/services/ahclient/ma_spipe_handler.h"
#include "ma/internal/utils/datastructures/ma_slist.h"

MA_CPP(extern "C" {)

typedef struct ma_spipe_handler_node_s ma_spipe_handler_node_t;
struct ma_spipe_handler_node_s{
	ma_spipe_handler_t *handler;
	ma_slist_node_define(ma_spipe_handler_node_t);
};

typedef struct ma_spipe_demux_s{
	ma_slist_define(hlist,ma_spipe_handler_node_t);	
	ma_spipe_package_t *pkg;
}ma_spipe_demux_t;

static MA_INLINE void ma_spipe_demux_init(ma_spipe_demux_t *self){
	ma_slist_init(self,hlist);	
}
static MA_INLINE void ma_spipe_demux_deinit(ma_spipe_demux_t *self){
	ma_spipe_handler_node_t *node=NULL;	
	for ( node = self->hlist_head ; node != NULL ; ){
		ma_spipe_handler_node_t *current=node;
		node=node->p_link;
		(void)ma_spipe_handler_release(current->handler);		
		free(current);
	}
	ma_slist_init(self,hlist);	
}

static MA_INLINE ma_error_t ma_spipe_demux_add_spipe_handler(ma_spipe_demux_t *self, ma_spipe_handler_t *shandler){
	ma_spipe_handler_node_t *node=(ma_spipe_handler_node_t*)calloc(1,sizeof(ma_spipe_handler_node_t) );
	if(node){		
		node->handler=shandler;
		(void)ma_spipe_handler_add_ref(shandler);
		ma_slist_push_back(self,hlist,node);		
		return MA_OK;
	}
	return MA_ERROR_OUTOFMEMORY;
}

static MA_INLINE void ma_spipe_demux_remove_spipe_handler(ma_spipe_demux_t *self, ma_spipe_handler_t *shandler){
	ma_spipe_handler_node_t *current=NULL,*prev=NULL;	
	for ( prev=NULL, current = self->hlist_head ; current != NULL ; prev=current,current=current->p_link){		
		if(current->handler == shandler){
			(prev) ? (prev->p_link=current->p_link) : (self->hlist_head=current->p_link);			
			(void)ma_spipe_handler_release(current->handler);		
			free(current);
			if(!self->hlist_head){
				ma_slist_init(self,hlist);	
			}
			break;
		}
	}							
}

static ma_error_t ma_spipe_demux_deserialize_spipe_package(ma_spipe_demux_t *self, ma_crypto_t *crypto, ma_stream_t *stream, ma_spipe_package_t **spipepkg){
	if(self->pkg){
		*spipepkg = self->pkg;
		self->pkg = NULL;
	}
	return MA_OK;
}

static ma_error_t ma_spipe_demux_start(ma_spipe_demux_t *self, ma_spipe_response_t *response){
	ma_error_t error=MA_OK;
	ma_spipe_handler_node_t *node=NULL;
	ma_slist_foreach(self,hlist,node){
		(void)ma_spipe_handler_on_spipe_response(node->handler,response);					
	}	
	return error;	
}

MA_CPP(})
