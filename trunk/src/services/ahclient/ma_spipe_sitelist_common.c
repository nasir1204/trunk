#include "ma_spipe_sitelist_common.h"

#include <string.h>
#include <stdlib.h>


#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "ahclient"

/*TBD: pass host size */
void split_host_and_port(const char *host_port, char *host, ma_uint32_t *port) {
    int i = 0;
    while(host_port[i] && (host_port[i++] != ':'));
    
    /* If there is no ':' in input string, return the same string as host name and port number to 80 */
    if(i == strlen(host_port)) {
        strncpy(host, host_port, strlen(host_port));
        *port = 80;
        return;
    }

    /* TBD - Assume that the host has the sufficient length t ocopy the host name */
    strncpy(host, host_port, i-1);
    host[i-1] = '\0';
    *port = atoi(host_port+i);
    return;
}

