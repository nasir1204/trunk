#ifndef MA_SPIPE_KEY_MANAGER_H_INCLUDED
#define MA_SPIPE_KEY_MANAGER_H_INCLUDED

#include "ma/ma_msgbus.h"
#include "ma/internal/services/ahclient/ma_ah_client_service.h"
#include "ma/internal/defs/ma_object_defs.h"

/* This is derived from ma_spipe_decorator and ma_spipe_handler */
typedef struct ma_spipe_key_manager_s ma_spipe_key_manager_t, *ma_spipe_key_manager_h;

ma_error_t ma_spipe_key_manager_create(ma_context_t *context, ma_spipe_key_manager_t **spipe_key_manager);

ma_spipe_decorator_t *ma_spipe_key_manager_get_decorator(ma_spipe_key_manager_t *spipe_key_manager);

ma_spipe_handler_t *ma_spipe_key_manager_get_handler(ma_spipe_key_manager_t *spipe_key_manager);

ma_error_t ma_spipe_key_manager_set_logger(ma_spipe_key_manager_t *spipe_key_manager, ma_logger_t *logger);

ma_error_t ma_spipe_key_manager_release(ma_spipe_key_manager_t *spipe_key_manager);

#endif  /* MA_SPIPE_KEY_MANAGER_H_INCLUDED */

