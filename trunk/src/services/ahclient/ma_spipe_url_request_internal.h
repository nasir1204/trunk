#ifndef MA_SPIPE_URL_REQUEST_INTERNAL_H_INCLUDED
#define MA_SPIPE_URL_REQUEST_INTERNAL_H_INCLUDED

#include "ma_spipe_url_request.h"
#include "ma/proxy/ma_proxy_config.h"

#define MA_SPIPE_CONNECT_TIMEOUT						(30)	  /*30 secs*/
#define MA_SPIPE_TRANSFER_TIMEOUT						(30 * 60) /*30 mins*/

#define MA_SPIPE_CONNECT_TIMEOUT_ATUNINSTALL			(10)	/*10 secs*/
#define MA_SPIPE_TRANSFER_TIMEOUT_ATUNINSTALL			(30)    /*30 secs*/

MA_CPP(extern "C" {)

typedef enum ma_connection_method_e ma_connection_method_t;
enum ma_connection_method_e {
	MA_CONNECTION_DIRECT = 0 ,
	MA_CONNECTION_VIA_PROXY  ,
	MA_CONNECTION_VIA_RELAY
};

struct ma_spipe_url_request_s {
	ma_context_t                 *g_context;
	ma_ah_site_url_provider_t	 *spipe_site_url_provider;
	ma_url_request_io_t			 io_streams;
	ma_url_request_ssl_info_t	 ssl_info;
	ma_url_request_auth_t		 url_auth;
	on_spipe_response			 final_cb;
	on_spipe_site_connect		 site_connect_cb;
	ma_url_request_t			 *url_request;
	const char					 *current_url;
	ma_bool_t					 is_cancelled;
	ma_bool_t					 is_connected;
	ma_bool_t					 relay_enabled;
	void						 *userdata;
	ma_proxy_list_t				 *proxy_list;
	ma_proxy_config_t			 *proxy_config;
	ma_connection_method_t		 method;
};

void call_ah_final_cb(ma_error_t status, long response_code, ma_spipe_url_request_t *spipereq, ma_bool_t is_success);
void spipe_url_request_final_cb(ma_error_t status, long response_code, void *userdata, ma_url_request_t *net_request);
void spipe_url_request_via_relay_final_cb(ma_error_t status, long response_code, void *userdata, ma_url_request_t *net_request);
void spipe_url_request_via_proxy_final_cb(ma_error_t status, long response_code, void *userdata, ma_url_request_t *net_request);

MA_CPP(})

#endif
