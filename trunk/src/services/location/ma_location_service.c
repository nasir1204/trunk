#include "ma/internal/services/location/ma_location_service.h"
#include "ma/internal/utils/context/ma_context.h"
#include "ma/internal/defs/ma_location_defs.h"
#include "ma/internal/services/ma_service.h"
#include "ma/ma_log.h"
#include "ma/ma_message.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/ma_variant.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/sensor/ma_sensor_msg.h"
#include "ma/sensor/ma_sensor_utils.h"
#include "ma/internal/utils/location/ma_location.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

const char base_path[] = "/location/";

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "location service"

MA_CPP(extern "C" {)
MA_CPP(})

struct ma_location_service_s {
    ma_service_t            service_base;
    ma_context_t            *context;
    ma_msgbus_server_t      *server;
    ma_logger_t             *logger;
    ma_msgbus_subscriber_t  *subscriber;
    ma_location_t             *location;
};


static ma_error_t location_subscriber_cb(const char *topic, ma_message_t *message, void *cb_data);
static ma_error_t ma_location_service_cb(ma_msgbus_server_t *server, ma_message_t *msg, void *cb_data, ma_msgbus_client_request_t *c_request);

static ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint);
static ma_error_t service_start(ma_service_t *service);
static ma_error_t service_stop(ma_service_t *service);
static void service_release(ma_service_t *service);

static const ma_service_methods_t service_methods = {
    &service_configure,
    &service_start,
    &service_stop,
    &service_release
};

ma_error_t ma_location_service_create(const char *service_name, ma_service_t **service) {
    if(service_name && service) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;

        if((*service = (ma_service_t*)calloc(1, sizeof(ma_location_service_t)))) {
            ma_service_init(*service, &service_methods, service_name, service);
            return MA_OK;
        }
        return err ;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_location_service_release(ma_location_service_t *self) {
    if(self) {
        ma_error_t err = MA_OK;
        if(self->server && (MA_OK != (err = ma_msgbus_server_release(self->server)))) {
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "msgbus server release failed");
        }
        (void)ma_msgbus_subscriber_release(self->subscriber);
        free(self) ;
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_bool_t check_context_validity(ma_context_t *context){	
    /*Will see if we need this*/
    return MA_TRUE;
}

ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint) {
    if(service && context) {
        ma_error_t rc = MA_OK;
        if(MA_SERVICE_CONFIG_NEW == hint && check_context_validity(context)) {
            ma_configurator_t *configurator = MA_CONTEXT_GET_CONFIGURATOR(context);
            ma_location_service_t *self = (ma_location_service_t *)service;
            ma_msgbus_t *msgbus = MA_CONTEXT_GET_MSGBUS(context);

            self->context = context;
            self->logger = MA_CONTEXT_GET_LOGGER(context);

            if(!ma_configurator_get_location_datastore(configurator))
                 MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_INFO, "location datastore does not exist");
            if(MA_OK == (rc = ma_location_create(msgbus, ma_configurator_get_location_datastore(configurator), MA_LOCATION_BASE_PATH_STR, &self->location))) {
                (void)ma_location_set_logger(self->logger);
                if(MA_OK == (rc = ma_msgbus_subscriber_create(MA_CONTEXT_GET_MSGBUS(context), &self->subscriber))) {
                    (void)ma_msgbus_subscriber_setopt(self->subscriber, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                    (void)ma_msgbus_subscriber_setopt(self->subscriber, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
                    if(MA_OK == (rc = ma_msgbus_server_create(msgbus, MA_LOCATION_SERVICE_NAME_STR, &self->server))) {                        
                        (void)ma_msgbus_server_setopt(self->server, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
                        if(MA_OK == (rc = ma_msgbus_server_setopt(self->server, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC))) {
                            /*TODO - No need to add the location object in the context if we can use the location client API's */
                            ma_context_add_object_info(context, MA_OBJECT_LOCATION_NAME_STR, (void *const *)&self->location);
                            MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_DEBUG, "location service configuration successful");
                            return MA_OK;
                        }
                        ma_msgbus_server_release(self->server);
                    }
                    ma_msgbus_subscriber_release(self->subscriber);
                }
            }
            else
                MA_LOG(self->logger, MA_LOG_SEV_ERROR, "location creation failed, last error(%d)", rc);
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t service_start(ma_service_t *service) {
    if(service) {
        ma_error_t err = MA_OK;
        ma_location_service_t *self = (ma_location_service_t *)service ;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);
        
        if(MA_OK == (err = ma_location_start(self->location))) {
            if(MA_OK == (err = ma_msgbus_server_start(((ma_location_service_t*)service)->server, ma_location_service_cb, service))) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "location service started successfully");
                if(MA_OK == (err = ma_msgbus_subscriber_register(((ma_location_service_t*)service)->subscriber,  "ma.task.*", location_subscriber_cb, service))) {
                    MA_LOG(((ma_location_service_t*)service)->logger, MA_LOG_SEV_DEBUG, "location subscriber registered successfully");
                    return MA_OK ;
                } 
                else 
                    MA_LOG(((ma_location_service_t*)service)->logger, MA_LOG_SEV_ERROR, "location service failed to subscribe topic, errocode(%d)", err);
                ma_msgbus_server_stop(((ma_location_service_t*)service)->server);
            }
            else
                MA_LOG(logger, MA_LOG_SEV_CRITICAL, "location service start failed, last error(%d)", err);
            ma_location_stop(self->location);
        }
        else
            MA_LOG(logger, MA_LOG_SEV_CRITICAL, "location service start failed, last error(%d)", err);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_location_service_get_version(ma_uint32_t *version) {
    if(version) {
        *version = 0x05020100; //TODO fetch version 
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_location_service_start(ma_location_service_t *service) {
    return service_start((ma_service_t*)service);
}

ma_error_t ma_location_service_stop(ma_location_service_t *service) {
    return service_stop((ma_service_t*)service);
}

ma_error_t service_stop(ma_service_t *service) {
    if(service) {
        ma_error_t err = MA_OK;
        ma_location_service_t *self = (ma_location_service_t *)service;

        if(MA_OK != (err = ma_msgbus_subscriber_unregister(((ma_location_service_t*)service)->subscriber))) {
            MA_LOG(((ma_location_service_t*)service)->logger, MA_LOG_SEV_ERROR, "location service unregistering subscriber failed, last error(%d)", err);
        }

        if(MA_OK == (err = ma_msgbus_server_stop(self->server))) {
            if(MA_OK != (err = ma_location_stop(self->location))) 
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_CRITICAL, "location stop failed, last error(%d)", err);
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO, "location service stopped successfully");
        }
        else
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_CRITICAL, "location service stop failed, last error(%d)", err);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

void service_release(ma_service_t *service) {
    if(service) {
        ma_location_service_release((ma_location_service_t *)service) ;
        return ;
    }
    return ;
}

ma_error_t ma_location_service_get_location(ma_location_service_t *service, ma_location_t **location) {
    if(service && location) {
        *location = service->location;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t order_create_handler(ma_location_service_t *service, ma_message_t *req_msg, ma_message_t *rsp_msg) {
    if(service && req_msg && rsp_msg) {
        ma_error_t err = MA_OK;
        const char *location_id = NULL;
        char *status = MA_LOCATION_ID_NOT_EXIST;
        ma_variant_t *payload = NULL;
        char error_code[MA_MAX_LEN] = {0};
        ma_location_info_t *info = NULL;

        if(MA_OK == (err = ma_message_get_property(req_msg, MA_LOCATION_ID, &location_id))) {
            (void)ma_message_set_property(rsp_msg, MA_LOCATION_ID, location_id);
            MA_LOG(service->logger, MA_LOG_SEV_TRACE, "location id(%s) create order processing", location_id);
            if(MA_OK == (err = ma_message_get_payload(req_msg, &payload))) {
                if(MA_OK == (err = ma_location_info_convert_from_variant(payload, &info))) {
                    char id[MA_MAX_NAME+1] = {0};

                    //(void)ma_location_info_get_email_id(info, id);
                    (void)ma_location_info_get_pincode(info, id);
                    if(MA_OK == (err = ma_location_add_variant(service->location, location_id, payload))) {
                        status = MA_LOCATION_ID_WRITTEN;
                        MA_LOG(service->logger, MA_LOG_SEV_TRACE, "location_id(%s) crn(%s) written into DB successfully", location_id, id);

                    }
                    (void)ma_location_info_release(info);
                }
                (void)ma_variant_release(payload);
            }
        }
        if(MA_OK != err)
            MA_LOG(service->logger, MA_LOG_SEV_CRITICAL, "location order creation failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(rsp_msg, MA_LOCATION_STATUS, status);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t order_modify_handler(ma_location_service_t *service, ma_message_t *req_msg, ma_message_t *rsp_msg) {
    if(service && req_msg && rsp_msg) {
        ma_error_t err = MA_OK;
        const char *location_id = NULL;
        char *status = MA_LOCATION_ID_NOT_EXIST;
        ma_variant_t *payload = NULL;
        char error_code[MA_MAX_LEN] = {0};
        ma_location_info_t *info = NULL;

        if(MA_OK == (err = ma_message_get_property(req_msg, MA_LOCATION_ID, &location_id))) {
            (void)ma_message_set_property(rsp_msg, MA_LOCATION_ID, location_id);
            MA_LOG(service->logger, MA_LOG_SEV_TRACE, "location id(%s) order modify request  processing", location_id);
            if(MA_OK == (err = ma_message_get_payload(req_msg, &payload))) {
                if(MA_OK == (err = ma_location_info_convert_from_variant(payload, &info))) {
                    char id[MA_MAX_NAME+1] = {0};

                    //(void)ma_location_info_get_email_id(info, id);
                    (void)ma_location_info_get_pincode(info, id);
                    if(MA_OK == (err = ma_location_add_variant(service->location, location_id, payload))) {
                        status = MA_LOCATION_ID_WRITTEN;
                        MA_LOG(service->logger, MA_LOG_SEV_TRACE, "location_id(%s) crn(%s) written into DB successfully", location_id, id);

                    }
                    (void)ma_location_info_release(info);
                }
                (void)ma_variant_release(payload);
            }
        }
        if(MA_OK != err)
            MA_LOG(service->logger, MA_LOG_SEV_CRITICAL, "location order modify failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(rsp_msg, MA_LOCATION_STATUS, status);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t order_delete_handler(ma_location_service_t *service, ma_message_t *req_msg, ma_message_t *rsp_msg) {
    if(service && req_msg && rsp_msg) {
        ma_error_t err = MA_OK;
        const char *location_id = NULL;
        const char *crn = NULL;
        char *status = MA_LOCATION_ID_NOT_EXIST;
        char error_code[MA_MAX_LEN] = {0};

        if(MA_OK == (err = ma_message_get_property(req_msg, MA_LOCATION_ID, &location_id)) &&
           MA_OK == (err = ma_message_get_property(req_msg, MA_LOCATION_CRN, &crn))) {
            (void)ma_message_set_property(rsp_msg, MA_LOCATION_ID, location_id);
            (void)ma_message_set_property(rsp_msg, MA_LOCATION_CRN, crn);
            MA_LOG(service->logger, MA_LOG_SEV_TRACE, "location id(%s) crn(%s) order delete request processing", location_id, crn);
            if(MA_OK == (err = ma_location_delete_crn(service->location, location_id, crn))) {
                status = MA_LOCATION_ID_DELETED;
                MA_LOG(service->logger, MA_LOG_SEV_TRACE, "location id(%s) crn (%s) deleted successfully", location_id, crn);
            } else {
                MA_LOG(service->logger, MA_LOG_SEV_ERROR, "location id(%s) crn(%s) order delete failed, last error(%d)", location_id, crn ,err);
                status = MA_LOCATION_ID_NOT_EXIST;
            }
        }
        if(MA_OK != err)
            MA_LOG(service->logger, MA_LOG_SEV_CRITICAL, "location order delete failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(rsp_msg, MA_LOCATION_STATUS, status);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t order_update_status_handler(ma_location_service_t *service, ma_message_t *req_msg, ma_message_t *rsp_msg) {
    if(service && req_msg && rsp_msg) {
        ma_error_t err = MA_OK;
        const char *location_id = NULL;
        const char *crn = NULL;
        char *status = MA_LOCATION_ID_NOT_EXIST;
        char error_code[MA_MAX_LEN] = {0};
        const char *location_status = NULL;

        if(MA_OK == (err = ma_message_get_property(req_msg, MA_LOCATION_ID, &location_id)) &&
           MA_OK == (err = ma_message_get_property(req_msg, MA_LOCATION_CRN, &crn)) &&
           MA_OK == (err = ma_message_get_property(req_msg, MA_LOCATION_STATUS, &location_status))) {
            ma_location_info_t *info = NULL;

            (void)ma_message_set_property(rsp_msg, MA_LOCATION_ID, location_id);
            (void)ma_message_set_property(rsp_msg, MA_LOCATION_CRN, crn);
            MA_LOG(service->logger, MA_LOG_SEV_TRACE, "location id(%s) crn(%s) order update request processing", location_id, crn);
            if(MA_OK == (err = ma_location_get(service->location, location_id, &info))) {
                status = MA_LOCATION_ID_EXIST;
                //TODO : update status
                MA_LOG(service->logger, MA_LOG_SEV_TRACE, "location id(%s) crn (%s) deleted successfully", location_id, crn);
                (void)ma_location_info_release(info);
            } else {
                MA_LOG(service->logger, MA_LOG_SEV_ERROR, "location id(%s) crn(%s) order delete failed, last error(%d)", location_id, crn ,err);
                status = MA_LOCATION_ID_NOT_EXIST;
            }
        }
        if(MA_OK != err)
            MA_LOG(service->logger, MA_LOG_SEV_CRITICAL, "location order delete failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(rsp_msg, MA_LOCATION_STATUS, status);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t location_validate_pincode_handler(ma_location_service_t *service, ma_message_t *req_msg, ma_message_t *rsp_msg) {
    if(service && req_msg && rsp_msg) {
        ma_error_t err = MA_OK;
        const char *location_id = NULL;
        const char *contact = NULL;
        char *status = MA_LOCATION_ID_NOT_EXIST;
        char error_code[MA_MAX_LEN] = {0};

        if(MA_OK == (err = ma_message_get_property(req_msg, MA_LOCATION_ID, &location_id))) {
            ma_location_info_t *linfo = NULL;

            (void)ma_message_set_property(rsp_msg, MA_LOCATION_ID, location_id);
            MA_LOG(service->logger, MA_LOG_SEV_TRACE, "location id(%s) validate pincode request processing", location_id);
            if(MA_OK == (err = ma_location_get(service->location, location_id, &linfo))) {
                status = MA_LOCATION_ID_EXIST;
                (void)ma_location_info_release(linfo);
            } else {
                MA_LOG(service->logger, MA_LOG_SEV_ERROR, "location id(%s) get operation failed last error(%d)", location_id, err);
                status = MA_LOCATION_ID_NOT_EXIST;
            }
        }
        if(MA_OK != err)
            MA_LOG(service->logger, MA_LOG_SEV_CRITICAL, "location id validate pincode failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(rsp_msg, MA_LOCATION_STATUS, status);
        return err = MA_OK; // force reset
    }
    return MA_ERROR_INVALIDARG;
}
static ma_error_t location_delete_handler(ma_location_service_t *service, ma_message_t *req_msg, ma_message_t *rsp_msg) {
    if(service && req_msg && rsp_msg) {
        ma_error_t err = MA_OK;
        const char *location_id = NULL;
        const char *contact = NULL;
        char *status = MA_LOCATION_ID_NOT_EXIST;
        char error_code[MA_MAX_LEN] = {0};

        if(MA_OK == (err = ma_message_get_property(req_msg, MA_LOCATION_ID, &location_id)) &&
        MA_OK == (err = ma_message_get_property(req_msg, MA_LOCATION_CRN, &contact))) {
            ma_location_info_t *linfo = NULL;

            (void)ma_message_set_property(rsp_msg, MA_LOCATION_ID, location_id);
            MA_LOG(service->logger, MA_LOG_SEV_TRACE, "location id(%s) delete request processing", location_id);
            if(MA_OK == (err = ma_location_get(service->location, location_id, &linfo))) {
                if(MA_OK == (err = ma_location_info_remove_contact(linfo, location_id, contact))) {
                    if(MA_OK == (err = ma_location_add(service->location, linfo))) {
                        status = MA_LOCATION_ID_DELETED;
                        MA_LOG(service->logger, MA_LOG_SEV_TRACE, "location id(%s) contact(%s) deleted successfully", location_id, contact);
                    } else {
                        MA_LOG(service->logger, MA_LOG_SEV_ERROR, "location id(%s) contact(%s) delete failed last error(%d)", location_id, contact, err);
                        status = MA_LOCATION_ID_NOT_EXIST;
                    }
                } else {
                    MA_LOG(service->logger, MA_LOG_SEV_ERROR, "location id(%s) contact(%s) delete failed last error(%d)", location_id, contact, err);
                    status = MA_LOCATION_ID_NOT_EXIST;
                }
                (void)ma_location_info_release(linfo);
            } else {
                MA_LOG(service->logger, MA_LOG_SEV_ERROR, "location id(%s) contact(%s) delete failed last error(%d)", location_id, contact, err);
                status = MA_LOCATION_ID_NOT_EXIST;
            }
        }
        if(MA_OK != err)
            MA_LOG(service->logger, MA_LOG_SEV_CRITICAL, "location id contact delete failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(rsp_msg, MA_LOCATION_STATUS, status);
        return err = MA_OK; // force reset
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t location_update_handler(ma_location_service_t *service, ma_message_t *location_req_msg, ma_message_t *location_rsp_msg) {
    if(service && location_req_msg && location_rsp_msg) {
        ma_error_t err = MA_OK;
        const char *location_id = NULL;
        char *location_reg_status = MA_TASK_REPLY_STATUS_FAILED;
        ma_variant_t *location_variant = NULL;
        char error_code[MA_MAX_LEN] = {0};
        const char *contact = NULL;


        (void)ma_message_set_property(location_rsp_msg, MA_LOCATION_ID, location_id);
        if(MA_OK == (err = ma_message_get_property(location_req_msg, MA_LOCATION_ID, &location_id)) &&
           MA_OK == (err = ma_message_get_property(location_req_msg, MA_LOCATION_CRN, &contact))
          ) {
            ma_location_info_t *linfo = NULL;

            location_reg_status = MA_LOCATION_ID_NOT_EXIST;
            if(MA_OK == ma_location_get(service->location, location_id, &linfo)) {
                if(MA_OK == (err = ma_location_info_remove_contact(linfo, location_id, contact))) {
                    if(MA_OK == (err = ma_location_info_convert_to_variant(linfo, &location_variant))) {
                        if(MA_OK == (err = ma_location_add_variant(service->location, location_id, location_variant))) {
                            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "location id(%s) updated into DB successfully", location_id);
                            location_reg_status = MA_LOCATION_ID_WRITTEN;
                        }
                        (void)ma_variant_release(location_variant);
                    }
                }
            }
        }
        if(MA_OK != err)
            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_CRITICAL, "location id(%s) update failed, last error(%d)", location_id, err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(location_rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(location_rsp_msg, MA_LOCATION_STATUS, location_reg_status);
        return err = MA_OK; // force reset
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t location_register_handler(ma_location_service_t *service, ma_message_t *location_req_msg, ma_message_t *location_rsp_msg) {
    if(service && location_req_msg && location_rsp_msg) {
        ma_error_t err = MA_OK;
        const char *location_id = NULL;
        char *location_reg_status = MA_TASK_REPLY_STATUS_FAILED;
        ma_variant_t *location_variant = NULL;
        char error_code[MA_MAX_LEN] = {0};
        const char *contact = NULL;


        (void)ma_message_set_property(location_rsp_msg, MA_LOCATION_ID, location_id);
        if(MA_OK == (err = ma_message_get_property(location_req_msg, MA_LOCATION_ID, &location_id)) &&
           MA_OK == (err = ma_message_get_property(location_req_msg, MA_LOCATION_CRN, &contact))
          ) {
            ma_location_info_t *linfo = NULL;

            location_reg_status = MA_LOCATION_ID_NOT_EXIST;
            if(MA_OK == ma_location_get(service->location, location_id, &linfo)) {
                if(MA_OK == (err = ma_location_info_add_contact(linfo, location_id, contact))) {
                    if(MA_OK == (err = ma_location_info_convert_to_variant(linfo, &location_variant))) {
                        if(MA_OK == (err = ma_location_add_variant(service->location, location_id, location_variant))) {
                            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "location id(%s) written into DB successfully", location_id);
                            location_reg_status = MA_LOCATION_ID_WRITTEN;
                        }
                        (void)ma_variant_release(location_variant);
                    }
                }
            } else {
                if(MA_OK == (err = ma_message_get_payload(location_req_msg, &location_variant))) {
                    if(MA_OK == (err = ma_location_add_variant(service->location, location_id, location_variant))) {
                        MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "location id(%s) written into DB successfully", location_id);
                        location_reg_status = MA_LOCATION_ID_WRITTEN;
                    }
                    (void)ma_variant_release(location_variant);
                }
            }
        }
        if(MA_OK != err)
            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_CRITICAL, "location id register failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(location_rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(location_rsp_msg, MA_LOCATION_STATUS, location_reg_status);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t location_deregister_handler(ma_location_service_t *service, ma_message_t *location_req_msg, ma_message_t *location_rsp_msg) {
    if(service && location_req_msg && location_rsp_msg) {
        ma_error_t err = MA_OK;
        const char *location_id = NULL;
        char *location_reg_status = MA_LOCATION_ID_NOT_EXIST;
        char error_code[MA_MAX_LEN] = {0};


        (void)ma_message_set_property(location_rsp_msg, MA_LOCATION_ID, location_id);
        if(MA_OK == (err = ma_message_get_property(location_req_msg, MA_LOCATION_ID, &location_id))) {
            if(MA_OK == (err = ma_location_delete(service->location, location_id))) {
                location_reg_status = MA_LOCATION_ID_DELETED;
                MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "location id(%s) deleted successfully", location_id);
            }
        }
        if(MA_OK != err)
            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_CRITICAL, "location id deregister failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(location_rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(location_rsp_msg, MA_LOCATION_STATUS, location_reg_status);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_location_service_cb(ma_msgbus_server_t *server, ma_message_t *msg, void *cb_data, ma_msgbus_client_request_t *c_request) {
    if(server && msg && cb_data) {
        ma_error_t err = MA_OK;
        const char *msg_type = NULL;
        ma_message_t *response = NULL;
        ma_location_service_t *service = (ma_location_service_t*)cb_data;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(service->context);

        MA_LOG(logger, MA_LOG_SEV_DEBUG, "Location service callback invoked");
        if(MA_OK == (err = ma_message_create(&response))) {
            if(MA_OK == (err = ma_message_get_property(msg, MA_LOCATION_SERVICE_MSG_TYPE, &msg_type))) {
                if(!strcmp(msg_type, MA_LOCATION_SERVICE_MSG_UPDATE)) {
                    if(MA_OK == (err = location_update_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client location update request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_LOCATION_SERVICE_MSG_ORDER_CREATE_TYPE)) {
                    if(MA_OK == (err = order_create_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client order create request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_LOCATION_SERVICE_MSG_ORDER_MODIFY_TYPE)) {
                    if(MA_OK == (err = order_modify_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client order modify request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_LOCATION_SERVICE_MSG_REGISTER)) {
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "Location register request received");
                    if(MA_OK == (err = location_register_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client order modify request failed, last error(%d)", err);
                        }
                    } else
                        MA_LOG(logger, MA_LOG_SEV_ERROR, "location register request failed, last error(%d)", err);
                } else if(!strcmp(msg_type, MA_LOCATION_SERVICE_MSG_DEREGISTER)) {
                    if(MA_OK == (err = location_deregister_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client order modify request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_LOCATION_SERVICE_MSG_ORDER_DELETE_TYPE)) {
                    if(MA_OK == (err = order_delete_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client order delete request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_LOCATION_SERVICE_MSG_ORDER_UPDATE_STATUS_TYPE)) {
                    if(MA_OK == (err = order_update_status_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client order update request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_LOCATION_SERVICE_MSG_LOCATION_DELETE_CONTACT)) {
                    if(MA_OK == (err = location_delete_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client location contact delete request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_LOCATION_SERVICE_MSG_VALIDATE_PINCODE_TYPE)) {
                    if(MA_OK == (err = location_validate_pincode_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client location contact delete request failed, last error(%d)", err);
                        }
                    }
                } else {
                    if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, MA_ERROR_SCHEDULER_INVALID_OP_TYPE, response))) {
                        MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client request failed, last error(%d)", err);
                    }
                }
            } else {
                if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err = MA_ERROR_SCHEDULER_TASK_TYPE_NOT_SET, response))) {
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client request failed, last error(%d)", err);
                }
            }
            if(MA_OK != ma_message_release(response)) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client request message release failed");
            }
        }
        return err;
    }
    return MA_OK;
}

ma_error_t location_subscriber_cb(const char *topic, ma_message_t *message, void *cb_data) {
    if(topic && message && cb_data) {
        ma_error_t err = MA_OK;
        ma_location_service_t *service = (ma_location_service_t*)cb_data;
        //ma_location_t *location = service->location;

        MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "location subscriber received sensor message(%s)", topic);
        if(!strcmp(topic, MA_TASK_EXECUTE_PUBSUB_TOPIC) || !strcmp(topic, MA_TASK_STOP_PUBSUB_TOPIC) || !strcmp(MA_TASK_REMOVE_PUBSUB_TOPIC, topic) || !strcmp(topic, MA_TASK_UPDATE_STATUS_PUBSUB_TOPIC)) {
            const char *task_type = NULL , *task_owner_id = NULL , *task_id = NULL;

            MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "location processing (%s) message", topic);
            if( (MA_OK == (err = ma_message_get_property(message, MA_TASK_TYPE, &task_type))) && task_type &&
                (MA_OK == (err = ma_message_get_property(message, MA_TASK_PRODUCT_ID, &task_owner_id))) && task_owner_id &&
                (MA_OK == (err = ma_message_get_property(message, MA_TASK_ID, &task_id))) && task_id) {
                if(!strcmp(task_type, MA_BOOKING_TASK_TYPE)) {
                    MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "location task id(%s) processing request received", task_id);
                } else
                    MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "task id(%s) is not location task", task_id);
            }
        } else {
            /* do nothing */
        }


        return err;
    }
    return MA_ERROR_INVALIDARG;
}


