#include "ma/internal/services/board/ma_board_service.h"
#include "ma/internal/utils/context/ma_context.h"
#include "ma/internal/utils/board/ma_board.h"
#include "ma/internal/utils/board/ma_board_info.h"
#include "ma/internal/utils/board/ma_board_pins.h"
#include "ma/internal/utils/board/ma_board_topics.h"
#include "ma/internal/utils/season/ma_season_info.h"
#include "ma/internal/utils/season/ma_season.h"
#include "ma/internal/defs/ma_board_defs.h"
#include "ma/internal/services/ma_service.h"
#include "ma/ma_log.h"
#include "ma/ma_message.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/ma_variant.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/sensor/ma_sensor_msg.h"
#include "ma/sensor/ma_sensor_utils.h"
#include "ma/ma_message.h"
#include "ma/internal/ma_strdef.h"
#include "ma/scheduler/ma_task.h"
#include "ma/scheduler/ma_triggers.h"
#include "ma/scheduler/ma_enumerator.h"
#include "ma/scheduler/ma_triggerlist.h"
#include "ma/internal/services/scheduler/ma_scheduler_core.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

const char base_path[] = "/board/";

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "board service"

MA_CPP(extern "C" {)
MA_CPP(})

struct ma_board_service_s {
    ma_service_t            service_base;
    ma_context_t            *context;
    ma_msgbus_server_t      *server;
    ma_logger_t             *logger;
    ma_msgbus_subscriber_t  *subscriber;
    ma_board_t           *board;
    ma_msgbus_t             *msgbus;
};


static ma_error_t board_subscriber_cb(const char *topic, ma_message_t *message, void *cb_data);
static ma_error_t ma_board_service_cb(ma_msgbus_server_t *server, ma_message_t *msg, void *cb_data, ma_msgbus_client_request_t *c_request);

static ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint);
static ma_error_t service_start(ma_service_t *service);
static ma_error_t service_stop(ma_service_t *service);
static void service_release(ma_service_t *service);

static const ma_service_methods_t service_methods = {
    &service_configure,
    &service_start,
    &service_stop,
    &service_release
};

ma_error_t ma_board_service_create(const char *service_name, ma_service_t **service) {
    if(service_name && service) {
        ma_error_t err = MA_ERROR_OUTOFMEMORY;

        if((*service = (ma_service_t*)calloc(1, sizeof(ma_board_service_t)))) {
            ma_service_init(*service, &service_methods, service_name, service);
            return MA_OK;
        }
        return err ;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_service_release(ma_board_service_t *self) {
    if(self) {
        ma_error_t err = MA_OK;
        if(self->server && (MA_OK != (err = ma_msgbus_server_release(self->server)))) {
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "msgbus server release failed");
        }
        (void)ma_msgbus_subscriber_release(self->subscriber);
        (void)ma_board_release(self->board);
        free(self) ;
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_bool_t check_context_validity(ma_context_t *context){	
    /*Will see if we need this*/
    return MA_TRUE;
}

ma_error_t service_configure(ma_service_t *service, ma_context_t *context, unsigned hint) {
    if(service && context) {
        ma_error_t rc = MA_OK;
        if(MA_SERVICE_CONFIG_NEW == hint && check_context_validity(context)) {
            ma_configurator_t *configurator = MA_CONTEXT_GET_CONFIGURATOR(context);
            ma_board_service_t *self = (ma_board_service_t *)service;
            ma_msgbus_t *msgbus = MA_CONTEXT_GET_MSGBUS(context);

            self->context = context;
            self->logger = MA_CONTEXT_GET_LOGGER(context);
            self->msgbus = msgbus;

            (void)ma_board_set_logger(self->logger);
            if(MA_OK == (rc = ma_board_create(msgbus, ma_configurator_get_board_datastore(configurator), MA_BOARD_BASE_PATH_STR, &self->board))) {
                MA_LOG(self->logger, MA_LOG_SEV_DEBUG, "board configured successfully");
                if(MA_OK == (rc = ma_msgbus_subscriber_create(MA_CONTEXT_GET_MSGBUS(context), &self->subscriber))) {
                    (void)ma_msgbus_subscriber_setopt(self->subscriber, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
                    (void)ma_msgbus_subscriber_setopt(self->subscriber, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
                    if(MA_OK == (rc = ma_msgbus_server_create(msgbus, MA_BOARD_SERVICE_NAME_STR, &self->server))) {                        
                        (void)ma_msgbus_server_setopt(self->server, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO);
                        if(MA_OK == (rc = ma_msgbus_server_setopt(self->server, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC))) {
                            /*TODO - No need to add the board object in the context if we can use the board client API's */
                            ma_context_add_object_info(context, MA_OBJECT_BOARD_NAME_STR, (void *const *)&self->board);
                            (void)ma_board_set_context(context);
                            MA_LOG(MA_CONTEXT_GET_LOGGER(context), MA_LOG_SEV_DEBUG, "board service configuration successful");
                            return MA_OK;
                        }
                        ma_msgbus_server_release(self->server);
                    }
                    ma_msgbus_subscriber_release(self->subscriber);
                }
            }
            else
                MA_LOG(self->logger, MA_LOG_SEV_ERROR, "board creation failed, last error(%d)", rc);
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t service_start(ma_service_t *service) {
    if(service) {
        ma_error_t err = MA_OK;
        ma_board_service_t *self = (ma_board_service_t *)service ;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(self->context);
        
        if(MA_OK == (err = ma_board_start(self->board))) {
            if(MA_OK == (err = ma_msgbus_server_start(((ma_board_service_t*)service)->server, ma_board_service_cb, service))) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "board service started successfully");
                if(MA_OK == (err = ma_msgbus_subscriber_register(((ma_board_service_t*)service)->subscriber,  "ma.task.*", board_subscriber_cb, service))) {
                    MA_LOG(((ma_board_service_t*)service)->logger, MA_LOG_SEV_DEBUG, "board subscriber registered successfully");
                    return MA_OK ;
                } 
                else 
                    MA_LOG(((ma_board_service_t*)service)->logger, MA_LOG_SEV_ERROR, "board service failed to subscribe topic, errocode(%d)", err);
                ma_msgbus_server_stop(((ma_board_service_t*)service)->server);
            }
            else
                MA_LOG(logger, MA_LOG_SEV_CRITICAL, "board service start failed, last error(%d)", err);
            (void)ma_board_stop(self->board);
        }
        else
            MA_LOG(logger, MA_LOG_SEV_CRITICAL, "board service start failed, last error(%d)", err);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_service_get_msgbus(ma_board_service_t *service, ma_msgbus_t **msgbus) {
    if(service && msgbus) {
        *msgbus = service->msgbus;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_service_get_version(ma_uint32_t *version) {
    if(version) {
        *version = 0x05020100; //TODO fetch version 
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


ma_error_t ma_board_service_start(ma_board_service_t *service) {
    return service_start((ma_service_t*)service);
}

ma_error_t ma_board_service_stop(ma_board_service_t *service) {
    return service_stop((ma_service_t*)service);
}

ma_error_t service_stop(ma_service_t *service) {
    if(service) {
        ma_error_t err = MA_OK;
        ma_board_service_t *self = (ma_board_service_t *)service;

        if(MA_OK != (err = ma_msgbus_subscriber_unregister(((ma_board_service_t*)service)->subscriber))) {
            MA_LOG(((ma_board_service_t*)service)->logger, MA_LOG_SEV_ERROR, "board service unregistering subscriber failed, last error(%d)", err);
        }

        if(MA_OK == (err = ma_msgbus_server_stop(self->server))) {
            if(MA_OK != (err = ma_board_stop(self->board))) 
                MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_CRITICAL, "board stop failed, last error(%d)", err);
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_INFO, "board service stopped successfully");
        }
        else
            MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_CRITICAL, "board service stop failed, last error(%d)", err);

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

void service_release(ma_service_t *service) {
    if(service) {
        ma_board_service_release((ma_board_service_t *)service) ;
        return ;
    }
    return ;
}

static ma_error_t board_register_handler(ma_board_service_t *service, ma_message_t *board_req_msg, ma_message_t *board_rsp_msg) {
    if(service && board_req_msg && board_rsp_msg) {
        ma_error_t err = MA_OK;
        const char *board_id = NULL;
        char *board_reg_status = MA_TASK_REPLY_STATUS_FAILED;
        ma_board_info_t *info = NULL;
        ma_variant_t *board_variant = NULL;
        char error_code[MA_MAX_LEN] = {0};


        (void)ma_message_set_property(board_rsp_msg, MA_BOARD_ID, board_id);
        if(MA_OK == (err = ma_message_get_property(board_req_msg, MA_BOARD_ID, &board_id))) {
                board_reg_status = MA_BOARD_ID_NOT_EXIST;
                if(MA_OK == (err = ma_board_get(service->board, board_id, &info))) {
                    board_reg_status = MA_BOARD_ID_EXIST;
                    MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "board id(%s) already exist!", board_id);
                    (void)ma_board_info_release(info);
                } else {
                   if(MA_OK == ma_message_get_payload(board_req_msg, &board_variant)) {
                       if(MA_OK == (err = ma_board_add_variant(service->board, board_id, board_variant))) {
                           MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "board id(%s) written into DB successfully", board_id);
                           board_reg_status = MA_BOARD_ID_WRITTEN;
                       }
                       (void)ma_variant_release(board_variant);
                   }
                }
        }
        if(MA_OK != err)
            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_CRITICAL, "board id register failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(board_rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(board_rsp_msg, MA_BOARD_STATUS, board_reg_status);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t board_deregister_handler(ma_board_service_t *service, ma_message_t *board_req_msg, ma_message_t *board_rsp_msg) {
    if(service && board_req_msg && board_rsp_msg) {
        ma_error_t err = MA_OK;
        const char *board_id = NULL;
        char *board_reg_status = MA_BOARD_ID_NOT_EXIST;
        ma_board_info_t *info = NULL;
        char error_code[MA_MAX_LEN] = {0};


        (void)ma_message_set_property(board_rsp_msg, MA_BOARD_ID, board_id);
        if(MA_OK == (err = ma_message_get_property(board_req_msg, MA_BOARD_ID, &board_id))) {
            if(MA_OK == (err = ma_board_get(service->board, board_id, &info))) {
                board_reg_status = MA_BOARD_ID_EXIST;
                if(MA_OK == (err = ma_board_delete(service->board, board_id))) {
                    board_reg_status = MA_BOARD_ID_DELETED;
                    MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "board id(%s) deleted successfully", board_id);
                }
                (void)ma_board_info_release(info);
            }
        }
        if(MA_OK != err)
            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_CRITICAL, "board id deregister failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(board_rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(board_rsp_msg, MA_BOARD_STATUS, board_reg_status);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t board_get_handler(ma_board_service_t *service, ma_message_t *board_req_msg, ma_message_t *board_rsp_msg) {
    if(service && board_req_msg && board_rsp_msg) {
        ma_error_t err = MA_OK;
        const char *board_id = NULL;
        char *board_reg_status = MA_BOARD_ID_NOT_EXIST;
        ma_board_info_t *info = NULL;
        ma_variant_t *board_variant = NULL;
        char error_code[MA_MAX_LEN] = {0};


        (void)ma_message_set_property(board_rsp_msg, MA_BOARD_ID, board_id);
        if(MA_OK == (err = ma_message_get_property(board_req_msg, MA_BOARD_ID, &board_id))) {
            if(MA_OK == (err = ma_board_get(service->board, board_id, &info))) {
                board_reg_status = MA_BOARD_ID_EXIST;
                MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "board id(%s) get successfully", board_id);
                if(MA_OK == (err = ma_board_info_convert_to_variant(info, &board_variant))) {
                    (void)ma_message_set_payload(board_rsp_msg, board_variant);
                    (void)ma_variant_release(board_variant);
                }
                (void)ma_board_info_release(info);
            }
        }
        if(MA_OK != err)
            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_CRITICAL, "board id get failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(board_rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(board_rsp_msg, MA_BOARD_STATUS, board_reg_status);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static void board_table_cb(const char *key, ma_variant_t *value, void *cb_data, ma_bool_t *stop_loop) {
    ma_board_info_t *info = NULL;
    ma_json_array_t *array = (ma_json_array_t*)cb_data;
    ma_error_t err =  MA_OK;

    if(MA_OK == ma_board_info_convert_from_variant(value, &info)) {
        ma_json_t *json = NULL;

        if(MA_OK == (err = ma_json_create(&json))) {
            const char *image_url = NULL;

            (void)ma_board_info_get_image_url(info, &image_url);
            if(strlen(image_url)) {
                ma_json_add_element(json, key, image_url);
            } else {
                ma_json_add_element(json, key, key);
            }
            (void)ma_json_array_add_object(array, json);
            (void)ma_json_release(json);
        }
       
        (void)ma_board_info_release(info);
    }
}

static ma_error_t board_get_board_selection_handler(ma_board_service_t *service, ma_message_t *board_req_msg, ma_message_t *board_rsp_msg) {
    if(service && board_req_msg && board_rsp_msg) {
        ma_error_t err = MA_OK;
        char *board_reg_status = MA_BOARD_ID_NOT_EXIST;
        ma_board_info_t *info = NULL;
        ma_variant_t *board_variant = NULL;
        char error_code[MA_MAX_LEN] = {0};


        if(MA_OK == (err = ma_board_get_all(service->board, &board_variant))) {
            ma_table_t *table = NULL;

            board_reg_status = MA_BOARD_ID_EXIST;
            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "reading all boards");
            if(MA_OK == (err = ma_variant_get_table(board_variant, &table))) {
                ma_json_array_t *json_array = NULL;

                if(MA_OK == (err = ma_json_array_create(&json_array))) {
                    if(MA_OK == (err = ma_table_foreach(table, board_table_cb, json_array))) {
                        //TODO construct variant 
                        ma_bytebuffer_t *buffer = NULL;

                        ma_bytebuffer_create(MA_MAX_BUFFER_LEN, &buffer);
                        if(MA_OK == (err = ma_json_array_get_data(json_array, buffer))) {
                            ma_variant_t *var = NULL;

                            if(MA_OK == (err = ma_variant_create_from_string((const char*)ma_bytebuffer_get_bytes(buffer), ma_bytebuffer_get_size(buffer), &var))) {
                                (void)ma_message_set_payload(board_rsp_msg, var);
                                board_reg_status = MA_BOARD_ID_WRITTEN;
                                (void)ma_variant_release(var);
                            }
                        }
                    }
                    (void)ma_json_array_release(json_array);
                }
                (void)ma_table_release(table);
            }
            (void)ma_variant_release(board_variant);
        }
        if(MA_OK != err)
            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_CRITICAL, "get all board failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(board_rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(board_rsp_msg, MA_BOARD_STATUS, board_reg_status);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t board_add_handler(ma_board_service_t *service, ma_message_t *board_req_msg, ma_message_t *board_rsp_msg) {
    if(service && board_req_msg && board_rsp_msg) {
        ma_error_t err = MA_OK;
        char *board_reg_status = MA_BOARD_ID_NOT_EXIST;
        ma_board_info_t *info = NULL;
        ma_variant_t *board_variant = NULL;
        ma_variant_t *payload = NULL;
        char error_code[MA_MAX_LEN] = {0};


        if(MA_OK == (err = ma_message_get_payload(board_req_msg, &payload)) && MA_OK == (err = ma_board_get_all(service->board, &board_variant))) {
            ma_table_t *table = NULL;

            board_reg_status = MA_BOARD_ID_EXIST;
            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_TRACE, "reading all boards");
            if(MA_OK == (err = ma_variant_get_table(board_variant, &table))) {
                ma_board_info_t *board = NULL;

                if(MA_OK == (err = ma_board_info_convert_from_variant(payload, &board))) {
                    const char *id = NULL;
                    ma_variant_t *var = NULL;

                    (void)ma_board_info_get_id(board, &id);
                    if(MA_OK == (err = ma_table_get_value(table, id, &var))) {

                        if(MA_OK == (err = ma_board_info_convert_from_variant(var, &info))) {
                            const char *image_url = NULL;

                            (void)ma_board_info_get_image_url(board, &image_url);
                            (void)ma_board_info_set_image_url(info, image_url);
                            if(MA_OK == (err = ma_board_add(service->board, info))) {
                                board_reg_status = MA_BOARD_ID_WRITTEN;
                            }
                            (void)ma_board_info_release(info);
                        }
                        
                        (void)ma_variant_release(var);
                    } else {
                        if(MA_OK == (err = ma_board_add_variant(service->board, id, payload))) {
                            board_reg_status = MA_BOARD_ID_WRITTEN;
                        }
                    }
                    (void)ma_board_info_release(board);
                }
                (void)ma_table_release(table);
            }
            (void)ma_variant_release(payload);
            (void)ma_variant_release(board_variant);
        }
        if(MA_OK != err)
            MA_LOG(MA_CONTEXT_GET_LOGGER(service->context), MA_LOG_SEV_CRITICAL, "add board failed, last error(%d)", err);

        MA_MSC_SELECT(_snprintf, snprintf)(error_code, MA_MAX_LEN, "%d", err);
        (void)ma_message_set_property(board_rsp_msg, MA_ERROR_CODE, error_code);

        (void)ma_message_set_property(board_rsp_msg, MA_BOARD_STATUS, board_reg_status);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}
ma_error_t ma_board_service_cb(ma_msgbus_server_t *server, ma_message_t *msg, void *cb_data, ma_msgbus_client_request_t *c_request) {
    if(server && msg && cb_data) {
        ma_error_t err = MA_OK;
        const char *msg_type = NULL;
        ma_message_t *response = NULL;
        ma_board_service_t *service = (ma_board_service_t*)cb_data;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(service->context);

        if(MA_OK == (err = ma_message_create(&response))) {
            if(MA_OK == (err = ma_message_get_property(msg, MA_BOARD_SERVICE_MSG_TYPE, &msg_type))) {
                if(!strcmp(msg_type, MA_BOARD_SERVICE_MSG_REGISTER_TYPE)) {
                    if(MA_OK == (err = board_register_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client register id request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_BOARD_SERVICE_MSG_UNREGISTER_TYPE)) {
                    if(MA_OK == (err = board_deregister_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client deregister request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_BOARD_SERVICE_MSG_GET_TYPE)) {
                    if(MA_OK == (err = board_get_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client get request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_BOARD_SERVICE_MSG_GET_BOARD_SELECTION)) {
                    if(MA_OK == (err = board_get_board_selection_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client board selection request failed, last error(%d)", err);
                        }
                    }
                } else if(!strcmp(msg_type, MA_BOARD_SERVICE_MSG_ADD_BOARD)) {
                    if(MA_OK == (err = board_add_handler(service, msg, response))) {
                        if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err, response))) {
                            MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client add request failed, last error(%d)", err);
                        }
                    }
                } else {
                    if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, MA_ERROR_SCHEDULER_INVALID_OP_TYPE, response))) {
                        MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client request failed, last error(%d)", err);
                    }
                }
            } else {
                if(MA_OK != (err = ma_msgbus_server_client_request_post_result(c_request, err = MA_ERROR_SCHEDULER_TASK_TYPE_NOT_SET, response))) {
                    MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client request failed, last error(%d)", err);
                }
            }
            if(MA_OK != ma_message_release(response)) {
                MA_LOG(logger, MA_LOG_SEV_DEBUG, "msgbus posting client request message release failed");
            }
        }
        return err;
    }
    return MA_OK;
}

static ma_error_t save_board(ma_board_service_t *service, const char *pin_id) {
    if(service && pin_id) {
        ma_context_t *context = service->context;
        ma_error_t err = MA_OK;
        ma_logger_t *logger = MA_CONTEXT_GET_LOGGER(context);
        ma_board_t *dboard = MA_CONTEXT_GET_BOARD(context);
        ma_season_info_t *info = NULL;

        MA_LOG(logger, MA_LOG_SEV_DEBUG, "%s starts", __FUNCTION__);
        if(MA_OK == (err = ma_season_get(MA_CONTEXT_GET_SEASON(context), pin_id, &info))) {
            const char *board = NULL;
            const char *topic = NULL;
            const char *pid = NULL;
            const char *board_image_url = NULL;

            (void)ma_season_info_get_board(info, &board);
            (void)ma_season_info_get_topic(info, &topic);
            (void)ma_season_info_get_id(info, &pid);
            (void)ma_season_info_get_board_image_url(info, &board_image_url);
            if(board && topic && pid && strcmp(board,MA_EMPTY) && strcmp(topic, MA_EMPTY) && strcmp(pid, MA_EMPTY)) {
                ma_board_pin_t *pin = NULL;

                MA_LOG(logger, MA_LOG_SEV_TRACE, "saving pin(%s) under board(%s) topic(%s)", pid, board, topic);
                if(MA_OK == (err = ma_board_pin_create(&pin))) {
                    ma_board_topic_t *t = NULL;
                    char *tmp = strdup(topic);
                    char *p = strtok(tmp, ",");

                    MA_LOG(logger, MA_LOG_SEV_TRACE, "board pin(%s) created successfully", pid);
                    (void)ma_board_pin_set_id(pin, pid);
                    while(p) {
                        (void)ma_board_pin_set_topic(pin, topic = p);
                        if(MA_OK == (err = ma_board_topic_create(&t))) {
                            MA_LOG(logger, MA_LOG_SEV_TRACE, "board topic(%s) created successfully", topic);
                            (void)ma_board_topic_set_id(t, topic);
                            (void)ma_board_topic_set_board(t, board);
                            if(MA_OK == (err = ma_board_topic_set_pin(t, pid, pin))) {
                                ma_board_info_t *b = NULL;

                                MA_LOG(logger, MA_LOG_SEV_TRACE, "board topic (%s) set pin(%s) successful", topic, pid);
                                if(MA_OK == (err = ma_board_info_create(&b))) {
                                    (void)ma_board_info_set_id(b, board);
                                    (void)ma_board_info_set_image_url(b, board_image_url);
                                    if(MA_OK == (err = ma_board_info_set_topic(b, topic, t))) {
                                        ma_board_info_t *o = NULL;

                                        MA_LOG(logger, MA_LOG_SEV_TRACE, "board (%s) set topic(%s) successful", board, topic);
                                        if(MA_OK == (err = ma_board_get(dboard, board, &o))) {
                                            if(MA_OK == (err = ma_board_info_copy(o, b))) {
                                                if(MA_OK == (err = ma_board_add(dboard, o))) {
                                                    MA_LOG(logger, MA_LOG_SEV_TRACE, "pin(%s) added successfully in board(%s) under topic(%s)", pid, board, topic);
                                                } else {
                                                    MA_LOG(logger, MA_LOG_SEV_ERROR, "pin(%s) added failed in board(%s) under topic(%s), last error(%d)", pid, board, topic, err);
                                                }
                                            }
                                            (void)ma_board_info_release(o);
                                        } else {
                                            if(MA_OK == (err = ma_board_add(dboard, b))) {
                                                MA_LOG(logger, MA_LOG_SEV_TRACE, "board(%s) add successful", board);
                                            } else {
                                                MA_LOG(logger, MA_LOG_SEV_ERROR, "board(%s) add failed, last error(%d)", board, err);
                                            }
                                        }
                                    } else
                                       MA_LOG(logger, MA_LOG_SEV_ERROR, "board set topic(%s) failed, last error(%d)", topic, err);
                                    (void)ma_board_info_release(b);
                                } else
                                    MA_LOG(logger, MA_LOG_SEV_ERROR, "board creation failed, last error(%d)", err);
                            } else
                                MA_LOG(logger, MA_LOG_SEV_ERROR, "board set pin(%s) failed, last error(%d)", pin, err);
                            (void)ma_board_topic_release(t);
                        } else
                            MA_LOG(logger, MA_LOG_SEV_ERROR, "board topic(%s) creation failed, last error(%d)", topic, err);
                        p = strtok(NULL, " ");
                    }
                    (void)ma_board_pin_release(pin);
                    if(tmp)free(tmp);
                } else
                    MA_LOG(logger, MA_LOG_SEV_ERROR, "board pin (%s) creation failed, last error(%d)", pid, err);
            } else
                MA_LOG(logger, MA_LOG_SEV_ERROR, "board and topic are invalid"); 
            MA_LOG(logger, MA_LOG_SEV_DEBUG, "%s end error(%d)", __FUNCTION__, err);
            (void)ma_season_info_release(info);
        } else {
            MA_LOG(logger, MA_LOG_SEV_ERROR, "season info convert from variant fialed, last error(%d)", err);
        }

        MA_LOG(logger, MA_LOG_SEV_DEBUG, "%s end error(%d)", __FUNCTION__, err);
        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t board_subscriber_cb(const char *topic, ma_message_t *msg, void *cb_data) {
    if(topic && msg && cb_data) {
        ma_error_t err = MA_OK;
        ma_board_service_t *service = (ma_board_service_t*)cb_data;
        //ma_board_t *board = service->board;

        MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "board subscriber received message(%s)", topic);
        if(!strcmp(topic, MA_TASK_EXECUTE_PUBSUB_TOPIC)) {
            const char *task_type = NULL , *task_owner_id = NULL , *task_id = NULL;

            MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "board service processing (%s) message", topic);
            if( (MA_OK == (err = ma_message_get_property(msg, MA_TASK_TYPE, &task_type))) && task_type &&
                (MA_OK == (err = ma_message_get_property(msg, MA_TASK_PRODUCT_ID, &task_owner_id))) && task_owner_id &&
                (MA_OK == (err = ma_message_get_property(msg, MA_TASK_ID, &task_id))) && task_id) {
                if(!strcmp(task_type, MA_SEASON_TASK_TYPE)) {
                    err = save_board(service, task_id);
                } else
                    MA_LOG(service->logger, MA_LOG_SEV_DEBUG, "task id(%s) is not season task", task_id);
            }
        } else {
            /* do nothing */
        }

        return err;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_board_service_get_board(ma_board_service_t *service, ma_board_t **board) {
    if(service && board) {
        *board = service->board;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


