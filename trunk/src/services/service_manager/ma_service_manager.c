#include "ma/internal/services/ma_service_manager.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal/utils/platform/ma_dl.h"
#include "ma/internal/utils/datastructures/ma_queue.h"
#include "ma/internal/ma_macros.h"

#include "uv.h"

#include <stdlib.h>
#include <string.h>

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "svcmgr"


typedef struct ma_service_info_s {
    ma_service_t    *service;

    const char      *service_name;

    ma_queue_t      qlink; /* list of ma_service_info_t */

    unsigned        flags;

    ma_lib_t        service_library;

    char service_name_buffer[1]; /* actual buffer to be larger */
} ma_service_info_t;


static ma_service_info_t *create_service_info(const char *service_name) {
    ma_service_info_t * si = calloc(1, sizeof(ma_service_info_t) + strlen(service_name) + 1);
    if (si) {
        strcpy(si->service_name_buffer,service_name);
        si->service_name =  si->service_name_buffer;
    }
    return si;
}

static void release_service_info(ma_service_info_t *si) {
    ma_service_release(si->service);
    ma_dl_close(&si->service_library);
    free(si);
}

static void append_service_info(ma_service_manager_t *self, ma_service_info_t *si) {
    ma_queue_insert_tail(&self->qhead, &si->qlink);
}

static ma_service_create_fn find_dyn_ep(ma_service_manager_t *self, ma_service_info_t *si, const ma_service_manager_service_entry_t *svc ) {
    if (svc->module_name) {
        char path_buffer[MA_MAX_PATH_LEN];
        size_t path_buffer_size = MA_COUNTOF(path_buffer);
        const char *ep = svc->entry_point ? svc->entry_point : "service_create";
        if (uv_exepath(path_buffer, &path_buffer_size)) {
            MA_LOG(self->logger, MA_LOG_SEV_WARNING, "could not get exepath");
            return NULL;
        } else {
            char *p=strrchr(path_buffer, MA_PATH_SEPARATOR);
            if (!p++) {
                MA_LOG(self->logger, MA_LOG_SEV_WARNING, "could not extract path separator from executable");
            } else {
                ma_error_t rc;
                strncpy(p,svc->module_name, path_buffer+MA_COUNTOF(path_buffer)-p);
                rc = ma_dl_open(path_buffer, 0, &si->service_library);
                if (rc) {
                    MA_LOG(self->logger, MA_LOG_SEV_WARNING, "could not load library <%s> for service <%s>, rc= <%ld>", path_buffer, svc->name, (long) rc);
                } else {
                    ma_service_create_fn creator = ma_dl_sym(&si->service_library, ep);
                    if (!creator) MA_LOG(self->logger, MA_LOG_SEV_WARNING, "could not locate entry point <%s> for library <%s> for service <%s>", ep, path_buffer, svc->name);
                    return creator;
                }
            }
        }
    }

    return NULL;
}

void ma_service_manager_init(ma_service_manager_t *self) {
    ma_queue_init(&self->qhead);
}


ma_error_t ma_service_manager_create_services(ma_service_manager_t *self, ma_service_manager_service_entry_t const *services) {
    ma_service_manager_service_entry_t const *svc;
    for (svc = services; svc->name; ++svc) {
        if (svc->flags & MA_SERVICE_OPTION_DISABLED) {
            MA_LOG(self->logger, MA_LOG_SEV_INFO, "Service <%s> is disabled, skipping creation", svc->name);
        } else if (*svc->name) {
            ma_service_create_fn creator = svc->creator_fn;
            ma_error_t rc = MA_OK;
            ma_service_info_t *si = create_service_info(svc->name);
            if (!si) {
                rc = MA_ERROR_OUTOFMEMORY;
            } else {
                if (!creator && (svc->flags & MA_SERVICE_OPTION_DYNLOAD)) {
                    /* TODO load from shared library */
                    creator = find_dyn_ep(self, si, svc);
                }

                if (creator) {
                    /* now invoke creator function and wish for the best */
                    rc = creator(svc->name,&si->service);
                    if (MA_OK == rc && si->service) {
                        append_service_info(self, si);
                        continue;
                    } else {
                        MA_LOG(self->logger, MA_LOG_SEV_CRITICAL, "Failed creating service <%s>, rc=<%ld>", svc->name, (long) rc);
                    }
                } else {
                    rc = MA_ERROR_OBJECTNOTFOUND;
                    MA_LOG(self->logger, MA_LOG_SEV_WARNING, "no creator function available for service <%s>, rc=<%ld>", svc->name, (long) rc);
                }
                release_service_info(si);
            }
                
            if (svc->flags & MA_SERVICE_OPTION_OPTIONAL) {
                MA_LOG(self->logger, MA_LOG_SEV_WARNING, "service creation failed but service is marked optional so ignoring failure", svc->name);
                continue;
            }
            ma_service_manager_release_all(self);
            return rc;
        } else {
            /* empty service name */
            MA_LOG(self->logger, MA_LOG_SEV_WARNING, "not creating nameless service");
        }
    }
    return MA_OK;
}


#define ma_queue_rforeach(q, h)                                         \
    for ((q) = ma_queue_last(h);                                        \
    (q) != ma_queue_sentinel(h) && !ma_queue_empty(h);                  \
    (q) = ma_queue_prev(q))


ma_error_t ma_service_manager_configure_all(ma_service_manager_t *self, ma_context_t *context, unsigned hint) {
    ma_error_t err = MA_OK;

    ma_queue_t *qitem;
    ma_queue_foreach(qitem, &self->qhead) {
        ma_service_info_t *service_info = ma_queue_data(qitem, ma_service_info_t, qlink);
        ma_error_t rc = MA_OK;
        MA_LOG(self->logger, MA_LOG_SEV_TRACE, "configuring service <%s>", service_info->service_name);
        rc = ma_service_configure(service_info->service, context, hint);
        
        if (MA_OK != rc) {
            MA_LOG(self->logger, MA_LOG_SEV_WARNING, "configuring %s failed, rc = <%d>", service_info->service_name, rc);
            err = rc;
        }
    }
    return err;
}

void ma_service_manager_set_logger(ma_service_manager_t *self, ma_logger_t *logger) {
    if(self)
        self->logger = logger;
}



ma_error_t ma_service_manager_add_service(ma_service_manager_t *self, ma_service_t *service, const char *service_name) {
    ma_service_info_t *si = create_service_info(service_name); 
    if (si) {
        MA_LOG(self->logger, MA_LOG_SEV_TRACE, "adding service <%s>", service_name);
        si->service = service;
        append_service_info(self, si);
        return MA_OK;
    }
    return MA_ERROR_OUTOFMEMORY;
}

ma_service_t *ma_service_manager_find_service(ma_service_manager_t *self, char const *service_name) {
    ma_queue_t *qitem;
    ma_queue_foreach(qitem, &self->qhead) {
        ma_service_info_t *service_info = ma_queue_data(qitem, ma_service_info_t, qlink);
        if (0 == strcmp(service_name, service_info->service_name))
            return service_info->service;
    }
    return NULL;
}

/*
void ma_service_manager_remove_service(ma_service_manager_t *self, ma_service_t *service) {
    self;
    ma_queue_remove(&service->qlink);
}*/

ma_error_t ma_service_manager_start_all(ma_service_manager_t *self) {
    ma_error_t err = MA_OK;
    ma_queue_t *qitem;
    ma_queue_foreach(qitem, &self->qhead) {
        ma_service_info_t *service_info = ma_queue_data(qitem, ma_service_info_t, qlink);
        ma_error_t rc = MA_OK;
        MA_LOG(self->logger, MA_LOG_SEV_INFO, "starting service <%s>...", service_info->service_name);
        rc = ma_service_start(service_info->service);
        if (MA_OK != rc) {
            MA_LOG(self->logger, MA_LOG_SEV_WARNING, "starting %s failed, rc = <%d>", service_info->service_name, rc);
            err = rc;
        }
    }
    return err;
}

ma_error_t ma_service_manager_stop_all(ma_service_manager_t *self) {
    ma_error_t err = MA_OK;
    ma_queue_t *qitem;
    ma_queue_rforeach(qitem, &self->qhead) {
        ma_service_info_t *service_info = ma_queue_data(qitem, ma_service_info_t, qlink);
        ma_error_t rc = MA_OK;
        MA_LOG(self->logger, MA_LOG_SEV_INFO, "stopping service <%s>", service_info->service_name);
        rc = ma_service_stop(service_info->service);
        if (MA_OK != rc) {
            MA_LOG(self->logger, MA_LOG_SEV_WARNING, "stopping %s failed, rc = <%d>", service_info->service_name, rc);
            err = rc;
        }
    }
    return err;
}

void ma_service_manager_release_all(ma_service_manager_t *self) {
    while (!ma_queue_empty(&self->qhead)) {
        ma_queue_t *qitem = ma_queue_last(&self->qhead);
        ma_service_info_t *service_info = ma_queue_data(qitem, ma_service_info_t, qlink);
        ma_queue_remove(qitem);
        release_service_info(service_info);
    }
}
