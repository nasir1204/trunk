#ifndef MA_EXCEPTION_HXX_INCLUDED
#define MA_EXCEPTION_HXX_INCLUDED

#include "ma/ma_common.h"

#ifdef MA_WINDOWS
	#include <stdexcept>
#endif

#include <exception>

namespace mileaccess { namespace ma { namespace utils {

struct exception : std::exception {
    exception(ma_error_t e) : error(e) {;}
    exception(const char *what, ma_error_t e) : error(e) {;}

    ma_error_t err() const {
        return error;
    }

    ma_error_t error;
};

} /* namespace utils */
using utils::exception; // pull exception up into the mileaccess::ma namespace
}} /* namespace mileaccess::ma */

#endif /* MA_EXCEPTION_HXX_INCLUDED */

