#ifndef MA_AUTO_HANDLE_HXX_INCLUDED
#define MA_AUTO_HANDLE_HXX_INCLUDED


#include <cassert>

namespace mileaccess { namespace ma { namespace utils {

/* 
 * supplied 'HT' traits class must provide:
 * type definition, 'handle_type', for the handle type
 * a static close() method taking handle_type
 * a static invalid_handle_value() that returns a unique, invalid, handle value
*/

template <typename HT>
struct auto_handle {

    typedef HT trait_type;

    typedef typename HT::handle_type handle_type;

    /*!
     * Default constructor. Sets the handle to invalid_handle_value
     */
    auto_handle() throw ();

    /*!
     * C'tor. This will always take ownership (e.g. call close() upon destruction) 
     */
    explicit auto_handle(handle_type h) throw ();

    /*!
     * returns a copy of the handle value
     */
    handle_type get() const throw ();

    /*!
     * Overloaded address-of operator to be used in functions that generate handles (e.g. where handle is an [out] param)
     */
    handle_type *operator &();

    /*!
     * Closes the handle now
     */
    void close();

    /*!
     * Destructor. Automatically closes the contained handle
     */
    ~auto_handle();

    handle_type *addr_of();

    handle_type handle;

private:

    // Not implemented, prevent memberwise copying
    auto_handle(auto_handle<HT> const &);
    auto_handle const & operator=(auto_handle<HT> const &);
};

/**
 * copyable_handle extends auto_handle by adding a copy c'tor and assignment operator
 * HT must, in addition to auto_handle requirements, provide a static "add_ref()" method
 */

template <typename HT>
struct copyable_handle : auto_handle<HT>  {

    typedef typename HT::handle_type handle_type;

    /**
     * Default constructor
     */
    copyable_handle();

    /**
     * Construct from specified handle, takes a reference unless add_ref is specified to false
     */
    explicit copyable_handle(handle_type h, bool add_ref = true);

    /**
     * Canonical copy constructor. Stores and take a reference to the rhs object
     */
    copyable_handle(copyable_handle<HT> const &o);

    /**
     * Canonical assignment operator
     */
    copyable_handle const & operator=(copyable_handle<HT> const &rhs);
};

template <typename HT>
inline void swap(copyable_handle<HT> &l, copyable_handle<HT> &r) {
    copyable_handle<HT> t(l);
    l = r;
    r = t;
}

template <typename HT>
inline auto_handle<HT>::auto_handle() throw () : handle(HT::invalid_handle_value()) {
}

template <typename HT>
inline auto_handle<HT>::auto_handle(handle_type h) throw () : handle(h)  {
}

template <typename HT>
inline typename HT::handle_type auto_handle<HT>::get() const throw () {
    return this->handle;
}

template <typename HT>
inline typename HT::handle_type *auto_handle<HT>::operator &() {
    assert("existing handle present, close it before trying to get another one..." && HT::invalid_handle_value() == this->handle);
    return &this->handle;
}

template <typename HT>
inline typename HT::handle_type *auto_handle<HT>::addr_of() {
    //assert("existing handle present, close it before trying to get another one..." && HT::invalid_handle_value == this->handle);
    return &this->handle;
}

template <typename HT>
inline void auto_handle<HT>::close() {
    if (HT::invalid_handle_value() != this->handle) {
        HT::close(this->handle);
        this->handle = HT::invalid_handle_value();
    }
}

template <typename HT>
inline auto_handle<HT>::~auto_handle() {
    close();
}


//////////////////////////////////////////////////////////////////////////
// copyable_handle implementation

template <typename HT>
inline copyable_handle<HT>::copyable_handle() {
}

template <typename HT>
inline copyable_handle<HT>::copyable_handle(typename copyable_handle<HT>::handle_type h, bool add_ref) : auto_handle<HT>(h) {
    if (add_ref) {
        HT::add_ref(h);
    }
}

template <typename HT>
inline copyable_handle<HT>::copyable_handle(copyable_handle<HT> const &o) : auto_handle<HT>(o.get()) {
    HT::add_ref(this->get());
}

template <typename HT>
inline copyable_handle<HT> const & copyable_handle<HT>::operator=(copyable_handle<HT> const &rhs) {
    copyable_handle<HT> temp(rhs);
    swap(*this, temp);
    return *this;
}


} } } /* namespace mileaccess::ma::utils */

#endif /* MA_AUTO_HANDLE_HXX_INCLUDED */

