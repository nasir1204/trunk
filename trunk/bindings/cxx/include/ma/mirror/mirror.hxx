#ifndef MA_REPOSITORY_MIRROR_HXX_INCLUDED
#define MA_REPOSITORY_MIRROR_HXX_INCLUDED

#include "ma/client.hxx"
#include "ma/utils/exception.hxx"
#include "ma/buffer.hxx"
#include "ma/repository/ma_repository_client_defs.h"
#include "ma/repository/ma_repository_mirrror.h"
#include "ma/buffer.hxx"
#include <string>
#include <exception>

namespace mileaccess { namespace ma {
using mileaccess::ma::client;
/*!
	* wrapper over ma repository Mirror.
*/
struct RepositoryMirror {
	/** 
	 * @brief start mirroring.
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
    void start();

	/** 
	 * @brief stop mirroring.
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
    void stop();

	/** 
	 * @brief get mirroring state.
	 * @result                   on success, return mirroring state       
	 *                                      
	 * @remark               
	 *
	 */
	ma_repository_mirror_state_t get_state();

	/** 
	 * @brief create mirroring task.
	 * @result                   on success, return MA task object
	 *                                      
	 * @remark					 It will just create task, its user responsibility schedule task
	 *
	 */
	ma_task_t* create_task();
	
	/** 
	 * @brief get schedule task session Id .
	 * @result					
	 *                                      
	 * @remark               It will set sessionId, user can check status of mirror operation.
	 *
	 */
	void get_task_sessionId(ma_task_t *task);

	/** 
	 * @brief register state callback .
	 * @result					
	 *                                      
	 * @remark               
	 *
	 */
	void register_state_callback();

	/*!
	 * c'tor,
     * @param c						[in]     The client used for mirroring.
	 * @param destination_path		[in]     destination path string.
     */
    explicit RepositoryMirror(client const &c, std::string &destination_path);

	/** 
	 * @brief get sessionId .
	 * @result		return session id			
	 *                                      
	 * @remark               
	 *
	 */
	buffer& get_sessionId();
private:
    client const &cli;
	std::string &path;
	buffer sessionId;
};

inline ma_task_t* RepositoryMirror::create_task() {
	ma_task_t *task = NULL;
   	ma_error_t err;
	if(MA_OK != (err = ma_repository_mirror_task_create(cli.get(), NULL, path.c_str(), &task))) 
		throw utils::exception(err) ;

	return task;
}

inline buffer& RepositoryMirror::get_sessionId(){
	return sessionId;
}

inline void RepositoryMirror::get_task_sessionId(ma_task_t *task) {
    ma_error_t err;
	ma_buffer_t *sessionId_buffer = NULL;
	if(MA_OK != (err= ma_repository_mirror_task_get_session_id(task, &sessionId_buffer))) 
		throw utils::exception(err) ;
	sessionId.handle = sessionId_buffer;
}

inline ma_repository_mirror_state_t RepositoryMirror::get_state(){
	ma_repository_mirror_state_t state;
	ma_buffer_t *sessionId_buffer = sessionId.get();
	ma_error_t err = ma_repository_mirror_get_state(cli.get(), sessionId_buffer, &state);
	if(err) throw utils::exception(err) ;
	return state;
}

inline void RepositoryMirror::start(){
	ma_buffer_t *sessionId_buffer = NULL;
    ma_error_t err =  ma_repository_mirror_start(cli.get(), path.c_str(), &sessionId_buffer);
	sessionId.handle = sessionId_buffer;
	if(err) throw utils::exception(err) ;
}

inline void RepositoryMirror::stop(){
    ma_error_t err =  ma_repository_mirror_stop(cli.get(), sessionId.get());
	if(err) throw utils::exception(err) ;
}

inline RepositoryMirror::RepositoryMirror(mileaccess::ma::client const &cli, std::string &destination_path):cli(cli), path(destination_path){
	sessionId.handle = NULL;
}

inline ma_error_t state_cb(ma_client_t *ma_client, const char *session_id, ma_repository_mirror_state_t state, void *cb_data) {
	//use state
	return MA_OK;
}

inline void RepositoryMirror::register_state_callback(){
	ma_error_t err = ma_repository_mirror_register_state_callback(cli.get(), state_cb, NULL);
	if(err) throw utils::exception(err) ;
}


}} // namespace mileaccess::ma

#endif /* MA_REPOSITORY_MIRROR_HXX_INCLUDED */
