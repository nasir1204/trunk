#ifndef MA_MSGBUS_HXX_INCLUDED
#define MA_MSGBUS_HXX_INCLUDED

#include "ma/ma_msgbus.h"
#include "ma/logger.hxx"
#include "ma/message.hxx"

#include "ma/utils/auto_handle.hxx"
#include "ma/utils/exception.hxx"

#include <string>
#include <functional>

namespace mileaccess { namespace ma {

struct msgbus_traits {

    typedef ::ma_msgbus_h handle_type;

    static handle_type invalid_handle_value() {
        return handle_type(0);
    };

    static void close(handle_type h) {
        ::ma_msgbus_release(h);
    }
};

struct msgbus_endpoint;


struct passphrase_provider {
	passphrase_provider(){}

    virtual ~passphrase_provider() {}

	virtual ma_error_t on_provide_passphrase(unsigned char *passphrase, size_t *passphrase_size) = 0;
};


struct msgbus : utils::auto_handle<msgbus_traits> {

    //! constructs a new instance of msgbus
    explicit msgbus(const char *product_id);

    void set_logger(logger &l);

	void set_passphrase_provider(passphrase_provider const &p);

    unsigned int get_version() const;

    /*!
     * starts msgbus in another thread. This function returns immediately
     *
     */
    void start();

    /*!
     *
     * Runs the msgbus on the caller's thread (that is, this thread!)
     * This is a blocking call.
     *
     * Must use msgbus::stop() to unblock
     */
    void run();

    /*!
     * stops the msgbus. If msgbus was started via the 'run' method, stop() will eventually cause run to return.
     * if started via 'start' the worker thread is terminated. If fwait is set, stop() will wait for that thread to finish before returning (this is typically a good thing)
     */
    void stop(bool fwait);


private:
	passphrase_provider *p ;

	static ma_error_t  passphrase_provider_cb(::ma_msgbus_t *msgbus, void *cb_data, unsigned char *passphrase, size_t *passphrase_size);
    // prevent copying and such
    msgbus(msgbus const &m);

    msgbus &operator=(msgbus const &o);

//
};


inline msgbus::msgbus(const char *product_id):p(NULL) {
    if (ma_error_t err = ma_msgbus_create(product_id, addr_of())) throw utils::exception(err);
}

inline void msgbus::set_logger(logger &l) {

}

// callback
inline ma_error_t msgbus::passphrase_provider_cb(::ma_msgbus_t *bus, void *cb_data, unsigned char *passphrase, size_t *passphrase_size) {
    try {
        msgbus *m= static_cast<msgbus*>(cb_data);
        return m->p ?  m->p->on_provide_passphrase(passphrase, passphrase_size) : MA_ERROR_INVALIDARG;
    } catch (...) {

    }
    return MA_ERROR_UNEXPECTED;
}

inline void msgbus::set_passphrase_provider(passphrase_provider const &p) {
	this->p = const_cast<passphrase_provider*>(&p);
	if (ma_error_t err = ma_msgbus_set_passphrase_provider_callback(get(), &msgbus::passphrase_provider_cb, this)) throw utils::exception(err);
}


inline unsigned int msgbus::get_version() const { 
    unsigned int v  = 0 ;
    if(ma_error_t err = ma_msgbus_get_version(&v)) throw utils::exception(err);
    return v ;
}

inline void msgbus::start() {
    if (ma_error_t err = ma_msgbus_start(get())) throw utils::exception(err);
}

inline void msgbus::run() {
    if (ma_error_t err = ma_msgbus_run(get())) throw utils::exception(err);
}

inline void msgbus::stop(bool fwait) {
    if (ma_error_t err = ma_msgbus_stop(get(), fwait)) throw utils::exception(err);
}


struct msgbus_server_traits {

    typedef ma_msgbus_server_h handle_type;

    static handle_type invalid_handle_value() {
        return handle_type(0);
    }

    static void close(handle_type h) {
        ma_msgbus_server_release(h);
    }
};


struct msgbus_server : utils::auto_handle<msgbus_server_traits> {

    /*!
     * constructs a new server using the specified server name. You must pass a valid msgbus instance in the call to start()
     */
    msgbus_server(const char *server_name);

    /*!
     * Creates the underlying msgbus server object
     * Call this before you do anything interesting
     */
    void create(::ma_msgbus_t *msgbus);


    /*!
     * handler class that you must derive from to receive messages from clients
     */
    struct handler {
        /*!
         * you implement this method
         */
        virtual ma_error_t on_request(msgbus_server &server, message const &request_msg, ma_msgbus_client_request_t *c_request) = 0;

		virtual ~handler() {}
    };


    /*!
     * sets an option
     */
    void set_option(ma_msgbus_option_t opt, long value);


    /*!
     * starts the server, after which you may receive messages via the supplied handler's on_request method
     */
    void start(handler & handler);


    /*!
     * stops listening for messages
     */
    void stop();

private:
    static ma_error_t server_handler(ma_msgbus_server_t *server, ma_message_t *request_payload, void *cb_data, ma_msgbus_client_request_t *c_request);

    handler *client_handler;

    std::string server_name;
};

struct msgbus_subscriber_traits {

    typedef ma_msgbus_subscriber_h handle_type;

    static handle_type invalid_handle_value() {
        return handle_type(0);
    }

    static void close(handle_type h) {
        ma_msgbus_subscriber_release(h);
    }
};

struct msgbus_subscriber { /* pub-sub consumer */

    msgbus_subscriber() {}

    msgbus_subscriber(ma_msgbus_t *msgbus, char const *topic_filter) : topic_filter(topic_filter) {
        create(msgbus);
    }

    void create(ma_msgbus_t *msgbus) {
        if (ma_error_t err = ma_msgbus_subscriber_create(msgbus, &handle)) throw utils::exception(err);
    }

    struct handler {
        virtual ma_error_t on_pubsub_message(char const *topic, mileaccess::ma::message const &msg_payload) = 0;
		virtual ~handler() {}
    };

    void start(handler &h, char const *topic_filter = 0) {
        if (topic_filter) this->topic_filter = topic_filter;
        this->client_handler = &h;
        if (ma_error_t err = ma_msgbus_subscriber_register(handle, this->topic_filter.c_str(), &subscriber_handler_stub, this)) throw utils::exception(err);
    }

    void stop() {
        if (ma_error_t err = ma_msgbus_subscriber_unregister(handle)) throw utils::exception(err);
    }

    void set_option(ma_msgbus_option_t opt, long value);


    void release() throw() {
        (void) ma_msgbus_subscriber_release(handle);
    }

protected:
    std::string topic_filter;

    ma_msgbus_subscriber_h handle;

private:
    handler *client_handler;

    static ma_error_t subscriber_handler_stub(const char *topic, ma_message_t *payload, void *cb_data) {
        msgbus_subscriber *self = reinterpret_cast<msgbus_subscriber *>(cb_data);
        try {
            mileaccess::ma::message msg(payload,true);
            return self->client_handler->on_pubsub_message(topic, msg);
        } catch (...) {

        }
        return MA_ERROR_UNEXPECTED; // we are not lying, errors are usually unexpected!
    }
};

inline void msgbus_subscriber::set_option(ma_msgbus_option_t opt, long value) {
	if (ma_error_t err = ma_msgbus_subscriber_setopt(handle, opt, value)) throw utils::exception(err);
}



/************************************************************************/
/* msgbus_server implementation                                         */
/************************************************************************/

inline msgbus_server::msgbus_server(char const *name) : client_handler(0), server_name(name?name:"") {
}

inline void msgbus_server::create(ma_msgbus_t *msgbus) {
    if (msgbus) {
        this->close();
        if (ma_error_t err = ma_msgbus_server_create(msgbus, server_name.c_str(), addr_of())) throw utils::exception("ma_msgbus_server_create", err);
    }
}

inline void msgbus_server::set_option(ma_msgbus_option_t opt, long value) {
    if (ma_error_t err = ma_msgbus_server_setopt(get(), opt, value)) throw utils::exception("ma_msgbus_server_setopt exception", err);
}


inline void msgbus_server::start(msgbus_server::handler &handler) {
    this->client_handler = &handler;
    if (ma_error_t err = ma_msgbus_server_start(get(), &msgbus_server::server_handler, this)) throw utils::exception(err);
}


inline void msgbus_server::stop() {
    if (ma_error_t err = ma_msgbus_server_stop(get())) throw utils::exception("ma_msgbus_server_stop", err);
}

// callback
inline ma_error_t msgbus_server::server_handler(ma_msgbus_server_t *server, ma_message_t *request_payload, void *cb_data, ma_msgbus_client_request_t *c_request) {
    try {
        msgbus_server *my_server = reinterpret_cast<msgbus_server*>(cb_data);
        return my_server->client_handler->on_request(*my_server, message(request_payload), c_request);
    } catch (...) {

    }
    return MA_ERROR_UNEXPECTED;
}


/************************************************************************/
/* msgbus_endpoint                                                      */
/************************************************************************/

struct msgbus_endpoint_traits {

    typedef ::ma_msgbus_endpoint_h handle_type;

    static handle_type invalid_handle_value() {
        return handle_type(0);
    };

    static void close(handle_type h) {
        ::ma_msgbus_endpoint_release(h);
    }

};


struct msgbus_endpoint : utils::auto_handle<msgbus_endpoint_traits> {

    template <typename T>
    struct request {
        request(T t) : handler(t) {

        }

        void cancel(unsigned flags);

        msgbus_endpoint get_endpoint() const;

        ma_msgbus_request_t *c_req;

        T handler;
    };

    explicit msgbus_endpoint(msgbus &mbus, const char *server_name, const char *host_name = 0);

    explicit msgbus_endpoint(ma_msgbus_t *c_mbus, const char *server_name, const char *host_name = 0);

    // TODO
    void set_option(::ma_msgbus_option_t opt, long value);

    
    /*!
     * Sends the specified message to the endpoint/server. Does not wait
     */
    template <typename H>
    request<H> async_send(message const &msg, H handler);

    /*!
     * sync send
     *
     */
    message send(message const &msg);

    /*!
     * send without expecting a response
     */
    void send_and_forget(message const &msg);

private:
    static ma_error_t async_cb(ma_error_t status, ma_message_t *response, void *cb_data, ma_msgbus_request_t *request);

};

inline msgbus_endpoint::msgbus_endpoint(msgbus &mbus, const char *server_name, const char *host_name) {
    if (ma_error_t e = ma_msgbus_endpoint_create(mbus.get(), server_name, host_name, addr_of())) throw utils::exception(e);
}

inline msgbus_endpoint::msgbus_endpoint(ma_msgbus_t *c_mbus, const char *server_name, const char *host_name) {
    if (ma_error_t e = ma_msgbus_endpoint_create(c_mbus, server_name, host_name, addr_of())) throw utils::exception(e);
}


inline void msgbus_endpoint::set_option(::ma_msgbus_option_t opt, long value) {
	if(::ma_error_t e = ma_msgbus_endpoint_setopt(get(), opt, value)) throw utils::exception(e);
}


template <typename H>
inline msgbus_endpoint::request<H> msgbus_endpoint::async_send(message const &msg, H handler) {

    request<H> result(handler);

    if (ma_error_t e = ma_msgbus_async_send(get(), msg.m.get(), &msgbus_endpoint::async_cb, 0, &result.c_req)) throw utils::exception(e);

    return result;
}

inline void msgbus_endpoint::send_and_forget(message const &msg) {
    if (ma_error_t e = ma_msgbus_send_and_forget(get(), msg.m.get())) throw utils::exception(e);
}

inline message msgbus_endpoint::send(message const &msg) {
	::ma_message_t *res = NULL;
	if (::ma_error_t e = ma_msgbus_send(get(), msg.m.get(), &res )) throw utils::exception(e);

	message response(res, true);
	return response;
}


}} // namespace mileaccess::ma



#endif // MA_MSGBUS_HXX_INCLUDED
