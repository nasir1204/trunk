#ifndef MA_INFO_HXX_INCLUDED
#define MA_INFO_HXX_INCLUDED

#include "ma/client.hxx"
#include "ma/utils/exception.hxx"
#include "ma/variant.hxx"
#include "ma/info/ma_info.h"

#include <string>
#include <exception>

namespace mileaccess { namespace ma { namespace info {

struct info_event_handler {
	/**
     * call this to register for info events
     */
    void register_info_events(const char *product_id) ;
	void register_info_events(std::string const &product_id) ;

    /**
     * and this to unregister
     */
    void unregister_info_events(const char *product_id) ;
	void unregister_info_events(std::string const &product_id) ;

	/**
	* API to get agent information like mode, version, guid, locale ...
	* @param cli, The client used for registration.
	* @param key, agent info key defined as MA_INFO_* in ma_info.h
	* 
	* @return return the info value as variant
	*/
	mileaccess::ma::variant	get_info(mileaccess::ma::client const &cli, const char *key) ;
	
	mileaccess::ma::variant	get_info(mileaccess::ma::client const &cli, std::string const &key) ;

protected :

	/**
     * protected constructor, must be invoked by your derived class
     */
    info_event_handler(mileaccess::ma::client const &c) : cli(c) {}

    virtual ~info_event_handler() {}

	/**
     * invoked by MA when an event happens like poperties sent, policy enforced ...
     * @param cli, The client used for registration.
     * @param product_id, The product id of the PP registered
     * @param info_event, event happened in MA  defined as MA_INFO_EVENT_* in ma_info.h
     *
     */
    virtual ma_error_t on_info_event(client const &cli, std::string const &product_id, std::string const & info_event) = 0 ;

private:
    static ::ma_error_t info_events_cb(::ma_client_t *, const char *, const char *, void *) ;

	client const &cli;

    // private copy c'tor and assignment operator to prevent accidental copying

    info_event_handler(info_event_handler const &) ;

    info_event_handler const &operator=(info_event_handler const &) ;

} ;

inline void info_event_handler::register_info_events(const char *product_id) {
    if (::ma_error_t err = ma_info_events_register_callback(cli.get(), product_id, &info_event_handler::info_events_cb, this)) throw utils::exception(err);
}

inline void info_event_handler::register_info_events(std::string const &product_id) {
    if (::ma_error_t err = ma_info_events_register_callback(cli.get(), product_id.c_str(), &info_event_handler::info_events_cb, this)) throw utils::exception(err);
}

inline void info_event_handler::unregister_info_events(const char *product_id) {
    if (::ma_error_t err = ma_info_events_unregister_callback(cli.get(), product_id)) throw utils::exception(err);
}

inline void info_event_handler::unregister_info_events(std::string const &product_id) {
    if (::ma_error_t err = ma_info_events_unregister_callback(cli.get(), product_id.c_str())) throw utils::exception(err);
}

/*static*/ 
inline ::ma_error_t info_event_handler::info_events_cb(::ma_client_t *client, const char *product_id, const char *info_event, void *cb_data) {
    info_event_handler *self = (info_event_handler *)cb_data;
    try {
		std::string s(info_event) ;
        return self->on_info_event(self->cli, product_id, s) ;
    } 
	catch (...) {
    } ;

    return MA_ERROR_UNEXPECTED ;
}

inline mileaccess::ma::variant info_event_handler::get_info(mileaccess::ma::client const &cli, const char *key) {
	ma_variant_t *value = NULL ;
	if (::ma_error_t err = ma_info_get(cli.get(), key, (ma_variant_t **)&value)) throw utils::exception(err) ;

	return mileaccess::ma::variant(value, false) ;
}

inline mileaccess::ma::variant info_event_handler::get_info(mileaccess::ma::client const &cli, std::string const &key) {
	ma_variant_t *value = NULL ;
	if (::ma_error_t err = ma_info_get(cli.get(), key.c_str(), (ma_variant_t **)&value)) throw utils::exception(err) ;

	return mileaccess::ma::variant(value, false) ;
}

}}} // namespace mileaccess::ma::info

#endif /* MA_INFO_PROVIDER_HXX_INCLUDED */

