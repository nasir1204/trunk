#ifndef MA_LOGGER_HXX_INCLUDED
#define MA_LOGGER_HXX_INCLUDED

#include "ma/ma_log.h"
#include "ma/logger/ma_logger.h"
#include "ma/logger/ma_console_logger.h"
#include "ma/logger/ma_file_logger.h"
#include "ma/logger/ma_logger_collector.h"
#include "ma/logger/ma_mailslot_logger.h"
#include "ma/logger/ma_log_filter.h"

#include "ma/utils/auto_handle.hxx"
#include "ma/utils/exception.hxx"

#include <string>

using namespace std;

namespace mileaccess { namespace ma {

struct logger_traits {
    typedef ::ma_logger_h handle_type;

    static handle_type invalid_handle_value() {
        return handle_type(NULL);
    }

    static void close(handle_type h) {
        ::ma_logger_release(h);
    }

    static void add_ref(handle_type h) {
        ::ma_logger_inc_ref(h);
    }
};

struct filter {
    typedef ma_log_filter_t *filter_handle;

    filter_handle handle;

    filter(const char *pat) {
        if(ma_error_t err = ma_generic_log_filter_create(pat, &handle))throw utils::exception(err);
    }

    filter_handle get() const {
        return handle;
    }

    ~filter() {
        //TODO do we need to explicitly call release
    }
};

struct logger {
    typedef utils::copyable_handle<logger_traits> logger_copyable_handle;
    typedef logger_copyable_handle::handle_type handle_type;

    logger(handle_type h, bool add_ref);

    logger(logger const &o);

    handle_type get() const;

    void add(filter f);

    void remove(filter f);

public:
    logger_copyable_handle m;
};

inline logger::logger(handle_type h, bool add_ref) : m(h, add_ref) {
}

inline logger::handle_type logger::get() const {
    return m.get();
}

inline logger::logger(logger const &o) {
    m = o.m;
}

inline void logger::add(filter f) {
    if(ma_error_t err = ma_logger_add_filter(m.get(), f.get()))throw utils::exception(err);
}

inline void logger::remove(filter f) {
    if(ma_error_t err = ma_logger_remove_filter(m.get()))throw utils::exception(err);
}

struct file_logger : logger {
    typedef ::ma_file_logger_t *file_logger_handle_type;
    
    file_logger_handle_type handle;
    
    explicit file_logger() : logger(NULL, false) {;}

    explicit file_logger(const char *log_base_dir , const char *log_file_name  , const char *log_file_ext) : logger(NULL, false), handle(0) {
        if(ma_error_t err = ma_file_logger_create(log_base_dir, log_file_name, log_file_ext, &this->handle))
            throw utils::exception(err);
    }
    
    explicit file_logger(file_logger_handle_type h) : logger(reinterpret_cast<handle_type>(h), h ? true : false), handle(h) {;}
    
    explicit file_logger(file_logger const &o):logger(o) {}
    
    file_logger &operator=(file_logger const &o) {
        this->handle = o.handle;
        return *this;
    }
    
    void set_policy(ma_file_logger_policy_t *policy) {
        if(ma_error_t err = ma_file_logger_set_policy(handle, policy))throw utils::exception(err);
    }
    
    const ma_file_logger_policy_t *get_policy() const {
        const ma_file_logger_policy_t *policy = NULL;

        if(ma_error_t err = ma_file_logger_get_policy(handle, &policy))
            throw utils::exception(err);

        return policy;
    }
    
    ~file_logger() {
        (void)ma_file_logger_release(handle);
    }

    handle_type operator()() {
        return reinterpret_cast<handle_type>(handle);
    }
    
};


struct console_logger : logger {
    typedef ::ma_console_logger_t *console_logger_handle_type;
    console_logger_handle_type handle;
    
    explicit console_logger() : logger(NULL, false), handle(NULL) { 
        if(ma_error_t err = ma_console_logger_create(&handle))throw ma::utils::exception(err);
    }
    
    explicit console_logger(console_logger_handle_type h) : logger(reinterpret_cast<handle_type>(h), h ? true : false), handle(h) {;}
    
    explicit console_logger(console_logger const &o) : logger(o) {;}
    
    console_logger &operator=(console_logger const &o) {
        this->handle = o.handle;
        return *this;
    }
    
    ~console_logger() {
        (void)ma_console_logger_release(handle);
    }

    handle_type operator()() {
        return reinterpret_cast<handle_type>(handle);
    }
};

struct collector : logger {
    typedef ::ma_logger_collector_h collector_logger_handle_type;
    collector_logger_handle_type handle;
    
    explicit collector() :logger(NULL, false), handle(NULL) { 
        if(ma_error_t err = ma_logger_collector_create(&handle))throw ma::utils::exception(err);
    }
    
    explicit collector(collector_logger_handle_type h) : logger(reinterpret_cast<handle_type>(h), h ? true : false), handle(h){;}
    
    explicit collector(collector const &o) : logger(o) {;}
    
    collector &operator=(collector const &o) {
        this->handle = o.handle;
        return *this;
    }
    
    void add(logger &l) {
        if(ma_error_t err = ma_logger_collector_add_logger(handle, l.get()))
            throw ma::utils::exception(err);
    }
    
    void remove(logger &l) {
        if(ma_error_t err = ma_logger_collector_add_logger(handle, l.get()))
            throw ma::utils::exception(err);
    }
    
    ~collector() {
        (void)ma_logger_collector_release(handle);
    }

    handle_type operator()() {
        return reinterpret_cast<handle_type>(handle);
    }
};

struct mail_slot : logger {
    typedef ::ma_mail_slot_logger_t *mail_slot_logger_handle_type;
    string slot;
    mail_slot_logger_handle_type handle;
    
    explicit mail_slot(const char *s):logger(NULL, false), slot(s), handle(NULL) { 
        if(ma_error_t err = ma_mail_slot_logger_create(slot.c_str(), &handle))throw ma::utils::exception(err);
    }
    
    explicit mail_slot(mail_slot_logger_handle_type h) : logger(reinterpret_cast<handle_type>(h), h ? true : false), handle(h){;}
    
    explicit mail_slot(mail_slot const &o) : logger(o) {;}
    
    mail_slot &operator=(mail_slot const &o) {
        this->handle = o.handle;
        return *this;
    }
    
    void set_policy(ma_mail_slot_logger_policy_t *policy) {
        if(ma_error_t err = ma_mail_slot_logger_set_policy(handle, policy))throw ma::utils::exception(err);
    }
    
    const ma_mail_slot_logger_policy_t *get_policy() const {
        const ma_mail_slot_logger_policy_t *policy = NULL;

        if(ma_error_t err = ma_mail_slot_logger_get_policy(handle, &policy))throw ma::utils::exception(err);

        return policy;
    }
    
    ~mail_slot() {
        (void)ma_mail_slot_logger_release(handle);
    }
    
    handle_type operator()() {
        return reinterpret_cast<handle_type>(handle);
    }
};

}}

#endif /* MA_LOGGER_HXX_INCLUDED */
