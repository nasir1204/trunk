#ifndef MA_TRIGGER_HXX_INCLUDED
#define MA_TRIGGER_HXX_INCLUDED

#include "ma/scheduler/time.hxx"
#include "ma/utils/exception.hxx"

#include "ma/scheduler/ma_triggerlist.h"

#include <string>

namespace mileaccess { namespace ma { namespace scheduler {

// not sure what reference counting mechanism should be employed here. The 
struct trigger {
    typedef ma_trigger_h handle_type;

    trigger(handle_type h = 0, bool owned = false);

    // copy c'tor - takes ownership (if any) away from source
    trigger(trigger const &);


    ~trigger();

    static trigger make_run_now_trigger(ma_trigger_run_now_policy_t &rnp);

    static trigger make_daily_trigger(ma_trigger_daily_policy_t &dp);

    static trigger make_weekly_trigger(ma_trigger_weekly_policy_t &wp);

    static trigger make_monthly_date_trigger(ma_trigger_monthly_date_policy_t &mp);

    static trigger make_monthly_dow_trigger(ma_trigger_monthly_dow_policy_t &dowp);

    static trigger make_logon_trigger(ma_trigger_logon_policy_t &lp);

    static trigger make_system_startup_trigger(ma_trigger_systemstart_policy_t &sp);

    ::ma_trigger_type_t get_type() const;
    ::ma_trigger_state_t get_state() const;
    std::string get_id() const;

    // begin date
    void set_begin_date(ma_task_time_t const &start_date);
    time get_begin_date() const;

    // end date
    void set_end_date(ma_task_time_t const &end_date);
    time get_end_date() const;

    // returns the underlying C handle
    handle_type get() const;

    void close();

protected:
    handle_type handle;
    mutable bool owned;

private:
    trigger &operator=(trigger const &);
};

inline trigger::trigger(trigger::handle_type h, bool o) : handle(h), owned(o) {
    
}

inline trigger::trigger(trigger const &o) : handle(o.handle), owned(o.owned) {
    o.owned = false;
}

inline trigger::~trigger() {
    close();
}

inline trigger::handle_type trigger::get() const {
    return this->handle;
}

inline ::ma_trigger_type_t trigger::get_type() const {
    ::ma_trigger_type_t type = MA_TRIGGER_NOW;

    if (::ma_error_t err = ma_trigger_get_type(get(), &type)) throw mileaccess::ma::utils::exception(err);

    return type;
}

inline ::ma_trigger_state_t trigger::get_state() const {
    ::ma_trigger_state_t state = MA_TRIGGER_STATE_BLOCKED;

    if (::ma_error_t err = ma_trigger_get_state(get(), &state)) throw mileaccess::ma::utils::exception(err);

    return state;
}

inline std::string trigger::get_id() const {
    const char *id = NULL;

    if (::ma_error_t err = ma_trigger_get_id(get(), &id)) throw mileaccess::ma::utils::exception(err);
    
    std::string s(id);

    return s;
}

inline void trigger::set_begin_date(ma_task_time_t const &start_date) {
    if (::ma_error_t err = ma_trigger_set_begin_date(get(), (ma_task_time_t*)&start_date)) throw mileaccess::ma::utils::exception(err);
}

inline time trigger::get_begin_date() const {
    time date;

    if(::ma_error_t err = ma_trigger_get_begin_date(get(), &date)) throw mileaccess::ma::utils::exception(err);

    return date;
}

    // end date
inline void trigger::set_end_date(ma_task_time_t const &end_date) {
    if (::ma_error_t err = ma_trigger_set_end_date(get(), (ma_task_time_t*)&end_date)) throw mileaccess::ma::utils::exception(err);
}

inline time trigger::get_end_date() const {
    time date;

    if(::ma_error_t err = ma_trigger_get_end_date(get(), &date)) throw mileaccess::ma::utils::exception(err);

    return date;
}

inline void trigger::close() {
    if (owned) {
        ma_trigger_release(handle);
    }
    handle = 0;
}

inline trigger trigger::make_run_now_trigger(ma_trigger_run_now_policy_s &rnp) {
    ::ma_trigger_h h;
    if (::ma_error_t err = ma_trigger_create_run_now(&rnp, &h)) throw mileaccess::ma::utils::exception(err);

    return trigger(h,true);
}

inline trigger trigger::make_daily_trigger(ma_trigger_daily_policy_t &dp) {
    ::ma_trigger_h h;
    if (::ma_error_t err = ma_trigger_create_daily(&dp, &h)) throw mileaccess::ma::utils::exception(err);
    
    return trigger(h, true);
}

inline trigger trigger::make_weekly_trigger(ma_trigger_weekly_policy_t &wp) {
    ::ma_trigger_h h;
    if (::ma_error_t err = ma_trigger_create_weekly(&wp, &h)) throw mileaccess::ma::utils::exception(err);
    
    return trigger(h, true);
}
    
inline trigger trigger::make_monthly_date_trigger(ma_trigger_monthly_date_policy_t &mp) {
    ::ma_trigger_h h;
    if (::ma_error_t err = ma_trigger_create_monthly_date(&mp, &h)) throw mileaccess::ma::utils::exception(err);
    
    return trigger(h, true);
}
    
inline trigger trigger::make_monthly_dow_trigger(ma_trigger_monthly_dow_policy_t &dowp) {
    ::ma_trigger_h h;
    if (::ma_error_t err = ma_trigger_create_monthly_dow(&dowp, &h)) throw mileaccess::ma::utils::exception(err);
    
    return trigger(h, true);
}
    
inline trigger trigger::make_logon_trigger(ma_trigger_logon_policy_t &lp) {
    ::ma_trigger_h h;
    if (::ma_error_t err = ma_trigger_create_logon(&lp, &h)) throw mileaccess::ma::utils::exception(err);
    
    return trigger(h, true);
}
    
inline trigger trigger::make_system_startup_trigger(ma_trigger_systemstart_policy_t &sp) {
    ::ma_trigger_h h;
    if (::ma_error_t err = ma_trigger_create_system_start(&sp, &h)) throw mileaccess::ma::utils::exception(err);
    
    return trigger(h, true);
}

// a collection of triggers. No reference counting or memory mgmt on the embedded C handles!
struct trigger_list {
    typedef ma_trigger_list_h handle_type;

    trigger_list(handle_type h = 0) : handle(h){;}

    trigger_list(trigger_list const &o) : handle(o.handle) {;}

    trigger_list &operator=(trigger_list const &o) {
        this->handle = o.handle;
        return *this;
    }

    void add(trigger &tr);

    // remove by name/id, returns true if found and removed
    bool remove(const char *id);
    
    // find a trigger by id
    bool find(const char *id, trigger *tr);
    
    // number of triggers in this list
    std::size_t size() const;

    // get trigger by pos - (0 or 1) based?
    trigger at(std::size_t index) const;

    // returns the underlying C handle
    handle_type get() const;

protected:
    handle_type handle;
};

inline std::size_t trigger_list::size() const {
    ::size_t s;
    if (ma_error_t err = ma_trigger_list_size(get(), &s)) throw utils::exception(err);
    return s;
}

inline trigger trigger_list::at(std::size_t index) const {
    ma_trigger_t *t;
    if (ma_error_t err = ma_trigger_list_at(get(), index, &t)) throw utils::exception(err);
    return trigger(t,true);
}

inline trigger_list::handle_type trigger_list::get() const {
    return this->handle;
}

} } } // namespace mileaccess::ma::scheduler

#endif // MA_TRIGGER_HXX_INCLUDED
