#ifndef MA_SCHEDULER_TIME_HXX_INCLUDED
#define MA_SCHEDULER_TIME_HXX_INCLUDED

#include "ma/scheduler/ma_triggers.h"

namespace mileaccess { namespace ma { namespace scheduler {

struct time : ma_task_time_s {

};

} } } // mileaccess::ma::scheduler

#endif // MA_SCHEDULER_TIME_HXX_INCLUDED
