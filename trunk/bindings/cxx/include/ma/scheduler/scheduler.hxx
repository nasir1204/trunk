#ifndef MA_SCHEDULER_HXX_INCLUDED
#define MA_SCHEDULER_HXX_INCLUDED

#include "ma/client.hxx"
#include "ma/scheduler/task.hxx"
#include <vector>

namespace mileaccess { namespace ma { namespace scheduler {

using mileaccess::ma::client;

struct scheduler {

    scheduler(client const & cli);

    scheduler(scheduler const & o);

    void add_task(maybe_owned_task &t);

    maybe_owned_task get_task(char const *task_id);

    void remove_task(char const *task_id);

    void modify_task(maybe_owned_task &t);

    void update_status(char const *task_id, ::ma_task_exec_status_t status);

    void postpone(char const *task_id, size_t intervals);

    // on successful enumeration vector will be holding all the task handles and it is user responsibility to drop all tasks individual handles before vector.erase() called
    void enumerate(char const *product_id, char const *creator_id, char const *task_type, std::vector<maybe_owned_task *> &v);

    client const &cli;

private:
    scheduler &operator=(scheduler const &o);
};


inline scheduler::scheduler(client const & c) : cli(c) {}

inline scheduler::scheduler(scheduler const & o) : cli(o.cli) {}

inline void scheduler::add_task(maybe_owned_task &t) {
    if (ma_error_t err = ma_scheduler_add_task(cli.get(), t.get())) throw utils::exception(err);
    t.drop_ownership();
}

inline maybe_owned_task scheduler::get_task(char const *task_id) {
    ma_task_h h;
    if (ma_error_t err = ma_scheduler_get_task(cli.get(), task_id, &h)) throw utils::exception(err);
    return maybe_owned_task(h, true);
}

inline void scheduler::remove_task(char const *task_id) {
    if (ma_error_t err = ma_scheduler_remove_task(cli.get(), task_id)) throw utils::exception(err);
}

inline void scheduler::modify_task(maybe_owned_task &t) {
    if (ma_error_t err = ma_scheduler_alter_task(cli.get(), t.get())) throw utils::exception(err);
    t.drop_ownership();
}

inline void scheduler::update_status(char const *task_id, ::ma_task_exec_status_t status) {
    if (ma_error_t err = ma_scheduler_update_task_status(cli.get(), task_id, status)) throw utils::exception(err);
}

inline void scheduler::postpone(char const *task_id, size_t intervals) {
    if (ma_error_t err = ma_scheduler_postpone_task(cli.get(), task_id, intervals)) throw utils::exception(err);
}


inline void scheduler::enumerate(char const *product_id, char const *creator_id, char const *task_type, std::vector<maybe_owned_task *> &v) {
    ma_task_t **h_set = NULL;
    size_t no_of_tasks = 0;
    
    if (ma_error_t err = ma_scheduler_enumerate_task(cli.get(), product_id, creator_id, task_type, &h_set, &no_of_tasks)) throw utils::exception(err);
    else {
        if(h_set) {
            for(size_t itr = 0; itr < no_of_tasks; ++itr) {
                maybe_owned_task *t = new maybe_owned_task(h_set[itr], true);
                v.push_back(t);
            }
            (void)ma_scheduler_enumerate_task_release(h_set, &no_of_tasks);
        }
    }
}

} } } //namespace mileaccess::ma::scheduler


#endif
