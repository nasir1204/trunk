#ifndef MA_TASK_H_HXX_INCLUDED
#define MA_TASK_H_HXX_INCLUDED

#include "ma/variant.hxx"
#include "ma/scheduler/trigger.hxx"

#include "ma/ma_common.h"
#include "ma/scheduler/ma_task.h"


namespace mileaccess { namespace ma { namespace scheduler {

using mileaccess::ma::variant;

struct never_owned_task;
struct maybe_owned_task;

// task_base is a base class only (it cannot be instantiated directly). It does not deal with object ownership
struct task_base {

    typedef ma_task_h handle_type;

    // underlying C handle
    handle_type handle;

    // setting
    void set_setting(const char *path, const char *name, variant const &value);

    variant get_setting(const char *path , const char *name) const;

    void clear_setting();

    
    // retry policy
    void set_retry_policy(::ma_task_retry_policy_t retry_policy);

    ::ma_task_retry_policy_t get_retry_policy() const;

    
    // payload
    void set_app_payload(variant const &app_payload);

    variant get_app_payload() const;
        
    
    // repeat policy
    void set_repeat_policy(::ma_task_repeat_policy_t repeat_policy);

    ::ma_task_repeat_policy_t get_repeat_policy() const;

    
    // max runtime
    void set_max_run_time(ma_uint16_t max_run_time);
    
    ::ma_uint16_t get_max_run_time() const;

    
    // runtime limit
    void set_max_run_time_limit(ma_uint16_t max_run_time_limit);
    
    ::ma_uint16_t get_max_run_time_limit() const;

    
    // randomize policy
    void set_randomize_policy(::ma_task_randomize_policy_t randomize_policy);

    ::ma_task_randomize_policy_t get_randomize_policy() const;

    
    // priority
    void set_priority(::ma_task_priority_t priority);

    ::ma_task_priority_t get_priority() const;

    
    // conditions, OR'ed together from ma_task_cond_t bit values
    void set_conditions(::ma_uint32_t conds);

    ::ma_uint32_t get_conditions() const;
    
    
    // execution status
    void set_execution_status(::ma_task_exec_status_t status);

    ::ma_task_exec_status_t get_execution_status() const;

    
    // delay policy
    void set_delay_policy(::ma_task_delay_policy_t policy);

    ::ma_task_delay_policy_t get_delay_policy() const;
    
    
    // missed policy
    void set_missed_policy(::ma_task_missed_policy_t policy);

    ::ma_task_missed_policy_t get_missed_policy() const;

    
    // creator id
    void set_creator_id(const char *id);

    std::string get_creator_id() const;
    
    // software id
    void set_software_id(const char *id);

    std::string get_software_id() const;

    // name
    void set_name(const char *name);

    std::string get_name() const;

    // missed
    void set_missed(ma_bool_t missed);

    ma_bool_t get_missed() const;

    // type
    void set_type(const char *type);

    ::ma_task_state_t get_state() const;
    
    ::ma_task_time_t get_last_run_time() const;
    
    ::ma_task_time_t get_next_run_time() const;

    std::string get_id() const;
    
    std::string get_type() const;
   
    // trigger
    trigger_list get_trigger_list() const;

    void add_trigger(trigger &);
    
    void remove_trigger(const char *trigger_id);
    
    trigger find_trigger(const char *trigger_id);
    
    // time zone
    void set_time_zone(::ma_time_zone_t zone);
    
    ::ma_time_zone_t get_time_zone() const;
    
    maybe_owned_task clone() const;

    ::ma_task_h get() const;

protected:
    /**
     * creates a new C++ task object from the, lower level, ma_task_h C handle
     * @param h pointer to the underlying C handle
     * @param owns if set to true the task object takes ownership of the handle (and releases it upon destruction) otherwise the task does not take ownership
     */
    task_base(handle_type h);

    ~task_base() {};

private:
    // prevent copying etc
    task_base(task_base const &o);

    task_base &operator=(task_base const &o);
};

typedef task_base task;

// never owns the handle
struct never_owned_task : task_base {
    never_owned_task(handle_type h) : task_base(h) {}

    never_owned_task(never_owned_task const &o) : task_base(o.handle) {}

    never_owned_task &operator=(never_owned_task const & o) {
        this->handle = o.handle;
        return *this;
    }
};

// instances of maybe_owned_tasks are rare.
struct maybe_owned_task : task_base {

    maybe_owned_task(handle_type h, bool initially_owns) : task_base(h), is_owner(initially_owns) {
    }

    /**
     * creates fresh task. At some point the task should be added
     */
    static maybe_owned_task new_task(char const *task_id);

    // copy c'tor copies ownership to the new object. After this source object will not have ownership
    maybe_owned_task(maybe_owned_task const &o) : task_base(o.handle), is_owner(o.is_owner) {
         o.is_owner = false;
    }

    void close() {
        if (is_owner) {
            ma_task_release(this->handle);
        }
        this->handle = 0;
    }

    void drop_ownership() {
        is_owner = false;
    }

    ~maybe_owned_task() {
        close();
    }

    mutable bool is_owner;

private:
  
    // do not want to assign from another owned task
    maybe_owned_task &operator=(maybe_owned_task const &);
};


inline task_base::task_base(handle_type h) : handle(h) {
}

inline void task_base::set_setting(const char *path, const char *name, variant const &v) {
    if (ma_error_t err = ma_task_set_setting(get(), path, name, v.get())) throw utils::exception(err);
}

inline variant task_base::get_setting(const char *path, const char *name) const {
    ma_variant_t *value;
    if (ma_error_t err = ma_task_get_setting(get(), path, name, &value)) throw utils::exception(err);
    return variant(value, false);
}

inline void task_base::set_retry_policy(::ma_task_retry_policy_t retry_policy) {
    if (ma_error_t err = ma_task_set_retry_policy(get(), retry_policy)) throw utils::exception(err);
}

inline ::ma_task_retry_policy_t task_base::get_retry_policy() const {
    ::ma_task_retry_policy_t retry_policy;
    if (ma_error_t err = ma_task_get_retry_policy(get(), &retry_policy)) throw utils::exception(err);
    return retry_policy;
}
 
inline void task_base::set_repeat_policy(::ma_task_repeat_policy_t repeat_policy) {
    if (ma_error_t err = ma_task_set_repeat_policy(get(), repeat_policy)) throw utils::exception(err);
}

inline ::ma_task_repeat_policy_t task_base::get_repeat_policy() const {
    ::ma_task_repeat_policy_t repeat_policy;
    if (ma_error_t err = ma_task_get_repeat_policy(get(), &repeat_policy)) throw utils::exception(err);
    return repeat_policy;
}

inline void task_base::set_max_run_time(ma_uint16_t max_run_time) {
    if (ma_error_t err = ma_task_set_max_run_time(get(), max_run_time)) throw utils::exception(err);
}

inline ::ma_uint16_t task_base::get_max_run_time() const {
    ::ma_uint16_t max_run_time;
    if (ma_error_t err = ma_task_get_max_run_time(get(), &max_run_time)) throw utils::exception(err);
    return max_run_time;
}

inline void task_base::set_max_run_time_limit(ma_uint16_t max_run_time_limit) {
    if (ma_error_t err = ma_task_set_max_run_time_limit(get(), max_run_time_limit)) throw utils::exception(err);
}

inline ::ma_uint16_t task_base::get_max_run_time_limit() const {
    ::ma_uint16_t max_run_time_limit;
    if (ma_error_t err = ma_task_get_max_run_time_limit(get(), &max_run_time_limit)) throw utils::exception(err);
    return max_run_time_limit;
}

inline void task_base::set_randomize_policy(::ma_task_randomize_policy_t randomize_policy) {
    if (ma_error_t err = ma_task_set_randomize_policy(get(), randomize_policy)) throw utils::exception(err);
}

inline ::ma_task_randomize_policy_t task_base::get_randomize_policy() const {
    ::ma_task_randomize_policy_t randomize_policy;
    if (ma_error_t err = ma_task_get_randomize_policy(get(), &randomize_policy)) throw utils::exception(err);
    return randomize_policy;
}

inline void task_base::set_priority(::ma_task_priority_t priority) {
    if (ma_error_t err = ma_task_set_priority(get(), priority)) throw utils::exception(err);
}

inline ::ma_task_priority_t task_base::get_priority() const {
    ::ma_task_priority_t priority;
    if (ma_error_t err = ma_task_get_priority(get(), &priority)) throw utils::exception(err);
    return priority;
}
 
inline void task_base::set_conditions(::ma_uint32_t conds) {
    if (ma_error_t err = ma_task_set_conditions(get(), (ma_task_cond_t)conds)) throw utils::exception(err);
}

inline ::ma_uint32_t task_base::get_conditions() const {
    ::ma_uint32_t conds;
    if (ma_error_t err = ma_task_get_conditions(get(), &conds)) throw utils::exception(err);
    return conds;
}   
    
/*TODO- how to do this ? */
inline void task_base::set_execution_status(::ma_task_exec_status_t status) {
}

/*TODO- how to do this ? */
inline ::ma_task_exec_status_t get_execution_status()  {
    ::ma_task_exec_status_t status = MA_TASK_EXEC_SUCCESS;

    //if(ma_error_t err = ma_task_get_execution_status(get(), &status)) throw utils::exception(err);
    return status;
}


inline void task_base::set_delay_policy(::ma_task_delay_policy_t delay_policy) {
    if (ma_error_t err = ma_task_set_delay_policy(get(), delay_policy)) throw utils::exception(err);
}

inline ::ma_task_delay_policy_t task_base::get_delay_policy() const {
    ::ma_task_delay_policy_t delay_policy;
    if (ma_error_t err = ma_task_get_delay_policy(get(), &delay_policy)) throw utils::exception(err);
    return delay_policy;
}

inline void task_base::set_missed_policy(::ma_task_missed_policy_t missed_policy) {
    if (ma_error_t err = ma_task_set_missed_policy(get(), missed_policy)) throw utils::exception(err);
}

inline ::ma_task_missed_policy_t task_base::get_missed_policy() const {
    ::ma_task_missed_policy_t missed_policy;
    if (ma_error_t err = ma_task_get_missed_policy(get(), &missed_policy)) throw utils::exception(err);
    return missed_policy;
}
 
inline void task_base::set_creator_id(const char *id) {
    if (ma_error_t err = ma_task_set_creator_id(get(), id)) throw utils::exception(err);
}

inline std::string task_base::get_creator_id() const {
    const char *id = NULL;
    if (ma_error_t err = ma_task_get_creator_id(get(), &id)) throw utils::exception(err);
    return id;
}

inline void task_base::set_software_id(const char *software_id) {
    if (ma_error_t err = ma_task_set_software_id(get(), software_id)) throw utils::exception(err);
}

inline std::string task_base::get_software_id() const {
    const char *software_id = NULL;
    if (ma_error_t err = ma_task_get_software_id(get(), &software_id)) throw utils::exception(err);
    return software_id;
}

inline void task_base::set_name(const char *name) {
    if (ma_error_t err = ma_task_set_name(get(), name)) throw utils::exception(err);
}

inline void task_base::set_missed(ma_bool_t missed) {
    if (ma_error_t err = ma_task_set_missed(get(), missed)) throw utils::exception(err);
}

inline ma_bool_t task_base::get_missed() const {
    ma_bool_t missed = MA_FALSE;
    if (ma_error_t err = ma_task_get_missed(get(), &missed)) throw utils::exception(err);
    return missed;
}

inline std::string task_base::get_name() const {
    const char *name = NULL;
    if (ma_error_t err = ma_task_get_name(get(), &name)) throw utils::exception(err);
    return name;
}

inline void task_base::set_type(const char *type) {
    if (ma_error_t err = ma_task_set_type(get(), type)) throw utils::exception(err);
}

inline std::string task_base::get_type() const {
    const char *type = NULL;
    if (ma_error_t err = ma_task_get_type(get(), &type)) throw utils::exception(err);
    return type;
}

inline ::ma_task_state_t task_base::get_state() const {
    ::ma_task_state_t state;
    if (ma_error_t err = ma_task_get_state(get(), &state)) throw utils::exception(err);
    return state;
}
 
inline ::ma_task_time_t task_base::get_last_run_time() const {
    ::ma_task_time_t last_run_time;
    if (ma_error_t err = ma_task_get_last_run_time(get(), &last_run_time)) throw utils::exception(err);
    return last_run_time;
}
 
inline ::ma_task_time_t task_base::get_next_run_time() const {
    ::ma_task_time_t next_run_time;
    if (ma_error_t err = ma_task_get_next_run_time(get(), &next_run_time)) throw utils::exception(err);
    return next_run_time;
}

inline std::string task_base::get_id() const {
    const char *id = NULL;
    if (ma_error_t err = ma_task_get_id(get(), &id)) throw utils::exception(err);
    return id;
}

inline trigger_list task_base::get_trigger_list() const {
    ::ma_trigger_list_t *tl; 
    if (ma_error_t err = ma_task_get_trigger_list(get(),&tl)) throw utils::exception(err);
    return trigger_list(tl);
}

inline void task_base::add_trigger(trigger &trigger)  {
    if (ma_error_t err = ma_task_add_trigger(get(),trigger.get())) throw utils::exception(err);
}

inline void task_base::remove_trigger(const char *trigger_id) {
    if (ma_error_t err = ma_task_remove_trigger(get(),trigger_id)) throw utils::exception(err);
}

inline trigger task_base::find_trigger(const char *trigger_id) {
    ::ma_trigger_t *trigger; 
    if (ma_error_t err = ma_task_find_trigger(get(),trigger_id, &trigger)) throw utils::exception(err);
    return trigger;
}

inline void task_base::set_time_zone(::ma_time_zone_t zone) {
    if (ma_error_t err = ma_task_set_time_zone(get(),zone)) throw utils::exception(err);
}
 
inline ::ma_time_zone_t task_base::get_time_zone() const{
    ::ma_time_zone_t zone; 
    if (ma_error_t err = ma_task_get_time_zone(get(),&zone)) throw utils::exception(err);
    return zone;
}   

inline maybe_owned_task task_base::clone() const {
    handle_type new_task;
    if (ma_error_t err = ma_task_copy(get(),&new_task)) throw utils::exception(err);
    return maybe_owned_task(new_task, true);
}

inline ::ma_task_h task_base::get() const {
    return handle;
}




// 
/*static*/
inline maybe_owned_task maybe_owned_task::new_task(char const *task_id) {
    handle_type handle;
    if (ma_error_t err = ma_task_create(task_id, &handle)) throw utils::exception(err);
    return maybe_owned_task(handle, true);
}



}}} // namespace mileaccess::ma::scheduler

#endif // MA_TASK_H_HXX_INCLUDED
