#ifndef MA_TASKHANDLER_HXX_INCLUDED
#define MA_TASKHANDLER_HXX_INCLUDED

#include "ma/client.hxx"
#include "ma/scheduler/task.hxx"

#include "ma/scheduler/ma_scheduler.h"

#include <string>

namespace mileaccess { namespace ma { namespace scheduler {

using mileaccess::ma::client;

// interface a task handler must implement
struct task_handler {

public:

    void register_task_handler(char const *product_id, char const *task_type);

    void unregister_task_handler(char const *product_id, char const *task_type);

	virtual ~task_handler() {}
protected:
    task_handler(client const &c) : cli(c) {}

protected:
    // callbacks must be implemented by your derived class
    virtual ::ma_error_t on_execute(client const &cli, std::string const &product_id, std::string const &task_type, task &t) = 0;

    virtual ::ma_error_t on_stop(client const &cli, std::string const &product_id, std::string const &task_type, task &t) = 0;

    virtual ::ma_error_t on_remove(client const &cli, std::string const &product_id, std::string const &task_type, task &t) = 0;

    virtual ::ma_error_t on_update_status(client const &cli, std::string const &product_id, std::string const &task_type, task &t) = 0;

private:

    static ::ma_error_t execute_cb(::ma_client_t *client, const char *product_id, const char *task_type, ::ma_task_t *task, void *cb_data);

    static ::ma_error_t stop_cb(::ma_client_t *client, const char *product_id, const char *task_type, ::ma_task_t *task, void *cb_data);

    static ::ma_error_t remove_cb(::ma_client_t *client, const char *product_id, const char *task_type, ::ma_task_t *task, void *cb_data);

    static ::ma_error_t update_status_cb(::ma_client_t *client, const char *product_id, const char *task_type, ::ma_task_t *task, void *cb_data);

    static const ::ma_scheduler_task_callbacks_t *get_methods() {
        static const ::ma_scheduler_task_callbacks_t methods = {&execute_cb, &stop_cb, &remove_cb, &update_status_cb};
        return &methods;
    }

    client const &cli;
};

inline void task_handler::register_task_handler(char const *product_id, char const *task_type) {
    if (ma_error_t err = ma_scheduler_register_task_callbacks(cli.get(), product_id, task_type, get_methods(), this)) throw utils::exception(err);
}

inline void task_handler::unregister_task_handler(char const *product_id, char const *task_type) {
    if (ma_error_t err = ma_scheduler_unregister_task_callbacks(cli.get(), product_id, task_type)) throw utils::exception(err);
}


inline ::ma_error_t task_handler::execute_cb(::ma_client_t *client, const char *product_id, const char *task_type, ::ma_task_t *t, void *cb_data) {
    try {
        task_handler *self = reinterpret_cast<task_handler *>(cb_data);
        scheduler::never_owned_task task(t);
        return self->on_execute(self->cli, std::string(product_id), std::string(task_type), task);
    } catch (...) {
    }

    return MA_ERROR_UNEXPECTED;    
}

inline ::ma_error_t task_handler::stop_cb(::ma_client_t *client, const char *product_id, const char *task_type, ::ma_task_t *t, void *cb_data) {
    try {
        task_handler *self = reinterpret_cast<task_handler *>(cb_data);
        scheduler::never_owned_task task(t);
        return self->on_stop(self->cli, std::string(product_id), std::string(task_type), task);
    } catch (...) {
    }

    return MA_ERROR_UNEXPECTED;    

}

inline ::ma_error_t task_handler::remove_cb(::ma_client_t *client, const char *product_id, const char *task_type, ::ma_task_t *t, void *cb_data) {
    try {
        task_handler *self = reinterpret_cast<task_handler *>(cb_data);
        scheduler::never_owned_task task(t);
        return self->on_remove(self->cli, std::string(product_id), std::string(task_type), task);
    } catch (...) {
    }

    return MA_ERROR_UNEXPECTED;    

}

inline ::ma_error_t task_handler::update_status_cb(::ma_client_t *client, const char *product_id, const char *task_type, ::ma_task_t *t, void *cb_data) {
    try {
        task_handler *self = reinterpret_cast<task_handler *>(cb_data);
        scheduler::never_owned_task task(t);
        return self->on_update_status(self->cli, std::string(product_id), std::string(task_type), task);
    } catch (...) {
    }

    return MA_ERROR_UNEXPECTED;    

}


}}} // namespace mileaccess::ma::scheduler

#endif // MA_TASKHANDLER_HXX_INCLUDED
