#ifndef MA_UPDATER_UI_PROVIDER_HXX_INCLUDED
#define MA_UPDATER_UI_PROVIDER_HXX_INCLUDED

#include "ma/updater/ma_updater_ui_provider.h"
#include "ma/client.hxx"
#include "ma/updater/updater_handler.hxx"
#include "ma/utils/exception.hxx"

#include <string>

namespace mileaccess { namespace ma { namespace updater {

struct ui_request;
struct ui_response;
struct ui_provider;
struct ui_registrar;
struct event_info;

struct ui_provider {

    virtual 
    ::ma_error_t on_show_ui(mileaccess::ma::client const &client, std::string &session_id, ::ma_updater_ui_type_t type, ui_request const &request, ui_response &response) = 0; 

    /* UTF8 encoded strings will be passed */
    virtual
    ::ma_error_t on_progress(mileaccess::ma::client const &client, std::string &session_id, ::ma_int32_t event_type, ::ma_int32_t progress_step , ::ma_int32_t max_progress_step, const char *progress_message) = 0;
	    
    /* UTF8 encoded strings will be passed */
    virtual
    ::ma_error_t on_end_ui(mileaccess::ma::client const &client, std::string &session_id, const char *title, const char *end_message, const char *count_down_message, ::ma_int64_t count_down_value) = 0;

	/* UTF8 encoded strings will be passed */
    virtual
    ::ma_error_t on_event(mileaccess::ma::client const &client, std::string &session_id, updater_event const &ei) = 0;

	virtual ~ui_provider() {}

};

struct ui_registrar {
    /*!
     * ui_registrar c'tor.
     * @param client for which the ui_registrar is associated
     * 
     * @note client can also be set later, but before the call to register_ui_provider()
     */
    ui_registrar(mileaccess::ma::client *client = 0); 

    ui_registrar(::ma_client_t *client) ;


    /*!
     * Destructor. Automatically unregisters
     */
    ~ui_registrar();

    /*!
     * Register the specified ui provider with the message bus. After this, you may receive update notifications
     */
    void register_ui_provider(ui_provider &provider, std::string &software_id, ::ma_msgbus_callback_thread_options_t to);

    /*!
     * unregisters the one and only ui_provider
     */
    void unregister_ui_provider(std::string &software_id); 

	/*!
     * get the name of the ui provider
    */
	std::string get_ui_provider_name();

    /*!
    *   send show ui response 
    */
    void post_ui_provider_response(ui_response &response) ;

    ::ma_client_t *client;

    ui_provider *provider;

	std::string software_id ;

private:
    static ::ma_error_t on_show_ui(::ma_client_t *ma_client, void *cb_data,  const char *session_id, ::ma_updater_ui_type_t type, const ::ma_updater_show_ui_request_t *ui_request, ::ma_updater_show_ui_response_t *ui_response); 

    static ::ma_error_t on_progress(::ma_client_t *ma_client, void *cb_data, const char *session_id, ::ma_int32_t event_type, ::ma_int32_t progress_step , ::ma_int32_t max_progress_step, const char *progress_message);

    static ::ma_error_t on_end_ui(::ma_client_t *ma_client, void *cb_data, const char *session_id, const char *title, const char *end_message, const char *count_down_message, ::ma_int64_t count_down_value);

	static ::ma_error_t on_event(::ma_client_t *ma_client, void *cb_data, const char *session_id, const ::ma_updater_event_t *event_info);

    static ::ma_updater_ui_provider_callbacks_t const *callbacks() throw() {
        static const ::ma_updater_ui_provider_callbacks_t vtab = {
            &on_show_ui,
            &on_progress,
			&on_end_ui,
            &on_event            
        };
        return &vtab;
    }
};


struct ui_request {

    ui_request(::ma_updater_show_ui_request_t const *s) : handle(s) {

    }

    // Note, the below methods throw exceptions if they fail, you should have try/catch somewhere in your code 

    std::string get_title() const;

    std::string get_message() const;

    std::string get_countdown_message() const;

	::ma_uint32_t get_initiator_type() const;

    ::ma_uint32_t get_countdown_value() const;

protected:
    ::ma_updater_show_ui_request_t const *handle;
};

struct ui_response {

    ui_response(::ma_updater_show_ui_response_t *s) : handle(s) {

    }

    // Note, the below methods throw exceptions if they fail, you should have try/catch somewhere in your code 

    void set_postpone_timeout(::ma_uint32_t) ;

    void set_return_code(::ma_updater_ui_return_code_t) ;

    ::ma_updater_show_ui_response_t *get() { return handle ; }

protected:
    ::ma_updater_show_ui_response_t *handle;
};


inline ui_registrar::ui_registrar(mileaccess::ma::client *c) : client(c ? c->get() : 0), provider(0) {

}

inline ui_registrar::ui_registrar(::ma_client_t *c) : client(c), provider(0) {

}


inline 
ui_registrar::~ui_registrar() {
    if (provider) {
        unregister_ui_provider(this->software_id) ;
    }
}

inline void 
ui_registrar::register_ui_provider(ui_provider &provider, std::string &software_id, ::ma_msgbus_callback_thread_options_t to) {
    if (!client) throw utils::exception(MA_ERROR_PRECONDITION);
    if (ma_error_t err = ma_updater_register_ui_provider_callbacks(client, software_id.c_str(), to, callbacks(), this)) throw utils::exception(err);
	this->software_id.assign(software_id) ;
	this->provider = &provider;
}

inline void 
ui_registrar::unregister_ui_provider(std::string &software_id) {
    if (client) {
        ma_updater_unregister_ui_provider_callbacks(client, software_id.c_str());
        this->provider = 0;
    }
}

inline std::string
ui_registrar::get_ui_provider_name(){
	const char *name = NULL;
	if (!client) throw utils::exception(MA_ERROR_PRECONDITION);
    if (ma_error_t err = ma_updater_get_ui_provider_name(client, software_id.c_str(), &name)) throw utils::exception(err);	
	return std::string(name ? name : "");
}

inline void
ui_registrar::post_ui_provider_response(ui_response &response) {
    ma_updater_show_ui_response_post(client, response.get()) ;
}

inline ::ma_error_t ui_registrar::on_show_ui(::ma_client_t *ma_client, void *cb_data,  const char *session_id, ::ma_updater_ui_type_t type, const ::ma_updater_show_ui_request_t *request, ::ma_updater_show_ui_response_t *response) {
    ui_registrar *self = reinterpret_cast<ui_registrar*>(cb_data);
    try {
		ui_response r(response);
		mileaccess::ma::client c(ma_client);
		std::string s(session_id);
		return self->provider->on_show_ui(c, s, type, ui_request(request), r);		
    } catch(...) {
    }
    return MA_ERROR_UNEXPECTED;
} 

inline ::ma_error_t ui_registrar::on_progress(::ma_client_t *ma_client, void *cb_data, const char *session_id, ::ma_int32_t event_type , ::ma_int32_t progress_step , ::ma_int32_t max_progress_step, const char *progress_message) {
    ui_registrar *self = reinterpret_cast<ui_registrar*>(cb_data);
    try {
		mileaccess::ma::client c(ma_client);
		std::string s(session_id);
		return self->provider->on_progress(c, s, event_type, progress_step, max_progress_step, progress_message);
    } catch(...) {
    }
    return MA_ERROR_UNEXPECTED;
}


inline ::ma_error_t ui_registrar::on_end_ui(::ma_client_t *ma_client, void *cb_data, const char *session_id, const char *title, const char *end_message, const char *count_down_message, ::ma_int64_t count_down_value) {
    ui_registrar *self = reinterpret_cast<ui_registrar*>(cb_data);
    try {
		mileaccess::ma::client c(ma_client);
		std::string s(session_id);
		return self->provider->on_end_ui(c, s, title, end_message, count_down_message, count_down_value);
    } catch(...) {
    }
    return MA_ERROR_UNEXPECTED;

}

inline ::ma_error_t ui_registrar::on_event(::ma_client_t *ma_client, void *cb_data, const char *session_id, const ::ma_updater_event_t *ev_info) {
    ui_registrar *self = reinterpret_cast<ui_registrar*>(cb_data);
    try {
        updater_event evi(ev_info);
		mileaccess::ma::client c(ma_client);
		std::string s(session_id);
        return self->provider->on_event(c, s, evi);
    } catch(...) {
    }
    return MA_ERROR_UNEXPECTED;

}

inline	::ma_uint32_t ui_request::get_initiator_type() const {
	ma_uint32_t value;
	if (ma_error_t err = ma_updater_show_ui_request_get_initiator_type(this->handle, &value)) throw utils::exception(err);
    return value;
}

inline std::string ui_request::get_title() const {
    const char *title;
    if (ma_error_t err = ma_updater_show_ui_request_get_title(this->handle, &title)) throw utils::exception(err);
    return std::string(title?title:"");
}

inline std::string ui_request::get_message() const {
    const char *message;
    if (ma_error_t err = ma_updater_show_ui_request_get_message(this->handle, &message)) throw utils::exception(err);
    return std::string(message?message:"");
}

inline std::string ui_request::get_countdown_message() const {
    const char *message;
    if (ma_error_t err = ma_updater_show_ui_request_get_count_down_message(this->handle, &message)) throw utils::exception(err);
    return std::string(message?message:"");
}

inline ::ma_uint32_t ui_request::get_countdown_value() const {
    ma_uint32_t value;
    if (ma_error_t err = ma_updater_show_ui_request_get_count_down_value(this->handle, &value)) throw utils::exception(err);
    return value;

}

inline void ui_response::set_return_code(::ma_updater_ui_return_code_t return_code) {
    (void)ma_updater_show_ui_response_set_return_code(this->handle, return_code) ;
    return ;
}

inline void ui_response::set_postpone_timeout(::ma_uint32_t timeout) {
    (void)ma_updater_show_ui_response_set_postpone_timeout(this->handle, timeout) ;
    return ;
}


}}} // namespace mileaccess::ma::updater


#endif // MA_UPDATER_UI_PROVIDER_HXX_INCLUDED
