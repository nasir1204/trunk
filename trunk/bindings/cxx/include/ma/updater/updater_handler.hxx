#ifndef MA_UPDATER_HANDLER_HXX_INCLUDED
#define MA_UPDATER_HANDLER_HXX_INCLUDED

#include "ma/updater/ma_updater.h"
#include "ma/client.hxx"
#include "ma/variant.hxx"
#include <string>

namespace mileaccess { namespace ma { namespace updater {

using mileaccess::ma::client;

struct updater_event {
    // Note, the below methods throw exceptions if they fail, you should have try/catch somewhere in your code 
    updater_event(::ma_updater_event_t const *upd_event);

    ~updater_event();

    /*!
     * Returns updater session id
     */
	std::string get_session_id();

	/*!
     * Returns updater product id
     */
	std::string get_product_id();

	/*!
     * Returns updater locale id
     */
	std::string get_locale();

	/*!
     * Returns updater type
     */
	std::string get_update_type();
	
	/*!
     * Returns updater new version
     */
	std::string get_new_version();

	/*!
     * Returns updater time
     */
	std::string get_date_time();	

	/*!
     * Returns update state
     */
	::ma_update_state_t  get_update_state();

	/*!
     * Returns update error
     */
	ma_int32_t  get_update_error();
    
private:
    const ::ma_updater_event_t *upd_event;
};

inline updater_event::updater_event(::ma_updater_event_t const *upd_event): upd_event(upd_event){
	if(!upd_event) throw utils::exception(MA_ERROR_PRECONDITION);
}

inline updater_event::~updater_event(){}

inline std::string updater_event::get_session_id(){	
	const char  *cstr = NULL;
	if(ma_error_t err = ma_updater_event_get_session_id(this->upd_event, &cstr)) throw utils::exception(err);
	return std::string(cstr ? cstr : "");
}

inline std::string updater_event::get_product_id(){	
	const char  *cstr = NULL;
	if(ma_error_t err = ma_updater_event_get_product_id(this->upd_event, &cstr)) throw utils::exception(err);
	return std::string(cstr ? cstr : "");
}

inline std::string updater_event::get_locale(){	
	const char  *cstr = NULL;
	if(ma_error_t err = ma_updater_event_get_locale(this->upd_event, &cstr)) throw utils::exception(err);
	return std::string(cstr ? cstr : "");
}

inline std::string updater_event::get_update_type(){	
	const char  *cstr = NULL;
	if(ma_error_t err = ma_updater_event_get_update_type(this->upd_event, &cstr)) throw utils::exception(err);
	return std::string(cstr ? cstr : "");
}

inline std::string updater_event::get_new_version(){	
	const char  *cstr = NULL;
	if(ma_error_t err = ma_updater_event_get_new_version(this->upd_event, &cstr)) throw utils::exception(err);
	return std::string(cstr ? cstr : "");
}

inline std::string updater_event::get_date_time(){
	const char  *cstr = NULL;
	if(ma_error_t err = ma_updater_event_get_date_time(this->upd_event, &cstr)) throw utils::exception(err);
	return std::string(cstr ? cstr : "");
}

inline ::ma_update_state_t  updater_event::get_update_state(){	
	ma_update_state_t state;	
	if(ma_error_t err = ma_updater_event_get_update_state(this->upd_event, &state)) throw utils::exception(err);
	return state;
}


inline ma_int32_t updater_event::get_update_error(){
	ma_int32_t error;
	if(ma_error_t err = ma_updater_event_get_update_error(this->upd_event, &error)) throw utils::exception(err);
	return error;
}

// interface a updater handler must implement
struct updater_handler {

	updater_handler(client const &clip);

	virtual ~updater_handler();

protected:
	
    // callbacks must be implemented by your derived class
    virtual ::ma_error_t notify_cb(client const &cli, std::string const &product_id,  std::string const &update_type, ::ma_update_state_t state, std::string const &message, std::string const& extra_info, ::ma_updater_product_return_code_t &return_code) = 0;

    virtual ::ma_error_t set_cb(client const &cli, std::string const &product_id, std::string const &update_type, std::string const &key, mileaccess::ma::variant const &value, ::ma_updater_product_return_code_t &return_code) = 0;

    virtual ::ma_error_t get_cb(client const &cli, std::string const &product_id, std::string const &update_type, std::string const &key, mileaccess::ma::variant &value, ::ma_updater_product_return_code_t &return_code) = 0;

	virtual ::ma_error_t updater_event_cb(client const &cli, std::string const &product_id, const updater_event &upd_event) = 0;

private:
	
	static ::ma_error_t c_notify_cb(::ma_client_t *ma_client, const char *product_id, void *cb_data, const char *update_type, ma_update_state_t state, const char *message, const char *extra_info, ::ma_updater_product_return_code_t *return_code);

    static ::ma_error_t c_set_cb(::ma_client_t *ma_client, const char *product_id, void *cb_data, const char *update_type, const char *key, ma_variant_t *value, ::ma_updater_product_return_code_t *return_code);

    static ::ma_error_t c_get_cb(::ma_client_t *ma_client, const char *product_id, void *cb_data, const char *update_type, const char *key, ma_variant_t **value, ::ma_updater_product_return_code_t *return_code);

    static ::ma_error_t c_updater_event_cb(::ma_client_t *ma_client, const char *product_id, void *cb_data, const ::ma_updater_event_t *upd_event);

	client const &cli;
public:
    static const ::ma_updater_callbacks_t *get_methods() {
        static const ::ma_updater_callbacks_t methods = {&c_notify_cb, &c_set_cb, &c_get_cb, &c_updater_event_cb};
        return &methods;
    }

};

inline updater_handler::updater_handler(client const &clip):cli(clip){}

inline updater_handler::~updater_handler() {}


inline ::ma_error_t updater_handler::c_notify_cb(::ma_client_t *ma_client, const char *product_id, void *cb_data, const char *update_type, ::ma_update_state_t state, const char *message, const char *extra_info, ::ma_updater_product_return_code_t *return_code) {
    try {
        updater_handler *self = reinterpret_cast<updater_handler *>(cb_data);
        ::ma_updater_product_return_code_t ret;
        ma_error_t err;
        if(MA_OK == (err = self->notify_cb(self->cli, std::string(product_id), std::string(update_type), state, std::string(message), std::string(extra_info), ret)))
            *return_code = ret;
        
        return err;
        
    } catch (...) {
    }

    return MA_ERROR_UNEXPECTED;    
}

inline ::ma_error_t updater_handler::c_set_cb(::ma_client_t *ma_client, const char *product_id, void *cb_data, const char *update_type, const char *key, ma_variant_t *value, ::ma_updater_product_return_code_t *return_code) {
    try {
        updater_handler *self = reinterpret_cast<updater_handler *>(cb_data);
        ::ma_updater_product_return_code_t ret;
        ma_error_t err;
        if(MA_OK == (err = self->set_cb(self->cli, std::string(product_id), std::string(update_type), std::string(key), mileaccess::ma::variant(value), ret))) 
            *return_code = ret;
        
        return err;
        
    } catch (...) {
    }

    return MA_ERROR_UNEXPECTED;    
}

inline ::ma_error_t updater_handler::c_get_cb(::ma_client_t *ma_client, const char *product_id, void *cb_data, const char *update_type, const char *key, ma_variant_t **value, ::ma_updater_product_return_code_t *return_code) {
    try {
        updater_handler *self = reinterpret_cast<updater_handler *>(cb_data);
        ::ma_updater_product_return_code_t ret;
        mileaccess::ma::variant v;
        ma_error_t err;
        if(MA_OK == (err = self->get_cb(self->cli, std::string(product_id), std::string(update_type), std::string(key), v, ret)))  {
            *return_code = ret;
            ma_variant_add_ref(*value = v.get());
        }
        
        return err;
        
    } catch (...) {
    }

    return MA_ERROR_UNEXPECTED;    
}

inline ::ma_error_t updater_handler::c_updater_event_cb(::ma_client_t *ma_client, const char *product_id, void *cb_data, const ::ma_updater_event_t *event_info) {
	 try {
        updater_handler *self = reinterpret_cast<updater_handler *>(cb_data);
        updater_event upd_event(event_info);
		return self->updater_event_cb(self->cli, std::string(product_id), upd_event);        
        
    } catch (...) {
    }
    return MA_ERROR_UNEXPECTED;    
}

}}} // namespace mileaccess::ma::updater

#endif // MA_UPDATER_HANDLER_HXX_INCLUDED
