#ifndef MA_UPDATER_HXX_INCLUDED
#define MA_UPDATER_HXX_INCLUDED

#include "ma/updater/ma_updater.h"
#include "ma/client.hxx"
#include "ma/updater/updater_handler.hxx"
#include "ma/scheduler/task.hxx"


namespace mileaccess { namespace ma { namespace updater {

using mileaccess::ma::client;

struct update_request {
    // Note, the below methods throw exceptions if they fail, you should have try/catch somewhere in your code 
    update_request(::ma_updater_update_type_t);

    ~update_request();
    
	/*!
     * set product list
     */
    void set_product_list(mileaccess::ma::array &product_list);

	/*!
     * Returns product list 
     */
    array get_product_list();

	/*!
     * set locale
     */
    void set_locale(::ma_uint32_t locale);

	/*!
     * get locale id
     */
    ::ma_uint32_t get_locale();

	/*!
     * get update type
     */
    ::ma_updater_update_type_t get_type();

	/*!
     * get updater request
     */
    ::ma_updater_update_request_t *get() { return this->request;}

	/*!
     * set ui option.
     */
    void set_uioption(bool show_ui);
	
	/*!
     * get ui option.
     */
    bool get_uioption();

	/*!
     * set initiator_type.
     */
    void set_initiator_type(ma_updater_initiator_type_t initiator_type);
	
	/*!
     * get initiator_type.
     */
    ma_updater_initiator_type_t get_initiator_type();
	
	/*!
     * set ui provider name, which is registered for the UI messages.
     */
    void set_ui_provider_name(std::string &ui_provider_name);
	
	/*!
     * get ui provider name.
     */
    std::string get_ui_provider_name();		

private:
    ::ma_updater_update_request_t *request;
};

struct update_task {
    
    // Note, the below methods throw exceptions if they fail, you should have try/catch somewhere in your code 
    update_task(client const &c, char const *task_id);

    ~update_task();
        
	/*!
     * set product list
     */
    void set_product_list(mileaccess::ma::array &product_list);

	/*!
     * set locale
     */
    void set_locale(::ma_uint32_t locale);

	/*!
     * returns the underlying c task handle
     */
    ::ma_task_t *get() { return this->m_task; }

private:
    ::ma_task_t *m_task;

    client const &cli;
};


struct updater {

    updater(client const &c) : cli(c) {}

	/*!
     * start update for given session id
     */
    ::ma_error_t start_update(update_request &request, mileaccess::ma::buffer &session_id);

	/*!
     * stop update for given session id
     */
    ::ma_error_t stop_update(mileaccess::ma::buffer &session_id);

	/*!
     * get update state for given session id
     */
	::ma_error_t get_update_state(mileaccess::ma::buffer &session_id, ::ma_update_state_t &state);

	/*!
     * register product handler
     */
	::ma_error_t register_handler(std::string const &product_id, updater_handler const &handler);

	/*!
     * un register product handler
     */
	::ma_error_t unregister_handler(std::string const &product_id);	

private:
	private:		 

    client const &cli;

    updater &operator=(updater const &o);
};


inline update_request::update_request(::ma_updater_update_type_t type) {
    if (ma_error_t err = ma_updater_update_request_create(type, &request)) throw utils::exception(err);
}

inline update_request::~update_request(){
    if (ma_error_t err = ma_updater_update_request_release(request)) throw utils::exception(err);
}
    
inline void update_request::set_product_list(mileaccess::ma::array &product_list) {
    if (ma_error_t err = ma_updater_update_request_set_product_list(request, product_list.get())) throw utils::exception(err);
}

inline array update_request::get_product_list() {
    ma_array_t *arr = NULL;
    if (ma_error_t err = ma_updater_update_request_get_product_list(request, &arr)) throw utils::exception(err);
    return array(arr, false);
}

inline void update_request::set_locale(::ma_uint32_t locale) {
    if (ma_error_t err = ma_updater_update_request_set_locale(request, locale)) throw utils::exception(err);
}


inline ::ma_uint32_t update_request::get_locale() {
    ma_uint32_t locale;
    if (ma_error_t err = ma_updater_update_request_get_locale(request, &locale)) throw utils::exception(err);
    return locale;
}

inline ::ma_updater_update_type_t update_request::get_type() {
    ma_updater_update_type_t type;
    if (ma_error_t err = ma_updater_update_request_get_type(request, &type)) throw utils::exception(err);
    return type;
}

inline void update_request::set_uioption(bool show_ui){
	if (ma_error_t err = ma_updater_update_request_set_uioption(request, show_ui)) throw utils::exception(err);    
}
	
inline bool update_request::get_uioption(){
	ma_bool_t is_show_ui = MA_FALSE;
	if (ma_error_t err = ma_updater_update_request_get_uioption(request, &is_show_ui)) throw utils::exception(err);    
	return (is_show_ui ? true : false);
}

inline void update_request::set_initiator_type(ma_updater_initiator_type_t initiator_type){
	if (ma_error_t err = ma_updater_update_request_set_initiator_type(request, initiator_type)) throw utils::exception(err);    
}
	
inline ma_updater_initiator_type_t update_request::get_initiator_type(){
	ma_updater_initiator_type_t in_type;
	if (ma_error_t err = ma_updater_update_request_get_initiator_type(request, &in_type)) throw utils::exception(err);    
	return in_type;
}
	
inline void update_request::set_ui_provider_name(std::string &ui_provider_name){
	if (ma_error_t err = ma_updater_update_request_set_ui_provider_name(request, ui_provider_name.c_str())) throw utils::exception(err);    
}
	
inline std::string update_request::get_ui_provider_name(){
	const char *name = NULL;
	if(ma_error_t err = ma_updater_update_request_get_ui_provider_name(request, &name)) throw utils::exception(err);    
	return std::string(name);
}

inline update_task::update_task(client const &c, char const *task_id):cli(c) {
    if (ma_error_t err = ma_updater_update_task_create(c.get(), task_id, &m_task)) throw utils::exception(err);
}

inline update_task::~update_task() {
    if (ma_error_t err = ma_task_release(m_task)) throw utils::exception(err);
}

inline void update_task::set_product_list(mileaccess::ma::array &product_list) {
    if (ma_error_t err = ma_updater_update_task_set_product_list(m_task, product_list.get())) throw utils::exception(err);
}


inline void update_task::set_locale(::ma_uint32_t locale) {
    if (ma_error_t err = ma_updater_update_task_set_locale(m_task, locale)) throw utils::exception(err);
}

inline ::ma_error_t updater::register_handler(std::string const &product_id, updater_handler const &handler) {
    return ma_updater_register_callbacks(cli.get(), product_id.c_str(), handler.get_methods(), (void*)&handler);
}

inline ::ma_error_t updater::unregister_handler(std::string const &product_id) {
    return ma_updater_unregister_callbacks(cli.get(), product_id.c_str());
}

inline ::ma_error_t updater::start_update(update_request &request, mileaccess::ma::buffer &session_id) {
    try {
        ma_error_t err = ma_updater_start_update(cli.get(), request.get(), (ma_buffer_t **)session_id.get());
        return err;
    }
    catch(...) {
    }
    return MA_ERROR_UNEXPECTED;
}
inline ::ma_error_t updater::get_update_state(mileaccess::ma::buffer &session_id, ::ma_update_state_t &state) {
    try {
        ma_error_t err = ma_updater_get_update_state(cli.get(),session_id.get(), &state);
        return err;
    }
    catch(...) {
    }
    return MA_ERROR_UNEXPECTED;
}


inline ::ma_error_t updater::stop_update(mileaccess::ma::buffer &session_id) {
    try {
        ma_error_t err = ma_updater_stop_update(cli.get(), session_id.get());
        return err;
    }
    catch(...) {
    }
    return MA_ERROR_UNEXPECTED;
}



} } } /* namespace mileaccess::ma::updater; */


#endif /*MA_UPDATER_HXX_INCLUDED */
