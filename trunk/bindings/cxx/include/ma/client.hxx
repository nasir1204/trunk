#ifndef MA_CLIENT_HXX_INCLUDED
#define MA_CLIENT_HXX_INCLUDED

#include "ma/ma_client.h"
#include "ma/logger.hxx"
#include "ma/msgbus/msgbus.hxx"
#include "ma/utils/auto_handle.hxx"
#include "ma/utils/exception.hxx"

#include <string>


namespace mileaccess { namespace ma {

/* first, define a traits class to be used with utils::auto_handle */
struct client_traits {
    typedef ::ma_client_h handle_type;

    static handle_type invalid_handle_value() {
        return handle_type(NULL);
    }

    static void close(handle_type h) {       
    }
};

struct msgbus; // Not yet defined. Not copyable!

struct client_logger {
	client_logger(){}

    virtual ~client_logger() {}

	virtual void on_logmessage(::ma_log_severity_t sev, const std::string &module_name, const std::string &log_message) = 0;
};



struct client {
    typedef utils::auto_handle<client_traits> client_auto_handle;

    /**
     * Constructor for a new client
     * @param product_id string that identifies your product
     */
    explicit client(const char *product_id);

    /*!
     * creates a shim C++ interface on top of an existing handle
     */
    explicit client(::ma_client_t *c, bool add_ref = true);

    ~client() {
        if (!owner) {
            // prevents
            c.handle = 0; 
        }
        else
            ma_client_release(c.get());
    }

    /**
     * Starts client
     * 
     */
    void start();

    /**
     * Stops client
     * 
     */
    void stop();

    /**
     * Sets a logger for use by the client and associated functionality
     * @param logger The logger object to use
     */
    void set_logger(client_logger const &logger);

	void set_passphrase_provider(passphrase_provider const &p);

    void set_thread_option(::ma_msgbus_callback_thread_options_t thread_option);

    ::ma_msgbus_callback_thread_options_t get_thread_option() const;

    /**
     * Gets the underlying msgbus 
     */
    ::ma_msgbus_t *get_msgbus() const;
    
    /**
     * Gets the underlying C ma_client_h handle. Reference count will NOT be incremented as a result of this.
     */
    client_auto_handle::handle_type get() const;

protected:
	static void client_log_message_cb(::ma_log_severity_t severity, const char *module_name, const char *log_message, void *user_data);

public:
    client_auto_handle c;

    bool owner;

private:
	client_logger *cli_logger;
	passphrase_provider *p ;

	static ma_error_t  passphrase_provider_cb(::ma_msgbus_t *msgbus, void *cb_data, unsigned char *passphrase, size_t *passphrase_size);
    
    /* prevent accidental copying and assignment */
    client(client const&);
    client const &operator=(client const &);
};

inline client::client(const char *product_id) : owner(true), cli_logger(NULL) {
    if (::ma_error_t e = ma_client_create(product_id, &c)) throw utils::exception(e);    
}

inline client::client(ma_client_t *c, bool add_ref) : c(c), owner(!add_ref), cli_logger(NULL) {
}

inline void client::start() {
    if (::ma_error_t e = ma_client_start(get())) throw utils::exception(e);
}

inline void client::stop() {
    if (ma_error_t err = ma_client_stop(get())) throw utils::exception(err);
}

inline ::ma_client_h client::get() const {
    return this->c.get();
}

inline ::ma_msgbus_t *client::get_msgbus() const {
	::ma_msgbus_t *msgbus = NULL;
	if(ma_error_t err = ma_client_get_msgbus(get(), &msgbus)) throw utils::exception(err);
	return msgbus;
}

inline void client::set_logger(client_logger const &logger) {
	this->cli_logger = const_cast<client_logger*>(&logger);
	if (ma_error_t err = ma_client_set_logger_callback(get(), &client::client_log_message_cb, this)) throw utils::exception(err);
}


// callback
inline ma_error_t client::passphrase_provider_cb(::ma_msgbus_t *bus, void *cb_data, unsigned char *passphrase, size_t *passphrase_size) {
    try {
        client *cli = static_cast<client*>(cb_data);
        return cli->p ?  cli->p->on_provide_passphrase(passphrase, passphrase_size) : MA_ERROR_INVALIDARG;
    } catch (...) {

    }
    return MA_ERROR_UNEXPECTED;
}

inline void client::set_passphrase_provider(passphrase_provider const &p) {
	this->p = const_cast<passphrase_provider*>(&p);
	if (ma_error_t err = ma_client_set_msgbus_passphrase_callback(get(), &client::passphrase_provider_cb, this)) throw utils::exception(err);
}


inline void client::set_thread_option(::ma_msgbus_callback_thread_options_t thread_option) {
    if (ma_error_t err = ma_client_set_thread_option(get(), thread_option)) throw utils::exception(err);
}


inline ::ma_msgbus_callback_thread_options_t client::get_thread_option() const {
    ::ma_msgbus_callback_thread_options_t t;
    if (ma_error_t err = ma_client_get_thread_option(get(), &t)) throw utils::exception(err);
    return t;
}

inline void client::client_log_message_cb(::ma_log_severity_t severity, const char *module_name, const char *log_message, void *user_data){
	client *cli = static_cast<client*>(user_data);
	if(cli->cli_logger)
		cli->cli_logger->on_logmessage(severity, std::string(module_name), std::string(log_message));	
}

struct notification_handler {
	 /**
     * call this to register your notification handler
     */
    void register_handler(client const &c) ;

    /**
     * and this to unregister
     */
    void unregister_handler(client const &c) ;

protected:
    /**
     * protected constructor, must be invoked by your derived class
     */
    notification_handler(){}

    virtual ~notification_handler() {}

    /**
     * invoked by MA when there is a any notification sent, PP's should implement this 
     * @param cli The client used for registration. Most likely you can ignore this
     * @param type notification type MASTART, MASTOP PP's should take action based on the type(i.e., start/stop the client)
     *
     * @return status value. Please return MA_OK on successor a non 0 value to indicate an error 
     */
	
    virtual void on_notification(client const &cli, ::ma_client_notification_t type) = 0 ;

private:
    static void notification_handler_cb(::ma_client_t *, ::ma_client_notification_t type, void *) ;
    
    notification_handler(notification_handler const &) ;

    notification_handler const &operator=(notification_handler const &) ;
} ;

inline void notification_handler::register_handler(client const &cli) {
	if (::ma_error_t err = ma_client_register_notification_callback(cli.get(), &notification_handler::notification_handler_cb, (void *)this)) throw utils::exception(err);
}

inline void notification_handler::unregister_handler(client const &cli) {
    if (::ma_error_t err = ma_client_unregister_notification_callback(cli.get())) throw utils::exception(err) ;
}

/*static*/ 
inline void notification_handler::notification_handler_cb(::ma_client_t *clientp, ::ma_client_notification_t type, void *cb_data) {
	notification_handler *self = (notification_handler *)cb_data;
    try {
		client cli(clientp, true);
        self->on_notification(cli, type) ;
    } 
	catch (...) {
    } ;
}

}} /* namespace mileaccess::ma */

#endif /* MA_CLIENT_HXX_INCLUDED */
