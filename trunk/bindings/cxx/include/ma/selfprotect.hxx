#ifndef MA_SELFPROTECT_HPP_INCLUDED
#define MA_SELFPROTECT_HPP_INCLUDED

#define MA_SELF_PROTECT_ENABLE				1

#define MA_SELF_PROTECT_DISABLE				0

#define MA_SELF_PROTECT_PRODUCT_MA			0x1

#define MA_SELF_PROTECT_PRODUCT_SYSCORE		0x2

//Defines for manageability settings
/// McAfee, Inc. Namespace
namespace mileaccess_com {
/// McAfee Agent Namespace
namespace MA {
/// protection Namespace	
namespace protection {
	
class SelfProtect
{
	SelfProtect(){};
	~SelfProtect(){};
	public:
	/**
		* Enables or disables self protection for 0 or more products
		*   The following products are currently supported
		*   MA, Syscore
 
		* @param products Combination (OR'ed together) of MA_SELF_PROTECT_PRODUCT_xxx that should be enabled or disabled
		* @param enable Specifies whether specified products should be enabled or disabled
 
		* @return 0 on success, otherwise a failure code
		*/ 
	static int enable(unsigned products, bool enable);

	/**
		* Retrieves the products for which self protection is enabled.
		*
		* @param products Pointer to variable that receives the list of product where self protection is enabled. The corresponding bits will be lit
		*
		* @return 0 on success, otherwise a failure code
		*/
	static int query_status(unsigned *products);
};


}}}

#endif /* MA_SELFPROTECT_HPP_INCLUDED */
