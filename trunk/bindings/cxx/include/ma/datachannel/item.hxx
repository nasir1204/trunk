#ifndef MA_DATACHANNEL_ITEM_HXX_INCLUDED
#define MA_DATACHANNEL_ITEM_HXX_INCLUDED

#include "ma/datachannel/ma_datachannel_item.h"
#include "ma/client.hxx"
#include "ma/variant.hxx"
#include "ma/utils/exception.hxx"

#include <string>

namespace mileaccess { namespace ma { namespace datachannel {

using mileaccess::ma::client;

struct item {
    
    // Note, the below methods throw exceptions if they fail, you should have try/catch somewhere in your code 
    item(char const *message_type, mileaccess::ma::variant &payload);

    item(::ma_datachannel_item_t *c);

    ~item();

    //returns the underlying c handler
    ::ma_datachannel_item_t *get() { return this->c; }
    
    mileaccess::ma::buffer get_message_type()  const;

    mileaccess::ma::buffer get_origin()  const;

    mileaccess::ma::variant get_payload()  const;

    ::ma_int64_t get_ttl() const;

    ::ma_int64_t get_correlation_id() const;

    bool get_option(::ma_uint32_t &option) const;

    void set_origin(const char *origin);

    void set_payload(mileaccess::ma::variant &payload);

    void set_ttl(::ma_int64_t ttl);

    void set_correlation_id(::ma_int64_t correlation_id);
    
    void set_option(::ma_uint32_t option, bool b_enable);

private :
    ::ma_datachannel_item_t *c;

    mutable bool is_owner;
};

inline item::item(char const *message_type, mileaccess::ma::variant &payload):is_owner(true) {
       if (ma_error_t err = ma_datachannel_item_create(message_type, payload.get(), &c)) throw utils::exception(err);
}

inline item::item(::ma_datachannel_item_t *c):c(c),is_owner(false){}

inline item::~item() {
    if(is_owner) {
        if (ma_error_t err = ma_datachannel_item_release(c)) throw utils::exception(err);
    }
}

inline mileaccess::ma::buffer item::get_message_type()  const {
    ::ma_buffer_t *buffer = NULL;
    if (ma_error_t err = ma_datachannel_item_get_message_type(c, &buffer)) throw utils::exception(err);
    return mileaccess::ma::buffer(buffer, false);
}

inline mileaccess::ma::buffer item::get_origin()  const {
    ::ma_buffer_t *buffer = NULL;
    if (ma_error_t err = ma_datachannel_item_get_origin(c, &buffer)) throw utils::exception(err);
    return mileaccess::ma::buffer(buffer, false);
}

inline mileaccess::ma::variant item::get_payload()  const {
    ::ma_variant_t *v = NULL;
    if (ma_error_t err = ma_datachannel_item_get_payload(c, &v)) throw utils::exception(err);
    return mileaccess::ma::variant(v);
}

inline ::ma_int64_t item::get_ttl() const {
    ::ma_int64_t ttl;
    if (ma_error_t err = ma_datachannel_item_get_ttl(c, &ttl)) throw utils::exception(err);
    return ttl;
}

inline ::ma_int64_t item::get_correlation_id() const {
    ::ma_int64_t id;
    if (ma_error_t err = ma_datachannel_item_get_correlation_id(c, &id)) throw utils::exception(err);
    return id;
}

inline bool item::get_option(::ma_uint32_t &option) const {
    ::ma_bool_t b_enable = true;
    if (ma_error_t err = ma_datachannel_item_get_option(c, &option)) throw utils::exception(err);
    return (MA_TRUE == b_enable) ? true: false;
}

inline void item::set_origin(const char *origin) {
    if (ma_error_t err = ma_datachannel_item_set_origin(c, origin)) throw utils::exception(err);
}

inline void item::set_payload(mileaccess::ma::variant &payload) {
    if (ma_error_t err = ma_datachannel_item_set_payload(c, payload.get())) throw utils::exception(err);
}

inline void item::set_ttl(::ma_int64_t ttl) {
    if (ma_error_t err = ma_datachannel_item_set_ttl(c, ttl)) throw utils::exception(err);
}

inline void item::set_correlation_id(::ma_int64_t correlation_id) {
    if (ma_error_t err = ma_datachannel_item_set_correlation_id(c, correlation_id)) throw utils::exception(err);
}
    
inline void item::set_option(::ma_uint32_t option, bool b_enable) {
    if (ma_error_t err = ma_datachannel_item_set_option(c, option, b_enable)) throw utils::exception(err);
}


} } } // mileaccess::ma::datachannel

#endif // MA_DATACHANNEL_ITEM_HXX_INCLUDED
