#ifndef MA_DATACHANNEL_HXX_INCLUDED
#define MA_DATACHANNEL_HXX_INCLUDED

#include "ma/datachannel/ma_datachannel.h"

#include "ma/client.hxx"
#include "ma/variant.hxx"
#include "ma/datachannel/item.hxx"
#include "ma/utils/exception.hxx"

#include <string>

namespace mileaccess { namespace ma { namespace datachannel {

using mileaccess::ma::client;

// base class that your datachannel handler must derive from 
// and implement the on_datachannel_notification and on_datachannel_message method
struct datachannel {

    datachannel(mileaccess::ma::client const &c) : cli(c) {}

    ~datachannel() {}

    // Note, the below methods throw exceptions if they fail, you should have try/catch somewhere in your code 

    void subscribe(std::string const &product_id, std::string const &dc_message_id, ::ma_datachannel_subscription_type_t subscription_type);

    void unsubscribe(std::string const &product_id, std::string const &dc_message_id);

    void async_send(std::string const &product_id, mileaccess::ma::datachannel::item &dc_message);
    
private:
    mileaccess::ma::client const &cli;

    // prevent copy semantics
    datachannel(datachannel const&);
    
    datachannel &operator=(datachannel const&);
};

inline void datachannel::subscribe(std::string const &product_id, std::string const &dc_message_id, ::ma_datachannel_subscription_type_t subscription_type) {
    if (ma_error_t err = ma_datachannel_subscribe(cli.get(), product_id.c_str(), dc_message_id.c_str(), subscription_type) ) throw utils::exception(err);
}

inline void datachannel::unsubscribe(std::string const &product_id, std::string const &dc_message_id) {
    if (ma_error_t err = ma_datachannel_unsubscribe(cli.get(), product_id.c_str(), dc_message_id.c_str()) ) throw utils::exception(err);
}

inline void datachannel::async_send(std::string const &product_id, mileaccess::ma::datachannel::item &dc_message) {
    if (ma_error_t err = ma_datachannel_async_send(cli.get(), product_id.c_str(), dc_message.get())) throw utils::exception(err);
}


} } } // mileaccess::ma::datachannel

#endif // MA_DATACHANNEL_HXX_INCLUDED
