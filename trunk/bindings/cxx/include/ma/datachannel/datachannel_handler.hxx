#ifndef MA_DATACHANNEL_HANDLER_HXX_INCLUDED
#define MA_DATACHANNEL_HANDLER_HXX_INCLUDED

#include "ma/datachannel/ma_datachannel.h"

#include "ma/client.hxx"
#include "ma/variant.hxx"
#include "ma/datachannel/item.hxx"
#include "ma/utils/exception.hxx"

#include <string>

namespace mileaccess { namespace ma { namespace datachannel {

using mileaccess::ma::client;

// base class that your datachannel handler must derive from 
// and implement the on_datachannel_notification and on_datachannel_message method
struct datachannel_handler {

    //! registers a notification handler. Throws a utils::exception on error
    void register_notification_handler(std::string const &product_id);

    //! unregisters notification handler
    void unregister_notification_handler(std::string const &product_id);

    //! registers a message handler . Throws a utils::exception on error
    void register_datachannel_message_handler(std::string const &product_id);

    //! unregisters message handler
    void unregister_datachannel_message_handler(std::string const &product_id);

	virtual ~datachannel_handler() {}

protected:
    //! protected constructor for your derived class to call
    datachannel_handler(mileaccess::ma::client  const &c) : cli(c) {}

    //! you must implement this method in your derived class and use the above registration API to get notifications
    virtual void on_datachannel_notification(mileaccess::ma::client  const &c, std::string const &product_id, std::string const &message_id, ::ma_int64_t correlation_id, ::ma_datachannel_item_notification_t status, ::ma_error_t reason) = 0;

    //! you must implement this method in your derived class and use the above registration API to get messages
    virtual void on_datachannel_message(mileaccess::ma::client const &c, std::string const &product_id, std::string const &message_id, ::ma_int64_t correlation_id, mileaccess::ma::datachannel::item message) = 0;

private:

    static void c_on_datachannel_notification_cb(::ma_client_t *ma_client, const char *product_id, const char *message_id, ::ma_int64_t correlation_id, ::ma_datachannel_item_notification_t status, ::ma_error_t reason,void *cb_data);

    static void c_on_datachannel_message_cb(::ma_client_t *ma_client, const char *product_id, const char *message_id, ::ma_int64_t correlation_id, ::ma_datachannel_item_t *dc_message, void *cb_data);

    client const &cli;

    // prevent copy semantics
    datachannel_handler(datachannel_handler const&);
    
    datachannel_handler &operator=(datachannel_handler const&);
};

inline void datachannel_handler::register_notification_handler(const std::string &product_id) {
    if (ma_error_t err = ma_datachannel_register_notification_callback(this->cli.get(), product_id.c_str(), &c_on_datachannel_notification_cb, this) ) throw utils::exception(err);
}

inline void datachannel_handler::unregister_notification_handler(const std::string &product_id) {
    if (ma_error_t err = ma_datachannel_unregister_notification_callback(this->cli.get(), product_id.c_str()) ) throw utils::exception(err);
}

inline void datachannel_handler::register_datachannel_message_handler(const std::string &product_id) {
    if (ma_error_t err = ma_datachannel_register_message_handler_callback(this->cli.get(), product_id.c_str(), &c_on_datachannel_message_cb, this) ) throw utils::exception(err);
}

inline void datachannel_handler::unregister_datachannel_message_handler(const std::string &product_id) {
    if (ma_error_t err = ma_datachannel_unregister_message_handler_callback(this->cli.get(), product_id.c_str()) ) throw utils::exception(err);
}

inline void datachannel_handler::c_on_datachannel_notification_cb(::ma_client_t *ma_client, const char *product_id, const char *message_id, ::ma_int64_t correlation_id, ::ma_datachannel_item_notification_t status, ma_error_t reason, void *cb_data) {
    try {
        datachannel_handler *self = reinterpret_cast<datachannel_handler *>(cb_data);
        self->on_datachannel_notification(self->cli, std::string(product_id), std::string(message_id), correlation_id, status, reason);
    } catch (...) {
    }
}

inline void datachannel_handler::c_on_datachannel_message_cb(::ma_client_t *ma_client, const char *product_id, const char *message_id, ::ma_int64_t correlation_id, ::ma_datachannel_item_t *dc_message, void *cb_data) {
    try {
        datachannel_handler *self = reinterpret_cast<datachannel_handler *>(cb_data);
        self->on_datachannel_message(self->cli, std::string(product_id), std::string(message_id), correlation_id, mileaccess::ma::datachannel::item(dc_message));
    } catch (...) {
    }
}


} } } // mileaccess::ma::datachannel

#endif // MA_DATACHANNEL_HANDLER_HXX_INCLUDED
