#ifndef MA_MESSAGE_HXX_INCLUDED
#define MA_MESSAGE_HXX_INCLUDED

#include "ma/ma_message.h"
#include "ma/variant.hxx"
#include "ma/message_auth_info.hxx"

#include "ma/utils/auto_handle.hxx"
#include "ma/utils/exception.hxx"

#include <ctime>
#include <string>

namespace mileaccess { namespace ma {

struct message_traits {

    typedef ma_message_h handle_type;

    static handle_type invalid_handle_value() {
        return handle_type(0);
    };

    static void close(handle_type h) {
        ::ma_message_release(h);
    }

    static void add_ref(handle_type h) {
        ::ma_message_add_ref(h);
    }
};




/**
 * simple wrapper over the ma_message type
 */
struct message {
    
    typedef ma::utils::copyable_handle<message_traits> message_auto_handle;
    
    typedef message_auto_handle::handle_type handle_type;
    
    /**
     * creates a new, empty message. 
     */
    message();

    /**
     * creates a new message from the low level C handle 
     */
    message(handle_type h, bool add_ref = true) throw();

    /**
     * creates a new message from a conforming variant. Not all variants are convertible
     *
     */
    message(variant const &v);

    /* compiler generated copy c'tor and assignment operator are ok */

    /**
     * gets this message's timestamp
     */
    std::time_t get_timestamp() const;

    /**
     * gets this message's type
     */
    ::ma_message_type_t get_message_type() const;

    /**
     * sets a named property value
     */
    void set_property(const char *key, const char *value);

    /**
     * gets a property value for the specified key
     */
    std::string get_property(const char *key) const;

    /**
     * sets the payload for this message
     */
    void set_payload(variant const &payload);

    /**
     * well, gets the payload
     */
    variant get_payload() const;

    /**
     * returns this message turned into a variant
     */
    variant to_variant() const;

    /**
     * returns the auth info object
     */
    message_auth_info get_auth_info() const;

	message clone();
public:
    message_auto_handle m;
};


inline message::message() {
    if (::ma_error_t e = ma_message_create(&m)) throw exception(e);
}

inline message::message(handle_type h, bool add_ref) throw() : m(h, add_ref) {

}

inline message::message(variant const &v) {
    if (::ma_error_t e = ma_message_from_variant(v.get(), &m)) throw utils::exception(e);
}

inline std::time_t message::get_timestamp() const {
    std::time_t ts;
    if (::ma_error_t e = ma_message_get_timestamp(m.get(), &ts)) throw utils::exception(e);
    return ts;
}

inline ::ma_message_type_t message::get_message_type() const {
    ::ma_message_type_t mt;
    if (::ma_error_t e = ma_message_get_message_type(m.get(), &mt)) throw utils::exception(e);
    return mt;
}

inline void message::set_property(const char *key, const char *value) {
    if (::ma_error_t e = ma_message_set_property(m.get(), key, value)) throw utils::exception(e);
}

inline std::string message::get_property(const char *key) const {
    char const *p;
    if (::ma_error_t e = ma_message_get_property(m.get(), key, &p)) throw utils::exception(e);
    return std::string(p);
}

inline void message::set_payload(variant const &payload) {
    if (::ma_error_t e = ma_message_set_payload(m.get(), payload.get())) throw utils::exception(e);
}

inline variant message::get_payload() const {
    variant v(0, false);
    if (::ma_error_t e = ma_message_get_payload(m.get(), &v)) throw utils::exception(e);
    return v;
}

inline variant message::to_variant() const {
    variant v(0, false);
    if (::ma_error_t e = ma_message_as_variant(m.get(), &v)) throw utils::exception(e);
    return v;
}

inline message_auth_info message::get_auth_info() const {
    ::ma_message_auth_info_t *auth_info = NULL;
    if (::ma_error_t e = ma_message_get_auth_info(m.get(), &auth_info)) throw utils::exception(e);
    return message_auth_info(auth_info, true);
}

inline message message::clone() {
	message dest;
	if(::ma_error_t e = ma_message_clone(m.get(), (dest.m.addr_of()))) throw utils::exception(e);
	return message(dest);
}

}};


#endif /* MA_MESSAGE_HXX_INCLUDED */
