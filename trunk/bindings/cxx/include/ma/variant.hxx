#ifndef MA_VARIANT_HXX_INCLUDED
#define MA_VARIANT_HXX_INCLUDED

#include "ma/ma_variant.h"

#include "ma/utils/auto_handle.hxx"
#include "ma/buffer.hxx"

#include <string>
#include <vector>
#include <exception>

namespace mileaccess { namespace ma {

struct variant_traits {

    typedef ma_variant_h handle_type;
    
    static handle_type invalid_handle_value() {
        return handle_type(0);
    }

    static void close(handle_type h) {
        ::ma_variant_release(h);
    }

    static void add_ref(handle_type h) {
        ::ma_variant_add_ref(h);
    }
};

struct table_traits {

    typedef ma_table_h handle_type;
    
    static handle_type invalid_handle_value() {
        return handle_type(0);
    };

    static void close(handle_type h) {
        ::ma_table_release(h);
    }

    static void add_ref(handle_type h) {
        ::ma_table_add_ref(h);
    }
};

struct array_traits {

    typedef ma_array_h handle_type;
    
    static handle_type invalid_handle_value() {
        return handle_type(0);
    };

    static void close(handle_type h) {
        ::ma_array_release(h);
    }

    static void add_ref(handle_type h) {
        ::ma_array_add_ref(h);
    }
};


typedef ma::utils::copyable_handle<variant_traits> variant_copyable_handle;
typedef ma::utils::copyable_handle<table_traits> table_copyable_handle;
typedef ma::utils::copyable_handle<array_traits> array_copyable_handle;

struct variant;
struct table;
struct array;

struct const_variant;
struct const_table;
struct const_array;

/*!
 * wrapper over ma_variant. Allows copying by applying reference counting on the underlying ma_variant_h handle
 */
struct variant : variant_copyable_handle {

    /*!
     * Default c'tor
     * Constructs a variant of type MA_VARTYPE_NULL
     */
    variant();

    /*!
     * Copy c'tor
     */
    variant(variant const &) throw();

    /*!
     * creates a C++ variant from the underlying C handle.
     * @param h Handle to an existing ma_variant
     * @param add_ref Specifies if the reference count should be bumped also. If add_ref is set to true, the caller still assumes ownership of the original handle and must release it
     */
    explicit variant(handle_type h, bool add_ref = true);

    /*!
     * create from uint8
     */
    explicit variant(ma_uint8_t v);

    /*!
     * create from int8
     */
    explicit variant(ma_int8_t v);


    /*!
     * create from uint16
     */
    explicit variant(ma_uint16_t v);

    /*!
     * create from int16
     */
    explicit variant(ma_int16_t v);


    /*!
     * create from 32 bit unsigned integer
     */
    explicit variant(ma_uint32_t v);
    
    /*!
     * create from 32 bit integer
     */
    explicit variant(ma_int32_t v);


    /*!
     * create from 64 bit unsigned integer
     */
    explicit variant(ma_uint64_t v);
    
    /*!
     * constructs a variant from 64 bit integer
     * resulting type will be MA_VARTYPE_INT64
     */
    explicit variant(ma_int64_t v);

    /*!
     * constructs a variant from a float
     * resulting type will be MA_VARTYPE_FLOAT
     */
    explicit variant(float v);

    /*!
     * constructs a variant from a double
     * resulting type will be MA_VARTYPE_DOUBLE
     */
    explicit variant(double v);


    /*!
     * create from a (sub)string, ascii or utf-8
     */
    explicit variant(const char *v, std::size_t length = std::size_t(-1));

    /*!
     * create from portion of a std::string
     */
    explicit variant(std::string const &v, std::size_t pos = 0, std::size_t length = std::string::npos);

    /*!
     * create from a wide character string
     */
    explicit variant(const wchar_t *v, std::size_t length = std::size_t(-1));

    /*!
     * create from a binary
     */
    explicit variant(const ma_uint8_t *v, std::size_t length);

    /*!
     * create from portion of a std::wstring, defaults to entire string
     */
    explicit variant(std::basic_string<wchar_t> const &v, std::size_t pos, std::size_t length = std::wstring::npos);

    /*!
     * Construct a new variant from a table
     */
    explicit variant(table const &t); 

    /*!
     * Constructs a variant from an existing array
     */
    explicit variant(array const &a);

    /*!
     * Returns this variant's type, this is deprecated API, use is_type instead.
     */
    ::ma_vartype_e type() const;

	/*!
     * Checks if the variant's type is type of e.
     */
    bool is_type(::ma_vartype_e e) const;
	
    /*!
     * assignment, defined for all types that has a corresponding constructor
     * NOTE, takes arguments by reference, this may not be the most efficient for simple types
     */
    template <typename T>
    variant &operator=(T const &o);

    /*!
     * swaps this variant with the specified variant
     */
    void swap(variant &o) throw();
};

bool operator==(variant const &l, variant const &r);

struct table : table_copyable_handle {
    
    /*!
     * Default c'tor, creates a new, empty, table object. 
     */
    table();

    /*!
     * creates table object from the underlying C handle
     * @param h Handle to an existing table
     * @param add_ref If true, takes a ref count, you still maintain ownership of the specified C handle. If add_ref is false, the ownership is transferred to the C++ variant and you should not release the handle
     */ 
    explicit table(handle_type h, bool add_ref = true);

    /*!
     * Copy c'tor. This will just take a reference to the copied-from object
     */
    table(table const &o);

    table &operator=(table const &o) throw() {
        table tmp(o);
        swap(tmp);
        return *this;
    }

    void set_entry(const char *key, variant const &value);

    void set_entry(const wchar_t *key, variant const &value);

    variant get_entry(const char *key) const;

    variant get_entry(const wchar_t *key) const;

    /*!
     * retrieves entry for specified key as variant. variant must contain a string
     */
    variant get_entry(variant const &key) const;


    /*!
     * returns an array containing the keys for this table
     */
    array keys() const;

    void swap(table &o) throw();
};

bool operator==(table const &l, table const &r);

struct array : array_copyable_handle {

    array();

    array(handle_type h, bool add_ref = true);

    array(array const &o);

    array &operator=(array const &o);

    /*!
     * adds an element to the end
     * returns *this, so you can do a.push().push() for whatever it's worth
     */
    array &push(variant const &e);

    /*!
     * returns the number of elements
     */
    std::size_t size() const;

    /*!
     * returns the element at the specified positon (index appears to be 1-based, not 0-based!)
     * if the specified index is out of range, an exception will be thrown
     */
    variant at(std::size_t index) const;

    /*!
     * retrieves the element at the specified position (same as at())
     */
    variant operator [](size_t index) const {return at(index);}

    /*!
     * enumerates over every element
     * T must have function call operator taking something that variant& can be converted into, for example variant& :-)
     */
    template <typename T>
    void for_each(T t);

    void swap(array &o) throw();
};

bool operator==(array const &l, array const &r);


/* * * * get method to extract values from a variant * * * */
template <typename T>
T get(variant const&);

template <>
inline ma_uint8_t get<ma_uint8_t>(variant const &v) {
    ma_uint8_t t;
    if (ma_error_t e = ma_variant_get_uint8(v.get(), &t)) throw exception(e);
    return t;
}

template <>
inline ma_int8_t get<ma_int8_t>(variant const &v) {
    ma_int8_t t;
    if (ma_error_t e = ma_variant_get_int8(v.get(), &t)) throw exception(e);
    return t;
}

template <>
inline ma_uint16_t get<ma_uint16_t>(variant const &v) {
    ma_uint16_t t;
    if (ma_error_t e = ma_variant_get_uint16(v.get(), &t)) throw exception(e);
    return t;
}

template <>
inline ma_int16_t get<ma_int16_t>(variant const &v) {
    ma_int16_t t;
    if (ma_error_t e = ma_variant_get_int16(v.get(), &t)) throw exception(e);
    return t;
}

template <>
inline ma_uint32_t get<ma_uint32_t>(variant const &v) {
    ma_uint32_t t;
    if (ma_error_t e = ma_variant_get_uint32(v.get(), &t)) throw exception(e);
    return t;
}

template <>
inline ma_int32_t get<ma_int32_t>(variant const &v) {
    ma_int32_t t;
    if (ma_error_t e = ma_variant_get_int32(v.get(), &t)) throw exception(e);
    return t;
}

template <>
inline ma_uint64_t get<ma_uint64_t>(variant const &v) {
    ma_uint64_t t;
    if (ma_error_t e = ma_variant_get_uint64(v.get(), &t)) throw exception(e);
    return t;
}

template <>
inline ma_int64_t get<ma_int64_t>(variant const &v) {
    ma_int64_t t;
    if (ma_error_t e = ma_variant_get_int64(v.get(), &t)) throw exception(e);
    return t;
}

template <>
inline float get<float>(variant const &v) {
    float t;
    if (ma_error_t e = ma_variant_get_float(v.get(), &t)) throw exception(e);
    return t;
}

template <>
inline double get<double>(variant const &v) {
    double t;
    if (ma_error_t e = ma_variant_get_double(v.get(), &t)) throw exception(e);
    return t;
}

template <>
inline std::string get<std::string>(variant const &v) {
    buffer buf;
    if (ma_error_t e = ma_variant_get_string_buffer(v.get(), &buf))  throw exception(e);
    return std::string(buf.data(), buf.size());
}

template <>
inline std::wstring get<std::wstring>(variant const &v) {
    wbuffer wbuf;
    if (ma_error_t e = ma_variant_get_wstring_buffer(v.get(), &wbuf))  throw exception(e);
    return std::wstring(wbuf.data(), wbuf.size());
}

template <>
inline const unsigned char *get<const unsigned char *>(variant const &v) {
    buffer buf;
    if (ma_error_t e = ma_variant_get_raw_buffer(v.get(), &buf))  throw exception(e);
    return buf.raw_data();
}

template <>
inline table get<table>(variant const &v) {
    ma_table_h h;
    if (ma_error_t e = ma_variant_get_table(v.get(), &h)) throw exception(e);
    return table(h, false); // false => take ownership
}

template <>
inline array get<array>(variant const &v) {
    ma_array_h h;
    if (ma_error_t e = ma_variant_get_array(v.get(), &h)) throw exception(e);
    return array(h, false); // false => take ownership
}

template <>
inline std::vector<ma_uint8_t> get< std::vector<ma_uint8_t> >(variant const &v) {
    buffer buf;
    if (ma_error_t e = ma_variant_get_raw_buffer(v.get(), &buf)) throw exception(e);
    return std::vector<ma_uint8_t>(buf.data(), 1 + buf.data() + buf.size() ); /* Note, adding 1 as our mileaccess::ma::buffer uses ma_buffer_get_string which never includes the last byte*/
}


inline void swap(variant &v1, variant &v2) {
    v1.swap(v2);
}

inline bool operator==(variant const &l, variant const &r) {
    ::ma_bool_t result;
    if (ma_error_t e = ma_variant_is_equal(l.get(), r.get(), &result)) throw exception(e);
    return MA_FALSE != result;
}

struct const_variant {

    typedef ma_variant_t const *handle_type;

    const_variant(handle_type h) : handle(h) {;}

    handle_type handle;
};

/* * * variant methods implementation * * * */

inline variant::variant() {
    if (ma_error_t e = ma_variant_create(addr_of())) throw exception(e);
}


inline variant::variant(variant const &o) throw() : variant_copyable_handle(o) {
}

inline variant::variant(variant::handle_type h, bool add_ref /* = true */) : variant_copyable_handle(h, add_ref) {
}


inline variant::variant(ma_uint8_t v) {
    if (ma_error_t e = ma_variant_create_from_uint8(v,addr_of())) throw exception(e);
}

inline variant::variant(ma_int8_t v) {
    if (ma_error_t e = ma_variant_create_from_int8(v,addr_of())) throw exception(e);
}

inline variant::variant(ma_uint16_t v) {
    if (ma_error_t e = ma_variant_create_from_uint16(v,addr_of())) throw exception(e);
}

inline variant::variant(ma_int16_t v) {
    if (ma_error_t e = ma_variant_create_from_int16(v,addr_of())) throw exception(e);
}

inline variant::variant(ma_uint32_t v) {
    if (ma_error_t e = ma_variant_create_from_uint32(v,addr_of())) throw exception(e);
}

inline variant::variant(ma_int32_t v) {
    if (ma_error_t e = ma_variant_create_from_int32(v,addr_of())) throw exception(e);
}

inline variant::variant(ma_uint64_t v) {
    if (ma_error_t e = ma_variant_create_from_uint64(v,addr_of())) throw exception(e);
}

inline variant::variant(ma_int64_t v) {
    if (ma_error_t e = ma_variant_create_from_int64(v,addr_of())) throw exception(e);
}

inline variant::variant(float v) {
    if (ma_error_t e = ma_variant_create_from_float(v,addr_of())) throw exception(e);
}

inline variant::variant(double v) {
    if (ma_error_t e = ma_variant_create_from_double(v,addr_of())) throw exception(e);
}


inline variant::variant(const char *v, std::size_t length /*= std::size_t(-1)*/) {
    if (ma_error_t e = ma_variant_create_from_string(v, length, addr_of())) throw exception(e);
}    

inline variant::variant(std::string const &v, size_t pos, std::size_t length /*= std::npos*/) {
    if (ma_error_t e = ma_variant_create_from_string(v.data() + pos,  std::string::npos == length ? v.length() - pos : length, addr_of())) throw exception(e);
}

inline variant::variant(const wchar_t *v, std::size_t length /*= std::size_t(-1)*/) {
    if (ma_error_t e = ma_variant_create_from_wstring(v, length, addr_of()))  throw exception(e);
}    

inline variant::variant(std::wstring const &v, size_t pos, std::size_t length /*= std::npos*/) {
    if (ma_error_t e = ma_variant_create_from_wstring(v.data() + pos, std::wstring::npos == length ? v.length() - pos : length , addr_of())) throw exception(e);
}

inline variant::variant(const ma_uint8_t *v, std::size_t length) {
    if (ma_error_t e = ma_variant_create_from_raw(v, length, addr_of())) throw exception(e);
}

inline variant::variant(array const &a) {
    if (ma_error_t e = ma_variant_create_from_array(a.get(), addr_of())) throw exception(e);
}

inline variant::variant(table const &t) {
    if (ma_error_t e = ma_variant_create_from_table(t.get(), addr_of())) throw exception(e);
}

inline ::ma_vartype_e variant::type() const {
    ::ma_vartype_e type;
    if (ma_error_t e = ma_variant_get_type(this->get(), &type)) throw exception(e);
    return type;
}

inline bool variant::is_type(::ma_vartype_e t) const {
    return (MA_OK == ma_variant_is_type(this->get(), t));		
}

/* generic assignment, works as long as there is a corresponding c'tor */
template <typename T>
inline variant &variant::operator=(T const &v) {
    variant tmp(v);
    this->swap(tmp);
    return *this;
}

inline void variant::swap(variant &o) throw() {
    handle_type tmp = this->handle;
    this->handle = o.handle;
    o.handle = tmp;
}

/* *************** t a b l e ******************* */

inline table::table() {
    if (ma_error_t e = ma_table_create(addr_of())) throw exception(e);
}

inline table::table(table::handle_type h, bool add_ref /* = true */) : table_copyable_handle(h, add_ref) {
}

inline table::table(table const &o) : table_copyable_handle(o) {
}

inline void table::set_entry(const char *key, variant const &value) {
    if (ma_error_t e = ma_table_add_entry(get(), key, value.get())) throw exception(e);
}

#ifdef MA_HAS_WCHAR_T

inline void table::set_entry(const wchar_t *key, variant const &value) {
    if (ma_error_t e = ma_table_add_wentry(get(), key, value.get())) throw exception(e);
}
#endif /* MA_HAS_WCHAR_T */

inline variant table::get_entry(const char *key) const {
    variant v(0, false);
    if (ma_error_t e = ma_table_get_value(get(), key, &v)) throw exception(e);
    return v;
}

inline variant table::get_entry(const wchar_t *key) const {
    variant v(0, false);
    if (ma_error_t e = ma_table_get_wvalue(get(), key, &v)) throw exception(e);
    return v;
}

   /*!
    * retrieves entry for specified key as variant. variant must contain a string
    */
inline variant table::get_entry(variant const &key) const {
    variant v(0, false);
    if (ma_error_t e = ma_table_get_value(get(), mileaccess::ma::get<std::string>(key).c_str(), &v)) throw exception(e);
    return v;
}

inline bool operator==(table const &l, table const &r) {
    ::ma_bool_t result;
    if (ma_error_t e = ma_table_is_equal(l.get(), r.get(), &result)) throw exception(e);
    return MA_FALSE != result;
}

inline array table::keys() const {
    array a(0, false);
    if (ma_error_t e = ma_table_get_keys(get(), &a)) throw exception(e);
    return a;
}


inline void table::swap(table &o) throw() {
    handle_type tmp = this->handle;
    this->handle = o.handle;
    o.handle = tmp;
}


/* *************** a r r a y ******************* */

inline array::array() {
    if (ma_error_t e = ma_array_create(addr_of())) throw exception(e);
}


inline array::array(array::handle_type h, bool add_ref /* = true */) : array_copyable_handle(h, add_ref) {
}

inline array::array(array const &o) : array_copyable_handle(o) {
}

inline array &array::operator =(array const &o) {
    array tmp(o);
    this->swap(tmp);
    return *this;
}

inline array& array::push(variant const &element) {
    if (ma_error_t e = ma_array_push(get(), element.get())) throw exception(e);
    return *this;
}

inline std::size_t array::size() const {
    ::size_t size;
    if (ma_error_t e = ma_array_size(get(), &size)) throw exception(e);
    return size;
}

inline variant array::at(std::size_t index) const {
    mileaccess::ma::variant v(0,false);
    if (ma_error_t e = ma_array_get_element_at(get(), index, &v)) throw exception(e);
    return v;
}


template <typename T>
inline void array::for_each(T t) {
    size_t count = size();
    for (size_t i=0; i!=count; ++i) {
        t( at(1+i) );
    }
}

inline void array::swap(array &o) throw() {
    handle_type tmp = this->handle;
    this->handle = o.handle;
    o.handle = tmp;
}

inline bool operator==(array const &l, array const &r) {
    ::ma_bool_t result;
    if (ma_error_t e = ma_array_is_equal(l.get(), r.get(), &result)) throw exception(e);
    return MA_FALSE != result;
}


}} /* namespace mileaccess::ma */

#endif /* MA_VARIANT_HXX_INCLUDED */
