#ifndef MA_MESSAGE_AUTH_INFO_HXX_INCLUDED
#define MA_MESSAGE_AUTH_INFO_HXX_INCLUDED

#include "ma/ma_message_auth_info.h"
#include "ma/variant.hxx"

#include "ma/utils/auto_handle.hxx"
#include "ma/utils/exception.hxx"

#include <ctime>
#include <string>

namespace mileaccess { namespace ma {

struct message_auth_info_traits {

    typedef ma_message_auth_info_h handle_type;

    static handle_type invalid_handle_value() {
        return handle_type(0);
    };

    static void close(handle_type h) {
        ::ma_message_auth_info_release(h);
    }

    static void add_ref(handle_type h) {
        ::ma_message_auth_info_add_ref(h);
    }
};




/**
 * simple wrapper over the ma_message_auth_info type
 */
struct message_auth_info {
    
    typedef ma::utils::copyable_handle<message_auth_info_traits> message_auth_info_auto_handle;
    
    typedef message_auth_info_auto_handle::handle_type handle_type;
    
    /**
     * creates a new message auth info from the low level C handle 
     */
    message_auth_info(handle_type h, bool add_ref = true) throw();

    /**
     * gets Issuer's CN.
     */
    std::string get_issuer_cn() const;

    /**
     * gets Subject's CN.
    */
    std::string get_subject_cn() const;

    /**
     * gets Process' user (s)id.
    */
    void *get_psid() const;

    /**
     * gets Process' id.
    */
    long get_pid() const;

    /**
     * gets user id.
    */
    long get_uid() const;

    /**
     * gets group id.
    */
    long get_gid() const;
    
    /**
     * Process is admin/root
    */
    bool is_privileged() const;

private:

    message_auth_info_auto_handle m;
};


inline message_auth_info::message_auth_info(handle_type h, bool add_ref) throw() : m(h, add_ref) {
}

inline std::string message_auth_info::get_issuer_cn() const {
    const char *p = NULL;
    if (::ma_error_t e = ma_message_auth_info_get_attribute(m.get(), MA_MSGBUS_AUTH_INFO_ATTRIBUTE_VERIFIED_SIGNER, &p)) throw utils::exception(e);
    return p;
}

inline std::string message_auth_info::get_subject_cn() const {
    const char *p = NULL;
    if (::ma_error_t e = ma_message_auth_info_get_attribute(m.get(), MA_MSGBUS_AUTH_INFO_ATTRIBUTE_VERIFIED_PUBLISHER, &p)) throw utils::exception(e);
    return p;
}

inline void *message_auth_info::get_psid() const {
    void *p = NULL;
    if (::ma_error_t e = ma_message_auth_info_get_attribute(m.get(), MA_MSGBUS_AUTH_INFO_ATTRIBUTE_USER_SID, &p)) throw utils::exception(e);
    return p;
}

inline long message_auth_info::get_pid() const {
    long pid = -1;
    if (::ma_error_t e = ma_message_auth_info_get_attribute(m.get(), MA_MSGBUS_AUTH_INFO_ATTRIBUTE_PID, &pid)) throw utils::exception(e);
    return pid;
}

inline long message_auth_info::get_uid() const {
    long uid = -1;
    if (::ma_error_t e = ma_message_auth_info_get_attribute(m.get(), MA_MSGBUS_AUTH_INFO_ATTRIBUTE_UID, &uid)) throw utils::exception(e);
    return uid;
}

inline long message_auth_info::get_gid() const {
    long gid = -1;
    if (::ma_error_t e = ma_message_auth_info_get_attribute(m.get(), MA_MSGBUS_AUTH_INFO_ATTRIBUTE_GID, &gid)) throw utils::exception(e);
    return gid;
}

inline bool message_auth_info::is_privileged() const {
    ::ma_bool_t b = MA_FALSE;
    if (::ma_error_t e = ma_message_auth_info_get_attribute(m.get(), MA_MSGBUS_AUTH_INFO_ATTRIBUTE_USER_IS_PRIVILEGED, &b)) throw utils::exception(e);
    return (MA_TRUE == b) ? true : false;
}


}};


#endif /* MA_MESSAGE_AUTH_INFO_HXX_INCLUDED */
