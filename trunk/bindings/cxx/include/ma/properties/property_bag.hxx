#ifndef MA_PROPERTY_BAG_HXX_INCLUDED
#define MA_PROPERTY_BAG_HXX_INCLUDED

#include "ma/property/ma_property_bag.h"
#include "ma/variant.hxx"
#include "ma/utils/exception.hxx"

namespace mileaccess { namespace ma {

struct property_bag {

    property_bag(::ma_property_bag_h h);
	
	~property_bag() ;
	
    /**
     * @return a reference to itself (so calls to add_property can be chained)
     */

    property_bag &add_property(const char *path, const char *name, variant const &value);
	
	property_bag &add_property(std::string const &path, std::string const &name, variant const &value) ;

	/**
     * @return a type of the bag
     */
    ::ma_property_bag_type_t get_type();

    ma_property_bag_h handle; /* lifetime not managed */
};

inline property_bag::property_bag(::ma_property_bag_h h) : handle(h) {

}

inline property_bag::~property_bag() {
}

inline property_bag &property_bag::add_property(const char *path, const char *name, variant const &value) {
    if (::ma_error_t err = ma_property_bag_add_property(this->handle, path, name, value.get())) throw utils::exception(err);
    return *this;
}

inline property_bag &property_bag::add_property(std::string const &path, std::string const &name, variant const &value) {
    if (::ma_error_t err = ma_property_bag_add_property(this->handle, path.c_str(), name.c_str(), value.get())) throw utils::exception(err);
    return *this;
}

inline ::ma_property_bag_type_t property_bag::get_type() {
	::ma_property_bag_type_t t = MA_PROPERTY_BAG_TYPE_MIN_PROPS;
    if (::ma_error_t err = ma_property_bag_get_type(this->handle, &t)) throw utils::exception(err);
    return t;
}

}}

#endif
