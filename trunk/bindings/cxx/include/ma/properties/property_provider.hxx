#ifndef MA_PROPERTY_PROVIDER_HXX_INCLUDED
#define MA_PROPERTY_PROVIDER_HXX_INCLUDED

#include "ma/client.hxx"
#include "ma/properties/property_bag.hxx"
#include "ma/utils/exception.hxx"

#include "ma/property/ma_property.h"

#include <string>
#include <exception>

namespace mileaccess { namespace ma {

// interface that a property provider must implement (yes, you, the point product)
// the same property_provider instance can be used for multiple product ids, they must however share the same client
struct property_provider {

    /**
     * call this to register your property provider
     */
    void register_provider(const char *product_id);
	
	void register_provider(std::string const &product_id);

    /**
     * and this to unregister
     */
    void unregister_provider(const char *product_id);
	
	void unregister_provider(std::string const &product_id);

protected:
    /**
     * protected constructor, must be invoked by your derived class
     */
    property_provider(client const &c) : cli(c) {;}

    virtual ~property_provider() {;}

    /**
     * invoked by MA when it is time to collect your properties. You must populate the supplied property_bag
     * @param cli The client used for registration. Most likely you can ignore this
     * @param product_id The product id for which properties are to be collected
     * @param properties The property object to be populated
     *
     * @return status value to indicate if property collection was successful. Please return MA_OK on successful collection or a non 0 value to indicate an error 
     */
    virtual ma_error_t on_collect_properties(client const &cli, std::string const &product_id, property_bag &properties) = 0;

private:
    static ::ma_error_t property_provider_cb(::ma_client_t *, const char *, ::ma_property_bag_t *, void *);

    client const &cli;

    // private copy c'tor and assignment operator to prevent accidental copying

    property_provider(property_provider const &);

    property_provider const &operator=(property_provider const &);
};


inline void property_provider::register_provider(const char *product_id) {
    if (::ma_error_t err = ma_property_register_provider_callback(cli.get(), product_id, &property_provider::property_provider_cb, this)) throw utils::exception(err);
}

inline void property_provider::register_provider(std::string const &product_id) {
    if (::ma_error_t err = ma_property_register_provider_callback(cli.get(), product_id.c_str(), &property_provider::property_provider_cb, this)) throw utils::exception(err);
}

inline void property_provider::unregister_provider(const char *product_id) {
    if (::ma_error_t err = ma_property_unregister_provider_callback(cli.get(), product_id)) throw utils::exception(err);
}

inline void property_provider::unregister_provider(std::string const &product_id) {
    if (::ma_error_t err = ma_property_unregister_provider_callback(cli.get(), product_id.c_str())) throw utils::exception(err);
}

/*static*/ 
inline ::ma_error_t property_provider::property_provider_cb(::ma_client_t *client, const char *product_id, ::ma_property_bag_t *property_bag, void *cb_data) {
    property_provider *self = (property_provider *)cb_data;
    // ensure no C++ exceptions bubble back into the C code that invokes this function...
    try {
        ::mileaccess::ma::property_bag properties(property_bag);
        return self->on_collect_properties(self->cli, product_id, properties);
    } catch (...) {
    };

    return MA_ERROR_UNEXPECTED;
}


}} // namespace mileaccess::ma

#endif /* MA_PROPERTY_PROVIDER_HXX_INCLUDED */
