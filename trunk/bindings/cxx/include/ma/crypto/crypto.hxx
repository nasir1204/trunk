#ifndef MA_CRYPTO_HXX_INCLUDED
#define MA_CRYPTO_HXX_INCLUDED

#include "ma/crypto/ma_crypto.h"
#include "ma/utils/auto_handle.hxx"
#include "ma/utils/exception.hxx"
#include "ma/buffer.hxx"
#include "ma/client.hxx"

namespace mileaccess { namespace ma { namespace crypto {

using mileaccess::ma::client;
using mileaccess::ma::buffer;

/** 
 * @brief encryption of given string
 * @param c                    [in]     The client used for encryption.
 * @param algo_type            [in]     encryption algorithm type string(e.g. "3DES", "AES128" ..)
 * @param data                 [in]     string to be encrypted.
 * @param data_len             [in]     length of pointer string to be encrypted.
 * @result                              buffer object of encrypted string on success.
 *                                      throw integer exception of appropriate error code.
 * @remark               
 *
 */
inline buffer encrypt(mileaccess::ma::client const &c, const char *algo_type, const unsigned char *data, size_t data_len) {
    ::ma_buffer_t *buffer = NULL;    
    if (ma_error_t e = ma_crypto_encrypt_data_by_type(c.get(), algo_type, data, data_len, &buffer)) throw exception(e);
    return mileaccess::ma::buffer(buffer, true);
}

/** 
 * @brief decryption of given encrypted string.
 * @param c                    [in]     The client used for decryption.
 * @param algo_type            [in]     decryption algorithm type string(e.g. "3DES", "AES128" ..)
 * @param data                 [in]     encrypted string.
 * @param data_len             [in]     length of encrypted string.
 * @result                              buffer object of decrypted string on success.
 *                                      throw integer exception of appropriate error code.
 * @remark               
 *
 */
inline buffer decrypt(mileaccess::ma::client const &c, const char *algo_type, const unsigned char *data, size_t data_len) {
    ::ma_buffer_t *buffer = NULL;    
    if (ma_error_t e = ma_crypto_decrypt_data_by_type(c.get(), algo_type, data, data_len, &buffer)) throw exception(e);
    return mileaccess::ma::buffer(buffer, true);
}

/** 
 * @brief hashing of given string.
 * @param c                    [in]     The client used for hashing.
 * @param algo_type            [in]     hashing algorithm type string(e.g. "SHA1", "SHA256" ..)
 * @param data                 [in]     string to ne hash.
 * @param data_len             [in]     length of string.
 * @result                              buffer object of hashed string on success.
 *                                      throw integer exception of appropriate error code.
 * @remark               
 *
 */
inline buffer hash(mileaccess::ma::client const &c, const char *digest_type, const unsigned char *data, size_t data_len) {
    ::ma_buffer_t *buffer = NULL;    
    if (ma_error_t e = ma_crypto_hash_data_by_type(c.get(), digest_type, data, data_len, &buffer)) throw exception(e);
    return mileaccess::ma::buffer(buffer, true);
}

}}}

#endif // MA_EVENT_HXX_INCLUDED
