#ifndef MA_CUSTOM_EVENT_HXX_INCLUDED
#define MA_CUSTOM_EVENT_HXX_INCLUDED

#include "ma/event/ma_custom_event.h"
#include "ma/utils/auto_handle.hxx"
#include "ma/utils/exception.hxx"
#include "ma/client.hxx"

namespace mileaccess { namespace ma { namespace events {

struct custom_event_traits {
    typedef ::ma_custom_event_h handle_type;

    static handle_type invalid_handle_value() {
        return 0;
    }

	static void add_ref(handle_type h) {
		::ma_custom_event_add_ref(h);
	}

    static void close(handle_type h) {
        ::ma_custom_event_release(h);
    }
};

using mileaccess::ma::client;

/*!
 * wrapper over ma_custom_event. Allows copying by applying reference counting on the underlying ma_custom_event_h handle
 */
struct custom_event : mileaccess::ma::utils::copyable_handle<custom_event_traits> {

     /*!
	 * c'tor,
     * creates a C++ custom event from the underlying C handle.
     * @param h Handle to an existing ma_custom event
     * @param add_ref Specifies if the reference count should be bumped also. If add_ref is set to true, the caller still assumes ownership of the original handle and must release it
     */
	custom_event(mileaccess::ma::utils::copyable_handle<custom_event_traits>::handle_type h, bool add_ref = true);

	/*!
     * create custom event from 
	 * @param c						[in]     The client used for custom event create.
	 * @param event_root_name		[in]     custom event root name.
	 * @param root_node				[out]    contain new node.
     */
    custom_event(client const &c, const char *event_root_name, ma_xml_node_t **root_node);

	/** 
	 * @brief create custome event from given xml string.
	 * @param c                    [in]     The client used for custom event create.
	 * @param buffer	           [in]     xml string
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
    void custom_event_create_from_buffer(client const &c, const char *buffer);

	/** 
	 * @brief create custome event node.
	 * @param parent               [in]     The client used for custom event create.
	 * @param event_node_name      [in]     new event node name
	 * @param event_id			   [in]     new event node id
	 * @param severity			   [in]     new event node severity
	 * @param node				   [out]    node pointer
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
	void custom_event_create_event_node(ma_xml_node_t *parent, const char *event_node_name, ma_uint32_t event_id, ma_event_severity_t severity, ma_xml_node_t **node);
	
	/*!
     * @brief send custom event.
     */
	void custom_event_send();
	
	/** 
	 * @brief create custome event node.
	 * @param parent                [in]     parent node, to which new node will be added.
	 * @param name					[in]     name of new node.
	 * @param node					[out]    pointing to new node.
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
	void custom_event_node_create(ma_xml_node_t *parent, const char *name, ma_xml_node_t **node);
	
	/** 
	 * @brief set custome event node attribute.
	 * @param node	                [in]     node, whose attribute to be set.
	 * @param name					[in]     attribute name string.
	 * @param value					[in]     attribute value string.
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
	void custom_event_node_attribute_set(ma_xml_node_t *node, const char *name, const char *value);
	
	/** 
	 * @brief set custome event node data.
	 * @param node	                [in]     node, whose data to be set.
	 * @param data					[in]     data string.
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
	void custom_event_node_set_data(ma_xml_node_t *node, const char *data);

	/** 
	 * @brief get custome event root node.
	 * @param root_node				[out]     containing root node.
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
	void custom_event_get_root_node(ma_xml_node_t **root_node);

	/** 
	 * @brief get custome event node data.
	 * @param node	                [in]     custom event node.
	 * @param data					[out]    string containing node data.
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
	void custom_event_node_data_get(ma_xml_node_t *node, const char **data);

	/** 
	 * @brief get custome event node name.
	 * @param node	                [in]     custom event node.
	 * @param name					[out]    string containing node name.
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
	void custom_event_node_name_get(ma_xml_node_t *node, const char **name);

	/** 
	 * @brief get attribute value.
	 * @param node	                [in]    custom event node.
	 * @param attribute				[in]    name of attribute.
	 * @param data					[out]   attribute value.
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
	void custom_event_node_attribute_get(ma_xml_node_t *node, const char *attribute, const char **data);

	/** 
	 * @brief get node by name.
	 * @param node	                [in]    custom event node.
	 * @param name					[in]    name of node to be search.
	 * @param data					[out]   data of given name, if finds. Else NULL.
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
	void custom_event_node_search(ma_xml_node_t *node, const char *name, ma_xml_node_t **data);

	/** 
	 * @brief get child node.
	 * @param node	                [in]    custom event node.
	 * @param child_node			[out]   child node.
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
	void custom_event_get_child_node(ma_xml_node_t *node, ma_xml_node_t **child_node);

	/** 
	 * @brief get next node.
	 * @param node	                [in]    custom event node.
	 * @param next_node				[out]   next node.
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
	void custom_event_get_next_node(ma_xml_node_t *node, ma_xml_node_t **next_node);

	/** 
	 * @brief get sibling node.
	 * @param node	                [in]    custom event node.
	 * @param sibling_node			[out]   sibling node.
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
	void custom_event_get_next_sibling(ma_xml_node_t *node, ma_xml_node_t **sibling_node);

private:
    custom_event(custom_event const &);
    custom_event& operator=(custom_event const &);
};

inline custom_event::custom_event(mileaccess::ma::utils::copyable_handle<custom_event_traits>::handle_type h, bool add_ref) : mileaccess::ma::utils::copyable_handle<custom_event_traits>(h, add_ref) {
}

inline custom_event::custom_event(client const &c, const char *event_root_name, ma_xml_node_t **root_node) {
    if (ma_error_t err = ma_custom_event_create(c.get(), event_root_name, &handle, root_node)) throw utils::exception(err);
}

inline void custom_event::custom_event_create_from_buffer(client const &c, const char *buffer) {
    if (ma_error_t err = ma_custom_event_create_from_buffer(c.get(), buffer, &handle)) throw utils::exception(err);
}

inline void custom_event::custom_event_create_event_node(ma_xml_node_t *parent, const char *event_node_name, ma_uint32_t event_id, ma_event_severity_t severity, ma_xml_node_t **node) {
    if (ma_error_t err = ma_custom_event_create_event_node(handle, parent, event_node_name, event_id, severity, node)) throw utils::exception(err);
}

inline void custom_event::custom_event_send() {
    if (ma_error_t err = ma_custom_event_send(handle)) throw utils::exception(err);
}

inline void custom_event::custom_event_node_create(ma_xml_node_t *parent, const char *name, ma_xml_node_t **node) {
    if (ma_error_t err = ma_custom_event_node_create(parent, name, node)) throw utils::exception(err);
}

inline void custom_event::custom_event_node_attribute_set(ma_xml_node_t *node, const char *name, const char *value) {
    if (ma_error_t err = ma_custom_event_node_attribute_set(node, name, value)) throw utils::exception(err);
}

inline void custom_event::custom_event_node_set_data(ma_xml_node_t *node, const char *data) {
    if (ma_error_t err = ma_custom_event_node_set_data(node, data)) throw utils::exception(err);
}

inline void custom_event::custom_event_get_root_node(ma_xml_node_t **root_node) {
	if (ma_error_t err = ma_custom_event_get_root_node(this->get(), root_node)) throw utils::exception(err);
}

inline void custom_event::custom_event_node_data_get(ma_xml_node_t *node, const char **data) {
	if (ma_error_t err = ma_custom_event_node_data_get(node, data)) throw utils::exception(err);
}

inline void custom_event::custom_event_node_name_get(ma_xml_node_t *node, const char **name) {
	if (ma_error_t err = ma_custom_event_node_name_get(node, name)) throw utils::exception(err);
}

inline void custom_event::custom_event_node_attribute_get(ma_xml_node_t *node, const char *attribute, const char **data) {
	if (ma_error_t err = ma_custom_event_node_attribute_get(node, attribute, data)) throw utils::exception(err);
}

inline void custom_event::custom_event_node_search(ma_xml_node_t *node, const char *name, ma_xml_node_t **data) {
	if (ma_error_t err = ma_custom_event_node_search(node, name, data)) throw utils::exception(err);
}

inline void custom_event::custom_event_get_child_node(ma_xml_node_t *node, ma_xml_node_t **child_node) {
	if (ma_error_t err = ma_custom_event_get_child_node(node, child_node)) throw utils::exception(err);
}

inline void custom_event::custom_event_get_next_node(ma_xml_node_t *node, ma_xml_node_t **next_node) {
	if (ma_error_t err = ma_custom_event_get_next_node(node, next_node)) throw utils::exception(err);
}

inline void custom_event::custom_event_get_next_sibling(ma_xml_node_t *node, ma_xml_node_t **sibling_node) {
	if (ma_error_t err = ma_custom_event_get_next_sibling(node, sibling_node)) throw utils::exception(err);
}

}

}}

#endif // MA_CUSTOM_EVENT_HXX_INCLUDED
