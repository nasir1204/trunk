#ifndef MA_EVENT_CLIENT_HXX_INCLUDED
#define MA_EVENT_CLIENT_HXX_INCLUDED

#include "ma/events/custom_event.hxx"
#include "ma/events/event_bag.hxx"
#include "ma/utils/auto_handle.hxx"
#include "ma/utils/exception.hxx"
#include "ma/client.hxx"
#include "ma/variant.hxx"

#include "ma/event/ma_event_client.h"

namespace mileaccess { namespace ma { namespace events {

using mileaccess::ma::client;
using mileaccess::ma::variant;

struct event_client {

    /**
     * subscribe for the event.
     */
    void subscribe();

    /**
     * unsubscribe for the event.
     */
    void unsubscribe();

	/**
     * Ask agent to upload events to ePO.
     */
    void upload_event(std::string const &product_id);
protected:
    /**
     * protected constructor, must be invoked by your derived class
     */
    event_client(client const &c) : cli(c) {;}

    virtual ~event_client() {;}
	    
	virtual ::ma_error_t on_cef_event(client const &cli, std::string const &product_id, event_bag const &bag) {return MA_OK;};

	virtual ::ma_error_t on_custom_event(client const &cli, std::string const &product_id, custom_event const &bag) { return MA_OK;};

protected:
    client const &cli;

private:
    static ::ma_error_t cef_event_bag_cb(::ma_client_t *ma_client, const char *event_product_id, ::ma_event_bag_t *event_bag, void *cb_data);

	static ::ma_error_t custom_event_bag_cb(::ma_client_t *ma_client, const char *event_product_id, ::ma_custom_event_t *event_bag, void *cb_data);    

    // private copy c'tor and assignment operator to prevent accidental copying

	event_client(event_client const &c);

	event_client const &operator=(event_client const &c);

	 static ::ma_event_callbacks_t *get_methods() {
        static ::ma_event_callbacks_t methods = {&cef_event_bag_cb, &custom_event_bag_cb};
        return &methods;
    }
};

inline ::ma_error_t event_client::cef_event_bag_cb(::ma_client_t *ma_client, const char *event_product_id, ::ma_event_bag_t *event_bag_ptr, void *cb_data){
	event_client *client = (event_client*)cb_data;
	event_bag bag(event_bag_ptr, true);
	return client->on_cef_event(client->cli, std::string(event_product_id), bag);	
}

inline ::ma_error_t event_client::custom_event_bag_cb(::ma_client_t *ma_client, const char *event_product_id, ::ma_custom_event_t *event_bag_ptr, void *cb_data){
	event_client *client = (event_client*)cb_data;
	custom_event bag(event_bag_ptr, true);
	return client->on_custom_event(client->cli, std::string(event_product_id), bag);	
}

inline void event_client::subscribe(){	
	if(::ma_error_t err = ma_event_register_callbacks(cli.get(), get_methods(), this)) throw utils::exception(err);
}

inline void event_client::unsubscribe(){
	if(::ma_error_t err = ma_event_unregister_callbacks(cli.get())) throw utils::exception(err);
}

inline void event_client::upload_event(std::string const &product_id){
	if(::ma_error_t err = ma_event_upload(cli.get(), product_id.c_str())) throw utils::exception(err);
}

} } }  // mileaccess::ma::events

#endif
