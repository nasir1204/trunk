#ifndef MA_EVENT_BAG_HXX_INCLUDED
#define MA_EVENT_BAG_HXX_INCLUDED

#include "ma/events/event.hxx"
#include "ma/utils/auto_handle.hxx"
#include "ma/utils/exception.hxx"
#include "ma/client.hxx"
#include "ma/variant.hxx"

#include "ma/event/ma_event_bag.h"


namespace mileaccess { namespace ma { namespace events {

struct event_bag_traits {
    typedef ::ma_event_bag_h handle_type;
    static handle_type invalid_handle_value() {
        return 0;
    }
		
	static void add_ref(handle_type h) {
        ::ma_event_bag_add_ref(h);
    }

    static void close(handle_type h) {
        ::ma_event_bag_release(h);
    }
};

using mileaccess::ma::client;
using mileaccess::ma::variant;
struct event_client;

/*!
 * wrapper over ma event bag. Allows copying by applying reference counting on the underlying ma_event_bag_h handle
 */
struct event_bag : mileaccess::ma::utils::copyable_handle<event_bag_traits> {

    /*!
	 * c'tor,
     * creates a C++ custom event from the underlying C handle.
     * @param event_b Handle to an existing ma event bag
     * @param add_ref Specifies if the reference count should be bumped also. If add_ref is set to true, the caller still assumes ownership of the original handle and must release it
     */
	event_bag(::ma_event_bag_t *event_b, bool add_ref);

	/*!
     * c'tor, create event from 
	 * @param c						[in]     The client used for event bag.
	 * @param product_name			[in]     product name.
     */
    event_bag(client const &c, char const *product_name);

	/** 
	 * @brief set product version in event bag.
	 * @param product_version       [in]     product version.
	 * @result                              
	 *                                      
	 * @remark								
	 *
	 */
    void set_product_version(char const *product_version);

	/** 
	 * @brief set product family in event bag.
	 * @param product_family       [in]     product family.
	 * @result                              
	 *                                      
	 * @remark								
	 *
	 */
    void set_product_family(char const *product_family);

	/** 
	 * @brief add common field in event bag.
	 * @param key	                [in]     key string.
	 * @param field					[in]     value of key.
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
    void add_common_field(char const *key, variant const &field);

    /** 
	 * @brief set custom field table name in event bag.
	 * @param table_name            [in]     table name.
	 * @result                              
	 *                                      
	 * @remark								hint to ePO on where to insert event data         
	 *
	 */
    void set_custom_fields_table_name(const char *table_name);

	/** 
	 * @brief add custom field in event bag.
	 * @param key	                [in]     key string.
	 * @param field					[in]     value of key.
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
    void add_custom_field(char const *key, variant const &field);

	/** 
	 * @brief add event in bag.
	 * @param evt	                [in]     event.
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
    void add_event(event &evt);

    /** 
	 * @brief commit this bag.
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
    void send();

	/** 
	 * @brief get product name.
	 * @result								product name string .
	 *                                      
	 * @remark               
	 *
	 */
	const char *get_product_name() const;

	/** 
	 * @brief get product version.
	 * @result								product version string .
	 *                                      
	 * @remark               
	 *
	 */
	const char *get_product_version() const;

	/** 
	 * @brief get product family.
	 * @result								product family string .
	 *                                      
	 * @remark               
	 *
	 */
	const char *get_product_family() const;

	/** 
	 * @brief get common field.
	 * @param key	                [in]    key string of common field.
	 * @result								variant value .
	 *                                      
	 * @remark               
	 *
	 */
	variant get_common_field(char const *key) const;

	/** 
	 * @brief get custom field table name.
	 * @result								table name.                
	 *                                      
	 * @remark               
	 *
	 */
	const char *get_custom_field_table_name() const;

	/** 
	 * @brief get custom field.
	 * @param key	                [in]    key string of custom field.
	 * @result								variant value .
	 *                                      
	 * @remark               
	 *
	 */
	variant get_custom_field(char const *key) const;
		
	/** 
	 * @brief get event count.
	 * @result								count value .
	 *                                      
	 * @remark               
	 *
	 */
	size_t get_event_count() const;

	/** 
	 * @brief get event at indexed location.
	 * @param index	                [in]    index.
	 * @result								event .
	 *                                      
	 * @remark               
	 *
	 */
	event get_event(size_t index) const;
};

inline event_bag::event_bag(::ma_event_bag_t *h, bool add_ref = true) : mileaccess::ma::utils::copyable_handle<event_bag_traits>(h, add_ref) {	
}

inline event_bag::event_bag(client const &c, char const *product_name) {
    if (ma_error_t err = ma_event_bag_create(c.get(), product_name, &handle )) throw utils::exception(err);
}

inline void event_bag::set_product_version(char const *product_version) {
    if (ma_error_t err = ma_event_bag_set_product_version(get(), product_version)) throw utils::exception(err);
}

inline void event_bag::set_product_family(char const *product_family) {
    if (ma_error_t err = ma_event_bag_set_product_family(get(), product_family)) throw utils::exception(err);
}

inline void event_bag::add_common_field(char const *key, variant const &field) {
    if (ma_error_t err = ma_event_bag_add_common_field(get(), key, field.get())) throw utils::exception(err);
}

// hint to ePO on where to insert event data
inline void event_bag::set_custom_fields_table_name(const char *table_name) {
    if (ma_error_t err = ma_event_bag_set_custom_fields_table(get(), table_name)) throw utils::exception(err);
}

inline void event_bag::add_custom_field(char const *key, variant const &field) {
    if (ma_error_t err = ma_event_bag_add_custom_field(get(), key, field.get())) throw utils::exception(err);
}

inline void event_bag::add_event(event &evt) {
    if (ma_error_t err = ma_event_bag_add_event(get(), evt.get())) throw utils::exception(err);
}

inline void event_bag::send() {
    if (ma_error_t err = ma_event_bag_send(get())) throw utils::exception(err);
}

inline const char *event_bag::get_product_name() const{
	const char *var = NULL;
	if (ma_error_t err = ma_event_bag_get_product_name(get(), &var)) throw utils::exception(err);
	return var;
}

inline const char *event_bag::get_product_version() const{
	const char *var = NULL;
	if (ma_error_t err = ma_event_bag_get_product_version(get(), &var)) throw utils::exception(err);
	return var;
}

inline const char *event_bag::get_product_family() const{
	const char *var = NULL;
	if (ma_error_t err = ma_event_bag_get_product_family(get(), &var)) throw utils::exception(err);
	return var;
}

inline variant event_bag::get_common_field(char const *key) const{
	::ma_variant_t *var = NULL;
    if (ma_error_t err = ma_event_bag_get_common_field(get(), key, &var)) throw utils::exception(err);
	return variant(var, false);
}

inline const char *event_bag::get_custom_field_table_name() const{
	const char *var = NULL;
	if (ma_error_t err = ma_event_bag_get_custom_fields_table(get(), &var)) throw utils::exception(err);
	return var;
}

inline variant event_bag::get_custom_field(char const *key) const{
	::ma_variant_t *var = NULL;
	if (ma_error_t err = ma_event_bag_get_custom_field(get(), key, &var)) throw utils::exception(err);
	return variant(var, false);
}

inline size_t event_bag::get_event_count() const{
	size_t size = 0;
	if (ma_error_t err = ma_event_bag_get_event_count(get(), &size)) throw utils::exception(err);
	return size;
}

inline event event_bag::get_event(size_t index) const{
	::ma_event_t *evt = NULL;
	if (ma_error_t err = ma_event_bag_get_event(get(), index, &evt)) throw utils::exception(err);
	return event(evt, false);
}

} } } // mileaccess::ma::events

#endif // MA_EVENT_BAG_HXX_INCLUDED
