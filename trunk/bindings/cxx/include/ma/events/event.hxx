#ifndef MA_EVENT_HXX_INCLUDED
#define MA_EVENT_HXX_INCLUDED

#include "ma/event/ma_event.h"
#include "ma/utils/auto_handle.hxx"
#include "ma/utils/exception.hxx"
#include "ma/variant.hxx"
#include "ma/client.hxx"

namespace mileaccess { namespace ma { namespace events {

struct event_traits {
    typedef ::ma_event_h handle_type;

    static handle_type invalid_handle_value() {
        return 0;
    }

	static void add_ref(handle_type h) {
        ::ma_event_add_ref(h);
    }

    static void close(handle_type h) {
        ::ma_event_release(h);
    }
};

using mileaccess::ma::client;
using mileaccess::ma::variant;

/*!
 * wrapper over ma_event. Allows copying by applying reference counting on the underlying ma_event_h handle
 */
struct event : mileaccess::ma::utils::copyable_handle<event_traits> {

    /*!
     * Default c'tor
     */    
	event();

	/*!
	 * c'tor,
     * creates a C++ custom event from the underlying C handle.
     * @param h Handle to an existing ma_event
     * @param add_ref Specifies if the reference count should be bumped also. If add_ref is set to true, the caller still assumes ownership of the original handle and must release it
     */
	event(mileaccess::ma::utils::copyable_handle<event_traits>::handle_type h, bool add_ref = true);

	/*!
     * c'tor, create event from 
	 * @param c						[in]     The client used for event create.
	 * @param eventid				[in]     event id.
	 * @param severity				[in]	 event severity.
     */
	event(client const &c, ma_uint32_t eventid, ma_event_severity_t severity);

	/*!
     * Copy c'tor
     */
	event(event const &et);

	/*!
     * Assignment operator
     */
    event& operator=(event const &et);

	/** 
	 * @brief add common field in event.
	 * @param key	                [in]     key string.
	 * @param field					[in]     value of key.
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
    void add_common_field(char const *key, variant const &field);

    /** 
	 * @brief set custom field table name in event.
	 * @param table_name            [in]     table name.
	 * @result                              
	 *                                      
	 * @remark								hint to ePO on where to insert event data         
	 *
	 */
    void set_custom_fields_table_name(const char *table_name);

	/** 
	 * @brief add custom field in event.
	 * @param key	                [in]     key string.
	 * @param field					[in]     value of key.
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
    void add_custom_field(char const *key, variant const &field);

	/** 
	 * @brief get event id
	 * @result								event id.                
	 *                                      
	 * @remark               
	 *
	 */
	ma_uint32_t get_id();

	/** 
	 * @brief get event severity.
	 * @result								event severity.                
	 *                                      
	 * @remark               
	 *
	 */
	::ma_event_severity_t get_severity();

	/** 
	 * @brief get event common field.
	 * @param key	                [in]    key string of common field.
	 * @result								key variant value .
	 *                                      
	 * @remark               
	 *
	 */
	variant get_common_field(char const *key);

	/** 
	 * @brief get custom field table name.
	 * @result								table name.                
	 *                                      
	 * @remark               
	 *
	 */
	const char *get_custom_field_table_name();

	/** 
	 * @brief get event custom field.
	 * @param key	                [in]    key string of custom field.
	 * @result								key variant value .
	 *                                      
	 * @remark               
	 *
	 */
	variant get_custom_field(char const *key);
private:

};

inline event::event(){};

inline event::event(client const &c, ma_uint32_t event_id, ma_event_severity_t severity) {
    if (ma_error_t err = ma_event_create(c.get(), event_id, severity, &handle )) throw utils::exception(err);
}

inline event::event(mileaccess::ma::utils::copyable_handle<event_traits>::handle_type h, bool add_ref) : mileaccess::ma::utils::copyable_handle<event_traits>(h, add_ref) {
}

inline void event::add_common_field(char const *key, variant const &field) {
    if (ma_error_t err = ma_event_add_common_field(get(), key, field.get())) throw utils::exception(err);
}

// hint to ePO on where to insert event data
inline void event::set_custom_fields_table_name(const char *table_name) {
    if (ma_error_t err = ma_event_set_custom_fields_table(get(), table_name)) throw utils::exception(err);
}

inline void event::add_custom_field(char const *key, variant const &field) {
    if (ma_error_t err = ma_event_add_custom_field(get(), key, field.get())) throw utils::exception(err);
}

inline ma_uint32_t event::get_id() {
	ma_uint32_t id = 0;
    if (ma_error_t err = ma_event_get_id(get(), &id )) throw utils::exception(err);
	return id;
}

inline ::ma_event_severity_t event::get_severity() {
	::ma_event_severity_t severity;
	if (ma_error_t err = ma_event_get_severity(get(), &severity )) throw utils::exception(err);
	return severity;
}

inline variant event::get_common_field(char const *key) {
	::ma_variant_t *var = NULL;
    if (ma_error_t err = ma_event_get_common_field(get(), key, &var)) throw utils::exception(err);
	return variant(var, false);
}

inline const char *event::get_custom_field_table_name(){
	const char *var = NULL;
	if (ma_error_t err = ma_event_get_custom_fields_table(get(), &var)) throw utils::exception(err);
	return var;
}

inline variant event::get_custom_field(char const *key) {
	::ma_variant_t *var = NULL;
	if (ma_error_t err = ma_event_get_custom_field(get(), key, &var)) throw utils::exception(err);
	return variant(var, false);
}

}}}

#endif // MA_EVENT_HXX_INCLUDED
