#ifndef MA_BUFFER_HXX_INCLUDED
#define MA_BUFFER_HXX_INCLUDED

#include "ma/ma_buffer.h"

#include "ma/utils/auto_handle.hxx"

#include <ma/utils/exception.hxx>

#include <cstddef>

namespace mileaccess { namespace ma {

struct buffer_traits {
    typedef ma_buffer_h handle_type;

    static handle_type invalid_handle_value() {
        return handle_type(NULL);
    }

    static void close(handle_type h) {
        ::ma_buffer_release(h);
    }

    // bonus
    static void add_ref(handle_type h) {
        ::ma_buffer_add_ref(h);
    }
};


struct wbuffer_traits {
    typedef ma_wbuffer_h handle_type;

    static handle_type invalid_handle_value() {
        return handle_type(NULL);
    }

    static void close(handle_type h) {
        ::ma_wbuffer_release(h);
    }

    // bonus
    static void add_ref(handle_type h) {
        ::ma_wbuffer_add_ref(h);
    }
};

//typedef utils::auto_handle<buffer_traits> buffer_auto_handle;
//typedef utils::auto_handle<wbuffer_traits> wbuffer_auto_handle;
typedef ma::utils::copyable_handle<buffer_traits> buffer_copyable_handle;
typedef ma::utils::copyable_handle<wbuffer_traits> wbuffer_copyable_handle;



struct buffer : buffer_copyable_handle {

    buffer() {;}

     /*!
     * creates buffer object from the underlying C handle
     * @param h Handle to an existing buffer
     * @param add_ref If true, takes a ref count, you still maintain ownership of the specified C handle. If add_ref is false, the ownership is transferred to the C++ variant and you should not release the handle
     */ 
    explicit buffer(handle_type h, bool add_ref = true);

    buffer &operator=(buffer const &o);

    std::size_t size() const;

    char const *data() const;
	
	const unsigned char  *raw_data() const;
};

struct wbuffer : wbuffer_copyable_handle {

    wbuffer() {;}
    /*!
     * creates wbuffer object from the underlying C handle
     * @param h Handle to an existing wbuffer
     * @param add_ref If true, takes a ref count, you still maintain ownership of the specified C handle. If add_ref is false, the ownership is transferred to the C++ variant and you should not release the handle
     */ 
    explicit wbuffer(handle_type h, bool add_ref = true);

    wbuffer &operator=(wbuffer const &o);

    std::size_t size() const;

    wchar_t const *data() const;
};

/* * * buffer definition * * * * */
inline buffer::buffer(buffer::handle_type h, bool add_ref /* = true */) : buffer_copyable_handle(h, add_ref) {
}

inline std::size_t buffer::size() const {
    const char *p;
    ::size_t size;
    if (ma_error_t err = ma_buffer_get_string(this->get(), &p, &size)) throw exception(err);
    return size;
}

inline char const *buffer::data() const {
    const char *p;
    ::size_t size;
    if (ma_error_t err = ma_buffer_get_string(this->get(), &p, &size)) throw exception(err);
    return p;
}

inline char unsigned const *buffer::raw_data() const {
    const unsigned char *p;
    ::size_t size;
    if (ma_error_t err = ma_buffer_get_raw(this->get(), &p, &size)) throw exception(err);
    return p;
}

inline wbuffer::wbuffer(wbuffer::handle_type h, bool add_ref /* = true */) : wbuffer_copyable_handle(h, add_ref) {
}

inline std::size_t wbuffer::size() const {
    const wchar_t *p;
    ::size_t size;
    if (ma_error_t err = ma_wbuffer_get_string(this->get(), &p, &size)) throw exception(err);
    return size;
}

inline wchar_t const *wbuffer::data() const {
    const wchar_t *p;
    ::size_t size;
    if (ma_error_t err = ma_wbuffer_get_string(this->get(), &p, &size)) throw exception(err);
    return p;
}


}} /* namespace mileaccess::ma */

#endif /* MA_BUFFER_HXX_INCLUDED */
