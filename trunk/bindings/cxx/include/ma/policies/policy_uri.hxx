#ifndef MA_POLICY_URI_HXX_INCLUDED
#define MA_POLICY_URI_HXX_INCLUDED

#include "ma/policy/ma_policy_uri.h"
#include "ma/utils/auto_handle.hxx"
#include "ma/variant.hxx"

#include <string>

namespace mileaccess { namespace ma { namespace policies {

using mileaccess::ma::client;

// traits class for memory management
struct policy_uri_traits {
    typedef ma_policy_uri_h handle_type;

    static handle_type invalid_handle_value() {return 0;}

    static void close(handle_type h) {
        ::ma_policy_uri_release(h);
    }

    static void add_ref(handle_type h) {
        ma_policy_uri_add_ref(h);
    }
};

struct const_policy_uri;

// C++ wrapper class over ma_policy_uri_t
struct policy_uri : mileaccess::ma::utils::copyable_handle<policy_uri_traits> {

    policy_uri(handle_type h, bool add_ref = true);

   /*!
    * Construct from a const_policy_uri. Performs a clone
    */
    policy_uri(const_policy_uri const &o);

	policy_uri();

    policy_uri clone() const;

    void set_software_id(std::string &software_id);

    void set_version(std::string &version);

    void set_user(std::string &user);

    void set_user_list(mileaccess::ma::array &user_list);

    void set_location_id(std::string &location);

    void set_feature(std::string &feature);

    void set_category(std::string &category);

    void set_type(std::string &type);

    void set_name(std::string &name);

	void set_pso_name(std::string &pso_name);

	void set_pso_param_int(std::string &pso_param_int);

	void set_pso_param_str(std::string &pso_param_str);

    ma_policy_notification_type_t get_notification_type() const; 

    std::string get_software_id() const; 

    std::string get_version() const; 

    std::string get_user() const; 

    ::ma_uint32_t get_user_session_id() const; 

    ma_user_logon_state_t get_user_logon_state() const; 
    
    std::string get_location_id() const; 
    
    std::string get_feature() const; 

    std::string get_category() const; 

    std::string get_type() const; 

    std::string get_name() const; 

	std::string get_pso_name() const;

	std::string get_pso_param_int() const;

	std::string get_pso_param_str() const;
};


/*!
 * unmanaged const_policy_uri
 */
struct const_policy_uri {

    /*!
     * construct const_policy_uri from underlying CONST c handle
     */
    const_policy_uri(ma_policy_uri_t  const *c_handle);

    /*!
     * canonical copy c'tor
     */
    const_policy_uri(const_policy_uri const &o);

    /*!
     * assignment
     */
    const_policy_uri &operator=(const_policy_uri const &o);

	const_policy_uri clone() const;

	/*!
     * return underline const handle
     */
    const ma_policy_uri_t *get();

    std::string get_software_id() const; 

    std::string get_version() const; 

    std::string get_user() const; 

    ::ma_uint32_t get_user_session_id() const; 

    ma_user_logon_state_t get_user_logon_state() const; 
    
    std::string get_location_id() const; 
    
    std::string get_feature() const; 

    std::string get_category() const; 

    std::string get_type() const; 

    std::string get_name() const; 

	std::string get_pso_name() const;

	std::string get_pso_param_int() const;

	std::string get_pso_param_str() const;

protected:
    ma_policy_uri_t  const *const_handle;
};


inline policy_uri::policy_uri(handle_type h, bool add_ref) : mileaccess::ma::utils::copyable_handle<policy_uri_traits>(h, add_ref) {

}

inline policy_uri::policy_uri() {
	if (ma_error_t err = ma_policy_uri_create(&this->handle)) throw utils::exception(err);
}

inline const_policy_uri::const_policy_uri(const_policy_uri const &o) : const_handle(o.const_handle) {

}

inline const_policy_uri &const_policy_uri::operator=(const_policy_uri const &o) {
    this->const_handle = o.const_handle;
    return *this;
}

inline policy_uri policy_uri::clone() const {
    handle_type h;
    if (ma_error_t err = ma_policy_uri_copy(get(), &h)) throw utils::exception(err);
    return policy_uri(h,false);
}

inline const ma_policy_uri_t *const_policy_uri::get() {
	return const_handle;
}


inline void policy_uri::set_software_id(std::string &software_id) {
    if (ma_error_t err = ma_policy_uri_set_software_id(get(), software_id.c_str()) ) throw utils::exception(err);
}

inline void policy_uri::set_version(std::string &version) {
    if (ma_error_t err = ma_policy_uri_set_version(get(), version.c_str()) ) throw utils::exception(err);
}

inline void policy_uri::set_user(std::string &user) {
    if (ma_error_t err = ma_policy_uri_set_user(get(), user.c_str()) ) throw utils::exception(err);
}

inline void policy_uri::set_user_list(mileaccess::ma::array &user_list) {
    if (ma_error_t err = ma_policy_uri_set_user_list(get(), user_list.get()) ) throw utils::exception(err);
}

inline void policy_uri::set_location_id(std::string &location) {
    if (ma_error_t err = ma_policy_uri_set_location_id(get(), location.c_str()) ) throw utils::exception(err);
}

inline void policy_uri::set_feature(std::string &feature) {
    if (ma_error_t err = ma_policy_uri_set_feature(get(), feature.c_str()) ) throw utils::exception(err);
}

inline void policy_uri::set_category(std::string &category) {
    if (ma_error_t err = ma_policy_uri_set_category(get(), category.c_str()) ) throw utils::exception(err);
}

inline void policy_uri::set_type(std::string &type) {
    if (ma_error_t err = ma_policy_uri_set_type(get(), type.c_str()) ) throw utils::exception(err);
}

inline void policy_uri::set_name(std::string &name) {
    if (ma_error_t err = ma_policy_uri_set_name(get(), name.c_str()) ) throw utils::exception(err);
}

inline void policy_uri::set_pso_name(std::string &pso_name) {
    if (ma_error_t err = ma_policy_uri_set_pso_name(get(), pso_name.c_str()) ) throw utils::exception(err);
}

inline void policy_uri::set_pso_param_int(std::string &pso_param_int) {
	if (ma_error_t err = ma_policy_uri_set_pso_param_int(get(), pso_param_int.c_str()) ) throw utils::exception(err);
}

inline void policy_uri::set_pso_param_str(std::string &pso_param_str) {
	if (ma_error_t err = ma_policy_uri_set_pso_param_str(get(), pso_param_str.c_str()) ) throw utils::exception(err);
}

inline ma_policy_notification_type_t policy_uri::get_notification_type() const {
    ma_policy_notification_type_t value;
    if (ma_error_t err = ma_policy_uri_get_notification_type(get(), &value)) throw utils::exception(err);
    return value;
}

inline std::string policy_uri::get_software_id() const {
    const char *value;
    if (ma_error_t err = ma_policy_uri_get_software_id(get(), &value)) throw utils::exception(err);
    return std::string(value);
} 

inline std::string policy_uri::get_version() const {
    const char *value;
    if (ma_error_t err = ma_policy_uri_get_version(get(), &value)) throw utils::exception(err);
    return std::string(value);
} 

inline std::string policy_uri::get_user() const {
    const char *value;
    if (ma_error_t err = ma_policy_uri_get_user(get(), &value)) throw utils::exception(err);
    return std::string(value);
}

inline ::ma_uint32_t policy_uri::get_user_session_id() const {
    ::ma_uint32_t value;
    if (ma_error_t err = ma_policy_uri_get_user_session_id(get(), &value)) throw utils::exception(err);
    return value;
}

inline ma_user_logon_state_t policy_uri::get_user_logon_state() const {
    ma_user_logon_state_t value;
    if (ma_error_t err = ma_policy_uri_get_user_logon_state(get(), &value)) throw utils::exception(err);
    return value;
}

inline std::string policy_uri::get_location_id() const {
    const char *value;
    if (ma_error_t err = ma_policy_uri_get_location_id(get(), &value)) throw utils::exception(err);
    return std::string(value);
}

inline std::string policy_uri::get_feature() const {
    const char *value;
    if (ma_error_t err = ma_policy_uri_get_feature(get(), &value)) throw utils::exception(err);
    return std::string(value);
}

inline std::string policy_uri::get_category() const {
    const char *value;
    if (ma_error_t err = ma_policy_uri_get_category(get(), &value)) throw utils::exception(err);
    return std::string(value);
}

inline std::string policy_uri::get_type() const {
    const char *value;
    if (ma_error_t err = ma_policy_uri_get_type(get(), &value)) throw utils::exception(err);
    return std::string(value);
}

inline std::string policy_uri::get_name() const {
    const char *value;
    if (ma_error_t err = ma_policy_uri_get_name(get(), &value)) throw utils::exception(err);
    return std::string(value);
}

inline std::string policy_uri::get_pso_name() const {
    const char *value;
    if (ma_error_t err = ma_policy_uri_get_pso_name(get(), &value)) throw utils::exception(err);
    return std::string(value);
}

inline std::string policy_uri::get_pso_param_int() const {
    const char *value;
	if (ma_error_t err = ma_policy_uri_get_pso_param_int(get(), &value)) throw utils::exception(err);
    return std::string(value);
}

inline std::string policy_uri::get_pso_param_str() const {
    const char *value;
	if (ma_error_t err = ma_policy_uri_get_pso_param_str(get(), &value)) throw utils::exception(err);
    return std::string(value);
}

inline const_policy_uri::const_policy_uri(ma_policy_uri_t  const *c_handle) : const_handle(c_handle) {

}

inline const_policy_uri const_policy_uri::clone() const {
    return const_policy_uri(const_handle);
}

inline std::string const_policy_uri::get_software_id() const {
    const char *value;
    if (ma_error_t err = ma_policy_uri_get_software_id(const_handle, &value)) throw utils::exception(err);
    return std::string(value);
} 

inline std::string const_policy_uri::get_version() const {
    const char *value;
    if (ma_error_t err = ma_policy_uri_get_version(const_handle, &value)) throw utils::exception(err);
    return std::string(value);
} 

inline std::string const_policy_uri::get_user() const {
    const char *value;
    if (ma_error_t err = ma_policy_uri_get_user(const_handle, &value)) throw utils::exception(err);
    return std::string(value);
}

inline ::ma_uint32_t const_policy_uri::get_user_session_id() const {
    ::ma_uint32_t value;
    if (ma_error_t err = ma_policy_uri_get_user_session_id(const_handle, &value)) throw utils::exception(err);
    return value;
}

inline ma_user_logon_state_t const_policy_uri::get_user_logon_state() const {
    ma_user_logon_state_t value;
    if (ma_error_t err = ma_policy_uri_get_user_logon_state(const_handle, &value)) throw utils::exception(err);
    return value;
}

inline std::string const_policy_uri::get_location_id() const {
    const char *value;
    if (ma_error_t err = ma_policy_uri_get_location_id(const_handle, &value)) throw utils::exception(err);
    return std::string(value);
}

inline std::string const_policy_uri::get_feature() const {
    const char *value;
    if (ma_error_t err = ma_policy_uri_get_feature(const_handle, &value)) throw utils::exception(err);
    return std::string(value);
}

inline std::string const_policy_uri::get_category() const {
    const char *value;
    if (ma_error_t err = ma_policy_uri_get_category(const_handle, &value)) throw utils::exception(err);
    return std::string(value);
}

inline std::string const_policy_uri::get_type() const {
    const char *value;
    if (ma_error_t err = ma_policy_uri_get_type(const_handle, &value)) throw utils::exception(err);
    return std::string(value);
}

inline std::string const_policy_uri::get_name() const {
    const char *value;
    if (ma_error_t err = ma_policy_uri_get_name(const_handle, &value)) throw utils::exception(err);
    return std::string(value);
}

inline std::string const_policy_uri::get_pso_name() const {
    const char *value;
    if (ma_error_t err = ma_policy_uri_get_pso_name(const_handle, &value)) throw utils::exception(err);
    return std::string(value);
}

inline std::string const_policy_uri::get_pso_param_int() const {
    const char *value;
	if (ma_error_t err = ma_policy_uri_get_pso_param_int(const_handle, &value)) throw utils::exception(err);
    return std::string(value);
}

inline std::string const_policy_uri::get_pso_param_str() const {
    const char *value;
	if (ma_error_t err = ma_policy_uri_get_pso_param_str(const_handle, &value)) throw utils::exception(err);
    return std::string(value);
}

} } } // mileaccess::ma::policies

#endif // MA_POLICY_URI_HXX_INCLUDED
