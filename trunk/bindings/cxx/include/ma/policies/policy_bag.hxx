#ifndef MA_POLICY_BAG_HXX_INCLUDED
#define MA_POLICY_BAG_HXX_INCLUDED

#include "ma/policies/policy_uri.hxx"
#include "ma/variant.hxx"
#include "ma/utils/exception.hxx"


#include "ma/policy/ma_policy.h"

namespace mileaccess { namespace ma { namespace policies {

using mileaccess::ma::client;
using mileaccess::ma::variant;

struct policy_bag_iterator;

struct policy_bag {
    typedef ma_policy_bag_h handle_type;
    typedef policy_bag_iterator iterator;

    policy_bag(client const &cli, policy_uri const &uri);

    ~policy_bag();

    /*!
     * returns the value for the specified (uri, section_name, key_name) tuple
     */
    variant get_value(const_policy_uri *uri, char const *section_name, char const *key_name);
	
	variant get_value(const_policy_uri *uri, std::string const &section_name, std::string const &key_name);

	/*!
     * returns the value for the specified ( policy_uri uri, section_name, key_name) tuple
     */
	variant get_value(policy_uri const &uri, char const *section_name, char const *key_name);
	
	variant get_value(policy_uri const &uri, std::string const &section_name, std::string const &key_name);

	/*!
     * returns ma policy bag handle
     */
    handle_type get() const;

    handle_type handle;
};

inline ma_policy_bag_h policy_bag::get() const {
    return handle;
}

inline policy_bag::policy_bag(client const &cli, policy_uri const &uri) {
    if (ma_error_t err = ma_policy_get(cli.get(), uri.get(), &this->handle)) throw utils::exception(err);

}

inline policy_bag::~policy_bag() {
    if (ma_error_t err = ma_policy_bag_release(handle)) throw utils::exception(err);
}

inline variant policy_bag::get_value(const_policy_uri  *uri, char const *section_name, char const *key_name) {
    ma_variant_t *value;
    if (ma_error_t err = ma_policy_bag_get_value(get(), uri->get(), section_name, key_name, (ma_variant_t **) &value)) throw utils::exception(err);
    return variant(value, false);
}

inline variant policy_bag::get_value(const_policy_uri  *uri, std::string const &section_name, std::string const &key_name) {
    ma_variant_t *value;
    if (ma_error_t err = ma_policy_bag_get_value(get(), uri->get(), section_name.c_str(), key_name.c_str(), (ma_variant_t **) &value)) throw utils::exception(err);
    return variant(value, false);
}

inline variant policy_bag::get_value(policy_uri const &uri, char const *section_name, char const *key_name) {
    ma_variant_t *value;
    if (ma_error_t err = ma_policy_bag_get_value(get(), uri.get(), section_name, key_name, (ma_variant_t **) &value)) throw utils::exception(err);
    return variant(value, false);
}

inline variant policy_bag::get_value(policy_uri const &uri, std::string const &section_name, std::string const &key_name) {
    ma_variant_t *value;
    if (ma_error_t err = ma_policy_bag_get_value(get(), uri.get(), section_name.c_str(), key_name.c_str(), (ma_variant_t **) &value)) throw utils::exception(err);
    return variant(value, false);
}

} } } // mileaccess::ma::policies

#endif // MA_POLICY_BAG_HXX_INCLUDED
