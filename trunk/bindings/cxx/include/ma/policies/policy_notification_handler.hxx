#ifndef MA_POLICY_NOTIFICATION_HANDLER_HXX_INCLUDED
#define MA_POLICY_NOTIFICATION_HANDLER_HXX_INCLUDED

#include "ma/client.hxx"
#include "ma/policies/policy_uri.hxx"
#include "ma/utils/exception.hxx"

#include "ma/policy/ma_policy.h"

#include <string>

namespace mileaccess { namespace ma { namespace policies {

using mileaccess::ma::client;

// base class that your policy notification handler must derive from 
// and implement the on_policy_notification() method
struct policy_notification_handler {

    //! registers a notification handler. Throws a utils::exception on error
    void register_policy_notification_handler(const char *product_id, ma_uint32_t notify_type = MA_POLICY_NOTIFICATION_ALL);
	
	void register_policy_notification_handler(std::string const &product_id, ma_uint32_t notify_type = MA_POLICY_NOTIFICATION_ALL);

    //! unregisters notification handler
    void unregister_policy_notification_handler(const char *product_id);
	
	void unregister_policy_notification_handler(std::string const &product_id);

	virtual ~policy_notification_handler() {}
protected:
    //! protected constructor for your derived class to call
    policy_notification_handler(client const &c) : cli(c) {}

    //! you must implement this method in your derived class and use the above registration API to get notifications
    virtual ma_error_t on_policy_notification(client const &c, std::string const &product_id, policy_uri const &uri) = 0;

private:

    static ma_error_t policy_notification_cb(::ma_client_t *ma_client, const char *product_id, const ::ma_policy_uri_t *info_uri, void *cb_data);

    client const &cli;

    // prevent copy semantics
    policy_notification_handler(policy_notification_handler const&);
    
    policy_notification_handler &operator=(policy_notification_handler const&);
};

inline void policy_notification_handler::register_policy_notification_handler(const char *product_id, ma_uint32_t notify_type) {
    if (ma_error_t err = ma_policy_register_notification_callback(this->cli.get(), product_id, notify_type, &policy_notification_cb, this) ) throw utils::exception(err);
}

inline void policy_notification_handler::register_policy_notification_handler(std::string const &product_id, ma_uint32_t notify_type) {
    if (ma_error_t err = ma_policy_register_notification_callback(this->cli.get(), product_id.c_str(), notify_type, &policy_notification_cb, this) ) throw utils::exception(err);
}

inline void policy_notification_handler::unregister_policy_notification_handler(std::string const &product_id) {
    if (ma_error_t err = ma_policy_unregister_notification_callback(this->cli.get(), product_id.c_str()) ) throw utils::exception(err);
}

inline void policy_notification_handler::unregister_policy_notification_handler(const char *product_id) {
    if (ma_error_t err = ma_policy_unregister_notification_callback(this->cli.get(), product_id) ) throw utils::exception(err);
}

inline ma_error_t policy_notification_handler::policy_notification_cb(::ma_client_t *ma_client, const char *product_id, const ma_policy_uri_t *info_uri, void *cb_data) {
    policy_notification_handler *self = reinterpret_cast<policy_notification_handler*>(cb_data);
    try {
        policy_uri uri(const_cast<ma_policy_uri_h>(info_uri)); // Note, this currently does an extra add_ref/release, which is not technically necessary
        return self->on_policy_notification(self->cli, std::string(product_id), uri);
    } catch (...) {
    }
    return MA_ERROR_UNEXPECTED;
}

} } } // mileaccess::ma::policies

#endif // MA_POLICY_NOTIFICATION_HANDLER_HXX_INCLUDED
