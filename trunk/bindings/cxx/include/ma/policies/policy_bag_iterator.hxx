#ifndef MA_POLICY_BAG_ITERATOR_HXX_INCLUDED
#define MA_POLICY_BAG_ITERATOR_HXX_INCLUDED

#include "ma/utils/auto_handle.hxx"
#include "ma/utils/exception.hxx"
#include "ma/policies/policy_bag.hxx"
#include "ma/policies/policy_uri.hxx"

#include <string>

struct policy_bag_iterator_traits {

    typedef ::ma_policy_bag_iterator_h handle_type;

    static handle_type invalid_handle_value() {
        return 0;
    }

    static void close(handle_type h) {
        ma_policy_bag_iterator_release(h);
    }
};

namespace mileaccess { namespace ma { namespace policies {

/*!
 * C++ iterator class over a policy bag.
 * Allows you to write canonical loops over a policy bag:
 * 
 * for (policy_bag_iterator it(bag); it; ++it) {
 *   (*it).uri ... ;
 *   (*it).section_name ... ;
 *   (*it).key_name ... ;
 *   (*it).value ... ;
 * }
 *
 * Notice, you must use pre-increment operator, post-increment operator is not supported
 */

struct policy_bag_iterator : mileaccess::ma::utils::auto_handle<policy_bag_iterator_traits> {

    struct referenced {
        // C++ accessors
        const_policy_uri get_uri() const {return const_policy_uri(uri);}

        std::string get_section_name() const {return std::string(section_name);}

        std::string get_key_name() const {return std::string(key_name);}

        const_variant get_value() const {return const_variant(value);}

        // raw C members
        ma_policy_uri_t const *uri;

        char const *section_name;

        char const *key_name;

        ma_variant_t const *value;
    } r;

    ::ma_error_t result;

    /*!
     * construct iterator for given policy bag
     */
    policy_bag_iterator(policy_bag &bag);

    operator bool() const {
        return MA_ERROR_NO_MORE_ITEMS != this->result;
    } 

    referenced &operator *() {
        return this->r;
    }
    /*!
     * pre-increment
     */
    policy_bag_iterator &operator ++() {
        advance();
        return *this;
    }
    
private :

    void advance();

    // prevent accidental copying
    policy_bag_iterator(policy_bag_iterator const &);
    policy_bag_iterator &operator=(policy_bag_iterator const &);
};

policy_bag_iterator::policy_bag_iterator(policy_bag & bag) {
    if (ma_error_t err = ma_policy_bag_get_iterator(bag.get(), this->addr_of())) throw mileaccess::ma::utils::exception(err);
    advance();
}

void policy_bag_iterator::advance() {
    result = ma_policy_bag_iterator_get_next(get(), &r.uri, &r.section_name, &r.key_name, &r.value);
    if (MA_OK == result || MA_ERROR_NO_MORE_ITEMS == result) return;
    throw mileaccess::ma::utils::exception(result);
}

/*!
 * C++ iterator class over a policy bag uri's
 * Allows you to write canonical loops over a policy bag:
 * 
 * for (policy_bag_iterator it(bag); it; ++it) {
 *   (*it).uri ... ;
 * }
 *
 * Notice, you must use pre-increment operator, post-increment operator is not supported
 */

struct policy_bag_uri_iterator : mileaccess::ma::utils::auto_handle<policy_bag_iterator_traits> {

    struct referenced {
		referenced() : uri(NULL) {}
        // C++ accessors
        const_policy_uri get_uri() const {return const_policy_uri(uri);}

        // raw C members
        ma_policy_uri_t const *uri;
    } r;

    ::ma_error_t result;

    /*!
     * construct iterator for given policy bag
     */
    policy_bag_uri_iterator(policy_bag &bag);

    operator bool() const {
        return MA_ERROR_NO_MORE_ITEMS != this->result;
    } 

    referenced &operator *() {
        return this->r;
    }
    /*!
     * pre-increment
     */
    policy_bag_uri_iterator &operator ++() {
        advance();
        return *this;
    }
    
private :

    void advance();

    // prevent accidental copying
    policy_bag_uri_iterator(policy_bag_uri_iterator const &);
    policy_bag_uri_iterator &operator=(policy_bag_uri_iterator const &);
};

policy_bag_uri_iterator::policy_bag_uri_iterator(policy_bag & bag) {
    if (ma_error_t err = ma_policy_bag_get_uri_iterator(bag.get(), this->addr_of())) throw mileaccess::ma::utils::exception(err);
    advance();
}

void policy_bag_uri_iterator::advance() {
    result = ma_policy_bag_uri_iterator_get_next(get(), &r.uri);
    if (MA_OK == result || MA_ERROR_NO_MORE_ITEMS == result) return;
    throw mileaccess::ma::utils::exception(result);
}


/*!
 * C++ iterator class over a policy bag section's
 * Allows you to write canonical loops over a policy bag:
 * 
 * for (policy_bag_iterator it(bag); it; ++it) {
 *   (*it).uri ... ;
 *   (*it).section_name  ;
 * }
 *
 * Notice, you must use pre-increment operator, post-increment operator is not supported
 */

struct policy_bag_section_iterator : mileaccess::ma::utils::auto_handle<policy_bag_iterator_traits> {

    struct referenced {
		referenced() : uri(NULL), section_name(NULL) {}
        // C++ accessors
        const_policy_uri get_uri() const {return const_policy_uri(uri);}

        // raw C members
        ma_policy_uri_t const *uri;

		char const *section_name;
    } r;

    ::ma_error_t result;

    /*!
     * construct iterator for given policy bag
     */
    policy_bag_section_iterator(policy_bag &bag, const_policy_uri &uri);

    operator bool() const {
        return MA_ERROR_NO_MORE_ITEMS != this->result;
    } 

    referenced &operator *() {
        return this->r;
    }
    /*!
     * pre-increment
     */
    policy_bag_section_iterator &operator ++() {
        advance();
        return *this;
    }
    
private :

    void advance();

    // prevent accidental copying
    policy_bag_section_iterator(policy_bag_section_iterator const &);
    policy_bag_section_iterator &operator=(policy_bag_section_iterator const &);
};

policy_bag_section_iterator::policy_bag_section_iterator(policy_bag & bag, const_policy_uri &uri)  {
	if (ma_error_t err = ma_policy_bag_get_section_iterator(bag.get(), uri.get(), this->addr_of())) throw mileaccess::ma::utils::exception(err);
    advance();
}

void policy_bag_section_iterator::advance() {
	result = ma_policy_bag_section_iterator_get_next(get(), &r.section_name);
    if (MA_OK == result || MA_ERROR_NO_MORE_ITEMS == result) return;
    throw mileaccess::ma::utils::exception(result);
}


/*!
 * C++ iterator class over a policy bag settings
 * Allows you to write canonical loops over a policy bag:
 * 
 * for (policy_bag_iterator it(bag); it; ++it) {
 *   (*it).uri ... ;
 *   (*it).section_name  ;
 *   (*it).key_name  ;
 * }
 *
 * Notice, you must use pre-increment operator, post-increment operator is not supported
 */

struct policy_bag_setting_iterator : mileaccess::ma::utils::auto_handle<policy_bag_iterator_traits> {

    struct referenced {
		referenced() : uri(NULL), section_name(NULL), key_name(NULL) {}
        // C++ accessors
        const_policy_uri get_uri() const {return const_policy_uri(uri);}

        // raw C members
        ma_policy_uri_t const *uri;

		char const *section_name;

		char const *key_name;

		ma_variant_t const *key_value;
    } r;

    ::ma_error_t result;

    /*!
     * construct iterator for given policy bag
     */
    policy_bag_setting_iterator(policy_bag &bag, const_policy_uri &uri, const char *section_name);

    operator bool() const {
        return MA_ERROR_NO_MORE_ITEMS != this->result;
    } 

    referenced &operator *() {
        return this->r;
    }
    /*!
     * pre-increment
     */
    policy_bag_setting_iterator &operator ++() {
        advance();
        return *this;
    }
    
private :

    void advance();

    // prevent accidental copying
    policy_bag_setting_iterator(policy_bag_setting_iterator const &);
    policy_bag_setting_iterator &operator=(policy_bag_setting_iterator const &);
};

policy_bag_setting_iterator::policy_bag_setting_iterator(policy_bag & bag, const_policy_uri &uri, const char *section_name)  {
	if (ma_error_t err = ma_policy_bag_get_setting_iterator(bag.get(), uri.get(), section_name, this->addr_of())) throw mileaccess::ma::utils::exception(err);
    advance();
}

void policy_bag_setting_iterator::advance() {
	result = ma_policy_bag_setting_iterator_get_next(get(), &r.key_name, &r.key_value);
    if (MA_OK == result || MA_ERROR_NO_MORE_ITEMS == result) return;
    throw mileaccess::ma::utils::exception(result);
}


} } } // mileaccess::ma::policies

#endif // MA_POLICY_BAG_ITERATOR_HXX_INCLUDED
