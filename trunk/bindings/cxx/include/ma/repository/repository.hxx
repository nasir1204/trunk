#ifndef MA_REPOSITORY_HXX_INCLUDED
#define MA_REPOSITORY_HXX_INCLUDED

#include "ma/utils/exception.hxx"
#include "ma/repository/ma_repository_list.h"

namespace mileaccess { namespace ma {

struct repository_traits {
    typedef ::ma_repository_h handle_type;

    static handle_type invalid_handle_value() {
        return 0;
    }

	static void add_ref(handle_type h) {
		::ma_repository_add_ref(h);
	}

    static void close(handle_type h) {
        ::ma_repository_release(h);
    }
};

/*!
	* wrapper over ma repository. Allows copying by applying reference counting on the underlying ma_repository_h handle
*/
struct Repository: mileaccess::ma::utils::copyable_handle<repository_traits> {
	/*!
	 * c'tor,
     * creates a C++ Repository from the underlying C handle.
     * @param h Handle to an existing ma repository object
     * @param add_ref Specifies if the reference count should be bumped also. If add_ref is set to true, the caller still assumes ownership of the original handle and must release it
     */
	Repository(mileaccess::ma::utils::copyable_handle<repository_traits>::handle_type h, bool add_ref);
	Repository();
	~Repository();

	/** 
	 * @brief set repository type.
	 * @param type              [in]     repository type.
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
	void set_type(ma_repository_type_t const &type);

	/** 
	 * @brief set repository state.
	 * @param state              [in]     repository state.
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
	void set_state(ma_repository_state_t const &state);

	/** 
	 * @brief set repository url tyep.
	 * @param url_type              [in]     repository url type.
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
	void set_url_type(ma_repository_url_type_t const &url_type);

	/** 
	 * @brief set repository name.
	 * @param name              [in]     repository name.
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
	void set_name(std::string const &name);

	/** 
	 * @brief set repository fqdn.
	 * @param fqdn              [in]     repository fqdn.
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
	void set_server_fqdn(std::string const &fqdn);

	/** 
	 * @brief set repository server name.
	 * @param server_name              [in]     repository server name.
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
	void set_server_name(std::string const &server_name);

	/** 
	 * @brief set repository server ip.
	 * @param server_ip              [in]     repository server ip.
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
	void set_server_ip(std::string const &server_ip); 

	/** 
	 * @brief set repository path.
	 * @param path              [in]     repository path.
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
	void set_server_path(std::string const &path);

	/** 
	 * @brief set repository enable/disable.
	 * @param is_enabled              [in]     repository is_enabled(1:enable, 0:disable).
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
	void set_enabled(::ma_bool_t const &is_enabled);

	/** 
	 * @brief set repository authentication type.
	 * @param type              [in]     repository authentication type.
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
	void set_authentication(ma_repository_auth_t const &type);

	/** 
	 * @brief set repository namespace.
	 * @param repo_namespace              [in]     repository namespace.
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
	void set_namespace(ma_repository_namespace_t const &repo_namespace);

	/** 
	 * @brief set proxy usage type.
	 * @param proxy_usage              [in]     proxy usage type.
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
	void set_proxy_usage_type(ma_proxy_usage_type_t const &proxy_usage);

	/** 
	 * @brief set port.
	 * @param port              [in]     port.
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
	void set_port(::ma_uint32_t const &port);

	/** 
	 * @brief set ssl port.
	 * @param port              [in]     ssl port.
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
	void set_ssl_port(::ma_uint32_t const &port);

	/** 
	 * @brief set user name.
	 * @param user_name              [in]     user name
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
	void set_user_name(std::string const &user_name);

	/** 
	 * @brief set domain name.
	 * @param domain_name              [in]     domain name
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
	void set_domain_name(std::string const &domain_name);

	/** 
	 * @brief set password.
	 * @param password              [in]     password string.
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
	void set_password(std::string const &password);

	/** 
	 * @brief set ping time.
	 * @param pingtime              [in]     ping time.
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
	void set_pingtime(::ma_uint32_t const &pingtime);

	/** 
	 * @brief set subnet distance.
	 * @param subnetdistance              [in]     subnet distance.
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
	void  set_subnetdistance(::ma_uint32_t const &subnetdistance);

	/** 
	 * @brief set siteorder.
	 * @param siteorder              [in]     site order.
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
	void set_siteorder(::ma_uint32_t const &siteorder);

	/** 
	 * @brief get repository name.
	 * @result                  repository name            
	 *                                      
	 * @remark               
	 *
	 */
	std::string get_name() const;

	/** 
	 * @brief get repository type.
	 * @result                 repository type             
	 *                                      
	 * @remark               
	 *
	 */
	ma_repository_type_t get_type() const;

	/** 
	 * @brief get repository url type.

	 * @result                      repository url type        
	 *                                      
	 * @remark               
	 *
	 */
	ma_repository_url_type_t get_url_type() const;

	/** 
	 * @brief get repository name space.
	 * @result                  name space            
	 *                                      
	 * @remark               
	 *
	 */
	ma_repository_namespace_t get_namespace( ) const;

	/** 
	 * @brief get repository authentication type.
	 * @result                  repository authentication type.
	 *                                      
	 * @remark               
	 *
	 */
	ma_repository_auth_t get_authentication() const;

	/** 
	 * @brief get proxy usage type.
	 * @result                              proxy usage type.
	 *                                      
	 * @remark               
	 *
	 */
	ma_proxy_usage_type_t get_proxy_usage_type() const;

	/** 
	 * @brief get serve fqdn.
	 * @result                         repository server fqdn.     
	 *                                      
	 * @remark               
	 *
	 */
	std::string get_server_fqdn() const;

	/** 
	 * @brief get serve name.
	 * @result               repository server name.               
	 *                                      
	 * @remark               
	 *
	 */
	std::string get_server_name() const;

	/** 
	 * @brief get serve path.
	 * @result                              repository server path.
	 *                                      
	 * @remark               
	 *
	 */
	std::string get_server_path() const;

	/** 
	 * @brief get port number.
	 * @result                 port number.              
	 *                                      
	 * @remark               
	 *
	 */
	::ma_uint32_t get_port() const;

	/** 
	 * @brief get ssl port number.
	 * @result                              ssl port number.
	 *                                      
	 * @remark               
	 *
	 */
	::ma_uint32_t get_ssl_port() const;

	/** 
	 * @brief get repository state (enable/disable).
	 * @result                              repository state (enable/disable).
	 *                                      
	 * @remark               
	 *
	 */
	::ma_bool_t get_enabled() const ;

	/** 
	 * @brief get user name.
	 * @result              user name.                
	 *                                      
	 * @remark               
	 *
	 */
	std::string get_user_name() const;

	/** 
	 * @brief get domain name.
	 * @result                domain name.              
	 *                                      
	 * @remark               
	 *
	 */
	std::string get_domain_name() const;

	/** 
	 * @brief get password.
	 * @result             password                 
	 *                                      
	 * @remark               
	 *
	 */
	std::string get_password() const;

	/** 
	 * @brief get repository ping time.
	 * @result                               repository ping time.
	 *                                      
	 * @remark               
	 *
	 */
	::ma_uint32_t get_pingtime() const;

	/** 
	 * @brief get repository sub distance
	 * @result                           repository subnet distance.   
	 *                                      
	 * @remark               
	 *
	 */
	::ma_uint32_t get_subnetdistance() const;

	/** 
	 * @brief get repository siteorder
	 * @result                              repository siteorder.
	 *                                      
	 * @remark               
	 *
	 */
	::ma_uint32_t get_siteorder() const;

	/** 
	 * @brief check whether repository is excluded or not
	 * @param repo_name		             [in]   name of repository
	 * @param configuration              [in]   proxy config
	 * @result                              excluded or not.
	 *                                      
	 * @remark               
	 *
	 */
	::ma_bool_t excusion_check(std::string const &repo_name, ma_proxy_config_t const &configuration) const;

	/** 
	 * @brief get server ip
	 * @result                              server ip
	 *                                      
	 * @remark               
	 *
	 */
	std::string get_server_ip()const;

	/** 
	 * @brief get repository state
	 * @result                              states
	 *                                      
	 * @remark               
	 *
	 */
	ma_repository_state_t get_state() const;

	/** 
	 * @brief check validity of repository
	 * @result                              validity state
	 *                                      
	 * @remark               
	 *
	 */
	::ma_bool_t validate() const;
};

inline Repository::Repository(mileaccess::ma::utils::copyable_handle<repository_traits>::handle_type h, bool add_ref = true): mileaccess::ma::utils::copyable_handle<repository_traits>(h, add_ref){
}

inline Repository::Repository(){
	ma_repository_create(&this->handle);
}

inline Repository::~Repository(){
	ma_repository_release(this->handle);
}

inline void Repository::set_type(ma_repository_type_t const &type){
	if(ma_error_t err = ma_repository_set_type(this->handle, type)) throw utils::exception(err);
}

inline void Repository::set_state(ma_repository_state_t const &state){
	if(ma_error_t err = ma_repository_set_state(this->handle, state)) throw utils::exception(err);
}

inline void Repository::set_url_type(ma_repository_url_type_t const &url_type){
	if(ma_error_t err =  ma_repository_set_url_type(this->handle, url_type)) throw utils::exception(err);
}

inline void Repository::set_name(std::string const &name){
	if(ma_error_t err = ma_repository_set_name(this->handle, name.c_str())) throw utils::exception(err);
}

inline void Repository::set_server_fqdn(std::string const &server_fqdn){
	if(ma_error_t err = ma_repository_set_server_fqdn(this->handle, server_fqdn.c_str())) throw utils::exception(err);
}

inline void Repository::set_server_name(std::string const &server_name){
	if(ma_error_t err = ma_repository_set_server_name(this->handle, server_name.c_str())) throw utils::exception(err);
}

inline void Repository::set_server_ip(std::string const &server_ip){
	if(ma_error_t err = ma_repository_set_server_ip(this->handle, server_ip.c_str())) throw utils::exception(err);
}

inline void Repository::set_server_path(std::string const &repo_path){
	if(ma_error_t err = ma_repository_set_server_path(this->handle, repo_path.c_str())) throw utils::exception(err);
}

inline void Repository::set_enabled(ma_bool_t const &is_enabled){
	if(ma_error_t err = ma_repository_set_enabled(this->handle, is_enabled)) throw utils::exception(err);
}

inline void Repository::set_authentication(ma_repository_auth_t const &type){
	if(ma_error_t err = ma_repository_set_authentication(this->handle, type)) throw utils::exception(err);
}

inline void Repository::set_namespace(ma_repository_namespace_t const &repo_namespace){
	if(ma_error_t err = ma_repository_set_namespace(this->handle, repo_namespace)) throw utils::exception(err);
}

inline void Repository::set_proxy_usage_type(ma_proxy_usage_type_t const &proxy_usage){
	if(ma_error_t err = ma_repository_set_proxy_usage_type(this->handle, proxy_usage)) throw utils::exception(err);
}

inline void Repository::set_port(ma_uint32_t const &port){
	if(ma_error_t err = ma_repository_set_port(this->handle, port)) throw utils::exception(err);
}

inline void Repository::set_ssl_port(ma_uint32_t const &port){
	if(ma_error_t err = ma_repository_set_ssl_port(this->handle, port)) throw utils::exception(err);
}

inline void Repository::set_user_name(std::string const &user_name){
	if(ma_error_t err = ma_repository_set_user_name(this->handle, user_name.c_str())) throw utils::exception(err);
}

inline void Repository::set_domain_name(std::string const &domain_name){
	if(ma_error_t err = ma_repository_set_domain_name(this->handle, domain_name.c_str())) throw utils::exception(err);
}

inline void Repository::set_password(std::string const &password){
	if(ma_error_t err = ma_repository_set_password(this->handle, password.c_str())) throw utils::exception(err);
}

inline void Repository::set_pingtime(ma_uint32_t const &pingtime){
	if(ma_error_t err = ma_repository_set_pingtime(this->handle, pingtime)) throw utils::exception(err);
}

inline void Repository::set_subnetdistance(ma_uint32_t const &subnetdistance){
	if(ma_error_t err = ma_repository_set_subnetdistance(this->handle, subnetdistance)) throw utils::exception(err);
}

inline void Repository::set_siteorder(ma_uint32_t const &siteorder){
	if(ma_error_t err = ma_repository_set_siteorder(this->handle, siteorder)) throw utils::exception(err);
}

inline std::string Repository::get_name() const{
	const char *repo = NULL;
	std::string repository_name;
	if(ma_error_t err = ma_repository_get_name(this->handle, &repo)) throw utils::exception(err);
	if(repo){
		repository_name = repo;
	}
	return repository_name;
}

inline ma_repository_type_t Repository::get_type() const{
	ma_repository_type_t repository_type;
	if(ma_error_t err = ma_repository_get_type(this->handle, &repository_type)) throw utils::exception(err);
	return repository_type;
}

inline ma_repository_url_type_t Repository::get_url_type() const{
	ma_repository_url_type_t url_type;
	if(ma_error_t err = ma_repository_get_url_type(this->handle, &url_type)) throw utils::exception(err);
	return url_type;
}

inline ma_repository_namespace_t Repository::get_namespace() const{
	ma_repository_namespace_t repo_namespace;
	if(ma_error_t err = ma_repository_get_namespace(this->handle, &repo_namespace)) throw utils::exception(err);
	return repo_namespace;
}

inline ma_repository_auth_t Repository::get_authentication() const{
	ma_repository_auth_t type;
	if(ma_error_t err = ma_repository_get_authentication(this->handle, &type)) throw utils::exception(err);
	return type;
}

inline ma_proxy_usage_type_t Repository::get_proxy_usage_type() const{
	ma_proxy_usage_type_t proxy_usage;
	if(ma_error_t err = ma_repository_get_proxy_usage_type(this->handle, &proxy_usage)) throw utils::exception(err);
	return proxy_usage;
}

inline std::string Repository::get_server_fqdn() const{
	std::string server_fqdn;
	const char *fqdn = NULL;
	if(ma_error_t err = ma_repository_get_server_fqdn(this->handle, &fqdn)) throw utils::exception(err);
	if(fqdn){
		server_fqdn = fqdn;
	}
	return server_fqdn;
}

inline std::string Repository::get_server_name() const{
	std::string server_name;
	const char *name = NULL;
	if(ma_error_t err = ma_repository_get_server_name(this->handle, &name)) throw utils::exception(err);
	if(name){
		server_name = name;
	}
	return server_name;
}

inline std::string Repository::get_server_path() const{
	std::string server_path;
	const char *path = NULL;
	if(ma_error_t err = ma_repository_get_server_path(this->handle, &path)) throw utils::exception(err);
	if(path){
		server_path = path;
	}
	return server_path;
}

inline ::ma_uint32_t Repository::get_port() const{
	::ma_uint32_t port = 0;
	if(ma_error_t err = ma_repository_get_port(this->handle, &port)) throw utils::exception(err);
	return port;
}

inline ::ma_uint32_t Repository::get_ssl_port() const{
	::ma_uint32_t port = 0;
	if(ma_error_t err = ma_repository_get_ssl_port(this->handle, &port)) throw utils::exception(err);
	return port;
}

inline ::ma_bool_t Repository::get_enabled() const{
	::ma_bool_t is_enabled;
	if(ma_error_t err = ma_repository_get_enabled(this->handle, &is_enabled)) throw utils::exception(err);
	return is_enabled;
}

inline std::string Repository::get_user_name() const{
	std::string user_name;
	const char *name = NULL;
	if(ma_error_t err = ma_repository_get_user_name(this->handle, &name)) throw utils::exception(err);
	if(name){
		user_name = name;
	}
	return user_name;
}

inline std::string Repository::get_domain_name()const{
	std::string domain_name;
	const char *name = NULL;
	if(ma_error_t err = ma_repository_get_domain_name(this->handle, &name)) throw utils::exception(err);
	if(name){
		domain_name = name;
	}
	return domain_name;
}

inline std::string Repository::get_password()const{
	std::string password;
	const char *p = NULL;
	if(ma_error_t err = ma_repository_get_password(this->handle, &p)) throw utils::exception(err);
	if(p){
		password = p;
	}
	return password;
}

inline ::ma_uint32_t Repository::get_pingtime() const{
	::ma_uint32_t pingtime;
	if(ma_error_t err = ma_repository_get_pingtime(this->handle, &pingtime)) throw utils::exception(err);
	return pingtime;
}

inline ::ma_uint32_t Repository::get_subnetdistance() const{
	::ma_uint32_t subnetdistance;
	if(ma_error_t err = ma_repository_get_subnetdistance(this->handle, &subnetdistance)) throw utils::exception(err);
	return subnetdistance;
}

inline ::ma_uint32_t Repository::get_siteorder() const{
	::ma_uint32_t siteorder;
	if(ma_error_t err = ma_repository_get_siteorder(this->handle, &siteorder)) throw utils::exception(err);
	return siteorder;
}

inline ::ma_bool_t Repository::excusion_check(std::string const &repo_name, ma_proxy_config_t const &configuration) const{
	::ma_bool_t is_excuded;
	if(ma_error_t err = ma_repository_excusion_check(const_cast <char*> (repo_name.c_str()), const_cast <ma_proxy_config_t*> (&configuration), &is_excuded)) throw utils::exception(err);
	return is_excuded;
}

inline std::string Repository::get_server_ip()const{
	std::string server_ip;
	const char *ip = NULL;
	if(ma_error_t err = ma_repository_get_server_ip(this->handle, &ip)) throw utils::exception(err);
	if(ip){
		server_ip = ip;
	}
	return server_ip;
}

inline ma_repository_state_t Repository::get_state()const{
	ma_repository_state_t state;
	if(ma_error_t err = ma_repository_get_state(this->handle, &state)) throw utils::exception(err);
	return state;
}

inline ::ma_bool_t Repository::validate() const{
	::ma_bool_t is_valid;
	ma_error_t err = ma_repository_validate(this->handle);
	if(MA_OK == err)
		is_valid = 1;
	else
		is_valid = 0;
	return is_valid;
}


//------------------------------------------------------------------------------------------------------------------------------

struct RepositoryList_traits {
    typedef ::ma_repository_list_h handle_type;

    static handle_type invalid_handle_value() {
        return 0;
    }

	static void add_ref(handle_type h) {
	}

    static void close(handle_type h) {
        ::ma_repository_list_release(h);
    }
};

/*!
	* wrapper over ma repository list. Allows copying by applying reference counting on the underlying ma_repository_h handle
*/
struct RepositoryList : mileaccess::ma::utils::copyable_handle<RepositoryList_traits> {
	/*!
	 * c'tor,
     * creates a C++ RepositoryList from the underlying C handle.
     * @param h Handle to an existing ma repository list object
     * @param add_ref Specifies if the reference count should be bumped also. If add_ref is set to true, the caller still assumes ownership of the original handle and must release it
     */
	RepositoryList(mileaccess::ma::utils::copyable_handle<RepositoryList_traits>::handle_type h, bool add_ref);

	/** 
	 * @brief add repository in list.
	 * @param Repository              [in]     repository object
	 * @result							                       
	 *                                      
	 * @remark               
	 *
	 */
	void add_repository(Repository const &repo);

	/** 
	 * @brief get number of repositories in list.
	 * @result						repositories count	                       
	 *                                      
	 * @remark               
	 *
	 */
	size_t get_repo_count()const;

	/** 
	 * @brief get repository by index from list.
	 * @param index              [in]     repository index in list
	 * @result							  repository at given index                     
	 *                                      
	 * @remark               
	 *
	 */
	Repository& get_repository_by_index(::ma_int32_t const &index) const;

	/** 
	 * @brief get repository by name from list.
	 * @param name              [in]     repository name in list
	 * @result							                       
	 *                                      
	 * @remark               
	 *
	 */
	Repository& get_repository_by_name(std::string const &name) const;
	
	private:
		RepositoryList(RepositoryList const &r) ;
		RepositoryList const &operator= (RepositoryList const &r);
};

inline RepositoryList::RepositoryList(mileaccess::ma::utils::copyable_handle<RepositoryList_traits>::handle_type h, bool add_ref = true): mileaccess::ma::utils::copyable_handle<RepositoryList_traits>(h, add_ref){
}

inline void RepositoryList::add_repository(Repository const &repo) {
	ma_repository_t *ma_repo = repo.get();
	if(ma_error_t err = ma_repository_list_add_repository(this->handle, ma_repo)) throw utils::exception(err);
}

inline size_t RepositoryList::get_repo_count() const {
	size_t count = 0;
	if(ma_error_t err = ma_repository_list_get_repositories_count(this->handle, &count)) throw utils::exception(err);
		return count;
}

inline Repository& RepositoryList::get_repository_by_index(::ma_int32_t const &index)const {
	ma_repository_t *ma_repo = NULL;
	ma_error_t err ;
	if(MA_OK != (err =ma_repository_list_get_repository_by_index(this->handle, index, &ma_repo))) 
		throw utils::exception(err) ;
	return *(new Repository(ma_repo));
}

inline Repository& RepositoryList::get_repository_by_name(std::string const &name)const {
	ma_repository_t *ma_repo = NULL;
	ma_error_t err ;
	if(MA_OK != (err = ma_repository_list_get_repository_by_name(this->handle, name.c_str(), &ma_repo)))
		throw utils::exception(err) ;
	return *(new Repository(ma_repo));
}

} }

#endif // MA_REPOSITORY_HXX_INCLUDED
