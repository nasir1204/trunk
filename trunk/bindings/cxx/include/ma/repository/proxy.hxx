#ifndef MA_PROXY_HXX_INCLUDED
#define MA_PROXY_HXX_INCLUDED

#include "ma/utils/exception.hxx"
#include "ma/proxy/ma_proxy_config.h"

namespace mileaccess { namespace ma {

/*Since this is not reference counted, do not return as reference */
struct proxy_traits {
    typedef ::ma_proxy_h handle_type;

    static handle_type invalid_handle_value() {
        return 0;
    }

	static void add_ref(handle_type h) {
		
	}

    static void close(handle_type h) {
        ::ma_proxy_release(h);
    }
};

/*!
	* wrapper over ma proxy. Allows copying by applying reference counting on the underlying ma_proxy_h handle
*/
struct Proxy: mileaccess::ma::utils::copyable_handle<proxy_traits> {
	/*!
	 * c'tor,
     * creates a C++ proxy from the underlying C handle.
     * @param h Handle to an existing ma proxy object
     * @param add_ref Specifies if the reference count should be bumped also. If add_ref is set to true, the caller still assumes ownership of the original handle and must release it
     */
	Proxy(mileaccess::ma::utils::copyable_handle<proxy_traits>::handle_type h, bool add_ref);

	/** 
	 * @brief set proxy address.
	 * @param address              [in]     proxy address string.
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
	void set_address(std::string const &address);

	/** 
	 * @brief get proxy address.     
	 * @result                    proxy address string.          
	 *                                      
	 * @remark               
	 *
	 */
	std::string get_address() const;

	/** 
	 * @brief set proxy port.
	 * @param port              [in]     port number.
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
	void set_port(::ma_int32_t const &port);

	/** 
	 * @brief get proxy port.          
	 * @result                port number              
	 *                                      
	 * @remark               
	 *
	 */
	::ma_int32_t get_port() const;

	/** 
	 * @brief set proxy scopeid.
	 * @param scopeid              [in]     scopeid.
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
	void set_scopeid(::ma_int32_t const &scopeid);

	/** 
	 * @brief get proxy scopeid.            
	 * @result                   scopeid           
	 *                                      
	 * @remark               
	 *
	 */
	::ma_int32_t get_scopeid() const;

	/** 
	 * @brief set proxy protocol type.
	 * @param type              [in]     type.
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
	void set_protocol_type(ma_proxy_protocol_type_t const &type);

	/** 
	 * @brief get proxy protocol type.
	 * @result                  type            
	 *                                      
	 * @remark               
	 *
	 */
	ma_proxy_protocol_type_t get_protocol_type() const;

	/** 
	 * @brief set proxy authentication.
	 * @param auth_req              [in]     authentication type.
	 * @param user_name             [in]     user name.
	 * @param password             [in]     password.
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
	void set_authentication(::ma_bool_t const &auth_req, std::string const &user_name, std::string const &password);

	/** 
	 * @brief get proxy authentication.
	 * @param auth_req              [in]     authentication type.
	 * @param user_name             [in]     user name.
	 * @param password             [in]     password.
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
	void get_authentication(::ma_bool_t &auth_req, std::string &user_name, std::string &password) const;

	/** 
	 * @brief set proxy flag.
	 * @param flag              [in]     flag.
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
	void set_flag(::ma_int32_t const &flag);

	/** 
	 * @brief get proxy flag.           
	 * @result               flag               
	 *                                      
	 * @remark               
	 *
	 */
	::ma_int32_t get_flag() const;
	private:
		Proxy(Proxy const &r) ;
		Proxy const &operator= (Proxy const &r);
};

inline Proxy::Proxy(mileaccess::ma::utils::copyable_handle<proxy_traits>::handle_type h, bool add_ref = true): mileaccess::ma::utils::copyable_handle<proxy_traits>(h, add_ref){
}

inline void Proxy::set_address(std::string const &address){
	if (ma_error_t err = ma_proxy_set_address(this->handle, address.c_str())) 
		throw utils::exception(err);
}

inline std::string Proxy::get_address() const{
	const char *ad = NULL;
	if(ma_error_t err = ma_proxy_get_address(this->handle, &ad)) 
		throw utils::exception(err);

	std::string address = ad;
	return address;
}

inline void Proxy::set_port(::ma_int32_t const &port){
	if(ma_error_t err = ma_proxy_set_port(this->handle, port)) 
		throw utils::exception(err);
}
inline ::ma_int32_t Proxy::get_port() const{
	ma_int32_t port = 0;
	if(ma_error_t err = ma_proxy_get_port(this->handle, &port)) 
		throw utils::exception(err);
	return port;
}
inline void Proxy::set_scopeid(::ma_int32_t const &scopeid){
	if(ma_error_t err = ma_proxy_set_scopeid(this->handle, scopeid)) 
		throw utils::exception(err);
}
inline int Proxy::get_scopeid() const{
	ma_uint32_t scopeid;
	if(ma_error_t err = ma_proxy_get_scopeid(this->handle, &scopeid)) 
		throw utils::exception(err);
	return (int)scopeid;
}
inline void Proxy::set_protocol_type(ma_proxy_protocol_type_t const &type){
	if(ma_error_t err = ma_proxy_set_protocol_type(this->handle, type)) 
		throw utils::exception(err);
}
inline ma_proxy_protocol_type_t Proxy::get_protocol_type() const{
	ma_proxy_protocol_type_t type;
	if(ma_error_t err = ma_proxy_get_protocol_type(this->handle, &type)) 
		throw utils::exception(err);
	return type;
}
inline void Proxy::set_authentication(::ma_bool_t const &auth_req, std::string const &user_name, std::string const &password){
	if(ma_error_t err = ma_proxy_set_authentication(this->handle, auth_req, user_name.c_str(), password.c_str())) 
		throw utils::exception(err);
}
inline void Proxy::get_authentication(::ma_bool_t &auth_req, std::string &user_name, std::string &password) const {
	const char *name = NULL;
	const char *pass = NULL;
	if(ma_error_t err = ma_proxy_get_authentication(this->handle, &auth_req, &name, &pass)) 
		throw utils::exception(err);
	if(name){
		user_name = name;
	}
	if(pass){
		password = pass;
	}
}
inline void Proxy::set_flag(::ma_int32_t const &flag){
	if(ma_error_t err = ma_proxy_set_flag(this->handle, flag)) 
		throw utils::exception(err);
}
inline ::ma_int32_t Proxy::get_flag() const{
	ma_int32_t flag;
	if(ma_error_t err = ma_proxy_get_flag(this->handle, &flag)) 
		throw utils::exception(err);
	return flag;
}

struct ProxyConfig_traits {
    typedef ::ma_proxy_config_h handle_type;

    static handle_type invalid_handle_value() {
        return 0;
    }

	static void add_ref(handle_type h) {
		::ma_proxy_config_add_ref(h);
	}

    static void close(handle_type h) {
        ::ma_proxy_config_release(h);
    }
};

/*!
	* wrapper over ma proxy config. Allows copying by applying reference counting on the underlying ma_proxy_config_h handle
*/
struct ProxyConfig : mileaccess::ma::utils::copyable_handle<ProxyConfig_traits> {
	ProxyConfig();
	~ProxyConfig();
	/*!
	 * c'tor,
     * creates a C++ proxy config from the underlying C handle.
     * @param h Handle to an existing ma proxy config object
     * @param add_ref Specifies if the reference count should be bumped also. If add_ref is set to true, the caller still assumes ownership of the original handle and must release it
     */
	ProxyConfig(mileaccess::ma::utils::copyable_handle<ProxyConfig_traits>::handle_type h, bool add_ref);

	/** 
	 * @brief set proxy usage type
	 * @param type              [in]     proxy usage type
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
	void set_proxy_usage_type(ma_proxy_usage_type_t const &type);

	/** 
	 * @brief get proxy usage type
	 * @result                  proxy usage type            
	 *                                      
	 * @remark               
	 *
	 */
	ma_proxy_usage_type_t get_proxy_usage_type() const;

	/** 
	 * @brief set proxy config authentication.
	 * @param protocol_type         [in]     protocol type.
	 * @param auth_req              [in]     authentication type.
	 * @param user_name             [in]     user name.
	 * @param password              [in]     password.
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
	void set_system_proxy_authentication(ma_proxy_protocol_type_t const &protocol_type, ::ma_bool_t const &auth_req, std::string const &user_name, std::string const &password);

		/** 
	 * @brief get proxy config authentication.
	 * @param protocol_type         [in]     protocol type.
	 * @param auth_req              [in]     authentication type.
	 * @param user_name             [in]     user name.
	 * @param password              [in]     password.
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
	void get_system_proxy_authentication(ma_proxy_protocol_type_t &protocol_type, ::ma_bool_t &auth_req, std::string &user_name, std::string &password) const;

	/** 
	 * @brief set proxy config local configuration.
	 * @param allow_or_not              [in]     local configuration flag(1: allow, 0: not allow).
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
	void set_allow_local_proxy_configuration(::ma_bool_t const &allow_or_not);

	/** 
	 * @brief get proxy config local configuration.
	 * @result									local configuration flag on success.                     
	 *                                      
	 * @remark               
	 *
	 */
	::ma_bool_t get_allow_local_proxy_configuration() const;

	/** 
	 * @brief set proxy config bypass local configuration.
	 * @param bypass_local              [in]     bypass local configuration.
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
	void set_bypass_local(::ma_bool_t const &bypass_local);

	/** 
	 * @brief get bypass local configuration.
	 * @result									bypass local flag        
	 *                                      
	 * @remark               
	 *
	 */
	::ma_bool_t get_bypass_local() const;

	/** 
	 * @brief set proxy config exclusion url.
	 * @param exclusion_urls              [in]     exclusion url list
	 * @result                              
	 *                                      
	 * @remark               
	 *
	 */
	void set_exclusion_urls(std::string const &exclusion_urls);

	/** 
	 * @brief get proxy config exclusion url.
	 * @result										NULL, if api success and no exclusion url list.
	 *												non-NULL, if api success and exclusion url list available.
	 *                                      
	 * @remark               
	 *
	 */
	std::string get_exclusion_urls()const;
};

inline ProxyConfig::ProxyConfig(){
	ma_proxy_config_create(&this->handle);
}
inline ProxyConfig::~ProxyConfig(){
	ma_proxy_config_release(this->handle);
}
inline ProxyConfig::ProxyConfig(mileaccess::ma::utils::copyable_handle<ProxyConfig_traits>::handle_type h, bool add_ref = true): mileaccess::ma::utils::copyable_handle<ProxyConfig_traits>(h, add_ref){
}

inline void ProxyConfig::set_proxy_usage_type(ma_proxy_usage_type_t const &type){
	if(ma_error_t err = ma_proxy_config_set_proxy_usage_type(this->handle, type)) 
		throw utils::exception(err);
}
inline ma_proxy_usage_type_t ProxyConfig::get_proxy_usage_type() const{
	ma_proxy_usage_type_t type;
	if(ma_error_t err = ma_proxy_config_get_proxy_usage_type(this->handle, &type)) 
		throw utils::exception(err);
	return type;
}
inline void ProxyConfig::set_system_proxy_authentication(ma_proxy_protocol_type_t const &protocol_type, ::ma_bool_t const &auth_req, std::string const &user_name, std::string const &password){
	if(ma_error_t err = ma_proxy_config_set_system_proxy_authentication(this->handle, protocol_type, auth_req, user_name.c_str(), password.c_str())) 
		throw utils::exception(err);
}
inline void ProxyConfig::get_system_proxy_authentication(ma_proxy_protocol_type_t &protocol_type, ::ma_bool_t &auth_req, std::string &user_name, std::string &password) const{
	const char *user = NULL;
	const char *pass = NULL;
	if(ma_error_t err = ma_proxy_config_get_system_proxy_authentication(this->handle, protocol_type, &auth_req, &user, &pass)) 
		throw utils::exception(err);
	if(user){
		user_name = user;
	}
	if(pass){
		password = pass;
	}
}
inline void ProxyConfig::set_allow_local_proxy_configuration(::ma_bool_t const &allow_or_not){
	if(ma_error_t err = ma_proxy_config_set_allow_local_proxy_configuration(this->handle, allow_or_not)) 
		throw utils::exception(err);
}
inline ::ma_bool_t ProxyConfig::get_allow_local_proxy_configuration()const{
	ma_bool_t allow_or_not;
	if(ma_error_t err = ma_proxy_config_get_allow_local_proxy_configuration(this->handle, &allow_or_not)) throw utils::exception(err);
	return allow_or_not;
}
inline void ProxyConfig::set_bypass_local(::ma_bool_t const &bypass_local){
	if(ma_error_t err = ma_proxy_config_set_bypass_local(this->handle, bypass_local)) 
		throw utils::exception(err);
}
inline ::ma_bool_t ProxyConfig::get_bypass_local() const{
	ma_bool_t bypass_local;
	if(ma_error_t err = ma_proxy_config_get_bypass_local(this->handle, &bypass_local)) 
		throw utils::exception(err);
	return bypass_local;
}
inline void ProxyConfig::set_exclusion_urls(std::string const &exclusion_urls){
	if(ma_error_t err = ma_proxy_config_set_exclusion_urls(this->handle, exclusion_urls.c_str())) 
		throw utils::exception(err);
}
inline std::string ProxyConfig::get_exclusion_urls() const{
	const char *exclusion_urls = NULL;
	if(ma_error_t err = ma_proxy_config_get_exclusion_urls(this->handle, &exclusion_urls)) 
		throw utils::exception(err);
	std::string ret(exclusion_urls);
	return ret;
}


struct ProxyList_traits {
    typedef ::ma_proxy_list_h handle_type;

    static handle_type invalid_handle_value() {
        return 0;
    }

	static void add_ref(handle_type h) {
		::ma_proxy_list_add_ref(h);
	}

    static void close(handle_type h) {
        ::ma_proxy_list_release(h);
    }
};

/*!
	* wrapper over ma proxy list. Allows copying by applying reference counting on the underlying ma_proxy_list_h handle
*/
struct ProxyList: mileaccess::ma::utils::copyable_handle<ProxyList_traits> { 
	ProxyList();
	~ProxyList();
	/*!
	 * c'tor,
     * creates a C++ proxy list from the underlying C handle.
     * @param h Handle to an existing ma proxy list object
     * @param add_ref Specifies if the reference count should be bumped also. If add_ref is set to true, the caller still assumes ownership of the original handle and must release it
     */
	ProxyList(mileaccess::ma::utils::copyable_handle<ProxyList_traits>::handle_type h, bool add_ref);

	/** 
	 * @brief get c++ Proxy object at index location in list.
	 * @param index              [in]     index
	 * @result                            Proxy object on success  
	 *                                      
	 * @remark               
	 *
	 */
	Proxy* get_proxy(ma_int32_t const &index) const;

	/** 
	 * @brief get number of proxies in proxy list.
	 * @result                            number of proxies in proxy list.
	 *                                      
	 * @remark               
	 *
	 */
	size_t get_proxy_list_size() const;
};


inline ProxyList::ProxyList(mileaccess::ma::utils::copyable_handle<ProxyList_traits>::handle_type h, bool add_ref = true): mileaccess::ma::utils::copyable_handle<ProxyList_traits>(h, add_ref){
}
inline Proxy* ProxyList::get_proxy(ma_int32_t const &index) const{
	ma_proxy_t *ma_proxy = NULL;
	ma_proxy_list_get_proxy(this->handle, index, &ma_proxy);
	return new Proxy(ma_proxy);
}

inline size_t ProxyList::get_proxy_list_size() const{
	size_t size = 0;
	if(ma_error_t err = ma_proxy_list_size(this->handle, &size)) 
		throw utils::exception(err);
    return size;
}

inline ProxyList::ProxyList() {
    ma_proxy_list_create(&this->handle);
}
inline ProxyList::~ProxyList() {
    ma_proxy_list_release(this->handle);
}


} }

#endif // MA_PROXY_HXX_INCLUDED
