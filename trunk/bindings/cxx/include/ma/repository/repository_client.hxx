#ifndef MA_REPOSITORY_CLIENT_HXX_INCLUDED
#define MA_REPOSITORY_CLIENT_HXX_INCLUDED

#include "ma/client.hxx"
#include "ma/utils/exception.hxx"
#include "ma/buffer.hxx"
#include "ma/repository/ma_repository_client.h"
#include "ma/repository/repository.hxx"
#include "ma/repository/proxy.hxx"
#include <string>
#include <exception>

namespace mileaccess { namespace ma {
using mileaccess::ma::client;
/*!
	* wrapper over ma repository client.
*/
struct RepositoryClient {
	/** 
	 * @brief get repository list.
	 * @result                          RepositoryList on success.    
	 *                                      
	 * @remark               
	 *
	 */
    RepositoryList* get_repositories()const;

	/** 
	 * @brief set repository list.
	 * @param repository_list              [in]     repository_list.
	 * @result                          
	 *                                      
	 * @remark               
	 *
	 */
	void set_repositories(RepositoryList const &repository_list);

	/** 
	 * @brief import repository list from repository buffer .
	 * @param repo_buffer              [in]     repository buffer.
	 * @result                          
	 *                                      
	 * @remark               
	 *
	 */
	buffer & import_repositories_buffer() const;
	
	/** 
	 * @brief get proxy config .
	 * @result								       proxy config on success      
	 *                                      
	 * @remark               
	 *
	 */
	ProxyConfig& get_proxy_config()const;

	/** 
	 * @brief set proxy config .
	 * @param proxy_config_hdl              [in]   proxy config
	 * @result                          
	 *                                      
	 * @remark               
	 *
	 */
	void set_proxy_config(ProxyConfig const &proxy_config);

	/** 
	 * @brief get proxy config in async way .
	 * @param cb_data              [in]   callback data (can be NULL)
	 * @result                          
	 *                                      
	 * @remark               
	 *
	 */
	void get_async_proxy_config(void *cb_data) const;

	/** 
	 * @brief get system proxy list.
	 * @param url              [in]     url for which system proxy to detect.
	 * @result                          ProxyList on success.    
	 *                                      
	 * @remark               
	 *
	 */
	ProxyList& get_system_proxy(std::string const &url) const;

	/*!
	 * c'tor,
     * @param c						[in]     The client used for repository.
     */
    explicit RepositoryClient(client const &c);
private:
    client const &cli;
};

inline RepositoryList* RepositoryClient::get_repositories() const {
	ma_repository_list_t *repository_list = NULL;
   	ma_error_t err;
	if(MA_OK != (err = ma_repository_client_get_repositories(cli.get(), &repository_list))) 
		throw utils::exception(err) ;

	return (new RepositoryList (repository_list));
}

inline void RepositoryClient::set_repositories(RepositoryList const &repository_list_hdl) {
	ma_repository_list_t *repository_list = repository_list_hdl.handle;
    ma_error_t err;
	if(MA_OK != (err= ma_repository_client_set_repositories(cli.get(), repository_list))) 
		throw utils::exception(err) ;
}

inline buffer & RepositoryClient::import_repositories_buffer() const{
	ma_error_t err;  
	ma_buffer_t *ma_repo_buffer = NULL;
	if(MA_OK != (err = ma_repository_client_import_repositories(cli.get(), ma_repo_buffer)))  
		throw utils::exception(err) ;
	return *(new buffer(ma_repo_buffer));
}

inline ProxyConfig& RepositoryClient::get_proxy_config() const{
	ma_proxy_config_t *proxy_config = NULL;
	ma_error_t err = ma_repository_client_get_proxy_config(cli.get(), &proxy_config);
	if(err) throw utils::exception(err) ;
	return *(new ProxyConfig(proxy_config));
}

inline void RepositoryClient::set_proxy_config(ProxyConfig const &proxy_config_hdl){
	ma_proxy_config_t *ma_proxy_config = proxy_config_hdl.handle;
    ma_error_t err =  ma_repository_client_set_proxy_config(cli.get(), ma_proxy_config);
	if(err) throw utils::exception(err) ;
}

inline RepositoryClient::RepositoryClient(mileaccess::ma::client const &cli):cli(cli){
}

/*User has to implement this callback and play arround config object */
inline ma_error_t proxy_config_get_cb(ma_error_t status, ma_client_t *ma_client, const ma_proxy_config_t *proxy_config, void *cb_data) {
	if(MA_OK == status){
		ProxyConfig proxy_config_hdl (const_cast<ma_proxy_config_t*>(proxy_config));
		return status;
	}
	else
		throw utils::exception(status) ;
}

inline void RepositoryClient::get_async_proxy_config(void *cb_data) const{
	ma_error_t err = ma_repository_client_async_get_proxy_config(cli.get(), proxy_config_get_cb, cb_data);
	if(err) throw utils::exception(err) ;
}

inline ProxyList& RepositoryClient::get_system_proxy(std::string const &url) const{
	ma_proxy_list_t *ma_proxy_list = NULL;
	ma_error_t err = ma_repository_client_get_system_proxy(cli.get(), url.c_str(), &ma_proxy_list);
	if(err) throw utils::exception(err) ;
	return *(new ProxyList(ma_proxy_list));
}

}} // namespace mileaccess::ma

#endif /* MA_REPOSITORY_CLIENT_HXX_INCLUDED */
