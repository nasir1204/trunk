/* Java Script helper functions for viewing log remotely  */
var xmlhttp;
function displayData(myObject)
{
	clearTab1TableData();
	var SecondRow=document.getElementById("MetaDataTable").rows[0];
	var x = SecondRow.insertCell(1);	
	x.innerHTML=myObject[0]["Version"];
	var ThirdRow=document.getElementById("MetaDataTable").rows[1];
	var y =  ThirdRow.insertCell(1);
	y.innerHTML=myObject[0]["AgentHostName"];
	for(var i=1;i<myObject.length;i++)
	{   
		time = MyGetDateTime(myObject[i]["Time"]);
		var table=document.getElementById("DataTable").tBodies[0];
		var row=table.insertRow(-1);
		var cell1=row.insertCell(0);
		var cell2=row.insertCell(1);
		var cell3=row.insertCell(2);
		var cell4=row.insertCell(3);
		cell1.innerHTML=time;
		cell2.innerHTML=myObject[i]["Severity"];
		cell3.innerHTML=myObject[i]["Facility"];
		cell4.innerHTML=myObject[i]["Message"];
		if(myObject[i]["Facility"] == 'Error')
			row.setAttribute("style", "background:#ffcccc");
		else if (myObject[i]["Facility"] == 'Warning')
			row.setAttribute("style", "background:#ffcc99");
		else 
			row.setAttribute("style", "background:#FFFFFF");
	}
}

function clearTab1TableData()
{
      var SecondRow=document.getElementById("MetaDataTable").rows[0];
      SecondRow.deleteCell(1);      
      var ThirdRow=document.getElementById("MetaDataTable").rows[1];
      ThirdRow.deleteCell(1);
      var table = document.getElementById("DataTable");
      for(var i = table.rows.length - 1; i > 0; i--)
      {
          table.deleteRow(i);
      }
}

function onJSONData()
{
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
		var myObject = JSON.parse(xmlhttp.responseText);
		displayData(myObject);
		loadData();
    }
}
function displayFile() 
{ 
	if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {	
		data = xmlhttp.responseText;
		data = data.replace(/</g,"&lt");
		data = data.split("\n").join("<br>");		
		document.getElementById("fileData").innerHTML = data;
    }
}
function getFileFromServer(fileName, cfunc)
{	
	if (window.XMLHttpRequest) // code for IE7+, Firefox, Chrome, Opera, Safari
	{
	  xmlhttp=new XMLHttpRequest();
	}
	else // code for IE6, IE5
	{
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=cfunc;

	xmlhttp.open("GET",fileName,true);
	xmlhttp.send();
}
function displayResult()
{
	// Attaching date and time also to make the request as unique for every time for IE11 - BZ - 966789 
	var d = new Date();
    	var t = d.getTime();
	getFileFromServer("AgentLog.json?t="+t, onJSONData);   
}
function clearSelectOptions(element)
{
	while(element.options.length > 0){                
		element.remove(0);
	}
}
function onProductDetails()
{
	var selectBox = document.getElementById("products");
	clearSelectOptions(selectBox);
	selectBox.options[selectBox.options.length] = new Option("---Select Product---", "-1");
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
		var myObject = JSON.parse(xmlhttp.responseText);
		loadMetadata(myObject);
	    
	    for (var i = 0; i < myObject.length; i++){
	    	if(myObject[i].product_id != undefined)
	    		selectBox.options[selectBox.options.length] = new Option( myObject[i].product_name, myObject[i].product_id);
	    }
    }
	clearFilesList();
}
function loadMetadata(data)
{
	var myObject = data;
	var ThirdRowCells=document.getElementById("MetaDataTable2").rows[0].cells;
	ThirdRowCells[1].innerHTML=myObject[0]["AgentHostName"];
}
function populateFiles()
{
	var selectBox = document.getElementById("files");
	clearSelectOptions(selectBox);
	selectBox.options[selectBox.options.length] = new Option( "---Select Log File---", "-1");
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
		var fileList = JSON.parse(xmlhttp.responseText);
		for (var i = 0; i < fileList.length; i++){
			selectBox.options[selectBox.options.length] = new Option( fileList[i].log_file_name, fileList[i].product_id);
		}
		
    }
}
function clearFilesList()
{
	var selectBox = document.getElementById("files");
	clearSelectOptions(selectBox);
	selectBox.options[selectBox.options.length] = new Option( "---Select Log File---", "-1");
	clearFile();
}
function clearFile()
{
	document.getElementById("fileData").innerHTML = "";
	document.getElementById("fileName").value = "";
}
function loadFilesList(id)
{
	clearFilesList();	
	if(id > 0)
	{
		var productId = getSelectedValue('products');
		getFileFromServer('LoadLogFileNames?product_id='+productId,populateFiles);
	}
}
function loadFile(id)
{
	clearFile();
	if(id > 0)
	{
		document.getElementById("fileData").innerHTML = '<br><br><div align="center"> Please wait..</div>';
		document.getElementById("fileName").value= getSelectedText('files');
		var productId = getSelectedValue('products');
		// Attaching date and time also to make the request as unique for every time for IE11 - BZ - 966789 
		var d = new Date();
		var t = d.getTime();
		getFileFromServer("DisplayLogFile?log_file_name="+getSelectedText('files')+"&product_id="+productId+"&t="+t,displayFile);
	}
}

function getSelectedText(eleId) {
    var element = document.getElementById(eleId);

    if (element.selectedIndex == -1)
        return null;

    return element.options[element.selectedIndex].text;
}

function getSelectedValue(eleId) {
    var element = document.getElementById(eleId);

    if (element.selectedIndex == -1)
        return null;

    return element.options[element.selectedIndex].value;
}

function getSelectedId(eleId) {
    var element = document.getElementById(eleId);
    
    if (element.selectedIndex == -1)
        return null;
    
    return element.options[element.selectedIndex].id;
}

function loadProductDetails()
{
	getFileFromServer("LoadProductNames", onProductDetails);
}
function downLoadFile()
{
	var fileName = document.getElementById("fileName").value;
	if(fileName.length > 0)
	{
		var productId = getSelectedValue('products');
		window.open("DownLoadLogFile?log_file_name="+fileName+"&product_id="+productId);
	}
}
//Format the date (value = 2008-05-30T14:32:44) to a localized string 
function MyGetDateTime(value) 
{
    try
    {
        var Strs;
        var Str1;
        regExpression = /-|T|:/;
        Str1 = value;
        d = new Date(2007,08,22);
        Strs = Str1.split(regExpression);
        d.setFullYear (Strs[0]);
        d.setMonth(Strs[1]-1);
        d.setDate(Strs[2]);
        d.setHours(Strs[3]);
        d.setMinutes(Strs[4]);
        d.setSeconds(Strs[5]);
        return d.toLocaleString();
     }
     catch(o)
     {
     //in case of wrong format or unavailable return the original value
        return value;
     }
}

function loadData() {
	 loadProductDetails();
	 resizeWindow();
}

function resizeWindow()
{
	 var winHeight = 0;
	 if( typeof( window.innerWidth ) == 'number' ) 
	 {
		    //Non-IE
		     winHeight = window.innerHeight;
	 } else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) 
	 {
		    //IE 6+ in 'standards compliant mode'
		    winHeight = document.documentElement.clientHeight;
	 }
	 document.getElementById("dataDiv").style.height=winHeight-150 + "px" ;
	 document.getElementById("fileData").style.height=winHeight-160 + "px";
}

function swap(id1, id2) {
	document.getElementById('tab-'+id1).style.display = 'block';
	document.getElementById('tab-'+id2).style.display = 'none';
	document.getElementById('link-'+id1).className += ' active';
	document.getElementById('link-'+id2).className = "";
}

function TableSorter(){
	var table = Object;
	var rows = Array;
	var cols = Array;
	var curSortCol = Object;
	var prevSortCol = '1';

	function getCell(index){
		return rows[index].cells[curSortCol]; 
	}
	
	function get(index){
		return getCell(index).firstChild.nodeValue;
	}
	
	this.initialize = function(tableName)
	{
		table = document.getElementById(tableName);
		cols = table.getElementsByTagName("th");
		for(var i = 0; i < cols.length ; i++)
		{
			cols[i].onclick = function()
			{
				sort(this);
			}
		}
		return true;
	};
		
	function sort(tHeader)
	{
		curSortCol = tHeader.cellIndex;
		sortType = tHeader.abbr;
		rows = table.tBodies[0].getElementsByTagName("tr");

		if(prevSortCol == curSortCol)
		{
			tHeader.className = (tHeader.className != 'ascend' ? 'ascend' : 'descend' );
			reverseTable();
		}
		else
		{
			tHeader.className = 'ascend';
			if(cols[prevSortCol].className == 'ascend' || cols[prevSortCol].className == 'descend')
			{
				cols[prevSortCol].className = 'nosort';
			}
			quicksort(0, rows.length);
		}
		prevSortCol = curSortCol;
	}
	
	function quicksort(low, high)
	{
		if(high <= low+1) return;
		 
		if((high - low) == 2) {
			if(get(high-1) > get(low)) swap(high-1, low);
			return;
		}
		
		var i = low + 1;
		var j = high - 1;
		
		if(get(low) > get(i)) swap(i, low);
		if(get(j) > get(low)) swap(low, j);
		if(get(low) > get(i)) swap(i, low);
		
		var pivot = get(low);
		
		while(true) {
			j--;
			while(pivot > get(j)) j--;
			i++;
			while(get(i) > pivot) i++;
			if(j <= i) break;
			swap(i, j);
		}
		swap(low, j);
		
		if((j-low) < (high-j)) {
			quicksort(low, j);
			quicksort(j+1, high);
		} else {
			quicksort(j+1, high);
			quicksort(low, j);
		}
	}

	function swap(i, j)
	{
		if(i == j+1) {
			table.tBodies[0].insertBefore(rows[i], rows[j]);
		} else if(j == i+1) {
			table.tBodies[0].insertBefore(rows[j], rows[i]);
		} else {
			var tmpNode = table.tBodies[0].replaceChild(rows[i], rows[j]);
			if(typeof(rows[i]) == "undefined") {
				table.appendChild(tmpNode);
			} else {
				table.tBodies[0].insertBefore(tmpNode, rows[i]);
			}
		}
	}
	
	function reverseTable()
	{
		for(var i = 1; i<rows.length; i++)
		{
			table.tBodies[0].insertBefore(rows[i], rows[0]);
		}
	}
}