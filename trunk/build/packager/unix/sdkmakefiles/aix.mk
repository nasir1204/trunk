#This file contains all aix specific compile stuff
#define the compile flags

ifeq ($(CONFIGURATION),debug)
CFLAGS:=  -g -Wall -DDEBUG -D_DEBUG   -pthread
CXXFLAGS:= -g  -Wall -DDEBUG -D_DEBUG -pthread
else
CFLAGS:=  -Wall   -pthread
CXXFLAGS:=  -Wall -pthread
endif

##Define the shared library extension
SO=so
BOOST_EXTN=-gcc-mt
BOOST_THREAD:=$(BOOST_THREAD)$(BOOST_EXTN)
BOOST_SERIALIZATION:=$(BOOST_SERIALIZATION)$(BOOST_EXTN)
BOOST_WSERIALIZATION:=$(BOOST_WSERIALIZATION)$(BOOST_EXTN)
BOOST_FILESYSTEM:=$(BOOST_FILESYSTEM)$(BOOST_EXTN)
BOOST_UNIT_TEST_FRAMEWORK:=$(BOOST_UNIT_TEST_FRAMEWORK)$(BOOST_EXTN)

#define the compiler
CC=gcc
CXX=g++

#Extend the defines to specify aix
DEFINES:=$(DEFINES) -DAIX -D_AIX_ -DPLATFORM_AIX -DNO_CERT -DAIX4_3  
#DEFINES:=$(DEFINES) -DAIX -D_AIX_ -DPLATFORM_AIX -DAIX4_3 
PLATFORM=AIX
ENGINE_INCLUDES=-I$(CMA_BASE)/include/aix/engine
