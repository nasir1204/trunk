# This is the guy who actually creates a shared library for you depending on the platforms

ifndef OUTDIR
OUTDIR=../unix/$(CONFIGURATION)
endif

#MAC needs directories ppc and i386 in addition to $OUTDIR for universal binary creation
ifdef MACX
OUTDIRPPC=$(OUTDIR)/ppc
OUTDIRINTEL=$(OUTDIR)/i386
endif

ifdef LNX
include $(SOURCE_BASE)/project/unix/linux.mk
PLATFORMID=LNX
else 
ifdef SOL
include $(SOURCE_BASE)/project/unix/solaris.mk
PLATFORMID=SOL
else
ifdef WSH
include $(SOURCE_BASE)/project/unix/scm.mk
PLATFORMID=WSH
else
ifdef MACX
include $(SOURCE_BASE)/project/unix/macosx.mk
PLATFORMID=MACX
else
ifdef HPUX
include $(SOURCE_BASE)/project/unix/hpux.mk
PLATFORMID=HPUX
else
ifdef AIX
include $(SOURCE_BASE)/project/unix/aix.mk
PLATFORMID=AIX
else
ifdef MLOS
include $(SOURCE_BASE)/project/unix/mlos.mk
PLATFORMID=MLOS
else
ifdef LIFELINE
include $(SOURCE_BASE)/project/unix/lifeline.mk
PLATFORMID=LIFELINE
endif
endif
endif
endif
endif
endif
endif
endif

CFLAGS:=$(CFLAGS) -c -fPIC
CXXFLAGS:=$(CXXFLAGS) -c -fPIC

ifndef MACX
SONAME:=$(SONAME).$(SO)
SOLIBNAME=$(SONAME).$(SOMAJVER)
SOFILENAME=$(SOLIBNAME).$(SOMINVER)
ifdef SOL
LFLAGS:=$(LFLAGS) -G -o $(SOFILENAME) 
else
ifdef HPUX
LFLAGS:=$(LFLAGS) -fPIC -shared   -o $(SOFILENAME) -lgcc_s -lpthread
else
ifdef AIX
LFLAGS:=$(LFLAGS)  -pthread -fPIC -shared -o $(SOFILENAME)  -Wl,-brtl,-G,-berok,-bM:SRE -lpthread -lstdc++ -liconv
else
LFLAGS:=$(LFLAGS) -shared -Wl,-soname,$(SOLIBNAME) -o $(SOFILENAME) -ldl
endif
endif
endif
LINKER=gcc
else
ARCH=arch
ARCHPPC=-arch ppc
ARCHINTEL=-arch i386
SOLIBNAME=$(SONAME:.$(SO)=).$(SOMAJVER).$(SO)
SOFILENAME=$(SONAME:.$(SO)=).$(SOMAJVER).$(SOMINVER).$(SO)
SONAME:=$(SONAME).$(SO)
LFLAGS:=$(LFLAGS) $(ARCH) -o $(SOFILENAME) -single_module -undefined warning -flat_namespace -dynamiclib -multiply_defined warning -isysroot $(MACOSX_SDKROOT) -install_name /Library/mileaccess/cma/lib/$(SOLIBNAME)
LINKER=g++
endif


all:  $(CONFIGURATION)  $(SOFILENAME)

.PHONY: $(CONFIGURATION)

$(CONFIGURATION) : 
ifndef MACX
	if [ ! -e $(OUTDIR) ] ;then mkdir $(OUTDIR);fi
else
	if [ ! -e $(OUTDIRPPC) ] ;then mkdir -p $(OUTDIRPPC);fi
	if [ ! -e $(OUTDIRINTEL) ] ;then mkdir -p $(OUTDIRINTEL);fi
endif

ifeq ($(nodeps),1)
DEPLIBS=
endif

ifndef MACX
VPATH=$(SOURCES):$(OUTDIR)
ASMCOMPILE=$(CC) $(CFLAGS) $(DEFINES) $(INCLUDES) -o $(OUTDIR)/${@F} $<
CPPCOMPILE=$(CXX) $(CXXFLAGS) $(DEFINES) $(INCLUDES) -o $(OUTDIR)/${@F} $<
CCOMPILE=$(CC) $(CFLAGS) $(DEFINES) $(INCLUDES) -o $(OUTDIR)/${@F} $<
else
VPATH=$(SOURCES):$(OUTDIRPPC):$(OUTDIRINTEL)
ASMCOMPILE=$(CC) $(CFLAGS) $(ARCH) $(DEFINES) $(INCLUDES)
CPPCOMPILE=$(CXX) $(CXXFLAGS) $(ARCH) $(DEFINES) $(INCLUDES)
CCOMPILE=$(CC) $(CFLAGS) $(ARCH) $(DEFINES) $(INCLUDES)
endif

ifndef MACX
.s.o:
	@echo Compiling  $<
	$(ASMCOMPILE)
.c.o:
	@echo Compiling  $<
	$(CCOMPILE)
.cpp.o:
	@echo Compiling  $<
	$(CPPCOMPILE)
else
.s.o:
	@echo Compiling  $<
	$(ASMCOMPILE:$(ARCH)=$(ARCHPPC)) -o $(OUTDIRPPC)/${@F} $<
	$(ASMCOMPILE:$(ARCH)=$(ARCHINTEL)) -o $(OUTDIRINTEL)/${@F} $<
.c.o:
	@echo Compiling  $<
	$(CCOMPILE:$(ARCH)=$(ARCHPPC)) -o $(OUTDIRPPC)/${@F} $<
	$(CCOMPILE:$(ARCH)=$(ARCHINTEL)) -o $(OUTDIRINTEL)/${@F} $<
.cpp.o:
	@echo Compiling  $<
	$(CPPCOMPILE:$(ARCH)=$(ARCHPPC)) -o $(OUTDIRPPC)/${@F} $<
	$(CPPCOMPILE:$(ARCH)=$(ARCHINTEL)) -o $(OUTDIRINTEL)/${@F} $<
endif

.SUFFIXES: .c .cpp .s .o

ifndef MACX
$(SOFILENAME) : $(DEPLIBS) $(OBJS) $(MAKEFILE)
	cd $(OUTDIR); \
	$(LINKER) $(LFLAGS) $(OBJS) $(LIBPATH) $(LIBS);\
	if [ ! -e $(SOURCE_BASE)/lib/built/$(CONFIGURATION) ]; then mkdir -p  $(SOURCE_BASE)/lib/built/$(CONFIGURATION); fi;\
	cp $(SOFILENAME) $(SOURCE_BASE)/lib/built/$(CONFIGURATION); 
	cd $(SOURCE_BASE)/lib/built/$(CONFIGURATION);	\
	if [ ! -L $(SOLIBNAME) ]; then ln -sf $(SOFILENAME) $(SOLIBNAME); fi;	\
	if [ ! -L $(SONAME) ]; then ln -sf $(SOLIBNAME) $(SONAME); fi;
else
$(SOFILENAME) :  $(OBJS) $(MAKEFILE)
	cd $(OUTDIRPPC); \
	$(LINKER) $(LFLAGS:$(ARCH)=$(ARCHPPC)) $(LIBPATH) $(OBJS) $(LIBS);
	cd $(OUTDIRINTEL);\
	$(LINKER) $(LFLAGS:$(ARCH)=$(ARCHINTEL)) $(LIBPATH) $(OBJS) $(LIBS);
	cd $(OUTDIR);\
	/usr/bin/lipo -create $(ARCHPPC) ppc/$(SOFILENAME) $(ARCHINTEL) i386/$(SOFILENAME) -output $(SOFILENAME);\
	if [ ! -e $(SOURCE_BASE)/lib/built/$(CONFIGURATION) ]; then mkdir -p  $(SOURCE_BASE)/lib/built/$(CONFIGURATION); fi;\
	cp $(SOFILENAME) $(SOURCE_BASE)/lib/built/$(CONFIGURATION); 
	cd $(SOURCE_BASE)/lib/built/$(CONFIGURATION);	\
	if [ ! -L $(SOLIBNAME) ]; then ln -sf $(SOFILENAME) $(SOLIBNAME); fi;	\
	if [ ! -L $(SONAME) ]; then ln -sf $(SOLIBNAME) $(SONAME); fi;
endif

