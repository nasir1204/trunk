# This is the guy who actually creates a shared library for you depending on the platforms
ifndef OUTDIR
OUTDIR=../unix/$(CONFIGURATION)
endif

#MAC needs directories ppc and i386 in addition to $OUTDIR for universal binary creation
ifdef MACX
OUTDIRPPC=$(OUTDIR)/ppc
OUTDIRINTEL=$(OUTDIR)/i386
endif

ifdef LNX
include $(SOURCE_BASE)/project/unix/linux.mk
PLATFORMID=LNX
else 
ifdef SOL
include $(SOURCE_BASE)/project/unix/solaris.mk
PLATFORMID=SOL
else
ifdef WSH
include $(SOURCE_BASE)/project/unix/scm.mk
PLATFORMID=WSH
else
ifdef MACX
include $(SOURCE_BASE)/project/unix/macosx.mk
PLATFORMID=MACX
else
ifdef HPUX
include $(SOURCE_BASE)/project/unix/hpux.mk
PLATFORMID=HPUX
else
ifdef AIX
include $(SOURCE_BASE)/project/unix/aix.mk
PLATFORMID=AIX
else
ifdef MLOS
include $(SOURCE_BASE)/unix/buildproject/mlos.mk
PLATFORMID=MLOS
else
ifdef LIFELINE
include $(CMA_BASE)/unix/buildproject/lifeline.mk
PLATFORMID=LIFELINE
endif
endif
endif
endif
endif
endif
endif
endif

CFLAGS:=$(CFLAGS) -c 
CXXFLAGS:=$(CXXFLAGS) -c

ifndef MACX
ARCHIVE_FILE_NAME:=$(ARCHIVENAME).a
ifdef SOL
ARCHIVER=/usr/ccs/bin/ar
else
ARCHIVER=ar
endif
ARCHIVER_FLAGS=-rcs
else
ARCHIVER=libtool
ARCHIVER_FLAGS=-static
ARCH=arch
ARCHPPC=-arch ppc
ARCHINTEL=-arch i386
ARCHIVE_FILE_NAME:=$(ARCHIVENAME).a
endif

all:  $(CONFIGURATION)  $(ARCHIVE_FILE_NAME)

.PHONY: $(CONFIGURATION)

$(CONFIGURATION) : 
ifndef MACX
	if [ ! -e $(OUTDIR) ] ;then mkdir $(OUTDIR);fi
else
	if [ ! -e $(OUTDIRPPC) ] ;then mkdir -p $(OUTDIRPPC);fi
	if [ ! -e $(OUTDIRINTEL) ] ;then mkdir -p $(OUTDIRINTEL);fi
endif

ifndef MACX
VPATH=$(SOURCES):$(OUTDIR)
ASMCOMPILE=$(CC) $(CFLAGS) $(DEFINES) $(INCLUDES) -o $(OUTDIR)/${@F} $<
CPPCOMPILE=$(CXX) $(CXXFLAGS) $(DEFINES) $(INCLUDES) -o $(OUTDIR)/${@F} $<
CCOMPILE=$(CC) $(CFLAGS) $(DEFINES) $(INCLUDES) -o $(OUTDIR)/${@F} $<
else
VPATH=$(SOURCES):$(OUTDIRPPC):$(OUTDIRINTEL)
ASMCOMPILE=$(CC) $(CFLAGS) $(ARCH) $(DEFINES) $(INCLUDES)
CPPCOMPILE=$(CXX) $(CXXFLAGS) $(ARCH) $(DEFINES) $(INCLUDES)
CCOMPILE=$(CC) $(CFLAGS) $(ARCH) $(DEFINES) $(INCLUDES)
endif

ifndef MACX
.c.o:
	@echo Compiling  $<
	$(CCOMPILE)
.cpp.o:
	@echo Compiling  $<
	$(CPPCOMPILE)
else
.c.o:
	@echo Compiling  $<
	$(CCOMPILE:$(ARCH)=$(ARCHPPC)) -o $(OUTDIRPPC)/${@F} $<
	$(CCOMPILE:$(ARCH)=$(ARCHINTEL)) -o $(OUTDIRINTEL)/${@F} $<
.cpp.o:
	@echo Compiling  $<
	$(CPPCOMPILE:$(ARCH)=$(ARCHPPC)) -o $(OUTDIRPPC)/${@F} $<
	$(CPPCOMPILE:$(ARCH)=$(ARCHINTEL)) -o $(OUTDIRINTEL)/${@F} $<
endif

.SUFFIXES: .c .cpp .o

ifndef MACX
$(ARCHIVE_FILE_NAME) : $(OBJS) $(MAKEFILE)
	cd $(OUTDIR); \
	$(ARCHIVER) $(ARCHIVER_FLAGS) $@ $(OBJS);\
	if [ ! -e $(SOURCE_BASE)/lib/built/$(CONFIGURATION) ]; then mkdir -p  $(SOURCE_BASE)/lib/built/$(CONFIGURATION); fi;\
	cp $(ARCHIVE_FILE_NAME) $(SOURCE_BASE)/lib/built/$(CONFIGURATION); 
else
$(ARCHIVE_FILE_NAME) : $(OBJS) $(MAKEFILE)
	cd $(OUTDIRPPC); \
	$(ARCHIVER) $(ARCHIVER_FLAGS) -o $@ $(OBJS);
	cd $(OUTDIRINTEL);\
	$(ARCHIVER) $(ARCHIVER_FLAGS) -o $@ $(OBJS);
	cd $(OUTDIR);\
	/usr/bin/lipo -create $(ARCHPPC) ppc/$(ARCHIVE_FILE_NAME) $(ARCHINTEL) i386/$(ARCHIVE_FILE_NAME) -output $(ARCHIVE_FILE_NAME);\
	if [ ! -e $(SOURCE_BASE)/lib/built/$(CONFIGURATION) ]; then mkdir -p  $(SOURCE_BASE)/lib/built/$(CONFIGURATION); fi;\
	cp $(ARCHIVE_FILE_NAME) $(SOURCE_BASE)/lib/built/$(CONFIGURATION); 
endif

