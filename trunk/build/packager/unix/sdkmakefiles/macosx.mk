#This file contains all linux specific compile stuff
SHELL=/bin/bash
#define the compile flags
ifeq ($(CONFIGURATION),debug)
CFLAGS:=  -g -Wall -DDEBUG -D_DEBUG
CXXFLAGS:=  -g -Wall -DDEBUG -D_DEBUG
else
CFLAGS:=  -Wall
CXXFLAGS:= -Wall
endif

##Define the shared library extension
SO=dylib
ifndef BOOST_EXTN
BOOST_EXTN=-1_32
endif
BOOST_SERIALIZATION:=$(BOOST_SERIALIZATION)$(BOOST_EXTN)
BOOST_WSERIALIZATION:=$(BOOST_WSERIALIZATION)$(BOOST_EXTN)

#define the compiler
CC=gcc
CXX=g++

#Extend the defines to specify linux
#-DNOXOCDE is temporary and will be removed when makefile build on MAC is stabilized
DEFINES:=$(DEFINES) -DMACX -D__APPLE__ -DNOXCODE
PLATFORM=MACX

