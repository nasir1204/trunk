#Copyright mileaccess Inc 2006, All rights reserved 

########## Version information
ifndef MYSOMAJVER
MYSOMAJVER = 4
endif
ifndef MYSOMINVER
MYSOMINVER = 6 
endif
ifndef MYSUBMINVER
MYSUBMINVER = 0
endif
ifndef MYSOBLDNUM
MYSOBLDNUM = 999
endif
ifndef MYHOTFIXNUM
MYHOTFIXNUM = 
endif


SOMAJVER=$(MYSOMAJVER)
SOMINVER=$(MYSOMINVER)
SUBMINVER=$(MYSUBMINVER)
SOBLDNUM=$(MYSOBLDNUM)


########## Configuration information
ifeq ($(CONFIG),debug)
CONFIGURATION=debug
else
CONFIGURATION=release
endif

########## boost defines
ifndef BOOST_INCLUDE
BOOST_INCLUDE=$(SOURCE_BASE)/boost/boost/boost_1_39_0
endif
ifndef BOOST_LIB
BOOST_LIB=$(SOURCE_BASE)/boost/boost/lib
endif

###The library names will be suffixed with the platform specific extensions, hence use the simply expanded form
BOOST_SERIALIZATION:=boost_serialization
BOOST_WSERIALIZATION:=boost_wserialization

####### Common include paths
#sub makes may want to append additional stuff to INCLUDES, so we have the "simply expanded" flavor for INCLUDES
INCLUDES:= 	-I$(SOURCE_BASE)/include -I../../include -I../include -I$(BOOST_INCLUDE)

DEFINES:=	-D_LINUX_REDHAT \
		-DSOMAJVER=$(MYSOMAJVER)\
		-DSOMINVER=$(MYSOMINVER) \
		-DSUBMINVER=$(MYSUBMINVER) \
		-DSOBLDNUM=$(MYSOBLDNUM) \
		-DHOTFIXNUM=$(MYHOTFIXNUM) \
		-D_REENTRANT \

LIBPATH:=	-L$(BOOST_LIB) 

SOURCES:=	./source \
		../source

MAKEFILE=Makefile

