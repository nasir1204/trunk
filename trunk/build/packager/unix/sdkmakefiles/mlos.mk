#This file contains all linux specific compile stuff
#define the compile flags

ifeq ($(CONFIGURATION),debug)
CFLAGS:=  -g -Wall -DDEBUG -D_DEBUG
CXXFLAGS:= -g  -Wall -DDEBUG -D_DEBUG
else
CFLAGS:=  -Wall
CXXFLAGS:=  -Wall
endif

##Define the shared library extension
SO=so
ifndef BOOST_EXTN
BOOST_EXTN=-gcc-mt
endif

BOOST_SERIALIZATION:=$(BOOST_SERIALIZATION)$(BOOST_EXTN)
BOOST_WSERIALIZATION:=$(BOOST_WSERIALIZATION)$(BOOST_EXTN)

#define the compiler
CC=gcc
CXX=g++

#Extend the defines to specify linux
DEFINES:=$(DEFINES) -DLNX
PLATFORM=LNX

