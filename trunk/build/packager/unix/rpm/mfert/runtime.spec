Name		: MFErt
Version		: RPMVERSION
Release		: 1
Summary		: Run time libraries 
Group       : System
License     : commercial. See the COPYRIGHT file for details.
Vendor		: mileaccess Inc.
Packager	: mileaccess Inc.
BuildRoot 	: /tmp/MFErt-%{version}-buildroot


%undefine __check_files
%define _source_filedigest_algorithm 0
%define _binary_filedigest_algorithm 0
%define _binary_payload w9.gzdio

%description
Run time libraries needed by mileaccess products on Linux


%install
cp -rf %{buildroot}_tmp/* %{buildroot}

%pre

user=`id | cut -d'=' -f2 | cut -d\( -f1`
if [ $user != 0 ]; then
    echo "This package needs root authentication to install the package."
    exit 1
fi

if [ -a /opt ]; then
    if [ ! -d /opt ]; then
        echo "/opt exists and is not a directory. Exiting!!!"
        exit 2
    else
        lines=`df -l /opt | wc -l`
        if [ $lines -lt 2 ]; then
            echo "/opt is not a locally mounted file system"
            echo "This software can only be installed on a locally mounted filesystem."
            exit 3
        fi
        diskavail=`df -l -k /opt | awk '{if(NR != 1) print $0}' | sed -e's/\n//g'`
        diskavail=`echo $diskavail | cut -d' ' -f4`
    fi
else
    diskavail=`df -l -k / | awk '{if(NR != 1) print $0}' | sed -e's/\n//g'`
    diskavail=`echo $diskavail | cut -d' ' -f4`
fi

#The build script is going to change this to a valid value.

REQUIRED_INSTALLATION_SIZE=XXXXXX #To be inserted from the build script.

echo "Available space : $diskavail KB"
echo "Required size   : $REQUIRED_INSTALLATION_SIZE KB"

if (( $diskavail < $REQUIRED_INSTALLATION_SIZE )); then
    echo "Not enough disk space to install this software."
    exit 4
fi



%post
if [ -L /lib/ld-mfert.so.2 ]; then
    rm -f  /lib/ld-mfert.so.2
fi

if [ -f /etc/ld-mfert.so.conf ]; then 
    rm -f /etc/ld-mfert.so.conf
fi

if [ -f /etc/ld-mfert.so.cache ]; then 
    rm -f /etc/ld-mfert.so.cache
fi

echo "Writing conf file ..."
echo "/opt/mileaccess/runtime/RPMVERSION/lib" >> /etc/ld-mfert.so.conf

echo "Updating linker cache ..."
/opt/mileaccess/runtime/RPMVERSION/sbin/ldconfig  -C /etc/ld-mfert.so.cache  -f /etc/ld-mfert.so.conf  --format=old


ln -sf /opt/mileaccess/runtime/RPMVERSION/lib/ld-mfert.so.2 /lib/ld-mfert.so.2

#Removing old nails tools links
#if [ -f /lib/ld-nails.so.2 ]; then
#	rm -f /lib/ld-nails.so.2
#fi

#if [ -f /etc/ld-nails.so.conf ]; then
#	rm -f /etc/ld-nails.so.conf
#fi

#if [ -f /etc/ld-nails.so.cache ]; then
#	rm -f /etc/ld-nails.so.cache
#fi

#if [ -d /opt/mileaccess/runtime/1.0/ ] ; then
#	rm -rf /opt/mileaccess/runtime/1.0/
#fi


echo "Runtime installed successfully"


%preun

if [ "$1" == "0" ] ; then 

	if [ -L /lib/ld-mfert.so.2 ]; then
		rm -f  /lib/ld-mfert.so.2
	fi

	if [ -f /etc/ld-mfert.so.conf ]; then 
		rm -f /etc/ld-mfert.so.conf
	fi

	if [ -f /etc/ld-mfert.so.cache ]; then 
		rm -f /etc/ld-mfert.so.cache
	fi

	echo "Runtime uninstalled successfully"
fi


%postun


%files
%defattr(755,root,root)
%dir /opt/
%dir /opt/mileaccess/runtime/
%dir /opt/mileaccess/runtime/RPMVERSION/
%dir /opt/mileaccess/runtime/RPMVERSION/lib
%dir /opt/mileaccess/runtime/RPMVERSION/etc
%dir /opt/mileaccess/runtime/RPMVERSION/sbin
%attr(755,root,root) /opt/mileaccess/runtime/RPMVERSION/sbin/ldconfig
%attr(755,root,root) /opt/mileaccess/runtime/RPMVERSION/lib/ld-LIBCVERSION.so
%attr(755,root,root) /opt/mileaccess/runtime/RPMVERSION/lib/ld-mfert.so.2
%attr(755,root,root) /opt/mileaccess/runtime/RPMVERSION/lib/libBrokenLocale-LIBCVERSION.so
%attr(755,root,root) /opt/mileaccess/runtime/RPMVERSION/lib/libBrokenLocale.so.1
%attr(755,root,root) /opt/mileaccess/runtime/RPMVERSION/lib/libc-LIBCVERSION.so
%attr(755,root,root) /opt/mileaccess/runtime/RPMVERSION/lib/libcrypt-LIBCVERSION.so
%attr(755,root,root) /opt/mileaccess/runtime/RPMVERSION/lib/libcrypt.so.1
%attr(755,root,root) /opt/mileaccess/runtime/RPMVERSION/lib/libc.so.6
%attr(755,root,root) /opt/mileaccess/runtime/RPMVERSION/lib/libdl-LIBCVERSION.so
%attr(755,root,root) /opt/mileaccess/runtime/RPMVERSION/lib/libdl.so.2
%attr(755,root,root) /opt/mileaccess/runtime/RPMVERSION/lib/libgcc_s.so
%attr(644,root,root) /opt/mileaccess/runtime/RPMVERSION/lib/libgcc_s.so.1
%attr(755,root,root) /opt/mileaccess/runtime/RPMVERSION/lib/libm-LIBCVERSION.so
%attr(755,root,root) /opt/mileaccess/runtime/RPMVERSION/lib/libm.so.6
%attr(755,root,root) /opt/mileaccess/runtime/RPMVERSION/lib/libnsl-LIBCVERSION.so
%attr(755,root,root) /opt/mileaccess/runtime/RPMVERSION/lib/libnsl.so.1
%attr(755,root,root) /opt/mileaccess/runtime/RPMVERSION/lib/libnss_compat-LIBCVERSION.so
%attr(755,root,root) /opt/mileaccess/runtime/RPMVERSION/lib/libnss_compat.so.2
%attr(755,root,root) /opt/mileaccess/runtime/RPMVERSION/lib/libnss_dns-LIBCVERSION.so
%attr(755,root,root) /opt/mileaccess/runtime/RPMVERSION/lib/libnss_dns.so.2
%attr(755,root,root) /opt/mileaccess/runtime/RPMVERSION/lib/libnss_files-LIBCVERSION.so
%attr(755,root,root) /opt/mileaccess/runtime/RPMVERSION/lib/libnss_files.so.2
%attr(755,root,root) /opt/mileaccess/runtime/RPMVERSION/lib/libnss_hesiod-LIBCVERSION.so
%attr(755,root,root) /opt/mileaccess/runtime/RPMVERSION/lib/libnss_hesiod.so.2
%attr(755,root,root) /opt/mileaccess/runtime/RPMVERSION/lib/libnss_nis-LIBCVERSION.so
%attr(755,root,root) /opt/mileaccess/runtime/RPMVERSION/lib/libnss_nisplus-LIBCVERSION.so
%attr(755,root,root) /opt/mileaccess/runtime/RPMVERSION/lib/libnss_nisplus.so.2
%attr(755,root,root) /opt/mileaccess/runtime/RPMVERSION/lib/libnss_nis.so.2
%attr(755,root,root) /opt/mileaccess/runtime/RPMVERSION/lib/libresolv-LIBCVERSION.so
%attr(755,root,root) /opt/mileaccess/runtime/RPMVERSION/lib/libresolv.so.2
%attr(755,root,root) /opt/mileaccess/runtime/RPMVERSION/lib/libstdc++.so.6
%attr(755,root,root) /opt/mileaccess/runtime/RPMVERSION/lib/libstdc++.so.6.0.8
%attr(755,root,root) /opt/mileaccess/runtime/RPMVERSION/lib/libpthread.so.0
%attr(755,root,root) /opt/mileaccess/runtime/RPMVERSION/lib/libpthread-0.10.so
%attr(755,root,root) /opt/mileaccess/runtime/RPMVERSION/lib/librt-LIBCVERSION.so
%attr(755,root,root) /opt/mileaccess/runtime/RPMVERSION/lib/librt.so.1
%attr(755,root,root) /opt/mileaccess/runtime/RPMVERSION/lib/libnss_mdns4.so.2
%attr(755,root,root) /opt/mileaccess/runtime/RPMVERSION/lib/libnss_mdns6.so.2
%attr(755,root,root) /opt/mileaccess/runtime/RPMVERSION/lib/libnss_mdns6_minimal.so.2
%attr(755,root,root) /opt/mileaccess/runtime/RPMVERSION/lib/libnss_mdns.so.2
%attr(755,root,root) /opt/mileaccess/runtime/RPMVERSION/lib/libnss_mdns_minimal.so.2
%attr(755,root,root) /opt/mileaccess/runtime/RPMVERSION/lib/libnss_mdns4_minimal.so.2

