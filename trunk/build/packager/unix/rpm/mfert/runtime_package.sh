#!/bin/bash



Usage()
{
    echo "Usage: bash <CompletePath>/runtime_package.sh -v <version> -c <libc version > -p <package> -o <outloc-dir>" 
    echo "All  flags are to be specified. There are no default values."
    echo "   -v version i.e. 1.0"
    echo "   -c libc version i.e. 2.3.2"
    echo "   -p package-id number example 215"
    echo "   -o output location <Complete path from Root> /home/pramod/debug"
    exit 2
}

setDebianVariable()
{
	DEB_BUILD_ROOT=/tmp/MFErt-$version-buildroot-debian
	echo The current debian buildroot is $DEB_BUILD_ROOT

	if [ -d $DEB_BUILD_ROOT ]; then
	        rm -rf $DEB_BUILD_ROOT
	fi

	echo Creating the debian buildroot at $DEB_BUILD_ROOT
	mkdir -p $DEB_BUILD_ROOT

}

createDebianPackage()
{
	echo "Editing the debian control file for package Sizes"
	CONTROLNAME=$dir/runtime.control

	echo "using debian control file $CONTROLNAME"
	mkdir -p $DEB_BUILD_ROOT/DEBIAN

	str="s/RPMVERSION/$version/;s/LIBCVERSION/$libcversion/"

	echo $str
	sed -e "{$str}" $CONTROLNAME  > $DEB_BUILD_ROOT/DEBIAN/control
	sed -e "{$str}" $dir/runtime_postinst > $DEB_BUILD_ROOT/DEBIAN/postinst

	cp $dir/runtime_prerm $DEB_BUILD_ROOT/DEBIAN/prerm

	chmod 755 $DEB_BUILD_ROOT/DEBIAN/control
	chmod 755 $DEB_BUILD_ROOT/DEBIAN/postinst
	chmod 755 $DEB_BUILD_ROOT/DEBIAN/prerm

	dfout=`du -sk $DEB_BUILD_ROOT`
	pkgsize=`echo $dfout | cut -d' ' -f1`
	reqsize=$(( (($pkgsize + 1023 )/1024)*1024 ))
	echo "Package Size is $reqsize KB"

	str="s/^REQUIRED_INSTALLATION_SIZE.*/REQUIRED_INSTALLATION_SIZE=$reqsize/"
	sed -e "{$str}" $dir/runtime_preinst > $DEB_BUILD_ROOT/DEBIAN/preinst

	chmod 755 $DEB_BUILD_ROOT/DEBIAN/preinst

	echo "End editing the debian control file for package sizes"

	echo "Building the debian package..."

	if [ -f /usr/bin/dpkg-deb ] ; then
	        DEBCMD="/usr/bin/dpkg-deb -b"
	else
	        DEBCMD="/bin/dpkg-deb -b"
	fi

	echo $DEBCMD $DEB_BUILD_ROOT
	$DEBCMD $DEB_BUILD_ROOT
	echo "Finished creating debian package with status $?"

}

while getopts v:c:p:o: d
  do
  case $d in
      v)
	  version=$OPTARG 
	  ;;
      c)
	  libcversion=$OPTARG 
	  ;;
      p)
          package=$OPTARG 
          ;;
      o)
	  outdir=$OPTARG 
	  ;;
      \?) 
          Usage;;
  esac
done
shift `expr $OPTIND - 1` 

scriptdir=$0

firstchar=`echo $scriptdir | grep "^\/"`

if [ "$firstchar" == "" ]; then
    echo "The script path is not absolute path"
    Usage
fi

if [ -z "$version" ]; then 
    echo "Version not defined"
    Usage
fi

if [ -z "$libcversion" ]; then 
    echo "libc Version not defined"
    Usage
fi

if [ -z "$package" ]; then
    echo "Package not defined"
    Usage
fi

if [ -z "$outdir" ]; then 
    echo "Outdir not defined"
    Usage
fi

export PACKRPM_NO_REQ=1

dir=`echo $scriptdir | grep "runtime_package.sh" | sed -e  's/runtime_package.sh//g'`

#nailstoolsdir=/usr/local/mfert-tools/2.0/i686-NAI-linux
nailstoolsdir=/usr/local/mfert-tools/3.0/i686-NAI-linux

RPM_BUILD_ROOT=/tmp/MFErt-$version-buildroot
echo The current rpm buildroot is $RPM_BUILD_ROOT
    
if [ -d $RPM_BUILD_ROOT ]; then
	rm -rf $RPM_BUILD_ROOT
fi
    
echo Creating the rpm buildroot at $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT
    
# This will set variables required for debian.  
setDebianVariable

str="s/LIBCVERSION/$libcversion/"
sed -e "{$str}" $dir/runtime.manifest > /tmp/runtimemanifest ;

cd $nailstoolsdir
if (( $? != 0 )); then
     echo "packaging Failed. Exiting..."
     exit 1
fi

    
for BUILD_ROOT in $RPM_BUILD_ROOT $DEB_BUILD_ROOT
do
    
	mkdir -p $BUILD_ROOT/opt/mileaccess/runtime
	mkdir -p $BUILD_ROOT/opt/mileaccess/runtime/$version/lib
	mkdir -p $BUILD_ROOT/opt/mileaccess/runtime/$version/etc
	mkdir -p $BUILD_ROOT/opt/mileaccess/runtime/$version/sbin
    
for i in `cat /tmp/runtimemanifest`
do
		cp -d $i $BUILD_ROOT/opt/mileaccess/runtime/$version/$i
done
	#copy nss-mdn library for linux only
	cp $NSS_MDNS/lib/libnss_mdns4_minimal.so.2 $BUILD_ROOT/opt/mileaccess/runtime/$version/lib/
	if (( $? )); then echo "Copy Failed $NSS_MDNS/lib/libnss_mdns4_minimal.so.2"; fi

	cp $NSS_MDNS/lib/libnss_mdns4.so.2 $BUILD_ROOT/opt/mileaccess/runtime/$version/lib/
	if (( $? )); then echo "Copy Failed $NSS_MDNS/lib/libnss_mdns4.so.2"; fi

	cp $NSS_MDNS/lib/libnss_mdns6.so.2 $BUILD_ROOT/opt/mileaccess/runtime/$version/lib/
	if (( $? )); then echo "Copy Failed $NSS_MDNS/lib/libnss_mdns6.so.2"; fi

	cp $NSS_MDNS/lib/libnss_mdns6_minimal.so.2 $BUILD_ROOT/opt/mileaccess/runtime/$version/lib/
	if (( $? )); then echo "Copy Failed $NSS_MDNS/lib/libnss_mdns6_minimal.so.2"; fi

	cp $NSS_MDNS/lib/libnss_mdns.so.2 $BUILD_ROOT/opt/mileaccess/runtime/$version/lib/
	if (( $? )); then echo "Copy Failed $NSS_MDNS/lib/libnss_mdns.so.2"; fi

	cp $NSS_MDNS/lib/libnss_mdns_minimal.so.2 $BUILD_ROOT/opt/mileaccess/runtime/$version/lib/
	if (( $? )); then echo "Copy Failed $NSS_MDNS/lib/libnss_mdns_minimal.so.2"; fi

	strip $BUILD_ROOT/opt/mileaccess/runtime/$version/lib/*
done
    
rm /tmp/runtimemanifest
    
echo "Editing the rpm spec file for package Sizes"

SPECNAME=$dir/runtime.spec

    
echo "using rpm spec file $SPECNAME"
    
str="s/RPMVERSION/$version/;s/LIBCVERSION/$libcversion/"

echo $str
sed -e "{$str}" $SPECNAME  > /tmp/spec1
    
dfout=`du -sk $RPM_BUILD_ROOT`
pkgsize=`echo $dfout | cut -d' ' -f1`
reqsize=$(( (($pkgsize + 1023 )/1024)*1024 ))
echo "Package Size is $reqsize KB"
    
str="s/^REQUIRED_INSTALLATION_SIZE.*/REQUIRED_INSTALLATION_SIZE=$reqsize/"
sed -e "{$str}" /tmp/spec1 > /tmp/spec
    
echo "End editing the rpm spec for package sizes"

echo "Building the rpm package..."

if [ -f /usr/bin/rpmbuild ] ; then 
	RPMCMD="/usr/bin/rpmbuild -bb"
else
	RPMCMD="/bin/rpm -bb"
fi

mkdir ${RPM_BUILD_ROOT}_tmp
cp -rf ${RPM_BUILD_ROOT}/* ${RPM_BUILD_ROOT}_tmp
echo "Creating rpm package"
echo $RPMCMD --target i686 /tmp/spec
cd
$RPMCMD --buildroot "$RPM_BUILD_ROOT" --target i686 /tmp/spec 

#echo "Creating debian package"
#This will create debian package
createDebianPackage
    
    
mkdir -p $outdir
    
if (( $? )); then
	echo "Unable to create target directory for the rpm."
fi

if [ -f /usr/src/packages/RPMS/i686/MFErt-$version-1.i686.rpm ]; then
	mv /usr/src/packages/RPMS/i686/MFErt-$version-1.i686.rpm $outdir/MFErt.$version-1.i686.rpm
else
	mv ~/rpmbuild/RPMS/i686/MFErt-$version-1.i686.rpm $outdir/MFErt.$version-1.i686.rpm
fi
if (( $? )); then
        echo "Unable to copy MFErt.i686.rpm to $outdir "
        exit 5
fi

#Move debian package to output directory
mv /tmp/MFErt-$version-buildroot-debian.deb $outdir/MFErt.$version-1.i686.deb
if (( $? )); then
	echo "Unable to copy MFErt.i686.deb to $outdir "
	exit 5
fi

if [ -d $RPM_BUILD_ROOT ]; then
	rm -rf $RPM_BUILD_ROOT
fi
if [ -d ${RPM_BUILD_ROOT}_tmp ]; then
	rm -rf ${RPM_BUILD_ROOT}_tmp
fi
if [ -d $DEB_BUILD_ROOT ]; then
	rm -rf $DEB_BUILD_ROOT
fi
if [ -e /tmp/spec ]; then
	rm -rf /tmp/spec
fi
if [ -e /tmp/spec1 ]; then
	rm -rf /tmp/spec1
fi
