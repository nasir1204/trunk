#!/bin/bash

#########################################
# Don't invoke this script directly, it must be invoked via ma_packager.sh
#########################################

HasCRLFHenceExit()
{
    error=0
    for i in `find $1`
    do
		file $i | cut -d: -f2 | grep -e "\([Ss]cript\)\|\(ASCII..*text\)" >/dev/null
      	if (( $? == 0 )); then
	  	str1=`strings -1 $i | head -1`
	  	str2=`cat $i | head -1`
	  	if [ "$str1" != "$str2" ]; then
	    	echo $i 
	    	error=1
	  	fi
      	fi
    done
    if (( $error != 0 )); then
	echo "Have CRLF in Text file $i" 
	file $i
	#Sdas to fix
	#exit 4;
    fi
}
parseFile()
{
	#parseFile "/build/packager/unix" "ma_rpm_spec1.spec" "/ma50/out.tmp" 
	FILE_NAME="$2"
	SCRIPT_PATH="$1/scripts"
	OUT_FILE_NAME=$3
	rm -rf $OUT_FILE_NAME
	touch $OUT_FILE_NAME	
	cat $FILE_NAME | while read LINE
	do
		if [ "$(echo $LINE | grep "^__REPLACE_PREPINSTALL_SCRIPT__")"  ]; then
			cat $SCRIPT_PATH/ma_prepinstall  | sed 's/\\\\/\\/g;s/\\&/\&/g' >>$OUT_FILE_NAME
		elif [ "$(echo $LINE | grep "^__REPLACE_INSTALL_SCRIPT__")"  ]; then
			cat $SCRIPT_PATH/ma_install  | sed 's/\\\\/\\/g;s/\\&/\&/g' >>$OUT_FILE_NAME
		elif [ "$(echo $LINE | grep "^__REPLACE_PREINSTALL_SCRIPT__")"  ]; then
			cat $SCRIPT_PATH/ma_preinstall  | sed 's/\\\\/\\/g;s/\\&/\&/g' >>$OUT_FILE_NAME
		elif [ "$(echo $LINE | grep "^__REPLACE_POSINSTALL_SCRIPT__")"  ]; then
			cat $SCRIPT_PATH/ma_postinstall  | sed 's/\\\\/\\/g;s/\\&/\&/g' >>$OUT_FILE_NAME
		elif [ "$(echo $LINE | grep "^__REPLACE_PREUNINSTALL_SCRIPT__")"  ]; then
			cat $SCRIPT_PATH/ma_preuninstall  | sed 's/\\\\/\\/g;s/\\&/\&/g' >>$OUT_FILE_NAME
		elif [ "$(echo $LINE | grep "^__REPLACE_POSTUNINSTALL_SCRIPT__")"  ]; then
			cat $SCRIPT_PATH/ma_postuninstall  | sed 's/\\\\/\\/g;s/\\&/\&/g' >>$OUT_FILE_NAME
		else
			echo $LINE >>$OUT_FILE_NAME
		fi
	done
}
setDebianVariableIfRequired()
{
	if [ "$PLATFORM" == "Linux" ]; then
		DEBIAN_BUILD_ROOT=/tmp/MFEma-$MA_VERSION-$PACKAGE_NUM-buildroot-debian
		echo "The current debian buildroot is $DEBIAN_BUILD_ROOT"
		if [ -d $DEBIAN_BUILD_ROOT ]; then
			rm -rf $DEBIAN_BUILD_ROOT
		fi
		echo "Creating the debian buildroot at $DEBIAN_BUILD_ROOT"
		mkdir -p $DEBIAN_BUILD_ROOT
		else
		echo "Platform is not Linux so setting DEBIAN_BUILD_ROOT to blank"
		DEBIAN_BUILD_ROOT=""
	fi
}

createDebianPackageIfRequired()
{
	if [ "$PLATFORM" == "Linux" ]; then
		echo "Creating debian package"
		echo "Editing the debian control file for package Sizes"
		CONTROLNAME=$RPM_CURRENT_DIR/dpkg_install_control
		mkdir -p $DEBIAN_BUILD_ROOT/DEBIAN
		cp -rf $RPM_BUILD_ROOT/* $DEBIAN_BUILD_ROOT
		echo "using debian control file $CONTROLNAME"
		str="s:__MA_VERSION__:$MA_VERSION:g;s:__PACKAGE_NUMBER__:$PACKAGE_NUM:g"
		echo $str
		sed -e "{$str}" $CONTROLNAME > $DEBIAN_BUILD_ROOT/DEBIAN/control

		#Editing init script file
		echo "Editing init script file"
		str="s:# Required-Start\: network:# Required-Start\: \$network:g;s:# Required-Stop\: network:# Required-Stop\: \$network:g;"
		sed -e "$str" $DEBIAN_BUILD_ROOT/$MA_INSTALL_PATH/scripts/ma > $DEBIAN_BUILD_ROOT/$MA_INSTALL_PATH/scripts/ma_tmp
		rm -rf $DEBIAN_BUILD_ROOT/$MA_INSTALL_PATH/scripts/ma
		rm -rf $DEBIAN_BUILD_ROOT/etc/init.d/ma
		mv $DEBIAN_BUILD_ROOT/$MA_INSTALL_PATH/scripts/ma_tmp $DEBIAN_BUILD_ROOT/$MA_INSTALL_PATH/scripts/ma
		cp $DEBIAN_BUILD_ROOT/$MA_INSTALL_PATH/scripts/ma $DEBIAN_BUILD_ROOT/etc/init.d/ma
	
		# Update placeholders in config.xml
		MA_START_CMD="/etc/init.d/ma start"
		MA_STOP_CMD="/etc/init.d/ma stop"
		if [ x"$MA_DEPS" == x ]
		then
			MA_UNINSTALL_CMD="dpkg --remove MFEcma"
		else
			MA_UNINSTALL_CMD="dpkg --remove MFEcma;dpkg --remove MFErt"
		fi	
		str="s:__MA_UPDATER_PATH__:$MA_UPDATER_PATH:g;s:__MA_SOFTWARE_ID__:$MA_SOFTWAREID:g;s:__MA_VERSION__:$MA_VERSION:g;s:__PACKAGE_NUMBER__:$PACKAGE_NUM:g;s:__MA_DATA_PATH__:$MA_DATA_PATH:g;s:__MA_INSTALL_PATH__:$MA_INSTALL_PATH:g;s:__MA_START_CMD__:$MA_START_CMD:g;s:__MA_STOP_CMD__:$MA_STOP_CMD:g;s:__MA_UNINSTALL_CMD__:$MA_UNINSTALL_CMD:g;s:__MA_SHARED_LIB_PATH__:$MA_CMA_LPC_LIBS_PATH:g"
		sed -e "{$str}" $RPM_CURRENT_DIR/../data/config.xml > $DEBIAN_BUILD_ROOT/$MA_CONFIG_PATH/$MA_SOFTWAREID/config.xml

		str="s:__MA_VERSION__:$MA_VERSION:g;s:__PACKAGE_NUMBER__:$PACKAGE_NUM:g;s:__MA_INSTALL_PATH__:$MA_INSTALL_PATH:g;s:__MA_DATA_PATH__:$MA_DATA_PATH:g"
		sed -e "{$str}" $RPM_CURRENT_DIR/../data/mainfo.ini > $DEBIAN_BUILD_ROOT/$MA_CONFIG_PATH/mainfo.ini
		cp $RPM_CURRENT_DIR/../data/msaconfig $DEBIAN_BUILD_ROOT/$MA_INSTALL_PATH/bin/
		dfout=`du -sk $DEBIAN_BUILD_ROOT`
		pkgsize=`echo $dfout | cut -d' ' -f1`
		reqsize=$(( (($pkgsize + 1023 )/1024)*1024 ))
		echo "Package Size is $reqsize KB"
		
		#pre and post install script replace text
		OPT_FLAG="-f"
		DF_FLAG1="-l"	
		DF_FLAG2="-f4"
		GREP_CMD="grep 'EPOAGENT\|CMNUPD\|CMDAGENT'"
		str=""
		str="s:__OPT_FLAG__:$OPT_FLAG:g;s:__DF_FLAG1__:$DF_FLAG1:g;s:__DF_FLAG2__:$DF_FLAG2:g;s:__GREP_CMD__:$GREP_CMD:g;s:__PLATFORM__:Linux:g;s:__STARTUP_SCRIPT__:/etc/init.d/ma:g;"
		str="s:^REQUIRED_INSTALLATION_SIZE.*:REQUIRED_INSTALLATION_SIZE=$reqsize:;s:__DEPS__:$MA_DEPS:g;s:__MA_VERSION__:$MA_VERSION:g;s:__PACKAGE_NUMBER__:$PACKAGE_NUM:g;s:__MA_SOFTWARE_ID__:$MA_SOFTWAREID:g;s:__MA_DATA_PATH__:$MA_DATA_PATH:g;s:__MA_MSGBUS_DATA_PATH__:$MA_MSGBUS_DATA_PATH:g;s:__MA_INSTALL_PATH__:$MA_INSTALL_PATH:g;s:__CMA_CONFIG_PATH__:$CMA_CONFIG_PATH:g;s:__CMA_SOFTWAREID__:$CMA_SOFTWAREID:g;s:__MA_CMN_UPDATERID__:$MA_CMN_UPDATERID:g;s:__MA_CONFIG_PATH__:$MA_CONFIG_PATH:g;s:__MA_CMA_LPC_LIBS_PATH__:$MA_CMA_LPC_LIBS_PATH:g;s:__LIB_EXTENSION__:$LIB_EXTENSION:g;s:__CMA_INSTALL_PATH__:$CMA_INSTALL_PATH:g;s:__CMA_SHARED_PATH__:$CMA_SHARED_PATH:g;$str"

		SCRIPT_PATH=$RPM_CURRENT_DIR/../scripts
		awk -v var="`cat $SCRIPT_PATH/ma_preinstall`" '{ gsub(/__REPLACE_DEBIAN_SCRIPT__/,var,$0); print  }' $SCRIPT_PATH/ma_debian_script > /tmp/ma_debian_script
		sed -e "{$str}" /tmp/ma_debian_script > $DEBIAN_BUILD_ROOT/DEBIAN/preinst
		rm -rf /tmp/ma_debian_script

		awk -v var="`cat $SCRIPT_PATH/ma_postinstall`" '{ gsub(/__REPLACE_DEBIAN_SCRIPT__/,var,$0); print  }' $SCRIPT_PATH/ma_debian_script > /tmp/ma_debian_script
		sed -e "{$str}" /tmp/ma_debian_script > /tmp/ma_debian_script_postscript		
		awk -v var="`cat /tmp/ma_dpkg_spec1`" '{ gsub(/__DEBIAN_SET_FILE_DIR_PERMISSION__/,var,$0); print  }' /tmp/ma_debian_script_postscript > $DEBIAN_BUILD_ROOT/DEBIAN/postinst
		rm -rf /tmp/ma_debian_script
		rm -rf /tmp/ma_debian_script_postscript

		awk -v var="`cat $SCRIPT_PATH/ma_preuninstall`" '{ gsub(/__REPLACE_DEBIAN_SCRIPT__/,var,$0); print  }' $SCRIPT_PATH/ma_debian_script > /tmp/ma_debian_script
		sed -e "{$str}" /tmp/ma_debian_script > $DEBIAN_BUILD_ROOT/DEBIAN/prerm
		rm -rf /tmp/ma_debian_script

		awk -v var="`cat $SCRIPT_PATH/ma_postuninstall`" '{ gsub(/__REPLACE_DEBIAN_SCRIPT__/,var,$0); print  }' $SCRIPT_PATH/ma_debian_script > /tmp/ma_debian_script
		sed -e "{$str}" /tmp/ma_debian_script > $DEBIAN_BUILD_ROOT/DEBIAN/postrm
		rm -rf /tmp/ma_debian_script

		chmod 755 $DEBIAN_BUILD_ROOT/DEBIAN/preinst
		chmod 755 $DEBIAN_BUILD_ROOT/DEBIAN/postinst
		chmod 755 $DEBIAN_BUILD_ROOT/DEBIAN/prerm
		chmod 755 $DEBIAN_BUILD_ROOT/DEBIAN/postrm
		chmod 755 $DEBIAN_BUILD_ROOT/DEBIAN/control

		echo "End editing the debian control file for package sizes"
		echo "Building the debian package..."
		cp $DEBIAN_BUILD_ROOT/DEBIAN/control /tmp/control.bak

		if [ -f /usr/bin/dpkg-deb ] ; then
				DEBCMD="/usr/bin/dpkg-deb -b"
		else
				DEBCMD="/bin/dpkg-deb -b"
		fi
		echo $DEBCMD $DEBIAN_BUILD_ROOT
		$DEBCMD $DEBIAN_BUILD_ROOT
		echo "Finished debian package creation with status $?"
	else
	   echo "Platform is not Linux so ignoring debian package creation"
	fi
	return 0
}

################################################################
#Step 1: Creating directory and defines
################################################################
echo $'\n\n************* Step 1: Creating directory and defines'

RPM_CURRENT_DIR=`echo $0 | grep ".rpm_packager.sh" | sed -e 's/.rpm_packager.sh//g'`
echo "RPM_CURRENT_DIR =" $RPM_CURRENT_DIR
OUTPUT_DIR=$OUTPUT_DIR/$ARCH_BIT/$CONFIG_TYPE
echo Output dir is $OUTPUT_DIR
if [ ! -d $OUTPUT_DIR ]; then
    echo $OUTPUT_DIR directory doesnt exist, creating
    mkdir -p $OUTPUT_DIR
fi
#### constants to be used in rpm spec #####
RPM_DEFAULT_PERMISSIONS="%defattr(755,root,root)"
RPM_FILE_PERMISSIONS="(755, root, root)"
RPM_FILE_PERMISSIONS_2="(744, root, root)"
RPM_FILE_MFE_PERMISSIONS="(744, mfe, mfe)"
RPM_SIG_FILE_PERMISSIONS="(400, root, root)"
RPM_DIR_CONST="%dir"
RPM_FILE_CONST="%attr"
####
SCRIPT_DIR=$PLATFORM
RPM_SPEC_NAME=$RPM_CURRENT_DIR/rpm_install.spec
echo "Using rpm spec file $RPM_SPEC_NAME"
cp $RPM_SPEC_NAME /tmp/ma_rpm_spec1
if [ x"$ARCH_BIT" == x"32" ] ;then
	SUFFIX_DIR=$CONFIG_TYPE
	MA_DEPS=MFErt
	TARGET=i686
else
	# Note: Not adding dependency on MFErt for all 64 bit builds as it is not available including MLOS
	SUFFIX_DIR=$CONFIG_TYPE/$ARCH_BIT
	MA_DEPS=
	TARGET=x86_64
fi
if [ x"$PLATFORM" == x"AIX" ] ;then
	MA_DEPS=
	SCRIPT_DIR="Linux"
	TARGET=ppc
fi

# MA binaries Path
MA_TARGETS_DIR=$MA_BASE/build/makefiles/$SUFFIX_DIR/
echo MA binaries Path $MA_TARGETS_DIR
if [ ! -e $MA_TARGETS_DIR ]; then
	echo MA targets dir $MA_TARGETS_DIR not found
	exit 1
fi

MA_SIG_FILE=$MA_BASE/build/packager/data/ma_msgbus_auth.sig
echo Sig file path is $MA_SIG_FILE
if [ -f $MA_SIG_FILE ]; then
	echo ma_msgbus_auth.sig file found
else
	echo $MA_SIG_FILE not found, Warning continuing without it by touching it
	touch $MA_SIG_FILE
fi

echo copying $MA_SIG_FILE to $MA_TARGETS_DIR for packaging 
cp $MA_SIG_FILE $MA_TARGETS_DIR

# MA Tools binaries Path  
MA_TOOLS_TARGETS_DIR=$MA_TOOLS
echo MA tools binaries Path $MA_TOOLS_TARGETS_DIR
if [ ! -e $MA_TOOLS_TARGETS_DIR ]; then
	echo MA Tools targets dir $MA_TOOLS_TARGETS_DIR not found
	exit 1
fi
# MA compat sdk binaries Path  
MA_COMPAT_SDK_TARGETS_DIR=$MA_COMPAT_SDK
echo MA compat sdk binaries Path $MA_COMPAT_SDK_TARGETS_DIR
if [ ! -e $MA_COMPAT_SDK_TARGETS_DIR ]; then
	echo MA compat sdk targets dir $MA_COMPAT_SDK_TARGETS_DIR not found
	exit 1
fi

if [ x"$PLATFORM" == x"Linux" ] && [ -f "/etc/redhat-release" ]; then
	# it is mileaccess for MLOS...
	LINUX_DIST=`cat /etc/redhat-release | cut -d" " -f1`
fi

# RPM build root path
RPM_BUILD_ROOT=/tmp/MFEma-$MA_VERSION-$PACKAGE_NUM-buildroot
echo The current rpm buildroot is $RPM_BUILD_ROOT
if [ -d $RPM_BUILD_ROOT ]; then
	rm -rf $RPM_BUILD_ROOT
fi    
echo Creating the rpm build root at $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT

#This will set debian variables if platform is linux 
if [ x"mileaccess" != x"$LINUX_DIST" ]; then
	setDebianVariableIfRequired
fi
# The below env file contains all the required environment variables.
source $RPM_CURRENT_DIR/../ma_packager.env
echo $RPM_DEFAULT_PERMISSIONS >> /tmp/ma_rpm_spec1


################################################################
#Step 2: Creating install files and directory in buildroot
################################################################

echo $'\n\n************* Step 2: Creating install files and directory in buildroot'

# Creating install, data and config dirs in build root
if [ x"mileaccess" == x"$LINUX_DIST" ]; then
	DIRS_LIST=`cat $RPM_CURRENT_DIR/../ma_install_files | grep dir: | cut -d: -f2`
else
	DIRS_LIST=`cat $RPM_CURRENT_DIR/../ma_install_files | egrep  'dir:|dir_compat:' | cut -d: -f2`
fi
for dir in `eval echo $DIRS_LIST`
do
	echo Creating $RPM_BUILD_ROOT/$dir
	mkdir -p $RPM_BUILD_ROOT/$dir
	echo $RPM_DIR_CONST $dir >> /tmp/ma_rpm_spec1
done

# Copying data files into build root
FILES_LIST=`cat $RPM_CURRENT_DIR/../ma_install_files | grep data: | cut -d: -f2`
for file in `eval echo $FILES_LIST` 
do
	echo Copying $RPM_CURRENT_DIR/../../data/$file $RPM_BUILD_ROOT/$MA_DATA_PATH/data/$file
	cp $RPM_CURRENT_DIR/../../data/$file $RPM_BUILD_ROOT/$MA_DATA_PATH/data/$file
	echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS $MA_DATA_PATH/data/$file >> /tmp/ma_rpm_spec1
done

# Copying certs files into build root
FILES_LIST=`cat $RPM_CURRENT_DIR/../ma_install_files | grep certs: | cut -d: -f2`
for file in `eval echo $FILES_LIST` 
do
	echo Copying $MA_BASE/build/packager/data/certs/public/$file $RPM_BUILD_ROOT/$MA_DATA_PATH/certstore/$file
	cp $MA_BASE/build/packager/data/certs/public/$file $RPM_BUILD_ROOT/$MA_DATA_PATH/certstore/$file
	echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS $MA_DATA_PATH/certstore/$file >> /tmp/ma_rpm_spec1
done

# Copying config files into build root
FILES_LIST=`cat $RPM_CURRENT_DIR/../ma_install_files | grep config: | cut -d: -f2`
for file in `eval echo $FILES_LIST` 
do
	echo Copying $RPM_CURRENT_DIR/../data/$file $RPM_BUILD_ROOT/$MA_CONFIG_PATH/$MA_SOFTWAREID/$file
	cp $RPM_CURRENT_DIR/../data/$file $RPM_BUILD_ROOT/$MA_CONFIG_PATH/$MA_SOFTWAREID/$file
	echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS $MA_CONFIG_PATH/$MA_SOFTWAREID/$file >> /tmp/ma_rpm_spec1
done


if [ -e $RPM_CURRENT_DIR/../data/cmnupd_config.xml ]; then
	echo Copying $RPM_CURRENT_DIR/../data/cmnupd_config.xml $RPM_BUILD_ROOT/$MA_CONFIG_PATH/$MA_CMN_UPDATERID/config.xml
	cp $RPM_CURRENT_DIR/../data/cmnupd_config.xml $RPM_BUILD_ROOT/$MA_CONFIG_PATH/$MA_CMN_UPDATERID/config.xml
	echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS $MA_CONFIG_PATH/$MA_CMN_UPDATERID/config.xml >> /tmp/ma_rpm_spec1
fi

# Copying config files into build root
SCRIPT_LIST=`cat $RPM_CURRENT_DIR/../ma_install_files | grep script: | cut -d: -f2`
if [ x"mileaccess" == x"$LINUX_DIST" ]; then
	IS_COMPAT_SVC=0
else
	IS_COMPAT_SVC=1
fi
if [ "$PLATFORM" = "Linux" ]; then
	RUNTIME_LIBRARY_FLAG="LD_LIBRARY_PATH"
elif [ "$PLATFORM" = "AIX" ]; then
	RUNTIME_LIBRARY_FLAG="LIBPATH"
fi
for file in `eval echo $SCRIPT_LIST` 
do
	echo Copying $RPM_CURRENT_DIR/../scripts/$SCRIPT_DIR/$file $RPM_BUILD_ROOT/$$MA_INSTALL_PATH/scripts/$file
	str="s:__RUNTIME_LIBRARY_FLAG__:$RUNTIME_LIBRARY_FLAG:g;s:__PLATFORM__:$PLATFORM:g;s:__MA_SOFTWARE_ID__:$CMA_SOFTWAREID:g;s:__IS_COMPAT_SVC__:$IS_COMPAT_SVC:g;s:__MA_VERSION__:$MA_VERSION:g;s:__PACKAGE_NUMBER__:$PACKAGE_NUM:g;s:__MA_INSTALL_PATH__:$MA_INSTALL_PATH:g"
	sed -e "$str" $RPM_CURRENT_DIR/../scripts/$SCRIPT_DIR/$file > $RPM_BUILD_ROOT/$MA_INSTALL_PATH/scripts/$file	
	echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS $MA_INSTALL_PATH/scripts/$file >> /tmp/ma_rpm_spec1
done
# Copying all ma install lib files into build root
if [ x"mileaccess" == x"$LINUX_DIST" ]; then
	LIBS_LIST=`cat $RPM_CURRENT_DIR/../ma_install_files | egrep  'lib:' | cut -d: -f2`
else
	LIBS_LIST=`cat $RPM_CURRENT_DIR/../ma_install_files | egrep  'lib:|lib_compat:' | cut -d: -f2`
fi
for file in `eval echo $LIBS_LIST` 
do
	MAJ_VER=`echo $MA_VERSION | cut -d. -f1`
	MIN_VER=`echo $MA_VERSION | cut -d. -f2`
	if [ -e $MA_TARGETS_DIR/$file.$MAJ_VER.$MIN_VER ]; then
		echo Copying file  $MA_TARGETS_DIR/$file.$MAJ_VER.$MIN_VER to $RPM_BUILD_ROOT/$MA_INSTALL_PATH/lib/$file.$MAJ_VER.$MIN_VER
		cp $MA_TARGETS_DIR/$file.$MAJ_VER.$MIN_VER $RPM_BUILD_ROOT/$MA_INSTALL_PATH/lib/$file.$MAJ_VER.$MIN_VER
		echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS $MA_INSTALL_PATH/lib/$file.$MAJ_VER.$MIN_VER >> /tmp/ma_rpm_spec1
		echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS $MA_INSTALL_PATH/lib/$file.$MAJ_VER >> /tmp/ma_rpm_spec1
		echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS $MA_INSTALL_PATH/lib/$file >> /tmp/ma_rpm_spec1
		cd $RPM_BUILD_ROOT/$MA_INSTALL_PATH/lib/
		ln -sf $file.$MAJ_VER.$MIN_VER $file.$MAJ_VER
		ln -sf $file.$MAJ_VER $file
		cd $RPM_CURRENT_DIR
	fi
done

# Creating links in rsdk
RSDK_LIST=`cat $RPM_CURRENT_DIR/../ma_install_files | grep rsdk: | cut -d: -f2`
for file in `eval echo $RSDK_LIST` 
do
	MAJ_VER=`echo $MA_VERSION | cut -d. -f1`
	MIN_VER=`echo $MA_VERSION | cut -d. -f2`
	if [ -e $MA_TARGETS_DIR/$file.$MAJ_VER.$MIN_VER ]; then
		echo Copying file  $MA_TARGETS_DIR/$file.$MAJ_VER.$MIN_VER to $RPM_BUILD_ROOT/$MA_INSTALL_PATH/lib/rsdk/$file.$MAJ_VER.$MIN_VER
		cp $MA_TARGETS_DIR/$file.$MAJ_VER.$MIN_VER $RPM_BUILD_ROOT/$MA_INSTALL_PATH/lib/rsdk/$file.$MAJ_VER.$MIN_VER
		echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS $MA_INSTALL_PATH/lib/rsdk/$file.$MAJ_VER.$MIN_VER >> /tmp/ma_rpm_spec1
		echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS $MA_INSTALL_PATH/lib/rsdk/$file.$MAJ_VER >> /tmp/ma_rpm_spec1
		echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS $MA_INSTALL_PATH/lib/rsdk/$file >> /tmp/ma_rpm_spec1
		cd $RPM_BUILD_ROOT/$MA_INSTALL_PATH/lib/rsdk
		ln -sf $file.$MAJ_VER.$MIN_VER $file.$MAJ_VER
		ln -sf $file.$MAJ_VER $file
		cd $RPM_CURRENT_DIR
	fi
done


# Copying all ma install bin files into build root
if [ x"mileaccess" == x"$LINUX_DIST" ]; then
	BINS_LIST=`cat $RPM_CURRENT_DIR/../ma_install_files | egrep  'bin:' | cut -d: -f2`
else
	BINS_LIST=`cat $RPM_CURRENT_DIR/../ma_install_files | egrep  'bin:|bin_compat:' | cut -d: -f2`
fi
for file in `eval echo $BINS_LIST` 
do
	if [ -e $MA_TARGETS_DIR/$file ]; then
		echo Copying file $MA_TARGETS_DIR/$file to $RPM_BUILD_ROOT/$MA_INSTALL_PATH/bin/$file
		cp $MA_TARGETS_DIR/$file $RPM_BUILD_ROOT/$MA_INSTALL_PATH/bin/$file
		if [ $file == "cmdagent" ] || [ $file == "msaconfig" ]; then
			echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS $MA_INSTALL_PATH/bin/$file >> /tmp/ma_rpm_spec1
		elif [ $file == "macmnsvc" ]; then
			echo $RPM_FILE_CONST $RPM_FILE_MFE_PERMISSIONS $MA_INSTALL_PATH/bin/$file >> /tmp/ma_rpm_spec1
		else
			echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS_2 $MA_INSTALL_PATH/bin/$file >> /tmp/ma_rpm_spec1
		fi		
	fi
done


#Fix for LinuxShield Update libstdc++ mismatch issue. Engine dll requires stdc++ 5 in order to get loaded into Mue
if [ x"$PLATFORM" == x"Linux" ] && [ x"$ARCH_BIT" == x"32" ]; then
	cp $MA_BASE/build/packager/unix/data/libstdc++.so.5.0.4 $RPM_BUILD_ROOT/$MA_INSTALL_PATH/lib/tools
	echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS $MA_INSTALL_PATH/lib/tools/libstdc++.so.5.0.4 >> /tmp/ma_rpm_spec1
	echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS $MA_INSTALL_PATH/lib/tools/libstdc++.so.5 >> /tmp/ma_rpm_spec1
	cd $RPM_BUILD_ROOT/$MA_INSTALL_PATH/lib/tools
	ln -s libstdc++.so.5.0.4 libstdc++.so.5
fi
##########################
#Copying lpc64 bit library
if [ x"$PLATFORM" == x"Linux" ] && [ x"$ARCH_BIT" == x"32" ]; then
	MFE_LPC_FILE=$MASDK5_LNX64/$CONFIG_TYPE/libmfelpc.so.5.0
	if [ ! -f $MFE_LPC_FILE ]; then
		echo MA 64 bit mfelpc library file $MFE_LPC_FILE not found
		exit 1
	fi
	mkdir -p $RPM_BUILD_ROOT/$CMA_SHARED_PATH/5.0.0/lib/lib64/
	echo $RPM_DIR_CONST $CMA_SHARED_PATH/5.0.0/lib/lib64/ >> /tmp/ma_rpm_spec1
	echo Copying 64 bit mfelpc file $MFE_LPC_FILE
	cp $MFE_LPC_FILE $RPM_BUILD_ROOT/$CMA_SHARED_PATH/5.0.0/lib/lib64/
	echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS $CMA_SHARED_PATH/5.0.0/lib/lib64/libmfelpc.so.5.0 >> /tmp/ma_rpm_spec1
	echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS $CMA_SHARED_PATH/5.0.0/lib/lib64/libmfelpc.so.5 >> /tmp/ma_rpm_spec1
	echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS $CMA_SHARED_PATH/5.0.0/lib/lib64/libmfelpc.so >> /tmp/ma_rpm_spec1
	cd $RPM_BUILD_ROOT/$CMA_SHARED_PATH/5.0.0/lib/lib64/
	ln -s libmfelpc.so.5.0 libmfelpc.so.5
	ln -s libmfelpc.so.5 libmfelpc.so
fi
#########################

# Copying mue resource file
if [ -e $MA_TARGETS_DIR/MueRes_0409.cat ]; then
	if [ x"mileaccess" == x"$LINUX_DIST" ]; then
		echo Copying file $MA_TARGETS_DIR/MueRes_0409.cat to $RPM_BUILD_ROOT/$MA_DATA_PATH/data/0409/MueRes_InUse.cat
		mkdir -p $RPM_BUILD_ROOT/$MA_DATA_PATH/data/0409/
		echo $RPM_DIR_CONST $MA_DATA_PATH/data/0409/ >> /tmp/ma_rpm_spec1
		cp $MA_TARGETS_DIR/MueRes_0409.cat $RPM_BUILD_ROOT/$MA_DATA_PATH/data/0409/MueRes_InUse.cat
		echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS $MA_DATA_PATH/data/0409/MueRes_InUse.cat >> /tmp/ma_rpm_spec1	
	else
		echo Creating $RPM_BUILD_ROOT/$MA_INSTALL_PATH/lib/0409
		mkdir -p $RPM_BUILD_ROOT/$MA_INSTALL_PATH/lib/0409
		echo $RPM_DIR_CONST $MA_INSTALL_PATH/lib/0409 >> /tmp/ma_rpm_spec1	
		echo Copying file $MA_TARGETS_DIR/MueRes_0409.cat to $RPM_BUILD_ROOT/$MA_INSTALL_PATH/lib/0409/MueRes_InUse.cat
		cp $MA_TARGETS_DIR/MueRes_0409.cat $RPM_BUILD_ROOT/$MA_INSTALL_PATH/lib/0409/MueRes_InUse.cat
		echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS $MA_INSTALL_PATH/lib/0409/MueRes_InUse.cat >> /tmp/ma_rpm_spec1	
	fi
fi


if [ -z "$ENCRYPT_DIR" ]; then 
echo "Already encrypted mcs sripts from source tree"
# Copying update script file
	if [ -e $MA_BASE/src/compat/mue/common/mue/scripts/MSA/encrypted/UpdateMain.McS ]; then
		echo Copying file $MA_BASE/src/compat/mue/common/mue/scripts/MSA/encrypted/UpdateMain.McS to $RPM_BUILD_ROOT/$MA_DATA_PATH/update/UpdateMain.McS
		cp $MA_BASE/src/compat/mue/common/mue/scripts/MSA/encrypted/UpdateMain.McS $RPM_BUILD_ROOT/$MA_DATA_PATH/update/UpdateMain.McS
		echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS $MA_DATA_PATH/update/UpdateMain.McS >> /tmp/ma_rpm_spec1
	fi

	# Copying Install script file
	if [ -e $MA_BASE/src/compat/mue/common/mue/scripts/MSA/encrypted/InstallMain.McS ]; then
		echo Copying file $MA_BASE/src/compat/mue/common/mue/scripts/MSA/encrypted/InstallMain.McS to $RPM_BUILD_ROOT/$MA_DATA_PATH/update/InstallMain.McS
		cp $MA_BASE/src/compat/mue/common/mue/scripts/MSA/encrypted/InstallMain.McS $RPM_BUILD_ROOT/$MA_DATA_PATH/update/InstallMain.McS
		echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS $MA_DATA_PATH/update/InstallMain.McS >> /tmp/ma_rpm_spec1
	fi
else
	echo "Encrypted mcs sripts from build room"
	# Copying update script file
	if [ -e $ENCRYPT_DIR/UpdateMain.McS ]; then
		echo Copying file $ENCRYPT_DIR/UpdateMain.McS to $RPM_BUILD_ROOT/$MA_DATA_PATH/update/UpdateMain.McS
		cp $ENCRYPT_DIR/UpdateMain.McS $RPM_BUILD_ROOT/$MA_DATA_PATH/update/UpdateMain.McS
		echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS $MA_DATA_PATH/update/UpdateMain.McS >> /tmp/ma_rpm_spec1
	fi

	# Copying Install script file
	if [ -e $ENCRYPT_DIR/InstallMain.McS ]; then
		echo Copying file $ENCRYPT_DIR/InstallMain.McS to $RPM_BUILD_ROOT/$MA_DATA_PATH/update/InstallMain.McS
		cp $ENCRYPT_DIR/InstallMain.McS $RPM_BUILD_ROOT/$MA_DATA_PATH/update/InstallMain.McS
		echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS $MA_DATA_PATH/update/InstallMain.McS >> /tmp/ma_rpm_spec1
	fi	
fi

# Copying all ma install sig files into build root
SIGS_LIST=`cat $RPM_CURRENT_DIR/../ma_install_files | grep sig: | cut -d: -f2`
for file in `eval echo $SIGS_LIST` 
do
	if [ -e $MA_TARGETS_DIR/$file ]; then
		echo Copying file $MA_TARGETS_DIR/$file to $RPM_BUILD_ROOT/$MA_INSTALL_PATH/bin/$file
		cp $MA_TARGETS_DIR/$file $RPM_BUILD_ROOT/$MA_INSTALL_PATH/bin/$file
		echo $RPM_FILE_CONST $RPM_SIG_FILE_PERMISSIONS $MA_INSTALL_PATH/bin/$file >> /tmp/ma_rpm_spec1
	fi
done

# Copying all ma tools lib files into build root
TOOLS_LIST=`cat $RPM_CURRENT_DIR/../ma_install_tools | grep $PLATFORM  | grep -v ^#`
for file in `eval echo $TOOLS_LIST` 
do
	lib_ext=`echo $file | cut -d: -f2`
	lib_dir=`echo $file | cut -d: -f3`
	lib_name=`echo $file | cut -d: -f4`
	
	if [ "$lib_dir" == "mfecryptc" -a "$CRYPTO" == "openssl" ]; then
		lib_dir=mfecryptc_openssl
	fi

	if [ "$lib_dir" == "curl" -a "$CRYPTO" == "openssl" ]; then
		lib_dir=curl_openssl
	fi

	if [ -e $MA_TOOLS_TARGETS_DIR/$lib_dir/lib/$SUFFIX_DIR/$lib_name ]; then
		lib_name1=`echo $lib_name | cut -d. -f1-3`
		lib_name2=`echo $lib_name | cut -d. -f1-2`
		if [ $lib_ext = "a" ]; then
			echo Copying file $MA_TOOLS_TARGETS_DIR/$lib_dir/lib/$SUFFIX_DIR/$lib_name to $RPM_BUILD_ROOT/$MA_INSTALL_PATH/lib/tools/$lib_name
			cp $MA_TOOLS_TARGETS_DIR/$lib_dir/lib/$SUFFIX_DIR/$lib_name $RPM_BUILD_ROOT/$MA_INSTALL_PATH/lib/tools/$lib_name
			echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS $MA_INSTALL_PATH/lib/tools/$lib_name >> /tmp/ma_rpm_spec1
			cd $RPM_CURRENT_DIR
		else
			echo Copying file $MA_TOOLS_TARGETS_DIR/$lib_dir/lib/$SUFFIX_DIR/$lib_name to $RPM_BUILD_ROOT/$MA_INSTALL_PATH/lib/tools/$lib_name
			cp $MA_TOOLS_TARGETS_DIR/$lib_dir/lib/$SUFFIX_DIR/$lib_name $RPM_BUILD_ROOT/$MA_INSTALL_PATH/lib/tools/$lib_name
			echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS $MA_INSTALL_PATH/lib/tools/$lib_name >> /tmp/ma_rpm_spec1
			echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS $MA_INSTALL_PATH/lib/tools/$lib_name1 >> /tmp/ma_rpm_spec1
			echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS $MA_INSTALL_PATH/lib/tools/$lib_name2 >> /tmp/ma_rpm_spec1
			cd $RPM_BUILD_ROOT/$MA_INSTALL_PATH/lib/tools
			ln -sf $lib_name $lib_name1
			ln -sf $lib_name1 $lib_name2
			cd $RPM_CURRENT_DIR
		fi
	fi
        if [ -e $MA_TOOLS_TARGETS_DIR/$lib_dir/bin/$SUFFIX_DIR/$lib_name ]; then
                if [ -f $MA_TOOLS_TARGETS_DIR/$lib_dir/bin/$SUFFIX_DIR/$lib_name ]; then
                        echo Copying file $MA_TOOLS_TARGETS_DIR/$lib_dir/bin/$SUFFIX_DIR/$lib_name to $RPM_BUILD_ROOT/$MA_INSTALL_PATH/bin/$lib_name
                        cp $MA_TOOLS_TARGETS_DIR/$lib_dir/bin/$SUFFIX_DIR/$lib_name $RPM_BUILD_ROOT/$MA_INSTALL_PATH/bin/$lib_name
                        chmod +x $RPM_BUILD_ROOT/$MA_INSTALL_PATH/bin/$lib_name
                fi
        fi
done

echo ******* $CRYPTO ***************
# RSA or OPENSSL files copying into tools folder, 
if [ "$CRYPTO" == "openssl" ]
then
	cp $MA_TOOLS_TARGETS_DIR/mfecryptc_openssl/lib/$SUFFIX_DIR/libcrypto.so.1.0.0  $RPM_BUILD_ROOT/$MA_INSTALL_PATH/lib/tools/libcrypto.so.1.0.0
	# adding write permissions
	chmod 755 $RPM_BUILD_ROOT/$MA_INSTALL_PATH/lib/tools/libcrypto.so.1.0.0
	echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS $MA_INSTALL_PATH/lib/tools/libcrypto.so.1.0.0 >> /tmp/ma_rpm_spec1
	echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS $MA_INSTALL_PATH/lib/tools/libcrypto.so >> /tmp/ma_rpm_spec1
	cp $MA_TOOLS_TARGETS_DIR/mfecryptc_openssl/lib/$SUFFIX_DIR/libssl.so.1.0.0  $RPM_BUILD_ROOT/$MA_INSTALL_PATH/lib/tools/libssl.so.1.0.0
	chmod 755 $RPM_BUILD_ROOT/$MA_INSTALL_PATH/lib/tools/libssl.so.1.0.0
	echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS $MA_INSTALL_PATH/lib/tools/libssl.so.1.0.0 >> /tmp/ma_rpm_spec1
	echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS $MA_INSTALL_PATH/lib/tools/libssl.so >> /tmp/ma_rpm_spec1
	cd $RPM_BUILD_ROOT/$MA_INSTALL_PATH/lib/tools
	ln -sf libcrypto.so.1.0.0 libcrypto.so
	ln -sf libssl.so.1.0.0 libssl.so
	cd $RPM_CURRENT_DIR
else
	cp $MA_TOOLS_TARGETS_DIR/mfecryptc/lib/$SUFFIX_DIR/libccme_base.so  $RPM_BUILD_ROOT/$MA_INSTALL_PATH/lib/tools/libccme_base.so
	echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS $MA_INSTALL_PATH/lib/tools/libccme_base.so >> /tmp/ma_rpm_spec1
	cp $MA_TOOLS_TARGETS_DIR/mfecryptc/lib/$SUFFIX_DIR/libccme_base_non_fips.so  $RPM_BUILD_ROOT/$MA_INSTALL_PATH/lib/tools/libccme_base_non_fips.so
	echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS $MA_INSTALL_PATH/lib/tools/libccme_base_non_fips.so >> /tmp/ma_rpm_spec1
	cp $MA_TOOLS_TARGETS_DIR/mfecryptc/lib/$SUFFIX_DIR/libccme_asym.so  $RPM_BUILD_ROOT/$MA_INSTALL_PATH/lib/tools/libccme_asym.so
	echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS $MA_INSTALL_PATH/lib/tools/libccme_asym.so >> /tmp/ma_rpm_spec1
	cp $MA_TOOLS_TARGETS_DIR/mfecryptc/lib/$SUFFIX_DIR/libcryptocme.sig  $RPM_BUILD_ROOT/$MA_INSTALL_PATH/lib/tools/libcryptocme.sig
	echo $RPM_FILE_CONST $RPM_SIG_FILE_PERMISSIONS $MA_INSTALL_PATH/lib/tools/libcryptocme.sig >> /tmp/ma_rpm_spec1
	cp $MA_TOOLS_TARGETS_DIR/mfecryptc/lib/$SUFFIX_DIR/libcryptocme.so  $RPM_BUILD_ROOT/$MA_INSTALL_PATH/lib/tools/libcryptocme.so
	echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS $MA_INSTALL_PATH/lib/tools/libcryptocme.so >> /tmp/ma_rpm_spec1
fi	

# Copying all compat tools lib files into build root
TOOLS_LIST=`cat $RPM_CURRENT_DIR/../ma_compat_install_tools | grep $PLATFORM | grep -v ^#`
echo Tool list for compat $TOOLS_LIST
for file in `eval echo $TOOLS_LIST` 
do
	lib_dir=`echo $file | cut -d: -f2`
	lib_name=`echo $file | cut -d: -f3`
	if [ -e $BOOST_BASE/lib/$lib_name ]; then
		lib_name1=`echo $lib_name | cut -d. -f1-3`
		lib_name2=`echo $lib_name | cut -d. -f1-2`
		echo Copying file $BOOST_BASE/lib/$lib_name to $RPM_BUILD_ROOT/$MA_INSTALL_PATH/lib/tools/$lib_name
		cp $BOOST_BASE/lib/$lib_name $RPM_BUILD_ROOT/$MA_INSTALL_PATH/lib/tools/$lib_name
		echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS $MA_INSTALL_PATH/lib/tools/$lib_name >> /tmp/ma_rpm_spec1
		if [ x"$PLATFORM" == x"Linux" ] ;then
			echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS $MA_INSTALL_PATH/lib/tools/$lib_name1 >> /tmp/ma_rpm_spec1
			echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS $MA_INSTALL_PATH/lib/tools/$lib_name2 >> /tmp/ma_rpm_spec1
			cd $RPM_BUILD_ROOT/$MA_INSTALL_PATH/lib/tools
			ln -sf $lib_name $lib_name1
			ln -sf $lib_name1 $lib_name2
			cd $RPM_CURRENT_DIR
		fi
	fi
done

echo $RPM_DIR_CONST $MA_DATA_PATH/scratch >> /tmp/ma_rpm_spec1
ln -sf $MA_DATA_PATH/data $RPM_BUILD_ROOT/$MA_DATA_PATH/scratch

if [ x"mileaccess" != x"$LINUX_DIST" ]; then
	echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS $MA_INSTALL_PATH/lib/libeventwriter.so.4 >> /tmp/ma_rpm_spec1
	echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS $MA_INSTALL_PATH/lib/libeventinterface.so.4 >> /tmp/ma_rpm_spec1
	echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS $MA_INSTALL_PATH/lib/libppupdaterstub.so.4 >> /tmp/ma_rpm_spec1
	echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS $MA_INSTALL_PATH/lib/libnaxml.so.4 >> /tmp/ma_rpm_spec1
	echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS $MA_INSTALL_PATH/lib/libmfecomn.so.4 >> /tmp/ma_rpm_spec1
	echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS $MA_INSTALL_PATH/lib/libnairal.so.4 >> /tmp/ma_rpm_spec1
	cd $RPM_BUILD_ROOT/$MA_INSTALL_PATH/lib
	ln -sf libeventwriter.so.5.0 libeventwriter.so.4
	ln -sf libeventinterface.so.5.0 libeventinterface.so.4	
	ln -sf libppupdaterstub.so.5.0 libppupdaterstub.so.4	
	ln -sf libnaxml.so.5.0 libnaxml.so.4
	ln -sf libmfecomn.so.5.0 libmfecomn.so.4
	ln -sf libnairal.so.5.0 libnairal.so.4
fi

if [ x"$PLATFORM" == x"AIX" ] ;then
	#LIB_PATH="/opt/freeware/lib/gcc/powerpc-ibm-aix5.3.0.0/4.2.4/pthread"
	LIB_PATH="$RPM_CURRENT_DIR/../data/aix_stdlib"
	LIB_STD="$LIB_PATH/libstdc++.a"
	LIB_GCC="$LIB_PATH/libgcc_s.a"
	echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS $MA_INSTALL_PATH/lib/tools/libstdc++.a >> /tmp/ma_rpm_spec1
	echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS $MA_INSTALL_PATH/lib/tools/libgcc_s.a >> /tmp/ma_rpm_spec1

	cp -f  $LIB_STD  $RPM_BUILD_ROOT/opt/mileaccess/agent/lib/tools/
	if (( $? )); then echo "Copy Failed  $$LIB_STD"; fi

	cp -f $LIB_GCC $RPM_BUILD_ROOT/opt/mileaccess/agent/lib/tools/
	if (( $? )); then echo "Copy Failed $LIB_GCC "; fi
fi
################################################################
#Step 3: Creating Script and config Files
################################################################
echo $'\n\n************* Step 3: Creating script and config files'

if [ x"$PLATFORM" == x"AIX" ] ;then
	INIT_PATH="/usr/sbin"
	STARTUP_SCRIPT="/usr/sbin/cma"
	mkdir -p $RPM_BUILD_ROOT/etc/rc.d/rc2.d/
else
	INIT_PATH="/etc/init.d"
	STARTUP_SCRIPT="/etc/init.d/ma"
fi
# Update placeholders in config.xml
MA_START_CMD="$INIT_PATH/ma start"
MA_STOP_CMD="$INIT_PATH/ma stop"
if [ x"$MA_DEPS" == x ]; then
	MA_UNINSTALL_CMD="rpm -e MFEcma"
else
	MA_UNINSTALL_CMD="rpm -e MFEcma;rpm -e MFErt"
fi
str="s:__MA_UPDATER_PATH__:$MA_UPDATER_PATH:g;s:__MA_SOFTWARE_ID__:$MA_SOFTWAREID:g;s:__MA_VERSION__:$MA_VERSION:g;s:__PACKAGE_NUMBER__:$PACKAGE_NUM:g;s:__MA_DATA_PATH__:$MA_DATA_PATH:g;s:__MA_INSTALL_PATH__:$MA_INSTALL_PATH:g;s:__MA_START_CMD__:$MA_START_CMD:g;s:__MA_STOP_CMD__:$MA_STOP_CMD:g;s:__MA_UNINSTALL_CMD__:$MA_UNINSTALL_CMD:g;s:__MA_SHARED_LIB_PATH__:$MA_CMA_LPC_LIBS_PATH:g"
sed -e "$str" $RPM_CURRENT_DIR/../data/config.xml > $RPM_BUILD_ROOT/$MA_CONFIG_PATH/$MA_SOFTWAREID/config.xml

str="s:__MA_VERSION__:$MA_VERSION:g;s:__PACKAGE_NUMBER__:$PACKAGE_NUM:g;s:__MA_INSTALL_PATH__:$MA_INSTALL_PATH:g;s:__MA_DATA_PATH__:$MA_DATA_PATH:g"
sed -e "$str" $RPM_CURRENT_DIR/../data/mainfo.ini > $RPM_BUILD_ROOT/$MA_CONFIG_PATH/mainfo.ini 
echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS /etc/ma.d/mainfo.ini >> /tmp/ma_rpm_spec1

if [ x"mileaccess" != x"$LINUX_DIST" ]; then	
	cp $RPM_CURRENT_DIR/../data/msaconfig $RPM_BUILD_ROOT/$MA_INSTALL_PATH/bin/
	echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS $MA_INSTALL_PATH/bin/msaconfig >> /tmp/ma_rpm_spec1
fi

if [ x"$PLATFORM" == x"AIX" ] ;then
	cp -f $RPM_BUILD_ROOT/$MA_INSTALL_PATH/scripts/ma $RPM_BUILD_ROOT/etc/rc.d/rc2.d/Kma
	cp -f $RPM_BUILD_ROOT/$MA_INSTALL_PATH/scripts/ma $RPM_BUILD_ROOT/etc/rc.d/rc2.d/Sma
	echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS /etc/rc.d/rc2.d/Kma >> /tmp/ma_rpm_spec1
	echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS /etc/rc.d/rc2.d/Sma >> /tmp/ma_rpm_spec1
fi

mkdir -p $RPM_BUILD_ROOT/$INIT_PATH
cp $RPM_BUILD_ROOT/$MA_INSTALL_PATH/scripts/ma $RPM_BUILD_ROOT/$INIT_PATH/ma
cd $RPM_BUILD_ROOT/$INIT_PATH/
ln -sf  ma cma
echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS /$INIT_PATH/ma >> /tmp/ma_rpm_spec1
echo $RPM_FILE_CONST $RPM_FILE_PERMISSIONS /$INIT_PATH/cma >> /tmp/ma_rpm_spec1
# TODO - strip $RPM_BUILD_ROOT/opt/mileaccess/cma/lib/lib*

HasCRLFHenceExit $RPM_BUILD_ROOT

##### Adding pre/post install/uninstall scripts ######
cat /tmp/ma_rpm_spec1 |grep %dir > /tmp/ma_dpkg_spec
cat /tmp/ma_rpm_spec1 |grep %attr >> /tmp/ma_dpkg_spec
str="s:%dir:set_directory_permission:g;s:%attr:set_file_permission:g;s:(: :g;s:): :g;s:,: :g"
sed -e "$str" /tmp/ma_dpkg_spec > /tmp/ma_dpkg_spec1

SCRIPT_PATH=$RPM_CURRENT_DIR/../scripts
if [ x"$PLATFORM" == x"AIX" ] ;then
	parseFile "$RPM_CURRENT_DIR/.." "/tmp/ma_rpm_spec1" "/tmp/ma_rpm_spec7" 
else
	awk -v var="`cat $SCRIPT_PATH/ma_prepinstall`" '{ gsub(/__REPLACE_PREPINSTALL_SCRIPT__/,var,$0); print  }' /tmp/ma_rpm_spec1 > /tmp/ma_rpm_spec2
	awk -v var="`cat $SCRIPT_PATH/ma_install`" '{ gsub(/__REPLACE_INSTALL_SCRIPT__/,var,$0); print  }' /tmp/ma_rpm_spec2 > /tmp/ma_rpm_spec3
	awk -v var="`cat $SCRIPT_PATH/ma_preinstall`" '{ gsub(/__REPLACE_PREINSTALL_SCRIPT__/,var,$0); print  }' /tmp/ma_rpm_spec3 > /tmp/ma_rpm_spec4
	awk -v var="`cat $SCRIPT_PATH/ma_postinstall`" '{ gsub(/__REPLACE_POSINSTALL_SCRIPT__/,var,$0); print  }' /tmp/ma_rpm_spec4 > /tmp/ma_rpm_spec5
	awk -v var="`cat $SCRIPT_PATH/ma_preuninstall`" '{ gsub(/__REPLACE_PREUNINSTALL_SCRIPT__/,var,$0); print  }' /tmp/ma_rpm_spec5 > /tmp/ma_rpm_spec6
	awk -v var="`cat $SCRIPT_PATH/ma_postuninstall`" '{ gsub(/__REPLACE_POSTUNINSTALL_SCRIPT__/,var,$0); print  }' /tmp/ma_rpm_spec6 > /tmp/ma_rpm_spec7
fi
str="s:__STARTUP_SCRIPT__:$STARTUP_SCRIPT:g;s:__PLATFORM__:$PLATFORM:g;s:__DEBIAN_SET_FILE_DIR_PERMISSION__: :g"
sed -e "$str" /tmp/ma_rpm_spec7 > /tmp/ma_rpm_spec8
#####################################################
dfout=`du -sk $RPM_BUILD_ROOT`
pkgsize=`echo $dfout | cut -d' ' -f1`
reqsize=$(( (($pkgsize + 1023 )/1024)*1024 ))
echo "Package Size of rpm package is $reqsize KB"

str="s:^REQUIRED_INSTALLATION_SIZE.*:REQUIRED_INSTALLATION_SIZE=$reqsize:;s:__MA_VERSION__:$MA_VERSION:g;s:__PACKAGE_NUMBER__:$PACKAGE_NUM:g;s:__MA_SOFTWARE_ID__:$MA_SOFTWAREID:g;s:__MA_DATA_PATH__:$MA_DATA_PATH:g;s:__MA_MSGBUS_DATA_PATH__:$MA_MSGBUS_DATA_PATH:g;s:__MA_INSTALL_PATH__:$MA_INSTALL_PATH:g;s:__CMA_CONFIG_PATH__:$CMA_CONFIG_PATH:g;s:__CMA_SOFTWAREID__:$CMA_SOFTWAREID:g;s:__MA_CMN_UPDATERID__:$MA_CMN_UPDATERID:g;s:__MA_CONFIG_PATH__:$MA_CONFIG_PATH:g;s:__MA_CMA_LPC_LIBS_PATH__:$MA_CMA_LPC_LIBS_PATH:g;s:__LIB_EXTENSION__:$LIB_EXTENSION:g;s:__CMA_INSTALL_PATH__:$CMA_INSTALL_PATH:g;s:__CMA_SHARED_PATH__:$CMA_SHARED_PATH:g"
if [ x"$MA_DEPS" == x ];then
	str="$str;s:^Requires:#Requires:g;"
else
	str="$str;s:__DEPS__:$MA_DEPS:g;"
fi
#pre and post install script replace text
if [ x"$PLATFORM" == x"AIX" ] ;then
	OPT_FLAG="-a"
	DF_FLAG1=""	
	DF_FLAG2="-f3"
	GREP_CMD="grep -E 'EPOAGENT|CMNUPD|CMDAGENT'"
else
	OPT_FLAG="-f"
	DF_FLAG1="-l"	
	DF_FLAG2="-f4"
	GREP_CMD="grep 'EPOAGENT\|CMNUPD\|CMDAGENT'"
fi
str="$str;s:__OPT_FLAG__:$OPT_FLAG:g;s:__DF_FLAG1__:$DF_FLAG1:g;s:__DF_FLAG2__:$DF_FLAG2:g;s:__GREP_CMD__:$GREP_CMD:g"
sed -e "$str" /tmp/ma_rpm_spec8 > /tmp/ma_rpm_spec9
echo "End editing the rpm spec file for package sizes"

################################################################
#Step 4: Building rpm installer
################################################################
echo $'\n\n************* Step 4: Building rpm installer'

echo "Building the rpm package..."
cp /tmp/ma_rpm_spec9 /tmp/ma_rpm_spec.bak
if [ -f /usr/bin/rpmbuild ] ; then 
	RPMCMD="/usr/bin/rpmbuild -bb"
else
	RPMCMD="/bin/rpm -bb"
fi

RPMDIR="packages"

if [ x"mileaccess" == x"$LINUX_DIST" ]; then
	echo  Creating tar of file for MLOS rpmbuld 
	if [ -f /tmp/MFEmatar-$MA_VERSION-$PACKAGE_NUM.tar ]; then
          rm /tmp/MFEmatar-$MA_VERSION-$PACKAGE_NUM.tar
        fi

	cd $RPM_BUILD_ROOT
	tar cf /tmp/MFEmatar-$MA_VERSION-$PACKAGE_NUM.tar *
 	cd $RPM_CURRENT_DIR
fi

mkdir ${RPM_BUILD_ROOT}_tmp
if [ x"$PLATFORM" == x"AIX" ] ;then
	CP_FLAG="-rPf"
elif [ x"$PLATFORM" == x"Linux" ] ;then
	CP_FLAG="-rf"
fi
cp $CP_FLAG ${RPM_BUILD_ROOT}/* ${RPM_BUILD_ROOT}_tmp
echo $RPMCMD --buildroot "$RPM_BUILD_ROOT" --target $TARGET /tmp/ma_rpm_spec9
cd
$RPMCMD --buildroot "$RPM_BUILD_ROOT" --target $TARGET /tmp/ma_rpm_spec9
echo "Finished rpm package creation with status $?"
if [ x"mileaccess" != x"$LINUX_DIST" ]; then
	createDebianPackageIfRequired
fi
export NSS_MDNS=$MA_TOOLS/nss-mdns/lib/release
#Call Nails tools Package creation ,hard coding for libcversion , we may have to change it to 2.3.4
# TODO -- skipping MFErt builds for 64 builds. will think about later...
#if [ x"$PLATFORM" == x"Linux" -a x"$ARCH_BIT" == x"32" ]; then
	echo "Executing command sh $RPM_CURRENT_DIR/mfert/runtime_package.sh -v 3.0 -c 2.5 -p $PACKAGE_NUM -o $OUTPUT_DIR"
	sh $RPM_CURRENT_DIR/mfert/runtime_package.sh -v 2.0 -c 2.5 -p $PACKAGE_NUM -o $OUTPUT_DIR
	if (( $? )); then
		echo "Failed to create the run time Package"
	fi
#fi


echo "Copying MA rpm files to output directory, $OUTPUT_DIR"
#/opt/freeware/src/packages/RPMS/i686/MFEcma-5.0.0-111.aix5.3.i686.rpm

if [ x"mileaccess" == x"$LINUX_DIST" ]; then
	# For MLOS
	if [ -f /tmp/MFEmatar-$MA_VERSION-$PACKAGE_NUM.tar ]; then
			rm /tmp/MFEmatar-$MA_VERSION-$PACKAGE_NUM.tar
	fi
	mv ~/rpmbuild/RPMS/$TARGET/MFEcma-$MA_VERSION-$PACKAGE_NUM.$TARGET.rpm $OUTPUT_DIR/MFEma-$MA_VERSION-$PACKAGE_NUM.$TARGET.mlos.rpm
elif [ x"$PLATFORM" == x"AIX" ] ;then
	#AIX
	AIX_VER="aix5.3"
	if [ -f /opt/freeware/src/packages/RPMS/$TARGET/MFEcma-$MA_VERSION-$PACKAGE_NUM.$AIX_VER.$TARGET.rpm ]; then
		mv /opt/freeware/src/packages/RPMS/$TARGET/MFEcma-$MA_VERSION-$PACKAGE_NUM.$AIX_VER.$TARGET.rpm $OUTPUT_DIR/MFEma-$MA_VERSION-$PACKAGE_NUM.$TARGET.$PLATFORM.rpm
	fi
else
	# Non MLOS
	if [ -f /usr/src/$RPMDIR/RPMS/$TARGET/MFEcma-$MA_VERSION-$PACKAGE_NUM.$TARGET.rpm ]; then
		mv /usr/src/$RPMDIR/RPMS/$TARGET/MFEcma-$MA_VERSION-$PACKAGE_NUM.$TARGET.rpm $OUTPUT_DIR/MFEma-$MA_VERSION-$PACKAGE_NUM.$TARGET.$PLATFORM.rpm
	else
		mv ~/rpmbuild/RPMS/$TARGET/MFEcma-$MA_VERSION-$PACKAGE_NUM.$TARGET.rpm $OUTPUT_DIR/MFEma-$MA_VERSION-$PACKAGE_NUM.$TARGET.$PLATFORM.rpm
	fi
	if (( $? )); then
		echo "Unable to copy ma rpm to $OUTPUT_DIR "
	fi
	#Copying debian package
	mv /tmp/MFEma-$MA_VERSION-$PACKAGE_NUM-buildroot-debian.deb $OUTPUT_DIR/MFEma-$MA_VERSION-$PACKAGE_NUM.$TARGET.$PLATFORM.deb
	if (( $? )); then
			echo "Unable to copy ma deb to $OUTPUT_DIR "
	else
		echo "Successfully copied debian package to output directory $outdir"
	fi
fi

if (( $? )); then
	echo "Unable to move MA rpm to $OUTPUT_DIR "
fi
################################################################
#Step 5: Packaging bootstrap
################################################################
echo $'\n\n************* Step 5: Packaging bootstrap'

###################################
#bootstrap  bootstrap_sfx.sh  PackageInfo.xml  BootstrapInfo.xml 
strip $MA_TARGETS_DIR/bootstrap
if [ x"$PLATFORM" = x"Linux" ] ;then
	echo "compressing with upx command $UPX_PATH -9 $MA_TARGETS_DIR/bootstrap"
	$UPX_PATH -9 $MA_TARGETS_DIR/bootstrap
fi
if [ x"$PLATFORM" = x"Linux" ] && [ x"$ARCH_BIT" == x"32" ];then
	#For Linux 32, packaging the 64 bit bootstrap as well
	BOOTSTRAP_PAYLOAD="bootstrap.tar.gz"
	cp $MA_TARGETS_DIR/bootstrap $OUTPUT_DIR/bootstrap_x32
	cp $MA_BASE/build/packager/unix/rpm/lib64/bootstrap_x64 $OUTPUT_DIR/bootstrap_x64
	cur_dir=`pwd`
	cd $OUTPUT_DIR
	tar -czf bootstrap bootstrap_x32 bootstrap_x64
	cd $cur_dir
else
	BOOTSTRAP_PAYLOAD="bootstrap"
	cp $MA_TARGETS_DIR/bootstrap $OUTPUT_DIR/bootstrap
fi
cp $RPM_CURRENT_DIR/../data/BootstrapInfo.xml $OUTPUT_DIR
cp $RPM_CURRENT_DIR/../data/PackageInfo.xml $OUTPUT_DIR
if [ x"$PLATFORM" = x"Linux" ] ;then
	bootstrap_size=`stat -c %s $OUTPUT_DIR/bootstrap`
	cp $MA_BASE/build/packager/data/PkgCatalog_unx.xml $OUTPUT_DIR/PkgCatalog.xml
	str="s:__BOOSTRAP_SIZE__:$bootstrap_size:g;s:__BOOTSTRAP_PAYLOAD__:$BOOTSTRAP_PAYLOAD:g"
	sed -e "$str" $RPM_CURRENT_DIR/../bootstrap_sfx/bootstrap_sfx.linux.sh.template > $OUTPUT_DIR/bootstrap_sfx.sh
elif [ x"$PLATFORM" = x"AIX" ] ;then
	PRODUCT_ID_CATALOG=EPOAGENT4000AIXX
	bootstrap_size=`ls  -l $OUTPUT_DIR/bootstrap | awk '{print $5}'`
	unzip_exe_size=`ls  -l $MA_TARGETS_DIR/unz | awk '{print $5}'`
	MAJVER=`echo $MA_VERSION | cut -d. -f1`
	MINVER=`echo $MA_VERSION | cut -d. -f2`
	SUBVER=`echo $MA_VERSION | cut -d. -f3`
	str="s:UNZIP_SIZE_IN_BYTES:$unzip_exe_size:g;s:__MA_Major_Version__:$MAJVER:g;s:__MA_Minor_Version__:$MINVER:g;s:__MA_Patch_Version__:$SUBVER:g;s:__MA_Build_Number__:$PACKAGE_NUM:g"
	ls -l $MA_BASE/build/packager/unix/sfx/sfx.aix.sh.template
	sed -e "$str" $MA_BASE/build/packager/unix/sfx/sfx.aix.sh.template > $OUTPUT_DIR/sfx.sh
	sed -e "s:OUTPUT_FILELIST:\"install.sh;AgentInst.McS;setup;FrameworkConfig.zip\":g;s:_PRODUCT_ID_:$PRODUCT_ID_CATALOG:g;s:<InstallType>script</InstallType>:<InstallType>command</InstallType>:g;s:<InstallCommand>AgentInst.McS</InstallCommand>:<InstallCommand>setup</InstallCommand>:g;" $MA_BASE/build/packager/data/PkgCatalog_unx.xml > $OUTPUT_DIR/PkgCatalog.xml
	cp $MA_TARGETS_DIR/unz $OUTPUT_DIR/unz
	str="s:__BOOSTRAP_SIZE__:$bootstrap_size:g;s:__BOOTSTRAP_PAYLOAD__:$BOOTSTRAP_PAYLOAD:g"
	sed -e "$str" $RPM_CURRENT_DIR/../bootstrap_sfx/bootstrap_sfx.aix.sh.template > $OUTPUT_DIR/bootstrap_sfx.sh
fi
echo "bootstrap packaging done."	
	
################################################################
#Step 6: Packaging SDK and beacon
################################################################
echo $'\n\n************* Step 6: Packaging SDK and beacon'

###################################
# Creating SDK tar file (sdk and beacon rpm files) in output folder
SDK_DIR=/tmp/sdk_temp
if [ -e $SDK_DIR ]
then
rm -fr $SDK_DIR
fi
mkdir -p $SDK_DIR
mkdir -p $SDK_DIR/samples
mkdir -p $SDK_DIR/include
mkdir -p $SDK_DIR/bindings
mkdir -p $SDK_DIR/lib/$SUFFIX_DIR
mkdir -p $SDK_DIR/rsdk/$SUFFIX_DIR
mkdir -p $SDK_DIR/masign

SAMPLES_DIR=$SDK_DIR/samples
cp -r $MA_BASE/tools/ma_sdk/include/* $SDK_DIR/include
cp -r $MA_BASE/tools/ma_sdk/bindings/* $SDK_DIR/bindings
cp -r $MA_BASE/tools/ma_sdk/lib/$SUFFIX_DIR/* $SDK_DIR/lib/$SUFFIX_DIR
cp -r $MA_BASE/tools/ma_sdk/rsdk/$SUFFIX_DIR/* $SDK_DIR/rsdk/$SUFFIX_DIR
cp -r $MA_TOOLS_TARGETS_DIR/masign/* $SDK_DIR/masign
cp  $MA_BASE/build/packager/data/certs/ma_test_certs.tar $SDK_DIR/masign/

echo "Executing command sh $MA_BASE/tools/beacon/build/packager/unix/bc_packager.sh -v $MA_VERSION -n $PACKAGE_NUM -c $CONFIG_TYPE  -p $PLATFORM -b $ARCH_BIT -s $DATE_AND_TIME -o $SAMPLES_DIR"
sh  $MA_BASE/tools/beacon/build/packager/unix/bc_packager.sh  -v $MA_VERSION -n $PACKAGE_NUM -c $CONFIG_TYPE  -p $PLATFORM -b $ARCH_BIT -s $DATE_AND_TIME -o $SAMPLES_DIR
if (( $? )); then
	echo "Failed to create the beacon Package"
fi

cur_dir=`pwd`
cd $MA_BASE/tools/beacon/
tar cvf $SAMPLES_DIR/beacon.tar build include src
cd $cur_dir

cd $SDK_DIR
if [ x"mileaccess" == x"$LINUX_DIST" ] # For MLOS
then
tar cvf $OUTPUT_DIR/ma_sdk500_mlos.tar include lib bindings rsdk samples masign
else
tar cvf $OUTPUT_DIR/ma_sdk500_$PLATFORM.tar include lib bindings rsdk samples masign
fi
cd $cur_dir
rm -fr $SDK_DIR
if [ -d $RPM_BUILD_ROOT ]; then
	rm -rf $RPM_BUILD_ROOT
fi
if [ -d ${RPM_BUILD_ROOT}_tmp ]; then
	rm -rf ${RPM_BUILD_ROOT}_tmp
fi
if [ -d $DEBIAN_BUILD_ROOT ]; then
	rm -rf $DEBIAN_BUILD_ROOT
fi
rm -rf /tmp/ma_rpm_spec1
rm -rf /tmp/ma_rpm_spec2
rm -rf /tmp/ma_rpm_spec3
rm -rf /tmp/ma_rpm_spec4
rm -rf /tmp/ma_rpm_spec5
rm -rf /tmp/ma_rpm_spec6
rm -rf /tmp/ma_rpm_spec7
rm -rf /tmp/ma_rpm_spec8
rm -rf /tmp/ma_rpm_spec9
rm -rf /tmp/ma_dpkg_spec
rm -rf /tmp/ma_dpkg_spec1
cp  /tmp/ma_rpm_spec.bak $OUTPUT_DIR
rm -rf /tmp/ma_rpm_spec.bak
