# Copyright (C) 2013  mileaccess Inc; All rights reserved
###################################################################
# mileaccess Agent
#
###################################################################
Name            : MFEcma
Version         : __MA_VERSION__
Release         : __PACKAGE_NUMBER__
Summary         : mileaccess Agent 
License         : commercial. See the COPYRIGHT file for details.
Group           : Network/Agent
Vendor          : mileaccess Inc.
Packager        : mileaccess Inc.
Conflicts       : NWA 
BuildRoot       : /tmp/%{name}-%{version}-%{release}-buildroot
Requires        : __DEPS__
Autoreq         : 0
Autoprov        : 0



#################################
# Here begins the beefy bits....#
#################################
%undefine __check_files
%define _source_filedigest_algorithm 0
%define _binary_filedigest_algorithm 0
%define _binary_payload w9.gzdio
%define __os_install_post %{nil}


%description 
mileaccess Agent

%prep
__REPLACE_PREPINSTALL_SCRIPT__

%install
__REPLACE_INSTALL_SCRIPT__

%pre
__REPLACE_PREINSTALL_SCRIPT__

%post
__REPLACE_POSINSTALL_SCRIPT__

%clean

%preun
__REPLACE_PREUNINSTALL_SCRIPT__

%postun
__REPLACE_POSTUNINSTALL_SCRIPT__


%files
# The files list will be appended here dynamically based on the files list in ma_install_files and ma_install_tools
# Don't add anything after this...

