#!/bin/sh
# Script to notify cma about point product plugin library change

COMAPT_SVC_PID=/var/mileaccess/agent/.macompatsvc.pid

notify() {
        pid=
        if [ -f $COMAPT_SVC_PID ]; then
                pid=`cat $COMAPT_SVC_PID`
	else
		pid=`ps ax | grep macompatsvc | grep -v grep | awk '{print $1}' | head -1`
	fi
       	if [ $pid ];then
	        kill -USR1 $pid
        else
            echo "CMA is not running , can't notify."
       fi
}
reload() {
        pid=
        if [ -f $COMAPT_SVC_PID ]; then
                pid=`cat $COMAPT_SVC_PID`
	else
		pid=`ps ax | grep macompatsvc | grep -v grep | awk '{print $1}' | head -1`
	fi
      	if [ $pid ];then
		echo "$1" > "/var/mileaccess/agent/.plugininfo"
                kill -USR1 $pid
        else
                echo "CMA is not running , can't notify."
        fi
}
unload() {
        pid=
        if [ -f $COMAPT_SVC_PID ]; then
                pid=`cat $COMAPT_SVC_PID`
	else
		pid=`ps ax | grep macompatsvc | grep -v grep | awk '{print $1}' | head -1`
	fi
      	if [ $pid ];then
		echo "$1" > "/var/mileaccess/agent/.plugininfo"
                kill -USR2 $pid
        else
                echo "CMA is not running , can't notify."
        fi
}

case "$1" in
  notify)
	notify
 	;;
  reload)
	if [ $# -ne 2 ]; then
		echo "Usage: $0 {notify|reload SOFTWAREID|unload SOFTWAREID}"
		exit 1;
	fi
	reload "$2"
 	;;
  unload)
	if [ $# -ne 2 ]; then
		echo "Usage: $0 {notify|reload SOFTWAREID|unload SOFTWAREID}"
		exit 1;
	fi
	unload "$2"
 	;;
  *)
	echo "Usage: $0 {notify|reload SOFTWAREID|unload SOFTWAREID}"
	exit 4
esac

exit $?

