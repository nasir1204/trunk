#!/bin/bash

bin_path=
signing_dir=

echo $# 

if [ $# -ne 3 ]; then
  echo "$0 <bin_path> <signing_dir> <bin/app/pkg>"
  exit 1  
fi

bin_path=$1
signing_dir=$2
bin_pkg=$3 

sign_ready_sig=
sign_done_sig=

if [ $bin_pkg == "bin" -o $bin_pkg == "app" ]; then
sign_ready_sig="sign_ready"
sign_done_sig="sign_done"
else
sign_ready_sig="pkg_ready"
sign_done_sig="pkg_done"
fi

echo "binary files path " $bin_path
echo "signing directory path" $signing_dir

if [ ! -d $bin_path ]; then
    echo "Invalid path $bin_path for binaries which are need to be signed"
    exit 1
fi

if [ ! -d $signing_dir ]; then
    echo "Invalid signing dir $signing_dir"
    exit 1
fi

#mkdir -p $signing_dir/sign_in/
#mkdir -p $signing_dir/sign_out/
rm -rf  $signing_dir/sign_in/* $signing_dir/sign_out/*

cd $bin_path
files=
if [ $bin_pkg == "bin" ]; then
	files=`find . -maxdepth 1 -type f`
	echo "binary files for signing " $files
	echo "coying all files "
	cp $files $signing_dir/sign_in/

	# remove the third party libs
	rm -f $signing_dir/sign_in/libboost*
	rm -f $signing_dir/sign_in/libcurl*
	rm -f $signing_dir/sign_in/libz.*
	rm -f $signing_dir/sign_in/sign_*

	cd $signing_dir/sign_in/
	files=`find . -maxdepth 1 -type f`
	echo "binary files for signing after removing third party libs" $files
fi

if [ $bin_pkg == "app" ]; then
	files=`find . -maxdepth 1 -type d`
	echo "binary apps for signing " $files
	echo "coying all files "
	cp -r $files $signing_dir/sign_in/

	cd $signing_dir/sign_in/
	files=`find . -maxdepth 1 -type d`
	echo "binary apps for signing..." $files
fi


if [ $bin_pkg == "pkg" ]; then
	files=`find . -maxdepth 1 -type f`
	echo "binary files for signing " $files
	echo "coying all files "
	cp $files $signing_dir/sign_in/
	
	cd $signing_dir/sign_in/
	files=`find . -maxdepth 1 -type f`
	echo "binary files for signing..." $files
fi

touch $signing_dir/sign_in/$sign_ready_sig

counter=1
sleep_time=160
sign_success=0

while [ $counter -lt 4 ]
do
    echo "signing attempt $counter"
    sleep $sleep_time    
    success=1
    for file in $files
    do
	if [ ! -f $signing_dir/sign_out/$file -a ! -d $signing_dir/sign_out/$file ]; then
	    echo "Signing is not done for file $signing_dir/sign_out/$file"
            success=0;
	else
	    echo "Signing is done for file $signing_dir/sign_out/$file"
	fi
    done
            
    if [ ! -f $signing_dir/sign_out/$sign_done_sig ]; then
	success=0
    fi

    if [ $success -eq 1 ]; then
        echo "Signing is done for all files."
	sign_success=1
	counter=5
    fi

    counter=`expr $counter + 1`

done

$signing_dir/sign_out/$sign_done_sig

if [ $sign_success -eq 1 ]; then
    echo "all binaries are signed successfully."
    exit 0
else
 
   echo "all binaries are not signed yet. see the detailed log for more info."
   exit 1
fi

