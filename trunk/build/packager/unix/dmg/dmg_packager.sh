#!/bin/bash

#########################################
# Don't invoke this script directly, it must be invoked via ma_packager.sh
#########################################

HasCRLFHenceExit()
{
    error=0
    for i in `find $1`
    do
		file $i | cut -d: -f2 | grep -e "\([Ss]cript\)\|\(ASCII..*text\)" >/dev/null
      	if (( $? == 0 )); then
	  	str1=`strings -1 $i | head -1`
	  	str2=`cat $i | head -1`
	  	if [ "$str1" != "$str2" ]; then
	    	echo $i 
	    	error=1
	  	fi
      	fi
    done
    if (( $error != 0 )); then
	echo "Have CRLF in Text file $i" 
	file $i
	#Sdas to fix
	#exit 4;
    fi
}

CURRENT_DIR=`echo $0 | grep ".dmg_packager.sh" | sed -e 's/.dmg_packager.sh//g'`
echo "CURRENT_DIR =" $CURRENT_DIR

# If required arch_bit can be added to OUTPUT_DIR
OUTPUT_DIR=$OUTPUT_DIR/$CONFIG_TYPE

echo Output dir is $OUTPUT_DIR

if [ ! -d $OUTPUT_DIR ];
then
    echo $OUTPUT_DIR directory doesnt exist, creating
    mkdir -p $OUTPUT_DIR
fi

if [ ! -d $SIGN_DIR ]; then
    mkdir -p $SIGN_DIR
    if [ $? != 0 ]; then
        echo "Invalid signing directory $SIGN_DIR"
        exit 1
    fi     
fi
mkdir -p $SIGN_DIR/sign_in
mkdir -p $SIGN_DIR/sign_out
rm -f $SIGN_DIR/sign_in/*
rm -f $SIGN_DIR/sign_out/*

if [ x"$ARCH_BIT" == x"32" ]
then
SUFFIX_DIR=$CONFIG_TYPE
MA_DEPS=
TARGET=i686
else
SUFFIX_DIR=$CONFIG_TYPE/$ARCH_BIT
MA_DEPS=
TARGET=x86_64
fi

# MA binaries Path
MA_TARGETS_DIR=$MA_BASE/build/makefiles/$SUFFIX_DIR/
echo MA binaries Path $MA_TARGETS_DIR
if [ ! -e $MA_TARGETS_DIR ]; then
	echo MA targets dir $MA_TARGETS_DIR not found
	exit 1
fi

MA_SIG_FILE=$MA_BASE/build/packager/data/ma_msgbus_auth.sig
echo Sig file path is $MA_SIG_FILE
if [ -f $MA_SIG_FILE ]; then
        echo ma_msgbus_auth.sig file found
else
        echo $MA_SIG_FILE not found, Warning continuing without it by touching it
        touch $MA_SIG_FILE
fi

echo copying $MA_SIG_FILE to $MA_TARGETS_DIR for packaging
cp $MA_SIG_FILE $MA_TARGETS_DIR


# MA Tools binaries Path  
MA_TOOLS_TARGETS_DIR=$MA_TOOLS
echo MA tools binaries Path $MA_TOOLS_TARGETS_DIR
if [ ! -e $MA_TOOLS_TARGETS_DIR ]; then
	echo MA Tools targets dir $MA_TOOLS_TARGETS_DIR not found
	exit 1
fi

DSTROOT=$OUTPUT_DIR/DSTROOT

if [ -d $DSTROOT ]; then
	rm -rf $DSTROOT
fi
    
echo "Creating DSTROOT directory at $DSTROOT"
mkdir -p $DSTROOT

source $CURRENT_DIR/../ma_packager.env

# Creating install, data and config dirs in build root
DIRS_LIST=`cat $CURRENT_DIR/../ma_install_files | egrep  'dir:|dir_compat:' | cut -d: -f2`
for dir in `eval echo $DIRS_LIST`
do
	echo Creating $DSTROOT/$dir
	mkdir -p $DSTROOT/$dir
done

mkdir -p $DSTROOT/var/tmp/.msgbus
#### Copy the mue resource files
mkdir -p $DSTROOT/$MA_INSTALL_PATH/lib/0409
mkdir -p $DSTROOT/$MA_INSTALL_PATH/lib/0407
mkdir -p $DSTROOT/$MA_INSTALL_PATH/lib/0411
mkdir -p $DSTROOT/$MA_INSTALL_PATH/lib/040C
mkdir -p $DSTROOT/$MA_INSTALL_PATH/lib/040A

cp $MA_BASE/build/packager/unix/dmg/mue/MueRes* $DSTROOT/$MA_INSTALL_PATH/lib/0409
cp $MA_BASE/build/packager/unix/dmg/mue/0407/MueRes* $DSTROOT/$MA_INSTALL_PATH/lib/0407
cp $MA_BASE/build/packager/unix/dmg/mue/0411/MueRes* $DSTROOT/$MA_INSTALL_PATH/lib/0411
cp $MA_BASE/build/packager/unix/dmg/mue/040C/MueRes* $DSTROOT/$MA_INSTALL_PATH/lib/040C
cp $MA_BASE/build/packager/unix/dmg/mue/040A/MueRes* $DSTROOT/$MA_INSTALL_PATH/lib/040A


if [ -z "$ENCRYPT_DIR" ]; then 
	echo "Already encrypted mcs sripts from source tree"
	# Copying update script file
	if [ -e $MA_BASE/src/compat/mue/common/mue/scripts/MSA/encrypted/UpdateMain.McS ]; then
		echo Copying file $MA_BASE/src/compat/mue/common/mue/scripts/MSA/encrypted/UpdateMain.McS to $DSTROOT/$MA_DATA_PATH/update/UpdateMain.McS
		cp $MA_BASE/src/compat/mue/common/mue/scripts/MSA/encrypted/UpdateMain.McS $DSTROOT/$MA_DATA_PATH/update/UpdateMain.McS
	fi

	# Copying Install script file
	if [ -e $MA_BASE/src/compat/mue/common/mue/scripts/MSA/encrypted/InstallMain.McS ]; then
		echo Copying file $MA_BASE/src/compat/mue/common/mue/scripts/MSA/encrypted/InstallMain.McS to $DSTROOT/$MA_DATA_PATH/update/InstallMain.McS
		cp $MA_BASE/src/compat/mue/common/mue/scripts/MSA/encrypted/InstallMain.McS $DSTROOT/$MA_DATA_PATH/update/InstallMain.McS
	fi

else
	echo "Encrypted mcs sripts from build room"
		# Copying update script file
	if [ -e $ENCRYPT_DIR/UpdateMain.McS ]; then
		echo Copying file $ENCRYPT_DIR/UpdateMain.McS to $DSTROOT/$MA_DATA_PATH/update/UpdateMain.McS
		cp $ENCRYPT_DIR/UpdateMain.McS $DSTROOT/$MA_DATA_PATH/update/UpdateMain.McS
	fi

	# Copying Install script file
	if [ -e $ENCRYPT_DIR/InstallMain.McS ]; then
		echo Copying file $ENCRYPT_DIR/InstallMain.McS to $DSTROOT/$MA_DATA_PATH/update/InstallMain.McS
		cp $ENCRYPT_DIR/InstallMain.McS $DSTROOT/$MA_DATA_PATH/update/InstallMain.McS
	fi
fi

#creating /Library/LaunchDaemons/ in DSTROOT
mkdir -p $DSTROOT/Library/StartupItems/
mkdir -p $DSTROOT/Library/LaunchDaemons/

# Copying data files into build root
FILES_LIST=`cat $CURRENT_DIR/../ma_install_files | grep data: | cut -d: -f2`
for file in `eval echo $FILES_LIST` 
do
	echo Copying $CURRENT_DIR/../../data/$file $DSTROOT/$MA_DATA_PATH/data/$file
	cp $CURRENT_DIR/../../data/$file $DSTROOT/$MA_DATA_PATH/data/$file
done

# Copying certs files into build root
FILES_LIST=`cat $CURRENT_DIR/../ma_install_files | grep certs: | cut -d: -f2`
for file in `eval echo $FILES_LIST`
do
        echo Copying $MA_BASE/build/packager/data/certs/public/$file $DSTROOT/$MA_DATA_PATH/certstore/$file
        cp $MA_BASE/build/packager/data/certs/public/$file $DSTROOT/$MA_DATA_PATH/certstore/$file
done


# Copying config files into build root
FILES_LIST=`cat $CURRENT_DIR/../ma_install_files | grep config: | cut -d: -f2`
for file in `eval echo $FILES_LIST` 
do
	echo Copying $CURRENT_DIR/../data/$file $DSTROOT/$MA_CONFIG_PATH/$MA_SOFTWAREID/$file
	cp $CURRENT_DIR/../data/$file $DSTROOT/$MA_CONFIG_PATH/$MA_SOFTWAREID/$file
done

# Copying config files into build root
SCRIPT_LIST=`cat $CURRENT_DIR/../ma_install_files | grep script: | cut -d: -f2`
for file in `eval echo $SCRIPT_LIST` 
do
	echo Copying $CURRENT_DIR/../scripts/$PLATFORM/$file $DSTROOT/$$MA_INSTALL_PATH/scripts/$file
	cp $CURRENT_DIR/../scripts/$PLATFORM/$file $DSTROOT/$MA_INSTALL_PATH/scripts/$file
done
cp $CURRENT_DIR/uninstall.sh $DSTROOT/$MA_INSTALL_PATH/scripts/uninstall.sh
cp $CURRENT_DIR/ma $DSTROOT/$MA_INSTALL_PATH/scripts/ma
cp $CURRENT_DIR/cma.sh $DSTROOT/$MA_INSTALL_PATH/scripts/cma.sh

# Copying all ma install bin files into build root
BINS_LIST=`cat $CURRENT_DIR/../ma_install_files | egrep 'bin:|bin_compat:' | cut -d: -f2`
for file in `eval echo $BINS_LIST`
do
        if [ -e $MA_TARGETS_DIR/$file ]; then
                echo Copying file $MA_TARGETS_DIR/$file to $DSTROOT/$MA_INSTALL_PATH/bin/$file
                cp $MA_TARGETS_DIR/$file $DSTROOT/$MA_INSTALL_PATH/bin/$file
        fi
done

# Copying all ma install lib files into build root
LIBS_LIST=`cat $CURRENT_DIR/../ma_install_files | egrep 'lib:|lib_compat:' | cut -d: -f2 | cut -d. -f1`
for file in `eval echo $LIBS_LIST` 
do
	MAJ_VER=`echo $MA_VERSION | cut -d. -f1`
	MIN_VER=`echo $MA_VERSION | cut -d. -f2`
	if [ -e $MA_TARGETS_DIR/$file.$MAJ_VER.$MIN_VER.$LIB_EXTENSION ]; then
		echo Copying file  $MA_TARGETS_DIR/$file.$MAJ_VER.$MIN_VER.$LIB_EXTENSION to $DSTROOT/$MA_INSTALL_PATH/lib/$file.$MAJ_VER.$MIN_VER.$LIB_EXTENSION
		cp $MA_TARGETS_DIR/$file.$MAJ_VER.$MIN_VER.$LIB_EXTENSION $DSTROOT/$MA_INSTALL_PATH/lib/$file.$MAJ_VER.$MIN_VER.$LIB_EXTENSION
		cd $DSTROOT/$MA_INSTALL_PATH/lib/
		ln -sf $file.$MAJ_VER.$MIN_VER.$LIB_EXTENSION $file.$MAJ_VER.$LIB_EXTENSION
		ln -sf $file.$MAJ_VER.$LIB_EXTENSION $file.$LIB_EXTENSION
		cd $CURRENT_DIR
	fi
done

#To fix BUG#953552
cd $DSTROOT/$MA_INSTALL_PATH/lib/
ln -sf libeventwriter.dylib libeventwriter.4.dylib
cd $CURRENT_DIR

# Creating links in rsdk
RSDK_LIST=`cat $CURRENT_DIR/../ma_install_files | grep rsdk: | cut -d: -f2 | cut -d. -f1`
for file in `eval echo $RSDK_LIST` 
do
	MAJ_VER=`echo $MA_VERSION | cut -d. -f1`
	MIN_VER=`echo $MA_VERSION | cut -d. -f2`
	if [ -e $MA_TARGETS_DIR/$file.$MAJ_VER.$MIN_VER.$LIB_EXTENSION ]; then
		echo Copying file  $MA_TARGETS_DIR/$file.$MAJ_VER.$MIN_VER.$LIB_EXTENSION to $DSTROOT/$MA_INSTALL_PATH/lib/rsdk/$file.$MAJ_VER.$MIN_VER.$LIB_EXTENSION
		cp $MA_TARGETS_DIR/$file.$MAJ_VER.$MIN_VER.$LIB_EXTENSION $DSTROOT/$MA_INSTALL_PATH/lib/rsdk/$file.$MAJ_VER.$MIN_VER.$LIB_EXTENSION
		cd $DSTROOT/$MA_INSTALL_PATH/lib/rsdk
		ln -sf $file.$MAJ_VER.$MIN_VER.$LIB_EXTENSION $file.$MAJ_VER.$LIB_EXTENSION
		ln -sf $file.$MAJ_VER.$LIB_EXTENSION $file.$LIB_EXTENSION
		cd $CURRENT_DIR
	fi
done


# Copying all ma install sig files into build root
SIGS_LIST=`cat $CURRENT_DIR/../ma_install_files | grep sig: | cut -d: -f2`
for file in `eval echo $SIGS_LIST` 
do
	if [ -e $MA_TARGETS_DIR/$file ]; then
		echo Copying file $MA_TARGETS_DIR/$file to $DSTROOT/$MA_INSTALL_PATH/bin/$file
		cp $MA_TARGETS_DIR/$file $DSTROOT/$MA_INSTALL_PATH/bin/$file
	fi
done

# Copying all ma tools lib files into build root
TOOLS_LIST=`cat $CURRENT_DIR/../ma_install_tools | grep Darwin | grep -v ^#`
for file in `eval echo $TOOLS_LIST` 
do
	lib_dir=`echo $file | cut -d: -f2`
	lib_name=`echo $file | cut -d: -f3`

	if [ "$lib_dir" == "mfecryptc" -a "$CRYPTO" == "openssl" ]; then
                lib_dir=mfecryptc_openssl
        fi

        if [ "$lib_dir" == "curl" -a "$CRYPTO" == "openssl" ]; then
                lib_dir=curl_openssl
        fi

	if [ -e $MA_TOOLS_TARGETS_DIR/$lib_dir/lib/$SUFFIX_DIR/$lib_name ]; then
		field_three=`echo $lib_name | cut -d. -f3`
		major_ver=`echo $lib_name | cut -d. -f2`
		file_name=`echo $lib_name | cut -d. -f1-1`
		echo Copying file $MA_TOOLS_TARGETS_DIR/$lib_dir/lib/$SUFFIX_DIR/$lib_name to $DSTROOT/$MA_INSTALL_PATH/lib/tools/$lib_name
		cp $MA_TOOLS_TARGETS_DIR/$lib_dir/lib/$SUFFIX_DIR/$lib_name $DSTROOT/$MA_INSTALL_PATH/lib/tools/$lib_name
		cd $DSTROOT/$MA_INSTALL_PATH/lib/tools
		if [ $field_three != $LIB_EXTENSION ];then
			ln -sf $lib_name $file_name.$major_ver.$LIB_EXTENSION
		fi
		ln -sf $file_name.$major_ver.$LIB_EXTENSION $file_name.$LIB_EXTENSION
		cd $CURRENT_DIR
	fi

	if [ -e $MA_TOOLS_TARGETS_DIR/$lib_dir/bin/$SUFFIX_DIR/$lib_name ]; then
		if [ -f $MA_TOOLS_TARGETS_DIR/$lib_dir/bin/$SUFFIX_DIR/$lib_name ]; then
			echo Copying file $MA_TOOLS_TARGETS_DIR/$lib_dir/bin/$SUFFIX_DIR/$lib_name to $DSTROOT/$MA_INSTALL_PATH/bin/$lib_name
			cp $MA_TOOLS_TARGETS_DIR/$lib_dir/bin/$SUFFIX_DIR/$lib_name $DSTROOT/$MA_INSTALL_PATH/bin/$lib_name
			chmod +x $DSTROOT/$MA_INSTALL_PATH/bin/$lib_name
		fi
	fi

done

###################################################################
#COMPAT library packaging

#config.xml files
if [ -e $CURRENT_DIR/../data/cmnupd_config.xml ]; then
	echo Copying $CURRENT_DIR/../data/data/cmnupd_config.xml $DSTROOT/$MA_CONFIG_PATH/$MA_CMN_UPDATERID/config.xml
	cp $CURRENT_DIR/../data/cmnupd_config.xml $DSTROOT/$MA_CONFIG_PATH/$MA_CMN_UPDATERID/config.xml
fi

#if [ -e $CURRENT_DIR/../data/cma_config.xml ]; then
#	str="s:__MA_VERSION__:$MA_VERSION:g;s:__PACKAGE_NUMBER__:$PACKAGE_NUM:g;s:__MA_SOFTWARE_ID__:$CMA_SOFTWAREID:g;s:__MA_INSTALL_PATH__:$MA_INSTALL_PATH:g;s:__MA_UPDATER_PATH__:$MA_UPDATER_PATH:g;s:__MA_SHARED_LIB_PATH__:$MA_CMA_LPC_LIBS_PATH:g"
#	sed -e "$str" $CURRENT_DIR/../data/cma_config.xml > $DSTROOT/$MA_CONFIG_PATH/$CMA_SOFTWAREID/config.xml
#fi

# Copying all ma tools lib files into build root
TOOLS_LIST=`cat $CURRENT_DIR/../ma_compat_install_tools | grep Darwin | grep -v ^#`
for file in `eval echo $TOOLS_LIST` 
do
	lib_name=`echo $file | cut -d: -f3`
	if [ -e $BOOST_BASE/lib/$lib_name ]; then
		echo Copying file $BOOST_BASE/lib/$lib_name to $DSTROOT/$MA_INSTALL_PATH/lib/tools/$lib_name
		cp $BOOST_BASE/lib/$lib_name $DSTROOT/$MA_INSTALL_PATH/lib/tools/$lib_name		
	fi
done

# RSA files copying into tools folder, 
if [ "$CRYPTO" == "openssl" ]; then
	cp $MA_TOOLS_TARGETS_DIR/mfecryptc_openssl/lib/$SUFFIX_DIR/libcrypto.1.0.0.dylib  $DSTROOT/$MA_INSTALL_PATH/lib/tools/libcrypto.1.0.0.dylib
	cp $MA_TOOLS_TARGETS_DIR/mfecryptc_openssl/lib/$SUFFIX_DIR/libssl.1.0.0.dylib  $DSTROOT/$MA_INSTALL_PATH/lib/tools/libssl.1.0.0.dylib
	CUR_DIR=`pwd`
	cd $DSTROOT/$MA_INSTALL_PATH/lib/tools
	ln -sf libcrypto.1.0.0.dylib libcrypto.1.dylib
	ln -sf libssl.1.0.0.dylib libssl.1.dylib
	cd $CUR_DIR
else
	cp $MA_TOOLS_TARGETS_DIR/mfecryptc/lib/$SUFFIX_DIR/libcryptocme.so  $DSTROOT/$MA_INSTALL_PATH/lib/tools/
	cp $MA_TOOLS_TARGETS_DIR/mfecryptc/lib/$SUFFIX_DIR/libcryptocme.sig  $DSTROOT/$MA_INSTALL_PATH/lib/tools/
	cp $MA_TOOLS_TARGETS_DIR/mfecryptc/lib/$SUFFIX_DIR/libccme_base_non_fips.so  $DSTROOT/$MA_INSTALL_PATH/lib/tools/
	cp $MA_TOOLS_TARGETS_DIR/mfecryptc/lib/$SUFFIX_DIR/libccme_base.so $DSTROOT/$MA_INSTALL_PATH/lib/tools/
	cp $MA_TOOLS_TARGETS_DIR/mfecryptc/lib/$SUFFIX_DIR/libccme_asym.so  $DSTROOT/$MA_INSTALL_PATH/lib/tools/
	#rm libsqlite3.0.dylib libsqlite3.dylib
fi

changeReferences()
{
    echo "testing ma tools references "
    otool -L $1
    install_name_tool -change libz.1.dylib /Library/mileaccess/agent/lib/tools/libz.1.dylib $1
    install_name_tool -change libuv.0.10.dylib /Library/mileaccess/agent/lib/tools/libuv.0.10.dylib $1
    install_name_tool -change libtrex.1.3.dylib /Library/mileaccess/agent/lib/tools/libtrex.1.3.dylib $1
    install_name_tool -change libsqlite3.0.dylib /Library/mileaccess/agent/lib/tools/libsqlite3.0.dylib $1
    install_name_tool -change libmxml.dylib /Library/mileaccess/agent/lib/tools/libmxml.dylib $1
	install_name_tool -change libmsgpackc.2.dylib /Library/mileaccess/agent/lib/tools/libmsgpackc.2.dylib $1
    install_name_tool -change libmfecryptc.5.0.dylib /Library/mileaccess/agent/lib/tools/libmfecryptc.5.0.dylib $1
    install_name_tool -change libini.1.dylib /Library/mileaccess/agent/lib/tools/libini.1.dylib $1
	install_name_tool -change libcurl.4.dylib /Library/mileaccess/agent/lib/tools/libcurl.4.dylib $1
    install_name_tool -change libtntnet.12.dylib  /Library/mileaccess/agent/lib/tools/libtntnet.12.dylib $1
    install_name_tool -change libtntnet_sdk.12.dylib /Library/mileaccess/agent/lib/tools/libtntnet_sdk.12.dylib $1
    install_name_tool -change libcxxtools-bin.9.dylib /Library/mileaccess/agent/lib/tools/libcxxtools-bin.9.dylib $1
    install_name_tool -change libcxxtools-http.9.dylib /Library/mileaccess/agent/lib/tools/libcxxtools-http.9.dylib $1
    install_name_tool -change libcxxtools-json.9.dylib /Library/mileaccess/agent/lib/tools/libcxxtools-json.9.dylib $1
    install_name_tool -change libcxxtools-unit.9.dylib /Library/mileaccess/agent/lib/tools/libcxxtools-unit.9.dylib $1
    install_name_tool -change libcxxtools-xmlrpc.9.dylib /Library/mileaccess/agent/lib/tools/libcxxtools-xmlrpc.9.dylib $1
    install_name_tool -change libcxxtools.9.dylib        /Library/mileaccess/agent/lib/tools/libcxxtools.9.dylib $1
    otool -L $1
}

#for i in `ls  $DSTROOT/Library/mileaccess/agent/lib/lib* $DSTROOT/Library/mileaccess/agent/lib/rsdk/lib* $DSTROOT/Library/mileaccess/agent/lib/tools/lib*`
for i in `ls  $DSTROOT/Library/mileaccess/agent/lib/tools/lib* $DSTROOT/Library/mileaccess/agent/lib/lib* $DSTROOT/Library/mileaccess/agent/lib/rsdk/lib* `
do
		echo changing reference for $i
        changeReferences $i
done


# Update placeholders in config.xml
#MA_START_CMD="SystemStarter start ma"
#MA_STOP_CMD="SystemStarter stop ma"
MA_START_CMD="/Library/mileaccess/agent/scripts/ma start"
MA_STOP_CMD="/Library/mileaccess/agent/scripts/ma stop"
MA_UNINSTALL_CMD="/Library/mileaccess/agent/scripts/uninstall.sh"
str="s:__MA_UPDATER_PATH__:$MA_UPDATER_PATH:g;s:__MA_SOFTWARE_ID__:$MA_SOFTWAREID:g;s:__MA_VERSION__:$MA_VERSION:g;s:__PACKAGE_NUMBER__:$PACKAGE_NUM:g;s:__MA_DATA_PATH__:$MA_DATA_PATH:g;s:__MA_INSTALL_PATH__:$MA_INSTALL_PATH:g;s:__MA_START_CMD__:$MA_START_CMD:g;s:__MA_STOP_CMD__:$MA_STOP_CMD:g;s:__MA_UNINSTALL_CMD__:$MA_UNINSTALL_CMD:g;s:__MA_SHARED_LIB_PATH__:$MA_CMA_LPC_LIBS_PATH:g"
sed -e "$str" $CURRENT_DIR/../data/config.xml > $DSTROOT/$MA_CONFIG_PATH/$MA_SOFTWAREID/config.xml

str="s:__MA_VERSION__:$MA_VERSION:g;s:__PACKAGE_NUMBER__:$PACKAGE_NUM:g;s:__MA_INSTALL_PATH__:$MA_INSTALL_PATH:g;s:__MA_DATA_PATH__:$MA_DATA_PATH:g"
sed -e "$str" $CURRENT_DIR/../data/mainfo.ini > $DSTROOT/$MA_CONFIG_PATH/mainfo.ini
cp $CURRENT_DIR/../data/msaconfig $DSTROOT/$MA_INSTALL_PATH/bin/

									##  jen : TODO copy macosx/install.sh 
str="s:__MA_SOFTWARE_ID__:$MA_SOFTWAREID:g;s:__MA_VERSION__:$MA_VERSION:g;s:__PACKAGE_NUMBER__:$PACKAGE_NUM:g;s:__MA_DATA_PATH__:$MA_DATA_PATH:g;s:__MA_INSTALL_PATH__:$MA_INSTALL_PATH:g;s:__MA_MSGBUS_DATA_PATH__:$MA_MSGBUS_DATA_PATH:g"
#sed -e "$str" $CURRENT_DIR/../scripts/$PLATFORM/ma > $DSTROOT/$MA_INSTALL_PATH/scripts/ma

cp -pRf $CURRENT_DIR/Resources $OUTPUT_DIR

echo copying launchd scripts
cp -rf $CURRENT_DIR/StartupItems/* $DSTROOT/Library/StartupItems/
cp -rf $CURRENT_DIR/LaunchDaemons/* $DSTROOT/Library/LaunchDaemons/

echo copying pkgproj
cp $CURRENT_DIR/ma.pkgproj  $OUTPUT_DIR
cp $CURRENT_DIR/ma_raw.pkgproj $OUTPUT_DIR

MAJ_VER=`echo $MA_VERSION | cut -d. -f1`
MIN_VER=`echo $MA_VERSION | cut -d. -f2`
PATCH_VER=`echo $MA_VERSION | cut -d. -f3`
sed -e "s/SubstituteMajorVersionHere/$MAJ_VER/" -e "s/SubstituteMinorVersionHere/$MIN_VER/" -e "s/SubstitutePatchVersionHere/$PATCH_VER/" -e "s/SubstituteBuildNumberHere/$PACKAGE_NUM/" $CURRENT_DIR/ma_preinstall.template > $OUTPUT_DIR/ma_preinstall

str="s:__MA_INSTALL_PATH__:$MA_INSTALL_PATH:g;s:__CMA_INSTALL_PATH__:$CMA_INSTALL_PATH:g;s:__CMA_SOFTWAREID__:$CMA_SOFTWAREID:g;s:__CMA_CONFIG_PATH__:$CMA_CONFIG_PATH:g;s:__MA_CMA_LPC_LIBS_PATH__:$MA_CMA_LPC_LIBS_PATH:g;s:__MA_DATA_PATH__:$MA_DATA_PATH:g;s:__MA_CONFIG_PATH__:$MA_CONFIG_PATH:g;s:__MA_SOFTWARE_ID__:$MA_SOFTWAREID:g;s:__MA_MSGBUS_DATA_PATH__:$MA_MSGBUS_DATA_PATH:g"
sed -e "$str" $CURRENT_DIR/ma_postinstall > $OUTPUT_DIR/ma_postinstall

mkdir -p $OUTPUT_DIR/build

echo Checking CRLF
HasCRLFHenceExit $DSTROOT

cd  $DSTROOT
if (( $? != 0 )); then
     echo "packaging Failed. Exiting..."
     exit 1
fi

echo changing permissions for files 
for i in `find . -type f`
do 
  chmod 700 $i
done

echo changing permissions for directories
for i in `find . -type d`
do 
  chmod 755 $i
done
cd  -


echo creating package

echo Listing out dstroot
ls -al $DSTROOT


########### Signing binaries ##########

## TODO : libmfelpc bundle, Enable signing in buildroom

#bash $CURRENT_DIR/binary_sign.sh $DSTROOT/$MA_INSTALL_PATH/bin $SIGN_DIR "bin"

if [ $? != 0 ]; then
    echo "Binaries signing failed"
#    exit 1;
fi

cp -f $SIGN_DIR/sign_out/* $DSTROOT/$MA_INSTALL_PATH/bin
rm -f $DSTROOT/$MA_INSTALL_PATH/bin/sign_*

########### create packages ###########

echo change dir to $OUTPUT_DIR
cd $OUTPUT_DIR
mkdir -p $OUTPUT_DIR/build 
rm -f $OUTPUT_DIR/build/*

# TODO : Add Startup items in cma_raw.pkgproj

echo now build raw packages.
/usr/local/bin/packagesbuild  -v ma_raw.pkgproj

if [ ! -f $OUTPUT_DIR/build/ma.pkg ]; then
   echo "failed to create agent raw pkg"
   exit 1
fi
rm -f $OUTPUT_DIR/ma_raw.pkg
cp $OUTPUT_DIR/build/ma.pkg $OUTPUT_DIR/cma_raw.pkg

########## create dist packages #########
echo change dir to $OUTPUT_DIR
cd $OUTPUT_DIR
mkdir -p $OUTPUT_DIR/dist
rm -f $OUTPUT_DIR/dist/*

echo now build the dist package.
/usr/local/bin/packagesbuild  -v ma.pkgproj
if [ ! -f $OUTPUT_DIR/dist/ma.pkg ]; then
    echo "failed to create agent distribution pkg"
    exit 1
fi

############# Signing packages ###########################
if [ -z "$SIGN_DIR" ]; then 
echo "Siging directory not specified"
else
echo "signing_dir=" $SIGN_DIR
bash $CURRENT_DIR/binary_sign.sh $OUTPUT_DIR/dist/ $SIGN_DIR "pkg"
if [ $? != 0 ]; then
    echo "Package signing failed"
   exit 1
fi

rm -f $OUTPUT_DIR/ma.pkg
cp $SIGN_DIR/sign_out/ma.pkg $OUTPUT_DIR/ma.pkg

############  temp overriding signing 

#cp $OUTPUT_DIR/dist/ma.pkg $OUTPUT_DIR/ma.pkg

fi

###########################################################
#create disk image
cd  $OUTPUT_DIR
if (( $? != 0 )); then
     echo "packaging Failed. Exiting..."
     exit 1
fi

#check for the disk size required
disksize=`du -sk $OUTPUT_DIR/ma.pkg | cut -f1`
echo "Calculated required disk size using du command is, $disksize"

disksize=$(( (disksize + 1123)/1024 ))
if (( $disksize < 7 )); then
    disksize=10
else
    disksize=`expr $disksize + 2`
fi
echo "Used disk size is: $disksize"

# create a 10MB disk image

/usr/bin/hdiutil create -size "$disksize"m ma10mb.dmg -layout NONE
if (( $? != 0 )); then
    echo "Unable to Create ma10mb.dmg exiting..."
    exit 1
fi
# associate a device with this but don't mount it
MYDEV=$(/usr/bin/hdid -nomount ma10mb.dmg)
# create a file system
/sbin/newfs_hfs -v MFEMA $MYDEV
if (( $? != 0 )); then
    echo "Unable to Create filesystem on ma10mb.dmg exiting..."
    exit 1
fi
# diassociate device
/usr/bin/hdiutil eject $MYDEV
# mount it
MNTNAME=`/usr/bin/hdid ma10mb.dmg`
MNTF2=`echo $MNTNAME | cut -d' ' -f2`
MNTF3=`echo $MNTNAME | cut -d' ' -f3`
echo F2 $MNTF2
echo F3 $MNTF3
echo Mount name $MNTNAME

if [ ! -z "$MNTF3" ]; then
    MNTDIR="$MNTF2\ $MNTF3"
else
    MNTDIR="$MNTF2"
fi
echo "Created Mount $MNTDIR" 
if (( $? != 0 )); then
    echo "Mounting ma10mb.dmg failed. exiting..."
    exit 1
fi
# copy stuff to it
/bin/cp -R ma.pkg $MNTDIR
echo /bin/cp -R ma.pkg "$MNTDIR"
if (( $? != 0 )); then
    /usr/bin/hdiutil eject $MYDEV
    echo "Copying package failed. exiting..."
    exit 1
fi

# eject
/usr/bin/hdiutil eject $MYDEV
if (( $? != 0 )); then
    echo "Unable to eject device ma10mb.dmg. exiting..."
    exit 1
fi
rm $OUTPUT_DIR/ma.dmg
# compress it and make it read only
/usr/bin/hdiutil convert -format UDZO ma10mb.dmg -o ma.dmg
if (( $? != 0 )); then
    echo "Compressing ma10mb.dmg to cma.dmg failed. exiting..."
    exit 1
fi

rm -rf $OUTPUT_DIR/ma10mb.dmg $OUTPUT_DIR/dstroot $OUTPUT_DIR/Resources $OUTPUT_DIR/Info.plist $OUTPUT_DIR/CMA.Info

###################################
#bootstrap  bootstrap_sfx.sh  PackageInfo.xml  BootstrapInfo.xml 
cp -rf $MA_TARGETS_DIR/mileaccessSmartInstall.app $OUTPUT_DIR
/usr/bin/ditto -c -k --sequesterRsrc --keepParent  $OUTPUT_DIR/mileaccessSmartInstall.app  $OUTPUT_DIR/mileaccessSmartInstall.app.zip
/usr/bin/ditto -c -k --sequesterRsrc --keepParent  $OUTPUT_DIR/mileaccessSmartInstall.app  $OUTPUT_DIR/bootstrap
cp $MA_BASE/build/packager/unix/data/BootstrapInfo.xml $OUTPUT_DIR
cp $MA_BASE/build/packager/unix/data/PackageInfo.xml $OUTPUT_DIR

bootstrap_size=`stat -f %z  $OUTPUT_DIR/bootstrap`
str="s/__BOOSTRAP_SIZE__/$bootstrap_size/"
sed -e "{$str}" $MA_BASE/build/packager/unix/bootstrap_sfx/bootstrap_sfx.osx.sh.template > $OUTPUT_DIR/bootstrap_sfx.sh
###################################
#PkgCatalog changes for mac
sed -e "s:<InstallCommand>AgentInst.McS</InstallCommand>:<InstallCommand>setup</InstallCommand>:g" $MA_BASE/build/packager/data/PkgCatalog_unx.xml > $OUTPUT_DIR/PkgCatalog_unx.xml
sed -e "s:<InstallType>script</InstallType>:<InstallType>command</InstallType>:g" $OUTPUT_DIR/PkgCatalog_unx.xml > $OUTPUT_DIR/PkgCatalog.xml
rm -rf $OUTPUT_DIR/PkgCatalog_unx.xml

#moving PkgCatalog  to build out
#cp $MA_BASE/build/packager/data/PkgCatalog_unx.xml $OUTPUT_DIR/PkgCatalog.xml

# Creating SDK tar file (sdk and beacon rpm files) in output folder
SDK_DIR=/tmp/sdk_temp
if [ -e $SDK_DIR ]
then
rm -fr $SDK_DIR
fi
mkdir -p $SDK_DIR
mkdir -p $SDK_DIR/samples
mkdir -p $SDK_DIR/include
mkdir -p $SDK_DIR/bindings
mkdir -p $SDK_DIR/lib/$SUFFIX_DIR
mkdir -p $SDK_DIR/rsdk/$SUFFIX_DIR
mkdir -p $SDK_DIR/masign

SAMPLES_DIR=$SDK_DIR/samples
cp -r $MA_BASE/tools/ma_sdk/include/* $SDK_DIR/include
cp -r $MA_BASE/tools/ma_sdk/bindings/* $SDK_DIR/bindings
cp -r $MA_BASE/tools/ma_sdk/lib/$SUFFIX_DIR/* $SDK_DIR/lib/$SUFFIX_DIR
cp -r $MA_BASE/tools/ma_sdk/rsdk/$SUFFIX_DIR/* $SDK_DIR/rsdk/$SUFFIX_DIR
cp -r $MA_TOOLS_TARGETS_DIR/masign/* $SDK_DIR/masign
cp  $MA_BASE/build/packager/data/certs/ma_test_certs.tar $SDK_DIR/masign/

echo "Executing command sh $MA_BASE/tools/beacon/build/packager/unix/bc_packager.sh -v $MA_VERSION -n $PACKAGE_NUM -c $CONFIG_TYPE  -p $PLATFORM -b $ARCH_BIT -s $DATE_AND_TIME -o $SAMPLES_DIR"
sh  $MA_BASE/tools/beacon/build/packager/unix/bc_packager.sh  -v $MA_VERSION -n $PACKAGE_NUM -c $CONFIG_TYPE  -p $PLATFORM -b $ARCH_BIT -s $DATE_AND_TIME -o $SAMPLES_DIR
if (( $? )); then
	echo "Failed to create the beacon Package"
fi

cur_dir=`pwd`
cd $MA_BASE/tools/beacon/
tar cvf $SAMPLES_DIR/beacon.tar build include src
cd $cur_dir

cd $SDK_DIR
tar cvf $OUTPUT_DIR/ma_sdk500_$PLATFORM.tar include lib bindings rsdk samples masign

cd $cur_dir
rm -fr $SDK_DIR

# TODO : If required enable the following lines
#echo "Creating PkgCatlog.xml for deployable package for CMA"
#str="s/PACKAGE_NUMBER/$package/"
#sed -e "$str" $dir/installer/macosx/PkgCatalog.xml > $OUTPUT_DIR/PkgCatalog.xml

# Repo keys later
# cp $dir/data/RepoKeys.ini $OUTPUT_DIR



