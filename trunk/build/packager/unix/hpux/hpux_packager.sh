#!/bin/bash

#########################################
# Don't invoke this script directly, it must be invoked via ma_packager.sh
#########################################

HasCRLFHenceExit()
{
    error=0
    echo "CRLF..."
	for i in `find $1`
    do
      file $i | cut -d: -f2 | grep -w "\([Ss]cript\)\|\(ASCII..*text\)" >/dev/null
      if (( $? == 0 )); then
	  str1=`strings -1 $i | head -1`
	  str2=`cat $i | head -1`
	  if [ "$str1" != "$str2" ]; then
	      echo $i 
	      error=1
	  fi
      fi
    done
    if (( $error != 0 )); then
	echo "Have CRLF in Text files. Hence Exiting..." 
	exit 4;
    fi
}

################################################################
#Step 1: Creating directory and defines
################################################################
echo $'\n\n************* Step 1: Creating directory and defines'
PKG_CURRENT_DIR=`echo $0 | grep ".hpux_packager.sh" | sed -e 's/.hpux_packager.sh//g'`
echo "PKG_CURRENT_DIR =" $PKG_CURRENT_DIR
OUTPUT_DIR=$OUTPUT_DIR/$ARCH_BIT/$CONFIG_TYPE
echo Output dir is $OUTPUT_DIR
if [ ! -d $OUTPUT_DIR ]; then
    echo $OUTPUT_DIR directory doesnt exist, creating
    mkdir -p $OUTPUT_DIR
fi

#### Constants used in PSF spec #####
PSF_DIR_ENTRY="file -m 0755 -o root -g root -t d"
PSF_FILE_ENTRY="file -v -m 0755 -o root -g root"
PSF_BIN_FILE_ENTRY="file -v -m 0744 -o root -g root"
PSF_MFE_FILE_ENTRY="file -v -m 0744 -o mfe -g mfe"
PSF_SIG_FILE_ENTRY="file -v -m 0400 -o root -g root"
PSF_SYMB_LINK_FILE_ENTRY="file -v -o root -g root -t s"

####

SCRIPT_DIR=Linux

echo "Identifying architecture and setting PSFNAME and CHECKINSTALLNAME"
ITANIUM_ARCH="ia64"
architecture=$(uname -m)
if	[ "$architecture" == "$ITANIUM_ARCH" ]	;then
	PSFNAME=$PKG_CURRENT_DIR/product.psf_itanium.template
	TARGET=itanium
	CHECKINSTALLNAME=$PKG_CURRENT_DIR/checkinstall_itanium.template
else
	PSFNAME=$PKG_CURRENT_DIR/product.psf.template
	TARGET=parisc
	CHECKINSTALLNAME=$PKG_CURRENT_DIR/checkinstall.template
fi

echo "Using hpux psf file $PSFNAME"
rm -rf /tmp/ma_hpux_pkg
mkdir /tmp/ma_hpux_pkg
cp $PSFNAME /tmp/ma_hpux_pkg/product.psf
if [ x"$ARCH_BIT" == x"32" ] ;then
	SUFFIX_DIR=$CONFIG_TYPE
	MA_DEPS=MFErt	
fi

# MA binaries Path
MA_TARGETS_DIR=$MA_BASE/build/makefiles/$SUFFIX_DIR/
echo MA binaries Path $MA_TARGETS_DIR
if [ ! -e $MA_TARGETS_DIR ]; then
	echo MA targets dir $MA_TARGETS_DIR not found
	exit 1
fi

MA_SIG_FILE=$MA_BASE/build/packager/data/ma_msgbus_auth.sig
echo Sig file path is $MA_SIG_FILE
if [ -f $MA_SIG_FILE ]; then
	echo ma_msgbus_auth.sig file found
else
	echo $MA_SIG_FILE not found, Warning continuing without it by touching it
	touch $MA_SIG_FILE
fi

echo copying $MA_SIG_FILE to $MA_TARGETS_DIR for packaging 
cp $MA_SIG_FILE $MA_TARGETS_DIR

# MA Tools binaries Path  
MA_TOOLS_TARGETS_DIR=$MA_TOOLS
echo MA tools binaries Path $MA_TOOLS_TARGETS_DIR
if [ ! -e $MA_TOOLS_TARGETS_DIR ]; then
	echo MA Tools targets dir $MA_TOOLS_TARGETS_DIR not found
	exit 1
fi
# MA compat sdk binaries Path  
MA_COMPAT_SDK_TARGETS_DIR=$MA_COMPAT_SDK
echo MA compat sdk binaries Path $MA_COMPAT_SDK_TARGETS_DIR
if [ ! -e $MA_COMPAT_SDK_TARGETS_DIR ]; then
	echo MA compat sdk targets dir $MA_COMPAT_SDK_TARGETS_DIR not found
	exit 1
fi

# build root path
PKG_BUILD_ROOT=/tmp/MFEma-$MA_VERSION-$PACKAGE_NUM-buildroot
echo The current buildroot is $PKG_BUILD_ROOT
if [ -d $PKG_BUILD_ROOT ]; then
	rm -rf $PKG_BUILD_ROOT
fi    
echo Creating the build root at $PKG_BUILD_ROOT
mkdir -p $PKG_BUILD_ROOT

MAJ_VER=`echo $MA_VERSION | cut -d. -f1`
echo $MAJ_VER
MIN_VER=`echo $MA_VERSION | cut -d. -f2`
echo $MIN_VER
SUB_VER=`echo $MA_VERSION | cut -d. -f3`
echo $SUB_VER

# The below env file contains all the required environment variables.
source $PKG_CURRENT_DIR/../ma_packager.env



################################################################
#Step 2: Creating install files and directory in buildroot
################################################################z
echo $'\n\n************* Step 2: Creating install files and directory in buildroot'

echo "Creating the PSF for package "
echo "using  psf file $PSFNAME"
str="s:MAPKGROOT:$PKG_BUILD_ROOT:;s/VERSION_NUMBER/$MA_VERSION/;s/RELEASE_NUMBER/$PACKAGE_NUM/;s/EnterCMASoftwareIdHere/$MA_SOFTWAREID/;s/EnterMajVerHere/$MAJ_VER/g;s/EnterMinVerHere/$MIN_VER/g;s/EnterSubVerHere/$SUB_VER/g;s/EnterUpdaterSoftwareIdHere/$MA_CMN_UPDATERID/g;s/EnterCmdAgentSoftwareIdHere/$CMDAGENT_SOFTWAREID/g"

echo $str
sed -e "$str" $PSFNAME > /tmp/ma_hpux_pkg/product.psf

###  For PSF entries, we need to remove the first '/' symbol
MA_INSTALL_PATH_PSF=`echo $MA_INSTALL_PATH | sed -e "s:^/::g" `
CMA_INSTALL_PATH_PSF=`echo $CMA_INSTALL_PATH | sed -e "s:^/::g" `
CMA_SHARED_PATH_PSF=`echo $CMA_SHARED_PATH | sed -e "s:^/::g" `
MA_CMA_LPC_LIBS_PATH_PSF=`echo $MA_CMA_LPC_LIBS_PATH | sed -e "s:^/::g" `
MA_DATA_PATH_PSF=`echo $MA_DATA_PATH | sed -e "s:^/::g" `
MA_MSGBUS_DATA_PATH_PSF=`echo $MA_MSGBUS_DATA_PATH | sed -e "s:^/::g" `
MA_CONFIG_PATH_PSF=`echo $MA_CONFIG_PATH | sed -e "s:^/::g" `
CMA_CONFIG_PATH_PSF=`echo $CMA_CONFIG_PATH | sed -e "s:^/::g" `
MA_UPDATER_PATH_PSF=`echo $MA_UPDATER_PATH | sed -e "s:^/::g" `

DIRS_LIST=`cat $PKG_CURRENT_DIR/../ma_install_files | egrep  'dir:|dir_compat:' | cut -d: -f2`
for dir in `eval echo $DIRS_LIST`
do
	echo Creating $PKG_BUILD_ROOT/$dir
	mkdir -p $PKG_BUILD_ROOT/$dir
	echo $PSF_DIR_ENTRY $dir >> /tmp/ma_hpux_pkg/product.psf
done

# Copying data files into build root
FILES_LIST=`cat $PKG_CURRENT_DIR/../ma_install_files | grep data: | cut -d: -f2`
for file in `eval echo $FILES_LIST` 
do
	echo Copying $PKG_CURRENT_DIR/../../data/$file $PKG_BUILD_ROOT/$MA_DATA_PATH/data/$file
	cp $PKG_CURRENT_DIR/../../data/$file $PKG_BUILD_ROOT/$MA_DATA_PATH/data/$file
	echo $PSF_FILE_ENTRY $MA_DATA_PATH_PSF/data/$file >> /tmp/ma_hpux_pkg/product.psf
done

# Copying certs files into build root
FILES_LIST=`cat $PKG_CURRENT_DIR/../ma_install_files | grep certs: | cut -d: -f2`
for file in `eval echo $FILES_LIST` 
do
	echo Copying $MA_BASE/build/packager/data/certs/public/$file $PKG_BUILD_ROOT/$MA_DATA_PATH/certstore/$file
	cp $MA_BASE/build/packager/data/certs/public/$file $PKG_BUILD_ROOT/$MA_DATA_PATH/certstore/$file
	echo $PSF_FILE_ENTRY $MA_DATA_PATH_PSF/certstore/$file >> /tmp/ma_hpux_pkg/product.psf
done

# Copying config files into build root
FILES_LIST=`cat $PKG_CURRENT_DIR/../ma_install_files | grep config: | cut -d: -f2`
for file in `eval echo $FILES_LIST` 
do
	echo Copying $PKG_CURRENT_DIR/../data/$file $PKG_BUILD_ROOT/$MA_CONFIG_PATH/$MA_SOFTWAREID/$file
	cp $PKG_CURRENT_DIR/../data/$file $PKG_BUILD_ROOT/$MA_CONFIG_PATH/$MA_SOFTWAREID/$file
	echo $PSF_FILE_ENTRY $MA_CONFIG_PATH_PSF/$MA_SOFTWAREID/$file  >> /tmp/ma_hpux_pkg/product.psf
done

if [ -e $PKG_CURRENT_DIR/../data/cmnupd_config.xml ]; then
	echo Copying $PKG_CURRENT_DIR/../data/cmnupd_config.xml $PKG_BUILD_ROOT/$MA_CONFIG_PATH/$MA_CMN_UPDATERID/config.xml
	cp $PKG_CURRENT_DIR/../data/cmnupd_config.xml $PKG_BUILD_ROOT/$MA_CONFIG_PATH/$MA_CMN_UPDATERID/config.xml
	echo $PSF_FILE_ENTRY $MA_CONFIG_PATH_PSF/$MA_CMN_UPDATERID/config.xml >> /tmp/ma_hpux_pkg/product.psf
fi

#if [ -e $PKG_CURRENT_DIR/../data/cma_config.xml ]; then
#	echo Copying $PKG_CURRENT_DIR/../data/cma_config.xml $PKG_BUILD_ROOT/$MA_CONFIG_PATH/$CMA_SOFTWAREID/config.xml
#	str="s:__MA_VERSION__:$MA_VERSION:g;s:__PACKAGE_NUMBER__:$PACKAGE_NUM:g;s:__MA_SOFTWARE_ID__:$CMA_SOFTWAREID:g;s:__MA_INSTALL_PATH__:$MA_INSTALL_PATH:g;s:__MA_UPDATER_PATH__:$MA_UPDATER_PATH:g;s:__MA_SHARED_LIB_PATH__:$MA_CMA_LPC_LIBS_PATH:g"
#	sed -e "$str" $PKG_CURRENT_DIR/../data/cma_config.xml > $PKG_BUILD_ROOT/$MA_CONFIG_PATH/$CMA_SOFTWAREID/config.xml	
#	echo $PKG_FILE_CONST $PKG_FILE_PERMISSIONS $MA_CONFIG_PATH/$CMA_SOFTWAREID/config.xml >> /tmp/ma_hpux_pkg/product.psf
#fi

# Copying script files into build root
SCRIPT_LIST=`cat $PKG_CURRENT_DIR/../ma_install_files | grep script: | cut -d: -f2`
IS_COMPAT_SVC=1
SHLIB=SHLIB_PATH
for file in `eval echo $SCRIPT_LIST` 
do
	echo Copying $PKG_CURRENT_DIR/../scripts/$SCRIPT_DIR/$file $PKG_BUILD_ROOT/$MA_INSTALL_PATH/scripts/$file
	str="s:__PLATFORM__:$PLATFORM:g;s:__MA_SOFTWARE_ID__:$CMA_SOFTWAREID:g;s:__IS_COMPAT_SVC__:$IS_COMPAT_SVC:g;s:__MA_VERSION__:$MA_VERSION:g;s:__PACKAGE_NUMBER__:$PACKAGE_NUM:g;s:__MA_INSTALL_PATH__:$MA_INSTALL_PATH:g;s:__RUNTIME_LIBRARY_FLAG__:$SHLIB:g;s:__MA_CONFIG_PATH__:$MA_CONFIG_PATH:g"
	sed -e "$str" $PKG_CURRENT_DIR/../scripts/$SCRIPT_DIR/$file > $PKG_BUILD_ROOT/$MA_INSTALL_PATH/scripts/$file	
	#cp $PKG_CURRENT_DIR/../scripts/$SCRIPT_DIR/$file $PKG_BUILD_ROOT/$MA_INSTALL_PATH/scripts/$file
	echo $PSF_FILE_ENTRY $MA_INSTALL_PATH_PSF/scripts/$file >> /tmp/ma_hpux_pkg/product.psf
done
# Copying all ma install lib files into build root
LIBS_LIST=`cat $PKG_CURRENT_DIR/../ma_install_files | egrep  'lib:|lib_compat:' | cut -d: -f2`
for file in `eval echo $LIBS_LIST` 
do
	if [ -e $MA_TARGETS_DIR/$file.$MAJ_VER.$MIN_VER ]; then
		echo Copying file  $MA_TARGETS_DIR/$file.$MAJ_VER.$MIN_VER to $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/$file.$MAJ_VER.$MIN_VER
		cp $MA_TARGETS_DIR/$file.$MAJ_VER.$MIN_VER $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/$file.$MAJ_VER.$MIN_VER
		echo $PSF_FILE_ENTRY $MA_INSTALL_PATH_PSF/lib/$file.$MAJ_VER.$MIN_VER >> /tmp/ma_hpux_pkg/product.psf
		echo $PSF_SYMB_LINK_FILE_ENTRY $MA_INSTALL_PATH/lib/$file.$MAJ_VER.$MIN_VER $MA_INSTALL_PATH/lib/$file.$MAJ_VER >> /tmp/ma_hpux_pkg/product.psf
		echo $PSF_SYMB_LINK_FILE_ENTRY $MA_INSTALL_PATH/lib/$file.$MAJ_VER $MA_INSTALL_PATH/lib/$file >> /tmp/ma_hpux_pkg/product.psf		
		cd $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/
		ln -sf $file.$MAJ_VER.$MIN_VER $file.$MAJ_VER
		ln -sf $file.$MAJ_VER $file
		cd $PKG_CURRENT_DIR
	fi
done
# Creating links in rsdk
RSDK_LIST=`cat $PKG_CURRENT_DIR/../ma_install_files | grep rsdk: | cut -d: -f2`
for file in `eval echo $RSDK_LIST` 
do
	MAJ_VER=`echo $MA_VERSION | cut -d. -f1`
	MIN_VER=`echo $MA_VERSION | cut -d. -f2`
	if [ -e $MA_TARGETS_DIR/$file.$MAJ_VER.$MIN_VER ]; then
		echo Copying file  $MA_TARGETS_DIR/$file.$MAJ_VER.$MIN_VER to $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/rsdk/$file.$MAJ_VER.$MIN_VER
		cp $MA_TARGETS_DIR/$file.$MAJ_VER.$MIN_VER $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/rsdk/$file.$MAJ_VER.$MIN_VER
		echo $PSF_FILE_ENTRY $MA_INSTALL_PATH_PSF/lib/rsdk/$file.$MAJ_VER.$MIN_VER >> /tmp/ma_hpux_pkg/product.psf
		echo $PSF_SYMB_LINK_FILE_ENTRY $MA_INSTALL_PATH/lib/rsdk/$file.$MAJ_VER.$MIN_VER $MA_INSTALL_PATH/lib/rsdk/$file.$MAJ_VER >> /tmp/ma_hpux_pkg/product.psf
		echo $PSF_SYMB_LINK_FILE_ENTRY $MA_INSTALL_PATH/lib/rsdk/$file.$MAJ_VER $MA_INSTALL_PATH/lib/rsdk/$file >> /tmp/ma_hpux_pkg/product.psf
		cd $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/rsdk
		ln -sf $file.$MAJ_VER.$MIN_VER $file.$MAJ_VER
		ln -sf $file.$MAJ_VER $file
		cd $PKG_CURRENT_DIR
	fi
done

# Copying all ma install bin files into build root
BINS_LIST=`cat $PKG_CURRENT_DIR/../ma_install_files | egrep  'bin:|bin_compat:' | cut -d: -f2`
for file in `eval echo $BINS_LIST` 
do
	if [ -e $MA_TARGETS_DIR/$file ]; then
		echo Copying file $MA_TARGETS_DIR/$file to $PKG_BUILD_ROOT/$MA_INSTALL_PATH/bin/$file
		cp $MA_TARGETS_DIR/$file $PKG_BUILD_ROOT/$MA_INSTALL_PATH/bin/$file
		if [ $file == "cmdagent" ] || [ $file == "msaconfig" ]; then
			echo $PSF_FILE_ENTRY $MA_INSTALL_PATH_PSF/bin/$file >> /tmp/ma_hpux_pkg/product.psf
		elif [ $file == "macmnsvc" ]; then
			echo $PSF_MFE_FILE_ENTRY $MA_INSTALL_PATH_PSF/bin/$file >> /tmp/ma_hpux_pkg/product.psf
		else
			echo $PSF_BIN_FILE_ENTRY $MA_INSTALL_PATH_PSF/bin/$file >> /tmp/ma_hpux_pkg/product.psf
		fi		
	fi
done
# Copying mue resource file
if [ -e $MA_TARGETS_DIR/MueRes_0409.cat ]; then
	echo Creating $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/0409
	mkdir -p $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/0409
	echo $PSF_DIR_ENTRY $MA_INSTALL_PATH_PSF/lib/0409 >> /tmp/ma_hpux_pkg/product.psf	
	echo Copying file $MA_TARGETS_DIR/MueRes_0409.cat to $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/0409/MueRes_InUse.cat
	cp $MA_TARGETS_DIR/MueRes_0409.cat $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/0409/MueRes_InUse.cat
	echo $PSF_FILE_ENTRY $MA_INSTALL_PATH_PSF/lib/0409/MueRes_InUse.cat >> /tmp/ma_hpux_pkg/product.psf	
fi


if [ -z "$ENCRYPT_DIR" ]; then 
	echo "Already encrypted mcs sripts from source tree"
	
	# Copying update script file	
	if [ -e $MA_BASE/src/compat/mue/common/mue/scripts/MSA/encrypted/UpdateMain.McS ]; then
		echo Copying file $MA_BASE/src/compat/mue/common/mue/scripts/MSA/encrypted/UpdateMain.McS to $PKG_BUILD_ROOT/$MA_DATA_PATH/update/UpdateMain.McS
		cp $MA_BASE/src/compat/mue/common/mue/scripts/MSA/encrypted/UpdateMain.McS $PKG_BUILD_ROOT/$MA_DATA_PATH/update/UpdateMain.McS
		echo $PSF_FILE_ENTRY $MA_DATA_PATH_PSF/update/UpdateMain.McS >> /tmp/ma_hpux_pkg/product.psf
	fi

	# Copying Install script file
	if [ -e $MA_BASE/src/compat/mue/common/mue/scripts/MSA/encrypted/InstallMain.McS ]; then
		echo Copying file $MA_BASE/src/compat/mue/common/mue/scripts/MSA/encrypted/InstallMain.McS to $PKG_BUILD_ROOT/$MA_DATA_PATH/update/InstallMain.McS
		cp $MA_BASE/src/compat/mue/common/mue/scripts/MSA/encrypted/InstallMain.McS $PKG_BUILD_ROOT/$MA_DATA_PATH/update/InstallMain.McS
		echo $PSF_FILE_ENTRY $MA_DATA_PATH_PSF/update/InstallMain.McS >> /tmp/ma_hpux_pkg/product.psf
	fi
else
	echo "Encrypted mcs sripts from build room"
	# Copying update script file
	if [ -e $ENCRYPT_DIR/UpdateMain.McS ]; then
		echo Copying file $ENCRYPT_DIR/UpdateMain.McS to $PKG_BUILD_ROOT/$MA_DATA_PATH/update/UpdateMain.McS
		cp $ENCRYPT_DIR/UpdateMain.McS $PKG_BUILD_ROOT/$MA_DATA_PATH/update/UpdateMain.McS
		echo $PSF_FILE_ENTRY $MA_DATA_PATH_PSF/update/UpdateMain.McS >> /tmp/ma_hpux_pkg/product.psf
	fi

	# Copying Install script file
	if [ -e $ENCRYPT_DIR/InstallMain.McS ]; then
		echo Copying file $ENCRYPT_DIR/InstallMain.McS to $PKG_BUILD_ROOT/$MA_DATA_PATH/update/InstallMain.McS
		cp $ENCRYPT_DIR/InstallMain.McS $PKG_BUILD_ROOT/$MA_DATA_PATH/update/InstallMain.McS
		echo $PSF_FILE_ENTRY $MA_DATA_PATH_PSF/update/InstallMain.McS >> /tmp/ma_hpux_pkg/product.psf
	fi	
fi

# Copying all ma install sig files into build root
SIGS_LIST=`cat $PKG_CURRENT_DIR/../ma_install_files | grep sig: | cut -d: -f2`
for file in `eval echo $SIGS_LIST` 
do
	if [ -e $MA_TARGETS_DIR/$file ]; then
		echo Copying file $MA_TARGETS_DIR/$file to $PKG_BUILD_ROOT/$MA_INSTALL_PATH/bin/$file
		cp $MA_TARGETS_DIR/$file $PKG_BUILD_ROOT/$MA_INSTALL_PATH/bin/$file
		echo $PSF_SIG_FILE_ENTRY $MA_INSTALL_PATH/bin/$file >> /tmp/ma_hpux_pkg/product.psf
	fi
done

# Copying all ma tools lib files into build root
TOOLS_LIST=`cat $PKG_CURRENT_DIR/../ma_install_tools | grep $PLATFORM  | grep -v ^#`
for file in `eval echo $TOOLS_LIST` 
do    
	lib_dir=`echo $file | cut -d: -f2`  
	lib_name=`echo $file | cut -d: -f3`
 
	if [ "$lib_dir" == "mfecryptc" -a "$CRYPTO" == "openssl" ]; then
		lib_dir=mfecryptc_openssl
	fi

	if [ "$lib_dir" == "curl" -a "$CRYPTO" == "openssl" ]; then
		lib_dir=curl_openssl
	fi

	if [ -e $MA_TOOLS_TARGETS_DIR/$lib_dir/lib/$SUFFIX_DIR/$lib_name ]; then
		lib_name1=`echo $lib_name | cut -d. -f1-3`
		lib_name2=`echo $lib_name | cut -d. -f1-2`
		echo Copying file $MA_TOOLS_TARGETS_DIR/$lib_dir/lib/$SUFFIX_DIR/$lib_name to $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/tools/$lib_name
		cp $MA_TOOLS_TARGETS_DIR/$lib_dir/lib/$SUFFIX_DIR/$lib_name $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/tools/$lib_name
		echo $PSF_FILE_ENTRY $MA_INSTALL_PATH_PSF/lib/tools/$lib_name >> /tmp/ma_hpux_pkg/product.psf
        if [ "$lib_name1" != "$lib_name" ]; then
            echo $PSF_SYMB_LINK_FILE_ENTRY $MA_INSTALL_PATH/lib/tools/$lib_name $MA_INSTALL_PATH/lib/tools/$lib_name1   >> /tmp/ma_hpux_pkg/product.psf
        fi
        if [ "$lib_name2" != "$lib_name1" ]; then
            echo $PSF_SYMB_LINK_FILE_ENTRY $MA_INSTALL_PATH/lib/tools/$lib_name1 $MA_INSTALL_PATH/lib/tools/$lib_name2   >> /tmp/ma_hpux_pkg/product.psf
        fi
		
        cd $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/tools
        
        if [ "$lib_name1" != "$lib_name" ]; then
            ln -sf $lib_name $lib_name1
        fi
        
        if [ "$lib_name2" != "$lib_name1" ]; then
            ln -sf $lib_name1 $lib_name2
        fi
		
		cd $PKG_CURRENT_DIR
	fi
        if [ -e $MA_TOOLS_TARGETS_DIR/$lib_dir/bin/$SUFFIX_DIR/$lib_name ]; then
                if [ -f $MA_TOOLS_TARGETS_DIR/$lib_dir/bin/$SUFFIX_DIR/$lib_name ]; then 
                        echo Copying file $MA_TOOLS_TARGETS_DIR/$lib_dir/bin/$SUFFIX_DIR/$lib_name to $PKG_BUILD_ROOT/$MA_INSTALL_PATH/bin/$lib_name >> /tmp/ma_hpux_pkg/product.psf
                        cp $MA_TOOLS_TARGETS_DIR/$lib_dir/bin/$SUFFIX_DIR/$lib_name $PKG_BUILD_ROOT/$MA_INSTALL_PATH/bin/$lib_name
                        chmod +x $PKG_BUILD_ROOT/$MA_INSTALL_PATH/bin/$lib_name
                fi
        fi
done

echo ******* $CRYPTO ***************
# RSA or OPENSSL files copying into tools folder, 
if [ "$CRYPTO" == "openssl" ]
then
	cp $MA_TOOLS_TARGETS_DIR/mfecryptc_openssl/lib/$SUFFIX_DIR/libcrypto.so.1.0.0  $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/tools/libcrypto.so.1.0.0
	echo $PSF_FILE_ENTRY $MA_INSTALL_PATH_PSF/lib/tools/libcrypto.so.1.0.0 >> /tmp/ma_hpux_pkg/product.psf
	echo $PSF_FILE_ENTRY $MA_INSTALL_PATH_PSF/lib/tools/libcrypto.so >> /tmp/ma_hpux_pkg/product.psf
	cp $MA_TOOLS_TARGETS_DIR/mfecryptc_openssl/lib/$SUFFIX_DIR/libssl.so.1.0.0  $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/tools/libssl.so.1.0.0
	echo $PSF_FILE_ENTRY $MA_INSTALL_PATH_PSF/lib/tools/libssl.so.1.0.0 >> /tmp/ma_hpux_pkg/product.psf
	echo $PSF_FILE_ENTRY $MA_INSTALL_PATH_PSF/lib/tools/libssl.so >> /tmp/ma_hpux_pkg/product.psf
	cd $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/tools
	ln -sf libcrypto.so.1.0.0 libcrypto.so
	ln -sf libssl.so.1.0.0 libssl.so
	cd $PKG_CURRENT_DIR
else
	cp $MA_TOOLS_TARGETS_DIR/mfecryptc/lib/$SUFFIX_DIR/libccme_base.sl  $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/tools/libccme_base.sl
	echo $PSF_FILE_ENTRY $MA_INSTALL_PATH_PSF/lib/tools/libccme_base.sl >> /tmp/ma_hpux_pkg/product.psf
	cp $MA_TOOLS_TARGETS_DIR/mfecryptc/lib/$SUFFIX_DIR/libccme_base_non_fips.sl  $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/tools/libccme_base_non_fips.sl
	echo $PSF_FILE_ENTRY $MA_INSTALL_PATH_PSF/lib/tools/libccme_base_non_fips.sl >> /tmp/ma_hpux_pkg/product.psf
	cp $MA_TOOLS_TARGETS_DIR/mfecryptc/lib/$SUFFIX_DIR/libccme_asym.sl  $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/tools/libccme_asym.sl
	echo $PSF_FILE_ENTRY $MA_INSTALL_PATH_PSF/lib/tools/libccme_asym.sl >> /tmp/ma_hpux_pkg/product.psf
	cp $MA_TOOLS_TARGETS_DIR/mfecryptc/lib/$SUFFIX_DIR/libcryptocme.sig  $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/tools/libcryptocme.sig
	echo $PSF_FILE_ENTRY $MA_INSTALL_PATH_PSF/lib/tools/libcryptocme.sig >> /tmp/ma_hpux_pkg/product.psf
	cp $MA_TOOLS_TARGETS_DIR/mfecryptc/lib/$SUFFIX_DIR/libcryptocme.sl  $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/tools/libcryptocme.sl
	echo $PSF_FILE_ENTRY $MA_INSTALL_PATH_PSF/lib/tools/libcryptocme.sl >> /tmp/ma_hpux_pkg/product.psf
fi	

# Copying all compat tools lib files into build root
TOOLS_LIST=`cat $PKG_CURRENT_DIR/../ma_compat_install_tools | grep $PLATFORM | grep -v ^#`
echo Tool list for compat $TOOLS_LIST
for file in `eval echo $TOOLS_LIST` 
do    
    echo $file
	lib_dir=`echo $file | cut -d: -f2`
	lib_name=`echo $file | cut -d: -f3`
	if [ -e $BOOST_BASE/lib/$lib_name ]; then
		lib_name1=`echo $lib_name | cut -d. -f1-3`
		lib_name2=`echo $lib_name | cut -d. -f1-2`
		echo Copying file $BOOST_BASE/lib/$lib_name to $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/tools/$lib_name
		cp $BOOST_BASE/lib/$lib_name $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/tools/$lib_name
		echo $PSF_FILE_ENTRY $MA_INSTALL_PATH_PSF/lib/tools/$lib_name >> /tmp/ma_hpux_pkg/product.psf
		echo $PSF_SYMB_LINK_FILE_ENTRY $MA_INSTALL_PATH/lib/tools/$lib_name $MA_INSTALL_PATH/lib/tools/$lib_name1 >> /tmp/ma_hpux_pkg/product.psf
		echo $PSF_SYMB_LINK_FILE_ENTRY $MA_INSTALL_PATH/lib/tools/$lib_name1 $MA_INSTALL_PATH/lib/tools/$lib_name2 >> /tmp/ma_hpux_pkg/product.psf
		cd $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/tools
		ln -sf $lib_name $lib_name1
		ln -sf $lib_name1 $lib_name2
		cd $PKG_CURRENT_DIR
	fi
done


#LIB_STD=libstdc++.sl.6.5 
LIB_STD=libstdc++.so 
#LIB_GCC=libgcc_s.1
LIB_ICONV=libiconv.so
echo $PSF_FILE_ENTRY $MA_INSTALL_PATH_PSF/lib/$LIB_STD >> /tmp/ma_hpux_pkg/product.psf
#echo $PSF_FILE_ENTRY $MA_INSTALL_PATH_PSF/lib/$LIB_GCC >> /tmp/ma_hpux_pkg/product.psf
echo $PSF_FILE_ENTRY $MA_INSTALL_PATH_PSF/lib/$LIB_ICONV >> /tmp/ma_hpux_pkg/product.psf
#echo $PSF_SYMB_LINK_FILE_ENTRY $MA_INSTALL_PATH/lib/$LIB_STD $MA_INSTALL_PATH/lib/libstdc++.so.6 >> /tmp/ma_hpux_pkg/product.psf
#cp /opt/hp-gcc/4.0.1/ilp32/lib/$LIB_STD $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/
cp /usr/local/lib/$LIB_STD $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/
if (( $? )); then echo "Copy Failed /opt/hp-gcc/4.0.1/ilp32/lib/$LIB_STD"; fi
#cp /opt/hp-gcc/4.0.1/ilp32/lib/$LIB_GCC $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/
#if (( $? )); then echo "Copy Failed /opt/hp-gcc/4.0.1/ilp32/lib/$LIB_GCC"; fi
cp /usr/local/lib/hpux32/$LIB_ICONV $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/
if (( $? )); then echo "Copy Failed /usr/local/lib/"; fi
cd $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib
#ln -sf $LIB_STD libstdc++.so.6

##################
#Creating symbolic links (moved from postinstall script)

#echo $PKG_DIR_CONST $CMA_INSTALL_PATH >> /tmp/ma_hpux_pkg/product.psf
#ln -sf $MA_INSTALL_PATH $PKG_BUILD_ROOT/$CMA_INSTALL_PATH

#echo $PKG_DIR_CONST $CMA_CONFIG_PATH/$CMA_SOFTWAREID >> /tmp/ma_hpux_pkg/product.psf
#ln -sf $MA_CONFIG_PATH/$MA_SOFTWAREID $PKG_BUILD_ROOT/$CMA_CONFIG_PATH/$CMA_SOFTWAREID

#echo $PKG_DIR_CONST $CMA_CONFIG_PATH/$MA_CMN_UPDATERID >> /tmp/ma_hpux_pkg/product.psf
#ln -sf $MA_CONFIG_PATH/$MA_CMN_UPDATERID $PKG_BUILD_ROOT/$CMA_CONFIG_PATH/$MA_CMN_UPDATERID

ln -sf $MA_DATA_PATH/data $PKG_BUILD_ROOT/$MA_DATA_PATH/scratch

#echo $PKG_DIR_CONST $MA_INSTALL_PATH/scratch >> /tmp/ma_hpux_pkg/product.psf
#ln -sf $MA_DATA_PATH/scratch/ $PKG_BUILD_ROOT/$MA_INSTALL_PATH/scratch

#echo $PKG_FILE_CONST $PKG_FILE_PERMISSIONS $MA_CMA_LPC_LIBS_PATH/libmfelpc.so >> /tmp/ma_hpux_pkg/product.psf
#ln -sf $MA_INSTALL_PATH/lib/libmfelpc.so $PKG_BUILD_ROOT/$MA_CMA_LPC_LIBS_PATH/libmfelpc.so
#if [ x"$ARCH_BIT" == x"64" ]
#then
#	echo $PKG_DIR_CONST ${MA_CMA_LPC_LIBS_PATH}/lib64 >> /tmp/ma_hpux_pkg/product.psf
#	mkdir -p $PKG_BUILD_ROOT/${MA_CMA_LPC_LIBS_PATH}/lib64
#	echo $PKG_FILE_CONST $PKG_FILE_PERMISSIONS $MA_CMA_LPC_LIBS_PATH/lib64/libmfelpc.so >> /tmp/ma_hpux_pkg/product.psf
#	cd $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/
#	ln -sf libmfelpc.so $PKG_BUILD_ROOT/$MA_CMA_LPC_LIBS_PATH/lib64/libmfelpc.so
#fi




echo $'\n************* Copy files done **************'
################################################################
#Step 3: Creating install files and directory in buildroot
################################################################
echo $'\n\n************* Step 3: Creating installer **************'

# Update placeholders in config.xml
MA_START_CMD="/sbin/init.d/ma start"
MA_STOP_CMD="/sbin/init.d/ma stop"
MA_UNINSTALL_CMD="swremove MFEcma"      

str="s:__MA_UPDATER_PATH__:$MA_UPDATER_PATH:g;s:__MA_SOFTWARE_ID__:$MA_SOFTWAREID:g;s:__MA_VERSION__:$MA_VERSION:g;s:__PACKAGE_NUMBER__:$PACKAGE_NUM:g;s:__MA_DATA_PATH__:$MA_DATA_PATH:g;s:__MA_INSTALL_PATH__:$MA_INSTALL_PATH:g;s:__MA_START_CMD__:$MA_START_CMD:g;s:__MA_STOP_CMD__:$MA_STOP_CMD:g;s:__MA_UNINSTALL_CMD__:$MA_UNINSTALL_CMD:g;s:__MA_SHARED_LIB_PATH__:$MA_CMA_LPC_LIBS_PATH:g"
sed -e "$str" $PKG_CURRENT_DIR/../data/config.xml > $PKG_BUILD_ROOT/$MA_CONFIG_PATH/$MA_SOFTWAREID/config.xml

str="s:__MA_VERSION__:$MA_VERSION:g;s:__PACKAGE_NUMBER__:$PACKAGE_NUM:g;s:__MA_INSTALL_PATH__:$MA_INSTALL_PATH:g;s:__MA_DATA_PATH__:$MA_DATA_PATH:g"
sed -e "$str" $PKG_CURRENT_DIR/../data/mainfo.ini > $PKG_BUILD_ROOT/$MA_CONFIG_PATH/mainfo.ini 
echo $PSF_FILE_ENTRY etc/ma.d/mainfo.ini >> /tmp/ma_hpux_pkg/product.psf

#str="s:__MA_SOFTWARE_ID__:$MA_SOFTWAREID:g;s:__MA_VERSION__:$MA_VERSION:g;s:__PACKAGE_NUMBER__:$PACKAGE_NUM:g;s:__MA_DATA_PATH__:$MA_DATA_PATH:g;s:__MA_CONFIG_PATH__:$MA_CONFIG_PATH:g;s:__MA_INSTALL_PATH__:$MA_INSTALL_PATH:g"
#sed -e "$str" $PKG_CURRENT_DIR/../scripts/$PLATFORM/ma > $PKG_BUILD_ROOT/$MA_INSTALL_PATH/scripts/ma

#mkdir -p $PKG_BUILD_ROOT/sbin/init.d
#cp $PKG_BUILD_ROOT/$MA_INSTALL_PATH/scripts/ma $PKG_BUILD_ROOT/sbin/init.d/ma
#cd $PKG_BUILD_ROOT/sbin/init.d/
#ln -sf  ma cma
#echo $PSF_FILE_ENTRY sbin/init.d/ma >> /tmp/ma_hpux_pkg/product.psf
#echo $PSF_FILE_ENTRY sbin/init.d/cma >> /tmp/ma_hpux_pkg/product.psf
# TODO - strip $PKG_BUILD_ROOT/opt/mileaccess/cma/lib/lib*

echo "end" >> /tmp/ma_hpux_pkg/product.psf

HasCRLFHenceExit $PKG_BUILD_ROOT



#cp $PKG_CURRENT_DIR/preinstall > $PKG_BUILD_ROOT/preinstall
#####################################################
#changing installer config files
STARTUP_SCRIPT=/sbin/init.d/ma
GREP_CMD="grep -e 'EPOAGENT' -e 'CMNUPD' -e 'CMDAGENT'"
str="s:__MA_VERSION__:$MA_VERSION:g;s:__PACKAGE_NUMBER__:$PACKAGE_NUM:g;s:__MA_SOFTWARE_ID__:$MA_SOFTWAREID:g;s:__MA_DATA_PATH__:$MA_DATA_PATH:g;s:__MA_MSGBUS_DATA_PATH__:$MA_MSGBUS_DATA_PATH:g;s:__GREP_CMD__:$GREP_CMD:g;s:__MA_INSTALL_PATH__:$MA_INSTALL_PATH:g;s:__CMA_CONFIG_PATH__:$CMA_CONFIG_PATH:g;s:__CMA_SOFTWAREID__:$CMA_SOFTWAREID:g;s:__MA_CMN_UPDATERID__:$MA_CMN_UPDATERID:g;s:__MA_CONFIG_PATH__:$MA_CONFIG_PATH:g;s:__MA_CMA_LPC_LIBS_PATH__:$MA_CMA_LPC_LIBS_PATH:g;s:__LIB_EXTENSION__:$LIB_EXTENSION:g;s:__CMA_INSTALL_PATH__:$CMA_INSTALL_PATH:g;s:__CMA_SHARED_PATH__:$CMA_SHARED_PATH:g;s:__STARTUP_SCRIPT__:$STARTUP_SCRIPT:g;"
sed -e "{$str}" $PKG_CURRENT_DIR/request > $PKG_BUILD_ROOT/request
sed -e "{$str}" $PKG_CURRENT_DIR/postinstall > $PKG_BUILD_ROOT/postinstall
sed -e "{$str}" $PKG_CURRENT_DIR/preinstall > $PKG_BUILD_ROOT/preinstall
sed -e "{$str}" $PKG_CURRENT_DIR/preremove > $PKG_BUILD_ROOT/preremove
sed -e "{$str}" $PKG_CURRENT_DIR/postremove > $PKG_BUILD_ROOT/postremove
sed -e "{$str}" $CHECKINSTALLNAME > $PKG_BUILD_ROOT/checkinstall
touch $PKG_BUILD_ROOT/response


chmod 755 $PKG_BUILD_ROOT/request
chmod 755 $PKG_BUILD_ROOT/checkinstall
chmod 755 $PKG_BUILD_ROOT/postinstall
chmod 755 $PKG_BUILD_ROOT/preinstall
chmod 755 $PKG_BUILD_ROOT/preremove
chmod 755 $PKG_BUILD_ROOT/postremove
chmod 755 $PKG_BUILD_ROOT/response

dfout=`du -sk /tmp/ma_hpux_pkg`
pkgsize=`echo $dfout | cut -d' ' -f1`
reqsize=$(( (($pkgsize + 1023 )/1024)*1024 ))
echo "Package Size is $reqsize KB"

str="s/^REQUIRED_INSTALLATION_SIZE.*/REQUIRED_INSTALLATION_SIZE=$reqsize/"
echo $str

sed -e "$str" $CHECKINSTALLNAME > $PKG_BUILD_ROOT/checkinstall
  
echo "Building the HPUX  package..."
PKGCMD=swpackage
PKG_FILE_NAME=MFEcma-$MA_VERSION-$PACKAGE_NUM-$TARGET.depot 

echo $PKGCMD -s  /tmp/ma_hpux_pkg/product.psf -x media_type=tape @  $OUTPUT_DIR/$PKG_FILE_NAME
$PKGCMD  -s  /tmp/ma_hpux_pkg/product.psf -x media_type=tape @ $OUTPUT_DIR/$PKG_FILE_NAME
if (( $? )); then
	echo "FAILED to create the CMA package"
	exit 1 
fi

###################################
#bootstrap  bootstrap_sfx.sh  PackageInfo.xml  BootstrapInfo.xml
strip $MA_TARGETS_DIR/bootstrap

BOOTSTRAP_PAYLOAD="bootstrap"
cp $MA_TARGETS_DIR/bootstrap $OUTPUT_DIR/bootstrap
cp $PKG_CURRENT_DIR/../data/BootstrapInfo.xml $OUTPUT_DIR
cp $PKG_CURRENT_DIR/../data/PackageInfo.xml $OUTPUT_DIR
bootstrap_size=`ls -l  $OUTPUT_DIR/bootstrap | awk '{print $5}'`

str="s:__BOOSTRAP_SIZE__:$bootstrap_size:g;s:__BOOTSTRAP_PAYLOAD__:$BOOTSTRAP_PAYLOAD:g"
sed -e "$str" $PKG_CURRENT_DIR/../bootstrap_sfx/bootstrap_sfx.hpux.sh.template > $OUTPUT_DIR/bootstrap_sfx.sh

#moving PkgCatalog  to build out
#str="s:_PRODUCT_ID_:$PRODUCT_ID_CATALOG:g;"
#sed -e "$str" $MA_BASE/build/packager/data/PkgCatalog_unx.xml > $OUTPUT_DIR/PkgCatalog.xml
sed -e "s:OUTPUT_FILELIST:\"install.sh;installhpia.sh;AgentInst.McS;setup;FrameworkConfig.zip\":g;" $MA_BASE/build/packager/data/PkgCatalog_unx.xml > $OUTPUT_DIR/PkgCatalog.xml

#cp $MA_BASE/build/packager/data/PkgCatalog_unx.xml  $OUTPUT_DIR/PkgCatalog.xml

#moving unz to build out
unzip_exe_size=`ls  -l $MA_TARGETS_DIR/unz | awk '{print $5}'`
str="s:UNZIP_SIZE_IN_BYTES:$unzip_exe_size:g;s:__MA_Major_Version__:$MAJ_VER:g;s:__MA_Minor_Version__:$MIN_VER:g;s:__MA_Patch_Version__:$SUB_VER:g;s:__MA_Build_Number__:$PACKAGE_NUM:g"
ls -l $MA_BASE/build/packager/unix/sfx/sfx.hpux.sh.template
sed -e "$str" $MA_BASE/build/packager/unix/sfx/sfx.hpux.sh.template > $OUTPUT_DIR/sfx.sh
cp $MA_TARGETS_DIR/unz $OUTPUT_DIR/unz

###################################
# Creating SDK tar file (sdk and beacon rpm files) in output folder
SDK_DIR=/tmp/sdk_temp
if [ -e $SDK_DIR ]
then
rm -fr $SDK_DIR
fi
mkdir -p $SDK_DIR
mkdir -p $SDK_DIR/samples
mkdir -p $SDK_DIR/include
mkdir -p $SDK_DIR/bindings
mkdir -p $SDK_DIR/lib/$SUFFIX_DIR
mkdir -p $SDK_DIR/rsdk/$SUFFIX_DIR
mkdir -p $SDK_DIR/masign
mkdir -p $SDK_DIR/certs

SAMPLES_DIR=$SDK_DIR/samples
cp -r $MA_BASE/tools/ma_sdk/include/* $SDK_DIR/include
cp -r $MA_BASE/tools/ma_sdk/bindings/* $SDK_DIR/bindings
cp -r $MA_BASE/tools/ma_sdk/lib/$SUFFIX_DIR/* $SDK_DIR/lib/$SUFFIX_DIR
cp -r $MA_BASE/tools/ma_sdk/rsdk/$SUFFIX_DIR/* $SDK_DIR/rsdk/$SUFFIX_DIR
cp -r $MA_TOOLS_TARGETS_DIR/masign/* $SDK_DIR/masign
cp -r $MA_BASE/build/packager/data/certs $SDK_DIR/
cp  $MA_BASE/build/packager/data/certs/ma_test_certs.tar $SDK_DIR/certs/

cd /tmp

cur_dir=`pwd`
cd $MA_BASE/tools/beacon/
tar cvf $SAMPLES_DIR/beacon.tar build include src
cd $cur_dir

cd $SDK_DIR
tar cvf $OUTPUT_DIR/ma_sdk500_$PLATFORM.tar include lib bindings rsdk samples masign

cd $cur_dir
rm -fr $SDK_DIR

cd $cur_dir
if [ -d $PKG_BUILD_ROOT ]
then         
	rm -rf $PKG_BUILD_ROOT
    echo "finishing " $PKG_BUILD_ROOT
fi

rm -rf /tmp/ma_hpux_pkg/product.psf

