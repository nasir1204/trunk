#!/bin/bash

SCRIPT_DEBUG=1

HasCRLFHenceExit()
{
    error=0
    for i in `find $1`
    do
      file $i | cut -d: -f2 | grep -w "\([Ss]cript\)\|\(ASCII..*text\)" >/dev/null
      if (( $? == 0 )); then
	  str1=`strings -1 $i | head -1`
	  str2=`cat $i | head -1`
	  if [ "$str1" != "$str2" ]; then
	      echo $i 
	      error=1
	  fi
      fi
    done
    if (( $error != 0 )); then
	echo "Have CRLF in Text files. Hence Exiting..." 
	exit 4;
    fi
}

DebugEcho()
{
    if (( SCRIPT_DEBUG )); then
	echo $*
    fi
}

Usage()
{
    echo "Usage: bash <CompletePath>/package.sh -v <version> -b <base-architecture> -t <type> -p <package-no> -o <output-loc-dir>  -s <date/time stamp>  -m <mcs-script-dir> "
    echo "All  flags are to be specified. There are no default values."
    echo "   -v version i.e. 4.8.0"
    echo "   -b base-architecture i.e. parisc/itanium"
    echo "   -t type    i.e  debug/release/"	
    echo "   -p package-id number example 215"
    echo "   -o output location <Complete path from Root> /Users/sdash/NWA/Debug"
    echo "   -s date/time stamp - YYYYMMDD0VVV.00 "
	echo "   -m encrypted mcs script dir <Absolute path> OR Skip this option/provide an empty string as option argument to use the script files from source tree"
    exit 2
}

add_path()
{
	echo "Adding $1 to \$PATH"
	INDATA=$1
	data=`echo $PATH | grep "\(\(^$INDATA\)\|\(:$INDATA\)\)" | wc -l`
	if (( !$data )); then
	    if [ "$PATH" == "" ]; then 
		PATH=$1
	    else
		PATH=$PATH:$1
	    fi
	fi
}
encryptdir=""

while getopts v:b:t:p:o:s:m: c
  do
  case $c in
      v)
	  version=$OPTARG
	  DebugEcho version $version;;
      b)
          architecture=$OPTARG
          DebugEcho architecture $architecture;;
      t)
          type=$OPTARG
          DebugEcho type $type;;
      p)
	  package=$OPTARG
	  DebugEcho package $package;;
      o)
	  outdir=$OPTARG
	  DebugEcho outdir $outdir;;
      s)  
	  # Format is YYYYMMDD0VVV.00
	  timestamp=$OPTARG
	  DebugEcho timestamp $timestamp;;
	  m)
	  encryptdir=$OPTARG
	  DebugEcho encryptdir $encryptdir;;
      \?) 
          Usage;;
  esac
done
shift `expr $OPTIND - 1` 

scriptdir=$0

add_path /usr/sbin

firstchar=`echo $scriptdir | grep "^\/"`

if [ "$firstchar" == "" ]; then
    echo "The script path is not absolute path"
    Usage
fi

if [ -z "$version" ]; then 
    echo "Version not defined"
    Usage
fi

if [ -z "$architecture" ]; then
    echo "Architecture not defined"
    Usage
fi

if [ -z "$package" ]; then 
    echo "Package not defined"
    Usage
fi


if [ -z "$outdir" ]; then 
    echo "Outdir not defined"
    Usage
fi


if [ -z "$timestamp" ]; then 
    echo "Time Stamp not defined"
    Usage
fi

if [ -z "$encryptdir" ]; then 
    echo "Mcs script directory not specified"
fi

MAJVER=`echo $version | cut -d. -f1`
DebugEcho $MAJVER
MINVER=`echo $version | cut -d. -f2`
DebugEcho $MINVER
SUBVER=`echo $version | cut -d. -f3`
DebugEcho $SUBVER

#SOFTWAREID=EPOAGENT"$MAJVER$MINVER$SUBVER"0HPUX
SOFTWAREID=EPOAGENT4000HPUX
UPDATER_SOFTWAREID=CMNUPD__3000
CMDAGENT_SOFTWAREID=CMDAGENT1000
echo Current \$PATH is $PATH



dir=`echo $scriptdir | grep ".installer.hpux.package.sh" | sed -e  's/.installer.hpux.package.sh//g'`
DebugEcho $dir
dir1=$dir/../unix/


# In compile.sh and regress.sh, the Makefile sets these development default
# locations for us. Here there is no Makefile and we must to it ourselves.
if [ "$BOOST_BASE" == "" ]; then
    export BOOST_BASE=$dir/../../boost
fi

if [ "$CURL" == "" ]; then
    export CURL=$dir/../../curl
fi

if [ "$ZLIB" == "" ]; then
    export ZLIB=$dir/../../zlib
fi

if [ "$MA_TOOLS" == "" ]; then
    export MA_TOOLS=$dir/../..
fi
export MFECRYPTC=$MA_TOOLS/mfecryptc
export CRYPTSHIM=$MA_TOOLS/cryptshim
export MFEDISCOVERY=$MA_TOOLS/mfediscovery


echo BOOST_BASE=$BOOST_BASE
echo CURL=$CURL
echo ZLIB=$ZLIB
echo MA_TOOLS=$MA_TOOLS
echo MFECRYPTC=$MFECRYPTC
echo CRYPTSHIM=$CRYPTSHIM
echo MFEDISCOVERY=$MFEDISCOVERY


 
PKG_BUILD_ROOT=/tmp/CMA-$MAJVER$MINVER$SUBVER-buildroot
echo The current buildroot is $PKG_BUILD_ROOT

if [ -d $PKG_BUILD_ROOT ]; then
        rm -rf $PKG_BUILD_ROOT
fi

echo Creating the build root at $PKG_BUILD_ROOT
mkdir -p $PKG_BUILD_ROOT
if (( $? )) ; then echo "Failed to make a directory " ; fi

mkdir -p $PKG_BUILD_ROOT/etc/cma.d/$SOFTWAREID
mkdir -p $PKG_BUILD_ROOT/etc/cma.d/$UPDATER_SOFTWAREID
mkdir -p $PKG_BUILD_ROOT/etc/cma.d/$CMDAGENT_SOFTWAREID
mkdir -p $PKG_BUILD_ROOT/opt/mileaccess/shared/$MAJVER.$MINVER.$SUBVER/lib
mkdir -p $PKG_BUILD_ROOT/opt/mileaccess/cma/bin
mkdir -p $PKG_BUILD_ROOT/opt/mileaccess/cma/bin/0409
mkdir -p $PKG_BUILD_ROOT/opt/mileaccess/cma/lib
mkdir -p $PKG_BUILD_ROOT/opt/mileaccess/cma/resource
mkdir -p $RPM_BUILD_ROOT/opt/mileaccess/cma/scratch/keystore
mkdir -p $PKG_BUILD_ROOT/opt/mileaccess/cma/scratch/AgArchv
mkdir -p $PKG_BUILD_ROOT/opt/mileaccess/cma/scratch/AgentDB/Event
mkdir -p $PKG_BUILD_ROOT/opt/mileaccess/cma/scratch/AgentDB/DataChannel
mkdir -p $PKG_BUILD_ROOT/opt/mileaccess/cma/scratch/AgUnpack
mkdir -p $PKG_BUILD_ROOT/opt/mileaccess/cma/scratch/etc
mkdir -p $PKG_BUILD_ROOT/opt/mileaccess/cma/scratch/cmamesh
mkdir -p $PKG_BUILD_ROOT/opt/mileaccess/cma/scratch/update
mkdir -p $PKG_BUILD_ROOT/opt/mileaccess/cma/scratch/update/tmp
mkdir -p $PKG_BUILD_ROOT/sbin/init.d/

for i in $dir1/$type/lib*
 do
        if [ ! -L $i ]; then
                DebugEcho $i
                cp $i $PKG_BUILD_ROOT/opt/mileaccess/cma/bin
        fi
 done

curlLib="libcurl.sl.6.0" 
zlibLib="libz.sl.1.2.3"
dppName="dpp-1.0.0.depot"
stdcppLibCmd="cp /opt/hp-gcc/4.0.1/ilp32/lib/libstdc++.sl.6.5 $PKG_BUILD_ROOT/opt/mileaccess/cma/bin/"
iconvCmd="cp /usr/local/lib/libiconv.sl $PKG_BUILD_ROOT/opt/mileaccess/cma/bin/"

if [ "$architecture" != "parisc" ]; then
  curlLib="libcurl.so.6.0"
  zlibLib="libz.so.1.2.3"
  dppName="dpp-1.0.0_itanium.depot"
  stdcppLibCmd="cp /usr/local/lib/libstdc++.so $PKG_BUILD_ROOT/opt/mileaccess/cma/lib/"
  iconvCmd="cp /usr/local/lib/hpux32/libiconv.so $PKG_BUILD_ROOT/opt/mileaccess/cma/lib/"
  if [ ! -e $PKG_BUILD_ROOT/opt/mileaccess/cma/bin/libmacbplugin.sl.4.8 ];then
    cp $dir1/../common/macbplugin/unix/$type/libmacbplugin.sl.4.8 $PKG_BUILD_ROOT/opt/mileaccess/cma/bin
  fi
fi

cp -r $dir1/$type/en_US $PKG_BUILD_ROOT/opt/mileaccess/cma/resource
cp $dir1/$type/MueRes_0409.cat $PKG_BUILD_ROOT/opt/mileaccess/cma/bin/0409/MueRes.cat
cp $dir1/$type/cma $PKG_BUILD_ROOT/opt/mileaccess/cma/bin/
cp $dir1/$type/cma.sig $PKG_BUILD_ROOT/opt/mileaccess/cma/bin/
cp $dir1/$type/cmdagent $PKG_BUILD_ROOT/opt/mileaccess/cma/bin/
cp $dir1/$type/cmdagent.sig $PKG_BUILD_ROOT/opt/mileaccess/cma/bin/
cp $dir1/$type/msaconfig $PKG_BUILD_ROOT/opt/mileaccess/cma/bin/
cp $dir1/$type/msaconfig.sig $PKG_BUILD_ROOT/opt/mileaccess/cma/bin/
cp $dir1/$type/cmamesh $PKG_BUILD_ROOT/opt/mileaccess/cma/bin/
cp $dir1/$type/cmamesh.sig $PKG_BUILD_ROOT/opt/mileaccess/cma/bin/
cp $dir1/$type/Mue $PKG_BUILD_ROOT/opt/mileaccess/cma/bin/
cp $dir1/$type/Mue.sig $PKG_BUILD_ROOT/opt/mileaccess/cma/bin/
cp $dir1/$type/libmfelpc.sl.$MAJVER.$MINVER $PKG_BUILD_ROOT/opt/mileaccess/shared/$MAJVER.$MINVER.$SUBVER/lib/
cd $PKG_BUILD_ROOT/opt/mileaccess/shared/$MAJVER.$MINVER.$SUBVER/lib
ln -s libmfelpc.sl.$MAJVER.$MINVER libmfelpc.sl.$MAJVER
ln -s libmfelpc.sl.$MAJVER libmfelpc.sl

cp $dir1/$type/unz $outdir

unzip_exe_size=`ls -l  $dir1/$type/unz | awk '{print $5}'`
str="s/UNZIP_SIZE_IN_BYTES/$unzip_exe_size/"
sed -e "$str" $dir/installer/sfx/sfx.hpux.sh.template > $outdir/sfx.hpux.sh

bootstrap_size=`ls -l  $dir1/$type/bootstrap | awk '{print $5}'`
str="s/__BOOSTRAP_SIZE__/$bootstrap_size/"
sed -e "$str" $dir/installer/bootstrap_sfx/bootstrap_sfx.hpux.sh.template > $outdir/bootstrap_sfx.hpux.sh

cp $dir/data/updaterconfig.xml $PKG_BUILD_ROOT/etc/cma.d/$UPDATER_SOFTWAREID/config.xml
if (( $? )); then echo "Copy Failed $dir/data/updaterconfig.xml"; fi

cp $dir/data/cmdagentconfig.xml $PKG_BUILD_ROOT/etc/cma.d/$CMDAGENT_SOFTWAREID/config.xml
if (( $? )); then echo "Copy Failed $dir/data/cmdagentconfig.xml"; fi

str="s/PACKAGE_NUMBER/$package/"
sed -e "$str" $dir/data/hpux/config/$SOFTWAREID/config.xml > $PKG_BUILD_ROOT/etc/cma.d/$SOFTWAREID/config.xml
if (( $? )); then echo "Copy Failed $dir/data/hpux/config/$SOFTWAREID/config.xml"; fi

str="s/EnterCMASoftwareIdHere/$SOFTWAREID/"
sed -e "$str" $dir/data/agent.ini > $PKG_BUILD_ROOT/opt/mileaccess/cma/scratch/agent.ini;
if (( $? )); then echo "Copy Failed $dir/data/agent.ini"; fi

cp $dir/data/registry.ini $PKG_BUILD_ROOT/opt/mileaccess/cma/scratch
if (( $? )); then echo "Copy Failed $dir/data/registry.ini"; fi

cp $dir/data/cma.config $PKG_BUILD_ROOT/opt/mileaccess/cma/scratch
if (( $? )); then echo "Copy Failed $dir/data/cma.config"; fi

if [ -z "$encryptdir" ]; then 
echo "Encrypted mcs sripts from source tree"
cp $dir/../common/mue/scripts/MSA/encrypted/InstallMain.McS $PKG_BUILD_ROOT/opt/mileaccess/cma/scratch/update/
if (( $? )); then echo "Copy Failed $dir/../common/mue/scripts/MSA/encrypted/InstallMain.McS"; fi
cp $dir/../common/mue/scripts/MSA/encrypted/UpdateMain.McS $PKG_BUILD_ROOT/opt/mileaccess/cma/scratch/update/
if (( $? )); then echo "Copy Failed $dir/../common/mue/scripts/MSA/encrypted/UpdateMain.McS"; fi
else
echo "Encrypted mcs sripts from build room"
cp $encryptdir/InstallMain.McS $PKG_BUILD_ROOT/opt/mileaccess/cma/scratch/update/
if (( $? )); then echo "Copy Failed $encryptdir/InstallMain.McS"; fi
cp $encryptdir/UpdateMain.McS $PKG_BUILD_ROOT/opt/mileaccess/cma/scratch/update/
if (( $? )); then echo "Copy Failed $encryptdir/UpdateMain.McS"; fi
fi


cp $dir/data/SiteList.xml $PKG_BUILD_ROOT/opt/mileaccess/cma/scratch/etc
if (( $? )); then echo "Copy Failed $dir/data/SiteList.xml"; fi

cp $dir/data/hpux/Server.xml $PKG_BUILD_ROOT/opt/mileaccess/cma/scratch
if (( $? )); then echo "Copy Failed $dir/data/hpux/Server.xml"; fi


cp $dir/data/hpux/bin/cma $PKG_BUILD_ROOT/sbin/init.d/
if (( $? )); then echo "Copy Failed $dir/data/hpux/bin/cma"; fi

cp $dir/data/hpux/bin/cmamesh $PKG_BUILD_ROOT/sbin/init.d/
if (( $? )); then echo "Copy Failed $dir/data/hpux/bin/cmamesh"; fi

cp $BOOST_BASE/lib/libboost_thread.sl.1.39.0 $PKG_BUILD_ROOT/opt/mileaccess/cma/bin/
if (( $? )); then echo "Copy Failed $BOOST_BASE/lib/libboost_thread.sl.1.39.0";fi
cp $BOOST_BASE/lib/libboost_filesystem.sl.1.39.0 $PKG_BUILD_ROOT/opt/mileaccess/cma/bin/
if (( $? )); then echo "Copy Failed $BOOST_BASE/lib/libboost_filesystem.sl.1.39.0"; fi
cp $BOOST_BASE/lib/libboost_system.sl.1.39.0 $PKG_BUILD_ROOT/opt/mileaccess/cma/bin/
if (( $? )); then echo "Copy Failed $BOOST_BASE/lib/libboost_system.sl.1.39.0"; fi

cp $CURL/lib/$curlLib $PKG_BUILD_ROOT/opt/mileaccess/cma/bin/
if (( $? )); then echo "Copy Failed $CURL/lib/$curlLib"; fi
cp $ZLIB/lib/$zlibLib $PKG_BUILD_ROOT/opt/mileaccess/cma/bin/
if (( $? )); then echo "Copy Failed $ZLIB/lib/$zlibLib"; fi

$stdcppLibCmd 
if (( $? )); then echo "Copy Command $stdcppLibCmd Failed"; fi

if [ "$architecture" = "parisc" ]; then
cp /opt/hp-gcc/4.0.1/ilp32/lib/libgcc_s.1 $PKG_BUILD_ROOT/opt/mileaccess/cma/bin/
if (( $? )); then echo "Copy Failed /opt/hp-gcc/4.0.1/ilp32/lib/libgcc_s.1"; fi
fi

$iconvCmd 
if (( $? )); then echo "Copy Command $iconvCmd Failed"; fi

cd $PKG_BUILD_ROOT/opt/mileaccess/cma/lib
if (( $? != 0 )); then
     echo "packaging Failed. Exiting..."
     exit 1
fi

ln -s /opt/mileaccess/cma/bin/libboost_thread.sl.1.39.0 libboost_thread.sl
ln -s /opt/mileaccess/cma/bin/libboost_filesystem.sl.1.39.0 libboost_filesystem.sl
ln -s /opt/mileaccess/cma/bin/libboost_system.sl.1.39.0 libboost_system.sl
if [ "$architecture" = "parisc" ]; then
ln -s /opt/mileaccess/cma/bin/libstdc++.sl.6.5 libstdc++.sl.6
ln -s libstdc++.sl.6 libstdc++.sl 
ln -s /opt/mileaccess/cma/bin/libgcc_s.1 libgcc_s.sl
ln -s /opt/mileaccess/cma/bin/libiconv.sl libiconv.sl
fi

chmod 755 $PKG_BUILD_ROOT/opt/mileaccess/cma/bin/lib*

#strip  -g -S -d $PKG_BUILD_ROOT/opt/mileaccess/cma/bin/lib*
#strip  -g -S -d $PKG_BUILD_ROOT/opt/mileaccess/cma/bin/cma
#strip  -g -S -d $PKG_BUILD_ROOT/opt/mileaccess/cma/bin/msaconfig
#strip  -g -S -d $PKG_BUILD_ROOT/opt/mileaccess/cma/bin/Mue
#chatr +s disable $PKG_BUILD_ROOT/opt/mileaccess/cma/bin/msaconfig

chmod 755 $PKG_BUILD_ROOT/opt/mileaccess/cma/bin/*

cd $PKG_BUILD_ROOT/opt/mileaccess/cma/bin
for i in *.symbols
 do
        rm -f $i
done

for i in *
 do
        DebugEcho $i
        f1=`echo $i | cut -d'.' -f1`
        f2=`echo $i | cut -d'.' -f2`
        f3=`echo $i | cut -d'.' -f3`
        f4=`echo $i | cut -d'.' -f4`

        if [ "$f4" != "" ]&&
         [ "$f2" == "sl" ]; then
                cd ../lib
                ln -sf /opt/mileaccess/cma/bin/$i $f1.$f2.$f3
                ln -sf /opt/mileaccess/cma/lib/$f1.$f2.$f3 $f1.$f2
                cd ../bin
        fi
        if [ "$architecture" != "parisc" ]; then
          if [ "$f4" != "" ]&&
           [ "$f2" == "so" ]; then
                  cd ../lib
                  ln -sf /opt/mileaccess/cma/bin/$i $f1.$f2.$f3
                  ln -sf /opt/mileaccess/cma/lib/$f1.$f2.$f3 $f1.$f2
                  cd ../bin
          fi
        fi
 done

#copying RSA lib after symbol stripping.

cp $MFECRYPTC/lib/libmfecryptc.sl.1.0 $PKG_BUILD_ROOT/opt/mileaccess/cma/bin/
if (( $? )); then echo "Copy Failed $MFECRYPTC/lib/libmfecryptc.sl.1.0"; fi
cp $MFECRYPTC/lib/libmfecryptc.sig $PKG_BUILD_ROOT/opt/mileaccess/cma/lib/
if (( $? )); then echo "Copy Failed $MFECRYPTC/lib/libmfecryptc.sig"; fi
cp $CRYPTSHIM/lib/libcryptshim.sl.1.0 $PKG_BUILD_ROOT/opt/mileaccess/cma/bin/
if (( $? )); then echo "Copy Failed $CRYPTSHIM/lib/libcryptshim.sl.1.0"; fi

cp $MFEDISCOVERY/lib/libmfediscovery.sl.1.0 $PKG_BUILD_ROOT/opt/mileaccess/cma/bin/
if (( $? )); then echo "Copy Failed $MFEDISCOVERY/lib/libmfediscovery.sl.1.0"; fi

ln -sf /opt/mileaccess/cma/bin/libmfediscovery.sl.1.0  libmfediscovery.sl.1
ln -sf libmfediscovery.sl.1  libmfediscovery.sl

ln -sf /opt/mileaccess/cma/bin/libmfecryptc.sl.1.0  libmfecryptc.sl.1
ln -sf libmfecryptc.sl.1  libmfecryptc.sl

ln -sf /opt/mileaccess/cma/bin/libcryptshim.sl.1.0  libcryptshim.sl.1
ln -sf libcryptshim.sl.1  libcryptshim.sl

#No symbol strip for RSA lib otherwise it will fail FIPS validation
cp $MFECRYPTC/lib/libcryptocme2.sl $PKG_BUILD_ROOT/opt/mileaccess/cma/bin/
if (( $? )); then echo "Copy Failed $MFECRYPTC/lib/libcryptocme2.sl"; fi
cp $MFECRYPTC/lib/libccme_base.sl $PKG_BUILD_ROOT/opt/mileaccess/cma/bin/
if (( $? )); then echo "Copy Failed $MFECRYPTC/lib/libccme_base.sl"; fi
cp $MFECRYPTC/lib/libcryptocme2.sig $PKG_BUILD_ROOT/opt/mileaccess/cma/bin/
if (( $? )); then echo "Copy Failed $MFECRYPTC/lib/libcryptocme2.sig"; fi
chmod 755 $PKG_BUILD_ROOT/opt/mileaccess/cma/bin/libcryptocme2.sl
chmod 755 $PKG_BUILD_ROOT/opt/mileaccess/cma/bin/libccme_base.sl
chmod 755 $PKG_BUILD_ROOT/opt/mileaccess/cma/bin/libcryptocme2.sig

#cp $CURL/lib/libssl.sl.0.9.8 $PKG_BUILD_ROOT/opt/mileaccess/cma/bin/
#if (( $? )); then echo "Copy Failed $CURL/lib/libssl.sl.0.9.8"; fi
#cp $CURL/lib/libcrypto.sl.0.9.8 $PKG_BUILD_ROOT/opt/mileaccess/cma/bin/
#if (( $? )); then echo "Copy Failed $CURL/lib/libcrypto.sl.0.9.8"; fi

cd ../lib
if (( $? != 0 )); then
     echo "packaging Failed. Exiting..."
     exit 1
fi
#ln -s /opt/mileaccess/cma/bin/libssl.sl.0.9.8 libssl.sl
#ln -s /opt/mileaccess/cma/bin/libcrypto.sl.0.9.8 libcrypto.sl
cd ../bin
if (( $? != 0 )); then
     echo "packaging Failed. Exiting..."
     exit 1
fi

echo "Identifying architecture and setting PSFNAME and CHECKINSTALLNAME"
if [ "$architecture" = "parisc" ]; then
  PSFNAME=$dir/installer/hpux/product.psf.template
  CHECKINSTALLNAME=$dir/installer/hpux/checkinstall.template
else
  PSFNAME=$dir/installer/hpux/product.psf_itanium.template
  CHECKINSTALLNAME=$dir/installer/hpux/checkinstall_itanium.template
fi

HasCRLFHenceExit $PKG_BUILD_ROOT

echo "Creating the PSF for package "
echo "using  psf file $PSFNAME"

str="s/MSAPKGROOT/CMA-$MAJVER$MINVER$SUBVER-buildroot/;s/VERSION_NUMBER/$version/;s/RELEASE_NUMBER/$package/;s/EnterCMASoftwareIdHere/$SOFTWAREID/;s/EnterMajVerHere/$MAJVER/g;s/EnterMinVerHere/$MINVER/g;s/EnterSubVerHere/$SUBVER/g;s/EnterUpdaterSoftwareIdHere/$UPDATER_SOFTWAREID/g;s/EnterCmdAgentSoftwareIdHere/$CMDAGENT_SOFTWAREID/g"

echo $str
sed -e "$str" $PSFNAME > $dir/installer/hpux/product.psf

echo "Creating the request file for the package "
dfout=`du -sk $PKG_BUILD_ROOT`
pkgsize=`echo $dfout | cut -d' ' -f1`
reqsize=$(( (($pkgsize + 1023 )/1024)*1024 ))
echo "Package Size is $reqsize KB"

str="s/^REQUIRED_INSTALLATION_SIZE.*/REQUIRED_INSTALLATION_SIZE=$reqsize/"
echo $str

sed -e "$str" $CHECKINSTALLNAME > $PKG_BUILD_ROOT/checkinstall

echo "Creating the postremove file for the package "
POSTREMOVENAME=$dir/installer/hpux/postremove.template
str="s/EnterCMASoftwareIdHere/$SOFTWAREID/g;s/EnterUpdaterSoftwareIdHere/$UPDATER_SOFTWAREID/g;s/EnterCmdAgentSoftwareIdHere/$CMDAGENT_SOFTWAREID/g"
sed -e "$str" $POSTREMOVENAME > $PKG_BUILD_ROOT/postremove

cp $dir/installer/hpux/request $PKG_BUILD_ROOT/
cp $dir/installer/hpux/preinstall  $PKG_BUILD_ROOT/
cp $dir/installer/hpux/postinstall $PKG_BUILD_ROOT/
cp $dir/installer/hpux/preremove $PKG_BUILD_ROOT/
touch $PKG_BUILD_ROOT/response


chmod 755 $PKG_BUILD_ROOT/request
chmod 755 $PKG_BUILD_ROOT/checkinstall
chmod 755 $PKG_BUILD_ROOT/postinstall
chmod 755 $PKG_BUILD_ROOT/preinstall
chmod 755 $PKG_BUILD_ROOT/preremove
chmod 755 $PKG_BUILD_ROOT/response

mkdir -p $outdir

if (( $? )); then
        echo "Unable to create target directory for the pkg."
fi

echo "Building the HPUX  package..."
PKGCMD=swpackage 
PKG_FILE_NAME=CMA-$MAJVER.$MINVER.$SUBVER-$package-$type.depot
echo $PKGCMD -s  $dir/installer/hpux/product.psf -x media_type=tape @  $outdir/$PKG_FILE_NAME
$PKGCMD  -s  $dir/installer/hpux/product.psf -x media_type=tape @ $outdir/$PKG_FILE_NAME

echo "Creating PkgCatalog.xml for deployable package for CMA"
str="s/PACKAGE_NUMBER/$package/"
sed -e "$str" $dir/installer/hpux/PkgCatalog.xml > $outdir/PkgCatalog.xml

cp $dir/data/hpux/PackageInfo.xml $outdir
cp $dir/installer/hpux/BootstrapInfo.xml $outdir
cp $dir/data/RepoKeys.ini $outdir

if (( $? )); then
        echo "FAILED to create the CMA package"
        exit 1
fi



if (( $? )); then
        echo "Unable to copy cma pkg to $outdir "
fi

cd $dir/../common/nwa/dpp/install/hpux
if (( $? != 0 )); then
     echo "packaging Failed. Exiting..."
     exit 1
fi

bash package_dpp.sh

if (( $? )); then
        echo "Failed to create the dpp package"
fi

echo "copying dpp-1.0.0-1.depot to $outdir/$dppName"

mv /tmp/dpp-1.0.0-1.depot $outdir/$dppName
if (( $? )); then
        echo "Unable to copy dpp package to $outdir "
fi

if [ -d $outdir/dpp-pkg-files ]; then
        rm -rf $outdir/dpp-pkg-files
fi

mkdir $outdir/dpp-pkg-files
cp $dir/../common/nwa/dpp/install/data/PkgCatalog.xml $outdir/dpp-pkg-files/
cp $dir/../common/nwa/dpp/install/data/DppDet.McS $outdir/dpp-pkg-files/
cp $dir/../common/nwa/dpp/install/data/setup $outdir/dpp-pkg-files/


rm -rf $PKG_BUILD_ROOT

exit 0
