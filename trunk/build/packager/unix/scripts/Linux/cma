#!/bin/sh
#
# chkconfig: - 91 35
# description: mileaccess Agent
#	       

### BEGIN INIT INFO
# Provides: cma
# Required-Start:
# Required-Stop:
# Default-Start: 2 3 4 5
# Default-Stop:
# Description : mileaccess agent
### END INIT INFO

COMAPT_SVC_PID=/var/mileaccess/agent/.macompatsvc.pid

distro=
if [ -f /etc/redhat-release ]
    then
    distro=redhat
elif [ -f /etc/SuSE-release ]
    then
    distro=SuSE
elif [ -f /etc/lsb-release ]
    then
    distro=ubuntu
fi

echo $COLUMNS

[ -z "${COLUMNS:-}" ] && COLUMNS=80

if [ -z "${BOOTUP:-}" ]; then
  if [ -f /etc/sysconfig/init ]; then
      . /etc/sysconfig/init
  elif [ "$distro" = "ubuntu" ]; then
    BOOTUP=color
    RES_COL=60
    MOVE_TO_COL="/bin/echo -en \\033[${RES_COL}G"
    SETCOLOR_SUCCESS="/bin/echo -en \\033[1;32m"
    SETCOLOR_FAILURE="/bin/echo -en \\033[1;31m"
    SETCOLOR_WARNING="/bin/echo -en \\033[1;33m"
    SETCOLOR_NORMAL="/bin/echo -en \\033[0;39m"
    CARRIAGE_RETURN="/bin/echo -en \r"
    LOGLEVEL=1
  else
    BOOTUP=color
    RES_COL=60
    MOVE_TO_COL="echo -en \\033[${RES_COL}G"
    SETCOLOR_SUCCESS="echo -en \\033[1;32m"
    SETCOLOR_FAILURE="echo -en \\033[1;31m"
    SETCOLOR_WARNING="echo -en \\033[1;33m"
    SETCOLOR_NORMAL="echo -en \\033[0;39m"
    CARRIAGE_RETURN="echo -en \r"
    LOGLEVEL=1
  fi
  if [ "$CONSOLETYPE" = "serial" ]; then
      BOOTUP=serial
      MOVE_TO_COL=
      SETCOLOR_SUCCESS=
      SETCOLOR_FAILURE=
      SETCOLOR_WARNING=
      SETCOLOR_NORMAL=
  fi
fi



checkprocessid() {
    local i   
    for i in $* ; do
	if [ -d "/proc/$i" ] ; then 
		return 0  
	fi
    done
    return 1 
}

status() {
       pid=
        if [ -f $COMAPT_SVC_PID ]; then
                pid=`cat $COMAPT_SVC_PID`
                if checkprocessid $pid; then
                        echo "MA compat service (pid $pid) is running..."
                        return 0
                else
                        echo "MA compat service dead but pid file exists"
                        return 1 
                fi
        elif [ ! -f $COMAPT_SVC_PID ]; then
                pid=`ps -ef  | grep /opt/mileaccess/agent/bin/macompatsvc | grep -v "grep" | awk '{print $2}'| head -1 `
                if checkprocessid $pid; then
                        echo "MA compat service (pid $pid) is running..."
                        return 0
                fi
        fi

        echo "MA compat service  is stopped"
        return 1 
}

echo_success() {
    if [ "$distro" = "SuSE" ]
	then
	[ "$BOOTUP" = "color" ] && $MOVE_TO_COL
	[ "$BOOTUP" = "color" ] && $SETCOLOR_SUCCESS
	echo -n done
	[ "$BOOTUP" = "color" ] && $SETCOLOR_NORMAL
	echo -ne "\r"	
    else
 	[ "$BOOTUP" = "color" ] && $MOVE_TO_COL
	echo -n "[  "
	[ "$BOOTUP" = "color" ] && $SETCOLOR_SUCCESS
	echo -n "OK"
	[ "$BOOTUP" = "color" ] && $SETCOLOR_NORMAL
	echo -n "  ]"
	$CARRIAGE_RETURN
    fi    
    return 0
}

echo_failure() {
    if [ "$distro" = "SuSE" ]
	then
	[ "$BOOTUP" = "color" ] && $MOVE_TO_COL
	[ "$BOOTUP" = "color" ] && $SETCOLOR_FAILURE
	echo -n failed
	[ "$BOOTUP" = "color" ] && $SETCOLOR_NORMAL
	echo -ne "\r"	
    else
 	[ "$BOOTUP" = "color" ] && $MOVE_TO_COL
	echo -n "[  "
	[ "$BOOTUP" = "color" ] && $SETCOLOR_FAILURE
	echo -n "FAILED"
	[ "$BOOTUP" = "color" ] && $SETCOLOR_NORMAL
	echo -n "  ]"
	$CARRIAGE_RETURN
    fi    
    return 1
}

echo_passed() {
    if [ "$distro" = "SuSE"]
	then
	[ "$BOOTUP" = "color" ] && $MOVE_TO_COL
	[ "$BOOTUP" = "color" ] && $SETCOLOR_SUCCESS
	echo -n passed
	[ "$BOOTUP" = "color" ] && $SETCOLOR_NORMAL
	echo -ne "\r"	
    else
 	[ "$BOOTUP" = "color" ] && $MOVE_TO_COL
	echo -n "[  "
	[ "$BOOTUP" = "color" ] && $SETCOLOR_SUCCESS
	echo -n "PASSED"
	[ "$BOOTUP" = "color" ] && $SETCOLOR_NORMAL
	echo -n "  ]"
	$CARRIAGE_RETURN
    fi    
    return 1
}

echo_warning() {
    if [ "$distro" = "SuSE"]
	then
	[ "$BOOTUP" = "color" ] && $MOVE_TO_COL
	[ "$BOOTUP" = "color" ] && $SETCOLOR_WARNING
	echo -n warning
	[ "$BOOTUP" = "color" ] && $SETCOLOR_NORMAL
	echo -ne "\r"	
    else
 	[ "$BOOTUP" = "color" ] && $MOVE_TO_COL
	echo -n "[  "
	[ "$BOOTUP" = "color" ] && $SETCOLOR_WARNING
	echo -n $"WARNING"
	[ "$BOOTUP" = "color" ] && $SETCOLOR_NORMAL
	echo -n "  ]"
        $CARRIAGE_RETURN
    fi    
  return 1
}


RETVAL=0


reload() {
        pid=
	if [ -f $COMAPT_SVC_PID ]; then
                pid=`cat $COMAPT_SVC_PID`
        else
              pid=`ps -ef  | grep /opt/mileaccess/agent/bin/macompatsvc | grep -v "grep" | awk '{print $2}'| head -1 `              
        fi
        
        if [ $pid ];then
		echo "$1" > "/var/mileaccess/agent/.plugininfo"
                kill -USR1 $pid
		sleep 3 
        else
               echo "MA compat service is not running , can't notify."

        fi
}
unload() {
        pid=
		if [ -f $COMAPT_SVC_PID ]; then
					pid=`cat $COMAPT_SVC_PID`
		else
              pid=`ps -ef  | grep /opt/mileaccess/agent/bin/macompatsvc | grep -v "grep" | awk '{print $2}'| head -1 `                
        fi
        if [ $pid ];then
		echo "$1" > "/var/mileaccess/agent/.plugininfo"
                kill -USR2 $pid
		sleep 3 
        else
                echo "mileaccess compat Agent is not running , can't notify."
        fi
}

case "$1" in
  status)
  	status
	;;
  reload)
	if [ $# -ne 2 ]; then
		echo "Usage: $0 {status|reload SOFTWAREID|unload SOFTWAREID}"
		exit 1;
	fi
	reload "$2"
 	;;
  unload)
	if [ $# -ne 2 ]; then
		echo "Usage: $0 {status|reload SOFTWAREID|unload SOFTWAREID}"
		exit 1;
	fi
	unload "$2"
 	;;
  *)
	echo "Usage: $0 {status|reload SOFTWAREID|unload SOFTWAREID}"

	exit 4
esac

exit $?

