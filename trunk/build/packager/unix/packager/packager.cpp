// server.cpp : Defines the entry point for the console application.
//
#include <windows.h>
#include <string.h>
#include <tchar.h>
#include "zip.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <stdexcept>
#include <algorithm> 
#include <sys/stat.h>
#include "sha.h"

 
#ifdef WIN32
#define USEWIN32IOAPI
#include "iowin32.h"
#endif
#define STR_FRAMEINST_TOKENS                   TEXT("-/")
#define STR_FRAMEINST_ASSIGN                   '='
#define STR_SIZEOF(s)	(sizeof(s) / sizeof(*(s)) )

#define XML_TAG_W L"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
#define XML_TAG_A "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"

//Booststrap files
const std::string bootstrap_sfx("bootstrap_sfx.sh");
const std::string bootstrap_sh("bootstrap.sh");
//OSX specific files
const std::string bootstrap_app_zip("mileaccessSmartInstall.app.zip");
const std::string bootstrap_con_path("mileaccessSmartInstall.app/Contents/_CodeSignature/coninfo.xml");


//Linux files
const std::string mfecma_rpm("MFEma.i686.rpm");
const std::string mfert_rpm("MFErt.i686.rpm");

//Linux (Debian) files
const std::string mfecma_deb("MFEma.i686.deb");
const std::string mfert_deb("MFErt.i686.deb");

//MLOS files
const std::string mfema_mlos_rpm("MFEma.mlos.rpm");

// AIX files
const std::string mfemsa_rpm("MFEma.ppc.rpm");

//OSX files
const std::string mfecma_dmg("MFEma.dmg");

//Solaris files
const std::string mfecma_pkg("MFEma.pkg");
const std::string cmaadmin("cmaadmin");

//HPUX files
const std::string mfemsapa_depot("MFEma.depot");
const std::string mfemsaia_depot("MFEma.itanium.depot");

//SCM files
const std::string mfecma_scm_rpm("MFEma.scm.rpm");

//Other files
const std::string reqseckey("reqseckey.bin");
const std::string srpubkey("srpubkey.bin");
const std::string sitelist("sitelist.xml");
const std::string reqseckey_2048("req2048seckey.bin");
const std::string srpubkey_2048("sr2048pubkey.bin");
const std::string agentfipsmode("agentfipsmode");
const std::string repokeysini("RepoKeys.ini");
const std::string hashes("hashes.xml");
const std::string BootstrapInfo("BootstrapInfo.xml");
const std::string PackageInfo("PackageInfo.xml");

//FrameworkConfig zip file
const std::string config_zip("FrameworkConfig.zip");

//Output zip file
const std::string out_zip("out.zip");

//Stub file
const std::string stub_file("sfx.sh");

//output script
const std::string out_file("install.sh");
const std::string out_file_rpm("installrpm.sh");
const std::string out_file_deb("installdeb.sh");
const std::string install_zip("FrameworkInstall.zip");
const std::string installrpm_zip("FrameworkInstallrpm.zip");
const std::string installdeb_zip("FrameworkInstalldeb.zip");


//HPUX install scripts
const std::string out_file_hppa("installhppa.sh");
const std::string out_file_hpia("installhpia.sh");
const std::string installhppa_zip("FrameworkInstallhppa.zip");
const std::string installhpia_zip("FrameworkInstallhpia.zip");


//The unzip executable
const std::string unzip_exe("unz");

enum InstallType 
{
  INSTALL,
  INSTALLDEB,
  INSTALLRPM,
  INSTALLHPPA,
  INSTALLHPIA,
  INSTALL_END
};

enum ZipType 
{
  FRAMEWORK_CONFIG_ZIP,
  FRAMEWORK_INSTALL_ZIP,
  INSTALL_SH_ZIP,
  BOOTSTRAP_SH_ZIP
};

const unsigned long buffer_size = 16384;
const unsigned long block_size = 512;

unsigned long  filetime(const char *f, tm_zip *tmzip, unsigned long *dt)
{
  int ret = 0;
  {
    FILETIME ftLocal;
    HANDLE hFind;
    WIN32_FIND_DATA  ff32;

    hFind = FindFirstFile(f,&ff32);
    if (hFind != INVALID_HANDLE_VALUE)
      {
        FileTimeToLocalFileTime(&(ff32.ftLastWriteTime),&ftLocal);
        FileTimeToDosDateTime(&ftLocal,((LPWORD)dt)+1,((LPWORD)dt)+0);
        FindClose(hFind);
        ret = 1;
      }
  }
  return ret;
}


bool file_exists(const std::string &file_name)
{
  HANDLE hFile = CreateFile(file_name.c_str(),GENERIC_READ,
			    0, NULL,
			    OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL, 
			    NULL);
  if ( hFile == INVALID_HANDLE_VALUE )
    {
      std::cout<<"The file "<<file_name.c_str()<<" cannot be found"<<std::endl;
      return false;
    }
  CloseHandle(hFile);
  return true;
}

bool get_file_size(const std::string &file_name, unsigned long &file_size)
{
  HANDLE hFile = CreateFile(file_name.c_str(),GENERIC_READ,
			    0, NULL,
			    OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL, 
			    NULL);
  if ( hFile == INVALID_HANDLE_VALUE )
    {
      std::cout<<"The file "<<file_name.c_str()<<" cannot be found"<<std::endl;
      return false;
    }
  file_size = GetFileSize(hFile,NULL);
  CloseHandle(hFile);	
  if (file_size == INVALID_FILE_SIZE)
    return false;
  return true;
	
}

//Checks if the mandatory files required to create a meaningful zip exists or not.
//This means that one of the install packages MUST be present
bool can_create_zip(InstallType iType)
{
  bool linux = file_exists(mfecma_rpm) && file_exists(mfert_rpm);
  bool debian = file_exists(mfecma_deb) && file_exists(mfert_deb);
  bool aix = file_exists(mfemsa_rpm);
  bool osx = file_exists(mfecma_dmg);
  bool hpuxpa = file_exists(mfemsapa_depot);
  bool hpuxia = file_exists(mfemsaia_depot);
  bool solaris = file_exists(cmaadmin) && file_exists(mfecma_pkg);
  bool scm = file_exists(mfecma_scm_rpm);
  bool mlos = file_exists(mfema_mlos_rpm);
  return linux || osx || hpuxpa || solaris || aix || scm || mlos || (debian && (INSTALLDEB == iType)) || (hpuxia && (INSTALLHPIA == iType));
}

bool create_file_list(std::vector<std::string > &v, InstallType iType,ZipType zType)
{
  
	//when zType != FRAMEWORK_CONFIG_ZIP, we need config files.

  if(FRAMEWORK_CONFIG_ZIP != zType)
  {
	  if (file_exists(mfecma_rpm) && file_exists(mfert_rpm) && (iType != INSTALLDEB))
	  {
		  v.push_back(mfecma_rpm);
		  v.push_back(mfert_rpm);
	  }
	  if (file_exists(mfecma_deb) && file_exists(mfert_deb) && (iType != INSTALLRPM))
	  {
		  v.push_back(mfecma_deb);
		  v.push_back(mfert_deb);
	  }
	  if (file_exists(mfemsa_rpm))
		  v.push_back(mfemsa_rpm);

	  if (file_exists(mfecma_dmg))
		v.push_back(mfecma_dmg);
	
	  if (file_exists(mfemsapa_depot) && (iType != INSTALLHPIA))
		v.push_back(mfemsapa_depot);

	  if (file_exists(mfemsaia_depot) && (iType != INSTALLHPPA))
		v.push_back(mfemsaia_depot);
 
	  if (file_exists(cmaadmin) && file_exists(mfecma_pkg))
		{
		  v.push_back(cmaadmin);
		  v.push_back(mfecma_pkg);
		}
	
	  if (file_exists(mfecma_scm_rpm))
		v.push_back(mfecma_scm_rpm);

	  if (file_exists(mfema_mlos_rpm))
		v.push_back(mfema_mlos_rpm);
  }
  else
  {//new files for bootstrap support
	  if (file_exists(hashes))
	  v.push_back(hashes);
	  if (file_exists(BootstrapInfo))
	  v.push_back(BootstrapInfo);
	  if (file_exists(PackageInfo))
	  v.push_back(PackageInfo);
  }
 
   if(FRAMEWORK_INSTALL_ZIP != zType)
   {
	if (file_exists(reqseckey))
	v.push_back(reqseckey);

	if (file_exists(srpubkey))
	v.push_back(srpubkey);

	if (file_exists(sitelist))
	v.push_back(sitelist);

	if (file_exists(reqseckey_2048))
	v.push_back(reqseckey_2048);

	if (file_exists(srpubkey_2048))
	v.push_back(srpubkey_2048);

	if (file_exists(agentfipsmode))
	v.push_back(agentfipsmode);

	if (file_exists(repokeysini))
	v.push_back(repokeysini);
   }
   else
   {
	if (file_exists(stub_file))
	v.push_back(stub_file);
   }
 
	return true;
}

//-a -9 a.app.zip 
// bootstrap.app/Contents/Info.plist

bool AddConInfoToAppZip(const std::string &ZipPath,const std::string &DataPath)
{

  zipFile zf;
  int errclose;
  int err=0;
  int opt_compress_level=Z_DEFAULT_COMPRESSION;
  int size_buf=buffer_size;;
  if (0 == CopyFile(ZipPath.c_str(),bootstrap_app_zip.c_str(),FALSE))
  {
	  printf("Error in making a copy of %s \n",ZipPath.c_str());
	  return false;
  }

#ifdef USEWIN32IOAPI

  zlib_filefunc_def ffunc;
  fill_win32_filefunc(&ffunc);
  zf = zipOpen2(bootstrap_app_zip.c_str(),APPEND_STATUS_ADDINZIP,NULL,&ffunc);
#else
  zf = zipOpen(bootstrap_app_zip.c_str(),APPEND_STATUS_ADDINZIP);
#endif

  if (zf)
  {
    FILE * fin = NULL;
    int size_read;
    zip_fileinfo zi;
	void* buf=(void *)new char [size_buf];
    zi.tmz_date.tm_sec = zi.tmz_date.tm_min = zi.tmz_date.tm_hour =
	zi.tmz_date.tm_mday = zi.tmz_date.tm_mon = zi.tmz_date.tm_year = 0;
    zi.dosDate = 0;
    zi.internal_fa = 0;
    zi.external_fa = 0;

	printf("Adding connection data to %s\n",bootstrap_app_zip.c_str());
    filetime(DataPath.c_str(),&zi.tmz_date,&zi.dosDate);
    err = zipOpenNewFileInZip3(zf,bootstrap_con_path.c_str(),&zi,
                                 NULL,0,NULL,0,NULL /* comment*/,
                                 (opt_compress_level != 0) ? Z_DEFLATED : 0,
                                 opt_compress_level,0,
                                 /* -MAX_WBITS, DEF_MEM_LEVEL, Z_DEFAULT_STRATEGY, */
                                 -MAX_WBITS, DEF_MEM_LEVEL, Z_DEFAULT_STRATEGY,
                                 NULL,0);

    if (err != ZIP_OK)
	printf("error in opening %s in zipfile\n",DataPath.c_str());
      else
	{
	  fin = fopen(DataPath.c_str(),"rb");
	  if (fin==NULL)
	    {
	      err=ZIP_ERRNO;
	      printf("error in opening %s for reading\n",DataPath.c_str());
	    }
	}

    if (err == ZIP_OK)
	do
	  {
	    err = ZIP_OK;
	    size_read = (int)fread(buf,1,size_buf,fin);
	    if (size_read < size_buf)
      if (feof(fin)==0)
		{
		  printf("error in reading %s\n",DataPath.c_str());
		  err = ZIP_ERRNO;
		}

	    if (size_read>0)
	      {
		err = zipWriteInFileInZip (zf,buf,size_read);
		if (err<0)
		  {
		    printf("error in writing %s in the zipfile\n", DataPath.c_str());
		  }

	      }
	  } while ((err == ZIP_OK) && (size_read>0));

      if (fin)
	fclose(fin);

      if (err<0)
	err=ZIP_ERRNO;
      else
	{
	  err = zipCloseFileInZip(zf);
	  if (err!=ZIP_OK)
	    printf("error in closing %s in the zipfile\n",  DataPath.c_str());
	}

	  errclose = zipClose(zf,NULL);
	    delete []buf;
		  if (errclose != ZIP_OK)
    printf("error in closing %s\n",bootstrap_app_zip.c_str());

  }
  else
  {
	  printf("error opening %s\n",bootstrap_app_zip.c_str());
      err= ZIP_ERRNO;
  }
  return true;
}

bool create_zip(InstallType iType,ZipType zType)
{
 
	if(iType != INSTALL_END && !can_create_zip(iType) ) 
    return false;

  std::vector<std::string > v;

  try
    {
      create_file_list(v,iType,zType);
    }
  catch(std::length_error &/*e*/)
    {
      std::cout<<"Caught exception"<<std::endl;
      return false;
    }

  if (v.empty())
    {
      std::cout<<"No files to add"<<std::endl;
      return false;
    }

  zipFile zf;
  int errclose;
  int err=0;
  int opt_overwrite=0;
  int opt_compress_level=Z_DEFAULT_COMPRESSION;
  int size_buf=buffer_size;;
  void* buf=(void *)new char [size_buf];


#        ifdef USEWIN32IOAPI

  zlib_filefunc_def ffunc;
  fill_win32_filefunc(&ffunc);
  zf = zipOpen2(out_zip.c_str(),(opt_overwrite==2) ? 2 : 0,NULL,&ffunc);
#        else
  zf = zipOpen(out_zip.c_str(),(opt_overwrite==2) ? 2 : 0);
#        endif

  if (zf == NULL)
    {
      printf("error opening %s\n",out_zip.c_str());
      err= ZIP_ERRNO;
    }
  else
    printf("creating %s\n",out_zip.c_str());

  for (std::vector<std::string>::iterator it = v.begin(); it != v.end(); it++ )
    {
      FILE * fin = NULL;
      int size_read;
      const char* filenameinzip = (*it).c_str();
      zip_fileinfo zi;
      unsigned long crcFile=0;

      zi.tmz_date.tm_sec = zi.tmz_date.tm_min = zi.tmz_date.tm_hour =
	zi.tmz_date.tm_mday = zi.tmz_date.tm_mon = zi.tmz_date.tm_year = 0;
      zi.dosDate = 0;
      zi.internal_fa = 0;
      zi.external_fa = 0;
      filetime(filenameinzip,&zi.tmz_date,&zi.dosDate);

      /*
	err = zipOpenNewFileInZip(zf,filenameinzip,&zi,
	NULL,0,NULL,0,NULL / * comment * /,
	(opt_compress_level != 0) ? Z_DEFLATED : 0,
	opt_compress_level);
      */

      err = zipOpenNewFileInZip3(zf,filenameinzip,&zi,
                                 NULL,0,NULL,0,NULL /* comment*/,
                                 (opt_compress_level != 0) ? Z_DEFLATED : 0,
                                 opt_compress_level,0,
                                 /* -MAX_WBITS, DEF_MEM_LEVEL, Z_DEFAULT_STRATEGY, */
                                 -MAX_WBITS, DEF_MEM_LEVEL, Z_DEFAULT_STRATEGY,
                                 NULL,crcFile);

      if (err != ZIP_OK)
	printf("error in opening %s in zipfile\n",filenameinzip);
      else
	{
	  fin = fopen(filenameinzip,"rb");
	  if (fin==NULL)
	    {
	      err=ZIP_ERRNO;
	      printf("error in opening %s for reading\n",filenameinzip);
	    }
	}

      if (err == ZIP_OK)
	do
	  {
	    err = ZIP_OK;
	    size_read = (int)fread(buf,1,size_buf,fin);
	    if (size_read < size_buf)
      if (feof(fin)==0)
		{
		  printf("error in reading %s\n",filenameinzip);
		  err = ZIP_ERRNO;
		}

	    if (size_read>0)
	      {
		err = zipWriteInFileInZip (zf,buf,size_read);
		if (err<0)
		  {
		    printf("error in writing %s in the zipfile\n",
			   filenameinzip);
		  }

	      }
	  } while ((err == ZIP_OK) && (size_read>0));

      if (fin)
	fclose(fin);

      if (err<0)
	err=ZIP_ERRNO;
      else
	{
	  err = zipCloseFileInZip(zf);
	  if (err!=ZIP_OK)
	    printf("error in closing %s in the zipfile\n",
		   filenameinzip);
	}
    }
      
  errclose = zipClose(zf,NULL);
  if (errclose != ZIP_OK)
    printf("error in closing %s\n",out_zip.c_str());
  delete []buf;
	
	
  /*	HZIP hz = CreateZip(out_zip.c_str(), NULL);
	if (hz)
	{
	for (std::vector<std::wstring>::const_iterator it = v.begin(); it != v.end(); it++)
	{
	ZRESULT result = ZipAdd(hz,(*it).c_str(), (*it).c_str());
	if (result != ZR_OK)
	{
	std::cout<<"Failed to add file to zip"<<std::endl;
	result = CloseZip(hz);
	if (result != ZR_OK)
	std::cout<<"Failed to close zip handle"<<std::endl;
	return false;
	}
	}		
	}
	else
	return false;
	CloseZip(hz);*/
  return true;
}

bool create_empty_out_file(const std::string &output_file)
{
  FILE *fptr = fopen(output_file.c_str(),"wb");
  if (!fptr)
    {
      std::cout<<"Failed to create output file"<<std::endl;
      return false;
    }
  fclose(fptr);
  return true;
}

bool append_file_to_another(const std::string &first,
			    const std::string &second)
{
  unsigned long second_size;
  if ( !get_file_size( second,second_size))
    return false;

  FILE *fstub = fopen(first.c_str(),"ab");
  FILE *fzip = fopen(second.c_str(),"rb");
  char buffer[buffer_size];
  bool done = false;
  unsigned long size_so_far = 0;
  if (fstub && fzip)
    {
      fseek(fstub,0,SEEK_END);	
      while (size_so_far < second_size)
	{
	  size_t nRead = fread(buffer,sizeof(char),buffer_size,fzip);
	  fwrite(buffer,nRead,1,fstub);
	  size_so_far+= nRead;
	}
      fclose(fstub);
      fclose(fzip);
      return true;
    }
	
  if (fstub) fclose(fstub);
  if (fzip) fclose(fzip);
  return false;		
}
							  
bool pad_file_to_block_size_multiple(const std::string &filename, const unsigned long &blk_size)
{
  unsigned long size = 0;
  if (!get_file_size( filename,size ))
    return false;
  bool need_padding = 0 != size % blk_size;
  if (need_padding)
    {
      unsigned long n_padding_bytes = blk_size - (size % blk_size);
      char *buffer = new (std::nothrow) char[n_padding_bytes];
      if (!buffer)
	return false;
      memset(buffer, 0 , n_padding_bytes);
      std::ofstream stream(filename.c_str(),std::ios::app | std::ios::binary );
      bool result = false;
      if (stream.is_open())
	{
	  stream.write(buffer, n_padding_bytes);      
	  result = stream.good();
	  if (result)
	    stream.flush();      
	}
      delete []buffer;
      return result;
    }
  else
    return true;
}

bool append_unzip_exe_and_zip(const std::string &out_file,
			      const std::string &stub_file,
			      const std::string &unzip_exe,
			      const std::string &zip_file)
{	
  if (!append_file_to_another(out_file,stub_file))
    {	
      std::cout<<"Failed to append unzip exe to stub"<<std::endl;
      return false;
    }

  if (!pad_file_to_block_size_multiple(unzip_exe, block_size))
    {
      std::cout<<"Failed to pad unzip exe"<<std::endl;
      return false;

    }
  if (!append_file_to_another(out_file,unzip_exe))
    {	
      std::cout<<"Failed to append unzip exe to stub"<<std::endl;
      return false;
    }
  if (!append_file_to_another(out_file,zip_file))
    {
      std::cout<<"Failed to append zip to stub"<<std::endl;
      return false;	
    }
  return true;
}


/*
<sha256>
<install.sh>6950c0cb57e8db10a488cfdd10d96c04a0247b37e279f316ffafdd7e35138eda</install.sh>
<installrpm.sh>63adaba0bb06cb584024a19f7e35d569237c8fe7311bec98f982673c5d3ae72d</installrpm.sh>
<installdeb.sh>bba9512071b8d060a00f0567b1c11be7887f6b3748d0eb337579b7a2b836dd21</installdeb.sh>
</sha256>
*/

bool create_hashfile(const std::string &hashfile,std::string hashedFiles[])
{
  FILE *fptr = fopen(hashfile.c_str(),"w");
  if (!fptr)
    {
      std::cout<<"Failed to open hashes file"<<std::endl;
      return false;
    }

   int index = 0;
   fprintf(fptr,"%s",XML_TAG_A);
   fprintf(fptr,"<%s>","sha256");
   for (int index = 0; hashedFiles[index].length() ; index++)
   {
	   char Message_Digest[SHA256StringSize]={0};
	   FILE *fptrshs = fopen(hashedFiles[index].c_str(),"rb");   
	   if(fptrshs)
	   {
		 if(shaSuccess == SHA256File(fptrshs,Message_Digest))
		 {
			std::cout<<hashedFiles[index].c_str()<<std::endl;
			std::cout<<Message_Digest<<std::endl;
			fprintf(fptr,"<%s>%s</%s>",hashedFiles[index].c_str(),Message_Digest,hashedFiles[index].c_str());
		 }
	  
		 fclose(fptrshs);
		 fptrshs =NULL;

	   }
   }
  fprintf(fptr,"</%s>","sha256");
  fclose(fptr);
  fptr =NULL;
  return true;
}
bool getRelativeBranchPath(const std::wstring &path,const std::wstring &brachkey, std::wstring &branchpath)
 {				
	size_t position = path.rfind(brachkey);
	if (std::wstring::npos != position)
	{
		size_t eposition =  path.rfind(L'\\');
		if (std::wstring::npos != eposition)
		{
			if(eposition>position)
			{
				branchpath = path.substr(position, eposition-position);
				std::replace( branchpath.begin(), branchpath.end(), L'\\',L'/');
				return true;
			}
		}
	}

	return false;
}

bool update_PackageInfoXml()
{
	wchar_t mFilename[MAX_PATH] = {0};
	std::wstring branchinfo;
	bool ret = false;
	
	GetModuleFileNameW(NULL,mFilename,MAX_PATH);
	std::wstring path(mFilename);
	if(!ret && !(ret=getRelativeBranchPath(path,L"\\Current",branchinfo)))
	{
		std::cout<<"Failed to get current package branch info"<<std::endl;
	}
	
	if(!ret && !(ret=getRelativeBranchPath(path,L"\\Previous",branchinfo)))
	{
		std::cout<<"Failed to get previous package branch info"<<std::endl;
	}
	
	if(!ret && !(ret=getRelativeBranchPath(path,L"\\Evaluation",branchinfo)))
	{
		std::cout<<"Failed to get evaluation package branch info"<<std::endl;
	}

	if(!ret) return false;

	FILE *fptr = fopen(PackageInfo.c_str(),"w");
	if (!fptr)
	{
		std::cout<<"Failed to open packageInfo file"<<std::endl;
		return false;
	}

	fwprintf(fptr,L"%s",XML_TAG_W);				  
	fwprintf(fptr,L"<location>%s</location>",branchinfo.c_str());
	fclose(fptr);
	fptr = NULL;
	return true;
}

int	appendData(char* exePath, char* dataFilePath)
{
	
	struct stat buffer ={0};
	char byte;
	char exeSize[128] ={0};
	char dataSize[128] ={0};
	int succeeded = 0;
	
	if(-1 == stat(exePath, &buffer))
	{
		std::cerr << "Couldn't find the specified executable :" << exePath << "\n";
		return 0;
	}
	//_ltoa_s(buffer.st_size,exeSize,128,10);
	sprintf(exeSize, "%010d", buffer.st_size);
	memset(&buffer,0x0,sizeof(struct stat));

	if(-1 ==stat(dataFilePath, &buffer))
	{
		std::cerr << "Couldn't find the specified data file:" << dataFilePath << "\n";
		return 0;
	}
	
	sprintf(dataSize, "%010d", buffer.st_size);
	//_ltoa_s(buffer.st_size,dataSize,128,10);
	
	FILE *streamExe = NULL, *streamData = NULL;
	if((streamExe  = fopen( exePath, "ab" )) == NULL)
	{
       	std::cerr << "Couldn't open the specified executable file for append data:" << exePath << "\n";
		return 0;
	}

    if( (streamData = fopen(dataFilePath, "rb" )) == NULL)
	{
		std::cerr << "Couldn't open the specified data file for read:" << dataFilePath << "\n";
		return 0;
	}
	
	for(;;)
	{
		if(1 == fread(&byte,sizeof(char),1,streamData))
		{
			if(1!= fwrite(&byte,sizeof(char),1,streamExe))
			{
				std::cout<<"Write error occured for"<< exePath <<std::endl;
				break;
			}
		}
		else 
		{
			if(feof(streamData))
			{
					std::cout<<"Read and write finished"<<std::endl;
					succeeded = 1;

			}
			
			if(ferror(streamData))
					std::cout<<"Read and write error occured"<<std::endl;
			break;
		}
	}

/*	for(int j=0;succeeded && fileSize[j]; j++)
	{
		if(1!= fwrite(&(fileSize[j]),sizeof(char),1,streamExe))
			{
				std::cout<<"Write error occured while injecting data file szie"<< exePath <<std::endl;
				break;
			}
	}
*/	
	if(succeeded)
	{
		fprintf(streamExe,"<bs>%s</bs>",exeSize);
		fprintf(streamExe,"<ds>%s</ds>",dataSize);
	}

	if(streamData) 
	{
		fclose(streamData);
		streamData =NULL;
	}
	if(streamExe)
	{
		fclose(streamExe);
		streamExe = NULL; 
	}
		
	return 0;
}


int extractData(char* exePath,char* dataFilePath )
{

	struct stat buffer ={0};
	char byte;
	long iexeSize=0;
	long idataSize=0;;
	int succeeded = 0;
	
	if(-1 == stat(exePath, &buffer))
	{
		std::cerr << "Couldn't find the specified executable :" << exePath << "\n";
		return 0;
	}
	
	FILE *streamExe = NULL, *streamData = NULL;
	if((streamExe  = fopen( exePath, "rb" )) == NULL)
	{
       	std::cerr << "Couldn't open the specified executable file in read mode:" << exePath << "\n";
		return 0;
	}

    if( (streamData = fopen(dataFilePath, "wb" )) == NULL)
	{
		std::cerr << "Couldn't open the specified data file for read:" << dataFilePath << "\n";
		return 0;
	}
	
	if( 0== fseek(streamExe,buffer.st_size-19,SEEK_SET))
	{
		char sizeinfo[128]={0};
		fread(sizeinfo,sizeof(char),19,streamExe);

		if(strlen(sizeinfo)==19 && strstr(sizeinfo,"<ds>") && strstr(sizeinfo,"</ds>"))
		{
			char * ptr = sizeinfo+4;			
			idataSize = atol(ptr);
		}
	}
			
	if( 0== fseek(streamExe,buffer.st_size-38,SEEK_SET))
	{
		char sizeinfo[128]={0};
		fread(sizeinfo,sizeof(char),19,streamExe);

		if(strlen(sizeinfo) && strstr(sizeinfo,"<bs>") && strstr(sizeinfo,"</bs>"))
		{
			char * ptr = sizeinfo+4;
			iexeSize = atol(ptr);
		}
	}

	if((iexeSize + idataSize + 38) == buffer.st_size)
	{
			
		fseek(streamExe,iexeSize,SEEK_SET);

			for(int k = 0;k<idataSize;k++)
			{
				if(1 == fread(&byte,sizeof(char),1,streamExe))
				{
					if(1!= fwrite(&byte,sizeof(char),1,streamData))
					{
						std::cout<<"Write error occured for"<< exePath <<std::endl;
						break;
					}
				}
				else 
				{
					if(feof(streamExe))
					{
							std::cout<<"Read and write finished before required data"<<std::endl;
							succeeded = 1;

					}
			
					if(ferror(streamExe))
							std::cout<<"Read and write error occured"<<std::endl;
					break;
				}
			}
			std::cout<<"Read and write finished"<<std::endl;
			succeeded = 1;
		}
		else
		{
			std::cout<<"Couldn't read size of files"<<std::endl;
		}
	
	if(streamData) 
	{
		fclose(streamData);
		streamData =NULL;
	}
	if(streamExe)
	{
		fclose(streamExe);
		streamExe = NULL; 
	}
		return 0;
}


int createPackage()
{
	bool isLinux = (file_exists(mfecma_rpm) && file_exists(mfert_rpm)) || (file_exists(mfecma_deb) && file_exists(mfert_deb));
    bool isHPPARisc = file_exists(mfemsapa_depot);
    bool isHPItanium = file_exists(mfemsaia_depot);
    bool isAix = file_exists(mfemsa_rpm);
    bool isOsx = file_exists(mfecma_dmg);
    bool isSolaris = file_exists(cmaadmin) && file_exists(mfecma_pkg);
    bool isScm = file_exists(mfecma_scm_rpm);
	bool isMlos = file_exists(mfema_mlos_rpm);
    //Base install types for all platforms
    InstallType types[5] = {INSTALL, INSTALL_END};
    std::string outputFiles[5] = {out_file};
	std::string install_outputFiles[5] = {install_zip};
	std::string hashed_Files[6] = {stub_file};
    
    //Linux requires installrpm.sh and installdeb.sh in addition to install.sh
    if (isLinux)
    {
      types[1] = INSTALLRPM; types[2] = INSTALLDEB; types[3] = INSTALL_END;
      outputFiles[1] = out_file_rpm; outputFiles[2] = out_file_deb;
	  install_outputFiles[1] = installrpm_zip; install_outputFiles[2] = installdeb_zip;
	  hashed_Files[1] = mfecma_rpm; hashed_Files[2] = mfert_rpm;hashed_Files[3] = mfecma_deb; hashed_Files[4]=mfert_deb; hashed_Files[5]= "";
    }

    //If the package contains MFEcma.depot and MFEcma_itanium.depot, install.sh
    //installhppa.sh and installhpia.sh should get created
    if (isHPItanium && isHPPARisc)
    {
      types[1] = INSTALLHPPA; types[2] = INSTALLHPIA; types[3] = INSTALL_END;
      outputFiles[1] = out_file_hppa; outputFiles[2] = out_file_hpia;
      install_outputFiles[1] = installhppa_zip; install_outputFiles[2] = installhpia_zip;
	  hashed_Files[1] = mfemsapa_depot; hashed_Files[2]=mfemsaia_depot ;hashed_Files[3]="";

    } 
    else if (isHPItanium)
    {
      types[1] = INSTALLHPIA; types[2] = INSTALL_END;
      outputFiles[1] = out_file_hpia;
      install_outputFiles[1] = installhpia_zip;
	  hashed_Files[1] = mfemsaia_depot;hashed_Files[2]="";
    }
    else if (isHPPARisc)
    {
      types[1] = INSTALLHPPA; types[2] = INSTALL_END;
      outputFiles[1] = out_file_hppa;
      install_outputFiles[1] = installhppa_zip;
	  hashed_Files[1] = mfemsapa_depot ;hashed_Files[2]="";
    }
	if (isAix)
    {
	  hashed_Files[1] = mfemsa_rpm ;hashed_Files[2]="";
    }
	if (isOsx)
    {
	  hashed_Files[1] = mfecma_dmg ;hashed_Files[2]="";
    }
	if (isSolaris)
    {
	  hashed_Files[1] = mfecma_pkg ;hashed_Files[2] = cmaadmin;hashed_Files[3]="";
    }
	if (isScm)
    {
	  hashed_Files[1] = mfecma_scm_rpm;hashed_Files[2]="";
    }
	if (isMlos)
	{
      types[1] = INSTALLRPM; types[2] = INSTALL_END;
      outputFiles[1] = out_file_rpm; 
	  install_outputFiles[1] = installrpm_zip;
	  hashed_Files[1] = mfema_mlos_rpm;hashed_Files[2]="";
	}
   
	int index = 0;
    for (int index = 0; INSTALL_END != types[index]; index++)
    {
		create_zip(types[index],INSTALL_SH_ZIP);	
		if (!create_empty_out_file(outputFiles[index]))
			return 1;
		append_unzip_exe_and_zip(outputFiles[index],stub_file,unzip_exe,out_zip);
		DeleteFile(out_zip.c_str());
		if(create_zip(types[index],FRAMEWORK_INSTALL_ZIP))			
			MoveFileEx(out_zip.c_str(),install_outputFiles[index].c_str(),MOVEFILE_REPLACE_EXISTING);
    }
	if(create_hashfile(hashes,hashed_Files))
	{
		//no need to check return value -backward compatibility < 5.0
		update_PackageInfoXml(); 
		if(create_zip(INSTALL_END,FRAMEWORK_CONFIG_ZIP))
			MoveFileEx(out_zip.c_str(),config_zip.c_str(),MOVEFILE_REPLACE_EXISTING);
	}
	return 0;
}

void help()
{
	std::cout << "Usage 1: Inject data to the given executable: "
		"unixpkg.exe -e=<Path to the executable> -d=<Path to the file containing data to append> -i\n\n"
		"Usage 2: Generate bootstrap.sh to the given executable: "
		"unixpkg.exe -e=<Path to the executable> -d=<Path to the file containing data to append>\n\n"
		"Usage 3: Help: unixpkg.exe -?\n\n"
		"Usage 4: Generate package: unixpkg.exe <With or without arguments>\n\n"
		"Usage 5: Generate package: -e=<Path to the executable> -r=<Path to a file to extract data from the exeutable>\n\n";
}




std::string RemoveTrailingSpace(const std::string& Value) {

	std::string	newString = Value;

	if(newString[newString.size() - 1] == ' ') {

		newString[newString.size() - 1] = '\0';
		newString.resize(newString.size() - 1);
	}

	return newString;
}

char* FindOneOf(char* p1, char* p2) {

    while (p1 != NULL && *p1 != NULL) {

        char* p = p2;
        while (p != NULL && *p != NULL) {

            if (*p1 == *p)
                return CharNext(p1);
            p = CharNext(p);
        }

        p1 = CharNext(p1);
    }

    return NULL;
}

char* GetSwitchData(char * lpParam, char * lpszRetData, WORD wSize) {

    char *	lpData	= NULL;
    WORD	wIndex	= 0;

    lpData = strchr(lpParam, STR_FRAMEINST_ASSIGN);
    
	if (lpData) {

        lpData++;

        while (*lpData && *lpData != '/' && *lpData != '-' && wIndex < wSize - 1) {

            lpszRetData[wIndex++] = *lpData++;
        }

        lpszRetData[wIndex] = '\0';
    }
    
    return lpData;
}

int fileExist(const char *fileName)
{
	struct stat buf;
	if( 0 != stat(fileName , &buf) )
		return 0 ;
	else
		return 1;
}

//The first argument is the output file, second is 
int main(int argc, char* argv[])
{
	int opcode = 1;
	LPTSTR lpstrCmdLine;
	try
	{
		lpstrCmdLine = GetCommandLine(); //this line necessary for _ATL_MIN_CRT
		std::cout<<"Command line "<<  lpstrCmdLine << std::endl;

		std::string ExePath,DataPath,ExtractedPath,StrHelp,StrInject;

		for(LPCTSTR lpszToken = FindOneOf(lpstrCmdLine, STR_FRAMEINST_TOKENS);
			lpszToken != NULL;
			lpszToken = FindOneOf((char *)lpszToken, STR_FRAMEINST_TOKENS))
		{
    		TCHAR szTemp[MAX_PATH];
			if (strnicmp(lpszToken, _T("?"), _tcslen(_T("?"))) == 0) {

				GetSwitchData((char *)lpszToken, szTemp, STR_SIZEOF(szTemp));
				StrHelp		= "?";
				opcode = 0;
			}
			else if (strnicmp(lpszToken, _T("i"), _tcslen(_T("i"))) == 0) {

				GetSwitchData((char *)lpszToken, szTemp, STR_SIZEOF(szTemp));
				StrInject		= "i";
				opcode = 0;
			}
			else if (_tcsnicmp(lpszToken, _T("r"), _tcslen(_T("r"))) == 0)
			{
				GetSwitchData((char *)lpszToken, szTemp, STR_SIZEOF(szTemp));
				ExtractedPath			= RemoveTrailingSpace(szTemp);
				opcode = 0;
			}
			else if (_tcsnicmp(lpszToken, _T("e"), _tcslen(_T("e"))) == 0)
			{
				GetSwitchData((char *)lpszToken, szTemp, STR_SIZEOF(szTemp));
				ExePath			= RemoveTrailingSpace(szTemp);
				opcode = 0;
			}
			else if (_tcsnicmp(lpszToken, _T("d"), _tcslen(_T("d"))) == 0)
			{
				GetSwitchData((char *)lpszToken, szTemp, STR_SIZEOF(szTemp));
				DataPath			= RemoveTrailingSpace(szTemp);
				opcode = 0;
			}
			else
			{
				opcode = 1;
			}
		}
/*
		{

		FILE * ptr = fopen( "c.txt","a+");
		
		for(int j=0 ;j<argc; j++)
			fprintf(ptr,"%s\n",argv[j]);
		  fclose(ptr);
		}
*/
		if(opcode == 1)
		{
			help();
			std::cout<<"Creating package...\n"<<std::endl;
			createPackage();
		}
		if(ExePath.length() && DataPath.length() && StrInject.length())
		{
			std::cout<<"Injecting data to the given executable...\n"<<std::endl;
			if(fileExist(ExePath.c_str()) && fileExist(DataPath.c_str()))
				appendData((char *)ExePath.c_str() ,(char *)DataPath.c_str());
		}
		if(ExePath.length() && DataPath.length() && !StrInject.length())
		{
			
			if(fileExist(bootstrap_sh.c_str()))
				DeleteFile(bootstrap_sh.c_str());
			//Append the bootstrap.exe in bootstrap.sfx(script file) and generate bootstrap.sh
			if(fileExist(ExePath.c_str()) && fileExist(DataPath.c_str()))
			{
				if(file_exists(mfecma_dmg))
				{
					std::cout<<"Creating bootstrap.app.zip ...\n"<<std::endl;
					AddConInfoToAppZip(ExePath,DataPath);
				}
				else
				{ 
					std::cout<<"Creating bootstrap.sh...\n"<<std::endl;
					append_unzip_exe_and_zip(bootstrap_sh,bootstrap_sfx,ExePath,DataPath);
				}
			}
		}

		if(ExePath.length() && ExtractedPath.length())
		{
			std::cout<<"Extracting data from given executable...\n"<<std::endl;
			 extractData((char *)ExePath.c_str(),(char *)ExtractedPath.c_str());
		}
	   if(StrHelp.length())
		{
			std::cout<<"Displaying the option help...\n"<<std::endl;
			 help();
		}

	  }
	  catch(std::exception &/*e*/)
	  {
		  std::cout<<"Exception while creating package items"<<std::endl;
		  return 1;
	  }
	  return 0;
}
