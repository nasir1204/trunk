#!/bin/bash
#########################################
# Don't invoke this script directly, it must be invoked via ma_packager.sh
#########################################

HasCRLFHenceExit()
{
    error=0
    echo "CRLF..."
	for i in `find $1`
    do
      file $i | cut -d: -f2 | grep -w "\([Ss]cript\)\|\(ASCII..*text\)" >/dev/null
      if (( $? == 0 )); then
	  str1=`strings -1 $i | head -1`
	  str2=`cat $i | head -1`
	  if [ "$str1" != "$str2" ]; then
	      echo $i 
	      error=1
	  fi
      fi
    done
    if (( $error != 0 )); then
	echo "Have CRLF in Text files. Hence Exiting..." 
	exit 4;
    fi
}

################################################################
#Step 1: Creating directory and defines
################################################################
echo $'\n\n************* Step 1: Creating directory and defines'
PKG_CURRENT_DIR=`echo $0 | grep ".solaris_packager.sh" | sed -e 's/.solaris_packager.sh//g'`
echo "PKG_CURRENT_DIR =" $PKG_CURRENT_DIR
OUTPUT_DIR=$OUTPUT_DIR/$ARCH_BIT/$CONFIG_TYPE
echo Output dir is $OUTPUT_DIR
if [ ! -d $OUTPUT_DIR ]; then
    echo $OUTPUT_DIR directory doesnt exist, creating
    mkdir -p $OUTPUT_DIR
fi
#### constants to be used in rpm spec #####
PKG_DIR_PERMISSIONS=" 0755 root root"
PKG_FILE_PERMISSIONS=" 0755 root root"
PKG_FILE_PERMISSIONS_2=" 0744 root root"
PKG_FILE_MFE_PERMISSIONS=" 0744 mfe mfe"
PKG_SIG_FILE_PERMISSIONS=" 0400 root root"
PKG_DIR_CONST="d none "
PKG_FILE_CONST="f none "
PKG_LINK_CONST="s none "
####
SCRIPT_DIR=Linux
SOLARIS_PROTOTYPE=$PKG_CURRENT_DIR/prototype.template
echo "Using solaris prototype file $SOLARIS_PROTOTYPE"
rm -rf /tmp/ma_solaris_pkg
mkdir /tmp/ma_solaris_pkg
cp $SOLARIS_PROTOTYPE /tmp/ma_solaris_pkg/prototype
if [ x"$ARCH_BIT" == x"32" ] ;then
	SUFFIX_DIR=$CONFIG_TYPE
	MA_DEPS=MFErt
	TARGET=i686
fi
# MA binaries Path
MA_TARGETS_DIR=$MA_BASE/build/makefiles/$SUFFIX_DIR/
echo MA binaries Path $MA_TARGETS_DIR
if [ ! -e $MA_TARGETS_DIR ]; then
	echo MA targets dir $MA_TARGETS_DIR not found
	exit 1
fi

MA_SIG_FILE=$MA_BASE/build/packager/data/ma_msgbus_auth.sig
echo Sig file path is $MA_SIG_FILE
if [ -f $MA_SIG_FILE ]; then
	echo ma_msgbus_auth.sig file found
else
	echo $MA_SIG_FILE not found, Warning continuing without it by touching it
	touch $MA_SIG_FILE
fi

echo copying $MA_SIG_FILE to $MA_TARGETS_DIR for packaging 
cp $MA_SIG_FILE $MA_TARGETS_DIR

# MA Tools binaries Path  
MA_TOOLS_TARGETS_DIR=$MA_TOOLS
echo MA tools binaries Path $MA_TOOLS_TARGETS_DIR
if [ ! -e $MA_TOOLS_TARGETS_DIR ]; then
	echo MA Tools targets dir $MA_TOOLS_TARGETS_DIR not found
	exit 1
fi
# MA compat sdk binaries Path  
MA_COMPAT_SDK_TARGETS_DIR=$MA_COMPAT_SDK
echo MA compat sdk binaries Path $MA_COMPAT_SDK_TARGETS_DIR
if [ ! -e $MA_COMPAT_SDK_TARGETS_DIR ]; then
	echo MA compat sdk targets dir $MA_COMPAT_SDK_TARGETS_DIR not found
	exit 1
fi

# RPM build root path
PKG_BUILD_ROOT=/tmp/MFEma-$MA_VERSION-$PACKAGE_NUM-buildroot
echo The current rpm buildroot is $PKG_BUILD_ROOT
if [ -d $PKG_BUILD_ROOT ]; then
	rm -rf $PKG_BUILD_ROOT
fi    
echo Creating the rpm build root at $PKG_BUILD_ROOT
mkdir -p $PKG_BUILD_ROOT

MAJ_VER=`echo $MA_VERSION | cut -d. -f1`
echo $MAJ_VER
MIN_VER=`echo $MA_VERSION | cut -d. -f2`
echo $MIN_VER
SUB_VER=`echo $MA_VERSION | cut -d. -f3`
echo $SUB_VER

# The below env file contains all the required environment variables.
source $PKG_CURRENT_DIR/../ma_packager.env



################################################################
#Step 2: Creating install files and directory in buildroot
################################################################
echo $'\n\n************* Step 2: Creating install files and directory in buildroot'

DIRS_LIST=`cat $PKG_CURRENT_DIR/../ma_install_files | egrep  'dir:|dir_compat:' | cut -d: -f2`
for dir in `eval echo $DIRS_LIST`
do
	echo Creating $PKG_BUILD_ROOT/$dir
	mkdir -p $PKG_BUILD_ROOT/$dir
	echo $PKG_DIR_CONST $dir $PKG_DIR_PERMISSIONS >> /tmp/ma_solaris_pkg/prototype
done

# Copying data files into build root
FILES_LIST=`cat $PKG_CURRENT_DIR/../ma_install_files | grep data: | cut -d: -f2`
for file in `eval echo $FILES_LIST` 
do
	echo Copying $PKG_CURRENT_DIR/../../data/$file $PKG_BUILD_ROOT/$MA_DATA_PATH/data/$file
	cp $PKG_CURRENT_DIR/../../data/$file $PKG_BUILD_ROOT/$MA_DATA_PATH/data/$file
	echo $PKG_FILE_CONST $MA_DATA_PATH/data/$file $PKG_FILE_PERMISSIONS >> /tmp/ma_solaris_pkg/prototype
done

# Copying certs files into build root
FILES_LIST=`cat $PKG_CURRENT_DIR/../ma_install_files | grep certs: | cut -d: -f2`
for file in `eval echo $FILES_LIST` 
do
	echo Copying $MA_BASE/build/packager/data/certs/public/$file $PKG_BUILD_ROOT/$MA_DATA_PATH/certstore/$file
	cp $MA_BASE/build/packager/data/certs/public/$file $PKG_BUILD_ROOT/$MA_DATA_PATH/certstore/$file
	echo $PKG_FILE_CONST $MA_DATA_PATH/certstore/$file $PKG_FILE_PERMISSIONS >> /tmp/ma_solaris_pkg/prototype
done

# Copying config files into build root
FILES_LIST=`cat $PKG_CURRENT_DIR/../ma_install_files | grep config: | cut -d: -f2`
for file in `eval echo $FILES_LIST` 
do
	echo Copying $PKG_CURRENT_DIR/../data/$file $PKG_BUILD_ROOT/$MA_CONFIG_PATH/$MA_SOFTWAREID/$file
	cp $PKG_CURRENT_DIR/../data/$file $PKG_BUILD_ROOT/$MA_CONFIG_PATH/$MA_SOFTWAREID/$file
	echo $PKG_FILE_CONST $MA_CONFIG_PATH/$MA_SOFTWAREID/$file $PKG_FILE_PERMISSIONS >> /tmp/ma_solaris_pkg/prototype
done

if [ -e $PKG_CURRENT_DIR/../data/cmnupd_config.xml ]; then
	echo Copying $PKG_CURRENT_DIR/../data/cmnupd_config.xml $PKG_BUILD_ROOT/$MA_CONFIG_PATH/$MA_CMN_UPDATERID/config.xml
	cp $PKG_CURRENT_DIR/../data/cmnupd_config.xml $PKG_BUILD_ROOT/$MA_CONFIG_PATH/$MA_CMN_UPDATERID/config.xml
	echo $PKG_FILE_CONST $MA_CONFIG_PATH/$MA_CMN_UPDATERID/config.xml $PKG_FILE_PERMISSIONS >> /tmp/ma_solaris_pkg/prototype
fi

#if [ -e $PKG_CURRENT_DIR/../data/cma_config.xml ]; then
#	echo Copying $PKG_CURRENT_DIR/../data/cma_config.xml $PKG_BUILD_ROOT/$MA_CONFIG_PATH/$CMA_SOFTWAREID/config.xml
#	str="s:__MA_VERSION__:$MA_VERSION:g;s:__PACKAGE_NUMBER__:$PACKAGE_NUM:g;s:__MA_SOFTWARE_ID__:$CMA_SOFTWAREID:g;s:__MA_INSTALL_PATH__:$MA_INSTALL_PATH:g;s:__MA_UPDATER_PATH__:$MA_UPDATER_PATH:g;s:__MA_SHARED_LIB_PATH__:$MA_CMA_LPC_LIBS_PATH:g"
#	sed -e "$str" $PKG_CURRENT_DIR/../data/cma_config.xml > $PKG_BUILD_ROOT/$MA_CONFIG_PATH/$CMA_SOFTWAREID/config.xml	
#	echo $PKG_FILE_CONST $PKG_FILE_PERMISSIONS $MA_CONFIG_PATH/$CMA_SOFTWAREID/config.xml >> /tmp/ma_solaris_pkg/prototype
#fi

# Copying config files into build root
SCRIPT_LIST=`cat $PKG_CURRENT_DIR/../ma_install_files | grep script: | cut -d: -f2`
IS_COMPAT_SVC=1
RUNTIME_LIBRARY_FLAG="LD_LIBRARY_PATH"
for file in `eval echo $SCRIPT_LIST` 
do
	echo Copying $PKG_CURRENT_DIR/../scripts/$SCRIPT_DIR/$file $PKG_BUILD_ROOT/$$MA_INSTALL_PATH/scripts/$file
	str="s:__RUNTIME_LIBRARY_FLAG__:$RUNTIME_LIBRARY_FLAG:g;s:__PLATFORM__:$PLATFORM:g;s:__MA_SOFTWARE_ID__:$CMA_SOFTWAREID:g;s:__IS_COMPAT_SVC__:$IS_COMPAT_SVC:g;s:__MA_VERSION__:$MA_VERSION:g;s:__PACKAGE_NUMBER__:$PACKAGE_NUM:g;s:__MA_INSTALL_PATH__:$MA_INSTALL_PATH:g"
	sed -e "$str" $PKG_CURRENT_DIR/../scripts/$SCRIPT_DIR/$file > $PKG_BUILD_ROOT/$MA_INSTALL_PATH/scripts/$file	
	#cp $PKG_CURRENT_DIR/../scripts/$SCRIPT_DIR/$file $PKG_BUILD_ROOT/$MA_INSTALL_PATH/scripts/$file
	echo $PKG_FILE_CONST $MA_INSTALL_PATH/scripts/$file $PKG_FILE_PERMISSIONS >> /tmp/ma_solaris_pkg/prototype
done
# Copying all ma install lib files into build root
LIBS_LIST=`cat $PKG_CURRENT_DIR/../ma_install_files | egrep  'lib:|lib_compat:' | cut -d: -f2`
for file in `eval echo $LIBS_LIST` 
do
	MAJ_VER=`echo $MA_VERSION | cut -d. -f1`
	MIN_VER=`echo $MA_VERSION | cut -d. -f2`
	if [ -e $MA_TARGETS_DIR/$file.$MAJ_VER.$MIN_VER ]; then
		echo Copying file  $MA_TARGETS_DIR/$file.$MAJ_VER.$MIN_VER to $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/$file.$MAJ_VER.$MIN_VER
		cp $MA_TARGETS_DIR/$file.$MAJ_VER.$MIN_VER $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/$file.$MAJ_VER.$MIN_VER
		echo $PKG_FILE_CONST $MA_INSTALL_PATH/lib/$file.$MAJ_VER.$MIN_VER $PKG_FILE_PERMISSIONS >> /tmp/ma_solaris_pkg/prototype
		echo $PKG_LINK_CONST $MA_INSTALL_PATH/lib/$file.$MAJ_VER=$MA_INSTALL_PATH/lib/$file.$MAJ_VER.$MIN_VER  >> /tmp/ma_solaris_pkg/prototype
		echo $PKG_LINK_CONST $MA_INSTALL_PATH/lib/$file=$MA_INSTALL_PATH/lib/$file.$MAJ_VER.$MIN_VER  >> /tmp/ma_solaris_pkg/prototype
		cd $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/
		ln -sf $file.$MAJ_VER.$MIN_VER $file.$MAJ_VER
		ln -sf $file.$MAJ_VER $file
		cd $PKG_CURRENT_DIR
	fi
done
# Creating links in rsdk
RSDK_LIST=`cat $PKG_CURRENT_DIR/../ma_install_files | grep rsdk: | cut -d: -f2`
for file in `eval echo $RSDK_LIST` 
do
	MAJ_VER=`echo $MA_VERSION | cut -d. -f1`
	MIN_VER=`echo $MA_VERSION | cut -d. -f2`
	if [ -e $MA_TARGETS_DIR/$file.$MAJ_VER.$MIN_VER ]; then
		echo Copying file  $MA_TARGETS_DIR/$file.$MAJ_VER.$MIN_VER to $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/rsdk/$file.$MAJ_VER.$MIN_VER
		cp $MA_TARGETS_DIR/$file.$MAJ_VER.$MIN_VER $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/rsdk/$file.$MAJ_VER.$MIN_VER
		echo $PKG_FILE_CONST $MA_INSTALL_PATH/lib/rsdk/$file.$MAJ_VER.$MIN_VER $PKG_FILE_PERMISSIONS >> /tmp/ma_solaris_pkg/prototype
		echo $PKG_LINK_CONST $MA_INSTALL_PATH/lib/rsdk/$file.$MAJ_VER=$MA_INSTALL_PATH/lib/rsdk/$file.$MAJ_VER.$MIN_VER $PKG_FILE_PERMISSIONS  >> /tmp/ma_solaris_pkg/prototype
		echo $PKG_LINK_CONST $MA_INSTALL_PATH/lib/rsdk/$file=$MA_INSTALL_PATH/lib/rsdk/$file.$MAJ_VER.$MIN_VER $PKG_FILE_PERMISSIONS  >> /tmp/ma_solaris_pkg/prototype
		cd $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/rsdk
		ln -sf $file.$MAJ_VER.$MIN_VER $file.$MAJ_VER
		ln -sf $file.$MAJ_VER $file
		cd $PKG_CURRENT_DIR
	fi
done

# Copying all ma install bin files into build root
BINS_LIST=`cat $PKG_CURRENT_DIR/../ma_install_files | egrep  'bin:|bin_compat:' | cut -d: -f2`
for file in `eval echo $BINS_LIST` 
do
	if [ -e $MA_TARGETS_DIR/$file ]; then
		echo Copying file $MA_TARGETS_DIR/$file to $PKG_BUILD_ROOT/$MA_INSTALL_PATH/bin/$file
		cp $MA_TARGETS_DIR/$file $PKG_BUILD_ROOT/$MA_INSTALL_PATH/bin/$file
		if [ $file == "cmdagent" ] || [ $file == "msaconfig" ]; then
			echo $PKG_FILE_CONST $MA_INSTALL_PATH/bin/$file $PKG_FILE_PERMISSIONS >> /tmp/ma_solaris_pkg/prototype
		elif [ $file == "macmnsvc" ]; then
			echo $PKG_FILE_CONST $MA_INSTALL_PATH/bin/$file $PKG_FILE_MFE_PERMISSIONS >> /tmp/ma_solaris_pkg/prototype
		else
			echo $PKG_FILE_CONST $MA_INSTALL_PATH/bin/$file $PKG_FILE_PERMISSIONS_2 >> /tmp/ma_solaris_pkg/prototype
		fi		
	fi
done
# Copying mue resource file
if [ -e $MA_TARGETS_DIR/MueRes_0409.cat ]; then
	if [ x"mileaccess" == x"$LINUX_DIST" ]; then
		echo Copying file $MA_TARGETS_DIR/MueRes_0409.cat to $PKG_BUILD_ROOT/$MA_DATA_PATH/data/0409/MueRes_InUse.cat
		mkdir -p $PKG_BUILD_ROOT/$MA_DATA_PATH/data/0409/
		echo $PKG_DIR_CONST $MA_DATA_PATH/data/0409/ $PKG_DIR_PERMISSIONS >> /tmp/ma_solaris_pkg/prototype
		cp $MA_TARGETS_DIR/MueRes_0409.cat $PKG_BUILD_ROOT/$MA_DATA_PATH/data/0409/MueRes_InUse.cat
		echo $PKG_FILE_CONST $MA_DATA_PATH/data/0409/MueRes_InUse.cat $PKG_FILE_PERMISSIONS >> /tmp/ma_solaris_pkg/prototype	
	else
		echo Creating $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/0409
		mkdir -p $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/0409
		echo $PKG_DIR_CONST $MA_INSTALL_PATH/lib/0409 $PKG_DIR_PERMISSIONS >> /tmp/ma_solaris_pkg/prototype	
		echo Copying file $MA_TARGETS_DIR/MueRes_0409.cat to $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/0409/MueRes_InUse.cat
		cp $MA_TARGETS_DIR/MueRes_0409.cat $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/0409/MueRes_InUse.cat
		echo $PKG_FILE_CONST $MA_INSTALL_PATH/lib/0409/MueRes_InUse.cat $PKG_FILE_PERMISSIONS >> /tmp/ma_solaris_pkg/prototype	
	fi
fi


if [ -z "$ENCRYPT_DIR" ]; then 
echo "Already encrypted mcs sripts from source tree"
# Copying update script file
	if [ -e $MA_BASE/src/compat/mue/common/mue/scripts/MSA/encrypted/UpdateMain.McS ]; then
		echo Copying file $MA_BASE/src/compat/mue/common/mue/scripts/MSA/encrypted/UpdateMain.McS to $PKG_BUILD_ROOT/$MA_DATA_PATH/update/UpdateMain.McS
		cp $MA_BASE/src/compat/mue/common/mue/scripts/MSA/encrypted/UpdateMain.McS $PKG_BUILD_ROOT/$MA_DATA_PATH/update/UpdateMain.McS
		echo $PKG_FILE_CONST $MA_DATA_PATH/update/UpdateMain.McS $PKG_FILE_PERMISSIONS >> /tmp/ma_solaris_pkg/prototype
	fi

	# Copying Install script file
	if [ -e $MA_BASE/src/compat/mue/common/mue/scripts/MSA/encrypted/InstallMain.McS ]; then
		echo Copying file $MA_BASE/src/compat/mue/common/mue/scripts/MSA/encrypted/InstallMain.McS to $PKG_BUILD_ROOT/$MA_DATA_PATH/update/InstallMain.McS
		cp $MA_BASE/src/compat/mue/common/mue/scripts/MSA/encrypted/InstallMain.McS $PKG_BUILD_ROOT/$MA_DATA_PATH/update/InstallMain.McS
		echo $PKG_FILE_CONST $MA_DATA_PATH/update/InstallMain.McS $PKG_FILE_PERMISSIONS >> /tmp/ma_solaris_pkg/prototype
	fi
else
	echo "Encrypted mcs sripts from build room"
	# Copying update script file
	if [ -e $ENCRYPT_DIR/UpdateMain.McS ]; then
		echo Copying file $ENCRYPT_DIR/UpdateMain.McS to $PKG_BUILD_ROOT/$MA_DATA_PATH/update/UpdateMain.McS
		cp $ENCRYPT_DIR/UpdateMain.McS $PKG_BUILD_ROOT/$MA_DATA_PATH/update/UpdateMain.McS
		echo $PKG_FILE_CONST $MA_DATA_PATH/update/UpdateMain.McS $PKG_FILE_PERMISSIONS >> /tmp/ma_solaris_pkg/prototype
	fi

	# Copying Install script file
	if [ -e $ENCRYPT_DIR/InstallMain.McS ]; then
		echo Copying file $ENCRYPT_DIR/InstallMain.McS to $PKG_BUILD_ROOT/$MA_DATA_PATH/update/InstallMain.McS
		cp $ENCRYPT_DIR/InstallMain.McS $PKG_BUILD_ROOT/$MA_DATA_PATH/update/InstallMain.McS
		echo $PKG_FILE_CONST $MA_DATA_PATH/update/InstallMain.McS $PKG_FILE_PERMISSIONS >> /tmp/ma_solaris_pkg/prototype
	fi	
fi

# Copying all ma install sig files into build root
SIGS_LIST=`cat $PKG_CURRENT_DIR/../ma_install_files | grep sig: | cut -d: -f2`
for file in `eval echo $SIGS_LIST` 
do
	if [ -e $MA_TARGETS_DIR/$file ]; then
		echo Copying file $MA_TARGETS_DIR/$file to $PKG_BUILD_ROOT/$MA_INSTALL_PATH/bin/$file
		cp $MA_TARGETS_DIR/$file $PKG_BUILD_ROOT/$MA_INSTALL_PATH/bin/$file
		echo $PKG_FILE_CONST $MA_INSTALL_PATH/bin/$file $PKG_SIG_FILE_PERMISSIONS >> /tmp/ma_solaris_pkg/prototype
	fi
done

# Copying all ma tools lib files into build root
TOOLS_LIST=`cat $PKG_CURRENT_DIR/../ma_install_tools | grep $PLATFORM  | grep -v ^#`
for file in `eval echo $TOOLS_LIST` 
do
	lib_dir=`echo $file | cut -d: -f3`
	lib_name=`echo $file | cut -d: -f4`

	if [ "$lib_dir" == "mfecryptc" -a "$CRYPTO" == "openssl" ]; then
		lib_dir=mfecryptc_openssl
	fi

	if [ "$lib_dir" == "curl" -a "$CRYPTO" == "openssl" ]; then
		lib_dir=curl_openssl
	fi

	if [ -e $MA_TOOLS_TARGETS_DIR/$lib_dir/lib/$SUFFIX_DIR/$lib_name ]; then
		lib_name1=`echo $lib_name | cut -d. -f1-3`
		lib_name2=`echo $lib_name | cut -d. -f1-2`
		echo Copying file $MA_TOOLS_TARGETS_DIR/$lib_dir/lib/$SUFFIX_DIR/$lib_name to $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/tools/$lib_name
		cp $MA_TOOLS_TARGETS_DIR/$lib_dir/lib/$SUFFIX_DIR/$lib_name $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/tools/$lib_name
		echo $PKG_FILE_CONST $MA_INSTALL_PATH/lib/tools/$lib_name $PKG_FILE_PERMISSIONS >> /tmp/ma_solaris_pkg/prototype
		echo $PKG_LINK_CONST $MA_INSTALL_PATH/lib/tools/$lib_name1=$MA_INSTALL_PATH/lib/tools/$lib_name  >> /tmp/ma_solaris_pkg/prototype
		echo $PKG_LINK_CONST $MA_INSTALL_PATH/lib/tools/$lib_name2=$MA_INSTALL_PATH/lib/tools/$lib_name  >> /tmp/ma_solaris_pkg/prototype
		cd $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/tools
		ln -sf $lib_name $lib_name1
		ln -sf $lib_name1 $lib_name2
		cd $PKG_CURRENT_DIR
	fi
        if [ -e $MA_TOOLS_TARGETS_DIR/$lib_dir/bin/$SUFFIX_DIR/$lib_name ]; then
                if [ -f $MA_TOOLS_TARGETS_DIR/$lib_dir/bin/$SUFFIX_DIR/$lib_name ]; then
                        echo Copying file $MA_TOOLS_TARGETS_DIR/$lib_dir/bin/$SUFFIX_DIR/$lib_name to $PKG_BUILD_ROOT/$MA_INSTALL_PATH/bin/$lib_name >> /tmp/ma_solaris_pkg/prototype
                        cp $MA_TOOLS_TARGETS_DIR/$lib_dir/bin/$SUFFIX_DIR/$lib_name $PKG_BUILD_ROOT/$MA_INSTALL_PATH/bin/$lib_name
                        chmod +x $PKG_BUILD_ROOT/$MA_INSTALL_PATH/bin/$lib_name
                fi
        fi
done

echo ******* $CRYPTO ***************
# RSA or OPENSSL files copying into tools folder, 
if [ "$CRYPTO" == "openssl" ]
then
	cp $MA_TOOLS_TARGETS_DIR/mfecryptc_openssl/lib/$SUFFIX_DIR/libcrypto.so.1.0.0  $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/tools/libcrypto.so.1.0.0
	echo $PKG_FILE_CONST $MA_INSTALL_PATH/lib/tools/libcrypto.so.1.0.0 $PKG_FILE_PERMISSIONS >> /tmp/ma_solaris_pkg/prototype
	echo $PKG_FILE_CONST $MA_INSTALL_PATH/lib/tools/libcrypto.so $PKG_FILE_PERMISSIONS >> /tmp/ma_solaris_pkg/prototype
	cp $MA_TOOLS_TARGETS_DIR/mfecryptc_openssl/lib/$SUFFIX_DIR/libssl.so.1.0.0  $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/tools/libssl.so.1.0.0
	echo $PKG_FILE_CONST $MA_INSTALL_PATH/lib/tools/libssl.so.1.0.0 $PKG_FILE_PERMISSIONS >> /tmp/ma_solaris_pkg/prototype
	echo $PKG_FILE_CONST $MA_INSTALL_PATH/lib/tools/libssl.so $PKG_FILE_PERMISSIONS >> /tmp/ma_solaris_pkg/prototype
	cd $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/tools
	ln -sf libcrypto.so.1.0.0 libcrypto.so
	ln -sf libssl.so.1.0.0 libssl.so
	cd $PKG_CURRENT_DIR
else
	cp $MA_TOOLS_TARGETS_DIR/mfecryptc/lib/$SUFFIX_DIR/libccme_base.so  $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/tools/libccme_base.so
	echo $PKG_FILE_CONST $MA_INSTALL_PATH/lib/tools/libccme_base.so $PKG_FILE_PERMISSIONS >> /tmp/ma_solaris_pkg/prototype
	cp $MA_TOOLS_TARGETS_DIR/mfecryptc/lib/$SUFFIX_DIR/libccme_base_non_fips.so  $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/tools/libccme_base_non_fips.so
	echo $PKG_FILE_CONST $MA_INSTALL_PATH/lib/tools/libccme_base_non_fips.so $PKG_FILE_PERMISSIONS >> /tmp/ma_solaris_pkg/prototype
	cp $MA_TOOLS_TARGETS_DIR/mfecryptc/lib/$SUFFIX_DIR/libccme_asym.so  $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/tools/libccme_asym.so
	echo $PKG_FILE_CONST $MA_INSTALL_PATH/lib/tools/libccme_asym.so $PKG_FILE_PERMISSIONS >> /tmp/ma_solaris_pkg/prototype
	cp $MA_TOOLS_TARGETS_DIR/mfecryptc/lib/$SUFFIX_DIR/libcryptocme.sig  $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/tools/libcryptocme.sig
	echo $PKG_FILE_CONST $MA_INSTALL_PATH/lib/tools/libcryptocme.sig $PKG_SIG_FILE_PERMISSIONS >> /tmp/ma_solaris_pkg/prototype
	cp $MA_TOOLS_TARGETS_DIR/mfecryptc/lib/$SUFFIX_DIR/libcryptocme.so  $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/tools/libcryptocme.so
	echo $PKG_FILE_CONST $MA_INSTALL_PATH/lib/tools/libcryptocme.so $PKG_FILE_PERMISSIONS >> /tmp/ma_solaris_pkg/prototype
fi	

# Copying all compat tools lib files into build root
TOOLS_LIST=`cat $PKG_CURRENT_DIR/../ma_compat_install_tools | grep $PLATFORM | grep -v ^#`

echo Tool list for compat $TOOLS_LIST
for file in `eval echo $TOOLS_LIST` 
do
	lib_dir=`echo $file | cut -d: -f2`
	lib_name=`echo $file | cut -d: -f3`
	if [ -e $BOOST_BASE/lib/$lib_name ]; then
		lib_name1=`echo $lib_name | cut -d. -f1-3`
		lib_name2=`echo $lib_name | cut -d. -f1-2`
		echo Copying file $BOOST_BASE/lib/$lib_name to $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/tools/$lib_name
		cp $BOOST_BASE/lib/$lib_name $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/tools/$lib_name
		echo $PKG_FILE_CONST $MA_INSTALL_PATH/lib/tools/$lib_name $PKG_FILE_PERMISSIONS >> /tmp/ma_solaris_pkg/prototype
		echo $PKG_LINK_CONST $MA_INSTALL_PATH/lib/tools/$lib_name1=$MA_INSTALL_PATH/lib/tools/$lib_name >> /tmp/ma_solaris_pkg/prototype
		echo $PKG_LINK_CONST $MA_INSTALL_PATH/lib/tools/$lib_name2=$MA_INSTALL_PATH/lib/tools/$lib_name >> /tmp/ma_solaris_pkg/prototype
		cd $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/tools
		ln -sf $lib_name $lib_name1
		ln -sf $lib_name1 $lib_name2
		cd $PKG_CURRENT_DIR
	fi
done


LIB_STD=libstdc++.so.6.0.3
LIB_GCC=libgcc_s.so.1
LIB_ICONV=libiconv.so.2.4.0
echo $PKG_FILE_CONST $MA_INSTALL_PATH/lib/$LIB_STD $PKG_FILE_PERMISSIONS >> /tmp/ma_solaris_pkg/prototype
echo $PKG_FILE_CONST $MA_INSTALL_PATH/lib/$LIB_GCC $PKG_FILE_PERMISSIONS >> /tmp/ma_solaris_pkg/prototype
echo $PKG_FILE_CONST $MA_INSTALL_PATH/lib/$LIB_ICONV $PKG_FILE_PERMISSIONS >> /tmp/ma_solaris_pkg/prototype
echo $PKG_LINK_CONST $MA_INSTALL_PATH/lib/libstdc++.so.6=$MA_INSTALL_PATH/lib/$LIB_STD >> /tmp/ma_solaris_pkg/prototype
echo $PKG_LINK_CONST $MA_INSTALL_PATH/lib/libiconv.so.2=$MA_INSTALL_PATH/lib/$LIB_ICONV >> /tmp/ma_solaris_pkg/prototype
cp /usr/local/lib/$LIB_STD $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/
if (( $? )); then echo "Copy Failed /usr/local/lib/$LIB_STD"; fi
cp /usr/local/lib/$LIB_GCC $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/
if (( $? )); then echo "Copy Failed /usr/local/lib/$LIB_GCC"; fi
cp /usr/local/lib/$LIB_ICONV $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/
if (( $? )); then echo "Copy Failed /usr/local/lib/$LIB_ICONV"; fi
cd $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib
ln -sf $LIB_STD libstdc++.so.6
ln -s $LIB_ICONV libiconv.so.2
##################
#Creating symbolic links (moved from postinstall script)

#echo $PKG_DIR_CONST $CMA_INSTALL_PATH >> /tmp/ma_solaris_pkg/prototype
#ln -sf $MA_INSTALL_PATH $PKG_BUILD_ROOT/$CMA_INSTALL_PATH

#echo $PKG_DIR_CONST $CMA_CONFIG_PATH/$CMA_SOFTWAREID >> /tmp/ma_solaris_pkg/prototype
#ln -sf $MA_CONFIG_PATH/$MA_SOFTWAREID $PKG_BUILD_ROOT/$CMA_CONFIG_PATH/$CMA_SOFTWAREID

#echo $PKG_DIR_CONST $CMA_CONFIG_PATH/$MA_CMN_UPDATERID >> /tmp/ma_solaris_pkg/prototype
#ln -sf $MA_CONFIG_PATH/$MA_CMN_UPDATERID $PKG_BUILD_ROOT/$CMA_CONFIG_PATH/$MA_CMN_UPDATERID

ln -sf $MA_DATA_PATH/data $PKG_BUILD_ROOT/$MA_INSTALL_PATH/scratch

#echo $PKG_DIR_CONST $MA_INSTALL_PATH/scratch >> /tmp/ma_solaris_pkg/prototype
#ln -sf $MA_DATA_PATH/scratch/ $PKG_BUILD_ROOT/$MA_INSTALL_PATH/scratch

#echo $PKG_FILE_CONST $PKG_FILE_PERMISSIONS $MA_CMA_LPC_LIBS_PATH/libmfelpc.so >> /tmp/ma_solaris_pkg/prototype
#ln -sf $MA_INSTALL_PATH/lib/libmfelpc.so $PKG_BUILD_ROOT/$MA_CMA_LPC_LIBS_PATH/libmfelpc.so
#if [ x"$ARCH_BIT" == x"64" ]
#then
#	echo $PKG_DIR_CONST ${MA_CMA_LPC_LIBS_PATH}/lib64 >> /tmp/ma_solaris_pkg/prototype
#	mkdir -p $PKG_BUILD_ROOT/${MA_CMA_LPC_LIBS_PATH}/lib64
#	echo $PKG_FILE_CONST $PKG_FILE_PERMISSIONS $MA_CMA_LPC_LIBS_PATH/lib64/libmfelpc.so >> /tmp/ma_solaris_pkg/prototype
#	cd $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib/
#	ln -sf libmfelpc.so $PKG_BUILD_ROOT/$MA_CMA_LPC_LIBS_PATH/lib64/libmfelpc.so
#fi
if [ x"mileaccess" != x"$LINUX_DIST" ]; then
	cd $PKG_BUILD_ROOT/$MA_INSTALL_PATH/lib
	ln -sf libeventwriter.so.5.0 libeventwriter.so.4
	ln -sf libeventinterface.so.5.0 libeventinterface.so.4	
	ln -sf libppupdaterstub.so.5.0 libppupdaterstub.so.4	
	ln -sf libnaxml.so.5.0 libnaxml.so.4
	ln -sf libmfecomn.so.5.0 libmfecomn.so.4
	ln -sf libnairal.so.5.0 libnairal.so.4
fi


echo $'\n************* Copy files done **************'
################################################################
#Step 3: Creating install files and directory in buildroot
################################################################
echo $'\n\n************* Step 3: Creating installer **************'



# Update placeholders in config.xml
MA_START_CMD="/etc/init.d/ma start"
MA_STOP_CMD="/etc/init.d/ma stop"
MA_UNINSTALL_CMD="pkgrm MFEcma"
str="s:__MA_UPDATER_PATH__:$MA_UPDATER_PATH:g;s:__MA_SOFTWARE_ID__:$MA_SOFTWAREID:g;s:__MA_VERSION__:$MA_VERSION:g;s:__PACKAGE_NUMBER__:$PACKAGE_NUM:g;s:__MA_DATA_PATH__:$MA_DATA_PATH:g;s:__MA_INSTALL_PATH__:$MA_INSTALL_PATH:g;s:__MA_START_CMD__:$MA_START_CMD:g;s:__MA_STOP_CMD__:$MA_STOP_CMD:g;s:__MA_UNINSTALL_CMD__:$MA_UNINSTALL_CMD:g;s:__MA_SHARED_LIB_PATH__:$MA_CMA_LPC_LIBS_PATH:g"
sed -e "$str" $PKG_CURRENT_DIR/../data/config.xml > $PKG_BUILD_ROOT/$MA_CONFIG_PATH/$MA_SOFTWAREID/config.xml

str="s:__MA_VERSION__:$MA_VERSION:g;s:__PACKAGE_NUMBER__:$PACKAGE_NUM:g;s:__MA_INSTALL_PATH__:$MA_INSTALL_PATH:g;s:__MA_DATA_PATH__:$MA_DATA_PATH:g"
sed -e "$str" $PKG_CURRENT_DIR/../data/mainfo.ini > $PKG_BUILD_ROOT/$MA_CONFIG_PATH/mainfo.ini 
echo $PKG_FILE_CONST /etc/ma.d/mainfo.ini $PKG_FILE_PERMISSIONS >> /tmp/ma_solaris_pkg/prototype

if [ x"mileaccess" != x"$LINUX_DIST" ]; then	
	cp $PKG_CURRENT_DIR/../data/msaconfig $PKG_BUILD_ROOT/$MA_INSTALL_PATH/bin/
	echo $PKG_FILE_CONST $MA_INSTALL_PATH/bin/msaconfig $PKG_FILE_PERMISSIONS >> /tmp/ma_solaris_pkg/prototype
fi
#str="s:__MA_SOFTWARE_ID__:$MA_SOFTWAREID:g;s:__MA_VERSION__:$MA_VERSION:g;s:__PACKAGE_NUMBER__:$PACKAGE_NUM:g;s:__MA_DATA_PATH__:$MA_DATA_PATH:g;s:__MA_CONFIG_PATH__:$MA_CONFIG_PATH:g;s:__MA_INSTALL_PATH__:$MA_INSTALL_PATH:g"
#sed -e "$str" $PKG_CURRENT_DIR/../scripts/$PLATFORM/ma > $PKG_BUILD_ROOT/$MA_INSTALL_PATH/scripts/ma

mkdir -p $PKG_BUILD_ROOT/etc/init.d
cp $PKG_BUILD_ROOT/$MA_INSTALL_PATH/scripts/ma $PKG_BUILD_ROOT/etc/init.d/ma
cd $PKG_BUILD_ROOT/etc/init.d/
#ln -sf  ma cma
echo $PKG_FILE_CONST /etc/init.d/ma $PKG_FILE_PERMISSIONS >> /tmp/ma_solaris_pkg/prototype
#echo $PKG_FILE_CONST /etc/init.d/cma $PKG_FILE_PERMISSIONS >> /tmp/ma_solaris_pkg/prototype
# TODO - strip $PKG_BUILD_ROOT/opt/mileaccess/cma/lib/lib*

HasCRLFHenceExit $PKG_BUILD_ROOT

#####################################################
#changing installer config files
str="s:__MA_VERSION__:$MA_VERSION:g;s:__PACKAGE_NUMBER__:$PACKAGE_NUM:g;s:__MA_SOFTWARE_ID__:$MA_SOFTWAREID:g;s:__MA_DATA_PATH__:$MA_DATA_PATH:g;s:__MA_MSGBUS_DATA_PATH__:$MA_MSGBUS_DATA_PATH:g;s:__MA_INSTALL_PATH__:$MA_INSTALL_PATH:g;s:__CMA_CONFIG_PATH__:$CMA_CONFIG_PATH:g;s:__CMA_SOFTWAREID__:$CMA_SOFTWAREID:g;s:__MA_CMN_UPDATERID__:$MA_CMN_UPDATERID:g;s:__MA_CONFIG_PATH__:$MA_CONFIG_PATH:g;s:__MA_CMA_LPC_LIBS_PATH__:$MA_CMA_LPC_LIBS_PATH:g;s:__LIB_EXTENSION__:$LIB_EXTENSION:g;s:__CMA_INSTALL_PATH__:$CMA_INSTALL_PATH:g;s:__CMA_SHARED_PATH__:$CMA_SHARED_PATH:g;"
sed -e "{$str}" $PKG_CURRENT_DIR/postinstall > /tmp/ma_solaris_pkg/postinstall
sed -e "{$str}" $PKG_CURRENT_DIR/preinstall > /tmp/ma_solaris_pkg/preinstall
sed -e "{$str}" $PKG_CURRENT_DIR/preremove > /tmp/ma_solaris_pkg/preremove
sed -e "{$str}" $PKG_CURRENT_DIR/checkinstall > /tmp/ma_solaris_pkg/checkinstall
sed -e "{$str}" $PKG_CURRENT_DIR/postremove.template > /tmp/ma_solaris_pkg/postremove

echo "Creating the pkginfo file for package "
PKGINFONAME=$PKG_CURRENT_DIR/pkginfo.template
echo "using  pkginfo file $PKGINFONAME"
PKG_ROOT=MFEcma
rm -rf /tmp/$PKG_ROOT
PACKAGE_TIME=`date`
str="s/PKG_ROOT/$PKG_ROOT/;s/VERSION_NUMBER/$MA_VERSION/;s/PACKAGE_TIME/$PACKAGE_TIME/;s/RELEASE_NUMBER/$PACKAGE_NUM/"
echo $str
sed -e "$str" $PKGINFONAME > /tmp/ma_solaris_pkg/pkginfo

PKGREQUESTNAME=$PKG_CURRENT_DIR/request.template
echo "Creating the request file for the package "
dfout=`du -sk $PKG_BUILD_ROOT`
pkgsize=`echo $dfout | cut -d' ' -f1`
reqsize=$(( (($pkgsize + 1023 )/1024)*1024 ))
echo "Package Size is $reqsize KB"
str="s/^REQUIRED_INSTALLATION_SIZE.*/REQUIRED_INSTALLATION_SIZE=$reqsize/"
echo $str
sed -e "$str" $PKGREQUESTNAME > /tmp/ma_solaris_pkg/request

#echo "Creating the postremove file for the package "
#POSTREMOVENAME=$PKG_CURRENT_DIR/postremove.template
#str="s/EnterCMASoftwareIdHere/$SOFTWAREID/g;s/EnterUpdaterSoftwareIdHere/$UPDATER_SOFTWAREID/g;s/EnterCmdAgentSoftwareIdHere/$CMDAGENT_SOFTWAREID/g"
  
echo "Building the Solaris package..."  
PKGCMD=pkgmk 
echo $PKGCMD -o -r $PKG_BUILD_ROOT  -d /tmp -f /tmp/ma_solaris_pkg/prototype
$PKGCMD -o -r $PKG_BUILD_ROOT  -d /tmp -f /tmp/ma_solaris_pkg/prototype
if (( $? )); then
	echo "FAILED to create the CMA package"
	exit 1 
fi

PKG_FILE_NAME=MFEcma-$MA_VERSION-$PACKAGE_NUM-$TARGET.pkg
if [ -f /tmp/$PKG_ROOT/install/preinstall ]; then
    chmod 755 /tmp/$PKG_ROOT/install/preinstall
fi
if [ -f /tmp/$PKG_ROOT/install/postinstall ]; then
    chmod 755 /tmp/$PKG_ROOT/install/postinstall
fi
if [ -f /tmp/$PKG_ROOT/install/preremove ]; then
    chmod 755 /tmp/$PKG_ROOT/install/preremove
fi
if [ -f /tmp/$PKG_ROOT/install/postremove ]; then
    chmod 755 /tmp/$PKG_ROOT/install/postremove
fi
if [ -f /tmp/$PKG_ROOT/install/checkinstall ]; then
    chmod 755 /tmp/$PKG_ROOT//install/checkinstall
fi

cd /tmp/
if (( $? != 0 )); then
     echo "packaging Failed. Exiting..."
     exit 1
fi

echo Changing the pkg format from filesystem /tmp/$PKG_ROOT to datastream /tmp/$PKG_FILE_NAME
echo pkgtrans -son /tmp  /tmp/$PKG_FILE_NAME MFEcma
pkgtrans -son /tmp  /tmp/$PKG_FILE_NAME MFEcma
echo Moving  /tmp/$PKG_FILE_NAME to $OUTPUT_DIR/$PKG_FILE_NAME
mv $PKG_FILE_NAME $OUTPUT_DIR/MFEma.pkg
cp $PKG_CURRENT_DIR/cmaadmin $OUTPUT_DIR/cmaadmin
###################################
#bootstrap  bootstrap_sfx.sh  PackageInfo.xml  BootstrapInfo.xml
strip $MA_TARGETS_DIR/bootstrap
if [ x"$UPX_PATH" != x"" ]; then
	echo "compressing with upx command $UPX_PATH -9 $MA_TARGETS_DIR/bootstrap"
	$UPX_PATH -9 $MA_TARGETS_DIR/bootstrap
fi
BOOTSTRAP_PAYLOAD="bootstrap"
cp $MA_TARGETS_DIR/bootstrap $OUTPUT_DIR/bootstrap
cp $PKG_CURRENT_DIR/../data/BootstrapInfo.xml $OUTPUT_DIR
cp $PKG_CURRENT_DIR/../data/PackageInfo.xml $OUTPUT_DIR
bootstrap_size=`ls  -l $MA_TARGETS_DIR/bootstrap | awk '{print $5}'`
str="s:__BOOSTRAP_SIZE__:$bootstrap_size:g;s:__BOOTSTRAP_PAYLOAD__:$BOOTSTRAP_PAYLOAD:g"
sed -e "$str" $PKG_CURRENT_DIR/../bootstrap_sfx/bootstrap_sfx.solaris.sh.template > $OUTPUT_DIR/bootstrap_sfx.sh
#moving PkgCatalog  to build out
echo "Creating PkgCatlog.xml for deployable package for MA"
sed -e "s:PACKAGE_NUMBER:$PACKAGE_NUM:g;s:<ProductName>mileaccess Agent for _PLATFORMID_NX_</ProductName>:<ProductName>mileaccess Agent for Solaris</ProductName>:g;s:_PLATFORMID_NX_:SLR:g;s:OUTPUT_FILELIST:\"install.sh;setup;FrameworkConfig.zip\":g;s:_PRODUCT_ID_:$CMA_SOFTWAREID:g;s:<InstallType>script</InstallType>:<InstallType>command</InstallType>:g;s:<InstallCommand>AgentInst.McS</InstallCommand>:<InstallCommand>setup</InstallCommand>:g;" $MA_BASE/build/packager/data/PkgCatalog_unx.xml > $OUTPUT_DIR/PkgCatalog.xml

#moving unz to build out
unzip_exe_size=`ls  -l $MA_TARGETS_DIR/unz | awk '{print $5}'`
str="s:UNZIP_SIZE_IN_BYTES:$unzip_exe_size:g;s:__MA_Major_Version__:$MAJ_VER:g;s:__MA_Minor_Version__:$MIN_VER:g;s:__MA_Patch_Version__:$SUB_VER:g;s:__MA_Build_Number__:$PACKAGE_NUM:g"
ls -l $MA_BASE/build/packager/unix/sfx/sfx.solaris.sh.template
sed -e "$str" $MA_BASE/build/packager/unix/sfx/sfx.solaris.sh.template > $OUTPUT_DIR/sfx.sh
cp $MA_TARGETS_DIR/unz $OUTPUT_DIR/unz

###################################
# Creating SDK tar file (sdk and beacon rpm files) in output folder
SDK_DIR=/tmp/sdk_temp
if [ -e $SDK_DIR ]
then
rm -fr $SDK_DIR
fi
mkdir -p $SDK_DIR
mkdir -p $SDK_DIR/samples
mkdir -p $SDK_DIR/include
mkdir -p $SDK_DIR/bindings
mkdir -p $SDK_DIR/lib/$SUFFIX_DIR
mkdir -p $SDK_DIR/rsdk/$SUFFIX_DIR
mkdir -p $SDK_DIR/masign
mkdir -p $SDK_DIR/certs

SAMPLES_DIR=$SDK_DIR/samples
cp -r $MA_BASE/tools/ma_sdk/include/* $SDK_DIR/include
cp -r $MA_BASE/tools/ma_sdk/bindings/* $SDK_DIR/bindings
cp -r $MA_BASE/tools/ma_sdk/lib/$SUFFIX_DIR/* $SDK_DIR/lib/$SUFFIX_DIR
cp -r $MA_BASE/tools/ma_sdk/rsdk/$SUFFIX_DIR/* $SDK_DIR/rsdk/$SUFFIX_DIR
cp -r $MA_TOOLS_TARGETS_DIR/masign/* $SDK_DIR/masign
cp -r $MA_BASE/build/packager/data/certs $SDK_DIR/
cp  $MA_BASE/build/packager/data/certs/ma_test_certs.tar $SDK_DIR/certs/

echo "Executing command sh $MA_BASE/tools/beacon/build/packager/unix/bc_packager.sh -v $MA_VERSION -n $PACKAGE_NUM -c $CONFIG_TYPE  -p $PLATFORM -b $ARCH_BIT -s $DATE_AND_TIME -o $SAMPLES_DIR"
#sh  $MA_BASE/tools/beacon/build/packager/unix/bc_packager.sh  -v $MA_VERSION -n $PACKAGE_NUM -c $CONFIG_TYPE  -p $PLATFORM -b $ARCH_BIT -s $DATE_AND_TIME -o $SAMPLES_DIR
#if (( $? )); then
#	echo "Failed to create the beacon Package"
#fi

cur_dir=`pwd`
#cd $MA_BASE/tools/beacon/
#tar cvf $SAMPLES_DIR/beacon.tar build include src
cd $cur_dir

cd $SDK_DIR
if [ x"mileaccess" == x"$LINUX_DIST" ] # For MLOS
then
tar cvf $OUTPUT_DIR/ma_sdk500_mlos.tar include lib bindings rsdk samples masign
else
tar cf $OUTPUT_DIR/ma_sdk500_$PLATFORM.tar include lib bindings rsdk samples masign
fi
cd $cur_dir
rm -fr $SDK_DIR
#if [ -d /tmp/$PKG_ROOT ]; then
#	rm -rf /tmp/$PKG_ROOT
#fi
if [ -d $PKG_BUILD_ROOT ]; then	
	cp -rf /tmp/$PKG_ROOT/install $OUTPUT_DIR
	cp /tmp/ma_solaris_pkg/prototype $OUTPUT_DIR
	rm -rf $PKG_BUILD_ROOT
fi
rm -rf /tmp/ma_solaris_pkg
rm -rf /tmp/$PKG_ROOT


