#!/bin/bash

add_cma_library_to_package()
{
    utype=$1
    in_location=$2
    out_location=$3
    major=$4
    minor=$5
    lib=$6
    if [ "$utype" == "linux" ] || [ "$utype" == "webshield" ] || [ "$utype" == "solaris" ] || [ "$utype" == "aix" ] || [ "$utype" == "mlos" ] || [ "$utype" == "lifeline" ]
	then
	echo Copying $in_location/lib$lib.so.$major.$minor to $out_location
	cp $in_location/lib$lib.so.$major.$minor $out_location
	cd $out_location
	if (( $? != 0 )); then
    		echo "packaging cma library  Failed. Exiting..."
		exit 1 
	fi
	echo Creating symlink lib$lib.so.$major.$minor lib$lib.so.$major 
	ln -sf lib$lib.so.$major.$minor lib$lib.so.$major 
	echo Creating symlink lib$lib.so.$major lib$lib.so 
	ln -sf lib$lib.so.$major lib$lib.so 
	cd -
    elif [ "$utype" == "hpux" ] 
	then
	echo Copying $in_location/lib$lib.sl.$major.$minor to $out_location
        cp $in_location/lib$lib.sl.$major.$minor $out_location
        cd $out_location
	if (( $? != 0 )); then
    		echo "packaging cma library  Failed. Exiting..."
    		exit 1
	fi
        echo Creating symlink lib$lib.sl.$major.$minor lib$lib.sl.$major
        ln -sf lib$lib.sl.$major.$minor lib$lib.sl.$major
        echo Creating symlink lib$lib.sl.$major lib$lib.sl
        ln -sf lib$lib.sl.$major lib$lib.sl
    else
	echo Copying $in_location/lib$lib.$major.$minor.dylib to $out_location
	cp $in_location/lib$lib.$major.$minor.dylib $out_location
	cd $out_location
	if (( $? != 0 )); then
    		echo "packaging cma library  Failed. Exiting..."
		exit 1 
	fi
	ln -sf lib$lib.$major.$minor.dylib lib$lib.$major.dylib 
	ln -sf lib$lib.$major.dylib lib$lib.dylib 
	cd -
    fi
}

add_cma_static_library_to_package()
{
  utype=$1
  in_location=$2
  out_location=$3
  lib=$4

  echo Copying $in_location/lib$lib.a to $out_location
  cp $in_location/lib$lib.a $out_location
  cd $out_location
  if (( $? != 0 )); then
    echo "packaging cma static library  Failed. Exiting..."
    exit 1 
  fi
  cd -
}
add_boost_libraries_to_package()
{
    BOOST_EXT="so"
    if [ ! -e $1/lib/libboost_filesystem.so.1.39.0 ]; then BOOST_EXT="sl"; fi
    echo Set the boost extension to $BOOST_EXT
    echo Copying boost libraries $1 $2 $3
    cp $1/lib/libboost_serialization.a $2/cmasdk/lib/shipped	
    cp $1/lib/libboost_wserialization.a $2/cmasdk/lib/shipped	
    cp $1/lib/libboost_filesystem.$BOOST_EXT.1.39.0 $2/cmasdk/lib/shipped	
    cp $1/lib/libboost_system.$BOOST_EXT.1.39.0 $2/cmasdk/lib/shipped	
    cp $1/lib/libboost_system.$BOOST_EXT.1.39.0 $2/cmasdk/lib/shipped	
}
create_sdk()
{
    cma_base_dir=$1
    sdk_unix_type=$2
    sdk_version=$3
    sdk_package_number=$4
    sdk_out_dir=$5
    sdk_configuration=$6
    sdk_major=`echo $sdk_version | cut -d. -f1`
    sdk_minor=`echo $sdk_version | cut -d. -f2`
	boost_sdk_dir=$7
	echo Check point 1 : $boost_sdk_dir
	if [ "$boost_sdk_dir" == "" ];then 
		boost_sdk_dir=$cma_base_dir/boost_masdk
		echo Check point 2 : $boost_sdk_dir
		if [ ! -d "$boost_sdk_dir" ];then 
			echo Check point 3 : $boost_sdk_dir
			boost_sdk_dir=
		fi
		echo Check point 4 : $boost_sdk_dir
	fi
	echo the value of boost_sdk_dir is $boost_sdk_dir

	mkdir -p $sdk_out_dir/cmasdk/include
	mkdir -p $sdk_out_dir/cmasdk/docs
	mkdir -p $sdk_out_dir/cmasdk/example
	mkdir -p $sdk_out_dir/cmasdk/ma_src
	mkdir -p $sdk_out_dir/cmasdk/project/unix
	mkdir -p $sdk_out_dir/cmasdk/lib/shipped

	mkdir -p $sdk_out_dir/cmasdk/include/manageability
    
	if [ "$boost_sdk_dir" != "" ]	; then 
		echo Copying boost sdk stuff to cmasdk
		cp -r $boost_sdk_dir $sdk_out_dir/cmasdk/boost	
		
    fi
    
    cp  $cma_base_dir/common/nwa/include/poplugin.h $sdk_out_dir/cmasdk/include/manageability
    cp  $cma_base_dir/common/nwa/include/taskstatus.h $sdk_out_dir/cmasdk/include/manageability

    #Put in the event interface headers
    cp -r $cma_base_dir/common/nwa/include/eventinterface $sdk_out_dir/cmasdk/include

    #Put in the event writer headers
    cp -r $cma_base_dir/common/nwa/include/eventwriter $sdk_out_dir/cmasdk/include

    #Put in LPC headers and generated lpc source
    cp -r $cma_base_dir/common/include/lpc $sdk_out_dir/cmasdk/include
    cp -r $cma_base_dir/common/include/mfelpchelper $sdk_out_dir/cmasdk/include
    cp -r $cma_base_dir/common/lpc $sdk_out_dir/cmasdk/ma_src
    cp -r $cma_base_dir/common/macrypto $sdk_out_dir/cmasdk/ma_src
    cp -r $cma_base_dir/../../../build/packager/unix/sdkmakefiles/*  $sdk_out_dir/cmasdk/project/unix
	cat $cma_base_dir/common/lpc/core/unix/Makefile | sed -e 's/CMA_BASE/SOURCE_BASE/g' -e 's/unix\/buildproject/project\/unix/g' >  $sdk_out_dir/cmasdk/ma_src/lpc/core/unix/Makefile
	cat $cma_base_dir/common/lpc/manageability/unix/Makefile | sed -e 's/CMA_BASE/SOURCE_BASE/g' -e 's/unix\/buildproject/project\/unix/g' >  $sdk_out_dir/cmasdk/ma_src/lpc/manageability/unix/Makefile
	cat $cma_base_dir/common/macrypto/unix/Makefile | sed -e 's/CMA_BASE/SOURCE_BASE/g' -e 's/unix\/buildproject/project\/unix/g' >  $sdk_out_dir/cmasdk/ma_src/macrypto/unix/Makefile
	mv $sdk_out_dir/cmasdk/ma_src/macrypto $sdk_out_dir/cmasdk/ma_src/mfelpchelper
    
    rm -rf $sdk_out_dir/cmasdk/ma_src/mfelpchelper/test
    rm -rf $sdk_out_dir/cmasdk/ma_src/mfelpchelper/win32
    rm -rf $sdk_out_dir/cmasdk/ma_src/mfelpchelper/unix/release
    rm -rf $sdk_out_dir/cmasdk/ma_src/lpc/core/test
    rm -rf $sdk_out_dir/cmasdk/ma_src/lpc/core/win32
    rm -rf $sdk_out_dir/cmasdk/ma_src/lpc/core/unix/release
    rm -rf $sdk_out_dir/cmasdk/ma_src/lpc/datachannel/test
    rm -rf $sdk_out_dir/cmasdk/ma_src/lpc/datachannel/win32
    rm -rf $sdk_out_dir/cmasdk/ma_src/lpc/datachannel/unix/release
    rm -rf $sdk_out_dir/cmasdk/ma_src/lpc/datachannel/Tools
    rm -rf $sdk_out_dir/cmasdk/ma_src/lpc/manageability/test
    rm -rf $sdk_out_dir/cmasdk/ma_src/lpc/manageability/win32
    rm -rf $sdk_out_dir/cmasdk/ma_src/lpc/manageability/unix/release
    rm -rf $sdk_out_dir/cmasdk/ma_src/lpc/example





	mkdir -p $sdk_out_dir/cmasdk/include/updater
	mkdir -p $sdk_out_dir/cmasdk/include/ipc/channel
    
    cp  $cma_base_dir/common/include/ipc/channel/ibase.hpp $sdk_out_dir/cmasdk/include/ipc/channel
    cp  $cma_base_dir/common/include/ipc/container/iupdateprogress.hpp $sdk_out_dir/cmasdk/include/updater
    cp  $cma_base_dir/common/include/ipc/container/iupdater.hpp $sdk_out_dir/cmasdk/include/updater
    cp  $cma_base_dir/common/include/updater/upddefs.h $sdk_out_dir/cmasdk/include/updater
    cp  $cma_base_dir/common/include/updater/updcallback.h $sdk_out_dir/cmasdk/include/updater
    

    add_cma_library_to_package $sdk_unix_type $cma_base_dir/../../../build/makefiles/$sdk_configuration $sdk_out_dir/cmasdk/lib/shipped $sdk_major $sdk_minor eventinterface
    add_cma_library_to_package $sdk_unix_type $cma_base_dir/../../../build/makefiles/$sdk_configuration $sdk_out_dir/cmasdk/lib/shipped $sdk_major $sdk_minor eventwriter
    add_cma_library_to_package $sdk_unix_type $cma_base_dir/../../../build/makefiles/$sdk_configuration $sdk_out_dir/cmasdk/lib/shipped  $sdk_major $sdk_minor mfecomn
    #don't need naisign/mfesign because encryption is removed from the event files
    #add_cma_library_to_package $sdk_unix_type $cma_base_dir/unix/$sdk_configuration $sdk_out_dir/cmasdk/lib  $sdk_major $sdk_minor mfesign
    add_cma_library_to_package $sdk_unix_type $cma_base_dir/../../../build/makefiles/$sdk_configuration $sdk_out_dir/cmasdk/lib/shipped  $sdk_major $sdk_minor naxml
    add_cma_library_to_package $sdk_unix_type $cma_base_dir/../../../build/makefiles/$sdk_configuration $sdk_out_dir/cmasdk/lib/shipped  $sdk_major $sdk_minor nairal
    add_cma_library_to_package $sdk_unix_type $cma_base_dir/../../../build/makefiles/$sdk_configuration $sdk_out_dir/cmasdk/lib/shipped  $sdk_major $sdk_minor ppupdaterstub
    #LPC library
    add_cma_library_to_package $sdk_unix_type $cma_base_dir/../../../build/makefiles/$sdk_configuration $sdk_out_dir/cmasdk/lib/shipped $sdk_major $sdk_minor mfelpc
    add_cma_library_to_package $sdk_unix_type $cma_base_dir/../../../build/makefiles/$sdk_configuration $sdk_out_dir/cmasdk/lib/shipped $sdk_major $sdk_minor mfelpchelper
    #LPC Services  library
    add_cma_static_library_to_package $sdk_unix_type $cma_base_dir/../../../build/makefiles/${sdk_configuration}_static $sdk_out_dir/cmasdk/lib/shipped mfelpcservices
 
    
	if [ "$unixtype" != "mlos" ]; then
		boost_base=$boost_sdk_dir
		add_boost_libraries_to_package $boost_base $sdk_out_dir $sdk_unix_type
	fi 
	cd $sdk_out_dir/cmasdk
	if (( $? != 0 )); then
		echo "packaging Failed. Exiting..."
		exit 1
	fi

	echo "Deleting SVN entries"
	for i in `find . -name ".svn" -type d`
	do
		echo Deleting SVN directories if any
		rm -rf $i
	done   
	cd - 
	echo creating tar.gz for sdk
	cd $sdk_out_dir
	if (( $? != 0 )); then
		echo "packaging Failed. Exiting..."
		exit 1 
	fi
	tar -cvf cmasdk.$sdk_unix_type.$sdk_version-$sdk_package_number.tar cmasdk
	gzip cmasdk.$sdk_unix_type.$sdk_version-$sdk_package_number.tar
	rm -rf cmasdk
	echo done creating sdk
}
printParams()
{
	echo "version=" $version
	echo "package_num=" $package_num
	echo "config_type=" $config_type
	echo "platform=" $platform
	echo "arch_bit=" $arch_bit
	echo "crypto=" $crypto
	echo "output_dir=" $output_dir
	echo "date_and_time=" $date_and_time
	echo "sign_dir=" $sign_dir
	echo "encrypt_dir=" $encrypt_dir
}

ValidateParams()
{
	MAJVER=`echo $version | cut -d. -f1`
	MINVER=`echo $version | cut -d. -f2`
	SUBVER=`echo $version | cut -d. -f3`

	if [ "$MAJVER" == "" ] || [ "$MINVER" == "" ] || [ "$SUBVER" == "" ]
        then
		echo "Version Details are not correct"
        	Usage 
        fi

	if [ "$platform" != "Linux" ] && [ "$platform" != "Darwin" ] && [ "$platform" != "HP-UX" ] && [ "$platform" != "AIX" ] && [ "$platform" != "SunOS" ] && [ "$platform" != "FreeBSD" ] 
	then
    		echo "Error: " $platform "Platform is not supported"
    		exit 3
	fi

	if [ "$config_type" != "release" ] && [ "$config_type" != "debug" ] && [ "$config_type" != "all" ]
	then
    		Usage
	fi

	if [ "$arch_bit" != "32" ] && [ "$arch_bit" != "64" ] && [ "$arch_bit" != "all" ]
	then
    		Usage
	fi

	if [ "$crypto" != "rsa" ] && [ "$crypto" != "openssl" ]
	then
    		Usage
	fi
}

# Usage 
Usage()
{
    echo "Usage: bash <Absolute path>/build/packager/unix/ma_packager.sh -v <version> -n <build number> -c <config type> -p <platform> -b <arch-bit> -C <crypto type> -o <output directory> -d <signing-dir> -m <mcs-script-dir>"
    echo "   -v version i.e. 5.0.0 (Required)"
    echo "   -n build number i.e. 143 (Required)"
    echo "   -c configuration type i.e release/debug/all default is release (optional)"
    echo "   -p unix platform type, output of 'uname -s' i.e. Linux/SunOS/HP-UX/AIX/Darwin/FreeBSD (Required)"
    echo "   -b bit type i.e. 32/64/all default is 32 (optional)"
    echo "   -C crypto type i.e. rsa/openssl default is rsa (optional)"
    echo "   -d apple certificate signing directory only for mac e.g. /Users/xyz/sign"
    echo "   -s date/time stamp - YYYYMMDDHHMMSS"
    echo "   -o output directory i.e. <Absolute path> e.g. /abc/xyz"
    echo "   -m encrypted mcs script dir <Absolute path> and skip this option for local builds"
    exit 1
}

# Enable debugging option if SH_DEBUG=true
if [ "x$SH_DEBUG" == "xtrue" ]
then
	set -xv
fi

scriptFullPath=$0
firstchar=`echo $scriptFullPath | grep "^\/"`

if [ "$firstchar" == "" ]; then
    echo "The build script path is not an absolute path"
    Usage
fi

CURRENT_DIR=`echo $scriptFullPath | grep ".ma_packager.sh" | sed -e 's/.ma_packager.sh//g'`

echo "CURRENT_DIR =" $CURRENT_DIR

if [ "x$MA_BASE" == "x" ]
then
MA_BASE=`echo $CURRENT_DIR | sed -e 's/.unix//g' | sed -e 's/.packager//g' | sed -e 's/.build//g'`
fi

echo "MA_BASE =" $MA_BASE

if [ "x$MA_TOOLS" == "x" ]
then
MA_TOOLS=$MA_BASE/../ma_tools
fi

echo "MA_TOOLS =" $MA_TOOLS

if [ "x$BOOST_BASE" == "x" ]
then
BOOST_BASE=$MA_BASE/../boost
fi

echo "BOOST_BASE =" $BOOST_BASE

# Parsing build script arguments 
while getopts v:n:c:p:b:C:d:o:s:m: opt
  do
  case $opt in
      v)
	  	version=$OPTARG;;
      n)
	  	package_num=$OPTARG;;
      c)
	  	config_type=$OPTARG;;
      p)
        	platform=$OPTARG;;
      b)  
	  	arch_bit=$OPTARG;;
      C)  
	  	crypto=$OPTARG;;
      d)
		sign_dir=$OPTARG;;
      s) 
        	date_and_time=$OPTARG;;
      o)  
	  	output_dir=$OPTARG;;
      m)  
	  	encrypt_dir=$OPTARG;;
      h)
	  	Usage;;
      \?) 
        Usage;;
  esac
done
shift `expr $OPTIND - 1` 

# Assigning default values for optional parameters if not specified
[ -z $version ] && Usage
[ -z $package_num ] && Usage 
[ -z $config_type ] && config_type=release
[ -z $platform ] && Usage 
[ -z $arch_bit ] && arch_bit=32
[ -z $crypto ] && crypto=rsa
[ -z $output_dir ] && Usage
[ -z $date_and_time ] && date_and_time=00000000000000 
[ -z $sign_dir ] && sign_dir=""
[ -z $encrypt_dir ] && encrypt_dir=""

# Printing all parameter values before validation
printParams

# Validating the path and parameters specified
ValidateParams

export MA_BASE 
export BOOST_BASE 
export MA_TOOLS 
export MA_VERSION=$version
export PACKAGE_NUM=$package_num 
export PLATFORM=$platform
export OUTPUT_DIR=$output_dir 
export DATE_AND_TIME=$date_and_time
export SIGN_DIR=$sign_dir
export ENCRYPT_DIR=$encrypt_dir
export CRYPTO=$crypto


CURR_DIR=`echo $0 | grep ".ma_packager.sh" | sed -e 's/.ma_packager.sh//g'`
echo "CURR_DIR =" $CURR_DIR

if [ "$config_type" == "all" ]
then
configuration="release debug"
else
configuration=$config_type
fi

if [ "$arch_bit" == "all" ]
then
bit_type="32 64"
else
bit_type=$arch_bit
fi

echo "Creating Packaging for $platform Platform"
for bit in $bit_type
do
for config in $configuration
do
echo "********** " $platform $bit bit $config "Packaging Start **********"
if [ "$platform" == "Linux" ] || [ "$platform" == "AIX" ]
then
	export ARCH_BIT=$bit
	export CONFIG_TYPE=$config 
	export LIB_EXTENSION=so
	bash $CURRENT_DIR/rpm/rpm_packager.sh
	if [ $? -ne 0 ]; then
		echo "********** " $platform $bit bit $config "Packaging Failed.... "
		exit 1
	else
		echo "********** " $platform $bit bit $config "Packaging End **********"
	fi
	if [ "$bit" == "32" ]; then
		cd $CURR_DIR
		create_sdk ../../../src/compat/mue linux $version $package_num $OUTPUT_DIR/$ARCH_BIT/$CONFIG_TYPE  $config $BOOST_BASE
		if [ $? -ne 0 ]; then
			echo "********** " $platform $bit bit $config "sdk Packaging Failed.... "
			exit 1
		else
			echo "********** " $platform $bit bit $config "sdk Packaging End **********"
		fi
	fi
	
elif [ "$platform" == "Darwin" ]
then
	export ARCH_BIT=$bit
	export CONFIG_TYPE=$config 
	export LIB_EXTENSION=dylib
	bash $CURRENT_DIR/dmg/dmg_packager.sh
	if [ $? -ne 0 ]; then
		echo "********** " $platform $bit bit $config "Packaging Failed.... "
		exit 1
	else
		echo "********** " $platform $bit bit $config "Packaging End **********"
	fi
	cd $CURR_DIR
	#create_sdk ../../../src/compat/mue mac $version $package_num $OUTPUT_DIR/$CONFIG_TYPE  $config $BOOST_BASE
	if [ $? -ne 0 ]; then
		echo "********** " $platform $bit bit $config "sdk Packaging Failed.... "
		exit 1
	else
		echo "********** " $platform $bit bit $config "sdk Packaging End **********"
	fi		
elif [ "$platform" == "SunOS" ]
then
	export ARCH_BIT=$bit
	export CONFIG_TYPE=$config 
	export LIB_EXTENSION=so
	bash $CURRENT_DIR/solaris/solaris_packager.sh
	if [ $? -ne 0 ]; then
		echo "********** " $platform $bit bit $config "Packaging Failed.... "
		exit 1
	else
		echo "********** " $platform $bit bit $config "Packaging End **********"
	fi
	cd $CURR_DIR
	#create_sdk ../../../src/compat/mue mac $version $package_num $OUTPUT_DIR/$CONFIG_TYPE  $config $BOOST_BASE
	if [ $? -ne 0 ]; then
		echo "********** " $platform $bit bit $config "sdk Packaging Failed.... "
		exit 1
	else
		echo "********** " $platform $bit bit $config "sdk Packaging End **********"
	fi		
elif [ "$platform" == "HP-UX" ]
then
	export ARCH_BIT=$bit
	export CONFIG_TYPE=$config 
	export LIB_EXTENSION=sl
	bash $CURRENT_DIR/hpux/hpux_packager.sh
	if [ $? -ne 0 ]; then
		echo "********** " $platform $bit bit $config "Packaging Failed.... "
		exit 1
	else
		echo "********** " $platform $bit bit $config "Packaging End **********"
	fi
	cd $CURR_DIR
	#create_sdk ../../../src/compat/mue mac $version $package_num $OUTPUT_DIR/$CONFIG_TYPE  $config $BOOST_BASE
	if [ $? -ne 0 ]; then
		echo "********** " $platform $bit bit $config "sdk Packaging Failed.... "
		exit 1
	else
		echo "********** " $platform $bit bit $config "sdk Packaging End **********"
	fi		
fi
done
done
exit 0
