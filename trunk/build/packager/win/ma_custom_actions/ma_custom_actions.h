#include "ma/ma_common.h"

#include <Windows.h>

#include <tchar.h>
#include <Msi.h>
#include <MsiDefs.h>
#include <Msiquery.h>

MA_CPP(extern "C" {)

extern HMODULE dll_module;

#define MA_MSI_ACTION  UINT __stdcall

typedef struct msi_vscore_context_s {
    MSIHANDLE hInstaller;
    TCHAR extraction_folder[MAX_PATH];

    TCHAR log_folder[MAX_PATH];
    TCHAR timestamp[MAX_PATH];

} msi_vscore_context_t;

void init_vscore_context(msi_vscore_context_t *context, MSIHANDLE hInstaller);

void log_msg(msi_vscore_context_t *ctx, LPCSTR format, ... );


MA_MSI_ACTION release_from_ap(MSIHANDLE hInstaller);

MA_MSI_ACTION revert_release_from_ap(MSIHANDLE hInstaller);

MA_CPP(}) 
