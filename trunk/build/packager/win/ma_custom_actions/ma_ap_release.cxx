/** 
 * This file is c++ because vscore's aacapi.h is c++
 */

#include "ma_custom_actions.h"

#include "ma/internal/utils/aac_defs/ma_aac_defs.h"

#include <windows.h>
#include "aacapi.h"

#include <cstdlib>
#include <vector>

#include <memory>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>

#include <atlbase.h>

#define AGENT_REG_KEY "SOFTWARE\\mileaccess\\Agent"
#define DISABLED_RULES_REG_VALUE "DisabledAccessProtectionRules"

template <class T>
struct aac_deleter {
    void operator()(T *p) const {
        if (p) p->Release();
    }
};

typedef std::unique_ptr<AacControl, aac_deleter<AacControl> > AacControlPtr;

std::ostream &operator<<(std::ostream &os, const UUID &uuid) {
    RPC_CSTR text;
    if (RPC_S_OK == UuidToStringA(&uuid,&text)) {
        os << text;
        RpcStringFreeA(&text);
    } else {
        os << "???";
    }
    return os;
}

std::istream &operator>>(std::istream &is, UUID &uuid) {
    std::string text;
    if (is >> text) {
        if (0 == UuidFromStringA((RPC_CSTR)text.c_str(), &uuid)) {
        } else {
            is.setstate(std::ios_base::badbit);
        }
    }
    return is;
}



std::ostream &operator<<(std::ostream &os, const std::pair<GUID, GUID> &pair) {
    os << "{" << pair.first << ", " << pair.second << "}";
    return os;
}

/*std::istream &operator>>(std::istream &is, std::pair<GUID, GUID> &pair) {
    is >> "{" >> pair.first >> ", " >> pair.second >> "}";
    return is;
}*/


typedef std::pair<GUID, GUID> POLICY_RULE_GUID;

static std::vector<POLICY_RULE_GUID> affected_policy_rules;




/* Invoked at the beginning of install/uninstall to allow msi to bypass access protection rules */
extern "C" MA_MSI_ACTION release_from_ap(MSIHANDLE hInstaller) {
    msi_vscore_context_t ctx;
    init_vscore_context(&ctx,hInstaller);
    log_msg(&ctx, "Hello from release_from_ap");

    try {
    
        /*
        std::vector<TCHAR> valueBuf;
        DWORD cchValueBuf = 0;
        if (hInstaller && ERROR_MORE_DATA == MsiGetProperty(hInstaller, _T("CustomActionData"), _T(""), &cchValueBuf)) {

            valueBuf.reserve(1+cchValueBuf);

            if (ERROR_SUCCESS !=  MsiGetProperty(hInstaller,_T("CustomActionData"), &valueBuf[0], &cchValueBuf)) {
                log_msg(&ctx, "Steve, you didn't set CustomActionData, will forgive you for now...");
                //return ERROR_SUCCESS; 
            }
        }
        */
        int rc;
        AacControl *aac_control = nullptr;
        if (ERROR_SUCCESS == (rc = AacControlCreate(&aac_control, MA_AAC_PRODUCT_GUID))) {
            AacControlPtr managed(aac_control);
            AacActivePolicyList *policy_list = nullptr;
            if (ERROR_SUCCESS == aac_control->GetActivePolicyList(&policy_list, &MA_AAC_PRODUCT_GUID)) {
                std::size_t count = policy_list->GetPolicyCount();
                LPCGUID policy_guids = policy_list->GetPolicyList();
                for (std::size_t i=0; i!= count; ++i) {

                    const GUID &policy_guid = policy_guids[i];
                    if (MA_SYSCORE_POLICY_GUID == policy_guid) continue; // ignore syscore and aac policy
                    if (MA_SYSCORE_AAC_POLICY_GUID == policy_guid) continue;

                    AacPolicy *policy = nullptr;
                    if (ERROR_SUCCESS == policy_list->GetPolicy(&policy, policy_guid)) {
                        AacRule *rule = nullptr;
                        while (rule = policy->GetNextRule(rule)) {
                            if (rule->IsEnabled()) {
                                const GUID &rule_guid = rule->GetUid();
                                LPCWSTR description = rule->GetDescription();

                                log_msg(&ctx, "Now disabling rule %ls ...", description);
                                rc = aac_control->EnableDisableRule(policy_guid, rule_guid, false);
                                if (rc) {
                                    log_msg(&ctx, "Error disabling rule %ls, rc = %d", description, rc);
                                } else {
                                    log_msg(&ctx, "Ok disabling rule %ls", description);
                                }
                                affected_policy_rules.push_back(std::make_pair(policy_guid, rule_guid));
                            }
                        }
                        policy->Release();
                    }
                }
                policy_list->Release();
            } 

            if (!affected_policy_rules.empty()) {
                std::ostringstream oss;
                const char *delim = "";
                std::for_each(affected_policy_rules.begin(), affected_policy_rules.end(), 
                    [&oss, &delim] (POLICY_RULE_GUID const &prg) { oss << delim << prg; delim = ", ";} 
                );

                CRegKey agentKey;
                if (ERROR_SUCCESS == agentKey.Open(HKEY_LOCAL_MACHINE, _T(AGENT_REG_KEY), KEY_WOW64_32KEY|KEY_WRITE)) {
                    agentKey.SetStringValue(DISABLED_RULES_REG_VALUE, CA2CT(oss.str().c_str()));
                }

                /*
                // for testing, enable again 
                for (LPCGUID *p = MA_AAC_ALL_RULES; *p ; ++p){
                    aac_control->EnableDisableRule(MA_AAC_DEFAULT_POLICY_GUID,**p, true);
                }*/

            }
        } else {
            log_msg(&ctx, "Error in AacControlCreate, cannot disable any Self Protection, rc=%d", rc);
        }
    } catch (...) {
        log_msg(&ctx, "Unknown exception in  release_from_ap CA");
    }
    return ERROR_SUCCESS;
}

extern "C" MA_MSI_ACTION revert_release_from_ap(MSIHANDLE hInstaller) {

    return ERROR_SUCCESS; /* Doesn't do anything, relies on aac_service to reconfigure aac from scratch */

    CRegKey agentKey;
    if (ERROR_SUCCESS == agentKey.Open(HKEY_LOCAL_MACHINE,_T("SOFTWARE\\mileaccess\\Agent"),KEY_WOW64_64KEY|KEY_QUERY_VALUE)) {


        //aac_control->EnableDisableRule(policy_guid, rule_guid, false);

        //agentKey.QueryStringValue()
        //agentKey.SetStringValue(_T("DisabledAccessProtectionRules"),CA2CT(oss.str().c_str()));
    }


    AacControl *aac_control = nullptr;
    if (ERROR_SUCCESS == AacControlCreate(&aac_control, MA_AAC_PRODUCT_GUID)) {
        for (LPCGUID *p = MA_AAC_ALL_RULES; *p ;++p){
            aac_control->EnableDisableRule(MA_AAC_SELF_PROTECTION_POLICY_GUID, **p, true);
        }
        aac_control->Release();
    }
    return ERROR_SUCCESS;
}
