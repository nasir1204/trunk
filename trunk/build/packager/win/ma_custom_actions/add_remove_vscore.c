#include "ma_custom_actions.h"
#include "resource.h"


#include <Fdi.h>
#include <io.h>
#include <fcntl.h>
#include <SYS\Stat.h>

#include <Shlwapi.h>
#include <stdio.h>



void init_vscore_context(msi_vscore_context_t *context, MSIHANDLE hInstaller) {
    char temp_path[MAX_PATH];
    SYSTEMTIME st;

    context->hInstaller = hInstaller;
    memset(context->extraction_folder, 0, sizeof(context->extraction_folder));

    if (GetTempPathA(_countof(temp_path), temp_path)) {
        _sntprintf(context->log_folder, _countof(context->log_folder), "%smileaccessLogs\\", temp_path);
    } else {
        /**/
        *context->log_folder = 0;
    }

    GetLocalTime(&st);
    _sntprintf(context->timestamp, _countof(context->timestamp), "%04u%02u%02uT%02u%02u%02u", (unsigned)st.wYear, (unsigned)st.wMonth, (unsigned)st.wDay, (unsigned)st.wHour, (unsigned)st.wMinute, (unsigned)st.wSecond );

};


void log_msg(msi_vscore_context_t *ctx, LPCSTR format, ... ) {
    TCHAR buffer[1024];  
    va_list args;
    va_start(args, format);
    _vsntprintf(buffer, _countof(buffer), format, args);
    buffer[_countof(buffer)-1] = 0;
    va_end(args);
    if (ctx->hInstaller) {
        MSIHANDLE hRec = MsiCreateRecord(1);
        if (hRec) {
            MsiRecordSetString(hRec, 0, buffer);
            MsiProcessMessage(ctx->hInstaller, INSTALLMESSAGE_INFO, hRec);
            MsiCloseHandle(hRec);
        }
    } else {
    }
    OutputDebugString(buffer);
}


static int extract_cab(msi_vscore_context_t *context);
static int invoke_mfehidin(msi_vscore_context_t *context, LPCTSTR args);
static int remove_files(msi_vscore_context_t *context);


static BOOL IsAMD64()
{
#ifdef _WIN64
    return TRUE;
#else
    typedef BOOL (WINAPI *LPFN_ISWOW64PROCESS) (HANDLE, PBOOL);

    LPFN_ISWOW64PROCESS fnIsWow64Process = (LPFN_ISWOW64PROCESS) GetProcAddress(
        GetModuleHandle(TEXT("kernel32")),"IsWow64Process");

    if(fnIsWow64Process)
    {
        BOOL bIsWow64 = FALSE;
        if (fnIsWow64Process(GetCurrentProcess(),&bIsWow64))
        {
            return bIsWow64;
        }
    }
    return FALSE;
#endif
}

MA_MSI_ACTION add_vscore(MSIHANDLE hInstaller) {
    char args[64 + 2*MAX_PATH];
    int rc;
    msi_vscore_context_t context;
    init_vscore_context(&context, hInstaller);

    log_msg(&context, "MA.add_vscore...");

    _sntprintf(args, _countof(args), "-i ma50 -x ma50.xml -l \"%sma_vscore_install_%s.log\" -etl \"%sma_vscore_install_%s.etl\"", context.log_folder, context.timestamp, context.log_folder, context.timestamp);

    extract_cab(&context);
    rc = invoke_mfehidin(&context, args);
    remove_files(&context);

    log_msg(&context, "mfehidin returned %d", rc);

    return (rc < 0 || CHAR_MAX < rc) 
        ?  ERROR_INSTALL_FAILURE 
        :  ERROR_SUCCESS;
}

MA_MSI_ACTION remove_vscore(MSIHANDLE hInstaller) {
    msi_vscore_context_t context;
    char args[64 + 2*MAX_PATH];
    init_vscore_context(&context, hInstaller);

    _sntprintf(args, _countof(args), "-u ma50 -x ma50.xml -l \"%sma_vscore_uninstall_%s.log\" -etl \"%sma_vscore_uninstall_%s.etl\"", context.log_folder, context.timestamp, context.log_folder, context.timestamp);

    extract_cab(&context);
    invoke_mfehidin(&context, args);
    remove_files(&context);

    return ERROR_SUCCESS;
}




typedef struct buffer_s {
    void const *data;
    DWORD size;
} buffer_t;

static FNFDINOTIFY(fdi_notify) {
    switch (fdint) {


    case fdintCABINET_INFO:   
    case fdintPARTIAL_FILE:  
        return 0;
    case fdintCOPY_FILE: {
        LPCTSTR folder = pfdin->pv;
        CHAR dest_path[MAX_PATH], dest_folder[MAX_PATH];
        
        strncpy(dest_path,folder,_countof(dest_path));
        PathAppend(dest_path, pfdin->psz1);

        /* Must create sub folder explicitly, for now support just one level */

        strncpy(dest_folder,dest_path,_countof(dest_folder));
        *PathFindFileName(dest_folder) = 0;

        CreateDirectory(dest_folder,NULL);

        return _open(dest_path,_O_WRONLY | _O_CREAT | _O_BINARY, _S_IWRITE); /* _S_IWRITE to avoid read-only attribute to be set! */

        }
    case fdintCLOSE_FILE_INFO:
        _close(pfdin->hf);
        return 1;

    case fdintNEXT_CABINET:
    case fdintENUMERATE:
        return 0;
    }
    return 0;
}



static void HUGE FAR *DIAMONDAPI malloc_stub(ULONG cb) {
    return malloc(cb);
}

static int do_extract_cab(LPCTSTR filepath, LPCTSTR extract_folder) {
    ERF erf = {0};
    HFDI hFdi = FDICreate(
        &malloc_stub,
        &free,
        (PFNOPEN)&_open,
        &_read,
        &_write,
        &_close,
        &_lseek,
        0,
        &erf);
    int rc = -1;

    if (hFdi) {
        rc = FDICopy(hFdi, PathFindFileName(filepath), extract_folder, 0, &fdi_notify, NULL, extract_folder) ? ERROR_SUCCESS : -1;
        if (rc) {
            erf.fError;
        } 
        FDIDestroy(hFdi);
    }

    return rc;
}

/* copies the content into the specified file */
static int populate_file(msi_vscore_context_t *context, HANDLE hFile) {
    int rc = -1;
    if (0/* DISABLED for now, binary table not accessible in a deferred custom action*/ /* context->hInstaller*/) {
        MSIHANDLE hMsiDatabase = MsiGetActiveDatabase(context->hInstaller);
        log_msg(context,"MA.populate_file 1");
        if (hMsiDatabase) {
            MSIHANDLE hMsiView;
            log_msg(context,"MA.populate_file 2");
            if (ERROR_SUCCESS == MsiDatabaseOpenView(hMsiDatabase,"SELECT 'Data' FROM Binary WHERE Name='ma50.cab'",&hMsiView)) {
                MSIHANDLE hMsiRecord;
                log_msg(context,"MA.populate_file 2.5");
                if (ERROR_SUCCESS == MsiViewExecute(hMsiView,0) && 
                    ERROR_SUCCESS == MsiViewFetch(hMsiView, &hMsiRecord)) {
                        rc = 0;
                        log_msg(context,"MA.populate_file 3");
                        do {
                            char szDataBuf[8192];
                            DWORD cbDataBuf = sizeof(szDataBuf);
                            if (ERROR_SUCCESS == MsiRecordReadStream(hMsiRecord, 1, szDataBuf, &cbDataBuf) && cbDataBuf) {
                                DWORD bytes_written;
                                log_msg(context,"MA.populate_file 4");
                                if (WriteFile(hFile,szDataBuf,cbDataBuf,&bytes_written,NULL) && cbDataBuf == bytes_written) continue;
                                rc = -1;
                            } else {
                                rc = -2; /* */
                            }
                        } while (0);
                        MsiCloseHandle(hMsiRecord);
                } else {
                    /* some error */
                }
                MsiCloseHandle(hMsiView);
            }
            MsiCloseHandle(hMsiDatabase);
        }
    } else {
        HRSRC hResInfo;
        HGLOBAL hResource;
        if ((hResInfo = FindResource(dll_module, MAKEINTRESOURCE(IDR_MA50CAB), RT_RCDATA)) && (hResource = LoadResource(dll_module, hResInfo))) {
            buffer_t b = {LockResource(hResource), SizeofResource(dll_module, hResInfo)};
            DWORD bytes_written;
            if (WriteFile(hFile,b.data,b.size,&bytes_written,NULL) && bytes_written == b.size) {
                rc = 0;
            }
        }
    }
    return rc;
}


static int extract_cab(msi_vscore_context_t *context) {
    int rc = -1;
    TCHAR temp_path[MAX_PATH], temp_subfolder[MAX_PATH], temp_pathname[MAX_PATH];
    HANDLE hFile = INVALID_HANDLE_VALUE;

    GetTempPath(_countof(temp_path), temp_path);

    GetTempFileName(temp_path,_T("ma"),0,temp_subfolder);
    /* GetTempFileName actually creates a file, go delete it so we can create a directory instead with the same name */
    DeleteFile(temp_subfolder);
    CreateDirectory(temp_subfolder, NULL);

    memset(context->extraction_folder, 0, sizeof(context->extraction_folder));
    strncpy(context->extraction_folder, temp_subfolder, _countof(context->extraction_folder));
    
    strncpy(temp_pathname, temp_subfolder, _countof(temp_pathname) );
    PathAppend(temp_pathname,"ma50.cab");

    if (INVALID_HANDLE_VALUE != (hFile = CreateFile(temp_pathname, GENERIC_WRITE, FILE_SHARE_READ|FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, /*FILE_FLAG_DELETE_ON_CLOSE*/FILE_ATTRIBUTE_NORMAL, NULL))) {
        rc = populate_file(context, hFile);
        CloseHandle(hFile);
        hFile = INVALID_HANDLE_VALUE;
    }
    if (0==rc) {
        strcat(temp_subfolder,"\\");
        rc = do_extract_cab(temp_pathname,temp_subfolder);
    }

    return rc;
}



static int invoke_mfehidin(msi_vscore_context_t *context, LPCTSTR args) {
    if (*context->extraction_folder) {
        STARTUPINFO startup_info = {0};
        PROCESS_INFORMATION pi = {0};
        TCHAR cmdline[4*MAX_PATH];
        /* Launch mfehidin from x86 or x64 subdir depending on platform */
        _sntprintf(cmdline, _countof(cmdline), _T("\"%s\\%s\\mfehidin.exe\" %s"), context->extraction_folder, IsAMD64() ? _T("x64") : _T("x86"), args);
        cmdline[_countof(cmdline)-1] = 0;

        if (CreateProcess(NULL, cmdline, NULL, NULL, FALSE, CREATE_SUSPENDED | CREATE_NO_WINDOW, NULL, NULL, &startup_info, &pi)) {
            DWORD exit_code;
            const DWORD timeout_minutes = 15;
            /* we can do some signature validation here... */
            ResumeThread(pi.hThread);

            if (WAIT_TIMEOUT == WaitForSingleObject(pi.hProcess, timeout_minutes * 60 * 1000)) {
                /* some error or  timeout */
                log_msg(context, "Timed out after waiting %u mins for mfehidin to complete, killing it...", timeout_minutes);
                TerminateProcess(pi.hProcess, WAIT_TIMEOUT);
            }
            GetExitCodeProcess(pi.hProcess,&exit_code);
            
            CloseHandle(pi.hProcess);
            CloseHandle(pi.hThread);
            
            return exit_code;
        }
        
    }
    return -1;
}

static int remove_files(msi_vscore_context_t *context) {
    SHFILEOPSTRUCT fileop = {0};
    if (*context->extraction_folder) {
        int rc;
        fileop.wFunc = FO_DELETE;
        fileop.pFrom = context->extraction_folder;
        fileop.fFlags = FOF_SILENT | FOF_NOCONFIRMATION | FOF_NOERRORUI | FOF_NOCONFIRMMKDIR;
        rc = SHFileOperation(&fileop);
        if (rc || fileop.fAnyOperationsAborted) {
            log_msg(context, "SHFileOperation(FO_DELETE, \"%s\",) returned <%d>, AnyOperationsAborted: <%d>", (const char*) fileop.pFrom, rc, (int) fileop.fAnyOperationsAborted);
        }
    }
    return ERROR_SUCCESS;
}


/* RunDll32 entry point for direct invocation */
int CALLBACK rundll_invoke(HWND hwnd, HINSTANCE hinst, LPCSTR lpszCmdLine, int nCmdShow) {
    if (0 == lstrcmp(lpszCmdLine,"add_vscore"))
        return add_vscore(0);

    if (0 == lstrcmp(lpszCmdLine,"remove_vscore"))
        return remove_vscore(0);

    if (0 == lstrcmp(lpszCmdLine,"release_from_ap"))
        return release_from_ap(0);

    if (0 == lstrcmp(lpszCmdLine,"revert_release_from_ap"))
        return revert_release_from_ap(0);

    return 1;
}
