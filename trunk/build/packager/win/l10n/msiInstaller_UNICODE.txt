IDS_LOCALIZE_ERROR_INVALID_SITELIST	The specified sitelist is invalid. [ProductName] Setup will not continue.	
IDS_LOCALIZE_ERROR_INVALIDDIR_ROOT	[ProductName] cannot be installed to the the root of a drive.	
IDS_LOCALIZE_ERROR_INVALIDDIR_SYSTEM	[ProductName] cannot be installed to the Windows system folder.	
IDS_LOCALIZE_ERROR_INVALIDDIR_TEMP	[ProductName] cannot be installed to a temporary folder.	
IDS_LOCALIZE_ERROR_INVALIDDIR_UNC	[ProductName] cannot be installed to a UNC folder.	
IDS_LOCALIZE_ERROR_INVALIDDIR_WINDOWS	[ProductName] cannot be installed to the Windows folder.	
IDS_LOCALIZE_ERROR_NEWERVERSION	A newer version of this product is already installed.   Setup cannot proceed.	
IDS_LOCALIZE_ERROR_POINTPRODUCTMANAGEDMODE	The existing Agent is in managed mode and will not be upgraded.	
IDS_LOCALIZE_ERROR_REBOOTNEEDED	This computer must be rebooted before [ProductName] can be installed.	
IDS_LOCALIZE_ERROR_SAMEVERSION	[ProductName] [ProductVersion] is already installed and /Repair option was not specified.  [ProductName] install will now quit.	
IDS_LOCALIZE_ERROR_UNINSTALLNOTALLOWED	[ProductName] cannot be removed because other products are still using it.	
IDS_LOCALIZE_ERROR_UNINSTALLNOTALLOWEDMANAGEMODE	[ProductName] cannot be removed while it is in managed mode.	
IDS_LOCALIZE_LAUNCH_CONDITION_OS	[ProductName] can not be installed on this version of Windows.  Please see the product documentation.	
