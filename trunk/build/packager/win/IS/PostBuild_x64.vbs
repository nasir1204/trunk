' PostBuild_x64.vbs - This is run as a post build step to modify the x64 .msi
'  USAGE EXAMPLE: wscript PostBuild_x64.vbs /msifile:C:\projects\MA\trunk\build\packager\win\IS\x64\SINGLE_MSI_IMAGE\DiskImages\DISK1\MFEagent.msi

' An object that allows us to access the command line arguments.
set scriptarguments = WScript.Arguments.Named

Dim MsiFile, MsiFolder, MstName, MstFile, AgentVer

' The full path name to the recently built .MSI file in our current build.
MsiFile=scriptarguments.Item("msifile")

' get the folder the .msi is in
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objFile = objFSO.GetFile(MsiFile)
MsiFolder=objFSO.GetParentFolderName(objFile)

Dim msi
Set msi = Nothing
Set msi = Wscript.CreateObject("WindowsInstaller.Installer")

Dim db
set db=msi.OpenDatabase(MsiFile, 1)
Dim view
Dim record

' use ProgramFiles64Folder as our default
set view=db.OpenView("UPDATE Directory SET Directory.Directory_Parent='ProgramFiles64Folder' WHERE Directory.Directory='MCAFEE'")
view.Execute
db.Commit

set view=nothing
set db=nothing