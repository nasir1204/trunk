' PostBuild_EmbedTransforms.vbs - This is run as a post build step to embed the language transforms in the .msi
'  USAGE EXAMPLE: wscript PostBuild_EmbedTransforms.vbs /msifile:C:\projects\MA\trunk\build\packager\win\IS\x64\SINGLE_MSI_IMAGE\DiskImages\DISK1\MFEagent.msi

' An object that allows us to access the command line arguments.
set scriptarguments = WScript.Arguments.Named

Dim MsiFile, MsiFolder, MstName, MstFile, AgentVer

' The full path name to the recently built .MSI file in our current build.
MsiFile=scriptarguments.Item("msifile")

' get the folder the .msi is in
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objFile = objFSO.GetFile(MsiFile)
MsiFolder=objFSO.GetParentFolderName(objFile)

Dim msi
Set msi = Nothing
Set msi = Wscript.CreateObject("WindowsInstaller.Installer")

Dim db
set db=msi.OpenDatabase(MsiFile, 1)
Dim view
Dim record

' embed all of our language transforms in the _Storage table
MstName="1028.mst"
EmbedTransform MsiFolder, MstName

MstName="1029.mst"
EmbedTransform MsiFolder, MstName

MstName="1030.mst"
EmbedTransform MsiFolder, MstName

MstName="1031.mst"
EmbedTransform MsiFolder, MstName

MstName="1033.mst"
EmbedTransform MsiFolder, MstName

MstName="1034.mst"
EmbedTransform MsiFolder, MstName

MstName="1035.mst"
EmbedTransform MsiFolder, MstName

MstName="1036.mst"
EmbedTransform MsiFolder, MstName

MstName="1040.mst"
EmbedTransform MsiFolder, MstName

MstName="1041.mst"
EmbedTransform MsiFolder, MstName

MstName="1042.mst"
EmbedTransform MsiFolder, MstName

MstName="1043.mst"
EmbedTransform MsiFolder, MstName

MstName="1044.mst"
EmbedTransform MsiFolder, MstName

MstName="1045.mst"
EmbedTransform MsiFolder, MstName

MstName="1046.mst"
EmbedTransform MsiFolder, MstName

MstName="1049.mst"
EmbedTransform MsiFolder, MstName

MstName="1053.mst"
EmbedTransform MsiFolder, MstName

MstName="1055.mst"
EmbedTransform MsiFolder, MstName

MstName="2052.mst"
EmbedTransform MsiFolder, MstName

MstName="2070.mst"
EmbedTransform MsiFolder, MstName

set view=nothing
set db=nothing

sub EmbedTransform(MsiFolder, MstName)
   ' embed the specified transform in the _Storage table
   set view=db.OpenView("SELECT `Name`,`Data` FROM _Storages")
   Set record = msi.CreateRecord(2)
   MstFile=objFSO.BuildPath(MsiFolder, MstName)
   record.StringData(1) = MstName
   view.Execute record
   record.SetStream 2, MstFile
   view.Modify 3, record
   db.Commit
end sub