' PostBuild_x64.vbs - This is run as a post build step to modify the x86 .msi
'  USAGE EXAMPLE: wscript PostBuild_x86.vbs /msifile:C:\projects\MA\trunk\build\packager\win\IS\x86\SINGLE_MSI_IMAGE\DiskImages\DISK1\MFEagent.msi

' An object that allows us to access the command line arguments.
set scriptarguments = WScript.Arguments.Named

Dim MsiFile, MsiFolder, MstName, MstFile, AgentVer

' The full path name to the recently built .MSI file in our current build.
MsiFile=scriptarguments.Item("msifile")

' get the folder the .msi is in
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objFile = objFSO.GetFile(MsiFile)
MsiFolder=objFSO.GetParentFolderName(objFile)

Dim msi
Set msi = Nothing
Set msi = Wscript.CreateObject("WindowsInstaller.Installer")

Dim db
set db=msi.OpenDatabase(MsiFile, 1)
Dim view
Dim record

' make Svc_x86 a required feature and set its Install Level to 1
set view=db.OpenView("UPDATE Feature SET Feature.Attributes='24' WHERE Feature.Feature='Svc_x86'")
view.Execute
db.Commit

set view=db.OpenView("UPDATE Feature SET Feature.Level='1' WHERE Feature.Feature='Svc_x86'")
view.Execute
db.Commit

set view=nothing
set db=nothing