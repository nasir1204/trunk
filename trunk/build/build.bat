@echo off
cls
echo executing %0 %*
set MA_BASE=%~dp0..

if /i "%1"=="help" goto help
if /i "%1"=="--help" goto help
if /i "%1"=="-help" goto help
if /i "%1"=="/help" goto help
if /i "%1"=="?" goto help
if /i "%1"=="-?" goto help
if /i "%1"=="--?" goto help
if /i "%1"=="/?" goto help

if exist %MA_BASE%\build\msvc\ma_tools_outdir (
	@rem delete all files 
	del /q /s %MA_BASE%\build\msvc\ma_tools_outdir
	@rem all the folders
	rmdir /q /s %MA_BASE%\build\msvc\ma_tools_outdir
)


@rem Process arguments.
set config=Release
set target=Rebuild
set bit=32
set arch=intel
set vs_toolset=x86
set platform=Win32
set library=static_library
set builgLogFile=.\build.log
set allconfig=
set allbit=1

:next-arg
set str1=%1
if "%1"=="" 						goto args-done
if /i "%1"=="debug"        			set config=Debug & goto arg-ok
if /i "%1"=="release"      			set config=Release & goto arg-ok
if /i "%1"=="allconfig"      		set allconfig=1 & goto arg-ok

if /i "%1"=="clean"        			set target=Clean & goto arg-ok
if /i "%1"=="build"        			set target=Build & goto arg-ok
if /i "%1"=="rebuild"        		set target=Rebuild & goto arg-ok

if /i "%1"=="32"          			set bit=32 & goto arg-ok
if /i "%1"=="64"         			set bit=64 & goto arg-ok
if /i "%1"=="all"         			set allbit=1 & goto arg-ok

if /i "%1"=="intel"          		set arch=intel & goto arg-ok
if /i "%1"=="itanium"          		set arch=itanium & goto arg-ok
if /i "%1"=="amd"          			set arch=amd & goto arg-ok

if /i "%1"=="test"          		set test=1 & goto arg-ok

if /i "%1"=="msi"          			set msi=1 & goto arg-ok

if /i "%1"=="msgbus_auth"          	set msgbus_auth=1 & goto arg-ok

if /i "%1"=="beacon"          		set beacon=1 & goto arg-ok

if /i "%1"=="pseudo"          		set l10npseudo=pseudo & goto arg-ok

if /i "%1"=="nolws"          		set nolwsoption=1 & goto arg-ok

if not x%str1::\=%==x%str1%  (
	if not x%str1:.log=%==x%str1% (
		set builgLogFile=%1 & goto arg-ok
	) else (
		set outFolder=%1 & goto arg-ok
	)
)

if /i "%1"=="shared"       			set library=shared_library & goto arg-ok
if /i "%1"=="static"       			set library=static_library & goto arg-ok
:arg-ok
shift
goto next-arg
:args-done

if "%MA_TOOLS%"=="" set MA_TOOLS=%MA_BASE%\..

if not defined MA_VERSION_MAJOR set MA_VERSION_MAJOR=5
if not defined MA_VERSION_MINOR set MA_VERSION_MINOR=0
if not defined MA_VERSION_SUBMINOR set MA_VERSION_SUBMINOR=0
if not defined MA_VERSION_BUILDNUMBER set MA_VERSION_BUILDNUMBER=999

set CMD_LINE=MA_VERSION_MAJOR=%MA_VERSION_MAJOR%;MA_VERSION_MINOR=%MA_VERSION_MINOR%;MA_VERSION_SUBMINOR=%MA_VERSION_SUBMINOR%;MA_VERSION_BUILDNUMBER=%MA_VERSION_BUILDNUMBER%
set MA_COMPAT_SDK=%MA_TOOLS%\ma_compat_sdk\
set MASIGN=%MA_TOOLS%\masign\masign.exe
set MASIGN_PREP_OUT=%MA_BASE%\build\packager\data

if not defined MA_VSCORE_SDK set MA_VSCORE_SDK=%MA_BASE%\..\VSCore SDK.15.3.0\sdk\

if not defined MA_BOOST set MA_BOOST=%MA_BASE%\..\windows
if not defined MA_BOOST64 set MA_BOOST64=%MA_BASE%\..\windows_x64

if not defined RSA_MES set RSA_MES=%MA_BASE%\..\RSA_BSAFE_MES-4.0.5

echo MA Tools base=%MA_TOOLS%
echo MA base=%MA_BASE%
echo.
echo config=%config%
echo bit=%bit%
echo arch=%arch%

if /i %bit%==32 (
	if %arch%==intel set vs_toolset=x86 & set platform=Win32
	if %arch%==itanium set vs_toolset=x86 & set platform=Win32
	if %arch%==amd set vs_toolset=x86 & set platform=Win32
)
if /i %bit%==64 (
	if %arch%==intel set vs_toolset=x86_amd64 & set platform=x64
	if %arch%==itanium set vs_toolset=x86_ia64 & set platform=x64
	if %arch%==amd set vs_toolset=x86_amd64 & set platform=x64
)

rem if /i %vs_toolset%==x86 (
rem	set platform=Win32
rem	) else (
rem		set platform=%vs_toolset%
rem	)

set allbits=(%platform%)
if defined allbit set allbits=(Win32, x64)


set allconfigs=(%config%)
if defined allconfig  set allconfigs=(Release, Debug)

echo vs_toolset=%vs_toolset%
echo pltaform=%platform%
echo.
echo outFolder=%outFolder%
echo builgLogFile=%builgLogFile%

if defined beacon goto buildbeacon
if defined msi goto buildmsi
if defined msgbus_auth goto prepmsgbusauth

if "%outFolder%"=="" goto skip-outfolder
if not exist %outFolder% (
	echo creating folder %outFolder%
	mkdir %outFolder%
) else (
	echo %outFolder% already exists. Provide another out folder.
	goto exit
)

:skip-outfolder


@rem Look for Visual Studio 2010
if not defined VS100COMNTOOLS goto vc-set-notfound
if not exist "%VS100COMNTOOLS%\..\..\vc\vcvarsall.bat" goto vc-set-notfound
goto msbuild


:vc-set-notfound
echo *************************************************
echo Error: Visual Studio 2010 not found
echo *************************************************
goto exit

:msbuild
if exist %MA_BASE%\build\packager\data\ma_msgbus_auth.xml (
	del /q /s %MA_BASE%\build\packager\data\ma_msgbus_auth.xml
)

if exist %MA_BASE%\build\packager\data\ma_msgbus_auth.sig (
	del /q /s %MA_BASE%\build\packager\data\ma_msgbus_auth.sig
)

set MSBuildEmitSolution=1
echo *******************************
echo allbits=%allbits%
echo allconfigs=%allconfigs%
echo.
for %%A in %allbits% Do (
	if %%A==Win32 (
		 call "%VS100COMNTOOLS%\..\..\vc\vcvarsall.bat" x86
	) else (
		if %arch%==itanium ( 
			call "%VS100COMNTOOLS%\..\..\vc\vcvarsall.bat" x86_ia64
			) else (
				call "%VS100COMNTOOLS%\..\..\vc\vcvarsall.bat" x86_amd64
			)
	)
	for %%B in %allconfigs% Do (
		msbuild %MA_BASE%\build\msvc\ma.sln /t:%target% /p:Configuration=%%B /p:Platform=%%A >> %builgLogFile%				
		if errorlevel 1 goto exit-error
		msbuild %MA_BASE%\build\msvc\ma.sln /t:%target% /p:Configuration=%%BStatic /p:Platform=%%A >> %builgLogFile%						
		if errorlevel 1 goto exit-error
		msbuild %MA_BASE%\build\msvc\ma.sln /t:%target% /p:Configuration=%%BStaticMT /p:Platform=%%A >> %builgLogFile%						
		if errorlevel 1 goto exit-error				
		if defined test msbuild %MA_BASE%\build\msvc\ma_test.sln /t:%target% /p:Configuration=%%B /p:Platform=%%A >> %builgLogFile%
		if errorlevel 1 goto exit-error
	)
)
msbuild %MA_BASE%\build\msvc\ma_compat.sln /t:ReBuild /p:Configuration=Release /p:Platform=Win32 >> %builgLogFile%				
if errorlevel 1 goto exit-error
msbuild %MA_BASE%\build\msvc\ma_compat.sln /t:ReBuild /p:Configuration=Release /p:Platform=x64 >> %builgLogFile%				
if errorlevel 1 goto exit-error
msbuild %MA_BASE%\build\msvc\ma_installer.sln /t:Rebuild /p:Configuration=ReleaseInstall /p:Platform=Win32 >> %builgLogFile%				
if errorlevel 1 goto exit-error
msbuild %MA_BASE%\build\msvc\ma_installer.sln /t:Rebuild /p:Configuration=Release_UPD /p:Platform=Win32 >> %builgLogFile%				
if errorlevel 1 goto exit-error
msbuild %MA_BASE%\build\msvc\ma_installer.sln /t:Rebuild /p:Configuration=Release-EmbeddedInstall /p:Platform=Win32 >> %builgLogFile%				
if errorlevel 1 goto exit-error
msbuild %MA_BASE%\build\msvc\ma_dev_tools.sln /t:Rebuild /p:Configuration=ReleaseStaticMT /p:Platform=Win32 >> %builgLogFile%				
if errorlevel 1 goto exit-error
msbuild %MA_BASE%\build\msvc\ma_dev_tools.sln /t:Rebuild /p:Configuration=Release /p:Platform=Win32 >> %builgLogFile%				
if errorlevel 1 goto exit-error

xcopy /Y /S %MA_TOOLS%\msgpack %MA_BASE%\build\msvc\msgpack\

echo **************************************************************************
if defined nolwsoption goto exit
echo **************************************************************************
echo LWS Integration
echo **************************************************************************
rem assuming lwsconsole.exe is set in the path
rem set MA_L10N_PROJ=C:\Users\anilch\Documents\work\code\MA50\1srcMA50\LWS
echo ************************************************************************
echo ma building agent resource ini
echo ************************************************************************
msbuild %MA_BASE%\tools\ma_restoini\ma_restoini.sln /t:%target% /p:Configuration=Release /p:Platform=Win32 >> %builgLogFile%			
if errorlevel 1 goto exit-error
%MA_BASE%\tools\ma_restoini\Release\ma_restoini.exe %MA_BASE%\build\msvc\Release\0409\
if "%MA_L10N_PROJ%"=="" set MA_L10N_PROJ=%MA_BASE%\LWS
set L10N_OUT_DIR=%MA_BASE%\build\msvc\l10n_outdir\
if exist %L10N_OUT_DIR% (
	@rem delete all files 
	mkdir %L10N_OUT_DIR%
)
if /i "%l10npseudo%"=="pseudo"  (
	rem echo Creating the real localization build
	rem lwsconsole.exe /O=%MA_L10N_PROJ% /I=%MA_BASE%\build\msvc\release\0409 /L="cs,da,de,es,es-es,fi,fr,it,ja,ko,nb-NO,nl,pl,pt-BR,pt-PT,ru,sv,tr,zh-CN,zh-TW" /V /S
	rem echo Creating pseudo localized build
	rem lwsconsole.exe /O=%MA_L10N_PROJ% /I=%MA_BASE%\build\msvc\release\0409 /L="cs,da,de,es,es-es,fi,fr,it,ja,ko,nb-NO,nl,pl,pt-BR,pt-PT,ru,sv,tr,zh-CN,zh-TW" /D=%L10N_OUT_DIR% /V /S /P
	lwsconsole.exe /O=%MA_L10N_PROJ% /I=%MA_BASE%\build\msvc\release\0409 /L="cs,da,de,es,es-es,fi,fr,it,ja,ko,nb-NO,nl,pl,pt-BR,pt-PT,ru,sv,tr,zh-CN,zh-TW" /D=%L10N_OUT_DIR% /V /S
	) else (
	echo real localized build
	lwsconsole.exe /O=%MA_L10N_PROJ% /I=%MA_BASE%\build\msvc\release\0409 /L="cs,da,de,es,es-es,fi,fr,it,ja,ko,nb-NO,nl,pl,pt-BR,pt-PT,ru,sv,tr,zh-CN,zh-TW" /D=%L10N_OUT_DIR% /V /S
	)

if errorlevel 1 goto exit-error

xcopy /y /s %MA_BASE%\tools\ma_restoini\Release\ma_restoini.exe %MA_BASE%\build\msvc\Release\
%MA_BASE%\build\msvc\Release\ma_restoini.exe %MA_BASE%\build\msvc\Release\0409\

echo **************************************************************************
goto exit


:help
echo fullpath\build.bat [debug/release/allconfig] [test] [32/64/all] [intel/itanium/amd] [static/shared] [outfolderFullPath] [buildLogFilenameFullPath]
echo Examples:
echo   w:\code\build.bat               										: builds release build, build.log would be generated at .\ where the bat file is invoked from. 
echo   w:\code\build.bat test          										: builds release build, build.log would be generated at .\ where the bat file is invoked from and also builds the test solution. 
echo   w:\code\build.bat Release all intel w:\code\test\ w:\build.log		: builds release on all platforms (Win32, x64) and the binaries are copied to "w:\code\test\" and the compile logs are generated in w:\build.log
echo   w:\code\build.bat allconfig Win32 intel w:\code\test\ w:\build.log	: builds release on all configuraions (Release, Debug) on intel and the binaries are copied to "w:\code\test\" and the compile logs are generated in w:\build.log
goto exit

:exit-error
echo ************************************************************************
echo Error occured while compilation, please check the logs (%builgLogFile%). error=%errorlevel%
exit /B 1
echo ************************************************************************
goto exit

:buildbeacon
echo "*********************************"
echo "Creating ma sdk folder and fillig content"
echo "*********************************"
rem Create the ma_sdk base folder and the folder structure
set MA_SDK_BASE=%MA_BASE%\tools\ma_sdk
if not exist %MA_SDK_BASE% (
	echo creating folder %MA_SDK_BASE%
	mkdir %MA_SDK_BASE%
	mkdir %MA_SDK_BASE%\include
	mkdir %MA_SDK_BASE%\lib
	mkdir %MA_SDK_BASE%\samples
	mkdir %MA_SDK_BASE%\bindings
	mkdir %MA_SDK_BASE%\variant_sdk
	mkdir %MA_SDK_BASE%\rsdk
)
rem copy includes
xcopy /y /S %MA_BASE%\include %MA_SDK_BASE%\include\
xcopy /Y /S %MA_BASE%\bindings %MA_SDK_BASE%\bindings\

rem copy masign
xcopy /y /S %MA_TOOLS%\masign %MA_SDK_BASE%\masign\

xcopy /Y /S %MA_SDK_BASE%\include\ma\internal\utils\serialization %MA_SDK_BASE%\variant_sdk\include\ma\serialization\
xcopy /Y /S %MA_SDK_BASE%\include\ma\ma_variant.h %MA_SDK_BASE%\variant_sdk\include\ma\
xcopy /Y /S %MA_SDK_BASE%\include\ma\ma_common.h %MA_SDK_BASE%\variant_sdk\include\ma\
xcopy /Y /S %MA_SDK_BASE%\include\ma\ma_buffer.h %MA_SDK_BASE%\variant_sdk\include\ma\
xcopy /Y /S %MA_SDK_BASE%\include\ma\ma_errors.h %MA_SDK_BASE%\variant_sdk\include\ma\
xcopy /Y /S %MA_BASE%\build\msvc\msgpack\include %MA_SDK_BASE%\variant_sdk\include\tools\msgpack\
rem trim the unwanted folders
rmdir /S /Q %MA_SDK_BASE%\include\ma\datastore
rmdir /S /Q %MA_SDK_BASE%\include\ma\internal
rmdir /S /Q %MA_SDK_BASE%\include\ma\sensor
rmdir /S /Q %MA_SDK_BASE%\include\ma\p2p
del /F /S /Q %MA_SDK_BASE%\include\ma\ma_sdk.h
rem copy binaries (only ma.lib and also other static libs of variant and mautils)
xcopy /y /S %MA_BASE%\build\msvc\release\ma.lib %MA_SDK_BASE%\lib\release\
xcopy /y /S %MA_BASE%\build\msvc\debug\ma.lib %MA_SDK_BASE%\lib\debug\
rem Variant SDK (variant, serialization, utils, msgpack, etc)
xcopy /y /S %MA_BASE%\build\msvc\releasestaticMT\*.lib %MA_SDK_BASE%\variant_sdk\lib\releasestaticMT\
xcopy /y /S %MA_BASE%\build\msvc\debugstaticMT\*.lib %MA_SDK_BASE%\variant_sdk\lib\debugstaticMT\
xcopy /Y /S %MA_BASE%\build\msvc\msgpack\lib\releasestaticMT\*.lib %MA_SDK_BASE%\variant_sdk\lib\releasestaticMT\
xcopy /Y /S %MA_BASE%\build\msvc\msgpack\lib\debugstaticMT\*.lib %MA_SDK_BASE%\variant_sdk\lib\debugstaticMT\
rem RSDK Files

copy %MA_BASE%\build\msvc\release\ma_db_viewer.dll %MA_BASE%\build\msvc\release\policy_viewer.dll
copy %MA_BASE%\build\msvc\release\ma_db_viewer.dll %MA_BASE%\build\msvc\release\madb2xml.dll

xcopy /y /S %MA_BASE%\build\msvc\release\ma_client.dll %MA_SDK_BASE%\rsdk\release\
xcopy /y /S %MA_BASE%\build\msvc\release\ma_logger.dll %MA_SDK_BASE%\rsdk\release
xcopy /y /S %MA_BASE%\build\msvc\release\ma_msgbus.dll %MA_SDK_BASE%\rsdk\release\
xcopy /y /S %MA_BASE%\build\msvc\release\ma_variant.dll %MA_SDK_BASE%\rsdk\release\
xcopy /y /S %MA_BASE%\build\msvc\release\ma_serialization.dll %MA_SDK_BASE%\rsdk\release\
xcopy /y /S %MA_BASE%\build\msvc\release\ma_crypto.dll %MA_SDK_BASE%\rsdk\release\
xcopy /y /S %MA_BASE%\build\msvc\release\mfecryptc.dll %MA_SDK_BASE%\rsdk\release\
xcopy /y /S %MA_BASE%\build\msvc\release\ma_utils.dll %MA_SDK_BASE%\rsdk\release\
xcopy /y /S %MA_BASE%\build\msvc\release\ma_xml.dll %MA_SDK_BASE%\rsdk\release\
xcopy /y /S %MA_BASE%\build\msvc\release\libuv.dll %MA_SDK_BASE%\rsdk\release\
xcopy /y /S %MA_BASE%\build\msvc\release\trex.dll %MA_SDK_BASE%\rsdk\release\
xcopy /y /S %MA_BASE%\build\msvc\release\mxml.dll %MA_SDK_BASE%\rsdk\release\
xcopy /y /S %MA_BASE%\build\msvc\release\ma_service_manager_client.dll %MA_SDK_BASE%\rsdk\release\

xcopy /y /S %MA_BASE%\build\msvc\debug\ma_client.dll %MA_SDK_BASE%\rsdk\debug\
xcopy /y /S %MA_BASE%\build\msvc\debug\ma_logger.dll %MA_SDK_BASE%\rsdk\debug
xcopy /y /S %MA_BASE%\build\msvc\debug\ma_msgbus.dll %MA_SDK_BASE%\rsdk\debug\
xcopy /y /S %MA_BASE%\build\msvc\debug\ma_variant.dll %MA_SDK_BASE%\rsdk\debug\
xcopy /y /S %MA_BASE%\build\msvc\debug\ma_serialization.dll %MA_SDK_BASE%\rsdk\debug\
xcopy /y /S %MA_BASE%\build\msvc\debug\ma_crypto.dll %MA_SDK_BASE%\rsdk\debug\
xcopy /y /S %MA_BASE%\build\msvc\debug\mfecryptc.dll %MA_SDK_BASE%\rsdk\debug\
xcopy /y /S %MA_BASE%\build\msvc\debug\ma_utils.dll %MA_SDK_BASE%\rsdk\debug\
xcopy /y /S %MA_BASE%\build\msvc\debug\ma_xml.dll %MA_SDK_BASE%\rsdk\debug\
xcopy /y /S %MA_BASE%\build\msvc\debug\libuv.dll %MA_SDK_BASE%\rsdk\debug\
xcopy /y /S %MA_BASE%\build\msvc\debug\trex.dll %MA_SDK_BASE%\rsdk\debug\
xcopy /y /S %MA_BASE%\build\msvc\debug\mxml.dll %MA_SDK_BASE%\rsdk\debug\
xcopy /y /S %MA_BASE%\build\msvc\debug\ma_service_manager_client.dll %MA_SDK_BASE%\rsdk\debug\

xcopy /y /S %MA_BASE%\build\msvc\release\x64\ma_client.dll %MA_SDK_BASE%\rsdk\release\x64\
xcopy /y /S %MA_BASE%\build\msvc\release\x64\ma_logger.dll %MA_SDK_BASE%\rsdk\release\x64\
xcopy /y /S %MA_BASE%\build\msvc\release\x64\ma_msgbus.dll %MA_SDK_BASE%\rsdk\release\x64\
xcopy /y /S %MA_BASE%\build\msvc\release\x64\ma_variant.dll %MA_SDK_BASE%\rsdk\release\x64\
xcopy /y /S %MA_BASE%\build\msvc\release\x64\ma_serialization.dll %MA_SDK_BASE%\rsdk\release\x64\
xcopy /y /S %MA_BASE%\build\msvc\release\x64\ma_crypto.dll %MA_SDK_BASE%\rsdk\release\x64\
xcopy /y /S %MA_BASE%\build\msvc\release\x64\mfecryptc.dll %MA_SDK_BASE%\rsdk\release\x64\
xcopy /y /S %MA_BASE%\build\msvc\release\x64\ma_utils.dll %MA_SDK_BASE%\rsdk\release\x64\
xcopy /y /S %MA_BASE%\build\msvc\release\x64\ma_xml.dll %MA_SDK_BASE%\rsdk\release\x64\
xcopy /y /S %MA_BASE%\build\msvc\release\x64\libuv.dll %MA_SDK_BASE%\rsdk\release\x64\
xcopy /y /S %MA_BASE%\build\msvc\release\x64\trex.dll %MA_SDK_BASE%\rsdk\release\x64\
xcopy /y /S %MA_BASE%\build\msvc\release\x64\mxml.dll %MA_SDK_BASE%\rsdk\release\x64\
xcopy /y /S %MA_BASE%\build\msvc\release\x64\ma_service_manager_client.dll %MA_SDK_BASE%\rsdk\release\x64\

xcopy /y /S %MA_BASE%\build\msvc\debug\x64\ma_client.dll %MA_SDK_BASE%\rsdk\debug\x64\
xcopy /y /S %MA_BASE%\build\msvc\debug\x64\ma_logger.dll %MA_SDK_BASE%\rsdk\debug\x64\
xcopy /y /S %MA_BASE%\build\msvc\debug\x64\ma_msgbus.dll %MA_SDK_BASE%\rsdk\debug\x64\
xcopy /y /S %MA_BASE%\build\msvc\debug\x64\ma_variant.dll %MA_SDK_BASE%\rsdk\debug\x64\
xcopy /y /S %MA_BASE%\build\msvc\debug\x64\ma_serialization.dll %MA_SDK_BASE%\rsdk\debug\x64\
xcopy /y /S %MA_BASE%\build\msvc\debug\x64\ma_crypto.dll %MA_SDK_BASE%\rsdk\debug\x64\
xcopy /y /S %MA_BASE%\build\msvc\debug\x64\mfecryptc.dll %MA_SDK_BASE%\rsdk\debug\x64\
xcopy /y /S %MA_BASE%\build\msvc\debug\x64\ma_utils.dll %MA_SDK_BASE%\rsdk\debug\x64\
xcopy /y /S %MA_BASE%\build\msvc\debug\x64\ma_xml.dll %MA_SDK_BASE%\rsdk\debug\x64\
xcopy /y /S %MA_BASE%\build\msvc\debug\x64\libuv.dll %MA_SDK_BASE%\rsdk\debug\x64\
xcopy /y /S %MA_BASE%\build\msvc\debug\x64\trex.dll %MA_SDK_BASE%\rsdk\debug\x64\
xcopy /y /S %MA_BASE%\build\msvc\debug\x64\mxml.dll %MA_SDK_BASE%\rsdk\debug\x64\
xcopy /y /S %MA_BASE%\build\msvc\debug\x64\ma_service_manager_client.dll %MA_SDK_BASE%\rsdk\debug\x64\
echo "*********************************"
echo "Building Beacon binaries"
echo "*********************************"
set BC_BASE=%MA_BASE%\tools\beacon\
set MSBuildEmitSolution=1
echo allbits=%allbits%
echo allconfigs=%allconfigs%
echo.
for %%A in %allbits% Do (
	if %%A==Win32 (
		 call "%VS100COMNTOOLS%\..\..\vc\vcvarsall.bat" x86
	) else (
		if %arch%==itanium ( 
			call "%VS100COMNTOOLS%\..\..\vc\vcvarsall.bat" x86_ia64
			) else (
				call "%VS100COMNTOOLS%\..\..\vc\vcvarsall.bat" x86_amd64
			)
	)
	for %%B in %allconfigs% Do (
		msbuild %BC_BASE%\build\msvc\bc.sln /t:%target% /p:Configuration=%%B /p:Platform=%%A >> %builgLogFile%				
		if errorlevel 1 goto exit-error
	)
)
goto exit

:prepmsgbusauth
set MA_SIGN=%MA_TOOLS%\masign\masign.exe
set MA_MASIGN_PREP_OUT=%MA_BASE%/build/packager/data
set BC_BASE=%MA_BASE%\tools\beacon\
set BC_MASIGN_PREP_OUT=%BC_BASE%/build/packager/data

if exist %MA_BASE%/build/packager/data/ma_msgbus_auth.xml {
	del  %MA_BASE%/build/packager/data/ma_msgbus_auth.xml
	)

if exist %MA_BASE%/build/packager/data/ma_msgbus_auth.sig (
	del  %MA_BASE%/build/packager/data/ma_msgbus_auth.sig
)

if exist %BC_BASE%/build/packager/data/ma_msgbus_auth.xml {
	del  %BC_BASE%/build/packager/data/ma_msgbus_auth.xml
	)

if exist %BC_BASE%/build/packager/data/ma_msgbus_auth.sig (
	del  %BC_BASE%/build/packager/data/ma_msgbus_auth.sig
)
%MA_SIGN% -prep -dir %MA_BASE%\build\msvc\release -name masvc.exe -out %MA_MASIGN_PREP_OUT%
%MA_SIGN% -prep -dir %MA_BASE%\build\msvc\release -name macmnsvc.exe -out %MA_MASIGN_PREP_OUT%
%MA_SIGN% -prep -dir %MA_BASE%\build\msvc\release -name macompatsvc.exe -out %MA_MASIGN_PREP_OUT%
%MA_SIGN% -prep -dir %MA_BASE%\build\msvc\release -name maconfig.exe -out %MA_MASIGN_PREP_OUT%
%MA_SIGN% -prep -dir %MA_BASE%\build\msvc\release -name cmdagent.exe -out %MA_MASIGN_PREP_OUT%

%MA_SIGN% -prep -dir %MA_BASE%\build\msvc\release -name UpdaterUI.exe -out %MA_MASIGN_PREP_OUT%
%MA_SIGN% -prep -dir %MA_BASE%\build\msvc\release -name Mue.exe -out %MA_MASIGN_PREP_OUT%

%MA_SIGN% -prep -dir %MA_BASE%\build\msvc\release\x64 -name masvc.exe -out %MA_MASIGN_PREP_OUT%
%MA_SIGN% -prep -dir %MA_BASE%\build\msvc\release\x64 -name macmnsvc.exe -out %MA_MASIGN_PREP_OUT%
%MA_SIGN% -prep -dir %MA_BASE%\build\msvc\release\x64 -name macompatsvc.exe -out %MA_MASIGN_PREP_OUT%
%MA_SIGN% -prep -dir %MA_BASE%\build\msvc\release\x64 -name maconfig.exe -out %MA_MASIGN_PREP_OUT%
%MA_SIGN% -prep -dir %MA_BASE%\build\msvc\release\x64 -name cmdagent.exe -out %MA_MASIGN_PREP_OUT%

%MA_SIGN% -prep -dir %BC_BASE%\build\msvc\release -name beacon.exe -out %BC_MASIGN_PREP_OUT%
%MA_SIGN% -prep -dir %BC_BASE%\build\msvc\release -name beacon++.exe -out %BC_MASIGN_PREP_OUT%
%MA_SIGN% -prep -dir %BC_BASE%\build\msvc\release\x64 -name beacon.exe -out %BC_MASIGN_PREP_OUT%
%MA_SIGN% -prep -dir %BC_BASE%\build\msvc\release\x64 -name beacon++.exe -out %BC_MASIGN_PREP_OUT%

goto exit

:buildmsi
set BC_BASE=%MA_BASE%\tools\beacon\
echo "*********************************"
echo "Building MA Installer"
echo "*********************************"
set allbits=(%platform%)
if %platform%==Win32 set allbits=(x86)
if defined allbit set allbits=(x86, x64)
for %%A in %allbits% Do (
	for %%B in %allconfigs% Do (
		msbuild %MA_BASE%\build\packager\win\MirAcklezAgent.sln /t:Rebuild /p:Configuration=Release /p:Platform=%%A >> %builgLogFile%
		if errorlevel 1 goto exit-error
	)
)
echo "*********************************"
echo "Building Beacon Installer"
echo "*********************************"

msbuild %BC_BASE%\build\packager\win\src\beacon.wixproj /t:Rebuild /p:Configuration=Release /p:Platform=x86 >> %builgLogFile%
if errorlevel 1 goto exit-error
msbuild %BC_BASE%\build\packager\win\src\beacon.wixproj /t:Rebuild /p:Configuration=Release /p:Platform=x64 >> %builgLogFile%
if errorlevel 1 goto exit-error
msbuild %BC_BASE%\build\packager\win\src\beacon++.wixproj /t:Rebuild /p:Configuration=Release /p:Platform=x86 >> %builgLogFile%
if errorlevel 1 goto exit-error
msbuild %BC_BASE%\build\packager\win\src\beacon++.wixproj /t:Rebuild /p:Configuration=Release /p:Platform=x64 >> %builgLogFile%
if errorlevel 1 goto exit-error

:exit
echo ************************************************************************
echo Build is Success.
echo ************************************************************************
exit /B 0
