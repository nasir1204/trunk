include $(MA_BASE)/build/makefiles/defs.mk

ifdef SOL
SHELL=/usr/bin/bash
endif

export CMA_BASE=$(MA_BASE)
export MA_COMPAT_BASE = $(MA_BASE)/src/compat/mue
##############################
#Set the MAC OSX SDK root
ifdef MACX
#export MACOSX_SDKROOT=/Developer/SDKs/MacOSX10.5.sdk
export MACOSX_SDKROOT=/Developer/SDKs/MacOSX10.9.sdk
#export MACOSX_SDKROOT=/Developer/SDKs/MacOSX10.7.sdk
endif
##############################

#this variable needed to find rsamanager and licensemanager
ifndef MA_TOOLS
ifdef MACX
ifdef CURL
export MA_TOOLS=$(CURL)/..
else
export MA_TOOLS=$(MA_BASE)/..
endif
else
export MA_TOOLS=$(MA_BASE)/..
endif
endif

export CRYPTO_LIB=cryptshim -lmfecryptc 
export CRYPTSHIM_LIB_PATH=$(MA_TOOLS)/cryptshim/lib
export MFECRYPTC_LIB_PATH=$(MA_TOOLS)/mfecryptc/lib
export MFEDISCOVERY_LIB_PATH=$(MA_TOOLS)/mfediscovery/lib
export MFEDISCOVERY_STATIC_LIB_PATH=$(MA_TOOLS)/mfediscovery_static/lib
export CRYPTO_INCLUDE=$(MA_TOOLS)/cryptshim/include 
export SIG_DIR=$(MA_TOOLS)/mfecryptc/lib
export SIG_UTILITY=masign

SIG_LIBRARY_PATH=LD_LIBRARY_PATH=$(MFECRYPTC_LIB_PATH)

ifdef HPUX
SIG_LIBRARY_PATH=SHLIB_PATH=$(MFECRYPTC_LIB_PATH):/usr/local/lib:.:/opt/hp-gcc/4.0.1/ilp32/lib:/usr/lib
endif
ifdef AIX
SIG_LIBRARY_PATH=LIBPATH=$(MFECRYPTC_LIB_PATH)
endif
ifdef MACX
SIG_LIBRARY_PATH=DYLD_LIBRARY_PATH=$(MFECRYPTC_LIB_PATH)
endif
ifdef SOL
SIG_LIBRARY_PATH=LD_LIBRARY_PATH=/usr/local/lib:$(MFECRYPTC_LIB_PATH)
endif
ifdef WSH
SIG_LIBRARY_PATH=LD_LIBRARY_PATH=/usr/local/lib:$(MFECRYPTC_LIB_PATH):/usr/gcc-4.1.2/lib
endif
#######################################################################


########## boost defines
ifndef BOOST_BASE
BOOST_BASE=$(MA_BASE)/../boost
endif
BOOST_INCLUDE_132=$(BOOST_BASE)/include/
BOOST_LIB_132=$(BOOST_BASE)/lib
###The library names will be suffixed with the platform specific extensions, hence use the simply expanded form
BOOST_THREAD:=boost_thread
BOOST_SERIALIZATION:=boost_serialization
BOOST_WSERIALIZATION:=boost_wserialization
BOOST_FILESYSTEM:=boost_filesystem
BOOST_SYSTEM:=boost_system
BOOST_UNIT_TEST_FRAMEWORK=$(BOOST_LIB_132)/libboost_unit_test_framework.a


###### mfediscovery defines
ifndef MFEDISCOVERY_LIB_PATH
MFEDISCOVERY_LIB_PATH=$(MA_TOOLS)/mfediscovery/lib
endif

#use open SSL on MAC only
ifdef MACX
SSL_LIB=$(CURL)/lib
SSL_LIBNAME=-lssl 
CRYPTO_LIBNAME=-lcrypto
RSAMGR_LIB_DIR_ARG=
else
SSL_LIB=$(RSA_SSLC)/lib
ifdef HPUX
SSL_LIBNAME=-lsslc_fips140
else
SSL_LIBNAME=
endif
CRYPTO_LIBNAME=
RSAMGR_LIB_DIR_ARG=-L$(MA_TOOLS)/rsamanager/lib
endif


####### Common include paths
#sub makes may want to append additional stuff to INCLUDES, so we have the "simply expanded" flavor for INCLUDES
INCLUDES:= 	-I../include \
		-I../../include \
		-I$(MA_BASE)/src/compat/mue/common/include \
		-I$(MA_BASE)/src/compat/mue/common/nwa/include \
		-I$(MA_BASE)/src/compat/mue/common/nwa/mfecomn/include \
		-I$(MA_BASE)/src/compat/mue/common/nwa/include/mfecomn \
		-I$(MA_BASE)/src/compat/mue/common/nwa/include/registry \
		-I$(BOOST_INCLUDE_132) \
		-I$(CURL_INCLUDE) \
		-I$(ZLIB_INCLUDE) \
		-I$(CRYPTO_INCLUDE) \
		-I$(MA_BASE)/include \
		-I$(MA_BASE)/src/compat/mue/common/include  \
		-I$(MA_TOOLS)/libuv/include \
		-I$(MA_COMPAT_BASE)/common/mue/include  \
		-I$(MA_BASE)/bindings/cxx/include 


#DEFINES:=	-D_LINUX_REDHAT \
#		-DSOMAJVER=$(MYSOMAJVER)\
#		-DSOMINVER=$(MYSOMINVER) \
#		-DSUBMINVER=$(MYSUBMINVER) \
#		-DSOBLDNUM=$(MYSOBLDNUM) \
#		-DHOTFIXNUM=$(MYHOTFIXNUM) \
#		-D_REENTRANT \
#		-DNO_NAT_METH \
#		-DMA_NO_DISPATCHER

DEFINES:= $(DEFINES) \
		-D_REENTRANT \
		-DNO_NAT_METH \
		-DSOMAJVER=$(MAJ_VER)\
		-DSOMINVER=$(MIN_VER) \
		-DSUBMINVER=$(SUBMIN_VER) \
		-DSOBLDNUM=$(BUILD_VER) \
		-DHOTFIXNUM=$(HOTFIX_VER) \
		-D_LINUX_REDHAT
		

#####################################################################
#platform specific settings from ( MA48\unix\buildproject\aix.mk)		

#DEFINES:=$(DEFINES) -DAIX -D_AIX_ -DPLATFORM_AIX -DNO_CERT -DAIX4_3  
#PLATFORM=AIX
#ENGINE_INCLUDES=-I$(CMA_BASE)/include/aix/engine

ifeq ($(OS_NAME),AIX)    # Linux specific settings
	DEFINES:= $(DEFINES) -DAIX -D_AIX_ -DPLATFORM_AIX -DNO_CERT -DAIX4_3  -Dunix
	export AIX=1
endif
		
#Extend the defines to specify linux
#DEFINES:=$(DEFINES) -DSOL -DSOLARIS -D_SOLARIS_ -pthreads
#PLATFORM=SOL
#ENGINE_INCLUDES=-I$(CMA_BASE)/include/solaris/engine
#ENGINE_DEFINES=-DUNIX -DSOLARIS -DSUN

ifeq ($(OS_NAME),SunOS)    # Linux specific settings
	DEFINES:= $(DEFINES) -DSOL -DSOLARIS -D_SOLARIS_ -pthreads  -DUNIX -DSOLARIS -DSUN
	export SOL=1	
        INCLUDES := $(INCLUDES) -I$(MA_TOOLS)/ma_sys/solaris/include/
endif


ifeq ($(OS_NAME),Linux)    # Linux specific settings
	DEFINES:= $(DEFINES) -DLNX 
	export LNX=1
	export _LINUX_REDHAT=1
	ifeq ($(BIT),64) 
		DEFINES:= $(DEFINES) -DLIFELINE
		export LIFELINE=1
		BOOST_THREAD:=boost_thread-mt
	endif
endif	

ifeq ($(OS_NAME),Darwin)    # Linux specific settings
	DEFINES:= $(DEFINES) -D__APPLE__  -DMacOS -DUNIX -DMACX -DNOXCODE
	export _LINUX_REDHAT=1
	export __APPLE__=1
	export MacOS=1
	export UNIX=1
endif		

ifeq ($(BIT),32) 
LIBPATH:=	-L$(BOOST_LIB_132) \
		-L$(MA_BASE)/build/makefiles/$(CONFIGURATION) \
		-L$(MA_TOOLS)/curl/lib/$(CONFIGURATION) \
		-L$(MA_TOOLS)/mfecryptc/lib/$(CONFIGURATION) \
		-L$(MA_TOOLS)/libuv/lib/$(CONFIGURATION) \
		-L$(MA_TOOLS)/trex/lib/$(CONFIGURATION) \
		-L$(MA_TOOLS)/msgpack/lib/$(CONFIGURATION) \
		-L$(MA_TOOLS)/libini/lib/$(CONFIGURATION) \
		-L$(MA_TOOLS)/zlib/lib/$(CONFIGURATION) \
		-L$(MA_TOOLS)/mxml/lib/$(CONFIGURATION) \
		-L$(MA_COMPAT_BASE)/unix/$(CONFIGURATION) \
		-L/usr/local/lib 
else
LIBPATH:=	-L$(BOOST_LIB_132) \
		-L$(MA_BASE)/build/makefiles/$(CONFIGURATION)/$(BIT) \
		-L$(MA_TOOLS)/curl/lib/$(CONFIGURATION)/$(BIT) \
		-L$(MA_TOOLS)/mfecryptc/lib/$(CONFIGURATION)/$(BIT) \
		-L$(MA_TOOLS)/libuv/lib/$(CONFIGURATION)/$(BIT) \
		-L$(MA_TOOLS)/trex/lib/$(CONFIGURATION)/$(BIT) \
		-L$(MA_TOOLS)/msgpack/lib/$(CONFIGURATION)/$(BIT)\
		-L$(MA_TOOLS)/libini/lib/$(CONFIGURATION)/$(BIT) \
		-L$(MA_TOOLS)/zlib/lib/$(CONFIGURATION)/$(BIT) \
		-L$(MA_TOOLS)/mxml/lib/$(CONFIGURATION)/$(BIT) \
		-L$(MA_COMPAT_BASE)/unix/$(CONFIGURATION) \
		-L/usr/local/lib -L/usr/lib64/
endif

ifeq ($(OS_NAME), HP-UX)    # HPUX specific settings
        DEFINES:= $(DEFINES) -DHPUX -D_HPUX_ -D_PSTAT64 -DUNIX -DREENTRANT -D_REENTRANT -D_HPIA_       
        export HPUX=1
        export _HPUX_=1
        ifeq ($(BIT),64)
                BOOST_THREAD:=boost_thread-mt
				LIBPATH:= $(LIBPATH) -L/usr/local/lib/hpux64/
		else
			ifeq ($(BIT),32)
				LIBPATH:= $(LIBPATH) -L/usr/local/lib/hpux32/
			endif 
        endif
        LIBPATH := $(LIBPATH) -L$(MA_TOOLS)/zlib/$(LIB_DIR_SUFFIX) -L$(MA_TOOLS)/openssl/$(LIB_DIR_SUFFIX) -L$(MA_TOOLS)/sqlite/$(LIB_DIR_SUFFIX)/


endif

SOURCES:=	./source \
		../source

ifdef CRUISE_ENABLED
DEFINES:= $(DEFINES) -DCRUISE_ENABLED
endif


MAKEFILE=Makefile



