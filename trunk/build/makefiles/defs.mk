###################################################
#Copyright mileaccess Inc 2012, All rights reserved 
#Date 		August 2012
###################################################

# MA Version information
ifndef MAJ_VER
MAJ_VER :=5
endif
ifndef MIN_VER
MIN_VER :=0
endif
ifndef SUBMIN_VER
SUBMIN_VER :=0
endif
ifndef BUILD_VER
BUILD_VER :=0
endif
ifndef HOTFIX_VER
HOTFIX_VER :=0
endif

MAJVER :=$(MAJ_VER)
MINVER :=$(MIN_VER)
SUBMINVER :=$(SUBMIN_VER)
BUILDNUM :=$(BUILD_VER)
HOTFIXNUM :=$(HOTFIX_VER)

# Setting CONFIGURATION based on value of CONFIG
ifeq ($(CONFIG),debug)
export CONFIGURATION := debug
else
export CONFIGURATION := release
endif

ifeq ($(MSGBUS_AUTH_FLAG),disable)
DEFINES := $(DEFINES) -DMSGBUS_AUTH_DISABLED
endif



# Setting MA_BASE if not set already
ifndef MA_BASE 
export MA_BASE := $(PWD)/../../
endif

# Setting MA_TOOLS if not set already
ifndef MA_TOOLS
export MA_TOOLS := $(MA_BASE)/../ma_tools/
endif 

# Setting CRYPTO if not set already , default is rsa 
ifndef CRYPTO
export CRYPTO := rsa
endif 

ifndef MASIGN
export MASIGN := $(MA_TOOLS)/masign/masign
endif 

ifndef MASIGN_PREP_OUT
export MASIGN_PREP_OUT := $(MA_BASE)/build/packager/data
endif 


# Setting MA_COMPAT_SDK if not set already
ifndef MA_COMPAT_SDK
export MA_COMPAT_SDK := $(MA_BASE)/../ma_compat_sdk/
endif 

# Default settings and compilaiton flages

ifndef ARCH_BIT
export ARCH_BIT := 32
endif

export BIT := $(ARCH_BIT)

ifndef ARCH
export ARCH := intel
endif

CC := gcc
CXX := g++
# default linker is gcc, this can be overwritten in local makefiles for c++ modules 
LINKER := $(CC)
AR := ar

LIB_EXTN := so
SLIB_EXTN := a
CFLAGS :=  -Wall  -c -fPIC
CXXFLAGS :=  -Wall -c -fPIC
SO_LFLAGS := -fPIC -shared
LFLAGS := 
ARFLAGS := -rcs
LIBS := -lpthread
ARCH_FLAGS :=

# adding -m64 option for 64bit build
ifeq ($(ARCH_BIT),64)
BIT := 64
#CFLAGS := $(CFLAGS) -m64 -mtune=generic
#CXXFLAGS := $(CXXFLAGS) -m64 -mtune=generic
#SO_LFLAGS := $(SO_LFLAGS) -m64
#LFLAGS := $(LFLAGS) -m64 
endif

#if TOOLCHAIN path is set then overwrite the compiler macros with TOOLCHAIN paths 
ifdef TOOLCHAIN
CC := $(TOOLCHAIN)/gcc
CXX := $(TOOLCHAIN)/g++
LINKER := $(CC)
endif 

# Setting macros for all the source paths
SOURCE_DIR := $(MA_BASE)/src
INCLUDE_DIR := $(MA_BASE)/include 
BUILD_DIR := $(MA_BASE)/build

ifeq ($(BIT),32) 
LIB_DIR_SUFFIX=lib/$(CONFIGURATION)
STATIC_LIB_DIR_SUFFIX=lib/$(CONFIGURATION)_static
BIN_DIR_SUFFIX=bin/$(CONFIGURATION)
OUTPUT_DIR := $(BUILD_DIR)/makefiles/$(CONFIGURATION)
STATIC_OUTPUT_DIR := $(BUILD_DIR)/makefiles/$(CONFIGURATION)_static
else
OUTPUT_DIR := $(BUILD_DIR)/makefiles/$(CONFIGURATION)/$(BIT)
STATIC_OUTPUT_DIR := $(BUILD_DIR)/makefiles/$(CONFIGURATION)_static/$(BIT)/
LIB_DIR_SUFFIX=lib/$(CONFIGURATION)/$(BIT)
STATIC_LIB_DIR_SUFFIX=lib/$(CONFIGURATION)_static/$(BIT)
BIN_DIR_SUFFIX=bin/$(CONFIGURATION)/$(BIT)
endif


MODULES_DIR := $(SOURCE_DIR)/modules
CORE_DIR := $(SOURCE_DIR)/core
SERVICES_DIR := $(SOURCE_DIR)/services
CLIENTS_DIR := $(SOURCE_DIR)/clients
UTILS_DIR := $(SOURCE_DIR)/utils
MA_COMPAT_DIR := $(SOURCE_DIR)/compat
APPS_DIR := $(SOURCE_DIR)/apps


TESTS_DIR := $(MA_BASE)/tests
MODULES_TEST_DIR := $(TESTS_DIR)/modules
CORE_TEST_DIR := $(TESTS_DIR)/core
APPS_TEST_DIR := $(TESTS_DIR)/apps/
SERVICES_TEST_DIR := $(TESTS_DIR)/services
CLIENTS_TEST_DIR := $(TESTS_DIR)/clients
UTILS_TEST_DIR := $(TESTS_DIR)/utils
UNIT_TEST_DIR:=unit_test

TOOLS_DIR := $(MA_BASE)/tools

# common runtime library path , over write it for any platforms
RUNTIME_LIBRARY_PATH=export LD_LIBRARY_PATH=$(OUTPUT_DIR):/usr/local/lib:./

ifndef MA_INSTALL_PATH
ifeq ($(OS_NAME),Darwin)
MA_INSTALL_PATH := /Library/mileaccess/agent/
else
MA_INSTALL_PATH := /opt/mileaccess/agent/
endif
endif

# Checking OS name and overwrite/add the specific flags to existing macros  
uname_S := $(shell sh -c 'uname -s 2>/dev/null || echo not')

ifndef OS_NAME  
OS_NAME = $(uname_S)
endif

ifneq ($(OS_NAME),Linux)
LFLAGS := $(LFLAGS) -liconv   
endif

ifndef MA_USE_DISPATCHER
DEFINES:= $(DEFINES) -DMA_NO_DISPATCHER=1
endif

#Solaris procfs with LFS requires 64 bit binaries
DEFINES_LARGEFILE := -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64 
	   
ifeq ($(CRYPTO),openssl)
export MFECRYPTC_PATH := $(MA_TOOLS)/mfecryptc_openssl/
export CURL_PATH := $(MA_TOOLS)/curl_openssl/
DEFINES := $(DEFINES) -DMA_NETWORK_WITH_OPENSSL
else
export MFECRYPTC_PATH := $(MA_TOOLS)/mfecryptc/
export CURL_PATH := $(MA_TOOLS)/curl/
endif

ifeq ($(OS_NAME),Linux)    # Linux specific settings
   LINUX_DIST :=  $(shell sh -c '$(MA_BASE)/build/get_linux_dist.sh' || _unknown_)
   #DEFINES := $(DEFINES) -DLINUX -D_GNU_SOURCE -D$(LINUX_DIST)  
   DEFINES := $(DEFINES) -DLINUX -D_GNU_SOURCE -DHAVE_ICONV
   LIBS := $(LIBS) -ldl -lc -lm -lrt
   LIB_EXTN := $(LIB_EXTN)
   SLIB_EXTN := $(SLIB_EXTN)
   ARFLAGS := -rcs
   SO_LFLAGS := $(SO_LFLAGS) -fPIC -Wl,-rpath,$(MA_INSTALL_PATH)/lib:$(MA_INSTALL_PATH)/lib/rsdk:$(MA_INSTALL_PATH)/lib/tools/
   # -march=i686 is adding to CFLAGS to resolve the symbols __sync_sub_and_fetch and __sync_add_and_fetch whihc are using for atomic operations
   CFLAGS := $(CFLAGS)
   LFLAGS := $(LFLAGS) -Wl,-rpath,$(MA_INSTALL_PATH)/lib:$(MA_INSTALL_PATH)/lib/rsdk:$(MA_INSTALL_PATH)/lib/tools/
ifeq ($(CONFIG),release)
   CFLAGS := $(CFLAGS) -O3
endif
ifeq ($(ARCH_BIT),32)
   CFLAGS := $(CFLAGS) -march=i686
endif
ifeq ($(LINUX_DIST),_mlos_)
   DEFINES := $(DEFINES) -DMA_PLATFORM_MLOS 
else
   DEFINES := $(DEFINES) -DMA_PLATFORM_LINUX
endif
else
ifeq ($(OS_NAME),SunOS)   # Solaris specific settings
   DEFINES_LARGEFILE := 
   SHELL := /usr/bin/bash
   DEFINES := $(DEFINES)  -DSOLARIS -D_POSIX_PTHREAD_SEMANTICS -DMA_PLATFORM_SOLARIS -DSunOS -DHAVE_ICONV
   SO_LFLAGS := $(SO_LFLAGS) -G -R$(MA_INSTALL_PATH)/lib -R$(MA_INSTALL_PATH)/lib/rsdk -R$(MA_INSTALL_PATH)/lib/tools/
   LFLAGS := $(LFLAGS) -R$(MA_INSTALL_PATH)/lib -R$(MA_INSTALL_PATH)/lib/rsdk -R$(MA_INSTALL_PATH)/lib/tools/
   LIBS := $(LIBS) -lrt -lkstat -lsocket -lm -lnsl -lsendfile 
   AR=/usr/ccs/bin/ar
   CFLAGS := $(CFLAGS) -mcpu=v9
   CXXFLAGS := $(CXXFLAGS) -mcpu=v9
   INCLUDES := $(INCLUDES) -I$(MA_TOOLS)/ma_sys/solaris/include/
   export SOLARIS=1
else
ifeq ($(OS_NAME),HP-UX)     # HP UX specific ssettings
   DEFINES := $(DEFINES)  -DHPUX -D_HPUX_ -D_XOPEN_SOURCE_EXTENDED -DREENTRANT -D_REENTRANT -DHAVE_ICONV
   LIB_EXTN := sl
   CFLAGS := $(CFLAGS)  -Wno-missing-braces -D__BIG_ENDIAN__
   CXXFLAGS := $(CXXFLAGS)  -Wno-missing-braces -D__BIG_ENDIAN__
   ifeq ($(ARCH),pa_risc)
   CFLAGS := $(CFLAGS) -D__BIG_ENDIAN__
   CXXFLAGS := $(CXXFLAGS) -D__BIG_ENDIAN__
   endif
   SO_LFLAGS := $(SO_LFLAGS)  -Wl,+s,+nodefaultrpath,+b$(MA_INSTALL_PATH)/lib:$(MA_INSTALL_PATH)/lib/rsdk:$(MA_INSTALL_PATH)/lib/tools/:/usr/lib 
   LFLAGS := $(LFLAGS) -Wl,+s,+nodefaultrpath,+b$(MA_INSTALL_PATH)/lib:$(MA_INSTALL_PATH)/lib/rsdk:$(MA_INSTALL_PATH)/lib/tools/:/usr/lib -D_PSTAT64 
   RUNTIME_LIBRARY_PATH:=export SHLIB_PATH=./:$(OUTPUT_DIR):/usr/local/lib:/usr/local/lib/hpux32:/opt/hp-gcc/4.0.1/ilp32/lib/:/usr/lib/
   LIBGCC := -L/usr/local/lib/hpux32 -lgcc_s
   ifeq ($(ARCH),itanium)
        LIBGCC := -L/usr/local/lib/gcc/ia64-hp-hpux11.23/4.2.3 -lgcc
        DEFINES := $(DEFINES) -DMA_PLATFORM_HPIA 
   else
        DEFINES := $(DEFINES) -DMA_PLATFORM_HPUX
   endif
   LFLAGS := $(LFLAGS) $(LIBGCC)
   SO_LFLAGS := $(SO_LFLAGS) $(LIBGCC)
   LIBS := $(LIBS) -lrt -lunalign
   
   # Macro to enable alignment trap handler
   DEFINES := $(DEFINES) -DHPUX_ALIGNMENT_TRAP_HANDLER
   
   #Hacks that are required for HP-UX versions that are older than 11.31
   uname_R := $(shell sh -c 'uname -r 2>/dev/null || echo not')
   OS_VER = $(uname_R)
   ifneq ($(OS_VER), B.11.31)
        DEFINES := $(DEFINES) -DHPUX_IP_MREQ_HACK -DHPUX_INTPTR_MAX_HACK
   endif
else
ifeq ($(OS_NAME),AIX)       # AIX specific settings
   DEFINES := $(DEFINES) -DAIX -DMA_PLATFORM_AIX -DHAVE_ICONV
   SO_LFLAGS := $(SO_LFLAGS) -Wl,-brtl,-G,-berok,-bM:SRE -Wl,-blibpath:$(MA_INSTALL_PATH)/lib:$(MA_INSTALL_PATH)/lib/rsdk/:$(MA_INSTALL_PATH)/lib/tools/:/usr/lib  -lpthread -lstdc++ -liconv -pthread
   LFLAGS := $(LFLAGS) -fPIC -Wl,-brtl,-bmaxdata:0x80000000,-bmaxstack:0x10000000 -Wl,-blibpath:$(MA_INSTALL_PATH)/lib:$(MA_INSTALL_PATH)/lib/rsdk/:$(MA_INSTALL_PATH)/lib/tools/:/usr/lib  -lpthread -lstdc++ -pthread
   RUNTIME_LIBRARY_PATH:=export LIBPATH=$(OUTPUT_DIR):.:/opt/freeware/lib:/opt/freeware/lib/gcc/powerpc-ibm-aix5.3.0.0/4.2.4/:/usr/lib
   export AIX=1
else
ifeq ($(OS_NAME),Darwin)       # MAC specific settings  # TBD check actual uname -s output on MAC machine
   MACX := 1
   MACOSX_SDKROOT := /Applications/Xcode-Beta.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.9.sdk
   #MACOSX_SDKROOT := /Developer/SDKs/MacOSX10.6.sdk 
   MACOSX_SDKROOT_INCLUDE := /Developer/SDKs/MacOSX10.6.sdk/usr/lib/gcc/i686-apple-darwin10/4.0.1/include 	-I/usr/include
   #MACOSX_SDKROOT := /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.7.sdk
   #MACOSX_SDKROOT_INCLUDE := /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.7.sdk/usr/lib/gcc/i686-apple-darwin11/4.2.1/include
   #To avoid build error: Unknown value '10.6 ' of -mmacosx-version-min
   #export MACOSX_DEPLOYMENT_TARGET=10.6    
   DEFINES := $(DEFINES) -DMACX -D_DARWIN_USE_64_BIT_INODE=1 -DMA_PLATFORM_MAC -DHAVE_ICONV
   LIB_EXTN := dylib
   #SO_LFLAGS := $(SO_LFLAGS) -single_module -undefined warning -flat_namespace -dynamiclib -multiply_defined warning -isysroot /Developer/SDKs/MacOSX10.6.sdk 
   SO_LFLAGS := $(SO_LFLAGS) -single_module -undefined warning -flat_namespace -dynamiclib -multiply_defined warning -Xlinker -headerpad_max_install_names -isysroot $(MACOSX_SDKROOT) -framework CoreServices -framework CoreFoundation
   LFLAGS := $(LFLAGS)  -isysroot $(MACOSX_SDKROOT) -Xlinker -multiply_defined -Xlinker suppress  -Xlinker -headerpad_max_install_names -Xlinker -rpath -Xlinker $(MA_INSTALL_PATH)/lib -Xlinker -rpath -Xlinker $(MA_INSTALL_PATH)/lib/rsdk -Xlinker -rpath -Xlinker $(MA_INSTALL_PATH)/lib/tools/ 
   RUNTIME_LIBRARY_PATH=export DYLD_LIBRARY_PATH=$(OUTPUT_DIR):/usr/local/lib
   #AR=libtool
   #ARFLAGS=-static
   ifeq ($(ARCH),intel)
     ifeq ($(BIT),64)
        ARCH_FLAGS := -arch x86_64
        #ARCH_FLAGS := -arch x86_64
     else
        ARCH_FLAGS := -arch i386
     endif
   else
   ARCH_FLAGS := -arch ppc 
   endif
else
ifeq ($(OS_NAME),FreeBSD)
   LIBS := $(LIBS) -lkvm -lm 
endif
endif
endif
endif
endif
endif

#### TBD -- Add additional platforms if any ########

# adding debugging options for debug builds
ifeq ($(CONFIGURATION),debug)
CFLAGS := $(CFLAGS) -g -DDEBUG -D_DEBUG
CXXFLAGS := $(CXXFLAGS) -g -DDEBUG -D_DEBUG
endif

INCLUDES := $(INCLUDES) -I. -I$(INCLUDE_DIR)

DEFINES := -DUNIX \
           $(DEFINES) \
	   -DREENTRANT \
           -D_REENTRANT \
           -D$(ARCH) \
	   -DMAJVER=$(MAJ_VER)\
	   -DMINVER=$(MIN_VER) \
	   -DSUBMINVER=$(SUBMIN_VER) \
	   -DBUILDNUM=$(BUILD_VER) \
	   -DHOTFIXNUM=$(HOTFIX_VER) \
	   $(DEFINES_LARGEFILE)

LIBPATH := -L$(OUTPUT_DIR)
STATIC_LIBPATH := $(STATIC_OUTPUT_DIR)

ifeq ($(OS_NAME),HP-UX)     # HP UX specific ssettings
LIBPATH := $(LIBPATH) -L$(MA_TOOLS)/zlib/$(LIB_DIR_SUFFIX)/ -L$(MA_TOOLS)/openssl/$(LIB_DIR_SUFFIX)/ -L$(MA_TOOLS)/curl/$(LIB_DIR_SUFFIX)/ -L$(MA_TOOLS)/sqlite/$(LIB_DIR_SUFFIX)/
endif

SOURCES := $(SOURCE_DIR)

TNTNET:= $(MA_TOOLS)/tntnet/$(BIN_DIR_SUFFIX)/tntnet
ECPPC:= $(MA_TOOLS)/tntnet/$(BIN_DIR_SUFFIX)/ecppc
