###################################################
#Copyright mileaccess Inc 2012, All rights reserved
#Date           August 2012
###################################################

# Common rules for all components

ifeq ($(BIT),32)
OUTDIR = $(CONFIGURATION)
else
OUTDIR = $(CONFIGURATION)/$(BIT)
endif

DEPDIR = $(OUTDIR)/.deps

.PHONY: all init

all: init $(DEPLIBS) $(LIB_TARGET) $(SLIB_TARGET) $(EXE_TARGET) $(UTEXE_TARGET) $(POST_BUILD_TARGET)


init:
	@if [ ! -e $(OUTDIR) ];then mkdir -p $(OUTDIR);fi
	@if [ ! -e $(STATIC_OUTPUT_DIR) ];then mkdir -p $(STATIC_OUTPUT_DIR);fi
	@if [ ! -e $(OUTPUT_DIR) ];then mkdir -p $(OUTPUT_DIR);fi
	@if [ ! -e $(DEPDIR) ];then mkdir $(DEPDIR);fi

VPATH = $(SOURCES):$(OUTDIR)
ASMCOMPILE = $(CC) $(CFLAGS) $(DEFINES) $(INCLUDES) -o $(OUTDIR)/${@F} $<

#CPPCOMPILE = $(CXX) $(CXXFLAGS) $(ARCH_FLAGS) $(DEFINES) $(INCLUDES) -o $(OUTDIR)/${@F} $<
CPPCOMPILE = $(CXX) $(CXXFLAGS) $(ARCH_FLAGS) $(DEFINES) $(INCLUDES) -o $(OUTDIR)/$@ $<
CCOMPILE = $(CC) $(CFLAGS) $(ARCH_FLAGS) $(DEFINES) $(INCLUDES) -o $(OUTDIR)/${@F} $<
#CCOMPILE = $(CC) $(CFLAGS) $(ARCH_FLAGS) $(DEFINES) $(INCLUDES) -o $(OUTDIR)/$@ $<
ECPPCOMPILE = $(ECPPC) $(ECPPFLAGS) $(ECPPFLAGS_CPP) -o $@ $<
ECPPCOMPILE_GIF = $(ECPPC) $(ECPPFLAGS) -m image/gif $(ECPPFLAGS_GIF) -b -o $@ $<
ECPPCOMPILE_JPG = $(ECPPC) $(ECPPFLAGS) -m image/jpg $(ECPPFLAGS_JPG) -b -o $@ $<
ECPPCOMPILE_PNG = $(ECPPC) $(ECPPFLAGS) -m image/png $(ECPPFLAGS_PNG) -b -o $@ $<
ECPPCOMPILE_CSS = $(ECPPC) $(ECPPFLAGS) -m text/css  $(ECPPFLAGS_CSS) -b -o $@ $<
ECPPCOMPILE_JS  = $(ECPPC) $(ECPPFLAGS) -m application/javascript $(ECPPFLAGS_JS) -b -o $@ $<
ECPPCOMPILE_ICO = $(ECPPC) $(ECPPFLAGS) -m image/x-icon $(ECPPFLAGS_ICO) -b -o $@ $<

.s.o:
	@echo Compiling  $<
	$(ASMCOMPILE) 
.c.o:
	@echo creating $< dependencies
	$(CC) $(CFLAGS) -MM $(ARCH_FLAGS) $(DEFINES) $(INCLUDES) $< > $(DEPDIR)/$*.d
	@mv -f $(DEPDIR)/$*.d $(DEPDIR)/$*.d.tmp
	@sed -e 's|.*:|$*.o:|' < $(DEPDIR)/$*.d.tmp > $(DEPDIR)/$*.d
	@sed -e 's/.*://' -e 's/\\$$//' < $(DEPDIR)/$*.d.tmp | fmt -1 | sed -e 's/^ *//' -e 's/$$/:/' >> $(DEPDIR)/$*.d
	@rm -f $(DEPDIR)/$*.d.tmp
	@echo Compiling $< 
	$(CCOMPILE)
.cpp.o .cxx.o:
	@echo creating $< dependencies
	@echo creating $(DEPDIR)/$(^D)
	@mkdir -p $(DEPDIR)/$(^D)
	@mkdir -p $(OUTDIR)/$(^D)
	$(CXX) $(CXXFLAGS) -MM $(ARCH_FLAGS) $(DEFINES) $(INCLUDES) $< > $(DEPDIR)/$*.d
	@mv -f $(DEPDIR)/$*.d $(DEPDIR)/$*.d.tmp
	@sed -e 's|.*:|$*.o:|' < $(DEPDIR)/$*.d.tmp > $(DEPDIR)/$*.d
	@sed -e 's/.*://' -e 's/\\$$//' < $(DEPDIR)/$*.d.tmp | fmt -1 | sed -e 's/^ *//' -e 's/$$/:/' >> $(DEPDIR)/$*.d
	@rm -f $(DEPDIR)/$*.d.tmp
	@echo Compiling $<
	$(CPPCOMPILE)
.ecpp.cpp:
	@echo Compiling $<
	$(ECPPCOMPILE)
.gif.cpp:
	@echo Compiling $<
	$(ECPPCOMPILE_GIF)
.png.cpp:
	@echo Compiling $<
	$(ECPPCOMPILE_PNG)
.jpg.cpp:
	@echo Compiling $<
	$(ECPPCOMPILE_JPG)
.ico.cpp:
	@echo Compiling $<
	$(ECPPCOMPILE_ICO)
.css.cpp:
	@echo Compiling $<
	$(ECPPCOMPILE_CSS)
.js.cpp:
	@echo Compiling $<
	$(ECPPCOMPILE_JS)

-include $(OBJS:%.o=$(DEPDIR)/%.d)

.SUFFIXES: .c .cxx .cpp .s .o .d .ecpp .gif .jpg .css .js .png

ifdef DEPLIBS 
$(DEPLIBS):
	@echo [$(MAKELEVEL)] building $@ for $(TARGET)
	cd $(BUILD_DIR)/makefiles; $(MAKE) $@ OS_NAME=$(OS_NAME) ARCH_BIT=$(ARCH_BIT) ARCH=$(ARCH) CONFIG=$(CONFIGURATION) 
	@echo [$(MAKELEVEL)] built $@ for $(TARGET)
endif


# Rule for build exe
ifdef EXE_TARGET
TARGET := $(EXE_TARGET)
$(EXE_TARGET) : $(OBJS)
	cd $(OUTDIR); \
	$(LINKER) $(LFLAGS) $(ARCH_FLAGS) $(OBJS) $(LIBPATH) $(LIBS) -o $@; \
	cp $(TARGET) $(OUTPUT_DIR);
	rm -f $(CLEANFILES)
endif

# Rule for build shared library
ifdef LIB_TARGET
TARGET := $(LIB_TARGET)

ifndef MACX
LIB_TARGET := $(TARGET).$(LIB_EXTN)
LIBLINK_MAJ := $(LIB_TARGET).$(MAJVER)
LIBLINK_MAJ_MIN := $(LIBLINK_MAJ).$(MINVER)
else
LIB_TARGET := $(TARGET).$(LIB_EXTN)
LIBLINK_MAJ := $(TARGET).$(MAJVER)
LIBLINK_MAJ_MIN := $(TARGET).$(MAJVER).$(MINVER).$(LIB_EXTN)
SO_LFLAGS := $(SO_LFLAGS) -install_name @rpath/$(LIBLINK_MAJ_MIN)
endif

# Sets soname for Linux
ifeq ($(OS_NAME),Linux)
SO_LFLAGS := -Wl,-soname,$(LIBLINK_MAJ) $(SO_LFLAGS)
endif

$(TARGET): $(LIBLINK_MAJ_MIN)

$(LIBLINK_MAJ_MIN) : $(OBJS)
	cd $(OUTDIR); \
	$(LINKER) $(SO_LFLAGS) $(ARCH_FLAGS) $(OBJS) $(LIBPATH) $(LIBS) -o $(LIBLINK_MAJ_MIN); \
	cp $(LIBLINK_MAJ_MIN) $(OUTPUT_DIR); \
	cd $(OUTPUT_DIR); \
	if [ ! -L $(LIBLINK_MAJ) ]; then ln -sf $(LIBLINK_MAJ_MIN) $(LIBLINK_MAJ); fi;	\
	if [ ! -L $(LIB_TARGET) ]; then ln -sf $(LIBLINK_MAJ) $(LIB_TARGET); fi;
endif

# Rule for build static library
ifdef SLIB_TARGET
TARGET := $(SLIB_TARGET)
SLIB_TARGET := $(TARGET).$(SLIB_EXTN)

$(TARGET): $(SLIB_TARGET)

AR_EXTRACT_COMMAND := echo "No extra ar files"
ifdef EXTRA_SLIB
AR_EXTRACT_COMMAND := $(AR) -x $(EXTRA_SLIB)
endif

AR_COMMAND := $(AR) $(ARFLAGS) $(SLIB_TARGET) $(OBJS) $(EXTRA_OBJS)

$(SLIB_TARGET) : $(OBJS)
	cd $(OUTDIR); \
	$(AR_EXTRACT_COMMAND); \
	$(AR_COMMAND); \
	cp $(SLIB_TARGET) $(STATIC_OUTPUT_DIR);
endif



ifdef POST_BUILD_TARGET
$(POST_BUILD_TARGET):
	echo "Executing post build command $(POST_BUILD_COMMAND) "
	$(POST_BUILD_COMMAND)
endif



# Rule for build unit test exe and run if UT_RUN environment variable defined
ifdef UTEXE_TARGET
TARGET := $(UTEXE_TARGET)
$(UTEXE_TARGET) : $(OBJS)
	cd $(OUTDIR); \
	$(LINKER) $(LFLAGS) $(ARCH_FLAGS) $(OBJS) $(LIBPATH) $(LIBS) -o $@; \
	cp $(UTEXE_TARGET) $(OUTPUT_DIR);

ifdef UT_RUN
	cd $(OUTDIR); $(RUNTIME_LIBRARY_PATH); ./$(UTEXE_TARGET) $(CLOPTIONS)
endif
endif

clean:
	if [ -e $(OUTDIR) ];then rm -fr $(OUTDIR);fi
	if [ -e $(OUTDIR)/.deps ];then rm -fr $(OUTDIR)/.deps;fi
	@echo cleaned $(TARGET)
	rm -f $(CLEANFILES)
