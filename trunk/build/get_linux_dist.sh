#!/bin/bash

if [ "$(uname -s)" == 'FreeBSD' ]; then
	DIST='_freebsd_'
elif [ -f "/etc/redhat-release" ]; then
	VER=$(egrep -o 'Fedora|CentOS|Red.Hat|mileaccess' /etc/redhat-release)
	case $VER in
		mileaccess) DIST='_mlos_';;
		Fedora) DIST='_fedora_';;
		CentOS) DIST='_centos_';;
		Red.Hat) DIST='_redhat_';;
	esac
elif [ -f "/etc/debian_version" ]; then
	DIST='_debian_'
elif [ -f "/etc/arch-release" ]; then
	DIST='_arch_'
elif [ -f "/etc/SuSE-release" ]; then
	DIST='_suse_'
fi

echo "$DIST"

