@echo off
call ma_env_vars.bat
if "%MA_BASE%"=="" goto error1
if "%MA_TOOLS%"=="" goto error2

set RSA_MES=%MA_BASE%\..\RSA_BSAFE_MES-4.0.3\

start msvc\ma_installer.sln

goto end

:error1
echo Error: MA_BASE path not defined

:error2
echo Error: MA_TOOLS path not defined

:end
