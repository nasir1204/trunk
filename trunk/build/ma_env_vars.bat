@echo off
cd ..\
.\build\pwd.exe > .\build\cur_path.txt
cd .\build\
copy /Y root_cmd.txt + cur_path.txt set_ma_base.bat > nul
call set_ma_base.bat
if "%MA_BASE%"=="" goto error

set VC_VER=vc100
set MA_TOOLS=%MA_BASE%\..\
set MA_VSCORE_SDK=%MA_BASE%\..\VSCore SDK.15.3.0\sdk\
set MA_BOOST=%MA_BASE%\..\boost_windows
set MA_BOOST64=%MA_BASE%\..\boost_windows_x64
set MASIGN=%MA_TOOLS%\masign\masign.exe
set MASIGN_PREP_OUT=%MA_BASE%\build\packager\data
set PATH=%PATH%\
set RSA_MES=%MA_BASE%\..\RSA_BSAFE_MES-4.0.5
goto end

:error
echo *********************************
echo Error: Did not find base path set
echo .

:end
