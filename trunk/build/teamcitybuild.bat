@echo off

set target_=build
if /i "%1"=="rebuild" set target_=rebuild
if /i "%1"=="build" set target_=build

set MA_VERSION_MAJOR=5
set MA_VERSION_MINOR=0
set MA_VERSION_SUBMINOR=0
set MA_VERSION_BUILDNUMBER=999

set OLDDIR=%CD%

set checkoutpath=c:\teamcity\ma50\checkout
set logpath=c:\teamcity\ma50\

if not exist %logpath%\logs (	
mkdir %logpath%\logs
) 

rem to run UTs locally from this batch file update the below env variables are start using 
rem set checkoutpath=w:\code\1srcMA5.0\
rem set logpath=w:\code\test

del %logpath%\logs\build.log
del %logpath%\logs\unitest.log

call %checkoutpath%\build\build.bat %target_% release all intel test %logpath%\logs\build.log nolws
if not  "%errorlevel%"=="0" goto exit-build-error

call %checkoutpath%\build\build.bat %target_% release all intel beacon %logpath%\logs\build.log nolws
if not  "%errorlevel%"=="0" goto exit-build-error

cd %checkoutpath%\build\msvc\debug\

rem echo ******************************************** > %logpath%\logs\unitest.log
rem echo "       Running 32-bit (Debug) Unit Tests          " >> %logpath%\logs\unitest.log
rem echo ******************************************** >> %logpath%\logs\unitest.log

rem FOR /F "tokens=*" %%i IN (%checkoutpath%\build\utfile.txt) DO (
		rem echo Running UTs for %%i >> %logpath%\logs\unitest.log
rem 		%%i >> %logpath%\logs\unitest.log
		rem if not  "%errorlevel%"=="0" goto exit-error
rem )

rem echo ******************************************** >> %logpath%\logs\unitest.log
rem echo "       Running 64-bit (Debug) Unit Tests          " >> %logpath%\logs\unitest.log
rem echo ******************************************** >> %logpath%\logs\unitest.log

rem cd %checkoutpath%\build\msvc\Debug\x64

rem FOR /F "tokens=*" %%i IN (%checkoutpath%\build\utfile.txt) DO (
rem 		echo Running UTs for %%i >> %logpath%\logs\unitest.log
rem 		%%i >> %logpath%\logs\unitest.log
rem 		if not  "%errorlevel%"=="0" goto exit-error
rem )

cd %checkoutpath%\build\msvc\release\

echo ******************************************** >> %logpath%\logs\unitest.log
echo "       Running 32-bit (Release) Unit Tests          " >> %logpath%\logs\unitest.log
echo ******************************************** >> %logpath%\logs\unitest.log

FOR /F "tokens=*" %%i IN (%checkoutpath%\build\utfile.txt) DO (
		echo Running UTs for %%i >> %logpath%\logs\unitest.log
		%%i >> %logpath%\logs\unitest.log
		if not  "%errorlevel%"=="0" goto exit-error
		type %logpath%\logs\unitest.log
)

echo ******************************************** >> %logpath%\logs\unitest.log
echo "       Running 64-bit (Release) Unit Tests          " >> %logpath%\logs\unitest.log
echo ******************************************** >> %logpath%\logs\unitest.log

cd %checkoutpath%\build\msvc\release\x64

FOR /F "tokens=*" %%i IN (%checkoutpath%\build\utfile.txt) DO (
		echo Running UTs for %%i >> %logpath%\logs\unitest.log
		%%i >> %logpath%\logs\unitest.log
		if not  "%errorlevel%"=="0" goto exit-error
		type %logpath%\logs\unitest.log
)

goto exit

:exit-build-error
echo errorlevel := %errorlevel% >> %logpath%\logs\build.log
type %logpath%\logs\build.log
echo Build failed, Please look at %logpath%\logs\build.log for more details
cd %OLDDIR%
EXIT /B 1

:exit-error
echo errorlevel := %errorlevel% >> %logpath%\logs\unitest.log
type %logpath%\logs\unitest.log
echo Unit Tests failed, Please look at %logpath%\logs\unitest.log for more details
cd %OLDDIR%
EXIT /B 1

:exit
echo errorlevel := %errorlevel% >> %logpath%\logs\unitest.log
type %logpath%\logs\unitest.log
echo Unit Tests SUCCESS
cd %OLDDIR%
EXIT /B 0