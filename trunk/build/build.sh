#!/bin/bash

add_path()
{
	echo "Adding $1 to \$PATH"
	INDATA=$1
	data=`echo $PATH | grep "\(\(^$INDATA\)\|\(:$INDATA\)\)" | wc -l`
	if (( !$data )); then
	    if [ "$PATH" == "" ]; then 
		PATH=$1
	    else
		PATH=$1:$PATH
	    fi
	fi
}

printParams()
{
	echo "version=" $version
	echo "buildType=" $buildType
	echo "configType=" $configType
	echo "platform=" $platform
	echo "arch=" $arch
	echo "archBit=" $archBit
	echo "toolChainPath=" $toolChainPath
	echo "IsFortify=" $IsFortify
	echo "coverity=" $coverity
	echo "signing_dir=" $signing_dir
	echo "msgbus_auth=" $msgbus_auth
	echo "crypto=" $crypto
}

ValidateParams()
{
        # Build version bust be separated with .(dot)        
#	if [ $version != "" ]
#	then
#       		echo $version | grep -oq [.]
#		[ $? != 0 ] && echo "version doesn't contain . symbol" && exit 2
#	fi
         
	MAJVER=`echo $version | cut -d. -f1`
	MINVER=`echo $version | cut -d. -f2`
	SUBVER=`echo $version | cut -d. -f3`
	BUILDVER=`echo $version | cut -d. -f4`

	if [ "$MAJVER" == "" ] || [ "$MINVER" == "" ] || [ "$SUBVER" == "" ] || [ "$BUILDVER" == "" ] 
        then
		echo "Version Details are not correct"
        	Usage 
        fi

	if [ "$platform" != "Linux" ] && [ "$platform" != "Darwin" ] && [ "$platform" != "HP-UX" ] && [ "$platform" != "AIX" ] && [ "$platform" != "SunOS" ] && [ "$platform" != "FreeBSD" ] 
	then
    		echo "Error: " $platform "Platform is not supported"
    		exit 3
	fi

	if [ "$buildType" != "all" ] && [ "$buildType" != "compile" ] && [ "$buildType" != "test" ] && [ "$buildType" != "clean" ]
	then
    		Usage
	fi

	if [ "$configType" != "release" ] && [ "$configType" != "debug" ] && [ "$configType" != "all" ]
	then
    		Usage
	fi

	if [ "$arch" != "intel" ] && [ "$arch" != "ppc" ] && [ "$arch" != "sparc" ] && [ "$arch" != "itanium" ] && [ "$arch" != "pa_risc" ] && [ "$arch" != "aix" ] && [ "$arch" != "amd" ]
	then
    		Usage
	fi

	if [ "$archBit" != "all" ] && [ "$archBit" != "32" ] && [ "$archBit" != "64" ] 
	then
    		Usage
	fi

	if [ "$IsFortify" != "true" ] && [ "$IsFortify" != "false" ] 
	then
    		Usage
	fi

	if [ "$msgbus_auth" != "enable" ] && [ "$msgbus_auth" != "disable" ] 
	then
    		Usage
	fi

	if [ "$crypto" != "rsa" ] && [ "$crypto" != "openssl" ] 
	then
    		Usage
	fi
}

# Usage 
Usage()
{
    echo "Usage: bash <complete path>/build/build.sh -v <version> -t <build type> -c <config type> -p <platform> -a <arch> -b <arch-bit> -C <crypto type> -T <tool chain path> -f <fortify build> -s <coverity build> -d <signing-dir>"
    echo "   -v version i.e. 5.0.0.215 (Required)"
    echo "   -t build type i.e. all/compile/test/clean default is all (optional)"
    echo "   -c configuration type  i.e. release/debug/all default is release (optional)"
    echo "   -p unix platform type, output of 'uname -s' i.e. Linux/SunOS/HP-UX/AIX/Darwin/FreeBSD (Required)"
    echo "   -a Architecture type i.e. intel/ppc/sparc/itanium/pa-risc/aix/amd default is intel (optional)"
    echo "   -b bit type i.e. 32/64/all default is 32 (optional)"
    echo "   -T tool chain path default is taken from PATH env variable (optional)"
    echo "   -C crypto usage i.e. rsa/openssl default is rsa (optional)"
    echo "   -f fortify build i.e. true/false default is false (optional)"
    echo "   -m msgbus authentication i.e. enable/disable default is enable (optional)"
    echo "   -s Coverity build i.e. true/false default is false (optional)"
    echo "   -d Apple certificate signing directory <Absolute path> and skip this option for local builds"
    exit 1
}

changeReference()
{
	install_name_tool -id $2 $1
	echo "ma_tools path check: $1"
	echo "listing..."
	ls -lrt $1
	echo "otool..."
	otool -D $1
}
changeBinaryReferences_rsdk()
{
    
    if [ -f $1 ]; then
        echo "*****************testing ma tools references ($1) ******************"
        otool -L $1
        install_name_tool -change  @rpath/libma_client.5.0.dylib /Library/mileaccess/agent/lib/rsdk/libma_client.5.0.dylib $1
		install_name_tool -change  @rpath/libma_variant.5.0.dylib /Library/mileaccess/agent/lib/rsdk/libma_variant.5.0.dylib $1
		install_name_tool -change  @rpath/libma_msgbus.5.0.dylib /Library/mileaccess/agent/lib/rsdk/libma_msgbus.5.0.dylib $1
		install_name_tool -change  @rpath/libma_utils.5.0.dylib /Library/mileaccess/agent/lib/libma_utils.5.0.dylib  $1
		install_name_tool -change  @rpath/libma_logger.5.0.dylib /Library/mileaccess/agent/lib/rsdk/libma_logger.5.0.dylib $1
		install_name_tool -change  @rpath/libma_xml.5.0.dylib /Library/mileaccess/agent/lib/libma_xml.5.0.dylib $1
		install_name_tool -change  @rpath/libma_crypto.5.0.dylib /Library/mileaccess/agent/lib/libma_crypto.5.0.dylib $1
		install_name_tool -change  @rpath/libma_serialization.5.0.dylib /Library/mileaccess/agent/lib/libma_serialization.5.0.dylib $1
		install_name_tool -change  @rpath/libma_service_manager_client.5.0.dylib /Library/mileaccess/agent/lib/rsdk/libma_service_manager_client.5.0.dylib $1
        otool -L $1
    fi
}
change_rsdk_reference()
{
	MA_BINARY_PATH=$1
	for file in libma_client.5.0.dylib libma_logger.5.0.dylib libma_msgbus.5.0.dylib libma_service_manager_client.5.0.dylib libma_variant.5.0.dylib
	do
		lib=$MA_BINARY_PATH/$file
		if [ -f $lib ]; then
			changeBinaryReferences_rsdk "$lib"
		fi
	done
	install_name_tool -change  @rpath/libma_utils.5.0.dylib /Library/mileaccess/agent/lib/libma_utils.5.0.dylib $MA_BINARY_PATH/libma_crypto.5.0.dylib
	install_name_tool -change  @rpath/libma_utils.5.0.dylib /Library/mileaccess/agent/lib/libma_utils.5.0.dylib $MA_BINARY_PATH/libma_xml.5.0.dylib
}

changeBinaryReferences()
{
    
    if [ -f $1 ]; then
        echo "testing ma tools references ($1)"
        otool -L $1
        install_name_tool -change libz.1.dylib /Library/mileaccess/agent/lib/tools/libz.1.dylib $1
        install_name_tool -change libuv.0.10.dylib /Library/mileaccess/agent/lib/tools/libuv.0.10.dylib $1
        install_name_tool -change libtrex.1.3.dylib /Library/mileaccess/agent/lib/tools/libtrex.1.3.dylib $1
        install_name_tool -change libsqlite3.0.dylib /Library/mileaccess/agent/lib/tools/libsqlite3.0.dylib $1
        install_name_tool -change libmxml.dylib /Library/mileaccess/agent/lib/tools/libmxml.dylib $1
        install_name_tool -change libmsgpackc.2.dylib /Library/mileaccess/agent/lib/tools/libmsgpackc.2.dylib $1
        install_name_tool -change libmfecryptc.5.0.dylib /Library/mileaccess/agent/lib/tools/libmfecryptc.5.0.dylib $1
        install_name_tool -change libini.1.dylib /Library/mileaccess/agent/lib/tools/libini.1.dylib $1
        install_name_tool -change libcurl.4.dylib /Library/mileaccess/agent/lib/tools/libcurl.4.dylib $1
        install_name_tool -change libtntnet.12.dylib /Library/mileaccess/agent/lib/tools/libtntnet.12.dylib $1
        install_name_tool -change libtntnet_sdk.12.dylib /Library/mileaccess/agent/lib/tools/libtntnet_sdk.12.dylib $1
        install_name_tool -change libcxxtools-bin.9.dylib /Library/mileaccess/agent/lib/tools/libcxxtools-bin.9.dylib $1
        install_name_tool -change libcxxtools-http.9.dylib /Library/mileaccess/agent/lib/tools/libcxxtools-http.9.dylib $1
        install_name_tool -change libcxxtools-json.9.dylib /Library/mileaccess/agent/lib/tools/libcxxtools-json.9.dylib $1
        install_name_tool -change libcxxtools-unit.9.dylib /Library/mileaccess/agent/lib/tools/libcxxtools-unit.9.dylib $1
        install_name_tool -change libcxxtools-xmlrpc.9.dylib /Library/mileaccess/agent/lib/tools/libcxxtools-xmlrpc.9.dylib $1
        install_name_tool -change libcxxtools.9.dylib /Library/mileaccess/agent/lib/tools/libcxxtools.9.dylib $1
        otool -L $1
    fi
}

copy_Darwin_binaries()
{
	cp $MA_BASE/build/makefiles/$con/libma_client.5.0.dylib $MA_SDK_BASE/rsdk/$con/
	ln -s libma_client.5.0.dylib libma_client.5.dylib
	ln -s libma_client.5.dylib libma_client.dylib
	
	cp $MA_BASE/build/makefiles/$con/libma_logger.5.0.dylib $MA_SDK_BASE/rsdk/$con/
	ln -s libma_logger.5.0.dylib libma_logger.5.dylib
	ln -s libma_logger.5.dylib libma_logger.dylib
	
	cp $MA_BASE/build/makefiles/$con/libma_msgbus.5.0.dylib $MA_SDK_BASE/rsdk/$con/
	ln -s libma_msgbus.5.0.dylib libma_msgbus.5.dylib
	ln -s libma_msgbus.5.dylib libma_msgbus.dylib
	
	cp $MA_BASE/build/makefiles/$con/libma_variant.5.0.dylib $MA_SDK_BASE/rsdk/$con/
	ln -s libma_variant.5.0.dylib libma_variant.5.dylib
	ln -s libma_variant.5.dylib libma_variant.dylib
	
	cp $MA_BASE/build/makefiles/$con/libma_serialization.5.0.dylib $MA_SDK_BASE/rsdk/$con/
	ln -s libma_serialization.5.0.dylib libma_serialization.5.dylib
	ln -s libma_serialization.5.dylib libma_serialization.dylib
	
	cp $MA_BASE/build/makefiles/$con/libma_crypto.5.0.dylib $MA_SDK_BASE/rsdk/$con/
	ln -s libma_crypto.5.0.dylib libma_crypto.5.dylib
	ln -s libma_crypto.5.dylib libma_crypto.dylib
	
	
	cp $MA_BASE/build/makefiles/$con/libma_utils.5.0.dylib $MA_SDK_BASE/rsdk/$con/
	ln -s libma_utils.5.0.dylib libma_utils.5.dylib
	ln -s libma_utils.5.dylib libma_utils.dylib
	
	cp $MA_BASE/build/makefiles/$con/libma_xml.5.0.dylib $MA_SDK_BASE/rsdk/$con/
	ln -s libma_xml.5.0.dylib libma_xml.5.dylib
	ln -s libma_xml.5.dylib libma_xml.dylib
	
	cp $MFECRYPTC_PATH/lib/$con/libmfecryptc.5.0.dylib $MA_SDK_BASE/rsdk/$con/
	ln -s libmfecryptc.5.0.dylib libmfecryptc.5.dylib
	ln -s libmfecryptc.5.dylib libmfecryptc.dylib

	cp $MA_TOOLS/libuv/lib/$con/libuv.0.10.dylib $MA_SDK_BASE/rsdk/$con/
	ln -s libuv.0.10.dylib libuv.0.dylib
	ln -s libuv.0.dylib libuv.dylib
	
	cp $MA_TOOLS/trex/lib/$con/libtrex.1.3.dylib $MA_SDK_BASE/rsdk/$con/
	ln -s libtrex.1.3.dylib libtrex.1.dylib
	ln -s libtrex.1.dylib libtrex.dylib

        cp $MA_TOOLS/tntnet/lib/$con/libtntnet.12.dylib $MA_SDK_BASE/rsdk/$con/
        ln -s libtntnet.12.dylib libtntnet.dylib

        cp $MA_TOOLS/tntnet/lib/$con/libtntnet_sdk.12.dylib $MA_SDK_BASE/rsdk/$con/
        ln -s libtntnet_sdk.12.dylib libtntnet_sdk.dylib


        cp $MA_TOOLS/cxxtools/lib/$con/libcxxtools-bin.9.dylib $MA_SDK_BASE/rsdk/$con/
        ln -s libcxxtools-bin.9.dylib libcxxtools-bin.dylib

        cp $MA_TOOLS/cxxtools/lib/$con/libcxxtools-http.9.dylib $MA_SDK_BASE/rsdk/$con/
        ln -s libcxxtools-http.9.dylib libcxxtools-http.dylib

        cp $MA_TOOLS/cxxtools/lib/$con/libcxxtools-json.9.dylib $MA_SDK_BASE/rsdk/$con/
        ln -s libcxxtools-json.9.dylib libcxxtools-json.dylib

        cp $MA_TOOLS/cxxtools/lib/$con/libcxxtools-unit.9.dylib $MA_SDK_BASE/rsdk/$con/
        ln -s libcxxtools-unit.9.dylib libcxxtools-unit.dylib

        cp $MA_TOOLS/cxxtools/lib/$con/libcxxtools-xmlrpc.9.dylib $MA_SDK_BASE/rsdk/$con/
        ln -s libcxxtools-xmlrpc.9.dylib libcxxtools-xmlrpc.dylib

        cp $MA_TOOLS/cxxtools/lib/$con/libcxxtools.9.dylib $MA_SDK_BASE/rsdk/$con/
        ln -s libcxxtools.9.dylib libcxxtools.dylib
   

        cp $MA_TOOLS/cxxtools/lib/$con/libcxxtools-bin.la $MA_SDK_BASE/rsdk/$con/
        ln -s libcxxtools-bin.la libcxxtools-bin.a
	
        cp $MA_TOOLS/cxxtools/lib/$con/libcxxtools-http.la $MA_SDK_BASE/rsdk/$con/
        ln -s libcxxtools-http.la libcxxtools-http.a

        cp $MA_TOOLS/cxxtools/lib/$con/libcxxtools-json.la $MA_SDK_BASE/rsdk/$con/
        ln -s libcxxtools-json.la libcxxtools-json.a

        cp $MA_TOOLS/cxxtools/lib/$con/libcxxtools-unit.la $MA_SDK_BASE/rsdk/$con/
        ln -s libcxxtools-unit.la libcxxtools-unit.a

        cp $MA_TOOLS/cxxtools/lib/$con/libcxxtools-xmlrpc.la $MA_SDK_BASE/rsdk/$con/
        ln -s libcxxtools-xmlrpc.la libcxxtools-xmlrpc.a

        cp $MA_TOOLS/cxxtools/lib/$con/libcxxtools.la $MA_SDK_BASE/rsdk/$con/
        ln -s libcxxtools.la libcxxtools.a

        cp $MA_TOOLS/tntnet/lib/$con/libtntnet.la $MA_SDK_BASE/rsdk/$con/
        ln -s libtntnet.la libtntnet.a

        cp $MA_TOOLS/tntnet/lib/$con/libtntnet_sdk.la $MA_SDK_BASE/rsdk/$con/
        ln -s libtntnet_sdk.la libtntnet_sdk.a



	cp $MA_TOOLS/mxml/lib/$con/libmxml.1.dylib $MA_SDK_BASE/rsdk/$con/
	ln -s libmxml.1.dylib libmxml.dylib
	#ln -s libmxml.dylib.1 libmxml.dylib
	
	cp $MA_BASE/build/makefiles/$con/libma_service_manager_client.5.0.dylib $MA_SDK_BASE/rsdk/$con/
	ln -s libma_service_manager_client.5.0.dylib libma_service_manager_client.5.dylib
	ln -s libma_service_manager_client.5.dylib libma_service_manager_client.dylib
}

# Enable debugging option if SH_DEBUG=true
if [ "x$SH_DEBUG" == "xtrue" ]
then
	set -xv
fi


scriptFullPath=$0
firstchar=`echo $scriptFullPath | grep "^\/"`

if [ "$firstchar" == "" ]; then
    echo "The build script path is not absolute path"
    Usage
fi

if [ "x$MA_BASE" == "x" ]
then
MA_BASE=`echo $scriptFullPath | grep ".build.sh" | sed -e  's/.build.sh//g' | sed -e 's/.build//g'`
fi

echo "MA  base =" $MA_BASE

if [ "x$MA_TOOLS" == "x" ]
then
	MA_TOOLS=$MA_BASE/../ma_tools
fi

export MASIGN=$MA_TOOLS/masign/masign
export MASIGN_PREP_OUT=$MA_BASE/build/packager/data


if [ -f $MA_BASE/build/packager/data/ma_msgbus_auth.xml ]; then
	rm  $MA_BASE/build/packager/data/ma_msgbus_auth.xml
fi

if [ -f $MA_BASE/build/packager/data/ma_msgbus_auth.sig ]; then
	rm  $MA_BASE/build/packager/data/ma_msgbus_auth.sig
fi

if [ -f $MA_BASE/tools/beacon/build/packager/data/ma_msgbus_auth.xml ]; then
	rm  $MA_BASE/tools/beacon/build/packager/data/ma_msgbus_auth.xml
fi
if [ -f $MA_BASE/tools/beacon/build/packager/data/ma_msgbus_auth.sig ]; then
	rm  $MA_BASE/tools/beacon/build/packager/data/ma_msgbus_auth.sig
fi

# Parsing build script arguments 
while getopts v:t:c:p:a:b:T:C:f:m:s:d: opt
  do
  case $opt in
      v)
	  version=$OPTARG;;
      t)
	  buildType=$OPTARG;;
      c)
	  configType=$OPTARG;;
      p)
          platform=$OPTARG;;
      a)
          arch=$OPTARG;;
      b)  
	  archBit=$OPTARG;;
      T)  
	  toolChainPath=$OPTARG;;
      C)  
	  crypto=$OPTARG;;
      f) 
          IsFortify=$OPTARG;;
      m) 
          msgbus_auth=$OPTARG;;
       s)
		  coverity=$OPTARG;;
	  d)
		  signing_dir=$OPTARG;;
      h)
	  Usage;;
      \?) 
          Usage;;
  esac
done
shift `expr $OPTIND - 1` 

# Assigning default values for optional parameters if not specified
[ -z $version ] && Usage
[ -z $buildType ] && buildType=all
[ -z $configType ] && configType=release
[ -z $platform ] && Usage 
[ -z $arch ] && arch=intel
[ -z $archBit ] && archBit=32
[ -z $IsFortify ] && IsFortify=false
[ -z $msgbus_auth ] && msgbus_auth=enable
[ -z $coverity ] && coverity=false
[ -z $signing_dir ] && signing_dir=""
[ -z $crypto ] && crypto="rsa"

# Printing all parameter values before validation
printParams

# Validating the path and parameters specified
ValidateParams

if [ "$crypto" == "openssl" ]
then
MFECRYPTC_PATH=$MA_TOOLS/mfecryptc_openssl
CURL_PATH=$MA_TOOLS/curl_openssl
else  # default rsa 
MFECRYPTC_PATH=$MA_TOOLS/mfecryptc
CURL_PATH=$MA_TOOLS/curl
fi

# Adding TOOLCHAIN Path to PATH
if [ ! -z $toolChainPath ]
then
add_path $toolChainPath
makeCmdArgs=" TOOLCHAIN=$toolChainPath "
fi

if [ "$platform" == "Linux" ] && [ "$archBit" == "64" ] && [ -f "/etc/redhat-release" ]
then
DIST=`cat /etc/redhat-release | cut -d" " -f1`
makeCmdArgs=" DIST=$DIST "$makeCmdArgs
fi

# cheicking build environment
echo ""
echo "Build Environment is..."
echo ""
echo `uname -a`
echo Using make `which make`
echo Using gcc  `which gcc`
echo Using g++  `which g++`
echo Using ld   `which ld`
echo Using nm   `which nm`
echo Disp space   `df -h`
echo ""

echo "Displaying gcc version:"
echo `gcc -v`
echo `uname -a`
if [ "$platform" == "Darwin" ]
then
	if [ -f /Developer/usr/bin/xcodebuild ]; then
		echo `/Developer/usr/bin/xcodebuild  -version`
	else
		echo `/usr/bin/xcodebuild  -version`
	fi
fi
echo ""

# Setting Fortify specific variables
if [ "$IsFortify" == "true" ]; then
   makeCmdArgs=" FORTIFY=1 "$makeCmdArgs
   if [ -z "$FORTIFY_BASE" ]; then
       export FORTIFY_BASE=/home/bserver/Code/fortify
   fi
   echo "Using FORTIFY_BASE=$FORTIFY_BASE"
   add_path $FORTIFY_BASE
fi
   
# setting target setting for make

# Adding configuration, Platform, arch, version, type

makeCmdArgs=" MAJ_VER=$MAJVER MIN_VER=$MINVER SUB_VER=$SUBVER BUILD_VER=$BUILDVER "$makeCmdArgs
makeCmdArgs=" OS_NAME=$platform ARCH=$arch CRYPTO=$crypto"$makeCmdArgs

if [ "$configType" == "all" ]
then
configuration="release debug"
else
configuration=$configType
fi

if [ "$archBit" == "all" ]
then
bit_type="32 64"
else
bit_type=$archBit
fi

if [ "$msgbus_auth" == "disable" ]
then
echo "building msgbus with no auth"
export MSGBUS_AUTH_FLAG=$msgbus_auth
fi

# Add components for build
############# TO DO ----  components will be retrived from manifest file based on built type and platform #################
components=$buildType

makeCmd="make "$components

# saving current directory and changin directory to $MA_BASE/build.makefiles to execute make command
curdir=`pwd`
cd $MA_BASE/build/makefiles

echo "Changing directory to " $MA_BASE/build/makefiles

########################################################################
##### Coverity check and run skip the rest if coverity is enabled#######
########################################################################
if [ "$coverity" == "true" ]; then
    if [ -z "$COVERITY_BASE" ]; then 
        export COVERITY_BASE=/home/bserver/Code/prevent
    fi
    echo "Using COVERITY_BASE=$COVERITY_BASE"
	Cmd=$makeCmd" CONFIG=release ARCH_BIT=32 CRYPTO=$crypto"$makeCmdArgs
    # Put Coverity directory in the root of the code
    export COVDIR=../Coverity
    if [ -d "$COVDIR" ]; then
        /bin/rm -rf "$COVDIR"
    fi
    
    echo "__1__ Running cov-build (Coverity)"
	echo  ${COVERITY_BASE}/bin/cov-build -c ${COVERITY_BASE}/config/coverity_config.xml --dir ${COVDIR} $Cmd
    ${COVERITY_BASE}/bin/cov-build -c ${COVERITY_BASE}/config/coverity_config.xml --dir ${COVDIR} $Cmd
    echo "__2__ Done with cov-build (Coverity)"

	echo "Running Coverity analysis on the emit folder ..."
	${COVERITY_BASE}/bin/cov-analyze --all --dir ${COVDIR} -c ${COVERITY_BASE}/config/coverity_config.xml --checker-option STACK_USE:max_single_base_use_bytes:16384

	#commit taken care by ECM
	#echo "Committing Coverity results to the database ..."
	#${COVERITY_BASE}/bin/cov-commit-defects --product MSA --version $version --description MSA-$unixtype-$version-$package --user admin --dir ${COVDIR} --remote beaappcov1 --port 8080

	if (( $? != 0 )); then
		echo "Build Failed. Exiting..."
		cd $dir
		exit 1
	fi
	exit 0
fi
########################################################################

#fix for rpath issue in mac
if [ "$platform" == "Darwin" ]; then
for con in $configuration
do
	for bit in $bit_type
	do
		if [ "$bit" == "32" ]
               	then
			changeReference $MA_TOOLS/zlib/lib/$con/libz.1.dylib libz.1.dylib
			changeReference $MA_TOOLS/libuv/lib/$con/libuv.0.10.dylib libuv.0.10.dylib
			changeReference $MA_TOOLS/trex/lib/$con/libtrex.1.3.dylib libtrex.1.3.dylib
			changeReference $MA_TOOLS/sqlite/lib/$con/libsqlite3.0.dylib libsqlite3.0.dylib
			changeReference $MA_TOOLS/mxml/lib/$con/libmxml.dylib libmxml.dylib
			changeReference $MA_TOOLS/msgpack/lib/$con/libmsgpackc.2.dylib libmsgpackc.2.dylib
			changeReference $MFECRYPTC_PATH/lib/$con/libmfecryptc.5.0.dylib libmfecryptc.5.0.dylib
			changeReference $MA_TOOLS/libini/lib/$con/libini.1.dylib libini.1.dylib
			changeReference $CURL_PATH/lib/$con/libcurl.4.dylib libcurl.4.dylib
			changeReference $MA_TOOLS/openssl/lib/$con/libcrypto.1.0.0.dylib libcrypto.1.0.0.dylib
			changeReference $MA_TOOLS/openssl/lib/$con/libssl.1.0.0.dylib libssl.1.0.0.dylib
        		changeReference $MA_TOOLS/cxxtools/lib/$con/libcxxtools-bin.9.dylib libcxxtools-bin.9.dylib
        		changeReference $MA_TOOLS/cxxtools/lib/$con/libcxxtools-http.9.dylib libcxxtools-http.9.dylib
        		changeReference $MA_TOOLS/cxxtools/lib/$con/libcxxtools-json.9.dylib libcxxtools-json.9.dylib
        		changeReference $MA_TOOLS/cxxtools/lib/$con/libcxxtools-unit.9.dylib libcxxtools-unit.9.dylib
        		changeReference $MA_TOOLS/cxxtools/lib/$con/libcxxtools-xmlrpc.9.dylib libcxxtools-xmlrpc.9.dylib
        		changeReference $MA_TOOLS/cxxtools/lib/$con/libcxxtools.9.dylib libcxxtools.9.dylib
        		changeReference $MA_TOOLS/tntnet/lib/$con/libtntnet.12.dylib libtntnet.12.dylib
        		changeReference $MA_TOOLS/tntnet/lib/$con/libtntnet_sdk.12.dylib libtntnet_sdk.12.dylib
		else
			changeReference $MA_TOOLS/zlib/lib/$con/$bit/libz.1.dylib libz.1.dylib
			changeReference $MA_TOOLS/libuv/lib/$con/$bit/libuv.0.10.dylib libuv.0.10.dylib
			changeReference $MA_TOOLS/trex/lib/$con/$bit/libtrex.1.3.dylib libtrex.1.3.dylib
			changeReference $MA_TOOLS/sqlite/lib/$con/$bit/libsqlite3.0.dylib libsqlite3.0.dylib
			changeReference $MA_TOOLS/mxml/lib/$con/$bit/libmxml.dylib libmxml.dylib
			changeReference $MA_TOOLS/msgpack/lib/$con/$bit/libmsgpackc.2.dylib libmsgpackc.2.dylib
			changeReference $MFECRYPTC_PATH/lib/$con/$bit/libmfecryptc.5.0.dylib libmfecryptc.5.0.dylib
			changeReference $MA_TOOLS/libini/lib/$con/$bit/libini.1.dylib libini.1.dylib
			changeReference $CURL_PATH/lib/$con/$bit/libcurl.4.dylib libcurl.4.dylib
			changeReference $MA_TOOLS/openssl/lib/$con/$bit/libcrypto.1.0.0.dylib libcrypto.1.0.0.dylib
			changeReference $MA_TOOLS/openssl/lib/$con/$bit/libssl.1.0.0.dylib libssl.1.0.0.dylib
        		changeReference $MA_TOOLS/cxxtools/lib/$con/$bit/libcxxtools-bin.9.dylib libcxxtools-bin.9.dylib
        		changeReference $MA_TOOLS/cxxtools/lib/$con/$bit/libcxxtools-http.9.dylib libcxxtools-http.9.dylib
        		changeReference $MA_TOOLS/cxxtools/lib/$con/$bit/libcxxtools-json.9.dylib libcxxtools-json.9.dylib
        		changeReference $MA_TOOLS/cxxtools/lib/$con/$bit/libcxxtools-unit.9.dylib libcxxtools-unit.9.dylib
        		changeReference $MA_TOOLS/cxxtools/lib/$con/$bit/libcxxtools-xmlrpc.9.dylib libcxxtools-xmlrpc.9.dylib
        		changeReference $MA_TOOLS/cxxtools/lib/$con/$bit/libcxxtools.9.dylib libcxxtools.9.dylib
        		changeReference $MA_TOOLS/tntnet/lib/$con/$bit/libtntnet.12.dylib libtntnet.12.dylib
        		changeReference $MA_TOOLS/tntnet/lib/$con/$bit/libtntnet_sdk.12.dylib libtntnet_sdk.12.dylib
		fi
	done
done
fi
if [ -z "$signing_dir" ]; then 
echo "Siging directory not specified"
else
if [ "$platform" == "Darwin" ]; then
#	signing_dir=/Volumes/CODE_SIGN
	echo "signing_dir=" $signing_dir


	if [ ! -d $signing_dir ]; then
		mkdir -p $signing_dir
		if [ $? != 0 ]; then
        echo "Invalid signing directory $signing_dir"
        exit 1
		fi     
	fi
	
	mkdir -p $signing_dir/app
	mkdir -p $signing_dir/sign_in
	mkdir -p $signing_dir/sign_out
fi
fi



for con in $configuration
do
for bit in $bit_type
do
echo "********** " $bit bit $con "Build Starting **********"
Cmd=$makeCmd" CONFIG=$con ARCH_BIT=$bit CRYPTO=$crypto"$makeCmdArgs
echo $Cmd
$Cmd
	if [ $? -ne 0 ] 
	then
	echo "********** " $bit bit $con " Build Failed.... " 
	cd $curdir
	exit 1
	else  
	echo "********** " $bit bit $con "Build Ending **********" 
	fi 

    export BINARY_DIR=$MA_BASE/build/makefiles/$con/

    if [ $bit == "64" ]; then
        export BINARY_DIR=$MA_BASE/build/makefiles/$con/$bit/
    fi

	if [ "$platform" == "Darwin" ]; then
		changeBinaryReferences $BINARY_DIR/masvc
		changeBinaryReferences $BINARY_DIR/macmnsvc
		#changeBinaryReferences $BINARY_DIR/macompatsvc
		changeBinaryReferences $BINARY_DIR/maconfig
		changeBinaryReferences $BINARY_DIR/cmdagent		
		#changeBinaryReferences $BINARY_DIR/policy_upgrade		

		change_rsdk_reference $BINARY_DIR
		
	if [ -z "$signing_dir" ]; then 
		echo "Siging directory not specified"
	else	

		echo "Binaries in "$BINARY_DIR" "
		ls -lrt $BINARY_DIR

	
		rm -f $signing_dir/sign_in/*
		rm -f $signing_dir/sign_out/*
		
		
		bash $MA_BASE/build/packager/unix/dmg/binary_sign.sh $BINARY_DIR $signing_dir "bin"

		if [ $? != 0 ]; then
			echo "Binaries signing failed"
			exit 1;
		fi

		echo "Binaries in "$signing_dir"/sign_out"
		ls -lrt $signing_dir/sign_out
		
		cp -f $signing_dir/sign_out/* $BINARY_DIR
		rm -f $BINARY_DIR/sign_*
	
		
########### Signing main bootstrap app ##########
##### Then create the app directory for app signing.

		if [ ! -d $signing_dir/app ]; then
		   mkdir -p $signing_dir/app
		   if [ $? != 0 ]; then
			echo "Invalid out directory $signing_dir/app"
			exit 1
		   fi
		fi

		#sign internal app.
		rm -rf $signing_dir/app/*
		cp -rf $BINARY_DIR/mileaccessSmartInstall.app/Contents/Resources/mileaccessSmartInstall.app $signing_dir/app 
		bash $MA_BASE/build/packager/unix/dmg/binary_sign.sh $signing_dir/app $signing_dir "app"

		if [ $? != 0 ]; then
			echo "Binaries app signing failed"
			exit 1;
		fi

		cp -rf $signing_dir/sign_out/mileaccessSmartInstall.app $BINARY_DIR/mileaccessSmartInstall.app/Contents/Resources/

		#sign wrapper app.
		rm -rf $signing_dir/app/*
		cp -rf $BINARY_DIR/mileaccessSmartInstall.app $signing_dir/app 
		bash $MA_BASE/build/packager/unix/dmg/binary_sign.sh $signing_dir/app $signing_dir "app"

		if [ $? != 0 ]; then
			echo "Binaries app signing failed"
			exit 1;
		fi

		cp -rf $signing_dir/sign_out/mileaccessSmartInstall.app $BINARY_DIR/
		rm -rf $signing_dir/app

		#/usr/bin/ditto -c -k --sequesterRsrc --keepParent  $BINARY_DIR/mileaccessSmartInstall.app  $BINARY_DIR/mileaccessSmartInstall.app.zip
		#cp $BINARY_DIR/mileaccessSmartInstall.app.zip $BINARY_DIR/bootstrap
	 fi
	fi
	
	
	#******************IF NEEDED APPLE BINARY SIGNING PUT IT ABOVE THAT 
	$MA_TOOLS/masign/masign -prep -dir $BINARY_DIR -name masvc -out $MA_BASE/build/packager/data/
	$MA_TOOLS/masign/masign -prep -dir $BINARY_DIR -name macmnsvc -out $MA_BASE/build/packager/data/
	#$MA_TOOLS/masign/masign -prep -dir $BINARY_DIR -name macompatsvc -out $MA_BASE/build/packager/data/
	$MA_TOOLS/masign/masign -prep -dir $BINARY_DIR -name maconfig -out $MA_BASE/build/packager/data/
	$MA_TOOLS/masign/masign -prep -dir $BINARY_DIR -name cmdagent -out $MA_BASE/build/packager/data/
	#$MA_TOOLS/masign/masign -prep -dir $BINARY_DIR -name Mue -aliases Mue_InUse -out $MA_BASE/build/packager/data/
done
done



if [ ! -f $MA_BASE/build/packager/data/ma_msgbus_auth.xml ]
then
	echo WARNING !!!!  ma_msgbus_auth.xml is not found
else
	echo ma_msgbus_auth.xml is present at the expected location for signing
	cat $MA_BASE/build/packager/data/ma_msgbus_auth.xml 
fi


if [ "$buildType" != "clean" ]
then

# Create the ma_sdk base folder and the folder structure
echo "Creating ma sdk folder and fillig content"
MA_SDK_BASE=$MA_BASE/tools/ma_sdk

# remove sdk folde if already exists
if [ -e $MA_SDK_BASE ]; then
	rm -fr $MA_SDK_BASE
fi

echo Creating MA SDK dircetory
mkdir -p $MA_SDK_BASE
mkdir -p $MA_SDK_BASE/samples
mkdir -p $MA_SDK_BASE/rsdk
mkdir -p $MA_SDK_BASE/bin

# copy include folder
cp -r  $MA_BASE/include $MA_SDK_BASE/
cp -r  $MA_BASE/bindings $MA_SDK_BASE/
cp -r  $MA_TOOLS/masign $MA_SDK_BASE/
cp     $MA_BASE/build/packager/data/certs/ma_test_certs.tar $MA_SDK_BASE/masign/

# trim the unwanted folders
rm -fr $MA_SDK_BASE/include/ma/datastore
rm -fr $MA_SDK_BASE/include/ma/internal
rm -fr $MA_SDK_BASE/include/ma/sensor
rm -fr $MA_SDK_BASE/include/ma/ma_sdk.h

# overwriting build type for beacon build as beacon build script doesn't have the test and all options
if [ "$buildType" == "all" ] || [ "$buildType" == "compile" ] || [ "$buildType" == "test" ]
then
buildType=compile
fi

cwd=`pwd`

# copy binaries (only ma_clientlib)
for con in $configuration
do
for bit in $bit_type
do
    LIB_EXT="so"
	
	if [ "$platform" == "HP-UX" ]
	then
		LIB_EXT="sl"
	fi
	
	if [ $bit == "32" ]
	then
       	mkdir -p $MA_SDK_BASE/lib/$con
       		mkdir -p $MA_SDK_BASE/bin/$con
		mkdir -p $MA_SDK_BASE/rsdk/$con
		cp $MA_BASE/build/makefiles/${con}_static/libma.a $MA_SDK_BASE/lib/$con/
		
		cd $MA_SDK_BASE/rsdk/$con/
		if [ "$platform" == "Darwin" ]
		then
			copy_Darwin_binaries
		else	
			cp $MA_BASE/build/makefiles/$con/libma_client.$LIB_EXT.5.0 $MA_SDK_BASE/rsdk/$con/
			ln -s libma_client.$LIB_EXT.5.0 libma_client.$LIB_EXT.5
			ln -s libma_client.$LIB_EXT.5 libma_client.$LIB_EXT
			
			cp $MA_BASE/build/makefiles/$con/libma_logger.$LIB_EXT.5.0 $MA_SDK_BASE/rsdk/$con/
			ln -s libma_logger.$LIB_EXT.5.0 libma_logger.$LIB_EXT.5
			ln -s libma_logger.$LIB_EXT.5 libma_logger.$LIB_EXT
			
			cp $MA_BASE/build/makefiles/$con/libma_msgbus.$LIB_EXT.5.0 $MA_SDK_BASE/rsdk/$con/
			ln -s libma_msgbus.$LIB_EXT.5.0 libma_msgbus.$LIB_EXT.5
			ln -s libma_msgbus.$LIB_EXT.5 libma_msgbus.$LIB_EXT
			
			cp $MA_BASE/build/makefiles/$con/libma_variant.$LIB_EXT.5.0 $MA_SDK_BASE/rsdk/$con/
			ln -s libma_variant.$LIB_EXT.5.0 libma_variant.$LIB_EXT.5
			ln -s libma_variant.$LIB_EXT.5 libma_variant.$LIB_EXT
			
			cp $MA_BASE/build/makefiles/$con/libma_serialization.$LIB_EXT.5.0 $MA_SDK_BASE/rsdk/$con/
			ln -s libma_serialization.$LIB_EXT.5.0 libma_serialization.$LIB_EXT.5
			ln -s libma_serialization.$LIB_EXT.5 libma_serialization.$LIB_EXT
			
			cp $MA_BASE/build/makefiles/$con/libma_crypto.$LIB_EXT.5.0 $MA_SDK_BASE/rsdk/$con/
			ln -s libma_crypto.$LIB_EXT.5.0 libma_crypto.$LIB_EXT.5
			ln -s libma_crypto.$LIB_EXT.5 libma_crypto.$LIB_EXT
			
			
			cp $MA_BASE/build/makefiles/$con/libma_utils.$LIB_EXT.5.0 $MA_SDK_BASE/rsdk/$con/
			ln -s libma_utils.$LIB_EXT.5.0 libma_utils.$LIB_EXT.5
			ln -s libma_utils.$LIB_EXT.5 libma_utils.$LIB_EXT
			
			cp $MA_BASE/build/makefiles/$con/libma_xml.$LIB_EXT.5.0 $MA_SDK_BASE/rsdk/$con/
			ln -s libma_xml.$LIB_EXT.5.0 libma_xml.$LIB_EXT.5
			ln -s libma_xml.$LIB_EXT.5 libma_xml.$LIB_EXT
			
			cp $MFECRYPTC_PATH/lib/$con/libmfecryptc.$LIB_EXT.5.0 $MA_SDK_BASE/rsdk/$con/
			ln -s libmfecryptc.$LIB_EXT.5.0 libmfecryptc.$LIB_EXT.5
			ln -s libmfecryptc.$LIB_EXT.5 libmfecryptc.$LIB_EXT

			cp $MA_TOOLS/libuv/lib/$con/libuv.$LIB_EXT.0.10 $MA_SDK_BASE/rsdk/$con/
			ln -s libuv.$LIB_EXT.0.10 libuv.$LIB_EXT.0
			ln -s libuv.$LIB_EXT.0 libuv.$LIB_EXT
			
			cp $MA_TOOLS/trex/lib/$con/libtrex.$LIB_EXT.1.3 $MA_SDK_BASE/rsdk/$con/
			ln -s libtrex.$LIB_EXT.1.3 libtrex.$LIB_EXT.1
			ln -s libtrex.$LIB_EXT.1 libtrex.$LIB_EXT

	cp $MA_TOOLS/tntnet/lib/$con/libtntnet.$LIB_EXT.12 $MA_SDK_BASE/rsdk/$con/
        ln -s libtntnet.$LIB_EXT.12 libtntnet.$LIB_EXT

        cp $MA_TOOLS/tntnet/lib/$con/libtntnet_sdk.$LIB_EXT.12 $MA_SDK_BASE/rsdk/$con/
        ln -s libtntnet_sdk.$LIB_EXT.12 libtntnet_sdk.$LIB_EXT

        cp $MA_TOOLS/cxxtools/lib/$con/libcxxtools-bin.$LIB_EXT.9 $MA_SDK_BASE/rsdk/$con/
        ln -s libcxxtools-bin.$LIB_EXT.9 libcxxtools-bin.$LIB_EXT

        cp $MA_TOOLS/cxxtools/lib/$con/libcxxtools-http.$LIB_EXT.9 $MA_SDK_BASE/rsdk/$con/
        ln -s libcxxtools-http.$LIB_EXT.9 libcxxtools-http.$LIB_EXT

        cp $MA_TOOLS/cxxtools/lib/$con/libcxxtools-json.$LIB_EXT.9 $MA_SDK_BASE/rsdk/$con/
        ln -s libcxxtools-json.$LIB_EXT.9 libcxxtools-json.$LIB_EXT

        cp $MA_TOOLS/cxxtools/lib/$con/libcxxtools-unit.$LIB_EXT.9 $MA_SDK_BASE/rsdk/$con/
        ln -s libcxxtools-unit.$LIB_EXT.9 libcxxtools-unit.$LIB_EXT

        cp $MA_TOOLS/cxxtools/lib/$con/libcxxtools-xmlrpc.$LIB_EXT.9 $MA_SDK_BASE/rsdk/$con/
        ln -s libcxxtools-xmlrpc.$LIB_EXT.9 libcxxtools-xmlrpc.$LIB_EXT

        cp $MA_TOOLS/cxxtools/lib/$con/libcxxtools.$LIB_EXT.9 $MA_SDK_BASE/rsdk/$con/
        ln -s libcxxtools.9 libcxxtools.$LIB_EXT

        cp $MA_TOOLS/cxxtools/lib/$con/libcxxtools-bin.la $MA_SDK_BASE/rsdk/$con/
        ln -s libcxxtools-bin.la libcxxtools-bin.a
	
        cp $MA_TOOLS/cxxtools/lib/$con/libcxxtools-http.la $MA_SDK_BASE/rsdk/$con/
        ln -s libcxxtools-http.la libcxxtools-http.a

        cp $MA_TOOLS/cxxtools/lib/$con/libcxxtools-json.la $MA_SDK_BASE/rsdk/$con/
        ln -s libcxxtools-json.la libcxxtools-json.a

        cp $MA_TOOLS/cxxtools/lib/$con/libcxxtools-unit.la $MA_SDK_BASE/rsdk/$con/
        ln -s libcxxtools-unit.la libcxxtools-unit.a

        cp $MA_TOOLS/cxxtools/lib/$con/libcxxtools-xmlrpc.la $MA_SDK_BASE/rsdk/$con/
        ln -s libcxxtools-xmlrpc.la libcxxtools-xmlrpc.a

        cp $MA_TOOLS/cxxtools/lib/$con/libcxxtools.la $MA_SDK_BASE/rsdk/$con/
        ln -s libcxxtools.la libcxxtools.a

        cp $MA_TOOLS/tntnet/lib/$con/libtntnet.la $MA_SDK_BASE/rsdk/$con/
        ln -s libtntnet.la libtntnet.a

        cp $MA_TOOLS/tntnet/lib/$con/libtntnet_sdk.la $MA_SDK_BASE/rsdk/$con/
        ln -s libtntnet_sdk.la libtntnet_sdk.a


			if [ x"$PLATFORM" = x"AIX" ] ;then
				cp $MA_TOOLS/mxml/lib/$con/libmxml.a $MA_SDK_BASE/rsdk/$con/
			elif [ x"$PLATFORM" = x"Linux" ] ;then
				cp $MA_TOOLS/mxml/lib/$con/libmxml.$LIB_EXT.1.5 $MA_SDK_BASE/rsdk/$con/
				ln -s libmxml.$LIB_EXT.1.5 libmxml.$LIB_EXT.1
				ln -s libmxml.$LIB_EXT.1 libmxml.$LIB_EXT
			fi
			
			cp $MA_BASE/build/makefiles/$con/libma_service_manager_client.$LIB_EXT.5.0 $MA_SDK_BASE/rsdk/$con/
			ln -s libma_service_manager_client.$LIB_EXT.5.0 libma_service_manager_client.$LIB_EXT.5
			ln -s libma_service_manager_client.$LIB_EXT.5 libma_service_manager_client.$LIB_EXT
   	        fi			
		$MA_BASE/tools/beacon/build/build.sh -v $version -c $con -t $buildType -p $platform -a $arch -b $bit #-T $toolChainPath
	fi

	if [ $bit == "64" ]
	then
       	mkdir -p $MA_SDK_BASE/lib/$con/$bit
		mkdir -p $MA_SDK_BASE/rsdk/$con/$bit
		cp $MA_BASE/build/makefiles/${con}_static/$bit/libma.a $MA_SDK_BASE/lib/$con/$bit/
		
		cd $MA_SDK_BASE/rsdk/$con/$bit
	       	if [ "$platform" == "Darwin" ]
		then
			copy_Darwin_binaries
		else	
			cp $MA_BASE/build/makefiles/$con/$bit/libma_client.$LIB_EXT.5.0 $MA_SDK_BASE/rsdk/$con/$bit/
			ln -s libma_client.$LIB_EXT.5.0 libma_client.$LIB_EXT.5
			ln -s libma_client.$LIB_EXT.5 libma_client.$LIB_EXT
			
			cp $MA_BASE/build/makefiles/$con/$bit/libma_logger.$LIB_EXT.5.0 $MA_SDK_BASE/rsdk/$con/$bit/
			ln -s libma_logger.$LIB_EXT.5.0 libma_logger.$LIB_EXT.5
			ln -s libma_logger.$LIB_EXT.5 libma_logger.$LIB_EXT
			
			cp $MA_BASE/build/makefiles/$con/$bit/libma_msgbus.$LIB_EXT.5.0 $MA_SDK_BASE/rsdk/$con/$bit/
			ln -s libma_msgbus.$LIB_EXT.5.0 libma_msgbus.$LIB_EXT.5
			ln -s libma_msgbus.$LIB_EXT.5 libma_msgbus.$LIB_EXT
			
			cp $MA_BASE/build/makefiles/$con/$bit/libma_variant.$LIB_EXT.5.0 $MA_SDK_BASE/rsdk/$con/$bit/
			ln -s libma_variant.$LIB_EXT.5.0 libma_variant.$LIB_EXT.5
			ln -s libma_variant.$LIB_EXT.5 libma_variant.$LIB_EXT
			
			cp $MA_BASE/build/makefiles/$con/$bit/libma_serialization.$LIB_EXT.5.0 $MA_SDK_BASE/rsdk/$con/$bit/
			ln -s libma_serialization.$LIB_EXT.5.0 libma_serialization.$LIB_EXT.5
			ln -s libma_serialization.$LIB_EXT.5 libma_serialization.$LIB_EXT
			
			cp $MA_BASE/build/makefiles/$con/$bit/libma_crypto.$LIB_EXT.5.0 $MA_SDK_BASE/rsdk/$con/$bit/
			ln -s libma_crypto.$LIB_EXT.5.0 libma_crypto.$LIB_EXT.5
			ln -s libma_crypto.$LIB_EXT.5 libma_crypto.$LIB_EXT
			
			cp $MA_BASE/build/makefiles/$con/$bit/libma_utils.$LIB_EXT.5.0 $MA_SDK_BASE/rsdk/$con/$bit/
			ln -s libma_utils.$LIB_EXT.5.0 libma_utils.$LIB_EXT.5
			ln -s libma_utils.$LIB_EXT.5 libma_utils.$LIB_EXT
			
			cp $MA_BASE/build/makefiles/$con/$bit/libma_xml.$LIB_EXT.5.0 $MA_SDK_BASE/rsdk/$con/$bit/
			ln -s libma_xml.$LIB_EXT.5.0 libma_xml.$LIB_EXT.5
			ln -s libma_xml.$LIB_EXT.5 libma_xml.$LIB_EXT
			
			cp $MFECRYPTC_PATH/lib/$con/$bit/libmfecryptc.$LIB_EXT.5.0 $MA_SDK_BASE/rsdk/$con/$bit/
			ln -s libmfecryptc.$LIB_EXT.5.0 libmfecryptc.$LIB_EXT.5
			ln -s libmfecryptc.$LIB_EXT.5 libmfecryptc.$LIB_EXT

			cp $MA_TOOLS/libuv/lib/$con/$bit/libuv.$LIB_EXT.0.10 $MA_SDK_BASE/rsdk/$con/$bit/
			ln -s libuv.$LIB_EXT.0.10 libuv.$LIB_EXT.0
			ln -s libuv.$LIB_EXT.0 libuv.$LIB_EXT
			
			cp $MA_TOOLS/trex/lib/$con/$bit/libtrex.$LIB_EXT.1.3 $MA_SDK_BASE/rsdk/$con/$bit/
			ln -s libtrex.$LIB_EXT.1.3 libtrex.$LIB_EXT.1
			ln -s libtrex.$LIB_EXT.1 libtrex.$LIB_EXT
			
			cp $MA_TOOLS/mxml/lib/$con/$bit/libmxml.$LIB_EXT.1.5 $MA_SDK_BASE/rsdk/$con/$bit/
			ln -s libmxml.$LIB_EXT.1.5 libmxml.$LIB_EXT.1
			ln -s libmxml.$LIB_EXT.1 libmxml.$LIB_EXT
			
	cp $MA_TOOLS/tntnet/lib/$con/$bit/libtntnet.$LIB_EXT.12 $MA_SDK_BASE/rsdk/$con/$bit/
        ln -s libtntnet.$LIB_EXT.12 libtntnet.$LIB_EXT

        cp $MA_TOOLS/tntnet/lib/$con/$bit/libtntnet_sdk.$LIB_EXT.12 $MA_SDK_BASE/rsdk/$con/$bit/
        ln -s libtntnet_sdk.$LIB_EXT.12 libtntnet_sdk.$LIB_EXT

        cp $MA_TOOLS/cxxtools/lib/$con/$bit/libcxxtools-bin.$LIB_EXT.9 $MA_SDK_BASE/rsdk/$con/$bit/
        ln -s libcxxtools-bin.$LIB_EXT.9 libcxxtools-bin.$LIB_EXT

        cp $MA_TOOLS/cxxtools/lib/$con/$bit/libcxxtools-http.$LIB_EXT.9 $MA_SDK_BASE/rsdk/$con/$bit/
        ln -s libcxxtools-http.$LIB_EXT.9 libcxxtools-http.$LIB_EXT

        cp $MA_TOOLS/cxxtools/lib/$con/$bit/libcxxtools-json.$LIB_EXT.9 $MA_SDK_BASE/rsdk/$con/$bit/
        ln -s libcxxtools-json.$LIB_EXT.9 libcxxtools-json.$LIB_EXT

        cp $MA_TOOLS/cxxtools/lib/$con/$bit/libcxxtools-unit.$LIB_EXT.9 $MA_SDK_BASE/rsdk/$con/$bit/
        ln -s libcxxtools-unit.$LIB_EXT.9 libcxxtools-unit.$LIB_EXT

        cp $MA_TOOLS/cxxtools/lib/$con/$bit/libcxxtools-xmlrpc.$LIB_EXT.9 $MA_SDK_BASE/rsdk/$con/$bit/
        ln -s libcxxtools-xmlrpc.$LIB_EXT.9 libcxxtools-xmlrpc.$LIB_EXT

        cp $MA_TOOLS/cxxtools/lib/$con/$bit/libcxxtools.$LIB_EXT.9 $MA_SDK_BASE/rsdk/$con/$bit/
        ln -s libcxxtools.9 libcxxtools.$LIB_EXT

cp $MA_TOOLS/cxxtools/lib/$con/$bit/libcxxtools-bin.la $MA_SDK_BASE/rsdk/$con/$bit
        ln -s libcxxtools-bin.la libcxxtools-bin.a
	
        cp $MA_TOOLS/cxxtools/lib/$con/$bit/libcxxtools-http.la $MA_SDK_BASE/rsdk/$con/$bit
        ln -s libcxxtools-http.la libcxxtools-http.a

        cp $MA_TOOLS/cxxtools/lib/$con/$bit/libcxxtools-json.la $MA_SDK_BASE/rsdk/$con/$bit
        ln -s libcxxtools-json.la libcxxtools-json.a

        cp $MA_TOOLS/cxxtools/lib/$con/$bit/libcxxtools-unit.la $MA_SDK_BASE/rsdk/$con/$bit
        ln -s libcxxtools-unit.la libcxxtools-unit.a

        cp $MA_TOOLS/cxxtools/lib/$con/$bit/libcxxtools-xmlrpc.la $MA_SDK_BASE/rsdk/$con/$bit
        ln -s libcxxtools-xmlrpc.la libcxxtools-xmlrpc.a

        cp $MA_TOOLS/cxxtools/lib/$con/$bit/libcxxtools.la $MA_SDK_BASE/rsdk/$con/$bit
        ln -s libcxxtools.la libcxxtools.a

        cp $MA_TOOLS/tntnet/lib/$con/$bit/libtntnet.la $MA_SDK_BASE/rsdk/$con/$bit
        ln -s libtntnet.la libtntnet.a

        cp $MA_TOOLS/tntnet/lib/$con/$bit/libtntnet_sdk.la $MA_SDK_BASE/rsdk/$con/$bit
        ln -s libtntnet_sdk.la libtntnet_sdk.a


			cp $MA_BASE/build/makefiles/$con/$bit/libma_service_manager_client.$LIB_EXT.5.0 $MA_SDK_BASE/rsdk/$con/$bit/
			ln -s libma_service_manager_client.$LIB_EXT.5.0 libma_service_manager_client.$LIB_EXT.5
			ln -s libma_service_manager_client.$LIB_EXT.5 libma_service_manager_client.$LIB_EXT
	        fi	
		$MA_BASE/tools/beacon/build/build.sh -v $version -c $con -t $buildType -p $platform -a $arch -b $bit $toolChainPath
	fi
	if [ $? -ne 0 ] 
	then
	echo "********** " Beacon $bit bit $con " Build Failed.... " 
	exit 1
	else  
	echo "********** " Beacon $bit bit $con "Build Ending **********" 
	fi 
done
done

cd $cwd

##############Creating SDK tar ball
OUTPUT_FILE_NAME="ma"_"$platform"_"$arch"_"$configType"".tar"

if [ "$platform" == "HP-UX" ] && [ "$arch" == "itanium" ]
then
OUTPUT_FILE_NAME="ma"_"$platform"_"$arch"_"$configType"".tar"
fi

## changing file name for MLOS
if [ "$platform" == "Linux" ] && [ "$archBit" == "64" ] && [ -f "/etc/redhat-release" ]
then
DIST=`cat /etc/redhat-release | cut -d" " -f1`
if [ x"mileaccess" == x"$DIST" ]
then
OUTPUT_FILE_NAME="ma"_"$platform"_"MLOS"_"$arch"_"$configType"".tar"
fi
fi

MA_OUT_DIR=$MA_BASE/build/makefiles/ma_out_dir
rm -rf $MA_OUT_DIR 
mkdir $MA_OUT_DIR 
cd $MA_OUT_DIR 
cp -rf $MA_BASE/include ./
echo "Deleting SVN entries"
for i in `find ./include -name ".svn" -type d`
do
	rm -rf $i
done 
cp -rf ../release ./
PACKAGES="./release ./include"
echo $OUTPUT_FILE_NAME
echo "Changing directory to " $outputDir
tar cvf $OUTPUT_FILE_NAME $PACKAGES 
gzip $OUTPUT_FILE_NAME
rm -rf ./include
rm -rf ./release
echo $PACKAGES


fi #[ "$buildType" != "clean" ]

cd $curdir
echo "Changing directory back to " $curdir
exit 0

