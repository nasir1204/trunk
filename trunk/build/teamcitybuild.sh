#!/bin/bash

#compilation of Unit Tests is done by this step and expect the executables to be present.
curdir=`pwd`
scriptFullPath=$0

BUILD_PATH=`echo $scriptFullPath | grep ".teamcitybuild.sh" | sed -e  's/.teamcitybuild.sh//g' | sed -e 's/.teamcitybuild//g'`

utfilepath=$BUILD_PATH/utfile.txt
exepath=$BUILD_PATH/makefiles/release/
ma_tools_base=$BUILD_PATH/../../ma_tools/

export LD_LIBRARY_PATH=$ma_tools_base/mxml/lib/release/:$ma_tools_base/mfecryptc/lib/release/:$ma_tools_base/zlib/lib/release/:$ma_tools_base/libuv/lib/release/:$ma_tools_base/msgpack/lib/release/:$ma_tools_base/unity/lib/release:$ma_tools_base/libini/lib/release/:$ma_tools_base/cppunit/lib/release/:$ma_tools_base/gtest/lib/release/:$ma_tools_base/curl/lib/release/:$ma_tools_base/sqlite/lib/release/:$ma_tools_base/openssl/lib/release/:$ma_tools_base/trex/lib/release/:.
export PATH=$PATH:.
export DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH:/usr/lib:$ma_tools_base/mxml/lib/release/:$ma_tools_base/mfecryptc/lib/release/:$ma_tools_base/zlib/lib/release/:$ma_tools_base/libuv/lib/release/:$ma_tools_base/msgpack/lib/release/:$ma_tools_base/unity/lib/release:$ma_tools_base/libini/lib/release/:$ma_tools_base/cppunit/lib/release/:$ma_tools_base/gtest/lib/release/:$ma_tools_base/curl/lib/release/:$ma_tools_base/openssl/lib/release/:$ma_tools_base/trex/lib/release/:.
cd $exepath

echo "********************************************"
echo "       Running (Release) Unit Tests"
echo "********************************************"

for test_binary in `cat $utfilepath`
do
  test_binary_=`echo "$test_binary" | tr -d '\n' | tr -d '\r'`
  echo "Executing ["$test_binary_ "]..."

  ./$test_binary_

  if [ $? -ne 0 ] 
  then
    echo "********** Unit Tests Failed.... " 
    exit 1
  else  
    echo "********** Unit Tests Passed.... " 
  fi 
done
cd $curdir
echo "Changing directory back to " $curdir
exit 0
