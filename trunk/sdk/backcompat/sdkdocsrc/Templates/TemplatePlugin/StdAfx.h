/******************************************************************************
    
    CMA Template Plugin DLL StdAfx.h file

 *****************************************************************************/
#if !defined(AFX_STDAFX_H__4E52E94C_3C4B_4A29_8784_0EE79A07827F__INCLUDED_)
#define AFX_STDAFX_H__4E52E94C_3C4B_4A29_8784_0EE79A07827F__INCLUDED_

#define _UNICODE //or _MBCS for ANSI programs
#define UNICODE

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#endif
