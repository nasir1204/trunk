-- TODO: If you are using this file as a template to create an event schema
-- for your event parser plugin, make the modifications noted below.


-- TODO: Add a CREATE TABLE statement for each of your event tables. For the
-- sample event parser plugin, there is only one table, SampleEvent.
IF NOT EXISTS (SELECT * FROM sysobjects WHERE name = 'SampleEvent')
    CREATE TABLE SampleEvent
    (
     -- TODO: You must have a date/time field in each of your tables, therefore
     -- leave the following EventDateTime column declaration as is.
        EventDateTime DATETIME DEFAULT GETDATE(),

     -- TODO: Add your event columns here. The EventID and HostName columns are
     -- used by the sample event parser plugin. If you are using this file as a
     -- template you should replace them with columns appropriate to your event
     -- data structure.
        EventID INT,
        HostName NVARCHAR(255)
    )

GO


-- TODO: Add a row to ReportEventTables for each of your event data tables.
--- Change the EventTableName column value to the name of your table. Leave the
-- rest of the column values as they are.

-- NOTE: Tables in ReportEventTables can be purged by users via the ePO
-- console. If you have a table that you don't want to have purged, do not
-- include it in ReportEventTables.

-- The column values are:

--      EventTableName
--          The name of one of your tables.

--      DateTimeFieldName
--          The name of the column of the table that contains the date and time
--          of the event.

--      Active
--          Default this to 1.
IF not exists(select EventTableName from ReportEventTables where EventTableName = 'SampleEvent')
    insert into ReportEventTables (EventTableName,DateTimeFieldName,Active)
        VALUES ('SampleEvent','EventDateTime',1)

GO


-- TODO: Each of your events must have an event category. You can pick an
-- existing category from the EventCategory table, or add new categories. If
-- you need a new category, modify the code below. Otherwise remove it.

-- The column values are:

--      Name
--          The name of the event category. Event categories are composed of all
--          uppercase words separated by underscores, ex. VIRUS_DETECTED_REMOVED,
--          FIREWALL_RULE_TRIGGERED.

--      AssociateThreat
--          Set this to 1 if all events in this category relate to a known,
--          named threat or violated rule, and the event XML contains the name
--          of the threat or rule. Set to 0 if they all do not. Here are some
--          examples of existing categories to make this clearer:

--              Category                    AssociatedThreat  Reason
--              BANNED_CONTENT_NOT_REMOVED                 1  Events contain
--                                                            the name of the
--                                                            banned content
--                                                            threat
--              FIREWALL_RULE_TRIGGERED                    1  The threat name
--                                                            is the rule name
--              ON_ACCESS_DISABLED                         0  No threat name
--              ROGUE_DETECTED                             0  No threat name
IF not exists(select EventCategoryID from EventCategory where Name = 'SAMPLE_CATEGORY')
    insert into EventCategory (Name, AssociateThreat)
        values('SAMPLE_CATEGORY', 0);

GO


-- TODO: Add an row to the EventFilter table for each of your event IDs.

-- You must provide the following column values:

--      EventID
--          Contact the ePO development manager to obtain a range of event IDs.

--      Flag
--          First choose ONE of these severities (despite the appearance, they
--          are not bit values). The severity value you add to the database
--          must match the severity in events you send from your product on
--          client computers. NOTE that these are *7* digit hex values, not 8.

--              0x8000000 critical
--              0x4000000 major
--              0x2000000 minor
--              0x1000000 warning
--              0x0000000 informational

--          Second, OR the severity with one of these forwarding values:

--              0x0000001 Forward this event from client computers by default
--              0x0000000 Don't forward this event

--      EventCategory
--          Use an existing category from the EventCategory table or add your
--          own as shown above.

--      HideInFilterTab
--          Set to zero.

-- As an example, the following events are defined for the sample point product:

--      EventID  Severity       Category                             Forward  Description
--      99900    critical       SAMPLE_CATEGORY                      Yes      Sample Event A
--      99901    major          VIRUS_DETECTED_NOT_REMOVED           Yes      Sample Event B
--      99902    informational  ACCESS_PROTECTION_VIOLATION_BLOCKED  No       Sample Event C

declare @category int;
declare @eventID int;
declare @flag int;

select @category = EventCategoryID from EventCategory where Name = 'SAMPLE_CATEGORY'
select @eventID = 99900
select @flag = 0x8000001

IF exists(select EventID from EventFilter where EventID=@eventID)
    update EventFilter set EventCategory=@category, HideInFilterTab=0, Flag=@flag where EventID=@eventID;
ELSE
    insert into EventFilter (EventID, Flag, EventCategory, HideInFilterTab) 
        values(@eventID, @flag, @category, 0)


select @category = EventCategoryID from EventCategory where Name = 'VIRUS_DETECTED_NOT_REMOVED'
select @eventID = 99901
select @flag = 0x4000001

IF exists(select EventID from EventFilter where EventID=@eventID)
    update EventFilter set EventCategory=@category, HideInFilterTab=0, Flag=@flag where EventID=@eventID;
ELSE
    insert into EventFilter (EventID, Flag, EventCategory, HideInFilterTab) 
        values(@eventID, @flag, @category, 0)


select @category = EventCategoryID from EventCategory where Name = 'ACCESS_PROTECTION_VIOLATION_BLOCKED'
select @eventID = 99902
select @flag = 0x0000000

IF exists(select EventID from EventFilter where EventID=@eventID)
    update EventFilter set EventCategory=@category, HideInFilterTab=0, Flag=@flag where EventID=@eventID;
ELSE
    insert into EventFilter (EventID, Flag, EventCategory, HideInFilterTab) 
        values(@eventID, @flag, @category, 0)

GO


-- TODO: Add a localized description for each of your events in the following
-- languages:

--      0409    English
--      0407    German (Standard)
--      040A    Spanish (Spain, Traditional Sort)
--      040C    French (Standard)
--      0412    Korean
--      0411    Japanese
--      0804    Chinese (PRC)
--      0404    Chinese (Taiwan)

-- You must provide the following column values:

--      EventID             The event ID you are setting the localized description for
--      Name                The description
--      Description         Make this the same as Name
--      Language            The 4-digit locale ID

-- Here are an example for the sample point product's 3 events.
-- You can modify these for your own product's events.

DELETE EventFilterDesc WHERE EventID=99900
DELETE EventFilterDesc WHERE EventID=99901
DELETE EventFilterDesc WHERE EventID=99902

-- 0409 English
INSERT EventFilterDesc (EventID, Name, Description, Language)
    VALUES(99900,'Sample Event A Occurred','Sample Event A Occurred','0409')

INSERT EventFilterDesc (EventID, Name, Description, Language)
    VALUES(99901,'Sample Event B Occurred','Sample Event B Occurred','0409')

INSERT EventFilterDesc (EventID, Name, Description, Language)
    VALUES(99902,'Sample Event C Occurred','Sample Event C Occurred','0409')
GO

-- 0407 German (Standard)
INSERT EventFilterDesc (EventID, Name, Description, Language)
    VALUES(99900,'Beispielfall A Trat Auf','Beispielfall A Trat Auf','0407')

INSERT EventFilterDesc (EventID, Name, Description, Language)
    VALUES(99901,'Beispielfall B Trat Auf','Beispielfall B Trat Auf','0407')

INSERT EventFilterDesc (EventID, Name, Description, Language)
    VALUES(99902,'Beispielfall C Trat Auf','Beispielfall C Trat Auf','0407')
GO

-- 040A Spanish (Spain, Traditional Sort)
INSERT EventFilterDesc (EventID, Name, Description, Language)
    VALUES(99900,'El Acontecimiento A De la Muestra Ocurrió','El Acontecimiento A De la Muestra Ocurrió','040A')

INSERT EventFilterDesc (EventID, Name, Description, Language)
    VALUES(99901,'El Acontecimiento B De la Muestra Ocurrió','El Acontecimiento B De la Muestra Ocurrió','040A')

INSERT EventFilterDesc (EventID, Name, Description, Language)
    VALUES(99902,'El Acontecimiento C De la Muestra Ocurrió','El Acontecimiento C De la Muestra Ocurrió','040A')
GO

-- 040C French (Standard)
INSERT EventFilterDesc (EventID, Name, Description, Language)
    VALUES(99900,'L''Événement A D''Échantillon S''est produit','L''Événement A D''Échantillon S''est produit','040C')

INSERT EventFilterDesc (EventID, Name, Description, Language)
    VALUES(99901,'L''Événement B D''Échantillon S''est produit','L''Événement B D''Échantillon S''est produit','040C')

INSERT EventFilterDesc (EventID, Name, Description, Language)
    VALUES(99902,'L''Événement C D''Échantillon S''est produit','L''Événement C D''Échantillon S''est produit','040C')
GO

-- 0412 Korean
INSERT EventFilterDesc (EventID, Name, Description, Language)
    VALUES(99900,'견본 사건A은 일어났다','견본 사건A은 일어났다','0412')

INSERT EventFilterDesc (EventID, Name, Description, Language)
    VALUES(99901,'견본 사건B은 일어났다','견본 사건B은 일어났다','0412')

INSERT EventFilterDesc (EventID, Name, Description, Language)
    VALUES(99902,'견본 사건C은 일어났다','견본 사건C은 일어났다','0412')
GO

-- 0411 Japanese
INSERT EventFilterDesc (EventID, Name, Description, Language)
    VALUES(99900,'サンプルでき事A は起こった','サンプルでき事A は起こった','0411')

INSERT EventFilterDesc (EventID, Name, Description, Language)
    VALUES(99901,'サンプルでき事B は起こった','サンプルでき事B は起こった','0411')

INSERT EventFilterDesc (EventID, Name, Description, Language)
    VALUES(99902,'サンプルでき事C は起こった','サンプルでき事C は起こった','0411')
GO

--  0804 Chinese (PRC)
INSERT EventFilterDesc (EventID, Name, Description, Language)
    VALUES(99900,'样品事件A 发生了','样品事件A 发生了','0804')

INSERT EventFilterDesc (EventID, Name, Description, Language)
    VALUES(99901,'样品事件B 发生了','样品事件B 发生了','0804')

INSERT EventFilterDesc (EventID, Name, Description, Language)
    VALUES(99902,'样品事件C 发生了','样品事件C 发生了','0804')
GO

-- 0404 Chinese (Taiwan)
INSERT EventFilterDesc (EventID, Name, Description, Language)
    VALUES(99900,'樣品事件A 發生了','樣品事件A 發生了','0404')

INSERT EventFilterDesc (EventID, Name, Description, Language)
    VALUES(99901,'樣品事件B 發生了','樣品事件B 發生了','0404')

INSERT EventFilterDesc (EventID, Name, Description, Language)
    VALUES(99902,'樣品事件C 發生了','樣品事件C 發生了','0404')
GO

-- Always make sure that the last GO in a .SQL file has a carriage return
-- after it.
