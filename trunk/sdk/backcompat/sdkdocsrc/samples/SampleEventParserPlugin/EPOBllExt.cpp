/* Copyright (C) 2005 McAfee, Inc.  All rights reserved. */

/* TODO: To use this sample file as a template, see the ePOBllExt_ProcessEvent
   function below. */

#include "stdafx.h"

#include "SampleEventParserPlugin.h"
#include "..\Common\SamplePointProduct.h"
#include "EPOBllExt.h"
#include "..\..\Include\EventXmlSchema.h"

#define MODULE L"EvtPars"

bstr_t CEPOBllExt::m_bstrConnectionString;

STDMETHODIMP CEPOBllExt::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&IID_IEPOBllExt
	};
	for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
	{
		if (::IsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}

STDMETHODIMP CEPOBllExt::ePOBllExt_SetConnectionString (
    BSTR connectString)
{
	m_bstrConnectionString = connectString;
	return S_OK;
}

STDMETHODIMP CEPOBllExt::ePOBllExt_ProcessEvent(
    BSTR eventXmlFile)
{
/*
TODO: If you are using this sample event parser plugin as template code, this
is probably the only function you will need to modify.

See the "TODO" comments below to find out what you need to change.

Development tips:

-   To test your plugin in isolation:

        1.	Install ePO server.

        2.	Build your plugin DLL file.

        4.	Load your .SQL schema file into the ePO database. You can use Query
            Analyzer to do this, or use this OSQL.exe command:

                osql -E -i <your schema name>.sql -d ePO_<your server name>

            Use Query Analyzer, Enterprise Manager, or OSQL to make sure your
            table(s) now exist and are also listed in the ReportEventTables table.

        5.	Register your DLL. For the sample event parser plugin, you could use
            "Regsvr32.exe SampleEventParserPlugin.dll" in the build folder.

        6.	Start AVIView.exe to see whether your debug logging appears.
            AVIView.exe is in the MgmtSDK Tools folder.

        7.	Create some test event files for your product. For example, the
            sample event parser plugin has six sample files, sample01.xml�
            sample06.xml. You can use these to test the sample event parser plugin.

        8.	Copy the test event files to the ePO server Events folder, ex.
            C:\Program Files\Network Associates\ePO\3.5.0\Events. You can copy
            them all at once, or one at a time.

    The ePO EventParser service should pick up the files, pass them to your
    plugin, and the data should appear in your tables in the ePO server
    database. For example, for the sample event parser plugin, each file should
    add one row to the SampleEvent table.

    When it is done with a file, ePO EventParser service will delete it. You
    can add the same file several times as a test. Each time the file should be
    processed.

-   You can hard-code debug break points in an event parser plugin. First
    attach the debugger to EventParser.exe, then trigger an event.

*/

 /* EXAMPLE CODE */

 /* TODO: Parse the event file. The eventXmlFile parameter includes the full
    path to the file.
    
    As a simplified example, this sample code extracts only two things from the
    file: the name of the machine (hostName) and the event ID.

    This example only parses out one event per file as the sample program only
    sends one event per file. You can extend this to parse and send multiple
    events per file.

    A typical sample event file looks like this:

        <?xml version="1.0" encoding="UTF-8"?>
        <SampleEvents>
          <MachineInfo>
            <MachineName>MERRITT</MachineName>
            <AgentGUID>{0BEB0FD3-C755-4307-B316-697213D55A4D}</AgentGUID>
            <IPAddress>172.16.39.131</IPAddress>
            <OSName>Windows 2000</OSName>
            <UserName>mthompso</UserName>
            <TimeZoneBias>420</TimeZoneBias>
          </MachineInfo>
          <SampleSoftware ProductName="Sample Point Product" ProductVersion="1.0.0" ProductFamily="Secure" AdditionalSoftwareValue="test">
            <SampleEvent SpecialValue="test">
              <EventID>99901</EventID>
              <Severity>3</Severity>
              <GMTTime>2004-09-21T21:00:49</GMTTime>
              <Text>This is something about this occurance of event B</Text>
              <MoreEventData>stuff</MoreEventData>
            </SampleEvent>
            <AdditionalSoftwareNode>example
              <AddedSubnodeOfNode>a value</AddedSubnodeOfNode>
            </AdditionalSoftwareNode>
          </SampleSoftware>
        </SampleEvents>
 */

DebugBreak ();

    VARIANT eventId; 
    VARIANT machineName;

    try
    {
        CComPtr<MSXML2::IXMLDOMDocument> document;
        HRESULT result = document.CoCreateInstance (CLSID_FreeThreadedDOMDocument);
        if (FAILED (result))
        {
            SampleLog (FALSE, MODULE, L"Create XML doc %s failed %d", eventXmlFile, result);
            return E_FAIL;
        }

        document->put_validateOnParse (VARIANT_FALSE);
        document->put_async (VARIANT_FALSE);

        VARIANT_BOOL loaded;
        result = document->load (CComVariant (eventXmlFile), &loaded);
    
        if (FAILED (result) || loaded != VARIANT_TRUE)
        {
            SampleLog (FALSE, MODULE, L"Load XML file %s failed %d", eventXmlFile, result);
            return E_FAIL;
        }

        //<?xml version="1.0" encoding="UTF-8"?>
        //<SampleEvents>
        CComPtr<MSXML2::IXMLDOMElement> rootNode;
        result = document->get_documentElement (&rootNode);
        if (FAILED (result) || rootNode == NULL)
        {
            SampleLog (FALSE, MODULE, L"File %s get doc root error %d", eventXmlFile, result);
            return E_FAIL;
        }

        //  <MachineInfo>
        CComPtr<MSXML2::IXMLDOMNode> machineNode;
        result = rootNode->selectSingleNode (NA_XML_EVENT_MACHINE_INFO, &machineNode);
        if (FAILED (result) || machineNode == NULL)
        {
            SampleLog (FALSE, MODULE, L"File %s get machine node error %d", eventXmlFile, result);
            return E_FAIL;
        }

        //    <MachineName>MERRITT</MachineName>
        CComPtr<MSXML2::IXMLDOMNode> nameNode;
        result = machineNode->selectSingleNode (NA_XML_EVENT_MACHINE_NAME, &nameNode);
        if (FAILED (result) || nameNode == NULL)
        {
            SampleLog (FALSE, MODULE, L"File %s get machine name node error %d", eventXmlFile, result);
            return E_FAIL;
        }

        result = nameNode->get_nodeTypedValue (&machineName);
        if (FAILED (result))
        {
            SampleLog (FALSE, MODULE, L"File %s get machine name error %d", eventXmlFile, result);
            return E_FAIL;
        }

        //  <SampleSoftware ProductName="Sample Point Product" ProductVersion="1.0.0" ProductFamily="Secure" AdditionalSoftwareValue="test">
        CComPtr<MSXML2::IXMLDOMNode> softwareNode;
        result = rootNode->selectSingleNode (SAMPLE_EVENT_SOFTWARE_NODE_NAME, &softwareNode);
        if (FAILED (result) || softwareNode == NULL)
        {
            SampleLog (FALSE, MODULE, L"File %s get software node error %d", eventXmlFile, result);
            return E_FAIL;
        }

        //    <SampleEvent SpecialValue="test">
        CComPtr<MSXML2::IXMLDOMNode> eventRootNode;
        result = softwareNode->selectSingleNode (SAMPLE_EVENT_EVENTNODE, &eventRootNode);
        if (FAILED (result) || eventRootNode == NULL)
        {
            SampleLog (FALSE, MODULE, L"File %s get event root node error %d", eventXmlFile, result);
            return E_FAIL;
        }

        //      <EventID>99901</EventID>
        CComPtr<MSXML2::IXMLDOMNode> eventNode;
        result = eventRootNode->selectSingleNode (NA_XML_EVENT_EVENTID, &eventNode);
        if (FAILED (result) || eventNode == NULL)
        {
            SampleLog (FALSE, MODULE, L"File %s get machine name node error %d", eventXmlFile, result);
            return E_FAIL;
        }

        result = eventNode->get_nodeTypedValue (&eventId);
        if (FAILED (result))
        {
            SampleLog (FALSE, MODULE, L"File %s get event ID error %d", eventXmlFile, result);
            return E_FAIL;
        }
    }
    catch (_com_error &error)
    {
        SampleLog (FALSE, MODULE, L"File %s XML parse error %d", eventXmlFile, error.Error ());
        return E_FAIL;
    }

 /* TODO: Assemble a SQL query for inserting a new event using the values from
    the XML file.

    This sample uses the ePO naepodal.h interface to write the data to the a table
    in the ePO database.

    Your tables will already exist. You will make a .SQL schema file to create
    your tables. You will add the schema file to your extended NAP file. When an
    administrator checks in the NAP file, ePO runs the .SQL file and creates the
    tables.

    See the MgmtSDK documentation section "How do I receive my product's events
    at the ePO server?" for more details. */
    ADODB::_ConnectionPtr connection (_uuidof(ADODB::Connection));
    try
    {
     /* Connect to database */
        connection->Open (m_bstrConnectionString, bstr_t(), bstr_t(), ADODB::adConnectUnspecified);
    }
    catch (_com_error &error)
    {
        SampleLog (FALSE, MODULE, L"Error %d on database connect with %s", error.Error (), (WCHAR *)m_bstrConnectionString);
        return E_FAIL;
    }

    bstr_t statement;
    try
    {
     /* Build an insert statement to put the XML data in our table */
        statement = L"INSERT INTO SampleEvent (EventID,HostName) VALUES ('";
        statement += eventId.bstrVal;
        statement += L"','";
        statement += machineName.bstrVal;
        statement += L"')";

     /* Execute the statement */
        connection->Execute (statement, NULL, ADODB::adExecuteNoRecords);
    }
    catch (_com_error &error)
    {
        SampleLog (FALSE, MODULE, L"Error %d on execute of %s", error.Error (), (WCHAR *)statement);
        return E_FAIL;
    }

    return S_OK;
}


STDMETHODIMP CEPOBllExt::ePOBllExt_ProcessProps(BSTR bstrPropsFile)
{
	return S_FALSE;	
}
