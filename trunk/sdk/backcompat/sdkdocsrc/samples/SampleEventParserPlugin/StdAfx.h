/* Copyright (C) 2005 McAfee, Inc.  All rights reserved. */
// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__63ABC1BF_5E24_4E8E_905D_D03D13AF820B__INCLUDED_)
#define AFX_STDAFX_H__63ABC1BF_5E24_4E8E_905D_D03D13AF820B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#define STRICT
#ifndef _WIN32_WINNT
#define _WIN32_WINNT 0x0400
#endif
#define _ATL_APARTMENT_THREADED

#include <atlbase.h>
//You may derive a class from CComModule and use it if you want to override
//something, but do not change the name of _Module
extern CComModule _Module;
#include <atlcom.h>
#include <comutil.h>
#include <vector>

//#pragma warning(disable : 4146)

#include <io.h>
#include <FCNTL.H>
#include <SYS\STAT.H>

#import "msxml4.dll" named_guids raw_interfaces_only 
using namespace MSXML2;

#import <msado15.dll> named_guids\
    rename("EOF", "adoEOF") rename("Recordset", "adoRecordset")

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__63ABC1BF_5E24_4E8E_905D_D03D13AF820B__INCLUDED_)
