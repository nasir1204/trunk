/* Copyright (C) 2005 McAfee, Inc.  All rights reserved. */
#include "stdafx.h"
#include "..\Common\SamplePointProduct.h"
#include "SampleProgram.h"
#include "stdio.h"
#include "string.h"

/** Sample Program Test Interface

        You can run and test the behavior of the Sample Program using its test
        interface.

        The test interface code does not illustrate anything about CMA. You do
        not have to include code similar to it in you own point product.
 
    Using the Test Interface

        The test interface is driven by string commands.

        For example, there is sample code in the Sample Program that calls the
        CMA RunUpdateNow and ImportSiteList APIs. You can make the Sample Program
        execute the code two ways:

            1. Put one or more commands on its command line. For example:

                SampleProgram.exe ImportSiteList RunUpdateNow quit

            2. Run the program at a command prompt, then type in commands. For example:

                SampleProgram.exe
                >ImportSiteList
                >RunUpdateNow
                >quit

        You can also do both:

            SampleProgram.exe ImportSiteList
            >RunUpdateNow
            >quit

    List of commands

        To get a full list of commands, run:

            SampleProgram.exe help

    About CmaWrap

        Most of the samples use CmaWrap classes. CmaWrap simplifies access to
        CMA functions. You can include the CmaWrap source files directly into
        your projects, or create a CmaWrap DLL if that is more convenient.
        
        CmaWrap was originally created for the McAfee VirusScan product. The
        source files included in this sample are modified versions of the
        CmaWrap files that shipped with VirusScan 8.0.
 */

static bool executeCommand (
    wchar_t *command)
{
    if (wcsicmp (command, L"quit") == 0)
        return false;

    if (wcsicmp (command, L"help") == 0)
    {
        wprintf (L" quit                     Exit this program\n");
        wprintf (L" help                     Display this list of commands\n");
        wprintf (L"Scheduling:\n");
        wprintf (L" AddOrUpdateTask          Add or modify a task\n");
        wprintf (L" IterateTasks             List tasks\n");
        wprintf (L" DeleteTask               Delete a task\n");
        wprintf (L" GetTaskInfo              List information about a task\n");
        wprintf (L"Updating:\n");
        wprintf (L" RunUpdateNow             Run an update immediately\n");
        wprintf (L" StopUpdate               Stop an ongoing update\n");
        wprintf (L" RunRollbackUpdate        Rollback the previous update\n");
        wprintf (L" GetUpdateState           Get the state of the current update, if any\n");
        wprintf (L" ImportSitelist           Set the site list\n");
        wprintf (L"Plugin communications:\n");
        wprintf (L" waitForMessages          Wait for registry changes from the plugin\n");
        wprintf (L" \"task <n> <s>\"           Execute a task for the plugin\n");
		wprintf (L"Event sending:\n");
		wprintf (L" \"event <A|B|C> <text>\"   Send an event\n");
    }
    else if (wcsicmp (command, L"waitForMessages") == 0)
    {
        WaitForMessages ();
    }
    else if (_wcsnicmp (command, L"task ", 5) == 0)
    {
        ExecuteTask (command + 5);
    }
    else if (wcsnicmp (command, L"event ", 6) == 0)
    {
        SampleSendEvent (command + 6);
    }
    else if (wcsicmp (command, L"RunUpdateNow") == 0)
    {
        SampleRunUpdateNow ();
    }
    else if (wcsicmp (command, L"AddOrUpdateTask") == 0)
    {
        SampleAddOrUpdateTask ();
    }
    else if (wcsicmp (command, L"IterateTasks") == 0)
    {
        SampleIterateTasks ();
    }
    else if (wcsicmp (command, L"DeleteTask") == 0)
    {
        SampleDeleteTask ();
    }
    else if (wcsicmp (command, L"GetTaskInfo") == 0)
    {
        SampleGetTaskInfo ();
    }
    else if (wcsicmp (command, L"ImportSitelist") == 0)
    {
        SampleImportSitelist ();
    }
    else if (wcsicmp (command, L"StopUpdate") == 0)
    {
        SampleStopUpdate ();
    }
    else if (wcsicmp (command, L"RunRollbackUpdate") == 0)
    {
        SampleRunRollbackUpdate ();
    }
    else if (wcsicmp (command, L"GetUpdateState") == 0)
    {
        SampleGetUpdateState ();
    }
    else
    {
        SampleLog (FALSE, MODULE, L"Unknown command. Type help<enter> for a list of commands");
    }
    return true;
}

int wmain (
    int argc,
    wchar_t *argv[])
{
//DebugBreak ();

    SampleLog (TRUE, MODULE, L"START [%s]", GetCommandLine ());

    bool quit = false;

 /* First execute all of the commands on the command line. If we hit a quit
    command, exit.*/
    for (int i = 1; i < argc && !quit; ++i)
    {
        if (!executeCommand (argv[i]))
        {
            quit = true;
        }
    }

 /* If we didn't get a quit on the command line, execute any one-line commands
    that are typed in at the console until we get a quit there. */

    if (!quit)
    {
        wprintf (L"Type help<enter> for a list of commands\n");

        for (;;)
        {
            wprintf (L">");

            wchar_t command[256];
            _getws (command);

            if (!executeCommand (command))
                break;
        }
    }

    SampleLog (TRUE, MODULE, L"END");

	return 0;
}
