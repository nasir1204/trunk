#include "eventwriter.h"
#include <stdio.h>

main()
{
	/*This is by one way we can generate the events , in which MA have the reponsibility of generating event file and checking all the things...
	  PP's pass the raw XML buffer */
	
	/*There is a second way also , where PP' save the file and then ask MA to process the event , for more details please refer eventwriter.h*/
	
	if(ICMA_OK == CheckDiskSpace())
	{

		/*First check event is filtered or not */
		if( ICMA_EVENT_FILTERED != IsEventFiltered(1045))
		{
			/* Get the machine information and make the event RAW XML buffer as per schema 
				for e.g L"<?xml version=\"1.0\" encoding=\"UTF-8\"?><DummyEvents><MachineInfo><MachineName>scmgateway</MachineName><AgentGUID>{E4C76CC7-98AC-DD11-87C6-0A089C1B2040}</AgentGUID><IPAddress>172.16.220.222</IPAddress><OSName>&quot;YYYYYYYYYYYYYY</OSName><UserName>root</UserName><TimeZoneBias>-330</TimeZoneBias><RawMACAddress>001422212131</RawMACAddress></MachineInfo><Dummy ProductName=\"Dummy\" ProductVersion=\"2.0.0\" ProductFamily=\"TVD\"><PolicyEnforceEvent LocalTime=\"2008:11:11:10:39:33\"><EventID>6000</EventID><Severity>4</Severity><GMTTime>2008-11-11T05:09:33</GMTTime></PolicyEnforceEvent></Dummy></DummyEvents>"
			*/
			wchar_t *pRawXMLBuffer = L"<?xml version=\"1.0\" encoding=\"UTF-8\"?><DummyEvents><MachineInfo><MachineName>scmgateway</MachineName><AgentGUID>{E4C76CC7-98AC-DD11-87C6-0A089C1B2040}</AgentGUID><IPAddress>172.16.220.222</IPAddress><OSName>&quot;YYYYYYYYYYYYYY</OSName><UserName>root</UserName><TimeZoneBias>-330</TimeZoneBias><RawMACAddress>001422212131</RawMACAddress></MachineInfo><Dummy ProductName=\"Dummy\" ProductVersion=\"2.0.0\" ProductFamily=\"TVD\"><PolicyEnforceEvent LocalTime=\"2008:11:11:10:39:33\"><EventID>1045</EventID><Severity>4</Severity><GMTTime>2008-11-11T05:09:33</GMTTime></PolicyEnforceEvent></Dummy></DummyEvents>";
	
			int result = ProcessEvent(pRawXMLBuffer , L"Dummy" , 1045 , 0 );
			if( ICMA_OK != result)
				printf("Failed to process the event due to error code %d\n" ,result);
			else
				printf("event file is generated and Agent will process it as per priority\n");
			
		}	
		else
			printf("Event is filtered by the admin as per event policies\n");
	}
	else
		printf("Space is not sufficent to generate the event\n");
}

