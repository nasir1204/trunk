/* Copyright (C) 2005 McAfee, Inc.  All rights reserved. */
#include "stdafx.h"
#include "SampleProgram.h"
#include "..\Common\SamplePointProduct.h"
#include "stdio.h"
#include "string.h"
#include "..\..\Include\Event.h"
#include <atlbase.h>
#include "..\..\Generated\genevtinf.h"
#include "..\..\Include\naerror.h"

/*
About Sending Events

    You send events from a client computer to an ePO server using the COM
    IEpoEventInf2 event interface.
    
    The interface creates an XML file that contains the events.
    
    It places the file in the the AgentEvents folder under the CMA data folder
    (usually C:\Documents and Settings\All Users\Application Data\Network
    Associates\Common Framework\AgentEvents).

    The CMA service does not need to be running to create events. They will be
    stored in the AgentEvents folder until the CMA service is ready to send
    them.


Creating and Using an Event Interface Object

    To send events from a client computer to the ePO server, you need to create
    an IEpoEventInf2 event interface object:
    
        - Include the MgmtSDK file Generated\genevtinf.h.

        - Call CoCreateInstance as follows:

            CComPtr<IEpoEventInf2> eventInterface;

            HRESULT result = CoCreateInstance (
                __uuidof (EpoEventInf),
                NULL,
                CLSCTX_ALL,
                __uuidof (IEpoEventInf2),
                (void **)(&eventInterface));

        - Check the result code for negative values which indicate an error creating
          the object.

          If you get the result code REGDB_E_CLASSNOTREG (0x80040154), it means
          that CMA is not installed. In that case don't bother trying to send
          events to a non-existent ePO server via a non-existent agent.

    After you have created the object, you can call the following functions.
    
    You must call the IEpoEventInf2::Initialize and IEpoEventInf2::CreateMachineElement
    functions, in that order, first before calling any of the other functions.


Debugging Tips

    - After you create an event XML file, it should appear in the CMA
      AgentEvents folder, usually C:\Documents and Settings\All Users\
      Application Data\Network Associates\Common Framework\AgentEvents.

      Set the REG_DWORD value HKEY_LOCAL_MACHINE\SOFTWARE\Network Associates\
      TVD\Shared Components\Framework\XMLClearText=1 so that your file will
      not be encrypted.

    - If CMA does not send your event, remember that CMA will not send the
      event immediately unless the severity is at or above the current
      immediate forwarding threshold, which is SEVERITY_MAJOR (3) by default.


IEpoEventInf2::Initialize

    HRESULT IEpoEventInf2::Initialize ( 
        BSTR softwareId,
        BSTR rootName,
        BSTR softwareNodeName,
        BSTR softwareName,
        BSTR version,
        BSTR productFamily);

    Uses

        IEpoEventInf2::Initialize effectively creates the structure of the event
        XML file. Here is an example call to illustrate where the parameters
        are used in the XML file. This is for illustration only, these are not
        valid parameters:

            eventInterface->Initialize (
                L"MY_SOFTWARE_ID",
                L"MY_ROOT_NAME,
                L"MY_SOFTWARE_NODE_NAME",
                L"MY_SOFTWARE_NAME",
                L"MY_VERSION",
                L"MY_PRODUCT_FAMILY")

        This XML would be created by the example call:

            <?xml version="1.0" encoding="UTF-8"?>
            <MY_ROOT_NAME>
              <MY_SOFTWARE_NODE_NAME ProductName="MY_SOFTWARE_NAME" ProductVersion="MY_VERSION"
                ProductFamily="MY_PRODUCT_FAMILY">
              </MY_SOFTWARE_NODE_NAME>
            </MY_ROOT_NAME>

    When to call

        You must call this function after creating the IEpoEventInf2 object, and
        before calling the IEpoEventInf2::CreateMachineElement function.

    Return Value

        Returns a negative value if initialization failed. See the MgmtSDK file
        naError.h for the list of codes.

    Parameters

        BSTR softwareId

            Your product's software ID.

        BSTR rootName

            The name of the root node as described in Uses above. This value be
            unique to your product and must match the value you register for
            your event parser plugin. For example, registering the sample event
            parser plugin creates this registry value (see the MgmtSDK file
            Samples\SampleEventParserPlugin\Sample.rgs for where this comes from):

                HKLM\SOFTWARE\Network Associates\ePolicy Orchestrator\EventParser\
                    BLL Extension\SampleEvents\CLSID="91A5D241-CB4D-4e6a-BC96-AA75227253DC"

            This tells the EventParser service that if it receives any events with
            "SampleEvents" as the rootName, it should run the event parser registered
            with the clsid 91A5D241-CB4D-4e6a-BC96-AA75227253DC.

        BSTR softwareNodeName

            The name of the software node as described in Uses above.

        BSTR softwareName

            The name of your product.

        BSTR version

            The product version in x.y.z format.

        BSTR productFamily

            Use one of these strings:

                TVD     Your product is an anti-virus product, ie it uses DAT and engine files
                Secure  Your product is a non-anti-virus product, ex. McAfee Desktop Firewall


IEpoEventInf2::CreateMachineElement

    HRESULT IEpoEventInf2::CreateMachineElement (
        BSTR ipAddress,
        BSTR os,
        BSTR userName)

    When to call

        You must call this function after calling the IEpoEventInf2::Initialize function.

    Uses

        IEpoEventInf2::CreateMachineElement adds a "MachineInfo" subtree to the
        event XML file. Here is an example call to illustrate where the parameters
        are used in the XML file. This is for illustration only, these are not
        valid parameters:

            eventInterface->CreateMachineElement (L"MY_IP_ADDRESS", L"MY_OS", L"MY_USER_NAME");

        The example call would add the MachineInfo subtree shown below within the
        XML structure created by the IEpoEventInf2::Initialize function:

            <?xml version="1.0" encoding="UTF-8"?>
            <MY_ROOT_NAME>

              <MachineInfo>
                <MachineName>...</MachineName>
                <AgentGUID>...</AgentGUID>
                <IPAddress>MY_IP_ADDRESS</IPAddress>
                <OSName>MY_OS</OSName>
                <UserName>MY_USER_NAME</UserName>
                <TimeZoneBias>...</TimeZoneBias>
              </MachineInfo>

              <MY_SOFTWARE_NODE_NAME ProductName="MY_SOFTWARE_NAME" ProductVersion="MY_VERSION"
                ProductFamily="MY_PRODUCT_FAMILY">
              </MY_SOFTWARE_NODE_NAME>
            </MY_ROOT_NAME>

    Return value

        Returns a negative value if the function fails. See the MgmtSDK file
        naError.h for the list of codes.

    Parameters

        BSTR ipAddress

            Pass NULL to have CMA put in the computer's IP address automatically,
            or pass a string IP address of the form "xxx.xxx.xxx.xxx".

        BSTR os

            Pass NULL to have CMA put in an OS description value from an
            internal list of OS description strings, or pass your own string.

        BSTR userName

            Pass NULL to have CMA put in the current user name, or pass in the
            name.


IEpoEventInf2::CreateEventElement

    HRESULT IEpoEventInf2::CreateEventElement (
        BSTR eventNodeName,
        long eventId,
        long severity,
        DATE gmtTime,
        IUnknown **resultingEventElement);

    When to call

        Call this function after creating your IEpoEventInf2 object using
        CoCreateInstance, IEpoEventInf2::Initialize, and IEpoEventInf2::
        CreateMachineElement.

    Uses

        This function adds an event element to the XML file.

        Each time you call this function, it creates another event element in
        the XML file. You can create as many event elements at one time as you
        need. However, you must create at least one.

        The event elements are created under the software element that was
        created by the IEpoEventInf2::Initialize function. Here is an example
        call to illustrate where the parameters are used in the XML file. This
        is for illustration only, these are not valid parameters:

            CComPtr<IEventElement> eventElement;
            eventInterface->CreateEventElement (
                L"MY_EVENT_NODE",
                MY_EVENT_ID,
                MY_SEVERITY,
                MY_TIME,
                &eventElement);

        The example call would add the MachineInfo subtree shown below within the
        XML structure created by the IEpoEventInf2::Initialize function:

            ...
              <MY_SOFTWARE_NODE_NAME ProductName="MY_SOFTWARE_NAME" ProductVersion="MY_VERSION"
                ProductFamily="MY_PRODUCT_FAMILY">
                <MY_EVENT_NODE>
                  <EventID>MY_EVENT_ID</EventID>
                  <Severity>MY_SEVERITY</Severity>
                  <GMTTime>MY_TIME</GMTTime>
                </MY_EVENT_NODE>
              </MY_SOFTWARE_NODE_NAME>
            ...

    Return value

        Returns a negative value if the function fails. See the MgmtSDK file
        naError.h for the list of codes.

        This function may also return ERROR_EVTINF_EVT_FILTERED, which is not
        an error. ePO administrators can specify that some events should be
        "filtered", which means that they are not added to the XML file and not
        sent to the ePO server.
        
        If an event is filterd, IEpoEventInf2::CreateEventElement returns
        ERROR_EVTINF_EVT_FILTERED.

        The file EvtFiltr.ini in the CMA data directory contains the list of
        filtered IDs on the client computer.

    Parameters

        BSTR eventNodeName

            The name of your event node as described in Uses above.

        long eventId

            The ID number of the event. You can have different event IDs for 
            for different situations. For example, VirusScan has an event for
            when a scan starts (event ID 10016), when an infected file is found
            (1024), etc.

            Every product has a range of event IDs reserved for its use. For
            example, the sample product has the range 99900 to 99999. You can
            get a range of IDs by contacting the ePO development manager.

            You can also reuse IDs of other products. For example, a specialized
            virus scanning product could reuse VirusScan's infected file 1024
            event when it finds an infected file.

            There is a list of existing events and event ranges in the MgmtSDK
            file Include\EventIds.h.
            
        long severity

            Every event ID has a fixed severity. For example, the severity of
            event 1024 "Infected file found" is SEVERITY_CRITICAL. The severity
            you return here must be the same as the value that is listed in the
            ePO console. The console gets the severity from the EventFilter table
            of the ePO database, which is loaded from your extended NAP file.

            Use one of these values of type EVENTSEVERITY from the MgmtSDK file
            Include\Event.h:

                SEVERITY_INFORMATIONAL
                SEVERITY_WARNING
                SEVERITY_MINOR
                SEVERITY_MAJOR
                SEVERITY_CRITICAL

            The severity level controls whether the CMA service sends an event
            to the ePO server immediately or waits until the next normal
            connection time.
            
            By default, SEVERITY_MAJOR events are sent immediately, others
            wait. However, ePO administrators can set immediate forwarding to
            whatever severity level they choose via the ePO console by setting
            the McAfee Agent event forwarding policies.

        DATE gmtTime

            The time the event occurred. If you are counting the time as the
            moment the event is created, you can use this code to get the time
            in the correct format:

                SYSTEMTIME systemTime;
                GetSystemTime (&systemTime);
                DATE gmtTime;
                SystemTimeToVariantTime (&systemTime, &gmtTime);

        IUnknown **resultingEventElement
       
            This returns a COM pointer to the event element that was created.
            Pass an IEventElement object pointer, like this:

                CComPtr<IEventElement> eventElement;
                eventInterface->CreateEventElement (..., ..., ..., ..., &eventElement);

            You can use the IEventElement::SetAttributeString and IEventElement::
            AddElementInto functions to add information to the event element.


IEpoEventInf2::GetMachineElement/GetSoftwareElement

    HRESULT IEpoEventInf2::GetMachineElement (
        IUnknown **node)
    
    HRESULT IEpoEventInf2::GetSoftwareElement (
        IUnknown **node)

    When to call

        Call either of these functions after creating your IEpoEventInf2 object
        using CoCreateInstance, IEpoEventInf2::Initialize, and IEpoEventInf2::
        CreateMachineElement.

    Uses

        You may wish to add additional nodes or attributes to the event XML
        MachineInfo node or the software node. Use these function to access
        them if necessary.

    Return value

        Returns a negative value if the function fails. See the MgmtSDK file
        naError.h for the list of codes.

    Parameter

        IUnknown **node

            Returns a COM pointer to the machine node or software node. Pass an
            IEventElement object pointer, like this:

                CComPtr<IEventElement> machineElement;
                eventInterface->GetMachineElement (&machineElement);

            Or:

                CComPtr<IEventElement> softwareElement;
                eventInterface->GetSoftwareElement (&softwareElement);

            You can use the IEventElement::SetAttributeString and IEventElement::
            AddElementInto functions to add information to the elements.


IEpoEventInf2::Save

    HRESULT IEpoEventInf2::Save (
        void)

    When to call

        Call this function after you have created all of your events or other
        XML values using the other IEpoEventInf2 functions.

    Uses

        This function saves the event XML file to the CMA data folder's
        AgentEvent subfolder. The file name is automatically assigned by CMA

    Return value

        Returns a negative value if the function fails. See the MgmtSDK file
        naError.h for the list of codes.


IEpoEventInf2::DeInitialize

    HRESULT IEpoEventInf2::DeInitialize (
        void)

    When to call

        Call this function when you are finished using the IEpoEventInf2 object.

    Uses

        This frees memory allocated by the IEpoEventInf2 object.

    Return value

        Returns a negative value if the function fails. See the MgmtSDK file
        naError.h for the list of codes.


IEventElement::SetAttributeString/SetAttributeInt

    HRESULT IEventElement::SetAttributeString (
        BSTR attributeName,
        BSTR value);

    HRESULT IEventElement::SetAttributeInt (
        BSTR attributeName,
        long value);

    When to call

        Call either of these functions after you get a COM pointer to a node
        from one of the IEpoEventInf2 functions GetMachineElement,
        GetSoftwareElement, or CreateEventElement; or from the IEventElement
        function AddElementInto.

    Uses

        Use these functions to set attributes in an existing node in an XML
        event file. Here is an example call which adds an additional attribute
        MY_ATTRIBUTE with the value "22" to an existing software node created
        by the IEpoEventInf2::Initialize function:

            CComPtr<IEventElement> softwareElement;
            eventInterface->GetSoftwareElement (&softwareElement);
            softwareElement->SetAttributeInt (L"MY_ATTRIBUTE", 22);

        This shows the resulting XML with MY_ATTRIBUTE="22".
            ...
              <MY_SOFTWARE_NODE_NAME ProductName="MY_SOFTWARE_NAME" ProductVersion="MY_VERSION"
                ProductFamily="MY_PRODUCT_FAMILY" MY_ATTRIBUTE="22">
              </MY_SOFTWARE_NODE_NAME>
            ...

    Return value

        Returns a negative value if the function fails. See the MgmtSDK file
        naError.h for the list of codes.

    Parameters

        BSTR attributeName

            The name of the attribute to set.

        BSTR value
        long value

            The value to set.


IEventElement::AddElementInto

    HRESULT IEventElement::AddElementInto (
        BSTR newNodeName,
        BSTR newNodeValue,
        IEventElement **newNode);

    When to call

        Call this function after you get a COM pointer to a node from one of
        the IEpoEventInf2 functions GetMachineElement, GetSoftwareElement, or
        CreateEventElement; or from the IEventElement function AddElementInto.
    
    Uses

        Use this function to add a subnode to an existing node of an XML event
        file. Here is an example call which adds an additional node MY_SUBNODE
        with the value "MY SUBNODE VALUE" to an existing event node created by
        the IEpoEventInf2::CreateEventElement function:

            CComPtr<IEventElement> newSubnode;
            eventElement->AddElementInto (L"MY_SUBNODE", L"MY SUBNODE VALUE", &newSubnode);

        This shows the resulting XML with the new node.

            ...
                <MY_EVENT_NODE>
                  <EventID>MY_EVENT_ID</EventID>
                  <Severity>MY_SEVERITY</Severity>
                  <GMTTime>MY_TIME</GMTTime>

                  <MY_SUBNODE>MY SUBNODE VALUE</MY_SUBNODE>

                </MY_EVENT_NODE>
            ...

    Return value

        Returns a negative value if the function fails. See the MgmtSDK file
        naError.h for the list of codes.

    Parameters

        BSTR newNodeName

            The name of the node to create.

        BSTR newNodeValue

            The value of the node.

        IEventElement **newNode

            Returns a COM pointer to the new subnode. See Uses above for an
            example.
*/

extern void SampleSendEventWithMatchingDeInitialize (
    int eventId,
    EVENTSEVERITY severity,
    WCHAR *eventText);
extern void SampleSendEventCreate (
    IEpoEventInf2 *eventInterface,
    int eventId,
    EVENTSEVERITY severity,
    WCHAR *eventText);

void SampleSendEvent (
    WCHAR *parameters)
{
    SampleLog (TRUE, MODULE, L"Generating event");

 /* For our sample program, the user must enter two parameters. Extract and check
    them. */
    WCHAR eventName[3] = L"";
    int eventId;
    EVENTSEVERITY severity;
    WCHAR eventText[256] = L"";
    swscanf (parameters, L"%2s %[^`]", eventName, eventText);

    if (wcsicmp (eventName, L"A") == 0)
    {
        eventId = SAMPLE_EVENT_A;
        severity = SEVERITY_CRITICAL;
    }
    else if (wcsicmp (eventName, L"B") == 0)
    {
        eventId = SAMPLE_EVENT_B;
        severity = SEVERITY_MAJOR;
    }
    else if (wcsicmp (eventName, L"C") == 0)
    {
        eventId = SAMPLE_EVENT_C;
        severity = SEVERITY_INFORMATIONAL;
    }
    else
    {
        SampleLog (FALSE, MODULE, L"First parameter (event name) must be A, B, or C in [%s]", parameters);
        return;
    }
    if (wcslen (eventText) == 0)
    {
        SampleLog (FALSE, MODULE, L"Second parameter (event text) not found in [%s]", parameters);
        return;
    }

 /* The event interface uses COM. You must initialize COM sometime before using
    the event interface. */
    HRESULT result = CoInitialize (NULL);
    if (FAILED (result))
    {
        SampleLog (FALSE, MODULE, L"CoInitialize error %d", result);
    }
    else
    {
     /* This actually sends the event */
        SampleSendEventWithMatchingDeInitialize (eventId, severity, eventText);

     /* CoInitializes must have matching CoUninitializes. */
        CoUninitialize ();
    }
}

static void SampleSendEventWithMatchingDeInitialize (
    int eventId,
    EVENTSEVERITY severity,
    WCHAR *eventText)
{
 /* First create the event interface pointer. */
    CComPtr<IEpoEventInf2> eventInterface;
    HRESULT result = CoCreateInstance (
        __uuidof (EpoEventInf),
        NULL,
        CLSCTX_ALL,
        __uuidof (IEpoEventInf2),
        (void **)(&eventInterface));

    if (FAILED (result))
    {
        SampleLog (FALSE, MODULE, L"CoCreateInstance error %d", result);
        return;
    }

 /* You must call the IEpoEventInf2::Initialize function after creating the
    IEpoEventInf2 object with CoCreateInstance. For this sample, it would
    create this XML based on the parameters:
    
        <?xml version="1.0" encoding="UTF-8"?>
        <SampleEvents>
          <SampleSoftware ProductName="Sample Point Product" ProductVersion="1.0.0" ProductFamily="Secure">
          </SampleSoftware>
        </SampleEvents>
 */
    result = eventInterface->Initialize( 
        CComBSTR (SAMPLE_SOFTWARE_ID),              // "SAMPLE__1000"
        CComBSTR (SAMPLE_EVENT_ROOT_NAME),          // "SampleEvents"
        CComBSTR (SAMPLE_EVENT_SOFTWARE_NODE_NAME), // "SampleSoftware"
        CComBSTR (SAMPLE_SOFTWARE_NAME),            // "Sample Point Product"
        CComBSTR (SAMPLE_SOFTWARE_VERSION),         // "1.0.0"
        CComBSTR (L"Secure"));

    if (FAILED (result))
    {
        SampleLog (FALSE, MODULE, L"Error %d initializing event interface", result);
    }
    else
    {
     /* This actually sends the event */
        SampleSendEventCreate (eventInterface, eventId, severity, eventText);

     /* You MUST have a matching IEpoEventInf2::DeInitialize call for the
        Initialize call, or you will get memory leaks. */
        eventInterface->DeInitialize();
    }
}

static void SampleSendEventCreate (
    IEpoEventInf2 *eventInterface,
    int eventId,
    EVENTSEVERITY severity,
    WCHAR *eventText)
{
 /* You must call the IEpoEventInf2::CreateMachineElement function after
    calling IEpoEventInf2::Initialize. When I ran this code on my computer, it
    created the MachineInfo section shown here:

        <?xml version="1.0" encoding="UTF-8"?>
        <SampleEvents>

          <MachineInfo>
            <MachineName>MERRITT</MachineName>
            <AgentGUID>{4645961D-40B8-4D65-BEC3-6FB53B96E69B}</AgentGUID>
            <IPAddress>172.16.39.131</IPAddress>
            <OSName>Windows 2000</OSName>
            <UserName>mthompso</UserName>
            <TimeZoneBias>420</TimeZoneBias>
          </MachineInfo>

          <SampleSoftware ProductName="Sample Point Product" ProductVersion="1.0.0" ProductFamily="Secure">
          </SampleSoftware>
        </SampleEvents>
 */
    HRESULT result = eventInterface->CreateMachineElement (NULL, NULL, NULL);

    if (FAILED (result))
    {
        SampleLog (FALSE, MODULE, L"Error %d creating machine element", result);
        return;
    }

 /* You can have one or more event elements (but you can't have zero). Use
    IEpoEventInf2::CreateEventElement to create each element.
    
    For this sample, we will create one event element. When I ran this on my
    computer using an event ID of 99901 and severity of 1, it created the
    SampleEvent section shown here:
 
          ...
          <SampleSoftware ProductName="Sample Point Product" ProductVersion="1.0.0" ProductFamily="Secure" AdditionalSoftwareValue="test">

            <SampleEvent>
              <EventID>99901</EventID>
              <Severity>1</Severity>
              <GMTTime>2004-09-15T19:07:58</GMTTime>
            </SampleEvent>

          </SampleSoftware>
          ...
 */
    SYSTEMTIME systemTime;
    GetSystemTime (&systemTime);

    DATE variantTime;
    SystemTimeToVariantTime (&systemTime, &variantTime);

    CComPtr<IEventElement> eventElement;

    result = eventInterface->CreateEventElement(
        CComBSTR (SAMPLE_EVENT_EVENTNODE),   // "SampleEvent"
        eventId,
        severity,
        variantTime,
        &eventElement);

 /* Filtering is normal, not an error. */
    if (result == ERROR_EVTINF_EVT_FILTERED)
    {
        SampleLog (TRUE, MODULE, L"FYI, event ID %d was filtered out", eventId);
    }
    else if (FAILED (result))
    {
        SampleLog (FALSE, MODULE, L"Error %d creating machine element", result);
        return;
    }

 /* This sample uses IEventElement::SetAttributeString to add a "SpecialValue"
    attribute with the value "test" to the event element:

        ... 
            <SampleEvent SpecialValue="test">
        ...
 */
    result = eventElement->SetAttributeString (CComBSTR("SpecialValue"), CComBSTR("test"));
    if (FAILED (result))
    {
        SampleLog (FALSE, MODULE, L"Error %d setting SpecialValue", result);
        return;
    }

 /* This sample uses IEventElement::AddElementInto to add two subnodes to the
    node, "Text" with the value passed in on the command line, and
    "MoreEventData" with the value "stuff":

        ...
            <SampleEvent SpecialValue="test">
              <EventID>99901</EventID>
              <Severity>1</Severity>
              <GMTTime>2004-09-15T20:18:59</GMTTime>

              <Text>I typed this in at the console</Text>
              <MoreEventData>stuff</MoreEventData>

            </SampleEvent>
        ...
 */
    CComPtr<IEventElement> newEventSubnode;
    result = eventElement->AddElementInto (CComBSTR(L"Text"), CComBSTR(eventText), &newEventSubnode);
    if (FAILED (result))
    {
        SampleLog (FALSE, MODULE, L"Error %d setting Text", result);
        return;
    }

 /* We could add attributes or subnodes to the newEventSubnode, but we're not,
    so release it before reusing the variable. */
    newEventSubnode = NULL;

    result = eventElement->AddElementInto (CComBSTR (L"MoreEventData"), CComBSTR (L"st��ff"), &newEventSubnode);
    if (FAILED (result))
    {
        SampleLog (FALSE, MODULE, L"Error %d setting MoreEventData", result);
        return;
    }

 /* You may wish to add additional attributes or subnodes to the software node
    as well. Use IEpoEventInf2::GetSoftwareElement to get the node for
    modification. */
    CComPtr<IEventElement> softwareElement;
    result = eventInterface->GetSoftwareElement (&softwareElement);
    if (FAILED (result))
    {
        SampleLog (FALSE, MODULE, L"Error %d getting software element", result);
        return;
    }

 /* This sample illustrates adding attributes and subnodes to the software
    element and subnodes of a created subnode:

        ...
          <SampleSoftware ... AdditionalSoftwareValue="test">
            ...
            <AdditionalSoftwareNode>example
              <AddedSubnodeOfNode>a value</AddedSubnodeOfNode>
            </AdditionalSoftwareNode>

          </SampleSoftware>
        ...
 */
    result = softwareElement->SetAttributeString (CComBSTR ("AdditionalSoftwareValue"), CComBSTR ("test"));
    if (FAILED (result))
    {
        SampleLog (FALSE, MODULE, L"Error %d setting AdditionalSoftwareValue", result);
        return;
    }

    CComPtr<IEventElement> newSoftwareSubnode;
    result = softwareElement->AddElementInto (CComBSTR (L"AdditionalSoftwareNode"), CComBSTR (L"example"), &newSoftwareSubnode);
    if (FAILED (result))
    {
        SampleLog (FALSE, MODULE, L"Error %d setting AdditionalSoftwareNode", result);
        return;
    }

    CComPtr<IEventElement> newSubSubnode;
    result = newSoftwareSubnode->AddElementInto (CComBSTR (L"AddedSubnodeOfNode"), CComBSTR (L"a value"), &newSubSubnode);
    if (FAILED (result))
    {
        SampleLog (FALSE, MODULE, L"Error %d setting AddedSubnodeOfNode", result);
        return;
    }

 /* Save the XML file. When I ran it with "event B This is something about this
    occurance of event B", the complete file looked like this:

        <?xml version="1.0" encoding="UTF-8"?>
        <SampleEvents>
          <MachineInfo>
            <MachineName>MERRITT</MachineName>
            <AgentGUID>{0BEB0FD3-C755-4307-B316-697213D55A4D}</AgentGUID>
            <IPAddress>172.16.39.131</IPAddress>
            <OSName>Windows 2000</OSName>
            <UserName>mthompso</UserName>
            <TimeZoneBias>420</TimeZoneBias>
          </MachineInfo>
          <SampleSoftware ProductName="Sample Point Product" ProductVersion="1.0.0" ProductFamily="Secure" AdditionalSoftwareValue="test">
            <SampleEvent SpecialValue="test">
              <EventID>99901</EventID>
              <Severity>3</Severity>
              <GMTTime>2004-09-21T21:00:49</GMTTime>
              <Text>This is something about this occurance of event B</Text>
              <MoreEventData>stuff</MoreEventData>
            </SampleEvent>
            <AdditionalSoftwareNode>example
              <AddedSubnodeOfNode>a value</AddedSubnodeOfNode>
            </AdditionalSoftwareNode>
          </SampleSoftware>
        </SampleEvents>
 */
    result = eventInterface->Save ();
    if (FAILED (result))
    {
        SampleLog (FALSE, MODULE, L"Error %d saving event file", result);
        return;
    }
}
