/* Copyright (C) 2005 McAfee, Inc.  All rights reserved. */
#include "stdafx.h"
#include "SampleProgram.h"
#include "..\Common\SamplePointProduct.h"
#include "CmaWrap\CmaWrap.h"
#include "stdio.h"

void SampleRunRollbackUpdate (
    void)
{
/** RunRollbackUpdate

    int CmaWrap::RunRollbackUpdate (
        void)

    Uses

        Sometimes users regret doing a DAT file update. There may occasionally
        be a defect in a DAT file, and they want to go back to the previous
        version of the file.

        CmaWrap::RunRollbackUpdate allows you to easily go back to the previous
        verion of the current DAT file. It does not apply to engine or product
        files.

        RunRollbackUpdate does not download any files. Instead, the CMA Updater
        just replaces the current DAT files with a locally saved copy of the
        previous DAT files.

    Return value

        Returns a negative error code, or zero for success. The MgmtSDK
        Include\naError.h file contains a list of the error codes.

 */

    SampleLog (TRUE, MODULE, L"Run rollback update");

    CmaWrap *cma = CmaWrapCreate (SAMPLE_SOFTWARE_CLASS, 0);
    if (!cma)
    {
        SampleLog (FALSE, MODULE, L"CmaWrapCreate failed");
    }
    else
    {
     /* This sample program only creates or deletes a single task. The
        SampleAddOrUpdateTask function creates this task. */
        int result = cma->RunRollbackUpdate ();
        if (result < 0)
        {
            SampleLog (FALSE, MODULE, L"RunRollbackUpdate failed %d", result);
        }

        cma->Release();
    }
}
