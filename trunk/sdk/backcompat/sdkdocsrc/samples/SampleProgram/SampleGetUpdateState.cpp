/* Copyright (C) 2005 McAfee, Inc.  All rights reserved. */
#include "stdafx.h"
#include "SampleProgram.h"
#include "..\Common\SamplePointProduct.h"
#include "CmaWrap\CmaWrap.h"
#include "..\..\Include\naUpdate.h"
#include "stdio.h"

void SampleGetUpdateState (
    void)
{
/** GetUpdateState

    int CmaWrap::GetUpdateState (
        int *updateState)

    Uses

        CmaWrap::GetUpdateState allows you to find out what the CMA updater is
        currently doing.

    Parameters

        int *updateState

            Returns the current state. See the MgmtSDK Include\naUpdate.h file
            for a list of the possible values.

    Return value

        Returns a negative error code, or zero for success. The MgmtSDK
        Include\naError.h file contains a list of the error codes.
  */

    CmaWrap *cma = CmaWrapCreate (SAMPLE_SOFTWARE_CLASS, 0);
    if (!cma)
    {
        SampleLog (FALSE, MODULE, L"CmaWrapCreate failed");
    }
    else
    {
        int updateState;
        int result = cma->GetUpdateState (&updateState);
        if (result < 0)
        {
            SampleLog (FALSE, MODULE, L"RunUpdateNow failed %d", result);
        }
        else
        {
            wchar_t *updateStateString;
            switch (updateState)
            {
                case eUPDATE_UNKNOWN:
                    updateStateString = L"UNKNOWN";
                    break;
                case eUPDATE_READY:
                    updateStateString = L"READY";
                    break;
                case eUPDATE_FINISHED_OK:
                    updateStateString = L"FINISHED_OK";
                    break;
                case eUPDATE_FINISHED_FAIL:
                    updateStateString = L"FINISHED_FAIL";
                    break;
                case eUPDATE_FINISHED_NOTFOUND:
                    updateStateString = L"FINISHED_NOTFOUND";
                    break;
                case eUPDATE_FINISHED_CANCEL:
                    updateStateString = L"FINISHED_CANCEL";
                    break;
                case eUPDATE_FINISHED_WAITING_REBOOT:
                    updateStateString = L"FINISHED_WAITING_REBOOT";
                    break;
                case eUPDATE_FINISHED_WAITING_REBOOT_RESTART:
                    updateStateString = L"FINISHED_WAITING_REBOOT_RESTART";
                    break;
                case eUPDATE_FINISHED_PARTIAL:
                    updateStateString = L"FINISHED_PARTIAL";
                    break;
                case eUPDATE_SUCCEEDED:
                    updateStateString = L"SUCCEEDED";
                    break;
                case eUPDATE_FAILED:
                    updateStateString = L"FAILED";
                    break;
                case eUPDATE_INIT:
                    updateStateString = L"INIT";
                    break;
                case eUPDATE_LOADCONFIG:
                    updateStateString = L"LOADCONFIG";
                    break;
                case eUPDATE_PRENOTIFY:
                    updateStateString = L"PRENOTIFY";
                    break;
                case eUPDATE_EOL_CHECK:
                    updateStateString = L"EOL_CHECK";
                    break;
                case eUPDATE_ROLLBACK_CHECK:
                    updateStateString = L"ROLLBACK_CHECK";
                    break;
                case eUPDATE_RUNNING:
                    updateStateString = L"RUNNING";
                    break;
                case eUPDATE_CRITICAL:
                    updateStateString = L"CRITICAL";
                    break;
                case eUPDATE_NONCRITICAL:
                    updateStateString = L"NONCRITICAL";
                    break;
                case eUPDATE_SUSPEND:
                    updateStateString = L"SUSPEND";
                    break;
                case eUPDATE_RESUME:
                    updateStateString = L"RESUME";
                    break;
                case eUPDATE_DOWNLOAD:
                    updateStateString = L"DOWNLOAD";
                    break;
                case eUPDATE_STOPSERVICE:
                    updateStateString = L"STOPSERVICE";
                    break;
                case eUPDATE_STARTSERVICE:
                    updateStateString = L"STARTSERVICE";
                    break;
                case eUPDATE_BACKUP:
                    updateStateString = L"BACKUP";
                    break;
                case eUPDATE_COPY:
                    updateStateString = L"COPY";
                    break;
                case eUPDATE_SETCONFIG:
                    updateStateString = L"SETCONFIG";
                    break;
                case eUPDATE_POSTNOTIFY:
                    updateStateString = L"POSTNOTIFY";
                    break;
                case eUPDATE_CHECK_SITE_STATUS:
                    updateStateString = L"CHECK_SITE_STATUS";
                    break;
                case eUPDATE_COMPARE_SITES:
                    updateStateString = L"COMPARE_SITES";
                    break;
                case eUPDATE_PRENOTIFYFORCE:
                    updateStateString = L"PRENOTIFYFORCE";
                    break;
            }
            SampleLog (TRUE, MODULE, L"updateState=%s (%d)",   updateStateString, updateState);
        }
        cma->Release();
    }
}
