//----------------------------------------------------------------------------
/* Copyright (C) 2005 McAfee, Inc.  All rights reserved. */
//----------------------------------------------------------------------------
// filename:    assertivew.cpp
// created:     2003.04.11
//----------------------------------------------------------------------------
#include "stdafx.h"
#include "compiler.h"
void CDECL AssertiveStradd(char* dest,size_t maxDestChars,const char* source);
void CDECL AssertiveLineText(int line,char lineText[6]);
void CDECL DebugHeapAtExit(void);

typedef unsigned char       UINT8, *PUINT8;

#ifdef DEBUGHEAP

bool debugHeapLockInitialized = 0;
UINT debugHeapLockOwnerThreadId = 0;
CRITICAL_SECTION debugHeapCriticalSection;

void CDECL DebugHeapLock(void)
{
    // This code assumes it won't be reentered on the first call. In debug
    // builds race condition will exist if more than one thread could
    // potentially allocate memory first.
    if(!debugHeapLockInitialized)
    {
        atexit(DebugHeapAtExit);
        // Not going to worry about uninitializing this critical section. It
        // will get cleaned up with process termination.
        InitializeCriticalSection(&debugHeapCriticalSection);
        debugHeapLockInitialized = 1;
    }
    EnterCriticalSection(&debugHeapCriticalSection);
    ASSERT(!debugHeapLockOwnerThreadId);
    debugHeapLockOwnerThreadId = GetCurrentThreadId();
}

void CDECL DebugHeapUnlock(void)
{
    ASSERT(debugHeapLockOwnerThreadId == GetCurrentThreadId());
    debugHeapLockOwnerThreadId = 0;
    LeaveCriticalSection(&debugHeapCriticalSection);
}

bool/*success*/ CDECL DebugHeapMemProbe(void* mem,size_t size)
{
    bool success = 0;
    __try
    {
        size_t count = 1;
        char* check = static_cast<char*>(mem);
        while(size--)
        {
            count += !!*(check++);
        }
        // If the compiler is able to figure out this is always true then it
        // might optimize out the loop.
        success = !!count;
    }
    __except(EXCEPTION_EXECUTE_HANDLER)
    {
    }
    return success;
}

bool/*success*/ CDECL DebugHeapStrProbe(const char* string)
{
    bool success = 0;
    __try
    {
        strlen(string);
        success = 1;
    }
    __except(EXCEPTION_EXECUTE_HANDLER)
    {
    }
    return success;
}

#endif

void* /*mem*/ CDECL MallocPageable(size_t size)
{
    return malloc(size);
}

void CDECL FreePageable(void* ptr)
{
    free(ptr);
}

int/*newValue*/ CDECL INTERLOCK::IntInc(int* loc)
{
    C_ASSERT(sizeof(int) == sizeof(LONG));
    return InterlockedIncrement(reinterpret_cast<LONG*>(loc));
}

int/*newValue*/ CDECL INTERLOCK::IntDec(int* loc)
{
    C_ASSERT(sizeof(int) == sizeof(LONG));
    return InterlockedDecrement(reinterpret_cast<LONG*>(loc));
}

int/*oldValue*/ CDECL INTERLOCK::IntExc(int* loc,int newValue)
{
    C_ASSERT(sizeof(int) == sizeof(LONG));
    return InterlockedExchange(reinterpret_cast<LONG*>(loc),newValue);
}

void* /*oldValue*/ CDECL INTERLOCK::PtrExc(void** loc,void* newValue)
{
#ifdef InterlockedExchangePointer
    return InterlockedExchangePointer(loc,newValue);
#else
    C_ASSERT(sizeof(void*) == sizeof(LONG));
    return (void*)InterlockedExchange(reinterpret_cast<LONG*>(loc),
        reinterpret_cast<LONG>(newValue));
#endif
}

MUTEX::MUTEX(void)
{
    C_ASSERT(sizeof(reserved) >= sizeof(CRITICAL_SECTION));
    owner = 0;
    InitializeCriticalSection(reinterpret_cast<CRITICAL_SECTION*>(reserved));
}

MUTEX::~MUTEX(void)
{
    ASSERT(!owner);
    DeleteCriticalSection(reinterpret_cast<CRITICAL_SECTION*>(reserved));
}

void MUTEX::Lock(void)
{
    EnterCriticalSection(reinterpret_cast<CRITICAL_SECTION*>(reserved));
    ASSERT(!owner);
    owner = reinterpret_cast<void*>(GetCurrentThreadId());
}

void MUTEX::Unlock(void)
{
    ASSERT(owner == reinterpret_cast<void*>(GetCurrentThreadId()));
    owner = 0;
    LeaveCriticalSection(reinterpret_cast<CRITICAL_SECTION*>(reserved));
}

bool MUTEX::IsLocked(void)
{
    return owner == reinterpret_cast<void*>(GetCurrentThreadId());
}

SPINLOCK::SPINLOCK(void)
{
    C_ASSERT(sizeof(reserved) >= sizeof(CRITICAL_SECTION));
    owner = 0;
    InitializeCriticalSection(reinterpret_cast<CRITICAL_SECTION*>(reserved));
}

SPINLOCK::~SPINLOCK(void)
{
    ASSERT(!owner);
    DeleteCriticalSection(reinterpret_cast<CRITICAL_SECTION*>(reserved));
}

void SPINLOCK::Lock(void)
{
    EnterCriticalSection(reinterpret_cast<CRITICAL_SECTION*>(reserved));
    ASSERT(!owner);
    owner = reinterpret_cast<void*>(GetCurrentThreadId());
}

void SPINLOCK::Unlock(void)
{
    ASSERT(owner == reinterpret_cast<void*>(GetCurrentThreadId()));
    owner = 0;
    LeaveCriticalSection(reinterpret_cast<CRITICAL_SECTION*>(reserved));
}

bool SPINLOCK::IsLocked(void)
{
    return owner == reinterpret_cast<void*>(GetCurrentThreadId());
}

int/*slot*/ CDECL TlsReserve(void)
{
    DWORD slot = TlsAlloc();
    if(!slot) {
        slot = TlsAlloc();
    }
    ASSERT(slot <= INT_MAX);
    if(slot > INT_MAX) {
        slot = 0;
    }
    // win32 documentation does not specify if tls slots are initialized to
    // zero or not, significant overhead involved in initializing them so
    // assume they are until find out otherwise
    ASSERT(!slot || TlsGet(slot) == 0);
    return static_cast<int>(slot);
}

void CDECL TlsSet(int slot,const void* data)
{
    VERIFY(TlsSetValue(slot,const_cast<void*>(data)));
}

void* /*data*/ CDECL TlsGet(int slot)
{
    return TlsGetValue(slot);
}

typedef void (__stdcall*TRAPHANDLERPROTO)(void);
void (__stdcall*trapHandler)(void) = reinterpret_cast<TRAPHANDLERPROTO>(
    DebugBreak);

void CDECL AssertHandler(const char* file,int line,const char* expr)
{
    enum { SCRATCHCHARS = 300 };
    char scratch[SCRATCHCHARS] = "ASSERT: ";
    if(file)
    {
        AssertiveStradd(scratch,SCRATCHCHARS,file);
    }
    if(line)
    {
        char lineText[30];
        AssertiveLineText(line,lineText);
        AssertiveStradd(scratch,SCRATCHCHARS,lineText);
    }
    if(expr)
    {
        AssertiveStradd(scratch,SCRATCHCHARS," \"");
        AssertiveStradd(scratch,SCRATCHCHARS,expr);
        AssertiveStradd(scratch,SCRATCHCHARS,"\"");
    }
    AssertiveStradd(scratch,SCRATCHCHARS,"\r\n");
    DWORD unused;
    WriteFile(GetStdHandle(STD_OUTPUT_HANDLE),scratch,static_cast<UINT32>(
        strlen(scratch)),&unused,0);
    TerminateProcess(GetCurrentProcess(),static_cast<UINT8>(-1));
}


//----------------------------------------------------------------------------
// (C) Copyright 2004 Network Associates Inc. All Rights Reserved
//----------------------------------------------------------------------------
