//----------------------------------------------------------------------------
// (C) Copyright 2004 McAfee Inc. All Rights Reserved
//----------------------------------------------------------------------------
// filename:    regprims.h
// created:     2003.05.15
//----------------------------------------------------------------------------
#ifndef REGPRIMS_H
#define REGPRIMS_H
#ifndef _INC_WINDOWS
    // typedef void* HKEY;
    struct HKEY__ { int unused; };
    typedef struct HKEY__* HKEY;
    #define HKEY_CURRENT_USER           (( HKEY ) 0x80000001 )
    #define HKEY_LOCAL_MACHINE          (( HKEY ) 0x80000002 )
#endif


#ifdef _INC_WINDOWS
void SidConvertToText(PSID sid,wchar_t* textSid,size_t maxTextSidChars);
#endif
bool/*success*/ StrAddThreadSid(wchar_t* textSid,size_t maxTextSidChars);

void RegKeyClose(HKEY key);
HKEY RegKeyOpenRegistry(HKEY constKey,const TCHAR* machineName);
HKEY RegKeyOpenThreadCurrentUser(const TCHAR* machineName);
HKEY RegKeyOpenRead(HKEY rootKey,const TCHAR* keyName);
HKEY RegKeyOpen(HKEY rootKey,const TCHAR* keyName);
HKEY RegKeyCreate(HKEY rootKey,const TCHAR* keyName);
HKEY RegKeyCreateOrOpen(HKEY rootKey,const TCHAR* keyName);

bool/*success*/ RegKeyExists(HKEY rootKey,const TCHAR* keyName);
bool/*success*/ RegValDelete(HKEY key,const TCHAR* valueName);
bool/*success*/ RegValSet(HKEY key,const TCHAR* valueName,int valueType,
    const void* value,size_t valueSize);
bool/*success*/ RegValGet(HKEY key,const TCHAR* valueName,int* valueType,void*
    value,size_t maxValueSize,size_t* valueSize);
bool/*success*/ RegValCopy(HKEY sourceKey,const TCHAR* sourceValueName,HKEY
    destKey,const TCHAR* destValueName);
bool/*success*/ RegValGetInt(HKEY key,const TCHAR* valueName,int* value);
int/*value*/ RegValGetIntDef(HKEY key,const TCHAR* valueName,int defaultValue);
bool/*success*/ RegValGetBool(HKEY key,const TCHAR* valueName,bool* _value);
bool/*success*/ RegValGetTcs(HKEY key,const TCHAR* valueName,TCHAR* value,
    size_t maxValueChars);
bool/*success*/ RegValSetInt(HKEY key,const TCHAR* valueName,int value);
bool/*success*/ RegValSetTcs(HKEY key,const TCHAR* valueName,const TCHAR*
    value);
bool/*success*/ RegKeyEnum(HKEY key,int index,TCHAR* keyName,size_t
    maxChars);
bool/*success*/ RegValEnum(HKEY key,int index,TCHAR* valueName,size_t
    maxValueNameChars,int* valueType,void* value,size_t maxValueSize,size_t*
    valueSize);
bool/*success*/ RegValEnumAsTcs(HKEY key,int index,TCHAR* valueName,size_t
    maxValueNameChars,TCHAR* value,size_t maxValueChars);
bool/*success*/ RegValGetTcsAsInt64(HKEY key,const TCHAR* valueName,INT64*
    value);
bool/*success*/ RegValSetTcsAsInt64(HKEY key,const TCHAR* valueName,INT64
    value);
bool/*success*/ RegKeyDelete(HKEY rootKey,const TCHAR* keyname);

void RegKeyIgnoreLeak(HKEY key);
void DebugHeap_RegKeyClose(HKEY key);
HKEY DebugHeap_RegKeyOpenRegistry(HKEY constKey,const TCHAR* machineName,const char* file,int line);
HKEY DebugHeap_RegKeyOpenThreadCurrentUser(const TCHAR* machineName,const char* file,int line);
HKEY DebugHeap_RegKeyOpenRead(HKEY rootKey,const TCHAR* keyName,const char* file,int line);
HKEY DebugHeap_RegKeyOpen(HKEY rootKey,const TCHAR* keyName,const char* file,int line);
HKEY DebugHeap_RegKeyCreate(HKEY rootKey,const TCHAR* keyName,const char* file,int line);
HKEY DebugHeap_RegKeyCreateOrOpen(HKEY rootKey,const TCHAR* keyName,const char* file,int line);
#ifdef DEBUGHEAP
#define RegKeyClose(a)                 DebugHeap_RegKeyClose(a)
#define RegKeyOpenRegistry(a,b)        DebugHeap_RegKeyOpenRegistry(a,b,__FILE__,__LINE__)
#define RegKeyOpenThreadCurrentUser(a) DebugHeap_RegKeyOpenThreadCurrentUser(a,__FILE__,__LINE__)
#define RegKeyOpenRead(a,b)            DebugHeap_RegKeyOpenRead(a,b,__FILE__,__LINE__)
#define RegKeyOpen(a,b)                DebugHeap_RegKeyOpen(a,b,__FILE__,__LINE__)
#define RegKeyCreate(a,b)              DebugHeap_RegKeyCreate(a,b,__FILE__,__LINE__)
#define RegKeyCreateOrOpen(a,b)        DebugHeap_RegKeyCreateOrOpen(a,b,__FILE__,__LINE__)
#endif


#endif
//----------------------------------------------------------------------------
// (C) Copyright 2004 McAfee Inc. All Rights Reserved
//----------------------------------------------------------------------------
