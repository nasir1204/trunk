//----------------------------------------------------------------------------
/* Copyright (C) 2005 McAfee, Inc.  All rights reserved. */
//----------------------------------------------------------------------------
// filename:    assertive.cpp
// created:     2002.01.14
//----------------------------------------------------------------------------
#include "stdafx.h"
#include "compiler.h"
bool/*success*/ CDECL DebugHeapMemProbe(void* mem,size_t size);
bool/*success*/ CDECL DebugHeapStrProbe(const char* string);
void CDECL DebugHeapLock(void);
void CDECL DebugHeapUnlock(void);


void CDECL AssertiveStradd(char* dest,size_t maxDestChars,const char* source)
{
    ASSERT(dest && maxDestChars > 0 && memchr(dest,0,maxDestChars) && source);
    size_t destLength = strlen(dest);
    size_t maxSourceLength = maxDestChars-destLength-1;
    size_t sourceLength = strlen(source);
    if(maxSourceLength > sourceLength)
    {
        maxSourceLength = sourceLength;
    }
    memcpy((dest += destLength),source,maxSourceLength*sizeof(char));
    dest[maxSourceLength] = 0;
}

void CDECL AssertiveLineText(int line,char lineText[6])
{
    lineText[0] = static_cast<char>('0'+(line/10000)%10);
    lineText[1] = static_cast<char>('0'+(line/1000)%10);
    lineText[2] = static_cast<char>('0'+(line/100)%10);
    lineText[3] = static_cast<char>('0'+(line/10)%10);
    lineText[4] = static_cast<char>('0'+line%10);
    lineText[5] = 0;
    int index = 0;
    while(index < 4 && lineText[index] == '0')
    {
        index ++;
    }
    if(index)
    {
        memmove(lineText,lineText+index,(6-index)*sizeof(lineText[0]));
    }
}

#ifdef DEBUGHEAP

#undef malloc
#undef free
#undef realloc
#undef strdup
#undef wcsdup

const char DEBUGHEAPTAILTAG[] = "end";
struct DEBUGHEAPTAIL
{
    char tag[sizeof(DEBUGHEAPTAILTAG)];
    char string[1];
};
struct DEBUGHEAPLINK
{
    DEBUGHEAPLINK** prev;
    DEBUGHEAPLINK* next;
    DEBUGHEAPTAIL* tail;
    enum { TAG = TAGDEF('h','p','c','k') };
    enum { IGNORE = TAGDEF('h','p','l','k') };
    enum { FREE = TAGDEF('f','r','e','e') };
    int tag[sizeof(void*)/sizeof(int)];
};
DEBUGHEAPLINK* debugHeapLinkFirst = 0;
bool debugHeapLeakTrap = 1;
bool debugHeapLeakAssert = 0;
int debugHeapLocked = 0;

void CDECL DebugHeapCheck(void)
{
    DebugHeapLock();
    debugHeapLocked ++;
    ASSERT(debugHeapLocked == 1);
    debugHeapLocked = 1;
    DEBUGHEAPLINK* link = debugHeapLinkFirst;
    while(link)
    {
        if(DebugHeapMemProbe(link,sizeof(DEBUGHEAPLINK)) &&
            DebugHeapMemProbe(link->prev,sizeof(void*)) && (!link->next ||
            DebugHeapMemProbe(link->next,sizeof(DEBUGHEAPLINK))) &&
            DebugHeapMemProbe(link->tail,sizeof(DEBUGHEAPTAIL)) &&
            DebugHeapStrProbe(link->tail->string))
        {
            int tag = link->tag[sizeof(void*)/sizeof(int)-1];
            if(tag != DEBUGHEAPLINK::TAG && tag != DEBUGHEAPLINK::IGNORE)
            {
                TRAP;
                AssertHandler("heap corruption, allocation underflow?",0,
                    link->tail->string);
            }
            else if(memcmp(link->tail->tag,DEBUGHEAPTAILTAG,sizeof(
                DEBUGHEAPTAILTAG)) != 0)
            {
                TRAP;
                AssertHandler("heap corruption, allocation overflow?",0,
                    link->tail->string);
            }
            else if(*(link->prev) != link)
            {
                TRAP;
                AssertHandler("heap corruption",0,link->tail->string);
            }
        }
        else
        {
            TRAP;
            AssertHandler("heap corruption",0,0);
        }
        link = link->next;
    }
    debugHeapLocked --;
    ASSERT(debugHeapLocked == 0);
    DebugHeapUnlock();
}

void CDECL DebugHeapAtExit(void)
{
    DebugHeapCheck();
    DebugHeapLock();
    debugHeapLocked ++;
    ASSERT(debugHeapLocked == 1);
    DEBUGHEAPLINK* link = debugHeapLinkFirst;
    while(link)
    {
        if(link->tag[sizeof(void*)/sizeof(int)-1] != DEBUGHEAPLINK::IGNORE)
        {
            struct {
                void* ptr;
                size_t size;
                const char* tag;
            } leakInfo;
            leakInfo.ptr = link+1;
            leakInfo.size = reinterpret_cast<char*>(link->tail)-
                static_cast<char*>(leakInfo.ptr);
            leakInfo.tag = link->tail->string;
            if(debugHeapLeakTrap)
            {
                TRAP;
            }
            if(debugHeapLeakAssert)
            {
                AssertHandler("memory leak",0,leakInfo.tag);
            }
        }
        link = link->next;
    }
    debugHeapLocked --;
    ASSERT(debugHeapLocked == 0);
    DebugHeapUnlock();
}

void* /*mem*/ CDECL DebugHeap_malloc(size_t size,const char* file,int line)
{
    DebugHeapCheck();
    void* mem = 0;
    enum { SCRATCHCHARS = 200 };
    char scratch[SCRATCHCHARS] = "";
    AssertiveStradd(scratch,SCRATCHCHARS,file?file:"anonymous");
    if(line)
    {
        AssertiveStradd(scratch,SCRATCHCHARS,":");
        char lineText[6];
        AssertiveLineText(line,lineText);
        AssertiveStradd(scratch,SCRATCHCHARS,lineText);
    }
    size_t length = strlen(scratch);
    DEBUGHEAPLINK* link = static_cast<DEBUGHEAPLINK*>(malloc(sizeof(
        DEBUGHEAPLINK)+size+sizeof(DEBUGHEAPTAIL)+length));
    if(link)
    {
        DebugHeapLock();
        debugHeapLocked ++;
        ASSERT(debugHeapLocked == 1);
        mem = link+1;
        memset(mem,'z',size);
        DEBUGHEAPTAIL* tail = link->tail = reinterpret_cast<DEBUGHEAPTAIL*>(
            static_cast<char*>(mem)+size);
        link->tag[sizeof(void*)/sizeof(int)-1] = DEBUGHEAPLINK::TAG;
        memcpy(tail->tag,DEBUGHEAPTAILTAG,sizeof(DEBUGHEAPTAILTAG));
        strcpy(tail->string,scratch);
        DEBUGHEAPLINK* next = link->next = debugHeapLinkFirst;
        if(next)
        {
            ASSERT(next->prev == &debugHeapLinkFirst);
            next->prev = &(link->next);
        }
        link->prev = &debugHeapLinkFirst;
        debugHeapLinkFirst = link;
        debugHeapLocked --;
        ASSERT(debugHeapLocked == 0);
        DebugHeapUnlock();
    }
    DebugHeapCheck();
    TRAPIF(reinterpret_cast<size_t>(mem) == 0x00872980);
    return mem;
}

void CDECL DebugHeap_free(void* mem)
{
    if(mem)
    {
        DebugHeapLock();
        debugHeapLocked ++;
        ASSERT(debugHeapLocked == 1);
        DEBUGHEAPLINK* link = static_cast<DEBUGHEAPLINK*>(mem)-1;
        if(DebugHeapMemProbe(link,sizeof(DEBUGHEAPLINK)) && (!link->prev ||
            DebugHeapMemProbe(link->prev,sizeof(void*))) && (!link->next ||
            DebugHeapMemProbe(link->next,sizeof(DEBUGHEAPLINK))) &&
            DebugHeapMemProbe(link->tail,sizeof(DEBUGHEAPTAIL)) &&
            DebugHeapStrProbe(link->tail->string))
        {
            switch(link->tag[sizeof(void*)/sizeof(int)-1])
            {
            default:
                TRAP;
                AssertHandler("bad heap ptr",0,0);
                break;
            case DEBUGHEAPLINK::FREE:
                TRAP;
                AssertHandler("bad heap ptr, already freed?",0,link->tail->
                    string);
                break;
            case DEBUGHEAPLINK::IGNORE:
            case DEBUGHEAPLINK::TAG:
                ASSERT(link->prev);
                if(link->next)
                {
                    link->next->prev = link->prev;
                }
                *(link->prev) = link->next;
                link->tag[sizeof(void*)/sizeof(int)-1] =
                    DEBUGHEAPLINK::FREE;
                break;
            }
        }
        else
        {
            TRAP;
            AssertHandler("bad free ptr",0,0);
        }
        debugHeapLocked --;
        ASSERT(debugHeapLocked == 0);
        DebugHeapUnlock();
        free(link);
    }
    DebugHeapCheck();
}

void* CDECL DebugHeap_realloc(void* oldMem,size_t newSize,const char* file,
    int line)
{
    void* newMem = DebugHeap_malloc(newSize,file,line);
    if(newMem)
    {
        DEBUGHEAPLINK* link = static_cast<DEBUGHEAPLINK*>(oldMem)-1;
        size_t oldSize = reinterpret_cast<char*>(link->tail)-
            static_cast<char*>(oldMem);
        memcpy(newMem,oldMem,(newSize > oldSize)?oldSize:newSize);
        DebugHeap_free(oldMem);
    }
    return newMem;
}

void CDECL DebugHeapIgnoreLeak(void* mem)
{
    if(mem)
    {
        DebugHeapLock();
        debugHeapLocked ++;
        ASSERT(debugHeapLocked == 1);
        DEBUGHEAPLINK* link = static_cast<DEBUGHEAPLINK*>(mem)-1;
        if(DebugHeapMemProbe(link,sizeof(DEBUGHEAPLINK)) && (!link->prev ||
            DebugHeapMemProbe(link->prev,sizeof(void*))) && (!link->next ||
            DebugHeapMemProbe(link->next,sizeof(DEBUGHEAPLINK))) &&
            DebugHeapMemProbe(link->tail,sizeof(DEBUGHEAPTAIL)) &&
            DebugHeapStrProbe(link->tail->string))
        {
            switch(link->tag[sizeof(void*)/sizeof(int)-1])
            {
            default:
                TRAP;
                AssertHandler("bad heap ptr",0,0);
                break;
            case DEBUGHEAPLINK::FREE:
                TRAP;
                AssertHandler("bad heap ptr, already freed?",0,link->tail->
                    string);
                break;
            case DEBUGHEAPLINK::IGNORE:
            case DEBUGHEAPLINK::TAG:
                link->tag[sizeof(void*)/sizeof(int)-1] =
                   DEBUGHEAPLINK::IGNORE;
                break;
            }
        }
        else
        {
            TRAP;
            AssertHandler("bad heap ptr",0,0);
        }
        debugHeapLocked --;
        ASSERT(debugHeapLocked == 0);
        DebugHeapUnlock();
    }
    DebugHeapCheck();
}

char* CDECL DebugHeap_strdup(const char* string,const char* file,int line)
{
    ASSERT(string);
    char* newString = static_cast<char*>(DebugHeap_malloc((strlen(string)+1)*
        sizeof(string[0]),file,line));
    if(newString)
    {
        strcpy(newString,string);
    }
    return newString;
}

wchar_t* CDECL DebugHeap_wcsdup(const wchar_t* string,const char* file,int
    line)
{
    ASSERT(string);
    wchar_t* newString = static_cast<wchar_t*>(DebugHeap_malloc((wcslen(
        string)+1)*sizeof(string[0]),file,line));
    if(newString)
    {
        wcscpy(newString,string);
    }
    return newString;
}

void* CDECL operator new(size_t size,const char* file,int line)
{
    return DebugHeap_malloc(size,file,line);
}

void* CDECL operator new[](size_t size,const char* file,int line)
{
    return DebugHeap_malloc(size,file,line);
}

void* CDECL operator new(size_t size)
{
    return DebugHeap_malloc(size,"anonymous new",0);
}

void* CDECL operator new[](size_t size)
{
    return DebugHeap_malloc(size,"anonymous new",0);
}

void CDECL operator delete(void* mem)
{
    DebugHeap_free(mem);
}

void CDECL operator delete[](void* mem)
{
    DebugHeap_free(mem);
}

#else

void* /*mem*/ CDECL DebugHeap_malloc(size_t size,const char* file,int line)
{
    return malloc(size);
}

void CDECL DebugHeap_free(void* mem)
{
    free(mem);
}

void* CDECL DebugHeap_realloc(void* oldMem,size_t newSize,const char* file,int line)
{
    return realloc(oldMem,newSize);
}

char* CDECL DebugHeap_strdup(const char* string,const char* file,int line)
{
    return strdup(string);
}

wchar_t* CDECL DebugHeap_wcsdup(const wchar_t* string,const char* file,int line)
{
    return wcsdup(string);
}

void* CDECL operator new(size_t size,const char* file,int line)
{
    return malloc(size);
}

void* CDECL operator new[](size_t size,const char* file,int line)
{
    return malloc(size);
}

void* CDECL operator new(size_t size)
{
    return malloc(size);
}

void* CDECL operator new[](size_t size)
{
    return malloc(size);
}

void CDECL operator delete(void* mem)
{
    free(mem);
}

void CDECL operator delete[](void* mem)
{
    free(mem);
}

#endif


//----------------------------------------------------------------------------
// (C) Copyright 2004 Network Associates Inc. All Rights Reserved
//----------------------------------------------------------------------------
