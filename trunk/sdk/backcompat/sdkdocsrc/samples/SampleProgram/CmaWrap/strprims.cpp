//----------------------------------------------------------------------------
/* Copyright (C) 2005 McAfee, Inc.  All rights reserved. */
//----------------------------------------------------------------------------
// filename:    strprims.cpp
// created:     2001.11.08
//----------------------------------------------------------------------------
#include "stdafx.h"
#include "compiler.h"
#include <stdio.h>
#include "strprims.h"


// Use template to facilitate single implementation of string primitives for
// use with normal and wide chars.
template<typename CharType> class StringPrims
{
public:
    static const CharType* Empty(void)
    {
        static const CharType empty[] = {0};
        return empty;
    }

    static CharType* FixNull(const CharType* string)
    {
        return const_cast<CharType*>(string?string:Empty());
    }

    static void FixNull(const CharType** string)
    {
        if(!*string)
        {
            *string = Empty();
        }
    }

    static size_t Len(const char* string)
    {
        return string?strlen(string):0;
    }

    static size_t Len(const wchar_t* string)
    {
        return string?wcslen(string):0;
    }

    static void Cpy(char* dest,const char* source)
    {
        if(dest)
        {
            strcpy(dest,source?source:"");
        }
    }

    static void Cpy(wchar_t* dest,const wchar_t* source)
    {
        if(dest)
        {
            wcscpy(dest,source?source:L"");
        }
    }

    static char Toupper(char character)
    {
        return static_cast<char>(toupper(static_cast<unsigned char>(
            character)));
    }

    static wchar_t Toupper(wchar_t character)
    {
        return towupper(character);
    }

    static bool Isspace(char character)
    {
        return !!isspace(character);
    }

    static bool Isspace(wchar_t character)
    {
        return !!iswspace(character);
    }

    static bool Isdigit(char character)
    {
        return !!isdigit(character);
    }

    static bool Isdigit(wchar_t character)
    {
        return !!iswdigit(character);
    }

    static void Move(CharType* dest,const CharType* source,size_t count)
    {
        if(dest && source)
        {
            memmove(dest,source,count*sizeof(dest[0]));
        }
    }

    static CharType* Mchr(const CharType* data,CharType value,size_t length)
    {
        if(!data)
        {
            length = 0;
        }
        while(length-- && *data && *data != value)
        {
            data ++;
        }
        return (length+1)?const_cast<CharType*>(data):0;
    }

    static void Addn(CharType* dest,size_t maxDestChars,const CharType*
        source,size_t sourceLength)
    {
        if(dest && maxDestChars)
        {
            ASSERT(Mchr(dest,0,maxDestChars));
            size_t destLength = Len(dest);
            size_t maxSourceLength = maxDestChars-destLength-1;
            if(!source)
            {
                sourceLength = 0;
            }
            if(maxSourceLength > sourceLength)
            {
                maxSourceLength = sourceLength;
            }
            Move((dest += destLength),source,maxSourceLength);
            dest[maxSourceLength] = 0;
        }
    }

    static void Add(CharType* dest,size_t maxDestChars,const CharType* source)
    {
        Addn(dest,maxDestChars,source,Len(source));
    }

    static void Addlwr(CharType* dest,size_t maxDestChars,const CharType*
        source)
    {
        if(dest && maxDestChars)
        {
            ASSERT(Mchr(dest,0,maxDestChars));
            size_t sourceLength = Len(source);
            size_t destLength = Len(dest);
            dest += destLength;
            maxDestChars -= destLength;
            if(sourceLength >= maxDestChars)
            {
                sourceLength = maxDestChars-1;
            }
            size_t index = 0;
            while(index < sourceLength)
            {
                CharType tempChar = source[index];
                if(tempChar >= 'A' && tempChar <= 'Z')
                {
                    tempChar = static_cast<CharType>(tempChar+'a'-'A');
                }
                dest[index] = tempChar;
                index ++;
            }
            dest[sourceLength] = 0;
        }
    }

    static void Ins(CharType* dest,size_t maxDestChars,const CharType* string)
    {
        if(dest && maxDestChars)
        {
            ASSERT(Mchr(dest,0,maxDestChars));
            size_t destLength = Len(dest);
            size_t length = Len(string);
            if(length >= maxDestChars)
            {
                length = maxDestChars-1;
            }
            if(length+destLength >= maxDestChars)
            {
                destLength = maxDestChars-length-1;
            }
            dest[destLength] = 0;
            Move(dest+length,dest,(destLength+1));
            Move(dest,string,length);
        }
    }

    static int/*0,1,-1*/ Cmpi(const CharType* string1,const CharType*
        string2,size_t count)
    {
        FixNull(&string1);
        FixNull(&string2);
        int result = 0;
        CharType char1,char2;
        do
        {
            if((char1 = Toupper(*(string1++))) != (char2 = Toupper(*(
                string2++))))
            {
                result = (char1 < char2)?-1:1;
                char1 = 0;
            }
        } while(char1 && char2);
        return result;
    }

    static int/*0,1,-1*/ Ncmpi(const CharType* string1,const CharType*
        string2,size_t count)
    {
        FixNull(&string1);
        FixNull(&string2);
        int result = 0;
        CharType char1,char2;
        if(count)
        {
            do
            {
                if((char1 = Toupper(*(string1++))) != (char2 = Toupper(*(
                    string2++))))
                {
                    result = (char1 < char2)?-1:1;
                    char1 = 0;
                }
            } while(--count && char1 && char2);
        }
        return result;
    }

    static int/*0,1,-1*/ Scmpi(const CharType* string1,const CharType*
        string2)
    {
        return Ncmpi(string1,string2,Len(string2));
    }

    // Untested
    // CharType* void Stri(const CharType* dest,const CharType* subString)
    // {
    //     FixNull(&dest);
    //     FixNull(&subString);
    //     size_t index = 0;
    //     while(*dest && Scmpi(dest,subString) != 0)
    //     {
    //         dest ++;
    //     }
    //     return (*dest)?dest:0;
    // }

    static void FromInt(_INT64 value,int flags,int pad,CharType dest[30])
    {
        ASSERT(dest && pad <= 20);
        static CharType const yesText[]   = { 'y','e','s',0 };
        static CharType const noText[]    = { 'n','o',0 };
        static CharType const trueText[]  = { 't','r','u','e',0 };
        static CharType const falseText[] = { 'f','a','l','s','e',0 };
        static CharType const onText[]    = { 'o','n',0 };
        static CharType const offText[]   = { 'o','f','f',0 };
        int index = 0;
        if(value)
        {
            if(value == 1 && (flags&(STR_INTYESNO|STR_INTTRUEFALSE|
                STR_INTONOFF)))
            {
                // special handling for boolean values
                if(flags&STR_INTYESNO)
                {
                    Cpy(dest,yesText);
                }
                else if(flags&STR_INTTRUEFALSE)
                {
                    Cpy(dest,trueText);
                }
                else if(flags&STR_INTONOFF)
                {
                    Cpy(dest,onText);
                }
            }
            else
            {
                if(flags&STR_INTHEX)
                {
                    do
                    {
                        static char hexDigits[] = "0123456789ABCDEF";
                        dest[index] = hexDigits[static_cast<int>(value)&0xF];
                        index ++;
                        value = static_cast<INT64>(static_cast<UINT64>(value
                            )>>4);
                    } while(value);
                    while(index < pad)
                    {
                        dest[index++] = '0';
                    }
                    dest[index] = 'x';
                    dest[index+1] = '0';
                    index += 2;
                }
                else
                {
                    if(flags&STR_INTSIGNED)
                    {
                        if(value < 0)
                        {
                            value = -value;
                        }
                        else
                        {
                            flags &= ~STR_INTSIGNED;
                        }
                    }
                    do
                    {
                        dest[index] = static_cast<CharType>(
                            static_cast<UINT64>(value)%10+'0');
                        index ++;
                        value = static_cast<INT64>(static_cast<UINT64>(value
                            )/10);
                    } while(value);
                    while(index < pad)
                    {
                        dest[index++] = '0';
                    }
                    if(flags&STR_INTSIGNED)
                    {
                        dest[index++] = '-';
                    }
                }
                dest[index] = 0;
                int swapIndex = 0;
                while(index > swapIndex)
                {
                    index --;
                    CharType swap = dest[swapIndex];
                    dest[swapIndex] = dest[index];
                    dest[index] = swap;
                    swapIndex ++;
                }
            }
        }
        else
        {
            // special handling for boolean values
            if(flags&STR_INTYESNO)
            {
                Cpy(dest,noText);
            }
            else if(flags&STR_INTTRUEFALSE)
            {
                Cpy(dest,falseText);
            }
            else if(flags&STR_INTONOFF)
            {
                Cpy(dest,offText);
            }
            else
            {
                if(flags&STR_INTHEX)
                {
                    dest[0] = '0';
                    dest[0] = 'x';
                    dest[0] = '0';
                    pad += 2;
                    index = 3;
                }
                else
                {
                    dest[0] = '0';
                    index = 1;
                }
                while(index < pad)
                {
                    dest[index++] = '0';
                }
                dest[index] = 0;
            }
        }
    }

    static INT64 Toint(const CharType* string,size_t length,int* _flags)
    {
        FixNull(&string);
        static CharType const yesText[]   = { 'y','e','s',0 };
        static CharType const noText[]    = { 'n','o',0 };
        static CharType const trueText[]  = { 't','r','u','e',0 };
        static CharType const falseText[] = { 'f','a','l','s','e',0 };
        static CharType const onText[]    = { 'o','n',0 };
        static CharType const offText[]   = { 'o','f','f',0 };
        INT64 value = 0;
        int flags = 0;
        // strip leading space
        CharType* end = Mchr(string,0,length);
        if(end)
        {
            length = end-string;
        }
        while(length && Isspace(*string))
        {
            string ++;
            length --;
        }
        // strip trialing space
        while(length && Isspace(string[length-1]))
        {
            length --;
        }
        if(length)
        {
            // special handling for boolean letters and strings
            switch(Toupper(string[0]))
            {
            case 'Y':
                if(length == 1 || Scmpi(string,yesText) == 0)
                {
                    value = 1;
                    flags |= STR_INTYESNO;
                }
                break;
            case 'N':
                if(length == 1 || Scmpi(string,noText) == 0)
                {
                    flags |= STR_INTYESNO;
                }
                break;
            case 'T':
                if(length == 1 || Scmpi(string,trueText) == 0)
                {
                    value = 1;
                    flags |= STR_INTTRUEFALSE;
                }
                break;
            case 'F':
                if(length == 1 || Scmpi(string,falseText) == 0)
                {
                    flags |= STR_INTTRUEFALSE;
                }
                break;
            case 'O':
                if(Strscmpi(string,onText) == 0)
                {
                    value = 1;
                    flags |= STR_INTONOFF;
                }
                else if(Strscmpi(string,offText) == 0)
                {
                    flags |= STR_INTONOFF;
                }
                break;
            }
            if(!flags)
            {
                if(string[0] == '-')
                {
                    flags |= STR_INTSIGNED;
                    string ++;
                    length --;
                }
                else if(string[0] == '+')
                {
                    string ++;
                    length --;
                }
                CharType character;
                if(length >= 3 && string[0] == '0' && Toupper(string[1]) ==
                    'X' && (Isdigit(character = Toupper(string[2])) ||
                    (character >= 'A' && character <= 'F')))
                {
                    // C hex notation
                    flags |= STR_INTHEX;
                    string += 2;
                    length -= 2;
                }
                else if(length >= 2 && Isdigit(string[0]))
                {
                    // check for assembly hex notation
                    size_t index = 1;
                    while(index < length && (Isdigit(character = Toupper(
                        string[index])) || (character >= 'A' && character <=
                        'F')))
                    {
                        index ++;
                    }
                    if(Toupper(string[index]) == 'H')
                    {
                        flags |= STR_INTHEX;
                    }
                }
                if(flags&STR_INTHEX)
                {
                    while(length && (Isdigit(character = Toupper(string[0]))
                        || (character >= 'A' && character <= 'F')))
                    {
                        length --;
                        string ++;
                        if(character > '9')
                        {
                            character -= 'A'-'9'-1;
                        }
                        value = (value<<4)+character-'0';
                    }
                }
                else
                {
                    while(length && Isdigit(character = string[0]))
                    {
                        length --;
                        string ++;
                        value = value*10+character-'0';
                    }
                }
                if(flags&STR_INTSIGNED)
                {
                    value = -value;
                }
            }
        }
        if(_flags)
        {
            *_flags = flags;
        }
        return value;
    }

    static void Addint(CharType* dest,size_t maxDestChars,INT64 value,int
        flags,int pad)
    {
        CharType scratch[30];
        if(pad > 20)
        {
            pad = 20;
        }
        FromInt(value,flags,pad,scratch);
        Add(dest,maxDestChars,scratch);
    }

    static void ToEscape(CharType* dest,size_t maxDestChars,const CharType*
        source)
    {
        // Convert any control chars to escape sequences.
        FixNull(&source);
        if(dest && maxDestChars)
        {
            size_t sourceIndex = 0;
            size_t destIndex = 0;
            CharType checkChar;
            CharType escChar = 0;
            while(destIndex+1 < maxDestChars && !!(checkChar = source[
                sourceIndex++]))
            {
                switch(checkChar)
                {
                case '\n':
                    escChar = 'n';
                    break;
                case '\r':
                    escChar = 'r';
                    break;
                case '\t':
                    escChar = 't';
                    break;
                case '\'':
                case '\"':
                case '\\':
                    escChar = checkChar;
                    break;
                }
                if(escChar)
                {
                    if(destIndex+2 < maxDestChars)
                    {
                        dest[destIndex++] = checkChar;
                        dest[destIndex++] = escChar;
                    }
                    else
                    {
                        maxDestChars --;
                    }
                }
                else
                {
                    dest[destIndex++] = checkChar;
                }
           }
            dest[destIndex] = 0;
        }
    }

    static void FromEscape(CharType* dest,size_t maxDestChars,const CharType*
        source)
    {
        // Convert any escape sequences to control chars.
        FixNull(&source);
        if(dest && maxDestChars)
        {
            size_t sourceIndex = 0;
            size_t destIndex = 0;
            CharType checkChar;
            while(destIndex+1 < maxDestChars && !!(checkChar = source[
                sourceIndex++]))
            {
                if(checkChar == '\\')
                {
                    switch(checkChar = source[sourceIndex++])
                    {
                    case 0:
                        maxDestChars = destIndex+1;
                        break;
                    case 'n':
                    case 'N':
                        checkChar = '\n';
                        break;
                    case 'r':
                    case 'R':
                        checkChar = '\r';
                        break;
                    case 't':
                    case 'T':
                        checkChar = '\t';
                        break;
                    }
                }
                if(checkChar)
                {
                    dest[destIndex++] = checkChar;
                }
            }
            dest[destIndex] = 0;
        }
    }

    static CharType* Malloc(size_t chars)
    {
        return static_cast<CharType*>(malloc(chars*sizeof(CharType)));
    }

    static CharType* Dup(const CharType* string)
    {
        CharType* newString = 0;
        if(string)
        {
            newString = Malloc(Len(string)+1);
            if(newString)
            {
                Cpy(newString,string);
            }
        }
        return newString;
    }

    static void Free(CharType* buffer)
    {
        if(buffer)
        {
            free(buffer);
        }
    }

    static CharType* DebugHeapMalloc(size_t chars,const char* file,int line)
    {
        return static_cast<CharType*>(DebugHeap_malloc(chars*sizeof(CharType),
            file,line));
    }

    static CharType* DebugHeapDup(const CharType* string,const char* file,int
        line)
    {
        CharType* newString = 0;
        if(string)
        {
            newString = DebugHeapMalloc(Len(string)+1,file,line);
            if(newString)
            {
                Cpy(newString,string);
            }
        }
        return newString;
    }

    static void DebugHeapFree(CharType* buffer)
    {
        if(buffer)
        {
            DebugHeap_free(buffer);
        }
    }
};
template class StringPrims<char>;
template class StringPrims<wchar_t>;

void Stradd(char* dest,size_t maxDestChars,const char* source)
{
    StringPrims<char>::Add(dest,maxDestChars,source);
}

void Stradd(wchar_t* dest,size_t maxDestChars,const wchar_t* source)
{
    StringPrims<wchar_t>::Add(dest,maxDestChars,source);
}

void Straddn(char* dest,size_t maxDestChars,const char* source,size_t length)
{
    StringPrims<char>::Addn(dest,maxDestChars,source,length);
}

void Straddn(wchar_t* dest,size_t maxDestChars,const wchar_t* source,size_t
    length)
{
    StringPrims<wchar_t>::Addn(dest,maxDestChars,source,length);
}

void Straddlwr(char* dest,size_t maxDestChars,const char* source)
{
    StringPrims<char>::Addlwr(dest,maxDestChars,source);
}

void Straddlwr(wchar_t* dest,size_t maxDestChars,const wchar_t* source)
{
    StringPrims<wchar_t>::Addlwr(dest,maxDestChars,source);
}

void Strins(char* dest,size_t maxDestChars,const char* string)
{
    StringPrims<char>::Ins(dest,maxDestChars,string);
}

void Strins(wchar_t* dest,size_t maxDestChars,const wchar_t* string)
{
    StringPrims<wchar_t>::Ins(dest,maxDestChars,string);
}

int/*0,1,-1*/ Strcmpi(const char* string1,const char* string2)
{
    return StringPrims<char>::Cmpi(string1,string2,SIZE_MAX);
}

int/*0,1,-1*/ Strcmpi(const wchar_t* string1,const wchar_t* string2)
{
    return StringPrims<wchar_t>::Cmpi(string1,string2,SIZE_MAX);
}

int/*0,1,-1*/ Strncmpi(const char* string1,const char* string2,size_t count)
{
    return StringPrims<char>::Ncmpi(string1,string2,count);
}

int/*0,1,-1*/ Strncmpi(const wchar_t* string1,const wchar_t* string2,size_t
    count)
{
    return StringPrims<wchar_t>::Ncmpi(string1,string2,count);
}

int/*0,1,-1*/ Strscmpi(const char* string1,const char* string2)
{
    return StringPrims<char>::Scmpi(string1,string2);
}

int/*0,1,-1*/ Strscmpi(const wchar_t* string1,const wchar_t* string2)
{
    return StringPrims<wchar_t>::Scmpi(string1,string2);
}

int/*0,1,-1*/ Strcmp(const char* string1,const char* string2)
{
    StringPrims<char>::FixNull(&string1);
    StringPrims<char>::FixNull(&string2);
    return strcmp(string1,string2);
}

int/*0,1,-1*/ Strcmp(const wchar_t* string1,const wchar_t* string2)
{
    StringPrims<wchar_t>::FixNull(&string1);
    StringPrims<wchar_t>::FixNull(&string2);
    return wcscmp(string1,string2);
}

int/*0,1,-1*/ Strncmp(const char* string1,const char* string2,size_t count)
{
    StringPrims<char>::FixNull(&string1);
    StringPrims<char>::FixNull(&string2);
    return strncmp(string1,string2,count);
}

int/*0,1,-1*/ Strncmp(const wchar_t* string1,const wchar_t* string2,size_t
    count)
{
    StringPrims<wchar_t>::FixNull(&string1);
    StringPrims<wchar_t>::FixNull(&string2);
    return wcsncmp(string1,string2,count);
}

int/*0,1,-1*/ Strscmp(const char* string1,const char* string2)
{
    StringPrims<char>::FixNull(&string1);
    StringPrims<char>::FixNull(&string2);
    return strncmp(string1,string2,strlen(string2));
}

int/*0,1,-1*/ Strscmp(const wchar_t* string1,const wchar_t* string2)
{
    StringPrims<wchar_t>::FixNull(&string1);
    StringPrims<wchar_t>::FixNull(&string2);
    return wcsncmp(string1,string2,wcslen(string2));
}

// char* void Strstri(const char* dest,const char* subString)
// {
//     return StringPrims<char>::Stri(dest,subString);
// }

// wchar_t* void Strstri(const wchar_t* dest,const wchar_t* subString)
// {
//     return StringPrims<wchar_t>::Stri(dest,subString);
// }

void Strset(char* dest,char value,size_t count)
{
    if(dest)
    {
        memset(dest,value,count);
    }
}

void Strset(wchar_t* dest,wchar_t value,size_t count)
{
    if(dest)
    {
        while(count--)
        {
            *dest++ = value;
        }
    }
}

INT64 Strtoint(const char* string,size_t length,int* flags)
{
    return StringPrims<char>::Toint(string,length,flags);
}

INT64 Strtoint(const wchar_t* string,size_t length,int* flags)
{
    return StringPrims<wchar_t>::Toint(string,length,flags);
}

void Straddint(char* dest,size_t maxDestChars,INT64 value,int flags,int pad)
{
    StringPrims<char>::Addint(dest,maxDestChars,value,flags,pad);
}

void Straddint(wchar_t* dest,size_t maxDestChars,INT64 value,int flags,int
    pad)
{
    StringPrims<wchar_t>::Addint(dest,maxDestChars,value,flags,pad);
}

void Strmove(char* dest,const char* source,size_t count)
{
    StringPrims<char>::Move(dest,source,count);
}

void Strmove(wchar_t* dest,const wchar_t* source,size_t count)
{
    StringPrims<wchar_t>::Move(dest,source,count);
}

char* Strmchr(const char* data,char value,size_t length)
{
    return StringPrims<char>::Mchr(data,value,length);
}

wchar_t* Strmchr(const wchar_t* data,wchar_t value,size_t length)
{
    return StringPrims<wchar_t>::Mchr(data,value,length);
}

void Straddfv(char* dest,size_t maxDestChars,const char* format,va_list args)
{
    if(dest && maxDestChars)
    {
        ASSERT(Strmchr(dest,0,maxDestChars));
        StringPrims<char>::FixNull(&format);
        _vsnprintf(dest,maxDestChars-strlen(dest)-1,format?format:"",args);
        dest[maxDestChars-1] = 0;
    }
}

void Straddfv(wchar_t* dest,size_t maxDestChars,const wchar_t* format,va_list
    args)
{
    if(dest && maxDestChars)
    {
        ASSERT(Strmchr(dest,0,maxDestChars));
        StringPrims<wchar_t>::FixNull(&format);
        _vsnwprintf(dest,maxDestChars-wcslen(dest)-1,format?format:L"",args);
        dest[maxDestChars-1] = 0;
    }
}

void Straddf(char* dest,size_t maxDestChars,const char* format,...)
{
    va_list args;
    va_start(args,format);
    Straddfv(dest,maxDestChars,format,args);
}

void Straddf(wchar_t* dest,size_t maxDestChars,const wchar_t* format,...)
{
    va_list args;
    va_start(args,format);
    Straddfv(dest,maxDestChars,format,args);
}

bool Strisempty(const char* string)
{
    return !string || !string[0];
}

bool Strisempty(const wchar_t* string)
{
    return !string || !string[0];
}

void StrToEscape(char* dest,size_t maxDestChars,const char* source)
{
    StringPrims<char>::ToEscape(dest,maxDestChars,source);
}

void StrFromEscape(char* dest,size_t maxDestChars,const char* source)
{
    StringPrims<char>::FromEscape(dest,maxDestChars,source);
}

void StrToEscape(wchar_t* dest,size_t maxDestChars,const wchar_t* source)
{
    StringPrims<wchar_t>::ToEscape(dest,maxDestChars,source);
}

void StrFromEscape(wchar_t* dest,size_t maxDestChars,const wchar_t* source)
{
    StringPrims<wchar_t>::FromEscape(dest,maxDestChars,source);
}

#undef Strmalloc
#undef Wcsmalloc
#undef Strdup
#undef Strfree

char* Strmalloc(size_t chars)
{
    return StringPrims<char>::Malloc(chars);
}

wchar_t* Wcsmalloc(size_t chars)
{
    return StringPrims<wchar_t>::Malloc(chars);
}

char* Strdup(const char* string)
{
    return StringPrims<char>::Dup(string);
}

wchar_t* Strdup(const wchar_t* string)
{
    return StringPrims<wchar_t>::Dup(string);
}

void Strfree(char* string)
{
    StringPrims<char>::Free(string);
}

void Strfree(wchar_t* string)
{
    StringPrims<wchar_t>::Free(string);
}

char* DebugHeapStrmalloc(size_t chars,const char* file,int line)
{
    return StringPrims<char>::DebugHeapMalloc(chars,file,line);
}

wchar_t* DebugHeapWcsmalloc(size_t chars,const char* file,int line)
{
    return StringPrims<wchar_t>::DebugHeapMalloc(chars,file,line);
}

char* DebugHeapStrdup(const char* string,const char* file,int line)
{
    return StringPrims<char>::DebugHeapDup(string,file,line);
}

wchar_t* DebugHeapStrdup(const wchar_t* string,const char* file,int line)
{
    return StringPrims<wchar_t>::DebugHeapDup(string,file,line);
}

void DebugHeapStrfree(char* string)
{
    StringPrims<char>::DebugHeapFree(string);
}

void DebugHeapStrfree(wchar_t* string)
{
    StringPrims<wchar_t>::DebugHeapFree(string);
}

size_t/*destLength*/ Utf8ToWcs(wchar_t* dest,size_t maxDestLength,
    const char* source,size_t sourceLength)
{
    ASSERT((maxDestLength || !dest) && source && (sourceLength == SIZE_MAX ||
        sourceLength < INT_MAX));
    ASSERT(dest <= dest+maxDestLength);
    wchar_t* destEnd = dest+maxDestLength;
    wchar_t* destStart = dest;
    if(sourceLength == SIZE_MAX)
    {
        sourceLength = strlen(source);
    }
    const char* sourceEnd = source+sourceLength;
    while(source < sourceEnd)
    {
        int code,code1,code2,code3;
        if(!((code = *(source++))&0x80))
        {
            if(++dest <= destEnd)
            {
                dest[-1] = static_cast<wchar_t>(code);
            }
        }
        else if(code&0x40)
        {
            if(!(code&0x20))
            {
                if(source < sourceEnd && ((code1 = source[0])&0xC0) == 0x80)
                {
                    source ++;
                    if(++dest <= destEnd)
                    {
                        dest[-1] = static_cast<wchar_t>(((code&0x1F)<<6)+
                            (code1&0x3F));
                    }
                }
            }
            else if(!(code&0x10))
            {
                if(source+1 < sourceEnd && ((code1 = source[0])&0xC0) ==
                    0x80 && ((code2 = source[1])&0xC0) == 0x80)
                {
                    source += 2;
                    if(++dest <= destEnd)
                    {
                        dest[-1] = static_cast<wchar_t>(((code&0x0F)<<12)+
                            ((code1&0x3F)<<6)+(code2&0x3F));
                    }
                }
            }
            else if(!(code&0x08))
            {
                if(source+2 < sourceEnd && ((code1 = source[0])&0xC0) ==
                    0x80 && ((code2 = source[1])&0xC0) == 0x80 && ((code3 =
                    source[2])&0xC0) == 0x80)
                {
                    source += 3;
                    if((dest += 2) <= destEnd)
                    {
                        code = ((code&0x07)<<18)+((code1&0x3F)<<12)+((code2&
                            0x3F)<<6)+(code3&0x3F);
                        dest[-2] = static_cast<wchar_t>(0xD7c0+(code>>10));
                        dest[-1] = static_cast<wchar_t>(0xDC00+(code&0x3FF));
                    }
                }
            }
            else
            {
                // illegal lead byte, silently stripped
            }
        }
        else
        {
            // tail byte found without lead byte, silently stripped
        }
    }
    return dest-destStart;
}

size_t/*destLength*/ WcsToUtf8(char* dest,size_t maxDestLength,
    const wchar_t* source,size_t sourceLength)
{
    ASSERT((!maxDestLength || dest) && source && (sourceLength == SIZE_MAX ||
        sourceLength < INT_MAX));
    ASSERT(dest <= dest+maxDestLength);
    const char* destEnd = dest+maxDestLength;
    char* destStart = dest;
    if(sourceLength == SIZE_MAX)
    {
        sourceLength = wcslen(source);
    }
    const wchar_t* sourceEnd = source+sourceLength;
    while(source < sourceEnd)
    {
        UINT code;
        if((code = *(source++)) < 0x80)
        {
            if(++dest <= destEnd)
            {
                dest[-1] = static_cast<char>(code);
            }
        }
        else
        {
            if(code < 0x800)
            {
                if((dest += 2) <= destEnd)
                {
                    dest[-2] = static_cast<char>(0xC0|(code>>6));
                    dest[-1] = static_cast<char>(0x80|(code&0x3F));
                }
            }
            else if(code < 0x10000)
            {
                UINT code1;
                // watch for valid surrogate 20 bit character sequences
                if((code&0xFC00) == 0xD800 && source < sourceEnd && ((code1 =
                    *source)&0xFC00) == 0xDC00)
                {
                    source ++;
                    code = ((code&0x3FF)<<10)+(code1&0x3FF)+0x10000;
                    if((dest += 4) <= destEnd)
                    {
                        dest[-4] = static_cast<char>(0xE0|(code>>18));
                        dest[-3] = static_cast<char>(0x80|((code>>12)&
                            0x3F));
                        dest[-2] = static_cast<char>(0x80|((code>>6)&
                            0x3F));
                        dest[-1] = static_cast<char>(0x80|(code&0x3F));
                    }
                }
                else
                {
                    if((dest += 3) <= destEnd)
                    {
                        dest[-3] = static_cast<char>(0xE0|(code>>12));
                        dest[-2] = static_cast<char>(0x80|((code>>6)&0x3F));
                        dest[-1] = static_cast<char>(0x80|(code&0x3F));
                    }
                }
            }
            else
            {
                // out of range code, silently stripped
            }
        }
    }
    return dest-destStart;
}


//----------------------------------------------------------------------------
// (C) Copyright Network Associates 2004 Inc. All Rights Reserved
//----------------------------------------------------------------------------
