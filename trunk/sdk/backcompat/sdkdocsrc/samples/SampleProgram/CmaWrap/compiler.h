//----------------------------------------------------------------------------
// (C) Copyright 2004 McAfee Inc. All Rights Reserved
//----------------------------------------------------------------------------
// filename:    compiler.h
// created:     2001.08.22
//      compiler details encapsulation
//----------------------------------------------------------------------------
#if !defined(COMPILER_H)
#define COMPILER_H
// normalize debug macros
#if !defined(NDEBUG) && ((defined(DBG) && DBG) || (!defined(DBG) && defined(DEBUG)))
    #undef NDEBUG
    #ifndef DEBUG
        #define DEBUG
    #endif
    #ifndef _DEBUG
        #define _DEBUG
    #endif
    #ifndef DBG
        #define DBG 1
    #endif
#else
    #undef DEBUG
    #undef _DEBUG
    #ifndef NDEBUG
        #define NDEBUG
    #endif
    #ifndef DBG
        #define DBG 0
    #endif
#endif
#ifndef DEPSCAN


#include <limits.h>
#include <stddef.h>
#include <stdlib.h>
#include <mbstring.h>
#include <wchar.h>
#include <memory.h>
#include <string.h>
#include <ctype.h>
#include <stdarg.h>
#include <malloc.h>
// enable specific debug functionality
#ifdef DEBUG
    #define TESTING
    #define DEBUGTRAP
    #define DEBUGASSERT
//    #define DEBUGASSERTTRAP
    #define DEBUGTRAPASSERT
#ifndef NODEBUGHEAP
    #define DEBUGHEAP
#endif
    #define DEBUGTRACES
#endif

#ifdef _MSC_VER
    #define LITTLEENDIAN
    #define _INT64 signed __int64
    typedef _INT64 INT64;
    #define INT64_MIN _I64_MIN
    #define INT64_MAX _I64_MAX
    #define UINT64_MAX _UI64_MAX
    #define STDCALL __stdcall
    #ifndef CDECL
        #define CDECL __cdecl
    #endif
    #define _UINT64 unsigned __int64
    typedef _UINT64 UINT64;
    #define UINT64_MAX _UI64_MAX
    #define INT_BITS 32
    #define INT32_MIN INT_MIN
    #define INT32_MAX INT_MAX
    #define UINT32_MAX UINT_MAX
    #if INT_MAX == 0x7FFF
        #error ERROR: 16 bit compiler not supported
    #elif defined(_M_IA64) || defined(_M_AMD64) || defined(_WIN64)
        extern void (__stdcall*trapHandler)(void);
        #define __TRAP__ trapHandler()
        #define PTR_BITS 64
        #define _INTL _INT64
        typedef _INTL INTL;
        #define INTL_MIN UINT64_MIN
        #define INTL_MAX UINT64_MAX
        #define _UINTL _UINT64
        typedef _UINTL UINTL;
        #define UINTL_MAX UINT64_MAX
        #define SIZE_MAX UINT64_MAX
        #define PTRDIFF_MAX INT64_MAX
        #define PTRDIFF_MIN INT64_MIN
    #elif defined(_M_IX86)
        #define __TRAP__ __asm nop __asm int 3 __asm nop __asm { }
        #define PTR_BITS 32
        #define _INTL long
        typedef _INTL INTL;
        #define INTL_MIN LONG_MIN
        #define INTL_MAX LONG_MAX
        #define _UINTL unsigned long
        typedef _UINTL UINTL;
        #define UINTL_MAX ULONG_MAX
        #define SIZE_MAX UINT_MAX
        #define PTRDIFF_MAX INT_MAX
        #define PTRDIFF_MIN INT_MIN
    #else
        #error ERROR: must add support for new cpu to "compiler.h"
    #endif
    #define WCHAR_BITS 16
    #define WCHAR_MIN 0
    #include <tchar.h>
    #ifdef _UNICODE
        #define TCHAR_BITS 8
        #define TCHAR_MIN CHAR_MIN
        #define TCHAR_MAX CHAR_MAX
    #else
        #define TCHAR_BITS 16
        #define TCHAR_MIN 0
        #define TCHAR_MAX USHRT_MAX
    #endif
    // disable zero size array warning
    #pragma warning(disable:4200)
    // disable nameless struct/union warning
    #pragma warning(disable:4201)
    // disable assignment within conditional expression warning
    #pragma warning(disable:4706)
    // disable variable may be used without being initialized warning
    // #pragma warning(disable:4701)
    // disable formal parameter never used warning
    #pragma warning(disable:4100)
    // disable structure was padded due to __declspec(align()) warning
    #pragma warning(disable:4324)
    // disable cast on l-value warning
    #pragma warning(disable:4213)
    // disable copy constructor could not be generated warning
    #pragma warning(disable:4511)
    // disable assignment operator could not be generated warning
    #pragma warning(disable:4512)
    // disable inline function has been removed warning
    #pragma warning(disable:4514)
    // disable function not inlined warning
    #pragma warning(disable:4710)
    // disable static cast truncation of constant value warning
    // #pragma warning(disable:4309)
    // disable __declspec warning on malloc and free implementation
    #pragma warning(disable:4565)
    // disable intrinsic function not available warning
    #pragma warning(disable:4163)
    // disable zero sized array in stack object warning
    #pragma warning(disable:4815)
    #ifdef DEBUG
        // disable conditional expression is constant warning
        #pragma warning(disable:4127)
    #endif
    #define strcmpi(s1,s2) _stricmp(s1,s2)
    #define strncmpi(s1,s2,n) _strnicmp(s1,s2,n)
    #define wcscmpi(s1,s2) _wcsicmp(s1,s2)
    #define wcsncmpi(s1,s2,n) _wcsnicmp(s1,s2,n)
    #define _tcscmpi(s1,s2) _tcsicmp(s1,s2)
    #define _tcsncmpi(s1,s2) _tcsnicmp(s1,s2,n)
#elif defined(__GNUC__)
    #define _INT64 signed long long int
    typedef _INT64 INT64;
    #define INT64_MIN  (-9223372036854775807LL-1)
    #define INT64_MAX  9223372036854775807LL
    #define _UINT64 unsigned long long int
    typedef _UINT64 UINT64;
    #define UINT64_MAX 0xFFFFFFFFFFFFFFFFULL
    #define _INTL long
    typedef _INTL INTL;
    #define INTL_MIN LONG_MIN
    #define INTL_MAX LONG_MAX
    #define _UINTL unsigned long
    typedef _UINTL UINTL;
    #define UINTL_MAX ULONG_MAX
    #if ULONG_MAX == UINT64_MAX
        #define PTR_BITS 64
        #define SIZE_MAX ULONG_MAX
        #define PTRDIFF_MAX ULONG_MAX
        #define PTRDIFF_MIN ULONG_MIN
    #else
        #define PTR_BITS 32
        #define SIZE_MAX UINT_MAX
        #define PTRDIFF_MAX UINT_MAX
        #define PTRDIFF_MIN UINT_MIN
    #endif
    #define INT_BITS 32
    #define INT32_MIN INT_MIN
    #define INT32_MAX INT_MAX
    #define UINT32_MAX UINT_MAX
    #define WCHAR_BITS INT_BITS
    #define WCHAR_MIN INT_MIN
    #define WCHAR_MAX INT_MAX
    #define strncmpi(s1,s2,n) strncasecmp(s1,s2,n)
    #define strcmpi(s1,s2) strcasecmp(s1,s2)
    #define wcsncmpi(s1,s2,n) wcsncasecmp(s1,s2,n)
    #define wcscmpi(s1,s2) wcscasecmp(s1,s2)
    typedef char TCHAR;
    #define TCHAR_BITS 8
    #define TCHAR_MIN CHAR_MIN
    #define TCHAR_MAX CHAR_MAX
    #define _tcslen strlen
    #define _tcscpy strcpy
    #define _tcsncpy strncpy
    #define _tcscat strcat
    #define _tcsncat strncat
    #define _tcschr strchr
    #define _tcsrchr strrchr
    #define _tcsstr strstr
    #define _tcscmp strcmp
    #define _tcsncmp strncmp
    #define _tcscmpi strcmpi
    #define _tcsncmpi strncmpi
    #define STDCALL
    #define CDECL
    extern int (*trapHandler)(int);
    extern int trapHandlerParam;
    #ifdef PROC_IA32
        #define __TRAP__ asm("int $3")
    #else
        #define __TRAP__ trapHandler(trapHandlerParam)
    #endif
#else 
    #error ERROR: must add support for new compiler to "compiler.h"
#endif

#define _INTP _INTL
typedef _INTP INTP;
#define INTP_MIN UINTL_MIN
#define INTP_MAX UINTL_MAX
#define _UINTP _UINTL
typedef _UINTP UINTP;
#define UINTP_MAX UINTL_MAX
#define INT8_MIN SCHAR_MIN
#define INT8_MAX SCHAR_MAX
#define UINT8_MAX UCHAR_MAX
#define INT16_MIN SHRT_MIN
#define INT16_MAX SHRT_MAX
#define UINT16_MAX USHRT_MAX

#if !defined(LITTLEENDIAN) && !defined(BIGENDIAN)
    #error ERROR: must define LITTLEENDIAN or BIGENDIAN to indicate byte order
#endif
#if defined(LITTLEENDIAN) && defined(BIGENDIAN)
    #error ERROR: cannot define both LITTLEENDIAN and BIGENDIAN
#endif

#ifdef LITTLEENDIAN
    #define TAGDEF(a,b,c,d) ((a)+((b)<<8)+((c)<<16)+((d)<<24))
#endif
#ifdef BIGENDIAN
    #define TAGDEF(a,b,c,d) ((d)+((c)<<8)+((b)<<16)+((a)<<24))
#endif

void CDECL AssertHandler(const char* File,int Line,const char* Expr);
#ifdef DEBUGTRAP
    #ifdef DEBUGASSERTTRAP
        #define TRAP __TRAP__; AssertHandler(__FILE__,__LINE__,"TRAP")
    #else
        #define TRAP __TRAP__
    #endif
    #ifdef DEBUGTRAPASSERT
        #define __TRAP __TRAP__
    #else
        #define __TRAP
    #endif
    #define TRAPIF(expr) if(expr) TRAP;
#else
    #define TRAP
    #define __TRAP
    #define TRAPIF(expr)
#endif
#undef ASSERT
#undef VERIFY
#ifdef DEBUGASSERT
    #define ASSERT(expr) if(!(expr)) { __TRAP; AssertHandler(__FILE__,__LINE__,#expr); }
    #define VERIFY(expr) ASSERT(expr)
    #define VERIFYNOT(expr) ASSERT(!(expr))
    #define VERIFYIS(expr,val) ASSERT((expr) == (val))
    #define VERIFYISNOT(expr,val) ASSERT((expr) != (val))
#else
    #define ASSERT(expr)
    #define VERIFY(expr) expr
    #define VERIFYNOT(expr) expr
    #define VERIFYIS(expr,val) expr
    #define VERIFYISNOT(expr,val) expr
#endif
#define FATAL(comment) __TRAP; AssertHandler(__FILE__,__LINE__,comment)

void* /*mem*/ CDECL DebugHeap_malloc(size_t size,const char* file,int line);
void CDECL DebugHeap_free(void* mem);
void* /*newMem*/ CDECL DebugHeap_realloc(void* oldMem,size_t newSize,
    const char* file,int line);
char* CDECL DebugHeap_strdup(const char* str,const char* file,int line);
wchar_t* CDECL DebugHeap_wcsdup(const wchar_t* str,const char* file,int line);
void* CDECL operator new(size_t size,const char* file,int line);
void* CDECL operator new[](size_t size,const char* file,int line);
void* CDECL operator new(size_t size);
void* CDECL operator new[](size_t size);
void CDECL operator delete(void*);
void CDECL operator delete[](void*);
inline void CDECL operator delete(void*m,const char*,int) { operator delete(m); }
inline void CDECL operator delete[](void*m,const char*,int) { operator delete[](m); }

#ifdef DEBUGHEAP
    #define heapnew new(__FILE__,__LINE__)
    #define malloc(size) DebugHeap_malloc(size,__FILE__,__LINE__)
    #define free(mem) DebugHeap_free(mem)
    #define realloc(mem,size) DebugHeap_realloc(mem,size,__FILE__,__LINE__)
    #define strdup(str) DebugHeap_strdup(str,__FILE__,__LINE__)
    #define wcsdup(str) DebugHeap_wcsdup(str,__FILE__,__LINE__)
    void CDECL DebugHeapCheck(void);
    void CDECL DebugHeapIgnoreLeak(void*);
#else
    #define heapnew new
    inline void DebugHeapCheck(void) { ; }
    inline void DebugHeapIgnoreLeak(void*) { ; }
#endif

extern void (CDECL*traceHandler)(const char* format,...);
#ifdef DEBUGTRACES
#define DEBUGTRACE(args) traceHandler args
#else
#define DEBUGTRACE(args)
#endif

#define C_ASSERT(exp) typedef char __C_ASSERT__[(exp)?1:-1]

// make sure wchar macros look correct for size and sign
C_ASSERT(sizeof(wchar_t)*8 == WCHAR_BITS);
C_ASSERT(static_cast<INT64>(WCHAR_MAX) == static_cast<INT64>(
    static_cast<wchar_t>(WCHAR_MAX)));
C_ASSERT(static_cast<INT64>(WCHAR_MIN) == static_cast<INT64>(
    static_cast<wchar_t>(WCHAR_MIN)));

// thread local storage
int/*slot*/ CDECL TlsReserve(void);
void CDECL TlsSet(int slot,const void* data);
void* /*data*/ CDECL TlsGet(int slot);

// serialization primitive, implemented as kernel spinlock or win32 critical
// section, non recursive and unsafe to call system while locked
class SPINLOCK
{
private:
    void* owner;
    void* reserved[(sizeof(int)*2+sizeof(void*)*6)/sizeof(void*)];
public:
    SPINLOCK(void);
    ~SPINLOCK(void);
    void Lock(void);
    void Unlock(void);
    bool IsLocked(void);
};
// serialization primitive, implemented as kernel fast mutex or win32 critical
// section or pthread mutex, treat as non recursive and will block APC's
class MUTEX
{
private:
    void* owner;
    void* reserved[(sizeof(int)*2+sizeof(void*)*6)/sizeof(void*)];
public:
    MUTEX(void);
    ~MUTEX(void);
    void Lock(void);
    void Unlock(void);
    bool IsLocked(void);
};

// interlocked arithmetic primitive
struct INTERLOCK
{
#ifdef _WIN32
    // windows supports interlocked arithmetic API's so don't need mutex
    static int/*newValue*/ CDECL IntInc(int*);
    static int/*newValue*/ CDECL IntDec(int*);
    static int/*oldValue*/ CDECL IntExc(int*,int);
    static void* /*oldValue*/ CDECL PtrExc(void**,void*);
#else
    // *nix platforms have some non portable atomic stuff but with less
    // functionality than windows, use mutex to match functionality
private:
    MUTEX mutex;
public:
    int/*newValue*/ IntInc(int*);
    int/*newValue*/ IntDec(int*);
    int/*oldValue*/ IntExc(int*,int);
    void* /*oldValue*/ PtrExc(void**,void*);
#endif
};
// deprecated interlocked arithmetic
inline int/*newValue*/ LockedIntInc(int* loc) { return INTERLOCK::IntInc(loc); }
inline int/*newValue*/ LockedIntDec(int* loc) { return INTERLOCK::IntDec(loc); }
inline int/*oldValue*/ LockedIntExc(int* loc,int val) { return INTERLOCK::IntExc(loc,val); }
inline void* /*oldValue*/ LockedPtrExc(void** loc,void* val) { return INTERLOCK::PtrExc(loc,val); }


#endif
#endif
//----------------------------------------------------------------------------
// (C) Copyright 2004 McAfee Inc. All Rights Reserved
//----------------------------------------------------------------------------
