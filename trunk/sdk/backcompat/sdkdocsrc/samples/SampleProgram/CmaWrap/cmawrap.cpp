//----------------------------------------------------------------------------
/* Copyright (C) 2005 McAfee, Inc.  All rights reserved. */
//----------------------------------------------------------------------------
// file name:   cmawrap.cpp
// created:     2003.09.10
//   Rework of 75% of the logic that used to be in cmaschediface.cpp.
//   Changes:
//   -  Thread safe. Multiple interfaces can exist and be used concurrently.
//   -  Aliases type definitions so client doesn't have to include the EPO
//      headers and all they pull in.
//   -  Client registry access removed, must be done outside of this module.
//   -  Doesn't use ATL string class. Doesn't require exception handling. (May
//      still need to add try frames around calls to CMA to catch CMA or RPC
//      exceptions?).
//----------------------------------------------------------------------------

// Adapted from the VirusScan cmawrap.cpp file for general use in the MgmtSDK

#include "stdafx.h"
#include "compiler.h"
#include <stdio.h>
#include <objbase.h>
#include "strprims.h"
#include "regprims.h"

#include "..\..\..\Generated\SecureFrameworkFactory.h"
#include "..\..\..\Generated\ComponentFrameworkCallback.h"
#include "..\..\..\Include\naSubSystems.h"
#include "..\..\..\Generated\Scheduler.h"
#include "..\..\..\Generated\Logging.h"
#include "..\..\..\Generated\UpdateSubSys.h"
#include "..\..\..\Include\naLoggerObj.h"

#include "cmawrap.h"

static const int DEFAULTTASKSTARTDELAY = 5/*minutes*/;
static const int DEFAULTMISSEDTASKDELAY = 5/*minutes*/;

// BSTR initialization and cleanup is tedious and error prone. Use of the MFC
// or ATL BSTR pulls in exception handling and dependencies. This minimal BSTR
// wrapper deals with the trivial BSTR handling problems.
class MiniStr
{
private:
    BSTR bs;
public:
    MiniStr(void)                { bs = 0; }
    MiniStr(const wchar_t* sz)   { bs = SysAllocString(sz); }
    MiniStr(MiniStr& m)          { bs = SysAllocString(m.Sz()); }
    void operator=(const wchar_t* sz) { SysReAllocString(&bs,sz); }
    ~MiniStr(void)               { SysFreeString(bs); }
    const wchar_t* Sz(void)      { return bs?bs:L""; }
    BSTR Bstr(void)              { return bs; }
    BSTR* Recieve(void)          { SysFreeString(bs); bs = 0; return &bs; }
    BSTR* Modify(void)           { return &bs; }
    size_t Len(void)             { return SysStringLen(bs); }
};

void GetEpoSoftwareId(const wchar_t* prefix,wchar_t* softwareId,size_t
    maxChars)
{
    ASSERT(prefix && softwareId && maxChars);
    softwareId[0] = 0;
    HKEY key = RegKeyOpenRead(HKEY_LOCAL_MACHINE,L"SOFTWARE\\"
        L"Network Associates\\ePolicy Orchestrator\\Application Plugins");
    int index = 0;
    while(!softwareId[0] && RegKeyEnum(key,index,softwareId,maxChars))
    {
        if(Strscmpi(softwareId,prefix) != 0)
        {
            softwareId[0] = 0;
        }
        index ++;
    }
    if(!softwareId[0])
    {
        Stradd(softwareId,maxChars,prefix);
    }
    RegKeyClose(key);
}

class CmaWrapImp: public CmaWrap
{
public:
    int references;
    wchar_t* clientId;
    wchar_t* fullClientId;
    UINT comThreadId;
    IFrameworkFactory* m_CMAFWFactory;
    ISchedule* m_pScheduler;
    ILogger* m_pLogger;
    IMcAfeeUpdate2* m_pUpdater;
    class CmaCallback;
    CmaCallback* cmaCallback;
    CmaWrapNotify* notify;

    const wchar_t* GetFullClientId(void)
    {
        if(!fullClientId)
        {
            const size_t maxScratchChars = 100;
            wchar_t scratch[maxScratchChars] = L"";
            GetEpoSoftwareId(clientId,scratch,maxScratchChars);
            ASSERT(scratch[0]);
            fullClientId = Strdup(scratch);
        }
        return fullClientId?fullClientId:clientId;
    }

    class CmaTaskIteratorImp;
    friend class CmaTaskIteratorImp;
    class CmaTaskIteratorImp: public CmaTaskIterator
    {
        friend class CmaWrapImp;
        int references;
        wchar_t* softwareId;
        wchar_t* fullSoftwareId;
        IEnumTask* pEnum;
        CmaWrapImp* cmaWrap;
        bool first;
        bool finished;

        CmaTaskIteratorImp::~CmaTaskIteratorImp(void)
        {
            ASSERT(!references);
            if(pEnum)
            {
                pEnum->FreeEnumObj();
                pEnum->Release();
            }
            if(cmaWrap)
            {
                cmaWrap->Release();
            }
            Strfree(softwareId);
        }

    public:
        CmaTaskIteratorImp::CmaTaskIteratorImp(CmaWrapImp* _cmaWrap,
            const wchar_t* _softwareId)
        {
            references = 1;
            const size_t maxScratchChars = 100;
            wchar_t scratch[maxScratchChars] = L"";
            if(_softwareId && _softwareId[0])
            {
                GetEpoSoftwareId(_softwareId,scratch,maxScratchChars);
            }
            softwareId = Strdup(scratch);
            pEnum = 0;
            cmaWrap = _cmaWrap;
            if(cmaWrap)
            {
                cmaWrap->AddRef();
            }
            first = 1;
            finished = 0;
        }

        void CmaTaskIteratorImp::Reset(void)
        {
            if(pEnum)
            {
                pEnum->Release();
                pEnum = 0;
            }
            if(cmaWrap)
            {
                ASSERT(cmaWrap->comThreadId == GetCurrentThreadId());
                if(!SUCCEEDED(cmaWrap->m_pScheduler->EnumAllTask(&pEnum)))
                {
                    pEnum = 0;
                }
            }
        }

        bool/*success*/ CmaTaskIteratorImp::Init(void)
        {
            if(softwareId && cmaWrap)
            {
                Reset();
            }
            return !!pEnum;
        }

        void CmaTaskIteratorImp::AddRef(void)
        {
            ASSERT(references);
            INTERLOCK::IntInc(&references);
        }

        void CmaTaskIteratorImp::Release(void)
        {
            ASSERT(references);
            if(!INTERLOCK::IntDec(&references))
            {
                delete this;
            }
        }

        bool/*success*/ CmaTaskIteratorImp::Next(wchar_t* taskGuid,
            size_t maxTaskGuidChars,wchar_t* taskName,
            size_t maxTaskNameChars,wchar_t* taskType,
            size_t maxTaskTypeChars)
        {
            bool success = 0;
            ASSERT(cmaWrap->comThreadId == GetCurrentThreadId());
            do
            {
                MiniStr strTaskGuid;
                if(pEnum && SUCCEEDED(first?pEnum->GetFirst(strTaskGuid.
                    Recieve()):pEnum->GetNext(strTaskGuid.Recieve())))
                {
                    first = 0;
                    ITask* task;
                    if(SUCCEEDED(cmaWrap->m_pScheduler->GetTask(strTaskGuid.
                        Bstr(),&task)) && task)
                    {
                        MiniStr strTaskCreator;
                        MiniStr strTaskSoftware;
                        MiniStr strTaskType;
                        MiniStr strTaskName;
                        if(SUCCEEDED(task->GetCreatorSoftwareID(
                            strTaskCreator.Recieve())) && SUCCEEDED(task->
                            GetOwnerSoftwareID(strTaskSoftware.Recieve())) &&
                            SUCCEEDED(task->GetTaskType(strTaskType.
                            Recieve())) && SUCCEEDED(task->GetTaskName(strTaskName.
                            Recieve())))
                        {
                            if(Strscmpi(strTaskCreator.Sz(),cmaWrap->clientId)
                                == 0 && Strscmpi(strTaskSoftware.Sz(),
                                softwareId) == 0)
                            {
                                success = 1;
                            }
                        }
                        if(success)
                        {
                            if(taskGuid && maxTaskGuidChars)
                            {
                                taskGuid[0] = 0;
                                Stradd(taskGuid,maxTaskGuidChars,strTaskGuid.
                                    Sz());
                            }
                            if(taskType && maxTaskTypeChars)
                            {
                                taskType[0] = 0;
                                Stradd(taskType,maxTaskTypeChars,strTaskType.
                                    Sz());
                            }
                            if(taskName && maxTaskNameChars)
                            {
                                taskName[0] = 0;
                                Stradd(taskName,maxTaskNameChars,strTaskName.
                                    Sz());
                            }
                        }
                        task->Release();
                    }
                }
                else
                {
                    finished = 1;
                }
            } while(!finished && !success);
            return success;
        }
    };

    bool/*success*/ GetCmaTaskId(const wchar_t* taskId,wchar_t* cmaTaskId,
        size_t maxCmaTaskIdChars)
    {
        bool success = 0;
        ASSERT(cmaTaskId && maxCmaTaskIdChars);
        if(taskId && taskId[0] && cmaTaskId && maxCmaTaskIdChars)
        {
            cmaTaskId[0] = 0;
            ITask* task;
            MiniStr strTaskGuid(taskId);
            ASSERT(comThreadId == GetCurrentThreadId());
            if(SUCCEEDED(m_pScheduler->GetTask(strTaskGuid.Bstr(),&task)) &&
                task)
            {
                success = 1;
                Stradd(cmaTaskId,maxCmaTaskIdChars,taskId);
                task->Release();
            }
        }
        return success;
    }

    friend class CmaCallback;
    class CmaCallback: public IFrameworkCallback
    {
        friend class CmaWrapImp;
        int references;
        MUTEX mutex;
        CmaWrapImp* cmaWrap;

        CmaCallback::~CmaCallback(void)
        {
            ASSERT(!references);
            ASSERT(!cmaWrap);
        }

        CmaWrapImp* CmaCallback::RefCmaWrap(CmaWrapNotify** notify)
        {
            if(notify)
            {
                *notify = 0;
            }
            mutex.Lock();
            CmaWrapImp* tempCmaWrap = cmaWrap;
            if(tempCmaWrap)
            {
                tempCmaWrap->AddRef();
                if(notify && tempCmaWrap->notify)
                {
                    *notify = tempCmaWrap->notify;
                    (*notify)->AddRef();
                }
            }
            mutex.Unlock();
            return tempCmaWrap;
        }

    public:
        CmaCallback::CmaCallback(CmaWrapImp* _cmaWrap)
        {
            references = 1;
            ASSERT(cmaWrap);
            cmaWrap = _cmaWrap;
        }

        ULONG STDMETHODCALLTYPE CmaCallback::AddRef(void)
        {
            return INTERLOCK::IntInc(&references);
        }

        HRESULT STDMETHODCALLTYPE CmaCallback::QueryInterface(REFIID riid,
            void** ppvObject)
        {
            if(riid == IID_IUnknown)
            {
                *ppvObject= static_cast<IUnknown*>(this);
                AddRef();
            }
            else if(riid == __uuidof(IFrameworkCallback))
            {
                *ppvObject= static_cast<IFrameworkCallback*>(this);
                AddRef();
            }
            else
            {
                // Don't respect IDispatch
                // or anything else
                *ppvObject = 0;
                return E_NOINTERFACE;
            }
            return S_OK;
        }

        ULONG STDMETHODCALLTYPE CmaCallback::Release(void)
        {
            ASSERT(references);
            int result = INTERLOCK::IntDec(&references);
            if(!result)
            {
                delete this;
            }
            return static_cast<UINT>(result);
        }

        HRESULT STDMETHODCALLTYPE CmaCallback::GetTypeInfoCount(UINT* pctinfo)
        {
            return E_NOTIMPL;
        }

        HRESULT STDMETHODCALLTYPE CmaCallback::GetTypeInfo(UINT iTInfo,LCID
            lcid,ITypeInfo** ppTInfo)
        {
            return E_NOTIMPL;
        }

        HRESULT STDMETHODCALLTYPE CmaCallback::GetIDsOfNames(REFIID riid,
            LPOLESTR* rgszNames,UINT cNames,LCID lcid,DISPID* rgDispId)
        {
            return E_NOTIMPL;
        }

        HRESULT STDMETHODCALLTYPE CmaCallback::Invoke(DISPID dispIdMember,
            REFIID riid,LCID lcid,WORD wFlags,DISPPARAMS* pDispParams,VARIANT*
            pVarResult,EXCEPINFO* pExcepInfo,UINT* puArgErr)
        {
            return E_NOTIMPL;
        }

        HRESULT STDMETHODCALLTYPE CmaCallback::OnFrameworkTerminate(void)
        {
            CmaWrapNotify* notify = 0;
            CmaWrapImp* cmaWrap = RefCmaWrap(&notify);
            if(cmaWrap)
            {
                cmaWrap->Release();
            }
            if(notify)
            {
                notify->CmaWrapNotifyTerminate();
            }
            return S_OK;
        }

        HRESULT STDMETHODCALLTYPE CmaCallback::OnFrameworkAvailable(void)
        {
            return S_OK;
        }
    };

    ~CmaWrapImp(void)
    {
        ASSERT(!references);
        ASSERT(!comThreadId || comThreadId == GetCurrentThreadId());
        if(m_CMAFWFactory)
        {
            m_CMAFWFactory->Release();
        }
        if(m_pScheduler)
        {
            m_pScheduler->Release();
        }
        if(m_pLogger)
        {
            m_pLogger->Release();
        }
        if(m_pUpdater)
        {
            m_pUpdater->Release();
        }
        if(cmaCallback)
        {
            cmaCallback->Release();
        }
        if(notify)
        {
            notify->Release();
        }
        if(comThreadId)
        {
            CoUninitialize();
        }
        Strfree(clientId);
        Strfree(fullClientId);
    }

public:
    CmaTaskIterator* IterateTasks(const wchar_t* softwareId)
    {
        CmaTaskIteratorImp* iterator = heapnew CmaTaskIteratorImp(this,
            softwareId);
        if(iterator && !iterator->Init())
        {
            iterator->Release();
            iterator = 0;
        }
        return iterator;
    }

    void FreeAppConfig(CmaAppConfig* appConfig,int appConfigCount)
    {
     /* Use this to free all of the szValue members of the GetTaskInfo function's
        appConfig return value. */
        for (int n = 0; n < appConfigCount; ++n)
        {
            Strfree(appConfig[n].szValue);
        }
    }

    bool/*success*/ GetTaskInfo(const wchar_t* taskId,wchar_t* taskName,
        size_t maxTaskNameChars,wchar_t* taskType,size_t maxTaskTypeChars,
        int* taskStatus,long* taskNextTime,long* taskLastTime,
        CmaTaskScheduleInfo* taskSchedInfo, CmaAppConfig* appConfig,
        int appConfigCount)
    {
        ASSERT(comThreadId == GetCurrentThreadId());
        bool success = 0;
        ITask* task;
        MiniStr strCmaTaskId(taskId);
        if(SUCCEEDED(m_pScheduler->GetTask(strCmaTaskId.Bstr(),&task)) &&
            task)
        {
            success = 1;
            if(taskType && maxTaskTypeChars)
            {
                taskType[0] = 0;
                MiniStr strTaskType;
                if(SUCCEEDED(task->GetTaskType(strTaskType.Recieve())))
                {
                    Stradd(taskType,maxTaskTypeChars,strTaskType.Sz());
                }
            }
            if(taskName && maxTaskNameChars)
            {
                taskName[0] = 0;
                MiniStr strTaskName;
                if(SUCCEEDED(task->GetTaskName(strTaskName.Recieve())))
                {
                    Stradd(taskName,maxTaskNameChars,strTaskName.Sz());
                }
            }
            if(taskStatus)
            {
                if(!SUCCEEDED(m_pScheduler->GetTaskStatus(strCmaTaskId.Bstr(),
                    taskStatus)))
                {
                    *taskStatus = 0;
                }
            }
            if(taskNextTime)
            {
                if(!SUCCEEDED(m_pScheduler->GetNextRunTime(strCmaTaskId.
                    Bstr(),taskNextTime)))
                {
                    *taskNextTime = 0;
                }
            }
            if(taskLastTime)
            {
                if(!SUCCEEDED(m_pScheduler->GetLastRunTime(strCmaTaskId.
                    Bstr(),taskLastTime)))
                {
                    *taskLastTime = 0;
                }
            }
            if(taskSchedInfo)
            {
                memset(taskSchedInfo,0,sizeof(TASKSCHEDULE));
                ITrigger* pTrigger;
                int i;
                short s;
                long l;
                if(SUCCEEDED(task->GetTaskTrigger(&pTrigger)) &&
                    pTrigger)
                {
                    i = 0; pTrigger->get_GMTTime(&i); taskSchedInfo->bGMTTime = !!i;
                    l = 0; pTrigger->get_StartHour(&l); taskSchedInfo->nStartHour = l;
                    l = 0; pTrigger->get_StartMinute(&l); taskSchedInfo->nStartMinute = l;
                    l = 0; pTrigger->get_StartDay(&l); taskSchedInfo->nStartDay = l;
                    s = 0; pTrigger->get_StartMonth(&s); taskSchedInfo->nStartMonth = s;
                    l = 0; pTrigger->get_StartYear(&l); taskSchedInfo->nStartYear = l;
                    i = 0; pTrigger->get_StopDateValid(&i); taskSchedInfo->bStopDateValid= !!i;
                    l = 0; pTrigger->get_StopDay(&l); taskSchedInfo->nStopDay = l;
                    l = 0; pTrigger->get_StopMonth(&l); taskSchedInfo->nStopMonth = l;
                    l = 0; pTrigger->get_StopYear(&l); taskSchedInfo->nStopYear = l;
                    i = 0; pTrigger->get_Repeatable(&i); taskSchedInfo->bRepeatable = !!i;
                    s = 0; pTrigger->get_RepeatOption(&s); taskSchedInfo->eRepeatOption = s;
                    l = 0; pTrigger->get_RepeatInterval(&l); taskSchedInfo->nRepeatInterval = l;
                    s = 0; pTrigger->get_UntilOption(&s); taskSchedInfo->eUntilOption = s;
                    l = 0; pTrigger->get_UntilHour(&l); taskSchedInfo->nUntilHour = l;
                    l = 0; pTrigger->get_UntilMinute(&l); taskSchedInfo->nUntilMinute = l;
                    l = 0; pTrigger->get_UntilDuration(&l); taskSchedInfo->nUntilDuration = l;
                    i = 0; pTrigger->get_RandomizationEnabled(&i); taskSchedInfo->bRandomizationEnabled = !!i;
                    l = 0; pTrigger->get_RandomizationWndMins(&l); taskSchedInfo->nRandomizationWndMins = l;
                    i = 0; pTrigger->get_OnceADayEnabled(&i); taskSchedInfo->bOnceADayEnabled = !!i;
                    l = DEFAULTTASKSTARTDELAY; pTrigger->get_AtStartupDelayMinutes(&l); taskSchedInfo->uStartupDelay = l;
                    l = 0; pTrigger->get_RepeatDays(&l); taskSchedInfo->Type.Daily_nRepeatDays = l;
                    l = 0; pTrigger->get_RepeatWeeks(&l); taskSchedInfo->Type.Weekly_nRepeatWeeks = l;
                    l = 0; pTrigger->get_maskDaysOfWeek(&l); taskSchedInfo->Type.Weekly_maskDaysOfWeek = l;
                    s = 0; pTrigger->get_MonthlyOption(&s); taskSchedInfo->Type.Monthly_eMonthlyOption = s;
                    l = 0; pTrigger->get_DayNumOfMonth(&l); taskSchedInfo->Type.Monthly_nDayNumOfMonth = l;
                    l = 0; pTrigger->get_WeekNumOfMonth(&l); taskSchedInfo->Type.Monthly_nWeekNumOfMonth = l;
                    l = 0; pTrigger->get_DayOfWeek(&l); taskSchedInfo->Type.Monthly_nDayOfWeek = l;
                    l = 0; pTrigger->get_maskMonthsOfYear(&l); taskSchedInfo->Type.Monthly_maskMonthsOfYear = l;
                    l = 0; pTrigger->get_IdleMinutes(&l); taskSchedInfo->Type.Idle_nIdleMinutes = l;
                    C_ASSERT(sizeof(SCHEDULETYPE) == sizeof(i));
                    i = 0; pTrigger->get_eScheduleType(reinterpret_cast<SCHEDULETYPE*>(&i)); taskSchedInfo->eScheduleType = i;
                    pTrigger->Release();
                }
                // Get ITask info
                i = 0; task->GetTaskEnabled(&i); taskSchedInfo->bEnabled = !!i;
                i = 0; task->GetRunIfMissed(&i); taskSchedInfo->bRunIfMissed = !!i;
                l = DEFAULTMISSEDTASKDELAY; task->GetRunIfMissedDelayMins(&l); taskSchedInfo->uMissedTaskDelay = l;
                l = 0; task->GetStopAfterMinutes(&l); taskSchedInfo->dwStopAfterMinutes = l;
                taskSchedInfo->bStopScanPeriod = !!taskSchedInfo->dwStopAfterMinutes;

                for (int n = 0; n < appConfigCount; ++n)
                {
                    MiniStr strSection(appConfig[n].szSection);
                    MiniStr strSetting(appConfig[n].szSetting);
                    MiniStr strValue;
                    
                    if(SUCCEEDED(task->GetAppConfig(strSection.Bstr(),strSetting.Bstr(),strValue.Recieve())))
                    {
                        appConfig[n].szValue = Strdup(strValue.Sz());
                    }
                }
            }
            task->Release();
        }
        return success;
    }

    bool/*success*/ AddOrUpdateTask(const wchar_t* taskId,const wchar_t* taskName,
        const wchar_t* taskType,const CmaTaskScheduleInfo* taskSchedInfo,CmaAppConfig*
        appConfig,int appConfigCount)
    {
        bool success = 1;
        ASSERT(comThreadId == GetCurrentThreadId());
        bool newTask = 0;
        ITask* task = 0;
        MiniStr strCmaTaskId(taskId);
        MiniStr strClientId(GetFullClientId());
        if(!SUCCEEDED(m_pScheduler->ModifyTask(strCmaTaskId.Bstr(),
            strClientId.Bstr(),&task)) || !task)
        {
//            printf("WARNING: Unable to modify existing CMA task so trying"
//                " to delete and recreate.\n");
            task = 0;
            if(!SUCCEEDED(m_pScheduler->DeleteTask(strCmaTaskId.Bstr(),
                strClientId.Bstr())))
            {
//                printf("ERROR: Unable to delete existing CMA task.\n");
            }
        }
        if(!task && success)
        {
            strCmaTaskId = taskId;
            if(!SUCCEEDED(m_pScheduler->CreateTask(strCmaTaskId.Modify(),
                &task)))
            {
                task = 0;
            }
            if(task)
            {
                newTask = 1;
                success = !!SUCCEEDED(task->SetCreatorSoftwareID(strClientId.Bstr()));
                success = success && SUCCEEDED(task->SetOwnerSoftwareID(strClientId.Bstr()));
                MiniStr strAllPlatforms(L"*");
                success = success && SUCCEEDED(task->SetPlatforms(strAllPlatforms.Bstr()));
                MiniStr strTaskType(taskType);
                success = success && SUCCEEDED(task->SetTaskType(strTaskType.Bstr()));
            }
        }
        if(task && success)
        {
            MiniStr strTaskName(taskName);
            success = success && SUCCEEDED(task->SetTaskName(strTaskName.Bstr()));
        }
        ITrigger* pTrigger = 0;
        if(task && !SUCCEEDED(task->GetTaskTrigger(&pTrigger)))
        {
            pTrigger = 0;
            printf("ERROR: Unable to instantiate CMA trigger interface.\n");
        }
        success = success && task && pTrigger;

        success = success && SUCCEEDED(task->SetTaskEnabled(taskSchedInfo->bEnabled));
        success = success && SUCCEEDED(pTrigger->put_GMTTime(taskSchedInfo->bGMTTime));
        success = success && SUCCEEDED(pTrigger->put_StartHour(taskSchedInfo->nStartHour));
        success = success && SUCCEEDED(pTrigger->put_StartMinute(taskSchedInfo->nStartMinute));
        success = success && SUCCEEDED(pTrigger->put_StartDay(taskSchedInfo->nStartDay));
        success = success && SUCCEEDED(pTrigger->put_StartMonth(static_cast<short>(taskSchedInfo->nStartMonth)));
        success = success && SUCCEEDED(pTrigger->put_StartYear(taskSchedInfo->nStartYear));
        success = success && SUCCEEDED(pTrigger->put_StopDateValid(taskSchedInfo->bStopDateValid));
        success = success && SUCCEEDED(pTrigger->put_StopDay(taskSchedInfo->nStopDay));
        success = success && SUCCEEDED(pTrigger->put_StopMonth(taskSchedInfo->nStopMonth));
        success = success && SUCCEEDED(pTrigger->put_StopYear(taskSchedInfo->nStopYear));
        success = success && SUCCEEDED(pTrigger->put_Repeatable(taskSchedInfo->bRepeatable));
        success = success && SUCCEEDED(pTrigger->put_RepeatOption(static_cast<short>(taskSchedInfo->eRepeatOption)));
        success = success && SUCCEEDED(pTrigger->put_RepeatInterval(taskSchedInfo->nRepeatInterval));
        success = success && SUCCEEDED(pTrigger->put_UntilOption(static_cast<short>(taskSchedInfo->eUntilOption)));
        success = success && SUCCEEDED(pTrigger->put_UntilHour(taskSchedInfo->nUntilHour));
        success = success && SUCCEEDED(pTrigger->put_UntilMinute(taskSchedInfo->nUntilMinute));
        success = success && SUCCEEDED(pTrigger->put_UntilDuration(taskSchedInfo->nUntilDuration));
        success = success && SUCCEEDED(pTrigger->put_RandomizationEnabled(taskSchedInfo->bRandomizationEnabled ));
        success = success && SUCCEEDED(pTrigger->put_RandomizationWndMins(taskSchedInfo->nRandomizationWndMins));
        success = success && SUCCEEDED(pTrigger->put_OnceADayEnabled(taskSchedInfo->bOnceADayEnabled));
        success = success && SUCCEEDED(pTrigger->put_AtStartupDelayMinutes(taskSchedInfo->uStartupDelay));
        success = success && SUCCEEDED(pTrigger->put_RepeatDays(taskSchedInfo->Type.Daily_nRepeatDays));
        success = success && SUCCEEDED(pTrigger->put_RepeatWeeks(taskSchedInfo->Type.Weekly_nRepeatWeeks));
        success = success && SUCCEEDED(pTrigger->put_maskDaysOfWeek(taskSchedInfo->Type.Weekly_maskDaysOfWeek));
        success = success && SUCCEEDED(pTrigger->put_MonthlyOption(static_cast<short>(taskSchedInfo->Type.Monthly_eMonthlyOption)));
        success = success && SUCCEEDED(pTrigger->put_DayNumOfMonth(taskSchedInfo->Type.Monthly_nDayNumOfMonth));
        success = success && SUCCEEDED(pTrigger->put_WeekNumOfMonth(taskSchedInfo->Type.Monthly_nWeekNumOfMonth));
        success = success && SUCCEEDED(pTrigger->put_DayOfWeek(taskSchedInfo->Type.Monthly_nDayOfWeek));
        success = success && SUCCEEDED(pTrigger->put_maskMonthsOfYear(taskSchedInfo->Type.Monthly_maskMonthsOfYear));
        success = success && SUCCEEDED(pTrigger->put_IdleMinutes(taskSchedInfo->Type.Idle_nIdleMinutes));
        C_ASSERT(sizeof(SCHEDULETYPE) == sizeof(int));
        success = success && SUCCEEDED(pTrigger->put_eScheduleType(static_cast<SCHEDULETYPE>(taskSchedInfo->eScheduleType)));
        success = success && SUCCEEDED(task->SetRunIfMissed(taskSchedInfo->bRunIfMissed));
        success = success && SUCCEEDED(task->SetRunIfMissedDelayMins(taskSchedInfo->uMissedTaskDelay));
        success = success && SUCCEEDED(task->SetStopAfterMinutes(taskSchedInfo->dwStopAfterMinutes));
        for (int n = 0; n < appConfigCount; ++n)
        {
            MiniStr strSection(appConfig[n].szSection);
            MiniStr strSetting(appConfig[n].szSetting);
            MiniStr strValue(appConfig[n].szValue);
            success = success && SUCCEEDED(task->SetAppConfig(strSection.Bstr(),
                strSetting.Bstr(),strValue.Bstr()));
        }
        // use specialized CMA method to set password so it will
        // be encrypted in task file
        MiniStr strRunAsUser(taskSchedInfo->szRunAsUser);
        MiniStr strRunAsDomain(taskSchedInfo->szRunAsDomain);
        MiniStr strRunAsPassword(taskSchedInfo->szRunAsPassword);
        success = success && SUCCEEDED(task->SetAccountInformation(
            strRunAsUser.Bstr(),strRunAsDomain.Bstr(),strRunAsPassword.Bstr()));

        if(success)
        {
            ASSERT(task);
            if(newTask)
            {
                // Add new task to cma scheduler.
                if(!SUCCEEDED(m_pScheduler->AddTask(strCmaTaskId.Bstr())))
                {
                    printf("ERROR: Unable to add task to CMA scheduler.\n");
                    success = 0;
                }
            }
            else
            {
                // For whatever reason, CMA only expects save to be called
                // when editing an existing task. Pretty sure CMA has already
                // modified the ini file it uses to hold the data.
                if(!SUCCEEDED(task->Save()))
                {
                    printf("ERROR: Unable to save existing task to CMA.\n");
                    success = 0;
                }
            }
        }
        else if(pTrigger && task)
        {
            printf("ERROR: Unable to save all settings to CMA task.\n");
        }

        if(pTrigger)
        {
            pTrigger->Release();
        }
        if(task)
        {
            task->Release();
        }
        return success;
    }

    bool/*success*/ DeleteTask(const wchar_t* taskId)
    {
        ASSERT(comThreadId == GetCurrentThreadId());
        bool success = 0;
        MiniStr strCmaTaskId(taskId);
        MiniStr strClientId(GetFullClientId());
        if(SUCCEEDED(m_pScheduler->DeleteTask(strCmaTaskId.Bstr(),
            strClientId.Bstr())))
        {
            success = 1;
        }
        else
        {
            printf("ERROR: Unable to delete CMA task.\n");
        }
        return success;
    }

    int/*result*/ RunUpdateTask(const wchar_t* taskId)
    {
        MiniStr strClientId(GetFullClientId());
        MiniStr strTaskGuid(taskId);
        return static_cast<int>(m_pUpdater->RunUpdateTask(strClientId.Bstr(),
            strTaskGuid.Bstr()));
    }

    int/*result*/ RunPullTask(const wchar_t* taskId,const wchar_t* dest)
    {
        MiniStr strClientId(GetFullClientId());
        MiniStr strTaskGuid(taskId);
        MiniStr strEmpty(L"");
        MiniStr strDest(dest);
        return static_cast<int>(m_pUpdater->RunPullTask(strClientId.Bstr(),
            strTaskGuid.Bstr(),strEmpty.Bstr(),strDest.Bstr()));
    }

    int/*result*/ RunUpdateNow(bool quiet)
    {
        MiniStr strClientId(GetFullClientId());
        long cookie = 0;
        m_CMAFWFactory->GetCookie(&cookie);
        return static_cast<int>(m_pUpdater->RunUpdateNow(strClientId.
            Bstr(),cookie,quiet));
    }

    int/*result*/ SetUpdateNowOption(const wchar_t* section,const
        wchar_t* setting,const wchar_t* value)
    {
        MiniStr strClientId(GetFullClientId());
        MiniStr strSection(section);
        MiniStr strSetting(setting);
        MiniStr strValue(value);
        return static_cast<int>(m_pUpdater->SetUpdateNowOption(strClientId.Bstr(),
            strSection.Bstr(), strSetting.Bstr(), strValue.Bstr()));
    }
    
    int/*result*/ SetUpdateTaskOption(const wchar_t* taskId,const
        wchar_t* section,const wchar_t* setting,const wchar_t* value)
    {
        MiniStr strClientId(GetFullClientId());
        MiniStr strTaskId(taskId);
        MiniStr strSection(section);
        MiniStr strSetting(setting);
        MiniStr strValue(value);
        return static_cast<int>(m_pUpdater->SetUpdateTaskOption(strClientId.Bstr(),
            strTaskId.Bstr(), strSection.Bstr(), strSetting.Bstr(), strValue.Bstr()));
    }

    int/*result*/ GetUpdateState(int* _updateState)
    {
        UPDATE_STATE updateState;
        int result = static_cast<int>(m_pUpdater->get_UpdateState(
            &updateState));
        if(_updateState)
        {
            *_updateState = updateState;
        }
        return result;
    }

    int/*result*/ GetPullTaskState(int* _updateState)
    {
        UPDATE_STATE updateState;
        int result = static_cast<int>(m_pUpdater->get_PullTaskState(
            &updateState));
        if(_updateState)
        {
            *_updateState = updateState;
        }
        return result;
    }

    void StopUpdate(void)
    {
        m_pUpdater->StopUpdate();
    }

    void StopPullTask(void)
    {
        m_pUpdater->StopPullTask();
    }

    int/*result*/ RunRollbackUpdate(void)
    {
        MiniStr strClientId(GetFullClientId());
        long cookie = 0;
        m_CMAFWFactory->GetCookie(&cookie);
        return static_cast<int>(m_pUpdater->RunRollbackUpdate(strClientId.
            Bstr(),cookie));
    }

    int/*result*/ ImportSitelist(const wchar_t* siteListFile)
    {
        MiniStr strSiteListFile(siteListFile);
        return static_cast<int>(m_pUpdater->ImportSitelist(strSiteListFile.
            Bstr()));
    }

    CmaWrapImp(const wchar_t* _clientId,CmaWrapNotify* _notify)
    {
        references = 1;
        clientId = Strdup(_clientId);
        fullClientId = 0;
        comThreadId = 0;
        m_CMAFWFactory = 0;
        m_pScheduler = 0;
        m_pLogger = 0;
        m_pUpdater = 0;
        cmaCallback = 0;
        notify = _notify;
        if(notify)
        {
            notify->AddRef();
        }
    }

    bool/*success*/ Init(void)
    {
        bool success = 0;
        ASSERT(!comThreadId && !m_CMAFWFactory && !m_pScheduler &&
            !cmaCallback);
        if(clientId && SUCCEEDED(CoInitializeEx(0,COINIT_MULTITHREADED)))
        {
            comThreadId = GetCurrentThreadId();
            ASSERT(comThreadId);
            cmaCallback = heapnew CmaCallback(this);
            if(cmaCallback)
            {
                // get CMA Framework interface
                if(SUCCEEDED(CoCreateInstance(__uuidof(FrameworkFactory),0,
                    CLSCTX_ALL,__uuidof(IFrameworkFactory),
                    reinterpret_cast<void**>(&m_CMAFWFactory))) &&
                    m_CMAFWFactory)
                {
                    // Register with Framework callback.
                    // TODO: unregister ??
                    if(SUCCEEDED(m_CMAFWFactory->RegisterFrameworkCallback(
                        cmaCallback)))
                    {
                        // Get Scheduler interface
                        const size_t maxIidChars = 40;
                        wchar_t iid[maxIidChars];
                        StringFromGUID2(__uuidof(ISchedule),iid,maxIidChars);
                        iid[maxIidChars-1] = 0;
                        MiniStr strIid(iid);
                        MiniStr strSubSysId(NASUBSYS_SCHEDULE);
                        if(SUCCEEDED(m_CMAFWFactory->GetSubSystemInterface(
                            strSubSysId.Bstr(),strIid.Bstr(),
                            reinterpret_cast<IUnknown**>(&m_pScheduler))) &&
                            m_pScheduler)
                        {
                            // Get the logger interface
                            StringFromGUID2(__uuidof(ILogger),iid,maxIidChars);
                            iid[maxIidChars-1] = 0;
                            strIid = iid;
                            strSubSysId = NASUBSYS_LOGGING;
                            if(SUCCEEDED(m_CMAFWFactory->
                                GetSubSystemInterface(strSubSysId.Bstr(),
                                strIid.Bstr(),reinterpret_cast<IUnknown**>(
                                &m_pLogger))) && m_pLogger)
                            {
                                // Get Update interface
                                StringFromGUID2(__uuidof(IMcAfeeUpdate2),iid,
                                    maxIidChars);
                                strIid = iid;
                                strSubSysId = NASUBSYS_UPDATE;
                                if(SUCCEEDED(m_CMAFWFactory->
                                    GetSubSystemInterface(strSubSysId.Bstr(),
                                    strIid.Bstr(),
                                    reinterpret_cast<IUnknown**>(&m_pUpdater
                                    ))) && m_pUpdater)
                                {
                                    success = 1;
                                }
                            }
                        }
                        else
                        {
                            printf("ERROR: Unable to instantiate CMA"
                                " scheduler interface.\n");
                            m_pScheduler = 0;
                        }
                    }
                    else
                    {
                        printf("ERROR: Unable to register CMA callback.\n");
                    }
                }
                else
                {
                    m_CMAFWFactory = 0;
                    printf("ERROR: Unable to instantiate CMA class"
                        " factory.\n");
                }
            }
        }
        return success;
    }

    void AddRef(void)
    {
        ASSERT(references);
        INTERLOCK::IntInc(&references);
    }

    void Release(void)
    {
        ASSERT(references);
        if(cmaCallback)
        {
            cmaCallback->mutex.Lock();
        }
        bool deleteNow = !INTERLOCK::IntDec(&references);
        if(deleteNow)
        {
            // Undo uncounted circular reference from callback. Callback may
            // still hang around for a while if still referenced by CMA but
            // won't be able to do anything.
            if(cmaCallback)
            {
                ASSERT(cmaCallback->cmaWrap);
                cmaCallback->cmaWrap = 0;
            }
        }
        if(cmaCallback)
        {
            cmaCallback->mutex.Unlock();
        }
        if(deleteNow)
        {
            delete this;
        }
    }
};

extern "C" CmaWrapPtr CDECL CmaWrapCreate(const wchar_t* clientId,
    CmaWrapNotify* notify)
{
    CmaWrapImp* cmaWrap = heapnew CmaWrapImp(clientId,notify);
    if(cmaWrap && !cmaWrap->Init())
    {
        cmaWrap->Release();
        cmaWrap = 0;
    }
    return cmaWrap;
}


//----------------------------------------------------------------------------
// (C) Copyright 2004 Network Associates Inc. All Rights Reserved
//----------------------------------------------------------------------------
