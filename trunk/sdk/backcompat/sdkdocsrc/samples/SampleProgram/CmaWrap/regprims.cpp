//----------------------------------------------------------------------------
/* Copyright (C) 2005 McAfee, Inc.  All rights reserved. */
//----------------------------------------------------------------------------
// filename:    regprims.cpp
// created:     2003.05.15
//----------------------------------------------------------------------------
#include "stdafx.h"
#include "compiler.h"
#include <tchar.h>
#include "strprims.h"
#include "regprims.h"

typedef unsigned short      UINT16, *PUINT16;
typedef unsigned char       UINT8, *PUINT8;

void SidConvertToText(PSID sid,wchar_t* textSid,size_t maxTextSidChars)
{
    ASSERT(IsValidSid(sid));
    ASSERT(textSid && maxTextSidChars > 1);
    ASSERT((textSid[maxTextSidChars-2] = 0,1)); // check for overflow
    _sntprintf(textSid,maxTextSidChars,L"S-%u-",SID_REVISION);
    textSid[maxTextSidChars-1] = 0;
    size_t sidLength = wcslen(textSid);
    PSID_IDENTIFIER_AUTHORITY sidAuthority = GetSidIdentifierAuthority(
        sid);
    if(sidAuthority->Value[0] || sidAuthority->Value[1])
    {
        _sntprintf(textSid+sidLength,maxTextSidChars,
            L"0x%02hx%02hx%02hx%02hx%02hx%02hx",
            static_cast<UINT16>(sidAuthority->Value[0]),
            static_cast<UINT16>(sidAuthority->Value[1]),
            static_cast<UINT16>(sidAuthority->Value[2]),
            static_cast<UINT16>(sidAuthority->Value[3]),
            static_cast<UINT16>(sidAuthority->Value[4]),
            static_cast<UINT16>(sidAuthority->Value[5]));
        textSid[maxTextSidChars-1] = 0;
        sidLength += wcslen(textSid+sidLength);
    }
    else
    {
        _sntprintf(textSid+sidLength,maxTextSidChars,L"%u",
            static_cast<UINT>(sidAuthority->Value[5])+
            (static_cast<UINT>(sidAuthority->Value[4])<<8)+
            (static_cast<UINT>(sidAuthority->Value[3])<<16)+
            (static_cast<UINT>(sidAuthority->Value[2])<<24));
        textSid[maxTextSidChars-1] = 0;
        sidLength += wcslen(textSid+sidLength);
    }
    int subAuthorityCount = static_cast<int>(*GetSidSubAuthorityCount(sid));
    int index = 0;
    while(index < subAuthorityCount)
    {
        sidLength += _sntprintf(textSid+sidLength,maxTextSidChars,L"-%u",
            *GetSidSubAuthority(sid,index));
        textSid[maxTextSidChars-1] = 0;
        sidLength += wcslen(textSid+sidLength);
        index ++;
    }
    ASSERT(!textSid[maxTextSidChars-2]);
}

bool/*success*/ StrAddThreadSid(wchar_t* textSid,size_t maxTextSidChars)
{
    bool success = 0;
    HANDLE token = 0;
    if(!OpenThreadToken(GetCurrentThread(),TOKEN_QUERY,0,&token))
    {
        OpenProcessToken(GetCurrentProcess(),TOKEN_QUERY,&token);
    }
    if(token)
    {
        DWORD tokenSize = 0;
        VERIFYNOT(GetTokenInformation(token,TokenUser,0,0,&tokenSize));
        const size_t stackBufferSize = 300;
        UINT8 stackBuffer[stackBufferSize];
        TOKEN_USER* tokenInfo = reinterpret_cast<TOKEN_USER*>(stackBuffer);
        void* heapBuffer = 0;
        if(tokenSize > stackBufferSize)
        {
            tokenSize += 100;
            heapBuffer = malloc(tokenSize);
            tokenInfo = static_cast<TOKEN_USER*>(heapBuffer);
        }
        else
        {
            tokenSize = stackBufferSize;
        }
        if(tokenInfo)
        {
            if(GetTokenInformation(token,TokenUser,tokenInfo,tokenSize,
                &tokenSize))
            {
                if(textSid && maxTextSidChars)
                {
                    size_t length = wcslen(textSid);
                    ASSERT(length < maxTextSidChars);
                    SidConvertToText(tokenInfo->User.Sid,textSid+length,
                        maxTextSidChars-length);
                }
                success = 1;
            }
            if(heapBuffer)
            {
                free(heapBuffer);
            }
        }
        VERIFY(CloseHandle(token));
    }
    return success;
}

const wchar_t* RegLocalMachineName(void)
{
    const size_t maxMachineNameChars = 100;
    static wchar_t machineName[maxMachineNameChars] = L"";
    if(!machineName[0])
    {
        DWORD _size = maxMachineNameChars;
        if(!GetComputerName(machineName,&_size))
        {
            wcscpy(machineName,L".");
        }
        machineName[maxMachineNameChars-1] = 0;
    }
    return machineName;
}

#undef RegKeyClose
#undef RegKeyOpenRegistry
#undef RegKeyOpenThreadCurrentUser
#undef RegKeyOpenRead
#undef RegKeyOpen
#undef RegKeyCreate
#undef RegKeyCreateOrOpen

bool RegKeyIsNonConst(HKEY key)
{
    bool isNonConst = 1;
    switch(reinterpret_cast<INT_PTR>(key))
    {
    case 0:
    case reinterpret_cast<INT_PTR>(HKEY_LOCAL_MACHINE):
    case reinterpret_cast<INT_PTR>(HKEY_CURRENT_USER):
    case reinterpret_cast<INT_PTR>(HKEY_USERS):
        isNonConst = 0;
    }
    return isNonConst;
}

void RegKeyClose(HKEY key)
{
    if(RegKeyIsNonConst(key))
    {
        VERIFYIS(RegCloseKey(key),ERROR_SUCCESS);
    }
}

HKEY RegKeyOpenRegistry(HKEY constKey,const TCHAR* machineName)
{
    HKEY key = 0;
    switch(reinterpret_cast<INT_PTR>(constKey))
    {
    // If additional predefined keys are added here they should also be added
    // to the close function.
    case reinterpret_cast<INT_PTR>(HKEY_LOCAL_MACHINE):
    case reinterpret_cast<INT_PTR>(HKEY_CURRENT_USER):
    case reinterpret_cast<INT_PTR>(HKEY_USERS):
        // Open remote registry if a machine name is specified and it isn't
        // a name for the local machine. Otherwise just return the passed in
        // predefined key.
        key = constKey;
        if(machineName && machineName[0] &&
            Strcmp(machineName,_T(  "."         )) != 0 &&
            Strcmpi(machineName,_T( "localhost" )) != 0 &&
            Strcmp(machineName,_T(  "127.0.0.1" )) != 0)
        {
            const size_t maxScratchChars = 200;
            TCHAR scratch[maxScratchChars] = _T("");
            DWORD chars = maxScratchChars;
            if(!GetComputerName(scratch,&chars))
            {
                scratch[0] = 0;
            }
            scratch[maxScratchChars-1] = 0;
            if(Strcmpi(machineName,scratch) != 0)
            {
                scratch[0] = 0;
                Stradd(scratch,maxScratchChars,_T("\\\\"));
                Stradd(scratch,maxScratchChars,machineName);
                key = 0;
                RegConnectRegistry(scratch,constKey,&key);
            }
        }
        break;
    default:
        ASSERT(!"Invalid predefined registry key.");
    }
    return key;
}

HKEY RegKeyOpenRead(HKEY rootKey,const TCHAR* keyName)
{
    HKEY key;
    return rootKey?((RegOpenKeyEx(rootKey,keyName,0,KEY_READ,&key) ==
        ERROR_SUCCESS)?key:0):0;
}

HKEY RegKeyOpen(HKEY rootKey,const TCHAR* keyName)
{
    HKEY key;
    return rootKey?((RegOpenKeyEx(rootKey,keyName,0,KEY_READ|KEY_WRITE,&key)
        == ERROR_SUCCESS)?key:0):0;
}

HKEY RegKeyOpenThreadCurrentUser(const TCHAR* machineName)
{
    HKEY usersKey = RegKeyOpenRegistry(HKEY_USERS,machineName);
    const size_t maxScratchChars = 100;
    wchar_t scratch[maxScratchChars] = L"";
    StrAddThreadSid(scratch,maxScratchChars);
    HKEY key = RegKeyOpen(usersKey,scratch);
    RegKeyClose(usersKey);
    return key;
}

HKEY RegKeyCreate(HKEY rootKey,const TCHAR* keyName)
{
    HKEY key;
    return rootKey?((RegCreateKeyEx(rootKey,keyName,0,0,0,KEY_READ|KEY_WRITE,
        0,&key,0) == ERROR_SUCCESS)?key:0):0;
}

HKEY RegKeyCreateOrOpen(HKEY rootKey,const TCHAR* keyName)
{
    HKEY key = RegKeyCreate(rootKey,keyName);
    if(!key)
    {
        key = RegKeyOpen(rootKey,keyName);
    }
    return key;
}

bool/*success*/ RegKeyExists(HKEY rootKey,const TCHAR* keyName)
{
    HKEY key = RegKeyOpenRead(rootKey,keyName);
    RegKeyClose(key);
    return !!key;
}

bool/*success*/ RegValDelete(HKEY key,const TCHAR* valueName)
{
    return key && (RegDeleteValue(key,valueName) == ERROR_SUCCESS);
}

bool/*success*/ RegValSet(HKEY key,const TCHAR* valueName,int valueType,
    const void* value,size_t valueSize)
{
    return key && RegSetValueEx(key,valueName,0,static_cast<UINT32>(
        valueType),static_cast<BYTE*>(const_cast<void*>(value)),
        static_cast<UINT32>(valueSize)) == ERROR_SUCCESS;
}

bool/*success*/ RegValGet(HKEY key,const TCHAR* valueName,int* valueType,void*
    value,size_t maxValueSize,size_t* valueSize)
{
    bool success = 0;
    if(key)
    {
        DWORD size = static_cast<DWORD>(maxValueSize);
        DWORD type = 0;
        if(RegQueryValueEx(key,valueName,0,&type,static_cast<BYTE*>(value),
            &size) == ERROR_SUCCESS)
        {
            success = 1;
        }
        else
        {
            type = 0;
            size = 0;
        }
        if(valueType)
        {
            *valueType = static_cast<int>(type);
        }
        if(valueSize)
        {
            *valueSize = size;
        }
    }
    return success;
}

bool/*success*/ RegValCopy(HKEY sourceKey,const TCHAR* sourceValueName,HKEY
    destKey,const TCHAR* destValueName)
{
    bool success = 0;
    const size_t maxValueSize = 10000;
    char value[maxValueSize];
    if(!destValueName)
    {
        destValueName = sourceValueName;
    }
    int valueType;
    size_t valueSize;
    if(RegValGet(sourceKey,sourceValueName,&valueType,value,maxValueSize,
        &valueSize))
    {
        success = RegValSet(destKey,destValueName,valueType,value,valueSize);
    }
    else
    {
        success = RegValDelete(destKey,destValueName);
    }
    return success;
}

bool/*success*/ RegValSetInt(HKEY key,const TCHAR* valueName,int value)
{
    C_ASSERT(sizeof(int) == sizeof(DWORD));
    return RegValSet(key,valueName,REG_DWORD,&value,sizeof(int));
}

bool/*success*/ RegValSetTcs(HKEY key,const TCHAR* valueName,const TCHAR*
    value)
{
    if(!value)
    {
        value = _T("");
    }
    return RegValSet(key,valueName,REG_SZ,value,(_tcslen(value)+1)*sizeof(
        value[0]));
}

bool/*success*/ RegValGetInt(HKEY key,const TCHAR* valueName,int* value)
{
    int valueType;
    size_t valueSize;
    C_ASSERT(sizeof(int) == sizeof(DWORD));
    return RegValGet(key,valueName,&valueType,value,sizeof(int),&valueSize) &&
        valueType == REG_DWORD && valueSize == sizeof(int);
}

int/*value*/ RegValGetIntDef(HKEY key,const TCHAR* valueName,int defaultValue)
{
    int value;
    int valueType;
    size_t valueSize;
    C_ASSERT(sizeof(int) == sizeof(DWORD));
    return (RegValGet(key,valueName,&valueType,&value,sizeof(int),&valueSize)
        && valueType == REG_DWORD && valueSize == sizeof(int))?value:
        defaultValue;
}

bool/*success*/ RegValGetBool(HKEY key,const TCHAR* valueName,bool* _value)
{
    bool success = 0;
    int value;
    int valueType;
    size_t valueSize;
    C_ASSERT(sizeof(int) == sizeof(DWORD));
    if(RegValGet(key,valueName,&valueType,&value,sizeof(int),&valueSize) &&
        valueType == REG_DWORD && valueSize == sizeof(int))
    {
        success = 1;
        if(_value)
        {
            *_value = !!value;
        }
    }
    return success;
}

bool/*success*/ RegValGetTcs(HKEY key,const TCHAR* valueName,TCHAR* value,
    size_t maxValueChars)
{
    bool success = 0;
    int valueType;
    size_t valueSize;
    if(RegValGet(key,valueName,&valueType,value,maxValueChars*sizeof(value[
        0]),&valueSize) && (valueType == REG_SZ || valueType ==
        REG_EXPAND_SZ))
    {
        success = 1;
        ASSERT(valueSize <= maxValueChars*sizeof(value[0]));
        // make sure string is character aligned and null terminated
        size_t length = valueSize/sizeof(value[0]);
        value[(length < maxValueChars)?length:(maxValueChars-1)] = 0;
    }
    return success;
}

bool/*success*/ RegKeyEnum(HKEY key,int index,TCHAR* keyName,size_t
    maxChars)
{
    ASSERT(keyName && maxChars);
    bool success = 0;
    C_ASSERT(sizeof(DWORD) == sizeof(INT32));
    if(key && index >= 0 && maxChars <= UINT32_MAX)
    {
        DWORD _maxChars = static_cast<UINT32>(maxChars);
        FILETIME changeTime;
        if(RegEnumKeyEx(key,static_cast<UINT>(index),keyName,&_maxChars,0,0,0,
            &changeTime) == ERROR_SUCCESS)
        {
            keyName[maxChars-1] = 0;
            success = 1;
        }
    }
    return success;
}

bool/*success*/ RegValEnum(HKEY key,int index,TCHAR* valueName,size_t
    maxValueNameChars,int* _valueType,void* value,size_t maxValueSize,size_t*
    _valueSize)
{
    ASSERT(valueName && maxValueNameChars);
    bool success = 0;
    C_ASSERT(sizeof(DWORD) == sizeof(int));
    if(key && index >= 0 && maxValueNameChars < INT_MAX)
    {
        if(maxValueSize > UINT_MAX)
        {
            maxValueSize = UINT_MAX;
        }
        DWORD valueType;
        DWORD valueNameChars = static_cast<UINT>(maxValueNameChars);
        DWORD valueSize = static_cast<UINT>(maxValueSize);
        if(RegEnumValue(key,static_cast<UINT>(index),valueName,
            &valueNameChars,0,&valueType,static_cast<BYTE*>(value),
            &valueSize) == ERROR_SUCCESS)
        {
            valueName[maxValueNameChars-1] = 0;
            if(_valueType)
            {
                *_valueType = static_cast<int>(valueType);
            }
            if(_valueSize)
            {
                *_valueSize = valueSize;
            }
            success = 1;
        }
    }
    return success;
}

bool/*success*/ RegValEnumAsTcs(HKEY key,int index,TCHAR* valueName,size_t
    maxValueNameChars,TCHAR* _value,size_t _maxValueChars)
{
    ASSERT(valueName && maxValueNameChars);
    bool success = 0;
    const size_t maxScratchChars = 100;
    TCHAR scratch[maxScratchChars];
    TCHAR* value = _value;
    size_t maxValueChars = _maxValueChars;
    if(!_value || _maxValueChars < maxScratchChars)
    {
        value = scratch;
        maxValueChars = maxScratchChars;
    }
    int valueType;
    size_t valueSize;
    if(RegValEnum(key,index,valueName,maxValueNameChars,&valueType,value,
        maxValueChars*sizeof(value[0]),&valueSize))
    {
        if(_value && _maxValueChars)
        {
            switch(valueType)
            {
            case REG_DWORD:
                {
                    C_ASSERT(sizeof(DWORD) == sizeof(int));
                    int intValue = (valueSize >= sizeof(int))?
                        (*reinterpret_cast<int*>(value)):0;
                    _value[0] = 0;
                    Straddint(_value,_maxValueChars,intValue,0,0);
                }
                break;
            case REG_SZ:
            case REG_EXPAND_SZ:
                {
                    // make sure string is character aligned and null
                    // terminated
                    size_t length = valueSize/sizeof(value[0]);
                    value[(length < maxValueChars)?length:(maxValueChars-1)] =
                        0;
                    if(value != _value && _value && _maxValueChars)
                    {
                        _value[0] = 0;
                        Stradd(_value,_maxValueChars,value);
                    }
                }
                break;
            default:
                // other reg value types are not supported
                _value[0] = 0;
                break;
            }
        }
        success = 1;
    }
    return success;
}

// bool/*success*/ RegValGetAsTcs(HKEY key,const TCHAR* valueName,TCHAR* _value,
//     size_t _maxValueChars)
// {
//     ASSERT(valueName);
//     bool success = 0;
//     C_ASSERT(sizeof(int) == sizeof(DWORD));
//     const size_t maxScratchChars = 100;
//     TCHAR scratch[maxScratchChars];
//     TCHAR* value = _value;
//     size_t maxValueChars = _maxValueChars;
//     if(!_value || _maxValueChars < maxScratchChars)
//     {
//         value = scratch;
//         maxValueChars = maxScratchChars;
//     }
//     int valueType;
//     size_t valueSize;
//     if(RegValGet(key,valueName,&valueType,value,maxValueChars*sizeof(value[
//         0]),&valueSize))
//     {
//         switch(valueType)
//         {
//         case REG_DWORD:
//             if(valueSize >= sizeof(int))
//             {
//                 if(_value && _maxValueChars)
//                 {
//                     _value[0] = 0;
//                     Straddint(_value,_maxValueChars,*reinterpret_cast<int*>(
//                         value),0,0);
//                 }
//                 success = 1;
//             }
//             break;
//         case REG_SZ:
//         case REG_EXPAND_SZ:
//             {
//                 // make sure string is character aligned and null terminated
//                 size_t length = valueSize/sizeof(value[0]);
//                 value[(length < maxValueChars)?length:(maxValueChars-1)] = 0;
//                 if(value != _value && _value && _maxValueChars)
//                 {
//                     _value[0] = 0;
//                     Stradd(_value,_maxValueChars,value);
//                 }
//                 success = 1;
//             }
//             break;
//         }
//     }
//     return success;
// }

bool/*success*/ RegValGetTcsAsInt64(HKEY key,const TCHAR* valueName,INT64*
    value)
{
    bool success = 0;
    const size_t maxScratchChars = 100;
    wchar_t scratch[maxScratchChars];
    if(RegValGetTcs(key,valueName,scratch,maxScratchChars))
    {
        success = 1;
        if(value)
        {
            *value = Strtoint(scratch,wcslen(scratch),0);
        }
    }
    return success;
}

bool/*success*/ RegValSetTcsAsInt64(HKEY key,const TCHAR* valueName,INT64
    value)
{
    const size_t maxScratchChars = 100;
    wchar_t scratch[maxScratchChars] = L"";
    Straddint(scratch,maxScratchChars,value,0,0);
    return RegValSetTcs(key,valueName,scratch);
}

bool/*success*/ RegKeyDelete(HKEY rootKey,const TCHAR* keyName)
{
    return RegDeleteKey(rootKey,keyName) == ERROR_SUCCESS;
}


#ifdef DEBUGHEAP

// Registry handle leak detection. Allocate link structure for each key
// opened. Free link structure when matching key is closed. If exit occurs
// when handles are still open, the debug heap code will complain about the
// unfreed link structures.

MUTEX regKeyTagMutex;
struct RegKeyTagLink
{
    RegKeyTagLink* next;
    HKEY key;
};
RegKeyTagLink* firstRegKeyTagLink = 0;

HKEY RegKeyTag(HKEY key,const char* file,int line)
{
    if(RegKeyIsNonConst(key))
    {
        RegKeyTagLink* link = static_cast<RegKeyTagLink*>(DebugHeap_malloc(
            sizeof(RegKeyTagLink),file,line));
        ASSERT(link);
        if(link)
        {
            regKeyTagMutex.Lock();
            link->key = key;
            link->next = firstRegKeyTagLink;
            firstRegKeyTagLink = link;
            regKeyTagMutex.Unlock();
        }
    }
    return key;
}

void RegKeyUntag(HKEY key,bool dontAssert)
{
    if(RegKeyIsNonConst(key))
    {
        regKeyTagMutex.Lock();
        RegKeyTagLink** to = &firstRegKeyTagLink;
        RegKeyTagLink* link;
        while(!!(link = *to) && link->key != key)
        {
            to = &(link->next);
        }
        if(link)
        {
            *to = link->next;
        }
        regKeyTagMutex.Unlock();
        ASSERT(link || dontAssert);
        if(link)
        {
            ASSERT(link->key == key);
            DebugHeap_free(link);
        }
    }
}

void RegKeyIgnoreLeak(HKEY key)
{
    RegKeyUntag(key,1);
}

void DebugHeap_RegKeyClose(HKEY key)
{
    RegKeyClose(key);
    RegKeyUntag(key,0);
}

HKEY DebugHeap_RegKeyOpenRegistry(HKEY constKey,const TCHAR* machineName,
    const char* file,int line)
{
    return RegKeyTag(RegKeyOpenRegistry(constKey,machineName),file,line);
}

HKEY DebugHeap_RegKeyOpenThreadCurrentUser(const TCHAR* machineName,
    const char* file,int line)
{
    return RegKeyTag(RegKeyOpenThreadCurrentUser(machineName),file,line);
}

HKEY DebugHeap_RegKeyOpenRead(HKEY rootKey,const TCHAR* keyName,
    const char* file,int line)
{
    return RegKeyTag(RegKeyOpenRead(rootKey,keyName),file,line);
}

HKEY DebugHeap_RegKeyOpen(HKEY rootKey,const TCHAR* keyName,
    const char* file,int line)
{
    return RegKeyTag(RegKeyOpen(rootKey,keyName),file,line);
}

HKEY DebugHeap_RegKeyCreate(HKEY rootKey,const TCHAR* keyName,
    const char* file,int line)
{
    return RegKeyTag(RegKeyCreate(rootKey,keyName),file,line);
}

HKEY DebugHeap_RegKeyCreateOrOpen(HKEY rootKey,const TCHAR* keyName,
    const char* file,int line)
{
    return RegKeyTag(RegKeyCreateOrOpen(rootKey,keyName),file,line);
}

#endif


//----------------------------------------------------------------------------
// (C) Copyright 2004 McAfee Inc. All Rights Reserved
//----------------------------------------------------------------------------
