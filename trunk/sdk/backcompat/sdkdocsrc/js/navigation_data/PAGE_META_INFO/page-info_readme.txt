The meta info list can be updated in the .csv file and converted to json using csv2json.py with python.
If the meta info for a new page doesn't exist here, it will not show up in the sidebar.