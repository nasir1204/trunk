#!python3
f = open('navitems.csv', 'r');
output_filename = "page-info_ma.json"
first_line = f.readline()
column_names = first_line.split(",")
num_columns = len(column_names)
for i in range(0,len(column_names)): #Strip trailing newlines from column names
	column_names[i] = column_names[i].rstrip()

line = ""
full_json_str = ""
itr = 0
output_file = open(output_filename, 'w')
line = "dummy string"
while(len(line) > 0):
	json_str = ""
	windows = ""
	url = ""
	line = f.readline()
	columns = line.split(",")
	url = columns[0]
	if(len(url) == 0):
		break;
	
	url = url.replace("\\", "/")

	for i in range(1, num_columns):
		try:
			col = columns[i]
			if 'x' in col or 'X' in col:
				item_str = "true"
			else:
				item_str = "false"
		except:
			item_str = "false"

		json_item = "\"" + column_names[i] + "\":" + item_str
		json_str += json_item
		if(i < (num_columns - 1)):
			json_str += ", "

	json_str = "{\"url\":\"" + url + "\", " + json_str + "},\n"
	full_json_str += json_str
	itr += 1


output_file.write(full_json_str)
output_file.close()
f.close()


	

