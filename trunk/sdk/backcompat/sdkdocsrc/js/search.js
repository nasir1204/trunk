/**
 * MFS SDK Doc Search
 * If we've got jQuery, check to see if the search host is available, then, if it is, show the search box.
 */
if(typeof $ !== "undefined") {
    $(function() {
        var searchUrl = "http://oriondemo.corp.nai.org:8983/solr/select?json.wrf=?",
            crawlBase = "http://localhost:8000/";

        $.getJSON(searchUrl, {'wt':'json'},
            function() {
                // If the request succeeds, assume that Search is online and show the search box.
                $("#search")
                    .show()
                    .keyup(function(e) {
                        if(e.keyCode === 13) { // Enter
                            $.getJSON(
                                searchUrl,
                                {
                                    wt:'json',
                                    q: $("#search").val(),
                                    rows: 20
                                },
                                function(res) {
                                    $("#searchResults").empty().fadeIn(100);

                                    if(!res.response.docs.length) {
                                        $("<li/>")
                                            .text("No Results Found")
                                            .appendTo("#searchResults")
                                    }

                                    $.each(res.response.docs, function(i, doc) {
                                        $("<a/>")
                                            .text(doc.title)
                                            .attr('href', doc.url.replace(crawlBase, ""))
                                            .appendTo($("<li/>").appendTo("#searchResults"))
                                    })
                                }
                            );
                        }
                    })

                // Create the search results box, position it, setup events, then hide it.
                $("<ol/>")
                    .attr("id", "searchResults")
                    .css({
                        top: $("#search").offset().top + $("#search").outerHeight() + 2,
                        right: $(window).width() - $("#search").offset().left - $("#search").outerWidth()
                    })
                    .click(function(e) { e.stopPropagation(); })
                    .hide()
                    .appendTo("body")

                // When any part of the document is clicked, hide the search results box.
                $(document).click( function() { $("#searchResults").hide() });
            }
        )
    })
}