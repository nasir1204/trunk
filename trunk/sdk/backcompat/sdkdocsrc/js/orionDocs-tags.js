// creates a table of contents that links to each
// H2 in the document.
function buildTOC() {
	var headers = getElementsByTagNames("h2,h3"),
		TOC = document.getElementById("TOC"),
		currentH2,
		currentH3ul,
		header,
        id;

	
    if(TOC == null)
        return;

    removeChildren(TOC);
    
    for(var i = 0; i < headers.length; i++)
    {
		header = headers[i];


        if(header.id) {
            id = header.id;
        } else {
            var title = typeof(header.textContent) !== "undefined" ? header.textContent : header.innerText;
            id = toSlug(title);
        }

        var item = document.createElement("li");
        item.innerHTML = "<span class=\"toc-title\">" + "<a href=\"#" + id + "\">" + header.innerHTML + "</a>" + "</span>";
        
		// Since our headers are ordered, check if we're
		// an h3 and append it to the current h2.
		if(header.tagName === "H2") {
			currentH2 = item;
			currentH2.setAttribute("class", "toc-branch")
			currentH3Ul = null;
			TOC.appendChild(currentH2);
		}

		if(header.tagName === "H3") {
			if(!currentH3Ul) {
				currentH3Ul = document.createElement("ul");
				currentH3Ul.setAttribute("class", "toc-branch")
				currentH2.appendChild(currentH3Ul);
			}

			currentH3Ul.appendChild(item);
		}

        header.id = id;
        header.innerHTML+= "<a name=\"TOC_ITEM_" + i + "\"></a>";
    }
}

/**
 * Converts title string to a url-safe slug.
 * http://stackoverflow.com/a/1054862/1245595
 */
function toSlug(title) {
    return title
        .toLowerCase()
        .replace(/^\s\s*/, '')
        .replace(/\s\s*$/, '')
        .replace(/[^\w ]+/g,'')
        .replace(/ +/g,'-')
        ;
};

function buildTOCById()
{
    var TOC = document.getElementById("TOC");

    if(TOC == null)
        return;

    var titles = document.getElementsByTagName("h2");
    for(var i = 0; i < titles.length; i++)
    {
        // add the LI element.
        var title = titles[i];
        if( title.id )
        {
            var item = document.createElement("li");
            item.innerHTML = "<a href=\"#" + titles[i].id + "\">" + titles[i].innerHTML + "</a>";
            TOC.appendChild(item);
        }
    }
}

/*
 * Creates a Table of Contents that links to each H2 that has an id attribute in the document such that
 * that each of those H2's represents a step in a sequence of ordered steps (ie. Step 1, Step 2, etc).
 * ONLY H2's with id's will be included as steps in the sequence.
 * 
 * Useful for creating Tutorials or anything else that contains a series of steps.
 */
function buildTOCByIdAddingStepNumbers()
{
    var TOC = document.getElementById("TOC");

    if(TOC == null)
        return;

    removeChildren(TOC);
    addStepNumbersById();
    buildTOCById();
}

/*
 * Updates the H2's that have id's, prepending the text "Step X: " where X is the 
 * order of that H2 in the document.  Only H2's with id's will be changed.
 *
 */
function addStepNumbersById()
{
    var h2s = document.getElementsByTagName("h2");
    for(var i = 0; i < h2s.length; i++)
    {
    	if (h2s[i].id)
   		{
    		h2s[i].innerHTML = "Step " + i + ": " + h2s[i].innerHTML;
   		}
    }
}


/*
 * Remove any child nodes so we can create our own
 * This allows us to put in a dummy <li> node so that 
 * we can pass w3c validation for xhtml.
 * 
 */
function removeChildren(toc)
{
    if ( toc.hasChildNodes() )
    {
        while ( toc.childNodes.length >= 1 )
        {
            toc.removeChild( toc.firstChild );       
        } 
    }
}

function autoIframe(frameId, contentContainerId){
    try{
        frame = document.getElementById(frameId);
        innerDoc = (frame.contentDocument) ? frame.contentDocument : frame.contentWindow.document;
        contentContainer = contentContainerId ? innerDoc.getElementById(contentContainerId) : innerDoc.body;

        if (innerDoc == null){
            // Google Chrome (untested)
            frame.height = document.all[frameId].clientHeight + document.all[frameId].offsetHeight + document.all[frameId].offsetTop;
        }
        else if (frame.height != undefined){
            frame.height = contentContainer.scrollHeight;
    	}
        else {
            frame.style.height = contentContainer.scrollHeight;
        }
    }

    catch(err){
            alert('Err: ' + err.message);
        window.status = err.message;
    }
}

// Compare Position - MIT Licensed, John Resig
function comparePosition(a, b){
  return a.compareDocumentPosition ?
    a.compareDocumentPosition(b) :
    a.contains ?
      (a != b && a.contains(b) && 16) +
        (a != b && b.contains(a) && 8) +
        (a.sourceIndex >= 0 && b.sourceIndex >= 0 ?
          (a.sourceIndex < b.sourceIndex && 4) +
            (a.sourceIndex > b.sourceIndex && 2) :
          1) +
      0 :
      0;
}

// Original by PPK quirksmode.org
function getElementsByTagNames(list, elem) {
  elem = elem || document;
 
  var tagNames = list.split(','), results = [];
 
  for ( var i = 0; i < tagNames.length; i++ ) {
    var tags = elem.getElementsByTagName( tagNames[i] );
    for ( var j = 0; j < tags.length; j++ )
      results.push( tags[j] );
  }
 
  return results.sort(function(a, b){
    return 3 - (comparePosition(a, b) & 6);
  });
}


/**
 * User highlight.js to highlight code syntax in pre tags.
 */
var highlightSyntax = function() {
    if(typeof hljs !== "undefined") {
        setTimeout(function() {
            var pres = document.getElementsByTagName("pre");

            for(var i = 0; i < pres.length; i++) {
                hljs.highlightBlock(pres[i]);
            }
        }, 0);
    }
}

// Add syntax highlighting in browsers that support addEventListener (IE9+ & Everything Else)
// highlight.js is both buggy and slow in IE8-.
window.addEventListener && window.addEventListener("load", highlightSyntax)
